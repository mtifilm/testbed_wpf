// ---------------------------------------------------------------------------
#pragma hdrstop
#pragma package(smart_init)

#include "ColorBreathingTool.h"
#include "ColorBreathingGuiWin.h"
#include "bthread.h"
#include "ClipAPI.h"
#include "ColorPickTool.h"
#include "JobManager.h"
#include "MTIDialogs.h"
#include "MTIKeyDef.h"
#include "MTIsleep.h"
#include "MTImalloc.h"
#include "MTIstringstream.h"
#include "MTIWinInterface.h"  // for CBusyCursor
#include "NavigatorTool.h"
#include "PDL.h"
#include "PixelRegions.h"
#include "RegionOfInterest.h"
#include "SysInfo.h"
#include "ToolCommand.h"
#include "ToolProgressMonitor.h"
#include "ToolCommandTable.h"
#include "ToolSystemInterface.h"
#include "ToolUserInputMap.h"
#include "ColorBreathingProc.h"
#include "ColorBreathingRenderProc.h"
#include "TimeLine.h"
#include "ColorBreathingToolParameters.h"
#include "MathFunctions.h"
#include "MTIBitmap.h"
#include "MTIDialogs.h"
#include <algorithm>
#include "SpatialFlickerPreProc.h"
#include "SpatialFlickerRenderProc.h"
#include "err_cb.h"
#include "CmdSpawner.h"
#include "MaskTool.h"
#include "IpaStripeStream.h"

static MTI_UINT16 ColorBreathingToolNumber = COLORBREATHING_TOOL_NUMBER;

#define SPATIAL_FLICKER_SECTION "SpatialFlickerModel"
#define SPATIAL_FLICKER_ROI_SECTION  SPATIAL_FLICKER_SECTION".ROI"
#define ZONAL_FLICKER_SECTION "ZonalFlicker"
#define GLOBAL_FLICKER_SECTION "GlobalFlicker"

// -------------------------------------------------------------------------
// ColorBreathing Tool Default Keyboard and Mouse Button Configuration

static CUserInputConfiguration::SConfigItem ColorBreathingToolDefaultConfigItems[] =
{
	// ColorBreathing Tool Command             Modifiers      Action + Key
	{CB_CMD_ADD_REF_FRAME, " B		          KeyDown    "},
   {CB_CMD_DEL_REF_FRAME, " Ctrl+B 	       KeyDown    "},
	{CB_CMD_CLEAR_REF_GROUP, " Ctrl+Shift+B    KeyDown    "},
	{CB_CMD_GOTO_NEXT_REF_FRAME, " Ctrl+F          KeyDown    "},
	{CB_CMD_GOTO_PREV_FRAME, " Ctrl+S          KeyDown    "},
	{CB_CMD_RENDER_GRAPH, " Ctrl+G	       KeyDown    "},
	{CB_CMD_START_SELECT, " LButton         ButtonDown "},
   {CB_CMD_END_SELECT, " LButton         ButtonUp   "},
	{CB_CMD_REJECT_FIXES, " A               KeyDown    "},
   {CB_CMD_TOGGLE_FIXES, " T               KeyDown    "},
	{CB_CMD_PRESET_1, " 1               KeyDown    "},
	{CB_CMD_PRESET_2, " 2               KeyDown    "},
	{CB_CMD_PRESET_3, " 3               KeyDown    "},
   {CB_CMD_PRESET_4, " 4               KeyDown    "},
	{CB_CMD_CENTER_SEGMENT, " Shift+C         KeyDown    "},
   {CB_CMD_USE_ROI, " Shift+T         KeyDown    "},
	{CB_CMD_TOGGLE_ROI_USE, " Ctrl+T          KeyDown    "},
   {CB_CMD_LOCK_ROI, " Ctrl+Shift+T    KeyDown    "},
	{CB_CMD_ACCEPT_FIXES, " G               KeyDown    "},
   {CB_CMD_RENDER_MARKED, " Shift+G         KeyDown    "},
	{CB_CMD_STOP, " Ctrl+Space      KeyDown    "},
   {CB_CMD_PAUSE_OR_RESUME, " Space           KeyDown    "},
	{EXEC_BUTTON_CAPTURE_PDL, " ,               KeyDown    "},
	{EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS, " Shift+<         KeyDown    "},
	{CB_CMD_DEBUG_VIEW, " MButton			 ButtonDown "},
	{SF_CMD_TOGGLE_SHOW_BOXES, " Shift+T       KeyDown      "},
   {SF_CMD_TOGGLE_HIDE_ALL, " Alt+T       KeyDown      "},
};

static CUserInputConfiguration *ColorBreathingToolDefaultUserInputConfiguration =
	 new CUserInputConfiguration(sizeof(ColorBreathingToolDefaultConfigItems) / sizeof
	 (CUserInputConfiguration::SConfigItem), ColorBreathingToolDefaultConfigItems);

// -------------------------------------------------------------------------
// DewarpTool Tool Command Table

static CToolCommandTable::STableEntry ColorBreathingToolCommandTableEntries[] =
{
	// ColorBreathing Tool Command            ColorBreathing Tool Command String
	// -------------------------------------------------------------

	{CB_CMD_ADD_REF_FRAME, "CB_CMD_ADD_REF_FRAME"}, {CB_CMD_DEL_REF_FRAME, "CB_CMD_DEL_REF_FRAME"},
	{CB_CMD_CLEAR_REF_GROUP, "CB_CMD_CLEAR_REF_GROUP"}, {CB_CMD_GOTO_NEXT_REF_FRAME, "CB_CMD_GOTO_NEXT_REF_FRAME"},
	{CB_CMD_COLOR_BREAK, "CB_CMD_COLOR_BREAK"}};

static CToolCommandTable *ColorBreathingToolCommandTable =
	 new CToolCommandTable(sizeof(ColorBreathingToolCommandTableEntries) / sizeof(CToolCommandTable::STableEntry),
	 ColorBreathingToolCommandTableEntries);

//////////////////////////////////////////////////////////////////////
// CDewarpTool:
//////////////////////////////////////////////////////////////////////

ColorBreathingTool::ColorBreathingTool(const string &newToolName, MTI_UINT32 **newFeatureTable)
	 : CToolObject(newToolName, newFeatureTable)
{
	// Tool-specific constructors go here
	_colorBreathingModel = new ColorBreathingModel();
	_zonalFlickerModel = new SpatialFlickerModel();

	IniFileName = CPMPIniFileName("ColorBreathing");
	_colorBreathingModel->LoadAllSettingsFromIni(IniFileName);
	ColorPickTool = new CColorPickTool;
	LoadAllSettingFromIni(IniFileName);
}

void ColorBreathingTool::LoadAllSettingFromIni(const string &iniFileName)
{
	// auto ini = CreateIniFile(iniFileName);
	//
	// try
	// {
	// RECT rect;
	// // rect.top = ini->ReadInteger(SPATIAL_FLICKER_ROI_SECTION, "Top", 0);
	// // rect.bottom = ini->ReadInteger(SPATIAL_FLICKER_ROI_SECTION, "Bottom", 0);
	// // rect.left = ini->ReadInteger(SPATIAL_FLICKER_ROI_SECTION, "Left", 0);
	// // rect.right = ini->ReadInteger(SPATIAL_FLICKER_ROI_SECTION, "Right", 0);
	//
	// setZonalFlickerShowBoxes(ini->ReadBool(SPATIAL_FLICKER_ROI_SECTION, "ShowBoxes", false));
	//
	// setIsZonalFlickerRoiDisplayed(ini->ReadBool(SPATIAL_FLICKER_ROI_SECTION, "DisplayRoi", false));
	//
	// setZonalFlickerUseRoi(ini->ReadBool(SPATIAL_FLICKER_ROI_SECTION, "UseRoi", false));
	// ///        setZonalFlickerLockRoi(ini->ReadBool(SPATIAL_FLICKER_ROI_SECTION, "LockRoi", false));
	// auto temporalWindow = ini->ReadInteger(SPATIAL_FLICKER_ROI_SECTION, "TemporalWindow", 20);
	// }
	// catch (...)
	// {
	// TRACE_0(errout << "Unknow Error");
	// }
	// DeleteIniFile(ini);
}

void ColorBreathingTool::SaveAllSettingFromIni(const string &iniFileName)
{
	// auto ini = CreateIniFile(iniFileName);
	// ini->WriteBool(SPATIAL_FLICKER_ROI_SECTION, "LockRoi", getZonalFlickerLockRoi());
	// ini->WriteBool(SPATIAL_FLICKER_ROI_SECTION, "UseRoi", getZonalFlickerUseRoi());
	// ini->WriteBool(SPATIAL_FLICKER_ROI_SECTION, "DisplayRoi", getIsZonalFlickerRoiDisplayed());
	// ini->WriteBool(SPATIAL_FLICKER_ROI_SECTION, "ShowBoxes", getZonalFlickerShowBoxes());

	// auto roi = getZonalFlickerRoi();
	// ini->WriteInteger(SPATIAL_FLICKER_ROI_SECTION, "Top", roi.top);
	// ini->WriteInteger(SPATIAL_FLICKER_ROI_SECTION, "Bottom", roi.bottom);
	// ini->WriteInteger(SPATIAL_FLICKER_ROI_SECTION, "Left", roi.left);
	// ini->WriteInteger(SPATIAL_FLICKER_ROI_SECTION, "Right", roi.right);

	// DeleteIniFile(ini);
}

ColorBreathingTool::~ColorBreathingTool() {}

// ===================================================================
//
// Function:    toolInitialize
//
// Description: Called when the ToolManager wants to initialize
// the DewarpTool Tool
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================

int ColorBreathingTool::toolInitialize(CToolSystemInterface *newSystemAPI)
{
	int retVal;

	JobManager jobManager;
	jobManager.SetCurrentToolCode("cb");

	// Initialize the CDewarpTool's base class, i.e., CToolObject
	// This must be done before the ColorBreathingTool initializes itself
	retVal = initBaseToolObject(ColorBreathingToolNumber, newSystemAPI, ColorBreathingToolCommandTable,
		 ColorBreathingToolDefaultUserInputConfiguration);
	if (retVal != 0)
	{
		// ERROR: initBaseToolObject failed
		TRACE_0(errout << "Error in ColorBreathingTool Tool, initBaseToolObject failed " << retVal);
		return retVal;
	}

	ColorPickTool->loadSystemAPI(newSystemAPI);
	SET_CBHOOK(ColorPickerColorChanged, ColorPickTool->newColorHoveredOver);
	SET_CBHOOK(ColorPickerColorPicked, ColorPickTool->newColorPicked);

	// Create the Tool's GUI
	CreateColorBreathingToolGUI();

	_currentFrameIndex = -1;
	_doingOnRedraw = false;
	_needToTellGuiWeJustStoppedPlaying = false;
	_needToTellGuiThatFrameHasChanged = false;
	_multiFrameRenderInProgress = false;

	// clear the mask drawing flag
	_nowDrawingMask = false;
	return 0;

} // end toolInitialize

// ===================================================================
//
// Function:    toolActivate
//
// Description: Called when a tool is being Activated
//
// Arguments:
//
// Returns:
//
// ===================================================================
int ColorBreathingTool::toolActivate()
{
	// First
	int retVal = CToolObject::toolActivate();
	if (retVal != 0)
	{
		return retVal;
	}

	// Do we need to disable or re-enable the tool?
	if (isItOkToUseToolNow())
		getSystemAPI()->EnableTool();
	else
		getSystemAPI()->DisableTool(getToolDisabledReason());

	// Disable the Mask Tool
	///    _previousMaskToolEnabled = getSystemAPI()->InternalDisableMaskTool();

	// Enable mask tool ROI mode and set remembered mode
	getSystemAPI()->SetMaskAllowed(true, true, _maskWasInROIMode);
	getSystemAPI()->setMaskRoiIsRectangularOnly(true);

	_toolActive = true;

	JobManager jobManager;
	jobManager.SetCurrentToolCode("cb");

	getSystemAPI()->SetToolNameForUserGuide("DeflickerTool");

	auto clipTimeline = getSystemAPI()->getClipTimeline();
	SET_CBHOOK_DATA(ClipTimelineEventChanged, clipTimeline->EventChanged, &(clipTimeline->EventData));

	if (_deferredClipChangeFlag)
	{
		onNewClip();
	}

	_needToTellGuiThatFrameHasChanged = true;
	UpdateRenderedFramesThatWereDiscarded(0, getTotalFrames());
	return 0;
}

// ===================================================================
//
// Function:    toolDeactivate
//
// Description: Called when a tool is being deactivated
//
// Arguments:
//
// Returns:
//
// ===================================================================
int ColorBreathingTool::toolDeactivate()
{
	try
	{
		CBinManager binManager;

		SaveAllSettingFromIni(IniFileName);

		// Auto-accept GOV if pending
		getSystemAPI()->AcceptGOV();

		// Get rid of any review regions that might be present on the screen
		TryToResolveProvisional();

		DestroyAllToolSetups(true);
		_toolActive = false;

		GetToolProgressMonitor()->SetIdle();
		getSystemAPI()->SetGOVToolProgressMonitor(NULL);

		getGlobalFlickerModel()->BreakFrameData.WriteReferenceFrames();

		auto clipTimeline = getSystemAPI()->getClipTimeline();
		REMOVE_CBHOOK(ClipTimelineEventChanged, clipTimeline->EventChanged);
		getSystemAPI()->exitPreviewHackMode();

		_toolActive = false;
		if (IsInColorPickMode())
		{
			ToggleColorPicker();
		}

		// Remember if mask is in ROI mode and then disable ROI mode
		_maskWasInROIMode = getSystemAPI()->IsMaskInRoiMode();
		//// NO        getSystemAPI()->SetMaskAllowed(true, false);
		GColorBreathingTool->getSystemAPI()->setMaskVisibilityLockedOff(false);

		hideDisplayOverlay();
	}
	catch (const std::exception &ex)
	{
		TRACE_0(errout << "Unexpected std::exception " << ex.what());
	}
	catch (...)
	{
		TRACE_0(errout << "Alas, unknown execption");
	}

	// Last
	return CToolObject::toolDeactivate();
}

// ===================================================================
//
// Function:    toolShutdown
//
// Description: Called when the Tool Manager wants to shut down the
// DewarpTool Tool, typically when the main program is
// shutting down.
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================
int ColorBreathingTool::toolShutdown()
{
	int retVal;

	// Auto-accept GOV if pending
	getSystemAPI()->AcceptGOV();

	delete _colorBreathingModel;
	delete _zonalFlickerModel;
	delete ColorPickTool;

	DestroyColorBreathingToolGUI(); // Destroy the GUI
	GColorBreathingTool = NULL; // Say we are destroyed

	// Shutdown the DewarpTool Tool's base class, i.e., CToolObject.
	// This must be done after the DewarpTool shuts down itself
	retVal = shutdownBaseToolObject();
	if (retVal != 0)
	{
		// ERROR: shutdownBaseToolObject failed
		return retVal;
	}

	return retVal;
} // end toolShutdown

CToolProcessor* ColorBreathingTool::makeToolProcessorInstance(const bool *newEmergencyStopFlagPtr)
{
	switch (_toolProcessorTypeToMake)
	{
	case PROC_TYPE_GENERATE_RGB_VALUES:
		return new ColorBreathingProc(GetToolNumber(), GetToolName(), getSystemAPI(), newEmergencyStopFlagPtr,
			 GetToolProgressMonitor());

	case PROC_TYPE_RENDER_PROCESS:
		return new ColorBreathingRenderProc(GetToolNumber(), GetToolName(), getSystemAPI(), newEmergencyStopFlagPtr,
			 GetToolProgressMonitor());

	case PROC_TYPE_FLICKER_PREPROCESS:
		return new SpatialFlickerPreProc(GetToolNumber(), GetToolName(), getSystemAPI(), newEmergencyStopFlagPtr,
			 GetToolProgressMonitor());
		break;

	case PROC_TYPE_FLICKER_RENDER:
		return new SpatialFlickerRenderProc(GetToolNumber(), GetToolName(), getSystemAPI(), newEmergencyStopFlagPtr,
			 GetToolProgressMonitor());
		break;
	}

	CAutoErrorReporter autoErr("ColorBreathingTool::makeToolProcessorInstance", AUTO_ERROR_NOTIFY_WAIT,
		 AUTO_ERROR_TRACE_0);
	autoErr.traceAction = AUTO_ERROR_NO_TRACE;
	autoErr.errorCode = -1;
	autoErr.msg << "Logic error, do not know what to process";
	return nullptr;
}

void ColorBreathingTool::ResetResumeFrameIndex(int resumeFrame) {_resumeFrameIndex = resumeFrame;}

bool ColorBreathingTool::DoesToolPreserveHistory() {return false;}

void ColorBreathingTool::DestroyAllToolSetups(bool notIfPaused)
{
	int activeToolSetup = getSystemAPI()->GetActiveToolSetupHandle();

	if (_graphToolSetupHandle >= 0)
	{
		bool isPaused = (getSystemAPI()->GetToolSetupStatus(_graphToolSetupHandle) == AT_STATUS_PAUSED);
		if (!(notIfPaused && isPaused))
		{
			if (_graphToolSetupHandle == activeToolSetup)
				getSystemAPI()->SetActiveToolSetup(-1);
			getSystemAPI()->DestroyToolSetup(_graphToolSetupHandle);
			_graphToolSetupHandle = -1;
		}
	}

	if (_renderToolSetupHandle >= 0)
	{
		bool isPaused = (getSystemAPI()->GetToolSetupStatus(_renderToolSetupHandle) == AT_STATUS_PAUSED);
		if (!(notIfPaused && isPaused))
		{
			if (_renderToolSetupHandle == activeToolSetup)
				getSystemAPI()->SetActiveToolSetup(-1);
			getSystemAPI()->DestroyToolSetup(_renderToolSetupHandle);
			_renderToolSetupHandle = -1;
		}
	}

	if (_flickerPreProcessingToolSetupHandle >= 0)
	{
		bool isPaused = (getSystemAPI()->GetToolSetupStatus(_flickerPreProcessingToolSetupHandle) == AT_STATUS_PAUSED);
		if (!(notIfPaused && isPaused))
		{
			if (_flickerPreProcessingToolSetupHandle == activeToolSetup)
				getSystemAPI()->SetActiveToolSetup(-1);
			getSystemAPI()->DestroyToolSetup(_flickerPreProcessingToolSetupHandle);
			_flickerPreProcessingToolSetupHandle = -1;
		}
	}

	if (_flickerRenderToolSetupHandle >= 0)
	{
		bool isPaused = (getSystemAPI()->GetToolSetupStatus(_flickerRenderToolSetupHandle) == AT_STATUS_PAUSED);
		if (!(notIfPaused && isPaused))
		{
			if (_flickerRenderToolSetupHandle == activeToolSetup)
				getSystemAPI()->SetActiveToolSetup(-1);
			getSystemAPI()->DestroyToolSetup(_flickerRenderToolSetupHandle);
			_flickerRenderToolSetupHandle = -1;
		}
	}
} // DestroyAllToolSetups()

// ---------------------------------------------------------------------------

bool ColorBreathingTool::IsShowing(void) {return IsToolVisible();}

// ---------------------------------------------------------------------------

// ===================================================================
//
// Function:    toolHide
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int ColorBreathingTool::toolHide()
{
	HideColorBreathingToolGUI();
	doneWithTool();

	if (_oldBusyCursor == 0)
		SetBusyCursorToDefault();
	else if (_oldBusyCursor == 2)
		SetBusyCursorToBusyCross();

	return 0;
}

bool ColorBreathingTool::toolHideQuery()
{
	writeClipBoundingBox();
	return true;
}

// ===================================================================
//
// Function:    onUserInput
//
// Description: Navigator Tool's User Input Handler
//
// Arguments:   CUserInput &userInput
//
// Returns:     Results of command handling as CToolObject's return codes,
// TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================
int ColorBreathingTool::onUserInput(CUserInput &userInput)
{
	// default: user input will be passed back to the tool manager
	int retVal = TOOL_RETURN_CODE_NO_ACTION;

	if (IsDisabled() || !IsActive())
	{
		return retVal;
	}

	if ((userInput.action == USER_INPUT_ACTION_KEY_DOWN || userInput.action == USER_INPUT_ACTION_KEY_UP)
		 && userInput.key == MTK_TAB)
	{
		TWinControl *baseToolForm = getSystemAPI()->getBaseToolForm();
		if (baseToolForm->Visible)
		{
			getSystemAPI()->getBaseToolForm()->SetFocus();
		}

		return retVal;
	}

	// pass the event to the colorpicker
	switch (userInput.action)
	{
	case USER_INPUT_ACTION_MOUSE_BUTTON_DOWN:
		retVal = ColorPickTool->onMouseDown(userInput);
		break;
	case USER_INPUT_ACTION_MOUSE_BUTTON_UP:
		retVal = ColorPickTool->onMouseUp(userInput);
		break;

	default:
		break;
	}

	// If we're disabled or there's no clip, don't do anything
	if (!getSystemAPI()->isAClipLoaded())
	{
		return retVal;
	}

	// If we didn't handle the INPUT event, feed it back through the
	// onToolCommand interface!
	if (retVal == TOOL_RETURN_CODE_NO_ACTION)
	{
		retVal = CToolObject::onUserInput(userInput);
	}

	return retVal;
}

int ColorBreathingTool::OnStopPDLRendering()
{
	_deflickerStopFlag = true;
	auto err = getClipPreprocessData().reloadDefaultDatabase();
	if (err != 0)
	{
		CAutoErrorReporter autoErr("ColorBreathingTool::onPDLExecutionComplete", AUTO_ERROR_NOTIFY_WAIT,
			 AUTO_ERROR_TRACE_0);
		autoErr.traceAction = AUTO_ERROR_NO_TRACE;
		autoErr.errorCode = -1;
		autoErr.msg << "Could not reload information, please restart DRSNova";

	}

	return 0;
}

int ColorBreathingTool::onPDLExecutionComplete()
{
	auto err = getClipPreprocessData().reloadDefaultDatabase();
	if (err != 0)
	{
		CAutoErrorReporter autoErr("ColorBreathingTool::onPDLExecutionComplete", AUTO_ERROR_NOTIFY_WAIT,
			 AUTO_ERROR_TRACE_0);
		autoErr.traceAction = AUTO_ERROR_NO_TRACE;
		autoErr.errorCode = -1;
		autoErr.msg << "Could not reload information, please restart DRSNova";
	}

	return 0;
}

// ===================================================================
//
// Function:    onNewClip
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int ColorBreathingTool::onNewClip()
{
	try
	{
		if (!IsActive())
		{
			_deferredClipChangeFlag = true;
			return TOOL_RETURN_CODE_NO_ACTION;
		}

		_deferredClipChangeFlag = false;
		_turnOffHeartBeat = true;

		auto previousClipSize = _currentClipSize;

		// Do we need to disable or re-enable the tool?
		if (isItOkToUseToolNow())
			getSystemAPI()->EnableTool();
		else
			getSystemAPI()->DisableTool(getToolDisabledReason());

		DisplayRoi();

		auto curClip = getSystemAPI()->getClip();
		if (curClip != nullptr)
		{
			_initializeDataFromClipOnHeartBeat = true;
			// Set the image size
			const auto imgFmt = getSystemAPI()->getVideoClipImageFormat();

			_currentClipSize =
			{(int)imgFmt->getPixelsPerLine(), (int)imgFmt->getLinesPerFrame()};
		}

		// Reset the global ROI if the clip sizes are different
		if (previousClipSize != _currentClipSize)
		{
			_globalFlickerRoi.left = 0;
			_globalFlickerRoi.top = 0;
			_globalFlickerRoi.right = _currentClipSize.width;
			_globalFlickerRoi.bottom = _currentClipSize.height;
			_lastROI = _globalFlickerRoi;
		}

		readClipBoundingBox();

		// Update the zonal parameters, this probably isn't needed
		getClipPreprocessData().onNewClip();
      onNewMarks();
		updateZonalFlickerParameters();
	}
	catch (const std::exception &ex)
	{
		CAutoErrorReporter autoErr("ColorBreathingTool::OnNewClip", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
		autoErr.traceAction = AUTO_ERROR_NO_TRACE;
		autoErr.errorCode = -1;
		autoErr.msg << string(ex.what());
	}
	catch (...)
	{
		CAutoErrorReporter autoErr("ColorBreathingTool::OnNewClip", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
		autoErr.traceAction = AUTO_ERROR_NO_TRACE;
		autoErr.errorCode = -2;
		autoErr.msg << "Unknown error in onNew";
	}

	// Reset the read from database parameters
	_currentCutRange =
	{-1, -1};
	_oldFrameIndex = -1;
	_turnOffHeartBeat = false;
	return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

// ===================================================================
//
// Function:    onNewMarks
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int ColorBreathingTool::onNewMarks()
{
	auto clipLoaded = getSystemAPI()->isAClipLoaded();
	if (!IsActive() || IsDisabled() || !clipLoaded)
	{
		return TOOL_RETURN_CODE_NO_ACTION;
	}

	auto markIn = getSystemAPI()->getMarkIn();
	auto markOut = getSystemAPI()->getMarkOut();
	getGlobalFlickerModel()->RGBFrameData.SetMarkRange(markIn, markOut);
	getGlobalFlickerModel()->SetGraphDirty();

	MarksChange.Notify();
	_needToTellGuiThatFrameHasChanged = true;

	auto totalFrames = getTotalFrames();
	UpdateRenderedFramesThatWereDiscarded(0, totalFrames);

	// Now let the preprocess data know new marks
	getClipPreprocessData().onNewMarks();

   // Now load the parameters to the model if need be
    if (getClipPreprocessData().isSegmentAtMarksPreprocessed())
    {
        // Now load the parameters into the model
       auto row = getClipPreprocessData().getSegmentRowFromFrameIndex(markIn);
       auto zonalParameters = getClipPreprocessData().getZonalDatabase().getProcessedParameters(row);
       getZonalFlickerModel()->setParameters(zonalParameters);
    }
	// Check GUI (set by the setParameters above) matches processes
	if (getClipPreprocessData().isPreprocessingValidOverMarks())
	{
		resmoothZonalFlickerData();
	}

	return (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
}

// ===================================================================
//
// Function:    onChangingClip
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int ColorBreathingTool::onChangingClip()
{
	auto clipLoaded = getSystemAPI()->isAClipLoaded();
	if (!IsActive() || IsDisabled() || !clipLoaded)
	{
		return TOOL_RETURN_CODE_NO_ACTION;
	}

	ClipChanging.Notify();
	writeClipBoundingBox();
	getSystemAPI()->AcceptGOV(); // Auto-accept GOV if pending
	getSystemAPI()->ClearProvisional(false);
	MTImillisleep(50);
	DestroyAllToolSetups(false);
	return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

// ===================================================================
//
// Function:    onDeletingOpenedClip
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int ColorBreathingTool::onDeletingOpenedClip()
{
	_turnOffHeartBeat = true;
	getClipPreprocessData().getZonalDatabase().close();
	return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

// ===================================================================
//
// Function:    toolShow
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int ColorBreathingTool::toolShow()
{
	ShowColorBreathingToolGUI();

	// Todo:
	// if (!isItOkToUseToolNow())
	// getSystemAPI()-> DisableTool(getToolDisabledReason());

	// in MTIWinInterface, the cursor type is
	// 0 = default, 1 = arrows, 2 = cross
	_oldBusyCursor = CBusyCursor::getBusyCursorType();
	if (_oldBusyCursor != 1)
		SetBusyCursorToBusyArrows();

	return 0;
}

// ===================================================================
//
// Function:    onToolCommand
//
// Description: GrainTool Tool's Command Handler
//
// Arguments:   CToolCommand &toolCommand
//
// Returns:     Results of command handling as CToolObject's return codes,
// TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================

int ColorBreathingTool::onToolCommand(CToolCommand &toolCommand)
{
	if (IsDisabled() || !IsActive())
		return TOOL_RETURN_CODE_NO_ACTION;

	int commandNumber = toolCommand.getCommandNumber();
	int Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);

	const char *commandName = getCommandTable()->findCommandName(commandNumber);
	TRACE_2(errout << "Command: " << ((commandName != NULL) ? commandName : "GRN_UNKNOWN")
		 << " (" << commandNumber << ")");

	auto globalModel = getGlobalFlickerModel();

	// Dispatch loop for color breathing commands
	switch (commandNumber)
	{

	case CB_CMD_START_SELECT:
		Result = BeginUserSelect();
		break;

	case CB_CMD_END_SELECT:
		Result = EndUserSelect();
		break;

	case CB_CMD_REJECT_FIXES:
		getSystemAPI()->ClearProvisional(false);
		break;

	case CB_CMD_ACCEPT_FIXES:
		AcceptFix();
		break;

	case CB_CMD_TOGGLE_FIXES:
		if (CanWeToggle())
		{
			SetInPreviewMode(!GetInPreviewMode());
			_needToTellGuiThatGuiReferenceFramesHaveChanged = true;
		}

		Result = TOOL_RETURN_CODE_PROCESSED_OK | TOOL_RETURN_CODE_EVENT_CONSUMED;
		break;

	case CB_CMD_CENTER_SEGMENT:
		globalModel->SetCenterState(!globalModel->GetCenterState());
		onTimelineVisibleFrameRangeChange();
		_needToTellGuiThatGuiReferenceFramesHaveChanged = true;
		break;

	case CB_CMD_PRESET_1:
		switch (getSubToolType())
		{
		case SubToolType::GlobalFlicker:
			globalModel->GetPresets()->SetSelectedIndexAndCopyParametersToCurrent(0);
			_needToTellGuiThatGuiReferenceFramesHaveChanged = true;
			break;

		case SubToolType::ZonalFlicker:
			SelectZonalPreset(0);
			break;

		default:
			MTIassert(false);
		}

		break;

	case CB_CMD_PRESET_2:
		switch (getSubToolType())
		{
		case SubToolType::GlobalFlicker:
			globalModel->GetPresets()->SetSelectedIndexAndCopyParametersToCurrent(1);
			_needToTellGuiThatGuiReferenceFramesHaveChanged = true;
			break;

		case SubToolType::ZonalFlicker:
			SelectZonalPreset(1);
			break;

		default:
			MTIassert(false);
		}

		break;

	case CB_CMD_PRESET_3:
		switch (getSubToolType())
		{
		case SubToolType::GlobalFlicker:
			globalModel->GetPresets()->SetSelectedIndexAndCopyParametersToCurrent(2);
			_needToTellGuiThatGuiReferenceFramesHaveChanged = true;
			break;

		case SubToolType::ZonalFlicker:
			SelectZonalPreset(2);
			break;

		default:
			MTIassert(false);
		}

		break;

	case CB_CMD_PRESET_4:
		switch (getSubToolType())
		{
		case SubToolType::GlobalFlicker:
			globalModel->GetPresets()->SetSelectedIndexAndCopyParametersToCurrent(3);
			_needToTellGuiThatGuiReferenceFramesHaveChanged = true;
			break;

		case SubToolType::ZonalFlicker:
			SelectZonalPreset(3);
			break;

		default:
			MTIassert(false);
		}

		break;

	case CB_CMD_SAVE_PRESET:
		// Bad programming but whatever
		ColorBreathingForm->SavePresets();
		break;

	case CB_CMD_RENDER_GRAPH:
		if (getSubToolType() == SubToolType::ZonalFlicker)
		{
			ButtonCommandHandler(FLICKER_TOOL_PREPROCESS_CMD);
		}
		else
		{
			ButtonCommandHandler(CB_TOOL_GRAPH_CMD);
		}
		break;

	case CB_CMD_ADD_REF_FRAME:
		AddReferenceFrameAtCurrentIndex();
		break;

	case CB_CMD_DEL_REF_FRAME:
		DeleteReferenceFrameAtCurrentIndex();
		break;

	case CB_CMD_CLEAR_REF_GROUP:
		DeleteReferenceFrameInCurrentBreak();
		break;

	case CB_CMD_USE_ROI:
		setUseProcessingRegion(!getUseProcessingRegion());
		_needToTellGuiThatGuiReferenceFramesHaveChanged = true;
		break;

	case CB_CMD_DEBUG_VIEW:
		// Kludge
		{
			auto model = getGlobalFlickerModel();
			auto debugOn = !model->GetDumpDebugInfo();
			if (debugOn)
			{
				Beep();
			}
			getGlobalFlickerModel()->SetDumpDebugInfo(debugOn);
		}

		break;

	case CB_CMD_TOGGLE_ROI_USE:
		SetIsRoiDisplayed(!GetIsRoiDisplayed());
		_needToTellGuiThatGuiReferenceFramesHaveChanged = true;
		break;

	case CB_CMD_GOTO_NEXT_REF_FRAME:
		GotoNextReferenceFrame();
		break;

	case CB_CMD_GOTO_PREV_FRAME:
		GotoPreviousReferenceFrame();
		break;

	case CB_CMD_RENDER_MARKED:
		switch (getSubToolType())
		{
		case SubToolType::GlobalFlicker:
			ButtonCommandHandler(TOOL_PROCESSING_CMD_RENDER);
			break;

		case SubToolType::ZonalFlicker:
         if (CanWeToggle())
			{
            ButtonCommandHandler(FLICKER_TOOL_RENDER_CMD);
         }
         else
         {
            Beep();
         }
			break;

		default:
			MTIassert(false);
		}

		break;

	case CB_CMD_STOP:
		ButtonCommandHandler(TOOL_PROCESSING_CMD_STOP);
		break;

	case CB_CMD_PAUSE_OR_RESUME:
		if (getRenderState() == RENDER_STATE_IDLE)
		{
			Result = TOOL_RETURN_CODE_NO_ACTION;
		}
		else if (getRenderState() == RENDER_STATE_PAUSED)
		{
			////   ButtonCommandHandler(TOOL_PROCESSING_CMD_PAUSE);
			Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
		}
		else
		{
			//// ButtonCommandHandler(TOOL_PROCESSING_CMD_CONTINUE);
			Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
		}
		break;

	case CB_CMD_LOCK_ROI:
		if (GetIsRoiLocked())
		{
			if (_MTIConfirmationDialog(Handle, "Do you really wish to unlock the ROI") != MTI_DLG_OK)
			{
				break;
			}
		}

		SetIsRoiLocked(!GetIsRoiLocked());
		_needToTellGuiThatGuiReferenceFramesHaveChanged = true;
		Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
		break;

	case EXEC_BUTTON_CAPTURE_PDL:
	case EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS:
		{
			if (getSubToolType() == SubToolType::ZonalFlicker && getZonalProcessType() == ZonalProcessType::Unknown)
			{
				_MTIErrorDialog(Handle, "Please choose B&W or Color");
				Result = TOOL_RETURN_CODE_EVENT_CONSUMED;
				break;
			}

         // KLUDGE: reset database between marks
         getClipPreprocessData().setBetweenMarksToParameters(*_zonalFlickerModel);

			bool consumed = RunExecButtonsCommand((EExecButtonId) commandNumber);
			if (consumed)
				Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
			break;
		}

	case EXEC_BUTTON_PREVIEW_ALL:
		// if (getSubToolType() == SubToolType::ZonalFlicker)
		// {
		// ButtonCommandHandler(FLICKER_TOOL_PREVIEW_CMD);
		// }
		break;

	case SF_CMD_TURN_COLOR_PICKER_ON:
		if (!IsInColorPickMode())
			ToggleColorPicker();
		Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
		break;

	case SF_CMD_TURN_COLOR_PICKER_OFF:
		if (IsInColorPickMode())
			ToggleColorPicker();
		Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
		break;

	case SF_CMD_TOGGLE_SHOW_BOXES:
		setZonalFlickerShowBoxes(!getZonalFlickerShowBoxes());
		ExternalGuiRefresh(__LINE__);
		break;
	case SF_CMD_TOGGLE_HIDE_ALL:
		ToggleHideAllOverlays();
		break;
	}

	return Result;
} // onToolCommand

bool ColorBreathingTool::CanWeToggle()
{
	switch (getSubToolType())
	{
	case SubToolType::GlobalFlicker:
		return CanWeToggleCB();

	case SubToolType::ZonalFlicker:
		return CanWeToggleSF();

	default:
		MTIassert(false);
	}

	return false;
}

bool ColorBreathingTool::CanWeToggleSF()
{
   auto valid = getClipPreprocessData().isDeltaValidOverMarks();
   auto rendered = getClipPreprocessData().areMarksRendered();
   return valid && !rendered;
}

bool ColorBreathingTool::CanWeToggleCB()
{
	auto model = getGlobalFlickerModel();

	auto cutPair = model->BreakFrameData.GetBreakPair(GetCurrentFrameIndex());
	auto anyReferenceFrames = model->BreakFrameData.AreThereReferenceFramesHere(cutPair.first, cutPair.second); ;
	auto graphRunning = (_toolProcessorTypeToMake == PROC_TYPE_GENERATE_RGB_VALUES) &&
		 (getRenderState() == RENDER_STATE_RUNNING);

	return (!model->RGBFrameData[GetCurrentFrameIndex()].Rendered) && !graphRunning && model->RGBFrameData
		 [GetCurrentFrameIndex()].Valid && anyReferenceFrames;
}

bool ColorBreathingTool::CanWeRender()
{
	switch (getSubToolType())
	{
	case SubToolType::GlobalFlicker:
		return CanWeRenderColorBreathing();

	case SubToolType::ZonalFlicker:
		return CanWeRenderZonalFlicker();

	default:
		MTIassert(false);
	}

	return false;
}

bool ColorBreathingTool::CanWeRenderZonalFlicker()
{
	auto notRendered = getClipPreprocessData().areMarksRendered() == false;
	return getClipPreprocessData().isPreprocessingValidOverMarks() && notRendered;
}

// Duplicate code, we should set error flag
bool ColorBreathingTool::CanWeRenderColorBreathing()
{
	auto model = getGlobalFlickerModel();
	auto dataValid = model->RGBFrameData.IsGraphValid(getSystemAPI()->getMarkIn(), getSystemAPI()->getMarkOut());
	if (!dataValid)
	{
		return false;
	}

	// Find if any reference frames
	auto anyReferenceFrames = model->BreakFrameData.AreThereReferenceFramesHere(getSystemAPI()->getMarkIn(),
		 getSystemAPI()->getMarkOut());
	if (!anyReferenceFrames)
	{
		return false;
	}

	auto anyRenderedFrames = model->RGBFrameData.AreAnyRenderFramesValid(getSystemAPI()->getMarkIn(),
		 getSystemAPI()->getMarkOut());
	if (anyRenderedFrames)
	{
		return false;
	}

	return true;
}

bool ColorBreathingTool::CanWeRenderDisplayError()
{
	CAutoErrorReporter autoErr("Render Frames", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

	auto model = getGlobalFlickerModel();
	auto dataValid = model->RGBFrameData.IsGraphValid(getSystemAPI()->getMarkIn(), getSystemAPI()->getMarkOut());
	if (!dataValid)
	{
		return false;
	}

	// Find if any reference frames
	auto anyReferenceFrames = model->BreakFrameData.AreThereReferenceFramesHere(getSystemAPI()->getMarkIn(),
		 getSystemAPI()->getMarkOut());
	if (!anyReferenceFrames)
	{
		autoErr.traceAction = AUTO_ERROR_NO_TRACE;
		autoErr.errorCode = -1;
		autoErr.msg << "A Graph and reference frame(s) must be present in all of marked range";
		return false;
	}

	auto anyRenderedFrames = model->RGBFrameData.AreAnyRenderFramesValid(getSystemAPI()->getMarkIn(),
		 getSystemAPI()->getMarkOut());
	if (anyRenderedFrames)
	{
		autoErr.traceAction = AUTO_ERROR_NO_TRACE;
		autoErr.errorCode = -1;
		autoErr.msg << "The marked range contains rendered frames, please discard to rerender";

		return false;
	}

	return true;
}

int ColorBreathingTool::MonitorFrameProcessing(EToolProcessingCommand newToolCommand,
	 const CAutotoolStatus& newToolStatus, int newResumeFrame)
{
	int retVal = 0;
	EAutotoolStatus newToolProcessingStatus = newToolStatus.status;

	EToolControlState nextToolControlState;

	// Tool Processing Control State Machine
	nextToolControlState = toolProcessingControlState;
	switch (toolProcessingControlState)
	{
	case TOOL_CONTROL_STATE_STOPPED:
		if (newToolCommand == TOOL_PROCESSING_CMD_PREVIEW_1)
		{
			toolProcessingErrorCode = 0; // clear any old errors
			renderFlag = false;
			retVal = ProcessTrainingFrame();
			if (retVal == 0)
			{
				nextToolControlState = TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_STOPPED;
			}
		}
		else if (newToolCommand == TOOL_PROCESSING_CMD_PREVIEW)
		{
			toolProcessingErrorCode = 0; // clear any old errors
			renderFlag = false;
			retVal = RunFramePreviewing(newResumeFrame);
			if (retVal == 0)
			{
				nextToolControlState = TOOL_CONTROL_STATE_RUNNING;
			}
		}
		else if (newToolCommand == TOOL_PROCESSING_CMD_RENDER_1)
		{
			toolProcessingErrorCode = 0; // clear any old errors
			renderFlag = true;
			retVal = RenderSingleFrame();
			if (retVal == 0)
			{
				nextToolControlState = TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_STOPPED;
			}
		}
		else if (newToolCommand == TOOL_PROCESSING_CMD_RENDER)
		{
			toolProcessingErrorCode = 0; // clear any old errors
			renderFlag = true;
			retVal = RunFrameProcessing(newResumeFrame);
			if (retVal == 0)
				nextToolControlState = TOOL_CONTROL_STATE_RUNNING;
		}
		else if (newToolCommand == TOOL_PROCESSING_CMD_PREPROCESS)
		{
			toolProcessingErrorCode = 0; // clear any old errors
			renderFlag = false;
			retVal = RunFramePreprocessing();
			if (retVal == 0)
				nextToolControlState = TOOL_CONTROL_STATE_RUNNING;
		}

		break;
	case TOOL_CONTROL_STATE_WAITING_TO_STOP:
		if (newToolProcessingStatus == AT_STATUS_STOPPED)
		{
			setControlState(EMultiControlState::IDLE);
			nextToolControlState = TOOL_CONTROL_STATE_STOPPED;
		}
		break;
	case TOOL_CONTROL_STATE_RUNNING:
		if (newToolProcessingStatus == AT_STATUS_PROCESS_ERROR)
		{
			toolProcessingErrorCode = newToolStatus.errorCode;
			nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_STOP;
		}
		else if (newToolProcessingStatus == AT_STATUS_STOPPED)
		{
			if (getControlState() == EMultiControlState::ACQURING)
			{
				finishAcquire();
				if (_nextStateAfterAcquire == EMultiControlState::NEED_TO_RUN_RENDERER)
				{
					startRendering();
					setControlState(EMultiControlState::RENDERING);
					retVal = RunFrameProcessing(newResumeFrame);
					if (retVal != 0)
						nextToolControlState = TOOL_CONTROL_STATE_STOPPED;
				}
				else
				{
					setControlState(EMultiControlState::ACQURING_COMPLETE);
					nextToolControlState = TOOL_CONTROL_STATE_STOPPED;
				}
			}
			else
			{
				setControlState(EMultiControlState::RENDERING_COMPLETE);
				nextToolControlState = TOOL_CONTROL_STATE_STOPPED;
			}
		}
		else if (newToolCommand == TOOL_PROCESSING_CMD_STOP)
		{
			retVal = StopFrameProcessing();
			setControlState(EMultiControlState::IDLE);
			if (retVal == 0)
				nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_STOP;
		}
		else if (newToolCommand == TOOL_PROCESSING_CMD_PAUSE)
		{
			retVal = PauseFrameProcessing();
			if (retVal == 0)
				nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_PAUSE;
		}
		break;
	case TOOL_CONTROL_STATE_WAITING_TO_PAUSE:
		if (newToolProcessingStatus == AT_STATUS_STOPPED)
		{
			nextToolControlState = TOOL_CONTROL_STATE_STOPPED;
		}
		else if (newToolProcessingStatus == AT_STATUS_PAUSED)
		{
			nextToolControlState = TOOL_CONTROL_STATE_PAUSED;
		}
		else if (newToolCommand == TOOL_PROCESSING_CMD_STOP)
		{
			retVal = StopFrameProcessing();
			if (retVal == 0)
				nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_STOP;
		}
		break;
	case TOOL_CONTROL_STATE_PAUSED:
		if (newToolCommand == TOOL_PROCESSING_CMD_STOP)
		{
			retVal = StopFrameProcessing();
			if (retVal == 0)
				nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_STOP;
		}
		else if (newToolCommand == TOOL_PROCESSING_CMD_CONTINUE)
		{
			retVal = RunFrameProcessing(newResumeFrame);
			if (retVal == 0)
				nextToolControlState = TOOL_CONTROL_STATE_RUNNING;
		}
		else if (newToolCommand == TOOL_PROCESSING_CMD_PREVIEW_1)
		{
			retVal = ProcessTrainingFrame();
			if (retVal == 0)
			{
				nextToolControlState = TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_PAUSED;
			}
		}
		else if (newToolCommand == TOOL_PROCESSING_CMD_RENDER_1)
		{
			retVal = RenderSingleFrame();
			if (retVal == 0)
			{
				nextToolControlState = TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_PAUSED;
			}
		}
		break;
	case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_STOPPED:
	case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_STOPPED:
		if (newToolProcessingStatus == AT_STATUS_STOPPED)
		{
			nextToolControlState = TOOL_CONTROL_STATE_STOPPED;
		}
		else if (newToolCommand == TOOL_PROCESSING_CMD_STOP)
		{
			StopSingleFrameProcessing();
			nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_STOP;
		}
		break;
	case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_PAUSED:
	case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_PAUSED:
		if (newToolProcessingStatus == AT_STATUS_STOPPED)
		{
			nextToolControlState = TOOL_CONTROL_STATE_PAUSED;
		}
		else if (newToolCommand == TOOL_PROCESSING_CMD_STOP)
		{
			StopSingleFrameProcessing();
			nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_STOP;
		}
		break;
	default:
		break;
	}

	if (toolProcessingControlState != nextToolControlState)
	{
		// Control State has changed

		toolProcessingControlState = nextToolControlState;
      if (getSubToolType() == SubToolType::GlobalFlicker)
      {
         UpdateExecutionGUI(toolProcessingControlState, renderFlag);

         // Make sure the Processed/Original status bar panel is shown
         // correctly right after processing if the provisional is pending
         if (GetToolProcessingControlState() == TOOL_CONTROL_STATE_STOPPED)
         {
            getSystemAPI()->UpdateProvisionalStatusBarPanel();
         }
      }
      else
		{
// WTF.... WE CANNOT REMEMBER WHY WE DID THIS QQQ
// But it prevents the red STOP button from being enabled!
//         if (GetToolProcessingControlState() == TOOL_CONTROL_STATE_STOPPED)
         {
            UpdateExecutionGUI(toolProcessingControlState, renderFlag);
         }
      }
	}

	return retVal;

} // MonitorFrameProcessing

int ColorBreathingTool::onHeartbeat()
{
	if (_turnOffHeartBeat)
	{
		return 0;
	}

	if (_initializeDataFromClipOnHeartBeat)
	{
		_initializeDataFromClipOnHeartBeat = false;
		InitializeDataFromClip();
	}

   if (getSubToolType() == SubToolType::GlobalFlicker || toolProcessingControlState == TOOL_CONTROL_STATE_STOPPED)
   {
      if (_needToTellGuiThatFrameHasChanged)
      {
         _needToTellGuiThatFrameHasChanged = false;
         ExternalGuiRefresh(__LINE__);
      }

      if (_needToTellGuiThatGuiReferenceFramesHaveChanged)
      {
         _needToTellGuiThatGuiReferenceFramesHaveChanged = false;

         // Added because playing turns above flag off
         ExternalGuiReferenceFrameRefresh(__LINE__);
      }
   }

	if (_dirtyMediaHasChanged)
	{
		_dirtyMediaHasChanged = false;
		onTopPanelRedraw();
	}

	if (getGlobalFlickerModel()->GraphIsOutOfSync())
	{
		onTopPanelRedraw();
	}

	// If frame hasn't changed, this is very fast
	// auto markIn = GColorBreathingTool->getSystemAPI()->getMarkIn();
	// auto markOut = GColorBreathingTool->getSystemAPI()->getMarkOut();
	// if ((markIn <= getCurrentFrameIndex()) && (markOut > getCurrentFrameIndex());
	// {
	// loadZonalParametersIfNecessary();
	// }
	return TOOL_RETURN_CODE_NO_ACTION;
}

int ColorBreathingTool::onRedraw(int frameIndex)
{
	if (IsDisabled() || !IsActive())
	{
		return TOOL_RETURN_CODE_NO_ACTION;
	}

	// For sanity - re-entry protection. Things called from here are not
	// supposed to refresh the frame!!
	////    MTIassert(!_doingOnRedraw);
	if (_doingOnRedraw)
	{
		return TOOL_RETURN_CODE_EVENT_CONSUMED;
	}

	_doingOnRedraw = true;

	// Did the frame change?
	bool frameChanged = (frameIndex != _currentFrameIndex);
	_currentFrameIndex = frameIndex;

	if (frameChanged)
	{
		_needToTellGuiThatGuiReferenceFramesHaveChanged = true;
	}

	DisplayRoi();

	_doingOnRedraw = false;
	return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

int ColorBreathingTool::GetCurrentFrameIndex() const {return _currentFrameIndex;}

void ColorBreathingTool::ButtonCommandHandler(EToolProcessingCommand buttonCommand)
{
	int b = (int)buttonCommand;

	switch (b)
	{
	case TOOL_PROCESSING_CMD_PREVIEW_1:
		if (!getSystemAPI()->IsPlayerReallyPlaying())
		{
			getSystemAPI()->refreshFrameCached();
		}
		break;

	case TOOL_PROCESSING_CMD_RENDER:
		if (CanWeRenderDisplayError())
		{
			SetInPreviewMode(false);
			SetColorBreathingProcessing();
			StartToolProcessing(buttonCommand, false);
		}
		break;

	case CB_TOOL_GRAPH_CMD:
		{
			if (CheckIfFramesAreRenderedBetweenMarks())
			{
				if (_MTIConfirmationDialog(Handle, "Frames have been rendered\nDo you want to regraph?\nNo rendered frames will be affected") != MTI_DLG_OK)
            {
               break;
            }
			}

        	auto markIn = getSystemAPI()->getMarkIn();
			auto markOut = getSystemAPI()->getMarkOut();
         getGlobalFlickerModel()->RGBFrameData.Invalidate(markIn, markOut);
			SetRGBDataProcessing();
			StartToolProcessing(TOOL_PROCESSING_CMD_PREVIEW, false);
			_needToTellGuiThatGuiReferenceFramesHaveChanged = true;
		}
		break;

	case FLICKER_TOOL_PREPROCESS_CMD:
		if (CheckIfFlickerPreprocessingIsLegal() == true)
		{
			// We need to fix this
			ColorBreathingForm->ZonalAnalyzeButtonClick(nullptr);
			// SetFlickerPreProcessing();
			// StartToolProcessing(TOOL_PROCESSING_CMD_PREVIEW, false);
		}
		break;

	case FLICKER_TOOL_RENDER_CMD:
		if (CheckIfFlickerRenderIsLegal() == true)
		{
			SetInPreviewMode(false);
			SetFlickerRender();
			setRenderInProgress(true);
			StartToolProcessing(TOOL_PROCESSING_CMD_RENDER, false);
		}

		break;

	case FLICKER_TOOL_PREVIEW_CMD:
		if (CheckIfFlickerRenderIsLegal() == true)
		{
			SetInPreviewMode(false);
			SetFlickerRender();
			setRenderInProgress(true);
			StartToolProcessing(TOOL_PROCESSING_CMD_PREVIEW, false);
		}

		break;

	case TOOL_PROCESSING_CMD_PREVIEW:
		SetColorBreathingProcessing();
		StartToolProcessing(buttonCommand, false);
		break;

	case TOOL_PROCESSING_CMD_PAUSE:
		PauseToolProcessing();
		break;

	case TOOL_PROCESSING_CMD_CONTINUE:
		// resume frame was set when the pause button was hit
		ContinueToolProcessing(_resumeFrameIndex);
		break;
	case TOOL_PROCESSING_CMD_STOP:
		_deflickerStopFlag = true;
		setRenderInProgress(false);
		StopToolProcessing();
		break;

	default:
		break;
	}
}

bool ColorBreathingTool::CheckIfFramesAreRenderedBetweenMarks()
{
	auto model = getGlobalFlickerModel();
   return model->RGBFrameData.AreAnyRenderFramesValid(getSystemAPI()->getMarkIn(), getSystemAPI()->getMarkOut());
}

bool ColorBreathingTool::CheckAndReportErrorIfGraphingIsLegal()
{
	CAutoErrorReporter autoErr("Graph Frames", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

	auto model = getGlobalFlickerModel();

	auto anyRenderedFrames = CheckIfFramesAreRenderedBetweenMarks();
	if (anyRenderedFrames)
	{
		autoErr.traceAction = AUTO_ERROR_NO_TRACE;
		autoErr.errorCode = -1;
		autoErr.msg << "The marked range contains rendered frames, please discard to graph";

		return false;
	}

	return true;
}

ColorBreathingModel *ColorBreathingTool::getGlobalFlickerModel() const {return _colorBreathingModel;}

SpatialFlickerModel *ColorBreathingTool::getZonalFlickerModel() const {return _zonalFlickerModel;}

vector<int>ColorBreathingTool::getBreaksFromCurrentClip() {return getClipPreprocessData().getCutsFromClip();}

void ColorBreathingTool::UpdateBreaksFromClip()
{
	auto breaks = getBreaksFromCurrentClip();

	// Normally two breaks, but if timeline is in flux this gives 0
	if (breaks.size() < 2)
	{
		return;
	}

	auto model = getGlobalFlickerModel();
	model->BreakFrameData.SetBreaks(breaks);

	getClipPreprocessData().updateSegmentsFromBreaks();
	onTopPanelRedraw();
}

void ColorBreathingTool::ClipTimelineEventChanged(void *sender)
{
	auto eventData = (std::pair<MTI_UINT64, MTI_UINT64> *)(sender);
	UpdateBreaksFromClip();
	_needToTellGuiThatFrameHasChanged = true;
}

void ColorBreathingTool::SetRGBDataProcessing()
{
	_toolProcessorTypeToMake = PROC_TYPE_GENERATE_RGB_VALUES;
	return;
}

void ColorBreathingTool::SetColorBreathingProcessing()
{
	_toolProcessorTypeToMake = PROC_TYPE_RENDER_PROCESS;
	return;
}

void ColorBreathingTool::SetFlickerPreProcessing()
{
	_toolProcessorTypeToMake = PROC_TYPE_FLICKER_PREPROCESS;
	return;
}

void ColorBreathingTool::SetFlickerRender()
{
	_toolProcessorTypeToMake = PROC_TYPE_FLICKER_RENDER;
	return;
}

int ColorBreathingTool::RunFrameFlickerPreProcessing(int newResumeFrame)
{
	int retVal;
	CAutoErrorReporter autoErr(__func__, AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

	// This sets up all the preprocessing info
	auto toolSetupHandle = PROC_TYPE_INVALID;
	if (_flickerPreProcessingToolSetupHandle < 0)
	{
		// Create the tool setup
		CToolIOConfig ioConfig(1, 1);
		ioConfig.processingThreadCount = SysInfo::AvailableProcessorCoreCount();

		_flickerPreProcessingToolSetupHandle = getSystemAPI()->MakeSimpleToolSetup(COLORBREATHING_TOOL_NAME,
			 TOOL_SETUP_TYPE_MULTI_FRAME, &ioConfig);
		if (_flickerPreProcessingToolSetupHandle < 0)
		{
			autoErr.errorCode = -1;
			autoErr.msg << "ColorBreathingTool internal error: CNavSystemInterface::MakeSimpleToolSetup failed";
			return -1;
		}
	}

	toolSetupHandle = _flickerPreProcessingToolSetupHandle;

	// Set the active tool setup (this is different than the active tool)
	retVal = getSystemAPI()->SetActiveToolSetup(toolSetupHandle);
	if (retVal != 0)
	{
		autoErr.errorCode = retVal;
		autoErr.msg << "ColorBreathingTool internal error: SetActiveToolSetup(" << toolSetupHandle <<
			 ") failed with return code " << retVal;
		return retVal;
	}

	if (newResumeFrame < 0)
	{
		// Starting from Stop, so set processing frame range to in and out marks
		CToolFrameRange frameRange(1, 1);
		frameRange.inFrameRange[0].randomAccess = false;
		int markIn = getSystemAPI()->getMarkIn();
		int markOut = getSystemAPI()->getMarkOut();
		if (markIn < 0 && markOut < 0)
		{
			markIn = 0;
			markOut = getTotalFrames();
		}

		if (markIn < 0 || markOut < 0)
		{
			autoErr.traceAction = AUTO_ERROR_NO_TRACE;
			autoErr.errorCode = -1;
			autoErr.msg << "Mark In and Out must be set to Preview or Render";
			return -1;
		}
		else if (markOut <= markIn)
		{
			autoErr.traceAction = AUTO_ERROR_NO_TRACE;
			autoErr.errorCode = -1;
			autoErr.msg << "Mark Out must be beyond Mark In";
			return -1;
		}

		// Check to see if this is a preview frame
		frameRange.inFrameRange[0].inFrameIndex = markIn;
		frameRange.inFrameRange[0].outFrameIndex = markOut;

		retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
		if (retVal != 0)
		{
			autoErr.errorCode = retVal;
			autoErr.msg << "ColorBreathingTool internal error: SetToolFrameRange" << " failed with return code " << retVal;
			return retVal;
		}
	}
	else
	{
		// Resume needs to be added
		retVal = -22222;
		autoErr.errorCode = retVal;
		autoErr.msg << "ColorBreathingTool internal error: " << " resume not supported " << retVal;
		return retVal;
	}

	getSystemAPI()->SetProvisionalRender(false);

	// After this call, the parameters should be in correct stage execpt
	// inFrame and _outframe
	// updateZonalFlickerParameters();
	// auto tempDataFileName = getFlickerRepairDataFileName();
	// getZonalFlickerModel()->readFlickerData(getZonalFlickerParameters(), tempDataFileName);

	// Set tool parameters
	auto toolParams = new ZonalFlickerToolParameters(); ;
	toolParams->Tool = this;
	toolParams->Model = getZonalFlickerModel();

	// ----------Collect the model parameters
	// NOTE: Number of frames for Model is set in SetRange
	const CImageFormat* imgFmt = getSystemAPI()->getVideoClipImageFormat();
	if (imgFmt == nullptr)
	{
		return false;
	}

	// Set the image size
	toolParams->Model->setImageSize({.height = (int)imgFmt->getLinesPerFrame(), .width =
			 (int)imgFmt->getPixelsPerLine()});

	MTI_UINT16 componentValues[3];
	imgFmt->getComponentValueMax(componentValues);
	toolParams->Model->setMaxValue(componentValues[0]);

	// -------------------Done collecting them

	retVal = getSystemAPI()->SetToolParameters(toolParams);
	if (retVal != 0)
	{
		autoErr.errorCode = retVal;
		autoErr.msg << "ColorBreathing tool error in SetToolParameters" << " failed with return code " << retVal;
		return retVal;
	}

	// Start the image processing a-runnin'
	getSystemAPI()->RunActiveToolSetup();
	return 0;
}

/// FLICKER RENDER WORKING
int ColorBreathingTool::RunFrameFlickerRender(int newResumeFrame)
{
	CAutoErrorReporter autoErr(__func__, AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
	int retVal;

	_nextStateAfterAcquire = EMultiControlState::IDLE;

	// This sets up all the preprocessing info
	auto toolSetupHandle = PROC_TYPE_INVALID;
	if (_flickerRenderToolSetupHandle < 0)
	{
		// Create the tool setup
		CToolIOConfig ioConfig(1, 1);
		ioConfig.processingThreadCount = SysInfo::AvailableProcessorCoreCount();

		_flickerRenderToolSetupHandle = getSystemAPI()->MakeSimpleToolSetup(COLORBREATHING_TOOL_NAME,
			 TOOL_SETUP_TYPE_MULTI_FRAME, &ioConfig);
		if (_flickerRenderToolSetupHandle < 0)
		{
			autoErr.errorCode = -1;
			autoErr.msg << "ColorBreathingTool internal error: CNavSystemInterface::MakeSimpleToolSetup failed";
			return -1;
		}
	}

	toolSetupHandle = _flickerRenderToolSetupHandle;

	// Set the active tool setup (this is different than the active tool)
	retVal = getSystemAPI()->SetActiveToolSetup(toolSetupHandle);
	if (retVal != 0)
	{
		autoErr.errorCode = retVal;
		autoErr.msg << "Zonal Flicker internal error: SetActiveToolSetup(" << toolSetupHandle <<
			 ") failed with return code " << retVal;
		return retVal;
	}

	if (newResumeFrame < 0)
	{
		// Starting from Stop, so set processing frame range to in and out marks
		CToolFrameRange frameRange(1, 1);
		frameRange.inFrameRange[0].randomAccess = false;
		int markIn = getSystemAPI()->getMarkIn();
		int markOut = getSystemAPI()->getMarkOut();
		if (markIn < 0 && markOut < 0)
		{
			markIn = 0;
			markOut = getTotalFrames();
		}

		if (markIn < 0 || markOut < 0)
		{
			autoErr.traceAction = AUTO_ERROR_NO_TRACE;
			autoErr.errorCode = -1;
			autoErr.msg << "Mark In and Out must be set to Preview or Render";
			return -1;
		}
		else if (markOut <= markIn)
		{
			autoErr.traceAction = AUTO_ERROR_NO_TRACE;
			autoErr.errorCode = -1;
			autoErr.msg << "Mark Out must be beyond Mark In";
			return -1;
		}

		// Check to see if this is a preview frame
		frameRange.inFrameRange[0].inFrameIndex = markIn;
		frameRange.inFrameRange[0].outFrameIndex = markOut;

		retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
		if (retVal != 0)
		{
			autoErr.errorCode = retVal;
			autoErr.msg << "ColorBreathingTool internal error: SetToolFrameRange" << " failed with return code " << retVal;
			return retVal;
		}
	}
	else
	{
		// Resume needs to be added
		retVal = -22222;
		autoErr.errorCode = retVal;
		autoErr.msg << "ColorBreathingTool internal error: " << " resume not supported " << retVal;
		return retVal;
	}

	getSystemAPI()->SetProvisionalRender(renderFlag);

	// Set tool parameters
	auto toolParams = new ZonalFlickerToolParameters(); ;
	toolParams->Tool = this;

	toolParams->Model = getZonalFlickerModel();

	// ----------Collect the model parameters
	// NOTE: Number of rames for Model is set in SetRange
	// toolParams->Model->setRoi(getZonalFlickerRoi());
	// toolParams->Model->setUseRoi(getZonalFlickerUseRoi());

	const CImageFormat* imgFmt = getSystemAPI()->getVideoClipImageFormat();
	if (imgFmt == NULL)
		return false;

	// Set the image size
	toolParams->Model->setImageSize({.height = (int)imgFmt->getLinesPerFrame(), .width =
			 (int)imgFmt->getPixelsPerLine()});

	MTI_UINT16 componentValues[3];
	imgFmt->getComponentValueMax(componentValues);
	toolParams->Model->setMaxValue(componentValues[0]);

	// Temporary for debuggins
	// auto roi = toolParams->Model->getAnalysisRoi();
	// auto tempDataFileName = getDataFilePath() + "Boxes";
	// toolParams->Model->thinPlateSplineLutRawData.readData(tempDataFileName, roi);

	// end Temporary for debuggins

	// -------------------Done collecting them

	retVal = getSystemAPI()->SetToolParameters(toolParams);
	if (retVal != 0)
	{
		autoErr.errorCode = retVal;
		autoErr.msg << "ColorBreathing tool error in SetToolParameters" << " failed with return code " << retVal;
		return retVal;
	}

	getClipPreprocessData().updatePrerenderedFlags();
	// Start the image processing a-runnin'
	getSystemAPI()->RunActiveToolSetup();
	return 0;
}

int ColorBreathingTool::RunFrameProcessing(int newResumeFrame)
{
	int retVal;
	CAutoErrorReporter autoErr("ColorBreathingTool::RunFrameProcessing", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

	if (IsDisabled())
	{
		autoErr.errorCode = -999;
		autoErr.msg << "ColorBreathing internal error, tried to run when disabled ";
		return autoErr.errorCode;
	}

	const CImageFormat *imageFormat = getSystemAPI()->getVideoClipImageFormat();
	if (imageFormat == 0)
		return -1; // No image format, probably clip isn't loaded

	// See if GPU is ok
	GpuAccessor gpuAccessor;
	if (_isGpuUsed != gpuAccessor.useGpu())
	{
		DestroyAllToolSetups(true);
		_isGpuUsed = gpuAccessor.useGpu();
	}

	// Check if we have a tool setup yet
	auto toolSetupHandle = PROC_TYPE_INVALID;
	switch (_toolProcessorTypeToMake)
	{
	case PROC_TYPE_FLICKER_PREPROCESS:
		return RunFrameFlickerPreProcessing(newResumeFrame);

	case PROC_TYPE_FLICKER_RENDER:
		return RunFrameFlickerRender(newResumeFrame);

	default:
		break;
	}

	if (_toolProcessorTypeToMake == PROC_TYPE_GENERATE_RGB_VALUES)
	{
		if (_graphToolSetupHandle < 0)
		{
			// Create the tool setup
			CToolIOConfig ioConfig(1, 1);
			ioConfig.processingThreadCount = SysInfo::AvailableProcessorCoreCount();

			_graphToolSetupHandle = getSystemAPI()->MakeSimpleToolSetup(COLORBREATHING_TOOL_NAME,
				 TOOL_SETUP_TYPE_MULTI_FRAME, &ioConfig);
			if (_graphToolSetupHandle < 0)
			{
				autoErr.errorCode = -1;
				autoErr.msg << "ColorBreathingTool internal error: CNavSystemInterface::MakeSimpleToolSetup failed";
				return -1;
			}
		}

		toolSetupHandle = _graphToolSetupHandle;
	}

	// Check if we have a tool setup yet
	if (_toolProcessorTypeToMake == PROC_TYPE_RENDER_PROCESS)
	{
		if (_renderToolSetupHandle < 0)
		{
			// Create the tool setup
			CToolIOConfig ioConfig(1, 1);
			ioConfig.processingThreadCount = SysInfo::AvailableProcessorCoreCount();

			_renderToolSetupHandle = getSystemAPI()->MakeSimpleToolSetup(COLORBREATHING_TOOL_NAME,
				 TOOL_SETUP_TYPE_MULTI_FRAME, &ioConfig);
			if (_renderToolSetupHandle < 0)
			{
				autoErr.errorCode = -1;
				autoErr.msg << "ColorBreathingTool internal error: CNavSystemInterface::MakeSimpleToolSetup failed";
				return -1;
			}
		}

		toolSetupHandle = _renderToolSetupHandle;
	}

	// Set the active tool setup (this is different than the active tool)
	retVal = getSystemAPI()->SetActiveToolSetup(toolSetupHandle);
	if (retVal != 0)
	{
		autoErr.errorCode = retVal;
		autoErr.msg << "ColorBreathingTool internal error: SetActiveToolSetup(" << toolSetupHandle <<
			 ") failed with return code " << retVal;
		return retVal;
	}

	if (newResumeFrame < 0)
	{
		// Starting from Stop, so set processing frame range to in and out marks
		CToolFrameRange frameRange(1, 1);
		frameRange.inFrameRange[0].randomAccess = false;
		int markIn = getSystemAPI()->getMarkIn();
		int markOut = getSystemAPI()->getMarkOut();
		if (markIn < 0 && markOut < 0)
		{
			markIn = 0;
			markOut = getTotalFrames();
		}

		if (markIn < 0 || markOut < 0)
		{
			autoErr.traceAction = AUTO_ERROR_NO_TRACE;
			autoErr.errorCode = -1;
			autoErr.msg << "Mark In and Out must be set to Preview or Render";
			return -1;
		}
		else if (markOut <= markIn)
		{
			autoErr.traceAction = AUTO_ERROR_NO_TRACE;
			autoErr.errorCode = -1;
			autoErr.msg << "Mark Out must be beyond Mark In";
			return -1;
		}

		// Check to see if this is a preview frame
		frameRange.inFrameRange[0].inFrameIndex = markIn;
		frameRange.inFrameRange[0].outFrameIndex = markOut;

		retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
		if (retVal != 0)
		{
			autoErr.errorCode = retVal;
			autoErr.msg << "ColorBreathingTool internal error: SetToolFrameRange" << " failed with return code " << retVal;
			return retVal;
		}
	}
	else
	{
		// // Resuming from Pause state, so set processing frame range
		// // from newResumeFrame to out mark
		// CToolFrameRange frameRange(1, 1);
		// frameRange.inFrameRange[0].randomAccess = false;
		// int markOut = getSystemAPI()->getMarkOut();
		// if (markOut < 0)
		// {
		// autoErr.traceAction = AUTO_ERROR_NO_TRACE;
		// autoErr.errorCode = -1;
		// autoErr.msg << "Mark Out must be set to Preview or Render";
		// return -1;
		// }
		// else if (markOut <= newResumeFrame)
		// {
		// autoErr.traceAction = AUTO_ERROR_NO_TRACE;
		// autoErr.errorCode = -1;
		// autoErr.msg << "Mark Out must be beyond the Resume frame";
		// return -1;
		// }
		// frameRange.inFrameRange[0].inFrameIndex = newResumeFrame;
		// frameRange.inFrameRange[0].outFrameIndex = markOut;
		// retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
		// if (retVal != 0)
		// {
		// autoErr.errorCode = retVal;
		// autoErr.msg << "ColorBreathingTool internal error: SetToolFrameRange" << " failed with return code " << retVal;
		// return retVal;
		// }
	}

	// // Set render/preview flag for tool infrastructure.
	// // Don't ever render during the acquire pass!
	if (_toolProcessorTypeToMake == PROC_TYPE_GENERATE_RGB_VALUES)
	{
		getSystemAPI()->SetProvisionalRender(false);
	}
	else
	{
		getSystemAPI()->SetProvisionalRender(renderFlag);
	}

	// Set tool parameters
	auto toolParams = new ColorBreathingToolParameters(); ;
	toolParams->Tool = this;
	toolParams->Model = getGlobalFlickerModel();

	retVal = getSystemAPI()->SetToolParameters(toolParams);
	if (retVal != 0)
	{
		autoErr.errorCode = retVal;
		autoErr.msg << "ColorBreathing tool error in SetToolParameters" << " failed with return code " << retVal;
		return retVal;
	}

	// Start the image processing a-runnin'
	getSystemAPI()->RunActiveToolSetup();

	return 0;
} // RunFrameProcessing()

// ===================================================================
//
// Function:    GetToolProcessingControlState
//
// Description: This is a stupid hack to make PDL rendering work correctly.
// The problem is that we will first run the TrackTool, then
// when that's done we run the StabilizerProc.
// But the PDL sees that the toolProcessingControlState is STOPPED
// while the TrackTool is running so it assumes that rendering has
// been completed.
//
// Arguments:
//
// Returns:
//
// ===================================================================

EToolControlState ColorBreathingTool::GetToolProcessingControlState()
{
	EToolControlState retVal = toolProcessingControlState;
	// switch (getControlState())
	// {
	// // case EMultiControlState::ACQURING_COMPLETE:
	// // retVal = TOOL_CONTROL_STATE_RUNNING;
	// // break;
	// case EMultiControlState::RENDERING_COMPLETE:
	// case EMultiControlState::GOT_AN_ERROR:
	// case EMultiControlState::IDLE:
	// break;
	//
	// default:
	// retVal = TOOL_CONTROL_STATE_RUNNING;
	// break;
	// }

	return retVal;
}

// ===================================================================
// Function:    UpdateExecutionGUI
// Description: Update the GUI according to the process control state from the
// tool infrastructure (overrides CToolObject version). Also use
// it to initiate another pass through the frames since Colorbreathing
// processing requires an an acquire pass followed by a render pass.
// Arguments: TI control state + render/preview flag.
// Returns:
// ===================================================================

void ColorBreathingTool::UpdateExecutionGUI(EToolControlState toolProcessingControlState, bool processingRenderFlag)
{
	setToolProcessingControlState(toolProcessingControlState);
	CAutoErrorReporter autoErr("ColorBreathingTool::onNewClip", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

	UpdateExecutionButtons(toolProcessingControlState, processingRenderFlag);
	if (IsActive)
	{
		if (toolProcessingControlState == TOOL_CONTROL_STATE_STOPPED)
		{
			if (getGlobalFlickerModel()->RGBFrameData.NewProcessedData == true)
			{
				auto err = getGlobalFlickerModel()->RGBFrameData.Save();
				if (err != 0)
				{
					autoErr.traceAction = AUTO_ERROR_NO_TRACE;
					autoErr.errorCode = err;
					autoErr.msg << "Error saving Graph data";
				}
			}
		}

		switch (toolProcessingControlState)
		{
		case TOOL_CONTROL_STATE_RUNNING:
			_renderState = RENDER_STATE_RUNNING;
			break;

		case TOOL_CONTROL_STATE_PAUSED:
			_renderState = RENDER_STATE_PAUSED;
			break;

		case TOOL_CONTROL_STATE_STOPPED:
			_renderState = RENDER_STATE_IDLE;
			break;

		case TOOL_CONTROL_STATE_WAITING_TO_STOP:
			_runRenderAfterStop = false;
			setAbortPreprocessingFlag(true);
			break;

		default:
			break;
		}

		ExternalGuiRefresh(__LINE__);
	} // UpdateExecutionGUI()
}

int ColorBreathingTool::GetPreviewFrameIndex(void) {return _previewFrameIndex;}

bool ColorBreathingTool::areMarksValid() {return getClipPreprocessData().areMarksValid();}

void ColorBreathingTool::setZonalFlickerRoi(const RECT &newBox)
{
	// Remove duplicate data
	_zonalFlickerRoi = newBox;
	getZonalFlickerModel()->setRoi(newBox);
	makeTileBoxes();

	getClipPreprocessData().computeIsDefaultPreprocessingDone();
	if (!getSystemAPI()->IsPlayerReallyPlaying())
	{
		getSystemAPI()->refreshFrameCached();
	}

	DisplayRoi();
}

void ColorBreathingTool::setZonalProcessType(ZonalProcessType value)
{
	if (getZonalProcessType() == value)
	{
		return;
	}

	getZonalFlickerModel()->setZonalProcessType(value);
	getClipPreprocessData().computeIsDefaultPreprocessingDone();
}

void ColorBreathingTool::EnterColorPickMode(void) {ColorPickTool->SetEnabled(true);}
// ----------------------------------------------------------------------------

void ColorBreathingTool::ExitColorPickMode(void) {ColorPickTool->SetEnabled(false);}
// ----------------------------------------------------------------------------

bool ColorBreathingTool::IsInColorPickMode(void) {return ColorPickTool->IsEnabled();}

// ----------------------------------------------------------------------------

void ColorBreathingTool::ColorPickerColorChanged(void *Sender)
{
	// We don't do anything here yet
}
// ----------------------------------------------------------------------------

void ColorBreathingTool::ColorPickerColorPicked(void *Sender)
{
	int R, G, B, X, Y;
	ColorPickTool->getColor(R, G, B);
	ColorPickTool->getXY(X, Y);
	FindBoundingBox(R, G, B, X, Y);
	BoundBoxHasChanged.Notify();
}

void ColorBreathingTool::setBoundingBoxInternal(const RECT &newBox)
{
	// _boundingBox = newBox;
	// _BoundingBoxDefined = true;
	// if (getSubToolType() == SubToolType::ColorBreathing)
	// {
	// _roiRect = _boundingBox;
	// }
	// else
	// {
	// setZonalFlickerRoi(_boundingBox);
	// makeTileBoxes();
	// }
	//
	// if (!getSystemAPI()->IsPlayerReallyPlaying())
	// {
	// auto curClip = getSystemAPI()->getClip();
	// writeBoundingBoxToDesktopIni(curClip);
	// getSystemAPI()->refreshFrameCached();
	// }
}

bool ColorBreathingTool::FindBoundingBox(int R, int G, int B, int X, int Y, int slack)
{
	const CImageFormat* imgFmt = getSystemAPI()->getVideoClipImageFormat();
	if (imgFmt == NULL)
		return false;

	// Get some value
	int _pxlComponentCount = imgFmt->getComponentCountExcAlpha();
	int nPicCol = imgFmt->getPixelsPerLine();
	int nPicRow = imgFmt->getLinesPerFrame();
	bool bRGB = (imgFmt->getPixelComponents() == IF_PIXEL_COMPONENTS_RGB) ||
		 (imgFmt->getPixelComponents() == IF_PIXEL_COMPONENTS_BGR) ||
		 (imgFmt->getPixelComponents() == IF_PIXEL_COMPONENTS_RGBA) ||
		 (imgFmt->getPixelComponents() == IF_PIXEL_COMPONENTS_BGRA) ||
		 (imgFmt->getPixelComponents() == IF_PIXEL_COMPONENTS_Y) ||
		 (imgFmt->getPixelComponents() == IF_PIXEL_COMPONENTS_YYY);

	RECT _ActiveBox = imgFmt->getActivePictureRect();

	// Hack - assume slack was for 10-bit data, so if it's 8, divide by 2
	// (dividing by 4 makes it too narrow, I think
	if (imgFmt->getPixelPacking() == IF_PIXEL_PACKING_8Bits_IN_1Byte)
		slack /= 2;

	// Allocate buffer
	int pxlCnt = nPicRow * nPicCol;
	int totWds = (2 * pxlCnt * _pxlComponentCount + ((pxlCnt + 7) / 8) + 1) / 2;
	MTI_UINT16 *BaseImage = new MTI_UINT16[totWds];

	getSystemAPI()->getLastFrameAsIntermediate(BaseImage);
	setBoundingBoxInternal(FindMatBox(BaseImage, nPicCol, nPicRow, slack, _ActiveBox, bRGB, R, G, B, X, Y));
	delete[]BaseImage;
	return true;
}

void ColorBreathingTool::setUseAnalysisRegion(const bool value)
{
	switch (getSubToolType())
	{
	case SubToolType::GlobalFlicker:
		setColorBreathingUseAnalysisRegion(value);
		break;

	case SubToolType::ZonalFlicker:
		setZonalFlickerUseAnalysisRegion(value);
		break;

	default:
		MTIassert(false);
	}
}

bool ColorBreathingTool::getUseAnalysisRegion() const
{
	switch (getSubToolType())
	{
	case SubToolType::GlobalFlicker:
		return getColorBreathingUseAnalysisRegion();

	case SubToolType::ZonalFlicker:
		return getZonalFlickerUseAnalysisRegion();

	default:
		MTIassert(false);
	}

	return false;
}

void ColorBreathingTool::setColorBreathingUseAnalysisRegion(const bool value)
{
	if (getColorBreathingUseAnalysisRegion() == value)
	{
		return;
	}

	getGlobalFlickerModel()->setUseAnalysisRegion(value);
}

bool ColorBreathingTool::getColorBreathingUseAnalysisRegion() const {
	return getGlobalFlickerModel()->getUseAnalysisRegion();}

bool ColorBreathingTool::getZonalFlickerUseAnalysisRegion() const {
	return getZonalFlickerModel()->getUseAnalysisRegion();}

void ColorBreathingTool::setZonalFlickerUseAnalysisRegion(bool value)
{
	if (getZonalFlickerUseAnalysisRegion() == value)
	{
		return;
	}

	getZonalFlickerModel()->setUseAnalysisRegion(value);
	makeTileBoxes();
}

void ColorBreathingTool::SetIsRoiDisplayed(const bool newDisp)
{
	switch (getSubToolType())
	{
	case SubToolType::GlobalFlicker:
		setIsGlobalFlickerRoiDisplayed(newDisp);
		break;

	case SubToolType::ZonalFlicker:
		setIsZonalFlickerRoiDisplayed(newDisp);
		break;

	default:
		MTIassert(false);
	}
}

void ColorBreathingTool::setIsGlobalFlickerRoiDisplayed(const bool newDisp)
{
	if (GetIsRoiDisplayed() == newDisp)
	{
		return;
	}

	_isGlobalFlickerRoiDisplayed = newDisp;

	if (!getSystemAPI()->IsPlayerReallyPlaying())
	{
		getSystemAPI()->refreshFrameCached();
	}
}

void ColorBreathingTool::setIsZonalFlickerRoiDisplayed(const bool newDisp)
{
	auto visible = AreAllOverlaysHidden() ? false : newDisp;

	if (GetIsRoiDisplayed() == visible)
	{
		return;
	}

	_isZonalFlickerRoiDisplayed = visible;

	if (!getSystemAPI()->IsPlayerReallyPlaying())
	{
		getSystemAPI()->refreshFrameCached();
	}
}

bool ColorBreathingTool::GetIsRoiDisplayed() const
{
	switch (getSubToolType())
	{
	case SubToolType::GlobalFlicker:
		return getIsGlobalFlickerRoiDisplayed();

	case SubToolType::ZonalFlicker:
		return getIsZonalFlickerRoiDisplayed();

	default:
		MTIassert(false);
	}

	return false;
}

void ColorBreathingTool::DisplayRoi()
{
	switch (getSubToolType())
	{
	case SubToolType::GlobalFlicker:
		DisplayRoiColorBreathing();
		break;

	case SubToolType::ZonalFlicker:
		DisplayRoiZonalFlicker();
		break;

	default:
		MTIassert(false);
		break;
	}
}

void ColorBreathingTool::DisplayRoiZonalFlicker(void)
{
	if (getIsZonalFlickerRoiDisplayed())
	{
		getSystemAPI()->setGraphicsColor(SOLID_CYAN);
		getSystemAPI()->drawRectangleFrame(&_zonalFlickerRoi);
	}
}

void ColorBreathingTool::DisplayRoiColorBreathing(void)
{
	if (GetIsRoiDisplayed())
	{
		auto model = getGlobalFlickerModel();
		auto n = model->RGBFrameData.size();
		if ((_currentFrameIndex < n) && (_currentFrameIndex >= 0))
		{
			auto rgbData = getGlobalFlickerModel()->RGBFrameData.at(_currentFrameIndex);
			if (rgbData.Valid)
			{
				_globalFlickerRoi.left = rgbData.ROI.Left;
				_globalFlickerRoi.right = rgbData.ROI.Right;
				_globalFlickerRoi.top = rgbData.ROI.Top;
				_globalFlickerRoi.bottom = rgbData.ROI.Bottom;
				_lastROI = _globalFlickerRoi;
			}
			else
			{
				// KLUDGE, if a new mark in, mark out set and ROI is not valid, reset it
				// THIS NEEDS TO BE FIXED
				if ((rgbData.ROI.Left != _lastROI.left) || (rgbData.ROI.Top != _lastROI.top) ||
					 (rgbData.ROI.Bottom != _lastROI.bottom) || (rgbData.ROI.Right != _lastROI.right))
				{
					auto markIn = getSystemAPI()->getMarkIn();
					auto markOut = getSystemAPI()->getMarkOut();
					if ((_currentFrameIndex >= markIn) && (_currentFrameIndex < markOut))
					{
						_globalFlickerRoi = _lastROI;
						model->RGBFrameData.SetROI(markIn, markOut, _globalFlickerRoi);
					}
					else
					{
						_lastROI = _globalFlickerRoi;
					}
				}

				model->RGBFrameData.SetAllUnsetROI(_lastROI, _currentFrameIndex);
			}

			getSystemAPI()->setGraphicsColor(SOLID_CYAN);
			getSystemAPI()->drawRectangleFrame(&_globalFlickerRoi);
		}
	}
}

void ColorBreathingTool::setUseProcessingRegion(const bool value)
{
	switch (getSubToolType())
	{
	case SubToolType::GlobalFlicker:
		setGlobalFlickerUseProcessingRegion(value);
		break;

	case SubToolType::ZonalFlicker:
		setZonalFlickerUseProcessingRegion(value);
		break;

	default:
		MTIassert(false);
	}

	if (!getSystemAPI()->IsPlayerReallyPlaying())
	{
		getSystemAPI()->refreshFrameCached();
	}
}

bool ColorBreathingTool::getUseProcessingRegion() const
{
	switch (getSubToolType())
	{
	case SubToolType::GlobalFlicker:
		return getGlobalFlickerUseProcessingRegion();

	case SubToolType::ZonalFlicker:
		return getZonalFlickerUseProcessingRegion();

	default:
		MTIassert(false);
	}

	return false;
}

void ColorBreathingTool::setZonalFlickerUseProcessingRegion(bool use) {
	getZonalFlickerModel()->setUseProcessingRegion(use);}

bool ColorBreathingTool::getZonalFlickerUseProcessingRegion() const {
	return getZonalFlickerModel()->getUseProcessingRegion();}

void ColorBreathingTool::setGlobalFlickerUseProcessingRegion(bool use) {
	getGlobalFlickerModel()->setUseProcessingRegion(use);}

bool ColorBreathingTool::getGlobalFlickerUseProcessingRegion() const {
	return getGlobalFlickerModel()->getUseProcessingRegion();}

int ColorBreathingTool::onTopPanelRedraw()
{
	if (!IsActive())
	{
		return 0;
	}

	if (getSubToolType() != SubToolType::GlobalFlicker)
	{
		if (getSystemAPI()->GetMainWindowTopPanelVisibility() == true)
		{
			getSystemAPI()->SetMainWindowTopPanelVisibility(false);
		}

		return 0;
	}

	if (!getSystemAPI()->GetMainWindowTopPanelVisibility())
	{
		getSystemAPI()->SetMainWindowTopPanelVisibility(true);
	}

	auto bitmap = getSystemAPI()->GetBitmapRefForTopPanelDrawing();
	auto bitmapData = (unsigned char *)bitmap->getBitmapFrameBuffer();
	auto h = bitmap->getHeight();
	if (h == 0)
	{
		return 0;
	}

	if (h <= 2)
	{
		// auto g = Graphics.FromImage(bitmap);
		// g.Clear(0x0);
		return 0;
	}

	auto frame = getSystemAPI()->getLastFrameIndex();

	CHRTimer hrt;
	hrt.Start();
	if (getGlobalFlickerModel()->ComputeBitmap(bitmap->getWidth(), h - 4, frame, bitmapData) != 0)
	{
		return -1;
	}

	getSystemAPI()->DrawBitmapOnMainWindowTopPanel();
	return 0;
}

// ----------------------------------------------------------------------------

EMultiControlState ColorBreathingTool::getControlState() {return _multiControlState;}

// ----------------------------------------------------------------------------

void ColorBreathingTool::setControlState(EMultiControlState newControlState)
{
	if (_multiControlState != newControlState)
	{
		_multiControlState = newControlState;
	}
}

int ColorBreathingTool::onTimelineVisibleFrameRangeChange()
{
	if (IsActive() == false)
	{
		return TOOL_RETURN_CODE_NO_ACTION;
	}

	if (getGlobalFlickerModel()->GetCenterState() == false)
	{
		_needToTellGuiThatFrameHasChanged = true;
		auto curClip = getSystemAPI()->getClip();
		int inIndex = -1; ;
		int exclusiveOutIndex = -1;
		getSystemAPI()->getTimelineVisibleFrameIndexRange(inIndex, exclusiveOutIndex);
		getGlobalFlickerModel()->SetDisplayIn(inIndex);
		getGlobalFlickerModel()->SetDisplayOut(exclusiveOutIndex);
	}

	return TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK;
}

int ColorBreathingTool::onSwitchSubtool()
{
	if (IsActive() == false)
	{
		return TOOL_RETURN_CODE_NO_ACTION;
	}

	if (ColorBreathingForm->ColorBreathingPageControl->ActivePage == ColorBreathingForm->ColorBreathingTabSheet)
	{
		ColorBreathingForm->ColorBreathingPageControl->ActivePage = ColorBreathingForm->FlickerTabSheet;
	}
	else
	{
		ColorBreathingForm->ColorBreathingPageControl->ActivePage = ColorBreathingForm->ColorBreathingTabSheet;
	}

	////    Application->ProcessMessages();   // ????

	return TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK;
}

int ColorBreathingTool::onMouseMove(CUserInput &userInput)
{
	int retVal = TOOL_RETURN_CODE_NO_ACTION;

	if (IsDisabled() || getSubToolType() == SubToolType::GlobalFlicker)
	{
		return retVal;
	}

	if (_autoRoiModeActivationIsPending)
	{
		// Because of the fucking ROI hack where this tool does all the ROI drawing,
		// we can't let move event go through to the mask tool at this point!
		retVal = TOOL_RETURN_CODE_EVENT_CONSUMED;

		const int AutoRoiActivationSaneMoveDistance = 100;
		_autoRoiModeActivationIsPending = false;

		auto xMove = userInput.imageX - _autoRoiStartMouseX;
		auto yMove = userInput.imageY - _autoRoiStartMouseY;
		auto distanceMovedSinceClick = sqrt((xMove * xMove) + (yMove * yMove));
		if (!getSystemAPI()->IsMaskInRoiMode() && distanceMovedSinceClick < AutoRoiActivationSaneMoveDistance)
		{
			// Mouse moved after click, so we're good to turn on ROI mode.
			getSystemAPI()->SetMaskAllowed(true, true, true);
		}
		else
		{
			// Sonething's bogus - cancel stretching.
			getSystemAPI()->stopStretchRectangleOrLasso();
			SetIsRoiDisplayed(_isZonalFlickerRoiDisplayedOld);
		}
	}

	_mouseX = userInput.imageX;
	_mouseY = userInput.imageY;

	return retVal;
}

int ColorBreathingTool::BeginUserSelect()
{
	switch (getSubToolType())
	{
	case SubToolType::GlobalFlicker:
		return BeginUserSelectColorBreathing();

	case SubToolType::ZonalFlicker:
		return BeginUserSelectZonalFlicker();
		break;

	default:
		MTIassert(false);
	}

	return TOOL_RETURN_CODE_NO_ACTION;
}

void ColorBreathingTool::NotifyMaskChanged(const string &newMaskAsString) {::NotifyMaskChanged(newMaskAsString);}

void ColorBreathingTool::NotifyMaskVisibilityChanged() {::NotifyMaskVisibilityChanged();}

int ColorBreathingTool::BeginUserSelectZonalFlicker()
{
	CAutoErrorReporter autoErr("ColorBreathingTool::BeginUserSelect", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
	auto result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);

	if (GetIsRoiLocked())
	{
		GetToolProgressMonitor()->SetStatusMessage("Zonal Flicker ROI is locked");
		return result;
	}

	if (getSystemAPI()->IsMaskEnabled() == false)
	{
		// Auto turn on ROI mode!
		// getSystemAPI()->SetMaskAllowed(true, true, true);
		_autoRoiStartMouseX = _mouseX;
		_autoRoiStartMouseY = _mouseY;
		_autoRoiModeActivationIsPending = true;
	}
	else if (getSystemAPI()->IsMaskInRoiMode() == false)
	{
		return TOOL_RETURN_CODE_NO_ACTION;
	}

	_isZonalFlickerRoiDisplayedOld = getIsZonalFlickerRoiDisplayed();

	GetToolProgressMonitor()->SetStatusMessage("Drawing Zonal Flicker ROI");

	// set the color for RECTs, LASSOs, and BEZIERs        x
	getSystemAPI()->setGraphicsColor(SOLID_CYAN);
	getSystemAPI()->setDRSLassoMode(false);
	getSystemAPI()->setStretchRectangleColor(SOLID_CYAN);
	getSystemAPI()->startStretchRectangleOrLasso();
	getSystemAPI()->ClearBezier(false);
	SetIsRoiDisplayed(false);

	return result;
}

int ColorBreathingTool::BeginUserSelectColorBreathing()
{
	CAutoErrorReporter autoErr("ColorBreathingTool::BeginUserSelect", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
	auto result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);

	if (GetIsRoiLocked())
	{
		GetToolProgressMonitor()->SetStatusMessage("Global Flicker ROI is locked");
		return result;
	}

	if (getSystemAPI()->IsMaskEnabled() == false)
	{
		// Auto turn on ROI mode!
		getSystemAPI()->SetMaskAllowed(true, true, true);
	}

	if (getSystemAPI()->IsMaskInRoiMode() == false)
	{
		return TOOL_RETURN_CODE_NO_ACTION;
	}

	_isGlobalFlickerRoiDisplayedOld = GetIsRoiDisplayed();

	GetToolProgressMonitor()->SetStatusMessage("Drawing Global Flicker ROI");

	auto markIn = getSystemAPI()->getMarkIn();
	auto markOut = getSystemAPI()->getMarkOut();
	if ((_currentFrameIndex < markIn) || (_currentFrameIndex >= markOut))
	{
		autoErr.traceAction = AUTO_ERROR_NO_TRACE;
		autoErr.errorCode = -1;
		autoErr.msg << "ROI can only be set in marked section";
		return result;
	}

	// set the color for RECTs, LASSOs, and BEZIERs
	getSystemAPI()->setGraphicsColor(SOLID_CYAN);
	getSystemAPI()->setDRSLassoMode(false);
	getSystemAPI()->setStretchRectangleColor(SOLID_CYAN);
	getSystemAPI()->startStretchRectangleOrLasso();
	getSystemAPI()->ClearBezier(false);
	SetIsRoiDisplayed(false);

	return result;
}

int ColorBreathingTool::EndUserSelect()
{
	switch (getSubToolType())
	{
	case SubToolType::GlobalFlicker:
		return EndUserSelectColorBreathing();

	case SubToolType::ZonalFlicker:
		return EndUserSelectZonalFlicker();

	default:
		MTIassert(false);
		break;
	}

	return TOOL_RETURN_CODE_NO_ACTION;
}

int ColorBreathingTool::EndUserSelectZonalFlicker()
{
	GetToolProgressMonitor()->SetStatusMessage("Idle");
	getSystemAPI()->stopStretchRectangleOrLasso();
	setIsZonalFlickerRoiDisplayed(_isZonalFlickerRoiDisplayedOld);

	if (_autoRoiModeActivationIsPending)
	{
		_autoRoiModeActivationIsPending = false;
		initializeMask();
		return TOOL_RETURN_CODE_NO_ACTION;
	}

	if (getSystemAPI()->IsMaskInRoiMode() == false)
	{
		initializeMask();
		return TOOL_RETURN_CODE_NO_ACTION;
	}

	auto result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
	if (GetIsRoiLocked())
	{
		return result;
	}

	POINT *lassoPtr = nullptr;
	RECT newRoi;
	if (getSystemAPI()->getStretchRectangleCoordinates(newRoi, lassoPtr) == 0)
	{
		auto w = newRoi.right - newRoi.left;
		auto h = newRoi.bottom - newRoi.top;
		if ((w < 100) || (h < 100))
		{
			_MTIErrorDialog(Handle, "The ROI is too small");
			return result;
		}

		setZonalFlickerRoi(newRoi);
		setIsZonalFlickerRoiDisplayed(true);
	}

	ExternalGuiRefresh(__LINE__);
	return result;
}

int ColorBreathingTool::EndUserSelectColorBreathing()
{
	if (getSystemAPI()->IsMaskInRoiMode() == false)
	{
		return TOOL_RETURN_CODE_NO_ACTION;
	}

	auto result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
	if (GetIsRoiLocked())
	{
		return result;
	}

	GetToolProgressMonitor()->SetStatusMessage("Idle");

	getSystemAPI()->stopStretchRectangleOrLasso();
	POINT *lassoPtr = nullptr;
	RECT newRoi;
	if (getSystemAPI()->getStretchRectangleCoordinates(newRoi, lassoPtr) == 0)
	{
		SetIsRoiDisplayed(true);

		auto model = getGlobalFlickerModel();
		auto markIn = getSystemAPI()->getMarkIn();
		auto markOut = getSystemAPI()->getMarkOut();

		// See if we are valid
		auto anyRenderValid = model->RGBFrameData.AreAnyRenderFramesValid(markIn, markOut);
		if (anyRenderValid)
		{
			_MTIErrorDialog(Handle,
				 "The ROI cannot be changed because some of the marked range has been rendered\nPlease discard or commit the rendered frames");

			SetIsRoiDisplayed(_isGlobalFlickerRoiDisplayedOld);
			_needToTellGuiThatFrameHasChanged = true;
			getSystemAPI()->refreshFrameCached();
			return result;
		}

		auto graphValid = model->RGBFrameData.IsGraphValid(markIn, markOut);
		if (graphValid)
		{
			if (_MTIConfirmationDialog(Handle,
				 "Redrawing the ROI will delete the graph for the marked range\nYes to delete graph, No to cancel")
				 != MTI_DLG_OK)
			{
				SetIsRoiDisplayed(_isGlobalFlickerRoiDisplayedOld);
				_needToTellGuiThatFrameHasChanged = true;
				getSystemAPI()->refreshFrameCached();
				return result;
			}
		}

		auto imageFormat = getSystemAPI()->getVideoClipImageFormat();
		auto pixelsCol = imageFormat->getPixelsPerLine();
		auto pixelsRow = imageFormat->getLinesPerFrame();
		newRoi.left = std::max(0, (int)newRoi.left);
		newRoi.top = std::max(0, (int)newRoi.top);
		newRoi.right = std::min((int)pixelsCol, (int)newRoi.right);
		newRoi.bottom = std::min((int)pixelsRow, (int)newRoi.bottom);

		// At this point see what has been done
		model->RGBFrameData.SetROI(markIn, markOut, newRoi);
		_lastROI = newRoi;
		model->LoadRGBDataInToTimeline();
		_globalFlickerRoi = newRoi;
		model->SetGraphDirty();
		onTopPanelRedraw();
		SetIsRoiDisplayed(true);
	}
	else
	{
		// DO WE NEED THIS???
		// This may seem redundent but while drawing the display is forced
		SetIsRoiDisplayed(_isGlobalFlickerRoiDisplayedOld);
	}

	_needToTellGuiThatFrameHasChanged = true;
	getSystemAPI()->refreshFrameCached();
	return result;
}

void ColorBreathingTool::SetIsRoiLocked(const bool newLock)
{
	switch (getSubToolType())
	{
	case SubToolType::GlobalFlicker:
		setIsGlobalFlickerRoiLocked(newLock);
		break;

	case SubToolType::ZonalFlicker:
		setIsZonalFlickerRoiLocked(newLock);
		break;

	default:
		MTIassert(false);
	}
}

bool ColorBreathingTool::GetIsRoiLocked() const
{
	switch (getSubToolType())
	{
	case SubToolType::GlobalFlicker:
		return getIsGlobalFlickerRoiLocked();

	case SubToolType::ZonalFlicker:
		return getIsZonalFlickerRoiLocked();

	default:
		MTIassert(false);
	}

	return false;
}

int ColorBreathingTool::onTopPanelMouseDown(int x, int y)
{
	auto bitmap = getSystemAPI()->GetBitmapRefForTopPanelDrawing();
	auto bitmapData = (unsigned char *)bitmap->getBitmapFrameBuffer();
	auto width = bitmap->getWidth();

	auto model = getGlobalFlickerModel();
	auto displayFrames = model->GetGraphDisplayOut() - model->GetGraphDisplayIn();

	auto newFrame = (double)x / (width - 1.0) * displayFrames + model->GetGraphDisplayIn();
	getSystemAPI()->goToFrameSynchronous(round(newFrame));

	return 0;
}

int ColorBreathingTool::onTopPanelMouseDrag(int x, int y)
{
	onTopPanelMouseDown(x, y);
	return 0;
}

#define ERR_PLUGIN_MALLOC -101
#define ERR_PLUGIN_UNHANDLED_EXCEPTION -102

int ColorBreathingTool::ProcessSingleFrame(EToolSetupType toolSetupType)
{
	return 0;
	int retVal;
	CAutoErrorReporter autoErr("ColorBreathingTool::ColorBreathingTool", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

	if (getSystemAPI()->GetActiveToolSetupStatus() == AT_STATUS_RUNNING)
	{
		// Something is running, so can't do Training processing right now
		return -1;
	}

	const CImageFormat *imageFormat = getSystemAPI()->getVideoClipImageFormat();
	if (imageFormat == 0)
	{
		return -1; // No image format, probably clip isn't loaded
	}

	// Check if we have a tool setup yet
	if (singleFrameToolSetupHandle < 0)
	{
		// Create the tool setup
		CToolIOConfig ioConfig(1, 1);
		ioConfig.processingThreadCount = 1;
		singleFrameToolSetupHandle = getSystemAPI()->MakeSimpleToolSetup(COLORBREATHING_TOOL_NAME,
			 TOOL_SETUP_TYPE_SINGLE_FRAME, &ioConfig);
		if (singleFrameToolSetupHandle < 0)
		{
			autoErr.errorCode = -1;
			autoErr.msg << "ImageFilter internal error: CNavSystemInterface::MakeSimpleToolSetup failed";
			return -1;
		}
	}

	// Set the active tool setup (this is different than the active tool)
	retVal = getSystemAPI()->SetActiveToolSetup(singleFrameToolSetupHandle);
	if (retVal != 0)
	{
		autoErr.errorCode = retVal;
		autoErr.msg << "ImageFilter internal error: SetActiveToolSetup(" << singleFrameToolSetupHandle <<
			 ") failed with return code " << retVal;
		return retVal;
	}

	// Set processing frame range to the current frame index
	CToolFrameRange frameRange(1, 1);
	frameRange.inFrameRange[0].randomAccess = true;
	frameRange.inFrameRange[0].inFrameIndex = getSystemAPI()->getLastFrameIndex();
	frameRange.inFrameRange[0].outFrameIndex = frameRange.inFrameRange[0].inFrameIndex + 1;
	retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
	if (retVal != 0)
	{
		autoErr.errorCode = retVal;
		autoErr.msg << "ImageFilter internal error: SetToolFrameRange" << " failed with return code " << retVal;
		return retVal;
	}

	// Set provisional highlighting flag
	getSystemAPI()->SetProvisionalHighlight(highlightFlag);
	getSystemAPI()->SetProvisionalRender(false);

	// Set ImageFilter tool's parameters
	auto toolParams = new ColorBreathingToolParameters();
	toolParams->SetRegionOfInterest(getSystemAPI()->GetMaskRoi());
	toolParams->Tool = this;
	toolParams->Model = getGlobalFlickerModel();

	retVal = getSystemAPI()->SetToolParameters(toolParams);
	if (retVal != 0)
	{
		autoErr.errorCode = retVal;
		autoErr.msg << "ImageFilter internal error: SetToolParameters" << " failed with return code " << retVal;
		return retVal;
	}

	// Start the image processing a-runnin'
	getSystemAPI()->RunActiveToolSetup();

	return 0;
}

int ColorBreathingTool::TryToResolveProvisional()
{
	getSystemAPI()->WaitForToolProcessing();
	getSystemAPI()->AcceptGOV();
	getSystemAPI()->ClearProvisional(false);

	return 0;
}

bool ColorBreathingTool::CanWeAddReferenceFrame()
{
	auto model = getGlobalFlickerModel();

	auto currentIndex = getSystemAPI()->getLastFrameIndex();
	auto pair = model->BreakFrameData.GetBreakPair(currentIndex);
	if (model->RGBFrameData.AreAnyRenderFramesValid(pair.first, pair.second))
	{
		return false;
	}

	return model->RGBFrameData.IsGraphValid(pair.first, pair.second);
}

void ColorBreathingTool::AddReferenceFrameAtCurrentIndex()
{
	CAutoErrorReporter autoErr("Add Reference Frame", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

	auto model = getGlobalFlickerModel();

	auto currentIndex = getSystemAPI()->getLastFrameIndex();
	auto pair = model->BreakFrameData.GetBreakPair(currentIndex);

	if (!model->RGBFrameData.IsGraphValid(pair.first, pair.second))
	{
		autoErr.errorCode = -1;
		autoErr.msg << "Cannot add a reference frame unless graph is valid";
		return;
	}

	auto presets = model->GetPresets();
	auto r = presets->GetCurrentPreset();
	r.FrameIndex = currentIndex;
	if (!model->AddKeyframe(r))
	{
		autoErr.errorCode = -1;
		autoErr.msg << "Failed to add reference frame because frame is rendered";
		return;
	}

	SetInPreviewMode(true);
	_needToTellGuiThatFrameHasChanged = true;
}

void ColorBreathingTool::DeleteReferenceFrameAtCurrentIndex()
{
	auto model = getGlobalFlickerModel();

	auto currentIndex = getSystemAPI()->getLastFrameIndex();
	auto pair = model->BreakFrameData.GetBreakPair(currentIndex);
	if (model->RGBFrameData.AreAnyRenderFramesValid(pair.first, pair.second))
	{
		CAutoErrorReporter autoErr("Delete Reference Frame", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
		autoErr.errorCode = -1;
		autoErr.traceAction = AUTO_ERROR_NO_TRACE;
		autoErr.msg << "Cannot delete a reference frame in a shot with rendered frames";
		return;
	}

	auto referenceKeyframeData = model->BreakFrameData.FindReferenceframeDataAtFrameNumber(currentIndex);

	if (referenceKeyframeData == nullptr)
	{
		return;
	}

	// This does NOT delete this from the gui list box, we need to refresh all to do it
	model->BreakFrameData.Delete(*referenceKeyframeData);
	_needToTellGuiThatFrameHasChanged = true;
}

void ColorBreathingTool::DeleteReferenceFrameInCurrentBreak()
{
	auto model = getGlobalFlickerModel();

	auto currentIndex = getSystemAPI()->getLastFrameIndex();
	auto pair = model->BreakFrameData.GetBreakPair(currentIndex);
	if (model->RGBFrameData.AreAnyRenderFramesValid(pair.first, pair.second))
	{
		CAutoErrorReporter autoErr("Delete Reference Frame", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
		autoErr.errorCode = -1;
		autoErr.traceAction = AUTO_ERROR_NO_TRACE;
		autoErr.msg << "Cannot delete a reference frame in a shot with rendered frames";
		return;
	}

	for (auto r : model->BreakFrameData.GetReferenceKeyFrames(currentIndex))
	{
		model->BreakFrameData.Delete(*r);
		_needToTellGuiThatFrameHasChanged = true;
	}

	model->SetGraphDirty();
	onTopPanelRedraw();
}

void ColorBreathingTool::DeleteReferenceFrame(const ReferenceKeyframeData &referenceKeyframeData)
{
	auto model = getGlobalFlickerModel();
	model->BreakFrameData.Delete(referenceKeyframeData);
	_needToTellGuiThatFrameHasChanged = true;

	model->SetGraphDirty();
	onTopPanelRedraw();
}

// void ColorBreathingTool::DeleteReferenceFrameInCurrentBreak()
// {
// auto model = getGlobalFlickerModel();
//
// auto in = getSystemAPI()->getMarkIn();
// auto out = getSystemAPI()->getMarkOut();
//
// if ((in < 0) || (out < 0))
// {
////      auto currentIndex = getSystemAPI()->getLastFrameIndex();
////      auto pair = model->BreakFrameData.GetBreakPair(currentIndex);
////      in = pair.first;
////      out = pair.second;
// CAutoErrorReporter autoErr("Delete Reference Frame", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
// autoErr.errorCode = -1;
// autoErr.traceAction = AUTO_ERROR_NO_TRACE;
// autoErr.msg << "In/Out marks must be set to delete all keyframes";
// return;
// }
//
// if (_MTIConfirmationDialog(Handle,
// "All keyframes between marks on non-rendered shots will be deleted, Yes to delete, No to cancel")
// != MTI_DLG_OK)
// {
// return;
// }
//
// auto breaks = model->BreakFrameData.GetAllBreakPairs();
// for (auto b : breaks)
// {
// if (!model->RGBFrameData.AreAnyRenderFramesValid(b.first, b.second))
// {
// auto referenceFrames = model->BreakFrameData.GetReferenceKeyFrames(b.first);
// for (auto r : referenceFrames)
// {
// if ((r->FrameIndex >= in) && (r->FrameIndex < out))
// {
// model->BreakFrameData.Delete(*r);
// _needToTellGuiThatFrameHasChanged = true;
// }
// }
// }
// }
//
// model->SetGraphDirty();
// onTopPanelRedraw();
// }

void ColorBreathingTool::GotoNextReferenceFrame()
{
	auto model = getGlobalFlickerModel();
	auto frameIndex = getSystemAPI()->getLastFrameIndex();
	for (auto r : model->BreakFrameData.GetAllReferenceKeyFrames())
	{
		if (r->FrameIndex > frameIndex)
		{
			getSystemAPI()->goToFrameSynchronous(r->FrameIndex);
			return;
		}
	}
}

void ColorBreathingTool::GotoPreviousReferenceFrame()
{
	auto model = getGlobalFlickerModel();
	auto frameIndex = getSystemAPI()->getLastFrameIndex();

	// We never use this
	auto oldFrameIndex = -1;
	for (auto r : model->BreakFrameData.GetAllReferenceKeyFrames())
	{
		if (r->FrameIndex >= frameIndex)
		{
			break;
		}

		oldFrameIndex = r->FrameIndex;
	}

	if (oldFrameIndex <= -1)
	{
		// This means we are at the beginning
		return;
	}

	getSystemAPI()->goToFrameSynchronous(oldFrameIndex);
}

void ColorBreathingTool::AcceptFix()
{
	getSystemAPI()->AcceptGOV(); // Make sure it's not a GOV pending!

	if (getSystemAPI()->IsProvisionalPending())
	{
		// It's ours!
		getSystemAPI()->AcceptProvisional(true); // true = show the frame

		GetToolProgressMonitor()->SetStatusMessage("Fixes accepted");
	}
}

int ColorBreathingTool::onPreviewHackDraw(int frameIndex, unsigned short *frameBits, int frameBitsSizeInBytes)
{
	switch (getSubToolType())
	{
	case SubToolType::GlobalFlicker:
		return onPreviewHackDrawCB(frameIndex, frameBits);
		break;

	case SubToolType::ZonalFlicker:
		return onPreviewHackDrawZonal(frameIndex, frameBits);
		break;

	default:
		MTIassert(false);
		break;
	}

	return 0;
}

bool ColorBreathingTool::loadZonalDataIntoModelIfNecessary(int frameIndex)
{
	auto &segmentsDeltas = getZonalFlickerModel()->getSegmentsDeltas();
	if (segmentsDeltas.isDataLoaded(frameIndex))
	{
		// BAD Side effect is to load data into model
		findAnchorDeltaValuesIfNecessary(frameIndex);
		return true;
	}

	auto result = getClipPreprocessData().readPreprocessorData(frameIndex, segmentsDeltas);

	// BAD Side effect is to load data into model
	findAnchorDeltaValuesIfNecessary(frameIndex);
	return result;
}

int ColorBreathingTool::onPreviewHackDrawZonal(int frameIndex, unsigned short *frameBits)
{
	auto model = getZonalFlickerModel();
	if (model->getPreviewMode())
	{
		const CImageFormat *imageFormat = getSystemAPI()->getVideoClipImageFormat();
		if (imageFormat == 0)
		{
			// No image format, probably clip isn't loade
			return TOOL_RETURN_CODE_EVENT_CONSUMED;
		}

		auto height = (int)imageFormat->getLinesPerFrame();
		auto width = (int)imageFormat->getPixelsPerLine();
		model->setImageSize({.width = width, .height = height});

		MTI_UINT16 caMax[3];
		imageFormat->getComponentValueMax(caMax);

		loadZonalDataIntoModelIfNecessary(frameIndex);
		try
		{
			model->PreviewFrame16Bit(frameIndex, width, height, caMax[0], frameBits, getMask(frameIndex));
		}
		catch (...)
		{
			DBCOMMENT("model->PreviewFrame16Bit EXCEPTION");
		}
	}

	return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

int ColorBreathingTool::onPreviewHackDrawCB(int frameIndex, unsigned short *frameBits)
{
	auto model = getGlobalFlickerModel();
	if (model->IsPreviewMode())
	{
		const CImageFormat *imageFormat = getSystemAPI()->getVideoClipImageFormat();
		if (imageFormat == 0)
		{
			// No image format, probably clip isn't loade
			return TOOL_RETURN_CODE_EVENT_CONSUMED;
		}

		auto width = imageFormat->getPixelsPerLine();
		auto height = imageFormat->getLinesPerFrame();
		MTI_UINT16 caMax[3];
		imageFormat->getComponentValueMax(caMax);

		model->PreviewColorBreathingFrame16Bit(frameIndex, width, height, caMax[0], frameBits);
	}

	return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

// int ColorBreathingTool::onPreviewDisplayBufferDraw(int frameIndex, int width, int height, unsigned int *pixels)
// {
// auto model = getGlobalFlickerModel();
//
// if (model->IsPreviewMode())
// {
// model->PreviewColorBreathingFrame(frameIndex, width, height, (unsigned char *)pixels);
// }
//
// return TOOL_RETURN_CODE_EVENT_CONSUMED;
// }

bool ColorBreathingTool::GetInPreviewMode() const
{
	switch (getSubToolType())
	{
	case SubToolType::GlobalFlicker:
		return GetInGlobalFlickerPreviewMode();

	case SubToolType::ZonalFlicker:
		return GetInZonalFlickerPreviewMode();

	default:
		MTIassert(false);
	}

	return false;
}

bool ColorBreathingTool::GetInGlobalFlickerPreviewMode() const {return _inGlobalFlickerPreviewMode;}

bool ColorBreathingTool::GetInZonalFlickerPreviewMode() const {return _inZonalFlickerPreviewMode;}

void ColorBreathingTool::SetInPreviewMode(const bool mode)
{
	switch (getSubToolType())
	{
	case SubToolType::GlobalFlicker:
		SetInGlobalFlickerPreviewMode(mode);
		break;

	case SubToolType::ZonalFlicker:
		SetInZonalFlickerPreviewMode(mode);
		break;

	default:
		MTIassert(false);
	}
}

void ColorBreathingTool::SetInZonalFlickerPreviewMode(const bool mode)
{
	if (GetInZonalFlickerPreviewMode() != mode)
	{
		if (mode)
		{
			resmoothZonalFlickerDataIfNecessary();
			getSystemAPI()->ClearProvisional(false);
			getSystemAPI()->enterPreviewHackMode();
			GetToolProgressMonitor()->SetStatusMessage("Previewing fixed frames");
		}
		else
		{
			getSystemAPI()->exitPreviewHackMode();
			GetToolProgressMonitor()->SetStatusMessage("Idle");
		}

		_inZonalFlickerPreviewMode = mode;
		getZonalFlickerModel()->setPreviewMode(_inZonalFlickerPreviewMode);

		onTopPanelRedraw();

		if (!getSystemAPI()->IsPlayerReallyPlaying())
		{
			getSystemAPI()->refreshFrameCached();
		}
	}
}

void ColorBreathingTool::SetInGlobalFlickerPreviewMode(const bool mode)
{
	if (_inGlobalFlickerPreviewMode != mode)
	{
		if (mode)
		{
			getSystemAPI()->enterPreviewHackMode();
			GetToolProgressMonitor()->SetStatusMessage("Previewing fixed frames");
		}
		else
		{
			getSystemAPI()->exitPreviewHackMode();
			GetToolProgressMonitor()->SetStatusMessage("Idle");
		}

		_inGlobalFlickerPreviewMode = mode;

		getGlobalFlickerModel()->SetPreviewMode(_inGlobalFlickerPreviewMode);

		onTopPanelRedraw();

		if (!getSystemAPI()->IsPlayerReallyPlaying())
		{
			getSystemAPI()->refreshFrameCached();
		}
	}
}

void ColorBreathingTool::UpdateRenderedFramesThatWereDiscarded(int frameIn, int frameOut)
{
	if ((frameIn < 0) || (frameOut < 0))
	{
		return;
	}

	if (IsActive() == false)
	{
		return;
	}

	auto frameList = getSystemAPI()->getVideoFrameList();
	if (frameList == nullptr)
	{
		return;
	}

	vector<int>listOfDirtyFrameIndexes;
	CBinManager binManager;
	int retVal = binManager.FindDirtyFrames(frameList, frameIn, frameOut - 1, listOfDirtyFrameIndexes, nullptr, nullptr);
	if (retVal)
	{
		// Because the clip at this point is not fully formed, just return
		return;
	}

	// We need the clean frames between frameIn and frame out
	vector<int>listOfCleanFrameIndexes;
	for (auto i = frameIn; i < frameOut; i++)
	{
		if (find(listOfDirtyFrameIndexes.begin(), listOfDirtyFrameIndexes.end(), i) == listOfDirtyFrameIndexes.end())
		{
			listOfCleanFrameIndexes.push_back(i);
		}
	}

	getClipPreprocessData().updateZonalRenderFlag(listOfCleanFrameIndexes, listOfDirtyFrameIndexes);
	getGlobalFlickerModel()->RGBFrameData.UpdateRenderFlag(listOfCleanFrameIndexes);
}

void ColorBreathingTool::DirtyMediaHasChanged(void *sender) {_dirtyMediaHasChanged = true;}

int ColorBreathingTool::onFrameWasDiscardedOrCommitted(int frameIndex, bool discardedFlag,
	 const std::pair<int, int> &frameRange)
{
	auto model = getGlobalFlickerModel();
	// An out of bounds index, we are done
	if (frameIndex < 0)
	{
		UpdateRenderedFramesThatWereDiscarded(frameRange.first, frameRange.second);
		model->AddEndKeyframeToUnrenderedBreakArea(frameRange.first);
		model->RGBFrameData.Save();
		model->RGBFrameData.SetDataDirty(true);
		model->SetGraphDirty();
		onTopPanelRedraw();
		return TOOL_RETURN_CODE_NO_ACTION;
	}

	auto &rgbFrameDatum = getGlobalFlickerModel()->RGBFrameData[frameIndex];
	if (discardedFlag)
	{
      if (rgbFrameDatum.WasRendered)
      {
      	rgbFrameDatum.SetRGBInvalid();
			model->RGBFrameData.Save();
			model->RGBFrameData.SetDataDirty(true);
			model->SetGraphDirty();
			onTopPanelRedraw();
      }
		else if (rgbFrameDatum.Rendered)
		{
			// This code is wrong, it should set the model that then
			// sets the dirty flag but I don't have time to fix it
			rgbFrameDatum.Rendered = false;
			model->RGBFrameData.Save();
			model->RGBFrameData.SetDataDirty(true);
			model->SetGraphDirty();
			onTopPanelRedraw();
		}
	}
	else
	{
		if (rgbFrameDatum.Rendered || rgbFrameDatum.Valid)
		{
			rgbFrameDatum.Valid = false;
			rgbFrameDatum.Rendered = false;
			model->RGBFrameData.Save();
			model->RGBFrameData.SetDataDirty(true);
			model->SetGraphDirty();
			onTopPanelRedraw();
		}
	}

	return TOOL_RETURN_CODE_NO_ACTION;
}

string ColorBreathingTool::GetRgbFileNameFromClip()
{
	// Create the file name
   try
	{
   	auto cid = new ClipIdentifier(getSystemAPI()->getClipFilename());
      if (cid == nullptr)
      {
      	return "";
      }

		return AddDirSeparator(cid->GetClipPath()) + "RGBFrameData.data";
   }
   catch (...)
   {
   	return "";
   }
}

void ColorBreathingTool::InitializeDataFromClip()
{
	CAutoErrorReporter autoErr("ColorBreathingTool::onNewClip", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

	auto model = getGlobalFlickerModel();

	// Just in the case that we don't have a loaded clip
	model->RGBFrameData.Invalidate();
	// DestroyAllToolSetups(true);
	model->BreakFrameData.SetFileName("");
	model->BreakFrameData.ClearAll();

	auto clipGUID = getSystemAPI()->getClip()->getClipGUID();

	// Read reference frames
	auto cid = new ClipIdentifier(getSystemAPI()->getClipFilename());
	_referenceKeyFrameFileName = AddDirSeparator(cid->GetClipPath()) + "ReferenceKeyFrames.ini";
	_rgbDataFileName = AddDirSeparator(cid->GetClipPath()) + "RGBFrameData.data";

	// We have switched clips, assume we have invalid data
	auto needToRebuildRgbData = true;

	if (DoesFileExist(_rgbDataFileName))
	{
		// Load the data
		auto err = model->RGBFrameData.Load(_rgbDataFileName, clipGUID);
		if (err != 0)
		{
			autoErr.traceAction = AUTO_ERROR_NO_TRACE;
			autoErr.errorCode = err;
			autoErr.msg << model->RGBFrameData.LoadMessage;
			needToRebuildRgbData = true;
		}
		else
		{
			needToRebuildRgbData = false;
			model->LoadRGBDataInToTimeline();
		}
	}
	else
	{
		model->RGBFrameData.SetLoadedFilename(_rgbDataFileName, clipGUID);
	}

	auto videoFrameList = getSystemAPI()->getVideoFrameList();
	auto totalFrames = videoFrameList->getOutFrameIndex();

	auto imageFormat = getSystemAPI()->getVideoClipImageFormat();
	int nPicCol = imageFormat->getPixelsPerLine();
	int nPicRow = imageFormat->getLinesPerFrame();
	model->SetTotalFrames(totalFrames);
	RECT FullRect
	{
		0, 0, nPicCol, nPicRow
	};

	// Kludge put in
	if ((_globalFlickerRoi.right - _globalFlickerRoi.left) > 0)
	{
		FullRect = _globalFlickerRoi;
	}

	if (needToRebuildRgbData)
	{
		// Working on going to indexes
		model->RGBFrameData.SetROI(0, totalFrames, FullRect);
	}

	_lastROI = FullRect;

	model->RGBFrameData.SetMarkRange(getSystemAPI()->getMarkIn(), getSystemAPI()->getMarkOut());

	// Read the breaks
	UpdateBreaksFromClip();
	model->BreakFrameData.ReadReferenceFrames(_referenceKeyFrameFileName);

	// Because reference frames are inherited from the parent clip,
	// and rendered frames of parent delete graph in version
	// We must fix it up.
	model->DeleteReferenceFramesWhereThereIsNoGraph();
	/// TO DO: FIX THIS!!!   SetUseColorBreathingRoi(true);

	model->IsDataInitialized = true;
	model->RGBFrameData.SetDataDirty(true);
	model->SetGraphDirty();
	_needToTellGuiThatGuiReferenceFramesHaveChanged = true;

	// This allows the tool to notify the gui
	ClipChange.Notify();

	onTimelineVisibleFrameRangeChange();
}

void ColorBreathingTool::RenderedConsistencyCheck()
{
	if (getSystemAPI()->getClip() == nullptr)
	{
		return;
	}

	// First thing to do is see if RGB data belongs to the clip
	// This is because there can be a delayed clip change.
	if (guid_compare(getGlobalFlickerModel()->RGBFrameData.GetGUID(), getSystemAPI()->getClip()->getClipGUID()) != 0)
	{
		return;
	}

	auto in = getSystemAPI()->getMarkIn();
	auto out = getSystemAPI()->getMarkOut();

	UpdateRenderedFramesThatWereDiscarded(in, out);
	getGlobalFlickerModel()->RGBFrameData.Save();

}

int ColorBreathingTool::onCapturePDLEntry(CPDLElement &pdlEntryToolParams)
{
	switch (getSubToolType())
	{
	case SubToolType::GlobalFlicker:
		return onCaptureGlobalFlickerPDLEntry(pdlEntryToolParams);

	case SubToolType::ZonalFlicker:
		return onCaptureZonalFlickerPDLEntry(pdlEntryToolParams);

	default:
		MTIassert(false);
	}

	return -1;
}

int ColorBreathingTool::onCaptureZonalFlickerPDLEntry(CPDLElement &pdlEntryToolParams)
{
	if (IsDisabled())
	{
		return 0;
	}

	// Write Reference Frames
	getClipPreprocessData().WritePDLEntry(pdlEntryToolParams);
	return 0;
}

int ColorBreathingTool::onCaptureGlobalFlickerPDLEntry(CPDLElement &pdlEntryToolParams)
{
	if (IsDisabled())
	{
		return 0;
	}

	// Write Reference Frames
	auto model = getGlobalFlickerModel();
	model->WritePDLEntry(pdlEntryToolParams);
	return 0;
}

int ColorBreathingTool::onGoToPDLEntry(CPDLEntry &pdlEntry)
{
	CPDLEntry_Tool *pdlEntryTool = pdlEntry.toolList[0];
	CPDLElement *toolAttribRoot = pdlEntryTool->parameters;

	// Default to support old PLS before the subtype was set
	string toolType = "Zonal";

	for (unsigned int i = 0; i < toolAttribRoot->GetChildElementCount(); ++i)
	{
		CPDLElement *subElement = toolAttribRoot->GetChildElement(i);
		string elementName = subElement->GetElementName();

		if (elementName == "ToolType")
		{
			toolType = subElement->GetAttribString("ToolSubType");
			break;
		}
	}

	if (toolType == "Zonal")
	{
		ColorBreathingForm->ColorBreathingPageControl->ActivePage = ColorBreathingForm->FlickerTabSheet;
		////		Application->ProcessMessages();
	}
	else
	{
		ColorBreathingForm->ColorBreathingPageControl->ActivePage = ColorBreathingForm->ColorBreathingTabSheet;
		////		Application->ProcessMessages();
	}

	ColorBreathingForm->ColorBreathingPageControlChange(nullptr);
	ColorBreathingForm->fullGuiUpdate();
	////   Application->ProcessMessages();

	switch (getSubToolType())
	{
	case SubToolType::GlobalFlicker:
		return onGoToGlobalFlickerPDLEntry(pdlEntry);

	case SubToolType::ZonalFlicker:
		return onGoToZonalFlickerPDLEntry(pdlEntry);

	default:
		MTIassert(false);
	}

	return -1;
}

int ColorBreathingTool::onGoToZonalFlickerPDLEntry(CPDLEntry &pdlEntry)
{
	try
	{
		if (IsDisabled())
		{
			return 0;
		}

		// Set the Mask, this was already done before we we4e called, but
		// it gets messed up when we switch subtools.
		getSystemAPI()->SetMaskFromPdlEntry(pdlEntry);

		auto retVal = getClipPreprocessData().ReadPDLEntry(pdlEntry);
		if (retVal != 0)
		{
			return retVal;
		}

	}
	catch (const std::exception &ex)
	{
		// Show errors here
		TRACE_0(errout << string(ex.what()));
	}
	catch (...)
	{
		TRACE_0(errout << "unknown error");
	}

	ExternalGuiRefresh(__LINE__);

	// Set the Mask, this was already done before we were called, but
	// it gets messed before it gets here for various reasons!!
	getSystemAPI()->SetMaskFromPdlEntry(pdlEntry);

	// See if we have to do a render or a preprocess/render
	updateZonalFlickerParameters();
	auto &result = getZonalFlickerParameters();

	auto model = getZonalFlickerModel();
	getClipPreprocessData().writeControlFile(*model);
	getClipPreprocessData().computeIsDefaultPreprocessingDone();
	if (getClipPreprocessData().isDeltaValidOverMarks() == false)
	{
		SetFlickerPreProcessing();
		setControlState(EMultiControlState::ACQURING);
		_nextStateAfterAcquire = EMultiControlState::NEED_TO_RUN_RENDERER;
	}
	else
	{
		SetFlickerRender();
		setControlState(EMultiControlState::RENDERING);
	}

	return 0;
}

int ColorBreathingTool::onGoToGlobalFlickerPDLEntry(CPDLEntry &pdlEntry)
{
	if (IsDisabled())
	{
		return 0;
	}

	auto model = getGlobalFlickerModel();
	auto retVal = model->ReadPDLEntry(pdlEntry);
	if (retVal != 0)
	{
		return retVal;
	}

	// This will force a render
	_toolProcessorTypeToMake = PROC_TYPE_RENDER_PROCESS;
	return 0;
}

//// **** NEED TO FILL THESE IN *****
bool ColorBreathingTool::CheckIfFlickerPreprocessingIsLegal()
{
	CAutoErrorReporter autoErr("Flicker Preprocess", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

	return true;
}

bool ColorBreathingTool::CheckIfFlickerRenderIsLegal()
{
	CAutoErrorReporter autoErr("Flicker Render", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

	auto valid = getClipPreprocessData().isPreprocessingValidOverMarks();
	if (valid == false)
	{
		Beep();
		return false;
	}

	return true;
}

void ColorBreathingTool::SetSubToolType(SubToolType subToolType)
{
	if (getSubToolType() == subToolType)
	{
		return;
	}

	_subToolType = subToolType;

	switch (getSubToolType())
	{
	case SubToolType::GlobalFlicker:
		hideDisplayOverlay();
		break;

	case SubToolType::ZonalFlicker:
		///         _boundingBox = IppArrayConvert::convertToRect(getZonalFlickerModel()->getRoi());
		makeTileBoxes();
		break;

	default:
		MTIassert(false);
		break;
	}

	// We might want to do everything here
	onTopPanelRedraw();
}

void ColorBreathingTool::setZonalFlickerShowBoxes(bool value)
{
	if (value == getZonalFlickerShowBoxes())
	{
		return;
	}

	_zonalFlickerShowBoxes = value;
	makeTileBoxes();
}


void ColorBreathingTool::addSimpleBox(const IppiRect &rect, TColor color, int id)
{
	OverlayDisplayItem overlayItem
	{
		.itemType = OverlayDisplayItem::ACOIT_rect_inclusive, .id = id, .rect = IppArrayConvert::convertToRect(rect), .R =
			 GetRValue(color), .G = GetGValue(color), .B = GetBValue(color)
	};

	_overlayList.push_back(overlayItem);
}

void ColorBreathingTool::updateZonalFlickerParameters()
{
	auto &result = getZonalFlickerParameters();

	const CImageFormat* imgFmt = getSystemAPI()->getVideoClipImageFormat();
	if (imgFmt != nullptr)
	{
		result.setImageSize({.height = (int)imgFmt->getLinesPerFrame(), .width = (int)imgFmt->getPixelsPerLine()});
	}

	MTI_UINT16 componentValues[3];
	imgFmt->getComponentValueMax(componentValues);
	result.setMaxValue(componentValues[0]);

	result.setInFrame(getSystemAPI()->getMarkIn());
	result.setOutFrame(getSystemAPI()->getMarkOut());
}

void ColorBreathingTool::makeTileBoxes()
{
	// No tile boxes if we are not in zonal flicker
	if (getSubToolType() != SubToolType::ZonalFlicker)
	{
		_overlayList.clear();
		return;
	}

	// KLUDGE, find out why this is necessary
	if (getZonalFlickerModel()->isAnalysisRoiValid() == false)
	{
		return;
	}

	updateZonalFlickerParameters();
	auto boxes = getZonalFlickerModel()->makeTileBoxes();

	_overlayList.clear();

	// Do we want to display?
	if (getZonalFlickerShowBoxes())
	{
		auto i = 0;
		for (auto&box : boxes)
		{
			addSimpleBox(box, clYellow);
			i++;
		}
	}

	showOverlays();
}

void ColorBreathingTool::hideDisplayOverlay(void)
{
	// clear the boxes
	OverlayDisplayList overlayList;
	setOverlayDisplayList(overlayList);
}

void ColorBreathingTool::showOverlays() {setOverlayDisplayList(_overlayList);}

void ColorBreathingTool::setOverlayDisplayList(const OverlayDisplayList &newList)
{
	_overlayList = newList;
	getSystemAPI()->SetAutoCleanOverlayList(newList);
	getSystemAPI()->refreshFrameCached();
}

string ColorBreathingTool::getDataFilePath()
{
	auto clip = getSystemAPI()->getClip();
	if (clip == nullptr)
	{
		return "";
	}

	// and the => frame list
	auto frameList = clip->getVideoFrameList(VIDEO_SELECT_NORMAL, FRAMING_SELECT_VIDEO);
	if (frameList == nullptr)
	{
		return "";
	}

	CVideoFrame *frame = frameList->getFrame(0);
	if (frame == NULL)
	{
		// Bogus index!
		return "";
	}

	string imageFilePath = frameList->getImageFilePath(frame);
	return GetFilePath(imageFilePath);
}

////////////////////////  BLUE BOX CODE

// void ColorBreathingTool::setUseBoundingBox(const bool newDisp)
// {
// if (getUseBoundingBox() == newDisp)
// {
// ///       return;
// };
//
// _useBoundingBox = newDisp;
// setZonalFlickerUseRoi(_useBoundingBox);
// makeTileBoxes();
// getSystemAPI()->refreshFrameCached();
// }

// void ColorBreathingTool::setShowBoundingBox(const bool newDisp)
// {
// if (getShowBoundingBox() == newDisp)
// {
// return;
// }
//
// _showBoundingBox = newDisp;
// getSystemAPI()->refreshFrameCached();
// }

// void ColorBreathingTool::showBoundingBoxIfNeeded(void)
// {
// if (getShowBoundingBox())
// {
// getSystemAPI()->setGraphicsColor(SOLID_CYAN);
// getSystemAPI()->drawRectangleFrame(&_boundingBox);
// }
// }

void ColorBreathingTool::readClipBoundingBox(void)
{
	// Read in the bounding box in case it got changed in another tool
	auto curClip = getSystemAPI()->getClip();
	if (curClip != nullptr)
		readParametersFromDesktopIni(curClip);
	else
		setZonalFlickerRoi({-1, -1, -1, -1});
}

void ColorBreathingTool::writeClipBoundingBox(void)
{
	// Read in the bounding box in case it got changed in another tool
	auto curClip = getSystemAPI()->getClip();
	writeParametersToDesktopIni(curClip);
}

// ---------------------------------------------------------------------------

void ColorBreathingTool::readParametersFromDesktopIni(ClipSharedPtr &clip)
{
	CBinManager binManager;
	CIniFile *desktopIni = binManager.openClipDesktop(clip);

	auto bbox = getFullImageRoi();
	setZonalFlickerRoi(bbox);

	getZonalFlickerModel()->setImageSize({.height = bbox.bottom - bbox.top, .width = bbox.right - bbox.left});

	if (desktopIni != nullptr)
	{
		auto underLying = desktopIni->ReadAssociation(ZonalProcessAssociation, ZONAL_FLICKER_SECTION, "ProcessType",
			 (int)ZonalProcessType::Unknown);

		auto processType = static_cast<ZonalProcessType>(underLying);
		setZonalProcessType(processType);

		bbox.left = desktopIni->ReadInteger(ZONAL_FLICKER_SECTION, "Left", bbox.left);
		bbox.top = desktopIni->ReadInteger(ZONAL_FLICKER_SECTION, "Top", bbox.top);
		bbox.right = desktopIni->ReadInteger(ZONAL_FLICKER_SECTION, "Right", bbox.right);
		bbox.bottom = desktopIni->ReadInteger(ZONAL_FLICKER_SECTION, "Bottom", bbox.bottom);

		setIsZonalFlickerRoiLocked(desktopIni->ReadBool(ZONAL_FLICKER_SECTION, "LockRoi", getIsZonalFlickerRoiLocked()));
		setIsZonalFlickerRoiDisplayed(desktopIni->ReadBool(ZONAL_FLICKER_SECTION, "DisplayRoi",
			 getIsZonalFlickerRoiDisplayed()));
		setZonalFlickerUseProcessingRegion(desktopIni->ReadBool(ZONAL_FLICKER_SECTION, "UseProcessingRegion",
			 getZonalFlickerUseProcessingRegion()));
		setZonalFlickerShowBoxes(desktopIni->ReadBool(ZONAL_FLICKER_SECTION, "ShowBoxes", getZonalFlickerShowBoxes()));

		setIsGlobalFlickerRoiLocked(desktopIni->ReadBool(GLOBAL_FLICKER_SECTION, "LockRoi",
			 getIsGlobalFlickerRoiLocked()));
		setIsGlobalFlickerRoiDisplayed(desktopIni->ReadBool(GLOBAL_FLICKER_SECTION, "DisplayRoi",
			 getIsGlobalFlickerRoiDisplayed()));
		setGlobalFlickerUseProcessingRegion(desktopIni->ReadBool(GLOBAL_FLICKER_SECTION, "UseProcessingRegion",
			 getGlobalFlickerUseProcessingRegion()));

		auto hboxes = desktopIni->ReadInteger(ZONAL_FLICKER_SECTION, "BoxesInRow",
			 getZonalFlickerParameters().getHorizontalBlocks());
		auto smoothingSigma = desktopIni->ReadInteger(ZONAL_FLICKER_SECTION, "Smoothing",
			 getZonalFlickerParameters().getSmoothingSigma());
		getZonalFlickerParameters().setHorizontalBlocks(hboxes);
		getZonalFlickerParameters().setSmoothingSigma(smoothingSigma);
      getZonalFlickerParameters().setSmoothingType(0);

		int headAnchorOffset = desktopIni->ReadInteger(ZONAL_FLICKER_SECTION, "HeadAnchorOffset",
			 getAnchorFrameOffsets().first);
		int tailAnchorOffset = desktopIni->ReadInteger(ZONAL_FLICKER_SECTION, "TailAnchorOffset",
			 getAnchorFrameOffsets().second);

		setAnchorFrameOffsets({headAnchorOffset, tailAnchorOffset});
		setRefFrame(desktopIni->ReadInteger(ZONAL_FLICKER_SECTION, "RefFrameClipIndex", getRefFrame()));

		// These are not used
		setZonalFlickerUseRoi(desktopIni->ReadBool(ZONAL_FLICKER_SECTION, "UseRoi", true));
		setZonalFlickerUseAnalysisRegion(desktopIni->ReadBool(ZONAL_FLICKER_SECTION, "UseAnalysisRegion", true));

		setZonalFlickerUseRoi(true);
		setZonalFlickerUseAnalysisRegion(true);

		DeleteIniFileDiscardChanges(desktopIni);
	}

	setZonalFlickerRoi(bbox);
	makeTileBoxes();
	ExternalGuiRefresh(__LINE__);
}

// ---------------------------------------------------------------------------

void ColorBreathingTool::writeParametersToDesktopIni(ClipSharedPtr &clip)
{
	if (clip == nullptr)
	{
		return;
	}

	CBinManager binManager;
	CIniFile *desktopIni = binManager.openClipDesktop(clip);
	if (desktopIni != nullptr)
	{
		// On inital load, this is called for some reason, unknown why
		if (isZonalFlickerRoiDefined())
		{
			RECT bbox = getZonalFlickerRoi();
			desktopIni->WriteInteger(ZONAL_FLICKER_SECTION, "Left", bbox.left);
			desktopIni->WriteInteger(ZONAL_FLICKER_SECTION, "Top", bbox.top);
			desktopIni->WriteInteger(ZONAL_FLICKER_SECTION, "Right", bbox.right);
			desktopIni->WriteInteger(ZONAL_FLICKER_SECTION, "Bottom", bbox.bottom);

			desktopIni->WriteAssociation(ZonalProcessAssociation, ZONAL_FLICKER_SECTION, "ProcessType",
				 (int)getZonalProcessType());

			desktopIni->WriteBool(ZONAL_FLICKER_SECTION, "LockRoi", getIsZonalFlickerRoiLocked());
			desktopIni->WriteBool(ZONAL_FLICKER_SECTION, "DisplayRoi", getIsZonalFlickerRoiDisplayed());
			desktopIni->WriteBool(ZONAL_FLICKER_SECTION, "UseProcessingRegion", getZonalFlickerUseProcessingRegion());
			desktopIni->WriteBool(ZONAL_FLICKER_SECTION, "ShowBoxes", getZonalFlickerShowBoxes());

			desktopIni->WriteBool(GLOBAL_FLICKER_SECTION, "LockRoi", getIsGlobalFlickerRoiLocked());
			desktopIni->WriteBool(GLOBAL_FLICKER_SECTION, "DisplayRoi", getIsGlobalFlickerRoiDisplayed());
			desktopIni->WriteBool(GLOBAL_FLICKER_SECTION, "UseProcessingRegion", getGlobalFlickerUseProcessingRegion());

			// These are not used but left in here in case we need to use them
			desktopIni->WriteBool(ZONAL_FLICKER_SECTION, "UseRoi", getZonalFlickerUseRoi());
			desktopIni->WriteBool(ZONAL_FLICKER_SECTION, "UseAnalysisRegion", getZonalFlickerUseAnalysisRegion());

			desktopIni->WriteInteger(ZONAL_FLICKER_SECTION, "BoxesInRow",
				 getZonalFlickerParameters().getHorizontalBlocks());
			desktopIni->WriteInteger(ZONAL_FLICKER_SECTION, "Smoothing", getZonalFlickerParameters().getSmoothingSigma());

			desktopIni->WriteInteger(ZONAL_FLICKER_SECTION, "HeadAnchorOffset", getAnchorFrameOffsets().first);
			desktopIni->WriteInteger(ZONAL_FLICKER_SECTION, "TailAnchorOffset", getAnchorFrameOffsets().second);
			desktopIni->WriteInteger(ZONAL_FLICKER_SECTION, "RefFrameClipIndex", getRefFrame());
		}

		DeleteIniFile(desktopIni);
	}
}

// --------------------------------------------------------------------------

string ColorBreathingTool::getImageFileName(int frameIndex)
{
	auto clip = getSystemAPI()->getClip();
	if (clip == nullptr)
	{
		return "";
	}

	// and the => frame list
	auto *videoFrameList = clip->getVideoFrameList(VIDEO_SELECT_NORMAL, FRAMING_SELECT_VIDEO);
	if (videoFrameList == nullptr)
	{
		return "";
	}

	CVideoFrame *frame = videoFrameList->getFrame(frameIndex);
	CVideoField *field = frame ? frame->getField(0) : nullptr;
	if (frame == nullptr || field == nullptr)
	{
		return nullptr;
	}

	return videoFrameList->getImageFilePath(frame);
}

string ParseReturnString(const string &line, int &frame)
{
	std::string delimiter = ": ";
	std::string token;
	auto pos = line.find(delimiter);
	if (pos == std::string::npos)
	{
		frame = -1;
		return "";
	}

	auto result = line.substr(0, pos);
	auto frameString = line.substr(pos + delimiter.size(), std::string::npos);
	MTIistringstream os(frameString);
	os >> frame;
	return result;
}

void ColorBreathingTool::rereadZonalFlickerData()
{
	auto model = getZonalFlickerModel();

	auto in = getSystemAPI()->getMarkIn();
	auto out = getSystemAPI()->getMarkOut();
	getClipPreprocessData().updatePreprocessDatabase(in, out);
	getClipPreprocessData().readPreprocessorData(in, out, model->getSegmentsDeltas());
	ExternalGuiRefresh(__LINE__);
}

void ColorBreathingTool::updateDatabaseSegmentAtIndex(int index)
{
	auto row = getClipPreprocessData().getSegmentRowFromFrameIndex(index);
	if (row.Id <= 0)
	{
		TRACE_0(errout << "Warning: No row found for " << index);
		return;
	}

	// This is a little dangerous as the database is not locked beween
	// the start and end.
	row.HeadAnchorOffset = getZonalFlickerModel()->getHeadAnchorOffset();
	row.TailAnchorOffset = getZonalFlickerModel()->getTailAnchorOffset();
	row.ReferenceFrame = getZonalFlickerModel()->getReferenceFrame();
	getClipPreprocessData().updateRow(row);
   getClipPreprocessData().getZonalDatabase().insertParametersIfNecessary(*getZonalFlickerModel());
}

int ColorBreathingTool::resmoothZonalFlickerData()
{
	CAutoErrorReporter autoErr(__func__, AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

	auto model = getZonalFlickerModel();

	int retVal = 1; // ????? WHY 1 ??? QQQ
	if (getClipPreprocessData().isDeltaValidOverMarks() == false)
	{
		retVal = CB_ERROR_NO_ANALYSIS_DATA;
		autoErr.errorCode = retVal;
		autoErr.msg << "Zonal: " << " No analysis data " << retVal;
		MTIassert(retVal < 0);
		return retVal;
	}

	updateZonalFlickerParameters();

	//// THIS DOESN'T BELONG HERE BUT WHERE????
   updateDatabaseSegmentAtIndex(GetCurrentFrameIndex());

	//// END THIS DOESN'T BELONG HERE

	// This updates database, needs to be fixed
	getClipPreprocessData().writeControlFile(*model);
	auto &zonalParameters = getZonalFlickerParameters();

	CHRTimer smoothTimer;
	auto result = getClipPreprocessData().resmoothPreprocessorData(zonalParameters, getRefFrame(),
		 getAnchorFrameOffsets(), getZonalFlickerSmoothingType());
	if (result)
	{
		rereadZonalFlickerData();
		getClipPreprocessData().resetRenderFlagsInMarks();
		retVal = 1; // ????? WHY 1 ??? QQQ
	}
	else
	{
		Beep(1000, 250);
		GetToolProgressMonitor()->StopProgress(true);
		// Note that the status message is gong to be immediately clobbered below.
		// I guess we don't care because it's not supposed to ever fail.
		GetToolProgressMonitor()->SetStatusMessage("Smoothing failed");
		retVal = -1;
	}

	GetToolProgressMonitor()->SetStatusMessage("Smoothing Completed");
	getSystemAPI()->goToFrameSynchronous(GetCurrentFrameIndex());
	ExternalGuiRefresh(__LINE__);

	return retVal;
}

void ColorBreathingTool::onlyRunZonalPreprocessing()
{
   getClipPreprocessData().clearAllAnalysis();
	auto model = getZonalFlickerModel();
	getClipPreprocessData().writeControlFile(*model);
	getClipPreprocessData().computeIsDefaultPreprocessingDone();
	setControlState(EMultiControlState::ACQURING);
	_nextStateAfterAcquire = EMultiControlState::IDLE;
	SetFlickerPreProcessing();
	StartToolProcessing(TOOL_PROCESSING_CMD_RENDER, false);
}

int ColorBreathingTool::generateZonalPreprocessDataIfNecessary()
{
	getClipPreprocessData().computeIsDefaultPreprocessingDone();

	if (getClipPreprocessData().isDeltaValidOverMarks() == false)
	{
		return generateZonalPreprocessData();
	}

	if (getClipPreprocessData().isSmoothingValidOverMarks() == false)
	{
		return resmoothZonalFlickerData();
	}

	return 0;
}

void ColorBreathingTool::resmoothZonalFlickerDataIfNecessary()
{
	if (getClipPreprocessData().isDeltaValidOverMarks() && !getClipPreprocessData().isSmoothingValidOverMarks())
	{
		resmoothZonalFlickerData();
	}

	findAnchorDeltaValuesIfNecessary(GetCurrentFrameIndex());
}

int ColorBreathingTool::generateZonalPreprocessData()
{
	CAutoErrorReporter autoErr(__func__, AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
	auto model = getZonalFlickerModel();

	int retVal = 0;
	if (model->isAnalysisRoiValid() == false)
	{
		retVal = CB_ERROR_USER_ROI_TOO_SMALL;
		autoErr.errorCode = retVal;
		autoErr.msg << "Zonal: " << " ROI too small, width must be >= 100, height >= 100 " << retVal;

		return retVal;
	}

	// This needs to be removed
	updateZonalFlickerParameters();
	auto &result = getZonalFlickerParameters();

	// This needs to be done to update database
	// but the actual file is only for debugging
	getClipPreprocessData().writeControlFile(*model);
	SetFlickerPreProcessing();
	StartToolProcessing(TOOL_PROCESSING_CMD_RENDER, false);

	writeClipBoundingBox();
	ExternalGuiRefresh(__LINE__);
	return 0;
}

string ColorBreathingTool::getClipPath() const
{
	auto clip = getSystemAPI()->getClip();
	return AddDirSeparator(clip->getClipPathName());
}

string ColorBreathingTool::getDeflickerPreprocessorExeName()
{
	String P = ExtractFilePath(Application->ExeName);
	return WCharToStdString(P.c_str()) + "DeflickerPreprocessorControl.exe";
}

string ColorBreathingTool::getDeflickerSmoothingExeName()
{

	String P = ExtractFilePath(Application->ExeName);
	return WCharToStdString(P.c_str()) + "DeflickerSmoothingControl.exe";
}

RECT ColorBreathingTool::getColorBreathingAnaysisRoi() const
{
	auto model = getGlobalFlickerModel();
	if (model->getUseAnalysisRegion())
	{
		return _globalFlickerRoi;
	}

	return getFullImageRoi();
}

RECT ColorBreathingTool::getFullImageRoi() const
{
	auto imageFormat = getSystemAPI()->getVideoClipImageFormat();
	if (imageFormat == nullptr)
	{
		return
		{
			-1, -1, -1, -1
		};
	}

	int pixelsCol = imageFormat->getPixelsPerLine();
	int pixelsRow = imageFormat->getLinesPerFrame();

	return
	{
		.left = 0, .right = pixelsCol, .top = 0, .bottom = pixelsRow
	};
}

void ColorBreathingTool::resetZonalRoi()
{
	if (GetIsRoiLocked())
	{
		Beep();
		return;
	}

	setZonalFlickerRoi(getFullImageRoi());
}

int ColorBreathingTool::getTotalFrames()
{
	auto videoFrameList = getSystemAPI()->getVideoFrameList();
	return (videoFrameList == nullptr) ? 0 : videoFrameList->getOutFrameIndex();
}

static inline float DistanceKernelX(int dx, int dy)
{
	auto r = sqrt(dx * dx + dy * dy);
	return r == 0 ? 0 : r * log(r);
}

int ColorBreathingTool::startAcquire() {return 0;}

int ColorBreathingTool::finishAcquire()
{
	if (_nextStateAfterAcquire == EMultiControlState::IDLE)
	{
		// Tracking only - do full stop
		// StopToolProcessing();  no - processed in TrackTool now!
		UpdateExecutionGUI(TOOL_CONTROL_STATE_STOPPED, false);
		GetToolProgressMonitor()->SetStatusMessage("Preprocessing complete");
	}

	rereadZonalFlickerData();

	return 0;
}

int ColorBreathingTool::startRendering()
{
	SetFlickerRender();
	RunFrameFlickerRender(-1);

	// I don't think this is needed
	_renderState = RENDER_STATE_RUNNING;
	return 0;
}

int ColorBreathingTool::finishRendering() {return 0;}

const unsigned char *ColorBreathingTool::getMask(int frameIndex)
{
	if (getSystemAPI()->IsMaskAvailable() == false)
	{
		_mask = nullptr;
		return _mask;
	}

   CHRTimer maskComputeTimer;
	if (_mask == nullptr || getSystemAPI()->IsMaskAnimated())
	{
		auto retVal = getSystemAPI()->GetMaskRoi(frameIndex, _maskROI);
		if (retVal != 0)
		{
			_mask = nullptr;
		}
		else
		{
			_mask = _maskROI.getBlendPtr();
		}
	}
   else
   {
		_mask = getSystemAPI()->GetMaskRoi()->getBlendPtr();
   }

	return _mask;
}

// Create a mask
void ColorBreathingTool::roiToMask() {getSystemAPI()->setRectToMask(getZonalFlickerRoi());}

void ColorBreathingTool::UnitTest()
{

}

void ColorBreathingTool::findAnchorDeltaValuesIfNecessary(int frameIndex)
{
	if (!getClipPreprocessData().isDeltaValidOverMarks())
	{
		// No valid data to analyze, can't do anything
      // (In reality, should be is Anchor analyzed over marks
		return;
	}

	try
	{
		auto sceneCut = _colorBreathingModel->BreakFrameData.GetBreakPair(frameIndex);

		auto markIn = getSystemAPI()->getMarkIn();
		auto markOut = getSystemAPI()->getMarkOut();
      sceneCut = {markIn, markOut};

      auto imageFormat = getSystemAPI()->getVideoClipImageFormat();
		MTIassert(imageFormat != nullptr);

		MTI_UINT16 caMax[3];
		imageFormat->getComponentValueMax(caMax);

		if (_zonalFlickerModel->getAnchorDeltaValues().areValuesValid(getZonalFlickerParameters(),
			 getAnchorFrameOffsets(), getRefFrame(), sceneCut))
		{
    	 //	getZonalFlickerModel()->createContrastLuts(caMax[0], CONTRAST_FORWARD_MAX, CONTRAST_LUT_SIZE);
			return;
		}

		auto anchorDeltaValues = findAnchorDeltaValues(sceneCut);
		_zonalFlickerModel->setAnchorDeltaValues(anchorDeltaValues, caMax[0]);
		getZonalFlickerModel()->createContrastLuts(caMax[0], CONTRAST_FORWARD_LIMIT, CONTRAST_LUT_SIZE);
	}
	catch (const std::exception &ex)
	{
		// We should do an auto error
		TRACE_0(errout << "Error in findAnchorDeltaValuesIfNecessary: " << ex.what());
	}
	catch (...)
	{
		TRACE_0(errout << "Unknown error in findAnchorDeltaValuesIfNecessary");
	}

}

void ColorBreathingTool::setRefFrame(int refFrameClipIndex)
{
	getZonalFlickerModel()->setReferenceFrame(refFrameClipIndex);
}

int ColorBreathingTool::getRefFrame() const
{
	return getZonalFlickerModel()->getReferenceFrame();
}

void ColorBreathingTool::setAnchorFrameOffsets(std::pair<int, int>anchorOffsets)
{
	// Should we do an earily out here if nothing changes. How do we REALLY know???
	getZonalFlickerModel()->setHeadAnchorOffset(anchorOffsets.first);
	getZonalFlickerModel()->setTailAnchorOffset(anchorOffsets.second);
}

std::pair<int, int>ColorBreathingTool::getAnchorFrameOffsets() const
{

return
	{
		getZonalFlickerModel()->getHeadAnchorOffset(), getZonalFlickerModel()->getTailAnchorOffset()
	};
}

void ColorBreathingTool::loadZonalParametersIfNecessary()
{
	// Easy out
	if (_oldFrameIndex == GetCurrentFrameIndex())
	{
		return;
	}

	_oldFrameIndex = GetCurrentFrameIndex();

	auto row = getClipPreprocessData().getSegmentRowFromFrameIndex(GetCurrentFrameIndex());
	if (row.Id <= 0)
	{
		return;
	}

	// Now lets if we have crossed a boundary
	if ((_currentCutRange.first == row.InFrame) && (_currentCutRange.second == row.OutFrame))
	{
		return;
	}

	_currentCutRange.first = row.InFrame;
	_currentCutRange.second = row.OutFrame;

	// If nothing is analysised, do nothing
	if (!getClipPreprocessData().isPreprocessingDoneForRow(row))
	{
//		DBTRACE("no preprocessing");
		return;
	}

	auto shotParameters = getClipPreprocessData().getZonalDatabase().getProcessedParameters(row);
	setZonalFlickerRoi(IppArrayConvert::convertToRect(shotParameters.getAnalysisRoi()));
	getZonalFlickerModel()->setParameters(shotParameters);
	ExternalGuiRefresh(__LINE__);
}

// Specialized routine, scratchArray must be local to any thread
Ipp16uArray ColorBreathingTool::readFrameNormalizeRoi(int frameIndex, Ipp16uArray &scratchArray)
{
   // We assume these parameters are set correctly
 	auto width = getZonalFlickerParameters().getImageSize().width;
	auto height = getZonalFlickerParameters().getImageSize().height;
  	auto analysisRoi = getZonalFlickerParameters().getAnalysisRoi();

   // Use temporary storage to read the data + alpha channel.
   // The alpha channel is after the 3 other channels
   MtiSize readFrameSize(width, height, 4);
   if (scratchArray.getSize() != readFrameSize)
   {
   	scratchArray = Ipp16uArray(readFrameSize);
   }

   // Read in first frame, this could contain alpha at end
	auto retVal = getSystemAPI()->getToolFrame(scratchArray.data(), frameIndex);
	if (retVal < 0)
	{
		MTIostringstream os;
		os << "Could not read frame, Error: " << retVal;
		THROW_MTI_RUNTIME_ERROR(os.str());
	}

   // This little trick removes the alpha at the end
   // We now have full image as an IppArray
   Ipp16uArray inputImageFull({width, height, 3}, scratchArray.data());

   // Create the subimage, cheap
   auto inputImageRoi16u = inputImageFull(analysisRoi).duplicate();

	// Apply lut, we really should have a class
   Ipp32sArray forwardLut;
   Ipp32sArray axisValues;
   std::tie(forwardLut, axisValues) = getZonalFlickerModel()->getContrastLuts();

   ScratchArray scratch;
  // inputImageRoi16u.applyLutInPlace({forwardLut}, axisValues, scratch);

   return inputImageRoi16u;
}

// This computes 5 frames from beginning of shot to end of shot
// If frames don't exist, they are duplicated at end (should be mirrored but I don't care as
// this should NEVER be requested by user.
// TODO: All this belongs down in the model, well actually a tool but don't have time now.
AnchorDeltaValues ColorBreathingTool::computeAnchorDeltaValues(const SceneCut &sceneCutx)
{
	AnchorDeltaValues anchorDeltaValues;

	auto markIn = getSystemAPI()->getMarkIn();
	auto markOut = getSystemAPI()->getMarkOut();
   SceneCut markCut = {markIn, markOut};

	auto row = getClipPreprocessData().getSegmentRowFromFrameIndex(sceneCutx.first);
	if (row.Id <= 0)
	{
		THROW_MTI_RUNTIME_ERROR("Unable to find Scene Cut");
	}

   if ((getAnchorFrameOffsets().first < 0) && (getAnchorFrameOffsets().second < 0))
   {
      return anchorDeltaValues;
   }

   // Lets see if the anchor values are correct
   if (DoesFileExist(row.getAnchorsFileName(markCut)))
   {
      return anchorDeltaValues;
   }


	auto monitor = GetToolProgressMonitor();
   monitor->PushStatusMessage("Processing Anchor Frames");
	monitor->Push();
   monitor->SetFrameCount(10);
   monitor->StartProgress();
   Application->ProcessMessages();

	anchorDeltaValues.set(getZonalFlickerParameters(), getAnchorFrameOffsets(), getRefFrame(), markCut);

	// We assume the zonal parameters are correct.
	// We need to the anchor size to reflect the downsample
	auto anchorZonalParameters = getZonalFlickerParameters();
	auto width = getZonalFlickerParameters().getImageSize().width;
	auto height = getZonalFlickerParameters().getImageSize().height;

//   DBTRACE(getZonalFlickerParameters().getImageSize());
	// The image size is the downsampled ROI
	auto processRoi = anchorZonalParameters.getAnalysisRoi();
	auto downsample = getZonalFlickerParameters().getDownsample();
 	MtiSize downSampleSize(width / downsample, height / downsample);
	anchorZonalParameters.setImageSize(downSampleSize);

//   DBTRACE(downSampleSize);
	// We are passing in only the analysis region to the lower leves
	anchorZonalParameters.setHorizontalBlocks(32);
	anchorZonalParameters.setSmoothingSigma(50);
  	anchorZonalParameters.setSmoothingType(0);
	anchorZonalParameters.setUseAnalysisRegion(false);
	anchorZonalParameters.setUseProcessingRegion(false);

	// We set the number of horizontal blocks but we are going to resize, so we must force recompute
	// the vertical blocks
	anchorZonalParameters.setVerticalBlocks(-1);

	// 5 is maximum anchor offset
	anchorZonalParameters.setHeadAnchorOffset(5);
	anchorZonalParameters.setTailAnchorOffset(5);

	int headAnchorOffset = anchorZonalParameters.getHeadAnchorOffset();
	int tailAnchorOffset = anchorZonalParameters.getTailAnchorOffset();

   // TODO: Convert to 16 bitWe are going to map the input values to 16 bit
   // anchorZonalParameters.setMaxValue(CONTRAST_FORWARD_MAX);
//   DBTRACE(anchorZonalParameters.getMaxValue());
	getZonalFlickerModel()->createContrastLuts(getZonalFlickerParameters().getMaxValue(), anchorZonalParameters.getMaxValue(), CONTRAST_LUT_SIZE);

   Ipp16uArray anchorScratch;

	auto analysisFullRoi = getZonalFlickerParameters().getAnalysisRoi();
	auto components = anchorZonalParameters.getZonalProcessType() == ZonalProcessType::Intensity ? 1 : 3;
   components = 3;
	auto imageScale = 1.0f / (anchorZonalParameters.getMaxValue() + 1);

//   DBTRACE2(anchorZonalParameters.getMaxValue(), imageScale);
	vector<ZonalFlickerOneChannelAnalysis>zonalFlickerAnalysis;
	IpaStripeStream iss;

	if (headAnchorOffset > 0)
	{
		// If head anchor offset is 0, we don't need to do anything.
		// If head anchor offset is n, we need to do n+1 frames, where n+1 th frame is the anchor frame
		// At the end the anchor frame has delta of zero and we will delete it.
		anchorZonalParameters.setInFrame(markCut.first);
		anchorZonalParameters.setOutFrame(markCut.first + headAnchorOffset + 1);

		// For each channel, create a analysis
		for (auto d : range(components))
		{
			zonalFlickerAnalysis.emplace_back(anchorZonalParameters);
		}

		// Read in first frame
      auto inputImageRoi16u = readFrameNormalizeRoi(markCut.first, anchorScratch);
		auto inputImageResized = Ipp32fArray(inputImageRoi16u).resizeAntiAliasing(downSampleSize);
		inputImageResized *= imageScale;

		// NOTE: copy to planer is from interleaved to new memory so we can now resuse the input buffer
		auto nextImage = inputImageResized.copyToPlanar();

		// Set up analysis
		auto previousImage = nextImage;


		CHRTimer hrt;
		for (auto offset : range(anchorZonalParameters.getFramesToProcess()))
		{
			// Analysis each component
			// for (auto d : range(components))
			auto f = [&](int d)
			{
				zonalFlickerAnalysis[d].preprocess(previousImage[d], nextImage[d]);
			};

			iss << components << efu_job(f);
			iss.stripe();

			previousImage = nextImage;

			// We don't need to read the next image if we are at anchor frame
			if (offset == headAnchorOffset)
			{
				break;
			}

			// Read next frame Note: sceneCut is a [], not a [) range
			auto nextFrameIndex = std::min<int>(markCut.first + offset + 1, markCut.second);
         inputImageRoi16u = readFrameNormalizeRoi(nextFrameIndex, anchorScratch);

  			// This saves a realloc
			inputImageResized <<= Ipp32fArray(inputImageRoi16u).resizeAntiAliasing(downSampleSize);
			inputImageResized *= imageScale;
			nextImage = inputImageResized.copyToPlanar();
         monitor->BumpProgress();
   		Application->ProcessMessages();
		}


		// Save raw values
		for (auto d : range(components))
		{
			auto rawValues = zonalFlickerAnalysis[d].getDeltaValues();
			anchorDeltaValues.headDeltaValues[d] = zonalFlickerAnalysis[d].smoothWithAnchors(rawValues, {0}, getZonalFlickerSmoothingType());
		}
	}

	if (tailAnchorOffset > 0)
	{
		// If tail anchor offset is 0, we don't need to do anything.
		// If tail anchor offset is n, we need to do n+1 frames, where first frame at (end - tail) is the anchor frame
		// Note, can actually be negative or before cut
		auto tailCutIndex = markCut.second - tailAnchorOffset;
		anchorZonalParameters.setInFrame(tailCutIndex);
		anchorZonalParameters.setOutFrame(markCut.second + 1);

		// Analysis each component, clear out any old analysis
		zonalFlickerAnalysis.clear();
		for (auto d : range(components))
		{
			zonalFlickerAnalysis.emplace_back(anchorZonalParameters);
		}

		// Read in first frame
		auto frameIndex = std::max<int>(tailCutIndex, markCut.first);
      auto inputImageRoi16u = readFrameNormalizeRoi(frameIndex, anchorScratch);

		auto inputImageResized = Ipp32fArray(inputImageRoi16u).resizeAntiAliasing(downSampleSize);
		inputImageResized *= imageScale;
		auto nextImage = inputImageResized.copyToPlanar();

		// Set up analysis
		auto previousImage = nextImage;

		CHRTimer hrt;
		for (auto offset : range(anchorZonalParameters.getFramesToProcess()))
		{
			// Analysis the frame
			// for (auto d : range(components))
			auto f = [&](int d)
			{
				zonalFlickerAnalysis[d].preprocess(previousImage[d], nextImage[d]);
			};

			iss << components << efu_job(f);
			iss.stripe();

			previousImage = nextImage;

			// We don't need to read the next image if are at the end frame
			if (offset == tailAnchorOffset)
			{
				break;
			}

			// Read next frame Note: sceneCut is a [], not a [) range
			auto nextFrameIndex = std::max<int>(tailCutIndex + offset + 1, markCut.first);
         inputImageRoi16u = readFrameNormalizeRoi(nextFrameIndex, anchorScratch);

			// Saves a memory allocation
			inputImageResized <<= Ipp32fArray(inputImageRoi16u).resizeAntiAliasing(downSampleSize);
			inputImageResized *= imageScale;
			nextImage = inputImageResized.copyToPlanar();
         monitor->BumpProgress();
         Application->ProcessMessages();
		}

		// Save raw values
		for (auto d : range(components))
		{
			auto rawValues = zonalFlickerAnalysis[d].getDeltaValues();
			anchorDeltaValues.tailDeltaValues[d] = zonalFlickerAnalysis[d].smoothWithAnchors(rawValues, {0}, getZonalFlickerSmoothingType());
		}
	}

	anchorDeltaValues.controlPositions = zonalFlickerAnalysis[0].getControlPositionInOriginalFrame
		 (getZonalFlickerParameters().getDownsample(), analysisFullRoi.x, analysisFullRoi.y);

	// Ugly flxup.
	if (components == 1)
	{
		anchorDeltaValues.headDeltaValues[1] = anchorDeltaValues.headDeltaValues[0];
		anchorDeltaValues.headDeltaValues[2] = anchorDeltaValues.headDeltaValues[0];
		anchorDeltaValues.tailDeltaValues[1] = anchorDeltaValues.tailDeltaValues[0];
		anchorDeltaValues.tailDeltaValues[2] = anchorDeltaValues.tailDeltaValues[0];
	}

  	monitor->Pop();
   monitor->PopStatusMessage();
   Application->ProcessMessages();
	return anchorDeltaValues;
}

// This half ass thing is a  proc and model, it really needs call backs etc
// No time to fix,
AnchorDeltaValues ColorBreathingTool::findAnchorDeltaValues(const SceneCut &sceneCutx)
{
	auto row = getClipPreprocessData().getSegmentRowFromFrameIndex(sceneCutx.first);
	if (row.Id <= 0)
	{
		THROW_MTI_RUNTIME_ERROR("No row found");
	}

   auto markIn = getSystemAPI()->getMarkIn();
	auto markOut = getSystemAPI()->getMarkOut();
   SceneCut markCut = {markIn, markOut};
 	int headAnchorOffset, tailAnchorOffset;
	std::tie(headAnchorOffset, tailAnchorOffset) = getAnchorFrameOffsets();
	MTIassert(headAnchorOffset <= 5);
	MTIassert(tailAnchorOffset <= 5);

   // This reads nothing if nothing to read
   if ((headAnchorOffset < 0) && (tailAnchorOffset < 0))
   {
	   AnchorDeltaValues emptyDeltaValues;
	   return emptyDeltaValues;
   }

   AnchorDeltaValues anchorDeltaValues;

   // The preprocessing may not have been done, so if not create it
   if (DoesFileExist(row.getAnchorsFileName(markCut)) == false)
   {
		anchorDeltaValues = computeAnchorDeltaValues(markCut);
		getClipPreprocessData().saveAnchorDeltaValues(row, anchorDeltaValues, markCut);
   }
   else
	{
		anchorDeltaValues = ThinPlateSplineDeltaData::readAnchorDeltaValues(row.getAnchorsFileName(markCut));
   }

	// The raw head is smoothed starting at zero
	if (headAnchorOffset > 0)
	{
		for (auto r : range(headAnchorOffset))
		{
			anchorDeltaValues.headDeltaValues[0][r] -= anchorDeltaValues.headDeltaValues[0][headAnchorOffset];
			anchorDeltaValues.headDeltaValues[1][r] -= anchorDeltaValues.headDeltaValues[1][headAnchorOffset];
			anchorDeltaValues.headDeltaValues[2][r] -= anchorDeltaValues.headDeltaValues[2][headAnchorOffset];
		}
	}

	if (tailAnchorOffset > 0)
	{
		for (auto r : range(6))
		{
			anchorDeltaValues.tailDeltaValues[0][5-r] -= anchorDeltaValues.tailDeltaValues[0][5-tailAnchorOffset];
			anchorDeltaValues.tailDeltaValues[1][5-r] -= anchorDeltaValues.tailDeltaValues[1][5-tailAnchorOffset];
			anchorDeltaValues.tailDeltaValues[2][5-r] -= anchorDeltaValues.tailDeltaValues[2][5-tailAnchorOffset];
		}
	}

	// This is overkill
	auto params = getZonalFlickerParameters();
	anchorDeltaValues.set(params, getAnchorFrameOffsets(), getRefFrame(), markCut);
	return anchorDeltaValues;
}

   int ColorBreathingTool::onFrameDiscardingOrCommitting(bool discardedFlag, const std::pair<int, int> &frameRange, std::string &message)
   {
   	if (discardedFlag == false)
      {
      	return TOOL_RETURN_CODE_NO_ACTION;
      }

      // The RGB filename may not exist, so ask for it
      if (DoesFileExist(GetRgbFileNameFromClip()) == false)
      {
         return TOOL_RETURN_CODE_NO_ACTION;
      }

      // Lets see if we are in one shot
		auto model = getGlobalFlickerModel();
		auto breakPair = model->BreakFrameData.GetBreakPair(frameRange.first);

      // Break pairs are inclusive, frameRange exclusing right.
      breakPair.second++;

      // See if we have to ask

      auto inOneCut = (breakPair.first >= frameRange.first) && (frameRange.second <= breakPair.second);

      // If we are in one cut, just clober it all, we don't care
		if (inOneCut)
		{
      	// We are in one cut, so frameRange is valid
         for (auto frameIndex = frameRange.first; frameIndex < frameRange.second; frameIndex++)
         {
            model->RGBFrameData[frameIndex].SetRGBInvalid();
         }

			model->RGBFrameData.Save();
			model->RGBFrameData.SetDataDirty(true);
			model->SetGraphDirty();
			onTopPanelRedraw();
         return TOOL_RETURN_CODE_NO_ACTION;
		}


      auto lastValidIndex = std::min<int>(frameRange.second, model->RGBFrameData.GetFrames());

      // See if we have any valid data
      auto renderedData = false;
      for (auto frameIndex = frameRange.first; frameIndex < lastValidIndex; frameIndex++)
      {
// Use this line when larry compains about valid data being thrown out
//         if (model->RGBFrameData[frameIndex].Rendered)
	      if (model->RGBFrameData[frameIndex].Valid)
         {
            renderedData = true;
            break;
         }
      }

      if (renderedData == false)
      {
         return TOOL_RETURN_CODE_NO_ACTION;
      }

      // Now ask
		MTIostringstream ostr;
		ostr << "There is valid GLOBAL FLICKER data in the marked range " << endl
           << "This will be discarded" << std::endl << std::endl
           << "Do you wish to continue?";

		int retVal = _MTIConfirmationDialog(Handle, ostr.str());
		if (retVal != MTI_DLG_OK)
		{
      	DBCOMMENT("Refusing to delete");
			return TOOL_RETURN_CODE_OPERATION_REFUSED;
		}

		// Now blow away we should do this by rendered frames in cut
		// But for now Larry always wants it destroyed
		for (auto frameIndex = frameRange.first; frameIndex < lastValidIndex; frameIndex++)
		{
			model->RGBFrameData[frameIndex].SetRGBInvalid();
		}

		model->RGBFrameData.Save();
		model->RGBFrameData.SetDataDirty(true);
		model->SetGraphDirty();
		onTopPanelRedraw();
		return TOOL_RETURN_CODE_NO_ACTION;


                   MTIostringstream os;
//      if (rgbFrameDatum.WasRendered)
//      {
//      	rgbFrameDatum.SetRGBInvalid();
//			model->RGBFrameData.Save();
//			model->RGBFrameData.SetDataDirty(true);
//			model->SetGraphDirty();
//			onTopPanelRedraw();
//      }
//		else if (rgbFrameDatum.Rendered)
//		{
//			// This code is wrong, it should set the model that then
//			// sets the dirty flag but I don't have time to fix it
//			rgbFrameDatum.Rendered = false;
//			model->RGBFrameData.Save();
//			model->RGBFrameData.SetDataDirty(true);
//			model->SetGraphDirty();
//			onTopPanelRedraw();
//		}

//	return TOOL_RETURN_CODE_NO_ACTION;
//   	DBTRACE(discardedFlag);
//   	DBTRACE2(frameRange.first, frameRange.second);
//      message = "WARNING: Discarding will remove the global color breathing data\n";
//      DBTRACE(message);
   }
