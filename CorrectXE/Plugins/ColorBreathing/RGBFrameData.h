// ---------------------------------------------------------------------------

#ifndef RGBFrameDataH
#define RGBFrameDataH

#include "ReferenceKeyframeData.h"
#include <algorithm>
#include <map>
#include <type_traits>
#include "Mmsystem.h"
#include "RGBFrameDatum.h"
#include "guid_mti.h"

class RGBFrameData
{
public:
   RGBFrameData();
   virtual ~RGBFrameData();

   void SetTotalFrames(int frames);
   void SetMarkRange(int markIn, int markOut);

   int GetFrames();

   int GetMarkIn();
   int GetMarkOut();

   bool CheckAndSetDataValid();
   bool CheckDataValid(const std::pair<int, int> &breakPair, const ShortROI &roi) const;
   bool CheckAndSetDataValid(const std::pair<int, int> &breakPair);

   RGBFrameDatum &operator[](int index);
   void SetRgbFrameDatum(int index, const RGBFrameDatum &rgbFrameDatum);
   RGBFrameDatum &GetRgbFrameDatum(int index);
   bool SetROI(int inIndex, int outIndex, const RECT &newROI);
   RECT GetROI(int frameIndex) const;
   void MarkRendered(int frameIndex);

   int Save(const string &fileName);
   int Load(const string &fileName, const _guid_t &expectedClipGUID);
   int Save();
   _guid_t GetGUID() const;

   vector<RGBFrameDatum> GetRGBFrameData();

   // This just says that the frame in, frame out don't define a range
   bool IsRangeValid() const;
   bool IsDataValid() const;
   void Invalidate();
	void Invalidate(int markIn, int markOut);
   void DissassociateFilename();

   bool IsGraphValid(int markIn, int markOut);
   bool IsRenderValid(int markIn, int markOut);
   bool AreAnyRenderFramesValid(int markIn, int markOut);
   void UpdateRenderFlag(const vector<int> &listOfCleanFrameIndex);

   int size() const;

   void SetROI(const RECT rect);
   RECT GetROI() const;
   bool SetAllUnsetROI(const RECT &newROI, int frameIndex);

   RGBFrameDatum at(int index) const;

   bool NewProcessedData = false;

   void SetLoadedFilename(const string fileName, const _guid_t &clipGUID);

   string LoadMessage;

   bool IsDataDirty() const;
   void SetDataDirty(bool dirty);

   void AddToPDLEntry(CPDLElement &parent);
   int ReadRGBFrameDataFromPDLEntry(CPDLElement *pdlAttribute);

private:
   // This is the magic number for the RGB file header
   const DWORD MagicNumber = MAKEFOURCC('R','G','B','4');
   vector<RGBFrameDatum>_rgbData;

   int _frameOut = -1;
   int _markIn = -1;
   int _markOut = -1;
   bool _rbgDataDirty = true;
   RECT _roiRect;

   bool _isRangeValid = false;
   bool _isDataValid = false;

   RGBFrameDatum _invalidRgbDatum;
   string _loadedFileName;
   _guid_t _clipGUID;

private:
   void setDataValid(bool value);
   static CSpinLock readWriteLock;
};
// ---------------------------------------------------------------------------
#endif
