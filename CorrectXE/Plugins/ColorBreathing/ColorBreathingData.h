//---------------------------------------------------------------------------

#ifndef ColorBreathingDataH
#define ColorBreathingDataH
//---------------------------------------------------------------------------

class ColorBreathingData
{
public:
   ColorBreathingData();
   int GetGraphType();

public:
   bool RedEnabled;
   bool GreenEnabled;
   bool BlueEnabled;

};

#endif
