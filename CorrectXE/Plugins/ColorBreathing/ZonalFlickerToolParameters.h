//---------------------------------------------------------------------------

#ifndef ZonalFlickerToolParametersH
#define ZonalFlickerToolParametersH
//---------------------------------------------------------------------------

#include "SpatialFlickerModel.h"
#include "ColorBreathingTool.h"

class ZonalFlickerToolParameters : public CToolParameters
{
public:
   ZonalFlickerToolParameters();
   virtual ~ZonalFlickerToolParameters() { };

   SpatialFlickerModel *Model = nullptr;
   ColorBreathingTool *Tool = nullptr;
   vector<int> anchorSceneIndexes;
};

#endif
