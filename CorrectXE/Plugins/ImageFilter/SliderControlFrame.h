//---------------------------------------------------------------------------


#ifndef SliderControlFrameH
#define SliderControlFrameH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TFrame1 : public TFrame
{
__published:	// IDE-managed Components
   TTrackBar *TrackBar;
   TLabel *NameLabel;
   TLabel *ValueLabel;
   TLabel *MinLabel;
   TLabel *MaxLabel;
private:	// User declarations
public:		// User declarations
   __fastcall TFrame1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFrame1 *Frame1;
//---------------------------------------------------------------------------
#endif
