#ifndef CPMPFilterH
#define CPMPFilterH

/* CPMPFilter.h: header file for filter library */

#include "machine.h"
#include "Repair.h"
#include "mthread.h"
#include "ImageDatumConvert.h"

#if !defined(SUCCESS)
#define SUCCESS 0
#endif 

#if !defined(FAILURE)
#define FAILURE -1
#endif 

/*
 * CGaussianKernel1D: one dimensional Gaussian kernel
 */

class CGaussianKernel1D {
public:
  CGaussianKernel1D(float radius = 1.0, float sigma = 1.0);
  void setRadius(float radius);
  void setSigma(float sigma);
  long generateKernel(float *fpKernel, long lLength);
private:
  float fRadius;
  float fSigma;
};


/*
 * CSeparableFilter: filters with separable kernels
 * Note:
 * (1) use setImageSize() instead of CRepair::setImageDimension() for dimensions.
 * (2) use CRepair::setImageComponent(long NCom, long PrincipalCom,
 *     long *pMinValue, long *pMaxValue, long *pFillValue) for components
 *     (arrays are either long or MTI_UNINT16 due to overloading). 
 */

class CSeparableFilter : public CRepair {
public:

  CSeparableFilter();
  ~CSeparableFilter();

  long allocate();
  void deallocate();

  void setRadii(bool issymmetric, float radiusX, float radiusY = 0.0);
  void setImageSize(long width, long height);
  void setActiveComponent(long active);
  void setROI(long lUserULCx, long lUserULCy, long lUserLRCx, long lUserLRCy);
  void setHorizAndVertIntervals(long lHoriz, long lVert);
  void setSourceDataTypeIsHalf(bool flag) { isSourceDataTypeHalf = flag; };

  void extractImage(MTI_UINT16 *uspImage);
  void applyFilter();
  void replaceImage(MTI_UINT16 *uspImage);
  float computeROIAverage(MTI_UINT16 *uspImage);

protected:
  bool bSymmetric;
  bool isSourceDataTypeHalf = false;
  float fRadiusX, fRadiusY;
  long lRadiusX, lRadiusY;
  float *fpKernelX, *fpKernelY;
  float *fpBuffer;
  float **fpVertical;
  long lActiveCom;
  long lULCx,lULCy,lLRCx,lLRCy;   /* rectangular ROI */
  long lPaddedNRow, lPaddedNCol;
  ////long lRangeMin, lRangeMax;
  long lLeftBoundary, lTopBoundary; /* ULC of Active Rectangle */


	inline float imageDatumToFloat(unsigned short datum)
	{
		return ImageDatum::toFloat(datum, isSourceDataTypeHalf);
	}

	inline unsigned short floatToImageDatum(float f)
	{
		return ImageDatum::fromFloat(f, isSourceDataTypeHalf);
	}
}; /* CSeparableFilter */

/*
 * class CGaussianFilter: Gaussian kernel filter  
 */

class CGaussianFilter : public CSeparableFilter {
public:

  CGaussianFilter();
  ~CGaussianFilter();

  long allocate();

  void setSigma(float sigmaX, float sigmaY = 1.0);
protected:
  float fSigmaX, fSigmaY;
};

/*
 * CUnsharpFilter: Unsharp Masking Filter
 */

class CUnsharpFilter : public CGaussianFilter {
public:

  CUnsharpFilter();
  ~CUnsharpFilter();

  void setSharpness(float sharpness);

  void replaceImage(MTI_UINT16 *uspImage);
  /* overrides CSeparableFilter::replaceImage() */
  float fThreshold;
private:
  float fSharpness;
};


// multi-threaded version of Unsharp Masking Filter

class CUnsharpFilter_MT
{
public:
   CUnsharpFilter_MT();
   virtual ~CUnsharpFilter_MT();

   void Initialize (long lNThreadArg);

   void setActiveComponent(long active);
	void setDataPointer(MTI_UINT16* dataPointer);
	void setSourceDataTypeIsHalf(bool flag);
	int setImageComponent (long NCom, long PrincipalCom, MTI_UINT16 *pMinValue,
                          MTI_UINT16 *pMaxValue, MTI_UINT16 *pFillValue);
   void setImageSize(long width, long height);
   void setHorizAndVertIntervals(long lHoriz, long lVert);
   void setRadii(bool issymmetric, float radiusX, float radiusY = 0.0);
   int setRegionOfInterestPtr(const MTI_UINT8 *ucpBlend, MTI_UINT8 *ucpLabel);
   void setROI(long lUserULCx, long lUserULCy, long lUserLRCx, long lUserLRCy);
   void setSharpness(float sharpness);
   void setSigma(float sigmaX, float sigmaY = 1.0);
   void setThreshold(float newThreshold);

   long allocate();
   void deallocate();

	long applyFilter();

	void extractImage(MTI_UINT16 *uspImage);
	void replaceImage(MTI_UINT16 *uspImage);


private:
  CUnsharpFilter *aufp;            // one per thread
  MTI_UINT16 *uspDataPointer;

  MTHREAD_STRUCT msUnsharpFilter;

  long lNThread;
  int iProcessError;

  static void CallProcess (void *vp, int iJob);

};

#endif // CPMP_FILTER_H
