/*
   File Name:  ImageFilterGUIWin.h

   Header file for the Windows-based (Borland) ImageFilter GUI

$Header: /usr/local/filmroot/Plugins/ImageFilter/ImageFilterGUIWin.h,v 1.10.2.11 2009/09/09 21:35:07 tolks Exp $
*/
//---------------------------------------------------------------------------

#ifndef GuiWinH
#define GuiWinH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MTIUNIT.h"
#include <ComCtrls.hpp>
#include <ToolWin.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include "ImageFilterTool.h"
#include <Mask.hpp>
#include "VTimeCodeEdit.h"
#include "HRTimer.h"
#include "SliderControlUnit.h"
#include "ExecButtonsFrameUnit.h"
#include "TrackEditFrameUnit.h"
#include "ExecStatusBarUnit.h"
#include "cspin.h"
#include "ColorPanel.h"
#include <System.ImageList.hpp>

/////////////////////////////////////////////////////////////////////////////
// Forward Declarations

class CPDLElement;

/////////////////////////////////////////////////////////////////////////////
// The public interface
// No need to class this because we link in when compilied
//
extern void CreateImageFilterGUI(void);
extern void DestroyImageFilterGUI(void);
extern bool ShowImageFilterGUI(void);
extern bool HideImageFilterGUI(void);
extern void UpdateExecutionButtons(EToolControlState toolProcessingControlState,
                                   bool processingRenderFlag);
extern bool RunExecButtonsCommand(EExecButtonId command);
extern void CaptureGUIParameters(CPDLElement &toolParams);
extern void GatherGUIParameters(CImageFilterParameters &imageFilterParams);
extern void SetGUIParameters(CImageFilterParameters &toolParams);
extern bool IsToolVisible(void);
extern void SelectApertureCorrection(void);
extern void SelectBlur(void);
extern void ToggleApertureRadiusGang(void);

//
//---------------------------------------------------------------------------
class TImageFilterForm : public TMTIForm
{
__published:	// IDE-managed Components
        TPanel *DontKnowWhatThisPanelIsFor;
        TTimer *Timer1;
        TImageList *ButtonImageList;
   TLabel *DoNotDeleteLabel;
        TPanel *StatusBar1;
        TLabel *FPSLabel;
        TLabel *MSLabel;
	TColorPanel *ImagefilterControlPanel;
    TPanel *CenteringPanel;
        TPanel *TitlePanel;
        TLabel *TitleLabel;
    TExecButtonsFrame *ExecButtonsToolbar;
        TGroupBox *ImageFilterTypeGroupBox;
        TRadioButton *ApertureCorrectionRadioButton;
        TRadioButton *BlurRadioButton;
        TGroupBox *ApertureCorrectionGroupBox;
        TTrackEditFrame *ApertureAmountTrackEdit;
        TTrackEditFrame *ApertureHRadiusTrackEdit;
        TTrackEditFrame *ApertureVRadiusTrackEdit;
        TTrackEditFrame *ApertureThresholdTrackEdit;
        TGroupBox *BlurGroupBox;
        TTrackEditFrame *BlurAmountTrackEdit;
        TTrackEditFrame *BlurRadiusTrackEdit;
        TCheckBox *ApertureRadiusGangCheckBox;
    TExecStatusBarFrame *ExecStatusBar;
        void __fastcall Timer1Timer(TObject *Sender);
   void __fastcall ImageFilterTypeRadioButtonChange(TObject *Sender);
   void __fastcall RightClickPopupPopup(TObject *Sender);
   void __fastcall ApertureHRadiusTrackEditChange(
          TObject *Sender);
   void __fastcall ApertureVRadiusTrackEditChange(
          TObject *Sender);
   void __fastcall ApertureRadiusGangCheckBoxClick(TObject *Sender);
   void __fastcall PDLCaptureButtonClick(TObject *Sender);
    void __fastcall ExecButtonsToolbar_ButtonPressedNotifier(
          TObject *Sender);
private:	// User declarations
   CHRTimer HRT;
   bool updatingApertureHRadius;
   bool updatingApertureVRadius;

   DEFINE_CBHOOK(ClipHasChanged, TImageFilterForm);
   DEFINE_CBHOOK(MarksHaveChanged, TImageFilterForm);
   
   void PreviewFrame(void);
   bool bDoPreviewAgain;

   // Exec buttons toolbar
   void ExecRenderOrPreview(EToolProcessingCommand buttonCommand);
   void ExecStop(void);
   void ExecCaptureToPDL(void);
   void ExecCaptureAllEventsToPDL(void);
   void ExecSetResumeTimecode(int frameIndex);
   void ExecSetResumeTimecodeToCurrent(void);
   void ExecSetResumeTimecodeToDefault(void);
   void ExecGoToResumeTimecode(void);
   void UpdateExecutionButtonToolbar(
         EToolControlState toolProcessingControlState,
         bool processingRenderFlag);

   void setBusyCursor(void);
   void setIdleCursor(void);

public:		// User declarations
   __fastcall TImageFilterForm(TComponent* Owner);
   bool doesFormUseOwnWindow() { return false; };

   void formCreate(void);
   void formShow(void);
   void formHide(void);

   void UpdateExecutionButtons(EToolControlState toolProcessingControlState,
                               bool processingRenderFlag);
   void CaptureParameters(CPDLElement &toolParams);
   void GatherParameters(CImageFilterParameters &imageFilterParams);
   void SetParameters(CImageFilterParameters &pdlToolParams);
   bool runExecButtonsCommand(EExecButtonId command);

   bool ReadSettings(CIniFile *ini, const string &IniSection);
   bool WriteSettings(CIniFile *ini, const string &IniSection);

   void selectApertureCorrection(void);
   void selectBlur(void);
   void toggleApertureRadiusGang(void);
};

//---------------------------------------------------------------------------
extern PACKAGE TImageFilterForm *ImageFilterForm;
extern MTI_UINT32 *ImageFeatureTable[];

//---------------------------------------------------------------------------
#endif
