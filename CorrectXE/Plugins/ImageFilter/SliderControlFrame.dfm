object Frame1: TFrame1
  Left = 0
  Top = 0
  Width = 320
  Height = 240
  TabOrder = 0
  object NameLabel: TLabel
    Left = 48
    Top = 48
    Width = 36
    Height = 13
    Caption = 'Amount'
  end
  object ValueLabel: TLabel
    Left = 120
    Top = 48
    Width = 12
    Height = 13
    Caption = '50'
  end
  object MinLabel: TLabel
    Left = 48
    Top = 80
    Width = 6
    Height = 13
    Caption = '0'
  end
  object MaxLabel: TLabel
    Left = 240
    Top = 80
    Width = 18
    Height = 13
    Caption = '100'
  end
  object TrackBar: TTrackBar
    Left = 44
    Top = 63
    Width = 215
    Height = 7
    Max = 100
    Orientation = trHorizontal
    PageSize = 10
    Frequency = 1
    Position = 50
    SelEnd = 0
    SelStart = 0
    TabOrder = 0
    TickMarks = tmBottomRight
    TickStyle = tsNone
  end
end
