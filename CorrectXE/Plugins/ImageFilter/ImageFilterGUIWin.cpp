/*
   File Name:  ImageFilterGUIWin.cpp

   This contains all the toolbar components for the ImageFilter

$Header: /usr/local/filmroot/Plugins/ImageFilter/ImageFilterGUIWin.cpp,v 1.12.2.26 2009/09/09 21:35:22 tolks Exp $
*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ImageFilterGUIWin.h"
#include "ClipAPI.h"
#include "PDL.h"
#include "ToolCommand.h"
#include "ToolObject.h"
#include "ToolSystemInterface.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MTIUNIT"
#pragma link "VTimeCodeEdit"
#pragma link "TrackEditFrameUnit"
#pragma link "ExecButtonsFrameUnit"
#pragma link "TrackEditFrameUnit"
#pragma link "ExecStatusBarUnit"
//#pragma link "cspin"
#pragma link "ColorPanel"
#pragma resource "*.dfm"

#define BUTTON_WIDTH 20
TImageFilterForm *ImageFilterForm;

static string generalSectionName = "General";
static string filterNameKey = "FilterName";
static string apertureCorrectionSectionName = "ApertureCorrection";
static string blurSectionName = "Blur";
static string amountKey = "Amount";
static string radiusKey = "Radius";
static string horizontalRadiusKey = "HorizontalRadius";
static string verticalRadiusKey = "VerticalRadius";
static string radiusLockKey = "RadiusLock";
static string thresholdKey = "Threshold";

#define PLUGIN_TAB_INDEX 9

//------------------CreateImageFilterGUI--------------------

  void CreateImageFilterGUI(void)

// This creates the ImageFilter GUI if one does not already exist.
// Note: only one can be created.
//
//****************************************************************************
{
   if (ImageFilterForm != NULL) return;         // Already created

   ImageFilterForm = new TImageFilterForm(Application);   // Create it
   ImageFilterForm->RestoreProperties();
   ImageFilterForm->formCreate();

   // Move all our controls to the unified tabbed tool window
   extern const char *PluginName;
   GImageFilter->getSystemAPI()->AdoptPluginToolGUI(PluginName, PLUGIN_TAB_INDEX,
         reinterpret_cast<int *>( ImageFilterForm->ImagefilterControlPanel ),
         NULL);
}

//-----------------DestroyImageFilterGUI------------------

  void DestroyImageFilterGUI(void)

//  This destroys then entire GUI interface
//
//***************************************************************************
{
   if (ImageFilterForm == NULL) return;         // Already destroyed

   // Reparent the controls back to us before destroying them
   if (GImageFilter != NULL)
      GImageFilter->getSystemAPI()->UnadoptPluginToolGUI(7);
   ImageFilterForm->ImagefilterControlPanel->Parent = ImageFilterForm;

   ImageFilterForm->Free();
   ImageFilterForm = NULL;
}

//-------------------ShowImageFilterGUI-------------------

    bool ShowImageFilterGUI(void)

//  This creates the GUI if not already exists and then shows it
//
//****************************************************************************
{
   CreateImageFilterGUI();            // Create the gui if necessary

   if (ImageFilterForm == NULL || GImageFilter == NULL)
      return false;

   ImageFilterForm->ImagefilterControlPanel->Visible = true;
   ImageFilterForm->formShow();

   return true;
}

//-------------------HideImageFilterGUI-------------------

    bool  HideImageFilterGUI(void)

//  This removes the tool from the screen
//
//****************************************************************************
{
   if (ImageFilterForm == NULL) return false;

   ImageFilterForm->ImagefilterControlPanel->Visible = false;
   ImageFilterForm->formHide();

   return false;  // was: (ImageFilterForm->Visible);
}

//------------------IsToolVisible------------------John Mertus----Aug 2001----

  bool IsToolVisible(void)

//  This returns the visual state of the gui
//
//****************************************************************************
{
    if (ImageFilterForm == NULL) return(false);         // Not created

    return ImageFilterForm->ImagefilterControlPanel->Visible;
}
//---------------------------------------------------------------------------

void UpdateExecutionButtons(EToolControlState toolProcessingControlState,
                            bool processingRenderFlag)

{
   if (ImageFilterForm == 0)
      return;

    ImageFilterForm->UpdateExecutionButtons(toolProcessingControlState,
                                           processingRenderFlag);
}
//---------------------------------------------------------------------------

void GatherGUIParameters(CImageFilterParameters &imageFilterParams)
{
   ImageFilterForm->GatherParameters(imageFilterParams);
}
//---------------------------------------------------------------------------

void CaptureGUIParameters(CPDLElement &pdlToolParams)
{
   ImageFilterForm->CaptureParameters(pdlToolParams);
}
//---------------------------------------------------------------------------

void SetGUIParameters(CImageFilterParameters &toolParams)
{
   if (ImageFilterForm == 0)
      return;  // form not available

   ImageFilterForm->SetParameters(toolParams);
}
//---------------------------------------------------------------------------

__fastcall TImageFilterForm::TImageFilterForm(TComponent* Owner)
 : TMTIForm(Owner)
{
   updatingApertureHRadius = false;
   updatingApertureVRadius = false;
   bDoPreviewAgain = false;
}
//---------------------------------------------------------------------------

void TImageFilterForm::UpdateExecutionButtons(EToolControlState toolProcessingControlState,
                                             bool processingRenderFlag)
{
   UpdateExecutionButtonToolbar(toolProcessingControlState,
                                processingRenderFlag);

}
//---------------------------------------------------------------------------

void TImageFilterForm::GatherParameters(
                                     CImageFilterParameters &imageFilterParams)
{
   if (ApertureCorrectionRadioButton->Checked)
      {
      imageFilterParams.filterType = IMAGE_FILTER_TYPE_APERTURE_CORRECT;
      imageFilterParams.amount = ApertureAmountTrackEdit->GetValue();
      imageFilterParams.horizontalRadius = ApertureHRadiusTrackEdit->GetValue();
      imageFilterParams.verticalRadius = ApertureVRadiusTrackEdit->GetValue();

      // Larry made me flip this control around
      int minVal, maxVal;
      ApertureThresholdTrackEdit->GetMinAndMax(minVal, maxVal);
      imageFilterParams.threshold = maxVal -
                                    ApertureThresholdTrackEdit->GetValue()
                                    + minVal;
      }
   else if (BlurRadioButton->Checked)
      {
      imageFilterParams.filterType = IMAGE_FILTER_TYPE_BLUR;
      imageFilterParams.amount = BlurAmountTrackEdit->GetValue();
      imageFilterParams.horizontalRadius = BlurRadiusTrackEdit->GetValue();
      imageFilterParams.verticalRadius = BlurRadiusTrackEdit->GetValue();
      }
   else
      {
      imageFilterParams.filterType = IMAGE_FILTER_TYPE_INVALID;
      }

}
//---------------------------------------------------------------------------

void TImageFilterForm::CaptureParameters(CPDLElement &pdlEntryToolParams)
{
   // Get the Image Filter parameters in the GUI to include in a PDL Entry
   CImageFilterParameters toolParams;

   // Get the settings from the GUI
   GatherParameters(toolParams);

   // Add the settings to the PDL Entry
   toolParams.WritePDLEntry(pdlEntryToolParams);
}
//---------------------------------------------------------------------------

void TImageFilterForm::SetParameters(CImageFilterParameters &toolParams)
{
   // load the image filter parameters
   if (toolParams.filterType == IMAGE_FILTER_TYPE_APERTURE_CORRECT)
      {
      ApertureCorrectionRadioButton->Checked = true;
      ApertureAmountTrackEdit->SetValue(toolParams.amount);
      ApertureHRadiusTrackEdit->SetValue(toolParams.horizontalRadius);
      ApertureVRadiusTrackEdit->SetValue(toolParams.verticalRadius);

      // Larry made me flip this control around
      int minVal, maxVal;
      ApertureThresholdTrackEdit->GetMinAndMax(minVal, maxVal);
      ApertureThresholdTrackEdit->SetValue(maxVal - toolParams.threshold
                                           + minVal);

      ApertureCorrectionGroupBox->Visible = true;
      BlurGroupBox->Visible = false;
      }
   else if (toolParams.filterType == IMAGE_FILTER_TYPE_BLUR)
      {
      BlurRadioButton->Checked = true;
      BlurAmountTrackEdit->SetValue(toolParams.amount);
      BlurRadiusTrackEdit->SetValue(toolParams.horizontalRadius);
      ApertureCorrectionGroupBox->Visible = false;
      BlurGroupBox->Visible = true;
      }
}
//---------------------------------------------------------------------------

bool RunExecButtonsCommand(EExecButtonId command)
{
   if (ImageFilterForm == NULL)  return false;

   return ImageFilterForm->runExecButtonsCommand(command);
}

bool TImageFilterForm::runExecButtonsCommand(EExecButtonId command)
{
   return ExecButtonsToolbar->RunCommand(command);
}
//---------------------------------------------------------------------------


void __fastcall TImageFilterForm::Timer1Timer(TObject *Sender)
{

   if (GImageFilter == NULL) return;

   if (bDoPreviewAgain)
     {
     EToolControlState toolControlState = GImageFilter->GetToolProcessingControlState();
     if (toolControlState == TOOL_CONTROL_STATE_STOPPED)
        {
        PreviewFrame();
        bDoPreviewAgain = false;
        UpdateExecutionButtonToolbar(toolControlState, false);
        }
     }
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------


//---------------------------------------------------------------------------

void TImageFilterForm::PreviewFrame(void)
{
   // Preview a single frame

   if (GImageFilter == NULL) return;     // Never can happen

   switch(GImageFilter->GetToolProcessingControlState())
      {
      case TOOL_CONTROL_STATE_STOPPED :
      case TOOL_CONTROL_STATE_PAUSED :
         // Start Preview processing for the current frame.  This function will
         // only act if Previewing/Rendering is stopped or paused.
         GImageFilter->StartToolProcessing(TOOL_PROCESSING_CMD_PREVIEW_1, false);
         break;

      case TOOL_CONTROL_STATE_RUNNING :
      case TOOL_CONTROL_STATE_WAITING_TO_PAUSE :
      case TOOL_CONTROL_STATE_WAITING_TO_STOP :
        // Preview or Render processing is running, so should not attempt
        // to do a preview 1 frame.

        // TTT If this function was called because the user changed a parameter
        //     then, in theory, we could immediately send the parameters to
        //     the ImageFilter algorithm.  Maybe we'll do this in the future,
        //     but for now, just beep.
        Beep();
        break;

      case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_STOPPED :
      case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_PAUSED :
         // Preview 1 Frame is already running, so this function was probably 
         // called because the user had changed a parameter.  Remember to run
         // Preview 1 Frame again later.
         bDoPreviewAgain = true;
         break;

      default:
         MTIassert(false);
         break;
      }
}
//---------------------------------------------------------------------------

void __fastcall TImageFilterForm::RightClickPopupPopup(TObject *Sender)
{
   // This function is called just as the right-click popup menu pops up
   // This gives us a chance to set the state of any menu item

   // Set checked state of "Always on Top" menu item based on the
   // style of this form
}
//---------------------------------------------------------------------------


//------------------ClipHasChanged----------------John Mertus----Nov 2002-----

    void TImageFilterForm::ClipHasChanged(void *Sender)

//  This is called when the clip is loaded into the main window
//  Just disconnect the remote display
//
//****************************************************************************
{
   if (GImageFilter == NULL) return;

   ExecSetResumeTimecodeToDefault();

   UpdateExecutionButtons(TOOL_CONTROL_STATE_STOPPED, false);
}

//------------------MarksHaveChanged----------------------------Mar 2008-----

    void TImageFilterForm::MarksHaveChanged(void *Sender)

//  This is called when the marks change
//
//****************************************************************************
{
   if (GImageFilter == NULL) return;

   EToolControlState toolControlState = GImageFilter->GetToolProcessingControlState();
   if (toolControlState == TOOL_CONTROL_STATE_STOPPED)
      UpdateExecutionButtonToolbar(toolControlState, false);
}
//---------------------------------------------------------------------------

void TImageFilterForm::formCreate(void)
{
   // Larry made me turn this control around
   //ApertureThresholdTrackEdit->SetHighlightRightToLeft(true);

   GImageFilter->SetToolProgressMonitor(ExecStatusBar);
}
//---------------------------------------------------------------------------

void TImageFilterForm::formShow(void)
{
   if (GImageFilter == NULL) return;

   SET_CBHOOK(ClipHasChanged, GImageFilter->ClipChange);
   SET_CBHOOK(MarksHaveChanged, GImageFilter->MarksChange);

   // Init region select type
   ImageFilterTypeRadioButtonChange(NULL);

   UpdateExecutionButtonToolbar(TOOL_CONTROL_STATE_STOPPED, false);
}
//---------------------------------------------------------------------------

void TImageFilterForm::formHide(void)
{
   if (GImageFilter == NULL) return;

   REMOVE_CBHOOK(ClipHasChanged, GImageFilter->ClipChange);
   REMOVE_CBHOOK(MarksHaveChanged, GImageFilter->MarksChange);
}
//---------------------------------------------------------------------------


void __fastcall TImageFilterForm::ImageFilterTypeRadioButtonChange(
      TObject *Sender)
{
   bool apertureCorrectionFlag = ApertureCorrectionRadioButton->Checked;
   bool blurFlag = BlurRadioButton->Checked;

   ApertureCorrectionGroupBox->Visible = apertureCorrectionFlag;
   BlurGroupBox->Visible = blurFlag;

}
//---------------------------------------------------------------------------

bool TImageFilterForm::ReadSettings(CIniFile *ini, const string &IniSection)

//  This is the usual save, however, the ini file points to the calling program
//  and we want it to go to the ImageFilter.ini file in .cpmprc
//  So we just open up a new one
//
//******************************************************************************
{
   string ImageFilterIniFileName = "$(CPMP_USER_DIR)"
                                  + GImageFilter->GetToolName() + ".ini";

   CIniFile* iniFile = CreateIniFile(ImageFilterIniFileName);
   if (iniFile == NULL)
      {
      TRACE_0(errout << "ERROR: ImageFilter::ReadSettings: Could not read "
                     << ImageFilterIniFileName << endl
                     << "       Because " << theError.getMessage());
      return(false);
      }

   int value;

   // Read Filter Name
   string filterName, defaultFilterName;
   defaultFilterName = StringToStdString(ApertureCorrectionRadioButton->Caption.Trim());
   filterName = iniFile->ReadString(generalSectionName, filterNameKey, defaultFilterName);

   if (filterName == defaultFilterName)
      {
      ApertureCorrectionRadioButton->Checked = true;
      }
   else
      {
      BlurRadioButton->Checked = true;
      }
   ImageFilterTypeRadioButtonChange(NULL);
   
   // Read Aperture Correction Filter section
   value = iniFile->ReadInteger(apertureCorrectionSectionName, amountKey, 20);
   ApertureAmountTrackEdit->SetValue(value);
   value = iniFile->ReadInteger(apertureCorrectionSectionName,
                                horizontalRadiusKey, 50);
   ApertureHRadiusTrackEdit->SetValue(value);
   value = iniFile->ReadInteger(apertureCorrectionSectionName,
                                verticalRadiusKey, 50);
   ApertureVRadiusTrackEdit->SetValue(value);
   bool checked = iniFile->ReadBool(apertureCorrectionSectionName,
                                    radiusLockKey, true);
   ApertureRadiusGangCheckBox->Checked = checked;
   value = iniFile->ReadInteger(apertureCorrectionSectionName, thresholdKey, 90);
   ApertureThresholdTrackEdit->SetValue(value);

   // Read Blur Filter section
   value = iniFile->ReadInteger(apertureCorrectionSectionName, amountKey, 50);
   BlurAmountTrackEdit->SetValue(value);
   value = iniFile->ReadInteger(apertureCorrectionSectionName, radiusKey, 50);
   BlurRadiusTrackEdit->SetValue(value);

   return DeleteIniFile(iniFile);
}
//---------------------------------------------------------------------------

bool TImageFilterForm::WriteSettings(CIniFile *ini, const string &IniSection)

//  This is the usual save, however, the ini file points to the calling program
//  and we want it to go to the ImageFilter.ini file in .cpmprc
//  So we just open up a new one
//
//******************************************************************************
{
   string ImageFilterIniFileName = "$(CPMP_USER_DIR)"
                                  + GImageFilter->GetToolName() + ".ini";

   CIniFile* iniFile = CreateIniFile(ImageFilterIniFileName);
   if (iniFile == NULL)
      {
      TRACE_0(errout << "ERROR: ImageFilter::WriteSettings: Could not create "
                     << ImageFilterIniFileName << endl
                     << "       Because " << theError.getMessage());
      return(false);
      }

   // Write Filter Name
   string filterName = StringToStdString(ApertureCorrectionRadioButton->Caption.Trim());
   if (BlurRadioButton->Checked)
      {
      filterName = StringToStdString(BlurRadioButton->Caption.Trim());
      }
   iniFile->WriteString(generalSectionName, filterNameKey, filterName);

   // Write Aperture Correction Filter section
   iniFile->WriteInteger(apertureCorrectionSectionName, amountKey,
                         ApertureAmountTrackEdit->GetValue());
   iniFile->WriteInteger(apertureCorrectionSectionName, horizontalRadiusKey,
                         ApertureHRadiusTrackEdit->GetValue());
   iniFile->WriteInteger(apertureCorrectionSectionName, verticalRadiusKey,
                         ApertureVRadiusTrackEdit->GetValue());
   iniFile->WriteBool(apertureCorrectionSectionName, radiusLockKey,
                      ApertureRadiusGangCheckBox->Checked);
   iniFile->WriteInteger(apertureCorrectionSectionName, thresholdKey,
                         ApertureThresholdTrackEdit->GetValue());

   // Write Blur Filter section
   iniFile->WriteInteger(apertureCorrectionSectionName, amountKey,
                         BlurAmountTrackEdit->GetValue());
   iniFile->WriteInteger(apertureCorrectionSectionName, radiusKey,
                         BlurRadiusTrackEdit->GetValue());

   return DeleteIniFile(iniFile);
}
//---------------------------------------------------------------------------

void __fastcall TImageFilterForm::ApertureHRadiusTrackEditChange(
      TObject *Sender)
{
   updatingApertureHRadius = true;
   //ApertureHRadiusTrackEdit->TrackBarChange(Sender);

   if (ApertureRadiusGangCheckBox->Checked && !updatingApertureVRadius)
      ApertureVRadiusTrackEdit->SetValue(ApertureHRadiusTrackEdit->GetValue());

   updatingApertureHRadius = false;
}
//---------------------------------------------------------------------------

void __fastcall TImageFilterForm::ApertureVRadiusTrackEditChange(
      TObject *Sender)
{
   updatingApertureVRadius = true;
   //ApertureVRadiusTrackEdit->TrackBarChange(Sender);

   if (ApertureRadiusGangCheckBox->Checked && !updatingApertureHRadius)
      ApertureHRadiusTrackEdit->SetValue(ApertureVRadiusTrackEdit->GetValue());

   updatingApertureVRadius = false;
}
//---------------------------------------------------------------------------

void __fastcall TImageFilterForm::ApertureRadiusGangCheckBoxClick(
      TObject *Sender)
{
   updatingApertureHRadius = true;

   if (ApertureRadiusGangCheckBox->Checked && !updatingApertureVRadius)
      ApertureVRadiusTrackEdit->SetValue(ApertureHRadiusTrackEdit->GetValue());

   updatingApertureHRadius = false;
}

void ToggleApertureRadiusGang()
{
   if (ImageFilterForm == 0)
      return;  // form not available

   ImageFilterForm->toggleApertureRadiusGang();
}

void TImageFilterForm::toggleApertureRadiusGang()
{
   if (ApertureCorrectionRadioButton->Checked)
   {
      ApertureRadiusGangCheckBox->Checked = !ApertureRadiusGangCheckBox->Checked;
   }
}
//---------------------------------------------------------------------------


void __fastcall TImageFilterForm::PDLCaptureButtonClick(TObject *Sender)
{
   if (GImageFilter == NULL) return;

   bool all = false;
   if (HIWORD(GetKeyState(VK_SHIFT)) != 0)
      all = true;

   GImageFilter->getSystemAPI()->CapturePDLEntry(all);
}
//---------------------------------------------------------------------------




//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//------------------------ EXEC BUTTONS TOOLBAR -----------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

void __fastcall TImageFilterForm::ExecButtonsToolbar_ButtonPressedNotifier(
      TObject *Sender)
{
   switch (ExecButtonsToolbar->GetLastButtonClicked())
   {
      default:
      case EXEC_BUTTON_NONE:
         // No-op
      break;

      case EXEC_BUTTON_PREVIEW_FRAME:
         PreviewFrame();
      break;

      case EXEC_BUTTON_PREVIEW_ALL:
         ExecRenderOrPreview(TOOL_PROCESSING_CMD_PREVIEW);
      break;

      case EXEC_BUTTON_RENDER_FRAME:
#if 0 // FIX ME
         RenderFrame();
#endif
      break;

      case EXEC_BUTTON_RENDER_ALL:
         ExecRenderOrPreview(TOOL_PROCESSING_CMD_RENDER);
      break;

      case EXEC_BUTTON_PAUSE_OR_RESUME:
         if (ExecButtonsToolbar->IsButtonEnabled(EXEC_BUTTON_PAUSE))
            ExecRenderOrPreview(TOOL_PROCESSING_CMD_PAUSE);
         else if (ExecButtonsToolbar->IsButtonEnabled(EXEC_BUTTON_RESUME))
            ExecRenderOrPreview(TOOL_PROCESSING_CMD_CONTINUE);
      break;

      case EXEC_BUTTON_PAUSE:
         ExecRenderOrPreview(TOOL_PROCESSING_CMD_PAUSE);
      break;

      case EXEC_BUTTON_RESUME:
         ExecRenderOrPreview(TOOL_PROCESSING_CMD_CONTINUE);
      break;

      case EXEC_BUTTON_STOP:
         ExecStop();
         break;

      case EXEC_BUTTON_CAPTURE_PDL:
         ExecCaptureToPDL();
      break;

      case EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS:
         ExecCaptureAllEventsToPDL();
      break;

      case EXEC_BUTTON_GO_TO_RESUME_TIMECODE:
         ExecGoToResumeTimecode();
      break;

      case EXEC_BUTTON_SET_RESUME_TC:
         ExecSetResumeTimecodeToCurrent();
      break;
   }
}
//---------------- Resume Timecode Functions --------------------------------

void TImageFilterForm::ExecSetResumeTimecode(int frameIndex)
{
   CVideoFrameList *frameList = GImageFilter->getSystemAPI()->getVideoFrameList();
   if (frameList == NULL) return;

   if (frameIndex < 0)
      frameIndex = frameList->getInFrameIndex();

   PTimecode frameTimecode = frameList->getTimecodeForFrameIndex(frameIndex);
   ExecButtonsToolbar->SetResumeTimecode(frameTimecode);
}
//---------------------------------------------------------------------------

void TImageFilterForm::ExecSetResumeTimecodeToCurrent(void)
{
   if (GImageFilter == NULL) return;

   int frameIndex = GImageFilter->getSystemAPI()->getLastFrameIndex();
   ExecSetResumeTimecode(frameIndex);
}
//---------------------------------------------------------------------------

void TImageFilterForm::ExecSetResumeTimecodeToDefault(void)
{
   if (GImageFilter == NULL) return;

   int frameIndex = GImageFilter->getSystemAPI()->getMarkIn();
   ExecSetResumeTimecode(frameIndex);
}
//---------------------------------------------------------------------------

void TImageFilterForm::ExecGoToResumeTimecode(void)
{
   if (GImageFilter == NULL) return;

   CVideoFrameList *frameList;
   frameList = GImageFilter->getSystemAPI()->getVideoFrameList();
   if (frameList == NULL) return;

   // No! GImageFilter->getSystemAPI()->AcceptGOV();   // Auto-accept GOV if pending

   PTimecode resumeTimecode = ExecButtonsToolbar->GetResumeTimecode();
   int frameIndex = frameList->getFrameIndex(resumeTimecode);
   GImageFilter->getSystemAPI()->goToFrameSynchronous(frameIndex);

   // In case it ends up somewhere else?
   frameIndex = GImageFilter->getSystemAPI()->getLastFrameIndex();
   ExecButtonsToolbar->SetResumeTimecode(
                        frameList->getTimecodeForFrameIndex(frameIndex));
}

//------------------ ExecRenderOrPreview ------------------------------------
//
//  Call this when any render or preview button is clicked
//  buttonCommand identifies the button that was pressed
//
void TImageFilterForm::ExecRenderOrPreview(EToolProcessingCommand buttonCommand)
{
   switch (buttonCommand)
      {
      // OLD_RENDER_DEST_HACK
      case TOOL_PROCESSING_CMD_RENDER :
         GImageFilter->getSystemAPI()->initRenderDestinationClip(0 /*RenderRadioBox->ItemIndex*/);
         /* FALL THROUGH */
      case TOOL_PROCESSING_CMD_PREVIEW :
         GImageFilter->StartToolProcessing(buttonCommand,false);
         break;

      case TOOL_PROCESSING_CMD_PAUSE :
         GImageFilter->PauseToolProcessing();
         break;

      case TOOL_PROCESSING_CMD_CONTINUE :
         {
         CVideoFrameList *frameList;
         frameList = GImageFilter->getSystemAPI()->getVideoFrameList();
         int resumeFrame;
         if (frameList == NULL)
            resumeFrame = GImageFilter->getSystemAPI()->getMarkIn();
         else
         {
            PTimecode resumeTimecode = ExecButtonsToolbar->GetResumeTimecode();
            resumeFrame = frameList->getFrameIndex(resumeTimecode);
         }

         GImageFilter->ContinueToolProcessing(resumeFrame);
         }
         break;

      default:
         MTIassert(false);
         break;
      }
}
//---------------------------------------------------------------------------

void TImageFilterForm::ExecStop(void)
{
   if (GImageFilter == NULL) return;

   GImageFilter->StopToolProcessing();
}
//---------------------------------------------------------------------------

void TImageFilterForm::ExecCaptureToPDL(void)
{
   if (GImageFilter == NULL) return;

   bool all = false;
   if (HIWORD(GetKeyState(VK_SHIFT)) != 0)
      all = true;

   GImageFilter->getSystemAPI()->CapturePDLEntry(all);
}
//---------------------------------------------------------------------------

void TImageFilterForm::ExecCaptureAllEventsToPDL(void)
{
   if (GImageFilter == NULL) return;

   GImageFilter->getSystemAPI()->CapturePDLEntry(true);
}
//---------------------------------------------------------------------------

void TImageFilterForm::UpdateExecutionButtonToolbar(
      EToolControlState toolProcessingControlState,
      bool processingRenderFlag)
{
   if (GImageFilter == NULL) return;

   if (GImageFilter->IsDisabled())
   {
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
         ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_STOP);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

//         ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_ALL,
//                                 "Can't preview - tool is disabled");
//         ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL,
//                                 "Can't render - tool is disabled");
//         ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL,
//                                 "Can't add to PDL - tool is disabled");
         return;
   }

   switch(toolProcessingControlState)
   {
      case TOOL_CONTROL_STATE_STOPPED :

         // Previewing or Rendering has stopped, either because processing
         // has completed or the user pressed the Stop button

         setIdleCursor();

         ExecButtonsToolbar->EnableButton(EXEC_BUTTON_PREVIEW_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);

         if (GImageFilter->AreMarksValid())
         {
            ExecButtonsToolbar->EnableButton(EXEC_BUTTON_PREVIEW_ALL);
            ExecButtonsToolbar->EnableButton(EXEC_BUTTON_RENDER_ALL);
            ExecButtonsToolbar->EnableButton(EXEC_BUTTON_CAPTURE_PDL);

//            ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_ALL,
//                                    "Preview marked range (Shift+D)");
//            ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL,
//                                    "Render marked range (Shift+G)");
//            ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL,
//                                    "Add to PDL (, key)");
         }
         else
         {
            ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
            ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
            ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

//            ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_ALL,
//                                    "Can't preview - marks are invalid");
//            ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL,
//                                    "Can't render - marks are invalid");
//            ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL,
//                                    "Can't add to PDL - marks are invalid");
         }

         ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_STOP);

         ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_NONE);

         ExecButtonsToolbar->EnableResumeWidget();
         ExecSetResumeTimecodeToDefault();

      break;

      case TOOL_CONTROL_STATE_RUNNING :

         // Preview1, Render1, Preview or Render is now running

         setBusyCursor();

         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
         ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
#if 0 // FIX ME
         if (processingSingleFrameFlag)
            ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
         else
#endif
            ExecButtonsToolbar->EnableButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->EnableButton(EXEC_BUTTON_STOP);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

         if (!processingRenderFlag)
            ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_PREVIEW_ALL);
         else
            ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_RENDER_ALL);

      break;

      case TOOL_CONTROL_STATE_PAUSED :
         // Previewing or Rendering is now Paused

         setIdleCursor();

         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
         ExecButtonsToolbar->ShowButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->EnableButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->HideButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->EnableButton(EXEC_BUTTON_STOP);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

         // Now go load the current timecode
         ExecSetResumeTimecodeToCurrent();

      break;

      case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_STOPPED :
      case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_PAUSED :
      case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_STOPPED :
      case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_PAUSED :
      case TOOL_CONTROL_STATE_WAITING_TO_PAUSE :
      case TOOL_CONTROL_STATE_WAITING_TO_STOP :

         // Processing single frame or waiting to stop or pause --
         // disable everything
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
         ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
         ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_STOP);
         ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

         if (toolProcessingControlState != TOOL_CONTROL_STATE_WAITING_TO_PAUSE
          && toolProcessingControlState != TOOL_CONTROL_STATE_WAITING_TO_STOP)
         {
            if (!processingRenderFlag)
               ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_PREVIEW_FRAME);
            else
               ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_RENDER_FRAME);
         }

         break;

      default:
         MTIassert(false);

         break;
   }

   //UpdateStatusBarInfo();

} // UpdateExecutionButton()
//---------------------------------------------------------------------------

void TImageFilterForm::setIdleCursor(void)
{
   Screen->Cursor = crDefault;
}
//---------------------------------------------------------------------------

void TImageFilterForm::setBusyCursor(void)
{
   Screen->Cursor = crAppStart;
}
//---------------------------------------------------------------------------

void SelectApertureCorrection(void)
{
   if (ImageFilterForm != NULL)
      ImageFilterForm->selectApertureCorrection();
}

void TImageFilterForm::selectApertureCorrection(void)
{
   ApertureCorrectionRadioButton->Checked = true;

   if ((GImageFilter == NULL) || GImageFilter->IsDisabled() || !IsToolVisible())
      return;

   CToolSystemInterface *api = GImageFilter->getSystemAPI();
   TObject *focusedObject =
            reinterpret_cast<TObject *>(api->GetFocusedAdoptedControl());

   if (focusedObject == ApertureAmountTrackEdit->TrackBar ||
       focusedObject == ApertureAmountTrackEdit->Edit)
   {
      api->FocusAdoptedControl(
            reinterpret_cast<int *>(ApertureHRadiusTrackEdit->TrackBar)
            );
   }
   else if (focusedObject == ApertureHRadiusTrackEdit->TrackBar ||
       focusedObject == ApertureHRadiusTrackEdit->Edit)
   {
      if (ApertureRadiusGangCheckBox->Checked)
         api->FocusAdoptedControl(
               reinterpret_cast<int *>(ApertureThresholdTrackEdit->TrackBar)
               );
      else
         api->FocusAdoptedControl(
               reinterpret_cast<int *>(ApertureVRadiusTrackEdit->TrackBar)
               );
   }
   else if (focusedObject == ApertureVRadiusTrackEdit->TrackBar ||
       focusedObject == ApertureVRadiusTrackEdit->Edit)
   {
      api->FocusAdoptedControl(
            reinterpret_cast<int *>(ApertureThresholdTrackEdit->TrackBar)
            );
   }
   else // ApertureThresholdTrackEdit or none of the four was selected
   {
      api->FocusAdoptedControl(
            reinterpret_cast<int *>(ApertureAmountTrackEdit->TrackBar)
            );
   }
}
//---------------------------------------------------------------------------

void SelectBlur(void)
{
   if (ImageFilterForm != NULL)
      ImageFilterForm->selectBlur();
}

void TImageFilterForm::selectBlur(void)
{
   BlurRadioButton->Checked = true;

   if (GImageFilter == NULL) return;

   CToolSystemInterface *api = GImageFilter->getSystemAPI();
   TObject *focusedObject =
            reinterpret_cast<TObject *>(api->GetFocusedAdoptedControl());

   if (focusedObject == BlurAmountTrackEdit->TrackBar ||
       focusedObject == BlurAmountTrackEdit->Edit)
   {
      api->FocusAdoptedControl(
            reinterpret_cast<int *>(BlurRadiusTrackEdit->TrackBar)
            );
   }
   else // BlurRadiusTrackEdit or neither of the two was selected
   {
      api->FocusAdoptedControl(
            reinterpret_cast<int *>(BlurAmountTrackEdit->TrackBar)
            );
   }
}
//---------------------------------------------------------------------------

