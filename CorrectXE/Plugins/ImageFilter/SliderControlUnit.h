//---------------------------------------------------------------------------


#ifndef SliderControlUnitH
#define SliderControlUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TSliderControlFrame : public TFrame
{
__published:	// IDE-managed Components
   TTrackBar *TrackBar;
   TLabel *NameLabel;
   TLabel *ValueLabel;
   TLabel *MinLabel;
   TLabel *MaxLabel;
   void __fastcall TrackBarChange(TObject *Sender);
private:	// User declarations
   void SetValueLabel(int newValue);
   bool ReverseSelectionFlag;

public:		// User declarations
   __fastcall TSliderControlFrame(TComponent* Owner);

   int GetValue();
   void GetLimits(int &callersMin, int &callersMax);
   bool GetReverseSelectionFlag();

   void SetValue(int newValue);
   void SetLimits(int newMin, int newMax, int newValue);
   void SetReverseSelectionFlag(bool newFlag);
};
//---------------------------------------------------------------------------
extern PACKAGE TSliderControlFrame *SliderControlFrame;
//---------------------------------------------------------------------------
#endif
