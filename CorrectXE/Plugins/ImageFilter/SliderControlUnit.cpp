//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "SliderControlUnit.h"
#include "machine.h"
#include "MTIstringstream.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TSliderControlFrame *SliderControlFrame;
//---------------------------------------------------------------------------
__fastcall TSliderControlFrame::TSliderControlFrame(TComponent* Owner)
   : TFrame(Owner), ReverseSelectionFlag(false)
{
   SetValueLabel(TrackBar->Position);
}
//---------------------------------------------------------------------------
void __fastcall TSliderControlFrame::TrackBarChange(TObject *Sender)
{
   SetValueLabel(TrackBar->Position);
   if (ReverseSelectionFlag)
   {
      TrackBar->SelEnd = TrackBar->Max;
      TrackBar->SelStart = TrackBar->Position;
   }
   else
   {
      TrackBar->SelStart = TrackBar->Min;
      TrackBar->SelEnd = TrackBar->Position;
   }
}
//---------------------------------------------------------------------------

void TSliderControlFrame::SetValueLabel(int newValue)
{
   MTIostringstream ostrm;
   ostrm << newValue;
   ValueLabel->Caption = ostrm.str().c_str();
}

void TSliderControlFrame::SetValue(int newValue)
{
   if (newValue < TrackBar->Min)
      newValue = TrackBar->Min;
   else if (newValue > TrackBar->Max)
      newValue = TrackBar->Max;

   TrackBar->Position = newValue;
//   SetValueLabel(TrackBar->Position);
}

void TSliderControlFrame::SetLimits(int newMin, int newMax, int newValue)
{
   if (newMax < newMin || newValue < newMin || newValue > newMax)
      return;  // something wrong, don't bother

   TrackBar->Min = newMin;
   TrackBar->Position = newValue;
   TrackBar->Max = newMax;

   SetValueLabel(TrackBar->Position);
}

void TSliderControlFrame::SetReverseSelectionFlag(bool newFlag)
{
   ReverseSelectionFlag = newFlag;
   TrackBarChange(NULL);
}

int TSliderControlFrame::GetValue()
{
   return TrackBar->Position;
}

void TSliderControlFrame::GetLimits(int &callersMin, int &callersMax)
{
   callersMin = TrackBar->Min;
   callersMax = TrackBar->Max;
}

bool TSliderControlFrame::GetReverseSelectionFlag()
{
   return ReverseSelectionFlag;
}




