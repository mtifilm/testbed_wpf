object SliderControlFrame: TSliderControlFrame
  Left = 0
  Top = 0
  Width = 297
  Height = 44
  TabOrder = 0
  DesignSize = (
    297
    44)
  object NameLabel: TLabel
    Left = 4
    Top = 0
    Width = 26
    Height = 13
    Caption = 'name'
  end
  object ValueLabel: TLabel
    Left = 100
    Top = 0
    Width = 17
    Height = 16
    Caption = '50'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object MinLabel: TLabel
    Left = 4
    Top = 29
    Width = 6
    Height = 13
    Caption = '0'
  end
  object MaxLabel: TLabel
    Left = 266
    Top = 29
    Width = 18
    Height = 13
    Anchors = [akTop, akRight]
    Caption = '100'
  end
  object TrackBar: TTrackBar
    Left = 0
    Top = 15
    Width = 292
    Height = 17
    Anchors = [akLeft, akTop, akRight]
    Max = 100
    PageSize = 10
    Position = 50
    TabOrder = 0
    ThumbLength = 15
    TickMarks = tmTopLeft
    TickStyle = tsNone
    OnChange = TrackBarChange
  end
end
