// ImageFilter.cpp: implementation of the CImageFilter class.
//
/*
$Header: /usr/local/filmroot/Plugins/ImageFilter/ImageFilterTool.cpp,v 1.22.2.25 2009/09/10 18:30:58 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "ImageFilterTool.h"
#include "CPMPFilter.h"
#include "ImageFormat3.h"
#include "JobManager.h"
#include "LineEngine.h"
#include "MTIsleep.h"
#include "PDL.h"
#include "PixelRegions.h"
#include "ToolCommand.h"
#include "ToolCommandTable.h"
#include "ToolProgressMonitor.h"
#include "ToolSystemInterface.h"
#include "ToolUserInputMap.h"
#include "fstream"

#ifdef __BORLANDC__
#include "ImageFilterGUIWin.h"
#else
#include "GuiSgi/ImageFilterGUISGI.h"
#endif

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

static MTI_UINT16 ImageFilterNumber = IMAGEFILTER_TOOL_NUMBER;

// -------------------------------------------------------------------------
// ImageFilter Tool Default Keyboard and Mouse Button Configuration

static CUserInputConfiguration::SConfigItem ImageFilterDefaultConfigItems[] =
{
   // ImageFilter Tool Command             Modifiers      Action
   //                                       + Key
   // ----------------------------------------------------------------
 { EXEC_BUTTON_PREVIEW_FRAME,                " D                KeyDown    " },
 { EXEC_BUTTON_PREVIEW_NEXT_FRAME,           " Ctrl+D           KeyDown    " },
 { EXEC_BUTTON_PREVIEW_ALL,                  " Shift+D          KeyDown    " },
 { EXEC_BUTTON_RENDER_FRAME,                 " G                KeyDown    " },
 { EXEC_BUTTON_RENDER_NEXT_FRAME,            " Ctrl+G           KeyDown    " },
 { EXEC_BUTTON_RENDER_ALL,                   " Shift+G          KeyDown    " },
 { EXEC_BUTTON_STOP,                         " Ctrl+Space       KeyDown    " },
 { EXEC_BUTTON_PAUSE_OR_RESUME,              " Space            KeyDown    " },
 { EXEC_BUTTON_SET_RESUME_TC,                " Return           KeyDown    " },
 { EXEC_BUTTON_CAPTURE_PDL,                  " ,                KeyDown    " },
 { EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS,       " Shift+,          KeyDown    " },

 { IF_CMD_SELECT_APERTURE_CORRECTION,        " 1                KeyDown    " },
 { IF_CMD_TOGGLE_APERTURE_RADIUS_GANG,       " 2                KeyDown    " },
 { IF_CMD_SELECT_BLUR,                       " 3                KeyDown    " },

 { IF_CMD_TOGGLE_FIX,                        " T                KeyDown    " },
 { IF_CMD_REJECT_FIX,                        " A                KeyDown    " },

};

static CUserInputConfiguration *ImageFilterDefaultUserInputConfiguration
= new CUserInputConfiguration(sizeof(ImageFilterDefaultConfigItems)
                              /sizeof(CUserInputConfiguration::SConfigItem),
                               ImageFilterDefaultConfigItems);

// -------------------------------------------------------------------------
// ImageFilter Tool Command Table

static CToolCommandTable::STableEntry ImageFilterCommandTableEntries[] =
{
   // ImageFilter Tool Command            ImageFilter Tool Command String
   // -------------------------------------------------------------

 { EXEC_BUTTON_PREVIEW_FRAME,                "EXEC_BUTTON_PREVIEW_FRAME" },
 { EXEC_BUTTON_PREVIEW_ALL,                  "EXEC_BUTTON_PREVIEW_ALL" },
 { EXEC_BUTTON_RENDER_FRAME,                 "EXEC_BUTTON_RENDER_FRAME" },
 { EXEC_BUTTON_RENDER_ALL,                   "EXEC_BUTTON_RENDER_ALL" },
 { EXEC_BUTTON_STOP,                         "EXEC_BUTTON_STOP" },
 { EXEC_BUTTON_PAUSE_OR_RESUME,              "EXEC_BUTTON_PAUSE_OR_RESUME" },
 { EXEC_BUTTON_SET_RESUME_TC,                "EXEC_BUTTON_SET_RESUME_TC" },
 { EXEC_BUTTON_CAPTURE_PDL,                  "EXEC_BUTTON_CAPTURE_PDL" },
 { EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS,       "EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS" },

 { IF_CMD_SELECT_APERTURE_CORRECTION,        "IF_CMD_SELECT_APERTURE_CORRECTION" },
 { IF_CMD_SELECT_BLUR,                       "IF_CMD_SELECT_BLUR" },
 { IF_CMD_TOGGLE_APERTURE_RADIUS_GANG,       "IF_CMD_TOGGLE_APERTURE_RADIUS_GANG" },
 { IF_CMD_TOGGLE_FIX,                        "IF_CMD_TOGGLE_FIX" },

};

static CToolCommandTable *ImageFilterCommandTable
   = new CToolCommandTable(sizeof(ImageFilterCommandTableEntries)
                           /sizeof(CToolCommandTable::STableEntry),
                           ImageFilterCommandTableEntries);

// -------------------------------------------------------------------------
// Static variables for CImageFilterParameters

const string CImageFilterParameters::filterTypeAttr = "FilterType";
const string CImageFilterParameters::filterTypeApertureCorrect = "ApertureCorrect";
const string CImageFilterParameters::filterTypeBlur = "Blur";
const string CImageFilterParameters::amountAttr = "Amount";
const string CImageFilterParameters::horizontalRadiusAttr = "HorizontalRadius";
const string CImageFilterParameters::verticalRadiusAttr = "VerticalRadius";
const string CImageFilterParameters::thresholdAttr = "Threshold";

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CImageFilterTool::CImageFilterTool(const string &newToolName,
                                   MTI_UINT32 **newFeatureTable)
: CToolObject(newToolName, newFeatureTable), toolSetupHandle(-1),
  trainingToolSetupHandle(-1)
{
   // TTT Tool-specific constructors go here
}

CImageFilterTool::~CImageFilterTool()
{
}


// ===================================================================
//
// Function:    toolInitialize
//
// Description: Called when the ToolManager wants to initialize
//              the ImageFilter Tool
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================

int CImageFilterTool::toolInitialize(CToolSystemInterface *newSystemAPI)
{
   int retVal;

   // Initialize the CImageFilterTool's base class, i.e., CToolObject
   // This must be done before the CImageFilterTool initializes itself
   retVal = initBaseToolObject(ImageFilterNumber, newSystemAPI,
                               ImageFilterCommandTable,
                               ImageFilterDefaultUserInputConfiguration);
   if (retVal != 0)
      {
      // ERROR: initBaseToolObject failed
      TRACE_0(errout << "Error in ImageFilter Tool, initBaseToolObject failed "
                     << retVal);
      return retVal;
      }

   // Tool-specific initialization here

   // Create the Tool's GUI
   CreateImageFilterGUI();

   // Update the tool's GUI to match the tool processing initial state
   UpdateExecutionGUI(GetToolProcessingControlState(), false);

   return 0;

}   // end toolInitialize

// ===================================================================
//
// Function:    toolActivate
//
// Description: Called when a tool is being activated
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CImageFilterTool::toolActivate()
{
   // First
   int retVal = CToolObject::toolActivate();
   if (retVal != 0)
   {
      return retVal;
   }

   getSystemAPI()->SetGOVToolProgressMonitor(GetToolProgressMonitor());

   // Make sure displayed frame is clean
   getSystemAPI()->refreshFrameCached();

   JobManager jobManager;
   jobManager.SetCurrentToolCode("fi");

	getSystemAPI()->SetToolNameForUserGuide("FilterTool");

   // Enable mask tool
   getSystemAPI()->SetMaskAllowed(true);

   return 0;
}

// ===================================================================
//
// Function:    toolDeactivate
//
// Description: Called when a tool is being deactivated
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CImageFilterTool::toolDeactivate()
{
   getSystemAPI()->AcceptGOV();   // Auto-accept GOV if pending

   // Get rid of any review regions that might be present on the screen
   getSystemAPI()->ClearProvisional(true);

   DestroyAllToolSetups(true);

   GetToolProgressMonitor()->SetIdle();
   getSystemAPI()->SetGOVToolProgressMonitor(NULL);

   // Last
   return CToolObject::toolDeactivate();
}

// ===================================================================
//
// Function:    toolShutdown
//
// Description: Called when the Tool Manager wants to shut down the
//              ImageFilter Tool, typically when the main program is
//              shutting down.
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================
int CImageFilterTool::toolShutdown()
{
	int retVal;

	getSystemAPI()->AcceptGOV(); // Auto-accept GOV if pending

	// Destroy ImageFilter's GUI
	DestroyImageFilterGUI();

	GImageFilter = NULL; // Say we are destroyed

	// Shutdown the ImageFilter Tool's base class, i.e., CToolObject.
	// This must be done after the ImageFilter shuts down itself
	retVal = shutdownBaseToolObject();
	if (retVal != 0)
	{
		// ERROR: shutdownBaseToolObject failed
		return retVal;
	}

	return retVal;
}


bool CImageFilterTool::DoesToolPreserveHistory()
{
   return false;
}


CToolProcessor* CImageFilterTool::makeToolProcessorInstance(
                                           const bool *newEmergencyStopFlagPtr)
{
   return new CImageFilterProc(GetToolNumber(), GetToolName(), getSystemAPI(),
                              newEmergencyStopFlagPtr,
                              GetToolProgressMonitor());
}

// ===================================================================
//
// Function:    onToolCommand
//
// Description: ImageFilter Tool's Command Handler
//
// Arguments:   CToolCommand &toolCommand
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================

int CImageFilterTool::onToolCommand(CToolCommand &toolCommand)
{
   if (IsDisabled())
      return TOOL_RETURN_CODE_NO_ACTION;

   int commandNumber = toolCommand.getCommandNumber();
   int Result = TOOL_RETURN_CODE_NO_ACTION;
   TRACE_3(errout << "ImageFilter Tool command number " << commandNumber);

   CToolSystemInterface *systemAPI = getSystemAPI();

   // Dispatch loop for ImageFilter Tool commands

   switch (commandNumber)
      {
      case EXEC_BUTTON_PREVIEW_FRAME:
      case EXEC_BUTTON_PREVIEW_NEXT_FRAME:
      case EXEC_BUTTON_PREVIEW_ALL:
      case EXEC_BUTTON_RENDER_FRAME:
      case EXEC_BUTTON_RENDER_NEXT_FRAME:
      case EXEC_BUTTON_RENDER_ALL:
      case EXEC_BUTTON_STOP:
      case EXEC_BUTTON_PAUSE_OR_RESUME:
      case EXEC_BUTTON_SET_RESUME_TC:
      case EXEC_BUTTON_CAPTURE_PDL:
      case EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS:
      {
         bool consumed = RunExecButtonsCommand((EExecButtonId) commandNumber);
         if (consumed)
            Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                      TOOL_RETURN_CODE_PROCESSED_OK);
         break;
      }

      case IF_CMD_SELECT_APERTURE_CORRECTION:
         SelectApertureCorrection();
         break;

      case IF_CMD_SELECT_BLUR:
         SelectBlur();
         break;

      case IF_CMD_TOGGLE_APERTURE_RADIUS_GANG:
         ToggleApertureRadiusGang();
         break;

		case IF_CMD_TOGGLE_FIX :
			if (systemAPI->IsProvisionalPending())
            {
            // Provisional is pending, so this command is interpreted
            // as toggle
            systemAPI->ToggleProvisional();
            Result = TOOL_RETURN_CODE_PROCESSED_OK
                     | TOOL_RETURN_CODE_EVENT_CONSUMED;
            }
         else
            {
            Result = TOOL_RETURN_CODE_NO_ACTION;
            }
         break;
//----------------------------------------------------------------------

		case IF_CMD_REJECT_FIX :
			if (getSystemAPI()->IsProvisionalPending())
			{
				getSystemAPI()->RejectProvisional(true);  // true = show the frame
				GetToolProgressMonitor()->SetStatusMessage("");
			}

			break;

      default:
         Result = TOOL_RETURN_CODE_NO_ACTION;
         break;
      }

   return(Result);

} // onToolCommand


// ===================================================================
//
// Function:    onRedraw
//
// Description:
//
// Arguments:   None
//
// Returns:
//
// ===================================================================
int CImageFilterTool::onRedraw(int frameIndex)
{
  return(TOOL_RETURN_CODE_EVENT_CONSUMED);
}

int CImageFilterTool::onChangingClip()
{
   // This function is called just before the current clip is unloaded.

   if (IsDisabled())                      // !IsActive() is OK!!!
      return TOOL_RETURN_CODE_NO_ACTION;

   // I'm pretty sure this is unnecessary - GOVTool has own onChangingClip
   getSystemAPI()->AcceptGOV();   // Auto-accept GOV if pending

   getSystemAPI()->ClearProvisional(false);

   DestroyAllToolSetups(false);

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

int CImageFilterTool::onNewClip()
{
   // This function is called after a new clip is loaded.

   if (!(IsLicensed() && IsShowing())) //NOT IsDisabled(), might want to Enable!
      return TOOL_RETURN_CODE_NO_ACTION;

   // Do we need to disable or re-enable the tool?
   if (isItOkToUseToolNow())
      getSystemAPI()->EnableTool();
   else
      getSystemAPI()->DisableTool(getToolDisabledReason());

   // Make sure we start fresh - probably unnecessary
   getSystemAPI()->ClearProvisional(false);

   // This allows the tool to notify the gui
   ClipChange.Notify();

   // Do this after the disabled check
   UpdateExecutionButtons(TOOL_CONTROL_STATE_STOPPED, false);

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

int CImageFilterTool::onNewMarks()
{
   if (IsDisabled() || !IsActive())
      return TOOL_RETURN_CODE_NO_ACTION;

   // If necessary, clear "Paused" state from GUI
   // mbraca says " I don't understand this comment at all... why would
   // changing the marks affect PAUSE? In any case, the Notify() below
   // updates the execution buttons, but only if the tool is STOPPED!
   // So I'm nuking this line!!
   //UpdateExecutionButtons(TOOL_CONTROL_STATE_STOPPED, false);

   // Huh?? Why??
   //DestroyAllToolSetups(false);

   // This allows the tool to notify the gui
   MarksChange.Notify();

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

int CImageFilterTool::onChangingFraming()
{
   if (IsDisabled() || !IsActive())
      return TOOL_RETURN_CODE_NO_ACTION;

   ClearProvisional();

   // If necessary, clear "Paused" state from GUI
   UpdateExecutionButtons(TOOL_CONTROL_STATE_STOPPED, false);

   DestroyAllToolSetups(false);

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

void CImageFilterTool::DestroyAllToolSetups(bool notIfPaused)
{
   // DRS has three tool setups: DRS, Review Tool and Move Tool.
   // This function should not be called unless all of the tool setups
   // are stopped and there is no provisional pending.

   int activeToolSetup = getSystemAPI()->GetActiveToolSetupHandle();

   if (toolSetupHandle >= 0)
      {
      bool isPaused
      = (getSystemAPI()->GetToolSetupStatus(toolSetupHandle)==AT_STATUS_PAUSED);
      if (!(notIfPaused && isPaused))
         {
         if (toolSetupHandle == activeToolSetup)
            getSystemAPI()->SetActiveToolSetup(-1);
         getSystemAPI()->DestroyToolSetup(toolSetupHandle);
         toolSetupHandle = -1;
         }
      }

   if (trainingToolSetupHandle >= 0)
      {
      if (trainingToolSetupHandle == activeToolSetup)
         getSystemAPI()->SetActiveToolSetup(-1);
      getSystemAPI()->DestroyToolSetup(trainingToolSetupHandle);
      trainingToolSetupHandle = -1;
      }
}

// ===================================================================
//
// Function:    toolShow
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CImageFilterTool::toolShow()
{
   ShowImageFilterGUI();

   if (!isItOkToUseToolNow())
      getSystemAPI()-> DisableTool(getToolDisabledReason());

   // Just in case these did change.... could be more clever about it?
   onNewClip();
   onNewMarks();

   return 0;
}

bool CImageFilterTool::IsShowing(void)
{
   return(IsToolVisible());
}

// ===================================================================
//
// Function:    toolHide
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CImageFilterTool::toolHide()
{
   HideImageFilterGUI();
   doneWithTool();
   return 0;
}

// ===================================================================
//
// Function:    ProcessTrainingFrame
//
// Description:
//
// Arguments:   None
//
// Returns:
//
// ===================================================================
int CImageFilterTool::ProcessTrainingFrame()
{
   int retVal;
   CAutoErrorReporter autoErr("CImageFilterTool::ProcessTrainingFrame",
                              AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   if (getSystemAPI()->GetActiveToolSetupStatus() == AT_STATUS_RUNNING)
      {
      // Something is running, so can't do Training processing right now
      return -1;
      }

   const CImageFormat *imageFormat = getSystemAPI()->getVideoClipImageFormat();
   if (imageFormat == 0)
      return -1;  // No image format, probably clip isn't loaded

   int pixelsPerLine = imageFormat->getPixelsPerLine();

   // If image size greater than 3K, then can only have one tool setup
   // around at a time because of memory constraints
   if (pixelsPerLine > 3072 && toolSetupHandle >= 0
       && toolSetupHandle == getSystemAPI()->GetActiveToolSetupHandle())
      {
      DestroyAllToolSetups(false);
      }

   // Check if we have a tool setup yet
   if (trainingToolSetupHandle < 0)
      {
      // Create the tool setup
      CToolIOConfig ioConfig(1, 1);
      ioConfig.processingThreadCount = 2;  // Number of threads for ImageFilter
                                           // algorithm.  Hardcoded at 2, really
                                           // should be in an ini file.
      trainingToolSetupHandle = getSystemAPI()->MakeSimpleToolSetup("Filter",
                                                   TOOL_SETUP_TYPE_TRAINING,
                                                            &ioConfig);
      if (trainingToolSetupHandle < 0)
         {
         autoErr.errorCode = -1;
         autoErr.msg << "ImageFilter internal error: CNavSystemInterface::MakeSimpleToolSetup failed";
         return -1;
        }
      }

   // Set the active tool setup (this is different than the active tool)
   retVal = getSystemAPI()->SetActiveToolSetup(trainingToolSetupHandle);
   if (retVal != 0)
      {
      autoErr.errorCode = retVal;
      autoErr.msg << "ImageFilter internal error: SetActiveToolSetup("
                     << toolSetupHandle  << ") failed with return code " << retVal;
      return retVal;
      }

   // Set processing frame range to the current frame index
   CToolFrameRange frameRange(1, 1);
   frameRange.inFrameRange[0].randomAccess = true;
   frameRange.inFrameRange[0].inFrameIndex
                                          = getSystemAPI()->getLastFrameIndex();
   frameRange.inFrameRange[0].outFrameIndex
                                  = frameRange.inFrameRange[0].inFrameIndex + 1;
   retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
   if (retVal != 0)
      {
         autoErr.errorCode = retVal;
         autoErr.msg << "ImageFilter internal error: SetToolFrameRange"
                     << " failed with return code " << retVal;
      return retVal;
      }

   // Set provisional highlighting flag
   getSystemAPI()->SetProvisionalHighlight(highlightFlag);

   // Set ImageFilter tool's parameters
   CImageFilterToolParameters *toolParams = new CImageFilterToolParameters; // deleted by tool infrastructure
   GatherParameters(*toolParams);
   retVal = getSystemAPI()->SetToolParameters(toolParams);
   if (retVal != 0)
      {
      autoErr.errorCode = retVal;
      autoErr.msg << "ImageFilter internal error: SetToolParameters"
                  << " failed with return code " << retVal;
      return retVal;
      }

   // Start the image processing a-runnin'
   getSystemAPI()->RunActiveToolSetup();

   return 0;
}

// ===================================================================
//
// Function:    RunFrameProcessing
//
// Description:
//
// Arguments:   None
//
// Returns:
//
// ===================================================================
int CImageFilterTool::RunFrameProcessing(int newResumeFrame)
{
   int retVal;
   CAutoErrorReporter autoErr("CImageFilterTool::RunFrameProcessing",
                              AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   if (IsDisabled())
      {
      autoErr.errorCode = -999;
      autoErr.msg << "ImageFilter internal error, tried to run when disabled ";
      return autoErr.errorCode;
      }

// TBD:  Fix correctly.  The call to refreshFrame partially fixes
//       the problem with the timeline slider getting stuck during
//       tool processing
    getSystemAPI()->refreshFrame();

   const CImageFormat *imageFormat = getSystemAPI()->getVideoClipImageFormat();
   if (imageFormat == 0)
      return -1;  // No image format, probably clip isn't loaded

   int pixelsPerLine = imageFormat->getPixelsPerLine();

   // If image size greater than 3K, then can only have one tool setup
   // around at a time because of memory constraints
   if (pixelsPerLine > 3072 && trainingToolSetupHandle >= 0
       && trainingToolSetupHandle == getSystemAPI()->GetActiveToolSetupHandle())
      {
      DestroyAllToolSetups(false);
      }

   // Check if we have a tool setup yet
   if (toolSetupHandle < 0)
      {
      // Create the tool setup
      CToolIOConfig ioConfig(1, 1);
      ioConfig.processingThreadCount = 2;  // Number of threads for ImageFilter
                                           // algorithm.  Hardcoded at 2, really
                                           // should be in an ini file.
      toolSetupHandle = getSystemAPI()->MakeSimpleToolSetup("Filter",
                                                   TOOL_SETUP_TYPE_MULTI_FRAME,
                                                            &ioConfig);
      if (toolSetupHandle < 0)
         {
         autoErr.errorCode = -1;
         autoErr.msg << "ImageFilter internal error: CNavSystemInterface::MakeSimpleToolSetup failed";
         return -1;
         }
      }

   // Set the active tool setup (this is different than the active tool)
   retVal = getSystemAPI()->SetActiveToolSetup(toolSetupHandle);
   if (retVal != 0)
      {
      autoErr.errorCode = retVal;
      autoErr.msg << "ImageFilter internal error: SetActiveToolSetup("
                     << toolSetupHandle  << ") failed with return code " << retVal;
      return retVal;
      }

   if (newResumeFrame < 0)
      {
      // Starting from Stop, so set processing frame range to in and out marks
      CToolFrameRange frameRange(1, 1);
      frameRange.inFrameRange[0].randomAccess = false;
      int markIn = getSystemAPI()->getMarkIn();
      int markOut = getSystemAPI()->getMarkOut();
      if (markIn < 0 || markOut < 0)
         {
         autoErr.traceAction = AUTO_ERROR_NO_TRACE;
         autoErr.errorCode = -1;
         autoErr.msg << "Mark In and Out must be set to Preview or Render";
         return -1;
         }
      else if(markOut <= markIn)
         {
         autoErr.traceAction = AUTO_ERROR_NO_TRACE;
         autoErr.errorCode = -1;
         autoErr.msg << "Mark Out must be beyond Mark In";
         return -1;
         }
      frameRange.inFrameRange[0].inFrameIndex = markIn;
      frameRange.inFrameRange[0].outFrameIndex = markOut;
      retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
      if (retVal != 0)
         {
         autoErr.errorCode = retVal;
         autoErr.msg << "ImageFilter internal error: SetToolFrameRange"
                     << " failed with return code " << retVal;
         return retVal;
         }
      }
   else
      {
      // Resuming from Pause state, so set processing frame range
      // from newResumeFrame to out mark
      CToolFrameRange frameRange(1, 1);
      frameRange.inFrameRange[0].randomAccess = false;
      int markOut = getSystemAPI()->getMarkOut();
      if (markOut < 0)
         {
         autoErr.traceAction = AUTO_ERROR_NO_TRACE;
         autoErr.errorCode = -1;
         autoErr.msg << "Mark Out must be set to Preview or Render";
         return -1;
         }
      else if(markOut <= newResumeFrame)
         {
         autoErr.traceAction = AUTO_ERROR_NO_TRACE;
         autoErr.errorCode = -1;
         autoErr.msg << "Mark Out must be beyond the Resume frame";
         return -1;
         }
      frameRange.inFrameRange[0].inFrameIndex = newResumeFrame;
      frameRange.inFrameRange[0].outFrameIndex = markOut;
      retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
      if (retVal != 0)
         {
         autoErr.errorCode = retVal;
         autoErr.msg << "ImageFilter internal error: SetToolFrameRange"
                     << " failed with return code " << retVal;
         return retVal;
         }
      }

   // Set provisional highlighting flag
   getSystemAPI()->SetProvisionalHighlight(highlightFlag);

   // Set render/preview
   getSystemAPI()->SetProvisionalRender(renderFlag);

   // Set ImageFilter's tool parameters
   CImageFilterToolParameters *toolParams = new CImageFilterToolParameters; // deleted by tool infrastructure
   GatherParameters(*toolParams);
   retVal = getSystemAPI()->SetToolParameters(toolParams);
   if (retVal != 0)
      {
      autoErr.errorCode = retVal;
      autoErr.msg << "ImageFilter internal error: SetToolParameters"
                  << " failed with return code " << retVal;
      return retVal;
      }

   // Start the image processing a-runnin'
   getSystemAPI()->RunActiveToolSetup();

   return 0;
}

int CImageFilterTool::GatherParameters(CImageFilterToolParameters &toolParams)
{
   toolParams.SetRegionOfInterest(getSystemAPI()->GetMaskRoi());

   GatherGUIParameters(toolParams.imageFilterParameters);

   return 0;
}

int CImageFilterTool::StopFrameProcessing()
{
   int retVal;

   // Kludge so we don't get stuck if the following sequence is followed
   //   Preview -> Pause -> Preview 1 -> Stop
   // We really need some better logic here, as this prevents Stop button
   // from acting on Preview 1 processing
   if (toolSetupHandle >= 0)
      getSystemAPI()->SetActiveToolSetup(toolSetupHandle);

   retVal = CToolObject::StopFrameProcessing();
   if (retVal != 0)
      return retVal;

   return 0;
}

void CImageFilterTool::UpdateExecutionGUI(EToolControlState toolProcessingControlState,
                                     bool processingRenderFlag)
{
   UpdateExecutionButtons(toolProcessingControlState, processingRenderFlag);
}

//---------------------------------------------------------------------------

int CImageFilterTool::onCapturePDLEntry(CPDLElement &pdlEntryToolParams)
{
   if (IsDisabled())
      return 0;

   CaptureGUIParameters(pdlEntryToolParams);

   return 0;
}

int CImageFilterTool::onGoToPDLEntry(CPDLEntry &pdlEntry)
{
   if (IsDisabled())
      return 0;

   int retVal;

   CPDLEntry_Tool *pdlEntryTool = pdlEntry.toolList[0];
   CPDLElement *toolAttribRoot = pdlEntryTool->parameters;

   CImageFilterParameters toolParams;

   for (unsigned int i = 0; i < toolAttribRoot->GetChildElementCount(); ++i)
      {
      CPDLElement *toolAttribs = toolAttribRoot->GetChildElement(i);
      if (toolAttribs->GetElementName() == "ImageFilterParameters")
         {
         toolParams.ReadPDLEntry(toolAttribs);
         break;
         }
      }

   SetGUIParameters(toolParams);

   return 0;
}

//---------------------------------------------------------------------------

void CImageFilterTool::SetHighlightFlag(bool newHighlightFlag)
{
   if (highlightFlag != newHighlightFlag)
      {
      highlightFlag = newHighlightFlag;

      if (getSystemAPI()->GetActiveToolSetupStatus() == AT_STATUS_RUNNING
          || getSystemAPI()->IsProvisionalPending())
         {
         // There is a processed image on the screen, so update the
         // Show Highlight/Fix mode immediately
         getSystemAPI()->SetProvisionalHighlight(highlightFlag);
         }
      }
}

//---------------------------------------------------------------------------

void CImageFilterTool::ClearProvisional()
{
   getSystemAPI()->AcceptGOV();   // Auto-accept GOV if pending

   // Clear Provisional frame if there is one and we are not running the ImageFilter
   if (getSystemAPI()->GetActiveToolSetupStatus() != AT_STATUS_RUNNING
       && getSystemAPI()->IsProvisionalPending())
      {
      getSystemAPI()->ClearProvisional(true);
      }
}

//---------------------------------------------------------------------------

void CImageFilterTool::WriteSettings(CIniFile *iniFile, const string &section)

//  This writes the ini files, saving the Tool's platform-independent
//  parameters.  Called by Tool's platform-dependent GUI.
//
//******************************************************************************
{
}

//-----------------------------------------------------------------------------

void CImageFilterTool::ReadSettings(CIniFile *iniFile, const string &section)

//  This reads the init file and sets the tool's platform-independent
//  parameters.  Called by the Tool's platform-dependent GUI.
//******************************************************************************
{
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Construction/Destruction
///////////////////////////////////////////////////////////////////////////////

CImageFilterProc::CImageFilterProc(int newToolNumber, const string &newToolName,
                                   CToolSystemInterface *newSystemAPI,
                                   const bool *newEmergencyStopFlagPtr,
                                   IToolProgressMonitor *newToolProgressMonitor)
 : CToolProcessor(newToolNumber, newToolName, 1, 1, newSystemAPI,
                  newEmergencyStopFlagPtr, newToolProgressMonitor),
   filter(0)
{
   StartProcessThread(); // Required by Tool Infrastructure
}

CImageFilterProc::~CImageFilterProc()
{
  delete filter;
  filter = NULL;
}

///////////////////////////////////////////////////////////////////////////////
//
///////////////////////////////////////////////////////////////////////////////


int CImageFilterProc::SetIOConfig(CToolIOConfig *toolIOConfig)
{
   // Make sure the number of Grain algorithm processing threads
   // is reasonable
   processingThreadCount = toolIOConfig->processingThreadCount;
   if (processingThreadCount < 1)
      processingThreadCount = 1;
   else if (processingThreadCount > IMAGE_FILTER_MAX_THREAD_COUNT)
      processingThreadCount = IMAGE_FILTER_MAX_THREAD_COUNT;

   // Tell Tool Infrastructure, via CToolProcessor supertype, about the
   // number of input and output frames
   SetInputMaxFrames(0, 1, 1);   // Input Port 0 = 1 frame per iteration
   SetOutputModifyInPlace(0, 0, false);
   SetOutputMaxFrames(0, 1);  // Output Port 0 = 1 frame

   return 0;
}

int CImageFilterProc::SetFrameRange(CToolFrameRange *toolFrameRange)
{
   CToolFrameRange::SFrameRange &frameRange = toolFrameRange->inFrameRange[0];
   inFrameIndex = frameRange.inFrameIndex;
   outFrameIndex = frameRange.outFrameIndex;

   // Calculate the number of iterations
   iterationCount = outFrameIndex - inFrameIndex;

   return 0;
}

int CImageFilterProc::SetParameters(CToolParameters *toolParams)
{
   int retVal;

   CImageFilterToolParameters *imageFilterToolParams
                        = static_cast<CImageFilterToolParameters*>(toolParams);

//   maskROI = toolParams->GetRegionOfInterest();

   // Make a private copy of the parameters for later reference
   // Caller's toolParam is deleted after this function returns
   imageFilterParams = imageFilterToolParams->imageFilterParameters;

   return 0;
}

int CImageFilterProc::BeginProcessing(SToolProcessingData &procData)
{
   int retVal;

   // Image Filter does not save to history, since it is a "full frame" tool
   // TTT: this needs to be revisited when 1) doing small regions 2) full frame
   //      history is implemented
   SetSaveToHistory(false);

   // Allocate the filter algorithm
   filter = new CUnsharpFilter_MT;

   filter->Initialize(processingThreadCount);

   // Initialize the filter algorithm a little bit
   const CImageFormat *imageFormat = GetSrcImageFormat(0);

   filter->setImageSize(imageFormat->getTotalFrameWidth(),
                        imageFormat->getTotalFrameHeight());

   MTI_UINT16 cvMin[MAX_COMPONENT_COUNT], cvMax[MAX_COMPONENT_COUNT];
   MTI_UINT16 cvFill[MAX_COMPONENT_COUNT];
   imageFormat->getComponentValueMin(cvMin);
   imageFormat->getComponentValueMax(cvMax);
   imageFormat->getComponentValueFill(cvFill);
   int componentCount = imageFormat->getComponentCountExcAlpha();
   filter->setImageComponent(componentCount, 0, cvMin, cvMax, cvFill);

   filter->setActiveComponent(0);

   // Set the Horizontal+Vertical Intervals
   long lHorizIntPixels, lVertIntRows;
   lHorizIntPixels = imageFormat->getHorizontalIntervalPixels();
   lVertIntRows = imageFormat->getVerticalIntervalRows();
   filter->setHorizAndVertIntervals(lHorizIntPixels,lVertIntRows);

   // Based on the caller's tool parameters, figure out the parameters
   // that the algorithm wants to see.

   /* A note on the parameters for the unsharp masking algorithm
    * that we're using for Aperture Correction (ie sharpening or
    * blurring):
    * 1) the GUI radius parameter will be converted to sigma,
    *    the normalized width of the Gaussian blur which lives
    *    in the interval [0,1].
    * 2) The true radius of the Gaussian kernel will be set to 3.0
    *    since (according to KM) we should only be interested in making
    *    sub-pixel changes in the aperture. Never set the true radius < 1.0!
    * 3) The amount parameter will be converted to a sharpness
    *    that lives in the interval [-1.0,1.0] with negative
    *    values for blurring and positive for sharpening.
    * 4) The contrast threshold parameter needs to be converted from
    *    percent to float.
    * Right now the GUI parameters (amount, radius, threshold) exist
    * in [0,100] and the threshold is only used for sharpening.
    * At some pont we will want to allow RadiusX != RadiusY
    ********************************************************************/

   float fRadiusX, fRadiusY, fSharpness;

   fRadiusX = fRadiusY = 3.0;
   float fHSigma = ((float)(imageFilterParams.horizontalRadius + 1))/100.0;
   float fVSigma = ((float)(imageFilterParams.verticalRadius + 1))/100.0;

   if (imageFilterParams.filterType == IMAGE_FILTER_TYPE_APERTURE_CORRECT)
      {
      // For sharpening, map user's amount 0 to 100 into fSharpness 0 to 1.5
      fSharpness = (float)imageFilterParams.amount / 66.66667;
      }
   else if (imageFilterParams.filterType == IMAGE_FILTER_TYPE_BLUR)
      {
      // For softening, map user's amount 0 to 100 into fSharpness 0 to -1
      fSharpness = (float)imageFilterParams.amount / (-100.0);
      }
   else
      {
      return -1;   // request for unknown or invalid filter
      }
   for (int i = 0; i < componentCount; ++i)
      {
      faSharpness[i] = fSharpness;
      }

   // Map user's threshold 0 to 100 into fThreshold 0 to 1
   float fThreshold = (float)imageFilterParams.threshold / 100.0;

   filter->setActiveComponent(0);

   filter->setRadii(false, fRadiusX, fRadiusY);   // TTT watch out for non-square pixels
   filter->setSharpness(faSharpness[0]);
   filter->setSigma(fHSigma, fVSigma);
   filter->setThreshold(fThreshold);

   if (!GetSystemAPI()->IsMaskAnimated())
      {
      retVal = GetSystemAPI()->GetMaskRoi(inFrameIndex, maskROI);
      if (retVal != 0)
         return retVal;

      RECT roiRect = maskROI.getBlendBox();
      filter->setROI(roiRect.left, roiRect.top, roiRect.right + 1,
                     roiRect.bottom + 1);

      retVal = (int)filter->allocate();
      if (retVal != 0)
         return retVal;

      retVal = filter->setRegionOfInterestPtr(maskROI.getBlendPtr(),
                                              maskROI.getLabelPtr());
      if (retVal)
         return retVal;
      }

   // This tool processes one frame per iteration
   GetToolProgressMonitor()->SetFrameCount(iterationCount);
   GetToolProgressMonitor()->SetStatusMessage("Processing");
   GetToolProgressMonitor()->StartProgress();

   // Success, continue
   return 0;
}

int CImageFilterProc::BeginIteration(SToolProcessingData &procData)
{
   int inputFrameIndexList[1];
   inputFrameIndexList[0] = inFrameIndex + procData.iteration;
   // inputFrameIndexList now contains the frame indices we want as input
   SetInputFrames(0, 1, inputFrameIndexList);

   // These are the frames that will be the output
   int outputFrameIndexList[1];
   outputFrameIndexList[0] = inFrameIndex + procData.iteration;
   SetOutputFrames(0, 1, outputFrameIndexList);

   return 0;
}

int CImageFilterProc::GetIterationCount(SToolProcessingData &procData)
{
   // This function is called by the Tool Infrastructure so it knows
   // how many processing iterations to do before stopping

   return iterationCount;
}


int CImageFilterProc::DoProcess(SToolProcessingData &procData)
{
   int retVal;

   const CImageFormat *imageFormat = GetSrcImageFormat(0);

   // Get the input image buffer for this iteration.
   CToolFrameBuffer *inToolFrameBuffer = GetRequestedInputFrame(0, 0);
   MTI_UINT16 *imageData = inToolFrameBuffer->GetVisibleFrameBufferPtr();

   filter->setDataPointer(imageData);
	filter->setSourceDataTypeIsHalf(inToolFrameBuffer->ContainsFloatData());

   if (GetSystemAPI()->IsMaskAnimated())
      {
      int inFrameIndex = inToolFrameBuffer->GetFrameIndex();
      retVal = GetSystemAPI()->GetMaskRoi(inFrameIndex, maskROI);
      if (retVal != 0)
         return retVal;

      RECT roiRect = maskROI.getBlendBox();
      filter->setROI(roiRect.left, roiRect.top, roiRect.right + 1,
                     roiRect.bottom + 1);

      // Deallocate before we allocate.  This is probably horribly
      // inefficient and memory-fragmenting, but it is what works with
      // the filter.  Might be better to just allocate frame-sized
      // buffers once rather than try to be space-efficient when the
      // regions are small
      filter->deallocate();

      retVal = (int)filter->allocate();
      if (retVal != 0)
         return retVal;

      retVal = filter->setRegionOfInterestPtr(maskROI.getBlendPtr(),
                                              maskROI.getLabelPtr());
      if (retVal)
         return retVal;
      }

   int componentCount = imageFormat->getComponentCountExcAlpha();

   // Save the shape of the region-of-interest so the filtered pixels
   // can be copied to the hidden fields.  The pixel values are not copied
   // into the Original Values Pixel Region List
   // TTT When we have full-frame history, we will need to revisit this code
   CPixelRegionList* originalValues
                    = inToolFrameBuffer->GetOriginalValuesPixelRegionListPtr();

   // Use Region of Interest blend mask to determine which pixels to
   // put in the original values pixel region list.
   // NB: We probably could be more efficient for single rects, but
   //     this will do the trick for now
   originalValues->Add(maskROI.getBlendBox(), maskROI.getBlendPtr(),
                       NULL, imageFormat->getPixelsPerLine(),
                       imageFormat->getLinesPerFrame(), componentCount, 0xff);

   EPixelComponents pixelComponents = imageFormat->getPixelComponents();
   if (pixelComponents == IF_PIXEL_COMPONENTS_Y
   || pixelComponents == IF_PIXEL_COMPONENTS_YYY)
      {
         filter->setActiveComponent(0);
         filter->setSharpness(faSharpness[0]);
         filter->applyFilter();

         int componentsPerFrame = imageFormat->getPixelsPerLine() * imageFormat->getLinesPerFrame() * 3;
         for (int component = 0; component < componentsPerFrame; component += 3)
         {
            imageData[component + 2] = imageData[component + 1] = imageData[component];
         }
      }
   else
      {
      if (pixelComponents == IF_PIXEL_COMPONENTS_YUV444)
         {
         // For YUV images, filter only the Y (luminance) component and do
         // not filter the U or V (chroma) components.  This is based on the
         // assumption that most of the contrast/spatial information is in
         // the luminance channel and chroma channels are half-resolution.
         // It is also faster and possibly avoids color artifacts and
         // thresholding issues.
         componentCount = 1;
         }
      // Iterate over the number of components in a pixel, applying the
      // filter to each component separately.
      // TTT: this is probably inefficient for large images, but good enough
      //      for a first pass.  We can optimize if it becomes necessary.
      for(int component = 0; component < componentCount; ++component)
         {
         filter->setActiveComponent(component);
         filter->setSharpness(faSharpness[component]);
         filter->applyFilter();
         }
      }

   // Since the Tool Processor was set up to do modify-in-place, the
   // Tool Infrastructure takes care of passing the modified input buffer
   // along to its destination

   return 0;  // success
}

int CImageFilterProc::EndIteration(SToolProcessingData &procData)
{
   GetToolProgressMonitor()->BumpProgress();

   return 0;
}

int CImageFilterProc::EndProcessing(SToolProcessingData &procData)
{
   // Deallocate the algorithm stuff allocated in BeginProcessing
   delete filter;
   filter = 0;

   if (procData.iteration == iterationCount)
   {
      GetToolProgressMonitor()->SetStatusMessage("Processing complete");
      GetToolProgressMonitor()->StopProgress(true);
   }
   else
   {
      GetToolProgressMonitor()->SetStatusMessage("Stopped");
      GetToolProgressMonitor()->StopProgress(false);
   }

   return 0;
}

/////////////////////////////////////////////////////////////////////////////

CImageFilterParameters::CImageFilterParameters()
 : filterType(IMAGE_FILTER_TYPE_INVALID),
   amount(0), horizontalRadius(0), verticalRadius(0), threshold(0)
{
}

//---------------------------------------------------------------------

void CImageFilterParameters::ReadPDLEntry(CPDLElement *pdlToolAttribs)
{
   // Read Image Filter Parameters from a PDL Entry's Tool Attributes

   string filterTypeStr = pdlToolAttribs->GetAttribString(filterTypeAttr,
														  filterTypeApertureCorrect);
   if (filterTypeStr == filterTypeApertureCorrect)
	  filterType = IMAGE_FILTER_TYPE_APERTURE_CORRECT;
   else if (filterTypeStr == filterTypeBlur)
      filterType = IMAGE_FILTER_TYPE_BLUR;
   else
      filterType = IMAGE_FILTER_TYPE_INVALID;

   amount = pdlToolAttribs->GetAttribInteger(amountAttr, amount);
   horizontalRadius = pdlToolAttribs->GetAttribInteger(horizontalRadiusAttr,
                                                       horizontalRadius);
   verticalRadius = pdlToolAttribs->GetAttribInteger(verticalRadiusAttr,
                                                     verticalRadius);
   threshold = pdlToolAttribs->GetAttribInteger(thresholdAttr, threshold);
}

void CImageFilterParameters::WritePDLEntry(CPDLElement &parent)
{
   // Write Grain Parameters to a PDL Entry's Tool Attributes

   CPDLElement *pdlToolAttribs = parent.MakeNewChild("ImageFilterParameters");

   string filterTypeStr;
   if (filterType == IMAGE_FILTER_TYPE_APERTURE_CORRECT)
      filterTypeStr = filterTypeApertureCorrect;
   else if (filterType == IMAGE_FILTER_TYPE_BLUR)
      filterTypeStr = filterTypeBlur;
   else
      filterTypeStr = "Invalid";
   pdlToolAttribs->SetAttribString(filterTypeAttr, filterTypeStr);

   pdlToolAttribs->SetAttribInteger(amountAttr, amount);
   pdlToolAttribs->SetAttribInteger(horizontalRadiusAttr, horizontalRadius);
   pdlToolAttribs->SetAttribInteger(verticalRadiusAttr, verticalRadius);
   pdlToolAttribs->SetAttribInteger(thresholdAttr, threshold);
}
