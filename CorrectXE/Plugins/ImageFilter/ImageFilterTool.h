#ifndef ImageFilterToolH
#define ImageFilterToolH

// ImageFilter.h: interface for the CImageFilter class.
//
/*
$Header: /usr/local/filmroot/Plugins/ImageFilter/ImageFilterTool.h,v 1.12.2.9 2009/06/16 12:03:04 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "ImageInfo.h"
#include "IniFile.h"
#include "RegionOfInterest.h"
#include "ToolObject.h"

//////////////////////////////////////////////////////////////////////

// ImageFilter's Tool Number (16-bit number that uniquely identifies this tool)
#define IMAGEFILTER_TOOL_NUMBER    14

// ImageFilter Tool Command Numbers
//
#define IF_CMD_NOOP                        0
#define IF_CMD_SELECT_APERTURE_CORRECTION  1
#define IF_CMD_SELECT_BLUR                 2
#define IF_CMD_TOGGLE_FIX                  3
#define IF_CMD_LASSO_TOGGLE                4
#define IF_CMD_TOGGLE_APERTURE_RADIUS_GANG 5
#define IF_CMD_REJECT_FIX                  6


//////////////////////////////////////////////////////////////////////

// max number of threads (not realistic given limited memory)
#define IMAGE_FILTER_MAX_THREAD_COUNT     8

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CImageFilterToolParameters;
class CPDLElement;
class CRegionOfInterest;
class CUnsharpFilter_MT;

//////////////////////////////////////////////////////////////////////

class CImageFilterTool : public CToolObject
{
public:
   CImageFilterTool(const string &newToolName, MTI_UINT32 **newFeatureTable);
   virtual ~CImageFilterTool();

   // Tool startup and shutdowm
   virtual int toolInitialize(CToolSystemInterface *newSystemAPI);
   virtual int toolShutdown();
   virtual int toolActivate();
   virtual int toolDeactivate();
   virtual bool DoesToolPreserveHistory();
   virtual CToolProcessor* makeToolProcessorInstance(
                                          const bool *newEmergencyStopFlagPtr);

   // Tool GUI
   virtual int toolHide();    // Hide the tool's GUI
   virtual int toolShow();    // Show the tool's GUI
   virtual bool IsShowing(void);

   // Tool Event Handlers
   virtual int onToolCommand(CToolCommand &toolCommand);
   virtual int onChangingClip();
   virtual int onNewClip();
   virtual int onNewMarks();
   virtual int onChangingFraming();
   virtual int onRedraw(int frameIndex);

   // PDL Interface
   int onCapturePDLEntry(CPDLElement &pdlEntryToolParams);
   int onGoToPDLEntry(CPDLEntry &pdlEntry);
   
   // Tool Configuration Save and Restore
   void ReadSettings(CIniFile *iniFile, const string &section);
   void WriteSettings(CIniFile *iniFile, const string &section);

   void SetHighlightFlag(bool newHighlightFlag);
   void ClearProvisional();

public:

   CBHook ClipChange;            // Needed to call back on a change
   CBHook MarksChange;           // Needed to call back on a change

private:
   int GatherParameters(CImageFilterToolParameters &toolParams);
   int RunFrameProcessing(int newResumeFrame);
   int StopFrameProcessing();
   int ProcessTrainingFrame();
   void UpdateExecutionGUI(EToolControlState toolProcessingControlState,
                           bool processingRenderFlag);

   void DestroyAllToolSetups(bool notIfPaused);

private:

   int toolSetupHandle;    // Init to -1
   int trainingToolSetupHandle;
};

extern CImageFilterTool *GImageFilter;

//////////////////////////////////////////////////////////////////////

enum EImageFilterType
{
   IMAGE_FILTER_TYPE_INVALID = 0,
   IMAGE_FILTER_TYPE_APERTURE_CORRECT,
   IMAGE_FILTER_TYPE_BLUR
};

//////////////////////////////////////////////////////////////////////

class CImageFilterParameters
{
public:
   CImageFilterParameters();

   void ReadPDLEntry(CPDLElement *pdlToolAttribs);
   void WritePDLEntry(CPDLElement &parent);

public:
   // All member variables are public for easy access.  

   EImageFilterType filterType;

   int amount;
   int horizontalRadius;
   int verticalRadius;
   int threshold;

private:
   const static string filterTypeAttr;
   const static string filterTypeApertureCorrect;
   const static string filterTypeBlur;
   const static string amountAttr;
   const static string horizontalRadiusAttr;
   const static string verticalRadiusAttr;
   const static string thresholdAttr;
};

//////////////////////////////////////////////////////////////////////

class CImageFilterToolParameters : public CToolParameters
{
public:
   CImageFilterToolParameters() : CToolParameters(1, 1) {};
   virtual ~CImageFilterToolParameters() { };

public:
   CImageFilterParameters imageFilterParameters;
};


//////////////////////////////////////////////////////////////////////

class CImageFilterProc : public CToolProcessor
{
public:
   CImageFilterProc(int newToolNumber, const string &newToolName,
                    CToolSystemInterface *newSystemAPI,
                    const bool *newEmergencyStopFlagPtr,
                    IToolProgressMonitor *newToolProgressMonitor);
   virtual ~CImageFilterProc();

   int SetIOConfig(CToolIOConfig *toolIOConfig);
   int SetFrameRange(CToolFrameRange *toolFrameRange);

private:
   // Tool's functions called from within context of the processing loop.
   int SetParameters(CToolParameters *toolParams);
   int BeginProcessing(SToolProcessingData &procData);
   int EndProcessing(SToolProcessingData &procData);
   int BeginIteration(SToolProcessingData &procData);
   int EndIteration(SToolProcessingData &procData);
   int DoProcess(SToolProcessingData &procData);
   int GetIterationCount(SToolProcessingData &procData);

private:
   int inFrameIndex;        // Should we save the whole tool parameter object?
   int outFrameIndex;
   int iterationCount;

   CImageFilterParameters imageFilterParams;    // private copy of parameters
   float faSharpness[MAX_COMPONENT_COUNT];      // Calculated parameter

   CUnsharpFilter_MT *filter;

   // variables to support multithreading of algorithm
   int processingThreadCount;

   // Mask Region
   CRegionOfInterest maskROI;
};


//////////////////////////////////////////////////////////////////////

#endif // !defined(IMAGE_FILTER_TOOL_H)


