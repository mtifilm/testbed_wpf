/* CPMPFilter.cpp: implementation file for filter library */

#include "math.h"
#include "stdio.h"
#include "stdlib.h"
#include "CPMPFilter.h"
#include "assert.h"
#include "algorithm"

/*
 * CGaussianKernel1D: one dimensional Gaussian kernel
 */

CGaussianKernel1D::CGaussianKernel1D(float radius, float sigma)
		: fRadius(radius),
		  fSigma(sigma)
{
}

void CGaussianKernel1D::setRadius(float radius)
{
	fRadius = radius;
}

void CGaussianKernel1D::setSigma(float sigma)
{
	fSigma = sigma;
}

long CGaussianKernel1D::generateKernel(float *fpKernel, long lLength)
{
	float fMult, fSum, fValue;
	long lRadius;

	if (fpKernel == NULL)
	{
		return FAILURE;
	}

	lRadius = (long) floor(fRadius);
	if (lRadius < 0)
	{
		return FAILURE;
	}

	if (lLength < 2 * lRadius + 1)
	{
		return FAILURE;
	}

	fMult = fSigma * fRadius;
	fMult = 1.0 / (2.0 * fMult * fMult);
	fpKernel[lRadius] = 1.0;
	fSum = 1.0;
	for (long lR = 1; lR <= lRadius; lR++)
	{
		fValue = exp(-lR * lR * fMult);
		fpKernel[lRadius + lR] = fValue;
		fpKernel[lRadius - lR] = fValue;
		fSum += 2.0 * fValue;
	}

	/* normalize */
	fMult = 1.0 / fSum;
	for (long lR = 0; lR < 2 * lRadius + 1; lR++)
	{
		fpKernel[lR] *= fMult;
	}

#if 0
	printf("Kernel:\n");
	for (long lR = 0; lR < 2 * lRadius + 1; lR++)
	{
		printf("k[%d] = %f\n", lR, fpKernel[lR]);
	}
#endif

	return SUCCESS;
}

/*
 * CSeparableFilter: filters with separable kernels
 */

CSeparableFilter::CSeparableFilter()
		: CRepair(),
		  bSymmetric(true),
		  fRadiusX(1.0),
		  fRadiusY(1.0),
		  fpKernelX(NULL),
		  fpKernelY(NULL),
		  fpBuffer(NULL),
		  fpVertical(NULL)
{
	lNRow = lNCol = lActiveCom = lNCom = 0;
	lULCx = lULCy = lLRCx = lLRCy = 0;

	/* very important: initialize to empty horizontal/vertical intervals */
	lLeftBoundary = lTopBoundary = 0;
}

CSeparableFilter::~CSeparableFilter()
{
	deallocate();
}

long CSeparableFilter::allocate()
{
	long lRet;

	lRadiusX = (long) floor(fRadiusX);
	if (lRadiusX < 0)
	{
		return FAILURE;
	}

	lRadiusY = (long) floor(fRadiusY);
	if (lRadiusY < 0)
	{
		return FAILURE;
	}

	if (lLRCx == 0)
	{
		lLRCx = lNCol;
	}

	if (lLRCy == 0)
	{
		lLRCy = lNRow;
	}

	if (lULCx < 0 || lULCx >= lLRCx || lLRCx > lNCol || lULCy < 0 || lULCy >= lLRCy || lLRCy > lNRow)
	{
		return FAILURE;
	}

	fpKernelX = (float *) calloc(2 * lRadiusX + 1, sizeof(*fpKernelX));
	if (fpKernelX == NULL)
	{
		return FAILURE;
	}

	if (bSymmetric)
	{
		fpKernelY = fpKernelX;
	}
	else
	{
		fpKernelY = (float *) calloc(2 * lRadiusY + 1, sizeof(*fpKernelY));
		if (fpKernelY == NULL)
		{
			return FAILURE;
		}
	}

	fpVertical = (float **) calloc(2 * lRadiusY + 1, sizeof(*fpVertical));
	if (fpVertical == NULL)
	{
		return FAILURE;
	}

	lPaddedNRow = lLRCy - lULCy + 2 * lRadiusY;
	lPaddedNCol = lLRCx - lULCx + 2 * lRadiusX;

	fpBuffer = (float *) calloc(lPaddedNRow * lPaddedNCol, sizeof(*fpBuffer));
	if (fpBuffer == NULL)
	{
		return FAILURE;
	}

	return SUCCESS;
}

void CSeparableFilter::deallocate()
{
	if (fpKernelX != NULL)
	{
		free(fpKernelX);
		fpKernelX = NULL;
		if (bSymmetric)
		{
			fpKernelY = NULL;
		}
	}

	if (!bSymmetric && fpKernelY != NULL)
	{
		free(fpKernelY);
	}
	fpKernelY = NULL;

	if (fpBuffer != NULL)
	{
		free(fpBuffer);
	}
	fpBuffer = NULL;

	if (fpVertical != NULL)
	{
		free(fpVertical);
	}
	fpVertical = NULL;
}

void CSeparableFilter::setRadii(bool issymmetric, float radiusX, float radiusY)
{
	bSymmetric = issymmetric;
	fRadiusX = radiusX;
	if (bSymmetric)
	{
		fRadiusY = fRadiusX;
	}
	else
	{
		fRadiusY = radiusY;
	}
}

void CSeparableFilter::setImageSize(long width, long height)
{

	/*
	 * Provide this alternative to the base class (CRepair) method
	 * setImageDimension() to ensure that the image dimensions are
	 * properly set to those of the entire image.
	 */

	(void) setImageDimension(0, 0, height, width, height, width);

}

void CSeparableFilter::setActiveComponent(long active)
{
	lActiveCom = active;
}

void CSeparableFilter::setROI(long lUserULCx, long lUserULCy, long lUserLRCx, long lUserLRCy)
{
	lULCx = lUserULCx;
	lULCy = lUserULCy;
	lLRCx = lUserLRCx;
	lLRCy = lUserLRCy;
}

void CSeparableFilter::setHorizAndVertIntervals(long lHoriz, long lVert)
{
	lLeftBoundary = lHoriz;
	lTopBoundary = lVert;
}

/*
 * extractImage(): extract the active component portion of the ROI to fpBuffer.
 * Note: the arrangement of the data is special to the separable filter.
 */

void CSeparableFilter::extractImage(MTI_UINT16 *uspImage)
{
	long lStartX, lStopX, lStartY, lStopY, lPadTop, lPadBottom, lPadRight, lPadLeft, lX, lY, lUseX, lUseY;
	MTI_UINT16 *uspImg;
	float *fpDest;

	/* Step 1: compute ROI to extract and any needed artificial padding */

	lPadTop = 0;
	lPadBottom = 0;
	lPadRight = 0;
	lPadLeft = 0;

	lStartX = lULCx - lRadiusX;
	if (lStartX < lLeftBoundary)
	{
		lPadLeft = lLeftBoundary - lStartX;
		lStartX = lLeftBoundary;
	}

	lStopX = lLRCx + lRadiusX;
	if (lStopX > lNCol)
	{
		lPadRight = lStopX - lNCol;
		lStopX = lNCol;
	}

	lStartY = lULCy - lRadiusY;
	if (lStartY < lTopBoundary)
	{
		lPadTop = lTopBoundary - lStartY;
		lStartY = lTopBoundary;
	}

	lStopY = lLRCy + lRadiusY;
	if (lStopY > lNRow)
	{
		lPadBottom = lStopY - lNRow;
		lStopY = lNRow;
	}

	/* Step 2: extract the ROI */

	/* hello: compute dynamic range */
	////lRangeMax = 0;
	////lRangeMin = laMaxValue[lActiveCom];

	for (lY = lStartY; lY < lStopY; lY++)
	{
		uspImg = uspImage + (lY * lNCol + lStartX) * lNCom + lActiveCom;
		fpDest = fpBuffer + (lPadTop + lY - lStartY) * lPaddedNCol + lPadLeft;
		for (lX = lStartX; lX < lStopX; lX++)
		{
			////if (*uspImg > lRangeMax)
			////{
			////	lRangeMax = *uspImg;
			////}
			////if (*uspImg < lRangeMin)
			////{
			////	lRangeMin = *uspImg;
			////}

			*fpDest++ = imageDatumToFloat(*uspImg);
			uspImg += lNCom;
		}
	}

	/* Step 2: add artificial padding -- reflect about the outermost ring of pixels */

	for (long lCnt = 0; lCnt < 4; lCnt++)
	{
		bool bDoLoop;
		switch (lCnt)
		{
		case 0:
			lStartX = 0;
			lStopX = lPaddedNCol;
			lStartY = 0;
			lStopY = lPadTop;
			bDoLoop = (lPadTop > 0);
			break; /* top */
		case 1:
			lStartX = 0;
			lStopX = lPaddedNCol;
			lStartY = lPaddedNRow - lPadBottom;
			lStopY = lPaddedNRow;
			bDoLoop = (lPadBottom > 0);
			break; /* bottom */
		case 2:
			lStartX = 0;
			lStopX = lPadLeft;
			lStartY = lPadTop;
			lStopY = lPaddedNRow - lPadBottom;
			bDoLoop = (lPadLeft > 0);
			break; /* left */
		case 3:
			lStartX = lPaddedNCol - lPadRight;
			lStopX = lPaddedNCol;
			lStartY = lPadTop;
			lStopY = lPaddedNRow - lPadBottom;
			bDoLoop = (lPadRight > 0);
			break; /* right */
		} /* switch */

		if (!bDoLoop)
		{
			continue;
		} /* no padding here */

		for (lY = lStartY; lY < lStopY; lY++)
		{
			fpDest = fpBuffer + lY * lPaddedNCol + lStartX;
			lUseY = lY;
			if (lY < lPadTop)
			{
				lUseY = 2 * lPadTop - lY;
			}
			else if (lY >= lPaddedNRow - lPadBottom)
			{
				lUseY = 2 * (lPaddedNRow - lPadBottom - 1) - lY;
			}
			for (lX = lStartX; lX < lStopX; lX++)
			{
				lUseX = lX;
				if (lX < lPadLeft)
				{
					lUseX = 2 * lPadLeft - lX;
				}
				else if (lX >= lPaddedNCol - lPadRight)
				{
					lUseX = 2 * (lPaddedNCol - lPadRight - 1) - lX;
				}
				*fpDest++ = fpBuffer[lUseY * lPaddedNCol + lUseX];
			} /* lX */
		} /* lY */

	} /* lCnt */

} /* extractImage() */

void CSeparableFilter::applyFilter()
{
	long lX, lY, lStopY, lPos, lMaxPos;
	float *fpDest, fSum;

	lStopY = lLRCy - lULCy;

	/* apply the vertical (Y) kernel */

	lMaxPos = 2 * lRadiusY + 1;
	for (lY = 0; lY < lStopY; lY++)
	{
		for (lPos = 0; lPos < lMaxPos; lPos++)
		{
			fpVertical[lPos] = fpBuffer + (lY + lPos) * lPaddedNCol;
		}

		fpDest = fpVertical[0];
		for (lX = 0; lX < lPaddedNCol; lX++)
		{
			fSum = 0.0;
			for (lPos = 0; lPos < lMaxPos; lPos++)
			{
				fSum += fpKernelY[lPos] * (*(fpVertical[lPos])++);
			}
			*fpDest++ = fSum;
		} /* lX */
	} /* lY */

	/* apply the horizontal (X) kernel */

	lMaxPos = 2 * lRadiusX + 1;
	for (lY = 0; lY < lStopY; lY++)
	{
		fpDest = fpBuffer + lY * lPaddedNCol;
		for (lX = lULCx; lX < lLRCx; lX++)
		{
			fSum = 0.0;
			float *fpSource = fpDest;
			for (lPos = 0; lPos < lMaxPos; lPos++)
			{
				fSum += fpKernelX[lPos] * (*fpSource++);
			}
			*fpDest++ = fSum;
		} /* lX */
	} /* lY */

} /* applyFilter() */

void CSeparableFilter::replaceImage(MTI_UINT16 *uspImage)
{
	long lX, lY, lMin, lMax;
	MTI_UINT16 *uspImg;
	const MTI_UINT8 *ucpBM;
	float *fpSource, fValue;

	lMin = laMinValue[lActiveCom];
	lMax = laMaxValue[lActiveCom];

	for (lY = lULCy; lY < lLRCy; lY++)
	{
		uspImg = uspImage + (lY * lNCol + lULCx) * lNCom + lActiveCom;
		if (ucpROIBlend == 0)
		{
			ucpBM = 0;
		}
		else
		{
			ucpBM = ucpROIBlend + lY * lNCol + lULCx;
		}
		fpSource = fpBuffer + (lY - lULCy) * lPaddedNCol;
		for (lX = lULCx; lX < lLRCx; lX++)
		{
			if (ucpBM == 0 || *ucpBM > 0)
			{
				float fNew = *fpSource;
				float fOld = (float) * uspImg;

				if (ucpBM != 0 && *ucpBM != 255)
				{
					float fWei = (float)(*ucpBM) / 255.;
					fNew = fWei * fNew + (1. - fWei) * fOld;
				}

				fValue = isSourceDataTypeHalf ? fNew : clamp<float>(fNew, (float)lMin, (float)lMax);
				*uspImg = floatToImageDatum(fValue);
			} /* selected */

			uspImg += lNCom;
			if (ucpBM != 0)
			{
				ucpBM++;
			}

			fpSource++;
		} /* lX */
	} /* lY */

} /* replaceImage() */

/* computeROIAverage(): average the active component within the ROI.
 * We'll use this to adjust the color balance after filtering.
 */

float CSeparableFilter::computeROIAverage(MTI_UINT16 *uspImage)
{
	long lX, lY, lNum;
	MTI_UINT16 *uspImg;
	const MTI_UINT8 *ucpBM;
	float fSum;

	fSum = 0.0;
	lNum = 0;
	for (lY = lULCy; lY < lLRCy; lY++)
	{
		uspImg = uspImage + (lY * lNCol + lULCx) * lNCom + lActiveCom;
		if (ucpROIBlend == 0)
		{
			ucpBM = 0;
		}
		else
		{
			ucpBM = ucpROIBlend + lY * lNCol + lULCx;
		}
		for (lX = lULCx; lX < lLRCx; lX++)
		{
			if (ucpBM == 0 || *ucpBM > 0)
			{
				fSum += (float) * uspImg;
				lNum++;
			} /* selected */
			uspImg += lNCom;
			if (ucpBM != 0)
			{
				ucpBM++;
			}
		} /* lX */
	} /* lY */

	return fSum / lNum;

} /* computeROIAverage() */

/*
 * class CGaussianFilter: Gaussian kernel filter
 */

CGaussianFilter::CGaussianFilter()
		: CSeparableFilter(),
		  fSigmaX(1.0),
		  fSigmaY(1.0)
{
}

CGaussianFilter::~CGaussianFilter()
{
}

long CGaussianFilter::allocate()
{
	long lRet;

	/* base class (CSeparableFilter) does general allocation */
	lRet = static_cast<CSeparableFilter*>(this)->allocate();
	if (lRet)
	{
		return lRet;
	}

	CGaussianKernel1D kern(fRadiusX, fSigmaX);
	lRet = kern.generateKernel(fpKernelX, 2 * lRadiusX + 1);
	if (lRet)
	{
		return lRet;
	}

	if (!bSymmetric)
	{
		kern.setRadius(fRadiusY);
		kern.setSigma(fSigmaY);
		lRet = kern.generateKernel(fpKernelY, 2 * lRadiusY + 1);
		if (lRet)
		{
			return lRet;
		}
	}

	return SUCCESS;
}

void CGaussianFilter::setSigma(float sigmaX, float sigmaY)
{
	fSigmaX = sigmaX;
	if (bSymmetric)
	{
		fSigmaY = fSigmaX;
	}
	else
	{
		fSigmaY = sigmaY;
	}
}

/*
 * CUnsharpFilter: Unsharp Masking Filter
 */

CUnsharpFilter::CUnsharpFilter()
		: CGaussianFilter(),
		  fSharpness(0.0)
{
}

CUnsharpFilter::~CUnsharpFilter()
{
}

void CUnsharpFilter::setSharpness(float sharpness)
{
	fSharpness = sharpness;
}

void CUnsharpFilter::replaceImage(MTI_UINT16 *uspImage)
{
	long lX, lY, lMin, lMax;
	MTI_UINT16 *uspImg;
	const MTI_UINT8 *ucpBM;

	lMin = laMinValue[lActiveCom];
	lMax = laMaxValue[lActiveCom];

	for (lY = lULCy; lY < lLRCy; lY++)
	{
		uspImg = uspImage + (lY * lNCol + lULCx) * lNCom + lActiveCom;
		if (ucpROIBlend == 0)
		{
			ucpBM = 0;
		}
		else
		{
			ucpBM = ucpROIBlend + lY * lNCol + lULCx;
		}

		float *fpSource = fpBuffer + (lY - lULCy) * lPaddedNCol;
		for (lX = lULCx; lX < lLRCx; lX++)
		{
			if (ucpBM == 0 || *ucpBM > 0)
			{
				float fOld = imageDatumToFloat(*uspImg);
				float fNew;
				// 0.0001 used to be 1.0
				float fPercent = fabs((*fpSource < 0.0001) ? 0.0 : (fOld - *fpSource) / (*fpSource));
				if (fPercent > fThreshold)
				{
					fNew = fOld + (fSharpness * (fOld - *fpSource));
				}
				else
				{
					fNew = fOld;
				}

				if (ucpBM != 0 && *ucpBM != 255)
				{
					float fWei = (float)(*ucpBM) / 255.;
					fNew = (fWei * fNew) + ((1. - fWei) * fOld);
				}

				float fResult = isSourceDataTypeHalf ? fNew : clamp<float>(fNew, (float)lMin, (float)lMax);
				*uspImg = floatToImageDatum(fResult);
			} /* selected */

			uspImg += lNCom;
			if (ucpBM != 0)
			{
				ucpBM++;
			}

			fpSource++;
		} /* lX */
	} /* lY */
} /* replaceImage() */

//////////////////////////////////////////////////////////////////////////////

// CUnsharpFilter_MT - implementation of multi-threaded Unsharp Filter
// that uses multiple instances of CUnsharpFilter

CUnsharpFilter_MT::CUnsharpFilter_MT()
{
	aufp = 0;
	lNThread = 0;
}

CUnsharpFilter_MT::~CUnsharpFilter_MT()
{
	delete[]aufp;
	aufp = 0;

	lNThread = 0;

	if (MThreadFree(&msUnsharpFilter))
	{
		assert(false);
	}
}

void CUnsharpFilter_MT::Initialize(long lNThreadArg)
{
	if (lNThreadArg > 0)
	{
		aufp = new CUnsharpFilter[lNThreadArg];
	}

	lNThread = lNThreadArg;

	// set up the multithreading
	msUnsharpFilter.PrimaryFunction = &CallProcess;
	msUnsharpFilter.SecondaryFunction = NULL;
	msUnsharpFilter.CallBackFunction = NULL;
	msUnsharpFilter.vpApplicationData = this;
	msUnsharpFilter.iNThread = lNThread;
	msUnsharpFilter.iNJob = lNThread;

	if (MThreadAlloc(&msUnsharpFilter))
	{
		assert(false);
	}

} /* Initialize */

/////////////////////////////////////////////////////////////////////////////
// Set Parameters
/////////////////////////////////////////////////////////////////////////////

void CUnsharpFilter_MT::setActiveComponent(long active)
{
	for (long lThread = 0; lThread < lNThread; lThread++)
	{
		aufp[lThread].setActiveComponent(active);
	}
}

void CUnsharpFilter_MT::setDataPointer(MTI_UINT16 *dataPointer)
{
	uspDataPointer = dataPointer;
}

void CUnsharpFilter_MT::setSourceDataTypeIsHalf(bool flag)
{
	for (long lThread = 0; lThread < lNThread; lThread++)
	{
		aufp[lThread].setSourceDataTypeIsHalf(flag);
	}
}

int CUnsharpFilter_MT::setImageComponent(long NCom, long PrincipalCom, MTI_UINT16 *pMinValue, MTI_UINT16 *pMaxValue, MTI_UINT16 *pFillValue)
{
	int iRet;

	for (long lThread = 0; lThread < lNThread; lThread++)
	{
		iRet = aufp[lThread].setImageComponent(NCom, PrincipalCom, pMinValue, pMaxValue, pFillValue);
		if (iRet)
		{
			return iRet;
		}
	}

	return 0;
}

void CUnsharpFilter_MT::setImageSize(long width, long height)
{
	for (long lThread = 0; lThread < lNThread; lThread++)
	{
		aufp[lThread].setImageSize(width, height);
	}
}

void CUnsharpFilter_MT::setHorizAndVertIntervals(long lHoriz, long lVert)
{
	for (long lThread = 0; lThread < lNThread; lThread++)
	{
		aufp[lThread].setHorizAndVertIntervals(lHoriz, lVert);
	}
}

void CUnsharpFilter_MT::setRadii(bool issymmetric, float radiusX, float radiusY)
{
	for (long lThread = 0; lThread < lNThread; lThread++)
	{
		aufp[lThread].setRadii(issymmetric, radiusX, radiusY);
	}
}

void CUnsharpFilter_MT::setROI(long lUserULCx, long lUserULCy, long lUserLRCx, long lUserLRCy)
{
	// TEMPORARY CODE TO TEST MULTITHREADING.  WILL BE REPLACED BY
	// REAL MASK CODE

	int roiHeight = lUserLRCy - lUserULCy;
	int subROIHeight = (int)(((double)roiHeight / lNThread) + .5);
	RECT roiRect;
	roiRect.left = lUserULCx;
	roiRect.right = lUserLRCx - 1;

	for (long lThread = 0; lThread < lNThread; lThread++)
	{
		roiRect.top = lUserULCy + lThread * subROIHeight;
		roiRect.bottom = std::min(roiRect.top + subROIHeight - 1, lUserLRCy - 1);

		aufp[lThread].setROI(roiRect.left, roiRect.top, roiRect.right + 1, roiRect.bottom + 1);
	}
}

int CUnsharpFilter_MT::setRegionOfInterestPtr(const MTI_UINT8 *ucpBlend, MTI_UINT8 *ucpLabel)
{
	for (long lThread = 0; lThread < lNThread; lThread++)
	{
		aufp[lThread].setRegionOfInterestPtr(ucpBlend, ucpLabel);
	}

	return 0;
} /* setRegionOfInterestPtr */

void CUnsharpFilter_MT::setSharpness(float sharpness)
{
	for (long lThread = 0; lThread < lNThread; lThread++)
	{
		aufp[lThread].setSharpness(sharpness);
	}
}

void CUnsharpFilter_MT::setSigma(float sigmaX, float sigmaY)
{
	for (long lThread = 0; lThread < lNThread; lThread++)
	{
		aufp[lThread].setSigma(sigmaX, sigmaY);
	}
}

void CUnsharpFilter_MT::setThreshold(float newThreshold)
{
	for (long lThread = 0; lThread < lNThread; lThread++)
	{
		aufp[lThread].fThreshold = newThreshold;
	}
}

/////////////////////////////////////////////////////////////////////////////
// Memory Allocation
/////////////////////////////////////////////////////////////////////////////

long CUnsharpFilter_MT::allocate()
{
	long lRet;

	for (long lThread = 0; lThread < lNThread; lThread++)
	{
		lRet = aufp[lThread].allocate();
		if (lRet != 0)
		{
			return lRet;
		}
	}

	return 0;
}

void CUnsharpFilter_MT::deallocate()
{
	for (long lThread = 0; lThread < lNThread; lThread++)
	{
		aufp[lThread].deallocate();
	}
}

/////////////////////////////////////////////////////////////////////////////
// Extract & Replace Image
/////////////////////////////////////////////////////////////////////////////

void CUnsharpFilter_MT::extractImage(MTI_UINT16 *uspImage)
{
	for (long lThread = 0; lThread < lNThread; lThread++)
	{
		aufp[lThread].extractImage(uspImage);
	}
}

void CUnsharpFilter_MT::replaceImage(MTI_UINT16 *uspImage)
{
	for (long lThread = 0; lThread < lNThread; lThread++)
	{
		aufp[lThread].replaceImage(uspImage);
	}
}

/////////////////////////////////////////////////////////////////////////////
// Processing
/////////////////////////////////////////////////////////////////////////////

long CUnsharpFilter_MT::applyFilter()
{
	long iRet;

	iProcessError = 0;

	iRet = MThreadStart(&msUnsharpFilter);
	if (iRet)
	{
		return iRet;
	}

	return iProcessError;
}

void CUnsharpFilter_MT::CallProcess(void *vp, int iJob)
{
	CUnsharpFilter_MT *aufmt = (CUnsharpFilter_MT*)vp;

	CUnsharpFilter &filter = aufmt->aufp[iJob];
	filter.extractImage(aufmt->uspDataPointer);
	filter.applyFilter();
	filter.replaceImage(aufmt->uspDataPointer);
}
