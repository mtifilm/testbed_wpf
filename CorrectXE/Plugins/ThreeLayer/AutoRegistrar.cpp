#include "AutoRegistrar.h"
#include "Registrar.h"
#include "IniFile.h"
#include "ImageFormat3.h"
#include "MathFunctions.h"
#include "MTImalloc.h"
#include "SynchronousThreadRunner.h"
#include "SysInfo.h"

#include <math.h>

#define HI_CONTRAST

#define THRESHOLD_PERCENTILE 99

#define USE_ROBUST_REGRESSION

#define PI 3.1415927

//#define NO_MULTI

/* NOTES
   // Min is "B"
   float Min = BlkSum00;
   if ( ((float)BlkSum45 /1.414) < Min) Min = BlkSum45;
   if (  (float)BlkSum90  < Min)        Min = BlkSum90;
   if ( ((float)BlkSum135/1.414) < Min) Min = BlkSum135;

   // Std Dev is "C"
   //
   // floating-point can lose bits, so....
   float MeanSq = BlkInt*BlkInt;
   if (BlkIntSq < MeanSq) {
      BlkIntSq = MeanSq;
   }
   float StdDev = sqrt(BlkIntSq - MeanSq);

   // Now Max(C,100)
   if (StdDev < 100.) StdDev = 100.;

   // Now "D" = B / Max(C,100). This is the measure
   // of registration utility for the block
   scalarGradient[i*frameWidth + j] = Min / StdDev;
*/

CAutoRegistrar::CAutoRegistrar(int numStripes)
{
   _nStripes = (numStripes == 0)
             ? std::min<int>(SysInfo::AvailableProcessorCount(), MAX_AUTOREGISTRAR_THREADS)
             : numStripes;

   frameWidth  = 0;
   frameHeight = 0;
   totalPels   = 0;

   goodPts = NULL;

   // init the gradient kernels
   initializeGradientKernels();
}

CAutoRegistrar::~CAutoRegistrar()
{
   MTIfree(goodPts);
}

void CAutoRegistrar::InitTransform(const CImageFormat *imgFmt,
                                   RECT *matbox,
                                   bool *pauseflag,
                                   int numfrms,
                                   int numpts,
                                   int corrng,
                                   int smldiam,
                                   int subdivs)
{
   // => pause flag for render thread
   pauseFlag = pauseflag;

   // from image format
   frameWidth  = imgFmt->getPixelsPerLine();
   frameHeight = imgFmt->getLinesPerFrame();
   totalPels   = frameWidth * frameHeight;

   // the frame for finding points of interest
   // is half- or quarter-scale of the original
   subScale = 2;
   if (frameWidth >= 4096) {
      subScale = 4;
   }

   // half- or quarter scale of green channel will be used
   hframeWidth  = frameWidth/subScale;
   hframeHeight = frameHeight/subScale;
   htotalPels   = hframeWidth * hframeHeight;

   // get the matBox = Points Area
   hmatBox.left   = 0;
   hmatBox.top    = 0;
   hmatBox.right  = hframeWidth;
   hmatBox.bottom = hframeHeight;

   // WTF! What is this +/- 3 shit??!? And why aren't they used in above defaults? QQQ
   if (matbox != NULL) {
      hmatBox.left   = (matbox->left   + 3)/subScale;
      hmatBox.top    = (matbox->top    + 3)/subScale;
      hmatBox.right  = (matbox->right  - 3)/subScale;
      hmatBox.bottom = (matbox->bottom - 3)/subScale;
   }

   // this array holds the alignment data for all pts processed
   MTIfree(goodPts);
   goodPts = (ShiftPoint *)MTImalloc(numfrms*numpts*sizeof(ShiftPoint));
   iendGoodPt = 0;

   // these form the basis...
   numFrms        = numfrms;
   numPts         = numpts;
   corRange       = corrng;          // range of possible correlations

   // adjust the diameter of the correlation box for resolution
   smlDiam = (smldiam - 1) / 2;
   smlDiam = ((int)((double)smlDiam*(double)frameWidth/2048.))&0xfffffffe;
   smlDiam = 2*smlDiam + 1; // force an odd number

   subDivPerPixel = subdivs;         // number of subdivisions per pixel

   // ...from which these are derived
   smlRadius        = (smlDiam - 1)/2;
   lrgRadius        = smlRadius + corRange;
   lrgDiam          = lrgRadius*2 + 1;

   corRangeSubDivs  = corRange*subDivPerPixel;
   smlRadiusSubDivs = smlRadius*subDivPerPixel;
   lrgRadiusSubDivs = lrgRadius*subDivPerPixel;
   gradDiam         = lrgDiam*subDivPerPixel + 1;

   // these count the frames & points
   framesAligned = 0;
   numGoodPts    = 0;
}

int CAutoRegistrar::RegisterFrame(MTI_UINT16 *frm)
{
   int retVal = 0;

   frame = frm; // => RGB intermediate

   int hsmlRadius   = smlRadius/subScale;
   int hsmlDiam     = hsmlRadius*2 + 1;

   /////////////////////////////////////////////////////////////////////////////

   maxScaledGradient = -100.;
   int totalGradientPts = 0;
   ScaledGradientBin = new int[NUM_BINS + 1];
   for (int k=0; k<=NUM_BINS; k++) {
      ScaledGradientBin[k] = 0;
   }

   /////////////////////////////////////////////////////////////////////////////

   // from here we do everything at half- or quarter-scale

   MTI_UINT32 *Gradient = (MTI_UINT32 *)MTImalloc(htotalPels*sizeof(MTI_UINT32));
   if (Gradient == NULL) return -1;
   ScaledGradient = (float *)Gradient;

   // now pull out the green channel at half- or quarter- scale
   MTI_UINT16 *HalfGrn = (MTI_UINT16 *)MTImalloc(htotalPels*sizeof(MTI_UINT16));
   for (int i=0; i<hframeHeight; i++) {
      for (int j=0; j<hframeWidth; j++) {

         HalfGrn[i*hframeWidth + j] = frame[3*subScale*(i*frameWidth+j) + 1];
      }
   }

   MTI_UINT32 *Sum = (MTI_UINT32 *)MTImalloc(htotalPels*sizeof(MTI_UINT32));
   if (Sum == NULL) return -1;

   MTI_UINT64 *Sum64 = (MTI_UINT64 *)MTImalloc(htotalPels*sizeof(MTI_UINT64));
   if (Sum64 == NULL) return -1;

   // clear first rows
   for (int j=0; j<hframeWidth; j++)
      Sum[j] = 0;

   for (int j=0; j<hframeWidth; j++)
      Sum64[j] = 0;

   /////////////////////////////////////////////////////////////////////////////
   //
   // Ix = 0-deg gradient

   for (int i=1; i< (hframeHeight-1) ; i++) {

      MTI_UINT16 *row = HalfGrn + i*hframeWidth;  // => GRN channel

      MTI_INT32 Accum = 0;

      // now continue with the rest of the row
      for (int j=1; j<(hframeWidth-1) ; j++) {

         MTI_UINT16 *pel = row + j;

         // simple expressions in place of full gradients
         int Ix = pel[1] - pel[-1];

         // accumulate
         Accum  += abs(Ix);

         Sum [i*hframeWidth + j] = Sum [(i-1)*hframeWidth + j] + Accum;
      }
   }

   // now we work with 9x9 blocks...
   for (int i=hsmlRadius+1; i < (hframeHeight-hsmlRadius-1); i++) {

      int top = i-hsmlRadius;
      int bot = i+hsmlRadius;

      for (int j=hsmlRadius+1; j < (hframeWidth-hsmlRadius-1); j++) {

         int lft = j-hsmlRadius;
         int rgt = j+hsmlRadius;

         MTI_UINT32 BlkSum00  = Sum[  bot  *hframeWidth+  rgt  ] +
                                Sum[(top-1)*hframeWidth+(lft-1)] -
                                Sum[  bot  *hframeWidth+(lft-1)] -
                                Sum[(top-1)*hframeWidth+  rgt  ];

         Gradient[i*hframeWidth + j] = BlkSum00;
      }
   }

   /////////////////////////////////////////////////////////////////////////////
   //
   // Ix = 45-deg gradient

   for (int i=1; i< (hframeHeight-1) ; i++) {

      MTI_UINT16 *row = HalfGrn + i*hframeWidth;  // => GRN channel

      MTI_INT32 Accum = 0;

      // now continue with the rest of the row
      for (int j=1; j<(hframeWidth-1) ; j++) {

         MTI_UINT16 *pel = row + j;

         // simple expressions in place of full gradients
         int Ix = pel[1] - pel[-1];
         int Iy = pel[hframeWidth] - pel[-hframeWidth];

         // accumulate
         Accum  += (int)fabs((float)(Ix+Iy)/1.4);

         Sum [i*hframeWidth + j] = Sum [(i-1)*hframeWidth + j] + Accum;
      }
   }

   // now we work with 9x9 blocks...
   for (int i=hsmlRadius+1; i < (hframeHeight-hsmlRadius-1); i++) {

      int top = i-hsmlRadius;
      int bot = i+hsmlRadius;

      for (int j=hsmlRadius+1; j < (hframeWidth-hsmlRadius-1); j++) {

         int lft = j-hsmlRadius;
         int rgt = j+hsmlRadius;

         MTI_UINT32 BlkSum45  = Sum[  bot  *hframeWidth+  rgt  ] +
                                Sum[(top-1)*hframeWidth+(lft-1)] -
                                Sum[  bot  *hframeWidth+(lft-1)] -
                                Sum[(top-1)*hframeWidth+  rgt  ];

         if (BlkSum45 < Gradient[i*hframeWidth + j]) {

            Gradient[i*hframeWidth + j] = BlkSum45;
         }
      }
   }

   /////////////////////////////////////////////////////////////////////////////
   //
   // Iy = 90-deg gradient

   for (int i=1; i< (hframeHeight-1) ; i++) {

      MTI_UINT16 *row = HalfGrn + i*hframeWidth;  // => GRN channel

      int Accum = 0;

      // now continue with the rest of the row
      for (int j=1; j<(hframeWidth-1) ; j++) {

         MTI_UINT16 *pel = row + j;

         // simple expressions in place of full gradients
         int Iy = pel[hframeWidth] - pel[-hframeWidth];

         // accumulate
         Accum  += abs(Iy);

         Sum [i*hframeWidth + j] = Sum [(i-1)*hframeWidth + j] + Accum;
      }
   }

   // now we work with 9x9 blocks...
   for (int i=hsmlRadius+1; i < (hframeHeight-hsmlRadius-1); i++) {

      int top = i-hsmlRadius;
      int bot = i+hsmlRadius;

      for (int j=hsmlRadius+1; j < (hframeWidth-hsmlRadius-1); j++) {

         int lft = j-hsmlRadius;
         int rgt = j+hsmlRadius;

         MTI_UINT32 BlkSum90  = Sum[  bot  *hframeWidth+  rgt  ] +
                                Sum[(top-1)*hframeWidth+(lft-1)] -
                                Sum[  bot  *hframeWidth+(lft-1)] -
                                Sum[(top-1)*hframeWidth+  rgt  ];

         if (BlkSum90 < Gradient[i*hframeWidth + j]) {

            Gradient[i*hframeWidth + j] = BlkSum90;
         }
      }
   }

   /////////////////////////////////////////////////////////////////////////////
   //
   // Ix = 135-deg gradient

   for (int i=1; i< (hframeHeight-1) ; i++) {

      MTI_UINT16 *row = HalfGrn + i*hframeWidth;  // => GRN channel

      MTI_INT32 Accum = 0;

      // now continue with the rest of the row
      for (int j=1; j<(hframeWidth-1) ; j++) {

         MTI_UINT16 *pel = row + j;

         // simple expressions in place of full gradients
         int Ix = pel[1] - pel[-1];
         int Iy = pel[hframeWidth] - pel[-hframeWidth];

         // accumulate
         Accum  += (int)fabs((float)(Ix-Iy)/1.4);

         Sum [i*hframeWidth + j] = Sum [(i-1)*hframeWidth + j] + Accum;
      }
   }

   // now we work with 9x9 blocks...
   for (int i=hsmlRadius+1; i < (hframeHeight-hsmlRadius-1); i++) {

      int top = i-hsmlRadius;
      int bot = i+hsmlRadius;

      for (int j=hsmlRadius+1; j < (hframeWidth-hsmlRadius-1); j++) {

         int lft = j-hsmlRadius;
         int rgt = j+hsmlRadius;

         MTI_UINT32 BlkSum135  = Sum[  bot  *hframeWidth+  rgt  ] +
                                 Sum[(top-1)*hframeWidth+(lft-1)] -
                                 Sum[  bot  *hframeWidth+(lft-1)] -
                                 Sum[(top-1)*hframeWidth+  rgt  ];

         if (BlkSum135 < Gradient[i*hframeWidth + j]) {

            Gradient[i*hframeWidth + j] = BlkSum135;
         }
      }
   }

   /////////////////////////////////////////////////////////////////////////////
   //
   // now find the mean GREEN value in each 9x9 block centered at (i,j)

   for (int i=1; i< (hframeHeight-1) ; i++) {

      MTI_UINT16 *row = HalfGrn + i*hframeWidth;  // => GRN channel

      MTI_INT32 Accum = 0;

      // now continue with the rest of the row
      for (int j=1; j<(hframeWidth-1) ; j++) {

         MTI_UINT16 *pelcmp = row + j;

         // accumulate
         Accum  += *pelcmp;

         Sum64 [i*hframeWidth + j]   = Sum64 [(i-1)*hframeWidth + j] + Accum;
      }
   }

   // now we work with 9x9 blocks...
   for (int i=hsmlRadius+1; i < (hframeHeight-hsmlRadius-1); i++) {

      int top = i-hsmlRadius;
      int bot = i+hsmlRadius;

      for (int j=hsmlRadius+1; j < (hframeWidth-hsmlRadius-1); j++) {

         int lft = j-hsmlRadius;
         int rgt = j+hsmlRadius;

         MTI_UINT64 BlkCmp    = Sum64[  bot  *hframeWidth+  rgt  ] +
                                Sum64[(top-1)*hframeWidth+(lft-1)] -
                                Sum64[  bot  *hframeWidth+(lft-1)] -
                                Sum64[(top-1)*hframeWidth+  rgt  ];

         // with n = hsmlDiam*hsmlDiam
         //
         // ...store n*mean here
         //
         Sum[i*hframeWidth+j] = (MTI_UINT32) BlkCmp;
      }
   }

   // now find mean GREEN value-squared in each 9x9 block
   for (int i=1; i< (hframeHeight-1) ; i++) {

      MTI_UINT16 *row = HalfGrn + i*hframeWidth;  // => GRN channel

      MTI_INT32 Accum = 0;

      // now continue with the rest of the row
      for (int j=1; j<(hframeWidth-1) ; j++) {

         MTI_UINT16 *pelcmp = row + j;

         // accumulate
         Accum  += (*pelcmp)*(*pelcmp);

         Sum64 [i*hframeWidth + j]   = Sum64 [(i-1)*hframeWidth + j] + Accum;
      }
   }

   // now we work with 9x9 blocks...
   for (int i=hsmlRadius+1; i < (hframeHeight-hsmlRadius-1); i++) {

      int top = i-hsmlRadius;
      int bot = i+hsmlRadius;

      for (int j=hsmlRadius+1; j < (hframeWidth-hsmlRadius-1); j++) {

         int lft = j-hsmlRadius;
         int rgt = j+hsmlRadius;

         // n = hsmlDiam*hsmlDiam,
         //
         // NOW (Std-Dev)**2 = mean-sq of components - (mean*mean)
         //
         // get BlkCmpSq = n * mean-sq of components
         //
         MTI_UINT64 BlkCmpSq  = Sum64[  bot  *hframeWidth+  rgt  ] +
                                Sum64[(top-1)*hframeWidth+(lft-1)] -
                                Sum64[  bot  *hframeWidth+(lft-1)] -
                                Sum64[(top-1)*hframeWidth+  rgt  ];

         // get (n**2)*( Std-Dev over block ):
         //
         MTI_UINT64 sdsq  = (hsmlDiam*hsmlDiam*BlkCmpSq -
                            (MTI_UINT64)Sum[i*hframeWidth + j] *
                            (MTI_UINT64)Sum[i*hframeWidth + j]);

         // now get the Std-Dev by taking the root and dividing by n
         //
         double stdDev = sqrt((double)sdsq) / (double)(hsmlDiam*hsmlDiam);

#ifdef HI_CONTRAST
         ScaledGradient[i*hframeWidth + j] =
                        (float)((double)Gradient[i*hframeWidth + j]);// * stdDev);
#else
         if (stdDev > 0 ) { // careful!

            if (stdDev < 100.) stdDev = 100.;

            ScaledGradient[i*hframeWidth + j] =
                           (float)((double)Gradient[i*hframeWidth + j] / stdDev);
         }
         else {

            ScaledGradient[i*hframeWidth + j] = 0;
         }
#endif
         // track the maximum scaled gradient value
         totalGradientPts++;
         if (ScaledGradient[i*hframeWidth + j] > maxScaledGradient)
            maxScaledGradient = ScaledGradient[i*hframeWidth + j];
      }
   }

   // build a thousand-bin histogram of the scaled gradients
   for (int i=1; i< (hframeHeight-1) ; i++) {

      for (int j=1; j<(hframeWidth-1) ; j++) {

          float val = ScaledGradient[i*hframeWidth + j];

          int k = maxScaledGradient == 0 ? 0 : (int)(1000.*val/maxScaledGradient);

          ScaledGradientBin[k] += 1;
      }
   }

   // find the top bins that together comprise the desired percentile
   int quota = (100 - THRESHOLD_PERCENTILE)*totalGradientPts/100.;
   int tot = 0;
   int k = 999;
   for (; k >= 0; k--) {
      tot += ScaledGradientBin[k];
      if (tot >= quota)
         break;
   }

   // above this level you have the desired percentile of points
   thresholdScaledGradient = k * maxScaledGradient / 1000.;

   delete ScaledGradientBin;

   // free work arrays
   MTIfree(Sum64);

   MTIfree(Sum);

   MTIfree(HalfGrn);

   /////////////////////////////////////////////////////////////////////////////
   //
   // using the scaledGradient array (merit data) choose
   // a set of points on which to align the RED and BLUE

   // mark beg of pts for this frame
   ibegGoodPt = iendGoodPt;

   // safety MARGIN is critical to avoid memory faults when
   // generating the subpixel gradients surrounding a point
   // remember the frame is half-scale at this point
   margin = (lrgRadius+3)/subScale + 1;
   //
   // this routine advances index iendGoodPt
   //
   if (numPts < 4) {

      chooseBestPoints( numPts,
                        hmatBox.left   + margin,
                        hmatBox.top    + margin,
                        hmatBox.right  - margin,
                        hmatBox.bottom - margin );
   }
   else {

      // use the distance maintenance algorithm
      // in each of the four quadrants of frame

      int hxctr = (hmatBox.left + hmatBox.right) / 2;
      int hyctr = (hmatBox.top + hmatBox.bottom) / 2;

      int quadpts = numPts / 4;

      int q1 = chooseBestPoints( quadpts,
                                 hmatBox.left   + margin,
                                 hmatBox.top    + margin,
                                 hxctr,
                                 hyctr );

      int q2 = chooseBestPoints( quadpts,
                                 hxctr,
                                 hmatBox.top   + margin,
                                 hmatBox.right - margin,
                                 hyctr );

      int q3 = chooseBestPoints( quadpts,
                                 hmatBox.left  + margin,
                                 hyctr,
                                 hxctr,
                                 hmatBox.bottom - margin );

      quadpts = numPts - (q1 + q2 + q3);

      if (quadpts > 0) {

         int q4 = chooseBestPoints( quadpts,
                                    hxctr,
                                    hyctr,
                                    hmatBox.right  - margin,
                                    hmatBox.bottom - margin );
      }
   }

   // got best points, free merit array
   MTIfree(ScaledGradient);
   ScaledGradient = 0;

   // allocate array for GREEN gradients at 1/5 pel coords
   IxGrn = (float *)MTImalloc(gradDiam*gradDiam*sizeof(float));
   IyGrn = (float *)MTImalloc(gradDiam*gradDiam*sizeof(float));

   for (int i=ibegGoodPt; i < iendGoodPt; i++) {

      // calculate integral gradients, then do bilinear interp
      generateSubpixelGradients(GREEN_CHANNEL,
                                goodPts[i].x, goodPts[i].y,
                                IxGrn, IyGrn); // 1 => GRN

      // do red channel

      int redxoff = 0; int redyoff = 0;
      retVal = alignChannelAtPoint(RED_CHANNEL,
                                   goodPts[i].x, goodPts[i].y,
                                   &redxoff, &redyoff);
      if (retVal != 0) {

         // discard the data from beg pt on
         iendGoodPt = ibegGoodPt;

         break;
      }

      goodPts[i].redXShft = (float)redxoff*.2;
      goodPts[i].redYShft = (float)redyoff*.2;

      // do blue channel

      int bluxoff = 0; int bluyoff = 0;
      retVal = alignChannelAtPoint(BLUE_CHANNEL,
                                   goodPts[i].x, goodPts[i].y,
                                   &bluxoff, &bluyoff);
      if (retVal != 0) {

         // discard the data from beg pt on
         iendGoodPt = ibegGoodPt;

         break;
      }

      goodPts[i].blueXShft = (float)bluxoff*.2;
      goodPts[i].blueYShft = (float)bluyoff*.2;
   }

   // free arrays for subpixel GREEN gradients
   MTIfree(IyGrn);
   MTIfree(IxGrn);

   // success
   return retVal;
}

void CAutoRegistrar::GetLastFramesRegistrationTransform(AffineCoeff *xfrm,
                                                        bool usewarp)
{
   int numpts = iendGoodPt - ibegGoodPt;

   // return the transform
   if (usewarp&(numpts >= MINPTS_WRP)) {
      affineTransform(ibegGoodPt, iendGoodPt, xfrm);
   }
   else {
      simpleTransform(ibegGoodPt, iendGoodPt, xfrm);
   }
}

void CAutoRegistrar::OpenForReadingAlignPts()
{
  inxtGoodPt = ibegGoodPt;
}

int CAutoRegistrar::GetNextAlignPt(int *x, int *y)
{
   if (inxtGoodPt >= iendGoodPt)
      return -1;

   *x = goodPts[inxtGoodPt].x;
   *y = goodPts[inxtGoodPt].y;

   inxtGoodPt++;

   return 0;
}

void CAutoRegistrar::GetAllFramesRegistrationTransform(AffineCoeff *xfrm,
                                                       bool usewarp)
{
   // return the transform
   if (usewarp&(iendGoodPt >= MINPTS_WRP)) {
      affineTransform(0, iendGoodPt, xfrm);
   }
   else {
      simpleTransform(0, iendGoodPt, xfrm);
   }

   // done w pts array
   MTIfree(goodPts);
   ibegGoodPt = iendGoodPt = 0;
   goodPts = NULL;
}

int CAutoRegistrar::RegisterFrame(MTI_UINT16 *frm,
                                  const CImageFormat *imgFmt,
                                  RECT *matbox,
                                  bool *pauseflag,
                                  AffineCoeff *xfrm,
                                  bool usewarp,
                                  int numpts,
                                  int corrng,
                                  int smldiam,
                                  int subdivs)
{
   InitTransform(imgFmt,
                 matbox,
                 pauseflag,
                      1,
                 numpts,
                 corrng,
                 smldiam,
                 subdivs);

   int retVal = RegisterFrame(frm);

   if (retVal == 0)
      GetLastFramesRegistrationTransform(xfrm, usewarp);

   return retVal;
}

void CAutoRegistrar::initializeGradientKernels()
{
   // compute gradient filter for x-direction using
   // Hamming kernel and its derivative
   for (int i=0; i<3; i++) {
      for (int j=0; j<3; j++) {

         gradX[i][j] =

         // Hamming kernel in Y
         ( .42 + .5*cos(PI*(i-1)/Rflt) + .08*cos(2*PI*(i-1)/Rflt) ) *

         // ...its derivative in X
         ( (.5*PI/Rflt)*sin(PI*(j-1)/Rflt) + (.16*PI/Rflt)*sin(2*PI*(j-1)/Rflt) );
      }
   }

   // compute sum for normalization
   float Sum = 0.0;
   for (int i=0; i<3; i++) {
      for (int j=0; j<3; j++) {

         Sum += (j + 1)*gradX[i][j];
      }
   }

   // normalize
   for (int i=0; i<3; i++) {
      for (int j=0; j<3; j++) {

         gradX[i][j] /= Sum;
      }
   }

   // compute gradient filter for y-direction using
   // Hamming kernel and its derivative
   for (int i=0; i<3; i++) {
      for (int j=0; j<3; j++) {

         gradY[i][j] =

         // Hamming kernel in X
         ( .42 + .5*cos(PI*(j-1)/Rflt) + .08*cos(2*PI*(j-1)/Rflt) ) *

         // ...its derivative in Y
         ( (.5*PI/Rflt)*sin(PI*(i-1)/Rflt) + (.16*PI/Rflt)*sin(2*PI*(i-1)/Rflt) );
      }
   }

   // compute sum for normalization
   Sum = 0.0;
   for (int i=0; i<3; i++) {
      for (int j=0; j<3; j++) {

         Sum += (i + 1)*gradY[i][j];
      }
   }

   // normalize
   for (int i=0; i<3; i++) {
      for (int j=0; j<3; j++) {

         gradY[i][j] /= Sum;
      }
   }
}

bool CAutoRegistrar::chooseBestPoint(int lft, int top, int rgt, int bot)
{
   float max = thresholdScaledGradient;

   int k = -1;
   int l = -1;

   for (int i=top; i<bot; i++) {

      for (int j=lft; j<rgt; j++) {

         float tntGradient = ScaledGradient[hframeWidth*i+j];
         if (tntGradient > max) {

            max = tntGradient;

            k = i;
            l = j;
         }
      }
   }

   if (k != -1) {

      // note that when using an average of
      // several alignment points you can
      // weight the various offsets by the
      // scaledGradient measure of the point

      // we multiply the coords because we are
      // now returning to full resolution
      goodPts[iendGoodPt].x         = l*subScale;
      goodPts[iendGoodPt].y         = k*subScale;

      goodPts[iendGoodPt].rating    = 1.0;

      goodPts[iendGoodPt].redXShft  = 0.0;
      goodPts[iendGoodPt].redYShft  = 0.0;
      goodPts[iendGoodPt].blueXShft = 0.0;
      goodPts[iendGoodPt].blueYShft = 0.0;

      // now MARK the point with an exclusion zone
      markBestPoint(l, k, lft, top, rgt, bot);

      return true;
   }

   return false;
}

// this creates an exclusion zone around a point after it's been chosen
void CAutoRegistrar::markBestPoint( int x, int y, int lft, int top, int rgt, int bot)
{
   // derive this from SMLRADIUS
   int radius = 8*smlRadius/subScale;

   int exlft, extop, exrgt, exbot;

   exlft = x-radius;
   if (exlft < lft) exlft = lft;
   extop = y-radius;
   if (extop < top) extop = top;
   exrgt = x+radius;
   if (exrgt > rgt) exrgt = rgt;
   exbot = y+radius;
   if (exbot > bot) exbot = bot;

   for (int i = extop; i <= exbot; i++) {

      for (int j = exlft; j <= exrgt; j++) {

         ScaledGradient[hframeWidth*i+j] = 0;
      }
   }
}

int CAutoRegistrar::chooseBestPoints(int quota, int lft, int top, int rgt, int bot)
{
   int retVal = 0;

   for (int i = 0; i < quota; i++) {

      // get a good point far enough
      // from the other good points
      if (chooseBestPoint(lft, top, rgt, bot)) {

         iendGoodPt++;

         retVal++;
      }
      else
         break;
   }

   return retVal;
}

double CAutoRegistrar::det(ThreeByThree *matrix)
{
   double retVal;
   retVal  = matrix->a11 * (matrix->a22 * matrix->a33 - matrix->a32 * matrix->a23);
   retVal -= matrix->a21 * (matrix->a12 * matrix->a33 - matrix->a32 * matrix->a13);
   retVal += matrix->a31 * (matrix->a12 * matrix->a23 - matrix->a22 * matrix->a13);

   return retVal;
}

// args are indices into goodPts array, with the latter being exclusive
void CAutoRegistrar::simpleTransform(int ibegpt, int iendpt, AffineCoeff *reslt)
{
   // warp parameters are NULL
   //
   reslt->AredX = 0.0;
   reslt->BredX = 0.0;

   reslt->AredY = 0.0;
   reslt->BredY = 0.0;

   reslt->AbluX = 0.0;
   reslt->BbluX = 0.0;

   reslt->AbluY = 0.0;
   reslt->BbluY = 0.0;

   // displ parameters are simple avgs
   //
   reslt->CredX = 0.0;
   reslt->CredY = 0.0;

   reslt->CbluX = 0.0;
   reslt->CbluY = 0.0;

   int numpts = iendpt - ibegpt;
   if ( numpts > 0 ) {

      for (int i=ibegpt; i<iendpt; i++) {
         reslt->CredX += goodPts[i].redXShft;
         reslt->CredY += goodPts[i].redYShft;
         reslt->CbluX += goodPts[i].blueXShft;
         reslt->CbluY += goodPts[i].blueYShft;
      }

      reslt->CredX /= numpts;
      reslt->CredY /= numpts;
      reslt->CbluX /= numpts;
      reslt->CbluY /= numpts;
   }
}

#ifdef USE_ROBUST_REGRESSION
// args are indices into goodPts array, with the latter being exclusive
void CAutoRegistrar::affineTransform(int ibegpt, int iendpt, AffineCoeff *reslt)
{
   // number of pts
   int n = (iendpt - ibegpt);

   // from robust
   mvector<double> y(n);
   matrix<double> X(n, 3);

   // output from robust()
   mvector<double> b(3);

   // RED X
   //
   for (int i=0; i < n; i++) {

      y[i] = goodPts[ibegpt + i].redXShft;

      X[i][0] = goodPts[i + ibegpt].x;
      X[i][1] = goodPts[i + ibegpt].y;
      X[i][2] = 1;
   }

   robust(y, X, 0.001, b);

   reslt->AredX = b[0];
   reslt->BredX = b[1];
   reslt->CredX = b[2];

   // RED Y
   //
   for (int i=0; i < n; i++) {

      y[i] = goodPts[ibegpt + i].redYShft;

      X[i][0] = goodPts[i + ibegpt].x;
      X[i][1] = goodPts[i + ibegpt].y;
      X[i][2] = 1;
   }

   robust(y, X, 0.001, b);

   reslt->AredY = b[0];
   reslt->BredY = b[1];
   reslt->CredY = b[2];

   // BLUE X
   //
   for (int i=0; i < n; i++) {

      y[i] = goodPts[ibegpt + i].blueXShft;

      X[i][0] = goodPts[i + ibegpt].x;
      X[i][1] = goodPts[i + ibegpt].y;
      X[i][2] = 1;
   }

   robust(y, X, 0.001, b);

   reslt->AbluX = b[0];
   reslt->BbluX = b[1];
   reslt->CbluX = b[2];

   // BLUE Y
   //
   for (int i=0; i < n; i++) {

      y[i] = goodPts[ibegpt + i].blueYShft;

      X[i][0] = goodPts[i + ibegpt].x;
      X[i][1] = goodPts[i + ibegpt].y;
      X[i][2] = 1;
   }

   robust(y, X, 0.001, b);

   reslt->AbluY = b[0];
   reslt->BbluY = b[1];
   reslt->CbluY = b[2];
}
#else
// args are indices into goodPts array, with the latter being exclusive
void CAutoRegistrar::affineTransform(int ibegpt, int iendpt, AffineCoeff *reslt)
{
   ThreeByThree Dnm;
   ThreeByThree Num;

   // fill in the denominator
   //
   Dnm.a11 = 0;
   Dnm.a12 = 0;
   Dnm.a13 = 0;
   Dnm.a22 = 0;
   Dnm.a23 = 0;
   for (int i=ibegpt; i<iendpt; i++) {
      Dnm.a11 += goodPts[i].x * goodPts[i].x;
      Dnm.a12 += goodPts[i].x * goodPts[i].y;
      Dnm.a13 += goodPts[i].x;
      Dnm.a22 += goodPts[i].y * goodPts[i].y;
      Dnm.a23 += goodPts[i].y;
   }
   Dnm.a21 = Dnm.a12;
   Dnm.a31 = Dnm.a13;
   Dnm.a32 = Dnm.a23;
   Dnm.a33 = (iendpt - ibegpt);

   double denom = det(&Dnm);
   if (fabs(denom) < 0.000001) {

      // ignore  Cramer's  Rule - just
      // get xform with no warp coeffs
      //
      simpleTransform(ibegpt, iendpt, reslt);

      return;
   }

   double Col[3];

   /////////////////////////////////////////////////////////////////////////////

   Col[0] = 0;
   Col[1] = 0;
   Col[2] = 0;
   for (int i=ibegpt; i<iendpt; i++) {
      Col[0] += goodPts[i].redXShft * goodPts[i].x;
      Col[1] += goodPts[i].redXShft * goodPts[i].y;
      Col[2] += goodPts[i].redXShft;
   }

   // Cramer's Rule 3 times
   Num = Dnm;
   Num.a11 = Col[0];
   Num.a21 = Col[1];
   Num.a31 = Col[2];

   reslt->AredX = det(&Num) / denom;

   Num = Dnm;
   Num.a12 = Col[0];
   Num.a22 = Col[1];
   Num.a32 = Col[2];

   reslt->BredX = det(&Num) / denom;

   Num = Dnm;
   Num.a13 = Col[0];
   Num.a23 = Col[1];
   Num.a33 = Col[2];

   reslt->CredX = det(&Num) / denom;

   /////////////////////////////////////////////////////////////////////////////

   Col[0] = 0;
   Col[1] = 0;
   Col[2] = 0;
   for (int i=ibegpt; i<iendpt; i++) {
      Col[0] += goodPts[i].redYShft * goodPts[i].x;
      Col[1] += goodPts[i].redYShft * goodPts[i].y;
      Col[2] += goodPts[i].redYShft;
   }

   // Cramer's Rule 3 times
   Num = Dnm;
   Num.a11 = Col[0];
   Num.a21 = Col[1];
   Num.a31 = Col[2];

   reslt->AredY = det(&Num) / denom;

   Num = Dnm;
   Num.a12 = Col[0];
   Num.a22 = Col[1];
   Num.a32 = Col[2];

   reslt->BredY = det(&Num) / denom;

   Num = Dnm;
   Num.a13 = Col[0];
   Num.a23 = Col[1];
   Num.a33 = Col[2];

   reslt->CredY = det(&Num) / denom;

   /////////////////////////////////////////////////////////////////////////////

   Col[0] = 0;
   Col[1] = 0;
   Col[2] = 0;
   for (int i=ibegpt; i<iendpt; i++) {
      Col[0] += goodPts[i].blueXShft * goodPts[i].x;
      Col[1] += goodPts[i].blueXShft * goodPts[i].y;
      Col[2] += goodPts[i].blueXShft;
   }

   // Cramer's Rule 3 times
   Num = Dnm;
   Num.a11 = Col[0];
   Num.a21 = Col[1];
   Num.a31 = Col[2];

   reslt->AbluX = det(&Num) / denom;

   Num = Dnm;
   Num.a12 = Col[0];
   Num.a22 = Col[1];
   Num.a32 = Col[2];

   reslt->BbluX = det(&Num) / denom;

   Num = Dnm;
   Num.a13 = Col[0];
   Num.a23 = Col[1];
   Num.a33 = Col[2];

   reslt->CbluX = det(&Num) / denom;

   /////////////////////////////////////////////////////////////////////////////

   Col[0] = 0;
   Col[1] = 0;
   Col[2] = 0;
   for (int i=ibegpt; i<iendpt; i++) {
      Col[0] += goodPts[i].blueYShft * goodPts[i].x;
      Col[1] += goodPts[i].blueYShft * goodPts[i].y;
      Col[2] += goodPts[i].blueYShft;
   }

   // Cramer's Rule 3 times
   Num = Dnm;
   Num.a11 = Col[0];
   Num.a21 = Col[1];
   Num.a31 = Col[2];

   reslt->AbluY = det(&Num) / denom;

   Num = Dnm;
   Num.a12 = Col[0];
   Num.a22 = Col[1];
   Num.a32 = Col[2];

   reslt->BbluY = det(&Num) / denom;

   Num = Dnm;
   Num.a13 = Col[0];
   Num.a23 = Col[1];
   Num.a33 = Col[2];

   reslt->CbluY = det(&Num) / denom;
}
#endif

float CAutoRegistrar::normalizeShift(float shft)
{
   float retVal;

   if (shft < 0.0) {
      retVal = -shft;
      retVal = (float)((int)(retVal/.2 + .5))*.2;
      retVal = -retVal;
   }
   else {
      retVal = (float)((int)(shft/.2 + .5))*.2;
   }

   return retVal;
}

void CAutoRegistrar::generateSubpixelGradients(int channel,
                                               int xctr, int yctr,
                                               float *ixgrad, float *iygrad)
{
   // fill in the gradient values at the (5M, 5N) integral pts
   for (int i=0; i<=lrgDiam; i++) {

      // row ptr into frame. The "channel" is 0 for RED, 1 for GRREN, 2 for BLUE
      MTI_UINT16 *row = frame + (yctr - lrgRadius + i)*frameWidth*3 + channel;

      for (int j=0; j<=lrgDiam; j++) {

         MTI_UINT16 *pel = row + (xctr - lrgRadius + j)*3;

         // gradient components
         float Ix = 0.0; float Iy = 0.0;
         for (int k=0; k<3; k++) {
            for (int l=0; l<3; l++) {
               MTI_UINT16 component = pel[((k-1)*frameWidth + (l-1))*3];
               Ix += component*gradX[k][l];
               Iy += component*gradY[k][l];
            }
         }

         // save the values at (5J, 5I) pts
         ixgrad[(i*gradDiam+j)*subDivPerPixel] = Ix;
         iygrad[(i*gradDiam+j)*subDivPerPixel] = Iy;
      }
   }

   // interpolate bilinearly to get gradients at all subpixel pts
   for (int i=0; i<(gradDiam-1); i++) {

      // index of highest multiple of 5 row < i
      int k = (i/subDivPerPixel)*subDivPerPixel;

      // combining constants for rows
      float yfrn  = (float)(i-k)/(float)subDivPerPixel;
      float afrn  = (1.0 - yfrn);

      for (int j=0; j<(gradDiam-1); j++) {

         // index of highest multiple of 5 pel < j
         int l = (j/subDivPerPixel)*subDivPerPixel;

         // combining constants for pels
         float xfrn = (float)(j-l)/(float)subDivPerPixel;
         float bfrn = (1.0 - xfrn);

         ixgrad[gradDiam*i+j]  =  afrn*(bfrn*ixgrad[gradDiam*k+l] +
                                   xfrn*ixgrad[gradDiam*k+l+subDivPerPixel]) +
                              yfrn*(bfrn*ixgrad[gradDiam*(k+subDivPerPixel)+l] +
                                   xfrn*ixgrad[gradDiam*(k+subDivPerPixel)+l+subDivPerPixel]);

         iygrad[gradDiam*i+j]  =  afrn*(bfrn*iygrad[gradDiam*k+l] +
                                   xfrn*iygrad[gradDiam*k+l+subDivPerPixel]) +
                              yfrn*(bfrn*iygrad[gradDiam*(k+subDivPerPixel)+l] +
                                   xfrn*iygrad[gradDiam*(k+subDivPerPixel)+l+subDivPerPixel]);
      }
   }
}

float CAutoRegistrar::computeCorrelation(int xchn, int ychn, int step)
{
   //xgrn = (gradDiam-1)/2;
   //ygrn = (gradDiam-1)/2;
   xgrn = lrgRadiusSubDivs;
   ygrn = lrgRadiusSubDivs;

   float pels = (2*smlRadiusSubDivs+1)*(2*smlRadiusSubDivs+1);
   pels /= step*step;

   // get the means of Ix and Iy over the 81x81 box
   float chnxmean = 0.0;
   float chnymean = 0.0;
   for (int i=-smlRadiusSubDivs; i<=smlRadiusSubDivs; i+=step) {

      for (int j=-smlRadiusSubDivs; j<=smlRadiusSubDivs; j+=step) {

         chnxmean += IxChn[(ychn+i)*gradDiam+(xchn+j)];
         chnymean += IyChn[(ychn+i)*gradDiam+(xchn+j)];
      }
   }

   chnxmean /= pels;
   chnymean /= pels;

   float tcorr = 0.0;
   float norm  = 0.0;
   for (int i=-smlRadiusSubDivs; i<=smlRadiusSubDivs; i+=step) {

      for (int j=-smlRadiusSubDivs; j<=smlRadiusSubDivs; j+=step) {

         tcorr += (IxGrn[(ygrn+i)*gradDiam+(xgrn+j)]-grnxmean)*
                  (IxChn[(ychn+i)*gradDiam+(xchn+j)]-chnxmean)   +
                  (IyGrn[(ygrn+i)*gradDiam+(xgrn+j)]-grnymean)*
                  (IyChn[(ychn+i)*gradDiam+(xchn+j)]-chnymean);

         norm  += (IxChn[(ychn+i)*gradDiam+(xchn+j)]-chnxmean)*
                  (IxChn[(ychn+i)*gradDiam+(xchn+j)]-chnxmean)   +
                  (IyChn[(ychn+i)*gradDiam+(xchn+j)]-chnymean)*
                  (IyChn[(ychn+i)*gradDiam+(xchn+j)]-chnymean);
      }
   }

   if (norm > 0.0) {

      tcorr /= sqrt(norm);

   }
   else {

      tcorr = -1000000000;
   }

   return tcorr;
}

// Two levels of search: COARSE does 13x13 pts at 5/5 pel increments
//                       FINE   does 11x11 pts at 1/5 pel increments
// The ratio improvement with this method is 4225 / 290 - about 15:1

int CAutoRegistrar::alignChannelWithGreenSlow(int *xoffs, int *yoffs)
{
   *xoffs = 0;
   *yoffs = 0;

   //xgrn = (gradDiam-1)/2;
   //ygrn = (gradDiam-1)/2;
   xgrn = lrgRadiusSubDivs;
   ygrn = lrgRadiusSubDivs;

   float pels = (2*smlRadiusSubDivs+1)*(2*smlRadiusSubDivs+1);

   // get the means of Ix and Iy over the 17x17 box
   grnxmean = 0.0;
   grnymean = 0.0;
   for (int i=-smlRadiusSubDivs; i<=smlRadiusSubDivs; i++) {

      for (int j=-smlRadiusSubDivs; j<=smlRadiusSubDivs; j++) {

         if (pauseFlag != NULL) {
            if (*pauseFlag)
              return -1;
         }

         grnxmean += IxGrn[(ygrn+i)*gradDiam+(xgrn+j)];
         grnymean += IyGrn[(ygrn+i)*gradDiam+(xgrn+j)];
      }
   }

   grnxmean /= pels;
   grnymean /= pels;

   // COARSE level in increments of 5/5 pel - corRange of 6 => 144 cycles

   float corr = -1000000000;
   float tcorr;
   for (int yoff = 0; yoff <= corRangeSubDivs-subDivPerPixel; yoff+=subDivPerPixel) {

      int ychn = ygrn + yoff;

      for (int xoff = 0; xoff <= corRangeSubDivs-subDivPerPixel; xoff+=subDivPerPixel) {

         if (pauseFlag != NULL) {
            if (*pauseFlag)
              return -1;
         }

         int xchn = xgrn + xoff;

         tcorr = computeCorrelation(xchn, ychn);
         if (tcorr > corr) {

            corr = tcorr;
            *xoffs =  xoff;
            *yoffs =  yoff;
         }

         if (xoff == 0) continue;
         xchn = xgrn - xoff;

         tcorr = computeCorrelation(xchn, ychn);
         if (tcorr > corr) {

            corr = tcorr;
            *xoffs = -xoff;
            *yoffs =  yoff;
         }
      } // xoff

      if (yoff == 0) continue;
      ychn = ygrn - yoff;

      for (int xoff = 0; xoff <= corRangeSubDivs-subDivPerPixel; xoff+=subDivPerPixel) {

         if (pauseFlag != NULL) {
            if (*pauseFlag)
              return -1;
         }

         int xchn = xgrn + xoff;

         tcorr = computeCorrelation(xchn, ychn);
         if (tcorr > corr) {

            corr = tcorr;
            *xoffs =  xoff;
            *yoffs = -yoff;
         }

         if (xoff == 0) continue;
         xchn = xgrn - xoff;

         tcorr = computeCorrelation(xchn, ychn);
         if (tcorr > corr) {

            corr = tcorr;
            *xoffs = -xoff;
            *yoffs = -yoff;
         }
      } // xoff
   } // yoff

   // FINE level in increments of 1/5 pel - 81 cycles

   int xbest = *xoffs;
   int ybest = *yoffs;

   for (int yoff = ybest-subDivPerPixel; yoff <= ybest+subDivPerPixel; yoff+=1) {

      int ychn = ygrn + yoff;

      for (int xoff = xbest-subDivPerPixel; xoff <= xbest+subDivPerPixel; xoff+=1) {

         if (pauseFlag != NULL) {
            if (*pauseFlag)
              return -1;
         }

         int xchn = xgrn + xoff;

         tcorr = computeCorrelation(xchn, ychn);
         if (tcorr > corr) {

            corr = tcorr;
            *xoffs = xoff;
            *yoffs = yoff;
         }
      } // xoff
   } // yoff

   // success
   return 0;
}

int CAutoRegistrar::alignChannelWithGreenFast(int *xoffs, int *yoffs)
{
   // do no harm
   *xoffs = 0;
   *yoffs = 0;

   //xgrn = (gradDiam-1)/2;
   //ygrn = (gradDiam-1)/2;
   xgrn = lrgRadiusSubDivs;
   ygrn = lrgRadiusSubDivs;

   float pels = (2*smlRadiusSubDivs+1)*(2*smlRadiusSubDivs+1);

   // get the means of Ix and Iy over the 17x17 box
   grnxmean = 0.0;
   grnymean = 0.0;
   for (int i=-smlRadiusSubDivs; i<=smlRadiusSubDivs; i++) {

      for (int j=-smlRadiusSubDivs; j<=smlRadiusSubDivs; j++) {

         if (pauseFlag != NULL) {
            if (*pauseFlag)
              return -1;
         }

         grnxmean += IxGrn[(ygrn+i)*gradDiam+(xgrn+j)];
         grnymean += IyGrn[(ygrn+i)*gradDiam+(xgrn+j)];
      }
   }

   grnxmean /= pels;
   grnymean /= pels;

   for (auto i = 0; i < _nStripes; ++i)
   {
      _results[i] = {-1000000000, 0, 0, 0};
   }

   // do the COARSE alignment
//   tsThread.PrimaryFunction = (void (*)(void *,int))&alignOneStripeCoarse;
#ifdef NO_MULTI
   for (int i = 0; i < _nStripes; i++)
   {
//      tsThread.PrimaryFunction(this,i);
      alignOneStripeCoarse(this, i, _nStripes);
   }
#else
   if (_nStripes == 1)
   {
      // if we're only doing one stripe, call the function directly
//      tsThread.PrimaryFunction(this,0);
      alignOneStripeCoarse(this, 0, _nStripes);
   }
   else
   {
      // fire up one thread for each stripe
//      int iRet = MThreadStart(&tsThread);
      SynchronousThreadRunner threadRunner(_nStripes, this, alignOneStripeCoarse);
      int iRet = threadRunner.Run();
      if (iRet)
      {
         TRACE_0(errout << "Register: Coarse alignment FAIL, code " << iRet);
         return -1;
      }
   }
#endif

   // find best offsets by looking at
   // best correlation in all stripes
   float corr = -1000000000;
   int failedCount = 0;
   int bestStripe = -1;
   for (int i = 0; i < _nStripes; i++)
   {
      if (_results[i].retVal == -1)
      {
         failedCount++;
      }

      if (_results[i].tcorr > corr)
      {
         bestStripe = i;
         corr = _results[i].tcorr;
      }
   }

   if (failedCount != 0)
   {
      TRACE_0(errout << "ERROR: " << failedCount << "/" << _nStripes << " failed! ");
      return -1;
   }

   if (bestStripe < 0 || bestStripe >= _nStripes || corr == -1000000000)
   {
      TRACE_0(errout << "ERROR: " << "INTERNAL ERROR: Coarse alignment didn't find the best stripe! (" << bestStripe << ")");
      return -1;
   }

   _xbest = _results[bestStripe].xoffs;
   _ybest = _results[bestStripe].yoffs;

   const int NumberOfFineStripes = 5 + 1 + 5; // "best" coarse row plus 5/5 above and below

   for (auto i = 0; i < NumberOfFineStripes; ++i)
   {
      _results[i] = {-1000000000, 0, 0, 0};
   }

   // do the FINE alignment
//   tsThread.PrimaryFunction = (void (*)(void *,int))&alignOneStripeFine;
#ifdef NO_MULTI
   for (int i = 0; i < NumberOfFineStripes; i++)
   {
//      tsThread.PrimaryFunction(this,i);
      alignOneStripeFine(this, i, NumberOfFineStripes);
   }
#else
   // fire up one thread for each stripe
//      int iRet = MThreadStart(&tsThread);
   SynchronousThreadRunner runner(NumberOfFineStripes, this, alignOneStripeFine);
   int iRet = runner.Run();
   if (iRet)
   {
      TRACE_0(errout << "Register: Fine alignment FAIL, code " << iRet);
      return -1;
   }
#endif

   // find best offsets by looking at
   // best correlation in all stripes
   corr = -1000000000;
   failedCount = 0;
   bestStripe = -1;
   for (int i=0; i < _nStripes; i++) {

      if (_results[i].retVal == -1)
      {
         failedCount++;
      }

      if (_results[i].tcorr > corr)
      {
         bestStripe = i;
         corr   = _results[i].tcorr;
      }
   }

   if (failedCount != 0)
   {
      TRACE_0(errout << "ERROR: " << failedCount << "/" << _nStripes << " failed! ");
      return -1;
   }

   if (bestStripe < 0 || bestStripe >= _nStripes || corr == -1000000000)
   {
      TRACE_0(errout << "ERROR: " << "INTERNAL ERROR: Fine alignment didn't find the best stripe! (" << bestStripe << ")");
      return -1;
   }

   *xoffs = _results[bestStripe].xoffs;
   *yoffs = _results[bestStripe].yoffs;

   return 0;
}

int CAutoRegistrar::alignChannelAtPoint(int channel,
                                        int xreg, int yreg,
                                        int *xoffs, int *yoffs)
{
   int retVal = -1;

   // use offsets just found to initialize sub-pixel tests
   //
   // allocate array for channel gradients at 1/5 pel coords
   IxChn = (float *)MTImalloc(gradDiam*gradDiam*sizeof(float));
   IyChn = (float *)MTImalloc(gradDiam*gradDiam*sizeof(float));

   // calculate integral channel gradients, then do bilinear interp
   generateSubpixelGradients(channel,          // channel is 0 or 2
                             xreg, yreg,
                             IxChn, IyChn);
   // find best chn offsets
   retVal = alignChannelWithGreenFast(xoffs, yoffs);

   MTIfree(IyChn);
   MTIfree(IxChn);

   return retVal;
}

void CAutoRegistrar::null(void *v, int iJob)
{
}

int CAutoRegistrar::alignOneStripeCoarse(void *v, int iJob, int iTotalJobs)
{
   CAutoRegistrar *vp = (CAutoRegistrar *) v;

   bool *pauseFlag = vp->pauseFlag;

   int xgrn = vp->xgrn;
   int ygrn = vp->ygrn;
   int subDivPerPixel = vp->subDivPerPixel;
   int corRangeSubDivs = vp->corRangeSubDivs;

   // divvying up the work
   int nRows = corRangeSubDivs / subDivPerPixel;

   int nUsableStripes = std::min<int>(nRows, vp->_nStripes);
   if (iJob >= nUsableStripes)
   {
      // More jobs than rows to operate on! Do nothing in this job!
      vp->_results[iJob].tcorr = -1000000000;
      vp->_results[iJob].xoffs =  0;
      vp->_results[iJob].yoffs =  0;
      vp->_results[iJob].retVal = 0;
      return 0;
   }

   int nominalPixelRowsPerStripe = nRows / nUsableStripes;
   int extraPixelRows = nRows - (nUsableStripes * nominalPixelRowsPerStripe);
   int numberOfDivRowsInThisStripe = (nominalPixelRowsPerStripe + ((iJob < extraPixelRows) ? 1 : 0))  * subDivPerPixel;
   int stripeStartOffset = ((iJob * nominalPixelRowsPerStripe) + std::min<int>(extraPixelRows, iJob)) * subDivPerPixel;

   // COARSE level in increments of 5/5 pel - corRange of 6 => 144 cycles

   float corr = -1000000000;
   float tcorr;
   for (int yoff = stripeStartOffset; yoff < stripeStartOffset + numberOfDivRowsInThisStripe; yoff += subDivPerPixel) {

      int ychn = ygrn + yoff;

      for (int xoff = 0; xoff < corRangeSubDivs; xoff+=subDivPerPixel) {

         if (pauseFlag != NULL) {
            if (*pauseFlag) {
              vp->_results[iJob].retVal = -1;
              return 0;
            }
         }

         int xchn = xgrn + xoff;

         tcorr = vp->computeCorrelation(xchn, ychn, 2);
         if (tcorr > corr) {
            corr = tcorr;
            vp->_results[iJob].tcorr = tcorr;
            vp->_results[iJob].xoffs =  xoff;
            vp->_results[iJob].yoffs =  yoff;
         }

         if (xoff == 0) continue;
         xchn = xgrn - xoff;

         tcorr = vp->computeCorrelation(xchn, ychn, 2);
         if (tcorr > corr) {
            corr = tcorr;
            vp->_results[iJob].tcorr = tcorr;
            vp->_results[iJob].xoffs = -xoff;
            vp->_results[iJob].yoffs =  yoff;
         }
      } // xoff

      if (yoff == 0) continue;
      ychn = ygrn - yoff;

      for (int xoff = 0; xoff < corRangeSubDivs; xoff+=subDivPerPixel) {

         if (pauseFlag != NULL) {
            if (*pauseFlag) {
              vp->_results[iJob].retVal = -1;
              return 0;
            }
         }

         int xchn = xgrn + xoff;

         tcorr = vp->computeCorrelation(xchn, ychn, 2);
         if (tcorr > corr) {
            corr = tcorr;
            vp->_results[iJob].tcorr = tcorr;
            vp->_results[iJob].xoffs =  xoff;
            vp->_results[iJob].yoffs = -yoff;
         }

         if (xoff == 0) continue;
         xchn = xgrn - xoff;

         tcorr = vp->computeCorrelation(xchn, ychn, 2);
         if (tcorr > corr) {
            corr = tcorr;
            vp->_results[iJob].tcorr = tcorr;
            vp->_results[iJob].xoffs = -xoff;
            vp->_results[iJob].yoffs = -yoff;
         }
      } // xoff
   } // yoff

   // valid return value
   vp->_results[iJob].retVal = 0;

   return 0;
}

int CAutoRegistrar::alignOneStripeFine(void *v, int iJob, int iTotalJobs)
{
   CAutoRegistrar *vp = (CAutoRegistrar *) v;

   bool *pauseFlag = vp->pauseFlag;

   int xgrn = vp->xgrn;
   int ygrn = vp->ygrn;
   int xbest = vp->_xbest;
   int ybest = vp->_ybest;

   // divvying up the work
   int nRows = 11;   // 5 + 1 + 5

   int nUsableStripes = std::min<int>(nRows, vp->_nStripes);
   if (iJob >= nUsableStripes)
   {
      // We have more jobs than rows to operate on. Skip this job.
      vp->_results[iJob].tcorr = -1000000000;
      vp->_results[iJob].xoffs =  0;
      vp->_results[iJob].yoffs =  0;
      vp->_results[iJob].retVal = 0;
      return 0;
   }

   int nominalRowsPerStripe = nRows / nUsableStripes;
   int extraRows = nRows - (nUsableStripes * nominalRowsPerStripe);
   int numberOfRowsInThisStripe = nominalRowsPerStripe + ((iJob < extraRows) ? 1 : 0);
   int stripeStartRow = (ybest - 5) + (iJob * nominalRowsPerStripe) + std::min<int>(extraRows, iJob);

   // FINE level in increments of 1/5 pel - 121 cycles

   float corr = -1000000000;
   float tcorr;
   for (int yoff = stripeStartRow; yoff < (stripeStartRow + numberOfRowsInThisStripe); ++yoff) {

      int ychn = ygrn + yoff;

      for (int xoff = (xbest - 5); xoff < (xbest + 6); ++xoff) {

         if (pauseFlag != NULL) {
            if (*pauseFlag) {
              vp->_results[iJob].retVal = -1;
              return 0;
            }
         }

         int xchn = xgrn + xoff;

         tcorr = vp->computeCorrelation(xchn, ychn);
         if (tcorr > corr) {
            corr = tcorr;
            vp->_results[iJob].tcorr = tcorr;
            vp->_results[iJob].xoffs = xoff;
            vp->_results[iJob].yoffs = yoff;
         }
      } // xoff
   } // yoff

   // valid return value
   vp->_results[iJob].retVal = 0;

   return 0;
}

////////////////////////////////////////////////////////////////////////////////





