#ifndef ThreeLayerToolH
#define ThreeLayerToolH

// ThreeLayer.h: interface for the CThreeLayer class.
//
/*
$Header: /usr/local/filmroot/Plugins/AlphaFilter/Attic/ThreeLayerTool.h,v 1.1.2.58 2009/09/17 20:06:29 tolks Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "HRTimer.h"
#include "IniFile.h"
#include "PropX.h"
#include "RegionOfInterest.h"
#include "Registrar.h"
#include "ToolObject.h"

#define RED_MASK  0xff0000ff
#define BLUE_MASK 0xffff0000

//////////////////////////////////////////////////////////////////////

// ThreeLayer's Tool Number (16-bit number that uniquely identifies this tool)
#define THREELAYER_TOOL_NUMBER    16

// ThreeLayer Tool Command Numbers
#define THL_BUTTON_PREVIEW_FRAME            1
#define THL_BUTTON_PREVIEW_NEXT_FRAME       2
#define THL_BUTTON_PREVIEW_ALL              3
#define THL_BUTTON_RENDER_FRAME             4
#define THL_BUTTON_RENDER_NEXT_FRAME        5
#define THL_BUTTON_RENDER_ALL               6
#define THL_BUTTON_STOP                     7
#define THL_BUTTON_PAUSE_OR_RESUME          8
#define THL_BUTTON_SET_RESUME_TC            9
#define THL_BUTTON_CAPTURE_PDL             10
#define THL_CMD_TOGGLE_PROCESSED_ORIGINAL  11
#define THL_CMD_ACTIVATE_RED               12
#define THL_CMD_ACTIVATE_RED_MASK          13
#define THL_CMD_ACTIVATE_BLUE              14
#define THL_CMD_ACTIVATE_BLUE_MASK         15
#define THL_CMD_ACTIVATE_BOTH              16
#define THL_CMD_ACTIVATE_BOTH_MASK         17
#define THL_CMD_TOGGLE_GANGED              18
#define THL_CMD_MASTER_RESET               19
#define THL_CMD_RESET_RED                  20
#define THL_CMD_RESET_BLUE                 21
#define THL_CMD_RESET_X                    22
#define THL_CMD_RESET_Y                    23
#define THL_CMD_CTRL_DN                    24
#define THL_CMD_CTRL_UP                    25
#define THL_CMD_SHIFT_DN                   26
#define THL_CMD_SHIFT_UP                   27
#define THL_CMD_POSITION_LFT               28
#define THL_CMD_POSITION_RGT               29
#define THL_CMD_POSITION_UP                30
#define THL_CMD_POSITION_DN                31
#define THL_CMD_SHIFT_POS_LFT              32
#define THL_CMD_SHIFT_POS_RGT              33
#define THL_CMD_SHIFT_POS_UP               34
#define THL_CMD_SHIFT_POS_DN               35
#define THL_CMD_CTRL_POS_LFT               36
#define THL_CMD_CTRL_POS_RGT               37
#define THL_CMD_CTRL_POS_UP                38
#define THL_CMD_CTRL_POS_DN                39
#define THL_CMD_CYCLE_OP_MODE_FWD          40
#define THL_CMD_CYCLE_OP_MODE_BWD          41
#define THL_CMD_ALIGN_SELECTED_CHANNEL     42
#define THL_CMD_LOAD_REFERENCE_FRAME       43
#define THL_CMD_GO_TO_REFERENCE_FRAME      44
#define THL_CMD_TOGGLE_WARP_MODE           45
#define THL_CMD_LEFT_BUTTON_DOWN           46
#define THL_CMD_ENTER_COLOR_PICK_MODE      47
#define THL_CMD_EXIT_COLOR_PICK_MODE       48
#define THL_BUTTON_CAPTURE_PDL_ALL_EVENTS  49
#define THL_CMD_TOGGLE_SHOW_MATBOX         50
#define THL_CMD_TOGGLE_PROCESSING_REGION   51
#define THL_CMD_TOGGLE_SHOW_ALIGN_PTS      52

//////////////////////////////////////////////////////////////////////
// Pixel Mask bit flags (copied from Repair.h)

#define RM_NONE 0x0        // do not use this pixel
#define RM_SELECTED 0x1    // defective pixel should be replaced
#define RM_SURROUND 0x2    // area around defective area
#define RM_BORDER 0x10     // border region that gets blended

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CColorPickTool;
class CThreeLayerParameters;
class CThreeLayerParameters;
class CAutoRegistrar;
class CRegistrar;
class CPixelRegionList;
class CDebrisParameters;
class CPDLElement;

//////////////////////////////////////////////////////////////////////
// Encryption for "CORRECT-ThreeLayer"
extern MTI_UINT32 FEATURE_CORRECT_ThreeLayer[];
#define ENV_CORRECT_AUTOFILT "9d9c31657498f76de8275b0de413ffced808e790bff7779503325bf42c0b6a1d"
#define ENK_CORRECT_AUTOFILT 0xf9218e4


//////////////////////////////////////////////////////////////////////

class CThreeLayer : public CToolObject
{
public:
   CThreeLayer(const string &newToolName, MTI_UINT32 **newFeatureTable);
   virtual ~CThreeLayer();

   // Tool startup and shutdowm
   virtual int toolInitialize(CToolSystemInterface *newSystemAPI);
   virtual int toolShutdown();
   virtual int toolActivate();
   virtual int toolDeactivate();
   virtual bool DoesToolPreserveHistory();
   virtual CToolProcessor* makeToolProcessorInstance(const bool *);

   // Tool GUI
   virtual int toolHide();      // Hide the tool's GUI
   virtual bool toolHideQuery(); // OK to hide the tool?
   virtual int toolShow();      // Show the tool's GUI
   virtual bool IsShowing(void);

   // Tool Event Handlers
   virtual int onUserInput(CUserInput &userInput);
   virtual int onToolCommand(CToolCommand &toolCommand);
   virtual int onMouseMove(CUserInput &userInput);
   virtual int onNewMarks();
   virtual int onChangingClip();
   virtual int onNewClip();
   virtual int onNewFraming();
   virtual int onRedraw(int frameIndex);
   virtual int onPlayerStop();

   // Tool Execution
   virtual int StartToolProcessing(EToolProcessingCommand processingType,
                                   bool newHighlightFlag);
   virtual int StopToolProcessing();

   // Tool Configuration Save and Restore
   int  InitClip();
   void ReadSettings();
   void WriteSettings();

   // PDL Interface
   int onCapturePDLEntry(CPDLElement &pdlEntryToolParams);
   int onGoToPDLEntry(CPDLEntry &pdlEntry);

   CBHook ClipChanging;   // Needed to call back on a change
   CBHook ClipChange;     // Needed to call back on a change
   CBHook MarksChange;    // Needed to call back on a change

public:

////////////////////////////////////////////////////////////////////////////////
//

#define MAJORSTATE_STOP                              0

#define MAJORSTATE_AUTO_PER_FRAME_HOME              10

#define MAJORSTATE_AUTO_PER_SHOT_HOME               20
#define   MAJORSTATE_AUTO_PER_SHOT_EDIT_IN          21
#define   MAJORSTATE_AUTO_PER_SHOT_EDIT_OUT         22
#define   MAJORSTATE_AUTO_PER_SHOT_EDIT_AVG         23

#define MAJORSTATE_MANUAL_HOME                      30
#define   MAJORSTATE_MANUAL_RED                     31
#define   MAJORSTATE_MANUAL_BLUE                    32
#define   MAJORSTATE_MANUAL_GANGED                  33

#define MAJORSTATE_REFERENCE_FRAME_HOME             40
#define   MAJORSTATE_REFERENCE_FRAME_EDIT_IN        41
#define   MAJORSTATE_REFERENCE_FRAME_EDIT_OUT       42
#define   MAJORSTATE_REFERENCE_FRAME_EDIT_REF       43

#define MAJORSTATE_ANALYSIS_EDIT_PTS                50
#define MAJORSTATE_ANALYSIS_EDIT_RNG                51

#define MAJORSTATE_PICK_MAT_COLOR                   60

//
////////////////////////////////////////////////////////////////////////////////
   void setMajorState(int, bool gui=true, bool showproc=false);
   int  getMajorState();

   int  getRenderState();

   void setResetMajorState(int);
   int  getResetMajorState();

#define OP_MODE_FWD  1
#define OP_MODE_BWD -1
   void CycleOperationMode(int);

   void ActivateRed(bool=false);
   void ActivateBlue(bool=false);
   void ActivateBoth(bool=false);
   void ToggleGanged();
   void ResetRedOffsets(bool x, bool y);
   void ResetBlueOffsets(bool x, bool y);
   void ResetRedAndBlueOffsets();
   void ResetXOffset();
   void ResetYOffset();
   bool JogOffsets(double xincr, double yincr);

   void JogMatBox(int, int, int, int);
   void setAllBox();
   RECT *GetAllBox();
   void ClearMatBox();
   void SetMatBoxAll();
   void SetMatBox(RECT *);
   RECT *GetMatBoxForAnalysis();
   RECT *GetMatBoxForProcessing();
   bool FindBoundingBox(int R, int G, int B, int X, int Y, int slack=50);
   void EnterColorPickMode(void);
   void ExitColorPickMode(void);
   bool IsInColorPickMode(void);
   void ClearColorPickMode();

   void ShowProcessed(bool);
   void ShowProcessedChanged(bool);
   bool ManualOffsetsZeroed();
   bool MarksAreValid();
   bool GetMarks(int *in, int *out);
   bool MarksAreUnchanged(int in, int out);
   int  ExecuteSingleFrame(int, bool render);
   int  ExecuteCurrentFrame(bool render);
   int  ExecuteNextFrame(bool render);
   int  ExecuteRangeProcess(bool render);
   int  CapturePDLEvent(bool all);
   int  ExecutePDLRangeProcess();
   void SuspendRangeProcess(bool stop);
   void ResumeRangeProcess();
   void SetEditIn(int);
   void SetEditOut(int);
   void SetEditAvg(int);
   void InitEditInOut();
   void InitEditAvg();
   void SetRefEditIn(int);
   void SetRefEditOut(int);
   void InitRefEditInOut();
   void SetReferenceFrame(int);
   void InitReferenceFrame();
   void ToggleWarpMode();
   void SetWarpMode(bool);
   void InitWarpMode();
   void SetPointsDim(int);
   int GetPointsDim();
   void InitPointsDim();
   void SetRangeDim(int);
   int GetRangeDim();
   void InitRangeDim();
   void SetPixelsDim(int);
   void InitPixelsDim();
   void SetAnalysisMatBox(bool);
   void SetProcessingMatBox(bool);
   void ToggleProcessingMatBox();
   void SetShowMatBox(bool);
   void ToggleShowMatBox();
   void InitResumeTimecode();
   int  ClipResumeFrame(int);
   void SetResumeFrame(int);
   void InitCumulativeOffsets();
   double NormalizeOffset(double);
   void ClearTransform(AffineCoeff *);
   void ReverseTransform(AffineCoeff *);
   void AddOffsetsToTransform(AffineCoeff *);
   void SetCombinedTransform();
   void ClearBaseTransform();
   void ClearAllOffsets();
   void ClearCombinedTransform();
   double GetWarpNorm(AffineCoeff *);
   void ToggleShowAlignPoints();

   void AlignCurrentFrameSelChannels(bool render);
   void AlignNextFrameSelChannels(bool render);
   void AlignBetweenMarksSelChannels(bool render);

   void LoadReferenceFrame();
   void GoToReferenceFrame();

private:

   void readBoundingBoxFromDesktopIni(ClipSharedPtr &clip);
   void writeBoundingBoxToDesktopIni(ClipSharedPtr &clip);

   void ExecutePauseSequence(int resumeUpperLimitArg, bool updateToolProcessingControlState);
   void ExecuteResumeSequence();
   void ExecuteStopSequence();
   void ExecuteFinishSequence();
   void ToNextRenderState();
   void TestAlignInForeground();
   int  AlignAndWritebackFrame(int, bool, bool *);
   int  AlignSingleFrame(int, bool *);
   void InitForCapture(int, bool *);
   int  CaptureSingleFrame(int);
   void SetCapturedTransform();
   void TransformAndWritebackFrame(int, bool);

   void BackgroundAlignRender(void *);
   static void BackgroundAlignRenderCallback(void *, void *);

private:

   CHRTimer *FrameTimer;

   string IniFileName;

   ClipSharedPtr curClip;
   string clipBinPath;
   string clipName;
   const CImageFormat *curImageFormat;
   RECT curFrameRect;
   CVideoFrameList *curFrameList;

   int ptBoxRadius;

   int resetMajorState;
   int prevMajorState;
   int majorState;

   bool showProc;

   bool renderFlag;

#define RENDER_STATE_ENDED           -2
#define RENDER_STATE_DISABLED        -1
#define RENDER_STATE_IDLE             0
#define RENDER_STATE_PAUSED           1
#define RENDER_STATE_IN_FRAMES        2
#define RENDER_STATE_ALIGN_ONE        3
#define RENDER_STATE_APPLY_ONE        4
#define RENDER_STATE_AVERAGE          5
#define RENDER_STATE_RUNNING          6
#define RENDER_STATE_OUT_FRAMES       7

   bool pauseFlag;
   bool oneFrame;
   int pauseMajorState; // takes effect after pause
   int savedRenderState;
   int renderState;
   int *renderStatePtr;

   bool marksValid;

   int _currentRenderFrame;

   // the reference frame
   int strRefFrame;

   // for averaging
   int framesAveraged;
   double frameStep;
   double avgFrame;

   // beg <= in <= out <= end
   int strBegFrame;
   int strInFrame;
   int strOutFrame;
   int strEndFrame;
   int strAlignOneFrame;
   int strApplyOneFrame;

   int strResumeLowerLim;
   int strResumeUpperLim;

   double jogIncr;

   bool analyzeMatBox;
   bool processMatBox;
   bool showMatBox;

   RECT allBox;

   RECT matBox;

   CColorPickTool *ColorPickTool;

   DEFINE_CBHOOK(ColorPickerColorChanged, CThreeLayer);
   DEFINE_CBHOOK(ColorPickerColorPicked, CThreeLayer);

   int matIncr;

#define MINEDTIN 0
#define DFLTEDTIN 2
#define MAXEDTIN 4
   int editInCount;
#define MINEDTOUT 0
#define DFLTEDTOUT 1
#define MAXEDTOUT 4
   int editOutCount;
#define MINEDTAVG 1
#define DFLTEDTAVG 1
   int editAverage;

#define MINREFEDTIN 0
#define DFLTREFEDTIN 2
#define MAXREFEDTIN 4
   int refEditInCount;
#define MINREFEDTOUT 0
#define DFLTREFEDTOUT 1
#define MAXREFEDTOUT 4
   int refEditOutCount;

   int mouseFrameX;
   int mouseFrameY;

   bool showAlignPts;
   int lastFrameAligned;

   double redOffsetX;
   double redOffsetY;
   double blueOffsetX;
   double blueOffsetY;

   AffineCoeff autoParams;

   int autoPreviewedFrame;

#define DFLTWRP false
   bool warpMode;
#define MINPTS_MOV   1
#define MAXPTS_MOV  32
#define DFLTPTS_MOV  4
#define MINPTS_WRP   4
#define MAXPTS_WRP  48
#define DFLTPTS_WRP 16
   int movPtsDim;
   int movWrpPtsDim;
   int pointsDim;
#define MINRNG  2
#define DFLTRNG 6
#define MAXRNG 32
   int movRngDim;
   int movWrpRngDim;
   int rangeDim;
#define MINPXLS 17
#define DFLTPXLS 17
#define MAXPXLS 81
   int pixelsDim;

   CAutoRegistrar *AutoRegistrar;

   CRegistrar *Registrar;

   CPixelRegionList *OrgPels;

public:

   static int AutoPerShotStateTable[];
   static int AutoPerShotEndFrameStateTable[];
   static int AutoPerShotMidFrameStateTable[];
   static int AutoPerFrameStateTable[];
   static int ManualStateTable[];
   static int ManualAlignOneStateTable[];
   static int ManualApplyOneStateTable[];
   static int ReferenceFrameStateTable[];
   static int ReferenceFrameOneFrameStateTable[];
   static int OutOnlyStateTable[];

   static string editInCountKey;
   static string editOutCountKey;
   static string editAverageKey;
   static string refEditInCountKey;
   static string refEditOutCountKey;
   static string referenceFrameKey;

   static string warpModeKey;
   static string movPointsDimKey;
   static string movWrpPointsDimKey;
   static string movRangeDimKey;
   static string movWrpRangeDimKey;

   static string analyzeMatKey;
   static string processMatKey;
   static string showMatKey;
};

extern CThreeLayer *GThreeLayerTool;       // Lives in

class CThreeLayerParameters
{
public:
   CThreeLayerParameters() { initOffsets(); };
   ~CThreeLayerParameters() {};

   /////////////////////////////////////////////////////////////////////////////

   void setResetMajorState(int state);

   int  getResetMajorState();

   void setShowProcessed(bool proc);

   bool getShowProcessed();

   void setEditCounts( int edtin,    int edtout, int edtavg,
                       int refedtin, int refedtout);

   void getEditCounts( int *edtin,    int *edtout, int *edtavg,
                       int *refedtin, int *refedtout);

   void initOffsets();

   void setOffsets(AffineCoeff *xform,
                   double roffx, double roffy,
                   double boffx, double boffy);

   void getOffsets(AffineCoeff *xform,
                   double *roffx, double *roffy,
                   double *boffx, double *boffy);

   void setAnalysisParams(bool wrpmod, int ptsdim, int rngdim);

   void getAnalysisParams(bool *wrpmod, int *ptsdim, int *rngdim);

   void setMatParams(RECT *matbox, bool analyzemat, bool processmat, bool showmat);

   void getMatParams(RECT *matbox, bool *analyzemat, bool *processmat, bool *showmat);

   void setReferenceFrame( int reffrm);

   void getReferenceFrame( int *reffrm);

   /////////////////////////////////////////////////////////////////////////////

   void writePDLEntry(CPDLElement &pdlEntryToolParams);

   void readPDLEntry(CPDLElement *);

private:

   int resetMajorState;

   bool showProcessed;

   int editInCount;
   int editOutCount;
   int editAverage;
   int refEditInCount;
   int refEditOutCount;
   int referenceFrame;

   double redOffsetX;
   double redOffsetY;
   double blueOffsetX;
   double blueOffsetY;

   AffineCoeff autoParams;

   bool warpMode;
   int pointsDim;
   int rangeDim;

   RECT matBox;

   bool analyzeMatBox;
   bool processMatBox;
   bool showMatBox;

public:

   static string ResetMajorStateKey;
   static string ShowProcessedKey;

   static string EditInCountKey;
   static string EditOutCountKey;
   static string EditAverageKey;
   static string RefEditInCountKey;
   static string RefEditOutCountKey;
   static string ReferenceFrameKey;

   static string RedOffsetXKey;
   static string RedOffsetYKey;
   static string BlueOffsetXKey;
   static string BlueOffsetYKey;

   static string ARedXKey;
   static string BRedXKey;
   static string CRedXKey;
   static string ARedYKey;
   static string BRedYKey;
   static string CRedYKey;
   static string ABluXKey;
   static string BBluXKey;
   static string CBluXKey;
   static string ABluYKey;
   static string BBluYKey;
   static string CBluYKey;

   static string WarpModeKey;
   static string PointsDimKey;
   static string RangeDimKey;

   static string MatBoxLeftKey;
   static string MatBoxTopKey;
   static string MatBoxRightKey;
   static string MatBoxBottomKey;

   static string AnalyzeMatKey;
   static string ProcessMatKey;
   static string ShowMatKey;
};

//////////////////////////////////////////////////////////////////////

class CThreeLayerToolParameters : public CToolParameters
{
public:
   CThreeLayerToolParameters()
    : CToolParameters(1, 1), param3L() {};

   virtual ~CThreeLayerToolParameters() { };
   CThreeLayerParameters& getParameterRef() { return param3L; }

private:
   CThreeLayerParameters param3L;
};

//////////////////////////////////////////////////////////////////////

#endif // !defined(THREELAYER_TOOL_H)
