/*
   File Name:  ThreeLayerGUIWin.h

   Header file for the Windows-based (Borland) ThreeLayer GUI

*/
//---------------------------------------------------------------------------

#ifndef GuiWinH
#define GuiWinH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MTIUNIT.h"
#include <ComCtrls.hpp>
#include <ToolWin.hpp>
#include <Graphics.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include "cspin.h"
#include "ThreeLayerTool.h"
#include <Mask.hpp>
#include "VTimeCodeEdit.h"
#include "HRTimer.h"
#include "ExecButtonsFrameUnit.h"
#include "ExecStatusBarUnit.h"
#include "SpinEditFrameUnit.h"
#include "ColorPanel.h"
#include <System.ImageList.hpp>

/////////////////////////////////////////////////////////////////////////////
// Forward Declarations

class CPDLElement;

/////////////////////////////////////////////////////////////////////////////

// Public interface
//
extern void CreateThreeLayerGUI(void);
extern void DestroyThreeLayerGUI(void);
extern bool ShowThreeLayerGUI(void);
extern bool HideThreeLayerGUI(void);
extern void GatherGUIParameters(CThreeLayerParameters &pdlEntryToolParams);
extern void CaptureGUIParameters(CPDLElement &toolParams);
extern void SetGUIParameters(CThreeLayerParameters &toolParams);
extern bool IsToolVisible(void);
extern void ToggleProcessedAndOriginal(bool forceFlag = false);
extern void SetSingleFramePreviewModeFrameIndex(void);
extern void ClearSingleFramePreviewModeFrameIndex(void);
//
//---------------------------------------------------------------------------
class TThreeLayerForm : public TMTIForm
{
__published:	// IDE-managed Components
        TTimer *DownTimer;
        TImageList *ButtonImageList;
	TColorPanel *ThreeLayerControlPanel;
        TPanel *StatusBar1;
		  TLabel *FPSLabel;
        TLabel *MSLabel;
        TPanel *Panel2;
        TLabel *Label9;
        TGroupBox *ReferenceFrameGroupBox;
   TLabel *RefInLabel;
   TLabel *RefAutoFramesLabel;
   TLabel *RefOutLabel;
   TSpinEditFrame *RefInSpinEdit;
        TSpeedButton *ReferenceFrameGoToSpeedButton;
        TSpeedButton *ReferenceFrameSetStartSpeedButton;
        VTimeCodeEdit *ReferenceFrameTCEdit;
        TGroupBox *AutoPerShotGroupBox;
        TGroupBox *AutoPerFrameGroupBox;
        TGroupBox *ManualGroupBox;
        TGroupBox *MattingGroupBox;
        TSpeedButton *TopUpButton;
        TSpeedButton *TopDownButton;
        TSpeedButton *BottomUpButton;
        TSpeedButton *BottomDownButton;
        TSpeedButton *LeftLeftButton;
        TSpeedButton *LeftRightButton;
        TSpeedButton *RightLeftButton;
        TSpeedButton *RightRightButton;
        TSpeedButton *ColorPickButton;
        TBitBtn *AllMatButton;
   TLabel *InLabel;
   TLabel *AutoFramesLabel;
   TLabel *OutLabel;
   TLabel *AverageLabel;
        TEdit *DummyEdit;
        TGroupBox *TargetViewGroupBox;
        TSpeedButton *ShowOriginalSpeedButton;
        TSpeedButton *ShowProcessedSpeedButton;
        TGroupBox *ManualOffsetsGroupBox;
        TPaintBox *RedChannelPaintBox;
        TPaintBox *BlueChannelPaintBox;
        TLabel *ReAnalyzeLabel;
        TLabel *GangLabel;
   TSpeedButton *AlignResetSpeedButton;
        TSpeedButton *UpSpeedButton;
        TSpeedButton *DownSpeedButton;
        TSpeedButton *LeftSpeedButton;
        TSpeedButton *RightSpeedButton;
        TCheckBox *GangCheckBox;
        TButton *RedResetButton;
        TButton *BlueResetButton;
        TButton *MasterResetButton;
        TCheckBox *ReAnalyzeCheckBox;
        TPanel *RedOffsetXPanel;
        TPanel *RedOffsetYPanel;
        TPanel *BlueOffsetXPanel;
        TPanel *BlueOffsetYPanel;
        TGroupBox *AnalysisGroupBox;
        TLabel *PointsLabel;
        TLabel *RangeLabel;
        TRadioButton *MoveRadioButton;
        TRadioButton *MoveWarpRadioButton;
        TPanel *WarpPanel;
        TLabel *WarpLabel;
   TLabel *ReferenceLabel;
        TLabel *BaseSettingsLabel;
        TLabel *RedBXLabel;
        TLabel *RedBYLabel;
        TLabel *BWarpLabel;
        TLabel *BluBXLabel;
        TLabel *BluBYLabel;
        TLabel *RedBaseXLabel;
        TLabel *RedBaseYLabel;
        TLabel *WarpBaseLabel;
        TLabel *BluBaseXLabel;
        TLabel *BluBaseYLabel;
        TLabel *RedXLabel;
        TLabel *RedYLabel;
        TLabel *BluXLabel;
        TLabel *BluYLabel;
        TRadioButton *ReferenceFrameRadioButton;
        TRadioButton *AutoPerShotRadioButton;
        TRadioButton *AutoPerFrameRadioButton;
        TRadioButton *ManualRadioButton;
        TCheckBox *AnalysisCBox;
        TCheckBox *ShowMattingCBox;
        TLabel *StatusMessageLabel;
        TLabel *StatusMessageValue;
        TLabel *ToolMessageValue;
        TTimer *ColorPickerStateTimer;
        TCheckBox *ProcessingCBox;
        TExecStatusBarFrame *ExecStatusBar;
        TTimer *RefreshTimer;
   TSpinEditFrame *RefOutSpinEdit;
   TSpinEditFrame *InSpinEdit;
   TSpinEditFrame *OutSpinEdit;
   TSpinEditFrame *AverageSpinEdit;
   TSpinEditFrame *PointsSpinEdit;
   TSpinEditFrame *RangeSpinEdit;
   TCheckBox *PreviewCurrentFrameOnlyCheckBox;
	TExecButtonsFrame *ExecButtonsToolbar;
        void __fastcall RedPaintBoxPaint(TObject *Sender);
        void __fastcall BluePaintBoxPaint(TObject *Sender);
        void __fastcall MasterResetButtonClick(TObject *Sender);
        void __fastcall RedResetButtonClick(TObject *Sender);
        void __fastcall BlueResetButtonClick(TObject *Sender);
        void __fastcall RedButtonClick(TObject *Sender);
        void __fastcall BlueButtonClick(TObject *Sender);
        void __fastcall LeftSpeedButtonClick(TObject *Sender);
        void __fastcall RightSpeedButtonClick(TObject *Sender);
        void __fastcall UpSpeedButtonClick(TObject *Sender);
        void __fastcall DownSpeedButtonClick(TObject *Sender);
        void __fastcall GangCheckBoxClick(TObject *Sender);

        void __fastcall RedOffsetXPanelClick(TObject *Sender);
        void __fastcall RedOffsetYPanelClick(TObject *Sender);
        void __fastcall BlueOffsetXPanelClick(TObject *Sender);
        void __fastcall BlueOffsetYPanelClick(TObject *Sender);
        void __fastcall ShowOriginalSpeedButtonOnClick(TObject *Sender);
        void __fastcall ShowProcessedSpeedButtonOnClick(TObject *Sender);
        void __fastcall DummyEditFormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall DummyEditFormKeyUp(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall AlignSpeedButtonClick(TObject *Sender);
        void __fastcall AutoPerFrameRadioButtonOnEnter(TObject *Sender);
        void __fastcall AutoPerShotRadioButtonOnEnter(TObject *Sender);
        void __fastcall ManualRadioButtonOnEnter(TObject *Sender);
        void __fastcall ReferenceFrameRadioButtonOnEnter(TObject *Sender);
        void __fastcall ReferenceFrameSetStartSpeedButtonClick(
          TObject *Sender);
        void __fastcall ReferenceFrameGoToSpeedButtonClick(
          TObject *Sender);
        void __fastcall ReferenceFrameOnChange(TObject *Sender);
        void __fastcall ReferenceFrameOnEnter(TObject *Sender);
        void __fastcall ReferenceFrameOnExit(TObject *Sender);
        void __fastcall ReferenceFrameOnEnterKey(TObject *Sender);
        void __fastcall ReferenceFrameOnEscapeKey(TObject *Sender);
        void __fastcall MoveRadioButtonOnEnter(TObject *Sender);
        void __fastcall MoveWarpRadioButtonOnEnter(TObject *Sender);
        void __fastcall AnalysisCBoxClick(TObject *Sender);
        void __fastcall TopUpButtonClick(TObject *Sender);
        void __fastcall TopDownButtonClick(TObject *Sender);
        void __fastcall LeftLeftButtonClick(TObject *Sender);
        void __fastcall LeftRightButtonClick(TObject *Sender);
        void __fastcall RightLeftButtonClick(TObject *Sender);
        void __fastcall RightRightButtonClick(TObject *Sender);
        void __fastcall BottomUpButtonClick(TObject *Sender);
        void __fastcall BottomDownButtonClick(TObject *Sender);
        void __fastcall AllMatButtonClick(TObject *Sender);
        void __fastcall ColorPickButtonClick(TObject *Sender);
        void __fastcall ShowMattingCBoxClick(TObject *Sender);
        void __fastcall ColorPickerStateTimerTimer(TObject *Sender);
        void __fastcall ProcessingCBoxClick(TObject *Sender);
        void __fastcall DownTimerTimer(TObject *Sender);
        void __fastcall AutoMoveButtonMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall AutoMoveButtonMouseUp(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall ExecStatusBarElapsedTimerTimer(TObject *Sender);
        void __fastcall RefreshTimerTimer(TObject *Sender);
   void __fastcall AlignResetSpeedButtonClick(TObject *Sender);
   void __fastcall PreviewCurrentFrameOnlyCheckBoxClick(TObject *Sender);
   void __fastcall ExecButtonsToolbar_ButtonPressedNotifier(TObject *Sender);



private:	// User declarations

   void showForm();
   friend bool ShowThreeLayerGUI(void);
   void hideForm();
   friend bool HideThreeLayerGUI(void);

   int refreshTimer;

   // for edit boxes
   int savedMajorState;

   // for checkboxes - set to these when they become enabled
   bool procMatSetState;
   bool showMatSetState;

   Graphics::TBitmap *redViewer;
   Graphics::TBitmap *blueViewer;

   string ThreeLayerIniFileName;

#define OPERATING_MODE_AUTO_PER_SHOT        1
#define OPERATING_MODE_AUTO_PER_FRAME       2
#define OPERATING_MODE_MANUAL               3
#define OPERATING_MODE_REFERENCE_FRAME      4
#define OPERATING_MODE_COUNT                4
   int localOpMode;

#define AUTO_PER_SHOT_MAX_FRAMES 4
   int localEditIn;
   int   savedEditIn;
   int localEditOut;
   int   savedEditOut;
   int localEditAvg;
   int   savedEditAvg;

   double localJogIncrement;

#define LITE 106
   double localRedBaseX;
   double localRedBaseY;
   double localRedOffsetX;
   double localRedOffsetY;
   bool   localRedActive;

   double localBaseWarp;
   double localWarp;

   double localBlueBaseX;
   double localBlueBaseY;
   double localBlueOffsetX;
   double localBlueOffsetY;
   bool   localBlueActive;

#define REFERENCE_FRAME_MAX_FRAMES 4
   int localRefEditIn;
   int   savedRefEditIn;
   int localRefEditOut;
   int   savedRefEditOut;
   int localReferenceFrame;
   int   savedReferenceFrame;

   bool localWarpMode;
   int localPointsDim;
   int   savedPointsDim;
   int localRangeDim;
   int   savedRangeDim;
   int localPixelsDim;
   int   savedPixelsDim;

   int localResumeFrame;
   int   savedResumeFrame;

   DEFINE_CBHOOK(ClipHasChanged, TThreeLayerForm);

   void ExecSetResumeTimecode(int frameIndex);
   void ExecSetResumeTimecodeToDefault();

   TObject *GlobalSender;
   void ExecButtonsToolbarPreviewFrame();
   void ExecButtonsToolbarPreviewAll();
   void ExecButtonsToolbarRenderFrame();
   void ExecButtonsToolbarRenderAll();
   void ExecButtonsToolbarPause();
   void ExecButtonsToolbarResume();
   void ExecButtonsToolbarStop();
   void ExecButtonsToolbarCapturePDL();
   void ExecButtonsToolbarCaptureAllEventsToPDL();
   void ExecButtonsToolbarSetResumeTimecodeToCurrent();
   void ExecButtonsToolbarGoToResumeTimecode();

public:		// User declarations

   void setBusyCursor(void);
   void setIdleCursor(void);

   void formCreate(void);
   void formDestroy(void);
   void formPaint(void);

   void drawRectangle(Graphics::TBitmap *bitmap, RECT *rect,
                      MTI_UINT8 r, MTI_UINT8 g, MTI_UINT8 b);
#define MARGIN 20
   void drawOffsetRectangle(Graphics::TBitmap *bitmap,
                            int margin,
                            double, double,
                            MTI_UINT8 r, MTI_UINT8 g, MTI_UINT8 b);

   void drawIndicatorRectangle(Graphics::TBitmap *bitmap,
                               bool active);

   void drawOutlineRectangle(Graphics::TBitmap *bitmap,
                            int margin,
                            double, double,
                            MTI_UINT8 r, MTI_UINT8 g, MTI_UINT8 b);

   void enableManualOffsetsPanel(bool);
   void setMajorState(int);
   void setGUIRunning(bool);
   void setGUIRunningThru();
   void setGUIPause(bool);
   void setGUIExecButtons(bool);
   void enableMattingControls(bool, bool);

   void UpdateOperationMode(int);
   int  GetOperationMode();
   void SetJogIncrement(double);
   void UpdateColorPickButton(bool);
   void UpdateRedBaseX(double);
   void UpdateRedBaseY(double);
   void UpdateRedOffsetX(double);
   void UpdateRedOffsetY(double);
   void EnableRedIndicator(bool);
   void UpdateBaseWarp(double);
   void UpdateWarp(double);
   void UpdateBlueBaseX(double);
   void UpdateBlueBaseY(double);
   void UpdateBlueOffsetX(double);
   void UpdateBlueOffsetY(double);
   void EnableBlueIndicator(bool);
   void Error1Show();
   void Error2Show();
   void Error3Show();
   void Error4Show();
   bool ToSimpleInteger(AnsiString, int *);
   void UpdateEditIn(int);
   void LoadEditIn();
   void JogEditIn(int);
   void UpdateEditOut(int);
   void LoadEditOut();
   void JogEditOut(int);
   void UpdateEditAvg(int);
   void LoadEditAvg();
   void JogEditAvg(int);
   void UpdateTargetView(bool);
   void UpdateRefEditIn(int);
   void LoadRefEditIn();
   void JogRefEditIn(int);
   void UpdateRefEditOut(int);
   void LoadRefEditOut();
   void JogRefEditOut(int);
   void UpdateReferenceFrame(int);
   void LoadReferenceFrame();
   void JogReferenceFrame(int);
   int  GetReferenceFrame();
   void UpdateWarpMode(bool);
   void UpdatePointsDim(int);
   void LoadPointsDim();
   void JogPointsDim(int);
   void UpdateRangeDim(int);
   void LoadRangeDim();
   void JogRangeDim(int);
   void UpdatePixelsDim(int);
   void LoadPixelsDim();
   void UpdateAnalysisMatBox(bool);
   void UpdateProcessingMatBox(bool);
   void UpdateShowMatBox(bool);
   void UpdateResumeFrame(int);
   void LoadResumeFrame();
   void SetRefreshTimer(int);

   bool HandleToolEvent(const FosterParentEventInfo &eventInfo);

   bool doesFormUseOwnWindow() { return false; };

   __fastcall TThreeLayerForm(TComponent* Owner);

   virtual bool WriteSettings(CIniFile *ini, const string &IniSection);  // Writes the configuration to the file
   virtual bool ReadSettings(CIniFile *ini, const string &IniSection);   // Reads configuration.

   DEFINE_CBHOOK(InSpinEditHasChanged, TThreeLayerForm);
   DEFINE_CBHOOK(OutSpinEditHasChanged, TThreeLayerForm);
   DEFINE_CBHOOK(AverageSpinEditHasChanged, TThreeLayerForm);
   DEFINE_CBHOOK(RefInSpinEditHasChanged, TThreeLayerForm);
   DEFINE_CBHOOK(RefOutSpinEditHasChanged, TThreeLayerForm);
   DEFINE_CBHOOK(PointsSpinEditHasChanged, TThreeLayerForm);
   DEFINE_CBHOOK(RangeSpinEditHasChanged, TThreeLayerForm);

   void SetSpinEditHooks();
   void RemoveSpinEditHooks();

   int singleFramePreviewFrameIndex;
   void setSingleFramePreviewModeFrameIndex();
   void clearSingleFramePreviewModeFrameIndex();
   void toggleProcessedAndOriginal(bool forceFlag);

   void BounceOutOfColorPickMode();
};

//---------------------------------------------------------------------------

//void SetGUIParameters(CAutoFilterParameters &toolParams);

//---------------------------------------------------------------------------
extern PACKAGE TThreeLayerForm *ThreeLayerForm;
extern MTI_UINT32 *ThreeLayerFeatureTable[];

//---------------------------------------------------------------------------
#endif
