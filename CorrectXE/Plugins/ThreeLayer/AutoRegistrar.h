// AutoRegistrar.h: interface for the CAutoRegistrar class.
//
//////////////////////////////////////////////////////////////////////

#ifndef AUTOREGISTRARH
#define AUTOREGISTRARH

#include "machine.h"
#include "mthread.h"

#if 0
// green correlation sq 31x31
#define RRAD  15
#define RDIM  (RRAD*2 + 1)
#define SUBDIV 5
#define RSUB (RDIM*SUBDIV + 1)
//
// merit sq 17x17
#define DRAD  8
#define DDIM  (DRAD*2 + 1)

// red-blue correlation sq 17x17
#define ARAD (DRAD)
#define ARADSUB (ARAD*SUBDIV)
// correlation range +- in 1/5 pels
//#define CORSUB ((RRAD - ARAD)*SUBDIV)
#define CORSUB 30
#endif

#define MINPTS_MOV    1
#define MINPTS_WRP    4

#define RED_CHANNEL   0
#define GREEN_CHANNEL 1
#define BLUE_CHANNEL  2

#define MAX_AUTOREGISTRAR_THREADS 16

class CHRTimer;
class CImageFormat;
struct AffineCoeff;

struct ShiftPoint
{
   // location of pt
   int x;
   int y;

   float rating;

   // shift values at pt
   double redXShft;
   double redYShft;
   double blueXShft;
   double blueYShft;
};

struct ThreeByThree
{
   double a11;
   double a12;
   double a13;
   double a21;
   double a22;
   double a23;
   double a31;
   double a32;
   double a33;
};

struct StripeData
{
   float tcorr;

   int xoffs,
       yoffs;

   int retVal;
};

class CAutoRegistrar
{
public:
   CAutoRegistrar(int nStripes = 0);

   virtual ~CAutoRegistrar();

   void InitTransform(const CImageFormat *imgFmt, // image format
                      RECT *matbox,               // points zone
                      bool *pauseflag,            // -> pause flag
                      int numfrms,                // number of frames
                      int numpts,                 // points per frame
                      int corrng,                 // search range
                      int smldiam = 17,           // correlation block
                      int subdivs = 5);           // subdivs per pel

   int RegisterFrame(MTI_UINT16 *frm);

   void GetLastFramesRegistrationTransform(AffineCoeff *xfrm,
                                           bool usewarp);
   void OpenForReadingAlignPts();

   int GetNextAlignPt(int *x, int *y);

   void GetAllFramesRegistrationTransform(AffineCoeff *xfrm,
                                          bool usewarp);

   int RegisterFrame(MTI_UINT16 *frm,
                     const CImageFormat *imgFmt,
                     RECT *matbox,
                     bool *pauseflag,
                     AffineCoeff *xfrm,
                     bool usewarp,
                     int numpts,
                     int corrng,
                     int smldiam = 17,
                     int subdivs = 5);
private:

   static void null(void *v, int iJob);
   static int alignOneStripeCoarse(void *v, int iJob, int iTotalJobs);
   static int alignOneStripeFine(void *v, int iJob, int iTotalJobs);

   #define Rflt (float)1.6
   void initializeGradientKernels();

   bool chooseBestPoint(int lft, int top, int rgt, int bot);

   void markBestPoint(int x, int y, int lft, int top, int rgt, int bot);

   int chooseBestPoints(int quota, int lft, int top, int rgt, int bot);

   double det(ThreeByThree *matrix);

   void simpleTransform(int ibeg, int iend, AffineCoeff * reslt);

   void affineTransform(int ibeg, int iend, AffineCoeff * reslt);

   float normalizeShift(float shft);

   void generateSubpixelGradients(int chn,
                                  int xctr, int yctr,
                                  float *ixgrad, float *iygrad);

   float computeCorrelation(int xchn, int ychn, int step = 1);

   int alignChannelWithGreenSlow(int   *xoff,    int   *yoff);

   int alignChannelWithGreenFast(int   *xoff,    int   *yoff);

   int alignChannelAtPoint(int chn,
                           int x, int y,
                           int   *xoff,  int   *yoff);
private:

   int retVal;

   bool *pauseFlag;

   int ibegGoodPt;
   int ibegTestPt;
   int iendGoodPt;
   int inxtGoodPt;
   ShiftPoint *goodPts;

   float *IxGrn;
   float *IyGrn;
   float *IxChn;
   float *IyChn;

   int xgrn, ygrn;
   float grnxmean;
   float grnymean;

   int _xbest, _ybest;

   StripeData _results[MAX_AUTOREGISTRAR_THREADS];

#define NUM_BINS 1000
   float maxScaledGradient;
   float thresholdScaledGradient;
   int totalGradientPts;
   int *ScaledGradientBin;

   float *ScaledGradient;

   int framesAligned;   // total frames aligned
   int numGoodPts;      // number of good pts obtained

   int numFrms;         // max number of frames
   int numPts;          // alignment points per frame

   int subDivPerPixel;  // subdivisions per pixel
   int corRange;        // +- correlation range in pixels
   int corRangeSubDivs; // correlation range in subdivisions
   int smlRadius;       // size of moveable correl sq
   int smlRadiusSubDivs;// size of moveable correl sq in subpixels
   int lrgRadius;       // size of static   correl sq
   int margin;          // safety margin in half-scale frame
   int lrgRadiusSubDivs;// size of static   correl sq in subpixels

   int smlDiam;         // size of merit sq & moveable correlation sq
   int lrgDiam;         // size of static correlation sq
   int gradDiam;        // size of array to build subpixel gradients

   float gradX[3][3];
   float gradY[3][3];

   float intGradX[4][4];
   float intGradY[4][4];

   MTI_UINT16 *frame;

   RECT hmatBox;

   RECT analyzeBox;

   int frameWidth,
       frameHeight;
   int totalPels;

   int subScale;
   int hframeWidth,
       hframeHeight;
   int htotalPels;

   ///////////////////////////////////////////////////////////////

   int _nStripes; // number of stripes in frame (1,2,4,8, or 16)

   ///////////////////////////////////////////////////////////////
};

//////////////////////////////////////////////////////////////////////

#endif // !defined(AUTOREGISTRAR_H)