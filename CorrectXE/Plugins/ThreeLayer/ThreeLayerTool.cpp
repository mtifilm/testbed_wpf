// ThreeLayerFilter.cpp: implementation of the CThreeLayer class.
//
/*
*/
//////////////////////////////////////////////////////////////////////

#include "bthread.h"
#include "ThreeLayerTool.h"
#include "NavigatorTool.h"
#include "ClipAPI.h"
#include "ColorPickTool.h"
#include "ImageFormat3.h"
#include "JobManager.h"
#include "Mask.h"
#include "MathFunctions.h"
#include "MTIDialogs.h"
#include "MTIsleep.h"
#include "AutoRegistrar.h"
#include "Registrar.h"
#include "PixelRegions.h"
#include "PDL.h"
#include "SysInfo.h"
#include "ToolCommand.h"
#include "ToolCommandTable.h"
#include "ToolSystemInterface.h"
#include "ToolUserInputMap.h"
#include <exception>

#ifdef __BORLANDC__
#include "ThreeLayerGUIWin.h"
#else
#include "GuiSgi/ThreeLayerGUISGI.h"
#endif

CHRTimer HRT;                    // Temporary preformance counters
float glbfTotalElapsedTime;
float glbfIterationTime;
int   glbIterations=0;

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

static MTI_UINT16 ThreeLayerNumber = THREELAYER_TOOL_NUMBER;

// -------------------------------------------------------------------------
// ThreeLayer Tool Default Keyboard and Mouse Button Configuration

static CUserInputConfiguration::SConfigItem ThreeLayerDefaultConfigItems[] =
{
   // ThreeLayer Tool Command             Modifiers      Action
   //                                       + Key
   // ----------------------------------------------------------------
   //### = Removed per Larry's spec 2/21/2014
   // ----------------------------------------------------------------

   { THL_BUTTON_PREVIEW_FRAME,            " D                    KeyDown   " },
   { THL_BUTTON_PREVIEW_NEXT_FRAME,       " Ctrl+D               KeyDown   " },
   { THL_BUTTON_PREVIEW_ALL,              " Shift+D              KeyDown   " },
   { THL_BUTTON_RENDER_FRAME,             " G                    KeyDown   " },
   { THL_BUTTON_RENDER_NEXT_FRAME,        " Ctrl+G               KeyDown   " },
   { THL_BUTTON_RENDER_ALL,               " Shift+G              KeyDown   " },
   { THL_BUTTON_STOP,                     " Ctrl+Space           KeyDown   " },
   { THL_BUTTON_PAUSE_OR_RESUME,          " Space                KeyDown   " },
   { THL_BUTTON_SET_RESUME_TC,            " Return               KeyDown   " },
   { THL_BUTTON_CAPTURE_PDL,              " ,                    KeyDown   " },
	{ THL_BUTTON_CAPTURE_PDL_ALL_EVENTS,   " Shift+,              KeyDown   " },

   { THL_CMD_TOGGLE_PROCESSED_ORIGINAL,   " T                    KeyDown   " },
   { THL_CMD_TOGGLE_SHOW_MATBOX,          " Ctrl+T               KeyDown   " },

   { THL_CMD_ACTIVATE_RED,                " 1                    KeyDown   " },
   { THL_CMD_ACTIVATE_BOTH,               " 2                    KeyDown   " },
   { THL_CMD_ACTIVATE_BLUE,               " 3                    KeyDown   " },
   { THL_CMD_CYCLE_OP_MODE_FWD,           " 4                    KeyDown   " },
//### { THL_CMD_CYCLE_OP_MODE_BWD,           " Shift+4              KeyDown   " },
   { THL_CMD_TOGGLE_PROCESSING_REGION,    " 5                    KeyDown   " },
   { THL_CMD_TOGGLE_WARP_MODE,            " 6                    KeyDown   " },

//### { THL_CMD_ACTIVATE_RED_MASK,           " Ctrl+num1            KeyDown   " },
//### { THL_CMD_ACTIVATE_BLUE_MASK,          " Ctrl+num3            KeyDown   " },
//### { THL_CMD_ACTIVATE_BOTH_MASK,          " Ctrl+num0            KeyDown   " },

   { THL_CMD_MASTER_RESET,                " Shift+C              KeyDown   " },
   { THL_CMD_MASTER_RESET,                " A                    KeyDown   " },
//### { THL_CMD_RESET_RED,                   " Shift+1              KeyDown   " },
//### { THL_CMD_RESET_BLUE,                  " Shift+3              KeyDown   " },
//### { THL_CMD_RESET_X,                     " Ctrl+Shift+X         KeyDown   " },
//### { THL_CMD_RESET_Y,                     " Ctrl+Shift+Y         KeyDown   " },

   { THL_CMD_POSITION_LFT,                " Left                 KeyDown   " },
   { THL_CMD_POSITION_RGT,                " Right                KeyDown   " },
   { THL_CMD_POSITION_UP,                 " Up                   KeyDown   " },
   { THL_CMD_POSITION_DN,                 " Down                 KeyDown   " },

   { THL_CMD_CTRL_DN,                     " Ctrl+Ctrl            KeyDown   " },
   { THL_CMD_CTRL_UP,                     " Ctrl                 KeyUp     " },
   { THL_CMD_SHIFT_DN,                    " Shift+Shift          KeyDown   " },
   { THL_CMD_SHIFT_UP,                    " Shift                KeyUp     " },

   { THL_CMD_SHIFT_POS_LFT,               " Shift+Left           KeyDown   " },
   { THL_CMD_SHIFT_POS_RGT,               " Shift+Right          KeyDown   " },
   { THL_CMD_SHIFT_POS_UP,                " Shift+Up             KeyDown   " },
   { THL_CMD_SHIFT_POS_DN,                " Shift+Down           KeyDown   " },

   { THL_CMD_CTRL_POS_LFT,                " Ctrl+Left            KeyDown   " },
   { THL_CMD_CTRL_POS_RGT,                " Ctrl+Right           KeyDown   " },
   { THL_CMD_CTRL_POS_UP,                 " Ctrl+Up              KeyDown   " },
   { THL_CMD_CTRL_POS_DN,                 " Ctrl+Down            KeyDown   " },

//### { THL_CMD_LOAD_REFERENCE_FRAME,        " Ctrl+Shift+E         KeyDown   " },
//### { THL_CMD_GO_TO_REFERENCE_FRAME,       " Ctrl+Shift+R         KeyDown   " },
//### { THL_CMD_LEFT_BUTTON_DOWN,            " LButton              ButtonDown" },

//### Left the following in, but they are not in the spec
   { THL_CMD_ENTER_COLOR_PICK_MODE,       " Ctrl+Shift+Shift     KeyDown   " },
   { THL_CMD_ENTER_COLOR_PICK_MODE,       " Ctrl+Shift+Ctrl      KeyDown   " },
   { THL_CMD_EXIT_COLOR_PICK_MODE,        " Ctrl+Shift           KeyUp     " },
   { THL_CMD_EXIT_COLOR_PICK_MODE,        " Shift+Ctrl           KeyUp     " },
   { THL_CMD_TOGGLE_SHOW_ALIGN_PTS,       " F12                  KeyDown   " },

};

static CUserInputConfiguration *ThreeLayerDefaultUserInputConfiguration
= new CUserInputConfiguration(sizeof(ThreeLayerDefaultConfigItems)
                              /sizeof(CUserInputConfiguration::SConfigItem),
                               ThreeLayerDefaultConfigItems);

// -------------------------------------------------------------------------
// ThreeLayer Tool Command Table

static CToolCommandTable::STableEntry ThreeLayerCommandTableEntries[] =
{
   // ThreeLayer Tool Command            ThreeLayer Tool Command String
   // -------------------------------------------------------------

   { THL_CMD_POSITION_LFT,                "THL_CMD_POSITION_LFT" },
   { THL_CMD_POSITION_RGT,                "THL_CMD_POSITION_RGT" },
   { THL_CMD_POSITION_UP,                 "THL_CMD_POSITION_UP" },
   { THL_CMD_POSITION_DN,                 "THL_CMD_POSITION_DN" },
   { THL_CMD_SHIFT_POS_LFT,               "THL_CMD_SHIFT_POS_LFT" },
   { THL_CMD_SHIFT_POS_RGT,               "THL_CMD_SHIFT_POS_RGT" },
   { THL_CMD_SHIFT_POS_UP,                "THL_CMD_SHIFT_POS_UP" },
   { THL_CMD_SHIFT_POS_DN,                "THL_CMD_SHIFT_POS_DN" },
   { THL_CMD_CTRL_POS_LFT,                "THL_CMD_CTRL_POS_LFT" },
   { THL_CMD_CTRL_POS_RGT,                "THL_CMD_CTRL_POS_RGT" },
   { THL_CMD_CTRL_POS_UP,                 "THL_CMD_CTRL_POS_UP" },
   { THL_CMD_CTRL_POS_DN,                 "THL_CMD_CTRL_POS_DN" },

};

static CToolCommandTable *ThreeLayerCommandTable
   = new CToolCommandTable(sizeof(ThreeLayerCommandTableEntries)
                           /sizeof(CToolCommandTable::STableEntry),
                           ThreeLayerCommandTableEntries);

// state tables for the background rendering
// these are mode-specific & end with "idle"
int CThreeLayer::AutoPerShotStateTable[] =
{
   RENDER_STATE_IN_FRAMES,
   RENDER_STATE_AVERAGE,
   RENDER_STATE_RUNNING,
   RENDER_STATE_OUT_FRAMES,
   RENDER_STATE_IDLE,
};

int CThreeLayer::AutoPerShotEndFrameStateTable[] =
{
   RENDER_STATE_ALIGN_ONE,
   RENDER_STATE_APPLY_ONE,
   RENDER_STATE_IDLE,
};

int CThreeLayer::AutoPerShotMidFrameStateTable[] =
{
   RENDER_STATE_AVERAGE,
   RENDER_STATE_APPLY_ONE,
   RENDER_STATE_IDLE,
};

int CThreeLayer::AutoPerFrameStateTable[] =
{
   RENDER_STATE_OUT_FRAMES,
   RENDER_STATE_IDLE
};

int CThreeLayer::ManualStateTable[] =
{
   RENDER_STATE_RUNNING,
   RENDER_STATE_IDLE,
};

int CThreeLayer::ManualAlignOneStateTable[] =
{
   RENDER_STATE_ALIGN_ONE,
   RENDER_STATE_IDLE,
};

int CThreeLayer::ManualApplyOneStateTable[] =
{
   RENDER_STATE_APPLY_ONE,
   RENDER_STATE_IDLE,
};

int CThreeLayer::ReferenceFrameStateTable[] =
{
   RENDER_STATE_IN_FRAMES,
   RENDER_STATE_ALIGN_ONE,
   RENDER_STATE_RUNNING,
   RENDER_STATE_OUT_FRAMES,
   RENDER_STATE_IDLE,
};

int CThreeLayer::ReferenceFrameOneFrameStateTable[] =
{
   RENDER_STATE_ALIGN_ONE,
   RENDER_STATE_APPLY_ONE,
   RENDER_STATE_IDLE,
};

int CThreeLayer::OutOnlyStateTable[] =
{
   RENDER_STATE_OUT_FRAMES,
   RENDER_STATE_IDLE,
};

////////////////////////////////////////////////////////////////////////////////

string CThreeLayer::editInCountKey                    = "EditInCount";
string CThreeLayer::editOutCountKey                   = "EditOutCount";
string CThreeLayer::editAverageKey                    = "EditAverage";
string CThreeLayer::refEditInCountKey                 = "RefEditInCount";
string CThreeLayer::refEditOutCountKey                = "RefEditOutCount";
string CThreeLayer::warpModeKey                       = "WarpMode";
string CThreeLayer::movPointsDimKey                   = "MovPointsDim";
string CThreeLayer::movWrpPointsDimKey                = "MovWrpPointsDim";
string CThreeLayer::movRangeDimKey                    = "MovRangeDim";
string CThreeLayer::movWrpRangeDimKey                 = "MovWrpRangeDim";
string CThreeLayer::analyzeMatKey                     = "AnalyzeMat";
string CThreeLayer::processMatKey                     = "ProcessMat";
string CThreeLayer::showMatKey                        = "ShowMat";

////////////////////////////////////////////////////////////////////////////////

string CThreeLayerParameters::ResetMajorStateKey      = "ResetMajorState";
string CThreeLayerParameters::ShowProcessedKey        = "ShowProcessed";
string CThreeLayerParameters::EditInCountKey          = "EditInCount";
string CThreeLayerParameters::EditOutCountKey         = "EditOutCount";
string CThreeLayerParameters::EditAverageKey          = "EditAverage";
string CThreeLayerParameters::RefEditInCountKey       = "RefEditInCount";
string CThreeLayerParameters::RefEditOutCountKey      = "RefEditOutCount";
string CThreeLayerParameters::ReferenceFrameKey       = "ReferenceFrame";
string CThreeLayerParameters::RedOffsetXKey           = "RedLayerHorzOffset";
string CThreeLayerParameters::RedOffsetYKey           = "RedLayerVertOffset";
string CThreeLayerParameters::BlueOffsetXKey          = "BlueLayerHorzOffset";
string CThreeLayerParameters::BlueOffsetYKey          = "BlueLayerVertOffset";

string CThreeLayerParameters::ARedXKey                = "ARedX";
string CThreeLayerParameters::BRedXKey                = "BRedX";
string CThreeLayerParameters::CRedXKey                = "CRedX";
string CThreeLayerParameters::ARedYKey                = "ARedY";
string CThreeLayerParameters::BRedYKey                = "BRedY";
string CThreeLayerParameters::CRedYKey                = "CRedY";
string CThreeLayerParameters::ABluXKey                = "ABluX";
string CThreeLayerParameters::BBluXKey                = "BBluX";
string CThreeLayerParameters::CBluXKey                = "CBluX";
string CThreeLayerParameters::ABluYKey                = "ABluY";
string CThreeLayerParameters::BBluYKey                = "BBluY";
string CThreeLayerParameters::CBluYKey                = "CBluY";

string CThreeLayerParameters::WarpModeKey             = "WarpMode";
string CThreeLayerParameters::PointsDimKey            = "PointsDim";
string CThreeLayerParameters::RangeDimKey             = "RangeDim";
string CThreeLayerParameters::MatBoxLeftKey           = "MatBoxLeft";
string CThreeLayerParameters::MatBoxTopKey            = "MatBoxTop";
string CThreeLayerParameters::MatBoxRightKey          = "MatBoxRight";
string CThreeLayerParameters::MatBoxBottomKey         = "MatBoxBottom";
string CThreeLayerParameters::AnalyzeMatKey           = "AnalyzeMat";
string CThreeLayerParameters::ProcessMatKey           = "ProcessMat";
string CThreeLayerParameters::ShowMatKey              = "ShowMat";

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CThreeLayer::CThreeLayer(const string &newToolName, MTI_UINT32 **newFeatureTable)
: CToolObject(newToolName, newFeatureTable)
{
   // assume marks are invalid
   marksValid = false;

   // default increment for layer displacement
   jogIncr = 1.0;

   // default increment for matting box edge displacement
   matIncr = 1;

   // for writing & reading settings
   IniFileName = CPMPIniFileName("Registration");

   // default values important here
   warpMode  = DFLTWRP;     // false;
   pointsDim = MINPTS_MOV;  // 1
   rangeDim  = DFLTRNG;     // 6;
   pixelsDim = DFLTPXLS;    // 17;

   // point box radius
   ptBoxRadius = 1;

   // allocate a timer
   FrameTimer = new CHRTimer;

   // allocate an aligner with max
   // processor count multithreading
   AutoRegistrar = new CAutoRegistrar();

   // allocate a layer-shifter with max
   // processor count multithreading
   Registrar = new CRegistrar();

   // allocate a color-pick tool
   ColorPickTool = new CColorPickTool;

   // allocate a (null) pixel-region list
   OrgPels = new CPixelRegionList();

   // background render thread is now started in toolActivate
   renderState = RENDER_STATE_ENDED;

   // need these independent of the checkbox state
}

CThreeLayer::~CThreeLayer()
{
}

// ===================================================================
//
// Function:    toolInitialize
//
// Description: Called when the ToolManager wants to initialize
//              the ThreeLayer Tool
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================
int CThreeLayer::toolInitialize(CToolSystemInterface *newSystemAPI)
{
   int retVal = 0;

   // Initialize the CThreeLayer's base class, i.e., CToolObject
   // This must be done before the CThreeLayer initializes itself
   retVal = initBaseToolObject(ThreeLayerNumber, newSystemAPI,
                               ThreeLayerCommandTable,
                               ThreeLayerDefaultUserInputConfiguration);
   if (retVal != 0)
      {
      // ERROR: initBaseToolObject failed
      TRACE_0(errout << "Error in ThreeLayer Tool, initBaseToolObject failed "
                     << retVal);
      return retVal;
      }

   // Tool-specific initialization here

   // Create the Tool's GUI
   CreateThreeLayerGUI();

   // color pick tool needs system ptr
   ColorPickTool->loadSystemAPI(newSystemAPI);

   SET_CBHOOK(ColorPickerColorChanged, ColorPickTool->newColorHoveredOver);
   SET_CBHOOK(ColorPickerColorPicked, ColorPickTool->newColorPicked);

   return 0;

}   // end toolInitialize

// ===================================================================
//
// Function:    toolActivate
//
// Description: Called when a tool is being Activated
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CThreeLayer::toolActivate()
{
   // First
   int retVal = CToolObject::toolActivate();
   if (retVal != 0)
   {
      return retVal;
   }

   // Disable the Tool Infrastructure's cache because we write frames
   // behind the T.I.'s back!
   getSystemAPI()->GetToolFrameCache()->EnableCaching(false);

   // Get rid of any review regions that might be present on the screen
   // and clear the provisional frame index so we don't get handed the
   // provisional frame instead of a real frame
   getSystemAPI()->ClearProvisional(false);

   // Init for the clip
   retVal = InitClip();
   if (retVal != 0)
      return retVal;

   // eliminates weird initial TAB behavior
   // and sets up for any "onRedraw"
   majorState = resetMajorState = prevMajorState =
      MAJORSTATE_MANUAL_HOME;

   // GUI shows "MANUAL"
   ThreeLayerForm->UpdateOperationMode(OPERATING_MODE_MANUAL);
   ThreeLayerForm->setMajorState(MAJORSTATE_MANUAL_HOME);

   // clear the registration buffers
   // NOTE: Now done in enterRegistrationMode()
   //getSystemAPI()->unloadRegistrationFrame();

   // tell Displayer what tool we're in
   getSystemAPI()->enterRegistrationMode();

   // all zeroes
   ClearCombinedTransform();

   // init the Reference Frame timecode
   InitReferenceFrame();

   // init the Resume timecode
   InitResumeTimecode();

   // read matbox from desktop.ini
   auto curClip = getSystemAPI()->getClip();
   if (curClip != nullptr)
      readBoundingBoxFromDesktopIni(curClip);

   // matbox must be available before
   // this is executed (see above)
   ReadSettings();

   // init the Target View
   ClearSingleFramePreviewModeFrameIndex();
   showProc = false;
   getSystemAPI()->setDisplayProcessed(false);
   ThreeLayerForm->UpdateTargetView(false);

   // default
   showAlignPts = false;

   // last frame aligned
   lastFrameAligned = -1;

   // status bar = IDLE
   ThreeLayerForm->ExecStatusBar->SetIdle(true);

   // Start up background render thread
   BThreadSpawn(CThreeLayer::BackgroundAlignRenderCallback,this);

   // refresh the current frame
   getSystemAPI()->refreshFrameCached();

   JobManager jobManager;
	jobManager.SetCurrentToolCode("3l");

	getSystemAPI()->SetToolNameForUserGuide("ThreeLayerTool");

   return 0;
}

// ===================================================================
//
// Function:    toolDeactivate
//
// Description: Called when a tool is being deactivated
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CThreeLayer::toolDeactivate()
{
   getSystemAPI()->AcceptGOV();   // Auto-accept GOV if pending

   // Get rid of any review regions that might be present on the screen
   getSystemAPI()->ClearProvisional(true);

  // suspend the background render thread
  pauseFlag = true;
  renderState = RENDER_STATE_DISABLED;
  int sleepCount = 1000;
  while ((renderState != RENDER_STATE_ENDED)&&(--sleepCount > 0)) {
     MTImillisleep(1);
  }

   // write the "settings"
   WriteSettings();

   // write the clip's bounding box to desktop.ini
   auto curClip = getSystemAPI()->getClip();
   if (curClip != nullptr)
      writeBoundingBoxToDesktopIni(curClip);

   // tell Displayer we're back in Nav mode
   getSystemAPI()->exitRegistrationMode();

   // clear color pick mode
   if (IsInColorPickMode())
      ThreeLayerForm->ColorPickButtonClick(nullptr);

   // do this because an "onRedraw" can be called
   // before "toolActivate" is called (!)
   SetMatBoxAll();

   // Last
   return CToolObject::toolDeactivate();
}

// this tool does not preserve history
bool CThreeLayer::DoesToolPreserveHistory()
{
   return false;
}

// ===================================================================
//
// Function:    toolShutdown
//
// Description: Called when the Tool Manager wants to shut down the
//              ThreeLayer Tool, typically when the main program is
//              shutting down.
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================
int CThreeLayer::toolShutdown()
{
	int retVal = 0;

	getSystemAPI()->AcceptGOV(); // Auto-accept GOV if pending

	delete OrgPels; // delete null pixel-region list
	delete ColorPickTool; // delete color pick tool
	delete Registrar; // delete layer-shifter
	delete AutoRegistrar; // delete aligner
	delete FrameTimer; // delete frame timer

	// destroy the GUI
	DestroyThreeLayerGUI();
	GThreeLayerTool = nullptr;

	// Shutdown the ThreeLayer Tool's base class, i.e., CToolObject.
	// This must be done after the ThreeLayer shuts down itself
	retVal = shutdownBaseToolObject();
	if (retVal != 0)
	{
		// ERROR: shutdownBaseToolObject failed
		return retVal;
	}

	return retVal;
}

CToolProcessor* CThreeLayer::makeToolProcessorInstance(
                                           const bool *newEmergencyStopFlagPtr)
{
   return nullptr;
}

// ===================================================================
//
// Function:    onUserInput
//
// Description: ThreeLayer Tool's User Input Handler
//
// Arguments:   CUserInput &userInput
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================
int CThreeLayer::onUserInput(CUserInput &userInput)
{
   // default: user input will be passed back to the tool manager
   int retVal = TOOL_RETURN_CODE_NO_ACTION;

   // If we're disabled or there's no clip, don't do anything
   if ((!IsDisabled()) && IsShowing() && getSystemAPI()->isAClipLoaded())
   {
      // pass the event to the colorpicker
      switch (userInput.action)
      {
         case USER_INPUT_ACTION_MOUSE_BUTTON_DOWN:
            retVal = ColorPickTool->onMouseDown(userInput);
            break;
         case USER_INPUT_ACTION_MOUSE_BUTTON_UP:
            retVal = ColorPickTool->onMouseUp(userInput);
            break;
         default:
            // Don't care about other actions.
            break;
      }
   }

   // If we didn't handle the mouse event, feed it back through the
   // onToolCommand interface!
   if (retVal == TOOL_RETURN_CODE_NO_ACTION)
   {
      retVal = CToolObject::onUserInput(userInput);
   }

   return retVal;
}

// ===================================================================
//
// Function:    onToolCommand
//
// Description: ThreeLayer Tool's Command Handler
//
// Arguments:   CToolCommand &toolCommand
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================
int CThreeLayer::onToolCommand(CToolCommand &toolCommand)
{
   int commandNumber = toolCommand.getCommandNumber();
   int Result = TOOL_RETURN_CODE_NO_ACTION;
   TRACE_3(errout << "ThreeLayer Tool command number " << commandNumber);

   // Dispatch loop for ThreeLayer Tool commands

   switch (commandNumber) {

      case THL_BUTTON_PREVIEW_FRAME:

         {
            AlignCurrentFrameSelChannels(false);
            //TestAlignInForeground();
         }

         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_BUTTON_PREVIEW_NEXT_FRAME:

         {
            AlignNextFrameSelChannels(false);
         }

         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_BUTTON_PREVIEW_ALL:

         {
            AlignBetweenMarksSelChannels(false);
         }

         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_BUTTON_RENDER_FRAME:

         {
            AlignCurrentFrameSelChannels(true);
         }

         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_BUTTON_RENDER_NEXT_FRAME:

         {
            AlignCurrentFrameSelChannels(true);
         }

         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_BUTTON_RENDER_ALL:

         AlignBetweenMarksSelChannels(true);

         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_BUTTON_STOP:

         SuspendRangeProcess(true);

         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_BUTTON_PAUSE_OR_RESUME:
        {
           if (renderState == RENDER_STATE_IDLE) {

              Result = TOOL_RETURN_CODE_NO_ACTION;
           }
           else if (renderState == RENDER_STATE_PAUSED) {

              ResumeRangeProcess();
              Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                        TOOL_RETURN_CODE_PROCESSED_OK);
           }
           else {

              SuspendRangeProcess(false);
              Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                        TOOL_RETURN_CODE_PROCESSED_OK);
           }
        }

      break;

      case THL_BUTTON_SET_RESUME_TC:

      break;

      case THL_BUTTON_CAPTURE_PDL:

         CapturePDLEvent(false);

         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

		case THL_BUTTON_CAPTURE_PDL_ALL_EVENTS:

         CapturePDLEvent(true);

         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_TOGGLE_PROCESSED_ORIGINAL:

         if (renderState == RENDER_STATE_IDLE)
            ToggleProcessedAndOriginal(true);

         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_ACTIVATE_RED:

         ActivateRed(false);
         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_ACTIVATE_RED_MASK:

         ActivateRed(true);
         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_ACTIVATE_BLUE:

         ActivateBlue(false);
         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_ACTIVATE_BLUE_MASK:

         ActivateBlue(true);
         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_ACTIVATE_BOTH:

         ActivateBoth(false);
         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_ACTIVATE_BOTH_MASK:

         ActivateBoth(true);
         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_TOGGLE_GANGED:

         ToggleGanged();
         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_MASTER_RESET:

         ResetRedAndBlueOffsets();

         // NEW: master reset shows "original view"
         ShowProcessed(false);

         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_RESET_RED:

         ResetRedOffsets(true, true);
         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_RESET_BLUE:

         ResetBlueOffsets(true, true);
         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_RESET_X:

         ResetXOffset();
         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_RESET_Y:

         ResetYOffset();
         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      // ARROW KEYS

      case THL_CMD_CTRL_DN:

         jogIncr = 0.2;
         ThreeLayerForm->SetJogIncrement(jogIncr);
         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_SHIFT_DN:

         jogIncr = 5.0;
         matIncr = 40;
         ThreeLayerForm->SetJogIncrement(jogIncr);
         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_CTRL_UP:
      case THL_CMD_SHIFT_UP:

         jogIncr = 1.0;
         matIncr = 1;
         ThreeLayerForm->SetJogIncrement(jogIncr);
         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_POSITION_LFT:

         JogOffsets(-1.0, 0.0);

         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_SHIFT_POS_LFT:

         JogOffsets(-5.0, 0.0);

         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_CTRL_POS_LFT:

         JogOffsets(-0.2, 0.0);

         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_POSITION_RGT:

         JogOffsets(1.0, 0.0);

         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_SHIFT_POS_RGT:

         JogOffsets(5.0, 0.0);

         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_CTRL_POS_RGT:

         JogOffsets(0.2, 0.0);

         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_POSITION_UP:
      case THL_CMD_SHIFT_POS_UP:
      case THL_CMD_CTRL_POS_UP:

         if (!JogOffsets( 0.0, -jogIncr)) {

            if (majorState == MAJORSTATE_AUTO_PER_SHOT_EDIT_IN) {

               ThreeLayerForm->JogEditIn(1);
            }
            else if (majorState == MAJORSTATE_AUTO_PER_SHOT_EDIT_OUT) {

               ThreeLayerForm->JogEditOut(1);
            }
            else if (majorState == MAJORSTATE_AUTO_PER_SHOT_EDIT_AVG) {

               ThreeLayerForm->JogEditAvg(1);
            }
            else if (majorState == MAJORSTATE_REFERENCE_FRAME_EDIT_IN) {

               ThreeLayerForm->JogRefEditIn(1);;
            }
            else if (majorState == MAJORSTATE_REFERENCE_FRAME_EDIT_OUT) {

               ThreeLayerForm->JogRefEditOut(1);
            }
            else if (majorState == MAJORSTATE_REFERENCE_FRAME_EDIT_REF) {

               ThreeLayerForm->JogReferenceFrame(1);
            }
            else if (majorState == MAJORSTATE_ANALYSIS_EDIT_PTS) {

               ThreeLayerForm->JogPointsDim(1);
            }
            else if (majorState == MAJORSTATE_ANALYSIS_EDIT_RNG) {

               ThreeLayerForm->JogRangeDim(1);
            }
         }
         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_POSITION_DN:
      case THL_CMD_SHIFT_POS_DN:
      case THL_CMD_CTRL_POS_DN:

         if (!JogOffsets( 0.0, jogIncr)) {

            if (majorState == MAJORSTATE_AUTO_PER_SHOT_EDIT_IN) {

               ThreeLayerForm->JogEditIn(-1);
            }
            else if (majorState == MAJORSTATE_AUTO_PER_SHOT_EDIT_OUT) {

               ThreeLayerForm->JogEditOut(-1);
            }
            else if (majorState == MAJORSTATE_AUTO_PER_SHOT_EDIT_AVG) {

               ThreeLayerForm->JogEditAvg(-1);
            }
            else if (majorState == MAJORSTATE_REFERENCE_FRAME_EDIT_IN) {

               ThreeLayerForm->JogRefEditIn(-1);
            }
            else if (majorState == MAJORSTATE_REFERENCE_FRAME_EDIT_OUT) {

               ThreeLayerForm->JogRefEditOut(-1);
            }
            else if (majorState == MAJORSTATE_REFERENCE_FRAME_EDIT_REF) {

               ThreeLayerForm->JogReferenceFrame(-1);
            }
            else if (majorState == MAJORSTATE_ANALYSIS_EDIT_PTS) {

               ThreeLayerForm->JogPointsDim(-1);
            }
            else if (majorState == MAJORSTATE_ANALYSIS_EDIT_RNG) {

               ThreeLayerForm->JogRangeDim(-1);
            }
         }
         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_CYCLE_OP_MODE_FWD:

         CycleOperationMode(1);
         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_CYCLE_OP_MODE_BWD:

         CycleOperationMode(OPERATING_MODE_COUNT-1);
         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_LOAD_REFERENCE_FRAME:

         LoadReferenceFrame();
         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_GO_TO_REFERENCE_FRAME:

         GoToReferenceFrame();
         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_TOGGLE_WARP_MODE:

         ToggleWarpMode();
         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_ENTER_COLOR_PICK_MODE:

         if (!IsInColorPickMode()) {
            EnterColorPickMode();
            ThreeLayerForm->ColorPickButton->Down = true;
            ThreeLayerForm->ColorPickerStateTimer->Enabled = true;
         }
         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_EXIT_COLOR_PICK_MODE:

         if (IsInColorPickMode()) {
            ExitColorPickMode();
            ThreeLayerForm->ColorPickButton->Down = false;
            ThreeLayerForm->ColorPickerStateTimer->Enabled = false;
         }
         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_TOGGLE_SHOW_MATBOX:

         ToggleShowMatBox();

         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_TOGGLE_PROCESSING_REGION:

         ToggleProcessingMatBox();

         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      case THL_CMD_TOGGLE_SHOW_ALIGN_PTS:

         ToggleShowAlignPoints();

         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
      break;

      default:
         //Result = TOOL_RETURN_CODE_NO_ACTION;
      break;
   }

   return(Result);

} // onToolCommand

// ===================================================================
//
// Function:    onMouseMove
//
// Description: PaintTool Tool's Mouse Move Handler
//
// Arguments:   CUserInput &userInput
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================

int CThreeLayer::onMouseMove(CUserInput &userInput)
{
   // If we're disabled or there's no clip, don't do anything
   if ((!IsDisabled()) && IsShowing() && getSystemAPI()->isAClipLoaded())
   {
      ColorPickTool->onMouseMove(userInput);
   }

   mouseFrameX = (int)(getSystemAPI()->dscaleXClientToFrame(userInput.windowX) + 0.5);
   mouseFrameY = (int)(getSystemAPI()->dscaleYClientToFrame(userInput.windowY) + 0.5);

   // pass mouse move thru so panning will continue to work
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onNewMarks
//
// Description: Notification of Change in Marks
//
// Arguments:   none
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================

int CThreeLayer::onNewMarks()
{
   if (IsDisabled() || !IsActive())
      return TOOL_RETURN_CODE_NO_ACTION;

   // init the Resume timecode
   InitResumeTimecode();

   // reset the GUI to reflect the new situation
   ThreeLayerForm->setGUIExecButtons(true);

   MarksChange.Notify();

   return 0;
}

// ===================================================================
//
// Function:    onChangingClip
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CThreeLayer::onChangingClip()
{
   if (IsDisabled() || !IsActive())
      return TOOL_RETURN_CODE_NO_ACTION;

   ClipChanging.Notify();

   // write the clip's bounding box to desktop.ini
   auto curClip = getSystemAPI()->getClip();
   if (curClip != nullptr)
      writeBoundingBoxToDesktopIni(curClip);

   getSystemAPI()->AcceptGOV();   // Auto-accept GOV if pending
   getSystemAPI()->ClearProvisional(false);

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

// ===================================================================
//
// Function:    onNewClip
//
// Description: PaintTool Tool's new clip handler
//
// Arguments:   none
//
// Returns:     return TOOL_RETURN_CODE_NO_ACTION;
//
// ===================================================================

int CThreeLayer::onNewClip()
{
   if (!(IsLicensed() && IsShowing()))
      return TOOL_RETURN_CODE_NO_ACTION;

   // Wait a minute... do we need to disable or re-enable the tool?
   if (isItOkToUseToolNow())
      getSystemAPI()->EnableTool();
   else
      getSystemAPI()->DisableTool(getToolDisabledReason());

   // Auto-accept GOV if pending
   getSystemAPI()->ClearProvisional(false);

   // This allows the tool to notify the gui
   ClipChange.Notify();

   if (IsInColorPickMode())
      ThreeLayerForm->ColorPickButtonClick(nullptr);

   // read bounding box from desktop.ini
   auto curClip = getSystemAPI()->getClip();
   if (curClip != nullptr)
      readBoundingBoxFromDesktopIni(curClip);
//
//   // this will cause a REDRAW. Are we ready?
//   getSystemAPI()->SetMaskAllowed(false);

   // clear the registration buffers
   getSystemAPI()->unloadRegistrationFrame();

   // this does a master reset, => VIEWER & refreshes
   ResetRedAndBlueOffsets();

   // init the Reference Frame timecode
   InitReferenceFrame();

   // init the Resume timecode
   InitResumeTimecode();

   // init the Target View
   ShowProcessed(false);

   // default
   showAlignPts = false;

   // last frame aligned
   lastFrameAligned = -1;

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

// ===================================================================
//
// Function:    onNewFraming
//
// Description: PaintTool Tool's framing handler
//
// Arguments:   none
//
// Returns:     return TOOL_RETURN_CODE_NO_ACTION;
//
// ===================================================================

int CThreeLayer::onNewFraming()
{
   if (IsDisabled() || !IsActive())
      return TOOL_RETURN_CODE_NO_ACTION;

   getSystemAPI()->ClearProvisional(true);

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

// ===================================================================
//
// Function:    onRedraw
//
// Description: PaintTool Tool's redraw handler
//
// Arguments:   frame index
//
// Returns:     return TOOL_RETURN_CODE_NO_ACTION;
//
// ===================================================================

int CThreeLayer::onRedraw(int frameindex)
{
   if (IsDisabled() || !IsActive())
      return TOOL_RETURN_CODE_NO_ACTION;

   // show the mat box if show is on
   if (analyzeMatBox && showMatBox) {
      getSystemAPI()->setGraphicsColor(SOLID_BLUE);
      getSystemAPI()->drawRectangleFrame(&matBox);
   }

   // show the align pts if on the right frame
   if ((frameindex == lastFrameAligned)&&
       (resetMajorState == MAJORSTATE_MANUAL_HOME)&&
       showAlignPts) {

      int x, y;

      getSystemAPI()->setGraphicsColor(SOLID_YELLOW);

      AutoRegistrar->OpenForReadingAlignPts();
      while (AutoRegistrar->GetNextAlignPt(&x, &y) != -1) {

         RECT ptBox;
         ptBox.left   = x-ptBoxRadius;
         ptBox.top    = y-ptBoxRadius;
         ptBox.right  = x+ptBoxRadius;
         ptBox.bottom = y+ptBoxRadius;

         getSystemAPI()->drawRectangleFrame(&ptBox);
      }
   }

   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onPlayerStop
//
// Description: PaintTool Tool's onPlayerStop handler
//
// Arguments:   none
//
// Returns:     return TOOL_RETURN_CODE_NO_ACTION;
//
// ===================================================================

int CThreeLayer::onPlayerStop()
{
   //KKKKK SuspendRangeProcess(true);

   return (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
;
}

int CThreeLayer::StartToolProcessing(EToolProcessingCommand processingType,
                                   bool newHighlightFlag)
{
   getSystemAPI()->AcceptGOV();
   //??? getSystemAPI()->AcceptProvisional(false);

   switch (processingType) {

      case TOOL_PROCESSING_CMD_INVALID:
      break;

      case TOOL_PROCESSING_CMD_CONTINUE:
      break;

      case TOOL_PROCESSING_CMD_PAUSE:
         SuspendRangeProcess(false);
      break;

      case TOOL_PROCESSING_CMD_PREVIEW_1:
      break;

      case TOOL_PROCESSING_CMD_PREVIEW:
      break;

      case TOOL_PROCESSING_CMD_RENDER_1:
      break;

      case TOOL_PROCESSING_CMD_RENDER:
         ExecutePDLRangeProcess();
      break;

      case TOOL_PROCESSING_CMD_STOP:
         SuspendRangeProcess(true);
      break;

      case TOOL_PROCESSING_CMD_PREPROCESS:
      break;

      default:
      break;
   }

   return 0; /* CToolObject::StartToolProcessing(processingType, newHighlightFlag); */
}

int CThreeLayer::StopToolProcessing()
{
   SuspendRangeProcess(true);

   return 0;
}

// ===================================================================
//
// Function:    onCapturePDLEntry
//
// Description: PaintTool Tool's PDL capture handler
//
// Arguments:   tool parameters
//
// Returns:     return TOOL_RETURN_CODE_NO_ACTION;
//
// ===================================================================

int CThreeLayer::onCapturePDLEntry(CPDLElement &toolParams)
{
   if (IsDisabled())
      return 0;

   CThreeLayerParameters params;

   params.setResetMajorState(resetMajorState);
   params.setShowProcessed(showProc);

   params.setEditCounts(editInCount,    editOutCount, editAverage,
                        refEditInCount, refEditOutCount);

   params.setReferenceFrame(strRefFrame);

   params.setAnalysisParams(warpMode, pointsDim, rangeDim);

   // exc manual render - base params are zero
   if (resetMajorState != MAJORSTATE_MANUAL_HOME) {
      ClearTransform(&autoParams);
   }
   params.setOffsets(&autoParams,
                     redOffsetX, redOffsetY, blueOffsetX, blueOffsetY);

   params.setMatParams(&matBox, analyzeMatBox, processMatBox, showMatBox);

   params.writePDLEntry(toolParams);

   return 0;
}

// ===================================================================
//
// Function:    onGoToPDLEntry
//
// Description: PaintTool Tool's PDL execution handler
//
// Arguments:   PDL entry
//
// Returns:     return TOOL_RETURN_CODE_NO_ACTION;
//
// ===================================================================

int CThreeLayer::onGoToPDLEntry(CPDLEntry &pdlEntry)
{
   if (IsDisabled())
      return 0;

   CPDLEntry_Tool *pdlEntryTool = pdlEntry.toolList[0];
   CPDLElement *toolAttribRoot = pdlEntryTool->parameters;

   if (toolAttribRoot->GetChildElementCount() == 1) {

      CPDLElement *toolAttribs = toolAttribRoot->GetChildElement(0);
      if (toolAttribs->GetElementName() != "LayerOffsets")
         return 0;

      CThreeLayerParameters params;
      params.readPDLEntry(toolAttribs);

      // put it in a known state
      setResetMajorState(params.getResetMajorState());

      // show the correct processed/original state
      ShowProcessed(params.getShowProcessed());

      // get the edit-in & edit-out counts
      params.getEditCounts(&editInCount,    &editOutCount, &editAverage,
                           &refEditInCount, &refEditOutCount);
      ThreeLayerForm->UpdateEditIn (editInCount);
      ThreeLayerForm->UpdateEditOut(editOutCount);
      ThreeLayerForm->UpdateEditAvg(editAverage);
      ThreeLayerForm->UpdateRefEditIn (refEditInCount);
      ThreeLayerForm->UpdateRefEditOut(refEditOutCount);

      // get the reference frame index
      params.getReferenceFrame(&strRefFrame);
      ThreeLayerForm->UpdateReferenceFrame(strRefFrame);

      // get analysis params
      params.getAnalysisParams(&warpMode, &pointsDim, &rangeDim);

      // update the GUI
      ThreeLayerForm->UpdateWarpMode(warpMode);
      ThreeLayerForm->UpdatePointsDim(pointsDim);
      ThreeLayerForm->UpdateRangeDim(rangeDim);

      // get the manual offsets
      params.getOffsets(&autoParams,
                        &redOffsetX, &redOffsetY, &blueOffsetX, &blueOffsetY);

      ThreeLayerForm->UpdateRedBaseX(autoParams.CredX);
      ThreeLayerForm->UpdateRedBaseY(autoParams.CredY);
      ThreeLayerForm->UpdateBlueBaseX(autoParams.CbluX);
      ThreeLayerForm->UpdateBlueBaseY(autoParams.CbluY);

      ThreeLayerForm->UpdateRedOffsetX(redOffsetX);
      ThreeLayerForm->UpdateRedOffsetY(redOffsetY);
      ThreeLayerForm->UpdateBlueOffsetX(blueOffsetX);
      ThreeLayerForm->UpdateBlueOffsetY(blueOffsetY);

      if (resetMajorState == MAJORSTATE_MANUAL_HOME) {
         ThreeLayerForm->UpdateWarp(GetWarpNorm(&autoParams));
      }

      // show effect of manual offsets by forcing Target View = PROCESSED
      SetCombinedTransform();
      showProc = true;
      getSystemAPI()->setDisplayProcessed(true);
      ThreeLayerForm->UpdateTargetView(true);

      // get mat params & xmit mat box to displayer
      RECT newmat;
      params.getMatParams(&newmat, &analyzeMatBox, &processMatBox, &showMatBox);
      SetMatBox(&newmat);

      // update the GUI
      ThreeLayerForm->UpdateAnalysisMatBox(analyzeMatBox);
      ThreeLayerForm->UpdateProcessingMatBox(processMatBox);
      ThreeLayerForm->UpdateShowMatBox(showMatBox);

      getSystemAPI()->refreshFrameCached();
   }

   return 0;
}

// ===================================================================
//
// Function:    toolShow
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CThreeLayer::toolShow()
{
   ShowThreeLayerGUI();

   getSystemAPI()->SetGOVToolProgressMonitor(ThreeLayerForm->ExecStatusBar);

   if (!isItOkToUseToolNow())
      getSystemAPI()->DisableTool(getToolDisabledReason());

   return 0;
}

bool CThreeLayer::IsShowing(void)
{
   return(IsToolVisible());
}

// ===================================================================
//
// Function:    toolHide
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CThreeLayer::toolHide()
{
   getSystemAPI()->AcceptGOV();   // Auto-accept GOV if pending

   HideThreeLayerGUI();
   doneWithTool();
   return 0;
}

// ===================================================================
//
// Function:    toolHideQuery
//
// Description: tells ToolMgr if it's OK to hide the tool
//
// Arguments:
//
// Returns:
//
// ===================================================================

bool CThreeLayer::toolHideQuery()
{
   if (renderState != RENDER_STATE_IDLE) {

      _MTIErrorDialog("You must stop the processing tool.");
      return false;
   }

   return true;
}

// ---------------------------------------------------------------------------

void CThreeLayer::readBoundingBoxFromDesktopIni(ClipSharedPtr &clip)
{
   SetMatBoxAll();
   CBinManager binManager;
   CIniFile *desktopIni = binManager.openClipDesktop(clip);
   const string iniSection = "BlueBoundingBox";
   if (desktopIni != nullptr)
   {
      RECT bbox;
      bbox.left   = desktopIni->ReadInteger(iniSection, "Left",   matBox.left);
      bbox.top    = desktopIni->ReadInteger(iniSection, "Top",    matBox.top);
      bbox.right  = desktopIni->ReadInteger(iniSection, "Right",  matBox.right);
      bbox.bottom = desktopIni->ReadInteger(iniSection, "Bottom", matBox.bottom);
      if (bbox.left >= 0 && bbox.top >= 0 &&
          bbox.right > bbox.left && bbox.bottom > bbox.top)
      {
         SetMatBox(&bbox);
      }

      DeleteIniFileDiscardChanges(desktopIni);
   }
}

void CThreeLayer::writeBoundingBoxToDesktopIni(ClipSharedPtr &clip)
{
   CBinManager binManager;
   CIniFile *desktopIni = binManager.openClipDesktop(clip);
   const string iniSection = "BlueBoundingBox";
   if (desktopIni != nullptr)
   {
      desktopIni->WriteInteger(iniSection, "Left",   matBox.left);
      desktopIni->WriteInteger(iniSection, "Top",    matBox.top);
      desktopIni->WriteInteger(iniSection, "Right",  matBox.right);
      desktopIni->WriteInteger(iniSection, "Bottom", matBox.bottom);

      DeleteIniFile(desktopIni);
   }
}

//-----------------------------------------------------------------------------

int CThreeLayer::InitClip()
{
   // values on failure
   clipBinPath = "";
   clipName = "";

   curClip = getSystemAPI()->getClip();
   if (curClip == nullptr)
      return -1;

   curImageFormat = getSystemAPI()->getVideoClipImageFormat();
   if (curImageFormat == nullptr)
     return -1;
   curFrameRect = curImageFormat->getActivePictureRect();

   ptBoxRadius = 1;
   if (curImageFormat->getPixelsPerLine() > 2048) {
      ptBoxRadius = 2;
   }

   curFrameList = getSystemAPI()->getVideoFrameList();
   if (curFrameList == nullptr)
      return -1;

   // get the bin path and clip name
   clipBinPath = curClip->getBinPath();
   clipName    = curClip->getClipName();

   return 0;
}

void CThreeLayer::ReadSettings()
{
   if (IniFileName.empty())
      return;

   CIniFile* iniFile = CreateIniFile(IniFileName);

   if (iniFile == nullptr) {
      TRACE_1(errout << "CThreeLayerTool::ReadSettings: cannot open " << IniFileName);
      return;
   }

   string sectionName = "RegistrationSettings";

   editInCount  =  iniFile->ReadInteger(sectionName,
                                        editInCountKey,
                                        DFLTEDTIN);

   ThreeLayerForm->UpdateEditIn(editInCount);

   editOutCount =  iniFile->ReadInteger(sectionName,
                                        editOutCountKey,
                                        DFLTEDTOUT);

   ThreeLayerForm->UpdateEditOut(editOutCount);

   editAverage  =  iniFile->ReadInteger(sectionName,
                                        editAverageKey,
                                        DFLTEDTAVG);

   ThreeLayerForm->UpdateEditAvg(editAverage);

   refEditInCount  =  iniFile->ReadInteger(sectionName,
                                        refEditInCountKey,
                                        DFLTREFEDTIN);

   ThreeLayerForm->UpdateRefEditIn(refEditInCount);

   refEditOutCount =  iniFile->ReadInteger(sectionName,
                                        refEditOutCountKey,
                                        DFLTREFEDTOUT);

   ThreeLayerForm->UpdateRefEditOut(refEditOutCount);

   warpMode        =  iniFile->ReadBool(sectionName,
                                        warpModeKey,
                                        DFLTWRP);

   ThreeLayerForm->UpdateWarpMode(warpMode);

   movPtsDim       =  iniFile->ReadInteger(sectionName,
                                           movPointsDimKey,
                                           DFLTPTS_MOV);

   movWrpPtsDim    =  iniFile->ReadInteger(sectionName,
                                           movWrpPointsDimKey,
                                           DFLTPTS_WRP);

   pointsDim = (warpMode? movWrpPtsDim : movPtsDim);

   ThreeLayerForm->UpdatePointsDim(pointsDim);

   movRngDim      =  iniFile->ReadInteger(sectionName,
                                          movRangeDimKey,
                                          DFLTRNG);

   movWrpRngDim   =  iniFile->ReadInteger(sectionName,
                                          movWrpRangeDimKey,
                                          DFLTRNG);

   rangeDim = (warpMode? movWrpRngDim : movRngDim);

   ThreeLayerForm->UpdateRangeDim(rangeDim);

   analyzeMatBox  =  iniFile->ReadBool(sectionName,
                                       analyzeMatKey,
                                       true);
   SetAnalysisMatBox(analyzeMatBox);

   processMatBox  =  iniFile->ReadBool(sectionName,
                                       processMatKey,
                                       true);
   SetProcessingMatBox(processMatBox);

   showMatBox     =  iniFile->ReadBool(sectionName,
                                       showMatKey,
                                       true);
   SetShowMatBox(showMatBox);

   if (!DeleteIniFile(iniFile)) {
      TRACE_1(errout << "CThreeLayer::ReadSettings: cannot close RegistrationClip.ini");
   }
}

void CThreeLayer::WriteSettings()
{
   if (IniFileName.empty())
      return;

   CIniFile* iniFile = CreateIniFile(IniFileName);

   if (iniFile == nullptr) {
      TRACE_1(errout << "CThreeLayerTool::WriteSettings: cannot open " << IniFileName);
      return;
   }

   string sectionName = "RegistrationSettings";

   iniFile->WriteInteger(sectionName,
                         editInCountKey,
                         editInCount);

   iniFile->WriteInteger(sectionName,
                         editOutCountKey,
                         editOutCount);

   iniFile->WriteInteger(sectionName,
                         editAverageKey,
                         editAverage);

   iniFile->WriteInteger(sectionName,
                         refEditInCountKey,
                         refEditInCount);

   iniFile->WriteInteger(sectionName,
                         refEditOutCountKey,
                         refEditOutCount);

   iniFile->WriteBool(sectionName,
                      warpModeKey,
                      warpMode);

   iniFile->WriteInteger(sectionName,
                         movPointsDimKey,
                         movPtsDim);

   iniFile->WriteInteger(sectionName,
                         movWrpPointsDimKey,
                         movWrpPtsDim);

   iniFile->WriteInteger(sectionName,
                         movRangeDimKey,
                         movRngDim);

   iniFile->WriteInteger(sectionName,
                         movWrpRangeDimKey,
                         movWrpRngDim);

   iniFile->WriteBool(sectionName,
                      analyzeMatKey,
                      analyzeMatBox);

   iniFile->WriteBool(sectionName,
                      processMatKey,
                      processMatBox);

   iniFile->WriteBool(sectionName,
                      showMatKey,
                      showMatBox);

   if (!DeleteIniFile(iniFile)) {
      TRACE_1(errout << "CThreeLayer::WriteSettings: cannot close " << IniFileName);
   }

   //return 0;
}

///////////////////////////////////////////////////////////////////////////////

void CThreeLayer::setMajorState(int newstate, bool dogui, bool showproc)
{
   // clear the color pick button & cursor
   ClearColorPickMode();
   ClearSingleFramePreviewModeFrameIndex();

   // set the new one
   majorState = newstate;

   switch(majorState) {

      case MAJORSTATE_AUTO_PER_SHOT_HOME:
      case   MAJORSTATE_AUTO_PER_SHOT_EDIT_IN:
      case   MAJORSTATE_AUTO_PER_SHOT_EDIT_OUT:
      case   MAJORSTATE_AUTO_PER_SHOT_EDIT_AVG:

         resetMajorState = MAJORSTATE_AUTO_PER_SHOT_HOME;

      break;

      case MAJORSTATE_AUTO_PER_FRAME_HOME:

         resetMajorState = majorState;

      break;

      case MAJORSTATE_MANUAL_HOME:

         resetMajorState = majorState;

      break;

      case MAJORSTATE_MANUAL_RED:
      case MAJORSTATE_MANUAL_BLUE:
      case MAJORSTATE_MANUAL_GANGED:

         // leave resetMajorState alone

      break;

      case MAJORSTATE_REFERENCE_FRAME_HOME:
      case   MAJORSTATE_REFERENCE_FRAME_EDIT_IN:
      case   MAJORSTATE_REFERENCE_FRAME_EDIT_OUT:
      case   MAJORSTATE_REFERENCE_FRAME_EDIT_REF:

         resetMajorState = MAJORSTATE_REFERENCE_FRAME_HOME;

      break;

      default:
      break;
   }

   if (dogui) {

      // change GUI to reflect states
      ThreeLayerForm->setMajorState(majorState);
   }

   ShowProcessed(showproc);
}

int CThreeLayer::getMajorState()
{
   return majorState;
}

int CThreeLayer::getRenderState()
{
   return renderState;
}

void CThreeLayer::setResetMajorState(int state)
{
   ClearColorPickMode();
   ClearSingleFramePreviewModeFrameIndex();

   resetMajorState = state;

   switch(state) {

      case MAJORSTATE_AUTO_PER_SHOT_HOME:

         ThreeLayerForm->AutoPerShotRadioButton->OnEnter(nullptr);

      break;

      case MAJORSTATE_AUTO_PER_FRAME_HOME:

         ThreeLayerForm->AutoPerFrameRadioButton->OnEnter(nullptr);

      break;

      case MAJORSTATE_MANUAL_HOME:

         ThreeLayerForm->ManualRadioButton->OnEnter(nullptr);

      break;

      case MAJORSTATE_REFERENCE_FRAME_HOME:

         ThreeLayerForm->ReferenceFrameRadioButton->OnEnter(nullptr);

      break;
   }
}

int CThreeLayer::getResetMajorState()
{
   ClearColorPickMode();

   return resetMajorState;
}

void CThreeLayer::CycleOperationMode(int jog)
{
   // note that we switch off of RESETmajorState
   // and not majorState -- so that we can be
   // anywhere and the cycling still works

   if (jog==1) {

      switch(resetMajorState) {

         case MAJORSTATE_AUTO_PER_SHOT_HOME:

            ThreeLayerForm->AutoPerFrameRadioButton->OnEnter(nullptr);

         break;

         case MAJORSTATE_AUTO_PER_FRAME_HOME:

            ThreeLayerForm->ManualRadioButton->OnEnter(nullptr);

         break;

         case MAJORSTATE_MANUAL_HOME:

            ThreeLayerForm->ReferenceFrameRadioButton->OnEnter(nullptr);

         break;

          case MAJORSTATE_REFERENCE_FRAME_HOME:

            ThreeLayerForm->AutoPerShotRadioButton->OnEnter(nullptr);

         break;
     }
   }
   else {

      switch(resetMajorState) {

         case MAJORSTATE_AUTO_PER_SHOT_HOME:

            ThreeLayerForm->ReferenceFrameRadioButton->OnEnter(nullptr);

         break;

         case MAJORSTATE_AUTO_PER_FRAME_HOME:

            ThreeLayerForm->AutoPerShotRadioButton->OnEnter(nullptr);

         break;

         case MAJORSTATE_MANUAL_HOME:

            ThreeLayerForm->AutoPerFrameRadioButton->OnEnter(nullptr);

         break;

         case MAJORSTATE_REFERENCE_FRAME_HOME:

            ThreeLayerForm->ManualRadioButton->OnEnter(nullptr);

         break;
      }
   }
}

///////////////////////////////////////////////////////////////////////////////
// KTGUI

void CThreeLayer::ActivateRed(bool setmask)
{
   ClearColorPickMode();

   if (renderState != RENDER_STATE_IDLE)
      return;

   if (majorState == MAJORSTATE_REFERENCE_FRAME_HOME) {

      getSystemAPI()->goToFrameSynchronous(strRefFrame);
   }

   if ((majorState == MAJORSTATE_AUTO_PER_SHOT_HOME)||
       (majorState == MAJORSTATE_AUTO_PER_FRAME_HOME)||
       (majorState == MAJORSTATE_MANUAL_HOME)||
       (majorState == MAJORSTATE_MANUAL_RED)||
       (majorState == MAJORSTATE_MANUAL_BLUE)||
       (majorState == MAJORSTATE_MANUAL_GANGED)||
       (majorState == MAJORSTATE_REFERENCE_FRAME_HOME)) {

      setMajorState(MAJORSTATE_MANUAL_RED, true, showProc);
   }

   ThreeLayerForm->DummyEdit->SetFocus();
}

void CThreeLayer::ActivateBlue(bool setmask)
{
   ClearColorPickMode();

   if (renderState != RENDER_STATE_IDLE)
      return;

   if (majorState == MAJORSTATE_REFERENCE_FRAME_HOME) {

      getSystemAPI()->goToFrameSynchronous(strRefFrame);
   }

   if ((majorState == MAJORSTATE_AUTO_PER_SHOT_HOME)||
       (majorState == MAJORSTATE_AUTO_PER_FRAME_HOME)||
       (majorState == MAJORSTATE_MANUAL_HOME)||
       (majorState == MAJORSTATE_MANUAL_RED)||
       (majorState == MAJORSTATE_MANUAL_BLUE)||
       (majorState == MAJORSTATE_MANUAL_GANGED)||
       (majorState == MAJORSTATE_REFERENCE_FRAME_HOME)) {

      setMajorState(MAJORSTATE_MANUAL_BLUE, true, showProc);
   }

   ThreeLayerForm->DummyEdit->SetFocus();
}

void CThreeLayer::ActivateBoth(bool setmask)
{
   ClearColorPickMode();

   if (renderState != RENDER_STATE_IDLE)
      return;

   if (majorState == MAJORSTATE_REFERENCE_FRAME_HOME) {

      getSystemAPI()->goToFrameSynchronous(strRefFrame);
   }

   if ((majorState == MAJORSTATE_AUTO_PER_SHOT_HOME)||
       (majorState == MAJORSTATE_AUTO_PER_FRAME_HOME)||
       (majorState == MAJORSTATE_MANUAL_HOME)||
       (majorState == MAJORSTATE_MANUAL_RED)||
       (majorState == MAJORSTATE_MANUAL_BLUE)||
       (majorState == MAJORSTATE_MANUAL_GANGED)||
       (majorState == MAJORSTATE_REFERENCE_FRAME_HOME)) {

      setMajorState(MAJORSTATE_MANUAL_GANGED, true, showProc);
   }

   ThreeLayerForm->DummyEdit->SetFocus();
}

void CThreeLayer::ToggleGanged()
{
   ClearColorPickMode();

   if (renderState != RENDER_STATE_IDLE)
      return;

   if (resetMajorState == MAJORSTATE_REFERENCE_FRAME_HOME) {

      getSystemAPI()->goToFrameSynchronous(strRefFrame);
   }

   if ((majorState == MAJORSTATE_AUTO_PER_SHOT_HOME)||
       (majorState == MAJORSTATE_AUTO_PER_FRAME_HOME)||
       (majorState == MAJORSTATE_MANUAL_HOME)||
       (majorState == MAJORSTATE_MANUAL_RED)||
       (majorState == MAJORSTATE_MANUAL_BLUE)||
       (majorState == MAJORSTATE_REFERENCE_FRAME_HOME)) {

      setMajorState(MAJORSTATE_MANUAL_GANGED, true, showProc);
   }
   else if (majorState == MAJORSTATE_MANUAL_GANGED) {

      // target majorState for RESET
      setMajorState(resetMajorState, true, showProc);
   }

   ThreeLayerForm->DummyEdit->SetFocus();
}

void CThreeLayer::ResetRedOffsets(bool resetx, bool resety)
{
   ClearColorPickMode();
   SetSingleFramePreviewModeFrameIndex();

   if (renderState != RENDER_STATE_IDLE)
      return;

   if ((resetMajorState == MAJORSTATE_AUTO_PER_SHOT_HOME)||
       (resetMajorState == MAJORSTATE_AUTO_PER_FRAME_HOME)||
       (resetMajorState == MAJORSTATE_MANUAL_HOME)||
       (resetMajorState == MAJORSTATE_REFERENCE_FRAME_HOME)) {

      lastFrameAligned = -1;

      if (resetx) {
         redOffsetX = 0.0;
         ThreeLayerForm->UpdateRedOffsetX(redOffsetX);
      }
      if (resety) {
         redOffsetY = 0.0;
         ThreeLayerForm->UpdateRedOffsetY(redOffsetY);
      }

      ThreeLayerForm->setGUIExecButtons(true);

      SetCombinedTransform();

      getSystemAPI()->refreshFrameCached();
   }
}

void CThreeLayer::ResetBlueOffsets(bool resetx, bool resety)
{
   ClearColorPickMode();
   SetSingleFramePreviewModeFrameIndex();

   if (renderState != RENDER_STATE_IDLE)
      return;

   if ((resetMajorState == MAJORSTATE_AUTO_PER_SHOT_HOME)||
       (resetMajorState == MAJORSTATE_AUTO_PER_FRAME_HOME)||
       (resetMajorState == MAJORSTATE_MANUAL_HOME)||
       (resetMajorState == MAJORSTATE_REFERENCE_FRAME_HOME)) {

      lastFrameAligned = -1;

      if (resetx) {
         blueOffsetX = 0.0;
         ThreeLayerForm->UpdateBlueOffsetX(blueOffsetX);
      }
      if (resety) {
         blueOffsetY = 0.0;
         ThreeLayerForm->UpdateBlueOffsetY(blueOffsetY);
      }

      ThreeLayerForm->setGUIExecButtons(true);

      SetCombinedTransform();

      getSystemAPI()->refreshFrameCached();
   }
}

// Master Reset
void CThreeLayer::ResetRedAndBlueOffsets()
{
   ClearColorPickMode();
   SetSingleFramePreviewModeFrameIndex();

   if (renderState != RENDER_STATE_IDLE)
      return;

   if (resetMajorState == MAJORSTATE_MANUAL_HOME) {

      ClearTransform(&autoParams);
   }

   if ((resetMajorState == MAJORSTATE_AUTO_PER_SHOT_HOME)||
       (resetMajorState == MAJORSTATE_AUTO_PER_FRAME_HOME)||
       (resetMajorState == MAJORSTATE_MANUAL_HOME)||
       (resetMajorState == MAJORSTATE_REFERENCE_FRAME_HOME)) {

      lastFrameAligned = -1;

      // clear the manual offsets
      ClearAllOffsets();

      ThreeLayerForm->setGUIExecButtons(true);

      // base + manual offsets
      SetCombinedTransform();

      getSystemAPI()->refreshFrameCached();
   }
}

void CThreeLayer::ResetXOffset()
{
   ClearColorPickMode();
   SetSingleFramePreviewModeFrameIndex();

   if (renderState != RENDER_STATE_IDLE)
      return;

   if (majorState == MAJORSTATE_MANUAL_RED) {

      ResetRedOffsets(true, false);
   }
   else if (majorState == MAJORSTATE_MANUAL_BLUE) {

      ResetBlueOffsets(true, false);
   }
   else if (majorState == MAJORSTATE_MANUAL_GANGED) {

      ResetRedOffsets(true, false);
      ResetBlueOffsets(true, false);
   }
}

void CThreeLayer::ResetYOffset()
{
   ClearColorPickMode();
   SetSingleFramePreviewModeFrameIndex();

   if (renderState != RENDER_STATE_IDLE)
      return;

   if (majorState == MAJORSTATE_MANUAL_RED) {

      ResetRedOffsets(false, true);
   }
   else if (majorState == MAJORSTATE_MANUAL_BLUE) {

      ResetBlueOffsets(false, true);
   }
   else if (majorState == MAJORSTATE_MANUAL_GANGED) {

      ResetRedOffsets(false, true);
      ResetBlueOffsets(false, true);
   }
}

bool CThreeLayer::JogOffsets(double xincr, double yincr)
{
   ClearColorPickMode();
   SetSingleFramePreviewModeFrameIndex();

   if (renderState != RENDER_STATE_IDLE)
      return false;

   bool refresh = false;

   switch(majorState) {

      case MAJORSTATE_MANUAL_RED:

         redOffsetX += xincr;
         redOffsetY += yincr;
         refresh = true;

      break;

      case MAJORSTATE_MANUAL_BLUE:

         blueOffsetX += xincr;
         blueOffsetY += yincr;
         refresh = true;

      break;

      case MAJORSTATE_MANUAL_GANGED:

         redOffsetX  += xincr;
         redOffsetY  += yincr;
         blueOffsetX += xincr;
         blueOffsetY += yincr;
         refresh = true;

      break;

      default:

      break;

   }

   if (refresh) {

      ThreeLayerForm->setGUIExecButtons(true);

      // clears loaded reg frame to -1
      SetCombinedTransform();

      ThreeLayerForm->UpdateRedOffsetX(redOffsetX);
      ThreeLayerForm->UpdateRedOffsetY(redOffsetY);
      ThreeLayerForm->UpdateBlueOffsetX(blueOffsetX);
      ThreeLayerForm->UpdateBlueOffsetY(blueOffsetY);

      // force Target View = PROCESSED
      showProc = true;
      getSystemAPI()->setDisplayProcessed(true);
      ThreeLayerForm->UpdateTargetView(true);

      if (!getSystemAPI()->isDisplaying()) {

         getSystemAPI()->refreshFrameCached();
      }
   }

   return refresh;
}

void CThreeLayer::JogMatBox(int ljog, int tjog, int rjog, int bjog)
{
   const CImageFormat* imgFmt = getSystemAPI()->getVideoClipImageFormat();
   int maxr = imgFmt->getPixelsPerLine() - 1;
   int maxb = imgFmt->getLinesPerFrame() - 1;

   matBox.left   = std::max<long>(0,    matBox.left   + (ljog * matIncr));
   matBox.top    = std::max<long>(0,    matBox.top    + (tjog * matIncr));
   matBox.right  = std::min<long>(maxr, matBox.right  + (rjog * matIncr));
   matBox.bottom = std::min<long>(maxb, matBox.bottom + (bjog * matIncr));

   getSystemAPI()->SetMatBox(GetMatBoxForProcessing());
   getSystemAPI()->refreshFrameCached();
}

////////////////////////////////////////////////////////////////////////////////

RECT *CThreeLayer::GetAllBox()
{
   const CImageFormat* imgFmt = getSystemAPI()->getVideoClipImageFormat();

   allBox.left   = 0;
   allBox.top    = 0;
   allBox.right  = (imgFmt != nullptr) ? (imgFmt->getPixelsPerLine() - 1) : 0;
   allBox.bottom = (imgFmt != nullptr) ? (imgFmt->getLinesPerFrame() - 1) : 0;;

   return &allBox;
}

void CThreeLayer::SetMatBoxAll()
{
   SetMatBox(nullptr);
}

void CThreeLayer::SetMatBox(RECT *newMatBox)
{
   matBox = (newMatBox != nullptr) ? *newMatBox : *GetAllBox();

   getSystemAPI()->SetMatBox(GetMatBoxForProcessing());
   getSystemAPI()->refreshFrameCached();
}

RECT *CThreeLayer::GetMatBoxForAnalysis()
{
   if (!analyzeMatBox)
   {
      return GetAllBox();
   }

   return &matBox;
}

RECT *CThreeLayer::GetMatBoxForProcessing()
{
   // CAREFUL! Even if processMatBox is true, we don't want to restrict the
   // processing to the mat box unless analyzeMatBox is also true!
   if (!analyzeMatBox || !processMatBox)
   {
      return GetAllBox();
   }

   return &matBox;
}

bool CThreeLayer::FindBoundingBox(int R, int G, int B, int X, int Y, int slack)
{
   const CImageFormat* imgFmt = getSystemAPI()->getVideoClipImageFormat();
   if (imgFmt == nullptr) return false;

   // Get some value
   int nPicCol = imgFmt->getPixelsPerLine();
   int nPicRow = imgFmt->getLinesPerFrame();
   bool bRGB = (imgFmt->getPixelComponents() == IF_PIXEL_COMPONENTS_RGB) ||
               (imgFmt->getPixelComponents() == IF_PIXEL_COMPONENTS_BGR) ||
               (imgFmt->getPixelComponents() == IF_PIXEL_COMPONENTS_RGBA)||
               (imgFmt->getPixelComponents() == IF_PIXEL_COMPONENTS_BGRA)||
               (imgFmt->getPixelComponents() == IF_PIXEL_COMPONENTS_Y)   ||
               (imgFmt->getPixelComponents() == IF_PIXEL_COMPONENTS_YYY);

   RECT _ActiveBox = imgFmt->getActivePictureRect();

   // Hack - assume slack was for 10-bit data, so if it's 8, divide by 2
   // (dividing by 4 makes it too narrow, I think (?)
   if (imgFmt->getPixelPacking() == IF_PIXEL_PACKING_8Bits_IN_1Byte)
      slack /= 2;

   // load the Displayer's internal buffer with the frame
   MTI_UINT16 *BaseImage = getSystemAPI()->getPreRegisteredIntermediate();
   getSystemAPI()->getLastFrameAsIntermediate(BaseImage);

   RECT theBox = FindMatBox(BaseImage, nPicCol, nPicRow, slack, _ActiveBox,
                            bRGB, R, G, B, X, Y);

   // xmits to the Displayer also
   SetMatBox(&theBox);

   return true;
}

void CThreeLayer::ColorPickerColorChanged(void *Sender)
{
   // we don't do anything here yet
}

void CThreeLayer::ColorPickerColorPicked(void *Sender)
{
   int R, G, B, X, Y;

   ColorPickTool->getColor(R, G, B);
   ColorPickTool->getXY(X, Y);
   FindBoundingBox(R, G, B, X, Y);

   getSystemAPI()->refreshFrameCached();
}

void CThreeLayer::EnterColorPickMode(void)
{
   if (renderState != RENDER_STATE_IDLE)
      return;

   if ((majorState == MAJORSTATE_AUTO_PER_SHOT_HOME)||
         (majorState == MAJORSTATE_AUTO_PER_SHOT_EDIT_IN)||
         (majorState == MAJORSTATE_AUTO_PER_SHOT_EDIT_OUT)||
         (majorState == MAJORSTATE_AUTO_PER_SHOT_EDIT_AVG)||
       (majorState == MAJORSTATE_AUTO_PER_FRAME_HOME)||
       (majorState == MAJORSTATE_MANUAL_HOME)||
       (majorState == MAJORSTATE_MANUAL_RED)||
       (majorState == MAJORSTATE_MANUAL_BLUE)||
       (majorState == MAJORSTATE_MANUAL_GANGED)||
       (majorState == MAJORSTATE_REFERENCE_FRAME_HOME)||
         (majorState == MAJORSTATE_REFERENCE_FRAME_EDIT_IN)||
         (majorState == MAJORSTATE_REFERENCE_FRAME_EDIT_OUT)||
         (majorState == MAJORSTATE_REFERENCE_FRAME_EDIT_REF)||
       (majorState == MAJORSTATE_ANALYSIS_EDIT_PTS)||
       (majorState == MAJORSTATE_ANALYSIS_EDIT_RNG)) {

      prevMajorState = majorState;
      majorState = MAJORSTATE_PICK_MAT_COLOR;

      ColorPickTool->SetEnabled(true);

      ThreeLayerForm->UpdateColorPickButton(true);
   }
}

void CThreeLayer::ExitColorPickMode(void)
{
   majorState = prevMajorState;

   ColorPickTool->SetEnabled(false);

   ThreeLayerForm->UpdateColorPickButton(false);
}

bool CThreeLayer::IsInColorPickMode(void)
{
   return ColorPickTool->IsEnabled();
}

void CThreeLayer::ClearColorPickMode()
{
   if (majorState == MAJORSTATE_PICK_MAT_COLOR) {

      ExitColorPickMode();
   }
}

////////////////////////////////////////////////////////////////////////////////

void CThreeLayer::ShowProcessed(bool proc)
{
   ClearColorPickMode();

   if (renderState != RENDER_STATE_IDLE)
      return;

   if (showProc == proc)
      return;

   ToggleProcessedAndOriginal();
}

void CThreeLayer::ShowProcessedChanged(bool proc)
{
   ClearColorPickMode();

   if (renderState != RENDER_STATE_IDLE)
      return;

   if (showProc == proc)
      return;

   if ((majorState == MAJORSTATE_AUTO_PER_SHOT_HOME)||
       (majorState == MAJORSTATE_AUTO_PER_FRAME_HOME)||
       (majorState == MAJORSTATE_MANUAL_HOME)||
       (majorState == MAJORSTATE_MANUAL_RED)||
       (majorState == MAJORSTATE_MANUAL_BLUE)||
       (majorState == MAJORSTATE_MANUAL_GANGED)||
       (majorState == MAJORSTATE_REFERENCE_FRAME_HOME)) {

      getSystemAPI()->setDisplayProcessed(proc);
      ThreeLayerForm->UpdateTargetView(proc);

      showProc = proc;

      if (!getSystemAPI()->isDisplaying()) {

         // refresh from media & refill buffers
         getSystemAPI()->clearLoadedRegistrationFrame();
         int curfrm = getSystemAPI()->getLastFrameIndex();
         getSystemAPI()->goToFrameSynchronous(curfrm);
      }
   }
}

bool CThreeLayer::ManualOffsetsZeroed()
{
   if (resetMajorState == MAJORSTATE_MANUAL_HOME) {

      if (
          (fabs(autoParams.AredX) < .00001)&&
          (fabs(autoParams.BredX) < .00001)&&
          //(fabs(autoParams.CredX) < .00001)&&
          (fabs(autoParams.AredY) < .00001)&&
          (fabs(autoParams.BredY) < .00001)&&
          //(fabs(autoParams.CredY) < .00001)&&
          (fabs(autoParams.AbluX) < .00001)&&
          (fabs(autoParams.BbluX) < .00001)&&
          //(fabs(autoParams.CbluX) < .00001)&&
          (fabs(autoParams.AbluY) < .00001)&&
          (fabs(autoParams.BbluY) < .00001)&&
          //(fabs(autoParams.CbluY) < .00001)&&

          (fabs(redOffsetX)  < .00001)&&
          (fabs(redOffsetY)  < .00001)&&
          (fabs(blueOffsetX) < .00001)&&
          (fabs(blueOffsetY) < .00001) ) {

          return true;
      }
   }

   return false;
}

bool CThreeLayer::MarksAreValid()
{
   // get the Marks
   int infrm  = getSystemAPI()->getMarkIn();
   int outfrm = getSystemAPI()->getMarkOut();

   return ((infrm != -1)&&(outfrm != -1)&&(infrm < outfrm));
}

bool CThreeLayer::GetMarks(int *in, int *out)
{
   // get the Marks
   int infrm  = getSystemAPI()->getMarkIn();
   int outfrm = getSystemAPI()->getMarkOut();

   if ((infrm != -1)&&(outfrm != -1)&&(infrm < outfrm)) {

      *in  = infrm;
      *out = outfrm;
      return true;
   }
   else {

      *in = *out = -1;
      return false;
   }
}

bool CThreeLayer::MarksAreUnchanged(int in, int out)
{
   // get the Marks
   int infrm  = getSystemAPI()->getMarkIn();
   int outfrm = getSystemAPI()->getMarkOut();

   return ((in == infrm)&&(out == outfrm));
}

int CThreeLayer::ExecuteSingleFrame(int frm, bool render)
{
   ClearColorPickMode();

   if (renderState != RENDER_STATE_IDLE)
      return -1;

   // doing a single frame
   oneFrame = true;
   if (!render)
   {
      ClearSingleFramePreviewModeFrameIndex();
   }

   // class' render flag
   renderFlag = render;

   if (resetMajorState == MAJORSTATE_AUTO_PER_SHOT_HOME) {

      // it's ESSENTIAL to set this to RUNNING here so that
      // MonitorFrameProcessing doesn't call the modal dialog
      // twice and make an exception
      SetToolProcessingControlState(TOOL_CONTROL_STATE_RUNNING);

      if (renderFlag&&!getSystemAPI()->WarnThereIsNoUndo()) {

         SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

         return -1;
      }

      // for D and G current frame must be inside good marks
      if (!GetMarks(&strBegFrame, &strEndFrame)) {

         ThreeLayerForm->Error1Show();

         SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

         return -1;
      }

      if ((frm < strBegFrame)||(frm >= strEndFrame)) {

         ThreeLayerForm->Error2Show();

         SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

         return -1;
      }

      // set these up
      strInFrame  = strBegFrame + editInCount;
      if (strInFrame > strEndFrame)
         strInFrame = strEndFrame;

      strOutFrame = strEndFrame - editOutCount;
      if (strOutFrame < strBegFrame)
         strOutFrame = strBegFrame;

      if (strOutFrame < strInFrame)
         strInFrame = strOutFrame;

      frameStep = (double)(strOutFrame - strInFrame) / (double)(editAverage + 1);
      if (frameStep < 1) {
         frameStep = 1.0;
      }

      framesAveraged = ((strOutFrame - strInFrame) / frameStep) - 1;

      // apply transform here
      strApplyOneFrame = frm;

      if ((frm < strInFrame)||(frm >= strOutFrame)) {
         strAlignOneFrame = frm;
         renderStatePtr = AutoPerShotEndFrameStateTable;
      }
      else {
         avgFrame = strInFrame;
         renderStatePtr = AutoPerShotMidFrameStateTable;
      }

      // major state
      setMajorState(MAJORSTATE_MANUAL_GANGED, true, showProc);
      prevMajorState = majorState;

      // use the display ring buffer
      getSystemAPI()->lockReader(true);

      if (renderFlag) {

         // end state is 'original' and
         // 'displayFrameRenderThread'
         // needs this setting

         ShowProcessed(false);
      }
      else {

         // end state is 'processed' and
         // 'DisplayFrame' needs this
         // setting

         ShowProcessed(true);
      }

      // set the frame count
      ThreeLayerForm->ExecStatusBar->SetFrameCount(1);

      // start progress
      ThreeLayerForm->ExecStatusBar->StartProgress();

      // GUI to running-through state
      ThreeLayerForm->setGUIRunning(true);

      // clear pause flag
      pauseFlag = false;

      // here we go!
      ToNextRenderState();

      // success
      return 0;
   }
   else if (resetMajorState == MAJORSTATE_AUTO_PER_FRAME_HOME) {

      // it's ESSENTIAL to set this to RUNNING here so that
      // MonitorFrameProcessing doesn't call the modal dialog
      // twice and make an exception
      SetToolProcessingControlState(TOOL_CONTROL_STATE_RUNNING);

      if (renderFlag&&!getSystemAPI()->WarnThereIsNoUndo()) {

         SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

         return -1;
      }

      // for D and G current frame may be anywhere

      strAlignOneFrame = frm;
      strApplyOneFrame = frm;
      renderStatePtr = AutoPerShotEndFrameStateTable;

      // major state
      setMajorState(MAJORSTATE_MANUAL_GANGED, true, showProc);
      prevMajorState = majorState;

      // use the display ring buffer
      getSystemAPI()->lockReader(true);

      if (renderFlag) {

         // end state is 'original' and
         // 'displayFrameRenderThread'
         // needs this setting

         ShowProcessed(false);
      }
      else {

         // end state is 'processed' and
         // 'DisplayFrame' needs this
         // setting

         ShowProcessed(true);
      }

      // set the frame count
      ThreeLayerForm->ExecStatusBar->SetFrameCount(1);

      // start progress
      ThreeLayerForm->ExecStatusBar->StartProgress();

      // GUI to running-through state
      ThreeLayerForm->setGUIRunningThru();

      // clear pause flag
      pauseFlag = false;

      // here we go!
      ToNextRenderState();

      // success
      return 0;
   }
   else if (resetMajorState == MAJORSTATE_MANUAL_HOME) {

      // it's ESSENTIAL to set this to RUNNING here so that
      // MonitorFrameProcessing doesn't call the modal dialog
      // twice and make an exception
      SetToolProcessingControlState(TOOL_CONTROL_STATE_RUNNING);

      if (renderFlag&&!getSystemAPI()->WarnThereIsNoUndo()) {

         SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

         return -1;
      }

      // for D and G current frame may be anywhere

      // for render, transform must be non-zero
      if (renderFlag) {

         if (ManualOffsetsZeroed()) {

            ThreeLayerForm->Error4Show();

            SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

            return -1;
         }

         strApplyOneFrame = frm;

         // applies man offsets, then writes back
         renderStatePtr = ManualApplyOneStateTable;
      }
      else {

         strAlignOneFrame = frm;

         // aligns & loads man offsets, then into Displayer
         renderStatePtr = ManualAlignOneStateTable;
      }

      // major state ganged iff no channel enabled
      if (majorState == MAJORSTATE_MANUAL_HOME) {
         setMajorState(MAJORSTATE_MANUAL_GANGED, true, showProc);
      }
      prevMajorState = majorState;

      // lock the Player
      getSystemAPI()->lockReader(true);

      if (renderFlag) {

         // end state is 'original' and
         // 'displayFrameRenderThread'
         // needs this setting
         ShowProcessed(false);
      }
      else {

         // end state is 'processed' and
         // 'DisplayFrame' needs this
         // setting
         ShowProcessed(true);
      }

      // set the frame count
      ThreeLayerForm->ExecStatusBar->SetFrameCount(1);

      // start progress
      ThreeLayerForm->ExecStatusBar->StartProgress();

      // GUI to running-through state
      ThreeLayerForm->setGUIRunningThru();

      // clear pause flag
      pauseFlag = false;

      // here we go!
      ToNextRenderState();

      // success
      return 0;
   }
   else if (resetMajorState == MAJORSTATE_REFERENCE_FRAME_HOME) {

      // it's ESSENTIAL to set this to RUNNING here so that
      // MonitorFrameProcessing doesn't call the modal dialog
      // twice and make an exception
      SetToolProcessingControlState(TOOL_CONTROL_STATE_RUNNING);

      if (renderFlag&&!getSystemAPI()->WarnThereIsNoUndo()) {

         SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

         return -1;
      }

      // if rendering then
      //    get marks
      //    frame must be inside valid marks
      //    reference frame must be inside marks
      //    align ReferenceFrameOneFrameStateTable
      // else
      //    frame can be anywhere
      //    load into reference frame
      //    align ManualAlignOneStateTable

      if (renderFlag) {

         if (!GetMarks(&strBegFrame, &strEndFrame)) {

            // lacks legal IN and OUT marks
            ThreeLayerForm->Error1Show();

            SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

            return -1;
         }

         // valid marks - set these up
         strInFrame  = strBegFrame + refEditInCount;
         if (strInFrame > strEndFrame)
            strInFrame = strEndFrame;

         strOutFrame = strEndFrame - refEditOutCount;
         if (strOutFrame < strBegFrame)
            strOutFrame = strBegFrame;

         if (strOutFrame < strInFrame)
            strInFrame = strOutFrame;

         if ((frm < strBegFrame)||(frm >= strEndFrame)) {

            // frame is not inside marks
            ThreeLayerForm->Error2Show();

            SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

            return -1;
         }

         if ((strRefFrame < strBegFrame)||(strRefFrame >= strEndFrame)) {

            // ref frame is not inside marks
            ThreeLayerForm->Error3Show();

            SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

            return -1;
         }

         strAlignOneFrame = strRefFrame;
         strApplyOneFrame = frm;

         renderStatePtr = ReferenceFrameOneFrameStateTable;
      }
      else {

         // sets strRefFrame to current frame
         ThreeLayerForm->UpdateReferenceFrame(frm);

         strAlignOneFrame = strRefFrame;
         renderStatePtr = ManualAlignOneStateTable;
      }

      // major state
      setMajorState(MAJORSTATE_MANUAL_GANGED, true, showProc);
      prevMajorState = majorState;

      if (renderFlag) {

         // end state is 'original' and
         // 'displayFrameRenderThread'
         // needs this setting

         ShowProcessed(false);
      }
      else {

         // end state is 'processed' and
         // 'DisplayFrame' needs this
         // setting

         ShowProcessed(true);
      }

      // lock the Player
      getSystemAPI()->lockReader(true);

      // set the frame count
      ThreeLayerForm->ExecStatusBar->SetFrameCount(1);

      // start progress
      ThreeLayerForm->ExecStatusBar->StartProgress();

      // GUI to running-through state
      ThreeLayerForm->setGUIRunningThru();

      // clear pause flag
      pauseFlag = false;

      // here we go!
      ToNextRenderState();

      // success
      return 0;
   }

   return -1;
}

int CThreeLayer::ExecuteCurrentFrame(bool render)
{
   int curfrm = getSystemAPI()->getLastFrameIndex();
   return ExecuteSingleFrame(curfrm, render);
}

int CThreeLayer::ExecuteNextFrame(bool render)
{
   int curfrm = getSystemAPI()->getLastFrameIndex();
   return ExecuteSingleFrame((curfrm + 1), render);
}

int CThreeLayer::CapturePDLEvent(bool all)
{
   if (renderState != RENDER_STATE_IDLE)
      return -1;

   getSystemAPI()->CapturePDLEntry(all);

   return 0;
}

// sends a "play-between-marks" command to the Navigator
//
int CThreeLayer::ExecuteRangeProcess(bool render)
{
   ClearColorPickMode();
   ClearSingleFramePreviewModeFrameIndex();

   if (renderState != RENDER_STATE_IDLE)
      return -1;

   // single frame flag
   oneFrame = false;

   // load the class' render flag
   renderFlag = render;

   if (resetMajorState == MAJORSTATE_AUTO_PER_SHOT_HOME) {

      // it's ESSENTIAL to set this to RUNNING here so that
      // MonitorFrameProcessing doesn't call the modal dialog
      // twice and make an exception
      SetToolProcessingControlState(TOOL_CONTROL_STATE_RUNNING);

      if (renderFlag&&!getSystemAPI()->WarnThereIsNoUndo()) {

         SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

         return -1;
      }

      if (!GetMarks(&strBegFrame, &strEndFrame)) {

         ThreeLayerForm->Error1Show();

         SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

         return -1;
      }

      // major state
      prevMajorState = majorState;

      // using display ring buffer
      getSystemAPI()->lockReader(true);

      // display unprocessed from fields
      ShowProcessed(false);

      // zero GUI base values
      ClearBaseTransform();

      // set up FRAME parameters
      //
      strInFrame  = strBegFrame + editInCount;
      if (strInFrame > strEndFrame)
         strInFrame = strEndFrame;

      strOutFrame = strEndFrame - editOutCount;
      if (strOutFrame < strBegFrame)
         strOutFrame = strBegFrame;

      if (strOutFrame < strInFrame)
         strInFrame = strOutFrame;

      frameStep = (double)(strOutFrame - strInFrame) / (double)(editAverage + 1);
      if (frameStep < 1) {
         frameStep = 1.0;
      }

      framesAveraged = ((strOutFrame - strInFrame) / frameStep) - 1;

      // start at IN mark
      _currentRenderFrame = strBegFrame;

      // averaging begins here
      avgFrame = strInFrame;

       // if no frames are averaged, use abbrev state table
      renderStatePtr = AutoPerShotStateTable;
      if ((strInFrame == strOutFrame)||(frameStep == 1.0)) {
         renderStatePtr = OutOnlyStateTable;
      }

      // total = between marks
      int totFrames = strEndFrame - strBegFrame;

      // set the frame count
      ThreeLayerForm->ExecStatusBar->SetFrameCount(totFrames);

      // start progress feedback
      ThreeLayerForm->ExecStatusBar->StartProgress();

      // GUI to running state
      ThreeLayerForm->setGUIRunning(true);

      // clear pause flag
      pauseFlag = false;

      // here we go!
      ToNextRenderState();

      // success
      return 0;
   }
   else if (resetMajorState == MAJORSTATE_AUTO_PER_FRAME_HOME) {

      // it's ESSENTIAL to set this to RUNNING here so that
      // MonitorFrameProcessing doesn't call the modal dialog
      // twice and make an exception
      SetToolProcessingControlState(TOOL_CONTROL_STATE_RUNNING);

      if (renderFlag&&!getSystemAPI()->WarnThereIsNoUndo()) {

         SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

         return -1;
      }

      if (!GetMarks(&strBegFrame, &strEndFrame)) {

         ThreeLayerForm->Error1Show();

         SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

         return -1;
      }

      // major state
      prevMajorState = majorState;

      // using display ring buffer
      getSystemAPI()->lockReader(true);

      // display unprocessed from fields
      ShowProcessed(false);

      // zero GUI base values
      ClearBaseTransform();

      // set up FRAME parameters
      //
      // all frames individually aligned
      strInFrame = strOutFrame = strEndFrame;

      // start at IN mark
      _currentRenderFrame = strBegFrame;

      // the state table
      renderStatePtr = AutoPerFrameStateTable;

      // total = between marks
      int totFrames = strEndFrame - strBegFrame;

      // set the frame count
      ThreeLayerForm->ExecStatusBar->SetFrameCount(totFrames);

      // start progress feedback
      ThreeLayerForm->ExecStatusBar->StartProgress();

      // GUI to running state
      ThreeLayerForm->setGUIRunning(true);

      // clear pause flag
      pauseFlag = false;

      // here we go!
      ToNextRenderState();

      // success
      return 0;
   }
   else if (resetMajorState == MAJORSTATE_MANUAL_HOME) {

      // it's ESSENTIAL to set this to RUNNING here so that
      // MonitorFrameProcessing doesn't call the modal dialog
      // twice and make an exception
      SetToolProcessingControlState(TOOL_CONTROL_STATE_RUNNING);

      if (renderFlag&&!getSystemAPI()->WarnThereIsNoUndo()) {

         SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

         return -1;
      }

      // needs only good marks
      if (!GetMarks(&strBegFrame, &strEndFrame)) {

         ThreeLayerForm->Error1Show();

         SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

         return -1;
      }

      // transform must be non-zero
      if (ManualOffsetsZeroed()) {

         ThreeLayerForm->Error4Show();

         SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

         return -1;
      }

      // major state
      prevMajorState = majorState;

      // using display ring buffer
      getSystemAPI()->lockReader(true);

      // display unprocessed from fields
      ShowProcessed(false);

      // here we do NOT clear the base transform
      // because the A's and B's are relevant in
      // the case where the 'D' key was executed
      // using 'Move and Warp'. This was THE bug!

      // set up FRAME parameters
      //
      // all frames between marks
      strInFrame  = strBegFrame;
      strOutFrame = strEndFrame;

      // start at IN mark
      _currentRenderFrame = strBegFrame;

      // the state table
      renderStatePtr = ManualStateTable;

      // total = between marks
      int totFrames = strEndFrame - strBegFrame;

      // set the frame count
      ThreeLayerForm->ExecStatusBar->SetFrameCount(totFrames);

      // start progress feedback
      ThreeLayerForm->ExecStatusBar->StartProgress();

      // GUI to running state
      ThreeLayerForm->setGUIRunning(true);

      // clear pause flag
      pauseFlag = false;

      // here we go!
      ToNextRenderState();

      // success
      return 0;
   }
   else if (resetMajorState == MAJORSTATE_REFERENCE_FRAME_HOME) {

      // it's ESSENTIAL to set this to RUNNING here so that
      // MonitorFrameProcessing doesn't call the modal dialog
      // twice and make an exception
      SetToolProcessingControlState(TOOL_CONTROL_STATE_RUNNING);

      if (renderFlag&&!getSystemAPI()->WarnThereIsNoUndo()) {

         SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

         return -1;
      }

      // reference frame must be between valid marks
      if (!GetMarks(&strBegFrame, &strEndFrame)||
          (strRefFrame < strBegFrame)||(strRefFrame >= strEndFrame)) {

         ThreeLayerForm->Error3Show();

         SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

         return -1;
      }

      // major state
      prevMajorState = majorState;

      // using display ring buffer
      getSystemAPI()->lockReader(true);

      // display unprocessed from fields
      ShowProcessed(false);

      // zero GUI base values
      ClearBaseTransform();

      // set up FRAME parameters
      //
      strInFrame  = strBegFrame + refEditInCount;
      if (strInFrame > strEndFrame)
         strInFrame = strEndFrame;

      strOutFrame = strEndFrame - refEditOutCount;
      if (strOutFrame < strBegFrame)
         strOutFrame = strBegFrame;

      if (strOutFrame < strInFrame)
         strInFrame = strOutFrame;

      // start at IN mark
      _currentRenderFrame = strBegFrame;

      // the one frame is the reference frame
      strAlignOneFrame = strRefFrame;

      // the state table
      renderStatePtr = ReferenceFrameStateTable;
      if (strInFrame == strOutFrame) {
         renderStatePtr = OutOnlyStateTable;
      }

      // total = frame aligned + frames shifted
      int totFrames = strEndFrame - strBegFrame; // + 1;

      // set the frame count
      ThreeLayerForm->ExecStatusBar->SetFrameCount(totFrames);

      // start progress feedback
      ThreeLayerForm->ExecStatusBar->StartProgress();

      // GUI to running state
      ThreeLayerForm->setGUIRunning(true);

      // clear pause flag
      pauseFlag = false;

      // here we go!
      ToNextRenderState();

      // success
      return 0;
   }

   return -1;
}

int CThreeLayer::ExecutePDLRangeProcess()
{
   ClearColorPickMode();
   ClearSingleFramePreviewModeFrameIndex();

   if (renderState != RENDER_STATE_IDLE)
      return -1;

   // single frame flag
   oneFrame = false;

   // class' render flag
   renderFlag = true;

   if (resetMajorState == MAJORSTATE_AUTO_PER_SHOT_HOME) {

      // it's ESSENTIAL to set this to RUNNING here so that
      // MonitorFrameProcessing doesn't call the modal dialog
      // twice and make an exception
      SetToolProcessingControlState(TOOL_CONTROL_STATE_RUNNING);

      if (!getSystemAPI()->WarnThereIsNoUndo()) {

         SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

         return -1;
      }

      if (!GetMarks(&strBegFrame, &strEndFrame)) { // should not happen from PDL

         // IN and OUT marks must be correctly set
         ThreeLayerForm->Error1Show();

         SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

         return -1;
      }

      // major state
      prevMajorState = majorState;

      // lock the Player
      getSystemAPI()->lockReader(true);

      // display unprocessed from fields
      ShowProcessed(false);

      strInFrame  = strBegFrame + editInCount;
      if (strInFrame > strEndFrame)
         strInFrame = strEndFrame;

      strOutFrame = strEndFrame - editOutCount;
      if (strOutFrame < strBegFrame)
         strOutFrame = strBegFrame;

      if (strOutFrame < strInFrame)
         strInFrame = strOutFrame;

      frameStep = (double)(strOutFrame - strInFrame) / (double)(editAverage + 1);
      if (frameStep < 1) {
         frameStep = 1.0;
      }

      framesAveraged = ((strOutFrame - strInFrame) / frameStep) - 1;

      // start at IN mark
      _currentRenderFrame = strBegFrame;

      // averaging begins here
      avgFrame = strInFrame;

      // if no frames are averaged, use abbrev state table
      renderStatePtr = AutoPerShotStateTable;
      if ((strInFrame == strOutFrame)||(frameStep == 1.0)) {
         renderStatePtr = OutOnlyStateTable;
      }

      // total = frames rendered + frames averaged
      int totFrames = strEndFrame - strBegFrame;

      // set the frame count
      ThreeLayerForm->ExecStatusBar->SetFrameCount(totFrames);

      // start progress feedback
      ThreeLayerForm->ExecStatusBar->StartProgress();

      // GUI to running state
      ThreeLayerForm->setGUIRunning(true);

      // clear pause flag
      pauseFlag = false;

      // here we go!
      ToNextRenderState();

      return 0;
   }
   else if (resetMajorState == MAJORSTATE_AUTO_PER_FRAME_HOME) {

      // it's ESSENTIAL to set this to RUNNING here so that
      // MonitorFrameProcessing doesn't call the modal dialog
      // twice and make an exception
      SetToolProcessingControlState(TOOL_CONTROL_STATE_RUNNING);

      if (!getSystemAPI()->WarnThereIsNoUndo()) {

         SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

         return -1;
      }

      if (!GetMarks(&strBegFrame, &strEndFrame)) { // should not happen from PDL

         // IN and OUT marks must be correctly set
         ThreeLayerForm->Error1Show();

         SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

         return -1;
      }

      // major state
      prevMajorState = majorState;

      // using the display ring buffer
      getSystemAPI()->lockReader(true);

      // display unprocessed from fields
      ShowProcessed(false);

      // zero GUI base values
      ClearBaseTransform();

      // all frames individually aligned
      strInFrame = strOutFrame = strEndFrame;

      // start at IN mark
      _currentRenderFrame = strBegFrame;

      // the state table
      renderStatePtr = AutoPerFrameStateTable;

      // total = between marks
      int totFrames = strEndFrame - strBegFrame;

      // set the frame count
      ThreeLayerForm->ExecStatusBar->SetFrameCount(totFrames);

      // start progress feedback
      ThreeLayerForm->ExecStatusBar->StartProgress();

      // GUI to running state
      ThreeLayerForm->setGUIRunning(true);

      // clear pause flag
      pauseFlag = false;

      // here we go!
      ToNextRenderState();

      return 0;
   }
   else if (resetMajorState == MAJORSTATE_MANUAL_HOME) {

      // it's ESSENTIAL to set this to RUNNING here so that
      // MonitorFrameProcessing doesn't call the modal dialog
      // twice and make an exception
      SetToolProcessingControlState(TOOL_CONTROL_STATE_RUNNING);

      if (!getSystemAPI()->WarnThereIsNoUndo()) {

         SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

         return -1;
      }

      if (!GetMarks(&strBegFrame, &strEndFrame)) { // should not happen from PDL

         // IN and OUT marks must be correctly set
         ThreeLayerForm->Error1Show();

         SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

         return -1;
      }

      // major state
      prevMajorState = majorState;

      // using the display ring buffer
      getSystemAPI()->lockReader(true);

      // display unprocessed from fields
      ShowProcessed(false);

      // here we do NOT clear the base transform
      // because the A's and B's are relevant in
      // the case where the 'D' key was executed
      // using 'Move and Warp'. This was THE bug!

      // all frames between marks
      strInFrame  = strBegFrame;
      strOutFrame = strEndFrame;

      // start at IN mark
      _currentRenderFrame = strBegFrame;

      // the state table
      renderStatePtr = ManualStateTable;

      // total = between marks
      int totFrames = strEndFrame - strBegFrame;

      // set the frame count
      ThreeLayerForm->ExecStatusBar->SetFrameCount(totFrames);

      // start progress feedback
      ThreeLayerForm->ExecStatusBar->StartProgress();

      // GUI to running state
      ThreeLayerForm->setGUIRunning(true);

      // clear pause flag
      pauseFlag = false;

      // here we go!
      ToNextRenderState();

      return 0;
   }
   else if (resetMajorState == MAJORSTATE_REFERENCE_FRAME_HOME) {

      // it's ESSENTIAL to set this to RUNNING here so that
      // MonitorFrameProcessing doesn't call the modal dialog
      // twice and make an exception
      SetToolProcessingControlState(TOOL_CONTROL_STATE_RUNNING);

      if (!getSystemAPI()->WarnThereIsNoUndo()) {

         SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

         return -1;
      }

      if (!GetMarks(&strBegFrame, &strEndFrame)) { // should not happen from PDL

         SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

         // IN and OUT marks must be correctly set
         ThreeLayerForm->Error1Show();

         return -1;
      }

      // major state
      prevMajorState = majorState;

      // using display ring buffer
      getSystemAPI()->lockReader(true);

      // display unprocessed from fields
      ShowProcessed(false);

      strInFrame  = strBegFrame + refEditInCount;
      if (strInFrame > strEndFrame)
         strInFrame = strEndFrame;

      strOutFrame = strEndFrame - refEditOutCount;
      if (strOutFrame < strBegFrame)
         strOutFrame = strBegFrame;

      if (strOutFrame < strInFrame)
         strInFrame = strOutFrame;

      // start at IN mark
      _currentRenderFrame = strBegFrame;

      // the one frame is the reference frame
      strAlignOneFrame = strRefFrame;

      // the state table
      renderStatePtr = ReferenceFrameStateTable;
      if (strInFrame == strOutFrame) {
         renderStatePtr = OutOnlyStateTable;
      }

      // total = frame aligned + frames shifted
      int totFrames = strEndFrame - strBegFrame;

      // set the frame count
      ThreeLayerForm->ExecStatusBar->SetFrameCount(totFrames);

      // start progress feedback
      ThreeLayerForm->ExecStatusBar->StartProgress();

      // GUI to running state
      ThreeLayerForm->setGUIRunning(true);

      // clear pause flag
      pauseFlag = false;

      // here we go!
      ToNextRenderState();

      return 0;
   }

   return -1;
}

void CThreeLayer::SuspendRangeProcess(bool stop)
{
   ClearColorPickMode();

   if (renderState == RENDER_STATE_IDLE)
      return;

   if (renderState != RENDER_STATE_PAUSED) { // render state is BUSY

      ThreeLayerForm->ExecStatusBar->StopProgress(false);

      if (!stop) { // pausing

         SetToolProcessingControlState(TOOL_CONTROL_STATE_WAITING_TO_PAUSE);
      }
      else {

         SetToolProcessingControlState(TOOL_CONTROL_STATE_WAITING_TO_STOP);

         ThreeLayerForm->ExecStatusBar->SetIdle();

         setMajorState(MAJORSTATE_STOP);
      }

      pauseFlag = true;
   }
   else { // render state is PAUSED

      if (stop) {

         ExecuteStopSequence();
      }
   }
}

void CThreeLayer::ResumeRangeProcess()
{
   if (renderState != RENDER_STATE_PAUSED)
      return;

   if (MarksAreUnchanged(strBegFrame, strEndFrame)) {

      ExecuteResumeSequence();
   }
   else { // can't resume -- clear it

      // back to idle state!!
      renderState = RENDER_STATE_IDLE;

      // unlock the Player
      getSystemAPI()->lockReader(false);

      // clear the base transform but leave the offsets
      ClearBaseTransform();

      ThreeLayerForm->ExecStatusBar->StopProgress(false);

      ThreeLayerForm->
         ExecStatusBar->SetStatusMessage("Stopped                     ");

      // set the tool processing control state
      SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

      // go to auto-per-frame home
      setMajorState(prevMajorState);

      // resynchronize with GUI
      // getSystemAPI()->setDisplayProcessed(showProc);
      ShowProcessed(false);
   }
}

void CThreeLayer::SetEditIn(int edtin)
{
   editInCount = edtin;
}

void CThreeLayer::SetEditOut(int edtout)
{
   editOutCount = edtout;
}

void CThreeLayer::SetEditAvg(int edtavg)
{
   editAverage = edtavg;
}

void CThreeLayer::InitEditInOut()
{
   editInCount  = 2;
   editOutCount = 1;
   ThreeLayerForm->UpdateEditIn(editInCount);
   ThreeLayerForm->UpdateEditOut(editOutCount);
}

void CThreeLayer::InitEditAvg()
{
   editAverage = 10;
   ThreeLayerForm->UpdateEditAvg(editAverage);
}

void CThreeLayer::SetRefEditIn(int edtin)
{
   refEditInCount = edtin;
}

void CThreeLayer::SetRefEditOut(int edtout)
{
   refEditOutCount = edtout;
}

void CThreeLayer::InitRefEditInOut()
{
   refEditInCount  = 2;
   refEditOutCount = 1;
   ThreeLayerForm->UpdateRefEditIn(refEditInCount);
   ThreeLayerForm->UpdateRefEditOut(refEditOutCount);
}

void CThreeLayer::SetReferenceFrame(int reffrm)
{
   strRefFrame = reffrm;
}

void CThreeLayer::InitReferenceFrame()
{
   strRefFrame = getSystemAPI()->getLastFrameIndex();
   ThreeLayerForm->UpdateReferenceFrame(strRefFrame);
}

void CThreeLayer::ToggleWarpMode()
{
   SetWarpMode(!warpMode);
   ThreeLayerForm->UpdateWarpMode(warpMode);
}

void CThreeLayer::SetWarpMode(bool wrpmod)
{
   warpMode = wrpmod;

   if (warpMode) {

      pointsDim = movWrpPtsDim;

      if (pointsDim < MINPTS_WRP) {
         pointsDim = MINPTS_WRP;
      }
      else if (pointsDim > MAXPTS_WRP) {
         pointsDim = MAXPTS_WRP;
      }

      rangeDim = movWrpRngDim;
   }
   else {

      pointsDim = movPtsDim;

      if (pointsDim < MINPTS_MOV) {
         pointsDim = MINPTS_MOV;
      }
      else if (pointsDim > MAXPTS_MOV) {
         pointsDim = MAXPTS_MOV;
      }

      rangeDim = movRngDim;
   }

   ThreeLayerForm->UpdatePointsDim(pointsDim);

   if (rangeDim < MINRNG) rangeDim = MINRNG;
   if (rangeDim > MAXRNG) rangeDim = MAXRNG;
   ThreeLayerForm->UpdateRangeDim(rangeDim);

   // better do this
   ClearCombinedTransform();

   // repeat full align
   lastFrameAligned = -1;
}


void CThreeLayer::InitWarpMode()
{
   SetWarpMode(false);
   ThreeLayerForm->UpdateWarpMode(false);
}

void CThreeLayer::SetPointsDim(int ptsdim)
{
   if (warpMode) {

      if (ptsdim <  MINPTS_WRP) {
         ptsdim = MINPTS_WRP;
      }
      else if (ptsdim > MAXPTS_WRP) {
         ptsdim = MAXPTS_WRP;
      }

      pointsDim = ptsdim;
      movWrpPtsDim = ptsdim;
   }
   else {

      if (ptsdim < MINPTS_MOV) {
         ptsdim = MINPTS_MOV;
      }
      else if (ptsdim > MAXPTS_MOV) {
         ptsdim = MAXPTS_MOV;
      }

      pointsDim = ptsdim;
      movPtsDim = ptsdim;
   }

   // repeat full align
   lastFrameAligned = -1;
}

int CThreeLayer::GetPointsDim()
{
   return pointsDim;
}

void CThreeLayer::InitPointsDim()
{
   SetPointsDim(MINPTS_MOV);
   ThreeLayerForm->UpdatePointsDim(MINPTS_MOV);
}

void CThreeLayer::SetRangeDim(int rngdim)
{
   if (rngdim < MINRNG) rngdim = MINRNG;
   if (rngdim > MAXRNG) rngdim = MAXRNG;

   if (warpMode) {

      rangeDim = rngdim;
      movWrpRngDim = rngdim;
   }
   else {

      rangeDim = rngdim;
      movRngDim = rngdim;
   }

   // repeat full align
   lastFrameAligned = -1;
}

int CThreeLayer::GetRangeDim()
{
   return rangeDim;
}

void CThreeLayer::InitRangeDim()
{
   SetRangeDim(DFLTRNG);
   ThreeLayerForm->UpdateRangeDim(rangeDim);
}

void CThreeLayer::SetPixelsDim(int pxlsdim)
{
   pixelsDim = pxlsdim;
   if (pixelsDim < MINPXLS) pixelsDim = MINPXLS;
   if (pixelsDim > MAXPXLS) pixelsDim = MAXPXLS;

   // repeat full align
   lastFrameAligned = -1;
}

void CThreeLayer::InitPixelsDim()
{
   SetPixelsDim(DFLTPXLS);
   ThreeLayerForm->UpdatePixelsDim(pixelsDim);
}

void CThreeLayer::SetAnalysisMatBox(bool use)
{
   analyzeMatBox = use;
   ThreeLayerForm->UpdateAnalysisMatBox(use);

   // Careful: setting analysis box on/off also affects processing box!
   getSystemAPI()->SetMatBox(GetMatBoxForProcessing());
   getSystemAPI()->refreshFrameCached();
}

void CThreeLayer::SetProcessingMatBox(bool use)
{
   processMatBox = use;
   ThreeLayerForm->UpdateProcessingMatBox(use);
   getSystemAPI()->SetMatBox(GetMatBoxForProcessing());
   getSystemAPI()->refreshFrameCached();
}

void CThreeLayer::ToggleProcessingMatBox()
{
   SetProcessingMatBox(!processMatBox);
}

void CThreeLayer::SetShowMatBox(bool show)
{
   showMatBox = show;
   ThreeLayerForm->UpdateShowMatBox(show);
   getSystemAPI()->refreshFrameCached();
}

void CThreeLayer::ToggleShowMatBox()
{
   showMatBox ^= true;

   ThreeLayerForm->UpdateShowMatBox(showMatBox);

   getSystemAPI()->refreshFrameCached();
}

void CThreeLayer::SetResumeFrame(int frm)
{
   if (renderState == RENDER_STATE_PAUSED) {

      _currentRenderFrame = frm;
   }
}

void CThreeLayer::InitResumeTimecode()
{
   CVideoFrameList *frameList = getSystemAPI()->getVideoFrameList();
   if (frameList == nullptr) return;

   int inframe = getSystemAPI()->getMarkIn();
   if (inframe < 0) {
      inframe = frameList->getInFrameIndex();
   }

   ///ThreeLayerForm->ExecButtonsToolbar->ResumeTCEdit->tcP = frameList->getTimecodeForFrameIndex(inframe);
   ThreeLayerForm->UpdateResumeFrame(inframe);
}

int CThreeLayer::ClipResumeFrame(int frm)
{
   if (renderState != RENDER_STATE_PAUSED) {

      return frm;
   }

   int newResumeFrame = frm;

   if (newResumeFrame < strResumeLowerLim) {
      newResumeFrame = strResumeLowerLim;
   }
   else if (newResumeFrame > strResumeUpperLim) {
      newResumeFrame = strResumeUpperLim;
   }

   return newResumeFrame;
}

double CThreeLayer::NormalizeOffset(double offs)
{
   if (offs < 0) {
      return -((double)((int)(-offs*5.+.5))) / 5.;
   }
   else {
      return  ((double)((int)( offs*5.+.5))) / 5.;
   }
}

void CThreeLayer::ClearTransform(AffineCoeff *xfrm)
{
   xfrm->AredX = 0.0;
   xfrm->BredX = 0.0;
   xfrm->CredX = 0.0;
   xfrm->AredY = 0.0;
   xfrm->BredY = 0.0;
   xfrm->CredY = 0.0;
   xfrm->AbluX = 0.0;
   xfrm->BbluX = 0.0;
   xfrm->CbluX = 0.0;
   xfrm->AbluY = 0.0;
   xfrm->BbluY = 0.0;
   xfrm->CbluY = 0.0;
}

void CThreeLayer::ReverseTransform(AffineCoeff *xfrm)
{
   xfrm->AredX = -xfrm->AredX;
   xfrm->BredX = -xfrm->BredX;
   xfrm->CredX = -xfrm->CredX;
   xfrm->AredY = -xfrm->AredY;
   xfrm->BredY = -xfrm->BredY;
   xfrm->CredY = -xfrm->CredY;
   xfrm->AbluX = -xfrm->AbluX;
   xfrm->BbluX = -xfrm->BbluX;
   xfrm->CbluX = -xfrm->CbluX;
   xfrm->AbluY = -xfrm->AbluY;
   xfrm->BbluY = -xfrm->BbluY;
   xfrm->CbluY = -xfrm->CbluY;
}

void CThreeLayer::AddOffsetsToTransform(AffineCoeff *xfrm)
{
   xfrm->CredX += redOffsetX;
   xfrm->CredY += redOffsetY;
   xfrm->CbluX += blueOffsetX;
   xfrm->CbluY += blueOffsetY;
}

void CThreeLayer::SetCombinedTransform()
{
   AffineCoeff combParams = autoParams;

   AddOffsetsToTransform(&combParams);

   getSystemAPI()->SetRegistrationTransform(&combParams);
}

double CThreeLayer::GetWarpNorm(AffineCoeff *xfrm)
{
   double sum = 0.0;

   sum += xfrm->AredX * xfrm->AredX;
   sum += xfrm->BredX * xfrm->BredX;
   sum += xfrm->AredY * xfrm->AredY;
   sum += xfrm->BredY * xfrm->BredY;
   sum += xfrm->AbluX * xfrm->AbluX;
   sum += xfrm->BbluX * xfrm->BbluX;
   sum += xfrm->AbluY * xfrm->AbluY;
   sum += xfrm->BbluY * xfrm->BbluY;

   return (1000.*sqrt(sum));
}

void CThreeLayer::ClearBaseTransform()
{
   ClearTransform(&autoParams);

   ThreeLayerForm->UpdateRedBaseX(autoParams.CredX);
   ThreeLayerForm->UpdateRedBaseY(autoParams.CredY);
   ThreeLayerForm->UpdateBaseWarp(GetWarpNorm(&autoParams));
   ThreeLayerForm->UpdateBlueBaseX(autoParams.CbluX);
   ThreeLayerForm->UpdateBlueBaseY(autoParams.CbluY);
}

void CThreeLayer::ClearAllOffsets()
{
   redOffsetX  = 0.0;
   redOffsetY  = 0.0;
   blueOffsetX = 0.0;
   blueOffsetY = 0.0;

   ThreeLayerForm->UpdateRedOffsetX(redOffsetX);
   ThreeLayerForm->UpdateRedOffsetY(redOffsetY);
   ThreeLayerForm->UpdateWarp(0.0);
   ThreeLayerForm->UpdateBlueOffsetX(blueOffsetX);
   ThreeLayerForm->UpdateBlueOffsetY(blueOffsetY);

   SetCombinedTransform();
}

void CThreeLayer::ClearCombinedTransform()
{
   ClearBaseTransform();
   ClearAllOffsets();
}

void CThreeLayer::AlignCurrentFrameSelChannels(bool render)
{
   if (renderState != RENDER_STATE_IDLE)
      return;

   // manual modes goto current frame
   int thefrm = getSystemAPI()->getLastFrameIndex();

   ExecuteSingleFrame(thefrm, render);

   // make sure ARROW keys will be enabled
   ThreeLayerForm->DummyEdit->SetFocus();
}

void CThreeLayer::AlignNextFrameSelChannels(bool render)
{
   if (renderState != RENDER_STATE_IDLE)
      return;

   int thefrm = getSystemAPI()->getLastFrameIndex() + 1;

   ExecuteSingleFrame(thefrm, render);

   // make sure ARROW keys will be enabled
   ThreeLayerForm->DummyEdit->SetFocus();
}

void CThreeLayer::AlignBetweenMarksSelChannels(bool render)
{
   if (renderState != RENDER_STATE_IDLE)
      return;

   // make sure ARROW keys will be enabled
   ThreeLayerForm->DummyEdit->SetFocus();

   // set the majorState for ALIGN
   if ((majorState == MAJORSTATE_AUTO_PER_SHOT_HOME)||
       (majorState == MAJORSTATE_AUTO_PER_FRAME_HOME)||
       (majorState == MAJORSTATE_MANUAL_HOME)||
       (majorState == MAJORSTATE_REFERENCE_FRAME_HOME)) {

       // switch states, change GUI, and
       setMajorState(MAJORSTATE_MANUAL_GANGED, true, showProc);
   }

   ExecuteRangeProcess(render);
}

void CThreeLayer::LoadReferenceFrame()
{
   if (resetMajorState == MAJORSTATE_REFERENCE_FRAME_HOME) {

      CVideoFrameList *frameList = getSystemAPI()->getVideoFrameList();
      if (frameList == nullptr) return;

      int curframe = getSystemAPI()->getLastFrameIndex();

      ThreeLayerForm->UpdateReferenceFrame(curframe);

      ClearBaseTransform();

      SetCombinedTransform();

      lastFrameAligned = -1;
   }
}

void CThreeLayer::GoToReferenceFrame()
{
   if (resetMajorState == MAJORSTATE_REFERENCE_FRAME_HOME) {

      CVideoFrameList *frameList = getSystemAPI()->getVideoFrameList();
      if (frameList == nullptr) return;

      int refframe =
         frameList->getFrameIndex(ThreeLayerForm->ReferenceFrameTCEdit->tcP);

      getSystemAPI()->goToFrameSynchronous(refframe);
   }
}

void CThreeLayer::ToggleShowAlignPoints()
{
   if ((renderState == RENDER_STATE_IDLE)&&
       (resetMajorState == MAJORSTATE_MANUAL_HOME)) {

      showAlignPts ^= true;
      getSystemAPI()->refreshFrameCached();
   }
}

////////////////////////////////////////////////////////////////////////////////

void CThreeLayerParameters::setResetMajorState(int state)
{
   resetMajorState = state;
}

int CThreeLayerParameters::getResetMajorState()
{
   return resetMajorState;
}

void CThreeLayerParameters::setShowProcessed(bool proc)
{
   showProcessed = proc;
}

bool CThreeLayerParameters::getShowProcessed()
{
   return showProcessed;
}

void CThreeLayerParameters::setEditCounts(int edtin,    int edtout, int edtavg,
                                          int refedtin, int refedtout)
{
   editInCount     = edtin;
   editOutCount    = edtout;
   editAverage     = edtavg;
   refEditInCount  = refedtin;
   refEditOutCount = refedtout;
}

void CThreeLayerParameters::getEditCounts(int *edtin, int *edtout, int *edtavg,
                                          int *refedtin, int *refedtout)
{
   *edtin     = editInCount;
   *edtout    = editOutCount;
   *edtavg    = editAverage;
   *refedtin  = refEditInCount;
   *refedtout = refEditOutCount;
}

void CThreeLayerParameters::initOffsets()
{
   redOffsetX  = 0.0;
   redOffsetY  = 0.0;
   blueOffsetX = 0.0;
   blueOffsetY = 0.0;
}

void CThreeLayerParameters::setOffsets( AffineCoeff *xform,
                                        double roffx, double roffy,
                                        double boffx, double boffy)
{
   autoParams = *xform;

   redOffsetX  = roffx;
   redOffsetY  = roffy;
   blueOffsetX = boffx;
   blueOffsetY = boffy;
}

void CThreeLayerParameters::getOffsets( AffineCoeff *xform,
                                        double *roffx, double *roffy,
                                        double *boffx, double *boffy)
{
   *xform = autoParams;

   *roffx = redOffsetX;
   *roffy = redOffsetY;
   *boffx = blueOffsetX;
   *boffy = blueOffsetY;
}

void CThreeLayerParameters::setAnalysisParams(bool wrpmod, int ptsdim, int rngdim)
{
   warpMode = wrpmod;
   pointsDim = ptsdim;
   rangeDim = rngdim;
}

void CThreeLayerParameters::getAnalysisParams(bool *wrpmod, int *ptsdim, int *rngdim)
{
   *wrpmod = warpMode;
   *ptsdim = pointsDim;
   *rngdim = rangeDim;
}

void CThreeLayerParameters::setMatParams(RECT *matbox, bool analyzemat,
                                         bool processmat, bool showmat)
{
   matBox = *matbox;
   analyzeMatBox = analyzemat;
   processMatBox = processmat;
   showMatBox    = showmat;
}

void CThreeLayerParameters::getMatParams(RECT *matbox, bool *analyzemat,
                                         bool *processmat, bool *showmat)
{
   *matbox     = matBox;
   *analyzemat = analyzeMatBox;
   *processmat = processMatBox;
   *showmat    = showMatBox;
}

void CThreeLayerParameters::setReferenceFrame (int reffrm)
{
   referenceFrame = reffrm;
}

void CThreeLayerParameters::getReferenceFrame(int *reffrm)
{
   *reffrm = referenceFrame;
}

void CThreeLayerParameters::writePDLEntry(CPDLElement &parent)
{
   CPDLElement *pdlToolAttribs = parent.MakeNewChild("LayerOffsets");

   pdlToolAttribs->SetAttribInteger( ResetMajorStateKey, resetMajorState);
   pdlToolAttribs->SetAttribBool( ShowProcessedKey, showProcessed);

   pdlToolAttribs->SetAttribInteger( EditInCountKey, editInCount);
   pdlToolAttribs->SetAttribInteger(EditOutCountKey, editOutCount);
   pdlToolAttribs->SetAttribInteger(EditAverageKey, editAverage);
   pdlToolAttribs->SetAttribInteger( RefEditInCountKey, refEditInCount);
   pdlToolAttribs->SetAttribInteger(RefEditOutCountKey, refEditOutCount);
   pdlToolAttribs->SetAttribInteger(ReferenceFrameKey, referenceFrame);

   pdlToolAttribs->SetAttribDouble( RedOffsetXKey,  redOffsetX);
   pdlToolAttribs->SetAttribDouble( RedOffsetYKey,  redOffsetY);
   pdlToolAttribs->SetAttribDouble(BlueOffsetXKey, blueOffsetX);
   pdlToolAttribs->SetAttribDouble(BlueOffsetYKey, blueOffsetY);

   pdlToolAttribs->SetAttribDouble( ARedXKey,  autoParams.AredX);
   pdlToolAttribs->SetAttribDouble( BRedXKey,  autoParams.BredX);
   pdlToolAttribs->SetAttribDouble( CRedXKey,  autoParams.CredX);
   pdlToolAttribs->SetAttribDouble( ARedYKey,  autoParams.AredY);
   pdlToolAttribs->SetAttribDouble( BRedYKey,  autoParams.BredY);
   pdlToolAttribs->SetAttribDouble( CRedYKey,  autoParams.CredY);
   pdlToolAttribs->SetAttribDouble( ABluXKey,  autoParams.AbluX);
   pdlToolAttribs->SetAttribDouble( BBluXKey,  autoParams.BbluX);
   pdlToolAttribs->SetAttribDouble( CBluXKey,  autoParams.CbluX);
   pdlToolAttribs->SetAttribDouble( ABluYKey,  autoParams.AbluY);
   pdlToolAttribs->SetAttribDouble( BBluYKey,  autoParams.BbluY);
   pdlToolAttribs->SetAttribDouble( CBluYKey,  autoParams.CbluY);

   pdlToolAttribs->SetAttribBool( WarpModeKey, warpMode);
   pdlToolAttribs->SetAttribInteger( PointsDimKey, pointsDim);
   pdlToolAttribs->SetAttribInteger( RangeDimKey, rangeDim);

   pdlToolAttribs->SetAttribInteger( MatBoxLeftKey, matBox.left);
   pdlToolAttribs->SetAttribInteger( MatBoxTopKey, matBox.top);
   pdlToolAttribs->SetAttribInteger( MatBoxRightKey, matBox.right);
   pdlToolAttribs->SetAttribInteger( MatBoxBottomKey, matBox.bottom);

   pdlToolAttribs->SetAttribBool( AnalyzeMatKey, analyzeMatBox);
   pdlToolAttribs->SetAttribBool( ProcessMatKey, processMatBox);
   pdlToolAttribs->SetAttribBool( ShowMatKey, showMatBox);
}

void CThreeLayerParameters::readPDLEntry(CPDLElement *pdlToolAttribs)
{
   resetMajorState    = pdlToolAttribs->
     GetAttribInteger(ResetMajorStateKey, MAJORSTATE_MANUAL_HOME);
   showProcessed = pdlToolAttribs->GetAttribBool(ShowProcessedKey, false);

   editInCount      = pdlToolAttribs->GetAttribInteger(EditInCountKey, 2);
   editOutCount     = pdlToolAttribs->GetAttribInteger(EditOutCountKey, 2);
   editAverage      = pdlToolAttribs->GetAttribInteger(EditAverageKey, 1000000);
   refEditInCount   = pdlToolAttribs->GetAttribInteger(RefEditInCountKey, 2);
   refEditOutCount  = pdlToolAttribs->GetAttribInteger(RefEditOutCountKey, 2);
   referenceFrame = pdlToolAttribs->GetAttribInteger(ReferenceFrameKey, -1);

   redOffsetX  = pdlToolAttribs->GetAttribDouble( RedOffsetXKey, 0.0);
   redOffsetY  = pdlToolAttribs->GetAttribDouble( RedOffsetYKey, 0.0);
   blueOffsetX = pdlToolAttribs->GetAttribDouble(BlueOffsetXKey, 0.0);
   blueOffsetY = pdlToolAttribs->GetAttribDouble(BlueOffsetYKey, 0.0);

   autoParams.AredX = pdlToolAttribs->GetAttribDouble( ARedXKey,  0.0);
   autoParams.BredX = pdlToolAttribs->GetAttribDouble( BRedXKey,  0.0);
   autoParams.CredX = pdlToolAttribs->GetAttribDouble( CRedXKey,  0.0);
   autoParams.AredY = pdlToolAttribs->GetAttribDouble( ARedYKey,  0.0);
   autoParams.BredY = pdlToolAttribs->GetAttribDouble( BRedYKey,  0.0);
   autoParams.CredY = pdlToolAttribs->GetAttribDouble( CRedYKey,  0.0);
   autoParams.AbluX = pdlToolAttribs->GetAttribDouble( ABluXKey,  0.0);
   autoParams.BbluX = pdlToolAttribs->GetAttribDouble( BBluXKey,  0.0);
   autoParams.CbluX = pdlToolAttribs->GetAttribDouble( CBluXKey,  0.0);
   autoParams.AbluY = pdlToolAttribs->GetAttribDouble( ABluYKey,  0.0);
   autoParams.BbluY = pdlToolAttribs->GetAttribDouble( BBluYKey,  0.0);
   autoParams.CbluY = pdlToolAttribs->GetAttribDouble( CBluYKey,  0.0);

   warpMode  = pdlToolAttribs->GetAttribBool(WarpModeKey, false);
   pointsDim = pdlToolAttribs->GetAttribInteger(PointsDimKey, 2);
   rangeDim  = pdlToolAttribs->GetAttribInteger(RangeDimKey, 2);

   matBox.left   = pdlToolAttribs->GetAttribInteger(MatBoxLeftKey,   0);
   matBox.top    = pdlToolAttribs->GetAttribInteger(MatBoxTopKey,    0);
   matBox.right  = pdlToolAttribs->GetAttribInteger(MatBoxRightKey,  0);
   matBox.bottom = pdlToolAttribs->GetAttribInteger(MatBoxBottomKey, 0);

   analyzeMatBox  = pdlToolAttribs->GetAttribBool(AnalyzeMatKey, true);
   processMatBox  = pdlToolAttribs->GetAttribBool(ProcessMatKey, true);
   showMatBox     = pdlToolAttribs->GetAttribBool(ShowMatKey, true);
}

////////////////////////////////////////////////////////////////////////////////
//
void CThreeLayer::ExecutePauseSequence(int resumeUpperLimitArg, bool updateToolProcessingControlState)
{
   ThreeLayerForm->setGUIPause(true);

   ThreeLayerForm->
      ExecStatusBar->SetStatusMessage("Paused                     ");

   // Limits of adjusted resume frame. Always start at _currentRenderFrame.
   strResumeLowerLim = _currentRenderFrame;
   strResumeUpperLim = resumeUpperLimitArg;

   // lower limit loaded into GUI
   ThreeLayerForm->UpdateResumeFrame(strResumeLowerLim);

   savedRenderState = renderState;

   renderState = RENDER_STATE_PAUSED;

   // Unlock the Player. Do this BEFORE checking if we are stopping!
   getSystemAPI()->lockReader(false);

   if (majorState == MAJORSTATE_STOP) {
      ExecuteStopSequence();
   }
   else if (updateToolProcessingControlState) {
      SetToolProcessingControlState(TOOL_CONTROL_STATE_PAUSED);
   }
}

void CThreeLayer::ExecuteResumeSequence()
{
   if (renderState != RENDER_STATE_PAUSED) {

      return;
   }

   // Maybe the resume frame changed.
   strResumeLowerLim = ClipResumeFrame(_currentRenderFrame);

   // Relock the Player
   getSystemAPI()->lockReader(true);

   ThreeLayerForm->ExecStatusBar->StartProgress();

   ThreeLayerForm->setGUIPause(false);

   renderState = savedRenderState;
}

void CThreeLayer::ExecuteStopSequence()
{
   // unlock the Player
   if (renderState != RENDER_STATE_PAUSED) {

      getSystemAPI()->lockReader(false);
   }

   // back to idle state
   renderState = RENDER_STATE_IDLE;

   // set the tool processing control state
   SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

   // go to auto-per-shot home
   setMajorState(prevMajorState, true, showProc);

   ThreeLayerForm->
      ExecStatusBar->SetStatusMessage("Stopped                     ");
}

void CThreeLayer::ExecuteFinishSequence()
{
   // back to idle state
   renderState = RENDER_STATE_IDLE;

   // unlock the Player
   getSystemAPI()->lockReader(false);

   // set the tool processing control state
   SetToolProcessingControlState(TOOL_CONTROL_STATE_STOPPED);

   // go to auto-per-shot home
   setMajorState(prevMajorState, true, showProc);

   ThreeLayerForm->
      ExecStatusBar->SetStatusMessage("Complete                    ");

   // this is tricky: if you previewed one frame,
   // load the combined transform into the Displayer
   // so it will show its effect as you display.
   // Otherwise just show untransformed during display
   if (oneFrame&&!renderFlag) {

      SetCombinedTransform();

      // make sure that "goToFrame" is
      // called from the main thread
      ThreeLayerForm->SetRefreshTimer(0);
   }
   else { // more than one frame or render
      ShowProcessed(false);
   }
}

void CThreeLayer::ToNextRenderState()
{
   renderState = *renderStatePtr++;

   switch(renderState) {

      case RENDER_STATE_IDLE:

         ThreeLayerForm->ExecStatusBar->StopProgress();

         ExecuteFinishSequence();

      break;

      case RENDER_STATE_ALIGN_ONE:
      case RENDER_STATE_AVERAGE:

         //ThreeLayerForm->
         //   ExecStatusBar->SetStatusMessage("Analyzing                 ");

      break;

      case RENDER_STATE_APPLY_ONE:
      case RENDER_STATE_RUNNING:

         //ThreeLayerForm->
         //  ExecStatusBar->SetStatusMessage("Processing                 ");

      break;
   }
}

void CThreeLayer::TestAlignInForeground()
{
   int frmnum = getSystemAPI()->getLastFrameIndex();

   MTI_UINT16 *preFrame = getSystemAPI()->getPreRegisteredIntermediate();

   if (getSystemAPI()->getToolFrame(preFrame, frmnum) == -1)
      return;

   const CImageFormat *imgFmt = getSystemAPI()->getVideoClipImageFormat();

   int retVal = AutoRegistrar->RegisterFrame(preFrame,
                                             imgFmt,
                                             GetMatBoxForAnalysis(),
                                             nullptr,
                                             &autoParams,
                                             warpMode,
                                             pointsDim,
                                             rangeDim,
                                             pixelsDim);
   if (retVal != 0)
      return;

   lastFrameAligned = frmnum;

   // transform which aligns
   ReverseTransform(&autoParams);

   // update base values in GUI
   ThreeLayerForm->UpdateRedBaseX(autoParams.CredX);
   ThreeLayerForm->UpdateRedBaseY(autoParams.CredY);
   ThreeLayerForm->UpdateBaseWarp(GetWarpNorm(&autoParams));
   ThreeLayerForm->UpdateBlueBaseX(autoParams.CbluX);
   ThreeLayerForm->UpdateBlueBaseY(autoParams.CbluY);
}

int CThreeLayer::AlignAndWritebackFrame(int frmnum, bool render, bool *pauseflag)
{
   if (frmnum == -1)
      return -1;

   MTI_UINT16 *preFrame = getSystemAPI()->getPreRegisteredIntermediate();

   if (getSystemAPI()->getToolFrame(preFrame, frmnum) == -1)
      return -1;

   const CImageFormat *imgFmt = getSystemAPI()->getVideoClipImageFormat();

   ThreeLayerForm->
      ExecStatusBar->SetStatusMessage("Analyzing                     ");

   int retVal = AutoRegistrar->RegisterFrame(preFrame,
                                            imgFmt,
                                            GetMatBoxForAnalysis(),
                                            pauseflag,
                                            &autoParams,
                                            warpMode,
                                            pointsDim,
                                            rangeDim,
                                            pixelsDim);
   if (retVal != 0)
      return retVal;

   lastFrameAligned = -1;

   // transform which aligns
   ReverseTransform(&autoParams);

   // update base values in GUI
   ThreeLayerForm->UpdateRedBaseX(autoParams.CredX);
   ThreeLayerForm->UpdateRedBaseY(autoParams.CredY);
   ThreeLayerForm->UpdateBaseWarp(GetWarpNorm(&autoParams));
   ThreeLayerForm->UpdateBlueBaseX(autoParams.CbluX);
   ThreeLayerForm->UpdateBlueBaseY(autoParams.CbluY);

   // include any manual offsets
   AffineCoeff combParams = autoParams;
   combParams.CredX += redOffsetX;
   combParams.CredY += redOffsetY;
   combParams.CbluX += blueOffsetX;
   combParams.CbluY += blueOffsetY;

   ThreeLayerForm->
      ExecStatusBar->SetStatusMessage("Processing                    ");

   MTI_UINT16 *regFrame = getSystemAPI()->getRegisteredIntermediate();

   Registrar->registerChannels(regFrame,
                               preFrame,
                               imgFmt,
                               GetMatBoxForProcessing(),
                               &combParams);

   if (render) { // write the registered frame back

      // this is nullptr for the Three-Layer Tool
      CPixelRegionList originalPixels;

      getSystemAPI()->putToolFrame(regFrame,
                                   preFrame,
                                   originalPixels,
                                   frmnum);
   }

   // refresh the registered frame by dropping it
   // into the Player's display ring-buffer (!)
   getSystemAPI()->displayFromRenderThread(regFrame,
                                           frmnum);

   // success
   return 0;
}

int CThreeLayer::AlignSingleFrame(int frm, bool *pauseflag)
{
   int retVal;

   if (frm == -1)
      return -1;

   ThreeLayerForm->
      ExecStatusBar->SetStatusMessage("Analyzing                     ");

   if (majorState == MAJORSTATE_MANUAL_RED) {

         autoParams.AredX = 0.0;
         autoParams.BredX = 0.0;
         autoParams.CredX = 0.0;
         autoParams.AredY = 0.0;
         autoParams.BredY = 0.0;
         autoParams.CredY = 0.0;

         ThreeLayerForm->UpdateBaseWarp(0.0);
         ThreeLayerForm->UpdateRedBaseX(autoParams.CredX);
         ThreeLayerForm->UpdateRedBaseY(autoParams.CredY);

         if (resetMajorState == MAJORSTATE_MANUAL_HOME) {

            redOffsetX = 0.0;
            redOffsetY = 0.0;

            ThreeLayerForm->UpdateWarp(0.0);
            ThreeLayerForm->UpdateRedOffsetX(redOffsetX);
            ThreeLayerForm->UpdateRedOffsetY(redOffsetY);
         }

         MTI_UINT16 *preFrame = getSystemAPI()->getPreRegisteredIntermediate();

         if (getSystemAPI()->getToolFrame(preFrame, frm) == -1)
            return -1;

         const CImageFormat *imgFmt = getSystemAPI()->getVideoClipImageFormat();

         AffineCoeff redParams;

         retVal = AutoRegistrar->RegisterFrame(preFrame,
                                               imgFmt,
                                               GetMatBoxForAnalysis(),
                                               pauseflag,
                                               &redParams,
                                               warpMode,
                                               pointsDim,
                                               rangeDim,
                                               pixelsDim);
         if (retVal != 0)
            return retVal;

         lastFrameAligned = frm;

         // this is the transform which aligns
         ReverseTransform(&redParams);

         // load these
         autoParams.AredX = redParams.AredX;
         autoParams.BredX = redParams.BredX;
         autoParams.AredY = redParams.AredY;
         autoParams.BredY = redParams.BredY;

         if (resetMajorState == MAJORSTATE_MANUAL_HOME) {

            // zero these out
            autoParams.CredX = 0.0;
            autoParams.CredY = 0.0;

            // load offsets
            redOffsetX  = redParams.CredX;
            redOffsetY  = redParams.CredY;

            ThreeLayerForm->UpdateBaseWarp(0.0);
            ThreeLayerForm->UpdateWarp(GetWarpNorm(&autoParams));
         }
         else if ((resetMajorState == MAJORSTATE_AUTO_PER_SHOT_HOME)||
                  (resetMajorState == MAJORSTATE_AUTO_PER_FRAME_HOME)||
                  (resetMajorState == MAJORSTATE_REFERENCE_FRAME_HOME)) {

            // load these
            autoParams.CredX = redParams.CredX;
            autoParams.CredY = redParams.CredY;

            ThreeLayerForm->UpdateBaseWarp(GetWarpNorm(&autoParams));
            ThreeLayerForm->UpdateWarp(0.0);
         }

         ThreeLayerForm->UpdateRedBaseX(autoParams.CredX);
         ThreeLayerForm->UpdateRedBaseY(autoParams.CredY);
         ThreeLayerForm->UpdateRedOffsetX(redOffsetX);
         ThreeLayerForm->UpdateRedOffsetY(redOffsetY);
   }
   else if (majorState == MAJORSTATE_MANUAL_BLUE) {

         autoParams.AbluX = 0.0;
         autoParams.BbluX = 0.0;
         autoParams.CbluX = 0.0;
         autoParams.AbluY = 0.0;
         autoParams.BbluY = 0.0;
         autoParams.CbluY = 0.0;

         ThreeLayerForm->UpdateBaseWarp(0.0);
         ThreeLayerForm->UpdateBlueBaseX(autoParams.CbluX);
         ThreeLayerForm->UpdateBlueBaseY(autoParams.CbluY);

         if (resetMajorState == MAJORSTATE_MANUAL_HOME) {

            blueOffsetX = 0.0;
            blueOffsetY = 0.0;

            ThreeLayerForm->UpdateWarp(0.0);
            ThreeLayerForm->UpdateBlueOffsetX(blueOffsetX);
            ThreeLayerForm->UpdateBlueOffsetY(blueOffsetY);
         }

         MTI_UINT16 *preFrame = getSystemAPI()->getPreRegisteredIntermediate();

         if (getSystemAPI()->getToolFrame(preFrame, frm) == -1)
            return -1;

         const CImageFormat *imgFmt = getSystemAPI()->getVideoClipImageFormat();

         AffineCoeff bluParams;

         retVal = AutoRegistrar->RegisterFrame(preFrame,
                                               imgFmt,
                                               GetMatBoxForAnalysis(),
                                               pauseflag,
                                               &bluParams,
                                               warpMode,
                                               pointsDim,
                                               rangeDim,
                                               pixelsDim);
         if (retVal != 0)
            return retVal;

         lastFrameAligned = frm;

         // this is the transform which aligns
         ReverseTransform(&bluParams);

         // load these
         autoParams.AbluX = bluParams.AbluX;
         autoParams.BbluX = bluParams.BbluX;
         autoParams.AbluY = bluParams.AbluY;
         autoParams.BbluY = bluParams.BbluY;

         if (resetMajorState == MAJORSTATE_MANUAL_HOME) {

            // zero these out
            autoParams.CbluX = 0.0;
            autoParams.CbluY = 0.0;

            // load offsets
            blueOffsetX  = bluParams.CbluX;
            blueOffsetY  = bluParams.CbluY;

            ThreeLayerForm->UpdateBaseWarp(0.0);
            ThreeLayerForm->UpdateWarp(GetWarpNorm(&autoParams));
         }
         else if ((resetMajorState == MAJORSTATE_AUTO_PER_SHOT_HOME)||
                  (resetMajorState == MAJORSTATE_AUTO_PER_FRAME_HOME)||
                  (resetMajorState == MAJORSTATE_REFERENCE_FRAME_HOME)) {

            // load these
            autoParams.CbluX = bluParams.CbluX;
            autoParams.CbluY = bluParams.CbluY;

            ThreeLayerForm->UpdateBaseWarp(GetWarpNorm(&autoParams));
            ThreeLayerForm->UpdateWarp(0.0);
         }

         ThreeLayerForm->UpdateBlueBaseX(autoParams.CbluX);
         ThreeLayerForm->UpdateBlueBaseY(autoParams.CbluY);
         ThreeLayerForm->UpdateWarp(GetWarpNorm(&autoParams));
         ThreeLayerForm->UpdateBlueOffsetX(blueOffsetX);
         ThreeLayerForm->UpdateBlueOffsetY(blueOffsetY);
   }
   else if (majorState == MAJORSTATE_MANUAL_GANGED) {

         ClearTransform(&autoParams);

         ThreeLayerForm->UpdateBaseWarp(0.0);
         ThreeLayerForm->UpdateRedBaseX(autoParams.CredX);
         ThreeLayerForm->UpdateRedBaseY(autoParams.CredY);
         ThreeLayerForm->UpdateBlueBaseX(autoParams.CbluX);
         ThreeLayerForm->UpdateBlueBaseY(autoParams.CbluY);

         if (resetMajorState == MAJORSTATE_MANUAL_HOME) {

            redOffsetX  = 0.0;
            redOffsetY  = 0.0;
            blueOffsetX = 0.0;
            blueOffsetY = 0.0;

            ThreeLayerForm->UpdateWarp(0.0);
            ThreeLayerForm->UpdateRedOffsetX(redOffsetX);
            ThreeLayerForm->UpdateRedOffsetY(redOffsetY);
            ThreeLayerForm->UpdateBlueOffsetX(blueOffsetX);
            ThreeLayerForm->UpdateBlueOffsetY(blueOffsetY);
         }

         MTI_UINT16 *preFrame = getSystemAPI()->getPreRegisteredIntermediate();

         if (getSystemAPI()->getToolFrame(preFrame, frm) == -1)
            return -1;

         const CImageFormat *imgFmt = getSystemAPI()->getVideoClipImageFormat();

         retVal = AutoRegistrar->RegisterFrame(preFrame,
                                               imgFmt,
                                               GetMatBoxForAnalysis(),
                                               pauseflag,
                                               &autoParams,
                                               warpMode,
                                               pointsDim,
                                               rangeDim,
                                               pixelsDim);
         if (retVal != 0)
            return retVal;

         lastFrameAligned = frm;

         // this is the transform which aligns
         ReverseTransform(&autoParams);

         if (resetMajorState == MAJORSTATE_MANUAL_HOME) {

            // load the offsets
            redOffsetX  = autoParams.CredX;
            redOffsetY  = autoParams.CredY;
            blueOffsetX = autoParams.CbluX;
            blueOffsetY = autoParams.CbluY;

            // zero these out
            autoParams.CredX = 0.0;
            autoParams.CredY = 0.0;
            autoParams.CbluX = 0.0;
            autoParams.CbluY = 0.0;

            ThreeLayerForm->UpdateBaseWarp(0.0);
            ThreeLayerForm->UpdateWarp(GetWarpNorm(&autoParams));
         }
         else if ((resetMajorState == MAJORSTATE_AUTO_PER_SHOT_HOME)||
                  (resetMajorState == MAJORSTATE_AUTO_PER_FRAME_HOME)||
                  (resetMajorState == MAJORSTATE_REFERENCE_FRAME_HOME)) {

            // data already in autoParams - leave manual offsets alone

            ThreeLayerForm->UpdateBaseWarp(GetWarpNorm(&autoParams));
            ThreeLayerForm->UpdateWarp(0.0);
         }

      ThreeLayerForm->UpdateRedBaseX(autoParams.CredX);
      ThreeLayerForm->UpdateRedBaseY(autoParams.CredY);
      ThreeLayerForm->UpdateBlueBaseX(autoParams.CbluX);
      ThreeLayerForm->UpdateBlueBaseY(autoParams.CbluY);

      ThreeLayerForm->UpdateRedOffsetX(redOffsetX);
      ThreeLayerForm->UpdateRedOffsetY(redOffsetY);
      ThreeLayerForm->UpdateBlueOffsetX(blueOffsetX);
      ThreeLayerForm->UpdateBlueOffsetY(blueOffsetY);
   }

   return 0;
}

void CThreeLayer::InitForCapture(int frmcnt, bool *pauseflag)
{
   const CImageFormat *imgFmt = getSystemAPI()->getVideoClipImageFormat();

   AutoRegistrar->InitTransform(imgFmt,
                                GetMatBoxForAnalysis(),
                                pauseflag,
                                frmcnt,
                                pointsDim,
                                rangeDim,
                                pixelsDim);
}

int CThreeLayer::CaptureSingleFrame(int frmnum)
{
   if (frmnum == -1) return -1;

   MTI_UINT16 *preFrame = getSystemAPI()->getPreRegisteredIntermediate();

   if (getSystemAPI()->getToolFrame(preFrame, frmnum) == -1)
      return -1;

   ThreeLayerForm->
      ExecStatusBar->SetStatusMessage("Analyzing                     ");

   int retVal = AutoRegistrar->RegisterFrame(preFrame);

   if (retVal != 0)
      return retVal;

   lastFrameAligned = -1;

   AffineCoeff lastParams;
   AutoRegistrar->GetLastFramesRegistrationTransform(&lastParams,
                                                        warpMode);
   ReverseTransform(&lastParams);

   ThreeLayerForm->UpdateRedBaseX(lastParams.CredX);
   ThreeLayerForm->UpdateRedBaseY(lastParams.CredY);
   ThreeLayerForm->UpdateBaseWarp(GetWarpNorm(&lastParams));
   ThreeLayerForm->UpdateBlueBaseX(lastParams.CbluX);
   ThreeLayerForm->UpdateBlueBaseY(lastParams.CbluY);

   getSystemAPI()->displayFromRenderThread(preFrame,
                                           frmnum);
   return retVal;
}

void CThreeLayer::SetCapturedTransform()
{
   AutoRegistrar->GetAllFramesRegistrationTransform(&autoParams, warpMode);

   ReverseTransform(&autoParams);

   // show the align results in the GUI
   ThreeLayerForm->UpdateRedBaseX(autoParams.CredX);
   ThreeLayerForm->UpdateRedBaseY(autoParams.CredY);
   ThreeLayerForm->UpdateBlueBaseX(autoParams.CbluX);
   ThreeLayerForm->UpdateBlueBaseY(autoParams.CbluY);
   ThreeLayerForm->UpdateWarp(GetWarpNorm(&autoParams));
}

void CThreeLayer::TransformAndWritebackFrame(int frmnum , bool render)
{
   if (frmnum == -1) return;

   MTI_UINT16 *preFrame = getSystemAPI()->getPreRegisteredIntermediate();

   if (getSystemAPI()->getToolFrame(preFrame, frmnum) == -1)
      return;

   MTI_UINT16 *regFrame = getSystemAPI()->getRegisteredIntermediate();

   AffineCoeff combParams;

   combParams.AredX = autoParams.AredX;
   combParams.AredY = autoParams.AredY;
   combParams.BredX = autoParams.BredX;
   combParams.BredY = autoParams.BredY;
   combParams.CredX = autoParams.CredX + redOffsetX;
   combParams.CredY = autoParams.CredY + redOffsetY;

   combParams.AbluX = autoParams.AbluX;
   combParams.AbluY = autoParams.AbluY;
   combParams.BbluX = autoParams.BbluX;
   combParams.BbluY = autoParams.BbluY;
   combParams.CbluX = autoParams.CbluX + blueOffsetX;
   combParams.CbluY = autoParams.CbluY + blueOffsetY;

   const CImageFormat *imgFmt = getSystemAPI()->getVideoClipImageFormat();

   ThreeLayerForm->
      ExecStatusBar->SetStatusMessage("Processing                    ");

   Registrar->registerChannels(regFrame,
                               preFrame,
                               imgFmt,
                               GetMatBoxForProcessing(),
                               &combParams);

   if (render) { // write the registered frame back

      // this is nullptr for the Three-Layer Tool
      CPixelRegionList originalPixels;

      getSystemAPI()->putToolFrame(regFrame,
                                   preFrame,
                                   originalPixels,
                                   frmnum);
   }

   // refresh the registered frame by dropping it
   // into the Player's display ring-buffer (!)
   getSystemAPI()->displayFromRenderThread(regFrame,
                                           frmnum);
}

void CThreeLayer::BackgroundAlignRender(void *vr)
{
   renderState = RENDER_STATE_IDLE;

   BThreadBegin(vr);

   while (renderState != RENDER_STATE_DISABLED) {

      switch(renderState) {

         case RENDER_STATE_IDLE:
         case RENDER_STATE_PAUSED:

            MTImillisleep(1);

         break;

         case RENDER_STATE_IN_FRAMES:

            // AutoPerShot & ReferenceFrame modes

            if (pauseFlag) {

               pauseFlag = false;
               ExecutePauseSequence(strInFrame - 1, true);
            }
            else {

               if (_currentRenderFrame < strInFrame) {

                  // align individual frame
                  int retVal = AlignAndWritebackFrame(_currentRenderFrame,
                                                      renderFlag,
                                                      &pauseFlag);
                  if (retVal == 0) {

                     // show the progress
                     ThreeLayerForm->ExecStatusBar->BumpProgress();

                     // to next frame
                     _currentRenderFrame++;
                  }
               }
               else {

                  // done w IN frames - new state
                  ToNextRenderState();
               }
            }

         break;

         case RENDER_STATE_ALIGN_ONE:
         {
            // do simple align of ref frame
            int retVal = AlignSingleFrame(strAlignOneFrame, nullptr);

            if (retVal == 0) {

               // transition (to RENDER_STATE_RUNNING)
               ToNextRenderState();
            }
         }
         break;

         case RENDER_STATE_AVERAGE:

            // AutoPerShot - see 'ExecuteRangeProcess'
            // for the initialization of 'avgFrame'

            if (pauseFlag) {

               pauseFlag = false;
               ExecutePauseSequence(_currentRenderFrame, false);
            }
            else {

               _currentRenderFrame = (int)avgFrame;

               if (_currentRenderFrame <= strOutFrame - frameStep) {

                  if (_currentRenderFrame == strInFrame) {

                     ClearBaseTransform();

                     // set up accumulation
                     InitForCapture(framesAveraged+4, &pauseFlag);

                     // advance to the next frame
                     avgFrame += frameStep;
                  }
                  else {

                     // do the points for one frame
                     int retVal = CaptureSingleFrame(_currentRenderFrame);

                     if (retVal == 0) {

                        // advance to the next frame
                        avgFrame += frameStep;
                     }
                  }
               }
               else {

                  // we want the user to be able to
                  // read the base settings for the
                  // last frame included in the avg
                  // before showing all frames' net
                  MTImillisleep(3000);

                  // grab the accumulated transform
                  SetCapturedTransform();

                  // back to beginning
                  _currentRenderFrame = strInFrame;

                  // transition (to RENDER_STATE_RUNNING)
                  ToNextRenderState();
               }
            }

         break;

         case RENDER_STATE_APPLY_ONE:

            // apply the current combined transform to
            // the frame and write it back to the media
            TransformAndWritebackFrame(strApplyOneFrame, renderFlag);

            // show the progress
            ThreeLayerForm->ExecStatusBar->BumpProgress();

            // transition (to RENDER_STATE_IDLE)
            ToNextRenderState();

         break;

         case RENDER_STATE_RUNNING:

            // AutoPerShot, Manual, & ReferenceFrame modes

            if (pauseFlag) {

               pauseFlag = false;
               ExecutePauseSequence(strOutFrame - 1, true);
            }
            else {

               if (_currentRenderFrame < strOutFrame) {

                  // apply the current combined transform to
                  // the frame and write it back to the media
                  TransformAndWritebackFrame(_currentRenderFrame, renderFlag);

                  // show the progress
                  ThreeLayerForm->ExecStatusBar->BumpProgress();

                  _currentRenderFrame++;
               }
               else {

                  // to next state
                  ToNextRenderState();
               }
            }

         break;

         case RENDER_STATE_OUT_FRAMES:

            // AutoPerShot, AutoPerFrame & ReferenceFrame modes

            if (pauseFlag) {

               pauseFlag = false;
               ExecutePauseSequence(strEndFrame - 1, true);
            }
            else {

               if (_currentRenderFrame < strEndFrame) {

                  int retVal = AlignAndWritebackFrame(_currentRenderFrame,
                                                      renderFlag,
                                                      &pauseFlag);
                  if (retVal == 0) {

                     // show the progress
                     ThreeLayerForm->ExecStatusBar->BumpProgress();

                     // to next frame
                     _currentRenderFrame++;
                  }
               }
               else {

                  // transition to RENDER_STATE_IDLE
                  ToNextRenderState();
               }
            }

         break;
      }
   }

   // Signal thread has ended
   renderState = RENDER_STATE_ENDED;
}

void CThreeLayer::BackgroundAlignRenderCallback(void *vp, void *vr)
{
   CThreeLayer *p = static_cast <CThreeLayer *> (vp);
   p->BackgroundAlignRender(vr);
}
