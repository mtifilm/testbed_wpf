/*
   File Name:  ThreeLayerGUIWin.cpp

   This contains all the toolbar components for the ThreeLayer

*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ThreeLayerGUIWin.h"
#include "ClipAPI.h"
#include "MTIKeyDef.h"
#include "MTIDialogs.h"
#include "PDL.h"
#include "ToolCommand.h"
#include "ToolObject.h"
#include "ToolSystemInterface.h"

#include <stdlib.h>

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MTIUNIT"
//#pragma link "CSPIN"
#pragma link "VTimeCodeEdit"
#pragma link "ExecButtonsFrameUnit"
#pragma link "ExecButtonsFrameUnit"
#pragma link "ExecStatusBarUnit"
#pragma link "SpinEditFrameUnit"
#pragma link "ColorPanel"
#pragma resource "*.dfm"

#define BUTTON_WIDTH 20
TThreeLayerForm *ThreeLayerForm;

#define PLUGIN_TAB_INDEX 4

#define REFR_THRESHOLD 5

//---------------------------------------------------------------------------

bool HandleToolEventTrampoline(const FosterParentEventInfo &eventInfo)
{
    return ThreeLayerForm->HandleToolEvent(eventInfo);
}

//------------------CreateThreeLayerGUI--------------------

  void CreateThreeLayerGUI(void)

// This creates the ThreeLayer GUI if one does not already exist.
// Note: only one can be created.
//
//****************************************************************************
{
   if (ThreeLayerForm != NULL) return;         // Already created

   ThreeLayerForm = new TThreeLayerForm(Application);   // Create it

   ThreeLayerForm->formCreate();

   ThreeLayerForm->RestoreProperties();

   // Reparent the controls to the UniTool
   extern const char *PluginName;
   GThreeLayerTool->getSystemAPI()->AdoptPluginToolGUI(PluginName, PLUGIN_TAB_INDEX,
         reinterpret_cast<int *>( ThreeLayerForm->ThreeLayerControlPanel ),
         &HandleToolEventTrampoline);
}

//-----------------DestroyThreeLayerGUI------------------

  void DestroyThreeLayerGUI(void)

//  This destroys then entire GUI interface
//
//***************************************************************************
{
   if (ThreeLayerForm == NULL) return; // wtf, called twice here at shutdown

   // Reparent the controls back to us before destroying them
   if (GThreeLayerTool != NULL)
   GThreeLayerTool->getSystemAPI()->UnadoptPluginToolGUI(PLUGIN_TAB_INDEX);
   ThreeLayerForm->ThreeLayerControlPanel->Parent = ThreeLayerForm;

   ThreeLayerForm->Free();
   ThreeLayerForm = NULL;
}

//-------------------ShowThreeLayerGUI-------------------

    bool ShowThreeLayerGUI(void)

//  This creates the GUI if not already existing, then shows it
//
//****************************************************************************
{
   CreateThreeLayerGUI();            // Create the gui if necessary

   if (ThreeLayerForm == NULL)
   {
      return false;
   }

   ThreeLayerForm->showForm();

   return true;
}

void TThreeLayerForm::showForm()
{
	ThreeLayerControlPanel->Visible = true;
	ThreeLayerControlPanel->Repaint();
   clearSingleFramePreviewModeFrameIndex();
   SetSpinEditHooks();
   SET_CBHOOK(ClipHasChanged, GThreeLayerTool->ClipChange);
}

//-------------------HideThreeLayerGUI-------------------

    bool  HideThreeLayerGUI(void)

//  This removes the tool from the screen
//
//****************************************************************************
{
   if (ThreeLayerForm == NULL)
   {
      return false;
   }

   ThreeLayerForm->hideForm();

   return true;
}

void TThreeLayerForm::hideForm()
{
   ThreeLayerControlPanel->Visible = false;
   RemoveSpinEditHooks();
   REMOVE_CBHOOK(ClipHasChanged, GThreeLayerTool->ClipChange);
}

//------------------IsToolVisible------------------John Mertus----Aug 2001----

  bool IsToolVisible(void)

//  This returns the visual state of the gui
//
//****************************************************************************
{
    if (ThreeLayerForm == NULL) return(false);         // Not created

    return ThreeLayerForm->ThreeLayerControlPanel->Visible;
}

//---------------------------------------------------------------------------

__fastcall TThreeLayerForm::TThreeLayerForm(TComponent* Owner)
 : TMTIForm(Owner)
{
   ThreeLayerIniFileName = "$(CPMP_USER_DIR)ThreeLayer.ini";
}

//---------------------------------------------------------------------------

void TThreeLayerForm::formCreate()
{
   clearSingleFramePreviewModeFrameIndex();

   redViewer = new Graphics::TBitmap;
   redViewer->PixelFormat = pf32bit;
   redViewer->Width  = RedChannelPaintBox->Width;
   redViewer->Height = RedChannelPaintBox->Height;

   // force indicator repaint
   localRedOffsetX = 100.;
   localRedOffsetY = 100.;
   localRedActive = false;

   blueViewer = new Graphics::TBitmap;
   blueViewer->PixelFormat = pf32bit;
   blueViewer->Width  = BlueChannelPaintBox->Width;
   blueViewer->Height = BlueChannelPaintBox->Height;

   // force indicator repaint
   localBlueOffsetX = 100.;
   localBlueOffsetY = 100.;
   localBlueActive = false;

   drawOffsetRectangle(redViewer, 0, 0, 0, 106, 106, 106);
   drawOffsetRectangle(blueViewer, 0, 0, 0, 106, 106, 106);

   localJogIncrement = 1.0;

   // Initialize spin edits.
   InSpinEdit->SetRange(MINEDTIN, MAXEDTIN);
   InSpinEdit->SetValue(DFLTEDTIN);
   OutSpinEdit->SetRange(MINEDTOUT, MAXEDTOUT);
   OutSpinEdit->SetValue(DFLTEDTOUT);
   AverageSpinEdit->SetRange(MINEDTAVG, 100);
   AverageSpinEdit->SetValue(DFLTEDTAVG);
   RefInSpinEdit->SetRange(MINREFEDTIN, MAXREFEDTIN);
   RefInSpinEdit->SetValue(DFLTREFEDTIN);
   RefOutSpinEdit->SetRange(MINREFEDTOUT, MAXREFEDTOUT);
   RefOutSpinEdit->SetValue(DFLTREFEDTOUT);
   PointsSpinEdit->SetRange(MINPTS_MOV, MAXPTS_MOV);
   PointsSpinEdit->SetValue(DFLTPTS_MOV);
   RangeSpinEdit->SetRange(MINRNG, MAXRNG);
   RangeSpinEdit->SetValue(DFLTRNG);

   // Initialize mat box controls
   procMatSetState = true;
   showMatSetState = true;
	enableMattingControls(AnalysisCBox->Checked, true);

   ExecButtonsToolbar->SetButtonPressedNotifierCallback(ExecButtonsToolbar_ButtonPressedNotifier);
}

void TThreeLayerForm::formDestroy()
{
   delete redViewer;

   delete blueViewer;
}

void TThreeLayerForm::formPaint()
{
}

void TThreeLayerForm::ClipHasChanged(void *Sender)
{
   ExecSetResumeTimecodeToDefault();
}

void TThreeLayerForm::drawRectangle(Graphics::TBitmap *bitmap, RECT *rect,
                                    MTI_UINT8 red, MTI_UINT8 grn, MTI_UINT8 blu)
{
   int hght = bitmap->Height;
   int wdth = bitmap->Width;

   if (rect->top  < 0) rect->top = 0;
   if (rect->bottom > (hght - 1)) rect->bottom = hght - 1;
   if (rect->left < 0) rect->left = 0;
   if (rect->right  > (wdth - 1)) rect->right  = wdth - 1;
   if (rect->right < rect->left) return;

   MTI_UINT32 colr = (((MTI_UINT32)red)<<16) + (((MTI_UINT32)grn)<<8) + blu;

   for (int i = rect->top; i <= rect->bottom; i++) {

      MTI_UINT32 *dst = (MTI_UINT32 *)bitmap->ScanLine[i];
      for (int j=rect->left; j<=rect->right; j++) {

         dst[j] = colr;
      }
   }
}

void TThreeLayerForm::drawOffsetRectangle(Graphics::TBitmap *bitmap,
                                          int margin,
                                          double xoffs, double yoffs,
                                          MTI_UINT8 red, MTI_UINT8 grn, MTI_UINT8 blu)
{
   RECT rect;
   rect.left = margin;
   rect.top  = margin;
   rect.right  = bitmap->Width  - margin - 1;
   rect.bottom = bitmap->Height - margin - 1;

   // snap to nearest tenths
   int xoff = ((int)(xoffs*10.));
   if (xoff > 0) {
      rect.left  += margin/2;
      rect.right += margin/2;
   }
   else if (xoff < 0) {
      rect.left  -= margin/2;
      rect.right -= margin/2;
   }

   // snap to nearest tenths
   int yoff = ((int)(yoffs*10.));
   if (yoff > 0) {
      rect.top    += margin/2;
      rect.bottom += margin/2;
   }
   else if (yoff < 0) {
      rect.top    -= margin/2;
      rect.bottom -= margin/2;
   }

   drawRectangle(bitmap, &rect, red, grn, blu);
}

void TThreeLayerForm::drawIndicatorRectangle(Graphics::TBitmap *bitmap,
                                             bool active)
{
   if (active) {
      drawOffsetRectangle(bitmap, MARGIN, 0, 0, 45, 91, 45);
   }
   else {
      drawOffsetRectangle(bitmap, MARGIN, 0, 0, 91, 91, 91);
   }
}

void TThreeLayerForm::drawOutlineRectangle(Graphics::TBitmap *bitmap,
                                          int margin,
                                          double xoffs, double yoffs,
                                          MTI_UINT8 red, MTI_UINT8 grn, MTI_UINT8 blu)
{
   RECT rect;
   rect.left = margin;
   rect.top  = margin;
   rect.right  = bitmap->Width  - margin - 1;
   rect.bottom = bitmap->Height - margin - 1;

   // snap to nearest tenths
   int xoff = ((int)(xoffs*10.));
   if (xoff > 0) {
      rect.left  += margin/2;
      rect.right += margin/2;
   }
   else if (xoff < 0) {
      rect.left  -= margin/2;
      rect.right -= margin/2;
   }

   // snap to nearest tenths
   int yoff = ((int)(yoffs*10.));
   if (yoff > 0) {
      rect.top    += margin/2;
		rect.bottom += margin/2;
   }
   else if (yoff < 0) {
      rect.top    -= margin/2;
      rect.bottom -= margin/2;
   }

   RECT edge;

   // top edge
   edge.left   = rect.left;
   edge.top    = rect.top;
   edge.right  = rect.right;
   edge.bottom = rect.top + 1;
   drawRectangle(bitmap, &edge, red, grn, blu);

   // left edge
   edge.left   = rect.left;
   edge.top    = rect.top;
   edge.right  = rect.left + 1;
   edge.bottom = rect.bottom;
   drawRectangle(bitmap, &edge, red, grn, blu);

   // right edge
   edge.left   = rect.right - 1;
   edge.top    = rect.top;
   edge.right  = rect.right;
   edge.bottom = rect.bottom;
   drawRectangle(bitmap, &edge, red, grn, blu);

   // bottom edge
   edge.left   = rect.left;
   edge.top    = rect.bottom -1;
   edge.right  = rect.right;
   edge.bottom = rect.bottom;
   drawRectangle(bitmap, &edge, red, grn, blu);
}

bool TThreeLayerForm::HandleToolEvent(const FosterParentEventInfo &eventInfo)
{
   bool retVal = false;  // assume not handled
   WORD Key = eventInfo.Key;
   TShiftState ShiftState;
   int X = eventInfo.X;
   int Y = eventInfo.Y;

   if (eventInfo.Shift)  ShiftState << ssShift;
   if (eventInfo.Alt)    ShiftState << ssAlt;
   if (eventInfo.Ctrl)   ShiftState << ssCtrl;
   if (eventInfo.Left)   ShiftState << ssLeft;
   if (eventInfo.Right)  ShiftState << ssRight;
   if (eventInfo.Middle) ShiftState << ssMiddle;
   if (eventInfo.Double_Clicked)
                         ShiftState << ssDouble;

   switch (eventInfo.Id)
   {
      case FPE_FormPaint:
        formPaint();
         retVal = true;
      break;

      case FPE_FormKeyDown:
         //retVal = formKeyDown(Key, ShiftState);
      break;

      case FPE_FormKeyUp:
         //retVal = formKeyUp(Key, ShiftState);
      break;

      case FPE_FormMouseMove:
         //formMouseMove(ShiftState, X, Y);
         retVal = true;
      break;

      default:
         // Do nothing
      break;
   }

   return retVal;
}

//---------------------------------------------------------------------------
void __fastcall TThreeLayerForm::DummyEditFormKeyDown(TObject *Sender,
      WORD &Key, TShiftState Shift)
{
   MTIKeyEventHandler keyEventHandler =
      GThreeLayerTool->getSystemAPI()->getKeyDownEventHandler();
   keyEventHandler(Sender, Key, Shift);

   Key = 0;
}

//---------------------------------------------------------------------------
void __fastcall TThreeLayerForm::DummyEditFormKeyUp(TObject *Sender,
      WORD &Key, TShiftState Shift)
{
   MTIKeyEventHandler keyEventHandler =
      GThreeLayerTool->getSystemAPI()->getKeyUpEventHandler();
   keyEventHandler(Sender, Key, Shift);

   Key = 0;
}
//---------------------------------------------------------------------------
#if APPEARS_TO_NOT_BE_USED
void TThreeLayerForm::enableManualOffsetsPanel(bool enbl)
{
   ManualOffsetsGroupBox->Enabled     = enbl;

   RedXLabel->Enabled                 = enbl;
   RedYLabel->Enabled                 = enbl;
   WarpLabel->Enabled                 = enbl;
   BluXLabel->Enabled                 = enbl;
   BluYLabel->Enabled                 = enbl;
   RedOffsetXPanel->Enabled           = enbl;
   RedOffsetYPanel->Enabled           = enbl;
   WarpPanel->Enabled                 = enbl;
   BlueOffsetXPanel->Enabled          = enbl;
   BlueOffsetYPanel->Enabled          = enbl;

   RedChannelPaintBox->Enabled        = enbl;
   GangCheckBox->Enabled              = enbl;
   GangLabel->Enabled                 = enbl;
   BlueChannelPaintBox->Enabled       = enbl;

   UpSpeedButton->Enabled             = enbl;
   LeftSpeedButton->Enabled           = enbl;
   AlignResetSpeedButton->Enabled          = enbl;
   RightSpeedButton->Enabled          = enbl;
   DownSpeedButton->Enabled           = enbl;

   RedResetButton->Enabled            = enbl;
   MasterResetButton->Enabled         = enbl;
   BlueResetButton->Enabled           = enbl;
}
#endif

//---------------------------------------------------------------------------
void TThreeLayerForm::setMajorState(int newstate)
{
   switch(newstate) {

   case MAJORSTATE_AUTO_PER_SHOT_HOME: {
         InLabel->Enabled                   = true;
         OutLabel->Enabled                  = true;
         AutoFramesLabel->Enabled           = true;
         AverageLabel->Enabled              = true;
         InSpinEdit->Enable(true);
         OutSpinEdit->Enable(true);
         AverageSpinEdit->Enable(true);

         RefInLabel->Enabled                   = false;
         RefOutLabel->Enabled                  = false;
         RefAutoFramesLabel->Enabled           = false;
         ReferenceLabel->Enabled               = false;
         RefInSpinEdit->Enable(false);
         RefOutSpinEdit->Enable(false);

         ReferenceFrameGoToSpeedButton->Enabled     = false;
         ReferenceFrameTCEdit->Enabled              = false;
         ReferenceFrameSetStartSpeedButton->Enabled = false;

         BaseSettingsLabel->Enabled = true;
         RedBXLabel->Enabled        = true;
         RedBYLabel->Enabled        = true;
         BWarpLabel->Enabled        = true;
         BluBXLabel->Enabled        = true;
         BluBYLabel->Enabled        = true;
         RedBaseXLabel->Enabled     = true;
         RedBaseYLabel->Enabled     = true;
         WarpBaseLabel->Enabled     = true;
         BluBaseXLabel->Enabled     = true;
         BluBaseYLabel->Enabled     = true;

         RedOffsetXPanel->Enabled  = true;
         RedOffsetYPanel->Enabled  = true;
         BlueOffsetXPanel->Enabled = true;
         BlueOffsetYPanel->Enabled = true;

         EnableRedIndicator(false);
         GangCheckBox->OnClick = NULL;
         GangCheckBox->Checked = false;
         GangCheckBox->OnClick = GangCheckBoxClick;
         GangCheckBox->Enabled = true;
         GangLabel->Enabled    = true;
         EnableBlueIndicator(false);

         LeftSpeedButton->Enabled  = false;
         RightSpeedButton->Enabled = false;
         UpSpeedButton->Enabled    = false;
         DownSpeedButton->Enabled  = false;
         AlignResetSpeedButton->Enabled = false;

         MasterResetButton->Enabled = true;
         RedResetButton->Enabled    = true;
         BlueResetButton->Enabled   = true;

         setGUIRunning(false);
      } break;

   case MAJORSTATE_AUTO_PER_SHOT_EDIT_IN: {
      } break;

   case MAJORSTATE_AUTO_PER_SHOT_EDIT_OUT: {
      } break;

   case MAJORSTATE_AUTO_PER_SHOT_EDIT_AVG: {
      } break;

   case MAJORSTATE_AUTO_PER_FRAME_HOME: {
         InLabel->Enabled                   = false;
         OutLabel->Enabled                  = false;
         AutoFramesLabel->Enabled           = false;
         AverageLabel->Enabled              = false;
         InSpinEdit->Enable(false);
         OutSpinEdit->Enable(false);
         AverageSpinEdit->Enable(false);

         RefInLabel->Enabled                   = false;
         RefOutLabel->Enabled                  = false;
         RefAutoFramesLabel->Enabled           = false;
         ReferenceLabel->Enabled               = false;
         RefInSpinEdit->Enable(false);
         RefOutSpinEdit->Enable(false);

         ReferenceFrameGoToSpeedButton->Enabled     = false;
         ReferenceFrameTCEdit->Enabled              = false;
         ReferenceFrameSetStartSpeedButton->Enabled = false;

         BaseSettingsLabel->Enabled = true;
         RedBXLabel->Enabled        = true;
         RedBYLabel->Enabled        = true;
         BWarpLabel->Enabled        = true;
         BluBXLabel->Enabled        = true;
         BluBYLabel->Enabled        = true;
         RedBaseXLabel->Enabled     = true;
         RedBaseYLabel->Enabled     = true;
         WarpBaseLabel->Enabled     = true;
         BluBaseXLabel->Enabled     = true;
         BluBaseYLabel->Enabled     = true;

         RedOffsetXPanel->Enabled  = true;
         RedOffsetYPanel->Enabled  = true;
         BlueOffsetXPanel->Enabled = true;
         BlueOffsetYPanel->Enabled = true;

         EnableRedIndicator(false);
         GangCheckBox->OnClick = NULL;
         GangCheckBox->Checked = false;
         GangCheckBox->OnClick = GangCheckBoxClick;
         GangCheckBox->Enabled = true;
         GangLabel->Enabled    = true;
         EnableBlueIndicator(false);

         LeftSpeedButton->Enabled  = false;
         RightSpeedButton->Enabled = false;
         UpSpeedButton->Enabled    = false;
         DownSpeedButton->Enabled  = false;
         AlignResetSpeedButton->Enabled = true;

         MasterResetButton->Enabled = true;
         RedResetButton->Enabled    = true;
         BlueResetButton->Enabled   = true;

         setGUIRunning(false);
      } break;

   case MAJORSTATE_MANUAL_HOME: {
         InLabel->Enabled                   = false;
         OutLabel->Enabled                  = false;
         AutoFramesLabel->Enabled           = false;
         AverageLabel->Enabled              = false;
         InSpinEdit->Enable(false);
         OutSpinEdit->Enable(false);
         AverageSpinEdit->Enable(false);

         RefInLabel->Enabled                   = false;
         RefOutLabel->Enabled                  = false;
         RefAutoFramesLabel->Enabled           = false;
         ReferenceLabel->Enabled               = false;
         RefInSpinEdit->Enable(false);
         RefOutSpinEdit->Enable(false);

         ReferenceFrameGoToSpeedButton->Enabled     = false;
         ReferenceFrameTCEdit->Enabled              = false;
         ReferenceFrameSetStartSpeedButton->Enabled = false;

         BaseSettingsLabel->Enabled = false;
         RedBXLabel->Enabled        = false;
         RedBYLabel->Enabled        = false;
         BWarpLabel->Enabled        = false;
         BluBXLabel->Enabled        = false;
         BluBYLabel->Enabled        = false;
         RedBaseXLabel->Enabled     = false;
         RedBaseYLabel->Enabled     = false;
         WarpBaseLabel->Enabled     = false;
         BluBaseXLabel->Enabled     = false;
         BluBaseYLabel->Enabled     = false;

         RedOffsetXPanel->Enabled  = false;
         RedOffsetYPanel->Enabled  = false;
         BlueOffsetXPanel->Enabled = false;
         BlueOffsetYPanel->Enabled = false;

         EnableRedIndicator(false);
         GangCheckBox->OnClick = NULL;
         GangCheckBox->Checked = false;
         GangCheckBox->OnClick = GangCheckBoxClick;
         GangCheckBox->Enabled = true;
         GangLabel->Enabled    = true;
         EnableBlueIndicator(false);

         LeftSpeedButton->Enabled  = false;
         RightSpeedButton->Enabled = false;
         UpSpeedButton->Enabled    = false;
         DownSpeedButton->Enabled  = false;
         AlignResetSpeedButton->Enabled = false;

         MasterResetButton->Enabled = true;
         RedResetButton->Enabled    = true;
         BlueResetButton->Enabled   = true;

         setGUIRunning(false);
      } break;

   case MAJORSTATE_REFERENCE_FRAME_HOME: {
         InLabel->Enabled                   = false;
         OutLabel->Enabled                  = false;
         AutoFramesLabel->Enabled           = false;
         AverageLabel->Enabled              = false;
         InSpinEdit->Enable(false);
         OutSpinEdit->Enable(false);
         AverageSpinEdit->Enable(false);

         RefInLabel->Enabled                   = true;
         RefOutLabel->Enabled                  = true;
         RefAutoFramesLabel->Enabled           = true;
         ReferenceLabel->Enabled               = true;
         RefInSpinEdit->Enable(true);
         RefOutSpinEdit->Enable(true);

         ReferenceFrameGoToSpeedButton->Enabled     = true;
         ReferenceFrameTCEdit->Enabled              = true;
         ReferenceFrameSetStartSpeedButton->Enabled = true;

         BaseSettingsLabel->Enabled = true;
         RedBXLabel->Enabled        = true;
         RedBYLabel->Enabled        = true;
         BWarpLabel->Enabled        = true;
         BluBXLabel->Enabled        = true;
         BluBYLabel->Enabled        = true;
         RedBaseXLabel->Enabled     = true;
         RedBaseYLabel->Enabled     = true;
         WarpBaseLabel->Enabled     = true;
         BluBaseXLabel->Enabled     = true;
         BluBaseYLabel->Enabled     = true;

         RedOffsetXPanel->Enabled  = true;
         RedOffsetYPanel->Enabled  = true;
         BlueOffsetXPanel->Enabled = true;
         BlueOffsetYPanel->Enabled = true;

         EnableRedIndicator(false);
         GangCheckBox->OnClick = NULL;
         GangCheckBox->Checked = false;
         GangCheckBox->OnClick = GangCheckBoxClick;
         GangLabel->Enabled    = true;
         GangCheckBox->Enabled = true;
         EnableBlueIndicator(false);

         LeftSpeedButton->Enabled  = false;
         RightSpeedButton->Enabled = false;
         UpSpeedButton->Enabled    = false;
         DownSpeedButton->Enabled  = false;
         AlignResetSpeedButton->Enabled = false;

         MasterResetButton->Enabled = true;
         RedResetButton->Enabled    = true;
         BlueResetButton->Enabled   = true;

         setGUIRunning(false);
      } break;

   case MAJORSTATE_REFERENCE_FRAME_EDIT_IN: {
      }

      break;

   case MAJORSTATE_REFERENCE_FRAME_EDIT_OUT: {
      } break;

   case MAJORSTATE_REFERENCE_FRAME_EDIT_REF: {
      } break;

   case MAJORSTATE_MANUAL_RED: {
         GangLabel->Enabled    = true;
         GangCheckBox->Enabled = true;
         GangCheckBox->OnClick = NULL;
         GangCheckBox->Checked = false;
         GangCheckBox->OnClick = GangCheckBoxClick;

         EnableBlueIndicator(false);
         EnableRedIndicator(true);

         LeftSpeedButton->Enabled  = true;
         RightSpeedButton->Enabled = true;
         UpSpeedButton->Enabled    = true;
         DownSpeedButton->Enabled  = true;
         AlignResetSpeedButton->Enabled = true;

         MasterResetButton->Enabled = true;
         RedResetButton->Enabled    = true;
         BlueResetButton->Enabled   = true;

         RedOffsetXPanel->Enabled  = true;
         RedOffsetYPanel->Enabled  = true;
         BlueOffsetXPanel->Enabled = false;
         BlueOffsetYPanel->Enabled = false;

         setGUIRunning(false);
      } break;

   case MAJORSTATE_MANUAL_BLUE: {
         GangLabel->Enabled    = true;
         GangCheckBox->Enabled = true;
         GangCheckBox->OnClick = NULL;
         GangCheckBox->Checked = false;
         GangCheckBox->OnClick = GangCheckBoxClick;

         EnableRedIndicator(false);
         EnableBlueIndicator(true);

         LeftSpeedButton->Enabled  = true;
         RightSpeedButton->Enabled = true;
         UpSpeedButton->Enabled    = true;
         DownSpeedButton->Enabled  = true;
         AlignResetSpeedButton->Enabled = true;

         MasterResetButton->Enabled = true;
         RedResetButton->Enabled    = true;
         BlueResetButton->Enabled   = true;

         RedOffsetXPanel->Enabled  = false;
         RedOffsetYPanel->Enabled  = false;
         BlueOffsetXPanel->Enabled = true;
         BlueOffsetYPanel->Enabled = true;

         setGUIRunning(false);
      } break;

   case MAJORSTATE_MANUAL_GANGED: {
         GangLabel->Enabled    = true;
         GangCheckBox->Enabled = true;
         GangCheckBox->OnClick = NULL;
         GangCheckBox->Checked = true;
         GangCheckBox->OnClick = GangCheckBoxClick;

         EnableRedIndicator(true);
         EnableBlueIndicator(true);

         LeftSpeedButton->Enabled  = true;
         RightSpeedButton->Enabled = true;
         UpSpeedButton->Enabled    = true;
         DownSpeedButton->Enabled  = true;
         AlignResetSpeedButton->Enabled = true;

         MasterResetButton->Enabled = true;
         RedResetButton->Enabled    = true;
         BlueResetButton->Enabled   = true;

         RedOffsetXPanel->Enabled  = true;
         RedOffsetYPanel->Enabled  = true;
         BlueOffsetXPanel->Enabled = true;
         BlueOffsetYPanel->Enabled = true;

         setGUIRunning(false);
      }

      break;

      default:

      break;
   }

   setSingleFramePreviewModeFrameIndex();
}

void TThreeLayerForm::setGUIRunning(bool run)
{
   if (run) {

      AnalysisCBox->Enabled = false;
      enableMattingControls(false, false);

      MoveRadioButton->Enabled     = false;
      MoveWarpRadioButton->Enabled = false;

      PointsLabel->Enabled    = false;
      RangeLabel->Enabled     = false;
      PointsSpinEdit->Enable(false);
      RangeSpinEdit->Enable(false);

      GangCheckBox->Enabled     = false;
      GangLabel->Enabled        = false;

      LeftSpeedButton->Enabled  = false;
      RightSpeedButton->Enabled = false;
      UpSpeedButton->Enabled    = false;
      DownSpeedButton->Enabled  = false;
      AlignResetSpeedButton->Enabled = false;

      MasterResetButton->Enabled = false;
      RedResetButton->Enabled    = false;
      RedOffsetXPanel->Enabled   = false;
      RedOffsetYPanel->Enabled   = false;
      BlueResetButton->Enabled   = false;
      BlueOffsetXPanel->Enabled  = false;
      BlueOffsetYPanel->Enabled  = false;

      ShowOriginalSpeedButton->Enabled  = false;
      ShowProcessedSpeedButton->Enabled = false;

		setGUIExecButtons(false);

		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->EnableButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->EnableButton(EXEC_BUTTON_STOP);
	}
   else {

      AnalysisCBox->Enabled = true;
      enableMattingControls(AnalysisCBox->Checked, true);

      MoveRadioButton->Enabled     = true;
      MoveWarpRadioButton->Enabled = true;

      PointsLabel->Enabled    = true;
      RangeLabel->Enabled     = true;
      PointsSpinEdit->Enable(true);
      RangeSpinEdit->Enable(true);

      GangCheckBox->Enabled     = true;
      GangLabel->Enabled        = true;

      LeftSpeedButton->Enabled  = true;
      RightSpeedButton->Enabled = true;
      UpSpeedButton->Enabled    = true;
      DownSpeedButton->Enabled  = true;
      AlignResetSpeedButton->Enabled = true;

      MasterResetButton->Enabled = true;
      RedResetButton->Enabled    = true;
      RedOffsetXPanel->Enabled   = true;
      RedOffsetYPanel->Enabled   = true;
      BlueResetButton->Enabled   = true;
      BlueOffsetXPanel->Enabled  = true;
      BlueOffsetYPanel->Enabled  = true;

      ShowOriginalSpeedButton->Enabled  = true;
      ShowProcessedSpeedButton->Enabled = true;

      setGUIExecButtons(true);

		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_STOP);
	}

   clearSingleFramePreviewModeFrameIndex();
}

void TThreeLayerForm::setGUIRunningThru()
{
   {
      enableMattingControls(false, false);

      MoveRadioButton->Enabled     = false;
      MoveWarpRadioButton->Enabled = false;

      PointsLabel->Enabled    = false;
      RangeLabel->Enabled     = false;
      PointsSpinEdit->Enable(false);
      RangeSpinEdit->Enable(false);

      GangCheckBox->Enabled     = false;
      GangLabel->Enabled        = false;

      LeftSpeedButton->Enabled  = false;
      RightSpeedButton->Enabled = false;
      UpSpeedButton->Enabled    = false;
      DownSpeedButton->Enabled  = false;
      AlignResetSpeedButton->Enabled = false;

      MasterResetButton->Enabled = false;
      RedResetButton->Enabled    = false;
      RedOffsetXPanel->Enabled   = false;
      RedOffsetYPanel->Enabled   = false;
      BlueResetButton->Enabled   = false;
      BlueOffsetXPanel->Enabled  = false;
      BlueOffsetYPanel->Enabled  = false;

      ShowOriginalSpeedButton->Enabled  = false;
      ShowProcessedSpeedButton->Enabled = false;

      setGUIExecButtons(false);

		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_STOP);
	}

   clearSingleFramePreviewModeFrameIndex();
}

void TThreeLayerForm::setGUIPause(bool paused)
{
	if (paused) { // paused

		ExecButtonsToolbar->EnableButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->ShowButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->HideButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->EnableButton(EXEC_BUTTON_STOP);
	}
	else { // un-paused

		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->EnableButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->EnableButton(EXEC_BUTTON_STOP);
	}
}

void TThreeLayerForm::setGUIExecButtons(bool enbl)
{
   if (enbl) {

		bool zed = GThreeLayerTool->ManualOffsetsZeroed();
      bool mks = GThreeLayerTool->MarksAreValid();

		ExecButtonsToolbar->EnableButton(EXEC_BUTTON_PREVIEW_FRAME);
		ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_FRAME, "Preview Frame (D)");
		if (!zed)
		{
			ExecButtonsToolbar->EnableButton(EXEC_BUTTON_RENDER_FRAME);
			ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_FRAME, "Render Frame (G)");
		}
		else
		{
			ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
			ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_FRAME, "You must set offsets before rendering a frame");
		}

		if (!zed && mks)
		{
			ExecButtonsToolbar->EnableButton(EXEC_BUTTON_PREVIEW_ALL);
			ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_ALL, "Preview all (Shift+D)");
			ExecButtonsToolbar->EnableButton(EXEC_BUTTON_RENDER_ALL);
			ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL, "Render all (Shift+G)");
			ExecButtonsToolbar->EnableButton(EXEC_BUTTON_CAPTURE_PDL);
			ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL, "Capture PDL entry (, key)");
		}
		else
		{
			ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
			ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_ALL,
															mks ? "You must set offsets before previewing all"
																 : "You must set marks before previewing all");
			ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
			ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL,
															mks ? "You must set offsets before rendering all"
																 : "You must set marks before rendering all");
			ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);
			ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL,
															mks ? "You must set offsets before capturing a PDL entry"
																 : "You must set marks before capturing a PDL entry");
		}
	}
   else {

			ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
			ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_FRAME, "Preview Frame is not available");
			ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
			ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_FRAME, "Render Frame is not available");
			ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
			ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_ALL, "Preview All is not available");
			ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
			ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL, "Render All is not available");
			ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);
			ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL, "Can't capture a PDL entry now");
	}
}

//---------------------------------------------------------------------------

void TThreeLayerForm::enableMattingControls(bool enbl, bool unchk)
{
   bool enabledState = enbl && AnalysisCBox->Checked;
   bool uncheckBoxes = unchk && !AnalysisCBox->Checked;
   bool newProcCheckedState = enabledState
                              ? procMatSetState
                              : (uncheckBoxes
                                 ? false
                                 : ProcessingCBox->Checked);
   bool newShowCheckedState = enabledState
                              ? showMatSetState
                              : (uncheckBoxes
                                 ? false
                                 : ShowMattingCBox->Checked);

   ProcessingCBox->Enabled  = enabledState;
   ShowMattingCBox->Enabled = enabledState;

   ProcessingCBox->OnClick  = NULL;
   ProcessingCBox->Checked  = newProcCheckedState;
   ProcessingCBox->OnClick  = ProcessingCBoxClick;

   ShowMattingCBox->OnClick = NULL;
   ShowMattingCBox->Checked = newShowCheckedState;
   ShowMattingCBox->OnClick = ShowMattingCBoxClick;

   TopUpButton->Enabled         = enabledState;
   TopDownButton->Enabled       = enabledState;
   LeftLeftButton->Enabled      = enabledState;
   LeftRightButton->Enabled     = enabledState;
   RightLeftButton->Enabled     = enabledState;
   RightRightButton->Enabled    = enabledState;
   BottomUpButton->Enabled      = enabledState;
   BottomDownButton->Enabled    = enabledState;
   ColorPickButton->Enabled     = enabledState;
   AllMatButton->Enabled        = enabledState;
}

//---------------------------------------------------------------------------

void TThreeLayerForm::SetJogIncrement(double jogincr)
{
   localJogIncrement = jogincr;
}

void TThreeLayerForm::UpdateColorPickButton(bool dn)
{
   ColorPickButton->OnClick = NULL;
	ColorPickButton->Down    = dn;
   ColorPickButton->OnClick = ColorPickButtonClick;
   ColorPickButton->Repaint();
}


void TThreeLayerForm::UpdateRedBaseX(double xoffs)
{
   localRedBaseX = xoffs;

   if (xoffs == 0) {
      RedBaseXLabel->Color = clBtnFace;
   }
   else {
      RedBaseXLabel->Color = clYellow;
   }

   char str[16];
   sprintf(str, "%+5.2f ", xoffs);
   RedBaseXLabel->Caption = str;

   RedBaseXLabel->Repaint();

   setSingleFramePreviewModeFrameIndex();
}

void TThreeLayerForm::UpdateRedBaseY(double yoffs)
{
   localRedBaseY = yoffs;

   if (yoffs == 0) {
      RedBaseYLabel->Color = clBtnFace;
   }
   else {
      RedBaseYLabel->Color = clYellow;
   }

   char str[16];
   sprintf(str, "%+5.2f ", yoffs);
   RedBaseYLabel->Caption = str;

   RedBaseYLabel->Repaint();

   setSingleFramePreviewModeFrameIndex();
}

void TThreeLayerForm::UpdateRedOffsetX(double xoffs)
{
   int curxoff = (int)(10.*localRedOffsetX);
   int newxoff = (int)(10.*xoffs);

   localRedOffsetX = xoffs;

   if (newxoff != 0) {
      RedOffsetXPanel->Color = clYellow;
   }
   else {
      RedOffsetXPanel->Color = clBtnFace;
   }

   char str[16];
   sprintf(str, "%+5.2f", xoffs);
   RedOffsetXPanel->Caption = str;

   RedOffsetXPanel->Repaint();

   // now decide if indicator box needs repainting
   if ((newxoff <  0)&&(curxoff < 0))   return;
   if ((newxoff == 0)&&(curxoff == 0))  return;
   if ((newxoff >  0)&&(curxoff > 0))   return;

   drawOffsetRectangle(redViewer, 0, 0, 0, 106, 106, 106);

   drawIndicatorRectangle(redViewer, localRedActive);

   drawOutlineRectangle(redViewer, MARGIN, localRedOffsetX, localRedOffsetY, 255,   0,   0);

   RedChannelPaintBox->Repaint();

   setSingleFramePreviewModeFrameIndex();
}

void TThreeLayerForm::UpdateRedOffsetY(double yoffs)
{
   int curyoff = (int)(10.*localRedOffsetY);
   int newyoff = (int)(10.*yoffs);

   localRedOffsetY = yoffs;

   if (newyoff != 0) {
      RedOffsetYPanel->Color = clYellow;
   }
   else {
      RedOffsetYPanel->Color = clBtnFace;
   }

   char str[16];
   sprintf(str, "%+5.2f", yoffs);
   RedOffsetYPanel->Caption = str;

   RedOffsetYPanel->Repaint();

   // now decide if indicator box needs repainting
   if ((newyoff <  0)&&(curyoff < 0))   return;
   if ((newyoff == 0)&&(curyoff == 0))  return;
   if ((newyoff >  0)&&(curyoff > 0))   return;

   drawOffsetRectangle(redViewer, 0, 0, 0, 106, 106, 106);

   drawIndicatorRectangle(redViewer, localRedActive);

   drawOutlineRectangle(redViewer, MARGIN, localRedOffsetX, localRedOffsetY, 255,   0,   0);

   RedChannelPaintBox->Repaint();

   setSingleFramePreviewModeFrameIndex();
}

void TThreeLayerForm::EnableRedIndicator(bool enbl)
{
   if (enbl != localRedActive) { // change
      localRedActive = enbl;

      drawIndicatorRectangle(redViewer, localRedActive);

      drawOutlineRectangle(redViewer, MARGIN, localRedOffsetX, localRedOffsetY, 255,   0,   0);

      RedChannelPaintBox->Repaint();
   }

   setSingleFramePreviewModeFrameIndex();
}

void TThreeLayerForm::UpdateBaseWarp(double warp)
{
   localBaseWarp = warp;

   if (warp == 0) {
      WarpBaseLabel->Color = clBtnFace;
   }
   else {
      WarpBaseLabel->Color = clYellow;
   }

   char str[16];
   sprintf(str, "%+5.2f ", warp);
   WarpBaseLabel->Caption = str;

   WarpBaseLabel->Repaint();

   setSingleFramePreviewModeFrameIndex();
}

void TThreeLayerForm::UpdateWarp(double warp)
{
   localWarp = warp;

   char str[16];
   sprintf(str, "%+5.2f", warp);
   WarpPanel->Caption = str;

   WarpPanel->Repaint();

   setSingleFramePreviewModeFrameIndex();
}

void TThreeLayerForm::UpdateBlueBaseX(double xoffs)
{
   localBlueBaseX = xoffs;

   if (xoffs == 0) {
      BluBaseXLabel->Color = clBtnFace;
   }
   else {
      BluBaseXLabel->Color = clYellow;
   }

   char str[16];
   sprintf(str, " %+5.2f ", xoffs);
   BluBaseXLabel->Caption = str;

   BluBaseXLabel->Repaint();

   setSingleFramePreviewModeFrameIndex();
}

void TThreeLayerForm::UpdateBlueBaseY(double yoffs)
{
   localBlueBaseY = yoffs;

   if (yoffs == 0) {
      BluBaseYLabel->Color = clBtnFace;
   }
   else {
      BluBaseYLabel->Color = clYellow;
   }

   char str[16];
   sprintf(str, "%+5.2f ", yoffs);
   BluBaseYLabel->Caption = str;

   BluBaseYLabel->Repaint();

   setSingleFramePreviewModeFrameIndex();
}

void TThreeLayerForm::UpdateBlueOffsetX(double xoffs)
{
   int curxoff = (int)(10.*localBlueOffsetX);
   int newxoff = (int)(10.*xoffs);

   localBlueOffsetX = xoffs;

   if (newxoff != 0) {
      BlueOffsetXPanel->Color = clYellow;
   }
   else {
      BlueOffsetXPanel->Color = clBtnFace;
   }

   char str[16];
   sprintf(str, "%+5.2f", xoffs);
   BlueOffsetXPanel->Caption = str;

   BlueOffsetXPanel->Repaint();

   // now decide if indicator box needs repainting
   if ((newxoff <  0)&&(curxoff < 0))   return;
   if ((newxoff == 0)&&(curxoff == 0))  return;
   if ((newxoff >  0)&&(curxoff > 0))   return;

   drawOffsetRectangle(blueViewer, 0, 0, 0, 106, 106, 106);

   drawIndicatorRectangle(blueViewer, localBlueActive);

   drawOutlineRectangle(blueViewer, MARGIN, localBlueOffsetX, localBlueOffsetY, 0,   0, 255);

   BlueChannelPaintBox->Repaint();

   setSingleFramePreviewModeFrameIndex();
}

void TThreeLayerForm::UpdateBlueOffsetY(double yoffs)
{
   int curyoff = (int)(10.*localBlueOffsetY);
   int newyoff = (int)(10.*yoffs);

   localBlueOffsetY = yoffs;

   if (newyoff != 0) {
      BlueOffsetYPanel->Color = clYellow;
   }
   else {
      BlueOffsetYPanel->Color = clBtnFace;
   }

   char str[16];
   sprintf(str, "%+5.2f", yoffs);
   BlueOffsetYPanel->Caption = str;

   BlueOffsetYPanel->Repaint();

   // now decide if indicator box needs repainting
   if ((newyoff <  0)&&(curyoff < 0))   return;
   if ((newyoff == 0)&&(curyoff == 0))  return;
   if ((newyoff >  0)&&(curyoff > 0))   return;

   drawOffsetRectangle(blueViewer, 0, 0, 0, 106, 106, 106);

   drawIndicatorRectangle(blueViewer, localBlueActive);

   drawOutlineRectangle(blueViewer, MARGIN, localBlueOffsetX, localBlueOffsetY, 0,   0, 255);

   BlueChannelPaintBox->Repaint();

   setSingleFramePreviewModeFrameIndex();
}

void TThreeLayerForm::EnableBlueIndicator(bool enbl)
{
   if (enbl != localBlueActive) { // change
      localBlueActive = enbl;

      drawIndicatorRectangle(blueViewer, localBlueActive);

      drawOutlineRectangle(blueViewer, MARGIN, localBlueOffsetX, localBlueOffsetY, 0,   0, 255);

      BlueChannelPaintBox->Repaint();
   }

   setSingleFramePreviewModeFrameIndex();
}

void TThreeLayerForm::Error1Show()
{
   _MTIErrorDialog(Handle, "Operation requires legal In and Out Marks");

   DummyEdit->SetFocus();
}

void TThreeLayerForm::Error2Show()
{
   _MTIErrorDialog(Handle, "Operation requires Current Frame be between legal In and Out Marks");

   DummyEdit->SetFocus();
}

void TThreeLayerForm::Error3Show()
{
   _MTIErrorDialog(Handle, "Operation requires Reference Frame be between legal In and Out Marks");

   DummyEdit->SetFocus();
}

void TThreeLayerForm::Error4Show()
{
   _MTIErrorDialog(Handle, "Operation requires offsets or warp be non-zero");

   DummyEdit->SetFocus();
}

void TThreeLayerForm::UpdateOperationMode(int mode)
{
   localOpMode = mode;

   bool autoperfrm  = false;
   bool autopershot = false;
   bool manual      = false;
   bool reffrm      = false;

   switch(mode) {

      case OPERATING_MODE_AUTO_PER_SHOT:

         autopershot = true;

      break;

      case OPERATING_MODE_AUTO_PER_FRAME:

         autoperfrm = true;

      break;

      case OPERATING_MODE_MANUAL:

         manual = true;

      break;

      case OPERATING_MODE_REFERENCE_FRAME:

         reffrm = true;

      break;

      default:
      break;

   }

   AutoPerFrameRadioButton->OnEnter  = NULL;
   AutoPerFrameRadioButton->OnClick  = NULL;
   AutoPerFrameRadioButton->Checked  = autoperfrm;
   AutoPerFrameRadioButton->OnEnter  = AutoPerFrameRadioButtonOnEnter;
   AutoPerFrameRadioButton->OnClick  = AutoPerFrameRadioButtonOnEnter;

   AutoPerShotRadioButton->OnEnter   = NULL;
   AutoPerShotRadioButton->OnClick   = NULL;
   AutoPerShotRadioButton->Checked   = autopershot;
   AutoPerShotRadioButton->OnEnter   = AutoPerShotRadioButtonOnEnter;
   AutoPerShotRadioButton->OnClick   = AutoPerShotRadioButtonOnEnter;

   ManualRadioButton->OnEnter        = NULL;
   ManualRadioButton->OnClick        = NULL;
   ManualRadioButton->Checked        = manual;
   ManualRadioButton->OnEnter        = ManualRadioButtonOnEnter;
   ManualRadioButton->OnClick        = ManualRadioButtonOnEnter;

   ReferenceFrameRadioButton->OnEnter = NULL;
   ReferenceFrameRadioButton->OnClick = NULL;
   ReferenceFrameRadioButton->Checked = reffrm;
   ReferenceFrameRadioButton->OnEnter = ReferenceFrameRadioButtonOnEnter;
   ReferenceFrameRadioButton->OnClick = ReferenceFrameRadioButtonOnEnter;

   clearSingleFramePreviewModeFrameIndex();
}

int TThreeLayerForm::GetOperationMode()
{
   return localOpMode;
}
//---------------------------------------------------------------------------

bool TThreeLayerForm::WriteSettings(CIniFile *ini, const string &IniSection)

//  This is the usual save, however, the ini file points to the calling program
//  and we want it to go to the ThreeLayer.ini file in .cpmprc
//  So we just open up a new one
//
//******************************************************************************
{
  if (ThreeLayerIniFileName == "") return(false);
  CIniFile* iniAF = CreateIniFile(ThreeLayerIniFileName);
  if (iniAF == NULL)
    {
      TRACE_0(errout << "SettingsProperties: Could not create "
              << ThreeLayerIniFileName << endl
              << "Because " << theError.getMessage());
      return(false);
    }

  return DeleteIniFile(iniAF);
}

//----------------------------------------------------------------------------

bool TThreeLayerForm::ReadSettings(CIniFile *ini, const string &IniSection)

//  This is the usual restore, however, the ini file points to the calling program
//  and we want it to go to the ThreeLayer.ini file in .cpmprc
//
//******************************************************************************
{
  if (ThreeLayerIniFileName == "") return(false);
  CIniFile* iniAF = CreateIniFile(ThreeLayerIniFileName);
  if (iniAF == NULL)
    {
      TRACE_0(errout << "SettingsProperties: Could not create "
              << ThreeLayerIniFileName << endl
              << "Because " << theError.getMessage());
      return(false);
    }

  return DeleteIniFile(iniAF);
}

//---------------------------------------------------------------------------

void TThreeLayerForm::setIdleCursor(void)
{
   Screen->Cursor = crDefault;
}
//---------------------------------------------------------------------------

void TThreeLayerForm::setBusyCursor(void)
{
   Screen->Cursor = crAppStart;
}

//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::RedPaintBoxPaint(TObject *Sender)
{
   TRect rect_red;
   rect_red.left   = 0;
   rect_red.top    = 0;
   rect_red.right  = redViewer->Width;
   rect_red.bottom = redViewer->Height;

   RedChannelPaintBox->Canvas->StretchDraw (rect_red, redViewer);
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::BluePaintBoxPaint(TObject *Sender)
{
   TRect rect_blue;
   rect_blue.left   = 0;
   rect_blue.top    = 0;
   rect_blue.right  = blueViewer->Width;
   rect_blue.bottom = blueViewer->Height;

   BlueChannelPaintBox->Canvas->StretchDraw (rect_blue, blueViewer);
}

//---------------------------------------------------------------------------

//////////////////////  MANUAL REGISTRATION CONTROLS  ///////////////////////

//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::MasterResetButtonClick(TObject *Sender)
{
   clearSingleFramePreviewModeFrameIndex();
   GThreeLayerTool->ResetRedAndBlueOffsets();
   GThreeLayerTool->ShowProcessedChanged(false);

   DummyEdit->SetFocus();
}
//---------------------------------------------------------------------------

static bool foo = false;
void __fastcall TThreeLayerForm::RedResetButtonClick(TObject *Sender)
{
	ExecButtonsToolbar->ToolbarPanel->Visible = true;
	ExecButtonsToolbar->Visible = true;
   setSingleFramePreviewModeFrameIndex();
   GThreeLayerTool->ResetRedOffsets(true, true);

   DummyEdit->SetFocus();
}

void __fastcall TThreeLayerForm::RedOffsetXPanelClick(TObject *Sender)
{
   setSingleFramePreviewModeFrameIndex();
   GThreeLayerTool->ResetRedOffsets(true, false);

   DummyEdit->SetFocus();
}

void __fastcall TThreeLayerForm::RedOffsetYPanelClick(TObject *Sender)
{
   setSingleFramePreviewModeFrameIndex();
   GThreeLayerTool->ResetRedOffsets(false, true);

   DummyEdit->SetFocus();
}

//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::BlueResetButtonClick(TObject *Sender)
{
   setSingleFramePreviewModeFrameIndex();
   GThreeLayerTool->ResetBlueOffsets(true, true);

   DummyEdit->SetFocus();
}

void __fastcall TThreeLayerForm::BlueOffsetXPanelClick(TObject *Sender)
{
   setSingleFramePreviewModeFrameIndex();
   GThreeLayerTool->ResetBlueOffsets(true, false);

   DummyEdit->SetFocus();
}

void __fastcall TThreeLayerForm::BlueOffsetYPanelClick(TObject *Sender)
{
   setSingleFramePreviewModeFrameIndex();
   GThreeLayerTool->ResetBlueOffsets(false, true);

   DummyEdit->SetFocus();
}

//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::RedButtonClick(TObject *Sender)
{
   setSingleFramePreviewModeFrameIndex();
   // click either red horz or red vert offset display buttons
   GThreeLayerTool->ActivateRed();
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::BlueButtonClick(TObject *Sender)
{
   setSingleFramePreviewModeFrameIndex();
   // click either blue horz or blue vert offset display buttons
   GThreeLayerTool->ActivateBlue();
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::GangCheckBoxClick(TObject *Sender)
{
   setSingleFramePreviewModeFrameIndex();
   GThreeLayerTool->ToggleGanged();

   DummyEdit->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::AlignResetSpeedButtonClick(TObject *Sender)
{
#if ZERO_RESETS_SELECTED_ONLY
   int majorState = GThreeLayerTool->getMajorState();

   if (majorState == MAJORSTATE_MANUAL_RED) {
      GThreeLayerTool->ResetRedOffsets(true, true);
   }
   else if (majorState == MAJORSTATE_MANUAL_BLUE) {
      GThreeLayerTool->ResetBlueOffsets(true, true);
   }
   else if (majorState == MAJORSTATE_MANUAL_GANGED) {
      GThreeLayerTool->ResetRedAndBlueOffsets();
   }
#else // Zero is master reset

   clearSingleFramePreviewModeFrameIndex();
   GThreeLayerTool->ResetRedAndBlueOffsets();
   GThreeLayerTool->ShowProcessedChanged(false);

#endif

   DummyEdit->SetFocus();
}
// ---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::LeftSpeedButtonClick(TObject *Sender)
{
   setSingleFramePreviewModeFrameIndex();
   GThreeLayerTool->JogOffsets(-localJogIncrement, 0.0);
   DummyEdit->SetFocus();
}
// ---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::RightSpeedButtonClick(TObject *Sender)
{
   setSingleFramePreviewModeFrameIndex();
   GThreeLayerTool->JogOffsets(localJogIncrement, 0.0);
   DummyEdit->SetFocus();
}
// ---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::UpSpeedButtonClick(TObject *Sender)
{
   setSingleFramePreviewModeFrameIndex();
   GThreeLayerTool->JogOffsets(0.0, -localJogIncrement);
   DummyEdit->SetFocus();
}
// ---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::DownSpeedButtonClick(TObject *Sender)
{
   setSingleFramePreviewModeFrameIndex();
   GThreeLayerTool->JogOffsets(0.0, localJogIncrement);
   DummyEdit->SetFocus();
}
//---------------------------------------------------------------------------

/////////////////////////  EXEC BUTTON TOOLBAR  ///////////////////////

void __fastcall TThreeLayerForm::ExecButtonsToolbar_ButtonPressedNotifier(TObject *Sender)

{
   switch (ExecButtonsToolbar->GetLastButtonClicked())
   {
      default:
      case EXEC_BUTTON_NONE:
         // No-op
         break;

      case EXEC_BUTTON_PREVIEW_FRAME:
         ExecButtonsToolbarPreviewFrame();
         break;

      case EXEC_BUTTON_PREVIEW_ALL:
         ExecButtonsToolbarPreviewAll();
         break;

      case EXEC_BUTTON_RENDER_FRAME:
         ExecButtonsToolbarRenderFrame();
         break;

      case EXEC_BUTTON_RENDER_ALL:
         ExecButtonsToolbarRenderAll();
         break;

      case EXEC_BUTTON_PAUSE_OR_RESUME:
         if (ExecButtonsToolbar->IsButtonEnabled(EXEC_BUTTON_PAUSE))
         {
            ExecButtonsToolbarPause();
         }
         else if (ExecButtonsToolbar->IsButtonEnabled(EXEC_BUTTON_RESUME))
         {
            ExecButtonsToolbarResume();
         }

         break;

      case EXEC_BUTTON_PAUSE:
         ExecButtonsToolbarPause();
         break;

      case EXEC_BUTTON_RESUME:
         ExecButtonsToolbarResume();
         break;

      case EXEC_BUTTON_STOP:
         ExecButtonsToolbarStop();
         break;

      case EXEC_BUTTON_CAPTURE_PDL:
         ExecButtonsToolbarCapturePDL();
         break;

      case EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS:
         ExecButtonsToolbarCaptureAllEventsToPDL();
         break;

		case EXEC_BUTTON_GO_TO_RESUME_TIMECODE:
         ExecButtonsToolbarGoToResumeTimecode();
         break;

      case EXEC_BUTTON_SET_RESUME_TC:
         ExecButtonsToolbarSetResumeTimecodeToCurrent();
         break;
   }

}

/////////////////////////  PREVIEW & RENDER CONTROLS  ///////////////////////

//---------------------------------------------------------------------------

void  TThreeLayerForm::ExecButtonsToolbarPreviewFrame()
{
	// WTF is this Down shit??? QQQ
  if (!ExecButtonsToolbar->PreviewFrameButton->Down) {

     setSingleFramePreviewModeFrameIndex();

     // preview one frame
     GThreeLayerTool->AlignCurrentFrameSelChannels(false);
  }
}
//---------------------------------------------------------------------------

void TThreeLayerForm::ExecButtonsToolbarPreviewAll()
{
	// WTF is this Down shit??? QQQ
  if (!ExecButtonsToolbar->PreviewAllButton->Down) {

     clearSingleFramePreviewModeFrameIndex();

     // no render, just display
     GThreeLayerTool->ExecuteRangeProcess(false);
  }
}
//---------------------------------------------------------------------------

void TThreeLayerForm::ExecButtonsToolbarRenderFrame()
{
  if (!ExecButtonsToolbar->RenderFrameButton->Down) {

     clearSingleFramePreviewModeFrameIndex();

     // render one frame
     GThreeLayerTool->AlignCurrentFrameSelChannels(true);
  }
}
//---------------------------------------------------------------------------

void TThreeLayerForm::ExecButtonsToolbarRenderAll()
{
	// WTF is this Down shit??? QQQ
  if (!ExecButtonsToolbar->RenderAllButton->Down) {

     clearSingleFramePreviewModeFrameIndex();

    // render it
    GThreeLayerTool->ExecuteRangeProcess(true);
  }
}
//---------------------------------------------------------------------------

void TThreeLayerForm::ExecButtonsToolbarPause()
{
	// WTF is this Down shit??? QQQ
  if (!ExecButtonsToolbar->PauseButton->Down) {

     GThreeLayerTool->SuspendRangeProcess(false);
  }
}
//---------------------------------------------------------------------------

void TThreeLayerForm::ExecButtonsToolbarResume()
{
	// WTF is this Down shit??? QQQ
  if (!ExecButtonsToolbar->ResumeButton->Down) {

     GThreeLayerTool->ResumeRangeProcess();
  }
}
//---------------------------------------------------------------------------

void TThreeLayerForm::ExecButtonsToolbarStop()
{
	// WTF is this Down shit??? QQQ
  if (!ExecButtonsToolbar->StopButton->Down) {

     clearSingleFramePreviewModeFrameIndex();
     GThreeLayerTool->SuspendRangeProcess(true);
  }
}
//---------------------------------------------------------------------------

void TThreeLayerForm::ExecButtonsToolbarCapturePDL()
{
   clearSingleFramePreviewModeFrameIndex();
   GThreeLayerTool->CapturePDLEvent(false);
}
//---------------------------------------------------------------------------

void TThreeLayerForm::ExecButtonsToolbarCaptureAllEventsToPDL()
{
   clearSingleFramePreviewModeFrameIndex();
   GThreeLayerTool->CapturePDLEvent(true);
}
//---------------------------------------------------------------------------

/////////////////////////  Resume Timecode Widget  ///////////////////////

void TThreeLayerForm::ExecSetResumeTimecode(int frameIndex)
{
   if (GThreeLayerTool == NULL) return;

   CVideoFrameList *frameList = GThreeLayerTool->getSystemAPI()->getVideoFrameList();
   if (frameList == NULL) return;

   if (frameIndex < 0)
      frameIndex = frameList->getInFrameIndex();

   PTimecode frameTimecode = frameList->getTimecodeForFrameIndex(frameIndex);
   ExecButtonsToolbar->SetResumeTimecode(frameTimecode);

   GThreeLayerTool->SetResumeFrame(frameIndex);
}
//---------------------------------------------------------------------------

void TThreeLayerForm::ExecSetResumeTimecodeToDefault()
{
   if (GThreeLayerTool == NULL) return;

   int frameIndex = GThreeLayerTool->getSystemAPI()->getMarkIn();
   ExecSetResumeTimecode(frameIndex);
}
//---------------------------------------------------------------------------

void TThreeLayerForm::ExecButtonsToolbarSetResumeTimecodeToCurrent()
{
   if (GThreeLayerTool == NULL) return;

   int frameIndex = GThreeLayerTool->getSystemAPI()->getLastFrameIndex();
   ExecSetResumeTimecode(frameIndex);
}
//---------------------------------------------------------------------------

void TThreeLayerForm::ExecButtonsToolbarGoToResumeTimecode()
{
   if (GThreeLayerTool == NULL) return;

   CVideoFrameList *frameList;
   frameList = GThreeLayerTool->getSystemAPI()->getVideoFrameList();
   if (frameList == NULL) return;

   PTimecode resumeTimecode = ExecButtonsToolbar->GetResumeTimecode();
   int frameIndex = frameList->getFrameIndex(resumeTimecode);
   GThreeLayerTool->getSystemAPI()->goToFrameSynchronous(frameIndex);

   // In case it ends up somewhere else?
   frameIndex = GThreeLayerTool->getSystemAPI()->getLastFrameIndex();
   ExecButtonsToolbar->SetResumeTimecode(frameList->getTimecodeForFrameIndex(frameIndex));
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------


void __fastcall TThreeLayerForm::ShowOriginalSpeedButtonOnClick(TObject *Sender)
{
   if (ShowProcessedSpeedButton->Enabled && ShowOriginalSpeedButton->Down)
   {
      clearSingleFramePreviewModeFrameIndex();
      GThreeLayerTool->ShowProcessedChanged(false);
   }

   DummyEdit->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::ShowProcessedSpeedButtonOnClick(
      TObject *Sender)
{
   if (ShowProcessedSpeedButton->Down)
   {
      if (singleFramePreviewFrameIndex < 0)
      {
         setSingleFramePreviewModeFrameIndex();
      }

      GThreeLayerTool->ShowProcessedChanged(true);
   }

   DummyEdit->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::AlignSpeedButtonClick(TObject *Sender)
{
  // preview one frame
  setSingleFramePreviewModeFrameIndex();
  GThreeLayerTool->AlignCurrentFrameSelChannels(false);
}
//---------------------------------------------------------------------------

bool TThreeLayerForm::ToSimpleInteger(AnsiString txt, int *val)
{
   *val = 0;
   int count = txt.Length();
   int state = 0;
   int whole = 0;

   for (int i=1; i<=count; i++) {

      char thechar = txt[i];

      switch(state) {

         case 0: // leading spaces

            if ((thechar >= '0')&&(thechar <= '9')) {

               whole = thechar - '0';

               state = 1;
            }
            else if (thechar != ' ')
               return(false);

         break;

         case 1: // integer part

            if ((thechar >= '0')&&(thechar <= '9')) {

               whole = whole*10 + (thechar - '0');
            }
            else if (thechar == ' ') {

               state = 2;
            }
            else {
               return(false);
            }

         break;

         case 2:  // trailing spaces

            if (thechar != ' ')
               return(false);

         break;
      }
   }

   if (state == 0)
      return(false);

   *val = whole;

   return(true);
}
//---------------------------------------------------------------------------

void TThreeLayerForm::UpdateEditIn(int val)
{
   clearSingleFramePreviewModeFrameIndex();
   localEditIn = val;
   GThreeLayerTool->SetEditIn(val);
   InSpinEdit->SetValue(val);
}

void TThreeLayerForm::LoadEditIn()
{
   UpdateEditIn(InSpinEdit->GetValue());
}

void TThreeLayerForm::JogEditIn(int incr)
{
   LoadEditIn();

   int newval = localEditIn + incr;
   if (newval < MINEDTIN) newval = MINEDTIN;
   else if (newval > MAXEDTIN)
      newval = MAXEDTIN;

   UpdateEditIn(newval);
}
//---------------------------------------------------------------------------

void TThreeLayerForm::UpdateEditOut(int val)
{
   clearSingleFramePreviewModeFrameIndex();
   localEditOut = val;
   GThreeLayerTool->SetEditOut(val);
   OutSpinEdit->SetValue(val);
}

void TThreeLayerForm::LoadEditOut()
{
   UpdateEditOut(OutSpinEdit->GetValue());
}

void TThreeLayerForm::JogEditOut(int incr)
{
   LoadEditOut();

   int newval = localEditOut + incr;
   if (newval < MINEDTOUT) newval = MINEDTOUT;
   else if (newval > MAXEDTOUT)
      newval = MAXEDTOUT;

   UpdateEditOut(newval);
}
//---------------------------------------------------------------------------

void TThreeLayerForm::UpdateEditAvg(int val)
{
   clearSingleFramePreviewModeFrameIndex();
   localEditAvg = val;
   GThreeLayerTool->SetEditAvg(val);
   AverageSpinEdit->SetValue(val);
}

void TThreeLayerForm::LoadEditAvg()
{
   UpdateEditAvg(AverageSpinEdit->GetValue());
}

void TThreeLayerForm::JogEditAvg(int incr)
{
   LoadEditAvg();

   int newval = localEditAvg + incr;
   if (newval < MINEDTAVG) newval = MINEDTAVG;

   UpdateEditAvg(newval);
}
//---------------------------------------------------------------------------

void TThreeLayerForm::UpdateRefEditIn(int val)
{
   clearSingleFramePreviewModeFrameIndex();
   localRefEditIn = val;
   GThreeLayerTool->SetRefEditIn(val);
   RefInSpinEdit->SetValue(val);
}

void TThreeLayerForm::LoadRefEditIn()
{
   UpdateRefEditIn(RefInSpinEdit->GetValue());
}

void TThreeLayerForm::JogRefEditIn(int incr)
{
   LoadRefEditIn();

   int newval = localRefEditIn + incr;
   if (newval < MINREFEDTIN) newval = MINREFEDTIN;
   else if (newval > MAXREFEDTIN)
      newval = MAXREFEDTIN;

   UpdateRefEditIn(newval);
}
//---------------------------------------------------------------------------

void TThreeLayerForm::UpdateRefEditOut(int val)
{
   clearSingleFramePreviewModeFrameIndex();
   localRefEditOut = val;
   GThreeLayerTool->SetRefEditOut(val);
   RefOutSpinEdit->SetValue(val);
}

void TThreeLayerForm::LoadRefEditOut()
{
   UpdateRefEditOut(RefOutSpinEdit->GetValue());
}

void TThreeLayerForm::JogRefEditOut(int incr)
{
   LoadRefEditOut();

   int newval = localRefEditOut + incr;
   if (newval < MINREFEDTOUT) newval = MINREFEDTOUT;
   else if (newval > MAXREFEDTOUT)
      newval = MAXREFEDTOUT;

   UpdateRefEditOut(newval);
}
//---------------------------------------------------------------------------

void TThreeLayerForm::UpdateReferenceFrame(int frm)
{
   clearSingleFramePreviewModeFrameIndex();
   localReferenceFrame = frm;

   GThreeLayerTool->SetReferenceFrame(frm);

   CVideoFrameList *frameList = GThreeLayerTool->getSystemAPI()->getVideoFrameList();
   if (frameList == NULL) return;

   ReferenceFrameTCEdit->OnTimecodeChange = NULL;
   ReferenceFrameTCEdit->tcP = frameList->getTimecodeForFrameIndex(frm);
   ReferenceFrameTCEdit->OnTimecodeChange = ReferenceFrameOnChange;

   ReferenceFrameTCEdit->Repaint();
}

void TThreeLayerForm::LoadReferenceFrame()
{
   CVideoFrameList *frameList = GThreeLayerTool->getSystemAPI()->
                                   getVideoFrameList();
   if (frameList == NULL) return;

   int minFrame = frameList->getInFrameIndex();
   int maxFrame = frameList->getOutFrameIndex() - 1;

   int newfrm = frameList->getFrameIndex(ReferenceFrameTCEdit->tcP);

   if (newfrm < minFrame)
      newfrm = minFrame;
   else if (newfrm > maxFrame)
      newfrm = maxFrame;

   UpdateReferenceFrame(newfrm);
}

int  TThreeLayerForm::GetReferenceFrame()
{
   return localReferenceFrame;
}

void TThreeLayerForm::JogReferenceFrame(int incr)
{
   LoadReferenceFrame();

   CVideoFrameList *frameList = GThreeLayerTool->getSystemAPI()->
                                getVideoFrameList();
   if (frameList == NULL) return;

   // frame limits of the clip
   int minFrame = frameList->getInFrameIndex();
   int maxFrame = frameList->getOutFrameIndex() - 1;

   int newref = localReferenceFrame + incr;
   if (newref < minFrame)
      newref = minFrame;
   else if (newref > maxFrame)
      newref = maxFrame;

   UpdateReferenceFrame(newref);
}
//---------------------------------------------------------------------------

void TThreeLayerForm::UpdateTargetView(bool proc)
{
   if (proc == ShowProcessedSpeedButton->Down) return;

   ShowOriginalSpeedButton->OnClick  = NULL;
   ShowProcessedSpeedButton->OnClick = NULL;

   if (proc) {

      ShowProcessedSpeedButton->Down =  true;
   }
   else {

      ShowOriginalSpeedButton->Down  = true;
   }

   ShowOriginalSpeedButton->Repaint();
   ShowProcessedSpeedButton->Repaint();

   ShowProcessedSpeedButton->OnClick = ShowProcessedSpeedButtonOnClick;
   ShowOriginalSpeedButton->OnClick  = ShowOriginalSpeedButtonOnClick;

}
//---------------------------------------------------------------------------

void TThreeLayerForm::UpdateWarpMode(bool wrpmod)
{
   localWarpMode = wrpmod;

   // Make sure we remember these so we can restore them if we switch modes back!!
   GThreeLayerTool->SetPointsDim(PointsSpinEdit->GetValue());
   GThreeLayerTool->SetRangeDim(RangeSpinEdit->GetValue());

   GThreeLayerTool->SetWarpMode(wrpmod);

   MoveRadioButton->OnEnter     = NULL;
   MoveRadioButton->Checked     = !wrpmod;
   MoveRadioButton->OnEnter     = MoveRadioButtonOnEnter;

   MoveWarpRadioButton->OnEnter = NULL;
   MoveWarpRadioButton->Checked = wrpmod;
   MoveWarpRadioButton->OnEnter = MoveWarpRadioButtonOnEnter;

   if (wrpmod)
   {
      PointsSpinEdit->SetRange(MINPTS_WRP, MAXPTS_WRP);
   }
   else
   {
      PointsSpinEdit->SetRange(MINPTS_MOV, MAXPTS_MOV);
   }

   // These must go AFTER SetWarpMode() and SetRange()s!!
   PointsSpinEdit->SetValue(GThreeLayerTool->GetPointsDim());
   RangeSpinEdit->SetValue(GThreeLayerTool->GetRangeDim());
}
//---------------------------------------------------------------------------

void TThreeLayerForm::UpdatePointsDim(int val)
{
   localPointsDim = val;
   GThreeLayerTool->SetPointsDim(val);
   PointsSpinEdit->SetValue(val);
}

void TThreeLayerForm::LoadPointsDim()
{
   UpdatePointsDim(PointsSpinEdit->GetValue());
}

void TThreeLayerForm::JogPointsDim(int incr)
{
   LoadPointsDim();

   int value = localPointsDim + incr;

   if (!localWarpMode) {

      if (value < MINPTS_MOV) {
         value = MINPTS_MOV;
      }
      else if (value > MAXPTS_MOV) {
         value = MAXPTS_MOV;
      }
   }
   else {

      if (value < MINPTS_WRP) {
         value = MINPTS_WRP;
      }
      else if (value > MAXPTS_WRP) {
         value = MAXPTS_WRP;
      }
   }

   UpdatePointsDim(value);
}
//---------------------------------------------------------------------------

void TThreeLayerForm::UpdateRangeDim(int val)
{
   localRangeDim = val;
   GThreeLayerTool->SetRangeDim(localRangeDim);
   RangeSpinEdit->SetValue(val);
}

void TThreeLayerForm::LoadRangeDim()
{
   UpdateRangeDim(RangeSpinEdit->GetValue());
}

void TThreeLayerForm::JogRangeDim(int incr)
{
   LoadRangeDim();

   int value = localRangeDim + incr;

   if (value < 2)
      value = 2;
   else if (value > MAXRNG)
      value = MAXRNG;

   UpdateRangeDim(value);
}
//---------------------------------------------------------------------------

void TThreeLayerForm::UpdatePixelsDim(int val)
{
   GThreeLayerTool->SetPixelsDim(val);
   // It seems this control was eliminated
}

void TThreeLayerForm::LoadPixelsDim()
{
   // It seems this control was eliminated
}
//---------------------------------------------------------------------------

void TThreeLayerForm::UpdateAnalysisMatBox(bool use)
{
   AnalysisCBox->OnClick = NULL;
   AnalysisCBox->Checked = use;
   AnalysisCBox->OnClick = AnalysisCBoxClick;
   AnalysisCBox->Repaint();

   // This must be done AFTER setting AnalysisCBox->Checked
   enableMattingControls(use, true);
}
//---------------------------------------------------------------------------

void TThreeLayerForm::UpdateProcessingMatBox(bool use)
{
   procMatSetState = use;

   ProcessingCBox->OnClick = NULL;
   ProcessingCBox->Checked = use;
   ProcessingCBox->OnClick = ProcessingCBoxClick;
   ProcessingCBox->Repaint();
}
//---------------------------------------------------------------------------

void TThreeLayerForm::UpdateShowMatBox(bool show)
{
   showMatSetState = show;

   ShowMattingCBox->OnClick = NULL;
   ShowMattingCBox->Checked = show;
   ShowMattingCBox->OnClick = ShowMattingCBoxClick;
   ShowMattingCBox->Repaint();
}
//---------------------------------------------------------------------------

void TThreeLayerForm::UpdateResumeFrame(int frm)
{
   localResumeFrame = frm;

   GThreeLayerTool->SetResumeFrame(frm);

   CVideoFrameList *frameList = GThreeLayerTool->getSystemAPI()->getVideoFrameList();
   if (frameList == NULL)
   {
      return;
   }

   ExecButtonsToolbar->SetResumeTimecode(frameList->getTimecodeForFrameIndex(frm));
}

void TThreeLayerForm::LoadResumeFrame()
{
   CVideoFrameList *frameList = GThreeLayerTool->getSystemAPI()->getVideoFrameList();
   if (frameList == NULL)
   {
      return;
   }

	int newfrm = frameList->getFrameIndex(ExecButtonsToolbar->GetResumeTimecode());
   newfrm = GThreeLayerTool->ClipResumeFrame(newfrm);

   UpdateResumeFrame(newfrm);
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::AutoPerShotRadioButtonOnEnter(
      TObject *Sender)
{
   // clear color pick if necessary
   int majorState = GThreeLayerTool->getMajorState();
   if (majorState == MAJORSTATE_PICK_MAT_COLOR) {
      GThreeLayerTool->ExitColorPickMode();
	  majorState = GThreeLayerTool->getMajorState();
   }

   int renderState = GThreeLayerTool->getRenderState();
   if (renderState != RENDER_STATE_IDLE) {
      AutoPerShotRadioButton->Checked = false;
      return;
   }

   switch(majorState) {

      case MAJORSTATE_MANUAL_RED:
      case MAJORSTATE_MANUAL_BLUE:
      case MAJORSTATE_MANUAL_GANGED:

         GThreeLayerTool->setMajorState(MAJORSTATE_MANUAL_HOME);

      break;

      default:
      break;
   }

   majorState = GThreeLayerTool->getMajorState();
   switch(majorState) {

      case MAJORSTATE_AUTO_PER_SHOT_HOME:
      case MAJORSTATE_AUTO_PER_FRAME_HOME:
      case MAJORSTATE_MANUAL_HOME:
      case MAJORSTATE_REFERENCE_FRAME_HOME:

         UpdateOperationMode(OPERATING_MODE_AUTO_PER_SHOT);

         GThreeLayerTool->ClearCombinedTransform();

         GThreeLayerTool->setMajorState(MAJORSTATE_AUTO_PER_SHOT_HOME);

      break;

      default:

      break;
   }

   DummyEdit->SetFocus();
}
//---------------------------------------------------------------------------

void TThreeLayerForm::InSpinEditHasChanged(void *Sender)
{
   savedEditIn = localEditIn;
   savedMajorState = GThreeLayerTool->getMajorState();
   GThreeLayerTool->setMajorState(MAJORSTATE_AUTO_PER_SHOT_EDIT_IN, false);
   LoadEditIn();
   GThreeLayerTool->setMajorState(savedMajorState);
}
//---------------------------------------------------------------------------

void TThreeLayerForm::OutSpinEditHasChanged(void *Sender)
{
   savedEditOut = localEditOut;
   savedMajorState = GThreeLayerTool->getMajorState();
   GThreeLayerTool->setMajorState(MAJORSTATE_AUTO_PER_SHOT_EDIT_OUT, false);
   LoadEditOut();
   GThreeLayerTool->setMajorState(savedMajorState);
}
//---------------------------------------------------------------------------

void TThreeLayerForm::AverageSpinEditHasChanged(void *Sender)
{
   savedEditAvg = localEditAvg;
   savedMajorState = GThreeLayerTool->getMajorState();
   GThreeLayerTool->setMajorState(MAJORSTATE_AUTO_PER_SHOT_EDIT_AVG, false);
   LoadEditAvg();
   GThreeLayerTool->setMajorState(savedMajorState);
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::AutoPerFrameRadioButtonOnEnter(
      TObject *Sender)
{
   // clear color pick if necessary
   int majorState = GThreeLayerTool->getMajorState();
   if (majorState == MAJORSTATE_PICK_MAT_COLOR) {
      GThreeLayerTool->ExitColorPickMode();
	  majorState = GThreeLayerTool->getMajorState();
   }

   int renderState = GThreeLayerTool->getRenderState();
   if (renderState != RENDER_STATE_IDLE) {
      AutoPerFrameRadioButton->Checked = false;
      return;
   }

   switch(majorState) {

      case MAJORSTATE_MANUAL_RED:
      case MAJORSTATE_MANUAL_BLUE:
      case MAJORSTATE_MANUAL_GANGED:

         GThreeLayerTool->setMajorState(MAJORSTATE_MANUAL_HOME);

      break;

      default:
      break;
   }

   majorState = GThreeLayerTool->getMajorState();
   switch(majorState) {

      case MAJORSTATE_AUTO_PER_SHOT_HOME:
      case MAJORSTATE_AUTO_PER_FRAME_HOME:
      case MAJORSTATE_MANUAL_HOME:
      case MAJORSTATE_REFERENCE_FRAME_HOME:

         UpdateOperationMode(OPERATING_MODE_AUTO_PER_FRAME);

         GThreeLayerTool->ClearCombinedTransform();

         GThreeLayerTool->setMajorState(MAJORSTATE_AUTO_PER_FRAME_HOME);

      break;

      default:

      break;
   }

   DummyEdit->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::ManualRadioButtonOnEnter(TObject *Sender)
{
   // clear color pick if necessary
   int majorState = GThreeLayerTool->getMajorState();
   if (majorState == MAJORSTATE_PICK_MAT_COLOR) {
      GThreeLayerTool->ExitColorPickMode();
      majorState = GThreeLayerTool->getMajorState();
   }

   int renderState = GThreeLayerTool->getRenderState();
   if (renderState != RENDER_STATE_IDLE) {
      ManualRadioButton->Checked = false;
      return;
   }

   switch(majorState) {

      case MAJORSTATE_MANUAL_RED:
      case MAJORSTATE_MANUAL_BLUE:
      case MAJORSTATE_MANUAL_GANGED:

         GThreeLayerTool->setMajorState(MAJORSTATE_MANUAL_HOME);

      break;

      default:
      break;
   }

   majorState = GThreeLayerTool->getMajorState();
   switch(majorState) {

      case MAJORSTATE_AUTO_PER_SHOT_HOME:
      case MAJORSTATE_AUTO_PER_FRAME_HOME:
      case MAJORSTATE_MANUAL_HOME:
      case MAJORSTATE_REFERENCE_FRAME_HOME:

         UpdateOperationMode(OPERATING_MODE_MANUAL);

         GThreeLayerTool->ClearCombinedTransform();

         GThreeLayerTool->setMajorState(MAJORSTATE_MANUAL_HOME);

      break;

      default:

      break;

   }

   DummyEdit->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::ReferenceFrameRadioButtonOnEnter(
      TObject *Sender)
{
   // clear color pick if necessary
   int majorState = GThreeLayerTool->getMajorState();
   if (majorState == MAJORSTATE_PICK_MAT_COLOR) {
      GThreeLayerTool->ExitColorPickMode();
      majorState = GThreeLayerTool->getMajorState();
   }

   int renderState = GThreeLayerTool->getRenderState();
   if (renderState != RENDER_STATE_IDLE) {
      ReferenceFrameRadioButton->Checked = false;
      return;
   }

   switch(majorState) {

      case MAJORSTATE_MANUAL_RED:
      case MAJORSTATE_MANUAL_BLUE:
      case MAJORSTATE_MANUAL_GANGED:

         GThreeLayerTool->setMajorState(MAJORSTATE_MANUAL_HOME);

      break;

      default:
      break;
   }

   majorState = GThreeLayerTool->getMajorState();
   switch(majorState) {

      case MAJORSTATE_AUTO_PER_SHOT_HOME:
      case MAJORSTATE_AUTO_PER_FRAME_HOME:
      case MAJORSTATE_MANUAL_HOME:
      case MAJORSTATE_REFERENCE_FRAME_HOME:

         UpdateOperationMode(OPERATING_MODE_REFERENCE_FRAME);

         GThreeLayerTool->ClearCombinedTransform();

         GThreeLayerTool->setMajorState(MAJORSTATE_REFERENCE_FRAME_HOME);

      break;

      default:

      break;
   }

   DummyEdit->SetFocus();
}
//---------------------------------------------------------------------------

void TThreeLayerForm::RefInSpinEditHasChanged(void *Sender)
{
   savedRefEditIn = localRefEditIn;
   savedMajorState = GThreeLayerTool->getMajorState();
   GThreeLayerTool->setMajorState(MAJORSTATE_REFERENCE_FRAME_EDIT_IN, false);
   LoadRefEditIn();
   GThreeLayerTool->setMajorState(savedMajorState , false);
}
//---------------------------------------------------------------------------

void TThreeLayerForm::RefOutSpinEditHasChanged(void *Sender)
{
   savedRefEditOut = localRefEditOut;
   savedMajorState = GThreeLayerTool->getMajorState();
   GThreeLayerTool->setMajorState(MAJORSTATE_REFERENCE_FRAME_EDIT_OUT, false);
   LoadRefEditOut();
   GThreeLayerTool->setMajorState(savedMajorState);
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::ReferenceFrameSetStartSpeedButtonClick(
      TObject *Sender)
{
   GThreeLayerTool->LoadReferenceFrame();

   DummyEdit->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::ReferenceFrameGoToSpeedButtonClick(
      TObject *Sender)
{
   GThreeLayerTool->GoToReferenceFrame();

   DummyEdit->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::ReferenceFrameOnChange(TObject *Sender)
{
   LoadReferenceFrame();
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::ReferenceFrameOnEnter(TObject *Sender)
{
   savedReferenceFrame = localReferenceFrame;

   savedMajorState = GThreeLayerTool->getMajorState();

   GThreeLayerTool->setMajorState(MAJORSTATE_REFERENCE_FRAME_EDIT_REF, false);
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::ReferenceFrameOnExit(TObject *Sender)
{
   LoadReferenceFrame();

   GThreeLayerTool->setMajorState(savedMajorState);
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::ReferenceFrameOnEscapeKey(TObject *Sender)
{
   UpdateReferenceFrame(savedReferenceFrame);

   DummyEdit->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::ReferenceFrameOnEnterKey(TObject *Sender)
{
   LoadReferenceFrame();

   GThreeLayerTool->ClearBaseTransform();

   GThreeLayerTool->getSystemAPI()->refreshFrameCached();

   DummyEdit->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::MoveRadioButtonOnEnter(TObject *Sender)
{
   MoveRadioButton->Checked = true;

   UpdateWarpMode(false);

   DummyEdit->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::MoveWarpRadioButtonOnEnter(TObject *Sender)
{
   MoveWarpRadioButton->Checked = true;

   UpdateWarpMode(true);

   DummyEdit->SetFocus();
}
//---------------------------------------------------------------------------

void TThreeLayerForm::PointsSpinEditHasChanged(void *Sender)
{
   savedPointsDim = localPointsDim;
   savedMajorState = GThreeLayerTool->getMajorState();
   GThreeLayerTool->setMajorState(MAJORSTATE_ANALYSIS_EDIT_PTS, false);
   LoadPointsDim();
   GThreeLayerTool->setMajorState(savedMajorState);
}
//---------------------------------------------------------------------------

void TThreeLayerForm::RangeSpinEditHasChanged(void *Sender)
{
   savedRangeDim = localRangeDim;
   savedMajorState = GThreeLayerTool->getMajorState();
   GThreeLayerTool->setMajorState(MAJORSTATE_ANALYSIS_EDIT_RNG, false);
   LoadRangeDim();
   GThreeLayerTool->setMajorState(savedMajorState);
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::AnalysisCBoxClick(TObject *Sender)
{
   BounceOutOfColorPickMode();
   enableMattingControls(AnalysisCBox->Checked, true);
   GThreeLayerTool->SetAnalysisMatBox(AnalysisCBox->Checked);
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::ProcessingCBoxClick(TObject *Sender)
{
   BounceOutOfColorPickMode();
   GThreeLayerTool->SetProcessingMatBox(ProcessingCBox->Checked);
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::ShowMattingCBoxClick(TObject *Sender)
{
   BounceOutOfColorPickMode();
   GThreeLayerTool->SetShowMatBox(ShowMattingCBox->Checked);
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::TopUpButtonClick(TObject *Sender)
{
   if (GlobalSender == NULL) return;
   BounceOutOfColorPickMode();
   GThreeLayerTool->JogMatBox(0, -1, 0, 0);
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::TopDownButtonClick(TObject *Sender)
{
   if (GlobalSender == NULL) return;
   BounceOutOfColorPickMode();
   GThreeLayerTool->JogMatBox(0, 1, 0, 0);
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::LeftLeftButtonClick(TObject *Sender)
{
   if (GlobalSender == NULL) return;
   BounceOutOfColorPickMode();
   GThreeLayerTool->JogMatBox(-1, 0, 0, 0);
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::LeftRightButtonClick(TObject *Sender)
{
   if (GlobalSender == NULL) return;
   BounceOutOfColorPickMode();
   GThreeLayerTool->JogMatBox( 1, 0, 0, 0);
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::RightLeftButtonClick(TObject *Sender)
{
   if (GlobalSender == NULL) return;
   BounceOutOfColorPickMode();
   GThreeLayerTool->JogMatBox( 0, 0, -1, 0);
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::RightRightButtonClick(TObject *Sender)
{
   if (GlobalSender == NULL) return;
   BounceOutOfColorPickMode();
   GThreeLayerTool->JogMatBox( 0, 0, 1, 0);
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::BottomUpButtonClick(TObject *Sender)
{
   if (GlobalSender == NULL) return;
   BounceOutOfColorPickMode();
   GThreeLayerTool->JogMatBox( 0, 0, 0, -1);
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::BottomDownButtonClick(TObject *Sender)
{
   if (GlobalSender == NULL) return;
   BounceOutOfColorPickMode();
   GThreeLayerTool->JogMatBox( 0, 0, 0, 1);
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::AllMatButtonClick(TObject *Sender)
{
   BounceOutOfColorPickMode();
   GThreeLayerTool->SetMatBoxAll();
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::ColorPickButtonClick(TObject *Sender)
{
   if (GThreeLayerTool == NULL) return;

   if (GThreeLayerTool->IsInColorPickMode())
   {
      GThreeLayerTool->ExitColorPickMode();
      ColorPickerStateTimer->Enabled = false;
   }
   else
   {
      GThreeLayerTool->EnterColorPickMode();
      ColorPickerStateTimer->Enabled = true;
      RemoveSpinEditHooks();
      ReferenceFrameTCEdit->OnExit = NULL;

      // now safe to do this
      DummyEdit->SetFocus();

      SetSpinEditHooks();
      ReferenceFrameTCEdit->OnExit = ReferenceFrameOnExit;
   }
}

//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::ColorPickerStateTimerTimer(
      TObject *Sender)
{
   if ((GThreeLayerTool == NULL) || !GThreeLayerTool->IsInColorPickMode())
   {
      ColorPickButton->Down = false;
      ColorPickerStateTimer->Enabled = false;
   }
}
//---------------------------------------------------------------------------

void TThreeLayerForm::BounceOutOfColorPickMode()
{
   if (GThreeLayerTool->IsInColorPickMode())
   {
      ThreeLayerForm->ColorPickButtonClick(NULL);
   }
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::DownTimerTimer(TObject *Sender)
{
    if (GlobalSender == NULL)
      {
        DownTimer->Enabled = false;
        return;
      }

    TSpeedButton *sb = dynamic_cast<TSpeedButton *>(GlobalSender);

    sb->OnClick(sb);

    DownTimer->Interval = 75;
}
//---------------------------------------------------------------------------
// Emulate auto-repeat on mouse down on one of the matte sizing buttons
//
void __fastcall TThreeLayerForm::AutoMoveButtonMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
   GlobalSender = Sender;
   TSpeedButton *sb = dynamic_cast<TSpeedButton *>(GlobalSender);
   sb->OnClick(sb);

   DownTimer->Interval = 250;
   DownTimer->Enabled = true;

}
//---------------------------------------------------------------------------
// Emulate auto-repeat on mouse down on one of the matte sizing buttons
//
void __fastcall TThreeLayerForm::AutoMoveButtonMouseUp(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
   GlobalSender = NULL;
   DownTimer->Enabled = false;
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::ExecStatusBarElapsedTimerTimer(
      TObject *Sender)
{
  ExecStatusBar->ElapsedTimerTimer(Sender);

}
//---------------------------------------------------------------------------

void TThreeLayerForm::SetRefreshTimer(int time)
{
   // set the refresh timer to generate a refresh
   refreshTimer = time;
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::RefreshTimerTimer(TObject *Sender)
{
   if (!IsToolVisible())
   {
      refreshTimer = REFR_THRESHOLD + 1;
      clearSingleFramePreviewModeFrameIndex();
      return;
   }

   // count the tick
   refreshTimer++;

   // if max hit, loop around
   if (refreshTimer == 0x7fffffff) {

      refreshTimer = REFR_THRESHOLD + 1;
   }

   // can only happen if timer was zeroed
   if (refreshTimer == REFR_THRESHOLD) {

      GThreeLayerTool->getSystemAPI()->clearLoadedRegistrationFrame();
      int curfrm = GThreeLayerTool->getSystemAPI()->getLastFrameIndex();
      GThreeLayerTool->getSystemAPI()->goToFrameSynchronous(curfrm);
      setSingleFramePreviewModeFrameIndex();
   }

   if (singleFramePreviewFrameIndex >= 0 && PreviewCurrentFrameOnlyCheckBox->Checked)
   {
      int curfrm = GThreeLayerTool->getSystemAPI()->getLastFrameIndex();
      if (curfrm == singleFramePreviewFrameIndex
         && (!ShowProcessedSpeedButton->Enabled || !ShowProcessedSpeedButton->Down))
      {
         UpdateTargetView(true);
         GThreeLayerTool->ShowProcessedChanged(true);
         ShowOriginalSpeedButton->Enabled = true;
         ShowProcessedSpeedButton->Enabled = true;
      }
      else if (curfrm != singleFramePreviewFrameIndex
         && (!ShowOriginalSpeedButton->Enabled || !ShowOriginalSpeedButton->Down))
      {
         UpdateTargetView(false);
         GThreeLayerTool->ShowProcessedChanged(false);
         ShowOriginalSpeedButton->Enabled = true;
         ShowProcessedSpeedButton->Enabled = false;
      }
   }
   else
   {
      singleFramePreviewFrameIndex = -1;
      if (!ShowProcessedSpeedButton->Enabled)
      {
         ShowOriginalSpeedButton->Enabled = true;
         ShowProcessedSpeedButton->Enabled = true;
      }
   }
}
//---------------------------------------------------------------------------



void TThreeLayerForm::SetSpinEditHooks()
{
   SET_CBHOOK(InSpinEditHasChanged, InSpinEdit->SpinEditValueChange);
   SET_CBHOOK(OutSpinEditHasChanged, OutSpinEdit->SpinEditValueChange);
   SET_CBHOOK(AverageSpinEditHasChanged, AverageSpinEdit->SpinEditValueChange);
   SET_CBHOOK(RefInSpinEditHasChanged, RefInSpinEdit->SpinEditValueChange);
   SET_CBHOOK(RefOutSpinEditHasChanged, RefOutSpinEdit->SpinEditValueChange);
   SET_CBHOOK(PointsSpinEditHasChanged, PointsSpinEdit->SpinEditValueChange);
   SET_CBHOOK(RangeSpinEditHasChanged, RangeSpinEdit->SpinEditValueChange);
}
//---------------------------------------------------------------------------

void TThreeLayerForm::RemoveSpinEditHooks()
{
   REMOVE_CBHOOK(InSpinEditHasChanged, InSpinEdit->SpinEditValueChange);
   REMOVE_CBHOOK(OutSpinEditHasChanged, OutSpinEdit->SpinEditValueChange);
   REMOVE_CBHOOK(AverageSpinEditHasChanged, AverageSpinEdit->SpinEditValueChange);
   REMOVE_CBHOOK(RefInSpinEditHasChanged, RefInSpinEdit->SpinEditValueChange);
   REMOVE_CBHOOK(RefOutSpinEditHasChanged, RefOutSpinEdit->SpinEditValueChange);
   REMOVE_CBHOOK(PointsSpinEditHasChanged, PointsSpinEdit->SpinEditValueChange);
   REMOVE_CBHOOK(RangeSpinEditHasChanged, RangeSpinEdit->SpinEditValueChange);
}
//---------------------------------------------------------------------------

void SetSingleFramePreviewModeFrameIndex()
{
   if (ThreeLayerForm == NULL) return;

   ThreeLayerForm->setSingleFramePreviewModeFrameIndex();
}
//---------------------------------------------------------------------------

void ClearSingleFramePreviewModeFrameIndex()
{
   if (ThreeLayerForm == NULL) return;

   ThreeLayerForm->clearSingleFramePreviewModeFrameIndex();
}
//---------------------------------------------------------------------------

void TThreeLayerForm::setSingleFramePreviewModeFrameIndex()
{
    singleFramePreviewFrameIndex = GThreeLayerTool->getSystemAPI()->getLastFrameIndex();
}
//---------------------------------------------------------------------------

void TThreeLayerForm::clearSingleFramePreviewModeFrameIndex()
{
    singleFramePreviewFrameIndex = -1;
}
//---------------------------------------------------------------------------

void __fastcall TThreeLayerForm::PreviewCurrentFrameOnlyCheckBoxClick(TObject *Sender)

{
   if (!PreviewCurrentFrameOnlyCheckBox->Checked)
   {
      clearSingleFramePreviewModeFrameIndex();
   }
}
//---------------------------------------------------------------------------

void ToggleProcessedAndOriginal(bool forceFlag)
{
   if (ThreeLayerForm == NULL) return;

   ThreeLayerForm->toggleProcessedAndOriginal(forceFlag);
}

void TThreeLayerForm::toggleProcessedAndOriginal(bool forceFlag)
{
   if ((singleFramePreviewFrameIndex >= 0)
       && PreviewCurrentFrameOnlyCheckBox->Checked
       && !ShowProcessedSpeedButton->Enabled)
   {
      if (!forceFlag)
      {
         return;
      }

      GThreeLayerTool->getSystemAPI()->goToFrame(singleFramePreviewFrameIndex);
   }

	bool proc = !(ShowProcessedSpeedButton->Enabled && ShowProcessedSpeedButton->Down);

   if ((singleFramePreviewFrameIndex < 0) && proc)
   {
      setSingleFramePreviewModeFrameIndex();
   }
   else if ((singleFramePreviewFrameIndex >= 0) && !proc)
   {
      clearSingleFramePreviewModeFrameIndex();
   }

   UpdateTargetView(proc);
   GThreeLayerTool->ShowProcessedChanged(proc);
}
//---------------------------------------------------------------------------

