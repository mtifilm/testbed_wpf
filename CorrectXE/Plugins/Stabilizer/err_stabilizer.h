/*
   File Name:  err_stabilizer.h
   Created: July 18, 2006

   This contains all the error codes for all core_code functions
*/
#ifndef _ERR_STABILIZER_H
#define _ERR_STABILIZER_H

//-------------------------------------------------------------------------
//
//  Stabilizer Errors  STABILIZER_ERROR__XXX
//
#define STABILIZER_ERROR_TRACKING_COLLAPSE -8500
//  Internal error, this happens when the tracking array is malformed,
//  which usually means that the number of points is NOT the same for
//  each frame.

#define STABILIZER_ERROR_SMOOTHING_TOO_FEW_FRAMES -8501
//  This happens when the number of points to smooth is < 3.
//  Should never happen because the number of frames is checked eariler.

#define STABILIZER_ERROR_SMOOTHING_MATRIX_SINGULAR -8502
//  Internal error, happens when the smoothing matrix cannot be
//  inverted, most likely do to finite arithmetic errors

#define STABILIZER_ERROR_STABILIZER_MATRIX_SINGULAR -8503
//  If there are too few tracking points (caught before the function
//  call) or the points are on a line, this error happens.

#define STABILIZER_ERROR_STABILIZER_INVALID_POINTS -8504
//  Internal error, this happens if the original or smoothed tracking
//  points have a point that is marked as invalid.  Normally the points
//  are collapsed before stabilizering, marks a flaw in the program logic.

#define STABILIZER_ERROR_STABILIZER_TOO_FEW_TRACKPOINTS -8505
//  At least 1 tracking point must be defined on a frame.

#define STABILIZER_ERROR_STABILIZER_TOO_MANY_TRACKPOINTS -8506
//  At most 2 tracking points must be defined on a frame.

#define STABILIZER_ERROR_CANT_SPAWN_THREAD -8507
//  Failed to start a processing thread

#define STABILIZER_INTERNAL_ERROR -8508
//  Don't know exactly what the error was

#endif
