//---------------------------------------------------------------------------

#ifndef TrackChartUnitH
#define TrackChartUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Chartfx3.hpp>
#include <OleCtrls.hpp>

#include "TrackingPoints.h"
#include <ExtCtrls.hpp>
#include <Chart.hpp>
#include <Graphics.hpp>
#include <Series.hpp>
#include <TeEngine.hpp>
#include <TeeProcs.hpp>
#include <VclTee.TeeGDIPlus.hpp>
//---------------------------------------------------------------------------
class TTrackChartForm : public TForm
{
__published:	// IDE-managed Components
        TChart *VTrackGraph;
        TLineSeries *Series20;
        TLineSeries *Series21;
        TChart *HTrackGraph;
        TLineSeries *LineSeries1;
        TLineSeries *LineSeries2;
private:	// User declarations
       int _Tag;
       CTrackingPoints _TrackingPoints;
       CTrackingPoints _SmoothedPoints;
       CTrackingArray *_TrackingArray;
       CTrackingArray *_SmoothedArray;

       valarray<double> RLum;
       valarray<double> GLum;
       valarray<double> VLum;

       double FindExtentX(void);
       double FindExtentY(void);

public:		// User declarations
        __fastcall TTrackChartForm(TComponent* Owner);
        void SetTrackTag(int Tag);
        void SetTrackingArray(CTrackingArray *newTrackingArray, CTrackingArray *newSmoothedArray);
};
//---------------------------------------------------------------------------
extern PACKAGE TTrackChartForm *TrackChartForm;
//---------------------------------------------------------------------------
#endif
