//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "TestUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//#pragma link "CSPIN"
#pragma resource "*.dfm"
#include <stdio.h>
#include <sstream>
#include "TrackingPoints.h"
#include <fstream>
#include "tarray.h"
#include "TrackChartUnit.h"
#include "MathFunctions.h"
#include "ZoomFractional.h"
#include "StabilizerCoreUnit.h"

using namespace std;

enum eMoveLine {mlNone, mlTopLeft, mlBottomRight, mlMagnify};

TForm1 *Form1;


float (*UEnergy)(float X0, float Y0, float X1, float Y1);

float UEnergyPoint(const DPOINT &iDP, const DPOINT &jDP)
{
   return UEnergy(iDP.x, iDP.y, jDP.x, jDP.y);
}
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
   nPicRow = 0;
   nPicCol = 0;
   _CurrentImageIndex = 0;
   _NextImageIndex = 1;
   _OneToOneMode = false;
   _MoveSmoothPoint = -1;
   _MoveLine = mlNone;
    UEnergy = UEuclidean;
   _isStabilizering = false;
   _TrackBoxData = NULL;
   _SearchBoxData = NULL;

   ReadParameters(CPMPIniFileName());
   SetTrackBoxExtent(StabilizerParameters.TrackBoxExtent);
   SetSearchBoxExtent(StabilizerParameters.SearchBoxExtent);

   pOrigBitmap = new Graphics::TBitmap();
   pStabilizerBitmap = new Graphics::TBitmap();

   // This will be the magnified bitmap
   pMagnifyBitmap = new Graphics::TBitmap();
   pMagnifyBitmap->PixelFormat = pf32bit;
   pMagnifyBitmap->Width = 41;
   pMagnifyBitmap->Height = 41;

   pBitmap = pOrigBitmap;

   // Update the GUI
   if (StabilizerParameters.Alpha2 == 0)
     SmoothTrackBar->Position = SmoothTrackBar->Max;
   else if (StabilizerParameters.Alpha2 < 0)
     SmoothTrackBar->Position = SmoothTrackBar->Min;
   else
       SmoothTrackBar->Position = (log(StabilizerParameters.Alpha2)/log(10.0)/(-0.75) + 1.5);
//     SmoothTrackBar->Position = (-log(StabilizerParameters.Alpha2 - 0.75)/log(10.0) + 1.5);


   UFunctionComboBox->Items->Clear();
   for (int i=0; i < UFunctionAssociations.Size(); i++)
     UFunctionComboBox->Items->Add(UFunctionAssociations.DisplayNameByIndex(i).c_str());

   UFunctionComboBox->ItemIndex = UFunctionAssociations.Index(StabilizerParameters.UFunction);

}
        __fastcall TForm1::~TForm1(void)
{
}
       bool TForm1::ReadParameters(const string &strFileName)
{
   StabilizerParameters.ReadParameters(strFileName);
   CIniFile *ini = CreateIniFile(strFileName);
   _MRU.ReadSettings(ini, "FilePrefix", "Prefix");
   if (!_MRU.empty())
     {
       FileNameEdit->Text = _MRU[0].c_str();
     }

   return DeleteIniFile(ini);
}

       bool TForm1::WriteParameters(const string &strFileName)
{
   StabilizerParameters.WriteParameters(strFileName);

   CIniFile *ini = CreateIniFile(strFileName);
   _MRU.WriteSettings(ini, "FilePrefix", "Prefix");
   return DeleteIniFile(ini);
}


void TForm1::SetTrackBoxExtent(int nTrackBoxExtent)
{
   if ((StabilizerParameters.TrackBoxExtent == nTrackBoxExtent) && (_TrackBoxData != NULL))return;

   StabilizerParameters.TrackBoxExtent = nTrackBoxExtent;
   _TrackBoxSize = 2*StabilizerParameters.TrackBoxExtent+1;
   _TrackBoxPoints = _TrackBoxSize*_TrackBoxSize;

   // Boxes must be bigger because we need point to perform a binlinear interpolation
   int nTmp = 2*(StabilizerParameters.TrackBoxExtent+1) + 1;
   delete [] _TrackBoxData;
   _TrackBoxData = new Pixel[nTmp*nTmp];
   TrackSEdit->Value = nTrackBoxExtent;
}

void TForm1::SetSearchBoxExtent(int nSearchBoxExtent)
{
   if ((StabilizerParameters.SearchBoxExtent == nSearchBoxExtent) && (_SearchBoxData != NULL)) return;

   StabilizerParameters.SearchBoxExtent = nSearchBoxExtent;
   _SearchBoxSize = 2*StabilizerParameters.SearchBoxExtent+1;
   _SearchBoxPoints = _SearchBoxSize*_SearchBoxSize;

   // Boxes must be bigger because we need point to perform a binlinear interpolation
   int nTmp = 2*(StabilizerParameters.SearchBoxExtent+1) + 1;
   delete [] _SearchBoxData;
   _SearchBoxData = new Pixel[nTmp*nTmp];
   SearchSEdit->Value = nSearchBoxExtent;
}
//---------------------------------------------------------------------------
#define U32 unsigned int
#define U16 unsigned short
#define U8 unsigned char
#define R32 float

typedef struct file_information
{
    U32   magic_num;        /* magic number 0x53445058 (SDPX) or 0x58504453 (XPDS) */
    U32   offset;           /* offset to image data in bytes */
    char  vers[8];          /* which header format version is being used (v1.0)*/
    U32   file_size;        /* file size in bytes */
    U32   ditto_key;        /* read time short cut - 0 = same, 1 = new */
    U32   gen_hdr_size;     /* generic header length in bytes */
    U32   ind_hdr_size;     /* industry header length in bytes */
    U32   user_data_size;   /* user-defined data length in bytes */
    char  file_name[100];   /* iamge file name */
    char  create_time[24];  /* file creation date "yyyy:mm:dd:hh:mm:ss:LTZ" */
    char  creator[100];     /* file creator's name */
    char  project[200];     /* project name */
    char  copyright[200];   /* right to use or copyright info */
    U32   key;              /* encryption ( FFFFFFFF = unencrypted ) */
    char  Reserved[104];    /* reserved field TBD (need to pad) */
} FileInformation;

typedef struct _image_information
{
    U16    orientation;          /* image orientation */
    U16    element_number;       /* number of image elements */
    U32    pixels_per_line;      /* or x value */
    U32    lines_per_image_ele;  /* or y value, per element */
    struct _image_element
    {
        U32    data_sign;        /* data sign (0 = unsigned, 1 = signed ) */
				 /* "Core set images are unsigned" */
        U32    ref_low_data;     /* reference low data code value */
        R32    ref_low_quantity; /* reference low quantity represented */
        U32    ref_high_data;    /* reference high data code value */
        R32    ref_high_quantity;/* reference high quantity represented */
        U8     descriptor;       /* descriptor for image element */
        U8     transfer;         /* transfer characteristics for element */
        U8     colorimetric;     /* colormetric specification for element */
        U8     bit_size;         /* bit size for element */
	U16    packing;          /* packing for element */
        U16    encoding;         /* encoding for element */
        U32    data_offset;      /* offset to data of element */
        U32    eol_padding;      /* end of line padding used in element */
        U32    eo_image_padding; /* end of image padding used in element */
        char   description[32];  /* description of element */
    } image_element[8];          /* NOTE THERE ARE EIGHT OF THESE */

    U8 reserved[52];             /* reserved for future use (padding) */
} ImageInformation;;


void Reverse16(U16 &Value)
{
   unsigned char *ucp = (unsigned char *)&Value;
   unsigned char uc = ucp[0];
   ucp[0] = ucp[1];
   ucp[1] = uc;
}

void Reverse32(U32 &Value)
{
   unsigned char *ucp = (unsigned char *)&Value;
   U32 V2;
   unsigned char *ucV2 = (unsigned char *)&V2;
   ucV2[0] = ucp[3];
   ucV2[1] = ucp[2];
   ucV2[2] = ucp[1];
   ucV2[3] = ucp[0];
   Value = V2;
}

void Reverse32(R32 &Value)
{
   U32 *u32p = (U32 *)&Value;
   Reverse32(*u32p);
}


using namespace std;
#define KB_SIZE 1024

const char *commaStr(unsigned long number)
{
  const int size = 30;    // max number of digits
  static char str[size];  // string to return
  char *ptr1, *ptr2;      // place holders
  char tempStr[size];     // workplace
  int counter = 0;        // three's counter
  memset(str, 0, size);
  ptr1 = tempStr;
  do {
    // grab rightmost digit and add value of character zero
    *ptr1++ = (char)(number % 10) + '0';
    // strip off rightmost digit
    number /= 10;
    // if moved over three digits insert comma into string
    if (number &&  !(++counter % 3))
      *ptr1++ = ',';
    // continue until number equal zero
  } while(number);
  // this loop reverses characters in a string
  for( --ptr1, ptr2 = str; ptr1 >= tempStr; --ptr1)
    *ptr2++ = *ptr1;
  // add the zero string terminator
  *ptr2 = '\0';
  return str;
}


typedef vector <PROCESS_HEAP_ENTRY> HeapEntryVector;

typedef vector <PROCESS_HEAP_ENTRY> HeapEntryVector;
  unsigned long totalBytes = 0;
  unsigned long committedBytes = 0;
  unsigned long uncommittedBytes = 0;

int ShowHeap(HANDLE pHeap)
{

  // Traversing a Heap blocks by using a HeapWalk API.
  HeapEntryVector heapEntryVec;
  PROCESS_HEAP_ENTRY pHeapEntry;

  // Initialize data members of PROCESS_HEAP_ENTRY to Zero.
  ZeroMemory(&pHeapEntry,sizeof(pHeapEntry));

  pHeapEntry.lpData = NULL;
//  unsigned long totalBytes = 0;
//  unsigned long committedBytes = 0;
//  unsigned long uncommittedBytes = 0;

  // This loop will be iterating all the blocks of the memory and will be
  // Calculating the committed and uncommitted memory for a heap.

  BOOL bFirstBlock = false;

  while (BOOL bWalk = HeapWalk(pHeap,&pHeapEntry))
  {
    heapEntryVec.push_back(pHeapEntry);
  }

  HeapEntryVector::iterator iterHeapEntry = heapEntryVec.begin();
  int pos = 0;
  for (iterHeapEntry; iterHeapEntry != heapEntryVec.end();
                      iterHeapEntry++, pos++)
  {
    PROCESS_HEAP_ENTRY tempHeapEntry = (PROCESS_HEAP_ENTRY)(*iterHeapEntry);
    // This represents the REGION block,
    // which will containing all the details about
    // committed block and uncommitted block present in a Heap.

    if (!bFirstBlock)
    {
      totalBytes += (unsigned long) tempHeapEntry.lpData -
                   (unsigned long) pHeap;
      totalBytes += (unsigned long) tempHeapEntry.Region.lpFirstBlock -
                    (unsigned long)tempHeapEntry.lpData;
      committedBytes += (unsigned long)tempHeapEntry.Region.lpFirstBlock -
                        (unsigned long)tempHeapEntry.lpData;
      committedBytes += (unsigned long)tempHeapEntry.lpData -
                        (unsigned long) pHeap;
      bFirstBlock = true;
    }

    // This represents the allocated blocks in a heap.
    // The amount of bytes in the block is obtained
    // by subtracting the starting address [virtual address]
    // of the next block from the
    // starting address [ virtual address ] of the present block.
    if (tempHeapEntry.wFlags == PROCESS_HEAP_ENTRY_BUSY)
      if ((unsigned)pos < heapEntryVec.size()-1)
        {
          unsigned long bytesAllocated =
             (unsigned long) heapEntryVec[pos + 1].lpData -
             (unsigned long) tempHeapEntry.lpData;
          totalBytes += bytesAllocated;
          committedBytes += bytesAllocated;
        }
      else
        {
          unsigned long bytesAllocated =
             (unsigned long) heapEntryVec[pos + 1].lpData -
             (unsigned long) tempHeapEntry.lpData;
          totalBytes += bytesAllocated;
          committedBytes += bytesAllocated;
        }

    // This represents the committed block which
    // is free, i.e. not being allocated or not being used
    // as control structure. Data member cbData represents
    // the size in bytes for this range of free block.

    if (tempHeapEntry.wFlags == 0)
    {
      totalBytes += tempHeapEntry.cbData;
      committedBytes += tempHeapEntry.cbData;
    }

    // For Uncommitted block, cbData represents the size
    // (in bytes) for range of uncommitted memory.
    //
    if (tempHeapEntry.wFlags == PROCESS_HEAP_UNCOMMITTED_RANGE)
    {
      uncommittedBytes += tempHeapEntry.cbData;
      totalBytes += tempHeapEntry.cbData;
    }
  }// end of for loop

  return 0;

}
int ShowHeap()
{


  totalBytes = 0;
  committedBytes = 0;
  uncommittedBytes = 0;

  HANDLE ProcessHeaps[1000];
  int nH = GetProcessHeaps(1000, ProcessHeaps);
  for (int i =0; i < nH; i++)
    ShowHeap(ProcessHeaps[i]);

  ostringstream os;
//  os << "Heap entries: " << heapEntryVec.size() << endl;
  os<<"Total KBytes in Heap::" << commaStr(totalBytes) <<endl;
  os<<"Total Committed KBytes in Heap::" << commaStr(committedBytes >> 10) <<endl;
  os<<"Total UnCommitted KBytes in Heap::"<< commaStr(uncommittedBytes >> 10) <<endl;
//  os << "Handle: " << hex << pHeap << endl;
  ShowMessage(os.str().c_str());
  return 0;
}

bool TForm1::ReadDPXM(String sfname, int nStartFrame)
{
// Reads the file into _CurrentIndex+1
   String fname = sfname + Format("%0.6d.dpx",ARRAYOFCONST((nStartFrame)));
   _CurrentFileName = fname;

   FILE *in = fopen(fname.c_str(), "rb");
   if (in == NULL)
     {
       ShowMessage(fname + " opened failed");
       return false;
     }
   FileInformation Header;
   ImageInformation Info;

   fread(&Header, 1, sizeof(Header),  in);
   fread(&Info, 1, sizeof(Info),  in);

   // Hard code these
   _pxlComponentCount = 3;
   _BlackPixel[0] = 0;
   _BlackPixel[1] = 0;
   _BlackPixel[2] = 0;

   _WhitePixel[0] = 1023;
   _WhitePixel[1] = 1023;
   _WhitePixel[2] = 1023;

   if (Header.magic_num == 0x58504453)
     {
       Reverse32(Header.magic_num);
       Reverse32(Header.offset);
       Reverse32(Header.file_size);        /* file size in bytes */
       Reverse32(Header.ditto_key);        /* read time short cut - 0 = same, 1 = new */
       Reverse32(Header.gen_hdr_size);     /* generic header length in bytes */
       Reverse32(Header.ind_hdr_size);     /* industry header length in bytes */
       Reverse32(Header.user_data_size);

       Reverse16(Info.orientation);
       Reverse16(Info.element_number);
       Reverse32(Info.pixels_per_line);
       Reverse32(Info.lines_per_image_ele);


       Reverse32(Info.image_element[0].data_sign);
       Reverse32(Info.image_element[0].ref_low_data);
       Reverse32(Info.image_element[0].ref_high_data);
       Reverse32(Info.image_element[0].ref_low_quantity);
       Reverse32(Info.image_element[0].ref_high_quantity);

       Reverse16(Info.image_element[0].packing);
       Reverse16(Info.image_element[0].encoding);
       Reverse32(Info.image_element[0].data_offset);
     }

     // Check to see if we have a valid header
   if (Header.magic_num != 0x53445058) throw;

   fseek(in, Header.offset, SEEK_SET);

// DANGEROUS CODE
   if ((Info.pixels_per_line != (unsigned)nPicCol) || (Info.lines_per_image_ele != (unsigned)nPicRow))
     {
        delete[] _RawData[0];
        delete[] _RawData[1];

        delete[] _RGBData[0];
        delete[] _RGBData[1];

        delete[] _FixedRGBData[0];
        delete[] _FixedRGBData[1];

        nPicRow = Info.lines_per_image_ele;
        nPicCol = Info.pixels_per_line;
        _RawData[0] = new Pixel[nPicRow*nPicCol];
        _RawData[1] = new Pixel[nPicRow*nPicCol];

        _RGBData[0] = new Pixel[nPicRow*nPicCol*_pxlComponentCount];
        _RGBData[1] = new Pixel[nPicRow*nPicCol*_pxlComponentCount];

        _FixedRGBData[0] = new Pixel[nPicRow*nPicCol*_pxlComponentCount];
        _FixedRGBData[1] = new Pixel[nPicRow*nPicCol*_pxlComponentCount];

        _CurrentImageIndex = 0;
        _NextImageIndex = 1;
     }
   else
     {
       _NextImageIndex = _CurrentImageIndex;
       _CurrentImageIndex = (_CurrentImageIndex + 1) %2;
     }

    Image1->Height = ((Image1->Width*nPicRow+0.5)/nPicCol);
    Height = Image1->Height + Image1->Top + 100;

#define STRD 4
   unsigned N = STRD*Info.pixels_per_line;
   unsigned char *paSrc = (unsigned char *)malloc(N);

   Image RGBBuffer = _RGBData[_CurrentImageIndex];

   for (int i=0; i < nPicRow; i++)
     {
        if (fread(paSrc, 1, N, in) != N) throw;
        for (int j = 0; j < STRD*nPicCol; j+=STRD)
          {
            unsigned a1 = paSrc[j];
            unsigned a2 = paSrc[j+1];
            unsigned a3 = paSrc[j+2];
            unsigned a4 = paSrc[j+3];
            int R = (a1 << 2) + (a2 >> 6);
            int G = ((0x3F & a2) << 4) + (a3 >> 4);
            int B = ((0x0F & a3) << 6) + (a4 >> 2);
            *RGBBuffer++ = R;
            *RGBBuffer++ = G;
            *RGBBuffer++ = B;
          }
     }

   free(paSrc);
   fclose(in);

   return true;
}

bool TForm1::WriteDPXM(String sfname, Image &imgA, int nStartFrame)
{
   String fname = sfname + Format("%0.4d.dpx",ARRAYOFCONST((nStartFrame)));

   FILE *in = fopen(fname.c_str(), "rb");
   if (in == NULL)
     {
       ShowMessage(fname + " opened failed");
       return false;
     }

   // Find the header size
   FileInformation Header;
   fread(&Header, 1, sizeof(Header),  in);
   if (Header.magic_num == 0x58504453)
     {
       Reverse32(Header.offset);
     }

   unsigned char *upc = new unsigned char[Header.offset];
   fseek(in, 0, SEEK_SET);
   fread(upc, 1, Header.offset,  in);
   fclose(in);

   // Now write out new file
   String foutname = ExtractFilePath(sfname) + "FX-" + ExtractFileName(sfname) + Format("%0.4d.dpx",ARRAYOFCONST((nStartFrame)));
   FILE *out = fopen(foutname.c_str(), "wb");
   fwrite(upc, 1, Header.offset,  out);
   delete[] upc;

   // Now write the image
   Image RGBBuffer = imgA;
   unsigned char a[4];

   for (int i=0; i < nPicRow*nPicCol; i++)
     {
            unsigned R = *RGBBuffer++;
            unsigned G = *RGBBuffer++;
            unsigned B = *RGBBuffer++;

            a[0] = (R >> 2) & 0xFF;
            a[1] = ((R << 8) & 0xC0) + ((G >> 4) & 0x3F);
            a[2] = ((G << 6) & 0xF0) + ((B >> 6) & 0x0F);
            a[3] = ((B << 2) & 0xFC);
            fwrite(a, 1, 4, out);
//            int R = (a1 << 2) + (a2 >> 6);
//            int G = ((0x3F & a2) << 4) + (a3 >> 4);
//            int B = ((0x0F & a3) << 6) + (a4 >> 2);
     }

   fclose(out);

   return true;
}

// Take Image coordinates and make it picture coordinates
int TForm1::VCol(int X)
{
    if (_OneToOneMode) return X - _XOneToOne;
    return (nPicCol - 1.0)*X/(Image1->Width-1.0);
}
int TForm1::VRow(int Y)
{
    if (_OneToOneMode) return Y - _YOneToOne;
    return (nPicRow - 1.0)*Y/(Image1->Height-1.0);
}

// Take picture coordinates and make it image coordinates
int TForm1::ICol(int X)
{
    if (_OneToOneMode) return X + _XOneToOne;
    return (Image1->Width - 1.0)*X/(nPicCol - 1.0);
}
int TForm1::IRow(int Y)
{
    if (_OneToOneMode) return Y  + _YOneToOne;
    return (Image1->Height - 1.0)*Y/(nPicRow - 1.0);
}

void __fastcall TForm1::LoadButtonClick(TObject *Sender)
{
    ReadPoints();
    LoadImage(FileNameEdit->Text, StartSpinEdit->Value);
    UpdateTrackMemo();
}

void TForm1::BitmapFromImage(Graphics::TBitmap *Bitmap, Image pImage, Image mImage)
{
    Bitmap->PixelFormat = pf32bit;
    Bitmap->Width = nPicCol;
    Bitmap->Height = nPicRow;

    for (int y = 0; y < Bitmap->Height; y++)
    {
      Byte *ptr = (Byte *)Bitmap->ScanLine[y];
      Image ImageLine = mImage + y*nPicCol;
      for (int x = 0; x < Bitmap->Width; x++)
       {
         Pixel *p = pImage + _pxlComponentCount*(y*nPicCol + x);
         int R = min(*p++/4, 255);
         int G = min(*p++/4, 255);
         int B = min(*p++/4, 255);

         ptr[4*x] = (Byte)B;
         ptr[4*x+1] = (Byte)G;
         ptr[4*x+2] = (Byte)R;
         ptr[4*x+3] = (Byte)0;
         ImageLine[x] = (R + G + B)*0.333333;
       }
    }

}

bool  TForm1::LoadImage(String fname, int Frame)
{
    _CurrentFrame = Frame;

    _HRT.Start();
    if (!ReadDPXM(fname, _CurrentFrame)) return false;
    double dReadTime = _HRT.Read();


    _HRT.Start();

    BitmapFromImage(pBitmap, _RGBData[_CurrentImageIndex], _RawData[_CurrentImageIndex]);

    Image1->Picture->Bitmap->Width = Image1->Width;
    Image1->Picture->Bitmap->Height = Image1->Height;
    double dDisplayTime = _HRT.Read();

    StatusBar1->Panels->Items[0]->Text = Format("Read time: %4.1f; Display time: %4.1f",
                                     ARRAYOFCONST((dReadTime, dDisplayTime)));

    return true;
}

//---------------------------------------------------------------------------
void __fastcall TForm1::Image1MouseMove(TObject *Sender, TShiftState Shift,
      int X, int Y)
{
    if (nPicRow <= 0) return;
    PositionLabel->Caption = Format("(%4d,%4d)",ARRAYOFCONST((VCol(X),VRow(Y))));
    _VX = VCol(X);
    _VY = VCol(Y);

    if (_MoveSmoothPoint >= 0)
      {
         if ((_SmoothedArray[_CurrentFrame][_MoveSmoothPoint].x != VCol(X)) ||
             (_SmoothedArray[_CurrentFrame][_MoveSmoothPoint].y != VCol(Y)))

           {
             _SmoothedArray[_CurrentFrame][_MoveSmoothPoint].x = VCol(X);
             _SmoothedArray[_CurrentFrame][_MoveSmoothPoint].y = VCol(Y);
             UpdateDisplay();
           }
      }

    switch (_MoveLine)
      {
        case mlTopLeft:
          _BoundingBox.top = VRow(Y);
          _BoundingBox.left = VCol(X);
          UpdateDisplay();
          break;

        case mlBottomRight:
          _BoundingBox.right = VCol(X);
          _BoundingBox.bottom = VRow(Y);
          UpdateDisplay();
          break;

        case mlMagnify:
          UpdateDisplay();
          Magnify(X,Y);
          break;

      }

}
//---------------------------------------------------------------------------

void TForm1::UpdateTrackMemo(void)
{
    // See if we are valid
    if (nPicRow == 0) return;

    TrackPointMemo->Lines->BeginUpdate();
    TrackPointMemo->Clear();

    for (unsigned i=0; i < _TrackingArray[_CurrentFrame].size(); i++)
      if (_TrackingArray[_CurrentFrame][i].isValid())
        {
          double x = _TrackingArray[_CurrentFrame][i].x;
          double y = _TrackingArray[_CurrentFrame][i].y;
          String S = Format("%2.0f: (%7.2f,%7.2f)",ARRAYOFCONST((1.0*i,x,y)));

          if (_SmoothedArray[_CurrentFrame][i].isValid())
          {
            x = _SmoothedArray[_CurrentFrame][i].x;
            y = _SmoothedArray[_CurrentFrame][i].y;
            S +=  Format("  (%7.2f,%7.2f)",ARRAYOFCONST((x,y)));
           }
         TrackPointMemo->Lines->Add(S);
       }
    TrackPointMemo->Lines->EndUpdate();

    FrameLabel->Caption =  Format("Frame: %04d",ARRAYOFCONST((_CurrentFrame)));
    Caption = _CurrentFileName + ";  " + FrameLabel->Caption;

   Image1->Canvas->CopyMode = cmSrcCopy;
   if (pBitmap == pOrigBitmap)
     TypeLabel->Caption = "Original";
   else
     TypeLabel->Caption = "Stabilizered";

   UpdateDisplay();

}

void TForm1::UpdateDisplay(void)
{
    // See if we are valid
    if (nPicRow == 0) return;

    if (_OneToOneMode)
      Image1->Picture->Bitmap->Canvas->Draw(_XOneToOne, _YOneToOne, pBitmap);
    else
      Image1->Picture->Bitmap->Canvas->StretchDraw(Image1->ClientRect,pBitmap);

    Image1->Canvas->Brush->Color = clRed;
    Image1->Canvas->Pen->Color = clRed;
    Image1->Canvas->CopyMode = cmSrcCopy;

    for (unsigned i=0; i < _TrackingArray[_CurrentFrame].size(); i++)
      if (_TrackingArray[_CurrentFrame][i].isValid())
        {
          double x = _TrackingArray[_CurrentFrame][i].x;
          double y = _TrackingArray[_CurrentFrame][i].y;

          int tbe = StabilizerParameters.TrackBoxExtent;
          TRect r(ICol(x-tbe), IRow(y-tbe), ICol(x+tbe), IRow(y+tbe));
          Image1->Canvas->Rectangle(r);

          int sbe = StabilizerParameters.SearchBoxExtent;
          TRect re(ICol(x-sbe), IRow(y-sbe), ICol(x+sbe), IRow(y+sbe));
          Image1->Canvas->FrameRect (re);
        }

    Image1->Canvas->Brush->Color = clBlue;
    Image1->Canvas->Pen->Color = clBlue;

    for (unsigned i=0; i < _SmoothedArray[_CurrentFrame].size(); i++)
      if (_SmoothedArray[_CurrentFrame][i].isValid())
        {
          double x = _SmoothedArray[_CurrentFrame][i].x;
          double y = _SmoothedArray[_CurrentFrame][i].y;

          int tbe = StabilizerParameters.TrackBoxExtent;
          TRect r(ICol(x-tbe), IRow(y-tbe), ICol(x+tbe), IRow(y+tbe));
          Image1->Canvas->Rectangle(r);

          int sbe = StabilizerParameters.SearchBoxExtent;
          TRect re(ICol(x-sbe), IRow(y-sbe), ICol(x+sbe), IRow(y+sbe));
          Image1->Canvas->FrameRect(re);
        }

     // Display the boxes
     TRect dspBox;
     dspBox.left = ICol(_BoundingBox.left);
     dspBox.Right = ICol(_BoundingBox.right);
     dspBox.top = IRow(_BoundingBox.top);
     dspBox.bottom = IRow(_BoundingBox.bottom);

     Image1->Canvas->Brush->Color = clBlue;
     Image1->Canvas->FrameRect(dspBox);

          // Display the boxes
     Image1->Canvas->Brush->Color = clYellow;
     dspBox.left = ICol(_StabilizerBox.left);
     dspBox.Right = ICol(_StabilizerBox.right);
     dspBox.top = IRow(_StabilizerBox.top);
     dspBox.bottom = IRow(_StabilizerBox.bottom);
//     Image1->Canvas->FrameRect(dspBox);

}

void __fastcall TForm1::Image1MouseUp(TObject *Sender, TMouseButton Button,
      TShiftState Shift, int X, int Y)
{
   if (_MoveLine == mlMagnify)
    {
      _MoveLine = mlNone;
      return;
    }

   if (Button == mbLeft)
     {
       if ((_MoveSmoothPoint < 0) && (_MoveLine == mlNone))
         _TrackingArray[_CurrentFrame].add(VCol(X),VRow(Y));
     }
   else if (Button == mbRight)
     {
       int iX = VCol(X);
       int iY = VRow(Y);
       for (unsigned i=0; i < _TrackingArray[_CurrentFrame].size(); i++)
         if (_TrackingArray[_CurrentFrame][i].isValid())
           {
             int x = _TrackingArray[_CurrentFrame][i].x;
             int y = _TrackingArray[_CurrentFrame][i].y;
             if ((abs(x-iX) + abs(y -iY)) < 40)
               {
                  _TrackingArray[_CurrentFrame].erase(i);
                  break;
               }
           }

     }
   else if (Button == mbMiddle)
     {
        // The mode will switch but we need the actual point before
        // the switch
        if (_OneToOneMode)
          {
            _XOneToOne = 0;
            _YOneToOne = 0;
          }
        else
          {
            _XOneToOne = -VCol(X);
            _YOneToOne = -VRow(Y);
          }

        _OneToOneMode = ! _OneToOneMode;
     }
   else
     {
        return;
     }

   // Done Moving
   _MoveSmoothPoint = -1;
   _MoveLine = mlNone;
   UpdateTrackMemo();
}
//---------------------------------------------------------------------------

  int TForm1::L1BoxNorm(const DPOINT &tp, int iO, int jO)
{
   int nR = 0;
   // Copy over the track box and then the search range

   // Set up the pointers
   int tbe = StabilizerParameters.TrackBoxExtent;
   Pixel *pB = _BaseImage + ((int)tp.y - tbe)*nPicCol + (int)tp.x - tbe;
   Pixel *pN = _NextImage + (jO + (int)tp.y)*nPicCol + iO + (int)tp.x;

   // Now compute them
   for (int i = -tbe; i <= tbe; i++)
       {
         nR += L1Norm(pB, pN, _TrackBoxSize);
         pB += nPicCol;
         pN += nPicCol;
       }

   return nR;
}
/*
 int TForm1::L1BoxNorm(const DPOINT &tp, int iO, int jO)
{
   int nR = 0;

   // Set up the pointers
   Pixel *pB = _BaseImage + ((int)tp.y -_TrackBoxExtent)*nPicCol + (int)tp.x - _TrackBoxExtent;
   Pixel *pN = _NextImage + (jO + (int)tp.y  -_TrackBoxExtent)*nPicCol + iO + (int)tp.x - _TrackBoxExtent;
   int nSize = 2*_TrackBoxExtent + 1;

   // Now compute them
   for (int i = -_TrackBoxExtent; i <= _TrackBoxExtent; i++)
       {
         nR += L1Norm(pB, pN, nSize);
         pB += nPicCol;
         pN += nPicCol;
       }

   return nR;
}

*/


// This function extracts an image with center at point tp,
 DPOINT TForm1::FindBestFirstMatch(const DPOINT &tp)
{
   DPOINT tpR;
   int tbe = StabilizerParameters.TrackBoxExtent;
   int sbe = StabilizerParameters.SearchBoxExtent;

   CopySubImage(_BaseImage, nPicCol, nPicRow, tp.x, tp.y, tbe, _TrackBoxData);
   CopySubImage(_NextImage, nPicCol, nPicRow, tp.x, tp.y, sbe, _SearchBoxData);

   // The largest this can be is
   int minL1 = (_TrackBoxPoints << 16);

   // We first start tracking the upper left corner which is (-TrackBoxExtent, -TrackBoxExtent)
   // from the actual track point
   for (int i = -sbe; i <= sbe - 2*tbe; i++)
     for (int j = -sbe; j <= sbe - 2*tbe; j++)
       {
         double L1 = ComputeL1BoxNorm(_TrackBoxData, _TrackBoxSize, _SearchBoxData, _SearchBoxSize, i+sbe, j+sbe);
         if (L1 < minL1)
           {
              minL1 = L1;
              // Restore the proper track box extent
              tpR.x = tp.x + i + tbe;
              tpR.y = tp.y + j + tbe;
           }
       }

   return tpR;
}



 DPOINT TForm1::FindBestRefinedMatch(const DPOINT &dpB, Pixel *pBase, const DPOINT &dpN, Pixel *pNext)
{
   DPOINT tpR;
   int tbe = StabilizerParameters.TrackBoxExtent;
   int sbe = StabilizerParameters.SearchBoxExtent;

   int BSearchBoxExtent = tbe + 1;
   int nBase = 4*_TrackBoxSize;
   int nNext = 4*(2*BSearchBoxExtent+1);
   int iF, jF;

   CopySubImage(_BaseImage, nPicCol, nPicRow, dpB.x, dpB.y, tbe+1, _TrackBoxData);
   CopySubImage(_NextImage, nPicCol, nPicRow, dpN.x, dpN.y, sbe+1, _SearchBoxData);

   CreateSubPixelMatrix2(_TrackBoxData,  2*tbe+3, tbe+1, tbe+1, tbe, pBase);
   CreateSubPixelMatrix2(_SearchBoxData, 2*sbe+3, sbe+1, sbe+1, BSearchBoxExtent, pNext);

   double minL1 = 1024*8*8*BSearchBoxExtent*BSearchBoxExtent;
   for (int i = -4; i <= 3; i++)
     for (int j = -4; j <= 3; j++)
       {
         double L1 = ComputeL1BoxNorm(pBase, nBase, pNext, nNext,i+4, j+4);
         if (L1 < minL1)
           {
              minL1 = L1;
              // The 1 comes from -4/4 the extent of the sub image
              tpR.x = dpN.x + i/4.0 + 1;
              tpR.y = dpN.y + j/4.0 + 1;
              iF = i;
              jF = j;
           }
       }

   tpR.x = dpN.x + iF/4.0;
   tpR.y = dpN.y + jF/4.0;
   return tpR;

}
void TForm1::TrackNextFrame(void)
{
  // Advance a frame, Note: A load must be called first
  _BaseImage =_RawData[_CurrentImageIndex];
  _BaseFrame = _CurrentFrame;

  LoadImage(FileNameEdit->Text, _CurrentFrame+1);

  _NextImage = _RawData[_CurrentImageIndex];
  _TrackingArray[_BaseFrame+1].clear();

  // Allocate some storage
  _HRT.Start();
  int tbe = StabilizerParameters.TrackBoxExtent;

  int nBase = 4*(2*tbe + 1);
  int nNext = 4*(2*(tbe+1) + 1);
  Pixel *pRBase = new Pixel[nBase*nBase];
  Pixel *pRNext = new Pixel[nNext*nNext];

  for (unsigned i=0; i < _TrackingArray[_BaseFrame].size(); i++)
    if (_TrackingArray[_BaseFrame][i].isValid())
      {
         DPOINT dp = FindBestFirstMatch(_TrackingArray[_BaseFrame][i]);
         dp = FindBestRefinedMatch(_TrackingArray[_BaseFrame][i], pRBase, dp, pRNext);
         _TrackingArray[_CurrentFrame][i] = dp;
      }

  // Display times
  double dTrackTime = _HRT.Read();
  double dPerPointTrackTime = 0;
  if (_TrackingArray[_CurrentFrame].getCountOfValidPoints() > 0)
    {
      dPerPointTrackTime = dTrackTime/_TrackingArray[_CurrentFrame].getCountOfValidPoints();
    }

  StatusBar1->Panels->Items[1]->Text = Format("Total Track time: %4.1f, Per point %4.1f",
                                     ARRAYOFCONST((dTrackTime, dPerPointTrackTime)));

  delete[] pRBase;
  delete[] pRNext;

  UpdateTrackMemo();
}

void __fastcall TForm1::TrackButtonClick(TObject *Sender)
{
   LoadImage(FileNameEdit->Text, StartSpinEdit->Value);

   for (int i=StartSpinEdit->Value+1; i < EndSpinEdit->Value; i++)
    {
      TrackNextFrame();
      Application->ProcessMessages();
    }

}
//---------------------------------------------------------------------------



void TForm1::CreateSubPixelMatrix(Pixel *pIn, const DPOINT &dp, int N, Pixel *pOut)

// Computes the fixed (4 levels) subpixel matrix pOut from the position
//  in the base frame given by dp
//
//  Where pIn is apointer to the image
//  N is the extent of the source image (x-N, x+N) x (y-N, y+N)
//  pOut is 4 times bigger
//  pOut MUST exist
//
// Bilinear interpolation is done by
//  f(x,y) = ax + by + cxy + d
//  where 0 <= x,y <= 1 and the coefficients are given by
//  a = -C00 + C10
//  b = -C00 + C01
//  c =  C00 - C10 - C01 + C11
//  d =  C00
{
   // Set up the pointer
   Pixel *pR = pIn + ((int)dp.y - N)*nPicCol + (int)dp.x - N;
   Pixel *pO = pOut;
   int IntraColSize = 4*(2*N + 1);

   for (int iR = 0; iR < (2*N+1); iR++)
     {
       Pixel *p0 = pO;
       Pixel *p1 = p0 + IntraColSize;
       Pixel *p2 = p1 + IntraColSize;
       Pixel *p3 = p2 + IntraColSize;

       Pixel *pRI = pR;
       for (int iC = 0; iC < (2*N+1); iC++)
         {
            double C00 = *pRI;
            double C10 = *(pRI+1);
            double C01 = *(pRI + nPicCol);
            double C11 = *(pRI + nPicCol + 1);
            double a = -C00 + C10;
            double b = -C00 + C01;
            double c =  C00 - C10 - C01 + C11;
            double d =  C00;

            // y = 0
            *p0++ = d;
            *p0++ = 0.25*a + d;
            *p0++ = 0.50*a + d;
            *p0++ = 0.75*a + d;

            // y = .25
            *p1++ = 0.25*b + d;
            *p1++ = 0.25*a + 0.25*b + (0.25*0.25)*c + d;
            *p1++ = 0.50*a + 0.25*b + (0.50*0.25)*c + d;
            *p1++ = 0.75*a + 0.25*b + (0.75*0.25)*c + d;

            // y = .50
            *p2++ = 0.50*b + d;
            *p2++ = 0.25*a + 0.50*b + (0.25*0.50)*c + d;
            *p2++ = 0.50*a + 0.50*b + (0.50*0.50)*c + d;
            *p2++ = 0.75*a + 0.50*b + (0.75*0.50)*c + d;

            // y = .75
            *p3++ = 0.75*b + d;
            *p3++ = 0.25*a + 0.75*b + (0.25*0.75)*c + d;
            *p3++ = 0.50*a + 0.75*b + (0.50*0.75)*c + d;
            *p3++ = 0.75*a + 0.75*b + (0.75*0.75)*c + d;

             pRI += 1;
         }

       // Done with input row, continue to next input row
         pR += nPicCol;
         pO += 4*IntraColSize;
     }
}

void __fastcall TForm1::BackButtonClick(TObject *Sender)
{
  _BaseImage =_RawData[_CurrentImageIndex];
  LoadImage(FileNameEdit->Text, _CurrentFrame-1);
  _NextImage = _RawData[_CurrentImageIndex];
  UpdateTrackMemo();

}
//---------------------------------------------------------------------------

#define N 101


void __fastcall TForm1::Button1Click(TObject *Sender)
{
   int tbe = StabilizerParameters.TrackBoxExtent;

   int nBase = 4*(2*tbe + 1);
   int nNext = 4*(2*(tbe+1) + 1);
   Pixel *pBase = new Pixel[nBase*nBase];
   Pixel *pNext = new Pixel[nNext*nNext];
   int BSearchBoxExtent = tbe + 1;

   CreateSubPixelMatrix(_BaseImage, _TrackingArray[_BaseFrame][0], tbe, pBase);
   CreateSubPixelMatrix(_NextImage, _TrackingArray[_CurrentFrame][0], BSearchBoxExtent, pNext);

   int nSize = nNext;
   Pixel *p = pNext;

   pBitmap->Width = nSize;
   pBitmap->Height = nSize;
   for (int i=0; i < nSize; i++)
     for (int j=0; j < nSize; j++)
       {
         int v = *p++/4;
         pBitmap->Canvas->Pixels[j][i] = (TColor)(v + v*256 + v*256*256);
       }
   Image1->Picture->Bitmap->Canvas->Draw(20,20, pBitmap);
   Image1->Picture->Bitmap->Canvas->Draw(20,30+8*BSearchBoxExtent, pBitmap);

    nSize = nBase;
   p = pBase;

   pBitmap->Width = nSize;
   pBitmap->Height = nSize;
   for (int i=0; i < nSize; i++)
     for (int j=0; j < nSize; j++)
       {
         int v = *p++/4;
         pBitmap->Canvas->Pixels[j][i] = (TColor)(v + v*256 + v*256*256);
       }
  Image1->Picture->Bitmap->Canvas->Draw(24,24, pBitmap);


  for (int i=-4; i <= 4; i++)
    for (int j=-4; j <= 4; j++)
      {
         double d = ComputeL1BoxNorm(pBase, nBase, pNext, nNext, i, j);
         TrackPointMemo->Lines->Add(Format("(%4d,%4d) %10.0f",ARRAYOFCONST((i,j, d))));
      }

  delete[] pBase;
  delete[] pNext;
}
//---------------------------------------------------------------------------

typedef struct
  {
     double dMin;
     int    nLoc;
  }  SFcnValue;

typedef map<int, SFcnValue> CFcnValues;
typedef map<int, CFcnValues> AFcnValues;
typedef map<int, AFcnValues> CDPArray;


void Smooth(valarray<double> &MC)
{
  valarray<double> Ret(MC);
  int T = MC.size();
  vector<double> sa(5);

  // Fix two points on the end
  for (int k=2; k < T-2; k++)
    {
     for (int id=-2; id <= 2; id++)
       sa[id+2] = MC[k+id];

    // Bubble sort
    vector<double>::iterator i,j;
    for (i = sa.begin(); i != sa.end() ; i++)
        for (j = sa.begin(); j < i; j++)
            if (*i < *j)
                std::iter_swap(i, j);

     Ret[k] = sa[2];
    }

  // Hamming filter
  for (int k=1; k < T-1; k++)
     MC[k] = 0.25*Ret[k-1] + 0.50*Ret[k] + 0.25*Ret[k+1];

  for (int k=1; k < T; k++)
     MC[k] = MC[0];

}

void Smooth2(valarray<double> &MC, double Alpha)

{
   CDPArray DPA;

   double dMin = MC.min();

   valarray<double> Z(4.0*(MC - dMin));
   int L = Z.max();
   int T = Z.size();

   // Compute all the F and G functions
   double c0, c1, cm, mn;
   SFcnValue FV;
   // Create the first array
   for (int a=0; a < L; a++)
     for (int b=0; b < L; b++)
       {
          // Compute F(0,a,b)
          mn = (-2*a + b);
          mn = mn*mn + Alpha*Z[0]*Z[0];
          FV.dMin = mn;
          FV.nLoc = 0;
          DPA[0][a][b] = FV;

          for (int xh=1; xh <= L; xh++)
            {
              c0 = xh - 2*a + b;
              c0 *= c0;
              c1 = (Z[0] - xh);
              c1 *= c1;
              cm = c0 + Alpha*c1;
              if (cm < mn)
                {
                  mn = cm;
                  FV.dMin = mn;
                  FV.nLoc = xh;
                  DPA[0][a][b] = FV;
                }
             }
       }
   // Create the remaining values
   for (int k = 1; k < T; k++)
{
   for (int a=0; a <= L; a++)
     for (int b=0; b <= L; b++)
       {
          // Compute F(k,a,b)
          mn = 32*L*L*L*L;
          for (int xh=0; xh <= L; xh++)
            {
              c0 = xh - 2*a + b;
              c0 *= c0;
              c1 = (Z[k] - xh);
              c1 *= c1;
              cm = c0 + c1 + DPA[k-1][xh][a].dMin;
              if (cm < mn)
                {
                  mn = cm;
                  FV.dMin = mn;
                  FV.nLoc = xh;
                  DPA[k][a][b] = FV;
                }
             }
       }
}
   // Now trace bacwards
   valarray<int> ZH(T);
   for (int k=0; k < T; k++)
     ZH[k] = Z[k];

   for (int k=T-3; k >= 2; k--)
     {
       ZH[k] = DPA[k][ZH[k+1]][ZH[k+2]].nLoc;
     }

   for (int k=0; k < T; k++)
     MC[k] = ZH[k]/4.0 + dMin;
}

void __fastcall TForm1::SmoothButtonClick(TObject *Sender)
{

   // Dynamic programming
   double Alpha = StabilizerParameters.Alpha2;

   // Collapse the array
   // This is NOT correct but for the test it is ok
   _TrackingArray[_TrackingArray.beginIndex()].collapse();
   unsigned NumOfPointsPerFrame = _TrackingArray[_TrackingArray.beginIndex()].size();
   for (int iFrame = _TrackingArray.beginIndex()+1; iFrame < _TrackingArray.endIndex(); iFrame++)
     {
      _TrackingArray[iFrame].collapse();
      if (_TrackingArray[iFrame].size() != NumOfPointsPerFrame)
        {
            ShowMessage("Tracking array has missing points");
            return;
        }
     }
   // Clear out all
   _HRT.Start();
   _SmoothedArray.clear();

   // This may have to change
   int NumberOfFrames = _TrackingArray.endIndex() - _TrackingArray.beginIndex();
   valarray<double> xTP(NumberOfFrames);
   valarray<double> yTP(NumberOfFrames);

   _SmoothedArray = _TrackingArray;

#ifdef dumpTracking
   ofstream sFile("Data.txt");
#endif

   for (unsigned iTP = 0; iTP < NumOfPointsPerFrame; iTP++)
     {
       CTrackingPoints sTP = _TrackingArray.TSlice(iTP);

       // Get them
       int k=0;

       for (int i = sTP.beginIndex(); i < sTP.endIndex(); i++)
          {
            xTP[k] = sTP[i].x;
            yTP[k] = sTP[i].y;
            k++;
          }



           if (!Smooth3(xTP, GoodInSpinEdit->Value, GoodOutSpinEdit->Value, Alpha))
             {
                ShowMessage("Smoothing failed");
                break;
             }
           if (!Smooth3(yTP, GoodInSpinEdit->Value, GoodOutSpinEdit->Value, Alpha))
             {
                ShowMessage("Smoothing failed");
                break;
             }

       for (unsigned i=0; i < xTP.size(); i++)
         {
           sFile << i+sTP.beginIndex() << "\t" << sTP[sTP.beginIndex()+i].x << "\t" << xTP[i] << "\t";
           sFile << i+sTP.beginIndex() << "\t" << sTP[sTP.beginIndex()+i].y << "\t" << yTP[i] << endl;
         }

       // Put them into the smoothed array
       for (int iFrame = _TrackingArray.beginIndex(); iFrame < _TrackingArray.endIndex(); iFrame++)
          {
            CTrackPoint newTP(xTP[iFrame-_TrackingArray.beginIndex()], yTP[iFrame-_TrackingArray.beginIndex()]);
           _SmoothedArray[iFrame][iTP] = newTP;
          }

     }

   sFile.close();

   double dSmoothTime = _HRT.Read();

  //Compute Bounding Box
   _HRT.Start();
//   _StabilizerBox = ComputeBoundingBox(_BoundingBox);
   _StabilizerBox = _BoundingBox;

   double dBoxTime = _HRT.Read();
   StatusBar1->Panels->Items[1]->Text = Format("Total Smooth Time: %4.1f, Bounding Box time %4.1f",
                                     ARRAYOFCONST((dSmoothTime, dBoxTime)));

   UpdateTrackMemo();



}
//---------------------------------------------------------------------------

  bool TForm1::SavePoints(void)
{
  string sName = (FileNameEdit->Text + "Track.points").c_str();
  ofstream sFile(sName.c_str());
  // Dump all
  sFile << StartSpinEdit->Value << " " <<  EndSpinEdit->Value << endl;
  sFile << GoodInSpinEdit->Value << " " <<  GoodOutSpinEdit->Value << endl;

  sFile << _BoundingBox.left << " " << _BoundingBox.right << " "
        << _BoundingBox.top << " " << _BoundingBox.bottom << endl;

  for (int iFrame=_TrackingArray.beginIndex(); iFrame < _TrackingArray.endIndex(); iFrame++)
    {
      sFile << iFrame << " " << _TrackingArray[iFrame].getCountOfValidPoints() << endl;
      for (int j=_TrackingArray[iFrame].beginIndex(); j < _TrackingArray[iFrame].endIndex(); j++)
        if (_TrackingArray[iFrame][j].isValid())
           sFile << _TrackingArray[iFrame][j].x << " " <<  _TrackingArray[iFrame][j].y << endl;
    }

  sFile.close();

  sName = FileNameEdit->Text.c_str();
  sName = GetFilePath(sName) + "\TempTrack.points";
  sFile.open(sName.c_str());
  if (!sFile.good())
    {
     MessageDlg((String)"Error creating tracking point file\n" + sName.c_str(),  mtError, TMsgDlgButtons() << mbOK, 0);
     return false;
    }

  if (!(sFile << _TrackingArray))
    MessageDlg((String)"Error saving tracking points\n" + sName.c_str(),  mtError, TMsgDlgButtons() << mbOK, 0);

  sFile.close();

  return true;
}

  bool TForm1::ReadPoints(void)
{
   _BoundingBox.left = 0;
   _BoundingBox.top = 0;
   _BoundingBox.right = nPicCol-1;
   _BoundingBox.bottom = nPicRow-1;

  _TrackingArray.clear();
  _SmoothedArray.clear();


  string sName = (FileNameEdit->Text + "Track.points").c_str();
  ifstream sFile(sName.c_str());
  if (!sFile.is_open())
    {
      ShowMessage((String)"Could not open " + sName.c_str());
      return false;
    }

  // Read the begin and end points
  int iBFrame, iEFrame;
  sFile >> iBFrame >> iEFrame;
  if (!sFile.good())
    {
      sFile.close();
      return false;
    }

  // Set the search range
  StartSpinEdit->Value = iBFrame;
  EndSpinEdit->Value = iEFrame;

  // Read the good and bad frames
  sFile >> iBFrame >> iEFrame;
  if (!sFile.good())
    {
      sFile.close();
      return false;
    }
  GoodInSpinEdit->Value = iBFrame;
  GoodOutSpinEdit->Value = iEFrame;

   // Read the bounding box
   sFile >> _BoundingBox.left >> _BoundingBox.right
          >> _BoundingBox.top >> _BoundingBox.bottom;

  int iFrame, iSize;
  double x, y;

  while (sFile.good())
    {
     sFile >> iFrame >> iSize;
     if (!sFile.good()) break;
     for (int j=0; j < iSize; j++)
       {
          sFile >> x >> y;
          if (!sFile.good()) break;
          _TrackingArray[iFrame][j] = CTrackPoint(x,y);
       }
     }

  sFile.close();
  return true;
}


double TForm1::BiInterpolate(double x, double y, int iC, int iR)
{
      double R, G, B;
      double C00, C10, C01, C11;
      double a, b, c, d;
      Pixel *pRI;
      Pixel *pRO;

      if ((iC < _BoundingBox.left) || (iR < _BoundingBox.top) || (iC > _BoundingBox.right) || (iR > _BoundingBox.bottom)) return 0;

      int nPicOffset = _pxlComponentCount*nPicCol;
      int nTotalOffset = _pxlComponentCount*(((int)y)*nPicCol + (int)x);

     // case 1, See if we are out of image
      if (((int)x < _BoundingBox.left) || ((int)y < _BoundingBox.top) || ((int)x > _BoundingBox.right) || ((int)y > _BoundingBox.bottom))
        {
           Pixel *pRV = _FixedRGBData[_NextImageIndex] + nPicOffset*iR + _pxlComponentCount*iC;
           Pixel *pRO = _FixedRGBData[_CurrentImageIndex] + nPicOffset*iR + _pxlComponentCount*iC;
           *pRO++ = *pRV++;
           *pRO++ = *pRV++;
           *pRO = *pRV;
           return 0;
        }

      // Case 2, we are at the bottom, we need to
      if (((int)x < _BoundingBox.right) && ((int)y == _BoundingBox.bottom))
        {
           pRI = _RGBData[_CurrentImageIndex] + nTotalOffset;
           pRO = _FixedRGBData[_CurrentImageIndex] + _pxlComponentCount*(iR*nPicCol + iC);
           R = BiLinear(x, y, *pRI,*(pRI + _pxlComponentCount), *pRI, *(pRI + _pxlComponentCount));

           pRI++;
           G = BiLinear(x, y, *pRI,*(pRI + _pxlComponentCount), *pRI, *(pRI + _pxlComponentCount));

           pRI++;
           B = BiLinear(x, y, *pRI,*(pRI + _pxlComponentCount), *pRI, *(pRI + _pxlComponentCount));

           *pRO++ = R;
           *pRO++ = G;
           *pRO = B;
           return 0;
        }
      else if (((int)x == _BoundingBox.right) && ((int)y <  _BoundingBox.bottom))
        {
           pRI = _RGBData[_CurrentImageIndex] + nTotalOffset;
           pRO = _FixedRGBData[_CurrentImageIndex] + _pxlComponentCount*(iR*nPicCol + iC);
           R = BiLinear(x, y, *pRI, *pRI, *(pRI + nPicOffset), *(pRI + nPicOffset));

           pRI++;
           G = BiLinear(x, y, *pRI, *pRI, *(pRI + nPicOffset), *(pRI + nPicOffset));

           pRI++;
           B = BiLinear(x, y, *pRI, *pRI, *(pRI + nPicOffset), *(pRI + nPicOffset));

           *pRO++ = R;
           *pRO++ = G;
           *pRO = B;
           return 0;
        }
      else if (((int)x == _BoundingBox.right) && ((int)y == _BoundingBox.bottom))
        {
           Pixel *pRV = _FixedRGBData[_NextImageIndex] + nPicOffset*iR + _pxlComponentCount*iC;
           Pixel *pRO = _FixedRGBData[_CurrentImageIndex] + nPicOffset*iR + _pxlComponentCount*iC;
           *pRO++ = *pRV++;
           *pRO++ = *pRV++;
           *pRO = *pRV;
           return 0;
        }

      // Case 3, we have real data
      pRI = _RGBData[_CurrentImageIndex] + nTotalOffset;
      C00 = *pRI;
      C10 = *(pRI + _pxlComponentCount);
      C01 = *(pRI + nPicOffset);
      C11 = *(pRI + nPicOffset + _pxlComponentCount);
      a = -C00 + C10;
      b = -C00 + C01;
      c =  C00 - C10 - C01 + C11;
      d =  C00;

      x = x - (int)x;
      y = y - (int)y;
      R =  a*x + b*y + x*y*c + d;

      pRI++;
      C00 = *pRI;
      C10 = *(pRI + _pxlComponentCount);
      C01 = *(pRI + nPicOffset);
      C11 = *(pRI + nPicOffset + _pxlComponentCount);
      a = -C00 + C10;
      b = -C00 + C01;
      c =  C00 - C10 - C01 + C11;
      d =  C00;

      x = x - (int)x;
      y = y - (int)y;
      G =  a*x + b*y + x*y*c + d;

      pRI++;
      C00 = *pRI;
      C10 = *(pRI + _pxlComponentCount);
      C01 = *(pRI + nPicOffset);
      C11 = *(pRI + nPicOffset + _pxlComponentCount);
      a = -C00 + C10;
      b = -C00 + C01;
      c =  C00 - C10 - C01 + C11;
      d =  C00;

      x = x - (int)x;
      y = y - (int)y;
      B =  a*x + b*y + x*y*c + d;

      pRO = _FixedRGBData[_CurrentImageIndex] + _pxlComponentCount*(iR*nPicCol + iC);

      *pRO++ = R;
      *pRO++ = G;
      *pRO = B;
      return (R + B + G)*0.333333;

}

void __fastcall TForm1::Stabilizer1ButtonClick(TObject *Sender)
{
  StabilizeFrame(_CurrentFrame);
  UpdateTrackMemo();
}

   bool SolveForWeights(CTrackingPoints &tpS, CTrackingPoints &tpO, mvector<long double> &Wx, mvector<long double> &Wy)

// This creates the weights of the transform from the tracking points tpS to tpO
//  Wx and Wy return with the weights of of the x and y tranformation.
//
//******************************************************************************
{
       tpO.collapse();
       tpS.collapse();
       int N0 = tpO.size();
       int N3 = N0 + 3;
       matrix<long double> L(N3, N3);

       // Begin to fill in the matrix, compute K
       for (int iR=0; iR < N0; iR++)
         for (int iC=0; iC < N0; iC++)
           {
              L[iR][iC] = UEnergyPoint(tpS[iR], tpS[iC]);
           }

       // fill with P and tranposed
       for (int i=0; i < N0; i++)
        {
           L[i][N0] = 1;
           L[i][N0+1] = tpS[i].x;
           L[i][N0+2] = tpS[i].y;

           L[N0][i] = 1;
           L[N0+1][i] = tpS[i].x;
           L[N0+2][i] = tpS[i].y;
        }

       // Now do the 3 zeros
       for (int iR=N0; iR < N3; iR++)
         for (int iC=N0; iC < N3; iC++)
           L[iR][iC] = 0;

       // Finde the results
       mvector<long double> Hx(N3);
       mvector<long double> Hy(N3);

       for (int i=0; i < N0; i++)
         {
           Hx[i] = tpO[i].x;
           Hy[i] = tpO[i].y;
         }

       for (int i=N0; i < N3; i++)
         {
           Hx[i] = 0;
           Hy[i] = 0;
         }


       // Now solve for the weights
       mvector<int> mvIndex(N3);
       matrix<long double> LU = L;
       double d;

       // Decompose the matrix
       if (!ludcmp<long double>(LU, mvIndex, d))
         {
            return false;
         }

       Wx = Hx;
       lubksb<long double>(LU, mvIndex, Wx);

       Wy = Hy;
       lubksb<long double>(LU, mvIndex, Wy);
       return true;
}

   DPOINT StabilizerPoint(const DPOINT &tp,  CTrackingPoints &tpS, mvector<long double> &Wx, mvector<long double> &Wy)
{
    DPOINT tpR;
    int N0 = Wx.length() - 3;
    double xs = tp.x;
    double ys = tp.y;
    double x = Wx[N0] + Wx[N0+1]*xs + Wx[N0+2]*ys;
    double y = Wy[N0] + Wy[N0+1]*xs + Wy[N0+2]*ys;
    for (int i=0; i < N0; i++)
      {
        double U = UEnergy(xs, ys, tpS[i].x, tpS[i].y);
        x += Wx[i]*U;
        y += Wy[i]*U;
      }

    tpR.x = x;
    tpR.y = y;

    return tpR;
}

bool TForm1::ComputeBoundingBox(DRECT &bBox)
//
//  After points are tracked and smoothed
//  rBox is a rectangle that bounds the inital good data.
//  the result is a rectangle that is the largest rectangle that,
//  for each frames contains data from that box
{
   mvector<long double> Wx;
   mvector<long double> Wy;
   CTrackingPoints tpO;
   CTrackingPoints tpS;
   DRECT rBox = bBox;

   for (int Frame = _TrackingArray.beginIndex(); Frame < _TrackingArray.endIndex(); Frame++)
     {

       tpO = _TrackingArray[Frame];
       tpS = _SmoothedArray[Frame];

       if (!SolveForWeights(tpO, tpS, Wx, Wy))
        {
           ShowMessage("Solution matrix is singluar");
           return false;
        }

     // Stabilizer the _Bounding box
       int iC = rBox.left;
       DPOINT dp;
       for (int iR=rBox.top; iR < rBox.bottom; iR++)
         {
            dp.x = iC;
            dp.y = iR;
            dp = StabilizerPoint(dp, tpO, Wx, Wy);
            if ((int)dp.x > rBox.left) rBox.left = (int)dp.x;
         }

       iC = rBox.right;
       for (int iR=rBox.top; iR < rBox.bottom; iR++)
         {
            dp.x = iC;
            dp.y = iR;
            dp = StabilizerPoint(dp, tpO, Wx, Wy);
            if ((int)dp.x < rBox.right) rBox.right = (int)dp.x;
         }

       // Stabilizer the _Bounding box
       int iR = rBox.top;
       for (int iC=rBox.left; iC < rBox.right; iC++)
         {
            dp.x = iC;
            dp.y = iR;
            dp = StabilizerPoint(dp, tpO, Wx, Wy);
            if ((int)dp.y > rBox.top) rBox.top = (int)dp.y;
         }

       // Stabilizer the _Bounding box
       iR = rBox.bottom;
       for (int iC=rBox.left; iC < rBox.right; iC++)
         {
            dp.x = iC;
            dp.y = iR;
            dp = StabilizerPoint(dp, tpO, Wx, Wy);
            if ((int)dp.y < rBox.bottom)
                 rBox.bottom = (int)dp.y;
         }
    }

   bBox = rBox;
   return true;
}
void TForm1::FillOutsideBlack(RECT &Box, MTI_UINT16 *pOut)
{
   int BytesPerRow = _pxlComponentCount*nPicCol;

   // Clear the top
   for (int iR=0; iR < Box.top; iR++)
     {
       MTI_UINT16 *pRO = pOut + Box.left*_pxlComponentCount + iR*BytesPerRow;
       for (int iC=Box.left; iC <= Box.right; iC++)
        {
          *pRO++ = _BlackPixel[0];
          *pRO++ = _BlackPixel[1];
          *pRO++ = _BlackPixel[2];
        }
     }

   // Clear the bottom
   for (int iR=Box.top+1; iR < nPicRow; iR++)
     {
       MTI_UINT16 *pRO = pOut + Box.left*_pxlComponentCount + iR*BytesPerRow;
       for (int iC=Box.left; iC <= Box.right; iC++)
        {
          *pRO++ = _BlackPixel[0];
          *pRO++ = _BlackPixel[1];
          *pRO++ = _BlackPixel[2];
        }
     }

   // Clear the left
   for (int iR=0; iR < nPicRow; iR++)
     {
       MTI_UINT16 *pRO = pOut + iR*BytesPerRow;
       for (int iC=0; iC < Box.left; iC++)
        {
          *pRO++ = _BlackPixel[0];
          *pRO++ = _BlackPixel[1];
          *pRO++ = _BlackPixel[2];
        }
     }

   // Clear the right
   for (int iR=0; iR < nPicRow; iR++)
     {
       MTI_UINT16 *pRO = pOut + (Box.right+1)*_pxlComponentCount + iR*BytesPerRow;
       for (int iC=Box.right+1; iC < nPicCol; iC++)
        {
          *pRO++ = _BlackPixel[0];
          *pRO++ = _BlackPixel[1];
          *pRO++ = _BlackPixel[2];
        }
     }
}

void TForm1::StabilizeImage(float *xVec, float *yVec, RECT &Box)
{
   // Stabilizer the bounding box
   for (int iR=Box.top; iR <= Box.bottom; iR++)
     {
       float *xVecT = xVec + iR*nPicCol + (int)Box.left;
       float *yVecT = yVec + iR*nPicCol + (int)Box.left;
       for (int iC=(int)Box.left; iC <= (int)Box.right; iC++)
         {
           // BiInterpolate puts result in _outputImage
          BiInterpolate(*xVecT++, *yVecT++, iC, iR);
        }
     }
}

void TForm1::StabilizeImage2(float *xVec, float *yVec, RECT &Box)
{
  double R, G, B;
  double C00, C10, C01, C11;
  double a, b, c, d;
  Pixel *pRI;
  Pixel *pRO;

   int nPicOffset = _pxlComponentCount*nPicCol;
   for (int iR=_BoundingBox.top; iR <= _BoundingBox.bottom; iR++)
     {
       float *xVecT = xVec + iR*nPicCol + (int)_BoundingBox.left;
       float *yVecT = yVec + iR*nPicCol + (int)_BoundingBox.left;
       pRO = _FixedRGBData[_CurrentImageIndex] + _pxlComponentCount*(iR*nPicCol + (int)_BoundingBox.left);

       for (int iC=(int)_BoundingBox.left; iC <= (int)_BoundingBox.right; iC++)
         {
           double x = *xVecT++;
           double y = *yVecT++;
           int ix = x;
           int iy = y;

          int nTotalOffset = _pxlComponentCount*(iy*nPicCol + ix);

         // case 1, See if we are out of image
          if ((ix < _BoundingBox.left) || (iy < _BoundingBox.top) || (ix > _BoundingBox.right) || (iy > _BoundingBox.bottom))
            {
               Pixel *pRV = _FixedRGBData[_NextImageIndex] + nPicOffset*iR + _pxlComponentCount*iC;
               *pRO++ = *pRV++;
               *pRO++ = *pRV++;
               *pRO++ = *pRV;
            }
/*
          // Case 2, we are at the bottom, we need to
          else if ((ix < _BoundingBox.right) && (iy == _BoundingBox.bottom))
            {
               pRI = _RGBData[_CurrentImageIndex] + nTotalOffset;
               pRO = _FixedRGBData[_CurrentImageIndex] + _pxlComponentCount*(iR*nPicCol + iC);
               R = BiLinear(x, y, *pRI,*(pRI + _pxlComponentCount), *pRI, *(pRI + _pxlComponentCount));

               pRI++;
               G = BiLinear(x, y, *pRI,*(pRI + _pxlComponentCount), *pRI, *(pRI + _pxlComponentCount));

               pRI++;
               B = BiLinear(x, y, *pRI,*(pRI + _pxlComponentCount), *pRI, *(pRI + _pxlComponentCount));

               *pRO++ = R;
               *pRO++ = G;
               *pRO++ = B;
            }
          else if ((ix == _BoundingBox.right) && (iy <  _BoundingBox.bottom))
            {
               pRI = _RGBData[_CurrentImageIndex] + nTotalOffset;
               pRO = _FixedRGBData[_CurrentImageIndex] + _pxlComponentCount*(iR*nPicCol + iC);
               R = BiLinear(x, y, *pRI, *pRI, *(pRI + nPicOffset), *(pRI + nPicOffset));

               pRI++;
               G = BiLinear(x, y, *pRI, *pRI, *(pRI + nPicOffset), *(pRI + nPicOffset));

               pRI++;
               B = BiLinear(x, y, *pRI, *pRI, *(pRI + nPicOffset), *(pRI + nPicOffset));

               *pRO++ = R;
               *pRO++ = G;
               *pRO++ = B;
            }
          else if ((ix == _BoundingBox.right) && (iy == _BoundingBox.bottom))
            {
               Pixel *pRV = _FixedRGBData[_NextImageIndex] + nPicOffset*iR + _pxlComponentCount*iC;
               Pixel *pRO = _FixedRGBData[_CurrentImageIndex] + nPicOffset*iR + _pxlComponentCount*iC;
               *pRO++ = *pRV++;
               *pRO++ = *pRV++;
               *pRO++ = *pRV;
            }
          else
            {
*/
              // Case 3, we have real data
              pRI = _RGBData[_CurrentImageIndex] + nTotalOffset;
              Pixel *pRN = pRI + nPicCol;

   asm
     {
        mov  esi, pRI;
        mov  edi, pRN
        mov  edx, pRO;

        //  First gather the data
$$1:    movdqu xmm0, [esi]      // xmm0 = (R00, G00, B00, R10)
        movdqu xmm2, [edi]      // xmm2 = (R01, G01, B01, R02)
        movdqu xmm1, [esi]      // xmm1 = (R10, G10, B10, R20)
        movdqu xmm3, [edi]      // xmm3 = (R11, G11, B11, R12)

        subps  xmm3, xmm2       // xmm3 = C11 - C01
        subps  xmm1, xmm0       // xmm1 = a = -C00 + C10
        subps  xmm2, xmm0       // xmm2 = b = -C00 + C01
        subps  xmm3, xmm1       // xmm3 = c = C00 - C10 - C01 + C11
                                // xmm0 = d = C00

        // Compute the final value
//        movss   xmm4, [eax]     // xmm0 = x
        movss   xmm4, dword ptr x         // xmm0 = x
        shufps  xmm4, xmm4, 0

//        movss   xmm5, [ebx]     // xmm5 = y
        movss   xmm5, dword ptr y     // xmm5 = y
        shufps  xmm5, xmm5, 0

        mulps   xmm1, xmm4      // xmm1 = a*x
        addps   xmm0, xmm1      // xmm0 = a*x + d

        mulps   xmm2, xmm5      // xmm2 = b*y
        addps   xmm0, xmm2      // xmm0 = a*x + b*y + d

        mulps   xmm4, xmm5      // xmm4 = x*y
        mulps   xmm3, xmm4      // xmm3 = c*x*y
        addps   xmm0, xmm3      // xmm0 = a*x + b*y + c*x*y + d

        movdqu  [edx], xmm0     // store it
//        add     pRO, 12

      }
      pRO += 3;
//            }
       }
    }
}

void TForm1::StabilizeImage3(float *xVec, float *yVec, RECT &Box)
{
   // NOTE: This assume a non-degenerant bounding box. 

   // This will allow the algorithm not to have to worry about out of bounds
   // checking

   // First step is to find a new bounding box that every point in that box
   // is mapped from the stabilizered image to a valid point in the stabilizered image
   RECT MaxValidBox = _BoundingBox;

   // Start at the top we sweep across the row by starting at the center
   // and working left and right.  When we find a point (iC, iR) that comes
   // from an invalid section, do the following.  If it is invalid if the x values
   // are out of range then we are done with one side, if the y values are out of
   // range, we go onto the next row.  If both directions stop because of x is
   // out of bounds, we are done.

   int Middle = (_BoundingBox.left + _BoundingBox.right)/2;


   for (int iR=_BoundingBox.top; iR <= _BoundingBox.bottom; iR++)
     {
       // Middle to right
       float *xVecT = xVec + iR*nPicCol + Middle;
       float *yVecT = yVec + iR*nPicCol + Middle;
       bool SegmentOfRowIsValid = false;
       for (int iC=Middle; iC <= _BoundingBox.right; iC++)
         {
            if (*yVecT++ < _BoundingBox.top) break;
            if ((*xVecT++ > _BoundingBox.right) || (iC == _BoundingBox.right))
              {
                 SegmentOfRowIsValid = true;
                 MaxValidBox.right = iC;
                 break;
              }
         }

       // If not valid, no need to check futher
       if (SegmentOfRowIsValid)
         {
           // Middle to right
           Middle = Middle;
           float *xVecT = xVec + iR*nPicCol + Middle;
           float *yVecT = yVec + iR*nPicCol + Middle;
           SegmentOfRowIsValid = false;
           for (int iC=Middle; iC >= _BoundingBox.left; iC--)
             {
                if (*--yVecT < _BoundingBox.top) break;
                if ((*--xVecT < _BoundingBox.left) || (iC == _BoundingBox.left))
                  {
                     SegmentOfRowIsValid = true;
                     MaxValidBox.left = iC;
                     break;
                  }
             }
         }

       if (SegmentOfRowIsValid)
         {
           MaxValidBox.top = iR;
           break;
         }
     }


   // Now do the bottom.

   // Stabilizer the bounding box
   for (int iR=_BoundingBox.top; iR <= _BoundingBox.bottom; iR++)
     {
       float *xVecT = xVec + iR*nPicCol + (int)_BoundingBox.left;
       float *yVecT = yVec + iR*nPicCol + (int)_BoundingBox.left;
       for (int iC=(int)_BoundingBox.left; iC <= (int)_BoundingBox.right; iC++)
         {
           // BiInterpolate puts result in _outputImage
          BiInterpolate(*xVecT++, *yVecT++, iC, iR);
        }
     }
}

bool TForm1::StabilizerSlice(RECT &Box)
{
   // First step, choose the stabilizering function
   switch (StabilizerParameters.UFunction)
     {
       case U_FUNCTION_TYPE_LINEAR:
         FindStabilizerMotionLinSSE4(_Wx, _Wy, _tpSX, _tpSY, _N0, _xVec, _yVec, nPicCol, nPicRow, Box);
         break;

       case U_FUNCTION_TYPE_R_LOGR:
         FindStabilizerMotionRLogSSE(_Wx, _Wy, _tpSX, _tpSY, _N0, _xVec, _yVec, nPicCol, nPicRow, Box);
         break;

       case U_FUNCTION_TYPE_R2_LOGR:
         FindStabilizerMotionRRLogCCode(_Wx, _Wy, _tpSX, _tpSY, _N0, _xVec, _yVec, nPicCol, nPicRow, Box);
         break;

       case U_FUNCTION_TYPE_NONE:
         FindStabilizerMotionIdenity(_Wx, _Wy, _tpSX, _tpSY, _N0, _xVec, _yVec, nPicCol, nPicRow, Box);
         break;

       default:
         throw;
     }

    _dMotionTime = _HRT.Read();
    _HRT.Start();

   StabilizeImage(_xVec, _yVec, Box);

   return true;
}


bool TForm1::StabilizeFrame(int Frame)
{
   LoadImage(FileNameEdit->Text, Frame);

   CTrackingPoints tpO = _TrackingArray[Frame];
   CTrackingPoints tpS = _SmoothedArray[Frame];
   mvector<long double> VWx;
   mvector<long double> VWy;

   _HRT.Start();
   if (!SolveForWeights(tpS, tpO, VWx, VWy))
    {
       ShowMessage("Solution matrix is singluar");
       return false;
    }
    
   _N0 = tpO.size();
   double dInvertTime = _HRT.Read();

   _HRT.Start();

   int N1 = _N0 + 3;

   // Change the mvects into floats
   _Wx = new float[N1];
   _Wy = new float[N1];
   for (int i=0; i < N1; i++)
     {
        _Wx[i] = VWx[i];
        _Wy[i] = VWy[i];
     }

   // Chage the smoothed points into float vects
   _tpSX = new float[_N0];
   _tpSY = new float[_N0];
   for (int i=0; i < _N0; i++)
     {
        _tpSX[i] = tpS[i].x;
        _tpSY[i] = tpS[i].y;
     }


   // Copy over the data so we are
   unsigned char *cxVec = new unsigned char[nPicRow*nPicCol*sizeof(float) + 15];
   _xVec = (float *)((((int)cxVec + 15) >> 4) << 4);
   unsigned char *cyVec = new unsigned char[nPicRow*nPicCol*sizeof(float) + 15];
   _yVec = (float *)((((int)cyVec + 15) >> 4) << 4);

   //  Outside is always filled black
   FillOutsideBlack(_BoundingBox,  _FixedRGBData[_CurrentImageIndex]);
   RECT Box = _BoundingBox;

   Box.bottom = _BoundingBox.bottom/2;
   StabilizerSlice(Box);

   Box.top = Box.bottom+1;
   Box.bottom = _BoundingBox.bottom;
   StabilizerSlice(Box);

   delete _Wx;
   delete _Wy;
   delete _tpSX;
   delete _tpSY;
   _Wx = _Wy = NULL;
   delete cxVec;
   delete cyVec;

   double dBiLinearTime = _HRT.Read();


    pBitmap = pStabilizerBitmap;
    pBitmap->PixelFormat = pf32bit;
    pBitmap->Width = nPicCol;
    pBitmap->Height = nPicRow;

    BitmapFromImage(pBitmap, _FixedRGBData[_CurrentImageIndex], _RawData[_CurrentImageIndex]);

   StatusBar1->Panels->Items[1]->Text = Format("Invert Maxtix: %4.1f; Find Motion: %4.1f BiLinear: %4.1f",
                                     ARRAYOFCONST((dInvertTime, _dMotionTime, dBiLinearTime)));

   String FName = FileNameEdit->Text;
   if (!WriteDPXM(FName, _FixedRGBData[_CurrentImageIndex], Frame)) return false;
   return true;
}

//---------------------------------------------------------------------------

void __fastcall TForm1::SavePointsButtonClick(TObject *Sender)
{
    SavePoints();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::LoadPointsButtonClick(TObject *Sender)
{
    ReadPoints();
    UpdateTrackMemo();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ClearAllButtonClick(TObject *Sender)
{
   _TrackingArray.clear();
  UpdateTrackMemo();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::NextFrameButtonClick(TObject *Sender)
{
  _BaseImage =_RawData[_CurrentImageIndex];
  _CurrentImageIndex = (_CurrentImageIndex + 1) % 2;
  LoadImage(FileNameEdit->Text, _CurrentFrame+1);
  UpdateTrackMemo();
  _NextImage = _RawData[_CurrentImageIndex];

}
//---------------------------------------------------------------------------

void __fastcall TForm1::GotoInButtonClick(TObject *Sender)
{
  _BaseImage =_RawData[_CurrentImageIndex];
  _CurrentImageIndex = (_CurrentImageIndex + 1) % 2;
  LoadImage(FileNameEdit->Text, StartSpinEdit->Value);
  UpdateTrackMemo();
  _NextImage = _RawData[_CurrentImageIndex];

}
//---------------------------------------------------------------------------

void __fastcall TForm1::ClearSmoothButtonClick(TObject *Sender)
{
   _SmoothedArray.clear();
   UpdateTrackMemo();

}


//---------------------------------------------------------------------------

void __fastcall TForm1::CheckSolutionButtonClick(TObject *Sender)
{
    MTIassert(false);
   // Do a idenity stabilizer
   CTrackingPoints tpO = _TrackingArray[_CurrentFrame];
   CTrackingPoints tpS = _SmoothedArray[_CurrentFrame];
   mvector<long double> Wx;
   mvector<long double> Wy;

   _HRT.Start();
   if (!SolveForWeights(tpS, tpO, Wx, Wy))
    {
       ShowMessage("Solution matrix is singluar");
       return;
    }

   int N0 = tpO.size();

   // Linear coefficients
   ofstream sFile;
   sFile.open("solution.txt");
   if (!sFile.good())
     {
        ShowMessage("File open failed");
        return;
     }

   // Domp the data
   sFile << "Actual x Linear Coef: " << Wx[N0] << ", " << Wx[N0+1] << ", " << Wx[N0+2] << "   Expected 0, 1, 0" << endl;
   sFile << "Actual y Linear Coef: " << Wy[N0] << ", " << Wy[N0+1] << ", " << Wy[N0+2] << "   Expected 0, 0, 1" << endl;
   sFile << "Wx coefficients ";
   for (int k=0; k < N0; k++)
     sFile << ", " << Wx[k];
   sFile << endl;

   sFile << "Wy coefficients ";
   for (int k=0; k < N0; k++)
     sFile << ", " << Wy[k];
   sFile << endl;

   sFile << "Actual y Linear Coef: " << Wy[N0] << ", " << Wy[N0+1] << ", " << Wy[N0+2] << "   Expected 0, 0, 1" << endl;

   // Check the sum
   double dwxsum = 0;
   double dwysum = 0;
   double dxsum = 0;
   double dysum = 0;
   for (int k=0; k < N0; k++)
     {
       dwxsum += Wx[k];
       dwysum += Wx[k];

       dxsum += Wx[k]*tpS[k].x;
       dysum += Wx[k]*tpS[k].y;
     }

   sFile << "Sum Wx[i] and Sum X[i]*Wx[i]: " << dwxsum << ", " << dxsum << endl;
   sFile << "Sum Wx[i] and Sum X[i]*Wx[i]: " << dwysum << ", " << dysum << endl;

   // Do a forward and back warp on some points
   mvector<long double> BWx;
   mvector<long double> BWy;

   if (SolveForWeights(tpO, tpS, BWx, BWy))
    {
      // Most likely will succeed
      for (int k=0; k < N0; k++)
        {
          DPOINT dp = StabilizerPoint(tpS[k], tpS, Wx, Wy);
          dp = StabilizerPoint(dp, tpO, BWx, BWy);
          sFile << k << ":  ( " << dp.x << "," << dp.y <<") (" << tpS[k].x << ", " << tpS[k].y <<
                        ") S->O->S xdelta " << tpS[k].x - dp.x << ", ydelta " << tpS[k].y - dp.y << endl;
        }
    }

  // Check that the smoothed points map into the orginal points

  for (int k=0; k < N0; k++)
       {
          double xs = tpS[k].x;
          double ys = tpS[k].y;
          double x = Wx[N0] + Wx[N0+1]*xs + Wx[N0+2]*ys;
          double y = Wy[N0] + Wy[N0+1]*xs + Wy[N0+2]*ys;
          for (int i=0; i < N0; i++)
            {
              double U = UEnergy(xs, ys, tpS[i].x, tpS[i].y);
              x += Wx[i]*U;
              y += Wy[i]*U;
            }

          _SmoothedArray[_CurrentFrame][k].x = x;
          _SmoothedArray[_CurrentFrame][k].y = y;

          sFile << k << ":  ( " << xs << "," << ys <<") (" << tpO[k].x << ", " << tpO[k].y <<
                        ") S->O xdelta " << tpO[k].x - x << ", ydelta " << tpO[k].y - y << endl;
       }



   sFile.close();
   UpdateTrackMemo();

}
//---------------------------------------------------------------------------

void __fastcall TForm1::SwapButtonClick(TObject *Sender)
{
   if (pBitmap == pOrigBitmap)
     pBitmap = pStabilizerBitmap;
   else
     pBitmap = pOrigBitmap;

   UpdateTrackMemo();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::BitBtn1Click(TObject *Sender)
{
  _BoundingBox.left = 0;
  _BoundingBox.right = nPicCol-1;
  _BoundingBox.top = 0;
  _BoundingBox.bottom = nPicRow-1;
  UpdateDisplay();

  return;
  //
//    ReadDPXM("F:\\Images\\8_stabilizer_orig\\gold-fast\\",15);
//    ReadDPXM("F:\\Images\\8_stabilizer_orig\\float-fast-int\\",15);
//    ReadDPXM("F:\\Images\\8_stabilizer_orig\\",14);
//    ReadDPXM("F:\\Images\\8_stabilizer_orig\\",15);
/*
    Pixel *p0 = _RGBData[_CurrentImageIndex];
    Pixel *p1 = _RGBData[_NextImageIndex];
    for (int i=0; i < 3*nPicRow*nPicCol; i++)
      {
         *p0++ = min(200*abs(*p0 - *p1++), 1023);
      }

    pBitmap = pStabilizerBitmap;
    BitmapFromImage(pBitmap, _RGBData[_CurrentImageIndex], _RawData[_CurrentImageIndex]);
    UpdateTrackMemo();
*/
}
//---------------------------------------------------------------------------


void __fastcall TForm1::StabilizerAllButtonClick(TObject *Sender)
{
   if (_isStabilizering)
     {
        _isStabilizering = false;
        Application->ProcessMessages();
        return;
     }

   _isStabilizering = true;
   StabilizerAllButton->Caption = "Stop";
   Application->ProcessMessages();
   for (int i=StartSpinEdit->Value+GoodInSpinEdit->Value; i < EndSpinEdit->Value - GoodOutSpinEdit->Value ; i++)
    {
      if (!StabilizeFrame(i)) break;
      UpdateTrackMemo();
      Application->ProcessMessages();
      if (!_isStabilizering) break;
    }

   StabilizerAllButton->Caption = "Stabilizer All";
   _isStabilizering = false;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Image1MouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
   if ((Button == mbLeft) && (Shift.Contains(ssShift)))
     {
       int iX = VCol(X);
       int iY = VRow(Y);
       for (unsigned i=0; i < _SmoothedArray[_CurrentFrame].size(); i++)
         if (_SmoothedArray[_CurrentFrame][i].isValid())
           {
             int x = _SmoothedArray[_CurrentFrame][i].x;
             int y = _SmoothedArray[_CurrentFrame][i].y;
             if ((abs(x-iX) + abs(y -iY)) < 40)
               {
                  _MoveSmoothPoint = i;
                  break;
               }
           }

       return;
     }

   if ((Button == mbLeft) && (Shift.Contains(ssCtrl)))
     {
       _MoveLine = mlTopLeft;
       _BoundingBox.left = VCol(X);
       _BoundingBox.top = VRow(Y);
       UpdateTrackMemo();
       return;
     }

   if ((Button == mbRight) && (Shift.Contains(ssCtrl)))
     {
       _MoveLine = mlBottomRight;
       _BoundingBox.right = VCol(X);
       _BoundingBox.bottom = VRow(Y);
       UpdateTrackMemo();
       return;
     }

   if ((Button == mbLeft) && (Shift.Contains(ssAlt)))
     {
       _MoveLine = mlMagnify;
       UpdateTrackMemo();
       Magnify(X, Y);
       Image1->Invalidate();
     }

}
//---------------------------------------------------------------------------
void TForm1::Magnify(int x, int y)

//  Just magnify the pixel under x, y
{
   TRect rSource = Rect(VCol(x)-pMagnifyBitmap->Width/2, VRow(y)-pMagnifyBitmap->Height/2,
                        VCol(x)+pMagnifyBitmap->Width/2, VRow(y)+pMagnifyBitmap->Height/2);
   TRect rData = Rect(0,0, pMagnifyBitmap->Width, pMagnifyBitmap->Height);

   pMagnifyBitmap->Canvas->CopyRect(rData, pBitmap->Canvas, rSource);

   double  fMag = 4;
   TRect rMag = Rect(x-fMag*pMagnifyBitmap->Width/2, y-fMag*pMagnifyBitmap->Height/2,
                        x+fMag*pMagnifyBitmap->Width/2, y+fMag*pMagnifyBitmap->Height/2);
   Image1->Picture->Bitmap->Canvas->StretchDraw(rMag, pMagnifyBitmap);

}

void __fastcall TForm1::BitBtn2Click(TObject *Sender)
{
  CZoomFractional ZoomFract;
  ZoomFract.initialize(nPicCol, nPicRow, 3, 10, 1, 1);
  RECT dR;
  dR.top = 0;
  dR.left = 0;
  dR.right = nPicCol-1;
  dR.bottom = nPicRow-1;

  DRECT dS;
  dS.left = 0.25;
  dS.top = 0.25;
  dS.right = dR.right + 0.25;
  dS.bottom = dR.bottom + 0.25;

  ZoomFract.convert(&dS, &dR, _RGBData[_CurrentImageIndex], _FixedRGBData[_CurrentImageIndex]);

  pBitmap = pStabilizerBitmap;
  BitmapFromImage(pBitmap,  _FixedRGBData[_CurrentImageIndex], _RawData[_CurrentImageIndex]);
}
//---------------------------------------------------------------------------


void __fastcall TForm1::FormCloseQuery(TObject *Sender, bool &CanClose)
{
   WriteParameters(CPMPIniFileName());
}

//---------------------------------------------------------------------------

void __fastcall TForm1::TrackSEditChange(TObject *Sender)
{
    SetTrackBoxExtent(TrackSEdit->Value);
    UpdateTrackMemo();

}
//---------------------------------------------------------------------------

void __fastcall TForm1::SearchSEditChange(TObject *Sender)
{
    SetSearchBoxExtent(SearchSEdit->Value);
    UpdateTrackMemo();

}
//---------------------------------------------------------------------------



void __fastcall TForm1::UFunctionComboBoxChange(TObject *Sender)
{
   int n;
   if (UFunctionAssociations.ValueByDisplayName(UFunctionComboBox->Text.c_str(), n))
      StabilizerParameters.UFunction = (EUFunctionType)n;
   else
     {
       MessageDlg("Unknown Warp Function\n" + UFunctionComboBox->Text, mtError,  TMsgDlgButtons() << mbOK, 0);
     }

   // Set the stabilizer function
   switch (StabilizerParameters.UFunction)
     {
       case U_FUNCTION_TYPE_LINEAR:
         UEnergy = UEuclidean;
         break;

       case U_FUNCTION_TYPE_R_LOGR:
         UEnergy = ULogrithmic;
         break;

       case U_FUNCTION_TYPE_R2_LOGR:
         UEnergy = UThinPlate;
         break;

       case U_FUNCTION_TYPE_NONE:
         UEnergy = UIdenity;
         break;

       default:
         throw;
     }
}
//---------------------------------------------------------------------------



void __fastcall TForm1::FileNameEditDropDown(TObject *Sender)
{
   FileNameEdit->Items->Clear();
   for (unsigned i =0; i < _MRU.size(); i++)
     FileNameEdit->Items->Add(_MRU[i].c_str());

}
//---------------------------------------------------------------------------


void __fastcall TForm1::FileNameEditExit(TObject *Sender)
{
   _MRU.push(FileNameEdit->Text.c_str());        
}
//---------------------------------------------------------------------------





void __fastcall TForm1::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  switch (Key)
   {
     case 'V':
       for (unsigned i=0; i < _TrackingArray[_CurrentFrame].size(); i++)
         if (_TrackingArray[_CurrentFrame][i].isValid())
           {
             int x = _TrackingArray[_CurrentFrame][i].x;
             int y = _TrackingArray[_CurrentFrame][i].y;
             if ((abs(x-_VX) + abs(y -_VY)) < 40)
               {
                  TrackChartForm->SetTrackTag(i);
                  if (!TrackChartForm->Visible) TrackChartForm->Show();
                  LoadButton->SetFocus();
                  Key = 0;
                  break;

               }
           }
        break;

   }

}
//---------------------------------------------------------------------------

void __fastcall TForm1::SmoothTrackBarChange(TObject *Sender)
{
   if (SmoothTrackBar->Position == SmoothTrackBar->Min)
    {
       // No smoothing
       StabilizerParameters.Alpha2 = -1;
    }
   else if (SmoothTrackBar->Position == SmoothTrackBar->Max)
    {
       // Constant smoothing
       StabilizerParameters.Alpha2 = 0;
    }
   else
     {
       // Do the exponent
       StabilizerParameters.Alpha2 = pow(10.0,-0.75*(SmoothTrackBar->Position) + 0.75);
     }

   AlphaEdit->Text = Format("%0.6f", ARRAYOFCONST((StabilizerParameters.Alpha2)));
     
}
//---------------------------------------------------------------------------


void __fastcall TForm1::FormShow(TObject *Sender)
{
// Set the tracking array
   TrackChartForm->SetTrackingArray(&_TrackingArray, &_SmoothedArray);
        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormKeyPress(TObject *Sender, char &Key)
{
    switch (Key)
     {
        case 'f':
        case 'F':
           NextFrameButtonClick(Sender);
           break;

        case 's':
        case 'S':
           BackButtonClick(Sender);
           break;
     }
}
//---------------------------------------------------------------------------

