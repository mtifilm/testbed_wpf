//---------------------------------------------------------------------------

#ifndef StabilizerGuiWinH
#define StabilizerGuiWinH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "VTimeCodeEdit.h"
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>

#include "AdvancedTrackingUnit.h"
#include "machine.h"
#include "StabilizerTool.h"
#include "MTIUNIT.h"
#include <ComCtrls.hpp>
#include <OleCtrls.hpp>
#include "cspin.h"
#include <VCLTee.Chart.hpp>
#include <VCLTee.Series.hpp>
#include <VCLTee.TeEngine.hpp>
#include <VCLTee.TeeProcs.hpp>
#include <Dialogs.hpp>
#include "ExecButtonsFrameUnit.h"
#include "ExecStatusBarUnit.h"
#include "TrackEditFrameUnit.h"
#include "ColorPanel.h"
#include <VclTee.TeeGDIPlus.hpp>
#include <System.ImageList.hpp>
#include "PresetsUnit.h"
#include <string>
using std::string;

#include "ToolSystemInterface.h"    // For FosterParentEventInfo

//---------------------------------------------------------------------------
/////////////////////////////////////////////////////////////////////////////
// The public interface
// No need to class this because we link in when compilied
//
extern void CreateStabilizerToolGUI(void);
extern void DestroyStabilizerToolGUI(void);
extern bool ShowStabilizerToolGUI(void);
extern bool HideStabilizerToolGUI(void);
extern bool IsToolVisible(void);

extern void UpdateExecutionButtons(EToolControlState toolProcessingControlState,
											  bool processingRenderFlag,
											  StupidTrackingHackState stupidTrackingHackState);
extern bool RunExecButtonsCommand(EExecButtonId command);
extern void UpdateTrackingPointButtons(void);
extern void SetResumeTimecodeToCurrent();
extern void UpdateTrackingCharts(void);
extern void CaptureGUIParameters(CPDLElement &toolParams);
extern void GatherGUIParameters(CStabilizerParameters &toolParams);
extern void SetGUIParameters(CStabilizerParameters &toolParams);
extern void EnableProcessingRegionGUI(bool onOffFlag);
extern bool IsProcessingRegionGUIEnabled(void);
extern void SetControlFocus(EStabilizerCmd controlCommand);
extern string GetFilenameForSavingTrackingPoints(const string &suggestedFilename);
extern string GetFilenameForLoadingTrackingPoints(const string &suggestedFilename);
extern void ToggleTrackPointVisibility(void);
extern void SetTrackPointsVisibility(bool flag);
extern void ToggleProcRegionVisibility(void);
extern void ToggleProcRegionOperation(void);
extern void ToggleColorPicker(void);
extern void ToggleClearOnShotChange(void);
extern void ToggleSubPixelInterpolation(void);
extern void ToggleSubtool(void);



enum EStabilizeMotionMode
{
   H,
   V,
   HV,
   HVR
};



class TStabilizerForm : public TMTIForm
{
__published:	// IDE-managed Components
	TImageList *ButtonImageList;
        TTimer *UpdateTimer;
        TLabel *AnchorRestrictionLabel;
        TImageList *BitBtnImageList;
        TTimer *DownTimer;
    TOpenDialog *OpenTrackingPointsDialog;
    TSaveDialog *SaveTrackingPointsDialog;
        TGroupBox *AutoLoadGroupBox;
        TPanel *Panel2;
	TColorPanel *StabilizationControlPanel;
    TStatusBar *StatusBar;
    TLabel *StatusLabel;
    TLabel *StatusMessageLabel;
    TLabel *PointCountLabel;
    TLabel *PointsLabel;
    TExecButtonsFrame *ExecButtonsToolbar;
    TPanel *TitlePanel;
    TLabel *TitleLabel;
        TTimer *ColorPickerStateTimer;
    TExecStatusBarFrame *ExecStatusBar;
	TTabControl *ManualAutoTabControl;
	TPanel *PanelHolderPanel;
	TChart *RTrackGraph;
	TPanel *RGraphLabelPanel;
	TLineSeries *LineSeries3;
	TLineSeries *LineSeries4;
	TColorPanel *NoRotationPanel;
	TPanel *NoRotationInnerPanel;
	TColorPanel *TopControlPanel;
	TGroupBox *TrackingPointsGroupBox;
	TLabel *SelectPrimaryLink;
	TLabel *SelectSecondaryLink;
	TPanel *Panel3;
	TCheckBox *ShowRenderPointCBox;
	TCheckBox *AutoLoadCBox;
	TCheckBox *AutoSaveCBox;
	TCheckBox *ClearOnShotChangeCBox;
	TBitBtn *TrackButton;
	TBitBtn *LoadPointsButton;
	TBitBtn *SavePointsButton;
	TBitBtn *PrevPointButton;
	TBitBtn *NextPointButton;
	TBitBtn *DeletePointButton;
	TGroupBox *TrackingParametersGroupBox;
	TCheckBox *ShowPointCBox;
	TPanel *TabStopDoNotDeletePanel;
	TColorPanel *MiddleControlPanel;
	TGroupBox *RenderParametersGroupBox;
	TLabel *JitterMinLabel;
	TLabel *JitterMaxLabel;
	TLabel *SmoothMaxLabel;
	TLabel *SmoothMinLabel;
	TPanel *JitterClippingPanel;
	TTrackEditFrame *JitterTrackEdit;
	TPanel *SmoothingClippingPanel;
	TTrackEditFrame *SmoothTrackEdit;
	TCheckBox *UseSubPixelInterpolationCheckBox;
	TGroupBox *ProcessingRegionGroupBox;
	TSpeedButton *TopUpButton;
	TSpeedButton *TopDownButton;
	TSpeedButton *BottomUpButton;
	TSpeedButton *BottomDownButton;
	TSpeedButton *LeftLeftButton;
	TSpeedButton *LeftRightButton;
	TSpeedButton *RightLeftButton;
	TSpeedButton *RightRightButton;
	TSpeedButton *ColorPickButton;
	TBitBtn *SetMatButton;
	TBitBtn *AllMatButton;
	TCheckBox *ShowMattingBoxCBox;
	TCheckBox *UseMattingCBox;
	TPresetsFrame *PresetsFrame;
	TPanel *ChartsPanel;
	TChart *HTrackGraph;
	TPanel *XGraphLabelPanel;
	TLineSeries *LineSeries1;
	TLineSeries *LineSeries2;
	TChart *VTrackGraph;
	TPanel *YGraphLabelPanel;
	TLineSeries *Series20;
	TLineSeries *Series21;
	TPanel *TrackingDataAvailabilityPanel;
	TPanel *ChartNumbersCoverUpPanel;
	TColorPanel *BottomControlPanel;
	TGroupBox *HowToStabilizeGroupBox;
	TSpeedButton *GoToRefframeButton;
	TSpeedButton *ResetRefFrameButton;
	TRadioButton *SmoothMotionRadioButton;
	TRadioButton *NoMotionRadioButton;
	TRadioButton *MatchRefRadioButton;
	VTimeCodeEdit *RefFrameTimeCodeEdit;
	TGroupBox *WhatToStabilizeGroupBox;
	TRadioButton *HorizontalRadioButton;
	TRadioButton *VerticalRadioButton;
	TRadioButton *HorizAndVertRadioButton;
	TRadioButton *HorizVertAndRotationRadioButton;
	TGroupBox *WhereToAnchorGroupBox;
	TRadioButton *AnchorFirstRadioButton;
	TRadioButton *AnchorLastRadioButton;
	TRadioButton *AnchorBothRadioButton;
	TRadioButton *AnchorNoneRadioButton;
	TGroupBox *DataFillGroupBox;
	TRadioButton *MissingDataBlackRadioButton;
	TRadioButton *MissingDataWhiteRadioButton;
	TRadioButton *MissingDataPrevFrameRadioButton;
	TRadioButton *MissingDataZoomRadioButton;
	TGroupBox *AutoModeRenderParametersGroupBox;
	TLabel *AutoModeSmoothingMinLabel;
	TLabel *AutoModeSmoothingMaxLabel;
	TPanel *AutoModeSmoothingClippingPanel;
	TTrackEditFrame *AutoModeSmoothTrackEdit;
	TCheckBox *AutoModeUseSubPixelInterpolationCheckBox;
   TComboBox *TrailSizeComboBox;
   TLabel *TrailSizeLabel;
   TPanel *TrailSizePanel;
   TSpeedButton *AdvancedSettingsButton;
   TSpeedButton *AdvancedSettingsButtonWithWarning;
	TCheckBox *AutoSourceIsAnimationCheckBox;
	TPanel *ManualTrackParametersPanel;
	TLabel *MotionMinusLabel;
	TLabel *MotionPlusLabel;
	TTrackEditFrame *MotionTrackEdit;
	TLabel *TrackingBoxSizeMinusLabel;
	TLabel *TrackingBoxSizePlusLabel;
	TTrackEditFrame *TrackingBoxSizeTrackEdit;
        void __fastcall SmoothTrackBarChange(TObject *Sender);
        void __fastcall DeletePointButtonClick(TObject *Sender);
        void __fastcall TrackingBoxSizeChange(TObject *Sender);
        void __fastcall SearchTrackBarChange(TObject *Sender);
        void __fastcall TrackButtonClick(TObject *Sender);
        void __fastcall TopUpButtonClick(TObject *Sender);
        void __fastcall TopDownButtonClick(TObject *Sender);
        void __fastcall BottomDownButtonClick(TObject *Sender);
        void __fastcall BottomUpButtonClick(TObject *Sender);
        void __fastcall LeftLeftButtonClick(TObject *Sender);
        void __fastcall RightLeftButtonClick(TObject *Sender);
        void __fastcall RightRightButtonClick(TObject *Sender);
        void __fastcall LeftRightButtonClick(TObject *Sender);
        void __fastcall AllMatButtonClick(TObject *Sender);
		void __fastcall UseMattingCBoxClick(TObject *Sender);
        void __fastcall SetMatButtonClick(TObject *Sender);
		void __fastcall UpdateTimerTimer(TObject *Sender);
        void __fastcall ShowPointCBoxClick(TObject *Sender);
        void __fastcall ShowMattingBoxCBoxClick(TObject *Sender);
        void __fastcall LoadPointsButtonClick(TObject *Sender);
        void __fastcall SavePointsButtonClick(TObject *Sender);
        void __fastcall DownTimerTimer(TObject *Sender);
        void __fastcall AutoMoveButtonMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall AutoMoveButtonMouseUp(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
    void __fastcall PrevPointButtonClick(TObject *Sender);
    void __fastcall NextPointButtonClick(TObject *Sender);
    void __fastcall GoToRefframeButtonClick(TObject *Sender);
    void __fastcall ResetRefFrameButtonClick(TObject *Sender);
    void __fastcall JitterLevelTrackBarChange(TObject *Sender);
    void __fastcall RefFrameTimeCodeEditExit(TObject *Sender);
        void __fastcall HowToStabilizeRadioButtonClick(TObject *Sender);
        void __fastcall AnchorButtonClick(TObject *Sender);
        void __fastcall PreferencesMenuItemClick(TObject *Sender);
    void __fastcall ExecButtonsToolbar_ButtonPressedNotifier(
          TObject *Sender);
        void __fastcall ColorPickButtonClick(TObject *Sender);
        void __fastcall ColorPickerStateTimerTimer(TObject *Sender);
        void __fastcall ClearOnShotChangeCBoxClick(TObject *Sender);
    void __fastcall StabilizeMotionRadioButtonClick(TObject *Sender);
    void __fastcall TrackingBoxSizeTrackEditTrackBarChange(
          TObject *Sender);
	void __fastcall TabToBeginning(TObject *Sender);
   void __fastcall RefFrameTimeCodeEditEnterOrEscapeKey(TObject *Sender);
   void __fastcall AutoModeUseSubPixelInterpolationCheckBoxClick(TObject *Sender);
   void __fastcall MissingDataRadioButtonClick(TObject *Sender);
   void __fastcall TrailSizeComboBoxChange(TObject *Sender);
	void __fastcall ManualAutoTabControlChange(TObject *Sender);
	void __fastcall UseSubPixelInterpolationCheckBoxClick(TObject *Sender);
   void __fastcall TrackingGraphClick(TObject *Sender);
   void __fastcall TrackingGraphDblClick(TObject *Sender);
   void __fastcall TrackingGraphClickSeries(TCustomChart *Sender, TChartSeries *Series,
          int ValueIndex, TMouseButton Button, TShiftState Shift, int X,
          int Y);
   void __fastcall TrackingGraphMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
   void __fastcall TrackingGraphMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
   void __fastcall AdvancedTrackingParameterChanged(TObject *Sender);
   void __fastcall AdvancedSettingsButtonClick(TObject *Sender);
	void __fastcall AutoSourceIsAnimationCheckBoxClick(TObject *Sender);


private:	// User declarations

   EMarkInMovePreference _MarkInMovePreference;
   EStabilizeMotionMode  _StabilizeMotionPreference;
   bool                  _UseSubPixelInterpolationPreference = true;
   bool                  _SubPixelCheckBoxWasClickedProgrammatically;
   bool                  _TabStopDoNotDeletePanelVisible;

   int _GoodInCount = 0;
   int _GoodOutCount = 0;
   int _oldShowCheckBox = true;

   bool _doubleClickedOnHTrackGraph = false;

   TAdvancedTrackingForm *AdvancedTrackingSettingsPopup = nullptr;

	void ConformUseSubPixelInterpolationCheckBox();
	void UpdatePresetCurrentParameters();
   void InvalidateSmoothing(void);
   void ClearCharts();

   DEFINE_CBHOOK(ClipHasChanged, TStabilizerForm);
   DEFINE_CBHOOK(ClipChanging, TStabilizerForm);
   DEFINE_CBHOOK(MarksHaveChanged, TStabilizerForm);
	DEFINE_CBHOOK(CurrentPresetHasChanged, TStabilizerForm);

	void SetRefFrameTCToDefault(void);

	// Called when anything having to do with tracking has changed
	DEFINE_CBHOOK(TrackingPointsChanged, TStabilizerForm);

   bool ReadSettings(CIniFile *ini, const string &IniSection);
   bool WriteSettings(CIniFile *ini, const string &IniSection);
	void UpdateGuiFromCurrentPreset();
	void gatherCommonParameters(CStabilizerParameters &stabilizerParameters);
	void gatherAutoModeParameters(CStabilizerParameters &stabilizerParameters);
	void gatherManualModeParameters(CStabilizerParameters &stabilizerParameters);

   void DrawChartsTag(int Tag);
   int  LastSelectedTag(void);

   TObject *GlobalSender;

	EStabilizerControlState ShowPointsOldControlState;
   bool ShowPointsOldCheckedState;

   // Exec buttons toolbar
   void ExecRenderOrPreview(EStabilizerProcessCmd buttonCommand);
   void ExecStop(void);
   void ExecCaptureToPDL(void);
   void ExecCaptureAllEventsToPDL(void);
   void ExecSetResumeTimecode(int frameIndex);
   void ExecSetResumeTimecodeToCurrent(void);
   void ExecSetResumeTimecodeToDefault(void);
   void ExecGoToResumeTimecode(void);
	void UpdateExecutionButtonToolbar(
         EToolControlState toolProcessingControlState,
			bool processingRenderFlag,
			StupidTrackingHackState stupidTrackingHackState);

   void setBusyCursor(void);
	void setIdleCursor(void);


public:		// User declarations
        __fastcall TStabilizerForm(TComponent* Owner);
   bool doesFormUseOwnWindow() { return false; };

   void formCreate(void);
   void formShow(void);
   void formHide(void);

   void SetResumeTimecodeToCurrent();
	void UpdateExecutionButtons(EToolControlState toolProcessingControlState,
										 bool processingRenderFlag,
										 StupidTrackingHackState stupidTrackingHackState);
   void UpdateTrackingPointButtons(void);
   void UpdateTrackingCharts(void);
	void UpdateToolStatusBar(int iPanel, const string &strMessage);
   void UpdateStatusBarInfo(void);

	void GatherParameters(CStabilizerParameters &stabilizerParameters);
	void CaptureParameters(CPDLElement &toolParams);
   void SetParameters(CStabilizerParameters &StabilizerParameters);
	void setCommonParameters(CStabilizerParameters &stabilizerParameters);
	void setAutoModeParameters(CStabilizerParameters &stabilizerParameters);
	void setManualModeParameters(CStabilizerParameters &stabilizerParameters);

   void SaveTrackingPointsIfNecessary(void);
   string getSaveFilename(const string &suggestedFilename);
   string getLoadFilename(const string &suggestedFilename);

   void InvalidateSmoothedData(void);
   
   void setControlFocus(EStabilizerCmd controlCommand);
   bool runExecButtonsCommand(EExecButtonId command);
	void toggleTrackPointVisibility();
   void setTrackPointsVisibility(bool flag);
	void toggleProcRegionVisibility();
	void toggleProcRegionOperation();
	void toggleClearOnShotChange();
	void toggleSubPixelInterpolation();
   void toggleSubtool();

   void updateAutoManualGuiDisplay();

};
//---------------------------------------------------------------------------
extern PACKAGE TStabilizerForm *StabilizerForm;
extern MTI_UINT32 *StabilizationFeatureTable[];

//---------------------------------------------------------------------------
#endif
