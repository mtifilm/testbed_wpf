// Stabilizer.cpp: implementation of the CStabiliz

#include "StabilizerTool.h"
#include "StabilizerCoreUnit.h"
#include "StabilizerGuiWin.h"   // Hideous - for global routines
#include "StabilizerProc.h"
#include "MathFunctions.h"
#include "err_stabilizer.h"
#include "MoveInMarkDialogUnit.h"

#include "bthread.h"
#include "ClipAPI.h"
#include "ColorPickTool.h"
#include "JobManager.h"
#include "MTIDialogs.h"
#include "MTIKeyDef.h"
#include "MTImalloc.h"
#include "MTIsleep.h"
#include "MTIstringstream.h"
#include "MTIWinInterface.h"
#include "NavigatorTool.h"
#include "PixelRegions.h"
#include "PDL.h"
#include "PresetParameterModel.h"
#include "RegionOfInterest.h"
#include "SafeClasses.h"
#include "SysInfo.h"
#include "ToolCommand.h"
#include "ToolCommandTable.h"
#include "ToolSystemInterface.h"
#include "ToolUserInputMap.h"
#include "KAutoTrackPass1Proc.h"

#include <iomanip>          // for stream manipulators

#define ANNOYING_DIALOG_TRACK_TIME_THRESHOLD 60 // seconds
#define RESTORE_TRACK_DATA_ON_LOAD 0

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

static MTI_UINT16 StabilizerToolNumber = STABILIZER_TOOL_NUMBER;

// -------------------------------------------------------------------------
// StabilizerTool Tool Default Keyboard and Mouse Button Configuration

static CUserInputConfiguration::SConfigItem StabilizerToolDefaultConfigItems[] =
{
   // StabilizerTool Tool Command             Modifiers      Action
   // + Key
   // ----------------------------------------------------------------
   {EXEC_BUTTON_PREVIEW_FRAME,              " D                KeyDown    "},
   {EXEC_BUTTON_PREVIEW_NEXT_FRAME,         " Ctrl+D           KeyDown    "},
   {EXEC_BUTTON_PREVIEW_ALL,                " Shift+D          KeyDown    "},
   {EXEC_BUTTON_RENDER_FRAME,               " G                KeyDown    "},
   {EXEC_BUTTON_RENDER_NEXT_FRAME,          " Ctrl+G           KeyDown    "},
   {EXEC_BUTTON_RENDER_ALL,                 " Shift+G          KeyDown    "},
   {EXEC_BUTTON_STOP,                       " Ctrl+Space       KeyDown    "},
   {EXEC_BUTTON_PAUSE_OR_RESUME,            " Space            KeyDown    "},
   {EXEC_BUTTON_SET_RESUME_TC,              " Return           KeyDown    "},
   {EXEC_BUTTON_CAPTURE_PDL,                " ,                KeyDown    "},
	{EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS,     " Shift+,          KeyDown    "},

   {STAB_CMD_PAUSE_TRACKING,                " C                KeyDown    "},

	{STAB_CMD_TRACK,                         " T                KeyDown    "},
	{STAB_CMD_TOGGLE_TRACK_POINT_VISIBILITY, " Shift+T          KeyDown    "},
	{STAB_CMD_TOGGLE_PROC_REGION_VISIBILITY, " Ctrl+T           KeyDown    "},

	{STAB_CMD_PRESET_1,                      " 1                KeyDown    "},
	{STAB_CMD_PRESET_2,                      " 2                KeyDown    "},
	{STAB_CMD_PRESET_3,                      " 3                KeyDown    "},
	{STAB_CMD_PRESET_4,                      " 4                KeyDown    "},
   {STAB_CMD_TOGGLE_PROC_REGION_OPERATION,  " 5                KeyDown    "},
	{STAB_CMD_ANCHOR_GROUP_NEXT,             " 6                KeyDown    "},
	{STAB_CMD_FOCUS_ON_TRACK_BOX_SIZE,       " 7                KeyDown    "},
   {STAB_CMD_FOCUS_ON_MOTION_SEARCH_AREA,   " 8                KeyDown    "},
   {STAB_CMD_FOCUS_ON_SMOOTHING,            " 9                KeyDown    "},
	{STAB_CMD_MISSING_DATA_FILL_NEXT,        " 0                KeyDown    "},

   {STAB_CMD_MOTION_GROUP_NEXT,             " Shift+1          KeyDown    "},
   {STAB_CMD_ANCHOR_GROUP_NEXT,             " Shift+2          KeyDown    "},
   {STAB_CMD_STABILIZE_GROUP_NEXT,          " Shift+3          KeyDown    "},

   {STAB_CMD_TURN_COLOR_PICKER_ON,          " Ctrl+Shift+Shift KeyDown    "},
   {STAB_CMD_TURN_COLOR_PICKER_ON,          " Ctrl+Shift+Ctrl  KeyDown    "},
   {STAB_CMD_TURN_COLOR_PICKER_OFF,         " Ctrl+Shift       KeyUp      "},
   {STAB_CMD_TURN_COLOR_PICKER_OFF,         " Shift+Ctrl       KeyUp      "},
	{STAB_CMD_SHIFT_IS_ON,                   " Shift+Shift      KeyDown    "},
   {STAB_CMD_SHIFT_IS_OFF,                  " Shift            KeyUp      "},

   {STAB_CMD_LOAD_TRACKPOINTS,              " Ctrl+L           KeyDown    "},
   {STAB_CMD_LOAD_TRACKPOINTS_NOT_MARKS,    " Shift+L          KeyDown    "},
   {STAB_CMD_SAVE_TRACKPOINTS,              " Ctrl+S           KeyDown    "},

#if DECOUPLE_MARKS_FROM_TRACKED_RANGE

   {STAB_CMD_SET_MARKS_FROM_TRACK_ARRAY,    " Ctrl+Shift+E     KeyDown    "},
   {STAB_CMD_SET_MARKS_FROM_TRACK_ARRAY,    " Ctrl+Shift+R     KeyDown    "},

#endif

};

static CUserInputConfiguration *StabilizerToolDefaultUserInputConfiguration =
      new CUserInputConfiguration(sizeof(StabilizerToolDefaultConfigItems) / sizeof(CUserInputConfiguration::SConfigItem),
      StabilizerToolDefaultConfigItems);

// -------------------------------------------------------------------------
// StabilizerTool Tool Command Table

static CToolCommandTable::STableEntry StabilizerToolCommandTableEntries[] =
{
   // StabilizerTool Tool Command            StabilizerTool Tool Command String
   // -------------------------------------------------------------

	{EXEC_BUTTON_PREVIEW_FRAME, "EXEC_BUTTON_PREVIEW_FRAME"},
	{EXEC_BUTTON_PREVIEW_NEXT_FRAME, "EXEC_BUTTON_PREVIEW_NEXT_FRAME"},
	{EXEC_BUTTON_PREVIEW_ALL, "EXEC_BUTTON_PREVIEW_ALL"},
	{EXEC_BUTTON_RENDER_FRAME, "EXEC_BUTTON_RENDER_FRAME"},
	{EXEC_BUTTON_RENDER_NEXT_FRAME, "EXEC_BUTTON_RENDER_NEXT_FRAME"},
	{EXEC_BUTTON_RENDER_ALL, "EXEC_BUTTON_RENDER_ALL"},
	{EXEC_BUTTON_STOP, "EXEC_BUTTON_STOP"},
	{EXEC_BUTTON_PAUSE_OR_RESUME, "EXEC_BUTTON_PAUSE_OR_RESUME"},
	{EXEC_BUTTON_SET_RESUME_TC, "EXEC_BUTTON_SET_RESUME_TC"},
	{EXEC_BUTTON_CAPTURE_PDL, "EXEC_BUTTON_CAPTURE_PDL"},
   {EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS, "EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS"},

	{STAB_CMD_TOGGLE_FIX, "STAB_CMD_TOGGLE_FIX"},
	{STAB_CMD_TOGGLE_TRACK_POINT_VISIBILITY, "STAB_CMD_TOGGLE_TRACK_POINT_VISIBILITY"},
   {STAB_CMD_TOGGLE_PROC_REGION_VISIBILITY, "STAB_CMD_TOGGLE_PROC_REGION_VISIBILITY"},
	{STAB_CMD_FOCUS_ON_TRACK_BOX_SIZE, "STAB_CMD_FOCUS_ON_TRACK_BOX_SIZE"},
   {STAB_CMD_FOCUS_ON_MOTION_SEARCH_AREA, "STAB_CMD_FOCUS_ON_MOTION_SEARCH_AREA"},
	{STAB_CMD_FOCUS_ON_SMOOTHING, "STAB_CMD_FOCUS_ON_SMOOTHING"},
	{STAB_CMD_MISSING_DATA_FILL_NEXT, "STAB_CMD_MISSING_DATA_FILL_NEXT"},
   {STAB_CMD_MISSING_DATA_FILL_PREV, "STAB_CMD_MISSING_DATA_FILL_PREV"},
   {STAB_CMD_TOGGLE_PROC_REGION_OPERATION, "STAB_CMD_TOGGLE_PROC_REGION_OPERATION"},
//	{STAB_CMD_FOCUS_ON_ADD_JITTER, "STAB_CMD_FOCUS_ON_ADD_JITTER"},
	{STAB_CMD_STABILIZE_GROUP_NEXT, "STAB_CMD_STABILIZE_GROUP_NEXT"},
	{STAB_CMD_STABILIZE_GROUP_PREV, "STAB_CMD_STABILIZE_GROUP_PREV"},
	{STAB_CMD_MOTION_GROUP_NEXT, "STAB_CMD_MOTION_GROUP_NEXT"},
	{STAB_CMD_MOTION_GROUP_PREV, "STAB_CMD_MOTION_GROUP_PREV"},
	{STAB_CMD_ANCHOR_GROUP_NEXT, "STAB_CMD_ANCHOR_GROUP_NEXT"},
   {STAB_CMD_ANCHOR_GROUP_PREV, "STAB_CMD_ANCHOR_GROUP_PREV"},
	{STAB_CMD_TOGGLE_CLEAR_ON_SHOT_CHANGE, "STAB_CMD_TOGGLE_CLEAR_ON_SHOT_CHANGE"},
	{STAB_CMD_TOGGLE_SUBPIXEL_INTERP,        "STAB_CMD_TOGGLE_SUBPIXEL_INTERP"},

	{STAB_CMD_PRESET_1,                      "STAB_CMD_PRESET_1"},
	{STAB_CMD_PRESET_2,                      "STAB_CMD_PRESET_2"},
	{STAB_CMD_PRESET_3,                      "STAB_CMD_PRESET_3"},
	{STAB_CMD_PRESET_4,                      "STAB_CMD_PRESET_4"},

	{STAB_CMD_TURN_COLOR_PICKER_ON, "STAB_CMD_TURN_COLOR_PICKER_ON"},
	{STAB_CMD_TURN_COLOR_PICKER_ON, "STAB_CMD_TURN_COLOR_PICKER_ON"},
	{STAB_CMD_TURN_COLOR_PICKER_OFF, "STAB_CMD_TURN_COLOR_PICKER_OFF"},
	{STAB_CMD_TURN_COLOR_PICKER_OFF, "STAB_CMD_TURN_COLOR_PICKER_OFF"},

	{STAB_CMD_LOAD_TRACKPOINTS, "STAB_CMD_LOAD_TRACKPOINTS"},
	{STAB_CMD_LOAD_TRACKPOINTS_NOT_MARKS, "STAB_CMD_LOAD_TRACKPOINTS_NOT_MARKS"},
	{STAB_CMD_SAVE_TRACKPOINTS, "STAB_CMD_SAVE_TRACKPOINTS"},
////	{STAB_CMD_CLEAR_TRACKPOINTS, "STAB_CMD_CLEAR_TRACKPOINTS"},
	{STAB_CMD_SET_MARKS_FROM_TRACK_ARRAY, "STAB_CMD_SET_MARKS_FROM_TRACK_ARRAY"},
	{STAB_CMD_SHIFT_IS_ON, "STAB_CMD_SHIFT_IS_ON"},
	{STAB_CMD_SHIFT_IS_OFF, "STAB_CMD_SHIFT_IS_OFF"},
	{STAB_CMD_TRACK,                         "STAB_CMD_TRACK"},
};

static CToolCommandTable *StabilizerToolCommandTable =
      new CToolCommandTable(sizeof(StabilizerToolCommandTableEntries) / sizeof(CToolCommandTable::STableEntry),
      StabilizerToolCommandTableEntries);

//////////////////////////////////////////////////////////////////////
// CStabilizerTool:
//////////////////////////////////////////////////////////////////////

CStabilizerTool::CStabilizerTool(const string & newToolName, MTI_UINT32 * *newFeatureTable)
    : CToolObject(newToolName,
      newFeatureTable),
      stabilizerToolSetupHandle(-1),
      trainingToolSetupHandle(-1), // Single frame preview setup
		singleFrameToolSetupHandle(-1), // Single frame render setup
		_MarkInMovePreference(MOVEPREF_PROMPT)
{
   // TTT Tool-specific constructors go here

   _resumeFrameIndex = -1;
   _processSingleFrameIndex = INVALID_FRAME_INDEX;
	_stabilizerControlState = STABILIZER_STATE_IDLE;
	_nextStateAfterSmoothing = STABILIZER_STATE_IDLE;
   _displayRect = true;
   _WarningDialogs = true;
   _BoundingBoxDefined = false;
   _MarkInMovePreference = MOVEPREF_PROMPT;

   IniFileName = CPMPIniFileName("Stabilizer");

   TrkPtsEditor = new TrackingBoxManager;
   TrkPtsEditor->setTrailSize(0);
   TrkPtsEditor->setCanMoveTrackPointsAtAnyFrame(true);
   TrkPtsEditor->setCanSelectMultiplePoints(false);

   ColorPickTool = new CColorPickTool;
   SET_CBHOOK(ColorPickerColorChanged, ColorPickTool->newColorHoveredOver);
   SET_CBHOOK(ColorPickerColorPicked, ColorPickTool->newColorPicked);

   smoothDataIsValid = false;

	CurrentTrackBoxExtentX = 10;
   CurrentTrackBoxExtentY = 10;
   CurrentSearchBoxExtentLeft = 10;
   CurrentSearchBoxExtentRight = 10;
   CurrentSearchBoxExtentUp = 10;
   CurrentSearchBoxExtentDown = 10;

	needToSendTrackingPointChangeNotification = false;
   needToUpdateTrackingPointsGui = false;
   needToAutoSave = false;
	shiftIsOn = false;

	// PRESETS!
	_userPresets = new PresetParameterModel<CStabilizerParameters>("StabilizerParameters", 4);
	_userPresets->ReadPresetsFromIni(IniFileName);
	_userPresets->SetSelectedIndexAndCopyParametersToCurrent(0);
}

CStabilizerTool::~CStabilizerTool()
{
   // Don't do this, the GUI may have a pointer to it!
	////delete _userPresets;
}

// ===================================================================
//
// Function:    toolInitialize
//
// Description: Called when the ToolManager wants to initialize
// the StabilizerTool Tool
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================

int CStabilizerTool::toolInitialize(CToolSystemInterface *newSystemAPI)
{
   int retVal;

   // Initialize the CStabilizerTool's base class, i.e., CToolObject
   // This must be done before the CStabilizerTool initializes itself
   retVal = initBaseToolObject(StabilizerToolNumber, newSystemAPI, StabilizerToolCommandTable, StabilizerToolDefaultUserInputConfiguration);
   if (retVal != 0)
   {
      // ERROR: initBaseToolObject failed
      TRACE_0(errout << "Error in StabilizerTool Tool, initBaseToolObject failed " << retVal);
      return retVal;
   }

	// Load the system API into the TrkPtsEditor and ColorPickTool.
	// Do this FIRST!
   TrkPtsEditor->loadSystemAPI(newSystemAPI);
   ColorPickTool->loadSystemAPI(newSystemAPI);

   // Create the Tool's GUI
   CreateStabilizerToolGUI();

   // Load the tool's tracking array into the editor
   TrkPtsEditor->setTrackingArray(&TrackingArray);

   _CurrentClipName = getSystemAPI()->getClipFilename();

   // Deferred event flags
   _deferredClipChangeFlag = true;
   _deferredMarkChangeFlag = false;

   // Stupid hack flag
	yetAnotherStupidHackFlag = false;
   trackingPauseHackTimePerFrame = 0.0;

   // Annoying disalog stuff
   HowLongItTookToTrackInSecs = 0;
   ShowedInMarkMoveMessage = false;
   ShowedOutMarkMoveMessage = false;
   TrackingDataHasBeenUsedToRenderAll = false;
   SingleFrameFlag = false;

   return 0;

} // end toolInitialize

// ===================================================================
//
// Function:    toolActivate
//
// Description: Called when a tool is being Activated
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CStabilizerTool::toolActivate()
{
   // First
   int retVal = CToolObject::toolActivate();
   if (retVal != 0)
   {
      return retVal;
   }

   // These go AFTER CToolObject::toolActivate()!!!
   if (_deferredClipChangeFlag)
   {
      onNewClip();
   }
	else if (_deferredMarkChangeFlag)
   {
      // Brute force clear tracking if marks or clip changed outside of tool
      if (_clearTrackingStuffOnNewShot)
      {
         ResetTracking();
      }

      onNewMarks();
   }
   _deferredClipChangeFlag = false;
   _deferredMarkChangeFlag = false;

	needToSendTrackingPointChangeNotification = false;
   needToUpdateTrackingPointsGui = false;
	SET_CBHOOK(TrackingPointsChanged, TrkPtsEditor->TrackingPointStateChange);
   SET_CBHOOK(SetNeedToAutoSave, TrkPtsEditor->TrackingParameterChange);

   // Grab and enable the tracking tool
   bool success = getSystemAPI()->LockTrackingTool();
   MTIassert(success);
   getSystemAPI()->SetTrackingToolEditor(TrkPtsEditor);
   getSystemAPI()->SetTrackingToolProgressMonitor(GetToolProgressMonitor());
   getSystemAPI()->ActivateTrackingTool();
   TrkPtsEditor->refreshTrackingPointsFrame();

   getSystemAPI()->SetGOVToolProgressMonitor(GetToolProgressMonitor());

   // Read in the bounding box in case it got changed in another tool
   auto curClip = getSystemAPI()->getClip();
   if (curClip != nullptr)
   {
		readBoundingBoxFromDesktopIni(curClip);
   }
   else
   {
      _BoundingBoxDefined = false;
   } // yuck QQQ

   JobManager jobManager;
	jobManager.SetCurrentToolCode("st");

	getSystemAPI()->SetToolNameForUserGuide("StabilizeTool");

   return 0;
}

// ===================================================================
//
// Function:    toolDeactivate
//
// Description: Called when a tool is being deactivated
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CStabilizerTool::toolDeactivate()
{
   getSystemAPI()->AcceptGOV(); // Auto-accept GOV if pending

   // Get rid of any review regions that might be present on the screen
   getSystemAPI()->ClearProvisional(true);

   // Release the tracking tool
   getSystemAPI()->SetTrackingToolEditor(nullptr);
   getSystemAPI()->SetTrackingToolProgressMonitor(nullptr);
   getSystemAPI()->DeactivateTrackingTool();
   getSystemAPI()->ReleaseTrackingTool();
   getSystemAPI()->refreshFrameCached();
	needToSendTrackingPointChangeNotification = false;
   needToUpdateTrackingPointsGui = false;
   REMOVE_CBHOOK(TrackingPointsChanged, TrkPtsEditor->TrackingPointStateChange);
   REMOVE_CBHOOK(SetNeedToAutoSave, TrkPtsEditor->TrackingParameterChange);

   DestroyAllToolSetups(true);

   auto curClip = getSystemAPI()->getClip();
   if (curClip != nullptr)
   {
      writeBoundingBoxToDesktopIni(curClip);
   }

   if (IsInColorPickMode())
   {
      ToggleColorPicker();
   }

   GetToolProgressMonitor()->SetIdle();
   getSystemAPI()->SetGOVToolProgressMonitor(nullptr);

   // Last
   return CToolObject::toolDeactivate();
}

// ===================================================================
//
// Function:    toolShow
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CStabilizerTool::toolShow()
{
   // Because Stabilizer is sensitive to the order setting marks and tracking
	// points, it gets confused when loading a PDL entry.
   // To avoid this problem, Stabilizer needs to know when a PDL Entry
	// is being loaded (as opposed to being setup by a human).
	if (getSystemAPI()->IsPDLEntryLoading())
	{
		TrkPtsEditor->resetTrackingArray();
   }

   ShowStabilizerToolGUI();

   if (!isItOkToUseToolNow())
   {
      getSystemAPI()->DisableTool(getToolDisabledReason());
   }

   UpdateTrackingPointGUI();

   return 0;
}
// ---------------------------------------------------------------------------

bool CStabilizerTool::IsShowing(void)
{
   return (IsToolVisible());
}

// ===================================================================
//
// Function:    toolHide
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CStabilizerTool::toolHide()
{
   getSystemAPI()->AcceptGOV(); // Auto-accept GOV if pending

   HideStabilizerToolGUI();
   doneWithTool();
   return (0);
}
// ---------------------------------------------------------------------------

// ===================================================================
//
// Function:    toolShutdown
//
// Description: Called when the Tool Manager wants to shut down the
// StabilizerTool Tool, typically when the main program is
// shutting down.
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================
int CStabilizerTool::toolShutdown()
{
   int retVal;

   getSystemAPI()->AcceptGOV(); // Auto-accept GOV if pending

   // Destroy StabilizerTool's GUI
   DestroyStabilizerToolGUI();

   GStabilizerTool = nullptr; // Say we are destroyed

   delete TrkPtsEditor;
   delete ColorPickTool;

   // Shutdown the StabilizerTool Tool's base class, i.e., CToolObject.
   // This must be done after the StabilizerTool shuts down itself
   retVal = shutdownBaseToolObject();
   if (retVal != 0)
   {
      // ERROR: shutdownBaseToolObject failed
      return retVal;
   }

   return retVal;
}

// ===================================================================
//
// Function:    onRedraw
//
// Description: StabilizerTool Tool's redraw handler
//
// Arguments:   frame index
//
// Returns:     return TOOL_RETURN_CODE_NO_ACTION;
//
// ===================================================================

int CStabilizerTool::onRedraw(int frameindex)
{
   if (IsDisabled() || !IsActive())
   {
      return TOOL_RETURN_CODE_NO_ACTION;
   }

   // Draw the blue scratch-limit rectangle
   if (_displayRect && _BoundingBoxDefined)
   {
      getSystemAPI()->setGraphicsColor(SOLID_BLUE);
      getSystemAPI()->drawRectangleFrame(&_BoundingBox);
   }

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

// ===================================================================
//
// Function:    onToolCommand
//
// Description: StabilizerTool Tool's Command Handler
//
// Arguments:   CToolCommand &toolCommand
//
// Returns:     Results of command handling as CToolObject's return codes,
// TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================

int CStabilizerTool::onToolCommand(CToolCommand &toolCommand)
{
   if (IsDisabled() || !IsActive())
   {
      return TOOL_RETURN_CODE_NO_ACTION;
   }

   int commandNumber = toolCommand.getCommandNumber();
   int Result = TOOL_RETURN_CODE_NO_ACTION;
   TRACE_3(errout << "Stabilizer Tool command number " << commandNumber);

   // Dispatch loop for Stabilizer Tool commands

   switch (commandNumber)
   {

   case STAB_CMD_TOGGLE_FIX:
      getSystemAPI()->ToggleProvisional();
      Result = TOOL_RETURN_CODE_PROCESSED_OK | TOOL_RETURN_CODE_EVENT_CONSUMED;
      break;

	case STAB_CMD_TOGGLE_TRACK_POINT_VISIBILITY:
		if (isInAutoTrack())
		{
			break;
		}

		ToggleTrackPointVisibility();
      Result = TOOL_RETURN_CODE_PROCESSED_OK | TOOL_RETURN_CODE_EVENT_CONSUMED;
      break;

   case STAB_CMD_TOGGLE_PROC_REGION_VISIBILITY:
      ToggleProcRegionVisibility();
      Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
      break;

   case STAB_CMD_TOGGLE_PROC_REGION_OPERATION:
      ToggleProcRegionOperation();
      Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
      break;

	case STAB_CMD_TOGGLE_CLEAR_ON_SHOT_CHANGE:
		if (isInAutoTrack())
		{
			break;
		}

		ToggleClearOnShotChange();
		Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
		break;

	case STAB_CMD_TOGGLE_SUBPIXEL_INTERP:
		ToggleSubPixelInterpolation();
		Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
		break;

	case EXEC_BUTTON_CAPTURE_PDL:
	case EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS:
	{
		// Pop up annoying dialog if there are no scene breaks!
		if (!areThereAnySceneBreaks())
		{
			if (MTI_DLG_OK != MTIWarningDialog("There are no scene breaks defined.  \nPress OK to continue capturing the PDL entry.  "))
			{
				break;
			}
      }

		bool consumed = RunExecButtonsCommand((EExecButtonId) commandNumber);
		if (consumed)
		{
			Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
		}
		break;
	}

   case EXEC_BUTTON_PREVIEW_FRAME:
   case EXEC_BUTTON_PREVIEW_NEXT_FRAME:
   case EXEC_BUTTON_PREVIEW_ALL:
   case EXEC_BUTTON_RENDER_FRAME:
   case EXEC_BUTTON_RENDER_NEXT_FRAME:
   case EXEC_BUTTON_RENDER_ALL:
   case EXEC_BUTTON_STOP:
   case EXEC_BUTTON_PAUSE_OR_RESUME:
		{
			bool consumed = RunExecButtonsCommand((EExecButtonId) commandNumber);
			if (consumed)
			{
				Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
			}
			break;
		}


   // I DON'T KNOW WHY I CAN'T JUST ADD THIS TO THE PREVIOUOS CASE,
   // but the pause state gets fucked up when I do.
   case EXEC_BUTTON_SET_RESUME_TC:
      // I expected to find STABILIZER_STATE_PAUSED, but there isn't one!
      if (GetControlState() == STABILIZER_STATE_RENDERING)
      {
         SetResumeTimecodeToCurrent();
         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
      }

      break;

   case STAB_CMD_TRACK:
		if (isInAutoTrack())
		{
			break;
		}

		ButtonCommandHandler(STABILIZER_PROC_RETRACK);
      Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
      break;

   case STAB_CMD_PAUSE_TRACKING:
      if (GetStupidTrackingHackState() == TRACKING)
      {
         RunExecButtonsCommand((EExecButtonId) EXEC_BUTTON_PAUSE_OR_RESUME);
         Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
      }

		break;

	case STAB_CMD_FOCUS_ON_SMOOTHING:
	case STAB_CMD_MISSING_DATA_FILL_NEXT:
	case STAB_CMD_MISSING_DATA_FILL_PREV:
		SetControlFocus((EStabilizerCmd) commandNumber);
		Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
		break;

	case STAB_CMD_FOCUS_ON_TRACK_BOX_SIZE:
   case STAB_CMD_FOCUS_ON_MOTION_SEARCH_AREA:
//   case STAB_CMD_FOCUS_ON_ADD_JITTER:
   case STAB_CMD_STABILIZE_GROUP_NEXT:
   case STAB_CMD_MOTION_GROUP_NEXT:
   case STAB_CMD_ANCHOR_GROUP_NEXT:
   case STAB_CMD_STABILIZE_GROUP_PREV:
   case STAB_CMD_MOTION_GROUP_PREV:
   case STAB_CMD_ANCHOR_GROUP_PREV:
		if (isInAutoTrack())
		{
			break;
		}

		SetControlFocus((EStabilizerCmd) commandNumber);
		Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
		break;

	case STAB_CMD_TURN_COLOR_PICKER_ON:
      if (!IsInColorPickMode())
      {
         ToggleColorPicker();
      }

      Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
      break;

   case STAB_CMD_TURN_COLOR_PICKER_OFF:
      if (IsInColorPickMode())
      {
         ToggleColorPicker();
      }

      Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
      break;

   case STAB_CMD_LOAD_TRACKPOINTS:
      LoadTrackingPointsFromFile();
      Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
      break;

   case STAB_CMD_LOAD_TRACKPOINTS_NOT_MARKS:
      LoadTrackingPointsFromFile(false);
      Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
      break;

   case STAB_CMD_SAVE_TRACKPOINTS:
      SaveTrackingPointsToFile();
      Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
      break;

#if DECOUPLE_MARKS_FROM_TRACKED_RANGE

   case STAB_CMD_SET_MARKS_FROM_TRACK_ARRAY:
      if (TrackingArray.getColor() != TrackingBoxSet::ETPRed)
      {
         // OUT, then IN - I think that has a better chance of success!!
         getSystemAPI()->setMarkOut(TrackingArray.getOutFrame());
         getSystemAPI()->setMarkIn(TrackingArray.getInFrame());
         getSystemAPI()->goToFrameSynchronous(getSystemAPI()->getMarkIn());
      }
      break;

#endif

   case STAB_CMD_SHIFT_IS_ON:
      // Don't "consume" this.
      shiftIsOn = true;
      break;

	case STAB_CMD_SHIFT_IS_OFF:
      // Don't "consume" this.
      shiftIsOn = false;
      break;

   // PRESETS!
	case STAB_CMD_PRESET_1:
		if (isInAutoTrack())
		{
			break;
		}

		_userPresets->SetSelectedIndexAndCopyParametersToCurrent(0);
		Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
		break;

	case STAB_CMD_PRESET_2:
		if (isInAutoTrack())
		{
			break;
		}

		_userPresets->SetSelectedIndexAndCopyParametersToCurrent(1);
      Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
		break;

	case STAB_CMD_PRESET_3:
		if (isInAutoTrack())
		{
			break;
		}

		_userPresets->SetSelectedIndexAndCopyParametersToCurrent(2);
      Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
		break;

	case STAB_CMD_PRESET_4:
		if (isInAutoTrack())
		{
			break;
		}

		_userPresets->SetSelectedIndexAndCopyParametersToCurrent(3);
      Result = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
		break;

   default:

      break;
   }

   UpdateTrackingPointGUI();

   if (IsInColorPickMode() && ((Result & TOOL_RETURN_CODE_EVENT_CONSUMED) != 0) && (commandNumber != STAB_CMD_TURN_COLOR_PICKER_ON))
   {
      ToggleColorPicker();
   }

   return (Result);

} // onToolCommand

// ===================================================================
//
// Function:    onUserInput
//
// Description: Navigator Tool's User Input Handler
//
// Arguments:   CUserInput &userInput
//
// Returns:     Results of command handling as CToolObject's return codes,
// TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================
int CStabilizerTool::onUserInput(CUserInput &userInput)
{
   // default: user input will be passed back to the tool manager
   int retVal = TOOL_RETURN_CODE_NO_ACTION;

   // If we're disabled or there's no clip, don't do anything
   if ((!IsDisabled()) && IsShowing() && getSystemAPI()->isAClipLoaded())
   {
      // pass the event to the colorpicker
      switch (userInput.action)
      {
      case USER_INPUT_ACTION_MOUSE_BUTTON_DOWN:
         retVal = ColorPickTool->onMouseDown(userInput);
         break;
      case USER_INPUT_ACTION_MOUSE_BUTTON_UP:
         retVal = ColorPickTool->onMouseUp(userInput);
         break;
      default:
         // We don't care about the other cases!
         break;
      }
   }

   // If we didn't handle the mouse event, feed it back through the
   // onToolCommand interface!
   if (retVal == TOOL_RETURN_CODE_NO_ACTION)
   {
      retVal = CToolObject::onUserInput(userInput);
   }

   return retVal;
}

// ===================================================================
//
// Function:    onMouseMove
//
// Description: Navigator Tool's Mouse Movement Handler
//
// Arguments:   CUserInput &userInput
//
// Returns:     Results of command handling as CToolObject's return codes,
// TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================
int CStabilizerTool::onMouseMove(CUserInput &userInput)
{
   // If we're disabled or there's no clip, don't do anything
   if ((!IsDisabled()) && IsShowing() && getSystemAPI()->isAClipLoaded())
   {
      ColorPickTool->onMouseMove(userInput);
   }

   // Always pass through the mouse move so panning will continue to work
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onNewMarks
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CStabilizerTool::onNewMarks()
{
   if (!IsLicensed())
   { // NOT IsDisabled(), which, WTF, is true when invisible
      return TOOL_RETURN_CODE_NO_ACTION;
   }

   if (IsDisabled() || !IsActive())
   {
      _deferredMarkChangeFlag = true;
      return TOOL_RETURN_CODE_NO_ACTION;
	}

	// A hack for Larry - if tracking is paused, stop it.
	if (GetControlState() == STABILIZER_STATE_TRACKING_PAUSED)
   {
		// Tracking is actually stopped when we claim it's paused,
		// so we don't have to call StopTracking().
		SetControlState(STABILIZER_STATE_TRACKING_COMPLETE);
	}

	if (!getSystemAPI()->IsPDLRendering())
	{
		AutoSaveTrackingPoints();
	}

   // CAREFUL!!! canMarksBeMoved HAS SIDE EFFECTS - for example, if it
   // decides the mark CAN'T be moved it sets it back to the old setting!!
   if (canMarksBeMoved())
   {
		TrkPtsEditor->setRangeFromMarks();
		StabilizerForm->InvalidateSmoothedData();
      StabilizerForm->UpdateStatusBarInfo();
   }

   MarksChange.Notify();

   TrkPtsEditor->refreshTrackingPointsFrame();

   return (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
}

// ===================================================================
//
// Function:    onChangingClip
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CStabilizerTool::onChangingClip()
{
   if (IsDisabled())
   { // !IsActive() is OK!!!
      return TOOL_RETURN_CODE_NO_ACTION;
   }

   AutoSaveTrackingPoints();

   ClipChanging.Notify();

   auto curClip = getSystemAPI()->getClip();
   if (curClip != nullptr)
   {
      writeBoundingBoxToDesktopIni(curClip);
   }

   getSystemAPI()->AcceptGOV(); // probably unnecessary

   getSystemAPI()->ClearProvisional(false);

   DestroyAllToolSetups(false);

   if (IsInColorPickMode())
   {
      ToggleColorPicker();
   }

   ResetTracking();

   return (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
}

// ===================================================================
//
// Function:    onNewClip
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CStabilizerTool::onNewClip()
{
   if (!IsLicensed())
   { // NOT IsDisabled(), might want to Enable!
      return TOOL_RETURN_CODE_NO_ACTION;
   }

   if (!IsActive())
   {
      _deferredClipChangeFlag = true;
      return TOOL_RETURN_CODE_NO_ACTION;
   }

   // Do we need to disable or re-enable the tool?
   if (isItOkToUseToolNow())
   {
      getSystemAPI()->EnableTool();
   }
   else
   {
      getSystemAPI()->DisableTool(getToolDisabledReason());
   }

   // these are inited anew
   _CurrentClipName = getSystemAPI()->getClipFilename();

   // When a clip is loaded, try to read bounding box from the desktop.ini file
   auto curClip = getSystemAPI()->getClip();
   if (curClip != nullptr)
   {
      readBoundingBoxFromDesktopIni(curClip);
   }
   else
   {
      _BoundingBoxDefined = false;
   } // yuck QQQ

   // Clear out all tracking info
   ResetTracking();

   // Re-autofind bounding box if there wasn't one associated with the clip
   if (!BoundingBoxDefined())
   {
      FindBoundingBox();
   }

   // This allows the tool to notify the gui
   ClipChange.Notify();

   return (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
}

// ===================================================================
//
// Function:    onChangingFraming
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CStabilizerTool::onChangingFraming()
{
   if (IsDisabled() || !IsActive())
   {
      return TOOL_RETURN_CODE_NO_ACTION;
   }

   getSystemAPI()->AcceptGOV(); // Auto-accept GOV if pending

   getSystemAPI()->ClearProvisional(true);

   DestroyAllToolSetups(false);

   // Clear out all tracking info
   ResetTracking();

   return (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
}

// ===================================================================
//
// Function:    onSwitchSubtool
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CStabilizerTool::onSwitchSubtool()
{
   if (IsDisabled() || !IsActive())
   {
      return TOOL_RETURN_CODE_NO_ACTION;
   }

   ToggleSubtool();

   return (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
}

// ===================================================================
//
// Function:    onHeartbeat
//
// Description:
// This is an awful hack - a state machine parallel to the one in
// the tool infrastructure, but that one was simply not designed to
// to support the common three-part scenario:
//
// (1) Perform a readonly preprocessing pass over the frame range
//
// (2) Perform a potentially lengthy processing phase
//
// (3) Make a rendering pass through the frame range
//
// Here the preprocessing phase consists of tracking points, and
// the processing involves computing a smooth trajectory through the
// tracked data, and the third phase renders the stabilized frames.
//
// I tried to keep this as simple as possible because I eventually want
// to migrate it into the infrastructure. A second goal I had in doing
// this is that I don't want this modult to have to override the
// MonitorFrameProcessing() base method.
//
// Arguments:  Nope
//
// Returns: 0. I guess I should just declare it 'void'
//
// ===================================================================

int CStabilizerTool::onHeartbeat()
{
   if (IsDisabled() || !IsActive())
   {
	  return TOOL_RETURN_CODE_NO_ACTION;
	}

	switch (GetControlState())
	{
	default:
	case STABILIZER_STATE_IDLE:
		// Do nothing, duh
		break;

	case STABILIZER_STATE_NEED_TO_RUN_TRACKER:
		StartTracking();
		break;

	case STABILIZER_STATE_TRACKING:
		// Do nothing -- tracking tool is doing all the work.
		break;

	case STABILIZER_STATE_TRACKING_PAUSED:
		GetToolProgressMonitor()->SetStatusMessage("Tracking paused");
		break;

	case STABILIZER_STATE_TRACKING_COMPLETE:
		UpdateTrackingPointGUI();
		SetControlState(STABILIZER_STATE_NEED_TO_RUN_SMOOTHER);
		smoothDataIsValid = false;
		if (!getSystemAPI()->IsPDLRendering())
		{
			AutoSaveTrackingPoints();
		}
		/* FALL THROUGH */

	case STABILIZER_STATE_NEED_TO_RUN_SMOOTHER:
		SetControlState(STABILIZER_STATE_SMOOTHING);
		StartSmoothing();
		break;

	case STABILIZER_STATE_SMOOTHING:
		MonitorSmoothing();
		break;

	case STABILIZER_STATE_SMOOTHING_COMPLETE:
		needToUpdateTrackingPointsGui = true;

		// Next could be IDLE, NEED_TO_RUN_SMOOTHER, or NEED_TO_RUN_RENDERER
		SetControlState(getNextStateAfterSmoothing());

		if (getNextStateAfterSmoothing() == STABILIZER_STATE_IDLE)
		{
			// Tracking only - do full stop
			// StopToolProcessing();  no - processed in TrackTool now!
			UpdateExecutionGUI(TOOL_CONTROL_STATE_STOPPED, false);
			GetToolProgressMonitor()->SetStatusMessage("Tracking complete");
		}
		else
		{
			setNextStateAfterSmoothing(STABILIZER_STATE_IDLE);
		}
		break;

	case STABILIZER_STATE_NEED_TO_RUN_RENDERER:
		StartRendering();
		break;

	case STABILIZER_STATE_RENDERING:
		// Do nothing - monitored by MonitorFrameProcessing()
		break;

	case STABILIZER_STATE_RENDERING_COMPLETE:
		FinishRendering();
		SetControlState(STABILIZER_STATE_IDLE);
		break;

	case STABILIZER_STATE_GOT_AN_ERROR:
		StopToolProcessing();
		SetControlState(STABILIZER_STATE_IDLE);
		break;

    // AUTO TRACKING
	case STABILIZER_STATE_NEED_TO_RUN_AUTOTRACK_PASS_1:
		HandleKPass1Command();
		break;

	case STABILIZER_STATE_AUTOTRACKING_PASS_1:
		break;

	case STABILIZER_STATE_AUTOTRACKING_PAUSED:
		break;

	case STABILIZER_STATE_AUTOTRACKING_COMPLETE:
		SetControlState(STABILIZER_STATE_NEED_TO_RUN_AUTOLOAD);
		break;

	case STABILIZER_STATE_NEED_TO_RUN_AUTOLOAD:
		{
			auto autoTrackingString = getAutoTrackingString();
//         DBTRACE(autoTrackingString);
			if (autoTrackingString != "")
			{
				std::istringstream  autoTrackingStream(autoTrackingString);

				// NOTE: 'Restore marks' arg MUST BE 'true, else tracking data is cleared.
				//       'Restore processing region' arg MUST BE 'false' because it
				//       appears to not be being set right in the autoTrackingStream,
            //       and it's annoying the shit out of Larry.
				LoadTrackingPoints(autoTrackingStream, true, false);
            GetTrackingArrayRef().toString();
				GetTrackingArrayRef().checkConsistency();

				needToUpdateTrackingPointsGui = true;
				SelectTrackingPointRelative(1);
				SetControlState(STABILIZER_STATE_TRACKING_COMPLETE);
			}
			else
			{
				// Aborted
				setNextStateAfterSmoothing(STABILIZER_STATE_IDLE);
				SetControlState(STABILIZER_STATE_IDLE);
			}
		}
		break;
	}

	if (needToUpdateTrackingPointsGui)
	{
		needToUpdateTrackingPointsGui = false;
		UpdateTrackingPointGUI();
	}

   // Do stuff that needs to get done in the main thread
   // (i.e. anything that modifies the GUI). We are guaranteed to
   // be executing in the main thread here.
   if (needToSendTrackingPointChangeNotification)
   {
      needToSendTrackingPointChangeNotification = false;
      TrackingPointStateChange.Notify();
   }

   return 0;
}

bool CStabilizerTool::NotifyTrackingStarting()
{
	CAutoErrorReporter autoErr("CStabilizerTool::NotifyTrackingStarting", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

	if (isInAutoTrack())
	{
		// Tell TrackTool to leave it be, then we will get called to process 'T' key.
      return false;
	}

	if (!AreMarksValid() != 0)
	{
		autoErr.errorCode = -1;
		autoErr.msg << "Marks are invalid!  ";
		SetControlState(STABILIZER_STATE_GOT_AN_ERROR);
		return false;
	}

   if (!getSystemAPI()->isAClipLoaded())
	{
      autoErr.errorCode = -1;
		autoErr.msg << "No clip is open! ";
		SetControlState(STABILIZER_STATE_GOT_AN_ERROR);
		return false;
	}

	// If we are smoothing we try to stop it, then run tracking when it's done.
	if (GetControlState() == STABILIZER_STATE_SMOOTHING)
	{
		if (getNextStateAfterSmoothing() == STABILIZER_STATE_IDLE)
		{
			setNextStateAfterSmoothing(STABILIZER_STATE_TRACKING);
			_deferredToolProcessingCommand = TOOL_PROCESSING_CMD_INVALID;
			StopSmoothing();
		}

		// Tell tracking tool to not do tracking.
		return false;
	}

	if (isAllTrackDataValid() && !getSystemAPI()->IsPDLRendering())
	{
		// All track data is valid. If we re-track, we're going to end up with
		// the exact same tracks, so ask the user if that's what they want to do.
		if (MTI_DLG_OK == _MTIConfirmationDialog(Handle, "Already fully tracked and nothing has changed.    \nDo you really want to re-track?"))
		{
			 InvalidateAllTrackingData();
		}
	}

	if (isAllTrackDataValid())
	{
		// Don't need to run the track tool - check the state of smoothing
		if (!IsSmoothedDataValid())
      {
			// All tracking data was valid, so do smoothing instead
			SetControlState(STABILIZER_STATE_NEED_TO_RUN_SMOOTHER);
      }
      else
      {
			// Signal that everything is done!
			SetControlState(STABILIZER_STATE_SMOOTHING_COMPLETE);
		}

		GetToolProgressMonitor()->SetIdle();

		// Tell tracking tool to not do tracking.
		return false;
	}

   // Here -1 because while the in frame is technically already tracked, the tracking tool is expecting it to not be counted yet.
	auto preElapsed = TrackingArray.getFirstUntrackedFrame() - TrackingArray.getInFrame() - 1;
	auto preTimePerFrame = preElapsed > 0 ? trackingPauseHackTimePerFrame : 0.0;
	auto totalFrameCount = TrackingArray.getOutFrame() - TrackingArray.getInFrame();
	GetToolProgressMonitor()->SetFrameCount(totalFrameCount, 1, preElapsed, preTimePerFrame);

	SetControlState(STABILIZER_STATE_TRACKING);
	GetToolProgressMonitor()->SetStatusMessage("Tracking");

	// We want to remember how long tracking took so we can decide later
	// whether it's worth putting up annoying dialogs when marks are moved
	TrackingTimer.Start();

	needToUpdateTrackingPointsGui = true;

	return true;
}

void CStabilizerTool::NotifyTrackingDone(int errorCode)
{
	CAutoErrorReporter autoErr("CStabilizerTool::NotifyTrackingStarting", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

	// PAUSE HACK: If the state is STABILIZER_STATE_TRACKING_PAUSED then we are
	// actually pausing, not stopping, so do NOT change the state to "COMPLETE"!!
	if (GetControlState() == STABILIZER_STATE_TRACKING)
	{
		if (errorCode != 0)
      {
			// Tracking got an error
			autoErr.errorCode = errorCode;
			autoErr.msg << "Tracking got an error!  ";
			SetControlState(STABILIZER_STATE_GOT_AN_ERROR);
      }
		else
      {
         // Successfully completed tracking
			SetControlState(STABILIZER_STATE_TRACKING_COMPLETE);
      }
	}
	else if (GetControlState() == STABILIZER_STATE_TRACKING_PAUSED)
	{
		trackingPauseHackTimePerFrame = GetToolProgressMonitor()->GetTimePerFrameInSeconds();
	}

	// Remember how long it took
	HowLongItTookToTrackInSecs = int(TrackingTimer.Read() / 1000.0);
	ShowedInMarkMoveMessage = false;
	ShowedOutMarkMoveMessage = false;
	TrackingDataHasBeenUsedToRenderAll = false;
	UpdateExecutionButtons(TOOL_CONTROL_STATE_STOPPED, false, GetStupidTrackingHackState());
}

void CStabilizerTool::NotifyTrackingCleared()
{
	SetTrackPointsVisibility(!isInAutoTrack());
}

void CStabilizerTool::TrackingPointsChanged(void *Sender)
{
   // This may come from a thread - need to relay notifications from
   // the main thread only to be able to update GUIs!
	needToSendTrackingPointChangeNotification = true;
}

void CStabilizerTool::SetNeedToAutoSave(void *Sender)
{
   // Careful - this may come from a thread!
   needToAutoSave = true;
}

void CStabilizerTool::StartTracking()
{
	if (TrkPtsEditor->wasTrackingDataInvalidatedByRendering())
	{
		TrkPtsEditor->invalidateAllTrackingData();
	}

	getSystemAPI()->StartTracking();
}

void CStabilizerTool::StopTracking(bool reallyPausing)
{
	if (GetControlState() == STABILIZER_STATE_TRACKING_PAUSED)
	{
		// It's already stopped!
		SetControlState(STABILIZER_STATE_IDLE);
		return;
	}

	// If we actually finished tracking, don't try to stop it,
	// that just puts us in a confused state.
	if (isAllTrackDataValid())
	{
      return;
	}

	// IMPORTANT: Need to do this before the StopTracking call
	// because it gets looked at in the NotifyTrackingDone callback!
	if (reallyPausing)
	{
		SetControlState(STABILIZER_STATE_TRACKING_PAUSED);
	}

	getSystemAPI()->StopTracking();
}

void CStabilizerTool::StartSmoothing(void)
{
	CAutoErrorReporter autoErr("CStabilizerTool::NotifyTrackingStarting", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

	if (!isAllTrackDataValid())
	{
//// Removed because it was popping up an error on "stop" command.
////		autoErr.errorCode = -1;
////		autoErr.msg << "Tracking failed!  ";
		SetControlState(STABILIZER_STATE_GOT_AN_ERROR);
		return;
	}

	int errorCode = DoSmoothing();
	if (errorCode != 0)
	{
		autoErr.errorCode = errorCode;
		autoErr.msg << "Smoothing failed!  ";
		SetControlState(STABILIZER_STATE_GOT_AN_ERROR);
	}
}

void CStabilizerTool::MonitorSmoothing(void)
{
	// QQQ
}

void CStabilizerTool::StopSmoothing()
{
	// QQQ
}

void CStabilizerTool::StartRendering(void)
{
	CAutoErrorReporter autoErr("CStabilizerTool::NotifyTrackingStarting", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

	EToolProcessingCommand commandToRun = _deferredToolProcessingCommand;
	_deferredToolProcessingCommand = TOOL_PROCESSING_CMD_INVALID;

	// Workaround for a bug I can't seem to track down! ====================
	if (getNextStateAfterSmoothing() == STABILIZER_STATE_NEED_TO_RUN_RENDERER)
	{
		setNextStateAfterSmoothing(STABILIZER_STATE_IDLE);
	}

	if (commandToRun == TOOL_PROCESSING_CMD_INVALID)
	{
		SetControlState(STABILIZER_STATE_IDLE);
		return;
	}
	// ========================== End of workartound.

	SetControlState(STABILIZER_STATE_RENDERING);

	StartToolProcessing(commandToRun, false);

	// This used to be in "FinishRendering" but it really applies as soon
	// as the first frame of the range is processed.
	TrackingDataHasBeenUsedToRenderAll = true;
}

void CStabilizerTool::FinishRendering(void)
{
   if (renderFlag && !SingleFrameFlag)
   {
      // Per larry 2010-06, turn "Show tracking points" off,
      // and return to the begin frame
      SetTrackPointsVisibility(false);
      getSystemAPI()->goToFrameSynchronous(getSystemAPI()->getMarkIn());
	}
}

void CStabilizerTool::InvalidateAllTrackingData()
{
	TrkPtsEditor->invalidateAllTrackingData();
	MarkSmoothedDataInvalid();
	TrackingDataHasBeenUsedToRenderAll = false;
}

bool CStabilizerTool::isAllTrackDataValid() const
{
   if (GetNumberOfTrackingBoxes() < 1)
   {
      return false;
   }

	return TrkPtsEditor->isTrackingDataValidOverEntireRange()
				&& !TrkPtsEditor->wasTrackingDataInvalidatedByRendering();
}

bool CStabilizerTool::IsSmoothedDataValid() const
{
	return (isAllTrackDataValid() && smoothDataIsValid);
}

TrackingBoxSet &CStabilizerTool::GetTrackingArrayRef()
{
   return TrackingArray;
}

TrackingBoxSet &CStabilizerTool::GetRobustArrayRef()
{
   return RobustArray;
}

TrackingBoxSet &CStabilizerTool::GetSmoothedArrayRef()
{
   return SmoothRobustArray;
}

void CStabilizerTool::SetTrackingBoxSizes(int newTrackBoxXRadius, int newTrackBoxYRadius, int newSearchLeftDistance,
      int newSearchRightDistance, int newSearchUpDistance, int newSearchDownDistance)
{
   CurrentTrackBoxExtentX = newTrackBoxXRadius;
   CurrentTrackBoxExtentY = newTrackBoxYRadius;
   CurrentSearchBoxExtentLeft = newSearchLeftDistance;
   CurrentSearchBoxExtentRight = newSearchRightDistance;
   CurrentSearchBoxExtentUp = newSearchUpDistance;
   CurrentSearchBoxExtentDown = newSearchDownDistance;

	if (CurrentTrackBoxExtentX != TrackingArray.getTrackingBoxRadius() || CurrentSearchBoxExtentLeft != TrackingArray.getSearchBoxRadius())
	{
		InvalidateAllTrackingData();
      TrackingArray.setTrackingBoxRadius(CurrentTrackBoxExtentX);
      TrackingArray.setSearchBoxRadius(CurrentSearchBoxExtentLeft);
   }

   TrkPtsEditor->refreshTrackingPointsFrame();
}

void CStabilizerTool::SetAdvancedTrackingParameters(
   ETrackingChannel trackingChannel,
   bool degrainBeforeTracking,
   bool applyLutBeforeTracking,
   bool applyAutoContrastBeforeTracking)
{
   CurrentTrackingChannel = trackingChannel == R ? 0 : (trackingChannel == G ? 1 : (trackingChannel == B ? 2 : -1));
   CurrentDegrainBeforeTracking = degrainBeforeTracking;
   CurrentApplyLutBeforeTracking = applyLutBeforeTracking;
   CurrentApplyAutoContrastBeforeTracking = applyAutoContrastBeforeTracking;

	if (CurrentTrackingChannel != TrackingArray.getChannel()
   || CurrentDegrainBeforeTracking != TrackingArray.getDegrainFlag()
   || CurrentApplyLutBeforeTracking != TrackingArray.getApplyLutFlag()
   || CurrentApplyAutoContrastBeforeTracking != TrackingArray.getAutoContrastFlag())
	{
		InvalidateAllTrackingData();
      TrackingArray.setChannel(CurrentTrackingChannel);
      TrackingArray.setDegrainFlag(CurrentDegrainBeforeTracking);
      TrackingArray.setApplyLutFlag(CurrentApplyLutBeforeTracking);
      TrackingArray.setAutoContrastFlag(CurrentApplyAutoContrastBeforeTracking);
   }

   TrkPtsEditor->refreshTrackingPointsFrame();
}

int CStabilizerTool::GetLastSelectedTag()
{
   return TrkPtsEditor->lastSelectedTag;
}

// This call is used to determine whether or not to warn the user if
// a master clip is about to be stabilized
bool CStabilizerTool::DoesToolPreserveHistory()
{
   return false;
}

// ===================================================================
//
// Function:    GetToolProcessingControlState
//
// Description: This is a stupid hack to make PDL rendering work correctly.
// The problem is that we will first run the TrackTool, then
// when that's done we run the StabilizerProc.
// But the PDL sees that the toolProcessingControlState is STOPPED
// while the TrackTool is running so it assumes that rendering has
// been completed.
//
// Arguments:
//
// Returns:
//
// ===================================================================

EToolControlState CStabilizerTool::GetToolProcessingControlState()
{
   EToolControlState retVal = toolProcessingControlState;

   switch (GetControlState())
   {
   case STABILIZER_STATE_RENDERING_COMPLETE:
   case STABILIZER_STATE_GOT_AN_ERROR:
   case STABILIZER_STATE_IDLE:

      break;

   default:

      retVal = TOOL_CONTROL_STATE_RUNNING;
      break;
   }

   return retVal;
}

// ===================================================================
//
// Function:    makeToolProcessorInstance
//
// Description: Override ToolObject to make a frame procesor
//
// Arguments:   newEmergencyStopFlagPtr - I don't think that works!
//
// Returns:     return an instance of our ToolProcessor variant
//
// ===================================================================

CToolProcessor* CStabilizerTool::makeToolProcessorInstance(const bool *newEmergencyStopFlagPtr)
{
	switch (_toolProcessorTypeToMake)
	{
	case PROC_TYPE_KPASS_2:
		MTIassert(false);

	case PROC_TYPE_KPASS_1:
	   return new CKAutoTrackPass1Proc(GetToolNumber(), GetToolName(), getSystemAPI(), newEmergencyStopFlagPtr, GetToolProgressMonitor());

	case PROC_TYPE_RENDER_PROCESS:
	   return new CStabilizerProc(GetToolNumber(), GetToolName(), getSystemAPI(), newEmergencyStopFlagPtr, GetToolProgressMonitor());
	}

	MTIassert(false);
	return nullptr;
}

// ===================================================================
//
// Function:    makeToolProcessorInstance
//
// Description: Override ToolObject to make a frame procesor
//
// Arguments:   newEmergencyStopFlagPtr - I don't think that works!
//
// Returns:     return an instance of our ToolProcessor variant
//
// ===================================================================

int CStabilizerTool::StartToolProcessing(EToolProcessingCommand processingType, bool newHighlightFlag)
{
   int retVal = 0;

	// NASTY hack to get PDL to track and/or smooth brfore rendering
	if (getSystemAPI()->IsPDLRendering()
	&& !yetAnotherStupidHackFlag
	&& processingType == TOOL_PROCESSING_CMD_RENDER)
	{
		// stupid fricken hack flag because we will reenter here after tracking!!
		yetAnotherStupidHackFlag = true;
		setNextStateAfterSmoothing(STABILIZER_STATE_NEED_TO_RUN_RENDERER);
		_deferredToolProcessingCommand = TOOL_PROCESSING_CMD_RENDER;

		// When PDL, need to always start with tracking! Tracking may not be needed, but smoothing always is!
		SetControlState(isInAutoTrack()
								? STABILIZER_STATE_NEED_TO_RUN_AUTOTRACK_PASS_1
								: STABILIZER_STATE_NEED_TO_RUN_TRACKER);
	}
	else
   {
		retVal = CToolObject::StartToolProcessing(processingType, newHighlightFlag);
      // In case we are canceled
      if (retVal == 1)
      {
			SetControlState(STABILIZER_STATE_IDLE);
		}
   }

   return retVal;
}

// ===================================================================
// Status bar stuff
// ===================================================================

void CStabilizerTool::getStatusMessage(string &sMessage)
{
   _MessageLocker.Lock("CStabilizerTool::getStatusMessage");
   sMessage = _StatusMessage;
   _MessageLocker.UnLock();
}
// ---------------------------------------------------------------------------

void CStabilizerTool::setStatusMessage(const string &NewMessage)
{
   _MessageLocker.Lock("CStabilizerTool::setStatusMessage");
   _StatusMessage = NewMessage;
   _MessageLocker.UnLock();
}
// ---------------------------------------------------------------------------

void CStabilizerTool::getTimeMessage(string &sMessage)
{
   _TimeMessageLocker.Lock("CStabilizerTool::getTimeMessage");
   sMessage = _TimeMessage;
   _TimeMessageLocker.UnLock();
}
// ---------------------------------------------------------------------------

void CStabilizerTool::setTimeMessage(const string &NewMessage)
{
   _TimeMessageLocker.Lock("CStabilizerTool::setTimeMessage");
   _TimeMessage = NewMessage;
   _TimeMessageLocker.UnLock();
}
// ---------------------------------------------------------------------------

bool CStabilizerTool::FindBoundingBox(int R, int G, int B, int X, int Y, int slack)
{
   const CImageFormat* imgFmt = getSystemAPI()->getVideoClipImageFormat();
   if (imgFmt == nullptr)
   {
      return false;
   }

	// Compute size of largest buffer possible.
	int pxlComponentCount = imgFmt->getComponentCount()
									+ ((imgFmt->getComponentCount() == 4 || imgFmt->getAlphaMatteType() == IF_ALPHA_MATTE_NONE)
											? 0
											: 1);
	int nPicCol = imgFmt->getPixelsPerLine();
   int nPicRow = imgFmt->getLinesPerFrame();
	bool bRGB = (imgFmt->getPixelComponents() == IF_PIXEL_COMPONENTS_RGB) || (imgFmt->getPixelComponents() == IF_PIXEL_COMPONENTS_BGR) ||
         (imgFmt->getPixelComponents() == IF_PIXEL_COMPONENTS_RGBA) || (imgFmt->getPixelComponents() == IF_PIXEL_COMPONENTS_BGRA) ||
         (imgFmt->getPixelComponents() == IF_PIXEL_COMPONENTS_Y) || (imgFmt->getPixelComponents() == IF_PIXEL_COMPONENTS_YYY);

   RECT _ActiveBox = imgFmt->getActivePictureRect();

   // Hack - assume slack was for 10-bit data, so if it's 8, divide by 2
   // (dividing by 4 makes it too narrow, I think
   if (imgFmt->getPixelPacking() == IF_PIXEL_PACKING_8Bits_IN_1Byte)
   {
      slack /= 2;
   }

   // Allocate buffer
	int pxlCnt = nPicRow * nPicCol;
	int totWds = (2 * pxlCnt * pxlComponentCount + ((pxlCnt + 7) / 8) + 1) / 2;
   MTI_UINT16 *BaseImage = new MTI_UINT16[totWds];

   getSystemAPI()->getLastFrameAsIntermediate(BaseImage);
   setBoundingBoxInternal(FindMatBox(BaseImage, nPicCol, nPicRow, slack, _ActiveBox, bRGB, R, G, B, X, Y));

   delete[]BaseImage;
   return true;
}
// ---------------------------------------------------------------------------

int CStabilizerTool::readStabilizerParameters()
{
   if (IniFileName.empty())
   {
      return 0;
   }

   CIniFile* ini = CreateIniFile(IniFileName);
   if (ini == nullptr)
   {
      return 0;
   }

   delete ini;

   return 0;
}
// ---------------------------------------------------------------------------

void CStabilizerTool::writeStabilizerParameters()
{
   if (IniFileName.empty())
   {
      return;
   }

   CIniFile* ini = CreateIniFile(IniFileName);
   if (ini == nullptr)
   {
      return;
   }

   delete ini;
}
// ---------------------------------------------------------------------------

TrackingBoxManager *CStabilizerTool::GetTrackingPointsEditor(void)
{
   return TrkPtsEditor;
}
// ---------------------------------------------------------------------------

int CStabilizerTool::onCapturePDLEntry(CPDLElement &pdlEntryToolParams)
{
   if (IsDisabled())
   {
      return 0;
   }

   CaptureGUIParameters(pdlEntryToolParams);

   // Write Tracking Points
   TrackingArray.setBoundingBox(getBoundingBox());
   TrackingArray.AddToPDLEntry(pdlEntryToolParams);

   return 0;
}
// ---------------------------------------------------------------------------

int CStabilizerTool::onGoToPDLEntry(CPDLEntry &pdlEntry)
{
   if (IsDisabled())
   {
      return 0;
   }

   int retVal;

   CPDLEntry_Tool *pdlEntryTool = pdlEntry.toolList[0];
   CPDLElement *toolAttribRoot = pdlEntryTool->parameters;

   CStabilizerParameters toolParams;
   TrackingBoxSet newTrackingArray;

   for (unsigned int i = 0; i < toolAttribRoot->GetChildElementCount(); ++i)
   {
      CPDLElement *toolAttribs = toolAttribRoot->GetChildElement(i);
      string elementName = toolAttribs->GetElementName();
      if (elementName == "StabilizerParameters")
      {
         toolParams.ReadPDLEntry(toolAttribs);
         SetGUIParameters(toolParams);
      }
		else if (elementName == "TrackingBoxSet")
      {
         newTrackingArray.ReadPDLEntry(toolAttribs);

         setBoundingBox(newTrackingArray.getBoundingBox());

         SetTrackingPoints(newTrackingArray);

         TrkPtsEditor->lastSelectedTag = INVALID_FRAME_INDEX;
         TrkPtsEditor->refreshTrackingPointsFrame();
			MarkSmoothedDataInvalid();
         StabilizerForm->UpdateStatusBarInfo();
      }
   }

	// Oh good lord...
   yetAnotherStupidHackFlag = false;

   return 0;
}
// ---------------------------------------------------------------------------

// ===================================================================
// Stuff called from the GUI
// ===================================================================

void CStabilizerTool::ResetResumeFrameIndex(int resumeFrame)
{
   _resumeFrameIndex = resumeFrame;
}
// ---------------------------------------------------------------------------

void CStabilizerTool::ButtonCommandHandler(EStabilizerProcessCmd command)
{

   switch (command)
   {
   case STABILIZER_PROC_NOOP:
      // Do nothing, duh
      break;

   case STABILIZER_PROC_TRACK_IF_NEEDED:
      HandleTrackCommand();
      break;

   case STABILIZER_PROC_RETRACK:
      HandleRetrackCommand();
      break;

   case STABILIZER_PROC_SMOOTH:
      HandleSmoothCommand();
      break;

   case STABILIZER_PROC_PREVIEW_1:
      HandleProcessingCommand(TOOL_PROCESSING_CMD_PREVIEW_1);
      break;

   case STABILIZER_PROC_PREVIEW:
      HandleProcessingCommand(TOOL_PROCESSING_CMD_PREVIEW);
      break;

   case STABILIZER_PROC_RENDER_1:
      HandleProcessingCommand(TOOL_PROCESSING_CMD_RENDER_1);
      break;

   case STABILIZER_PROC_RENDER:
      HandleProcessingCommand(TOOL_PROCESSING_CMD_RENDER);
      break;

   case STABILIZER_PROC_PAUSE:
      HandlePauseCommand();
      break;

   case STABILIZER_PROC_CONTINUE:
      // resume frame was set when the pause button was hit
      HandleContinueCommand();
	  break;

   case STABILIZER_PROC_STOP:
	  HandleStopCommand();
      break;

//   case STABILIZER_PROC_KPASS1:
//	  SetControlState(STABILIZER_STATE_NEED_TO_RUN_AUTOTRACK_PASS_1);
//	  break;

   default:
      MTIassert(false);
      break;
   }
}

void CStabilizerTool::HandleTrackCommand()
{
   // This command is only valid when we are IDLE and when we are
   // SMOOTHING, but ONLY if the next state after smoothing is going to
   // be IDLE (i.e. we will NOT stop a render in progress)

	if (GetControlState() == STABILIZER_STATE_IDLE || GetControlState() == STABILIZER_STATE_TRACKING_PAUSED)
	{
		if (isInAutoTrack())
		{
         ResetTracking();
			setNextStateAfterSmoothing(STABILIZER_STATE_IDLE);
			_deferredToolProcessingCommand = TOOL_PROCESSING_CMD_INVALID;
			SetControlState(STABILIZER_STATE_NEED_TO_RUN_AUTOTRACK_PASS_1);
		}
		else if (GetNumberOfTrackingBoxes() > 0)
		{
			setNextStateAfterSmoothing(STABILIZER_STATE_IDLE);
			_deferredToolProcessingCommand = TOOL_PROCESSING_CMD_INVALID;
			SetControlState(STABILIZER_STATE_NEED_TO_RUN_TRACKER);
	   }
   }
   else if (GetControlState() == STABILIZER_STATE_SMOOTHING && getNextStateAfterSmoothing() == STABILIZER_STATE_IDLE)
   {
		setNextStateAfterSmoothing(isInAutoTrack()
											? STABILIZER_STATE_NEED_TO_RUN_AUTOTRACK_PASS_1
											: STABILIZER_STATE_NEED_TO_RUN_TRACKER);
		_deferredToolProcessingCommand = TOOL_PROCESSING_CMD_INVALID;
      StopSmoothing();
   }
}

void CStabilizerTool::HandleRetrackCommand()
{
   HandleTrackCommand();
}

void CStabilizerTool::HandleSmoothCommand()
{
   // This command is only valid when:
   // -  we are IDLE and the track data is valid (i.e. we will NOT
   // automatically re-track)
   // - we are SMOOTHING and the next state after smoothing is going to
   // be IDLE (i.e. we will NOT stop a render in progress)

   if (isAllTrackDataValid() && (GetControlState() == STABILIZER_STATE_IDLE))
   {
   	SetControlState(STABILIZER_STATE_SMOOTHING);
		setNextStateAfterSmoothing(STABILIZER_STATE_IDLE);
      _deferredToolProcessingCommand = TOOL_PROCESSING_CMD_INVALID;
		StartSmoothing();
   }
   else if (GetControlState() == STABILIZER_STATE_SMOOTHING && getNextStateAfterSmoothing() == STABILIZER_STATE_IDLE)
   {
      setNextStateAfterSmoothing(STABILIZER_STATE_NEED_TO_RUN_SMOOTHER);
      _deferredToolProcessingCommand = TOOL_PROCESSING_CMD_INVALID;
      StopSmoothing();
   }
}

void CStabilizerTool::HandleProcessingCommand(EToolProcessingCommand toolProcessingCommand)
{
   // Tool processing is only OK when we are IDLE, TRACKING or SMOOTHING;
   // if not IDLE, then we just make sure that after SMOOTHING is complete
   // we go to the RENDERING state.

   // Processing involves first tracking, then smoothing, THEN the requested
   // processing. If the track data is valid, we skip tracking. If the
   // smoothed data is valid, we'll skip that, too.

   if (GetControlState() == STABILIZER_STATE_RENDERING)
   {
      // ILLEGAL REQUEST - GUI MUST AVOID DOING THIS
      TRACE_0(errout << "ERROR: Stabilize: received render command while already rendering");
      Beep();
      return;
   }

   // If tracking or smoothing is already in progress, we'll
   // just hijack the process

	setNextStateAfterSmoothing(STABILIZER_STATE_NEED_TO_RUN_RENDERER);
	_deferredToolProcessingCommand = toolProcessingCommand;

	if (GetControlState() == STABILIZER_STATE_IDLE)
   {
      // Nothing's going on, we need to kickstart the process
		if (!isAllTrackDataValid())
		{
			// Need to start with tracking
			SetControlState(isInAutoTrack()
									? STABILIZER_STATE_NEED_TO_RUN_AUTOTRACK_PASS_1
									: STABILIZER_STATE_NEED_TO_RUN_TRACKER);
		}
      else if (!IsSmoothedDataValid())
      {
         // Need to start with smoothing
			SetControlState(STABILIZER_STATE_NEED_TO_RUN_SMOOTHER);
      }
      else
      {
			setNextStateAfterSmoothing(STABILIZER_STATE_IDLE);
			SetControlState(STABILIZER_STATE_NEED_TO_RUN_RENDERER);
      }
   }
}

void CStabilizerTool::HandlePauseCommand()
{
	if (GetStupidTrackingHackState() == TRACKING)
	{
		StopTracking(true);
	}
	else if (GetStupidTrackingHackState() == NOT_TRACKING)
	{
		PauseToolProcessing();
	}
}

void CStabilizerTool::HandleContinueCommand()
{
	if (GetStupidTrackingHackState() == TRACKING_PAUSED)
	{
		StartTracking();
	}
	else if (GetStupidTrackingHackState() == NOT_TRACKING)
	{
		ContinueToolProcessing(_resumeFrameIndex);
	}
}

void CStabilizerTool::HandleStopCommand()
{
	if (GetStupidTrackingHackState() != NOT_TRACKING)
   {
		StopTracking(false);
	}
   else
   {
		StopToolProcessing();
   }
}
// ---------------------------------------------------------------------------

EStabilizerControlState CStabilizerTool::GetControlState()
{
	return _stabilizerControlState;
}
// ---------------------------------------------------------------------------

void CStabilizerTool::SetControlState(EStabilizerControlState newControlState)
{
   _stabilizerControlState = newControlState;
	UpdateExecutionButtons(GetToolProcessingControlState(), GetToolProcessingRenderFlag(), GetStupidTrackingHackState());
}
// ---------------------------------------------------------------------------

StupidTrackingHackState CStabilizerTool::GetStupidTrackingHackState()
{
	return (_stabilizerControlState == STABILIZER_STATE_TRACKING)
			 ? TRACKING
			 : (_stabilizerControlState == STABILIZER_STATE_TRACKING_PAUSED)
				? TRACKING_PAUSED
				: NOT_TRACKING;
}
// ---------------------------------------------------------------------------

void CStabilizerTool::SetTrackingPoints(const TrackingBoxSet &TA)
{
   TrackingArray = TA;
   TrkPtsEditor->setTrackingArray(&TrackingArray, true);
   UpdateTrackingPointGUI();
}
// ----------------------------------------------------------------------------

DRECT CStabilizerTool::GetZoomBox(RECT& boundingRect)
{
	const double PI = 3.1415927;

	//////////////////////////////////////////////////////////////////////
	// We want to compute the "Zoom Rectangle" at the start frame. This
	// rectangle only includes the pixels inside the bounding box at the
	// start frame that are visible in all the other tracked frames. The
	// resulting rectangle is then trimmed to make its aspect ratio match
	// the aspect ratio of the bounding box.
	//////////////////////////////////////////////////////////////////////

	DRECT zoomAdjust;
	zoomAdjust.left = 0;
	zoomAdjust.top = 0;
	zoomAdjust.right = 0;
	zoomAdjust.bottom = 0;

	// Initialize to no zooming. Note that bounding box is INCLUSIVE on right
	// and bottom, but we return a rectangle that is EXCLUSIVE.
	DRECT zoomRect;
	zoomRect.left = boundingRect.left;
	zoomRect.top = boundingRect.top;
	zoomRect.right = boundingRect.right + 1;
	zoomRect.bottom = boundingRect.bottom + 1;

	double boundingRectWidth = (double)(boundingRect.right - boundingRect.left + 1);
	double boundingRectHeight = (double)(boundingRect.bottom - boundingRect.top + 1);

	TrackingBoxSet &rawTrackingBoxSet = GetTrackingArrayRef();
	TrackingBoxSet &robustBoxSet = GetRobustArrayRef();
	TrackingBoxSet &smoothedBoxSet = GetSmoothedArrayRef();

	auto robustBox = robustBoxSet.getTrackingBoxByIndex(0);
	auto smoothedBox = smoothedBoxSet.getTrackingBoxByIndex(0);
	auto rotatedBox = smoothedBoxSet.getTrackingBoxByIndex(1);

	TRACE_3(errout << "ROBUST BOX: " << robustBox.toString());
	TRACE_3(errout << "SMOOTH BOX: " << smoothedBox.toString());
	TRACE_3(errout << "ROTATE BOX: " << rotatedBox.toString());

	int startFrame = rawTrackingBoxSet.getInFrame();
	int stopFrame = rawTrackingBoxSet.getOutFrame();

	for (auto frame = startFrame; frame < stopFrame; ++frame)
	{
		MTIassert(robustBox.isValidAtFrame(frame));
		MTIassert(smoothedBox.isValidAtFrame(frame));
		if (!robustBox.isValidAtFrame(frame) || !smoothedBox.isValidAtFrame(frame))
		{
			// Not really sure whether or not this is an error. I think not,
			// because we may have only partially tracked.
			break;
		}

		// Tracking offset from start frame after smoothing.
		double deltaX = smoothedBox[frame].x - robustBox[frame].x;
		double deltaY = smoothedBox[frame].y - robustBox[frame].y;

		double deltaRotXAbs = 0.0;
		double deltaRotYAbs = 0.0;
		if (rotatedBox.isValidAtFrame(frame))
		{
			// Rotation always forces a shrink of the zoom box.
			// Is this right?? QQQ
			double rotFactor = fabs(rotatedBox[frame].y - smoothedBox[frame].y) * PI / 180.0;
			deltaRotXAbs = rotFactor * boundingRectHeight / 2.0;
			deltaRotYAbs = rotFactor * boundingRectWidth / 2.0;
		}

		double leftAdjust   = deltaX + deltaRotXAbs;
		double topAdjust    = deltaY + deltaRotYAbs;
		double rightAdjust  = deltaX - deltaRotXAbs;
		double bottomAdjust = deltaY - deltaRotYAbs;

		TRACE_3(errout << "LEFT:   MAX(" << zoomAdjust.left   << ", " << leftAdjust   << ")");
		TRACE_3(errout << "TOP:    MAX(" << zoomAdjust.top    << ", " << topAdjust    << ")");
		TRACE_3(errout << "RIGHT:  MIN(" << zoomAdjust.right  << ", " << rightAdjust  << ")");
		TRACE_3(errout << "BOTTOM: MIN(" << zoomAdjust.bottom << ", " << bottomAdjust << ")");

		// See if we need to eliminate more start frame pixels.
		zoomAdjust.left   = max<double>(zoomAdjust.left,   leftAdjust);
		zoomAdjust.top    = max<double>(zoomAdjust.top,    topAdjust);
		zoomAdjust.right  = min<double>(zoomAdjust.right,  rightAdjust);
		zoomAdjust.bottom = min<double>(zoomAdjust.bottom, bottomAdjust);
	}

	// Should only be zooming in, never out.
	MTIassert(zoomAdjust.left >= 0);
	MTIassert(zoomAdjust.top >= 0);
	MTIassert(zoomAdjust.right <= 0);
	MTIassert(zoomAdjust.bottom <= 0);

	zoomRect.left += zoomAdjust.left;
	zoomRect.top += zoomAdjust.top;
	// QQQ "+ 1.0"?
	zoomRect.right += zoomAdjust.right + 1.0;
	zoomRect.bottom += zoomAdjust.bottom + 1.0;

	TRACE_3(errout << "ZOOMRECT: " << zoomRect.left << ", " << zoomRect.top << ", " << zoomRect.right << ", " << zoomRect.bottom);

	// Maybe zoom in more in one dimension so aspect ratio matches the bounding box.
	double zoomRectWidth = zoomRect.right - zoomRect.left;
	double zoomRectHeight = zoomRect.bottom - zoomRect.top;
	if (zoomRectWidth / zoomRectHeight > boundingRectWidth / boundingRectHeight)
	{
		// zoom box too wide
		double zoomRectCenterX = (zoomRect.left + zoomRect.right) / 2.0;
		zoomRectWidth = zoomRectHeight * boundingRectWidth / boundingRectHeight;
		zoomRect.left = zoomRectCenterX - zoomRectWidth / 2.0;
		zoomRect.right = zoomRectCenterX + zoomRectWidth / 2.0;

		TRACE_3(errout << "ZR TOO WIDE: " << zoomRect.left << ", " << zoomRect.top << ", " << zoomRect.right << ", " << zoomRect.bottom);
	}
	else
	{
		// zoom box too high
		double zoomRectCenterY = (zoomRect.top + zoomRect.bottom) / 2.0;
		zoomRectHeight = zoomRectWidth * boundingRectHeight / boundingRectWidth;
		zoomRect.top = zoomRectCenterY - zoomRectHeight / 2.0;
		zoomRect.bottom = zoomRectCenterY + zoomRectHeight / 2.0;

		TRACE_3(errout << "ZR TOO HIGH: " << zoomRect.left << ", " << zoomRect.top << ", " << zoomRect.right << ", " << zoomRect.bottom);
	}

	return zoomRect;
}

// -----------------GatherParameters-------------------------------------------

int CStabilizerTool::GatherParameters(CStabilizerToolParameters &toolParams)
{
	toolParams.SetRegionOfInterest(getSystemAPI()->GetMaskRoi());

	GatherGUIParameters(toolParams.stabilizerParameters);
	_stabilizerParameters = toolParams.stabilizerParameters;
	return 0;
}

// ---------------------------------------------------------------------------

bool CStabilizerTool::isFrameBetweenMarks(int iMarkIn, int iMarkOut, int frameToCheckBetweenMarks)
{
	int retVal = true;
	CAutoErrorReporter autoErr("CStabilizerTool::ValidateMarks", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

	// check Marks In/Out
	if (iMarkIn < 0 || iMarkOut < 0)
	{
      autoErr.traceAction = AUTO_ERROR_NO_TRACE;
      autoErr.errorCode = -1;
      autoErr.msg << "Set marks to designate the material to be processed";
      retVal = false;
   }

   if (retVal && (iMarkIn >= iMarkOut))
   {
      autoErr.traceAction = AUTO_ERROR_NO_TRACE;
      autoErr.errorCode = -1;
      autoErr.msg << "Mark IN must be earlier than mark OUT";
      retVal = false;
   }

   // check index of single preview frame
   if (retVal && (frameToCheckBetweenMarks >= 0) && (frameToCheckBetweenMarks < iMarkIn || frameToCheckBetweenMarks >= iMarkOut))
   {
      autoErr.traceAction = AUTO_ERROR_NO_TRACE;
      autoErr.errorCode = -1;
      autoErr.msg << "Frame must be between the marks";
      retVal = false;
   }

   return retVal;
}
// ---------------------------------------------------------------------------

int CStabilizerTool::RunFramePreprocessing()
{
   CAutoErrorReporter autoErr("CStabilizerTool::RunFramePreprocessing", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

	if (isInAutoTrack())
	{
		return RunFrameProcessingAutoTrack(-1);
	}

   if (!AreMarksValid() != 0)
   {
      autoErr.errorCode = -1;
      autoErr.msg << "Marks are invalid!    ";
		SetControlState(STABILIZER_STATE_GOT_AN_ERROR);
      return -1;
   }

   if (!getSystemAPI()->isAClipLoaded())
   {
      autoErr.errorCode = -1;
      autoErr.msg << "No clip is open   ";
		SetControlState(STABILIZER_STATE_GOT_AN_ERROR);
      return -1;
   }

   // Skip tracking phase if tracking data is valid
   if (!isAllTrackDataValid())
   {
		// IMPORTANT LOCATION: must come before call to StartTracking()
		SetControlState(STABILIZER_STATE_TRACKING);
		StartTracking();

      // Smoothing will be kicked off from NotifyTrackingDone(), which is
		// called when, duh, tracking is done!
   }
   else
   {
      // Don't need to run the track tool -- maybe shouldn't have started it!
      StopToolProcessing();
      if (!IsSmoothedDataValid())
      {
         // All tracking data was valid, so do smoothing instead
			SetControlState(STABILIZER_STATE_NEED_TO_RUN_SMOOTHER);
      }
      else
      {
         // Signal that everything is done!
			SetControlState(STABILIZER_STATE_SMOOTHING_COMPLETE);
      }
   }

   return 0;
}
// ---------------------------------------------------------------------------

int CStabilizerTool::RunFrameProcessing(int newResumeFrame)
{
	int retVal;
	CAutoErrorReporter autoErr("CStabilizerTool::RunFrameProcessing", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   if (IsDisabled())
   {
      autoErr.errorCode = -999;
      autoErr.msg << "Stabilizer internal error, tried to run when disabled ";
      return autoErr.errorCode;
   }

   int markIn = getSystemAPI()->getMarkIn();
   int markOut = getSystemAPI()->getMarkOut();
   int checkFrameIndex = (newResumeFrame >= 0) ? newResumeFrame : _processSingleFrameIndex;
   if (!isFrameBetweenMarks(markIn, markOut, checkFrameIndex) != 0)
   {
      // isFrameBetweenMarks sets the error
      autoErr.RepeatTheError();
		SetControlState(STABILIZER_STATE_GOT_AN_ERROR);
      return -1; // QQQ
   }

   if (!getSystemAPI()->isAClipLoaded())
   {
      autoErr.errorCode = -1;
      autoErr.msg << "No clip open";
		SetControlState(STABILIZER_STATE_GOT_AN_ERROR);
      return -1; // QQQ
   }

   // IMPORTANT: Must come before MakeSimpleToolSetup()
	SetControlState(STABILIZER_STATE_RENDERING);

    // Kludge to make my life simple
	_toolProcessorTypeToMake = PROC_TYPE_RENDER_PROCESS;

   GpuAccessor gpuAccessor;
	GatherGUIParameters(_stabilizerParameters);
   if (_isGpuUsed != gpuAccessor.useGpu() || _isDisableSubPixelInterpolation != _stabilizerParameters.DisableSubPixelInterpolation)
   {
      DestroyAllToolSetups(true);
      _isGpuUsed = gpuAccessor.useGpu();
      _isDisableSubPixelInterpolation = _stabilizerParameters.DisableSubPixelInterpolation;
   }

   // Check if we have a stabilization tool setup yet
   if (stabilizerToolSetupHandle < 0)
   {
      // Create the tool setup
      CToolIOConfig ioConfig(1, 1);
      ioConfig.processingThreadCount = SysInfo::AvailableProcessorCount();
      ioConfig.disableGpu = _stabilizerParameters.DisableSubPixelInterpolation;
      stabilizerToolSetupHandle = getSystemAPI()->MakeSimpleToolSetup(STABILIZE_TOOL_NAME, TOOL_SETUP_TYPE_MULTI_FRAME, &ioConfig);
      if (stabilizerToolSetupHandle < 0)
      {
         autoErr.errorCode = -1;
         autoErr.msg << "Stabilizer internal error: CNavSystemInterface::MakeSimpleToolSetup failed";
			SetControlState(STABILIZER_STATE_GOT_AN_ERROR);
         return -1;
      }
   }

   // Set the active tool setup (this is different than the active tool)
   retVal = getSystemAPI()->SetActiveToolSetup(stabilizerToolSetupHandle);
   if (retVal != 0)
   {
      autoErr.errorCode = retVal;
      autoErr.msg << "Stabilizer internal error: SetActiveToolSetup(" << trackingToolSetupHandle << ") failed with return code " << retVal;
		SetControlState(STABILIZER_STATE_GOT_AN_ERROR);
      return retVal;
   }

   if (newResumeFrame < 0)
   {
      // Starting from Stop, so set processing frame range to in and out marks
      CToolFrameRange frameRange(1, 1);
      frameRange.inFrameRange[0].randomAccess = false;

      // Check to see if this is a single frame operation
      if (_processSingleFrameIndex >= 0)
      {
         // ??? frameRange.inFrameRange[0].inFrameIndex = _processSingleFrameIndex - _stabilizerParameters.GoodFramesIn;
         // ??? frameRange.inFrameRange[0].outFrameIndex = markOut - _stabilizerParameters.GoodFramesOut;
         // ??? frameRange.inFrameRange[0].outFrameIndex = markOut;
         frameRange.inFrameRange[0].inFrameIndex = _processSingleFrameIndex;
         frameRange.inFrameRange[0].outFrameIndex = _processSingleFrameIndex + 1;
      }
      else
      {
         frameRange.inFrameRange[0].inFrameIndex = markIn;
         frameRange.inFrameRange[0].outFrameIndex = markOut;
      }
      retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
      if (retVal != 0)
      {
         autoErr.errorCode = retVal;
         autoErr.msg << "Stabilizer internal error: SetToolFrameRange" << " failed with return code " << retVal;
         return retVal;
      }
   }
   else
   {
      // Resuming from Pause state, so set processing frame range
      // from newResumeFrame to out mark
      CToolFrameRange frameRange(1, 1);
      frameRange.inFrameRange[0].randomAccess = false;
      int markOut = getSystemAPI()->getMarkOut();
      frameRange.inFrameRange[0].inFrameIndex = newResumeFrame;
      frameRange.inFrameRange[0].outFrameIndex = markOut;
      retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
      if (retVal != 0)
      {
         autoErr.errorCode = retVal;
         autoErr.msg << "Stabilizer internal error: SetToolFrameRange" << " failed with return code " << retVal;
			SetControlState(STABILIZER_STATE_GOT_AN_ERROR);
         return retVal;
      }
   }

   // Set render/preview flag for tool infrastructure.
   getSystemAPI()->SetProvisionalRender(renderFlag);

   // Set Stabilizer's tool parameters
   CStabilizerToolParameters *toolParams = new CStabilizerToolParameters; // deleted by tool infrastructure
   GatherParameters(*toolParams);
   toolParams->stabilizerToolObject = this;

   retVal = getSystemAPI()->SetToolParameters(toolParams);
   if (retVal != 0)
   {
      autoErr.errorCode = retVal;
      autoErr.msg << "Stabilizer internal error: SetToolParameters" << " failed with return code " << retVal;
		SetControlState(STABILIZER_STATE_GOT_AN_ERROR);
      return retVal;
   }

	// hacks
   SingleFrameFlag = false;

	// Start the image processing a-runnin'
   getSystemAPI()->RunActiveToolSetup();

   return 0;
} // RunFrameProcessing()


// Should all be combined into one
int CStabilizerTool::RunFrameProcessingAutoTrack(int newResumeFrame)
{
   int retVal;
   CAutoErrorReporter autoErr("CStabilizerTool::RunFrameProcessingAutoTrack", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   if (IsDisabled())
   {
      autoErr.errorCode = -999;
      autoErr.msg << "Stabilizer internal error, tried to run when disabled ";
      return autoErr.errorCode;
   }

   int markIn = getSystemAPI()->getMarkIn();
   int markOut = getSystemAPI()->getMarkOut();
   int checkFrameIndex = (newResumeFrame >= 0) ? newResumeFrame : _processSingleFrameIndex;
   if (!isFrameBetweenMarks(markIn, markOut, checkFrameIndex) != 0)
   {
      // isFrameBetweenMarks sets the error
      autoErr.RepeatTheError();
	   SetControlState(STABILIZER_STATE_GOT_AN_ERROR);
      return -1; // QQQ
   }

   if (!getSystemAPI()->isAClipLoaded())
   {
      autoErr.errorCode = -1;
      autoErr.msg << "No clip open";
	   SetControlState(STABILIZER_STATE_GOT_AN_ERROR);
      return -1; // QQQ
   }

   // IMPORTANT: Must come before MakeSimpleToolSetup()
 ///	_toolProcessorTypeToMake = PROC_TYPE_KPASS1;

   // Check if we have a stabilization tool setup yet
   if (_autoTrackPass1ToolSetupHandle < 0)
   {
      // Create the tool setup
      CToolIOConfig ioConfig(1, 1);
      ioConfig.processingThreadCount = SysInfo::AvailableProcessorCount();

	  _autoTrackPass1ToolSetupHandle = getSystemAPI()->MakeSimpleToolSetup(STABILIZE_TOOL_NAME, TOOL_SETUP_TYPE_MULTI_FRAME, &ioConfig);
	  if (_autoTrackPass1ToolSetupHandle < 0)
      {
         autoErr.errorCode = -1;
         autoErr.msg << "Stabilizer internal error: CNavSystemInterface::MakeSimpleToolSetup failed";
			SetControlState(STABILIZER_STATE_GOT_AN_ERROR);
         return -1;
      }
   }

   // Set the active tool setup (this is different than the active tool)
   retVal = getSystemAPI()->SetActiveToolSetup(_autoTrackPass1ToolSetupHandle);
   if (retVal != 0)
   {
      autoErr.errorCode = retVal;
      autoErr.msg << "Stabilizer internal error: SetActiveToolSetup(" << trackingToolSetupHandle << ") failed with return code " << retVal;
		SetControlState(STABILIZER_STATE_GOT_AN_ERROR);
      return retVal;
   }

   if (newResumeFrame < 0)
   {
      // Starting from Stop, so set processing frame range to in and out marks
      CToolFrameRange frameRange(1, 1);
      frameRange.inFrameRange[0].randomAccess = false;

      // Check to see if this is a single frame operation
      if (_processSingleFrameIndex >= 0)
      {
         // ??? frameRange.inFrameRange[0].inFrameIndex = _processSingleFrameIndex - _stabilizerParameters.GoodFramesIn;
         // ??? frameRange.inFrameRange[0].outFrameIndex = markOut - _stabilizerParameters.GoodFramesOut;
         // ??? frameRange.inFrameRange[0].outFrameIndex = markOut;
         frameRange.inFrameRange[0].inFrameIndex = _processSingleFrameIndex;
         frameRange.inFrameRange[0].outFrameIndex = _processSingleFrameIndex + 1;
      }
      else
      {
         frameRange.inFrameRange[0].inFrameIndex = markIn;
         frameRange.inFrameRange[0].outFrameIndex = markOut;
      }
      retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
      if (retVal != 0)
      {
         autoErr.errorCode = retVal;
         autoErr.msg << "Stabilizer internal error: SetToolFrameRange" << " failed with return code " << retVal;
         return retVal;
      }
   }
   else
   {
      // Resuming from Pause state, so set processing frame range
      // from newResumeFrame to out mark
      CToolFrameRange frameRange(1, 1);
      frameRange.inFrameRange[0].randomAccess = false;
      int markOut = getSystemAPI()->getMarkOut();
      frameRange.inFrameRange[0].inFrameIndex = newResumeFrame;
      frameRange.inFrameRange[0].outFrameIndex = markOut;
      retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
      if (retVal != 0)
      {
         autoErr.errorCode = retVal;
         autoErr.msg << "Stabilizer internal error: SetToolFrameRange" << " failed with return code " << retVal;
			SetControlState(STABILIZER_STATE_GOT_AN_ERROR);
         return retVal;
      }
   }

   // Set render/preview flag for tool infrastructure.
   getSystemAPI()->SetProvisionalRender(renderFlag);

   // Set Stabilizer's tool parameters
   CStabilizerToolParameters *toolParams = new CStabilizerToolParameters; // deleted by tool infrastructure
   GatherParameters(*toolParams);
   toolParams->stabilizerToolObject = this;

   retVal = getSystemAPI()->SetToolParameters(toolParams);
   if (retVal != 0)
   {
      autoErr.errorCode = retVal;
      autoErr.msg << "Stabilizer internal error: SetToolParameters" << " failed with return code " << retVal;
		SetControlState(STABILIZER_STATE_GOT_AN_ERROR);
      return retVal;
   }

	// hacks
   SingleFrameFlag = false;

	// Start the image processing a-runnin'
   getSystemAPI()->RunActiveToolSetup();

     return 0;
}

// ===================================================================
//
// Function:    ProcessSingleFrame
//
// Description:
//
// Arguments:   None
//
// Returns:
//
// ===================================================================
int CStabilizerTool::ProcessSingleFrame(EToolSetupType setupType)
{
   // CBusyCursor busyCursor(true);   // Busy cursor until busyCursor goes
   // out-of-scope, e.g., this function returns
   int retVal;
   CAutoErrorReporter autoErr("CStabilizerTool::ProcessSingleFrame", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
   int &toolSetupHandle = (setupType == TOOL_SETUP_TYPE_SINGLE_FRAME) ? singleFrameToolSetupHandle : trainingToolSetupHandle;

   if (IsDisabled())
   {
		autoErr.errorCode = -999;
		autoErr.msg << "Stabilizer internal error, tried to run when disabled ";
		return autoErr.errorCode;
   }

   int markInFrameIndex = getSystemAPI()->getMarkIn();
   int markOutFrameIndex = getSystemAPI()->getMarkOut();
   int currentFrameIndex = getSystemAPI()->getLastFrameIndex();

   if (currentFrameIndex < markInFrameIndex || currentFrameIndex >= markOutFrameIndex)
   {
      // Out of range
		autoErr.errorCode = -998;
		autoErr.msg << "Stabilizer internal error, frame index out of range ";
		SetControlState(STABILIZER_STATE_GOT_AN_ERROR);
      return -1;
   }

   if (getSystemAPI()->GetActiveToolSetupStatus() == AT_STATUS_RUNNING)
   {
      // Something is running, so can't do single frame processing right now
		autoErr.errorCode = -997;
		autoErr.msg << "Stabilizer internal error, tool is already running ";
		SetControlState(STABILIZER_STATE_GOT_AN_ERROR);
      return -1;
   }

   // Check if we have a tool setup yet
   if (toolSetupHandle < 0)
   {
      // Create the tool setup
      CToolIOConfig ioConfig(1, 1);
      toolSetupHandle = getSystemAPI()->MakeSimpleToolSetup(STABILIZE_TOOL_NAME, setupType, &ioConfig);
      if (toolSetupHandle < 0)
      {
         autoErr.errorCode = -1;
         autoErr.msg << "Stabilize internal error: MakeSimpleToolSetup failed";
			SetControlState(STABILIZER_STATE_GOT_AN_ERROR);
         return -1;
      }
   }

   // Set the active tool setup (this is different than the active tool)
   retVal = getSystemAPI()->SetActiveToolSetup(toolSetupHandle);
   if (retVal != 0)
   {
      autoErr.errorCode = retVal;
      autoErr.msg << "Scratch internal error: SetActiveToolSetup(" << toolSetupHandle << ") failed with return code " << retVal;
		SetControlState(STABILIZER_STATE_GOT_AN_ERROR);
      return retVal;
   }

   // Set processing frame range to the current frame index
   CToolFrameRange frameRange(1, 1);
   frameRange.inFrameRange[0].randomAccess = true;

   // TTT
   int lastFrameIndex = getSystemAPI()->getLastFrameIndex();
   // Is it too much bother to put in a comment here explaining this shit??
   // Like, why isn't in=lastFrameIndex and out=lastFrameIndex+1 ?????????????
   // frameRange.inFrameRange[0].inFrameIndex  = lastFrameIndex - 2;
   // frameRange.inFrameRange[0].outFrameIndex = lastFrameIndex + 3; // 3 or 2?
   frameRange.inFrameRange[0].inFrameIndex = lastFrameIndex - 1;
   frameRange.inFrameRange[0].outFrameIndex = lastFrameIndex + 2;
   // frameRange.inFrameRange[0].inFrameIndex  = lastFrameIndex;   // OK Let's
   // frameRange.inFrameRange[0].outFrameIndex = lastFrameIndex + 1;  // try it!!

   retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
   if (retVal != 0)
   {
      autoErr.errorCode = retVal;
      autoErr.msg << "Stabilization internal error: SetToolFrameRange" << " failed with return code " << retVal;
		SetControlState(STABILIZER_STATE_GOT_AN_ERROR);
      return retVal;
   }

   // Set Scratch tool's parameters
   CStabilizerToolParameters *toolParams = new CStabilizerToolParameters; // deleted by tool infrastructure
   GatherParameters(*toolParams);
   retVal = getSystemAPI()->SetToolParameters(toolParams);
   if (retVal != 0)
   {
      autoErr.errorCode = retVal;
      autoErr.msg << "Stabilization internal error: SetToolParameters" << " failed with return code " << retVal;
		SetControlState(STABILIZER_STATE_GOT_AN_ERROR);
      return retVal;
   }

   // hack
   SingleFrameFlag = true;

   // Start the image processing a-runnin'
   getSystemAPI()->RunActiveToolSetup();

   return 0;
}

// ===================================================================
//
// Function:    MonitorFrameProcessing
//
// Description: Overrides MonitorFrameProcessing in CToolObject so that
// the different passes of stabilizer can be monitored by the
// PDL rendering.  This function is very similar to the
// base version but has some key differences.
//
// Arguments:
//
// Returns:
//
// ===================================================================

int CStabilizerTool::MonitorFrameProcessing(EToolProcessingCommand newToolCommand, const CAutotoolStatus& newToolStatus, int newResumeFrame)
{
   int retVal = 0;
   EAutotoolStatus newToolProcessingStatus = newToolStatus.status;
   /* */
   bool updateGUI = false;
   /* */

   EToolControlState nextToolControlState;

   // Tool Processing Control State Machine
	nextToolControlState = toolProcessingControlState;
	switch (toolProcessingControlState)
   {
   case TOOL_CONTROL_STATE_WAITING_TO_RUN:
      if (newToolCommand == TOOL_PROCESSING_CMD_STOP)
      {
         nextToolControlState = TOOL_CONTROL_STATE_STOPPED;
      }
	/* FALL THROUGH */
    case TOOL_CONTROL_STATE_STOPPED:
      if (newToolCommand == TOOL_PROCESSING_CMD_PREVIEW_1)
      {
         toolProcessingErrorCode = 0; // clear any old errors
         retVal = ProcessTrainingFrame();
         if (retVal == 0)
         {
            nextToolControlState = TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_STOPPED;
            // was nextToolControlState = TOOL_CONTROL_STATE_RUNNING;
         }
         else
         {
            // TBD: Error Handling
         }
      }
      else if (newToolCommand == TOOL_PROCESSING_CMD_PREVIEW)
      {
         toolProcessingErrorCode = 0; // clear any old errors
         renderFlag = false;
         /* */
         _processSingleFrameIndex = -1; // Not previewing a single frame
         /* */
         retVal = RunFramePreviewing(newResumeFrame);
         if (retVal == 0)
         {
            nextToolControlState = TOOL_CONTROL_STATE_RUNNING;
         }
         else
         {
            // TBD: Error Handling
         }
      }
      else if (newToolCommand == TOOL_PROCESSING_CMD_RENDER_1)
      {
         toolProcessingErrorCode = 0; // clear any old errors
         renderFlag = true;
         retVal = RenderSingleFrame();
         if (retVal == 0)
         {
            nextToolControlState = TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_STOPPED;
         }
         else
         {
            // TBD: Error Handling
         }
      }
      else if (newToolCommand == TOOL_PROCESSING_CMD_RENDER)
      {
         toolProcessingErrorCode = 0; // clear any old errors
         renderFlag = true;
         /* */
         _processSingleFrameIndex = -1; // Not previewing a single frame
         /* */
         retVal = RunFrameProcessing(newResumeFrame);
         if (retVal == 0)
         {
            nextToolControlState = TOOL_CONTROL_STATE_RUNNING;
         }
         else
         {
            // TBD: Error Handling
         }
      }
		else if (newToolCommand == TOOL_PROCESSING_CMD_PREPROCESS)
      {
         toolProcessingErrorCode = 0; // clear any old errors
         renderFlag = false;
         /* */
         _processSingleFrameIndex = -1; // Not preprocessing a single frame
         /* */
			retVal = RunFramePreprocessing();
         if (retVal == 0)
         {
            nextToolControlState = TOOL_CONTROL_STATE_RUNNING;
         }
         else
         {
            // TBD: Error Handling
         }
      }
      break;
   case TOOL_CONTROL_STATE_WAITING_TO_STOP:
      if (newToolProcessingStatus == AT_STATUS_STOPPED)
      {
			/* */
			if (GetStupidTrackingHackState() != NOT_TRACKING)
         {
				SetControlState(STABILIZER_STATE_TRACKING_COMPLETE);
         }
         else if (GetControlState() == STABILIZER_STATE_RENDERING)
         {
            SetControlState(STABILIZER_STATE_RENDERING_COMPLETE);
         }
         /* */
         nextToolControlState = TOOL_CONTROL_STATE_STOPPED;
      }
	  break;

   case TOOL_CONTROL_STATE_RUNNING:
      if (newToolProcessingStatus == AT_STATUS_PROCESS_ERROR)
      {
         toolProcessingErrorCode = newToolStatus.errorCode;

         nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_STOP;
      }
      if (newToolProcessingStatus == AT_STATUS_STOPPED)
      {
         /* */
			if (GetStupidTrackingHackState() != NOT_TRACKING)
         {
				SetControlState(STABILIZER_STATE_TRACKING_COMPLETE);

            // ahahaha this is HIDEOUS! But don't want to change to STOPPED
            // else the PDL monitor gets very confused!
            nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_RUN;

            // Even though the tool control state has not changed,
            // we still want to update the GUI to reflect the new
            // processing pass
            updateGUI = true;
         }
         else
         {
            if (GetControlState() == STABILIZER_STATE_RENDERING)
            {
               SetControlState(STABILIZER_STATE_RENDERING_COMPLETE);
            }
            nextToolControlState = TOOL_CONTROL_STATE_STOPPED;
         }
         /* */
      }
      else if (newToolCommand == TOOL_PROCESSING_CMD_STOP)
      {
         /* */
			if (GetStupidTrackingHackState() != NOT_TRACKING)
			{
				SetControlState(STABILIZER_STATE_TRACKING_COMPLETE);
         }
         else if (GetControlState() == STABILIZER_STATE_RENDERING)
         {
            SetControlState(STABILIZER_STATE_RENDERING_COMPLETE);
         }
         /* */
         retVal = StopFrameProcessing();
         if (retVal == 0)
         {
            nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_STOP;
         }
         else
         {
            // TBD: Error Handling
         }
      }
      else if (newToolCommand == TOOL_PROCESSING_CMD_PAUSE)
      {
         retVal = PauseFrameProcessing();
         if (retVal == 0)
         {
            nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_PAUSE;
         }
         else
         {
            // TBD: Error Handling
         }
      }
      break;
   case TOOL_CONTROL_STATE_WAITING_TO_PAUSE:
      if (newToolProcessingStatus == AT_STATUS_STOPPED)
		{
         /* */
			if (GetStupidTrackingHackState() != NOT_TRACKING)
         {
            SetControlState(STABILIZER_STATE_TRACKING_COMPLETE);
         }
         else if (GetControlState() == STABILIZER_STATE_RENDERING)
         {
            SetControlState(STABILIZER_STATE_RENDERING_COMPLETE);
         }
         /* */
         nextToolControlState = TOOL_CONTROL_STATE_STOPPED;
      }
      else if (newToolProcessingStatus == AT_STATUS_PAUSED)
      {
         nextToolControlState = TOOL_CONTROL_STATE_PAUSED;
      }
      else if (newToolCommand == TOOL_PROCESSING_CMD_STOP)
      {
         retVal = StopFrameProcessing();
         if (retVal == 0)
         {
            nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_STOP;
         }
         else
         {
            // TBD: Error Handling
         }
      }
      break;
   case TOOL_CONTROL_STATE_PAUSED:
      if (newToolCommand == TOOL_PROCESSING_CMD_STOP)
      {
         retVal = StopFrameProcessing();
         if (retVal == 0)
         {
            nextToolControlState = TOOL_CONTROL_STATE_WAITING_TO_STOP;
         }
         else
         {
            // TBD: Error Handling
         }
      }
      else if (newToolCommand == TOOL_PROCESSING_CMD_CONTINUE)
      {
         retVal = RunFrameProcessing(newResumeFrame);
         if (retVal == 0)
         {
            nextToolControlState = TOOL_CONTROL_STATE_RUNNING;
         }
         else
         {
            // TBD: Error Handling
         }
      }
      else if (newToolCommand == TOOL_PROCESSING_CMD_PREVIEW_1)
      {
         retVal = ProcessTrainingFrame();
         if (retVal == 0)
         {
            nextToolControlState = TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_PAUSED;
         }
         else
         {
            // TBD: Error Handling
         }
      }
      else if (newToolCommand == TOOL_PROCESSING_CMD_RENDER_1)
      {
         retVal = RenderSingleFrame();
         if (retVal == 0)
         {
            nextToolControlState = TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_PAUSED;
         }
         else
         {
            // TBD: Error Handling
         }
      }
      break;
   case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_STOPPED:
   case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_STOPPED:
      if (newToolProcessingStatus == AT_STATUS_STOPPED)
      {
         /* */
			if (GetStupidTrackingHackState() != NOT_TRACKING)
			{
				SetControlState(STABILIZER_STATE_TRACKING_COMPLETE);
         }
         else if (GetControlState() == STABILIZER_STATE_RENDERING)
         {
            SetControlState(STABILIZER_STATE_RENDERING_COMPLETE);
         }
         /* */
         nextToolControlState = TOOL_CONTROL_STATE_STOPPED;
      }
      break;
   case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_PAUSED:
   case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_PAUSED:
      if (newToolProcessingStatus == AT_STATUS_STOPPED)
      {
         nextToolControlState = TOOL_CONTROL_STATE_PAUSED;
      }
      break;
   default:
      MTIassert(false);
      break;
   }

   if (toolProcessingControlState != nextToolControlState)
   {
      // Control State has changed
      toolProcessingControlState = nextToolControlState;
      /* */
      updateGUI = true;
      /* */
      /* UpdateExecutionGUI(toolProcessingControlState, renderFlag); */
   }

   /* */
   if (updateGUI)
   {
	  UpdateExecutionGUI(toolProcessingControlState, renderFlag);
   }
   /* */

   return retVal;
}
// ---------------------------------------------------------------------------

int CStabilizerTool::ProcessTrainingFrame()
{
   int retVal;

   _processSingleFrameIndex = getSystemAPI()->getLastFrameIndex();

	retVal = RunFrameProcessing(-1);
   if (retVal != 0)
   {
      return retVal;
   }

   return 0;
}
// ---------------------------------------------------------------------------

void CStabilizerTool::DestroyAllToolSetups(bool notIfPaused)
{
   // DRS has three tool setups: DRS, Review Tool and Move Tool.
   // This function should not be called unless all of the tool setups
   // are stopped and there is no provisional pending.

   int activeToolSetup = getSystemAPI()->GetActiveToolSetupHandle();

   if (stabilizerToolSetupHandle >= 0)
   {
      bool isPaused = (getSystemAPI()->GetToolSetupStatus(stabilizerToolSetupHandle) == AT_STATUS_PAUSED);
      if (!(notIfPaused && isPaused))
      {
         if (stabilizerToolSetupHandle == activeToolSetup)
         {
            getSystemAPI()->SetActiveToolSetup(-1);
         }

         getSystemAPI()->DestroyToolSetup(stabilizerToolSetupHandle);
         stabilizerToolSetupHandle = -1;
      }
   }

   if (_autoTrackPass1ToolSetupHandle >= 0)
   {
	  bool isPaused = (getSystemAPI()->GetToolSetupStatus(_autoTrackPass1ToolSetupHandle) == AT_STATUS_PAUSED);
	  if (!(notIfPaused && isPaused))
	  {
		 if (_autoTrackPass1ToolSetupHandle == activeToolSetup)
		 {
			getSystemAPI()->SetActiveToolSetup(-1);
		 }

		 getSystemAPI()->DestroyToolSetup(_autoTrackPass1ToolSetupHandle);
		 _autoTrackPass1ToolSetupHandle = -1;
	  }
   }
}
// ---------------------------------------------------------------------------

void CStabilizerTool::NotifyFrameRendered(int frame)
{
	if (renderFlag)
	{
		TrkPtsEditor->notifyFrameRendered(frame);
	}
}

void CStabilizerTool::NotifyRenderedCompleteRange()
{
	if (renderFlag)
	{
		TrkPtsEditor->notifyRenderComplete();
	}
}

// ===================================================================
// Function:    UpdateExecutionGUI
// Description: Update the GUI according to the process control state from the
// tool infrastructure (overrides CToolObject version). Also use
// it to initiate another pass through the frames since Stabilizer
// processing requires an an acquire pass followed by a render pass.
// Arguments: TI control state + render/preview flag.
// Returns:
// ===================================================================

void CStabilizerTool::UpdateExecutionGUI(EToolControlState toolProcessingControlState, bool processingRenderFlag)
{
	UpdateExecutionButtons(toolProcessingControlState, processingRenderFlag, GetStupidTrackingHackState());

} // UpdateExecutionGUI()
// ---------------------------------------------------------------------------

void CStabilizerTool::UpdateTrackingPointGUI(void)
{
   UpdateTrackingPointButtons();
	UpdateTrackingCharts();
	UpdateExecutionButtons(GetToolProcessingControlState(), GetToolProcessingRenderFlag(), GetStupidTrackingHackState());

} // UpdateTrackingPointGUI()
// ---------------------------------------------------------------------------

void CStabilizerTool::setBoundingBox(const RECT &newBox)
{
   if (IsInColorPickMode())
   {
      ToggleColorPicker();
   }
   setBoundingBoxInternal(newBox);
}
// ---------------------------------------------------------------------------

void CStabilizerTool::setBoundingBoxInternal(const RECT &newBox)
{
   // If ANY of the sides are outside the frame bounds, set this to the full frame.
   const CImageFormat* imgFmt = GStabilizerTool->getSystemAPI()->getVideoClipImageFormat();
   if (newBox.left < 0 || newBox.right >= imgFmt->getPixelsPerLine() || newBox.right < newBox.left
   || newBox.top < 0 || newBox.bottom >= imgFmt->getLinesPerFrame() || newBox.bottom < newBox.top)
   {
      _BoundingBox.left = 0;
      _BoundingBox.top = 0;
      _BoundingBox.right = int(imgFmt->getPixelsPerLine() - 1);
      _BoundingBox.bottom = int(imgFmt->getLinesPerFrame() - 1);
   }
   else
   {
      _BoundingBox = newBox;
   }

   _BoundingBoxDefined = true;
   TrackingArray.setBoundingBox(newBox);
   getSystemAPI()->refreshFrameCached();
}
// ---------------------------------------------------------------------------

void CStabilizerTool::readBoundingBoxFromDesktopIni(ClipSharedPtr &clip)
{
   CBinManager binManager;
   _BoundingBoxDefined = false; // yuck QQQ
   CIniFile *desktopIni = binManager.openClipDesktop(clip);
   const string iniSection = "BlueBoundingBox";
   if (desktopIni != nullptr)
   {
      RECT bbox;
      bbox.left = desktopIni->ReadInteger(iniSection, "Left", -1);
      bbox.top = desktopIni->ReadInteger(iniSection, "Top", -1);
      bbox.right = desktopIni->ReadInteger(iniSection, "Right", -1);
      bbox.bottom = desktopIni->ReadInteger(iniSection, "Bottom", -1);

      setBoundingBox(bbox);

      DeleteIniFileDiscardChanges(desktopIni);
   }
}
// ---------------------------------------------------------------------------

void CStabilizerTool::writeBoundingBoxToDesktopIni(ClipSharedPtr &clip)
{
   CBinManager binManager;
   CIniFile *desktopIni = binManager.openClipDesktop(clip);
   const string iniSection = "BlueBoundingBox";

   if (desktopIni != nullptr)
   {
      if (_BoundingBoxDefined)
      {
         RECT bbox = getBoundingBox();
         desktopIni->WriteInteger(iniSection, "Left", bbox.left);
         desktopIni->WriteInteger(iniSection, "Top", bbox.top);
         desktopIni->WriteInteger(iniSection, "Right", bbox.right);
         desktopIni->WriteInteger(iniSection, "Bottom", bbox.bottom);
      }
      else
      {
         desktopIni->EraseSection(iniSection);
      }

      DeleteIniFile(desktopIni);
   }
}
// ---------------------------------------------------------------------------

void CStabilizerTool::setDisplayRect(const bool newDisp)
{
   if (_displayRect == newDisp)
   {
      return;
   }

   _displayRect = newDisp;

   // For Larry, try extra hard to shoVw the blue rectangle if the check
   // boxes indicate that it should be shown
   if (_displayRect)
   {
      const CImageFormat* imgFmt = GStabilizerTool->getSystemAPI()->getVideoClipImageFormat();

      if ((!_BoundingBoxDefined) || (_BoundingBox.bottom <= _BoundingBox.top) || (_BoundingBox.right <= _BoundingBox.left))
      {
         // Reset bounding bos to "All" if we asked for it to be displayed,
         // and it either wasn't defined or it was bogus
         RECT bb;
         bb.left = 0;
         bb.top = 0;
         bb.right = imgFmt->getPixelsPerLine() - 1;
         bb.bottom = imgFmt->getLinesPerFrame() - 1;
         setBoundingBoxInternal(bb);
      }
      else
      {
         // If it's already defined, enforce bounding box legal values
         // so it doesn't disappear.
         if (_BoundingBox.top < 0)
         {
            _BoundingBox.top = 0;
         }
         if (_BoundingBox.left < 0)
         {
            _BoundingBox.left = 0;
         }
         if (_BoundingBox.right >= int(imgFmt->getPixelsPerLine()))
         {
            _BoundingBox.right = int(imgFmt->getPixelsPerLine() - 1);
         }
         if (_BoundingBox.bottom >= int(imgFmt->getLinesPerFrame()))
         {
            _BoundingBox.bottom = int(imgFmt->getLinesPerFrame() - 1);
         }
      }
   }

   getSystemAPI()->refreshFrameCached();
}
// ---------------------------------------------------------------------------

bool CStabilizerTool::getDisplayTrackingPoints(void)
{
   return TrkPtsEditor->displayTrackingPoints;
}
// ---------------------------------------------------------------------------

void CStabilizerTool::setDisplayTrackingPoints(const bool newDisp)
{
   if (newDisp != TrkPtsEditor->displayTrackingPoints)
   {
      TrkPtsEditor->displayTrackingPoints = newDisp;
   }
}

// ------------------------BoundingBoxDefined-------------------------Oct 2006---

bool CStabilizerTool::BoundingBoxDefined(void)
{
   return _BoundingBoxDefined;
}

// ------------------------ResetTracking------------------------------Oct 2007---

void CStabilizerTool::ResetTracking(void)
{
	ClearTrackingPoints();
	TrkPtsEditor->setRangeFromMarks();

   // Side effects - do some GUI tasks
   GetToolProgressMonitor()->SetIdle();
   UpdateExecutionGUI(TOOL_CONTROL_STATE_STOPPED, false);
   StabilizerForm->UpdateStatusBarInfo();
}

// -----------------GetNumberOfTrackingBoxes---------------------Oct 2007---

int CStabilizerTool::GetNumberOfTrackingBoxes() const
{
   return TrackingArray.getNumberOfTrackingBoxes();
}

// ---------------------------canMarksBeMoved-------------------------Oct 2006---

bool CStabilizerTool::canMarksBeMoved()
{
   // Although return canInMarkBeMoved() & canOutMakeBeMoved(); should work
   // we want to make sure that some optimizer doesn't remove the second call
   // if the first one fails.

   if (!canInMarkBeMoved())
   {
      return false;
   }
   return canOutMarkBeMoved();
}

// ---------------------------canInMarkBeMoved------------------------Oct 2006---

bool CStabilizerTool::canInMarkBeMoved()
{

   // MAJOR LEAGUE KLUDGE  TTT
   // Because of the way the notifications are process, a mark change is done while
   // the clip is being loaded and before the ClipHasChange is called.  Thus the
   // stabilizer thinks the marks come from the old clip, not the new one.
   // To fix this, one would need an AboutToChangeClip Notification, but for now
   // attach a name to a set of tracking points, if the names don't match assume
   // we have a clip change
   //
   if (getSystemAPI()->getClipFilename() != _CurrentClipName)
   {
      return true;
   }
   //
   // END MAJOR LEAGUE KLUDGE TTT

   // ONE GOOD HACK DESERVES ANOTHER
   // Because Stabilizer is sensitive to the order setting marks and tracking
   // points, it gets confused when loading a PDL entry.
   // To avoid this problem, Stabilizer needs to know when a PDL Entry
   // is being loaded (as opposed to being setup by a human).
   if (getSystemAPI()->IsPDLEntryLoading())
   {
      return true;
   }
   //
   // END OF ANOTHER HACK

   switch (TrkPtsEditor->canInMarkChange())
   {
      // We do nothing if marks can change
   case TRK_IN_MARK_CAN_CHANGE:
      return true;

   case TRK_IN_MARK_IS_DELETED:
      if ((isAllTrackDataValid() && (HowLongItTookToTrackInSecs > ANNOYING_DIALOG_TRACK_TIME_THRESHOLD) &&
            (!TrackingDataHasBeenUsedToRenderAll)) || (GetNumberOfTrackingBoxes() > 1))
      {
			if (MTI_DLG_CANCEL == _MTIConfirmationDialog(Handle,
					"All tracking points will be deleted " "if you delete your IN mark.    \nContinue anyway?"))
			{
				// Undo mark in move
				getSystemAPI()->setMarkIn(TrkPtsEditor->getRangeBegin());
				return false;
			}
		}
      return true;

   case TRK_IN_MARK_IS_MOVED_OUTSIDE_INTERVAL:

      ///////////////////////////////////////////////////////////////////
      // Hack added per Larry 2010-06
      // If we think the user is trying to go to a new shot, clear all the
      // tracking stuff if the "Clear on new shot" checkbox is set;
      // "new shot" is hereby defined as moving the IN mark to someplace
      // OUTSIDE of the previous IN-OUT range!
      //
		if (_clearTrackingStuffOnNewShot)
		{
			ClearTrackingPoints();
			// Added benefit of this mode is to not annoy you with a dialog!
         return true;
      }
		else if (isAllTrackDataValid() && (HowLongItTookToTrackInSecs > ANNOYING_DIALOG_TRACK_TIME_THRESHOLD))
      {
         if (!ShowedInMarkMoveMessage)
         {
            if (MTI_DLG_CANCEL == _MTIConfirmationDialog(Handle,
                  "All tracking data will be lost " "if you move your IN mark.    \nContinue anyway?"))
            {
               // Undo mark in move
               getSystemAPI()->setMarkIn(TrkPtsEditor->getRangeBegin());
               return false;
            }
            else
            {
               ShowedInMarkMoveMessage = true;
            }
         }
      }

      // TrackingEditor will move the points to the new mark
      return true;

   case TRK_IN_MARK_IS_MOVED_INSIDE_INTERVAL:

#if !DECOUPLE_MARKS_FROM_TRACKED_RANGE

		if (isAllTrackDataValid() && (HowLongItTookToTrackInSecs > ANNOYING_DIALOG_TRACK_TIME_THRESHOLD))
      {
         if (!ShowedInMarkMoveMessage)
         {
            if (MTI_DLG_CANCEL == _MTIConfirmationDialog(Handle,
                  "Some tracking data will be lost " "if you move your IN mark.    \nContinue anyway?"))
            {
               // Undo mark in move
               getSystemAPI()->setMarkIn(TrkPtsEditor->getRangeBegin());
               return false;
            }
            else
            {
               ShowedInMarkMoveMessage = true;
            }
         }
      }

      // TrackingEditor will erase tracking data before the new
      // mark if tracking data is valid, else to move the points
      // to the new mark

#endif // !DECOUPLE_MARKS_FROM_TRACKED_RANGE

      return true;
   }

   return true;
}

// --------------------------canOutMarkBeMoved------------------------Oct 2006---

bool CStabilizerTool::canOutMarkBeMoved()
{

   // MAJOR LEAGUE KLUDGE  TTT
   // Because of the way the notifications are process, a mark change is done while
   // the clip is being loaded and before the ClipHasChange is called.  Thus the
   // stabilizer thinks the marks come from the old clip, not the new one.
   // To fix this, one would need an AboutToChangeClip Notification, but for now
   // attach a name to a set of tracking points, if the names don't match assume
   // we have a clip change
   //
   if (getSystemAPI()->getClipFilename() != _CurrentClipName)
   {
      return true;
   }
   //
   // END MAJOR LEAGUE KLUDGE TTT

   switch (TrkPtsEditor->canOutMarkChange())
   {
      // We do nothing if marks can change
   case TRK_OUT_MARK_CAN_CHANGE:
      return true;

   case TRK_OUT_MARK_IS_DELETED:
      if (!_WarningDialogs)
      {
         return true;
      }

		if (isAllTrackDataValid() && (HowLongItTookToTrackInSecs > ANNOYING_DIALOG_TRACK_TIME_THRESHOLD))
      {
         switch (_MTIConfirmationDialog(Handle, "Tracking information will be lost if you delete your OUT mark.\nContinue anyway?"))
         {
         case MTI_DLG_OK:
            ShowedOutMarkMoveMessage = true;
            return true;

         case MTI_DLG_CANCEL:
				getSystemAPI()->setMarkOut(TrkPtsEditor->getRangeEnd());
            return false;
         }
      }
      return true;

	case TRK_OUT_MARK_IS_MOVED_OUTSIDE_INTERVAL:
	{
      if (!_WarningDialogs)
      {
         return true;
		}

		int startFrame;
		int oldStopFrame;
		int newStopFrame = getSystemAPI()->getMarkOut();
		TrkPtsEditor->getRange(startFrame, oldStopFrame);
		bool isNewShot = newStopFrame <= startFrame;
		if (!isNewShot)
		{
			vector<int> cutList;
			getSystemAPI()->getCutList(cutList);
			for (int i = 0; i < cutList.size(); ++i)
			{
				if (oldStopFrame <= cutList[i] && newStopFrame > cutList[i])
				{
					isNewShot = true;
					break;
				}
			}
		}

		if (_clearTrackingStuffOnNewShot && isNewShot)
		{
			ClearTrackingPoints();
			// Added benefit of this mode is to not annoy you with a dialog!
         return true;
      }
		else if (isAllTrackDataValid() && (HowLongItTookToTrackInSecs > ANNOYING_DIALOG_TRACK_TIME_THRESHOLD))
      {
			// Ask the user
			switch (_MTIConfirmationDialog(Handle, "All points will need to be re-tracked.    \nContinue anyway?"))
         {
         case MTI_DLG_OK:
            return true;

         case MTI_DLG_CANCEL:
            getSystemAPI()->setMarkOut(TrkPtsEditor->getRangeEnd());
            return false;
         }
      }
      return true;
	}

   case TRK_OUT_MARK_IS_MOVED_INSIDE_INTERVAL:

#if !DECOUPLE_MARKS_FROM_TRACKED_RANGE

      // Handle without asking
      if (!_WarningDialogs)
      {
         return true;
      }

      // Ask the user

		if (isAllTrackDataValid() && (HowLongItTookToTrackInSecs > ANNOYING_DIALOG_TRACK_TIME_THRESHOLD))
      {
         switch (_MTIConfirmationDialog(Handle, "Tracking data will be truncated to new OUT mark.\nContinue anyway?"))
         {
         case MTI_DLG_OK:
            ShowedOutMarkMoveMessage = true;
            return true;

         case MTI_DLG_CANCEL:
            getSystemAPI()->setMarkOut(TrkPtsEditor->getRangeEnd());
            return false;
         }
      }

#endif // !DECOUPLE_MARKS_FROM_TRACKED_RANGE

      return true;
   }

   return true;
}

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
// -------------------- MOVED HERE FROM GUI ----------------------------------
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
//
// SaveTrackingPoints
// Called by GUI in response to the tracking point Save button
//
bool CStabilizerTool::SaveTrackingPoints(const string &sName)
{
   bool retVal = true;

   ofstream sFile(sName.c_str());
   if (!sFile.good())
   {
      TRACE_0(errout << "ERROR: Stabilizer: Can't create tracking point file " << sName);
      retVal = false;
   }
   else
   {
      // Set the bounding box
      TrackingArray.setBoundingBox(GStabilizerTool->getBoundingBox());
      TrackingArray.setUseBoundingBox(IsProcessingRegionGUIEnabled());

      if (!(sFile << TrackingArray))
      {
         TRACE_0(errout << "ERROR: Stabilizer: Can't save tracking points to file " << sName);
         retVal = false;
      }
      sFile.close();
   }

   return retVal; ;
}

// ----------------------------------------------------------------------------
//
// LoadTrackingPoints
// Called by GUI in response to the tracking point Load button
//
bool CStabilizerTool::LoadTrackingPoints(const string &sName, bool restoreMarks, bool restoreProcessingRegion)
{
   ifstream sFile(sName.c_str());

   if (!sFile.good())
   {
      TRACE_0(errout << "ERROR: Stabilizer: Can't open tracking point file " << sName);
      return false;
   }

   auto result = LoadTrackingPoints(sFile, restoreMarks, restoreProcessingRegion);
   // Done with file I/O
   sFile.close();

   return result;
}

bool CStabilizerTool::LoadTrackingPoints(istream &sFile, bool restoreMarks, bool restoreProcessingRegion)
{
   // Clear out any garbage
   TrackingBoxSet TA;
   if (!(sFile >> TA))
   {
	  TRACE_0(errout << "ERROR: Stabilizer: Can't load tracking points from string ");
	  return false;
   }

	// Auto-save the current tracking data, then clear them.
	AutoSaveTrackingPoints();
   ResetTracking();

	// Change tracking point region
	if (restoreProcessingRegion)
	{
		EnableProcessingRegionGUI(TA.getUseBoundingBox());
		setBoundingBox(TA.getBoundingBox());
	}

   if (TA.getNumberOfTrackingBoxes() > 0)
   {
      if (restoreMarks)
      {
         int oldMarkIn = getSystemAPI()->getMarkIn();
			int oldMarkOut = getSystemAPI()->getMarkOut();

         getSystemAPI()->setMarkIn(TA.getInFrame());
         getSystemAPI()->setMarkOut(TA.getOutFrame());

         // these will be different if the in saved indexes are out of range
         int newMarkIn = getSystemAPI()->getMarkIn();
         int newMarkOut = getSystemAPI()->getMarkOut();

         // If either of the saved marks was out of range, we put back
         // the old marks, then we will move the saved track points to the
         // current mark IN frame, or the current frame if there is no IN

         if (newMarkIn != TA.getInFrame() || newMarkOut != TA.getOutFrame())
         {
            // Bad marks range for this clip!

            restoreMarks = false;
            getSystemAPI()->setMarkIn(oldMarkIn);
            getSystemAPI()->setMarkOut(oldMarkOut);
         }
         else
         {
            // Saved marks were successfully restored;
            // go to the new mark IN frame
            getSystemAPI()->goToFrameSynchronous(newMarkIn);

#if RESTORE_TRACK_DATA_ON_LOAD

            // Preserve complete tracking data if it's present
            if (TA.getColor() == TrackingBoxSet::ETPBlue || TA.getColor() == TrackingBoxSet::ETPGreen)
            {
               // Full tracking data is available! Woo hoo!
               TA.setColor(TrackingBoxSet::ETPGreen); // Never leave it blue
               TrkPtsEditor->notifyTrackingComplete();
               HandleSmoothCommand();

               HowLongItTookToTrackInSecs = 0;
               ShowedInMarkMoveMessage = false;
               ShowedOutMarkMoveMessage = false;
					TrackingDataHasBeenUsedToRenderAll = false;
            }

#else
            //TA.eraseAllBut(newMarkIn);

#endif

         }
      }

      // If we're not restoring the saved marks, we need to move the
      // tracking points to a different frame - current mark IN frame
      // or current frame if IN isn't set, and we need to erase all
      // tracking data other than the original points!

      if (!restoreMarks)
      {
         int markInFrame = getSystemAPI()->getMarkIn();
			int markOutFrame = getSystemAPI()->getMarkOut();
			if (markInFrame == -1)
         {
            // No IN mark - set it at the current frame
            markInFrame = getSystemAPI()->getLastFrameIndex();
            getSystemAPI()->setMarkIn(markInFrame);
            // We don't do anything with the OUT mark
         }
         else
         {
            // Show the mark IN frame so we can see the points
            getSystemAPI()->goToFrameSynchronous(markInFrame);
         }
         // Note: we may or may not have a valid OUT mark at this point....

         // Erase all the saved trackpoints and move them to the new mark IN
			TA.setInAndOutFrames(markInFrame, markOutFrame);

         // Fingers crossed!!
      }
   }

   // OTHER OPTIONS

   // 1. Ignore the tracking points
   // ResetTracking();

   // 2 Delete the saved tracking points
   // ResetTracking();
   // if (DoesFileExist(sName.c_str()))
   // if (!DeleteFile(sName.c_str()))
   // TRACE_0(errout << "ERROR: Stabilizer: Could not remove "
   // + sName.c_str());

   // 3. Move the tracking points to the new IN frame
   // TrackingBoxList tps = TA[TA.getInFrame()];
   // ResetTracking();
   // TA[getSystemAPI()->getMarkIn()] = tps;

	// TA.setExtents(CurrentTrackBoxExtentX, CurrentTrackBoxExtentY,
   // CurrentSearchBoxExtentLeft, CurrentSearchBoxExtentRight,
   // CurrentSearchBoxExtentUp, CurrentSearchBoxExtentDown);
   TA.setTrackingBoxRadius(CurrentTrackBoxExtentX);
   TA.setSearchBoxRadius(CurrentSearchBoxExtentLeft);
   SetTrackingPoints(TA);
   // CAREFUL! TA was copied to real Tracking Array - DON'T REFER TO TA
   // ANYMORE AFTER THIS!

	TrackingDataHasBeenUsedToRenderAll = false;
   TrkPtsEditor->lastSelectedTag = INVALID_FRAME_INDEX;
   TrkPtsEditor->refreshTrackingPointsFrame();
	UpdateTrackingPointGUI();
	UpdateExecutionGUI(TOOL_CONTROL_STATE_STOPPED, false);
   StabilizerForm->UpdateStatusBarInfo();

   return true;
}
// ----------------------------------------------------------------------------

void CStabilizerTool::DeleteSelectedTrackingPoints()
{
   getSystemAPI()->AcceptGOV(); // Auto-accept GOV if pending

	TrackingDataHasBeenUsedToRenderAll = false;
	TrkPtsEditor->deleteSelectedTrackingPoints();
	SetControlState(STABILIZER_STATE_IDLE);
   SelectTrackingPointRelative(0);
   UpdateTrackingPointGUI();
}
// ----------------------------------------------------------------------------

void CStabilizerTool::ClearTrackingPoints()
{
	TrackingDataHasBeenUsedToRenderAll = false;
	TrkPtsEditor->resetTrackingArray(); // does TrackingArray.clear();
	SetTrackPointsVisibility(!isInAutoTrack());
	TrkPtsEditor->lastSelectedTag = INVALID_FRAME_INDEX;
   TrkPtsEditor->refreshTrackingPointsFrame();
	UpdateTrackingPointGUI();
}

// ----------------------------------------------------------------------------
//
// SelectTrackingPointRelative
// Called by GUI in response to the tracking point Prev/Next buttons
//
void CStabilizerTool::SelectTrackingPointRelative(int plusOrMinus)
{
   if (GetNumberOfTrackingBoxes() < 1 || plusOrMinus == 0)
   {
      // No points or we were told not to move.
      return;
   }

   auto tags = TrackingArray.getTrackingBoxTags();
   auto iter = find(tags.begin(), tags.end(), TrkPtsEditor->lastSelectedTag);
   if (iter == tags.end())
   {
      iter = (plusOrMinus > 0) ? tags.begin() : (tags.end() - 1);
   }

   if (plusOrMinus > 0)
   {
      ++iter;
      if (iter == tags.end())
      {
         iter = tags.begin();
      }
   }
   else
   {
      if (iter == tags.begin())
      {
         iter = tags.end();
      }

      --iter;
   }

   TrkPtsEditor->setUniqueSelection(*iter);
}
// ----------------------------------------------------------------------------

void CStabilizerTool::NeutralizeTrackingPointAtOffset(int offset)
{
   MTIassert(TrkPtsEditor->lastSelectedTag != INVALID_FRAME_INDEX);
   if (TrkPtsEditor->lastSelectedTag == INVALID_FRAME_INDEX)
   {
      return;
   }

   TrkPtsEditor->setUniqueSelection(TrkPtsEditor->lastSelectedTag);
   TrackingBox &trackingBox = GetTrackingArrayRef().getTrackingBoxByTag(TrkPtsEditor->lastSelectedTag);
   int frameToNeutralize = trackingBox.getStartFrame() + offset;
   MTIassert(trackingBox.isValidAtFrame(frameToNeutralize))
   if (trackingBox.isValidAtFrame(frameToNeutralize) == false)
   {
      return;
   }

   trackingBox.neutralizePositionAtFrame(frameToNeutralize);

   if (GetControlState() == STABILIZER_STATE_IDLE)
   {
		setNextStateAfterSmoothing(STABILIZER_STATE_IDLE);
		smoothDataIsValid = false;
		SetControlState(STABILIZER_STATE_NEED_TO_RUN_SMOOTHER);
   }

   getSystemAPI()->refreshFrameCached();
}
// ----------------------------------------------------------------------------

string CStabilizerTool::GetSuggestedTrackingDataSaveFilename()
{
   JobManager jobManager;
   string suggestedFolder = jobManager.GetJobFolderSubfolderPath(JobManager::stabilizeType);
   string suggestedFilePath = jobManager.GetJobFolderFileSavePath(suggestedFolder, "", false, "pts");
   return suggestedFilePath;
}
// ----------------------------------------------------------------------------

string CStabilizerTool::GetSuggestedTrackingDataLoadFilename()
{
   // Suggests the latest version for these marks.
   JobManager jobManager;
   string suggestedFolder = jobManager.GetJobFolderSubfolderPath(JobManager::stabilizeType);
   string suggestedFilePath = jobManager.GetJobFolderFileLoadPath(suggestedFolder, "", false, "pts");
   return suggestedFilePath;
}

// ----------------------------------------------------------------------------
//
// Get a filename from the user, and save the tracking points in the file
//
void CStabilizerTool::SaveTrackingPointsToFile()
{
   string suggestedFilename = GetSuggestedTrackingDataSaveFilename();
   string filename = GetFilenameForSavingTrackingPoints(suggestedFilename);
   if (filename.empty())
   {
      return;
   }

   SaveTrackingPoints(filename);
}

// ----------------------------------------------------------------------------
//
// Auto-save the tracking points to the file suggested by the Job Manager
//
void CStabilizerTool::AutoSaveTrackingPoints()
{
   if (!needToAutoSave || GetNumberOfTrackingBoxes() < 1)
   {
      return;
   }

   needToAutoSave = false;
   SaveTrackingPoints(GetSuggestedTrackingDataSaveFilename());
}

// ----------------------------------------------------------------------------
//
// Get a filename from the user, and load the tracking points in the file
//
void CStabilizerTool::LoadTrackingPointsFromFile(bool restoreMarks, bool restoreProcessingRegion)
{
   string suggestedFilename = GetSuggestedTrackingDataLoadFilename();
   string filename = GetFilenameForLoadingTrackingPoints(suggestedFilename);
   if (filename.empty())
   {
      return;
   }

	LoadTrackingPoints(filename, restoreMarks, restoreProcessingRegion);
}
// ----------------------------------------------------------------------------

bool CStabilizerTool::isShiftOn()
{
   return shiftIsOn;
}
// ----------------------------------------------------------------------------

bool CStabilizerTool::wasTrackingDataInvalidatedByRendering()
{
	if (TrkPtsEditor == nullptr)
	{
		return false;
	}

	return TrkPtsEditor->wasTrackingDataInvalidatedByRendering();
}

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
// -------------------------- SMOOTHING --------------------------------------
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

int CStabilizerTool::DoSmoothing()
{
	CAutoErrorReporter autoErr("CStabilizerTool::DoSmoothing", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

	SetControlState(STABILIZER_STATE_SMOOTHING);

	if (isInAutoTrack())
	{
		RobustArray = SmoothRobustArray = TrackingArray;
		TrackingBox &trackingBox = SmoothRobustArray.getTrackingBoxByIndex(0);
		int inFrame = SmoothRobustArray.getInFrame();
		int outFrame = SmoothRobustArray.getOutFrame();
		for (int frameNumber = inFrame; frameNumber < outFrame; ++frameNumber)
		{
            // Smoothing to a straight line at the arbitrary offset reference level.
            trackingBox.setTrackPointAtFrame(
                frameNumber,
                FPOINT(AUTO_TRACK_OFFSET_REFERENCE_LEVEL, AUTO_TRACK_OFFSET_REFERENCE_LEVEL));
		}

		SetControlState(STABILIZER_STATE_SMOOTHING_COMPLETE);
		setStatusMessage("Smoothing complete");
		GetToolProgressMonitor()->SetStatusMessage("Smoothing complete");
		smoothDataIsValid = true;

		return 0;
	}

   // Fire up the controller thread, which will do all the work
   void * threadid = BThreadSpawn(DoSmoothing_trampoline, this);
   if (threadid == nullptr)
   {
      autoErr.errorCode = STABILIZER_ERROR_CANT_SPAWN_THREAD;
      autoErr.msg << "Can't spawn thread";
		SetControlState(STABILIZER_STATE_GOT_AN_ERROR);
		return STABILIZER_ERROR_CANT_SPAWN_THREAD;
	}

   // Immediate return
   return 0;
}

static bool SENTRY = false;

// Static method: bounce to the worker code
void CStabilizerTool::DoSmoothing_trampoline(void *appData, void *threadid)
{
	CStabilizerTool *_this = reinterpret_cast<CStabilizerTool*>(appData);

	if (SENTRY)
	{
		TRACE_0(errout << "SENTRY WTF!");
		while (SENTRY)
		{
			MTIsleep(1);
		}
	}

	SENTRY = true;

   BThreadBegin(threadid); // Release the caller

	_this->DoSmoothing_thread();

	SENTRY = false;

	// Thread automatically destroyed when it exits here.
}

// The worker code
void CStabilizerTool::DoSmoothing_thread()
{
	CAutoThreadLocker autolock(_smoothingLock);

	CAutoErrorReporter autoErr("CStabilizerTool::DoSmoothing_trampoline", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

	try
	{
		setStatusMessage("Smoothing");
		GetToolProgressMonitor()->SetStatusMessage("Smoothing");

		GatherGUIParameters(_stabilizerParameters);
	}
	catch (...)
	{
		TRACE_0(errout << "***ERROR**** Cannot gather stabilizer GUI parameters");
		autoErr.errorCode = -8872;
		autoErr.msg << "Smoothing failed: Cannot gather stabilizer GUI parameters (-8872)";
		return;
	}

	bool success;
	try
	{
		success = ComputeRobustPoints(
						_stabilizerParameters.RefFrameIndex,
						_stabilizerParameters.HMode,
						_stabilizerParameters.VMode,
						_stabilizerParameters.RMode);
	}
	catch (...)
	{
		TRACE_0(errout << "***ERROR**** Cannot compute robust points!");
		autoErr.errorCode = -8873;
		autoErr.msg << "Smoothing failed: Cannot compute robust points! (-8873)";
		setStatusMessage("Smoothing failed!");
		SetControlState(STABILIZER_STATE_GOT_AN_ERROR);
		return;
	}

	if (success)
	{
		try
		{
			success = SmoothRobustPoints(
							_stabilizerParameters.GoodFramesIn,
							_stabilizerParameters.GoodFramesOut,
							_stabilizerParameters.RefFrameIndex,
							_stabilizerParameters.Alpha2,
							_stabilizerParameters.HMode,
							_stabilizerParameters.VMode,
							_stabilizerParameters.RMode);
		}
		catch (...)
		{
			TRACE_0(errout << "***ERROR**** Cannot smooth robust points!");
			autoErr.errorCode = -8874;
			autoErr.msg << "Smoothing failed: Cannot compute robust points! (-8874)";
			setStatusMessage("Smoothing failed!");
			SetControlState(STABILIZER_STATE_GOT_AN_ERROR);
			return;
		}
	}

	if (!success)
	{
		////autoErr.traceAction = AUTO_ERROR_NO_TRACE;
		autoErr.errorCode = theError.getError();
		autoErr.msg << "Smoothing failed: " << theError.getMessage();
		SetControlState(STABILIZER_STATE_GOT_AN_ERROR);
		return;
	}

	AddSyntheticPoints();
	SetControlState(STABILIZER_STATE_SMOOTHING_COMPLETE);
	setStatusMessage("Smoothing complete");
	GetToolProgressMonitor()->SetStatusMessage("Smoothing complete");
	smoothDataIsValid = true;
}
// ----------------------------------------------------------------------------

void CStabilizerTool::AddSyntheticPoints(void)
{
	// The renderer needs at least three points, so we oblige it by filling in
	// one or two synthesized points
	int firstRobustFrameIndex = RobustArray.getInFrame();
	int lastRobustFrameIndex = RobustArray.getOutFrame() - 1;
	int numberOfRobustPointsPerFrame = RobustArray.getNumberOfTrackingBoxes();

   if (numberOfRobustPointsPerFrame == 1)
   {
      for (int frame = firstRobustFrameIndex; frame <= lastRobustFrameIndex; ++frame)
      {
         auto robustPt = RobustArray.getTrackingBoxByIndex(0)[frame];
         auto smoothPt = SmoothRobustArray.getTrackingBoxByIndex(0)[frame];
         FPOINT robustSynthPt1(robustPt + FPOINT(100, 0));
         FPOINT robustSynthPt2(robustPt + FPOINT(0, 100));
         FPOINT smoothSynthPt1(smoothPt + FPOINT(100, 0));
         FPOINT smoothSynthPt2(smoothPt + FPOINT(0, 100));

         if (frame == firstRobustFrameIndex)
         {
            // Need to create the tracking boxes on the first pass!
            RobustArray.addTrackingBox(frame, robustSynthPt1);
            RobustArray.addTrackingBox(frame, robustSynthPt2);
            SmoothRobustArray.addTrackingBox(frame, smoothSynthPt1);
            SmoothRobustArray.addTrackingBox(frame, smoothSynthPt2);
         }
         else
         {
            RobustArray.getTrackingBoxByIndex(1).setTrackPointAtFrame(frame, robustSynthPt1);
            RobustArray.getTrackingBoxByIndex(2).setTrackPointAtFrame(frame, robustSynthPt2);
            SmoothRobustArray.getTrackingBoxByIndex(1).setTrackPointAtFrame(frame, smoothSynthPt1);
            SmoothRobustArray.getTrackingBoxByIndex(2).setTrackPointAtFrame(frame, smoothSynthPt2);
         }
      }
   }
   else if (numberOfRobustPointsPerFrame == 2)
   {
      for (int frame = firstRobustFrameIndex; frame <= lastRobustFrameIndex; ++frame)
      {
         double rx0 = RobustArray.getTrackingBoxByIndex(0)[frame].x;
         double ry0 = RobustArray.getTrackingBoxByIndex(0)[frame].y;
         double sx0 = SmoothRobustArray.getTrackingBoxByIndex(0)[frame].x;
         double sy0 = SmoothRobustArray.getTrackingBoxByIndex(0)[frame].y;
         double rx1 = RobustArray.getTrackingBoxByIndex(1)[frame].x;
         double ry1 = RobustArray.getTrackingBoxByIndex(1)[frame].y;
         double sx1 = SmoothRobustArray.getTrackingBoxByIndex(1)[frame].x;
         double sy1 = SmoothRobustArray.getTrackingBoxByIndex(1)[frame].y;
         FPOINT robustSynthPt(rx0 + (ry1 - ry0), ry0 - (rx1 - rx0));
         FPOINT smoothSynthPt(sx0 + (sy1 - sy0), sy0 - (sx1 - sx0));

         if (frame == firstRobustFrameIndex)
         {
            // Need to create the tracking boxes on the first pass!
            RobustArray.addTrackingBox(frame, robustSynthPt);
            SmoothRobustArray.addTrackingBox(frame, smoothSynthPt);
         }
         else
         {
            RobustArray.getTrackingBoxByIndex(2).setTrackPointAtFrame(frame, robustSynthPt);
            SmoothRobustArray.getTrackingBoxByIndex(2).setTrackPointAtFrame(frame, smoothSynthPt);
         }
      }
   }
}

// -------------------ComputeRobustTrack--------------------------Sept 2007---

bool CStabilizerTool::ComputeRobustTrack(int nextFrameIndex, int refFrameIndex)
{
   if (GetNumberOfTrackingBoxes() < 1)
   {
      return false;
   }

   MTIassert(RobustArray.getNumberOfTrackingBoxes() == 2);

   mvector<double>vec_b(3);
   vec_b.zero();

   if (refFrameIndex < TrackingArray.getInFrame())
   {
      refFrameIndex = TrackingArray.getInFrame();
   }
   if (refFrameIndex >= TrackingArray.getOutFrame())
   {
      refFrameIndex = TrackingArray.getOutFrame() - 1;
   }

   if (nextFrameIndex != refFrameIndex)
   {
      int n = GetNumberOfTrackingBoxes(); //TrackingArray.isValid(nextFrameIndex) ? (int) TrackingArray.at(nextFrameIndex).size() : 0;
      mvector<double>vec_y(n * 2);
      matrix<double>matrix_X(n * 2, 3);
      double threshold = 0.001;
      auto InputPoints = TrackingArray.getAllTrackPointsAtFrame(nextFrameIndex);
      auto RefPoints = TrackingArray.getAllTrackPointsAtFrame(refFrameIndex);

      TRACE_3(errout << endl << "=== FRAME " << nextFrameIndex << " ====");

      for (int iTag = 0; iTag < n; ++iTag)
      {
         vec_y[iTag] = InputPoints[iTag].x - RefPoints[iTag].x;
         matrix_X[iTag][0] = 1;
         matrix_X[iTag][1] = 0;
         matrix_X[iTag][2] = -RefPoints[iTag].y;

         vec_y[iTag + n] = InputPoints[iTag].y - RefPoints[iTag].y;
         matrix_X[iTag + n][0] = 0;
         matrix_X[iTag + n][1] = 1;
         matrix_X[iTag + n][2] = RefPoints[iTag].x;

         TRACE_3(errout << "IN[" << iTag << "] = (" << vec_y[iTag] << "," << vec_y[iTag + n] << ")");
      }

      if (!robust(vec_y, matrix_X, threshold, vec_b))
      {
         TRACE_1(errout << "WARNING: Check frame " << nextFrameIndex << " -- regression did not reach convergence");
      }
   }

   TRACE_3(errout << "OUT = (" << vec_b[0] << "," << vec_b[1] << "," << vec_b[2] << ")");

   matrix<double>rotMat(2, 2);
   mvector<double>vec_oldPt(2);
   mvector<double>vec_newPt(2);

   // Rotation matrix
   rotMat[0][0] = 1.0;
   rotMat[0][1] = -vec_b[2];
   rotMat[1][0] = vec_b[2];
   rotMat[1][1] = 1.0;

   // Synthetic point 0 set at (0,0) so doesn't get rotated, only translated
   FPOINT newPt(vec_b[0], vec_b[1]);
   RobustArray.getTrackingBoxByIndex(0).setTrackPointAtFrame(nextFrameIndex, newPt);

   // Synthetic point 1 - originally at (100,0) so apply rotation
   vec_oldPt[0] = 100.0;
   vec_oldPt[1] = 0.0;
   vec_newPt = vec_b + (rotMat * vec_oldPt);
   newPt.x = vec_newPt[0];
   newPt.y = vec_newPt[1];
   RobustArray.getTrackingBoxByIndex(1).setTrackPointAtFrame(nextFrameIndex, newPt);

#if 0 // Should be able to use the other synthetic point after smoothing
   // Synthetic point 2 - originally at (0,100)
   vec_oldPt[0] = 0.0;
   vec_oldPt[1] = 100.0;
   vec_newPt = vec_b + (rotMat * vec_oldPt);
   newPt.x = vec_newPt[0];
   newPt.y = vec_newPt[1];
   RobustArray.getTrackingBoxByIndex(2).setTrackPointAtFrame(nextFrameIndex, newPt);
#endif // 0

   TRACE_3(errout << "0: " << RobustArray.getTrackingBoxByIndex(0).getTrackPointAtFrame(nextFrameIndex));
   TRACE_3(errout << "1: " << RobustArray.getTrackingBoxByIndex(1).getTrackPointAtFrame(nextFrameIndex));
#if 0 // Should be able to use the other synthetic point after smoothing
   TRACE_3(errout << "2: " << RobustArray.getTrackingBoxByIndex(2).getTrackPointAtFrame(nextFrameIndex));
#endif // 0

   return true;

}
// -------------------ComputeRobustTrackNoRotation-----------------Sept 2007---

bool CStabilizerTool::ComputeRobustTrackNoRotation(int nextFrameIndex, int refFrameIndex)
{
	if (GetNumberOfTrackingBoxes() < 1)
   {
      return false;
   }

   MTIassert(RobustArray.getNumberOfTrackingBoxes() == 1);

   mvector<double>vec_b(2);
   vec_b.zero();

   // Hmmm - ref frame is not allowed to be outside the range!
   if (refFrameIndex < TrackingArray.getInFrame())
   {
      refFrameIndex = TrackingArray.getInFrame();
   }

   if (refFrameIndex >= TrackingArray.getOutFrame())
   {
      refFrameIndex = TrackingArray.getOutFrame() - 1;
   }

   if (nextFrameIndex != refFrameIndex)
   {
      auto InputPoints = TrackingArray.getAllTrackPointsAtFrame(nextFrameIndex);
      auto RefPoints = TrackingArray.getAllTrackPointsAtFrame(refFrameIndex);
      int n = InputPoints.size();

      //int n = GetNumberOfTrackingBoxes(); //TrackingArray.isValid(nextFrameIndex) ? (int) TrackingArray[nextFrameIndex].size() : 0;
      mvector<double>vec_y(n * 2);
      matrix<double>matrix_X(n * 2, 2);
      double threshold = 0.001;
      TRACE_3(errout << endl << "=== FRAME " << nextFrameIndex << " ====");

      for (int index = 0; index < n; ++index)
      {
         FPOINT x0 = InputPoints[index];
         FPOINT x1 = RefPoints[index];
         vec_y[index] = InputPoints[index].x - RefPoints[index].x;
         matrix_X[index][0] = 1;
         matrix_X[index][1] = 0;

         vec_y[index + n] = InputPoints[index].y - RefPoints[index].y;
         matrix_X[index + n][0] = 0;
         matrix_X[index + n][1] = 1;

         TRACE_3(errout << "IN[" << index << "] = (" << vec_y[index] << "," << vec_y[index + n] << ")");
      }

      if (!robust(vec_y, matrix_X, threshold, vec_b))
      {
         TRACE_1(errout << "WARNING: Check frame " << nextFrameIndex << " -- regression did not reach convergence");
      }
   }

   TRACE_3(errout << "OUT = (" << vec_b[0] << "," << vec_b[1] << ")");

   FPOINT newPt(vec_b[0], vec_b[1]);
   RobustArray.getTrackingBoxByIndex(0).setTrackPointAtFrame(nextFrameIndex, newPt);
#if 0 // waste of time to smooth these
   RobustArray[nextFrameIndex][1] = newPt;
   RobustArray[nextFrameIndex][1].x += 100.0;
   RobustArray[nextFrameIndex][2] = newPt;
   RobustArray[nextFrameIndex][2].y += 100.0;
#endif // 0

   return true;
}

// ------------------------ComputeRobustPoints---------------------Sept 2007---

bool CStabilizerTool::ComputeRobustPoints(int refFrameIndex, EStabilizationMode ModeX, EStabilizationMode ModeY, EStabilizationMode ModeR)
{
   int firstTrackingFrameIndex = TrackingArray.getInFrame();
   int lastTrackingFrameIndex = TrackingArray.getOutFrame() - 1;
	int numberOfTrackPointsPerFrame = GetNumberOfTrackingBoxes();

   if (numberOfTrackPointsPerFrame < 1)
   {
      return false;
   }

   RobustArray.clear();
//	RobustArray.setInFrame(firstTrackingFrameIndex);
//	RobustArray.setOutFrame(lastTrackingFrameIndex + 1);
	RobustArray.setInAndOutFrames(firstTrackingFrameIndex, lastTrackingFrameIndex + 1);

   if ((numberOfTrackPointsPerFrame == 1) || (numberOfTrackPointsPerFrame == 2 && ModeR != STABMODE_NONE))
   {
      // Can't get any more robust!
      for (auto index = 0; index < numberOfTrackPointsPerFrame; ++index)
      {
         RobustArray.addTrackingBox(TrackingArray.getTrackingBoxByIndex(index));
      }
   }
	else if (numberOfTrackPointsPerFrame == 2)
   {
      // For two points and no rotation, we just use the average.
      for (auto frame = firstTrackingFrameIndex; frame <= lastTrackingFrameIndex; ++frame)
      {
         auto point0 = TrackingArray.getTrackingBoxByIndex(0).getTrackPointAtFrame(frame);
         auto point1 = TrackingArray.getTrackingBoxByIndex(1).getTrackPointAtFrame(frame);
         double x = (point0.x + point1.x) / 2.0;
         double y = (point0.y + point1.y) / 2.0;
         FPOINT newPt(x, y);

         if (frame == firstTrackingFrameIndex)
         {
            RobustArray.addTrackingBox(frame, newPt);
         }
         else
         {
            RobustArray.getTrackingBoxByIndex(0).setTrackPointAtFrame(frame, newPt);
         }
      }
   }
   else
   {
      // For more than two points, we do robust regression.
      setStatusMessage("Initializing...");
      setTimeMessage("");
      RobustArray.addTrackingBox(firstTrackingFrameIndex, FPOINT(0, 0));
      if (ModeR != STABMODE_NONE)
      {
         RobustArray.addTrackingBox(firstTrackingFrameIndex, FPOINT(100, 0));
      }

      for (int frame = firstTrackingFrameIndex; frame <= lastTrackingFrameIndex; ++frame)
      {
         if (ModeR == STABMODE_NONE)
         {
            ComputeRobustTrackNoRotation(frame, refFrameIndex);
         }
         else
         {
            ComputeRobustTrack(frame, refFrameIndex);
         }
      }
   }
   return true;
}

// ------------------------SmoothThreadparams---------------------------------

struct SmoothThreadParams
{
   SmoothThreadParams(CStabilizerTool *argTool) : Tool(argTool), Mode(STABMODE_NONE), TP(nullptr), iGoodStart(0), iGoodEnd(0),
                                                  refFrameIndex(0), Alpha2(0.0), PercentDone(nullptr), AbortFlag(nullptr)
   {
   };

   CStabilizerTool *Tool;
   EStabilizationMode Mode;
   std::valarray<double> *TP;
   int iGoodStart;
   int iGoodEnd;
   int refFrameIndex;
   double Alpha2;
   int *PercentDone;
   bool *AbortFlag;
};

// ------------------------SmoothRobustPoints----------------------Sept 2007---

bool CStabilizerTool::SmoothRobustPoints(int iGoodStart, int iGoodEnd, int refFrameIndex, double Alpha2, EStabilizationMode ModeX,
      EStabilizationMode ModeY, EStabilizationMode ModeR, int *percentDone, bool *abortFlag)
{
   CBusyCursor Busy(true);

   setStatusMessage("Phase 2...");

//   // Collapse the array
//   if (!RobustArray.collapse())
//   {
//      theError.set(STABILIZER_ERROR_TRACKING_COLLAPSE, "Internal Error: A frame is missing a tracking point");
//      return false;
//   }

   // This may have to change
   int firstTrackingFrameIndex = RobustArray.getInFrame();
   int lastTrackingFrameIndex = RobustArray.getOutFrame() - 1;
   int NumberOfFrames = lastTrackingFrameIndex - firstTrackingFrameIndex + 1;
	int NumOfPointsPerFrame = RobustArray.getNumberOfTrackingBoxes();

   if (NumOfPointsPerFrame < 1)
   {
      theError.set(STABILIZER_ERROR_STABILIZER_TOO_FEW_TRACKPOINTS, "INTERNAL ERROR: No robust points found to smooth! ");
      return false;
   }

   if (NumOfPointsPerFrame > 2)
   {
      theError.set(STABILIZER_ERROR_STABILIZER_TOO_MANY_TRACKPOINTS, "INTERNAL ERROR: More than two robust points passed to smoother! ");
      return false;
   }

   if (NumberOfFrames <= (iGoodStart + iGoodEnd))
   {
// NO! This is annoying when you move the mark in to the mark out position in the middle of moving the range!!
//      theError.set(STABILIZER_ERROR_SMOOTHING_TOO_FEW_FRAMES,
//            "Operation failed: To render this shot, the marked frame range must be greater than the number of anchor frames");
		return false;
   }

   // Sanity
   int refFrameOffset = refFrameIndex - firstTrackingFrameIndex;
   if (refFrameOffset < 0 || refFrameOffset >= NumberOfFrames)
   {
      refFrameOffset = 0;
   }

   std::valarray<double>xTP0(NumberOfFrames);
   std::valarray<double>yTP0(NumberOfFrames);
   std::valarray<double>xTP1(NumberOfFrames);
   std::valarray<double>yTP1(NumberOfFrames);

	auto inFrame = RobustArray.getInFrame();
	auto outFrame = RobustArray.getOutFrame();

   SmoothThreadParams smoothThreadParams(this);
   smoothThreadParams.iGoodStart = iGoodStart;
	smoothThreadParams.iGoodEnd = iGoodEnd;
	smoothThreadParams.refFrameIndex = min<int>(outFrame - inFrame - 1, max<int>(0, refFrameIndex - inFrame));
   smoothThreadParams.Alpha2 = Alpha2;

   SmoothCountingWait.SetCount(2 * NumOfPointsPerFrame);

   // Todo: refactor this to TrackingBoxSet assignment
   SmoothRobustArray.clear();
//   SmoothRobustArray.setInFrame(inFrame);
//   SmoothRobustArray.setOutFrame(outFrame);
	SmoothRobustArray.setInAndOutFrames(inFrame, outFrame);
	for (auto tag : RobustArray.getTrackingBoxTags())
   {
      SmoothRobustArray.addTrackingBox(RobustArray.getTrackingBoxByTag(tag));
   }

	auto sTP = RobustArray.getTrackingBoxByIndex(0);
	for (auto frame = inFrame; frame < outFrame; ++frame)
	{
		xTP0[frame - inFrame] = sTP.getTrackPointAtFrame(frame).x;
		yTP0[frame - inFrame] = sTP.getTrackPointAtFrame(frame).y;
	}

   smoothThreadParams.Mode = ModeX;
   smoothThreadParams.TP = &xTP0;
   BThreadSpawn(StabilizerSmoothTrampoline, &smoothThreadParams);
   smoothThreadParams.Mode = ModeY;
   smoothThreadParams.TP = &yTP0;
   BThreadSpawn(StabilizerSmoothTrampoline, &smoothThreadParams);

   vector<void *> bThreadStructs;
   if (NumOfPointsPerFrame > 1)
   {
      auto sTP = RobustArray.getTrackingBoxByIndex(1);
      for (auto frame = inFrame; frame < outFrame; ++frame)
      {
         xTP1[frame - inFrame] = sTP.getTrackPointAtFrame(frame).x;
         yTP1[frame - inFrame] = sTP.getTrackPointAtFrame(frame).y;
      }

      smoothThreadParams.Mode = ModeX;
      smoothThreadParams.TP = &xTP1;
      bThreadStructs.push_back(BThreadSpawn(StabilizerSmoothTrampoline, &smoothThreadParams));
      smoothThreadParams.Mode = ModeY;
      smoothThreadParams.TP = &yTP1;
      bThreadStructs.push_back(BThreadSpawn(StabilizerSmoothTrampoline, &smoothThreadParams));
   }

   // Wait for all threads to finish
   SmoothCountingWait.Wait(bThreadStructs, __func__, __LINE__);
#if 0 // need to think about this
   GetToolProgressMonitor()->StopProgress(true);
#endif

   // Put them into the smoothed array
	for (int frame = inFrame; frame < outFrame; ++frame)
	{
		FPOINT newTP0(xTP0[frame - inFrame], yTP0[frame - inFrame]);
		SmoothRobustArray.getTrackingBoxByIndex(0).setTrackPointAtFrame(frame, newTP0);

		if (NumOfPointsPerFrame > 1)
		{
			FPOINT newTP1(xTP1[frame - inFrame], yTP1[frame - inFrame]);
			SmoothRobustArray.getTrackingBoxByIndex(1).setTrackPointAtFrame(frame, newTP1);
		}
	}

   return true;
}

// Translate the static to class
void CStabilizerTool::StabilizerSmoothTrampoline(void *vpAppData, void *vpReserved)
{
   // Copy the parameters before the BThreadBegin, which releases the parent
   SmoothThreadParams smoothThreadParams = *reinterpret_cast<SmoothThreadParams*>(vpAppData);

   if (BThreadBegin(vpReserved))
   {
      TRACE_0(errout << "StabilizerSmoothTrampoline: BThreadBegin failed");
      smoothThreadParams.Tool->SmoothCountingWait.ReleaseOne();
      exit(1);
   }

   try
   {
		smoothThreadParams.Tool->StabilizerSmooth3(smoothThreadParams.Mode, *smoothThreadParams.TP, smoothThreadParams.iGoodStart,
				smoothThreadParams.iGoodEnd, smoothThreadParams.refFrameIndex, smoothThreadParams.Alpha2, smoothThreadParams.PercentDone,
            smoothThreadParams.AbortFlag);
   }
   catch (...)
   {
      TRACE_0(errout << "StabilizerSmoothTrampoline: Stabilizer Slice created major error");
   }

   smoothThreadParams.Tool->SmoothCountingWait.ReleaseOne();

   MTImillisleep(10); // in case smoothing was really fast
}
///////////////////////////////////////////////////////////////////////////////

// ------------------------StabilizerSmooth3--------------------------Apr 2007---

bool CStabilizerTool::StabilizerSmooth3(EStabilizationMode Mode, std::valarray<double> &TP, int iGoodStart, int iGoodEnd, int refFrameOffset,
      double Alpha, int *percentDone, bool *abortFlag)
{
   bool retVal = true;
   const double TINY_DOUBLE = 1.0e-30; // Sigh... alpha = 0 is special

   switch (Mode)
   {
   case STABMODE_NONE:
      // No smoothing!
      break;

   case STABMODE_SMOOTHED:
      // stupid hack because smooth3 considers alpha = 0.0 to mean "flat"
      // instead of what it should be: "linear"
      if (Alpha == 0.0)
      {
         Alpha = TINY_DOUBLE;
      }
		retVal = Smooth3(TP, iGoodStart, iGoodEnd, Alpha, percentDone, abortFlag);
      break;

	case STABMODE_FLAT:
		if (iGoodStart == 1 && iGoodEnd == 0)
		{
			// Special case - flat line starting at first point.
			for (unsigned i = 1; i < TP.size(); i++)
			{
				TP[i] = TP[0];
			}
		}
		else if (iGoodStart == 0 && iGoodEnd == 1)
		{
			// Special case - flat line ending at last point.
			int lastIndex = TP.size() - 1;
			for (unsigned i = 0; i < lastIndex; i++)
			{
				TP[i] = TP[lastIndex];
			}
		}
		else
		{
			// Call the smoother with Alpha = 0.
			retVal = Smooth3(TP, iGoodStart, iGoodEnd, 0.0);
		}
      break;

   case STABMODE_REGISTERED:
		// Smooth3 doesn't handle this case
		for (unsigned i = 0; i < TP.size(); i++)
		{
			TP[i] = TP[refFrameOffset];
		}
		break;
   }

   return retVal;

}
// ----------------------------------------------------------------------------

void CStabilizerTool::MarkSmoothedDataInvalid()
{
	smoothDataIsValid = false;
}
// ----------------------------------------------------------------------------

void CStabilizerTool::ColorPickerColorChanged(void *Sender)
{
   // We don't do anything here yet
}
// ----------------------------------------------------------------------------

void CStabilizerTool::ColorPickerColorPicked(void *Sender)
{
   int R, G, B, X, Y;
   ColorPickTool->getColor(R, G, B);
   ColorPickTool->getXY(X, Y);
   FindBoundingBox(R, G, B, X, Y);
}
// ----------------------------------------------------------------------------

void CStabilizerTool::EnterColorPickMode(void)
{
   ColorPickTool->SetEnabled(true);
   getSystemAPI()->DeactivateTrackingTool();
}
// ----------------------------------------------------------------------------

void CStabilizerTool::ExitColorPickMode(void)
{
   ColorPickTool->SetEnabled(false);
   getSystemAPI()->ActivateTrackingTool();
}
// ----------------------------------------------------------------------------

bool CStabilizerTool::IsInColorPickMode(void)
{
   return ColorPickTool->IsEnabled();
}
// ----------------------------------------------------------------------------

bool CStabilizerTool::CheckIfNeedToRetrack() const
{

	auto nonConstThis = const_cast<CStabilizerTool *>(this);
	bool missingTrackData = !isAllTrackDataValid();
	bool trackBoxParametersChanged =
			CurrentTrackBoxExtentX != TrackingArray.getTrackingBoxRadius()
			|| CurrentSearchBoxExtentLeft != TrackingArray.getSearchBoxRadius();
	bool marksChanged =
			TrackingArray.getInFrame() != nonConstThis->getSystemAPI()->getMarkIn()
			|| TrackingArray.getOutFrame() != nonConstThis->getSystemAPI()->getMarkOut();
   bool advancedStuffChanged =
      CurrentApplyLutBeforeTracking != TrackingArray.getApplyLutFlag()
      || CurrentDegrainBeforeTracking != TrackingArray.getDegrainFlag()
      || CurrentTrackingChannel != TrackingArray.getChannel();

	bool needToReTrack = missingTrackData
								|| marksChanged
								|| (isInAutoTrack() ? false : trackBoxParametersChanged);

	return needToReTrack;
}
// ----------------------------------------------------------------------------

void CStabilizerTool::SetClearTrackingStuffOnNewShot(bool flag)
{
   _clearTrackingStuffOnNewShot = flag;
}
// ----------------------------------------------------------------------------

// NOTE: There is now only one pass - second analysis pass was eliminated!
void CStabilizerTool::HandleKPass1Command()
{
//     DBTRACE(_toolProcessorTypeToMake);
	_toolProcessorTypeToMake = PROC_TYPE_KPASS_1;
	 SetControlState(STABILIZER_STATE_AUTOTRACKING_PASS_1);
	 StartToolProcessing(TOOL_PROCESSING_CMD_PREPROCESS, false);
}
// ----------------------------------------------------------------------------

 int CStabilizerTool::onToolProcessingStatus(const CAutotoolStatus& autotoolStatus)
 {
    // NOTE: There is now only one pass - second analysis pass was eliminated!
    if (GetControlState() == STABILIZER_STATE_AUTOTRACKING_PASS_1 && autotoolStatus.status == AT_STATUS_STOPPED)
    {
       SetControlState(STABILIZER_STATE_AUTOTRACKING_COMPLETE);
    }

    return CToolObject::onToolProcessingStatus(autotoolStatus);
 }

// ----------------------------------------------------------------------------

void CStabilizerTool::setNextStateAfterSmoothing(EStabilizerControlState newState)
{
	_nextStateAfterSmoothing = newState;
}
// ----------------------------------------------------------------------------

EStabilizerControlState CStabilizerTool::getNextStateAfterSmoothing()
{
	return _nextStateAfterSmoothing;
}
// ----------------------------------------------------------------------------

bool CStabilizerTool::areThereAnySceneBreaks()
{
	vector<int> cutList;
	getSystemAPI()->getCutList(cutList);

   // Note: there's always one cut at 0 and one at the clip out frame.
	return cutList.size() > 2;
}
// ----------------------------------------------------------------------------
