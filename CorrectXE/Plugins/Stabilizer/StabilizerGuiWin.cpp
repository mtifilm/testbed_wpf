#pragma link "MTIUNIT"
// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "StabilizerGuiWin.h"

#include "bthread.h"
#include "ClipAPI.h"
#include "err_stabilizer.h"
#include "JobManager.h"
#include "LoadTrackingBoxDialogUnit.h"
#include "MTIDialogs.h"
#include "MTIKeyDef.h"
#include "PDL.h"
#include "PresetParameterModel.h"
#include "ShowModalDialog.h"
#include "StabilizerPreferencesUnit.h"

// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "VTimeCodeEdit"
//#pragma link "CSPIN"
#pragma link "ExecButtonsFrameUnit"
#pragma link "ExecStatusBarUnit"
#pragma link "TrackEditFrameUnit"
#pragma link "ColorPanel"
#pragma link "PresetsUnit"
#pragma resource "*.dfm"
TStabilizerForm *StabilizerForm;

#define NO_JITTER

// Our tab index on the Unitool tab bar
#define PLUGIN_TAB_INDEX 6

// Status bar panels
#define STATUS_BAR_STATUS 0
#define STATUS_BAR_MESSAGE 1
// #define STATUS_BAR_INFO 2     // not part of status bar anymore

// Random crap
#define CHART_SOFT_UNIT 2
#define MIN_CHART_EXTENT CHART_SOFT_UNIT
#define CHART_MARGIN 1

// Range of tracking and search sliders
#define TRACKING_SLIDER_MAX_2K 60
#define TRACKING_SLIDER_MIN_2K 5
#define TRACKING_SLIDER_MAX_4K (TRACKING_SLIDER_MAX_2K * 2)
#define TRACKING_SLIDER_MIN_4K (TRACKING_SLIDER_MIN_2K * 2)
#define SEARCH_SLIDER_MAX_2K   100
#define SEARCH_SLIDER_MIN_2K   5
#define SEARCH_SLIDER_MAX_4K   (SEARCH_SLIDER_MAX_2K * 2)
#define SEARCH_SLIDER_MIN_4K   (SEARCH_SLIDER_MIN_2K * 2)

#define PLUGIN_TAB_INDEX 6

// ---------------------------------------------------------------------------

// ------------------CreateStabilizerGUI---------------------

void CreateStabilizerToolGUI(void)

	// This creates the Stabilizer GUI if one does not already exist.
	// Note: only one can be created.
	//
	// ****************************************************************************
{
	if (StabilizerForm != NULL)
		return; // Already created

	StabilizerForm = new TStabilizerForm(Application); // Create it
	StabilizerForm->formCreate();

	StabilizerPreferencesForm = new TStabilizerPreferencesForm(StabilizerForm);
	StabilizerForm->RestoreProperties();

	// Move all our controls to the unified tabbed tool window
	extern const char *PluginName;
	GStabilizerTool->getSystemAPI()->AdoptPluginToolGUI(PluginName, PLUGIN_TAB_INDEX,
		reinterpret_cast<int *>(StabilizerForm->StabilizationControlPanel), NULL);
}

// -----------------DestroyStabilizerGUI------------------

void DestroyStabilizerToolGUI(void)

	// This destroys then entire GUI interface
	//
	// ***************************************************************************
{
	if (StabilizerForm == NULL)
		return; // Already destroyed

	// Reparent the controls back to us before destroying them
	if (GStabilizerTool != NULL)
		GStabilizerTool->getSystemAPI()->UnadoptPluginToolGUI(PLUGIN_TAB_INDEX);
	StabilizerForm->StabilizationControlPanel->Parent = StabilizerForm;

	delete StabilizerForm; // was StabilizerForm->Free(); but help says wrong!
	StabilizerForm = NULL;
}

// -------------------ShowStabilizerGUI-------------------

bool ShowStabilizerToolGUI(void)

	// This creates the GUI if not already exists and then shows it
	//
	// ****************************************************************************
{
	CreateStabilizerToolGUI(); // Create the gui if necessary

	if (StabilizerForm == NULL || GStabilizerTool == NULL)
		return false;

	StabilizerForm->StabilizationControlPanel->Visible = true;
	StabilizerForm->formShow();

	return true;
}

// -------------------HideStabilizerGUI-------------------

bool HideStabilizerToolGUI(void)

	// This removes the tool from the screen
	//
	// ****************************************************************************
{
	if (StabilizerForm == NULL)
		return false;

	StabilizerForm->StabilizationControlPanel->Visible = false;
	StabilizerForm->formHide();

	return false; // controls are NOT visible !!?!
}

// ------------------IsToolVisible------------------John Mertus----Aug 2001----

bool IsToolVisible(void)

	// This returns the visual state of the gui
	//
	// ****************************************************************************
{
	if (StabilizerForm == NULL)
		return (false); // Not created

	return StabilizerForm->StabilizationControlPanel->Visible;
}

// ****************************************************************************
// Called by the tool when execution status changes
// ****************************************************************************
void UpdateExecutionButtons(EToolControlState toolProcessingControlState, bool processingRenderFlag,
	StupidTrackingHackState stupidTrackingHackState)
{
	if (StabilizerForm == NULL)
		return;

	StabilizerForm->UpdateExecutionButtons(toolProcessingControlState, processingRenderFlag, stupidTrackingHackState);
}

// ****************************************************************************
// Called by the tool when tracking array changes
// ****************************************************************************
void UpdateTrackingPointButtons(void)
{
	if (StabilizerForm == NULL)
		return;

	StabilizerForm->UpdateTrackingPointButtons();
}

void UpdateTrackingCharts(void)
{
	if (StabilizerForm == NULL)
		return;

	StabilizerForm->UpdateTrackingCharts();
}

// ****************************************************************************
// Called by the tool for "set resume timecode" hot key.
// ****************************************************************************
void SetResumeTimecodeToCurrent()
{
	if (StabilizerForm == NULL)
		return;

	StabilizerForm->SetResumeTimecodeToCurrent();
}

void TStabilizerForm::SetResumeTimecodeToCurrent() {ExecSetResumeTimecodeToCurrent();}

// ****************************************************************************
// Called by the tool to get a snapshot of all the GUI settings
// ****************************************************************************
void GatherGUIParameters(CStabilizerParameters &stabilizerParameters) {
	StabilizerForm->GatherParameters(stabilizerParameters);}

// ****************************************************************************
// PDL parameter gather/scatter
// ****************************************************************************
void CaptureGUIParameters(CPDLElement &pdlToolParams) {StabilizerForm->CaptureParameters(pdlToolParams);}
// ---------------------------------------------------------------------------

void SetGUIParameters(CStabilizerParameters &stabilizerParameters)
{
	if (StabilizerForm == 0)
		return; // form not available

	StabilizerForm->SetParameters(stabilizerParameters);
}
// ---------------------------------------------------------------------------

void EnableProcessingRegionGUI(bool onOffFlag)
{
	if (StabilizerForm == 0)
		return; // form not available

	StabilizerForm->UseMattingCBox->Checked = onOffFlag;
}
// ---------------------------------------------------------------------------

bool IsProcessingRegionGUIEnabled()
{
	if (StabilizerForm == 0)
		return false; // form not available

	return StabilizerForm->UseMattingCBox->Checked;
}

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
// ----------------------- Stabilizer Form -----------------------------------
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------
__fastcall TStabilizerForm::TStabilizerForm(TComponent* Owner) : TMTIForm(Owner) {
	_TabStopDoNotDeletePanelVisible = false;}
// ---------------------------------------------------------------------------

void TStabilizerForm::formCreate(void)
{
	_MarkInMovePreference = MOVEPREF_PROMPT;
	_StabilizeMotionPreference = HV;
	MissingDataPrevFrameRadioButton->Checked = true;
	ShowPointsOldCheckedState = ShowPointCBox->Checked;
	ShowPointsOldControlState = STABILIZER_STATE_IDLE;

	auto iniFileName = CPMPIniFileName("Stabilizer");

	// PRESETS!
	auto presets = GStabilizerTool->GetPresets();
	SET_CBHOOK(CurrentPresetHasChanged, presets->SelectedIndexChanged);
	PresetsFrame->Initialize(presets, iniFileName);

	TrackingBoxSizeTrackEdit->Enable(true);
	TrackingBoxSizeTrackEdit->Edit->Font->Style = TFontStyles();
	TrackingBoxSizeTrackEdit->EditDisabledLabel->Font->Style = TFontStyles();

	MotionTrackEdit->Enable(true);
	MotionTrackEdit->Edit->Font->Style = TFontStyles();
	MotionTrackEdit->EditDisabledLabel->Font->Style = TFontStyles();

	SmoothTrackEdit->Enable(SmoothMotionRadioButton->Checked);
	SmoothTrackEdit->SetMinAndMax(0, 10);
	SmoothTrackEdit->Edit->Font->Style = TFontStyles();
	SmoothTrackEdit->EditDisabledLabel->Font->Style = TFontStyles();
	SmoothTrackEdit->SetDontNotifyIfMouseButtonIsStillDown(true);

	AutoModeSmoothTrackEdit->Enable(true);
	AutoModeSmoothTrackEdit->SetMinAndMax(0, 10);
	AutoModeSmoothTrackEdit->Edit->Font->Style = TFontStyles();
	AutoModeSmoothTrackEdit->EditDisabledLabel->Font->Style = TFontStyles();
	AutoModeSmoothTrackEdit->SetDontNotifyIfMouseButtonIsStillDown(true);

#ifndef NO_JITTER
	JitterTrackEdit->Enable(true);
	JitterTrackEdit->SetMinAndMax(0, 10);
	JitterTrackEdit->Edit->Font->Style = TFontStyles();
	JitterTrackEdit->EditDisabledLabel->Font->Style = TFontStyles();
#endif

   AdvancedTrackingSettingsPopup = new TAdvancedTrackingForm(StabilizationControlPanel);
   AdvancedTrackingSettingsPopup->TrackGrayButton->Down = true;
   AdvancedTrackingSettingsPopup->TrackDegrainCheckBox->Checked = false;
   AdvancedTrackingSettingsPopup->TrackUsingLUTCheckBox->Checked = false;
   AdvancedTrackingSettingsPopup->TrackAutoContrastCheckBox->Checked = false;

	if (GStabilizerTool == NULL)
	{
		return;
	}

	TrackingPointsChanged(NULL);

	GStabilizerTool->SetToolProgressMonitor(ExecStatusBar);

}
// ---------------------------------------------------------------------------

void TStabilizerForm::formShow(void)
{
	if (GStabilizerTool == NULL)
		return;

	// Do this first, affects other stuff
	ManualAutoTabControl->TabIndex = 0;  // "Manual"
	ManualAutoTabControlChange(nullptr);

	// Set the parameters
	GStabilizerTool->setDisplayRect(ShowMattingBoxCBox->Checked && UseMattingCBox->Checked);
	GStabilizerTool->setDisplayTrackingPoints(ShowPointCBox->Checked);
	GStabilizerTool->SetClearTrackingStuffOnNewShot(ClearOnShotChangeCBox->Checked);
	// ClipHasChanged(NULL);
	SET_CBHOOK(TrackingPointsChanged, GStabilizerTool->TrackingPointStateChange);
	SET_CBHOOK(ClipHasChanged, GStabilizerTool->ClipChange);
	SET_CBHOOK(ClipChanging, GStabilizerTool->ClipChanging);
	SET_CBHOOK(MarksHaveChanged, GStabilizerTool->MarksChange);

	_SubPixelCheckBoxWasClickedProgrammatically = false;
	UpdateTrackingPointButtons();
	UpdateTrackingCharts();
	UpdateExecutionButtonToolbar(TOOL_CONTROL_STATE_STOPPED, false, NOT_TRACKING);
	ConformUseSubPixelInterpolationCheckBox();
	TrackingBoxSizeChange(nullptr);
	SearchTrackBarChange(nullptr);

	UpdateTimer->Enabled = true;
	_TabStopDoNotDeletePanelVisible = true;
}
// ---------------------------------------------------------------------------

void TStabilizerForm::formHide(void)
{
	if (GStabilizerTool == NULL)
		return;

	// Turn off tracking points and windows
	GStabilizerTool->setDisplayRect(false);
	GStabilizerTool->setDisplayTrackingPoints(false);
	REMOVE_CBHOOK(TrackingPointsChanged, GStabilizerTool->TrackingPointStateChange);
	REMOVE_CBHOOK(ClipHasChanged, GStabilizerTool->ClipChange);
	REMOVE_CBHOOK(ClipChanging, GStabilizerTool->ClipChanging);
	REMOVE_CBHOOK(MarksHaveChanged, GStabilizerTool->MarksChange);

	// See if we want to autosave the tracking points
	SaveTrackingPointsIfNecessary();

	ColorPickButton->Down = false;
	ColorPickerStateTimer->Enabled = false;

	UpdateTimer->Enabled = false;
}
// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

class BoolString
{
public:
	bool State;
	String Name;
};

typedef vector<BoolString>CStateMemory;

void DisableAllControls(TControl *control, CStateMemory &StateMemory)
{
	TWinControl *twc;
	BoolString bs;

	// If already disabled, do  nothing
	if (!control->Enabled)
		return;

	// Disable the component
	bs.Name = control->Name;
	bs.State = control->Enabled;
	StateMemory.push_back(bs);
	control->Enabled = false;

	// Ignore some components
	if (control->InheritsFrom(__classid(TRadioGroup)))
		return;

	// Special case TEdits to set 'button face' color
	if (control->InheritsFrom(__classid(TEdit)))
	{
		TEdit *edit = dynamic_cast<TEdit*>(control);
		edit->Color = clBtnFace;
	}

	// Special case TTrackBar to hide 'thumb'
	if (control->InheritsFrom(__classid(TTrackBar)))
	{
		TTrackBar *trackBar = dynamic_cast<TTrackBar*>(control);
		trackBar->SliderVisible = false;
	}

	// Special case: TrackEdits need to have a method called
	TTrackEditFrame *trackEdit = dynamic_cast<TTrackEditFrame*>(control);
	if (trackEdit != NULL)
	{
		trackEdit->Enable(false);
		return; // Don't descend into the control - it's all set
	}

	// Window panels can have subcontrols
	if (!control->InheritsFrom(__classid(TWinControl)))
		return;
	twc = dynamic_cast<TWinControl*>(control);
	if (twc != NULL)
	{
		// Descend into the component
		for (int i = 0; i < twc->ControlCount; i++)
		{
			DisableAllControls(twc->Controls[i], StateMemory);
		}
	}
}

void EnableAllControls(TControl *control, CStateMemory &StateMemory)
{
	// Enable the controls
	for (unsigned j = 0; j < StateMemory.size(); j++)
		if (control->Name == StateMemory[j].Name)
		{
			control->Enabled = StateMemory[j].State;
			StateMemory.erase(StateMemory.begin() + j);
			break;
		}
	// Ignore some components
	if (control->InheritsFrom(__classid(TRadioGroup)))
		return;

	// Special case TEdits to restore 'window color'
	if (control->InheritsFrom(__classid(TEdit)))
	{
		TEdit *edit = dynamic_cast<TEdit*>(control);
		edit->Color = edit->Enabled ? clWindow : clBtnFace;
	}

	// Special case TTrackBar to restore 'thumb'
	if (control->InheritsFrom(__classid(TTrackBar)))
	{
		TTrackBar *trackBar = dynamic_cast<TTrackBar*>(control);
		trackBar->SliderVisible = trackBar->Enabled;
	}

	// Special case: TrackEdits need to have a method called
	TTrackEditFrame *trackEdit = dynamic_cast<TTrackEditFrame*>(control);
	if (trackEdit != NULL)
	{
		trackEdit->Enable(true);
		return; // Don't descend into the control - it's all set
	}

	// Descend into the component
	if (!control->InheritsFrom(__classid(TWinControl)))
		return;
	TWinControl *twc = dynamic_cast<TWinControl*>(control);
	for (int i = 0; i < twc->ControlCount; i++)
	{
		// mbraca changed this because it used to blindly enable the
		// child components, but you don't want to do that if this parent
		// component is disabled!!
		if (twc->Enabled)
			EnableAllControls(twc->Controls[i], StateMemory);
		else
			DisableAllControls(twc->Controls[i], StateMemory);
	}
}
// ---------------------------------------------------------------------------

void TStabilizerForm::UpdateExecutionButtons(EToolControlState toolProcessingControlState, bool processingRenderFlag,
	StupidTrackingHackState stupidTrackingHackState)
{
	UpdateExecutionButtonToolbar(toolProcessingControlState, processingRenderFlag, stupidTrackingHackState);
	UpdateStatusBarInfo();

} // UpdateExecutionButtons()

// ---------------------------------------------------------------------------

void TStabilizerForm::UpdateTrackingPointButtons(void)
{
	if (GStabilizerTool == NULL)
		return;

	int validCount = GStabilizerTool->GetNumberOfTrackingBoxes();
	if (validCount > 0 || GStabilizerTool->isInAutoTrack())
	{
		TrackButton->Enabled = true;
		PrevPointButton->Enabled = ((validCount < 2) ? false : true);
		NextPointButton->Enabled = PrevPointButton->Enabled;
		SavePointsButton->Enabled = !GStabilizerTool->isInAutoTrack();
	}
	else
	{
		TrackButton->Enabled = false;
		PrevPointButton->Enabled = false;
		NextPointButton->Enabled = false;
		SavePointsButton->Enabled = false;
	}

	DeletePointButton->Enabled = LastSelectedTag() != INVALID_TRACKING_BOX_TAG;
}
// ---------------------------------------------------------------------------

void TStabilizerForm::UpdateTrackingCharts(void)
{
	if (GStabilizerTool == NULL)
		return;

	if (!GStabilizerTool->_smoothingLock.TryToLock())
	{
		GStabilizerTool->doDeferredTrackingPointsGuiUpdate();
		return;
	}

	int nTag = LastSelectedTag();

	UpdateStatusBarInfo();

	// Now update the individual smoothing
	TrackingBoxSet &TA = GStabilizerTool->GetTrackingArrayRef();
	TA.checkConsistency();

	if (TA.getNumberOfTrackingBoxes() < 1)
	{
		TrackingDataAvailabilityPanel->Caption = "";
		TrackingDataAvailabilityPanel->Visible = true;
		ChartNumbersCoverUpPanel->Visible = true;
		ClearCharts();
		GStabilizerTool->_smoothingLock.UnLock();
		return;
	}

	if (!GStabilizerTool->isAllTrackDataValid())
	{
		bool noCaption = GStabilizerTool->wasTrackingDataInvalidatedByRendering()
								|| GStabilizerTool->isInAutoTrack();
		TrackingDataAvailabilityPanel->Caption = noCaption ? "" : "Selected point has not been tracked";
		TrackingDataAvailabilityPanel->Visible = true;
		ChartNumbersCoverUpPanel->Visible = true;
		ClearCharts();
		GStabilizerTool->_smoothingLock.UnLock();
		return;
	}

	if (!TA.isValidTrackingBoxTag(nTag))
	{
		TrackingDataAvailabilityPanel->Caption = "Tracking data is available - please select a point";
		TrackingDataAvailabilityPanel->Visible = true;
		ChartNumbersCoverUpPanel->Visible = true;
		GStabilizerTool->_smoothingLock.UnLock();
		ClearCharts();
		GStabilizerTool->_smoothingLock.UnLock();
		return;
	}

	DrawChartsTag(nTag);
	TrackingDataAvailabilityPanel->Visible = false;
	ChartNumbersCoverUpPanel->Visible = false;

	// Enable it if it has been disabled
	// Huh? WTF???
	// if (nTag > 0 && ShowPointsOldControlState != STABILIZER_STATE_RENDERING)
	// ShowPointCBox->Checked = true;

	GStabilizerTool->_smoothingLock.UnLock();
}
// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
// ---------------------- CALLBACKS ------------------------------------------
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

// ------------------ClipChanging----------------John Mertus----Nov 2006-----

void TStabilizerForm::ClipChanging(void *Sender)

	// This is called when the clip is about to change.  Note the clip change
	// may fail so do nothing that will leave the clip half open
	//
	// ****************************************************************************
{SaveTrackingPointsIfNecessary();}

// ------------------ClipHasChanged----------------John Mertus----Nov 2002-----

void TStabilizerForm::ClipHasChanged(void *Sender)

	// This is called when the clip is loaded into the main window
	//
	// ****************************************************************************
{
	if (GStabilizerTool == NULL)
		return;

	// Per Larry - double range of tracking box and search sliders for 4K

	int desiredTrackingBoxSize = TrackingBoxSizeTrackEdit->GetValue();
	int desiredSearchSize = MotionTrackEdit->GetValue();
	int sizeMin, sizeMax, motionMin, motionMax;
	TrackingBoxSizeTrackEdit->GetMinAndMax(sizeMin, sizeMax);
	MotionTrackEdit->GetMinAndMax(motionMin, motionMax);

	const CImageFormat *imageFormat = GStabilizerTool->getSystemAPI()->getVideoClipImageFormat();
	if (imageFormat == NULL || imageFormat->getPixelsPerLine() <= 2048)
	{
		if (sizeMax == TRACKING_SLIDER_MAX_4K)
		{
			desiredTrackingBoxSize /= (TRACKING_SLIDER_MAX_4K / TRACKING_SLIDER_MAX_2K);
		}
		TrackingBoxSizeTrackEdit->SetMinAndMax(TRACKING_SLIDER_MIN_2K, TRACKING_SLIDER_MAX_2K);

		if (motionMax == SEARCH_SLIDER_MAX_4K)
		{
			desiredSearchSize /= (SEARCH_SLIDER_MAX_4K / SEARCH_SLIDER_MAX_2K);
		}

		MotionTrackEdit->SetMinAndMax(SEARCH_SLIDER_MIN_2K, SEARCH_SLIDER_MAX_2K);
	}
	else // 4K Clip
	{
		if (sizeMax == TRACKING_SLIDER_MAX_2K)
		{
			desiredTrackingBoxSize *= (TRACKING_SLIDER_MAX_4K / TRACKING_SLIDER_MAX_2K);
		}

		TrackingBoxSizeTrackEdit->SetMinAndMax(TRACKING_SLIDER_MIN_4K, TRACKING_SLIDER_MAX_4K);

		if (motionMax == SEARCH_SLIDER_MAX_2K)
		{
			desiredSearchSize *= (SEARCH_SLIDER_MAX_4K / SEARCH_SLIDER_MAX_2K);
		}

		MotionTrackEdit->SetMinAndMax(SEARCH_SLIDER_MIN_4K, SEARCH_SLIDER_MAX_4K);
	}
	TrackingBoxSizeTrackEdit->SetValue(desiredTrackingBoxSize);
	MotionTrackEdit->SetValue(desiredSearchSize);

	// Apparently setting the values doesn't automatically trigger these
	TrackingBoxSizeChange(NULL);
	SearchTrackBarChange(NULL);

	// End of range-doubling hack

	string sName = GStabilizerTool->GetSuggestedTrackingDataSaveFilename();
	OpenTrackingPointsDialog->InitialDir = ExtractFileDir(sName.c_str());
	SaveTrackingPointsDialog->InitialDir = ExtractFileDir(sName.c_str());

	if (AutoLoadCBox->Checked && !GStabilizerTool->getSystemAPI()->IsPDLEntryLoading())
	{
		if (DoesFileExist(sName))
			GStabilizerTool->LoadTrackingPoints(sName.c_str());
	}

	UpdateStatusBarInfo();
	ExecSetResumeTimecodeToDefault();
	SetRefFrameTCToDefault();

	UpdateTrackingPointButtons();
	UpdateTrackingCharts();
	UpdateExecutionButtons(TOOL_CONTROL_STATE_STOPPED, false, NOT_TRACKING);
}
// ---------------------------------------------------------------------------

void TStabilizerForm::MarksHaveChanged(void *Sender)
{
	if (GStabilizerTool == NULL)
		return;

	EToolControlState toolControlState = GStabilizerTool->GetToolProcessingControlState();
	if (toolControlState == TOOL_CONTROL_STATE_STOPPED)
	{
		UpdateTrackingPointButtons();
		UpdateTrackingCharts();
		UpdateExecutionButtonToolbar(toolControlState, false, GStabilizerTool->GetStupidTrackingHackState());
		setTrackPointsVisibility(true); // per Larry 2010-06
	}
}
// ----------------------------------------------------------------------------

void TStabilizerForm::TrackingPointsChanged(void *Sender)
{
	UpdateTrackingCharts();

	if (GStabilizerTool == NULL)
	{
		return;
	}

	EToolControlState toolControlState = GStabilizerTool->GetToolProcessingControlState();
	if (toolControlState == TOOL_CONTROL_STATE_STOPPED)
	{
		UpdateTrackingPointButtons();
		UpdateExecutionButtonToolbar(toolControlState, false, GStabilizerTool->GetStupidTrackingHackState());
	}
}
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

void TStabilizerForm::CaptureParameters(CPDLElement &pdlEntryToolParams)
{
	// Get the Flicker parameters in the GUI to include in a PDL Entry
	CStabilizerParameters toolParams;

	// Get the settings from the GUI
	GatherParameters(toolParams);

	// Add the settings to the PDL Entry
	toolParams.WritePDLEntry(pdlEntryToolParams);
}
// ----------------------------------------------------------------------------

void TStabilizerForm::SaveTrackingPointsIfNecessary(void)
{
	if (GStabilizerTool != NULL)
		return;

	string sAutoName = GStabilizerTool->GetSuggestedTrackingDataSaveFilename();
	TrackingBoxSet &TA = GStabilizerTool->GetTrackingArrayRef();

	if (AutoSaveCBox->Checked && (TA.getNumberOfTrackingBoxes() > 0))
	{
		GStabilizerTool->SaveTrackingPoints(sAutoName.c_str());
	}
#ifdef USER_CONTROLS_AUTO_SAVE_AND_LOAD
	else if (DoesFileExist(sAutoName))
	{
		if (!DeleteFile(sAutoName.c_str()))
			TRACE_0(errout << "ERROR: Stabilizer: Could not remove " << sAutoName);
	}
#endif
}
// ---------------------------------------------------------------------------

// CAREFUL! This only sets MANUAL MODE parameters
static CStateMemory SmoothingStateMemory;

void TStabilizerForm::SetParameters(CStabilizerParameters &stabilizerParameters)
{
	// Tracking method
   int newTabIndex = stabilizerParameters.AutoTrackingMode ? 1 : 0;
   if (newTabIndex != ManualAutoTabControl->TabIndex)
   {
      ManualAutoTabControl->TabIndex = newTabIndex;
      ManualAutoTabControlChange(nullptr);
   }

	if (GStabilizerTool->isInAutoTrack())
	{
		setAutoModeParameters(stabilizerParameters);
	}
	else
	{
		setManualModeParameters(stabilizerParameters);
	}
}
// ---------------------------------------------------------------------------

void TStabilizerForm::setCommonParameters(CStabilizerParameters &stabilizerParameters)
{
	// Missing Data Fill
	switch (stabilizerParameters.MissingDataFill)
	{
	case MISSING_DATA_WHITE:
		MissingDataWhiteRadioButton->Checked = true;
		break;
	case MISSING_DATA_BLACK:
		MissingDataBlackRadioButton->Checked = true;
		break;
	case MISSING_DATA_PREVIOUS:
		MissingDataPrevFrameRadioButton->Checked = true;
		break;
	case MISSING_DATA_ZOOM:
		MissingDataZoomRadioButton->Checked = true;
		break;
	default:
		MTIassert(false);
		break;
	}

	// Use a mat box or not
	UseMattingCBox->Checked = stabilizerParameters.UseMatting;
}
// ---------------------------------------------------------------------------

void TStabilizerForm::setAutoModeParameters(CStabilizerParameters &stabilizerParameters)
{
	setCommonParameters(stabilizerParameters);

	// Smoothing
	int smoothMin, smoothMax;
	TTrackEditFrame *trackEdit = AutoModeSmoothTrackEdit;
	SmoothTrackEdit->GetMinAndMax(smoothMin, smoothMax);

	if (stabilizerParameters.Alpha2 == 0)
	{
		trackEdit->SetValue(smoothMax);
	}
	else if (stabilizerParameters.Alpha2 < 0)
	{
		trackEdit->SetValue(smoothMin);
	}
	else
	{
		trackEdit->SetValue((log(stabilizerParameters.Alpha2) / log(10.0) / (-0.75) + 1.5));
	}

	AutoModeUseSubPixelInterpolationCheckBox->Enabled = true;
	_SubPixelCheckBoxWasClickedProgrammatically = true;
	AutoModeUseSubPixelInterpolationCheckBox->Checked = !stabilizerParameters.DisableSubPixelInterpolation;
	_SubPixelCheckBoxWasClickedProgrammatically = false;
	// No conforming needed!

	AutoSourceIsAnimationCheckBox->Enabled = true;
	AutoSourceIsAnimationCheckBox->Checked = stabilizerParameters.AutoSourceIsAnimation;
}
// ---------------------------------------------------------------------------

void TStabilizerForm::setManualModeParameters(CStabilizerParameters &stabilizerParameters)
{
	setCommonParameters(stabilizerParameters);

	// Frame begin and end
	bool anchorAtIn = stabilizerParameters.GoodFramesIn > 0;
	bool anchorAtOut = stabilizerParameters.GoodFramesOut > 0;

	// Smoothing
	int smoothMin, smoothMax;
	TTrackEditFrame *trackEdit = SmoothTrackEdit;
	SmoothTrackEdit->GetMinAndMax(smoothMin, smoothMax);

	if (stabilizerParameters.Alpha2 == 0)
	{
		trackEdit->SetValue(smoothMax);
	}
	else if (stabilizerParameters.Alpha2 < 0)
	{
		trackEdit->SetValue(smoothMin);
	}
	else
	{
		trackEdit->SetValue((log(stabilizerParameters.Alpha2) / log(10.0) / (-0.75) + 1.5));
	}

	// Size of the tracking block
	TrackingBoxSizeTrackEdit->SetValue(stabilizerParameters.TrackBoxExtentX);
	MotionTrackEdit->SetValue(stabilizerParameters.SearchBoxExtentLeft);

	// Stabilization modes
	EStabilizationMode howToStabilize = STABMODE_FLAT;
	if (stabilizerParameters.RMode != STABMODE_NONE)
	{
		HorizVertAndRotationRadioButton->Checked = true;
		howToStabilize = stabilizerParameters.RMode;
		stabilizerParameters.HMode = stabilizerParameters.RMode;
		stabilizerParameters.VMode = stabilizerParameters.RMode;
	}
	else if (stabilizerParameters.HMode != STABMODE_NONE)
	{
		howToStabilize = stabilizerParameters.HMode;
		if (stabilizerParameters.VMode != STABMODE_NONE)
		{
			HorizAndVertRadioButton->Checked = true;
			stabilizerParameters.VMode = stabilizerParameters.HMode;
		}
		else
		{
			HorizontalRadioButton->Checked = true;
		}
	}
	else if (stabilizerParameters.VMode != STABMODE_NONE)
	{
		VerticalRadioButton->Checked = true;
		howToStabilize = stabilizerParameters.VMode;
	}
	else
	{
		HorizAndVertRadioButton->Checked = true;
	}

	switch (howToStabilize)
	{
	case STABMODE_SMOOTHED:
		SmoothMotionRadioButton->Checked = true;
		GoToRefframeButton->Enabled = false;
		RefFrameTimeCodeEdit->Enabled = false;
		ResetRefFrameButton->Enabled = false;
		AnchorFirstRadioButton->Enabled = true;
		AnchorLastRadioButton->Enabled = true;
		AnchorBothRadioButton->Enabled = true;
		AnchorNoneRadioButton->Enabled = true;
		////EnableAllControls(SmoothGroupBox, SmoothingStateMemory);
		SmoothTrackEdit->Enable(true);
		break;
	case STABMODE_FLAT:
		NoMotionRadioButton->Checked = true;
		GoToRefframeButton->Enabled = false;
		RefFrameTimeCodeEdit->Enabled = false;
		ResetRefFrameButton->Enabled = false;
		if (anchorAtIn && anchorAtOut)
		{
			anchorAtOut = false;
		}

		AnchorFirstRadioButton->Enabled = true;
		AnchorLastRadioButton->Enabled = true;
		AnchorBothRadioButton->Enabled = false;
		AnchorNoneRadioButton->Enabled = true;
		////DisableAllControls(SmoothGroupBox, SmoothingStateMemory);
		SmoothTrackEdit->Enable(false);
		break;
	case STABMODE_REGISTERED:
		MatchRefRadioButton->Checked = true;
		GoToRefframeButton->Enabled = true;
		RefFrameTimeCodeEdit->Enabled = true;
		ResetRefFrameButton->Enabled = true;
		////DisableAllControls(SmoothGroupBox, SmoothingStateMemory);
		SmoothTrackEdit->Enable(false);
		break;
	default:
		MTIassert(false);
		break;
	}

	_GoodInCount = anchorAtIn ? 1 : 0;
	_GoodOutCount = anchorAtOut ? 1 : 0;

	if (_GoodInCount > 0 && _GoodOutCount == 0)
	{
		AnchorFirstRadioButton->Checked = true;
	}
	else if (_GoodInCount == 0 && _GoodOutCount > 0)
	{
		AnchorLastRadioButton->Checked = true;
	}
	else if (_GoodInCount > 0 && _GoodOutCount > 0)
	{
		AnchorBothRadioButton->Checked = true;
	}
	else
	{
		AnchorNoneRadioButton->Checked = true;
	}

#ifndef NO_JITTER
		iJitter = 0;
#else
	// Jitter
	int iJitter = (int)(stabilizerParameters.Jitter + 0.5);
	if (iJitter < 0)
	{
		iJitter = 0;
	}

	int jitterMin, jitterMax;
	JitterTrackEdit->GetMinAndMax(jitterMin, jitterMax);
	if (iJitter > jitterMax)
	{
		iJitter = jitterMax;
	}
#endif

	JitterTrackEdit->SetValue(iJitter);

	UseSubPixelInterpolationCheckBox->Enabled = true;
	_SubPixelCheckBoxWasClickedProgrammatically = true;
	UseSubPixelInterpolationCheckBox->Checked = !stabilizerParameters.DisableSubPixelInterpolation;
	_SubPixelCheckBoxWasClickedProgrammatically = false;
	ConformUseSubPixelInterpolationCheckBox();

	AutoSourceIsAnimationCheckBox->Enabled = false;
}
// ----------------------------------------------------------------------------

void TStabilizerForm::GatherParameters(CStabilizerParameters &stabilizerParameters)
{
	// Tracking method
	stabilizerParameters.AutoTrackingMode = ManualAutoTabControl->TabIndex == 1;


	// Get mode-specific parameters
	if (stabilizerParameters.AutoTrackingMode)
	{
		 gatherAutoModeParameters(stabilizerParameters);
	}
	else
	{
		 gatherManualModeParameters(stabilizerParameters);
	}
}
// ----------------------------------------------------------------------------

void TStabilizerForm::gatherCommonParameters(CStabilizerParameters &stabilizerParameters)
{
	// Tracking method
	stabilizerParameters.AutoTrackingMode = ManualAutoTabControl->TabIndex == 1;

	// Warp Function
	stabilizerParameters.UFunction = U_FUNCTION_TYPE_LINEAR;

	// Missing Data Fill
	if (MissingDataWhiteRadioButton->Checked)
	{
		stabilizerParameters.MissingDataFill = MISSING_DATA_WHITE;
	}
	else if (MissingDataBlackRadioButton->Checked)
	{
		stabilizerParameters.MissingDataFill = MISSING_DATA_BLACK;
	}
	else if (MissingDataZoomRadioButton->Checked)
	{
		stabilizerParameters.MissingDataFill = MISSING_DATA_ZOOM;
	}
	else
	{
		stabilizerParameters.MissingDataFill = MISSING_DATA_PREVIOUS;
	}

	// Jitter amount to add
#ifdef NO_JITTER
	stabilizerParameters.Jitter = 0;
#else
	JitterTrackEdit->GetValue();
#endif

	// Use a mat box or not
	stabilizerParameters.UseMatting = UseMattingCBox->Checked;
	stabilizerParameters.KeepOriginalMatValues = true; // Removed: KeepOriginalValuesCBox->Checked;
}
// ----------------------------------------------------------------------------

void TStabilizerForm::gatherAutoModeParameters(CStabilizerParameters &stabilizerParameters)
{
	// Get parameters that are common between the two modes
	gatherCommonParameters(stabilizerParameters);

	// Source is animation?
	stabilizerParameters.AutoSourceIsAnimation = AutoSourceIsAnimationCheckBox->Checked;

	// Global Smoothing
	// Auto mode now always flat line ("remove all"), not smoothed.
	stabilizerParameters.Alpha2 = 0;

	stabilizerParameters.DisableSubPixelInterpolation = !AutoModeUseSubPixelInterpolationCheckBox->Checked;

	// No anchors - we just use the auto-tracking values as-is.
	stabilizerParameters.GoodFramesIn = 0;
	stabilizerParameters.GoodFramesOut = 0;

	// Tracking box size
	stabilizerParameters.TrackBoxExtentX = -1;
	stabilizerParameters.TrackBoxExtentY = -1;

	// Search box size
	stabilizerParameters.SearchBoxExtentLeft = -1;
	stabilizerParameters.SearchBoxExtentRight = -1;
	stabilizerParameters.SearchBoxExtentUp = -1;
	stabilizerParameters.SearchBoxExtentDown = -1;

	// Stabilization mode H + V = flat line
	stabilizerParameters.HMode = STABMODE_FLAT;
	stabilizerParameters.VMode = STABMODE_FLAT;
	stabilizerParameters.RMode = STABMODE_NONE;

	// Reference Frame
	stabilizerParameters.RefFrameIndex = -1;

	CStabilizerParameters dbStabilizerParameters = stabilizerParameters;
}
// ----------------------------------------------------------------------------

void TStabilizerForm::gatherManualModeParameters(CStabilizerParameters &stabilizerParameters)
{
	// Get parameters that are common between the two modes
	gatherCommonParameters(stabilizerParameters);

	// Global Smoothing
	int SmoothTrackEditValue = SmoothTrackEdit->GetValue();
	int smoothMin, smoothMax;
	SmoothTrackEdit->GetMinAndMax(smoothMin, smoothMax);
	if (SmoothTrackEditValue == smoothMin)
	{
		// No smoothing
		stabilizerParameters.Alpha2 = -1;
	}
	else if (SmoothTrackEditValue == smoothMax)
	{
		// Constant smoothing
		stabilizerParameters.Alpha2 = 0;
	}
	else
	{
		// Do the exponent
		stabilizerParameters.Alpha2 = pow(10.0, -0.75 * (SmoothTrackEdit->GetValue() - 1));
	}

	stabilizerParameters.DisableSubPixelInterpolation = !UseSubPixelInterpolationCheckBox->Checked;

	// Tracking box size
	stabilizerParameters.TrackBoxExtentX = TrackingBoxSizeTrackEdit->GetValue();
	stabilizerParameters.TrackBoxExtentY = TrackingBoxSizeTrackEdit->GetValue();

	// Search box size
	stabilizerParameters.SearchBoxExtentLeft = MotionTrackEdit->GetValue();
	stabilizerParameters.SearchBoxExtentRight = MotionTrackEdit->GetValue();
	stabilizerParameters.SearchBoxExtentUp = MotionTrackEdit->GetValue();
	stabilizerParameters.SearchBoxExtentDown = MotionTrackEdit->GetValue();

	// Jitter amount to add
#ifdef NO_JITTER
	stabilizerParameters.Jitter = 0;
#else
	JitterTrackEdit->GetValue();
#endif

	// Use a mat box or not
	stabilizerParameters.UseMatting = UseMattingCBox->Checked;
	stabilizerParameters.KeepOriginalMatValues = true; // Removed: KeepOriginalValuesCBox->Checked;

	// Stabilization modes
	EStabilizationMode howToStabilize =
			(SmoothMotionRadioButton->Checked)
			? STABMODE_SMOOTHED
			: (NoMotionRadioButton->Checked
				? STABMODE_FLAT
				: (MatchRefRadioButton->Checked
					? STABMODE_REGISTERED
					: STABMODE_NONE));

	// Frame begin and end
	stabilizerParameters.GoodFramesIn = (howToStabilize == STABMODE_REGISTERED) ? 0 : _GoodInCount;
	stabilizerParameters.GoodFramesOut = (howToStabilize == STABMODE_REGISTERED) ? 0 : _GoodOutCount;

	stabilizerParameters.HMode = STABMODE_NONE;
	stabilizerParameters.VMode = STABMODE_NONE;
	stabilizerParameters.RMode = STABMODE_NONE;
	if (HorizontalRadioButton->Checked)
	{
		stabilizerParameters.HMode = howToStabilize;
	}
	else if (VerticalRadioButton->Checked)
	{
		stabilizerParameters.VMode = howToStabilize;
	}
	else if (HorizAndVertRadioButton->Checked)
	{
		stabilizerParameters.HMode = stabilizerParameters.VMode = howToStabilize;
	}
	else
	{
		stabilizerParameters.HMode = stabilizerParameters.VMode = stabilizerParameters.RMode = howToStabilize;
	}

	// Reference Frame
	stabilizerParameters.RefFrameIndex = GStabilizerTool->getSystemAPI()->getMarkIn();
	CVideoFrameList *frameList;
	frameList = GStabilizerTool->getSystemAPI()->getVideoFrameList();
	if (frameList != NULL)
	{
		int index = frameList->getFrameIndex(RefFrameTimeCodeEdit->tcP);
		if (index >= GStabilizerTool->getSystemAPI()->getMarkIn() && index <
			GStabilizerTool->getSystemAPI()->getMarkOut())
		{
			stabilizerParameters.RefFrameIndex = index;
		}
	}

   // Advanced Tracking
   if (AdvancedTrackingSettingsPopup != nullptr)
   {
      ETrackingChannel trackingChannel = AdvancedTrackingSettingsPopup->TrackRedButton->Down
                                          ? ETrackingChannel::R
                                          : (AdvancedTrackingSettingsPopup->TrackGreenButton->Down
                                             ? ETrackingChannel::G
                                             : (AdvancedTrackingSettingsPopup->TrackBlueButton->Down
                                                ? ETrackingChannel::B
                                                : ETrackingChannel::Y));

      GStabilizerTool->SetAdvancedTrackingParameters(
                           trackingChannel,
                           AdvancedTrackingSettingsPopup->TrackDegrainCheckBox->Checked,
                           AdvancedTrackingSettingsPopup->TrackUsingLUTCheckBox->Checked,
                           AdvancedTrackingSettingsPopup->TrackAutoContrastCheckBox->Checked);
   }

	// Source is animation - for auto mode only!
	stabilizerParameters.AutoSourceIsAnimation = false;

	CStabilizerParameters dbStabilizerParameters = stabilizerParameters;
}

// ----------------ReadSettings---------------------John Mertus-----Apr 2005---

bool TStabilizerForm::ReadSettings(CIniFile *ini, const string &IniSection)

	// This reads the Size and Positions properties from the ini file
	// ini is the ini file
	// IniSection is the IniSection
	//
	// Return should be true
	//
	// Note: This function is designed to be overridden in the derived class
	//
	// ******************************************************************************
{
	CStabilizerParameters stabilizerParameters;
	stabilizerParameters.ReadParameters(ini, "Stabilizer");

	// HACK! Per Larry - ALWAYS START IN MissingFill = Previous MODE!
	stabilizerParameters.MissingDataFill = MISSING_DATA_PREVIOUS;

	///// Larry says to NOT remember the "UseMatting" setting!!
	stabilizerParameters.UseMatting = false;
	/////

	setManualModeParameters(stabilizerParameters);

	// Figure out the stabilization motion preference
	bool preferR = stabilizerParameters.RMode != STABMODE_NONE;
	bool preferH = stabilizerParameters.HMode != STABMODE_NONE;
	bool preferV = stabilizerParameters.VMode != STABMODE_NONE;
	_StabilizeMotionPreference = (preferR || (!preferH && !preferV)) ? HVR : (preferH && preferV) ? HV :
		preferH ? H : V;

	// Local parameters
	ShowPointCBox->Checked = ini->ReadBool(IniSection, "ShowTrackPoints", ShowPointCBox->Checked);
	ShowRenderPointCBox->Checked = ini->ReadBool(IniSection, "ShowWhileRender", ShowRenderPointCBox->Checked);
	ClearOnShotChangeCBox->Checked = ini->ReadBool(IniSection, "ClearOnShotChange", ClearOnShotChangeCBox->Checked);
#ifdef USER_CONTROLS_AUTO_SAVE_AND_LOAD
	AutoSaveCBox->Checked = ini->ReadBool(IniSection, "AutoSaveTrackPoints", AutoSaveCBox->Checked);
	AutoLoadCBox->Checked = ini->ReadBool(IniSection, "AutoLoadTrackPoints", AutoLoadCBox->Checked);
#else
	AutoSaveCBox->Checked = true;
	AutoLoadCBox->Checked = false;
#endif
	// WarnMarkOutMoveCheckBox->Checked = ini->ReadBool(IniSection, "MarkOutWarnings", WarnMarkOutMoveCheckBox->Checked);
	_MarkInMovePreference = (EMarkInMovePreference) ini->ReadInteger(IniSection, "MarkInMovePreference",
		(int) _MarkInMovePreference);
	_UseSubPixelInterpolationPreference = ini->ReadBool(IniSection, "SubPixelPreference",
		_UseSubPixelInterpolationPreference);

	TrailSizeComboBox->ItemIndex = ini->ReadInteger(IniSection, "TrailSize", TrailSizeComboBox->ItemIndex);
	TrailSizeComboBoxChange(NULL);

	string sName = GStabilizerTool->GetSuggestedTrackingDataSaveFilename();
	OpenTrackingPointsDialog->InitialDir = ExtractFileDir(sName.c_str());
	SaveTrackingPointsDialog->InitialDir = ExtractFileDir(sName.c_str());

	// Just to be safe
	if ((AutoLoadCBox->Checked) && (GStabilizerTool != NULL) && !GStabilizerTool->getSystemAPI()->IsPDLEntryLoading())
	{
		if (DoesFileExist(sName))
			GStabilizerTool->LoadTrackingPoints(sName.c_str());
	}

	// AFTER all other manual mode settings!
	UseSubPixelInterpolationCheckBox->Enabled = true;
	_SubPixelCheckBoxWasClickedProgrammatically = true;
	UseSubPixelInterpolationCheckBox->Checked = _UseSubPixelInterpolationPreference;
	_SubPixelCheckBoxWasClickedProgrammatically = false;
	ConformUseSubPixelInterpolationCheckBox();

	UpdatePresetCurrentParameters();

	// Auto mode parameters
	// Careful - "Stabilizer", NOT IniSection, which is "StabilizerForm"!!!!
	stabilizerParameters.Alpha2 = ini->ReadDouble("Stabilizer", "AutoModeAlpha2", stabilizerParameters.Alpha2);
	stabilizerParameters.DisableSubPixelInterpolation = ini->ReadBool("Stabilizer", "AutoModeDisableSubPixelInterpolation", stabilizerParameters.DisableSubPixelInterpolation);
	setAutoModeParameters(stabilizerParameters);

	AutoModeUseSubPixelInterpolationCheckBox->Enabled = true;
	_SubPixelCheckBoxWasClickedProgrammatically = true;
	AutoModeUseSubPixelInterpolationCheckBox->Checked = _UseSubPixelInterpolationPreference;
	_SubPixelCheckBoxWasClickedProgrammatically = false;
	// No conforming needed

	AutoSourceIsAnimationCheckBox->Checked =  ini->ReadBool("Stabilizer", "AutoModeSourceIsAnimation", stabilizerParameters.AutoSourceIsAnimation);

	return true;
}

// ----------------WriteSettings----------------John Mertus-----Apr 2005---

bool TStabilizerForm::WriteSettings(CIniFile *ini, const string &IniSection)

	// This writes the Size and Positions properties from the ini file
	// ini is the ini file
	// IniSection is the IniSection
	//
	// Return should be true
	//
	// Note: This function is designed to be overridden in the derived class
	//
	// ******************************************************************************
{
	CStabilizerParameters stabilizerParameters;
	gatherManualModeParameters(stabilizerParameters);

	///// Larry says to NOT remember the "UseMatting" setting!!
	stabilizerParameters.UseMatting = false;
	/////

	stabilizerParameters.WriteParameters(ini, "Stabilizer");

	// Local parameters
	ini->WriteBool(IniSection, "ShowTrackPoints", ShowPointCBox->Checked);
	ini->WriteBool(IniSection, "ShowWhileRender", ShowRenderPointCBox->Checked);
	ini->WriteBool(IniSection, "ClearOnShotChange", ClearOnShotChangeCBox->Checked);
#ifdef USER_CONTROLS_AUTO_SAVE_AND_LOAD
	ini->WriteBool(IniSection, "AutoSaveTrackPoints", AutoSaveCBox->Checked);
	ini->WriteBool(IniSection, "AutoLoadTrackPoints", AutoLoadCBox->Checked);
#else
	ini->WriteBool(IniSection, "AutoSaveTrackPoints", true);
	ini->WriteBool(IniSection, "AutoLoadTrackPoints", false);
#endif
	// ini->WriteBool(IniSection, "MarkOutWarnings", WarnMarkOutMoveCheckBox->Checked);
	ini->WriteInteger(IniSection, "MarkInMovePreference", (int) _MarkInMovePreference);
	ini->WriteBool(IniSection, "SubPixelPreference", _UseSubPixelInterpolationPreference);

	ini->WriteInteger(IniSection, "TrailSize", TrailSizeComboBox->ItemIndex);

	// Save the tracking points
	string sAutoName = GStabilizerTool->GetSuggestedTrackingDataSaveFilename();
	TrackingBoxSet &TA = GStabilizerTool->GetTrackingArrayRef();

	// See if we want to autosave them
	if ((AutoSaveCBox->Checked) && (GStabilizerTool != NULL))
		if (TA.getNumberOfTrackingBoxes() > 0)
		{
			GStabilizerTool->SaveTrackingPoints(sAutoName.c_str());
		}
#ifdef USER_CONTROLS_AUTO_SAVE_AND_LOAD
		else
		{
			if (DoesFileExist(sAutoName))
				// Larry says it's OK to get rid of this confusing dialog
				// if (_MTIConfirmationDialog(Handle, "No tracking points defined\nDo you wish to remove saved tracking points?") == MTI_DLG_OK)
				if (!DeleteFile(sAutoName.c_str()))
					_MTIErrorDialog(Handle, "Could not remove " + sAutoName);
		}
#endif

	// Auto mode settings
	gatherAutoModeParameters(stabilizerParameters);
	// Careful - "Stabilizer", NOT IniSection, which is "StabilizerForm"!!!!
	ini->WriteDouble("Stabilizer", "AutoModeAlpha2", stabilizerParameters.Alpha2);
	ini->WriteBool("Stabilizer", "AutoModeDisableSubPixelInterpolation", stabilizerParameters.DisableSubPixelInterpolation);
	ini->WriteBool("Stabilizer", "AutoModeSourceIsAnimation", stabilizerParameters.AutoSourceIsAnimation);
	// Everything else is constant.

	return true;
}
// ----------------------------------------------------------------------------

void __fastcall TStabilizerForm::SmoothTrackBarChange(TObject *Sender)
{
	InvalidateSmoothedData();

	if (!GStabilizerTool->isInAutoTrack())
	{
		UpdatePresetCurrentParameters();
   }
}
// ----------------------------------------------------------------------------

void __fastcall TStabilizerForm::JitterLevelTrackBarChange(TObject *Sender)
{
	InvalidateSmoothedData();
	UpdatePresetCurrentParameters();
}
// ----------------------------------------------------------------------------

void TStabilizerForm::InvalidateSmoothedData(void)
{
	if (GStabilizerTool == NULL)
		return;

	GStabilizerTool->MarkSmoothedDataInvalid();
	GStabilizerTool->ButtonCommandHandler(STABILIZER_PROC_SMOOTH);
}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::TrackingBoxSizeChange(TObject *Sender)
{
	if (GStabilizerTool == NULL)
		return;

	// TrackingLabel->Caption = TrackingTrackBar->Position;
	///TrackingEdit->Text = AnsiString(TrackingBoxSizeTrackEdit->GetValue());
	GStabilizerTool->SetTrackingBoxSizes(TrackingBoxSizeTrackEdit->GetValue(), TrackingBoxSizeTrackEdit->GetValue(),
		MotionTrackEdit->GetValue(), MotionTrackEdit->GetValue(), MotionTrackEdit->GetValue(),
		MotionTrackEdit->GetValue());
	UpdatePresetCurrentParameters();
}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::SearchTrackBarChange(TObject *Sender)
{
	if (GStabilizerTool == NULL)
		return;

	GStabilizerTool->SetTrackingBoxSizes(TrackingBoxSizeTrackEdit->GetValue(), TrackingBoxSizeTrackEdit->GetValue(),
		MotionTrackEdit->GetValue(), MotionTrackEdit->GetValue(), MotionTrackEdit->GetValue(),
		MotionTrackEdit->GetValue());
	UpdatePresetCurrentParameters();
}

// ---------------------------------------------------------------------------
void TStabilizerForm::UpdateStatusBarInfo(void)
{
	// Status readout
	EStabilizerControlState stabilizerControlState = GStabilizerTool->GetControlState();
	AnsiString statusString;
	switch (stabilizerControlState)
	{
	case STABILIZER_STATE_IDLE:
		statusString = "Idle";
		break;
	case STABILIZER_STATE_GOT_AN_ERROR:
		statusString = "Error";
		break;
	case STABILIZER_STATE_NEED_TO_RUN_TRACKER:
	case STABILIZER_STATE_TRACKING:
	case STABILIZER_STATE_TRACKING_COMPLETE:
		statusString = "Tracking";
		break;
	case STABILIZER_STATE_TRACKING_PAUSED:
		statusString = "Tracking PAUSED";
		break;
	case STABILIZER_STATE_NEED_TO_RUN_SMOOTHER:
	case STABILIZER_STATE_SMOOTHING:
	case STABILIZER_STATE_SMOOTHING_COMPLETE:
		statusString = "Smoothing";
		break;
	case STABILIZER_STATE_NEED_TO_RUN_RENDERER:
	case STABILIZER_STATE_RENDERING:
	case STABILIZER_STATE_RENDERING_COMPLETE:
		statusString = "Rendering";
		break;
	default:
		statusString = "Unknown";
		break;
	}
	StatusMessageLabel->Caption = statusString;
#if 0
	if (statusString == "Idle")
		ExecStatusBar->SetIdle();
	else
		ExecStatusBar->SetStatusMessage(statusString.c_str());
#endif

	// Readout of number of tracking points
	TrackingBoxSet &TA = GStabilizerTool->GetTrackingArrayRef();
	int pointCount = TA.getNumberOfTrackingBoxes();
	PointCountLabel->Caption = AnsiString(pointCount);
	PointsLabel->Caption = ((pointCount == 1) ? "point" : "points");

	ExecStatusBar->SetToolMessage(StringToStdString((PointCountLabel->Caption + " " + PointsLabel->Caption)).c_str());

	// The actual status bar
	string StatusMessage;
	string TimeMessage;

	GStabilizerTool->getStatusMessage(StatusMessage);
	GStabilizerTool->getTimeMessage(TimeMessage);
	UpdateToolStatusBar(STATUS_BAR_STATUS, StatusMessage);
	UpdateToolStatusBar(STATUS_BAR_MESSAGE, TimeMessage);
}
// ---------------------------------------------------------------------------

void TStabilizerForm::ClearCharts()
{
	HTrackGraph->Series[0]->Clear();
	HTrackGraph->Series[1]->Clear();
	VTrackGraph->Series[0]->Clear();
	VTrackGraph->Series[1]->Clear();
	RTrackGraph->Series[0]->Clear();
	RTrackGraph->Series[1]->Clear();
}
// ---------------------------------------------------------------------------

void TStabilizerForm::DrawChartsTag(int trackingBoxTag)
{
	TrackingBoxSet &TA = GStabilizerTool->GetTrackingArrayRef();
	TrackingBoxSet &RA = GStabilizerTool->GetRobustArrayRef(); // Always only one tracking box
	TrackingBoxSet &SA = GStabilizerTool->GetSmoothedArrayRef(); // Always two tracking boxes
	TA.checkConsistency();
	RA.checkConsistency();
   SA.checkConsistency();

	if (!TA.isValidTrackingBoxTag(trackingBoxTag))
	{
		throw std::runtime_error("Bad tracking box tag passed to DrawChartsTag()!");
	}

	// Find the time code
	CVideoFrameList *frameList = GStabilizerTool->getSystemAPI()->getVideoFrameList();
	if (frameList == NULL)
	{
		return;
	}

	PTimecode ptcBase = frameList->getInTimecode();
	PTimecode ptc(ptcBase);

	double dMin = 0.0;
	double dMax = 0.0;
	double dRef;

//TRACE_0(errout << "TA");
	const TrackingBox &rawBox = TA.getTrackingBoxByTag(trackingBoxTag);
//TRACE_0(errout << "RA");
	const TrackingBox *robustBox = (RA.getNumberOfTrackingBoxes() > 0) ? &RA.getTrackingBoxByIndex(0) : NULL;
//TRACE_0(errout << "SA");
	const TrackingBox *smoothedBox = (SA.getNumberOfTrackingBoxes() > 0) ? &SA.getTrackingBoxByIndex(0) : NULL;
//TRACE_0(errout << "XA");
	const TrackingBox *rotatedBox = (SA.getNumberOfTrackingBoxes() > 1) ? &SA.getTrackingBoxByIndex(1) : NULL;
//TRACE_0(errout << "OK");

	HTrackGraph->Series[0]->Clear();

	auto startFrame = TA.getInFrame();
	auto stopFrame = TA.getOutFrame();
	for (auto frame = startFrame; frame < stopFrame; ++frame)
	{
		ptc = ptcBase + frame;
		String TCS = StringTrimLeft(ptc.String()).c_str();

		// Not sure how this could happen - but no time to debug now QQQ
		if (!rawBox.isValidAtFrame(frame))
		{
			break;
		}

		double d = rawBox.getTrackPointAtFrame(frame).x;
		if (frame == startFrame)
		{
			dRef = d;
		}

		d -= dRef;
		HTrackGraph->Series[0]->AddXY(frame, d, TCS);

		dMax = max<double>(dMax, d);
		dMin = min<double>(dMin, d);
	}

	TRACE_3(errout << "TTTTTTTTTTT After H Series 0: " << ": dMin=" << dMin << ", dMax=" << dMax);

	HTrackGraph->Series[1]->Clear();

	if (robustBox != NULL && smoothedBox != NULL)
	{
		// Only display the smoothed points for the tracking points
		startFrame = max<int>(SA.getInFrame(), TA.getInFrame());
		stopFrame = min<int>(SA.getOutFrame(), TA.getOutFrame());

		for (auto frame = startFrame; frame < stopFrame; ++frame)
		{
			// Not sure how this could happen - but no time to debug now QQQ
			if (!smoothedBox->isValidAtFrame(frame))
			{
				break;
			}

			double d = smoothedBox->getTrackPointAtFrame(frame).x;
			if (frame == startFrame)
			{
				dRef = robustBox->getTrackPointAtFrame(frame).x;
			}

			d -= dRef;
			HTrackGraph->Series[1]->AddXY(frame, d);

			dMax = max<double>(dMax, d);
			dMin = min<double>(dMin, d);
		}

		TRACE_3(errout << "TTTTTTTTTTT After H Series 1: " << ": dMin=" << dMin << ", dMax=" << dMax);
	}
	// Scale axis to a multiple of "soft" units with a top and bottom "margin"
	auto iMinH = ((int)((dMin - 0.5 - CHART_MARGIN - CHART_SOFT_UNIT) / CHART_SOFT_UNIT)) * CHART_SOFT_UNIT;
	auto iMaxH = ((int)((dMax + 0.5 + CHART_MARGIN + CHART_SOFT_UNIT) / CHART_SOFT_UNIT)) * CHART_SOFT_UNIT;

	// Do the vertical graph
	dMin = 0.0;
	dMax = 0.0;

	VTrackGraph->Series[0]->Clear();
	startFrame = TA.getInFrame();
	stopFrame = TA.getOutFrame();
	for (int frame = startFrame; frame < stopFrame; ++frame)
	{
		ptc = ptcBase + frame;
		String TCS = StringTrimLeft(ptc.String()).c_str();

		// Not sure how this could happen - but no time to debug now QQQ
		if (!rawBox.isValidAtFrame(frame))
		{
			break;
		}

		double d = rawBox.getTrackPointAtFrame(frame).y;
		if (frame == startFrame)
		{
			dRef = d;
		}

		d -= dRef;
		VTrackGraph->Series[0]->AddXY(frame, d, TCS);

		dMax = max<double>(dMax, d);
		dMin = min<double>(dMin, d);
	}

	TRACE_3(errout << "TTTTTTTTTTT After V Series 0: " << ": dMin=" << dMin << ", dMax=" << dMax);

	VTrackGraph->Series[1]->Clear();

	if (robustBox != NULL && smoothedBox != NULL)
	{
		// Only display the smoothed points for the tracking points
		startFrame = max<int>(SA.getInFrame(), TA.getInFrame());
		stopFrame = min<int>(SA.getOutFrame(), TA.getOutFrame());
		for (auto frame = startFrame; frame < stopFrame; ++frame)
		{
			// Not sure how this could happen - but no time to debug now QQQ
			if (!smoothedBox->isValidAtFrame(frame))
			{
				break;
			}

			double d = smoothedBox->getTrackPointAtFrame(frame).y;
			if (frame == startFrame)
			{
				dRef = robustBox->getTrackPointAtFrame(frame).y;
			}

			d -= dRef;
			VTrackGraph->Series[1]->AddXY(frame, d);

			dMax = max<double>(dMax, d);
			dMin = min<double>(dMin, d);
		}

		TRACE_3(errout << "TTTTTTTTTTT After V Series 1: " << ": dMin=" << dMin << ", dMax=" << dMax);
	}

	// Scale axis to a multiple of "soft" units with a top and bottom "margin"
	int iMinV = ((int)((dMin - 0.5 - CHART_MARGIN - CHART_SOFT_UNIT) / CHART_SOFT_UNIT)) * CHART_SOFT_UNIT;
	int iMaxV = ((int)((dMax + 0.5 + CHART_MARGIN + CHART_SOFT_UNIT) / CHART_SOFT_UNIT)) * CHART_SOFT_UNIT;

	TRACE_3(errout << "TTTTTTTTTTT V Axis limits: " << ": iMinV=" << iMinV << ", iMaxV=" << iMaxV);

	// Stupid way of setting axis to avoid min being greater than max
	int iMin = min<int>(iMinH, iMinV);
	int iMax = max<int>(iMaxH, iMaxV);
	HTrackGraph->LeftAxis->Minimum = min<long>(iMin, HTrackGraph->LeftAxis->Maximum) - 1;
	HTrackGraph->LeftAxis->Maximum = iMax;
	HTrackGraph->LeftAxis->Minimum = iMin - 1; // -1 to show bottom axis label
	VTrackGraph->LeftAxis->Minimum = min<long>(iMin, VTrackGraph->LeftAxis->Maximum) - 1;
	VTrackGraph->LeftAxis->Maximum = iMax;
	VTrackGraph->LeftAxis->Minimum = iMin - 1; // -1 to show bottom axis label

	TRACE_3(errout << "TTTTTTTTTTT FINAL Axis limits: " << ": iMin=" << (iMin - 1) << ", iMax=" << iMax);

	// Do the rotation graph
	dMin = 0.0;
	dMax = 0.0;

	RTrackGraph->Series[0]->Clear();
	RTrackGraph->Series[1]->Clear();

	if (rotatedBox != NULL && smoothedBox != NULL)
	{
		startFrame = max<int>(RA.getInFrame(), TA.getInFrame());
		stopFrame = min<int>(RA.getOutFrame(), TA.getOutFrame());
		for (auto frame = startFrame; frame < stopFrame; ++frame)
		{
			// Not sure how this happens, but no time to debug it! QQQ
			if (!(rotatedBox->isValidAtFrame(frame) && smoothedBox->isValidAtFrame(frame)))
			{
				break;
			}

			double d = rotatedBox->getTrackPointAtFrame(frame).y - smoothedBox->getTrackPointAtFrame(frame).y;
			RTrackGraph->Series[1]->AddXY(frame, d);

			dMax = max<double>(dMax, d);
			dMin = min<double>(dMin, d);
		}

		// Stupid way of setting axis to avoid min being greater than max
		double frExtent = dMax - dMin;
		auto iMinR = ((int)(min<double>(dMin, -frExtent / 2) - 7.5) / 5) * 5;
		auto iMaxR = ((int)(max<double>(dMax, frExtent / 2) + 7.5) / 5) * 5;
		RTrackGraph->LeftAxis->Minimum = min<long>(iMinR, RTrackGraph->LeftAxis->Maximum) - 1;
		RTrackGraph->LeftAxis->Maximum = iMaxR;
		RTrackGraph->LeftAxis->Minimum = iMinR - 1;
	}
}
// ---------------------------------------------------------------------------

void SetControlFocus(EStabilizerCmd controlCommand)
{
	if (StabilizerForm == NULL)
		return;

	StabilizerForm->setControlFocus(controlCommand);
}
// ---------------------------------------------------------------------------

void TStabilizerForm::setControlFocus(EStabilizerCmd controlCommand)
{
	if ((GStabilizerTool == NULL) || GStabilizerTool->IsDisabled() || !IsToolVisible())
		return;

	TObject *newFocusedObject = NULL;

	switch (controlCommand)
	{
	case STAB_CMD_FOCUS_ON_TRACK_BOX_SIZE:
		newFocusedObject = TrackingBoxSizeTrackEdit->TrackBar;
		break;

	case STAB_CMD_FOCUS_ON_MOTION_SEARCH_AREA:
		newFocusedObject = MotionTrackEdit->TrackBar;
		break;

	case STAB_CMD_FOCUS_ON_SMOOTHING:
		if (SmoothTrackEdit->TrackBar->Enabled)
		{
			newFocusedObject = GStabilizerTool->isInAutoTrack()
										? AutoModeSmoothTrackEdit->TrackBar
										: SmoothTrackEdit->TrackBar;
		}
		break;

	case STAB_CMD_MISSING_DATA_FILL_NEXT:
		if (MissingDataBlackRadioButton->Checked)
		{
			MissingDataWhiteRadioButton->Checked = true;
			newFocusedObject = MissingDataWhiteRadioButton;
		}
		else if (MissingDataPrevFrameRadioButton->Checked)
		{
			MissingDataZoomRadioButton->Checked = true;
			newFocusedObject = MissingDataZoomRadioButton;
		}
		else if (MissingDataZoomRadioButton->Checked)
		{
			MissingDataBlackRadioButton->Checked = true;
			newFocusedObject = MissingDataBlackRadioButton;
		}
		else // default
		{
			MissingDataPrevFrameRadioButton->Checked = true;
			newFocusedObject = MissingDataPrevFrameRadioButton;
		}
		break;

	case STAB_CMD_MISSING_DATA_FILL_PREV:
		if (MissingDataBlackRadioButton->Checked)
		{
			MissingDataZoomRadioButton->Checked = true;
			newFocusedObject = MissingDataZoomRadioButton;
		}
		else if (MissingDataWhiteRadioButton->Checked)
		{
			MissingDataBlackRadioButton->Checked = true;
			newFocusedObject = MissingDataBlackRadioButton;
		}
		else if (MissingDataPrevFrameRadioButton->Checked)
		{
			MissingDataWhiteRadioButton->Checked = true;
			newFocusedObject = MissingDataWhiteRadioButton;
		}
		else
		{
			MissingDataPrevFrameRadioButton->Checked = true;
			newFocusedObject = MissingDataPrevFrameRadioButton;
		}
		break;

#ifndef NO_JITTER
	case STAB_CMD_FOCUS_ON_ADD_JITTER:
		newFocusedObject = JitterTrackEdit->TrackBar;
		break;
#endif

	case STAB_CMD_MOTION_GROUP_NEXT:
		// Leave "MatchRef" out of the cycle.
		if (SmoothMotionRadioButton->Checked)
		{
			NoMotionRadioButton->Checked = true;
			newFocusedObject = NoMotionRadioButton;
		}
		else
		{
			SmoothMotionRadioButton->Checked = true;
			newFocusedObject = SmoothMotionRadioButton;
		}
		break;

	case STAB_CMD_MOTION_GROUP_PREV:
		// Leave "MatchRef" out of the cycle.
		if (NoMotionRadioButton->Checked)
		{
			SmoothMotionRadioButton->Checked = true;
			newFocusedObject = SmoothMotionRadioButton;
		}
		else
		{
			NoMotionRadioButton->Checked = true;
			newFocusedObject = NoMotionRadioButton;
		}
		break;

	case STAB_CMD_ANCHOR_GROUP_NEXT:
		// Always leave "None" out of the cycle.
		// Leave "Both" out of cycle unless Motion is set to Smooth.
		// This group should be disabled when Motion is "Match Reference".
		if (AnchorFirstRadioButton->Checked)
		{
			AnchorLastRadioButton->Checked = true;
			newFocusedObject = AnchorLastRadioButton;
		}
		else if (AnchorLastRadioButton->Checked && SmoothMotionRadioButton->Checked)
		{
			AnchorBothRadioButton->Checked = true;
			newFocusedObject = AnchorBothRadioButton;
		}
#ifdef CYCLE_ANCHOR_INCLUDES_NONE
		else if (AnchorBothRadioButton->Checked)
		{
			AnchorNoneRadioButton->Checked = true;
			newFocusedObject = AnchorFramesGroupBox;
		}
#endif
		else
		{
			AnchorFirstRadioButton->Checked = true;
			newFocusedObject = AnchorFirstRadioButton;
		}
		break;

	case STAB_CMD_ANCHOR_GROUP_PREV:
		// Always leave "None" out of the cycle.
		// Leave "Both" out of cycle unless Motion is set to Smooth.
		// This group should be disabled when Motion is "Match Reference".
		if (AnchorLastRadioButton->Checked)
		{
			AnchorFirstRadioButton->Checked = true;
			newFocusedObject = AnchorFirstRadioButton;
		}
		else if (AnchorFirstRadioButton->Checked && SmoothMotionRadioButton->Checked)
		{
			AnchorBothRadioButton->Checked = true;
			newFocusedObject = AnchorBothRadioButton;
		}
		else
		{
			AnchorLastRadioButton->Checked = true;
			newFocusedObject = AnchorLastRadioButton;
		}
		break;

	case STAB_CMD_STABILIZE_GROUP_NEXT:
		if (HorizontalRadioButton->Checked)
		{
			VerticalRadioButton->Checked = true;
			newFocusedObject = VerticalRadioButton;
		}
		else if (VerticalRadioButton->Checked)
		{
			HorizAndVertRadioButton->Checked = true;
			newFocusedObject = HorizAndVertRadioButton;
		}
		else if (HorizAndVertRadioButton->Checked)
		{
			HorizVertAndRotationRadioButton->Checked = true;
			newFocusedObject = HorizVertAndRotationRadioButton;
		}
		else
		{
			HorizontalRadioButton->Checked = true;
			newFocusedObject = HorizontalRadioButton;
		}
		break;

	case STAB_CMD_STABILIZE_GROUP_PREV:
		if (VerticalRadioButton->Checked)
		{
			HorizontalRadioButton->Checked = true;
			newFocusedObject = HorizontalRadioButton;
		}
		else if (HorizAndVertRadioButton->Checked)
		{
			VerticalRadioButton->Checked = true;
			newFocusedObject = VerticalRadioButton;
		}
		else if (HorizVertAndRotationRadioButton->Checked)
		{
			HorizAndVertRadioButton->Checked = true;
			newFocusedObject = HorizAndVertRadioButton;
		}
		else
		{
			HorizVertAndRotationRadioButton->Checked = true;
			newFocusedObject = HorizVertAndRotationRadioButton;
		}
		break;

	default:

		break;
	}

	if (newFocusedObject != NULL)
	{
		GStabilizerTool->getSystemAPI()->FocusAdoptedControl(reinterpret_cast<int *>(newFocusedObject));
	}

	// In case of new conflict
	ConformUseSubPixelInterpolationCheckBox();
}

// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------
// --------------------- Legacy Crap -----------------------------------------
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------

void TStabilizerForm::UpdateToolStatusBar(int iPanel, const string &strMessage) {
	StatusBar->Panels->Items[iPanel]->Text = strMessage.c_str();}

// ---------------------------------------------------------------------------
void __fastcall TStabilizerForm::UpdateTimerTimer(TObject *Sender)
{
	if (GStabilizerTool == NULL)
		return;

	if (GStabilizerTool->GetControlState() == STABILIZER_STATE_RENDERING
	&& ShowPointsOldControlState != STABILIZER_STATE_RENDERING)
	{
		ShowPointsOldControlState = STABILIZER_STATE_RENDERING;
		ShowPointsOldCheckedState = ShowPointCBox->Checked;
		ShowPointCBox->Checked = false;
		GStabilizerTool->setDisplayTrackingPoints(false);
	}
	else if (GStabilizerTool->GetControlState() != STABILIZER_STATE_RENDERING
	&& ShowPointsOldControlState == STABILIZER_STATE_RENDERING)
	{
		ShowPointsOldControlState = STABILIZER_STATE_IDLE;
		// Per larry 2010-06, leave this OFF!
		ShowPointCBox->Checked = false; // ShowPointsOldCheckedState;
		GStabilizerTool->setDisplayTrackingPoints(ShowPointCBox->Checked);
	}

	UpdateStatusBarInfo();
}
// ---------------------------------------------------------------------------

int TStabilizerForm::LastSelectedTag(void)
{
	if (GStabilizerTool == NULL)
		return INVALID_TRACKING_BOX_TAG;
	return GStabilizerTool->GetLastSelectedTag();
}
// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------
// ------------------ Stabilization Mode Controls ----------------------------
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

// ------------------SetRefFrameTCToDefault--------John Mertus----Nov 2002-----
// This just returns the RefFrameTC to the mark in or the start of the file.

void TStabilizerForm::SetRefFrameTCToDefault(void)
{
	if (GStabilizerTool == NULL)
		return;

	CVideoFrameList *frameList;
	frameList = GStabilizerTool->getSystemAPI()->getVideoFrameList();
	if (frameList == NULL)
		return;
	int frameIndex = GStabilizerTool->getSystemAPI()->getMarkIn();
	if (frameIndex < 0)
		frameIndex = frameList->getInFrameIndex();
	PTimecode ptc(frameList->getTimecodeForFrameIndex(frameIndex));
	if (RefFrameTimeCodeEdit->tcP != ptc)
	{
		RefFrameTimeCodeEdit->tcP = ptc;
		RefFrameTimeCodeEditExit(NULL);
	}
}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::ResetRefFrameButtonClick(TObject *Sender)
{
	if (GStabilizerTool == NULL)
		return;

	// This gets the current time code
	CVideoFrameList *frameList = GStabilizerTool->getSystemAPI()->getVideoFrameList();
	if (frameList == NULL)
		return;
	int currentFrameIndex = GStabilizerTool->getSystemAPI()->getLastFrameIndex();
	PTimecode ptc(frameList->getTimecodeForFrameIndex(currentFrameIndex));
	if (RefFrameTimeCodeEdit->tcP != ptc)
	{
		RefFrameTimeCodeEdit->tcP = ptc;
		RefFrameTimeCodeEditExit(Sender);
	}
}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::GoToRefframeButtonClick(TObject *Sender)
{
	if (GStabilizerTool == NULL)
		return;

	RefFrameTimeCodeEdit->VExit(0);
	if (GStabilizerTool == NULL)
		return;
	CVideoFrameList *frameList = GStabilizerTool->getSystemAPI()->getVideoFrameList();
	if (frameList == NULL)
		return;

	// No! GStabilizerTool->getSystemAPI()->AcceptGOV();   // Auto-accept GOV if pending

	int nominalFrameIndex = frameList->getFrameIndex(RefFrameTimeCodeEdit->tcP);
	GStabilizerTool->getSystemAPI()->goToFrameSynchronous(nominalFrameIndex);

	// wtf?? may go someplace else?
	int actualFrameIndex = GStabilizerTool->getSystemAPI()->getLastFrameIndex();
	if (actualFrameIndex != nominalFrameIndex)
	{
		RefFrameTimeCodeEdit->tcP = frameList->getTimecodeForFrameIndex(actualFrameIndex);
		RefFrameTimeCodeEditExit(Sender);
	}
}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::RefFrameTimeCodeEditExit(TObject *Sender)
{
	if (MatchRefRadioButton->Checked)
	{
		InvalidateSmoothedData();
		UpdatePresetCurrentParameters();
	}
}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::RefFrameTimeCodeEditEnterOrEscapeKey(TObject *Sender)
{
	RefFrameTimeCodeEditExit(Sender);
	HowToStabilizeGroupBox->SetFocus();
}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::HowToStabilizeRadioButtonClick(TObject *Sender)
{
	if (SmoothMotionRadioButton->Checked)
	{
		GoToRefframeButton->Enabled = false;
		RefFrameTimeCodeEdit->Enabled = false;
		ResetRefFrameButton->Enabled = false;
		AnchorFirstRadioButton->Enabled = true;
		AnchorLastRadioButton->Enabled = true;
		AnchorBothRadioButton->Enabled = true;
		AnchorNoneRadioButton->Enabled = true;
		SmoothTrackEdit->Enable(true);
		SmoothMinLabel->Enabled = true;
		SmoothMaxLabel->Enabled = true;
	}
	else if (NoMotionRadioButton->Checked)
	{
		GoToRefframeButton->Enabled = false;
		RefFrameTimeCodeEdit->Enabled = false;
		ResetRefFrameButton->Enabled = false;
		if (AnchorBothRadioButton->Checked)
			AnchorFirstRadioButton->Checked = true;
		AnchorFirstRadioButton->Enabled = true;
		AnchorLastRadioButton->Enabled = true;
		AnchorBothRadioButton->Enabled = false;
		AnchorNoneRadioButton->Enabled = true;
		SmoothTrackEdit->Enable(false);
		SmoothMinLabel->Enabled = false;
		SmoothMaxLabel->Enabled = false;
	}
	else if (MatchRefRadioButton->Checked)
	{
		GoToRefframeButton->Enabled = true;
		RefFrameTimeCodeEdit->Enabled = true;
		ResetRefFrameButton->Enabled = true;
		AnchorFirstRadioButton->Enabled = false;
		AnchorLastRadioButton->Enabled = false;
		AnchorBothRadioButton->Enabled = false;
		AnchorNoneRadioButton->Enabled = false;
		SmoothTrackEdit->Enable(false);
		SmoothMinLabel->Enabled = false;
		SmoothMaxLabel->Enabled = false;
	}

	InvalidateSmoothedData();
	UpdatePresetCurrentParameters();
}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::AnchorButtonClick(TObject *Sender)
{
	_GoodInCount = 0;
	_GoodOutCount = 0;
	if (AnchorFirstRadioButton->Checked)
		_GoodInCount = 1;
	else if (AnchorLastRadioButton->Checked)
		_GoodOutCount = 1;
	else if (AnchorBothRadioButton->Checked)
		_GoodInCount = _GoodOutCount = 1;

	InvalidateSmoothedData();
	UpdatePresetCurrentParameters();
}

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
// ------------------------- PREFERENCES -------------------------------------
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::PreferencesMenuItemClick(TObject *Sender)
{
	switch (_MarkInMovePreference)
	{
	case MOVEPREF_ALWAYS_COPY:
		StabilizerPreferencesForm->MoveAndCopyRadioButton->Checked = true;
		break;
	case MOVEPREF_ALWAYS_CLEAR:
		StabilizerPreferencesForm->MoveAndClearRadioButton->Checked = true;
		break;
	case MOVEPREF_PROMPT:
		StabilizerPreferencesForm->PromptMeRadioButton->Checked = true;
		break;
	}

	if (ShowModalDialog(StabilizerPreferencesForm) == mrOk)
	{
		if (StabilizerPreferencesForm->MoveAndCopyRadioButton->Checked)
			_MarkInMovePreference = MOVEPREF_ALWAYS_COPY;
		else if (StabilizerPreferencesForm->MoveAndClearRadioButton->Checked)
			_MarkInMovePreference = MOVEPREF_ALWAYS_CLEAR;
		else // (StabilizerPreferencesForm->PromptMeRadioButton->Checked)
				_MarkInMovePreference = MOVEPREF_PROMPT;
	}

	if (_MarkInMovePreference != GStabilizerTool->getMarkInMovePreference())
		GStabilizerTool->setMarkInMovePreference(_MarkInMovePreference);
}
// ---------------------------------------------------------------------------

// PRESETS!
void TStabilizerForm::CurrentPresetHasChanged(void *Sender) {UpdateGuiFromCurrentPreset();}
// ---------------------------------------------------------------------------

// PRESETS!
void TStabilizerForm::UpdateGuiFromCurrentPreset()
{
	auto presets = GStabilizerTool->GetPresets();
	auto parameters = presets->GetCurrentPreset();
	SetParameters(parameters);
}
// ---------------------------------------------------------------------------

// PRESETS!
void TStabilizerForm::UpdatePresetCurrentParameters()
{
	CStabilizerParameters parameters;
	GatherParameters(parameters);
	GStabilizerTool->GetPresets()->SetCurrent(parameters);
}

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
// ------------------------ Tracking Points Controls -------------------------
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::TrackButtonClick(TObject *Sender)
{
	if (GStabilizerTool == NULL)
		return;

	GStabilizerTool->ButtonCommandHandler(STABILIZER_PROC_RETRACK);
}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::DeletePointButtonClick(TObject *Sender)
{
	if (GStabilizerTool == NULL)
		return;

	GStabilizerTool->getSystemAPI()->setMainWindowFocus();
	GStabilizerTool->DeleteSelectedTrackingPoints();
	GStabilizerTool->getSystemAPI()->getBaseToolForm()->SetFocus();
}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::NextPointButtonClick(TObject *Sender)
{
	if (GStabilizerTool == NULL)
		return;

	GStabilizerTool->SelectTrackingPointRelative(+1);
}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::PrevPointButtonClick(TObject *Sender)
{
	if (GStabilizerTool == NULL)
		return;

	GStabilizerTool->SelectTrackingPointRelative(-1);
}
// ---------------------------------------------------------------------------

string GetFilenameForSavingTrackingPoints(const string &suggestedFilename)
{
	if (StabilizerForm == NULL)
		return string("");

	return StabilizerForm->getSaveFilename(suggestedFilename);
}

string TStabilizerForm::getSaveFilename(const string &suggestedFilename)
{
	SaveTrackingPointsDialog->FileName = GetFileNameWithExt(suggestedFilename).c_str();
	bool retVal = SaveTrackingPointsDialog->Execute();
	if (!retVal || SaveTrackingPointsDialog->FileName.IsEmpty())
	{
		return string("");
	}

	return StringToStdString(SaveTrackingPointsDialog->FileName);
}
// ---------------------------------------------------------------------------

string GetFilenameForLoadingTrackingPoints(const string &suggestedFilename)
{
	if (StabilizerForm == NULL)
		return string("");

	return StabilizerForm->getLoadFilename(suggestedFilename);
}

string TStabilizerForm::getLoadFilename(const string &suggestedFilename)
{
	OpenTrackingPointsDialog->FileName = GetFileNameWithExt(suggestedFilename).c_str();
	bool retVal = OpenTrackingPointsDialog->Execute();
	if (!retVal || OpenTrackingPointsDialog->FileName.IsEmpty())
	{
		return string("");
	}

	return StringToStdString(OpenTrackingPointsDialog->FileName);
}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::LoadPointsButtonClick(TObject *Sender)
{
	if (GStabilizerTool == NULL)
		return;

	// New: Don't restore marks if shift is on
	GStabilizerTool->LoadTrackingPointsFromFile(!GStabilizerTool->isShiftOn());
	if (!ShowPointCBox->Checked)
	{
		// Always show points if not in auto-track, and never if in auto-track.
		ShowPointCBox->Checked = !GStabilizerTool->isInAutoTrack();
	}

	// In auto-track mode, 'select' the the virtual point so the graphs light up.
	if (GStabilizerTool->isInAutoTrack())
	{
		GStabilizerTool->SelectTrackingPointRelative(+1);
	}
}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::SavePointsButtonClick(TObject *Sender)
{
	if (GStabilizerTool == NULL)
		return;

	GStabilizerTool->SaveTrackingPointsToFile();
}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::ShowPointCBoxClick(TObject *Sender)
{
	if (GStabilizerTool == NULL)
		return;

	GStabilizerTool->setDisplayTrackingPoints(ShowPointCBox->Checked);
}
// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
// ------------------------ EXEC BUTTONS TOOLBAR -----------------------------
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::ExecButtonsToolbar_ButtonPressedNotifier(TObject *Sender)
{
	switch (ExecButtonsToolbar->GetLastButtonClicked())
	{
	default:
	case EXEC_BUTTON_NONE:
		// No-op
		break;

	case EXEC_BUTTON_PREVIEW_FRAME:
		ExecRenderOrPreview(STABILIZER_PROC_PREVIEW_1);
		break;

	case EXEC_BUTTON_PREVIEW_NEXT_FRAME:
		ExecRenderOrPreview(STABILIZER_PROC_PREVIEW_NEXT);
		break;

	case EXEC_BUTTON_PREVIEW_ALL:
		GStabilizerTool->AutoSaveTrackingPoints();
		ExecRenderOrPreview(STABILIZER_PROC_PREVIEW);
		break;

	case EXEC_BUTTON_RENDER_FRAME:
		ExecRenderOrPreview(STABILIZER_PROC_RENDER_1);
		break;

	case EXEC_BUTTON_RENDER_NEXT_FRAME:
		ExecRenderOrPreview(STABILIZER_PROC_RENDER_NEXT);
		break;

	case EXEC_BUTTON_RENDER_ALL:
		GStabilizerTool->AutoSaveTrackingPoints();
		ExecRenderOrPreview(STABILIZER_PROC_RENDER);
		break;

	case EXEC_BUTTON_PAUSE_OR_RESUME:
		if (ExecButtonsToolbar->IsButtonEnabled(EXEC_BUTTON_PAUSE))
			ExecRenderOrPreview(STABILIZER_PROC_PAUSE);
		else if (ExecButtonsToolbar->IsButtonEnabled(EXEC_BUTTON_RESUME))
			ExecRenderOrPreview(STABILIZER_PROC_CONTINUE);
		break;

	case EXEC_BUTTON_PAUSE:
		ExecRenderOrPreview(STABILIZER_PROC_PAUSE);
		break;

	case EXEC_BUTTON_RESUME:
		ExecRenderOrPreview(STABILIZER_PROC_CONTINUE);
		break;

	case EXEC_BUTTON_STOP:
		ExecStop();
		break;

	case EXEC_BUTTON_CAPTURE_PDL:
		ExecCaptureToPDL();
		break;

	case EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS:
		ExecCaptureAllEventsToPDL();
		break;

	case EXEC_BUTTON_GO_TO_RESUME_TIMECODE:
		ExecGoToResumeTimecode();
		break;

	case EXEC_BUTTON_SET_RESUME_TC:
		ExecSetResumeTimecodeToCurrent();
		break;
	}
}
// ---------------- Resume Timecode Functions --------------------------------

void TStabilizerForm::ExecSetResumeTimecode(int frameIndex)
{
	CVideoFrameList *frameList = GStabilizerTool->getSystemAPI()->getVideoFrameList();
	if (frameList == NULL)
		return;

	if (frameIndex < 0)
		frameIndex = frameList->getInFrameIndex();

	GStabilizerTool->ResetResumeFrameIndex(frameIndex);
	PTimecode frameTimecode = frameList->getTimecodeForFrameIndex(frameIndex);
	ExecButtonsToolbar->SetResumeTimecode(frameTimecode);
}
// ---------------------------------------------------------------------------

void TStabilizerForm::ExecSetResumeTimecodeToCurrent(void)
{
	if (GStabilizerTool == NULL)
		return;

	int frameIndex = GStabilizerTool->getSystemAPI()->getLastFrameIndex();
	ExecSetResumeTimecode(frameIndex);
}
// ---------------------------------------------------------------------------

void TStabilizerForm::ExecSetResumeTimecodeToDefault(void)
{
	if (GStabilizerTool == NULL)
		return;

	int frameIndex = GStabilizerTool->getSystemAPI()->getMarkIn();
	ExecSetResumeTimecode(frameIndex);

	// NOTE!! Old way used to do:
	// GStabilizerTool->ResetResumeFrameIndex(-1)
	// but we just let it be set to the mark in frame by ExecSetResumeTimecode!
}
// ---------------------------------------------------------------------------

void TStabilizerForm::ExecGoToResumeTimecode(void)
{
	if (GStabilizerTool == NULL)
		return;

	CVideoFrameList *frameList;
	frameList = GStabilizerTool->getSystemAPI()->getVideoFrameList();
	if (frameList == NULL)
		return;

	// No! GStabilizerTool->getSystemAPI()->AcceptGOV();   // Auto-accept GOV if pending

	PTimecode resumeTimecode = ExecButtonsToolbar->GetResumeTimecode();
	int frameIndex = frameList->getFrameIndex(resumeTimecode);
	GStabilizerTool->getSystemAPI()->goToFrameSynchronous(frameIndex);

	// In case it ends up somewhere else?
	frameIndex = GStabilizerTool->getSystemAPI()->getLastFrameIndex();
	GStabilizerTool->ResetResumeFrameIndex(frameIndex);
	ExecButtonsToolbar->SetResumeTimecode(frameList->getTimecodeForFrameIndex(frameIndex));
}

// ------------------ ExecRenderOrPreview ------------------------------------
//
// Call this when any render or preview button is clicked
// buttonCommand identifies the button that was pressed
//
void TStabilizerForm::ExecRenderOrPreview(EStabilizerProcessCmd buttonCommand)
{
	if (GStabilizerTool == NULL)
	{
		return;
	}

	if (GStabilizerTool->GetNumberOfTrackingBoxes() < 1 && !GStabilizerTool->isInAutoTrack())
	{
		theError.set(STABILIZER_ERROR_STABILIZER_TOO_FEW_TRACKPOINTS, "You must set at least one tracking point! ");
		_MTIErrorDialog(Handle, theError.getFmtError());
		return;
	}

	// Stupid tracking pause mega hack
	auto stupidTrackingHackState = GStabilizerTool->GetStupidTrackingHackState();
	if ((stupidTrackingHackState == TRACKING && buttonCommand == STABILIZER_PROC_PAUSE) ||
		(stupidTrackingHackState == TRACKING_PAUSED && buttonCommand == STABILIZER_PROC_CONTINUE))
	{
		GStabilizerTool->ButtonCommandHandler(buttonCommand);
		return;
	}

	// Consistence check
	// int markIn = GStabilizerTool->getSystemAPI()->getMarkIn();
	// int markOut = GStabilizerTool->getSystemAPI()->getMarkOut();

	// Did the resume timecode change?
	if (buttonCommand == STABILIZER_PROC_CONTINUE)
	{
		ExecGoToResumeTimecode();
	}

	// Operate on NEXT frame?
	if (buttonCommand == STABILIZER_PROC_PREVIEW_NEXT || buttonCommand == STABILIZER_PROC_RENDER_NEXT)
	{
		int current = GStabilizerTool->getSystemAPI()->getLastFrameIndex();
		int markOut = GStabilizerTool->getSystemAPI()->getMarkOut();
		if (current < (markOut - 1))
		{
			GStabilizerTool->getSystemAPI()->goToFrameSynchronous(current + 1);
			buttonCommand = (buttonCommand == STABILIZER_PROC_PREVIEW_NEXT) ? STABILIZER_PROC_PREVIEW_1 :
				STABILIZER_PROC_RENDER_1;
			int actualFrameIndex = GStabilizerTool->getSystemAPI()->getLastFrameIndex();
			ExecSetResumeTimecode(actualFrameIndex + 1);
		}
	}

	// // Pass the button's tool processing command to Process Handler
	// GStabilizerTool->CheckIfNeedToRetrack();

	GStabilizerTool->ButtonCommandHandler(buttonCommand);
}
// ---------------------------------------------------------------------------

void TStabilizerForm::ExecStop(void)
{
	if (GStabilizerTool == NULL)
		return;

	GStabilizerTool->ButtonCommandHandler(STABILIZER_PROC_STOP);
}
// ---------------------------------------------------------------------------

void TStabilizerForm::ExecCaptureToPDL(void)
{
	if (GStabilizerTool == NULL)
		return;

	bool all = false;
	if (HIWORD(GetKeyState(VK_SHIFT)) != 0)
		all = true;

	GStabilizerTool->getSystemAPI()->CapturePDLEntry(all);
}
// ---------------------------------------------------------------------------

void TStabilizerForm::ExecCaptureAllEventsToPDL(void)
{
	if (GStabilizerTool == NULL)
		return;

	GStabilizerTool->getSystemAPI()->CapturePDLEntry(true);
}
// ---------------------------------------------------------------------------

void TStabilizerForm::UpdateExecutionButtonToolbar(EToolControlState toolProcessingControlState,
	bool processingRenderFlag, StupidTrackingHackState stupidTrackingHackState)
{
	if (GStabilizerTool == NULL)
		return;

	if (GStabilizerTool->IsDisabled())
	{
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
		ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_STOP);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

		// ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_ALL, "Can't preview - tool is disabled");
		// ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL, "Can't render - tool is disabled");
		// ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL, "Can't add to PDL - tool is disabled");
		return;
	}

	if (stupidTrackingHackState == TRACKING || stupidTrackingHackState == TRACKING_PAUSED)
	{
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
		ExecButtonsToolbar->EnableButton(EXEC_BUTTON_STOP);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

		// ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_ALL, "Can't preview while tracking");
		// ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL, "Can't render while tracking");
		// ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL, "Can't add to PDL while tracking");

		switch (stupidTrackingHackState)
		{
		case TRACKING:
			ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
			ExecButtonsToolbar->EnableButton(EXEC_BUTTON_PAUSE);
			ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
			ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
			break;

		case TRACKING_PAUSED:
			ExecButtonsToolbar->HideButton(EXEC_BUTTON_PAUSE);
			ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
			ExecButtonsToolbar->ShowButton(EXEC_BUTTON_RESUME);
			ExecButtonsToolbar->EnableButton(EXEC_BUTTON_RESUME);
			break;

		default:
			break;
		}

		return;
	}

	int previewFrameIndex = GStabilizerTool->GetPreviewFrameIndex();
	bool processingSingleFrameFlag = (previewFrameIndex >= 0);

	switch (toolProcessingControlState)
	{
	case TOOL_CONTROL_STATE_STOPPED:

		// Previewing or Rendering has stopped, either because processing
		// has completed or the user pressed the Stop button

		setIdleCursor();

		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);

		if (!GStabilizerTool->isAllTrackDataValid() && !GStabilizerTool->isInAutoTrack())
		{
			ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
			// ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_ALL,
			// "Can't preview - invalid tracking data");

			ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
			// ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL,
			// "Can't render - invalid tracking data");

			if (TrackButton->Enabled)
			{
				ExecButtonsToolbar->EnableButton(EXEC_BUTTON_CAPTURE_PDL);
				// ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL,
				// "Add to PDL (, key)");
			}
			else
			{
				ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);
				// ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL,
				// "Can't add to PDL - no tracking points");
			}
		}
		else
		{
			if (GStabilizerTool->AreMarksValid())
			{
				ExecButtonsToolbar->EnableButton(EXEC_BUTTON_PREVIEW_ALL);
				ExecButtonsToolbar->EnableButton(EXEC_BUTTON_RENDER_ALL);
				ExecButtonsToolbar->EnableButton(EXEC_BUTTON_CAPTURE_PDL);

				// ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_ALL,
				// "Preview marked range (Shift+D)");
				// ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL,
				// "Render marked range (Shift+G)");
				// ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL,
				// "Add to PDL (, key)");
			}
			else
			{
				ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
				ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
				ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

				// ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_PREVIEW_ALL,
				// "Can't preview - marks are invalid");
				// ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_RENDER_ALL,
				// "Can't render - marks are invalid");
				// ExecButtonsToolbar->SetButtonHint(EXEC_BUTTON_CAPTURE_PDL,
				// "Can't add to PDL - marks are invalid");
			}
		}
		ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_STOP);

		ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_NONE);

		ExecButtonsToolbar->EnableResumeWidget();
		ExecSetResumeTimecodeToDefault();

		UpdateTrackingCharts();

		break;

	case TOOL_CONTROL_STATE_RUNNING:

		// Preview1, Render1, Preview or Render is now running

		setBusyCursor();

		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
		ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
		if (processingSingleFrameFlag || (GStabilizerTool->GetControlState() != STABILIZER_STATE_RENDERING))
		{
			ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
		}
		else
		{
			ExecButtonsToolbar->EnableButton(EXEC_BUTTON_PAUSE);
		}
		ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->EnableButton(EXEC_BUTTON_STOP);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

		if (!processingRenderFlag)
			ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_PREVIEW_ALL);
		else
			ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_RENDER_ALL);

		break;

	case TOOL_CONTROL_STATE_PAUSED:
		// Previewing or Rendering is now Paused

		setIdleCursor();

		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
		ExecButtonsToolbar->ShowButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->EnableButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->HideButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->EnableButton(EXEC_BUTTON_STOP);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

		// Now go load the current timecode
		ExecSetResumeTimecodeToCurrent();

		break;

	case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_STOPPED:
	case TOOL_CONTROL_STATE_RUNNING_PREVIEW_1_FROM_PAUSED:
	case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_STOPPED:
	case TOOL_CONTROL_STATE_RUNNING_RENDER_1_FROM_PAUSED:
	case TOOL_CONTROL_STATE_WAITING_TO_PAUSE:
	case TOOL_CONTROL_STATE_WAITING_TO_STOP:

		// Processing single frame or waiting to stop or pause --
		// disable everything
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_FRAME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_FRAME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PREVIEW_ALL);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RENDER_ALL);
		ExecButtonsToolbar->ShowButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_PAUSE);
		ExecButtonsToolbar->HideButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_RESUME);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_STOP);
		ExecButtonsToolbar->DisableButton(EXEC_BUTTON_CAPTURE_PDL);

		if (toolProcessingControlState != TOOL_CONTROL_STATE_WAITING_TO_PAUSE && toolProcessingControlState !=
			TOOL_CONTROL_STATE_WAITING_TO_STOP)
		{
			if (!processingRenderFlag)
				ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_PREVIEW_FRAME);
			else
				ExecButtonsToolbar->SetDownButton(EXEC_BUTTON_RENDER_FRAME);
		}

		break;

	default:
		MTIassert(false);

		break;
	}

	UpdateStatusBarInfo();

} // UpdateExecutionButton()
// ---------------------------------------------------------------------------

bool RunExecButtonsCommand(EExecButtonId command)
{
	if (StabilizerForm == NULL)
		return false;

	return StabilizerForm->runExecButtonsCommand(command);
}

bool TStabilizerForm::runExecButtonsCommand(EExecButtonId command) {return ExecButtonsToolbar->RunCommand(command);}
// ---------------------------------------------------------------------------

void TStabilizerForm::setIdleCursor(void)
{
	if (GStabilizerTool == NULL)
		return;

	if (!GStabilizerTool->IsInColorPickMode())
		Screen->Cursor = crDefault;
}
// ---------------------------------------------------------------------------

void TStabilizerForm::setBusyCursor(void) {Screen->Cursor = crAppStart;}
// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
// ---------------------- Processing Region Widget ---------------------------
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::TopUpButtonClick(TObject *Sender)
{
	// This avoids the button up causing a move
	if (GlobalSender == NULL)
		return;

	int m = 1;
	if (HIWORD(GetKeyState(VK_SHIFT)) != 0)
		m = 10 * m;

	RECT bb = GStabilizerTool->getBoundingBox();
	bb.top = max<long>(0, bb.top - m);
	GStabilizerTool->setBoundingBox(bb);
}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::TopDownButtonClick(TObject *Sender)
{
	// This avoids the button up causing a move
	if (GlobalSender == NULL)
		return;

	int m = 1;
	if (HIWORD(GetKeyState(VK_SHIFT)) != 0)
		m = 10 * m;

	const CImageFormat* imgFmt = GStabilizerTool->getSystemAPI()->getVideoClipImageFormat();
	RECT bb = GStabilizerTool->getBoundingBox();
	bb.top = min<long>(imgFmt->getLinesPerFrame() - 1, bb.top + m);
	GStabilizerTool->setBoundingBox(bb);
}

// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::BottomDownButtonClick(TObject *Sender)
{
	// This avoids the button up causing a move
	if (GlobalSender == NULL)
		return;

	int m = 1;
	if (HIWORD(GetKeyState(VK_SHIFT)) != 0)
		m = 10 * m;

	const CImageFormat* imgFmt = GStabilizerTool->getSystemAPI()->getVideoClipImageFormat();
	RECT bb = GStabilizerTool->getBoundingBox();
	bb.bottom = min<long>(imgFmt->getLinesPerFrame() - 1, bb.bottom + m);
	GStabilizerTool->setBoundingBox(bb);

}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::BottomUpButtonClick(TObject *Sender)
{
	// This avoids the button up causing a move
	if (GlobalSender == NULL)
		return;

	int m = 1;
	if (HIWORD(GetKeyState(VK_SHIFT)) != 0)
		m = 10 * m;

	RECT bb = GStabilizerTool->getBoundingBox();
	bb.bottom = max<long>(0, bb.bottom - m);
	GStabilizerTool->setBoundingBox(bb);

}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::LeftLeftButtonClick(TObject *Sender)
{
	// This avoids the button up causing a move
	if (GlobalSender == NULL)
		return;

	int m = 1;
	if (HIWORD(GetKeyState(VK_SHIFT)) != 0)
		m = 10 * m;

	RECT bb = GStabilizerTool->getBoundingBox();
	bb.left = max<long>(0, bb.left - m);
	GStabilizerTool->setBoundingBox(bb);

}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::RightLeftButtonClick(TObject *Sender)
{
	// This avoids the button up causing a move
	if (GlobalSender == NULL)
		return;

	int m = 1;
	if (HIWORD(GetKeyState(VK_SHIFT)) != 0)
		m = 10 * m;

	RECT bb = GStabilizerTool->getBoundingBox();
	bb.right = max<long>(0, bb.right - m);
	GStabilizerTool->setBoundingBox(bb);

}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::RightRightButtonClick(TObject *Sender)
{
	// This avoids the button up causing a move
	if (GlobalSender == NULL)
		return;

	int m = 1;
	if (HIWORD(GetKeyState(VK_SHIFT)) != 0)
		m = 10 * m;

	const CImageFormat* imgFmt = GStabilizerTool->getSystemAPI()->getVideoClipImageFormat();
	RECT bb = GStabilizerTool->getBoundingBox();
	bb.right = min<long>(imgFmt->getPixelsPerLine() - 1, bb.right + m);
	GStabilizerTool->setBoundingBox(bb);
}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::LeftRightButtonClick(TObject *Sender)
{
	// This avoids the button up causing a move
	if (GlobalSender == NULL)
		return;

	int m = 1;
	if (HIWORD(GetKeyState(VK_SHIFT)) != 0)
		m = 10 * m;

	const CImageFormat* imgFmt = GStabilizerTool->getSystemAPI()->getVideoClipImageFormat();
	RECT bb = GStabilizerTool->getBoundingBox();
	bb.left = min<long>(imgFmt->getPixelsPerLine() - 1, bb.left + m);
	GStabilizerTool->setBoundingBox(bb);

}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::AllMatButtonClick(TObject *Sender)
{
	const CImageFormat* imgFmt = GStabilizerTool->getSystemAPI()->getVideoClipImageFormat();
	RECT bb;
	bb.left = 0;
	bb.top = 0;
	bb.right = imgFmt->getPixelsPerLine() - 1;
	bb.bottom = imgFmt->getLinesPerFrame() - 1;
	GStabilizerTool->setBoundingBox(bb);
}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::UseMattingCBoxClick(TObject *Sender)
{
	// Disable/enable the matting box
	static CStateMemory StateMemory;
	if (UseMattingCBox->Checked)
		EnableAllControls(ProcessingRegionGroupBox, StateMemory);
	else
		DisableAllControls(ProcessingRegionGroupBox, StateMemory);
	UseMattingCBox->Enabled = true; // need to be able to re-enable the group!
	ProcessingRegionGroupBox->Enabled = true; // need this enabled as well

	// Set the parameters
	// ShowMattingBoxCBox->Enabled = UseMattingCBox->Checked; ?? part of the group!
	GStabilizerTool->setDisplayRect(ShowMattingBoxCBox->Checked && UseMattingCBox->Checked);
	UpdatePresetCurrentParameters();
}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::DownTimerTimer(TObject *Sender)
{
	if (GlobalSender == NULL)
	{
		DownTimer->Enabled = false;
		return;
	}

	TSpeedButton *sb = dynamic_cast<TSpeedButton*>(GlobalSender);

	sb->OnClick(sb);
	DownTimer->Interval = 75;

}

// ---------------------------------------------------------------------------
// Emulate auto-repeat on mouse down on one of the matte sizing buttons
//
void __fastcall TStabilizerForm::AutoMoveButtonMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
	int X, int Y)
{
	GlobalSender = Sender;
	TSpeedButton *sb = dynamic_cast<TSpeedButton*>(GlobalSender);
	sb->OnClick(sb);

	DownTimer->Interval = 250;
	DownTimer->Enabled = true;

}

// ---------------------------------------------------------------------------
// Emulate auto-repeat on mouse down on one of the matte sizing buttons
//
void __fastcall TStabilizerForm::AutoMoveButtonMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
	int X, int Y)
{
	GlobalSender = NULL;
	DownTimer->Enabled = false;
}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::ShowMattingBoxCBoxClick(TObject *Sender)
{
	if (GStabilizerTool == NULL)
		return;

	GStabilizerTool->setDisplayRect(ShowMattingBoxCBox->Checked && UseMattingCBox->Checked);
}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::SetMatButtonClick(TObject *Sender)
{
	if (GStabilizerTool == NULL)
		return;

	GStabilizerTool->FindBoundingBox();
}
// ---------------------------------------------------------------------------

void ToggleTrackPointVisibility()
{
	if (StabilizerForm == NULL)
		return;

	StabilizerForm->toggleTrackPointVisibility();
}

void TStabilizerForm::toggleTrackPointVisibility()
{
	if (GStabilizerTool == NULL)
		return;

	if (ShowPointCBox->Enabled)
	{
		ShowPointCBox->Checked = !ShowPointCBox->Checked;
		GStabilizerTool->setDisplayTrackingPoints(ShowPointCBox->Checked);
	}
}
// ---------------------------------------------------------------------------

void SetTrackPointsVisibility(bool flag)
{
	if (StabilizerForm == NULL)
		return;

	StabilizerForm->setTrackPointsVisibility(flag);
}

void TStabilizerForm::setTrackPointsVisibility(bool flag)
{
	if (GStabilizerTool == NULL)
		return;

	ShowPointCBox->Checked = flag;
	GStabilizerTool->setDisplayTrackingPoints(flag);
}
// ---------------------------------------------------------------------------

void ToggleProcRegionVisibility()
{
	if (StabilizerForm == NULL)
		return;

	StabilizerForm->toggleProcRegionVisibility();
}

void TStabilizerForm::toggleProcRegionVisibility()
{
	if (GStabilizerTool == NULL)
		return;

	ShowMattingBoxCBox->Checked = !ShowMattingBoxCBox->Checked;
}
// ---------------------------------------------------------------------------

void ToggleProcRegionOperation()
{
	if (StabilizerForm == NULL)
		return;

	StabilizerForm->toggleProcRegionOperation();
}

void TStabilizerForm::toggleProcRegionOperation()
{
	if (GStabilizerTool == NULL)
		return;

	UseMattingCBox->Checked = !UseMattingCBox->Checked;
}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::ColorPickButtonClick(TObject *Sender)
{
	if (GStabilizerTool == NULL)
		return;

	if (GStabilizerTool->IsInColorPickMode())
	{
		GStabilizerTool->ExitColorPickMode();
		ColorPickButton->Down = false;
		ColorPickerStateTimer->Enabled = false;
	}
	else
	{
		GStabilizerTool->EnterColorPickMode();
		ColorPickButton->Down = true;
		ColorPickerStateTimer->Enabled = true;
	}
}
// ---------------------------------------------------------------------------

void ToggleColorPicker(void)
{
	if (StabilizerForm == NULL)
		return;
	StabilizerForm->ColorPickButtonClick(NULL);
}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::ColorPickerStateTimerTimer(TObject *Sender)
{
	if ((GStabilizerTool == NULL) || !GStabilizerTool->IsInColorPickMode())
	{
		ColorPickButton->Down = false;
		ColorPickerStateTimer->Enabled = false;
	}
}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::ClearOnShotChangeCBoxClick(TObject *Sender)
{
	if (GStabilizerTool == NULL)
		return;

	GStabilizerTool->SetClearTrackingStuffOnNewShot(ClearOnShotChangeCBox->Checked);
}
// ---------------------------------------------------------------------------

void ToggleClearOnShotChange()
{
	if (StabilizerForm == NULL)
		return;
	StabilizerForm->toggleClearOnShotChange();
}

void TStabilizerForm::toggleClearOnShotChange() {ClearOnShotChangeCBox->Checked = !ClearOnShotChangeCBox->Checked;}
// ---------------------------------------------------------------------------

void ToggleSubPixelInterpolation()
{
	if (StabilizerForm == NULL)
		return;
	StabilizerForm->toggleSubPixelInterpolation();
}

void TStabilizerForm::toggleSubPixelInterpolation()
{
	TCheckBox *checkBox = GStabilizerTool->isInAutoTrack()
									? AutoModeUseSubPixelInterpolationCheckBox
									: UseSubPixelInterpolationCheckBox;
	checkBox->Checked = !checkBox->Checked;
}
// ---------------------------------------------------------------------------

void ToggleSubtool()
{
	if (StabilizerForm == NULL)
		return;
	StabilizerForm->toggleSubtool();
}

void TStabilizerForm::toggleSubtool()
{
	ManualAutoTabControl->TabIndex = (ManualAutoTabControl->TabIndex == 0) ? 1 : 0;
	ManualAutoTabControlChange(nullptr);
}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::StabilizeMotionRadioButtonClick(TObject *Sender)
{
	TRadioButton *radioButton = dynamic_cast<TRadioButton*>(Sender);

	if (radioButton != NULL && radioButton->Checked)
	{
		_StabilizeMotionPreference = (radioButton == HorizVertAndRotationRadioButton) ? HVR :
			((radioButton == HorizontalRadioButton) ? H : ((radioButton == VerticalRadioButton) ? V : HV));
	}

	ConformUseSubPixelInterpolationCheckBox();
	InvalidateSmoothedData();
	UpdatePresetCurrentParameters();
}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::TrackingBoxSizeTrackEditTrackBarChange(TObject *Sender)
{
	TrackingBoxSizeTrackEdit->TrackBarChange(Sender);
	UpdatePresetCurrentParameters();
}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::TabToBeginning(TObject *Sender)
{
	// This is called before the spinedit control is made visible
	try
	{
		if (_TabStopDoNotDeletePanelVisible)
		{
			HowToStabilizeGroupBox->SetFocus();
		}
	}
	catch (...)
	{}
}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::AutoModeUseSubPixelInterpolationCheckBoxClick(TObject *Sender)
{
	if (_SubPixelCheckBoxWasClickedProgrammatically)
	{
		return;
	}

	_UseSubPixelInterpolationPreference = AutoModeUseSubPixelInterpolationCheckBox->Checked;
	// This is not in presets!
}
//---------------------------------------------------------------------------

void __fastcall TStabilizerForm::UseSubPixelInterpolationCheckBoxClick(TObject *Sender)
{
	if (_SubPixelCheckBoxWasClickedProgrammatically)
	{
		return;
	}

	_UseSubPixelInterpolationPreference = UseSubPixelInterpolationCheckBox->Checked;
	UpdatePresetCurrentParameters();
}
//---------------------------------------------------------------------------

void __fastcall TStabilizerForm::MissingDataRadioButtonClick(TObject *Sender)

{
	ConformUseSubPixelInterpolationCheckBox();
	UpdatePresetCurrentParameters();
}
// ---------------------------------------------------------------------------

void TStabilizerForm::ConformUseSubPixelInterpolationCheckBox()
{
	_SubPixelCheckBoxWasClickedProgrammatically = true;

	if (HorizVertAndRotationRadioButton->Checked || MissingDataZoomRadioButton->Checked)
	{
		UseSubPixelInterpolationCheckBox->Enabled = false;
		UseSubPixelInterpolationCheckBox->Checked = true;
	}
	else if (!UseSubPixelInterpolationCheckBox->Enabled)
	{
		UseSubPixelInterpolationCheckBox->Enabled = true;
		UseSubPixelInterpolationCheckBox->Checked = _UseSubPixelInterpolationPreference;
	}

	_SubPixelCheckBoxWasClickedProgrammatically = false;
}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::TrailSizeComboBoxChange(TObject *Sender)
{
	if (GStabilizerTool == NULL || GStabilizerTool->GetTrackingPointsEditor() == NULL)
	{
		return;
	}

	// Assumes that the items in the combo box are 0, 2, 4, 8, 16, ...
	int trailSize = (TrailSizeComboBox->ItemIndex <= 0) ? 0 : (1 << TrailSizeComboBox->ItemIndex);

	GStabilizerTool->GetTrackingPointsEditor()->setTrailSize(trailSize);

	// Defocus the combo box, but not if we entered here programmatically!
	if (Sender != NULL)
	{
		HowToStabilizeGroupBox->SetFocus();
	}
}
// ---------------------------------------------------------------------------

//void __fastcall TStabilizerForm::KPass1ButtonClick(TObject *Sender)
//{
//	if (GStabilizerTool == nullptr)
//	{
//		return;
//	}
//
//	// Kludge
//	GStabilizerTool->setIsInAutoTrack(true);
//	GStabilizerTool->ButtonCommandHandler(STABILIZER_PROC_KPASS1);
//}
// ---------------------------------------------------------------------------

void TStabilizerForm::updateAutoManualGuiDisplay()
{
	static CStateMemory TrackingParametersGroupBoxStateMemory;

	if (GStabilizerTool->isInAutoTrack())
	{
		UpdateTrackingPointButtons();
		SavePointsButton->Enabled = false;
		LoadPointsButton->Enabled = true;
		ClearOnShotChangeCBox->Enabled = false;
		TrailSizeComboBox->Enabled = false;
		TrackingPointsGroupBox->Visible = false;

		DisableAllControls(ManualTrackParametersPanel, TrackingParametersGroupBoxStateMemory);
		ManualTrackParametersPanel->Visible = false;
		AutoSourceIsAnimationCheckBox->Visible = true;
		AutoSourceIsAnimationCheckBox->Enabled = true;
		TrackingParametersGroupBox->Visible = true;

		PresetsFrame->Visible = false;

		// These are hidden because Larry thinks that showing rotation is pointless.
		NoRotationPanel->Visible = false;
		RTrackGraph->Visible = false;

		ShowPointCBox->Visible = false;
		_oldShowCheckBox = ShowPointCBox->Checked;
		ShowPointCBox->Checked = false;
		ShowPointCBox->Enabled = false;
		GStabilizerTool->setDisplayTrackingPoints(false);

		HowToStabilizeGroupBox->Visible = false;
		WhereToAnchorGroupBox->Visible = false;
		WhatToStabilizeGroupBox->Visible = false;

		AutoModeRenderParametersGroupBox->Visible = true;
		RenderParametersGroupBox->Visible = false;
	}
	else
	{
		UpdateTrackingPointButtons();
		LoadPointsButton->Enabled = true;
		ClearOnShotChangeCBox->Enabled = true;
		TrailSizeComboBox->Enabled = true;
		TrackingPointsGroupBox->Visible = true;

		ManualTrackParametersPanel->Visible = true;
		EnableAllControls(ManualTrackParametersPanel, TrackingParametersGroupBoxStateMemory);
		AutoSourceIsAnimationCheckBox->Visible = false;
		AutoSourceIsAnimationCheckBox->Enabled = false;
		TrackingParametersGroupBox->Visible = true;

		PresetsFrame->Visible = true;

		// These are hidden because Larry thinks that showing rotation is pointless.
		RTrackGraph->Visible = false;
		NoRotationPanel->Visible = false;

		ShowPointCBox->Enabled = true;
		ShowPointCBox->Checked = _oldShowCheckBox;
		ShowPointCBox->Visible = true;
		GStabilizerTool->setDisplayTrackingPoints(_oldShowCheckBox);

		HowToStabilizeGroupBox->Visible = true;
		WhereToAnchorGroupBox->Visible = true;
		WhatToStabilizeGroupBox->Visible = true;

		RenderParametersGroupBox->Visible = true;
		AutoModeRenderParametersGroupBox->Visible = false;
	}
}
// ---------------------------------------------------------------------------

void __fastcall TStabilizerForm::ManualAutoTabControlChange(TObject *Sender)
{
	String selectedTabString = ManualAutoTabControl->Tabs->Strings[ManualAutoTabControl->TabIndex];
	GStabilizerTool->setIsInAutoTrack(selectedTabString == L"Auto");
	updateAutoManualGuiDisplay();
	UpdateExecutionButtons(TOOL_CONTROL_STATE_STOPPED, false, NOT_TRACKING);
	GStabilizerTool->ResetTracking();
	if (GStabilizerTool->isInAutoTrack())
	{
		GStabilizerTool->getSystemAPI()->DeactivateTrackingTool();
	}
	else
	{
		GStabilizerTool->getSystemAPI()->ActivateTrackingTool();
	}
}
//---------------------------------------------------------------------------
void __fastcall TStabilizerForm::TrackingGraphClick(TObject *Sender)
{
   _doubleClickedOnHTrackGraph = false;
//   TRACE_0(errout << "CLICK");
}
//---------------------------------------------------------------------------

void __fastcall TStabilizerForm::TrackingGraphDblClick(TObject *Sender)
{
   _doubleClickedOnHTrackGraph = true;
//   TRACE_0(errout << "DOUBLE CLICK ");
}
//---------------------------------------------------------------------------

void __fastcall TStabilizerForm::TrackingGraphClickSeries(TCustomChart *Sender, TChartSeries *Series,
          int ValueIndex, TMouseButton Button, TShiftState Shift, int X,
          int Y)
{
   // To make sense of this code to detect double click on a graph point,
   // here is the sequence of events:
   //
   // MouseDown, ClickSeries, Click, MouseUp, DoubleClick, MouseDown, ClickSeries, MouseUp
   //
   // Note that the Click comes AFTER the ClickSeries but the DoubleClick
   // fortunately comes BEFORE the Click Series, so I hope this sequence is
   // something I can count on.

//   TRACE_0(errout << "CLICK SERIES");
   if (_doubleClickedOnHTrackGraph == false)
   {
      return;
   }

   _doubleClickedOnHTrackGraph = false;
//   TRACE_0(errout << "                  frame offset " << ValueIndex);
   GStabilizerTool->NeutralizeTrackingPointAtOffset(ValueIndex);
}
//---------------------------------------------------------------------------

void __fastcall TStabilizerForm::TrackingGraphMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
//   TRACE_0(errout << "MOUSE DOWN");
}
//---------------------------------------------------------------------------

void __fastcall TStabilizerForm::TrackingGraphMouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
   _doubleClickedOnHTrackGraph = false;
//   TRACE_0(errout << "MOUSE UP");
}
//---------------------------------------------------------------------------

void __fastcall TStabilizerForm::AdvancedTrackingParameterChanged(TObject *Sender)
{
	if (GStabilizerTool == nullptr || AdvancedTrackingSettingsPopup == nullptr)
		return;

   // NOTE: UseLut is now always on (the contol is checked and invisible).
   ETrackingChannel trackingChannel = AdvancedTrackingSettingsPopup->TrackRedButton->Down
                                       ? ETrackingChannel::R
                                       : (AdvancedTrackingSettingsPopup->TrackGreenButton->Down
                                          ? ETrackingChannel::G
                                          : (AdvancedTrackingSettingsPopup->TrackBlueButton->Down
                                             ? ETrackingChannel::B
											 : ETrackingChannel::Y));
   bool degrainSetting = AdvancedTrackingSettingsPopup->TrackDegrainCheckBox->Checked;
   bool useLutSetting = true;  //AdvancedTrackingSettingsPopup->TrackUsingLUTCheckBox->Checked;
   bool autoContrastSetting = AdvancedTrackingSettingsPopup->TrackAutoContrastCheckBox->Checked;

	GStabilizerTool->SetAdvancedTrackingParameters(trackingChannel, degrainSetting, useLutSetting, autoContrastSetting);
   bool usingAdvancedSetting =
         trackingChannel != ETrackingChannel::Y || degrainSetting || (!useLutSetting) || autoContrastSetting;

   AdvancedSettingsButtonWithWarning->Visible = usingAdvancedSetting;

	UpdatePresetCurrentParameters();
}
//---------------------------------------------------------------------------

void __fastcall TStabilizerForm::AdvancedSettingsButtonClick(TObject *Sender)
{
   if (AdvancedTrackingSettingsPopup == nullptr)
   {
      return;
   }

   auto modalResult = AdvancedTrackingSettingsPopup->ShowModal();
   switch (modalResult)
   {
      case mrOk:
         TRACE_0(errout << "OK!");
         AdvancedTrackingParameterChanged(nullptr);
         break;
      default:
         TRACE_0(errout << "Cancel!");
         break;
   }
}
//---------------------------------------------------------------------------


void __fastcall TStabilizerForm::AutoSourceIsAnimationCheckBoxClick(TObject *Sender)

{
	if (GStabilizerTool->isInAutoTrack())
	{
 		InvalidateSmoothedData();
    }
}
//---------------------------------------------------------------------------

