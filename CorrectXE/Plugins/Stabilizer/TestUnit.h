//---------------------------------------------------------------------------

#ifndef TestUnitH
#define TestUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "cspin.h"
#include <ExtCtrls.hpp>
#include <Buttons.hpp>
#include <Graphics.hpp>
#include <Grids.hpp>
#include <ComCtrls.hpp>
#include <valarray>
#include <vector>
#include "HRTimer.h"
#include "TrackingPoints.h"
#include "StabilizerParameters.h"

typedef unsigned short Pixel;
typedef Pixel * Image;

//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TCSpinEdit *StartSpinEdit;
        TCSpinEdit *EndSpinEdit;
        TLabel *Label1;
        TLabel *Label2;
        TBitBtn *LoadButton;
        TImage *Image1;
        TLabel *PositionLabel;
        TMemo *TrackPointMemo;
        TButton *TrackButton;
        TLabel *FrameLabel;
        TBitBtn *BackButton;
        TButton *SmoothButton;
        TButton *Stabilizer1Button;
        TBitBtn *SavePointsButton;
        TBitBtn *LoadPointsButton;
        TBitBtn *ClearAllButton;
        TBitBtn *NextFrameButton;
        TStatusBar *StatusBar1;
        TBitBtn *GotoInButton;
        TBitBtn *ClearSmoothButton;
        TBitBtn *CheckSolutionButton;
        TButton *SwapButton;
        TLabel *TypeLabel;
        TBitBtn *BitBtn1;
        TButton *StabilizerAllButton;
        TCSpinEdit *GoodInSpinEdit;
        TCSpinEdit *GoodOutSpinEdit;
        TLabel *Label3;
        TLabel *Label4;
        TEdit *AlphaEdit;
        TBitBtn *BitBtn2;
        TCSpinEdit *TrackSEdit;
        TLabel *Label5;
        TCSpinEdit *SearchSEdit;
        TLabel *Label6;
        TLabel *Label7;
        TComboBox *UFunctionComboBox;
        TLabel *Label8;
        TComboBox *FileNameEdit;
        TLabel *Label9;
        TTrackBar *SmoothTrackBar;
        TLabel *Label10;
        TLabel *Label11;
        void __fastcall LoadButtonClick(TObject *Sender);
        void __fastcall Image1MouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
        void __fastcall Image1MouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
        void __fastcall TrackButtonClick(TObject *Sender);
        void __fastcall BackButtonClick(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall SmoothButtonClick(TObject *Sender);
        void __fastcall Stabilizer1ButtonClick(TObject *Sender);
        void __fastcall SavePointsButtonClick(TObject *Sender);
        void __fastcall LoadPointsButtonClick(TObject *Sender);
        void __fastcall ClearAllButtonClick(TObject *Sender);
        void __fastcall NextFrameButtonClick(TObject *Sender);
        void __fastcall GotoInButtonClick(TObject *Sender);
        void __fastcall ClearSmoothButtonClick(TObject *Sender);
        void __fastcall CheckSolutionButtonClick(TObject *Sender);
        void __fastcall SwapButtonClick(TObject *Sender);
        void __fastcall BitBtn1Click(TObject *Sender);
        void __fastcall StabilizerAllButtonClick(TObject *Sender);
        void __fastcall Image1MouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall BitBtn2Click(TObject *Sender);
        void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
        void __fastcall TrackSEditChange(TObject *Sender);
        void __fastcall SearchSEditChange(TObject *Sender);
        void __fastcall UFunctionComboBoxChange(TObject *Sender);
        void __fastcall FileNameEditDropDown(TObject *Sender);
        void __fastcall FileNameEditExit(TObject *Sender);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall SmoothTrackBarChange(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall FormKeyPress(TObject *Sender, char &Key);
private:	// User declarations
        int nPicRow;
        int nPicCol;
        int _CurrentFrame;
        int _BaseFrame;
        bool _OneToOneMode;
        int _XOneToOne;
        int _YOneToOne;

        Image _RawData[2];
        Image _RGBData[2];
        Image _FixedRGBData[2];
        int _CurrentImageIndex;
        int _NextImageIndex;
        int _MoveSmoothPoint;
        int _MoveLine;

        // These are just preallocated to speed up searches
        int _TrackBoxExtent;      //  the "extent" from the center point of the track box
        int _TrackBoxSize;        //  Always 2*_TrackBoxExtent+1
        int _TrackBoxPoints;      //  Always (_TrackBoxSize)^2
        Image _TrackBoxData;      //  Temp storage to hold box for tracking

        int _SearchBoxExtent;     //  the "extent" from the center point of the track box
        int _SearchBoxSize;       //  Always 2*_SearchBoxExtent+1
        int _SearchBoxPoints;     //  Always (_SearchBoxSize)^2
        Image _SearchBoxData;     //  Temp storage to hold box for tracking

        RECT _BoundingBox;       // Area to stabilize
        RECT _StabilizerBox;     // Largest rectange stabilized over all frames

        Image _BaseImage;
        Image _NextImage;
        String _CurrentFileName;
        bool _isStabilizering;
        CHRTimer _HRT;
        CMRU _MRU;

        Graphics::TBitmap *pBitmap;
        Graphics::TBitmap *pOrigBitmap;
        Graphics::TBitmap *pStabilizerBitmap;
        Graphics::TBitmap *pMagnifyBitmap;
        int VCol(int X);
        int VRow(int Y);
        int ICol(int X);
        int IRow(int Y);
        CTrackingArray _TrackingArray;
        CTrackingArray _SmoothedArray;

        void UpdateTrackMemo(void);
        void UpdateDisplay(void);
        bool LoadImage(String fname, int Frame);
        bool ReadDPXM(String sfname, int nStartFrame);
        bool WriteDPXM(String sfname, Image &imgA, int nStartFrame);

        void BitmapFromImage(Graphics::TBitmap *Bitmap, Image pImage, Image mImage);
        int L1BoxNorm(const DPOINT &tp, int iO, int jO);
        DPOINT FindBestFirstMatch(const DPOINT &tp);
        DPOINT FindBestRefinedMatch(const DPOINT &dpB, Pixel *pBase, const DPOINT &dpN, Pixel *pNext);
        void CreateSubPixelMatrix(Pixel *pIn, const DPOINT &dp, int N, Pixel *pOut);
        void TrackNextFrame(void);
        double BiInterpolate(double x, double y, int iC, int iR);
        bool StabilizeFrame(int Frame);
        bool ComputeBoundingBox(DRECT &rBox);

        bool SavePoints(void);
        bool ReadPoints(void);
        void Magnify(int x, int y);
        void FillOutsideBlack(RECT &Box, MTI_UINT16 *pOut);
        void StabilizeImage(float *xVec, float *yVec, RECT &Box);
        void StabilizeImage2(float *xVec, float *yVec, RECT &Box);
        void StabilizeImage3(float *xVec, float *yVec, RECT &Box);

        int _VX;
        int _VY;
        int _pxlComponentCount;
        MTI_UINT16 _BlackPixel[3];
        MTI_UINT16 _WhitePixel[3];

        // These are used for store in the multithreading model
        // Note:  No locking is done because it is the responsibility
        // of the setup program never to overlap xVec and yVec
        float *_Wx;
        float *_Wy;
        float *_tpSX;
        float *_tpSY;
        float *_xVec;
        float *_yVec;
        int   _N0;

        double _dMotionTime;


public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
        __fastcall ~TForm1(void);
       void SetTrackBoxExtent(int nTrackBoxExtent);
       void SetSearchBoxExtent(int nSearchBoxExtent);

       CStabilizerParameters StabilizerParameters;
       bool ReadParameters(const string &strFileName);
       bool WriteParameters(const string &strFileName);
       bool StabilizerSlice(RECT &Box);
       __property CTrackingArray TrackingArray = {read=_TrackingArray};
       __property CTrackingArray SmoothedArray = {read=_SmoothedArray};


};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
