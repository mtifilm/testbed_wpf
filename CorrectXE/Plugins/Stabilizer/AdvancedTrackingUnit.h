//---------------------------------------------------------------------------

#ifndef AdvancedTrackingUnitH
#define AdvancedTrackingUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "ColorPanel.h"
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TAdvancedTrackingForm : public TForm
{
__published:	// IDE-managed Components
   TPanel *ChannelPanel;
   TSpeedButton *HilitedTrackBlueButton;
   TSpeedButton *HilitedTrackGreenButton;
   TSpeedButton *HilitedTrackRedButton;
   TSpeedButton *HilitedTrackGrayButton;
   TSpeedButton *TrackBlueButton;
   TSpeedButton *TrackGreenButton;
   TSpeedButton *TrackRedButton;
   TSpeedButton *TrackGrayButton;
   TLabel *ChannelLabel;
   TColorPanel *BackgroundPanel;
   TCheckBox *TrackAutoContrastCheckBox;
   TCheckBox *TrackDegrainCheckBox;
   TCheckBox *TrackUsingLUTCheckBox;
   TButton *OkButton;
   TButton *CancelButton;
private:	// User declarations
public:		// User declarations
   __fastcall TAdvancedTrackingForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TAdvancedTrackingForm *AdvancedTrackingForm;
//---------------------------------------------------------------------------
#endif
