object StabilizerPreferencesForm: TStabilizerPreferencesForm
  Left = 2205
  Top = 113
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Stabilization Preferences'
  ClientHeight = 188
  ClientWidth = 352
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 12
    Top = 16
    Width = 329
    Height = 117
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 12
      Width = 172
      Height = 13
      Caption = 'When setting a new MARK IN point:'
    end
    object MoveAndCopyRadioButton: TRadioButton
      Left = 8
      Top = 32
      Width = 309
      Height = 17
      Caption = 'Always move the tracking points to the new MARK IN frame'
      TabOrder = 0
    end
    object MoveAndClearRadioButton: TRadioButton
      Left = 8
      Top = 56
      Width = 233
      Height = 17
      Caption = 'Always clear all the tracking points'
      TabOrder = 1
    end
    object PromptMeRadioButton: TRadioButton
      Left = 8
      Top = 80
      Width = 221
      Height = 17
      Caption = 'Prompt me for what action to take'
      TabOrder = 2
    end
  end
  object HR: TPanel
    Left = 0
    Top = 140
    Width = 352
    Height = 3
    BevelOuter = bvLowered
    TabOrder = 1
  end
  object OkButton: TButton
    Left = 104
    Top = 152
    Width = 75
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 2
  end
  object CancelButton: TButton
    Left = 188
    Top = 152
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
end
