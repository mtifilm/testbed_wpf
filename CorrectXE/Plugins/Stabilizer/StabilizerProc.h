#ifndef StabilizerProcH
#define StabilizerProcH

#include "StabilizerTool.h"
#include "ImageDatumConvert.h"
#include "MtiCudaCoreInterface.h"

//////////////////////////////////////////////////////////////////////
// Forward Declarations
//
class IToolProgressMonitor;
class CZoomFractional;

//////////////////////////////////////////////////////////////////////
//
class CStabilizerProc : public CToolProcessor
{
public:
   CStabilizerProc(int newToolNumber, const string &newToolName,
                   CToolSystemInterface *newSystemAPI,
                   const bool *newEmergencyStopFlagPtr,
                   IToolProgressMonitor *newToolProgressMonitor);
   virtual ~CStabilizerProc();

   CHRTimer HRT;
   int SetIOConfig(CToolIOConfig *toolIOConfig);
   int SetFrameRange(CToolFrameRange *toolFrameRange);
   bool StabilizerSlice(RECT Box);

   CCountingWait CountingWait;

private:
   // Tool's functions called from within context of the processing loop.
   int SetParameters(CToolParameters *toolParams);
   int BeginProcessing(SToolProcessingData &procData);
   int EndProcessing(SToolProcessingData &procData);
   int BeginIteration(SToolProcessingData &procData);
   int EndIteration(SToolProcessingData &procData);
   int DoProcess(SToolProcessingData &procData);
   int GetIterationCount(SToolProcessingData &procData);

	std::pair<float, float> getStabilizationDeltas(int frameIndex);
	vector<vector<float>>getStabilizationRotationWeights(int frameIndex);

   int StabilizeImageGpuAsync(int frameIndex, Ipp16uArray &imageToProcess, CudaResampler16u *resampler);
   int FillMissingData(int frameIndex, Ipp16uArray &&imageToProcessFull, const Ipp16uArray &imageFullFill);
   vector<Ipp16uArray> SaveMissingDataForFrame(int frameIndex, const Ipp16uArray &imageFill);

private:

   CZoomFractional *Zoomer;

   int _inFrameIndex;
   int _outFrameIndex;
   int _iterationCount;

	bool _useGpu = false;

   CudaResampler16u  *_currentResampler = nullptr;
   CudaResampler16u  *_nextResampler = nullptr;

   // Used for rotation
   CudaThinplateSpline16u *_thinplateSplineRotator = nullptr;

   // These are used for store in the multithreading model
	// Note:  No locking is done because it is the responsibility
   // of the setup program never to overlap xVec and yVec
   float *_Wx;
   float *_Wy;
   float *_tpSX;
   float *_tpSY;
   float *_xVec;
   float *_yVec;
   int   _N0;

   // Some timing information
   double _dMotionTime;
   double _dStabilizerTime;

   RECT _BoundingBox;       // Area to stabilize
   RECT _ActiveBox;
   DRECT _StabilizerBox;    // Largest rectange stabilized over all frames
   IntegerList _AnchorList;
   
   // variables to support multithreading of algorithm
   int _processingThreadCount = STAB_MAX_THREAD_COUNT;

   // Mask Region
   CRegionOfInterest *maskROI;
   CStabilizerParameters _stabilizerParameters;    // private copy of parameters
   CStabilizerTool *_stabilizerToolObject;  // pointer to CStabilizerTool object [EVIL!!!!]

   MTI_UINT16 *_BaseOrigImage = nullptr;
   MTI_UINT16 *_nextOrigImage = nullptr;
   MTI_UINT16 *_outputImage = nullptr;
   MTI_UINT16 *_previousFixedImage = nullptr;

	bool _imageDataIsHalfs = false;
   int nPicCol;
   int nPicRow;
   int _pxlComponentCount;
   MTI_UINT16 _BlackPixel[3];
   MTI_UINT16 _WhitePixel[3];

   int BiInterpolate(double x, double y, int iC, int iR);
   int NearestNeighborInterpolate(double x, double y, int iC, int iR);
   bool StabilizeFrame(int Frame);
	void StabilizeFrameWithLanczos(int frameIndex);
   void StabilizeFrameWithLanczosGpu(int frameIndex);
   bool StabilizeWithRotationViaLanczosGpu(int frameIndex);
	void StabilizeFrameIntegralPixel(int frameIndex);
	void FillOutsideBlack(RECT &Box, MTI_UINT16 *pOut);
   void StabilizeImage(float *xVec, float *yVec, RECT &Box);

	inline float imageDatumToFloat(unsigned short datum)
	{
		return ImageDatum::toFloat(datum, _imageDataIsHalfs);
	}

	inline unsigned short floatToImageDatum(float f)
	{
		return ImageDatum::fromFloat(f, _imageDataIsHalfs);
	}
};
//////////////////////////////////////////////////////////////////////

#endif // !defined(STABILIZERPROC_H)

