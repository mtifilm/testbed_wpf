//---------------------------------------------------------------------------


#pragma hdrstop

#include "StabilizerParameters.h"
#include "PDL.h"
#pragma package(smart_init)

static const SIniAssociation UFunctionData[] =
{
	{"U_Fcn_Euclidean", U_FUNCTION_TYPE_LINEAR},
	{"U_Fcn_Logrithmic", U_FUNCTION_TYPE_R_LOGR},
	{"U_Fcn_Thin_Plate", U_FUNCTION_TYPE_R2_LOGR},
	{"U_Fcn_None", U_FUNCTION_TYPE_NONE},
	{"", -1}
};

static const SIniAssociation MissingFillData[] =
{
	{"MISSING_DATA_White",MISSING_DATA_WHITE},
	{"MISSING_DATA_Black", MISSING_DATA_BLACK},
	{"MISSING_DATA_Previous_Frame", MISSING_DATA_PREVIOUS},
    {"MISSING_DATA_Zoom", MISSING_DATA_ZOOM},
	{"", -1}
};

static const SIniAssociation StabModeData[] =
{
	{"STABMODE_None",STABMODE_NONE},
	{"STABMODE_Smoothed", STABMODE_SMOOTHED},
	{"STABMODE_Flat", STABMODE_FLAT},
    {"STABMODE_Registered", STABMODE_REGISTERED},
	{"", -1}
};

static const SIniAssociation TrackingChannelData[] =
{
	{"R", ETrackingChannel::R},
	{"G", ETrackingChannel::G},
	{"B", ETrackingChannel::B},
	{"Y", ETrackingChannel::Y},
    {"", -1}
};

//

CIniAssociations UFunctionAssociations(UFunctionData, "U_Fcn_");
CIniAssociations UMissingDataAssociations(MissingFillData, "MISSING_DATA");
CIniAssociations UStabModeAssociations(StabModeData, "STABMODE");
CIniAssociations UTrackingChannelAssociations(TrackingChannelData, "TRACKCHAN");

//---------------------------------------------------------------------------

CStabilizerParameters::CStabilizerParameters()
{
}

CStabilizerParameters::CStabilizerParameters(const CStabilizerParameters &rhs)
{
	*this = rhs;
}

//----------------------ReadParameters-----------------------------April 2006---

	 void CStabilizerParameters::ReadParameters(const string &strFileName)

//  Reads in the parametet from the ini file of name strFileName
//    return true if it succeeds
//           false on failure
//
//******************************************************************************
{
   CIniFile *IniFile = CreateIniFile(strFileName);
   if (IniFile == NULL)
      {
         TRACE_0(errout << "ERROR: CStabilizerParameters::ReadParameters: Could not read "
                     << strFileName << endl
                     << "       Because " << theError.getMessage());
			return;
      }

	ReadParameters(IniFile, "Stabilizer");

	// No presets in AutoTrack mode, so just make sure its off.
   AutoTrackingMode = false;

   if (!DeleteIniFile(IniFile))
     {
         TRACE_0(errout << "ERROR: CStabilizerParameters::ReadParameters: Could not close "
                     << strFileName << endl
                     << "       Because " << theError.getMessage());

			return;
     }
}

//----------------------WriteParameters----------------------------April 2006---

	 void CStabilizerParameters::WriteParameters(const string &strFileName) const

//  Write out the parametet to the ini file of name strFileName
//    return true if it succeeds
//           false on failure
//
//******************************************************************************
{
	CIniFile *IniFile = CreateIniFile(strFileName);
   if (IniFile == NULL)
      {
         TRACE_0(errout << "ERROR: CStabilizerParameters::WriteParameters: Could not read "
                     << strFileName << endl
                     << "       Because " << theError.getMessage());
			return;
      }

	WriteParameters(IniFile, "Stabilizer");

   if (!DeleteIniFile(IniFile))
     {
         TRACE_0(errout << "ERROR: CStabilizerParameters::WriteParameters: Could not close "
                     << strFileName << endl
                     << "       Because " << theError.getMessage());

			return;
     }
}

//----------------ReadSettings---------------------John Mertus-----Jan 2001---

	void CStabilizerParameters::ReadParameters(CIniFile *ini, const string &IniSection)

//  This reads the Size and Positions properties from the ini file
//   ini is the ini file
//   IniSection is the IniSection
//
//  Return should be true
//
//  Note: This function is designed to be overridden in the derived class
//
//******************************************************************************
{
	AutoTrackingMode = false; ////ini->ReadBool(IniSection, "AutoTrackingMode", AutoTrackingMode);
	AutoSourceIsAnimation = ini->ReadInteger(IniSection, "AutoSourceIsAnimation", AutoSourceIsAnimation);
	TrackBoxExtentX = ini->ReadInteger(IniSection, "TrackBoxExtentX", TrackBoxExtentX);
   TrackBoxExtentY = ini->ReadInteger(IniSection, "TrackBoxExtentY", TrackBoxExtentY);
   SearchBoxExtentLeft = ini->ReadInteger(IniSection, "SearchBoxExtentLeft", SearchBoxExtentLeft);
   SearchBoxExtentRight = ini->ReadInteger(IniSection, "SearchBoxExtentRight", SearchBoxExtentRight);
   SearchBoxExtentUp = ini->ReadInteger(IniSection, "SearchBoxExtentUp", SearchBoxExtentUp);
   SearchBoxExtentDown = ini->ReadInteger(IniSection, "SearchBoxExtentDown", SearchBoxExtentDown);
   GoodFramesIn = ini->ReadInteger(IniSection, "GoodFramesIn", GoodFramesIn);
   GoodFramesOut = ini->ReadInteger(IniSection, "GoodFramesOut", GoodFramesOut);
   RefFrameIndex = ini->ReadInteger(IniSection, "RefFrameIndex", RefFrameIndex);
   Jitter = ini->ReadDouble(IniSection, "Jitter", Jitter);
   Alpha2 = ini->ReadDouble(IniSection, "Alpha2", Alpha2);
   HMode = (EStabilizationMode)ini->ReadAssociation(UStabModeAssociations, IniSection, "HMode", HMode);
   VMode = (EStabilizationMode)ini->ReadAssociation(UStabModeAssociations, IniSection, "VMode", VMode);
   RMode = (EStabilizationMode)ini->ReadAssociation(UStabModeAssociations, IniSection, "RMode", RMode);
   UFunction = (EUFunctionType)ini->ReadAssociation(UFunctionAssociations, IniSection, "UFunction", UFunction);
   MissingDataFill = (EMissingDataFillType)ini->ReadAssociation(UMissingDataAssociations, IniSection, "MissingDataFill", MissingDataFill);
	UseMatting = ini->ReadBool(IniSection, "UseMatting", UseMatting);
	KeepOriginalMatValues = ini->ReadBool(IniSection, "KeepOriginalMatValues", KeepOriginalMatValues);
   DisableSubPixelInterpolation = ini->ReadBool(IniSection, "DisableSubPixelInterpolation", DisableSubPixelInterpolation);
   ApplyLutBeforeTracking = ini->ReadBool(IniSection, "ApplyLutBeforeTracking", ApplyLutBeforeTracking);
   DegrainTrackingImage = ini->ReadBool(IniSection, "DegrainTrackingImage", DegrainTrackingImage);
   TrackingChannel = (ETrackingChannel)ini->ReadAssociation(UTrackingChannelAssociations, IniSection, "TrackingChannel", TrackingChannel);
}

//----------------WriteSettings----------------John Mertus-----Jan 2001---

	void CStabilizerParameters::WriteParameters(CIniFile *ini, const string &IniSection) const

//  This writes the Size and Positions properties from the ini file
//   ini is the ini file
//   IniSection is the IniSection
//
//  Return should be true
//
//  Note: This function is designed to be overridden in the derived class
//
//******************************************************************************
{
	ini->WriteBool(IniSection, "AutoTrackingMode", false); //// AutoTrackingMode);
	ini->WriteInteger(IniSection, "AutoSourceIsAnimation", AutoSourceIsAnimation);
	ini->WriteInteger(IniSection, "TrackBoxExtentX", TrackBoxExtentX);
   ini->WriteInteger(IniSection, "TrackBoxExtentY", TrackBoxExtentY);
   ini->WriteInteger(IniSection, "SearchBoxExtentLeft", SearchBoxExtentLeft);
   ini->WriteInteger(IniSection, "SearchBoxExtentRight", SearchBoxExtentRight);
   ini->WriteInteger(IniSection, "SearchBoxExtentUp", SearchBoxExtentUp);
   ini->WriteInteger(IniSection, "SearchBoxExtentDown", SearchBoxExtentDown);
   ini->WriteInteger(IniSection, "GoodFramesIn", GoodFramesIn);
   ini->WriteInteger(IniSection, "GoodFramesOut", GoodFramesOut);
   ini->WriteInteger(IniSection, "RefFrameIndex", RefFrameIndex);
	ini->WriteDouble(IniSection, "Jitter", Jitter);
	ini->WriteDouble(IniSection, "Alpha2", Alpha2);
   ini->WriteAssociation(UStabModeAssociations, IniSection, "HMode", HMode);
	ini->WriteAssociation(UStabModeAssociations, IniSection, "VMode", VMode);
   ini->WriteAssociation(UStabModeAssociations, IniSection, "RMode", RMode);
   ini->WriteAssociation(UFunctionAssociations, IniSection, "UFunction", UFunction);
   ini->WriteAssociation(UMissingDataAssociations, IniSection, "MissingDataFill", MissingDataFill);
   ini->WriteBool(IniSection, "UseMatting", UseMatting);
   ini->WriteBool(IniSection, "KeepOriginalMatValues", KeepOriginalMatValues);
   ini->WriteBool(IniSection, "DisableSubPixelInterpolation", DisableSubPixelInterpolation);
   ini->WriteBool(IniSection, "ApplyLutBeforeTracking", ApplyLutBeforeTracking);
   ini->WriteBool(IniSection, "DegrainTrackingImage", DegrainTrackingImage);

   ini->WriteAssociation(UTrackingChannelAssociations, IniSection, "TrackingChannel", TrackingChannel);
}
//---------------------------------------------------------------------------

bool CStabilizerParameters::AreParametersTheSame(const IPresetParameters &presetParameter) const
{
	auto rhs = (CStabilizerParameters *)(&presetParameter);

   // NOTE: WE DO NOT COMPARE THE Tag!

	bool retVal =
		AutoTrackingMode == rhs->AutoTrackingMode
		&& AutoSourceIsAnimation == rhs->AutoSourceIsAnimation
		&& TrackBoxExtentX == rhs->TrackBoxExtentX
		&& TrackBoxExtentY == rhs->TrackBoxExtentY
		&& SearchBoxExtentLeft == rhs->SearchBoxExtentLeft
		&& SearchBoxExtentRight == rhs->SearchBoxExtentRight
		&& SearchBoxExtentUp == rhs->SearchBoxExtentUp
		&& SearchBoxExtentDown == rhs->SearchBoxExtentDown
		&& GoodFramesIn == rhs->GoodFramesIn
		&& GoodFramesOut == rhs->GoodFramesOut
		&& RefFrameIndex == rhs->RefFrameIndex
		&& std::fabs(Jitter - rhs->Jitter) < 0.000001
//		&& std::fabs(Alpha2 - rhs->Alpha2) < 0.000001
		&& HMode == rhs->HMode
		&& VMode == rhs->VMode
		&& RMode == rhs->RMode
		&& UFunction == rhs->UFunction
		&& MissingDataFill == rhs->MissingDataFill
		&& UseMatting == rhs->UseMatting
		&& KeepOriginalMatValues == rhs->KeepOriginalMatValues
		&& DisableSubPixelInterpolation == rhs->DisableSubPixelInterpolation
      && ApplyLutBeforeTracking == rhs->ApplyLutBeforeTracking
      && DegrainTrackingImage == rhs->DegrainTrackingImage
      && TrackingChannel == rhs->TrackingChannel;

	if (retVal && (std::fabs(Alpha2 - rhs->Alpha2) >= 0.000001))
	{
		retVal = false;
	}

	if (!retVal)
	{
		static double foo = 0.0;
		double bar = std::fabs(Alpha2 - rhs->Alpha2);
		foo = bar;
	}

	return retVal;
}
//---------------------------------------------------------------------------

bool CStabilizerParameters::operator == (const CStabilizerParameters &rhs) const
{
	return AreParametersTheSame(rhs);
}
//---------------------------------------------------------------------------

bool CStabilizerParameters::operator != (const CStabilizerParameters &rhs) const
{
	return !(*this == rhs);
}
//---------------------------------------------------------------------------

CStabilizerParameters &CStabilizerParameters::operator = (const CStabilizerParameters &rhs)
{
	if (this == &rhs)
	{
		return *this;
	}

	AutoTrackingMode = rhs.AutoTrackingMode;
	AutoSourceIsAnimation = rhs.AutoSourceIsAnimation;
	TrackBoxExtentX = rhs.TrackBoxExtentX;
	TrackBoxExtentY = rhs.TrackBoxExtentY;
	SearchBoxExtentLeft = rhs.SearchBoxExtentLeft;
	SearchBoxExtentRight = rhs.SearchBoxExtentRight;
	SearchBoxExtentUp = rhs.SearchBoxExtentUp;
	SearchBoxExtentDown = rhs.SearchBoxExtentDown;
	GoodFramesIn = rhs.GoodFramesIn;
	GoodFramesOut = rhs.GoodFramesOut;
	RefFrameIndex = rhs.RefFrameIndex;
	Jitter = rhs.Jitter;
	Alpha2 = rhs.Alpha2;
	HMode = rhs.HMode;
	VMode = rhs.VMode;
	RMode = rhs.RMode;
	UFunction = rhs.UFunction;
	MissingDataFill = rhs.MissingDataFill;
	UseMatting = rhs.UseMatting;
	KeepOriginalMatValues = rhs.KeepOriginalMatValues;
	DisableSubPixelInterpolation = rhs.DisableSubPixelInterpolation;
   ApplyLutBeforeTracking = rhs.ApplyLutBeforeTracking;
   DegrainTrackingImage = rhs.DegrainTrackingImage;
   TrackingChannel = rhs.TrackingChannel;
	Tag = rhs.Tag;

	return *this;
}
//---------------------------------------------------------------------------

void CStabilizerParameters::ReadPDLEntry(CPDLElement *pdlToolAttribs)
{
   // Read Stabilizer Parameters from a PDL Entry's Tool Attributes

	AutoTrackingMode = pdlToolAttribs->GetAttribBool("AutoTrackingMode", AutoTrackingMode);
	AutoSourceIsAnimation = pdlToolAttribs->GetAttribBool("AutoSourceIsAnimation", AutoSourceIsAnimation);
	TrackBoxExtentX = pdlToolAttribs->GetAttribInteger("TrackBoxExtentX", TrackBoxExtentX);
	TrackBoxExtentY = pdlToolAttribs->GetAttribInteger("TrackBoxExtentY", TrackBoxExtentY);
	SearchBoxExtentLeft = pdlToolAttribs->GetAttribInteger("SearchBoxExtentLeft", SearchBoxExtentLeft);
	SearchBoxExtentRight = pdlToolAttribs->GetAttribInteger("SearchBoxExtentRight", SearchBoxExtentRight);
	SearchBoxExtentUp = pdlToolAttribs->GetAttribInteger("SearchBoxExtentUp", SearchBoxExtentUp);
	SearchBoxExtentDown = pdlToolAttribs->GetAttribInteger("SearchBoxExtentDown", SearchBoxExtentDown);
	GoodFramesIn = pdlToolAttribs->GetAttribInteger("GoodFramesIn", GoodFramesIn);
	GoodFramesOut = pdlToolAttribs->GetAttribInteger("GoodFramesOut", GoodFramesOut);
	RefFrameIndex = pdlToolAttribs->GetAttribInteger("RefFrameIndex", RefFrameIndex);
	Jitter = pdlToolAttribs->GetAttribDouble("Jitter", Jitter);
	Alpha2 = pdlToolAttribs->GetAttribDouble("Alpha2", Alpha2);

   string valueStr, defaultStr;
   int newValue;

   defaultStr = UStabModeAssociations.SymbolicName(HMode);
   valueStr = pdlToolAttribs->GetAttribString("HMode", defaultStr);
   if (UStabModeAssociations.ValueBySymbolicName(valueStr, newValue))
      HMode = (EStabilizationMode)newValue;

   defaultStr = UStabModeAssociations.SymbolicName(VMode);
   valueStr = pdlToolAttribs->GetAttribString("VMode", defaultStr);
   if (UStabModeAssociations.ValueBySymbolicName(valueStr, newValue))
      VMode = (EStabilizationMode)newValue;

   defaultStr = UStabModeAssociations.SymbolicName(RMode);
   valueStr = pdlToolAttribs->GetAttribString("RMode", defaultStr);
   if (UStabModeAssociations.ValueBySymbolicName(valueStr, newValue))
      RMode = (EStabilizationMode)newValue;

   defaultStr = UFunctionAssociations.SymbolicName(UFunction);
   valueStr = pdlToolAttribs->GetAttribString("UFunction", defaultStr);
   if (UFunctionAssociations.ValueBySymbolicName(valueStr, newValue))
      UFunction = (EUFunctionType)newValue;

   defaultStr = UMissingDataAssociations.SymbolicName(MissingDataFill);
   valueStr = pdlToolAttribs->GetAttribString("MissingDataFill", defaultStr);
   if (UMissingDataAssociations.ValueBySymbolicName(valueStr, newValue))
      MissingDataFill = (EMissingDataFillType)newValue;

	UseMatting = pdlToolAttribs->GetAttribBool("UseMatting", UseMatting);
   KeepOriginalMatValues = pdlToolAttribs->GetAttribBool("KeepOriginalMatValues",
                                                         KeepOriginalMatValues);
   DisableSubPixelInterpolation = pdlToolAttribs->GetAttribBool(
         "DisableSubPixelInterpolation",
         DisableSubPixelInterpolation);

   ApplyLutBeforeTracking = pdlToolAttribs->GetAttribBool("ApplyLutBeforeTracking", ApplyLutBeforeTracking);
   DegrainTrackingImage = pdlToolAttribs->GetAttribBool("DegrainTrackingImage", DegrainTrackingImage);

   defaultStr = UTrackingChannelAssociations.SymbolicName(TrackingChannel);
   valueStr = pdlToolAttribs->GetAttribString("TrackingChannel", defaultStr);
   if (UTrackingChannelAssociations.ValueBySymbolicName(valueStr, newValue))
      TrackingChannel = (ETrackingChannel)newValue;
}

void CStabilizerParameters::WritePDLEntry(CPDLElement &parent)
{
   // Write Stabilizer Parameters to a PDL Entry's Tool Attributes

   CPDLElement *pdlToolAttribs = parent.MakeNewChild("StabilizerParameters");

	pdlToolAttribs->SetAttribBool("AutoTrackingMode", AutoTrackingMode);
	pdlToolAttribs->SetAttribBool("AutoSourceIsAnimation", AutoSourceIsAnimation);
	pdlToolAttribs->SetAttribInteger("TrackBoxExtentX", TrackBoxExtentX);
	pdlToolAttribs->SetAttribInteger("TrackBoxExtentY", TrackBoxExtentY);
   pdlToolAttribs->SetAttribInteger("SearchBoxExtentLeft", SearchBoxExtentLeft);
   pdlToolAttribs->SetAttribInteger("SearchBoxExtentRight", SearchBoxExtentRight);
   pdlToolAttribs->SetAttribInteger("SearchBoxExtentUp", SearchBoxExtentUp);
   pdlToolAttribs->SetAttribInteger("SearchBoxExtentDown", SearchBoxExtentDown);
   pdlToolAttribs->SetAttribInteger("GoodFramesIn", GoodFramesIn);
   pdlToolAttribs->SetAttribInteger("GoodFramesOut", GoodFramesOut);
   pdlToolAttribs->SetAttribInteger("RefFrameIndex", RefFrameIndex);
   pdlToolAttribs->SetAttribDouble("Jitter", Jitter);
   pdlToolAttribs->SetAttribDouble("Alpha2", Alpha2);
   string HModeStr = UStabModeAssociations.SymbolicName(HMode);
   pdlToolAttribs->SetAttribString("HMode", HModeStr);
   string VModeStr = UStabModeAssociations.SymbolicName(VMode);
   pdlToolAttribs->SetAttribString("VMode", VModeStr);
   string RModeStr = UStabModeAssociations.SymbolicName(RMode);
   pdlToolAttribs->SetAttribString("RMode", RModeStr);
   string UFunctionStr = UFunctionAssociations.SymbolicName(UFunction);
   pdlToolAttribs->SetAttribString("UFunction", UFunctionStr);
   string MissingDataFillStr = UMissingDataAssociations.SymbolicName(MissingDataFill);
   pdlToolAttribs->SetAttribString("MissingDataFill", MissingDataFillStr);
   pdlToolAttribs->SetAttribBool("UseMatting", UseMatting);
	pdlToolAttribs->SetAttribBool("KeepOriginalMatValues", KeepOriginalMatValues);
	pdlToolAttribs->SetAttribBool("DisableSubPixelInterpolation", DisableSubPixelInterpolation);
   pdlToolAttribs->SetAttribBool("ApplyLutBeforeTracking", ApplyLutBeforeTracking);
   pdlToolAttribs->SetAttribBool("DegrainTrackingImage", DegrainTrackingImage);
   string TrackChanStr = UTrackingChannelAssociations.SymbolicName(TrackingChannel);
   pdlToolAttribs->SetAttribString("TrackingChannel", TrackChanStr);
}
