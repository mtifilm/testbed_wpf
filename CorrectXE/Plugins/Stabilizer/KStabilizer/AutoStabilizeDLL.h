
#ifndef AUTO_STABILIZE_DLL_H
#define AUTO_STABILIZE_DLL_H

/*
    This header file defines the export/import macros
    required to build a DLL.

    If AUTOSTABILIZEDLL_EXPORTS is defined, exports are defined.

    If not, imports are defined.

    AUTOSTABILIZEDLL_EXPORTS should be defined as a compiler
    option when building the DLL.

    AUTOSTABILIZEDLL_EXPORTS should not be defined as a compiler
    option when building the executable.
*/


#ifdef AUTOSTABILIZEDLL_EXPORTS

  #define AUTO_STABILIZE_API extern "C" __declspec(dllexport)

#else

  #define AUTO_STABILIZE_API [System::Runtime::InteropServices::DllImport("AutoStabilizeDLL.dll")] extern "C"

#endif


#endif
