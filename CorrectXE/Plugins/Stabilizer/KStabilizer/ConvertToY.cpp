
#include "stdafx.h"

#include "ConvertToY.h"
#include "ipp.h"

int ConvertToY (int nRow, int nCol, float *fpRGB, float *fpY)
{
	IppStatus status;
	IppiSize sizeImg;
	sizeImg.height = nRow;
	sizeImg.width = nCol;

	status = ippiRGBToGray_32f_C3C1R (fpRGB, 12*sizeImg.width, fpY, 4*sizeImg.width, sizeImg);

	return (int)status;
}
