#ifndef AUTOSTABILIZEH
#define AUTOSTABILIZEH

#include <vector>
using std::vector;

#include "AutoStabilizeArgs.h"
#include "Event.h"
#include "StabData.h"
#include "StabPatch.h"
#include "StabTrack.h"
#include <atomic>

/*
    The Auto Stabilize class is structured as a render engine.  The 
    caller must first allocate an engine before using it.  The caller
    must free the engine when it is no longer needed.

    The caller may make an unlimited number of calls to the engine to 
    either provide information to the engine or extract information from
    the engine.

    After allocating the engine, the caller must call FirstPass()
    for each frame in the sequence.  During these calls, the engine
    analyzes the user data and calculates the amount of correction 
    for each frame.  

    The calls to FirstPass() must be sequential.  For example, FirstPass
    must be called for frame 0 and it must return before calling 
    FirstPass for frame 1.  Etc.

*** NOTE: There is now only one pass - second analysis pass was eliminated! ***

    After passing all the frames to FirstPass(), the caller
    may call GetOffset() to retieve the offset corrections.

    This AutoStabilize class defines the following public functions:

    Alloc
        INPUT ARGUMENTS: 
            engine:      a structure with information necessary to 
                         configure the engine.

        OUTPUT ARGUMENTS:
            none

        RETURN VALUE:    a return value signifying success or failure.
                         See AutoStablizeReturns.h for a list of values.

    Free 
        INPUT ARGUMENTS:
            none

        OUTPUT ARGUMENTS:
            none

        RETURN VALUE:    an integer signifying success or failure.
                         See AutoStablizeReturns.h for a list of values.
         

    FirstPass
        INPUT ARGUMENTS:
            iFrame:      the index of the frame in the sequence.  This value
                         should range between 0 and (iNFrame-1).
            fpY:         a pointer to a frame of image data.  The data should
                         range between 0 and 1.

        OUTPUT ARGUMENTS:
            none

        RETURN VALUE:    an integer signifying success or failure.
                         See AutoStablizeReturns.h for a list of values.
                         
    GetOffset
        INPUT ARGUMENTS:
            iFrame:      the index of the frame in the sequence.  This value
                         should range between 0 and (iNFrame-1).

        OUTPUT ARGUMENTS:
            fpRowOff:    the offset in the vertical direction.  Positive
                         values mean move the image down.  Negative values
                         mean move the image up.
            fpColOff:    the offset in the horizontal direction.  Positive
                         values mean move the image right.  Negative values
                         mean move the image left.

        RETURN VALUE:    an integer signifying success or failure.
                         See AutoStablizeReturns.h for a list of values.
*/

class CAutoStabilize
{
public:
    CAutoStabilize ();
    ~CAutoStabilize ();

    int Alloc (EngineArgs engine);
    int Free ();

    // NOTE: There is now only one pass - second analysis pass was eliminated!
    int FirstPass (int iFrame, float *fpY);

    int GetOffset (int iFrame, float *fpRowOff, float *fpColOff);

private:
    CStabData Data;
    vector <CStabPatch *> Patch;
    vector <CStabTrack> TrackList;

    vector <float> listRowOff;
    vector <float> listColOff;

    CMotionBuffer *pPxyMotionBuf;
    CMotionBuffer *pFullMotionBuf;

    int iNThread;
    int iProcessFlag;
    int iNFrame;
    int iFrameExpected;
    int iFrameProcess;

    float fLockedCameraThresh;
    float fMaxCorrection;

    int AllocMotion ();
    int AppendPatches (int iTime);
    int ProcessPatches (int iTime);
    int CleanPatches (int iTime);

    int doProcessPatches (int iThread);
    int ProcessPxy (int iThread, int iPatch, int &iRow, int &iCol, float &fDiscrepancy);
    int ProcessFull (int iThread, int iPatch, int iRowPxy, int iColPxy,
        float &fRowFull, float &fColFull);
    int RoundF (float arg);
    int FitTrajectory ();
    int FindOffset ();
    void VectorMedian (vector <float> &listRow, vector <float> &listCol, float &fRowMedian, float &fColMedian);

    int iSprocThread;
    int iSprocRet;
    int iSprocTime;
    std::atomic_int iSprocRun;
    Event SprocsDone;

    friend void ProcessPatchesSproc (void *vp, void *vpReserved);
};

#endif
