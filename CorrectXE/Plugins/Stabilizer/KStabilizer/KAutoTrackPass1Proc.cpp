// ---------------------------------------------------------------------------

#pragma hdrstop

#include "KAutoTrackPass1Proc.h"
#include "StabManifests.h"
#include "ToolProgressMonitor.h"
#include "bthread.h"
#include "Clip3.h"
#include "err_tool.h"
#include "ImageFormat3.h"
#include "IniFile.h"  // for TRACE_N()
#include "MathFunctions.h"
#include "MTIKeyDef.h"
#include "MTImalloc.h"
#include "MTIsleep.h"
#include "MTIstringstream.h"
#include "IppArrayConvert.h"
#include "SysInfo.h"
#include "IpaStripeStream.h"

// ---------------------------------------------------------------------------
#pragma package(smart_init)

CKAutoTrackPass1Proc::CKAutoTrackPass1Proc(int newToolNumber, const string &newToolName,
	CToolSystemInterface *newSystemAPI, const bool *newEmergencyStopFlagPtr,
	IToolProgressMonitor *newToolProgressMonitor)
	: CToolProcessor(newToolNumber, newToolName, 1, 1, newSystemAPI, newEmergencyStopFlagPtr, newToolProgressMonitor)
{
	StartProcessThread(); // Required by Tool Infrastructure
}

///////////////////////////////////////////////////////////////////////////////

CKAutoTrackPass1Proc::~CKAutoTrackPass1Proc() {}

///////////////////////////////////////////////////////////////////////////////

int CKAutoTrackPass1Proc::SetIOConfig(CToolIOConfig *toolIOConfig)
{
	SetInputMaxFrames(0, 1, 1); // Input Port 0, 1 frame each itereation

// There may be an issue with "Modify in place" here that causes Stab to hang.
// Additional code was added to DoProcess to copy the input frame to the output
// so the screen isn't black while tracking.
//	SetOutputModifyInPlace(0, 0, false); // false = don't "clone for output"
	SetOutputNewFrame(0, 0);
//

	SetOutputMaxFrames(0, 1); // Output Port 0, just the one frame
	return 0;
}

///////////////////////////////////////////////////////////////////////////////

int CKAutoTrackPass1Proc::SetFrameRange(CToolFrameRange *toolFrameRange)
{
	CToolFrameRange::SFrameRange &frameRange = toolFrameRange->inFrameRange[0];
	_inFrameIndex = frameRange.inFrameIndex;
	_outFrameIndex = frameRange.outFrameIndex;

	return 0;
}

///////////////////////////////////////////////////////////////////////////////

int CKAutoTrackPass1Proc::SetParameters(CToolParameters *toolParams)
{
	int retVal = 0;

	CStabilizerToolParameters *stabilizerToolParams = static_cast<CStabilizerToolParameters*>(toolParams);

	_maskROI = toolParams->GetRegionOfInterest();

	// Make a private copy of the parameters for later reference
	// Caller's toolParam is deleted after this function returns
	_stabilizerParameters = stabilizerToolParams->stabilizerParameters;
	_stabilizerToolObject = stabilizerToolParams->stabilizerToolObject;

	// Compute the iteration count
	_iterationCount = _outFrameIndex - _inFrameIndex;

	const CImageFormat* imgFmt = _stabilizerToolObject->getSystemAPI()->getVideoClipImageFormat();
	if (imgFmt == nullptr)
	{
		return -1;
	}

	// Set the image size
	_imageSize = {.height = (int)imgFmt->getLinesPerFrame(), .width = (int)imgFmt->getPixelsPerLine()};

	MTI_UINT16 componentValues[3];
	imgFmt->getComponentValueMax(componentValues);
	_maxPixel = componentValues[0] + 1;

	_roi = {0, 0, _imageSize.width, _imageSize.height};
	if (_stabilizerParameters.UseMatting)
	{
		_roi = IppArrayConvert::convertToIppiRect(_stabilizerToolObject->getBoundingBox());

      // Fucking "processing region" is apparently off by one on width and height.
		++_roi.width;
		++_roi.height;
	}

	_stabilizerToolObject->setAutoTrackHackPassCompletedFlag(false);

	_autoEnginArgs = {.iNRow = _roi.height,
						  .iNCol = _roi.width,
						  .iNStride = _imageSize.width,
						  .iNFrame = _iterationCount,
						  .iNThread = SysInfo::AvailableProcessorCount(),
						  .iAnimation = _stabilizerParameters.AutoSourceIsAnimation? 1 : 0
						 };

	if (_stabilizerToolObject->autoStabilizeGlobal != nullptr)
	{
	   auto status = _stabilizerToolObject->autoStabilizeGlobal->Free();
	   if (status != 0)
	   {
			return status;
	   }

	   delete _stabilizerToolObject->autoStabilizeGlobal;
	   _stabilizerToolObject->autoStabilizeGlobal = nullptr;
	}

	_stabilizerToolObject->autoStabilizeGlobal = new CAutoStabilize();
	auto status = _stabilizerToolObject->autoStabilizeGlobal->Alloc(_autoEnginArgs);
    return status;
}

///////////////////////////////////////////////////////////////////////////////

int CKAutoTrackPass1Proc::GetIterationCount(SToolProcessingData &procData)
{
	// This function is called by the Tool Infrastructure so it knows
	// how many processing iterations to do before stopping
	// Calculate the number of iterations, depends on the state

	return _iterationCount;
}

int CKAutoTrackPass1Proc::BeginProcessing(SToolProcessingData &procData)
{
	// Initialize data
	_stabilizerToolObject->setAutoTrackingString("");
	GetToolProgressMonitor()->SetFrameCount(_outFrameIndex - _inFrameIndex);
	GetToolProgressMonitor()->SetStatusMessage("Analyzing");
	GetToolProgressMonitor()->StartProgress();

	return 0;
}

int CKAutoTrackPass1Proc::EndProcessing(SToolProcessingData &procData)
{
	int result = 0;

	// Did render run to completion or was it aborted?
	if (procData.iteration == GetIterationCount(procData))
	{
		GetToolProgressMonitor()->StopProgress(true);

      GetToolProgressMonitor()->SetStatusMessage("Analysis Complete");
      _stabilizerToolObject->setAutoTrackHackPassCompletedFlag(true);
      _stabilizerToolObject->setAutoTrackingString(getTrackPointDataString());
	}
	else
	{
		GetToolProgressMonitor()->SetStatusMessage("Auto Track Stopped");
		GetToolProgressMonitor()->StopProgress(false);
  		_stabilizerToolObject->setAutoTrackingString("");
	}

	return result;
}

int CKAutoTrackPass1Proc::BeginIteration(SToolProcessingData &procData)
{
	// List of the one frame we'll need as input/output/to be released when done
	int frameIndexList[1];
	frameIndexList[0] = _inFrameIndex + procData.iteration;

	SetInputFrames(0, 1, frameIndexList);
	SetOutputFrames(0, 1, frameIndexList);
	SetFramesToRelease(0, 1, frameIndexList);

	return 0;
}

int CKAutoTrackPass1Proc::EndIteration(SToolProcessingData &procData)
{
	GetToolProgressMonitor()->BumpProgress();
	return 0;
}

int CKAutoTrackPass1Proc::DoProcess(SToolProcessingData &procData)
{
	CToolFrameBuffer *inToolFrameBufferB;
	CToolFrameBuffer *outToolFrameBuffer;
	auto status = 0;

   CHRTimer doProcessTimer;

    MTI_UINT16* frameBits;
    inToolFrameBufferB = GetRequestedInputFrame(0, 0);
    frameBits = inToolFrameBufferB->GetVisibleFrameBufferPtr();
    // QQQ
    // We suspect ModifyInPlace is causing very intermittent hangs,
    // so we switched it to use a new output frame instead, which wastes time
    // due to having to copy the input frame to the new output frame... uggh.
    outToolFrameBuffer = GetRequestedOutputFrame(0, 0);
    // auto outputBits = outToolFrameBuffer->GetVisibleFrameBufferPtr();
    CopyInputFrameBufferToOutputFrameBuffer(inToolFrameBufferB, outToolFrameBuffer);
   /// DBTRACE(doProcessTimer);

    float *dataPointer;
    try
    {
        Ipp32fArray yArray(_imageSize);
        try
        {
            // Get normalized Y array
            // yArray = IppArrayConvert::ImportNormalizedYuvFromRgb(frameBits, _imageSize.height, _imageSize.width, _maxPixel, true);
            IpaStripeStream iss;

            iss << yArray;
            auto func = [&](Ipp32fArray slice)
            {
	            auto sliceRoi = slice.getRoi();
               auto sliceData = frameBits + 3*slice.getCols() * sliceRoi.y + sliceRoi.x;
               slice.importNormalizedYuvFromRgb(sliceData, sliceRoi.height, sliceRoi.width, _maxPixel);
            };

            doProcessTimer.Start();
            iss << efu_32f(func);  // Embarcardeo fuckup
            iss.stripe();

//            yArray.importNormalizedYuvFromRgb(frameBits, _imageSize.height, _imageSize.width, _maxPixel);
        }
        catch (...)
        {
            DBTRACE("CAUGHT KP.01");
            throw;
        }
        try
        {
            dataPointer = yArray.data();
        }
        catch (...)
        {
            DBTRACE("CAUGHT KP.02");
            throw;
        }

        // NOTE: There is now only one pass - second analysis pass was eliminated!
        try
        {
           status = _stabilizerToolObject->autoStabilizeGlobal->FirstPass(procData.iteration, dataPointer);
        //  					DBTRACE("FirstPass: " <<doProcessTimer);
        }
        catch (...)
        {
           DBTRACE("CAUGHT KP.03");
           throw;
        }
    }
    catch (const std::exception &ex)
    {
        TRACE_0(errout << "***ERROR**** AUTO-STABILIZE ANALYSIS EXCEPTION");
        TRACE_0(errout << ex.what());
        return -8881;
    }
    catch (...)
    {
        TRACE_0(errout << "***ERROR**** AUTO-STABILIZE ANALYSIS EXCEPTION");
        return -8881;
    }

	return status;
}

string CKAutoTrackPass1Proc::getTrackPointDataString()
{
	MTIostringstream os;
	float rowOff;
	float colOff;

	auto frames = _iterationCount;
	auto x = AUTO_TRACK_OFFSET_REFERENCE_LEVEL;
	auto y = AUTO_TRACK_OFFSET_REFERENCE_LEVEL;
	os << "v2{" << "\n";
	os <<_inFrameIndex << " " << _inFrameIndex+frames << "\n";
	os << "99 99" << "\n";
	os << "1 " << _roi.x << " " << _roi.width << " " << _roi.y << " " << _roi.height << "\n";
	os << "[" << _inFrameIndex;
	for (auto i = 0; i < frames; i++)
	{
		auto result = _stabilizerToolObject->autoStabilizeGlobal->GetOffset(i, &rowOff, &colOff);
		os << "(" << x -colOff << ", " << y -rowOff << ") " << "\n";
//DBTRACE(i);
//DBTRACE(-colOff);
//DBTRACE(-rowOff);
	}

	os << "]" << "\n" << "}" << "\n";

//	ofstream sfile("c:\\temp\\k2.pts");
//    sfile << os.str();
    return os.str();
}
