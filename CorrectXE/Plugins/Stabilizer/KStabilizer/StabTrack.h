#ifndef STABTRACKH
#define STABTRACKH

#include <vector>
using std::vector;

#include "ipp.h"
#include "StabManifests.h"
#include "StabResize.h"

class CMotionBuffer
{
public:
    CMotionBuffer ();
    ~CMotionBuffer ();

    int Alloc (bool bForProxy, int iNThreadArg, IppiSize sizeSrcArg, IppiSize sizeDstArg, int iMaxRowOff, int iMaxColOff);
    int MotionSearch (int iThread, float *fpRef, int iRowRef, int iColRef, float *fpDst,  
        int iRowDst0, int iColDst0, int &iRowDst, int &iColDst, float &fDiscrepancy);
    int MotionSearchSuper (int iThread, float *fpSrc, int iRowSrc, int iColSrc, float *fpDst,  
        int iRowDst0, int iColDst0, float &fRowDst, float &fColDst);
    IppiSize getSizeRef ();
    IppiSize getSizePatch ();
    int getMaxRowOff ();
    int getMaxColOff ();

    int getNThread ();

    int iThreadRun;
    int iThreadRet;
    int iThreadSrc;
    int iThreadTime;
    
private:
    // motion cross correlation is performed on the proxy resolution
    // image to measure a forward and backward motion offset.
    // 
    // motion cross correlation is performed on the full resolution 
    // image to refine the forward motion offset.
    IppiSize sizeCrossCorrFutr;
    IppiSize sizeCrossCorrPast;
    float *fpCrossCorr[MAX_THREAD];
    Ipp8u *pCrossCorr[MAX_THREAD];
    IppEnum algTypeCrossCorr;

    // on the full resolution image, the motion cross correlation surface is
    // up-converted to measure sub-pixel accuracy.  It is much
    // faster to up-convert the cross correlation surface than it
    // is to up-convert the images first and then perform cross-
    // correlation.  The results are very similar.
    CStabResize Resize[MAX_THREAD];
    IppiSize sizeCrossCorrSuper;
    float *fpCrossCorrSuper[MAX_THREAD];
    float *fpCrossCorrSum[MAX_THREAD];

    int iMaxRowOffFuture;
    int iMaxColOffFuture;
    int iMaxRowOffPast;
    int iMaxColOffPast;
    int iNThread;

    // size of the patch data (either proxy or full resolution)
    IppiSize sizePatch;

    // size of the destination data.  In the case of proxy or full, this 
    // is the size of the image.
    IppiSize sizeDst;

    // size of the padded patch data. The patch image gets padded so that there is
    // data for the reverse search.  Reverse searching only happens in the 
    // proxy case.  In the other cases, the reference is the same size as the 
    // patch
    IppiSize sizeRef;

    void LegalizeSearch (int &i0, int &i1, int iN);
    int SearchFuture (int iThread, float *fpRef, float *fpFutr, int iRowFutr0, int iColFutr0, 
                             int &iRowFutr, int &iColFutr);
    int SearchPast (int iThread, float *fpRef, int iRowRef0, int iColRef0, float *fpFutr, 
                               int iRowFutr, int iColFutr, int &iRowRef, int &iColRef);
};

class CTrackingPixel
{
public:
    CTrackingPixel () {iTime=iRowPxy=iColPxy=0;fRowFull=fColFull=0.f;fRowHat=fColHat=0.f;}
    ~CTrackingPixel () {};

    int iTime;
    int iRowPxy;
    int iColPxy;
    float fRowFull;
    float fColFull;
    float fRowHat;
    float fColHat;
};

class CStabTrack
{
public:
    CStabTrack ();
    ~CStabTrack ();

    int Assign (vector <CTrackingPixel> &Track);
    int getNPix ();
    int getRowPxy (int i);
    int getColPxy (int i);
    float getRowFull (int i);
    float getColFull (int i);
    float getRowHat (int i);
    float getColHat (int i);
    float getRowOff (int i);
    float getColOff (int i);
    int getTime (int i);
    int Fit (float fLockedCameraThresh);

private:
    vector <CTrackingPixel> Pixels;

    int CalcHat (bool bRow, float fLockedCameraThresh);
};

#endif
