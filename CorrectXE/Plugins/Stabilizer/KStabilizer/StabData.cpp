
#include "stdafx.h"

#define USE_MTI_MALLOC_INSTEAD_OF_REGULAR_MALLOC
#include "MTImalloc.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "bthread.h"

#include "AutoStabilizeReturns.h"
#include "StabManifests.h"
#include "StabData.h"
#include "ThreadLocker.h"

void SearchSproc (void *vp, void *vpReserved);
static CSpinLock slDataAccessLock;

//#define SINGLE_THREAD

CDataBuffer::CDataBuffer ()
{
    for (int iThread = 0; iThread < MAX_THREAD; iThread++)
    {
        fpDiff[iThread] = 0;
        fpConv[iThread] = 0;
        fpKernel[iThread] = 0;
        pKernelBuffer[iThread] = 0;
    }

    fpMinDiff = 0;
    ucpExclude = 0;
    KernelCfg = (IppEnum)(ippAlgAuto | ippiROIFull);
    ipSearchOffset = 0;
}

CDataBuffer::~CDataBuffer ()
{
    for (int iThread = 0; iThread < MAX_THREAD; iThread++)
    {
        free (fpDiff[iThread]);
        fpDiff[iThread] = 0;
        free (fpConv[iThread]);
        fpConv[iThread] = 0;
        free (fpKernel[iThread]);
        fpKernel[iThread] = 0;
        ippFree (pKernelBuffer[iThread]);
        pKernelBuffer[iThread] = 0;
    }
    free (fpMinDiff);
    fpMinDiff = 0;
    free (ucpExclude);
    ucpExclude = 0;
    free (ipSearchOffset);
    ipSearchOffset = 0;
}

int CDataBuffer::Alloc (int iNRow, int iNCol, int iNThreadArg)
{
    IppStatus status;

    sizeImg.height = iNRow;
    sizeImg.width = iNCol;
    sizeKernel.height = 2*PATCH_RADIUS + 1;
    sizeKernel.width = 2*PATCH_RADIUS + 1;
    sizeConv.height = sizeImg.height + sizeKernel.height - 1;
    sizeConv.width = sizeImg.width + sizeKernel.width - 1;
    sizeSearch.height = sizeImg.height - 2*PATCH_SEARCH_MAX;
    sizeSearch.width = sizeImg.width - 2*PATCH_SEARCH_MAX;
    sizeCenter.height = sizeSearch.height - 2*PATCH_RADIUS;
    sizeCenter.width = sizeSearch.width - 2*PATCH_RADIUS;
    iNThread = iNThreadArg;

    for (int iThread = 0; iThread < iNThread; iThread++)
    {
        fpDiff[iThread] = (float *) malloc (sizeImg.height * sizeImg.width * 4);
        if (fpDiff[iThread] == 0)
        {
            return ASTAB_ERR_ALLOC;
        }

        // initialize
        status = ippiSet_32f_C1R (0.f, fpDiff[iThread], 4*sizeImg.width, sizeImg);
        if (status)
        {
            return ASTAB_ERR_IPP_SET;
        }

        fpConv[iThread] = (float *) malloc (sizeConv.height * sizeConv.width * 4);
        if (fpConv[iThread] == 0)
        {
            return ASTAB_ERR_ALLOC;
        }
    }

    fpMinDiff = (float *) malloc (sizeImg.height*sizeImg.width*4);
    if (fpMinDiff == 0)
    {
        return ASTAB_ERR_ALLOC;
    }

    // initialize
    status = ippiSet_32f_C1R (0.f, fpMinDiff, 4*sizeImg.width, sizeImg);
    if (status)
    {
        return ASTAB_ERR_IPP_SET;
    }

    for (int iThread = 0; iThread < iNThread; iThread++)
    {
        fpKernel[iThread] = (float *) malloc (sizeKernel.height*sizeKernel.width*4);
        if (fpKernel[iThread] == 0)
        {
            return ASTAB_ERR_ALLOC;
        }

        for (int iRow = 0; iRow < sizeKernel.height; iRow++)
        {
            for (int iCol = 0; iCol < sizeKernel.width; iCol++)
            {
                fpKernel[iThread][iRow*sizeKernel.width+iCol] = 1.f / (sizeKernel.height*sizeKernel.width);
            }
        }
    }

    // For each location of a patch, exclude nearby pixels to force separation.
    //
    // We exclude 2*PATCH_RADIUS so that the patches don't overlap

    int iExclude = 2*PATCH_RADIUS;
    sizeExclude.height = 2*iExclude+1;
    sizeExclude.width = 2*iExclude+1;
    
    ucpExclude = (unsigned char *) malloc ( sizeExclude.height * sizeExclude.width );
    if (ucpExclude == 0)
    {
        return ASTAB_ERR_ALLOC;
    }

    status = ippiSet_8u_C1R (255, ucpExclude, sizeExclude.width, sizeExclude);
    if (status)
    {
        return ASTAB_ERR_IPP_SET;
    }

    int bufferSize;
    status = ippiConvGetBufferSize(sizeImg, sizeKernel, ipp32f, 1, KernelCfg, &bufferSize);
    if (status)
    {
        return ASTAB_ERR_IPP_CONV;
    }

    for (int iThread = 0; iThread < iNThread; iThread++)
    {
        pKernelBuffer[iThread] = (Ipp8u *)ippMalloc ( bufferSize );
		if (pKernelBuffer[iThread] == 0)
        {
            return ASTAB_ERR_IPP_ALLOC;
        }
    }

    // count the number of offsets used when searching for patch centers
    int iRowSearch = (sizeImg.height - sizeSearch.height) / 2;
    int iColSearch = (sizeImg.width - sizeSearch.width) / 2;

    // consider each offset
    iNSearch = 0;
    for (int iRowOff = -iRowSearch; iRowOff <= iRowSearch; iRowOff++)
    {
        for (int iColOff = -iColSearch; iColOff <= iColSearch; iColOff++)
        {
            float fOffset = sqrt ((float)(iRowOff*iRowOff) + (float)(iColOff*iColOff));

            if (fOffset > PATCH_SEARCH_MIN)
            {
                iNSearch++;
            }
        }
    }

    // allocate storage for the search offsets
    ipSearchOffset = (int *) malloc (iNSearch * sizeof(int));
    if (ipSearchOffset == 0)
    {
        return ASTAB_ERR_ALLOC;
    }

    int *ipSO = ipSearchOffset;
    for (int iRowOff = -iRowSearch; iRowOff <= iRowSearch; iRowOff++)
    {
        for (int iColOff = -iColSearch; iColOff <= iColSearch; iColOff++)
        {
            float fOffset = sqrt ((float)(iRowOff*iRowOff) + (float)(iColOff*iColOff));

            if (fOffset > PATCH_SEARCH_MIN)
            {
                *ipSO++ = iRowOff * sizeImg.width + iColOff;
            }
        }
    }

    return ASTAB_RET_SUCCESS;
}

IppiSize CDataBuffer::getSizeImg ()
{
    return sizeImg;
}

IppiSize CDataBuffer::getSizeSearch ()
{
    return sizeSearch;
}

IppiSize CDataBuffer::getSizeCenter ()
{
    return sizeCenter;
}

IppiSize CDataBuffer::getSizeExclude ()
{
    return sizeExclude;
}

IppiSize CDataBuffer::getSizeConv ()
{
    return sizeConv;
}

IppiSize CDataBuffer::getSizeKern ()
{
    return sizeKernel;
}

float *CDataBuffer::getDiff (int iThread)
{
    return fpDiff[iThread];
}

float *CDataBuffer::getConv (int iThread)
{
    return fpConv[iThread];
}

float *CDataBuffer::getMinDiff ()
{
    return fpMinDiff;
}

unsigned char *CDataBuffer::getExclude ()
{
    return ucpExclude;
}

int CDataBuffer::Conv (int iThread)
{
    IppStatus status;

    // perform the convolution
    status = ippiConv_32f_C1R (fpDiff[iThread], 4*sizeImg.width, sizeImg, fpKernel[iThread], 4*sizeKernel.width, 
        sizeKernel, fpConv[iThread], 4*sizeConv.width, KernelCfg, pKernelBuffer[iThread]);
    if (status)
    {
        return ASTAB_ERR_IPP_CONV;
    }

    return ASTAB_RET_SUCCESS;
}

int CDataBuffer::getNSearch ()
{
    return iNSearch;
}

int *CDataBuffer::getSearchOffset ()
{
    return ipSearchOffset;
}

int CDataBuffer::getNThread ()
{
    return iNThread;
}

CStabData::CStabData ()
{
    fpFull = 0;
    fpPxy = 0;
    pDataBuf = 0;
}

CStabData::~CStabData ()
{
    Free ();
}


int CStabData::Alloc (int iNFrame, int iNRow, int iNCol, int iNStride, int iNThread)
{
    int iRet;

    sizeFull.height = iNRow;
    sizeFull.width = iNCol;
    iNStrideFull = iNStride;

    // the proxy image size
    sizePxy.height = (int)((float)(sizeFull.height * PROXY_WIDTH) /
        (float)sizeFull.width + .5f);
    sizePxy.width = PROXY_WIDTH;

    // allocate storage for the full image and the proxy image
    fpFull = (float *) malloc (sizeFull.height * sizeFull.width * 4);
    fpPxy = (float *) malloc (sizePxy.height * sizePxy.width * 4);

    if (fpFull == 0 || fpPxy == 0)
    {
        return ASTAB_ERR_ALLOC;
    }

    // allocate the resizer to create the proxy image
    iRet = Resize.Alloc (sizeFull, sizePxy);
    if (iRet)
    {
        return iRet;
    }
    
    pDataBuf = new CDataBuffer;

    iRet = pDataBuf->Alloc (sizePxy.height, sizePxy.width, iNThread);
    if (iRet)
    {
        return iRet;
    }

    return ASTAB_RET_SUCCESS;
}

int CStabData::Free ()
{
    free (fpFull);
    fpFull = 0;
    free (fpPxy);
    fpPxy = 0;

    Resize.Free();

    delete pDataBuf;
    pDataBuf = 0;

    return ASTAB_RET_SUCCESS;
}

int CStabData::FindPatches ()
{
    IppStatus status;

    IppiSize sizeImg = pDataBuf->getSizeImg();
    IppiSize sizeCenter = pDataBuf->getSizeCenter();
    IppiSize sizeExclude = pDataBuf->getSizeExclude();

    int iNThread = pDataBuf->getNThread();

    // the central portion where we look for centers of patches
    int iRowCenULC = (sizeImg.height - sizeCenter.height) / 2;
    int iColCenULC = (sizeImg.width - sizeCenter.width) / 2;

    float *fpMinDiff = pDataBuf->getMinDiff () + iRowCenULC * sizeImg.width + iColCenULC;

    // set minimum difference on the center image
    status = ippiSet_32f_C1R (100.f, fpMinDiff, 4*sizeImg.width, sizeCenter);
    if (status)
    {
        return ASTAB_ERR_IPP_SET;
    }

    // consider each offset
    iSprocRet = ASTAB_RET_SUCCESS;

#ifdef SINGLE_THREAD

    for (iSprocThread = 0; iSprocThread < iNThread; iSprocThread++)
    {
        int iRet = doSearch (iSprocThread);
        if (iRet)
        {
            iSprocRet = iRet;
        }
	 }

#else

	 iSprocRun = iNThread;
	 SprocsDone.reset();

	 for (iSprocThread = 0; iSprocThread < iNThread; iSprocThread++)
	 {
        if (BThreadSpawn (SearchSproc, (void*) this) == 0)
        {
            iSprocRet = ASTAB_ERR_BTHREAD;
        }
    }

	// Wait up to 5 seconds
	if (!SprocsDone.wait(5000))
    {
		TRACE_0(errout << "HUNG in ProcessTemplates");
    }

#endif

    if (iSprocRet)
    {
        return iSprocRet;
    }


    return ASTAB_RET_SUCCESS;
}

int CStabData::Exclude (int iRow, int iCol)
{
    float *fpMinDiff = pDataBuf->getMinDiff ();
    IppiSize sizeImg = pDataBuf->getSizeImg ();
    IppiSize sizeExclude = pDataBuf->getSizeExclude ();

    // exclude nearby values to force separation of the patches
    int iRow0 = iRow - (sizeExclude.height-1)/2;
    int iRow1 = iRow + (sizeExclude.height-1)/2;
    int iCol0 = iCol - (sizeExclude.width-1)/2;
    int iCol1 = iCol + (sizeExclude.width-1)/2;

    // the adjusted exclusion
	int iR0 = std::max<int>(0, iRow0);
	int iR1 = std::min<int>(sizePxy.height-1, iRow1);
	int iC0 = std::max<int>(0, iCol0);
    int iC1 = std::min<int>(sizePxy.width-1, iCol1);

    unsigned char *ucpE = pDataBuf->getExclude() + ((iR0-iRow0) * sizeExclude.width + (iC0-iCol0));
    IppiSize sizeE;
    sizeE.height = iR1-iR0+1;
    sizeE.width = iC1-iC0+1;

    IppStatus status;
    status = ippiSet_32f_C1MR (0.f, fpMinDiff + iR0*sizeImg.width + iC0, 4*sizeImg.width, sizeE, ucpE, sizeExclude.width);
    if (status)
    {
        return ASTAB_ERR_IPP_SET;
    }

    return ASTAB_RET_SUCCESS;
}

int CStabData::getPatch (int &iRow, int &iCol, bool &bValid)
{
    int iRet;

    iRow = -1;
    iCol = -1;
    bValid = false;

    // select the location with the largest mindiff
    float *fpMinDiff = pDataBuf->getMinDiff();
    IppiSize sizeImg = pDataBuf->getSizeImg();

    float fMaxVal;
    int iMaxRow;
    int iMaxCol;
    IppStatus status;
    status = ippiMaxIndx_32f_C1R (fpMinDiff, 4*sizeImg.width, sizeImg, &fMaxVal, &iMaxCol, &iMaxRow);
    if (status)
    {
        return ASTAB_ERR_IPP_MAX_INDX;
    }

    if (fMaxVal > PATCH_THRESH)
    {
        bValid = true;
        iRow = iMaxRow;
        iCol = iMaxCol;

        iRet = Exclude (iRow, iCol);
        if (iRet)
        {
            return iRet;
        }
    }
        
    return ASTAB_RET_SUCCESS;
}

int CStabData::FirstPass (float *fpImg)
{
    int iRet;

    iRet = setFull (fpImg);
    if (iRet)
    {
        return iRet;
    }

    iRet = setPxy ();
    if (iRet)
    {
        return iRet;
    }

    return ASTAB_RET_SUCCESS;
}

float *CStabData::getImage (int iRes)
{
    float *fp = 0;
    if (iRes == IMAGE_RESOLUTION_FULL)
    {
        fp = fpFull;
    }
    else if (iRes == IMAGE_RESOLUTION_PXY)
    {
        fp = fpPxy;
    }

    return fp;
}

IppiSize CStabData::getImageSize (int iRes)
{
    IppiSize size;
    size.height = 0;
    size.width = 0;
    if (iRes == IMAGE_RESOLUTION_FULL)
    {
        size = sizeFull;
    }
    else if (iRes == IMAGE_RESOLUTION_PXY)
    {
        size = sizePxy;
    }

    return size;
}

float CStabData::getScale (int iSrcRes, int iDstRes)
{
    IppiSize sizeSrc = getImageSize (iSrcRes);
    IppiSize sizeDst = getImageSize (iDstRes);    
        
    // the scale factor needed to go from the source to the destination
    float fScale = (float)sizeDst.width / (float)sizeSrc.width;

    return fScale;
}

int CStabData::setFull (float *fpSrc)
{
    IppStatus status;

    // make a local copy
    status = ippiCopy_32f_C1R (fpSrc, 4*iNStrideFull, fpFull, 4*sizeFull.width, sizeFull);
    if (status)
    {
        return ASTAB_ERR_IPP_ADD;
    }

    return ASTAB_RET_SUCCESS;
}

int CStabData::setPxy ()
{
  int iRet;
  iRet = Resize.Apply (fpFull, fpPxy);
  if (iRet)
  {
      return iRet;
  }

  return ASTAB_RET_SUCCESS;
}

int CStabData::doSearch (int iThread)
{
    int iRet;
    IppStatus status;

    IppiSize sizeImg = pDataBuf->getSizeImg();
    IppiSize sizeSearch = pDataBuf->getSizeSearch();
    IppiSize sizeCenter = pDataBuf->getSizeCenter();
    IppiSize sizeConv = pDataBuf->getSizeConv();
    IppiSize sizeKern = pDataBuf->getSizeKern();

    int iNThread = pDataBuf->getNThread();
    int iNSearch = pDataBuf->getNSearch ();

    // the search portion of the source image
    int iRowSearch = (sizeImg.height - sizeSearch.height) / 2;
    int iColSearch = (sizeImg.width - sizeSearch.width) / 2;
    float *fpSrc = fpPxy + iRowSearch * sizeImg.width + iColSearch;

    // the central portion where we look for centers of patches
    int iRowCenULC = (sizeImg.height - sizeCenter.height) / 2;
    int iColCenULC = (sizeImg.width - sizeCenter.width) / 2;

    float *fpMinDiff = pDataBuf->getMinDiff () + iRowCenULC * sizeImg.width + iColCenULC;

    // the radius of the convolution
    int iKernRadRow = (sizeKern.height-1)/2;
    int iKernRadCol = (sizeKern.width-1)/2;

    // consider each offset
    int iSearch0 = (int)( (float)(iThread*iNSearch) / (float)iNThread);
    int iSearch1 = (int)( (float)((iThread+1)*iNSearch) / (float)iNThread);

    // the difference image
    float *fpDiff = pDataBuf->getDiff(iThread) + iRowSearch * sizeImg.width + iColSearch;
    float *fpCenConv = pDataBuf->getConv(iThread) + (iRowCenULC+iKernRadRow) * sizeConv.width + (iColCenULC+iKernRadCol);

    int *ipSO = pDataBuf->getSearchOffset() + iSearch0;
    for (int iSearch = iSearch0; iSearch < iSearch1; iSearch++)
    {
        // the destination image
        float *fpDst = fpSrc + *ipSO++;

        // subtract the fpDst from fpSrc
        status = ippiAbsDiff_32f_C1R (fpDst, 4*sizeImg.width, fpSrc, 4*sizeImg.width,
            fpDiff, 4*sizeImg.width, sizeSearch);
        if (status)
        {
            return ASTAB_ERR_IPP_SUBTRACT;
        }

        // convolve to get averages over each patch
        iRet = pDataBuf->Conv (iThread);
        if (iRet)
        {
            return iRet;
        }

        // keep track of the minimum.  Need to prevent multi-thread access here 
        {
            CAutoSpinLocker lock(slDataAccessLock);
            status = ippiMinEvery_32f_C1IR (fpCenConv, 4*sizeConv.width, fpMinDiff, 4*sizeImg.width, sizeCenter);
            if (status)
            {
                return ASTAB_ERR_IPP_MIN_EVERY;
            }
        }

    }

    return ASTAB_RET_SUCCESS;
}

void SearchSproc (void *vp, void *vpReserved)
{
    CStabData *sdp = (CStabData *)vp;

    int iSprocThread = sdp->iSprocThread;
    
    if (BThreadBegin (vpReserved))
    {
        // failure
        sdp->iSprocRet = ASTAB_ERR_BTHREAD;
    }
    else
    {
        // success
        int iRet = sdp->doSearch (iSprocThread);
        if (iRet)
        {
            sdp->iSprocRet = iRet;
        }
    }

    // finished
    if (--sdp->iSprocRun <= 0)
    {
        sdp->SprocsDone.signal();
    }
}
