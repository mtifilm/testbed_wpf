#ifndef STABDATAH
#define STABDATAH

#include "ipp.h"
#include "StabManifests.h"
#include <atomic>
#include "Event.h"
#include "StabResize.h"

class CDataBuffer
{
public:
    CDataBuffer();
    ~CDataBuffer();
    int Alloc (int iNRow, int iNCol, int iNThreadArg);
    int FindPatches ();
    IppiSize getSizeImg ();
    IppiSize getSizeSearch ();
    IppiSize getSizeCenter ();
    IppiSize getSizeExclude ();
    IppiSize getSizeConv ();
    IppiSize getSizeKern ();
    float *getDiff (int iThread);
    float *getConv (int iThread);
    float *getMinDiff ();
    unsigned char *getExclude();
    int Conv (int iThread);
    int getNSearch ();
    int *getSearchOffset ();
    int getNThread ();
    int CriticalEnter ();
    int CriticalExit ();

private:
    IppiSize sizeImg;
    IppiSize sizeConv;
    IppiSize sizeKernel;
    IppiSize sizeSearch;
    IppiSize sizeCenter;
    IppiSize sizeExclude;

    int iNSearch;
    int *ipSearchOffset;

    float *fpDiff[MAX_THREAD];
    float *fpConv[MAX_THREAD];
    float *fpMinDiff;
    float *fpKernel[MAX_THREAD];
    unsigned char *ucpExclude;
    IppEnum KernelCfg;
    Ipp8u *pKernelBuffer[MAX_THREAD];

    int iNThread;
};

class CStabData
{
public:
    CStabData ();
    ~CStabData ();

    int Alloc (int iNFrame, int iNRow, int iNCol, int iNStride, int iNThread);
    int Free ();
    int FirstPass (float *fpImg);
    float *getImage (int iRes);
    IppiSize getImageSize (int iRes);
    float getScale (int iSrcRes, int iDstRes);
    int FindPatches ();
    int Exclude (int iRow, int iCol);
    int getPatch (int &iRow, int &iCol, bool &bValid);

private:
    float *fpFull = nullptr;     // full resolution
    float *fpPxy = nullptr;      // proxy resolution
    int iNStrideFull;

    // image sizes
    IppiSize sizeFull;
    IppiSize sizePxy;

    CDataBuffer *pDataBuf = nullptr;
    CStabResize Resize;

    int setFull (float *fp);
    int setPxy ();

    int iSprocThread;
	std::atomic_int iSprocRun;
	Event SprocsDone;
    int iSprocRet;

    int doSearch (int iThread);

    friend void SearchSproc (void *vp, void *vpReserved);
};

#endif
