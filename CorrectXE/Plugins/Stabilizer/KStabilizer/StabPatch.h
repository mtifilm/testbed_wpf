#ifndef STABPATCHH
#define STABPATCHH

#include <vector>
using std::vector;

#include "ipp.h"
#include "StabManifests.h"
#include "StabTrack.h"

/*
    Patches are (2*PATCH_RADIUS+1) x (2*PATCH_RADIUS+1) rectangles.  

    Patches are chosen so they have poor matches when shifting around in the
    image.  This makes them good candidates for motion estimation.

    Patches are distributed throughout the image so that some of them fall
    in the background of the image.  Background points are much better for
    tracking.
*/

#define PATCH_VALUE_NONE     0x0
#define PATCH_VALUE_PXY      0x1
#define PATCH_VALUE_FULL     0x2
#define PATCH_VALUE_ROW      0x10
#define PATCH_VALUE_COL      0x20
#define PATCH_VALUE_TIME     0x40
#define PATCH_VALUE_COUNT    0x100
#define PATCH_VALUE_INDEX    0x200
#define PATCH_VALUE_FIRST    0x400
#define PATCH_VALUE_LAST     0x800

class CStabPatch
{
public:
    CStabPatch ();
    ~CStabPatch ();

    int Alloc (IppiSize sizeRefPxyArg, IppiSize sizeRefFullArg, IppiSize sizeImgPxyArg,
        IppiSize sizeImgFullArg);
    int AssignImage (int iRowPxy, int iColPxy, int iTime, float *fpPxy, float *fpFull);
    void Append (int iTime, int iRowPxy, int iColPxy, float fRowFull, float fColFull);

    float *getRef (int iType);
    int getFull (int iPxy);
    int getCount ();
    int getTime (int iIdx);
    int getRowPxy (int iIdx);
    int getColPxy (int iIdx);
    float getRowFull (int iIdx);
    float getColFull (int iIdx);

    vector <CTrackingPixel> getTrack ();

private:
    vector <CTrackingPixel> Track;

    float *fpRefPxy;
    float *fpRefFull;
    float fScaleFull;

    IppiSize sizeImgFull;
    IppiSize sizeImgPxy;

    IppiSize sizeRefPxy;
    IppiSize sizeRefFull;

    int Assign (int iRow, int iCol, float *fpSrc, float *fpDst, IppiSize sizeRef, IppiSize sizeImg);
};

#endif
