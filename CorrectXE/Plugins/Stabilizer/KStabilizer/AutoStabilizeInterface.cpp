
#include "stdafx.h"

#include "AutoStabilizeInterface.h"
#include "AutoStabilizeReturns.h"
#include "AutoStabilize.h"

void * AutoStabilizeCreate (EngineArgs engine, int *ipRet)
{
	CAutoStabilize *asp = new CAutoStabilize;
	*ipRet = asp->Alloc (engine);
	return (void *)asp;
}

int AutoStabilizeDestroy (void *vpEngine)
{
	CAutoStabilize *asp = (CAutoStabilize *)vpEngine;
	delete asp;
  
	return ASTAB_RET_SUCCESS;
}

void * AutoStabilizeNew ()
{
	CAutoStabilize *asp = new CAutoStabilize;
	return (void *)asp;
}

int AutoStabilizeAlloc (void *vpEngine, EngineArgs engine)
{
    if (vpEngine != 0)
    {
        CAutoStabilize *asp = (CAutoStabilize *)vpEngine;
        return asp->Alloc (engine);
    }

    return ASTAB_RET_SUCCESS;
}

int AutoStabilizeFree (void *vpEngine)
{
    if (vpEngine != 0)
    {
        CAutoStabilize *asp = (CAutoStabilize *)vpEngine;
        return asp->Free ();
    }

    return ASTAB_RET_SUCCESS;
}

void AutoStabilizeDelete (void *vpEngine)
{
    if (vpEngine != 0)
    {
	    CAutoStabilize *asp = (CAutoStabilize *)vpEngine;
        asp->Free ();
	    delete asp;
    }
  
	return;
}

// NOTE: There is now only one pass - second analysis pass was eliminated!
int AutoStabilizeFirstPass (void *vpEngine, int iFrame, float *fpY)
{
	CAutoStabilize *asp = (CAutoStabilize *)vpEngine;
	return asp->FirstPass (iFrame, fpY);
}

int AutoStabilizeGetOffset (void *vpEngine, int iFrame,  float *fpRowOff, 
    float *fpColOff)
{
   CAutoStabilize *asp = (CAutoStabilize *)vpEngine;
	return asp->GetOffset (iFrame, fpRowOff, fpColOff);
}
