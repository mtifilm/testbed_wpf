#include "stdafx.h"

#define USE_MTI_MALLOC_INSTEAD_OF_REGULAR_MALLOC
#include "MTImalloc.h"

#include <math.h>
#include "mti_math.h"

int InvertMatrix (float *fpSrc, float *fpDst, int iDim)
{
  double *dpTmp, dFactor, dEpsilon=0.0000001;
  int iRow, iCol, iRR, iCnt;
  bool bFoundOne, bError;
 
  // the temporal matrix is iDim high and (2*iDim) wide

  int iTmpW = 2*iDim;

  dpTmp = (double *) malloc (iDim * iTmpW * sizeof(double));
  if (dpTmp == 0)
  {
    return -1;
  }

/*
	Set up the temporary array
*/

  for (iRow = 0; iRow < iDim; iRow++)
   {
    for (iCol = 0; iCol < iDim; iCol++)
     {
      dpTmp[iRow*iTmpW+iCol] = (double)fpSrc[iRow*iDim+iCol];
      if (iRow == iCol)
        dpTmp[iRow*iTmpW+iCol+iDim] = 1.;
      else
        dpTmp[iRow*iTmpW+iCol+iDim] = 0.;
     }
   }

/*
	Make the left half of dpTmp upper diagonal with 1.'s along diagonal
*/

  bError = false;
  for (iRow = 0; iRow < iDim && bError == false; iRow++)
   {
    if (fabs(dpTmp[iRow*iTmpW+iRow]) < dEpsilon)
     {
/*
	The diagonal element is zero.  Add in a non-zero row.
*/
      bFoundOne = false;
      for (iCnt = iRow + 1; iCnt < iDim && bFoundOne == false; iCnt++)
       {
        if (fabs(dpTmp[iCnt*iTmpW+iRow]) > dEpsilon)
         {
          bFoundOne = true;
          for (iCol = iRow; iCol < 2*iDim; iCol++)
           {
            dpTmp[iRow*iTmpW+iCol] += dpTmp[iCnt*iTmpW+iCol];
           }
         }
       }

      if (bFoundOne == false)
       {
        bError = true;
       }
     }

    if (bError == false)
     {
/*
	Set the diagonal element to 1.
*/

      dFactor = 1. / dpTmp[iRow*iTmpW+iRow];
      for (iCol = iRow; iCol < 2*iDim; iCol++)
       {
        dpTmp[iRow*iTmpW+iCol] *= dFactor;
       }

/*
	Subtract this row from lower rows
*/

      for (iRR = iRow + 1; iRR < iDim; iRR++)
       {
        dFactor = dpTmp[iRR*iTmpW+iRow];
        for (iCol = iRow; iCol < 2*iDim; iCol++)
         {
          dpTmp[iRR*iTmpW+iCol] -= dFactor * dpTmp[iRow*iTmpW+iCol];
         }
       }
     }
   }

/*
	Get rid on entries in the upper right portion.  Keep 1.'s along
	diagonal.
*/

  for (iRow = iDim-1; iRow > 0 && bError == false; iRow--)
   {
/*
	Subtract this row from upper rows
*/

    for (iRR = 0; iRR < iRow; iRR++)
     {
      dFactor = dpTmp[iRR*iTmpW+iRow];
      for (iCol = iRow; iCol < 2*iDim; iCol++)
       {
        dpTmp[iRR*iTmpW+iCol] -= dFactor * dpTmp[iRow*iTmpW+iCol];
       }
     }
   }

/*
	Copy over the inverse values.
*/

  if (bError == false)
   {
    for (iRow = 0; iRow < iDim; iRow++)
    for (iCol = 0; iCol < iDim; iCol++)
     {
      fpDst[iRow*iDim+iCol] = (float)dpTmp[iRow*iTmpW+iCol+iDim];
     }
   }

  free (dpTmp);
  
  if (bError)
  {
    return -1;
  }
  else
  {
    return 0;
  }
}  /* InvertMatrix */

int InvertMatrix (double *dpSrc, double *dpDst, int iDim)
{
  double *dpTmp, dFactor, dEpsilon=0.0000001;
  int iRow, iCol, iRR, iCnt;
  bool bFoundOne, bError;
 
  // the temporal matrix is iDim high and (2*iDim) wide

  int iTmpW = 2*iDim;

  dpTmp = (double *) malloc (iDim * iTmpW * sizeof(double));
  if (dpTmp == 0)
  {
    return -1;
  }

/*
	Set up the temporary array
*/

  for (iRow = 0; iRow < iDim; iRow++)
   {
    for (iCol = 0; iCol < iDim; iCol++)
     {
      dpTmp[iRow*iTmpW+iCol] = (double)dpSrc[iRow*iDim+iCol];
      if (iRow == iCol)
        dpTmp[iRow*iTmpW+iCol+iDim] = 1.;
      else
        dpTmp[iRow*iTmpW+iCol+iDim] = 0.;
     }
   }

/*
	Make the left half of dpTmp upper diagonal with 1.'s along diagonal
*/

  bError = false;
  for (iRow = 0; iRow < iDim && bError == false; iRow++)
   {
    if (fabs(dpTmp[iRow*iTmpW+iRow]) < dEpsilon)
     {
/*
	The diagonal element is zero.  Add in a non-zero row.
*/
      bFoundOne = false;
      for (iCnt = iRow + 1; iCnt < iDim && bFoundOne == false; iCnt++)
       {
        if (fabs(dpTmp[iCnt*iTmpW+iRow]) > dEpsilon)
         {
          bFoundOne = true;
          for (iCol = iRow; iCol < 2*iDim; iCol++)
           {
            dpTmp[iRow*iTmpW+iCol] += dpTmp[iCnt*iTmpW+iCol];
           }
         }
       }

      if (bFoundOne == false)
       {
        bError = true;
       }
     }

    if (bError == false)
     {
/*
	Set the diagonal element to 1.
*/

      dFactor = 1. / dpTmp[iRow*iTmpW+iRow];
      for (iCol = iRow; iCol < 2*iDim; iCol++)
       {
        dpTmp[iRow*iTmpW+iCol] *= dFactor;
       }

/*
	Subtract this row from lower rows
*/

      for (iRR = iRow + 1; iRR < iDim; iRR++)
       {
        dFactor = dpTmp[iRR*iTmpW+iRow];
        for (iCol = iRow; iCol < 2*iDim; iCol++)
         {
          dpTmp[iRR*iTmpW+iCol] -= dFactor * dpTmp[iRow*iTmpW+iCol];
         }
       }
     }
   }

/*
	Get rid on entries in the upper right portion.  Keep 1.'s along
	diagonal.
*/

  for (iRow = iDim-1; iRow > 0 && bError == false; iRow--)
   {
/*
	Subtract this row from upper rows
*/

    for (iRR = 0; iRR < iRow; iRR++)
     {
      dFactor = dpTmp[iRR*iTmpW+iRow];
      for (iCol = iRow; iCol < 2*iDim; iCol++)
       {
        dpTmp[iRR*iTmpW+iCol] -= dFactor * dpTmp[iRow*iTmpW+iCol];
       }
     }
   }

/*
	Copy over the inverse values.
*/

  if (bError == false)
   {
    for (iRow = 0; iRow < iDim; iRow++)
    for (iCol = 0; iCol < iDim; iCol++)
     {
      dpDst[iRow*iDim+iCol] = dpTmp[iRow*iTmpW+iCol+iDim];
     }
   }

  free (dpTmp);
  
  if (bError)
  {
    return -1;
  }
  else
  {
    return 0;
  }
}  /* InvertMatrix */


/************************************************* 
   UNIFORM DISTRIBUTION [0,1) generator ran266

   From Kevin Manbeck, August 9, 1994
 
   NOTE:  Use ODD seed for good performance
**************************************************/

float ran266(long *lseed)

{  float ran;

   *lseed = 25*(*lseed);
   *lseed = *lseed % 67108864;
   *lseed = 25*(*lseed);
   *lseed = *lseed % 67108864;
   *lseed = 5*(*lseed);
   *lseed = *lseed % 67108864;

   ran = (float) *lseed / 67108864.f;

   return (ran);
}

int OLS_Estimate_Beta (float *fpX, float *fpY, int iNRow, int iNCol, float *fpBeta)
/*
	Use Ordinary Least Squares to estimate Beta.

	The mode is 

		Y = X*Beta

	where Y is an NRow X 1 vector
              X is an NRow X NCol matrix

	The resulting Beta is an NCol X 1 vector of coefficients
*/
{
  float *fpXpX = 0;
  float *fpXpXi = 0;
  float *fpXpY = 0;

  // allocate storage for X'X, inv(X'X)
  fpXpX = (float *) malloc (iNCol * iNCol * sizeof(float));
  fpXpXi = (float *) malloc (iNCol * iNCol * sizeof(float));

  if (fpXpX == 0 || fpXpXi == 0)
  {
    free (fpXpX);
    fpXpX = 0;
    free (fpXpXi);
    fpXpXi = 0;
    return -1;
  }

  
  // allocate storage for X'Y

  fpXpY = (float *) malloc (iNCol * sizeof(float)); 
  if (fpXpY == 0)
  {
    free (fpXpX);
    fpXpX = 0;
    free (fpXpXi);
    fpXpXi = 0;

    return -1;
  }


  // compute X'X
  for (int iR = 0; iR < iNCol; iR++)
  {
    for (int iC = 0; iC < iNCol; iC++)
    {
      float fSum = 0.f;

      for (int iT = 0; iT < iNRow; iT++)
      {
        fSum += fpX[iT*iNCol + iR] * fpX[iT*iNCol + iC];
      }

      fpXpX[iR*iNCol+iC] = fSum;
    }
  }

  // compute X'Y
  for (int iC = 0; iC < iNCol; iC++)
  {
    float fSum = 0.f;

    for (int iT = 0; iT < iNRow; iT++)
    {
      fSum += fpX[iT*iNCol + iC] * fpY[iT];
    }

    fpXpY[iC] = fSum;
  }

  // compute inverse of X'X
  int iRet = InvertMatrix (fpXpX, fpXpXi, iNCol);
  if (iRet)
  {
    free (fpXpX);
    fpXpX = 0;
    free (fpXpXi);
    fpXpXi = 0;

    free (fpXpY);
    fpXpY = 0;

    return iRet;
  }

  // find inv(X'X) * (X'Y)

  for (int iC = 0; iC < iNCol; iC++)
  {
    float fSum = 0.f;
    for (int iT = 0; iT < iNCol; iT++)
    {
      fSum += fpXpXi[iC*iNCol+iT] * fpXpY[iT];
    }
    fpBeta[iC] = fSum;
  }

  // free all the storage
  free (fpXpX);
  fpXpX = 0;
  free (fpXpXi);
  fpXpXi = 0;

  free (fpXpY);
  fpXpY = 0;

  return 0;
}


int OLS_Estimate_Beta (double *dpX, double *dpY, int iNRow, int iNCol, double *dpBeta)
/*
	Use Ordinary Least Squares to estimate Beta.

	The mode is 

		Y = X*Beta

	where Y is an NRow X 1 vector
              X is an NRow X NCol matrix

	The resulting Beta is an NCol X 1 vector of coefficients
*/
{
  double *dpXpX = 0;
  double *dpXpXi = 0;
  double *dpXpY = 0;

  // allocate storage for X'X, inv(X'X)
  dpXpX = (double *) malloc (iNCol * iNCol * sizeof(double));
  dpXpXi = (double *) malloc (iNCol * iNCol * sizeof(double));

  if (dpXpX == 0 || dpXpXi == 0)
  {
    free (dpXpX);
    dpXpX = 0;
    free (dpXpXi);
    dpXpXi = 0;
    return -1;
  }

  
  // allocate storage for X'Y

  dpXpY = (double *) malloc (iNCol * sizeof(double)); 
  if (dpXpY == 0)
  {
    free (dpXpX);
    dpXpX = 0;
    free (dpXpXi);
    dpXpXi = 0;

    return -1;
  }


  // compute X'X
  for (int iR = 0; iR < iNCol; iR++)
  {
    for (int iC = 0; iC < iNCol; iC++)
    {
      double dSum = 0.;

      for (int iT = 0; iT < iNRow; iT++)
      {
        dSum += dpX[iT*iNCol + iR] * dpX[iT*iNCol + iC];
      }

      dpXpX[iR*iNCol+iC] = dSum;
    }
  }

  // compute X'Y
  for (int iC = 0; iC < iNCol; iC++)
  {
    double dSum = 0.f;

    for (int iT = 0; iT < iNRow; iT++)
    {
      dSum += dpX[iT*iNCol + iC] * dpY[iT];
    }

    dpXpY[iC] = dSum;
  }

  // compute inverse of X'X
  int iRet = InvertMatrix (dpXpX, dpXpXi, iNCol);
  if (iRet)
  {
    free (dpXpX);
    dpXpX = 0;
    free (dpXpXi);
    dpXpXi = 0;

    free (dpXpY);
    dpXpY = 0;

    return iRet;
  }

  // find inv(X'X) * (X'Y)

  for (int iC = 0; iC < iNCol; iC++)
  {
    double dSum = 0.f;
    for (int iT = 0; iT < iNCol; iT++)
    {
      dSum += dpXpXi[iC*iNCol+iT] * dpXpY[iT];
    }
    dpBeta[iC] = dSum;
  }

  // free all the storage
  free (dpXpX);
  dpXpX = 0;
  free (dpXpXi);
  dpXpXi = 0;

  free (dpXpY);
  dpXpY = 0;

  return 0;
}


int OLS_Apply_Beta (float *fpX, float *fpY, int iNRow, int iNCol, float *fpBeta)
/*
	Use Ordinary Least Squares to estimate Y.

	The mode is 

		Y = X*Beta

	where Y is an NRow X 1 vector
              X is an NRow X NCol matrix

	The Beta (generated by OLS_Estimate_Beta) is an NCol X 1 vector 
        of coefficients
*/
{
  for (int iR = 0; iR < iNRow; iR++)
  {
    float fSum = 0.f;
    for (int iC = 0; iC < iNCol; iC++)
    {
      fSum += fpX[iR * iNCol + iC] * fpBeta[iC];
    }

    fpY[iR] = fSum;
  }

  return 0;
}

int OLS_Apply_Beta (double *dpX, double *dpY, int iNRow, int iNCol, double *dpBeta)
/*
	Use Ordinary Least Squares to estimate Y.

	The mode is 

		Y = X*Beta

	where Y is an NRow X 1 vector
              X is an NRow X NCol matrix

	The Beta (generated by OLS_Estimate_Beta) is an NCol X 1 vector 
        of coefficients
*/
{
  for (int iR = 0; iR < iNRow; iR++)
  {
    double dSum = 0.f;
    for (int iC = 0; iC < iNCol; iC++)
    {
      dSum += dpX[iR * iNCol + iC] * dpBeta[iC];
    }

    dpY[iR] = dSum;
  }

  return 0;
}

int mti_round (float f)
{
	int i;
	if (f < 0.f)
	{
		i = -(int)(-f + .5f);
	}
	else
	{
		i = (int)(f + .5f);
	}

	return i;
}

int mti_round (double d)
{
	int i;
	if (d < 0.)
	{
		i = -(int)(-d + .5);
	}
	else
	{
		i = (int)(d + .5);
	}

	return i;
}

void LineIntersect (float fA1, float fB1, float fC1,
                    float fA2, float fB2, float fC2,
                    float &fX, float &fY)
{
	// the equation of the two lines are:
	//
	//        fA1 * x + fB1 * y = fC1
	//     
	//        fA2 * x + fB2 * y + fC2
	//

	// Find the intersection
	fX = (fB2*fC1 - fB1*fC2) / (fB2*fA1 - fB1*fA2);
	fY = (fA2*fC1 - fA1*fC2) / (fA2*fB1 - fA1*fB2);
}
