#ifndef AUTO_STABILIZE_RETURNS_H
#define AUTO_STABILIZE_RETURNS_H

/* Successful return */
#define ASTAB_RET_SUCCESS                    0 

/* Generic error, used when specific reason cannot be determined */
#define ASTAB_RET_FAILURE                   -1 

/* errors that occur when using the engine */
#define ASTAB_ERR_STRIDE                     -200000
#define ASTAB_ERR_N_FRAME                    -200001
#define ASTAB_ERR_ALLOC                      -200002
#define ASTAB_ERR_SEQUENCE                   -200003
#define ASTAB_ERR_IN_USE                     -200004
#define ASTAB_ERR_DIMENSION                  -200005
#define ASTAB_ERR_OUT_OF_BOUNDS              -200006
#define ASTAB_ERR_EST_BETA                   -200007
#define ASTAB_ERR_CRITICAL_SECTION           -200008
#define ASTAB_ERR_BTHREAD                    -200009
#define ASTAB_ERR_APPLY_BETA                 -200010


/* IPP errors */
#define ASTAB_ERR_IPP_ALLOC                  -201000
#define ASTAB_ERR_IPP_GET_SIZE               -201001
#define ASTAB_ERR_IPP_SET                    -201002
#define ASTAB_ERR_IPP_RESIZE                 -201003
#define ASTAB_ERR_IPP_COMPARE                -201004
#define ASTAB_ERR_IPP_FLOOD_FILL             -201005
#define ASTAB_ERR_IPP_DISTANCE_TRANSFORM     -201006
#define ASTAB_ERR_IPP_SUBTRACT               -201007
#define ASTAB_ERR_IPP_MINIMUM                -201008
#define ASTAB_ERR_IPP_COPY                   -201009
#define ASTAB_ERR_IPP_MEAN_STDDEV            -201010
#define ASTAB_ERR_IPP_DFT_INIT               -201011
#define ASTAB_ERR_IPP_MULTIPLY               -201012
#define ASTAB_ERR_IPP_SQROOT                 -201013
#define ASTAB_ERR_IPP_DIVIDE                 -201014
#define ASTAB_ERR_IPP_THRESHOLD              -201015
#define ASTAB_ERR_IPP_SUM                    -201016
#define ASTAB_ERR_IPP_DFT_FWD                -201017
#define ASTAB_ERR_IPP_DFT_INV                -201018
#define ASTAB_ERR_IPP_MUL_CONJ               -201019
#define ASTAB_ERR_IPP_ADD                    -201020
#define ASTAB_ERR_IPP_AND                    -201021
#define ASTAB_ERR_IPP_MAX_EVERY              -201022
#define ASTAB_ERR_IPP_MIN_EVERY              -201023
#define ASTAB_ERR_IPP_ABS_DIFF               -201024
#define ASTAB_ERR_IPP_CONV                   -201025
#define ASTAB_ERR_IPP_ABS                    -201026
#define ASTAB_ERR_IPP_SQUARE                 -201027
#define ASTAB_ERR_IPP_HISTOGRAM              -201028
#define ASTAB_ERR_IPP_GRADIENT               -201029
#define ASTAB_ERR_IPP_MAX_INDX               -201030
#define ASTAB_ERR_IPP_MEAN                   -201031
#define ASTAB_ERR_IPP_CROSS_CORR             -201032
#define ASTAB_ERR_IPP_MAX                    -201033

#endif

