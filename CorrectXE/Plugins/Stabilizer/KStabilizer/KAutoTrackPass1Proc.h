//---------------------------------------------------------------------------

#ifndef KAutoTrackPass1ProcH
#define KAutoTrackPass1ProcH

//---------------------------------------------------------------------------

#include "StabilizerTool.h"
#include "ImageDatumConvert.h"
#include "IppArray.h"

//---------------------------------------------------------------------------
// Arbitrary value added to Kevin's offsets so you can see the motion of
// a tracking box on the frame if you go to manual mode and load the .pts file.
#define AUTO_TRACK_OFFSET_REFERENCE_LEVEL 500

class CKAutoTrackPass1Proc : public CToolProcessor
{
public:
   CKAutoTrackPass1Proc(int newToolNumber, const string &newToolName,
                   CToolSystemInterface *newSystemAPI,
                   const bool *newEmergencyStopFlagPtr,
                   IToolProgressMonitor *newToolProgressMonitor);
   virtual ~CKAutoTrackPass1Proc();

   int SetIOConfig(CToolIOConfig *toolIOConfig);
   int SetFrameRange(CToolFrameRange *toolFrameRange);;

   CHRTimer HRT;

private:
   // Tool's functions called from within context of the processing loop.
   int SetParameters(CToolParameters *toolParams);
   int BeginProcessing(SToolProcessingData &procData);
   int EndProcessing(SToolProcessingData &procData);
   int BeginIteration(SToolProcessingData &procData);
   int EndIteration(SToolProcessingData &procData);
   int DoProcess(SToolProcessingData &procData);
   int GetIterationCount(SToolProcessingData &procData);

//	void WriteFile(const string &fileName, SToolProcessingData &procData);
	string getTrackPointDataString();

private:
   CStabilizerTool *_stabilizerToolObject = nullptr;  // pointer to CStabilizerTool object [EVIL!!!!]
   CStabilizerParameters _stabilizerParameters;    // private copy of parameters

   EngineArgs _autoEnginArgs;
   int _inFrameIndex = 0;
   int _outFrameIndex = 0;
   int _iterationCount = 0;
   IppiSize _imageSize = {0,0};
   IppiRect _roi;
   int _maxPixel = 0;
   bool _localAutoTrackPassTwo = false;
   CRegionOfInterest *_maskROI;
	RECT _boundingBox = {0};
};

#endif
