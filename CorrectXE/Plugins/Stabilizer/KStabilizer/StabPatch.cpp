
#include "stdafx.h"

#define USE_MTI_MALLOC_INSTEAD_OF_REGULAR_MALLOC
#include "MTImalloc.h"
#include "IniFile.h"

#include "AutoStabilizeReturns.h"
#include "StabPatch.h"

CStabPatch::CStabPatch ()
{
    fpRefPxy = 0;
}

CStabPatch::~CStabPatch ()
{
    free (fpRefPxy);
    fpRefPxy = 0;
}

int CStabPatch::Alloc (IppiSize sizeRefPxyArg, IppiSize sizeRefFullArg,
    IppiSize sizeImgPxyArg, IppiSize sizeImgFullArg)
{
    sizeRefPxy = sizeRefPxyArg;
    sizeImgPxy = sizeImgPxyArg;

    sizeRefFull = sizeRefFullArg;
    sizeImgFull = sizeImgFullArg;

    // allocate storage for the reference data
    fpRefPxy = (float *) malloc (sizeRefPxy.height * sizeRefPxy.width * 
        sizeof(float));
    fpRefFull = (float *) malloc (sizeRefFull.height * sizeRefFull.width * 
        sizeof(float));
    if (fpRefPxy == 0 || fpRefFull == 0)
    {
        return ASTAB_ERR_ALLOC;
    }

    // scaling factor to go from proxy to full
    fScaleFull = (float)sizeImgFull.width / (float)sizeImgPxy.width;

    return ASTAB_RET_SUCCESS;
}

int CStabPatch::AssignImage (int iRowPxy, int iColPxy, int iTime, float *fpPxy, float *fpFull)
{
    int iRet;

    iRet = Assign (iRowPxy, iColPxy, fpPxy, fpRefPxy, sizeRefPxy, sizeImgPxy);
    if (iRet)
    {
        return iRet;
    }

    // rescale to full resolution
    int iRowFull = (int) ( ((float)iRowPxy * fScaleFull) + .5f );
    int iColFull = (int) ( ((float)iColPxy * fScaleFull) + .5f );
    iRet = Assign (iRowFull, iColFull, fpFull, fpRefFull, sizeRefFull, sizeImgFull);
    if (iRet)
    {
        return iRet;
    }

    // append this first patch to the list
    Append (iTime, iRowPxy, iColPxy, (float)iRowFull, (float)iColFull);

    return ASTAB_RET_SUCCESS;
}

void CStabPatch::Append (int iTime, int iRowPxy, int iColPxy, float fRowFull, float fColFull)
{
    CTrackingPixel track;
    track.iTime = iTime;

    track.iRowPxy = iRowPxy;
    track.iColPxy = iColPxy;
    track.fRowFull = fRowFull;
    track.fColFull = fColFull;

    Track.push_back (track);
}

float *CStabPatch::getRef (int iType)
{
    float *fpRef;

    if (iType & PATCH_VALUE_PXY)
    {
        fpRef = fpRefPxy;
    }
    else 
    {
        fpRef = fpRefFull;
    }

    return fpRef;
}

int CStabPatch::getCount ()
{
    return (int)Track.size();
}

int CStabPatch::getTime (int iIdx)
{
    if (iIdx < 0 || iIdx >= Track.size())
    {
        return 0;
    }

    return Track.at(iIdx).iTime;
}

int CStabPatch::getRowPxy (int iIdx)
{
    if (iIdx < 0 || iIdx >= Track.size())
    {
        return 0;
    }

    return Track.at(iIdx).iRowPxy;
}

int CStabPatch::getColPxy (int iIdx)
{
    if (iIdx < 0 || iIdx >= Track.size())
    {
        return 0;
    }

    return Track.at(iIdx).iColPxy;
}

float CStabPatch::getRowFull (int iIdx)
{
    if (iIdx < 0 || iIdx >= Track.size())
    {
        return 0.f;
    }

    return Track.at(iIdx).fRowFull;
}

float CStabPatch::getColFull (int iIdx)
{
    if (iIdx < 0 || iIdx >= Track.size())
    {
        return 0.f;
    }

    return Track.at(iIdx).fColFull;
}

int CStabPatch::getFull (int iPxy)
{
    return (int)((float)iPxy * fScaleFull + .5f);
}

vector <CTrackingPixel> CStabPatch::getTrack ()
{
    return Track;
}

int CStabPatch::Assign (int iRow, int iCol, float *fpSrc, float *fpDst,
    IppiSize sizeRef, IppiSize sizeImg)
{
    IppStatus status;

    // clear the reference image
    status = ippiSet_32f_C1R (0.f, fpDst, 4*sizeRef.width, sizeRef);
    if (status)
    {
        return ASTAB_ERR_IPP_SET;
    }

    // find the upper left corner and lower right corner of the reference image
    int iRowRad = (sizeRef.height-1)/2;
    int iColRad = (sizeRef.width-1)/2;

    // upper left corner
    int iRowULC = iRow - iRowRad;
    int iColULC = iCol - iColRad;

    // lower right corner
    int iRowLRC = iRow + iRowRad;
    int iColLRC = iCol + iColRad;

    // copy the data
    IppiSize sizeCopy = sizeRef;
    int iRowColImg = iRowULC * sizeImg.width + iColULC;
    int iRowColRef = 0;
    if (iRowULC < 0)
    {
        iRowColImg -= (sizeImg.width*iRowULC);
        iRowColRef -= (sizeRef.width*iRowULC);
        sizeCopy.height += iRowULC;
    }

    if (iColULC < 0)
    {
        iRowColImg -= iColULC;
        iRowColRef -= iColULC;
        sizeCopy.width += iColULC;
    }

    if (iRowLRC >= sizeImg.height)
    {
        sizeCopy.height += (sizeImg.height-1-iRowLRC);
    }

    if (iColLRC >= sizeImg.width)
    {
        sizeCopy.width += (sizeImg.width-1-iColLRC);
    }

    status = ippiCopy_32f_C1R (fpSrc + iRowColImg, 4*sizeImg.width,
        fpDst + iRowColRef, 4*sizeRef.width, sizeCopy);
    if (status)
    {
        return ASTAB_ERR_IPP_COPY;
    }

    return ASTAB_RET_SUCCESS;
}
