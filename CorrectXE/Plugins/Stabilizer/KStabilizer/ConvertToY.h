#ifndef CONVERT_TO_Y_H
#define CONVERT_TO_Y_H

#include "AutoStabilizeDLL.h"

AUTO_STABILIZE_API int
    ConvertToY (int nRow, int nCol, float *fpRGB, float *fpY);

#endif
