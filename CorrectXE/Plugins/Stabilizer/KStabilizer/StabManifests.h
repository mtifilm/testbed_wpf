#ifndef STAB_MANIFESTS_H
#define STAB_MANIFESTS_H

#define PROXY_WIDTH 512
#define SUPER_RESOLUTION 5
#define MIN_THREAD 2
#define MAX_THREAD 64

#define IMAGE_RESOLUTION_FULL 0
#define IMAGE_RESOLUTION_PXY  1

#define PATCH_RADIUS 10
#define PATCH_SEARCH_MAX 10
#define PATCH_SEARCH_MIN 1
#define PATCH_THRESH 0.01f
#define PATCH_PXY_DISCREPANCY 2

#define TRACK_OFFSET_ROW_MAX 15 // motion offset at proxy resolution
#define TRACK_OFFSET_COL_MAX 15 // motion offset at proxy resolution
#define PXY_OFFSET_REFINE 2
#define TRACK_MIN_LENGTH 10

#define LOCKED_CAMERA_THRESH 2.f  // pixels in proxy resolution
#define MAX_CORRECTION 2.5f       // maximum allowed correction

#define CALC_HAT_DEGREE 2
#define CALC_HAT_RADIUS 5

#define PROCESS_FLAG_NONE 0x0
#define PROCESS_FLAG_LOCKED_CAMERA 0x1


#endif
