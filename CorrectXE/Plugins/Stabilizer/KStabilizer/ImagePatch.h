#ifndef ImagePatchH
#define ImagePatchH

#include <mutex>
#include <map>
#include "Ippheaders.h"

//#define TIME_CLASS() this->TimeMetrices(__func__, __lineNumber__);
//#define TIME_CLASS(NAME) this->TimeMetrices(__func__, __lineNumber__, NAME);
//
//class TimeMetricsData
//{
//public:
//	TimeMetricsData(std::string &func, std::string &lineNumber, std::string &comment) : TimeMetricsData(func, lineNumber)
//	{
//		_comment = comment;
//	}
//
//	TimeMetricsData(std::string &func, std::string &lineNumber)
//	{
//		_key = func + "; " + lineNumber;;
//	}
//
//	~TimeMetricsData()
//	{
//		if (_autoStore)
//		{
//			store();
//		}
//	}
//
//	void store()
//	{
//		*_metrics[_key] = _hrt.elapsedMilliseconds();
//		_autoStore = false;
//	}
//
//private:
//	HRTimer _hrt;
//	bool _autoStore = true;
//	std::string _key;
//	std::string _comment;
//	std::map <string, double> *_metrics;
//};

class ImagePatch
{
public:
	ImagePatch();
	~ImagePatch();

	void setKernelSize(const IppiSize &kernelSize)
	{
		_kernelSize = kernelSize;
	}

	IppiSize getKernelSize() const { return _kernelSize; }

	void setImage(const Ipp32fArray &originalImage);
	void setImage(const IppiSize &size, float *imageData, float *proxyData = nullptr);

	Ipp32fArray &calcMinimumImage();
	void doSearch(const Ipp32fArray &inImageSlice, Ipp32fArray &minDiffImage);

   Ipp32fArray &getImage() { return _originalImage; }
   Ipp32fArray &getProxy() { return _proxyImage; }
   Ipp32fArray &getMinDiffProxy() { return _minDiffProxy; }

   std::map<string, double> metrics;


private:
	MtiSize getProxySizeFromOriginal() const;
	void allocOnSizeChange();

	vector<MtiPoint> &getSearchPatchOffsets();

	MtiRect getProxySearchRoi() const;

	Ipp32fArray _originalImage;
	Ipp32fArray _minDiffProxy;
	Ipp32fArray _proxyImage;
	Ipp8uArray _convScratch[2];
	Ipp32fArray _diffScratch[2];
	Ipp32fArray _tempMinDiff;

	Ipp32s	_proxyWidth = 512;
	IppiSize _kernelSize = { 21, 21 };

	// Search offsets, should be a class
	vector<MtiPoint> _searchOffsets;
	MtiSize _searchOffsetSize;
	int _downSample = 2;
	std::mutex _patchMutex;
};

#endif

