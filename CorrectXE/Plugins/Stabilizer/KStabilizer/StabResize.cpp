
#include "stdafx.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "AutoStabilizeReturns.h"
#include "StabManifests.h"
#include "StabResize.h"

CStabResize::CStabResize ()
{
    pSpec = 0;
    pInit = 0;
    pBuffer = 0;
}

CStabResize::~CStabResize ()
{
    Free ();
}

int CStabResize::Alloc (IppiSize sizeSrcArg, IppiSize sizeDstArg)
{
    sizeSrc = sizeSrcArg;
    sizeDst = sizeDstArg;

    // allocate storage for the IPPresize.  Either ippLinear or ippLanczos
    IppiInterpolationType interpolation = ippLanczos;
    int antialiasing = 1;

    IppStatus status;
    int specSize=0, initSize=0;

    status = ippiResizeGetSize_32f (sizeSrc, sizeDst, interpolation,
        antialiasing, &specSize, &initSize);
    if (status)
    {
        return ASTAB_ERR_IPP_GET_SIZE;
    }

    pSpec = (IppiResizeSpec_32f *) ippMalloc (specSize);
    pInit = (Ipp8u *) ippMalloc (initSize);
    
    if (pSpec == 0 || pInit == 0)
    {
        return ASTAB_ERR_IPP_ALLOC;
    }

    // initialize the filter
    if (interpolation == ippLanczos)
    {
        status = ippiResizeAntialiasingLanczosInit (sizeSrc, sizeDst, 3,
            pSpec, pInit);
    }
    else
    {
        status = ippiResizeAntialiasingLinearInit (sizeSrc, sizeDst,
            pSpec, pInit);
    }
    if (status)
    {
        return ASTAB_ERR_IPP_RESIZE;
    }

    // the size of the work space
    int bufSize = 0;
    status = ippiResizeGetBufferSize_32f (pSpec, sizeDst, 1, &bufSize);
    if (status)
    {
        return ASTAB_ERR_IPP_GET_SIZE;
    }

    pBuffer = (Ipp8u *) ippMalloc (bufSize);
    if (pBuffer == 0)
    {
        return ASTAB_ERR_IPP_ALLOC;
    }

    return ASTAB_RET_SUCCESS;
}

int CStabResize::Apply (float *fpSrc, float *fpDst)
{
    IppStatus status;
    IppiPoint pxyOffset;
    IppiBorderType border = ippBorderRepl;
    float borderVal = 0.f;

    pxyOffset.x = 0;
    pxyOffset.y = 0;

    // resize to the source image to destination
    status = ippiResizeAntialiasing_32f_C1R (fpSrc, 4*sizeSrc.width,
         fpDst, 4*sizeDst.width, pxyOffset, sizeDst, border, &borderVal, 
         pSpec, pBuffer);
    if (status)
    {
        return ASTAB_ERR_IPP_RESIZE;
    }

    return ASTAB_RET_SUCCESS;
}

int CStabResize::Free ()
{
    ippFree (pSpec);
    pSpec= 0;
    ippFree (pInit);
    pInit= 0;
    ippFree (pBuffer);
    pBuffer = 0;

    return ASTAB_RET_SUCCESS;
}
