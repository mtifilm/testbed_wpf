#ifndef AUTOSTABILIZEINTERFACEH
#define AUTOSTABILIZEINTERFACEH

#include "AutoStabilizeDLL.h"
#include "AutoStabilizeArgs.h"

/*
    The Auto Stabilize library is structured as a render engine.  The 
    caller must first allocate an engine before using it.  The caller
    must free the engine when it is no longer needed.

    There are two ways to allocate/free an engine:  
        (1) AutoStabilizeCreate() and AutoStabilizeDestroy()
        (2) AutoStabilizeNew(), AutoStabilizeAlloc(), AutoStabilizeFree(), 
            AutoStabilizeDelete()

    In approach (1), the engine is used for a single shot and then destroyed.
    In approach (2), the engine is re-used by calling Alloc() before each 
    shot and Free () after each shot.

    The caller may make an unlimited number of calls to the engine to 
    either provide information to the engine or extract information from
    the engine.

    After allocating the engine, the caller must call AutoStablizeFirstPass()
    for each frame in the sequence.  During these calls, the engine
    analyzes the user data and calculates the amount of correction 
    for each frame.

    The calls to FirstPass() must be sequential.  For example, FirstPass
    must be called for frame 0 and it must return before calling 
    FirstPass for frame 1.  Etc.

    After passing all the frames to AutoStabilizeFirstPass(), the caller
    may call AutoStabilizeGetOffset() to retieve the offset corrections.

    This interface defines the following function prototypes:

    AutoStabilizeCreate
        INPUT ARGUMENTS: 
            engine:      a structure with information necessary to 
                         configure the engine.

        OUTPUT ARGUMENTS:
            *ipRet:      a return value signifying success or failure.
                         See AutoStablizeReturns.h for a list of values.

        RETURN VALUE:    a void pointer to the engine.

    AutoStabilizeDestroy
        INPUT ARGUMENTS:
            vp:          a void pointer to the engine.

        OUTPUT ARGUMENTS:
            none

        RETURN VALUE:    an integer signifying success or failure.
                         See AutoStablizeReturns.h for a list of values.


    AutoStabilizeNew
        INPUT ARGUMENTS: 
            none

        OUTPUT ARGUMENTS:
            none

        RETURN VALUE:    a void pointer to the engine.


    AutoStabilizeDelete
        INPUT ARGUMENTS:
            vp:          a void pointer to the engine.

        OUTPUT ARGUMENTS:
            none

        RETURN VALUE:    an integer signifying success or failure.
                         See AutoStablizeReturns.h for a list of values.


    AutoStabilizeAlloc
        INPUT ARGUMENTS: 
            vp:          a void pointer to the engine.
            engine:      a structure with information necessary to 
                         configure the engine.

        OUTPUT ARGUMENTS:
            none

        RETURN VALUE:    a void pointer to the engine.


    AutoStabilizeFree
        INPUT ARGUMENTS:
            vp:          a void pointer to the engine.

        OUTPUT ARGUMENTS:
            none

        RETURN VALUE:    an integer signifying success or failure.
                         See AutoStablizeReturns.h for a list of values.
         

    AutoStabilizeFirstPass
        INPUT ARGUMENTS:
            vp:          a void pointer to the engine.
            iFrame:      the index of the frame in the sequence.  This value
                         should range between 0 and (iNFrame-1).
            fpY:         a pointer to a frame of image data.  The data should
                         range between 0 and 1.

        OUTPUT ARGUMENTS:
            none

        RETURN VALUE:    an integer signifying success or failure.
                         See AutoStablizeReturns.h for a list of values.


    // NOTE: There is now only one pass - second analysis pass was eliminated!

                         
    AutoStabilizeGetOffset
        INPUT ARGUMENTS:
            vp:          a void pointer to the engine.
            iFrame:      the index of the frame in the sequence.  This value
                         should range between 0 and (iNFrame-1).

        OUTPUT ARGUMENTS:
            fpRowOff:    the offset in the vertical direction.  Positive
                         values mean move the image down.  Negative values
                         mean move the image up.
            fpColOff:    the offset in the horizontal direction.  Positive
                         values mean move the image right.  Negative values
                         mean move the image left.

        RETURN VALUE:    an integer signifying success or failure.
                         See AutoStablizeReturns.h for a list of values.
*/

AUTO_STABILIZE_API void *
    AutoStabilizeCreate ( EngineArgs engine, int *ipRet );

AUTO_STABILIZE_API int
    AutoStabilizeDestroy ( void *vpEngine );

AUTO_STABILIZE_API void *
    AutoStabilizeNew ( );

AUTO_STABILIZE_API void
    AutoStabilizeDelete ( void *vpEngine );

AUTO_STABILIZE_API int
    AutoStabilizeAlloc ( void *vpEngine, EngineArgs engine );

AUTO_STABILIZE_API int
    AutoStabilizeFree ( void *vpEngine );

// NOTE: There is now only one pass - second analysis pass was eliminated!
AUTO_STABILIZE_API int
     AutoStabilizeFirstPass ( void *vpEngine, int iFrame, float *fpY );

AUTO_STABILIZE_API int
     AutoStabilizeGetOffset ( void *vpEngine, int iFrame, float *fpRowOff,
                              float *fpColOff );

#endif
