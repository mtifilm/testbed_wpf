#include "ImagePatch.h"
#include "IpaStripeStream.h"
#include "HRTimer.h"

// Why this is defined, I have no idea
#define PATCH_SEARCH_MIN 1

ImagePatch::ImagePatch()
{
}

ImagePatch::~ImagePatch()
{
}

void ImagePatch::setImage(const IppiSize &size, float *imageData, float *proxyData)
{
    Ipp32fArray unmanagedImage(size, imageData);
	if (proxyData == nullptr)
	{
		setImage(unmanagedImage);
		return;
	}

	_originalImage = unmanagedImage;
	auto proxySize = getProxySizeFromOriginal();
	_proxyImage = Ipp32fArray(proxySize, proxyData);
	
	allocOnSizeChange();
}

void ImagePatch::setImage(const Ipp32fArray & originalImage)
{
	if (originalImage.getComponents() != 1)
	{
		throw std::runtime_error("Channels must be 1");
	}

	_originalImage = originalImage;

	CHRTimer hrt;
	_proxyImage = _originalImage.resize(getProxySizeFromOriginal());
	metrics["calcMinimumImage"] = hrt.elapsedMilliseconds();

	allocOnSizeChange();
	metrics["setImage"] = hrt.elapsedMilliseconds();
}

Ipp32fArray &ImagePatch::calcMinimumImage()
{
	if (_originalImage.isEmpty())
	{
		throw std::runtime_error("setImage must be called first");
	}

	CHRTimer hrt;
	_minDiffProxy.set({ 100.0f });
	metrics["set"] = hrt.elapsedMilliseconds();

	auto widthRadius = _kernelSize.width / 2;
	auto heightRadius = _kernelSize.height / 2;
	auto &offsets = getSearchPatchOffsets();

	// This ROI is the data used for analysis, it is smaller than the
	// fullsize by twice kernel radius because the ROI is moved around to find min.
	IpaStripeStream iss;
	iss << getProxySearchRoi().inflate(-widthRadius, -heightRadius);
	auto func0 = [&](const MtiRect &stripedRoi, int jobNumber)
	{
		auto inputRoi = stripedRoi.inflate(widthRadius, heightRadius);

		// The scratch images needs to be the same size as the input but the
		// inflated strips overlap, so we must use two of them
		auto inputDiffRoi = _diffScratch[jobNumber % 2](inputRoi);
		auto scratchRoi = _convScratch[jobNumber % 2](inputRoi);

		// No overlap so we only need 1
		auto tempMinDiffRoi = _tempMinDiff(stripedRoi);
		auto minDiffProxyRoi = _minDiffProxy(stripedRoi);

		for (auto offset : offsets)
		{
			_proxyImage(inputRoi).absDiff(_proxyImage(inputRoi + offset), inputDiffRoi);
			inputDiffRoi.applyBoxFilterValid(_kernelSize, tempMinDiffRoi, scratchRoi);
			minDiffProxyRoi.minEveryInplace(tempMinDiffRoi);
		}
	};

	// 14 jobs seems good on 8 and above processor machines
   // WARNING 16 is maximum because the strips get too small
	iss << efu_roi_job(func0);  // CAST is only because Embarcardo has a bug in its resolution
	iss.stripe(14);

	metrics["calcMinimumImage"] = hrt.elapsedMilliseconds();

   return _minDiffProxy;
}

void ImagePatch::doSearch(const Ipp32fArray &inImageSlice, Ipp32fArray &minDiffImage)
{

}

MtiSize ImagePatch::getProxySizeFromOriginal() const
{
	if (_originalImage.isEmpty())
	{
		return { 0, 0 };
	}

	auto height = (int)((float)(_originalImage.getHeight() * _proxyWidth) /
		(float)_originalImage.getWidth() + .5f);

	return { _proxyWidth, height };
}

void ImagePatch::allocOnSizeChange()
{
	// This is new proxy size
	auto proxySize = _proxyImage.getSize();

	// Realloc memory if necessary
	if (proxySize != _minDiffProxy.getSize())
	{
		CHRTimer hrt;
		_minDiffProxy = Ipp32fArray(proxySize);
		_convScratch[0] = Ipp8uArray(proxySize);
		_convScratch[1] = Ipp8uArray(proxySize);
		_diffScratch[0] = Ipp32fArray(proxySize);
		_diffScratch[1] = Ipp32fArray(proxySize);
		_tempMinDiff = Ipp32fArray(proxySize);
		metrics["allocOnSizeChange"] = hrt.elapsedMilliseconds();
	}
}

vector<MtiPoint> &ImagePatch::getSearchPatchOffsets()
{
	std::lock_guard<std::mutex> lock(_patchMutex);

	if ((_kernelSize.height == _searchOffsetSize.height) && (_kernelSize.width == _searchOffsetSize.width))
	{
		return _searchOffsets;
	}

	CHRTimer hrt;
	auto searchRoi = getProxySearchRoi();
	auto rowSearch = searchRoi.y;
	auto colSearch = searchRoi.x;

	// This is maximum possible
	_searchOffsets.reserve(((2 * colSearch + 1) * (2 * rowSearch + 1)) / _downSample);
	_searchOffsets.clear();

	for (int iRowOff = -rowSearch; iRowOff <= rowSearch; iRowOff += _downSample)
	{
		for (int iColOff = -colSearch; iColOff <= colSearch; iColOff += _downSample)
		{
			float fOffset = std::sqrt((float)(iRowOff*iRowOff) + (float)(iColOff*iColOff));

			if (fOffset > PATCH_SEARCH_MIN)
			{
				_searchOffsets.emplace_back(iColOff, iRowOff);
			}
		}
	}

	_searchOffsetSize = _kernelSize;

	metrics["calcSearchPatchOffsets"] = hrt.elapsedMilliseconds();
	return _searchOffsets;
}

MtiRect ImagePatch::getProxySearchRoi() const
{
	if (_proxyImage.isEmpty())
	{
		throw std::runtime_error("setImage must be called first");
	}

	// Kevin's code uses kernel to do the search area
	auto kw2 = _kernelSize.width / 2;
	auto kh2 = _kernelSize.height / 2;

	return { kw2, kh2, _proxyImage.getWidth() - 2 * kw2, _proxyImage.getHeight() - 2 * kh2 };
}
