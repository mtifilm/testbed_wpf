#ifndef STABRESIZEH
#define STABRESIZEH

#include "ipp.h"
#include "StabManifests.h"

class CStabResize
{
public:
    CStabResize ();
    ~CStabResize ();

    int Alloc (IppiSize sizeSrcArg, IppiSize sizeDstArg);
    int Free ();
    int Apply (float *fpSrc, float *fpDst);

private:
    IppiSize sizeSrc;
    IppiSize sizeDst;
    IppiResizeSpec_32f *pSpec;
    Ipp8u *pInit;
    Ipp8u *pBuffer;
};

#endif
