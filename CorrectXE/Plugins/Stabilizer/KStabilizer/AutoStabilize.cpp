
#include "stdafx.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "mti_math.h"
#include "bthread.h"

#include "AutoStabilize.h"
#include "AutoStabilizeReturns.h"
#include "StabData.h"
#include "StabPatch.h"
#include "StabTrack.h"
#include "StabManifests.h"

#ifdef _DEBUG
//#define SINGLE_THREAD
//#define DEBUG_PRINT
#endif

void ProcessPatchesSproc (void *vp, void *vpReserved);

CAutoStabilize::CAutoStabilize ()
{
    pPxyMotionBuf = 0;
    pFullMotionBuf = 0;

    iFrameExpected = -1;
}

CAutoStabilize::~CAutoStabilize()
{
    Free ();
}

int CAutoStabilize::Alloc (EngineArgs engine)
{
    int iRet;

    // free up the last one
    iRet = Free ();
    if (iRet)
    {
        return iRet;
    }

    if (engine.iNRow <= 0 || engine.iNCol <= 0)
    {
        return ASTAB_ERR_DIMENSION;
    }

    if (engine.iNFrame <= 0)
    {
        return ASTAB_ERR_N_FRAME;
    }

    if ( engine.iNCol > engine.iNStride)
    {
        return ASTAB_ERR_STRIDE;
    }

    iNThread = std::max<int>(std::min<int>(engine.iNThread, MAX_THREAD), MIN_THREAD);

    // allocate the classes
    pPxyMotionBuf = new CMotionBuffer;
    pFullMotionBuf = new CMotionBuffer;

    iRet = Data.Alloc (engine.iNFrame, engine.iNRow, engine.iNCol, engine.iNStride, iNThread);
    if (iRet)
    {
        return iRet;
    }

    // allocate motion memory
    iRet = AllocMotion ();
    if (iRet)
    {
        return iRet;
    }

    iNFrame = engine.iNFrame;
    iFrameExpected = 0;
    iFrameProcess = 0;

    fLockedCameraThresh = LOCKED_CAMERA_THRESH * Data.getScale (IMAGE_RESOLUTION_PXY, IMAGE_RESOLUTION_FULL);
    fMaxCorrection = MAX_CORRECTION * Data.getScale (IMAGE_RESOLUTION_PXY, IMAGE_RESOLUTION_FULL);

    iProcessFlag = PROCESS_FLAG_NONE;
    if (engine.iAnimation)
    {
        iProcessFlag |= PROCESS_FLAG_LOCKED_CAMERA;
    }

    return ASTAB_RET_SUCCESS;
}

int CAutoStabilize::AllocMotion ()
{
    int iRet;

    // the proxy patch size is (2*PATCH_RADIUS+1) * (2*PATCH_RADIUS+1)
    IppiSize sizePatchPxy;
    sizePatchPxy.height = 2*PATCH_RADIUS + 1;
    sizePatchPxy.width = 2*PATCH_RADIUS + 1;
    iRet = pPxyMotionBuf->Alloc (true, iNThread, sizePatchPxy, Data.getImageSize(IMAGE_RESOLUTION_PXY), 
        TRACK_OFFSET_ROW_MAX, TRACK_OFFSET_COL_MAX);
    if (iRet)
    {
        return iRet;
    }

    // the full patch size is scaled from the proxy
    float fScaleFromPxy = (float)Data.getImageSize(IMAGE_RESOLUTION_FULL).width /
        (float)Data.getImageSize(IMAGE_RESOLUTION_PXY).width;

    IppiSize sizePatchFull;
    int iRadiusFull = (int) ( (float)PATCH_RADIUS * fScaleFromPxy + .5f );
    sizePatchFull.height = 2*iRadiusFull + 1;
    sizePatchFull.width = 2*iRadiusFull + 1;
    int iMaxRowOffFull = (int)((fScaleFromPxy * PXY_OFFSET_REFINE) + .5f);
    int iMaxColOffFull = (int)((fScaleFromPxy * PXY_OFFSET_REFINE) + .5f);

    iRet = pFullMotionBuf->Alloc (false, iNThread, sizePatchFull, Data.getImageSize(IMAGE_RESOLUTION_FULL), 
        iMaxRowOffFull, iMaxColOffFull);
    if (iRet)
    {
        return iRet;
    }

    return ASTAB_RET_SUCCESS;
}

int CAutoStabilize::Free ()
{
    int iRet;
    iRet = Data.Free ();
    if (iRet)
    {
        return (iRet);
    }

    vector <CStabPatch *>::iterator pos;
    for (pos = Patch.begin(); pos != Patch.end(); pos++)
    {
        delete (*pos);
    }
    Patch.clear ();
    TrackList.clear ();

    delete pPxyMotionBuf;
    pPxyMotionBuf = 0;
    delete pFullMotionBuf;
    pFullMotionBuf = 0;

    listRowOff.clear();
    listColOff.clear();

    return ASTAB_RET_SUCCESS;
}

// NOTE: There is now only one pass - second analysis pass was eliminated!
int CAutoStabilize::FirstPass (int iFrame, float *fpY)
{
    int iRet;

    // the FirstPass function must be called for each frame in sequence
    if (iFrame < 0 || iFrame >= iNFrame || iFrame != iFrameExpected)
    {
        return ASTAB_ERR_SEQUENCE;
    }

#ifdef DEBUG_PRINT_X
    FILE *fpYF;
    char caYName[64];
    sprintf_s (caYName, 64, "c:/tmp/AS/Y%.4d.dat", iFrame);
    fopen_s (&fpYF, caYName, "w");
    IppiSize imgSize = Data.getImageSize(IMAGE_RESOLUTION_FULL);
    int left = imgSize.width / 2 - 100;
    int right = imgSize.width / 2 + 100;
    int top = imgSize.height / 2 - 100;
    int bottom = imgSize.height / 2 + 100;
    for (int i = top; i < bottom; ++i)
    {
        fprintf (fpYF, "Row %d:\n", i);
        for (int j = left; j < right; ++j)
        {
            fprintf (fpYF, "%f\n", fpY[i*imgSize.width + j]);
        }
    }

    fclose (fpYF);
#endif

    // keep track of the image data
    iRet = Data.FirstPass (fpY);
    if (iRet)
    {
        return iRet;
    }

#ifdef DEBUG_PRINT_X
    {
    FILE *fpYF;
    char caYName[64];
    sprintf_s (caYName, 64, "c:/tmp/AS/Y%.4d.dat", iFrame);
    fopen_s (&fpYF, caYName, "w");
    IppiSize imgSize = Data.getImageSize(IMAGE_RESOLUTION_PXY);
    float *fpPxy = Data.getImage (IMAGE_RESOLUTION_PXY);
    for (int i = 0; i < imgSize.height; ++i)
    {
        for (int j = 0; j < imgSize.width; ++j)
        {
            fprintf (fpYF, "%f ", *fpPxy++);
        }
        fprintf (fpYF, "\n");
    }

    fclose (fpYF);
    }
#endif

    // process the patches
    iRet = ProcessPatches (iFrame);
    if (iRet)
    {
        return iRet;
    }

    // clean the patches
    iRet = CleanPatches (iFrame);
    if (iRet)
    {
        return iRet;
    }

    // append the list of patches used in tracking
    iRet = AppendPatches (iFrame);
    if (iRet)
    {
        return iRet;
    }

    iFrameExpected++;

    if (iFrameExpected == iNFrame)
    {
        // first pass is finished.

        iRet = FitTrajectory ();
        if (iRet)
        {
            return iRet;
        }

        iRet = FindOffset ();
        if (iRet)
        {
            return iRet;
        }

#ifdef DEBUG_PRINT_X
        {
            // all trajectories passing through each frame
            for (int iFr = 0; iFr < iNFrame; iFr++)
            {
                FILE *fpF;
                char caName[64];
                sprintf_s (caName, 64, "c:/tmp/AS/F%.4d.dat", iFr);
                fopen_s (&fpF, caName, "w");
                for (int iTrack = 0; iTrack != TrackList.size(); iTrack++)
                {
                    int iNPix = TrackList.at(iTrack).getNPix();
                    for (int iPix = 0; iPix < iNPix; iPix++)
                    {
                        if (TrackList.at(iTrack).getTime(iPix) == iFr)
                        {
                            fprintf (fpF, "%d %d %d %f %f\n", iTrack,
                                TrackList.at(iTrack).getTime(iPix),
                                TrackList.at(iTrack).getRowFull(iPix), TrackList.at(iTrack).getColFull(iPix),
                                iNPix);
                        }
                    }
                }
                fclose (fpF);
            }
        }
#endif

#ifdef DEBUG_PRINT
{
    // all trajectories
    for (int iTrack = 0; iTrack != TrackList.size(); iTrack++)
    {
        FILE *fpT;
        char caName[64];
        sprintf_s (caName, 64, "c:/tmp/AS/T%.4d.dat", iTrack);
        fopen_s (&fpT, caName, "w");
        int iNPix = TrackList.at(iTrack).getNPix();
        for (int iPix = 0; iPix < iNPix; iPix++)
        {
            fprintf (fpT, "%d %f %f %f %f %f %f\n",
                TrackList.at(iTrack).getTime(iPix),
                TrackList.at(iTrack).getRowFull(iPix), TrackList.at(iTrack).getColFull(iPix),
                TrackList.at(iTrack).getRowHat(iPix), TrackList.at(iTrack).getColHat(iPix),
                TrackList.at(iTrack).getRowOff(iPix), TrackList.at(iTrack).getColOff(iPix));
        }
        fclose (fpT);
    }
}
#endif
    }

    return ASTAB_RET_SUCCESS;
}

int CAutoStabilize::GetOffset (int iFrame, float *fpRowOff, float *fpColOff)
{
    *fpRowOff = 0.f;
    *fpColOff = 0.f;

    if (iFrame < 0 || iFrame >= iNFrame || iFrameExpected != iNFrame)
    {
        return ASTAB_ERR_SEQUENCE;
    }


    *fpRowOff = listRowOff.at(iFrame);
    *fpColOff = listColOff.at(iFrame);

    return ASTAB_RET_SUCCESS;
}

int CAutoStabilize::RoundF (float arg)
{
    int result;

    if (arg < 0.f)
    {
        result = -(int)(-arg+.5f);
    }
    else
    {
        result = (int)(arg+.5f);
    }

    return result;
}

int CAutoStabilize::FitTrajectory ()
{
    int iRet;
    vector <CStabTrack>::iterator pos;
    for (pos = TrackList.begin(); pos != TrackList.end(); pos++)
    {
        iRet = pos->Fit (fLockedCameraThresh);
        if (iRet)
        {
            return iRet;
        }
    }
       
    return ASTAB_RET_SUCCESS;
}

int CAutoStabilize::FindOffset ()
{
    // clear previous list
    listRowOff.clear();
    listColOff.clear();

    // at each frame find the median correction
    for (int iFrame = 0; iFrame < iNFrame; iFrame++)
    {
        // find the trajectories passing through this frame
        vector <float> listRowLocal;
        vector <float> listColLocal;

        vector <CStabTrack>::iterator pos;
        for (pos = TrackList.begin(); pos != TrackList.end(); pos++)
        {
            int iNPix = pos->getNPix ();
            if (iFrame >= pos->getTime(0) && iFrame <= pos->getTime(iNPix-1))
            {
                int iIndex = iFrame - pos->getTime(0);
                float fRowOff = pos->getRowOff (iIndex);
                float fColOff = pos->getColOff (iIndex);

                // is the offset too large to use?
                float fDist = sqrt (fRowOff * fRowOff + fColOff * fColOff);
                if (fDist < fMaxCorrection)
                {
                    listRowLocal.push_back (fRowOff);
                    listColLocal.push_back (fColOff);
                }
            }
        }

        // find the median offset at this frame
        float fRowMedian;
        float fColMedian;
        VectorMedian (listRowLocal, listColLocal, fRowMedian, fColMedian);
        listRowOff.push_back (fRowMedian);
        listColOff.push_back (fColMedian);
    }

    return ASTAB_RET_SUCCESS;
}

void CAutoStabilize::VectorMedian (vector <float> &listRow, vector <float> &listCol, float &fRowMedian, float &fColMedian)
{
    int iN = (int)listRow.size();

    if (iN == 0)
    {
        // no points in this frame
        fRowMedian = 0.f;
        fColMedian = 0.f;
    }
    else
    {
        // find the point that is closest to all the others
        float fBestDist = -1.f;
        int iBest = -1;
        for (int i = 0; i < iN; i++)
        {
            float fTotalDist = 0.f;
            for (int j = 0; j < iN; j++)
            {
                float fDeltaRow = listRow.at(i) - listRow.at(j);
                float fDeltaCol = listCol.at(i) - listCol.at(j);
                float fDist = sqrt (fDeltaRow*fDeltaRow + fDeltaCol*fDeltaCol);

                fTotalDist += fDist;
            }

            if (fBestDist == -1.f || fTotalDist < fBestDist)
            {
                fBestDist = fTotalDist;
                iBest = i;
            }
        }

        fRowMedian = listRow.at(iBest);
        fColMedian = listCol.at(iBest);
    }
}


int CAutoStabilize::AppendPatches (int iTime)
{
    int iRet;

    if (iTime == (iNFrame - 1))
    {
        // last frame, no need for more patches
        return ASTAB_RET_SUCCESS;
    }

    // pre-process the proxy image to look for good patches
    iRet = Data.FindPatches ();
    if (iRet)
    {
        return iRet;
    }

    // exclude the currently active patches
    vector <CStabPatch *>::iterator pos;
    for (pos = Patch.begin(); pos != Patch.end(); pos++)
    {
        int iRow = (*pos)->getRowPxy ((*pos)->getCount()-1);
        int iCol = (*pos)->getColPxy ((*pos)->getCount()-1);

        iRet = Data.Exclude (iRow, iCol);
        if (iRet)
        {
            return iRet;
        }
    }

    // add more patches
    bool bKeepLooking = true;
    while (bKeepLooking)
    {
        int iRow;
        int iCol;
        iRet = Data.getPatch (iRow, iCol, bKeepLooking);
        if (iRet)
        {
            return iRet;
        }

        if (bKeepLooking)
        {
            // create a new patch and append it
            CStabPatch *patch = new (CStabPatch);

            iRet = patch->Alloc (pPxyMotionBuf->getSizeRef (), 
                pFullMotionBuf->getSizeRef (),
                Data.getImageSize(IMAGE_RESOLUTION_PXY),
                Data.getImageSize(IMAGE_RESOLUTION_FULL));
            if (iRet)
            {
                delete patch;
                return iRet;
            }

            iRet = patch->AssignImage (iRow, iCol, iTime, Data.getImage(IMAGE_RESOLUTION_PXY),
                Data.getImage(IMAGE_RESOLUTION_FULL));
            if (iRet)
            {
                delete patch;
                return iRet;
            }

            Patch.push_back(patch);
        }
    }

#ifdef DEBUG_PRINT_X
    {
    FILE *fpP;
    char caName[64];
    sprintf_s (caName, 64, "c:/tmp/AS/P%.4d.dat", iTime);
    fopen_s (&fpP, caName, "w");

    for (pos = Patch.begin(); pos != Patch.end(); pos++)
    {
        int iRow = (*pos)->getValue (PATCH_VALUE_PXY | PATCH_VALUE_LAST | PATCH_VALUE_ROW);
        int iCol = (*pos)->getValue (PATCH_VALUE_PXY | PATCH_VALUE_LAST | PATCH_VALUE_COL);

        fprintf (fpP, "%d %d\n", iRow,iCol);
    }


    fclose (fpP);
    }
#endif

    return ASTAB_RET_SUCCESS;
}

int CAutoStabilize::ProcessPatches (int iTime)
{
    if (Patch.size() == 0)
    {
        return ASTAB_RET_SUCCESS;
    }

    // break the patches across the threads
    iSprocRet = ASTAB_RET_SUCCESS;
    iSprocTime = iTime;

#ifdef SINGLE_THREAD

    for (iSprocThread = 0; iSprocThread < iNThread; iSprocThread++)
    {
        int iRet = doProcessPatches (iSprocThread);
        if (iRet)
        {
            iSprocRet = iRet;
        }
	 }

#else

	 iSprocRun = iNThread;
	 SprocsDone.reset();

	 for (iSprocThread = 0; iSprocThread < iNThread; iSprocThread++)
	 {
        if (BThreadSpawn (ProcessPatchesSproc, (void*) this) == 0)
        {
            iSprocRet = ASTAB_ERR_BTHREAD;
        }
    }

	// Wait up to 5 seconds
	if (!SprocsDone.wait(5000))
    {
		TRACE_0(errout << "HUNG in ProcessPatches");
    }

#endif

    if (iSprocRet)
    {
        return iSprocRet;
    }

    return ASTAB_RET_SUCCESS;
}

int CAutoStabilize::CleanPatches (int iTime)
{
    // remove any patches that are stale.
    vector <CStabPatch *>::iterator pos = Patch.begin();
    while (pos != Patch.end())
    {
        int iLast = (*pos)->getTime ((*pos)->getCount()-1);

        // if this patch did not find a match or if this is the final frame, remove the patch
        if ( (iTime != iLast) || (iTime == iNFrame-1) )
        {
            // is the track long enough to keep
            int iCnt = (*pos)->getCount ();
            if (iCnt > TRACK_MIN_LENGTH)
            {
                // keep this track
                CStabTrack Track;
// this gets a compiler error in Borland
//                Track.Assign ((*pos)->getTrack());
//
                auto tempTrack = ((*pos)->getTrack());
                Track.Assign (tempTrack);
//////
                TrackList.push_back (Track);
            }

            // this patch should be removed
            delete (*pos);
            pos = Patch.erase (pos);
        }
        else
        {
            // advance to next patch
            pos++;
        }
    }

    return ASTAB_RET_SUCCESS;
}

int CAutoStabilize::doProcessPatches (int iThread)
{
    int iRet;
    int iNPatch = (int)Patch.size();

    // use this thread to process some of the patches
    for (int iPatch = iThread; iPatch < iNPatch; iPatch += iNThread)
    {
        // process the proxy motion
        int iRowPxy;
        int iColPxy;
        float fDiscrepancyPxy;

        iRet = ProcessPxy (iThread, iPatch, iRowPxy, iColPxy, fDiscrepancyPxy);
        if (iRet)
        {
            return iRet;
        }
        

        if (fDiscrepancyPxy < PATCH_PXY_DISCREPANCY)
        {
            // refine the forward motion
            float fRowFull;
            float fColFull;
            iRet = ProcessFull (iThread, iPatch, iRowPxy, iColPxy, fRowFull, fColFull);
            if (iRet)
            {
                return iRet;
            }

            bool bAppend = false;
            if (iProcessFlag & PROCESS_FLAG_LOCKED_CAMERA)
            {
                float fDeltaRow = fRowFull - Patch.at(iPatch)->getRowFull(0);
                float fDeltaCol = fColFull - Patch.at(iPatch)->getColFull(0);
                if (fabs(fDeltaRow) < fLockedCameraThresh && fabs(fDeltaCol) < fLockedCameraThresh)
                {
                    bAppend = true;
                }
            }
            else
            {
                bAppend = true;
            }

            if (bAppend)
            {
                // add this to the track of the patch
                Patch.at(iPatch)->Append (iSprocTime, iRowPxy, iColPxy, fRowFull, fColFull);
            }
        }
    }

    return ASTAB_RET_SUCCESS;
}

int CAutoStabilize::ProcessPxy (int iThread, int iPatch, int &iRow, int &iCol, float &fDiscrepancy)
{
    int iRet;
    CStabPatch *patch = Patch.at(iPatch);

    // the location of the reference
    int iRowRef = patch->getRowPxy(0);
    int iColRef = patch->getColPxy(0);

    // the initial position for the proxy search
    int iRow0 = patch->getRowPxy(patch->getCount()-1);
    int iCol0 = patch->getColPxy(patch->getCount()-1);

    // perform the motion search
    iRet = pPxyMotionBuf->MotionSearch (iThread, patch->getRef(PATCH_VALUE_PXY), iRowRef, iColRef,
        Data.getImage(IMAGE_RESOLUTION_PXY), iRow0, iCol0, iRow, iCol, fDiscrepancy);
    if (iRet)
    {
        return iRet;
    }

    return ASTAB_RET_SUCCESS;
}

int CAutoStabilize::ProcessFull (int iThread, int iPatch, int iRowPxy, int iColPxy, 
                                 float &fRowFull, float &fColFull)
{
    int iRet;
    CStabPatch *patch = Patch.at(iPatch);

    // the location of the reference
    int iRowRef = RoundF(patch->getRowFull(0));
    int iColRef = RoundF(patch->getColFull(0));

    // the initial position for the full search
    int iRow0 = patch->getFull(iRowPxy);
    int iCol0 = patch->getFull(iColPxy);

    // perform the motion search
    iRet = pFullMotionBuf->MotionSearchSuper (iThread, patch->getRef(PATCH_VALUE_FULL), iRowRef, iColRef,
        Data.getImage(IMAGE_RESOLUTION_FULL), iRow0, iCol0, fRowFull, fColFull);
    if (iRet)
    {
        return iRet;
    }

    return ASTAB_RET_SUCCESS;
}

void ProcessPatchesSproc (void *vp, void *vpReserved)
{
    CAutoStabilize *asp = (CAutoStabilize *)vp;

    int iSprocThread = asp->iSprocThread;

    if (BThreadBegin (vpReserved))
    {
        // failure
        asp->iSprocRet = ASTAB_ERR_BTHREAD;
    }
    else
    {
        // success
        int iRet = asp->doProcessPatches (iSprocThread);
        if (iRet)
        {
            asp->iSprocRet = iRet;
        }
    }

    // finished
	 if (--asp->iSprocRun <= 0)
	 {
		  asp->SprocsDone.signal();
	 }
}
