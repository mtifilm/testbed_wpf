#include "stdafx.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "mti_math.h"

#include "AutoStabilizeReturns.h"
#include "StabTrack.h"

#ifndef max
#define max(a, b) ((a >= b) ? a : b)
#endif

CMotionBuffer::CMotionBuffer ()
{
    for (int iThread = 0; iThread < MAX_THREAD; iThread++)
    {
        fpCrossCorr[iThread] = 0;
        pCrossCorr[iThread] = 0;
        fpCrossCorrSuper[iThread] = 0;
        fpCrossCorrSum[iThread] = 0;
    }

}

CMotionBuffer::~CMotionBuffer ()
{
    for (int iThread = 0; iThread < MAX_THREAD; iThread++)
    {
        free (fpCrossCorr[iThread]);
        fpCrossCorr[iThread] = 0;
        ippFree (pCrossCorr[iThread]);
        pCrossCorr[iThread] = 0;
        free (fpCrossCorrSuper[iThread]);
        fpCrossCorrSuper[iThread] = 0;
        free (fpCrossCorrSum[iThread]);
        fpCrossCorrSum[iThread] = 0;
    }
}

int CMotionBuffer::Alloc (bool bForProxy, int iNThreadArg, IppiSize sizeSrcArg, IppiSize sizeDstArg, 
                          int iMaxRowOffArg, int iMaxColOffArg)
{
    iNThread = iNThreadArg;

    sizePatch = sizeSrcArg;
    sizeDst = sizeDstArg;

    iMaxRowOffFuture = iMaxRowOffArg;
    iMaxColOffFuture = iMaxColOffArg;

    algTypeCrossCorr = ippAlgAuto | ippiNormCoefficient | ippiROIValid;


    if (bForProxy)
    {
        // We estimate proxy motion from the reference frame to some point
        // in the future.  The maximum offset in the forward direction is 
        // TRACK_OFFSET_ROW_MAX and and TRACK_OFFSET_COL_MAX.  
        //
        // The we estimate the proxy motion from the future frame back to the
        // reference frame.  Use a maximum offset twice as large.
        // 
        // The reference data must be large enough for the large backwards
        // motion.
        //
        iMaxRowOffPast = 2*iMaxRowOffFuture;
        iMaxColOffPast = 2*iMaxColOffFuture;
    }
    else
    {
        // Do not perform reverse motion
        iMaxRowOffPast = 0;
        iMaxColOffPast = 0;
    }

    IppiSize sizeFuture;
    IppiSize sizePast;

    sizeFuture.height = sizePatch.height + 2*iMaxRowOffFuture;
    sizeFuture.width = sizePatch.width + 2*iMaxRowOffFuture;
    sizePast.height = sizePatch.height + 2*iMaxRowOffPast;
    sizePast.width = sizePatch.width + 2*iMaxColOffPast;

    sizeRef = sizePast;

    int bufSizeFutr;
    int bufSizePast;

    IppStatus status;

    status = ippiCrossCorrNormGetBufferSize (sizeFuture, sizePatch, algTypeCrossCorr, &bufSizeFutr);
    if (status)
    {
        return ASTAB_ERR_IPP_GET_SIZE;
    }

    // size of the cross correlation
    sizeCrossCorrFutr.height = sizeFuture.height - sizePatch.height + 1;
    sizeCrossCorrFutr.width = sizeFuture.width - sizePatch.width + 1;
    sizeCrossCorrPast.height = 0;
    sizeCrossCorrPast.width = 0;
    sizeCrossCorrSuper.height = 0;
    sizeCrossCorrSuper.width = 0;

    // the size of the buffer used for motion to the past
    
    if (iMaxRowOffPast == 0)
    {
        bufSizePast = 0;
    }
    else
    {
        status = ippiCrossCorrNormGetBufferSize (sizePast, sizePatch, algTypeCrossCorr, &bufSizePast);
        if (status)
        {
            return ASTAB_ERR_IPP_GET_SIZE;
        }

        sizeCrossCorrPast.height = sizePast.height - sizePatch.height + 1;
        sizeCrossCorrPast.width = sizePast.width - sizePatch.width + 1;
    }

    int bufSize = max(bufSizeFutr, bufSizePast);

    // largest valid size of the cross correlation
    IppiSize sizeCrossCorr;
    sizeCrossCorr.height = max(sizeCrossCorrFutr.height, sizeCrossCorrPast.height);
    sizeCrossCorr.width = max(sizeCrossCorrFutr.width, sizeCrossCorrPast.width);



    for (int iThread = 0; iThread < iNThread; iThread++)
    {
        fpCrossCorr[iThread] = (float *) malloc (sizeCrossCorr.height * sizeCrossCorr.width * sizeof(float));
        if (fpCrossCorr[iThread] == 0)
        {
            return ASTAB_ERR_ALLOC;
        }

        pCrossCorr[iThread] = (Ipp8u *) ippMalloc (bufSize);
        if (pCrossCorr[iThread] == 0)
        {
            return ASTAB_ERR_IPP_ALLOC;
        }

        if (bForProxy == false)
        {
            // in the second pass, the cross correlation surface is up-converted
            // to super resolution

            sizeCrossCorrSuper.height = SUPER_RESOLUTION * sizeCrossCorrFutr.height;
            sizeCrossCorrSuper.width = SUPER_RESOLUTION * sizeCrossCorrFutr.width;

            // the super-resolution motion needs memory for the up-conversion
            fpCrossCorrSuper[iThread] = (float *) malloc (sizeCrossCorrSuper.height * sizeCrossCorrSuper.width *
                                                          sizeof(float));
            if (fpCrossCorrSuper[iThread] == 0)
            {
                return ASTAB_ERR_ALLOC;
            }
            fpCrossCorrSum[iThread] = (float *) malloc (sizeCrossCorrSuper.height * sizeCrossCorrSuper.width *
                                                          sizeof(float));
            if (fpCrossCorrSum[iThread] == 0)
            {
                return ASTAB_ERR_ALLOC;
            }

            // allocate storage for the super resolution resize
            int iRet = Resize[iThread].Alloc (sizeCrossCorrFutr, sizeCrossCorrSuper);
            if (iRet)
            {
                return iRet;
            }
        }
    }

    return ASTAB_RET_SUCCESS;
}

int CMotionBuffer::MotionSearch (int iThread, float *fpRef, int iRowRef0, int iColRef0, float *fpDst, 
                                int iRowDst0, int iColDst0, int &iRowDst, int &iColDst, float &fDiscrepancy)
{
/*

    The MotionSearch is called to find the future-motion from the reference patch to the destination
    image at some point in the future.  This initial guess of the position in the destination 
    image is (RowDst0, ColDst0).   After motion estimation, the actual position in the destination
    image is (RowDst, ColDst).

    Then the past-motion is calculated from the future frame back to the reference data.

    The fDiscrepancy is the distance between these two motion vectors

*/
    int iRet;

    // search for the motion to a future frame
    iRet = SearchFuture (iThread, fpRef, fpDst, iRowDst0, iColDst0, iRowDst, iColDst);
    if (iRet)
    {
        return iRet;
    }

    if (iMaxRowOffPast > 0 || iMaxColOffPast > 0)
    {
        // for the past motion of the patch at the future frame back to the reference
        int iRowRef;
        int iColRef;
        iRet = SearchPast (iThread, fpRef, iRowRef0, iColRef0, fpDst, iRowDst, iColDst, iRowRef, iColRef);
        if (iRet)
        {
            return iRet;
        }

        int iRowDelta = iRowRef - iRowRef0;
        int iColDelta = iColRef - iColRef0;

        fDiscrepancy = sqrt ((float)(iRowDelta*iRowDelta) + (float)(iColDelta*iColDelta));
    }
    else
    {
        fDiscrepancy = 0.f;
    }

    return ASTAB_RET_SUCCESS;
}

int CMotionBuffer::MotionSearchSuper (int iThread, float *fpSrc, int iRowSrc, int iColSrc, float *fpDst, 
                                int iRowDst0, int iColDst0, float &fRowDst, float &fColDst)
{
/*

    The MotionSearchSuper is called to find the super resolution offset of a patch in one frame to the 
    next frame.  The initial guess of the location in the next frame is (RowDst0, ColDst0).
    After motion estimation, the actual position in the destination image is (RowDst, ColDst).

*/
    float *fpTmpl;
    float *fpSearch;
    int iRowTmpl;
    int iColTmpl;
    int iRowSearch;
    int iColSearch;

    int iPatchRadRow = (sizePatch.height-1)/2;
    int iPatchRadCol = (sizePatch.width-1)/2;

    // ULC of the template patch.  The center of the patch is at (RowSrc0, ColSrc0)
    iRowTmpl = iRowSrc - iPatchRadRow;
    iColTmpl = iColSrc - iPatchRadCol;
    fpTmpl = fpSrc;

    // the ULC of the search patch.  The center of the patch is at (iRowDst0, iColDst0)
    iRowSearch = iRowDst0 - iPatchRadRow;
    iColSearch = iColDst0 - iPatchRadCol;

    // the search region
    int iRowSearch0 = iRowSearch - iMaxRowOffFuture;
    int iRowSearch1 = iRowSearch + sizePatch.height + iMaxRowOffFuture;
    int iColSearch0 = iColSearch - iMaxColOffFuture;
    int iColSearch1 = iColSearch + sizePatch.width + iMaxColOffFuture;
    LegalizeSearch (iRowSearch0, iRowSearch1, sizeDst.height);
    LegalizeSearch (iColSearch0, iColSearch1, sizeDst.width);

    fpSearch = fpDst + iRowSearch0 * sizeDst.width + iColSearch0;
    IppiSize sizeSearch;
    sizeSearch.height = iRowSearch1 - iRowSearch0;
    sizeSearch.width = iColSearch1 - iColSearch0;

    IppStatus status;
    status = ippiCrossCorrNorm_32f_C1R (fpSearch, 4*sizeDst.width, sizeSearch, fpTmpl, 4*sizePatch.width, sizePatch, fpCrossCorr[iThread], 
        4*sizeCrossCorrFutr.width, algTypeCrossCorr, pCrossCorr[iThread]);
    if (status)
    {
        return ASTAB_ERR_IPP_CROSS_CORR;
    }

    // up-convert the fpCrossCorr to fpCrossCorrSuper
    int iRet = Resize[iThread].Apply (fpCrossCorr[iThread], fpCrossCorrSuper[iThread]);
    if (iRet)
    {
        return  iRet;
    }

    float fCrossCorrBest;
    int iRowBest;
    int iColBest;

    status = ippiMaxIndx_32f_C1R (fpCrossCorrSuper[iThread], 4*sizeCrossCorrSuper.width, sizeCrossCorrSuper, &fCrossCorrBest, &iColBest, &iRowBest);
    if (status)
    {
        return ASTAB_ERR_IPP_MAX_INDX;
    }

    // the zero offset occurs at the center
    int iRowOff = iRowBest - (sizeCrossCorrSuper.height-1)/2;
    int iColOff = iColBest - (sizeCrossCorrSuper.width-1)/2;

    // remove the zoom
    float fRowOff = (float)iRowOff / (float)SUPER_RESOLUTION;
    float fColOff = (float)iColOff / (float)SUPER_RESOLUTION;

    // calculate the center of the patch at the future frame
    fRowDst= (float)iRowDst0 + fRowOff;
    fColDst = (float)iColDst0 + fColOff;

#ifdef TTT
{
    FILE *fpS;
    FILE *fpD;
    FILE *fpNC1;
    FILE *fpNC2;

    fopen_s (&fpS, "c:/tmp/AS/Src.dat", "w");
    fopen_s (&fpD, "c:/tmp/AS/Dst.dat", "w");
    fopen_s (&fpNC1, "c:/tmp/AS/NC1.dat", "w");
    fopen_s (&fpNC2, "c:/tmp/AS/NC2.dat", "w");

    for (int i = 0; i < sizePatch.height; i++)
    {
        for (int j = 0; j < sizePatch.width; j++)
        {
            fprintf (fpS, "%f ", fpTmpl[i*sizeDst.width+j]);
        }
        fprintf (fpS, "\n");
    }

    for (int i = 0; i < sizeSearch.height; i++)
    {
        for (int j = 0; j < sizeSearch.width; j++)
        {
            fprintf (fpD, "%f ", fpSearch[i*sizeDst.width+j]);
        }
        fprintf (fpD, "\n");
    }

    for (int i = 0; i < sizeCrossCorrFutr.height; i++)
    {
        for (int j = 0; j < sizeCrossCorrFutr.width; j++)
        {
            fprintf (fpNC1, "%f ", fpCrossCorr[iThread][i*sizeCrossCorrFutr.width+j]);
        }
        fprintf (fpNC1, "\n");
    }

    for (int i = 0; i < sizeCrossCorrSuper.height; i++)
    {
        for (int j = 0; j < sizeCrossCorrSuper.width; j++)
        {
            fprintf (fpNC2, "%f ", fpCrossCorrSuper[iThread][i*sizeCrossCorrSuper.width+j]);
        }
        fprintf (fpNC2, "\n");
    }

    fclose (fpS);
    fclose (fpD);
    fclose (fpNC1);
    fclose (fpNC2);

}
#endif

    return ASTAB_RET_SUCCESS;
}

IppiSize CMotionBuffer::getSizeRef ()
{
    return sizeRef;
}

IppiSize CMotionBuffer::getSizePatch ()
{
    return sizePatch;
}

int CMotionBuffer::getMaxRowOff ()
{
    return iMaxRowOffFuture;
}

int CMotionBuffer::getMaxColOff ()
{
    return iMaxColOffFuture;
}

void CMotionBuffer::LegalizeSearch (int &i0, int &i1, int iN)
{
    int iDelta = 0;
    if (i0 < 0)
    {
        iDelta = 0 - i0;
    }
    else if (i1 > iN)
    {
        iDelta = iN - i1;
    }

    i0 += iDelta;
    i1 += iDelta;
}

int CMotionBuffer::SearchFuture (int iThread, float *fpRef, float *fpFutr, int iRowFutr0, int iColFutr0, 
                             int &iRowFutr, int &iColFutr)
{
    float *fpTmpl;
    float *fpSearch;
    int iRowTmpl;
    int iColTmpl;
    int iRowSearch;
    int iColSearch;

    int iPatchRadRow = (sizePatch.height-1)/2;
    int iPatchRadCol = (sizePatch.width-1)/2;

    // ULC of the template patch.  The center of the patch is at the center of the reference
    iRowTmpl = ((sizeRef.height - 1)/2) - iPatchRadRow;
    iColTmpl = ((sizeRef.width - 1)/2) - iPatchRadCol;
    fpTmpl = fpRef + iRowTmpl * sizeRef.width + iColTmpl;

    // the ULC of the search patch.  The center of the patch is at (iRowFutr0, iColFutr0)
    iRowSearch = iRowFutr0 - iPatchRadRow;
    iColSearch = iColFutr0 - iPatchRadCol;

    // the search region
    int iRowSearch0 = iRowSearch - iMaxRowOffFuture;
    int iRowSearch1 = iRowSearch + sizePatch.height + iMaxRowOffFuture;
    int iColSearch0 = iColSearch - iMaxColOffFuture;
    int iColSearch1 = iColSearch + sizePatch.width + iMaxColOffFuture;
    LegalizeSearch (iRowSearch0, iRowSearch1, sizeDst.height);
    LegalizeSearch (iColSearch0, iColSearch1, sizeDst.width);

    fpSearch = fpFutr + iRowSearch0 * sizeDst.width + iColSearch0;
    IppiSize sizeSearch;
    sizeSearch.height = iRowSearch1 - iRowSearch0;
    sizeSearch.width = iColSearch1 - iColSearch0;

    IppStatus status;
    status = ippiCrossCorrNorm_32f_C1R (fpSearch, 4*sizeDst.width, sizeSearch, fpTmpl, 4*sizeRef.width, sizePatch, fpCrossCorr[iThread], 
        4*sizeCrossCorrFutr.width, algTypeCrossCorr, pCrossCorr[iThread]);
    if (status)
    {
        return ASTAB_ERR_IPP_CROSS_CORR;
    }

    float fCrossCorrBest;
    int iRowBest;
    int iColBest;

    status = ippiMaxIndx_32f_C1R (fpCrossCorr[iThread], 4*sizeCrossCorrFutr.width, sizeCrossCorrFutr, &fCrossCorrBest, &iColBest, &iRowBest);
    if (status)
    {
        return ASTAB_ERR_IPP_MAX_INDX;
    }

    // the RowBest and ColBest are pixel locations in the search region.
    //
    // when (RowBest,ColBest) = (0,0), this corresponds to (RowSearch0,ColSearch0)
    //

    int iRowOffBest = (iRowBest+iRowSearch0) - iRowSearch;
    int iColOffBest = (iColBest+iColSearch0) - iColSearch;

    // calculate the center of the patch at the future frame
    iRowFutr = iRowFutr0 + iRowOffBest;
    iColFutr = iColFutr0 + iColOffBest;

    return ASTAB_RET_SUCCESS;
}

int CMotionBuffer::SearchPast (int iThread, float *fpRef, int iRowRef0, int iColRef0, float *fpFutr, 
                               int iRowFutr, int iColFutr, int &iRowRef, int &iColRef)
{
    float *fpTmpl;
    float *fpSearch;
    int iRowTmpl;
    int iColTmpl;
    int iRowSearch;
    int iColSearch;

    int iPatchRadRow = (sizePatch.height-1)/2;
    int iPatchRadCol = (sizePatch.width-1)/2;
    
    // the ULC of the template patch.  The center of the patch is at (iRowFutr, iColFutr)
    iRowTmpl = iRowFutr - iPatchRadRow;
    iColTmpl = iColFutr - iPatchRadCol;
    fpTmpl= fpFutr + iRowTmpl * sizeDst.width + iColTmpl;

    // ULC of the search patch.  The center of the patch is at the center of the reference
    iRowSearch = ((sizeRef.height - 1)/2) - iPatchRadRow;
    iColSearch = ((sizeRef.width - 1)/2) - iPatchRadCol;

    // the search region
    int iRowSearch0 = iRowSearch - iMaxRowOffPast;
    int iRowSearch1 = iRowSearch + sizePatch.height + iMaxRowOffPast;
    int iColSearch0 = iColSearch - iMaxColOffPast;
    int iColSearch1 = iColSearch + sizePatch.width + iMaxColOffPast;
    LegalizeSearch (iRowSearch0, iRowSearch1, sizeRef.height);
    LegalizeSearch (iColSearch0, iColSearch1, sizeRef.width);

    fpSearch = fpRef + iRowSearch0 * sizeRef.width + iColSearch0;
    IppiSize sizeSearch;
    sizeSearch.height = iRowSearch1 - iRowSearch0;
    sizeSearch.width = iColSearch1 - iColSearch0;

    IppStatus status;
    status = ippiCrossCorrNorm_32f_C1R (fpSearch, 4*sizeRef.width, sizeSearch, fpTmpl, 4*sizeDst.width, sizePatch, fpCrossCorr[iThread], 
        4*sizeCrossCorrPast.width, algTypeCrossCorr, pCrossCorr[iThread]);
    if (status)
    {
        return ASTAB_ERR_IPP_CROSS_CORR;
    }

    float fCrossCorrBest;
    int iRowBest;
    int iColBest;

    status = ippiMaxIndx_32f_C1R (fpCrossCorr[iThread], 4*sizeCrossCorrPast.width, sizeCrossCorrPast, &fCrossCorrBest, &iColBest, &iRowBest);
    if (status)
    {
        return ASTAB_ERR_IPP_MAX_INDX;
    }

    // the RowBest and ColBest are pixel locations in the search region.
    //
    // when (RowBest,ColBest) = (0,0), this corresponds to (RowSearch0,ColSearch0)
    //

    int iRowOffBest = (iRowBest+iRowSearch0) - iRowSearch;
    int iColOffBest = (iColBest+iColSearch0) - iColSearch;

    // calculate the center of the patch at the reference frame
    iRowRef = iRowRef0 + iRowOffBest;
    iColRef = iColRef0 + iColOffBest;

    return ASTAB_RET_SUCCESS;
}

CStabTrack::CStabTrack ()
{
}

CStabTrack::~CStabTrack ()
{
    Pixels.clear();
}

int CStabTrack::Assign (vector <CTrackingPixel> &Track)
{

    Pixels = Track;

    return ASTAB_RET_SUCCESS;
}

int CStabTrack::getNPix ()
{
    return (int)Pixels.size();
}

int CStabTrack::getRowPxy (int i)
{
    if (i < 0 || i >= (int)Pixels.size())
    {
        return 0;
    }

    return Pixels.at(i).iRowPxy;
}

int CStabTrack::getColPxy (int i)
{
    if (i < 0 || i >= (int)Pixels.size())
    {
        return 0;
    }

    return Pixels.at(i).iColPxy;
}

float CStabTrack::getRowFull (int i)
{
    if (i < 0 || i >= (int)Pixels.size())
    {
        return 0;
    }

    return Pixels.at(i).fRowFull;
}

float CStabTrack::getColFull (int i)
{
    if (i < 0 || i >= (int)Pixels.size())
    {
        return 0;
    }

    return Pixels.at(i).fColFull;
}

float CStabTrack::getRowHat (int i)
{
    if (i < 0 || i >= (int)Pixels.size())
    {
        return 0.f;
    }

    return Pixels.at(i).fRowHat;
}

float CStabTrack::getColHat (int i)
{
    if (i < 0 || i >= (int)Pixels.size())
    {
        return 0.f;
    }

    return Pixels.at(i).fColHat;
}

float CStabTrack::getRowOff (int i)
{
    if (i < 0 || i >= (int)Pixels.size())
    {
        return 0.f;
    }

    return (Pixels.at(i).fRowHat - Pixels.at(i).fRowFull);
}

float CStabTrack::getColOff (int i)
{
    if (i < 0 || i >= (int)Pixels.size())
    {
        return 0.f;
    }

    return (Pixels.at(i).fColHat - Pixels.at(i).fColFull);
}

int CStabTrack::getTime (int i)
{
    if (i < 0 || i >= (int)Pixels.size())
    {
        return 0;
    }

    return Pixels.at(i).iTime;
}

int CStabTrack::Fit (float fLockedCameraThresh)
{
    int iRet;

    // fit the row values
    iRet = CalcHat (true, fLockedCameraThresh);
    if (iRet)
    {
        return iRet;
    }

    // fit the column values
    iRet = CalcHat (false, fLockedCameraThresh);
    if (iRet)
    {
        return iRet;
    }

    return ASTAB_RET_SUCCESS;
}

int CStabTrack::CalcHat (bool bRow, float fLockedCameraThresh)
{
    int iRet;
    int iN = (int)Pixels.size();
    vector <float> listY;
    vector <float> listYhat;

    // initialize the Y values
    if (bRow)
    {
        for (int iPix = 0; iPix < iN; iPix++)
        {
            listY.push_back(Pixels.at(iPix).fRowFull);
        }
    }
    else
    {
        for (int iPix = 0; iPix < iN; iPix++)
        {
            listY.push_back(Pixels.at(iPix).fColFull);
        }
    }

    if (fabs(listY.at(iN-1) - listY.at(0)) < fLockedCameraThresh)
    {
        // this trajectory is a locked camera.  The esimated value of Y is the mean
        float fSum = 0.f;
        for (int iPix = 0; iPix < iN; iPix++)
        {
            fSum += listY.at(iPix);
        }

        for (int iPix = 0; iPix < iN; iPix++)
        {
            listYhat.push_back (fSum / (float)iN);
        }
    }
    else if (iN < 2*CALC_HAT_RADIUS+1)
    {
        // this trajectory is too short, do not smooth it
        for (int iPix = 0; iPix < iN; iPix++)
        {
            listYhat.push_back (listY.at(iPix));
        }
    }
    else
    {
        // use a piece-wise spline to compute the smoothed trajectory
        double daY[2*CALC_HAT_RADIUS+1];
        double dYhat;
        double daX[(2*CALC_HAT_RADIUS+1)*(CALC_HAT_DEGREE+1)];
        double daBeta[CALC_HAT_DEGREE+1];

        // init daX  (the X values have a mean of 0. which helps numerical stability)
        double *dp = daX;
        for (int iX = -CALC_HAT_RADIUS; iX <= CALC_HAT_RADIUS; iX++)
        {
            dp[0] = 1.;
            dp++;
            for (int iDeg = 1; iDeg <= CALC_HAT_DEGREE; iDeg++)
            {
                dp[0] = dp[-1] * (double)iX;    // the column entries look like this:    1 X X*X
                dp++;
            }
        }

        for (int iPix = 0; iPix < iN; iPix++)
        {
            int iPix0 = iPix - CALC_HAT_RADIUS;
            int iPix1 = iPix + CALC_HAT_RADIUS;
            int iDelta = 0;
            if (iPix0 < 0)
            {
                iDelta = 0-iPix0;
            }
            else if (iPix1 > iN-1)
            {
                iDelta = iN-1 - iPix1;
            }
            iPix0 += iDelta;
            iPix1 += iDelta;

            dp = daY;
            for (int iP = iPix0; iP <= iPix1; iP++)
            {
                dp[0] = listY.at(iP);
                dp++;
            }

            // use OLS to estimate Beta
            iRet = OLS_Estimate_Beta (daX, daY, 2*CALC_HAT_RADIUS+1, CALC_HAT_DEGREE+1, daBeta);
            if (iRet)
            {
                return iRet;
            }

            // apply Beta to the current pixel
            iRet = OLS_Apply_Beta (daX + ((iPix-iPix0) * (CALC_HAT_DEGREE+1)), &dYhat, 1, CALC_HAT_DEGREE+1, daBeta);
            if (iRet)
            {
                return iRet;
            }

            listYhat.push_back ((float)dYhat);
        }
    }

    // keep track of the estimated values
    if (bRow)
    {
        for (int iPix = 0; iPix < iN; iPix++)
        {
            Pixels.at(iPix).fRowHat = listYhat.at(iPix);
        }
    }
    else
    {
        for (int iPix = 0; iPix < iN; iPix++)
        {
            Pixels.at(iPix).fColHat = listYhat.at(iPix);
        }
    }

    return ASTAB_RET_SUCCESS;
}
