#ifndef MTI_MATH_H
#define MTI_MATH_H

int InvertMatrix (float *fpSrc, float *fpDst, int iDim);
int InvertMatrix (double *dpSrc, double *dpDst, int iDim);
float ran266(long *lseed);

int OLS_Estimate_Beta (float *fpX, float *fpY, int iNRow, int iNCol, float *fpBeta);
int OLS_Estimate_Beta (double *dpX, double *dpY, int iNRow, int iNCol, double *dpBeta);
int OLS_Apply_Beta (float *fpX, float *fpY, int iNRow, int iNCol, float *fpBeta);
int OLS_Apply_Beta (double *dpX, double *dpY, int iNRow, int iNCol, double *dpBeta);

int mti_round (float f);
int mti_round (double d);

void LineIntersect (float fA1, float fB1, float fC1, float fA2, float fB2, float fC2, float &fX, float &fY);

#endif
