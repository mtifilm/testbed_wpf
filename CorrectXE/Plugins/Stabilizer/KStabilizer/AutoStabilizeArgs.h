#ifndef AUTO_STABILIZE_ARGS_H
#define AUTO_STABILIZE_ARGS_H

typedef struct 
{
    int iNRow;     // number of rows in the image
    int iNCol;     // number of columns in the image
    int iNStride;  // number of columns to move from one row to the next
    int iNFrame;   // number of frames in the sequence
    int iNThread;  // number of threads used by the library
    int iAnimation; // 0 for live action.  1 for animation.
} EngineArgs;

#endif
