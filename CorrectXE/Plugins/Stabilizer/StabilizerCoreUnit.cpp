//---------------------------------------------------------------------------


#pragma hdrstop

#include "StabilizerCoreUnit.h"
#include <math.h>
//---------------------------------------------------------------------------

#pragma package(smart_init)

//--------------------FindStabilizerMotionLinCCode------------------------May 06---

  void FindStabilizerMotionLinCCode(float Wx[], float Wy[], float tpX[], float tpY[], int N0, float *xVec, float *yVec, int nCol, int nRow, RECT &Box)

//  N0 is the number of tracking points, Wx and Wy are 3 larger
//
//******************************************************************************
{
   float *tpIR2 = new float[N0];
   for (int iR=Box.top; iR <= Box.bottom; iR++)
	 {
	  float *xVRow = xVec + iR*nCol + Box.left;
	  float *yVRow = yVec + iR*nCol + Box.left;
	  for (int i=0; i < N0; i++)
		{
		   tpIR2[i] = (iR-tpY[i]);
		   tpIR2[i] *= tpIR2[i];
		}

	  float xc = Wx[N0] + Wx[N0+2]*iR;
	  float yc = Wy[N0] + Wy[N0+2]*iR;

	  for (int iC=Box.left; iC <= Box.right; iC++)
	   {
		  float x = xc + Wx[N0+1]*iC;
		  float y = yc + Wy[N0+1]*iC;
		  for (int i=0; i < N0; i++)
			{
			  float r0 = (iC - tpX[i]);
			  float U = sqrt(r0*r0 + tpIR2[i]);
			  x += Wx[i]*U;
			  y += Wy[i]*U;
			}
		  *xVRow++ = x;
		  *yVRow++ = y;
	   }
	  }

   delete[] tpIR2;
}

//--------------------FindStabilizerMotionRLogCCode-----------------------May 06---

  void FindStabilizerMotionRLogCCode(float Wx[], float Wy[], float tpX[], float tpY[], int N0, float *xVec, float *yVec, int nCol, int nRow, RECT &Box)

//  N0 is the number of tracking points, Wx and Wy are 3 larger
//
//******************************************************************************
{
   float *tpIR2 = new float[N0];
   for (int iR=Box.top; iR <= Box.bottom; iR++)
	 {
	  float *xVRow = xVec + iR*nCol + Box.left;
	  float *yVRow = yVec + iR*nCol + Box.left;
	  for (int i=0; i < N0; i++)
		{
		   tpIR2[i] = (iR-tpY[i]);
		   tpIR2[i] *= tpIR2[i];
		}

	  float xc = Wx[N0] + Wx[N0+2]*iR;
	  float yc = Wy[N0] + Wy[N0+2]*iR;

	  for (int iC=Box.left; iC <= Box.right; iC++)
	   {
		  float x = xc + Wx[N0+1]*iC;
		  float y = yc + Wy[N0+1]*iC;
		  for (int i=0; i < N0; i++)
                    {
                      float r0 = (iC - tpX[i]);
                      float U = sqrt(r0*r0 + tpIR2[i]);
                      if (U > 10E-30)
                        U = U*log(U);
                      else if (U > 0)
                        U = 10E-30*log(10E-30);
                      x += Wx[i]*U;
                      y += Wy[i]*U;
                    }
		  *xVRow++ = x;
		  *yVRow++ = y;
	   }
	  }

   delete[] tpIR2;
}

//--------------------FindStabilizerMotionRRLogCCode----------------------May 06---

  void FindStabilizerMotionRRLogCCode(float Wx[], float Wy[], float tpX[], float tpY[], int N0, float *xVec, float *yVec, int nCol, int nRow, RECT &Box)

//  N0 is the number of tracking points, Wx and Wy are 3 larger
//
//******************************************************************************
{
   float *tpIR2 = new float[N0];
   for (int iR=Box.top; iR <= Box.bottom; iR++)
	 {
	  float *xVRow = xVec + iR*nCol + Box.left;
	  float *yVRow = yVec + iR*nCol + Box.left;
	  for (int i=0; i < N0; i++)
		{
		   tpIR2[i] = (iR-tpY[i]);
		   tpIR2[i] *= tpIR2[i];
		}

	  float xc = Wx[N0] + Wx[N0+2]*iR;
	  float yc = Wy[N0] + Wy[N0+2]*iR;

	  for (int iC=Box.left; iC <= Box.right; iC++)
	   {
		  float x = xc + Wx[N0+1]*iC;
		  float y = yc + Wy[N0+1]*iC;
		  for (int i=0; i < N0; i++)
			{
			  float r0 = (iC - tpX[i]);
			  float U = sqrt(r0*r0 + tpIR2[i]);
			  if (U > 10E-30)
				U = U*U*log(U);
			  else if (U > 0)
				U = 10E-60*log(10E-30);

			  x += Wx[i]*U;
			  y += Wy[i]*U;
			}
		  *xVRow++ = x;
		  *yVRow++ = y;
	   }
	  }

   delete[] tpIR2;
}

#ifndef _WIN64
//---------------------FindStabilizerMotionRLogSSE--------------------------May 06---

  void FindStabilizerMotionRLogSSE(float Wx[], float Wy[], float tpX[], float tpY[], int N0, float *xVec, float *yVec, int nCol, int nRow, RECT &Box)

//  N0 is the number of tracking points, Wx and Wy are 3 larger
//
//******************************************************************************
{
   // First make sure we are divisible by 4
   int n = ((N0+3) >> 2) << 2;

   unsigned char *DataBufferOrig = new unsigned char[6*sizeof(float)*n + 16];
   float *DataBuffer = (float *)((((int)DataBufferOrig + 15) >> 4) << 4);
   float *WxA = DataBuffer;
   float *WyA = WxA + n;
   float *tpXA = WyA + n;
   float *tpYA = tpXA + n;
   float *tpIR2 = tpYA + n;
   float *r = tpYA + n;

   // Copy over to aligned boundaries
   for (int i=0; i < N0; i++)
     {
          WxA[i] = Wx[i];
          WyA[i] = Wy[i];
          tpXA[i] = tpX[i];
          tpYA[i] = tpY[i];
     }

   // File the rest with zero
   for (int i=N0; i < n; i++)
     {
          WxA[i] = 0;
          WyA[i] = 0;
          tpXA[i] = 0;
          tpYA[i] = 0;
     }


   // Sweep for each row
//   for (int iR=Box.top; iR <= 2; iR++)
   for (int iR=Box.top; iR <= Box.bottom; iR++)
     {
       float *xVRow = xVec + iR*nCol + Box.left;
       float *yVRow = yVec + iR*nCol + Box.left;
       float xc = Wx[N0] + Wx[N0+2]*iR;
       float yc = Wy[N0] + Wy[N0+2]*iR;


      // Compute the (iR-tpY[i])^2 vector since this doesn't change
      asm
        {
          cvtsi2ss xmm1, iR			// xmm1 = (iR, 0, 0, 0)
          shufps  xmm1, xmm1, 0		// xmm1 = (iR, iR, iR, iR)

          mov     esi, tpYA
          mov	  edi, tpIR2
          mov	  ecx, n              // How many loop
          shr	  ecx, 2              // Divided by 4
  $1:     movdqa  xmm0, [esi]         //  xmm0 = tpY[i]
          add     esi, 16
          subps   xmm0, xmm1          //  xmm0 = (tpy[i] - iR)
          mulps   xmm0, xmm0          //  xmm0 = (tpy[i] - iR)^2
          movdqa  [edi], xmm0         //  tpIR2[i] = xmm0
          add     edi, 16
          dec	  ecx
          jne	  $1
        }

    float xR, yR;

    // Now process a row

    for (int iC=Box.left; iC <= Box.right; iC++)
     {

        // First compute the r's
        asm
           {
                cvtsi2ss xmm2, iC           // xmm2 = (iC, 0, 0, 0)
                shufps  xmm2, xmm2, 0	    // xmm2 = (iC, iC, iC, iC)

                mov     ecx, n              // How many loop
                shr     ecx, 2              // Divided by 4

                mov     eax, tpXA
                mov     edx, tpIR2
                mov     edi, r

$$1:            movdqa  xmm3, [eax]         //  xmm3 = tpX[i]
                add     eax, 16

                subps   xmm3, xmm2          //  xmm3 = (tpX[i] - iC)
                mulps   xmm3, xmm3          //  xmm3 =  (tpX[i] - iC)^2
                addps   xmm3, [edx]         //  xmm3 =  (tpX[i] - iC)^2 + tpIR2[i])
                add     edx, 16

                sqrtps  xmm3, xmm3          //  xmm3 = sqrt(tpX[i] - iC)^2 + tpIR2[i])
                movdqa  xmm4, xmm3          //  xmm4 = xmm3
                movdqa  [edi], xmm4         //  r[i] = xmm4
                add     edi, 16

                dec     ecx
                jne     $$1
            }

       // Now loop, creating the logs
       for (int i=0; i < N0; i++)
         {
             if (r[i] > 10E-30)
               r[i] = r[i]*log(r[i]);
             else if (r[i] > 0)
               r[i] = 10E-30*log(10E-30);
         }

       // Finish up
       asm
         {
                xorps   xmm0, xmm0          // xmm0 = 0
                xorps   xmm1, xmm1          // xmm1 = 0

                mov     ecx, n              // How many loop
                shr     ecx, 2              // Divided by 4

                mov     eax, r
                mov     esi, WxA
                mov     edi, WyA

$$2:            movdqa  xmm3, [eax]         //  xmm3 = r[i]*ln(r[i])
                add     eax, 16

                movdqa  xmm4, xmm3          //  xmm4 = xmm3
                mulps   xmm3, [esi]         //  xmm3 = Wx[i]*r[i]*ln(r[i])
                add     esi, 16

                mulps   xmm4, [edi]         //  xmm4 = Wy[i]*r[i]*ln(r[i])
                add     edi, 16

                addps   xmm0, xmm3         //  Sum them up
                addps   xmm1, xmm4

                dec     ecx
                jne     $$2

                // Sum up the results
                movdqa  xmm2,xmm0          // xmm0 = xmm2 = (x3, x2, x1, x0)
                movdqa  xmm3,xmm1

                shufps  xmm2,xmm0, 0x1B    // xmm2 = (x0, x1, x2, x3)
                shufps  xmm3,xmm1, 0x1B

                addps   xmm2,xmm0          // xmm2 = (x3+x0, x2+x1, x1+x2, x0+x3)
                addps   xmm3,xmm1

                movhlps xmm0,xmm2          // xmm0 = ( , , x3+x0, x2 + x1)
                movhlps xmm1,xmm3

                addps   xmm0,xmm2          // xmm0 = ( , , , x0 + x3 + x1 + x2)
                addps   xmm1,xmm3

                movss   [xR],xmm0          // Store for later use
                movss   [yR],xmm1
              }

        // Sum up the linear and the warp terms
        float xt = xc + Wx[N0+1]*iC + xR;
        float yt = yc + Wy[N0+1]*iC + yR;

        *xVRow++ = xt;
        *yVRow++ = yt;
     }
   }
   delete[] DataBufferOrig;
}

//---------------------FindStabilizerMotionLinSSE--------------------------May 06---

  void FindStabilizerMotionLinSSE(float Wx[], float Wy[], float tpX[], float tpY[], int N0, float *xVec, float *yVec, int nCol, int nRow, RECT &Box)

//  N0 is the number of tracking points, Wx and Wy are 3 larger
//
//******************************************************************************
{
   // First make sure we are divisible by 4
   int n = ((N0+3) >> 2) << 2;

   unsigned char *DataBufferOrig = new unsigned char[5*sizeof(float)*n + 16];
   float *DataBuffer = (float *)((((int)DataBufferOrig + 15) >> 4) << 4);
   float *WxA = DataBuffer;
   float *WyA = WxA + n;
   float *tpXA = WyA + n;
   float *tpYA = tpXA + n;
   float *tpIR2 = tpYA + n;

   // Copy over to aligned boundaries
   for (int i=0; i < N0; i++)
	 {
		WxA[i] = Wx[i];
		WyA[i] = Wy[i];
		tpXA[i] = tpX[i];
		tpYA[i] = tpY[i];
	 }

   // File the rest with zero
   for (int i=N0; i < n; i++)
	 {
		WxA[i] = 0;
		WyA[i] = 0;
		tpXA[i] = 0;
		tpYA[i] = 0;
	 }


   // Sweep for each row
//   for (int iR=Box.top; iR <= 2; iR++)
   for (int iR=Box.top; iR <= Box.bottom; iR++)
	 {
	  float *xVRow = xVec + iR*nCol + Box.left;
	  float *yVRow = yVec + iR*nCol + Box.left;
	  float xc = Wx[N0] + Wx[N0+2]*iR;
	  float yc = Wy[N0] + Wy[N0+2]*iR;


	  // Compute the (iR-tpY[i])^2 vector since this doesn't change
	  asm
            {
              cvtsi2ss xmm1, iR			// xmm1 = (iR, 0, 0, 0)
              shufps  xmm1, xmm1, 0		// xmm1 = (iR, iR, iR, iR)

              mov     esi, tpYA
              mov     edi, tpIR2
              mov     ecx, n              // How many loop
              shr     ecx, 2              // Divided by 4
$1:           movdqa  xmm0, [esi]         //  xmm0 = tpY[i]
              add     esi, 16
              subps   xmm0, xmm1          //  xmm0 = (tpy[i] - iR)
              mulps   xmm0, xmm0          //  xmm0 = (tpy[i] - iR)^2
              movdqa  [edi], xmm0         //  tpIR2[i] = xmm0
              add     edi, 16
              dec     ecx
              jne     $1
            }

	  float xR, yR;

	  // Now process the row

	  for (int iC=Box.left; iC <= Box.right; iC++)
	   {

              asm
                    {
                      xorps   xmm0, xmm0          // xmm0 = 0
                      xorps   xmm1, xmm1          // xmm1 = 0

                      cvtsi2ss xmm2, iC		  // xmm2 = (iC, 0, 0, 0)
                      shufps  xmm2, xmm2, 0	  // xmm2 = (iC, iC, iC, iC)

                      mov     ecx, n              // How many loop
                      shr     ecx, 2              // Divided by 4

                      mov     eax, tpXA
                      mov     edx, tpIR2
                      mov     esi, WxA
                      mov     edi, WyA

$$1:                  movdqa  xmm3, [eax]         //  xmm3 = tpX[i]
                      add     eax, 16

                      subps   xmm3, xmm2          //  xmm3 = (tpX[i] - iC)
                      mulps   xmm3, xmm3          //  xmm3 =  (tpX[i] - iC)^2
                      addps   xmm3, [edx]         //  xmm3 =  (tpX[i] - iC)^2 + tpIR2[i])
                      add     edx, 16

                      sqrtps  xmm3, xmm3          //  xmm3 = sqrt(tpX[i] - iC)^2 + tpIR2[i])
                      movdqa  xmm4, xmm3          //  xmm4 = xmm3
                      mulps   xmm3, [esi]         //  xmm3 = Wx[i]*sqrt(...)
                      add     esi, 16

                      mulps   xmm4, [edi]         //  xmm4 = Wy[i]*sqrt(...)
                      add     edi, 16

                      addps   xmm0, xmm3         //  Sum them up
                      addps   xmm1, xmm4

                      dec     ecx
                      jne     $$1

                      // Sum up the results
                      movdqa  xmm2,xmm0          // xmm0 = xmm2 = (x3, x2, x1, x0)
                      movdqa  xmm3,xmm1

                      shufps  xmm2,xmm0, 0x1B    // xmm2 = (x0, x1, x2, x3)
                      shufps  xmm3,xmm1, 0x1B

                      addps   xmm2,xmm0          // xmm2 = (x3+x0, x2+x1, x1+x2, x0+x3)
                      addps   xmm3,xmm1

                      movhlps xmm0,xmm2          // xmm0 = ( , , x3+x0, x2 + x1)
                      movhlps xmm1,xmm3

                      addps   xmm0,xmm2          // xmm0 = ( , , , x0 + x3 + x1 + x2)
                      addps   xmm1,xmm3

                      movss   [xR],xmm0          // Store for later use
                      movss   [yR],xmm1
                    }

              // Sum up the linear and the warp terms
              float xt = xc + Wx[N0+1]*iC + xR;
              float yt = yc + Wy[N0+1]*iC + yR;

              *xVRow++ = xt;
              *yVRow++ = yt;
	   }
   }
   delete[] DataBufferOrig;
}


//---------------------FindStabilizerMotionLinSSE4-------------------------May 06---

  void FindStabilizerMotionLinSSE4(float Wx[], float Wy[], float tpX[], float tpY[], int N0, float *xVec, float *yVec, int nCol, int nRow, RECT &Box)

//  N0 is the number of tracking points, Wx and Wy are 3 larger solution
//  to the equation
//    This does a 1/4 interpolation
//
//******************************************************************************
{
   // First make sure we are divisible by 4
   int n = ((N0+3) >> 2) << 2;

   unsigned char *DataBufferOrig = new unsigned char[5*sizeof(float)*n + 16];
   float *DataBuffer = (float *)((((int)DataBufferOrig + 15) >> 4) << 4);
   float *WxA = DataBuffer;
   float *WyA = WxA + n;
   float *tpXA = WyA + n;
   float *tpYA = tpXA + n;
   float *tpIR2 = tpYA + n;

   // Copy over to aligned boundaries
   for (int i=0; i < N0; i++)
	 {
		WxA[i] = Wx[i];
		WyA[i] = Wy[i];
		tpXA[i] = tpX[i];
		tpYA[i] = tpY[i];
	 }

   // File the rest with zero
   for (int i=N0; i < n; i++)
	 {
		WxA[i] = 0;
		WyA[i] = 0;
		tpXA[i] = 0;
		tpYA[i] = 0;
	 }

   // Save Box values
   int nBoxTop = Box.top;
   int nBoxRight = Box.right;
   int nBoxLeft = Box.left;
   int nBoxBottom = Box.bottom;

   bool bInterpolateLastRow = ((nBoxRight - nBoxLeft)) % 2 == 1;
   int iRA;

   // Sweep for every other row
   for (int iR=nBoxTop; iR <= nBoxBottom; iR += 2)
	 {

	  float *xVRow = xVec + iR*nCol + nBoxLeft;
	  float *yVRow = yVec + iR*nCol + nBoxLeft;
	  float xc = Wx[N0] + Wx[N0+2]*iR;
	  float yc = Wy[N0] + Wy[N0+2]*iR;

	  // Compute the (iR-tpY[i])^2 vector since this doesn't change
	  asm
		{
		  cvtsi2ss xmm1, iR			// xmm1 = (iR, 0, 0, 0)
		  shufps  xmm1, xmm1, 0		// xmm1 = (iR, iR, iR, iR)

		  mov     esi, tpYA
		  mov	  edi, tpIR2
		  mov	  ecx, n              // How many loop
		  shr	  ecx, 2              // Divided by 4
   $1:    movdqa  xmm0, [esi]         //  xmm0 = tpY[i]
		  add     esi, 16
		  subps   xmm0, xmm1          //  xmm0 = (tpy[i] - iR)
		  mulps   xmm0, xmm0          //  xmm0 = (tpy[i] - iR)^2
		  movdqa  [edi], xmm0         //  tpIR2[i] = xmm0
		  add     edi, 16
		  dec	  ecx
		  jne	  $1

		}


	  // Now process the row
	  float vxOld, vyOld;
	  float xR, yR;

	  for (int iC=nBoxLeft; iC <= nBoxRight; iC += 2)
	   {

              asm
                    {
                      xorps   xmm0, xmm0       // xmm0 = 0
                      xorps   xmm1, xmm1       // xmm1 = 0

                      cvtsi2ss xmm2, iC			// xmm2 = (iC, 0, 0, 0)
                      shufps   xmm2, xmm2, 0		// xmm2 = (iC, iC, iC, iC)

                      mov     ecx, n              // How many loop
                      shr     ecx, 2              // Divided by 4

                      mov     eax, tpXA
                      mov     edx, tpIR2
                      mov     esi, WxA
                      mov     edi, WyA

$$1:                  movdqa  xmm3, [eax]         //  xmm3 = tpX[i]
                      add     eax, 16

                      subps   xmm3, xmm2          //  xmm3 = (tpX[i] - iC)
                      mulps   xmm3, xmm3          //  xmm3 =  (tpX[i] - iC)^2
                      addps   xmm3, [edx]         //  xmm3 =  (tpX[i] - iC)^2 + tpIR2[i])
                      add     edx, 16

                      sqrtps  xmm3, xmm3          //  xmm3 = sqrt(tpX[i] - iC)^2 + tpIR2[i])
                      movdqa  xmm4, xmm3          //  xmm4 = xmm3
                      mulps   xmm3, [esi]         //  xmm3 = Wx[i]*sqrt(...)
                      add     esi, 16

                      mulps   xmm4, [edi]         //  xmm4 = Wy[i]*sqrt(...)
                      add     edi, 16

                      addps   xmm0, xmm3         //  Sum them up
                      addps   xmm1, xmm4

                      dec     ecx
                      jne     $$1

                      // Sum up the results
                      movdqa  xmm2,xmm0          // xmm0 = xmm2 = (x3, x2, x1, x0)
                      movdqa  xmm3,xmm1

                      shufps  xmm2,xmm0, 0x1B    // xmm2 = (x0, x1, x2, x3)
                      shufps  xmm3,xmm1, 0x1B

                      addps   xmm2,xmm0          // xmm2 = (x3+x0, x2+x1, x1+x2, x0+x3)
                      addps   xmm3,xmm1

                      movhlps xmm0,xmm2          // xmm0 = ( , , x3+x0, x2 + x1)
                      movhlps xmm1,xmm3

                      addps   xmm0,xmm2          // xmm0 = ( , , , x0 + x3 + x1 + x2)
                      addps   xmm1,xmm3

                      movss   [xR],xmm0          // Store for later use
                      movss   [yR],xmm1
 		 }

              // Sum up the linear and the warp terms
              float xt = xc + Wx[N0+1]*iC + xR;
              float yt = yc + Wy[N0+1]*iC + yR;

              //  See if we need to interpolate
              if (iC != nBoxLeft)
                    {
                      *xVRow++ = 0.5*(xt + vxOld);
                      *yVRow++ = 0.5*(yt + vyOld);
                    }

              //  See if we are at the end of the row
              if (bInterpolateLastRow && (iC == (nBoxRight-1)))
                    {
                      *xVRow++ = xt;
                      *yVRow++ = yt;
                      *xVRow++ = 2*xt - *(xVRow-2);
                      *yVRow++ = 2*yt - *(yVRow-2);

                    }
              else
                    {
                      vxOld = xt;
                      vyOld = yt;
                      *xVRow++ = xt;
                      *yVRow++ = yt;
                    }


	   }
   }

   // Make sure we are 16 byte aligned
   // We assume the floats are 4 byte aligned
   int nStart = (((nBoxLeft+3) / 4) * 4);
   int nEnd = (((nBoxRight+1) / 4) * 4);
   int nLoop = (nEnd - nStart) / 4;
   float half =0.5;

   // BUG 2872: THIS ONLY WORKS IF THE NUMBER OF COLUMNS IS DIVISIBLE BY 4
   if ((nCol & 3) != 0)
     {
       nStart = nBoxLeft;
       nEnd = nBoxLeft;
       nLoop = 0;
     }

   //  Now interpolate the rows
   for (int iR=nBoxTop+1; iR <= nBoxBottom-1; iR += 2)
	 {
	  float *xVRow0 = xVec + (iR-1)*nCol + nBoxLeft;
	  float *yVRow0 = yVec + (iR-1)*nCol + nBoxLeft;
	  float *xVRow1 = xVec + (iR+1)*nCol + nBoxLeft;
	  float *yVRow1 = yVec + (iR+1)*nCol + nBoxLeft;
	  float *xVRow = xVec + iR*nCol + nBoxLeft;
	  float *yVRow = yVec + iR*nCol + nBoxLeft;

	  // Process the unaligned start
	  for (int iC=nBoxLeft; iC < nStart; iC++)
		{
			*xVRow++ = 0.5*(*xVRow0++ + *xVRow1++);
			*yVRow++ = 0.5*(*yVRow0++ + *yVRow1++);
		}

	  // Process the aligned center
	  if (nLoop > 0) asm
		{
		   movss   xmm2, half			// xmm2 = (0.5, 0, 0, 0)
		   shufps  xmm2, xmm2, 0		// xmm1 = (0.5, 0.5, 0.5, 0.5)

		   // Do the x's
		   mov	   eax, xVRow0
		   mov     ebx, xVRow1
		   mov     edi, xVRow
		   mov	   ecx, nLoop
$$2:       movdqa  xmm0, [eax]
		   add	   eax, 16
		   addps   xmm0, [ebx]
		   add	   ebx, 16
		   mulps   xmm0, xmm2
		   movdqa  [edi], xmm0
		   add	   edi, 16

		   dec	   ecx
		   jne	   $$2

		   mov	   xVRow0, eax
		   mov     xVRow1, ebx
		   mov     xVRow, edi

		   //  Do the y's
		   mov	   eax, yVRow0
		   mov     ebx, yVRow1
		   mov     edi, yVRow
		   mov	   ecx, nLoop
$$3:       movdqa  xmm0, [eax]
		   add	   eax, 16
		   addps   xmm0, [ebx]
		   add	   ebx, 16
		   mulps   xmm0, xmm2
		   movdqa  [edi], xmm0
		   add	   edi, 16

		   dec	   ecx
		   jne	   $$3

		   mov	   yVRow0, eax
		   mov     yVRow1, ebx
		   mov     yVRow, edi
		}

	  // Process the remaining words < 4 (or ALL the words, if the number
      // of columns is not divisible by 4!!!)
	  for (int iC=nEnd; iC <= nBoxRight; iC++)
		{
			*xVRow++ = 0.5*(*xVRow0++ + *xVRow1++);
			*yVRow++ = 0.5*(*yVRow0++ + *yVRow1++);
		}
	 }

   // See if the end column is computed or not, if not
   // we have to create it
   if ((((nBoxBottom - nBoxTop)) % 2) == 1)
	{
	  float *xVRow0 = xVec + (nBoxBottom-1)*nCol + nBoxLeft;
	  float *yVRow0 = yVec + (nBoxBottom-1)*nCol + nBoxLeft;
	  float *xVRow1 = xVec + (nBoxBottom-2)*nCol + nBoxLeft;
	  float *yVRow1 = yVec + (nBoxBottom-2)*nCol + nBoxLeft;
	  float *xVRow = xVec + nBoxBottom*nCol + nBoxLeft;
	  float *yVRow = yVec + nBoxBottom*nCol + nBoxLeft;
	  for (int iC=nBoxLeft; iC <= nBoxRight; iC++)
		{
			*xVRow++ = 2**xVRow0++ - *xVRow1++;
			*yVRow++ = 2**yVRow0++ - *yVRow1++;
		}

	}

   delete[] DataBufferOrig;
}

#endif

//---------------------FindStabilizerMotionIdenity-------------------------June 06---

  void FindStabilizerMotionIdenity(float Wx[], float Wy[], float tpX[], float tpY[], int N0, float *xVec, float *yVec, int nCol, int nRow, RECT &Box)

//  This returns 0 motion for every point.  Mainly used for debugging and
//  testing of speed.
//
//******************************************************************************
{
   float *_xVecT = xVec;
   float *_yVecT = yVec;
   for (int iR=Box.top; iR <= Box.bottom; iR++)
	 {
	   for (int iC=Box.left; iC <= Box.right; iC++)
		 {
			*_xVecT++ = iC;
			*_yVecT++ = iR;
		 }
	 }
}

