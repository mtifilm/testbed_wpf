//---------------------------------------------------------------------------

#ifndef StabilizerCoreUnitH
#define StabilizerCoreUnitH
#include "Windows.h"
//---------------------------------------------------------------------------

  void FindStabilizerMotionLinCCode(float Wx[], float Wy[], float tpX[], float tpY[], int N0, float *xVec, float *yVec, int nCol, int nRow, RECT &Box);
  void FindStabilizerMotionRLogCCode(float Wx[], float Wy[], float tpX[], float tpY[], int N0, float *xVec, float *yVec, int nCol, int nRow, RECT &Box);
  void FindStabilizerMotionRRLogCCode(float Wx[], float Wy[], float tpX[], float tpY[], int N0, float *xVec, float *yVec, int nCol, int nRow, RECT &Box);

  void FindStabilizerMotionLinSSE4(float Wx[], float Wy[], float tpX[], float tpY[], int N0, float *xVec, float *yVec, int nCol, int nRow, RECT &Box);

  void FindStabilizerMotionLinSSE(float Wx[], float Wy[], float tpX[], float tpY[], int N0, float *xVec, float *yVec, int nCol, int nRow, RECT &Box);
  void FindStabilizerMotionRLogSSE(float Wx[], float Wy[], float tpX[], float tpY[], int N0, float *xVec, float *yVec, int nCol, int nRow, RECT &Box);
  void FindStabilizerMotionRRLogSSE(float Wx[], float Wy[], float tpX[], float tpY[], int N0, float *xVec, float *yVec, int nCol, int nRow, RECT &Box);

  void FindStabilizerMotionIdenity(float Wx[], float Wy[], float tpX[], float tpY[], int N0, float *xVec, float *yVec, int nCol, int nRow, RECT &Box);

#endif

