object TrackChartForm: TTrackChartForm
  Left = 895
  Top = 150
  Caption = 'Tracking Points'
  ClientHeight = 274
  ClientWidth = 653
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  ShowHint = True
  PixelsPerInch = 96
  TextHeight = 13
  object VTrackGraph: TChart
    Left = 329
    Top = 2
    Width = 320
    Height = 273
    Hint = 
      'Right mouse down to grab and scroll'#13#10'Left mouse down, draw recta' +
      'ngle downwards to zoom in'#13#10'Left mouse down, draw rectangel upwar' +
      'ds to zoom out|Grab with right mouse to scroll, Left mouse draw ' +
      'downwards to zoom in, upwards to zoom out'
    BackImage.Inside = True
    BackWall.Color = clBtnFace
    Legend.Alignment = laTop
    Legend.Color = clBtnFace
    Legend.Font.Color = clWhite
    Legend.HorizMargin = 10
    Legend.Shadow.Color = clGray
    Legend.Visible = False
    MarginRight = 8
    Title.Brush.Color = clBlue
    Title.Color = clBtnFace
    Title.Font.Color = clMenuText
    Title.Font.Height = -13
    Title.Frame.Width = 2
    Title.Text.Strings = (
      '       Vertical Movement')
    Title.AdjustFrame = False
    BottomAxis.Axis.Visible = False
    BottomAxis.Grid.Visible = False
    BottomAxis.Increment = 5.000000000000000000
    BottomAxis.LabelsSeparation = 20
    BottomAxis.TickLength = 6
    BottomAxis.Title.Caption = 'Timecode'
    LeftAxis.Automatic = False
    LeftAxis.AutomaticMaximum = False
    LeftAxis.AutomaticMinimum = False
    LeftAxis.Axis.Visible = False
    LeftAxis.Grid.Visible = False
    LeftAxis.Maximum = 300.000000000000000000
    LeftAxis.Title.Caption = 'Pixels'
    View3D = False
    BevelOuter = bvNone
    TabOrder = 0
    DefaultCanvas = 'TGDIPlusCanvas'
    ColorPaletteIndex = 13
    object Series20: TLineSeries
      SeriesColor = clBlack
      Title = 'Tracking Points'
      Brush.BackColor = clDefault
      Pointer.Brush.Color = clYellow
      Pointer.InflateMargins = True
      Pointer.Style = psCircle
      Pointer.Visible = True
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object Series21: TLineSeries
      SeriesColor = clBlack
      Title = 'Smoothed Points'
      Brush.BackColor = clDefault
      Pointer.Brush.Color = clBlue
      Pointer.InflateMargins = True
      Pointer.Style = psCircle
      Pointer.Visible = True
      XValues.DateTime = True
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
  end
  object HTrackGraph: TChart
    Left = 9
    Top = 2
    Width = 320
    Height = 273
    Hint = 
      'Right mouse down to grab and scroll'#13#10'Left mouse down, draw recta' +
      'ngle downwards to zoom in'#13#10'Left mouse down, draw rectangel upwar' +
      'ds to zoom out|Grab with right mouse to scroll, Left mouse draw ' +
      'downwards to zoom in, upwards to zoom out'
    BackImage.Inside = True
    BackWall.Color = clBtnFace
    Legend.Alignment = laTop
    Legend.Color = clBtnFace
    Legend.Font.Color = clWhite
    Legend.HorizMargin = 10
    Legend.Shadow.Color = clGray
    Legend.Visible = False
    MarginRight = 8
    Title.Brush.Color = clBlue
    Title.Color = clBtnFace
    Title.Font.Color = clMenuText
    Title.Font.Height = -13
    Title.Frame.Width = 2
    Title.Text.Strings = (
      '     Horizontal Movement')
    Title.AdjustFrame = False
    BottomAxis.Axis.Visible = False
    BottomAxis.Grid.Visible = False
    BottomAxis.Increment = 5.000000000000000000
    BottomAxis.LabelsSeparation = 20
    BottomAxis.TickLength = 6
    BottomAxis.Title.Caption = 'Timecode'
    LeftAxis.Automatic = False
    LeftAxis.AutomaticMaximum = False
    LeftAxis.AutomaticMinimum = False
    LeftAxis.Axis.Visible = False
    LeftAxis.Grid.Visible = False
    LeftAxis.Maximum = 300.000000000000000000
    LeftAxis.Title.Caption = 'Pixels'
    View3D = False
    BevelOuter = bvNone
    TabOrder = 1
    DefaultCanvas = 'TGDIPlusCanvas'
    ColorPaletteIndex = 13
    object LineSeries1: TLineSeries
      SeriesColor = clBlack
      Title = 'Tracking Points'
      Brush.BackColor = clDefault
      Pointer.Brush.Color = clYellow
      Pointer.InflateMargins = True
      Pointer.Style = psCircle
      Pointer.Visible = True
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object LineSeries2: TLineSeries
      SeriesColor = clBlack
      Title = 'Smoothed Points'
      Brush.BackColor = clDefault
      Pointer.Brush.Color = clBlue
      Pointer.InflateMargins = True
      Pointer.Style = psCircle
      Pointer.Visible = True
      XValues.DateTime = True
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
  end
end
