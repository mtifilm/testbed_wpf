object Form1: TForm1
  Left = 195
  Top = 117
  Caption = 'Less More'
  ClientHeight = 733
  ClientWidth = 901
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnCloseQuery = FormCloseQuery
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    901
    733)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 32
    Top = 64
    Width = 41
    Height = 13
    Caption = 'In Frame'
  end
  object Label2: TLabel
    Left = 128
    Top = 64
    Width = 51
    Height = 13
    Caption = 'End Frame'
  end
  object Image1: TImage
    Left = 24
    Top = 184
    Width = 857
    Height = 489
    Anchors = [akLeft, akTop, akRight]
    OnMouseDown = Image1MouseDown
    OnMouseMove = Image1MouseMove
    OnMouseUp = Image1MouseUp
  end
  object PositionLabel: TLabel
    Left = 328
    Top = 8
    Width = 21
    Height = 13
    Caption = '(0,0)'
  end
  object FrameLabel: TLabel
    Left = 24
    Top = 168
    Width = 29
    Height = 13
    Caption = 'Frame'
  end
  object TypeLabel: TLabel
    Left = 224
    Top = 168
    Width = 50
    Height = 13
    Caption = 'TypeLabel'
  end
  object Label3: TLabel
    Left = 32
    Top = 112
    Width = 38
    Height = 13
    Caption = 'Good In'
  end
  object Label4: TLabel
    Left = 128
    Top = 112
    Width = 46
    Height = 13
    Caption = 'Good Out'
  end
  object Label5: TLabel
    Left = 224
    Top = 64
    Width = 51
    Height = 13
    Caption = 'Track Size'
  end
  object Label6: TLabel
    Left = 224
    Top = 112
    Width = 57
    Height = 13
    Caption = 'Search Size'
  end
  object Label7: TLabel
    Left = 624
    Top = 24
    Width = 50
    Height = 13
    Caption = 'Smoothing'
  end
  object Label8: TLabel
    Left = 592
    Top = 112
    Width = 70
    Height = 13
    Caption = 'Warp Function'
  end
  object Label9: TLabel
    Left = 32
    Top = 16
    Width = 96
    Height = 13
    Caption = 'Path and File Prefixy'
  end
  object Label10: TLabel
    Left = 592
    Top = 40
    Width = 22
    Height = 13
    Caption = 'Less'
  end
  object Label11: TLabel
    Left = 672
    Top = 40
    Width = 24
    Height = 13
    Caption = 'More'
  end
  object StartSpinEdit: TCSpinEdit
    Left = 32
    Top = 80
    Width = 81
    Height = 22
    TabOrder = 0
    Value = 21
  end
  object EndSpinEdit: TCSpinEdit
    Left = 128
    Top = 80
    Width = 81
    Height = 22
    TabOrder = 1
    Value = 29
  end
  object LoadButton: TBitBtn
    Left = 40
    Top = 694
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Load'
    TabOrder = 2
    OnClick = LoadButtonClick
  end
  object TrackPointMemo: TMemo
    Left = 328
    Top = 32
    Width = 241
    Height = 121
    ScrollBars = ssVertical
    TabOrder = 3
  end
  object TrackButton: TButton
    Left = 128
    Top = 694
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Track'
    TabOrder = 4
    OnClick = TrackButtonClick
  end
  object BackButton: TBitBtn
    Left = 728
    Top = 694
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Back'
    TabOrder = 5
    OnClick = BackButtonClick
  end
  object SmoothButton: TButton
    Left = 216
    Top = 694
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Smooth'
    TabOrder = 6
    OnClick = SmoothButtonClick
  end
  object Stabilizer1Button: TButton
    Left = 336
    Top = 694
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Stabilizer 1'
    TabOrder = 7
    OnClick = Stabilizer1ButtonClick
  end
  object SavePointsButton: TBitBtn
    Left = 728
    Top = 48
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Save TPs'
    TabOrder = 8
    OnClick = SavePointsButtonClick
  end
  object LoadPointsButton: TBitBtn
    Left = 728
    Top = 16
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Load TPs'
    TabOrder = 9
    OnClick = LoadPointsButtonClick
  end
  object ClearAllButton: TBitBtn
    Left = 816
    Top = 16
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Clear TPs'
    TabOrder = 10
    OnClick = ClearAllButtonClick
  end
  object NextFrameButton: TBitBtn
    Left = 808
    Top = 694
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Next'
    TabOrder = 11
    OnClick = NextFrameButtonClick
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 714
    Width = 901
    Height = 19
    Panels = <
      item
        Width = 250
      end
      item
        Width = 50
      end>
    ExplicitTop = 719
    ExplicitWidth = 909
  end
  object GotoInButton: TBitBtn
    Left = 648
    Top = 694
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Goto In'
    TabOrder = 13
    OnClick = GotoInButtonClick
  end
  object ClearSmoothButton: TBitBtn
    Left = 816
    Top = 48
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Clear SPs'
    TabOrder = 14
    OnClick = ClearSmoothButtonClick
  end
  object CheckSolutionButton: TBitBtn
    Left = 728
    Top = 118
    Width = 75
    Height = 25
    Caption = 'Check Solution'
    TabOrder = 15
    OnClick = CheckSolutionButtonClick
  end
  object SwapButton: TButton
    Left = 552
    Top = 694
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Swap'
    TabOrder = 16
    OnClick = SwapButtonClick
  end
  object BitBtn1: TBitBtn
    Left = 816
    Top = 88
    Width = 75
    Height = 25
    Caption = 'Reset BB'
    TabOrder = 17
    OnClick = BitBtn1Click
  end
  object StabilizerAllButton: TButton
    Left = 424
    Top = 694
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Stabilize All'
    TabOrder = 18
    OnClick = StabilizerAllButtonClick
  end
  object GoodInSpinEdit: TCSpinEdit
    Left = 32
    Top = 128
    Width = 81
    Height = 22
    MaxValue = 2
    TabOrder = 19
    Value = 2
  end
  object GoodOutSpinEdit: TCSpinEdit
    Left = 128
    Top = 128
    Width = 81
    Height = 22
    MaxValue = 2
    TabOrder = 20
    Value = 2
  end
  object AlphaEdit: TEdit
    Left = 488
    Top = 0
    Width = 81
    Height = 21
    TabOrder = 21
    Text = '0.0001'
  end
  object BitBtn2: TBitBtn
    Left = 816
    Top = 120
    Width = 75
    Height = 25
    Caption = 'BitBtn2'
    TabOrder = 22
    OnClick = BitBtn2Click
  end
  object TrackSEdit: TCSpinEdit
    Left = 224
    Top = 80
    Width = 81
    Height = 22
    Increment = 5
    TabOrder = 23
    Value = 10
    OnChange = TrackSEditChange
  end
  object SearchSEdit: TCSpinEdit
    Left = 224
    Top = 128
    Width = 81
    Height = 22
    Increment = 5
    TabOrder = 24
    Value = 20
    OnChange = SearchSEditChange
  end
  object UFunctionComboBox: TComboBox
    Left = 592
    Top = 136
    Width = 105
    Height = 21
    Style = csDropDownList
    TabOrder = 25
    OnChange = UFunctionComboBoxChange
  end
  object FileNameEdit: TComboBox
    Left = 32
    Top = 32
    Width = 265
    Height = 21
    AutoComplete = False
    TabOrder = 26
    Text = 'D:\NightlyBuilds\BWDpx\'
    OnDropDown = FileNameEditDropDown
    OnExit = FileNameEditExit
  end
  object SmoothTrackBar: TTrackBar
    Left = 584
    Top = 56
    Width = 121
    Height = 45
    Ctl3D = True
    Max = 8
    ParentCtl3D = False
    TabOrder = 27
    ThumbLength = 15
    TickMarks = tmBoth
    OnChange = SmoothTrackBarChange
  end
end
