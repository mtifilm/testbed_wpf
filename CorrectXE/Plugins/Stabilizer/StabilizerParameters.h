//---------------------------------------------------------------------------

#ifndef StabilizerParametersH
#define StabilizerParametersH     

#include "IPresetParameters.h"

//---------------------------------------------------------------------------
  enum EUFunctionType
  {
     U_FUNCTION_TYPE_INVALID = -1,
     U_FUNCTION_TYPE_LINEAR = 0,
     U_FUNCTION_TYPE_R_LOGR = 1,
     U_FUNCTION_TYPE_NONE = 2,
     U_FUNCTION_TYPE_R2_LOGR = 3
  };

  enum EMissingDataFillType
  {
     MISSING_DATA_INVALID = -1,
     MISSING_DATA_WHITE = 0,
     MISSING_DATA_BLACK = 1,
     MISSING_DATA_PREVIOUS = 2,
     MISSING_DATA_ZOOM = 3
  };

  enum EStabilizationMode
  {
     STABMODE_NONE = -1,
     STABMODE_SMOOTHED = 0,
     STABMODE_FLAT = 1,
     STABMODE_REGISTERED = 2
  };

  enum ETrackingChannel
  {
     R = 0,
     G = 1,
     B = 2,
     Y = 3
  };

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CPDLElement;

//////////////////////////////////////////////////////////////////////
// This is almost a struct
class CStabilizerParameters : public IPresetParameters
{
public:
	CStabilizerParameters(void);
	CStabilizerParameters(const CStabilizerParameters &rhs);

	void ReadParameters(const string &strFileName);
	void WriteParameters(const string &strFileName) const;

	virtual void ReadParameters(CIniFile *ini, const string &iniSectionName);
	virtual void WriteParameters(CIniFile *ini, const string &iniSectionName) const;
	virtual bool AreParametersTheSame(const IPresetParameters &presetParameter) const;

	bool operator == (const CStabilizerParameters &rhs) const;
	bool operator != (const CStabilizerParameters &rhs) const;
	CStabilizerParameters &operator = (const CStabilizerParameters &rhs);

	void ReadPDLEntry(CPDLElement *pdlToolAttribs);
	void WritePDLEntry(CPDLElement &parent);

public:
	// All member variables are public for easy access.

	// TRACKING PARAMETERS
	bool AutoTrackingMode = false;
	bool AutoSourceIsAnimation = false;
	int TrackBoxExtentX = 10;
	int TrackBoxExtentY = 10;
	int SearchBoxExtentLeft = 20;
	int SearchBoxExtentRight = 20;
	int SearchBoxExtentUp = 20;
	int SearchBoxExtentDown = 20;

   // RENDER PARAMETERS
	int GoodFramesIn = 1;
	int GoodFramesOut = 1;
	int RefFrameIndex = 0;
	double Jitter = 0.0;;
	double Alpha2 = 0.0001;
	EStabilizationMode HMode = STABMODE_FLAT;
	EStabilizationMode VMode = STABMODE_FLAT;
	EStabilizationMode RMode = STABMODE_NONE;

	EUFunctionType UFunction = U_FUNCTION_TYPE_LINEAR;
	EMissingDataFillType MissingDataFill = MISSING_DATA_PREVIOUS;

   // Use a predefined mat box
	bool UseMatting = true;
	bool KeepOriginalMatValues = true;

	bool DisableSubPixelInterpolation = false;

   // Advanced tracking
   bool ApplyLutBeforeTracking = false;
   bool DegrainTrackingImage = false;
   ETrackingChannel TrackingChannel = ETrackingChannel::Y;


private:
};

extern CIniAssociations UFunctionAssociations;
extern CIniAssociations UMissingDataAssociations;

//---------------------------------------------------------------------------
#endif
