// StabilizerProc.cpp: implementation of the CStabilizerProc class.

#include "StabilizerProc.h"

#include "bthread.h"
#include "err_stabilizer.h"
#include "ImageFormat3.h"
#include "IppArray.h"
#include "MTImalloc.h"
#include "MTIsleep.h"
#include "MTI_UST.h"
#include "PixelRegions.h"
#include "MathFunctions.h"
#include "StabilizerCoreUnit.h"                 // WTF?? QQQ
#include "StabilizerTool.h"                     // EVIL !!! QQQ
#include "ToolProgressMonitor.h"
#include "ZoomFractional.h"
#include "resampler.h"
#include "SynchronousThreadRunner.h"
#include "MtiMkl.h"
#include "GpuAccessor.h"

#define USE_ROBUST_REGRESSION 1
#define TRACKING_MIN_THREADS 3

#include <iomanip>
#include <math.h>
#include <thread>

///////////////////////////////////////////////////////////////////////////////
// Construction/Destruction
///////////////////////////////////////////////////////////////////////////////

CStabilizerProc::CStabilizerProc(int newToolNumber, const string &newToolName, CToolSystemInterface *newSystemAPI,
	 const bool *newEmergencyStopFlagPtr, IToolProgressMonitor *newToolProgressMonitor)
	 : CToolProcessor(newToolNumber, newToolName, 1, 1, newSystemAPI, newEmergencyStopFlagPtr, newToolProgressMonitor),
	 maskROI(0)
{
	_stabilizerToolObject = NULL;
	_previousFixedImage = NULL;
	StartProcessThread(); // Required by Tool Infrastructure

	MTI_INT64 ust;
	mtiGetUST(&ust);
	setRandSeed(*(unsigned *) &ust);

	// make a Zoom processor
	Zoomer = new CZoomFractional(2);
}

CStabilizerProc::~CStabilizerProc()
{
	delete Zoomer;
}

///////////////////////////////////////////////////////////////////////////////
// ????????
///////////////////////////////////////////////////////////////////////////////

namespace
{

	float(*UEnergy)(float X0, float Y0, float X1, float Y1);

	float UEnergyPoint(const DPOINT &iDP, const DPOINT &jDP)
	{
		return UEnergy(iDP.x, iDP.y, jDP.x, jDP.y);
	}

};

///////////////////////////////////////////////////////////////////////////////
// ????????
///////////////////////////////////////////////////////////////////////////////

int CStabilizerProc::DoProcess(SToolProcessingData &procData)
{
	CAutoErrorReporter autoErr("CStabilizerProc::DoProcess", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
	CHRTimer DoProcessTimer;
	CToolFrameBuffer *inToolFrameBuffer0;
	CToolFrameBuffer *outToolFrameBuffer;
	try
	{
		inToolFrameBuffer0 = GetRequestedInputFrame(0, 0);
		_BaseOrigImage = inToolFrameBuffer0->GetVisibleFrameBufferPtr();
		outToolFrameBuffer = GetRequestedOutputFrame(0, 0);
		_outputImage = outToolFrameBuffer->GetVisibleFrameBufferPtr();
      _nextOrigImage = nullptr;

		if (_useGpu)
		{
			if ((procData.iteration + 1) < GetIterationCount(procData))
			{
				auto inToolFrameBuffer1 = GetRequestedInputFrame(0, 1);
				_nextOrigImage = inToolFrameBuffer1->GetVisibleFrameBufferPtr();
			}
		}
	}
	catch (const std::exception &ex)
	{
		TRACE_0(errout << "***ERROR**** Cannot get input buffers for rendering!");
		TRACE_0(errout << ex.what());
		autoErr.errorCode = -8862;
		autoErr.msg << "Stabilization failed:Cannot get input buffers (-8862)\n" << ex.what();
		return -8862;
	}
	catch (...)
	{
		TRACE_0(errout << "***ERROR**** Cannot get input buffers for rendering!");
		autoErr.errorCode = -8862;
		autoErr.msg << "Stabilization failed: Cannot get input buffers (-8862)";
		return -8862;
	}

	_imageDataIsHalfs = inToolFrameBuffer0->ContainsFloatData();

	if (_useGpu == false)
	{
		try
		{
			// mbraca added this crap on 2008-12-04 to fix "invisible fields
			// getting overwritten with totally unrelated fields" bug!
			// This is pretty stupid... we don't actually want to copy anything
			// here because the full field will get copied to the invisible field
			// after the render, but we need the side effect that sets the output
			// buffer to have the same number of invisible fields as the input buffer
			CopyInputFrameBufferToOutputFrameBuffer(inToolFrameBuffer0, outToolFrameBuffer);
		}
		catch (const std::exception &ex)
		{
			TRACE_0(errout << "***ERROR**** CopyInputFrameBufferToOutputFrameBuffer");
			TRACE_0(errout << ex.what());
			autoErr.errorCode = -8863;
			autoErr.msg << "Stabilization failed: Cannot copy input to output buffer (-8863)\n" << ex.what();
			return -8863;
		}
		catch (...)
		{
			TRACE_0(errout << "***ERROR**** Cannot copy input to output buffer for rendering!");
			autoErr.errorCode = -8863;
			autoErr.msg << "Stabilization failed: Cannot copy input to output buffer (-8863)";
			return -8863;
		}
	}


	try
	{
		// Here comes the ugly part, we must get the last good start frame, if one
		// exists, otherwise color with red.  None of this counts if we don't want
		// fill in missing data
		// QQQ Why do we care in ZOOM mode since the whole purpose of zooming is
		// to eliminate the need to fill?
		if ((_stabilizerParameters.MissingDataFill == MISSING_DATA_PREVIOUS) ||
			 (_stabilizerParameters.MissingDataFill == MISSING_DATA_ZOOM))
		{
			bool thisIsFirstFrameAndThereAreNoAnchors =
				 (_stabilizerParameters.GoodFramesIn <= 0) && (inToolFrameBuffer0->GetFrameIndex() == _inFrameIndex);

			bool thisIsTheLastAnchorFrameAtTheBeginningOfTheSequence =
				 (inToolFrameBuffer0->GetFrameIndex() == (_inFrameIndex + _stabilizerParameters.GoodFramesIn - 1));

			if (thisIsFirstFrameAndThereAreNoAnchors || thisIsTheLastAnchorFrameAtTheBeginningOfTheSequence)
			{
				// Save the data to be used to fill in the first processed frame
				MTImemcpy(_previousFixedImage, _BaseOrigImage, nPicRow*nPicCol*_pxlComponentCount*sizeof(MTI_UINT16));
			}
		}
		else
		{
			// WHITE and BLACK are done at the output stage.
		}
	}
	catch (const std::exception &ex)
	{
		TRACE_0(errout << "***ERROR**** Cannot copy missing fill data for rendering");
		TRACE_0(errout << ex.what());
		autoErr.errorCode = -8864;
		autoErr.msg << "Stabilization failed: Cannot copy missing fill data (-8864)\n" << ex.what();
		return -8864;
	}
	catch (...)
	{
		TRACE_0(errout << "***ERROR**** Cannot copy missing fill data for rendering!");
		autoErr.errorCode = -8864;
		autoErr.msg << "Stabilization failed: Cannot copy missing fill data (-8864)";
		return -8864;
	}

	try
	{
		// We do not want to process the good frames unless we are in "zoom"
		// mode, in which case we need to zoom in on the frame.
		if ((procData.iteration < _stabilizerParameters.GoodFramesIn) &&
			 (_stabilizerParameters.MissingDataFill != MISSING_DATA_ZOOM))
		{
			outToolFrameBuffer->SetForceWrite(false);
			return 0;
		}

		if (inToolFrameBuffer0->GetFrameIndex() >= (_outFrameIndex - _stabilizerParameters.GoodFramesOut))
		{
			// mbraca sez: This makes no sense at all!  QQQ
			// Why aren't good frames at end treated exactly the same as good frames
			// at the beginning? (i.e. process them if zooming, else do nothing)?
			MTImemcpy(_outputImage, _BaseOrigImage, nPicRow*nPicCol*_pxlComponentCount*sizeof(MTI_UINT16));
		}
		else
		{
			bool DoLanczos =
				 _stabilizerParameters.RMode == STABMODE_NONE && !_stabilizerParameters.DisableSubPixelInterpolation;
			bool doIntegralPixel =
				 _stabilizerParameters.RMode == STABMODE_NONE && _stabilizerParameters.DisableSubPixelInterpolation;

			// NOTE: the StabilizeFrame() method might call StabilizeFrameWithLanczos()
			// if it determines that there is no rotaion happening.
			if (DoLanczos)
			{
				StabilizeFrameWithLanczos(outToolFrameBuffer->GetFrameIndex());
			}
			else if (doIntegralPixel)
			{
				StabilizeFrameIntegralPixel(outToolFrameBuffer->GetFrameIndex());
			}
			else
			{
				// If there may be rotaion, need to call this legacy code.
            if (_useGpu)
            {
					if (!StabilizeWithRotationViaLanczosGpu(outToolFrameBuffer->GetFrameIndex()))
					{
						// if a frame stabilizer fails, then we must abort the processing
						int theErrorCode = theError.getError();
						if (theErrorCode == 0)
						{
							theErrorCode = STABILIZER_INTERNAL_ERROR;
						}

						return theErrorCode;
					}
				}
				else
				{
					if (!StabilizeFrame(outToolFrameBuffer->GetFrameIndex()))
					{
						// if a frame stabilizer fails, then we must abort the processing
						int theErrorCode = theError.getError();
						if (theErrorCode == 0)
						{
							theErrorCode = STABILIZER_INTERNAL_ERROR;
						}

						return theErrorCode;
					}
				}
			}
		}
	}
	catch (const std::exception &ex)
	{
		TRACE_0(errout << "***ERROR**** Cannot stabilize frame!");
		TRACE_0(errout << ex.what());
		autoErr.errorCode = -8865;
		autoErr.msg << "Stabilization failed: Cannot stabilize frame (-8865)\n" << ex.what();
		return -8865;
	}
	catch (...)
	{
		TRACE_0(errout << "***ERROR**** Cannot stabilize frame!");
		autoErr.errorCode = -8865;
		autoErr.msg << "Stabilization failed: Cannot stabilize frame (-8865)";
		return -8865;
	}

	try
	{
		if (_stabilizerParameters.MissingDataFill == MISSING_DATA_ZOOM)
		{
			// blow frame up to eliminate edges exposed by translation & rotation
			Zoomer->convert(&_StabilizerBox, &_BoundingBox, _outputImage, _outputImage);

			// copy fixed data over for more information
			// QQQ Why do we care about the _previousFixedImage in ZOOM mode since
			// the whole purpose of zooming is to eliminate the need to fill?
			MTImemcpy(_previousFixedImage, _outputImage, nPicRow*nPicCol*_pxlComponentCount*sizeof(MTI_UINT16));
		}
		else if (_stabilizerParameters.MissingDataFill == MISSING_DATA_PREVIOUS)
		{
			// Save the just-rendered frame to use as fill for the next frame.
			MTImemcpy(_previousFixedImage, _outputImage, nPicRow*nPicCol*_pxlComponentCount*sizeof(MTI_UINT16));
		}
	}
	catch (const std::exception &ex)
	{
		TRACE_0(errout << "***ERROR**** MissingDataFill!");
		TRACE_0(errout << ex.what());
		autoErr.errorCode = -8866;
		autoErr.msg << "Stabilization failed: Cannot zoom or fill after stabilizing! (-8866)\n" << ex.what();
		return -8866;
	}
	catch (...)
	{
		string whichOp = (_stabilizerParameters.MissingDataFill == MISSING_DATA_ZOOM) ? "zoom" : "restore missing data";
		TRACE_0(errout << "***ERROR**** Cannot " << whichOp << " after stabilizing!");
		autoErr.errorCode = -8866;
		autoErr.msg << "Stabilization failed: Cannot " << whichOp << " after stabilizing! (-8866)";
		return -8866;
	}

	try
	{
		// Make sure the frame gets written (we don't set history)
		outToolFrameBuffer->SetForceWrite(true);

		// mbraca added this crap on 2008-12-04 to fix "invisible fields
		// getting overwritten with totally unrelated fields" bug!
		// This is pretty stupid... the Tool Infrastructure uses the
		// "original values" region list to figure out what pixels to
		// copy to the invisible field from the appropriate visible field.
		// No original pixels are involved because the tool does not
		// preserve history!! If you don't do this, the invisible field
		// would be written with whatever was sitting in the buffer (often
		// the last invisible field in the marked range from running a
		// different tool)!
		//
		const CImageFormat *imageFormat = GetSrcImageFormat(0);
		CPixelRegionList* originalValues = outToolFrameBuffer->GetOriginalValuesPixelRegionListPtr();

		originalValues->Add(_ActiveBox, // Always copy the full frame
			 NULL, // Nothing to save
			 imageFormat->getPixelsPerLine(), imageFormat->getLinesPerFrame(), imageFormat->getComponentCountExcAlpha());
	}
	catch (const std::exception &ex)
	{
		TRACE_0(errout << "***ERROR**** Writing data!");
		TRACE_0(errout << ex.what());
		autoErr.errorCode = -8867;
		autoErr.msg << "Stabilization failed: Cannot execute invisible field hack (-8867)\n" << ex.what();
		return -8867;
	}
	catch (...)
	{
		TRACE_0(errout << "***ERROR**** Cannot execute invisible field hack after stabilizing!");
		autoErr.errorCode = -8867;
		autoErr.msg << "Stabilization failed: Cannot execute invisible field hack (-8867)";
		return -8867;
	}

	return 0;
}

///////////////////////////////////////////////////////////////////////////////

int CStabilizerProc::SetIOConfig(CToolIOConfig *toolIOConfig)
{
   // Just use the GPU flag
	GpuAccessor gpuAccessor;
	_useGpu = gpuAccessor.useGpu() && (toolIOConfig->disableGpu == false);

	// Make sure the number of the algorithm processing threads
	// is reasonable
	_processingThreadCount = toolIOConfig->processingThreadCount;
	if (_processingThreadCount < 1)
	{
		_processingThreadCount = 1;
	}
	else if (_processingThreadCount > STAB_MAX_THREAD_COUNT)
	{
		_processingThreadCount = STAB_MAX_THREAD_COUNT;
	}

	 // Tell Tool Infrastructure, via CToolProcessor supertype, about the
	 // number of input and output frames. We read and write one frame
	 // each iteration, and write output to a new buffer, not in place.

    // To support legecy code, we have two paths UGH!!!
	 if (_useGpu)
	 {
		 // Two frames required for each iteration
		 // One new frame needed every iteration
		 SetInputMaxFrames(0, 2, 1); // 'In' port index,
		 SetOutputModifyInPlace(0, 0, false);
		 SetOutputMaxFrames(0, 1); // 'Out' port index,
	 }
	 else
	 {
		 SetInputMaxFrames(0, 1, 1); // Input Port 0: 1 frames, replace 1 each iter
		 SetOutputNewFrame(0, 0); // 'Out' port index, 'In' port index
		 SetOutputMaxFrames(0, 1); // Output Port 0 = 1 frame
	 }

	return 0;
}
///////////////////////////////////////////////////////////////////////////////

int CStabilizerProc::SetFrameRange(CToolFrameRange *toolFrameRange)
{
	CToolFrameRange::SFrameRange &frameRange = toolFrameRange->inFrameRange[0];
	_inFrameIndex = frameRange.inFrameIndex;
	_outFrameIndex = frameRange.outFrameIndex;

	return 0;
}

///////////////////////////////////////////////////////////////////////////////

int CStabilizerProc::SetParameters(CToolParameters *toolParams)
{
	int retVal = 0;

	CStabilizerToolParameters *stabilizerToolParams = static_cast<CStabilizerToolParameters*>(toolParams);

	maskROI = toolParams->GetRegionOfInterest();

	// Make a private copy of the parameters for later reference
	// Caller's toolParam is deleted after this function returns
	_stabilizerParameters = stabilizerToolParams->stabilizerParameters;
	_stabilizerToolObject = stabilizerToolParams->stabilizerToolObject;

	// initialize the Zoom processor
	const CImageFormat *imageFormat = GetSrcImageFormat(0);
	Zoomer->initialize(imageFormat, 2, 2);
   UEnergy = MtiMkl::ULogrithmic; //UThinPlate;

	// Compute the iteration count
	_iterationCount = _outFrameIndex - _inFrameIndex;

	// Done
	return retVal;
}
///////////////////////////////////////////////////////////////////////////////

int CStabilizerProc::BeginProcessing(SToolProcessingData &procData)
{
	GpuAccessor gpuAccessor;
	try
	{
		int retVal = 0;
		int nTmpX, nTmpY;

		// Stabilizer does not save to history, since it is a "full frame" tool
		SetSaveToHistory(false);

		// Just save some values for later

		const CImageFormat *imageFormat = GetSrcImageFormat(0);
		_pxlComponentCount = imageFormat->getComponentCountExcAlpha();
		nPicCol = imageFormat->getPixelsPerLine();
		nPicRow = imageFormat->getLinesPerFrame();
		imageFormat->getComponentValueBlack(_BlackPixel);
		imageFormat->getComponentValueWhite(_WhitePixel);
		_ActiveBox = imageFormat->getActivePictureRect();

		// QQQ bogus
		_stabilizerToolObject->SetControlState(STABILIZER_STATE_RENDERING);

		if (!_stabilizerParameters.UseMatting)
		{
			_BoundingBox = _ActiveBox;
		}
		else
		{
			_BoundingBox = _stabilizerToolObject->getBoundingBox();
		}

		_StabilizerBox = _stabilizerToolObject->GetZoomBox(_BoundingBox);

		// Now allocate a data buffer for the fill in
		if ((_stabilizerParameters.MissingDataFill == MISSING_DATA_PREVIOUS) ||
			 (_stabilizerParameters.MissingDataFill == MISSING_DATA_ZOOM))
		{
		  	_previousFixedImage = new MTI_UINT16[_pxlComponentCount * nPicRow * nPicCol];
		}

		// See if we are using GPU
		// NOTE: to improve access, we really shouldn't create them each time
		// because memory is allocated when necessary.  We should allow the GPU to be changed
		// in an existing one.
		if (_useGpu)
		{
			delete _currentResampler;
			delete _nextResampler;
         delete _thinplateSplineRotator;

			_currentResampler = new CudaResampler16u(gpuAccessor.getDefaultGpu());
			_nextResampler = new CudaResampler16u(gpuAccessor.getDefaultGpu());
         _thinplateSplineRotator = new CudaThinplateSpline16u(gpuAccessor.getDefaultGpu());

			auto imageFormat = GetSrcImageFormat(0);
			MTI_UINT16 maxValues[3], minValues[3];
			imageFormat->getComponentValueMax(&maxValues[0]);
			imageFormat->getComponentValueMin(&minValues[0]);
			_currentResampler->setMinMax(minValues[0], maxValues[0]);
			_nextResampler->setMinMax(minValues[0], maxValues[0]);
         _thinplateSplineRotator->setMinMax(minValues[0], maxValues[0]);
		}

		GetToolProgressMonitor()->SetFrameCount(_outFrameIndex - _inFrameIndex);
		GetToolProgressMonitor()->SetStatusMessage("Rendering");
		GetToolProgressMonitor()->StartProgress();

		// Success, continue
		return retVal;
	}
	catch (const std::exception &ex)
	{
		TRACE_0(errout << "***ERROR**** failed to start processing");
		TRACE_0(errout << ex.what());
		return -8862;
	}
	catch (...)
	{
		TRACE_0(errout << "***UNKNOW ERROR**** Cannot start processing!");
		return -8862;
	}
}
///////////////////////////////////////////////////////////////////////////////

int CStabilizerProc::EndProcessing(SToolProcessingData &procData)
{
	delete[]_previousFixedImage;
	_previousFixedImage = NULL;

	// Did render run to completion or was it aborted?
	if (procData.iteration == _iterationCount)
	{
		_stabilizerToolObject->NotifyRenderedCompleteRange();
		GetToolProgressMonitor()->SetStatusMessage("Stabilization complete");
		GetToolProgressMonitor()->StopProgress(true);
	}
	else
	{
		GetToolProgressMonitor()->SetStatusMessage("Stopped");
		GetToolProgressMonitor()->StopProgress(false);
	}

	// Clean up any GPU
	delete _currentResampler;
	_currentResampler = nullptr;

	delete _nextResampler;
	_nextResampler = nullptr;

   delete _thinplateSplineRotator;
   _thinplateSplineRotator = nullptr;

	return 0;
}
///////////////////////////////////////////////////////////////////////////////

int CStabilizerProc::BeginIteration(SToolProcessingData &procData)
{
	// This is about as simple as you can get...
	// input, output, then release one frame each iteration
	// Whups! I just made it more complex... might want to look one frame ahead
	// in PREVDATA fill mode with no anchors up front there is no previous
	// frame to steal data from for the first frame in the range, so we get
	// the fill data from the second frame instead
	int processedCount = procData.iteration;

	// Assume no GPU, it needs only one buffer
   // Gawd, I hate to write such bad code...
	int countOfInputFramesNeeded = 1;
	if (_useGpu)
	{
	   // Two buffers except at end
		countOfInputFramesNeeded = ((processedCount + 1) == _iterationCount) ? 1 : 2;
	}

	int frameIndexList[] =
	{_inFrameIndex + processedCount, _inFrameIndex + processedCount + 1};

	SetInputFrames(0, countOfInputFramesNeeded, frameIndexList);
	SetOutputFrames(0, 1, frameIndexList);

   // Why only need in
	if (_useGpu == false)
	{
		SetFramesToRelease(0, 1, frameIndexList);
	}

	return 0;
}
///////////////////////////////////////////////////////////////////////////////

int CStabilizerProc::EndIteration(SToolProcessingData &procData)
{
	// Set the status
	// Remember render's good frames are not rendered
	if (procData.iteration >= _stabilizerParameters.GoodFramesIn)
	{
		// These DO count toward secs/frame
	}
	else
	{
		// These DON'T count toward secs/frame
	}

	GetToolProgressMonitor()->BumpProgress();
	_stabilizerToolObject->NotifyFrameRendered(_inFrameIndex + procData.iteration);

	return 0;
}
///////////////////////////////////////////////////////////////////////////////

int CStabilizerProc::GetIterationCount(SToolProcessingData &procData)
{
	// This function is called by the Tool Infrastructure so it knows
	// how many processing iterations to do before stopping
	// Calculate the number of iterations, depends on the state

	return _iterationCount;
}
///////////////////////////////////////////////////////////////////////////////

#define NUM_COMPONENTS 3


// This creates the weights of the transform from the tracking points tpS to tpO
// Wx and Wy return with the weights of of the x and y tranformation.
bool SolveForWeights(TrackPointList &tpS, TrackPointList &tpO, mvector<long double> &Wx, mvector<long double> &Wy)
{
	// // Check for bad logic
	// if (tpO.size() != tpO.getCountOfValidPoints())
	// {
	// theError.set(STABILIZER_ERROR_STABILIZER_INVALID_POINTS,
	// "Internal Error: Some original tracking points are marked invalid");
	// return false;
	// }
	//
	// if (tpS.size() != tpS.getCountOfValidPoints())
	// {
	// theError.set(STABILIZER_ERROR_STABILIZER_INVALID_POINTS,
	// "Internal Error: Some smoothed tracking points are marked invalid");
	// return false;
	// }

	if (tpO.size() != tpS.size())
	{
		theError.set(STABILIZER_ERROR_STABILIZER_INVALID_POINTS,
			 "Internal Error: Some smoothed tracking points are marked invalid");
		return false;
	}

	int N0 = tpO.size();
	int N3 = N0 + 3;
	matrix<long double>L(N3, N3);

	// Begin to fill in the matrix, compute K
	for (int iR = 0; iR < N0; iR++)
	{
		for (int iC = 0; iC < N0; iC++)
		{
			L[iR][iC] = UEnergyPoint(tpS[iR], tpS[iC]);
		}
	}

	// fill with P and tranposed
	for (int i = 0; i < N0; i++)
	{
		L[i][N0] = 1;
		L[i][N0 + 1] = tpS[i].x;
		L[i][N0 + 2] = tpS[i].y;

		L[N0][i] = 1;
		L[N0 + 1][i] = tpS[i].x;
		L[N0 + 2][i] = tpS[i].y;
	}

	// Now do the 3 zeros
	for (int iR = N0; iR < N3; iR++)
	{
		for (int iC = N0; iC < N3; iC++)
		{
			L[iR][iC] = 0;
		}
	}

	// Find the results
	mvector<long double>Hx(N3);
	mvector<long double>Hy(N3);

	for (int i = 0; i < N0; i++)
	{
		Hx[i] = tpO[i].x;
		Hy[i] = tpO[i].y;
	}

	for (int i = N0; i < N3; i++)
	{
		Hx[i] = 0;
		Hy[i] = 0;
	}

	// Now solve for the weights
	mvector<int>mvIndex(N3);
	matrix<long double>LU = L;
	double d;

	// Decompose the matrix
	if (!ludcmp<long double>(LU, mvIndex, d))
	{
		theError.set(STABILIZER_ERROR_STABILIZER_MATRIX_SINGULAR, "Tracking points are close to a stright line");
		return false;
	}

	Wx = Hx;
	lubksb<long double>(LU, mvIndex, Wx);

	Wy = Hy;
	lubksb<long double>(LU, mvIndex, Wy);
	return true;
}

///////////////////////////////////////////////////////////////////////////////

int CStabilizerProc::BiInterpolate(double x, double y, int iC, int iR)
{
	double R, G, B;
	double C00, C10, C01, C11;
	double a, b, c, d;
	MTI_UINT16 *pRI;
	MTI_UINT16 *pRO;

	if ((iC < _BoundingBox.left) || (iR < _BoundingBox.top) || (iC > _BoundingBox.right) || (iR > _BoundingBox.bottom))
	{
		return -1;
	}

	int nPicOffset = NUM_COMPONENTS * nPicCol;
	int nTotalOffset = NUM_COMPONENTS * (((int)y) * nPicCol + (int)x);

	// case 1, See if we are out of image
	if (((int)x < _BoundingBox.left) || ((int)y < _BoundingBox.top) || ((int)x > _BoundingBox.right) ||
		 ((int)y > _BoundingBox.bottom))
	{
		pRO = _outputImage + nPicOffset * iR + NUM_COMPONENTS * iC;
		if ((_stabilizerParameters.MissingDataFill == MISSING_DATA_PREVIOUS) ||
			 (_stabilizerParameters.MissingDataFill == MISSING_DATA_ZOOM))
		{
			pRI = _previousFixedImage + nPicOffset * iR + NUM_COMPONENTS * iC;
			*pRO++ = *pRI++;
			*pRO++ = *pRI++;
			*pRO = *pRI;
		}
		else if (_stabilizerParameters.MissingDataFill == MISSING_DATA_BLACK)
		{
			*pRO++ = _BlackPixel[0];
			*pRO++ = _BlackPixel[1];
			*pRO = _BlackPixel[2];
		}
		else
		{
			// Assume white
			*pRO++ = _WhitePixel[0];
			*pRO++ = _WhitePixel[1];
			*pRO = _WhitePixel[2];
		}

		return 1;
	}

	if (((int)x < _BoundingBox.right) && ((int)y == _BoundingBox.bottom))
	{
		// Case 2, we are at the bottom, we need to interpolate in x direction
		pRI = _BaseOrigImage + nTotalOffset;
		pRO = _outputImage + NUM_COMPONENTS * (iR * nPicCol + iC);
		R = BiLinear(x, y, *pRI, *(pRI + NUM_COMPONENTS), *pRI, *(pRI + NUM_COMPONENTS));

		pRI++;
		G = BiLinear(x, y, *pRI, *(pRI + NUM_COMPONENTS), *pRI, *(pRI + NUM_COMPONENTS));

		pRI++;
		B = BiLinear(x, y, *pRI, *(pRI + NUM_COMPONENTS), *pRI, *(pRI + NUM_COMPONENTS));

		*pRO++ = R;
		*pRO++ = G;
		*pRO = B;
		return 2;
	}
	else if (((int)x == _BoundingBox.right) && ((int)y < _BoundingBox.bottom))
	{
		// Case 3, we are at the right, we need to interpolate in y direction
		pRI = _BaseOrigImage + nTotalOffset;
		pRO = _outputImage + NUM_COMPONENTS * (iR * nPicCol + iC);
		R = BiLinear(x, y, *pRI, *pRI, *(pRI + nPicOffset), *(pRI + nPicOffset));

		pRI++;
		G = BiLinear(x, y, *pRI, *pRI, *(pRI + nPicOffset), *(pRI + nPicOffset));

		pRI++;
		B = BiLinear(x, y, *pRI, *pRI, *(pRI + nPicOffset), *(pRI + nPicOffset));

		*pRO++ = R;
		*pRO++ = G;
		*pRO = B;
		return 3;
	}
	else if (((int)x == _BoundingBox.right) && ((int)y == _BoundingBox.bottom))
	{
		// Case 4, we are at bottom right corner, just copy.
		pRI = pRI = _BaseOrigImage + nTotalOffset;
		pRO = _outputImage + nPicOffset * iR + NUM_COMPONENTS * iC;
		*pRO++ = *pRI++;
		*pRO++ = *pRI++;
		*pRO = *pRI;
		return 4;
	}

	// Case 3, we have real data
	pRI = _BaseOrigImage + nTotalOffset;
	C00 = imageDatumToFloat(*pRI);
	C10 = imageDatumToFloat(*(pRI + NUM_COMPONENTS));
	C01 = imageDatumToFloat(*(pRI + nPicOffset));
	C11 = imageDatumToFloat(*(pRI + nPicOffset + NUM_COMPONENTS));
	a = -C00 + C10;
	b = -C00 + C01;
	c = C00 - C10 - C01 + C11;
	d = C00;

	x = x - (int)x;
	y = y - (int)y;
	R = a * x + b * y + x * y * c + d;

	pRI++;
	C00 = imageDatumToFloat(*pRI);
	C10 = imageDatumToFloat(*(pRI + NUM_COMPONENTS));
	C01 = imageDatumToFloat(*(pRI + nPicOffset));
	C11 = imageDatumToFloat(*(pRI + nPicOffset + NUM_COMPONENTS));
	a = -C00 + C10;
	b = -C00 + C01;
	c = C00 - C10 - C01 + C11;
	d = C00;

	x = x - (int)x;
	y = y - (int)y;
	G = a * x + b * y + x * y * c + d;

	pRI++;
	C00 = imageDatumToFloat(*pRI);
	C10 = imageDatumToFloat(*(pRI + NUM_COMPONENTS));
	C01 = imageDatumToFloat(*(pRI + nPicOffset));
	C11 = imageDatumToFloat(*(pRI + nPicOffset + NUM_COMPONENTS));
	a = -C00 + C10;
	b = -C00 + C01;
	c = C00 - C10 - C01 + C11;
	d = C00;

	x = x - (int)x;
	y = y - (int)y;
	B = a * x + b * y + x * y * c + d;

	pRO = _outputImage + NUM_COMPONENTS * (iR * nPicCol + iC);

	*pRO++ = floatToImageDatum(R);
	*pRO++ = floatToImageDatum(G);
	*pRO = floatToImageDatum(B);

	return 0;
}

///////////////////////////////////////////////////////////////////////////////

int CStabilizerProc::NearestNeighborInterpolate(double x, double y, int iC, int iR)
{
	int iX = (int) floor(x + 0.5);
	int iY = (int) floor(y + 0.5);

	MTI_UINT16 *pRI;
	MTI_UINT16 *pRO;

	// Destination is not allowed to be out of bounds!
	if (iC < _BoundingBox.left || iR < _BoundingBox.top || iC > _BoundingBox.right || iR > _BoundingBox.bottom)
	{
		return -1;
	}

	int nTotalOffset = NUM_COMPONENTS * ((iY * nPicCol) + iX);

	// If the source is out of bounds, need to fill with something.
	if (iX < _BoundingBox.left || iY < _BoundingBox.top || iX > _BoundingBox.right || iY > _BoundingBox.bottom)
	{
		int nPicOffset = NUM_COMPONENTS * nPicCol;
		pRO = _outputImage + nPicOffset * iR + NUM_COMPONENTS * iC;

		if (_stabilizerParameters.MissingDataFill == MISSING_DATA_PREVIOUS ||
			 _stabilizerParameters.MissingDataFill == MISSING_DATA_ZOOM)
		{
			pRI = _previousFixedImage + nPicOffset * iR + NUM_COMPONENTS * iC;
			*pRO++ = *pRI++;
			*pRO++ = *pRI++;
			*pRO = *pRI;
		}
		else if (_stabilizerParameters.MissingDataFill == MISSING_DATA_BLACK)
		{
			*pRO++ = _BlackPixel[0];
			*pRO++ = _BlackPixel[1];
			*pRO = _BlackPixel[2];
		}
		else
		{
			// Assume white
			*pRO++ = _WhitePixel[0];
			*pRO++ = _WhitePixel[1];
			*pRO = _WhitePixel[2];
		}

		return 1;
	}

	pRI = _BaseOrigImage + nTotalOffset;
	pRO = _outputImage + (NUM_COMPONENTS * ((iR * nPicCol) + iC));

	*pRO++ = *pRI++;
	*pRO++ = *pRI++;
	*pRO = *pRI;

	return 0;
}
///////////////////////////////////////////////////////////////////////////////

void CStabilizerProc::FillOutsideBlack(RECT &Box, MTI_UINT16 *pOut)
{
	// Note, this is currently only valid for RGB or YUV formats
	// Must be modified for non-3 pixel formats
	if (_pxlComponentCount != 3)
	{
		return;
	}

	int BytesPerRow = _pxlComponentCount * nPicCol;

	// Clear the top
	for (int iR = 0; iR < Box.top; iR++)
	{
		MTI_UINT16 *pRO = pOut + Box.left * _pxlComponentCount + iR * BytesPerRow;
		for (int iC = Box.left; iC <= Box.right; iC++)
		{
			*pRO++ = _BlackPixel[0];
			*pRO++ = _BlackPixel[1];
			*pRO++ = _BlackPixel[2];
		}
	}

	// Clear the bottom
	for (int iR = Box.top + 1; iR < nPicRow; iR++)
	{
		MTI_UINT16 *pRO = pOut + Box.left * _pxlComponentCount + iR * BytesPerRow;
		for (int iC = Box.left; iC <= Box.right; iC++)
		{
			*pRO++ = _BlackPixel[0];
			*pRO++ = _BlackPixel[1];
			*pRO++ = _BlackPixel[2];
		}
	}

	// Clear the left
	for (int iR = 0; iR < nPicRow; iR++)
	{
		MTI_UINT16 *pRO = pOut + iR * BytesPerRow;
		for (int iC = 0; iC < Box.left; iC++)
		{
			*pRO++ = _BlackPixel[0];
			*pRO++ = _BlackPixel[1];
			*pRO++ = _BlackPixel[2];
		}
	}

	// Clear the right
	for (int iR = 0; iR < nPicRow; iR++)
	{
		MTI_UINT16 *pRO = pOut + (Box.right + 1) * _pxlComponentCount + iR * BytesPerRow;
		for (int iC = Box.right + 1; iC < nPicCol; iC++)
		{
			*pRO++ = _BlackPixel[0];
			*pRO++ = _BlackPixel[1];
			*pRO++ = _BlackPixel[2];
		}
	}
}
///////////////////////////////////////////////////////////////////////////////

bool CStabilizerProc::StabilizerSlice(RECT Box)
	 // Note: This procedure must be reentrent so make sure Box remains local
{
	// First step, choose the stabilizing function
	switch (_stabilizerParameters.UFunction)
	{
	case U_FUNCTION_TYPE_LINEAR:
#ifdef  NO_ASM
		FindStabilizerMotionLinCCode(_Wx, _Wy, _tpSX, _tpSY, _N0, _xVec, _yVec, nPicCol, nPicRow, Box);
#else
		FindStabilizerMotionLinSSE(_Wx, _Wy, _tpSX, _tpSY, _N0, _xVec, _yVec, nPicCol, nPicRow, Box);
#endif
		break;

	case U_FUNCTION_TYPE_R_LOGR:
#ifdef NO_ASM
		FindStabilizerMotionRLogCCode(_Wx, _Wy, _tpSX, _tpSY, _N0, _xVec, _yVec, nPicCol, nPicRow, Box);
#else
		FindStabilizerMotionRLogSSE(_Wx, _Wy, _tpSX, _tpSY, _N0, _xVec, _yVec, nPicCol, nPicRow, Box);
#endif
		break;

	case U_FUNCTION_TYPE_R2_LOGR:
		FindStabilizerMotionRRLogCCode(_Wx, _Wy, _tpSX, _tpSY, _N0, _xVec, _yVec, nPicCol, nPicRow, Box);
		break;

	case U_FUNCTION_TYPE_NONE:
		FindStabilizerMotionIdenity(_Wx, _Wy, _tpSX, _tpSY, _N0, _xVec, _yVec, nPicCol, nPicRow, Box);
		break;

	default:
		throw;
	}

	StabilizeImage(_xVec, _yVec, Box);

	return true;
}
///////////////////////////////////////////////////////////////////////////////

void CStabilizerProc::StabilizeImage(float *xVec, float *yVec, RECT &Box)
{
	// Stabilize the bounding box
	for (int iR = Box.top; iR <= Box.bottom; iR++)
	{
		float *xVecT = xVec + iR * nPicCol + (int)Box.left;
		float *yVecT = yVec + iR * nPicCol + (int)Box.left;

		if (_stabilizerParameters.DisableSubPixelInterpolation)
		{
			for (int iC = (int)Box.left; iC <= (int)Box.right; iC++)
			{
				// NearestNeighborInterpolate puts result in _outputImage
				NearestNeighborInterpolate(*xVecT++, *yVecT++, iC, iR);
			}
		}
		else
		{
			for (int iC = (int)Box.left; iC <= (int)Box.right; iC++)
			{
				// BiInterpolate puts result in _outputImage
				BiInterpolate(*xVecT++, *yVecT++, iC, iR);
			}
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
//
// This structure is designed to pass information to the thread
//
class CStabilizerSliceThreadParameters
{
public:
	CStabilizerSliceThreadParameters(CStabilizerProc *newStabilizerProc) : stabilizerProc(newStabilizerProc)
	{
		Count = 0;
	}

	CStabilizerProc *stabilizerProc;
	RECT Box;
	int Count;
};

///////////////////////////////////////////////////////////////////////////////
//
// This automatically deallocs the thread
//
class CAutoCountingWaitRelease
{
public:
	CAutoCountingWaitRelease(CCountingWait &cw)
	{
		_countingWait = &cw;
	}

	~CAutoCountingWaitRelease(void)
	{
		_countingWait->ReleaseOne();
	}

	void Finishup(void)
	{
	}

private:
	CCountingWait *_countingWait;
};
///////////////////////////////////////////////////////////////////////////////

// Translate the static to class
void StabilizerSliceThreadStart(void *vpAppData, void *vpReserved)
{
	// Cast the pointer
	CStabilizerSliceThreadParameters stabilizerSliceThreadParameters =
		 *reinterpret_cast<CStabilizerSliceThreadParameters*>(vpAppData);

	if (BThreadBegin(vpReserved))
	{
		TRACE_0(errout << "StabilizerSliceThreadStart: BThreadBegin failed");
		stabilizerSliceThreadParameters.stabilizerProc->CountingWait.ReleaseOne();
		exit(1);
	}

	try
	{
		stabilizerSliceThreadParameters.stabilizerProc->StabilizerSlice(stabilizerSliceThreadParameters.Box);
	}
	catch (const std::exception &ex)
	{
		TRACE_0(errout << "***ERROR**** StabilizerSliceThreadStart!");
		TRACE_0(errout << ex.what());
	}
	catch (...)
	{
		TRACE_0(errout << "StabilizerSliceThreadStart: Stabilizer Slice created major error");
	}

	stabilizerSliceThreadParameters.stabilizerProc->CountingWait.ReleaseOne();
}

///////////////////////////////////////////////////////////////////////////////

// Before calling, the following must be set each time
// _outputImage pointer to put the information
// _BaseOrigImage pointer to the image to stabilize
//
// If 'use previous image for missing data' is set then
// _previousFixedImage must contain the previous image.
//
//
// Before starting calling loop, the following must be set once
// _BoundingBox
//
bool CStabilizerProc::StabilizeFrame(int frame)
{
	TrackPointList synthTPO(3);
	TrackPointList synthTPS(3);
	mvector<long double>VWx;
	mvector<long double>VWy;

	// NOTE: Robust arrays are always padded to at least 3 points.
	auto robustTPO = _stabilizerToolObject->RobustArray.getAllTrackPointsAtFrame(frame);
	auto robustTPS = _stabilizerToolObject->SmoothRobustArray.getAllTrackPointsAtFrame(frame);

	synthTPO[0] = robustTPO[0];
	synthTPO[1] = robustTPO[1];
	synthTPO[2] = robustTPO[2];

	synthTPS[0] = robustTPS[0];
	synthTPS[1] = robustTPS[1];
	synthTPS[2] = robustTPS[2];

	TRACE_3(errout << "SYNTHTPO" << endl << synthTPO);
	TRACE_3(errout << "SYNTHTPS" << endl << synthTPS);

	// Compute the warp functions
	if (!SolveForWeights(synthTPS, synthTPO, VWx, VWy))
	{
		TRACE_0(errout << "Stabilizer Frame Failed");
		// autoErr.traceAction = AUTO_ERROR_NO_TRACE;
		// autoErr.errorCode = theError.getError();
		// autoErr.msg << "StabilizeFrame failed: " << theError.getMessage();
		return false;
	}

	// Note - this code was stolen from dewarp, which computes a weight
	// for each individual tracking point, plus affine values. We simply
	// ignore the tracking point weights and use the affine values to give
	// us displacement and rotation

	_N0 = synthTPO.size(); // weight for each point - which we ignore
	int N1 = _N0 + 3; // room for affines

	// Change the mvects into floats
	_Wx = new float[N1];
	_Wy = new float[N1];

	// No warping, so no tracking point weights
	for (int i = 0; i < N1; i++)
	{
		_Wx[i] = 0; // was VWx[i];
		_Wy[i] = 0; // was VWy[i];
	}

	// Copy affine terms
	for (int i = _N0; i < N1; i++)
	{
		_Wx[i] = VWx[i];
		_Wy[i] = VWy[i];
	}

//	TRACE_0(errout << "Weights X" << endl << "(" << _Wx[_N0] << "," << _Wx[_N0 + 1] << "," << _Wx[_N0 + 2] << ")");
//	TRACE_0(errout << "Weights Y" << endl << "(" << _Wy[_N0] << "," << _Wy[_N0 + 1] << "," << _Wy[_N0 + 2] << ")");

	RECT Box = _BoundingBox;

	// Add some jitter to the displacements if indicated
	if (_stabilizerParameters.Jitter > 0.0)
	{
		// here's some ad hoc multi-linearity
		double radius = (_stabilizerParameters.Jitter <= 4.0) ? (_stabilizerParameters.Jitter / 16.0) :
			 ((_stabilizerParameters.Jitter <= 8.0) ? ((_stabilizerParameters.Jitter - 2) / 8.0) :
			 ((_stabilizerParameters.Jitter - 7) / 2.0));
		radius *= (Box.bottom - Box.top) / 486.0; // Scale up for larger formats

		// getRandNormal() supposedly returns normal distribution about 0,
		// with standard deviation 1.0 (effectively -2 to 2 range)
		_Wx[_N0] += getRandNormal() * radius;
		_Wy[_N0] += getRandNormal() * radius;
	}

	// Make real sure there is never skewing (seems to get round-off errors)
	_Wx[_N0 + 1] = 1.0; // X multiplier - NO SCALING
	_Wy[_N0 + 2] = 1.0; // Y multiplier - NO SCALING

	// If no rotation, zap those babies too
	if (_stabilizerParameters.RMode == STABMODE_NONE)
	{
		_Wx[_N0 + 2] = 0.0; // Y multiplier - NO ROTATION
		_Wy[_N0 + 1] = 0.0; // X multiplier - NO ROTATION
	}

	////////////////////////////////////////////////////////////////
	/////   HACK!!!   If the rotation is zero - DO LANCZOS INSTEAD!
	/////   QQQ Really ahould do rotate-capable Lanczos code.
	if (fabs(_Wx[_N0 + 2]) < 0.000001 && fabs(_Wy[_N0 + 1]) < 0.000001)
	{
		delete[]_Wx;
		delete[]_Wy;
		_Wx = _Wy = NULL;

		StabilizeFrameWithLanczos(frame);

		// StabilizeFrameWithLanczos() is void (throws errors)
		return true;
	}
	/////////////////////////////////////////////////////////////////

	// Change the smoothed points into float vects
	_tpSX = new float[_N0];
	_tpSY = new float[_N0];

	// This is fucked up because SmoothArray is never set, and I don't have time to figure out what's up. QQQ
	// TrackPointList tpS = _stabilizerToolObject->SmoothedArray.getAllTrackPointsAtFrame(frame);
	for (int i = 0; i < _N0; i++)
	{
		// _tpSX[i] = tpS[i].x;
		// _tpSY[i] = tpS[i].y;
		_tpSX[i] = 0;
		_tpSY[i] = 0;
	}

	// Outside is always filled black
	if (!_stabilizerParameters.KeepOriginalMatValues)
	{
		FillOutsideBlack(_BoundingBox, _outputImage);
	}

	// Copy over the data so we are aligned for SSE instructions
	int vecSizeInBytes = nPicRow * nPicCol*sizeof(float);
	_xVec = (float *)MTImemalign(16, vecSizeInBytes);
	_yVec = (float *)MTImemalign(16, vecSizeInBytes);

	// Fire up a thread to work on each slice.
	CountingWait.SetCount(_processingThreadCount);

	int sliceSize = (_BoundingBox.bottom - _BoundingBox.top) / _processingThreadCount;
	Box.bottom = -1;

	// Do _processingThreadCount-1 slices
	CStabilizerSliceThreadParameters stabilizerSliceThreadParameters(this);
	// Note: This starts the count at 0

	vector<void *>bThreadStructs;
	for (int i = 0; i < _processingThreadCount - 1; i++)
	{
		Box.top = Box.bottom + 1;
		Box.bottom += sliceSize;
		stabilizerSliceThreadParameters.Box = Box;
		auto ts = BThreadSpawn(StabilizerSliceThreadStart, &stabilizerSliceThreadParameters);
		MTIassert(ts != nullptr);
		bThreadStructs.push_back(ts);
		stabilizerSliceThreadParameters.Count++;
	}

	// Do last one, this avoids round off errors
	Box.top = Box.bottom + 1;
	Box.bottom = _BoundingBox.bottom;
	stabilizerSliceThreadParameters.Box = Box;
	BThreadSpawn(StabilizerSliceThreadStart, &stabilizerSliceThreadParameters);

	// Now wait for it to finish
	CountingWait.Wait(bThreadStructs, __func__, __LINE__);

	// Free it all up and mark not allocated
	delete[]_Wx;
	delete[]_Wy;
	delete[]_tpSX;
	delete[]_tpSY;
	MTIfree(_xVec);
	MTIfree(_yVec);

	// Null them out
	_Wx = _Wy = NULL;
	// _tpSX = _tpSY = NULL;
	_xVec = _yVec = NULL;

	return true;
}

const int NumberOfSlices = 32;

///////////////////////////////////////////////////////////////////////////////
//
namespace
{

	IppiRect computeSliceRoiFromSliceNumber(int height, int width, int sliceNumber)
	{
		IppiRect sliceRoi;
		int nominalRowsPerSlice = height / NumberOfSlices;
		int extraRows = height - (nominalRowsPerSlice * NumberOfSlices);
		sliceRoi.y = (sliceNumber * nominalRowsPerSlice) + std::min<int>(sliceNumber, extraRows);
		sliceRoi.height = nominalRowsPerSlice + ((sliceNumber < extraRows) ? 1 : 0);
		sliceRoi.x = 0;
		sliceRoi.width = width;

		return sliceRoi;
	}

	struct ConvertPlanarToFloatParams
	{
		MtiPlanar<Ipp16uArray>in;
		MtiPlanar<Ipp32fArray>out;
	};

	int runConvertPlanarToFloat(void *paramsArg, int jobNumber, int totalJobs)
	{
		auto params = (ConvertPlanarToFloatParams*) paramsArg;

		// CHRTimer PlanarToFloatThreadTimer;

		int component = jobNumber % 3;
		int sliceNumber = jobNumber / 3;
		int height = params->in[component].getRows();
		int width = params->in[component].getCols();
		auto sliceRoi = computeSliceRoiFromSliceNumber(height, width, sliceNumber);

		Ipp16uArray roiInArray = params->in[component];
		Ipp32fArray roiOutArray = params->out[component];
		Ipp16uArray sliceInArray = roiInArray(sliceRoi);
		Ipp32fArray sliceOutArray = roiOutArray(sliceRoi);

		// This converts and copies ROI
		sliceOutArray <<= sliceInArray;

		return 0;
	}

	struct SliceResamplerParams
	{
		Ipp32fArray inputPlane;
		Ipp32fArray outputPlane;
		float minValue;
		float maxValue;
		float xOffset;
		float yOffset;
	};

	struct ConvertPlanarToIntParams
	{
		vector<Ipp32fArray>in;
		vector<Ipp16uArray>out;
	};

	int runConvertPlanarToInt(void *paramsArg, int jobNumber, int totalJobs)
	{
		auto params = (ConvertPlanarToIntParams*) paramsArg;

		// CHRTimer PlanarToIntThreadTimer;

		int component = jobNumber % 3;
		int sliceNumber = jobNumber / 3;
		int height = params->in[component].getRows();
		int width = params->in[component].getCols();
		auto sliceRoi = computeSliceRoiFromSliceNumber(height, width, sliceNumber);

		Ipp32fArray roiInArray = params->in[component];
		Ipp16uArray roiOutArray = params->out[component];
		Ipp32fArray sliceInArray = roiInArray(sliceRoi);
		Ipp16uArray sliceOutArray= roiOutArray(sliceRoi);

		// This converts and copies ROI
		sliceOutArray <<= sliceInArray;

		return 0;
	}

	int runSliceResampler(void *vp, int jobNumber, int totalJobs)
	{
		SliceResamplerParams *params = (SliceResamplerParams*) vp;
		if (params == nullptr)
		{
			return -1;
		}

		MTIassert((params->inputPlane).getRows() == (params->outputPlane).getRows() && (params->inputPlane).getCols() ==
			 (params->outputPlane).getCols());

		// WARNING***   OFFSETS ARE ROW,COL   NOT (x,y)
		int height = params->inputPlane.getRows();
		int width = params->inputPlane.getCols();
		IppiRect sliceRoi = computeSliceRoiFromSliceNumber(height, width, jobNumber);
		params->inputPlane.lanczosResampleRoi(params->outputPlane, sliceRoi, params->minValue, params->maxValue,
			 params->yOffset, params->xOffset);

		return 0;
	}

}

// end anonymous namespace
///////////////////////////////////////////////////////////////////////////////

std::pair<float, float>CStabilizerProc::getStabilizationDeltas(int frameIndex)
{
	// Amount to displace is given by the difference between the robust points
	// and their smoothed counterparts. The robust arrays always have three
	// points at each frame. Since we're ignoring rotation at this point,
	// only point 0 is needed.

	auto robustTPO = _stabilizerToolObject->RobustArray.getAllTrackPointsAtFrame(frameIndex);
	auto robustTPS = _stabilizerToolObject->SmoothRobustArray.getAllTrackPointsAtFrame(frameIndex);
	float xSourceOffset = robustTPO[0].x - robustTPS[0].x;
	float ySourceOffset = robustTPO[0].y - robustTPS[0].y;

	return
	{
		xSourceOffset, ySourceOffset
	};
}

vector<vector<float>>CStabilizerProc::getStabilizationRotationWeights(int frameIndex)
{
	TrackPointList synthTPO(3);
	TrackPointList synthTPS(3);
	mvector<long double>VWx;
	mvector<long double>VWy;

	// NOTE: Robust arrays are always padded to at least 3 points.
	auto robustTPO = _stabilizerToolObject->RobustArray.getAllTrackPointsAtFrame(frameIndex);
	auto robustTPS = _stabilizerToolObject->SmoothRobustArray.getAllTrackPointsAtFrame(frameIndex);

	synthTPO[0] = robustTPO[0];
	synthTPO[1] = robustTPO[1];
	synthTPO[2] = robustTPO[2];

	synthTPS[0] = robustTPS[0];
	synthTPS[1] = robustTPS[1];
	synthTPS[2] = robustTPS[2];

	TRACE_3(errout << "SYNTHTPO" << endl << synthTPO);
	TRACE_3(errout << "SYNTHTPS" << endl << synthTPS);

	// Compute the warp functions

	if (!SolveForWeights(synthTPS, synthTPO, VWx, VWy))
	{
		TRACE_0(errout << "Stabilizer Frame Failed: Could not solve for weights");
      throw std::runtime_error("Stabilizer Frame Failed: Could not solve for weights");
	}

	// Note - this code was stolen from dewarp, which computes a weight
	// for each individual tracking point, plus affine values. We simply
	// ignore the tracking point weights and use the affine values to give
	// us displacement and rotation

	auto N0 = synthTPO.size(); // weight for each point - which we ignore
	int N1 = N0 + 3; // room for affines

	// Change the mvects into floats
	auto Wx = vector<float>(3);
	auto Wy = vector<float>(3);

	// Copy affine terms
	for (int i = N0; i < N1; i++)
	{
		Wx[i-N0] = VWx[i];
		Wy[i-N0] = VWy[i];
	}

//	TRACE_0(errout << "Weights X" << endl << "(" << Wx[0] << "," << Wx[1] << "," << Wx[2] << ")");
 //	TRACE_0(errout << "Weights Y" << endl << "(" << Wy[0] << "," << Wy[1] << "," << Wy[2] << ")");
   return {Wx, Wy};
}

int CStabilizerProc::StabilizeImageGpuAsync(int frameIndex, Ipp16uArray &imageToProcess, CudaResampler16u *resampler)
{
	float xSourceOffset;
	float ySourceOffset;
	std::tie(xSourceOffset, ySourceOffset) = getStabilizationDeltas(frameIndex);

	CHRTimer translateTimer;
	resampler->translateStartAsync(imageToProcess, imageToProcess,  -xSourceOffset, -ySourceOffset);
	resampler->translateCopyResultAsync();
	return 0;
}

int CStabilizerProc::FillMissingData(int frameIndex, Ipp16uArray &&imageToProcess, const Ipp16uArray &imageFill)
{
	imageToProcess.throwOnArraySizeChannelMismatch(imageFill);

	float xSourceOffset;
	float ySourceOffset;
	std::tie(xSourceOffset, ySourceOffset) = getStabilizationDeltas(frameIndex);

	// Compute areas within the processing region that need to be filled.
	int emptyRoiRowsAtTop = (ySourceOffset < 0.F) ? ceil(-ySourceOffset) : 0;
	int emptyRoiRowsAtBottom = (ySourceOffset > 0.F) ? ceil(ySourceOffset) : 0;
	int emptyRoiColsAtLeft = (xSourceOffset < 0.F) ? ceil(-xSourceOffset) : 0;
	int emptyRoiColsAtRight = (xSourceOffset > 0.F) ? ceil(xSourceOffset) : 0;

	// Clamping values
	auto imageFormat = GetSrcImageFormat(0);
	MTI_UINT16 maxValues[3], minValues[3];
	imageFormat->getComponentValueMax(&maxValues[0]);
	imageFormat->getComponentValueMin(&minValues[0]);
	auto roiProcRegion = imageToProcess.getRoi();
	IppiRect roiInSubImage;
	roiInSubImage.x = emptyRoiColsAtLeft;
	roiInSubImage.y = emptyRoiRowsAtTop;
	roiInSubImage.width = roiProcRegion.width - (emptyRoiColsAtLeft + emptyRoiColsAtRight);
	roiInSubImage.height = roiProcRegion.height - (emptyRoiRowsAtTop + emptyRoiRowsAtBottom);

	CHRTimer MissingDataFillTimer;

	// Create the ROIs for each of the four fill rectangles. Note that only one
	// of {top, bottom} can have non-zero height and onl;y one of {left, right}
	// can have non-zero width. Struct order is { x, y, width, height }
	IppiRect fillTopRoi =
	{0, 0, roiProcRegion.width, emptyRoiRowsAtTop};
	IppiRect fillBottomRoi =
	{0, roiInSubImage.height, roiProcRegion.width, emptyRoiRowsAtBottom};
	IppiRect fillLeftRoi =
	{0, roiInSubImage.y, emptyRoiColsAtLeft, roiInSubImage.height};
	IppiRect fillRightRoi =
	{roiInSubImage.width, roiInSubImage.y, emptyRoiColsAtRight, roiInSubImage.height};

	Ipp16uArray imageProcRegionTopFill = imageToProcess(fillTopRoi);
	Ipp16uArray imageProcRegionBottomFill = imageToProcess(fillBottomRoi);
	Ipp16uArray imageProcRegionLeftFill = imageToProcess(fillLeftRoi);
	Ipp16uArray imageProcRegionRightFill = imageToProcess(fillRightRoi);

	// Fill in the missing data areas.
	switch (_stabilizerParameters.MissingDataFill)
	{
	case MISSING_DATA_WHITE:
		imageProcRegionTopFill.set({maxValues[0], maxValues[1], maxValues[2]});
		imageProcRegionBottomFill.set({maxValues[0], maxValues[1], maxValues[2]});
		imageProcRegionLeftFill.set({maxValues[0], maxValues[1], maxValues[2]});
		imageProcRegionRightFill.set({maxValues[0], maxValues[1], maxValues[2]});
		break;

	case MISSING_DATA_BLACK:
		imageProcRegionTopFill.set({minValues[0], minValues[1], minValues[2]});
		imageProcRegionBottomFill.set({minValues[0], minValues[1], minValues[2]});
		imageProcRegionLeftFill.set({minValues[0], minValues[1], minValues[2]});
		imageProcRegionRightFill.set({minValues[0], minValues[1], minValues[2]});
		break;

	case MISSING_DATA_PREVIOUS:
		imageProcRegionTopFill <<= imageFill(fillTopRoi);
		imageProcRegionBottomFill <<= imageFill(fillBottomRoi);
		imageProcRegionLeftFill <<= imageFill(fillLeftRoi);
		imageProcRegionRightFill <<= imageFill(fillRightRoi);
		break;

	case MISSING_DATA_ZOOM:
		// MISSING_DATA_ZOOM "fill" means there will be no empty area once the ROI
		// is zoomed (at higher level) so we do nothing here in that case/
		break;

	default:
		break;
	}

	return 0;
}

vector<Ipp16uArray> CStabilizerProc::SaveMissingDataForFrame(int frameIndex, const Ipp16uArray &imageFill)
{
	float xSourceOffset;
	float ySourceOffset;
	std::tie(xSourceOffset, ySourceOffset) = getStabilizationDeltas(frameIndex);

	// Compute areas within the processing region that need to be filled.
	int emptyRoiRowsAtTop = (ySourceOffset < 0.F) ? ceil(-ySourceOffset) : 0;
	int emptyRoiRowsAtBottom = (ySourceOffset > 0.F) ? ceil(ySourceOffset) : 0;
	int emptyRoiColsAtLeft = (xSourceOffset < 0.F) ? ceil(-xSourceOffset) : 0;
	int emptyRoiColsAtRight = (xSourceOffset > 0.F) ? ceil(xSourceOffset) : 0;

	// Clamping values
	auto imageFormat = GetSrcImageFormat(0);
	MTI_UINT16 maxValues[3], minValues[3];
	imageFormat->getComponentValueMax(&maxValues[0]);
	imageFormat->getComponentValueMin(&minValues[0]);
	auto roiProcRegion = imageFill.getRoi();
	IppiRect roiInSubImage;
	roiInSubImage.x = emptyRoiColsAtLeft;
	roiInSubImage.y = emptyRoiRowsAtTop;
	roiInSubImage.width = roiProcRegion.width - (emptyRoiColsAtLeft + emptyRoiColsAtRight);
	roiInSubImage.height = roiProcRegion.height - (emptyRoiRowsAtTop + emptyRoiRowsAtBottom);

	// Create the ROIs for each of the four fill rectangles. Note that only one
	// of {top, bottom} can have non-zero height and onl;y one of {left, right}
	// can have non-zero width. Struct order is { x, y, width, height }
	IppiRect fillTopRoi =
	{0, 0, roiProcRegion.width, emptyRoiRowsAtTop};
	IppiRect fillBottomRoi =
	{0, roiInSubImage.height, roiProcRegion.width, emptyRoiRowsAtBottom};
	IppiRect fillLeftRoi =
	{0, roiInSubImage.y, emptyRoiColsAtLeft, roiInSubImage.height};
	IppiRect fillRightRoi =
	{roiInSubImage.width, roiInSubImage.y, emptyRoiColsAtRight, roiInSubImage.height};

   // Fill with fill image, this wastes a copy if we are filling with white or black
	Ipp16uArray imageProcRegionTopFill;
   imageProcRegionTopFill <<= imageFill(fillTopRoi);

	Ipp16uArray imageProcRegionBottomFill;
   imageProcRegionBottomFill <<= imageFill(fillBottomRoi);

	Ipp16uArray imageProcRegionLeftFill;
   imageProcRegionLeftFill <<= imageFill(fillLeftRoi);

	Ipp16uArray imageProcRegionRightFill;
   imageProcRegionRightFill <<= imageFill(fillRightRoi);

	// Fill in the missing data areas.
	switch (_stabilizerParameters.MissingDataFill)
	{
	case MISSING_DATA_WHITE:
		imageProcRegionTopFill.set({maxValues[0], maxValues[1], maxValues[2]});
		imageProcRegionBottomFill.set({maxValues[0], maxValues[1], maxValues[2]});
		imageProcRegionLeftFill.set({maxValues[0], maxValues[1], maxValues[2]});
		imageProcRegionRightFill.set({maxValues[0], maxValues[1], maxValues[2]});
		break;

	case MISSING_DATA_BLACK:
		imageProcRegionTopFill.set({minValues[0], minValues[1], minValues[2]});
		imageProcRegionBottomFill.set({minValues[0], minValues[1], minValues[2]});
		imageProcRegionLeftFill.set({minValues[0], minValues[1], minValues[2]});
		imageProcRegionRightFill.set({minValues[0], minValues[1], minValues[2]});
		break;

	case MISSING_DATA_PREVIOUS:
      // Already done
   	break;

	case MISSING_DATA_ZOOM:
		// MISSING_DATA_ZOOM "fill" means there will be no empty area once the ROI
		// is zoomed (at higher level) so we do nothing here in that case/
		break;

	default:
		break;
	}

	return {imageProcRegionTopFill, imageProcRegionBottomFill, imageProcRegionLeftFill, imageProcRegionRightFill};
}

void CStabilizerProc::StabilizeFrameWithLanczosGpu(int frameIndex)
{
	// This algorithm ping pongs between two GPU streams.
	// Two frames come in, and both are modified in place.
	// At initialization, the current is fired upwith first frame
	// and then enters common execution.
	// Common execution just fires up the next image stab and then
	// waits for the current to complete.

   // WARNING, the _inFrameIndex may not be the first time this is called with frameIndex
   // This is because the first frame may not be changed

	auto imageFormat = GetSrcImageFormat(0);
	int width = imageFormat->getPixelsPerLine();
	int height = imageFormat->getLinesPerFrame();

	IppiRect roiProcRegion;
	roiProcRegion.x = _BoundingBox.left;
	roiProcRegion.y = _BoundingBox.top;
	roiProcRegion.width = _BoundingBox.right - _BoundingBox.left + 1;
	roiProcRegion.height = _BoundingBox.bottom - _BoundingBox.top + 1;

///*****************  This is as fast as the overlap one however
/// we need the overlap for times the GPU is slower
//		// This is the first image in, we use the same image as the fill
//		Ipp16uArray imageIn(height, width, 3, _outputImage);
//		auto imageRoiIn = imageIn(roiProcRegion);
//		StabilizeImageGpuAsync(frameIndex, imageRoiIn, _nextResampler);
//      _nextResampler->wait();
//
//		if (_fillArray.isNull())
//      {
//          _fillArray = imageRoiIn;
//      }
//
//  	   FillMissingData(frameIndex, imageRoiIn, _fillArray);
//  		_fillArray = imageRoiIn;
//		return;
/////// END HACK

   // If we haven't started up a resample, start one sync
	if (_nextResampler->getInArray().isEmpty())
	{
		// This is the first image in, we fire it up
		Ipp16uArray imageIn({width, height, 3}, _BaseOrigImage);
		Ipp16uArray imageRoiIn= imageIn(roiProcRegion);
		StabilizeImageGpuAsync(frameIndex, imageRoiIn, _nextResampler);
	}

	// the _nextResampler now becomes the current sampler the old current
	// must have completed
	std::swap(_nextResampler, _currentResampler);

	if ((frameIndex + 1) < _outFrameIndex)
	{
		// Get the next image
		Ipp16uArray imageIn({width, height, 3}, _nextOrigImage);
		Ipp16uArray imageRoiIn = imageIn(roiProcRegion);
		StabilizeImageGpuAsync(frameIndex + 1, imageRoiIn, _nextResampler);
	}

	// Lets wait for the current image to finish, when it does, the
	// next will automaticall start
	CHRTimer WaitTimer;
	_currentResampler->wait();
// 	DBTRACE(WaitTimer);

   // we fill the current processed frame with the fill data
   Ipp16uArray fillArray({width, height, 3}, _previousFixedImage);
   FillMissingData(frameIndex, _currentResampler->getInArray(), fillArray(roiProcRegion));
}

///////////////////////////////////////////////////////////////////////////////

bool CStabilizerProc::StabilizeWithRotationViaLanczosGpu(int frameIndex)
{
	auto imageFormat = GetSrcImageFormat(0);
	int width = imageFormat->getPixelsPerLine();
	int height = imageFormat->getLinesPerFrame();

	// Fill values, may or may not be used, don't really belong here
   // should be set once on set io.
	MTI_UINT16 whiteFillValues[3];
   MTI_UINT16 blackFillValues[3];
	imageFormat->getComponentValueWhiteFill(&whiteFillValues[0]);
	imageFormat->getComponentValueBlackFill(&blackFillValues[0]);

   int fill = -1;  // We assume each plane has same fill
	switch (_stabilizerParameters.MissingDataFill)
	{
	case MISSING_DATA_WHITE:
   	fill = whiteFillValues[0];
     	break;

   case MISSING_DATA_BLACK:
      fill = blackFillValues[1];
      break;

   default:
      fill = -1;
   }

	IppiRect roiProcRegion;
	roiProcRegion.x = _BoundingBox.left;
	roiProcRegion.y = _BoundingBox.top;
	roiProcRegion.width = _BoundingBox.right - _BoundingBox.left + 1;
	roiProcRegion.height = _BoundingBox.bottom - _BoundingBox.top + 1;

   // No overlap for now
	Ipp16uArray imageIn({width, height, 3}, _BaseOrigImage);
	Ipp16uArray imageRoiIn = imageIn(roiProcRegion);
	Ipp16uArray imageOut({width, height, 3}, _outputImage);
	Ipp16uArray imageRoiOut = imageOut(roiProcRegion);

   auto weights = getStabilizationRotationWeights(frameIndex);
   _thinplateSplineRotator->warpLocation(imageRoiIn, imageRoiOut, weights, {}, fill, 1);
	return true;
}

///////////////////////////////////////////////////////////////////////////////

void CStabilizerProc::StabilizeFrameWithLanczos(int frameIndex)
{
	if (_useGpu)
	{
		StabilizeFrameWithLanczosGpu(frameIndex);
		return;
	}

	MTI_UINT16 *sourceData = _BaseOrigImage;

	// Amount to displace is given by the difference between the robust points
	// and their smoothed counterparts. The robust arrays always have three
	// points at each frame. Since we're ignoring rotation at this point,
	// only point 0 is needed.
	auto robustTPO = _stabilizerToolObject->RobustArray.getAllTrackPointsAtFrame(frameIndex);
	auto robustTPS = _stabilizerToolObject->SmoothRobustArray.getAllTrackPointsAtFrame(frameIndex);
	float xSourceOffset = (robustTPO[0].x - robustTPS[0].x);
	float ySourceOffset = robustTPO[0].y - robustTPS[0].y;

	// TRACE_0(errout << "Frame " << frameIndex << endl
	// << "    xoff = " << xSourceOffset << endl
	// << "    yoff = " << ySourceOffset);

	auto imageFormat = GetSrcImageFormat(0);
	auto width = int(imageFormat->getPixelsPerLine());
	auto height = int(imageFormat->getLinesPerFrame());

	// Clamping values
	MTI_UINT16 maxValues[3], minValues[3];
	imageFormat->getComponentValueMax(&maxValues[0]);
	imageFormat->getComponentValueMin(&minValues[0]);

	// Fill values
	MTI_UINT16 whiteFillValues[3];
   MTI_UINT16 blackFillValues[3];
	imageFormat->getComponentValueWhiteFill(&whiteFillValues[0]);
	imageFormat->getComponentValueBlackFill(&blackFillValues[0]);

	Ipp16uArray imageIn({width, height, 3}, sourceData);
	MtiRect roiProcRegion;
	roiProcRegion.x = _BoundingBox.left;
	roiProcRegion.y = _BoundingBox.top;
	roiProcRegion.width = _BoundingBox.right - _BoundingBox.left + 1;
	roiProcRegion.height = _BoundingBox.bottom - _BoundingBox.top + 1;

	CHRTimer ConvertDataInTimer;

	Ipp16uArray imageRoiIn = imageIn(roiProcRegion);

	CHRTimer copyToPlanarTimer;
	auto planes16uIn = imageRoiIn.copyToPlanar();

	Ipp32fArray P32f0(roiProcRegion.getSize());
	Ipp32fArray P32f1(roiProcRegion.getSize());
	Ipp32fArray P32f2(roiProcRegion.getSize());

	vector<Ipp32fArray>planes32fIn =
	{P32f0, P32f1, P32f2};

	CHRTimer ConvertPlanarToFloatTimer;
#ifdef USE_STD_THREADS

	std:: thread t0c([&]() {planes32fIn[0] = Ipp32fArray(planes16uIn[0]);});
	std:: thread t1c([&]() {planes32fIn[1] = Ipp32fArray(planes16uIn[1]);});
	std:: thread t2c([&]() {planes32fIn[2] = Ipp32fArray(planes16uIn[2]);});

	t0c.joinxxx();
	t1c.joinxxx();
	t2c.joinxxxx();

#else

	ConvertPlanarToFloatParams convertPlanarToFloatParams;
	convertPlanarToFloatParams.in = planes16uIn;
	convertPlanarToFloatParams.out = planes32fIn;
	SynchronousThreadRunner convertPlanarToFloatThreadRunner(3 * NumberOfSlices, &convertPlanarToFloatParams,
		 runConvertPlanarToFloat);
	convertPlanarToFloatThreadRunner.Run();

#endif
	CHRTimer resamplerTimer;
	const int numComponents = 3;

	// Construct floating-point output planes.
	Ipp32fArray P0(roiProcRegion.getSize());
	Ipp32fArray P1(roiProcRegion.getSize());
	Ipp32fArray P2(roiProcRegion.getSize());
	vector<Ipp32fArray>planes32fOut =
	{P0, P1, P2};
	vector<Ipp16uArray>planes16uOut =
	{Ipp16uArray(roiProcRegion.getSize()), Ipp16uArray(roiProcRegion.getSize()),
		Ipp16uArray(roiProcRegion.getSize())};

	// Resample.
	SliceResamplerParams params;
	params.xOffset = xSourceOffset;
	params.yOffset = ySourceOffset;

	for (int comp = 0; comp < numComponents; comp++)
	{
		params.inputPlane = planes32fIn[comp];
		params.outputPlane = planes32fOut[comp];
		params.minValue = float(minValues[comp]);
		params.maxValue = float(maxValues[comp]);
		SynchronousThreadRunner runner(NumberOfSlices, &params, runSliceResampler);
		runner.Run();
	}
	// DBTRACE(resamplerTimer);

	// Convert back to unsigned shorts.
	CHRTimer ConvertDataOutTimer;
#ifdef USE_STD_THREADS

	Ipp16uArray I0, I1, I2;
	std:: thread t0([&]() {planes16uOut[0] = Ipp16uArray(planes32fOut[0]);});
	std:: thread t1([&]() {planes16uOut[1] = Ipp16uArray(planes32fOut[1]);});
	std:: thread t2([&]() {planes16uOut[2] = Ipp16uArray(planes32fOut[2]);});

	t0.joinxxx();
	t1.joinxxx();
	t2.joinxxx();

#else

	ConvertPlanarToIntParams convertPlanarToIntParams;
	convertPlanarToIntParams.in = planes32fOut;
	convertPlanarToIntParams.out = planes16uOut;
	SynchronousThreadRunner convertPlanarToIntThreadRunner(3 * NumberOfSlices, &convertPlanarToIntParams,
		 runConvertPlanarToInt);
	convertPlanarToIntThreadRunner.Run();

#endif

	// DBTRACE(ConvertDataOutTimer);
	CHRTimer CopyFromPlanarTimer;

	// Compute areas within the processing region that need to be filled.
	int emptyRoiRowsAtTop = (ySourceOffset < 0.F) ? ceil(-ySourceOffset) : 0;
	int emptyRoiRowsAtBottom = (ySourceOffset > 0.F) ? ceil(ySourceOffset) : 0;
	int emptyRoiColsAtLeft = (xSourceOffset < 0.F) ? ceil(-xSourceOffset) : 0;
	int emptyRoiColsAtRight = (xSourceOffset > 0.F) ? ceil(xSourceOffset) : 0;

	// Compute the sub-ROI for the resampled data that will be copied to the output.
	IppiRect roiInSubImage;
	roiInSubImage.x = emptyRoiColsAtLeft;
	roiInSubImage.y = emptyRoiRowsAtTop;
	roiInSubImage.width = roiProcRegion.width - (emptyRoiColsAtLeft + emptyRoiColsAtRight);
	roiInSubImage.height = roiProcRegion.height - (emptyRoiRowsAtTop + emptyRoiRowsAtBottom);

	// Create output frame arrays (whole frame, proc region, proc region minus missing data areas).
	Ipp16uArray imageOutput({width, height, 3}, _outputImage);
	Ipp16uArray imageRoiProcRegion = imageOutput(roiProcRegion);
	Ipp16uArray imageRoiSubImage = imageRoiProcRegion(roiInSubImage);

	// Copy the resampled data, combining the three separate planes into the
	// inteleaved output array.
	imageRoiSubImage.copyFromPlanar({planes16uOut[0](roiInSubImage),
			 planes16uOut[1](roiInSubImage), planes16uOut[2](roiInSubImage)});

	// DBTRACE(CopyFromPlanarTimer);
	CHRTimer MissingDataFillTimer;

	// Create the ROIs for each of the four fill rectangles. Note that only one
	// of {top, bottom} can have non-zero height and onl;y one of {left, right}
	// can have non-zero width. Struct order is { x, y, width, height }
	IppiRect fillTopRoi =
	{0, 0, roiProcRegion.width, emptyRoiRowsAtTop};
	IppiRect fillBottomRoi =
	{0, roiInSubImage.height, roiProcRegion.width, emptyRoiRowsAtBottom};
	IppiRect fillLeftRoi =
	{0, roiInSubImage.y, emptyRoiColsAtLeft, roiInSubImage.height};
	IppiRect fillRightRoi =
	{roiInSubImage.width, roiInSubImage.y, emptyRoiColsAtRight, roiInSubImage.height};

	Ipp16uArray imageProcRegionTopFill = imageRoiProcRegion(fillTopRoi);
	Ipp16uArray imageProcRegionBottomFill = imageRoiProcRegion(fillBottomRoi);
	Ipp16uArray imageProcRegionLeftFill = imageRoiProcRegion(fillLeftRoi);
	Ipp16uArray imageProcRegionRightFill = imageRoiProcRegion(fillRightRoi);

	// Fill in the missing data areas.
	switch (_stabilizerParameters.MissingDataFill)
	{
	case MISSING_DATA_WHITE:
		imageProcRegionTopFill.set({whiteFillValues[0], whiteFillValues[1], whiteFillValues[2]});
		imageProcRegionBottomFill.set({whiteFillValues[0], whiteFillValues[1], whiteFillValues[2]});
		imageProcRegionLeftFill.set({whiteFillValues[0], whiteFillValues[1], whiteFillValues[2]});
		imageProcRegionRightFill.set({whiteFillValues[0], whiteFillValues[1], whiteFillValues[2]});
		break;

	case MISSING_DATA_BLACK:
		imageProcRegionTopFill.set({blackFillValues[0], blackFillValues[1], blackFillValues[2]});
		imageProcRegionBottomFill.set({blackFillValues[0], blackFillValues[1], blackFillValues[2]});
		imageProcRegionLeftFill.set({blackFillValues[0], blackFillValues[1], blackFillValues[2]});
		imageProcRegionRightFill.set({blackFillValues[0], blackFillValues[1], blackFillValues[2]});
		break;

	case MISSING_DATA_PREVIOUS:
		{
			Ipp16uArray imagePrevious({width, height, 3}, _previousFixedImage);
			Ipp16uArray imagePreviousProcRegion = imagePrevious(roiProcRegion);

			if (fillTopRoi.height != 0 && fillTopRoi.width != 0)
				imageProcRegionTopFill <<= imagePreviousProcRegion(fillTopRoi);
			if (fillBottomRoi.height != 0 && fillBottomRoi.width != 0)
				imageProcRegionBottomFill <<= imagePreviousProcRegion(fillBottomRoi);
			if (fillLeftRoi.height != 0 && fillLeftRoi.width != 0)
				imageProcRegionLeftFill <<= imagePreviousProcRegion(fillLeftRoi);
			if (fillRightRoi.height != 0 && fillRightRoi.width != 0)
				imageProcRegionRightFill <<= imagePreviousProcRegion(fillRightRoi);
			break;
		}

	case MISSING_DATA_ZOOM:
		// MISSING_DATA_ZOOM "fill" means there will be no empty area once the ROI
		// is zoomed (at higher level) so we do nothing here in that case/
		break;

	default:
		break;
	}

	// DBTRACE(MissingDataFillTimer);
}

///////////////////////////////////////////////////////////////////////////////

void CStabilizerProc::StabilizeFrameIntegralPixel(int frameIndex)
{
	MTI_UINT16 *sourceData = _BaseOrigImage;

	// Amount to displace is given by the difference between the robust points
	// and their smoothed counterparts. The robust arrays always have three
	// points at each frame. Since we're ignoring rotation at this point,
	// only point 0 is needed.
	auto robustTPO = _stabilizerToolObject->RobustArray.getAllTrackPointsAtFrame(frameIndex);
	auto robustTPS = _stabilizerToolObject->SmoothRobustArray.getAllTrackPointsAtFrame(frameIndex);
	int xSourceOffset = robustTPO[0].x - robustTPS[0].x;
	int ySourceOffset = robustTPO[0].y - robustTPS[0].y;

	auto imageFormat = GetSrcImageFormat(0);
	int width = imageFormat->getPixelsPerLine();
	int height = imageFormat->getLinesPerFrame();

	// Fill values
	MTI_UINT16 whiteFillValues[3];
   MTI_UINT16 blackFillValues[3];
	imageFormat->getComponentValueWhiteFill(&whiteFillValues[0]);
	imageFormat->getComponentValueBlackFill(&blackFillValues[0]);

	Ipp16uArray imageIn({width, height, 3}, sourceData);
	IppiRect roiProcRegion;
	roiProcRegion.x = _BoundingBox.left;
	roiProcRegion.y = _BoundingBox.top;
	roiProcRegion.width = _BoundingBox.right - _BoundingBox.left + 1;
	roiProcRegion.height = _BoundingBox.bottom - _BoundingBox.top + 1;
	// find the movement

	IppiRect roiInRegion =
	{0, 0, roiProcRegion.width - abs(xSourceOffset), roiProcRegion.height - abs(ySourceOffset)};
	IppiRect roiOutRegion = roiInRegion;

	if (ySourceOffset >= 0)
	{
		roiOutRegion.y = 0;
		roiInRegion.y = ySourceOffset;
	}
	else
	{
		roiOutRegion.y = -ySourceOffset;
		roiInRegion.y = 0;
	}

	if (xSourceOffset >= 0)
	{
		roiOutRegion.x = 0;
		roiInRegion.x = xSourceOffset;
	}
	else
	{
		roiOutRegion.x = -xSourceOffset;
		roiInRegion.x = 0;
	}

	auto imageRoiProcRegion = imageIn(roiProcRegion);
	auto imageRoiIn = imageRoiProcRegion(roiInRegion);

	// Create output frame arrays (whole frame, proc region, proc region minus missing data areas).
	Ipp16uArray imageOutput({width, height, 3}, _outputImage);
	auto imageRoiProcOutput = imageOutput(roiProcRegion);
	auto imageRoiOut = imageRoiProcOutput(roiOutRegion);
	CHRTimer MoveViaCopyTimer;

   imageRoiOut <<= imageRoiIn;
	//imageRoiOut.convertFrom(imageRoiIn);

	// Compute areas within the processing region that need to be filled.
	int emptyRoiRowsAtTop = (ySourceOffset < 0.F) ? ceil(-ySourceOffset) : 0;
	int emptyRoiRowsAtBottom = (ySourceOffset > 0.F) ? ceil(ySourceOffset) : 0;
	int emptyRoiColsAtLeft = (xSourceOffset < 0.F) ? ceil(-xSourceOffset) : 0;
	int emptyRoiColsAtRight = (xSourceOffset > 0.F) ? ceil(xSourceOffset) : 0;

	// Compute the sub-ROI for the resampled data that will be copied to the output.
	IppiRect roiInSubImage;
	roiInSubImage.x = emptyRoiColsAtLeft;
	roiInSubImage.y = emptyRoiRowsAtTop;
	roiInSubImage.width = roiProcRegion.width - (emptyRoiColsAtLeft + emptyRoiColsAtRight);
	roiInSubImage.height = roiProcRegion.height - (emptyRoiRowsAtTop + emptyRoiRowsAtBottom);

	CHRTimer MissingDataFillTimer;

	// Create the ROIs for each of the four fill rectangles. Note that only one
	// of {top, bottom} can have non-zero height and onl;y one of {left, right}
	// can have non-zero width. Struct order is { x, y, width, height }
	IppiRect fillTopRoi =
	{0, 0, roiProcRegion.width, emptyRoiRowsAtTop};
	IppiRect fillBottomRoi =
	{0, roiInSubImage.height, roiProcRegion.width, emptyRoiRowsAtBottom};
	IppiRect fillLeftRoi =
	{0, roiInSubImage.y, emptyRoiColsAtLeft, roiInSubImage.height};
	IppiRect fillRightRoi =
	{roiInSubImage.width, roiInSubImage.y, emptyRoiColsAtRight, roiInSubImage.height};

	Ipp16uArray imageProcRegionTopFill = imageRoiProcOutput(fillTopRoi);
	Ipp16uArray imageProcRegionBottomFill = imageRoiProcOutput(fillBottomRoi);
	Ipp16uArray imageProcRegionLeftFill = imageRoiProcOutput(fillLeftRoi);
	Ipp16uArray imageProcRegionRightFill = imageRoiProcOutput(fillRightRoi);

	// Fill in the missing data areas.
	switch (_stabilizerParameters.MissingDataFill)
	{
		//////////////////////////////////////////////////////////////////////////
		/// QQQ IppArray.set() crashes with empty ROIs! FIX IT!
		//////////////////////////////////////////////////////////////////////////

	case MISSING_DATA_WHITE:
		imageProcRegionTopFill.set({whiteFillValues[0], whiteFillValues[1], whiteFillValues[2]});
		imageProcRegionBottomFill.set({whiteFillValues[0], whiteFillValues[1], whiteFillValues[2]});
		imageProcRegionLeftFill.set({whiteFillValues[0], whiteFillValues[1], whiteFillValues[2]});
		imageProcRegionRightFill.set({whiteFillValues[0], whiteFillValues[1], whiteFillValues[2]});

		break;

	case MISSING_DATA_BLACK:
		imageProcRegionTopFill.set({blackFillValues[0], blackFillValues[1], blackFillValues[2]});
		imageProcRegionBottomFill.set({blackFillValues[0], blackFillValues[1], blackFillValues[2]});
		imageProcRegionLeftFill.set({blackFillValues[0], blackFillValues[1], blackFillValues[2]});
		imageProcRegionRightFill.set({blackFillValues[0], blackFillValues[1], blackFillValues[2]});
		break;

	case MISSING_DATA_PREVIOUS:
		{
			Ipp16uArray imagePrevious({width, height, 3}, _previousFixedImage);
			Ipp16uArray imagePreviousProcRegion = imagePrevious(roiProcRegion);
			if (fillTopRoi.height != 0 && fillTopRoi.width != 0)
				imageProcRegionTopFill <<= imagePreviousProcRegion(fillTopRoi);
			if (fillBottomRoi.height != 0 && fillBottomRoi.width != 0)
				imageProcRegionBottomFill <<= imagePreviousProcRegion(fillBottomRoi);
			if (fillLeftRoi.height != 0 && fillLeftRoi.width != 0)
				imageProcRegionLeftFill <<= imagePreviousProcRegion(fillLeftRoi);
			if (fillRightRoi.height != 0 && fillRightRoi.width != 0)
				imageProcRegionRightFill <<= imagePreviousProcRegion(fillRightRoi);
			break;
		}

	case MISSING_DATA_ZOOM:
		// MISSING_DATA_ZOOM "fill" means there will be no empty area once the ROI
		// is zoomed (at higher level) so we do nothing here in that case/
		break;

	default:
		break;
	}

	// DBTRACE(MissingDataFillTimer);
}

///////////////////////////////////////////////////////////////////////////////
