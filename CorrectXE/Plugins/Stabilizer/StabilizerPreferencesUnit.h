//---------------------------------------------------------------------------

#ifndef StabilizerPreferencesUnitH
#define StabilizerPreferencesUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TStabilizerPreferencesForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *Panel1;
        TLabel *Label1;
        TRadioButton *MoveAndCopyRadioButton;
        TRadioButton *MoveAndClearRadioButton;
        TRadioButton *PromptMeRadioButton;
        TPanel *HR;
        TButton *OkButton;
        TButton *CancelButton;
private:	// User declarations
public:		// User declarations
        __fastcall TStabilizerPreferencesForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TStabilizerPreferencesForm *StabilizerPreferencesForm;
//---------------------------------------------------------------------------
#endif
