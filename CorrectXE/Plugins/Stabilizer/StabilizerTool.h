#ifndef StabilizerToolH
#define StabilizerToolH

#include "UserInput.h"
#include "ToolObject.h"
#include "IniFile.h"
#include "PresetParameterModel.h"
#include "TrackingBox.h"
#include "TrackingBoxManager.h"
#include "StabilizerParameters.h"
#include "HRTimer.h"
#include "ThreadLocker.h"
#include <fstream>
#include <valarray>
#include "bthread.h"

// Kevin
#include "AutoStabilize.h"

//////////////////////////////////////////////////////////////////////

// Stabilizer Tool Number (16-bit number that uniquely identifies this tool)
// TTT
#define STABILIZER_TOOL_NUMBER    21

#define INVALID_FRAME_INDEX (-1)

#define STABILIZE_TOOL_NAME "Stabilize"

// Stabilizer Tool Command Numbers

enum EStabilizerCmd
{
   STAB_CMD_NOOP,
   STAB_CMD_SET_IN_PT,
   STAB_CMD_SET_OUT_PT,
   STAB_CMD_BEGIN_PAN,
   STAB_CMD_END_PAN,
   STAB_CMD_TOGGLE_FIX,
   STAB_CMD_TOGGLE_TRACK_POINT_VISIBILITY,
   STAB_CMD_TOGGLE_PROC_REGION_VISIBILITY,
   STAB_CMD_TOGGLE_PROC_REGION_OPERATION,
   STAB_CMD_TOGGLE_CLEAR_ON_SHOT_CHANGE,

   STAB_CMD_ADD_SELECT_OR_DRAG_TRACKPOINT,
   STAB_CMD_STOP_DRAGGING_TRACKPOINT,
   STAB_CMD_SELECT_ANOTHER_TRACKPOINT,
   STAB_CMD_SAVE_TRACKPOINTS,
   STAB_CMD_LOAD_TRACKPOINTS,
   STAB_CMD_LOAD_TRACKPOINTS_NOT_MARKS,
   STAB_CMD_CLEAR_TRACKPOINTS,
   STAB_CMD_DELETE_SELECTED_TRACKPOINTS,

   STAB_CMD_TRACK,
   STAB_CMD_PAUSE_TRACKING,

   STAB_CMD_FOCUS_ON_TRACK_BOX_SIZE,
   STAB_CMD_FOCUS_ON_MOTION_SEARCH_AREA,
   STAB_CMD_FOCUS_ON_SMOOTHING,
//   STAB_CMD_FOCUS_ON_ADD_JITTER,
	STAB_CMD_TOGGLE_SUBPIXEL_INTERP,

	STAB_CMD_MISSING_DATA_FILL_NEXT,
   STAB_CMD_MISSING_DATA_FILL_PREV,
   STAB_CMD_STABILIZE_GROUP_NEXT,
   STAB_CMD_MOTION_GROUP_NEXT,
   STAB_CMD_ANCHOR_GROUP_NEXT,
   STAB_CMD_STABILIZE_GROUP_PREV,
   STAB_CMD_MOTION_GROUP_PREV,
   STAB_CMD_ANCHOR_GROUP_PREV,

   STAB_CMD_ADD_EVENT_TO_PDL,
	STAB_CMD_EXECUTE_PDL,

   STAB_CMD_TURN_COLOR_PICKER_ON,
   STAB_CMD_TURN_COLOR_PICKER_OFF,

   STAB_CMD_SET_MARKS_FROM_TRACK_ARRAY,
   STAB_CMD_SHIFT_IS_ON,
	STAB_CMD_SHIFT_IS_OFF,

	STAB_CMD_PRESET_1,
	STAB_CMD_PRESET_2,
	STAB_CMD_PRESET_3,
	STAB_CMD_PRESET_4
};

#define STAB_MAX_THREAD_COUNT  64      // Maximum number of processing threads

// Mark IN move preference options
enum EMarkInMovePreference
{
   MOVEPREF_ALWAYS_COPY=1,
   MOVEPREF_ALWAYS_CLEAR=2,
   MOVEPREF_PROMPT=3
};

enum EStabilizerProcessCmd
{
   STABILIZER_PROC_NOOP,
   STABILIZER_PROC_TRACK_IF_NEEDED,
   STABILIZER_PROC_RETRACK,
   STABILIZER_PROC_SMOOTH,
   STABILIZER_PROC_PREVIEW_1,
   STABILIZER_PROC_PREVIEW_NEXT,
   STABILIZER_PROC_PREVIEW,
   STABILIZER_PROC_RENDER_1,
   STABILIZER_PROC_RENDER_NEXT,
   STABILIZER_PROC_RENDER,
   STABILIZER_PROC_PAUSE,
   STABILIZER_PROC_CONTINUE,
   STABILIZER_PROC_STOP,
//   STABILIZER_PROC_KPASS1,
};

//////////////////////////////////////////////////////////////////////

// Stabilizer Tool Control States
enum EStabilizerControlState
{
   STABILIZER_STATE_IDLE = 0,
   STABILIZER_STATE_GOT_AN_ERROR,
   STABILIZER_STATE_NEED_TO_RUN_TRACKER,
   STABILIZER_STATE_TRACKING,
   STABILIZER_STATE_TRACKING_PAUSED,
   STABILIZER_STATE_TRACKING_COMPLETE,
   STABILIZER_STATE_NEED_TO_RUN_SMOOTHER,
   STABILIZER_STATE_SMOOTHING,
   STABILIZER_STATE_SMOOTHING_COMPLETE,
   STABILIZER_STATE_NEED_TO_RUN_RENDERER,
   STABILIZER_STATE_RENDERING,
   STABILIZER_STATE_RENDERING_COMPLETE,

   // Added for auto stabilize
   // NOTE: There is now only one pass - second analysis pass was eliminated!
	STABILIZER_STATE_NEED_TO_RUN_AUTOTRACK_PASS_1,
	STABILIZER_STATE_AUTOTRACKING_PASS_1,
	STABILIZER_STATE_AUTOTRACKING_PAUSED,
   STABILIZER_STATE_AUTOTRACKING_COMPLETE,
   STABILIZER_STATE_NEED_TO_RUN_AUTOLOAD,

};

// Stupid tracking hack states
enum StupidTrackingHackState
{
	NOT_TRACKING,
	TRACKING,
	TRACKING_PAUSED
};

//////////////////////////////////////////////////////////////////////

typedef MTI_UINT16 Pixel;
typedef Pixel * Image;


//////////////////////////////////////////////////////////////////////
// Forward Declarations
//
class TrackingBoxManager;
class CStabilizerToolParameters;
class CColorPickTool;

//////////////////////////////////////////////////////////////////////

class CStabilizerTool : public CToolObject
{
public:

   CStabilizerTool(const string &newToolName, MTI_UINT32 **newFeatureTable);
   virtual ~CStabilizerTool();

   // Tool startup and shutdowm
   virtual int toolInitialize(CToolSystemInterface *newSystemAPI);
   virtual int toolShutdown();
   virtual int toolActivate();
   virtual int toolDeactivate();
   virtual bool DoesToolPreserveHistory();
   virtual CToolProcessor* makeToolProcessorInstance(const bool *);
   virtual int StartToolProcessing(EToolProcessingCommand processingType,
                                   bool newHighlightFlag);
   virtual EToolControlState GetToolProcessingControlState();

   // Tool GUI
   virtual int toolHide();
   virtual int toolShow();
   virtual bool IsShowing(void);

   // Tool Event Handlers
   virtual int onToolCommand(CToolCommand &toolCommand);
   virtual int onMouseMove(CUserInput &userInput);
   virtual int onUserInput(CUserInput &userInput);
   virtual int onNewMarks();
   virtual int onChangingClip();
   virtual int onNewClip();
   virtual int onChangingFraming();
   virtual int onSwitchSubtool();
   virtual int onRedraw(int frameIndex);
   virtual int onHeartbeat();
	virtual int onToolProcessingStatus(const CAutotoolStatus& autotoolStatus);

   // Tracking tool callbacks
   virtual bool NotifyTrackingStarting();
   virtual void NotifyTrackingDone(int errorCode);
   virtual void NotifyTrackingCleared();

   // PDL Interface
   int onCapturePDLEntry(CPDLElement &pdlEntryToolParams);
   int onGoToPDLEntry(CPDLEntry &pdlEntry);

   // Called from the GUI
   void ButtonCommandHandler(EStabilizerProcessCmd command);
	void HandleTrackCommand();
	void HandleRetrackCommand();
	void HandleSmoothCommand();
   void HandleProcessingCommand(EToolProcessingCommand toolProcessingCommand);
	void HandlePauseCommand();
	void HandleContinueCommand();
	void HandleStopCommand();

   // NOTE: There is now only one pass - second analysis pass was eliminated!
	void HandleKPass1Command();

   void ResetResumeFrameIndex(int resumeFrame);
	bool BoundingBoxDefined();
	void ResetTracking();
	int  GetNumberOfTrackingBoxes() const;
	void SaveTrackingPointsToFile();
   void AutoSaveTrackingPoints();
   void LoadTrackingPointsFromFile(bool restoreMarks=true, bool restoreProcessingRegion=true);
   bool SaveTrackingPoints(const string &sName);
   bool LoadTrackingPoints(const string &sName, bool restoreMarks=true, bool restoreProcessingRegion=true);
	bool LoadTrackingPoints(istream &inStream, bool restoreMarks=true, bool restoreProcessingRegion=true);
   void ClearTrackingPoints();
   void SelectTrackingPointRelative(int plusOrMinus);
   void MarkSmoothedDataInvalid();
   bool IsSmoothedDataValid() const;
   TrackingBoxSet &GetTrackingArrayRef();
   TrackingBoxSet &GetRobustArrayRef();
   TrackingBoxSet &GetSmoothedArrayRef();
   void SetTrackingBoxSizes(
         int newTrackBoxWidth,      int newTrackBoxHeight,
         int newSearchLeftDistance, int newSearchRightDistance,
         int newSearchUpDisatnce,   int newSearchDownDistance);
   int GetLastSelectedTag();
	bool CheckIfNeedToRetrack() const;
   void SetClearTrackingStuffOnNewShot(bool flag);
	void SetAdvancedTrackingParameters(
         ETrackingChannel trackingChannel,
         bool degrainBeforeTracking,
         bool applyLutBeforeTracking,
         bool applyAutoContrastBeforeTracking);

   // These needed to call back on changes
   DEFINE_CBHOOK(TrackingPointsChanged, CStabilizerTool);
   DEFINE_CBHOOK(SetNeedToAutoSave, CStabilizerTool);
   CBHook TrackingPointStateChange;
   CBHook ClipChanging;
   CBHook ClipChange;
   CBHook MarksChange;

   // Status information
   void getStatusMessage(string &sMessage);
   void setStatusMessage(const string &NewMessage);
   void getTimeMessage(string &sMessage);
   void setTimeMessage(const string &NewMessage);

   int  readStabilizerParameters();
   void writeStabilizerParameters();

   EStabilizerControlState GetControlState(void);
	void SetControlState(EStabilizerControlState newControlState);
	StupidTrackingHackState GetStupidTrackingHackState();
	int GetPreviewFrameIndex(void) { return _processSingleFrameIndex; }

   TrackingBoxSet TrackingArray;
   TrackingBoxSet RobustArray;
   TrackingBoxSet SmoothRobustArray;

   std::valarray<double> *_Theta_hat;  // estimated rotation
   std::valarray<double> *_Alpha_hat;  // estimated x displacement
   std::valarray<double> *_Beta_hat;   // estimated y displacement
   std::valarray<double> *_Theta_smooth;  // smooth rotation
   std::valarray<double> *_Alpha_smooth;  // smooth x displacement
   std::valarray<double> *_Beta_smooth;   // smooth y displacement

   int TrackedTrackBoxExtentX;
   int TrackedTrackBoxExtentY;
   int TrackedSearchBoxExtentLeft;
   int TrackedSearchBoxExtentRight;
   int TrackedSearchBoxExtentUp;
   int TrackedSearchBoxExtentDown;
   int CurrentTrackBoxExtentX;
   int CurrentTrackBoxExtentY;
   int CurrentSearchBoxExtentLeft;
   int CurrentSearchBoxExtentRight;
   int CurrentSearchBoxExtentUp;
   int CurrentSearchBoxExtentDown;

   int CurrentTrackingChannel = -1;
   bool CurrentDegrainBeforeTracking = false;
   bool CurrentApplyLutBeforeTracking = false;
   bool CurrentApplyAutoContrastBeforeTracking = false;

   void DeleteSelectedTrackingPoints();
   TrackingBoxManager *GetTrackingPointsEditor(void);
   bool ComputeRobustTrack(int nextFrame, int refFrame);
   bool ComputeRobustTrackNoRotation(int nextFrame, int refFrame);

   int DoSmoothing(void);
   static void DoSmoothing_trampoline(void *appData, void *threadid);
   void DoSmoothing_thread(void);

   bool ComputeRobustPoints( int refFrameIndex,
                             EStabilizationMode ModeX,
                             EStabilizationMode ModeY,
                             EStabilizationMode ModeR);
   bool SmoothRobustPoints(  int iGoodStart, int iGoodEnd,
                             int refFrameIndex, double Alpha2,
                             EStabilizationMode ModeX,
                             EStabilizationMode ModeY,
                             EStabilizationMode ModeR,
                             int *percentDone=NULL, bool *abortFlag=NULL);
   static void StabilizerSmoothTrampoline( void *vpAppData, void *vpReserved);
   bool StabilizerSmooth3(EStabilizationMode Mode,
                             std::valarray<double> &TP,
                             int iGoodStart, int iGoodEnd,
                             int refFrameIndex, double Alpha,
                             int *percentDone=NULL, bool *abortFlag=NULL);

   void SetTrackingPoints(const TrackingBoxSet &TA);

   void NeutralizeTrackingPointAtOffset(int offset);

   DRECT GetZoomBox(RECT &);
   RECT &getBoundingBox(void) { return _BoundingBox; }
   void setBoundingBox(const RECT &newBox);
   bool FindBoundingBox(int R=25, int G=25, int B=25, int X=0, int Y=0, int slack=50);
   void EnterColorPickMode(void);
   void ExitColorPickMode(void);
   bool IsInColorPickMode(void);

   bool getDisplayRect(void) { return _displayRect; }
   void setDisplayRect(const bool newDisp);

   bool getDisplayTrackingPoints(void);
   void setDisplayTrackingPoints(const bool newDisp);

   void setWarningDialogs(bool value) {_WarningDialogs = value; }
   bool getWarningDialogs(void) {return _WarningDialogs; }

   EMarkInMovePreference getMarkInMovePreference(void)
       { return _MarkInMovePreference; };
   void setMarkInMovePreference(EMarkInMovePreference value)
       { _MarkInMovePreference = value; };
   
   void InvalidateAllTrackingData(void);
   bool isAllTrackDataValid(void) const;

	void NotifyFrameRendered(int frame);
	void NotifyRenderedCompleteRange();

   bool isShiftOn();

   string GetSuggestedTrackingDataLoadFilename();
   string GetSuggestedTrackingDataSaveFilename();

	PresetParameterModel<CStabilizerParameters> *GetPresets() { return _userPresets; };

	CThreadLock _smoothingLock;
	void doDeferredTrackingPointsGuiUpdate() { needToUpdateTrackingPointsGui = true; };

	bool isInAutoTrack() const { return _isInAutoTrack; }
	void setIsInAutoTrack(bool value) {_isInAutoTrack = value; }

	// Kludge for Kevin's autotrack
	CAutoStabilize *autoStabilizeGlobal = nullptr;
	string getAutoTrackingString() const { return _autoTrackingString; }
	void setAutoTrackingString(const string &value) { _autoTrackingString = value; }

	bool wasTrackingDataInvalidatedByRendering();

 	bool getAutoTrackHackPassCompletedFlag() const { return _autoTrackHackPassCompletedFlag; }
	void setAutoTrackHackPassCompletedFlag(bool value) { _autoTrackHackPassCompletedFlag = value; }

protected:  // Override ToolObj
   int ProcessTrainingFrame(void);                   // Preview 1
   int ProcessSingleFrame(EToolSetupType setupType); // Render 1
   int RunFramePreprocessing();                      // Tracking
   int RunFrameProcessing(int newResumeFrame);       // Preview / Render multi
   void DestroyAllToolSetups(bool notIfPaused);
   int MonitorFrameProcessing(EToolProcessingCommand newToolCommand,
							  const CAutotoolStatus& newToolStatus,
							  int newResumeFrame);
   void UpdateExecutionGUI(EToolControlState toolProcessingControlState,
						   bool processingRenderFlag);
   void UpdateTrackingPointGUI(void);

   // GUI Related methods
	int ValidateMarks(void);
	int RunFrameProcessingAutoTrack(int newResumeFrame);

private:
	string _autoTrackingString;
	bool _isInAutoTrack = true;
   bool _autoTrackHackPassCompletedFlag = false;
   bool _isGpuUsed = false;
   bool _isDisableSubPixelInterpolation = false;

   RECT _BoundingBox;
   bool _BoundingBoxDefined;
   CThreadLock _MessageLocker;
   string _StatusMessage;
   CThreadLock _TimeMessageLocker;
   string _TimeMessage;
   CStabilizerParameters _stabilizerParameters;
   string _CurrentClipName;
   EMarkInMovePreference _MarkInMovePreference;
   bool smoothDataIsValid;
	bool shiftIsOn;     // for Larry's Shift+(click the LOAD button) HACK

	PresetParameterModel<CStabilizerParameters> *_userPresets;

	bool yetAnotherStupidHackFlag;
	double trackingPauseHackTimePerFrame;

   int GatherParameters(CStabilizerToolParameters &toolParams);

   int _resumeFrameIndex;           // to resume after a pause
   int _processSingleFrameIndex;
   bool _displayRect;
   bool _deferredClipChangeFlag;
   bool _deferredMarkChangeFlag;
   bool _clearTrackingStuffOnNewShot;

   EStabilizerControlState _stabilizerControlState;
   int stabilizerToolSetupHandle;  // Init to -1
   int trackingToolSetupHandle;    // Init to -1
   int trainingToolSetupHandle;    // Init to -1
   int singleFrameToolSetupHandle; // Init to -1
   int _autoTrackPass1ToolSetupHandle = -1;

   string IniFileName;

   CCountingWait SmoothCountingWait;

   CColorPickTool *ColorPickTool;

   // TEMPORARY - remember the state of the Mask Tool, enabled or disabled.
   bool previousMaskToolEnabled;
   // This are just constant strings that are initialized via statis.
   //static string title;

   bool canMarksBeMoved();
   bool canInMarkBeMoved();
   bool canOutMarkBeMoved();
   TrackingBoxManager *TrkPtsEditor;
   bool _WarningDialogs;
   //bool _smoothedTrackingIsValid;
	EStabilizerControlState _nextStateAfterSmoothing = STABILIZER_STATE_IDLE;
	EToolProcessingCommand  _deferredToolProcessingCommand;
	bool needToSendTrackingPointChangeNotification;
	bool needToUpdateTrackingPointsGui;
	bool needToAutoSave;

	void StartTracking();
	void StopTracking(bool reallyPausing);
	void StartSmoothing();
	void MonitorSmoothing();
	void StopSmoothing();
	void StartRendering();
	void FinishRendering();

   string TrackingDataAutoSaveCurrentFilePath;

   CHRTimer TrackingTimer;
   int HowLongItTookToTrackInSecs;
   bool ShowedInMarkMoveMessage;
   bool ShowedOutMarkMoveMessage;
   bool TrackingDataHasBeenUsedToRenderAll;
   bool SingleFrameFlag;

   bool isFrameBetweenMarks(int iMarkIn, int iMarkOut,
                            int frameToCheckBetweenMarks=INVALID_FRAME_INDEX);
   void AddSyntheticPoints(void);

   void readBoundingBoxFromDesktopIni(ClipSharedPtr &clip);
	void writeBoundingBoxToDesktopIni(ClipSharedPtr &clip);
   void setBoundingBoxInternal(const RECT &newBox);

	void setNextStateAfterSmoothing(EStabilizerControlState newState);
	EStabilizerControlState getNextStateAfterSmoothing();

	bool areThereAnySceneBreaks();

   DEFINE_CBHOOK(ColorPickerColorChanged, CStabilizerTool);
   DEFINE_CBHOOK(ColorPickerColorPicked, CStabilizerTool);

    // Ugly, just tell us what we are doing
#define PROC_TYPE_INVALID -1
#define PROC_TYPE_TRACK 0
#define PROC_TYPE_RENDER_PROCESS 1
#define PROC_TYPE_KPASS_1 2
#define PROC_TYPE_KPASS_2 3

	int _toolProcessorTypeToMake = PROC_TYPE_INVALID;
};

////////////////////////////////////////////////////////////////////////


class CStabilizerToolParameters : public CToolParameters
{
public:
   CStabilizerTool *stabilizerToolObject;
   CStabilizerToolParameters() : CToolParameters(1, 1) {};
   virtual ~CStabilizerToolParameters() { };

public:
   CStabilizerParameters stabilizerParameters;
};


extern CStabilizerTool *GStabilizerTool;    // Global from StabilizerTool.cpp

//////////////////////////////////////////////////////////////////////

#endif // !defined(STABILIZERTOOL_H)

