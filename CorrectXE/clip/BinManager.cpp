// BinManager.cpp: implementation of the CBinManager class.
//
//
//////////////////////////////////////////////////////////////////////

#define _CBIN_MANAGER_DATA_CPP
#define _CCLIP_SCHEME_DATA_CPP
#define _CCLIP_SCHEME_MANAGER_DATA_CPP

#include "BinManager.h"

#include "BinDir.h"
#include "Clip3.h"
#include "err_clip.h"
#include "FileSweeper.h"
#include "MediaFileIO.h"
#include "ImageFileMediaAccess.h"
#include "ImageFormat3.h"
#include "MediaAllocator.h"
#include "MTIsleep.h"
#include "MTIstringstream.h"
#include "TheError.h"
#include "ThreadLocker.h"
#include "timecode.h"

#include <time.h>
#include <errno.h>
#include <algorithm>
#include <memory>

#if defined(_WIN32)
#undef _WIN32_WINNT
#define _WIN32_WINNT 0x0400 // must be defined for proper use of direct.h
// The following pragma is for a wierd VC compiler bug. If this file has
// compile errors
#ifdef _MSC_VER
#include <direct.h>
#endif
#ifdef __BORLANDC__
#include <dir.h>
#endif
#else
// Unix
#include <sys/stat.h>
#endif

CBHook CBinManager::FrameWasDiscardedOrCommittedEvent;
CBHook CBinManager::ClipWasDeletedEvent;

/////////////////////////////////////////////////////////////////////
// Mutex for accessing version.ini so we can update the modified
// count from processing threads
static CSpinLock VersionIniAccessLock;

#ifdef PRIVATE_STATIC
//////////////////////////////////////////////////////////////////////
// The following variables used to be static class members.
// I switched the to namespaced globals for compatability with
// visual c++.  -Aaron Geman  7/9/04
//////////////////////////////////////////////////////////////////////

namespace CBinManagerData
{
   CBinManager::CBinMgrClipList openClipList;   // List of open clips

   // Strings for Bin Ini file
   string binIniFileName = "MTIBIN.ini";
   string binInfoSectionName = "BinInfo";
   string autoClipNameTemplateKey = "AutoClipNameTemplate";
   string autoClipNameIndexKey = "AutoClipNameIndex";
   string autoClipSchemeKey = "AutoClipScheme";
   string autoDiskSchemeKey = "AutoDiskScheme";
   string dailiesAudioDurationKey = "DailiesAudioDuration";
   string dailiesImageDurationKey = "DailiesImageDuration";
   string desktopIniFileName = "desktop.ini";

   string dailiesInfoSectionName = "DailiesInfo";
   string companyKey = "Company";
   string addressKey = "Address";
   string cityStatePostalKey = "CityStatePostal";
   string countryKey = "Country";
   string jobNoKey = "JobNo";
   string prodCoKey = "ProdCo";
   string projectKey = "Project";
   string titleKey = "Title";
   string prodNoKey = "ProdNo";
   string shootDateKey = "ShootDate";
   string transferDateKey = "TransferDate";
   string fieldAKey = "FieldA";
   string fieldBKey = "FieldB";
   string fieldCKey = "FieldC";
   string workOrderKey = "WorkOrder";
   string coloristKey = "Colorist";
   string assistKey = "Assist";


   // Strings for Versions (parent) & Version (child) Ini files
   string versionsIniFileName = "versions.ini";
   string versionInfoSectionName = "VersionInfo";
   string versionCountKey = "VersionCount";
   string versionStatusSectionName = "VersionStatus";

   string versionIniFileName = "version.ini";
   string clipInfoSectionName = "ClipInfo";
   string commentsKey = "Comments";
   string modifiedFrameCountKey = "ModifiedFrameCount";
   string dirtyMediaFolderKey = "DirtyMediaFolder";
   string dirtyHistoryFolderKey = "DirtyHistoryFolder";
   string statusSectionName = "Status";
   string defunctKey = "Defunct";
};
using namespace CBinManagerData;

namespace CClipSchemeData {
   string clipSchemeInfoSection = "ClipSchemeInfo";
   string clipSchemeNameKey = "ClipSchemeName";
   string clipSchemeTypeKey = "ClipSchemeType";
   string clipSchemeMainVideoSection = "MainVideo";
   string clipSchemeMediaTypeKey = "MediaType";
};
using namespace CClipSchemeData;

namespace CClipSchemeManagerData {
   bool clipSchemeListAvailable = false;
   CClipSchemeManager::ClipSchemeList clipSchemeList;
   StringList newClipSchemeFileList;

   string clipSchemeSectionName = "ClipScheme";
   string clipSchemeDirectoryKey = "ClipSchemeDirectory";
   string clipSchemeConfigFileKey = "ClipSchemeConfigFile";
   string activeClipSchemeSectionName = "ActiveClipSchemes";
   string clipSchemeFileKey = "ClipSchemeFile";
};
using namespace CClipSchemeManagerData;
#endif

namespace {

bool checkFileExists(const string& fileName)
{
   // Determine if the named file exists, don't care if it is a regular
   // file, a directory or whatever.

   // Get file status information
   struct stat buf;
   int result = stat(fileName.c_str(), &buf);

   if (result == 0)
      return true; // File exists

   return false;  // Presumably file does not exist
}

};

/*-----------GetSystemErrorMessage------------------------JAM--Mar 2000----*/

static  string GetSystemMessage(int ErrorCode)

/* This function returns a string that contains a human readable     */
/* version of the error message in ErrorCode                         */
/* Ususal usage is ErrorMessage = GetSystemMessage(GetLastError());  */
/*                                                                   */
/*********************************************************************/
{
  char cErrorMessage[255];

  string Result = "";

  // Do nothing if not an error
  if (ErrorCode == 0)
    {
       return(Result);
    }

  FormatMessage
          (
             FORMAT_MESSAGE_FROM_SYSTEM,
             nullptr,
             ErrorCode,
             MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
             (LPTSTR) &cErrorMessage,
             255,
             nullptr
          );

  Result = cErrorMessage;
  return(Result);
};

//////////////////////////////////////////////////////////////////////
// Local Type and Class Definitions
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Static Data Members Initialization
//
// These data members are constructed when the program is first started
// (prior to call to program's main function) and destroyed when the
// program is closing
//
//////////////////////////////////////////////////////////////////////

// openClipList is a list of clips that are currently open and managed by
// the BinManager.
// Note: When openClipList is destroyed as the program closes, it destroys
//       any clips that are still open
CBinManager::CBinMgrClipList CBinManager::openClipList;

// Strings for Bin Ini file
string CBinManager::binIniFileName = "MTIBIN.ini";
string CBinManager::binInfoSectionName = "BinInfo";
string CBinManager::autoClipNameTemplateKey = "AutoClipNameTemplate";
string CBinManager::autoClipNameIndexKey = "AutoClipNameIndex";
string CBinManager::autoClipSchemeKey = "AutoClipScheme";
string CBinManager::autoDiskSchemeKey = "AutoDiskScheme";
string CBinManager::dailiesAudioDurationKey = "DailiesAudioDuration";
string CBinManager::dailiesImageDurationKey = "DailiesImageDuration";
string CBinManager::desktopIniFileName = "desktop.ini";

// Strings for Versions (parent) & Version (child) Ini files
string CBinManager::versionsIniFileName = "versions.ini";
string CBinManager::versionInfoSectionName = "VersionInfo";
string CBinManager::versionCountKey = "VersionCount";
string CBinManager::versionStatusSectionName = "VersionStatus";

string CBinManager::versionIniFileName = "version.ini";
string CBinManager::clipInfoSectionName = "ClipInfo";
string CBinManager::commentsKey = "Comments";
string CBinManager::modifiedFrameCountKey = "ModifiedFrameCount";
string CBinManager::dirtyMediaFolderKey = "DirtyMediaFolder";
string CBinManager::dirtyHistoryFolderKey = "DirtyHistoryFolder";
string CBinManager::statusSectionName = "Status";
string CBinManager::defunctKey = "Defunct";


// Display LUT stuff goes into Bin Ini file... maybe there should be a
// desktop.ini for the Bin for compatibility with clip display LUTs?
// These keys exactly mirror the clip ones in NavMainComm.cpp
string CBinManager::DisplayLutSectionName = "DisplayLUT";
#ifdef MOVED_TO_DISPLAY_LUT_CLASS
string CBinManager::customLUTKey = "UseCustomLUT";
string CBinManager::customLUTColorSpaceIndexKey = "CustomLUTColorSpaceIndex";
string CBinManager::customLUTSourceRangeKey = "UseCustomLUTSourceRange";
string CBinManager::customLUTRYBlkKey = "CustomLUTRYBlk";
string CBinManager::customLUTRYWhiKey = "CustomLUTRYWhi";
string CBinManager::customLUTGUBlkKey = "CustomLUTGUBlk";
string CBinManager::customLUTGUWhiKey = "CustomLUTGUWhi";
string CBinManager::customLUTBVBlkKey = "CustomLUTBVBlk";
string CBinManager::customLUTBVWhiKey = "CustomLUTBVWhi";
string CBinManager::customLUTRMinKey = "CustomLUTRMin";
string CBinManager::customLUTRMaxKey = "CustomLUTRMax";
string CBinManager::customLUTGMinKey = "CustomLUTGMin";
string CBinManager::customLUTGMaxKey = "CustomLUTGMax";
string CBinManager::customLUTBMinKey = "CustomLUTBMin";
string CBinManager::customLUTBMaxKey = "CustomLUTBMax";
string CBinManager::gammaValueKey = "Gamma";
#endif

// mlm... have to add setters for these items
string CBinManager::dailiesInfoSectionName = "DailiesInfo";
string CBinManager::companyKey = "Company";
string CBinManager::addressKey = "Address";
string CBinManager::cityStatePostalKey = "CityStatePostal";
string CBinManager::countryKey = "Country";
string CBinManager::jobNoKey = "JobNo";
string CBinManager::prodCoKey = "ProdCo";
string CBinManager::projectKey = "Project";
string CBinManager::titleKey = "Title";
string CBinManager::prodNoKey = "ProdNo";
string CBinManager::shootDateKey = "ShootDate";
string CBinManager::transferDateKey = "TransferDate";
string CBinManager::fieldAKey= "FieldA";
string CBinManager::fieldBKey = "FieldB";
string CBinManager::fieldCKey = "FieldC";
string CBinManager::workOrderKey = "WorkOrder";
string CBinManager::coloristKey = "Colorist";
string CBinManager::assistKey = "Assist";

//#ifdef NOTYET
bool CClipSchemeManager::clipSchemeListAvailable = false;
CClipSchemeManager::ClipSchemeList CClipSchemeManager::clipSchemeList;
StringList CClipSchemeManager::newClipSchemeFileList;

string CClipSchemeManager::clipSchemeSectionName = "ClipScheme";
string CClipSchemeManager::clipSchemeDirectoryKey = "ClipSchemeDirectory";
string CClipSchemeManager::clipSchemeConfigFileKey = "ClipSchemeConfigFile";
string CClipSchemeManager::activeClipSchemeSectionName = "ActiveClipSchemes";
string CClipSchemeManager::clipSchemeFileKey = "ClipSchemeFile";

string CClipScheme::clipSchemeInfoSection = "ClipSchemeInfo";
string CClipScheme::clipSchemeNameKey = "ClipSchemeName";
string CClipScheme::clipSchemeTypeKey = "ClipSchemeType";
string CClipScheme::clipSchemeMainVideoSection = "MainVideo";
string CClipScheme::clipSchemeMediaTypeKey = "MediaType";
string CClipScheme::clipSchemeVideoClassKey = "VideoFormatClass";
string CClipScheme::clipSchemeAudioClassKey = "AudioFormatClass";
string CClipScheme::clipSchemeStorageClassKey = "MediaStorageClass";
//#endif // #ifdef NOTYET

//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//
// CBinManager Class Implementation
//
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBinManager::CBinManager()
{
   IndexOfFrameThatWasDiscardedOrCommitted = -1;
   FrameWasDiscarded = false;
   lastDeletedClipName = "something stupid";
}

CBinManager::~CBinManager()
{
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Mutators
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Path and file name helper functions
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    makeClipPath
//
// Description: Creates a standardized Clip File directory path by combining
//              the caller's binPath and clipName.
//
//              The returned string will be of the form
//                 binPath/clipName
//              with the correct platform-dependent dir separators.
//              Note that the returned path does NOT end with a dir separator.
//
// Arguments:   string& binPath     Reference to input Bin Path string
//              string& clipPath    Reference to input Clip Name string
//
// Returns:    binPath/clipName
//
//------------------------------------------------------------------------
string CBinManager::makeClipPath(const string& binPath,
                                 const string& clipName) const
{
   return AddDirSeparator(binPath) + clipName;
}

//------------------------------------------------------------------------
//
// Function:    makeClipPath
//
// Description: Creates a standardized Clip File directory path given a variety
//              of input formats.  The input Clip File Name can be in one
//              of the forms:
//                   binPath/clipName
//                   binPath/clipName/clipName
//                   binPath/clipName/clipName.clp
//
//              If the function succeeds, the returned string will be of
//              the form binPath/clipName with the correct platform-dependent
//              dir separators.  Note that the returned path does NOT end
//              with a dir separator.  If the functions fails,
//              an empty string is returned.  The function will fail if
//              CBinManager::splitClipFileName cannot parse the input string.
//
// Arguments:   string& clipFileName    Input Clip File Name
//
// Returns:     binPath/clipName if success
//              empty string if function failed
//
//------------------------------------------------------------------------
string CBinManager::makeClipPath(const string& clipFileName) const
{
   string binPath, clipName;

   int retVal = splitClipFileName(clipFileName, binPath, clipName);
   if (retVal != 0)
      {
      TRACE_0(errout << "ERROR: CBinManager::makeClipPath could not parse clip file name "
                     << endl
                     << "       " << clipFileName << endl
                     << "       CBinManager::splitClipFileName returned " << retVal);
      string emptyString;
      return emptyString;
      }

   return makeClipPath(binPath, clipName);
}

//------------------------------------------------------------------------
//
// Function:    makeClipFileName
//
// Description: Creates a standardized Clip File Name by combining
//              the caller's binPath and clipName.
//
//              The returned string will be of the form
//                 binPath/clipName/clipName
//              with the correct platform-dependent dir separators.
//
// Arguments:   string& binPath     Reference to input Bin Path string
//              string& clipPath    Reference to input Clip Name string
//
// Returns:     binPath/clipName/clipName
//
//------------------------------------------------------------------------
string CBinManager::makeClipFileName(const string& binPath,
                                     const string& clipName)
{
   return AddDirSeparator(makeClipPath(binPath, clipName)) + clipName;
}

//------------------------------------------------------------------------
//
// Function:    makeClipDesktopIniFileName
//
// Description: makes a full desktop ini file name from a
//              clip path name.
//              CAREFUL: This is also used to make paths for BIN desktop.ini
//              CAREFUL: Version clip desktop.ini goes in the PARENT folder
//
// Arguments:   string& clipPathName     A valid clip path name
//
// Returns:     full path and desktop name
//              No ini file is opened.
//
//------------------------------------------------------------------------
string CBinManager::makeClipDesktopIniFileName(const string& clipPathName)
{
   //
   string desktopIniFilePath = clipPathName;
   string pathPart, fileNamePart;
   splitClipFileName(clipPathName, pathPart, fileNamePart);

   // Version clips desktop.ini is in parent, shared with parent & siblings
   if (isVersionClipName(fileNamePart))
   {
      desktopIniFilePath = pathPart;
   }
   // Put the "desktop" file into the clip's directory
   return AddDirSeparator(desktopIniFilePath) + desktopIniFileName;
}

//------------------------------------------------------------------------
//
// Function:    openBinIniFile
//
// Description: opens or creates the MTIBin.ini file associated
//              with the bin
//              Caller must close inifile with DeleteIniFile.
//
// Arguments:   string& binPathName     A valid bin path name
//
// Returns:     ini file pointer, nullptr on failure
//
//------------------------------------------------------------------------
CIniFile *CBinManager::openBinIniFile(const string& binPath)
{
   string iniFileName = AddDirSeparator(binPath) + binIniFileName;

   return CreateIniFile(iniFileName);
}

//------------------------------------------------------------------------
//
// Function:    getDesktopSectionUniqueName
//
// Description: Gets a string unique to the application and user to be used
//              as the section name in desktop.ini files
//
// Arguments:   None
//
// Returns:     section name string
//
//------------------------------------------------------------------------
string CBinManager::getDesktopSectionUniqueName()
{
   // Put the "desktop" file into the clip's directory
   // CAREFUL! getenv() IS NOT THREADSAFE!

   static string sectionUniqueName;      // cache

   if (sectionUniqueName.empty())
   {
      string appName = GetApplicationName();
      char *userName = getenv("USERNAME");
      string userNameAsString = "common";
      if (userName == nullptr)
         userNameAsString = userName;

      // Create section name that will look like Appname/Username such as
      // "Navigator/bozo"... GetFileName gets the last component of the path,
      // stripping the extension
      sectionUniqueName = GetFileName(appName) + '/' + userNameAsString;
   }
   return sectionUniqueName;
}

//------------------------------------------------------------------------
//
// Function:    openClipDesktop
//
// Description: opens or creates the desktop.ini file associated
//              with the clip;
//              caller must close inifile with DeleteIniFile.
//
// Arguments:   ClipSharedPtr &clip         An opened valid clip pointer
//
// Returns:     ini file pointer, nullptr on failure
//
//------------------------------------------------------------------------
CIniFile *CBinManager::openClipDesktop(ClipSharedPtr &clip)
{
   string clipPath = RemoveDirSeparator(clip->getClipPathName());
   string iniFilePath = makeClipDesktopIniFileName(clipPath);

   return CreateIniFile(iniFilePath);
}

//------------------------------------------------------------------------
//
// Function:    openBinDesktop
//
// Description: opens or creates the desktop.ini file associated
//              with the bin of the clip;
//              caller must close inifile with DeleteIniFile.
//
// Arguments:   ClipSharedPtr &clip         An opened valid clip pointer
//
// Returns:     ini file pointer, nullptr on failure
//
//------------------------------------------------------------------------
CIniFile *CBinManager::openBinDesktop(ClipSharedPtr &clip)
{
   string binPath = RemoveDirSeparator(clip->getBinPath());

   if (isVersionClipName(clip->getClipName()))
   {
      string pathPart, fileNamePart;
      splitClipFileName(binPath, pathPart, fileNamePart);
      binPath = RemoveDirSeparator(pathPart);
   }
   string iniFilePath = makeClipDesktopIniFileName(binPath);
   return CreateIniFile(iniFilePath);
}

//------------------------------------------------------------------------
//
// Function:    openRootDesktop
//
// Description: opens or creates the "shared" desktop.ini file, which is
//              stashed in the bin root directory and which is the first
//              place we look for display parameters when opening a clip;
//              caller must close inifile with DeleteIniFile.
//
// Arguments:   ClipSharedPtr &clip         An opened valid clip pointer
//
// Returns:     ini file pointer, nullptr on failure
//
//------------------------------------------------------------------------
CIniFile *CBinManager::openRootDesktop()
{
   // Find the root bin directory
   StringList roots;
   CPMPGetBinRoots(&roots);
   string rootPath = RemoveDirSeparator(roots[0]);    // there is only one

   string iniFilePath = makeClipDesktopIniFileName(rootPath);
   return CreateIniFile(iniFilePath);
}

//------------------------------------------------------------------------
//
// Function:    doesDesktopIniExist
//
// Description: Given a path,    Check if desktop.ini file exist in that dir
//
// Arguments:   string& dirPath  Full path to dir to check
//
// Returns:     true if there is a desktop.ini file in that directory
//
//------------------------------------------------------------------------
bool CBinManager::doesDesktopIniExist(const string &dirPath)
{
    return checkFileExists(makeClipDesktopIniFileName(dirPath));
}

//------------------------------------------------------------------------
//
// Function:    makeClipFileName
//
// Description: Creates a standardized Clip File Name given a variety of
//              input formats.  The input Clip File Name can be in one
//              of the forms:
//                   binPath/clipName
//                   binPath/clipName/clipName
//                   binPath/clipName/clipName.clp
//
//              If the function succeeds, the returned string will be of
//              the form binPath/clipName/clipName with the correct
//              platform-dependent dir separators.  If the functions fails,
//              an empty string is returned.  The function will fail if
//              CBinManager::splitClipFileName cannot parse the input string.
//
// Arguments:   string& clipFileName    Input Clip File Name
//
// Returns:     binPath/clipName/clipName if success
//              empty string if function failed
//
//------------------------------------------------------------------------
string CBinManager::makeClipFileName(const string& clipFileName)
{
   string binPath, clipName;

   int retVal = splitClipFileName(clipFileName, binPath, clipName);
   if (retVal != 0)
      {
      TRACE_0(errout << "ERROR: CBinManager::makeClipFileName could not parse clip file name "
                     << endl
                     << "       " << clipFileName << endl
                     << "       CBinManager::splitClipFileName returned " << retVal);
      string emptyString;
      return emptyString;
      }

   return makeClipFileName(binPath, clipName);
}

//------------------------------------------------------------------------
//
// Function:    makeClipFileNameWithExt
//
// Description: Creates a standardized Clip File Name with a .clp extension
//              by combining the caller's binPath and clipName.
//
//              The returned string will be of the form
//                 binPath/clipName/clipName.clp
//              with the correct platform-dependent dir separators.
//
// Arguments:   string& binPath     Reference to input Bin Path string
//              string& clipPath    Reference to input Clip Name string
//
// Returns:     binPath/clipName/clipName.clp
//
//------------------------------------------------------------------------
string CBinManager::makeClipFileNameWithExt(const string& binPath,
                                            const string& clipName)
{
	return makeClipFileName(binPath, clipName) + '.' + CBinDir::getClipFileExtension();
}

//------------------------------------------------------------------------
//
// Function:    makeClipFileNameWithExt
//
// Description: Creates a standardized Clip File Name with a .clp extension
//              given a variety of input formats.  The input Clip File Name
//              can be in one of the forms:
//                   binPath/clipName
//                   binPath/clipName/clipName
//                   binPath/clipName/clipName.clp
//
//              If the function succeeds, the returned string will be of
//              the form binPath/clipName/clipName.clp with the correct
//              platform-dependent dir separators.  If the functions fails,
//              an empty string is returned.  The function will fail if
//              CBinManager::splitClipFileName cannot parse the input string.
//
// Arguments:   string& clipFileName    Input Clip File Name
//
// Returns:     binPath/clipName/clipName.clp if success
//              empty string if function failed
//
//------------------------------------------------------------------------
string CBinManager::makeClipFileNameWithExt(const string& clipFileName)
{
   string binPath, clipName;

   int retVal = splitClipFileName(clipFileName, binPath, clipName);
   if (retVal != 0)
      {
      TRACE_0(errout << "ERROR: CBinManager::makeClipFileNameWithExt could not parse clip file name "
                     << endl
                     << "       " << clipFileName << endl
                     << "       CBinManager::splitClipFileName returned " << retVal);
      string emptyString;
      return emptyString;
      }

   return makeClipFileNameWithExt(binPath, clipName);
}

void CBinManager::DumpCurrentRefCount(void)
{
   openClipList.dumpListToTrace("DumpCurrentRefCount");
}

//------------------------------------------------------------------------
//
// Function:    makeClipIniFileNameWithExt
//
// Description: Creates a standardized Clip Ini File Name with an .ini
//              extension by combining the caller's binPath and clipName.
//
//              The returned string will be of the form
//                 binPath/clipName/clipName.ini
//              with the correct platform-dependent dir separators.
//
// Arguments:   string& binPath     Reference to input Bin Path string
//              string& clipPath    Reference to input Clip Name string
//
// Returns:     binPath/clipName/clipName.ini
//
//------------------------------------------------------------------------
string CBinManager::makeClipIniFileNameWithExt(const string& binPath,
                                               const string& clipName)
{
   return makeClipFileName(binPath, clipName) + '.' + CBinDir::getClipIniFileExtension();
}

//------------------------------------------------------------------------
//
// Function:    makeClipIniFileNameWithExt
//
// Description: Creates a standardized Clip Ini File Name with the .ini
//              extension given a variety of input formats.  The input Clip
//              File Name can be in one of the forms:
//                   binPath/clipName
//                   binPath/clipName/clipName
//                   binPath/clipName/clipName.clp
//
//              Note: input with a .ini file extension will not work.
//
//              If the function succeeds, the returned string will be of
//              the form binPath/clipName/clipName.ini with the correct
//              platform-dependent dir separators.  If the functions fails,
//              an empty string is returned.  The function will fail if
//              CBinManager::splitClipFileName cannot parse the input string.
//
// Arguments:   string& clipFileName    Input Clip File Name
//
// Returns:     binPath/clipName/clipName.ini if success
//              empty string if function failed
//
//------------------------------------------------------------------------
string CBinManager::makeClipIniFileNameWithExt(const string& clipFileName)
{
   string binPath, clipName;

   int retVal = splitClipFileName(clipFileName, binPath, clipName);
   if (retVal != 0)
      {
      TRACE_0(errout << "ERROR: CBinManager::makeClipIniFileNameWithExt could not parse clip file name "
                     << endl
                     << "       " << clipFileName << endl
                     << "       CBinManager::splitClipFileName returned " << retVal);
      string emptyString;
      return emptyString;
      }

   return makeClipFileName(binPath, clipName);
}
//------------------------------------------------------------------------
//
// Function:    makeNextAutoClipName
//
// Description: Generates a new clip name for a given Bin.  The default
//              form of the name is "Clip###" where ### is the next
//              available three digit index in the Bin.  The current
//              index is recorded in the Bin's file MTIBIN.ini.  Repeated
//              calls to this function will return the same name if
//              a clip of that name has not been created.
//
// Arguments:   string& binPath     Bin in which the new clip will go
//
//              string& clipNameTemplate   The base of the clip name, to
//                                  which we will tack on the sequence
//                                  number. If omitted, 'Clip' will be used.
//
// Returns:     New clip name.  If function fails, will return blank string
//
//------------------------------------------------------------------------
string CBinManager::makeNextAutoClipName(const string& binPath)
{
   string emptyString;

   return makeNextAutoClipName(binPath, emptyString);
}

string CBinManager::makeNextAutoClipName(const string& binPath,
                                         const string& clipNameTemplate)
{
   string newName;

   // Open an existing Bin File or create it if it doesn't exist
   string iniFileName = AddDirSeparator(binPath) + binIniFileName;
   CIniFile *ini = CreateIniFile(iniFileName);
   if (ini == 0)
      {
      // ERROR: cannot create or open the Bin File
      TRACE_0(errout << "ERROR: CBinManager::makeNextAutoClipName" << endl;
              errout << "       Could not open or create Bin File " << iniFileName
                     << endl;
              errout << "       Because " << strerror(errno));
      newName.erase();
      return newName;  // return empty auto clip name
      }

   // The template for creating the clip name comes from the caller,
   // or if the caller has not supplied a template, then we look in
   // the Bin's ini file.  If not in the Bin file then use the default.
   string localTemplate = clipNameTemplate;
   if (localTemplate.empty())
      {
      string defaultPrefix = "Clip";
      localTemplate = ini->ReadString(binInfoSectionName,
                                      autoClipNameTemplateKey,
                                      defaultPrefix, COMMENT_CHARS_NO_PERCENT);
                                      // don't allow % to be a comment
      }

   // Check to see if numeric formatting is present in the template.
   // Note: The test for formatting is very simple-minded: if the '%'
   //       character is present, then we assume formatting is present.
   //       It is the caller's responsibility to make sure formatting is valid
   //       if a '%' character is present in the template.
   if (localTemplate.find('%') == string::npos)
      {
      // Numeric formatting is not present, so tack on the default
      localTemplate += "%.3d";
      }

   // Get the auto clip name index from the Bin's ini file.  The default
   // index is 1.
   int autoClipNameIndex = ini->ReadInteger(binInfoSectionName,
                                            autoClipNameIndexKey, 1);

   // With the template and the index, we now have enough information
   // to construct a trial clip file name.  If the trial clip file does
   // not exist, we're done.  If the trial clip file does exist, then
   // increment the index and try again.  Repeat this until a file is found.
   // To prevent endless loop, a loop count is checked, which returns.
   int loopCount = 0, maxLoopCount = 2000;
   bool done = false;
   char trialNameBuf[2048];
   string trialName;
   while (!done)
      {
      // Create trial clip name
      sprintf(trialNameBuf, localTemplate.c_str(), autoClipNameIndex);
      trialName = trialNameBuf;
      if (!checkFileExists(makeClipPath(binPath, trialName)))
         {
         // Trial clip name does not exist, so we're done
         newName = trialName;
         done = true;
         }
      else
         {
         // Trial clip name already exists, so increment the index and repeat
         ++autoClipNameIndex;

         // Check for runaway
         if (++loopCount > maxLoopCount)
            {
            // We've gone around this loop too many times, so assume
            // something is wrong with the template
            TRACE_0(errout << "Error in CBinManager::makeNextAutoClipName"
                           << endl;
                    errout << "  Unable to construct a clip name with template "
                           << localTemplate << " and index "
                           << autoClipNameIndex);
            ini->FileName = ""; // Don't update Bin file
            DeleteIniFile(ini);
            newName.erase();
            return newName;
            }
         }
      }

   // At this point we have presumably found a new clip name, so update
   // the Bin file and then return the new name to the caller
   ini->WriteString(binInfoSectionName, autoClipNameTemplateKey,
                    localTemplate);
   ini->WriteInteger(binInfoSectionName, autoClipNameIndexKey,
                     autoClipNameIndex);
   if (!DeleteIniFile(ini))
      {
      TRACE_0(errout << "Error in CBinManager::makeNextAutoClipName" << endl;
              errout << "  Could not write Bin File " << iniFileName << endl;
              errout << "  Because " << strerror(errno));
      }

   TRACE_3(errout << " CBinManager::makeNextAutoClipName returning "
                  << newName << endl;
           errout << "  constructed from template " << localTemplate
                  << " and index " << autoClipNameIndex);

   return newName;
}

//------------------------------------------------------------------------
//
// Function:    makeNextAutoClipName
//
// Description: Generates a new clip name for a given Bin.  The default
//              form of the name is "Clip###" where ### is the next
//              available three digit index in the Bin.  The current
//              index is recorded in the Bin's file MTIBIN.ini.  Repeated
//              calls to this function will return the same name if
//              a clip of that name has not been created.
//
// Arguments:   string& binPath     Bin in which the new clip will go
//
//		int* ipIndexOffset  Used to increment the name of the clip
//				    in order to generate a range of clips
//
//              string& clipNameTemplate   The base of the clip name, to
//                                  which we will tack on the sequence
//                                  number. If omitted, 'Clip' will be used.
//
// Returns:     New clip name.  If function fails, will return blank string
//
//------------------------------------------------------------------------
string CBinManager::makeNextAutoClipName(const string& binPath,
                                         int *ipIndexOffset)
{
   string emptyString;

   return makeNextAutoClipName(binPath, emptyString, ipIndexOffset);
}

string CBinManager::makeNextAutoClipName(const string& binPath,
                                         const string& clipNameTemplate,
                                         int *ipIndexOffset)
{
   string newName;

   // Open an existing Bin File or create it if it doesn't exist
   string iniFileName = AddDirSeparator(binPath) + binIniFileName;
   CIniFile *ini = CreateIniFile(iniFileName);
   if (ini == 0)
      {
      // ERROR: cannot create or open the Bin File
      TRACE_0(errout << "ERROR: CBinManager::makeNextAutoClipName" << endl;
              errout << "       Could not open or create Bin File " << iniFileName
                     << endl;
              errout << "       Because " << strerror(errno));
      newName.erase();
      return newName;  // return empty auto clip name
      }

   // The template for creating the clip name comes from the caller,
   // or if the caller has not supplied a template, then we look in
   // the Bin's ini file.  If not in the Bin file then use the default.
   string localTemplate = clipNameTemplate;
   if (localTemplate.empty())
      {
      string defaultPrefix = "Clip";
      localTemplate = ini->ReadString(binInfoSectionName,
                                      autoClipNameTemplateKey,
                                      defaultPrefix, COMMENT_CHARS_NO_PERCENT);
                                      // don't allow % to be a comment
      }

   // Check to see if numeric formatting is present in the template.
   // Note: The test for formatting is very simple-minded: if the '%'
   //       character is present, then we assume formatting is present.
   //       It is the caller's responsibility to make sure formatting is valid
   //       if a '%' character is present in the template.
   if (localTemplate.find('%') == string::npos)
      {
      // Numeric formatting is not present, so tack on the default
      localTemplate += "%.3d";
      }

   // Get the auto clip name index from the Bin's ini file.  The default
   // index is 1.
   int autoClipNameIndex = ini->ReadInteger(binInfoSectionName,
                                            autoClipNameIndexKey, 1);

   int autoClipNameIndexOrig = autoClipNameIndex;

   autoClipNameIndex += *ipIndexOffset;


   // With the template and the index, we now have enough information
   // to construct a trial clip file name.  If the trial clip file does
   // not exist, we're done.  If the trial clip file does exist, then
   // increment the index and try again.  Repeat this until a file is found.
   // To prevent endless loop, a loop count is checked, which returns.
   int loopCount = 0, maxLoopCount = 2000;
   bool done = false;
   char trialNameBuf[2048];
   string trialName;
   while (!done)
      {
      // Create trial clip name
      sprintf(trialNameBuf, localTemplate.c_str(), autoClipNameIndex);
      trialName = trialNameBuf;
      if (!checkFileExists(makeClipPath(binPath, trialName)))
         {
         // Trial clip name does not exist, so we're done
         newName = trialName;
         done = true;
         }
      else
         {
         // Trial clip name already exists, so increment the index and repeat
         ++autoClipNameIndex;

         // Check for runaway
         if (++loopCount > maxLoopCount)
            {
            // We've gone around this loop too many times, so assume
            // something is wrong with the template
            TRACE_0(errout << "Error in CBinManager::makeNextAutoClipName"
                           << endl;
                    errout << "  Unable to construct a clip name with template "
                           << localTemplate << " and index "
                           << autoClipNameIndex);
            ini->FileName = ""; // Don't update Bin file
            DeleteIniFile(ini);
            newName.erase();
            return newName;
            }
         }
      }

   // at this point we have discovered a unique clip number.  Update
   // the caller's index value
   *ipIndexOffset = autoClipNameIndex - autoClipNameIndexOrig;

   if (!DeleteIniFile(ini))
      {
      TRACE_0(errout << "Error in CBinManager::makeNextAutoClipName" << endl;
              errout << "  Could not write Bin File " << iniFileName << endl;
              errout << "  Because " << strerror(errno));
      }

   TRACE_3(errout << " CBinManager::makeNextAutoClipName returning "
                  << newName << endl;
           errout << "  constructed from template " << localTemplate
                  << " and index " << autoClipNameIndex);

   return newName;
}


/** @brief Return the name of the repaired cadence data file without a path.
 *
 */
string CBinManager::getRepairedCadenceFileName() const
{
    return "cadence.crt";
}


/** @brief Return the path of the repaired cadence data file.
 *
 */
string CBinManager::makeRepairedCadenceFilePath(
   const string &binPath,
   const string &clipName) const
{
   return AddDirSeparator(makeClipPath(binPath, clipName)) +
      getRepairedCadenceFileName();
}


void CBinManager::getAutoClipName (const string &binPath, string &fileTemplate,
	int &fileIndex)
{
   // init the return
   fileTemplate.erase();
   fileIndex = 0;

   // Open an existing Bin File or create it if it doesn't exist
   string iniFileName = AddDirSeparator(binPath) + binIniFileName;
   CIniFile *ini = CreateIniFile(iniFileName);
   if (ini == 0)
      {
      // ERROR: cannot create or open the Bin File
      TRACE_0(errout << "ERROR: CBinManager::makeNextAutoClipName" << endl;
              errout << "       Could not open or create Bin File " <<
		iniFileName << endl;
              errout << "       Because " << strerror(errno));
       return;
      }

   // read in the template
   fileTemplate = ini->ReadString(binInfoSectionName,
                                      autoClipNameTemplateKey, "Clip",
                                      COMMENT_CHARS_NO_PERCENT);

   // read in the index
   fileIndex = ini->ReadInteger (binInfoSectionName,
                                            autoClipNameIndexKey, 1);

  delete ini;

  return;
}

void CBinManager::setAutoClipName(const string& binPath,
	const string &newClipName)
{
   // Open an existing Bin File or create it if it doesn't exist
   string iniFileName = AddDirSeparator(binPath) + binIniFileName;
   CIniFile *ini = CreateIniFile(iniFileName);
   if (ini == 0)
      {
      // ERROR: cannot create or open the Bin File
      TRACE_0(errout << "ERROR: CBinManager::setAutoDiskScheme" << endl;
              errout << "       Could not open or create Bin File " <<
		iniFileName << endl;
              errout << "       Because " << strerror(errno));
      return;  // ERROR
      }

   string strTemplate;
   char caDigit[32];
   int iPos, iCount;

   // starting at the end of the string, count the number of numeric
   // characters
   iPos = newClipName.size();
   while (iPos > 0 && newClipName[iPos-1] >= '0' && newClipName[iPos-1] <= '9')
    {
     iPos--;
    }


   // copy over the non-numeric part
   strTemplate.assign(newClipName, 0, iPos);

   // append the formatted number of digits onto the template
   iCount = 0;
   if (iPos != (int)newClipName.size())
    {
     sprintf (caDigit, "%ld", (long)(newClipName.size() - iPos));
     strTemplate = strTemplate + "%." + caDigit + "d";

     iCount = atoi (newClipName.c_str() + iPos);
    }



   // write the Template  to the file
   ini->WriteString (binInfoSectionName, autoClipNameTemplateKey,
                strTemplate);

   // write the next index to the file
   ini->WriteInteger (binInfoSectionName, autoClipNameIndexKey, iCount);

   delete ini;
}

void CBinManager::setAutoClipName(const string& binPath,
	const string &strTemplate, int iIndex)
{
   // Open an existing Bin File or create it if it doesn't exist
   string iniFileName = AddDirSeparator(binPath) + binIniFileName;
   CIniFile *ini = CreateIniFile(iniFileName);
   if (ini == 0)
      {
      // ERROR: cannot create or open the Bin File
      TRACE_0(errout << "ERROR: CBinManager::setAutoDiskScheme" << endl;
              errout << "       Could not open or create Bin File " <<
		iniFileName << endl;
              errout << "       Because " << strerror(errno));
      return;  // ERROR
      }


   // write the Template  to the file
   ini->WriteString (binInfoSectionName, autoClipNameTemplateKey,
                strTemplate);

   // write the next index to the file
   ini->WriteInteger (binInfoSectionName, autoClipNameIndexKey, iIndex);

   delete ini;
}


// mlm... could be changed to use macro
string CBinManager::getAutoClipScheme(const string& binPath)
{
   string newName;

   // Open an existing Bin File or create it if it doesn't exist
   string iniFileName = AddDirSeparator(binPath) + binIniFileName;
   CIniFile *ini = CreateIniFile(iniFileName);
   if (ini == 0)
      {
      // ERROR: cannot create or open the Bin File
      TRACE_0(errout << "ERROR: CBinManager::getAutoClipScheme" << endl;
              errout << "       Could not open or create Bin File " <<
		iniFileName << endl;
              errout << "       Because " << strerror(errno));
      newName.erase();
      return newName;  // return empty auto clip name
      }

   // read the ClipScheme from the file
   newName = ini->ReadString (binInfoSectionName, autoClipSchemeKey, "");

   TRACE_3(errout << " CBinManager::getAutoClipScheme returning "
                  << newName << endl);

   delete ini;

   return newName;
}

// mlm... could be changed to use macro
string CBinManager::getAutoDiskScheme(const string& binPath)
{
   string newName;

   // Open an existing Bin File or create it if it doesn't exist
   string iniFileName = AddDirSeparator(binPath) + binIniFileName;
   CIniFile *ini = CreateIniFile(iniFileName);
   if (ini == 0)
      {
      // ERROR: cannot create or open the Bin File
      TRACE_0(errout << "ERROR: CBinManager::getAutoDiskScheme" << endl;
              errout << "       Could not open or create Bin File " <<
		iniFileName << endl;
              errout << "       Because " << strerror(errno));
      newName.erase();
      return newName;  // return empty auto clip name
      }

   // read the DiskScheme from the file
   newName = ini->ReadString (binInfoSectionName, autoDiskSchemeKey, "");

   TRACE_3(errout << " CBinManager::getAutoDiskScheme returning "
                  << newName << endl);

   delete ini;

   return newName;
}

string CBinManager::getDailiesAudioDuration(const string& binPath)
{
   string newName;

   // Open an existing Bin File or create it if it doesn't exist
   string iniFileName = AddDirSeparator(binPath) + binIniFileName;
   CIniFile *ini = CreateIniFile(iniFileName);
   if (ini == 0)
      {
      // ERROR: cannot create or open the Bin File
      TRACE_0(errout << "ERROR: CBinManager::getDailiesAudioDuration" << endl;
              errout << "       Could not open or create Bin File " <<
		iniFileName << endl;
              errout << "       Because " << strerror(errno));
      newName.erase();
      return newName;  // return empty auto clip name
      }

   // read the DiskScheme from the file
   newName = ini->ReadString (binInfoSectionName, dailiesAudioDurationKey, "01:00:00:00");

   TRACE_3(errout << " CBinManager::getDailiesAudioDuration returning "
                  << newName << endl);

   delete ini;

   return newName;
}

string CBinManager::getDailiesImageDuration(const string& binPath)
{
   string newName;

   // Open an existing Bin File or create it if it doesn't exist
   string iniFileName = AddDirSeparator(binPath) + binIniFileName;
   CIniFile *ini = CreateIniFile(iniFileName);
   if (ini == 0)
      {
      // ERROR: cannot create or open the Bin File
      TRACE_0(errout << "ERROR: CBinManager::getDailiesImageDuration" << endl;
              errout << "       Could not open or create Bin File " <<
		iniFileName << endl;
              errout << "       Because " << strerror(errno));
      newName.erase();
      return newName;  // return empty auto clip name
      }

   // read the DiskScheme from the file
   newName = ini->ReadString (binInfoSectionName, dailiesImageDurationKey, "01:00:00:00");

   TRACE_3(errout << " CBinManager::getDailiesImageDuration returning "
                  << newName << endl);

   delete ini;

   return newName;
}

// mlm... could be changed to use macro
void CBinManager::setAutoClipScheme(const string& binPath,
	const string &newClipScheme)
{
   // Open an existing Bin File or create it if it doesn't exist
   string iniFileName = AddDirSeparator(binPath) + binIniFileName;
   CIniFile *ini = CreateIniFile(iniFileName);
   if (ini == 0)
      {
      // ERROR: cannot create or open the Bin File
      TRACE_0(errout << "ERROR: CBinManager::setAutoClipScheme" << endl;
              errout << "       Could not open or create Bin File " <<
		iniFileName << endl;
              errout << "       Because " << strerror(errno));
      return;  // ERROR
      }

   // write the ClipScheme to the file
   ini->WriteString (binInfoSectionName, autoClipSchemeKey, newClipScheme);

   delete ini;
}

void CBinManager::setDailiesAudioDuration(const string& binPath,
	const string &newAudioDuration)
{
   // Open an existing Bin File or create it if it doesn't exist
   string iniFileName = AddDirSeparator(binPath) + binIniFileName;
   CIniFile *ini = CreateIniFile(iniFileName);
   if (ini == 0)
      {
      // ERROR: cannot create or open the Bin File
      TRACE_0(errout << "ERROR: CBinManager::setDailiesAudioDuration" << endl;
              errout << "       Could not open or create Bin File " <<
		iniFileName << endl;
              errout << "       Because " << strerror(errno));
      return;  // ERROR
      }

   // write the ClipScheme to the file
   ini->WriteString (binInfoSectionName, dailiesAudioDurationKey, newAudioDuration);

   delete ini;
}

void CBinManager::setDailiesImageDuration(const string& binPath,
	const string &newImageDuration)
{
   // Open an existing Bin File or create it if it doesn't exist
   string iniFileName = AddDirSeparator(binPath) + binIniFileName;
   CIniFile *ini = CreateIniFile(iniFileName);
   if (ini == 0)
      {
      // ERROR: cannot create or open the Bin File
      TRACE_0(errout << "ERROR: CBinManager::setDailiesImageDuration" << endl;
              errout << "       Could not open or create Bin File " <<
		iniFileName << endl;
              errout << "       Because " << strerror(errno));
      return;  // ERROR
      }

   // write the ClipScheme to the file
   ini->WriteString (binInfoSectionName, dailiesImageDurationKey, newImageDuration);

   delete ini;
}

#define DEFINE_BIN_CLIP_SETTER(section, key) \
void CBinManager::set ## key(const string &binOrClipPath, const string &val) \
{ \
   string iniFileName = AddDirSeparator(binOrClipPath) + \
                        (isClipDir(binOrClipPath) ? desktopIniFileName : \
                         binIniFileName); \
   CIniFile *ini = CreateIniFile(iniFileName); \
   if (ini == 0) \
      { \
      TRACE_0(errout << "ERROR: CBinManager::set" #key << endl; \
              errout << "       Could not open or create Bin File " << \
		iniFileName << endl; \
              errout << "       Because " << strerror(errno)); \
      return;  \
      } \
 \
   ini->WriteString (section, key ## Key, val); \
 \
   delete ini; \
}

#define DEFINE_BIN_CLIP_GETTER(section, key) \
string CBinManager::get ## key(const string& binOrClipPath) \
{ \
   string newName; \
 \
   string iniFileName = AddDirSeparator(binOrClipPath) + \
                        (isClipDir(binOrClipPath) ? desktopIniFileName : \
                         binIniFileName); \
   CIniFile *ini = CreateIniFile(iniFileName); \
   if (ini == 0) \
      { \
      TRACE_0(errout << "ERROR: CBinManager::get" #key << endl; \
              errout << "       Could not open or create Bin File " << \
		iniFileName << endl; \
              errout << "       Because " << strerror(errno)); \
      newName.erase(); \
      return newName; \
      } \
 \
   newName = ini->ReadString (section, key ## Key, ""); \
 \
   TRACE_3(errout << " CBinManager::get" #key " returning " \
                  << newName << endl); \
                   \
   delete ini; \
 \
   return newName; \
}

DEFINE_BIN_CLIP_SETTER(dailiesInfoSectionName, company);
DEFINE_BIN_CLIP_SETTER(dailiesInfoSectionName, address);
DEFINE_BIN_CLIP_SETTER(dailiesInfoSectionName, cityStatePostal);
DEFINE_BIN_CLIP_SETTER(dailiesInfoSectionName, country);
DEFINE_BIN_CLIP_SETTER(dailiesInfoSectionName, jobNo);
DEFINE_BIN_CLIP_SETTER(dailiesInfoSectionName, prodCo);
DEFINE_BIN_CLIP_SETTER(dailiesInfoSectionName, project);
DEFINE_BIN_CLIP_SETTER(dailiesInfoSectionName, title);
DEFINE_BIN_CLIP_SETTER(dailiesInfoSectionName, prodNo);
DEFINE_BIN_CLIP_SETTER(dailiesInfoSectionName, shootDate);
DEFINE_BIN_CLIP_SETTER(dailiesInfoSectionName, transferDate);
DEFINE_BIN_CLIP_SETTER(dailiesInfoSectionName, fieldA);
DEFINE_BIN_CLIP_SETTER(dailiesInfoSectionName, fieldB);
DEFINE_BIN_CLIP_SETTER(dailiesInfoSectionName, fieldC);
DEFINE_BIN_CLIP_SETTER(dailiesInfoSectionName, workOrder);
DEFINE_BIN_CLIP_SETTER(dailiesInfoSectionName, colorist);
DEFINE_BIN_CLIP_SETTER(dailiesInfoSectionName, assist);

DEFINE_BIN_CLIP_GETTER(dailiesInfoSectionName, company);
DEFINE_BIN_CLIP_GETTER(dailiesInfoSectionName, address);
DEFINE_BIN_CLIP_GETTER(dailiesInfoSectionName, cityStatePostal);
DEFINE_BIN_CLIP_GETTER(dailiesInfoSectionName, country);
DEFINE_BIN_CLIP_GETTER(dailiesInfoSectionName, jobNo);
DEFINE_BIN_CLIP_GETTER(dailiesInfoSectionName, prodCo);
DEFINE_BIN_CLIP_GETTER(dailiesInfoSectionName, project);
DEFINE_BIN_CLIP_GETTER(dailiesInfoSectionName, title);
DEFINE_BIN_CLIP_GETTER(dailiesInfoSectionName, prodNo);
DEFINE_BIN_CLIP_GETTER(dailiesInfoSectionName, shootDate);
DEFINE_BIN_CLIP_GETTER(dailiesInfoSectionName, transferDate);
DEFINE_BIN_CLIP_GETTER(dailiesInfoSectionName, fieldA);
DEFINE_BIN_CLIP_GETTER(dailiesInfoSectionName, fieldB);
DEFINE_BIN_CLIP_GETTER(dailiesInfoSectionName, fieldC);
DEFINE_BIN_CLIP_GETTER(dailiesInfoSectionName, workOrder);
DEFINE_BIN_CLIP_GETTER(dailiesInfoSectionName, colorist);
DEFINE_BIN_CLIP_GETTER(dailiesInfoSectionName, assist);

// mlm... could be changed to use macro
void CBinManager::setAutoDiskScheme(const string& binPath,
	const string &newDiskScheme)
{
   // Open an existing Bin File or create it if it doesn't exist
   string iniFileName = AddDirSeparator(binPath) + binIniFileName;
   CIniFile *ini = CreateIniFile(iniFileName);
   if (ini == 0)
      {
      // ERROR: cannot create or open the Bin File
      TRACE_0(errout << "ERROR: CBinManager::setAutoDiskScheme" << endl;
              errout << "       Could not open or create Bin File " <<
		iniFileName << endl;
              errout << "       Because " << strerror(errno));
      return;  // ERROR
      }

   // write the DiskScheme to the file
   ini->WriteString (binInfoSectionName, autoDiskSchemeKey, newDiskScheme);

   delete ini;
}

bool CBinManager::checkFileExists(const string& fileName)
{
   return ::checkFileExists(fileName);
}


// This is some fugly excrement & needs to be eliminated!! QQQ
//------------------------------------------------------------------------
//
// Function:    splitClipFileName
//
// Description: Splits a Clip File Name into separate Bin Path and
//              Clip Name strings.  The Clip File Name can be in one
//              of the forms:
//                   binPath/clipName
//                   binPath/clipName/clipName
//                   binPath/clipName/clipName.clp
//
// Arguments:   string& clipFileName    Input Clip File Name
//              string& binPath         Output Bin Path (cleared if error)
//              string& clipName        Output Clip Name (cleared if error)
//
// Returns:     0 if success, error if could not parse the file name
//
//------------------------------------------------------------------------
int CBinManager::splitClipFileName(
   const string& clipFileName,
   string& binPath,           // Output
   string& clipName) const    // Output
{
   // Pull apart the various elements of the clip file name
   string directoryPath1 = RemoveDirSeparator(GetFilePath(clipFileName));
   string directoryName = GetFileNameWithExt(directoryPath1);
   string directoryPath2 = RemoveDirSeparator(GetFilePath(directoryPath1));

   string dotExt = '.' + CBinDir::getClipFileExtension();  // ".clp"
   if (GetFileExt(clipFileName) == dotExt)
   {
      // There is a ".clp" extension, so assume the clip file name is of the
      // form "binPath/clipName/clipName.clp"
      binPath = directoryPath2;
      clipName = GetFileName(clipFileName);

      // Sanity check - the clip directory and clip name must match
      MTIassert(clipName == directoryName);
      if (clipName != directoryName)
      {
         binPath = "";
         clipName = "";
         return CLIP_ERROR_BAD_CLIP_FILENAME;
      }

      return 0;
   }

   // There is no extension or the extension is not .clp, so assume that
   // the clip file name is of the form "binPath/clipName/clipName"
   // or "binPath/clipName".
   binPath = directoryPath1;
   clipName = GetFileNameWithExt(clipFileName);

   // Completely bogus directory existence check is to allow clips that
   // are named the same as the bin they are in!!!
   if (clipName == directoryName && !DoesDirectoryExist(clipFileName))
   {
      binPath = directoryPath2;
   }

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    getProjectBinName
//
// Description: Splits the clip file name and extracts the name of the
//              first bin after the root bin.  The first such bin is the
//              "project bin".
//
// Arguments:   string& clipFileName    Input Clip File Name
//              string& longProjectBin      Output Root Bin + Project Bin
//				(cleared if error)
//              string& shortProjectBin      Output Project Bin Only
//				(cleared if error)
//
// Returns:     0 if success, error if could not parse the file name
//
//------------------------------------------------------------------------
int CBinManager::getProjectBinName (const string &clipFileName,
		string &longProjectBin, string &shortProjectBin)
{
  longProjectBin = "";
  shortProjectBin = "";

  string binPath, clipName;
  int iRet;

  iRet = splitClipFileName(clipFileName, binPath, clipName);
  if (iRet)
    return iRet;

  // get the list of root bins
  StringList slRoot;
  CPMPGetBinRoots(&slRoot);


  // find the root that matches the binPath
  for (unsigned int i = 0; i < slRoot.size(); i++)
   {
    if (binPath.size() > slRoot.at(i).size())
     {
      if (binPath.substr(0,slRoot.at(i).size()) == slRoot.at(i))
       {
        // found the correct root bin

        // find the first character of the Project Bin
        unsigned int iFirst = slRoot.at(i).size();
        while (iFirst < binPath.size() && binPath.at(iFirst) == DIR_SEPARATOR)
         {
          iFirst++;
         }

        // find the last character of the Project Bin
        unsigned int iLast = iFirst;
        while (iLast < binPath.size() && binPath.at(iLast) != DIR_SEPARATOR)
         {
          iLast++;
         }

        longProjectBin = binPath.substr(0, iLast);
        shortProjectBin = binPath.substr(iFirst, iLast-iFirst);

        return 0;
       }
     }
   }

  // if we got to here, we did not find the project bin name.  Return error
  return CLIP_ERROR_BAD_PROJECT_BIN;
} // getProjectBinName

//////////////////////////////////////////////////////////////////////
//
// This will take a clipname and return two lists of the Bins that
//   are part of the clipname.  The Project Directory (which is
//   the first directory after CPMP Bin Root) is NOT included
//   in the returned lists as a separate entry - it is, of course,
//   included in each entry in the first (full path) list.  There
//   are two lists because the first
//   is a list containing entries with the the full path including
//   the Bin name and the second list has entries which contain
//   only the Bin name.
//
//   Created: 7/24/2006 - Michael Russell
//   Modified: 8/07/2006 - mpr - fixed bug when no bins
//
//   Typical Usage:
//      i = bmMgr.GetRecursiveBinList(fullClipName, longProjectBins,
//                                    shortProjectBins);
//   Inputs:
//      fullClipName = string (e.g. "C:\\MTIShare\\MTIBins\\Proj\\Clip001")
//   Returns:
//      int              = number of Bins found
//                         -1 = error
//      longProjectBins  = list of Bins including path
//      shortProjectBins = list of Bins (no path)
//
//////////////////////////////////////////////////////////////////////
int CBinManager::GetRecursiveBinList(const string& fullClipName,
                                     StringList& longProjectBins,
                                     StringList& shortProjectBins)
{
  longProjectBins.clear();
  shortProjectBins.clear();

  string projectDir;
  string projectDirShort;

  int iRet;
  iRet = getProjectBinName(fullClipName, projectDir, projectDirShort);
  if (iRet != 0)
    return(iRet);

  string binPath, clipName;

  iRet = splitClipFileName(fullClipName, binPath, clipName);
  if (iRet != 0)
    return iRet;

  // If no bins within project, then just return
  if (binPath == projectDir)
    return 0;

  string binDir;

  int i = binPath.find(projectDir);
  if (i != (int)string::npos) {
    int start = i + projectDir.length() + 1;      // + 1 for backslash
    int len   = binPath.length() - projectDir.length() - i;
    binDir.assign(binPath, start, len);
  }
  else  // Project Directory not found - this should not happen
    return -1;

  string fullPath = projectDir;
  string shortBin, longBin;

  bool done_f = false;

  do {
    i = binDir.find(DIR_SEPARATOR);

    if (i == (int)string::npos) {        // if not found, then use remainder
      shortBin = binDir;
      binDir   = "";
      done_f = true;
    }
    else {
      shortBin.assign(binDir, 0, i);
      int len = binDir.length() - shortBin.length() - 1;
      binDir = binDir.substr(i + 1, len);
    }

    longBin = fullPath + DIR_SEPARATOR + shortBin;
    shortProjectBins.push_back(shortBin);
    longProjectBins.push_back(longBin);
    fullPath = longBin;
  } while (!done_f);

  return 0;
}

//------------------------------------------------------------------------
//
// Function:    getProjectBinList
//
// Description: gets list of all Project Bins.  A Project Bin the name of the
//              first bin after the root bin.
//
// Arguments:
//              string& longProjectBin      Output Root Bin + Project Bin
//				(cleared if error)
//              string& shortProjectBin      Output Project Bin Only
//				(cleared if error)
//
// Returns:     0 if success, error if could not parse the file name
//
//------------------------------------------------------------------------
int CBinManager::getProjectBinList (StringList &longProjectBin,
		StringList &shortProjectBin)
{
  longProjectBin.clear();
  shortProjectBin.clear();


  // get the list of root bins
  StringList slRoot;
  CPMPGetBinRoots(&slRoot);


  // run through all root bins
  for (unsigned int i = 0; i < slRoot.size(); i++)
   {
	 CBinDir binDir;
    bool foundBin;

    foundBin = binDir.findFirst (slRoot.at(i), BIN_SEARCH_FILTER_BIN);

    while (foundBin)
     {
      longProjectBin.push_back (AddDirSeparator (slRoot.at(i))+binDir.fileName);
      shortProjectBin.push_back (binDir.fileName);

      foundBin = binDir.findNext();
     }
   }

  return 0;
} // getProjectBinList


string CBinManager::createDateAndTimeString()
{
  static time_t lastTime=-1;
  static int lastCount=0;

   // Get the current date and time
   time_t ltime;
   time(&ltime);     // Y2K problem?, i.e. run out of seconds in 2038?
   if (ltime == lastTime)
    {
     lastCount++;
    }
   else
    {
     lastCount = 0;
    }
   struct tm *today = localtime(&ltime);

   // Format date/time string to YYYY:MM:DD:HH:MM:SS
   char todayStr[101], todayStrExtended[128];
   strftime(todayStr, 100, "%Y:%m:%d:%H:%M:%S", today);
   sprintf (todayStrExtended, "%s.%.2d", todayStr, lastCount);

   // Return as string
   return string(todayStrExtended);
}

string CBinManager::createTrashPrefix() const
{
   // Get the current date and time
   time_t ltime;
   time(&ltime);     // Y2K problem?, i.e. run out of seconds in 2038?

   char tmpStr[64];
   sprintf(tmpStr, ".mtitrash%lx-", (long)ltime);
   return string(tmpStr);
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    createBin
//
// Description:
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
int CBinManager::createBin(const string& path, const string& binName)
{
   int retVal;

   retVal = createDirectory(path, binName);
   if (retVal != 0)
      {
      // ERROR: Could not create directory for the Bin
      return retVal;        // error message in theError
      }

   // path is the parent bin, path+binName is the child.  Copy the values
   // out of the parent's MTIBin.ini into the child

   // get parents ini values
   string strAutoClipScheme = getAutoClipScheme (path);
   string strAutoDiskScheme = getAutoDiskScheme (path);
   string strAutoClipNameTemplate;
   int iAutoClipNameIndex;
   getAutoClipName (path, strAutoClipNameTemplate, iAutoClipNameIndex);

   // create the name of the child
   string strChild = AddDirSeparator(path) + binName;

   // set the values on the child
   setAutoClipName (strChild, strAutoClipNameTemplate, 1);
   setAutoClipScheme (strChild, strAutoClipScheme);
   setAutoDiskScheme (strChild, strAutoDiskScheme);

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    doesBinExist
//
// Description: Determines if the specified Bin exists
//
// Arguments:   Bin's path and name
//
// Returns:     true if Bin exists
//              false if Bin does not exist or
//                    if Bin exists but is not a directory  or
//                    if Bin or Bin's path is not accessible
//
//------------------------------------------------------------------------
bool CBinManager::doesBinExist(const string& path, const string& binName)
{
   string binPath = AddDirSeparator(path) + binName;

   return doesBinExist(binPath);
}

bool CBinManager::doesBinExist(const string& binPathName)
{
   return DoesDirectoryExist(binPathName);
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////
int CBinManager::createDirectory(const string& path, const string& dirName)
{
   int retVal;
   string newPath = AddDirSeparator(path) + dirName;
   CAutoErrorReporter autoErr("CBinManager::createDirectory",
                              AUTO_ERROR_NO_NOTIFY, AUTO_ERROR_TRACE_0);

#if defined(_WIN32)
   // Check for characters that Windows does not like in path names
   string badChars = "\\/:*?\"<>|#";
   if (dirName.find_first_of(badChars) != string::npos)
      {
      autoErr.errorCode = CLIP_ERROR_CREATE_DIRECTORY_FAILED;
      autoErr.msg << "Could not create directory " << newPath << "    " << endl
                  << "Directory name cannot contain any of the characters "
                  << badChars << "    ";
      return autoErr.errorCode;
      }
#endif

   return createDirectory(newPath);
}

int CBinManager::createDirectory(const string& newPath)
{
   int retVal;
   CAutoErrorReporter autoErr("CBinManager::createDirectory",
                              AUTO_ERROR_NO_NOTIFY, AUTO_ERROR_TRACE_0);

#if defined(_WIN32)

   if (CreateDirectory(newPath.c_str(), 0))
      {
      // Success
      retVal = 0;
      }
   else
      {
      auto errCode = GetLastError();
      autoErr.errorCode = CLIP_ERROR_CREATE_DIRECTORY_FAILED;
      autoErr.msg << "Could not create directory " << newPath << "    " << endl
                  << "Reason: " << GetSystemErrorMessage(errCode);
      return autoErr.errorCode;
      }

#else // defined(__sgi)
   if (mkdir(newPath.c_str(), 00755) == 0)
      {
      // Success
      retVal = 0;
      }
   else
      {
      // Create failed, look at errno for reason
      TRACE_0(errout << "CBinManager::createDirectory could not create directory"
                     << endl
                     << "     " << newPath << endl;
              errout << "  because " << strerror(errno) << endl;);
      retVal = CLIP_ERROR_CREATE_DIRECTORY_FAILED;
      }
#endif

   return retVal;
} // createDirectory

int CBinManager::removeDirectory(const string& path, const string& dirName)
{
   int retVal;
   string newPath = AddDirSeparator(path) + dirName;

   retVal = removeDirectory(newPath);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CBinManager::removeDirectory(const string& newPath)
{
   int retVal;

#if defined(_WIN32)
   if (RemoveDirectory(newPath.c_str()))
      {
      // Success
      retVal = 0;
      }
   else
      {
      // Remove failed, call GetLastError for reason
      TRACE_0(errout << "CBinManager::removeDirectory could not delete directory"
                     << endl
                     << "     " << newPath << endl;
              errout << "  because " << strerror(errno) << endl;);
      retVal = CLIP_ERROR_REMOVE_DIRECTORY_FAILED;
      }

#else // defined(__sgi)
   if (rmdir(newPath.c_str()) == 0)
      {
      // Success
      retVal = 0;
      }
   else
      {
      // Remove failed, look at errno for reason
      TRACE_0(errout << "CBinManager::removeDirectory could not delete directory"
                     << endl
                     << "     " << newPath << endl;
              errout << "  because " << strerror(errno) << endl;);
      retVal = CLIP_ERROR_REMOVE_DIRECTORY_FAILED;
      }
#endif

   return retVal;

} // deleteDirectory

// This removes ONE LEVEL of contents from a directory and the directory itself.
// For paranoic reasons, it is NOT FULLY RECURSIVE.
int CBinManager::removeDirectoryAndOneLevelOfContents(const string& path)
{
   if (path.empty())
      return 0;

   // For sanity, strip trailing slash
   string directory = path;
   if (directory[directory.length() - 1] == '\\')
      directory.erase(directory.length() - 1, 1);

   WIN32_FIND_DATA FindFileData;
   string pattern = directory + "\\*";
   HANDLE hFind = FindFirstFile(pattern.c_str(), &FindFileData);
   if (hFind != INVALID_HANDLE_VALUE)
   {
      do
      {
         if (strcmp(FindFileData.cFileName, ".") == 0
             || strcmp(FindFileData.cFileName, "..") == 0)
         {
            continue;
         }

         string filename = AddDirSeparator(directory) + FindFileData.cFileName;
         DWORD attributes = GetFileAttributes(filename.c_str());
         if (attributes & FILE_ATTRIBUTE_DIRECTORY)
         {
            RemoveDirectory(filename.c_str());
         }
         else
         {
            DeleteFile(filename.c_str());
         }

      } while(FindNextFile(hFind, &FindFileData));

      FindClose(hFind);
      hFind = INVALID_HANDLE_VALUE;
   }

   // This is the local removeDirectory so we get a trace if it fails
   return removeDirectory(directory);
}

int CBinManager::removeFile(const string &fileName)
{

#if defined(__sgi) || defined(__linux)    // UNIX

   // Delete the file
   int retVal = unlink(fileName.c_str());

   if (retVal != 0)
      {
      TRACE_3(errout << "CBinManager::removeFile failed to unlink file "
                     << fileName << endl;
              errout << strerror(errno) << endl;);
      return CLIP_ERROR_REMOVE_FILE_FAILED;
      }

#elif defined(_WIN32)      // Win32 API

   // Delete the file
   int retVal = DeleteFile(fileName.c_str());

   if (retVal == 0)
      {
      // GetLastError();
      TRACE_3(errout << "CFileIO::deleteFile failed to delete file "
                     << fileName << endl;
              errout << strerror(errno) << endl;);
      return CLIP_ERROR_REMOVE_FILE_FAILED;
      }

#else
#error Unknown compiler
#endif
   return 0;
}

//////////////////////////////////////////////////////////////////////
// Clip Management Functions
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:      closeAllClips
//
// Description:   Close all of the clips that are currently open
//
//                Note: This function must be called prior to exiting
//                      the program so that the clips are deleted in
//                      an orderly manner
//
// Arguments:     none
//
// Returns:       0 = Success
//
//------------------------------------------------------------------------
int CBinManager::closeAllClips()
{
   openClipList.deleteList();

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    closeClip
//
// Description: Decrement the reference count for this clip previously
//              returned by createClip or openClip..  When the
//              reference count is zero, meaning no one else has the
//              clip open, then the clip is closed.
//              The CClip instance and its constituent tracks and other
//              components are deleted from memory and removed from the
//              BinManager's Open Clip List.
//
//              Note: The clip is deleted even if it is not in the clip list
//
// Arguments:   ClipSharedPtr &clip     Pointer to the clip to close
//
// Returns:     0    Success
//              -1   Clip was not present in the clip list
//              CLIP_ERROR_UNBALANCED_CLIP_OPEN_CLOSED if reference
//                   is less than zero, indicating that closeClip was
//                   called more times than openClip
//
//------------------------------------------------------------------------
int CBinManager::closeClip(ClipSharedPtr &clip)
{
   int retVal;

#ifdef FIND_CLIP_LEAK
	TRACE_0(errout << "closeClip(" << clip->getRelativeClipPath() << ")");
#endif

   if (clip == nullptr)       // Test for nullptr clip pointer
      return 0;         // No clip to delete, simply return success

   int refCount = openClipList.decrRefCount(clip);

   if (refCount < 0)
      {
      DBCOMMENT("CLIP_ERROR_UNBALANCED_CLIP_OPEN_CLOSED");
      return CLIP_ERROR_UNBALANCED_CLIP_OPEN_CLOSED;
      }

   //  Check reference count, only delete when reference count is zero
   if (refCount == 0)
      {
//      DBCOMMENT("Ref count == 0 - really close the clip");
      retVal = closeClipForced(clip);

      if (retVal != 0)
         return retVal;
      }

   CClip::DumpMap();

   return 0;
}

int CBinManager::closeClipForced(ClipSharedPtr &clip)
{
   // Closes clip without regard to reference count

   int retVal;

   if (clip == nullptr)       // Test for nullptr clip pointer
      return 0;         // No clip to delete, simply return success

// THIS WAS MOVED TO CClip Destructor!
// BUT I'M PARANOID, so I left this here as well!
   // Update the Video Proxies' Field List files and Time Tracks' Frame List
   // files.
   if (!clip->isClipFileDeleted())
      {
      retVal = clip->updateAllTrackFiles();
      if (retVal != 0)
         return retVal;

      // Also the actual video frame lists if necessary
      retVal = clip->updateAllVideoFrameListFiles();
      if (retVal != 0)
         return retVal;
      }

   // Remove the clip from the Clip List
   retVal = openClipList.removeClip(clip);
   if (retVal != 0)
      return retVal;

   // Delete the CClip instance
   clip = nullptr;

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    createClip
//
// Description: Creates a new Clip according to the specifications in
//              the CClipInitInfo argument.
//
//              Note: Never delete the clip returned by this function.
//                    Use the CBinManager function closeClip if you wish
//                    to remove the clip from memory
//
// Arguments:   CClipInitInfo *clipInitInfo
//              int *status
//
// Returns:     Pointer to new CClip instance.  nullptr if creation failed.
//
//------------------------------------------------------------------------
ClipSharedPtr CBinManager::createClip(CClipInitInfo &clipInitInfo, int *status)
{
   int retVal = -1, retVal2;
   int i;

   // Validate the caller's clipInitInfo before attempting to create the clip
   retVal = clipInitInfo.validate();
   if (retVal != 0)
      {
      TRACE_0(errout << "ERROR: CBinManager::createClip: Init Info validation failed"
                     << endl
                     << "       Return code " << retVal);
      *status = retVal;  // Validation failed
      return nullptr;
      }

//   // mbraca added: don't allow interlacing in image files - it doesn't work
//   // QQQ Also disallow handles. Probably shouldn't do that?
//   // ***************** HACK-O-RAMA ****************************************
//   // QQQ This should be done in the MediaAccess objects, methinks
//   if (clipInitInfo.doesNormalVideoExist() &&
//        CMediaInterface::isMediaTypeImageRepository(
//                         clipInitInfo.mainVideoInitInfo->getMediaType()))
//      {
//      CImageRepositoryMediaAccess::hackImageFormat(
//                           clipInitInfo.mainVideoInitInfo->getMediaType(),
//                           *clipInitInfo.mainVideoInitInfo->getImageFormat());
//      clipInitInfo.mainVideoInitInfo->setHandleCount(0);
//      }

   // Check that the bin path exists
   if(!DoesDirectoryExist(clipInitInfo.binPath))
      {
      // ERROR: The Bin where the new clip is supposed to reside does
      //        not exist
      TRACE_0(errout << "ERROR: CBinManager::createClip: Bin "
                     << clipInitInfo.binPath << endl
                     << "       does not exist");
      *status = CLIP_ERROR_BIN_DOES_NOT_EXIST;
      return nullptr;
      }

   // Check if clip directory or clip file already exist
   // Okay if clip directory exists if it has the clip initialization file
   // but not the actual clip file
   string clipDir = makeClipPath(clipInitInfo.binPath, clipInitInfo.clipName);
   if ((DoesDirectoryExist(clipDir)
        && !doesClipIniExist(clipInitInfo.binPath, clipInitInfo.clipName))
       || doesClipExist(clipInitInfo.binPath, clipInitInfo.clipName))
      {
      // ERROR: Clip directory or clip file already exists in the Bin,
      //        so cannot create a new clip with the same name
      TRACE_0(errout << "ERROR: CBinManager::createClip: Clip directory or file "
                     << clipInitInfo.clipName << endl
                     << " already exists in Bin " << clipInitInfo.binPath);
      *status = CLIP_ERROR_CLIP_ALREADY_EXISTS;
      return nullptr;
      }

   // Check for enough space on metadata volume
   retVal = clipInitInfo.checkMetadataStorage();
   if (retVal != 0)
      {
      // ERROR: Metadata storage problem
      *status = retVal;
      return nullptr;
      }

   // Determine if requested media storage is present and check if there
   // is enough media storage to allocate for each of the video proxies
   // and audio tracks
   retVal = clipInitInfo.checkMediaStorage();
   if (retVal != 0)
      {
      // ERROR: Media storage problem
      *status = retVal;
      return nullptr;
      }

   // Create the directory for the clip.  This will establish, before
   // any media storage is allocated, that we have sufficient permissions
   // in order to create the clip directory and clip files
   // If any of the subsequent clip-creation steps fail, we must remember
   // to remove this directory
   if (!DoesDirectoryExist(clipDir))
      {
      retVal = createDirectory(clipInitInfo.binPath, clipInitInfo.clipName);
      if (retVal != 0)
         {
         // ERROR: Could not create the clip's directory
         *status = retVal;
         return nullptr;
         }
      }

   // Default Clip creation date and time is now unless it is provided
   // in the clip init info
   if (clipInitInfo.createDateTime.empty())
      clipInitInfo.createDateTime = createDateAndTimeString();

   // Create a CClip instance given caller's parameters
   auto clip = CClip::Create();
   retVal = clip->initClip(clipInitInfo);
   if (retVal != 0)
      {
      // ERROR: Could not initialize CClip instance
      *status = retVal;
//      delete clip;
      removeDirectory(clipInitInfo.binPath, clipInitInfo.clipName);
      return nullptr;
      }

   // Iterate over the video proxy and audio track initialization
   // information in the caller's clipInitInfo object, adding Video Proxies
   // and Audio Tracks to the new clip as required

   // Normal Video
   if (clipInitInfo.doesNormalVideoExist())
      {
      // Add the Normal Video (Proxy) to the new clip
      if (clipInitInfo.mainVideoInitInfo->getAppId().empty())
         {
         // Set default AppId for Main Video proxy
         clipInitInfo.mainVideoInitInfo->setAppId("MainVideo");
         }

      retVal = clip->addVideoProxy(*(clipInitInfo.mainVideoInitInfo));
      if (retVal != 0)
         {
         // ERROR: Could not add the Normal Video (Proxy)
         // Cleanup: delete clip directory
//         delete clip;
         removeDirectory(clipInitInfo.binPath, clipInitInfo.clipName);
         *status = retVal;
         return nullptr;
         }

      // Now consider adding Video Proxies
      int videoProxyCount = clipInitInfo.getVideoProxyCount();
      for (i = 0; i < videoProxyCount; ++i)
         {
         CVideoInitInfo *videoInitInfo = clipInitInfo.videoProxyInitInfoList[i];
         if (videoInitInfo->getAppId().empty())
            {
            // Set default App Id for Video Proxy
            MTIostringstream ostr;
            ostr << "VideoProxy" << i+1;
            videoInitInfo->setAppId(ostr.str());
            }

         retVal = clip->addVideoProxy(*videoInitInfo);
         if (retVal != 0)
            {
            // ERROR: Could not add the Video Proxy
            // Cleanup: delete whatever files and directories have already
            //      been created.  Delete clip and other objects from memory.
//            delete clip;
            retVal2 = deleteClip(clipInitInfo.binPath, clipInitInfo.clipName);
            if (retVal2 != 0)
               TRACE_0(errout << "ERROR: Failed to delete clip " << retVal2);
            *status = retVal;
            return nullptr;
            }
         }
      }

   // Now consider adding Audio Tracks
   int audioTrackCount = clipInitInfo.getAudioTrackCount();
   for (i = 0; i < audioTrackCount; ++i)
      {
      CAudioInitInfo *audioInitInfo = clipInitInfo.audioTrackInitInfoList[i];
      if (audioInitInfo->getAppId().empty())
         {
         // Set default App Id for Audio Track
         MTIostringstream ostr;
         ostr << "AudioTrack" << i+1;
         audioInitInfo->setAppId(ostr.str());
         }

      if (audioInitInfo->getHandleCount() !=
          clipInitInfo.mainVideoInitInfo->getHandleCount())
      {
         TRACE_0(errout << "CLIP SCHEME ERROR: Audio track's handle count does not match main video's");
         TRACE_0(errout << "Changing audio handle count: "
                        << audioInitInfo->getHandleCount() << " --> "
                        << clipInitInfo.mainVideoInitInfo->getHandleCount());
         audioInitInfo->setHandleCount(clipInitInfo.mainVideoInitInfo->getHandleCount());
      }

      retVal = clip->addAudioTrack(*audioInitInfo);
      if (retVal != 0)
         {
         // ERROR: Could not add the Audio Track
         // Cleanup: delete whatever files and directories have already
         //      been created.  Delete clip and other objects from memory.
//         delete clip;
         retVal2 = deleteClip(clipInitInfo.binPath, clipInitInfo.clipName);
         if (retVal2 != 0)
            TRACE_0(errout << "ERROR: Failed to delete clip " << retVal2);
         *status = retVal;
         return nullptr;
         }
      }

   // Now consider adding Time Tracks
   int timeTrackCount = clipInitInfo.getTimeTrackCount();

   for (i = 0; i < timeTrackCount; ++i)
      {
      retVal = clip->addTimeTrack(*(clipInitInfo.timeTrackInitInfoList[i]));
      if (retVal != 0)
         {
         // ERROR: Could not add the Time Track
         // Cleanup: delete whatever files and directories have already
         //      been created.  Delete clip and other objects from memory.
//         delete clip;
         retVal2 = deleteClip(clipInitInfo.binPath, clipInitInfo.clipName);
         if (retVal2 != 0)
            TRACE_0(errout << "ERROR: Failed to delete clip " << retVal2);
         *status = retVal;
         return nullptr;
         }
      }

   // Add the clip to the BinManager's Open Clip List
   openClipList.addClip(clip);

   // Remember the clip in the Clip Init Info
   clipInitInfo.setClipPtr(clip);

   *status = 0;
   return clip;

} // createClip

//------------------------------------------------------------------------
//
// Function:    createClipFromIniFile
//
// Description: Creates a new Clip according to the specifications in
//              the Clip Initialization file found in the clip's
//              directory.
//
//              The Clip Initialization file is a complete Clip Scheme file
//              with all of the information necessary to create the clip.
//
//              The Clip Initialization file is assumed to be the file:
//                  newBinPath/newClipName/newClipName.ini
//
//              Note: Never delete the clip returned by this function.
//                    Use the CBinManager function closeClip if you wish
//                    to remove the clip from memory
//
// Arguments:   CClipInitInfo *clipInitInfo
//              int *status
//
// Returns:     Pointer to new CClip instance.  nullptr if creation failed.
//
//------------------------------------------------------------------------
ClipSharedPtr CBinManager::createClipFromIniFile(const string& newBinPath,
                                           const string& newClipName,
                                           int* status)
{
   int retVal;
   CClipInitInfo clipInitInfo;

   // Put together the file name for the Clip Initialization file
   string clipIniFileName = makeClipFileName(newBinPath, newClipName)
									 + '.' + CBinDir::getClipIniFileExtension();

   // Read the Clip Initialization (Scheme) file and set the clip init info
   retVal = clipInitInfo.readClipScheme(clipIniFileName);
   if (retVal != 0)
      {
      *status = retVal; // ERROR: Could not read the Clip Initialization file
      return nullptr;  // nullptr clip pointer
      }

   // Force the Bin Path and Clip Name in the clip init info
   clipInitInfo.setBinPath(newBinPath);
   clipInitInfo.setClipName(newClipName);

   // Create the new clip
   CBinManager binMgr;
   auto clip = binMgr.createClip(clipInitInfo, &retVal);
   if (retVal != 0)
      {
      *status = retVal; // ERROR: Could no create the clip
      return nullptr;  // nullptr clip pointer
      }

   *status = 0;   // Success
   return clip;
}

//------------------------------------------------------------------------
//
// Function:    createClipIniFile
//
// Description: Creates a new Clip Initialization file according to the
//              specifications in the CClipInitInfo argument.
//
//              A new directory with the clip's name is created under
//              the Bin Path and the Clip Initialization file is written
//              to the new directory.
//
//              A error is reported if the clip directory or ini file already
//              exist.
//
// Arguments:   CClipInitInfo *clipInitInfo
//
// Returns:     0 if success, non-zero if error
//
//------------------------------------------------------------------------
int CBinManager::createClipIniFile(CClipInitInfo &clipInitInfo)
{
   int retVal;

   // Validate the caller's clipInitInfo before attempting to create the clip
   retVal = clipInitInfo.validate();
   if (retVal != 0)
      {
      TRACE_0(errout << "ERROR: CBinManager::createClipIniFile: Init Info validation failed"
                     << endl
                     << "       Return code " << retVal);
      return retVal; // Validation failed
      }

   // Check that the bin path exists
   if(!DoesDirectoryExist(clipInitInfo.binPath))
      {
      // ERROR: The Bin where the new clip is supposed to reside does
      //        not exist
      TRACE_0(errout << "ERROR: CBinManager::createClipIniFile: Bin "
                     << clipInitInfo.binPath << endl
                     << "       does not exist");
      return CLIP_ERROR_BIN_DOES_NOT_EXIST;
      }

   // Check if clip directory already exists
   string clipDir = makeClipPath(clipInitInfo.binPath, clipInitInfo.clipName);
   if (DoesDirectoryExist(clipDir))
      {
      // ERROR: Clip directory already exists in the Bin,
      //        so cannot create a new clip with the same name
      TRACE_0(errout << "ERROR: CBinManager::createClipIniFile: Clip directory "
                     << clipInitInfo.clipName << endl
                     << " already exists in Bin " << clipInitInfo.binPath);
      return CLIP_ERROR_CLIP_ALREADY_EXISTS;
      }

   // Create the directory for the clip.  This will establish that
   // we have sufficient permissions in order to create the clip directory
   // and clip files.  If any of the subsequent clip-creation steps fail,
   // we must remember to remove this directory.
   retVal = createDirectory(clipInitInfo.binPath, clipInitInfo.clipName);
   if (retVal != 0)
      return retVal; // ERROR: Could not create the clip's directory

   string clipIniFileName = makeClipIniFileNameWithExt(clipInitInfo.binPath,
                                                       clipInitInfo.clipName);

   retVal = clipInitInfo.writeClipScheme(clipIniFileName);
   if (retVal != 0)
      {
      // ERROR: Could not write the Clip Initialization file
      // Remove the Clip directory
      int retVal2;
      retVal2 = removeDirectory(clipInitInfo.binPath, clipInitInfo.clipName);
      if (retVal2 != 0)
         {
         TRACE_0(errout << "ERROR: removeDirectory failed for Bin: "
                        << clipInitInfo.binPath << " and clip "
                        << clipInitInfo.clipName);
         }

      return retVal;
      }

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    makeVersionClipName
//
// Description: Makes a name for a version clip given the version number
//
// Arguments:   versionNumber   the version number
//
// Returns:     the version name
//
//------------------------------------------------------------------------
string CBinManager::makeVersionClipName(int subVersionNumber)
{
   ClipVersionNumber clipVersionNumber(subVersionNumber);
   return makeVersionClipName(clipVersionNumber);
}

//------------------------------------------------------------------------
//
// Function:    makeVersionClipName
//
// Description: Makes a name for a version clip given the version number
//
// Arguments:   versionNumber   the version number
//
// Returns:     the version name
//
//------------------------------------------------------------------------
string CBinManager::makeVersionClipName(ClipVersionNumber versionNumber)
{
    return versionNumber.ToDisplayName();
}

//------------------------------------------------------------------------
//
// Function:    makeVersionClipName
//
// Description: Makes a name for a version clip given the version clip path
//
// Arguments:   versionClipPath   the version clip path
//
// Returns:     the version name
//
//------------------------------------------------------------------------
string CBinManager::makeVersionClipName(const string &versionClipPath)
{
   ClipVersionNumber versionNumber(versionClipPath);
   return versionNumber.ToDisplayName();
}

//------------------------------------------------------------------------
//
// Function:    makeVersionClipPath
//
// Description: Makes a path for a version clip given the master clip path
//              and the version number
//
// Arguments:   masterClipPath   the master clip path
//              versionNumber    the version number
//
// Returns:     the version clip path
//
//------------------------------------------------------------------------
string CBinManager::makeVersionClipPath(const string &masterClipPath, ClipVersionNumber versionNumber)
{
   return AddDirSeparator(masterClipPath) + versionNumber.ToRelativePath();
}

//------------------------------------------------------------------------
//
// Function:    isVersionClipName
//
// Description: Check if the name indicates the clip is a version clip.
//              Really stupid implementation - just checks that the name
//              starts with "(v".
//
// Arguments:   name    the name to check
//
// Returns:     true if the name is for a version clip
//
//------------------------------------------------------------------------
bool CBinManager::isVersionClipName(const string &name)
{
   ClipVersionNumber versionNumber(name);
   return versionNumber.IsValid();
}

//------------------------------------------------------------------------
//
// Function:    determineNextVersionNumber
//
// Description: Given a bin path + clip name, figures out what the next
//              version number should be. We want the version numbers to
//              be monotonically increasing for each clip. Image-file-based
//              clips have an additional requirement that the path of the
//              media folder must be unique, so if, e.g. two clips point
//              at the same media folder, the version numbers have to be
//              unique across both clips so there may be discontinuities
//              of version in one or both of hte clips (i.e. you might have
//              (v2), (v2) and (v4) in one clip and (v3) in the other clip)
//
// Arguments:   binpath            The bin where versioning is taking place
//              parentClipName     Name of the parent clip
//
// Returns:     The next version number for the clip
//
//------------------------------------------------------------------------
ClipVersionNumber CBinManager::determineNextVersionNumber(const ClipIdentifier &parentClipId)
{
   string parentClipPath = parentClipId.GetClipPath();

   // Determine parent media path if this is an image file clip, so
   // we can ensure uniqueness of the version media folder (which is
   // only a problem if two different parent clips are created pointing
   // to the same media files!)
   int retVal;
   string parentImageFilePath;
	auto parentClip = openClip(parentClipPath, &retVal);

   if (parentClip != nullptr)
   {
		CVideoProxy *videoProxy = parentClip->getVideoProxy(VIDEO_SELECT_NORMAL);
		if (videoProxy != nullptr && videoProxy->isMediaFromImageFile())
		{
			const CMediaStorageList *mediaStorageList = videoProxy->getMediaStorageList();
			CMediaStorage *mediaStorage_0 = mediaStorageList->getMediaStorage(0);

         if (mediaStorage_0 != nullptr)
         {
            string parentMediaIdentifier = mediaStorage_0->getMediaIdentifier();

            parentImageFilePath = RemoveDirSeparator(GetFilePath(parentMediaIdentifier));
         }
      }

      closeClip(parentClip);
   }

   // We want monotonically increasing version numbers - never reuse them
   // Let's start with the number we stashed in the versions.ini!

   string versionsIniPath = AddDirSeparator(parentClipPath) +  versionsIniFileName;
   CIniFile* iniFile = CreateIniFile(versionsIniPath);
   int newSubVersionNumber = 1 + iniFile->ReadInteger(versionInfoSectionName,
                                                versionCountKey, 0);
	ClipVersionNumber newVersionNumber(newSubVersionNumber);

   // In case the ini file was missing or wrong, look for an available number
   bool stillLooking = true;
   while (stillLooking)
	{
		newVersionNumber = ClipVersionNumber(newSubVersionNumber);
		ClipIdentifier versionClipId = parentClipId + newVersionNumber;
		string versionClipName = versionClipId.GetClipName();
      string versionClipPath = versionClipId.GetClipPath();

      if (DoesDirectoryExist(versionClipPath))
      {
         // No dice, that version already exists....  increment and continue
         ++newSubVersionNumber;
      }
      else if (parentImageFilePath.empty())
      {
         // This is not an image file clip, so this version number is usable
         stillLooking = false;
      }
      else
      {
         // Peek at where the media folder should be if this version were
         // created on a different machine attached to this media
         string versionImageFilePath = parentImageFilePath + " " + versionClipName;

         if (DoesDirectoryExist(versionImageFilePath))
         {
            ++newSubVersionNumber; // No dice, increment and keep looking
         }
         else
         {
            stillLooking = false;  // Dice!
         }
      }
   }

   // Remember the highest version number
	iniFile->WriteInteger(versionInfoSectionName, versionCountKey, newSubVersionNumber);

   DeleteIniFile(iniFile);

	return newVersionNumber;
}

//------------------------------------------------------------------------
//
// Function:    createVersionClip
//
// Description: Makes a new version of a clip, where the clip directory
//              of the version is actually nested inside the parent clip
//              directory. This essentially just clones the parent clip,
//              with a couple of modifications, to make the version, with
//              the result being the version clip shares media with the
//              parent clip.
//
// Arguments:   binpath      The bin where versioning is taking place
//              clipName     Name of the parent clip
//              newClipName  [OUTPUT] Name of the new clip
//
// Returns:     0 if successful
//
//------------------------------------------------------------------------
int CBinManager::createVersionClip(ClipIdentifier parentClipId,
                                   /* OUTPUT */ ClipVersionNumber &versionNumberOut)
{
	// Careful - determineNextVersionNumber now returns a number relative to the parent!
	TRACE_3(errout << "CREATE NEW VERSION OF " << parentClipId);
	ClipVersionNumber newVersionNumber = determineNextVersionNumber(parentClipId);
	ClipIdentifier versionClipId = parentClipId + newVersionNumber;
	TRACE_3(errout << "CREATING " << versionClipId);

   // Do the heavy lifting
   int retVal = cloneClip(parentClipId, versionClipId);
   if (retVal != 0)
   {
      TRACE_0(errout << "BinManager::createVersionClip: cloning failed (" << retVal << ")" << endl
                     << "for clip " << versionClipId.GetClipPath());
      return retVal;
   }

   setVersionStatus(parentClipId.GetClipPath(), versionClipId.GetClipName(), versionIsActive);

   versionNumberOut = newVersionNumber;
   return 0;
}

//------------------------------------------------------------------------
//
// Function:    doesClipHaveVersions
//
// Description: Checks if any version clips exist where the subject clip
//              is the original
//
// Arguments:   binpath      The bin
//              clipName     Name of the alleged parent clip
//
// Returns:     true if one or more versions exist
//
//------------------------------------------------------------------------
bool CBinManager::doesClipHaveVersions(const string &binPath,
                                      const string &clipName)
{
   string clipFullPath = AddDirSeparator(binPath) + clipName;
	return getFirstActiveVersionNumber(ClipIdentifier(clipFullPath)).IsValid();
}

//------------------------------------------------------------------------
//
// Function:    doesClipHaveVersions
//
// Description: Checks if any version clips exist where the subject clip
//              is the original
//
// Arguments:   clipId      The clip identifier
//
// Returns:     true if one or more versions exist
//
//------------------------------------------------------------------------
bool CBinManager::doesClipHaveVersions(const ClipIdentifier &clipId)
{
	return getFirstActiveVersionNumber(clipId).IsValid();
}

//------------------------------------------------------------------------
//
// Function:    getFirstActiveVersionNumber
//
// Description: Given a clip path, looks for the first version
//              that is currently active and returns that version number.
//
// Arguments:   clipFullPath   The version parent path (may be a version clip!)
//
// Returns:     The first active version number or an incvalid version number
//              if there are none.
//
//------------------------------------------------------------------------
ClipVersionNumber CBinManager::getFirstActiveVersionNumber(const ClipIdentifier &parentClipId)
{
   ClipVersionNumber invalidVersionNumber;
	return getNextActiveVersionNumber(parentClipId, invalidVersionNumber);
}

//------------------------------------------------------------------------
//
// Function:    getNextActiveVersionNumber
//
// Description: Given a version number, looks for the next higher version
//              that is currently active and returns that version number.
//
// Arguments:   binpath      The bin
//              clipName     Name of the alleged parent clip
//
// Returns:     The next active version number or -1 if there are none.
//
//------------------------------------------------------------------------
ClipVersionNumber CBinManager::getNextActiveVersionNumber(
	const ClipIdentifier &parentClipId,
	ClipVersionNumber previousVersionNumber)
{
	ClipVersionNumber nextActiveVersionNumber;

	string fullIniPath = ::AddDirSeparator(parentClipId.GetClipPath()) + versionsIniFileName;
   if (!::DoesFileExist(fullIniPath))
   {
      // Can't be any versions if there is no versions.ini file.
      return nextActiveVersionNumber;
   }

   CIniFile* iniFile = CreateIniFile(fullIniPath);
   if (iniFile == 0)
   {
      // Uggh, really an error....
      return nextActiveVersionNumber;
   }

   // Read the highest version number from the versions.ini file.
   const int versionCheckLimit = 999;
   int highestVersionNumber = iniFile->ReadInteger(versionInfoSectionName, versionCountKey, versionCheckLimit);
   DeleteIniFile(iniFile);  // closes it, doesn't actually delete it

   if (highestVersionNumber < 1)
   {
      // No versions have been created yet.
      return nextActiveVersionNumber;
   }

	// Linearly scan for the next higher numbered active sibling version.
	// Note that Next() returns version 1 when it's passed an invalid version number.
	for (ClipVersionNumber tmpVersionNumber = previousVersionNumber.Next();
        tmpVersionNumber.GetFinalSubVersionNumber() <= highestVersionNumber;
        tmpVersionNumber = tmpVersionNumber.Next())
	{
		// CAREFUL - since the parent clip may be a version, we need to
		//           concatenate the temp version number (operator+) to
		//           create the correct potential next version clip ID.
		ClipIdentifier tmpClipId = parentClipId + tmpVersionNumber;
		if (isVersionReallyActive(tmpClipId))
		{
			TRACE_3(errout << "FOUND next version clip " << tmpClipId);
			nextActiveVersionNumber = tmpVersionNumber;
         break;
		}

		TRACE_3(errout << "REJECTED next version clip " << tmpClipId);
   }

	return nextActiveVersionNumber;
}

//------------------------------------------------------------------------
//
// Function:    isVersionReallyActive
//
// Description: Given a version name, decides whether the version is really
//              active and is not a zombie. As a side effect, fixes the
//              status value for the version in the master clip if it's wrong..
//
// Arguments:   clipFullPath      master clip path
//              versionNumber     the version to check
//
// Returns:     true if the version is active, else false.
//
//------------------------------------------------------------------------
bool CBinManager::isVersionReallyActive(const ClipIdentifier &clipId)
{
   // Can't be an active version if it isn't even a version clip!
   if (!clipId.IsVersionClip())
   {
      return false;
   }

   string versionsIniPath = ::AddDirSeparator(clipId.GetParentPath()) + versionsIniFileName;
   if (!::DoesFileExist(versionsIniPath))
   {
      // Can't be any versions if there is no versions.ini file.
      return false;
   }

   CIniFile* iniFile = CreateIniFile(versionsIniPath, true);
   if (iniFile == 0)
   {
      // Uggh, really an error....
      return false;
   }

   string versionName = clipId.GetVersionDisplayName();
   string versionStatus = iniFile->ReadString(versionStatusSectionName,
                                              versionName,
                                              invalidVersionStatus);
   DeleteIniFile(iniFile);  // closes it, doesn't actually delete it

   if (versionStatus == versionWasCommitted ||
       versionStatus == versionWasDiscarded)
   {
      return false;
   }

   // NEVER change status A to U !!! For some reason it was getting changed
   // accidentally - it's better to have zombies than to lose a clip!
   if (versionStatus == versionIsActive)
   {
      return true;
   }

   // Status is unknown - let's set it to U or A depending on whether we
   // find a declaration of death or not.
   bool retVal = false;
   {
      CAutoSpinLocker lock(VersionIniAccessLock);

      string versionIniFilePath = ::AddDirSeparator(clipId.GetClipPath()) + versionIniFileName;
      if (!::DoesFileExist(versionIniFilePath))
      {
         // Can't be an active version if there is no version.ini file.
         return false;
      }

      CIniFile *versionIniFile = CreateIniFile(versionIniFilePath, true);
      if (versionIniFile == 0)
      {
         // This is really an error - can't create the ini file!
         return false;
      }

      // Zombie protection - see if the version has been declared dead.
      bool defunct = versionIniFile->ReadBool(statusSectionName, defunctKey, false);
      DeleteIniFile(versionIniFile);

      if (defunct)
      {
         iniFile = CreateIniFile(versionsIniPath);
         if (iniFile != 0)
         {
            // Put the version officially in limbo.
            iniFile->WriteString(versionStatusSectionName,
                                 versionName,
                                 dontKnowWhatHappenedToTheVersion);
         }

         DeleteIniFile(iniFile);
         return false;
      }

      // Assume the clip is alive. Mark it so.
      retVal = true;

      iniFile = CreateIniFile(versionsIniPath);
      if (iniFile != 0)
      {
         iniFile->WriteString(versionStatusSectionName,
                              versionName,
                              versionIsActive);
      }

      DeleteIniFile(iniFile);  // closes it, doesn't actually delete it
   }

   return retVal;
}

////------------------------------------------------------------------------
////
//// Function:    getListOfNamesOfClipsVersions
////
//// Description: Returns a list of the names of all existing versions
////              of a clip.
////
//// Arguments:   clipPath         Full path of the parent clip
////              versionNumberList  StringList of returned names
////              recurse          If true, also return versions of versions
////
//// Returns:     0
////
////------------------------------------------------------------------------
//int CBinManager::getListOfNamesOfClipsVersions(
//   const string &clipPath,
//   StringList &versionNameList,
//   bool recurse,
//   bool depthFirst)
//{
//   versionNameList.clear();
//   ClipVersionNumber versionNumber = getFirstActiveVersionNumber(clipPath);
//
//   while (versionNumber.IsValid())
//   {
//      if (!depthFirst)
//      {
//         versionNameList.push_back(versionNumber.ToDisplayName());
//      }
//
//      if (recurse)
//      {
//         ClipIdentifier subClipId(clipPath, versionNumber);
//         StringList recurseVersionList;
//         getListOfNamesOfClipsVersions(subClipId.GetClipPath(), recurseVersionList, true, depthFirst);
//         for (int i = 0; i < recurseVersionList.size(); ++i)
//         {
//            versionNameList.push_back(recurseVersionList[i]);
//         }
//      }
//
//      if (depthFirst)
//      {
//         versionNameList.push_back(versionNumber.ToDisplayName());
//      }
//
//      versionNumber = getNextActiveVersionNumber(clipPath, versionNumber);
//   }
//
//   int versionNameListCount = versionNameList.size();
//   const char* versionNameList0 = versionNameListCount > 0 ? versionNameList[0].c_str() : "N/A";
//   const char* versionNameList1 = versionNameListCount > 1 ? versionNameList[1].c_str() : "N/A";
//   const char* versionNameList2 = versionNameListCount > 2 ? versionNameList[2].c_str() : "N/A";
//   const char* versionNameList3 = versionNameListCount > 3 ? versionNameList[3].c_str() : "N/A";
//   const char* versionNameList4 = versionNameListCount > 4 ? versionNameList[4].c_str() : "N/A";
//   const char* versionNameList5 = versionNameListCount > 5 ? versionNameList[5].c_str() : "N/A";
//   const char* versionNameList6 = versionNameListCount > 6 ? versionNameList[6].c_str() : "N/A";
//   const char* versionNameList7 = versionNameListCount > 7 ? versionNameList[7].c_str() : "N/A";
//   const char* versionNameList8 = versionNameListCount > 8 ? versionNameList[8].c_str() : "N/A";
//   const char* versionNameList9 = versionNameListCount > 9 ? versionNameList[9].c_str() : "N/A";
//
//   return 0;
//}

//------------------------------------------------------------------------
//
// Function:    getListOfClipsActiveVersionNumbers
//
// Description: Returns a list of the names of all existing versions
//              of a clip.
//
// Arguments:   clipPath         Full path of the parent clip
//              versionNameList  StringList of returned names
//              recurse          If true, also return versions of versions
//
// Returns:     0
//
//------------------------------------------------------------------------
void CBinManager::getListOfClipsActiveVersionNumbers(
	const ClipIdentifier &clipId,
	vector<ClipVersionNumber> &versionNumberList,
   bool recurse,
   bool depthFirst)
{
   versionNumberList.clear();
	ClipVersionNumber versionNumber = getFirstActiveVersionNumber(clipId);

	while (versionNumber.IsValid())
   {
      if (!depthFirst)
      {
         versionNumberList.push_back(versionNumber);
			TRACE_3(errout << "PRE-ADDED VERSION TO ACTIVE LIST: " << (clipId + versionNumber));
		}

      if (recurse)
		{
			ClipIdentifier subClipId = clipId + versionNumber;
			vector<ClipVersionNumber> recurseVersionList;
			getListOfClipsActiveVersionNumbers(subClipId, recurseVersionList, true, depthFirst);
			for (int i = 0; i < recurseVersionList.size(); ++i)
			{
				versionNumberList.push_back(versionNumber + recurseVersionList[i]);
				TRACE_3(errout << "RECURSIVELY ADDED VERSION TO ACTIVE LIST: " << (clipId + (versionNumber + recurseVersionList[i])));
			}
      }

      if (depthFirst)
      {
         versionNumberList.push_back(versionNumber);
			TRACE_3(errout << "POST-ADDED VERSION TO ACTIVE LIST: " << (clipId + versionNumber));
		}

		versionNumber = getNextActiveVersionNumber(clipId, versionNumber);
   }

   int versionNumberListCount = versionNumberList.size();
	const string versionNumberList0 = (versionNumberListCount > 0) ? versionNumberList[0].ToDisplayName() : "N/A";
	const string versionNumberList1 = (versionNumberListCount > 1) ? versionNumberList[1].ToDisplayName() : "N/A";
	const string versionNumberList2 = (versionNumberListCount > 2) ? versionNumberList[2].ToDisplayName() : "N/A";
	const string versionNumberList3 = (versionNumberListCount > 3) ? versionNumberList[3].ToDisplayName() : "N/A";
	const string versionNumberList4 = (versionNumberListCount > 4) ? versionNumberList[4].ToDisplayName() : "N/A";
	const string versionNumberList5 = (versionNumberListCount > 5) ? versionNumberList[5].ToDisplayName() : "N/A";
	const string versionNumberList6 = (versionNumberListCount > 6) ? versionNumberList[6].ToDisplayName() : "N/A";
	const string versionNumberList7 = (versionNumberListCount > 7) ? versionNumberList[7].ToDisplayName() : "N/A";
	const string versionNumberList8 = (versionNumberListCount > 8) ? versionNumberList[8].ToDisplayName() : "N/A";
	const string versionNumberList9 = (versionNumberListCount > 9) ? versionNumberList[9].ToDisplayName() : "N/A";

	TRACE_3(errout << "[" << versionNumberList0 << "] [" << versionNumberList1 << "] [" << versionNumberList2 << "] [" << versionNumberList3
						<< "] [" << versionNumberList4 << "] [" << versionNumberList5 << "] [" << versionNumberList6 << "] [" << versionNumberList7
						<< "] [" << versionNumberList8 << "] [" << versionNumberList9 << "]");
}

//------------------------------------------------------------------------
//
// Function:    getRealBinPath
//
// Description: Given a clip path, finds the path to the bin containing
//              base clip for the clip (which of course will be the clip
//              if the clip is not a version clip).
//
// Arguments:   clipPath         Full path of the version clip
//
// Returns:     the real bin path
//
//------------------------------------------------------------------------
string CBinManager::getRealBinPath(const string &clipPath)
{
   string path(clipPath);

   // These clip names are so frickin' confusing... I'm not sure
   // isClipDir() is the right test here
   while ((!path.empty()) && isClipDir(path))
   {
      string left, right;

      splitClipFileName(path, left, right);
      path = left;
   }
   if (path.empty())
   {
      // hack - return first root bin
      StringList roots;

      CPMPGetBinRoots(&roots);
      path = roots[0];          // there is only one
   }

   return path;
}

//------------------------------------------------------------------------
//
// Function:    getMasterClipPath
//
// Description: Given a version clip path, returns the path of the master
//              clip. Given a non-version clip, just returns the argument.
//
// Arguments:   clipPath         Full path of the version clip
//
// Returns:     the master clip path
//
//------------------------------------------------------------------------
string CBinManager::getMasterClipPath(const string &clipPath)
{
//   string path;
//   string tmpPath(clipPath);
//
//   // These clip names are so frickin' confusing... I'm not sure
//   // isClipDir() is the right test here
//   while ((!tmpPath.empty()) && isClipDir(tmpPath))
//   {
//      string left, right;
//
//      path = tmpPath;
//      splitClipFileName(tmpPath, left, right);
//      tmpPath = left;
//   }
//
//   return path;
   ClipIdentifier clipId(clipPath);
   return clipId.GetMasterClipPath();
}

////------------------------------------------------------------------------
////
//// Function:    getConsolidatedVersionNumber
////
//// Description: Given a version clip path, returns a version name with all
////              the nested version names combined into one. If the path does
////              not point to a version clip, returns an empty string.
////              for example, the input "\bin\masterclip\(v1)\(v2)\(v3)" would
////              return the version number 1.2.3 .
////
//// Arguments:   clipPath         Full path of the version clip
////
//// Returns:     the consolidated version number
////
////------------------------------------------------------------------------
//ClipVersionNumber CBinManager::getConsolidatedVersionNumber(const string &clipPath)
//{
//   string path(clipPath);
//   ClipVersionNumber consolidatedVersionNumber;
//
//   while ((!path.empty()) && isClipDir(path))
//   {
//      string name;
//      string parentPath;
//      splitClipFileName(path, parentPath, name);
//
//      if (isVersionClipName(name))
//      {
//         ClipVersionNumber versionNumber(name);
//
//         // Need to put this subversion number in front of the previous ones!
//         consolidatedVersionNumber = versionNumber + consolidatedVersionNumber;
//      }
//
//      path = parentPath;
//   }
//
//   return consolidatedVersionNumber;
//}

//------------------------------------------------------------------------
//
// Function:    cloneClip
//
// Description: Makes a copy of a clip, with a new name. Media is shared
//              between the two clips. In the new clip, the media is
//              marked "virtual" and "write protected" so we won't modify
//              in place, rather there is an elaborate hack in place to
//              allocate storage at another "level" to write to. The idea
//              is that the clone and the clonee share all media until
//              a clone frame is written to, at which point media belonging
//              only to the clone is allocated for that frame.
//
// Arguments:   binpath      The bin where cloning is taking place
//              clipName     Name of the original clip
//              newClipName  Name of the new clip
//
// Returns:     0 if successful
//
//------------------------------------------------------------------------
int CBinManager::cloneClip(const ClipIdentifier &sourceClipId, const ClipIdentifier &cloneClipId)
{
   static char const *func = "CBinManager::cloneClip";
   CAutoErrorReporter autoErr("CBinManager::cloneClip", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
   int retVal;
   int i;

   string newClipParentPath = cloneClipId.GetParentPath();
   string newClipName = cloneClipId.GetClipName();
   string originalClipParentPath = sourceClipId.GetParentPath();
   string originalClipName = sourceClipId.GetClipName();

   // See if we should reject the name (but no check if it's a valid version name!)
   string badChars = ".\\/:*?\"<>|#";
   ClipVersionNumber versionNumber(newClipName);
   if ((!cloneClipId.GetVersionNumber().IsValid()) && (newClipName.find_first_of(badChars) != string::npos))
   {
      autoErr.errorCode = CLIP_ERROR_INVALID_CLIP_NAME_CHARACTERS;
      autoErr.msg << "A clip name cannot contain any of the following characters: " << endl << "    " << badChars;
      return autoErr.errorCode;
   }

   // Check that the original clip exists
   string originalClipDir = sourceClipId.GetClipPath();
   if (!DoesDirectoryExist(originalClipDir))
   {
      // Larry says this check bogusly fails intermittently, so wait 0.5 sec
      // and try try again
      MTImillisleep(500);
      if (!DoesDirectoryExist(originalClipDir))
      {
         // Maybe 3rd time's a charm?
         MTImillisleep(500);
         if (!DoesDirectoryExist(originalClipDir))
         {
            autoErr.errorCode = CLIP_ERROR_CLIP_CLONE_SOURCE_DOES_NOT_EXIST;
            autoErr.msg << "Source clip " << originalClipDir << " does not exist";
            return autoErr.errorCode;
         }
      }
   }

   // QQQ not needed because full IDs are now passed in!
   // Clones are now nested INSIDE the parent clip, so parent clip
   // becomes a virtual bin
   newClipParentPath = originalClipDir;

   // Check that clip directory does not already exist
   string newClipDir = cloneClipId.GetClipPath();
   if (DoesDirectoryExist(newClipDir))
   {
      // ERROR: Clip directory already exists in the Bin,
      // so cannot create a new clip with the same name
      autoErr.errorCode = CLIP_ERROR_CLIP_CLONE_TARGET_ALREADY_EXISTS;
      autoErr.msg << "Clip directory " << cloneClipId.GetClipName() << endl << "       already exists in Bin " << newClipParentPath;
      return autoErr.errorCode;
   }

   //// HACK ALERT //// HACK ALERT//// HACK ALERT //// HACK ALERT////
   //
   // Catch-22... in order to rename the stuff in the clip directory
   // we need to have the clip open. But in order to open the clip, we need
   // to rename some stuff. Sucks that these frickin' file names have the
   // clip name embedded in them!!
   //
   // So I need to:
   // (a) create a fake temporary bin
   // (b) copy the original clip to it without changing anything
   // (c) open and rename the clip
   // (d) copy the clip back to this bin
   // (e) destroy the temp bin
   //
   // Oh, yeah, this whole business of just blindly copying all the clip
   // files to create a new clip is a pretty nasty hack itself...
   // WARNING: the method used to copy those files does NOT RECURSE,
   // so if the clip ever starts needing stuff in subdirectories
   // this may break the hack! As this time, the only thing that
   // goes in a subdirectory is the recording history stuff,
   // which we don't care about.
   //
   //// HACK ALERT //// HACK ALERT//// HACK ALERT //// HACK ALERT////

   // Create the directory for the temp bin
   // CAREFUL!! MakeHiddenDirectory() returns 'true' on success NOT 0
   string tempBinDirName = AddDirSeparator(newClipParentPath) + "." + newClipName;
   retVal = MakeHiddenDirectory(tempBinDirName) ? 0 : -1;
   if (retVal != 0)
   {
      // ERROR: Could not create the temp bin
      // Just continue, maybe it already exists and we'll be OK
   }

   // Create a new clip directory in the temp bin - call it the same name
   // as the ORIGINAL clip
   retVal = createDirectory(tempBinDirName, originalClipName);
   if (retVal != 0)
   {
      // ERROR: Could not create the temp clip directory
      // QQQ CLEAN UP
      autoErr.RepeatTheError();
      return autoErr.errorCode;
   }

   // Copy all the regular files in the directory
   // CAREFUL!! CopyAllRegularFiles returns number of files copied,
   // or -1 on error !!
   string reason;
   string tempClipDir = AddDirSeparator(tempBinDirName) + originalClipName;
   StringList excludeList =
   {"versions.ini", "version.ini"};

   retVal = CopyAllRegularFiles(originalClipDir, tempClipDir, false, true, excludeList, reason);
   // Larry says this sometimes fails, so retry up to 9 times
   for (int retries = 0; retries < 9 && retVal < 0; ++retries)
   {
      MTImillisleep(100); // wait 1/10th sec before retrying
      retVal = CopyAllRegularFiles(originalClipDir, tempClipDir, false, true, excludeList, reason);
   }

   if (retVal < 0)
   {
      // ERROR: Could not copy the clip's files
      // QQQ CLEAN UP
      autoErr.errorCode = CLIP_ERROR_CLIP_CLONE_FILE_COPY_FAILED;
      autoErr.msg << "Temp directory " << tempClipDir << endl << "       Copy failed because: " << reason;
      return autoErr.errorCode;
   }

   // Generate a new GUID for the cloned clip so we don't confuse it with
   // the original clip - must do this BEFORE opening the clip!
   CClip::changeClipGUID(tempBinDirName, originalClipName);

   // Now rename the tmp clip to be the new clip
   retVal = renameClip(tempBinDirName, originalClipName, newClipParentPath, newClipName,
         /* renameMediaFlag= */ false);
   if (retVal != 0)
   {
      // ERROR: Could not rename the temp clip to be the new clip
      // QQQ CLEAN UP
      autoErr.RepeatTheError();
      TRACE_0(errout << "ERROR: " << func << ": Could't rename " << "temp clip (" << retVal << ")");
      return autoErr.errorCode;
   }

   // OK to remove the temp directory now
   RemoveDirectory(tempBinDirName.c_str());

   // Now open the new clip so we can diddle some properties
   string newClipPath = AddDirSeparator(newClipParentPath) + newClipName;
   auto newClip = openClip(newClipPath, &retVal);
   if (retVal != 0)
   {
      // ERROR: Could not open the new clip
      autoErr.RepeatTheError();
      TRACE_0(errout << "ERROR: " << func << ": New clip " << newClipPath << " open failed (" << retVal << ")");
      return autoErr.errorCode;
   }

   ////////////////////////////////////////////////////////////////////////
   // We need to figure out where we will write "dirty" frames.
   // If media storage location 0 is a videodisk, we will write dirty
   // frames to that. But if it's image files, we will create a new
   // folder to write the dirty frames to, and we will add a corresponding
   // Media Storage. As a convention, we will call the folder the same name
   // as the folder indicated in the media identifier for media storage
   // location 0, except we will tack on the same version string
   // that we used to differentiate the clip names (_V1, _V2, ...). The
   // files in the folder will be named exactly the same as the files
   // they are replacing in the source folder. Then we set the "dirty
   // media storage index" to identify the new media storage.
   ////////////////////////////////////////////////////////////////////////

   CVideoProxy *videoProxy = newClip->getVideoProxy(VIDEO_SELECT_NORMAL);
   if (videoProxy->isMediaFromImageFile())
   {
      const CMediaStorageList *mediaStorageList = videoProxy->getMediaStorageList();
      CMediaStorage *mediaStorage_0 = mediaStorageList->getMediaStorage(0);
      if (mediaStorage_0 == nullptr)
      {
         // This is not good
         closeClip(newClip);
         autoErr.errorCode = CLIP_ERROR_UNEXPECTED_NULL_RETURN;
         autoErr.msg << "Can't get media storage 0";
         return autoErr.errorCode;
      }

      // ############## HACK ALERT #############################
      // I stole this code from the old MainWindowUnit render destination
      // stuff. It is a hideous hack where we perform surgery on the
      // image file media identifier, which should be opaque to us.
      //
      string srcMediaIdentifier = mediaStorage_0->getMediaIdentifier();
      string srcImageFilePath = RemoveDirSeparator(GetFilePath(srcMediaIdentifier));
      string srcImageFileTemplate = GetFileName(srcMediaIdentifier) + GetFileExt(srcMediaIdentifier);
      string srcHistoryDirectory = RemoveDirSeparator(mediaStorage_0->getHistoryDirectory());
      string srcMetadataDirectory = RemoveDirSeparator(GetFileAllButLastDir(AddDirSeparator(srcHistoryDirectory)));
//      string srcAnalysisDirectory = AddDirSeparator(srcMetadataDirectory) + "Analysis";

      string dstImageFilePath;
      string dstMediaIdentifier;
      string dstMetaDataDirectory;
      string dstHistoryDirectory;
//      string dstAnalysisDirectory;
      string versionSuffix;

      // string consolidatedVersionName = makeVersionClipName(getConsolidatedVersionNumber(newClipPath));
      // if (!consolidatedVersionName.empty())
      ClipIdentifier clipId(newClipPath);
      string versionName = clipId.GetVersionDisplayName();
      if (!versionName.empty())
      {
         versionSuffix = string(" ") + versionName;
      }

      // WAAAY too many assumptions here! QQQ
      string baseImageFilePath = srcImageFilePath;
      size_t pos = baseImageFilePath.rfind(" (v");
      if (pos != string::npos)
      {
         baseImageFilePath = baseImageFilePath.substr(0, pos);
      }

      dstImageFilePath = baseImageFilePath + versionSuffix;

      // Careful! path up to final folder must exist or createDirectory fails!!
      retVal = createDirectory(dstImageFilePath);
      if (retVal != 0)
      {
         // QQQ UNWIND
         closeClip(newClip);
         deleteClip(newClipParentPath, newClipName);
         autoErr.RepeatTheError();
         return autoErr.errorCode;
      }

      // HACK: Remember the dirty media folder path so we can easily remove
      // it when the version clip is destroyed
      setDirtyMediaFolderPath(newClipPath, dstImageFilePath);

      // the MediaLocation is the full image file template
      dstMediaIdentifier = AddDirSeparator(dstImageFilePath) + srcImageFileTemplate;

      //////////////////////////////////////////////////////////////////////////

      if (!srcMetadataDirectory.empty())
      {
         dstMetaDataDirectory = srcMetadataDirectory + versionSuffix;
         if (!DoesDirectoryExist(dstMetaDataDirectory))
         {
            // Careful! Stupid MakeDirectory returns bool not int!!!!
            bool success = MakeDirectory(dstMetaDataDirectory);
            if (!success)
            {
               closeClip(newClip);
               deleteClip(newClipParentPath, newClipName);
               autoErr.RepeatTheError();
               return autoErr.errorCode;
            }
         }

         dstHistoryDirectory = AddDirSeparator(dstMetaDataDirectory) + "History";
//         dstAnalysisDirectory = AddDirSeparator(dstMetaDataDirectory) + "Analysis";
      }

      if (!dstHistoryDirectory.empty())  // empty => legacy parent, MONO history
      {
         // Careful! Stupid MakeDirectory returns bool not int!!!!
         bool success = MakeDirectory(dstHistoryDirectory);
         if (!success)
         {
            closeClip(newClip);
            deleteClip(newClipParentPath, newClipName);
            autoErr.RepeatTheError();
            return autoErr.errorCode;
         }
      }

//      // Make Analysis folder, and copy
//      if (!dstAnalysisDirectory.empty())
//      {
//         // Careful! Stupid MakeDirectory returns bool not int!!!!
//         bool success = MakeDirectory(dstAnalysisDirectory);
//         if (!success)
//         {
//            closeClip(newClip);
//            deleteClip(newClipParentPath, newClipName);
//            autoErr.RepeatTheError();
//            return autoErr.errorCode;
//         }
//      }

      // HACK: Remember the dirty history folder path so we can
      // easily remove it when the version clip is destroyed
      setDirtyHistoryFolderPath(newClipPath, dstHistoryDirectory);

      // Build a media access object for the media storage
      EMediaType mediaType = mediaStorage_0->getMediaType();
      const CImageFormat *imageFormat = videoProxy->getImageFormat();
      CMediaInterface mediaInterface;
      CMediaAccess *mediaAccess = mediaInterface.registerMediaAccess(mediaType, dstMediaIdentifier, dstHistoryDirectory, *imageFormat);
      // Add the new media storage to the list for this proxy
      videoProxy->addDirtyMediaStorage(mediaStorage_0->getMediaType(), dstMediaIdentifier, dstHistoryDirectory, mediaAccess);

   } // end elaborate hack to add a media storage for dirty image files
   else
   {
      // Image repository media?
      CClipInitInfo clipInitInfo;
      clipInitInfo.initFromClip(newClip);

      if (clipInitInfo.doesNormalVideoExist() && CMediaInterface::isMediaTypeImageRepository
            (clipInitInfo.mainVideoInitInfo->getMediaType()))
      {
         const CMediaStorageList *mediaStorageList = videoProxy->getMediaStorageList();
         CMediaStorage *mediaStorage_0 = mediaStorageList->getMediaStorage(0);
         if (mediaStorage_0 == nullptr)
         {
            // This is not good
            closeClip(newClip);
            autoErr.errorCode = CLIP_ERROR_UNEXPECTED_NULL_RETURN;
            autoErr.msg << "Can't get media storage 0";
            return autoErr.errorCode;
         }

         // ############## HACK ALERT #############################
         // Yet another hideous hack where we perform surgery on the
         // image file media identifier, which should be opaque to us.
         // Here we want to change the media storage item 0 to point
         // to the parent media - we do this by changing the symbolic
         // variable ${PATH} to ${PARENT} because the parent clip is the
         // virtual bin for the version clip.
         //
         string srcMediaIdentifier = mediaStorage_0->getMediaIdentifier();
         string srcHistoryDirectory = mediaStorage_0->getHistoryDirectory();
         string dstMediaIdentifier = srcMediaIdentifier;
         string dstHistoryDirectory = srcHistoryDirectory;

         EMediaType mediaType = mediaStorage_0->getMediaType();

         // Reuse the existing media access object for the destination
         CMediaAccess *dstMediaAccess(mediaStorage_0->getMediaAccessObj());

         const string PATH_VAR = "@{PATH=";
         const string PARENT_VAR = "@{PARENT=";
         string::size_type pos = srcMediaIdentifier.find(PATH_VAR);
         if (pos != string::npos)
         {
            // Holy crap, this is a way worse hack than I thought it would be!
            srcMediaIdentifier.replace(pos, PATH_VAR.size(), PARENT_VAR);
            string::size_type pos2 = srcMediaIdentifier.find("}", pos);
            if (pos2 != string::npos)
            {
               string::size_type pos3 = srcMediaIdentifier.rfind("\\", pos2);
               if (pos3 != string::npos)
               {
                  // rub out the version clip name portion of the path
                  srcMediaIdentifier.erase(pos3, pos2 - pos3);

                  // Build a media access object for the parent media storage
                  const CImageFormat *imageFormat = videoProxy->getImageFormat();
                  CMediaInterface mediaInterface;
                  CMediaAccess *srcMediaAccess = mediaInterface.registerMediaAccess(mediaType, srcMediaIdentifier, srcHistoryDirectory,
                        *imageFormat);

                  //////////////REALLY REALLY AWFUL HACK ALERT//////////////
                  // We're now going to reconstruct the clip's storage list;
                  // this is not exactly kosher & steps WAY over the line!!!
                  CMediaStorageList *nonConstMediaStorageList = const_cast<CMediaStorageList*>(mediaStorageList);
                  nonConstMediaStorageList->clear();
                  nonConstMediaStorageList->addMediaStorage(mediaType, srcMediaIdentifier, srcHistoryDirectory, srcMediaAccess);

                  newClip->writeMediaStorageSection(VIDEO_SELECT_NORMAL, nonConstMediaStorageList);
                  //
                  //////////////REALLY REALLY AWFUL HACK ALERT//////////////
               }
            }
         }

         // Add the new media storage to the list for this proxy
         videoProxy->addDirtyMediaStorage(mediaType, dstMediaIdentifier, dstHistoryDirectory, dstMediaAccess);
      }

   } // end second hack to add a media storage for dirty image files

   // Remember which clip we were cloned from, so we know where to
   // copy back modified frames for "commit version"
   // QQQ OFFSET WILL ALWAYS BE 0 - SHOULD BE REMOVED??
   newClip->setRenderInfo(originalClipDir, 0, FRAMING_SELECT_VIDEO);

   // Set all video proxies to virtual and bump write protect level
   int count = newClip->getVideoProxyCount();
   for (i = 0; i < count; ++i)
   {
      videoProxy = newClip->getVideoProxy(i);
      if (videoProxy == nullptr)
      {
         continue;
      }

      // Set track-level "virtual media" flag, because the media is mostly
      // virtual, unless dirty....
      // TBD - do we have to do anything to make the "remaining time" stuff
      // work?
      videoProxy->setVirtualMediaFlag(true);

      // TBD: Make everything really readonly except the main video proxy

      // Set field-level "virtual media" and "write protected" flags
      // and clear the "dirty" flag for each field in the track

      // Get the frame list
      // Our intention here is to diddle EVERY frame in the frame list...
      // we assume FRAMING_SELECT_VIDEO gives us every frame
      CVideoFrameList *frameList = videoProxy->getVideoFrameList(FRAMING_SELECT_VIDEO);
      assert(frameList != nullptr);

      // Iterate over frames
      int frameCount = frameList->getTotalFrameCount();
      for (int frameIndex = 0; frameIndex < frameCount; ++frameIndex)
      {
         CVideoFrame *frame = frameList->getFrame(frameIndex);
         assert(frame != nullptr);

         // Set flags for each field in the frame
         int fieldCount = frame->getTotalFieldCount();
         for (int fieldIndex = 0; fieldIndex < fieldCount; ++fieldIndex)
         {
            CVideoField *field = frame->getField(fieldIndex);
            assert(field != nullptr);
            field->setBorrowedMedia(true);
            field->setWriteProtectedMedia(true);
            field->setDirtyMedia(false);

         } // for each field in the frame

      } // for each frame in the frame list

   } // for each video proxy in the clip

   // Dop the same for the audio tracks
   // QQQ Our immediate intention is that audio should never be modified
   // in the version clip - need to hack something in for that?
   count = newClip->getAudioTrackCount();
   for (i = 0; i < count; ++i)
   {
      CAudioTrack *audioTrack = newClip->getAudioTrack(i);
      if (audioTrack != nullptr)
      {
         // Set track-level "virtual media" flag, which prevents us from
         // deleting media owned by parent versions of this clip
         audioTrack->setVirtualMediaFlag(true);
      }
   }

   // This is stupid. To avoid a warning popup about someone changing the .clp
   // behind our backs, need to do this before AND after the calls to
   // update all tracks, which shouldn't be writing the .clp file, but they do.
   newClip->setCreateDateTime(createDateAndTimeString());

   // Clone gets a new create date and description and special status
   newClip->updateAllTracksToCreatedStatusAndClearArchiveDate(newClipParentPath, newClipName);
   newClip->updateAllTracksToClonedStatus(originalClipParentPath, originalClipName);

   // Override stuff that's different from the [arent and overwrite various files.
   newClip->setCreateDateTime(createDateAndTimeString());
   newClip->setDescription(""); // Not inherited!
   newClip->writeFile(); // Need to flush clip file explicitly?
   newClip->updateAllTrackFiles(); // Need to flush field lists explicitly?
   // don't need to flush video frame lists, they didn't change

   closeClip(newClip);

   return retVal;

} // cloneClip
//------------------------------------------------------------------------

namespace
{
	// Utility function to get the "dirty media" MediaAccess object for the
	// specified video proxy. Used to check for clip corruption instead of
	// blindly delting "dirty" media files.
	CMediaAccess *getDirtyFieldMediaAccess(CVideoProxy *videoProxy)
	{
		if (videoProxy == nullptr)
		{
         // "Can't happen."
			return nullptr;
		}

		// Get the MediaStorage that indicates where dirty frames go.
		const CMediaStorageList *mediaStorageList = videoProxy->getMediaStorageList();
		int dirtyMediaStorageIndex = mediaStorageList->getDirtyMediaStorageIndex();
		if (dirtyMediaStorageIndex < 0)
		{
			// There isn't one. Apparently this isn't a version clip.
			return nullptr;
		}

		const CMediaStorage *dirtyMediaStorage = mediaStorageList->getMediaStorage(dirtyMediaStorageIndex);
		if (dirtyMediaStorage == nullptr)
		{
			// "Can't happen."
			return nullptr;
		}

		return dirtyMediaStorage->getMediaAccessObj();
	}
};

//------------------------------------------------------------------------
//
// Function:    destroyClipDirtyMedia
//
// Description: Looks through all the media in the clip and destroys
//              the media for the fields labeled as "dirty"
//
// Arguments:   binpath      The bin where destroying is taking place
//              clipName     Name of the clip whose dirty media is to be
//                           destroyed
//              startFrame   Index of first frame to check (default 0)
//              frameCount   Number of frames to check, or -1 for "all
//                           remaining frames" (default -1)
//
// Returns:     0 if successful
//
//------------------------------------------------------------------------
int CBinManager::destroyClipDirtyMedia(const string &binPath,
                                       const string &clipName)
{
	int retVal;
	const string func("CBinManager::destroyClipDirtyMedia");

	// Open the clip whose dirty media we wish to destroy
	string fullClipPath = AddDirSeparator(binPath) + clipName;
	auto clip = openClip(fullClipPath, &retVal);
	if (retVal != 0)
	{
		// ERROR: Could not open the new clip
		TRACE_0(errout << "ERROR: " << func << ": clip " << fullClipPath << " open failed (" << retVal << ")");
		return retVal;
	}

	// Set all video proxies to virtual and bump write protect level
	int count = clip->getVideoProxyCount();
	int i;
	for (i = 0; i < count; ++i)
	{
		CVideoProxy *videoProxy = clip->getVideoProxy(i);
		if (videoProxy == nullptr)
		{
			continue;
		}

		// Get the frame list
		// Our intention here is to examine EVERY frame in the frame list...
		// we assume FRAMING_SELECT_VIDEO gives us every frame
		CVideoFrameList *frameList = videoProxy->getVideoFrameList(FRAMING_SELECT_VIDEO);
		assert(frameList != nullptr);

		// Iterate over frames
		int frameCount = frameList->getTotalFrameCount();
		for (int frameIndex = 0; frameIndex < frameCount; ++frameIndex)
		{
			CVideoFrame *frame = frameList->getFrame(frameIndex);
			assert(frame != nullptr);

			// Destroy media for each field in the frame
			int fieldCount = frame->getTotalFieldCount();
			for (int fieldIndex = 0; fieldIndex < fieldCount; ++fieldIndex)
			{
				CVideoField *field = frame->getField(fieldIndex);
				MTIassert(field != nullptr);
				if (field->isDirtyMedia())
				{
					// Because of bad design, track file and field list file may get
					// out of sync, so we may override the MediaAccess to make sure
					// it's pointing at the dirty media storage area.
					CMediaAccess *mediaAccessOverride = getDirtyFieldMediaAccess(videoProxy);
					field->destroyMedia(mediaAccessOverride);
				}
			} // for each field in the frame
		} // for each frame in the frame list
	} // for each video proxy in the clip

	// Dop the same for the audio tracks
	// QQQ Our immediate intention is that audio should never be modified
	// in the version clip - need to hack something in for that?
	count = clip->getAudioTrackCount();
	for (i = 0; i < count; ++i)
	{
		CAudioTrack *audioTrack = clip->getAudioTrack(i);
		if (audioTrack != nullptr)
		{
			// Set track-level "virtual media" flag, which prevents us from
			// deleting media owned by parent versions of this clip
			audioTrack->setVirtualMediaFlag(true);
		}
	}

	// Flush stuff immediately for robustness (in case someone else has the
	// clip open)
	clip->writeFile();
	clip->updateAllTrackFiles();
	clip->updateAllVideoFrameListFiles();

	closeClip(clip);

	// Success
	return 0;
}

//------------------------------------------------------------------------
//
// Function:    deleteClip
//
// Description: Delete an existing Clip given its Bin path and its name.
//              The Clip is closed if it was present in the BinManager's
//              Open Clip List
//
// Arguments:   string& newBinPath
//              string& newClipName
//
// Returns:     0 if success, non-zero otherwise
//
//------------------------------------------------------------------------
int CBinManager::deleteClip(const string& binPath, const string& clipName)
{
   int status;

   auto clip = openClip(binPath, clipName, &status);
   if (status != 0)
      return status;

   status = clip->deleteClipFile();
   if (status != 0)
      return status;

   closeClip(clip);

   lastDeletedClipName = AddDirSeparator(binPath) + clipName;
	ClipWasDeletedEvent.SetAllData(this);
   ClipWasDeletedEvent.Notify();

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    deleteClipOrClipIni
//
// Description: Delete an existing Clip or Clip Initialization file
//              given its Bin path and its name.  The function first
//              determines if a regular Clip or only a Clip Ini file
//              exists.  If a regular Clip exists, the function deleteClip
//              is used to delete it.
//              The Clip is closed if it was present in the BinManager's
//              Open Clip List
//
// Arguments:   string& newBinPath
//              string& newClipName
//
// Returns:     0 if success, non-zero otherwise
//
//------------------------------------------------------------------------
int CBinManager::deleteClipOrClipIni(const string& binPath,
                                     const string& clipName)
{
   int status;

   // Open the clip or clip initialization and return a new CClipInitInfo
   // instance pointer.  CClipInitInfo instance pointer is assigned
   // to an "auto_ptr" which automatically deletes the CClipInitInfo
   // instance when clipInitInfo goes out-of-scope.  This makes error
   // handling much more concise
   std::auto_ptr<CClipInitInfo> clipInitInfo(openClipOrClipIni(binPath, clipName,
                                                            &status));
   if (status != 0)
      return status;

   EClipInitSource clipInitSource = clipInitInfo->getClipInitSource();
   if (clipInitSource == CLIP_INIT_SOURCE_CLIP)
      {
      auto clip = clipInitInfo->getClipPtr();
      status = clip->deleteClipFile();
      if (status != 0)
         return status;

      clipInitInfo->closeClip();

      lastDeletedClipName = AddDirSeparator(binPath) + clipName;
	   ClipWasDeletedEvent.SetAllData(this);
      ClipWasDeletedEvent.Notify();
      }

   else if (clipInitSource == CLIP_INIT_SOURCE_CLIP_INI)
      {
      status = deleteClipIniFile(binPath, clipName);
      if (status != 0)
         return status;
      }

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    archiveClip
//
// Description: Archive an existing Clip given its Bin path and its name.
//              The Clip is closed if it was present in the BinManager's
//              Open Clip List
//
// Arguments:   string& newBinPath
//              string& newClipName
//
// Returns:     0 if success, non-zero otherwise
//
//------------------------------------------------------------------------
int CBinManager::archiveClip(const string& binPath, const string& clipName)
{
   int status;

// TTT - Just return for now
   return 0;
}

//------------------------------------------------------------------------
//
// Function:    freeMedia
//
// Description: Free the reserved media of the specified type from a clip
//              given its Bin path and its name.
//
// Arguments:   string& binPath
//              string& clipName
//              int imageOrAudio
//
// Returns:     0 if success, non-zero otherwise
//
//------------------------------------------------------------------------
int CBinManager::freeMedia(const string& binPath,
                           const string& clipName,
                           ETrackType trackType)
{
   int status;

   // Open the clip or clip initialization and return a new CClipInitInfo
   // instance pointer.  CClipInitInfo instance pointer is assigned
   // to an "auto_ptr" which automatically deletes the CClipInitInfo
   // instance when clipInitInfo goes out-of-scope.  This makes error
   // handling much more concise
   std::auto_ptr<CClipInitInfo> clipInitInfo(openClipOrClipIni(binPath, clipName,
                                                            &status));
   if (status != 0)
      return status;

   EClipInitSource clipInitSource = clipInitInfo->getClipInitSource();
   if (clipInitSource == CLIP_INIT_SOURCE_CLIP)
      {
      auto clip = clipInitInfo->getClipPtr();
      switch (trackType)
          {
          case TRACK_TYPE_VIDEO:
              status = clip->freeClipVideo();
              break;
          case TRACK_TYPE_AUDIO:
              status = clip->freeClipAudio();
              break;
          default:
              assert(0);
              status = -1;
              break;
          }

      clipInitInfo->closeClip();   // no longer need opened clip
      }

   return status;
}

//------------------------------------------------------------------------
//
// Function:    renameClip
//
// Description: Rename a clip
//              given its Bin path and its name.
//
// Arguments:   string& binPath
//              string& clipName
//
// Returns:     0 if success, non-zero otherwise
//
//------------------------------------------------------------------------
int CBinManager::renameClip(const string& binPath,
                            const string& clipName,
                            const string& newBinPath,
                            const string& newClipName,
                            bool renameMediaFlag
                           )
{
   int status;

   ClipVersionNumber versionNumber(newClipName);
   string badChars = ".\\/:*?\"<>|#";
   if ((!versionNumber.IsValid())
   && (newClipName.find_first_of(badChars) != string::npos))
   {
      MTIostringstream ostrm;
      ostrm << "A clip name cannot contain any of the following characters: " << endl
            << "    " << badChars;
      theError.set(CLIP_ERROR_INVALID_CLIP_NAME_CHARACTERS, ostrm);

      return CLIP_ERROR_INVALID_CLIP_NAME_CHARACTERS;
   }

   // Open the clip or clip initialization and return a new CClipInitInfo
   // instance pointer.  CClipInitInfo instance pointer is assigned
   // to an "auto_ptr" which automatically deletes the CClipInitInfo
   // instance when clipInitInfo goes out-of-scope.  This makes error
   // handling much more concise
   std::auto_ptr<CClipInitInfo> clipInitInfo(openClipOrClipIni(binPath, clipName,
                                                          &status));
   if (status != 0)
   {
      return status;
   }

   EClipInitSource clipInitSource = clipInitInfo->getClipInitSource();
   if (clipInitSource == CLIP_INIT_SOURCE_CLIP)
   {
      auto clip = clipInitInfo->getClipPtr();
      status = clip->renameClip(newBinPath, newClipName, renameMediaFlag);
   }

   clipInitInfo->closeClip();

   return status;
}

//------------------------------------------------------------------------
//
// Function:    handleRenamedBin
//
// Description: keeps data consistent when a bin has been renamed
//
// Arguments:   string& oldBinPath
//              string& newBinPath
//
// Returns:     void
//
//------------------------------------------------------------------------
void CBinManager::handleRenamedBin(const string& oldBinPath, const string& newBinPath)
{
   openClipList.renameBin(oldBinPath, newBinPath);
}


//------------------------------------------------------------------------
//
// Function:    doesClipExist
//
// Description: Determines if the specified Clip exists
//
// Arguments:   Clip's Bin path and name
//
// Returns:     true if Clip exists
//              false if Clip does not exist or
//                    if Clip or Clip's path is not accessible
//
//------------------------------------------------------------------------
bool CBinManager::doesClipExist(const string& binPath, const string& clipName)
{
   string clipFileName = makeClipFileNameWithExt(binPath, clipName);

   return DoesFileExist(clipFileName);
}

//------------------------------------------------------------------------
//
// Function:    doesClipExist
//
// Description: Determines if the specified Clip exists
//
// Arguments:   Clip's file name.  The input Clip File Name
//              can be in one of the forms:
//                   binPath/clipName
//                   binPath/clipName/clipName
//                   binPath/clipName/clipName.clp
//
// Returns:     true if Clip exists
//              false if Clip does not exist or
//                    if Clip or Clip's path is not accessible
//
//------------------------------------------------------------------------
bool CBinManager::doesClipExist(const string& clipFileName)
{
   string fileName = makeClipFileNameWithExt(clipFileName);

   return DoesFileExist(fileName);
}

//------------------------------------------------------------------------
//
// Function:    doesClipIniExist
//
// Description: Determines if the specified Clip Initialization file exists
//
// Arguments:   Clip's Bin path and name
//
// Returns:     true if Clip Initialization file exists
//              false if Clip does not exist or
//                    if Clip or Clip's path is not accessible
//
//------------------------------------------------------------------------
bool CBinManager::doesClipIniExist(const string& binPath, const string& clipName)
{
   string clipIniFileName = makeClipFileName(binPath, clipName)
									 + '.' + CBinDir::getClipIniFileExtension();

   return DoesFileExist(clipIniFileName);
}

//------------------------------------------------------------------------
//
// Function:    isClipDir
//
// Description: Determines if the specified path is a clip's directory.
//              as determined by the existence of the .clp file with the
//              same name as the directory
//
// Arguments:   Clip's directory path
//
// Returns:     true if Clip exists in directory
//              false if Clip does not exist or
//                    if Clip or Clip's path is not accessible
//
//------------------------------------------------------------------------
bool CBinManager::isClipDir(const string& path)
{
   // Extract presumed clip name and bin path from caller's path
   string localPath = RemoveDirSeparator(path);
   string clipName = GetFileName(localPath);
   string binPath = GetFilePath(localPath);

   // Return true if clip file exists
   return doesClipExist(binPath, clipName);
}

//------------------------------------------------------------------------
//
// Function:    isClipOpen
//
// Description: Determine if Clip is currently open given its name.
//              The Clip Name can be in one of the forms:
//                   binPath/clipName
//                   binPath/clipName/clipName
//                   binPath/clipName/clipName.clp
//
// Arguments:   string& clipFileName
//
// Returns:     true if clip is already open, false otherwise
//
//------------------------------------------------------------------------
bool CBinManager::isClipOpen(const string& clipFileName)
{
   int retVal;

   string binPath, clipName;
   retVal = splitClipFileName(clipFileName, binPath, clipName);
   if (retVal != 0)
      return false;

   return isClipOpen(binPath, clipName);
}

//------------------------------------------------------------------------
//
// Function:    isClipOpen
//
// Description: Determine if Clip is currently open given its Bin path
//              and its name.
//
// Arguments:   string& newBinPath
//              string& newClipName
//
// Returns:     true if clip is already open, false otherwise
//
//------------------------------------------------------------------------
bool CBinManager::isClipOpen(const string& binPath, const string& clipName)
{
   // Look in open clip list to see if the clip is already open
   if (openClipList.findClip(binPath, clipName) != nullptr)
      {
      // Clip is already open
      return true;
      }
   else
      {
      // Clip is not currently open
      return false;
      }
}

//------------------------------------------------------------------------
//
// Function:    openClip
//
// Description: Open an existing Clip given its clip identifier.
//
//              Note: Never delete the clip returned by this function.
//                    Use the CBinManager function closeClip if you wish
//                    to remove the clip from memory
//
// Arguments:   ClipIdentifier clipId
//              int *status
//
// Returns:     Pointer to new CClip instance inititialized from Clip file
//
//------------------------------------------------------------------------
ClipSharedPtr CBinManager::openClip(ClipIdentifier clipId, int *status)
{
#ifdef FIND_CLIP_LEAK
   DBCOMMENT("[open with clipid]");
#endif
   return openClip(clipId.GetClipPath(), status);
}

//------------------------------------------------------------------------
//
// Function:    openClip
//
// Description: Open an existing Clip given its name.
//              The Clip Name can be in one of the forms:
//                   binPath/clipName
//                   binPath/clipName/clipName
//                   binPath/clipName/clipName.clp
//
//              Note: Never delete the clip returned by this function.
//                    Use the CBinManager function closeClip if you wish
//                    to remove the clip from memory
//
// Arguments:   string& clipFileName
//              int *status
//
// Returns:     Pointer to new CClip instance inititialized from Clip file
//
//------------------------------------------------------------------------
ClipSharedPtr CBinManager::openClip(const string& clipFileName, int *status)
{
#ifdef FIND_CLIP_LEAK
	DBCOMMENT("[open with full path name]");
#endif
   string binPath, clipName;
   int retVal = splitClipFileName(clipFileName, binPath, clipName);
   if (retVal != 0)
      {
      *status = retVal;
      return ClipSharedPtr();  // ERROR: Couldn't split the clip name
      }

   // Open the clip
   return openClip(binPath, clipName, status);
}

//------------------------------------------------------------------------
//
// Function:    openClip
//
// Description: Open an existing Clip given its Bin path and its name.
//
//              Note: Never delete the clip returned by this function.
//                    Use the CBinManager function closeClip if you wish
//                    to remove the clip from memory
//
// Arguments:   string& newBinPath
//              string& newClipName
//              int *status
//
// Returns:     Pointer to new CClip instance inititialized from Clip file
//
//------------------------------------------------------------------------
ClipSharedPtr CBinManager::openClip(const string& binPath, const string& clipName,
                              int *status)
{
   int retVal = -1;

#ifdef FIND_CLIP_LEAK
   TRACE_0(errout << "openClip(" << (AddDirSeparator(binPath) + clipName) << ")");
#endif

   // add directory separator if it doesn't already exist
   string binPathCopy = AddDirSeparator(binPath);


   // Look in open clip list to see if the clip is already open
   auto clip = openClipList.findClip(binPathCopy, clipName);
   if (clip != nullptr)
   {
#ifdef FIND_CLIP_LEAK
      DBCOMMENT("Found clip in the Open Clip List");
#endif

      // Re-read the clip file if necessary.
      clip->refreshFromFile();
      openClipList.incrRefCount(clip);

      CClip::DumpMap();

      // Clip is already open, so just return pointer to it
      *status = 0;
      return clip;
   }

   // Check if clip exists
   // TBD ...

   // Create a new CClip instance and set the clip's Bin and Name
   clip = CClip::Create();
   clip->setBinPath(binPathCopy);
   clip->setClipName(clipName);

   // Read the clip file
   retVal = clip->readFile();
   if (retVal != 0)
      {
      // ERROR: Could not read the Clip file
      *status = retVal;
//      delete clip;
      return nullptr;
      }

   // Confirm that the video resolutions are properly licensed
   retVal = clip->CheckVideoResolutionLicense();
   if (retVal != 0)
      {
      // Resolution is not licensed
      *status = retVal;
//      delete clip;
      return nullptr;
      }

   // Add the clip to the BinManager's Open Clip List
   openClipList.addClip(clip);
   *status = 0;

#ifdef FIND_CLIP_LEAK
	DBCOMMENT("Added clip to the Open Clip List");
#endif
   CClip::DumpMap();

   return clip;

} // openClip

//------------------------------------------------------------------------
//
// Function:    openClip
//
// Description: Bumps the ref count for an existing, already opened clip
//
// Arguments:   CClip &clip
///
// Returns:     Nothing
//
//------------------------------------------------------------------------
void CBinManager::openClip(ClipSharedPtr &clip)
{
   if (clip == nullptr)
   {
      return;
   }

#ifdef FIND_CLIP_LEAK
	TRACE_0(errout << "openClip(" << clip->getRelativeClipPath() << ") INCR REF ONLY");
#endif

   openClipList.incrRefCount(clip);

   CClip::DumpMap();
}

//------------------------------------------------------------------------
//
// Function:    openClipOrClipIni
//
// Description: Open an existing Clip or Clip Ini file given its Bin path
//              and its name.
//
// Arguments:   string& newBinPath
//              string& newClipName
//              int *status
//
// Returns:     Pointer to new CClipInitInfo instance inititialized from
//              Clip or Clip Ini file
//
//------------------------------------------------------------------------
CClipInitInfo* CBinManager::openClipOrClipIni(const string& binPath,
                                              const string& clipName,
                                              int *status)
{
   int retVal;
   CClipInitInfo *clipInitInfo = 0;

   if (doesClipExist(binPath, clipName))
      {

   CClip::DumpMap();

      // The clip files exists, so open that
      auto clip = openClip(binPath, clipName, &retVal);
      if (retVal != 0)
         {
         *status = retVal;
         return 0;
         }

      clipInitInfo = new CClipInitInfo;
      retVal = clipInitInfo->initFromClip(clip);
      closeClip(clip);    // done with this now
      clip = nullptr;
      if (retVal != 0)
         {
         // ERROR: Could not open/read the Clip file
         delete clipInitInfo;
         *status = retVal;
         return 0;
         }
      }
   else if (doesClipIniExist(binPath, clipName))
      {
      // The clip file does not exist, but the clip ini file does exist,
      // so open that
      clipInitInfo = new CClipInitInfo;
      retVal = clipInitInfo->initFromClipIni(binPath, clipName);
      if (retVal != 0)
         {
         // ERROR: Could not open/read the Clip Ini file
         delete clipInitInfo;
         *status = retVal;
         return 0;
         }
      }
   else
      {
      // ERROR: Neither the clip or clip ini file seem to exist
      *status = CLIP_ERROR_CLIP_FILE_DOES_NOT_EXIST;
      return 0;
      }

   *status = 0; // Success
   return clipInitInfo;

} // openClipOrClipIni

////------------------------------------------------------------------------
////
//// Function:    refreshClipStatus
////
//// Description:
////
//// Arguments:
////
//// Returns:
////
////------------------------------------------------------------------------
//int CBinManager::refreshClipStatus(const string& binPath,
//                                   const string& clipName)
//{
//   int status;
//
//   if (!isClipOpen(binPath, clipName))
//      return 0;   // Clip isn't open, so don't need to refresh
//
//   auto clip = openClip(binPath, clipName, &status);
//   if (status != 0)
//      return status;  // ERROR: Could not open clip when it should already
//                      //        have been open
//
//   status = clip->refreshFromFile();
//   if (status != 0)
//      {
//      closeClip(clip);
//      return status; // ERROR: Refresh failed
//      }
//
//   closeClip(clip);
//
//   return 0;
//}
//
//int CBinManager::refreshClipStatus(const string& clipFileName)
//{
//   int retVal;
//
//   // Split the clip file name into separate binPath and clipName
//   string binPath, clipName;
//   retVal = splitClipFileName(clipFileName, binPath, clipName);
//   if (retVal != 0)
//      return retVal; // ERROR: Could not parse the clip file name
//
//   retVal = refreshClipStatus(binPath, clipName);
//   if (retVal != 0)
//      return retVal; // ERROR: Refresh failed
//
//   return 0;
//}

//------------------------------------------------------------------------
//
// Function:    moveFilesToTrash
//
// Description: Move a set of files to the "trash."  Given a list of
//              file and directory names, renames each by prepending
//              the trash prefix.  The trash prefix is either supplied by
//              the caller or generated automatically.
//
//              Directories in the file list must appear after the files
//              that they contain appear
//
//
// Arguments:   StringList fileList    List of file names to be renamed.
//              string &trashPrefix    Renamed file prefix.  If string is empty,
//                                     then a prefix is automatically generated
//                                     and caller's argument is set with value
//
// Returns:     0 if sucess, non-zero otherwise
//
//------------------------------------------------------------------------
int CBinManager::moveFilesToTrash(StringList fileList,
                                  string &trashPrefix)
{
   // Generate a trash prefix if caller has not supplied one
   if (trashPrefix.empty())
      trashPrefix = createTrashPrefix();

   int i, retVal;
   string oldFileName, newFileName;
   // Iterate over all of the files in caller's list
   for (i = 0; i < (int)fileList.size(); ++i)
      {
      oldFileName = fileList[i];
      newFileName = GetFilePath(oldFileName) + trashPrefix
                    + GetFileName(oldFileName) + GetFileExt(oldFileName);
      TRACE_3(errout << "CBinManager::moveFilesToTrash: Rename " << oldFileName
                     << " to " << newFileName << endl);

      retVal = rename(oldFileName.c_str(), newFileName.c_str());
      if (retVal != 0)
         {
         // ERROR: Could not rename the file.  Cause is in errno
         TRACE_0(errout << "CBinManager::moveFilesToTrash could not rename file "
                        << endl;
                 errout << "    " << oldFileName << endl;
                 errout << " to " << newFileName << endl;
                 errout << " because " << strerror(errno) << endl;);
         return CLIP_ERROR_FILE_RENAME_FAILED;
         }

#if defined(_WIN32)
      if (trashPrefix[0] == '.')
         SetFileAttributes(newFileName.c_str(), FILE_ATTRIBUTE_HIDDEN);
#endif
      }

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    deleteClipIniFile
//
// Description:
//
// Arguments:
//
// Returns:     0 if sucess, non-zero otherwise
//
//------------------------------------------------------------------------
int CBinManager::deleteClipIniFile(const string& binPath,
                                   const string& clipName)
{
   int retVal;

   StringList filesToBeDeleted;
//   filesToBeDeleted.push_back(makeClipIniFileNameWithExt(binPath, clipName));
   filesToBeDeleted.push_back(makeClipPath(binPath, clipName));

   string emptyString;

   retVal = moveFilesToTrash(filesToBeDeleted, emptyString);
   if (retVal != 0)
      return retVal;

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    getModifiedFrameCount
//
// Description: Retrieve the count of frames modified in the clip
//
// Arguments:   clipPath - the path of the clip directory
//
// Returns:     the count, duh
//
//------------------------------------------------------------------------
int CBinManager::getModifiedFrameCount(const string &clipPath, bool forceRecompute)
{
   int count = -1;

   if (forceRecompute)
      {
      recomputeModifiedFrameCount(clipPath);
      }

   CAutoSpinLocker lock(VersionIniAccessLock);

   string versionIniFilePath = AddDirSeparator(clipPath)
                               + versionIniFileName;
   CIniFile *iniFile = CreateIniFile(versionIniFilePath);
   if (iniFile != nullptr)
      {
      count = iniFile->ReadInteger(clipInfoSectionName,
                                   modifiedFrameCountKey, 0);
      DeleteIniFile(iniFile, true);  // wasn't changed, don't write it
      }

   return count;
}

//------------------------------------------------------------------------
//
// Function:    bumpModifiedFrameCount
//
// Description: Increment the count of frames modified in the clip
//
// Arguments:   clipPath - the path of the clip directory
//              bumpCount - how much to increment the count by (can be < 0)
//
// Returns:     nada
//
//------------------------------------------------------------------------
void CBinManager::bumpModifiedFrameCounter(const string &clipPath,
   int bumpCount)
{
   {
      CAutoSpinLocker lock(VersionIniAccessLock);

      string versionIniFilePath = AddDirSeparator(clipPath) + versionIniFileName;
      CIniFile *iniFile         = CreateIniFile(versionIniFilePath);
      if (iniFile != nullptr)
      {
         int count = iniFile->ReadInteger(clipInfoSectionName, modifiedFrameCountKey, 0);
         count += bumpCount;
         if (count < 0)
         {
            count = 0;
         }
         iniFile->WriteInteger(clipInfoSectionName,
            modifiedFrameCountKey, count);
         DeleteIniFile(iniFile);
      }
   }

   // Huh? Why was this disabled??  #if 0
   string binPath, clipName;
   if (splitClipFileName(clipPath, binPath, clipName) == 0)
   {
      CClip::NotifyClipStatusChanged(binPath, clipName);
   }
   // #endif
}

//------------------------------------------------------------------------
//
// Function:    recomputeModifiedFrameCount
//
// Description: Compute the count of frames modified in the clip by scanning
//              the track file. Only applies to version clips!
//
// Arguments:   clipPath - the path of the clip directory
//
// Returns:     nothing
//
//
//------------------------------------------------------------------------
void CBinManager::recomputeModifiedFrameCount(const string &clipPath)
{
   int retVal;
   ClipIdentifier clipId(clipPath);
   if (!clipId.IsVersionClip())
      {
      // Not a version clip!
      return;
      }

   auto clip = openClip(clipPath, &retVal);
   if (retVal != 0)
      {
      // ERROR: Could not open the new clip
      TRACE_0(errout << "ERROR: RecomputeModifiedFrameCount: clip "
                     << clipPath << " open failed (" << retVal << ")");
      return;
      }

   CVideoProxy *videoProxy = clip->getVideoProxy(0);
   CVideoFrameList *frameList = videoProxy->getVideoFrameList(FRAMING_SELECT_VIDEO);
   int count = frameList->getDirtyFrameCount();

   {
      CAutoSpinLocker lock(VersionIniAccessLock);

      string versionIniFilePath = AddDirSeparator(clipPath) + versionIniFileName;
      CIniFile *iniFile         = CreateIniFile(versionIniFilePath);
      if (iniFile != nullptr)
      {
         iniFile->WriteInteger(clipInfoSectionName, modifiedFrameCountKey, count);
         DeleteIniFile(iniFile);
      }
   }

   string binPath, clipName;
   if (splitClipFileName(clipPath, binPath, clipName) == 0)
   {
      CClip::NotifyClipStatusChanged(binPath, clipName);
   }
}

//------------------------------------------------------------------------
//
// Function:    clearModifiedFrameCount
//
// Description: Set the count of frames modified in the clip to 0
//
// Arguments:   clipPath - the path of the clip directory
//
// Returns:     nada
//
//------------------------------------------------------------------------
void CBinManager::clearModifiedFrameCounter(const string &clipPath)
{
   {
      CAutoSpinLocker lock(VersionIniAccessLock);

      string versionIniFilePath = AddDirSeparator(clipPath) + versionIniFileName;
      CIniFile *iniFile         = CreateIniFile(versionIniFilePath);
      if (iniFile != nullptr)
      {
         iniFile->WriteInteger(clipInfoSectionName, modifiedFrameCountKey, 0);
         DeleteIniFile(iniFile);
      }
   }

   // Huh? Why was this disabled??  #if 0
   string binPath, clipName;
   if (splitClipFileName(clipPath, binPath, clipName) == 0)
   {
      CClip::NotifyClipStatusChanged(binPath, clipName);
   }
   // #endif
}

//------------------------------------------------------------------------
//
// Function:    getDirtyMediaFolderPath
//
// Description: Retrieve the full path for the folder that holds the
//              version clip's dirty media (we wnat to remember it so
//              we can remove it when the version clip is committed or
//              discarded)
//
// Arguments:   none
//
// Returns:     the path
//
//------------------------------------------------------------------------
string CBinManager::getDirtyMediaFolderPath(const string &clipPath)
{
   CAutoSpinLocker lock(VersionIniAccessLock);

   string folderPath;
   string versionIniFilePath = AddDirSeparator(clipPath)
                               + versionIniFileName;
   CIniFile *iniFile = CreateIniFile(versionIniFilePath);
   if (iniFile != nullptr)
   {
      folderPath = iniFile->ReadString(clipInfoSectionName, dirtyMediaFolderKey, "");
      DeleteIniFile(iniFile, true);  // wasn't changed, don't write it
   }

   return folderPath;
}

//------------------------------------------------------------------------
//
// Function:    setDirtyMediaFolderPath
//
// Description: Remember where the version clip dirty media goes
//
// Arguments:   folderPath - the path of the dirty media folder
//
// Returns:     nada
//
//------------------------------------------------------------------------
void CBinManager::setDirtyMediaFolderPath(const string &clipPath, const string &folderPath)
{
   CAutoSpinLocker lock(VersionIniAccessLock);

   string versionIniFilePath = AddDirSeparator(clipPath) + versionIniFileName;
   CIniFile *iniFile         = CreateIniFile(versionIniFilePath);
   if (iniFile != nullptr)
   {
      iniFile->WriteString(clipInfoSectionName, dirtyMediaFolderKey, folderPath);
      DeleteIniFile(iniFile);
   }
}

//------------------------------------------------------------------------
//
// Function:    getDirtyHistoryFolderPath
//
// Description: Retrieve the full path for the folder that holds the
//              version clip's dirty history (we want to remember it so
//              we can remove it when the version clip is committed or
//              discarded)
//
// Arguments:   none
//
// Returns:     the path
//
//------------------------------------------------------------------------
string CBinManager::getDirtyHistoryFolderPath(const string &clipPath)
{
   CAutoSpinLocker lock(VersionIniAccessLock);

   string folderPath;
   string versionIniFilePath = AddDirSeparator(clipPath) + versionIniFileName;
   CIniFile *iniFile         = CreateIniFile(versionIniFilePath);
   if (iniFile != nullptr)
   {
      folderPath = iniFile->ReadString(clipInfoSectionName, dirtyHistoryFolderKey, "");
      DeleteIniFile(iniFile, true); // wasn't changed, don't write it
   }

   return folderPath;
}

//------------------------------------------------------------------------
//
// Function:    setDirtyHistoryFolderPath
//
// Description: Remember where the version clip dirty history goes
//
// Arguments:   folderPath - the path of the dirty media folder
//
// Returns:     nada
//
//------------------------------------------------------------------------
void CBinManager::setDirtyHistoryFolderPath(const string &clipPath,
                                          const string &folderPath)
{
   CAutoSpinLocker lock(VersionIniAccessLock);

   string versionIniFilePath = AddDirSeparator(clipPath) + versionIniFileName;
   CIniFile *iniFile = CreateIniFile(versionIniFilePath);
   if (iniFile != nullptr)
   {
      iniFile->WriteString(clipInfoSectionName,
         dirtyHistoryFolderKey, folderPath);
      DeleteIniFile(iniFile);
   }
}

//------------------------------------------------------------------------
//
// Function:    getVersionStatus
//
// Description: Get the status of the specified clip version.
//
// Arguments:   clipPath - the master clip path
//              versionName - the version name
//
// Returns:     a one-character string representing status (A, C, D, U, X)
//
//------------------------------------------------------------------------
string CBinManager::getVersionStatus(const string &clipPath,
                                     ClipVersionNumber versionNumber)
{
	ClipIdentifier clipId(clipPath, versionNumber);
   string versionName = clipId.GetClipName();
   string versionsIniFilePath = AddDirSeparator(clipId.GetParentPath()) + versionsIniFileName;
   CIniFile *iniFile = CreateIniFile(versionsIniFilePath);
   if (iniFile != nullptr)
   {
      return invalidVersionStatus;   // "X"
   }

   string status = iniFile->ReadString(versionStatusSectionName, versionName, invalidVersionStatus);

   if (status == invalidVersionStatus)
   {
      // Uggh.. legacy crap... guess if the clip is active.
      ClipVersionNumber previousVersionNumber = versionNumber.Previous();
		ClipVersionNumber tmpVersionNumber = getNextActiveVersionNumber(clipPath, previousVersionNumber);
      if (versionNumber == tmpVersionNumber)
      {
         status = versionIsActive;
      }
      else
      {
         status = dontKnowWhatHappenedToTheVersion;
      }
   }

   DeleteIniFile(iniFile);

   return status;
}

//------------------------------------------------------------------------
//
// Function:    setVersionStatus
//
// Description: Remeber the version clip's status.
//
// Arguments:   clipPath - the master clip path
//              versionNumber - the version number
//              status = the new version clip status
//
// Returns:     nada
//
//------------------------------------------------------------------------
void CBinManager::setVersionStatus(const string &clipPath, const string &versionName, const string &status)
{
   string versionsIniFilePath = AddDirSeparator(clipPath)
                               + versionsIniFileName;
   CIniFile *iniFile = CreateIniFile(versionsIniFilePath);
   if (iniFile != nullptr)
   {
      iniFile->WriteString(versionStatusSectionName, versionName, status);
      DeleteIniFile(iniFile);
   }

   if (status == versionWasCommitted || status == versionWasDiscarded)
   {
      CAutoSpinLocker lock(VersionIniAccessLock);

      string versionIniFilePath = ::AddDirSeparator(::AddDirSeparator(clipPath) + versionName)
                                      + versionIniFileName;
      if (::DoesFileExist(versionIniFilePath))
      {
         iniFile = CreateIniFile(versionIniFilePath);
         if (iniFile != nullptr)
         {
            iniFile->WriteBool(statusSectionName, defunctKey, true);
            DeleteIniFile(iniFile);
         }
      }
   }
}

//------------------------------------------------------------------------
//
// Function:    setVersionStatus
//
// Description: Remember the version clip's status.
//
// Arguments:   clipPath - the master clip path
//              versionName - the version name
//              status = the new version clip status
//
// Returns:     nada
//
//------------------------------------------------------------------------
void CBinManager::setVersionStatus(const string &clipPath, const ClipVersionNumber &versionNumber, const string &status)
{
   setVersionStatus(clipPath, versionNumber.ToDisplayName(), status);
}

//////////////////////////////////////////////////////////////////////
// Clip List Functions
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    getOpenClipCount
//
// Description: Returns the count of currently open clips that are
//              being managed by the BinManager.
//
//              Note: The current open clip count will change as
//                    clips are created, opened, closed or deleted.
//
// Arguments:   None
//
// Returns:     Count of open clips, 0 if none open
//
//------------------------------------------------------------------------
unsigned int CBinManager::getOpenClipCount()
{
   return openClipList.getClipCount();
}

//------------------------------------------------------------------------
//
// Function:    getClipListEntry
//
// Description: Returns a pointer to a CClip instance given an index into
//              the BinManager's Open Clip List.  The primary purpose of this
//              function is to iterate through Open Clip List.
//
//              Note: The index of a particular clip may change as clips
//                    are opened, closed, created and deleted.
//
// Arguments:   unsigned int entryIndex   Index into BinManager's Clip List
//                                        of the desired clip.  Value must be
//                                        greater or equal to zero and less
//                                        the open clip count.
//
// Returns:     Pointer to CClip instance.  Returns nullptr if entryIndex
//              is out-of-range.
//
//------------------------------------------------------------------------
ClipSharedPtr CBinManager::getClipListEntry(unsigned int entryIndex)
{
   return openClipList.getClip(entryIndex);
}

//////////////////////////////////////////////////////////////////////
//
// This will read the status of the Main Video track from a given
//   clip file.  Note this is a bit of a hack b/c it avoids the entire
//   Clip infrastructure in order to avoid opening the clip just to
//   read this one value.  However, this is very quick.
//
//   Created: 6/28/2006 - Michael Russell
//
//   Typical Usage:
//      s = bmMgr.getMainVideoStatusQuick(clipIniFileName);
//   Returns:
//      string   = status e.g. "Archived", "Created", etc.
//      ""       = if inifile could not be opened or entry not found
//
//////////////////////////////////////////////////////////////////////
string CBinManager::getMainVideoStatusQuick(const string& clipIniFileName)
{
   CIniFile *clipIniFile = CreateIniFile(clipIniFileName);
   if (clipIniFile == nullptr)
    {
     TRACE_0(errout << "Could not open inifile: " << clipIniFileName
                    << ": " << theError.getMessage());
        return "";
    }

   string clipStatus = clipIniFile->ReadString("VideoProxyInfo0",
                                               "LastMediaAction", "");

   // close the ini file
   delete clipIniFile;

   return clipStatus;
}

//////////////////////////////////////////////////////////////////////
//
// This will change the passed binPath / clipName into the
//   form expected by getMainVideoStatusQuick(string) and
//   call that.
//
//   Created: 6/28/2006 - Michael Russell
//
//   Typical Usage:
//      s = bmMgr.getMainVideoStatusQuick(binPath, clipName);
//   Returns:
//      same as getMainVideoStatusQuick(string);
//
//////////////////////////////////////////////////////////////////////
string CBinManager::getMainVideoStatusQuick(const string& binPath,
                                            const string& clipName)
{
   string clipIniFileName = makeClipFileNameWithExt(binPath, clipName);
   return getMainVideoStatusQuick(clipIniFileName);
}

//////////////////////////////////////////////////////////////////////
//  CBinMgrClipList Class Member Functions
//
//  Implements list of currently open clips that is maintained by
//  the BinManager
//
//////////////////////////////////////////////////////////////////////


CBinManager::CBinMgrClipList::CBinMgrClipList()
{
}

CBinManager::CBinMgrClipList::~CBinMgrClipList()
{
   dumpListToTrace("Destructor");

   deleteList();
}

void CBinManager::CBinMgrClipList::deleteList()
{
   // Remove all the entries from the clip list
   clipList.clear();

   dumpListToTrace("deleteList");
}

void CBinManager::CBinMgrClipList::addClip(ClipSharedPtr &clip)
{
   // Add new clip list entry for clip pointer
   clipList.emplace_back(clip);

   dumpListToTrace("addClip");
}

int CBinManager::CBinMgrClipList::removeClip(ClipSharedPtr &clip)
{
   // Find the clip pointer in the clip list
   for (auto iter = clipList.begin(); iter != clipList.end(); ++iter)
   {
      // Note: == compares clip.get()s.
      if (iter->clip == clip)
      {
         // Found the clip list entry with the matching clip pointer,
         // so remove the entry from the clip list
         clipList.erase(iter);

         dumpListToTrace("removeClip");
         return 0;
      }
   }

   // If we reach here, then the clip pointer was not found in
   // the clip list
   return -1;
}

void CBinManager::CBinMgrClipList::renameBin(const string& oldBinPath, const string& newBinPath)
{
   unsigned int clipCount = getClipCount();
   for (unsigned int i = 0; i < clipCount; ++i)
   {
      auto clip = getClip(i);
      if (clip && AddDirSeparator(clip->getBinPath()) == AddDirSeparator(oldBinPath))
      {
         clip->setBinPath(newBinPath);
      }
   }
}

ClipSharedPtr CBinManager::CBinMgrClipList::getClip(unsigned int index)
{
   if (index >= getClipCount())
   {
      // ERROR: index is out of range
      return nullptr;
   }

   return clipList[index].clip;
}

unsigned int CBinManager::CBinMgrClipList::getClipCount()
{
   return clipList.size();
}

ClipSharedPtr CBinManager::CBinMgrClipList::findClip(const string& targetBinPath,
                                                     const string& targetClipName)
{
   // Iterate through clip list to find clip with matching Bin Path
   // and Clip Name
   string tmpBinPath = AddDirSeparator(targetBinPath);
   for (auto i = 0; i < getClipCount(); ++i)
   {
      auto clip = clipList[i].clip;
      if (AddDirSeparator(clip->getBinPath()) == tmpBinPath && clip->getClipName() == targetClipName)
      {
         // Found the clip, so return pointer to it
         return clip;
      }
   }

   // If this point is reached, then the clip was not found in
   // the clip list, so return nullptr pointer
   return nullptr;
}

int CBinManager::CBinMgrClipList::findClipEntryIndex(ClipSharedPtr &clip)
{
   // Iterate through clip list to find entry with matching clip pointer
   for (auto i = 0; i < getClipCount(); ++i)
   {
      if (clip == clipList[i].clip)
      {
         return i;
      }
   }

   // If this point is reached, then the clip was not found in
   // the clip list, so return -1
   return -1;
}

void CBinManager::CBinMgrClipList::incrRefCount(ClipSharedPtr &clip)
{
   int index = findClipEntryIndex(clip);
   if (index < 0)
   {
      return;
   }

   int newRefCount = ++(clipList[index].referenceCount);

   TRACE_3(errout << "++++++++ Refcount now " << newRefCount << " for " << clip->getClipName());
   dumpListToTrace("incrRefCount");
}

int CBinManager::CBinMgrClipList::decrRefCount(ClipSharedPtr &clip)
{
   int index = findClipEntryIndex(clip);
   if (index < 0)
   {
      return 0;
   }

   if (clipList[index].referenceCount <= 0)
   {
      TRACE_0(errout << "ERROR: Clip-open reference count less than 0");
      return -1;
   }

   int newRefCount = --(clipList[index].referenceCount);

   TRACE_3(errout << "-------- Refcount now " << newRefCount << " for " << clip->getClipName());
   dumpListToTrace("decrRefCount");
   return newRefCount;
}


void CBinManager::CBinMgrClipList::dumpListToTrace(const string &tag)
{
#ifdef FIND_CLIP_LEAK
   TRACE_0(errout << "DUMP BIN MANAGER CLIP LIST in " << tag);
   if (clipList.empty())
   {
      TRACE_0(errout << "CBinManager: All Clips are closed");
   }
   else
   {
      TRACE_0(errout << "CBinManager: " << clipList.size() << " clips are still open");
      for (unsigned int i = 0; i < clipList.size(); ++i)
      {
         auto clip = clipList[i].clip;
         if (clip != nullptr)
         {
            TRACE_0(errout << "  " << i + 1 << ": " << AddDirSeparator(clip->getBinPath()) << clip->getClipName()
                  << "  RefCount: " << clipList[i].referenceCount);
         }
         else
         {
            TRACE_0(errout << "  " << i + 1 << ": Clip pointer is null! " << "  RefCount: " << clipList[i].referenceCount);
         }
      }
   }
#endif
}

CBinManager::CBinMgrClipList::CBinMgrClipListEntry::CBinMgrClipListEntry(ClipSharedPtr newClip)
 : clip(newClip), referenceCount(1)
{
   TRACE_3(errout << "++++++++ OPEN " << clip->getClipName());
}

//CBinManager::CBinMgrClipList::CBinMgrClipListEntry::CBinMgrClipListEntry(CClip *newClip)
// : clip(ClipSharedPtr(std::make_shared<CClip>(newClip))), referenceCount(1)
//{
//   TRACE_3(errout << "++++++++ OPEN " << clip->getClipName());
//}

CBinManager::CBinMgrClipList::CBinMgrClipListEntry::~CBinMgrClipListEntry()
{
    TRACE_3(errout << "-------- CLOSE ");
}

//---------------------------------------------------------------------------
//#ifdef NOTYET
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//
// CClipSchemeManager Class Implementation
//
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CClipSchemeManager::CClipSchemeManager()
{
}

CClipSchemeManager::~CClipSchemeManager()
{
}

int CClipSchemeManager::init()
{
   int retVal;
   deleteClipSchemeList();
   newClipSchemeFileList.clear();

   // Conjure up a list of clip scheme files
   StringList clipSchemeFileNameList;
   retVal = readActiveClipSchemeFileNames(clipSchemeFileNameList);
   if (retVal != 0)
      return retVal; // ERROR

   // If there were clip scheme file names in the ini file, then add them
   // to the active clip scheme list
   if (!clipSchemeFileNameList.empty())
      {
      // Add the clip schemes to our active clip scheme list
      retVal = addClipSchemes(clipSchemeFileNameList);
      if (retVal != 0)
         return retVal; // ERROR

      if (!clipSchemeList.empty())
         {
         // We've actually got some clip schemes, so we're done
         clipSchemeListAvailable = true;
         return 0;
         }
      }

   return 0;
}

///////////////////////////////////////////////////////////////////////
// Stoopid little hack to clean up the ClipSchemManager static stuff at
// shutdown to shut up codeguard
class CStoopidClipSchemeManagerCleanUpHack
{
public:
   ~CStoopidClipSchemeManagerCleanUpHack()
   {
      CClipSchemeManager clipSchemeManager;
      clipSchemeManager.cleanUp();
   };
} StoopidClipSchemeManagerCleanUpHack;

//////////////////////////////////////////////////////////////////////

void CClipSchemeManager::cleanUp()
{
   deleteClipSchemeList();
   // Anything else? there's a bunch of strings & crap
}

void CClipSchemeManager::deleteClipSchemeList()
{
   for (ClipSchemeList::iterator clipScheme = clipSchemeList.begin();
        clipScheme != clipSchemeList.end();
        ++clipScheme)
      {
      delete *clipScheme;
      }

   clipSchemeList.clear();

   clipSchemeListAvailable = false;
}

//////////////////////////////////////////////////////////////////////
// Clip Scheme Search
//////////////////////////////////////////////////////////////////////

int CClipSchemeManager::searchClipSchemes(unsigned int filter,
                                          ClipSchemeList &searchResults)
{
   StringList localResults;
   int retVal;

   if (!clipSchemeListAvailable)
      {
      retVal = init();
      if (retVal != 0)
         return retVal;  // Could not initialize the clip scheme list
      }

#ifdef NOTYET
   if (filter & CLIP_SCHEME_FILTER_ACTIVE)
      {
      retVal = findActiveClipSchemes(filter, localResults);
      if (retVal != 0)
         return retVal; // ERROR: Could not get list of active schemes
                        //        from ini files
      }
   else
      {
      return -4000;  // Not implemented yet
      }

#endif // #ifdef NOTYET

   unsigned int typeFilter = filter & CLIP_SCHEME_FILTER_TYPE_ALL;
   if (typeFilter == 0)
      typeFilter = CLIP_SCHEME_FILTER_TYPE_ALL;   // Default = any type

   for (unsigned int i = 0; i < clipSchemeList.size(); ++i)
      {
      CClipScheme *clipScheme = clipSchemeList[i];
      EClipSchemeType clipSchemeType = clipScheme->getClipSchemeType();
      if ((clipSchemeType == CLIP_SCHEME_TYPE_SD && (typeFilter & CLIP_SCHEME_FILTER_SD))
          || (clipSchemeType == CLIP_SCHEME_TYPE_HD && (typeFilter & CLIP_SCHEME_FILTER_HD))
          || (clipSchemeType == CLIP_SCHEME_TYPE_HSDL && (typeFilter & CLIP_SCHEME_FILTER_HSDL))
          || (clipSchemeType == CLIP_SCHEME_TYPE_IMAGE_FILE && (typeFilter & CLIP_SCHEME_FILTER_IMAGE_FILE)))
         {
         searchResults.push_back(clipScheme);
         }
      }

   return 0;
}

int CClipSchemeManager::readActiveClipSchemeFileNames(StringList &results)
{
   CIniFile *iniFile;
   string clipSchemeDirectory = DEFAULT_CLIP_SCHEME_PATH;
   string activeClipSchemeIniFileName = DEFAULT_CLIP_SCHEME_INI_FILE;
   string activeClipSchemeFilePath;
   bool legacy = 0;
   bool foundClipDirectoryKey;
   bool foundClipIniFileKey;

   // Find the list of active clip scheme file names from the
   // [ActiveClipSchemes] section of either the ClipSchemes.ini file in the
   // ClipSchemes directory (location configurable in the MTILocalMachine.ini
   // file)
   // or, for legacy support, in the MTILocalMachine.ini

   results.clear();

   iniFile = CPMPOpenLocalMachineIniFile();
   if (iniFile != nullptr)
      {
      // We want to know if the MTILocalMachione,ini has been updated to
      // point at external clip scheme files
      foundClipDirectoryKey = iniFile->KeyExists(GENERAL_SECTION,
                                                 clipSchemeDirectoryKey);
      foundClipIniFileKey = iniFile->KeyExists(GENERAL_SECTION,
                                               clipSchemeConfigFileKey);

      // OK, now read them if necessary
      if (foundClipDirectoryKey)
         {
         clipSchemeDirectory = iniFile->ReadString(GENERAL_SECTION,
                                                   clipSchemeDirectoryKey,
                                                   clipSchemeDirectory);
         }
      if (foundClipIniFileKey)
         {
         activeClipSchemeIniFileName = iniFile->ReadString(GENERAL_SECTION,
                                                clipSchemeConfigFileKey,
                                                activeClipSchemeIniFileName);
         }

      // If the value for ClipSchemeConfigFile key is empty, force legacy
      if (activeClipSchemeIniFileName.empty())
         {
         legacy = true;
         }
      else
         {
         // Construct full, expanded path to external clip scheme file
         activeClipSchemeFilePath = ::AddDirSeparator(clipSchemeDirectory)
                                    + activeClipSchemeIniFileName;
         ExpandEnviornmentString(activeClipSchemeFilePath);
         ConformFileName(activeClipSchemeFilePath);

         // Does the external ini file exists?
         if (!checkFileExists(activeClipSchemeFilePath))
            {
            // No dice - file is missing
            static bool oneshot = false;
            if (!oneshot && foundClipIniFileKey)
               {
               oneshot = true;
               TRACE_0(errout << "ERROR: Can't find clip scheme file "
                              << activeClipSchemeFilePath);
               }
            legacy = true;
            }
         else
            {

            // Try to use the external ini file instead of MTIMachineLocal.ini
            DeleteIniFile(iniFile);
            iniFile = CreateIniFile(activeClipSchemeFilePath, /* RO= */ true);
            if (iniFile == nullptr)
               {
               // No dice - file is missing
               static bool oneshot = false;
               if (!oneshot)
                  {
                  oneshot = true;
                  TRACE_0(errout << "ERROR: Can't open clip scheme file "
                                 << activeClipSchemeFilePath);
                  }

               // Switch back to MTIMachineLocal.ini
               iniFile = CPMPOpenLocalMachineIniFile();
               legacy = true;
               }
            else
               {
               // Check for well-formed clip scheme ini file
               if (!iniFile->SectionExists(activeClipSchemeSectionName))
                  {
                  // Doesn't have the section we need - it's no good to us
                  DeleteIniFile(iniFile);

                  static bool oneshot = false;
                  if (!oneshot)
                     {
                     oneshot = true;
                     TRACE_0(errout << "ERROR: Active clip scheme file "
                                    << activeClipSchemeFilePath << endl
                                    << "        does not have a section named "
                                    << activeClipSchemeSectionName);
                      }

                  // Switch back to MTIMachineLocal.ini
                  iniFile = CPMPOpenLocalMachineIniFile();
                  legacy = true;
                  }
               }
            }
         }
      }

   if (iniFile == nullptr)
      {
      static bool oneshot = false;
      if (!oneshot)
         {
         oneshot = true;
         TRACE_0(errout
            << "ERROR: Can't open local machine file to get Clip Scheme list");
         }
      return CLIP_ERROR_CANNOT_FIND_ACTIVE_CLIP_SCHEME_FILE;
      }

   // Read names of active clip scheme files
   iniFile->ReadStringList(activeClipSchemeSectionName,
                                 clipSchemeFileKey, &results, 0);
   DeleteIniFile(iniFile);
   iniFile = 0;

   // If keys were missing from MTIMachineLocal.ini - write them
   if (!legacy && !(foundClipDirectoryKey && foundClipIniFileKey))
      {
      iniFile = CPMPOpenLocalMachineIniFile();
      if (iniFile != nullptr)
         {
         if (!foundClipDirectoryKey)
            {
            iniFile->WriteString(GENERAL_SECTION, clipSchemeDirectoryKey,
                                                  clipSchemeDirectory);
            }
         if (!foundClipIniFileKey)
            {
            iniFile->WriteString(GENERAL_SECTION, clipSchemeConfigFileKey,
                                                  activeClipSchemeIniFileName);
            }
         DeleteIniFile(iniFile);
         iniFile = nullptr;
         }
      }

   // Conform file names: expanding environment variables, etc.
   string fileName;
   for (unsigned int i = 0; i < results.size(); ++i)
      {
      if (legacy)
         fileName = "";
      else
         fileName = ::AddDirSeparator(clipSchemeDirectory);
      fileName += results[i];
      ExpandEnviornmentString(fileName);    // JM's spelling
      ConformFileName(fileName);
      results[i] = fileName;
      }

   return 0;
}

int CClipSchemeManager::addClipSchemes(StringList &fileNameList)
{
   // Add a CClipScheme instance to the clip scheme list for each
   // clip scheme file name in the caller's list

   int retVal;

   for (unsigned int i = 0; i < fileNameList.size(); ++i)
      {
      CClipScheme *clipScheme = new CClipScheme;
      retVal = clipScheme->init(fileNameList[i]);
      if (retVal == 0)
         clipSchemeList.push_back(clipScheme);
      else
         {
         TRACE_0(errout << "ERROR: CClipSchemeManager::addClipSchemes cannot read"
                        << endl
                        << "       clip scheme file " << fileNameList[i] << endl
                        << "       Return code " << retVal);
         delete clipScheme;
         }
      }

   return 0;
}

CClipScheme* CClipSchemeManager::findClipScheme(const string& clipSchemeName)
{
   if (!clipSchemeListAvailable)
      {
      // Initialize the clip scheme list
      int retVal = init();
      if (retVal != 0)
         return 0;  // Could not initialize the clip scheme list
      }

   // TTT Should use STL find with functors, but I don't have time
   //     to figure it out right now - JDS
   for (unsigned int i = 0; i < clipSchemeList.size(); ++i)
      {
      CClipScheme *clipScheme = clipSchemeList[i];
      if (clipScheme->getClipSchemeName() == clipSchemeName)
         return clipScheme;
      }

   return 0; // Didn't find a match
}

int CClipSchemeManager::findActiveClipSchemes(unsigned int filter,
                                              StringList &searchResults)
{
   StringList localResults, tmpResults;
   CIniFile *iniFile = 0;

   unsigned int searchScope = filter & CLIP_SCHEME_FILTER_SCOPE_ALL;
   if (searchScope == 0)
      searchScope = CLIP_SCHEME_FILTER_SITE;  // Default = Site only

   if (searchScope & CLIP_SCHEME_FILTER_USER)
      {
      iniFile = CreateIniFile(CPMPMasterIniFileName());
      if (iniFile != 0)
         {
         tmpResults.clear();
         iniFile->ReadStringList(clipSchemeSectionName,
                                 clipSchemeDirectoryKey, &tmpResults);
         mergeUnsortedList(localResults, tmpResults);
         DeleteIniFile(iniFile);
         iniFile = 0;
         }
      }

   if (searchScope & CLIP_SCHEME_FILTER_LOCAL_MACHINE)
      {
      iniFile = CPMPOpenLocalMachineIniFile();
      if (iniFile != 0)
         {
         tmpResults.clear();
         iniFile->ReadStringList(clipSchemeSectionName,
                                 clipSchemeDirectoryKey, &tmpResults);
         mergeUnsortedList(localResults, tmpResults);
         DeleteIniFile(iniFile);
         iniFile = 0;
         }
      }

   if (searchScope & CLIP_SCHEME_FILTER_SITE)
      {
      iniFile = CPMPOpenSiteIniFile();
      if (iniFile != 0)
         {
         tmpResults.clear();
         iniFile->ReadStringList(clipSchemeSectionName,
                                 clipSchemeDirectoryKey, &tmpResults);
         mergeUnsortedList(localResults, tmpResults);
         DeleteIniFile(iniFile);
         iniFile = 0;
         }
      }

   // Copy list of active scheme names to caller's result list
   searchResults.insert(searchResults.end(), localResults.begin(),
                        localResults.end());
   return 0;
}

void CClipSchemeManager::mergeUnsortedList(StringList &dstList,
                                           StringList &srcList)
{
   StringList::iterator srcEnd = srcList.end();
   StringList::iterator srcIterator;
   StringList::iterator foundScheme;

   // For each scheme name in the unsorted source list
   for (srcIterator = srcList.begin(); srcIterator != srcEnd; ++srcIterator)
      {
      // Use STL algorithm "find" to search for first matching scheme name
      // in the destination list.
      // If a match is not found, then add the scheme name from the source
      // list to the end of the destination list
      foundScheme = find(dstList.begin(), dstList.end(), *srcIterator);
      if (foundScheme == dstList.end())
         dstList.push_back(*srcIterator);
      }
}

#ifdef NOTYET
int CClipSchemeManager::findClipSchemeFiles(const string& dir)
{
   string fileMask = AddDirSeparator(dir) + "*.ccs";
   newFileList.clear();
   findClipSchemeFiles(fileMask);
}
#endif

bool CClipSchemeManager::findSingleClipSchemeFile(string fileName)
{
   newClipSchemeFileList.push_back(fileName);

   return true;
}

//////////////////////////////////////////////////////////////////////
// Utility Functions
//////////////////////////////////////////////////////////////////////

// Function: getClipSchemeIniFile
//
// Given a clip scheme name, returns a pointer to a CIniFile for the
// clip scheme ini file.

//------------------------------------------------------------------------
//
// Function:    getClipSchemeIniFile
//
// Description: Given a clip scheme name, returns a pointer to a CIniFile
//              for the clip scheme ini file.
//
// Arguments:   string &clipSchemeName   name of clip scheme
//
// Returns:     Pointer to CIniFile if function succeeded.
//              nullptr pointer if function fails.  If nullptr, error code and
//              reason are saved in theError.
//
//------------------------------------------------------------------------
CIniFile* CClipSchemeManager::getClipSchemeIniFile(const string& clipSchemeName)
{
   // Find the clip scheme
   CClipScheme *clipScheme = findClipScheme(clipSchemeName);
   if (clipScheme == 0)
      {
      string errMsg = "Cannot find Clip Scheme " + clipSchemeName;
      theError.set(CLIP_ERROR_CANNOT_FIND_CLIP_SCHEME, errMsg);
      return 0;
      }

   string clipSchemeFileName = clipScheme->getClipSchemeFileName();

   // Verify the existence of the clip scheme file
   if (!DoesFileExist(clipSchemeFileName))
      {
      string errMsg = "Cannot find Clip Scheme file " + clipSchemeFileName;
      theError.set(CLIP_ERROR_CLIP_SCHEME_FILE_NOT_FOUND, errMsg);
      return 0;
      }

   // Read the clip initialization file and set the clip init info from it
   CIniFile *iniFile = CreateIniFile(clipSchemeFileName);
   if (iniFile == 0)
      {
      string errMsg = "Cannot open Clip Scheme file " + clipSchemeFileName;
      theError.set(CLIP_ERROR_CLIP_SCHEME_FILE_OPEN_FAILED, errMsg);
      return 0;
      }

   return iniFile;

} // getClipSchemeIniFile


EClipSchemeType CClipSchemeManager::getClipSchemeType(const string &clipSchemeName)
{
   // Find the clip scheme
   CClipScheme *clipScheme = findClipScheme(clipSchemeName);
   if (clipScheme == 0)
      {
      string errMsg = "Cannot find Clip Scheme " + clipSchemeName;
      theError.set(CLIP_ERROR_CANNOT_FIND_CLIP_SCHEME, errMsg);
      return CLIP_SCHEME_TYPE_INVALID;
      }

   return clipScheme->getClipSchemeType();
}


EMediaType CClipSchemeManager::getClipSchemeMainVideoMediaType(
                                       const string &clipSchemeName)
{
   // Find the clip scheme
   CClipScheme *clipScheme = findClipScheme(clipSchemeName);
   if (clipScheme == 0)
      {
      string errMsg = "Cannot find Clip Scheme " + clipSchemeName;
      theError.set(CLIP_ERROR_CANNOT_FIND_CLIP_SCHEME, errMsg);
      return MEDIA_TYPE_INVALID;
      }

   return clipScheme->getMainVideoMediaType();
}

StringList CClipSchemeManager::getVideoFormatClassList(bool videoStore)
{
   StringList classList;
   StringList::iterator listEnd = classList.end();

   if (!clipSchemeListAvailable)
      {
      // Initialize the clip scheme list
      int retVal = init();
      if (retVal != 0)
         return classList;  // empty list
      }

   for (unsigned int i = 0; i < clipSchemeList.size(); ++i)
      {
      CClipScheme *clipScheme = clipSchemeList[i];
      EMediaType mediaType = clipScheme->getMainVideoMediaType();
      if (videoStore && CMediaInterface::isMediaTypeImageFile(mediaType))
         continue;  // mismatch: videostore vs file

      string videoClass = clipScheme->getVideoFormatClass();
      if (videoClass.empty())
         continue;

      if (listEnd == find(classList.begin(), listEnd, videoClass))
         {
         classList.push_back(videoClass);
         listEnd = classList.end();
         }
      }

   return classList;
}


StringList CClipSchemeManager::getAudioFormatClassList(bool videoStore)
{
   StringList classList;
   StringList::iterator listEnd = classList.end();

   if (!clipSchemeListAvailable)
      {
      // Initialize the clip scheme list
      int retVal = init();
      if (retVal != 0)
         return classList;  // empty list
      }

   for (unsigned int i = 0; i < clipSchemeList.size(); ++i)
      {
      CClipScheme *clipScheme = clipSchemeList[i];
      EMediaType mediaType = clipScheme->getMainVideoMediaType();
      if (videoStore && CMediaInterface::isMediaTypeImageFile(mediaType))
         continue;  // mismatch: videostore vs file

      string audioClass = clipScheme->getAudioFormatClass();
      if (audioClass.empty())
         continue;

      if (listEnd == find(classList.begin(), listEnd, audioClass))
         {
         classList.push_back(audioClass);
         listEnd = classList.end();
         }
      }

   return classList;
}


StringList CClipSchemeManager::getMediaStorageClassList(bool videoStore)
{
   StringList classList;
   StringList::iterator listEnd = classList.end();

   if (!clipSchemeListAvailable)
      {
      // Initialize the clip scheme list
      int retVal = init();
      if (retVal != 0)
         return classList;  // empty list
      }

   for (unsigned int i = 0; i < clipSchemeList.size(); ++i)
      {
      CClipScheme *clipScheme = clipSchemeList[i];
      EMediaType mediaType = clipScheme->getMainVideoMediaType();
      if (videoStore && CMediaInterface::isMediaTypeImageFile(mediaType))
         continue;  // mismatch: want videostore, this is file-based scheme
      if ((!videoStore) && (!CMediaInterface::isMediaTypeImageFile(mediaType)))
         continue;  // mismatch: want file-based, this is videostore scheme

      string storageClass = clipScheme->getMediaStorageClass();
      if (storageClass.empty())
         continue;

      if (listEnd == find(classList.begin(), listEnd, storageClass))
         {
         classList.push_back(storageClass);
         listEnd = classList.end();
         }
      }

   return classList;
}

// Look up a clip scheme from the three class components;
// if an argument is "" we treat that as a wild card.
CClipScheme* CClipSchemeManager::findClipScheme(
                                         const string& videoFormatClass,
                                         const string& audioFormatClass,
                                         const string& storageFormatClass)
{
   for (unsigned int i = 0; i < clipSchemeList.size(); ++i)
      {
      CClipScheme *clipScheme = clipSchemeList[i];

      if ((videoFormatClass.empty() ||
                     clipScheme->getVideoFormatClass() == videoFormatClass) &&
          (audioFormatClass.empty() ||
                     clipScheme->getAudioFormatClass() == audioFormatClass) &&
          (storageFormatClass.empty() ||
                     clipScheme->getMediaStorageClass() == storageFormatClass))
         {
         return clipScheme;
         }
      }
   return 0;   // utter failure
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//
// CClipScheme Class Implementation
//
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CClipScheme::CClipScheme()
 : clipSchemeType(CLIP_SCHEME_TYPE_INVALID)
 , mainVideoMediaType(MEDIA_TYPE_INVALID)
{
}

CClipScheme::~CClipScheme()
{
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CClipScheme::init(const string& newFileName)
{
   if (!DoesFileExist(newFileName))
      return CLIP_ERROR_CLIP_SCHEME_FILE_NOT_FOUND;

   fileName = newFileName;

   CIniFile *iniFile = CreateIniFile(fileName);
   if (iniFile == 0)
      return -CLIP_ERROR_CLIP_SCHEME_FILE_OPEN_FAILED; // ERROR: Cannot create CIniFile

   if (!iniFile->SectionExists(clipSchemeInfoSection)
       || !iniFile->KeyExists(clipSchemeInfoSection, clipSchemeNameKey)
       || !iniFile->KeyExists(clipSchemeInfoSection, clipSchemeTypeKey))
      {
      // ERROR: Cannot read Clip Scheme Name or Type, maybe it is not
      //        really a Clip Scheme file
      iniFile->FileName = "";  // Prevent file from being written
      DeleteIniFile(iniFile);
      clipSchemeName = "";
      clipSchemeType = CLIP_SCHEME_TYPE_INVALID;
      return -612;
      }

   string emptyString;  // Empty string to use as default value for ReadString

   // Read the Clip Scheme's Name from the file
   clipSchemeName = iniFile->ReadString(clipSchemeInfoSection,
                                        clipSchemeNameKey, emptyString);

   // Read the Clip Scheme's Type (SD, HD, etc) from the file
   string typeString = iniFile->ReadString(clipSchemeInfoSection,
                                           clipSchemeTypeKey, emptyString);
   clipSchemeType = parseClipSchemeType(typeString);

   typeString = iniFile->ReadString(clipSchemeMainVideoSection,
                                                      clipSchemeMediaTypeKey,
                                                      emptyString);
   mainVideoMediaType = CMediaInterface::queryMediaType(typeString);

   videoFormatClass = iniFile->ReadString(clipSchemeInfoSection,
                                          clipSchemeVideoClassKey,
                                          emptyString);
   audioFormatClass = iniFile->ReadString(clipSchemeInfoSection,
                                          clipSchemeAudioClassKey,
                                          emptyString);
   mediaStorageClass = iniFile->ReadString(clipSchemeInfoSection,
                                          clipSchemeStorageClassKey,
                                          emptyString);

   iniFile->FileName = "";  // Prevent file from being written
   DeleteIniFile(iniFile);
   return 0;   // success
}

//////////////////////////////////////////////////////////////////////
//  Accessors
//////////////////////////////////////////////////////////////////////

string CClipScheme::getClipSchemeFileName() const
{
   return fileName;
}

string CClipScheme::getClipSchemeName() const
{
   return clipSchemeName;
}

EClipSchemeType CClipScheme::getClipSchemeType() const
{
   return clipSchemeType;
}

EMediaType CClipScheme::getMainVideoMediaType() const
{
   return mainVideoMediaType;
}

string CClipScheme::getVideoFormatClass() const
{
   return videoFormatClass;
}

string CClipScheme::getAudioFormatClass() const
{
   return audioFormatClass;
}

string CClipScheme::getMediaStorageClass() const
{
   return mediaStorageClass;
}

bool CClipScheme::doClassesMatch(const string &argVideoClass,
                                 const string &argAudioClass,
                                 const string &argStorageClass)
{
   // assumes empty string input is wild card
   bool retVal = true;

   if (!argVideoClass.empty() && (argVideoClass != videoFormatClass))
      retVal = false;
   else if (!argAudioClass.empty() && (argAudioClass != audioFormatClass))
      retVal = false;
   else if (!argStorageClass.empty() && (argStorageClass != mediaStorageClass))
      retVal = false;

   return retVal;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

EClipSchemeType CClipScheme::parseClipSchemeType(const string& typeString)
{
   // Note for future development: this should be table-driven

   if (EqualIgnoreCase(typeString, "SD"))
      return CLIP_SCHEME_TYPE_SD;
   else if (EqualIgnoreCase(typeString, "HD"))
      return CLIP_SCHEME_TYPE_HD;
   else if (EqualIgnoreCase(typeString, "Film") || EqualIgnoreCase(typeString, "File"))
      return CLIP_SCHEME_TYPE_IMAGE_FILE;
   else if (EqualIgnoreCase(typeString, "HSDL"))
      return CLIP_SCHEME_TYPE_HSDL;
   else if (EqualIgnoreCase(typeString, "Audio"))
      return CLIP_SCHEME_TYPE_AUDIO_ONLY;
   else
      return CLIP_SCHEME_TYPE_INVALID;
}

bool ClipSchemeFileNameCompare::
operator()(const CClipScheme* lhs, const CClipScheme* rhs) const
{
   return (lhs->fileName < rhs->fileName);
}

bool ClipSchemeFileNameEqual::
operator()(const CClipScheme* lhs, const CClipScheme* rhs) const
{
   return (lhs->fileName == rhs->fileName);
}

bool ClipSchemeNameCompare::
operator()(const CClipScheme* lhs, const CClipScheme* rhs) const
{
   return (lhs->clipSchemeName < rhs->clipSchemeName);
}

bool ClipSchemeNameEqual::
operator()(const CClipScheme* lhs, const CClipScheme* rhs) const
{
   return (lhs->clipSchemeName == rhs->clipSchemeName);
}

//#endif // #ifdef NOTYET


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//
// Global Functions
//
//////////////////////////////////////////////////////////////////////


//------------------------------------------------------------------------
//
// Function:     TrimWhiteSpace
//
// Description:  Returns a string that is made from the input string
//               with leading and trailing white space removed.
//               Interior white space is not removed.  White space
//               is typically spaces, tabs, etc.  The input string is
//               not changed
//
// Arguments:    const string& inString     Reference to input string
//
// Returns:      string with leading and trailing whitespace trimmed.
//
//------------------------------------------------------------------------
string TrimWhiteSpace(const string& inString)
{
   string outString;

   // Check if input string is empty
   if (inString.empty())
      return outString;   // Return empty string

   // Iterator to input string
   string::const_iterator strIterator = inString.begin();

   // Skip over leading white space
   while (strIterator != inString.end() && isspace(*strIterator))
      {
      ++strIterator;
      }

   // Check if we have skipped past entire string
   if (strIterator == inString.end())
      return outString; // Return empty string

   // Trim trailing white space
   string::const_iterator lastChar = inString.end() - 1;
   while (strIterator != lastChar && isspace(*lastChar))
      {
      --lastChar;
      }

   // Copy the part of the input string that is within leading
   // and trailing white space
   for ( ; strIterator != lastChar+1; ++strIterator)
      {
      outString += *strIterator;
      }

   return outString;
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
#if 1 // not yet: OLD_RENDER_DEST_HACK

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                                    ###########################
// ###     CRenderDestinationClip Class     ##########################
// ####                                    ###########################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRenderDestinationClip::CRenderDestinationClip()
 : destType(DEST_CLIP_TYPE_SRC),
   srcVideoProxyIndex(-1), srcVideoFramingIndex(-1),
   dstInOutType(DEST_IN_OUT_TYPE_MARKS), dstClipInTC(0), dstClipOutTC(0)
{
}

CRenderDestinationClip::~CRenderDestinationClip()
{
}

//---------------------------------------------------------------------------

string CRenderDestinationClip::GetDestinationClipName()
{
   if (destType == DEST_CLIP_TYPE_SRC)
      return srcClipName;
   else
      return dstClipName;
}

ERenderDestClipType CRenderDestinationClip::GetDestinationType()
{
   return destType;
}

string CRenderDestinationClip::GetDestinationMediaLocation()
{
   // Return the media location for a  new destination clip.
   // May be a  disk scheme name or an image file template;
   return dstMediaLocation;
}

string CRenderDestinationClip::GetSourceClipName()
{
   return srcClipName;
}

string CRenderDestinationClip::GetSourceFormatName()
{
   int retVal;
   CClipInitInfo clipInitInfo;
   string formatName;

   // Initialize the clipInitInfo from the source clip
   CBinManager binMgr;
   auto srcClip = binMgr.openClip(srcClipName, &retVal);
   if (retVal != 0)
      return formatName;   // return empty format name string

   retVal = clipInitInfo.initFromClip(srcClip);
   if (retVal != 0)
      {
      binMgr.closeClip(srcClip);
      return formatName;   // return empty format name string
      }

   formatName = clipInitInfo.getImageFormatName();

   binMgr.closeClip(srcClip);

   return formatName;
}

//----------------------------------------------------------------------------

void CRenderDestinationClip::SetDestinationClip(const string &clipName)
{
   CBinManager binMgr;

   // Standardize the clip name to /BinPath/ClipName
   dstClipName = binMgr.makeClipPath(TrimWhiteSpace(clipName));
}

void CRenderDestinationClip::SetDestinationMediaLocation(const string &mediaLocation)
{
   dstMediaLocation = TrimWhiteSpace(mediaLocation);
}

void CRenderDestinationClip::SetDestinationType(ERenderDestClipType newDestType)
{
   destType = newDestType;
}

void CRenderDestinationClip::SetSourceClip(const string &clipName,
                                           int videoProxyIndex,
                                           int videoFramingIndex)
{
   CBinManager binMgr;

   // Standardize the clip name to /BinPath/ClipName
   string newName = binMgr.makeClipPath(TrimWhiteSpace(clipName));

   bool change = (srcClipName != newName
                  || srcVideoProxyIndex != videoProxyIndex
                  || srcVideoFramingIndex != videoFramingIndex);

   if (change)
      {
      srcClipName = newName;
      srcVideoProxyIndex = videoProxyIndex;
      srcVideoFramingIndex = videoFramingIndex;

      destType = DEST_CLIP_TYPE_SRC;
      }
}

//----------------------------------------------------------------------------

int CRenderDestinationClip::Check()
{
   int retVal;
   CBinManager binMgr;
   CAutoErrorReporter autoErr("CRenderDestinationClip::Check",
                              AUTO_ERROR_NO_NOTIFY, AUTO_ERROR_TRACE_1);

   // If Destination is the Source Clip, then nothing to check
   if (destType == DEST_CLIP_TYPE_SRC)
      return 0;

   if (dstClipName.empty())
      {
      autoErr.errorCode = CLIP_ERROR_MISSING_RENDER_DESTINATION_CLIP_NAME;
      autoErr.msg << "Missing name of Destination clip";
      return autoErr.errorCode;
      }

   if (destType == DEST_CLIP_TYPE_NEW)
      {
      CClipInitInfo clipInitInfo;
      retVal = SetClipInitInfo(clipInitInfo);
      if (retVal != 0)
         {
         autoErr.RepeatTheError();
         return retVal;
         }

      // Validate Clip Init Info
      retVal = clipInitInfo.validate();
      if (retVal != 0)
         {
         autoErr.RepeatTheError();
         return retVal;
         }

      // Bin Path must exist
      if (!DoesDirectoryExist(clipInitInfo.getBinPath()))
         {
         autoErr.errorCode = CLIP_ERROR_BIN_DOES_NOT_EXIST;
         autoErr.msg << "Bin Path " << clipInitInfo.getBinPath()
                     << " does not exist";
         return autoErr.errorCode;
         }

      // Clip and Clip directory cannot exist
      if (DoesDirectoryExist(dstClipName) || binMgr.doesClipExist(dstClipName))
         {
         autoErr.errorCode = CLIP_ERROR_RENDER_DESTINATION_CLIP_ALREADY_EXISTS;
         autoErr.msg << "Destination clip " << dstClipName << " already exists";
         return autoErr.errorCode;
         }

      // Check Media Storage
      retVal = clipInitInfo.checkMediaStorage();
      if (retVal != 0)
         {
         autoErr.RepeatTheError();
         return retVal;
         }

      //    If image files then Image Files cannot exist (this may take a long
      //     time, do we want to check this very much?)
      }

   else if (destType == DEST_CLIP_TYPE_EXISTING)
      {
      //    Destination Clip must already exist
      if (!binMgr.doesClipExist(dstClipName))
         {
         autoErr.errorCode = CLIP_ERROR_RENDER_DESTINATION_CLIP_DOES_NOT_EXIST;
         autoErr.msg << "Destination clip " << dstClipName << " does not exist";
         return autoErr.errorCode;
         }

      // Destination Clip format must match source clip
      auto srcClip = binMgr.openClip(srcClipName, &retVal);
      if (retVal != 0)
         {
         autoErr.RepeatTheError();
         return retVal;
         }
      const CImageFormat *srcImgFormat = srcClip->getImageFormat(srcVideoProxyIndex);
      auto dstClip = binMgr.openClip(dstClipName, &retVal);
      if (retVal != 0)
         {
         autoErr.RepeatTheError();
         binMgr.closeClip(srcClip);
         return retVal;
         }

      const CImageFormat *dstImgFormat = dstClip->getImageFormat(srcVideoProxyIndex);
      if (!srcImgFormat->compare(*dstImgFormat))
         {
         autoErr.errorCode = CLIP_ERROR_DESTINATION_FORMAT_DOES_NOT_MATCH_SOURCE;
         autoErr.msg << "Image format of destination clip is not compatible with the source clip";
         binMgr.closeClip(srcClip);
         binMgr.closeClip(dstClip);
         return autoErr.errorCode;
         }

      binMgr.closeClip(srcClip);
      binMgr.closeClip(dstClip);
      }

   return 0;
}

//----------------------------------------------------------------------------

int CRenderDestinationClip::CreateClip()
{
   int retVal;

   retVal = Check();
   if (retVal != 0)
      return retVal;

   CClipInitInfo clipInitInfo;
   retVal = SetClipInitInfo(clipInitInfo);
   if (retVal != 0)
      return retVal;

/*
       // Verify that none of the files exist
       retVal = clipInitInfo.checkNoImageFiles2(sImageFileNameTemplate,
                                                inFrameNumber,
                                                outFrameNumber + 1);
*/

   auto clip = clipInitInfo.createClip(&retVal);
   if (retVal != 0)
      return retVal;

   destType = DEST_CLIP_TYPE_EXISTING; // the clip now exists

   // Close clip, so clip reference count stays balanced
   CBinManager binMgr;
   binMgr.closeClip(clip);

   return 0;
}

//----------------------------------------------------------------------------

int CRenderDestinationClip::SetClipInitInfo(CClipInitInfo &clipInitInfo)
{
   int retVal;
   CAutoErrorReporter autoErr("CRenderDestinationClip::SetClipInitInfo",
                              AUTO_ERROR_NO_NOTIFY, AUTO_ERROR_TRACE_0);

   // Initialize the clipInitInfo from the source clip
   CBinManager binMgr;
   auto srcClip = binMgr.openClip(srcClipName, &retVal);
   if (retVal != 0)
      {
      autoErr.errorCode = retVal;
      autoErr.msg << "Could not open source clip " << srcClipName;
      return retVal;
      }
   retVal = clipInitInfo.initFromClip(srcClip);
   if (retVal != 0)
      {
      autoErr.errorCode = retVal;
      autoErr.msg << "Could not initialize CClipInitInfo";
      binMgr.closeClip(srcClip);
      return retVal;
      }

   binMgr.closeClip(srcClip);

   CVideoInitInfo *mainVideoInitInfo = clipInitInfo.getMainVideoInitInfo();
   if (mainVideoInitInfo == 0)
      {
      retVal = CLIP_ERROR_MAIN_VIDEO_NOT_AVAILABLE;
      autoErr.errorCode = retVal;
      autoErr.msg << "Source clip does not have any video";
      return retVal;
      }

   // Clear out all of video proxy info except the main video proxy
   clipInitInfo.deleteVideoProxyInitInfo();

   // Clear out all of the audio track info
   clipInitInfo.deleteAudioTrackInitInfo();

   // Note: we leave the time tracks alone, since one might hold cuts

   // Set the new clip name and bin path
   string binPath, clipName;
   retVal = binMgr.splitClipFileName(dstClipName, binPath, clipName);
   if (retVal != 0)
      {
      autoErr.errorCode = retVal;
      autoErr.msg << "Could not parse destination clip name " << dstClipName;
      return retVal;
      }
   clipInitInfo.setBinPath(binPath);
   clipInitInfo.setClipName(clipName);

   // Set a new description

   // Clear Create Date & Time so that it is set when destination clip
   // is actually created
   clipInitInfo.clearCreateDateTime();

   // Set the new in and out timecodes
   clipInitInfo.setInTimecode(dstClipInTC);
   clipInitInfo.setOutTimecode(dstClipOutTC);

   // Set the Main Video Init Info

   // Always create real (not virtual) media for the rendering destination
   mainVideoInitInfo->setVirtualMediaFlag(false);
   // Set the new media location
   // Raw disk name comes from the disk scheme
   // Image file name template is pulled out directly

   if(CMediaInterface::isMediaTypeImageFile(mainVideoInitInfo->getMediaType()))
      {
      // Media is stored in image files

      if (dstMediaLocation.empty())
         {
         retVal = CLIP_ERROR_IMAGE_FILE_BAD_NAME;
         autoErr.errorCode = retVal;
         autoErr.msg << "Missing Image File name";
         return retVal;
         }

      // Error if imageFileNameTemplate is not a properly formed template
      if (!CImageFileMediaAccess::IsTemplate(dstMediaLocation))
         {
         retVal = CLIP_ERROR_IMAGE_FILE_BAD_NAME;
         autoErr.errorCode = retVal;
         autoErr.msg << "Invalid image file template " << dstMediaLocation;
         return retVal;
         }

      // Set Media Identifier to caller's imageFileNameTemplate
      mainVideoInitInfo->setMediaIdentifier(dstMediaLocation);
      }
//   else if(CMediaInterface::isMediaTypeImageRepository(mainVideoInitInfo->getMediaType()))
//      {
//      // Media is stored in image files in a repository. The location
//      // includes a repository name and the relative path to the media
//      // directory within the repository.
//
//      if (dstMediaLocation.empty())
//         {
//         retVal = CLIP_ERROR_IMAGE_REPOSITORY_MEDIA_IDENTIFIER_NOT_AVAILABLE;
//         autoErr.errorCode = retVal;
//         autoErr.msg << "Missing Image Repository identifier";
//         return retVal;
//         }
//
//      // Error if imageFileNameTemplate is not a properly formed template
//      if (!CImageRepositoryMediaAccess::isValidIdentifier(dstMediaLocation))
//         {
//         retVal = CLIP_ERROR_IMAGE_FILE_BAD_NAME;
//         autoErr.errorCode = retVal;
//         autoErr.msg << "Invalid image repository identifier " << dstMediaLocation;
//         return retVal;
//         }
//
//      // Set Media Identifier to caller's imageFileNameTemplate
//      mainVideoInitInfo->setMediaIdentifier(dstMediaLocation);
//      }
   else
      {
      // Set Media Identifier to source clip's media identifier
      mainVideoInitInfo->setMediaIdentifier(dstMediaLocation);
      }

   return 0;
}
#endif // OLD_RENDER_DEST_HACK

void DumpCurrentRefCount(void)
{
   CBinManager bm;
   bm.DumpCurrentRefCount();
}
//---------------------------------------------------------------------------

int CBinManager::FindDirtyFrames(
		const CVideoFrameList *aFrameList,
	int firstFrameIndex,
	int lastFrameIndex,
	vector<int> &aListOfDirtyFrameIndexes,
	int *progressPercent,
	int *modifiedFrameCount)
{
	if (aFrameList == nullptr)
	{
		return CLIP_ERROR_UNEXPECTED_NULL_ARGUMENT;
	}

	int frameIndex;
	int fieldIndex;
	int fieldCount;
	CVideoFrame *frame;
	CVideoField *field;
	if (lastFrameIndex < 0)
	{
		// Gaaa getTotalFrameCount() should be const!!
		CVideoFrameList *nonConstFrameList = const_cast<CVideoFrameList*>(aFrameList);
		lastFrameIndex = nonConstFrameList->getTotalFrameCount() - 1;
	}
	int frameCount = lastFrameIndex - firstFrameIndex + 1;

	// Init to 'no dirty frames found'
	aListOfDirtyFrameIndexes.clear();

	// Progress for UI
	if (progressPercent != nullptr)
	{
		* progressPercent = 0;
	}
	if (modifiedFrameCount != nullptr)
	{
		* modifiedFrameCount = 0;
	}

	for (frameIndex = firstFrameIndex; frameIndex <= lastFrameIndex; ++frameIndex)
	{
		if (progressPercent != nullptr)
		{
			* progressPercent = (100 * (frameIndex - firstFrameIndex)) / frameCount;
		}

		// Get the next frame to check
		// WTF! getFrame() should be const!!
		CVideoFrameList *nonConstFrameList = const_cast<CVideoFrameList*>(aFrameList);
		frame = nonConstFrameList->getFrame(frameIndex);
		if (frame == nullptr)
		{
			TRACE_0(errout << "ERROR: FindDirtyFrames could not access frame " << frameIndex);
			return CLIP_ERROR_UNEXPECTED_NULL_RETURN;
		}

		// Check each field of the frame
		fieldCount = frame->getTotalFieldCount();
		for (fieldIndex = 0; fieldIndex < fieldCount; ++fieldIndex)
		{
			field = frame->getField(fieldIndex);
			if (field == nullptr)
			{
				TRACE_0(errout << "ERROR: FindDirtyFrames could not get" << " version field " << fieldIndex << " in frame " << frameIndex);
				return CLIP_ERROR_UNEXPECTED_NULL_RETURN;
			}
			if (field->isDirtyMedia())
			{
				aListOfDirtyFrameIndexes.push_back(frameIndex);
				if (modifiedFrameCount != nullptr)
				{
					++*modifiedFrameCount;
				}
				break; // Only need to see one dirty field per frame
			}
		} // for each field in frame
	} // for each frame

	// Progress is complete

	if (progressPercent != nullptr)
	{
		* progressPercent = 100;
	}

	return 0; // success
}
// ---------------------------------------------------------------------------

int CBinManager::CheckDirtyMedia(
		const vector<int>&listOfDirtyFrameIndexes,
		const CVideoFrameList *versionFrameList,
		string *firstCorruptFrameName,
		string *lastCorruptFrameName,
		int *progressPercent,
		int *countdown)
{
	int retVal = 0;
	int firstCorruptFrameIndex = -1;
	int lastCorruptFrameIndex = -1;

	// Check all dirty media
	int numberOfDirtyFrames = listOfDirtyFrameIndexes.size();
	for (unsigned i = 0; i < numberOfDirtyFrames; ++i)
	{
		if (progressPercent != nullptr)
		{
			*progressPercent = (int)(100 * (i / (float)numberOfDirtyFrames));
		}

		int checkResult = CheckAndMaybeFixOneDirtyFrame(listOfDirtyFrameIndexes[i], versionFrameList, /* fixIt = */ false);
		if (checkResult != 0)
		{
			if (firstCorruptFrameIndex < 0)
			{
				firstCorruptFrameIndex = listOfDirtyFrameIndexes[i];
			}

			lastCorruptFrameIndex = listOfDirtyFrameIndexes[i];
			retVal = -1;
		}

		if (countdown != nullptr)
		{
			--(*countdown);
		}
	}

	if (firstCorruptFrameIndex >= 0 && firstCorruptFrameName != nullptr)
	{
		CVideoFrameList *nonConstFrameList = const_cast<CVideoFrameList*>(versionFrameList);
		CTimecode firstCorruptFieldTimecode = nonConstFrameList->getFrame(firstCorruptFrameIndex)->getTimecode();
		firstCorruptFieldTimecode.getTimeString(*firstCorruptFrameName);
	}

	if (lastCorruptFrameIndex >= 0 && lastCorruptFrameName != nullptr)
	{
		CVideoFrameList *nonConstFrameList = const_cast<CVideoFrameList*>(versionFrameList);
		CTimecode lastCorruptFieldTimecode = nonConstFrameList->getFrame(lastCorruptFrameIndex)->getTimecode();
		lastCorruptFieldTimecode.getTimeString(*lastCorruptFrameName);
	}

	if (progressPercent != nullptr)
	{
		*progressPercent = 100;
	}

	if (countdown != nullptr)
	{
		*countdown = 0;
	}

	return retVal;
}
// ---------------------------------------------------------------------------

int CBinManager::FixOutOfSyncDirtyMedia(
	CVideoFrameList *versionFrameList,
	int *progressPercent,
	int *countdown)
{
	int retVal = 0;
	int firstCorruptFrameIndex = -1;
	int lastCorruptFrameIndex = -1;
	bool fixedAField = false;

	// Find all the dirty frames in the clip.
	vector<int> listOfDirtyFrameIndexes;
	int firstFrameIndex = versionFrameList->getInFrameIndex();
	int lastFrameIndex = firstFrameIndex + versionFrameList->getUserFrameCount() - 1;
	retVal = FindDirtyFrames(
						versionFrameList,
						firstFrameIndex,
						lastFrameIndex,
						listOfDirtyFrameIndexes,
						progressPercent,
						countdown);   // Hmmm this is actually a "countup"!

	// Check and maybe fix each dirty frame in the clip.
	int numberOfDirtyFrames = listOfDirtyFrameIndexes.size();
	for (unsigned i = 0; i < numberOfDirtyFrames; ++i)
	{
		if (progressPercent != nullptr)
		{
			*progressPercent = (int)(100 * (i / (float)numberOfDirtyFrames));
		}

		int fixResult = CheckAndMaybeFixOneDirtyFrame(listOfDirtyFrameIndexes[i], versionFrameList, /* fixIt = */ true);
		if (fixResult == 0)
		{
			fixedAField = true;
		}

		if (countdown != nullptr)
		{
			--(*countdown);
		}
	}

	if (retVal == 0)
	{
		if (progressPercent != nullptr)
		{
			*progressPercent = 100;
		}

		if (countdown != nullptr)
		{
			*countdown = 0;
		}
	}

	if (retVal == 0 && fixedAField)
	{
		CVideoFrameList *nonConstFrameList = const_cast<CVideoFrameList*>(versionFrameList);
		retVal = nonConstFrameList->writeFrameListFile();
	}

	return retVal;
}
// ---------------------------------------------------------------------------

int CBinManager::CheckAndMaybeFixOneDirtyFrame(
	int frameIndex,
	const CVideoFrameList *versionFrameList,
	bool fixIt)
{
	if (versionFrameList == nullptr)
	{
		return CLIP_ERROR_UNEXPECTED_NULL_ARGUMENT;
	}

   int retVal = 0;
	CVideoFrame *versionFrame;
	CVideoFrame *parentFrame = nullptr;
	int fieldIndex;
	int fieldCount;

	// Find the version clip frame
	// WTF! getFrame() should be const!!
	CVideoFrameList *nonConstFrameList = const_cast<CVideoFrameList*>(versionFrameList);
	versionFrame = nonConstFrameList->getFrame(frameIndex);
	if (versionFrame == nullptr)
	{
		TRACE_0(errout << "ERROR: CheckOneDirtyFrame could not get frame " << frameIndex << endl << "       from version clip");
		return CLIP_ERROR_UNEXPECTED_NULL_RETURN;
	}

	// See if we need to fix a field in the frame.
	fieldCount = versionFrame->getTotalFieldCount();
	for (fieldIndex = 0; fieldIndex < fieldCount; ++fieldIndex)
	{
		CVideoField *versionField = versionFrame->getField(fieldIndex);
		if (versionField == nullptr || !versionField->isDirtyMedia())
		{
			// Skip this field.
			continue;
		}

		CVideoProxy *videoProxy = versionFrameList->getVideoProxy();
		CMediaAccess *dirtyMediaMediaAccess = getDirtyFieldMediaAccess(videoProxy);
		CMediaLocation dirtyFieldMediaLocation = versionField->getMediaLocation();
		if (*(dirtyFieldMediaLocation.getMediaAccess()) != *dirtyMediaMediaAccess)
		{
			if (!fixIt)
			{
				// Check failed, and not fixing so no need to continue!
				return -1;
			}

			// Fix it!
			dirtyFieldMediaLocation.setMediaAccess(dirtyMediaMediaAccess);
			versionField->setMediaLocation(dirtyFieldMediaLocation);

			// Write
			const unsigned int externalBufferSize(4096);  // recommended min size
			char externalBuffer[externalBufferSize];

			int updateResult = versionField->getFieldList()->updateFieldListFile(
												versionField->getFieldListEntryIndex(),
												/* fieldCount = */ 1,
												externalBuffer,
												externalBufferSize);

			if (updateResult != 0 && retVal == 0)
			{
				retVal = updateResult;
			}
		}
	}

	return retVal;
}
// ---------------------------------------------------------------------------

int CBinManager::CommitMedia(
	const vector<int> &listOfDirtyFrameIndexes,
	const CVideoFrameList *versionFrameList,
	CVideoFrameList *parentFrameList,
	int *progressPercent,
	int *countdown)
{
	int retVal = 0;

	// Commit all dirty media
	int numberOfDirtyFrames = listOfDirtyFrameIndexes.size();
	for (unsigned i = 0; i < numberOfDirtyFrames; ++i)
	{
		if (progressPercent != nullptr)
		{
			*progressPercent = (int)(100 * (i / (float)numberOfDirtyFrames));
		}

		int retVal = CommitMediaForOneFrame(listOfDirtyFrameIndexes[i], versionFrameList, parentFrameList);
		if (retVal != 0)
		{
			break;
		}

		if (countdown != nullptr)
		{
			--(*countdown);
		}
	}

	if (retVal == 0)
	{
		if (progressPercent != nullptr)
		{
			*progressPercent = 100;
		}

		if (countdown != nullptr)
		{
			*countdown = 0;
		}
	}

	return retVal;
}
// ---------------------------------------------------------------------------

int CBinManager::CommitMediaForOneFrame(
	int frameIndex,
	const CVideoFrameList *versionFrameList,
	CVideoFrameList *parentFrameList)
{
	if (versionFrameList == nullptr || parentFrameList == nullptr)
	{
		return -1;
	}

	CVideoFrame *versionFrame;
	CVideoFrame *parentFrame;
	int fieldIndex;
	int fieldCount;

	// Find the version clip frame
	// WTF! getFrame() should be const!!
	CVideoFrameList *nonConstFrameList = const_cast<CVideoFrameList*>(versionFrameList);
	versionFrame = nonConstFrameList->getFrame(frameIndex);
	if (versionFrame == nullptr)
	{
		TRACE_0(errout << "ERROR: Version Committer could not get frame " << frameIndex << endl << "       from version clip");
		return -1;
	}

	// Find the parent clip frame
	parentFrame = parentFrameList->getFrame(frameIndex);
	if (parentFrame == nullptr)
	{
		TRACE_0(errout << "ERROR: Version Committer could not get frame " << frameIndex << endl << "       from parent clip");
		return -1;
	}

	CTrack *parentTrack = parentFrameList->getParentTrack();
	parentFrame->allocateDirtyMediaStorageIfNecessary(parentTrack->getMediaStorageList());

	// Move media for each dirty field in the frame
	fieldCount = versionFrame->getTotalFieldCount();
	for (fieldIndex = 0; fieldIndex < fieldCount; ++fieldIndex)
	{
		CVideoField *versionField;
		CVideoField *parentField;

		versionField = versionFrame->getField(fieldIndex);
		if (versionField == nullptr)
		{
			TRACE_0(errout << "ERROR: Version Committer could not get" << " version field " << fieldIndex << " in frame " << frameIndex);
			return -1;
		}
		if (!versionField->isDirtyMedia())
		{
			// Ignore unwritten fields -- continue with next field
			continue;
		}
		parentField = parentFrame->getField(fieldIndex);
		if (parentField == nullptr)
		{
			TRACE_0(errout << "ERROR: Version Committer could not get" << " parent field " << fieldIndex << " in frame " << frameIndex);
			return -1;
		}

		// Move the media from the version clip field to the parent

      // Put in retry hack -jam
      // TODO: Fix this properly
      auto retry = 5;
      auto errorCode1 = 0;
		while (retry-- > 0)
		{
			errorCode1 = parentField->transferMediaFrom(versionField);
			if (errorCode1 != 0)
			{
				TRACE_0(errout << "ERROR: " "Version Committer could not move media" << endl << "       for field " <<
					 fieldIndex << " in frame " << frameIndex << endl << "       Return code " << errorCode1 << ", Retry " << 5-retry);
            SleepMS(500);
			}
         else
         {
         	break;
         }
		}
      // end hack

		// Make the version clip field point back to the parent
	   int errorCode2 = ResetFrameDirtyMetadata(frameIndex, false, versionField, parentField);
		if (errorCode2 != 0)
		{
			TRACE_0(errout << "ERROR: " "Version Committer could not reset dirty metadata" << endl << "       for field " << fieldIndex <<
					" in frame " << frameIndex << endl << "       Return code " << errorCode2);
		}

		if (errorCode1)
		{
         // DOES NOT BELONG HERE, but report it
         CAutoErrorReporter autoErr(__func__,
                              AUTO_ERROR_NOTIFY_NO_WAIT, AUTO_ERROR_TRACE_0);

      	autoErr.errorCode = errorCode1;
      	autoErr.msg << "ERROR: " "Version Committer could not move all media" << endl << "       for field " <<
					 fieldIndex << " in frame " << frameIndex << endl << "       Return code " << errorCode1 << endl <<
                "The media has not been automatically copied from version to parent. You must do this manually";

			return errorCode1;
		}

		if (errorCode2)
		{
			return errorCode2;
		}
	}

	return 0;
}
// ---------------------------------------------------------------------------

int CBinManager::DiscardDirtyMedia(
	const vector<int> &listOfDirtyFrameIndexes,
	const CVideoFrameList *versionFrameList,
	CVideoFrameList *parentFrameList,
	bool destroyMediaFlag,
	int *progressPercent,
	int *countdown)
{
	int retVal = 0;

	unsigned dirtyFrameCount = listOfDirtyFrameIndexes.size();
	if (countdown != nullptr)
	{
		* countdown = dirtyFrameCount;
	}

	if (dirtyFrameCount > 0)
	{
		// Just find the first and last elements of the list
		// We are assuming the list is in increasing order
		DiscardedFrameRange.first = listOfDirtyFrameIndexes[0];
		DiscardedFrameRange.second = listOfDirtyFrameIndexes[dirtyFrameCount - 1] + 1;
	}

	// Discard all dirty media
	for (unsigned i = 0; i < dirtyFrameCount; ++i)
	{

		if (progressPercent != nullptr)
		{
			* progressPercent = (int)(100 * (i / (float) dirtyFrameCount));
		}

		retVal = DiscardDirtyMediaForOneFrame(listOfDirtyFrameIndexes[i], versionFrameList, parentFrameList, destroyMediaFlag);
		if (retVal != 0)
		{
			break;
		}

		if (countdown != nullptr)
		{
			--*countdown;
		}
	}

	// Really stupid, as this just set DiscardedFrameRange
	FinishedDiscardingCommittingFrames(DiscardedFrameRange, destroyMediaFlag);

	if (retVal == 0 && progressPercent != nullptr)
	{
		* progressPercent = 100;
	}

	return retVal;
}
// ---------------------------------------------------------------------------

int CBinManager::DiscardDirtyMediaForOneFrame(
	int frameIndex,
	const CVideoFrameList *versionFrameList,
	CVideoFrameList *parentFrameList,
	bool destroyMediaFlag)
{
	if (versionFrameList == nullptr)
	{
		return CLIP_ERROR_UNEXPECTED_NULL_ARGUMENT;
	}

	CVideoFrame *versionFrame;
	CVideoFrame *parentFrame = nullptr;
	int fieldIndex;
	int fieldCount;

	// Find the version clip frame
	// WTF! getFrame() should be const!!
	CVideoFrameList *nonConstFrameList = const_cast<CVideoFrameList*>(versionFrameList);
	versionFrame = nonConstFrameList->getFrame(frameIndex);
	if (versionFrame == nullptr)
	{
		TRACE_0(errout << "ERROR: DiscardDirtyMedia could not get frame " << frameIndex << endl << "       from version clip");
		return CLIP_ERROR_UNEXPECTED_NULL_RETURN;
	}

	// Find the parent clip frame
	if (parentFrameList != nullptr)
	{
		parentFrame = parentFrameList->getFrame(frameIndex);
		if (parentFrame == nullptr)
		{
			TRACE_0(errout << "ERROR: DiscardDirtyMedia could not get frame " << frameIndex << endl << "       from parent clip");
			return CLIP_ERROR_UNEXPECTED_NULL_RETURN;
		}
	}

	// Destroy media for each dirty field in the frame
	fieldCount = versionFrame->getTotalFieldCount();
	for (fieldIndex = 0; fieldIndex < fieldCount; ++fieldIndex)
	{
		CVideoField *versionField;
		CVideoField *parentField = nullptr;

		versionField = versionFrame->getField(fieldIndex);
		if (versionField == nullptr)
		{
			TRACE_0(errout << "ERROR: DiscardDirtyMedia could not get" << " version field " << fieldIndex << " in frame " << frameIndex);
			return CLIP_ERROR_UNEXPECTED_NULL_RETURN;
		}

		if (parentFrame != nullptr)
		{
			parentField = parentFrame->getField(fieldIndex);
			if (parentField == nullptr)
			{
				TRACE_0(errout << "ERROR: DiscardDirtyMedia could not get" << " parent field " << fieldIndex << " in frame " << frameIndex);
				return CLIP_ERROR_UNEXPECTED_NULL_RETURN;
			}

			if (!versionField->isDirtyMedia())
			{
				// Just copy the media location in case this was a
				// dirty field, but the media has already been discarded
				// while processing another frame list
				versionField->setMediaLocation(parentField->getMediaLocation());
			}
		}

		if (versionField->isDirtyMedia())
		{
			int errorCode1 = 0;
			int errorCode2 = 0;
			if (destroyMediaFlag)
			{
				// Delete the media.
				// Because of bad design, track file and field list file may get
				// out of sync, so we may override the MediaAccess to make sure
				// it's pointing at the dirty media storage area.
				CVideoProxy *videoProxy = versionFrameList->getVideoProxy();
				CMediaAccess *mediaAccessOverride = getDirtyFieldMediaAccess(videoProxy);
				errorCode1 = versionField->destroyMedia(mediaAccessOverride);
				if (errorCode1 != 0)
				{
					TRACE_0(errout << "ERROR: Could not destroy media " << endl << " for field " << fieldIndex << " in frame " << frameIndex <<
							" (" << errorCode1 << ")");
				}
			}

			errorCode2 = ResetFrameDirtyMetadata(frameIndex, true, versionField, parentField);
			if (errorCode2)
			{
				TRACE_0(errout << "ERROR: Could not reset dirty metadata" << " for field " << fieldIndex << " in frame " << frameIndex <<
						" (" << errorCode2 << ")");
				return errorCode2;
			}

			if (errorCode1)
			{
				return errorCode1;
			}
		}
	}

	return 0;
}
//---------------------------------------------------------------------------

int CBinManager::ResetFrameDirtyMetadata(
	int frameIndex,
	bool discardedFlag,
	CVideoField *versionField,
	CVideoField *parentField)
{
	int retVal = versionField->resetDirtyMetadata(parentField);
	if (retVal != 0)
	{
		return retVal;
	}

	IndexOfFrameThatWasDiscardedOrCommitted = frameIndex;
	FrameWasDiscarded = discardedFlag;
	FrameWasDiscardedOrCommittedEvent.SetAllData(this);
	FrameWasDiscardedOrCommittedEvent.Notify();

	return 0;
}
//---------------------------------------------------------------------------

void CBinManager::FinishedDiscardingCommittingFrames(const std::pair<int, int> &frameRange, bool discardedFlag)
{
	IndexOfFrameThatWasDiscardedOrCommitted = -1;

	// A little stupid because in reality it references itself
	DiscardedFrameRange = frameRange;
	FrameWasDiscarded = discardedFlag;
	FrameWasDiscardedOrCommittedEvent.SetAllData(this);
	FrameWasDiscardedOrCommittedEvent.Notify();
}
//---------------------------------------------------------------------------

namespace
{
   // Compare two strings for equality ignoring case.
   bool iequals(const string& a, const string& b)
   {
      return (a.length != b.length)
               ? false
               : std::equal(a.begin(), a.end(),
                         b.begin(), b.end(),
                         [](char a, char b) {
                             return tolower(a) == tolower(b);
                         });
   }
}
//---------------------------------------------------------------------------

int CBinManager::RelinkMediaFoldersForClipAndItsVersions(
   ClipIdentifier clipId,
   const string &newMasterClipMediaFolder,
   const string &newVersionMediaParentFolder)
{
   CAutoErrorReporter autoErr("CBinManager::RelinkMediaFoldersForClipAndItsVersions",
                              AUTO_ERROR_NO_NOTIFY, AUTO_ERROR_TRACE_0);

   int retVal = 0;
   auto masterClip = openClip(clipId, &retVal);
   if (masterClip == nullptr || retVal != 0)
   {
      autoErr.errorCode = retVal;
      autoErr.msg << "Clip open failed for " << clipId.GetClipPath();
      return autoErr.errorCode;
   }

   CVideoProxy *masterVideoProxy = masterClip->getVideoProxy(VIDEO_SELECT_NORMAL);
   if (!masterVideoProxy->isMediaFromImageFile())
   {
      // Can't happen anymore.
      closeClip(masterClip);
      autoErr.errorCode = CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
      autoErr.msg << "Internal error: relink not supported for this storage type";
      return autoErr.errorCode;
   }

   const CMediaStorageList *masterMediaStorageList = masterVideoProxy->getMediaStorageList();
   const int masterMediaStorageCount = masterMediaStorageList->getMediaStorageCount();

   // Only allow to be called with a master clip.
   if (masterMediaStorageCount != 1)
   {
      // Version clip cannot be independently relinked at this time.
      closeClip(masterClip);
      autoErr.errorCode = CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
      autoErr.msg << "Internal error: relink not supported for version clips";
      return autoErr.errorCode;
   }

   // Old media folder needs to be grabbed from the first media storage in the list
   // HACK: We depend on there only being ONE media storage for a master clip.
   CMediaStorage *mediaStorage_0 = masterMediaStorageList->getMediaStorage(0);
   if (mediaStorage_0 == nullptr)
   {
      closeClip(masterClip);
      autoErr.errorCode = CLIP_ERROR_UNEXPECTED_NULL_RETURN;
      autoErr.msg << "Internal error: Can't get media storage 0";
      return autoErr.errorCode;
   }

   string oldMasterClipMediaIdentifier = mediaStorage_0->getMediaIdentifier();
   string oldMasterClipMediaFolder = RemoveDirSeparator(GetFilePath(oldMasterClipMediaIdentifier));

   // Relink the maaster clip if it changed.
   if (oldMasterClipMediaFolder != newMasterClipMediaFolder)
   {
      retVal = RelinkAllMediaStorageForOneClip(clipId, oldMasterClipMediaFolder, newMasterClipMediaFolder);
      if (retVal != 0)
      {
         closeClip(masterClip);
         autoErr.errorCode = retVal;
         autoErr.msg << "Unable to relink media for clip " << clipId.GetClipPath();
         return autoErr.errorCode;
      }
   }

   CVideoFrameList *masterVideoFrameList = masterVideoProxy->getVideoFrameList(VIDEO_SELECT_NORMAL);
   if (masterVideoFrameList == nullptr)
   {
      closeClip(masterClip);
      autoErr.errorCode = CLIP_ERROR_UNEXPECTED_NULL_RETURN;
      autoErr.msg << "Internal error: clip has no frame list??";
      return autoErr.errorCode;
   }

   // No fukkin idea if this makes sense....!
   masterVideoFrameList->deleteFrameList();
   masterVideoFrameList->getTotalFrameCount();  // forces reconstruction

   closeClip(masterClip);

   if (!doesClipHaveVersions(clipId.GetParentPath(), clipId.GetClipName()))
   {
      // Nothing else to do!
      return 0;
   }

   // Relink the version clips if they've changed. The old media is determined
   // from the "dirty media" storage. It is assumed that ALL version media
   // folders have the same parent.
   string oldMasterClipFilename = GetFileNameWithExt(oldMasterClipMediaFolder);
   string newMasterClipFilename = GetFileNameWithExt(newMasterClipMediaFolder);
   vector<ClipVersionNumber> versionNumberList;
	getListOfClipsActiveVersionNumbers(clipId, versionNumberList, true, true);
   for (unsigned j = 0; j < versionNumberList.size(); ++j)
   {
      // Relink media folder for this version.
		ClipIdentifier versionClipId = clipId + versionNumberList[j];
      TRACE_0(errout << " RELINKING VERSION: " << versionClipId);

      // Fix media storage 0 (reference to the master). If the version media
      // files have the same parent forder as the master media files, this will
      // fix all of theose, too.
      if (oldMasterClipMediaFolder != newMasterClipMediaFolder)
      {
         retVal = RelinkAllMediaStorageForOneClip(versionClipId, oldMasterClipMediaFolder, newMasterClipMediaFolder);
         if (retVal != 0)
         {
            autoErr.errorCode = retVal;
            autoErr.msg << "Clean media relink failed for version clip " << versionClipId.GetClipPath();
            return autoErr.errorCode;
         }
      }

      auto versionClip = openClip(versionClipId, &retVal);
      if (versionClip == nullptr || retVal != 0)
      {
         autoErr.errorCode = retVal;
         autoErr.msg << "Clip open failed for " << versionClip->getClipPathName();
         return autoErr.errorCode;
      }

      CVideoProxy *versionVideoProxy = versionClip->getVideoProxy(VIDEO_SELECT_NORMAL);
      const CMediaStorageList *versionMediaStorageList = versionVideoProxy->getMediaStorageList();
      int dirtyMediaStorageIndex = versionMediaStorageList->getDirtyMediaStorageIndex();
      if (dirtyMediaStorageIndex < versionMediaStorageList->getMediaStorageCount())
      {
         CMediaStorage *dirtyMediaStorage = versionMediaStorageList->getMediaStorage(dirtyMediaStorageIndex);
         if (dirtyMediaStorage == nullptr)
         {
            closeClip(versionClip);
            autoErr.errorCode = CLIP_ERROR_UNEXPECTED_NULL_RETURN;
            autoErr.msg << "Internal error: Can't get media storage for dirty media index " << dirtyMediaStorageIndex;
            return autoErr.errorCode;
         }

         string oldVersionClipMediaIdentifier = dirtyMediaStorage->getMediaIdentifier();
         string oldVersionClipMediaFolder = RemoveDirSeparator(GetFilePath(oldVersionClipMediaIdentifier));
         string oldVersionClipMediaFolderPrefix = AddDirSeparator(GetFilePath(oldVersionClipMediaFolder))
                                                + oldMasterClipFilename;
         string newVersionClipMediaFolderPrefix = AddDirSeparator(newVersionMediaParentFolder) + newMasterClipFilename;
         if (oldVersionClipMediaFolderPrefix != newVersionClipMediaFolderPrefix)
         {
            retVal = RelinkAllMediaStorageForOneClip(
                        versionClipId,
                        oldVersionClipMediaFolderPrefix,
                        newVersionClipMediaFolderPrefix);
            if (retVal != 0)
            {
               closeClip(versionClip);
               autoErr.errorCode = retVal;
               autoErr.msg << "Dirty media relink failed for version clip " << versionClipId.GetClipPath();
               return autoErr.errorCode;
            }
         }
      }

      CVideoFrameList *versionVideoFrameList = versionVideoProxy->getVideoFrameList(VIDEO_SELECT_NORMAL);
      if (versionVideoFrameList == nullptr)
      {
         closeClip(versionClip);
         autoErr.errorCode = CLIP_ERROR_UNEXPECTED_NULL_RETURN;
         autoErr.msg << "Internal error: clip has no frame list??";
         return autoErr.errorCode;
      }

      // No fukkin idea if this makes sense....!
      versionVideoFrameList->deleteFrameList();
      versionVideoFrameList->getTotalFrameCount();  // forces reconstruction

      closeClip(versionClip);
   }

   return 0;
}
//---------------------------------------------------------------------------

int CBinManager::RelinkAllMediaStorageForOneClip(
   ClipIdentifier clipId,
   const string &oldMasterClipMediaFolder,
   const string &newMasterClipMediaFolder)
{
   CAutoErrorReporter autoErr("CBinManager::RelinkAllMediaStorageForOneClip",
                              AUTO_ERROR_NO_NOTIFY, AUTO_ERROR_TRACE_0);
//DBTRACE(clipId);

   int retVal = 0;
   auto clip = openClip(clipId, &retVal);
   if (clip == nullptr || retVal != 0)
   {
      autoErr.errorCode = retVal;
      autoErr.msg << "Clip open failed for " << clipId.GetClipPath();
      return autoErr.errorCode;
   }

   CVideoProxy *videoProxy = clip->getVideoProxy(VIDEO_SELECT_NORMAL);
   if (!videoProxy->isMediaFromImageFile())
   {
      // Can't happen anymore.
      closeClip(clip);
      autoErr.errorCode = CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
      autoErr.msg << "Internal error: relink not supported for this storage type";
      return autoErr.errorCode;
   }

   CMediaFormat *mediaFormat = (CMediaFormat *)clip->getImageFormat(VIDEO_SELECT_NORMAL);

   string oldMasterFolder = RemoveDirSeparator(oldMasterClipMediaFolder);
   string newMasterFolder = RemoveDirSeparator(newMasterClipMediaFolder);
TRACE_0(errout << "RELINK " << oldMasterFolder << " to " << newMasterFolder);
   if (videoProxy->getMediaStorageList() == nullptr)
   {
      closeClip(clip);
      autoErr.errorCode = CLIP_ERROR_UNEXPECTED_NULL_RETURN;
      autoErr.msg << "Internal error: clip has no storage??";
      return autoErr.errorCode;
   }

   // make a local copy of the media storage list.
   auto oldMediaStorageList = videoProxy->getMediaStorageList();
   auto newMediaStorageList = new CMediaStorageList();
   int mediaStorageCount = oldMediaStorageList->getMediaStorageCount();

   for (int mediaStorageIndex = 0;
   mediaStorageIndex < mediaStorageCount;
   ++mediaStorageIndex)
   {
      CMediaStorage *mediaStorage = oldMediaStorageList->getMediaStorage(mediaStorageIndex);
      if (mediaStorage == nullptr)
      {
         retVal = CLIP_ERROR_UNEXPECTED_NULL_RETURN;
         autoErr.errorCode = retVal;
         autoErr.msg << "Internal error: Can't get media storage 0";
         break;
      }

      EMediaType oldMediaType = mediaStorage->getMediaType();
      string oldMediaIdentifier = mediaStorage->getMediaIdentifier();
//DBTRACE(oldMediaIdentifier);
      string oldMediaFolder = RemoveDirSeparator(GetFilePath(oldMediaIdentifier));
      string mediaFileTemplate = GetFileName(oldMediaIdentifier) + GetFileExt(oldMediaIdentifier);
      string oldHistoryFolder = RemoveDirSeparator(mediaStorage->getHistoryDirectory());

      // If the first part of this media storage's path does not exactly match
      // the full new master clip media folder path, skip this media storage.
      // QQQ Lots of assumptions here!!
      if (oldMediaFolder.substr(0, oldMasterFolder.length()) != oldMasterFolder)
      {
         auto oldMediaAccess = mediaStorage->getMediaAccessObj();
         oldMediaAccess->incrRefCount();
         newMediaStorageList->addMediaStorage(oldMediaType, oldMediaIdentifier, oldHistoryFolder, oldMediaAccess);
         continue;
      }

      EMediaType newMediaType = oldMediaType;
      string newMediaFolder = newMasterFolder + oldMediaFolder.substr(oldMasterFolder.length());
      string newMediaIdentifier = AddDirSeparator(newMediaFolder) + mediaFileTemplate;
//DBTRACE(newMediaIdentifier);

      // Only want to rewrite the MtiMetadata.ini file if this is our dirty
      // media storage location.
      string newHistoryFolder = oldHistoryFolder;
      if (mediaStorageCount == 1
      || mediaStorageIndex == oldMediaStorageList->getDirtyMediaStorageIndex())
      {
         // ??? Old history folder seems to always be empty! so we just look at the
         // MTIMetaData.ini file.
         string newMetaDataIniPath = AddDirSeparator(newMediaFolder) + "MTIMetaData.ini";
         if (DoesFileExist(newMetaDataIniPath))
         {
            auto metaDataIniFile = CreateIniFile(newMetaDataIniPath);
            if (metaDataIniFile != nullptr)
            {
               const string historySectionName = "History";
               const string historyDirectoryKey = "HistoryDirectory";
               oldHistoryFolder = metaDataIniFile->ReadString(historySectionName, historyDirectoryKey, "");
               newHistoryFolder = oldHistoryFolder;
               if (oldHistoryFolder == (AddDirSeparator(oldMediaFolder) + "History"))
               {
                  newHistoryFolder = AddDirSeparator(newMediaFolder) + "History";
                  metaDataIniFile->WriteString(historySectionName, historyDirectoryKey, newHistoryFolder);
               }

               DeleteIniFile(metaDataIniFile);
            }
         }

         if (getDirtyMediaFolderPath(clipId.GetClipPath()) == oldMediaFolder
         && oldMediaFolder != newMediaFolder)
         {
            setDirtyMediaFolderPath(clipId.GetClipPath(), newMediaFolder);
         }

         if (getDirtyHistoryFolderPath(clipId.GetClipPath()) == oldHistoryFolder
         && oldHistoryFolder != newHistoryFolder)
         {
            setDirtyHistoryFolderPath(clipId.GetClipPath(), newHistoryFolder);
         }
      }

      const CImageFormat *imageFormat = clip->getImageFormat(VIDEO_SELECT_NORMAL);
      CMediaInterface mediaInterface;
      CMediaAccess *newMediaAccess = mediaInterface.registerMediaAccess(
                                       newMediaType,
                                       newMediaIdentifier,
                                       newHistoryFolder,
                                       *imageFormat);

      newMediaStorageList->addMediaStorage(newMediaType, newMediaIdentifier, newHistoryFolder, newMediaAccess);
   }

   newMediaStorageList->setDirtyMediaStorageIndex(oldMediaStorageList->getDirtyMediaStorageIndex());

   retVal = clip->updateMediaStorageSection(VIDEO_SELECT_NORMAL, newMediaStorageList);
   if (retVal != 0)
   {
      autoErr.errorCode = retVal;
      autoErr.msg << "Clip relink failed for " << clipId.GetClipPath() << " (" << retVal << ")";
   }

   closeClip(clip);
   clip = nullptr;

   CClip::NotifyClipStatusChanged(clipId.GetBinPath(), clipId.GetClipName());

   return retVal;
}
