// Clip3AudioField.cpp: implementation of the CAudioField class.
//
/*
$Header: /usr/local/filmroot/clip/source/Clip3AudioField.cpp,v 1.6 2005/02/16 18:33:16 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "Clip3AudioTrack.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAudioField::CAudioField()                   // Default Constructor
{

}

CAudioField::CAudioField(const CAudioField& sourceField) // Copy Constructor
: CField(sourceField)
{

}

CAudioField::~CAudioField()
{
}

//////////////////////////////////////////////////////////////////////
// Assignment Operator
//////////////////////////////////////////////////////////////////////

CAudioField& CAudioField::operator=(const CAudioField& rhs)
{
   if (this != &rhs)       // Check for self-assignment
      {
      // Copy base type CField
      CField::operator=(rhs);

      // Copy CAudioField members
      //   ... no members, so nothing to do.  If members are added to
      //       CAudioField, then assigments for those members will go here
      }

   return *this;   // Return reference to this
}

//////////////////////////////////////////////////////////////////////
// Virtual Copy
//////////////////////////////////////////////////////////////////////

int CAudioField::virtualCopy(const CAudioField &srcField,
                             CMediaStorageList &dstMediaStorageList)
{
   int retVal;

   // Make temporary copy of the Field List reference, since
   // the destination Video Proxy has its own Field List.
   CFieldList *tmpFieldList = getFieldList();
   int tmpFieldListEntryIndex = getFieldListEntryIndex();

   // Copy everything from source field
   operator=(srcField);

   // Set virtual flag is field
   setVirtualMedia(true);

   // Restore the original Field List reference that was overwritten
   // by the assignment operator
   setFieldListEntryIndex(tmpFieldList, tmpFieldListEntryIndex);

   // Copy the media status from the source field list entry
   // NB: Since this is a static copy of the media status, the virtual
   //     clip will not know that the source field has been written
   //     at a later time.  A better approach would be to have a single
   //     status for a field that was shared by all clips that used that field.
   //     This may be a good application for a real database.
   setMediaWritten(srcField.isMediaWritten());
   setMediaStorageExists(srcField.doesMediaStorageExist());

   // Make sure that the CTrack that owns this field knows about the
   // Media Access object that is used in this field
   retVal = fixMediaStorageForVirtualCopy(dstMediaStorageList);
   if (retVal != 0)
      return 0;

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Read/Write Media Data Functions
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    readMedia
//
// Description: Read a field's media data from media storage
//              into caller's buffer.  If the "Media Written" flag
//              for this field indicates that the audio media has never
//              been written, then just fill the caller's buffer without
//              actually reading the audio media. 
//
// Arguments    MTI_UINT8 *buffer  Pointer to media data buffer
//
// Returns:     0 if success.  Non-zero if error    
//
//------------------------------------------------------------------------
int CAudioField::readMedia(MTI_UINT8 *buffer)
{
   // Read the field's media data from the media storage into the caller's 
   // buffer and return the success/error status

   int retVal;

#ifndef INIT_AUDIO_FILE
   if (!isMediaWritten() || !doesMediaStorageExist())
      {
      // This field's media has not been written or the media storage
      // does not exist, so fill caller's buffer with zeros without
      // actually reading the audio media
      memset(buffer, 0, mediaLocation.getDataByteSize());
      }
   else
#endif
      {
      retVal = mediaLocation.read(buffer);
      if (retVal != 0)
         return retVal;    // ERROR
      }

   return 0;
}

//------------------------------------------------------------------------
//
// Function:   writeMedia
//
// Description: Write a field's media data from caller's buffer to
//              media storage 
//
// Arguments    MTI_UINT8 *buffer  Pointer to media data buffer
//
// Returns:     0 if success.  Non-zero if error    
//
//------------------------------------------------------------------------
int CAudioField::writeMedia(MTI_UINT8 *buffer)
{
   // Write the field's media data from the caller's buffer to the
   // the media storage given the field's media location.  Return
   // the success/error status;

   int retVal;

   retVal = mediaLocation.write(buffer);
   if (retVal != 0)
      return retVal;

   setMediaWritten(true);
   setMediaStorageExists(true);

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Testing and Debugging Functions
//////////////////////////////////////////////////////////////////////

void CAudioField::dump(MTIostringstream& str) const
{
   CField::dump(str);
}
