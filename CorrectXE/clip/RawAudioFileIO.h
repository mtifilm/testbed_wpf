// RawAudioFileIO.h: interface for the CRawAudioFileIO class.
//
//////////////////////////////////////////////////////////////////////

#ifndef RAWAUDIOFILEIO_H
#define RAWAUDIOFILEIO_H

#include <string>
using std::string;

#if defined(_WIN32)
#include <windows.h>
#endif

#include "machine.h"
#include "IniFile.h"

//////////////////////////////////////////////////////////////////////

class CRawAudioFileIO  
{
public:
   CRawAudioFileIO();
   virtual ~CRawAudioFileIO();

   int open(const string& fileName);
   int openReadOnly(const string& fileName);
   int close();

   static int createFile(const string& fileName, MTI_INT64 size);
   static bool checkFileExists(const string& fileName);
   static int deleteFile(const string &fileName);

   int extendFile(MTI_INT64 size);
   int shortenFile(MTI_INT64 index, MTI_INT64 size);

   int read(unsigned char* dataBuffer, MTI_INT64 offset, int size);
   int write(const unsigned char* dataBuffer, MTI_INT64 offset, int size);

   int setFileBytes(unsigned char value);    // whole file
   int setFileBytes(unsigned char value, MTI_INT64 offset, MTI_INT64 byteCount);
   MTI_INT64 getFileSize();

private:

#if defined(__sgi) || defined(__linux)        // UNIX
   int handle;
#elif defined(_WIN32)      // Windows, VC++ or BC++
   HANDLE handle;
#elif defined(_MSC_VER)     // Microsoft compilers
   int handle;
#else
#error Unknown Compiler
#endif

  CThreadLock rawAudioFileIOThreadLock;

  string m_fileName;
};

//////////////////////////////////////////////////////////////////////

#endif // #ifndef RAWAUDIOFILEIO_H
