// RawAudioFileIO.cpp: implementation of the CRawAudioFileIO class.
//
//////////////////////////////////////////////////////////////////////

#include "IniFile.h"      // Trace
#include "err_clip.h"
#include <errno.h>

#if defined(__sgi) || defined(__linux)        // UNIX
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
// #ifdef __sgi

#elif defined(_WIN32)
#include <time.h>      // time_t is broken in sys/types.h in RadStudio XE4
#include <sys/types.h>
#include <sys/stat.h>
// #elif defined(_WIN32)

#else
#error Unknown Compiler
#endif

#include "RawAudioFileIO.h"
#include "ThreadLocker.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRawAudioFileIO::CRawAudioFileIO()
{
#if defined(__sgi) || defined(__linux)        // UNIX
   handle = -1;
#elif defined(_WIN32)      // Windows, VC++ or BC++
   handle = INVALID_HANDLE_VALUE;
#else
#error Unknown Compiler
#endif

}

CRawAudioFileIO::~CRawAudioFileIO()
{
   close();
}

//////////////////////////////////////////////////////////////////////

int CRawAudioFileIO::open(const string& fileName)
{
   m_fileName = fileName;

#if defined(__sgi) || defined(__linux)        // UNIX
  handle = ::open(fileName.c_str(), O_RDWR|O_BINARY, 0666);
  if (handle == -1)
      {
      TRACE_3(errout << "CRawAudioFileIO::open failed to for file "
                     << fileName << endl;
              errout << strerror(errno) << endl;);
      return -1;
      }
  else
     return 0; // success

#elif defined(_WIN32)      // Windows, VC++ or BC++
//
// JAM Temporary fix, changed FILE_SHARE_READ | FILE_SHARE_WRITE from 0
   handle = CreateFile(fileName.c_str(), GENERIC_READ | GENERIC_WRITE,
                       FILE_SHARE_READ | FILE_SHARE_WRITE,
                       0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
   if (handle == INVALID_HANDLE_VALUE)
      {
      TRACE_0(errout << "CRawAudioFileIO::open failed to for file "
                     << fileName << endl;
              errout << GetLastSystemErrorMessage() << endl;);
      return CLIP_ERROR_FILE_OPEN_FAILED;
      }
   else
      return 0; // success

#else
#error Unknown Compiler
#endif
}

int CRawAudioFileIO::openReadOnly(const string& fileName)
{
   m_fileName = fileName;

#if defined(__sgi) || defined(__linux)        // UNIX
  handle = ::open(fileName.c_str(), O_RD|O_BINARY, 0666);
  if (handle == -1)
      {
      TRACE_3(errout << "CRawAudioFileIO::open failed to for file "
                     << fileName << endl;
              errout << strerror(errno) << endl;);
      return -1;
      }
  else
     return 0; // success

#elif defined(_WIN32)      // Windows, VC++ or BC++
//
// JAM Temporary fix, changed FILE_SHARE_READ | FILE_SHARE_WRITE from 0
   handle = CreateFile(fileName.c_str(), GENERIC_READ,
                       FILE_SHARE_READ | FILE_SHARE_WRITE, 0, OPEN_EXISTING,
                       FILE_ATTRIBUTE_NORMAL, 0);
   if (handle == INVALID_HANDLE_VALUE)
      {
      TRACE_0(errout << "CRawAudioFileIO::openReadOnly failed to for file "
                     << fileName << endl;
              errout << GetLastSystemErrorMessage() << endl;);
      return CLIP_ERROR_FILE_OPEN_FAILED;
      }
   else
      return 0; // success

#else
#error Unknown Compiler
#endif
}

int CRawAudioFileIO::close()
{
#if defined(__sgi) || defined(__linux)        // UNIX
   if (handle != -1)
      {
      int retVal = ::close(handle);
      handle = -1;
      return(retVal);
      }
   else
      return 0;

#elif defined(_WIN32)      // Windows, VC++ or BC++
   if (handle != INVALID_HANDLE_VALUE)
      {
      BOOL retVal = CloseHandle(handle);
      handle = INVALID_HANDLE_VALUE;
      if(retVal)
         return 0;
      else
         return CLIP_ERROR_FILE_CLOSE_FAILED;

      }

#else
#error Unknown Compiler
#endif
   return -1;
}

//////////////////////////////////////////////////////////////////////

int CRawAudioFileIO::read(unsigned char *dataBuffer, MTI_INT64 offset, int size)
{
  CAutoThreadLocker atl(rawAudioFileIOThreadLock);  // Lock thread until return
#if defined(__sgi) || defined(__linux) // UNIX

   // Standard read function that can read either raw disk or regular file.
   // Equivalent to a lseek64 followed by read
   return pread64(handle, dataBuffer, size, offset);

#elif defined(_WIN32)   // Defined by Microsoft and Borland compilers
   
   DWORD retVal;
   retVal = SetFilePointer(handle, LOWLONGLONG(offset), ((LONG*)&offset)+1,
                           FILE_BEGIN);
   if (retVal == INVALID_SET_FILE_POINTER && GetLastError() != NO_ERROR)
      {
      TRACE_0(errout << "ERROR: CRawAudioFileIO::read: "
                     << "SetFilePointer failed for " << endl
                     << "       audio file " << m_fileName << endl
                     << "       because "
                     << GetLastSystemErrorMessage());
      return -1;
      }

   DWORD numberOfBytesRead;
   BOOL retStatus;
   retStatus = ReadFile(handle, dataBuffer, size, &numberOfBytesRead, 0);
   if (retStatus)
      return(numberOfBytesRead);
   else
      {
      TRACE_0(errout << "ERROR: CRawAudioFileIO::read: "
                     << "ReadFile failed for " << endl
                     << "       audio file " << m_fileName << endl
                     << "       because "
                     << GetLastSystemErrorMessage());
      return -1;
      }

#else
#error Unknown compiler
#endif
}

//////////////////////////////////////////////////////////////////////

int CRawAudioFileIO::write(const unsigned char *dataBuffer, MTI_INT64 offset,
                           int size)
{
  CAutoThreadLocker atl(rawAudioFileIOThreadLock);  // Lock thread until return
#if defined(__sgi) || defined(__linux)    // UNIX

   // Standard write function that can write to either raw disk or regular file.
   // Equivalent to a lseek64 followed by write
   return pwrite64(handle, dataBuffer, size, offset);

#elif defined(_WIN32)  // Win32 API, Defined by Microsoft and Borland compilers
   
   DWORD retVal;
   retVal = SetFilePointer(handle, LOWLONGLONG(offset), ((LONG*)&offset)+1, 
                           FILE_BEGIN);
   if (retVal == INVALID_SET_FILE_POINTER && GetLastError() != NO_ERROR)
      {
      TRACE_0(errout << "ERROR: CRawAudioFileIO::write: "
                     << "SetFilePointer failed for " << endl
                     << "       audio file " << m_fileName << endl
                     << "       because "
                     << GetLastSystemErrorMessage());
      return -1;
      }

   DWORD numberOfBytesWritten;
   BOOL retStatus;
   retStatus = WriteFile(handle, dataBuffer, size, &numberOfBytesWritten, 0);
   if (retStatus)
      return(numberOfBytesWritten);
   else
      {
      TRACE_0(errout << "ERROR: CRawAudioFileIO::write: "
                     << "WriteFile failed for " << endl
                     << "       audio file " << m_fileName << endl
                     << "       because "
                     << GetLastSystemErrorMessage());
      return -1;
      }

#else
#error Unknown compiler
#endif
}

//////////////////////////////////////////////////////////////////////

int CRawAudioFileIO::createFile(const string& fileName, MTI_INT64 size)
{
#if defined(__sgi) || defined(__linux)    // UNIX

   int handle;
   char data = 0;

   // Create the file
   handle = creat(fileName.c_str(), S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH);

   if (handle == -1)
      {
      TRACE_0(errout << "ERROR: CRawAudioFileIO::createFile failed to create file "
                     << "       " <<fileName << endl;
              errout << "       because " << strerror(errno) << endl;);
      return -1;
      }

   // Standard write function 
   // Equivalent to a lseek64 followed by write
   if(pwrite64(handle, &data, 1, size - 1) == -1)
      {
      TRACE_0(errout << "ERROR: CRawAudioFileIO::createFile: pwrite failed for file "
                     << endl
                     << "       " << fileName << endl;
              errout << "       size=" << size << endl;
              errout << "       because " << strerror(errno) << endl;);
      return -1;  // Error
      }

   // Close the file
   ::close(handle);

   return 0;

#elif defined(_WIN32)      // Win32 API

   // Create the file
   HANDLE newHandle;
// Temporary fix, changed 0 to FILE_SHARE_READ | FILE_SHARE_WRITE
   newHandle = CreateFile(fileName.c_str(), GENERIC_READ | GENERIC_WRITE,
                          FILE_SHARE_READ | FILE_SHARE_WRITE,
                          0, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, 0);
   if (newHandle == INVALID_HANDLE_VALUE)
      {
      TRACE_0(errout << "ERROR: CRawAudioFileIO::createFile failed to create file "
                     << endl
                     << "       " << fileName << endl;
              errout << "       because " << GetLastSystemErrorMessage() << endl;);
      return CLIP_ERROR_FILE_CREATE_FAILED;
      }

   // Seek to the end of the file
   DWORD retVal;
   MTI_INT64 offset = size;
   retVal = SetFilePointer(newHandle, LOWLONGLONG(offset), ((LONG*)&offset)+1,
                           FILE_BEGIN);
   if (retVal == INVALID_SET_FILE_POINTER && GetLastError() != NO_ERROR)
      {
      TRACE_0(errout << "ERROR: CRawAudioFileIO::createFile: SetFilePointer failed for file "
                     << endl
                     << "       " << fileName << endl;
              errout << "       size=" << size << endl;
              errout << "       because " << GetLastSystemErrorMessage() << endl;);
      return CLIP_ERROR_FILE_CREATE_FAILED;
      }

   // Establish the actual size of the file by setting the EOF
   // No data is written to the file
   if(!SetEndOfFile(newHandle))
      {
      TRACE_0(errout << "ERROR: CRawAudioFileIO::createFile: SetEndOfFile failed for file "
                     << endl
                     << "       " << fileName << endl;
              errout << "       size=" << size << endl;
              errout << "       because " << GetLastSystemErrorMessage()
                     << endl;);
      return CLIP_ERROR_FILE_CREATE_FAILED;
      }

   // Close the file
   if(CloseHandle(newHandle))
      return 0;
   else
      return CLIP_ERROR_FILE_CLOSE_FAILED;

#else
#error Unknown compiler
#endif
}

//////////////////////////////////////////////////////////////////////

int CRawAudioFileIO::extendFile(MTI_INT64 size)
{
   MTI_INT64 currentFileSize = getFileSize();
   if (currentFileSize == -1)
      return -1;

#if defined(__sgi) || defined(__linux)    // UNIX

   char data = 0;

   // Standard write function
   // Equivalent to a lseek64 followed by write
   if(pwrite64(handle, &data, 1, currentFileSize + size - 1) == -1)
      {
      TRACE_0(errout << "ERROR: CRawAudioFileIO::extendFile: pwrite failed"
                     << endl;
              errout << "       size=" << size << endl;
              errout << "       because " << strerror(errno) << endl;);
      return -1;  // Error
      }

#elif defined(_WIN32)      // Win32 API

   // Seek to the end of the file
   DWORD retVal;
   MTI_INT64 offset = currentFileSize + size;
   retVal = SetFilePointer(handle, LOWLONGLONG(offset), ((LONG*)&offset)+1,
                           FILE_BEGIN);
   if (retVal == INVALID_SET_FILE_POINTER && GetLastError() != NO_ERROR)
      {
      TRACE_0(errout << "ERROR: CRawAudioFileIO::extendFile: SetFilePointer failed for file "
                     << endl
                     << "       " << m_fileName << endl;
              errout << "       size=" << offset << endl;
              errout << "       because " << GetLastSystemErrorMessage() << endl;);
      return CLIP_ERROR_MEDIA_ALLOCATION_FAILED;
      }

   // Establish the actual size of the file by setting the EOF
   // No data is written to the file
   if(!SetEndOfFile(handle))
      {
      TRACE_0(errout << "ERROR: CRawAudioFileIO::extendFile: SetEndOfFile failed for file "
                     << endl
                     << "       " << m_fileName << endl;
              errout << "       size=" << offset << endl;
              errout << "       because " << GetLastSystemErrorMessage()
                     << endl;);
      return CLIP_ERROR_MEDIA_ALLOCATION_FAILED;
      }

#else
#error Unknown compiler
#endif

#ifdef INIT_AUDIO_FILE
   if(setFileBytes(0, currentFileSize, size) != 0)
      return -1;
#endif // #ifdef INIT_AUDIO_FILE

   return 0;
}

//////////////////////////////////////////////////////////////////////

int CRawAudioFileIO::shortenFile(MTI_INT64 index, MTI_INT64 size)
{
   // mlm: to-do: should this delete the file if index == 0, i.e. the
   // file will be zero length at the end of this function?

   MTI_INT64 currentFileSize = getFileSize();
   if (currentFileSize == -1)
      return -1;

   // verify the amount to remove is at the end of the file

   /** @todo maybe allow index + size to be "near" currentFileSize to
    *  compensate for possibly imprecise calculation of samples in a
    *  field range.  The primary reason to allow a fudge factor would
    *  be to compensate for differences in where clip3 and clip5 begin
    *  the pattern of allocating samples to fields for formats with
    *  non-integral samples per field.
    */
   if (currentFileSize != index + size)
   {
   TRACE_0(errout << "ERROR: CRawAudioFileIO::shortenFile failed"
           << endl;
           errout << "       because currentFileSize != index + size" << endl;
           errout << "       currentFileSize: " << currentFileSize 
                  << " index: " << index << " size: " << size << endl;);

   if (index != 0)
    {
     // if we are deleting the audio media, override the error
     return -1;  // Error
    }
   }

#if defined(__sgi) || defined(__linux)    // UNIX
   // Standard write function
   // Equivalent to a lseek64 followed by write
   if(ftruncate64(handle, currentFileSize - size) == -1)
      {
      TRACE_0(errout << "ERROR: CRawAudioFileIO::shortenFile: ftruncate failed"
                     << endl;
              errout << "       size=" << size << endl;
              errout << "       because " << strerror(errno) << endl;);
      return -1;  // Error
      }

#elif defined(_WIN32)      // Win32 API

   // Seek to the new end of the file
   DWORD retVal;
   MTI_INT64 offset = currentFileSize - size;
   retVal = SetFilePointer(handle, LOWLONGLONG(offset), ((LONG*)&offset)+1,
                           FILE_BEGIN);
   if (retVal == INVALID_SET_FILE_POINTER && GetLastError() != NO_ERROR)
      {
      TRACE_0(errout << "ERROR: CRawAudioFileIO::shortenFile: SetFilePointer failed for file "
                     << endl
                     << "       " << m_fileName << endl;
              errout << "       size=" << offset << endl;
              errout << "       because " << GetLastSystemErrorMessage()
                     << endl;);
      return CLIP_ERROR_FILE_SEEK_FAILED;
      }

   // Establish the actual size of the file by setting the EOF
   // No data is written to the file
   if(!SetEndOfFile(handle))
      {
      TRACE_0(errout << "ERROR: CRawAudioFileIO::shortenFile: SetEndOfFile failed for file "
                     << endl
                     << "       " << m_fileName << endl;
              errout << "       size=" << offset << endl;
              errout << "       because " << GetLastSystemErrorMessage()
                     << endl;);
      return CLIP_ERROR_FILE_SEEK_FAILED;
      }

#else
#error Unknown compiler
#endif

   return 0;
}

//////////////////////////////////////////////////////////////////////

int CRawAudioFileIO::setFileBytes(unsigned char value, MTI_INT64 offset,
                                  MTI_INT64 byteCount)
{
#if defined(__sgi) || defined(__linux)        // UNIX
   if (handle == -1)
      return -1;  // ERROR: File is not open

#elif defined(_WIN32)      // Windows, VC++ or BC++
   if (handle == INVALID_HANDLE_VALUE)
      return -1;  // ERROR: File is not open

#endif

   // Large buffer to write from
   size_t blockSize = 1024;
   size_t bufferSize = blockSize * 32;
   MTI_UINT8 *buffer = new MTI_UINT8[bufferSize];

   // Set all bytes in buffer to caller's value
   memset(buffer, value, bufferSize);

   size_t writeSize = bufferSize;  // Number of bytes to write
   int retVal;

   // On each iteration, write a buffer's worth of data.  Last iteration
   // may write less than a full buffer.
   while (byteCount > 0)
      {
      if (byteCount < bufferSize)
         writeSize = (size_t)byteCount; // Remaining file is smaller than buffer

      // Write the buffer, filled with caller's value, to the file
      retVal = write(buffer, offset, writeSize);
      if (retVal == -1 || (unsigned int)retVal != writeSize)
         {
         delete [] buffer;
         return -1;  // Write error
         }

      // Prepare for next iteration
      offset += writeSize;      // Advance file offset
      byteCount -= writeSize;    // Reduce remaining bytes to write
      }

   delete [] buffer;

   return 0;
}

int CRawAudioFileIO::setFileBytes(unsigned char value)
{
   // Get the size, in bytes, of the raw audio file
   MTI_INT64 fileSize = getFileSize();
   if (fileSize < 0)
      return -1; // ERROR: Could not get the size

   return setFileBytes(value, 0, fileSize);
}

MTI_INT64 CRawAudioFileIO::getFileSize()
{
#if defined(__sgi) || defined(__linux)        // UNIX
   if (handle == -1)
      return -1;  // ERROR: File is not open

   MTI_INT64 fileSize;

   fileSize = lseek64(handle, 0, SEEK_END);

   return fileSize;

#elif defined(_WIN32)      // Windows, VC++ or BC++
   if (handle == INVALID_HANDLE_VALUE)
      return -1;  // ERROR: File is not open

   LARGE_INTEGER offset;
   offset.QuadPart = 0;
   offset.LowPart = SetFilePointer(handle, offset.LowPart, &(offset.HighPart),
                                   FILE_END);
   if (offset.LowPart == INVALID_SET_FILE_POINTER && GetLastError() != NO_ERROR)
      return -1;   // ERROR: SetFilePointer failed.

   return offset.QuadPart;

#endif
}

//////////////////////////////////////////////////////////////////////

bool CRawAudioFileIO::checkFileExists(const string& fileName)
{
   struct stat buf;

   // Get file status information
   int result = stat(fileName.c_str(), &buf);

   if (result == 0)
      {
      // File exists
      return true;
      }
   
   if (errno == ENOENT)
      {
      // File or path could not be found
      return false;
      }

   // Assume file exists even though stat failed
   return true;
}

//////////////////////////////////////////////////////////////////////

int CRawAudioFileIO::deleteFile(const string &fileName)
{
#if defined(__sgi) || defined(__linux)    // UNIX

   // Delete the file
   int retVal = unlink(fileName.c_str());

   if (retVal != 0)
      {
      TRACE_3(errout << "CRawAudioFileIO::deleteFile failed to unlink file "
                     << fileName << endl;
              errout << strerror(errno) << endl;);
      return retVal;
      }

#elif defined(_WIN32)      // Win32 API

   // Delete the file
   int retVal = DeleteFile(fileName.c_str());

   if (retVal == 0)
      {
      TRACE_3(errout << "CRawAudioFileIO::deleteFile failed to delete file "
                     << fileName << endl;
              errout << GetLastSystemErrorMessage() << endl;);
      return CLIP_ERROR_REMOVE_FILE_FAILED;
      }
#endif

   return 0;
}

