// RawMediaAccess.cpp: implementation of the CRawMediaAccess class.
//
//////////////////////////////////////////////////////////////////////

#ifdef _MSC_VER
// Disable warning C4786: symbol greater than 255 characters,
// Work-around for problem when using Microsoft compiler
// with vector of strings or string pointers
#pragma warning(disable : 4786)
#endif // #ifdef _MSC_VER

#include <iostream>
using std::cout;

#include "Bridge.h"
#include "Clip3.h"
#include "err_clip.h"
#include "ImageFormat3.h"
#include "IniFile.h"
#include "MTImalloc.h"
#include "MTIstringstream.h"
#include "RawDiskMediaAllocator.h"
#include "RawFileIO.h"
#include "RawMediaAccess.h"
#include "RawMediaDeallocator.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRawMediaAccess::CRawMediaAccess()
: CMediaAccess(MEDIA_TYPE_RAW), nonContiguousMinFrames(0), 
  m_gangRead(MEDIA_GANG_IO_TWO_FIELDS), m_gangWrite(MEDIA_GANG_IO_TWO_FIELDS),
  m_maxOverlappedReads(1), m_maxOverlappedWrites(1),
  m_timingReport(0)
{
   // Add one NULL entry to file IO list.  This will be set when
   // the Video Store (raw) file is opened for the first time
   fileIOList.push_back(0);
}


CRawMediaAccess::~CRawMediaAccess()
{
   DeleteFileIOList();
}

//////////////////////////////////////////////////////////////////////

int CRawMediaAccess::initialize(CIniFile *ini, const string &sectionName)
{
   int retVal;

   // Read information from the raw disk configuration file
   retVal = readRawDiskSection(ini, sectionName);
   if (retVal != 0)
      return retVal;

   return 0;  // Success
}

int CRawMediaAccess::readRawDiskSection(CIniFile *ini,
                                        const string &sectionName)
{
   // Read information about a single raw disk from a section in the
   // Raw Disk Config file.  Set member variables in this instance of
   // CRawMediaAccess

   // Keyword strings
   string mediaIdentifierKey = "MediaIdentifier";
   string rawVideoDirectoryKey = "RawVideoDirectory";
   string freeListFileKey = "FreeListFile";
   string rawDiskSizeKey = "RawDiskSize";
   string fastIOBoundaryKey = "FastIOBoundary";

   // Check existence of each required keyword
   if (!checkRawDiskConfigKey(ini, sectionName, mediaIdentifierKey)
       || !checkRawDiskConfigKey(ini, sectionName, rawVideoDirectoryKey)
       || !checkRawDiskConfigKey(ini, sectionName, freeListFileKey)
       || !checkRawDiskConfigKey(ini, sectionName, rawDiskSizeKey)
       || !checkRawDiskConfigKey(ini, sectionName, fastIOBoundaryKey))
      {
      // At least one keyword is missing from the raw disk config section
      return CLIP_ERROR_RAW_DISK_CFG_MISSING_KEYWORD;
      }

   string emptyString;

   // Read information from the raw disk configuration file and
   // save in member variables
   mediaIdentifier = ini->ReadString(sectionName, mediaIdentifierKey,
                                     emptyString);

   rawVideoDirectory = ini->ReadFileName(sectionName, rawVideoDirectoryKey,
                                         emptyString);

   freeListFileName = ini->ReadFileName(sectionName, freeListFileKey,
                                        emptyString);

   rawDiskSize = ini->ReadInteger(sectionName, rawDiskSizeKey, 0);

   fastIOBoundary = ini->ReadInteger(sectionName, fastIOBoundaryKey, 1);

   // Allocation Method and Minimum Allocation Unit
   string allocMethodKey = "AllocMethod";
   string allocContiguousFirstAvailable = "ALLOC_CONTIGUOUS_FIRST_AVAILABLE";
   string allocContiguousSmallestAvailable = "ALLOC_CONTIGUOUS_SMALLEST_AVAILABLE";
   string allocNonContiguous = "ALLOC_NON_CONTIGUOUS";
   string allocMethodDefault = allocNonContiguous;

   string allocMethodValueStr;
   if (checkRawDiskConfigKey(ini, sectionName, allocMethodKey))
      {
      allocMethodValueStr = ini->ReadString(sectionName, allocMethodKey,
                                            allocMethodDefault);
      }
   else
      {
      // The Allocation Method keyword is not present, so write a
      // default value
      allocMethodValueStr = allocMethodDefault;
      ini->WriteString(sectionName, allocMethodKey, allocMethodDefault);
      }

   if (allocMethodValueStr == allocContiguousFirstAvailable)
      allocMethod = RF_ALLOC_CONTIGUOUS_FIRST_AVAILABLE;
   else if (allocMethodValueStr == allocContiguousSmallestAvailable)
      allocMethod = RF_ALLOC_CONTIGUOUS_SMALLEST_AVAILABLE;
   else if (allocMethodValueStr == allocNonContiguous)
      allocMethod = RF_ALLOC_NON_CONTIGUOUS;
   else
      return CLIP_ERROR_RAW_DISK_CFG_BAD_ALLOC_METHOD;

   if (allocMethod == RF_ALLOC_NON_CONTIGUOUS)
      {
      // Minimum allocation block size for non-contiguous allocation
      string nonContiguousMinFramesKey = "NonContiguousMinFrames";
      if(checkRawDiskConfigKey(ini, sectionName, nonContiguousMinFramesKey))
         {
         nonContiguousMinFrames = ini->ReadInteger(sectionName,
                                                   nonContiguousMinFramesKey,
                                          RF_DEFAULT_NON_CONTIGUOUS_MIN_FRAMES);
         if (nonContiguousMinFrames < 1)
            nonContiguousMinFrames = RF_DEFAULT_NON_CONTIGUOUS_MIN_FRAMES;
         }
      else
         {
         nonContiguousMinFrames = RF_DEFAULT_NON_CONTIGUOUS_MIN_FRAMES;
         ini->WriteInteger(sectionName, nonContiguousMinFramesKey,
                           RF_DEFAULT_NON_CONTIGUOUS_MIN_FRAMES);
         }
      }
   else
      {
      nonContiguousMinFrames = 0;
      }

   // Raw disk performance parameters
   string gangReadKey = "GangRead";
   string gangWriteKey = "GangWrite";
   string gangNone = "None";
   string gangTwoFields = "TwoFields";
   string gangMultiFields = "MultiFields";
   string gangDefault = gangTwoFields;

   string gangReadStr = ini->ReadStringCreate(sectionName, gangReadKey,
                                              gangDefault);
   if (gangReadStr == gangNone)
      m_gangRead = MEDIA_GANG_IO_NONE;
   else if (gangReadStr == gangTwoFields)
      m_gangRead = MEDIA_GANG_IO_TWO_FIELDS;
   else if (gangReadStr == gangMultiFields)
      m_gangRead = MEDIA_GANG_IO_MULTI_FIELDS;

   string gangWriteStr = ini->ReadStringCreate(sectionName, gangWriteKey,
                                               gangDefault);
   if (gangWriteStr == gangNone)
      m_gangWrite = MEDIA_GANG_IO_NONE;
   else if (gangWriteStr == gangTwoFields)
      m_gangWrite = MEDIA_GANG_IO_TWO_FIELDS;
   else if (gangWriteStr == gangMultiFields)
      m_gangWrite = MEDIA_GANG_IO_MULTI_FIELDS;

   string maxOverlapReadKey = "MaxOverlappedReads";
   string maxOverlapWriteKey = "MaxOverlappedWrites";

   m_maxOverlappedReads = ini->ReadIntegerCreate(sectionName, maxOverlapReadKey,
                                                 1);
   if (m_maxOverlappedReads < 1)
      m_maxOverlappedReads = 1;
   else if (m_maxOverlappedReads > RF_MAX_OVERLAPPED_IO_COUNT)
      m_maxOverlappedReads = RF_MAX_OVERLAPPED_IO_COUNT;

   m_maxOverlappedWrites = ini->ReadIntegerCreate(sectionName,
                                                  maxOverlapWriteKey,
                                                  1);
   if (m_maxOverlappedWrites < 1)
      m_maxOverlappedWrites = 1;
   else if (m_maxOverlappedWrites > RF_MAX_OVERLAPPED_IO_COUNT)
      m_maxOverlappedWrites = RF_MAX_OVERLAPPED_IO_COUNT;

   // Reporting/debugging magic
   m_timingReport = ini->ReadInteger(sectionName, "TimingReport", 0);

   return 0;
}

bool CRawMediaAccess::checkRawDiskConfigKey(CIniFile *ini,
                                            const string &sectionName,
                                            const string &key)
{
   if (ini->KeyExists(sectionName, key))
      {
      return true;
      }
   else
      {
      TRACE_0(errout << "ERROR: Key: " << key
                     << " is missing from Raw Disk Config file section ["
                     << sectionName << "]");
      return false;
      }

}

//////////////////////////////////////////////////////////////////////

int CRawMediaAccess::preload()
{
   // Open raw disk if necessary
   if (fileIOList[0] == 0)
      {
      if (openRawIO() != 0)
         return CLIP_ERROR_RAW_DISK_OPEN_FAILED;
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////

int CRawMediaAccess::read(MTI_UINT8* buffer, CMediaLocation &mediaLocation)
{
   int retVal;

   retVal = readMT(buffer, mediaLocation, 0);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CRawMediaAccess::readMT(MTI_UINT8* buffer, CMediaLocation &mediaLocation,
                          int fileHandleIndex)
{
   int retVal;

   retVal = CheckFileHandleIndex(fileHandleIndex);
   if (retVal != 0)
      return retVal;

   // Open raw disk if necessary
   if (fileHandleIndex == 0 && fileIOList[0] == 0)
      {
      if (openRawIO() != 0)
         return CLIP_ERROR_RAW_DISK_OPEN_FAILED;
      }

   int status;
   unsigned int size = mediaLocation.getDataSize();
   status = fileIOList[fileHandleIndex]->read(buffer,
                                              mediaLocation.getDataIndex(),
                                              size);
   if (status == -1 || (unsigned int)status != size)
      {
      // Error: Read failed
      return CLIP_ERROR_RAW_DISK_READ_FAILED;
      }
   
   return 0;

}

// --------------------------------------------------------------------------

int CRawMediaAccess::write(const MTI_UINT8* buffer,
                           CMediaLocation &mediaLocation)
{
   // Open raw disk if necessary
   if (fileIOList[0] == 0)
      {
      if (openRawIO() != 0)
         return CLIP_ERROR_RAW_DISK_OPEN_FAILED;
      }

   int status;
   unsigned int size = mediaLocation.getDataSize();
   status = fileIOList[0]->write(buffer, mediaLocation.getDataIndex(), size);
   if (status == -1 || (unsigned int)status != size)
      {
      // Error: Write failed
      return CLIP_ERROR_RAW_DISK_WRITE_FAILED;
      }
   
   return 0;
}

// --------------------------------------------------------------------------

int CRawMediaAccess::read(MTI_UINT8* buffer, MTI_INT64 offset,
                                   unsigned int size)
{
   int retVal;

   retVal = readMT(buffer, offset, size, 0);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CRawMediaAccess::readMT(MTI_UINT8* buffer, MTI_INT64 offset,
                          unsigned int size, int fileHandleIndex)
{
   int retVal;

   retVal = CheckFileHandleIndex(fileHandleIndex);
   if (retVal != 0)
      return retVal;

   // Open Video Store file if necessary
   if (fileHandleIndex == 0 && fileIOList[0] == 0)
      {
      if (openRawIO() != 0)
         return CLIP_ERROR_RAW_DISK_OPEN_FAILED;
      }

   int status;
   status = fileIOList[fileHandleIndex]->read(buffer, offset, size);
   if (status == -1 || (unsigned int)status != size)
      {
      // Error: Read failed
      return CLIP_ERROR_RAW_DISK_READ_FAILED;
      }
   
   return 0;

}

int CRawMediaAccess::write(const MTI_UINT8* buffer, MTI_INT64 offset,
                           unsigned int size)
{
   // Open Video Store file if necessary
   if (fileIOList[0] == 0)
      {
      if (openRawIO() != 0)
         return CLIP_ERROR_RAW_DISK_OPEN_FAILED;
      }

   int status;
   status = fileIOList[0]->write(buffer, offset, size);
   if (status == -1 || (unsigned int)status != size)
      {
      // Error: Write failed
      return CLIP_ERROR_RAW_DISK_WRITE_FAILED;
      }
   
   return 0;
}

// --------------------------------------------------------------------------

int CRawMediaAccess::readMultipleMT(int count, MTI_UINT8** bufferArray,
                                    CMediaLocation mediaLocationArray[],
                                    int fileHandleIndex)
{
   int retVal;

   if (count < 1)
      return 0;

   retVal = CheckFileHandleIndex(fileHandleIndex);
   if (retVal != 0)
      return retVal;

   MTI_INT64 *offsetArray = new MTI_INT64[count];
   int *sizeArray = new int[count];
   MTI_UINT8** sortedBuffers = new MTI_UINT8*[count];

   int gangedCount;
   gangBlocks(m_gangRead, count, bufferArray, mediaLocationArray,
              offsetArray, sizeArray, sortedBuffers, gangedCount);

   retVal = fileIOList[fileHandleIndex]->readMulti(sortedBuffers, offsetArray,
                                                   sizeArray, gangedCount);
   if (retVal != 0)
      {
      delete [] offsetArray;
      delete [] sizeArray;
      delete [] sortedBuffers;
      return retVal;
      }

   delete [] offsetArray;
   delete [] sizeArray;
   delete [] sortedBuffers;
   return 0;
}

// --------------------------------------------------------------------------

int CRawMediaAccess::writeMultiple(int count, MTI_UINT8* bufferArray[],
                                   CMediaLocation mediaLocationArray[])
{
   int retVal;

   // Open Video Store file if necessary
   if (fileIOList[0] == 0)
      {
      if (openRawIO() != 0)
         return CLIP_ERROR_RAW_DISK_OPEN_FAILED;
      }

   MTI_INT64 *offsetArray = new MTI_INT64[count];
   int *sizeArray = new int[count];
   MTI_UINT8** sortedBuffers = new MTI_UINT8*[count];

   int gangedCount;
   gangBlocks(m_gangWrite, count, bufferArray, mediaLocationArray,
              offsetArray, sizeArray, sortedBuffers, gangedCount);

   retVal = fileIOList[0]->writeMulti(sortedBuffers, offsetArray, sizeArray,
                                      gangedCount);
   if (retVal != 0)
      {
      delete [] offsetArray;
      delete [] sizeArray;
      delete [] sortedBuffers;
      return retVal;
      }

   delete [] offsetArray;
   delete [] sizeArray;
   delete [] sortedBuffers;
   return 0;
}

// --------------------------------------------------------------------------

int CRawMediaAccess::gangBlocks(EMediaGangIO gangType,
                                int count, MTI_UINT8* bufferArray[],
                                CMediaLocation mediaLocationArray[],
                                MTI_INT64 *offsetArray, int *sizeArray,
                                MTI_UINT8** sortedBuffers, int &gangedCount)
{
   int *indexArray = new int[count];

   SortMediaLocations(count, indexArray, mediaLocationArray);

   // Gang if possible and VideoStore so configured
   if (gangType == MEDIA_GANG_IO_NONE)
      {
      // Don't attempt to gang reads
      for (int i =  0; i < count; ++i)
         {
         offsetArray[i] = mediaLocationArray[indexArray[i]].getDataIndex();
         sizeArray[i] = mediaLocationArray[indexArray[i]].getDataSize();
         sortedBuffers[i] = bufferArray[indexArray[i]];
         }
      gangedCount = count;
      }
   else if (gangType == MEDIA_GANG_IO_TWO_FIELDS)
      {
      // Gang pairs of media locations.  Treat pairs of media locations
      // as two fields in a frame.  This is roughly the case for 525, 625
      // and 1080i, but not the case for 720p or 1080p.  Close enough.
      CMediaLocation *m0, *m1;
      MTI_UINT8 *b0, *b1;
      int s0;
      int outIndex = 0;
      for (int i = 0; i < count; i += 2)
         {
         m0 = &(mediaLocationArray[indexArray[i]]);
         m1 = &(mediaLocationArray[indexArray[i+1]]);
         s0 = m0->getDataSize();
         b0 = bufferArray[indexArray[i]];
         b1 = bufferArray[indexArray[i+1]];
         if (b0 + s0 == b1 &&
             m0->getDataIndex() + s0 == m1->getDataIndex())
            {
            offsetArray[outIndex] = m0->getDataIndex();
            sizeArray[outIndex] = s0 + m1->getDataSize();
            sortedBuffers[outIndex] = b0;
            }
         else
            {
            offsetArray[outIndex] = m0->getDataIndex();
            sizeArray[outIndex] = s0;
            sortedBuffers[outIndex] = b0;
            ++outIndex;
            offsetArray[outIndex] = m1->getDataIndex();
            sizeArray[outIndex] = m1->getDataSize();
            sortedBuffers[outIndex] = b1;
            }
         ++outIndex;
         }
      if (count & 1)
         {
         int i = count - 1;
         offsetArray[i] = mediaLocationArray[indexArray[i]].getDataIndex();
         sizeArray[i] = mediaLocationArray[indexArray[i]].getDataSize();
         sortedBuffers[i] = bufferArray[indexArray[i]];
         ++outIndex;
         }
      gangedCount = outIndex;
      }
   else if (gangType == MEDIA_GANG_IO_MULTI_FIELDS)
      {
      // Gang as many as possible
      CMediaLocation *m0, *m1;
      MTI_UINT8 *b0, *b1;
      int s0;
      int outIndex = 0;
      m0 = &(mediaLocationArray[indexArray[0]]);
      s0 = m0->getDataSize();
      b0 = bufferArray[indexArray[0]];
      offsetArray[outIndex] = m0->getDataIndex();
      sizeArray[outIndex] = s0;
      sortedBuffers[outIndex] = b0;
      for (int i = 1; i < count; ++i)
         {
         m1 = &(mediaLocationArray[indexArray[i]]);
         b1 = bufferArray[indexArray[i]];
         if (b0 + s0 == b1 &&
             m0->getDataIndex() + s0 == m1->getDataIndex())
            {
            // The disk data and memory buffers are contiguous,
            // so gang the new block to the end of the current block
            sizeArray[outIndex] += m1->getDataSize();
            s0 += m1->getDataSize();
            }
         else
            {
            // Either the disk data or the memory buffers are not contiguous,
            // so start a new block
            m0 = m1;
            s0 = m0->getDataSize();
            b0 = b1;
            ++outIndex;
            offsetArray[outIndex] = m0->getDataIndex();
            sizeArray[outIndex] = s0;
            sortedBuffers[outIndex] = b0;
            }
         }
      gangedCount = outIndex + 1;
      }

   delete [] indexArray;

   return 0;
}
// --------------------------------------------------------------------------

int CRawMediaAccess::destroy(const CMediaLocation &mediaLocation)
{
   CRawMediaDeallocator rawMediaDeallocator;
   rawMediaDeallocator.freeMediaAllocation(mediaLocation);
   return deallocate(rawMediaDeallocator);
}
// --------------------------------------------------------------------------

int CRawMediaAccess::openRawIO()
{
   // Check first entry in fileIOList
   if (fileIOList[0] == 0)
      {
      CRawFileIO *newFileIO = new CRawFileIO(m_maxOverlappedReads,
                                             m_maxOverlappedWrites,
                                             0, m_timingReport);
      int retVal = newFileIO->open(rawVideoDirectory);
      if (retVal != 0)
         {
         delete newFileIO;
         return retVal;
         }
      fileIOList[0] = newFileIO;
      }

   return 0;
}
// --------------------------------------------------------------------------

int CRawMediaAccess::CreateNewFileHandle()
{
   // Make sure the default file handle exists before making
   // an extra file handle
   if (fileIOList[0] == 0)
      {
      if (openRawIO() != 0)
         return -1;
      }

   // Create a new instance of CRawFileIO
   CRawFileIO *newFileIO = new CRawFileIO(m_maxOverlappedReads,
                                          m_maxOverlappedWrites,
                                          fileIOList.size(), m_timingReport);

   // Open the Video Store file for reading only
   int retVal = newFileIO->openReadOnly(rawVideoDirectory);
   if (retVal != 0)
      {
      delete newFileIO;
      return -1;
      }

   // Add an entry to the fileIOList
   fileIOList.push_back(newFileIO);

   // Return  index of new entry in fileIOList
   return fileIOList.size() - 1;
}

int CRawMediaAccess::ReleaseFileHandle(int fileHandleIndex)
{
   if (fileHandleIndex == 0)
      return 0;   // never release default file handle index
      
   if (fileHandleIndex < 1 || fileHandleIndex >= (int)fileIOList.size())
      return CLIP_ERROR_INVALID_MEDIA_FILE_HANDLE_INDEX;

   delete fileIOList[fileHandleIndex];    // Delete CFileIO, which closes file

   fileIOList[fileHandleIndex] = 0;

   return 0;
}

int CRawMediaAccess::DeleteFileIOList()
{
   for (int i = 0; i < (int)fileIOList.size(); ++i)
      {
      delete fileIOList[i];
      fileIOList[i] = 0;
      }

   return 0;
}

int CRawMediaAccess::CheckFileHandleIndex(int fileHandleIndex)
{
   // Check if file handle index is out-of-range
   if (fileHandleIndex < 0 || fileHandleIndex >= (int)fileIOList.size())
      return CLIP_ERROR_INVALID_MEDIA_FILE_HANDLE_INDEX;

   // Check if file handle index has already been released
   if (fileHandleIndex > 0 && fileIOList[fileHandleIndex] == 0)
      return CLIP_ERROR_INVALID_MEDIA_FILE_HANDLE_INDEX;

   return 0;
}

//////////////////////////////////////////////////////////////////////

MTI_UINT32 CRawMediaAccess::getAdjustedFieldSize(MTI_UINT32 fieldSize)
{
   MTI_UINT32 blockSize = fastIOBoundary * RF_BLOCK_SIZE;
   MTI_UINT32 newSize = (fieldSize + blockSize - 1) / blockSize;
   newSize *= blockSize;

   return newSize;
}

//////////////////////////////////////////////////////////////////////

MTI_INT64 CRawMediaAccess::adjustToBlocking(MTI_INT64 size, 
                                            bool roundingFlag)
{
   MTI_INT64 blockSize = fastIOBoundary * RF_BLOCK_SIZE;
   MTI_INT64 newSize = size;
   if (roundingFlag)
      newSize += blockSize - 1; 
   newSize /= blockSize;
   newSize *= blockSize;

   return newSize;
}

// ----------------------------------------------------------------------

int CRawMediaAccess::Peek(CMediaLocation &mediaLocation,
                          string &filename, MTI_INT64 &offset)
{
   filename = rawVideoDirectory;

   offset = mediaLocation.getDataIndex();

   return 0;
}

//////////////////////////////////////////////////////////////////////

int CRawMediaAccess::CheckRFL()
{
   int retVal;

   // Create instance of raw disk free list class
   CRawDiskFreeList freeList(freeListFileName);

   retVal = freeList.CheckRFL();
   if (retVal != 0)
      return retVal;

   return 0;
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//
// Function:   queryFreeSpace
//
// Description:Queries the raw disk's free list to determine the
//             total available free space on the disk and also the
//             largest available contiguous free area   
//
// Arguments:  MTI_INT64 *totalFreeBytes Ptr to caller's variable to accept
//                                       free size in bytes
//             MTI_INT64 *maxContiguousFreeBytes
//                                     Ptr to caller's variable to accept number of 
//                                     bytes in largest contiguous free space
// Returns:    Returns 0 if query succeed.  Non-zero if failed.
//             Returns non-zero if either of totalBlocks or 
//             maxContiguousBlocks is NULL pointer    
//
//////////////////////////////////////////////////////////////////////
int CRawMediaAccess::queryFreeSpace(MTI_INT64 *totalFreeBytes,
                                    MTI_INT64 *maxContiguousFreeBytes)
{
   // Check arguments for null pointers
   if (totalFreeBytes == 0 || maxContiguousFreeBytes == 0)
      return CLIP_ERROR_UNEXPECTED_NULL_ARGUMENT;

   // Create instance of raw disk free list class
   CRawDiskFreeList freeList(freeListFileName);

   // Query raw disk's free block list
   MTI_INT64 totalFreeBlocks = 0, maxContiguousFreeBlocks = 0;
   int status = freeList.queryFreeSpace(&totalFreeBlocks,
                                        &maxContiguousFreeBlocks);
   if (status != 0)
      {
      // Raw Disk Free List query failed, possibly missing or
      // corrupted free list file
      return status;
      }

   // Multiply block counts by 512 byte/block to get free byte counts
   *totalFreeBytes = totalFreeBlocks * RF_BLOCK_SIZE;
   *maxContiguousFreeBytes = maxContiguousFreeBlocks * RF_BLOCK_SIZE;

   return 0;
}

int CRawMediaAccess::queryFreeSpaceEx(const CMediaFormat& mediaFormat,
                                      MTI_INT64 *totalFreeBytes,
                                      MTI_INT64 *maxContiguousFreeBytes)
{
   int retVal;

   // Check arguments for null pointers
   if (totalFreeBytes == 0 || maxContiguousFreeBytes == 0)
      return CLIP_ERROR_UNEXPECTED_NULL_ARGUMENT;

   if (mediaFormat.getMediaFormatType() != MEDIA_FORMAT_TYPE_VIDEO3)
      {
      // ERROR: Unexpected media format, CRawMediaAccess only does video media
      return CLIP_ERROR_INVALID_MEDIA_FORMAT_TYPE;
      }

   const CImageFormat& imageFormat
                              = static_cast<const CImageFormat&>(mediaFormat);
   MTI_UINT32 fieldSize = imageFormat.getBytesPerField();

   // Adjust the field size by the disk blocking factors, then
   // multiply by the number of fields requested
   MTI_UINT32 paddedFieldSize = getAdjustedFieldSize(fieldSize);
   MTI_UINT32 paddedFrameSize = paddedFieldSize * mediaFormat.getFieldCount();

   // Create instance of raw disk free list class
   CRawDiskFreeList freeList(freeListFileName);

   // Query raw disk's free block list
   MTI_INT64 totalFreeBlocks = 0, maxContiguousFreeBlocks = 0;
   retVal = freeList.queryFreeSpaceEx(paddedFrameSize, allocMethod,
                                      nonContiguousMinFrames, &totalFreeBlocks,
                                      &maxContiguousFreeBlocks);
   if (retVal != 0)
      {
      // Raw Disk Free List query failed, possibly missing or
      // corrupted free list file
      return retVal;
      }

   // Multiply block counts by 512 byte/block to get free byte counts
   *totalFreeBytes = totalFreeBlocks * RF_BLOCK_SIZE;
   *maxContiguousFreeBytes = maxContiguousFreeBlocks * RF_BLOCK_SIZE;

   return 0;

} // queryFreeSpaceEx

int CRawMediaAccess::queryTotalSpace(MTI_INT64 *totalBytes)
{
    int retVal = 0;

    *totalBytes = rawDiskSize * RF_BLOCK_SIZE;

    return retVal;
}

//////////////////////////////////////////////////////////////////////

bool CRawMediaAccess::isClipSpecific() const
{
   return false;
}

//////////////////////////////////////////////////////////////////////
// Raw Disk Space Allocation and Deallocation functions
//////////////////////////////////////////////////////////////////////

int CRawMediaAccess::checkSpace(const CMediaFormat& mediaFormat,
                                MTI_UINT32 frameCount)
{
   int retVal;

   if (mediaFormat.getMediaFormatType() != MEDIA_FORMAT_TYPE_VIDEO3)
      {
      // ERROR: Unexpected media format, CRawMediaAccess only does video media
      return CLIP_ERROR_INVALID_MEDIA_FORMAT_TYPE;
      }

   if (openRawIO() != 0)
      return CLIP_ERROR_RAW_DISK_OPEN_FAILED;

   const CImageFormat& imageFormat
                              = static_cast<const CImageFormat&>(mediaFormat);
   MTI_UINT32 fieldSize = imageFormat.getBytesPerField();

   // Adjust the field size by the disk blocking factors, then
   // multiply by the number of fields requested
   MTI_UINT32 paddedFieldSize = getAdjustedFieldSize(fieldSize);
   MTI_UINT32 fieldCount = frameCount * mediaFormat.getFieldCount();
   MTI_INT64 paddedSize = (MTI_INT64)paddedFieldSize * fieldCount;

   // Find out how much space is available on this raw disk
   MTI_INT64 totalFreeBytes, maxContiguousFreeBytes;
//   int retVal = queryFreeSpace(&totalFreeBytes, &maxContiguousFreeBytes);
   retVal = queryFreeSpaceEx(imageFormat, &totalFreeBytes,
                             &maxContiguousFreeBytes);
   if (retVal != 0)
      return retVal; // ERROR: problem with raw disk free list

   if (paddedSize > maxContiguousFreeBytes)
      {
      // Not enough space
      return CLIP_ERROR_INSUFFICIENT_MEDIA_STORAGE;
      }

   return 0;  // Okay

} // checkSpace

CMediaAllocator* CRawMediaAccess::allocate(const CMediaFormat& mediaFormat,
                                           const CTimecode& frame0Timecode, 
                                           MTI_UINT32 frameCount)
{
   int retVal;

   if (mediaFormat.getMediaFormatType() != MEDIA_FORMAT_TYPE_VIDEO3)
      {
      // ERROR: Unexpected media format, CRawMediaAccess only does video media
      theError.set(CLIP_ERROR_INVALID_MEDIA_FORMAT_TYPE,
                   "CRawMediaAccess::allocate internal error");
      return 0; // return NULL pointer
      }

   const CImageFormat& imageFormat
                              = static_cast<const CImageFormat&>(mediaFormat);
   MTI_UINT32 fieldSize = imageFormat.getBytesPerField();

#ifdef NEW_DISK_ALLOCATION

   // Create instance of raw disk free list class.  
   CRawDiskFreeList freeList(freeListFileName);

   // Calculate the number of fields to allocate and their padded size
   MTI_UINT32 fieldCount = frameCount * mediaFormat.getFieldCount();
   MTI_UINT32 paddedFieldSize = getAdjustedFieldSize(fieldSize);
   int minFieldCount = mediaFormat.getFieldCount() * nonContiguousMinFrames;

   // Create an empty list of allocation units that will receive pointers
   // to the allocated disk blocks
   CRawDiskBlockList *allocList = new CRawDiskBlockList(paddedFieldSize);

   // Allocate the raw disk space using the requested allocation method
   retVal = freeList.allocate(fieldCount, paddedFieldSize,
                              mediaFormat.getInterlaced(),
                              allocMethod, minFieldCount, *allocList);

   if (retVal != 0)
      {
      // Raw disk allocation failed.  Clean up and report the error
      delete allocList;
      MTIostringstream ostrm;
      ostrm << "Could not allocate video media in videostore "
            << mediaIdentifier << ", ";
      if (retVal == CLIP_ERROR_INSUFFICIENT_MEDIA_STORAGE)
         ostrm << "insufficient free space";
      else
         ostrm << "error return " << retVal;
      theError.set(retVal, ostrm);
      TRACE_1(errout << "ERROR: " << ostrm.str());
      return 0;  // return NULL pointer
      }

   // Create a new media allocator object and return it to the caller
   return (new CRawDiskMediaAllocator(this, fieldCount, paddedFieldSize,
                                      allocList)); 

#else // OLD WAY
   // Adjust the field size by the disk blocking factors, then
   // multiply by the number of fields requested
   MTI_UINT32 fieldCount = frameCount * mediaFormat.getFieldCount();
   MTI_UINT32 paddedFieldSize = getAdjustedFieldSize(fieldSize);
   MTI_INT64 paddedSize = (MTI_INT64)paddedFieldSize * fieldCount;

   // Create instance of raw disk free list class
   CRawDiskFreeList freeList(freeListFileName);

   // Allocate contiguous space on the raw disk for all fields
   MTI_INT64 offset = freeList.allocate(paddedSize);
   if (offset == -1)
      return 0;

   // Create a new media allocator object and return it to caller
   return(new CRawDiskMediaAllocator(this, offset, paddedSize, paddedFieldSize,
                                     fieldCount));
#endif

} // allocate

// Most of this version clip hackery is identical to the allocate() method -
// I'm too sick of this to bother refactoring
CMediaAllocator* CRawMediaAccess::allocateOneFrame(
                                           const CMediaFormat& mediaFormat,
                                           const CTimecode& frame0Timecode, 
                                           MTI_UINT32 fieldCount)
{
   int retVal;

   if (mediaFormat.getMediaFormatType() != MEDIA_FORMAT_TYPE_VIDEO3)
      {
      // ERROR: Unexpected media format, CRawMediaAccess only does video media
      theError.set(CLIP_ERROR_INVALID_MEDIA_FORMAT_TYPE,
                   "CRawMediaAccess::allocateOneFrame internal error");
      return 0; // return NULL pointer
      }

   const CImageFormat& imageFormat
                              = static_cast<const CImageFormat&>(mediaFormat);
   MTI_UINT32 fieldSize = imageFormat.getBytesPerField();

   // Create instance of raw disk free list class.
   CRawDiskFreeList freeList(freeListFileName);

   // Calculate the number of fields to allocate and their padded size
   // passed in: MTI_UINT32 fieldCount = frameCount * mediaFormat.getFieldCount();
   MTI_UINT32 paddedFieldSize = getAdjustedFieldSize(fieldSize);
   int minFieldCount = fieldCount; //mediaFormat.getFieldCount() * nonContiguousMinFrames;

   // Create an empty list of allocation units that will receive pointers
   // to the allocated disk blocks
   CRawDiskBlockList *allocList = new CRawDiskBlockList(paddedFieldSize);

   // Allocate the raw disk space using the requested allocation method
   retVal = freeList.allocate(fieldCount, paddedFieldSize,
                              mediaFormat.getInterlaced(),
                              allocMethod, minFieldCount, *allocList);

   if (retVal != 0)
      {
      // Raw disk allocation failed.  Clean up and report the error
      delete allocList;
      MTIostringstream ostrm;
      ostrm << "Could not allocate one video frame in videostore "
            << mediaIdentifier << ", ";
      if (retVal == CLIP_ERROR_INSUFFICIENT_MEDIA_STORAGE)
         ostrm << "insufficient free space";
      else
         ostrm << "error return " << retVal;
      theError.set(retVal, ostrm);
      TRACE_1(errout << "ERROR: " << ostrm.str());
      return 0;  // return NULL pointer
      }

   // Create a new media allocator object and return it to the caller
   return (new CRawDiskMediaAllocator(this, fieldCount, paddedFieldSize,
                                      allocList)); 

} // allocate

int CRawMediaAccess::cancelAllocation(CMediaAllocator *mediaAllocator)
{
   int retVal;

   CRawDiskFreeList freeList(freeListFileName);

   CRawDiskMediaAllocator *rawMediaAllocator
                      = static_cast<CRawDiskMediaAllocator*>(mediaAllocator);

   CRawDiskBlockList *allocList = rawMediaAllocator->getAllocList();
   if (allocList == 0)
      {
      MTI_INT64 initialOffset = rawMediaAllocator->getInitialOffset();
      MTI_INT64 paddedSize = rawMediaAllocator->getPaddedSize();

      retVal = freeList.deallocate(initialOffset, paddedSize);
      if (retVal != 0)
         return retVal; // ERROR
      }
   else
      {
      retVal = freeList.deallocate(*allocList);
      if (retVal != 0)
         return retVal; // ERROR
      }

   return 0;
} // cancelAllocation

CMediaAllocator* CRawMediaAccess::extend(const CMediaFormat& mediaFormat,
                                         const CTimecode& frame0Timecode,
                                         MTI_UINT32 frameCount)
{
   return allocate(mediaFormat, frame0Timecode, frameCount);
}

int CRawMediaAccess::cancelExtend(CMediaAllocator *mediaAllocator)
{
   return cancelAllocation(mediaAllocator);
}

CMediaDeallocator* CRawMediaAccess::createDeallocator()
{
   CRawMediaDeallocator *deallocator;

   deallocator = new CRawMediaDeallocator;

   return deallocator;
}

int CRawMediaAccess::deallocate(CMediaDeallocator& deallocator)
{
   int retVal;

   // Create instance of raw disk free list class
   CRawDiskFreeList freeList(freeListFileName);

   retVal = freeList.deallocate(
     (static_cast<CRawMediaDeallocator&>(deallocator)).getMediaDeallocList());
   if (retVal != 0)
      return retVal;

   return 0;
}


//////////////////////////////////////////////////////////////////////
// Testing functions
//////////////////////////////////////////////////////////////////////

void CRawMediaAccess::dump(ostream &str)
{
   str << "RawMediaAccess " << std::endl;
   str << " Media Type: " << mediaType;
   str << "  Media Identifier: <" << mediaIdentifier << ">" << std::endl;
   str << " Raw Video Directory: <" << rawVideoDirectory << ">" << std::endl;
   str << " Raw Disk Size: " << rawDiskSize;
   str << "  Fast IO Boundary: " << fastIOBoundary << std::endl;
   str << " Free List File Name: <" << freeListFileName << ">" << std::endl;

   // Create instance of raw disk free list class
   CRawDiskFreeList freeList(freeListFileName);
   freeList.dump(str);
}

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//
//  CRawOGLVMediaAccess - Raw media storage via OGLV Bridge
//
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRawOGLVMediaAccess::CRawOGLVMediaAccess()
: CMediaAccess(MEDIA_TYPE_RAW_OGLV),
  OGLVAvailable(false),
  fastIOBoundary(1),
  fileIO(0)
{
   mediaIdentifier = "OGLV";

   oglvBr = new CBridge;
}


CRawOGLVMediaAccess::~CRawOGLVMediaAccess()
{
   delete oglvBr;   
}

//////////////////////////////////////////////////////////////////////

int CRawOGLVMediaAccess::initialize()
{
   TRACE_2(errout << "initializing raw OGLV media access");
   int status;

   string keyword;

   fileIO = 0;

   // Set OGLVAvailable to true iff OGLV bridge is available
   OGLVAvailable = false;
   status = oglvBr->openOGLV();
   if (status != 0)
      {
      TRACE_2(errout << "Could not open OGLV Bridge for media initialize:  "
                     << status );
      return status;
      }

   OGLVAvailable = true;

   // Set rawVideoDirectory, which holds name of raw disk volume
   rawVideoDirectory = oglvBr->getRawVideoDirectory();

   // Set fastIOBoundary, which holds the blocking factor for disk reads
   // and writes to achieve optimum performance.  Media storage is
   // allocated as an integral multiple of this value, given as
   // a number of 512 byte blocks.  Must be 1 or greater.
   fastIOBoundary = oglvBr->getFastIOBoundary();

   // close it up again
   status = oglvBr->closeOGLV();
   if (status != 0)
      {
      TRACE_0(errout << "ERROR: Could not close OGLV Bridge for media initialize"
                     << endl
                     << "       Return value: " << status );
      return status;
      }

   TRACE_2(errout << "OGLV Raw media access is initialized");

   return 0;

} // initialize

//////////////////////////////////////////////////////////////////////

int CRawOGLVMediaAccess::read(MTI_UINT8* buffer, CMediaLocation &mediaLocation)
{
   if (!OGLVAvailable)
      return CLIP_ERROR_RAW_OGLV_DISK_NOT_AVAILABLE;

   // Open raw disk if necessary
   if (!fileIO)
      {
      if (openRawIO() != 0)
         return CLIP_ERROR_RAW_OGLV_DISK_OPEN_FAILED;
      }

   int status;
   unsigned int size = mediaLocation.getDataSize();
   status = fileIO->read(buffer, mediaLocation.getDataIndex(), size);
   if (status == -1 || (unsigned int)status != size)
      {
      // Error: Read failed
      return CLIP_ERROR_RAW_OGLV_DISK_READ_FAILED;
      }
   
   return 0;
   
} // read

int CRawOGLVMediaAccess::write(const MTI_UINT8* buffer,
                               CMediaLocation &mediaLocation)
{
   if (!OGLVAvailable)
      return CLIP_ERROR_RAW_OGLV_DISK_NOT_AVAILABLE;

   // Open raw disk if necessary
   if (!fileIO)
      {
      if (openRawIO() != 0)
         return CLIP_ERROR_RAW_OGLV_DISK_OPEN_FAILED;
      }

   int status;
   unsigned int size = mediaLocation.getDataSize();
   status = fileIO->write(buffer, mediaLocation.getDataIndex(), size);
   if (status == -1 || (unsigned int)status != size)
      {
      // Error: Write failed
      return CLIP_ERROR_RAW_OGLV_DISK_WRITE_FAILED;
      }

   return 0;
}

int CRawOGLVMediaAccess::read(MTI_UINT8* buffer, MTI_INT64 offset, 
                              unsigned int size)
{
   if (!OGLVAvailable)
      return CLIP_ERROR_RAW_OGLV_DISK_NOT_AVAILABLE;

   // Open raw disk if necessary
   if (!fileIO)
      {
      if (openRawIO() != 0)
         return CLIP_ERROR_RAW_OGLV_DISK_OPEN_FAILED;
      }

   int status;
   status = fileIO->read(buffer, offset, size);
   if (status == -1 || (unsigned int)status != size)
      {
      // Error: Read failed
      return CLIP_ERROR_RAW_OGLV_DISK_READ_FAILED;
      }
   
   return 0;
}

int CRawOGLVMediaAccess::write(const MTI_UINT8* buffer, MTI_INT64 offset, 
                                    unsigned int size)
{
   if (!OGLVAvailable)
      return CLIP_ERROR_RAW_OGLV_DISK_NOT_AVAILABLE;

   // Open raw disk if necessary
   if (!fileIO)
      {
      if (openRawIO() != 0)
         return CLIP_ERROR_RAW_OGLV_DISK_OPEN_FAILED;
      }

   int status;
   status = fileIO->write(buffer, offset, size);
   if (status == -1 || (unsigned int)status != size)
      {
      // Error: Write failed
      return CLIP_ERROR_RAW_OGLV_DISK_WRITE_FAILED;
      }

   return 0;
}

int CRawOGLVMediaAccess::openRawIO()
{
   if (!fileIO)
      {
      fileIO = new CRawFileIO(1, 1, 0, 0);
      return (fileIO->open(rawVideoDirectory));
      }
   else
      {
      // Open already
      return 0;
      }
}

//////////////////////////////////////////////////////////////////////

MTI_UINT32 CRawOGLVMediaAccess::getAdjustedFieldSize(MTI_UINT32 fieldSize)
{
   MTI_UINT32 blockSize = fastIOBoundary * RF_BLOCK_SIZE;
   MTI_UINT32 newSize = (fieldSize + blockSize - 1) / blockSize;
   newSize *= blockSize;

   return newSize;  // Adjusted size, in bytes
}

//////////////////////////////////////////////////////////////////////

MTI_INT64 CRawOGLVMediaAccess::adjustToBlocking(MTI_INT64 size,
                                                bool roundingFlag)
{
   MTI_INT64 blockSize = fastIOBoundary * RF_BLOCK_SIZE;
   MTI_INT64 newSize = size;
   if (roundingFlag)
      newSize += blockSize - 1; 
   newSize /= blockSize;
   newSize *= blockSize;

   return newSize;  // Adjusted size, in bytes
}

// ----------------------------------------------------------------------

int CRawOGLVMediaAccess::Peek(CMediaLocation &mediaLocation,
                          string &filename, MTI_INT64 &offset)
{
   filename = "";

   offset = 0;

   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//
// Function:   queryFreeSpace
//
// Description:Queries the OGLV raw disk's free list to determine the
//             total available free space on the disk and also the
//             largest available contiguous free area   
//
// Arguments:  MTI_INT64 *totalFreeBytes Ptr to caller's variable to accept
//                                       free size in bytes
//             MTI_INT64 *maxContiguousFreeBytes
//                                     Ptr to caller's variable to accept number of
//                                     bytes in largest contiguous free space
// Returns:    Returns 0 if query succeed.  Non-zero if failed.
//
//////////////////////////////////////////////////////////////////////
int CRawOGLVMediaAccess::queryFreeSpace(MTI_INT64 *totalFreeBytes,
                                        MTI_INT64 *maxContiguousFreeBytes)
{
   // Assume no free space
   MTI_INT64 totalFreeBlocks = 0;
   MTI_INT64 maxContiguousFreeBlocks = 0;
   int status;

   if (OGLVAvailable)
      {
      // open up the bridge
      status = oglvBr->openOGLV();
      if (status != 0)
         {
         TRACE_0(errout << "ERROR: Could not open OGLV Bridge"
                        << endl
                        << "       Return value: " << status );
         return status;
         }

      // Query OGLV Bridge code to determine the total available
      // free space on the raw disk and also the largest contiguous
      // free space on the raw disk.
      totalFreeBlocks = oglvBr->getTotalFreeBlocks();
      maxContiguousFreeBlocks = oglvBr->getMaxContiguousFreeBlocks();

      // close it up
      status = oglvBr->closeOGLV();
      if (status != 0)
         {
         TRACE_0(errout << "ERROR: Could not close OGLV Bridge"
                        << endl
                        << "       Return value: " << status );
         return status;
         }
      }

   // Multiply block counts by 512 byte/block to get free byte counts
   // and return via caller's pointers
   if (totalFreeBytes != 0)
      *totalFreeBytes = totalFreeBlocks * RF_BLOCK_SIZE;
   if (maxContiguousFreeBytes != 0)
      *maxContiguousFreeBytes = maxContiguousFreeBlocks * RF_BLOCK_SIZE;

   return 0;
}

//////////////////////////////////////////////////////////////////////

bool CRawOGLVMediaAccess::isClipSpecific() const
{
   // mlm: I think this is right.  Maybe this breaks the bridge.  Does
   // anyone still use the bridge? 2005-10-12
   return false;
}

//////////////////////////////////////////////////////////////////////
// Raw Disk Space Allocation and Deallocation functions
//////////////////////////////////////////////////////////////////////

int CRawOGLVMediaAccess::checkSpace(const CMediaFormat& mediaFormat,
                                    MTI_UINT32 frameCount)
{
   if (mediaFormat.getMediaFormatType() != MEDIA_FORMAT_TYPE_VIDEO3)
      {
      // ERROR: Unexpected media format, CRawMediaAccess only does video media
      return CLIP_ERROR_INVALID_MEDIA_FORMAT_TYPE;
      }

   if (!OGLVAvailable)
      {
      // OGLV Bridge is not available
      return CLIP_ERROR_RAW_OGLV_DISK_NOT_AVAILABLE;
      }

   const CImageFormat& imageFormat
                              = static_cast<const CImageFormat&>(mediaFormat);
   MTI_UINT32 fieldSize = imageFormat.getBytesPerField();

   // Adjust the field size by the disk blocking factors, then
   // multiply by the number of fields requested
   MTI_UINT32 paddedFieldSize = getAdjustedFieldSize(fieldSize);
   MTI_UINT32 fieldCount = frameCount * mediaFormat.getFieldCount();
   MTI_INT64 paddedSize = (MTI_INT64)paddedFieldSize * fieldCount;

   // Find out how much space is available on this raw disk
   MTI_INT64 totalFreeBytes, maxContiguousFreeBytes;
   int retVal = queryFreeSpace(&totalFreeBytes, &maxContiguousFreeBytes);
   if (retVal != 0)
      return retVal; // ERROR: problem with raw disk free list

   if (paddedSize > maxContiguousFreeBytes)
      {
      // Not enough space
      return CLIP_ERROR_INSUFFICIENT_MEDIA_STORAGE;
      }

   return 0;  // Okay

} // checkSpace

CMediaAllocator* CRawOGLVMediaAccess::allocate(const CMediaFormat& mediaFormat,
                                               const CTimecode& frame0Timecode,
                                               MTI_UINT32 frameCount)
{
   int status;

   if (!OGLVAvailable)
      return 0;   // OGLV raw disk not available, so cannot allocate
      
   MTI_UINT32 fieldSize;

   // Get number of bytes in a single field from the image format
   switch(mediaFormat.getMediaFormatType())
      {
      case MEDIA_FORMAT_TYPE_VIDEO3 :
         {
         const CImageFormat& imageFormat3
                              = static_cast<const CImageFormat&>(mediaFormat);
         fieldSize = imageFormat3.getBytesPerField();
         }
         break;
      default :
         return 0; // Something really wrong
      }

   // Determine the total space required, in bytes, by adjusting the field
   // size by the disk blocking factors, then multiply by the number of
   // fields requested
   MTI_UINT32 paddedFieldSize = getAdjustedFieldSize(fieldSize);
   MTI_UINT32 fieldCount = frameCount * mediaFormat.getFieldCount();
   MTI_INT64 paddedSize = (MTI_INT64)paddedFieldSize * fieldCount;

   MTI_INT32 offset;  // Offset to allocated space in blocks, returned by bridge

   MTI_INT64 offs64;  // Offset to allocated space from start of raw partition,
                      // in bytes

   // Use OGLV Bridge code to allocate contiguous space of on OGLV's
   // raw disk equal to the number of bytes in paddedSize
   status = oglvBr->openOGLV();
   if (status != 0)
      { 
      TRACE_0(errout << "ERROR: Could not open OGLV Bridge before media allocate"
                     << endl
                     << "       Return value: " << status );
      return 0 ; // return NULL pointer
      }

   // the bridge allocator function thinks in blocks - not bytes
   offset = oglvBr->allocateClipBlocks((MTI_INT32)(paddedSize/RF_BLOCK_SIZE)); 
   if (offset == -1)
      {
      // insufficient space available
      return 0 ; // return NULL pointer
      }

   status = oglvBr->closeOGLV();
   if (status != 0) 
      {
      TRACE_0(errout << "ERROR: Could not close OGLV Bridge after media allocate"
                     << endl
                     << "       Return value: " << status );
      return 0 ; // return NULL pointer
      }

   // the media allocator object needs the offset in bytes
   offs64 = ((MTI_INT64)(offset))*((MTI_INT64)RF_BLOCK_SIZE);

   // Create a new media allocator object and return it to caller
   return(new CRawDiskMediaAllocator(this, offs64, paddedSize, paddedFieldSize,
                                     fieldCount));
}

int CRawOGLVMediaAccess::cancelAllocation(CMediaAllocator *mediaAllocator)
{
   // This function does not have to do anything.  The correct way
   // to cancel storage allocation made throught the OGLV Bridge
   // is to not call the member function appendClipToOGLV

   return 0;
}

int CRawOGLVMediaAccess::appendClipToOGLV(const string& dstBin, ClipSharedPtr &inClip)
{
   if (!OGLVAvailable)
      return CLIP_ERROR_RAW_OGLV_DISK_NOT_AVAILABLE;

   oglvBr->openOGLV();

   // make a copy of the Bin Path name (but remember to free it)
   char *binPathname = MTIstrdup(dstBin.c_str());

   // get the index of the target bin - if it's not in
   // OGLV, append it and get the index
   int binidx;
   binidx = oglvBr->getBinIndex(binPathname);
   if (binidx < 0) { // bin not found
      binidx = oglvBr->appendBin(binPathname);
   }

   // the index may be that of a resrved OGLV bin. If it
   // is, the next call will fail

   int status = oglvBr->appendClip(binidx,inClip);
   if (status != 0)
      {
      MTIfree(binPathname);
      return status;
      }
/*
   TTT test the conform function using the clip
   just created and re-doing the Flist for it

   status = oglvBr->conformExistingFlist(inClip);
   if (status != 0)
      return status;
*/
   oglvBr->closeOGLV();

   MTIfree(binPathname);
   return 0;
}

int CRawOGLVMediaAccess::conformExistingFlist(ClipSharedPtr &inclp)
{
   if (!OGLVAvailable)
      return CLIP_ERROR_RAW_OGLV_DISK_NOT_AVAILABLE;

   oglvBr->openOGLV();

   int retVal = oglvBr->conformExistingFlist(inclp);

   oglvBr->closeOGLV();

   return retVal;
}

CMediaDeallocator* CRawOGLVMediaAccess::createDeallocator()
{
   CRawMediaDeallocator *deallocator;

   deallocator = new CRawMediaDeallocator;

   return deallocator;
}

int CRawOGLVMediaAccess::deallocate(CMediaDeallocator& deallocator)
{
   if (!OGLVAvailable)
      return CLIP_ERROR_RAW_OGLV_DISK_NOT_AVAILABLE;

   oglvBr->openOGLV();

   // Call OGLV Bridge code to deallocate media storage on OGLV's raw disk.
   // Possibly this will do nothing and rely on OGLV Bridge to deallocate
   // when it is explicitly asked to delete the OGLV clip (you got it).

   CMediaDeallocList& mediaDeallocList = (static_cast<CRawMediaDeallocator&>(deallocator)).getMediaDeallocList();

   CMediaDeallocList::DeallocList& deallocList
                                       = mediaDeallocList.getDeallocList();

   // Create a temporary free list from the caller's list of blocks to
   // be deallocated
   CMediaDeallocList::DeallocListIterator deallocListIterator;
   for (deallocListIterator = deallocList.begin();
        deallocListIterator != deallocList.end();
        ++deallocListIterator)
      {
      MTI_INT64 location = deallocListIterator->dataIndex / RF_BLOCK_SIZE;
      MTI_INT64 size = deallocListIterator->dataSize / RF_BLOCK_SIZE;
      int status;
      status = oglvBr->deallocateClipBlocks((MTI_INT32)location,(MTI_INT32)size);
      if (status != 0)
         return(status);
      }

   oglvBr->closeOGLV();

   return 0;
}


//////////////////////////////////////////////////////////////////////
// Testing functions
//////////////////////////////////////////////////////////////////////

void CRawOGLVMediaAccess::dump(ostream &str)
{
   str << "RawOGLVMediaAccess " << std::endl;
   str << " Media Type: " << mediaType;
   str << " Media Identifier: <" << mediaIdentifier << ">" << std::endl;
   str << " OGLV Available: " << OGLVAvailable << std::endl;
   str << " Raw Video Directory: <" << rawVideoDirectory << ">" << std::endl;
   str << " Fast IO Boundary: " << fastIOBoundary << std::endl;
}








