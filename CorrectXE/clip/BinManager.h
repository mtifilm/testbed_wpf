// BinManager.h: interface for the CBinManager class.
//
//////////////////////////////////////////////////////////////////////

#ifndef BinManagerH
#define BinManagerH

#include "cliplibint.h"

#include <functional>
#include <string>
#include <vector>
using std::string;
using std::vector;

#if !defined(_WIN32)
#include <sys/dir.h>
#endif

#include "ClipSharedPtr.h"
#include "Clip3Track.h" // for ETrackType
#include "ClipIdentifier.h"

#include "FileSweeper.h"
#include "ImageInfo.h"
//#ifdef NOTYET
#include "IniFile.h"
//#endif
#include "MediaInterface.h"

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CClip;
class CClipInitInfo;
class CImageFormat;
class CTimecode;
class CVideoField;
class CVideoFrameList;
struct JobInfo;

//////////////////////////////////////////////////////////////////////


#define DECLARE_BIN_CLIP_SETTER(key) \
void set ## key(const string &binOrClipPath, const string &val)

#define DECLARE_BIN_CLIP_GETTER(key) \
string get ## key(const string& binOrClipPath)

// These values mirror the values for the clip LUT option in NavMainCommon
// for some semblance of consistency. Don't use INTERNAL!
enum BinDisplayLutOption
{
  BIN_DISPLAY_LUT_INTERNAL = 0,
  BIN_DISPLAY_LUT_DEFAULT = 1,
  BIN_DISPLAY_LUT_CUSTOM  = 2
};

// Define to activate clip reference counting for clipOpen and clipClose
// This define exists to ease the transition to reference counting.
// Comment-out this define if you do not want to build CPMP with
// reference counting

#define versionIsActive "A"
#define versionWasCommitted "C"
#define versionWasDiscarded "D"
#define dontKnowWhatHappenedToTheVersion "U"
#define invalidVersionStatus "X"

//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CBinManager
{
public:
   CBinManager();
   virtual ~CBinManager();

   // Bin Functions
   int createBin(const string& path, const string& binName);
   bool doesBinExist(const string& binPathName);
	bool doesBinExist(const string& path, const string& binName);
	CIniFile *openBinIniFile(const string &binPath);

   // Clip Functions
   int closeAllClips();
   int closeClip(ClipSharedPtr &clip);
   int closeClipForced(ClipSharedPtr &clip);
   ClipSharedPtr createClip(CClipInitInfo& clipInitInfo, int* status);
   ClipSharedPtr createClipFromIniFile(const string& newBinPath, const string& newClipName, int* status);
   int createClipIniFile(CClipInitInfo &clipInitInfo);
   ClipVersionNumber determineNextVersionNumber(const ClipIdentifier &parentClipId);
   int createVersionClip(ClipIdentifier parentClipId,
            /* OUTPUT */ ClipVersionNumber &versionNumberOut);
   int cloneClip(const ClipIdentifier &sourceClipId, const ClipIdentifier &cloneClipId);
   int deleteClip(const string& binPath, const string& clipName);
   int deleteClipIniFile(const string& binPath, const string& clipName);
   int deleteClipOrClipIni(const string& binPath, const string& clipName);
   int destroyClipDirtyMedia(const string &binPath, const string& clipName);
   int archiveClip(const string& binPath, const string& clipName);

   int freeMedia(const string &binPath, const string &clipName,
                 ETrackType trackType);

   int renameClip(const string& binPath, const string& clipName,
                  const string& newBinPath, const string& newClipName,
                  bool renameMediaFlag=true);

   void handleRenamedBin(const string& oldBinPath, const string& newBinPath);

   bool doesClipExist(const string& clipFileName);
   bool doesClipExist(const string& binPath, const string& clipName);
   bool doesClipIniExist(const string& binPath, const string& clipName);
   bool isClipDir(const string& path);
   bool isClipOpen(const string& clipFileName);
   bool isClipOpen(const string& binPath, const string& clipName);
   ClipSharedPtr openClip(ClipIdentifier clipid, int *status);
   ClipSharedPtr openClip(const string& clipFileName, int *status);
   ClipSharedPtr openClip(const string& binPath, const string& clipName, int *status);
   void openClip(ClipSharedPtr &clip);
   string makeClipDesktopIniFileName(const string& clipPathName);
   string getDesktopSectionUniqueName();
   CIniFile *openClipDesktop(ClipSharedPtr &clip);
   CIniFile *openBinDesktop(ClipSharedPtr &clip);
   CIniFile *openRootDesktop();
   bool doesDesktopIniExist(const string &dirPath);

   CClipInitInfo* openClipOrClipIni(const string& binPath,
                                    const string& clipName, int *status);
//   int refreshClipStatus(const string& binPath, const string& clipName);
//   int refreshClipStatus(const string& clipFileName);

   // Path and file name helper functions
   string makeClipPath(const string& binPath, const string& clipName) const;
   string makeClipPath(const string& clipFileName) const;
   string makeClipFileName(const string& binPath, const string& clipName);
   string makeClipFileName(const string& clipFileName);
   string makeClipFileNameWithExt(const string& binPath,
                                  const string& clipName);
   string makeClipFileNameWithExt(const string& clipFileName);
   string makeClipIniFileNameWithExt(const string& binPath,
                                     const string& clipName);
   string makeClipIniFileNameWithExt(const string& clipFileNam);
   string makeNextAutoClipName(const string& binPath);
   string makeNextAutoClipName(const string& binPath,
                               const string& clipNameTemplate);
   string makeNextAutoClipName(const string& binPath, int *ipIndexOffset);
   string makeNextAutoClipName(const string& binPath,
                               const string& clipNameTemplate,
                               int *ipIndexOffset);
   string getRepairedCadenceFileName() const;
   string makeRepairedCadenceFilePath(const string &binPath,
                                      const string &clipName) const;
   void getAutoClipName(const string& binPath, string &fileTemplate,
                        int &fileIndex);
   // mlm... could use macro
   string getAutoClipScheme(const string& binPath);
   // mlm... could use macro
   string getAutoDiskScheme(const string& binPath);
   string getDailiesAudioDuration (const string& binPath);
   string getDailiesImageDuration (const string& binPath);
   // mlm... could use macro
   void setAutoClipScheme(const string& binPath, const string &newClipScheme);
   // mlm... could use macro
   void setAutoDiskScheme(const string& binPath, const string &newDiskScheme);
   void setAutoClipName(const string& binPath, const string &newClipName);
   void setAutoClipName(const string& binPath, const string &strTemplate,
                        int iIndex);
   void setDailiesAudioDuration (const string& binPath, const string &newDuration);
   void setDailiesImageDuration (const string& binPath, const string &newDuration);

   DECLARE_BIN_CLIP_SETTER(company);
   DECLARE_BIN_CLIP_SETTER(address);
   DECLARE_BIN_CLIP_SETTER(cityStatePostal);
   DECLARE_BIN_CLIP_SETTER(country);
   DECLARE_BIN_CLIP_SETTER(jobNo);
   DECLARE_BIN_CLIP_SETTER(prodCo);
   DECLARE_BIN_CLIP_SETTER(project);
   DECLARE_BIN_CLIP_SETTER(title);
   DECLARE_BIN_CLIP_SETTER(prodNo);
   DECLARE_BIN_CLIP_SETTER(shootDate);
   DECLARE_BIN_CLIP_SETTER(transferDate);
   DECLARE_BIN_CLIP_SETTER(fieldA);
   DECLARE_BIN_CLIP_SETTER(fieldB);
   DECLARE_BIN_CLIP_SETTER(fieldC);
   DECLARE_BIN_CLIP_SETTER(workOrder);
   DECLARE_BIN_CLIP_SETTER(colorist);
   DECLARE_BIN_CLIP_SETTER(assist);

   DECLARE_BIN_CLIP_GETTER(company);
   DECLARE_BIN_CLIP_GETTER(address);
   DECLARE_BIN_CLIP_GETTER(cityStatePostal);
   DECLARE_BIN_CLIP_GETTER(country);
   DECLARE_BIN_CLIP_GETTER(jobNo);
   DECLARE_BIN_CLIP_GETTER(prodCo);
   DECLARE_BIN_CLIP_GETTER(project);
   DECLARE_BIN_CLIP_GETTER(title);
   DECLARE_BIN_CLIP_GETTER(prodNo);
   DECLARE_BIN_CLIP_GETTER(shootDate);
   DECLARE_BIN_CLIP_GETTER(transferDate);
   DECLARE_BIN_CLIP_GETTER(fieldA);
   DECLARE_BIN_CLIP_GETTER(fieldB);
   DECLARE_BIN_CLIP_GETTER(fieldC);
   DECLARE_BIN_CLIP_GETTER(workOrder);
   DECLARE_BIN_CLIP_GETTER(colorist);
   DECLARE_BIN_CLIP_GETTER(assist);

   int splitClipFileName(const string& clipFileName, string& binPath,
                         string& clipName) const;
   int getProjectBinName(const string& clipFileName, string& longProjectBin,
		string& shortProjectBin);
   int GetRecursiveBinList(const string& fullClipName,
                           StringList& longProjectBins,
                           StringList& shortProjectBins);
   int getProjectBinList(StringList& longProjectBin,
		StringList& shortProjectBin);

   int moveFilesToTrash(StringList fileList, string &trashPrefix);

   static string createDateAndTimeString();   // Make a date & time string
                                              // YYYY:MM:DD:HH:MM:SS
   string createTrashPrefix() const;

   // Open Clip List functions
   ClipSharedPtr getClipListEntry(unsigned int entryIndex);
   unsigned int getOpenClipCount();
   string getMainVideoStatusQuick(const string& clipIniFileName);
   string getMainVideoStatusQuick(const string& binPath,
                                  const string& clipName);

   int removeDirectory(const string& newPath);
   int removeDirectoryAndOneLevelOfContents(const string& path);
   int removeFile(const string &fileName);

   void DumpCurrentRefCount(void);

   // Hacks
   int getModifiedFrameCount(const string &clipPath, bool forceRecompute = false);
   void bumpModifiedFrameCounter(const string &clipPath, int bumpCount=1);
   void recomputeModifiedFrameCount(const string &clipPath);
   void clearModifiedFrameCounter(const string &clipPath);

   // Version hacks
   string makeVersionClipName(int versionNumber);
   string makeVersionClipName(ClipVersionNumber versionNumber);
   string makeVersionClipName(const string &versionClipPath);
   bool isVersionClipName(const string &name);
   bool doesClipHaveVersions(const string &binPath, const string &clipName);
   bool doesClipHaveVersions(const ClipIdentifier &clipId);
//   int getListOfNamesOfClipsVersions(const string &clipPath,
//                                     StringList &versionNumberList,
//                                     bool recurse,
//                                     bool depthFirst = false);
   void getListOfClipsActiveVersionNumbers(
			const ClipIdentifier &clipId,
         vector<ClipVersionNumber> &versionNumberList,
         bool recurse,
         bool depthFirst = false);

	ClipVersionNumber getFirstActiveVersionNumber(const ClipIdentifier &parentClipId);
	ClipVersionNumber getNextActiveVersionNumber(const ClipIdentifier &parentClipId,
																ClipVersionNumber versionNumber);
   bool isVersionReallyActive(const ClipIdentifier &clipId);
   string getRealBinPath(const string &clipPath);
   string getMasterClipPath(const string &clippath);
//   ClipVersionNumber getConsolidatedVersionNumber(const string &clippath);

   string makeVersionClipPath(const string &masterClipPath, ClipVersionNumber versionNumber);
   string makeVersionClipFileNameWithExt(const string& binPath, const string& clipName, ClipVersionNumber versionNumber);
   string makeVersionClipFileNameWithExt(const string& clipFileName, ClipVersionNumber versionNumber);

   string getDirtyMediaFolderPath(const string &clipPath);
   void setDirtyMediaFolderPath(const string &clipPath,
                                const string &folderPath);

   string getDirtyHistoryFolderPath(const string &clipPath);
   void setDirtyHistoryFolderPath(const string &clipPath,
                                const string &folderPath);

   void setVersionStatus(const string &clipPath, const ClipVersionNumber &versionNumber, const string &status);
   void setVersionStatus(const string &clipPath, const string &versionName, const string &status);
   string getVersionStatus(const string &clipPath, ClipVersionNumber versionNumber);

   int FindDirtyFrames(const CVideoFrameList *aFrameList,
                       int firstFrameIndex, int lastFrameIndex,
             /* out */ vector<int> &aListOfDirtyFrameIndexes,
                       int *progressPercent=NULL,
                       int *modifiedFrameCount=NULL);
	int CheckDirtyMedia(const vector<int>&listOfDirtyFrameIndexes,
							  const CVideoFrameList *versionFrameList,
							  string *firstCorruptFieldName,
							  string *lastCorruptFrameName,
							  int *progressPercent,
							  int *countdown);
	int FixOutOfSyncDirtyMedia(CVideoFrameList *versionFrameList,
										int *progressPercent,
										int *countdown);
	int DiscardDirtyMedia(const vector<int> &listOfDirtyFrameIndexes,
								 const CVideoFrameList *versionFrameList,
                         CVideoFrameList *parentFrameList=NULL,
                         bool destroyMediaFlag=true,
                         int *progressPercent=NULL,
                         int *countdown=NULL);
   int CommitMedia(const vector<int> &listOfDirtyFrameIndexes,
                   const CVideoFrameList *versionFrameList,
                   CVideoFrameList *parentFrameList,
                   int *progressPercent,
                   int *countdown);

   int RelinkMediaFoldersForClipAndItsVersions(
            ClipIdentifier clipId,
            const string &newMasterMediaFolder,
            const string &newVersionMediaParentFolderPath);

   static CBHook FrameWasDiscardedOrCommittedEvent;
   int IndexOfFrameThatWasDiscardedOrCommitted;
   bool FrameWasDiscarded;
   std::pair<int, int> DiscardedFrameRange;

   static CBHook ClipWasDeletedEvent;
   string lastDeletedClipName;


protected:

	 int CheckAndMaybeFixOneDirtyFrame(int frameIndex,
												  const CVideoFrameList *versionFrameList,
												  bool fixIt);
	 int DiscardDirtyMediaForOneFrame(int frameIndex,
												 const CVideoFrameList *versionFrameList,
                                     CVideoFrameList *parentFrameList,
                                     bool destroyMediaFlag);
    int CommitMediaForOneFrame(int frameIndex,
                               const CVideoFrameList *versionFrameList,
                               CVideoFrameList *parentFrameList);
    int ResetFrameDirtyMetadata(int frameIndex,
                                bool discardedFlag,
                                CVideoField *versionField,
                                CVideoField *parentField);

    void FinishedDiscardingCommittingFrames(const std::pair<int, int> &frameRange, bool discardedFlag);

    int RelinkAllMediaStorageForOneClip(
                           ClipIdentifier clipId,
                           const string &oldMasterClipMediaFolder,
                           const string &newMasterClipMediaFolder);

private:
   // CBinMgrClipList - Private class used by BinManager to manage
   // the list of open clips
   class CBinMgrClipList
   {
   public:
      CBinMgrClipList();
      ~CBinMgrClipList();

      void deleteList();

      void addClip(ClipSharedPtr &clip);
      ClipSharedPtr findClip(const string& targetBinPath, const string& targetClipName);
      int findClipEntryIndex(ClipSharedPtr &clip);
      ClipSharedPtr getClip(unsigned int index);
      unsigned int getClipCount();
      int removeClip(ClipSharedPtr &clip);
      void incrRefCount(ClipSharedPtr &clip);
      int decrRefCount(ClipSharedPtr &clip);

      void renameBin(const string& oldBinPath, const string& newBinPath);

      void dumpListToTrace(const string &tag);

   public:
      class CBinMgrClipListEntry
      {
      public:
         CBinMgrClipListEntry(ClipSharedPtr clip);
         CBinMgrClipListEntry(CClip *clip);
         ~CBinMgrClipListEntry();

         ClipSharedPtr clip;
         MTI_INT64 referenceCount;  // difficult to get this count to roll over
         FILETIME timeStamp;   // last modified time of the clip file to detect staleness
      };

      vector<CBinMgrClipListEntry> clipList;
   };

private:

   static CBinMgrClipList openClipList;   // List of open clips

   // Strings for Bin Ini file
   static string binIniFileName;
   static string binInfoSectionName;
   static string autoClipNameTemplateKey;
   static string autoClipNameIndexKey;
   static string autoClipSchemeKey;
   static string autoDiskSchemeKey;
   static string dailiesAudioDurationKey;
   static string dailiesImageDurationKey;
   static string desktopIniFileName;

   static string dailiesInfoSectionName;
   static string companyKey;
   static string addressKey;
   static string cityStatePostalKey;
   static string countryKey;
   static string jobNoKey;
   static string prodCoKey;
   static string projectKey;
   static string titleKey;
   static string prodNoKey;
   static string shootDateKey;
   static string transferDateKey;
   static string fieldAKey;
   static string fieldBKey;
   static string fieldCKey;
   static string workOrderKey;
   static string coloristKey;
   static string assistKey;

   static string DisplayLutSectionName;

   static string versionsIniFileName;
   static string versionInfoSectionName;
   static string versionCountKey;
   static string versionStatusSectionName;

   static string versionIniFileName;
   static string clipInfoSectionName;
   static string commentsKey;
   static string modifiedFrameCountKey;
   static string dirtyMediaFolderKey;
   static string dirtyHistoryFolderKey;
   static string statusSectionName;
   static string defunctKey;

   // Strings for Clip Ini file
   static string customLUTKey; // DUPE of one in CNavCurrentClip!

private:
   void init();
   bool checkFileExists(const string& fileName);
   int createDirectory(const string& path, const string& dirName);
   int createDirectory(const string& newPath);  // full path including dirname
   int removeDirectory(const string& path, const string& dirName);
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//#ifdef NOTYET

// Clip Scheme Search Filters
//   Can be ORed to search on multiple characteristics
#define CLIP_SCHEME_FILTER_ACTIVE   0x00000001  // "Active" Schemes only
#define CLIP_SCHEME_FILTER_SD       0x00000002  // Standard-Def Video Schemes
#define CLIP_SCHEME_FILTER_HD       0x00000004  // High-Def Video Schemes
#define CLIP_SCHEME_FILTER_HSDL     0x00000008  // High-Speed Digital Link
#define CLIP_SCHEME_FILTER_VIDEO    (CLIP_SCHEME_FILTER_SD \
                                     | CLIP_SCHEME_FILTER_HD)
#define CLIP_SCHEME_FILTER_IMAGE_FILE    0x00000010  // Image File Schemes
#define CLIP_SCHEME_FILTER_AUDIO_ONLY    0x00000020  // Audio Track Schemes
#define CLIP_SCHEME_FILTER_TYPE_ALL (CLIP_SCHEME_FILTER_SD \
                                     | CLIP_SCHEME_FILTER_HD \
                                     | CLIP_SCHEME_FILTER_IMAGE_FILE \
                                     | CLIP_SCHEME_FILTER_AUDIO_ONLY \
                                     | CLIP_SCHEME_FILTER_HSDL)
#define CLIP_SCHEME_FILTER_USER     0x00000040  // User Ini file
#define CLIP_SCHEME_FILTER_LOCAL_MACHINE  0x00000080  // Local Machine Ini file
#define CLIP_SCHEME_FILTER_SITE     0x00000100  // Site Ini file

#define CLIP_SCHEME_FILTER_SCOPE_ALL  (CLIP_SCHEME_FILTER_USER \
                                       | CLIP_SCHEME_FILTER_LOCAL_MACHINE \
                                       | CLIP_SCHEME_FILTER_SITE)

// If the Active Scheme filter is not specified, then the default is
// to search both active and inactive schemes.

// If a scheme type filter is not specified, then the default is all types.

// The search scope of User, Local Machine and Site determines where to look
// for Clip Schemes.  If the search is for Active Schemes only, then the search
// scope determines which Ini files to consult for the list of Active Schemes.
// If the search is for both Active and inactive Schemes, then the
// search scope determines which Ini files to consult to get the list of
// Clip Scheme directories.
// If a search scope filter is not specified, the default search scope is
// Site-only.

// -------------------------------------------------------------------

enum EClipSchemeType
{
   CLIP_SCHEME_TYPE_INVALID = 0,
   CLIP_SCHEME_TYPE_UNKNOWN,        // Clip Scheme type cannot be determined
   CLIP_SCHEME_TYPE_SD,
   CLIP_SCHEME_TYPE_HD,
   CLIP_SCHEME_TYPE_IMAGE_FILE,
   CLIP_SCHEME_TYPE_HSDL,
   CLIP_SCHEME_TYPE_AUDIO_ONLY      // Scheme defines clip with audio but
                                    // without any video
};

// ===================================================================
// -------------------------------------------------------------------

class CClipScheme;

// Predicate functors to compare Clip Scheme Name and File Name
// between CClipScheme instances.  Used with C++ algorithms sort, etc.
struct ClipSchemeNameCompare
 : std::binary_function<const CClipScheme*, const CClipScheme*, bool>
{
   bool operator()(const CClipScheme* lhs, const CClipScheme* rhs) const;
};

struct ClipSchemeNameEqual
 : std::binary_function<const CClipScheme*, const CClipScheme*, bool>
{
   bool operator()(const CClipScheme* lhs, const CClipScheme* rhs) const;
};

struct ClipSchemeFileNameCompare
 : std::binary_function<const CClipScheme*, const CClipScheme*, bool>
{
   bool operator()(const CClipScheme* lhs, const CClipScheme* rhs) const;
};

struct ClipSchemeFileNameEqual
 : std::binary_function<const CClipScheme*, const CClipScheme*, bool>
{
   bool operator()(const CClipScheme* lhs, const CClipScheme* rhs) const;
};

// ===================================================================
// -------------------------------------------------------------------

class MTI_CLIPLIB_API CClipScheme
{
public:
   CClipScheme();
   virtual ~CClipScheme();

   int init(const string& newFileName);

   string getClipSchemeFileName() const;
   string getClipSchemeName() const;
   EClipSchemeType getClipSchemeType() const;
   EMediaType getMainVideoMediaType() const;
   string getVideoFormatClass() const;
   string getAudioFormatClass() const;
   string getMediaStorageClass() const;
   bool doClassesMatch(const string &videoClass, const string &audioClass,
                       const string &storageClass);

// Friends
   friend struct ClipSchemeFileNameCompare;
   friend struct ClipSchemeFileNameEqual;
   friend struct ClipSchemeNameCompare;
   friend struct ClipSchemeNameEqual;

private:
   string clipSchemeName;
   string fileName;
   EClipSchemeType clipSchemeType;
   EMediaType mainVideoMediaType;
   string videoFormatClass;
   string audioFormatClass;
   string mediaStorageClass;

private:
   EClipSchemeType parseClipSchemeType(const string& typeString);

   static string clipSchemeInfoSection;
   static string clipSchemeNameKey;
   static string clipSchemeTypeKey;
   static string clipSchemeMainVideoSection;
   static string clipSchemeMediaTypeKey;
   static string clipSchemeVideoClassKey;
   static string clipSchemeAudioClassKey;
   static string clipSchemeStorageClassKey;
};

// ===================================================================
// -------------------------------------------------------------------

class MTI_CLIPLIB_API CClipSchemeManager
{
public:
   CClipSchemeManager();
   virtual ~CClipSchemeManager();

   int init();
   CClipScheme* findClipScheme(const string& clipSchemeName);

   typedef vector<CClipScheme*> ClipSchemeList;

   int virtual searchClipSchemes(unsigned int filter,
                                 ClipSchemeList &searchResults);

   CIniFile* getClipSchemeIniFile(const string& clipSchemeName);
   EClipSchemeType getClipSchemeType(const string &clipSchemeName);
   EMediaType getClipSchemeMainVideoMediaType(const string &clipSchemeName);

   StringList getVideoFormatClassList(bool videoStore);
   StringList getAudioFormatClassList(bool videoStore);
   StringList getMediaStorageClassList(bool videoStore);
   CClipScheme* findClipScheme(const string& videoFormatClass,
                               const string& audioFormatClass,
                               const string& storageFormatClass);
   void cleanUp();  // called at shutdown to delete the clip list
                    // & thus avoid upsetting Codeguard

private:
   int virtual findActiveClipSchemes(unsigned int filter,
                                     StringList &searchResults);
   void mergeUnsortedList(StringList &dstList, StringList &srcList);

   bool findSingleClipSchemeFile(string fileName);

   int readActiveClipSchemeFileNames(StringList &results);
   int addClipSchemes(StringList &fileNameList);

   // Define function FindClipSchemeFiles
   DEFINE_FILE_SWEEPER(findClipSchemeFiles, findSingleClipSchemeFile,
                       false, CClipSchemeManager);

   void deleteClipSchemeList();

private:
   static ClipSchemeList clipSchemeList;
   static bool clipSchemeListAvailable;
   static StringList newClipSchemeFileList;  // Holds results of FindClipSchemeFiles

   static string clipSchemeSectionName;
   static string clipSchemeDirectoryKey;
   static string clipSchemeConfigFileKey;

   static string activeClipSchemeSectionName;
   static string clipSchemeFileKey;
};
// ===================================================================
// -------------------------------------------------------------------

class MTI_CLIPLIB_API CClipAudioScheme
{
public:
   CClipAudioScheme() {};
   virtual ~CClipAudioScheme() {};

private:
};

// ===================================================================
// -------------------------------------------------------------------

class MTI_CLIPLIB_API CClipVideoScheme
{
public:
   CClipVideoScheme() {};
   virtual ~CClipVideoScheme() {};

private:
};

// ===================================================================
//#endif // #ifdef NOTYET

#if 1 // not yet: OLD_RENDER_DEST_HACK
//////////////////////////////////////////////////////////////////////

enum ERenderDestClipType
{
   DEST_CLIP_TYPE_SRC,         // Destination same as source
   DEST_CLIP_TYPE_NEW,         // Create new clip for destination
   DEST_CLIP_TYPE_NEW_VERSION, // Create new "version" clip for destination
   DEST_CLIP_TYPE_EXISTING     // Use existing clip for destination
};

enum ERenderDestInOutType
{
   DEST_IN_OUT_TYPE_ENTIRE_SRC,  // Entire source
   DEST_IN_OUT_TYPE_MARKS,       // Source Marks
   DEST_IN_OUT_TYPE_USER,        // User-set
   DEST_IN_OUT_AUTO_PDL,         // Magic
};

//-------------------------------------------------------------------

class MTI_CLIPLIB_API CRenderDestinationClip
{
public:
   CRenderDestinationClip();
   ~CRenderDestinationClip();

   string GetDestinationClipName();
   string GetDestinationMediaLocation();
   ERenderDestClipType GetDestinationType();
   string GetSourceClipName();
   string GetSourceFormatName();

   void SetDestinationClip(const string &clipName);
   void SetDestinationMediaLocation(const string &mediaLocation);
   void SetSourceClip(const string &clipName,
                      int videoProxyIndex, int videoFramingIndex);
   void SetDestinationType(ERenderDestClipType newDestType);

   int CreateClip();

   int Check();

public:
   int srcVideoProxyIndex;
   int srcVideoFramingIndex;
   
   ERenderDestClipType destType; // Destination type: source, new or existing

   ERenderDestInOutType dstInOutType;
   CTimecode dstClipInTC;     // In and out timecodes for new clip
   CTimecode dstClipOutTC;    // Ignored if using source or existing clip

private:
   int SetClipInitInfo(CClipInitInfo &clipInitInfo);

private:
   string srcClipName;     // Source clip's name with bin path.

   string dstClipName;     // Clip name with bin path for new and existing clip
                           // Ignored when using source

   string dstMediaLocation;  // Disk scheme name or image file name to use
                             // when creating a new clip

};
#endif // OLD_RENDER_DEST_HACK

//////////////////////////////////////////////////////////////////////
// Global Functions

string MTI_CLIPLIB_API TrimWhiteSpace(const string& inString);
void MTI_CLIPLIB_API DumpCurrentRefCount(void);

//////////////////////////////////////////////////////////////////////


#endif // #ifndef BIN_MANAGER_H


