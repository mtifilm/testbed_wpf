// RawMediaAccess.h: interface for the CRawMediaAccess and CRawOGLVMediaAccess
//                   classes.
//
//////////////////////////////////////////////////////////////////////

#ifndef RAWMEDIAACCESS_H
#define RAWMEDIAACCESS_H

#include "ClipSharedPtr.h"

#include <string>
#include <iostream>

using std::string;
using std::ostream;

#include "machine.h"
#include "MediaAccess.h"
#include "RawDiskFreeList.h"

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CBridge;
class CClip;
class CConfigFileIO;
class CIniFile;
class CMediaAllocator;
class CRawFileIO;

//////////////////////////////////////////////////////////////////////

#define RF_BLOCK_SIZE 512       // Raw disk block size, in bytes

//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CRawMediaAccess : public CMediaAccess   
{
public:
   CRawMediaAccess();
   virtual ~CRawMediaAccess();

   int initialize(CIniFile *ini, const string &sectionName);

   bool isClipSpecific() const;

   int checkSpace(const CMediaFormat& mediaFormat, MTI_UINT32 frameCount);

   CMediaAllocator* allocate(const CMediaFormat& mediaFormat,
                             const CTimecode& frame0Timecode, 
                             MTI_UINT32 frameCount);
   CMediaAllocator* allocateOneFrame(const CMediaFormat& mediaFormat,
                             const CTimecode& frame0Timecode, 
                             MTI_UINT32 fieldCount);
   int cancelAllocation(CMediaAllocator *mediaAllocator);

   CMediaAllocator* extend(const CMediaFormat& mediaFormat,
                           const CTimecode& frame0Timecode,
                           MTI_UINT32 frameCount);
   int cancelExtend(CMediaAllocator *mediaAllocator);

   CMediaDeallocator* createDeallocator();
   int deallocate(CMediaDeallocator& deallocator);

   MTI_UINT32 getAdjustedFieldSize(MTI_UINT32 fieldSize);
   MTI_INT64 adjustToBlocking(MTI_INT64 size, bool roundingFlag);

   int Peek(CMediaLocation &mediaLocation, string &filename, MTI_INT64 &offset);

   int CheckRFL();
   
   int queryFreeSpace(MTI_INT64 *totalFreeBytes,
                      MTI_INT64 *maxContiguousFreeBytes);
   virtual int queryFreeSpaceEx(const CMediaFormat& mediaFormat,
                                MTI_INT64 *totalFreeBytes,
                                MTI_INT64 *maxContiguousFreeBytes);
   virtual int queryTotalSpace(MTI_INT64 *totalBytes);

   int preload();
   int CreateNewFileHandle();
   int ReleaseFileHandle(int fileHandleIndex);

   int read(MTI_UINT8* buffer, CMediaLocation &mediaLocation);
   int read(MTI_UINT8* buffer, MTI_INT64 offset, unsigned int size);
   int readMT(MTI_UINT8* buffer, CMediaLocation &mediaLocation,
              int fileHandleIndex);
   int readMT(MTI_UINT8* buffer, MTI_INT64 offset, unsigned int size,
              int fileHandleIndex);
   int readMultipleMT(int count, MTI_UINT8** bufferArray,
                      CMediaLocation mediaLocationArray[],
                      int fileHandleIndex);
   int write(const MTI_UINT8* buffer, MTI_INT64 offset, unsigned int size);
   int write(const MTI_UINT8* buffer, CMediaLocation &mediaLocation);
   int writeMultiple(int count, MTI_UINT8* bufferArray[],
                     CMediaLocation mediaLocationArray[]);

   // NOTE: do NOT implement move()! We want it to fail so media gets copied!
   // (i.e. we don't want to fragment the parent of a version clip on commit)
   int move(CMediaLocation &fromLocation, CMediaLocation &toLocation)
         { return CMediaAccess::move(fromLocation, toLocation); };

   int destroy(const CMediaLocation &mediaLocation);

   void dump(ostream& str);

private:
   int openRawIO();
   int readRawDiskSection(CIniFile *ini, const string &sectionName);
   bool checkRawDiskConfigKey(CIniFile *ini, const string &sectionName,
                              const string &key);
   int CheckFileHandleIndex(int fileHandleIndex);
   int DeleteFileIOList();

   int gangBlocks(EMediaGangIO gangType,
                  int count, MTI_UINT8* bufferArray[],
                  CMediaLocation mediaLocationArray[],
                  MTI_INT64 *offsetArray, int *sizeArray,
                  MTI_UINT8** sortedBuffers, int &gangedCount);

private:
   string rawVideoDirectory;
   MTI_INT64 rawDiskSize;
   MTI_INT64 fastIOBoundary;
   string freeListFileName;
   ERawDiskAllocMethod allocMethod;
   MTI_INT32 nonContiguousMinFrames;   // Minimum number of frames in a
                                       // non-contiguous allocation block

//   CRawFileIO* fileIO;         // Points to file IO class if raw disk has been opened

   vector<CRawFileIO*> fileIOList;

   EMediaGangIO m_gangRead;
   EMediaGangIO m_gangWrite;
   int m_maxOverlappedReads;
   int m_maxOverlappedWrites;

   int m_timingReport;  // For debugging.  Comes from rawdisk.cfg
                        // If 1 or greater, causes read timing to be
                        // reported (TRACE_0) every m_timingReport reads.
                        // If 0 or less, then no report
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CRawOGLVMediaAccess : public CMediaAccess
{
public:
   CRawOGLVMediaAccess();
   virtual ~CRawOGLVMediaAccess();

   int initialize();

   bool isClipSpecific() const;

   int checkSpace(const CMediaFormat& mediaFormat, MTI_UINT32 frameCount);

   CMediaAllocator* allocate(const CMediaFormat& mediaFormat,
                             const CTimecode& frame0Timecode, 
                             MTI_UINT32 frameCount);
   int cancelAllocation(CMediaAllocator *mediaAllocator);

   int appendClipToOGLV(const string& dstBin, ClipSharedPtr &inClip);
   int conformExistingFlist(ClipSharedPtr &inclp);

   CMediaDeallocator* createDeallocator();
   int deallocate(CMediaDeallocator& deallocator);

   MTI_UINT32 getAdjustedFieldSize(MTI_UINT32 fieldSize);
   MTI_INT64 adjustToBlocking(MTI_INT64 size, bool roundingFlag);

   int Peek(CMediaLocation &mediaLocation, string &filename, MTI_INT64 &offset);

   int queryFreeSpace(MTI_INT64 *totalFreeBytes,
                      MTI_INT64 *maxContiguousFreeBytes);

   int read(MTI_UINT8* buffer, CMediaLocation &mediaLocation);
   int read(MTI_UINT8* buffer, MTI_INT64 offset, unsigned int size);
   int write(const MTI_UINT8* buffer, CMediaLocation &mediaLocation);
   int write(const MTI_UINT8* buffer, MTI_INT64 offset, unsigned int size);

   void dump(ostream& str);

private:

   CBridge *oglvBr;           // -> the OGLV bridge class

   bool OGLVAvailable;        // true if OGLV Bridge is available, false
                              // otherwise.
   string rawVideoDirectory;  // Name of raw disk partition
   MTI_INT64 fastIOBoundary;  // Disk I/O blocking factor.  Must be 1 or larger
   CRawFileIO* fileIO;// Points to CFileIO class if OGLV raw disk has been opened

   int openRawIO();
};

//////////////////////////////////////////////////////////////////////

#endif // #ifndef RAWMEDIAACCESS_H
