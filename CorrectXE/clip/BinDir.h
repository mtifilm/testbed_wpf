//---------------------------------------------------------------------------

#ifndef BinDirH
#define BinDirH
//---------------------------------------------------------------------------

#include "cliplibint.h"

#include <string>
using std::string;

#include <filesystem>
#if __cplusplus <= 201103
	// <= 10.3.2
#  define STDFILESYSTEM std::tr2::sys
#else
	// >= 10.3.3
#  define STDFILESYSTEM std::filesystem
#endif

#include "IniFile.h"  // StringList

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

//////////////////////////////////////////////////////////////////////
//--------------------------------------------------------------------
// Class CBinDir
//

#define BIN_SEARCH_FILTER_BIN       0x00000001
#define BIN_SEARCH_FILTER_CLIP      0x00000002
//#define BIN_SEARCH_FILTER_CLIP_INI  0x00000004 // Dead
#define BIN_SEARCH_FILTER_FILE      0x00000008  // Plain files (not directories)
#define BIN_SEARCH_FILTER_HIDDEN    0x00000010  // Allow hidden files
#define BIN_SEARCH_FILTER_ANY_DIR   0x00000020  // Any directory, including
																// bin and clip directories
#define BIN_SEARCH_FILTER_EITHER (BIN_SEARCH_FILTER_BIN | BIN_SEARCH_FILTER_CLIP | BIN_SEARCH_FILTER_CLIP_INI)

class MTI_CLIPLIB_API CBinDir
{
public:
   CBinDir();
   virtual ~CBinDir();

   bool findFirst(const string &path, int filter);
   bool findNext();

   int findAll(const string &path, int filter, StringList& resultList);
	int countAll(const string &path, int filter);
	static bool isBinEmpty(const string &binPath);
	static bool hasAtLeastOneNestedBin(const string &binPath);

   string fileName;              // found file name that meets filter specs
   string filePath;              // full path of found file

	static string getClipFileExtension();
   static string getClipIniFileExtension();

private:
	bool _searchOpen;              // Search in progress
	int _searchFilter;

	string _searchPath;            // Input path ending with slash
	string _searchFileName;        // current result of directory search
	string _fullSearchFileName;    // searchPath + searchFileName

   // Platform-Dependent data used in reading directories
	HANDLE _searchHandle;
	WIN32_FIND_DATA *_searchData;
   DWORD _searchError;

   void closeSearch();
   bool checkSearchFilter();
	bool checkBin();
   bool checkClip();
   bool checkClipIni();
   bool checkFile();
};

#endif
