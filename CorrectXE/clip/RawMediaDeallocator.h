// RawMediaDeallocator.h: interface for the CRawMediaDeallocator class.
//
/* CVS Info:
$Header: /usr/local/filmroot/clip/include/RawMediaDeallocator.h,v 1.3 2006/01/11 01:55:53 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#if !defined(RAWMEDIADEALLOCATOR_H)
#define RAWMEDIADEALLOCATOR_H


#include "MediaDeallocator.h"
#include "MediaDeallocList.h"

//////////////////////////////////////////////////////////////////////

class CRawMediaDeallocator : public CMediaDeallocator  
{
public:
   CRawMediaDeallocator();
   virtual ~CRawMediaDeallocator();

   int freeMediaAllocation(const CMediaLocation& mediaLocation);
   int freeMediaAllocation(const CMediaLocation& startMediaLocation,
                           int itemCount);
                           
   CMediaDeallocList& getMediaDeallocList();

private:
   CMediaDeallocList deallocList;
};

//////////////////////////////////////////////////////////////////////

#endif // !defined(RAWMEDIADEALLOCATOR_H)
