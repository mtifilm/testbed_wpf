object Form1: TForm1
  Left = 192
  Top = 114
  Width = 1147
  Height = 618
  Caption = 'Clip 5 Test'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 8
    Top = 64
    Width = 1105
    Height = 497
    Lines.Strings = (
      'Memo1')
    ScrollBars = ssBoth
    TabOrder = 0
  end
  object Button1: TButton
    Left = 16
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 192
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Parse'
    TabOrder = 2
    OnClick = Button2Click
  end
  object OpenDialog1: TOpenDialog
    Filter = 'XML|*.xml|All Files|*.*'
    InitialDir = 'c:\MTIBins'
    Left = 128
    Top = 16
  end
end
