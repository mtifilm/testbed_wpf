// RawAudioMediaDeallocator.cpp: implementation of the CRawAudioMediaDeallocator class.
//
//////////////////////////////////////////////////////////////////////

#include "RawAudioMediaDeallocator.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRawAudioMediaDeallocator::CRawAudioMediaDeallocator()
{

}

CRawAudioMediaDeallocator::~CRawAudioMediaDeallocator()
{

}

//////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////

int CRawAudioMediaDeallocator::
freeMediaAllocation(const CMediaLocation& mediaLocation)
{
   deallocList.addToDeallocList(mediaLocation);

   return 0;
}

int CRawAudioMediaDeallocator::
freeMediaAllocation(const CMediaLocation& startMediaLocation, int itemCount)
{
   deallocList.addToDeallocList(startMediaLocation, itemCount);

   return 0;
}


CMediaDeallocList& CRawAudioMediaDeallocator::getMediaDeallocList()
{
   return deallocList;
}
