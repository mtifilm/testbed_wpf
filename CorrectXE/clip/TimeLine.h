/*
	File:	TimeLine.h
	By:	Kevin Manbeck
	Date:	March 16, 2003

	The TimeLine class is used to hold many tracks of data that will be
	displayed in the time line GUI.

	The TimeLine class also has tools to navigate the various tracks.
	For example, the caller can request the frame of the next event or
	the previous event.

	One of the following functions is required:

	1.  setClipName (string strClipName)
		- opens the specified clip and reads in all relevant tracks
	2.  setPlayList (CLogPlayList *newPlayList)
		- TimeLine will gain access to the media through newPlayList


	The following functions are optional:
	1.  getAudioLTC (float fFrame)
		- returns string containing audio LTC
	2.  getTelecineKeykode (int iFrame)
		- returns string containing telecine Keykode
	3.  getVideoTC (int iFrame)
		- returns string containing video time code
	4.  getEventFlag (int iFrame)
		- returns a bit flag with all marks present on frame
	5.  getAudioProxy (int iChannel, int iFrame, MTI_INT16 sMin,
                 MTI_INT sMax)
		- returns the smallest and larges audio sample values
		- in iFrame.
	6.  getAudioProxy (int iChannel, float fFrameStart, float fFrameStop,
		MTI_INT16 sMin, MTI_INT sMax)
		- returns the smallest and larges audio sample values
		- at between fFrameStart and fFrameStop.
		- Note:  fFrameStart in INCLUSIVE, and fFrameStop is EXCLUSIVE
		- Note:  if (fFrameStop - fFrameStart) is too large, this
		- function will return approximate values.
	7.  getNextEvent (int iFrame, int iFlag)
		- returns the index of the frame larger than iFrame which
		- has been marked with the designated flag
		- when no such event exists, iFrame is returned
	8.  getPreviousEvent (int iFrame, int iFlag)
		- returns the index of the frame smaller than iFrame which
		- has been marked with the designated flag
		- when no such event exists, iFrame is returned
	9.  addEvent (int iFrame, int iFlag, bool bAudio, bool bTrackFile)
		- sets iFrame to have the designated flag
		-
		- NOTE:  call updateEventFile() to make this a permanent
		- part of the track file
	10. deleteEvent (int iFrame, int iFlag, bool bAudio, bool bTrackFile)
		- removes the designated flag from iFrame
		-
		- NOTE:  call updateEventFile() to make this a permanent
		- part of the track file
	11. setScale (int iScaleArg)
		- sets scale factor for audio waveform signal.
	12. ResetAudioReader (void)
		- By default CTimeLine does not re-read audio data in
		- order to improve efficiency.  Sometimes, however,
		- the caller knows the audio has changed.  Use this
		- function to force the CTimeLine to re-read audio data.
	13. ResetAll (void)
		- By default CTimeLine does not update certain volatile values
		- in order to improve efficiency.  Sometimes, however,
		- the caller knows these values have changed.  Use this
		- function to force the CTimeLine to re-read volatile data.
	14. Free (void)
		- frees any memory allocated by CTimeLine
	15. updateEventFile (void)
		- updates the event file to reflect any changed caused
		- by addEvent() and deleteEvent().
  	16. newMultiFrameEvent (int iFrame, int iNFrame, MTI_UINT64 ullFlag,
		const string &strAnnotation, const string &strAnnotationShort,
		bool bUnderlineAnnotation, MTI_UINT32 uiColor,
		MTI_UINT32 uiColorType, MTI_UINT32 uiAnnotationColor,
		void *vpPtr);
		- adds a new MultiFrameEvent to the list of such events
  	17. clearMultiFrameEvent (void)
		- clears the list of MultiFrameEvents
  	18. getNextMultiFrameEvent (int iFrame, MTI_UINT64 ullFlag)
		- returns the CMultiFrameEvent of the frame larger than
		- iFrame which has been marked with the designated flag
		- when no such event exists, CMultiFrameEvvent::iFrame
		- is set to iFrame
  	19. getPreviousMultiFrameEvent (int iFrame, MTI_UINT64 ullFlag)
		- returns the CMultiFrameEvent of the frame smaller than
		- iFrame which has been marked with the designated flag
		- when no such event exists, CMultiFrameEvvent::iFrame
		- is set to iFrame
  	20. setSFEOverride (MTI_UINT64 ullFlag, MTI_UINT32 uiColor,
		MTI_UINT32 uiColorType, MTI_INT32 iSymbol)
		- adds the indicated values to a list of SingleFrameEvent
		- override values.
  	21. clearSFEOverride (MTI_UINT64 ullFlag)
		- clears the override values for the indicated value of
		- ullFlag.  Set ullFlag to EVENT_NONE to clear all override
		- values.
  	22. setSFEAnnotation (int iFrame, MTI_UINT64 ullFlag, const string &str,
		const string &strShort)
		- adds the indicated string to a list of SingleFrameEvent
		- annotations.  The annotation is assigned to the
		- (iFrame, ullFlag) pair
  	23. clearSFEAnnotation (void)
		- clears all annotations
  	24. getNextSingleFrameEvent (int iFrame, MTI_UINT64 ullFlag)
		- returns the CSingleFrameEvent of the frame larger than
		- iFrame which has been marked with the designated flag
		- when no such event exists, CSingleFrameEvvent::iFrame
		- is set to iFrame
  	25.  getPreviousSingleFrameEvent (int iFrame, MTI_UINT64 ullFlag)
		- returns the CSingleFrameEvent of the frame smaller than
		- iFrame which has been marked with the designated flag
		- when no such event exists, CSingleFrameEvvent::iFrame
		- is set to iFrame
	26.  getAudio (int iFrameStart, int iFrameStop,
			MTI_UINT16 **uspData, int iMaxSample,
			int iNChannelToRead, int *ipChannelToRead)
		- reads individual sample data between FrameStart and
		- FrameStop into uspData.  Will not read more than iMaxSample
		- Only reads from the channels specified by iNChannelToRead
		- and ipChannelToRead.
		- fStartFrame is INCLUSIVE.  fStopFrame is EXCLUSIVE.
		- Data from ipChannel[0] is placed in uspData[0], 
		- ipChannel[1] is placed in uspData[1], etc.
		- Returns the number of samples read.
		- fills in silence for illegal frames
	27.  getAudioCount (int iFrameStart, int iFrameStop)
		- Returns the number of audio samples between FrameStart
		- and FrameStop

*/

#ifndef TIMELINEH
#define TIMELINEH

#include "MetaDataDLL.h"

#include "machine.h"
#include "mti_event.h"
#include "Clip3.h"


//  Forward declarations
  class CClip;
  class CVideoFrameList;
  class CAudioFrameList;
  class CTimeTrack;
  class CTrackStrategy;

#define AUDIO_PROXY_PER_FRAME 10	// number of audio proxy points in
					// one frame
#define MAX_READ_PER_CALL 2		// maximum number of reads per call

typedef MTI_UINT64 MTI_TIMELINE_EVENT;

// the value uiColorType
#define COLOR_TYPE_DEFAULT 0	// use the default color
#define COLOR_TYPE_SOLID 1	// use the value of uiColor

// the value of iSymbol
#define SYMBOL_DEFAULT 0	// use the default symbol

#define BINS_PER_FRAME 20

typedef struct
{
  MTI_INT16
    **sppMinValue,	// [iSubFrame][iChannel]
    **sppMaxValue;	// [iSubFrame][iChannel]

  MTI_INT16
    *spMinValue,	// [iChannel]
    *spMaxValue;	// [iChannel]

  bool
    bInitialized;
} TL_AUDIO_PROXY;

class MTI_CLIPLIB_API CAnnotatedEvent
{
public:
  CAnnotatedEvent()
   {
    iFrame = -1;
    ullFlag = EVENT_NONE;
    strAnnotation = "";
    strAnnotationShort = "";
    bUnderlineAnnotation = false;
    uiColorType = COLOR_TYPE_DEFAULT;
    uiColor = 0x0;
    uiAnnotationColor = 0x0;
    vpPtr = NULL;
   };
  ~CAnnotatedEvent() {};

  int
    iFrame;

  MTI_UINT64
    ullFlag;		// the type of event, see EVENT_* in mti_event.h

  string
    strAnnotation,
    strAnnotationShort;

  bool
    bUnderlineAnnotation;

  MTI_UINT32
    uiColorType,
    uiColor,		// 0x00BBGGRR
    uiAnnotationColor;	// 0x00BBGGRR

  void
    *vpPtr;
};

class MTI_CLIPLIB_API CSingleFrameEvent:public CAnnotatedEvent
{
public:
  CSingleFrameEvent()
   {
    iSymbol = SYMBOL_DEFAULT;
   };
  ~CSingleFrameEvent() {};

  MTI_INT32
    iSymbol;		// see SYMBOL_* for values
};

class CMultiFrameEvent:public CAnnotatedEvent
{
public:
  CMultiFrameEvent()
   {
    iNFrame = -1;
   };
  ~CMultiFrameEvent() {};

  int
    iNFrame;		// number of frames
};

// ---------------------------------------------------------------------------

class MTI_CLIPLIB_API CAnimateTimelineInterface
{
public:
   virtual  ~CAnimateTimelineInterface() {};
   virtual bool GetFirstKeyframe(int &keyframeIndex, bool &inclusion) = 0;
   virtual bool GetNextKeyframe(int &keyframeIndex, bool &inclusion) = 0;
   virtual bool GetPreviousKeyframe(int &keyframeIndex, bool &inclusion) = 0;
   virtual bool IsKeyframeHere(int frameIndex) = 0;

   virtual void DeleteKeyframe(int frameIndex) = 0;
   virtual void DeleteAllKeyframes() = 0;

};

// ---------------------------------------------------------------------------

class MTI_CLIPLIB_API CTimeLine
{
public:
  CTimeLine(const ClipSharedPtr &clipArg);
  CTimeLine();
  ~CTimeLine();

  int setClipName (const string &strClipNameArg,
		int iVideoSelect=VIDEO_SELECT_NORMAL,
		int iAudioSelect=0,
      int iFramingSelect=FRAMING_SELECT_VIDEO);
  int setClip(const ClipSharedPtr &clipArg,
		int iVideoSelect=VIDEO_SELECT_NORMAL,
		int iAudioSelect=0,
      int iFramingSelect=FRAMING_SELECT_VIDEO);
  int setTrackStrategy (CTrackStrategy *newTrackStrategy);
  void setScale (int iScaleArg);
  int  getScale(void);
  void Free ();

  string getAudioLTC (float fFrame, bool bRound=false, int iOffset=0);
  string getClipAudioLTC (int newClipFrame, bool bRound=false, int iOffset=0);
  string getTelecineKeykode (int iFrame,
		EKeykodeFormat iFormat=
			MTI_KEYKODE_FORMAT_FEET_PLUS_FRAMES_DOT_PERFS, 
		int iPerfOffset=0,
		EKeykodeFilmDimension *ipFilmDimension=0);
  string getClipTelecineKeykode (int newClipFrame);
  string getTelecineTC (int iFrame);
  string getClipTelecineTC (int newClipFrame);
  string getVideoTC (int iFrame);
  bool getVideoCTimecode (int iFrame, CTimecode &ctc);
  int getVideoFrameIndex(CTimecode ctc);

  MTI_UINT64 getEventFlag (int iFrame);

  void getAudioProxy (int iChannel, int iFrame, MTI_INT16 &sMax,
                MTI_INT16 &sMin);
  void getAudioProxy (int iChannel, float fFrameStart, float fFrameStop,
		MTI_INT16 &sMax, MTI_INT16 &sMin);

  int getNextEvent (int iFrame, MTI_UINT64 ullFlag);
  int getPreviousEvent (int iFrame, MTI_UINT64 ullFlag);
  int getFrameIndexForLTC (CTimecode &tcArg);
  int getNumberOfMatchingFrameIndicesForLTC(CTimecode &tcArg);
  int getNthMatchingFrameIndexForLTC(int iNSeq);
  void addEvent (int iFrame, MTI_UINT64 ullFlag, bool bAudio, bool bTrackFile);
  void deleteEvent (int iFrame, MTI_UINT64 ullFlag, bool bAudio, bool bTrackFile);
  int updateEventFile ();
  bool isAudioValid(int iChannel, int iFrame);

  int getTotalFrameCount();
  ClipSharedPtr getClip(void);

  int getHandleCount();

  void ResetAudioReader (void);
  int ResetEvent (void);
  void ResetTrackFile (void);
  void ClearEvent (MTI_UINT64 ullMask);


  // functions for CMultiFrameEvent
  void newMultiFrameEvent (int iFrame, int iNFrame, MTI_UINT64 ullFlag,
		const string &strAnnotation, const string &strAnnotationShort,
		bool bUnderlineAnnotation, MTI_UINT32 uiColor,
		MTI_UINT32 uiColorType, MTI_UINT32 uiAnnotationColor,
		void *vpPtr, bool bConsolidate=false);
  void clearMultiFrameEvent ();
  CMultiFrameEvent getNextMultiFrameEvent (int iFrame, MTI_UINT64 ullFlag);
  CMultiFrameEvent getPreviousMultiFrameEvent (int iFrame, MTI_UINT64 ullFlag);

  // functions for CSingleFrameEvent
  void setSFEOverride (MTI_UINT64 ullFlag, MTI_UINT32 uiColor,
		MTI_UINT32 uiColorType, MTI_INT32 iSymbol);
  void clearSFEOverride (MTI_UINT64 ullFlag);
  void setSFEAnnotation (int iFrame, MTI_UINT64 ullFlag, const string &str,
		const string &strShort);
  void clearSFEAnnotation ();
  CSingleFrameEvent getNextSingleFrameEvent (int iFrame, MTI_UINT64 ullFlag);
  CSingleFrameEvent getPreviousSingleFrameEvent (int iFrame,
		MTI_UINT64 ullFlag);

  int getAudio (int iFrameStart, int iFrameStop, MTI_INT16 **spData,
		int iMaxSample, int iNChannel, int *ipChannel);
  int getAudioCount (int iFrameStart, int iFrameStop);
  TL_AUDIO_PROXY * getAudioProxyPointer (int iFrame);    // Adde JAM

  float findClap (int iChannel, int iFrameStart, int iFrameStop);

   bool GetFirstKeyframe(int &keyframeIndex, bool &inclusion);
   bool GetNextKeyframe(int &keyframeIndex, bool &inclusion);
   bool GetPreviousKeyframe(int &keyframeIndex, bool &inclusion);

   bool IsKeyframeHere(int frameIndex);

   void DeleteKeyframe(int frameIndex);
   void DeleteAllKeyframes();

   void SetAnimateTimelineInterface(CAnimateTimelineInterface *newInterface);

   CBHook EventChanged;
   std::pair<MTI_UINT64, MTI_UINT64> EventData;

private:
  string strClipName;

  TL_AUDIO_PROXY *app;
  int iAllocAudio;

  MTI_UINT64
    *ullpEvent;
  int iAllocEvent;

  MTI_UINT8 *ucpRawFrame;
  int iNFieldAudio;
  int iNChannelAudio;
  int iSampleSizeAudio;
  int iScale;

  int iFramingSelected;
  int iVideoSelected;

  ClipSharedPtr clip;
  CVideoFrameList *vflp;
  CAudioFrameList *aflp;
  CTimeTrack *ttpInkNumber;
  CTimeTrack *ttpKeykode;
  CTimeTrack *ttpAudioLTC;
  CTimeTrack *ttpEvent;
  CTimeTrack *ttpAudioProxy;

  // How the clip was opened
  
  int iFrameRateAudioLTC;

  void Init ();
  int Alloc ();
  int AllocAudio ();
  void FreeAudio ();
  void ReadAudio (int iFrameStart, int iFrameStop);
  void ClearAudioProxy (void);
  int ReadOneFrame (int iFrame);

  // translate frame indexes between timeline and timetrack spaces
  int line2track(int iTimeLineFrame);
  int track2line(int iTimeTrackFrame);

  vector <CMultiFrameEvent> listMFE;
  vector <CSingleFrameEvent> listSFEOverride, listSFEAnnotation;
  // listSFEOverride keeps track of override values (like uiColor, iSymbol, 
  // etc.) for different values of ullFlag.
  // listSFEAnnotation keeps track of annotations for (iFrame, ullFlag)
  // pairs
  void AugmentSFE (CSingleFrameEvent &sfe, MTI_UINT64 ullFlag);

  vector <int> listLTCBreak;  // list of LTC breaks

  vector <int> listLTCFrameIndex; // list of frame indices matching LTC

  CTrackStrategy *pTrackStrategy;
  CTrackStrategy *pDefaultTrackStrategy;

  CAnimateTimelineInterface *animateIFPtr;

};

#endif
