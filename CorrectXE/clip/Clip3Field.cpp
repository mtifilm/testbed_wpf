// Field.cpp: implementation of the CField abstract base class.
//
//////////////////////////////////////////////////////////////////////

#include "Clip3Track.h"
#include "MediaAccess.h"
#include "MediaAllocator.h"
#include "MTIstringstream.h"
#include "err_clip.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CField::CField()                               // Default Constructor
: flags(0), fieldList(0), fieldListEntryIndex(-1)
{
   // Set default values for field flags
   setFieldInvisible(false);           // Field is Visible
   setSharedMedia(false);              // Media is Not Shared
   setSharedMediaFirst(false);         // Media is Not Shared
   setVirtualMedia(false);             // Media is Not Virtual
   setVisibleSiblingFieldIndex(0);     // Sibling Field Index not relevant
}

CField::CField(const CField& sourceField)      // Copy Constructor
: mediaLocation(sourceField.mediaLocation),
  flags(sourceField.flags),
  fieldList(sourceField.fieldList),
  fieldListEntryIndex(sourceField.fieldListEntryIndex)
{
}

CField::~CField()
{

}

//////////////////////////////////////////////////////////////////////
// Assignment Operator
//////////////////////////////////////////////////////////////////////

void CField::operator=(const CField& rhs)       // Assignment Operator
{
   if (this == &rhs)    // Check for self-assignment
      return;

   // Copy members
   mediaLocation = rhs.mediaLocation;
   flags = rhs.flags;
   fieldList = rhs.fieldList;
   fieldListEntryIndex = rhs.fieldListEntryIndex;
}

//////////////////////////////////////////////////////////////////////
// Comparison Operators
//////////////////////////////////////////////////////////////////////

bool CField::operator>(const CField& rhs) const
{
   return (compare(rhs) > 0);
}

int CField::compare(const CField& rhs) const
{
   // Comparison Order
   //   Media Location
   //   Non-Shared Media < Shared Media First < Shared Media Non-First
   //   Visible Field < Invisible Field

   if (this == &rhs)
      return 0;

   int mediaLocationCompare = mediaLocation.compare(rhs.mediaLocation);

   if (mediaLocationCompare == 0)
      {
      bool lhsSharedMediaFirst = isSharedMediaFirst();
      bool rhsSharedMediaFirst = rhs.isSharedMediaFirst();
      if (lhsSharedMediaFirst == rhsSharedMediaFirst)
         {
         bool lhsFieldInvisible = isFieldInvisible();
         bool rhsFieldInvisible = rhs.isFieldInvisible();
         if (lhsFieldInvisible == rhsFieldInvisible)
            {
            return 0;
            }
         else if (!lhsFieldInvisible && rhsFieldInvisible)
            {
            return -1;
            }
         else  // if (lhsFieldInvisible && !rhsFieldInvisible)
            {
            return 1;
            }
         }
      else if (lhsSharedMediaFirst && !rhsSharedMediaFirst)
         {
         return -1;
         }
      else // if (!lhsSharedMediaFirst && rhsSharedMediaFirst)
         {
         return 1;
         }
      }
   else
      {
      return mediaLocationCompare;
      }
}

//////////////////////////////////////////////////////////////////////
// Accessor Functions
//////////////////////////////////////////////////////////////////////

MTI_UINT32 CField::getDataSize()
{
   return(mediaLocation.getDataSize());
}

CFieldList* CField::getFieldList() const
{
   return fieldList;
}

int CField::getFieldListEntryIndex() const
{
   return fieldListEntryIndex;
}

const CMediaLocation& CField::getMediaLocation() const
{
   return mediaLocation;
}

void CField::setMediaLocation(const CMediaLocation& newMediaLocation)
{
   mediaLocation = newMediaLocation;
}

void CField::setFieldListEntryIndex(CFieldList *newFieldList, int newIndex)
{
   fieldList = newFieldList;
   fieldListEntryIndex = newIndex;
}

//////////////////////////////////////////////////////////////////////
// Field Flag Accessor Functions
//////////////////////////////////////////////////////////////////////

bool CField::doesMediaStorageExist() const
{
   // This flag is in the shared Field List

   if (fieldList == 0)
      return true;  // default is that media storage exists, even though
                    // we don't actually know.

   CFieldListEntry *fieldListEntry = fieldList->getField(fieldListEntryIndex);
   if (fieldListEntry == 0)
      return false;

   return fieldListEntry->doesMediaStorageExist();
}

int CField::getVisibleSiblingFieldIndex() const
{
   // Return 0 if visible sibling is first visible field in frame
   // Return 1 if visible sibling is second visible field in frame
   return (isFieldFlagSet(FIELD_FLAG_VISIBLE_SIBLING)) ? 1 : 0;
}

bool CField::isFieldInvisible() const
{
   // Return false if field is visible
   // Return true if field is invisible
   return (isFieldFlagSet(FIELD_FLAG_INVISIBLE));
}

bool CField::isMediaWritten() const
{
   // This flag is in the shared Field List

   if (fieldList == 0)
      return true;  // default is that media was written, even though
                    // we don't actually know.

   CFieldListEntry *fieldListEntry = fieldList->getField(fieldListEntryIndex);
   if (fieldListEntry == 0)
      return true;

   return fieldListEntry->wasMediaWritten();
}

bool CField::isSharedMedia() const
{
   // Return false if field does not share media with another field in frame
   // Return true if this field does share media with another field in frame
   return (isFieldFlagSet(FIELD_FLAG_SHARED_MEDIA));
}

bool CField::isSharedMediaFirst() const
{
   // Return false if field does not share media with another field in frame
   //              or if field is not first of fields that do share the same
   //              media
   // Return true if this field does share media with another field in frame
   //             and is the first field that share this media
   return (isFieldFlagSet(FIELD_FLAG_SHARED_MEDIA)
           && isFieldFlagSet(FIELD_FLAG_SHARED_MEDIA_FIRST));
}

bool CField::isBorrowedMedia() const
{
   // This flag is in the shared Field List

   if (fieldList == 0)
      return false;  // default is that media is not dirty,
                     // even though we don't actually know.

   CFieldListEntry *fieldListEntry = fieldList->getField(fieldListEntryIndex);
   if (fieldListEntry == 0)
      return false;

   return fieldListEntry->isMediaBorrowed();
}

bool CField::isDirtyMedia() const
{
   // This flag is in the shared Field List

   if (fieldList == 0)
      return false;  // default is that media is not dirty,
                     // even though we don't actually know.

   CFieldListEntry *fieldListEntry = fieldList->getField(fieldListEntryIndex);
   if (fieldListEntry == 0)
      return false;

   return fieldListEntry->isMediaDirty();
}

bool CField::isVirtualMedia() const
{
   // Return false if field owns the media
   // Return true if field's media is virtual and owned by another clip
   return (isFieldFlagSet(FIELD_FLAG_VIRTUAL_MEDIA));
}

bool CField::isWriteProtectedMedia() const
{
   // This flag is in the shared Field List

   if (fieldList == 0)
      return false;  // default is that media is not write protected,
                    // even though we don't actually know.

   CFieldListEntry *fieldListEntry = fieldList->getField(fieldListEntryIndex);
   if (fieldListEntry == 0)
      return false;

   return fieldListEntry->isMediaWriteProtected();
}

MTI_UINT16 CField::getFieldFlags() const
{
   // Return all field flags as bit-flags
   return flags;
}

bool CField::isFieldFlagSet(MTI_UINT16 bitFlag) const
{
   return (flags & bitFlag) ? true : false;
}

//////////////////////////////////////////////////////////////////////
// Field Flag Mutator Functions
//////////////////////////////////////////////////////////////////////

void CField::setFieldInvisible(bool newFlag)
{
   setFieldFlag(FIELD_FLAG_INVISIBLE, newFlag);
}

void CField::setMediaStorageExists(bool newFlag)
{
   // This flag is in the shared Field List

   if (fieldList == 0)
      return;  // field list not available

   CFieldListEntry *fieldListEntry = fieldList->getField(fieldListEntryIndex);
   if (fieldListEntry == 0)
      return;

   fieldListEntry->setMediaStorageExists(newFlag);
}

void CField::setMediaWritten(bool newFlag)
{
   // This flag is in the shared Field List

   if (fieldList == 0)
      return;  // field list not available

   CFieldListEntry *fieldListEntry = fieldList->getField(fieldListEntryIndex);
   if (fieldListEntry == 0)
      return;

   fieldListEntry->setMediaWritten(newFlag);
}

void CField::setSharedMedia(bool newFlag)
{
   setFieldFlag(FIELD_FLAG_SHARED_MEDIA, newFlag);
}

void CField::setSharedMediaFirst(bool newFlag)
{
   setFieldFlag(FIELD_FLAG_SHARED_MEDIA_FIRST, newFlag);
}

void CField::setVirtualMedia(bool newFlag)
{
   setFieldFlag(FIELD_FLAG_VIRTUAL_MEDIA, newFlag);
}

void CField::setWriteProtectedMedia(bool newFlag)
{
   // This flag is in the shared Field List

   if (fieldList == 0)
      return;  // field list not available

   CFieldListEntry *fieldListEntry = fieldList->getField(fieldListEntryIndex);
   if (fieldListEntry == 0)
      return;

   fieldListEntry->setMediaWriteProtected(newFlag);
}

void CField::setBorrowedMedia(bool newFlag)
{
   // This flag is in the shared Field List

   if (fieldList == 0)
      return;  // field list not available

   CFieldListEntry *fieldListEntry = fieldList->getField(fieldListEntryIndex);
   if (fieldListEntry == 0)
      return;

   fieldListEntry->setMediaBorrowed(newFlag);
}

void CField::setDirtyMedia(bool newFlag)
{
   // This flag is in the shared Field List

   if (fieldList == 0)
      return;  // field list not available

   CFieldListEntry *fieldListEntry = fieldList->getField(fieldListEntryIndex);
   if (fieldListEntry == 0)
      return;

   fieldListEntry->setMediaDirty(newFlag);
}

void CField::setVisibleSiblingFieldIndex(int visibleSiblingFieldIndex)
{
   bool newFlag;
   if (visibleSiblingFieldIndex == 0)
      {
      // First visible field is sibling
      newFlag = false;
      }
   else
      {
      // Second visible field is sibling
      newFlag = true;
      }

   setFieldFlag(FIELD_FLAG_VISIBLE_SIBLING, newFlag);
}

void CField::setFieldFlags(MTI_UINT16 newFlags)
{
   // Set all field flags as bit flags, mask out reserved bits
   flags = newFlags & ~FIELD_FLAG_RESERVED_BITS;
}

void CField::setFieldFlag(MTI_UINT16 bitFlag, bool newValue)
{
   if (newValue)
      {
      // Set the bit in the field flags
      flags = flags | bitFlag;
      }
   else
      {
      // Clear the bit in the field flags
      flags = flags & ~bitFlag;
      }
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

void CField::assignVirtualMedia(MTI_UINT32 newDataSize)
{
   // Initialize this CField to hold virtual media reference
   
   mediaLocation.setDataSize(newDataSize);  // semi-bogus

   setVirtualMedia(true);
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CField::fixMediaStorageForVirtualCopy(CMediaStorageList &dstMediaStorageList)
{
   CMediaAccess *srcMediaAccess = mediaLocation.getMediaAccess();

   if (dstMediaStorageList.getMediaStorageIndex(srcMediaAccess) == -1)
      {
      // Media Access is missing from destination's Media Storage List,
      // so we need to add it
      dstMediaStorageList.addMediaStorage(srcMediaAccess->getMediaType(),
                                          srcMediaAccess->getMediaIdentifier(),
                                          srcMediaAccess->getHistoryDirectory(),
                                          srcMediaAccess);
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CField::assignDirtyMediaStorage(CMediaLocation dirtyMediaLocation)
{
   if (isDirtyMedia() || !isWriteProtectedMedia())
         return 0;

   setMediaLocation(dirtyMediaLocation);
   setMediaStorageExists(true);     // Field owns a chunk of disk
   setMediaWritten(false);          // Field is empty
   setBorrowedMedia(false);         // We returned it
   setWriteProtectedMedia(false);   // We can now write to it
   setDirtyMedia(true);       // We no longer point to the "clean" media

   return 0;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

void CField::unassignDirtyMediaStorage(const CMediaLocation &cleanMediaLocation,
                                       bool doesCleanMediaStorageExist,
                                       bool isCleanMediaWritten)
{
   if (isDirtyMedia())
      {
      setMediaLocation(cleanMediaLocation);
      setMediaStorageExists(doesCleanMediaStorageExist);
      setMediaWritten(isCleanMediaWritten);
      setBorrowedMedia(true);
      setWriteProtectedMedia(true);
      setDirtyMedia(false);
      }
}


//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CField::allocateDirtyMediaStorage(
                  const CMediaStorageList *mediaStorageList,
                  const CMediaFormat& mediaFormat)
{
   if (mediaStorageList == NULL)
      return -1;

   if (isDirtyMedia() || !isWriteProtectedMedia())
   {
      // Hmm, already dirty, don't reallocate
      return 0;
   }

	int dirtyMediaStorageIndex = mediaStorageList->getDirtyMediaStorageIndex();
   if (dirtyMediaStorageIndex < 0)
      return -1;   // there wasn't one

   const CMediaStorage *dirtyMediaStorage =
                    mediaStorageList->getMediaStorage(dirtyMediaStorageIndex);
   if (dirtyMediaStorage == NULL)
      return -1;

   CMediaAccess *mediaAccess = dirtyMediaStorage->getMediaAccessObj();
   if (mediaAccess == NULL)
      return -1;

	CMediaLocation oldMediaLocation = getMediaLocation();
   int FieldNameAsInteger = 0;
   const char *fieldName = oldMediaLocation.getFieldName();
   if (fieldName != NULL)
      {
      MTIistringstream istr;
      istr.str(fieldName);
      istr >> FieldNameAsInteger;
      }
   CTimecode timecode(FieldNameAsInteger);

   CMediaAllocator *mediaAllocator = mediaAccess->allocate(mediaFormat,
                                                           timecode,
                                       /* frameCount = */  1);
   if (mediaAllocator == NULL)
      return -1;

   CMediaLocation newMediaLocation;
   mediaAllocator->getNextFieldMediaAllocation(newMediaLocation);
   newMediaLocation.setFieldName(oldMediaLocation.getFieldName());
   newMediaLocation.setFieldNumber(oldMediaLocation.getFieldNumber());
   // QQQ what about media location flags??
   setMediaLocation(newMediaLocation);
   setMediaStorageExists(true);     // Field owns a chunk of disk
   setMediaWritten(false);          // Field is empty
   setBorrowedMedia(false);         // We returned it
   setWriteProtectedMedia(false);   // We can now write to it
   setDirtyMedia(true);             // We no longer point to the "clean" media

   // Flush the modified field list entry to disk
   const unsigned int externalBufferSize(4096);  // recommended min size
   char externalBuffer[externalBufferSize];
   getFieldList()->updateFieldListFile(getFieldListEntryIndex(),
                                       /* fieldCount = */ 1,
                                       externalBuffer,
                                       externalBufferSize);
   return 0;
}

//////////////////////////////////////////////////////////////////////
// hack stub
//////////////////////////////////////////////////////////////////////

int CField::hackMedia(EMediaHackCommand hackCommand)
{
   return CLIP_ERROR_MEDIA_CANNOT_BE_HACKED_AS_REQUESTED;
}

//////////////////////////////////////////////////////////////////////
// another hack stub or two
//////////////////////////////////////////////////////////////////////

int CField::transferMediaFrom(CField *anotherField)
{
   return CLIP_ERROR_MEDIA_CANNOT_BE_TRANSFERRED;
}

int CField::copyMediaFrom(CField *anotherField)
{
   return CLIP_ERROR_MEDIA_CANNOT_BE_TRANSFERRED;
}

//////////////////////////////////////////////////////////////////////
// will the madness never end?
//////////////////////////////////////////////////////////////////////

int CField::resetDirtyMetadata(CField *anotherField)
{
	return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

//////////////////////////////////////////////////////////////////////
// yet another hack stub
//////////////////////////////////////////////////////////////////////

int CField::destroyMedia(CMediaAccess *mediaAccessOverride)
{
   return 0;    // fail silently
}

//////////////////////////////////////////////////////////////////////
// Dave's DPX Header Copy Hack
//////////////////////////////////////////////////////////////////////

int CField::writeMediaWithDPXHeaderCopyHack(MTI_UINT8 *buffer,
                                    const string &sourceImageFilePath)
{
   return writeMedia(buffer);
}

//////////////////////////////////////////////////////////////////////
// Testing and Debugging Functions
//////////////////////////////////////////////////////////////////////

void CField::dump(MTIostringstream& str) const
{
   mediaLocation.dump(str);
   str << "  Field Flags: " << flags << std::endl;
   CFieldListEntry *fieldListEntry = fieldList->getField(fieldListEntryIndex);
   if (fieldListEntry != 0)
      fieldListEntry->dump(str);
}


