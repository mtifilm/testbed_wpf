// FileBuilder.h: interface for the CFileBuilder class.
//
/*
$Header: /usr/local/filmroot/clip/include/FileBuilder.h,v 1.2 2003/04/28 19:09:37 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef FILEBUILDERH
#define FILEBUILDERH

#include <string>
using std::string;

#include "FileRecord.h"

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CStructuredStream;
class CCommonHeaderFileRecord;

//////////////////////////////////////////////////////////////////////

class CFileBuilder  
{
public:
   CFileBuilder();
   virtual ~CFileBuilder();

   int createFile(CStructuredStream *newStream, const string& fileName, 
                  EFileType fileType, MTI_UINT16 fileTypeVersion);
   int initForFileUpdate(CStructuredStream *newStream);
   int putRecord(CFileRecord& fileRecord);

private:
   MTI_UINT32 magicWord;
   CCommonHeaderFileRecord* commonHeaderRecord;
   CStructuredStream* stream;
};

//////////////////////////////////////////////////////////////////////

#endif // #ifndef FILEBUILDER_H
