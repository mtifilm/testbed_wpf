// File: Clip5Parser.cpp
//
//    Implementation of parser for clip metadata stored as XML
//
// ---------------------------------------------------------------------------

#include "Clip5Parser.h"
#include "err_clip.h"

#ifdef _MSC_VER
#pragma warning(disable:4291)  // Too hard to fix, should be OK
#define NO_XERCES_HACK
#endif

#ifdef __BORLANDC__
  #ifndef NO_XERCES_HACK
	#include <xercesc/sax2/SAX2XMLReader.hpp>
	#include <xercesc/sax2/XMLReaderFactory.hpp>
	#include <xercesc/sax2/Attributes.hpp>
  #endif
#endif

#include <iostream>
#include <fstream>

#include "Clip5Proto.h"

#ifdef _MSC_VER
  #include "BinManager.h"
  #include "Clip3.h"
  #include "Clip3TimeTrack.h"
  #include "Clip3VideoTrack.h"
  #include "Clip5XMLiteReader.h"
#endif

#ifdef _MSC_VER
  #include "ImageFileMediaAccess.h"
  #include "ImageFormat3.h"
  #include "LineEngine.h"
  #include "MediaAccess.h"
  #include "RawAudioMediaAccess.h"
  #include "MediaAllocator.h"
  #include "MediaDeallocator.h"
  #include "SafeClasses.h"
  #include <fstream>
  #include <memory>
  #include <typeinfo>
  #include <math.h>
#endif

#ifdef __BORLANDC__
  #include "IniFile.h"
  #include "XMLWriter.h"
#endif

#include "MTIstringstream.h"

#ifdef _MSC_VER
  //----------------------------------------------------------------------------
  // Static Data
  // XML Tags and Attribute Names
  static const string trackTag =                  "Track";
  static const string serialNumberAttr =          "SerialNumber";
  static const string trackIDAttr =               "TrackID";
  static const string trackTypeAttr =             "TrackType";
  static const string rangeListTag =              "RangeList";
  static const string editRateAttr =              "EditRate";

  static const string rangeTag =                  "Range";
  static const string rangeTypeAttr =             "RangeType";
  static const string contentTypeAttr =           "ContentType";
  static const string itemCountAttr =             "ItemCount";
  static const string dstStartAttr =              "DstStart";

  static const string segmentTag =                "Segment";
  static const string srcTrackIDAttr =            "SrcTrackID";
  static const string srcStartAttr =              "SrcStart";

  static const string pulldownTag =               "Pulldown";
  static const string pulldownTypeAttr =          "PulldownType";
  static const string pulldownPhaseAttr =         "PulldownPhase";

  static const string mediaLocationRangeTag =     "MediaLocationBlock";
  static const string mediaTypeAttr =             "MediaType";
  static const string mediaIDAttr =               "MediaID";
  static const string dataIndexAttr =             "DataIndex";
  static const string dataSizeAttr =              "DataSize";
  static const string compressedSizeAttr =        "CompressedSize";

  static const string samplesPerFieldAttr =       "SamplesPerField";

  static const string mediaStatusRangeTag =       "MediaStatusBlock";
  static const string mediaStatusAttr =           "MediaStatus";

  static const string mediaTrackAttr =            "MediaTrack";
  static const string mediaStatusTrackAttr =      "MediaStatusTrack";
  static const string defaultMediaTypeAttr =      "DefaultMediaType";
  static const string defaultMediaIDAttr =        "DefaultMediaID";

  static const string imageFormatTag =            "ImageFormat";
  static const string imageFormatSectionPrefix =  "ImageFormat";
  static const string audioFormatTag =            "AudioFormat";
  static const string audioFormatSectionPrefix =  "AudioFormat";

  static const string rawSMPTETimecodeAttr =      "SMPTETimecode";
  static const string rawSMPTEUserDataAttr =      "SMPTEUserData";
  static const string timecodeFrameRateAttr =     "TimecodeFrameRate";
  static const string partialFrameAttr =          "PartialFrame";

  static const string keykodeCalibOffsetAttr =    "KKCalibOffset";
  static const string keykodeFilmDimensionAttr =  "KKFilmDim";
  static const string keykodeInterpolateFlagAttr= "KKIntFlag";
  static const string keykodeStrAttr =            "KKString";

  //----------------------------------------------------------------------------
  // Track Types
  static const CStringValueList<E5TrackType>::SInitPair<E5TrackType> trackTypeInit[] =
  {
     {CLIP5_TRACK_TYPE_MEDIA,           "Media" },
	 {CLIP5_TRACK_TYPE_SEQUENCE,        "Sequence" },
     {CLIP5_TRACK_TYPE_AUDIO_PROXY,     "AudioProxy"    },
     {CLIP5_TRACK_TYPE_KEYCODE,         "Keycode"       },
     {CLIP5_TRACK_TYPE_SMPTE_TIMECODE,  "SMPTETimecode" },
     {CLIP5_TRACK_TYPE_TIMECODE,        "Timecode"      },
     {CLIP5_TRACK_TYPE_MEDIA_STATUS,    "MediaStatus"   },
     {CLIP5_TRACK_TYPE_INVALID, 0}  // terminate list
  };

  static const CStringValueList<E5TrackType> clipTrackType(trackTypeInit);

  //----------------------------------------------------------------------------

  // Range Types
  static const CStringValueList<E5RangeType>::SInitPair<E5RangeType> rangeTypeInit[] =
  {
     {CLIP5_RANGE_TYPE_FILLER,                      "Filler"        },
     {CLIP5_RANGE_TYPE_MEDIA_LOCATION_VIDEO_STORE,  "VideoStore"    },
     {CLIP5_RANGE_TYPE_MEDIA_LOCATION_IMAGE_FILE,   "ImageFile"     },
     {CLIP5_RANGE_TYPE_MEDIA_LOCATION_AUDIO_FILE,   "AudioFile"     },
     {CLIP5_RANGE_TYPE_PULLDOWN,                    "Pulldown"      },
     {CLIP5_RANGE_TYPE_KEYCODE,                     "Keycode"       },
     {CLIP5_RANGE_TYPE_TIMECODE,                    "Timecode"      },
     {CLIP5_RANGE_TYPE_SMPTE_TIMECODE,              "SMPTETimecode" },
     {CLIP5_RANGE_TYPE_MEDIA_STATUS,                "MediaStatus"   },
     {CLIP5_RANGE_TYPE_INVALID, 0}  // terminate list
  };

  static const CStringValueList<E5RangeType> clipRangeType(rangeTypeInit);
#endif

// ---------------------------------------------------------------------------
#ifdef _MSC_VER
int ParseTrack(const string &filename)
{
  static int iCount = 0;
  MTIostringstream errMsg;

  // This function parses a Clip 5 Track file with the Xerces SAX XML Parser

  TRACE_3(errout << "ParseTrack " << filename);

  if (!DoesFileExist(filename))
  {
    errMsg << "ERROR: ParseTrack: Track file does not exist: "
      << filename << endl;
    TRACE_0(errout << errMsg.str());
    theError.set(CLIP_ERROR_TRACK_FILE_DOES_NOT_EXIST, errMsg);
    return CLIP_ERROR_TRACK_FILE_DOES_NOT_EXIST;
  }

  // Initialize the XML Parser system
  Clip5XMLiteReader *reader = new Clip5XMLiteReader(filename);

  // Parse it
  int ret = reader->ParseFile();
  delete reader;

  // Check for errors
  if (ret != S_OK)
  {
    errMsg << "ERROR: PDLParse: An error occurred while parsing track file " << endl
      << "       " << filename << endl
      << "       because of parser error" << endl
      << "       " << ret;

    TRACE_0(errout << errMsg.str());
    theError.set(S_OK, errMsg);
    return S_OK;
  }
  return 0;
} // ParseTrack

#endif  // #ifdef _MSC_VER

#ifdef __BORLANDC__
#ifndef NO_XERCES_HACK

// NOTE: Define "NO_XERCES_HACK" in the project if you do not want
//       the xerces library to be required.  This is only useful
//       for the MTIPackage static .lib and the Avid plug-in build.
//       If NO_XERCES_HACK is defined, Clip 5 XML files cannot be
//       parsed.

// ---------------------------------------------------------------------------
//  This is a simple class that lets us do easy (though not terribly efficient)
//  trancoding of XMLCh data to local code page for display.
// ---------------------------------------------------------------------------
class StrX
{
  public :
  // -----------------------------------------------------------------------
  //  Constructors and Destructor
  // -----------------------------------------------------------------------
  StrX(const XMLCh* const toTranscode)
  {
    // Call the private transcoding method
	fLocalForm = XMLString::transcode(toTranscode);
  }

  ~StrX()
	{
	   XMLString::release(&fLocalForm);
	}

  // Get a pointer to the local code page form of the XMLString
  const char* localForm() const {return fLocalForm;}

  private :
  char* fLocalForm;  //  This is the local code page form of the string.
};

inline ostream& operator<<(ostream& ostrm, const StrX& toDump)
{
  ostrm << toDump.localForm();
  return ostrm;
}

// ---------------------------------------------------------------------------
//  C5SAXHandlers: Constructors and Destructor
// ---------------------------------------------------------------------------
C5SAXHandlers::C5SAXHandlers()
   : parseLevel(-1), currentTrackHandle(0)
{
}

C5SAXHandlers::~C5SAXHandlers()
{
   delete currentTrackHandle;
}

// ---------------------------------------------------------------------------
//  C5SAXHandlers: Overrides of the SAX ErrorHandler interface
// ---------------------------------------------------------------------------
void C5SAXHandlers::error(const SAXParseException& e)
{
   TRACE_0(errout << "C5SAXHandlers: Error at file " << StrX(e.getSystemId())
                  << ", line " << e.getLineNumber()
                  << ", char " << e.getColumnNumber() << endl
                  << "              Message: " << StrX(e.getMessage()));
}

void C5SAXHandlers::fatalError(const SAXParseException& e)
{
   TRACE_0(errout << "C5SAXHandlers: Fatal Error at file " << StrX(e.getSystemId())
                  << ", line " << e.getLineNumber()
                  << ", char " << e.getColumnNumber() << endl
				  << "              Message: " << StrX(e.getMessage()));
}

void C5SAXHandlers::warning(const SAXParseException& e)
{
   TRACE_0(errout << "C5SAXHandlers: Warning at file " << StrX(e.getSystemId())
                  << ", line " << e.getLineNumber()
                  << ", char " << e.getColumnNumber() << endl
                  << "              Message: " << StrX(e.getMessage()));
}

// ---------------------------------------------------------------------------
//  C5SAXHandlers: Overrides of the SAX DocumentHandler interface
// ---------------------------------------------------------------------------

void C5SAXHandlers::startDocument()
{
   parseLevel = 0;
}

void C5SAXHandlers::endDocument()
{
   parseLevel = -1;
}

//------------------------------------------------------------------------
void C5SAXHandlers::startElement(const XMLCh* const uri,
                                     const XMLCh* const localname,
                                     const XMLCh* const qname,
                                     const Attributes& attributes)
{
  int retVal;

  // Convert to ASCII string (don't know if this is kosher)
  string elementName = StrX(qname).localForm();

  MTIostringstream ostrm;

  // Gather the attributes
  C5XMLAttribList attributeList;
  string newAttribName, newAttribValue;
  unsigned int attribCount = attributes.getLength();
  for (unsigned int index = 0; index < attribCount; ++index)
  {
    newAttribName = StrX(attributes.getQName(index)).localForm();
    newAttribValue = StrX(attributes.getValue(index)).localForm();
    attributeList.Add(newAttribName, newAttribValue);
  }
  ostrm.str("");

  if (parseLevel < 1)
  {
    if (elementName == "CLIP_5")
      parseLevel = 1;
  }
  else if (elementName == "Track")
  {
    currentTrackHandle = new C5TrackHandle;
    retVal = currentTrackHandle->CreateTrack(attributeList);
    if (retVal != 0)
    {
      // ERROR: could not create the track
      // TBD: handle error
      delete currentTrackHandle;
      currentTrackHandle = 0;

      // TBD: Do something with error (SAX exception?) so that
      //      XML parsing does not continue.
    }
  }
  else if (currentTrackHandle != 0)
  {
    C5Track *currentTrack = currentTrackHandle->GetTrack();
    // TBD: Should throw exception if currentTrack is NULL
    retVal = currentTrack->xmlParseStartElement(elementName, attributeList);
    if (retVal != 0)
    {
      ;
	  // TBD: Do something with error (SAX exception?) so that
      //      XML parsing does not continue.
    }
  }
}

//-------------------------------------------------------------------
void C5SAXHandlers::endElement(const XMLCh* const uri,
							   const XMLCh* const localname,
                               const XMLCh* const qname)
{
  // Convert to ASCII string (don't know if this is kosher)
  string elementName = StrX(qname).localForm();

  // TRACE_0(errout << "endElement " << elementName);

  if (elementName == "CLIP_5")
    parseLevel = 0;
  else if (currentTrackHandle != 0)
  {
    C5Track *currentTrack = currentTrackHandle->GetTrack();
    // TBD: Should throw exception if currentTrack is NULL
    currentTrack->xmlParseEndElement(elementName);
    if (elementName == "Track")
	{
      delete currentTrackHandle;
      currentTrackHandle = 0;
    }
	else
    {
      //// currentTrackHandle = currentTrackHandle;
    }
  }
}

//-------------------------------------------------------------------
void C5SAXHandlers::characters(const XMLCh* chars, const XMLSize_t length)
{
  MTIostringstream ostrm;

  XMLCh* const tmp = new XMLCh[length+1];
  XMLString::copyNString(tmp, chars, length);

  string tmpStr = StrX(tmp).localForm();

  if (currentTrackHandle != 0)
  {
	C5Track *currentTrack = currentTrackHandle->GetTrack();

    // TBD: Should throw exception if currentTrack is NULL
    currentTrack->xmlParseCharacters(tmpStr);
  }

  delete [] tmp;
}

//---------------------------------------------------------------------
void C5SAXHandlers::ignorableWhitespace(const XMLCh* chars,
										const unsigned long length)
{
  XMLCh *tmp = new XMLCh[length+1];
  XMLString::copyNString(tmp, chars, (XMLSize_t)length);
  tmp[length] = 0;
  string tmpStr = StrX(tmp).localForm();
}

// ---------------------------------------------------------------------------
// Static variables to manage initialization of Xerces-C++ parser
static bool xmlPlatformUtilsInitialized = false;
static SAX2XMLReader *xmlParser = 0;

int ParseTrack(const string &filename)
{
  static int iCount = 0;
  MTIostringstream errMsg;

  // This function parses a Clip 5 Track file with the Xerces SAX XML Parser

  TRACE_3(errout << "ParseTrack " << filename);

  if (!DoesFileExist(filename))
  {
    errMsg << "ERROR: ParseTrack: Track file does not exist: "
      << filename << endl;
    TRACE_0(errout << errMsg.str());
    theError.set(CLIP_ERROR_TRACK_FILE_DOES_NOT_EXIST, errMsg);
	return CLIP_ERROR_TRACK_FILE_DOES_NOT_EXIST;
  }

  // Initialize the XML Parser system
  if (!xmlPlatformUtilsInitialized)
  {
	try
	{
	  XMLPlatformUtils::Initialize();
	}
//// Problem with char in XMLException
////	catch (const XMLException& toCatch)
	catch (...)
	{
	  errMsg << "ERROR: ParseTrack Error during Xerces initialization!" << endl;
////		<< StrX(toCatch.getMessage());
	  TRACE_0(errout << errMsg.str());
	  theError.set(-1, errMsg);
	  return -1;
	}

    //  Create a XML parser instance.
	xmlParser = XMLReaderFactory::createXMLReader();

    // Set parser feature to validate if a DTD or schema is specified
    xmlParser->setFeature(XMLUni::fgSAX2CoreValidation, true);
    xmlParser->setFeature(XMLUni::fgXercesDynamic, true);

    xmlPlatformUtilsInitialized = true;
  }

  int errorCount = 0;
  try
  {
    // Create an instance of the SAX Event handler class that
    // is specific to parsing our PDL, then tell the parser about
    // the handlers
    C5SAXHandlers SAXHandler;
    xmlParser->setContentHandler(&SAXHandler);
    xmlParser->setErrorHandler(&SAXHandler);

    // Start the parsing
    xmlParser->parse(filename.c_str());
    iCount++;

    // We're done parsing, get the error count
    errorCount = xmlParser->getErrorCount();
    if (errorCount > 0)
    {
	  errMsg << "ERRORS: " << errorCount
        << " were encountered while parsing track file " << filename;
      TRACE_0(errout << errMsg.str());
      theError.set(-1, errMsg);
      return -1;
    }
  }
////  catch (const XMLException& toCatch)
  catch (...)
  {
	errMsg << "ERROR: PDLParse: An error occurred while parsing track file " << endl
      << "       " << filename << endl
	  << "       because of XERCES SAX parser error" << endl;
////	  << "       " << StrX(toCatch.getMessage());

	TRACE_0(errout << errMsg.str());
    theError.set(-1, errMsg);
	return -1;
  }
// Removed so we can compile
//  catch (...)
//  {
//	TRACE_0(errout << "CATCH ERRRO");
//  }
  return 0;
} // ParseTrack

// ---------------------------------------------------------------------------
#else // #ifndef NO_XERCES_HACK

int ParseTrack(const string &filename)
{
   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

#endif // #ifndef NO_XERCES_HACK
#endif // #ifdef __BORLANDC__

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
C5XMLAttrib::C5XMLAttrib(const string &newName, const string &newValue)
 : name(newName), value(newValue)
{
}

C5XMLAttrib::C5XMLAttrib(const C5XMLAttrib &src)
 : name(src.name), value(src.value)
{
}

C5XMLAttrib::~C5XMLAttrib()
{
}

C5XMLAttrib& C5XMLAttrib::operator=(const C5XMLAttrib &src)
{
   if (this != &src)    // check for self-copy
      {
      name = src.name;
      value = src.value;
      }

   return *this;
}

void C5XMLAttrib::GetIntegerList(vector<int> &intList)
{
   // Parse a list of integers separated by whitespace or commas
   // Note: this function does not handle hex or 64-bit integers

   string value = GetValueString();

   std::istringstream istrm(value);

   while(istrm)
      {
	  int i;
      istrm >> i;
      intList.push_back(i);
      if (istrm.peek() == ',')
         istrm.ignore(); // skip over commas
      }
}

string C5XMLAttrib::GetValueString()
{
   return value;
}

MTI_INT64 C5XMLAttrib::GetValueInteger()
{
   std::istringstream istr(value);
   MTI_INT64 result;

   // Check if value string is hex digits
   if (value.size() > 2)
      {
      // Borland's hex seems to be broken, use this one
      if ((value[0] == '0') && (toupper(value[1]) == 'X'))
         {
         char *p;
         result = strtol(value.c_str(), &p, 16);
         if (p == value.c_str())
             result = 0;  // error
         return result;
         }
      }

    // Parse as a decimal
    if ((istr >> result) == 0)
       result = 0;
    return(result);
}

MTI_UINT64 C5XMLAttrib::GetValueUnsignedInteger()
{
   std::istringstream istr(value);
   MTI_UINT64 result;

   // Parse as a decimal
   if ((istr >> result) == 0)
      result = 0;
   return(result);
}

bool C5XMLAttrib::GetValueBool()
{
   return (value == "1")
           || EqualIgnoreCase(value, "true")
           || EqualIgnoreCase(value, "t");
}

double C5XMLAttrib::GetValueDouble()
{
   std::istringstream istr(value);
   double result;

   // Parse as a decimal
   if ((istr >> result) == 0)
      result = 0.0;
   return(result);
}

void C5XMLAttrib::SetValueString(const string &newValue)
{
   value = newValue;
}

void C5XMLAttrib::SetValueInteger(MTI_INT64 newValue)
{
   MTIostringstream ostrm;
   ostrm << newValue;
   value = ostrm.str();
}

void C5XMLAttrib::SetValueUnsignedInteger(MTI_UINT64 newValue)
{
   MTIostringstream ostrm;
   ostrm << newValue;
   value = ostrm.str();
}

void C5XMLAttrib::SetValueBool(bool newValue)
{
   value = (newValue) ? "true" : "false";
}

void C5XMLAttrib::SetValueDouble(double newValue)
{
   MTIostringstream ostrm;
   ostrm << newValue;
   value = ostrm.str();
}

void C5XMLAttrib::writeXMLStream(CXMLStream &xmlStream)
{
   xmlStream << attr(name) << escape(value);
}

void C5XMLAttrib::dump(ostringstream& ostrm, string &indentLevel)
{
   ostrm << indentLevel << "A " << name << "=" << value << std::endl;
}

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

C5XMLAttribList::C5XMLAttribList()
{
}

C5XMLAttribList::C5XMLAttribList(const C5XMLAttribList &src)
{
   vector<C5XMLAttrib*>::const_iterator iter;
   for (iter = src.attributeList.begin(); iter != src.attributeList.end(); ++iter)
      {
      attributeList.push_back(new C5XMLAttrib(*(*iter)));
      }
}

C5XMLAttribList::~C5XMLAttribList()
{
   Clear();
}

C5XMLAttribList& C5XMLAttribList::operator=(const C5XMLAttribList &src)
{
   if (this == &src)  // check for self-copy
      return *this;

   Clear();  // clear existing attribute list

   vector<C5XMLAttrib*>::const_iterator iter;
   for (iter = src.attributeList.begin();
        iter != src.attributeList.end();
        ++iter)
      {
      attributeList.push_back(new C5XMLAttrib(*(*iter)));
      }

   return *this;
}

void C5XMLAttribList::Clear()
{
   vector<C5XMLAttrib*>::iterator iter;
   for (iter = attributeList.begin(); iter != attributeList.end(); ++iter)
      {
	  delete *iter;
      }

   attributeList.clear();
}

C5XMLAttrib* C5XMLAttribList::Add(const string &newName, const string &newValue)
{
   C5XMLAttrib *newAttrib = new C5XMLAttrib(newName, newValue);
   attributeList.push_back(newAttrib);

   return newAttrib;
}

bool C5XMLAttribList::DoesNameExist(const string &attribName)
{
   vector<C5XMLAttrib*>::iterator iter;
   for (iter = attributeList.begin(); iter != attributeList.end(); ++iter)
      {
      if ((*iter)->name == attribName)
         return true; // yeah, found it!
      }

   return false;   // nothing found, return false
}

string C5XMLAttribList::Find(const string &attribName)
{
   vector<C5XMLAttrib*>::iterator iter;
   for (iter = attributeList.begin(); iter != attributeList.end(); ++iter)
      {
      if ((*iter)->name == attribName)
         return (*iter)->value;
      }

   return ("");   // empty string if attribName was not found
}

C5XMLAttrib* C5XMLAttribList::FindAttrib(const string &attribName)
{
   vector<C5XMLAttrib*>::iterator iter;
   for (iter = attributeList.begin(); iter != attributeList.end(); ++iter)
      {
      if ((*iter)->name == attribName)    // case sensitive
         return (*iter); // found it!
      }

   return 0;   // boo hoo, didn't find nuthin
}

unsigned int C5XMLAttribList::GetCount()
{
   return (unsigned int) attributeList.size();
}

C5XMLAttrib* C5XMLAttribList::GetAttrib(unsigned int index)
{
   if (index >= GetCount())
      return 0;

   return attributeList[index];
}

string C5XMLAttribList::GetValueString(const string &attribName)
{
   C5XMLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      return "";  // didn't find the attribute with the given name

   return attrib->GetValueString();
}

string C5XMLAttribList::GetValueString(const string &attribName,
                                      const string &defaultValue)
{
   C5XMLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      return defaultValue;  // didn't find the attribute with the given name

   return attrib->GetValueString();
}

void C5XMLAttribList::GetIntegerList(const string &attribName,
                                    vector<int> &intList)
{
   intList.clear();

   C5XMLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      return;  // didn't find the attribute with the given name

   attrib->GetIntegerList(intList);
}

MTI_INT64 C5XMLAttribList::GetValueInteger(const string &attribName)
{
   C5XMLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      return 0;  // didn't find the attribute with the given name

   return attrib->GetValueInteger();
}

MTI_INT64 C5XMLAttribList::GetValueInteger(const string &attribName,
                                          MTI_INT64 defaultValue)
{
   C5XMLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      return defaultValue;  // didn't find the attribute with the given name

   string valueString = StringTrimLeft(attrib->GetValueString());
   std::istringstream istrm;
   istrm.str(valueString);
      // Check if value string is hex digits
   if (valueString.size() > 2)
   {
   if (valueString[0] == '0' && (valueString[1] == 'x' || valueString[1] == 'X'))
      istrm.unsetf(std::ios::dec);     // let base of value be determined by the
                                       // leading characters, in this case
                                       // leading 0x or 0X means Hex.
   }

   MTI_INT64 value;
   if (istrm >> value)
      return value;
   else
      return defaultValue; // conversion failed, used default value
}

MTI_UINT64 C5XMLAttribList::GetValueUnsignedInteger(const string &attribName)
{
   C5XMLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      return 0;  // didn't find the attribute with the given name

   return attrib->GetValueUnsignedInteger();
}

MTI_UINT64 C5XMLAttribList::GetValueUnsignedInteger(const string &attribName,
                                                   MTI_UINT64 defaultValue)
{
   C5XMLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      return defaultValue;  // didn't find the attribute with the given name

   string valueString = StringTrimLeft(attrib->GetValueString());
   std::istringstream istrm;
   istrm.str(valueString);
   if (valueString[0] == '0' && (valueString[1] == 'x' || valueString[1] == 'X'))
      istrm.unsetf(std::ios::dec);     // let base of value be determined by the
                                       // leading characters, in this case
                                       // leading 0x or 0X means Hex.

   MTI_UINT64 value;
   if (istrm >> value)
      return value;
   else
      return defaultValue; // conversion failed, used default value
}

bool C5XMLAttribList::GetValueBool(const string &attribName)
{
   C5XMLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      return 0;  // didn't find the attribute with the given name

   return attrib->GetValueBool();
}

bool C5XMLAttribList::GetValueBool(const string &attribName, bool defaultValue)
{
   C5XMLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      return defaultValue;  // didn't find the attribute with the given name

   return attrib->GetValueBool();
}

double C5XMLAttribList::GetValueDouble(const string &attribName)
{
   C5XMLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      return 0.0;  // didn't find the attribute with the given name

   return attrib->GetValueDouble();
}

double C5XMLAttribList::GetValueDouble(const string &attribName,
                                    double defaultValue)
{
   C5XMLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      return defaultValue;  // didn't find the attribute with the given name

   return attrib->GetValueDouble();
}

void C5XMLAttribList::SetValueString(const string &attribName,
                                    const string &newValue)
{
   C5XMLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      Add(attribName, newValue);
   else
      attrib->SetValueString(newValue);
}

void C5XMLAttribList::SetValueInteger(const string &attribName,
                                     MTI_INT64 newValue)
{
   string emptyStr;
   C5XMLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      attrib = Add(attribName, emptyStr);

   attrib->SetValueInteger(newValue);
}

void C5XMLAttribList::SetValueUnsignedInteger(const string &attribName,
                                             MTI_UINT64 newValue)
{
   string emptyStr;
   C5XMLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      attrib = Add(attribName, emptyStr);

   attrib->SetValueUnsignedInteger(newValue);
}

void C5XMLAttribList::SetValueBool(const string &attribName,
                                  bool newValue)
{
   string emptyStr;
   C5XMLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      attrib = Add(attribName, emptyStr);

   attrib->SetValueBool(newValue);
}

void C5XMLAttribList::SetValueDouble(const string &attribName,
                                    double newValue)
{
   string emptyStr;
   C5XMLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      attrib = Add(attribName, emptyStr);

   attrib->SetValueDouble(newValue);
}

void C5XMLAttribList::writeXMLStream(CXMLStream &xmlStream)
{
   if (!attributeList.empty())
      {
      for (vector<C5XMLAttrib*>::iterator iter = attributeList.begin();
           iter!= attributeList.end();
           ++iter)
         {
         (*iter)->writeXMLStream(xmlStream);
         }
      }
}

void C5XMLAttribList::dump(ostringstream& ostrm, string &indentLevel)
{
   if (!attributeList.empty())
      {
      for (vector<C5XMLAttrib*>::iterator iter = attributeList.begin();
           iter!= attributeList.end();
           ++iter)
         {
         (*iter)->dump(ostrm, indentLevel);
         }
      }
}




