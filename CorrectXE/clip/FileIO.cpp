// FileIO.cpp: implementation of the CFileIO class.
//
/*
$Header: /usr/local/filmroot/clip/source/FileIO.cpp,v 1.18 2006/10/26 17:35:54 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "IniFile.h"      // Trace
#include <errno.h>

#if defined(__sgi) || defined(__linux) // UNIX compilers
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#elif defined(_WIN32)   // Win32 API, MS VC++ & Borland C++Builder

#include <time.h>      // time_t is broken in sys/types.h in RadStudio XE4
#include <sys/types.h>
#include <sys/stat.h>

#else
#error Unknown Compiler
#endif

#include "FileIO.h"
#include "err_clip.h"

/*-----------GetSystemErrorMessage------------------------JAM--Mar 2000----*/

static  string GetSystemMessage(int ErrorCode)

/* This function returns a string that contains a human readable     */
/* version of the error message in ErrorCode                         */
/* Ususal usage is ErrorMessage = GetSystemMessage(GetLastError());  */
/*                                                                   */
/*********************************************************************/
{
  char cErrorMessage[255];

  string Result = "";

  // Do nothing if not an error
  if (ErrorCode == 0)
    {
       return(Result);
    }

  FormatMessage
          (
             FORMAT_MESSAGE_FROM_SYSTEM,
             NULL,
             ErrorCode,
             MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
             (LPTSTR) &cErrorMessage,
             255,
             NULL
          );

  Result = cErrorMessage;
  return(Result);
};


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFileIO::CFileIO()
{
#if defined(__sgi) || defined(__linux) // UNIX compilers
   handle = -1;
#elif defined(_WIN32)      // Windows, VC++ or BC++
   handle = INVALID_HANDLE_VALUE;
#else
#error Unknown Compiler
#endif

}

CFileIO::~CFileIO()
{
   close();
}

//////////////////////////////////////////////////////////////////////

int CFileIO::open(const string& fileName)
{
#if defined(__sgi) || defined(__linux) // UNIX compilers
   handle = ::open(fileName.c_str(), O_RDWR|O_BINARY, 0666);
   if (handle == -1)
      {
      TRACE_0(errout << "CFileIO::open failed to for file "
                     << fileName << endl;
              errout << strerror(errno) << endl;);
      return -1;
      }
   else
      return 0;

#elif defined(_WIN32)      // Windows, VC++ or BC++
//
// JAM Temporary kluge to allow audio read/write
// Next two lines replaced with SHARE_READ
//   handle = CreateFile(fileName.c_str(), GENERIC_READ,
//                       0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
   handle = CreateFile(fileName.c_str(), GENERIC_READ | GENERIC_WRITE,
                       FILE_SHARE_READ | FILE_SHARE_WRITE, 0, OPEN_EXISTING,
                       FILE_ATTRIBUTE_NORMAL, 0);
   if (handle == INVALID_HANDLE_VALUE)
      {
      TRACE_0(errout << "CFileIO::open failed to for file "
                     << fileName << endl;
              errout << GetSystemMessage(GetLastError()) << endl;);
      // GetLastError();
      return -1;
      }
   else
      return 0; // success

#else
#error Unknown Compiler
#endif
}

int CFileIO::close()
{
#if defined(__sgi) || defined(__linux) // UNIX compilers
   if (handle != -1)
      {
      int retVal = ::close(handle);
      handle = -1;
      return(retVal);
      }
   else
      return 0;

#elif defined(_WIN32)      // Windows, VC++ or BC++
   if (handle != INVALID_HANDLE_VALUE)
      {
      BOOL retVal = CloseHandle(handle);
      handle = INVALID_HANDLE_VALUE;
      if(retVal)
         return 0;
      else
         return -1;
      }

#else
#error Unknown Compiler
#endif
   return -1;
}

//////////////////////////////////////////////////////////////////////

int CFileIO::read(void *buffer, MTI_UINT32 count)
{
#if defined(__sgi) || defined(__linux)            // UNIX

   // Use C-library low-level file read function
   int status = ::read(handle, buffer, count);

   if(status == -1)
      {
      TRACE_3(errout << "CFileIO::read failed " << endl;
              errout << strerror(errno) << endl;);
      return status;
      }

   return status;  // Success

#elif defined(_WIN32)   // Defined by Microsoft and Borland compilers
   
   // Use Win32 API file read function
   DWORD numberOfBytesRead;
   BOOL retStatus;
   retStatus = ReadFile(handle, buffer, count, &numberOfBytesRead, 0);
   if (retStatus)
      return(numberOfBytesRead);
   else
      {
      TRACE_3(errout << "CFileIO::read failed " << endl;
              errout << strerror(errno) << endl;);
      return -1;
      }

#else
#error Unknown compiler
#endif
}

//////////////////////////////////////////////////////////////////////

int CFileIO::write(const void *buffer, MTI_UINT32 count)
{
#if defined(__sgi) || defined(__linux) // UNIX

   // Use C-library low-level file write function
   int status = ::write(handle, buffer, count);

   if(status == -1)
      {
      TRACE_3(errout << "CFileIO::write failed " << endl;
              errout << strerror(errno) << endl;);
      return status;
      }

   return(status);

#elif defined(_WIN32)  // Win32 API, Defined by Microsoft and Borland compilers
   
   // Use Win32 API file write function
   DWORD numberOfBytesWritten;
   BOOL retStatus;
   retStatus = WriteFile(handle, buffer, count, &numberOfBytesWritten, 0);
   if (retStatus)
      return(numberOfBytesWritten);
   else
      {
      TRACE_3(errout << "CFileIO::write failed " << endl;
              errout << strerror(errno) << endl;);
      return -1;
      }

#else
#error Unknown compiler
#endif
}

//////////////////////////////////////////////////////////////////////

int CFileIO::write(const void *buffer, MTI_INT64 offset, MTI_UINT32 count)
{
#if defined(__sgi) || defined(__linux)    // UNIX

   return pwrite64(handle, buffer, count, offset);

#elif defined(_WIN32)  // Win32 API, Defined by Microsoft and Borland compilers

   DWORD retVal;
   retVal = SetFilePointer(handle, LOWLONGLONG(offset), ((LONG*)&offset)+1,
                           FILE_BEGIN);
   if (retVal == INVALID_SET_FILE_POINTER && GetLastError() != NO_ERROR)
      {
      // GetLastError();
      return -1;
      }

   DWORD numberOfBytesWritten;
   BOOL retStatus;
   retStatus = WriteFile(handle, buffer, count, &numberOfBytesWritten, 0);
   if (retStatus)
      return(numberOfBytesWritten);
   else
      {
      TRACE_3(errout << "CFileIO::write failed " << endl;
              errout << strerror(errno) << endl;);
      return -1;
      }

#else
#error Unknown compiler
#endif
}

//////////////////////////////////////////////////////////////////////

int CFileIO::seek(MTI_INT64 offset)
{
#if defined(__sgi) || defined(__linux)    // UNIX

   // lseek64 returns -1 if it fails, byte offset if it succeeds
   if (lseek64(handle, offset, SEEK_SET) == -1)
      return CLIP_ERROR_FILE_SEEK_FAILED;

   return 0;

#elif defined(_WIN32)  // Win32 API, Defined by Microsoft and Borland compilers

   DWORD retVal;
   retVal = SetFilePointer(handle, LOWLONGLONG(offset), ((LONG*)&offset)+1,
                           FILE_BEGIN);
   if (retVal == INVALID_SET_FILE_POINTER && GetLastError() != NO_ERROR)
      {
      // GetLastError();
      return CLIP_ERROR_FILE_SEEK_FAILED;
      }

   return 0;

#else
#error Unknown compiler
#endif
}

//////////////////////////////////////////////////////////////////////

int CFileIO::createFile(const string& fileName)
{
#if defined(__sgi) || defined(__linux)  // UNIX

   // Create the file.  Read/Write User & Group
   handle = creat(fileName.c_str(), S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

   if (handle == -1)
      {
      TRACE_3(errout << "CFileIO::createFile failed to create file "
                     << fileName << endl;
              errout << strerror(errno) << endl;);
      return -1;
      }

#elif defined(_WIN32)      // Win32 API

   // Create the file
//
// JAM Temporary kluge to allow audio read/write
// Next two lines replaced with SHARE_READ
//
//   handle = CreateFile(fileName.c_str(), GENERIC_READ | GENERIC_WRITE,
//                       0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
   handle = CreateFile(fileName.c_str(), GENERIC_READ | GENERIC_WRITE,
                       FILE_SHARE_READ | FILE_SHARE_WRITE, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
   if (handle == INVALID_HANDLE_VALUE)
      {
      // GetLastError();
      TRACE_3(errout << "CFileIO::createFile failed to create file "
                     << fileName << endl;
              errout << strerror(errno) << endl;);
      return -1;
      }

#else
#error Unknown compiler
#endif
   return 0;
}

//////////////////////////////////////////////////////////////////////

bool CFileIO::checkFileExists(const string& fileName)
{
   // Note: this function uses the C-library stat function to determine
   //       if the file exists.  There probably is a Win32 equivalent
   //       to stat that should be used under MS Windows

   struct stat buf;

   // Get file status information
   int result = stat(fileName.c_str(), &buf);

   if (result == 0)
      {
      // File exists
      return true;
      }
   
   if (errno == ENOENT)
      {
      // File or path could not be found
      return false;
      }

   // Assume file exists even though stat failed
   return true;
}

//////////////////////////////////////////////////////////////////////

int CFileIO::deleteFile(const string &fileName)
{
#if defined(__sgi) || defined(__linux)    // UNIX

   // Delete the file
   int retVal = unlink(fileName.c_str());

   if (retVal != 0)
      {
      TRACE_3(errout << "CFileIO::deleteFile failed to unlink file "
                     << fileName << endl;
              errout << strerror(errno) << endl;);
      return retVal;
      }

#elif defined(_WIN32)      // Win32 API

   // Delete the file
   int retVal = DeleteFile(fileName.c_str());

   if (retVal == 0)
      {
      // GetLastError();
      TRACE_3(errout << "CFileIO::deleteFile failed to delete file "
                     << fileName << endl;
              errout << strerror(errno) << endl;);
      return -1;
      }

#else
#error Unknown compiler
#endif
   return 0;
}

