// ClipAPI.h: Clip Application Programming Interface
//
/*
$Header: /usr/local/filmroot/clip/include/ClipAPI.h,v 1.10 2003/07/23 17:57:31 mlm Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef CLIPAPIH
#define CLIPAPIH

#ifdef _MSC_VER
// Disable warning C4786: symbol greater than 255 characters,
// Work-around for problem when using Microsoft compiler
// with vector of strings or string pointers
#pragma warning(disable: 4786)
#endif // #ifdef _MSC_VER

#include "cliplibint.h"
#include "err_clip.h"
#include "guid_mti.h"
#include "ImageFormat3.h"
#include "timecode.h"
#include "MediaInterface.h"
#include "AudioFormat3.h"

// -------------------------------------------------------------------------

#include "BinManager.h"
#include "Clip3.h"
#include "Clip3TimeTrack.h"
#include "Clip3VideoTrack.h"

//////////////////////////////////////////////////////////////////////

#endif // #ifndef CLIP_API_H

