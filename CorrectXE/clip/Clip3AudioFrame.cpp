// Clip3AudioFrame.cpp: implementation of the CAudioFrame class.
//
/*
$Header: /usr/local/filmroot/clip/source/Clip3AudioFrame.cpp,v 1.6 2004/06/01 02:10:23 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "Clip3AudioTrack.h"
#include "timecode.h"
#include "AudioFormat3.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAudioFrame::CAudioFrame(int reserveFieldCount)
: CFrame(reserveFieldCount)
{

}

CAudioFrame::~CAudioFrame()
{
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

CField* CAudioFrame::createField(CFieldList *newFieldList,
                                 int fieldListEntryIndex)
{
   // Create a new Audio Field and add it to the Frame's Field List
   
   CAudioField *newField = new CAudioField;

   newField->setFieldListEntryIndex(newFieldList, fieldListEntryIndex);

   addField(newField);

   return newField;
}

CField* CAudioFrame::createField(const CField& srcField)
{
   // Create a new Audio Field and add it to the Frame's Field List

   const CAudioField &srcAudioField
                                 = static_cast<const CAudioField&>(srcField);

   CAudioField *newField = new CAudioField(srcAudioField);

   addField(newField);

   return newField;
}

//////////////////////////////////////////////////////////////////////
// Mutator Functions
//////////////////////////////////////////////////////////////////////

void CAudioFrame::setAudioFormat(const CAudioFormat *newAudioFormat)
{
   mediaFormat = newAudioFormat;
}

void CAudioFrame::setSlip(double newSlip)
{
   slip = newSlip;
}

//////////////////////////////////////////////////////////////////////
// Accessor Functions
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    getField
//
// Description: Get a pointer to a particular field in a frame given the
//              field index. 
//
// Arguments:   int fieldIndex  Index of the field, 0 is the first field
//
// Returns:     Pointer to a CAudioField object for the specified index
//              If the field index is not valid, returns a NULL pointer
//
//------------------------------------------------------------------------
CAudioField* CAudioFrame::getField(int fieldIndex)
{
   return static_cast<CAudioField*>(getBaseField(fieldIndex));
}

const CAudioFormat* CAudioFrame::getAudioFormat() const
{
   return static_cast<const CAudioFormat*>(mediaFormat);
}

double CAudioFrame::getSlip() const
{
   return slip;
}

//////////////////////////////////////////////////////////////////////
// Virtual Copy
//////////////////////////////////////////////////////////////////////

int CAudioFrame::virtualCopy(CAudioFrame &srcFrame,
                             CMediaStorageList &dstMediaStorageList)
{
   int retVal;

   // Virtual copy of field list
   // Note: assumes matching number of fields in src and dst, doesn't check
   int srcFieldCount = srcFrame.getTotalFieldCount();
   for (int i = 0; i < srcFieldCount; ++i)
      {
      CAudioField *dstField = getField(i);
      CAudioField *srcField = srcFrame.getField(i);
      retVal = dstField->virtualCopy(*srcField, dstMediaStorageList);
      if (retVal != 0)
         return retVal;
      }

   setTimecode(srcFrame.getTimecode());

   // Note: Audio format pointer is not changed.  Audio Format is
   //       assumed to be identical for both source and destination

   setSlip(srcFrame.getSlip());

   return 0;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

void CAudioFrame::blackenField(CField *field, int fieldIndex,
                               MTI_UINT8 *buffer)
{
   // Do nothing
}

//////////////////////////////////////////////////////////////////////
// Testing and Debugging Functions
//////////////////////////////////////////////////////////////////////

void CAudioFrame::dump(MTIostringstream& str)
{
   CTimecode tmpTimecode = getTimecode();

   CFrame::dump(str);
}


