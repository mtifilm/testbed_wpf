// MediaDeallocList.cpp: implementation of the CMediaDeallocList class.
//
//////////////////////////////////////////////////////////////////////

#include "MediaDeallocList.h"
#include "MediaLocation.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMediaDeallocList::CMediaDeallocList()
{

}

CMediaDeallocList::~CMediaDeallocList()
{

}

//////////////////////////////////////////////////////////////////////
// Accessors 
//////////////////////////////////////////////////////////////////////

CMediaDeallocList::DeallocList& CMediaDeallocList::getDeallocList()
{
   return deallocList;
}

//////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////

void CMediaDeallocList::addToDeallocList(const CMediaLocation& mediaLocation)
{
   // Clip 3 Legacy API
   addToDeallocList(mediaLocation.getDataIndex(), mediaLocation.getDataSize());
}

void CMediaDeallocList::addToDeallocList(const CMediaLocation& mediaLocation,
                                         int itemCount)
{
   MTI_INT64 totalDataSize = (MTI_INT64)itemCount
                              * (MTI_INT64)mediaLocation.getDataSize();
   addToDeallocList(mediaLocation.getDataIndex(), totalDataSize);
}

void CMediaDeallocList::addToDeallocList(MTI_INT64 newDataIndex,
                                         MTI_INT64 newDataSize)
{
   if (deallocList.empty())
      {
      // Deallocation list is currently empty, so mediaLocation defines
      // first entry in list
      CMediaDeallocListEntry listEntry(newDataIndex, newDataSize);
      deallocList.push_back(listEntry);
      return;
      }
   
   bool done = false;

   DeallocListIterator first = deallocList.begin();
   DeallocListIterator last = deallocList.end();
   --last;

   MTI_INT64 newStart = newDataIndex;
   MTI_INT64 newEnd = newStart + newDataSize;

   // Check if new block is completely before or completely after all the blocks
   // in the list.  If so, add new block at beginning or end
   if (newEnd < first->dataIndex || newStart > last->dataEnd)
      {
      CMediaDeallocListEntry listEntry(newDataIndex, newDataSize);

      if (newEnd < first->dataIndex)
         {
         deallocList.push_front(listEntry);
         }
      else
         {
         deallocList.push_back(listEntry);
         }
      return;
      }

   // Search for new start
   bool newStartBetweenBlocks = false;
   DeallocListIterator iterator = last;
   while (!done)
      {
      if (iterator->dataEnd < newStart)
         {
         // New start is between two existing blocks
         ++iterator;
         newStartBetweenBlocks = true;
         done = true;
         }
      else if (iterator->dataIndex <= newStart && newStart <= iterator->dataEnd)
         {
         // New start is within an existing block
         done = true;
         }
      else if (iterator == first)
         {
         // New start is before the first existing block
         done = true;
         }
      else
         {
         --iterator;
         }
      }

   DeallocListIterator newStartIterator = iterator;
   
   // Search for new end, traversing the blocks from the point
   // that the new Start was located
   done = false;
   while (!done)
      {
      if (newEnd < iterator->dataIndex)
         {
         // The new end falls between two existing blocks
         if (newStartBetweenBlocks && newStartIterator == iterator)
            {
            // The new Start fell between the same blocks, so
            // add a new block
            CMediaDeallocListEntry listEntry(newStart, newDataSize);
            deallocList.insert(iterator, listEntry);
            return;
            }
         else
            {
            --iterator;
            done = true;
            }
         }
      else if (iterator->dataIndex <= newEnd && newEnd <= iterator->dataEnd)
         {
         // New end is within an existing block
         done = true;
         }
      else if (iterator == last)
         {
         // New end is after the last existing block
         done = true;
         }
      else
         {
         ++iterator;
         }
      }

   DeallocListIterator newEndIterator = iterator;

   // Merge new block with existing blocks
   if (newStart < newStartIterator->dataIndex)
      {
      newStartIterator->dataIndex = newStart;
      }

   if (newEndIterator->dataEnd < newEnd)
      {
      newStartIterator->dataEnd = newEnd;
      }
   else
      {
      newStartIterator->dataEnd = newEndIterator->dataEnd;
      }

   newStartIterator->dataSize = newStartIterator->dataEnd 
                                 - newStartIterator->dataIndex;

   if (newStartIterator != newEndIterator)
      {
      // Delete the list blocks that have been absorbed into the 
      // updated block
      ++newStartIterator;
      ++newEndIterator;
      deallocList.erase(newStartIterator, newEndIterator);
      }
}

