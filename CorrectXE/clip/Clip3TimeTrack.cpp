// Clip3TimeTrack.cpp: implementation of the CTimeTrack, CTimeFrameList
//                     and various constituent classes
//
// Originally composed by: John Starr, January 26, 2003
//
///////////////////////////////////////////////////////////////////////////////

#define _CLIP_3_TIME_TRACK_DATA_CPP

#include "Clip3TimeTrack.h"
#include "Clip3.h"
#include "Clip3TimeTrackFile.h"
#include "err_clip.h"
#include "MTIstringstream.h"

///////////////////////////////////////////////////////////////////////////////
// Static Variables

string CTimeTrackList::maxTrackIndexKey = "MaxTrackIndex";
string CTimeTrackList::timeTrackSectionPrefix = "TimeTrack";

string CTimeTrack::frameListFileExtension = "trk";
string CTimeTrack::clipVerKey = "ClipVer";
string CTimeTrack::trackIDKey = "TrackID";
string CTimeTrack::appIdKey = "AppId";
string CTimeTrack::timeTypeKey = "TimeType";
string CTimeTrack::framingTypeKey = "FramingType";

#ifdef PRIVATE_STATIC
//////////////////////////////////////////////////////////////////////
// The following variables used to be static class members.
// I switched the to namespaced globals for compatability with
// visual c++.  -Aaron Geman  7/9/04
//////////////////////////////////////////////////////////////////////

namespace Clip3TimeTrackData {
   bool iniFileRead = false;
   CMTIKeykodeFilmTypeTable::ManufTable * manufTable = 0;
   CMTIKeykodeFilmTypeTable::FilmTypeTableList * allFilmTypeTables = 0;

   vector<CMTIKeykodeFilmTypeTable::SRecentSearch> * recentSearchList = 0;

   string mtiLocalMachineFileSection = "General";
   string mtiLocalMachineFileKey = "KeykodeFileTypeFile";
   string defaultFileName = "$(CPMP_LOCAL_DIR)KeykodeFilmTypes.ini";
   string manufacturerCodesSectionName = "ManufacturerCodes";
   string manufacturerSectionPrefix = "Manufacturer_";
   string filmTypeTableSectionPrefix = "FilmTypeTable_";
   string filmTypeTableKey = "FilmTypeTable";
   string manufacturerLetterKey = "ManufacturerLetter";

   string maxTrackIndexKey = "MaxTrackIndex";
   string timeTrackSectionPrefix = "TimeTrack";

   string frameListFileExtension = "trk";
   string appIdKey = "AppId";
   string timeTypeKey = "TimeType";
   string framingTypeKey = "FramingType";
};
using namespace Clip3TimeTrackData;
#endif

// ----------------------------------------------------------------------------
// Time Type Map Table

struct STimeTypeMap
{
   const char* name;
   ETimeType timeType;
};

static const STimeTypeMap timeTypeMap[] =
{
   {"Timecode",        TIME_TYPE_TIMECODE         },
   {"Keykode",         TIME_TYPE_KEYKODE          },
   {"SMPTETimecode",   TIME_TYPE_SMPTE_TIMECODE   },
   {"Event",           TIME_TYPE_EVENT            },
   {"AudioProxy",      TIME_TYPE_AUDIO_PROXY      },
};
#define TIME_TYPE_COUNT (sizeof(timeTypeMap)/sizeof(STimeTypeMap))

// ----------------------------------------------------------------------------
// CMTIKeykodeFilmTypeTable static member data

bool CMTIKeykodeFilmTypeTable::iniFileRead = false;
CMTIKeykodeFilmTypeTable::ManufTable* CMTIKeykodeFilmTypeTable::manufTable = 0;
CMTIKeykodeFilmTypeTable::FilmTypeTableList* CMTIKeykodeFilmTypeTable::allFilmTypeTables = 0;

//vector<CMTIKeykodeFilmTypeTable::SRecentSearch> CMTIKeykodeFilmTypeTable::recentSearchList;
vector<CMTIKeykodeFilmTypeTable::SRecentSearch> *CMTIKeykodeFilmTypeTable::recentSearchList = 0;

string CMTIKeykodeFilmTypeTable::mtiLocalMachineFileSection = "General";
string CMTIKeykodeFilmTypeTable::mtiLocalMachineFileKey = "KeykodeFileTypeFile";
string CMTIKeykodeFilmTypeTable::defaultFileName = "$(CPMP_LOCAL_DIR)KeykodeFilmTypes.ini";
string CMTIKeykodeFilmTypeTable::manufacturerCodesSectionName = "ManufacturerCodes";
string CMTIKeykodeFilmTypeTable::manufacturerSectionPrefix = "Manufacturer_";
string CMTIKeykodeFilmTypeTable::filmTypeTableSectionPrefix = "FilmTypeTable_";
string CMTIKeykodeFilmTypeTable::filmTypeTableKey = "FilmTypeTable";
string CMTIKeykodeFilmTypeTable::manufacturerLetterKey = "ManufacturerLetter";

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//
// CTimeFrame Class Implementation
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTimeFrame::CTimeFrame()
 : needsUpdate(false)
{
}

CTimeFrame::~CTimeFrame()
{
}

//////////////////////////////////////////////////////////////////////
//  Accessors
//////////////////////////////////////////////////////////////////////

bool CTimeFrame::getNeedsUpdate() const
{
   return needsUpdate;
}

//////////////////////////////////////////////////////////////////////
//  Mutators
//////////////////////////////////////////////////////////////////////

void CTimeFrame::setNeedsUpdate(bool newUpdateFlag)
{
   needsUpdate = newUpdateFlag;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//
// CMTIKeykodeFilmEmulsionCodeTable Class Implementation
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMTIKeykodeFilmTypeTable::CMTIKeykodeFilmTypeTable()
{
}

CMTIKeykodeFilmTypeTable::~CMTIKeykodeFilmTypeTable()
{
}

void CMTIKeykodeFilmTypeTable::Clear()
{
  manufTable->clear();
  delete manufTable;
  manufTable = 0;

  if (allFilmTypeTables)
   {
    for (FilmTypeTableList::iterator iter = allFilmTypeTables->begin();
        iter != allFilmTypeTables->end();
        ++iter)
      {
       delete (*iter);
      }

    allFilmTypeTables->clear();
    delete allFilmTypeTables;
    allFilmTypeTables = 0;
   }

  if (recentSearchList)
   {
    recentSearchList->clear();
    delete recentSearchList;
    recentSearchList = 0;
   }

  iniFileRead = false;
}

//////////////////////////////////////////////////////////////////////
//  Lookup film emulsion code
//////////////////////////////////////////////////////////////////////

string CMTIKeykodeFilmTypeTable::lookup(char *fecStr)
{
   int retVal;
   string dummyFilmTypeStr = "??";

   // we use the value "99" to indicate manual ink numbers
   if (strncmp (fecStr, "99", 2) == 0)
    {
     string strTmp;
     strTmp.append(fecStr+2, 2);
     return strTmp;
    }

   if (!iniFileRead)
      {
      manufTable = new ManufTable;
      allFilmTypeTables = new FilmTypeTableList;
      recentSearchList = new vector<SRecentSearch>;

      retVal = readIniFile();
      if (retVal != 0)
         return dummyFilmTypeStr;
      }

   // First check the cached results of recent searches
   string tmpFECString;
   tmpFECString.append(fecStr, 4);
   for (vector<SRecentSearch>::iterator recentSearchIter = recentSearchList->begin();
        recentSearchIter != recentSearchList->end();
        ++recentSearchIter)
      {
      if (recentSearchIter->fecStr == tmpFECString)
         {
         // Found a match!
         return recentSearchIter->filmType;
         }
      }

   // Search for matching manufacturer's code, the first two characters
   // of fecStr
   for (ManufTable::iterator manufIter = manufTable->begin();
        manufIter != manufTable->end();
        ++manufIter)
      {
      if (manufIter->manufCode[0] == fecStr[0]
          && manufIter->manufCode[1] == fecStr[1])
         {
         // Found a matching manuf code, so now search for a matching
         // film emulsion code
         for (FECTable::iterator filmTypeIter = manufIter->filmTypeTable->fecTable.begin();
              filmTypeIter != manufIter->filmTypeTable->fecTable.end();
              ++filmTypeIter)
            {
            if (filmTypeIter->filmCode[0] == fecStr[2]
                && filmTypeIter->filmCode[1] == fecStr[3])
               {
               // Found a match
               // First enter this match in cached search results
               SRecentSearch tmpSearch;
               tmpSearch.fecStr = tmpFECString;
               tmpSearch.filmType = filmTypeIter->filmType;
               recentSearchList->push_back(tmpSearch);

               // Return find to caller
               return filmTypeIter->filmType;
               }
            }
         }
      }

   // If we reached this point, then all searches failed to find
   // a matching film type code
   return dummyFilmTypeStr;
}

string CMTIKeykodeFilmTypeTable::getManufacturer(char *fecStr)
{
   int retVal;
   string dummyManufacturer = "Unknown";

   if (!iniFileRead)
      {
      manufTable = new ManufTable;
      allFilmTypeTables = new FilmTypeTableList;
      recentSearchList = new vector<SRecentSearch>;

      retVal = readIniFile();
      if (retVal != 0)
         return dummyManufacturer;
      }

   // Search for matching manufacturer's code, the first two characters
   // of fecStr
   for (ManufTable::iterator manufIter = manufTable->begin();
        manufIter != manufTable->end();
        ++manufIter)
      {
      if (manufIter->manufCode[0] == fecStr[0]
          && manufIter->manufCode[1] == fecStr[1])
         {
         // Found a matching manuf code, so now search for a matching
         // film emulsion code.  Return it.
         return manufIter->manufName;
         }
      }

   // If we reached this point, then all searches failed to find
   // a matching manufacturer
   return dummyManufacturer;
}

int CMTIKeykodeFilmTypeTable::getNumDesc()
{
   int retVal;

   if (!iniFileRead)
      {
      manufTable = new ManufTable;
      allFilmTypeTables = new FilmTypeTableList;
      recentSearchList = new vector<SRecentSearch>;

      retVal = readIniFile();
      if (retVal != 0)
         return 0;
      }

   // loop over manufacturer's code and count number of table entries
   int iCount=0;
   for (ManufTable::iterator manufIter = manufTable->begin();
        manufIter != manufTable->end();
        ++manufIter)
     {
      iCount += manufIter->filmTypeTable->fecTable.size();
     }

   return iCount;
}

string CMTIKeykodeFilmTypeTable::getDesc(int i)
/*
	Returns 2 character string for the emulsion description
*/
{
   int retVal;
   string dummyReturn = "??";

   if (!iniFileRead)
      {
      manufTable = new ManufTable;
      allFilmTypeTables = new FilmTypeTableList;
      recentSearchList = new vector<SRecentSearch>;

      retVal = readIniFile();
      if (retVal != 0)
         return dummyReturn;
      }


   // Search for matching manufacturer's code, the first two characters
   // of fecStr
   int iCount=0;
   for (ManufTable::iterator manufIter = manufTable->begin();
        manufIter != manufTable->end();
        ++manufIter)
      {
       if (iCount + (int)manufIter->filmTypeTable->fecTable.size() <= i)
        {
         iCount += manufIter->filmTypeTable->fecTable.size();
        }
       else
        {
         // Found the matching manuf code, so now search for film emulsion code
         for (FECTable::iterator filmTypeIter = manufIter->filmTypeTable->fecTable.begin();
              filmTypeIter != manufIter->filmTypeTable->fecTable.end();
              ++filmTypeIter)
            {
             if (iCount == i)
              {
               return filmTypeIter->filmType;
              }
             else
              {
               iCount++;
              }
            }
         }
      }

   // If we reached this point, then i is too large
   return dummyReturn;
}

string CMTIKeykodeFilmTypeTable::getCode(const string &strDesc)
/*
	Returns 4 digit string for the manufacturer/emulation code
*/
{
   int retVal;
   string dummyReturn = "0000";

   if (!iniFileRead)
      {
      manufTable = new ManufTable;
      allFilmTypeTables = new FilmTypeTableList;
      recentSearchList = new vector<SRecentSearch>;

      retVal = readIniFile();
      if (retVal != 0)
         return dummyReturn;
      }


   // loop over manufacturer's code, then emulsion codes and look for match
   for (ManufTable::iterator manufIter = manufTable->begin();
        manufIter != manufTable->end();
        ++manufIter)
      {
       for (FECTable::iterator filmTypeIter = manufIter->filmTypeTable->fecTable.begin();
              filmTypeIter != manufIter->filmTypeTable->fecTable.end();
              ++filmTypeIter)
        {
         if (filmTypeIter->filmType == strDesc)
          {
           return (manufIter->manufCode + filmTypeIter->filmCode);
          }
        }
      }

   // If we reached this point, then i is too large
   return dummyReturn;
}

int CMTIKeykodeFilmTypeTable::setNewCode(char *fecStr, char *cpDesc)
{
   int retVal;

   if (!iniFileRead)
      {
      manufTable = new ManufTable;
      allFilmTypeTables = new FilmTypeTableList;
      recentSearchList = new vector<SRecentSearch>;

      retVal = readIniFile();
      if (retVal != 0)
         return retVal;
      }

   // Search for matching manufacturer's code, the first two characters
   // of fecStr
   for (ManufTable::iterator manufIter = manufTable->begin();
        manufIter != manufTable->end();
        ++manufIter)
      {
      if (manufIter->manufCode[0] == fecStr[0]
          && manufIter->manufCode[1] == fecStr[1])
       {
         // found the correct manufacturer.  Write out the new description
         char caKey[3], caVal[3];
         caKey[0] = fecStr[2];
         caKey[1] = fecStr[3];
         caKey[2] = '\0';
         caVal[0] = cpDesc[0];
         caVal[1] = cpDesc[1];
         caVal[2] = '\0';

         CIniFile *iniFile = openIniFile (retVal);
         if (retVal)
          {
           return retVal;
          }

         iniFile->WriteString (filmTypeTableSectionPrefix+
		manufIter->filmTypeTable->tableName, caKey, caVal);


         // close the ini file
        if (!DeleteIniFile(iniFile))
         {
          TRACE_0(errout << "ERROR: CMTIKeykodeFilmTypeTable::readIniFile failed,"
                     << endl
                     << "       could not close Ini File");
          return CLIP_ERROR_KEYKODE_FILM_TYPE_FILE;
         }

         // clear out static variables so file will be reloaded
         Clear ();

         // return success
         return 0;
        }
         
      }

   // If we reached this point, then we could not find the correct
   // manufacturer's code
   return CLIP_ERROR_BAD_KEYKODE_MANUFACTURER;
}


//////////////////////////////////////////////////////////////////////
//  Read ini file
//////////////////////////////////////////////////////////////////////

CIniFile * CMTIKeykodeFilmTypeTable::openIniFile (int &iRet)
{
   iRet = 0;
   CIniFile *iniFile = 0;

   // Get the name of the keykode film type file
   CIniFile *ini = CPMPOpenLocalMachineIniFile();
   if (ini == 0)
      {
      TRACE_0(errout << "ERROR: CMTIKeykodeFilmTypeTable::readIniFile failed,"
                     << endl
                     << "       could not open Local Machine Ini File");
      iRet = CLIP_ERROR_KEYKODE_FILM_TYPE_FILE;
      return iniFile;
      }

   string fileName = ini->ReadFileNameCreate(mtiLocalMachineFileSection,
                                             mtiLocalMachineFileKey,
                                             defaultFileName);

   if (!DeleteIniFile(ini))
      {
      TRACE_0(errout << "ERROR: CMTIKeykodeFilmTypeTable::readIniFile failed,"
                     << endl
                     << "       could not create/write Local Machine Ini File");
      iRet = CLIP_ERROR_KEYKODE_FILM_TYPE_FILE;
      return iniFile;
      }

   if (!DoesFileExist(fileName))
      {
      string msg = "Cannot find Keykode Film Type file\n";
      msg += fileName;
      iRet = CLIP_ERROR_KEYKODE_FILM_TYPE_FILE;
      return iniFile;
      }

   // Open the keykode film type file
   iniFile = CreateIniFile(fileName);
   if (iniFile == 0)
      {
      TRACE_0(errout << "ERROR: CMTIKeykodeFilmTypeTable::readIniFile failed"
                     << endl;
              errout << "  Could not open/create Keykode Film Type file "
                     << fileName << endl);
      iRet = CLIP_ERROR_KEYKODE_FILM_TYPE_FILE;
      return iniFile;
      }

  return iniFile;
}

int CMTIKeykodeFilmTypeTable::readIniFile()
{
   iniFileRead = true;  // don't care if read fails, only try once


   int iRet;
   CIniFile *iniFile = openIniFile (iRet);
   if (iRet)
      {
      return iRet;
      }

   // Read all of the section names in the ini file
   StringList sectionNames;
   iniFile->ReadSections(&sectionNames);

   // Iterate over all of the sections in the ini file, looking for
   // sections that are actually Film Type tables
   for (unsigned i = 0; i < sectionNames.size(); i++)
      {
      // Check to see if they match the ini section
      if (sectionNames[i].find(filmTypeTableSectionPrefix, 0) == 0)
         {
         readFilmTypeTable(iniFile, sectionNames[i]);
         }
      }

   readManufTable(iniFile);

   // close the ini file
   if (!DeleteIniFile(iniFile))
      {
      TRACE_0(errout << "ERROR: CMTIKeykodeFilmTypeTable::readIniFile failed,"
                     << endl
                     << "       could not close Ini File");
      return CLIP_ERROR_KEYKODE_FILM_TYPE_FILE;
      }

   return 0;
}

void CMTIKeykodeFilmTypeTable::readFilmTypeTable(CIniFile *iniFile,
                                                 const string &sectionName)
{
   SFilmTypeTable *newFilmTypeTable = new SFilmTypeTable;

   // Set the table name, extracted from the section name
   string::size_type tableNamePosition = filmTypeTableSectionPrefix.size();
   newFilmTypeTable->tableName = sectionName.substr(tableNamePosition);
   allFilmTypeTables->push_back(newFilmTypeTable);

   // Iterate over all of the keyword=value pairs in the section and
   // interpret them <filmCode>=<filmType>, e.g. 74=KZ
   StringList keyList;
   iniFile->ReadSection(sectionName, &keyList);
   for (StringList::iterator iter = keyList.begin();
        iter != keyList.end();
        ++iter)
      {
      // If the keyword is exactly two digits, then add a film type table entry
      if (iter->size() == 2 && isdigit(iter->at(0)) && isdigit(iter->at(1)))
         {
         SFECEntry newFECEntry;
         newFECEntry.filmCode = *iter;
         newFECEntry.filmType = iniFile->ReadString(sectionName, *iter, "??");
         newFilmTypeTable->fecTable.push_back(newFECEntry);
         }
      }
}

void CMTIKeykodeFilmTypeTable::readManufTable(CIniFile *iniFile)
{
   // Iterate over all of the keyword=value pairs in the section and
   // interpret them <manufCode>=<manufTableSuffix>, e.g. 01=Agfa
   // where "Agfa" becomes part of the section name [Manufacturer_Agfa]

   StringList keyList;
   iniFile->ReadSection(manufacturerCodesSectionName, &keyList);
   for (StringList::iterator iter = keyList.begin();
        iter != keyList.end();
        ++iter)
      {
      // If the keyword is exactly two digits, then add a manuf table entry
      if (iter->size() == 2 && isdigit(iter->at(0)) && isdigit(iter->at(1)))
         {
         SManufEntry newManufEntry;
         newManufEntry.manufCode = *iter;
         newManufEntry.manufName
              = iniFile->ReadString(manufacturerCodesSectionName, *iter, "??");
         string manufSectionName = manufacturerSectionPrefix + *iter;
         string defaultManufLetter;
         defaultManufLetter = newManufEntry.manufName[0];
         string manufLetter = iniFile->ReadString(manufSectionName,
                                                  manufacturerLetterKey,
                                                  defaultManufLetter);
         newManufEntry.manufChar = manufLetter[0];
         string filmTypeTableName = iniFile->ReadString(manufSectionName,
                                                        filmTypeTableKey,
                                                        newManufEntry.manufName);
         newManufEntry.filmTypeTable = findFilmTypeTable(filmTypeTableName);
         manufTable->push_back(newManufEntry);
         }
      }
}

CMTIKeykodeFilmTypeTable::SFilmTypeTable*
  CMTIKeykodeFilmTypeTable::findFilmTypeTable(const string &filmTypeTableName)
{
   for (FilmTypeTableList::iterator iter = allFilmTypeTables->begin();
        iter != allFilmTypeTables->end();
        ++iter)
      {
      if ((*iter)->tableName == filmTypeTableName)
         return *iter;
      }

   return 0;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//
// CMTIKeykode Class Implementation
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMTIKeykode::CMTIKeykode()
 : m_calibrationOffset(0), m_filmDimension(KEYKODE_FILM_DIMENSION_INVALID),
   m_interpolateFlag(false)
{
   init();  // set initial keykode to all zeroes
}

CMTIKeykode::CMTIKeykode(const CMTIKeykode &srcKK)
 : m_calibrationOffset(srcKK.m_calibrationOffset),
   m_filmDimension(srcKK.m_filmDimension),
   m_interpolateFlag(srcKK.m_interpolateFlag)
{
   // Copy constructor - constructs a new CMTIKeykode given a reference
   // to an existing CMTIKeykode.  The keykodeString is copied from the
   // source to the new instance.

   setKeykodeStr(srcKK.m_keykodeString);
}

/*
CMTIKeykode::CMTIKeykode(const string& srcKKStr)
 : m_calibrationOffset(0), m_filmDimenstion(KEYKODE_FILM_DIMENSION_INVALID)
{
   // Construct a new CMTIKeykode with the keykode string taken from
   // the caller's srcKKStr string.

   setKeykodeStr(srcKKStr);
}
*/

CMTIKeykode::~CMTIKeykode()
{
}

//////////////////////////////////////////////////////////////////////
//  Operators
//////////////////////////////////////////////////////////////////////

CMTIKeykode& CMTIKeykode::operator=(const CMTIKeykode &srcKK)
{
   if (this == &srcKK)
      return *this;  // avoid self-copy

   m_calibrationOffset = srcKK.m_calibrationOffset;
   m_filmDimension = srcKK.m_filmDimension;
   m_interpolateFlag = srcKK.m_interpolateFlag;
   
   setKeykodeStr(srcKK.m_keykodeString);

   return *this;
}

void CMTIKeykode::init()
{
   // Set keykodeString to all zero characters
   memset(m_keykodeString, '0', MTI_KEYKODE_LENGTH);

   m_keykodeString[MTI_KEYKODE_LENGTH] = 0; // NULL-terminate string

   // Set keykodeUnknown to all ? characters
   memset(m_keykodeUnknown, '?', MTI_KEYKODE_LENGTH);

   m_keykodeUnknown[MTI_KEYKODE_LENGTH] = 0; // NULL-terminate string
}

//////////////////////////////////////////////////////////////////////
//  Accessors
//////////////////////////////////////////////////////////////////////

int CMTIKeykode::getCalibrationOffset() const
{
   return m_calibrationOffset;
}

EKeykodeFilmDimension CMTIKeykode::getFilmDimension() const
{
   return m_filmDimension;
}

string CMTIKeykode::getFilmTypeStr()
{
   // Lookup the film mfg & type based on keykode's emulsion codes
   CMTIKeykodeFilmTypeTable filmTypeTable;   // singleton class
   return filmTypeTable.lookup(m_keykodeString);
}

bool CMTIKeykode::getInterpolateFlag() const
{
   return m_interpolateFlag;
}

MTI_INT64 CMTIKeykode::getKeykodeFeet64() const
{
   int tmpPrefix, tmpFeet;
   MTI_INT64 result;


   sscanf(&(m_keykodeString[MTI_KEYKODE_PREFIXA_POSITION]), "%6d", &tmpPrefix);


   sscanf(&(m_keykodeString[MTI_KEYKODE_FEET_POSITION]), "%4d", &tmpFeet);

   result = (MTI_INT64)tmpPrefix * (MTI_INT64)10000 + (MTI_INT64)tmpFeet;

   return result;
}

string CMTIKeykode::getFormattedStr(EKeykodeFormat formatType,
                                    bool overrideInterp)
{
   MTI_INT64 llPerfCount, llPerfCountUp, llFeet;
   int iPerfs, iFrame;
   int iRefPerf;
   string tmpKKStr;
   char *keykodeLocal;
   char keykodeForDisplay[MTI_KEYKODE_LENGTH+1];
   char plusChar;
   int perfsPerFoot, perfsPerFrame;

   // initialize local variables
   keykodeLocal = m_keykodeUnknown;
   plusChar = ' ';

   // we use '00000000000000000000' for unknown keykodes
   if (strncmp (m_keykodeString, "0000000000", 10) != 0)
    {
     // Plus sign, '+', indicates a sensed keykode, space indicates
     // an interpolated value, except when overrideInterp is true
     plusChar = (!m_interpolateFlag || overrideInterp) ? '+' : ' ';

     llFeet = getKeykodeFeet64();
     iPerfs = getKeykodePerfs();
     perfsPerFoot = getPerfsPerFoot();
     perfsPerFrame = getPerfsPerFrame();

     // check for error condition
     if (perfsPerFoot >= 1 && perfsPerFrame >= 1)
      {
       // Convert feet and perfs into total perfs given the perfs per foot
       llPerfCount = llFeet * (MTI_INT64)perfsPerFoot + (MTI_INT64)iPerfs;

       // reference perforations (RefPerf) range from 0 to (PerfPerFrame-1).
       // If a KK value falls on RefPerf 2 in a frame, we have recorded the
       // value (KK minus 2 perfs) as the perf at the start of the frame.
       // However, the operator would like to see the information KK+2,
       // so we must round the KK associated with the frame to the next
       // higher value.
       //
       // this is only true for formats that converts perfs into frames

       // start by copying the default keykode into the display keykode
       memcpy(keykodeForDisplay, m_keykodeString, MTI_KEYKODE_LENGTH+1);

       if (
		formatType == MTI_KEYKODE_FORMAT_KEYKODE ||
		formatType == MTI_KEYKODE_FORMAT_KEYKODE_DOT_PERFS ||
		formatType == MTI_KEYKODE_FORMAT_FEET_PLUS_FRAMES ||
		formatType == MTI_KEYKODE_FORMAT_FEET_PLUS_FRAMES_DOT_PERFS ||
                formatType == MTI_KEYKODE_FORMAT_RP215
          )
        {
         llPerfCountUp = llPerfCount + (perfsPerFrame-1);
         llFeet = llPerfCountUp / perfsPerFoot;
         iPerfs = llPerfCountUp % perfsPerFoot;
         iFrame = iPerfs / perfsPerFrame;
         iRefPerf = iPerfs % perfsPerFrame;
         iRefPerf = perfsPerFrame - iRefPerf;

         int iPrefix, iFeet;
         iPrefix = llFeet / 10000;
         iFeet = llFeet % 10000;

         // override the contents for FOOT and PERF
         char tmpStr[16];
         sprintf(tmpStr, "%.*d%.*d", MTI_KEYKODE_PREFIXA_LENGTH +
		MTI_KEYKODE_PREFIXB_LENGTH, iPrefix,
			MTI_KEYKODE_FEET_LENGTH, iFeet);
         memcpy(&(keykodeForDisplay[MTI_KEYKODE_PREFIXA_POSITION]), tmpStr,
            MTI_KEYKODE_PREFIXA_LENGTH+MTI_KEYKODE_PREFIXB_LENGTH+
		MTI_KEYKODE_FEET_LENGTH);
         sprintf(tmpStr, "%.*d", MTI_KEYKODE_PERFS_LENGTH, (int)iPerfs);
         memcpy(&(keykodeForDisplay[MTI_KEYKODE_PERFS_POSITION]), tmpStr,
            MTI_KEYKODE_PERFS_LENGTH);
        }

       keykodeLocal = keykodeForDisplay;
      }
    }

   if (formatType == MTI_KEYKODE_FORMAT_RAW)
      {
      // Unformatted, 20 characters
      tmpKKStr = keykodeLocal;
      }
   else if (formatType == MTI_KEYKODE_FORMAT_KEYKODE_PERFS)
      {
      // Formatted Keykode
      // Example: KZ 23 1234 5677+00  (perf count at end)

      tmpKKStr = getFilmTypeStr();
      tmpKKStr += ' ';  // Space

      tmpKKStr.append(&(keykodeLocal[MTI_KEYKODE_PREFIXA_POSITION]),
                      MTI_KEYKODE_PREFIXA_LENGTH);
      tmpKKStr += ' ';

      tmpKKStr.append(&(keykodeLocal[MTI_KEYKODE_PREFIXB_POSITION]),
                      MTI_KEYKODE_PREFIXB_LENGTH);
      tmpKKStr += ' ';

      tmpKKStr.append(&(keykodeLocal[MTI_KEYKODE_FEET_POSITION]),
                      MTI_KEYKODE_FEET_LENGTH);

      tmpKKStr += plusChar;

      tmpKKStr.append(&(keykodeLocal[MTI_KEYKODE_PERFS_POSITION]),
                      MTI_KEYKODE_PERFS_LENGTH);

      }
   else if (formatType == MTI_KEYKODE_FORMAT_KEYKODE
            || formatType == MTI_KEYKODE_FORMAT_KEYKODE_DOT_PERFS)
      {
      // Formatted Keykode
      // Example: KZ 23 1234 5677+00

      tmpKKStr = getFilmTypeStr();
      tmpKKStr += ' ';  // Space

      tmpKKStr.append(&(keykodeLocal[MTI_KEYKODE_PREFIXA_POSITION]),
                      MTI_KEYKODE_PREFIXA_LENGTH);
      tmpKKStr += ' ';

      tmpKKStr.append(&(keykodeLocal[MTI_KEYKODE_PREFIXB_POSITION]),
                      MTI_KEYKODE_PREFIXB_LENGTH);
      tmpKKStr += ' ';

      tmpKKStr.append(&(keykodeLocal[MTI_KEYKODE_FEET_POSITION]),
                      MTI_KEYKODE_FEET_LENGTH);

      tmpKKStr += plusChar;

      if ((getPerfsPerFrame() > 0) && (keykodeLocal == keykodeForDisplay))
         {
         char frameStr[3];
         sprintf(frameStr, "%.2d", iFrame);
         tmpKKStr += frameStr;
         }
      else
         {
         // perfs per frame is less than 1, so we probably
         // do not know the film type
         tmpKKStr += "??";
         }

      if(formatType == MTI_KEYKODE_FORMAT_KEYKODE_DOT_PERFS)
         {
         char perfStr[5];
         if (keykodeLocal == keykodeForDisplay)
          {
           int perfNumber = iRefPerf;
           sprintf(perfStr, ".%d", perfNumber);
          }
         else
          {
           sprintf (perfStr, ".?");
          }

         tmpKKStr += perfStr;
         }
      }

   else if (formatType == MTI_KEYKODE_FORMAT_FEET_PLUS_FRAMES
            || formatType == MTI_KEYKODE_FORMAT_FEET_PLUS_FRAMES_DOT_PERFS)
      {
      // Feet + Frames
      // Example: 12345677+01

      tmpKKStr.append(&(keykodeLocal[MTI_KEYKODE_PREFIXA_POSITION]),
                      MTI_KEYKODE_PREFIXA_LENGTH);
      tmpKKStr.append(&(keykodeLocal[MTI_KEYKODE_PREFIXB_POSITION]),
                      MTI_KEYKODE_PREFIXB_LENGTH);
      tmpKKStr.append(&(keykodeLocal[MTI_KEYKODE_FEET_POSITION]),
                      MTI_KEYKODE_FEET_LENGTH);

      tmpKKStr += plusChar;
      if ((getPerfsPerFrame() > 0) && (keykodeLocal == keykodeForDisplay))
         {
         char frameStr[3];
         sprintf(frameStr, "%.2d", iFrame);
         tmpKKStr += frameStr;
         }
      else
         {
         // perfs per frame is less than 1, so we probably
         // do not know the film type
         tmpKKStr += "??";
         }

      if(formatType == MTI_KEYKODE_FORMAT_FEET_PLUS_FRAMES_DOT_PERFS)
         {
         char perfStr[5];
         if (keykodeLocal == keykodeForDisplay)
          {
           int perfNumber = iRefPerf;
           sprintf(perfStr, ".%d", perfNumber);
          }
         else
          {
           sprintf (perfStr, ".?");
          }

         tmpKKStr += perfStr;
         }
      }

   else if (formatType == MTI_KEYKODE_FORMAT_RP215)
      {
       // first 14 characters are raw keykode, next two are frame,
       // final two are "film format"
      tmpKKStr.append(keykodeLocal, 14);

      int iPerfsPerFrame = getPerfsPerFrame();

      if (iPerfsPerFrame>0 && (keykodeLocal == keykodeForDisplay))
         {
         char frameStr[16];
         sprintf(frameStr, "%.2d", iFrame);
         tmpKKStr += frameStr;
         char formatStr[16];
         sprintf (formatStr, "??");
         if (iPerfsPerFrame == 3)  // 35MM, 3 perf
          {
           if (iRefPerf == 1)
             sprintf (formatStr, "11");
           else if (iRefPerf == 2)
             sprintf (formatStr, "12");
           else if (iRefPerf == 3)
             sprintf (formatStr, "13");
          }
         else if (iPerfsPerFrame == 4)  // 35MM, 4 perf
          {
           sprintf (formatStr, "14");
          }
         else if (iPerfsPerFrame == 1)  // 16MM, 1 perf
          {
           sprintf (formatStr, "01");
          }

          tmpKKStr += formatStr;
         }
      else
         {
         // perfs per frame is less than 1, so we probably
         // do not know the film type
         tmpKKStr += "????";
         }
      }

   return tmpKKStr;
}

MTI_INT64 CMTIKeykode::getKeykodeFrames64() const
{
   MTI_INT64 llPerfs = getPerfCount64();

   int tmpPerfsPerFrame = getPerfsPerFrame();
   if (tmpPerfsPerFrame < 1)
      return 0;

   return llPerfs / (MTI_INT64)tmpPerfsPerFrame;
}

MTI_INT64 CMTIKeykode::getPerfCount64() const
{
  MTI_INT64 llFeet = getKeykodeFeet64();
  int iPerfs = getKeykodePerfs();
  int perfsPerFoot = getPerfsPerFoot();

  MTI_INT64 llPerfCount = llFeet*(MTI_INT64)perfsPerFoot + (MTI_INT64)iPerfs;

  return llPerfCount;
}

int CMTIKeykode::getKeykodePerfs() const
{
   int tmpPerfs;

   sscanf(&(m_keykodeString[MTI_KEYKODE_PERFS_POSITION]), "%2d", &tmpPerfs);

   return tmpPerfs;
}

int CMTIKeykode::getPerfsPerFoot() const
{
   // Return count of perfs per foot of film given the current film dimension
   // Returns 0 if film dimension is invalid or unknown

   return getPerfsPerFoot(m_filmDimension);
}

int CMTIKeykode::getPerfsPerFoot(EKeykodeFilmDimension newFilmDimension)
{
   // Static function that returns a count of perfs per foot of film given
   // the film dimension
   // Returns 0 if film dimension argument is invalid or unknown

   int perfCount;

   switch(newFilmDimension)
      {
      case KEYKODE_FILM_DIMENSION_35MM_4_PERF :
      case KEYKODE_FILM_DIMENSION_35MM_3_PERF :
         perfCount = 64;
         break;
      case KEYKODE_FILM_DIMENSION_16MM_1_PERF :
         // note:  there are 40 frames per linear foot of 16mm film.
         // However, the "foot" counter in a keykode increments twice
         // in each linear foot.  So, to make the calculations correct,
	 // we say there are 20 perfs per "foot"
         perfCount = 20;
         break;
      case KEYKODE_FILM_DIMENSION_35MM_FRAME :
         perfCount = 16;
         break;
      case KEYKODE_FILM_DIMENSION_INVALID :
      default :
         perfCount = 0;
         break;
      }

   return perfCount;
}

int CMTIKeykode::getPerfsPerFrame() const
{
   // Return count of perfs per frame given this instance's film dimension
   // Returns 0 if m_filmDimension has not been set, is invalid or unknown

   return getPerfsPerFrame(m_filmDimension);
}

int CMTIKeykode::getPerfsPerFrame(EKeykodeFilmDimension newFilmDimension)
{
   // Static function that returns the count of perfs per frame given the
   // film dimension
   // Returns 0 if film dimension argument is invalid or unknown

   int perfCount;

   switch(newFilmDimension)
      {
      case KEYKODE_FILM_DIMENSION_35MM_4_PERF :
         perfCount = 4;
         break;
      case KEYKODE_FILM_DIMENSION_35MM_3_PERF :
         perfCount = 3;
         break;
      case KEYKODE_FILM_DIMENSION_16MM_1_PERF :
         perfCount = 1;
         break;
      case KEYKODE_FILM_DIMENSION_35MM_FRAME :
         perfCount = 1;
         break;
      case KEYKODE_FILM_DIMENSION_INVALID :
      default :
         perfCount = 0;
         break;
      }

   return perfCount;
}

bool CMTIKeykode::isDummyValue() const
{
   return (strcmp(m_keykodeString, "00000000000000000000") == 0);
}

bool CMTIKeykode::isEqualTo(const CMTIKeykode &rhs) const
{
   return (strcmp(m_keykodeString, rhs.m_keykodeString) == 0);

// NOTE: this test ignores calibration offset, film dimension, etc.
}

//////////////////////////////////////////////////////////////////////
//  Mutators
//////////////////////////////////////////////////////////////////////

void CMTIKeykode::addFrameOffset(int frameOffset)
{
   // Add a frame offset to this keykode by adjusting the absolute
   // number of perfs
   
   MTI_INT64 deltaPerfs = (MTI_INT64)frameOffset * getPerfsPerFrame();

   setPerfCount64(getPerfCount64() + deltaPerfs);
}

void CMTIKeykode::setCalibrationOffset(int newCalibrationOffset)
{
   m_calibrationOffset = newCalibrationOffset;
}

void CMTIKeykode::setFilmDimension(EKeykodeFilmDimension newFilmDimension)
{
   m_filmDimension = newFilmDimension;
}

void CMTIKeykode::setInterpolateFlag(bool newInterpolateFlag)
{
   m_interpolateFlag = newInterpolateFlag;
}

void CMTIKeykode::setKeykodeFeet64(MTI_INT64 newFeet)
{
   // Filter bogus values
   if (newFeet < 0)
      newFeet = 0;
   else if (newFeet > INT64_LITERAL(9999999999))
      newFeet %= INT64_LITERAL(10000000000);

   int iPrefix, iFeet;
   iPrefix = newFeet / 10000;
   iFeet = newFeet % 10000;

   char tmpStr[11];
   sprintf (tmpStr, "%.*d%.*d", MTI_KEYKODE_PREFIXA_LENGTH+
		MTI_KEYKODE_PREFIXB_LENGTH, iPrefix,
		MTI_KEYKODE_FEET_LENGTH, iFeet);
   memcpy(&(m_keykodeString[MTI_KEYKODE_PREFIXA_POSITION]), tmpStr, 10);
}

void CMTIKeykode::setKeykodePerfs(int newPerfs)
{
   // Filter bogus values
   if (newPerfs < 0)
      newPerfs = 0;
   else if (newPerfs > 99)
      newPerfs %= 100;

   char tmpStr[MTI_KEYKODE_PERFS_LENGTH+1];
   sprintf(tmpStr, "%.*d", MTI_KEYKODE_PERFS_LENGTH, newPerfs);
   memcpy(&(m_keykodeString[MTI_KEYKODE_PERFS_POSITION]), tmpStr,
          MTI_KEYKODE_PERFS_LENGTH);
}

void CMTIKeykode::setPerfCount64(MTI_INT64 newPerfCount)
{
  MTI_INT64 perfsPerFoot = getPerfsPerFoot();
  MTI_INT64 llFeet;
  int iPerfs;

  if (perfsPerFoot == 0)
   {
    llFeet = 0;
    iPerfs = 0;
   }
  else
   {
    llFeet = newPerfCount / perfsPerFoot;
    iPerfs = newPerfCount % perfsPerFoot;
   }

  setKeykodeFeet64 (llFeet);
  setKeykodePerfs (iPerfs);
}

void CMTIKeykode::setKeykodeStr(const string &newKKStr)
{
   init();  // set initial keykode to all zeroes

   // Copy up to MTI_KEYKODE_LENGTH characters from source string.
   // If there are less than MTI_KEYKODE_LENGTH in the source string, then
   // the remaining locations are zero characters.
   newKKStr.copy(m_keykodeString, MTI_KEYKODE_LENGTH, 0);
}

void CMTIKeykode::setKeykodeStr(const char *newKKStr)
{
   init();  // set initial keykode to all zeroes

   if (newKKStr == 0)
      return;

   // Copy up to MTI_KEYKODE_LENGTH characters from source C-string.
   // If there are less than MTI_KEYKODE_LENGTH in the source C-string, then
   // the remaining locations are zero characters.
   size_t charsToCopy = std::min(strlen(newKKStr), (size_t)MTI_KEYKODE_LENGTH);
   strncpy(m_keykodeString, newKKStr, charsToCopy);
}

void CMTIKeykode::setRawKeykode(const string &newKKStr, int interpolationOffset)
{
   setKeykodeStr(newKKStr);

   // Adjust the feet and perfs in the keykode for the calibration offset
   adjustForCalibrationOffset(interpolationOffset);

   // If the caller's interpolationOffset is greater than the number
   // of perfs per frame, then we assume that the feet & perfs has
   // been interpolated rather than sensed directly
   m_interpolateFlag = (interpolationOffset >= getPerfsPerFrame());

}

void CMTIKeykode::setRawKeykode(const char *newKKStr, int interpolationOffset)
{
   // Note: 1) this function does not handle rollovers past 9999 feet or
   //          negative value (which could happen with negative calibration
   //          or interpolation values).
   //       2) Not sure how we are supposed to handle 65 mm film with
   //          120 perfs per foot.  This can result in a perf count value
   //          that has more than 2 digits.

   setKeykodeStr(newKKStr);

   // Adjust the feet and perfs in the keykode for the calibration offset
   adjustForCalibrationOffset(interpolationOffset);

   // If the caller's interpolationOffset is greater than the number
   // of perfs per frame, then we assume that the feet & perfs has
   // been interpolated rather than sensed directly
   m_interpolateFlag = (interpolationOffset >= getPerfsPerFrame());

}

void CMTIKeykode::setToDummyValue()
{
   init();
}

void CMTIKeykode::adjustForCalibrationOffset(int interpolationOffset)
{
   // Adjust the keykode's feet and perf value for the distance
   // between the keykode reader and the frame in the scanner head.
   // The keykode is read at the keykode reader, but we really want
   // to know what the keykode is of the frame at the scanner head.
   //
   // Note: this function does not take into account possible breaks in 
   //       the keykode sequence because of film splices or whatever

   MTI_INT64 llFeet = getKeykodeFeet64();
   int iPerfs = getKeykodePerfs();
   int perfsPerFoot = getPerfsPerFoot();

   if (perfsPerFoot < 1)
      return;     // Cannot do anything, perhaps an error

   // Convert feet and perfs into total perfs given the perfs per foot
   MTI_INT64 totalPerfs = llFeet * perfsPerFoot + (MTI_INT64)iPerfs;

   // adjust total perfs for the calibration offset
   totalPerfs += m_calibrationOffset;

   // adjust total perfs for the interpolation offset
   totalPerfs += interpolationOffset;

   // Convert back into feet and perfs
   llFeet = totalPerfs / perfsPerFoot;
   iPerfs = totalPerfs % perfsPerFoot;

   setKeykodeFeet64(llFeet);
   setKeykodePerfs(iPerfs);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//
// CKeykodeFrame Class Implementation
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Virtual Copy
//////////////////////////////////////////////////////////////////////

int CKeykodeFrame::virtualCopy(CKeykodeFrame &srcFrame)
{
   // Copy the Keykode from source to destination
   setKeykode(srcFrame.getKeykode());

   setNeedsUpdate(true);   // This frame has changed, so it may need
                           // to be written back to the frame list file
                           
   return 0;
}

//////////////////////////////////////////////////////////////////////
//  Mutators
//////////////////////////////////////////////////////////////////////

void CKeykodeFrame::setCalibrationOffset(int newCalibrationOffset)
{
   keykode.setCalibrationOffset(newCalibrationOffset);
}

void CKeykodeFrame::setFilmDimension(EKeykodeFilmDimension newFilmDimension)
{
   keykode.setFilmDimension(newFilmDimension);

   setNeedsUpdate(true);   // This frame has changed, so it may need
                           // to be written back to the frame list file
}

void CKeykodeFrame::setInterpolateFlag(bool newInterpolateFlag)
{
   keykode.setInterpolateFlag(newInterpolateFlag);

   setNeedsUpdate(true);   // This frame has changed, so it may need
                           // to be written back to the frame list file
}

void CKeykodeFrame::setKeykode(const CMTIKeykode &newKeykode)
{
   keykode = newKeykode;

   setNeedsUpdate(true);   // This frame has changed, so it may need
                           // to be written back to the frame list file
}

void CKeykodeFrame::setKeykode(const string &newKeykodeStr)
{
   keykode.setKeykodeStr(newKeykodeStr);

   setNeedsUpdate(true);   // This frame has changed, so it may need
                           // to be written back to the frame list file
}

void CKeykodeFrame::setKeykode(const char *newKeykodeStr)
{
   // NOTE: This function should not be used anymore
   keykode.setKeykodeStr(newKeykodeStr);
   
   setNeedsUpdate(true);   // This frame has changed, so it may need
                           // to be written back to the frame list file
}

   CKeykodeFrame::CKeykodeFrame() {};
   CKeykodeFrame::~CKeykodeFrame() {};

void CKeykodeFrame::setRawKeykode(const string &newKKStr,
                                  int interpolationOffset)
{
   keykode.setRawKeykode(newKKStr, interpolationOffset);

   setNeedsUpdate(true);   // This frame has changed, so it may need
                           // to be written back to the frame list file

}

void CKeykodeFrame::setRawKeykode(const char *newKKStr,
                                  int interpolationOffset)
{
   keykode.setRawKeykode(newKKStr, interpolationOffset);

   setNeedsUpdate(true);   // This frame has changed, so it may need
                           // to be written back to the frame list file

}

void CKeykodeFrame::initWithDummyValue()
{
//   setKeykode("Not Recorded          ");
   setKeykode("00000000000000000000");
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//
// CTimecodeFrame Class Implementation
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTimecodeFrame::CTimecodeFrame()
 : m_timecode(0)
{
}

CTimecodeFrame::~CTimecodeFrame()
{
}

//////////////////////////////////////////////////////////////////////
//  Virtual Copy
//////////////////////////////////////////////////////////////////////

int CTimecodeFrame::virtualCopy(CTimecodeFrame &srcFrame)
{
   // Copy the timecode from source to destination
   setTimecode(srcFrame.getTimecode());

   setNeedsUpdate(true);   // This frame has changed, so it may need
                           // to be written back to the frame list file

   return 0;
}

//////////////////////////////////////////////////////////////////////
//  Mutators
//////////////////////////////////////////////////////////////////////

void CTimecodeFrame::setTimecode(const CTimecode &newTimecode)
{
   m_timecode = newTimecode;

   setNeedsUpdate(true);   // This frame has changed, so it may need
                           // to be written back to the frame list file
}

void CTimecodeFrame::initWithDummyValue()
{
   CTimecode tc(0);

   setTimecode(tc);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//
// CSMPTETimecodeFrame Class Implementation
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSMPTETimecodeFrame::CSMPTETimecodeFrame()
 : m_partialFrame(0.0)
{
}

CSMPTETimecodeFrame::~CSMPTETimecodeFrame()
{
}

//////////////////////////////////////////////////////////////////////
//  Virtual Copy
//////////////////////////////////////////////////////////////////////

int CSMPTETimecodeFrame::virtualCopy(CSMPTETimecodeFrame &srcFrame)
{
   // Copy the timecode from source to destination
   setSMPTETimecode(srcFrame.m_smpteTimecode);

   setPartialFrame(srcFrame.m_partialFrame);
   
   setNeedsUpdate(true);   // This frame has changed, so it may need
                           // to be written back to the frame list file

   return 0;
}

//////////////////////////////////////////////////////////////////////
//  Accessors
//////////////////////////////////////////////////////////////////////

float CSMPTETimecodeFrame::getPartialFrame() const
{
   return m_partialFrame;
}

CTimecode CSMPTETimecodeFrame::getTimecode(int frameRate) const
{
   return m_smpteTimecode.getTimecode(frameRate);
}

CSMPTETimecode CSMPTETimecodeFrame::getSMPTETimecode() const
{
   return m_smpteTimecode;
}

//////////////////////////////////////////////////////////////////////
//  Mutators
//////////////////////////////////////////////////////////////////////

void CSMPTETimecodeFrame::setPartialFrame(float newPartialFrame)
{
   // should be -1.0 <= newPartialFrame < 1.0
   // but no validation is performed
   
   m_partialFrame = newPartialFrame;

   setNeedsUpdate(true);   // This frame has changed, so it may need
                           // to be written back to the frame list file
}

void CSMPTETimecodeFrame::setRawTimecode(MTI_UINT32 newTimecode,
                                         MTI_UINT32 newUserData)
{
   m_smpteTimecode.setRawTimecode(newTimecode);
   m_smpteTimecode.setRawUserData(newUserData);
   
   setNeedsUpdate(true);   // This frame has changed, so it may need
                           // to be written back to the frame list file
}

void CSMPTETimecodeFrame::setSMPTETimecode(const CSMPTETimecode &newTimecode)
{
   m_smpteTimecode = newTimecode;
   
   setNeedsUpdate(true);   // This frame has changed, so it may need
                           // to be written back to the frame list file
}

void CSMPTETimecodeFrame::initWithDummyValue()
{
   // set timecode and user bits to 0xFFFFFFFF.  This is a bogus timecode.

   setRawTimecode(0xFFFFFFFF, 0xFFFFFFFF);
   setPartialFrame(0.0);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//
// CEventFrame Class Implementation
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Construction/Destruction
//////////////////////////////////////////////////////////////////////

CEventFrame::CEventFrame()
 : m_eventFlags(0)
{
}

CEventFrame::~CEventFrame()
{
}

//////////////////////////////////////////////////////////////////////
//  Accessors
//////////////////////////////////////////////////////////////////////

MTI_UINT64 CEventFrame::getEventFlags() const
{
   return m_eventFlags;
}

bool CEventFrame::getEventState(MTI_UINT64 eventMask) const
{
   return (m_eventFlags & eventMask);
}

//////////////////////////////////////////////////////////////////////
//  Mutators
//////////////////////////////////////////////////////////////////////

void CEventFrame::setEventFlags(MTI_UINT64 newEventFlags)
{

   m_eventFlags = newEventFlags;
  
   setNeedsUpdate(true);   // This frame has changed, so it may need

                           // to be written back to the frame list file
}

void CEventFrame::setEventState(MTI_UINT64 eventMask, bool newValue)
{
   if (newValue)
      m_eventFlags |= eventMask;    // Set bits in argument
   else
      m_eventFlags &= ~eventMask;   // Clear bits in argument

   setNeedsUpdate(true);   // This frame has changed, so it may need
                           // to be written back to the frame list file
}

int CEventFrame::virtualCopy(CEventFrame &srcFrame)
{
   // Copy flag bits
   m_eventFlags = srcFrame.m_eventFlags;

   setNeedsUpdate(true);   // This frame has changed, so it may need
                           // to be written back to the frame list file

   return 0;
}

void CEventFrame::initWithDummyValue()
{
   // dummy value = 0
   setEventFlags(0);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//
// CAudioProxyFrame Class Implementation
//
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//  Construction/Destruction
//////////////////////////////////////////////////////////////////////
CAudioProxyFrame::CAudioProxyFrame()
{
   initWithDummyValue();
}

CAudioProxyFrame::~CAudioProxyFrame()
{
}

//////////////////////////////////////////////////////////////////////
//  Accessors
//////////////////////////////////////////////////////////////////////
int CAudioProxyFrame::getMinMaxAudio(int sample, int channel,
                                     MTI_INT16 *min, MTI_INT16 *max) const
{
/*
   if ((channel < 0)||(channel >= MAX_AUDIO_PROXY_CHANNELS))
      return(CLIP_ERROR_INVALID_AUDIO_PROXY_CHANNEL_INDEX);
   if ((sample < 0)||(sample >= MAX_AUDIO_PROXY_SAMPLES))
      return(CLIP_ERROR_INVALID_AUDIO_PROXY_SAMPLE_INDEX);
*/
   *min = audioProxySample[sample][channel].minAudio;
   *max = audioProxySample[sample][channel].maxAudio;

   return(0);
}

//////////////////////////////////////////////////////////////////////
//  Mutators
//////////////////////////////////////////////////////////////////////
void CAudioProxyFrame::initWithDummyValue()
{
   for (int isample=0;isample<MAX_AUDIO_PROXY_SAMPLES;++isample) {
      for (int ichannel=0;ichannel<MAX_AUDIO_PROXY_CHANNELS;++ichannel) {
         audioProxySample[isample][ichannel].minAudio = 0;
         audioProxySample[isample][ichannel].maxAudio = 0;
      }
   }
}

int CAudioProxyFrame::setMinMaxAudio(int sample, int channel,
                                     MTI_INT16 min, MTI_INT16 max)
{
/*
   if ((channel < 0)||(channel >= MAX_AUDIO_PROXY_CHANNELS))
      return(CLIP_ERROR_INVALID_AUDIO_PROXY_CHANNEL_INDEX);
   if ((sample < 0)||(sample >= MAX_AUDIO_PROXY_SAMPLES))
      return(CLIP_ERROR_INVALID_AUDIO_PROXY_SAMPLE_INDEX);
*/
   audioProxySample[sample][channel].minAudio = min;
   audioProxySample[sample][channel].maxAudio = max;

   return(0);
}

int CAudioProxyFrame::addSampleAudio(int sample, int channel,
                                     MTI_INT16 sampleValue)
{
/*
   if ((channel < 0)||(channel >= MAX_AUDIO_PROXY_CHANNELS))
      return(CLIP_ERROR_INVALID_AUDIO_PROXY_CHANNEL_INDEX);
   if ((sample < 0)||(sample >= MAX_AUDIO_PROXY_SAMPLES))
      return(CLIP_ERROR_INVALID_AUDIO_PROXY_SAMPLE_INDEX);
*/
   if (sampleValue < audioProxySample[sample][channel].minAudio)
      audioProxySample[sample][channel].minAudio = sampleValue;
   else if (sampleValue > audioProxySample[sample][channel].maxAudio)
      audioProxySample[sample][channel].maxAudio = sampleValue;

   return(0);
}

int CAudioProxyFrame::virtualCopy(CAudioProxyFrame &srcFrame)
{
   MTI_INT32 srcMin, srcMax;
   for (int isample=0; isample < MAX_AUDIO_PROXY_SAMPLES; ++isample) {
      for (int ichannel=0; ichannel < MAX_AUDIO_PROXY_CHANNELS; ++ichannel) {
         audioProxySample[isample][ichannel].minAudio =
            srcFrame.audioProxySample[isample][ichannel].minAudio;
         audioProxySample[isample][ichannel].maxAudio =
            srcFrame.audioProxySample[isample][ichannel].maxAudio  ;
      }
   }

   return 0;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//
// CTimeTrack Class Implementation
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTimeTrack::CTimeTrack(ETimeType newTimeType, EClipVersion newClipVer,
                       ClipSharedPtr &newParentClip, int newTrackNumber)
 : clipVer(newClipVer),
   m_trackNumber(newTrackNumber), m_frameListAvailable(false),
   m_timeType(newTimeType), m_parentClip(newParentClip),
   m_framingType(FRAMING_TYPE_VIDEO)
{
}

CTimeTrack::~CTimeTrack()
{
   deleteFrameList();
}

void CTimeTrack::deleteFrameList()
{
   TimeFrameList::iterator iter;
   TimeFrameList::iterator end = m_frameList.end();
   for (iter = m_frameList.begin(); iter != end; ++iter)
      delete *iter;

   m_frameList.clear();

   m_frameListAvailable = false;
}

int CTimeTrack::truncateFrameList(int startFrame)
{
   // Remove the frames in the frame list from startFrame to the
   // end of the frame list

   if (!isFrameIndexValid(startFrame))
      return CLIP_ERROR_INVALID_FRAME_NUMBER;

   TimeFrameList::iterator beginFrame = m_frameList.begin() + startFrame;
   TimeFrameList::iterator endFrame = m_frameList.end();

   // Iterate through frame list, deleting each frame in list.  This
   // will also delete any of the fields owned by the frame
   for (TimeFrameList::iterator frame = beginFrame; frame < endFrame; ++frame)
      {
      delete *frame;
      }

   // Remove the frame pointers from the frame list in the range
   // of [beginFrame, endFrame)
   m_frameList.erase(beginFrame, endFrame);

   return 0;

} // truncateFrameList

//////////////////////////////////////////////////////////////////////
//  Accessors
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    getBaseFrame
//
// Description: Get a pointer to a particular frame in a Time Track given the
//              frame number.
//
// Arguments:   int frameNumber  Number of the frame to get.  First frame is 0
//
// Returns:     Pointer to a CTimeFrame subtypeobject for the specified frame
//              number.  If the frame number is not valid, returns a NULL pointer
//
//------------------------------------------------------------------------
CTimeFrame* CTimeTrack::getBaseFrame(int frameNumber)
{
   if (getClipVer() == CLIP_VERSION_5L)
      return 0;   // Clip 5 clip does not have individual frame instances

   if (!isFrameIndexValid(frameNumber))
      {
      TRACE_0(errout << "ERROR: CTimeTrack::getBaseFrame: Clip 5 does not support individual time frame instances");
      return 0;
      }

   if (!isFrameListAvailable())
      {
      // Read the framelist from the Frame List file
      int retVal = readFrameListFile();
      if (retVal != 0)
         {
         m_frameListAvailable = false;
         TRACE_0(errout << "ERROR: In CTimeTrack::getBaseFrame, call to " << endl
                        << "       CTimeTrack::readFrameListFile failed with return value "
                        << retVal;
                );
         return 0;
         }
      }

   return (m_frameList)[frameNumber];
}

CTimecodeFrame* CTimeTrack::getTimecodeFrame(int frameNumber)
{
   if (getTimeType() != TIME_TYPE_TIMECODE)
      return 0;

   CTimeFrame *frame = getBaseFrame(frameNumber);
   if (frame == 0)
      return 0;

   if (frame->getTimeType() != TIME_TYPE_TIMECODE)
      return 0;

   return static_cast<CTimecodeFrame*>(frame);
}

int CTimeTrack::getKeykode(int frameNumber, CMTIKeykode &keykode)
{
   int retVal;

   if (getClipVer() == CLIP_VERSION_3)
      {
      CKeykodeFrame *frame = getKeykodeFrame(frameNumber);
      if (frame == 0)
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

      keykode = frame->getKeykode();
      }
   else if (getClipVer() == CLIP_VERSION_5L)
      {
      C5KeycodeTrackHandle trackHandle = GetKeycodeTrackHandle();
      retVal = trackHandle.GetKeycode(frameNumber, keykode);
      if (retVal != 0)
         return retVal;
      }
   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   return 0;
}

CKeykodeFrame* CTimeTrack::getKeykodeFrame(int frameNumber)
{
   if (clipVer == CLIP_VERSION_5L)
      {
      // Clip 5 time tracks do not have individual frame instances
      TRACE_0(errout << "CTimeTrack::getKeykodeFrame not supported by CLIP 5");
      return 0;
      }

   if (getTimeType() != TIME_TYPE_KEYKODE)
      return 0;

   CTimeFrame *frame = getBaseFrame(frameNumber);
   if (frame == 0)
      return 0;

    if (frame->getTimeType() != TIME_TYPE_KEYKODE)
      return 0;

   return static_cast<CKeykodeFrame*>(frame);
}

int CTimeTrack::getSMPTETimecode(int frameNumber, CSMPTETimecode &timecode,
                                 float &partialFrame)
{
   int retVal;

   if (getTimeType() != TIME_TYPE_SMPTE_TIMECODE)
      return CLIP_ERROR_INVALID_TIME_TRACK_TYPE;

   if (getClipVer() == CLIP_VERSION_3)
      {
      CSMPTETimecodeFrame *frame = getSMPTETimecodeFrame(frameNumber);
      if (frame == 0)
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

      timecode = frame->getSMPTETimecode();
      partialFrame = frame->getPartialFrame();
      }
   else if (getClipVer() == CLIP_VERSION_5L)
      {
      C5SMPTETimecodeTrackHandle trackHandle = GetSMPTETimecodeTrackHandle();
      retVal = trackHandle.GetTimecode(frameNumber, timecode, partialFrame);
      if (retVal != 0)
         return retVal;
      }
   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   return 0;
}

CSMPTETimecodeFrame* CTimeTrack::getSMPTETimecodeFrame(int frameNumber)
{
   if (clipVer == CLIP_VERSION_5L)
      {
      TRACE_0(errout << "CTimeTrack::getSMPTETimecodeFrame not supported by CLIP 5");
      return 0;
      }

   if (getTimeType() != TIME_TYPE_SMPTE_TIMECODE)
      return 0;

   CTimeFrame *frame = getBaseFrame(frameNumber);
   if (frame == 0)
      return 0;

   if (frame->getTimeType() != TIME_TYPE_SMPTE_TIMECODE)
      return 0;

   return static_cast<CSMPTETimecodeFrame*>(frame);
}

EFrameRate CTimeTrack::getDefaultTimecodeFrameRateEnum()
{
   int retVal;

   if (getTimeType() != TIME_TYPE_SMPTE_TIMECODE)
      return IF_FRAME_RATE_INVALID;

   if (getClipVer() == CLIP_VERSION_3)
      {
      return IF_FRAME_RATE_INVALID;
      }
   else if (getClipVer() == CLIP_VERSION_5L)
      {
      C5SMPTETimecodeTrackHandle trackHandle = GetSMPTETimecodeTrackHandle();
      return trackHandle.GetTimecodeFrameRateEnum();
      }
   else
      return IF_FRAME_RATE_INVALID;
}

EFrameRate CTimeTrack::getTimecodeFrameRateEnum(int frameNumber)
{
   int retVal;

   if (getTimeType() != TIME_TYPE_SMPTE_TIMECODE)
      return IF_FRAME_RATE_INVALID;

   if (getClipVer() == CLIP_VERSION_3)
      {
      return IF_FRAME_RATE_INVALID;
      }
   else if (getClipVer() == CLIP_VERSION_5L)
      {
      C5SMPTETimecodeTrackHandle trackHandle = GetSMPTETimecodeTrackHandle();
      return trackHandle.GetTimecodeFrameRateEnum(frameNumber);
      }
   else
      return IF_FRAME_RATE_INVALID;
}


CEventFrame* CTimeTrack::getEventFrame(int frameNumber)
{
   if (clipVer == CLIP_VERSION_5L)
      {
      TRACE_0(errout << "CTimeTrack::getEventFrame not supported by CLIP 5");
      return 0;
      }

   if (getTimeType() != TIME_TYPE_EVENT)
      return 0;

   CTimeFrame *frame = getBaseFrame(frameNumber);
   if (frame == 0)
      return 0;

    if (frame->getTimeType() != TIME_TYPE_EVENT)
      return 0;


   return static_cast<CEventFrame*>(frame);
}

CAudioProxyFrame* CTimeTrack::getAudioProxyFrame(int frameNumber)
{
   if (clipVer == CLIP_VERSION_5L)
      {
      TRACE_0(errout << "CTimeTrack::getAudioProxyFrame not supported by CLIP 5");
      return 0;
      }

   if (getTimeType() != TIME_TYPE_AUDIO_PROXY)
      return 0;

   CTimeFrame *frame = getBaseFrame(frameNumber);
   if (frame == 0)
      return 0;

    if (frame->getTimeType() != TIME_TYPE_AUDIO_PROXY)
      return 0;


   return static_cast<CAudioProxyFrame*>(frame);
}

string CTimeTrack::getFrameListFileName() const
{
   return makeFrameListFileName();
}

string CTimeTrack::getFrameListFileNameWithExt() const
{
   return makeFrameListFileName() + "." + frameListFileExtension;
}

int CTimeTrack::getTotalFrameCount()
{
   if (clipVer == CLIP_VERSION_3)
      {
      if (!isFrameListAvailable())
         {
         // Read the framelist from the Frame List file
         int retVal = readFrameListFile();
       if (retVal != 0)
            {
            m_frameListAvailable = false;
            TRACE_0(errout << "ERROR: In CTimeTrack::getTotalFrameCount(), call to "
                           << endl
                           << "       CTimeTrack::readFrameListFile failed with return value "
                           << retVal;);
            return 0;
            }
         }
      return m_frameList.size();
      }
   else if (clipVer == CLIP_VERSION_5L)
      {
      return clip5TrackHandle.GetTotalFrameCount();
      }
   else
      return 0;
}

C5SMPTETimecodeTrackHandle CTimeTrack::GetSMPTETimecodeTrackHandle()
{
   if (clip5TrackHandle.GetTrackType() != CLIP5_TRACK_TYPE_SMPTE_TIMECODE)
      {
      throw(std::runtime_error("Bad clip 5 range cast"));
      }

   C5SMPTETimecodeTrackHandle newTrackHandle = clip5TrackHandle;
   return newTrackHandle;
}

C5KeycodeTrackHandle CTimeTrack::GetKeycodeTrackHandle()
{
   if (clip5TrackHandle.GetTrackType() != CLIP5_TRACK_TYPE_KEYCODE)
      {
      throw(std::runtime_error("Bad clip 5 range cast"));
      }

   C5KeycodeTrackHandle newTrackHandle = clip5TrackHandle;
   return newTrackHandle;
}

int CTimeTrack::getFramingType()
{
    return m_framingType;
}

bool CTimeTrack::isFrameListAvailable() const
{
   return m_frameListAvailable;
}

bool CTimeTrack::isFrameIndexValid(int frameIndex)
{
   // Returns true if frame index is within range of total range of frames

   if (frameIndex < 0 || frameIndex >= getTotalFrameCount())
      return false;

   return true;
}

// ----------------------------------------------------------------------------

void CTimeTrack::setFramingType(int type)
{
    m_framingType = type;
}


void CTimeTrack::setClip5TrackHandle(const C5SMPTETimecodeTrackHandle &newHandle)
{
   clip5TrackHandle = newHandle;
}

void CTimeTrack::setClip5TrackHandle(const C5KeycodeTrackHandle &newHandle)
{
   clip5TrackHandle = newHandle;
}

int CTimeTrack::setDummyKeykode(int frameNumber)
{
   int retVal;

   if (getClipVer() == CLIP_VERSION_3)
      {
      CKeykodeFrame *frame = getKeykodeFrame(frameNumber);
      if (frame == 0)
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

      frame->initWithDummyValue();
      }
   else if (getClipVer() == CLIP_VERSION_5L)
      {
      C5KeycodeTrackHandle trackHandle = GetKeycodeTrackHandle();
      retVal = trackHandle.SetToDummyValue(frameNumber, 1);
      if (retVal != 0)
         return retVal;
      }
   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   return 0;
}

int CTimeTrack::setDummyKeykode(int frameNumber, int count)
{
   int retVal;

   if (getClipVer() == CLIP_VERSION_3)
      {
      for (int i = frameNumber; i < frameNumber + count; ++i)
         {
         CKeykodeFrame *frame = getKeykodeFrame(i);
         if (frame == 0)
            return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

         frame->initWithDummyValue();
         }
      }
   else if (getClipVer() == CLIP_VERSION_5L)
      {
      C5KeycodeTrackHandle trackHandle = GetKeycodeTrackHandle();
      retVal = trackHandle.SetToDummyValue(frameNumber, count);
      if (retVal != 0)
         return retVal;
      }
  else
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   return 0;
}

int CTimeTrack::setKeykode(int frameNumber, const CMTIKeykode &newKeykode)
{
   int retVal;

   if (getClipVer() == CLIP_VERSION_3)
      {
      CKeykodeFrame *frame = getKeykodeFrame(frameNumber);
      if (frame == 0)
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

      frame->setKeykode(newKeykode);
      }
   else if (getClipVer() == CLIP_VERSION_5L)
      {
      C5KeycodeTrackHandle trackHandle = GetKeycodeTrackHandle();
      retVal = trackHandle.SetKeycode(frameNumber, newKeykode);
      if (retVal != 0)
         return retVal;
      }
   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   return 0;
}

int CTimeTrack::setKeykode(int frameNumber, int count,
                           const CMTIKeykode &newKeykode)
{
   int retVal;

   if (getClipVer() == CLIP_VERSION_5L)
      {
      C5KeycodeTrackHandle trackHandle = GetKeycodeTrackHandle();
      retVal = trackHandle.SetKeycodeX(frameNumber, count, newKeykode);
      if (retVal != 0)
         return retVal;
      }
   else if (getClipVer() == CLIP_VERSION_3)
      {
      return CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_3;
      }
   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   return 0;
}

int CTimeTrack::setDummySMPTETimecode(int frameNumber)
{
   int retVal;

   if (getTimeType() != TIME_TYPE_SMPTE_TIMECODE)
      return CLIP_ERROR_INVALID_TIME_TRACK_TYPE;

   if (getClipVer() == CLIP_VERSION_3)
      {
      CSMPTETimecodeFrame *frame = getSMPTETimecodeFrame(frameNumber);
      if (frame == 0)
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

      frame->initWithDummyValue();
      }
   else if (getClipVer() == CLIP_VERSION_5L)
      {
      C5SMPTETimecodeTrackHandle trackHandle = GetSMPTETimecodeTrackHandle();
      retVal = trackHandle.SetToDummyValue(frameNumber, 1);
      if (retVal != 0)
         return retVal;
      }
   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   return 0;
}

int CTimeTrack::setDummySMPTETimecode(int frameNumber, int count)
{
   int retVal;

   if (getTimeType() != TIME_TYPE_SMPTE_TIMECODE)
      return CLIP_ERROR_INVALID_TIME_TRACK_TYPE;

   if (getClipVer() == CLIP_VERSION_3)
      {
      for (int i = frameNumber; i < frameNumber + count; ++i)
         {
         CSMPTETimecodeFrame *frame = getSMPTETimecodeFrame(i);
         if (frame == 0)
            return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

         frame->initWithDummyValue();
         }
      }
   else if (getClipVer() == CLIP_VERSION_5L)
      {
      C5SMPTETimecodeTrackHandle trackHandle = GetSMPTETimecodeTrackHandle();
      retVal = trackHandle.SetToDummyValue(frameNumber, count);
      if (retVal != 0)
         return retVal;
      }
   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   return 0;
}

int CTimeTrack::setSMPTETimecode(int frameNumber,
                                 const CSMPTETimecode &newTimecode,
                                 float newPartialFrame)
{
   int retVal;

   if (getTimeType() != TIME_TYPE_SMPTE_TIMECODE)
      return CLIP_ERROR_INVALID_TIME_TRACK_TYPE;

   if (getClipVer() == CLIP_VERSION_3)
      {
      CSMPTETimecodeFrame *frame = getSMPTETimecodeFrame(frameNumber);
      if (frame == 0)
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

      frame->setSMPTETimecode(newTimecode);
      frame->setPartialFrame(newPartialFrame);
      }
   else if (getClipVer() == CLIP_VERSION_5L)
      {
      C5SMPTETimecodeTrackHandle trackHandle = GetSMPTETimecodeTrackHandle();
      retVal = trackHandle.SetTimecode(frameNumber, newTimecode,
                                       newPartialFrame);
      if (retVal != 0)
         return retVal;
      }
   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   return 0;
}

int CTimeTrack::setSMPTETimecode(int frameNumber, int count,
                                 const CSMPTETimecode &newTimecode,
                                 float newPartialFrame)
{
   int retVal;

   if (getTimeType() != TIME_TYPE_SMPTE_TIMECODE)
      return CLIP_ERROR_INVALID_TIME_TRACK_TYPE;

   if (getClipVer() == CLIP_VERSION_5L)
      {
      C5SMPTETimecodeTrackHandle trackHandle = GetSMPTETimecodeTrackHandle();
      retVal = trackHandle.SetTimecodeX(frameNumber, count, newTimecode,
                                       newPartialFrame);
      if (retVal != 0)
         return retVal;
      }
   else if (getClipVer() == CLIP_VERSION_3)
      {
      return CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_3;
      }
   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   return 0;
}

int CTimeTrack::setTimecodeFrameRate(EFrameRate newFrameRate)
{
   int retVal;

   if (getTimeType() != TIME_TYPE_SMPTE_TIMECODE)
      return CLIP_ERROR_INVALID_TIME_TRACK_TYPE;

   if (getClipVer() == CLIP_VERSION_5L)
      {
      C5SMPTETimecodeTrackHandle trackHandle = GetSMPTETimecodeTrackHandle();
      retVal = trackHandle.SetTimecodeFrameRate(newFrameRate);
      if (retVal != 0)
         return retVal;
      }
   else if (getClipVer() == CLIP_VERSION_3)
      {
      return CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_3;
      }
   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   return 0;
}

// ---------------------------------------------------------------------------

void CTimeTrack::clearAllNeedsUpdate()
{
   TimeFrameList::iterator iter;
   TimeFrameList::iterator end = m_frameList.end();
   
   for (iter = m_frameList.begin(); iter != end; ++iter)
      (*iter)->setNeedsUpdate(false);
}

string CTimeTrack::makeFrameListFileName() const
{
   MTIostringstream ostr;

   // Compose Frame List File Name

   // Clip's Bin Path is directory
   ostr << AddDirSeparator(m_parentClip.lock()->getClipPathName());

   // Time Track Number
   ostr << "tt" << m_trackNumber;

   // Framing Number
   int framingNumber = 0;  // Hard-code to zero for now, maybe others in future
   ostr << "_f" << framingNumber;

   return ostr.str();
}

string CTimeTrack::makeNewFrameListFileName(string newClipName) const
{
   return makeFrameListFileName();
}

void CTimeTrack::addFrame(CTimeFrame *frame)
{
   if (frame == 0)
      return;

   m_frameListAvailable = true;

   m_frameList.push_back(frame);
}

CTimeFrame* CTimeTrack::createFrame()
{
   // Factory method to create a new instance of a CTimeFrame subtype
   CTimeFrame *frame;

   if (m_timeType == TIME_TYPE_TIMECODE)
      frame = new CTimecodeFrame;

   else if (m_timeType == TIME_TYPE_KEYKODE)
      frame = new CKeykodeFrame;

   else if (m_timeType == TIME_TYPE_SMPTE_TIMECODE)
      frame = new CSMPTETimecodeFrame;

   else if (m_timeType == TIME_TYPE_EVENT)
      frame = new CEventFrame;

   else if (m_timeType == TIME_TYPE_AUDIO_PROXY)
      frame = new CAudioProxyFrame;
      
   else
      return 0;

   frame->initWithDummyValue();

   return frame;
}

int CTimeTrack::createFrameList(int totalFrameCount)
{
   int retVal;

   retVal = extendFrameList(totalFrameCount);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CTimeTrack::extendFrameList(int newFrameCount)
{
   for (int i = 0; i < newFrameCount; ++i)
      {
      CTimeFrame *newFrame = createFrame();
      addFrame(newFrame);
      }

   return 0;
}

// ---------------------------------------------------------------------------

int CTimeTrack::extendTimeTrack(int newFrameCount)
{
   int retVal;

   if (getClipVer() == CLIP_VERSION_3)
      {
      retVal = extendTimeTrack_ClipVer_3(newFrameCount);
      }
   else if (getClipVer() == CLIP_VERSION_5L)
      {
      retVal = extendTimeTrack_ClipVer_5L(newFrameCount);
      }
   else
      {
      TRACE_0(errout << "ERROR: CTimeTrack::extendTimeTrack: Invalid Clip Version: "
                     << getClipVer());
      retVal = CLIP_ERROR_INVALID_CLIP_VERSION;
      }

   return retVal;
}

int CTimeTrack::extendTimeTrack_ClipVer_5L(int newFrameCount)
{
   int retVal;

   string clipPath = m_parentClip.lock()->getClipPathName();

   if (getTimeType() == TIME_TYPE_SMPTE_TIMECODE)
      {
      C5SMPTETimecodeTrackHandle SMPTETimecodeTrackHandle = clip5TrackHandle;
      retVal = SMPTETimecodeTrackHandle.ExtendTimeTrack(clipPath, newFrameCount);
      if (retVal != 0)
         return retVal;
      }
   else if (getTimeType() == TIME_TYPE_KEYKODE)
      {
      C5KeycodeTrackHandle keycodeTrackHandle = clip5TrackHandle;
      retVal = keycodeTrackHandle.ExtendKeycodeTrack(clipPath, newFrameCount);
      if (retVal != 0)
         return retVal;
      }
   else
      return CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;

   // Update the Clip File (not sure why)
   retVal = m_parentClip.lock()->writeFile();
   if (retVal != 0)
      return retVal; // ERROR: Failed to write the Clip File

   return retVal;

}  //  extendTimeTrack_ClipVer_5L

int CTimeTrack::extendTimeTrack_ClipVer_3(int newFrameCount)
{
   int retVal;

   // Hack to make sure the time track file has been read before
   // attempt to extend it.
   getTotalFrameCount();

   retVal = extendFrameList(newFrameCount);
   if (retVal != 0)
      return retVal;

   // Write all of the new Track's Frame List files
   retVal = writeFrameListFile();
   if (retVal != 0)
      return retVal; // ERROR: Failed to write Frame List files for Time Track

   // Update the Clip File
   retVal = m_parentClip.lock()->writeFile();
   if (retVal != 0)
      return retVal; // ERROR: Failed to write the Clip File

   return 0;

} // extendTimeTrack

int CTimeTrack::shortenTimeTrack(int frameCount)
{
   int retVal;

   int totalFrameCount = getTotalFrameCount();

   if (totalFrameCount < frameCount)
      return CLIP_ERROR_INVALID_FRAME_NUMBER;  // arg exceeds available frames

   if (frameCount == 0)
      return 0; // nothing to do

   int startFrame = totalFrameCount - frameCount;

   retVal = truncateFrameList(startFrame);
   if (retVal != 0)
      return retVal;

   // Write all of the Track's Frame List files
   retVal = writeFrameListFile();
   if (retVal != 0)
      return retVal; // ERROR: Failed to write Frame List files for Time Track

   // Update the Clip File
   retVal = m_parentClip.lock()->writeFile();
   if (retVal != 0)
      return retVal; // ERROR: Failed to write the Clip File

   return 0;


} // shortenTimeTrack

//////////////////////////////////////////////////////////////////////
// Virtual Copy
//////////////////////////////////////////////////////////////////////

int CTimeTrack::virtualCopy(CTimeTrack &srcTrack, int srcFrameIndex,
                            int dstFrameIndex, int frameCount)
{
   int retVal;

   if (getTimeType() != srcTrack.getTimeType())
      return CLIP_ERROR_INVALID_TIME_TRACK_TYPE;

   // Virtual copy frames
   // Note: assumes valid source and destination frame indices and sufficient
   //       source and destination frames to cover frameCount.  Doesn't check.
   if (getTimeType() == TIME_TYPE_TIMECODE)
      {
      for (int i = 0; i < frameCount; ++i)
         {
         CTimecodeFrame *dstFrame = getTimecodeFrame(dstFrameIndex + i);
         CTimecodeFrame *srcFrame = srcTrack.getTimecodeFrame(srcFrameIndex + i);
         retVal = dstFrame->virtualCopy(*srcFrame);
         if (retVal != 0)
            return retVal;
         }
      }

   else if (getTimeType() == TIME_TYPE_KEYKODE)
      {
      for (int i = 0; i < frameCount; ++i)
         {
         CKeykodeFrame *dstFrame = getKeykodeFrame(dstFrameIndex + i);
         CKeykodeFrame *srcFrame = srcTrack.getKeykodeFrame(srcFrameIndex + i);
         retVal = dstFrame->virtualCopy(*srcFrame);
         if (retVal != 0)
            return retVal;
         }
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Read/Write File Functions
//////////////////////////////////////////////////////////////////////

int CTimeTrack::readFrameListFile()
{
   int retVal;

   CTimeFrameListFileReader frameListFileReader;

   retVal = frameListFileReader.readFrameListFile(*this);
   if (retVal != 0)
      return retVal; // ERROR: Could not read the frame list file

   return 0;

} // readFrameListFile

int CTimeTrack::reloadFrameListFile()
{
   int retVal;

   if (getClipVer() == CLIP_VERSION_3)
      {

      CTimeFrameListFileReader frameListFileReader;

      retVal = frameListFileReader.reloadFrameListFile(*this);
      if (retVal != 0)
         return retVal; // ERROR: Could not read the frame list file
      }
   else if (getClipVer() == CLIP_VERSION_5L)
      {
      if (getTimeType() == TIME_TYPE_SMPTE_TIMECODE
          || getTimeType() == TIME_TYPE_KEYKODE)
         {
         string clipPath = m_parentClip.lock()->getClipPathName();
         retVal = clip5TrackHandle.ReloadTrackFile(clipPath);
         if (retVal != 0)
            return retVal;
         }
      else
         return CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;
      }

   return 0;

}

int CTimeTrack::writeFrameListFile()
{
   int retVal;
   CTimeFrameListFileWriter frameListFileWriter;

   retVal = frameListFileWriter.writeFrameListFile(this);
   if (retVal != 0)
      return retVal;  // ERROR: Frame List file write failed

   return 0;

} // writeFrameListFile

int CTimeTrack::updateFrameListFile()
{

   int retVal;
   CTimeFrameListFileUpdater frameListFileUpdater;

   retVal = frameListFileUpdater.updateFrameListFile(this, 0, -1);

   if (retVal != 0){
   
      return retVal;  // ERROR: Frame List file update failed
  }
   return 0;

} // updateFrameListFile

int CTimeTrack::updateFrameListFile(int firstFrameIndex, int frameCount,
                                    char *externalBuffer,
                                    unsigned int externalBufferSize)
{

   int retVal;
   CTimeFrameListFileUpdater frameListFileUpdater(externalBuffer,
                                                  externalBufferSize);

   retVal = frameListFileUpdater.updateFrameListFile(this, firstFrameIndex,
                                                     frameCount);

   if (retVal != 0){
   
      return retVal;  // ERROR: Frame List file update failed
  }
   return 0;

} // updateFrameListFile

int CTimeTrack::UpdateTrackFile()
{
   // Update clip 5 track file
   int retVal;

   if (getTimeType() != TIME_TYPE_SMPTE_TIMECODE
       && getTimeType() != TIME_TYPE_KEYKODE)
      return CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;

   string clipPath = m_parentClip.lock()->getClipPathName();

   if (clip5TrackHandle.GetNeedsUpdate())
      {
      retVal = clip5TrackHandle.WriteTrackFile(clipPath);
      if (retVal != 0)
         return retVal;
      }

   return 0;

}

//////////////////////////////////////////////////////////////////////
// Read & Write Parameters
//////////////////////////////////////////////////////////////////////

int CTimeTrack::readTimeTrackSection(CIniFile *iniFile,
                                     const string& sectionName)
{
   int retVal;
   string valueString, defaultString;

   // Read the clip version
   valueString = iniFile->ReadString(sectionName, clipVerKey,
                              CClip::clipVerLookup.FindByValue(CLIP_VERSION_3));
   clipVer = CClip::clipVerLookup.FindByStr(valueString, CLIP_VERSION_INVALID);
   if (clipVer == CLIP_VERSION_INVALID)
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   // Read the Track's Time Type
   valueString = iniFile->ReadString(sectionName, timeTypeKey, defaultString);
   ETimeType newTimeType = queryTimeType(valueString);
   if (newTimeType == TIME_TYPE_INVALID)
      return CLIP_ERROR_INVALID_TIME_TRACK_TYPE;

   m_timeType = newTimeType;

   if (clipVer == CLIP_VERSION_5L)
      {
      // Read the clip 5 track id
      valueString = iniFile->ReadString(sectionName, trackIDKey, GetNullGUIDStr());
      Clip5TrackID newTrackID;
      if (!guid_from_string(newTrackID, valueString))
         {
         // ERROR: invalid track ID (GUID)
         return -1;
         }
      if (IsNullGUID(newTrackID))
         {
         // ERROR: invalid track ID
         return -1;
         }
      clip5TrackHandle.SetTrackID(newTrackID);

      retVal = clip5TrackHandle.ReadTrackFile(m_parentClip.lock()->getClipPathName());
      if (retVal != 0)
         return retVal;
      }

   // Read the Application Identifier string
   m_appId = iniFile->ReadString(sectionName, appIdKey, defaultString);

   // Read the framing type
   m_framingType = iniFile->ReadIntegerCreate(sectionName, framingTypeKey,
                                                FRAMING_TYPE_VIDEO);

   return 0;
}

EClipVersion CTimeTrack::readClipVer(CIniFile *iniFile,
                                     const string& sectionName)
{
   // Read the clip version from the track section
   string defaultString = CClip::clipVerLookup.FindByValue(CLIP_VERSION_3);
   string valueString = iniFile->ReadString(sectionName, clipVerKey,
                                             defaultString);
   EClipVersion newClipVer = CClip::clipVerLookup.FindByStr(valueString,
                                                          CLIP_VERSION_INVALID);

   return newClipVer;
}


int CTimeTrack::writeTimeTrackSection(CIniFile *iniFile,
                                      const string& sectionName)
{
   // Write the ClipVer string
   iniFile->WriteString(sectionName, clipVerKey,
                         CClip::clipVerLookup.FindByValue(clipVer));

   if (clipVer == CLIP_VERSION_5L)
      {
      // Write the track ID
      iniFile->WriteString(sectionName, trackIDKey,
                            clip5TrackHandle.GetTrackIDStr());
      }
   
   // Write the Track's Time Type
   iniFile->WriteString(sectionName, timeTypeKey,
                        queryTimeTypeName(m_timeType));

   // Write the Application Identifier string
   iniFile->WriteString(sectionName, appIdKey, m_appId);

   // Write the framing type
   iniFile->WriteInteger(sectionName, framingTypeKey, m_framingType);

   return 0;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

ETimeType CTimeTrack::queryTimeType(const string& timeTypeName)
{
   for (int i = 0; i < (int)TIME_TYPE_COUNT; ++i)
      {
      if (EqualIgnoreCase(timeTypeMap[i].name, timeTypeName))
         return timeTypeMap[i].timeType;
      }

   return TIME_TYPE_INVALID;
}

string CTimeTrack::queryTimeTypeName(ETimeType timeType)
{
   for (int i = 0; i < (int)TIME_TYPE_COUNT; ++i)
      {
      if (timeTypeMap[i].timeType == timeType)
         return string(timeTypeMap[i].name);
      }

   return string("Unknown");
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//
// CTimeTrackList Class Implementation
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTimeTrackList::CTimeTrackList()
{
}

CTimeTrackList::~CTimeTrackList()
{
   deleteTrackList();
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CTimeTrackList::getNewTrackNumber() const
{
   // Search the Track List to find the highest Track number in use
   int maxTrackNumber = -1;
   for (int trackIndex = 0; trackIndex < getTrackCount(); ++trackIndex)
      {
      CTimeTrack* track = getTrack(trackIndex);
      int trackNumber = track->getTrackNumber();
      if (track != 0 && maxTrackNumber < trackNumber)
         {
         maxTrackNumber = trackNumber;
         }
      }

   // Add one to get a new Track number higher than all others
   return maxTrackNumber + 1;
}

//////////////////////////////////////////////////////////////////////
// Time Track List Management
//////////////////////////////////////////////////////////////////////

int CTimeTrackList::createTimeTrack(ETimeType newTimeType,
                                    EClipVersion newClipVer,
                                    ClipSharedPtr &newParentClip)
{
   // Find a new time track number
   int trackNumber = getNewTrackNumber();

   CTimeTrack *track = new CTimeTrack(newTimeType, newClipVer, newParentClip,
                                      trackNumber);

   addTrack(track);

   return 0;
}

void CTimeTrackList::addTrack(CTimeTrack *newTrack)
{
   m_trackList.push_back(newTrack);
}

void CTimeTrackList::deleteTrackList()
{
   if (m_trackList.empty())
      return;
      
   for (int i = 0; i < getTrackCount(); ++i)
      {
      delete m_trackList[i];
      }

   m_trackList.clear();
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

CTimeTrack* CTimeTrackList::getTrack(int trackIndex) const
{
   if (trackIndex < 0 || trackIndex >= getTrackCount())
      return 0; // ERROR: Track Index is out-of-range

   return m_trackList[trackIndex];
}

int CTimeTrackList::getTrackCount() const
{
   return (int)m_trackList.size();
}

//////////////////////////////////////////////////////////////////////
// AppId Search
//////////////////////////////////////////////////////////////////////

CTimeTrack* CTimeTrackList::findTrack(const string &appId) const
{
   // Find the first CTimeTrack in the Time Track List that has an appId
   // that matches (case-sensitive) the caller's appId
   // Returns pointer to CTimeTrack instance if found, NULL pointer if not found

   for (int index = 0; index < getTrackCount(); ++index)
      {
      CTimeTrack *track = getTrack(index);
      if (track->getAppId() == appId)
         return track;
      }

   return 0; // Match was not found
}

int CTimeTrackList::findTrackIndex(const string &appId) const
{
   // Find the Track List index of the first CTimeTrack in the Track List
   // that has an appId that matches (case-sensitive) the caller's appId
   // Returns index into the Track List if found, -1 if not found.

   for (int index = 0; index < getTrackCount(); ++index)
      {
      CTimeTrack *track = getTrack(index);
      if (track->getAppId() == appId)
         return index;
      }

   return -1;  // Match was not found
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Read & Write Parameters
//////////////////////////////////////////////////////////////////////

int CTimeTrackList::readTimeTrackListSection(CIniFile *iniFile, ClipSharedPtr &clip)
{
   int retVal;
   int trackNumber;
   MTIostringstream ostr;
   string emptyString;
   CTimeTrack *newTrack;

   string sectionPrefix = timeTrackSectionPrefix;
   string trackListSection = sectionPrefix + "List";
   string trackInfoSection = sectionPrefix + "Info";

   if (!iniFile->SectionExists(trackListSection)
       || !iniFile->KeyExists(trackListSection, maxTrackIndexKey))
      return 0;  // No time tracks

   // Read the maximum track index
   int maxTrackIndex = iniFile->ReadInteger(trackListSection, maxTrackIndexKey,
                                            MAX_TRACK_INDEX_DEFAULT);

   // Get Proxy Information
   for (int trackIndex = 0; trackIndex <= maxTrackIndex; ++trackIndex)
      {
      // Construct Track key such as "TimeTrack0"
      ostr.str(emptyString);
      ostr << sectionPrefix << trackIndex;
      // Read the Track number from the Track List section
      trackNumber = iniFile->ReadInteger(trackListSection, ostr.str(),
                                         TRACK_NUMBER_NO_TRACK);
      if (trackNumber == TRACK_NUMBER_NO_TRACK)
         {
         newTrack = 0;
         }
      else
         {
         // Create instance of class derived from CTrack and add to
         // the track list
         newTrack = new CTimeTrack(TIME_TYPE_INVALID, CLIP_VERSION_INVALID,
                                   clip, trackNumber);

         // Construct Track Information section prefix such as "TimeTrackInfo0"
         ostr.str(emptyString);
         ostr << trackInfoSection << trackNumber;

         // Read Time Track Info section
         retVal = newTrack->readTimeTrackSection(iniFile, ostr.str());
         if (retVal != 0)
            return retVal; // ERROR: Failed to read Track information section
         }

      // Add the new time track to the time track list
      addTrack(newTrack);
      }

   return 0;

} // readTimeTrackListSection( )

int CTimeTrackList::refreshTimeTrackListSection(CIniFile *iniFile)
{
   int retVal;
   int trackNumber;
   MTIostringstream ostr;
   string emptyString;
   CTimeTrack *track;

   if (getTrackCount() == 0)
      return 0;  // No time tracks in list, so nothing to refresh

   string sectionPrefix = timeTrackSectionPrefix;
   string trackInfoSection = sectionPrefix + "Info";

   // Refresh Track Information for each track in track list
   int trackIndex;
   for (trackIndex = 0; trackIndex <= getTrackCount(); ++trackIndex)
      {
      track = getTrack(trackIndex);

      if (track != 0)
         {
         trackNumber = track->getTrackNumber();

         // Construct Video Proxy Information section prefix such as
         // "VideoProxyInfo0"
         ostr.str(emptyString);
         ostr << trackInfoSection << trackNumber;
         // Write Track Info sections (Image Format, etc)
         retVal = track->readTimeTrackSection(iniFile, ostr.str());
         if (retVal != 0)
            return retVal; // ERROR: Failed to refresh from Track information
                           //        section
         }
      }

   return 0;

} // refreshTimeTrackListSection( )

int CTimeTrackList::writeTimeTrackListSection(CIniFile *iniFile)
{
   int retVal;
   int trackIndex, trackNumber;
   MTIostringstream ostr;
   string emptyString;
   CTimeTrack *track;

   if (getTrackCount() < 1)
      return 0;   // Nothing to do

   string sectionPrefix = timeTrackSectionPrefix;
   string trackListSection = sectionPrefix + "List";
   string trackInfoSection = sectionPrefix + "Info";

   // Determine the highest index of all Tracks
   int maxTrackIndex = getTrackCount() - 1;

   // Write the maximum Track index
   iniFile->WriteInteger(trackListSection, maxTrackIndexKey, maxTrackIndex);

   // Set Track Information
   for (trackIndex = 0; trackIndex <= maxTrackIndex; ++trackIndex)
      {
      track = m_trackList[trackIndex];
      if (trackIndex > 0 && track == 0)
         {
         // Skip past Time Track List entries without a CTimeTrack
         break;
         }

      if (track == 0)
         {
         trackNumber = TRACK_NUMBER_NO_TRACK;
         }
      else
         {
         trackNumber = track->getTrackNumber();

         // Construct Time Track Information section prefix such as
         // "TimeTrackInfo0"
         ostr.str(emptyString);
         ostr << trackInfoSection << trackNumber;
         // Write Track Info sections
         retVal = track->writeTimeTrackSection(iniFile, ostr.str());
         if (retVal != 0)
            return retVal; // ERROR: Failed to write Track information section
         }

      // Construct Time Track key such as "TimeTrack0"
      ostr.str(emptyString);
      ostr << sectionPrefix << trackIndex;
      // Write the track number to the Time Track List section
      iniFile->WriteInteger(trackListSection, ostr.str(), trackNumber);
      }

   return 0;

} // writeTimeTrackListSection( )







