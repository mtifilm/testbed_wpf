// Clip3TimeTrackFile.h
//
// Created by: John Starr, January 27, 2003
//
/* CVS Info:
$Header: /usr/local/filmroot/clip/include/Clip3TimeTrackFile.h,v 1.13 2006/01/13 18:20:29 mlm Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef CLIP3TIMETRACKFILEH
#define CLIP3TIMETRACKFILEH

#include "FileBuilder.h"
#include "FileRecord.h"
#include "FileRecordString.h"
#include "timecode.h"
#include "SMPTETimecode.h"
#include "Clip3TimeTrack.h"

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CFileScanner;
class CEventFrame;
class CAudioProxySample;
class CAudioProxyFrame;
class CKeykodeFrame;
class CSMPTETimecodeFileRecord;
class CSMPTETimecodeFrame;
class CStructuredFileStream;
class CTimecodeFileRecord;
class CTimecodeFrame;
class CTimeFrame;
class CTimeTrack;

//////////////////////////////////////////////////////////////////////
// Time Track File Version
#define FR_TIME_TRACK_FILE_TYPE_FILE_VERSION (FR_INITIAL_FILE_TYPE_VERSION+2)

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

// SMPTE Timecode Frame file record schema version
#define FR_SMPTE_TIMECODE_FRAME_FILE_RECORD_LATEST_VERSION (FR_INITIAL_RECORD_VERSION+2)

//////////////////////////////////////////////////////////////////////

class CSMPTETimecodeFrameFileRecord : public CFileRecord
{
public:
   CSMPTETimecodeFrameFileRecord(MTI_UINT16 version
                          = FR_SMPTE_TIMECODE_FRAME_FILE_RECORD_LATEST_VERSION);
   virtual ~CSMPTETimecodeFrameFileRecord();

   float getPartialFrame() const;
   CSMPTETimecode getSMPTETimecode() const;

   void setPartialFrame(float newPartialFrame);
   void setSMPTETimecode(const CSMPTETimecode &newTimecode);

   virtual int read(CStructuredStream& stream);
   virtual int write(CStructuredStream &stream);

   void calculateRecordByteCount();

private:
   mutable CSMPTETimecodeFileRecord *timecodeRecord;
   MTI_REAL32 partialFrame;
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

// Timecode Frame file record schema version
#define FR_TIMECODE_FRAME_FILE_RECORD_LATEST_VERSION (FR_INITIAL_RECORD_VERSION+2)

//////////////////////////////////////////////////////////////////////

class CTimecodeFrameFileRecord : public CFileRecord
{
public:
   CTimecodeFrameFileRecord(MTI_UINT16 version
                                = FR_TIMECODE_FRAME_FILE_RECORD_LATEST_VERSION);
   virtual ~CTimecodeFrameFileRecord();

   CTimecode getTimecode() const;
   void setTimecode(const CTimecode &newTimecode);

   virtual int read(CStructuredStream& stream);
   virtual int write(CStructuredStream &stream);

   void calculateRecordByteCount();

private:
   mutable CTimecodeFileRecord *timecodeRecord;
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

// Keykode Frame file record schema version
#define FR_KEYKODE_FRAME_FILE_RECORD_LATEST_VERSION (FR_INITIAL_RECORD_VERSION+2)

//////////////////////////////////////////////////////////////////////

class CKeykodeFrameFileRecord : public CFileRecord
{
public:
   CKeykodeFrameFileRecord(MTI_UINT16 version
                                 = FR_KEYKODE_FRAME_FILE_RECORD_LATEST_VERSION);
   virtual ~CKeykodeFrameFileRecord();

   MTI_INT16 getFilmDimension() const {return filmDimension;};
   MTI_INT16 getInterpolateFlag() const {return interpolateFlag;};
   const string& getKeykodeStr() {return keykodeStringFileRecord.getString();};

   void setFilmDimension(MTI_INT16 newFilmDimension)
      {filmDimension = newFilmDimension;};
   void setInterpolateFlag(MTI_INT16 newInterpolateFlag)
      {interpolateFlag = newInterpolateFlag;};
   void setKeykodeStr(const string& newKeykodeStr)
      {keykodeStringFileRecord.setString(newKeykodeStr);};

   virtual int read(CStructuredStream& stream);
   virtual int write(CStructuredStream &stream);

   void calculateRecordByteCount();

private:
   CFileRecordString keykodeStringFileRecord;

   MTI_INT8 filmDimension;
   MTI_INT8 interpolateFlag;
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

// Event Frame file record schema version
#define FR_EVENT_FRAME_FILE_RECORD_LATEST_VERSION (FR_INITIAL_RECORD_VERSION+2)

//////////////////////////////////////////////////////////////////////

class CEventFrameFileRecord : public CFileRecord
{
public:
   CEventFrameFileRecord(MTI_UINT16 version
                                 = FR_EVENT_FRAME_FILE_RECORD_LATEST_VERSION);
   virtual ~CEventFrameFileRecord();

   MTI_UINT64 getEventFlags() {return eventFlags;};
   void setEventFlags(MTI_UINT64 newEventFlags) {eventFlags = newEventFlags;};

   virtual int read(CStructuredStream& stream);
   virtual int write(CStructuredStream &stream);

   void calculateRecordByteCount();

private:
   MTI_UINT64 eventFlags;
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

// Audio Proxy Frame file record schema version
#define FR_AUDIO_PROXY_FRAME_FILE_RECORD_LATEST_VERSION (FR_INITIAL_RECORD_VERSION+2)

//////////////////////////////////////////////////////////////////////

class CTimeFrameListFileReader;

class CAudioProxyFrameFileRecord : public CFileRecord
{
public:
   CAudioProxyFrameFileRecord(MTI_UINT16 version
                                 = FR_AUDIO_PROXY_FRAME_FILE_RECORD_LATEST_VERSION);
   virtual ~CAudioProxyFrameFileRecord();

   void getMinMaxAudio(int sample, int channel,
                       MTI_INT16 *min, MTI_INT16 *max);

   void setMinMaxAudio(int sample, int channel,
                       MTI_INT16 min, MTI_INT16 max);

   virtual int read(CStructuredStream& stream);
   virtual int write(CStructuredStream &stream);

   void calculateRecordByteCount();

private:
   SAudioProxySample
      audioProxySample[MAX_AUDIO_PROXY_SAMPLES][MAX_AUDIO_PROXY_CHANNELS];

friend class CTimeFrameListFileReader;
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
// CTimeFrameListFileReader
//   This class is used to read an entire frame list file and populate
//   time track with time frames

class CTimeFrameListFileReader
{
friend class CTimeFrameListFileUpdater;

public:
   CTimeFrameListFileReader();
   CTimeFrameListFileReader(char *newBuffer, unsigned int newBufferSize);
   virtual ~CTimeFrameListFileReader();

   int readFrameListFile(CTimeTrack& timeTrack);
   int reloadFrameListFile(CTimeTrack& timeTrack);

private:
   int openFrameListFile(const string& frameListFileName);
   void closeFrameListFile();

   int buildFramesFromFile(CTimeTrack& timeTrack);
   int updateFramesFromFile(CTimeTrack& timeTrack);

   CTimeFrame* makeFrame(CTimecodeFrameFileRecord& frameRecord);
   CTimeFrame* makeFrame(CKeykodeFrameFileRecord& frameRecord);
   CTimeFrame* makeFrame(CSMPTETimecodeFrameFileRecord& frameRecord);
   CTimeFrame* makeFrame(CEventFrameFileRecord& frameRecord);
   CTimeFrame* makeFrame(CAudioProxyFrameFileRecord& frameRecord);

   void setFrame(CTimecodeFrameFileRecord& frameRecord, CTimecodeFrame *frame);
   void setFrame(CKeykodeFrameFileRecord& frameRecord, CKeykodeFrame *frame);
   void setFrame(CSMPTETimecodeFrameFileRecord& frameRecord,
                 CSMPTETimecodeFrame *frame);
   void setFrame(CEventFrameFileRecord& frameRecord, CEventFrame *frame);
   void setFrame(CAudioProxyFrameFileRecord& frameRecord, CAudioProxyFrame *frame);
   
private:
   char *externalBuffer;
   unsigned int externalBufferSize;

   CStructuredFileStream *stream;
   CFileScanner *fileScanner;
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
// CTimeFrameListFileWriter
//   This class is used to write a new frame list file or completely
//   overwrite an existing file

class CTimeFrameListFileWriter
{
friend class CTimeFrameListFileUpdater;

public:
   CTimeFrameListFileWriter();
   virtual ~CTimeFrameListFileWriter();

   int writeFrameListFile(CTimeTrack* timeTrack);

private:
   int writeFrame(CTimecodeFrame& frame);
   int writeFrame(CKeykodeFrame& frame);
   int writeFrame(CSMPTETimecodeFrame& frame);
   int writeFrame(CEventFrame& frame);
   int writeFrame(CAudioProxyFrame& frame);
   int writeFrameList(CTimeTrack* timeTrack);
   int writeFrameList(CTimeTrack* timeTrack, int startFrameIndex,
                      int frameCount);
   int initForUpdate(CStructuredFileStream *newStream);

private:
   CFileBuilder fileBuilder;
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
// CTimeFrameListFileUpdater
//   This class is used to update individual records in an pre-exisiting
//   frame list file.  We can update individual records given the
//   records are of fixed (not variable) length and there is
//   only a single record type (other than the headers) in the file.

class CTimeFrameListFileUpdater
{
public:
   CTimeFrameListFileUpdater();
   CTimeFrameListFileUpdater(char *newBuffer, unsigned int newBufferSize);
   virtual ~CTimeFrameListFileUpdater();

   int updateFrameListFile(CTimeTrack* timeTrack, int firstFrameIndex,
                           int frameCount);

private:
   int closeFrameListFile();
   int openFrameListFile(const string& frameListFileName);
   int seekToRecord(int recordIndex);
   int updateFrameList(CTimeTrack* timeTrack, int firstFrameIndex,
                       int frameCount);
   int updateFrames(CTimeTrack* timeTrack, int frameIndex, int frameCount);

private:
   char *externalBuffer;
   unsigned int externalBufferSize;

   CTimeFrameListFileReader *fileReader;
   CTimeFrameListFileWriter *fileWriter;
   unsigned long fileHeaderSize;
   unsigned long frameRecordSize;
};

//////////////////////////////////////////////////////////////////////

#endif // #ifndef CLIP_TIME_TRACK_FILE_H
