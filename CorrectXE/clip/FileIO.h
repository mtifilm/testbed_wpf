// CFileIO.h: interface for the CCFileIO class.
//
// CFileIO class is a thin, machine-independent wrapper around
// the Low-Level File I/O functions.  For SGI/Unix this class wraps
// the low-level C library functions creat, open, read, write, etc.
// For Microsoft Windows, this class wraps the Win32 API file functions
// CreateFile, ReadFile and WriteFile
//
/*
$Header: /usr/local/filmroot/clip/include/FileIO.h,v 1.5 2003/04/28 19:09:41 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef FILEIOH
#define FILEIOH

#include <string>
using std::string;

#if defined(_WIN32)
#include <windows.h>
#endif

#include "machine.h"

//////////////////////////////////////////////////////////////////////

class CFileIO  
{
public:
   CFileIO();
   virtual ~CFileIO();

   int open(const string& fileName);  // Open for read only
   int close();

   int createFile(const string& fileName);
   static bool checkFileExists(const string& fileName);
   static int deleteFile(const string& fileName);

   int read(void *buffer, MTI_UINT32 count);
   int write(const void *buffer, MTI_UINT32 count);
   int write(const void *buffer, MTI_INT64 offset, MTI_UINT32 count);
   int seek(MTI_INT64 offset);

private:

#if defined(__sgi) || defined(__linux) // UNIX compilers
   int handle;
#elif defined(_WIN32)      // Windows, VC++ or BC++
   HANDLE handle;
#elif defined(_MSC_VER)     // Microsoft compilers
   int handle;
#else
#error Unknown Compiler
#endif
};

//////////////////////////////////////////////////////////////////////

#endif // #ifndef FILEIO_H
