// File: testgen.cpp
//
//       Writes test patterns into clips
//
//       Copied from Kurt Tolksdorf's stamper program
//


#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <memory.h>
#include <time.h>

#include "machine.h"
#include "ClipAPI.h"
#include "LineEngine.h"

////////////////////////////////////////////////////////////////////////

// These defines determine the nature of the test pattern
#define DO_FILM_FRAMES

ClipSharedPtr srcClip;

int fntsiz = 6;

int srcVideoProxyIndex = VIDEO_SELECT_NORMAL;

#ifdef DO_FILM_FRAMES
int srcVideoFramingIndex = FRAMING_SELECT_FILM;
#else
int srcVideoFramingIndex = FRAMING_SELECT_VIDEO;
#endif

const CImageFormat *srcVideoTrackFormat;

int srcImageWdth,
    srcImageHght;

int srcBitsPerComponent;

bool srcInterlaced;

int srcFieldCount;

int srcFieldSize;

CVideoFrameList *srcVideoFrameList;

int srcMinFrame,
    srcMaxFrame;

unsigned char *yuvImage[2];
unsigned char *yuvImage2[2];

CLineEngine *myEng;


int WriteMedia(CVideoFrame *frame, MTI_UINT8* fieldArray[])
{
#ifdef DO_FILM_FRAMES
   return frame->writeMediaFilmFramesOptimized(fieldArray);
#else
   return frame->writeMediaAllFramesOptimized(fieldArray);
#endif
}

////////////////////////////////////////////////////////////////////////
//
//                         M A I N
//
////////////////////////////////////////////////////////////////////////

int main(int argc, char* argv[])
{
   CreateApplicationName(argv[0]);

   if (argc == 2) {


     return(0);
   }

   if ((argc < 3)||(argc > 4)) exit(1);

   CMediaInterface::initialize();

   CBinManager binMgr;
   int stat;

   srcClip = binMgr.openClip(argv[1],
                   argv[2],&stat);

   if (stat !=0) {
      printf("Error opening clip\n");
      exit(1);
   }

   if (argc==4) {
      if (sscanf(argv[3],"%d",&fntsiz)!=1) exit(1);
      if ((fntsiz<1)||(fntsiz>10)) exit(1);

      srcVideoTrackFormat = srcClip->getImageFormat(srcVideoProxyIndex);
      srcImageWdth = srcVideoTrackFormat->getPixelsPerLine();
      srcImageHght = srcVideoTrackFormat->getLinesPerFrame();
      srcBitsPerComponent = srcVideoTrackFormat->getBitsPerComponent();
      srcInterlaced = srcVideoTrackFormat->getInterlaced();

      srcVideoFrameList = srcClip->getVideoFrameList(srcVideoProxyIndex,
					  srcVideoFramingIndex);

      srcMinFrame = srcVideoFrameList->getInFrameIndex();
      srcMaxFrame = srcVideoFrameList->getOutFrameIndex() - 1;

      myEng = CLineEngineFactory::makePixelEng(*srcVideoTrackFormat);

      srcFieldSize = srcVideoFrameList->getMaxPaddedFieldByteCount();

      yuvImage[0] = new unsigned char[srcFieldSize];
      yuvImage[1] = NULL;
      if (srcInterlaced)
         yuvImage[1] = new unsigned char[srcFieldSize];
      myEng->setExternalFrameBuffer(yuvImage);

      // clear the frame buffer and set up for writing timecodes

      myEng->setFGColor(0);
      myEng->drawRectangle(0,0,srcImageWdth-1,srcImageHght-1);
      myEng->setFGColor(1);
      myEng->setBGColor(0);
      myEng->setFontSize(fntsiz);

      for (int k = 0; k < srcImageHght/12; k += 3)
         {
         myEng->setFGColor(0, 0, 65535);   // blue
         myEng->moveTo(0,k+0);
         myEng->lineTo(40, k+0);

         myEng->setFGColor(0,65535,0);     // green
         myEng->moveTo(0,k+1);
         myEng->lineTo(40, k+1);

         myEng->setFGColor(65535,0,0);     // red
         myEng->moveTo(0,k+2);
         myEng->lineTo(40, k+2);
         }

      yuvImage2[0] = new unsigned char[srcFieldSize];
      yuvImage2[1] = NULL;
      if (srcInterlaced)
         yuvImage2[1] = new unsigned char[srcFieldSize];
      myEng->setExternalFrameBuffer(yuvImage2);

      // clear the frame buffer and set up for writing timecodes

      myEng->setFGColor(0);
      myEng->drawRectangle(0,0,srcImageWdth-1,srcImageHght-1);
      myEng->setFGColor(1);
      myEng->setBGColor(0);
      myEng->setFontSize(fntsiz);

      for (int k = 0; k < srcImageHght/12; k += 3)
         {
         myEng->setFGColor(0, 0, 65535);   // blue
         myEng->moveTo(40,k+0);
         myEng->lineTo(80, k+0);

         myEng->setFGColor(0,65535,0);     // green
         myEng->moveTo(40,k+1);
         myEng->lineTo(80, k+1);

         myEng->setFGColor(65535,0,0);     // red
         myEng->moveTo(40,k+2);
         myEng->lineTo(80, k+2);
         }
      // set the timecode for each frame, then write it in

      myEng->setFGColor(1);

      CTimecode curTime(0);

      char curTimeStr[16];

      char *numeral="00000";
      int n;

      for (int i=srcMinFrame;i<=srcMaxFrame;i++) {

         CVideoFrame *curFrame = srcVideoFrameList->getFrame(i);

         curFrame->getTimecode(curTime);
         curTime.getTimeASCII(curTimeStr);
         if (i%2 == 0)
            myEng->setExternalFrameBuffer(yuvImage);
         else
            myEng->setExternalFrameBuffer(yuvImage2);
         myEng->setFGColor(1);

         myEng->drawString((srcImageWdth-11*8*fntsiz)/2,(srcImageHght-10*fntsiz)/2,curTimeStr);

         int numY = ((i%10)+1)*myEng->getFontHeight();
         n = i;
         numeral[4] = '0' + (n%10);
         n /= 10;
         numeral[3] = '0' + (n%10);
         n /= 10;
         numeral[2] = '0' + (n%10);
         n /= 10;
         numeral[1] = '0' + (n%10);
         n /= 10;
         numeral[0] = '0' + (n%10);
         myEng->drawString(550, numY, numeral);

         char *numeral2 = "0";
         int invisibleFieldCount = curFrame->getInvisibleFieldCount();
         numeral2[0] = '0' + invisibleFieldCount;
         int num2X = 20 + (i%8)*myEng->getFontWidth();
         int num2Y = 100;
         myEng->drawString(num2X, num2Y, numeral2);
         int l;
         for (l=0; l < invisibleFieldCount; ++l)
            {
            CVideoField* field = curFrame->getField(curFrame->getVisibleFieldCount()+l);
            int visibleSiblingFieldIndex = field->getVisibleSiblingFieldIndex();
            numeral2[0] = '0' + visibleSiblingFieldIndex;
            num2Y += myEng->getFontHeight();
            myEng->drawString(num2X, num2Y, numeral2);
            }


         // Write to disk
         if (i%2 == 0)
            WriteMedia(curFrame, yuvImage);
         else
            WriteMedia(curFrame, yuvImage2);

         // Erase it all
         myEng->setFGColor(0);
         myEng->drawString(550, numY, numeral);

         numeral2[0] = '0' + invisibleFieldCount;
         num2X = 20 + (i%8)*myEng->getFontWidth();
         num2Y = 100;
         myEng->drawString(num2X, num2Y, numeral2);
         for (l=0; l < invisibleFieldCount; ++l)
            {
            CVideoField* field = curFrame->getField(curFrame->getVisibleFieldCount()+l);
            int visibleSiblingFieldIndex = field->getVisibleSiblingFieldIndex();
            numeral2[0] = '0' + visibleSiblingFieldIndex;
            num2Y += myEng->getFontHeight();
            myEng->drawString(num2X, num2Y, numeral2);
            }


      }

      // clean up storage

      delete [] yuvImage[0];
      delete [] yuvImage[1];

      CLineEngineFactory::destroyPixelEng(myEng);

      return(0);

   }

}

  
