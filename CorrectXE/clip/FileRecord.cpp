// FileRecord.cpp: implementation of the CFileRecord class.
//
/*
$Header: /usr/local/filmroot/clip/source/FileRecord.cpp,v 1.10 2005/02/08 21:45:09 tolks Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "FileRecord.h"

#include "Clip3AudioTrackFile.h"
#include "Clip3FieldListFile.h"
#include "Clip3TimeTrackFile.h"
#include "Clip3VideoTrackFile.h"
#include "CommonHeaderFileRecord.h"
#include "err_clip.h"
#include "StructuredStream.h"
#include "TimecodeFileRecord.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFileRecord::CFileRecord(MTI_UINT32 type, MTI_UINT16 version)
: recordByteCount(FR_MIN_RECORD_SIZE), recordType(type), recordTypeVersion(version)
{
    
}

CFileRecord::~CFileRecord()
{
    
}


CFileRecord* CFileRecord::make(EFileRecordType type, MTI_UINT16 version)
{
   CFileRecord *newRecord;

   switch (type)
      {
      case FR_RECORD_TYPE_COMMON_FILE_HEADER :
         newRecord = new CCommonHeaderFileRecord(version);
         break;
      case FR_RECORD_TYPE_TIMECODE :
         newRecord = new CTimecodeFileRecord(version);
         break;
      case FR_RECORD_TYPE_VIDEO_FRAME_R3 :
         newRecord = new CVideoFrameFileRecord(version);
         break;
      case FR_RECORD_TYPE_VIDEO_FIELD_R3 :
         newRecord = new CVideoFieldFileRecord(version);
         break;
      case FR_RECORD_TYPE_AUDIO_FRAME_R3 :
         newRecord = new CAudioFrameFileRecord(version);
         break;
      case FR_RECORD_TYPE_AUDIO_FIELD_R3 :
         newRecord = new CAudioFieldFileRecord(version);
         break;
      case FR_RECORD_TYPE_TIMECODE_FRAME :
         newRecord = new CTimecodeFrameFileRecord(version);
         break;
      case FR_RECORD_TYPE_KEYKODE_FRAME :
         newRecord = new CKeykodeFrameFileRecord(version);
         break;
      case FR_RECORD_TYPE_SMPTE_TIMECODE_FRAME :
         newRecord = new CSMPTETimecodeFrameFileRecord(version);
         break;
      case FR_RECORD_TYPE_EVENT_FRAME :
         newRecord = new CEventFrameFileRecord(version);
         break;
      case FR_RECORD_TYPE_AUDIO_PROXY_FRAME :
         newRecord = new CAudioProxyFrameFileRecord(version);
         break;
      case FR_RECORD_TYPE_SMPTE_TIMECODE :
         newRecord = new CSMPTETimecodeFileRecord(version);
         break;
      case FR_RECORD_TYPE_FIELD_LIST_ENTRY :
         newRecord = new CFieldListEntryFileRecord(version);
         break;
      default :
         newRecord = 0;
         break;
      }

   return(newRecord);
}

 
int CFileRecord::readRecord(CStructuredStream &stream, CFileRecord **newRecordPtr)
{
   int retVal;

   MTI_UINT32 size;
   MTI_UINT16 type, version;

   // Note: generic file error codes are returned because it is assumed that
   //       the class derived from CFileRecord will supply more specific
   //       error codes.

   // Read in the record's size
   // Ordinary EOF will be encountered at this read
   if ((retVal = stream.readUInt32(&size)) != 0)
      return retVal;

   // Read in the record's type
   if ((retVal = stream.readUInt16(&type)) != 0)
      return retVal;

   // Read in the record type's version
   if ((retVal = stream.readUInt16(&version)) != 0)
      return retVal;

   // Call the "factory" method to create a new instance
   // of the subtype of CFileRecord specified by the record's type
   // just read.  Error if NULL pointer returned
   CFileRecord *newRecord = make(EFileRecordType(type), version);
   if (!(newRecord))
      return CLIP_ERROR_FILE_READ_FAILED;

   // Set the recordByteCount
   newRecord->recordByteCount = size;

   // Read in the rest of the file record using virtual read function
   if ((retVal = newRecord->read(stream)) != 0)
      return retVal;

   // Return pointer to new CFileRecord subtype
   *newRecordPtr = newRecord;

   return 0;
}

MTI_UINT32 CFileRecord::getRecordByteCount() const
{
   return recordByteCount;
}

MTI_UINT16 CFileRecord::getRecordType() const
{
   return recordType;
}
MTI_UINT16 CFileRecord::getRecordTypeVersion() const
{
   return recordTypeVersion;
}

bool CFileRecord::isRecordType(EFileRecordType type)
{
   return (type == EFileRecordType(recordType));
}


void CFileRecord::setRecordByteCount(MTI_UINT32 newByteCount) 
{
   recordByteCount = newByteCount;
}


int CFileRecord::write(CStructuredStream &stream)
{
   int retVal;

   // Note: generic file error codes are returned because it is assumed that
   //       the class derived from CFileRecord will supply more specific
   //       error codes.

   // Write out the record's size
   if ((retVal = stream.writeUInt32(recordByteCount)) != 0)
      return retVal;

   // Write out the record's type
   if ((retVal = stream.writeUInt16(recordType)) != 0)
      return retVal;

   // Write out the record type's version
   if ((retVal = stream.writeUInt16(recordTypeVersion)) != 0)
      return retVal;

   return 0;
}

void CFileRecord::calculateRecordByteCount()
{
   // Total the size of all the fields in the record
   MTI_UINT32 newByteCount = 0;

   newByteCount += sizeof(recordByteCount); 
   newByteCount += sizeof(recordType); 
   newByteCount += sizeof(recordTypeVersion); 

   // Update the object with the calculated size
   setRecordByteCount(newByteCount);
}

