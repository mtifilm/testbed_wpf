// MediaLocation.cpp: implementation of the CMediaLocation class.
//
//////////////////////////////////////////////////////////////////////

#include "err_clip.h"
#include "MediaAccess.h"
#include "MediaLocation.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

// Default Constructor
CMediaLocation::CMediaLocation()
 : mediaAccess(0), dataIndex(0), dataSize(0), fieldName(0), fieldNumber(0), flags(0)
{

}

CMediaLocation::
CMediaLocation(CMediaAccess *newMediaAccess, MTI_INT64 newDataIndex, 
               MTI_UINT32 newDataSize, const char* newFieldName)
 : mediaAccess(newMediaAccess), dataIndex(newDataIndex), 
   dataSize(newDataSize), fieldNumber(0), flags(0)
{
   fieldName = 0;
   fieldName = copyFieldName(newFieldName);
}

// Copy Constructor
CMediaLocation::CMediaLocation(const CMediaLocation& mediaLocation)
 : mediaAccess(mediaLocation.mediaAccess),
   dataIndex(mediaLocation.dataIndex),
   dataSize(mediaLocation.dataSize),
   fieldNumber(mediaLocation.fieldNumber),
   flags(mediaLocation.flags)
{
   fieldName = 0;
   fieldName = copyFieldName(mediaLocation.fieldName);

   //** Dave's DPX Header Copy Hack
   setSourceImageFilePath(mediaLocation.getSourceImageFilePath());
   //**//
}

CMediaLocation::~CMediaLocation()
{
   deleteFieldName();
}

//////////////////////////////////////////////////////////////////////
// Assignment Operator
//////////////////////////////////////////////////////////////////////

CMediaLocation& CMediaLocation::operator=(const CMediaLocation& mediaLocation)
{
   if (this != &mediaLocation)
      {
      this->assign(mediaLocation);
      }

   return *this;
}

CMediaLocation& CMediaLocation::assign(const CMediaLocation& mediaLocation)
{
   if (this != &mediaLocation)
      {
      // Make a copy of the field name.  An exception in the allocation
      // will leave *this unchanged
      char *fieldNameCopy = copyFieldName(mediaLocation.fieldName);

      mediaAccess = mediaLocation.mediaAccess;
      dataIndex = mediaLocation.dataIndex;
      dataSize = mediaLocation.dataSize;
      fieldNumber = mediaLocation.fieldNumber;
      flags = mediaLocation.flags;

      //** Dave's DPX Header Copy Hack
      setSourceImageFilePath(mediaLocation.getSourceImageFilePath());
      //**//

      deleteFieldName();
      fieldName = fieldNameCopy;
      }

   return *this;
}

//////////////////////////////////////////////////////////////////////
// Comparison Operators
//////////////////////////////////////////////////////////////////////

bool CMediaLocation::operator==(const CMediaLocation& rhs) const
{
   return (compare(rhs) == 0);
}

bool CMediaLocation::operator!=(const CMediaLocation& rhs) const
{
   return (compare(rhs) != 0);
}

bool CMediaLocation::operator>(const CMediaLocation& rhs) const
{
   return (compare(rhs) > 0);
}

bool CMediaLocation::operator<(const CMediaLocation& rhs) const
{
   return (compare(rhs) < 0);
}

int CMediaLocation::compare(const CMediaLocation& rhs) const
{
   // Order of comparison
   //    1. Media Access
   //    2. Field Name
   //    3. Data Index
   //
   //    flags are not compared
   //    field numbers are not compared
   //    DPX Header Copy Hack source image file paths are not compared

   int compareResult, mediaAccessCompare = -1;

   // Make simple pointer comparison followed by deep comparison
   if (
   (mediaAccess == NULL && rhs.mediaAccess == NULL)
       || (
          (mediaAccess != NULL && rhs.mediaAccess != NULL)
           && (
              (mediaAccess == rhs.mediaAccess)
              || (
                 ((mediaAccessCompare = mediaAccess->compare(*(rhs.mediaAccess))) == 0)
                 )
              )
          )
      )
      {
      // The media accesses match, so compare the field names
      int fieldNameCompare;
      if (fieldName == 0 && rhs.fieldName == 0)
         {
         // Both fieldNames are NULL pointers, which is as good as a match
         fieldNameCompare = 0;
         }
      else
         {
         // One or both of the fieldNames are non-NULL, so do string
         // comparison.  Replace the NULL fieldName with an empty string
         // to get a proper comparison
		 const char *lhsFieldName = fieldName;
         const char *rhsFieldName = rhs.fieldName;
         if (lhsFieldName == 0)
            lhsFieldName = "";
         else if (rhsFieldName == 0)
            rhsFieldName = "";
         fieldNameCompare = strcmp(lhsFieldName, rhsFieldName);
         }

      if (fieldNameCompare == 0)
         {
         // Field Names are the same, so compare the data indices
         if (dataIndex < rhs.dataIndex)
            compareResult = -1;
         else if (dataIndex > rhs.dataIndex)
            compareResult = 1;
         else // if (dataIndex == rhs.dataIndex)
            compareResult = 0;
         }
      else
         {
         compareResult = fieldNameCompare;
         }
      }
   else
      {
      compareResult = mediaAccessCompare;
      }

   return compareResult;
}

//////////////////////////////////////////////////////////////////////
// Read from/Write to Media Storage
//////////////////////////////////////////////////////////////////////

int CMediaLocation::read(MTI_UINT8 *buffer)
{
   int retVal;

   retVal = read(buffer, 0);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CMediaLocation::read(MTI_UINT8 *buffer, int fileHandleIndex)
{
   if (mediaAccess == 0)
      {
      // CMediaAccess instance missing
      return CLIP_ERROR_INVALID_MEDIA_LOCATION;
      }

   return (mediaAccess->readMT(buffer, *this, fileHandleIndex));
}

int CMediaLocation::readWithOffset(MTI_UINT8 *buffer, MTI_UINT8* &offsetBuffer)
{
   if (mediaAccess == 0)
      {
      // CMediaAccess instance missing
      return CLIP_ERROR_INVALID_MEDIA_LOCATION;
      }

   return (mediaAccess->readWithOffset(buffer, offsetBuffer, *this));
}

// xxxmfioxxx
//bool CMediaLocation::canBeReadAsynchronously()
//{
//   return (mediaAccess->canBeReadAsynchronously(*this));
//}
//
//int CMediaLocation::startAsynchReadWithOffset(MTI_UINT8 *buffer,
//                                              MTI_UINT8* &offsetBuffer,
//                                              void* &readContext,
//                                              bool dontNeedAlpha)
//{
//   if (mediaAccess == 0)
//      {
//      // CMediaAccess instance missing
//      return CLIP_ERROR_INVALID_MEDIA_LOCATION;
//      }
//
//   return (mediaAccess->startAsynchReadWithOffset(buffer, offsetBuffer,
//                                      *this, readContext, dontNeedAlpha));
//}
//
//bool CMediaLocation::isAsynchReadWithOffsetDone(void* readContext)
//{
//   if (mediaAccess == 0)
//      {
//      // CMediaAccess instance missing
//      return true;
//      }
//
//   return (mediaAccess->isAsynchReadWithOffsetDone(readContext));
//}
//
//int CMediaLocation::finishAsynchReadWithOffset(void* &readContext,
//                                               MTI_UINT8* &offsetBuffer)
//{
//   if (mediaAccess == 0)
//      {
//      // CMediaAccess instance missing
//      return CLIP_ERROR_INVALID_MEDIA_LOCATION;
//      }
//
//   return (mediaAccess->finishAsynchReadWithOffset(readContext, offsetBuffer));
//}
//
//bool CMediaLocation::canReadFrameBuffers()
//{
//   return (mediaAccess->canReadFrameBuffers(*this));
//}
//
//int CMediaLocation::startAsyncFrameBufferRead(void* &readContext)
//{
//   if (mediaAccess == 0)
//      {
//      // CMediaAccess instance missing
//      return CLIP_ERROR_INVALID_MEDIA_LOCATION;
//      }
//
//   return (mediaAccess->startAsyncFrameBufferRead(*this, readContext));
//}
//
//bool CMediaLocation::isAsyncFrameBufferReadDone(void* readContext)
//{
//   if (mediaAccess == 0)
//      {
//      // CMediaAccess instance missing
//      return true;
//      }
//
//   return (mediaAccess->isAsyncFrameBufferReadDone(readContext));
//}
//
//int CMediaLocation::finishAsyncFrameBufferRead(void* &readContext, MediaFrameBuffer* &frameBufferOut)
//{
//   if (mediaAccess == 0)
//      {
//      // CMediaAccess instance missing
//      return CLIP_ERROR_INVALID_MEDIA_LOCATION;
//      }
//
//   return (mediaAccess->finishAsynchFrameBufferRead(readContext, frameBufferOut));
//}
// xxxmfioxxx


// I don't know what to call this. It's a read that is very fast for video
// recording to VTR, but is allowed to fail for some valid clips which
// cannot be read fast enough to be recordable to VTR, e.g. DPX files whose
// image data is not aligned on a 4K page boundary.
int CMediaLocation::readMediaAsQuicklyAsPossible(MTI_UINT8* buffer)
{
   int retVal;

   retVal = readMediaAsQuicklyAsPossibleMT(buffer, 0);
   if (retVal != 0)
      return 0;

   return 0;
}

int CMediaLocation::readMediaAsQuicklyAsPossibleMT(MTI_UINT8* buffer,
                                                   int fileHandleIndex)
{
   int retVal;
   MTI_UINT8* offsetBuffer;

	if (mediaAccess == 0)
		{
		// CMediaAccess instance missing
		return CLIP_ERROR_INVALID_MEDIA_LOCATION;
		}

	retVal = mediaAccess->readWithOffsetMT(buffer, offsetBuffer, *this,
                                          fileHandleIndex);
   if (retVal == 0 && buffer != offsetBuffer)
      {
      // Oops, wasn't aligned...!
      return CLIP_ERROR_MEDIA_ACCESS_INVALID;   // qqq define a new error...
      }

   return retVal;
}

int CMediaLocation::write(MTI_UINT8 *buffer)
{
   if (mediaAccess == 0)
      {
      // CMediaAccess instance missing
      return CLIP_ERROR_INVALID_MEDIA_LOCATION;
      }

   return (mediaAccess->write(buffer, *this));
}

//////////////////////////////////////////////////////////////////////
// New frame buffer stuff.
//////////////////////////////////////////////////////////////////////

int CMediaLocation::fetchMediaFile()
{
	if (mediaAccess == 0)
		{
		// CMediaAccess instance missing
		return CLIP_ERROR_INVALID_MEDIA_LOCATION;
		}

	return mediaAccess->fetchMediaFile(*this);
}

int CMediaLocation::readMediaFrameBuffer(MediaFrameBufferSharedPtr &frameBuffer)
{
	if (mediaAccess == 0)
		{
		// CMediaAccess instance missing
      frameBuffer = nullptr;
		return CLIP_ERROR_INVALID_MEDIA_LOCATION;
		}

	return mediaAccess->readMediaFrameBuffer(frameBuffer, *this);
}

int CMediaLocation::writeMediaFrameBuffer(MediaFrameBufferSharedPtr frameBuffer)
{
	if (mediaAccess == 0)
		{
		// CMediaAccess instance missing
		return CLIP_ERROR_INVALID_MEDIA_LOCATION;
		}

	return mediaAccess->writeMediaFrameBuffer(frameBuffer, *this);
}

//////////////////////////////////////////////////////////////////////
// Hack Media Storage
//////////////////////////////////////////////////////////////////////

int CMediaLocation::hack(EMediaHackCommand hackCommand)
{
   if (mediaAccess == 0)
      {
      // CMediaAccess instance missing
      return CLIP_ERROR_INVALID_MEDIA_LOCATION;
      }

   return mediaAccess->hack(hackCommand, *this);
}

//////////////////////////////////////////////////////////////////////
// Destroy Media Storage
//////////////////////////////////////////////////////////////////////

int CMediaLocation::destroy()
{
   if (mediaAccess == 0)
      {
      // CMediaAccess instance missing
      return CLIP_ERROR_INVALID_MEDIA_LOCATION;
      }

   return mediaAccess->destroy(*this);
}

//////////////////////////////////////////////////////////////////////
// Accessor Member Functions
//////////////////////////////////////////////////////////////////////

MTI_UINT32 CMediaLocation::getDataByteSize() const
{
   if (mediaAccess == 0)
      return 0;

   return mediaAccess->convertDataSizeToByteCount(dataSize);
}

MTI_INT64 CMediaLocation::getDataIndex() const
{
   return dataIndex;
}

MTI_UINT32 CMediaLocation::getDataOffset() const
{
//	if (mediaAccess == 0)
//		return 0;
//
//	return mediaAccess->getDefaultDataOffset();
   return 0;
}

MTI_UINT32 CMediaLocation::getDataSize() const
{
   if (getDataOffset() != 0 && mediaAccess != 0)
      {
      // data size for image files with offset into buffer
      return mediaAccess->getAdjustedFieldSize(dataSize);
      }
   else
      {
      // normal data size
      return dataSize;
      }
}

const char* CMediaLocation::getFieldName() const
{
   return fieldName;
}

MTI_UINT32 CMediaLocation::getFieldNumber() const
{
   return fieldNumber;
}

MTI_UINT8 CMediaLocation::getFlags() const
{
   return flags;
}

CMediaAccess* CMediaLocation::getMediaAccess() const
{
   return mediaAccess;
}

//** Dave's DPX Header Copy Hack
string CMediaLocation::getImageFilePath() const
{
   if (mediaAccess == NULL)
      return string("");

   return mediaAccess->getImageFileName(*this);
}

string CMediaLocation::getHistoryFilePath() const
{
   if (mediaAccess == NULL)
      return string("");

   return mediaAccess->getHistoryFileName(*this);
}

//** Dave's DPX Header Copy Hack
// Virtual method, derived class overrides it
string CMediaLocation::getSourceImageFilePath() const
{
   return "";
}

bool CMediaLocation::isFlagSet(MTI_UINT8 bitFlag) const
{
   return (flags & bitFlag) ? true : false;
}

bool CMediaLocation::isImported() const
{
   // Return false if media data created by cpmp
   // Return true if media data was imported from elsewhere
   return (isFlagSet(MEDIA_LOCATION_FLAG_IMPORTED));
}

bool CMediaLocation::isInitialized() const
{
   // Return false if media is not initialized when it is allocated
   // Return true if media is initialized when it is allocated
   return (isFlagSet(MEDIA_LOCATION_FLAG_INITIALIZED));
}

bool CMediaLocation::isMarked() const
{
   // Return false if media storage has been allocated
   // Return true if media storage is "marked" and has not been allocated
   return (isFlagSet(MEDIA_LOCATION_FLAG_MARKED));
}

bool CMediaLocation::isTwoFields() const
{
   // Return false if pointing at a single field
   // Return true if pointing at a pair of adjacent fields
   return (isFlagSet(MEDIA_LOCATION_FLAG_TWO_FIELDS));
}

bool CMediaLocation::isSecondField() const
{
   // Return false if not pointing at second visible field in frame
   // Return true if pointing at a second visible field in frame
   return (isFlagSet(MEDIA_LOCATION_FLAG_SECOND_FIELD));
}

bool CMediaLocation::isMediaTypeImageFile()
{
   if (mediaAccess == 0)
      return false;

   return CMediaInterface::isMediaTypeImageFile(mediaAccess->getMediaType());
}

// Ugly hack - repositories are sort of half VideoStore, half image file beasts
bool CMediaLocation::isMediaTypeImageRepository()
{
   if (mediaAccess == 0)
      return false;

   return CMediaInterface::isMediaTypeImageRepository(mediaAccess->getMediaType());
}

bool CMediaLocation::isValid() const
{
   return ((mediaAccess != 0)
           && (dataIndex >= 0)
           && (dataSize > 0));
}

int CMediaLocation::Peek(string &filename, MTI_INT64 &offset)
{
   int retVal;

   filename.erase();
   offset = 0;

   if (!isValid())
      return CLIP_ERROR_INVALID_MEDIA_LOCATION;

   
   retVal = mediaAccess->Peek(*this, filename, offset);
   if (retVal != 0)
      return retVal;

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Mutator Member Functions
//////////////////////////////////////////////////////////////////////

void CMediaLocation::setDataIndex(MTI_INT64 newDataIndex)
{
   dataIndex = newDataIndex;
}

void CMediaLocation::setDataSize(MTI_UINT32 newDataSize)
{
   dataSize = newDataSize;
}

void CMediaLocation::setFieldName(const char* newFieldName)
{
   // If a field name has been allocated, delete it
   deleteFieldName();

   fieldName = copyFieldName(newFieldName);
}

void CMediaLocation::setFieldNumber(MTI_UINT32 newFieldNumber)
{
   // ******* NOTE THIS FIELD IS ACTUALLY 24 BITS, NOT 32 ********
   fieldNumber = newFieldNumber;
}

void CMediaLocation::setFlags(MTI_UINT8 newFlags)
{
   // Set all flags as bit flags, mask out reserved bits
   flags = newFlags & MEDIA_LOCATION_FLAG_VALID_BITS;
}

void CMediaLocation::setFlag(MTI_UINT8 bitFlag, bool newValue)
{
   // Set or clear a bit in the flags
   
   if (newValue)
      {
      // Set the bit in the flags
      flags = flags | bitFlag;
      }
   else
      {
      // Clear the bit in the flags
      flags = flags & ~bitFlag;
      }
}

void CMediaLocation::setImported(bool newFlag)
{
   setFlag(MEDIA_LOCATION_FLAG_IMPORTED, newFlag);
}

void CMediaLocation::setInitialized(bool newFlag)
{
   setFlag(MEDIA_LOCATION_FLAG_INITIALIZED, newFlag);
}

void CMediaLocation::setMarked(bool newFlag)
{
   setFlag(MEDIA_LOCATION_FLAG_MARKED, newFlag);
}

void CMediaLocation::setTwoFields(bool newFlag)
{
   setFlag(MEDIA_LOCATION_FLAG_TWO_FIELDS, newFlag);
}

void CMediaLocation::setSecondField(bool newFlag)
{
   setFlag(MEDIA_LOCATION_FLAG_SECOND_FIELD, newFlag);
}

void CMediaLocation::setMediaAccess(CMediaAccess* newMediaAccess)
{
   mediaAccess = newMediaAccess;
}

//** Dave's DPX Header Copy Hack
// Virtual method, derived class must override
void CMediaLocation::setSourceImageFilePath(const string &path)
{
   /* Base class default is to do absolutely nothing */
}

void CMediaLocation::setInvalid()
{
   // Set the media location to an invalid state
   mediaAccess = 0;
   dataSize = 0;

   // Clear the flags
   flags = 0;

   // Clear the field number
   fieldNumber = 0;

   // If a field name has been allocated, delete it
   deleteFieldName();

   //** Dave's DPX Header Copy Hack
   setSourceImageFilePath("");
   //**//
}


//////////////////////////////////////////////////////////////////////
// Miscellaneous Member Functions
//////////////////////////////////////////////////////////////////////

void CMediaLocation::getQuickReadLocation(int quickReadFlag, 
                                          CMediaLocation& adjustedMediaLocation
                                         ) const
{
   MTI_INT64 newSize;

   // Interpret quickReadFlag to determine how much of the image 
   // should be read
   switch (quickReadFlag)
      {
      case 0 :
         // Read entire image, media location remains the same
         adjustedMediaLocation = *this;
         return;
      case 1 :
         // Read central one-half of image
         newSize = dataSize / 2;
         break;
      case 2 :
         // Read central one-quarter of image
         newSize = dataSize / 4;
         break;
      case 3 :
         // Read central one-eigth of image
         newSize = dataSize / 8;
         break;
      default :  // Anything other than 0, 1, 2 and 3
         // Set size to zero so nothing will be read
         adjustedMediaLocation = *this;
         adjustedMediaLocation.setDataSize(0);
         return;
      }

   // Create CMediaInterface to access blocking of media storage
   CMediaInterface mediaInterface;

   // Adjust new size for blocking of media storage device
   if (mediaAccess)
      newSize = mediaAccess->adjustToBlocking(newSize, true);

   // Adjust offset for central portion of image
   MTI_INT64 offsetAdjustment = ((MTI_INT64)dataSize - newSize) / 2;
   MTI_INT64 newDataIndex = dataIndex + offsetAdjustment;
   if (mediaAccess)
      newDataIndex = mediaAccess->adjustToBlocking(newDataIndex, false);

   adjustedMediaLocation = *this;
   adjustedMediaLocation.setDataIndex(newDataIndex);
   adjustedMediaLocation.setDataSize((MTI_UINT32)newSize);

   return;
}

//////////////////////////////////////////////////////////////////////
// Private Helper Functions
//////////////////////////////////////////////////////////////////////

char* CMediaLocation::copyFieldName(const char* newFieldName)
{
   size_t fieldNameSize;
   char *fieldNameCopy;
   if (newFieldName != 0 && (fieldNameSize = strlen(newFieldName)) != 0)
      {
      fieldNameCopy = new char[fieldNameSize + 1];
      strcpy(fieldNameCopy, newFieldName);
      }
   else
      {
      fieldNameCopy = 0;
      }
   return fieldNameCopy;
}

void CMediaLocation::deleteFieldName()
{
   delete [] fieldName;
   fieldName = 0;
}

//////////////////////////////////////////////////////////////////////
// Testing Member Functions
//////////////////////////////////////////////////////////////////////

void CMediaLocation::dump(MTIostringstream &str) const
{
	std::ostringstream os;
	os << getMediaAccess();
	str << "mediaAccess: " << os.str() << std::endl;
   str << "            dataIndex: " << getDataIndex();
   str << "  dataSize: " << getDataSize()
       << "  flags: " << ((int) getFlags())
       << "  fieldName: <";
   if (getFieldName() != 0)
      str << getFieldName();
   str << ">  fieldNumber: " << getFieldNumber() << std::endl;
}

//** Dave's DPX Header Copy Hack
//////////////////////////////////////////////////////////////////////
// Extended version of class for DPX Header Copy Hack
//////////////////////////////////////////////////////////////////////

CMediaLocationExt::CMediaLocationExt()
{
}

CMediaLocationExt::CMediaLocationExt(const CMediaLocationExt& mediaLocationExt)
: CMediaLocation(mediaLocationExt)
{
   setSourceImageFilePath(mediaLocationExt.getSourceImageFilePath());
}

CMediaLocationExt::CMediaLocationExt(const CMediaLocation& mediaLocation)
: CMediaLocation(mediaLocation)
{
}

CMediaLocationExt::~CMediaLocationExt()
{
}

CMediaLocationExt &CMediaLocationExt::operator=(const CMediaLocationExt& rhs)
{
   if (&rhs != this)
      {
      this->assign(rhs);
      setSourceImageFilePath(rhs.getSourceImageFilePath());
      }

   return *this;
}

CMediaLocationExt &CMediaLocationExt::operator=(const CMediaLocation& rhs)
{
   if (&rhs != this)
      {
      this->assign(rhs);
      }

   return *this;
}

string CMediaLocationExt::getSourceImageFilePath() const
{
   return DPXHeaderCopyHackSourcePath;
}

void CMediaLocationExt::setSourceImageFilePath(const string &newPath)
{
   DPXHeaderCopyHackSourcePath = newPath;
}
//**///////////////////////////////////////////////////////////////////////


//------------------------------------------------------------------------
//
// Function:     SortMediaLocations
//
// Description:  Orders fields by ascending media location and field flags.
//               Used to determine optimal order of media storage disk I/O.
//               Sorting results are returned in caller's indexArray[] as 
//               ordered field indices.  Caller's fieldArray[] is set to 
//               Field object pointers from the Frame's field list.  The
//               fieldArray is ordered the same as the Frame's field list
//               and is not sorted.
//
//               Sorting Order:
//                1) Media Type         Numeric sort of media storage type enum
//                2) Media Identifier   Alpha sort of name
//                3) Field Name         Alpha sort field-specific portion of
//                                      media file name
//                4) Data Index         Numeric sort of data location in
//                                      media storage
//                5) Shared Media Flag  First shared media before non-first
//                                      shared media
//                6) Invisible Field Flag Visible field before invisible field
//
//               If two or more fields in a frame have the same media location,
//               one field will have the isSharedMediaFirst flag set to true
//               and the other fields will have the flag set to false.
//
// Arguments     int count              Number of media locations to sort
//               int *indexArray        Array to hold results of sort
//               CMediaLocation mediaLocationArray    Array of media locations
//
// Returns:      None.
//
// Side Effects: Caller's array indexArray[] set with indices into
//               mediaLocationArray[].
//
//------------------------------------------------------------------------
void SortMediaLocations(int count, int *indexArray,
                        CMediaLocation mediaLocationArray[])
{
   int i;
   // Initialize the output array
   for (i = 0; i < count; i++)
      {
      indexArray[i] = i;
      }

   // Now sort the fields in ascending media location
   if (count == 1)
      {
      // Only one field, so no need to sort anything, so return
      return;
      }
   else if (count == 2)
      {
      // Two fields, so sort with one comparison and possible swap
      if (mediaLocationArray[0] > mediaLocationArray[1])
         {
         // Swap the field indices
         indexArray[0] = 1;
         indexArray[1] = 0;
         }
      return;
      }
   else
      {
      // Three or more fields, so do a real sort

      // Sort below is an "Insertion" sort.  This sort algorithm has
      // the advantages of 1) simple, 2) O(n) performance when elements
      // are already sorted and 3) low overhead for small numbers of elements.
      // Disadvantages are O(n**2) worst-case performance, which is bad
      // for large number of elements, but mostly irrelevant for this
      // application

      // Initially, the first item is considered 'sorted'
      // i divides a into a sorted region, x<i, and an unsorted one, x >= i
      for(i = 1; i < count; i++)
         {
         // Select the item at the beginning of the as yet unsorted section
         int v = indexArray[i];

         // Work backwards through the array, finding where v should go
         int j = i;

         // If this element is greater than v, move it up one
         while (j > 0 &&
                mediaLocationArray[indexArray[j-1]] > mediaLocationArray[v])
            {
            indexArray[j] = indexArray[j-1];
            j--;
            }

         // Stopped when a[j-1] <= v, so put v at position j
         indexArray[j] = v;
         }
      }
}

