// MediaDeallocList.h: interface for the CMediaDeallocList class.
//
/* CVS Info:
$Header: /usr/local/filmroot/clip/include/MediaDeallocList.h,v 1.5 2006/06/01 18:26:55 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef MEDIADEALLOCLISTH
#define MEDIADEALLOCLISTH

#include "cliplibint.h"
#include "machine.h"

#include <list>
using std::list;

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CMediaLocation;

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////

class CMediaDeallocList  
{
public:
   class CMediaDeallocListEntry
   {
   public:
      CMediaDeallocListEntry() : dataIndex(0), dataEnd(0), dataSize(0) {};
      CMediaDeallocListEntry(MTI_INT64 newDataIndex, MTI_INT64 newDataSize)
         : dataIndex(newDataIndex), dataEnd(newDataIndex + newDataSize),
         dataSize(newDataSize) {};
      ~CMediaDeallocListEntry() {};

      MTI_INT64 dataIndex;
      MTI_INT64 dataEnd;
      MTI_INT64 dataSize;
   };

   typedef list<CMediaDeallocListEntry> DeallocList;
   typedef DeallocList::iterator DeallocListIterator;

public:
   CMediaDeallocList();
   virtual ~CMediaDeallocList();

   void addToDeallocList(const CMediaLocation& mediaLocation);
   void addToDeallocList(const CMediaLocation& mediaLocation,
                         int itemCount);

   DeallocList& getDeallocList();

private:
   void addToDeallocList(MTI_INT64 newDataIndex, MTI_INT64 newDataSize);
   
private:
   DeallocList deallocList;
};

//////////////////////////////////////////////////////////////////////

#endif // #ifndef MEDIADEALLOCLIST_H
