#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <iostream>

#include "machine.h"
#include "ClipAPI.h"
#include "Clip5Proto.h"
#include "getopt.h"

////////////////////////////////////////////////////////////////////////

void PrintUsage(char *cpArg)
{
  cerr << "Usage: " << "C325 " << " -A <clip name>" << endl;
}

////////////////////////////////////////////////////////////////////////
//
//                         M A I N
//
////////////////////////////////////////////////////////////////////////

int main(int argc, char* argv[])
{
   int retVal;
   
   CreateApplicationName(argv[0]);
   string strClipName;

   if (argc < 2) {

     PrintUsage (argv[0]);
     return(0);
   }

  InitClip5();
  
  // parse the input
  int iOpt;
  while ( (iOpt = getopt (argc, argv, "t:A:")) != EOF)
   {
    switch (iOpt)
     {
      case 'A':
        strClipName = optarg;
      break;
      default:
        PrintUsage (argv[0]);
        return -1;
     }
   }

   cout << "Converting clip " << strClipName << " to Version 5" << endl;

   retVal = ConvertAllVideoTracks(strClipName);
   switch (retVal)
      {
      case 0:
         cout << "Conversion to clip 5 succeeded" << endl;
         break;
      case CLIP_ERROR_INVALID_CLIP_VERSION:
         cout << "Clip has already been converted" << endl;
         break;
      case 1:
         cout << "The clip backup file already exists" << endl;
         break;
      case 2:
         cout << "Could not make backup copy of clip .clp file" << endl;
         break;
      default :
         cout << "Conversion failed, status code = " << retVal << endl;
         break;
      }

   return 0;
}
  
