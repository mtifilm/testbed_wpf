// hey emacs, this is -*-c++-*-

#ifndef CLIPTRACKSTRATEGYH
#define CLIPTRACKSTRATEGYH


#include "TrackStrategy.h"
#include "Clip3TimeTrack.h"


#include "Clip3.h"
//class CClip;


/** @brief strategy to provide track info to CTimeLine via CClip
 *
 */
class MTI_CLIPLIB_API CClipTrackStrategy : public CTrackStrategy
{
 public:
  /**
   * freeing pClip is the caller's responsibility
   */
  CClipTrackStrategy(ClipSharedPtr &pClip, int iVideoSelect, int iAudioSelect,
                     int iFramingSelect);

  virtual ~CClipTrackStrategy() {}

  virtual int getAudioClipFrame(int iFrame);
  virtual int getAudioFrameCount();


  /**
   * returns true when value is interpolated; returns false when value
   * is not interpolated
   */
  virtual bool getAudioLtc(float fFrameArg, bool bRound, int iOffset,
                           string &strAudioLtc);
  virtual int getHandleCount();


  /**
   * returns true is the key kode is interpolated; returns false if
   * key kode is not interpolated.
   */
  virtual bool getTelecineKeykode(int iFrame, EKeykodeFormat iFormat,
                                  int iPerfOffset,
                                  string &strKeykode,
                                  EKeykodeFilmDimension *ipFilmDimension);
  virtual void getVideoClipFrame(int iFrame, int &iClipFrame,
                                 int &iClipFramePartial);
  virtual int getVideoFrameCount();
  virtual void getTelecineTC (int iFrame, string &strTC);

 private:
  ClipSharedPtr mpClip;
  int mVideoSelect;
  int mAudioSelect;
  int mFramingSelect;
};

#endif
