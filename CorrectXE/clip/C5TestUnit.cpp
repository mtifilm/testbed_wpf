//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "C5TestUnit.h"
#include "IniFile.h"
#include "Clip5Parser.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
void ClearMemo()
{
   Form1->Memo1->Clear();
}

void AddLine(const char *str)
{
   Form1->Memo1->Lines->Add(str);
}

//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
   : TForm(Owner)
{
   C5TestSetup(ClearMemo, AddLine);

}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
   if (OpenDialog1->Execute())
      Memo1->Lines->LoadFromFile(OpenDialog1->FileName);
   else
      Memo1->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
   if (OpenDialog1->Execute())
      ParseTrack(OpenDialog1->FileName.c_str());
//      Memo1->Lines->LoadFromFile(OpenDialog1->FileName);
   else
      Memo1->Clear();

}
//---------------------------------------------------------------------------

