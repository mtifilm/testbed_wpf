// RawFileIO.h: interface for the CRawFileIO class.
//
//////////////////////////////////////////////////////////////////////

#ifndef RAWFILEIO_H
#define RAWFILEIO_H

#include <string>
using std::string;

#if defined(_WIN32)
#include <windows.h>
#endif

#include "machine.h"
#include "IniFile.h"

//////////////////////////////////////////////////////////////////////

// Maximum overlapped I/Os allowed by CRawFileIO.  This is
// arbitrary up to the maximum allowed by MS Windows
#define RF_MAX_OVERLAPPED_IO_COUNT  16

//////////////////////////////////////////////////////////////////////

class CRawFileIO
{
public:
   CRawFileIO(int newOverlappedReadCount, int newOverlappedWriteCount,
              int newFileHandleIndex, int newTimingReport);
   virtual ~CRawFileIO();

   int open(const string& newFileName);
   int openReadOnly(const string& newFileName);
   int close();

   int read(unsigned char* dataBuffer, MTI_INT64 offset, int size);
   int write(const unsigned char* dataBuffer, MTI_INT64 offset, int size);

   int readMulti(unsigned char* dataBuffer[], MTI_INT64 offset[], int size[],
                 int count);
   int writeMulti(unsigned char* dataBuffer[], MTI_INT64 offset[],
                  int size[], int count);

private:
   int m_overlappedReadCount;   // Maximum number of overlapped read operations
   int m_overlappedWriteCount;  // Maximum number of overlapped write operations
                                // If 1, then I/O is non-overlapped
   bool m_doOverlappedIO;

#if defined(__sgi) || defined(__linux)        // UNIX
   int m_handle;
#elif defined(_WIN32)      // Windows, VC++ or BC++
   HANDLE m_handle;
   OVERLAPPED *m_overlappedArray;
   HANDLE m_overlappedEventArray[RF_MAX_OVERLAPPED_IO_COUNT];
#elif defined(_MSC_VER)     // Microsoft compilers
   int m_handle;
   OVERLAPPED *m_overlappedArray;
   HANDLE m_overlappedEventArray[RF_MAX_OVERLAPPED_IO_COUNT];
#else
#error Unknown Compiler
#endif
   CThreadLock m_rawFileIOThreadLock;

   // Reporting/debugging info
   string m_fileName;         // Video Store file name, used in error messages
   int m_fileHandleIndex;     // File handle index for this file
   int m_timingReport;        // If 1 or greater, sets the rate at which
                              // the timing is reported (1 = every frame,
                              // 2 = every other frame, etc.) If less than 1,
                              // then there is no timing reporting
   double m_totalTimeMin, m_totalTimeMax;
   double m_lockTimeMin, m_lockTimeMax;
   double m_readTimeMin, m_readTimeMax;
   int m_readCount;
};

//////////////////////////////////////////////////////////////////////

#endif // #ifndef RAWFILEIO_H
