//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ClipFileViewerUnit.h"
#include "IniFile.h"
#include <string>
using std::string;

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TClipFileViewer *ClipFileViewer;
//---------------------------------------------------------------------------
__fastcall TClipFileViewer::TClipFileViewer(TComponent* Owner)
   : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void TClipFileViewer::Clear()
{
   TextArea->Lines->Clear();

   Caption = "Clip File Viewer";
}

void TClipFileViewer::ShowFile(const string &fileName)
{
   FILE *stream;               // File Handle
   int retVal;

   // Open the file
   stream = fopen(fileName.c_str(), "rt");  // open for read of text file
   if (stream == NULL)
      {
      // ERROR: File open failed
      string errMsg = "Could not open file " + fileName
                      + "\n" + strerror(errno);
      _MTIErrorDialog(Handle, errMsg);
      return;
      }

   string captionStr = "Clip File Viewer - " + fileName;
   Caption = captionStr.c_str();

   char buffer[4096];

   // Iterate over all of the lines in the file
   while (true)
      {
      // Read a line
      if(fgets(buffer, sizeof(buffer), stream) == NULL)
         {
         // fgets returned NULL, which indicates either an end-of-file
         // or a read error
         if (feof(stream))
            break;      // End-of-file, so exit while loop
         else
            {
            // ERROR: file read failed
            string errMsg = "Could not read file " + fileName
                            + "\n" + strerror(errno);
            _MTIErrorDialog(Handle, errMsg);
            fclose(stream);
            return;
            }
         }

      size_t newStrLen = strlen(buffer);
      if (buffer[newStrLen-1] == '\n')
         buffer[newStrLen-1] = 0;           // Strip newline character

      // Create a string with leading and trailing whitespace removed
      string newLine = StringTrim(string(buffer));

      // Add the lines to the viewer
      TextArea->Lines->Add(newLine.c_str());
      }

   fclose(stream);

}
