// StructuredStream.cpp: implementation of the CStructuredStream class.
//
/*
$Header: /usr/local/filmroot/clip/source/StructuredStream.cpp,v 1.2 2003/01/29 06:07:11 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "StructuredStream.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CStructuredStream::CStructuredStream()
: endianFixupRequired(false)
{

}

CStructuredStream::~CStructuredStream()
{

}

//////////////////////////////////////////////////////////////////////
// Accessor Functions 
//////////////////////////////////////////////////////////////////////

bool CStructuredStream::getEndianFixupRequired()
{
   return endianFixupRequired;
}

//////////////////////////////////////////////////////////////////////
// Mutator Functions 
//////////////////////////////////////////////////////////////////////

void CStructuredStream::setEndianFixupRequired(bool newEndianFixupRequired)
{
   endianFixupRequired = newEndianFixupRequired;
}

#ifndef INLINE_STRUCTURED_STREAM_IO

//////////////////////////////////////////////////////////////////////
// Structured Read Functions
//////////////////////////////////////////////////////////////////////

int CStructuredStream::readInt8(MTI_INT8 *value)
{
	int status = read(value, sizeof(MTI_INT8));

   return((status == (int)sizeof(MTI_INT8)) ? 0 : CLIP_ERROR_FILE_READ_FAILED);
}

int CStructuredStream::readInt16(MTI_INT16 *value)
{
	int status = read(value, sizeof(MTI_INT16));

	if (endianFixupRequired)
		endianInt16Fixup(value);

	return((status == (int)sizeof(MTI_INT16)) ? 0 : CLIP_ERROR_FILE_READ_FAILED);
}

int CStructuredStream::readInt32(MTI_INT32 *value)
{
	int status = read(value, sizeof(MTI_INT32));

	if (endianFixupRequired)
		endianInt32Fixup(value);

	return((status == (int)sizeof(MTI_INT32)) ? 0 : CLIP_ERROR_FILE_READ_FAILED);
}

int CStructuredStream::readInt64(MTI_INT64 *value)
{
   int status = read(value, sizeof(MTI_INT64));

   if (endianFixupRequired)
		endianInt64Fixup(value);

   return((status == (int)sizeof(MTI_INT64)) ? 0 : CLIP_ERROR_FILE_READ_FAILED);
}

int CStructuredStream::readUInt8(MTI_UINT8 *value)
{
   int status = read(value, sizeof(MTI_UINT8));

   return((status == (int)sizeof(MTI_UINT8)) ? 0 : CLIP_ERROR_FILE_READ_FAILED);
}

int CStructuredStream::readUInt16(MTI_UINT16 *value)
{
   int status = read(value, sizeof(MTI_UINT16));

   if (endianFixupRequired)
		endianUInt16Fixup(value);

   return((status == (int)sizeof(MTI_UINT16)) ? 0 : CLIP_ERROR_FILE_READ_FAILED);
}

int CStructuredStream::readUInt32(MTI_UINT32 *value)
{
   int status = read(value, sizeof(MTI_UINT32));

   if (endianFixupRequired)
      endianUInt32Fixup(value);

	return((status == (int)sizeof(MTI_UINT32)) ? 0 : CLIP_ERROR_FILE_READ_FAILED);
}

int CStructuredStream::readUInt64(MTI_UINT64 *value)
{
   int status = read(value, sizeof(MTI_UINT64));

   if (endianFixupRequired)
      endianUInt64Fixup(value);

   return((status == (int)sizeof(MTI_UINT64)) ? 0 : CLIP_ERROR_FILE_READ_FAILED);
}

int CStructuredStream::readReal32(MTI_REAL32 *value)
{
   int status = read(value, sizeof(MTI_REAL32));

   if (endianFixupRequired)
		endianReal32Fixup(value);

   return((status == (int)sizeof(MTI_REAL32)) ? 0 : CLIP_ERROR_FILE_READ_FAILED);
}

int CStructuredStream::readReal64(MTI_REAL64 *value)
{
   int status = read(value, sizeof(MTI_REAL64));

   if (endianFixupRequired)
      endianReal64Fixup(value);

	return((status == (int)sizeof(MTI_REAL64)) ? 0 : CLIP_ERROR_FILE_READ_FAILED);
}

//////////////////////////////////////////////////////////////////////
// Structured Write Functions
//////////////////////////////////////////////////////////////////////

int CStructuredStream::writeInt8(MTI_INT8 value)
{
   int status = write(&value, sizeof(MTI_INT8));

   return((status == (int)sizeof(MTI_INT8)) ? 0 : CLIP_ERROR_FILE_WRITE_FAILED);
}

int CStructuredStream::writeInt16(MTI_INT16 value)
{
   if (endianFixupRequired)
      endianInt16Fixup(&value);

   int status = write(&value, sizeof(MTI_INT16));

   return((status == (int)sizeof(MTI_INT16)) ? 0 : CLIP_ERROR_FILE_WRITE_FAILED);
}

int CStructuredStream::writeInt32(MTI_INT32 value)
{
	if (endianFixupRequired)
      endianInt32Fixup(&value);

   int status = write(&value, sizeof(MTI_INT32));

   return((status == (int)sizeof(MTI_INT32)) ? 0 : CLIP_ERROR_FILE_WRITE_FAILED);
}

int CStructuredStream::writeInt64(MTI_INT64 value)
{
	if (endianFixupRequired)
      endianInt64Fixup(&value);

   int status = write(&value, sizeof(MTI_INT64));

   return((status == (int)sizeof(MTI_INT64)) ? 0 : CLIP_ERROR_FILE_WRITE_FAILED);
}

int CStructuredStream::writeUInt8(MTI_UINT8 value)
{
   int status = write(&value, sizeof(MTI_UINT8));

	return((status == (int)sizeof(MTI_UINT8)) ? 0 : CLIP_ERROR_FILE_WRITE_FAILED);
}

int CStructuredStream::writeUInt16(MTI_UINT16 value)
{
   if (endianFixupRequired)
		endianUInt16Fixup(&value);

   int status = write(&value, sizeof(MTI_UINT16));

   return((status == (int)sizeof(MTI_UINT16)) ? 0 : CLIP_ERROR_FILE_WRITE_FAILED);
}

int CStructuredStream::writeUInt32(MTI_UINT32 value)
{
   if (endianFixupRequired)
      endianUInt32Fixup(&value);

	int status = write(&value, sizeof(MTI_UINT32));

   return((status == (int)sizeof(MTI_UINT32)) ? 0 : CLIP_ERROR_FILE_WRITE_FAILED);
}

int CStructuredStream::writeUInt64(MTI_UINT64 value)
{
   if (endianFixupRequired)
      endianUInt64Fixup(&value);

   int status = write(&value, sizeof(MTI_UINT64));

	return((status == (int)sizeof(MTI_UINT64)) ? 0 : CLIP_ERROR_FILE_WRITE_FAILED);
}

int CStructuredStream::writeReal32(MTI_REAL32 value)
{
   if (endianFixupRequired)
		endianReal32Fixup(&value);

   int status = write(&value, sizeof(MTI_REAL32));

   return((status == (int)sizeof(MTI_REAL32)) ? 0 : CLIP_ERROR_FILE_WRITE_FAILED);
}

int CStructuredStream::writeReal64(MTI_REAL64 value)
{
   if (endianFixupRequired)
      endianReal64Fixup(&value);

	int status = write(&value, sizeof(MTI_REAL64));

   return((status == (int)sizeof(MTI_REAL64)) ? 0 : CLIP_ERROR_FILE_WRITE_FAILED);
}

#endif

//////////////////////////////////////////////////////////////////////
// Endian Fixup Functions
//////////////////////////////////////////////////////////////////////


void CStructuredStream::reverseBytes(MTI_UINT8 *valueArray, int byteCount)
{
   MTI_UINT8 *lowPtr = valueArray;
	MTI_UINT8 *hiPtr = valueArray + byteCount - 1;
	for (int i = 0; i < byteCount / 2; ++i, ++lowPtr, --hiPtr)
		{
		MTI_UINT8 temp = *lowPtr;
		*lowPtr = *hiPtr;
		*hiPtr = temp;
		}
}

void CStructuredStream::reverse2Bytes(MTI_UINT8 *valueArray)
{
   MTI_UINT8 temp = *valueArray;
   MTI_UINT8 *hiPtr = valueArray + 2 - 1;
   *valueArray = *hiPtr;
   *hiPtr = temp;
}

void CStructuredStream::reverse4Bytes(MTI_UINT8 *valueArray)
{
   MTI_UINT8 *lowPtr = valueArray;
   MTI_UINT8 *hiPtr = valueArray + 4 - 1;

   MTI_UINT8 temp = *lowPtr;
   *lowPtr++ = *hiPtr;
   *hiPtr-- = temp;

   temp = *lowPtr;
   *lowPtr = *hiPtr;
   *hiPtr = temp;
}

void CStructuredStream::reverse8Bytes(MTI_UINT8 *valueArray)
{
   MTI_UINT8 *lowPtr = valueArray;
   MTI_UINT8 *hiPtr = valueArray + 8 - 1;

   MTI_UINT8 temp = *lowPtr;
   *lowPtr++ = *hiPtr;
   *hiPtr-- = temp;

   temp = *lowPtr;
   *lowPtr++ = *hiPtr;
   *hiPtr-- = temp;

   temp = *lowPtr;
   *lowPtr++ = *hiPtr;
   *hiPtr-- = temp;

   temp = *lowPtr;
   *lowPtr = *hiPtr;
   *hiPtr = temp;
}

