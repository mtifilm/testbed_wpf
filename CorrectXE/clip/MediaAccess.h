// MediaAccess.h: interface for the CMediaAccess class.
//  
// CMediaAccess is an abstract base class for subtypes that provide
// access to the various media storage types
//
//////////////////////////////////////////////////////////////////////

#ifndef MediaAccessH
#define MediaAccessH

#include <string>

using std::string;

#include "machine.h"
#include "cliplibint.h"
#include "MediaInterface.h"
#include "MediaFrameBuffer.h"

//////////////////////////////////////////////////////////////////////
// Forward declarations

class CMediaAllocator;
class CMediaDeallocator;
class MediaFrameBuffer;

//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CMediaAccess
{
public:
   CMediaAccess(EMediaType newMediaType = MEDIA_TYPE_INVALID);
   CMediaAccess(EMediaType newMediaType,
                const string& mediaIdentifier,
                const string& historyDirectory);
   virtual ~CMediaAccess();

   bool operator==(const CMediaAccess& rhs) const;
   bool operator!=(const CMediaAccess& rhs) const;
   bool operator<(const CMediaAccess& rhs) const;
   bool operator>(const CMediaAccess& rhs) const;
   int compare(const CMediaAccess& rhs) const;

   virtual int queryFreeSpace(MTI_INT64 *totalFreeBytes,
                              MTI_INT64 *maxContiguousFreeBytes) = 0;
   virtual int queryFreeSpaceEx(const CMediaFormat& mediaFormat,
                                MTI_INT64 *totalFreeBytes,
                                MTI_INT64 *maxContiguousFreeBytes);
   virtual int queryTotalSpace(MTI_INT64 *totalBytes);

   virtual MTI_UINT32 getAdjustedFieldSize(MTI_UINT32 fieldSize) = 0;
   virtual MTI_INT64 adjustToBlocking(MTI_INT64 size, bool roundingFlag) = 0;

   virtual MTI_UINT32 getDefaultDataOffset();
   const string& getMediaIdentifier() const;   
   virtual const string& getMediaRepositoryIdentifier() const;   
   EMediaType getMediaType() const;
   const string& getHistoryDirectory() const;

   virtual int Peek(CMediaLocation &mediaLocation, string &filename,
                    MTI_INT64 &offset) = 0;

   void setMediaIdentifier(const string& newMediaIdentifier);
   void setMediaType(EMediaType newMediaType);
   void setHistoryDirectory(const string& newHistoryDirectory);

   // default implementation of supportsMediaType return true iff
   // argument mediaType is the same as the object's mediaType
   virtual bool supportsMediaType(EMediaType testMediaType) const;

   virtual bool isClipSpecific() const = 0;

   // derived classes should override renameClipMedia if they store
   // media in directory paths that include bin and/or clip names; see
   // CImageRepositoryMediaAccess for an example
   virtual int renameClipMedia(string newBinPath, string newClipName)
       { return 0; }

   // derived classes should override renameBinMedia if they store
   // media in directory paths that include bin and/or clip names; see
   // CImageRepositoryMediaAccess for an example
   virtual int renameBinMedia(string oldBinPath, string newBinPath)
       { return 0; }

   virtual MTI_UINT32 convertDataSizeToByteCount(MTI_UINT32 dataSize) const;

   virtual CMediaAllocator* allocate(const CMediaFormat& mediaFormat,
                                     const CTimecode& frame0Timecode,
                                     MTI_UINT32 frameCount) = 0;

   // Awful version clip hack because raw allocator is too stupid to
   // figure out how many fields to a film frame. For other allocators,
   // this is wrong because I don't pass on the field count, but I am
   // way, way beyond caring about pulled-down file-based clips.
   virtual CMediaAllocator* allocateOneFrame(const CMediaFormat& mediaFormat,
                                     const CTimecode& frame0Timecode,
                                     MTI_UINT32 fieldCount)
       { return allocate(mediaFormat, frame0Timecode, 1 /* frame */); }

   virtual int cancelAllocation(CMediaAllocator *mediaAllocator) = 0;

   virtual CMediaAllocator* extend(const CMediaFormat& mediaFormat,
                                   const CTimecode& frame0Timecode,
                                   MTI_UINT32 frameCount);
   virtual int cancelExtend(CMediaAllocator *mediaAllocator);

   virtual int checkSpace(const CMediaFormat& mediaFormat,
                          MTI_UINT32 frameCount) = 0;

   virtual CMediaDeallocator* createDeallocator() = 0;
   virtual int deallocate(CMediaDeallocator& deallocator) = 0;

   virtual int preload();
   virtual int CreateNewFileHandle();
   virtual int ReleaseFileHandle(int fileHandleIndex);

   virtual int read(MTI_UINT8* buffer, CMediaLocation &mediaLocation) = 0;
   virtual int read(MTI_UINT8* buffer, MTI_INT64 offset,
                    unsigned int size) = 0;
                    
   virtual int readMT(MTI_UINT8* buffer, CMediaLocation &mediaLocation,
                    int fileHandleIndex);
   virtual int readMT(MTI_UINT8* buffer, MTI_INT64 offset,
                    unsigned int size, int fileHandleIndex);

   virtual int readWithOffset(MTI_UINT8* buffer, MTI_UINT8* &offsetBuffer,
                              CMediaLocation &mediaLocation);

//	virtual bool canBeReadAsynchronously(CMediaLocation &mediaLocation);
//	virtual int startAsynchReadWithOffset(MTI_UINT8* buffer,
//													  MTI_UINT8* &offsetBuffer,
//													  CMediaLocation &mediaLocation,
//													  void* &readContext,
//													  bool dontNeedAlpha);
//	virtual bool isAsynchReadWithOffsetDone(void* readContext);
//	virtual int finishAsynchReadWithOffset(void* &readContext,
//														MTI_UINT8* &offsetBuffer);

//	virtual bool canReadFrameBuffers(CMediaLocation &mediaLocation);
//	virtual int startAsyncFrameBufferRead(CMediaLocation& mediaLocation, void* &readContext);
//	virtual bool isAsyncFrameBufferReadDone(void* readContext);
//	virtual int finishAsynchFrameBufferRead(void* &readContext, MediaFrameBuffer* &frameBufferOut);

   virtual int readWithOffsetMT(MTI_UINT8* buffer, MTI_UINT8* &offsetBuffer,
                                CMediaLocation &mediaLocation,
                                int fileHandleIndex);
   virtual int readMultiple(int count, MTI_UINT8* bufferArray[],
                            CMediaLocation mediaLocationArray[]);
   virtual int readMultipleMT(int count, MTI_UINT8* bufferArray[],
                              CMediaLocation mediaLocationArray[],
                              int fileHandleIndex);
   virtual int readWithOffsetMultiple(int count, MTI_UINT8* bufferArray[],
                                      MTI_UINT8* offsetBufferArray[],
                                      CMediaLocation mediaLocationArray[]);
   virtual int write(const MTI_UINT8* buffer, CMediaLocation &mediaLocation) = 0;
   virtual int write(const MTI_UINT8* buffer, MTI_INT64 offset,
                     unsigned int size) = 0;
	virtual int writeMultiple(int count, MTI_UINT8* bufferArray[],
									  CMediaLocation mediaLocationArray[]);

	// New frame buffer stuff.
	virtual int fetchMediaFile(CMediaLocation &mediaLocation);
	virtual int readMediaFrameBuffer(MediaFrameBufferSharedPtr &frameBuffer, CMediaLocation &mediaLocation);
	virtual int writeMediaFrameBuffer(MediaFrameBufferSharedPtr frameBuffer, CMediaLocation &mediaLocation);

	//** Alpha Matte show/hide hack
   virtual int hack(EMediaHackCommand hackCommand,
                    CMediaLocation &mediaLocation);
   //**//

   //** Clip-to-clip copyback hack
   virtual int move(CMediaLocation &fromLocation, CMediaLocation &toLocation);
   virtual int destroy(const CMediaLocation &mediaLocation);
   //**//

   void incrRefCount();
   int decrRefCount();

   virtual string getImageFileName(const CMediaLocation &mediaLocation) const;
   virtual string getHistoryFileName(const CMediaLocation &mediaLocation) const;

   //** Stupid DPX Header Timecode Hack
   virtual void setFrame0Timecode(const CTimecode &frame0Timecode);
   //**//

   //** Clip-to-clip copyback hack
   static int moveMedia(CMediaLocation &fromLocation, // media gets destroyed
                        CMediaLocation &toLocation);
   static int copyMedia(CMediaLocation &fromLocation,
                        CMediaLocation &toLocation);
   //**//

protected:
   EMediaType mediaType;    // Media type, e.g., raw disk, filesystem, etc
   string mediaIdentifier;  // Media identifier, e.g. file name
   string historyDirectory; // History directory

private:
   int referenceCount;
};

//////////////////////////////////////////////////////////////////////

#endif // #ifndef MEDIAACCESS_H

