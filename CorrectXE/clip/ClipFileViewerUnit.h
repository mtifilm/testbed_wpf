//---------------------------------------------------------------------------

#ifndef ClipFileViewerUnitH
#define ClipFileViewerUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include <string>
using std::string;

//---------------------------------------------------------------------------
class TClipFileViewer : public TForm
{
__published:	// IDE-managed Components
   TMemo *TextArea;
private:	// User declarations
public:		// User declarations
   __fastcall TClipFileViewer(TComponent* Owner);
   void Clear();
   void ShowFile(const string &fileName);
};
//---------------------------------------------------------------------------
extern PACKAGE TClipFileViewer *ClipFileViewer;
//---------------------------------------------------------------------------
#endif
