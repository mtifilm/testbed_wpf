// FileScanner.cpp: implementation of the FileScanner class.
//
/*
$Header: /usr/local/filmroot/clip/source/FileScanner.cpp,v 1.5 2003/05/13 15:40:28 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "FileScanner.h"
#include "err_clip.h"
#include "StructuredStream.h"
#include "CommonHeaderFileRecord.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFileScanner::CFileScanner()
: stream(0), commonHeaderRecord(0),
  fileType(FR_FILE_TYPE_INVALID), fileVersion(0)
{

}

CFileScanner::~CFileScanner()
{
   delete commonHeaderRecord;
}


//////////////////////////////////////////////////////////////////////
// File Scanner Initialization
//////////////////////////////////////////////////////////////////////

// Initialize the FileScanner object for reading a file
// Stream argument refers to a StructuredStream that
// is assumed to be open already
int CFileScanner::initForReading(CStructuredStream *newStream)
{
   int retVal;

   // Save pointer to stream
   stream = newStream;

   // Read in the magic word
   if((retVal = stream->readUInt32(&magicWord)) != 0)
      return retVal;

   // Historical Note from J. Starr, May 13, 2003:
   //   The "magic word" is the first four bytes of of a binary clip file.
   //   The magic word has two purposes: 1) to verify that the file is
   //   really a MTI binary clip file and 2) to figure out if this is a
   //   big-endian or little-endian file.  The magic word can be read as
   //   four ASCII text characters by opening the binary clip file with a
   //   plain text editor.
   //
   //   The first implementation of the magic word was supposed to be "MTIX"
   //   for big-endian machines (SGI) and "XITM" for little-endian machines
   //   (Intel x86, Pentium, etc).  However, a bug in this code put "MTIX"
   //   in both big-endian and little-endian files.  This made it impossible
   //   to distinguish the endian of a file.
   //
   //   Just prior to NAB2003 the magic word code was patched to use new 
   //   magic words: "MTIY" or "YITM".  This new magic word worked fine with
   //   new software, but was incompatible with the previous releases of the
   //   software.
   //
   //   Now, in an attempt to "do things right," the magic word with
   //   revert back to "MTIX".  To distinguish big-endian from little-endian
   //   files, the first four bytes of the files will be written as:
   //      Litte-endian machine (Intel):   MTIX
   //      Big-endian machines (SGI):      XITM
   //
   //   Files with the magic words "MTIY" and "YITM" can still be read by the
   //   software, but these magic words will no longer be written.
   //
   //   For the PC this will mean:
   //     - all files can be read by this software
   //     - older software can read files created by this software
   //     - older software cannot read files with "MTIY" or "YITM"
   //
   //   For the SGI this will mean:
   //     - old files with "MTIX" magic word will crash the new software
   //     - new files with "XITM" magic word will crash the old software
   //     - new software can read files with "MTIY" or "YITM" magic words
   //
   //   To ameliorate the problems caused by this version incompatiblility
   //   on the SGI, Mike Russell has written a script that will change
   //   the magic word in existing files.

   // Validate the magic word
   if (   magicWord != FR_MAGIC_WORD_MTIX_LITTLE_ENDIAN
       && magicWord != FR_MAGIC_WORD_MTIX_BIG_ENDIAN
       && magicWord != FR_MAGIC_WORD_MTIY_LITTLE_ENDIAN
       && magicWord != FR_MAGIC_WORD_MTIY_BIG_ENDIAN)
      {
      return CLIP_ERROR_INVALID_MAGIC_WORD;
      }

   // If the endedness of the file is different than the native endedness
   // of this machine, then endian fixup is required when reading the file.
   if (magicWord == FR_MAGIC_WORD_MTIX
       || magicWord == FR_MAGIC_WORD_MTIY)
      {
      // Opened file endedness is the same as the system's native
      // endedness.  No endian fixup is required by structured stream
      stream->setEndianFixupRequired(false);
      }
   else
      {
      // Opened file endedness is different from the system's native
      // endedness.  Endian fixup is required by structured stream
      stream->setEndianFixupRequired(true);
      }

   // Read a CFileRecord, presumably a CCommonHeaderFileRecord
   CFileRecord *newRecordPtr;
   if ((retVal = CFileRecord::readRecord(*stream, &newRecordPtr)) != 0)
       return retVal;

   // Check to make sure new CFileRecord actually is a CCommonHeaderFileRecord
   //   If it is, assign pointer to member variable commonHeaderRecord
   //   If it is not, return error
   if (   !(newRecordPtr) 
       || !newRecordPtr->isRecordType(FR_RECORD_TYPE_COMMON_FILE_HEADER))
      return CLIP_ERROR_INVALID_COMMON_HEADER_RECORD;

   commonHeaderRecord = static_cast<CCommonHeaderFileRecord*>(newRecordPtr);

   fileType = EFileType(commonHeaderRecord->getFileType());
   fileVersion = commonHeaderRecord->getFileTypeVersion();

   // Now ready to start reading the specific file records
   return 0;
}

//////////////////////////////////////////////////////////////////////
// Record Reading Functions
//////////////////////////////////////////////////////////////////////

// Read the next record from stream and return pointer to 
// a new CFileRecord subtype object.  Caller's responsibility
// to delete CFileRecord object when its done with it.

int CFileScanner::readNextRecord(CFileRecord **newRecordPtr)
{
   return (CFileRecord::readRecord(*stream, newRecordPtr));
}

//////////////////////////////////////////////////////////////////////
// Accessor Functions
//////////////////////////////////////////////////////////////////////

CCommonHeaderFileRecord* CFileScanner::getCommonHeaderRecord()
{
   return commonHeaderRecord;
}

EFileType CFileScanner::getFileType() const
{
   return fileType;
}

MTI_UINT16 CFileScanner::getFileVersion() const
{
   return fileVersion;
}

CStructuredStream* CFileScanner::getStream() const
{
   return stream;
}

