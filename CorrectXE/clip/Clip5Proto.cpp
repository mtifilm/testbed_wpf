// File: Clip5Proto.cpp
//
// Clip 5 Prototype Code
//
//
//----------------------------------------------------------------------------

#include "Clip5Proto.h"
#include "BinManager.h"
#include "Clip3.h"
#include "Clip3TimeTrack.h"
#include "Clip3VideoTrack.h"
#include "Clip5Parser.h"
#include "err_clip.h"
#include "ImageFileMediaAccess.h"
#include "ImageFormat3.h"
#include "LineEngine.h"
#include "MediaAccess.h"
#include "RawAudioMediaAccess.h"
#include "MediaAllocator.h"
#include "MediaDeallocator.h"
#include "ThreadLocker.h"

#include <fstream>
#include <memory>
#include <typeinfo>
#include <math.h>

//----------------------------------------------------------------------------
// Static Data


// XML Tags and Attribute Names
static const string trackTag =                  "Track";
static const string serialNumberAttr =          "SerialNumber";
static const string trackIDAttr =               "TrackID";
static const string trackTypeAttr =             "TrackType";
static const string rangeListTag =              "RangeList";
static const string editRateAttr =              "EditRate";

static const string rangeTag =                  "Range";
static const string rangeTypeAttr =             "RangeType";
static const string contentTypeAttr =           "ContentType";
static const string itemCountAttr =             "ItemCount";
static const string dstStartAttr =              "DstStart";

static const string segmentTag =                "Segment";
static const string srcTrackIDAttr =            "SrcTrackID";
static const string srcStartAttr =              "SrcStart";

static const string pulldownTag =               "Pulldown";
static const string pulldownTypeAttr =          "PulldownType";
static const string pulldownPhaseAttr =         "PulldownPhase";

static const string mediaLocationRangeTag =     "MediaLocationBlock";
static const string mediaTypeAttr =             "MediaType";
static const string mediaIDAttr =               "MediaID";
static const string dataIndexAttr =             "DataIndex";
static const string dataSizeAttr =              "DataSize";

static const string samplesPerFieldAttr =       "SamplesPerField";

static const string mediaStatusRangeTag =       "MediaStatusBlock";
static const string mediaStatusAttr =           "MediaStatus";

static const string mediaTrackAttr =            "MediaTrack";
static const string mediaStatusTrackAttr =      "MediaStatusTrack";
static const string defaultMediaTypeAttr =      "DefaultMediaType";
static const string defaultMediaIDAttr =        "DefaultMediaID";

static const string imageFormatTag =            "ImageFormat";
static const string imageFormatSectionPrefix =  "ImageFormat";
static const string audioFormatTag =            "AudioFormat";
static const string audioFormatSectionPrefix =  "AudioFormat";

static const string rawSMPTETimecodeAttr =      "SMPTETimecode";
static const string rawSMPTEUserDataAttr =      "SMPTEUserData";
static const string timecodeFrameRateAttr =     "TimecodeFrameRate";
static const string partialFrameAttr =          "PartialFrame";

static const string keykodeCalibOffsetAttr =    "KKCalibOffset";
static const string keykodeFilmDimensionAttr =  "KKFilmDim";
static const string keykodeInterpolateFlagAttr= "KKIntFlag";
static const string keykodeStrAttr =            "KKString";


//----------------------------------------------------------------------------
// Track Types
static const CStringValueList<E5TrackType>::SInitPair<E5TrackType> trackTypeInit[] =
{
   {CLIP5_TRACK_TYPE_MEDIA,           "Media" },
   {CLIP5_TRACK_TYPE_SEQUENCE,        "Sequence" },
   {CLIP5_TRACK_TYPE_AUDIO_PROXY,     "AudioProxy"    },
   {CLIP5_TRACK_TYPE_KEYCODE,         "Keycode"       },
   {CLIP5_TRACK_TYPE_SMPTE_TIMECODE,  "SMPTETimecode" },
   {CLIP5_TRACK_TYPE_TIMECODE,        "Timecode"      },
   {CLIP5_TRACK_TYPE_MEDIA_STATUS,    "MediaStatus"   },
   {CLIP5_TRACK_TYPE_INVALID, 0}  // terminate list
};

static const CStringValueList<E5TrackType> clipTrackType(trackTypeInit);

//----------------------------------------------------------------------------

// Range Types
static const CStringValueList<E5RangeType>::SInitPair<E5RangeType> rangeTypeInit[] =
{
   {CLIP5_RANGE_TYPE_FILLER,                      "Filler"        },
   {CLIP5_RANGE_TYPE_MEDIA_LOCATION_VIDEO_STORE,  "VideoStore"    },
   {CLIP5_RANGE_TYPE_MEDIA_LOCATION_IMAGE_FILE,   "ImageFile"     },
   {CLIP5_RANGE_TYPE_MEDIA_LOCATION_AUDIO_FILE,   "AudioFile"     },
   {CLIP5_RANGE_TYPE_PULLDOWN,                    "Pulldown"      },
   {CLIP5_RANGE_TYPE_KEYCODE,                     "Keycode"       },
   {CLIP5_RANGE_TYPE_TIMECODE,                    "Timecode"      },
   {CLIP5_RANGE_TYPE_SMPTE_TIMECODE,              "SMPTETimecode" },
   {CLIP5_RANGE_TYPE_MEDIA_STATUS,                "MediaStatus"   },
   {CLIP5_RANGE_TYPE_INVALID, 0}  // terminate list
};

static const CStringValueList<E5RangeType> clipRangeType(rangeTypeInit);

//----------------------------------------------------------------------------

// Content Types
static const CStringValueList<E5ContentType>::SInitPair<E5ContentType> contentTypeInit[] =
{
   {CLIP5_CONTENT_TYPE_NONE,                       "None"            },
   {CLIP5_CONTENT_TYPE_VIDEO,                      "Video"           },
   {CLIP5_CONTENT_TYPE_AUDIO,                      "Audio"           },
   {CLIP5_CONTENT_TYPE_SMPTE_TIMECODE,             "SMPTETimecode"   },
   {CLIP5_CONTENT_TYPE_TIMECODE,                   "Timecode"        },
   {CLIP5_CONTENT_TYPE_KEYCODE,                    "Keycode"         },
   {CLIP5_CONTENT_TYPE_INVALID, 0}  // terminate list
};

static const CStringValueList<E5ContentType> clipContentType(contentTypeInit);

//----------------------------------------------------------------------------

// Media Location Type
static const CStringValueList<EMediaLocationType>::SInitPair<EMediaLocationType> mediaLocationTypeInit[] =
{
   {CLIP_MEDIA_LOCATION_TYPE_VIDEO_STORE, "VideoStore" },
   {CLIP_MEDIA_LOCATION_TYPE_IMAGE_FILE,  "ImageFile"  },
   {CLIP_MEDIA_LOCATION_TYPE_AUDIO_FILE,  "AudioFile"  },
   {CLIP_MEDIA_LOCATION_TYPE_INVALID, 0}   // terminate list
};

static const CStringValueList<EMediaLocationType> clipMediaLocationType(mediaLocationTypeInit);

//----------------------------------------------------------------------------

// Pulldown Types
static const CStringValueList<EPulldownType>::SInitPair<EPulldownType> pulldownTypeInit[] =
{
   {PULLDOWN_TYPE_3_2_FIELD, "3_2_FIELD" }, // typically NTSC
   {PULLDOWN_TYPE_3_2_FRAME, "3_2_FRAME" }, // typically 720P/60
   {PULLDOWN_TYPE_2_1,       "2_1"       }, // Interlaced Fields -> Frame
   {PULLDOWN_TYPE_1_1,       "1_1"       }, // Progressive Frame or single field
   {PULLDOWN_TYPE_INVALID, 0}  // terminate list
};

static const CStringValueList<EPulldownType> clipPulldownType(pulldownTypeInit);


//----------------------------------------------------------------------------

// Media Status
static const CStringValueList<E5MediaStatus>::SInitPair<E5MediaStatus> mediaStatusInit[] =
{
   {CLIP5_MEDIA_STATUS_UNALLOCATED, "Unallocated" }, // Media storage never allocated
   {CLIP5_MEDIA_STATUS_ALLOCATED,   "Allocated"   }, // Media storage exists, never written
   {CLIP5_MEDIA_STATUS_WRITTEN,     "Written"     }, // Written at least once
   {CLIP5_MEDIA_STATUS_DEALLOCATED, "Deallocated" }, // Media storage used to exist, now
                                                     // does not exist
   {CLIP5_MEDIA_STATUS_INVALID, 0}  // terminate list (must be last item)
};

static const CStringValueList<E5MediaStatus> mediaStatusLookup(mediaStatusInit);

//----------------------------------------------------------------------------

C5TrackRegistry* C5TrackHandle::trackRegistry = 0;

//----------------------------------------------------------------------------
// Helper functions to deal with "null" track IDs (AKA GUIDs)

static const string nullGUIDString = "00000000-0000-0000-0000-000000000000";
static guid_t *nullGUID = 0;

static void MakeNullGUID()
{
   if (nullGUID == nullptr)
      {
      nullGUID = new guid_t;
      guid_from_string(*nullGUID, nullGUIDString);
      }
}

guid_t GetNullGUID()
{
   if (nullGUID == nullptr)
      MakeNullGUID();

   return *nullGUID;
}

bool IsNullGUID(const guid_t &guid)
{
   if (nullGUID == nullptr)
      MakeNullGUID();

   return (guid_compare(guid, *nullGUID) == 0);
}

string GetNullGUIDStr()
{
   return nullGUIDString;
}

//----------------------------------------------------------------------------

void InitClip5()
{
//   clipTrackType = new CStringValueList<E5TrackType>(trackTypeInit);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

C5VideoStoreRange::C5VideoStoreRange()
{
}

C5VideoStoreRange::C5VideoStoreRange(const C5VideoStoreRange &src)
 : C5MediaLocationRange(src)
{
}

C5VideoStoreRange::C5VideoStoreRange(const CMediaLocation &newMediaLocation,
                                     int newItemCount)
{
   startMediaLocation = newMediaLocation;
   itemCount = newItemCount;
}

C5VideoStoreRange::~C5VideoStoreRange()
{
}

// ---------------------------------------------------------------------------

C5VideoStoreRange& C5VideoStoreRange::operator=(const C5VideoStoreRange &rhs)
{
   if (this == &rhs)
      return *this;  // avoid self-assign

   C5MediaLocationRange::operator=(rhs);

   return *this;
}

// ---------------------------------------------------------------------------
// "Virtual" assignment operator

C5MediaLocationRange& C5VideoStoreRange::assign(const C5MediaLocationRange& src)
{
   if (this == &src)
      return *this;  // avoid self-assign

   // If the types do not match, throw an exception.  This should
   // never happen unless there is a coding or logic error
   if(GetRangeType() != src.GetRangeType())
      throw std::bad_cast();
      
   *this = static_cast<const C5VideoStoreRange&>(src);

   return *this;
}

// ---------------------------------------------------------------------------

CMediaLocation C5VideoStoreRange::GetMediaLocation(int fieldIndex)
{
   CMediaLocation mediaLocation;
   mediaLocation.setInvalid();

   if (fieldIndex < 0 || fieldIndex >= itemCount)
      {
      // Caller's field index is out-of-range
      return mediaLocation;
      }

   MTI_UINT32 fieldSize = startMediaLocation.getDataSize();
   MTI_INT64 dataIndex = startMediaLocation.getDataIndex()
                         + (MTI_INT64)fieldSize * fieldIndex;
   mediaLocation.setDataIndex(dataIndex);
   mediaLocation.setDataSize(fieldSize);
   mediaLocation.setMediaAccess(startMediaLocation.getMediaAccess());
   // TBD: Set media location flags???

   return mediaLocation;
}

// -----------------------------------------------------------------------

void C5VideoStoreRange::writeXMLStream(CXMLStream &xmlStream)
{
   C5MediaLocationRange::writeXMLStream(xmlStream);

   xmlStream << endTag();    // End Range
}

//----------------------------------------------------------------------------

void C5VideoStoreRange::Dump(ostream &ostrm)
{
   C5MediaLocationRange::Dump(ostrm);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

C5ImageFileRange::C5ImageFileRange()
 : firstFileNumber(0), minFrameDigitCount(0)
{
}

C5ImageFileRange::C5ImageFileRange(const C5ImageFileRange &src)
 : C5MediaLocationRange(src),
   firstFileNumber(0), minFrameDigitCount(0)
{
}

C5ImageFileRange::C5ImageFileRange(const CMediaLocation &newMediaLocation,
                                   int newItemCount, int newFirstFileNumber,
                                   int newMinFrameDigitCount)
 : firstFileNumber(newFirstFileNumber),
   minFrameDigitCount(newMinFrameDigitCount)
{
   startMediaLocation = newMediaLocation;
   itemCount = newItemCount;
}

C5ImageFileRange::~C5ImageFileRange()
{
}

// ---------------------------------------------------------------------------

C5ImageFileRange& C5ImageFileRange::operator=(const C5ImageFileRange &rhs)
{
   if (this == &rhs)
      return *this;  // avoid self-assign

   C5MediaLocationRange::operator=(rhs);

   return *this;
}

// ---------------------------------------------------------------------------
// "Virtual" assignment operator

C5MediaLocationRange& C5ImageFileRange::assign(const C5MediaLocationRange& src)
{
   if (this == &src)
      return *this;  // avoid self-assign

   // If the types do not match, throw an exception.  This should
   // never happen unless there is a coding or logic error
   if(GetRangeType() != src.GetRangeType())
      throw std::bad_cast();
      
   *this = static_cast<const C5ImageFileRange&>(src);

   return *this;
}

// ---------------------------------------------------------------------------

CMediaLocation C5ImageFileRange::GetMediaLocation(int fieldIndex)
{
   CMediaLocation mediaLocation;
   mediaLocation.setInvalid();

   if (fieldIndex < 0 || fieldIndex >= itemCount)
      {
      // Caller's field index is out-of-range
      return mediaLocation;
      }

   mediaLocation.setDataIndex(0);
   mediaLocation.setDataSize(startMediaLocation.getDataSize());
   mediaLocation.setMediaAccess(startMediaLocation.getMediaAccess());

   // Create the field name for the new Media Location by converting
   // the frame number to a string.  The string has at least
   // minFrameDigitCount digits, with leading zeros to fill out the string
   int fileNumber = firstFileNumber + fieldIndex;

   string fieldName =
         CImageFileMediaAccess::MakeImageFileNameDigits(minFrameDigitCount,
                                                        fileNumber);
   mediaLocation.setFieldName(fieldName.c_str());
   mediaLocation.setFieldNumber(fieldIndex);

   // TBD: Set media location flags???

   return mediaLocation;
}

// -----------------------------------------------------------------------

void C5ImageFileRange::writeXMLStream(CXMLStream &xmlStream)
{
   C5MediaLocationRange::writeXMLStream(xmlStream);

   xmlStream << endTag();    // End Range
}

//----------------------------------------------------------------------------

void C5ImageFileRange::Dump(ostream &ostrm)
{
   C5MediaLocationRange::Dump(ostrm);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

C5AudioStoreRange::C5AudioStoreRange()
 : samplesPerField(0.0)
{
}

C5AudioStoreRange::C5AudioStoreRange(const C5AudioStoreRange &src)
 : C5MediaLocationRange(src),
   samplesPerField(0.0)
{
}

C5AudioStoreRange::C5AudioStoreRange(const CMediaLocation &newMediaLocation,
                                     int newItemCount,
                                     double newSamplesPerField)
{
   startMediaLocation = newMediaLocation;
   itemCount = newItemCount;
   samplesPerField = newSamplesPerField;
}

C5AudioStoreRange::~C5AudioStoreRange()
{
}

// ---------------------------------------------------------------------------

C5AudioStoreRange& C5AudioStoreRange::operator=(const C5AudioStoreRange &rhs)
{
   if (this == &rhs)
      return *this;  // avoid self-assign

   C5MediaLocationRange::operator=(rhs);

   samplesPerField = rhs.samplesPerField;

   return *this;
}

// ---------------------------------------------------------------------------
// "Virtual" assignment operator

C5MediaLocationRange& C5AudioStoreRange::assign(const C5MediaLocationRange& src)
{
   if (this == &src)
      return *this;  // avoid self-assign

   // If the types do not match, throw an exception.  This should
   // never happen unless there is a coding or logic error
   if(GetRangeType() != src.GetRangeType())
      throw std::bad_cast();

   *this = static_cast<const C5AudioStoreRange&>(src);

   return *this;
}

// ---------------------------------------------------------------------------

CMediaLocation C5AudioStoreRange::GetMediaLocation(int fieldIndex)
{
   CMediaLocation mediaLocation;
   mediaLocation.setInvalid();

   if (fieldIndex < 0 || fieldIndex >= itemCount)
      {
      // Caller's field index is out-of-range
      return mediaLocation;
      }

   CRawAudioMediaAccess* rawAudioMediaAccess =
      dynamic_cast<CRawAudioMediaAccess*>(startMediaLocation.getMediaAccess());
   MTI_INT64 startSampleIndex = startMediaLocation.getDataIndex();
   MTI_UINT32 startFieldIndex =
      rawAudioMediaAccess->samplesToFields(0, startSampleIndex);
   MTI_INT64 sampleIndex =
      rawAudioMediaAccess->fieldsToSamples(startFieldIndex, fieldIndex);
   MTI_INT64 dataIndex = startMediaLocation.getDataIndex() + sampleIndex;
   mediaLocation.setDataIndex(dataIndex);

   MTI_INT64 samplesInFieldAtIndex =
      rawAudioMediaAccess->fieldsToSamples(startFieldIndex+fieldIndex, 1);
   mediaLocation.setDataSize(samplesInFieldAtIndex);

   mediaLocation.setMediaAccess(startMediaLocation.getMediaAccess());

   return mediaLocation;
}

// -----------------------------------------------------------------------

int C5AudioStoreRange::initFromXMLAttributes(C5XMLAttribList &attribList)
{
   int retVal;

   double newSamplesPerField = attribList.GetValueDouble(samplesPerFieldAttr, 0.0);
   if (newSamplesPerField < 1)
      {
      // ERROR: Invalid required attribute
      return -1;
      }

   samplesPerField = newSamplesPerField;

   retVal = C5MediaLocationRange::initFromXMLAttributes(attribList);
   if (retVal != 0)
      return retVal;

   return 0;

}  // initFromXMLAttributes

//----------------------------------------------------------------------------

void C5AudioStoreRange::writeXMLStream(CXMLStream &xmlStream)
{
   C5MediaLocationRange::writeXMLStream(xmlStream);

   xmlStream << attr(samplesPerFieldAttr) << samplesPerField;

   xmlStream << endTag();    // End Range
}

//----------------------------------------------------------------------------

void C5AudioStoreRange::Dump(ostream &ostrm)
{
   C5MediaLocationRange::Dump(ostrm);
}

//============================================================================
//============================================================================

C5MediaLocationRange::C5MediaLocationRange()
 : mediaType(MEDIA_TYPE_INVALID)
{
}

// Copy constructor
C5MediaLocationRange::C5MediaLocationRange(const C5MediaLocationRange &src)
 : C5Range(src), mediaType(src.mediaType), mediaIdentifier(src.mediaIdentifier),
   startMediaLocation(src.startMediaLocation)
{
}

C5MediaLocationRange::~C5MediaLocationRange()
{
   // TBD: Unregister mediaAccess???
}

// -----------------------------------------------------------------------

// Assignment Operator
C5MediaLocationRange& C5MediaLocationRange::operator=(const C5MediaLocationRange &rhs)
{
   if (this == &rhs)
      return *this;  // avoid self-assignment

   C5Range::operator=(rhs);    // super-type's assignment

   mediaType = rhs.mediaType;
   mediaIdentifier = rhs.mediaIdentifier;
   startMediaLocation = rhs.startMediaLocation;

   return *this;
}

// -----------------------------------------------------------------------

int C5MediaLocationRange::GetMediaIdentifier(string &mediaIdentifierRet) const
{
   mediaIdentifierRet = mediaIdentifier;

   return 0;
}

int C5MediaLocationRange::GetMediaType(EMediaType &mediaTypeRet) const
{
   mediaTypeRet = mediaType;

   return 0;
}

MTI_UINT32 C5MediaLocationRange::getMaxPaddedFieldByteCount(int index,
                                                            int itemCount)
{
   return startMediaLocation.getDataByteSize();
}

E5MediaStatus C5MediaLocationRange::GetMediaStatus(int index) const
{
   return CLIP5_MEDIA_STATUS_INVALID;
}

CMediaLocation C5MediaLocationRange::GetStartMediaLocation() const
{
   return startMediaLocation;
}

// -----------------------------------------------------------------------

void C5MediaLocationRange::SetStartMediaLocation(const CMediaLocation &newMediaLocation)
{
   startMediaLocation = newMediaLocation;
}

// -----------------------------------------------------------------------

int C5MediaLocationRange::RegisterMediaAccess(CMediaFormat &mediaFormat)
{
   int retVal;
   CMediaInterface mediaInterface;
   CMediaAccess *mediaAccess;
   mediaAccess = mediaInterface.registerMediaAccess(mediaType,
                                                    mediaIdentifier,
                                                    "hist_dir_registerMediaAccess",
                                                    mediaFormat);
   if (mediaAccess == nullptr)
      return -1; // Error: could not get a media access object

   startMediaLocation.setMediaAccess(mediaAccess);

   return 0;
}

// -----------------------------------------------------------------------

int C5MediaLocationRange::ReadField(int index, MTI_UINT8* buffer)
{
   CMediaLocation mediaLocation = GetMediaLocation(index);

   // TBD: Error checking on returned mediaLocation

   return mediaLocation.read(buffer);
}

int C5MediaLocationRange::ReadFieldMT(int index, MTI_UINT8* buffer,
                                      int fileHandleIndex)
{
   CMediaLocation mediaLocation = GetMediaLocation(index);

   // TBD: Error checking on returned mediaLocation

   return mediaLocation.read(buffer, fileHandleIndex);
}

int C5MediaLocationRange::WriteField(int index, MTI_UINT8* buffer)
{
   CMediaLocation mediaLocation = GetMediaLocation(index);

   // TBD: Error checking on returned mediaLocation

   return mediaLocation.write(buffer);
}

// -----------------------------------------------------------------------

int C5MediaLocationRange::initFromXMLAttributes(C5XMLAttribList &attribList)
{
   int retVal;
   string valueString;
   string emptyString;
   string invalidValueString = "INVALID";

   // Media Type
   if (!attribList.DoesNameExist(mediaTypeAttr))
      {
      // ERROR: missing required attribute
      return -1;
      }
   valueString = attribList.GetValueString(mediaTypeAttr, invalidValueString);
   EMediaType newMediaType = CMediaInterface::queryMediaType(valueString);
   if (newMediaType == MEDIA_TYPE_INVALID)
      {
      // ERROR: invalid required attribute
      return CLIP_ERROR_INVALID_MEDIA_TYPE;
      }

   // Media ID
   if (!attribList.DoesNameExist(mediaIDAttr))
      {
      // ERROR: missing required attribute
      return -1;
      }
   string newMediaIdentifier = attribList.GetValueString(mediaIDAttr, emptyString);

   // NOTE: we cannot register the media access at this point because we
   //       do not have the Image Format

   // Data Index
   if (!attribList.DoesNameExist(dataIndexAttr))
      {
      // ERROR: missing required attribute
      return -1;
      }
   MTI_INT64 dataIndex = attribList.GetValueInteger(dataIndexAttr, -1);
   if (dataIndex < 0)
      {
      // ERROR: Invalid required attribute
      return -1;
      }

   // Data Size
   if (!attribList.DoesNameExist(dataSizeAttr))
      {
      // ERROR: missing required attribute
      return -1;
      }
   MTI_UINT32 dataSize = attribList.GetValueUnsignedInteger(dataSizeAttr, 0);
   if (dataSize < 1)
      {
      // ERROR: Invalid required attribute
      return -1;
      }

   mediaType = newMediaType;
   mediaIdentifier = newMediaIdentifier;
   startMediaLocation.setDataIndex(dataIndex);
   startMediaLocation.setDataSize(dataSize);
   startMediaLocation.setMediaAccess(0);
   startMediaLocation.setFieldName(0);

   retVal = C5Range::initFromXMLAttributes(attribList);
   if (retVal != 0)
      return retVal;

   return 0;

}  // initFromXMLAttributes

// -----------------------------------------------------------------------

void C5MediaLocationRange::writeXMLStream(CXMLStream &xmlStream)
{
   C5Range::writeXMLStream(xmlStream);

   xmlStream << attr(mediaTypeAttr)
             << escape(CMediaInterface::queryMediaTypeName(startMediaLocation.getMediaAccess()->getMediaType()));
   xmlStream << attr(mediaIDAttr)
             << escape(startMediaLocation.getMediaAccess()->getMediaIdentifier());
   xmlStream << attr(dataIndexAttr) << startMediaLocation.getDataIndex();
   xmlStream << attr(dataSizeAttr) << startMediaLocation.getDataSize();
}

// -----------------------------------------------------------------------

void C5MediaLocationRange::Dump(ostream &ostrm)
{
/*
   ostrm << "Start: " << dstStartIndex;
   ostrm << " Count: " << itemCount << endl;

	startMediaLocation.dump(ostrm);
*/
}

//----------------------------------------------------------------------------

C5MediaTrack::C5MediaTrack()
 : mediaFormat(0), parsingMediaFormat(false),
   defaultMediaType(MEDIA_TYPE_INVALID)
{
}

C5MediaTrack::~C5MediaTrack()
{
   delete mediaFormat;
}

// -------------------------------------------------------------------------

string C5MediaTrack::GetDefaultMediaIdentifier() const
{
   return defaultMediaIdentifier;
}

EMediaType C5MediaTrack::GetDefaultMediaType() const
{
   return defaultMediaType;
}

int C5MediaTrack::GetFirstMediaAccess(CMediaAccess* &mediaAccess) const
{
   mediaAccess = 0;

   // Iterate over the ranges in the track, searching for the first non-filler
   for (RangeList::const_iterator iter = rangeList.begin();
        iter != rangeList.end(); ++iter)
      {
      C5Range *range = *iter;
      E5RangeType rangeType = range->GetRangeType();
      if (rangeType == CLIP5_RANGE_TYPE_MEDIA_LOCATION_VIDEO_STORE ||
          rangeType == CLIP5_RANGE_TYPE_MEDIA_LOCATION_IMAGE_FILE ||
          rangeType == CLIP5_RANGE_TYPE_MEDIA_LOCATION_AUDIO_FILE)
         {
         C5MediaLocationRange *mediaLocationRange =
                                     static_cast<C5MediaLocationRange*>(range);
         CMediaLocation mediaLocation
                                 = mediaLocationRange->GetStartMediaLocation();
         mediaAccess = mediaLocation.getMediaAccess();
         break;
         }
      }

   if (mediaAccess == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   return 0;
}

int C5MediaTrack::GetFirstMediaIdentifier(string &mediaIdentifier) const
{
   int retVal = 0;

   mediaIdentifier.erase();

   // Iterate over the ranges in the track, searching for the first non-filler
   for (RangeList::const_iterator iter = rangeList.begin();
        iter != rangeList.end(); ++iter)
      {
      C5Range *range = *iter;
      E5RangeType rangeType = range->GetRangeType();
      if (rangeType == CLIP5_RANGE_TYPE_MEDIA_LOCATION_VIDEO_STORE ||
          rangeType == CLIP5_RANGE_TYPE_MEDIA_LOCATION_IMAGE_FILE ||
          rangeType == CLIP5_RANGE_TYPE_MEDIA_LOCATION_AUDIO_FILE)
         {
         C5MediaLocationRange *mediaLocationRange =
                                     static_cast<C5MediaLocationRange*>(range);
         retVal = mediaLocationRange->GetMediaIdentifier(mediaIdentifier);
         break;
         }
      }

   if (retVal != 0 || mediaIdentifier.empty())
      {
      mediaIdentifier = GetDefaultMediaIdentifier();
      if (mediaIdentifier.empty())
         return CLIP_ERROR_NO_MEDIA;
      }

   return retVal;
}

int C5MediaTrack::GetFirstMediaType(EMediaType &mediaType) const
{
   int retVal = 0;

   mediaType = MEDIA_TYPE_INVALID;

   // Iterate over the ranges in the track, searching for the first non-filler
   for (RangeList::const_iterator iter = rangeList.begin();
        iter != rangeList.end(); ++iter)
      {
      C5Range *range = *iter;
      E5RangeType rangeType = range->GetRangeType();
      if (rangeType == CLIP5_RANGE_TYPE_MEDIA_LOCATION_VIDEO_STORE ||
          rangeType == CLIP5_RANGE_TYPE_MEDIA_LOCATION_IMAGE_FILE ||
          rangeType == CLIP5_RANGE_TYPE_MEDIA_LOCATION_AUDIO_FILE)
         {
         C5MediaLocationRange *mediaLocationRange =
                                     static_cast<C5MediaLocationRange*>(range);
         retVal = mediaLocationRange->GetMediaType(mediaType);
         break;
         }
      }

   if (retVal != 0 || mediaType == MEDIA_TYPE_INVALID)
      {
      mediaType = GetDefaultMediaType();
      if (mediaType == MEDIA_TYPE_INVALID)
         return CLIP_ERROR_INVALID_MEDIA_TYPE;
      }

   return retVal;
}

MTI_UINT32 C5MediaTrack::getMaxPaddedFieldByteCount(int index, int itemCount)
{
   int retVal;

   list<S5RangeCover> coverSet;
   retVal = FindCoveringSet(index, itemCount, coverSet);
   if (retVal != 0)
      return 0;

   MTI_UINT32 maxByteCount = 0;
   for (list<S5RangeCover>::const_iterator iter = coverSet.begin();
        iter != coverSet.end(); ++iter)
      {
      C5Range *range = (*iter).rangeHandle.GetRange();
      E5RangeType rangeType = range->GetRangeType();
      if (rangeType == CLIP5_RANGE_TYPE_FILLER)
         continue;
      else if (rangeType != CLIP5_RANGE_TYPE_MEDIA_LOCATION_VIDEO_STORE &&
               rangeType != CLIP5_RANGE_TYPE_MEDIA_LOCATION_IMAGE_FILE &&
               rangeType != CLIP5_RANGE_TYPE_MEDIA_LOCATION_AUDIO_FILE)
         return -1;

      C5MediaLocationRange *mediaLocationRange
                                  = static_cast<C5MediaLocationRange*>(range);
      int rangeIndex = (*iter).startIndex - range->GetDstStartIndex();
      int rangeItemCount = (*iter).itemCount;
      MTI_UINT32 byteCount = mediaLocationRange->getMaxPaddedFieldByteCount(
                                                               rangeIndex,
                                                               rangeItemCount);
      if (byteCount > maxByteCount)
         maxByteCount = byteCount;
      }

   return maxByteCount;

}
const CMediaFormat* C5MediaTrack::GetMediaFormat() const
{
   return mediaFormat;
}

int C5MediaTrack::GetMediaLocation(int index, CMediaLocation &mediaLocation)
{
   mediaLocation.setInvalid();
   
   if(!IsIndexValid(index))
      return CLIP_ERROR_INVALID_FRAME_NUMBER;

   C5Range *range = FindRange(index);
   if (range == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   if (range->GetRangeType() == CLIP5_RANGE_TYPE_FILLER)
      return CLIP_ERROR_NO_MEDIA;

   C5MediaLocationRange *mediaLocationRange
                                   = static_cast<C5MediaLocationRange*>(range);

   mediaLocation
     = mediaLocationRange->GetMediaLocation(index - range->GetDstStartIndex());
   if (!mediaLocation.isValid())
      return -1;

   return 0;
}

int C5MediaTrack::GetMediaLocationsAllFields(int frameIndex,
                                     vector<CMediaLocation> &mediaLocationList)
{
   int retVal;

   int fieldCount = mediaFormat->getFieldCount();

   for (int fieldIndex = 0; fieldIndex < fieldCount; ++fieldIndex)
      {
      int index = frameIndex * fieldCount + fieldIndex;
      if (!IsIndexValid(index))
         return CLIP_ERROR_INVALID_FRAME_NUMBER;

      CMediaLocation mediaLocation;
      retVal = GetMediaLocation(index, mediaLocation);
      if (retVal != 0)
         return retVal;

      // Append to caller's media location list
      mediaLocationList.push_back(mediaLocation);
      }

   return 0;
}

// -----------------------------------------------------------------------

int C5MediaTrack::PeekAtMediaAllocation(vector<SMediaAllocation> &mediaAllocList,
                                        volatile int* currentFrame,
                                        volatile bool* stopRequest)
{
   SMediaAllocation mediaAllocation;

   // Iterate over the ranges in the track
   RangeListIterator endIter = rangeList.end();
   for (RangeListIterator rangeIterator = rangeList.begin();
        rangeIterator != endIter;
        ++rangeIterator)
      {
      // Caller wants to stop processing
      if (stopRequest != 0 && *stopRequest)
         break;

      C5Range *range = *rangeIterator;

      if (currentFrame != 0)
         {
         // Hack to tell caller the frame we're working on
         *currentFrame = range->GetDstStartIndex();
         }

      if (range->GetRangeType() == CLIP5_RANGE_TYPE_FILLER)
         continue;   // Skip over filler range

      // If not a filler range, we'll assume that it is a genuine
      // media location range
      C5MediaLocationRange *mediaLocationRange
                                   = static_cast<C5MediaLocationRange*>(range);

      // Gather info for the media allocation structure
      mediaAllocation.mediaLocation = mediaLocationRange->GetStartMediaLocation();
      mediaAllocation.mediaAccess = mediaAllocation.mediaLocation.getMediaAccess();
      mediaAllocation.index = mediaAllocation.mediaLocation.getDataIndex();
      mediaAllocation.size  =
                      (MTI_INT64)mediaAllocation.mediaLocation.getDataSize() *
                       mediaLocationRange->GetItemCount();

      // Add to end of caller's list
      mediaAllocList.push_back(mediaAllocation);
      }

   return 0;

} // PeekAtMediaAllocation

int C5MediaTrack::PeekAtMediaLocation(int frameIndex, int fieldIndex,
                                      string &filename, MTI_INT64 &offset)
{
   int retVal;

   int fieldCount = mediaFormat->getFieldCount();

   if (fieldIndex < 0 || fieldIndex >= fieldCount)
      return CLIP_ERROR_INVALID_FIELD_INDEX;

   int index = frameIndex * fieldCount + fieldIndex;
   if (!IsIndexValid(index))
      return CLIP_ERROR_INVALID_FRAME_NUMBER;

   CMediaLocation mediaLocation;
   retVal = GetMediaLocation(index, mediaLocation);
   if (retVal != 0)
      return retVal;

   retVal = mediaLocation.Peek(filename, offset);
   if (retVal != 0)
      return retVal;

   return 0;

} // PeekAtMediaLocation

// ---------------------------------------------------------------------------

E5MediaStatus C5MediaTrack::GetMediaStatus(int index)
{
   C5MediaStatusTrack *mediaStatusTrack = GetMediaStatusTrack();
   if (mediaStatusTrack == nullptr)
     return CLIP5_MEDIA_STATUS_INVALID;

   return mediaStatusTrack->GetMediaStatus(index);
}

C5MediaStatusTrack* C5MediaTrack::GetMediaStatusTrack() const
{
   C5Track *tp = mediaStatusTrackHandle.GetTrack();

   if (tp->GetTrackType() != CLIP5_TRACK_TYPE_MEDIA_STATUS)
      return 0;  // Bad track type

   return static_cast<C5MediaStatusTrack*>(tp);
}

int C5MediaTrack::GetTotalFrameCount()
{
   int frameCount = GetItemCount();

   if (mediaFormat != 0)
      {
      int fieldCount = mediaFormat->getFieldCount();
      if (fieldCount > 0)
         frameCount = frameCount / fieldCount; 
      }

   return frameCount;
}

//----------------------------------------------------------------------------

int C5MediaTrack::LSF2(int startIndex, int itemCount, int &allocCount)
{
   int retVal;

   allocCount = 0;

   C5MediaStatusTrack *mediaStatusTrack = GetMediaStatusTrack();
   if (mediaStatusTrack == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   retVal = mediaStatusTrack->LSF2(startIndex, itemCount, allocCount);
   if (retVal != 0)
      return retVal;




   return 0;
}

//----------------------------------------------------------------------------

void C5MediaTrack::CopyMediaFormat(const CMediaFormat &newMediaFormat)
{
   // Copy the content of caller's newMediaFormat to this object's
   // mediaFormat.  If mediaFormat has not been allocated yet, then
   // clone the caller's newMediaFormat

   if (mediaFormat == nullptr)
      {
      mediaFormat = newMediaFormat.clone();
      }
   else
      {
      mediaFormat->assign(newMediaFormat);
      }
}

//----------------------------------------------------------------------------

void C5MediaTrack::SetDefaultMediaIdentifier(const string &newMediaIdentifier)
{
   defaultMediaIdentifier = newMediaIdentifier;
}

void C5MediaTrack::SetDefaultMediaType(EMediaType newMediaType)
{
   defaultMediaType = newMediaType;
}

int C5MediaTrack::SetMediaStatus(int index, E5MediaStatus newStatus)
{
   int retVal;

   C5MediaStatusTrack *mediaStatusTrack = GetMediaStatusTrack();
   if (mediaStatusTrack == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   retVal = mediaStatusTrack->SetMediaStatus(index, newStatus);
   if (retVal != 0)
      return retVal;

   return 0;
}

int C5MediaTrack::SetMediaStatus(int startIndex, int itemCount,
                                  E5MediaStatus newStatus,
                                  bool allowStatusDecrease,
                                  bool allowStatusIncrease)
{
   int retVal;

   C5MediaStatusTrack *mediaStatusTrack = GetMediaStatusTrack();
   if (mediaStatusTrack == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   retVal = mediaStatusTrack->SetMediaStatus(startIndex, itemCount, newStatus,
                                             allowStatusDecrease,
                                             allowStatusIncrease);
   if (retVal != 0)
      return retVal;

   return 0;
}

void C5MediaTrack::SetMediaStatusTrack(const C5TrackHandle &newMediaStatusTrack)
{
   mediaStatusTrackHandle = newMediaStatusTrack;
}

//----------------------------------------------------------------------------

int C5MediaTrack::ReadField(int index, MTI_UINT8 *buffer)
{
   int retVal;

   C5MediaLocationRange *mediaLocationRange = FindBlock(index);
   if (mediaLocationRange == nullptr)
      return -1;  // Oops, can't find it

   int blockIndex = index - mediaLocationRange->GetDstStartIndex();

   retVal = mediaLocationRange->ReadField(blockIndex, buffer);
   if (retVal != 0)
      return retVal;

   return 0;
}

int C5MediaTrack::ReadFieldMT(int index, MTI_UINT8 *buffer, int fileHandleIndex)
{
   int retVal;

   C5MediaLocationRange *mediaLocationRange = FindBlock(index);
   if (mediaLocationRange == nullptr)
      return -1;  // Oops, can't find it

   int blockIndex = index - mediaLocationRange->GetDstStartIndex();

   retVal = mediaLocationRange->ReadFieldMT(blockIndex, buffer, fileHandleIndex);
   if (retVal != 0)
      return retVal;

   return 0;
}

int C5MediaTrack::ReadMedia(int frameIndex, MTI_UINT8 *buffer[])
{
   return ReadMediaMT(frameIndex, buffer, 0);
}

int C5MediaTrack::ReadMediaMT(int frameIndex, MTI_UINT8 *buffer[],
                              int fileHandleIndex)
{
   int retVal;

   int fieldCount = mediaFormat->getFieldCount();
   int field1Index = frameIndex * fieldCount;
   int field2Index = field1Index + 1;

   // Get the media status. If media has not been written, then just
   // blacken the buffer
   E5MediaStatus worstStatus = GetMediaStatus(field1Index);
   if (fieldCount > 1)
      {
      E5MediaStatus mediaStatus2 = GetMediaStatus(field2Index);
      if (mediaStatus2 < worstStatus)
         worstStatus = mediaStatus2;
      }
   if (worstStatus != CLIP5_MEDIA_STATUS_WRITTEN)
      {
      BlackenBuffer(frameIndex, buffer);

      switch(worstStatus)
         {
         case CLIP5_MEDIA_STATUS_ALLOCATED:
            // Media storage exists, never written, return success without
            // attempting to read from media storage.
            retVal = 0;
            break;

         case CLIP5_MEDIA_STATUS_NO_MEDIA:
         case CLIP5_MEDIA_STATUS_UNALLOCATED:
         case CLIP5_MEDIA_STATUS_DEALLOCATED:
            // Media storage does not exist, return an error.
            // Caller can decide if this is a soft or hard error.
            retVal = CLIP_ERROR_NO_MEDIA;
            break;

         case CLIP5_MEDIA_STATUS_INVALID:
         default:
            // Invalid media status value, return error code.
            retVal = CLIP_ERROR_INVALID_MEDIA_STATUS;
            break;
         }

      return  retVal;
      }

   retVal = ReadFieldMT(field1Index, buffer[0], fileHandleIndex);

   if (retVal == 0 && fieldCount > 1)
      {
      retVal = ReadFieldMT(field2Index, buffer[1], fileHandleIndex);
      }

   if (retVal != 0)
      {
      BlackenBuffer(frameIndex, buffer);
      return retVal;
      }

   return 0;
}

int C5MediaTrack::ReadMediaOneField(int frameIndex, int fieldIndex,
                                    MTI_UINT8 *buffer)
{
   return ReadMediaOneFieldMT(frameIndex, fieldIndex, buffer, 0);

}

int C5MediaTrack::ReadMediaOneFieldMT(int frameIndex, int fieldIndex,
                                      MTI_UINT8 *buffer, int fileHandleIndex)
{
   int retVal;

   MTI_UINT8 *blackenBuffer[2];
   blackenBuffer[0] = blackenBuffer[1] = buffer;

   int fieldCount = mediaFormat->getFieldCount();

   if (fieldIndex < 0 || fieldIndex >= fieldCount)
      return CLIP_ERROR_INVALID_FIELD_INDEX;

   int readIndex = frameIndex * fieldCount + fieldIndex;
   if (!IsIndexValid(readIndex))
      return CLIP_ERROR_INVALID_FRAME_NUMBER;

   // Get the media status. If media has not been written, then just
   // blacken the buffer
   E5MediaStatus mediaStatus = GetMediaStatus(readIndex);
   if (mediaStatus != CLIP5_MEDIA_STATUS_WRITTEN)
      {
      BlackenBuffer(frameIndex, blackenBuffer);

      switch(mediaStatus)
         {
         case CLIP5_MEDIA_STATUS_ALLOCATED:
            // Media storage exists, never written, return success without
            // attempting to read from media storage.
            retVal = 0;
            break;
         case CLIP5_MEDIA_STATUS_NO_MEDIA:
         case CLIP5_MEDIA_STATUS_UNALLOCATED:
         case CLIP5_MEDIA_STATUS_DEALLOCATED:
            // Media storage does not exist, return an error.
            // Caller can decide if this is a soft or hard error.
            retVal = CLIP_ERROR_NO_MEDIA;
            break;
         case CLIP5_MEDIA_STATUS_INVALID:
         default:
            // Invalid media status value, return error code.
            retVal = CLIP_ERROR_INVALID_MEDIA_STATUS;
            break;
         }

      return  retVal;
      }

   retVal = ReadFieldMT(readIndex, buffer, fileHandleIndex);
   if (retVal != 0)
      {
      BlackenBuffer(frameIndex, blackenBuffer);
      return retVal;
      }

   return 0;
}

// -------------------------------------------------------------------------

int C5MediaTrack::WriteField(int index, MTI_UINT8 *buffer)
{
   int retVal;

   C5MediaLocationRange *mediaLocationRange;

   mediaLocationRange = FindBlock(index);

   if (mediaLocationRange == nullptr)
      return -1;  // Oops, can't find it

   int blockIndex = index - mediaLocationRange->GetDstStartIndex();

   retVal = mediaLocationRange->WriteField(blockIndex, buffer);
   if (retVal != 0)
      return retVal;

   // If write succeeded, then set the media status
   retVal = SetMediaStatus(index, CLIP5_MEDIA_STATUS_WRITTEN);
   if (retVal != 0)
      return retVal;

   return 0;
}

int C5MediaTrack::WriteMedia(int frameIndex, MTI_UINT8 *buffer[])
{
   int retVal;

   int fieldCount = mediaFormat->getFieldCount();

   retVal = WriteField(frameIndex * fieldCount, buffer[0]);

   if (retVal == 0 && fieldCount > 1)
      {
      retVal = WriteField(frameIndex * fieldCount + 1, buffer[1]);
      }

   if (retVal != 0)
      return retVal;

   return 0;
}

// --------------------------------------------------------------------------

int C5MediaTrack::BlackenBuffer(int index, MTI_UINT8 *buffer[])
{
   int retVal;

   EMediaFormatType mediaFormatType = mediaFormat->getMediaFormatType();

   if (mediaFormatType == MEDIA_FORMAT_TYPE_VIDEO3)
      {
      const CImageFormat *imageFormat = static_cast<const CImageFormat*>(GetMediaFormat());

      CLineEngineFactory lef;
      CLineEngine *lineEngine = lef.makePixelEng (*imageFormat);
      lineEngine->setExternalFrameBuffer(buffer);

      lineEngine->setFGColor (40000, 40000, 40000);
      lineEngine->setBGColor (40000, 40000, 40000);

      lineEngine->fillField();

      int iFontSize = (int)(3. * (float)imageFormat->getPixelsPerLine() / 720. + 0.5);
      lineEngine->setFontSize(iFontSize);
      lineEngine->setFGColor (10000,10000,10000);
      lineEngine->drawString(imageFormat->getPixelsPerLine()/4,
                             imageFormat->getLinesPerFrame()/4,
                             "MTI EMPTY FRAME");

#ifdef TEST_CODE
      // Draw field index on the "blackened" field
      std::ostringstream msg;
      msg << "Blacken Field " << index;
      lineEngine->drawString(imageFormat->getPixelsPerLine()/4,
                             imageFormat->getLinesPerFrame()/4 + 50,
                             msg.str().c_str());
#endif // #ifdef TEST_CODE

      CLineEngineFactory::destroyPixelEng(lineEngine);
      }
   else if (mediaFormatType == MEDIA_FORMAT_TYPE_AUDIO3)
      {
      // Audio
      }
   else
      {
      // unknown type
      }


   return 0;

} // BlackenBuffer

// --------------------------------------------------------------------------

C5MediaLocationRange* C5MediaTrack::FindBlock(int fieldIndex)
{
   C5Range *range = FindRange(fieldIndex);
   if (range == 0)
      return 0;      // block was not found

   E5RangeType rangeType = range->GetRangeType();
   if (rangeType != CLIP5_RANGE_TYPE_MEDIA_LOCATION_VIDEO_STORE &&
       rangeType != CLIP5_RANGE_TYPE_MEDIA_LOCATION_AUDIO_FILE  &&
       rangeType != CLIP5_RANGE_TYPE_MEDIA_LOCATION_AUDIO_FILE )
      return 0;

   return static_cast<C5MediaLocationRange*>(range);
}

//----------------------------------------------------------------------------

int C5MediaTrack::GatherRawVideoStore(CVideoProxy *videoProxy)
{
   CVideoFrameList *frameList = videoProxy->getVideoFrameList(0);
   if (frameList == nullptr)
      return 0;

   int currentFieldIndex = 0;
   MTI_INT64 currentEnd = 0;
   C5VideoStoreRange *currentBlock = 0;
   CMediaAccess *currentMediaAccess = 0;
   int frameCount = frameList->getMediaAllocatedFrameCount();
   for (int frameIndex = 0; frameIndex < frameCount; ++frameIndex)
      {
      CVideoFrame *frame = frameList->getFrame(frameIndex);
      int frameFieldCount = frame->getTotalFieldCount();
      for (int fieldIndex = 0; fieldIndex < frameFieldCount; ++fieldIndex)
         {
         CVideoField *field = frame->getField(fieldIndex);
         const CMediaLocation& mediaLocation = field->getMediaLocation();
         CMediaAccess *fieldMediaAccess = mediaLocation.getMediaAccess();
         MTI_INT64 dataIndex = mediaLocation.getDataIndex();
         MTI_UINT32 dataSize = mediaLocation.getDataSize();
         if (fieldMediaAccess != currentMediaAccess
             || dataIndex != currentEnd)
            {
            // Time for a new block
            currentBlock = new C5VideoStoreRange;
            AppendRange(currentBlock);
            currentBlock->SetStartMediaLocation(mediaLocation);
            currentBlock->SetDstStartIndex(currentFieldIndex);
            currentEnd = dataIndex;
            currentMediaAccess = fieldMediaAccess;
            }
         currentBlock->SetItemCount(currentBlock->GetItemCount() + 1);
         currentEnd += dataSize;
         ++currentFieldIndex;
         }
      }

   return 0;
}

// ---------------------------------------------------------------------------

void C5MediaTrack::Dump(ostream &ostrm)
{
   C5Track::Dump(ostrm);
}

// -----------------------------------------------------------------------

int C5MediaTrack::xmlParseStartElement(const string &elementName,
                                               C5XMLAttribList &attribList)
{
   int retVal;

   if (!parsingMediaFormat &&
       parsingLevel == CLIP5_PARSING_LEVEL_TOP &&
       (elementName == imageFormatTag || elementName == audioFormatTag))
      {
      // Start parsing image format tag
      parsingMediaFormat = true;
      mediaFormatAccumulator.str("");

      }
   else
      {
      // Pass on up to C5Track
      retVal = C5Track::xmlParseStartElement(elementName, attribList);
      if (retVal != 0)
         return retVal;
      }

   return 0;
}

int C5MediaTrack::xmlParseCharacters(const string &chars)
{
   int retVal;

   if (parsingMediaFormat)
      {
      mediaFormatAccumulator << chars;
      }
   else
      {
      // Pass on up to C5Track
      retVal = C5Track::xmlParseCharacters(chars);
      if (retVal != 0)
         return retVal;
      }

   return 0;
}

// -----------------------------------------------------------------------

int C5MediaTrack::xmlParseEndElement(const string &elementName)
{
   int retVal;

   // Pass on up to C5Track
   retVal = C5Track::xmlParseEndElement(elementName);
   if (retVal != 0)
      return retVal;

   if (parsingMediaFormat &&
       (elementName == imageFormatTag || elementName == audioFormatTag))
      {
      // end accumulation of media format characters
      parsingMediaFormat = false;

      // Copy the characters in the mediaFormatAccumulator to
      // an in-memory CIniFile
      std::istringstream inStream;
      inStream.str(mediaFormatAccumulator.str());
      CIniFile ifIniFile(inStream);

      if (elementName == imageFormatTag)
         {
         // Initialize CImageFormat from in-memory CIniFile
         CImageFormat tmpImageFormat;
         retVal = tmpImageFormat.readSection(&ifIniFile, imageFormatSectionPrefix);
         if (retVal != 0)
            return retVal;

         // Copy the image format to this media track
         CopyMediaFormat(tmpImageFormat);
         }
      else if (elementName == audioFormatTag)
         {
         // Initialize CAudioFormat from in-memory CIniFile
         CAudioFormat tmpAudioFormat;
         retVal = tmpAudioFormat.readSection(&ifIniFile, audioFormatSectionPrefix);
         if (retVal != 0)
            return retVal;

         // Copy the audio format to this media track
         CopyMediaFormat(tmpAudioFormat);
         }
      }
   else if (elementName == trackTag)
      {
      // We're done parsing the media location track, so now
      // register the media

      // Iterate over the media location blocks and register
      // each media type and media identifier pair
      RangeListIterator endIter = rangeList.end();
      for (RangeListIterator rangeIterator = rangeList.begin();
           rangeIterator != endIter;
           ++rangeIterator)
         {
         C5Range *range = *rangeIterator;
         E5RangeType rangeType = range->GetRangeType();
         if (rangeType != CLIP5_RANGE_TYPE_MEDIA_LOCATION_VIDEO_STORE &&
             rangeType != CLIP5_RANGE_TYPE_MEDIA_LOCATION_IMAGE_FILE &&
             rangeType != CLIP5_RANGE_TYPE_MEDIA_LOCATION_AUDIO_FILE)
            return -1;  // bad range type, shouldn't ever happen
         C5MediaLocationRange* mediaLocationRange =
                                     static_cast<C5MediaLocationRange*>(range);
         retVal = mediaLocationRange->RegisterMediaAccess(*mediaFormat);
         if (retVal != 0)
            return retVal;
         }
      }
   else
      {
      // Pass on up to C5Track
      retVal = C5Track::xmlParseEndElement(elementName);
      if (retVal != 0)
         return retVal;
      }

   return 0;
}

// -----------------------------------------------------------------------

int C5MediaTrack::initFromXMLAttributes(C5XMLAttribList &attribList)
{
   int retVal;
   string valueString, emptyString;
   string invalidValueString = "INVALID";

   // mediaStatusTrack ID
   if (!attribList.DoesNameExist(mediaStatusTrackAttr))
      {
      // ERROR: missing required attribute
      return -1;   // TBD: real error code
      }
   valueString = attribList.GetValueString(mediaStatusTrackAttr, invalidValueString);
   mediaStatusTrackHandle.SetTrackID(valueString);   // if the string is not a legitimate
                                             // GUID string, then the track handle
                                             // does not reference a track
   // TBD: error detection to distinguish between a bad GUID and case
   //      where there is no source track

   // Media Type
   if (!attribList.DoesNameExist(defaultMediaTypeAttr))
      {
      // ERROR: missing required attribute
      return -1;
      }
   valueString = attribList.GetValueString(defaultMediaTypeAttr, invalidValueString);
   EMediaType newMediaType = CMediaInterface::queryMediaType(valueString);
   if (newMediaType == MEDIA_TYPE_INVALID)
      {
      // ERROR: invalid required attribute
      return CLIP_ERROR_INVALID_MEDIA_TYPE;
      }

   // Media ID
   if (!attribList.DoesNameExist(defaultMediaIDAttr))
      {
      // ERROR: missing required attribute
      return -1;
      }
   string newMediaIdentifier = attribList.GetValueString(defaultMediaIDAttr,
                                                         emptyString);

   retVal = C5Track::initFromXMLAttributes(attribList);
   if (retVal != 0)
      return retVal;

   defaultMediaType = newMediaType;
   defaultMediaIdentifier = newMediaIdentifier;

   return 0;

}  // initFromXMLAttributes

// -----------------------------------------------------------------------

int C5MediaTrack::ClearMediaWritten(int startIndex, int itemCount)
{
   int retVal;

   C5MediaStatusTrack *mediaStatusTrack = GetMediaStatusTrack();
   if (mediaStatusTrack == nullptr)
      return -1;

   retVal = mediaStatusTrack->ClearMediaWritten(startIndex, itemCount);
   if (retVal != 0)
      return retVal;

   return 0;
}

// --------------------------------------------------------------------------
// Extend an existing track with a combination of allocated media
// and filler ranges.

int C5MediaTrack::ExtendMediaTrack(const string &clipPath,
                                   int totalItemExtensionCount,
                                   int allocItemExtensionCount)
{
   int retVal;

   // If the last segment of the track is a filler segment, get its
   // length and then delete it.  (This assumes that there is at most only
   // one filler and it is at the at the end of the track)
   int existingTotalItemCount = GetItemCount();
   C5RangeHandle lastRangeHandle = FindRangeHandle(existingTotalItemCount-1);
   if (!IsEnd(lastRangeHandle))
      {
      C5Range *lastRange = lastRangeHandle.GetRange();
      if (lastRange == nullptr)
         return -1;
      if (lastRange->GetRangeType() == CLIP5_RANGE_TYPE_FILLER)
         {
         RemoveRange(lastRangeHandle);
         }
      }

   int allocStartIndex = GetItemCount();

   if (allocItemExtensionCount > 0)
      {
      // Allocate space

      // Adjust the itemCount in the case of interlaced.  
      // CMediaInterface::allocateSpace argument is always frames,
      // so it gets doubled for fields
      int newItemCount = allocItemExtensionCount;
      if (GetMediaFormat()->getInterlaced())
         newItemCount /= 2;

      // Allocate media storage space
      EMediaType reallocMediaType;
      retVal = GetFirstMediaType(reallocMediaType);
      if (retVal != 0)
         return retVal;

      string reallocMediaIdentifier;
      retVal = GetFirstMediaIdentifier(reallocMediaIdentifier);
      if (retVal != 0)
         return retVal;

      CTimecode frame0Timecode(0);     // Dummy
                                    // TBD: need to make sure this does nothing
                                    //      or perhaps eliminate it

      CMediaInterface mediaInterface;
      CMediaAllocator *mediaAllocator = 0;

      if (GetContentType() == CLIP5_CONTENT_TYPE_VIDEO)
         {
         mediaAllocator = mediaInterface.allocateSpace(reallocMediaType,
                                                       reallocMediaIdentifier,
                                                       "hist_dir_ExtendMediaTrack",
                                                       *GetMediaFormat(),
                                                       frame0Timecode,
                                                       newItemCount, "");
         if (mediaAllocator == nullptr)
            return CLIP_ERROR_MEDIA_ALLOCATION_FAILED;   // allocation failed.
         }
      else if (GetContentType() == CLIP5_CONTENT_TYPE_AUDIO)
         {
         CMediaAccess *mediaAccess;
         mediaAccess = mediaInterface.registerMediaAccess(reallocMediaType,
                                                          reallocMediaIdentifier,
                                                          "hist_dir_Extend_MediaTrack",
                                                          *GetMediaFormat());
         if (mediaAccess == nullptr)
            return -1; // Error: could not get a media access object

         mediaAllocator = mediaAccess->extend(*GetMediaFormat(), frame0Timecode,
                                              newItemCount);
         if (mediaAllocator == nullptr)
            return CLIP_ERROR_MEDIA_ALLOCATION_FAILED; // ERROR: Allocation failed
         }
      else
         {
         return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR;
         }

      // Create media location  for each block
      retVal = mediaAllocator->AssignMediaStorage(*this);
      if (retVal != 0)
         return retVal;   // Assignment failed

      delete mediaAllocator;
      }

   int newAllocItemCount = GetItemCount();
   int newTotalItemCount = existingTotalItemCount + totalItemExtensionCount;

   if (newTotalItemCount > newAllocItemCount)
      {
      // Create new filler range (if anything not allocated)
      C5FillerRange *fillerRange = new C5FillerRange;
      fillerRange->SetItemCount(newTotalItemCount - newAllocItemCount);
      fillerRange->SetDstStartIndex(newAllocItemCount);

      // Add new filler range to sequence track
      AppendRange(fillerRange);
      }

   // Adjust media status
   C5MediaStatusTrack *mediaStatusTrack = GetMediaStatusTrack();
   if (mediaStatusTrack == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   if (totalItemExtensionCount > 0)
      {
      retVal = mediaStatusTrack->ExtendMediaStatusTrack(totalItemExtensionCount);
      if (retVal != 0)
         return retVal;
         }

   if (allocItemExtensionCount > 0)
      {
      retVal = SetMediaStatus(allocStartIndex, allocItemExtensionCount,
                              CLIP5_MEDIA_STATUS_ALLOCATED, false, true);
      if (retVal != 0)
         return retVal;
      }

   if (totalItemExtensionCount > 0 || allocItemExtensionCount > 0)
      {
      retVal = UpdateMediaStatusTrackFile(clipPath);
      if (retVal != 0)
         return retVal;
      }

   return 0;
   
} // C5MediaTrack::ExtendMediaTrack

// -----------------------------------------------------------------------

int C5MediaTrack::FreeMedia(const string &clipPath, int startIndex,
                            int itemCount)
{
   int retVal;

   // Loop over ranges, modifying as necessary
   list<S5RangeCover> coverSet;
   retVal = FindCoveringSet(startIndex, itemCount, coverSet);
   if (retVal != 0)
      return retVal;
   list<S5RangeCover>::iterator iter;
   list<S5RangeCover>::iterator endIter = coverSet.end();

   // Build Media Storage List
   CMediaStorageList mediaStorageList;
   for(iter = coverSet.begin(); iter != endIter; ++iter)
      {
      C5Range *range = (*iter).rangeHandle.GetRange();
      if (range->GetRangeType() == CLIP5_RANGE_TYPE_FILLER)
         continue;   // Skip over filler range

      C5MediaLocationRange *mediaLocationRange
                                   = static_cast<C5MediaLocationRange*>(range);

      EMediaType mediaType;
      mediaLocationRange->GetMediaType(mediaType);
      string mediaIdentifier;
      mediaLocationRange->GetMediaIdentifier(mediaIdentifier);
      CMediaLocation mediaLocation= mediaLocationRange->GetStartMediaLocation();
      mediaStorageList.addMediaStorage(mediaType,
                                       mediaIdentifier,
                                       "hist_dir_FreeMedia",
                                       mediaLocation.getMediaAccess());
      }

   CMediaDeallocatorList mediaDeallocatorList;
   mediaDeallocatorList.init(mediaStorageList);

   for(iter = coverSet.begin(); iter != endIter; ++iter)
      {
      C5Range *range = (*iter).rangeHandle.GetRange();
      if (range->GetRangeType() == CLIP5_RANGE_TYPE_FILLER)
         continue;   // Skip over filler range

      C5MediaLocationRange *mediaLocationRange
                                   = static_cast<C5MediaLocationRange*>(range);

      int overlapStart, overlapEnd;
      if (!range->GetRangeIntersection(startIndex, itemCount, overlapStart,
                                       overlapEnd))
         continue;   // no intersection with caller's range, so skip
                     // NOTE: should never happen, an error?

      int dstStart = range->GetDstStartIndex();
      int count1 = overlapStart - dstStart;
      int count2 = overlapEnd - overlapStart + 1;
      int count3 = dstStart + range->GetItemCount() - 1 - overlapEnd;

      C5RangeHandle nextRangeHandle = (*iter).rangeHandle.Next();

      CMediaLocation deallocStartMediaLocation;
      if (count2 > 0)
         {
         // Get start media location for range to be deallocated.
         // This must be done before the existing media location range
         // is modified or removed.
         deallocStartMediaLocation
                        = mediaLocationRange->GetMediaLocation(count1);
         }

      CMediaLocation newStartMediaLocation;
      C5MediaLocationRange *newMediaLocationRange = 0;
      if (count3 > 0)
         {
         // Get start media location for new media location range.
         // This must be done before the existing media location range
         // is modified or removed.
         newStartMediaLocation
                        = mediaLocationRange->GetMediaLocation(count1+count2);
         newMediaLocationRange = mediaLocationRange->clone();
         }

      if (count1 > 0)
         {
         // Adjust the current media status block's size
         mediaLocationRange->SetItemCount(count1);
         }
      else
         {
         // Remove the current media status block
         RemoveRange((*iter).rangeHandle);
         }
      if (count2 > 0)
         {
         // This is the range that is being deallocated.
         retVal = mediaDeallocatorList.deallocate(deallocStartMediaLocation,
                                                  count2);
         if (retVal != 0)
            return retVal;

         // Insert new filler range
         C5FillerRange *fillerRange = new C5FillerRange;
         fillerRange->SetDstStartIndex(dstStart + count1);
         fillerRange->SetItemCount(count2);
         InsertBefore(nextRangeHandle, fillerRange);
         }
      if (count3 > 0)
         {
         // Insert new media location range

         newMediaLocationRange->SetDstStartIndex(dstStart + count1 + count2);
         newMediaLocationRange->SetItemCount(count3);
         newMediaLocationRange->SetStartMediaLocation(newStartMediaLocation);
         InsertBefore(nextRangeHandle, newMediaLocationRange);
         }
      }

   // Save for later
   int statusStartIndex = startIndex;
   int statusItemCount = itemCount;

   // Merge ranges as necessary
   if (startIndex > 0)
      {
      --startIndex;
      ++itemCount;
      }
   if (startIndex + itemCount < GetItemCount())
      ++itemCount;

   retVal = FindCoveringSet(startIndex, itemCount, coverSet);
   if (retVal != 0)
      return retVal;

   iter = coverSet.begin();
   endIter = coverSet.end();
   list<S5RangeCover>::iterator nextIter;
   while(iter != endIter)
      {
      C5Range *range = (*iter).rangeHandle.GetRange();
      nextIter = iter;
      ++nextIter;

      if (range->GetRangeType() == CLIP5_RANGE_TYPE_FILLER)
         {
         int nextStart = range->GetDstStartIndex() + range->GetItemCount();

         while (nextIter != endIter)
            {
            C5Range *nextRange = (*nextIter).rangeHandle.GetRange();

            if (nextRange->GetRangeType() != CLIP5_RANGE_TYPE_FILLER)
               {
               ++nextIter;
               break;
               }

            if (nextStart == nextRange->GetDstStartIndex())
               {
               // Merge
               range->SetItemCount(range->GetItemCount()
                                   + nextRange->GetItemCount());

               // Remove
               RemoveRange((*nextIter).rangeHandle);

               ++nextIter;
               }
            else
               {
               break;
               }
            }
         }

      iter = nextIter;
      }

   // Update the Status track
   C5MediaStatusTrack *mediaStatusTrack = GetMediaStatusTrack();
   if (mediaStatusTrack == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;  // clip corrupted
   retVal = mediaStatusTrack->SetMediaStatus(statusStartIndex, statusItemCount,
                                             CLIP5_MEDIA_STATUS_DEALLOCATED,
                                             true, false);
   if (retVal != 0)
      return retVal;

   // Free the deallocated media
   retVal = mediaDeallocatorList.freeMediaStorage();
   if (retVal != 0)
      return retVal;

   // Update the files
   retVal = WriteTrackFile(clipPath);
   if (retVal != 0)
      return retVal;

   retVal = UpdateMediaStatusTrackFile(clipPath);
   if (retVal != 0)
      return retVal;

   return 0;

} // FreeMedia

// -----------------------------------------------------------------------

int C5MediaTrack::ReallocateMediaStorage(int reallocItemCount)
{
   int retVal;

   // The last range of the track must be filler and the length of
   // filler must be at least as much as the caller's itemCount
   int existingTotalItemCount = GetItemCount();
   if (existingTotalItemCount <= 0)
      return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR;  // No items, should never happen
   C5RangeHandle lastRangeHandle = FindRangeHandle(existingTotalItemCount-1);
   if (IsEnd(lastRangeHandle))
      return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR;  // No ranges, should never happen
   C5Range *lastRange = lastRangeHandle.GetRange();
   if (lastRange == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   if (lastRange->GetRangeType() != CLIP5_RANGE_TYPE_FILLER)
      return -1; // last range was not a filler

   int fillerStartIndex = lastRange->GetDstStartIndex();
   int fillerItemCount = lastRange->GetItemCount();
   if (reallocItemCount > fillerItemCount)
      return -2;

   RemoveRange(lastRangeHandle);

   // Allocate space

   // Adjust the itemCount in the case of interlaced.  CMediaInterface::allocateSpace
   // argument is always frames, so it gets doubled for fields
   int newItemCount = reallocItemCount;
   if (GetMediaFormat()->getInterlaced())
      newItemCount /= 2;

   // Allocate media storage space
   EMediaType reallocMediaType;
   retVal = GetFirstMediaType(reallocMediaType);
   if (retVal != 0)
      return retVal;
   string reallocMediaIdentifier;
   retVal = GetFirstMediaIdentifier(reallocMediaIdentifier);
   if (retVal != 0)
      return retVal;

   CTimecode frame0Timecode(0);     // Dummy
                                    // TBD: need to make sure this does nothing
                                    //      or perhaps eliminate it

   CMediaInterface mediaInterface;
   CMediaAllocator *mediaAllocator = 0;

   if (GetContentType() == CLIP5_CONTENT_TYPE_VIDEO)
      {
      mediaAllocator = mediaInterface.allocateSpace(reallocMediaType,
                                                    reallocMediaIdentifier,
                                                    "hist_dir_ReallocateMediaStorage",
                                                    *GetMediaFormat(),
                                                    frame0Timecode,
                                                    newItemCount, "");
      if (mediaAllocator == nullptr)
         return CLIP_ERROR_MEDIA_ALLOCATION_FAILED;   // allocation failed.
      }
   else if (GetContentType() == CLIP5_CONTENT_TYPE_AUDIO)
      {
      CMediaAccess *mediaAccess;
      mediaAccess = mediaInterface.registerMediaAccess(reallocMediaType,
                                                       reallocMediaIdentifier,
                                                       "hist_dir_ReallocateMediaStorage",
                                                       *GetMediaFormat());
      if (mediaAccess == nullptr)
         return -1; // Error: could not get a media access object

      mediaAllocator = mediaAccess->extend(*GetMediaFormat(), frame0Timecode,
                                           newItemCount);
      if (mediaAllocator == nullptr)
         return CLIP_ERROR_MEDIA_ALLOCATION_FAILED; // ERROR: Allocation failed
      }
   else
      {
      return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR;
      }

   // Create media location  for each block
   retVal = mediaAllocator->AssignMediaStorage(*this);
   if (retVal != 0)
      return retVal;   // Assignment failed

   delete mediaAllocator;

   // If necessary, replace filler range, albeit smaller than before
   if (reallocItemCount < fillerItemCount)
      {
      C5FillerRange *fillerRange = new C5FillerRange;
      fillerRange->SetItemCount(fillerItemCount - reallocItemCount);
      fillerRange->SetDstStartIndex(fillerStartIndex + reallocItemCount);
      AppendRange(fillerRange);
      }

   // Adjust media status
   retVal = SetMediaStatus(fillerStartIndex, reallocItemCount,
                           CLIP5_MEDIA_STATUS_ALLOCATED, false, true);
   if (retVal != 0)
      return retVal;

   return 0;

} // C5MediaTrack::ReallocateMediaStorage

// -----------------------------------------------------------------------

int C5MediaTrack::UpdateMediaStatusTrackFile(const string &clipPath)
{
   int retVal;

   if (mediaStatusTrackHandle.GetNeedsUpdate())
      {
      retVal = mediaStatusTrackHandle.WriteTrackFile(clipPath);
      if (retVal != 0)
         return retVal;
      }

   return 0;
}

int C5MediaTrack::ReloadMediaStatusTrackFile(const string &clipPath)
{
   int retVal;

   retVal = mediaStatusTrackHandle.ReloadTrackFile(clipPath);
   if (retVal != 0)
      return retVal;

   return 0;
}

// ------------------------------------------------------------------------

void C5MediaTrack::writeXMLAttributes(CXMLStream &xmlStream)
{
   C5Track::writeXMLAttributes(xmlStream);

   xmlStream << attr(mediaStatusTrackAttr)
             << guid_to_string(mediaStatusTrackHandle.GetTrackID());

   xmlStream << attr(defaultMediaTypeAttr)
             << escape(CMediaInterface::queryMediaTypeName(defaultMediaType));
   xmlStream << attr(defaultMediaIDAttr) << escape(defaultMediaIdentifier);
}

void C5MediaTrack::writeXMLStream(CXMLStream &xmlStream)
{
   C5Track::writeXMLStream(xmlStream);

   if (mediaFormat != 0)
      {
      // Check media format type, audio or image and do the right thing
      string mediaFormatTag, mediaFormatSectionPrefix;
      EMediaFormatType mediaType = mediaFormat->getMediaFormatType();
      if (mediaType == MEDIA_FORMAT_TYPE_VIDEO3)
         {
         mediaFormatTag = imageFormatTag;
         mediaFormatSectionPrefix = imageFormatSectionPrefix;
         }
      else if (mediaType == MEDIA_FORMAT_TYPE_AUDIO3)
         {
         mediaFormatTag = audioFormatTag;
         mediaFormatSectionPrefix = audioFormatSectionPrefix;
         }
      else
         {
         mediaFormatTag = "UnknowMediaFormat";
         mediaFormatSectionPrefix = mediaFormatTag;
         }


      std::istringstream dummy;
      CIniFile ifIniFile(dummy);
      mediaFormat->writeSection(&ifIniFile, mediaFormatSectionPrefix);
      std::ostringstream ostrm;
      ostrm << endl;
      ifIniFile.WriteStream(ostrm);

      xmlStream << tag(mediaFormatTag);
      xmlStream << chardata(ostrm.str());
      xmlStream << endTag();
      }

   xmlStream << endTag();
}

// ===========================================================================
// ===========================================================================
// C5AudioMediaTrack

const CAudioFormat *C5AudioMediaTrack::GetAudioFormat() const
{
   const CMediaFormat* mediaFormat = GetMediaFormat();

   if (mediaFormat == nullptr ||
       mediaFormat->getMediaFormatType() != MEDIA_FORMAT_TYPE_AUDIO3)
      {
      // TBD: this should be an exception, as it should never happen
      return 0;
      }

   return static_cast<const CAudioFormat*>(mediaFormat);
}

int C5AudioMediaTrack::GetSampleCountAtField(int frameIndex, int fieldIndex)
{
   int retVal;
   
   int fieldCount = GetAudioFormat()->getFieldCount();
   int itemIndex = frameIndex * fieldCount + fieldIndex;

   CMediaLocation tmpMediaLocation;

   retVal = GetMediaLocation(itemIndex, tmpMediaLocation);
   if (retVal != 0)
      return 0;

   return tmpMediaLocation.getDataSize();
}

int C5AudioMediaTrack::GetSampleCountAtFrame(int frameIndex)
{
   int fieldCount = GetAudioFormat()->getFieldCount();

   int sampleCount = GetSampleCountAtField(frameIndex, 0);

   if (fieldCount > 1)
      {
      sampleCount += GetSampleCountAtField(frameIndex, 1);
      }

   return sampleCount;
}

// ---------------------------------------------------------------------------

int C5AudioMediaTrack::GetMediaLocationsBySample(int frameIndex,
                                                 int sampleOffset,
                                                 MTI_INT64 sampleCount,
                                      vector<CMediaLocation> &mediaLocationList,
                                                 vector<int> &fieldsFound)
{
   int retVal;
   int fieldCount = GetAudioFormat()->getFieldCount();

   // Check that the frameNumber falls within the frame list
   if (!IsIndexValid(frameIndex * fieldCount))
      return CLIP_ERROR_INVALID_FRAME_NUMBER;

   // Check that the caller's sampleOffset falls within the caller's frame
   if (sampleOffset >= GetSampleCountAtFrame(frameIndex))
      {
      TRACE_0 (errout << "sampleOffset: " << sampleOffset
                      << " frameIndex: " << frameIndex
                      << " SampleCountAtFrame: "
                      << GetSampleCountAtFrame(frameIndex));
      return CLIP_ERROR_INVALID_AUDIO_SAMPLE_NUMBER;
      }

   // Figure out if the sampleOffset falls in the first or second field
   // in an interlaced format.  If progressive, always the first field.
   int firstField;
   if (fieldCount == 1)
      firstField = 0;
   else if (sampleOffset < GetSampleCountAtField(frameIndex, 0))
      firstField = 0;
   else
      {
      firstField = 1;
      // Adjust sampleOffset so that it is the offset from the beginning
      // of the second field (field 1)
      sampleOffset -= GetSampleCountAtField(frameIndex, 0);
      }

   // Iterate over the fields to accumulate media locations
   // that will contribute to the samples that the caller has requested
   int totalFieldCount = GetItemCount();
   bool done = false;
   bool atFirstField = true;
   for (int fieldIndex = frameIndex * fieldCount + firstField;
        !done && fieldIndex < totalFieldCount;
        ++fieldIndex)
      {
      // Remember the pointer to this field so caller can
      // check/set the "media written" flag
      fieldsFound.push_back(fieldIndex);

      CMediaLocation tmpMediaLocation;
      retVal = GetMediaLocation(fieldIndex, tmpMediaLocation);
      if (retVal != 0)
         return retVal;
         
      if (atFirstField)
         {
         // First pass, adjust tmpMediaLocation for the sampleOffset
         tmpMediaLocation.setDataIndex(tmpMediaLocation.getDataIndex()
                                       + sampleOffset);
         tmpMediaLocation.setDataSize(tmpMediaLocation.getDataSize()
                                      - sampleOffset);
         atFirstField = false;
         }
      if (tmpMediaLocation.getDataSize() < sampleCount)
         {
         // Subtract this field's contribution to the sample count
         sampleCount -= tmpMediaLocation.getDataSize();
         }
      else
         {
         // With this field we have all the samples we need.  Shorten
         // the data size of the media location if we do not need all
         // of the samples in this field
         tmpMediaLocation.setDataSize(sampleCount);
         done = true;
         }

      bool addNewMediaLocation = true;
      if (!mediaLocationList.empty())
         {
         // See if we can merge the last media location with the newest
         if (mediaLocationList.back().getMediaAccess()
                                       == tmpMediaLocation.getMediaAccess())
            {
            MTI_INT64 end = mediaLocationList.back().getDataIndex()
                            + mediaLocationList.back().getDataSize();
            if (end == tmpMediaLocation.getDataIndex())
               {
               // Append this to end of last media location
               mediaLocationList.back().setDataSize(
                                         mediaLocationList.back().getDataSize()
                                          + tmpMediaLocation.getDataSize());
               addNewMediaLocation = false;
               }
            }
         }

      if (addNewMediaLocation)
         mediaLocationList.push_back(tmpMediaLocation);
      }

   // We have iterated to the end of the frame list without getting all
   // of the samples that the caller requested.  This must be an error.
   if (!done)
      {
      // There are not enough samples available to satisfy the caller's request
      TRACE_0 (errout << "Not enough samples to satisfy the request");
      return CLIP_ERROR_INVALID_AUDIO_SAMPLE_COUNT;
      }

   return 0; // Success

} // C5AudioMediaTrack::GetMediaLocationsBySample

//------------------------------------------------------------------------
//
// Function:     readMediaBySample
//
// Description:  Reads a block of audio samples given a frame index,
//               sample offset and sample count.  The requested number
//               of samples is read into the caller's buffer, starting
//               at the sample offset from the start of the frame's
//               samples.  The block of samples may be larger than
//               a single field or frame and thus may cross frame and
//               field boundaries.  The read respects the media type and
//               location for each field, so it is safe to use with a
//               virtual media audio track.
//
//               The sample offset must be within the given frame.
//
//               This function attempts to minimize the number of media
//               reads.
//
// Arguments     int frameNumber           Frame index of starting point
//               int sampleOffset          Offset from first sample of
//                                         caller's frameNumber.
//               MTI_INT64 sampleCount     Number of samples to read
//               MTI_UINT8 *buffer         Buffer to accept audio samples
//
// Returns:      Status code: 0 = success, other than 0 is error condition
//                 CLIP_ERROR_INVALID_AUDIO_SAMPLE_NUMBER
//                     Caller's sampleOffset is not within the given frame's
//                     audio media
//                 CLIP_ERROR_INVALID_FRAME_NUMBER
//                     Caller's frameNumber is out-of-range
//               Other errors may be returned from lower level functions
//               called for reading the media
//
//------------------------------------------------------------------------
int C5AudioMediaTrack::ReadMediaBySample(int frameNumber, int sampleOffset,
                                         MTI_INT64 sampleCount,
                                         MTI_UINT8 *buffer)
{
   return ReadMediaBySampleMT(frameNumber, sampleOffset, sampleCount, buffer, 0);
}

int C5AudioMediaTrack::ReadMediaBySampleMT(int frameNumber, int sampleOffset,
                                           MTI_INT64 sampleCount,
                                           MTI_UINT8 *buffer,
                                           int fileHandleIndex)
{
   int retVal;
   vector<CMediaLocation> mediaLocationList;
   vector<int> fieldsFound;   // list of fields that will contribute
                                  // something to the read of audio media

   retVal = GetMediaLocationsBySample(frameNumber, sampleOffset, sampleCount,
                                      mediaLocationList, fieldsFound);
   if (retVal != 0)
      return retVal;

#ifndef INIT_AUDIO_FILE
   // Look at the first field that will contribute to the media read.
   // If the first field has its "Media Written" flag set, then assume
   // that all fields have had their media written.  If the first field's
   // "Media Written" flag is not set, assume that all fields have not had
   // their media written and just return a buffer filled with zeros.
   bool mediaWritten = (GetMediaStatus(fieldsFound[0]) == CLIP5_MEDIA_STATUS_WRITTEN);
#else
   bool mediaWritten = true;   // Old way, always assume audio media written
#endif

   // Iterate over the list of accumulated media locations and perform
   // media reads.
   vector<CMediaLocation>::iterator iter;
   MTI_UINT8 *bufPtr = buffer;
   for (iter = mediaLocationList.begin();
        iter != mediaLocationList.end();
        ++iter)
      {
      CMediaLocation *mediaLocation = &(*iter);

      if (mediaWritten)
         {
         retVal = mediaLocation->read(bufPtr, fileHandleIndex);
         if (retVal != 0)
            return retVal;
         }
      else
         {
         // First field's media has never been written, so just fill caller's
         // buffer with zeros without attempting to read the audio media
         memset(bufPtr, 0, mediaLocation->getDataByteSize());
         }

      // Advance pointer into caller's data buffer
      bufPtr += mediaLocation->getDataByteSize();
      }

   return 0;

}  // C5AudioMediaTrack::ReadMediaBySample

//------------------------------------------------------------------------
//
// Function:     WriteMediaBySample
//
// Description:  Writes a block of audio samples given a frame index,
//               sample offset and sample count.  The requested number
//               of samples is written from the caller's buffer, starting
//               at the sample offset from the start of the frame's
//               samples.  The block of samples may be larger than a
//               single field or frame and thus may cross frame and
//               field boundaries.  The write respects the media type and
//               location for each field, so it is safe to use with a
//               virtual media audio track.
//
//               Updates the corresponding set of audio proxy frames
//               from the frame index, sample offset, and sample count
//
//               The sample offset must be within the given frame.
//
//               This function attempts to minimize the number of media
//               writes.
//
// Arguments     int frameNumber           Frame index of starting point
//               int sampleOffset          Offset from first sample of
//                                         caller's frameNumber.
//               MTI_INT64 sampleCount     Number of samples to write
//               MTI_UINT8 *buffer         Buffer to caller's audio samples
//                                         interleaved by channel (0, 1, 2, etc)
//               CTimeTrack *audioProxy    -> audio proxy time track (0 if none)
//
// Returns:      Status code: 0 = success, other than 0 is error condition
//                 CLIP_ERROR_INVALID_AUDIO_SAMPLE_NUMBER
//                     Caller's sampleOffset is not within the given frame's
//                     audio media
//                 CLIP_ERROR_INVALID_FRAME_NUMBER
//                       Caller's frameNumber is out-of-range
//               Other errors may be returned from lower level functions
//               called for writing the media
//
//------------------------------------------------------------------------
int C5AudioMediaTrack::WriteMediaBySample(int frameNumber, int sampleOffset,
                                          MTI_INT64 sampleCount,
                                          MTI_UINT8 *buffer,
                                          CTimeTrack *ttpAudioProxy)
{
   int retVal;
   vector<CMediaLocation> mediaLocationList;
   vector<int> fieldsFound;   // list of fields that will be written

   retVal = GetMediaLocationsBySample(frameNumber, sampleOffset, sampleCount,
                                      mediaLocationList, fieldsFound);
   if (retVal != 0)
      return retVal;

   // Iterate over the list of accumulated media locations and perform
   // media writes.
   vector<CMediaLocation>::iterator iter;
   MTI_UINT8 *bufPtr = buffer;
   for (iter = mediaLocationList.begin();
        iter != mediaLocationList.end();
        ++iter)
      {
      CMediaLocation *mediaLocation = &(*iter);

      retVal = mediaLocation->write(bufPtr);
      if (retVal != 0)
         return retVal;

      // Advance pointer into caller's data buffer
      bufPtr += mediaLocation->getDataByteSize();
      }

   // For all of the CAudioFields that contributed to the above media writes,
   // set the "Media Written" flag in the field list.  Note that is results in
   // fields being set as "Media Written" even though the media for that field
   // is only partially written
   vector<int>::iterator fieldIter;
   for (fieldIter = fieldsFound.begin();
        fieldIter != fieldsFound.end();
        ++fieldIter)
      {
      retVal = SetMediaStatus((*fieldIter), CLIP5_MEDIA_STATUS_WRITTEN);
      if (retVal != 0)
         return retVal;
      }

// NOTE: The following code was copied from CAudioFrameList::writeMediaBySample.
//       Why is this code buried here instead of being in the CTimeTrack
//       class with the other audio proxy code?  - JS

   // if no audio proxy we're done
   if (ttpAudioProxy == nullptr) return 0;

   // at this point we know that the call to
   // GetMediaLocationsBySample went thru OK

   // for each frame until samples exhausted do
   //   get frame from audio proxy timetrack
   //   get frame's sample count
   //   for each sample this frame do
   //      derive subframe index
   //      if sample is first for this subframe do
   //         min = sample; max = sample;
   //      else
   //         update min and max
   //   next sample (done?)
   // next frame

   const CAudioFormat *curFormat = GetAudioFormat();

   int totalFrameCount = GetItemCount() * curFormat->getFieldCount();

   int totalChannels = curFormat->getChannelCount();

   int curSampleOffset = sampleOffset;
   int samplesRemaining = sampleCount;

   MTI_UINT8 *curSamplePt8;
   MTI_INT16 curSampleValue;

   switch(curFormat->getSampleDataSize()) {

      case 16: // 16-bits per sample

         curSamplePt8 = buffer;

         for (int curFrameNumber = frameNumber; curFrameNumber < totalFrameCount;
              ++curFrameNumber ) {

            // set up an array to track filling of proxy subframes
            bool binFirst[MAX_AUDIO_PROXY_SAMPLES];
            for (int i=0;i<MAX_AUDIO_PROXY_SAMPLES;i++)
               binFirst[i] = false;

            // get the pointer to the current audio proxy frame
            CAudioProxyFrame *curFrame = ttpAudioProxy->getAudioProxyFrame(curFrameNumber);

            // this many samples for the frame (~ 48000 / fps)
            int curFrameSampleCount = GetSampleCountAtFrame(curFrameNumber);

            while (curSampleOffset < curFrameSampleCount) { // within the frame

               // binIndex = 0, 1, 2,  , MAX_AUDIO_PROXY_SAMPLES-1
               int binIndex = MAX_AUDIO_PROXY_SAMPLES * curSampleOffset /
                              curFrameSampleCount;

               if (!binFirst[binIndex]) { // 1st of bin

                  for (int ichan=0;ichan<totalChannels;ichan++) {

                     // big-endian
                     curSampleValue = (curSamplePt8[2*ichan]<<8) +
                                      (curSamplePt8[2*ichan+1]);

                     curFrame->setMinMaxAudio(binIndex, ichan, curSampleValue, curSampleValue);
                  }

                  // mark the bin
                  binFirst[binIndex] = true;
               }
               else {                     // 2nd or later of bin

                  for (int ichan=0;ichan<totalChannels;ichan++) {

                     // big-endian - grab all 16 bits
                     curSampleValue = (curSamplePt8[2*ichan]<<8) +
                                      (curSamplePt8[2*ichan+1]);

                     curFrame->addSampleAudio(binIndex, ichan, curSampleValue);
                  }
               }

               curSamplePt8 += 2*totalChannels;
               curSampleOffset++;

               if (--samplesRemaining == 0) break;
            }

            curFrame->setNeedsUpdate(true);

            if (samplesRemaining == 0) break;

            // set for next frame
            curSampleOffset = 0;
         }

      break;

      case 32: // 32-bits per sample

         curSamplePt8 = buffer;

         for (int curFrameNumber = frameNumber; curFrameNumber < totalFrameCount;
              ++curFrameNumber ) {

            // set up an array to track filling of proxy subframes
            bool binFirst[MAX_AUDIO_PROXY_SAMPLES];
            for (int i=0;i<MAX_AUDIO_PROXY_SAMPLES;i++)
               binFirst[i] = false;

            // get the pointer to the current audio proxy frame
            CAudioProxyFrame *curFrame = ttpAudioProxy->getAudioProxyFrame(curFrameNumber);

            // this many samples for the frame (~ 48000 / fps)
            int curFrameSampleCount = GetSampleCountAtFrame(curFrameNumber);

            while (curSampleOffset < curFrameSampleCount) { // within the frame

               // binIndex = 0, 1, 2,  , MAX_AUDIO_PROXY_SAMPLES-1
               int binIndex = MAX_AUDIO_PROXY_SAMPLES * curSampleOffset /
                              curFrameSampleCount;

               if (!binFirst[binIndex]) { // 1st of bin

                  for (int ichan=0;ichan<totalChannels;ichan++) {

                     // big-endian - grab the hi 16 bits out of 32
                     curSampleValue = (curSamplePt8[4*ichan]<<8) +
                                      (curSamplePt8[4*ichan+1]);

                     curFrame->setMinMaxAudio(binIndex, ichan,
                               curSampleValue, curSampleValue);
                  }

                  binFirst[binIndex] = true;
               }
               else {                     // 2nd or later of bin

                  for (int ichan=0;ichan<totalChannels;ichan++) {

                     // big-endian
                     curSampleValue = (curSamplePt8[4*ichan]<<24) +
                                      (curSamplePt8[4*ichan+1]<<16)+
                                      (curSamplePt8[4*ichan+2]<<8)+
                                      (curSamplePt8[4*ichan+3]);

                     curFrame->addSampleAudio(binIndex, ichan, curSampleValue);
                  }
               }

               curSamplePt8 += 4*totalChannels;
               curSampleOffset++;

               if (--samplesRemaining == 0) break;
            }

            curFrame->setNeedsUpdate(true);

            if (samplesRemaining == 0) break;

            // set for next frame
            curSampleOffset = 0;
         }

      break;
   }

   return 0;

} // C5AudioTrack::WriteMediaBySample

// ===========================================================================
// ===========================================================================
// C5VideoMediaTrack
//
// The C5VideoSequenceTrack provides a video-specific interface
// to the C5SequenceTrack class.

const CImageFormat *C5VideoMediaTrack::GetImageFormat() const
{
   const CMediaFormat* mediaFormat = GetMediaFormat();

   if (mediaFormat == nullptr ||
       mediaFormat->getMediaFormatType() != MEDIA_FORMAT_TYPE_VIDEO3)
      {
      // TBD: this should be an exception, as it should never happen
      return 0;
      }

   return static_cast<const CImageFormat*>(mediaFormat);
}

// ===========================================================================

C5Range::C5Range()
 : dstStartIndex(0), itemCount(0)
{
}

// Copy Constructor
C5Range::C5Range(const C5Range &src)
 : dstStartIndex(src.dstStartIndex), itemCount(src.itemCount)
{
}

C5Range::~C5Range()
{
}

//---------------------------------------------------------------------------

// Assignment Operator
C5Range& C5Range::operator=(const C5Range& rhs)
{
   if (this == &rhs)
      return *this;     // avoid self-assignment

   dstStartIndex = rhs.dstStartIndex;
   itemCount = rhs.itemCount;

   return *this;
}

//---------------------------------------------------------------------------

int C5Range::GetDstStartIndex() const
{
   return dstStartIndex;
}

int C5Range::GetItemCount() const
{
   return itemCount;
}

bool C5Range::GetRangeIntersection(int startIndexArg, int itemCountArg,
                                   int &overlapStart, int &overlapEnd)
{
   int dstEndIndex = dstStartIndex + itemCount - 1;
   int endIndex = startIndexArg + itemCountArg - 1;

   overlapStart = std::max(startIndexArg, dstStartIndex);
   overlapEnd = std::min(endIndex, dstEndIndex);

   bool intersectionOkay = (overlapStart <= overlapEnd);

   return intersectionOkay;
}

//---------------------------------------------------------------------------

void C5Range::SetDstStartIndex(int newIndex)
{
   dstStartIndex = newIndex;
}

void C5Range::SetItemCount(int newCount)
{
   itemCount = newCount;
}

// -----------------------------------------------------------------------

C5Pulldown* PulldownFactory(E5ContentType newContentType)
{
   C5Pulldown *newRange = 0;

   switch(newContentType)
      {
      case CLIP5_CONTENT_TYPE_VIDEO:
         newRange = new C5VideoPulldown;
         break;
      case CLIP5_CONTENT_TYPE_AUDIO:
         newRange = new C5AudioPulldown;
         break;
      case CLIP5_CONTENT_TYPE_SMPTE_TIMECODE:
         newRange = new C5SMPTETimecodePulldown;
         break;
      case CLIP5_CONTENT_TYPE_TIMECODE:
//         newRange = new C5TimecodePulldown;
         break;
      case CLIP5_CONTENT_TYPE_KEYCODE:
//         newRange = new C5KeycodePulldown;
		 break;

	  default:
	  	break;
      }

   return newRange;
}

C5Range* RangeFactory(E5RangeType newRangeType, E5ContentType newContentType)
{
   C5Range *newRange = 0;

   switch (newRangeType)
      {
      case CLIP5_RANGE_TYPE_FILLER:
         newRange = new C5FillerRange;
         break;
      case CLIP5_RANGE_TYPE_MEDIA_LOCATION_VIDEO_STORE:
         newRange = new C5VideoStoreRange;
         break;
      case CLIP5_RANGE_TYPE_MEDIA_LOCATION_IMAGE_FILE:
         newRange = new C5ImageFileRange;
         break;
      case CLIP5_RANGE_TYPE_MEDIA_LOCATION_AUDIO_FILE:
         newRange = new C5AudioStoreRange;
         break;
      case CLIP5_RANGE_TYPE_PULLDOWN:
         newRange = PulldownFactory(newContentType);
         break;
      case CLIP5_RANGE_TYPE_KEYCODE:
         newRange = new C5KeycodeRange;
         break;
#ifdef C5_TIMECODE
      case CLIP5_RANGE_TYPE_TIMECODE:
         newRange = new C5TimecodeRange;
         break;
#endif
      case CLIP5_RANGE_TYPE_SMPTE_TIMECODE:
         newRange = new C5SMPTETimecodeRange;
         break;
      case CLIP5_RANGE_TYPE_MEDIA_STATUS:
         newRange = new C5MediaStatusRange;
         break;
      default:
         break;
      }

   return newRange;
}

C5Range* C5Range::CreateRange(C5XMLAttribList &attribList)
{
   int retVal;
   string valueString;
   string invalidDefault = "INVALID";

   valueString = attribList.GetValueString(rangeTypeAttr, invalidDefault);
   E5RangeType newRangeType = clipRangeType.FindByStr(valueString,
                                                      CLIP5_RANGE_TYPE_INVALID);
   if (newRangeType == CLIP5_RANGE_TYPE_INVALID)
      {
      // ERROR: invalid range type
      TRACE_0(errout << "ERROR: C5Range::CreateRange: Invalid or missing range type"
                     << "<" << valueString << ">");
      return 0;
      }

   valueString = attribList.GetValueString(contentTypeAttr, invalidDefault);
   E5ContentType newContentType = clipContentType.FindByStr(valueString,
                                                   CLIP5_CONTENT_TYPE_INVALID);
   if (newContentType == CLIP5_CONTENT_TYPE_INVALID)
      {
      // ERROR: invalid range type
      TRACE_0(errout << "ERROR: C5Range::CreateRange: Invalid or missing content type "
                     << "<" << valueString << ">");
      return 0;
      }

   C5Range *newRange = RangeFactory(newRangeType, newContentType);
   if (newRange == nullptr)
      {
      // ERROR: could not create the range
      return 0;
      }

   retVal = newRange->initFromXMLAttributes(attribList);
   if (retVal != 0)
      {
      // ERROR: something wrong with attributes
      return 0;
      }

   return newRange;
}

int C5Range::initFromXMLAttributes(C5XMLAttribList &attribList)
{
   int retVal;

   // dstStartIndex, itemCount
   if (!attribList.DoesNameExist(dstStartAttr))
      {
      // ERROR: Missing required attribute
      return -1;
      }
   int newDstStartIndex = attribList.GetValueInteger(dstStartAttr, (MTI_INT64)(-1));
   if (newDstStartIndex < 0)
      {
      // ERROR: bad value for required attribute
      return -1;
      }

   if (!attribList.DoesNameExist(itemCountAttr))
      {
      // ERROR: missing required attribute
      return -1;
      }
   int newItemCount = attribList.GetValueInteger(itemCountAttr, (MTI_INT64)(-1));
   if (newItemCount < 0)
      {
      // ERROR: bad value for required attribute
      return -1;
      }

   dstStartIndex = newDstStartIndex;
   itemCount = newItemCount;

   return 0;
}
// -----------------------------------------------------------------------

void C5Range::writeXMLStream(CXMLStream &xmlStream)
{
   xmlStream << tag(rangeTag);
   xmlStream << attr(rangeTypeAttr) << escape(clipRangeType.FindByValue(GetRangeType()));
   xmlStream << attr(contentTypeAttr)
             << escape(clipContentType.FindByValue(GetContentType()));
   xmlStream << attr(dstStartAttr) << GetDstStartIndex();
   xmlStream << attr(itemCountAttr) << GetItemCount();

   // Note: subtype must end the tag
}

// -----------------------------------------------------------------------

void C5Range::Dump(ostream &ostrm)
{
   ostrm << " Range Type: " << clipRangeType.FindByValue(GetRangeType()) << endl;
   ostrm << " DstStartIndex: " << GetDstStartIndex() << endl;
   ostrm << " Item Count: " << GetItemCount() << endl;
}

// ===========================================================================

C5FillerRange::C5FillerRange()
{
}

C5FillerRange::~C5FillerRange()
{
}

// -----------------------------------------------------------------------

void C5FillerRange::writeXMLStream(CXMLStream &xmlStream)
{
   C5Range::writeXMLStream(xmlStream);

   xmlStream << endTag();
}

// ===========================================================================

C5Segment::C5Segment()
{
}

C5Segment::~C5Segment()
{
}

//---------------------------------------------------------------------------

C5TrackHandle C5Segment::GetSrcTrack() const
{
   return srcTrackHandle;
}

// -----------------------------------------------------------------------

void C5Segment::SetSrcStartIndex(int newSrcStartIndex)
{
   srcStartIndex = newSrcStartIndex;
}

void C5Segment::SetSrcTrack(const C5TrackHandle &newTrack)
{
   srcTrackHandle = newTrack;
}

// -----------------------------------------------------------------------

int C5Segment::initFromXMLAttributes(C5XMLAttribList &attribList)
{
   int retVal;
   string valueString;
   string invalidValueString = "INVALID";

   // srcStartIndex
   if (!attribList.DoesNameExist(srcStartAttr))
      {
      // ERROR: missing required attribute
      return -1;    // TBD: real error code
      }
   int newSrcStartIndex = attribList.GetValueInteger(srcStartAttr, -1);
   if (newSrcStartIndex < 0)
      {
      // ERROR: invalid required attribute
      return -1;   // TBD: real error code
      }

   // srcTrack ID
   if (!attribList.DoesNameExist(srcTrackIDAttr))
      {
      // ERROR: missing required attribute
      return -1;   // TBD: real error code
      }
   valueString = attribList.GetValueString(srcTrackIDAttr, invalidValueString);
   srcTrackHandle.SetTrackID(valueString);   // if the string is not a legitimate
                                             // GUID string, then the track handle
                                             // does not reference a track
   // TBD: error detection to distinguish between a bad GUID and case
   //      where there is no source track

   retVal = C5Range::initFromXMLAttributes(attribList);
   if (retVal != 0)
      return retVal;

   srcStartIndex = newSrcStartIndex;

   return 0;

}  // initFromXMLAttributes

// -----------------------------------------------------------------------

void C5Segment::writeXMLStream(CXMLStream &xmlStream)
{
   C5Range::writeXMLStream(xmlStream);

   string srcTrackIDStr;
   if (srcTrackHandle.IsInitialized())
      srcTrackIDStr = guid_to_string(srcTrackHandle.GetTrackID());
   else
      srcTrackIDStr = "None";

   xmlStream << attr(srcTrackIDAttr) << escape(srcTrackIDStr);
   xmlStream << attr(srcStartAttr) << srcStartIndex;
}

// ===========================================================================
#ifdef C5_TIMECODE

C5TimecodeRange::C5TimecodeRange()
 : startTimecode(0)
{
}

C5TimecodeRange::~C5TimecodeRange()
{
}

CTimecode C5TimecodeRange::GetTimecode(int index)
{
   return startTimecode + index;
}

void C5TimecodeRange::SetStartTimecode(const CTimecode &newStart)
{
   startTimecode = newStart;
}

// -----------------------------------------------------------------------

void C5TimecodeRange::writeXMLStream(CXMLStream &xmlStream)
{
}
#endif
// ===========================================================================
// ===========================================================================
// SMPTE Timecode Range
// ---------------------------------------------------------------------------

C5SMPTETimecodeRange::C5SMPTETimecodeRange()
 : m_startPartialFrame(0.0),
   m_timecodeFrameRate(IF_FRAME_RATE_INVALID),
   m_trackEditRate(IF_FRAME_RATE_INVALID)
{
}

C5SMPTETimecodeRange::~C5SMPTETimecodeRange()
{
}

// ---------------------------------------------------------------------------

int C5SMPTETimecodeRange::GetTimecodeFrameRate() const
{
   return (int)(CImageInfo::queryStandardFrameRate(m_timecodeFrameRate) + .5);
}

EFrameRate C5SMPTETimecodeRange::GetTimecodeFrameRateEnum() const
{
   return m_timecodeFrameRate;
}

float C5SMPTETimecodeRange::GetPartialFrame(int index)
{
   // based on the index number, calculate a new partial frame

   // TEMP: just return the start partial frame
   return m_startPartialFrame;
}


int C5SMPTETimecodeRange::GetTimecode(int index, CTimecode &timecode)
{
   int retVal;
   CSMPTETimecode smpteTC;
   float tmpPartial;

   retVal = GetTimecode(index, smpteTC, tmpPartial);
   if (retVal != 0)
      return retVal;

   timecode = smpteTC.getTimecode(GetTimecodeFrameRate());

   return 0;
}

int C5SMPTETimecodeRange::GetTimecode(int index, CSMPTETimecode &newTimecode,
                                      float &partialFrame)
{
   newTimecode.setToDummyValue();
   partialFrame = 0.0;

   if (m_startTimecode.isDummyValue())
      return 0;

   double frameRateRatio
         = floor(CImageInfo::queryStandardFrameRate(m_timecodeFrameRate) + .5)
           / floor(CImageInfo::queryStandardFrameRate(m_trackEditRate) + .5);

   double dNewIndex = index * frameRateRatio;
   int frameOffset = (int)dNewIndex;

   newTimecode = m_startTimecode;
   newTimecode.addFrameOffset(frameOffset, GetTimecodeFrameRate());

   partialFrame = m_startPartialFrame + dNewIndex - floor(dNewIndex);

   return 0;
}

// ---------------------------------------------------------------------------

void C5SMPTETimecodeRange::SetTimecodeFrameRate(EFrameRate newFrameRate)
{
   m_timecodeFrameRate = newFrameRate;
}

void C5SMPTETimecodeRange::SetTrackEditRate(EFrameRate newEditRate)
{
   m_trackEditRate = newEditRate;
}

void C5SMPTETimecodeRange::SetStartPartialFrame(float newPartialFrame)
{
   m_startPartialFrame = newPartialFrame;
}

void C5SMPTETimecodeRange::SetStartTimecode(const CSMPTETimecode &newTimecode)
{
   m_startTimecode = newTimecode;
}

void C5SMPTETimecodeRange::SetToDummyValue()
{
   // set timecode and user bits to 0xFFFFFFFF.  This is a bogus timecode.

   m_startTimecode.setToDummyValue();
   SetStartPartialFrame(0.0);
}

// ---------------------------------------------------------------------------

int C5SMPTETimecodeRange::initFromXMLAttributes(C5XMLAttribList &attribList)
{
   int retVal;
   string valueString;
   string invalidValueString = "INVALID";

   // Raw Timecode
   if (!attribList.DoesNameExist(rawSMPTETimecodeAttr))
      {
      // ERROR: missing required attribute
      return -1;    // TBD: real error code
      }

   MTI_UINT32 newRawTimecode = attribList.GetValueUnsignedInteger(rawSMPTETimecodeAttr, 0);

   // Raw Timecode User Data
   if (!attribList.DoesNameExist(rawSMPTEUserDataAttr))
      {
      // ERROR: missing required attribute
      return -1;    // TBD: real error code
      }
   MTI_UINT32 newRawUserData = attribList.GetValueUnsignedInteger(rawSMPTEUserDataAttr, 0);

   CSMPTETimecode newTimecode;
   newTimecode.setRawTimecode(newRawTimecode);
   newTimecode.setRawUserData(newRawUserData);

   // Timecode Frame Rate
   if (!attribList.DoesNameExist(timecodeFrameRateAttr))
      {
      // ERROR: missing required attribute
      return -1;    // TBD: real error code
      }
   valueString = attribList.GetValueString(timecodeFrameRateAttr,
                                           invalidValueString);
   double newFrameRate;
   EFrameRate newTimecodeFrameRateEnum;
   newTimecodeFrameRateEnum = CImageInfo::queryFrameRate(valueString,
                                                         newFrameRate);
   if (newTimecodeFrameRateEnum == IF_FRAME_RATE_INVALID ||
       newTimecodeFrameRateEnum == IF_FRAME_RATE_NON_STANDARD)
      return CLIP_ERROR_INVALID_FRAME_RATE;

   // Track Edit Rate
   if (!attribList.DoesNameExist(editRateAttr))
      {
      // ERROR: missing required attribute
      return -1;    // TBD: real error code
      }
   valueString = attribList.GetValueString(editRateAttr, invalidValueString);
   double newTrackEditRate;
   EFrameRate newTrackEditRateEnum;
   newTrackEditRateEnum = CImageInfo::queryFrameRate(valueString,
                                                     newTrackEditRate);
   if (newTrackEditRateEnum == IF_FRAME_RATE_INVALID ||
       newTrackEditRateEnum == IF_FRAME_RATE_NON_STANDARD)
      return CLIP_ERROR_INVALID_FRAME_RATE;

   if (!attribList.DoesNameExist(partialFrameAttr))
      return -1;  // missing required attribute
   double newPartialFrame = attribList.GetValueDouble(partialFrameAttr, -1000.0);
   if (newPartialFrame <= -1000.0)
      return -1;

   retVal = C5Range::initFromXMLAttributes(attribList);
   if (retVal != 0)
      return retVal;

   m_startTimecode = newTimecode;
   m_timecodeFrameRate = newTimecodeFrameRateEnum;
   m_trackEditRate = newTrackEditRateEnum;
   m_startPartialFrame = newPartialFrame;

   return 0;

}  // initFromXMLAttributes

void C5SMPTETimecodeRange::writeXMLStream(CXMLStream &xmlStream)
{
   C5Range::writeXMLStream(xmlStream);

   xmlStream << attr(rawSMPTETimecodeAttr)
             << m_startTimecode.getRawTimecode();
   xmlStream << attr(rawSMPTEUserDataAttr)
             << m_startTimecode.getRawUserData();
   xmlStream << attr(timecodeFrameRateAttr)
             << CImageInfo::queryFrameRateName(m_timecodeFrameRate);
   xmlStream << attr(editRateAttr)
             << CImageInfo::queryFrameRateName(m_trackEditRate);
   xmlStream << attr(partialFrameAttr) << m_startPartialFrame;

   xmlStream << endTag();    // End Range
}

// ---------------------------------------------------------------------------

void C5SMPTETimecodeRange::Dump(ostream &ostrm)
{
}

// ===========================================================================
// ===========================================================================
// Keycode Range

C5KeycodeRange::C5KeycodeRange()
{
   SetToDummyValue();
}

C5KeycodeRange::~C5KeycodeRange()
{
}

// ---------------------------------------------------------------------------

C5KeycodeRange& C5KeycodeRange::operator=(const C5KeycodeRange& rhs)
{
   if (this == &rhs)
      return *this;  // avoid self-assign

   C5Range::operator=(rhs);

   startKeycode = rhs.startKeycode;

   return *this;
}

// ---------------------------------------------------------------------------

int C5KeycodeRange::GetFormattedStr(int index, EKeykodeFormat formatType,
                                    string &keykodeStrReturn,
                                    bool overrideInterp)
{
   int retVal;

   CMTIKeykode tmpKeycode;
   retVal = GetKeykode(index, tmpKeycode);
   if (retVal != 0)
      return retVal;

   keykodeStrReturn = tmpKeycode.getFormattedStr(formatType, overrideInterp);

   return 0;
}

int C5KeycodeRange::GetKeykode(int index, CMTIKeykode &keykodeReturn)
{
   keykodeReturn = startKeycode;

   keykodeReturn.addFrameOffset(index);
   
   return 0;
}

int C5KeycodeRange::GetKeykodeStr(int index, string &keykodeStrReturn)
{
   int retVal;

   CMTIKeykode tmpKeycode;
   retVal = GetKeykode(index, tmpKeycode);
   if (retVal != 0)
      return retVal;

   keykodeStrReturn = tmpKeycode.getKeykodeStr();

   return 0;
}


// ---------------------------------------------------------------------------

int C5KeycodeRange::SetKeykode(const CMTIKeykode &newKeykode)
{
   startKeycode = newKeykode;

   return 0;
}

int C5KeycodeRange::SetRawKeykode(const string &newKKStr,
                                  int newInterpolationOffset)
{
   startKeycode.setRawKeykode(newKKStr, newInterpolationOffset);

   return 0;
}

int C5KeycodeRange::SetRawKeykode(const char *newKKStr,
                                  int newInterpolationOffset)
{
   startKeycode.setRawKeykode(newKKStr, newInterpolationOffset);

   return 0;
}

void C5KeycodeRange::SetToDummyValue()
{
   startKeycode.setToDummyValue();
}

// ---------------------------------------------------------------------------

int C5KeycodeRange::initFromXMLAttributes(C5XMLAttribList &attribList)
{
   int retVal;
   string emptyString;

   SetToDummyValue();

   // Keykode Calibration offset
   if (!attribList.DoesNameExist(keykodeCalibOffsetAttr))
      {
      // ERROR: missing required attribute
      return -1;    // TBD: real error code
      }
   int newCalibrationOffset
                = attribList.GetValueInteger(keykodeCalibOffsetAttr, -1);

   // Keykode Film Dimension
   if (!attribList.DoesNameExist(keykodeFilmDimensionAttr))
      {
      // ERROR: missing required attribute
      return -1;    // TBD: real error code
      }
   int newFilmDimension
                     = attribList.GetValueInteger(keykodeFilmDimensionAttr, -1);
   if (newFilmDimension < 0)
      return -1;

   // Keykode Interpolate Flag
   if (!attribList.DoesNameExist(keykodeInterpolateFlagAttr))
      {
      // ERROR: missing required attribute
      return -1;    // TBD: real error code
      }
   bool newInterpolateFlag
                  = attribList.GetValueBool(keykodeInterpolateFlagAttr, false);

   // Keykode String
   if (!attribList.DoesNameExist(keykodeStrAttr))
      {
      // ERROR: missing required attribute
      return -1;    // TBD: real error code
      }
   string newKeykodeStr
                   = attribList.GetValueString(keykodeStrAttr, emptyString);
   if (newKeykodeStr.empty())
      return -1;

   retVal = C5Range::initFromXMLAttributes(attribList);
   if (retVal != 0)
      return retVal;

   startKeycode.setCalibrationOffset(newCalibrationOffset);
   startKeycode.setFilmDimension((EKeykodeFilmDimension)newFilmDimension);
   startKeycode.setInterpolateFlag(newInterpolateFlag);
   startKeycode.setKeykodeStr(newKeykodeStr);

   return 0;

}  // initFromXMLAttributes


void C5KeycodeRange::writeXMLStream(CXMLStream &xmlStream)
{
   C5Range::writeXMLStream(xmlStream);

   // Keykode Calibration offset
   xmlStream << attr(keykodeCalibOffsetAttr)
             << startKeycode.getCalibrationOffset();

   // Keykode Film Dimension
   xmlStream << attr(keykodeFilmDimensionAttr)
             << (int)(startKeycode.getFilmDimension());

   // Keykode Interpolate Flag
   xmlStream << attr(keykodeInterpolateFlagAttr)
             << startKeycode.getInterpolateFlag();

   // Keykode String
   xmlStream << attr(keykodeStrAttr) << escape(startKeycode.getKeykodeStr());

   xmlStream << endTag();  // End Range tag
}

// ---------------------------------------------------------------------------

void C5KeycodeRange::Dump(ostream &ostrm)
{
   C5Range::Dump(ostrm);

   // Keykode Calibration offset
   ostrm << "Start Keykode Calibration Offset: "
         << startKeycode.getCalibrationOffset() << endl;

   // Keykode Film Dimension
   ostrm << "Start Keykode Film Dimension: "
         << startKeycode.getFilmDimension() << endl;

   // Keykode Interpolate Flag
   ostrm << "Start Keykode Interpolate Flage: "
         << startKeycode.getInterpolateFlag() << endl;

   // Keykode String
   ostrm << "Start Keykode String: "
         << startKeycode.getKeykodeStr() << endl;
}


// ===========================================================================
// ===========================================================================

C5Pulldown::C5Pulldown()
{
}

C5Pulldown::~C5Pulldown()
{
}


int C5Pulldown::GetSrcRange(int dstIndex, int dstCount,
                            int &srcIndex, int &srcCount)
{
   int retVal = 0;

   // Check that dstIndex and dstCount are totally within the
   // Pulldown Segments range

   switch(pulldownType)
      {
      case PULLDOWN_TYPE_3_2_FIELD:      // typically NTSC
         // TBD: start with phasing of first field in range and figure out
         //      from there
         retVal = CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
         break;
      case PULLDOWN_TYPE_3_2_FRAME:      // typically 720P/60
         // TBD: start with phasing of first frame in range and figure out
         //      from there
         retVal = CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
         break;
      case PULLDOWN_TYPE_2_1:            // Interlaced Fields -> Frame
         srcIndex = (dstIndex + srcStartIndex) * 2;
         srcCount = std::min(dstCount, GetItemCount() - dstIndex) * 2;  // always two fields per frame
         break;
      case PULLDOWN_TYPE_1_1:            // Progressive Frame or single field
         srcIndex = (dstIndex + srcStartIndex);
         srcCount = std::min(dstCount, GetItemCount() - dstIndex);    // always one field per frame
         break;
      default:
         // unknown pulldown type
         return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR;  // unknown pulldown type
      }

   return retVal;
}

int C5Pulldown::GetTotalFieldCount(int index) const
{
   int fieldCount = -1;

   switch(pulldownType)
      {
      case PULLDOWN_TYPE_3_2_FIELD:      // typically NTSC
         // TBD: start with phasing of first field in range and figure out
         //      from there
         break;
      case PULLDOWN_TYPE_3_2_FRAME:      // typically 720P/60
         // TBD: start with phasing of first frame in range and figure out
         //      from there
         break;
      case PULLDOWN_TYPE_2_1:            // Interlaced Fields -> Frame
         fieldCount = 2;  // always two fields per frame
         break;
      case PULLDOWN_TYPE_1_1:            // Progressive Frame or single field
         fieldCount = 1;  // always one field per frame
         break;
      default:
         fieldCount = -1;   // unknown pulldown type, return -1 as error
         break;
      }

   return fieldCount;
}

int C5Pulldown::GetVisibleFieldCount(int index) const
{
   int fieldCount = -1;

   switch(pulldownType)
      {
      case PULLDOWN_TYPE_3_2_FIELD:      // typically NTSC
         // TBD: start with phasing of first field in range and figure out
         //      from there
         break;
      case PULLDOWN_TYPE_3_2_FRAME:      // typically 720P/60
         // TBD: start with phasing of first frame in range and figure out
         //      from there
         break;
      case PULLDOWN_TYPE_2_1:            // Interlaced Fields -> Frame
         fieldCount = 2;  // always two fields per frame
         break;
      case PULLDOWN_TYPE_1_1:            // Progressive Frame or single field
         fieldCount = 1;  // always one field per frame       
         break;
      default:
         fieldCount = -1;   // unknown pulldown type, return -1 as error
         break;
      }

   return fieldCount;
}

int C5Pulldown::GetHiddenFieldCount(int index) const
{
   int fieldCount = -1;

   switch(pulldownType)
      {
      case PULLDOWN_TYPE_3_2_FIELD:      // typically NTSC
         // TBD: start with phasing of first field in range and figure out
         //      from there
         break;
      case PULLDOWN_TYPE_3_2_FRAME:      // typically 720P/60
         // TBD: start with phasing of first frame in range and figure out
         //      from there
         break;
      case PULLDOWN_TYPE_2_1:            // Interlaced Fields -> Frame
         fieldCount = 0;  // no hidden fields
         break;
      case PULLDOWN_TYPE_1_1:            // Progressive Frame or single field
         fieldCount = 0;  // no hidden fields
         break;
      default:
         fieldCount = -1;   // unknown pulldown type, return -1 as error
         break;
      }

   return fieldCount;
}

// ---------------------------------------------------------------------------

int C5Pulldown::GetVisibleFieldList(int index, int *fieldList, int *fieldCount) const
{
   int retVal;

   if (fieldList == nullptr || fieldCount == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_ARGUMENT;

   switch(pulldownType)
      {
/*
      case PULLDOWN_TYPE_3_2_FIELD:      // typically NTSC
         // TBD: start with phasing of first field in range and figure out
         //      from there
         break;
      case PULLDOWN_TYPE_3_2_FRAME:      // typically 720P/60
         // TBD: start with phasing of first frame in range and figure out
         //      from there
         break;
*/
      case PULLDOWN_TYPE_2_1:            // Interlaced Fields -> Frame
         fieldList[0] = index * 2;
         fieldList[1] = index * 2 + 1;
         *fieldCount = 2;
         break;
      case PULLDOWN_TYPE_1_1:            // Progressive Frame or single field
         fieldList[0] = index;
         *fieldCount = 1;
         break;
      default:
         return CLIP_ERROR_PULLDOWN_TYPE_UNKNOWN;   // unknown pulldown type
      }

   return 0;
}

// ---------------------------------------------------------------------------

int C5Pulldown::GetAllFieldList(int index, S5FieldListEntry fieldList[],
                                int &count)
{
   int retVal;
   int fieldIndexList[CLIP5_MAX_FIELD_COUNT];
   int fieldCount;

   // Get the list of items to read
   retVal = GetAllFieldList(index, fieldIndexList, &fieldCount);
   if (retVal != 0)
      return retVal; // ERROR

   // TBD: consider if optimization could be done here, with rearrangement
   //      of the fieldList and buffers.  Or perhaps optimization can
   //      only be done with knowledge of the way media is stored.

   // Get the source track
   C5Track *track = GetSrcTrack().GetTrack();
   if (track == nullptr)
      return -1;  // ERROR: No source track available

   E5TrackType type = track->GetTrackType();
   if (type != CLIP5_TRACK_TYPE_MEDIA)
      return -1;  // ERROR: track needs to be media location track

   C5MediaTrack *mediaTrack = static_cast<C5MediaTrack*>(track);

   // Read from the source track
   for (int i = 0; i < fieldCount; ++i)
      {
      fieldList[i].index = fieldIndexList[i] + srcStartIndex;
      fieldList[i].mediaTrack = mediaTrack;
      }

   count = fieldCount;

   return 0;
}

int C5Pulldown::GetAllFieldList(int index, int *fieldList, int *fieldCount) const
{
   int retVal;

   if (fieldList == nullptr || fieldCount == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_ARGUMENT;

   switch(pulldownType)
      {
      case PULLDOWN_TYPE_3_2_FIELD:      // typically NTSC
         // TBD: start with phasing of first field in range and figure out
         //      from there
         break;
      case PULLDOWN_TYPE_3_2_FRAME:      // typically 720P/60
         // TBD: start with phasing of first frame in range and figure out
         //      from there
         break;
      case PULLDOWN_TYPE_2_1:            // Interlaced Fields -> Frame
         fieldList[0] = index * 2;
         fieldList[1] = index * 2 + 1;
         *fieldCount = 2;
         break;
      case PULLDOWN_TYPE_1_1:            // Progressive Frame or single field
         fieldList[0] = index;
         *fieldCount = 1;
         break;
      default:
         return CLIP_ERROR_PULLDOWN_TYPE_UNKNOWN;   // unknown pulldown type
      }

   return 0;
}

// -----------------------------------------------------------------------

void C5Pulldown::SetPulldownPhase(int newPulldownPhase)
{
   pulldownPhase = newPulldownPhase;
}

void C5Pulldown::SetPulldownType(EPulldownType newPulldownType)
{
   pulldownType = newPulldownType;
}

// -----------------------------------------------------------------------


// -----------------------------------------------------------------------

int C5Pulldown::initFromXMLAttributes(C5XMLAttribList &attribList)
{
   int retVal;
   string valueString;
   string invalidValueString = "INVALID";

   // pulldownPhase
   if (!attribList.DoesNameExist(pulldownPhaseAttr))
      {
      // ERROR: missing required attribute
      return -1;    // TBD: real error code
      }
   int newPulldownPhase = attribList.GetValueInteger(pulldownPhaseAttr, -1);
   if (newPulldownPhase < 0)
      {
      // ERROR: invalid required attribute
      return -1;   // TBD: real error code
      }

   // pulldownType
   if (!attribList.DoesNameExist(pulldownTypeAttr))
      {
      // ERROR: missing required attribute
      return -1;    // TBD: real error code
      }
   valueString = attribList.GetValueString(pulldownTypeAttr, invalidValueString);
   EPulldownType newPulldownType = clipPulldownType.FindByStr(valueString,
                                                              PULLDOWN_TYPE_INVALID);
   if (newPulldownType == PULLDOWN_TYPE_INVALID)
      {
      // ERROR: invalid required attribute
      return -1;   // TBD: real error code
      }

   retVal = C5Segment::initFromXMLAttributes(attribList);
   if (retVal != 0)
      return retVal;

   pulldownPhase = newPulldownPhase;
   pulldownType = newPulldownType;

   return 0;

}  // initFromXMLAttributes

// -----------------------------------------------------------------------

void C5Pulldown::writeXMLStream(CXMLStream &xmlStream)
{
   C5Segment::writeXMLStream(xmlStream);

   xmlStream << attr(pulldownTypeAttr)
             << escape(clipPulldownType.FindByValue(pulldownType));

   xmlStream << attr(pulldownPhaseAttr) << pulldownPhase;
   xmlStream << endTag();  // End Pulldown tag
}

// ===========================================================================
// ===========================================================================
// C5MediaPulldown
// ---------------------------------------------------------------------------

int C5MediaPulldown::ClearMediaWritten(int startIndex, int itemCount)
{
   int retVal;

   int srcStart, srcItemCount;
   GetSrcRange(startIndex, itemCount, srcStart, srcItemCount);

   retVal = GetSrcMediaTrack().ClearMediaWritten(srcStart, srcItemCount);
   if (retVal != 0)
      return retVal;

   return 0;
}

int C5MediaPulldown::FreeMedia(const string &clipPath, int startIndex,
                               int itemCount)
{
   int retVal;

   int srcStart, srcItemCount;
   GetSrcRange(startIndex, itemCount, srcStart, srcItemCount);

   retVal = GetSrcMediaTrack().FreeMedia(clipPath, srcStart, srcItemCount);
   if (retVal != 0)
      return retVal;

   return 0;
}

C5MediaTrackHandle C5MediaPulldown::GetSrcMediaTrack() const
{
   C5MediaTrackHandle mediaTrackHandle = GetSrcTrack();

   return mediaTrackHandle;
}

// ---------------------------------------------------------------------------

int C5MediaPulldown::GetFirstMediaAccess(CMediaAccess* &mediaAccess) const
{
   int retVal;

   retVal = GetSrcMediaTrack().GetFirstMediaAccess(mediaAccess);
   if (retVal != 0)
      return retVal;

   return 0;
}

int C5MediaPulldown::GetFirstMediaIdentifier(string &mediaIdentifier) const
{
   int retVal;

   retVal = GetSrcMediaTrack().GetFirstMediaIdentifier(mediaIdentifier);
   if (retVal != 0)
      return retVal;

   return 0;
}

int C5MediaPulldown::GetFirstMediaType(EMediaType &mediaType) const
{
   int retVal;

   retVal = GetSrcMediaTrack().GetFirstMediaType(mediaType);
   if (retVal != 0)
      return retVal;

   return 0;
}

MTI_UINT32 C5MediaPulldown::getMaxPaddedFieldByteCount(int index, int count)
{
   int retVal, srcIndex, srcCount;

   retVal = GetSrcRange(index, count, srcIndex, srcCount);
   if (retVal != 0)
      return 0;

   return GetSrcMediaTrack().getMaxPaddedFieldByteCount(srcIndex, srcCount);
}

const CMediaFormat* C5MediaPulldown::GetMediaFormat() const
{
   return GetSrcMediaTrack().GetMediaFormat();
}

E5MediaStatus C5MediaPulldown::GetMediaStatus(int index)
{
   int retVal;

   // get the indices of the visible fields
   int fieldIndices[CLIP5_MAX_FIELD_COUNT];
   int fieldCount;
   retVal = GetVisibleFieldList(index, fieldIndices, &fieldCount);
   if (retVal != 0)
      return CLIP5_MEDIA_STATUS_INVALID;

   // figure out the worst media status of all of the fields
   C5MediaTrackHandle trackHandle = GetSrcMediaTrack();

   // Note: the items in this E5MediaStatus enum are assumed to be
   //       'ordered' from worst to best so that it is possible to sort
   //       the status of several fields and have the worst status
   //       dominate a better status.
   E5MediaStatus worstStatus = trackHandle.GetMediaStatus(fieldIndices[0]
                                                          + srcStartIndex);
   for (int i = 1; i < fieldCount; ++i)
      {
      E5MediaStatus status = trackHandle.GetMediaStatus(fieldIndices[i]
                                                        + srcStartIndex);
      if (status < worstStatus)
         worstStatus = status;
      }
   return worstStatus;
}

// ----------------------------------------------------------------------------

int C5MediaPulldown::LSF2(int startIndex, int itemCount, int &allocCount)
{
   int retVal;

   allocCount = 0;

   int srcStartIndex, srcItemCount;

   retVal = GetSrcRange(startIndex, itemCount, srcStartIndex, srcItemCount);
   if (retVal != 0)
      return retVal;

   int newCount;
   retVal = GetSrcMediaTrack().LSF2(srcStartIndex, srcItemCount, newCount);
   if (retVal != 0)
      return retVal;

   // TBD: Reverse Pulldown to Convert field count into a frame count
   //      Dependent on phasing.  Could return fractional value.  Ugh!
   // TTT: Punt for now, just divide by two or whatever
   switch(pulldownType)
      {
      case PULLDOWN_TYPE_3_2_FIELD:      // typically NTSC
         // TBD: start with phasing of first field in range and figure out
         //      from there
         break;
      case PULLDOWN_TYPE_3_2_FRAME:      // typically 720P/60
         // TBD: start with phasing of first frame in range and figure out
         //      from there
         break;
      case PULLDOWN_TYPE_2_1:            // Interlaced Fields -> Frame
         newCount /= 2;  // always two fields per frame
         break;
      case PULLDOWN_TYPE_1_1:            // Progressive Frame or single field
         // always one field per frame
         break;
      default:
         // unknown pulldown type,
         break;
      }

   allocCount = newCount;

   return 0;
}

// ---------------------------------------------------------------------------

// ReadMedia - read all fields in a frame
int C5MediaPulldown::ReadMedia(int index, MTI_UINT8 *buffer[])
{
   int retVal;
   int fieldList[CLIP5_MAX_FIELD_COUNT];
   int fieldCount;

   // Get the list of items to read
   retVal = GetAllFieldList(index, fieldList, &fieldCount);
   if (retVal != 0)
      return retVal; // ERROR

   // Get the source track
//   C5MediaTrack *srcTrack = GetSrcMediaTrack().GetMediaTrack();
   C5MediaContentAPI *srcTrack = GetSrcMediaTrack().GetMediaContentAPI();
   if (srcTrack == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;  // ERROR: No source track available

   // Read from the source track
   for (int i = 0; i < fieldCount; ++i)
      {
      retVal = srcTrack->ReadField(fieldList[i] + srcStartIndex, buffer[i]);
         if (retVal != 0)
            return retVal; // ERROR
      }

   return 0;

} // ReadMedia

int C5MediaPulldown::ReadMediaMT(int index, MTI_UINT8 *buffer[],
                               int fileHandleIndex)
{
   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

// ReadMedia - Read a single field in a frame given a frame number and
//             field index within the frame
int C5MediaPulldown::ReadMediaOneField(int frameIndex, int fieldIndex,
                                       MTI_UINT8 *buffer)
{
   int retVal;
   int fieldList[CLIP5_MAX_FIELD_COUNT];
   int fieldCount;

   // Get the list of items to read
   retVal = GetAllFieldList(frameIndex, fieldList, &fieldCount);
   if (retVal != 0)
      return retVal; // ERROR

   if (fieldIndex < 0 || fieldIndex >= fieldCount)
      return CLIP_ERROR_INVALID_FIELD_INDEX;

   // Get the source track
//   C5MediaTrack *srcTrack = GetSrcMediaTrack().GetMediaTrack();
   C5MediaContentAPI *srcTrack = GetSrcMediaTrack().GetMediaContentAPI();
   if (srcTrack == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;  // ERROR: No source track available

   // Read from the source track
   MTI_UINT8 *bufferArray[1];
   bufferArray[0] = buffer;
   retVal = srcTrack->ReadField(fieldList[fieldIndex] + srcStartIndex,
                                bufferArray[0]);
   if (retVal != 0)
      return retVal; // ERROR

   return 0;

} // ReadMedia


int C5MediaPulldown::ReadMediaOneFieldMT(int frameIndex, int fieldIndex,
                                         MTI_UINT8 *buffer, int fileHandleIndex)
{
   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

// -----------------------------------------------------------------------

int C5MediaPulldown::WriteMedia(int index, MTI_UINT8 *buffer[])
{
   int retVal;
   int fieldList[CLIP5_MAX_FIELD_COUNT];
   int fieldCount;

   // Get the list of items to read
   retVal = GetAllFieldList(index, fieldList, &fieldCount);
   if (retVal != 0)
      return retVal; // ERROR

   // Get the source track
//   C5MediaTrack *srcTrack = GetSrcMediaTrack().GetMediaTrack();
   C5MediaContentAPI *srcTrack = GetSrcMediaTrack().GetMediaContentAPI();
   if (srcTrack == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;  // ERROR: No source track available

   // Write to the source track
   for (int i = 0; i < fieldCount; ++i)
      {
      retVal = srcTrack->WriteField(fieldList[i] + srcStartIndex, buffer[i]);
      if (retVal != 0)
         return retVal; // ERROR
      }

   return 0;

} // WriteMedia

// ===========================================================================
// ===========================================================================
// C5AudioPulldown

const CAudioFormat* C5AudioPulldown::GetAudioFormat() const
{
   return static_cast<const CAudioFormat*>(GetMediaFormat());
}

// ===========================================================================
// ===========================================================================
// C5VideoPulldown

const CImageFormat* C5VideoPulldown::GetImageFormat() const
{
   return static_cast<const CImageFormat*>(GetMediaFormat());
}

// ===========================================================================
// ===========================================================================
// C5SMPTETimecodePulldown

int C5SMPTETimecodePulldown::GetTimecode(int index, CSMPTETimecode &timecode,
                                         float &partialFrame)
{
   int retVal;

   int srcIndex, srcItemCount;
   retVal = GetSrcRange(index, 1, srcIndex, srcItemCount);
   if (retVal != 0)
      return retVal;

   C5SMPTETimecodeTrackHandle srcTrackHandle = GetSrcSMPTETimecodeTrack();

   retVal = srcTrackHandle.GetTimecode(srcIndex, timecode, partialFrame);
   if (retVal != 0)
      return retVal;

   return 0;
}

int C5SMPTETimecodePulldown::GetTimecodeFrameRate() const
{
   C5SMPTETimecodeTrackHandle srcTrackHandle = GetSrcSMPTETimecodeTrack();

   return srcTrackHandle.GetTimecodeFrameRate();
}

EFrameRate C5SMPTETimecodePulldown::GetTimecodeFrameRateEnum() const
{
   C5SMPTETimecodeTrackHandle srcTrackHandle = GetSrcSMPTETimecodeTrack();

   return srcTrackHandle.GetTimecodeFrameRateEnum();
}

// ---------------------------------------------------------------------------

int C5SMPTETimecodePulldown::SetTimecode(int index,
                                         const CSMPTETimecode &newTimecode,
                                         float partialFrame)
{
   int retVal;

   int srcIndex, srcItemCount;
   retVal = GetSrcRange(index, 1, srcIndex, srcItemCount);
   if (retVal != 0)
      return retVal;

   C5SMPTETimecodeTrackHandle srcTrackHandle = GetSrcSMPTETimecodeTrack();

   retVal = srcTrackHandle.SetTimecode(srcIndex, newTimecode, partialFrame);
   if (retVal != 0)
      return retVal;

   return 0;
}

int C5SMPTETimecodePulldown::SetTimecodeX(int index, int count,
                                          const CSMPTETimecode &newTimecode,
                                          float partialFrame)
{
   int retVal;

   int srcIndex, srcItemCount;
   retVal = GetSrcRange(index, 1, srcIndex, srcItemCount);
   if (retVal != 0)
      return retVal;

   C5SMPTETimecodeTrackHandle srcTrackHandle = GetSrcSMPTETimecodeTrack();

   retVal = srcTrackHandle.SetTimecodeX(srcIndex, count, newTimecode,
                                        partialFrame);
   if (retVal != 0)
      return retVal;

   return 0;
}

int C5SMPTETimecodePulldown::SetToDummyValue(int startIndex, int itemCount)
{
   int retVal;

   int srcStartIndex, srcItemCount;
   retVal = GetSrcRange(startIndex, itemCount, srcStartIndex, srcItemCount);
   if (retVal != 0)
      return retVal;

   C5SMPTETimecodeTrackHandle srcTrackHandle = GetSrcSMPTETimecodeTrack();

   retVal = srcTrackHandle.SetToDummyValue(srcStartIndex, srcItemCount);
   if (retVal != 0)
      return retVal;

   return 0;
}

C5SMPTETimecodeTrackHandle C5SMPTETimecodePulldown::GetSrcSMPTETimecodeTrack() const
{
   C5SMPTETimecodeTrackHandle smpteTimecodeTrackHandle = GetSrcTrack();

   return smpteTimecodeTrackHandle;
}

// ===========================================================================
// ===========================================================================
// C5Track

C5Track::C5Track()
 : parsingLevel(CLIP5_PARSING_LEVEL_TOP), needsUpdate(false),
   serialNumber(0), cachedItemCount(-1),
   editRate(IF_FRAME_RATE_INVALID)
{
   trackID = guid_create();
}

C5Track::~C5Track()
{
}

int C5Track::initFromXMLAttributes(C5XMLAttribList &attribList)
{
   string valueString, emptyString;

   // Get the serial number
   if (!attribList.DoesNameExist(serialNumberAttr))
      {
      // ERROR: missing required attribute
      return -1;
      }
   MTI_UINT64 newSerialNumber = attribList.GetValueInteger(serialNumberAttr, 0);
   if (newSerialNumber == 0)
      {
      // ERROR: invalid serial number
      return -1;
      }

   // Get the track ID (GUID)
   if (!attribList.DoesNameExist(trackIDAttr))
      {
      // ERROR: missing required attribute
      return -1;
      }
   valueString = attribList.GetValueString(trackIDAttr, GetNullGUIDStr());
   Clip5TrackID newTrackID;
   if (!guid_from_string(newTrackID, valueString))
      {
      // ERROR: invalid track ID (GUID)
      return -1;
      }
   if (IsNullGUID(newTrackID))
      {
      // ERROR: invalid track ID
      return -1;
      }

   // Get the track's edit rate (frames per second)
   if (!attribList.DoesNameExist(editRateAttr))
      {
      // ERROR: missing required attribute
      return -1;
      }
   valueString = attribList.GetValueString(editRateAttr, emptyString);
   double newEditRate;
   EFrameRate newEditRateEnum;
   newEditRateEnum = CImageInfo::queryFrameRate(valueString, newEditRate);
   if (newEditRateEnum == IF_FRAME_RATE_INVALID ||
       newEditRateEnum == IF_FRAME_RATE_NON_STANDARD)
      return CLIP_ERROR_INVALID_FRAME_RATE;

   serialNumber = newSerialNumber;
   trackID = newTrackID;
   editRate = newEditRateEnum;

   return 0;
}

int C5Track::xmlParseStartElement(const string &elementName,
                                  C5XMLAttribList &attribList)
{
   if (parsingLevel == CLIP5_PARSING_LEVEL_TOP)
      {
      if (elementName == rangeListTag)
         {
         parsingLevel = CLIP5_PARSING_LEVEL_RANGE_LIST;
         }
       else
         {
         // TBD: Error handling, unexpected tag
         return -1;
         }
      }
   else if (parsingLevel == CLIP5_PARSING_LEVEL_RANGE_LIST)
      {
      if (elementName == rangeTag)
         {
         C5Range *currentRange = C5Range::CreateRange(attribList);
         if (currentRange == nullptr)
            {
            // ERROR
            return -1;
            }
         AppendRange(currentRange);
         }
       else
         {
         // TBD: Error handling, unexpected tag
         return -1;
         }
      }
   else
      {
      // ERROR:
      return -1;
      }

   return 0;
}

int C5Track::xmlParseCharacters(const string &chars)
{
   return 0;
}

int C5Track::xmlParseEndElement(const string &elementName)
{
   if (parsingLevel == CLIP5_PARSING_LEVEL_RANGE_LIST &&
       elementName == rangeListTag)
      {
      parsingLevel = CLIP5_PARSING_LEVEL_TOP;
      }
      
   return 0;
}

// --------------------------------------------------------------------------

bool C5Track::IsIndexValid(int index)
{
   // Check that index is within range 0 <= index < itemCount
   
   return (index >= 0 && index < GetItemCount());
}

int C5Track::GetItemCount()
{
   if (cachedItemCount < 0)
      CalculateItemCount();

   return cachedItemCount;
}

int C5Track::GetTotalFrameCount()
{
   return GetItemCount();
}

void C5Track::CalculateItemCount()
{
   cachedItemCount = 0;

   for (RangeList::iterator rangeIterator = rangeList.begin();
        rangeIterator != rangeList.end();
        ++rangeIterator)
      {
      cachedItemCount += (*rangeIterator)->GetItemCount();
      }
}

void C5Track::ResetItemCount()
{
   cachedItemCount = -1;    // this will force re-calc when needed
}

// --------------------------------------------------------------------------
// Edit Rate is the AAF nomenclature for Frame or Field rate

void C5Track::SetEditRate(EFrameRate newEditRate)
{
   editRate = newEditRate;
}

// --------------------------------------------------------------------------

void C5Track::SetNeedsUpdate(bool newNeedsUpdate)
{
   needsUpdate = newNeedsUpdate;
}

// --------------------------------------------------------------------------

C5Range* C5Track::FindRange(int itemIndex)
{
   C5RangeHandle rangeHandle = FindRangeHandle(itemIndex);
   if (IsEnd(rangeHandle))
      return 0;

   return rangeHandle.GetRange();
}

// --------------------------------------------------------------------------

C5RangeHandle C5Track::FindRangeHandle(int itemIndex)
{
   RangeListIterator endIter = rangeList.end();
   for (RangeListIterator rangeIterator = rangeList.begin();
        rangeIterator != endIter;
        ++rangeIterator)
      {
      int startIndex = (*rangeIterator)->GetDstStartIndex();
      if (itemIndex >= startIndex &&
          itemIndex < startIndex + (*rangeIterator)->GetItemCount())
         return C5RangeHandle(rangeIterator);
      }

   return C5RangeHandle(endIter);
}

// --------------------------------------------------------------------------

int C5Track::GetAllFieldList(int index, S5FieldListEntry fieldList[],
                             int &count)
{
   count = 0;
   
   // Function not implemented on base class
   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

// --------------------------------------------------------------------------

// Edit Rate is AAF nomenclature for the frame or field rate

double C5Track::GetEditRate() const
{
   return CImageInfo::queryStandardFrameRate(editRate);
}

EFrameRate C5Track::GetEditRateEnum() const
{
   return editRate;
}

// --------------------------------------------------------------------------

C5RangeHandle C5Track::GetEndRangeHandle()
{
   // Return a range handle with "end" iterator

   RangeListIterator endIter = rangeList.end();

   return C5RangeHandle(endIter);
}

// --------------------------------------------------------------------------

int C5Track::FindCoveringSet(int itemIndex, int count,
                             list<S5RangeCover> &coverSet)
{ 
   int retVal;

   RangeListIterator endIter = rangeList.end();
   coverSet.clear();

   while (count > 0)
      {
      C5RangeHandle rangeHandle = FindRangeHandle(itemIndex);
      if (rangeHandle == endIter)
         return -1;  // premature end of search, count exceeds available items

      C5Range *range = rangeHandle.GetRange();
      if (range == nullptr)
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN;  // shouldn't happen

      int availableCount = std::min(count,
                               range->GetItemCount() -
                                     (itemIndex - range->GetDstStartIndex() ) );

      S5RangeCover cover;
      cover.startIndex = itemIndex;
      cover.itemCount = availableCount;
      cover.rangeHandle = rangeHandle;
      coverSet.push_back(cover);

      itemIndex += availableCount;
      count -= availableCount;
      
      ++rangeHandle;
      }

   return 0;
}

bool C5Track::IsEnd(C5RangeHandle &rangeHandle)
{
   RangeListIterator endIter = rangeList.end();
   return (rangeHandle == endIter);
}

bool C5Track::GetNeedsUpdate() const
{
   return needsUpdate;
}

int C5Track::GetRangeCount() const
{
   return rangeList.size();
}

// -----------------------------------------------------------------------

int C5Track::AppendRange(C5Range *newRange)
{
   if (newRange == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_ARGUMENT;

   int newDstStartIndex;
   if (rangeList.empty())
      {
      newDstStartIndex = 0;
      }
   else
      {
      C5Range *lastRange = rangeList.back();
      newDstStartIndex = lastRange->GetDstStartIndex() + lastRange->GetItemCount();
      }

   newRange->SetDstStartIndex(newDstStartIndex);

   rangeList.push_back(newRange);

   SetNeedsUpdate(true);

   ResetItemCount();

   return 0;
}

void C5Track::RemoveRange(C5RangeHandle &rangeHandle)
{
   // Delete the range
   delete *(rangeHandle.iter);

   // Remove the range from the range list
   rangeList.erase(rangeHandle.iter);

   // Clear the range handle
   rangeHandle.iter = rangeList.end();

   SetNeedsUpdate(true);

   ResetItemCount();
}

void C5Track::InsertBefore(C5RangeHandle &position, C5Range *newRange)
{
   rangeList.insert(position.iter, newRange);

   SetNeedsUpdate(true);

   ResetItemCount();
}

void C5Track::InsertAfter(C5RangeHandle &position, C5Range *newRange)
{
   RangeListIterator next = position.iter;
   ++next;
   rangeList.insert(next, newRange);

   SetNeedsUpdate(true);

   ResetItemCount();
}

// -----------------------------------------------------------------------

Clip5TrackID C5Track::GetTrackID() const
{
   return trackID;
}

// -----------------------------------------------------------------------

string C5Track::GetTrackIDStr() const
{
   return guid_to_string(trackID);
}

//---------------------------------------------------------------------------

bool C5Track::compareTrackID(const _guid_t &trialID)
{
   // Return true if trialID matches this track's ID
   return (guid_compare(trialID, trackID) == 0);
}

// -----------------------------------------------------------------------

void C5Track::writeXMLAttributes(CXMLStream &xmlStream)
{
   xmlStream << attr(trackTypeAttr)
             << escape(clipTrackType.FindByValue(GetTrackType()));
   xmlStream << attr(contentTypeAttr)
             << escape(clipContentType.FindByValue(GetContentType()));

   ++serialNumber;
   xmlStream << attr(serialNumberAttr) << serialNumber;
   xmlStream << attr(trackIDAttr) << escape(guid_to_string(trackID));
   xmlStream << attr(editRateAttr)
             << CImageInfo::queryFrameRateName(GetEditRateEnum());
}

void C5Track::writeXMLStream(CXMLStream &xmlStream)
{
   xmlStream << tag(trackTag);

   writeXMLAttributes(xmlStream);

   if (!rangeList.empty())
      {
      xmlStream << tag(rangeListTag);

      for (RangeList::iterator iter = rangeList.begin();
           iter != rangeList.end(); ++iter)
         {
         (*iter)->writeXMLStream(xmlStream);
         }

      xmlStream << endTag();
      }

   // Track tag is terminated by child class
}

int C5Track::WriteTrackFile(const string &clipPath)
{
   // Write out track as a XML file

   // Open an output file stream
   string trackFilename = AddDirSeparator(clipPath) + GetTrackIDStr() + ".xml";
   ofstream outFile(trackFilename.c_str());
   if (!outFile.is_open())
      return CLIP_ERROR_FILE_OPEN_FAILED;

   CXMLStream xmlStream(outFile);
   xmlStream << prolog();
   xmlStream << tag("CLIP_5");
   writeXMLStream(xmlStream);    // call subtype's polymorphic function
   xmlStream << endTag();

   SetNeedsUpdate(false);

   return 0;
}

//----------------------------------------------------------------------------

void C5Track::Dump(ostream &ostrm)
{
   ostrm << "Track Type: " << clipTrackType.FindByValue(GetTrackType()) << endl;
   ostrm << "Content Type: " << clipContentType.FindByValue(GetContentType()) << endl;
   ostrm << "  Serial Number: " << serialNumber << endl;
   ostrm << "  TrackID: " << guid_to_string(trackID) << endl;
   ostrm << "  Range count: " << rangeList.size() << endl;

   for (RangeList::iterator iter = rangeList.begin(); iter != rangeList.end(); ++iter)
      {
      (*iter)->Dump(ostrm);
      }
}

// ===========================================================================

C5SequenceTrack::C5SequenceTrack()
{
}

C5SequenceTrack::~C5SequenceTrack()
{
}

//----------------------------------------------------------------------------

int C5SequenceTrack::GetAllFieldList(int index, S5FieldListEntry fieldList[],
                                     int &count)
{
   int retVal;

   if (!IsIndexValid(index))
      {
      // TBD: fill caller's buffers with something
      return CLIP_ERROR_INVALID_FRAME_NUMBER;
      }

   // Find the Segment with the caller's frame
   C5Range *range = FindRange(index);
   if (range == nullptr)
      return -1;  // TBD: Need more meaningfull error code

   E5RangeType rangeType = range->GetRangeType();
   if (rangeType == CLIP5_RANGE_TYPE_FILLER)
      return CLIP_ERROR_NO_MEDIA;
   else if (rangeType != CLIP5_RANGE_TYPE_PULLDOWN)
      return -1;  // unexpected range type.

   C5Pulldown *pulldown = static_cast<C5Pulldown*>(range);

   int rangeIndex = index - pulldown->GetDstStartIndex();
   retVal = pulldown->GetAllFieldList(rangeIndex, fieldList, count);
   if (retVal != 0)
      return retVal;

   return 0;

} // GetAllFieldList

// ---------------------------------------------------------------------------
// Get a list of media tracks that underlie the caller's range

int C5SequenceTrack::GetMediaTracks(int startIndex, int itemCount,
                                    list<S5MediaTrackListEntry> &outList)
{
   int retVal;

   list<S5RangeCover> coverSet;
   retVal = FindCoveringSet(startIndex, itemCount, coverSet);
   if (retVal != 0)
      return retVal;
   list<S5RangeCover>::iterator iter;
   list<S5RangeCover>::iterator endIter = coverSet.end();

   for(iter = coverSet.begin(); iter != endIter; ++iter)
      {
      C5Range *range = (*iter).rangeHandle.GetRange();
      E5RangeType rangeType = range->GetRangeType();
      if (rangeType == CLIP5_RANGE_TYPE_FILLER)
         {
         // TBD: Put this in results, then move on
         continue;   // Skip over filler range
         }
      else if (rangeType != CLIP5_RANGE_TYPE_PULLDOWN)
         return -1;

      C5Pulldown *pulldown = static_cast<C5Pulldown*>(range);

      C5TrackHandle trackHandle = pulldown->GetSrcTrack();

      // Figure out start and count, relative to this track and pulldown

      int overlapStart, overlapEnd;
      if (!pulldown->GetRangeIntersection(startIndex, itemCount,
                                          overlapStart, overlapEnd))
         continue;  // this shouldn't happen.  TBD: better response

      int overlapItemCount = overlapEnd - overlapStart + 1;
      overlapStart -= range->GetDstStartIndex();
      int srcStart, srcItemCount;
      pulldown->GetSrcRange(overlapStart, overlapItemCount, srcStart, srcItemCount);

      E5TrackType trackType = trackHandle.GetTrackType();
      if (trackType == CLIP5_TRACK_TYPE_MEDIA)
         {
         // put this in results list, then move on
         S5MediaTrackListEntry newEntry;
         newEntry.index = srcStart;
         newEntry.count = srcItemCount;
         newEntry.mediaTrack = static_cast<C5MediaTrack*>(trackHandle.GetTrack());
         outList.push_back(newEntry);
         }
      else if (trackType == CLIP5_TRACK_TYPE_SEQUENCE)
         {
         // Recurse for depth-first search
         retVal = GetMediaTracks(srcStart, srcItemCount, outList);
         if (retVal != 0)
            return retVal;
         }
      }

   return 0;

} // GetMediaTracks

// -----------------------------------------------------------------------

void C5SequenceTrack::writeXMLStream(CXMLStream &xmlStream)
{
   C5Track::writeXMLStream(xmlStream);

   xmlStream << endTag();  // End Track tag
}

// ===========================================================================
// ===========================================================================
// C5MediaSequenceTrack

C5MediaPulldown* C5MediaSequenceTrack::CastToMediaPulldown(C5Range *range)
{
   if (range == nullptr ||
       range->GetRangeType() != CLIP5_RANGE_TYPE_PULLDOWN ||
       (range->GetContentType() != CLIP5_CONTENT_TYPE_AUDIO &&
        range->GetContentType() != CLIP5_CONTENT_TYPE_VIDEO))
      {
      throw(std::runtime_error("Bad clip 5 range cast"));
      }

   return static_cast<C5MediaPulldown*>(range);
}

int C5MediaSequenceTrack::GetFirstMediaAccess(CMediaAccess* &mediaAccess) const
{
   mediaAccess = 0;

   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

int C5MediaSequenceTrack::GetFirstMediaIdentifier(string &mediaIdentifier) const
{
   int retVal;

   // Iterate over the ranges in the track, searching for the first non-filler
   for (RangeList::const_iterator iter = rangeList.begin();
        iter != rangeList.end(); ++iter)
      {
      C5Range *range = *iter;
      if (range->GetRangeType() == CLIP5_RANGE_TYPE_FILLER)
         continue;   // skip over filler range

      C5MediaPulldown *mediaPulldown = CastToMediaPulldown(range);
      retVal = mediaPulldown->GetFirstMediaIdentifier(mediaIdentifier);
      if (retVal != 0)
         return retVal;

      return 0;
      }

   return CLIP_ERROR_NO_MEDIA;
}

int C5MediaSequenceTrack::GetFirstMediaType(EMediaType &mediaType) const
{
   int retVal;

   // Iterate over the ranges in the track, searching for the first non-filler
   for (RangeList::const_iterator iter = rangeList.begin();
        iter != rangeList.end(); ++iter)
      {
      C5Range *range = *iter;
      if (range->GetRangeType() == CLIP5_RANGE_TYPE_FILLER)
         continue;

      C5MediaPulldown *mediaPulldown = CastToMediaPulldown(range);
      retVal = mediaPulldown->GetFirstMediaType(mediaType);
      if (retVal != 0)
         return retVal;
      return 0;
      }

   return CLIP_ERROR_NO_MEDIA;
}

MTI_UINT32 C5MediaSequenceTrack::getMaxPaddedFieldByteCount(int index,
                                                            int itemCount)
{
   int retVal;

   list<S5RangeCover> coverSet;
   retVal = FindCoveringSet(index, itemCount, coverSet);
   if (retVal != 0)
      return 0;

   MTI_UINT32 maxByteCount = 0;
   for (list<S5RangeCover>::const_iterator iter = coverSet.begin();
        iter != coverSet.end(); ++iter)
      {
      C5Range *range = (*iter).rangeHandle.GetRange();
      if (range->GetRangeType() == CLIP5_RANGE_TYPE_FILLER)
         continue;  // skip over filler


      int rangeIndex = (*iter).startIndex - range->GetDstStartIndex();
      int rangeItemCount = (*iter).itemCount;
      C5MediaPulldown *mediaPulldown = CastToMediaPulldown(range);
      MTI_UINT32 byteCount = mediaPulldown->getMaxPaddedFieldByteCount(rangeIndex,
                                                                rangeItemCount);
      if (byteCount > maxByteCount)
         maxByteCount = byteCount;
      }

   return maxByteCount;

}

const CMediaFormat *C5MediaSequenceTrack::GetMediaFormat() const
{
   // Iterate over the ranges in the track, searching for a non-null
   // media format
   for (RangeList::const_iterator iter = rangeList.begin();
        iter != rangeList.end(); ++iter)
      {
      C5Range *range = (*iter);
      if (range->GetRangeType() == CLIP5_RANGE_TYPE_FILLER)
         continue;  // skip over filler

      C5MediaPulldown *mediaPulldown = CastToMediaPulldown(range);
      const CMediaFormat *mediaFormat = mediaPulldown->GetMediaFormat();
      if (mediaFormat != 0)
         return mediaFormat;
      }

   return 0;
}

int C5MediaSequenceTrack::GetMediaLocationsAllFields(int frameIndex,
                                      vector<CMediaLocation> &mediaLocationList)
{
   // Function not implemented yet, just here to keep compiler happy
   // Would be easy to implement
   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

E5MediaStatus C5MediaSequenceTrack::GetMediaStatus(int index)
{
   // 1) find range for index
   // 2) call range->GetMediaStatus

   C5Range *range = FindRange(index);
   if (range == nullptr)
      return CLIP5_MEDIA_STATUS_INVALID;  // no range for caller's index

   if (range->GetRangeType() == CLIP5_RANGE_TYPE_FILLER)
      return CLIP5_MEDIA_STATUS_NO_MEDIA;

   C5MediaPulldown *mediaPulldown = CastToMediaPulldown(range);
   return mediaPulldown->GetMediaStatus(index - range->GetDstStartIndex());
}

//----------------------------------------------------------------------------

int C5MediaSequenceTrack::PeekAtMediaLocation(int frameIndex, int fieldIndex,
                                              string &filename,
                                              MTI_INT64 &offset)
{
   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

int C5MediaSequenceTrack::PeekAtMediaAllocation(vector<SMediaAllocation> &mediaAllocList,
                                                volatile int* currentFrame,
                                                volatile bool* stopRequest)
{
   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

//----------------------------------------------------------------------------

int C5MediaSequenceTrack::ReadMedia(int index, MTI_UINT8 *buffer[])
{
   int retVal;

   if (!IsIndexValid(index))
      {
      // TBD: fill caller's buffers with something
      return CLIP_ERROR_INVALID_FRAME_NUMBER;
      }

   // Find the Segment with the caller's frame
   C5Range *range = FindRange(index);
   if (range == nullptr)
      {
      // TBD: fill caller's buffers with something
      return -1;  // TBD: Need more meaningfull error code
      }

   if (range->GetRangeType() == CLIP5_RANGE_TYPE_FILLER)
      {
      // TBD: fill caller's buffers with something
      return CLIP_ERROR_NO_MEDIA;
      }

   C5MediaPulldown *mediaPulldown = CastToMediaPulldown(range);

   int rangeIndex = index - range->GetDstStartIndex();
   retVal = mediaPulldown->ReadMedia(rangeIndex, buffer);
   if (retVal != 0)
      return retVal;

   return 0;

} // ReadMedia

int C5MediaSequenceTrack::ReadMediaMT(int index, MTI_UINT8 *buffer[],
                                      int fileHandleIndex)
{
   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

int C5MediaSequenceTrack::ReadMediaOneField(int frameIndex, int fieldIndex,
                                            MTI_UINT8 *buffer)
{
   int retVal;

   if (!IsIndexValid(frameIndex))
      {
      // TBD: fill caller's buffers with something
      return CLIP_ERROR_INVALID_FRAME_NUMBER;
      }

   // Find the Segment with the caller's frame
   C5Range *range = FindRange(frameIndex);
   if (range == nullptr)
      {
      // TBD: fill caller's buffers with something
      return -1;  // TBD: Need more meaningfull error code
      }

   if (range->GetRangeType() == CLIP5_RANGE_TYPE_FILLER)
      {
      // TBD: fill caller's buffers with something
      return CLIP_ERROR_NO_MEDIA;
      }

   C5MediaPulldown *mediaPulldown = CastToMediaPulldown(range);

   int rangeIndex = frameIndex - range->GetDstStartIndex();
   retVal = mediaPulldown->ReadMediaOneField(rangeIndex, fieldIndex, buffer);
   if (retVal != 0)
      return retVal;

   return 0;

} // ReadMediaOneField

int C5MediaSequenceTrack::ReadMediaOneFieldMT(int frameIndex, int fieldIndex,
                                              MTI_UINT8 *buffer,
                                              int fileHandleIndex)
{
   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}
// -----------------------------------------------------------------------

int C5MediaSequenceTrack::ReadField(int index, MTI_UINT8 *buffer)
{
   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

int C5MediaSequenceTrack::WriteField(int index, MTI_UINT8 *buffer)
{
   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

// -----------------------------------------------------------------------

int C5MediaSequenceTrack::WriteMedia(int frameNumber, MTI_UINT8 *buffer[])
{
   int retVal;

   if (!IsIndexValid(frameNumber))
      return CLIP_ERROR_INVALID_FRAME_NUMBER;

   // Find the Segment with the caller's frame
   C5Range *range = FindRange(frameNumber);
   if (range == nullptr)
      return -1;  // TBD: Need more meaningfull error code

   if (range->GetRangeType() == CLIP5_RANGE_TYPE_FILLER)
      {
      // TBD: fill caller's buffers with something
      return CLIP_ERROR_NO_MEDIA;
      }

   C5MediaPulldown *mediaPulldown = CastToMediaPulldown(range);

   int rangeIndex = frameNumber - range->GetDstStartIndex();
   retVal = mediaPulldown->WriteMedia(rangeIndex, buffer);
   if (retVal != 0)
      return retVal;

   return 0;

} // WriteMedia

//----------------------------------------------------------------------------

int C5MediaSequenceTrack::LSF2(int startIndex, int itemCount, int &allocCount)
{
   int retVal;

   allocCount = 0;
   int sum = 0;

   int totalItemCount = GetItemCount();

   if (!IsIndexValid(startIndex))
      return CLIP_ERROR_INVALID_FRAME_NUMBER;

   if (itemCount > totalItemCount)
      return -1;
   if (itemCount == -1)
      itemCount = totalItemCount - startIndex;

   list<S5RangeCover> coverSet;
   retVal = FindCoveringSet(startIndex, itemCount, coverSet);
   if (retVal != 0)
      return retVal;

   list<S5RangeCover>::iterator iter;
   list<S5RangeCover>::iterator endIter = coverSet.end();
   for(iter = coverSet.begin(); iter != endIter; ++iter)
      {
      C5Range *range = (*iter).rangeHandle.GetRange();
      if (range->GetRangeType() == CLIP5_RANGE_TYPE_FILLER)
         continue;

      int overlapStart, overlapEnd;
      if (!range->GetRangeIntersection(startIndex, itemCount, overlapStart,
                                       overlapEnd))
         continue;   // no intersection with caller's range, so skip
                     // NOTE: should never happen, an error?

      int overlapItemCount = overlapEnd - overlapStart + 1;
      overlapStart -= range->GetDstStartIndex();
      int count = 0;
      C5MediaPulldown *mediaPulldown = CastToMediaPulldown(range);
      retVal = mediaPulldown->LSF2(overlapStart, overlapItemCount, count);
      if (retVal != 0)
         return retVal;

      sum += count;
      }

   allocCount = sum;

   return 0;
}

//----------------------------------------------------------------------------

int C5MediaSequenceTrack::FreeMedia(const string &clipPath, int startIndex,
                                    int itemCount)
{
   int retVal;

   // Iterate over the source tracks
   RangeListIterator endIter = rangeList.end();
   for (RangeListIterator rangeIterator = rangeList.begin();
        rangeIterator != endIter;
        ++rangeIterator)
      {
      C5Range *range = *rangeIterator;
      E5RangeType rangeType = range->GetRangeType();
      if (rangeType == CLIP5_RANGE_TYPE_FILLER)
         continue;   // ignore filler range

      C5MediaPulldown *mediaPulldown = CastToMediaPulldown(range);

      int overlapStart, overlapEnd;
      if (!mediaPulldown->GetRangeIntersection(startIndex, itemCount,
                                               overlapStart, overlapEnd))
         continue;

      int overlapItemCount = overlapEnd - overlapStart + 1;
      overlapStart -= range->GetDstStartIndex();
      retVal = mediaPulldown->FreeMedia(clipPath, overlapStart,
                                        overlapItemCount);
      if (retVal != 0)
         return retVal;
      }

   return 0;

} // FreeMedia

//----------------------------------------------------------------------------

int C5MediaSequenceTrack::ClearMediaWritten(int startIndex, int itemCount)
{
   int retVal;

   // Iterate over the source tracks
   RangeListIterator endIter = rangeList.end();
   for (RangeListIterator rangeIterator = rangeList.begin();
        rangeIterator != endIter;
        ++rangeIterator)
      {
      C5Range *range = *rangeIterator;
      E5RangeType rangeType = range->GetRangeType();
      if (rangeType == CLIP5_RANGE_TYPE_FILLER)
         continue;   // ignore filler range

      C5MediaPulldown *mediaPulldown = CastToMediaPulldown(range);

      int overlapStart, overlapEnd;
      if (!mediaPulldown->GetRangeIntersection(startIndex, itemCount,
                                               overlapStart, overlapEnd))
         continue;

      int overlapItemCount = overlapEnd - overlapStart + 1;
      overlapStart -= range->GetDstStartIndex();

      retVal = mediaPulldown->ClearMediaWritten(overlapStart, overlapItemCount);
      if (retVal != 0)
         return retVal;
      }

   return 0;

} // ClearMediaWritten

// -----------------------------------------------------------------------

int C5MediaSequenceTrack::UpdateMediaStatusTrackFile(const string &clipPath)
{
   int retVal;

   // Iterate over the source tracks
   RangeListIterator endIter = rangeList.end();
   for (RangeListIterator rangeIterator = rangeList.begin();
        rangeIterator != endIter;
        ++rangeIterator)
      {
      C5Range *range = *rangeIterator;
      E5RangeType rangeType = range->GetRangeType();
      if (rangeType == CLIP5_RANGE_TYPE_FILLER)
         continue;   // ignore filler range

      C5MediaPulldown *mediaPulldown = CastToMediaPulldown(range);

      retVal = mediaPulldown->GetSrcMediaTrack().UpdateMediaStatusTrackFile(clipPath);
      if (retVal != 0)
         return retVal;
      }

   return 0;

} // UpdateMediaStatusTrackFile

int C5MediaSequenceTrack::ReloadMediaStatusTrackFile(const string &clipPath)
{
   int retVal;

   // Iterate over the source tracks
   RangeListIterator endIter = rangeList.end();
   for (RangeListIterator rangeIterator = rangeList.begin();
        rangeIterator != endIter;
        ++rangeIterator)
      {
      C5Range *range = *rangeIterator;
      E5RangeType rangeType = range->GetRangeType();
      if (rangeType == CLIP5_RANGE_TYPE_FILLER)
         continue;   // ignore filler range

      C5MediaPulldown *mediaPulldown = CastToMediaPulldown(range);

      retVal = mediaPulldown->GetSrcMediaTrack().ReloadMediaStatusTrackFile(clipPath);
      if (retVal != 0)
         return retVal;
      }

   return 0;

} // ReloadMediaStatusTrackFile

// ===========================================================================
// ===========================================================================
// C5AudioSequenceTrack

const CAudioFormat *C5AudioSequenceTrack::GetAudioFormat() const
{
   const CMediaFormat* mediaFormat = GetMediaFormat();

   if (mediaFormat == nullptr ||
       mediaFormat->getMediaFormatType() != MEDIA_FORMAT_TYPE_AUDIO3)
      {
      // TBD: this should be an exception, as it should never happen
      return 0;
      }

   return static_cast<const CAudioFormat*>(mediaFormat);
}

// ---------------------------------------------------------------------------

int C5AudioSequenceTrack::GetSampleCountAtField(int frameIndex, int fieldIndex)
{
   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

int C5AudioSequenceTrack::GetSampleCountAtFrame(int frameIndex)
{
   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

// ---------------------------------------------------------------------------

int C5AudioSequenceTrack::ReadMediaBySample(int index, int sampleOffset,
                                            MTI_INT64 sampleCount,
                                            MTI_UINT8 *buffer)
{
   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

int C5AudioSequenceTrack::ReadMediaBySampleMT(int index, int sampleOffset,
                                              MTI_INT64 sampleCount,
                                              MTI_UINT8 *buffer,
                                              int fileHandleIndex)
{
   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

int C5AudioSequenceTrack::WriteMediaBySample(int index, int sampleOffset,
                                             MTI_INT64 sampleCount,
                                             MTI_UINT8 *buffer,
                                             CTimeTrack *ttpAudioProxy)
{
   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

// ===========================================================================
// ===========================================================================
// C5VideoSequenceTrack
//
// The C5VideoSequenceTrack provides a video-specific interface
// to the C5SequenceTrack class.

const CImageFormat *C5VideoSequenceTrack::GetImageFormat() const
{
   const CMediaFormat* mediaFormat = GetMediaFormat();

   if (mediaFormat == nullptr ||
       mediaFormat->getMediaFormatType() != MEDIA_FORMAT_TYPE_VIDEO3)
      {
      // TBD: this should be an exception, as it should never happen
      return 0;
      }

   return static_cast<const CImageFormat*>(mediaFormat);
}

// ===========================================================================
// ===========================================================================
// C5SMPTETimecodeSequenceTrack

C5SMPTETimecodePulldown* C5SMPTETimecodeSequenceTrack::CastToSMPTETimecodePulldown(C5Range *range)
{
   if (range == nullptr ||
       range->GetRangeType() != CLIP5_RANGE_TYPE_PULLDOWN ||
       range->GetContentType() != CLIP5_CONTENT_TYPE_SMPTE_TIMECODE)
      {
      throw(std::runtime_error("Bad clip 5 range cast"));
      }

   return static_cast<C5SMPTETimecodePulldown*>(range);
}

int C5SMPTETimecodeSequenceTrack::GetTimecode(int index, CSMPTETimecode &timecode,
                                              float &partialFrame)
{
   int retVal;

   // Init caller's args to dummy values in case anything goes wrong
   timecode.setToDummyValue();
   partialFrame = 0.0;

   if (!IsIndexValid(index))
      return CLIP_ERROR_INVALID_FRAME_NUMBER;

   // Find the Segment with the caller's frame
   C5Range *range = FindRange(index);
   if (range == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   if (range->GetRangeType() == CLIP5_RANGE_TYPE_FILLER)
      return CLIP_ERROR_NO_MEDIA;

   C5SMPTETimecodePulldown *pulldown = CastToSMPTETimecodePulldown(range);

   int rangeIndex = index - range->GetDstStartIndex();
   retVal = pulldown->GetTimecode(rangeIndex, timecode, partialFrame);
   if (retVal != 0)
      return retVal;

   return 0;

} // GetTimecode

int C5SMPTETimecodeSequenceTrack::GetTimecodeFrameRate() const
{
   // Get first non-filler range and ask for timecode frame rate
   int timecodeFrameRate = 0;

   // Iterate over the ranges in the track, searching for the first non-filler
   for (RangeList::const_iterator iter = rangeList.begin();
        iter != rangeList.end(); ++iter)
      {
      C5Range *range = *iter;
      if (range->GetRangeType() == CLIP5_RANGE_TYPE_FILLER)
         continue;   // skip over filler range

      C5SMPTETimecodePulldown *pulldown = CastToSMPTETimecodePulldown(range);
      timecodeFrameRate = pulldown->GetTimecodeFrameRate();

      break;
      }

   return timecodeFrameRate;
}

EFrameRate C5SMPTETimecodeSequenceTrack::GetTimecodeFrameRateEnum() const
{
   // Get first non-filler range and ask for timecode frame rate
   EFrameRate timecodeFrameRate = IF_FRAME_RATE_INVALID;

   // Iterate over the ranges in the track, searching for the first non-filler
   for (RangeList::const_iterator iter = rangeList.begin();
        iter != rangeList.end(); ++iter)
      {
      C5Range *range = *iter;
      if (range->GetRangeType() == CLIP5_RANGE_TYPE_FILLER)
         continue;   // skip over filler range

      C5SMPTETimecodePulldown *pulldown = CastToSMPTETimecodePulldown(range);
      timecodeFrameRate = pulldown->GetTimecodeFrameRateEnum();

      break;
      }

   return timecodeFrameRate;
}

EFrameRate C5SMPTETimecodeSequenceTrack::GetTimecodeFrameRateEnum(int index)
{
   // Function not implemented yet
   return IF_FRAME_RATE_INVALID;
}

// ----------------------------------------------------------------------------

int C5SMPTETimecodeSequenceTrack::SetTimecode(int index,
                                              const CSMPTETimecode &newTimecode,
                                              float partialFrame)
{
   int retVal;

   if (!IsIndexValid(index))
      return CLIP_ERROR_INVALID_FRAME_NUMBER;

   // Find the Segment with the caller's frame
   C5Range *range = FindRange(index);
   if (range == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   if (range->GetRangeType() == CLIP5_RANGE_TYPE_FILLER)
      return CLIP_ERROR_NO_MEDIA;

   C5SMPTETimecodePulldown *pulldown = CastToSMPTETimecodePulldown(range);

   int rangeIndex = index - range->GetDstStartIndex();
   retVal = pulldown->SetTimecode(rangeIndex, newTimecode, partialFrame);
   if (retVal != 0)
      return retVal;

   return 0;

}

int C5SMPTETimecodeSequenceTrack::SetTimecodeX(int index, int count,
                                              const CSMPTETimecode &newTimecode,
                                               float partialFrame)
{
   int retVal;

   if (!IsIndexValid(index))
      return CLIP_ERROR_INVALID_FRAME_NUMBER;

   // NOTE: This  code isn't quite right, should be creating a list of ranges
   //       rather than the single segment like in SetToDummyValue
   
   // Find the Segment with the caller's frame
   C5Range *range = FindRange(index);
   if (range == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   if (range->GetRangeType() == CLIP5_RANGE_TYPE_FILLER)
      return CLIP_ERROR_NO_MEDIA;

   C5SMPTETimecodePulldown *pulldown = CastToSMPTETimecodePulldown(range);

   int rangeIndex = index - range->GetDstStartIndex();
   retVal = pulldown->SetTimecodeX(rangeIndex, count, newTimecode, partialFrame);
   if (retVal != 0)
      return retVal;

   return 0;

}

int C5SMPTETimecodeSequenceTrack::SetTimecodeFrameRate(EFrameRate newFrameRate)
{
   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

int C5SMPTETimecodeSequenceTrack::SetToDummyValue(int startIndex, int itemCount)
{
   // Set a range of frames to a dummy timecode value
   int retVal;

   // Iterate over the source tracks
   RangeListIterator endIter = rangeList.end();
   for (RangeListIterator rangeIterator = rangeList.begin();
        rangeIterator != endIter;
        ++rangeIterator)
      {
      C5Range *range = *rangeIterator;
      E5RangeType rangeType = range->GetRangeType();
      if (rangeType == CLIP5_RANGE_TYPE_FILLER)
         continue;   // ignore filler range

      C5SMPTETimecodePulldown *pulldown = CastToSMPTETimecodePulldown(range);

      int overlapStart, overlapEnd;
      if (!pulldown->GetRangeIntersection(startIndex, itemCount,
                                          overlapStart, overlapEnd))
         continue;

      int overlapItemCount = overlapEnd - overlapStart + 1;
      overlapStart -= range->GetDstStartIndex();

      retVal = pulldown->SetToDummyValue(overlapStart, overlapItemCount);
      if (retVal != 0)
         return retVal;
      }

   return 0;

} // SetToDummyValue

// ===========================================================================
// ===========================================================================
// C5TimecodeTrack
#ifdef C5_TIMECODE

C5TimecodeTrack::C5TimecodeTrack()
// : C5Track(CLIP5_TRACK_TYPE_TIMECODE, CLIP5_CONTENT_TYPE_TIMECODE)
{
}

C5TimecodeTrack::~C5TimecodeTrack()
{
}

// -----------------------------------------------------------------------

CTimecode C5TimecodeTrack::GetTimecode(int index)
{
   return CTimecode(0);  // TTT DUMMY RETURN
}

// -----------------------------------------------------------------------

void C5TimecodeTrack::writeXMLStream(CXMLStream &xmlStream)
{
}
#endif
// ===========================================================================

C5MediaStatusTrack::C5MediaStatusTrack()
// : C5Track(CLIP5_TRACK_TYPE_MEDIA_STATUS, CLIP5_CONTENT_TYPE_NONE)
{
}

C5MediaStatusTrack::~C5MediaStatusTrack()
{
}

// -----------------------------------------------------------------------

E5MediaStatus C5MediaStatusTrack::GetMediaStatus(int index)
{
   // Make reading media status track thread safe.  Another thread
   // may be Updating the media status track which may delete or create
   // media status range entries on the track's range list.  This
   // cause problems when iterating over the range list (STL containers
   // are not thread-safe).
   CAutoThreadLocker atl(updateThreadLock);

   C5RangeHandle rangeHandle = FindRangeHandle(index);
   C5MediaStatusRange *block = GetMediaStatusBlock(rangeHandle);
   if (block == nullptr)
      return CLIP5_MEDIA_STATUS_INVALID;

   return block->mediaStatus;
}

C5MediaStatusRange* C5MediaStatusTrack::GetMediaStatusBlock(C5RangeHandle &rangeHandle)
{
   if (IsEnd(rangeHandle))
      return 0;

   C5Range* range = rangeHandle.GetRange();
   if (range == nullptr)
      return 0;

   if (range->GetRangeType() != CLIP5_RANGE_TYPE_MEDIA_STATUS)
      return 0; // Unexpected range type

   return static_cast<C5MediaStatusRange*>(range);
}

// -----------------------------------------------------------------------

int C5MediaStatusTrack::LSF2(int startIndex, int itemCount, int &allocCount)
{
   int retVal;

   allocCount = 0;

   int totalItemCount = GetItemCount();

   if (!IsIndexValid(startIndex))
      return CLIP_ERROR_INVALID_FRAME_NUMBER;

   if (itemCount > totalItemCount - startIndex)
      itemCount = totalItemCount - startIndex;
   if (itemCount == -1)
      itemCount = totalItemCount - startIndex;

   list<S5RangeCover> coverSet;
   retVal = FindCoveringSet(startIndex, itemCount, coverSet);
   if (retVal != 0)
      return retVal;

   list<S5RangeCover>::iterator iter;
   list<S5RangeCover>::iterator endIter = coverSet.end();
   for(iter = coverSet.begin(); iter != endIter; ++iter)
      {
      C5Range *range = (*iter).rangeHandle.GetRange();
      if (range->GetRangeType() != CLIP5_RANGE_TYPE_MEDIA_STATUS)
         return -1;   // Not a media status block, so an error

      C5MediaStatusRange *mediaStatusRange
                                     = static_cast<C5MediaStatusRange*>(range);
      if (mediaStatusRange->GetMediaStatus(0) < CLIP5_MEDIA_STATUS_ALLOCATED)
         continue;

      int overlapStart, overlapEnd;
      if (!range->GetRangeIntersection(startIndex, itemCount, overlapStart,
                                       overlapEnd))
         continue;   // no intersection with caller's range, so skip
                     // NOTE: should never happen, an error?

      allocCount += overlapEnd - overlapStart + 1;
      }

   return 0;
}

// -----------------------------------------------------------------------

int C5MediaStatusTrack::SetMediaStatus(int index, E5MediaStatus newMediaStatus)
{
   int retVal;

   retVal = SetMediaStatus(index, 1, newMediaStatus, true, true);
   if (retVal != 0)
      return retVal;

   return 0;

} // SetMediaStatus

// -----------------------------------------------------------------------

int C5MediaStatusTrack::ClearMediaWritten(int startIndex, int itemCount)
{
   int retVal;

   retVal = SetMediaStatus(startIndex, itemCount, CLIP5_MEDIA_STATUS_ALLOCATED,
                           true, false);
   if (retVal != 0)
      return 0;

   return 0;
}

// --------------------------------------------------------------------------

int C5MediaStatusTrack::SetMediaStatus(int startIndex, int itemCount,
                                       E5MediaStatus newStatus,
                                       bool allowStatusDecrease,
                                       bool allowStatusIncrease)
{
   int retVal;

   // Make update to media status track thread safe.  Updating the
   // media status track may delete or create media status range
   // entries on the track's range list, which will confuse another
   // thread that is iterating over the range list
   CAutoThreadLocker atl(updateThreadLock);

   list<S5RangeCover> coverSet;
   retVal = FindCoveringSet(startIndex, itemCount, coverSet);
   if (retVal != 0)
      return retVal;

   list<S5RangeCover>::iterator iter;
   list<S5RangeCover>::iterator endIter = coverSet.end();
   for(iter = coverSet.begin(); iter != endIter; ++iter)
      {
      C5Range *range = (*iter).rangeHandle.GetRange();
      if (range->GetRangeType() != CLIP5_RANGE_TYPE_MEDIA_STATUS)
         return -1;   // Not a media status block, so an error

      C5MediaStatusRange *mediaStatusRange
                                     = static_cast<C5MediaStatusRange*>(range);
      E5MediaStatus existingMediaStatus = mediaStatusRange->GetMediaStatus(0);
      if (existingMediaStatus == newStatus ||
          (!allowStatusDecrease && existingMediaStatus > newStatus) ||
          (!allowStatusIncrease && existingMediaStatus < newStatus))
         continue;   // Don't change this range's status

      int overlapStart, overlapEnd;
      if (!range->GetRangeIntersection(startIndex, itemCount, overlapStart,
                                       overlapEnd))
         continue;   // no intersection with caller's range, so skip
                     // NOTE: should never happen, an error?

      int dstStart = range->GetDstStartIndex();
      int count1 = overlapStart - dstStart;
      int count2 = overlapEnd - overlapStart + 1;
      int count3 = dstStart + range->GetItemCount() - 1 - overlapEnd;

      C5RangeHandle nextRangeHandle = (*iter).rangeHandle.Next();

      if (count1 > 0)
         {
         // Adjust the current media status block's size
         mediaStatusRange->SetItemCount(count1);
         }
      else
         {
         // Remove the current media status block
         RemoveRange((*iter).rangeHandle);
         }
      if (count2 > 0)
         {
         // Insert new range with new media status
         mediaStatusRange = new C5MediaStatusRange;
         mediaStatusRange->SetDstStartIndex(dstStart + count1);
         mediaStatusRange->SetItemCount(count2);
         mediaStatusRange->SetMediaStatus(newStatus);
         InsertBefore(nextRangeHandle, mediaStatusRange);
         }
      if (count3 > 0)
         {
         // Insert new range with existing media status
         mediaStatusRange = new C5MediaStatusRange;
         mediaStatusRange->SetDstStartIndex(dstStart + count1 + count2);
         mediaStatusRange->SetItemCount(count3);
         mediaStatusRange->SetMediaStatus(existingMediaStatus);
         InsertBefore(nextRangeHandle, mediaStatusRange);
         }
      }

   // Merge the adjacent media status blocks with the same media status
   if (startIndex > 0)
      {
      --startIndex;
      ++itemCount;
      }
   if (startIndex + itemCount < GetItemCount())
      ++itemCount;

   retVal = FindCoveringSet(startIndex, itemCount, coverSet);
   if (retVal != 0)
      return retVal;

   iter = coverSet.begin();
   endIter = coverSet.end();
   list<S5RangeCover>::iterator nextIter;
   while(iter != endIter)
      {
      C5Range *range = (*iter).rangeHandle.GetRange();
      nextIter = iter;
      ++nextIter;
      while (nextIter != endIter)
         {
         C5Range *nextRange = (*nextIter).rangeHandle.GetRange();
         C5MediaStatusRange *mediaStatusRange
                                  = static_cast<C5MediaStatusRange*>(range);
         C5MediaStatusRange *nextMediaStatusBlock
                                  = static_cast<C5MediaStatusRange*>(nextRange);
         if (mediaStatusRange->GetMediaStatus(0)
                                     == nextMediaStatusBlock->GetMediaStatus(0))
            {
            // Merge
            mediaStatusRange->SetItemCount(mediaStatusRange->GetItemCount()
                                        + nextMediaStatusBlock->GetItemCount());

            // Remove
            RemoveRange((*nextIter).rangeHandle);

            ++nextIter;
            }
         else
            {
            break;
            }
         }

      iter = nextIter;
      }

   // Remember that the track has changed
   SetNeedsUpdate(true);

   return 0;
}

//----------------------------------------------------------------------------

int C5MediaStatusTrack::ExtendMediaStatusTrack(int newItemCount)
{
   int retVal;
   
   if (newItemCount < 1)
      return 0;

   int currentItemCount = GetItemCount();
   // Add one Media status range with "unallocated" status and length of
   // caller's itemCount
   C5MediaStatusRange *mediaStatusRange = new C5MediaStatusRange;
   mediaStatusRange->SetItemCount(newItemCount);
   mediaStatusRange->SetDstStartIndex(currentItemCount);

   AppendRange(mediaStatusRange);

   retVal = SetMediaStatus(currentItemCount, newItemCount,
                           CLIP5_MEDIA_STATUS_UNALLOCATED, true, true);
   if (retVal != 0)
      return retVal;

   return 0;
}

//----------------------------------------------------------------------------

void C5MediaStatusTrack::writeXMLStream(CXMLStream &xmlStream)
{
   // Make reading media status track thread safe.  Another thread
   // may be Updating the media status track which may delete or create
   // media status range entries on the track's range list.  This
   // cause problems when iterating over the range list (STL containers
   // are not thread-safe).
   CAutoThreadLocker atl(updateThreadLock);

   C5Track::writeXMLStream(xmlStream);

   xmlStream << endTag();  // End Track tag
}

// -----------------------------------------------------------------------

int C5MediaStatusTrack::GatherMediaStatus(CVideoProxy *videoProxy)
{
   CVideoFrameList *frameList = videoProxy->getVideoFrameList(0);
   if (frameList == nullptr)
      return 0;

   int frameCount = frameList->getMediaAllocatedFrameCount();
   int totalFieldCount = 0;
   for (int frameIndex = 0; frameIndex < frameCount; ++frameIndex)
      {
      CVideoFrame *frame = frameList->getFrame(frameIndex);
      totalFieldCount += frame->getTotalFieldCount();
      }

   // Add single entry with total number of fields in track
   C5MediaStatusRange *firstBlock = new C5MediaStatusRange;
   firstBlock->SetItemCount(totalFieldCount);
   firstBlock->SetMediaStatus(CLIP5_MEDIA_STATUS_UNALLOCATED);
   AppendRange(firstBlock);

   int currentFieldIndex = 0;
   for (int frameIndex = 0; frameIndex < frameCount; ++frameIndex)
      {
      CVideoFrame *frame = frameList->getFrame(frameIndex);
      int frameFieldCount = frame->getTotalFieldCount();
      for (int fieldIndex = 0; fieldIndex < frameFieldCount; ++fieldIndex)
         {
         CVideoField *field = frame->getField(fieldIndex);
         E5MediaStatus mediaStatus;
         if (!field->doesMediaStorageExist())
            mediaStatus = CLIP5_MEDIA_STATUS_UNALLOCATED;
         else if (!field->isMediaWritten())
            mediaStatus = CLIP5_MEDIA_STATUS_ALLOCATED;
         else
            mediaStatus = CLIP5_MEDIA_STATUS_WRITTEN;
            
         SetMediaStatus(currentFieldIndex, mediaStatus);

         ++currentFieldIndex;
         }
      }

   SetNeedsUpdate(true);   // remember that the track has changed

   return 0;
}

// ===========================================================================
// ===========================================================================
// SMPTE Timecode Track

C5SMPTETimecodeTrack::C5SMPTETimecodeTrack()
 : timecodeFrameRate(IF_FRAME_RATE_INVALID)
{
}

C5SMPTETimecodeTrack::~C5SMPTETimecodeTrack()
{
}

// ---------------------------------------------------------------------------

int C5SMPTETimecodeTrack::GetTimecodeFrameRate() const
{
   return (int)(CImageInfo::queryStandardFrameRate(timecodeFrameRate) + .5);
}

EFrameRate C5SMPTETimecodeTrack::GetTimecodeFrameRateEnum() const
{
   return timecodeFrameRate;
}

EFrameRate C5SMPTETimecodeTrack::GetTimecodeFrameRateEnum(int index)
{
   int retVal;

   if (!IsIndexValid(index))
      return IF_FRAME_RATE_INVALID;

   C5Range *range = FindRange(index);
   if (range == nullptr)
      return IF_FRAME_RATE_INVALID;

   if (range->GetRangeType() != CLIP5_RANGE_TYPE_SMPTE_TIMECODE ||
       range->GetContentType() != CLIP5_CONTENT_TYPE_SMPTE_TIMECODE)
      return IF_FRAME_RATE_INVALID;  // should be exception

   C5SMPTETimecodeRange *tcRange = static_cast<C5SMPTETimecodeRange*>(range);

   return tcRange->GetTimecodeFrameRateEnum();
}

int C5SMPTETimecodeTrack::GetTimecode(int index, CSMPTETimecode &timecode,
                                      float &partialFrame)
{
   int retVal;

   timecode.setToDummyValue();
   partialFrame = 0.0;

   if (!IsIndexValid(index))
      return CLIP_ERROR_INVALID_FRAME_NUMBER;

   C5Range *range = FindRange(index);
   if (range == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   if (range->GetRangeType() != CLIP5_RANGE_TYPE_SMPTE_TIMECODE ||
       range->GetContentType() != CLIP5_CONTENT_TYPE_SMPTE_TIMECODE)
      return -1;  // should be exception

   C5SMPTETimecodeRange *tcRange = static_cast<C5SMPTETimecodeRange*>(range);

   retVal = tcRange->GetTimecode(index - range->GetDstStartIndex(),
                                 timecode, partialFrame);
   if (retVal != 0)
      return retVal;

   return 0;
}

// ---------------------------------------------------------------------------

int C5SMPTETimecodeTrack::SetTimecodeFrameRate(EFrameRate newFrameRate)
{
   timecodeFrameRate = newFrameRate;

   SetNeedsUpdate(true);

   return 0;
}

int C5SMPTETimecodeTrack::SetTimecode(int index,
                                      const CSMPTETimecode &newTimecode,
                                      float partialFrame)
{
   int retVal;

   retVal = SetTimecodeX(index, 1, newTimecode, partialFrame);
   if (retVal != 0)
      return 0;

   return 0;
}

// --------------------------------------------------------------------------

int C5SMPTETimecodeTrack::SetTimecodeX(int startIndex, int itemCount,
                                       const CSMPTETimecode &newTimecode,
                                       float newPartialFrame)
{
   int retVal;

   list<S5RangeCover> coverSet;
   retVal = FindCoveringSet(startIndex, itemCount, coverSet);
   if (retVal != 0)
      return retVal;

   list<S5RangeCover>::iterator iter;
   list<S5RangeCover>::iterator endIter = coverSet.end();
   for(iter = coverSet.begin(); iter != endIter; ++iter)
      {
      C5Range *range = (*iter).rangeHandle.GetRange();
      if (range->GetRangeType() != CLIP5_RANGE_TYPE_SMPTE_TIMECODE)
         return -1;   // Not a timecode block, so an error

      C5SMPTETimecodeRange *timecodeRange
                                   = static_cast<C5SMPTETimecodeRange*>(range);

      int overlapStart, overlapEnd;
      if (!range->GetRangeIntersection(startIndex, itemCount, overlapStart,
                                       overlapEnd))
         continue;   // no intersection with caller's range, so skip
                     // NOTE: should never happen, an error?

      int dstStart = range->GetDstStartIndex();
      int count1 = overlapStart - dstStart;

      // If value is already set, skip over this range
      CSMPTETimecode tmpTC;
      float tmpPartial;
      retVal = timecodeRange->GetTimecode(count1, tmpTC, tmpPartial);
      if (newTimecode.isEqualTo(tmpTC, false)
          && abs(newPartialFrame - tmpPartial) <= .001
          && timecodeRange->GetTimecodeFrameRateEnum() == GetTimecodeFrameRate())
         continue;

      int count2 = overlapEnd - overlapStart + 1;
      int count3 = dstStart + range->GetItemCount() - 1 - overlapEnd;
      int dstStart2 = dstStart + count1;
      int dstStart3 = dstStart + count1 + count2;

      CSMPTETimecode tc2, tc3;
      float partialFrame2, partialFrame3;
      EFrameRate tc3FrameRateEnum;
      if (count2 > 0)
         {
         tc2 = newTimecode;
         partialFrame2 = newPartialFrame;
         }
      if (count3 > 0)
         {
         retVal = GetTimecode(dstStart3, tc3, partialFrame3);
         if (retVal != 0)
            return retVal;
         tc3FrameRateEnum = GetTimecodeFrameRateEnum(dstStart3);
         }

      C5RangeHandle nextRangeHandle = (*iter).rangeHandle.Next();

      if (count1 > 0)
         {
         // Adjust the current media status block's size
         range->SetItemCount(count1);
         }
      else
         {
         // Remove the current media status block
         RemoveRange((*iter).rangeHandle);
         }
      if (count2 > 0)
         {
         // Insert new range with new media status
         C5SMPTETimecodeRange *newRange = new C5SMPTETimecodeRange;
         newRange->SetDstStartIndex(dstStart2);
         newRange->SetItemCount(count2);
         newRange->SetTimecodeFrameRate(GetTimecodeFrameRateEnum());
         newRange->SetTrackEditRate(GetEditRateEnum());
         newRange->SetStartTimecode(tc2);
         newRange->SetStartPartialFrame(partialFrame2);
         InsertBefore(nextRangeHandle, newRange);
         }
      if (count3 > 0)
         {
         // Insert new range with existing media status
         C5SMPTETimecodeRange *newRange = new C5SMPTETimecodeRange;
         newRange->SetDstStartIndex(dstStart3);
         newRange->SetItemCount(count3);
         newRange->SetTimecodeFrameRate(tc3FrameRateEnum);
         newRange->SetTrackEditRate(GetEditRateEnum());
         newRange->SetStartTimecode(tc3);
         newRange->SetStartPartialFrame(partialFrame3);
         InsertBefore(nextRangeHandle, newRange);
         }
      }

   // Merge the adjacent timecode blocks
   if (startIndex > 0)
      {
      --startIndex;
      ++itemCount;
      }
   if (startIndex + itemCount < GetItemCount())
      ++itemCount;

   retVal = FindCoveringSet(startIndex, itemCount, coverSet);
   if (retVal != 0)
      return retVal;

   iter = coverSet.begin();
   endIter = coverSet.end();
   list<S5RangeCover>::iterator nextIter;
   while(iter != endIter)
      {
      C5Range *range = (*iter).rangeHandle.GetRange();
      C5SMPTETimecodeRange *tcRange = static_cast<C5SMPTETimecodeRange*>(range);

      nextIter = iter;
      ++nextIter;
      while (nextIter != endIter)
         {
         // Get timecode one past end of this range
         int endIndex = range->GetDstStartIndex() + range->GetItemCount();
         CSMPTETimecode rangeTC;
         float rangePartialFrame;
         retVal = tcRange->GetTimecode(endIndex-range->GetDstStartIndex(),
                                       rangeTC, rangePartialFrame);
         if (retVal != 0)
            return retVal;

         C5Range *nextRange = (*nextIter).rangeHandle.GetRange();
         C5SMPTETimecodeRange *nextTCRange
                                = static_cast<C5SMPTETimecodeRange*>(nextRange);
         CSMPTETimecode nextTC;
         float nextPartialFrame;
         retVal = GetTimecode(endIndex, nextTC, nextPartialFrame);
         if (retVal != 0)
            return retVal;

         if (rangeTC.isEqualTo(nextTC, false)
             && abs(rangePartialFrame - nextPartialFrame) <= .001
             && tcRange->GetTimecodeFrameRateEnum() == nextTCRange->GetTimecodeFrameRateEnum())
            {
            // Merge
            range->SetItemCount(range->GetItemCount()
                                + nextRange->GetItemCount());

            // Remove
            RemoveRange((*nextIter).rangeHandle);

            ++nextIter;
            }
         else
            {
            break;
            }
         }

      iter = nextIter;
      }

   return 0;

} // SetTimecode

// --------------------------------------------------------------------------

int C5SMPTETimecodeTrack::SetToDummyValue(int startIndex, int itemCount)
{
   int retVal;

   CSMPTETimecode dummyTC;
   dummyTC.setToDummyValue();

   retVal = SetTimecodeX(startIndex, itemCount, dummyTC, 0.0);
   if (retVal != 0)
      return 0;

   return 0;
}

//----------------------------------------------------------------------------

int C5SMPTETimecodeTrack::initFromXMLAttributes(C5XMLAttribList &attribList)
{
   int retVal;
   string valueString, emptyString;
   string invalidValueString = "INVALID";

   // Get the timecode rate (frames per second)
   if (!attribList.DoesNameExist(timecodeFrameRateAttr))
      {
      // ERROR: missing required attribute
      return -1;
      }
   valueString = attribList.GetValueString(timecodeFrameRateAttr, emptyString);
   double newTimecodeFrameRate;
   EFrameRate newTimecodeFrameRateEnum;
   newTimecodeFrameRateEnum = CImageInfo::queryFrameRate(valueString,
                                                         newTimecodeFrameRate);
   if (newTimecodeFrameRateEnum == IF_FRAME_RATE_INVALID ||
       newTimecodeFrameRateEnum == IF_FRAME_RATE_NON_STANDARD)
      return CLIP_ERROR_INVALID_FRAME_RATE;

   retVal = C5Track::initFromXMLAttributes(attribList);
   if (retVal != 0)
      return retVal;

   timecodeFrameRate = newTimecodeFrameRateEnum;

   return 0;

}  // initFromXMLAttributes

// ---------------------------------------------------------------------------

void C5SMPTETimecodeTrack::writeXMLAttributes(CXMLStream &xmlStream)
{
   C5Track::writeXMLAttributes(xmlStream);

   xmlStream << attr(timecodeFrameRateAttr)
             << CImageInfo::queryFrameRateName(GetTimecodeFrameRateEnum());
}

void C5SMPTETimecodeTrack::writeXMLStream(CXMLStream &xmlStream)
{
   C5Track::writeXMLStream(xmlStream);

   xmlStream << endTag();  // End Track tag
}

// ---------------------------------------------------------------------------
// Extend an existing SMPTE timecode track

int C5SMPTETimecodeTrack::ExtendTimeTrack(const string &clipPath,
                                          int newItemCount)
{
   int retVal;

   if (newItemCount <= 0)
      return 0;   // nothing to do

   bool addNewRange = true;
   int currentItemCount = GetItemCount();
   if (currentItemCount > 0)
      {
      CSMPTETimecode lastTC;
      float lastPartialFrame;
      retVal = GetTimecode(currentItemCount - 1, lastTC, lastPartialFrame);
      if (retVal != 0)
         return retVal;

      if (lastTC.isDummyValue())
         addNewRange = false;
      }

   if (addNewRange)
      {
      // Add new dummy range
      C5SMPTETimecodeRange *timecodeRange = new C5SMPTETimecodeRange;
      timecodeRange->SetDstStartIndex(currentItemCount);
      timecodeRange->SetItemCount(newItemCount);
      timecodeRange->SetTimecodeFrameRate(GetTimecodeFrameRateEnum());
      timecodeRange->SetTrackEditRate(GetEditRateEnum());
      timecodeRange->SetToDummyValue();

      AppendRange(timecodeRange);
      }
   else
      {
      // The last range already contains dummy values, so we can
      // just increase the length of the last range
      C5Range* lastRange = FindRange(currentItemCount - 1);
      if (lastRange == nullptr)
         return -1;
      lastRange->SetItemCount(lastRange->GetItemCount() + newItemCount);
      }

   // Write the track file
   retVal = WriteTrackFile(clipPath);
   if (retVal != 0)
      {
      // TBD: Cleanup
      // TBD: Error code
      return retVal;
      }

   return 0;

} // ExtendTimeTrack

// ===========================================================================
// ===========================================================================
// Keycode Track

C5KeycodeTrack::C5KeycodeTrack()
{
}

C5KeycodeTrack::~C5KeycodeTrack()
{
}

// --------------------------------------------------------------------------

int C5KeycodeTrack::GetKeycode(int index, CMTIKeykode &keycode)
{
   int retVal;

   keycode.setToDummyValue();

   if (!IsIndexValid(index))
      return CLIP_ERROR_INVALID_FRAME_NUMBER;

   C5Range *range = FindRange(index);
   if (range == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   if (range->GetRangeType() != CLIP5_RANGE_TYPE_KEYCODE ||
       range->GetContentType() != CLIP5_CONTENT_TYPE_KEYCODE)
      return -1;  // should be exception

   C5KeycodeRange *kkRange = static_cast<C5KeycodeRange*>(range);

   retVal = kkRange->GetKeykode(index - range->GetDstStartIndex(), keycode);
   if (retVal != 0)
      return retVal;

   return 0;
}

// --------------------------------------------------------------------------

int C5KeycodeTrack::SetKeycode(int index, const CMTIKeykode &newKeycode)
{
   int retVal;

   retVal = SetKeycodeX(index, 1, newKeycode);
   if (retVal != 0)
      return 0;

   return 0;
}

int C5KeycodeTrack::SetKeycodeX(int startIndex, int itemCount,
                                const CMTIKeykode &newKeycode)
{
   int retVal;

   list<S5RangeCover> coverSet;
   retVal = FindCoveringSet(startIndex, itemCount, coverSet);
   if (retVal != 0)
      return retVal;

   list<S5RangeCover>::iterator iter;
   list<S5RangeCover>::iterator endIter = coverSet.end();
   for(iter = coverSet.begin(); iter != endIter; ++iter)
      {
      C5Range *range = (*iter).rangeHandle.GetRange();
      if (range->GetRangeType() != CLIP5_RANGE_TYPE_KEYCODE)
         return -1;   // Not a timecode block, so an error

      C5KeycodeRange *keycodeRange
                                   = static_cast<C5KeycodeRange*>(range);

      int overlapStart, overlapEnd;
      if (!range->GetRangeIntersection(startIndex, itemCount, overlapStart,
                                       overlapEnd))
         continue;   // no intersection with caller's range, so skip
                     // NOTE: should never happen, an error?

      int dstStart = range->GetDstStartIndex();
      int count1 = overlapStart - dstStart;

      // If value is already set, skip over this range
      CMTIKeykode tmpKK;
      retVal = keycodeRange->GetKeykode(count1, tmpKK);
      if (newKeycode.isEqualTo(tmpKK))
         continue;

      int count2 = overlapEnd - overlapStart + 1;
      int count3 = dstStart + range->GetItemCount() - 1 - overlapEnd;
      int dstStart2 = dstStart + count1;
      int dstStart3 = dstStart + count1 + count2;

      CMTIKeykode kk2, kk3;
      if (count2 > 0)
         {
         kk2 = newKeycode;
//         if (!kk2.isDummyValue())
//            {
//            kk2.addFrameOffset(dstStart2);
//            }
         }
      if (count3 > 0)
         {
         retVal = GetKeycode(dstStart3, kk3);
         if (retVal != 0)
            return retVal;
         }

      C5RangeHandle nextRangeHandle = (*iter).rangeHandle.Next();

      if (count1 > 0)
         {
         // Adjust the current media status block's size
         range->SetItemCount(count1);
         }
      else
         {
         // Remove the current media status block
         RemoveRange((*iter).rangeHandle);
         }
      if (count2 > 0)
         {
         // Insert new range with new media status
         C5KeycodeRange *newRange = new C5KeycodeRange;
         newRange->SetDstStartIndex(dstStart2);
         newRange->SetItemCount(count2);
         newRange->SetKeykode(kk2);
         InsertBefore(nextRangeHandle, newRange);
         }
      if (count3 > 0)
         {
         // Insert new range with existing media status
         C5KeycodeRange *newRange = new C5KeycodeRange;
         newRange->SetDstStartIndex(dstStart3);
         newRange->SetItemCount(count3);
         newRange->SetKeykode(kk3);
         InsertBefore(nextRangeHandle, newRange);
         }
      }

   // Merge the adjacent timecode blocks
   if (startIndex > 0)
      {
      --startIndex;
      ++itemCount;
      }
   if (startIndex + itemCount < GetItemCount())
      ++itemCount;

   retVal = FindCoveringSet(startIndex, itemCount, coverSet);
   if (retVal != 0)
      return retVal;

   iter = coverSet.begin();
   endIter = coverSet.end();
   list<S5RangeCover>::iterator nextIter;
   while(iter != endIter)
      {
      C5Range *range = (*iter).rangeHandle.GetRange();
      nextIter = iter;
      ++nextIter;
      while (nextIter != endIter)
         {
         CMTIKeykode kk, nextKK;
         C5Range *nextRange = (*nextIter).rangeHandle.GetRange();
         int endIndex = range->GetDstStartIndex() + range->GetItemCount();
         C5KeycodeRange *kkRange = static_cast<C5KeycodeRange*>(range);
         retVal = kkRange->GetKeykode(endIndex - range->GetDstStartIndex(), kk);
         if (retVal != 0)
            return 0;

         retVal = GetKeycode(endIndex, nextKK);
         if (retVal != 0)
            return 0;

         if (kk.isEqualTo(nextKK))
            {
            // Merge
            range->SetItemCount(range->GetItemCount()
                                + nextRange->GetItemCount());

            // Remove
            RemoveRange((*nextIter).rangeHandle);

            ++nextIter;
            }
         else
            {
            break;
            }
         }

      iter = nextIter;
      }

   return 0;

} // C5KeycodeTrack::SetKeycodeX

int C5KeycodeTrack::SetToDummyValue(int startIndex, int itemCount)
{
   int retVal;

   CMTIKeykode dummyKK;
   dummyKK.setToDummyValue();

   retVal = SetKeycodeX(startIndex, itemCount, dummyKK);
   if (retVal != 0)
      return 0;

   return 0;

} // C5KeycodeTrack::SetToDummyValue

// --------------------------------------------------------------------------

int C5KeycodeTrack::ExtendKeycodeTrack(const string &clipPath,
                                       int newItemCount)
{
   int retVal;

   if (newItemCount <= 0)
      return 0;   // nothing to do

   bool addNewRange = true;
   int currentItemCount = GetItemCount();
   if (currentItemCount > 0)
      {
      CMTIKeykode lastKK;
      retVal = GetKeycode(currentItemCount - 1, lastKK);
      if (retVal != 0)
         return retVal;

      if (lastKK.isDummyValue())
         addNewRange = false;
      }

   if (addNewRange)
      {
      // Add new dummy range
      C5KeycodeRange *keycodeRange = new C5KeycodeRange;
      keycodeRange->SetDstStartIndex(currentItemCount);
      keycodeRange->SetItemCount(newItemCount);
      keycodeRange->SetToDummyValue();

      AppendRange(keycodeRange);
      }
   else
      {
      // The last range already contains dummy values, so we can
      // just increase the length of the last range
      C5Range* lastRange = FindRange(currentItemCount - 1);
      if (lastRange == nullptr)
         return -1;
      lastRange->SetItemCount(lastRange->GetItemCount() + newItemCount);
      }

   // Write the track file
   retVal = WriteTrackFile(clipPath);
   if (retVal != 0)
      {
      // TBD: Cleanup
      // TBD: Error code
      return retVal;
      }

   return 0;

} // C5KeycodeTrack::ExtendKeycodeTrack


// --------------------------------------------------------------------------

void C5KeycodeTrack::writeXMLStream(CXMLStream &xmlStream)
{
   C5Track::writeXMLStream(xmlStream);

   xmlStream << endTag();  // End Track tag
}


void C5KeycodeTrack::Dump(ostream &ostrm)
{
   C5Track::Dump(ostrm);
}

// ===========================================================================
// ===========================================================================

C5RangeHandle::C5RangeHandle()
{
}

C5RangeHandle::C5RangeHandle(const C5RangeHandle &rhs)
 : iter(rhs.iter)
{
}

C5RangeHandle::C5RangeHandle(C5Track::RangeListIterator &rhs)
 : iter(rhs)
{
}

C5RangeHandle::~C5RangeHandle()
{
}

// -------------------------------------------------------------------------

C5RangeHandle& C5RangeHandle::operator=(const C5RangeHandle &rhs)
{
   if (this == &rhs)
      return *this;      // skip self-assignment

   iter = rhs.iter;

   return *this;
}

// -------------------------------------------------------------------------

C5RangeHandle& C5RangeHandle::operator++()          // ++handle
{
   ++iter;
   return *this;
}

C5RangeHandle& C5RangeHandle::operator--()          // --handle
{
   --iter;
   return *this;
}

C5RangeHandle C5RangeHandle::Next()
{
   C5RangeHandle nextHandle(*this);
   ++nextHandle;
   return nextHandle;
}

C5RangeHandle C5RangeHandle::Previous()
{
   C5RangeHandle previousHandle(*this);
   --previousHandle;
   return previousHandle;
}

// -------------------------------------------------------------------------

bool C5RangeHandle::operator==(C5RangeHandle &rhs)
{
   return (iter == rhs.iter);
}

bool C5RangeHandle::operator!=(C5RangeHandle &rhs)
{
   return (iter != rhs.iter);
}

bool C5RangeHandle::operator==(C5Track::RangeListIterator &rhs)
{
   return (iter == rhs);
}

bool C5RangeHandle::operator!=(C5Track::RangeListIterator &rhs)
{
   return (iter != rhs);
}

bool C5RangeHandle::operator==(C5Range *rhs)
{
   return (*iter == rhs);
}

bool C5RangeHandle::operator!=(C5Range *rhs)
{
   return (*iter != rhs);
}

// -------------------------------------------------------------------------

C5Range* C5RangeHandle::GetRange() const
{
   // This function may cause an exception if the iter does not
   // actually point to a valid list member
   return *iter;
}

// ===========================================================================

C5MediaStatusRange::C5MediaStatusRange()
 : mediaStatus(CLIP5_MEDIA_STATUS_INVALID)
{
}

C5MediaStatusRange::~C5MediaStatusRange()
{
}

// ------------------------------------------------------------------------

E5MediaStatus C5MediaStatusRange::GetMediaStatus(int index) const
{
   return mediaStatus;
}

void C5MediaStatusRange::SetMediaStatus(E5MediaStatus newMediaStatus)
{
   mediaStatus = newMediaStatus;
}

// -----------------------------------------------------------------------

int C5MediaStatusRange::initFromXMLAttributes(C5XMLAttribList &attribList)
{
   int retVal;
   string valueString;
   string invalidValueString = "INVALID";

   // Media Status
   if (!attribList.DoesNameExist(mediaStatusAttr))
      {
      // ERROR: missing required attribute
      return CLIP_ERROR_INVALID_MEDIA_STATUS;
      }
   valueString = attribList.GetValueString(mediaStatusAttr, invalidValueString);
   E5MediaStatus newMediaStatus = mediaStatusLookup.FindByStr(valueString,
                                                    CLIP5_MEDIA_STATUS_INVALID);
   if (newMediaStatus == CLIP5_MEDIA_STATUS_INVALID)
      {
      // ERROR: invalid required attribute
      return CLIP_ERROR_INVALID_MEDIA_STATUS;
      }

   mediaStatus = newMediaStatus;

   retVal = C5Range::initFromXMLAttributes(attribList);
   if (retVal != 0)
      return retVal;

   return 0;

}  // initFromXMLAttributes

// ------------------------------------------------------------------------

void C5MediaStatusRange::writeXMLStream(CXMLStream &xmlStream)
{
   C5Range::writeXMLStream(xmlStream);

//   xmlStream << tag(mediaStatusRangeTag);   // Start MediaStatusBlock
   xmlStream << attr(mediaStatusAttr)
             << escape(mediaStatusLookup.FindByValue(mediaStatus));
//   xmlStream << endTag();   // End MediaStatusBlock

   xmlStream << endTag();    // End Range
}

// ===========================================================================
// ===========================================================================

C5TrackHandle::C5TrackHandle()
 : initialized(false)
{
}

C5TrackHandle::C5TrackHandle(const C5TrackHandle &rhs)
 : initialized(rhs.initialized), registryEntry(rhs.registryEntry)
{
   IncrRefCount();
}

C5TrackHandle::C5TrackHandle(C5Track *track)
 : initialized(false)
{
   SetTrack(track);
}

C5TrackHandle::C5TrackHandle(const Clip5TrackID &trackID)
 : initialized(false)
{
   SetTrackID(trackID);
}

C5TrackHandle::~C5TrackHandle()
{
   DeleteRegistryEntry();  // deletes entry when ref count goes to zero
}

// ---------------------------------------------------------------------------

void C5TrackHandle::DeleteRegistryEntry()
{
   // Delete track registry entry when reference count goes to zero

   if (initialized)
      {
      C5TrackRegistry *registry = GetTrackRegistry();
      registry->DeleteEntry(registryEntry);
      initialized = false;
      }
}

// ---------------------------------------------------------------------------

C5TrackHandle& C5TrackHandle::operator=(const C5TrackHandle &rhs)
{
   if (this != &rhs)
      {
      DeleteRegistryEntry();  // delete registry entry if one exists

      initialized = rhs.initialized;
      registryEntry = rhs.registryEntry;

      IncrRefCount();
      }

   return *this;
}

void C5TrackHandle::IncrRefCount()
{
   if (initialized)
      {
      C5TrackRegistry *registry = GetTrackRegistry();
      registry->IncrRefCount(registryEntry);
      }
}

// ---------------------------------------------------------------------------

bool C5TrackHandle::operator==(const C5TrackHandle &rhs)
{
   if (this == &rhs)
      return true;

   return (initialized == rhs.initialized
           && registryEntry == rhs.registryEntry);
}

bool C5TrackHandle::operator!=(const C5TrackHandle &rhs)
{
   if (this == &rhs)
      return false;

   return (initialized != rhs.initialized
           || registryEntry != rhs.registryEntry);
}

// ---------------------------------------------------------------------------

bool C5TrackHandle::IsInitialized() const
{
   return initialized;
}

// ---------------------------------------------------------------------------

int C5TrackHandle::GetAllFieldList(int index, S5FieldListEntry fieldList[],
                                   int &count)
{
   int retVal;

   count = 0;

   if (!initialized)
      return CLIP_ERROR_NO_MEDIA;

   C5Track *track = GetTrack();
   if (track == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   retVal = track->GetAllFieldList(index, fieldList, count);
   if (retVal != 0)
      return retVal;

   return 0;
}

E5ContentType C5TrackHandle::GetContentType() const
{
   // Determine count of frames that have media allocated

   if (!initialized)
      return CLIP5_CONTENT_TYPE_INVALID;

   C5Track *track = GetTrack();
   if (track == nullptr)
      return CLIP5_CONTENT_TYPE_INVALID;

   return track->GetContentType();
}

double C5TrackHandle::GetEditRate() const
{
   if (!initialized)
      return 0.0;

   C5Track *track = GetTrack();
   if (track == nullptr)
      return 0.0;

   return track->GetEditRate();
}

EFrameRate C5TrackHandle::GetEditRateEnum() const
{
   if (!initialized)
      return IF_FRAME_RATE_INVALID;

   C5Track *track = GetTrack();
   if (track == nullptr)
      return IF_FRAME_RATE_INVALID;

   return track->GetEditRateEnum();
}

int C5TrackHandle::GetItemCount() const
{
   if (!initialized)
      return 0;

   C5Track *track = GetTrack();
   if (track == nullptr)
      return 0;

   return track->GetItemCount();
}

int C5TrackHandle::GetTotalFrameCount() const
{
   if (!initialized)
      return 0;

   C5Track *track = GetTrack();
   if (track == nullptr)
      return 0;

   return track->GetTotalFrameCount();
}

bool C5TrackHandle::GetNeedsUpdate() const
{
   if (!initialized)
      return false;

   C5Track *track = GetTrack();
   if (track == nullptr)
      return false;

   return track->GetNeedsUpdate();
}

C5Track* C5TrackHandle::GetTrack() const
{
   return (initialized) ? (*registryEntry)->GetTrack() : 0;
}

Clip5TrackID C5TrackHandle::GetTrackID() const
{
   if (initialized)
      return (*registryEntry)->GetTrackID();
   else
      return GetNullGUID();
}

string C5TrackHandle::GetTrackIDStr() const
{
   if (initialized)
      return (*registryEntry)->GetTrackIDStr();
   else
      return GetNullGUIDStr();
}

E5TrackType C5TrackHandle::GetTrackType() const
{
   // Determine count of frames that have media allocated

   if (!initialized)
      return CLIP5_TRACK_TYPE_INVALID;

   C5Track *track = GetTrack();
   if (track == nullptr)
      return CLIP5_TRACK_TYPE_INVALID;

   return track->GetTrackType();
}

// ---------------------------------------------------------------------------

C5TrackRegistry* C5TrackHandle::GetTrackRegistry()
{
   if (trackRegistry == nullptr)
      trackRegistry = new C5TrackRegistry;

   return trackRegistry;
}

// -----------------------------------------------------------------------

void C5TrackHandle::SetTrack(C5Track *track)
{
   // Get an existing track registry entry or create a new entry
   // if not in registry yet
   TrackRegistryEntryListIterator newRegistryEntry
                                = GetTrackRegistry()->GetIterator(track, true);
   if (initialized)
      {
      if (newRegistryEntry == registryEntry)
         return;  // already set
      else
         DeleteRegistryEntry();   // delete previous entry
      }

   registryEntry = newRegistryEntry;
   initialized = true;
   IncrRefCount();
}

void C5TrackHandle::SetTrackID(const Clip5TrackID &trackID)
{
   // Get an existing track registry entry or create a new entry
   // if not in registry yet
   TrackRegistryEntryListIterator newRegistryEntry
                             = GetTrackRegistry()->GetIterator(trackID, true);
   if (initialized)
      {
      if (newRegistryEntry == registryEntry)
         return;  // already set
      else
         DeleteRegistryEntry();   // delete previous entry
      }

   registryEntry = newRegistryEntry;
   initialized = true;
   IncrRefCount();
}

void C5TrackHandle::SetTrackID(const string &trackIDStr)
{
   // Set the track ID from a track ID string.  Must check to make
   // sure that the string is a valid GUID before attempting to
   // set the track ID

   Clip5TrackID newTrackID;
   if (guid_from_string(newTrackID, trackIDStr) && !IsNullGUID(newTrackID))
      {
      // Set the registryEntry and initialized member variables
      SetTrackID(newTrackID);
      }
   else
      {
      // Bogus or null GUID, so just clear existing entry, if extant
      DeleteRegistryEntry();
      }
}

// --------------------------------------------------------------------------
// Factory methods for creating an empty tracks

C5MediaTrack* C5TrackHandle::MediaTrackFactory(E5ContentType newContentType)
{
   C5MediaTrack *newTrack = 0;

   switch (newContentType)
      {
      case CLIP5_CONTENT_TYPE_AUDIO:
         newTrack = new C5AudioMediaTrack;
         break;
      case CLIP5_CONTENT_TYPE_VIDEO:
         newTrack = new C5VideoMediaTrack;
		 break;

	  default:
	  	break;
      }
      
   return newTrack;
}

C5SequenceTrack* C5TrackHandle::SequenceTrackFactory(E5ContentType newContentType)
{
   C5SequenceTrack *newTrack = 0;

   switch (newContentType)
      {
      case CLIP5_CONTENT_TYPE_AUDIO:
         newTrack = new C5AudioSequenceTrack;
         break;
      case CLIP5_CONTENT_TYPE_VIDEO:
         newTrack = new C5VideoSequenceTrack;
         break;
      case CLIP5_CONTENT_TYPE_SMPTE_TIMECODE:
        newTrack = new C5SMPTETimecodeSequenceTrack;
        break;
      case CLIP5_CONTENT_TYPE_TIMECODE:
//        newTrack = new C5TimecodeSequenceTrack;
         break;
      case CLIP5_CONTENT_TYPE_KEYCODE:
//        newTrack = new C5KeycodeSequenceTrack;
		break;

	  default:
		break;
      }

   return newTrack;
}

C5Track* C5TrackHandle::TrackFactory(E5TrackType newTrackType,
                                     E5ContentType newContentType)
{
   C5Track *newTrack = 0;
   switch (newTrackType)
      {
      case CLIP5_TRACK_TYPE_SEQUENCE:
         newTrack = SequenceTrackFactory(newContentType);
         break;
      case CLIP5_TRACK_TYPE_MEDIA:
         newTrack = MediaTrackFactory(newContentType);
         break;
      case CLIP5_TRACK_TYPE_MEDIA_STATUS:
         newTrack = new C5MediaStatusTrack;
         break;
      case CLIP5_TRACK_TYPE_AUDIO_PROXY:
         break;
      case CLIP5_TRACK_TYPE_KEYCODE:
         newTrack = new C5KeycodeTrack;
         break;
      case CLIP5_TRACK_TYPE_SMPTE_TIMECODE:
         newTrack = new C5SMPTETimecodeTrack;
         break;
#ifdef C5_TIMECODE
      case CLIP5_TRACK_TYPE_TIMECODE:
         newTrack = new C5TimecodeTrack;
         break;
#endif
      default:
         // ERROR
         newTrack = 0;
         break;
      }

   return newTrack;
}

// --------------------------------------------------------------------------
// Factory method for creating a track from XML
int C5TrackHandle::CreateTrack(C5XMLAttribList &attribList)
{
   int retVal;
   string valueString;
   string invalidDefault = "INVALID";

   valueString = attribList.GetValueString(trackTypeAttr, invalidDefault);
   E5TrackType newTrackType = clipTrackType.FindByStr(valueString,
                                                      CLIP5_TRACK_TYPE_INVALID);
   if (newTrackType == CLIP5_TRACK_TYPE_INVALID)
      return -1;   // ERROR: invalid track type

   valueString = attribList.GetValueString(contentTypeAttr, invalidDefault);
   E5ContentType newContentType = clipContentType.FindByStr(valueString,
                                                    CLIP5_CONTENT_TYPE_INVALID);
   if (newContentType == CLIP5_CONTENT_TYPE_INVALID)
      return -1;   // ERROR: invalid track type

   C5Track *newTrack = TrackFactory(newTrackType, newContentType);
   
   if (newTrack != 0)
      {
      retVal = newTrack->initFromXMLAttributes(attribList);
      if (retVal != 0)
         {
         delete newTrack;
         return retVal;  // ERROR: could not assign attributes
         }

      SetTrack(newTrack);
      }
   else
      {
      // ERROR: could not create a new track, perhaps the track type
      //        was unknown.
      DeleteRegistryEntry();  // clean up existing entry
      return -1;
      }

   return 0;

} // C5TrackHandle::CreateTrack

// -----------------------------------------------------------------------

int C5TrackHandle::ReadTrackFile(const string &clipPath)
{                   
   int retVal;

   if (!initialized)
      return -1;
   
   C5TrackRegistry *registry = GetTrackRegistry();

   retVal = registry->ReadTrackFiles(clipPath);
   if (retVal != 0)
      return retVal;

   return 0;
}

int C5TrackHandle::ReloadTrackFile(const string &clipPath)
{                   
   int retVal;

   if (!initialized)
      return -1;
   
   retVal = (*registryEntry)->ReloadTrackFile(clipPath);
   if (retVal != 0)
      return retVal;

   return 0;
}

// -----------------------------------------------------------------------

int C5TrackHandle::WriteTrackFile(const string &clipPath)
{
   int retVal;

   if (!initialized)
      return -1;  // Handle is not initialized, cannot write

   C5Track *track = GetTrack();

   if (track == nullptr)
      return -1;

   retVal = track->WriteTrackFile(clipPath);
   if (retVal != 0)
      return retVal;

   return 0;
}

// -----------------------------------------------------------------------

void C5TrackHandle::Dump(MTIostringstream &ostrm)
{
/*
   ostrm << "Track Handle" << endl;

   if (!initialized)
      {
      ostrm << " Track Handle not initialized" << endl;
      return;
      }

   ostrm << " Track GUID: " <<  GetTrackIDStr() << endl;
   C5Track *track = GetTrack();

	ostrm << " Track Ptr: " << track << endl;

   if (track == nullptr)
      return;

   ostrm << " Track Type: " << clipTrackType.FindByValue(track->GetTrackType())
         << " Content Type: " << clipContentType.FindByValue(track->GetContentType())
			<< endl;
*/
}

// ===========================================================================

C5MediaTrackHandle::C5MediaTrackHandle()
{
}

C5MediaTrackHandle::C5MediaTrackHandle(const C5TrackHandle &rhs)
 : C5TrackHandle(rhs)
{
}

C5MediaTrackHandle::C5MediaTrackHandle(C5Track *track)
 : C5TrackHandle(track)
{
//   SetTrack(track);
}

C5MediaTrackHandle::C5MediaTrackHandle(const Clip5TrackID &trackID)
 : C5TrackHandle(trackID)
{
//   SetTrackID(trackID);
}

C5MediaTrackHandle::~C5MediaTrackHandle()
{
}

// --------------------------------------------------------------------------

int C5MediaTrackHandle::ReadMedia(int index, MTI_UINT8 *buffer[])
{
   int retVal;

//   C5MediaTrack *mediaTrack = GetMediaTrack();
   C5MediaContentAPI *mediaTrack = GetMediaContentAPI();
   if (mediaTrack == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   retVal = mediaTrack->ReadMedia(index, buffer);
   if (retVal != 0)
      return retVal;

   return 0;

}

int C5MediaTrackHandle::ReadMediaMT(int index, MTI_UINT8 *buffer[],
                                    int fileHandleIndex)
{
   int retVal;

//   C5MediaTrack *mediaTrack = GetMediaTrack();
   C5MediaContentAPI *mediaTrack = GetMediaContentAPI();
   if (mediaTrack == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   retVal = mediaTrack->ReadMediaMT(index, buffer, fileHandleIndex);
   if (retVal != 0)
      return retVal;

   return 0;

}

int C5MediaTrackHandle::ReadMediaOneField(int frameIndex, int fieldIndex,
                                          MTI_UINT8 *buffer)
{
   return ReadMediaOneFieldMT(frameIndex, fieldIndex, buffer, 0);
}

int C5MediaTrackHandle::ReadMediaOneFieldMT(int frameIndex, int fieldIndex,
                                            MTI_UINT8 *buffer,
                                            int fileHandleIndex)
{
   int retVal;

   C5MediaContentAPI *mediaTrack = GetMediaContentAPI();
   if (mediaTrack == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   retVal = mediaTrack->ReadMediaOneFieldMT(frameIndex, fieldIndex, buffer,
                                            fileHandleIndex);
   if (retVal != 0)
      return retVal;

   return 0;
}

// -----------------------------------------------------------------------

int C5MediaTrackHandle::ReadField(int index, MTI_UINT8 *buffer)
{
   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

int C5MediaTrackHandle::WriteField(int index, MTI_UINT8 *buffer)
{
   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

// -----------------------------------------------------------------------

int C5MediaTrackHandle::WriteMedia(int index, MTI_UINT8 *buffer[])
{
   int retVal;

//   C5MediaTrack *mediaTrack = GetMediaTrack();
   C5MediaContentAPI *mediaTrack = GetMediaContentAPI();
   if (mediaTrack == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   retVal = mediaTrack->WriteMedia(index, buffer);
   if (retVal != 0)
      return retVal;

   return 0;
}

// -----------------------------------------------------------------------

/*
C5MediaTrack* C5MediaTrackHandle::GetMediaTrack() const
{
   if (!IsInitialized())
      return 0;

   C5Track *track = GetTrack();
   if (track == nullptr)
      return 0;

   if (track->GetContentType() != CLIP5_CONTENT_TYPE_AUDIO &&
       track->GetContentType() != CLIP5_CONTENT_TYPE_VIDEO)
      return 0;  // TBD: could be an exception instead

   return static_cast<C5MediaTrack*>(track);
}
*/

C5MediaContentAPI* C5MediaTrackHandle::GetMediaContentAPI() const
{
   if (!IsInitialized())
      return 0;

   C5Track *track = GetTrack();
   if (track == nullptr)
      return 0;

   if (track->GetContentType() != CLIP5_CONTENT_TYPE_AUDIO &&
       track->GetContentType() != CLIP5_CONTENT_TYPE_VIDEO)
      return 0;  // TBD: could be an exception instead

   // COMPLETE HACK TO KEEP MAKING PROGRESS
   if (track->GetTrackType() == CLIP5_TRACK_TYPE_SEQUENCE)
      {
      return static_cast<C5MediaSequenceTrack *>(track);
      }
   else if (track->GetTrackType() == CLIP5_TRACK_TYPE_MEDIA)
      {
      return static_cast<C5MediaTrack*>(track);
      }
   else
      return 0;
}

// -----------------------------------------------------------------------

int C5MediaTrackHandle::GetFirstMediaAccess(CMediaAccess* &mediaAccess) const
{
   int retVal;

   C5MediaContentAPI *mediaTrack = GetMediaContentAPI();
   if (mediaTrack == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   retVal = mediaTrack->GetFirstMediaAccess(mediaAccess);
   if (retVal != 0)
      return retVal;

   return 0;
}

int C5MediaTrackHandle::GetFirstMediaIdentifier(string &mediaIdentifier) const
{
   int retVal;

   C5MediaContentAPI *mediaTrack = GetMediaContentAPI();
   if (mediaTrack == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   retVal = mediaTrack->GetFirstMediaIdentifier(mediaIdentifier);
   if (retVal != 0)
      return retVal;

   return 0;
}

int C5MediaTrackHandle::GetFirstMediaType(EMediaType &mediaType) const
{
   int retVal;

   C5MediaContentAPI *mediaTrack = GetMediaContentAPI();
   if (mediaTrack == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   retVal = mediaTrack->GetFirstMediaType(mediaType);
   if (retVal != 0)
      return retVal;

   return 0;
}

MTI_UINT32 C5MediaTrackHandle::getMaxPaddedFieldByteCount(int index, int count)
{
   int retVal;

//   C5MediaTrack *mediaTrack = GetMediaTrack();
   C5MediaContentAPI *mediaTrack = GetMediaContentAPI();
   if (mediaTrack == nullptr)
      return 0;

   MTI_UINT32 maxByteCount = mediaTrack->getMaxPaddedFieldByteCount(index,
                                                                    count);
   return maxByteCount;
}

const CMediaFormat* C5MediaTrackHandle::GetMediaFormat() const
{
//   C5MediaTrack *mediaTrack = GetMediaTrack();
   C5MediaContentAPI *mediaTrack = GetMediaContentAPI();
   if (mediaTrack == nullptr)
      return 0;

   return mediaTrack->GetMediaFormat();
}

int C5MediaTrackHandle::GetMediaLocationsAllFields(int frameIndex,
                                      vector<CMediaLocation> &mediaLocationList)
{
   C5MediaContentAPI *mediaTrack = GetMediaContentAPI();
   if (mediaTrack == nullptr)
      return CLIP5_MEDIA_STATUS_NO_MEDIA;

   return mediaTrack->GetMediaLocationsAllFields(frameIndex,
                                                 mediaLocationList);
}

E5MediaStatus C5MediaTrackHandle::GetMediaStatus(int index)
{
//   C5MediaTrack *mediaTrack = GetMediaTrack();
   C5MediaContentAPI *mediaTrack = GetMediaContentAPI();
   if (mediaTrack == nullptr)
      return CLIP5_MEDIA_STATUS_NO_MEDIA;

   return mediaTrack->GetMediaStatus(index);
}

// ---------------------------------------------------------------------------

int C5MediaTrackHandle::PeekAtMediaLocation(int frameIndex, int fieldIndex,
                                            string &filename, MTI_INT64 &offset)
{
   int retVal;

   C5MediaContentAPI *mediaTrack = GetMediaContentAPI();
   if (mediaTrack == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   retVal = mediaTrack->PeekAtMediaLocation(frameIndex, fieldIndex,
                                            filename, offset);
   if (retVal != 0)
      return retVal;

   return 0;
}

int C5MediaTrackHandle::PeekAtMediaAllocation(vector<SMediaAllocation> &mediaAllocList,
                                              volatile int* currentFrame,
                                              volatile bool* stopRequest)
{
   int retVal;

   C5MediaContentAPI *mediaTrack = GetMediaContentAPI();
   if (mediaTrack == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   retVal = mediaTrack->PeekAtMediaAllocation(mediaAllocList, currentFrame,
                                              stopRequest);
   if (retVal != 0)
      return retVal;

   return 0;
}

// ---------------------------------------------------------------------------

int C5MediaTrackHandle::LSF1(int &count)
{
   int retVal;

   count = 0;

   // Determine count of frames that have media allocated
   retVal = LSF2(0, GetItemCount(), count);
   if (retVal != 0)
      return retVal;

   const CMediaFormat *mediaFormat = GetMediaFormat();
   if (mediaFormat == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   int fieldCount = mediaFormat->getFieldCount();
   if (fieldCount == 0)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   // Convert field count to frame count
   count /= fieldCount;

   return 0;
}

int C5MediaTrackHandle::LSF2(int startIndex, int itemCount, int &allocCount)
{
   int retVal;

//   C5MediaTrack *mediaTrack = GetMediaTrack();
   C5MediaContentAPI *mediaTrack = GetMediaContentAPI();
   if (mediaTrack == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   retVal = mediaTrack->LSF2(startIndex, itemCount, allocCount);
   if (retVal != 0)
      return retVal;

   return 0;
}

// -----------------------------------------------------------------------
// Factory method to create a conventional video track with
// media storage

int C5MediaTrackHandle::CreateSequenceTrack(const string &clipPath,
                                            EMediaType mediaType,
                                            const string& mediaIdentifier,
                                            const CMediaFormat& mediaFormat,
                                            EFrameRate newEditRate,
                                            int totalItemCount, int allocItemCount)
{
   int retVal;

   // Convert video track's allocItemCount to media track's item count
   int mediaItemCount = allocItemCount;
   int totalMediaItemCount = totalItemCount;
   if (mediaFormat.getInterlaced())
      {
      mediaItemCount *= 2;
      totalMediaItemCount *= 2;
      }

   // Create new media track  (if anything allocated)

   E5ContentType newContentType;
   if (mediaFormat.getMediaFormatType() == MEDIA_FORMAT_TYPE_VIDEO3)
      newContentType = CLIP5_CONTENT_TYPE_VIDEO;
   else if (mediaFormat.getMediaFormatType() == MEDIA_FORMAT_TYPE_AUDIO3)
      newContentType = CLIP5_CONTENT_TYPE_AUDIO;
   else
      return CLIP_ERROR_INVALID_MEDIA_FORMAT_TYPE;

   C5MediaTrackHandle mediaTrackHandle;
   if (allocItemCount > 0)
      {
      retVal = mediaTrackHandle.CreateMediaTrack(clipPath,
                                                 mediaType, mediaIdentifier,
                                                 mediaFormat, newEditRate,
                                                 totalMediaItemCount,
                                                 mediaItemCount);
      if (retVal != 0)
         return retVal;  // ERROR: Failed to create the media track.
      }

   // Create new sequence track
   C5SequenceTrack *sequenceTrack = SequenceTrackFactory(newContentType);
   C5MediaTrackHandle sequenceTrackHandle(sequenceTrack);

   // Create new pulldown segment (if anything allocated) that accomodates
   // relationship between sequence track's frames and media storage
   C5Pulldown *pulldownSegment = 0;
   if (allocItemCount > 0)
      {
      pulldownSegment = PulldownFactory(newContentType);
      pulldownSegment->SetItemCount(allocItemCount);
      pulldownSegment->SetDstStartIndex(0);
      pulldownSegment->SetSrcStartIndex(0);
      pulldownSegment->SetSrcTrack(mediaTrackHandle);
      pulldownSegment->SetPulldownPhase(0);
      EPulldownType pulldownType;
      pulldownType = (mediaFormat.getInterlaced()) ? PULLDOWN_TYPE_2_1
                                                   : PULLDOWN_TYPE_1_1;
      pulldownSegment->SetPulldownType(pulldownType);

      // Add new pulldown segment to sequence track
      sequenceTrack->AppendRange(pulldownSegment);
      }

   if (totalItemCount > allocItemCount)
      {
      // Create new filler range (if anything not allocated)
      C5FillerRange *fillerRange = new C5FillerRange;
      fillerRange->SetItemCount(totalItemCount - allocItemCount);
      fillerRange->SetDstStartIndex(allocItemCount);

      // Add new filler range to sequence track
      sequenceTrack->AppendRange(fillerRange);
      }

   // Write sequence track file
   retVal = sequenceTrack->WriteTrackFile(clipPath);
   if (retVal != 0)
      {
      // Write failed
      // TBD: clean up
      // TBD:  Deallocate media storage
      return retVal;
      }

   *this = sequenceTrackHandle;
   return 0;

} // C5MediaTrackHandle::CreateSequenceTrack

// ---------------------------------------------------------------------------
// Factory method for Media Track
int C5MediaTrackHandle::CreateMediaTrack(const string &clipPath,
                                         EMediaType mediaType,
                                         const string& mediaIdentifier,
                                         const CMediaFormat& mediaFormat,
                                         EFrameRate newEditRate,
                                         int totalItemCount,
                                         int allocItemCount)
{
   int retVal;

   if (totalItemCount <= 0)
      return -1;  // need better error code
   if (allocItemCount > totalItemCount)
      return -5;  // need better error code

   // TBD: Handle memory allocation exceptions for new operators

   // Create Media Status Track
   C5MediaStatusTrack *mediaStatusTrack = new C5MediaStatusTrack;
   mediaStatusTrack->SetEditRate(newEditRate);
   C5TrackHandle mediaStatusTrackHandle(mediaStatusTrack);

   // Add one Media status range with "unallocated" status and length of
   // caller's itemCount
   C5MediaStatusRange *mediaStatusRange = new C5MediaStatusRange;
   mediaStatusRange->SetItemCount(totalItemCount);
   mediaStatusRange->SetDstStartIndex(0);
   mediaStatusRange->SetMediaStatus(CLIP5_MEDIA_STATUS_UNALLOCATED);

   mediaStatusTrack->AppendRange(mediaStatusRange);

   // Create Media Location track and allocate storage
   C5MediaTrackHandle mediaLocationTrackHandle;
   retVal = mediaLocationTrackHandle.AllocateMediaStorage(mediaType,
                                                          mediaIdentifier,
                                                          mediaFormat,
                                                          allocItemCount);
   if (retVal != 0)
      {
      // ERROR: could not allocate media location track or

      // TBD: Delete the tracks already created so we do not have memory leaks
      //delete mediaStatusTrack;
      //delete mediaTrack;
      return retVal;
      }

   C5Track *track = mediaLocationTrackHandle.GetTrack();
   track->SetEditRate(newEditRate);
   C5MediaTrack *mediaLocationTrack = static_cast<C5MediaTrack*>(track);

   // Add filler range at end if not all frames allocated
   if (totalItemCount > allocItemCount)
      {
      // Create new filler range (if anything not allocated)
      C5FillerRange *fillerRange = new C5FillerRange;
      fillerRange->SetItemCount(totalItemCount - allocItemCount);
      fillerRange->SetDstStartIndex(allocItemCount);

      // Add new filler range to sequence track
      mediaLocationTrack->AppendRange(fillerRange);
      }

   mediaLocationTrack->SetMediaStatusTrack(mediaStatusTrackHandle);

   // Media storage allocation succeeded, so we now set the media status
   // track to "allocated but unwritten"
   retVal = mediaLocationTrack->SetMediaStatus(0, allocItemCount,
                                               CLIP5_MEDIA_STATUS_ALLOCATED,
                                               true, true);
   if (retVal != 0)
      return retVal;

   retVal = mediaStatusTrackHandle.WriteTrackFile(clipPath);
   if (retVal != 0)
      {
      // TBD: Cleanup
      // TBD: Error code
      return 0;
      }

   retVal = mediaLocationTrackHandle.WriteTrackFile(clipPath);
   if (retVal != 0)
      {
      // TBD: Cleanup
      // TBD: Error code
      return 0;
      }

   *this = mediaLocationTrackHandle;
   
   return 0;

} // C5MediaTrackHandle::CreateMediaTrack

// ---------------------------------------------------------------------------

int C5MediaTrackHandle::ClearMediaWritten(int startIndex, int itemCount)
{
   if (!IsInitialized())
      return CLIP_ERROR_NO_MEDIA;

//   C5MediaTrack *mediaTrack = GetMediaTrack();
   C5MediaContentAPI *mediaTrack = GetMediaContentAPI();
   if (mediaTrack == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   return mediaTrack->ClearMediaWritten(startIndex, itemCount);
}
   
// -----------------------------------------------------------------------

int C5MediaTrackHandle::UpdateMediaStatusTrackFile(const string &clipPath)
{
   int retVal;

   if (!IsInitialized())
      return CLIP_ERROR_NO_MEDIA;  // Handle is not initialized, cannot update

//   C5MediaTrack *mediaTrack = GetMediaTrack();
   C5MediaContentAPI *mediaTrack = GetMediaContentAPI();
   if (mediaTrack == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   retVal = mediaTrack->UpdateMediaStatusTrackFile(clipPath);
   if (retVal != 0)
      return retVal;

   return 0;
}

int C5MediaTrackHandle::ReloadMediaStatusTrackFile(const string &clipPath)
{
   int retVal;

   if (!IsInitialized())
      return CLIP_ERROR_NO_MEDIA;  // Handle is not initialized, cannot update

//   C5MediaTrack *mediaTrack = GetMediaTrack();
   C5MediaContentAPI *mediaTrack = GetMediaContentAPI();
   if (mediaTrack == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   retVal = mediaTrack->ReloadMediaStatusTrackFile(clipPath);
   if (retVal != 0)
      return retVal;

   return 0;
}

//----------------------------------------------------------------------------
// Factory Method for creating a C5MediaTrack and allocating
// media storage

int C5MediaTrackHandle::AllocateMediaStorage(EMediaType mediaType,
                                        const string& mediaIdentifier,
                                        const CMediaFormat& mediaFormat,
                                        MTI_UINT32 itemCount)
{
   int retVal;
   CMediaInterface mediaInterface;
   CMediaAllocator *mediaAllocator = 0;
   CTimecode frame0Timecode(0);     // Dummy
                                    // TBD: need to make sure this does nothing
                                    //      or perhaps eliminate it

   E5ContentType newContentType;
   if (mediaFormat.getMediaFormatType() == MEDIA_FORMAT_TYPE_VIDEO3)
      newContentType = CLIP5_CONTENT_TYPE_VIDEO;
   else if (mediaFormat.getMediaFormatType() == MEDIA_FORMAT_TYPE_AUDIO3)
      newContentType = CLIP5_CONTENT_TYPE_AUDIO;
   else
      return CLIP_ERROR_INVALID_MEDIA_FORMAT_TYPE;

   // Adjust the itemCount in the case of interlaced.  CMediaInterface::allocateSpace
   // argument is always frames, so it gets doubled for fields
   if (mediaFormat.getInterlaced())
      itemCount /= 2;

   // Allocate media storage space
   mediaAllocator = mediaInterface.allocateSpace(mediaType,
                                                 mediaIdentifier,
                                                 "hist_dir_AllocateMediaStorage",
                                                 mediaFormat,
                                                 frame0Timecode,
                                                 itemCount, "");
   if (mediaAllocator == nullptr)
      return CLIP_ERROR_MEDIA_ALLOCATION_FAILED;   // allocation failed.

   // Create C5MediaTrack
   C5MediaTrack *mediaLocationTrack = MediaTrackFactory(newContentType);
   mediaLocationTrack->CopyMediaFormat(mediaFormat);
   mediaLocationTrack->SetDefaultMediaType(mediaType);
   mediaLocationTrack->SetDefaultMediaIdentifier(mediaIdentifier);
   C5MediaTrackHandle newMediaLocationTrackHandle(mediaLocationTrack);

   // Create entry for each block
   retVal = mediaAllocator->AssignMediaStorage(*mediaLocationTrack);
   if (retVal != 0)
      return retVal;   // Assignment failed

   delete mediaAllocator;

   *this = newMediaLocationTrackHandle;
   return 0;

} // C5MediaTrackHandle::AllocateMediaStorage

// --------------------------------------------------------------------------
// Extend an existing track with a combination of allocated media
// and filler ranges.  This track must already

int C5MediaTrackHandle::ExtendMediaTrack(const string &clipPath,
                                         int totalItemExtensionCount,
                                         int allocItemExtensionCount)
{
   int retVal;

   // Get sequence track
   C5Track *track = GetTrack();
   if (track == nullptr)
      return -1;   // No track, can't extend

   // Find out the media format, media type and media identifier already
   // used by this sequence track.  If there is no media track, then we
   // are up the creek without a paddle.
   const CMediaFormat *mediaFormat = GetMediaFormat();
   if (mediaFormat == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   if (track->GetTrackType() == CLIP5_TRACK_TYPE_MEDIA)
      {
      C5MediaTrack *mediaTrack = static_cast<C5MediaTrack*>(track);
      retVal = mediaTrack->ExtendMediaTrack(clipPath,
                                            totalItemExtensionCount * mediaFormat->getFieldCount(),
                                            allocItemExtensionCount * mediaFormat->getFieldCount());
      if (retVal != 0)
         return retVal;

      retVal = WriteTrackFile(clipPath);
      if (retVal != 0)
         return retVal;

      return 0;  // all done!
      }
   else if (track->GetTrackType() != CLIP5_TRACK_TYPE_SEQUENCE)
      return -1;   // Not a sequence track, can't extend

   C5SequenceTrack *sequenceTrack = static_cast<C5SequenceTrack*>(track);

   // If the last segment of the track is a filler segment, get its
   // length and then delete it.  (This assumes that there is at most only
   // one filler and it is at the at the end of the track)
   int existingTotalItemCount = GetItemCount();
   C5RangeHandle lastRangeHandle = sequenceTrack->FindRangeHandle(existingTotalItemCount-1);
   if (!sequenceTrack->IsEnd(lastRangeHandle))
      {
      C5Range *lastRange = lastRangeHandle.GetRange();
      if (lastRange == nullptr)
         return -1;
      if (lastRange->GetRangeType() == CLIP5_RANGE_TYPE_FILLER)
         {
         sequenceTrack->RemoveRange(lastRangeHandle);
         }
      }

   // If filler range was removed or not, assume that the
   // remaining items are non-filler

   existingTotalItemCount = GetItemCount();

   int tmpAllocItemExtensionCount = allocItemExtensionCount;

   // Working backwards, find all of the Media Tracks that have
   // no media allocated
   C5RangeHandle rangeHandle;
   C5RangeHandle firstRangeHandle = sequenceTrack->GetEndRangeHandle();
   for (rangeHandle = sequenceTrack->FindRangeHandle(existingTotalItemCount-1);
        tmpAllocItemExtensionCount > 0 && !sequenceTrack->IsEnd(rangeHandle);
        --rangeHandle)
      {
      C5Range *range = rangeHandle.GetRange();
      if (range == nullptr)
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN; // shouldn't happen

      int rangeItemCount = range->GetItemCount();
      int allocCount;
      retVal = LSF2(range->GetDstStartIndex(), rangeItemCount, allocCount);
      if (retVal != 0)
         return retVal;

      if (allocCount == 0)
         {
         // This range has no media storage allocated for it
         firstRangeHandle = rangeHandle;
         tmpAllocItemExtensionCount -= rangeItemCount;
         continue;
         }
      else if (allocCount > 0 && allocCount == rangeItemCount)
         {
         // This range is completely allocated
         break;
         }
      else if (allocCount > 0 && allocCount < rangeItemCount)
         {
         // This range has some media storage allocated for it, but
         // not completely allocated
         firstRangeHandle = rangeHandle;
         break;
         }
      }

   // Iterate over ranges, reallocating media storage
   for (rangeHandle = firstRangeHandle;
        allocItemExtensionCount > 0 && !sequenceTrack->IsEnd(rangeHandle);
        ++rangeHandle)
      {
      C5Range *range = rangeHandle.GetRange();
      if (range == nullptr)
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN; // shouldn't happen

      int rangeItemCount = range->GetItemCount();
      int allocCount;
      retVal = LSF2(range->GetDstStartIndex(), rangeItemCount, allocCount);
      if (retVal != 0)
         return retVal;

      int reallocCount = rangeItemCount - allocCount;
      if (reallocCount <= 0)
         break;

      if (reallocCount > allocItemExtensionCount)
         reallocCount = allocItemExtensionCount;

      // Reallocate
      if (range->GetRangeType() != CLIP5_RANGE_TYPE_PULLDOWN)
         return -1;
      C5Pulldown *pulldown = static_cast<C5Pulldown*>(range);
      C5TrackHandle originalMediaTrackHandle = pulldown->GetSrcTrack();
      C5Track *track = originalMediaTrackHandle.GetTrack();
      if (track->GetTrackType() != CLIP5_TRACK_TYPE_MEDIA)
         return -3;
      C5MediaTrack *mediaTrack = static_cast<C5MediaTrack*>(track);
      MTI_UINT32 reallocMediaItemCount = reallocCount;
      if (mediaFormat->getInterlaced())
         reallocMediaItemCount *= 2;
      retVal = mediaTrack->ReallocateMediaStorage(reallocMediaItemCount);
      if (retVal != 0)
         return 0;

      // Reduce the allocItemExtensionCount by the number of items
      // reallocated in existing source track
      allocItemExtensionCount -= reallocCount;

      // Write back original media track
      retVal = mediaTrack->UpdateMediaStatusTrackFile(clipPath);
      if (retVal != 0)
         return retVal;

      retVal = originalMediaTrackHandle.WriteTrackFile(clipPath);
      if (retVal != 0)
         return retVal;
      }

   // Convert sequence track's allocItemCount to media track's item count
   MTI_UINT32 mediaItemCount = allocItemExtensionCount;
   if (mediaFormat->getInterlaced())
      mediaItemCount *= 2;

   EMediaType mediaType;
   retVal = GetFirstMediaType(mediaType);
   if (retVal != 0)
      return retVal;
   string mediaIdentifier;
   retVal = GetFirstMediaIdentifier(mediaIdentifier);
   if (retVal != 0)
      return retVal;

   // Create new media track  (if anything media is to be allocated)
   C5MediaTrackHandle newMediaTrackHandle;
   if (allocItemExtensionCount > 0)
      {
      retVal = newMediaTrackHandle.CreateMediaTrack(clipPath,
                                                 mediaType, mediaIdentifier,
                                                 *mediaFormat,
                                                 IF_FRAME_RATE_INVALID,   // TTT Dummy argument
                                                 mediaItemCount,
                                                 mediaItemCount);
      if (retVal != 0)
         return retVal;  // ERROR: Failed to create the media track.

      // Create new pulldown segment (if anything allocated) that accomodates
      // relationship between sequence track's frames and media storage
      C5Pulldown *pulldownSegment = 0;
      pulldownSegment = PulldownFactory(sequenceTrack->GetContentType());
      pulldownSegment->SetItemCount(allocItemExtensionCount);
      pulldownSegment->SetDstStartIndex(existingTotalItemCount);
      pulldownSegment->SetSrcStartIndex(0);
      pulldownSegment->SetSrcTrack(newMediaTrackHandle);
      pulldownSegment->SetPulldownPhase(0);
      EPulldownType pulldownType;
      pulldownType = (mediaFormat->getInterlaced()) ? PULLDOWN_TYPE_2_1
                                                    : PULLDOWN_TYPE_1_1;
      pulldownSegment->SetPulldownType(pulldownType);

      // Add new pulldown segment to sequence track
      sequenceTrack->AppendRange(pulldownSegment);
      }

   int newAllocItemCount = GetItemCount();
   int newTotalItemCount = existingTotalItemCount + totalItemExtensionCount;

   if (newTotalItemCount > newAllocItemCount)
      {
      // Create new filler range (if anything not allocated)
      C5FillerRange *fillerRange = new C5FillerRange;
      fillerRange->SetItemCount(newTotalItemCount - newAllocItemCount);
      fillerRange->SetDstStartIndex(newAllocItemCount);

      // Add new filler range to sequence track
      sequenceTrack->AppendRange(fillerRange);
      }

   // Write sequence track file
   retVal = sequenceTrack->WriteTrackFile(clipPath);
   if (retVal != 0)
      {
      // Write failed
      // TBD: clean up
      // TBD:  Deallocate media storage
      return retVal;
      }

   return 0;

} // C5MediaTrackHandle::ExtendMediaTrack

// -----------------------------------------------------------------------

int C5MediaTrackHandle::FreeMedia(const string &clipPath, int startIndex,
                             int itemCount)
{
   int retVal;
   
   C5MediaContentAPI *mediaContentAPI = GetMediaContentAPI();

   if (mediaContentAPI == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   retVal = mediaContentAPI->FreeMedia(clipPath, startIndex, itemCount);
   if (retVal != 0)
      return retVal;

   return 0;

} // FreeMedia

// ===========================================================================

C5AudioTrackHandle::C5AudioTrackHandle()
{
}


C5AudioTrackHandle::C5AudioTrackHandle(const C5TrackHandle &rhs)
 : C5MediaTrackHandle(rhs)
{
}

C5AudioTrackHandle::C5AudioTrackHandle(const Clip5TrackID &trackID)
 : C5MediaTrackHandle(trackID)
{
}

C5AudioTrackHandle::C5AudioTrackHandle(C5Track *track)
 : C5MediaTrackHandle(track)
{
}

C5AudioTrackHandle::~C5AudioTrackHandle()
{
}

// ---------------------------------------------------------------------------

const CAudioFormat* C5AudioTrackHandle::GetAudioFormat() const
{
   return static_cast<const CAudioFormat*>(GetMediaFormat());
}

// ---------------------------------------------------------------------------

C5AudioContentAPI* C5AudioTrackHandle::GetAudioContentAPI() const
{
   if (!IsInitialized())
      return 0;

   C5Track *track = GetTrack();
   if (track == nullptr)
      return 0;

   if (track->GetContentType() != CLIP5_CONTENT_TYPE_AUDIO)
      return 0;  // TBD: could be an exception instead

   // COMPLETE HACK TO KEEP MAKING PROGRESS
   if (track->GetTrackType() == CLIP5_TRACK_TYPE_SEQUENCE)
      {
      return static_cast<C5AudioSequenceTrack *>(track);
      }
   else if (track->GetTrackType() == CLIP5_TRACK_TYPE_MEDIA)
      {
      return static_cast<C5AudioMediaTrack*>(track);
      }
   else
      return 0;
}

// ---------------------------------------------------------------------------

int C5AudioTrackHandle::GetSampleCountAtField(int frameIndex, int fieldIndex)
{
   int retVal;

   C5AudioContentAPI *audioContentAPI = GetAudioContentAPI();
   if (audioContentAPI == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   return audioContentAPI->GetSampleCountAtField(frameIndex, fieldIndex);
}

int C5AudioTrackHandle::GetSampleCountAtFrame(int frameIndex)
{
   int retVal;

   C5AudioContentAPI *audioContentAPI = GetAudioContentAPI();
   if (audioContentAPI == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   return audioContentAPI->GetSampleCountAtFrame(frameIndex);
}

// ---------------------------------------------------------------------------

int C5AudioTrackHandle::ReadMediaBySample(int index, int sampleOffset,
                                          MTI_INT64 sampleCount,
                                          MTI_UINT8 *buffer)
{
   int retVal;

   C5AudioContentAPI *audioContentAPI = GetAudioContentAPI();
   if (audioContentAPI == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   retVal = audioContentAPI->ReadMediaBySample(index, sampleOffset,
                                               sampleCount, buffer);
   if (retVal != 0)
      return retVal;

   return 0;
}

int C5AudioTrackHandle::ReadMediaBySampleMT(int index, int sampleOffset,
                                            MTI_INT64 sampleCount,
                                            MTI_UINT8 *buffer,
                                            int fileHandleIndex)
{
   int retVal;

   C5AudioContentAPI *audioContentAPI = GetAudioContentAPI();
   if (audioContentAPI == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   retVal = audioContentAPI->ReadMediaBySampleMT(index, sampleOffset,
                                                 sampleCount, buffer,
                                                 fileHandleIndex);
   if (retVal != 0)
      return retVal;

   return 0;
}

int C5AudioTrackHandle::WriteMediaBySample(int index, int sampleOffset,
                                          MTI_INT64 sampleCount,
                                          MTI_UINT8 *buffer,
                                          CTimeTrack *audioProxyTrack)
{
   int retVal;

   C5AudioContentAPI *audioContentAPI = GetAudioContentAPI();
   if (audioContentAPI == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   retVal = audioContentAPI->WriteMediaBySample(index, sampleOffset,
                                                sampleCount, buffer,
                                                audioProxyTrack);
   if (retVal != 0)
      return retVal;

   return 0;
}

// ===========================================================================

C5VideoTrackHandle::C5VideoTrackHandle()
{
}

C5VideoTrackHandle::C5VideoTrackHandle(const C5TrackHandle &rhs)
 : C5MediaTrackHandle(rhs)
{
}

C5VideoTrackHandle::C5VideoTrackHandle(const Clip5TrackID &trackID)
 : C5MediaTrackHandle(trackID)
{
}

C5VideoTrackHandle::C5VideoTrackHandle(C5Track *track)
 : C5MediaTrackHandle(track)
{
}

C5VideoTrackHandle::~C5VideoTrackHandle()
{
}

// --------------------------------------------------------------------------

const CImageFormat* C5VideoTrackHandle::GetImageFormat() const
{
   return static_cast<const CImageFormat*>(GetMediaFormat());
}

// ===========================================================================
// ===========================================================================
// SMPTE Timecode Track Handle Class

C5SMPTETimecodeTrackHandle::C5SMPTETimecodeTrackHandle()
{
}

C5SMPTETimecodeTrackHandle::C5SMPTETimecodeTrackHandle(const C5TrackHandle &rhs)
 : C5TrackHandle(rhs)
{
}

C5SMPTETimecodeTrackHandle::C5SMPTETimecodeTrackHandle(const Clip5TrackID &trackID)
 : C5TrackHandle(trackID)
{
}

C5SMPTETimecodeTrackHandle::C5SMPTETimecodeTrackHandle(C5Track *track)
 : C5TrackHandle(track)
{
}

C5SMPTETimecodeTrackHandle::~C5SMPTETimecodeTrackHandle()
{
}

// ----------------------------------------------------------------------------

C5SMPTETimecodeContentAPI *C5SMPTETimecodeTrackHandle::GetSMPTETimecodeContentAPI() const
{
   if (!IsInitialized())
      return 0;

   C5Track *track = GetTrack();
   if (track == nullptr)
      return 0;

   if (track->GetContentType() != CLIP5_CONTENT_TYPE_SMPTE_TIMECODE)
      {
      // NOTE: this error should never happen unless clip data structures
      //       are corrupted, so probably should be an exception

      TRACE_0(errout << "ERROR: C5SMPTETimecodeTrackHandle::GetSMPTETimecodeTrack:" << endl
                     << "       wrong content type in handle " << GetTrackType());
      return 0;
      }

   // COMPLETE HACK TO KEEP MAKING PROGRESS
   if (track->GetTrackType() == CLIP5_TRACK_TYPE_SEQUENCE)
      {
      return static_cast<C5SMPTETimecodeSequenceTrack *>(track);
      }
   else if (track->GetTrackType() == CLIP5_TRACK_TYPE_SMPTE_TIMECODE)
      {
      return static_cast<C5SMPTETimecodeTrack*>(track);
      }
   else
      return 0;
}

int C5SMPTETimecodeTrackHandle::GetTimecode(int index, CSMPTETimecode &timecode,
                                            float &partialFrame)
{
   int retVal;

   C5SMPTETimecodeContentAPI *track = GetSMPTETimecodeContentAPI();
   if (track == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   retVal = track->GetTimecode(index, timecode, partialFrame);
   if (retVal != 0)
      return retVal;

   return 0;

}

int C5SMPTETimecodeTrackHandle::GetTimecodeFrameRate() const
{
   C5SMPTETimecodeContentAPI *track = GetSMPTETimecodeContentAPI();
   if (track == nullptr)
      return 0;

   return track->GetTimecodeFrameRate();
}

EFrameRate C5SMPTETimecodeTrackHandle::GetTimecodeFrameRateEnum() const
{
   C5SMPTETimecodeContentAPI *track = GetSMPTETimecodeContentAPI();
   if (track == nullptr)
      return IF_FRAME_RATE_INVALID;

   return track->GetTimecodeFrameRateEnum();
}

EFrameRate C5SMPTETimecodeTrackHandle::GetTimecodeFrameRateEnum(int index)
{
   C5SMPTETimecodeContentAPI *track = GetSMPTETimecodeContentAPI();
   if (track == nullptr)
      return IF_FRAME_RATE_INVALID;

   return track->GetTimecodeFrameRateEnum(index);
}

// ----------------------------------------------------------------------------

int C5SMPTETimecodeTrackHandle::SetTimecode(int index,
                                            const CSMPTETimecode &newTimecode,
                                            float partialFrame)
{
   int retVal;

   C5SMPTETimecodeContentAPI *track = GetSMPTETimecodeContentAPI();
   if (track == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   retVal = track->SetTimecode(index, newTimecode, partialFrame);
   if (retVal != 0)
      return retVal;

   return 0;

}

int C5SMPTETimecodeTrackHandle::SetTimecodeX(int index, int count,
                                            const CSMPTETimecode &newTimecode,
                                            float partialFrame)
{
   int retVal;

   C5SMPTETimecodeContentAPI *track = GetSMPTETimecodeContentAPI();
   if (track == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   retVal = track->SetTimecodeX(index, count, newTimecode, partialFrame);
   if (retVal != 0)
      return retVal;

   return 0;

}

int C5SMPTETimecodeTrackHandle::SetTimecodeFrameRate(EFrameRate newFrameRate)
{
   int retVal;

   C5SMPTETimecodeContentAPI *track = GetSMPTETimecodeContentAPI();
   if (track == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   retVal = track->SetTimecodeFrameRate(newFrameRate);
   if (retVal != 0)
      return retVal;

   return 0;

}

int C5SMPTETimecodeTrackHandle::SetToDummyValue(int index, int count)
{
   int retVal;

   C5SMPTETimecodeContentAPI *track = GetSMPTETimecodeContentAPI();
   if (track == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   retVal = track->SetToDummyValue(index, count);
   if (retVal != 0)
      return retVal;

   return 0;

}

// ----------------------------------------------------------------------------

int C5SMPTETimecodeTrackHandle::CreateSequenceTrack(const string &clipPath,
                                                    EFrameRate trackFrameRate,
                                                    EFrameRate timecodeFrameRate,
                                                    int itemCount)
{
   int retVal;

   C5SMPTETimecodeTrackHandle timecodeTrackHandle;
   retVal = timecodeTrackHandle.CreateSMPTETimecodeTrack(clipPath,
                                                         trackFrameRate,
                                                         timecodeFrameRate,
                                                         itemCount);
   if (retVal != 0)
      return retVal;  // ERROR: Failed to create the media track.

   // Create new sequence track
   C5SequenceTrack *sequenceTrack
                     = SequenceTrackFactory(CLIP5_CONTENT_TYPE_SMPTE_TIMECODE);
   C5SMPTETimecodeTrackHandle sequenceTrackHandle(sequenceTrack);

   // Create new pulldown segment (if anything allocated) that accomodates
   // relationship between sequence track's frames and media storage
   C5Pulldown *pulldownSegment = 0;
   pulldownSegment = PulldownFactory(CLIP5_CONTENT_TYPE_SMPTE_TIMECODE);
   pulldownSegment->SetItemCount(itemCount);
   pulldownSegment->SetDstStartIndex(0);
   pulldownSegment->SetSrcStartIndex(0);
   pulldownSegment->SetSrcTrack(timecodeTrackHandle);
   pulldownSegment->SetPulldownPhase(0);
   EPulldownType pulldownType;
   pulldownType = PULLDOWN_TYPE_1_1;
   pulldownSegment->SetPulldownType(pulldownType);

   // Add new pulldown segment to sequence track
   sequenceTrack->AppendRange(pulldownSegment);

   // Write sequence track file
   retVal = sequenceTrack->WriteTrackFile(clipPath);
   if (retVal != 0)
      {
      // Write failed
      // TBD: clean up
      // TBD:  Deallocate media storage
      return retVal;
      }

   *this = sequenceTrackHandle;

   return 0;

} // CreateSequenceTrack

int C5SMPTETimecodeTrackHandle::CreateSMPTETimecodeTrack(const string &clipPath,
                                                         EFrameRate newEditRate,
                                                         EFrameRate newTimecodeFrameRate,
                                                         int itemCount)
{
   int retVal;

   // if itemCount is zero, then this is an error and do not create a track
   // TBD

   // TBD: Handle memory allocation exceptions for new operators

   // Create SMPTE Timecode track
   C5SMPTETimecodeTrack *timecodeTrack = new C5SMPTETimecodeTrack;
   timecodeTrack->SetEditRate(newEditRate);
   timecodeTrack->SetTimecodeFrameRate(newTimecodeFrameRate);
   C5SMPTETimecodeTrackHandle timecodeTrackHandle(timecodeTrack);

   if (itemCount > 0)
      {
      C5SMPTETimecodeRange *timecodeRange = new C5SMPTETimecodeRange;
      timecodeRange->SetDstStartIndex(0);
      timecodeRange->SetItemCount(itemCount);
      timecodeRange->SetTimecodeFrameRate(timecodeTrack->GetTimecodeFrameRateEnum());
      timecodeRange->SetTrackEditRate(newEditRate);
      timecodeRange->SetToDummyValue();
      timecodeTrack->AppendRange(timecodeRange);
      }

   retVal = timecodeTrackHandle.WriteTrackFile(clipPath);
   if (retVal != 0)
      {
      // TBD: Cleanup
      // TBD: Error code
      return retVal;
      }

   *this = timecodeTrackHandle;

   return 0;
   
} // CreateSMPTETimecodeTrack

int C5SMPTETimecodeTrackHandle::ExtendTimeTrack(const string &clipPath,
                                                int itemCount)
{
   int retVal;

	////Dump(cout);

   if (itemCount <= 0)
      return 0;   // nothing to do

   if (!IsInitialized())
      return CLIP_ERROR_NO_MEDIA;  // should never happen, exception?

   C5Track *track = GetTrack();
   if (track == nullptr)
      return CLIP_ERROR_NO_MEDIA;  // should never happen, exception?

   if (track->GetContentType() != CLIP5_CONTENT_TYPE_SMPTE_TIMECODE)
      return CLIP_ERROR_NO_MEDIA;   // should never happen, exception?

   if (track->GetTrackType() == CLIP5_TRACK_TYPE_SEQUENCE)
      {
      return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
      }
   else if (track->GetTrackType() == CLIP5_TRACK_TYPE_SMPTE_TIMECODE)
      {
      C5SMPTETimecodeTrack *tcTrack = static_cast<C5SMPTETimecodeTrack*>(track);
      retVal = tcTrack->ExtendTimeTrack(clipPath, itemCount);
      if (retVal != 0)
         return retVal;
      }
   else
      return CLIP_ERROR_NO_MEDIA; // should never happen, exception?;
      
   return 0;

} // ExtendTimeTrack

// ===========================================================================
// ===========================================================================
// Keycode Track Handle Class

C5KeycodeTrackHandle::C5KeycodeTrackHandle()
{
}

C5KeycodeTrackHandle::C5KeycodeTrackHandle(const C5TrackHandle &rhs)
 : C5TrackHandle(rhs)
{
}

C5KeycodeTrackHandle::C5KeycodeTrackHandle(const Clip5TrackID &trackID)
 : C5TrackHandle(trackID)
{
}

C5KeycodeTrackHandle::C5KeycodeTrackHandle(C5Track *track)
 : C5TrackHandle(track)
{
}

C5KeycodeTrackHandle::~C5KeycodeTrackHandle()
{
}

// ----------------------------------------------------------------------------

C5KeycodeContentAPI *C5KeycodeTrackHandle::GetKeycodeContentAPI() const
{
   if (!IsInitialized())
      return 0;

   C5Track *track = GetTrack();
   if (track == nullptr)
      return 0;

   if (track->GetContentType() != CLIP5_CONTENT_TYPE_KEYCODE)
      {
      // NOTE: this error should never happen unless clip data structures
      //       are corrupted, so probably should be an exception

      TRACE_0(errout << "ERROR: C5KeycodeTrackHandle::GetKeycodeContentAPI:" << endl
                     << "       wrong content type in handle " << GetContentType());
      return 0;
      }

   // COMPLETE HACK TO KEEP MAKING PROGRESS
   if (track->GetTrackType() == CLIP5_TRACK_TYPE_SEQUENCE)
      {
//      return static_cast<C5KeycodeSequenceTrack *>(track);
      return 0;
      }
   else if (track->GetTrackType() == CLIP5_TRACK_TYPE_KEYCODE)
      {
      return static_cast<C5KeycodeTrack*>(track);
      }
   else
      return 0;
}

int C5KeycodeTrackHandle::GetKeycode(int index, CMTIKeykode &keycode)
{
   int retVal;

   C5KeycodeContentAPI *track = GetKeycodeContentAPI();
   if (track == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   retVal = track->GetKeycode(index, keycode);
   if (retVal != 0)
      return retVal;

   return 0;

}

// ----------------------------------------------------------------------------

int C5KeycodeTrackHandle::SetKeycode(int index, const CMTIKeykode &newKeycode)
{
   int retVal;

   C5KeycodeContentAPI *track = GetKeycodeContentAPI();
   if (track == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   retVal = track->SetKeycode(index, newKeycode);
   if (retVal != 0)
      return retVal;

   return 0;

}

int C5KeycodeTrackHandle::SetKeycodeX(int index, int count,
                                      const CMTIKeykode &newKeycode)
{
   int retVal;

   C5KeycodeContentAPI *track = GetKeycodeContentAPI();
   if (track == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   retVal = track->SetKeycodeX(index, count, newKeycode);
   if (retVal != 0)
      return retVal;

   return 0;

}

int C5KeycodeTrackHandle::SetToDummyValue(int index, int count)
{
   int retVal;

   C5KeycodeContentAPI *track = GetKeycodeContentAPI();
   if (track == nullptr)
      return CLIP_ERROR_NO_MEDIA;

   retVal = track->SetToDummyValue(index, count);
   if (retVal != 0)
      return retVal;

   return 0;

}

// ----------------------------------------------------------------------------

int C5KeycodeTrackHandle::CreateKeycodeTrack(const string &clipPath,
                                             EFrameRate newEditRate,
                                             int itemCount)
{
   int retVal;

   // if itemCount is zero, then this is an error and do not create a track
   // TBD

   // TBD: Handle memory allocation exceptions for new operators

   // Create Keycode track
   C5KeycodeTrack *keycodeTrack = new C5KeycodeTrack;
   keycodeTrack->SetEditRate(newEditRate);
   C5KeycodeTrackHandle keycodeTrackHandle(keycodeTrack);

   if (itemCount > 0)
      {
      C5KeycodeRange *keycodeRange = new C5KeycodeRange;
      keycodeRange->SetDstStartIndex(0);
      keycodeRange->SetItemCount(itemCount);
      keycodeRange->SetToDummyValue();
      keycodeTrack->AppendRange(keycodeRange);
      }

   retVal = keycodeTrackHandle.WriteTrackFile(clipPath);
   if (retVal != 0)
      {
      // TBD: Cleanup
      // TBD: Error code
      return retVal;
      }

   *this = keycodeTrackHandle;

   return 0;
   
} // CreateKeycodeTrack

int C5KeycodeTrackHandle::ExtendKeycodeTrack(const string &clipPath,
                                            int itemCount)
{
   int retVal;

	////Dump(cout);

   if (itemCount <= 0)
      return 0;   // nothing to do

   if (!IsInitialized())
      return CLIP_ERROR_NO_MEDIA;  // should never happen, exception?

   C5Track *track = GetTrack();
   if (track == nullptr)
      return CLIP_ERROR_NO_MEDIA;  // should never happen, exception?

   if (track->GetContentType() != CLIP5_CONTENT_TYPE_KEYCODE)
      return CLIP_ERROR_NO_MEDIA;   // should never happen, exception?

   if (track->GetTrackType() == CLIP5_TRACK_TYPE_SEQUENCE)
      {
      return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
      }
   else if (track->GetTrackType() == CLIP5_TRACK_TYPE_KEYCODE)
      {
      C5KeycodeTrack *kcTrack = static_cast<C5KeycodeTrack*>(track);
      retVal = kcTrack->ExtendKeycodeTrack(clipPath, itemCount);
      if (retVal != 0)
         return retVal;
      }
   else
      return CLIP_ERROR_NO_MEDIA; // should never happen, exception?;
      
   return 0;

} // ExtendKeycodeTrack

// ===========================================================================
// ===========================================================================

C5TrackRegistry::C5TrackRegistry()
{
}

C5TrackRegistry::~C5TrackRegistry()
{
   for (TrackRegistryEntryListIterator iter = trackList.begin();
        iter != trackList.end();
        ++iter)
      {
      delete *iter;
      }

}

// ---------------------------------------------------------------------------

TrackRegistryEntryListIterator C5TrackRegistry::GetIterator(const Clip5TrackID &trackID,
                                                            bool insertIfNotFound)
{
   TrackRegistryEntryListIterator iter;

   // Search for matching track ID
   for (iter = trackList.begin(); iter != trackList.end(); ++iter)
      {
      if ((*iter)->CompareTrackID(trackID))
         return iter;
      }

   if (insertIfNotFound)
      {
      // The Track ID was not found, so create a new entry
      C5TrackRegistryEntry *newEntry = new C5TrackRegistryEntry(trackID, 0);

      iter = trackList.insert(trackList.end(), newEntry);
      return iter;
      }

   // The entry was not found and the caller did not want to insert it,
   // so return "end" iterator
   return trackList.end();
}

TrackRegistryEntryListIterator C5TrackRegistry::GetIterator(C5Track *track,
                                                            bool insertIfNotFound)
{
   TrackRegistryEntryListIterator iter;

   Clip5TrackID trackID = track->GetTrackID();

   // Search for matching track ID
   for (iter = trackList.begin(); iter != trackList.end(); ++iter)
      {
      if ((*iter)->GetTrack() == track)
         return iter;
      else if ((*iter)->CompareTrackID(trackID))
         {
         // Set the track ptr
         // Note: if this is for a reload, the previous track is
         //       assumed to have be deleted already.
         (*iter)->SetTrack(track);

         return iter;
         }
      }

   if (insertIfNotFound)
      {
      // The Track was not found, so create a new entry
      Clip5TrackID dummyID;
      C5TrackRegistryEntry *newEntry = new C5TrackRegistryEntry(dummyID, track);

      iter = trackList.insert(trackList.end(), newEntry);
      return iter;
      }

   // The entry was not found and the caller did not want to insert it,
   // so return "end" iterator
   return trackList.end();
}

void C5TrackRegistry::DeleteEntry(TrackRegistryEntryListIterator entry)
{
   if (entry == trackList.end())
      return;  // bad arg

   if ((*entry)->DecrRefCount() == 0)
      {
      delete *entry;
      trackList.erase(entry);
      }
}

void C5TrackRegistry::IncrRefCount(TrackRegistryEntryListIterator entry)
{
   if (entry == trackList.end())
      return;  // bad arg

   (*entry)->IncrRefCount();
}

// ------------------------------------------------------------------------

int C5TrackRegistry::ReadTrackFiles(const string &clipPath)
{
   int retVal;

   bool done = false;

   // Loop through track registry until there are no more tracks to be read
   while(!done)
      {
      bool foundNull = false;
      for (TrackRegistryEntryListIterator iter = trackList.begin();
           iter != trackList.end();
           ++iter)
         {
         if ((*iter)->GetTrack() == nullptr)
            {
            // NULL track pointer means the track file has never be read
            foundNull = true;
            retVal = (*iter)->ReadTrackFile(clipPath);
            if (retVal != 0)
               return retVal;
            }
         }
      if (!foundNull)
         done = true;
      }

   return 0;
}

// ===========================================================================

C5TrackRegistryEntry::C5TrackRegistryEntry(const Clip5TrackID &newTrackID,
                                           C5Track *newTrack)
 : referenceCount(0)
{
   if(newTrack == nullptr)
      {
      // No pointer to a track, so just set the track ID.
      // Assume that the track will be read from XML at some future time
      track = 0;
      trackID = newTrackID;
      }
   else
      {
      // Pointer to track has been provided, so use that track's ID
      // and ignore the newTrackID argument
      track = newTrack;
      trackID = track->GetTrackID();
      }
}

C5TrackRegistryEntry::~C5TrackRegistryEntry()
{
   delete track;
}

// ---------------------------------------------------------------------------

C5Track* C5TrackRegistryEntry::GetTrack() const
{
   return track;
}

Clip5TrackID C5TrackRegistryEntry::GetTrackID() const
{
   return trackID;
}

string C5TrackRegistryEntry::GetTrackIDStr() const
{
   return guid_to_string(trackID);
}

// ---------------------------------------------------------------------------

void C5TrackRegistryEntry::SetTrack(C5Track *newTrack)
{
   // This function assumes that the track member variable is a
   // NULL pointer prior to this call.

   track = newTrack;
}

// ---------------------------------------------------------------------------

void C5TrackRegistryEntry::IncrRefCount()
{
   // Make reference counting thread-safe
   CAutoThreadLocker atl(refCountThreadLock);

   ++referenceCount;
}

int C5TrackRegistryEntry::DecrRefCount()
{
   // Make reference counting thread-safe
   CAutoThreadLocker atl(refCountThreadLock);

   if (referenceCount > 0)
      --referenceCount;

   return referenceCount;
}

// ---------------------------------------------------------------------------

bool C5TrackRegistryEntry::CompareTrackID(const Clip5TrackID &otherTrackID) const
{
   return (guid_compare(trackID, otherTrackID) == 0);
}

// ---------------------------------------------------------------------------

int C5TrackRegistryEntry::ReadTrackFile(const string &clipPath)
{
   int retVal;

   // Open an output file stream
   string trackFilename = AddDirSeparator(clipPath) + GetTrackIDStr() + ".xml";

   retVal = ParseTrack(trackFilename);
   if (retVal != 0)
      return retVal;

   if (track != 0)
      track->SetNeedsUpdate(false);

   return 0;
}

int C5TrackRegistryEntry::ReloadTrackFile(const string &clipPath)
{
   int retVal;

   // Remove the existing track
   delete track;
   track = 0;

   retVal = ReadTrackFile(clipPath);
   if (retVal != 0)
      return retVal;

   return 0;
}


// ============================================================================
//
// Convert Clip 3 to Clip 5 (Test Code)
//
// ============================================================================

int ConvertVideoProxy(ClipSharedPtr &clip, int proxyIndex)
{
   int retVal;

   CBinManager binMgr;

   CVideoProxy *videoProxy = clip->getVideoProxy(proxyIndex);
   if (videoProxy == nullptr)
      return -1;

   CVideoFrameList *frameList = videoProxy->getVideoFrameList(0);
   if (frameList == nullptr)
      return -1;

   int totalFrameCount = frameList->getTotalFrameCount();
   int allocatedFrameCount = frameList->getMediaAllocatedFrameCount(true);
   int unallocatedFrameCount = totalFrameCount - allocatedFrameCount;
   const CImageFormat *imageFormat = videoProxy->getImageFormat();

   C5MediaTrackHandle videoTrackHandle;
   C5SequenceTrack *videoTrack
             = videoTrackHandle.SequenceTrackFactory(CLIP5_CONTENT_TYPE_VIDEO);
   videoTrackHandle.SetTrack(videoTrack);

   string clipPath = clip->getClipPathName();

   if (allocatedFrameCount > 0)
      {
      // Create the media location track and gather the media location(s)
      // from the source track
      C5TrackHandle mediaLocationTrackHandle;
      C5MediaTrack *mediaLocationTrack
         = mediaLocationTrackHandle.MediaTrackFactory(CLIP5_CONTENT_TYPE_VIDEO);
      mediaLocationTrack->CopyMediaFormat(*imageFormat);
// TBD      mediaLocationTrack->SetDefaultMediaType(???);
// TBD      mediaLocationTrack->SetDefaultMediaIdentifier(???);
      mediaLocationTrackHandle.SetTrack(mediaLocationTrack);
      retVal = mediaLocationTrack->GatherRawVideoStore(videoProxy);
      if (retVal != 0)
         {
         binMgr.closeClip(clip);
         return retVal; // ERROR
         }

      // Create the media status track and gather the media status from
      // the fields in the source track
      C5MediaStatusTrack *mediaStatusTrack = new C5MediaStatusTrack;
      C5TrackHandle mediaStatusTrackHandle(mediaStatusTrack);
      retVal = mediaStatusTrack->GatherMediaStatus(videoProxy);
      if (retVal != 0)
         {
         binMgr.closeClip(clip);
         return retVal; // ERROR
         }

      mediaLocationTrack->SetMediaStatusTrack(mediaStatusTrackHandle);

      retVal = mediaStatusTrackHandle.WriteTrackFile(clipPath);
      if (retVal != 0)
         return retVal;
      retVal = mediaLocationTrackHandle.WriteTrackFile(clipPath);
      if (retVal != 0)
         return retVal;

      // Create the pulldown segment and add to video track
      C5Pulldown *pulldownSegment = PulldownFactory(CLIP5_CONTENT_TYPE_VIDEO);
      pulldownSegment->SetItemCount(allocatedFrameCount);
      pulldownSegment->SetDstStartIndex(0);
      pulldownSegment->SetSrcStartIndex(0);
      pulldownSegment->SetSrcTrack(mediaLocationTrackHandle);
      pulldownSegment->SetPulldownPhase(0);
      EPulldownType pulldownType;
      pulldownType = (imageFormat->getInterlaced()) ? PULLDOWN_TYPE_2_1
                                                    : PULLDOWN_TYPE_1_1;
      pulldownSegment->SetPulldownType(pulldownType);
      videoTrack->AppendRange(pulldownSegment);
      }

   if (unallocatedFrameCount > 0)
      {
      // Create filler range
      C5FillerRange *fillerRange = new C5FillerRange;
      fillerRange->SetItemCount(unallocatedFrameCount);
      fillerRange->SetDstStartIndex(allocatedFrameCount);
      videoTrack->AppendRange(fillerRange);
      }

   // Write out clip 5 track files (4 of them)
   retVal = videoTrackHandle.WriteTrackFile(clipPath);
   if (retVal != 0)
      return retVal;

   videoProxy->setClipVer(CLIP_VERSION_5L);
   videoProxy->setClip5TrackHandle(videoTrackHandle);

   // Write out clip file
   retVal = clip->writeFile();
   if (retVal != 0)
      return retVal;

   return 0;
}

int ConvertVideoTrack(const string &clipName, int proxyIndex)
{
   int retVal;

   CBinManager binMgr;

   auto clip = binMgr.openClip(clipName, &retVal);
   if (retVal != 0)
      return retVal;

   retVal = ConvertVideoProxy(clip, proxyIndex);
   if (retVal != 0)
      return retVal;

   binMgr.closeClip(clip);

   return 0;
}

int ConvertAllVideoTracks(const string &clipName)
{
   int retVal;

   CBinManager binMgr;

   // Open existing clip
   auto clip = binMgr.openClip(clipName, &retVal);
   if (retVal != 0)
      return retVal;

   int videoProxyCount = clip->getVideoProxyCount();

   // Check that all video proxies are version 3
   for (int i = 0; i < videoProxyCount; ++i)
      {
      CVideoProxy *videoProxy = clip->getVideoProxy(i);
      if (videoProxy == nullptr)
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

      if (videoProxy->getClipVer() != CLIP_VERSION_3)
         return CLIP_ERROR_INVALID_CLIP_VERSION;
      }

   // Make a copy of the existing .clp file
   string clipFileName = clip->getClipFileNameWithExt();
   string backupClipFileName = clipFileName + ".c3";
   if (DoesFileExist(backupClipFileName))
      return 1;  // File already exists, cannot copy over it
   if (!MTICopyFile(clipFileName.c_str(), backupClipFileName.c_str(), true))
      return 2;  // Copy failed, file may already exist

   // Iterate over all of the video proxies, converting each one in turn.
   retVal = 0;
   for (int i = 0; i < videoProxyCount; ++i)
      {
      retVal = ConvertVideoProxy(clip, i);
      if (retVal != 0)
         break;
      }

   binMgr.closeClip(clip);

   if (retVal != 0)
      {

      // Restore the backup copy of the .clp file
      MTICopyFile(backupClipFileName.c_str(), clipFileName.c_str(), false);

      return retVal;
      }

   return 0;
}

// ============================================================================
//
// Clip 5 Unit Test Drivers
//
// ============================================================================

// Test code to create a Clip-5 clip

int MakeNewClip(const string &path, const string &newClipName,
                const string &newClipSchemeName, const string &diskSchemeName,
                int videoReservationCount, int audioReservationCount,
                std::ostringstream &errMsg)
{
   // Create a Clip 5 clip

   int retVal;

   errMsg.str("");

   // Set up the intended parameters for the new clip in the
   // Clip Init Info structure
   CClipInitInfo clipInitInfo;

   // Initialize the clipInitInfo from the clip scheme
   retVal = clipInitInfo.initFromClipScheme(newClipSchemeName);
   if (retVal != 0)
      {
      errMsg << "ERROR: initFromClipSchemFailed.  Status = " << retVal << endl;
      return retVal;
      }

   // Set the Bin Path from the Bin selected in the Bin Tree
   clipInitInfo.setBinPath(path);

   // Set the Clip Name from user's Clip Name entry field
   clipInitInfo.setClipName(newClipName);

   CTimecode protoTC = clipInitInfo.getTimecodePrototype();

   // Set the clip length, and reserved audio and video sizes
   CTimecode inTC = protoTC;
   inTC.setTime(1, 0, 0, 0);   // In TC always = 01:00:00:00
   clipInitInfo.setInTimecode(inTC);

   CTimecode imageReservation = protoTC;
   imageReservation.setAbsoluteTime(videoReservationCount);
   CTimecode audioReservation = protoTC;
   audioReservation.setAbsoluteTime(audioReservationCount);

   if (imageReservation > audioReservation)
       {
       clipInitInfo.setOutTimecode(inTC + imageReservation.getFrameCount());

       // iterate over the audio tracks and make them smaller than the
       // image
       int audioTrackCount = clipInitInfo.getAudioTrackCount();
       for (int i = 0; i < audioTrackCount; ++i)
           {
           CAudioInitInfo *audioInitInfo =
                                        clipInitInfo.getAudioTrackInitInfo(i);
           audioInitInfo->setMediaAllocationFrameCount(
                                           audioReservation.getFrameCount() +
                                           2 * audioInitInfo->getHandleCount());
           }   
       }
   else
       {
       clipInitInfo.setOutTimecode(inTC + audioReservation.getFrameCount());

       // iterate over the image tracks and make them smaller than the
       // audio
       if (clipInitInfo.doesNormalVideoExist())
           {
           CVideoInitInfo *videoInitInfo = clipInitInfo.getMainVideoInitInfo();
           videoInitInfo->setMediaAllocationFrameCount(
               imageReservation.getFrameCount() +
               2 * videoInitInfo->getHandleCount());
           }

       int videoProxyCount = clipInitInfo.getVideoProxyCount();
       for (int i = 0; i < videoProxyCount; ++i)
           {
           CVideoInitInfo *videoInitInfo =
                                         clipInitInfo.getVideoProxyInitInfo(i);
           videoInitInfo->setMediaAllocationFrameCount(
                                            imageReservation.getFrameCount() +
                                           2 * videoInitInfo->getHandleCount());
           }
       }

   // Set the audio and video media locations from the Disk Scheme
   retVal = clipInitInfo.initMediaLocation(diskSchemeName);
   if (retVal != 0)
      {
      errMsg << "ERROR: initMediaLocation failed.  Status = " << retVal << endl;
      errMsg << "       because " << theError.getMessage() << endl;
      return retVal;
      }

   // Check to see if the RFL file is corrupted before creating clip
   retVal = clipInitInfo.CheckRFL();
   if (retVal != 0)
      {
      errMsg << "The raw disk free list (RFL) file may be corrupted, status = "
             << retVal << endl
             << "Creating a new clip is not possible at this time."  << endl;
      return retVal;
      }

   // Update the video init info to specify Clip version 5
   if (clipInitInfo.doesNormalVideoExist())
      {
      CVideoInitInfo *videoInitInfo = clipInitInfo.getMainVideoInitInfo();
//      videoInitInfo->setClipVer(CLIP_VERSION_5L);
      }

   int videoProxyCount = clipInitInfo.getVideoProxyCount();
   for (int i = 0; i < videoProxyCount; ++i)
      {
      CVideoInitInfo *videoInitInfo = clipInitInfo.getVideoProxyInitInfo(i);
//      videoInitInfo->setClipVer(CLIP_VERSION_5L);
      }


   // Update the audio init info to specify Clip 5
   int audioTrackCount = clipInitInfo.getAudioTrackCount();
   for (int i = 0; i < audioTrackCount; ++i)
      {
      CAudioInitInfo *audioInitInfo = clipInitInfo.getAudioTrackInitInfo(i);
//      audioInitInfo->setClipVer(CLIP_VERSION_5L);
      }

   // Update the SMPTE Timecode and Keykode Time Timecode Track init data
   // to specify Clip 5
   int timeTrackCount = clipInitInfo.getTimeTrackCount();
   for (int i = 0; i < timeTrackCount; ++i)
      {
      CTimeInitInfo *timeInitInfo = clipInitInfo.getTimeTrackInitInfo(i);
      if (timeInitInfo->getTimeType() == TIME_TYPE_SMPTE_TIMECODE
          || timeInitInfo->getTimeType() == TIME_TYPE_KEYKODE)
         timeInitInfo->setClipVer(CLIP_VERSION_5L);
      }

   // Create a new Clip based on the Clip Init Info
   auto clip = clipInitInfo.createClip(&retVal);
   if (retVal != 0)
      {
      errMsg << "ERROR: createClip failed, status = " << retVal << endl;
      return retVal;
      }

   // Check to see if the RFL file is still OK
   retVal = clipInitInfo.CheckRFL();
   if (retVal != 0)
      {
      errMsg << "Creating the new clip may have corrupted the RFL file, status = "
             << retVal << endl;
      CBinManager binMgr;
      binMgr.closeClip(clip);
      return retVal;
      }

   CBinManager binMgr;
   binMgr.closeClip(clip);

   return 0;
}

// ==========================================================================

bool getImageMediaDurationTest(CClipInitInfo *clipInfo, CTimecode &duration)
{
   // Unit test code based on TBinManagerForm::getImageMediaDuration

   bool retVal = true;

   duration = clipInfo->getTimecodePrototype();
   duration.setFrameCount(0);

   auto pClip = clipInfo->getClipPtr();

   if (pClip == nullptr)
      {
      // marked clips do this; there might be a better test for marked,
      // and pClip == nullptr for non-marked clips would then be an error
      return true;
      }

   // invariants: all non-virtual image tracks have equal amounts of
   // media allocated

   int videoProxyCount = pClip->getVideoProxyCount();
   vector<int> *allocedFrameCounts = new vector<int>(videoProxyCount);

   for (int i = 0; i < videoProxyCount; ++i)
      {
      CVideoFrameList *frameList =
         pClip->getVideoProxy(i)->getVideoFrameList(FRAMING_SELECT_VIDEO);
      (*allocedFrameCounts)[i] =
         frameList->getMediaAllocatedFrameCountCached();

      if (i > 0 && (*allocedFrameCounts)[i] != (*allocedFrameCounts)[i-1]
          && pClip->getVideoProxy(i)->getVirtualMediaFlag() != true) 
         retVal = false;
      }

   if (retVal == false)
      {
      std::ostringstream ostr;
      ostr << "Warning: track media lengths differ:" << endl;
      for (int i = 0; i < videoProxyCount; ++i)
         {
         ostr << "    video track " << i << ": " << (*allocedFrameCounts)[i] << " frames" << endl;
         }

      TRACE_0(errout << ostr.str() );
      }

   if (videoProxyCount > 0)
      {
      if ((*allocedFrameCounts)[0] >= 0)
         {
         duration.setFrameCount((*allocedFrameCounts)[0]);
         }
      else
         {
         std::ostringstream ostr;
         ostr << "Unable to determine image media duration.  Old clip file?"
              << endl;
         TRACE_1(errout << ostr.str() );
         retVal = false;
         }
      }

   delete allocedFrameCounts;
   return retVal;
}
//---------------------------------------------------------------------------

bool getAudioMediaDurationTest(CClipInitInfo *clipInfo, CTimecode &duration)
{
   // Unit test code based on TBinManagerForm::getAudioMediaDuration

   bool retVal = true;

   duration = clipInfo->getTimecodePrototype();
   duration.setFrameCount(0);

   auto pClip = clipInfo->getClipPtr();

   if (pClip == nullptr)
      {
      // marked clips do this; there might be a better test for marked,
      // and pClip == nullptr for non-marked clips would then be an error
      return true;
      }

   // invariants: all non-virtual audio tracks have equal amounts of
   // media allocated

   int audioTrackCount = pClip->getAudioTrackCount();
   vector<int> *allocedFrameCounts = new vector<int>(audioTrackCount);

   for (int i = 0; i < audioTrackCount; ++i)
      {
      CAudioFrameList *frameList =
         pClip->getAudioTrack(i)->getAudioFrameList(FRAMING_SELECT_VIDEO);
      (*allocedFrameCounts)[i] =
         frameList->getMediaAllocatedFrameCountCached();
      
      if (i > 0 && (*allocedFrameCounts)[i] != (*allocedFrameCounts)[i-1]
          && pClip->getAudioTrack(i)->getVirtualMediaFlag() != true)
         retVal = false;
      }

   if (retVal == false)
      {
      std::ostringstream ostr;
      ostr << "Warning: track media lengths differ:" << endl;
      for (int i = 0; i < audioTrackCount; ++i)
         {
         ostr << "    audio track " << i << ": " << (*allocedFrameCounts)[i] << " frames" << endl;
         }

      TRACE_0(errout << ostr.str() );
      }

   if (audioTrackCount > 0)
      {
      if ((*allocedFrameCounts)[0] >= 0)
         {
         duration.setFrameCount((*allocedFrameCounts)[0]);
         }
      else
         {
         std::ostringstream ostr;
         ostr << "Unable to determine image media duration.  Old clip file?"
              << endl;
         TRACE_1(errout << ostr.str() );
         retVal = false;
         }
      }

   delete allocedFrameCounts;
   return retVal;
}

int IncreaseClipMediaTest(const string &binPath, const string &clipName,
                          int imageAllocDeltaFrameCount,
                          int audioAllocDeltaFrameCount)
{
   // Unit Test Function based on TBinManagerForm::IncreaseClipMedia

   int retVal;

   CBinManager binMgr;

   // Open existing clip and return a CClipInitInfo instance
   std::auto_ptr<CClipInitInfo> clipInfo(binMgr.openClipOrClipIni(binPath, clipName, &retVal));
   if (retVal != 0)
      return retVal;

   auto pClip = clipInfo->getClipPtr();
   if (pClip == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   // Check for enough space on metadata volume
   retVal = clipInfo->checkMetadataStorage();
   if (retVal != 0)
      return -1;

   // invariants: all image tracks have equal amounts of media
   // allocated, all audio tracks have equal amounts of media
   // allocated

   int videoProxyCount = pClip->getVideoProxyCount();
   int audioTrackCount = pClip->getAudioTrackCount();

   for (int i = 0; i < videoProxyCount; ++i)
      {
      retVal = pClip->getVideoProxy(i)->checkExtendMediaStorage(
         imageAllocDeltaFrameCount);
      if (retVal != 0)
         return retVal;
      }

   for (int i = 0; i < audioTrackCount; ++i)
      {
      retVal = pClip->getAudioTrack(i)->checkExtendMediaStorage(
         audioAllocDeltaFrameCount);
      if (retVal != 0)
         return retVal;
      }

   CVideoProxy *videoProxy = pClip->getVideoProxy(0); // main video
   if (videoProxy == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   // Get the Video-Frames Frame List
   CVideoFrameList* videoFrameList =
      videoProxy->getVideoFrameList(FRAMING_SELECT_VIDEO);
   if (videoFrameList == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   int imageTotalFrameCount = videoFrameList->getTotalFrameCount();
   int imageAllocFrameCount = videoFrameList->getMediaAllocatedFrameCount();
   int frameShortage = imageAllocFrameCount + imageAllocDeltaFrameCount
      - imageTotalFrameCount;

   CTimecode duration;
   retVal = getImageMediaDurationTest(&(*clipInfo), duration);
   if (retVal == false)
      return -100;

   duration = duration + imageAllocDeltaFrameCount;
//   int imageNewLength = duration.getFrameCount();

   retVal = getAudioMediaDurationTest(&(*clipInfo), duration);
   if (retVal == false)
      return -200;

   duration = duration + audioAllocDeltaFrameCount;
//   int audioNewLength = duration.getFrameCount();

   // prepare to extend

   CClipBackup clipBackup(pClip->getClipPathName());

   retVal = clipBackup.BackupAllFiles();
   if (retVal != 0)
      return retVal;

   if (frameShortage > 0)
      {
      retVal = pClip->extendClip(frameShortage, false);
      if (retVal)
         return retVal;
      }

   // iterate over the image tracks and extend them
   for (int i = 0; i < videoProxyCount; ++i)
      {
      CVideoProxy *videoProxy = pClip->getVideoProxy(i); // main video

      if (!videoProxy->getVirtualMediaFlag())
         {
         retVal = videoProxy->extendMediaStorage(-1, imageAllocDeltaFrameCount);
         if (retVal != 0)
            return retVal;
         }
      }

   // iterate over audio tracks and increase storage
   CAudioTrack *audioTrack = pClip->getAudioTrack(0);
   if (audioTrack == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   // Get the Audio-Frames Frame List
   CAudioFrameList *audioFrameList = audioTrack->getAudioFrameList(FRAMING_SELECT_VIDEO);
   if (audioFrameList == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   int audioTotalFrameCount = audioFrameList->getTotalFrameCount();
   int audioAllocFrameCount = audioFrameList->getMediaAllocatedFrameCount();

   // careful if using this code as a guide to implement shrinking;
   // allocFrameCount < 0 for virtual media tracks
   frameShortage = audioAllocFrameCount + audioAllocDeltaFrameCount
                    - audioTotalFrameCount;

   if (frameShortage > 0)
      {
      retVal = pClip->extendClip(frameShortage, false);
      if (retVal)
         return retVal;
      }

   for (int i = 0; i < audioTrackCount; ++i)
      {
      CAudioTrack *audioTrack = pClip->getAudioTrack(i);

      if (!audioTrack->getVirtualMediaFlag())
         {
         // we're extending an existing track, so don't worry about
         // handles

         retVal = audioTrack->extendMediaStorage(-1, audioAllocDeltaFrameCount);
         if (retVal != 0)
            return retVal;
         }
      }


   // clear the time tracks in the extended region
   //
   // pClip is only a clip ini pointer, so we must open a proper clip
   // in order to access the time tracks
   ClipSharedPtr clipLocal;
   int iRet;
   clipLocal = binMgr.openClip (pClip->getBinPath(), pClip->getClipName(), &iRet);
   if (iRet == 0)
    {
     for (int iTrack = 0; iTrack < clipLocal->getTimeTrackCount(); iTrack++)
      {
       if (clipLocal->getTimeTrack(iTrack)->getClipVer() == CLIP_VERSION_5L)
         continue;
	   switch (clipLocal->getTimeTrack(iTrack)->getTimeType())
        {
         case TIME_TYPE_SMPTE_TIMECODE:
          {
           // this is related to the audio track
           for (int iCnt = 0; iCnt < audioAllocDeltaFrameCount; iCnt++)
            {
             int iFrame = iCnt + audioAllocFrameCount;
             clipLocal->getTimeTrack(iTrack)->getSMPTETimecodeFrame(iFrame)->
                        initWithDummyValue ();
             clipLocal->getTimeTrack(iTrack)->getSMPTETimecodeFrame(iFrame)->
                        setNeedsUpdate(true);
            }
          }
         break;
         case TIME_TYPE_KEYKODE:
          {
           // this is related to the image track
           for (int iCnt = 0; iCnt < imageAllocDeltaFrameCount; iCnt++)
            {
             int iFrame = iCnt + imageAllocFrameCount;
             clipLocal->getTimeTrack(iTrack)->getKeykodeFrame(iFrame)->
                        initWithDummyValue ();
             clipLocal->getTimeTrack(iTrack)->getKeykodeFrame(iFrame)->
                        setNeedsUpdate(true);
            }
          }
         break;
         case TIME_TYPE_AUDIO_PROXY:
          {
           // this is related to the audio track
           for (int iCnt = 0; iCnt < audioAllocDeltaFrameCount; iCnt++)
            {
             int iFrame = iCnt + audioAllocFrameCount;
             clipLocal->getTimeTrack(iTrack)->getAudioProxyFrame(iFrame)->
                        initWithDummyValue ();
             clipLocal->getTimeTrack(iTrack)->getAudioProxyFrame(iFrame)->
                        setNeedsUpdate(true);
            }
          }
		 break;

	   default:
	   	break;
		}
      }
    }

   // close the clip
   binMgr.closeClip (clipLocal);
   clipLocal = 0;

   return 0;
} // IncreaseClipMediaTest


// =========================================================================

int DecreaseClipMediaTest(const string &binPath, const string &clipName,
                          int imageAllocDeltaFrameCount,
                          int audioAllocDeltaFrameCount)
{
   int retVal;

   CBinManager binMgr;

   // Open existing clip and return a CClipInitInfo instance
   std::auto_ptr<CClipInitInfo> clipInfo(binMgr.openClipOrClipIni(binPath, clipName, &retVal));
   if (retVal != 0)
      return retVal;

   auto pClip = clipInfo->getClipPtr();
   if (pClip == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   // invariants: all image tracks have equal amounts of media
   // allocated, all audio tracks have equal amounts of media
   // allocated

   int videoProxyCount = pClip->getVideoProxyCount();
   int audioTrackCount = pClip->getAudioTrackCount();

   CTimecode duration;
   if (!getImageMediaDurationTest(&(*clipInfo), duration))
      return -100;
   duration = duration + (-imageAllocDeltaFrameCount);
   int imageNewLength = duration.getFrameCount();

   if (!getAudioMediaDurationTest(&(*clipInfo), duration))
      return -200;
   duration = duration + (-audioAllocDeltaFrameCount);
   int audioNewLength = duration.getFrameCount();

   // prepare to release

   CClipBackup clipBackup(pClip->getClipPathName());

   retVal = clipBackup.BackupAllFiles();
   if (retVal != 0)
      return retVal;

   if (imageAllocDeltaFrameCount > 0)
      {
      for (int i = 0; i < videoProxyCount; ++i)
         {
         CVideoFrameList *frameList =
               pClip->getVideoProxy(i)->getVideoFrameList(FRAMING_SELECT_VIDEO);
         int allocedFrameCount = frameList->getMediaAllocatedFrameCount();

         retVal = pClip->getVideoProxy(i)->releaseMediaStorage(imageNewLength,
                                           allocedFrameCount - imageNewLength);
         if (retVal != 0)
            return retVal;
         }
      }

   if (audioAllocDeltaFrameCount > 0)
      {
      for (int i = 0; i < audioTrackCount; ++i)
         {
         CAudioFrameList *frameList =
               pClip->getAudioTrack(i)->getAudioFrameList(FRAMING_SELECT_VIDEO);
         int allocedFrameCount = frameList->getMediaAllocatedFrameCount();

         retVal = pClip->getAudioTrack(i)->releaseMediaStorage(
               audioNewLength, allocedFrameCount - audioNewLength);
         if (retVal != 0)
            return retVal;
         }
      }

   binMgr.closeClip(pClip);

   return 0;

} // DecreaseClipMediaTest

// =========================================================================

CTimecode GetTimecodes(const string &binPath, const string &clipName,
                       int proxyIndex, CTimecode &inTC, CTimecode &outTC)
{
   int retVal;
   CTimecode tc;

   CBinManager binMgr;

   // Open existing clip
   auto clip = binMgr.openClip(binPath, clipName, &retVal);
   if (retVal != 0)
      return tc;

   CVideoProxy *videoProxy = clip->getVideoProxy(proxyIndex);
   CVideoFrameList *frameList = videoProxy->getVideoFrameList(0);

   inTC = frameList->getInTimecode();
   outTC = frameList->getOutTimecode();

   binMgr.closeClip(clip);

   return tc;
}

// ===========================================================================

int SetMediaWritten(const string &binPath, const string &clipName,
                    int proxyIndex, int startFrameNumber, int endFrameNumber)
{
   int retVal;

   CBinManager binMgr;

   // Open existing clip
   auto clip = binMgr.openClip(binPath, clipName, &retVal);
   if (retVal != 0)
      return retVal;

   CVideoProxy *videoProxy = clip->getVideoProxy(proxyIndex);
   CVideoFrameList *frameList = videoProxy->getVideoFrameList(0);

   retVal = frameList->setMediaWritten(startFrameNumber, endFrameNumber, false);
   if (retVal != 0)
      {
      binMgr.closeClip(clip);
      return retVal;
      }

   retVal = clip->updateAllTrackFiles();
   if (retVal != 0)
      {
      binMgr.closeClip(clip);
      return retVal;
      }

   binMgr.closeClip(clip);

   return 0;
}

int ReloadMediaStatusTrackFile(const string &binPath, const string &clipName,
                               int proxyIndex)
{
   int retVal;

   CBinManager binMgr;

   // Open existing clip
   auto clip = binMgr.openClip(binPath, clipName, &retVal);
   if (retVal != 0)
      return retVal;

   CVideoProxy *videoProxy = clip->getVideoProxy(proxyIndex);

   retVal = videoProxy->reloadFieldList();
   if (retVal != 0)
      return retVal;

   return 0;
}

int SetSMPTETimecodeTest(const string &binPath, const string &clipName)
{
   int retVal;

   CBinManager binMgr;

   // Open existing clip and return a CClipInitInfo instance
   std::auto_ptr<CClipInitInfo> clipInfo(binMgr.openClipOrClipIni(binPath, clipName, &retVal));
   if (retVal != 0)
      return retVal;

   auto clip = clipInfo->getClipPtr();
   if (clip == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   CTimeTrack *timeTrack;
   bool foundIt = false;

   for (int i = 0; i < clip->getTimeTrackCount(); ++i)
      {
      timeTrack = clip->getTimeTrack(i);
      if (timeTrack->getTimeType() == TIME_TYPE_SMPTE_TIMECODE &&
          timeTrack->getClipVer() == CLIP_VERSION_5L)
         {
         foundIt = true;
         break;
         }
      }

   if (!foundIt)
      return -10000;


   int itemCount = timeTrack->getTotalFrameCount();

   if (itemCount <= 0)
      return -11000;

   int frameIndex = itemCount / 4;
   CSMPTETimecode smpteTC;
   CTimecode ktTC(1, 0, 0, 0, 0, 30);
//   smpteTC.setTimecode(ktTC);

   smpteTC.setRawTimecode(408101651);

   retVal = timeTrack->setSMPTETimecode(frameIndex, smpteTC, 0.05);
   if (retVal != 0)
      return retVal;


   smpteTC.setRawTimecode(408101652);
   retVal = timeTrack->setSMPTETimecode(frameIndex+1, smpteTC, 0.30);
   if (retVal != 0)
      return retVal;

   smpteTC.setRawTimecode(408101654);
   retVal = timeTrack->setSMPTETimecode(frameIndex+3, smpteTC, 0.80);
   if (retVal != 0)
      return retVal;

   smpteTC.setRawTimecode(408101653);
   retVal = timeTrack->setSMPTETimecode(frameIndex+2, smpteTC, 0.55);
   if (retVal != 0)
      return retVal;

   retVal = clip->updateAllTrackFiles();
   if (retVal != 0)
      return retVal;

   return 0;

} // SetSMPTETimecodeTest

int SetKeykodeTest(const string &binPath, const string &clipName)
{
   int retVal;

   CBinManager binMgr;

   // Open existing clip and return a CClipInitInfo instance
   std::auto_ptr<CClipInitInfo> clipInfo(binMgr.openClipOrClipIni(binPath, clipName, &retVal));
   if (retVal != 0)
      return retVal;

   auto clip = clipInfo->getClipPtr();
   if (clip == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   CTimeTrack *timeTrack;
   bool foundIt = false;

   for (int i = 0; i < clip->getTimeTrackCount(); ++i)
      {
      timeTrack = clip->getTimeTrack(i);
      if (timeTrack->getTimeType() == TIME_TYPE_KEYKODE &&
          timeTrack->getClipVer() == CLIP_VERSION_5L)
         {
         foundIt = true;
         break;
         }
      }

   if (!foundIt)
      return -10000;

   int itemCount = timeTrack->getTotalFrameCount();

   if (itemCount <= 0)
      return -11000;

   int frameIndex = itemCount / 4;

   CMTIKeykode tmpKK;
   EKeykodeFilmDimension filmDimension = KEYKODE_FILM_DIMENSION_35MM_4_PERF;
   // initialize the keykode
   tmpKK.setFilmDimension(filmDimension);
   tmpKK.setCalibrationOffset (0);

   tmpKK.setRawKeykode ("1234567800626928", 0);
   retVal = timeTrack->setKeykode(frameIndex, tmpKK);
   if (retVal != 0)
      return retVal;

   tmpKK.setRawKeykode ("1234567800626932", 0);
   retVal = timeTrack->setKeykode(frameIndex + 1, tmpKK);
   if (retVal != 0)
      return retVal;

   tmpKK.setRawKeykode ("1234567800626940", 0);
   retVal = timeTrack->setKeykode(frameIndex + 3, tmpKK);
   if (retVal != 0)
      return retVal;

   tmpKK.setRawKeykode ("1234567800626936", 0);
   retVal = timeTrack->setKeykode(frameIndex + 2, tmpKK);
   if (retVal != 0)
      return retVal;

   retVal = clip->updateAllTrackFiles();
   if (retVal != 0)
      return retVal;

   return 0;

} // SetKeykodeTest

int GetAudioSampleCountAtFrame(const string &binPath, const string &clipName)
{
   int retVal;

   CBinManager binMgr;

   // Open existing clip and return a CClipInitInfo instance
   std::auto_ptr<CClipInitInfo> clipInfo(binMgr.openClipOrClipIni(binPath, clipName, &retVal));
   if (retVal != 0)
      return retVal;

   auto clip = clipInfo->getClipPtr();
   if (clip == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   CAudioTrack *audioTrack = clip->getAudioTrack(0);
   if (audioTrack == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   CAudioFrameList *frameList = audioTrack->getAudioFrameList(0);
   if (audioTrack == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   int count;

   count = frameList->getSampleCountAtFrame(0);

   return 0;

}

// =========================================================================

int PeekAtVideoMediaLocation(const string &binPath, const string &clipName,
                             int proxyIndex, int frameIndex, int fieldIndex,
                             string &filename, MTI_INT64 &offset)
{
   int retVal;
   CTimecode tc;

   CBinManager binMgr;

   // Open existing clip
   auto clip = binMgr.openClip(binPath, clipName, &retVal);
   if (retVal != 0)
      return retVal;

   CVideoProxy *videoProxy = clip->getVideoProxy(proxyIndex);
   if (videoProxy == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   CVideoFrameList *frameList = videoProxy->getVideoFrameList(0);
   if (frameList == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   retVal = frameList->PeekAtMediaLocation(frameIndex, fieldIndex,
                                           filename, offset);
   if (retVal != 0)
      return retVal;

   binMgr.closeClip(clip);

   return 0;
}

int PeekAtAudioMediaLocation(const string &binPath, const string &clipName,
                             int trackIndex, int frameIndex, int fieldIndex,
                             string &filename, MTI_INT64 &offset)
{
   int retVal;
   CTimecode tc;

   CBinManager binMgr;

   // Open existing clip
   auto clip = binMgr.openClip(binPath, clipName, &retVal);
   if (retVal != 0)
      return retVal;

   CAudioTrack *audioTrack = clip->getAudioTrack(trackIndex);
   if (audioTrack == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   CAudioFrameList *frameList = audioTrack->getAudioFrameList(0);
   if (frameList == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   retVal = frameList->PeekAtMediaLocation(frameIndex, fieldIndex,
                                           filename, offset);
   if (retVal != 0)
      return retVal;

   binMgr.closeClip(clip);

   return 0;
}

// =========================================================================

int PeekAtMediaAllocation(const string &binPath, const string &clipName)
{
   int retVal;
   CTimecode tc;

   CBinManager binMgr;

   // Open existing clip
   auto clip = binMgr.openClip(binPath, clipName, &retVal);
   if (retVal != 0)
      return retVal;

   CVideoProxy *videoProxy = clip->getVideoProxy(0);
   if (videoProxy == nullptr)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   vector<SMediaAllocation> mediaAllocList;
   int iCurrentFrame = -1;
   bool bRequestStop = false;
   retVal = videoProxy->PeekAtMediaAllocation(mediaAllocList, &iCurrentFrame,
                                              &bRequestStop);
   if (retVal != 0)
      return retVal;

   binMgr.closeClip(clip);

   return 0;
}







