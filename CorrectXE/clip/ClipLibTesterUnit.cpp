//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ClipLibTesterUnit.h"
#include "BinManagerGUIUnit.h"
#include "BinMgrGUIInterface.h"
#include "ClipAPI.h"
#include "ClipFileViewerUnit.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TClipLibTesterForm *ClipLibTesterForm;

CClip *OpenClipByName(const string &Name);
string MakeTrueClipName(const string &Name);

//---------------------------------------------------------------------------
__fastcall TClipLibTesterForm::TClipLibTesterForm(TComponent* Owner)
   : TForm(Owner)
{
    // This little bit of code prevents, somehow, a bad_cast exception on a
    // locale in the destructor for MTIForm.  It is a mystery why the 
    // exception happens and another mystery why this fixes the problem.
    int iIn;
    istringstream is("1");
    is >> iIn;

    currentClip = 0;

    currentTimeTrackIndex = -1;
    currentTimeTrack = 0;

    binMgrGUIInterface = new CBinMgrGUIInterface;
}

//---------------------------------------------------------------------------

void __fastcall TClipLibTesterForm::OpenClipButtonClick(TObject *Sender)
{
   ShowBinManagerGUI(false, BM_MODE_SELECT_CLIP);

//    if (lastBin != "")
//        BinManagerForm->SetSelectedBin(lastBin);

   if (BinManagerForm->ShowModal() != mrOk)
      return;

   string newClipName;
   StringList sl = BinManagerForm->SelectedClipNames();
   if (sl.empty())
      return;

   newClipName = sl[0];

   OpenClipByName(newClipName);
}

void TClipLibTesterForm::OpenClipByName(const string newClipName)
{
   CClip *newClip = ::OpenClipByName(newClipName);
   if (newClip == 0)
      {
      string msg = "Could not open clip\n" + newClipName + "\n"
                   + theError.getMessage();
      _MTIErrorDialog(Handle, msg);
      return;
      }

   // Close the current clip
   if (currentClip != 0)
      {
      CBinManager binMgr;
      binMgr.closeClip(currentClip);
      }

   currentClipName = newClipName;
   currentClip = newClip;

   UpdateClipInfoDisplay();
}

void TClipLibTesterForm::CloseCurrentClip()
{
   if (currentClip == 0)
      return;     // Clip isn't open

   CBinManager binMgr;
   binMgr.closeClip(currentClip);

   currentClip = 0;
   currentClipName.erase();

   currentTimeTrackIndex = -1;
   currentTimeTrack = 0;

   UpdateClipInfoDisplay();
}

//---------------------------------------------------------------------------
void TClipLibTesterForm::UpdateClipInfoDisplay()
{
   CurrentClipName->Text = currentClipName.c_str();

   if (ClipFileViewer->Visible)
      {
      if (currentClip != 0)
         ClipFileViewer->ShowFile(MakeTrueClipName(currentClipName));
      else
         ClipFileViewer->Clear();
      }

   UpdateTimeTrackSheet(currentClip);
}

void TClipLibTesterForm::UpdateTimeTrackSheet(CClip *clip)
{
   UpdateTimeTrackList(clip);
}

void TClipLibTesterForm::UpdateTimeTrackList(CClip *clip)
{
   if (clip == 0)
      {
      // No clip, just clear the list
      TimeTrackListView->Items->Clear();
      return;
      }

   TimeTrackListView->Items->BeginUpdate();
   TimeTrackListView->Items->Clear();

   int timeTrackCount = clip->getTimeTrackCount();
   if (timeTrackCount == 0)
      {
      }
   else
      {
      for (int timeTrackIndex = 0; timeTrackIndex < timeTrackCount; ++timeTrackIndex)
         {
         CTimeTrack *timeTrack = clip->getTimeTrack(timeTrackIndex);

         AddTimeTrackToList(TimeTrackListView, timeTrackIndex, timeTrack);
         }
      }

   TimeTrackListView->Items->EndUpdate();

}

void TClipLibTesterForm::AddTimeTrackToList(TListView *list, int itemIndex,
                                            CTimeTrack *timeTrack)
{
   list->Items->BeginUpdate();

   TListItem *listItem = list->Items->Add();
   ostringstream os;
   os << itemIndex;
   listItem->Caption = os.str().c_str();

   // Enter text strings for Time Track information columns, left to right

   // Time Track Type
   string timeTypeStr = CTimeTrack::queryTimeTypeName(timeTrack->getTimeType());
   listItem->SubItems->Add(timeTypeStr.c_str());

   // AppId
   listItem->SubItems->Add(timeTrack->getAppId().c_str());

   list->Items->EndUpdate();
}

//----------------MakeTrueClipName----------------John Mertus----May 2002-----

  string MakeTrueClipName(const string &Name)

//  This returns the actual clip name
//   Note Clips can be named as the following
//     <bin path>/ClipName 
//     <bin path>/ClipName/ClipName
//     <bin path>/ClipName/ClipName.clp
//
//****************************************************************************
{
  // Break the name apart
  string sFP = GetFilePath(Name);
  string sFN = GetFileName(Name);
  string sFE = GetFileExt(Name);
    //
  // Check to see if the name is Starr's double name, if not make it so
  string sTmp = GetFileName(RemoveDirSeparator(sFP));
  if (sTmp == sFN)
    {
      sTmp = AddFileExt(Name,".clp");
    }
  else
    {
      sTmp = AddDirSeparator(sFP + sFN) + sFN + ".clp";
    }
  return sTmp;
}

//-----------------OpenClipByName-----------------John Mertus----Apr 2002-----

    CClip *OpenClipByName(const string &Name)

//  This opens up a clip of name Name
//  if failure, the return is NULL and global error is set; 
//
//****************************************************************************
{
   string sBinName, sClipName, sClipNamePart;
   CBinManager bmBinMgr;
   //
   // First break off file name
   //
   string sName = MakeTrueClipName(Name);
   TRACE_2(errout << "Opening clip with Name " << sName);

   // Add an extension of one does not exist
   sName = AddFileExt(sName, ".clp");
   int iRet = bmBinMgr.splitClipFileName (sName, sBinName, sClipNamePart);
   if (iRet)
      {
      ostringstream os;
      os << "OpenClip: splitClipFileName returned: "
         << iRet << endl;
      TRACE_1(errout << os.str());
      theError.set(CLIP_ERROR_BAD_CLIP_FILENAME,os);
      return(NULL);
      }

   CClip *cpClip = bmBinMgr.openClip(sBinName, sClipNamePart, &iRet);
   if (iRet != 0)
      {
      ostringstream os;
      os << "openClip: returned: " << iRet << endl;
      TRACE_1(errout << os.str());
      theError.set(iRet, os);
      return(NULL);
      }

   return(cpClip);
}


void __fastcall TClipLibTesterForm::FormCloseQuery(TObject *Sender,
      bool &CanClose)
{
   if (currentClip != 0)
      {
      CBinManager binMgr;
      binMgr.closeClip(currentClip);
      }

   CanClose = true;
}
//---------------------------------------------------------------------------

void __fastcall TClipLibTesterForm::ShowClipFileMenuItemClick(TObject *Sender)
{
   if (currentClip == 0)
      return;
      
   ClipFileViewer->Show();
   ClipFileViewer->ShowFile(MakeTrueClipName(currentClipName));
}
//---------------------------------------------------------------------------

void __fastcall TClipLibTesterForm::ExitMenuItemClick(TObject *Sender)
{
   Close();   
}
//---------------------------------------------------------------------------


void __fastcall TClipLibTesterForm::ReloadClipMenuItemClick(
      TObject *Sender)
{
   if (currentClip == 0)
      return;

   string tmpClipName = currentClipName;

   CloseCurrentClip();

   OpenClipByName(tmpClipName);
}
//---------------------------------------------------------------------------

void __fastcall TClipLibTesterForm::CloseClipMenuItemClick(TObject *Sender)
{
   CloseCurrentClip();   
}
//---------------------------------------------------------------------------

void TClipLibTesterForm::ShowTimeTrackGroup(ETimeType timeType)
{
   EventTrackGroup->Visible = (timeType == TIME_TYPE_EVENT);
   KeykodeTrackGroup->Visible = (timeType == TIME_TYPE_KEYKODE);
   SMPTETimecodeTrackGroup->Visible = (timeType == TIME_TYPE_SMPTE_TIMECODE);
   TimecodeTrackGroup->Visible = (timeType == TIME_TYPE_TIMECODE);
}

void TClipLibTesterForm::UpdateTimeTrackGroup(int timeTrackIndex,
                                              CTimeTrack *timeTrack)
{
   currentTimeTrackIndex = timeTrackIndex;
   currentTimeTrack = timeTrack;
   
   if (timeTrack == 0)
      {
      ShowTimeTrackGroup(TIME_TYPE_INVALID);
      return;
      }

   ETimeType timeType = timeTrack->getTimeType();

   ShowTimeTrackGroup(timeType);

   switch(timeType)
      {
      case TIME_TYPE_EVENT :
         UpdateEventTimeTrackGroup(timeTrackIndex, timeTrack);
         break;
      case TIME_TYPE_KEYKODE :
         UpdateKeykodeTimeTrackGroup(timeTrackIndex, timeTrack);
         break;
      case TIME_TYPE_SMPTE_TIMECODE :
         UpdateSMPTETimecodeTimeTrackGroup(timeTrackIndex, timeTrack);
         break;
      case TIME_TYPE_TIMECODE :
         UpdateTimecodeTimeTrackGroup(timeTrackIndex, timeTrack);
         break;
      }
}

void TClipLibTesterForm::UpdateEventTimeTrackGroup(int timeTrackIndex,
                                                   CTimeTrack *timeTrack)
{
   if (timeTrack == 0)
      return;
}

void TClipLibTesterForm::UpdateKeykodeTimeTrackGroup(int timeTrackIndex,
                                                     CTimeTrack *timeTrack)
{
   if (timeTrack == 0)
      return;

   UpdateKeykodeFrameList(timeTrack);
}

void TClipLibTesterForm::UpdateKeykodeFrameList(CTimeTrack *timeTrack)
{
   if (timeTrack == 0)
      {
      // No clip, just clear the list
      KeykodeFrameList->Items->Clear();
      return;
      }

   KeykodeFrameList->Items->BeginUpdate();
   KeykodeFrameList->Items->Clear();

   int frameCount = timeTrack->getTotalFrameCount();
   ostringstream os;
   os << frameCount;
//   KeykodeTrackFrameCountValue->Caption = os.str().c_str();

//   for (int frameIndex = 0; frameIndex < frameCount; ++frameIndex)
   for (int frameIndex = 0; frameIndex < 32; ++frameIndex)
      {
      CKeykodeFrame *frame = timeTrack->getKeykodeFrame(frameIndex);

      AddKeykodeFrameToList(KeykodeFrameList, frameIndex, frame);
      }

   KeykodeFrameList->Items->EndUpdate();

}

void TClipLibTesterForm::AddKeykodeFrameToList(TListView *list, int frameIndex,
                                               CKeykodeFrame *frame)
{
   if (frame == 0)
      return;

   list->Items->BeginUpdate();

   TListItem *listItem = list->Items->Add();
   ostringstream os;
   os << frameIndex;
   listItem->Caption = os.str().c_str();

   // Enter text strings for Timecode Frame information columns, left to right

   // Time Track Type
   string keykodeStr;
   if (KeykodeDisplayStyle->ItemIndex == 1)
      {
      // Formatted keykode
      keykodeStr = frame->getFormattedStr(MTI_KEYKODE_FORMAT_KEYKODE, false);
      }
   else if (KeykodeDisplayStyle->ItemIndex == 2)
      {
      // Feet + Frames
      keykodeStr = frame->getFormattedStr(MTI_KEYKODE_FORMAT_FEET_PLUS_FRAMES, false);
      }
   else if (KeykodeDisplayStyle->ItemIndex == 3)
      {
      // Feet + Frames . Perfs
      keykodeStr = frame->getFormattedStr(MTI_KEYKODE_FORMAT_FEET_PLUS_FRAMES_DOT_PERFS, false);
      }
   else
      {
      // Raw keykode, all 20 characters unformatted
      keykodeStr = frame->getFormattedStr(MTI_KEYKODE_FORMAT_RAW, false);
      }
      
   listItem->SubItems->Add(keykodeStr.c_str());

   list->Items->EndUpdate();
}
void TClipLibTesterForm::UpdateSMPTETimecodeTimeTrackGroup(int timeTrackIndex,
                                                         CTimeTrack *timeTrack)
{
   if (timeTrack == 0)
      return;
}

void TClipLibTesterForm::UpdateTimecodeTimeTrackGroup(int timeTrackIndex,
                                                      CTimeTrack *timeTrack)
{
   if (timeTrack == 0)
      return;

   ostringstream os;
   os << timeTrackIndex;
   TimecodeTrackIndexValue->Caption = os.str().c_str();

   TimecodeTrackAppIdValue->Caption = timeTrack->getAppId().c_str();

   UpdateTimecodeFrameList(timeTrack);
}

void TClipLibTesterForm::UpdateTimecodeFrameList(CTimeTrack *timeTrack)
{
   if (timeTrack == 0)
      {
      // No clip, just clear the list
      TimecodeFrameList->Items->Clear();
      return;
      }

   TimecodeFrameList->Items->BeginUpdate();
   TimecodeFrameList->Items->Clear();

   int frameCount = timeTrack->getTotalFrameCount();
   ostringstream os;
   os << frameCount;
   TimecodeTrackFrameCountValue->Caption = os.str().c_str();

   for (int frameIndex = 0; frameIndex < frameCount; ++frameIndex)
      {
      CTimecodeFrame *frame = timeTrack->getTimecodeFrame(frameIndex);

       AddTimecodeFrameToList(TimecodeFrameList, frameIndex, frame);
      }

   TimecodeFrameList->Items->EndUpdate();

}

void TClipLibTesterForm::AddTimecodeFrameToList(TListView *list, int frameIndex,
                                                CTimecodeFrame *frame)
{
   if (frame == 0)
      return;

   list->Items->BeginUpdate();

   TListItem *listItem = list->Items->Add();
   ostringstream os;
   os << frameIndex;
   listItem->Caption = os.str().c_str();

   // Enter text strings for Timecode Frame information columns, left to right

   // Time Track Type
   char timecodeStr[20];
   frame->getTimecode().getTimeASCII(timecodeStr);
   listItem->SubItems->Add(timecodeStr);

   list->Items->EndUpdate();
}
//---------------------------------------------------------------------------

void __fastcall TClipLibTesterForm::TimeTrackListViewDblClick(
      TObject *Sender)
{
   if (currentClip == 0)
      return;

   TListItem *listItem = TimeTrackListView->Selected;
   if (listItem == 0)
      return;   // nothing really selected

   istringstream is;
   is.str(listItem->Caption.c_str());
   int timeTrackIndex;
   is >> timeTrackIndex;

   CTimeTrack *timeTrack = currentClip->getTimeTrack(timeTrackIndex);
   if (timeTrack == 0)
      return;

   UpdateTimeTrackGroup(timeTrackIndex, timeTrack);

}
//---------------------------------------------------------------------------

void __fastcall TClipLibTesterForm::SetTimecodesButtonClick(
      TObject *Sender)
{
   int retVal;
   ostringstream os;

   if (currentClip == 0 || currentTimeTrack == 0)
      return;

   // copy timecodes from main video to timecode track

   CVideoFrameList *videoFrameList = currentClip->getVideoFrameList(0, 0);
   if (videoFrameList == 0)
      return;

   int videoFrameCount = videoFrameList->getTotalFrameCount();

   int timeFrameCount = currentTimeTrack->getTotalFrameCount();

   if (timeFrameCount != videoFrameCount)
      {
      os << "Video and Time Track frame counts do not match\n";
      os << " Video Frame Count: " << videoFrameCount << "\n";
      os << " Time Track Frame Count: " << timeFrameCount;
      _MTIErrorDialog(Handle, os.str());
      return;
      }

   int updateGroupSize = 10;

   for (int frameIndex = 16; frameIndex < timeFrameCount-16; ++frameIndex)
      {
      CVideoFrame *videoFrame = videoFrameList->getFrame(frameIndex);
      CTimecodeFrame *timecodeFrame = currentTimeTrack->getTimecodeFrame(frameIndex);
      timecodeFrame->setTimecode(videoFrame->getTimecode());

      if ((frameIndex % updateGroupSize) == 0)
         {
         retVal = currentTimeTrack->updateFrameListFile();
         if (retVal != 0)
            {
            os << "Update Frame List File failed, return code " << retVal;
            _MTIErrorDialog(Handle, os.str());
            return;
            }
         }
      }

   retVal = currentTimeTrack->updateFrameListFile();
   if (retVal != 0)
      {
      os << "Update Frame List File failed, return code " << retVal;
      _MTIErrorDialog(Handle, os.str());
      return;
      }

   UpdateTimecodeTimeTrackGroup(currentTimeTrackIndex, currentTimeTrack);

}
//---------------------------------------------------------------------------

void __fastcall TClipLibTesterForm::ClearTimecodesButtonClick(
      TObject *Sender)
{
   int retVal;
   ostringstream os;

   if (currentClip == 0 || currentTimeTrack == 0)
      return;

   int timeFrameCount = currentTimeTrack->getTotalFrameCount();

   CTimecode tc(0);

   int updateGroupSize = 10;

   for (int frameIndex = 0; frameIndex < timeFrameCount; ++frameIndex)
      {
      CTimecodeFrame *timecodeFrame = currentTimeTrack->getTimecodeFrame(frameIndex);
      timecodeFrame->setTimecode(tc);

      if ((frameIndex % updateGroupSize) == 0)
         {
         retVal = currentTimeTrack->updateFrameListFile();
         if (retVal != 0)
            {
            os << "Update Frame List File failed, return code " << retVal;
            _MTIErrorDialog(Handle, os.str());
            return;
            }
         }
      }

   retVal = currentTimeTrack->updateFrameListFile();
   if (retVal != 0)
      {
      os << "Update Frame List File failed, return code " << retVal;
      _MTIErrorDialog(Handle, os.str());
      return;
      }

   UpdateTimecodeTimeTrackGroup(currentTimeTrackIndex, currentTimeTrack);

}
//---------------------------------------------------------------------------


void __fastcall TClipLibTesterForm::UseFrameIndicesButtonClick(
      TObject *Sender)
{
   int retVal;
   ostringstream os;

   if (currentClip == 0 || currentTimeTrack == 0)
      return;

   int timeFrameCount = currentTimeTrack->getTotalFrameCount();

   CTimecode tc(0);

   int updateGroupSize = 10;

   for (int frameIndex = 0; frameIndex < timeFrameCount; ++frameIndex)
      {
      CTimecodeFrame *timecodeFrame = currentTimeTrack->getTimecodeFrame(frameIndex);
      tc.setFrameCount(frameIndex);
      timecodeFrame->setTimecode(tc);

      if ((frameIndex % updateGroupSize) == 0)
         {
         retVal = currentTimeTrack->updateFrameListFile();
         if (retVal != 0)
            {
            os << "Update Frame List File failed, return code " << retVal;
            _MTIErrorDialog(Handle, os.str());
            return;
            }
         }
      }

   retVal = currentTimeTrack->updateFrameListFile();
   if (retVal != 0)
      {
      os << "Update Frame List File failed, return code " << retVal;
      _MTIErrorDialog(Handle, os.str());
      return;
      }

   UpdateTimecodeTimeTrackGroup(currentTimeTrackIndex, currentTimeTrack);

}
//---------------------------------------------------------------------------


void __fastcall TClipLibTesterForm::KeykodeDisplayStyleClick(
      TObject *Sender)
{
   UpdateKeykodeFrameList(currentTimeTrack);      
}
//---------------------------------------------------------------------------


void __fastcall TClipLibTesterForm::SetKeykodesButtonClick(TObject *Sender)
{
   int retVal;
   ostringstream os;

   if (currentClip == 0 || currentTimeTrack == 0)
      return;

   int timeFrameCount = currentTimeTrack->getTotalFrameCount();

   // Random keykode information
   char *mfgCode = "12";
   char *filmCode = "20";    // 12 + 20 should be "KY"
   char *prefix = "345678";
   char *filler = "0000";
   int calibOffset = 13;

   char tmpKeykodeStr[20 + 1];

   // iterate over the keykode frames, setting each to something interesting
   for (int frameIndex = 0; frameIndex < timeFrameCount; ++frameIndex)
      {
      CKeykodeFrame *keykodeFrame = currentTimeTrack->getKeykodeFrame(frameIndex);

      // The film dimensions and calibration offset have to be set before
      // setting the raw keykode string
      keykodeFrame->setFilmDimension(KEYKODE_FILM_DIMENSION_35MM_4_PERF);
      keykodeFrame->setCalibrationOffset(calibOffset);

      // Make up a raw keykode as might come from a keykode reader
      // Perfs only updated at foot and half foot
      // Interpolation offset makes up the difference
      int feet = 3000 + (frameIndex / 16);    // assume 16 frames per foot
      int perfs;
      if (frameIndex % 16 == 0)
         perfs = 0;
      else if (frameIndex % 8 == 0)
         perfs = 32;
      sprintf(tmpKeykodeStr, "%s%s%s%.4d%.2d%s", mfgCode, filmCode, prefix,
              feet, perfs, filler);

      int dummyInterpolateOffset = (frameIndex % 8) * 4;

      // Set the raw keykode string into the keykode frame
      keykodeFrame->setRawKeykode(tmpKeykodeStr, dummyInterpolateOffset);
      }

   // Write keykode track back to file
   retVal = currentTimeTrack->updateFrameListFile();
   if (retVal != 0)
      {
      os << "Update Frame List File failed, return code " << retVal;
      _MTIErrorDialog(Handle, os.str());
      return;
      }

   // Update the display
   UpdateKeykodeTimeTrackGroup(currentTimeTrackIndex, currentTimeTrack);
}
//---------------------------------------------------------------------------

void __fastcall TClipLibTesterForm::Button1Click(TObject *Sender)
{
   int retVal;
   istringstream istr;

   if (currentClip == 0)
      {
      Beep();
      return;
      }

   istr.str(FrameNumberEdit->Text.c_str());
   int frameNumber;
   istr >> frameNumber;

   istr.clear();
   istr.str(SampleOffsetEdit->Text.c_str());
   int sampleOffset;
   istr >> sampleOffset;

   istr.clear();
   istr.str(SampleCountEdit->Text.c_str());
   int sampleCount;
   istr >> sampleCount;

   CAudioFrameList *frameList;
   frameList = currentClip->getAudioFrameList(0);

   if (frameList == 0)
      {
      _MTIErrorDialog(Handle, "This clip does not have an audio track");
      return;
      }

   MTI_UINT8 *bigBuffer = new MTI_UINT8[1024*1024*16];
   retVal = frameList->readMediaBySample(frameNumber, sampleOffset,
                                         sampleCount, bigBuffer);
   if (retVal != 0)
      {
      ostringstream os;
      os << "readMediaBySample("
         << frameNumber << ", "
         << sampleOffset << ", "
         << sampleCount << ")" << endl
         << "failed with return code "
         << retVal;
      _MTIErrorDialog(Handle, os.str());
      }

   delete [] bigBuffer;

}
//---------------------------------------------------------------------------

