//---------------------------------------------------------------------------

#pragma hdrstop

#include "HistoryLocation.h"

#include "IniFile.h"
#include "err_clip.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
//---------------------------------------------------------------------------


// MtiMachineLocal.ini stuff
const string MetaDataSectionName       = "Metadata";
const string MetaDataRootDirectoryKey  = "MetadataRootDirectory";
const string IniFileInMetadataDirKey   = "IniFileInMetadataDir";

// MTIMetadata.ini stuff
const string MetaDataFileName     = "MTIMetadata.ini";
const string HistorySectionName   = "History";
const string HistoryDirectoryKey  = "HistoryDirectory";
const string UseDefaultLocationKey = "UseDefaultLocation";

constexpr bool OPEN_INI_FILE_READ_ONLY = true;
constexpr bool OPEN_INI_FILE_READ_WRITE = false;
//---------------------------------------------------------------------------

/////////////////////////////////////////////////////////////////////////////
//
// The history dir for the clip is made by taking the dir part of the
// imageFileNameTemplate and appending it (without its disk preface) to
// the main history path specified in MTILocalMachine.ini
//
// This will be replaced by an edit box to be filled-in by the user in
// the clip-creation process
//
string HistoryLocation::computeHistoryLocation(const string &imageFileDirectory)
{
   string historyDirectory;

   if (!DoesDirectoryExist(imageFileDirectory))
   {
      CAutoErrorReporter autoErr("HistoryLocation::getPerFrameHistoryLocation",
                                 AUTO_ERROR_NO_NOTIFY, AUTO_ERROR_TRACE_0);
      autoErr.msg << "ERROR: Cannot find media folder"  << endl
                  << imageFileDirectory;
      autoErr.errorCode = CLIP_ERROR_CANT_MAKE_HISTORY_DIRECTORY;

      // ?? return here??
   }

   auto metadataIniFilePath = computeMetadataIniFilePath(imageFileDirectory);

   // Get the history directory from MTIMetadata.ini.
   // Make sure to not create the ini file at this point.
   CIniFile *metadataIniFile = CreateIniFile(metadataIniFilePath, OPEN_INI_FILE_READ_ONLY);
   if (metadataIniFile != NULL)
   {
      // If history is at the "default location", then we ignore the historyDirectory
      // entry and just use the image file directory. Assume false because we need
      // to deal with MTImetadata.ini files created in by earlier versions of DRS Nova.
      if (!metadataIniFile->ReadBool(HistorySectionName, UseDefaultLocationKey, false))
      {
         // Read history directory specification from the ini file.
         historyDirectory = metadataIniFile->ReadString(HistorySectionName, HistoryDirectoryKey, historyDirectory);
         ExpandEnviornmentString(historyDirectory);
         ConformFileName(historyDirectory);
      }

      DeleteIniFile(metadataIniFile);
   }

   // If we didn't find the location of the history folder from the
   // MTIMetadata.ini, compute it here
   auto defaultHistoryDirectory = AddDirSeparator(GetFilePath(metadataIniFilePath)) + "History";
   if (historyDirectory.empty())
   {
      historyDirectory = defaultHistoryDirectory;
   }

   // If the history directory from the MTImetadata.ini file doesn't exist, but
   // there is one in the MTImetadata.ini's directory, then we assume the
   // MTImetadata.ini's directory was copied or moved, so use that history folder
   if (historyDirectory != defaultHistoryDirectory &&
       !DoesDirectoryExist(historyDirectory))
   {
      if (DoesDirectoryExist(defaultHistoryDirectory))
      {
         historyDirectory = defaultHistoryDirectory;
      }
   }

   // Now create or update the MTIMetadata.ini and create the history folder as needed.
   updateHistoryLocationIfNecessary(metadataIniFilePath, historyDirectory);

   return historyDirectory;
}

string HistoryLocation::computeMetadataIniFilePath(const string &imageFileDirectory)
{
   if (!DoesDirectoryExist(imageFileDirectory))
   {
      CAutoErrorReporter autoErr("HistoryLocation::computeMetaDataIniFilePath",
                                 AUTO_ERROR_NO_NOTIFY, AUTO_ERROR_TRACE_0);
      autoErr.msg << "ERROR: Cannot find media folder"  << endl
                  << imageFileDirectory;
      autoErr.errorCode = CLIP_ERROR_CANT_MAKE_HISTORY_DIRECTORY;

      // ?? return here??
   }

   // Defaults
   string metaDataDirectory = imageFileDirectory;
   bool putMetadataIniInMetadataDir = false;

   // See if the defaults are overridden in MTILocalMachine.ini .
   CIniFile *localMachineIniFile = CPMPOpenLocalMachineIniFile();
   if (localMachineIniFile != NULL)
   {
      // Never change the ini file.
      localMachineIniFile->ReadOnly(true);

      ////////////////// in MTILocalMachine.ini
      //
      // [Metadata]
      // MetadataRootDirectory=$(CPMP_SHARED_DIR)MTIMetadata    (default is to use image folder)
      // IniFileInMetadataDir=false     (default)
      //////////////////

      auto metadataRootDirectory = localMachineIniFile->ReadString(MetaDataSectionName, MetaDataRootDirectoryKey, string(""));
      if (!metadataRootDirectory.empty())
      {
         ExpandEnviornmentString(metadataRootDirectory);
         ConformFileName(metadataRootDirectory);

         // get directory portion of media identifier
         string imageDirNoDriveOrLeadSlash = imageFileDirectory;
         if ((((imageFileDirectory[0] >= 'a') && (imageFileDirectory[0] <= 'z'))
               || ((imageFileDirectory[0] >= 'A') && (imageFileDirectory[0] <= 'Z'))) &&
               (imageFileDirectory[1] == ':') && (imageFileDirectory[2] == '\\'))
         {
            imageDirNoDriveOrLeadSlash = imageFileDirectory.substr(3);
         }

         metaDataDirectory = AddDirSeparator(metadataRootDirectory) + imageDirNoDriveOrLeadSlash;
      }

      putMetadataIniInMetadataDir = localMachineIniFile->ReadBool(MetaDataSectionName, IniFileInMetadataDirKey, false);

      DeleteIniFile(localMachineIniFile);
   }

   auto metadataIniFilePath = AddDirSeparator(metaDataDirectory) + MetaDataFileName;

   bool foundIniInMetaDataDir = DoesFileExist(metadataIniFilePath);
   bool foundIniInImageFileDir = DoesFileExist(AddDirSeparator(imageFileDirectory) + MetaDataFileName);

   // - If we found the MTIMetadata.ini in the external metadata tree, always
   //   use that, regardless of the putMetadataIniInMetadataDir flag setting.
   // - else if we found it in the media folder, use that regardless of the
   //   putMetadataIniInMetadataDir flag setting.
   // - else we did not find a MTIMetadata.ini, so create it where the flag says to.
   if (!foundIniInMetaDataDir &&
      (foundIniInImageFileDir || !putMetadataIniInMetadataDir))
   {
      metadataIniFilePath = AddDirSeparator(imageFileDirectory) + MetaDataFileName;
   }

   // Now check for the History folder.

   return metadataIniFilePath;
}

int HistoryLocation::updateHistoryLocationIfNecessary(
   const string &metadataIniFilePath,
   const string &historyDirectory)
{
   // It is required that the history folder either exist or be creatable at this point.

   // Write the history directory entry back to the MTIMetadata.ini, creating it if necessary.
   auto metadataIniFile = CreateIniFile(metadataIniFilePath, OPEN_INI_FILE_READ_WRITE);
   if (metadataIniFile != nullptr)
   {
      auto defaultHistoryDirectory = AddDirSeparator(GetFilePath(metadataIniFilePath)) + "History";
      auto historyIsAtDefaultLocation = historyDirectory == defaultHistoryDirectory;
      if (DoesWritableDirectoryExist(historyDirectory) &&
          historyDirectory == metadataIniFile->ReadString(HistorySectionName, HistoryDirectoryKey, "") &&
          metadataIniFile->KeyExists(HistorySectionName, UseDefaultLocationKey) &&
          historyIsAtDefaultLocation == metadataIniFile->ReadBool(HistorySectionName, UseDefaultLocationKey, false))
      {
         // It's a match and the directory exists and is writable - don't change the MTIMetadata.ini file.
         metadataIniFile->ReadOnly(true);
      }
      else
      {
         // Update the MTImetadata.ini file to point at the correct history directory.
         // We write the history directory path even if it's at the default
         // location to be compatible with older versions of DRS Nova.
         metadataIniFile->WriteBool(HistorySectionName, UseDefaultLocationKey, historyIsAtDefaultLocation);
         metadataIniFile->WriteString(HistorySectionName, HistoryDirectoryKey, historyDirectory);
         if (!DoesDirectoryExist(historyDirectory))
         {
            // The history directory doen't exist, so create it now.
            if (!MakeDirectory(historyDirectory))
            {
               CAutoErrorReporter autoErr("HistoryLocation::getPerFrameHistoryLocation",
                                          AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
               autoErr.msg << "ERROR: Can't create History folder " << endl
                           << "in " << GetFilePath(historyDirectory) << "." << endl
                           << "History will not be saved for this media!";
               return autoErr.errorCode;
            }
         }
         else
         {
            // Test the history directory for writability.
            if (!DoesWritableDirectoryExist(historyDirectory))
            {
               CAutoErrorReporter autoErr("HistoryLocation::getPerFrameHistoryLocation",
                                          AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
               autoErr.msg << "ERROR: History folder"  << endl
                           << "in " << GetFilePath(historyDirectory) << endl
                           << "exists but is not writable." << endl
                           << "History will not be saved for this media!";
               autoErr.errorCode = CLIP_ERROR_CANT_MAKE_HISTORY_DIRECTORY;
               return autoErr.errorCode;
            }
         }
      }

      if (!DeleteIniFile(metadataIniFile))
      {
         // Can't write MTIMetadata.ini .
         CAutoErrorReporter autoErr("HistoryLocation::getPerFrameHistoryLocation",
                                    AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
         autoErr.msg << "ERROR: Can't write metadata config file " << GetFileNameWithExt(metadataIniFilePath) << endl
                     << "in " << GetFilePath(metadataIniFilePath) << "." << endl
                     << "History will not be saved for this media!";
         autoErr.errorCode = CLIP_ERROR_CANT_MAKE_HISTORY_DIRECTORY;
         return autoErr.errorCode;
      }
   }
   else
   {
      // Can't read MTIMetadata.ini .
      CAutoErrorReporter autoErr("HistoryLocation::getPerFrameHistoryLocation",
                                 AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
      autoErr.msg << "ERROR: Metadata config file " << GetFileNameWithExt(metadataIniFilePath) << endl
                  << "in " << GetFilePath(metadataIniFilePath) << endl
                  << "cannot be created because it's an invalid path." << endl
                  << "History will not be saved for this media!";
      autoErr.errorCode = CLIP_ERROR_CANT_MAKE_HISTORY_DIRECTORY;
      return autoErr.errorCode;
   }

   // Success!
   return 0;
}
