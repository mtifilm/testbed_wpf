// StructuredFileStream.h: interface for the CStructuredFileStream class.
//
/*
$Header: /usr/local/filmroot/clip/include/StructuredFileStream.h,v 1.3 2005/03/31 16:45:59 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef STRUCTUREDFILESTREAMH
#define STRUCTUREDFILESTREAMH

#include "StructuredStream.h"
#include "FileIO.h"

//////////////////////////////////////////////////////////////////////

class CStructuredFileStream : public CStructuredStream  
{
public:
   CStructuredFileStream();
   CStructuredFileStream(char *newBuffer, unsigned int newBufferSize);
   virtual ~CStructuredFileStream();

   virtual int createFile(const string& fileName);
   virtual int open(const string& fileName);
   virtual int close();
   virtual int read(void* buffer, unsigned int count);
   virtual int write(const void* buffer, unsigned int count);
   virtual int seek(MTI_INT64 offset);
   virtual void initBuffer();

private:
   CFileIO fileIO;     // Interface to Low-Level File I/O

   bool internalBufferAllocation;
   unsigned int bufferSize;
   unsigned int writeAvailable;
   unsigned int readAvailable;
   char* bufferHead;
   char* bufferPtr;

   int fillBuffer();
   int flushBuffer();
};

//////////////////////////////////////////////////////////////////////

#endif // #ifndef STRUCTUREDFILESTREAM_H

