// 
// File: createclip3.cpp
//
// main function for createclip utility program.
//                 .
// This program creates a Clip File and allocates Media Storage
// based on the user's specification in the Clip Initialization File
//
/*
$Header: /usr/local/filmroot/clip/source/createclip3.cpp,v 1.12 2006/03/10 14:43:29 tolks Exp $
*/
//////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>

#include "ClipAPI.h"
#include "IniFile.h"


//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////

// Print usage message
void printUsage()
{
   cerr << "Usage: " << endl;
   cerr <<     "createclip3 [-v] initFileName" << endl;
   cerr << "       -v  Verbose mode, prints out extra information about clip"
        << endl;
   cerr << "       initFileName = Clip Initialization File Name" << endl;
}

//////////////////////////////////////////////////////////////////////

static bool verboseMode;      // Global verbose mode flag

//////////////////////////////////////////////////////////////////////

int main(int argc, char* argv[])
   {
   CreateApplicationName(argv[0]);
   
   // Check command line arguments
   if (argc != 2 && argc != 3)
      {
      // Oops, unexpected number of arguments
      // Print out usage message then exit
      printUsage();
      return 1;
      }
   else if (argc == 3 && strcmp(argv[1], "-v") != 0)
      {
      // Oops, if two arguments, first must be verbose switch
      printUsage();
      return 1;
      }

   // Check if verbose switch is set
   verboseMode = (argc == 3 && strcmp(argv[1], "-v") == 0);

   // Clip Init file name assumed to be last argument
   string clipInitFileName = argv[argc - 1];

   // Verify the existence of the clip initialization file
   if (!DoesFileExist(clipInitFileName))
      {
      cerr << "ERROR: Clip Initialization File " << clipInitFileName
           << " cannot be found" << endl;
      return 1;
      }

   // Read the clip initialization file and set the clip init info from it
   CIniFile *iniFile = CreateIniFile(clipInitFileName);
   if (iniFile == 0)
      {
      cerr << "ERROR: Cannot open Clip Initialization File " << clipInitFileName
           << endl;
      return 1;
      }

   CClipInitInfo clipInitInfo;
   int retVal;

   // Set the clip init info from the contents of the clip initialization file
   retVal = clipInitInfo.readClipScheme(iniFile);
   if (retVal != 0)
      {
      cerr << "ERROR: Cannot parse Clip Initialization File "
           << clipInitFileName << "   Return Code: " << retVal << endl;
      iniFile->FileName = ""; // Don't write the ini file
      DeleteIniFile(iniFile);
      return 1;
      }

   iniFile->FileName = ""; // Don't write the ini file
   DeleteIniFile(iniFile);  // Don't need ini file any more

   // If we are creating a clip with images (not just audio) and the clip's
   // media will be stored in Image Files (SGI, DPX, CINEON, etc),
   // then we need to do some extra setup.
   if (clipInitInfo.doesNormalVideoExist())
      {
      CVideoInitInfo* mainVideoInitInfo = clipInitInfo.getMainVideoInitInfo();
      EMediaType mediaType = mainVideoInitInfo->getMediaType();

      // Image File media type?
      if (mediaType == MEDIA_TYPE_IMAGE_FILE_DPX
          || mediaType == MEDIA_TYPE_IMAGE_FILE_TIFF
          || mediaType == MEDIA_TYPE_IMAGE_FILE_EXR)
         {
         // Attempt to initialize the Clip and Main Video Init Info
         // from the header of the Image File
         string imageFileName = mainVideoInitInfo->getMediaIdentifier();
         retVal = clipInitInfo.initFromImageFile(imageFileName, 0, true);

         if (retVal == CLIP_ERROR_IMAGE_FILE_MISSING)
            {
            // First image file is not available, so check the others
            // If none exist, then assume the user wants the Image Files
            // to be created.  If some exist, then treat this as an error
            int frameCount = clipInitInfo.getOutTimecode()
                             - clipInitInfo.getInTimecode();

            // Verify that none of the files exist
            retVal = clipInitInfo.checkNoImageFiles(imageFileName, frameCount);
            if (retVal != 0)
               {
               cerr << "ERROR: Some Image Files are missing, some already exist"
                    << endl
                    << "       Cannot determine user's intention" << endl;
               return 1;
               }
            }
         else if (retVal != 0)
            {
            cerr << "ERROR: Cannot initialize from Image File "
                 << imageFileName << endl
                 << "       Return Code: " << retVal << endl;
            return 1;
            }
         }
      }

   // Initialize the media interface.  Among other things, this reads
   // in the raw disk configuration file rawdisk.cfg
   retVal = CMediaInterface::initialize();
   if(retVal != 0)
      {
      cerr << "ERROR: Could not initialize Media Interface.  Return Code: "
           << retVal << endl;
      return 1;
      }

   // Create the new clip
   CBinManager binMgr;
   auto clip = binMgr.createClip(clipInitInfo, &retVal);
   if (retVal != 0)
      {
      cerr << "ERROR: Create Clip failed.  Return Code: " << retVal;
      return 1;
      }

   binMgr.closeClip(clip);   // Cleanup

   // Close CMediaInterface
   CMediaInterface::close();

   if (verboseMode)
      {
      cout << "Create Clip for " << clipInitInfo.getClipName() << endl
           << " in Bin " << clipInitInfo.getBinPath() << " completed" << endl;
      }

   return 0;
}


