#include <stdio.h>
#include "machine.h"
#include "ClipAPI.h"

/////////////////////////////////////////////////////////////////////////////
//
// GetClipLocation.cpp - gets clip's location & length
//
//   Modification History:
//     6/17/02 - mpr - added looping thru all VideoProxies
//    ??/??/01 - mpr & jam - Creation
//
/////////////////////////////////////////////////////////////////////////////

main(int argc, char *argv[])
{
  CreateApplicationName(argv[0]);
  ClipSharedPtr InputClip;
  CBinManager bmBinMgr;
  string      sBinName, sClipNamePart;
  int         i;

  if (argc <= 1)
  {
    fprintf(stderr, "Usage: %s clip_name\n", argv[0]);
    exit(-1);
  }

  int iRet = bmBinMgr.splitClipFileName (argv[1], sBinName, sClipNamePart);
  if (iRet)
  {
    cout << "LoadClip: splitClipFileName returned: "
         << iRet << endl;
    return(-1);
  }

  InputClip = bmBinMgr.openClip(sBinName, sClipNamePart, &iRet);
  if (iRet != 0)
  {
    cout << "LoadClip: Unable to open the clip";
    return(-1);
  }

  int iNumProxies = InputClip->getVideoProxyCount();

  ////////////////////////////////////////////////
  // Print out info for each VideoProxy
  ////////////////////////////////////////////////

  for (i = 0; i < iNumProxies; i++)
  {
    CVideoFrameList *InputFrameList = InputClip->getVideoFrameList(
                                         i, FRAMING_SELECT_VIDEO);
    int InputFrameCount = InputFrameList->getTotalFrameCount();
    const CImageFormat *InputFormat = InputFrameList->getImageFormat();

    CVideoFrame *BeginFrame = InputFrameList->getFrame(0);
    int InputFieldsFrame = (InputFormat->getInterlaced()) ? 2 : 1;
    CField *BeginField = BeginFrame->getBaseField(0);

    const CMediaLocation & MediaLoc = BeginField->getMediaLocation();
    int ItemSize = MediaLoc.getDataSize() / 512;
    int location = MediaLoc.getDataIndex() / ((MTI_INT64) 512);

    printf("vp%d Location: %12d (blocks)\n", i, location);
    printf("vp%d ItemSize: %12d (blocks)\n", i, ItemSize);
    printf("vp%d Length  : %12d (blocks)\n", i, InputFrameCount * InputFieldsFrame
             * ItemSize);
  }

  return(0);
}
