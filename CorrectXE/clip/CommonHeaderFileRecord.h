// CommonHeaderFileRecord.h: interface for the CCommonHeaderFileRecord class.
//
/*
$Header: /usr/local/filmroot/clip/include/CommonHeaderFileRecord.h,v 1.1.1.1 2001/06/18 13:38:26 mertus Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef COMMONHEADERFILERECORDH
#define COMMONHEADERFILERECORDH

#include "FileRecord.h"

//////////////////////////////////////////////////////////////////////

#define FR_COMMON_HEADER_FILE_RECORD_LATEST_VERSION (FR_INITIAL_RECORD_VERSION+1)

//////////////////////////////////////////////////////////////////////

class CCommonHeaderFileRecord : public CFileRecord  
{
public:
   CCommonHeaderFileRecord(
                  MTI_UINT16 version = FR_COMMON_HEADER_FILE_RECORD_LATEST_VERSION);
   virtual ~CCommonHeaderFileRecord();

   virtual int read(CStructuredStream &stream);
   virtual int write(CStructuredStream &stream);

   // Accessor functions
   MTI_UINT16 getFileType() const;
   MTI_UINT16 getFileTypeVersion() const;

   void setFileType(MTI_UINT16 newFileType); 
   void setFileTypeVersion(MTI_UINT16 newFileTypeVersion);

   void calculateRecordByteCount();

private:
   MTI_UINT16 fileType;
   MTI_UINT16 fileTypeVersion;
};

//////////////////////////////////////////////////////////////////////

#endif // #ifndef COMMONHEADERFILERECORD_H
