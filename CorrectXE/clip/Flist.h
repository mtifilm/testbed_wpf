#ifndef FLISTH
#define FLISTH

#define TC_NONE 0x0

#define TC_PAIR0 0x4000
#define TC_PAIR1 0x8000

#define TC_HALF 0x20

#define TC_OFFSET0_USE_SRC 0x1
#define TC_OFFSET1_USE_SRC 0x2
#define TC_OFFSET2_USE_SRC 0x4

#define TC_PAIR_NEXT_1 0x100
#define TC_PAIR_NEXT_2 0x200

#define EXTRA_FRAMES 16

#define FALSE 0
#define TRUE  1
/////////////////////////////////////////////////////////////////////

struct Flist_Header
 {
  long
    laFirstElem[3],     /* 0 is raw video, 1 is auto 3/2 removed, 2 is manual */
    laNElem[3];         /* number of elements in this clip */
 };

struct Time_Code
{
  int
    iH,
    iM,
    iS,
    iF;
};

struct Flist_Elem
 {
  long
    laBlockOffset[3];

  Time_Code 
    tcTime;

  int
    iHalf;              /* see TC_* */
 };

/////////////////////////////////////////////////////////////////////

class CFlist
{

public:

   CFlist(char *flname);

   ~CFlist();

   int createFlist();

   void openTrack(int trkidx); 

   void openFrame(int h, int m, int s, int f);

   void addField(long blkoffs);

   int addFrame(int hlfcod);

   int closeTrack();

   int closeFlist();
 
private:

#define MAX_LENG 128

   char flistName[MAX_LENG];

   int fieldPhase;

   int flistFileDescriptor;

   Flist_Header *headerBuffer;

   static const int blfactr; // blfactr = 1024 (initialized in Flist.cpp)

   Flist_Elem *fieldBuffer;

   Flist_Elem *fieldBufferPtr;

#define FLIST_TRACK_VIDEO	0
#define FLIST_TRACK_FILM	1
   int curTrack;

   int curFrames; // count of frames in current buffer

   int curOffset; // offset within FLIST of beg of current buffer

};

#endif
