// StructuredStream.h: interface for the CStructuredStream class.
//
/*
$Header: /usr/local/filmroot/clip/include/StructuredStream.h,v 1.1.1.1 2001/06/18 13:38:26 mertus Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef STRUCTUREDSTREAMH
#define STRUCTUREDSTREAMH

#include "err_clip.h"
#include <string>
using std::string;

#include "machine.h"

//////////////////////////////////////////////////////////////////////

//#define INLINE_STRUCTURED_STREAM_IO

class CStructuredStream  
{
public:
   CStructuredStream();
   virtual ~CStructuredStream();

   // Pure Virtual Functions
   virtual int open(const string& fileName) = 0;
   virtual int createFile(const string& fileName) = 0;
   virtual int close() = 0;
   virtual int read(void*, unsigned int) = 0;
   virtual int write(const void*, unsigned int) = 0;
   //virtual int tell() = 0;
   //virtual int seek(unsigned long strpos) = 0;

   bool getEndianFixupRequired();
   void setEndianFixupRequired(bool newEndianFixupRequired);

#ifdef INLINE_STRUCTURED_STREAM_IO

//////////////////////////////////////////////////////////////////////
// Structured Read Functions
//////////////////////////////////////////////////////////////////////

inline int readInt8(MTI_INT8 *value)
{
	int status = read(value, sizeof(MTI_INT8));

   return((status == (int)sizeof(MTI_INT8)) ? 0 : CLIP_ERROR_FILE_READ_FAILED);
}

inline int readInt16(MTI_INT16 *value)
{
	int status = read(value, sizeof(MTI_INT16));

	if (endianFixupRequired)
		endianInt16Fixup(value);

	return((status == (int)sizeof(MTI_INT16)) ? 0 : CLIP_ERROR_FILE_READ_FAILED);
}

inline int readInt32(MTI_INT32 *value)
{
	int status = read(value, sizeof(MTI_INT32));

	if (endianFixupRequired)
		endianInt32Fixup(value);

	return((status == (int)sizeof(MTI_INT32)) ? 0 : CLIP_ERROR_FILE_READ_FAILED);
}

inline int readInt64(MTI_INT64 *value)
{
   int status = read(value, sizeof(MTI_INT64));

   if (endianFixupRequired)
		endianInt64Fixup(value);

   return((status == (int)sizeof(MTI_INT64)) ? 0 : CLIP_ERROR_FILE_READ_FAILED);
}

inline int readUInt8(MTI_UINT8 *value)
{
   int status = read(value, sizeof(MTI_UINT8));

   return((status == (int)sizeof(MTI_UINT8)) ? 0 : CLIP_ERROR_FILE_READ_FAILED);
}

inline int readUInt16(MTI_UINT16 *value)
{
   int status = read(value, sizeof(MTI_UINT16));

   if (endianFixupRequired)
		endianUInt16Fixup(value);

   return((status == (int)sizeof(MTI_UINT16)) ? 0 : CLIP_ERROR_FILE_READ_FAILED);
}

inline int readUInt32(MTI_UINT32 *value)
{
   int status = read(value, sizeof(MTI_UINT32));

   if (endianFixupRequired)
      endianUInt32Fixup(value);

	return((status == (int)sizeof(MTI_UINT32)) ? 0 : CLIP_ERROR_FILE_READ_FAILED);
}

inline int readUInt64(MTI_UINT64 *value)
{
   int status = read(value, sizeof(MTI_UINT64));

   if (endianFixupRequired)
      endianUInt64Fixup(value);

   return((status == (int)sizeof(MTI_UINT64)) ? 0 : CLIP_ERROR_FILE_READ_FAILED);
}

inline int readReal32(MTI_REAL32 *value)
{
   int status = read(value, sizeof(MTI_REAL32));

   if (endianFixupRequired)
		endianReal32Fixup(value);

   return((status == (int)sizeof(MTI_REAL32)) ? 0 : CLIP_ERROR_FILE_READ_FAILED);
}

inline int readReal64(MTI_REAL64 *value)
{
   int status = read(value, sizeof(MTI_REAL64));

   if (endianFixupRequired)
      endianReal64Fixup(value);

	return((status == (int)sizeof(MTI_REAL64)) ? 0 : CLIP_ERROR_FILE_READ_FAILED);
}

//////////////////////////////////////////////////////////////////////
// Structured Write Functions
//////////////////////////////////////////////////////////////////////

inline int writeInt8(MTI_INT8 value)
{
   int status = write(&value, sizeof(MTI_INT8));

   return((status == (int)sizeof(MTI_INT8)) ? 0 : CLIP_ERROR_FILE_WRITE_FAILED);
}

inline int writeInt16(MTI_INT16 value)
{
   if (endianFixupRequired)
      endianInt16Fixup(&value);

   int status = write(&value, sizeof(MTI_INT16));

   return((status == (int)sizeof(MTI_INT16)) ? 0 : CLIP_ERROR_FILE_WRITE_FAILED);
}

inline int writeInt32(MTI_INT32 value)
{
	if (endianFixupRequired)
      endianInt32Fixup(&value);

   int status = write(&value, sizeof(MTI_INT32));

   return((status == (int)sizeof(MTI_INT32)) ? 0 : CLIP_ERROR_FILE_WRITE_FAILED);
}

inline int writeInt64(MTI_INT64 value)
{
	if (endianFixupRequired)
      endianInt64Fixup(&value);

   int status = write(&value, sizeof(MTI_INT64));

   return((status == (int)sizeof(MTI_INT64)) ? 0 : CLIP_ERROR_FILE_WRITE_FAILED);
}

inline int writeUInt8(MTI_UINT8 value)
{
   int status = write(&value, sizeof(MTI_UINT8));

	return((status == (int)sizeof(MTI_UINT8)) ? 0 : CLIP_ERROR_FILE_WRITE_FAILED);
}

inline int writeUInt16(MTI_UINT16 value)
{
   if (endianFixupRequired)
		endianUInt16Fixup(&value);

   int status = write(&value, sizeof(MTI_UINT16));

   return((status == (int)sizeof(MTI_UINT16)) ? 0 : CLIP_ERROR_FILE_WRITE_FAILED);
}

inline int writeUInt32(MTI_UINT32 value)
{
   if (endianFixupRequired)
      endianUInt32Fixup(&value);

	int status = write(&value, sizeof(MTI_UINT32));

   return((status == (int)sizeof(MTI_UINT32)) ? 0 : CLIP_ERROR_FILE_WRITE_FAILED);
}

inline int writeUInt64(MTI_UINT64 value)
{
   if (endianFixupRequired)
      endianUInt64Fixup(&value);

   int status = write(&value, sizeof(MTI_UINT64));

	return((status == (int)sizeof(MTI_UINT64)) ? 0 : CLIP_ERROR_FILE_WRITE_FAILED);
}

inline int writeReal32(MTI_REAL32 value)
{
   if (endianFixupRequired)
		endianReal32Fixup(&value);

   int status = write(&value, sizeof(MTI_REAL32));

   return((status == (int)sizeof(MTI_REAL32)) ? 0 : CLIP_ERROR_FILE_WRITE_FAILED);
}

inline int writeReal64(MTI_REAL64 value)
{
   if (endianFixupRequired)
      endianReal64Fixup(&value);

	int status = write(&value, sizeof(MTI_REAL64));

   return((status == (int)sizeof(MTI_REAL64)) ? 0 : CLIP_ERROR_FILE_WRITE_FAILED);
}

#else

   int readInt8(MTI_INT8* value);
   int readUInt8(MTI_UINT8* value);
   int readInt16(MTI_INT16* value);
   int readUInt16(MTI_UINT16* value);
   int readInt32(MTI_INT32* value);
   int readUInt32(MTI_UINT32* value);
   int readInt64(MTI_INT64* value);
   int readUInt64(MTI_UINT64* value);
   int readReal32(MTI_REAL32* value);
   int readReal64(MTI_REAL64* value);

   int writeInt8(MTI_INT8 value);
   int writeUInt8(MTI_UINT8 value);
   int writeInt16(MTI_INT16 value);
   int writeUInt16(MTI_UINT16 value);
   int writeInt32(MTI_INT32 value);
   int writeUInt32(MTI_UINT32 value);
   int writeInt64(MTI_INT64 value);
   int writeUInt64(MTI_UINT64 value);
   int writeReal32(MTI_REAL32 value);
	int writeReal64(MTI_REAL64 value);

#endif

private:
   bool endianFixupRequired;

protected:
   void reverseBytes(MTI_UINT8 *valueArray, int byteCount);
   void reverse2Bytes(MTI_UINT8 *valueArray);
   void reverse4Bytes(MTI_UINT8 *valueArray);
   void reverse8Bytes(MTI_UINT8 *valueArray);

   inline void endianInt16Fixup(MTI_INT16* value)
      {
      reverse2Bytes((MTI_UINT8*)value);
      }

   inline void endianInt32Fixup(MTI_INT32* value)
      {
      reverse4Bytes((MTI_UINT8*)value);
      }

   inline void endianInt64Fixup(MTI_INT64* value)
      {
      reverse8Bytes((MTI_UINT8*)value);
      }

   inline void endianUInt16Fixup(MTI_UINT16* value)
      {
      reverse2Bytes((MTI_UINT8*)value);
      }

   inline void endianUInt32Fixup(MTI_UINT32* value)
      {
      reverse4Bytes((MTI_UINT8*)value);
      }

   inline void endianUInt64Fixup(MTI_UINT64* value)
      {
      reverse8Bytes((MTI_UINT8*)value);
      }

   inline void endianReal32Fixup(MTI_REAL32* value)
      {
      reverse4Bytes((MTI_UINT8*)value);
      }

   inline void endianReal64Fixup(MTI_REAL64* value)
      {
      reverse8Bytes((MTI_UINT8*)value);
      }
};

//////////////////////////////////////////////////////////////////////

#endif // #ifndef STRUCTUREDSTREAM_H

