// RawDiskFreeList.cpp: implementation of the CRawDiskFreeList class.
//
//////////////////////////////////////////////////////////////////////

//#define _RAW_DISK_FREE_LIST_DATA_CPP // #ifdef PRIVATE_STATIC

#include "err_clip.h"
#include "RawDiskFreeList.h"
#include "RawMediaAccess.h"
#include "IniFile.h"
#include "MediaDeallocList.h"
#include "SafeClasses.h"

#include <algorithm>
#include <stdio.h>

// Compiler-dependent format string for free list file entry
#if defined(_MSC_VER) || defined(__BORLANDC__)
const string CRawDiskFreeList::
                   freeEntryFormatString("Location=%I64d NumBlock=%I64d\n");
#else
// SGI's IRIX compiler, and GCC, both use this format; hopefully it's
// standard; if not, this #if needs to be more complicated
const string CRawDiskFreeList::
                   freeEntryFormatString("Location=%lld NumBlock=%lld\n");
#endif

//////////////////////////////////////////////////////////////////////
// The following variables used to be static class members.
// I switched the to namespaced globals for compatability with
// visual c++.  -Aaron Geman  7/9/04
//////////////////////////////////////////////////////////////////////

namespace RawDiskFreeListData {
   // Compiler-dependent format string for free list file entry
#if defined(_MSC_VER) || defined(__BORLANDC__)
   const string freeEntryFormatString("Location=%I64d NumBlock=%I64d\n");
#else
// SGI's IRIX compiler, and GCC, both use this format; hopefully it's
// standard; if not, this #if needs to be more complicated
   const string freeEntryFormatString("Location=%lld NumBlock=%lld\n");
#endif
};
using namespace RawDiskFreeListData;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRawDiskFreeList::CRawDiskFreeList(const string& newFileName)
: fileName(newFileName)
{
  posSearch = freeList.end();
}

CRawDiskFreeList::~CRawDiskFreeList()
{
   clearFreeList();
}


//////////////////////////////////////////////////////////////////////

int CRawDiskFreeList::queryFreeSpace(MTI_INT64 *totalBlocks, 
                                     MTI_INT64 *maxContiguousBlocks)
{
   int retVal;

   // Check arguments for null pointers
   if (totalBlocks == 0 || maxContiguousBlocks == 0)
      return CLIP_ERROR_UNEXPECTED_NULL_ARGUMENT;

   // Read in the free list from the raw disk free list file
   retVal = readFreeListFile();
   if (retVal != 0)
      return retVal;

   // Iterate over all entries in the raw disk free list
   // and sum size of entries and find largest entry
   MTI_INT64 totalSize = 0;
   MTI_INT64 maxEntrySize = 0;
   MTI_INT64 entrySize;
   RawDiskFreeListIterator freeListIterator;
   for (freeListIterator = freeList.begin(); 
        freeListIterator != freeList.end(); 
        ++freeListIterator)
      {
      entrySize = (*freeListIterator)->size;
      totalSize += entrySize;
      if (entrySize > maxEntrySize)
         maxEntrySize = entrySize;
      }

   *totalBlocks = totalSize;
   *maxContiguousBlocks = maxEntrySize;

   // Success
   return 0;

} // queryFreeSpace

int CRawDiskFreeList::queryFreeSpaceEx(MTI_UINT32 paddedFrameSize,
                                       ERawDiskAllocMethod allocMethod,
                                       int minFrameCount,
                                       MTI_INT64 *totalBlocks,
                                       MTI_INT64 *allocatableBlocks)
{
   int retVal;

   if (allocMethod == RF_ALLOC_CONTIGUOUS_FIRST_AVAILABLE
       || allocMethod == RF_ALLOC_CONTIGUOUS_SMALLEST_AVAILABLE)
      return queryFreeSpace(totalBlocks, allocatableBlocks);

   // Check arguments for null pointers
   if (totalBlocks == 0 || allocatableBlocks == 0)
      return CLIP_ERROR_UNEXPECTED_NULL_ARGUMENT;

   // Read in the free list from the raw disk free list file
   retVal = readFreeListFile();
   if (retVal != 0)
      return retVal;

   // Iterate over all entries in the raw disk free list
   // and sum size of entries and find largest entry
   MTI_UINT32 paddedFrameBlockSize = paddedFrameSize / RF_BLOCK_SIZE;
   MTI_INT64 minBlockSize = (MTI_INT64)paddedFrameBlockSize * minFrameCount;
   MTI_INT64 totalSize = 0;
   MTI_INT64 totalAllocatableBlocks = 0;
   MTI_INT64 maxEntrySize = 0;
   RawDiskFreeListIterator freeListIterator;
   for (freeListIterator = freeList.begin(); 
        freeListIterator != freeList.end(); 
        ++freeListIterator)
      {
      MTI_INT64 entrySize = (*freeListIterator)->size;
      totalSize += entrySize;
      if (entrySize >= minBlockSize)
         {
         MTI_INT64 allocatableFrames = entrySize / paddedFrameBlockSize;
         totalAllocatableBlocks += allocatableFrames * paddedFrameBlockSize;
         }
      if (entrySize > maxEntrySize)
         maxEntrySize = entrySize;
      }

   *totalBlocks = totalSize;
   if (maxEntrySize > totalAllocatableBlocks)
      *allocatableBlocks = maxEntrySize;
   else
      *allocatableBlocks = totalAllocatableBlocks;

   // Success
   return 0;

} // queryFreeSpaceEx
//////////////////////////////////////////////////////////////////////

void CRawDiskFreeList::compressList()
{
   RawDiskFreeList tmpList;
   CRawDiskFreeEntry *working;
   RawDiskFreeListIterator iterator, begin, end;

   if (freeList.size() > 1)
      {
      begin = freeList.begin();
      end = freeList.end();
      working = *begin;
      *begin = 0;          // This will be moved to tmpList
      for (iterator = begin + 1; iterator != end; ++iterator)
         {
         if (working->offset + working->size == (*iterator)->offset)
            {
            working->size += (*iterator)->size;
            // Delete free list entry that was just merged with
            // with the "working" free list entry
            delete *iterator;
            }
         else
            {
            // working entry and *iterator entry are not contiguous,
            // so put working in tmpList and set working to be *iterator.
            tmpList.push_back(working);
            working = *iterator;
            *iterator = 0; // This will be moved to tmpList
            }
         }
      tmpList.push_back(working);

      // Contents of freeList are now swapped with tmpList.
      // When tmpList goes out of scope, it will be deleted

      freeList.swap(tmpList);
      }
}

#ifndef _MSC_VER
static bool cmpEntry(CRawDiskFreeEntry* e1, CRawDiskFreeEntry* e2)
{
   return (e1->offset < e2->offset);
}

#else
// specialize greater<CRawDiskFreeEntry *>
bool greater<CRawDiskFreeEntry *>::operator()(CRawDiskFreeEntry* const & x,
                                              CRawDiskFreeEntry* const & y) const
{
   return (x->offset < y->offset);
}
#define cmpEntry greater<CRawDiskFreeEntry *>()
#endif

static bool overlap(CRawDiskFreeEntry* e1, CRawDiskFreeEntry* e2)
{
   MTI_INT64 e1Start = e1->offset;
   MTI_INT64 e1End = e1->offset + e1->size;
   MTI_INT64 e2Start = e2->offset;
   MTI_INT64 e2End = e2->offset + e2->size;

   return (std::max(e1Start, e2Start) < std::min(e1End, e2End));
}

int CRawDiskFreeList::newMergeList(list<CRawDiskFreeEntry*> &newList)
{
   // Append current entries in the free list onto the new list
   newList.insert(newList.end(), freeList.begin(), freeList.end());

   // Sort all of the entries in the new list by the offset
   newList.sort(cmpEntry);

   if (newList.empty())
      return 0;

   // Iterate over the merged list, looking for entries that are
   // contiguous and thus can be joined
   list<CRawDiskFreeEntry*>::iterator previousIter, iter, nextIter;
   previousIter = newList.begin();
   iter = previousIter;
   ++iter;
   while (iter != newList.end())
      {
      if (overlap(*previousIter, *iter))
         {
         // ERROR: entries overlap, should never happen
         TRACE_0(errout << "ERROR: While deallocating media back to raw disk, "
                        << "RFL file " << fileName << endl
                        << "       entry overlaps deallocated media location"
                        << endl
                        << "       Offset=" << (*previousIter)->offset << " Size=" << (*previousIter)->size << endl
                        << "       Offset=" << (*iter)->offset << " Size=" << (*iter)->size);
         return CLIP_ERROR_RFL_ENTRIES_OVERLAPPING;
         }
      else
         {
         nextIter = iter;
         nextIter++;
         if ((*previousIter)->offset+(*previousIter)->size == (*iter)->offset)
            {
            // entries are touching, so combine them into one entry
            (*previousIter)->size += (*iter)->size;
            delete *iter;
            newList.erase(iter); // remove merged entry from list
            }
         else
            {
            // entries are not touching, so move on
            previousIter = iter;
            }
         iter = nextIter;
         }
      }

   return 0;
}

void CRawDiskFreeList::mergeList(RawDiskFreeList &newList)
{
   RawDiskFreeList tmpList;
   RawDiskFreeListIterator newListBegin = newList.begin();
   RawDiskFreeListIterator newListEnd = newList.end();
   RawDiskFreeListIterator newListIterator = newListBegin;
   RawDiskFreeListIterator freeListBegin = freeList.begin();
   RawDiskFreeListIterator freeListEnd = freeList.end();
   RawDiskFreeListIterator freeListIterator = freeListBegin;

   while (newListIterator != newListEnd && freeListIterator != freeListEnd)
      {
      if ((*newListIterator)->offset < (*freeListIterator)->offset)
         {
         tmpList.push_back(*newListIterator);
         ++newListIterator;
         }
      else
         {
         tmpList.push_back(*freeListIterator);
         ++freeListIterator;
         }
      }

   // Copy the elements from the list (new or free) which has
   // something remaining 
   if (newListIterator != newListEnd)
      {
      for (; newListIterator != newListEnd; ++newListIterator)
         {
         tmpList.push_back(*newListIterator);
         }
      }
   else if (freeListIterator != freeListEnd)
      {
      for (; freeListIterator != freeListEnd; ++freeListIterator)
         {
         tmpList.push_back(*freeListIterator);
         }
      }

   // Contents of freeList are now swapped with tmpList.
   // When tmpList goes out of scope, it will be deleted
   freeList.swap(tmpList);

   compressList();
}

////////////////////////////////////////////////////////////////////////////

//--------------------------------------------------------------------------
//
// Function:   CheckRFL
//
// Description:Checks the free list file entries
//
// Arguments:  None
//
// Returns:    Returns 0 if RFL entries are okay
//             Returns non-zero if RFL is corrupted or cannot be read
//               CLIP_ERROR_RFL_ENTRIES_OUT_OF_ORDER
//                  One or more entries are out-of-order.  RFL entries
//                  should always be in ascending order of the Location
//               CLIP_ERROR_RFL_ENTRIES_OVERLAPPING
//                  One or more entries in RFL file describe disk blocks
//                  that are overlapping
//               Other error codes returned by readFreeList() function
//
//--------------------------------------------------------------------------
int CRawDiskFreeList::CheckRFL()
{
   int retVal;

   // Read in the free list from the raw disk free list file
   retVal = readFreeListFile();
   if (retVal != 0)
      return retVal;

   if (freeList.size() <= 1)
      return 0;   // less than 2 entries, so nothing to check

   list<CRawDiskFreeEntry*> newList;

   // Append current entries in the free list onto the new list
   newList.insert(newList.end(), freeList.begin(), freeList.end());

   // Iterate over the list, checking if entries are out-of-order
   list<CRawDiskFreeEntry*>::iterator previousIter, iter;
   previousIter = newList.begin();
   iter = previousIter;
   ++iter;
   while (iter != newList.end())
      {
      if ((*previousIter)->offset > (*iter)->offset)
         {
         // ERROR: entries are out of order
         TRACE_0(errout << "ERROR: Entries in RFL file " << fileName << endl
                        << "       are out-of-order" << endl
                        << "       Offset=" << (*previousIter)->offset
                        << "       Offset=" << (*iter)->offset);
         return CLIP_ERROR_RFL_ENTRIES_OUT_OF_ORDER;
         }
      previousIter = iter;
      ++iter;
      }

   // Sort all of the entries in the new list by the offset
   newList.sort(cmpEntry);

   // Iterate over the sorted list, looking for entries that are
   // overlapping
   previousIter = newList.begin();
   iter = previousIter;
   ++iter;
   while (iter != newList.end())
      {
      if (overlap(*previousIter, *iter))
         {
         // ERROR: entries overlap, should never happen
         TRACE_0(errout << "ERROR: Entries in RFL file " << fileName 
                        << " overlap" << endl
                        << "       Offset=" << (*previousIter)->offset
                        << " Size=" << (*previousIter)->size << endl
                        << "       Offset=" << (*iter)->offset << " Size="
                        << (*iter)->size);
         return CLIP_ERROR_RFL_ENTRIES_OVERLAPPING;
         }
      previousIter = iter;
      ++iter;
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////

MTI_INT64 CRawDiskFreeList::allocate(MTI_INT64 size)
{
   int retVal;

   RawDiskFreeListIterator iterator;

   // Read in the free list from the raw disk free list file
   retVal = readFreeListFile();
   if (retVal != 0)
      return -1;

   // Convert size from bytes to number of 512-byte blocks
   MTI_INT64 blockSize = (size + RF_BLOCK_SIZE - 1) / RF_BLOCK_SIZE;

   // Search for first free list entry that has enough space
   for (iterator = freeList.begin(); iterator != freeList.end(); ++iterator)
      {
      CRawDiskFreeEntry* freeListEntry = *iterator;
      if (freeListEntry->size >= blockSize)
         {
         // Offset to allocated space
         MTI_INT64 offset = freeListEntry->offset * RF_BLOCK_SIZE;

         // Adjust this free list entry for space allocated
         freeListEntry->offset += blockSize;
         freeListEntry->size -= blockSize;

         // Remove the free list entry if its size is now zero
         if (freeListEntry->size <= 0)
            {
            delete freeListEntry;
            freeList.erase(iterator);
            }

         // Update the raw disk free list file
         if (writeFreeListFile() != 0)
            return -1;

         return(offset);
         }
      }

   // If this point is reached, none of the free list entries was big
   // enough to allocate the requested space.  Return error indicator
   TRACE_0(errout << "ERROR: Could not allocate raw disk space, insufficient free space");
   return -1 ;
}

//////////////////////////////////////////////////////////////////////
#ifdef NEW_DISK_ALLOCATION
// Start of implementation of non-contiguous allocation from raw disk
//
//
int CRawDiskFreeList::allocate(int fieldCount, int fieldSize, bool interlaced,
                               ERawDiskAllocMethod allocMethod,
                               int minFieldCount, CRawDiskBlockList &allocList)
{
   int retVal;

   // Read in the free list from the raw disk free list file
   retVal = readFreeListFile();
   if (retVal != 0)
      return retVal;

   int remainingFieldCount = fieldCount;
   while (remainingFieldCount > 0)
      {
      // Find a block of raw disk space in the free list
      retVal = findBlock(remainingFieldCount, fieldSize, interlaced, 
                         allocMethod, minFieldCount, allocList);
      if (retVal != 0)
         return retVal;

      const SRawDiskBlock *lastBlock = allocList.getLastEntry();
      remainingFieldCount -= (int)(lastBlock->size / fieldSize);
      }

   // If the allocation list has more than one entry, sort it by disk
   // offset. This may give some read/write media performance advantage
   // to have the disk offset monotonically increasing with frame index
   if (allocList.getEntryCount() > 0)
      allocList.sortByOffset();

   // Update the raw disk free list file
   retVal = writeFreeListFile();
   if (retVal != 0)
      return retVal;

   return 0;
}

int CRawDiskFreeList::findBlock(int fieldCount, int fieldSize, bool interlaced,
                                ERawDiskAllocMethod allocMethod,
                                int minFieldCount, CRawDiskBlockList &allocList)
{
   int retVal;

   if (allocMethod == RF_ALLOC_CONTIGUOUS_FIRST_AVAILABLE)
      {
      // Allocate from the first available free raw disk area that is
      // large enough.
      retVal = findFirstBlock(fieldCount, fieldSize, allocList);
      }
   else if (allocMethod == RF_ALLOC_CONTIGUOUS_SMALLEST_AVAILABLE)
      {
      // Allocate from the smallest available free raw disk area that is
      // large enough.  This may result in maintaining the largest free area
      // and reduced fragmentation.
      retVal = findSmallestAvailable(fieldCount, fieldSize, allocList);
      }
   else if (allocMethod == RF_ALLOC_NON_CONTIGUOUS)
      {
      // First try to allocate a contiguous block.  If none of the free
      // areas are big enough for a single allocation, then allow for
      // partial allocation of minimum size
      retVal = findSmallestAvailable(fieldCount, fieldSize, allocList);
      if (retVal != 0)
         {
         retVal = findLargestAvailable(fieldSize, interlaced, minFieldCount,
                                       allocList);
         }
      }
   else
      {
      return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR;
      }

   if (retVal != 0)
      return retVal;

   return 0;
}

int CRawDiskFreeList::findFirstBlock(int fieldCount, int fieldSize,
                                     CRawDiskBlockList &allocList)
{
   // Convert size from bytes to number of 512-byte blocks
   MTI_INT64 size = (MTI_INT64)fieldCount * fieldSize;
   MTI_INT64 blockSize = (size + RF_BLOCK_SIZE - 1) / RF_BLOCK_SIZE;

   // Search for first free list entry that has enough space
   RawDiskFreeListIterator iterator;
   for (iterator = freeList.begin(); iterator != freeList.end(); ++iterator)
      {
      CRawDiskFreeEntry* freeListEntry = *iterator;
      if (freeListEntry->size >= blockSize)
         {
         // Found a free block that is big enough to hold the requested fields
         
         // Add the allocated block to caller's allocList
         allocList.addBlock(freeListEntry->offset * RF_BLOCK_SIZE,
                            blockSize * RF_BLOCK_SIZE);

         // Adjust this free list entry for space allocated
         freeListEntry->offset += blockSize;
         freeListEntry->size -= blockSize;

         // Remove the free list entry if its size is now zero
         if (freeListEntry->size <= 0)
            {
            delete freeListEntry;
            freeList.erase(iterator);
            }

         return 0;   // Done
         }
      }

   return CLIP_ERROR_INSUFFICIENT_MEDIA_STORAGE;
}

int CRawDiskFreeList::findSmallestAvailable(int fieldCount, int fieldSize,
                                            CRawDiskBlockList &allocList)
{
   // Convert size from bytes to number of 512-byte blocks
   MTI_INT64 size = (MTI_INT64)fieldCount * fieldSize;
   MTI_INT64 blockSize = (size + RF_BLOCK_SIZE - 1) / RF_BLOCK_SIZE;

   // Search for smallest free list entry that has enough space
   RawDiskFreeListIterator iterator, smallestFreeEntryIterator;
   CRawDiskFreeEntry *smallestFreeEntry = 0;
   for (iterator = freeList.begin(); iterator != freeList.end(); ++iterator)
      {
      CRawDiskFreeEntry* freeListEntry = *iterator;
      if (freeListEntry->size >= blockSize)
         {
         // Found a free block that is big enough to hold the requested fields
         if (smallestFreeEntry == 0
             || freeListEntry->size < smallestFreeEntry->size)
            {
            smallestFreeEntryIterator = iterator;
            smallestFreeEntry = freeListEntry;
            }
         }
      }

   if (smallestFreeEntry == 0)
      return CLIP_ERROR_INSUFFICIENT_MEDIA_STORAGE;

   // Add the allocated block to caller's allocList
   allocList.addBlock(smallestFreeEntry->offset * RF_BLOCK_SIZE,
                      blockSize * RF_BLOCK_SIZE);

   // Adjust this free list entry for space allocated
   smallestFreeEntry->offset += blockSize;
   smallestFreeEntry->size -= blockSize;

   // Remove the free list entry if its size is now zero
   if (smallestFreeEntry->size <= 0)
      {
      delete smallestFreeEntry;
      freeList.erase(smallestFreeEntryIterator);
      }

   return 0;   // Done
}

int CRawDiskFreeList::findLargestAvailable(int fieldSize, bool interlaced,
                                           int minFieldCount,
                                           CRawDiskBlockList &allocList)
{
   // Convert size from bytes to number of 512-byte blocks
   MTI_INT64 minSize = (MTI_INT64)minFieldCount * fieldSize;
   MTI_INT64 minBlockSize = (minSize + RF_BLOCK_SIZE - 1) / RF_BLOCK_SIZE;

   // Search for largest free list entry
   RawDiskFreeListIterator iterator, largestFreeEntryIterator;
   CRawDiskFreeEntry *largestFreeEntry = 0;
   for (iterator = freeList.begin(); iterator != freeList.end(); ++iterator)
      {
      CRawDiskFreeEntry* freeListEntry = *iterator;
      if (freeListEntry->size >= minBlockSize)
         {
         if (largestFreeEntry == 0
             || freeListEntry->size > largestFreeEntry->size)
            {
            largestFreeEntryIterator = iterator;
            largestFreeEntry = freeListEntry;
            }
         }
      }

   if (largestFreeEntry == 0)
      return CLIP_ERROR_INSUFFICIENT_MEDIA_STORAGE;

   // Allocate an integral number of frames from the largest available entry
   MTI_INT64 frameSize = fieldSize;
   if (interlaced)
      frameSize *= 2;
   MTI_INT64 blocksPerFrame = (frameSize + RF_BLOCK_SIZE - 1) / RF_BLOCK_SIZE;
   MTI_INT64 frameCount = largestFreeEntry->size / blocksPerFrame;
   MTI_INT64 blockCount = frameCount * blocksPerFrame;

   // Add the allocated block to caller's allocList
   allocList.addBlock(largestFreeEntry->offset * RF_BLOCK_SIZE,
                      blockCount * RF_BLOCK_SIZE);

   // Adjust this free list entry for space allocated
   largestFreeEntry->offset += blockCount;
   largestFreeEntry->size -= blockCount;

   // Remove the free list entry if its size is now zero
   if (largestFreeEntry->size <= 0)
      {
      delete largestFreeEntry;
      freeList.erase(largestFreeEntryIterator);
      }

   return 0;   // Done
}
#endif // #ifdef NEW_DISK_ALLOCATION

//////////////////////////////////////////////////////////////////////

int CRawDiskFreeList::deallocate(MTI_INT64 location, MTI_INT64 size)
{
   int retVal;

   // Read in the free list from the raw disk free list file
   retVal = readFreeListFile();
   if (retVal != 0)
      return retVal;

   // Create a temporary free list from the caller's block to
   // be deallocated
   RawDiskFreeList newList;
   CRawDiskFreeEntry* newEntry = new CRawDiskFreeEntry(location, size);
   newList.push_back(newEntry);

   // Merge temporary free list with the current free list
   mergeList(newList);

   // Write the updated free block list
   retVal = writeFreeListFile();
   if (retVal != 0)
      return retVal;

   return 0;
}

int CRawDiskFreeList::deallocate(CMediaDeallocList &mediaDeallocList)
{
   CMediaDeallocList::DeallocList& deallocList
                                       = mediaDeallocList.getDeallocList();

   if (!deallocList.empty())
      {
      int retVal;

      // Read in the free list from the raw disk free list file
      retVal = readFreeListFile();
      if (retVal != 0)
         return retVal;

      // Create a temporary free list from the caller's list of blocks to
      // be deallocated
      RawDiskFreeList newList;
      CMediaDeallocList::DeallocListIterator deallocListIterator;
      for (deallocListIterator = deallocList.begin();
           deallocListIterator != deallocList.end();
           ++deallocListIterator)
         {
         MTI_INT64 location = deallocListIterator->dataIndex / RF_BLOCK_SIZE;
         MTI_INT64 size = deallocListIterator->dataSize / RF_BLOCK_SIZE;
         CRawDiskFreeEntry* newEntry = new CRawDiskFreeEntry(location, size);
         newList.push_back(newEntry);
         }

      // Merge temporary free list with the current free list
      mergeList(newList);

      // Write the updated free block list
      retVal = writeFreeListFile();
      if (retVal != 0)
         return retVal;
      }

   return 0;
}

int CRawDiskFreeList::deallocate(CRawDiskBlockList &allocList)
{
   int retVal;

   int entryCount = allocList.getEntryCount();
   if (entryCount < 1)
      return 0;   // nothing to do

   // Read in the free list from the raw disk free list file
   retVal = readFreeListFile();
   if (retVal != 0)
      return retVal;

   // Create a temporary free list from the caller's list of blocks to
   // be deallocated
   RawDiskFreeList newList;
   for (int i = 0; i < entryCount; ++i)
      {
      SRawDiskBlock *block = allocList.getEntry(i);
      MTI_INT64 location = block->offset / RF_BLOCK_SIZE;
      MTI_INT64 size = block->size / RF_BLOCK_SIZE;
      CRawDiskFreeEntry* newEntry = new CRawDiskFreeEntry(location, size);
      newList.push_back(newEntry);
      }

   // Merge temporary free list with the current free list
   mergeList(newList);

   // Write the updated free block list
   retVal = writeFreeListFile();
   if (retVal != 0)
      return retVal;

   return 0;
}

//////////////////////////////////////////////////////////////////////

void CRawDiskFreeList::clearFreeList()
{
   RawDiskFreeListIterator iterator;

   // delete each entry in free list
   for (iterator = freeList.begin(); iterator != freeList.end(); ++iterator)
      {
      delete *iterator;
      }
   
   // Empty free list
   freeList.clear();

   posSearch = freeList.end();
}


//////////////////////////////////////////////////////////////////////

int CRawDiskFreeList::readFreeListFile()
{
   FILE *freeListFile;

   if ((freeListFile = fopen(fileName.c_str(), "r")) == 0)
      {
      TRACE_0(errout << " ERROR: Cannot open raw disk free list file:"
                     << "         " << fileName << endl
                     << "        because: " << endl
                     << strerror(errno));
      return CLIP_ERROR_CANNOT_OPEN_RFL_FILE;
      }

   MTI_INT64 location;
   MTI_INT64 size;
   int entryNumber = 0;
   while(!feof(freeListFile))
      {
      if (fscanf(freeListFile, freeEntryFormatString.c_str(), &location, &size) == 2)
         {
         // Construct new free list entry from offset and size read from file
         // then put the new entry in the free list
         CRawDiskFreeEntry* freeEntry = new CRawDiskFreeEntry(location, size);
         freeList.push_back(freeEntry);
         ++entryNumber;
         }
      else
         {
         TRACE_0(errout << " ERROR: Cannot parse raw disk free list file:"
                        << "         " << fileName << endl
                        << "        entry # " << entryNumber);
         return CLIP_ERROR_CANNOT_PARSE_RFL_FILE;
         }
      }

   fclose(freeListFile);

   if (entryNumber < 1)
      return CLIP_ERROR_CANNOT_PARSE_RFL_FILE;  // File has no entries
   else
      return 0;
}

// This simple routine renames a file back and reports any errors
//
   bool InternalRFLRename(const string &fromName, const string &toName)
{
   if ((fromName == "") || (toName == "")) return true;
   if (rename(fromName.c_str(), toName.c_str()) == 0) return true;
   TRACE_0(errout << " ERROR: Cannot rename temp disk free list to RFL " << endl
                  << "        because: "  << strerror(errno) << endl
                  << "        RFL " << toName << " is now called " << fromName);

   return false;
}

int CRawDiskFreeList::writeFreeListFile()
{
   FILE *freeListFile;

   string NewFileName = MTIUniqueFileName(fileName);
   if (NewFileName == "")
     {
     TRACE_0(errout << " ERROR: Cannot create a unique file name from " << fileName);
     return CLIP_ERROR_CANNOT_OPEN_RFL_FILE;
     }

   if (rename(fileName.c_str(), NewFileName.c_str()) != 0)
      {
      TRACE_0(errout << " ERROR: Cannot rename raw disk free list to temp name " << endl
                     << "        rfl: " << fileName << ", New name: " << NewFileName << endl
                     << "        because: " << endl
                     << strerror(errno));
      return CLIP_ERROR_CANNOT_OPEN_RFL_FILE;
      }

   if ((freeListFile = fopen(fileName.c_str(), "w")) == 0)
      {
      TRACE_0(errout << " ERROR: Cannot open raw disk free list file: "
                     << "         " << fileName << endl
                     << "        because: " << endl
                     << strerror(errno));

	  InternalRFLRename(NewFileName, fileName);
      return CLIP_ERROR_CANNOT_OPEN_RFL_FILE;
      }

   RawDiskFreeListIterator iterator;
   for (iterator = freeList.begin(); iterator != freeList.end(); ++iterator)
      {
      if (fprintf(freeListFile, freeEntryFormatString.c_str(),
          (*iterator)->offset, (*iterator)->size) < 0)
         {
         TRACE_0(errout << " ERROR: Cannot write to raw disk free list file: "
                        << "         " << fileName << endl
                        << "        because: " << endl
			<< strerror(errno) << endl
			<< "Restoring old RFL file");

         fclose(freeListFile);
         InternalRFLRename(NewFileName, fileName);
         return CLIP_ERROR_CANNOT_WRITE_RFL_FILE;
         }
      }

   if (freeList.size() == 0)
      {
      // No entries in free list, stick in a dummy entry "Location=0 NumBlock=0"
      if (fprintf(freeListFile, freeEntryFormatString.c_str(), (MTI_INT64)0, (MTI_INT64)0) < 0)
         {
         TRACE_0(errout << " ERROR: Cannot write to raw disk free list file: "
                        << "         " << fileName << endl
                        << "        because: " << endl
  			<< strerror(errno) << endl
			<< "Restoring old RFL file");
         (void)fclose (freeListFile);
         InternalRFLRename(NewFileName, fileName);
         return CLIP_ERROR_CANNOT_WRITE_RFL_FILE;
         }
      }

   // Check on the close
   if (fclose(freeListFile) < 0)
     {
         TRACE_0(errout << " ERROR: Cannot write to raw disk free list file: "
                        << "         " << fileName << endl
                        << "        because closed failed: " << endl
                        << strerror(errno) << endl
			<< "Restoring old RFL file");
		 InternalRFLRename(NewFileName, fileName);
         return CLIP_ERROR_CANNOT_WRITE_RFL_FILE;
     }

   // Remove the RFL file
   if (NewFileName != "")
     if (DoesFileExist(NewFileName))
       if (remove(NewFileName.c_str()) < 0)
         TRACE_0(errout << "Inifile OK, but backup could not be deleted " << endl
                        << "File: " << NewFileName
                        << "Because: " << strerror(errno));

   return 0;
}

int CRawDiskFreeList::resetFreeList(MTI_INT64 startingOffset, MTI_INT64 rawSize)
{
   clearFreeList();

   CRawDiskFreeEntry* freeEntry
                      = new CRawDiskFreeEntry(startingOffset, rawSize);
   freeList.push_back(freeEntry);

   return (writeFreeListFile());
}

//////////////////////////////////////////////////////////////////////
// Beware: this reads in the free list file, overwriting free list in memory
void CRawDiskFreeList::dump(ostream &str)
{
   RawDiskFreeListIterator iterator;

   // Read in the free list from the raw disk free list file
   if (readFreeListFile() != 0)
      return;     // return if readFreeListFile has failed

   str << " Raw Disk Free List File <" << fileName
       << "> Entries: " << freeList.size() << std::endl;

   // Iterate through entries in free list
   int i;
   for (iterator = freeList.begin(), i = 0; iterator != freeList.end(); ++iterator, ++i)
      {
      str << "  " << i;
      (*iterator)->dump(str);
      }
}

void CRawDiskFreeList::findFirst(MTI_INT64 &offset, MTI_INT64 &size)
{
  posSearch = freeList.begin();

  findNext (offset, size);
}

void CRawDiskFreeList::findNext (MTI_INT64 &offset, MTI_INT64 &size)
{
  if (posSearch == freeList.end())
   {
    // search is over
    offset = -1;
    size = -1;
    return;
   }

  offset = (*posSearch)->offset;
  size = (*posSearch)->size;

  posSearch++;
}

void CRawDiskFreeList::add (MTI_INT64 &offset, MTI_INT64 &size)
{
  CRawDiskFreeEntry *newEntry = new CRawDiskFreeEntry (offset, size);
  freeList.push_back (newEntry);
}

//////////////////////////////////////////////////////////////////////
// CRawDiskFreeEntry Contruction/Destruction
//////////////////////////////////////////////////////////////////////

CRawDiskFreeEntry::CRawDiskFreeEntry()
: offset(0), size(0)
{

}

CRawDiskFreeEntry::CRawDiskFreeEntry(MTI_INT64 newOffset, MTI_INT64 newSize)
: offset(newOffset), size(newSize)
{

}

CRawDiskFreeEntry::~CRawDiskFreeEntry()
{

}


void CRawDiskFreeEntry::dump(ostream &str)
{
   str << "  Offset: " << offset
       << " Size: " << size << std::endl;
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
// CRawDiskBlockList
//////////////////////////////////////////////////////////////////////

CRawDiskBlockList::CRawDiskBlockList(MTI_UINT32 newPaddedFieldSize)
: currentRawDiskBlock(0), paddedFieldSize(newPaddedFieldSize),
  maxFieldNumber(0)
{
}

CRawDiskBlockList::~CRawDiskBlockList()
{
   vector<SRawDiskBlock*>::iterator iter;
   for (iter = rawDiskBlockList.begin(); iter != rawDiskBlockList.end(); ++iter)
      {
      delete *iter;
      }

   rawDiskBlockList.clear();
}

//----------------------------------------------------------------------


void CRawDiskBlockList::addBlock(MTI_INT64 newOffset, MTI_INT64 newSize)
{
   SRawDiskBlock* diskBlock = new SRawDiskBlock;

   diskBlock->offset = newOffset;
   diskBlock->size = newSize;

   diskBlock->firstFieldNumber = -1;
   diskBlock->fieldCount = 0;

   rawDiskBlockList.push_back(diskBlock);
}

const SRawDiskBlock* CRawDiskBlockList::getLastEntry()
{
   if (rawDiskBlockList.empty())
      return 0;   // list is empty, return NULL pointer

   return rawDiskBlockList.back();
}

int CRawDiskBlockList::getEntryCount()
{
   return rawDiskBlockList.size();
}

SRawDiskBlock* CRawDiskBlockList::getEntry(int index)
{
   return rawDiskBlockList[index];
}

bool CRawDiskBlockList::compareByOffset(SRawDiskBlock const * const &b1,
                                        SRawDiskBlock const * const &b2)
{
   return (b1->offset < b2->offset);
}

void CRawDiskBlockList::sortByOffset()
{
   // Sorts the list by the offset of the block so that the list is in
   // ascending order by the disk offset.  This should give better
   // media I/O performance by reducing the disk seek distance.

   if (rawDiskBlockList.size() > 1)
      sort(rawDiskBlockList.begin(), rawDiskBlockList.end(), compareByOffset);

   // Set the field information in each block
   vector<SRawDiskBlock*>::iterator iter;
   MTI_UINT32 firstFieldNumber = 0;
   for (iter = rawDiskBlockList.begin(); iter != rawDiskBlockList.end(); ++iter)
      {
      SRawDiskBlock *block = *iter;
      block->firstFieldNumber = firstFieldNumber;
      MTI_UINT32 fieldCount = block->size / paddedFieldSize;
      block->fieldCount = fieldCount;
      firstFieldNumber += fieldCount;
      }
      
   maxFieldNumber = firstFieldNumber - 1;
}

MTI_INT64 CRawDiskBlockList::getFieldOffset(MTI_UINT32 fieldNumber)
{
   if (fieldNumber > maxFieldNumber)
      return -1;

   MTI_UINT32 currentFirstField;
   if (currentRawDiskBlock != 0)
      currentFirstField = currentRawDiskBlock->firstFieldNumber;
   if (currentRawDiskBlock != 0 && currentFirstField <= fieldNumber
       && currentFirstField + currentRawDiskBlock->fieldCount > fieldNumber)
      {
      return currentRawDiskBlock->offset
             + (MTI_INT64)paddedFieldSize * (fieldNumber - currentFirstField);
      }
   else
      {
      vector<SRawDiskBlock*>::iterator iter;
      for (iter = rawDiskBlockList.begin(); iter != rawDiskBlockList.end(); ++iter)
         {
         SRawDiskBlock *block = *iter;
         MTI_UINT32 firstField = block->firstFieldNumber;
         if (firstField <= fieldNumber
             && fieldNumber < firstField + block->fieldCount)
            {
            currentRawDiskBlock = block;
            return block->offset
                   + (MTI_INT64)paddedFieldSize * (fieldNumber - firstField);
            }
         }
      }

   return -1;  // If we reached this point something went wrong
}



