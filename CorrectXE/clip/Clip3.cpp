// Clip.cpp: implementation of the CClip class.
//
//////////////////////////////////////////////////////////////////////

//#define _CLIP_DATA_CPP PRIVATE_STATIC

#include "Clip3.h"

#include "AudioFormat3.h"
#include "BinDir.h"
#include "BinManager.h"
#include "Clip3TimeTrack.h"
#include "err_clip.h"
#include "FileSweeper.h"
#include "guid_mti.h"
#include "GuidBackupSet.hpp"
#include "HistoryLocation.h"
#include "ImageFileMediaAccess.h"
#include "ImageFormat3.h"
#include "IniFile.h"
#include "MediaAccess.h"
#include "MediaAllocator.h"
#include "MediaDeallocator.h"
#include "MediaStorage3.h"
#include "MTIstringstream.h"
#include "MediaFileIO.h"

#define IsResolutionLicensed(a,b) true

#include <math.h>
#include <errno.h>
#include <fstream>
#include <memory>
#include <map>
using std::map;

//** Bin Manager speedup hack - onform one interested party (typically the
// bin manager form) that a clip's status has changed
CClip::StatusChangeNotifyHandlerType CClip::statusChangeNotifyHandler = NULL;

//////////////////////////////////////////////////////////////////////
// Static Data Members Initialization
//////////////////////////////////////////////////////////////////////

// Clip File Information Section
string CClip::clipInfoSectionName =         "ClipInformation";
string CClip::descriptionKey =              "Description";
string CClip::cdIngestTypeKey =             "CDIngestType";
string CClip::diskSchemeKey =               "DiskScheme";
string CClip::createDateTimeKey =           "CreateDateTime";
string CClip::archiveDateTimeKey =          "ArchiveDateTime";
string CClip::projectNameKey =              "ProjectName";
string CClip::timecodeFrameRateKey =        "TimecodeFrameRate";
string CClip::dropFrameKey =                "DropFrame";
string CClip::timecode720PKey =             "Timecode720P";
string CClip::inTimecodeKey =               "InTimecode";
string CClip::outTimecodeKey =              "OutTimecode";
string CClip::lockRecordKey =               "RecordLock";
string CClip::clipGUIDKey =                 "ClipGUID";
string CClip::clipVersionKey =              "ClipVersion";
string CClip::mediaDestroyedKey =           "MediaDestroyed"; // GAACK

string CClip::nullClipGUIDStr = "00000000-0000-0000-0000-000000000000";

// Render Information Section
string CClip::renderInfoSectionName =       "RenderInformation";
string CClip::sourceClipNameKey =           "SourceClipName";
string CClip::sourceFrameIndexKey =         "SourceFrameIndex";
string CClip::renderFramingTypeKey =        "RenderFramingType";

//----------------------------------------------------------------------------
// Clip & Track Data Versions
static const CStringValueList<EClipVersion>::SInitPair<EClipVersion> clipVerInit[] =
{
   {CLIP_VERSION_3,        "3"  },
   {CLIP_VERSION_5L,       "5L" },
   {CLIP_VERSION_INVALID,  0}  // terminate list
};

CStringValueList<EClipVersion> CClip::clipVerLookup(clipVerInit);

// -------------------------------------------------------------------
// Clip Scheme File

string CClipInitInfo::clipInfoSectionName =   "ClipInformation";
string CClipInitInfo::binPathKey =            "BinPath";
string CClipInitInfo::clipNameKey =           "ClipName";
string CClipInitInfo::createDateTimeKey =     "CreateDateTime";
string CClipInitInfo::archiveDateTimeKey =    "ArchiveDateTime";
string CClipInitInfo::descriptionKey =        "Description";
string CClipInitInfo::cdIngestTypeKey =        "CDIngestType";
string CClipInitInfo::diskSchemeKey =         "DiskScheme";
string CClipInitInfo::timecodeFrameRateKey =  "TimecodeFrameRate";
string CClipInitInfo::dropFrameKey =          "DropFrame";
string CClipInitInfo::inTimecodeKey =         "InTimecode";
string CClipInitInfo::outTimecodeKey =        "OutTimecode";
string CClipInitInfo::markedClipMediaLocKey = "MarkedClipMediaLoc"; // UGGH

string CClipInitInfo::mainVideoSectionName =  "MainVideo";
string CClipInitInfo::videoProxySectionName = "VideoProxy";
string CClipInitInfo::audioTrackSectionName = "AudioTrack";
string CClipInitInfo::timeTrackSectionName =  "TimeTrack";

string CCommonInitInfo::clipVerKey =          "ClipVer";
string CCommonInitInfo::appIdKey =            "AppId";
string CCommonInitInfo::frameRateKey =        "FrameRate";
string CCommonInitInfo::handleCountKey =      "HandleCount";
string CCommonInitInfo::virtualMediaKey =     "VirtualMedia";
string CCommonInitInfo::mediaTypeKey =        "MediaType";
string CCommonInitInfo::mediaIdentifierKey =  "MediaIdentifier";

//****WARNING****WARNING******WARNING*********
// THESE ARE DUPLICATED IN MediaStorage3.cpp !!!!
// and now historySectionName and enableAutoCleanKey are also defined in
// NavSystemInterface.cpp!!!!!!   Woo hoo!
// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
//string CClipInitInfo::metaDataFileName     = "MTIMetadata.ini";
//string CClipInitInfo::historyTypeKey       = "HistoryType";
//string CClipInitInfo::enableAutoCleanKey   = "EnableAutoClean";
//
//string CClipInitInfo::metaDataSectionName   = "Metadata";
//string CClipInitInfo::metaDataDirectoryKey  = "MetadataRootDirectory";
//string CClipInitInfo::iniFileInMetadataDir  = "IniFileInMetadataDir";
//
//string CClipInitInfo::historySectionName   = "History";
//string CClipInitInfo::historyDirectoryKey  = "HistoryDirectory";
//
//string CClipInitInfo::historyTypeFilePerClip  = "FILE_PER_CLIP";
//string CClipInitInfo::historyTypeFilePerFrame = "FILE_PER_FRAME";
//  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
// THE ABOVE ARE DUPLICATED IN MediaStorage3.cpp !!!!
//****WARNING****WARNING******WARNING*********

string CAudioInitInfo::mediaDirectoryKey =    "MediaDirectory";

const string CVideoInitInfo::imageFile0IndexKey = "ImageFile0Index";

string CTimeInitInfo::timeTypeKey =           "TimeType";
string CTimeInitInfo::appIdKey =              "AppId";
string CTimeInitInfo::clipVerKey =            "ClipVer";
string CTimeInitInfo::timecodeFrameRateKey =  "TimecodeFrameRate";

// ----------------------------------------------------------------------------
// CD Ingest Type / Name Map Table

struct SCDIngestTypeMap
{
   const char* name;
   ECDIngestType cdIngestType;
};

static const SCDIngestTypeMap cdIngestTypeMap[] =
{
   {"Unknown",      CD_INGEST_TYPE_INVALID      }, // Not set
   {"FullReel",     CD_INGEST_TYPE_FULL_REEL    }, // full reel model
   {"CircleTakes",  CD_INGEST_TYPE_CIRCLE_TAKES }, // circle take model
};
#define CD_INGEST_TYPE_COUNT (sizeof(cdIngestTypeMap)/sizeof(SCDIngestTypeMap))

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                    ###########################################
// ###     CClip Class     ##########################################
// ####                    ###########################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

static map<CClip *, weak_ptr<CClip>> ClipMap;
static CSpinLock ClipMapLock;
static int ClipMapGeneration = 0;
static int OldClipMapGeneration = 0;

/* static */
ClipSharedPtr CClip::Create()
{
   CAutoSpinLocker lock(ClipMapLock);

   CClip *newClip = new CClip;
	shared_ptr<CClip> sharedClip(newClip);
#ifdef FIND_CLIP_LEAK
	DBTRACE(sharedClip.use_count());
#endif
	ClipMap[newClip] = sharedClip;
#ifdef FIND_CLIP_LEAK
	DBTRACE(sharedClip.use_count());
#endif

   ++ClipMapGeneration;
   return sharedClip;
}

/* static */
ClipSharedPtr CClip::RetrieveSharedPtr(CClip *rawPtr)
{
   CAutoSpinLocker lock(ClipMapLock);

   auto iter = ClipMap.find(rawPtr);
   if (iter == ClipMap.end())
   {
      // NOT FOUND!
      MTIassert(iter != ClipMap.end());
      ClipSharedPtr nullSharedPtr;
      return nullSharedPtr;
   }

   MTIassert(!iter->second.expired());
   return ClipSharedPtr(iter->second.lock());
}

/* static */
void CClip::Destroy(CClip *rawPtr)
{
   CAutoSpinLocker lock(ClipMapLock);

   auto iter = ClipMap.find(rawPtr);
   if (iter != ClipMap.end())
   {
      ClipMap.erase(iter);
   }

   ++ClipMapGeneration;
}

/* static */
void CClip::DumpMap()
{
   CAutoSpinLocker lock(ClipMapLock);

//   if (OldClipMapGeneration == ClipMapGeneration)
//   {
//      // Hasn't changed!
//      return;
//   }

   OldClipMapGeneration = ClipMapGeneration;

#ifdef FIND_CLIP_LEAK
	TRACE_0(errout << "CLIP MAP DUMP (" << ClipMap.size() << ")");
   int expiredCount = 0;
   int blankNameCount = 0;
   for (auto &mapPair : ClipMap)
   {
      auto &weakPtr = mapPair.second;
      if (weakPtr.expired())
      {
         ++expiredCount;
      }
      else
      {
         auto strongPtr = weakPtr.lock();
         auto clipName = strongPtr->getClipName();
         if (clipName.empty())
         {
            ++blankNameCount;
         }
         else
         {
            TRACE_0(errout << "CLIP " << clipName << ": use=" << (strongPtr.use_count() - 1));
         }
      }
   }

   TRACE_0(errout << "Found " << blankNameCount << " blank names and " << expiredCount << " expired pointers.");
#endif
}

CClip::CClip()
: inTimecode(0, 0, 0, 0, false, 1),
  outTimecode(0, 0, 0, 0, false, 1),
  lockRecordFlag(false),
  clipFilesDeleted(false),
  videoFrameListFileChanged(false)
{
   clipGUID = new CLIP_GUID;
   guid_from_string(*clipGUID, nullClipGUIDStr);

   clipVersion = 0;
}

CClip::~CClip()
{
   if (!clipFilesDeleted)
   {
      int retVal = updateAllTrackFiles();
      if (retVal != 0)
      {
         TRACE_0(errout << "ERROR: Could not update track files on clip close (" << retVal << ")");
         TRACE_0(errout <<        "Clip: " << AddDirSeparator(binPath) + clipName);
      }

      // Also the actual video frame lists if necessary
      retVal = updateAllVideoFrameListFiles();
      if (retVal != 0)
      {
         TRACE_0(errout << "ERROR: Could not update frame list files on clip close (" << retVal << ")");
         TRACE_0(errout <<        "Clip: " << AddDirSeparator(binPath) + clipName);
      }
   }

   delete clipGUID;

   Destroy(this);
}

CClip::CClip(const CClip &rhs)
: inTimecode(0, 0, 0, 0, false, 1),
  outTimecode(0, 0, 0, 0, false, 1),
  lockRecordFlag(false),
  clipFilesDeleted(false),
  videoFrameListFileChanged(false)
{
   // Copy constructor is private and should not be used
}

//------------------------------------------------------------------------
//
// Function:    initClip
//
// Description:
//
// Arguments:   CClipInitInfo &clipInitInfo
//
// Returns:     0 = Success
//
//------------------------------------------------------------------------
int CClip::initClip(CClipInitInfo &clipInitInfo)
{
   setBinPath(clipInitInfo.binPath);
   setClipName(clipInitInfo.clipName);
   setCreateDateTime(clipInitInfo.createDateTime);
   setArchiveDateTime(clipInitInfo.archiveDateTime);
   setDescription(clipInitInfo.description);
   setCDIngestType(clipInitInfo.cdIngestType);
   setDiskScheme(clipInitInfo.diskScheme_);
   setInTimecode(clipInitInfo.inTimecode);
   setOutTimecode(clipInitInfo.outTimecode);

   // Create this clip's Globally Unique Identifier
   *clipGUID = guid_create();

   // Init this clip's Version number
   clipVersion = 0;

   return 0;   // Return Success

} // initClip

//------------------------------------------------------------------------
//
// Function:    addVideoProxy
//
// Description: Add a new video proxy to the clip
//
// Arguments:   CVideoInitInfo &initInfo
//
// Returns:     0 = Success
//
//------------------------------------------------------------------------
int CClip::addVideoProxy(CVideoInitInfo &initInfo)
{
   int retVal;

   if (initInfo.clipVer == CLIP_VERSION_3)
      {
      retVal = addVideoProxy_ClipVer3(initInfo);
      }
   else if (initInfo.clipVer == CLIP_VERSION_5L)
      {
      // Clip 5 "Lite"
      retVal = addVideoProxy_ClipVer5L(initInfo);
      }
   else
      {
      TRACE_0(errout << "ERROR: Invalid Clip Version: " << initInfo.clipVer);
      retVal = CLIP_ERROR_INVALID_CLIP_VERSION;
      }

   return retVal;
}

int CClip::addVideoProxy_ClipVer5L(CVideoInitInfo &initInfo)
{
   // Create a Video Proxy with the Clip 5 "Lite" data structures underneath

   int retVal;

   // "Virtual Media" is obsolete in Clip 5
   if (initInfo.virtualMediaFlag)
      return CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;

   CImageFormat *newImageFormat = initInfo.getImageFormat();

   // Add a new Video Proxy object to the Clip
   auto sharedThis = CClip::RetrieveSharedPtr(this);
   if (!sharedThis)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   retVal = videoProxyList.createVideoProxy(sharedThis, initInfo.clipVer,
                                            initInfo.handleCount,
                                            *newImageFormat);
   if (retVal != 0)
      return retVal; // ERROR: Could not add the Video Proxy

   int newVideoProxyIndex = videoProxyList.getTrackCount() - 1;
   CVideoProxy *newVideoProxy
                           = videoProxyList.getVideoProxy(newVideoProxyIndex);

   // Set the Application ID
   newVideoProxy->setAppId(initInfo.getAppId());

   // Set the virtual media flag for this Video Proxy to false, as
   // "virtual media" concept is supplanted by sequences in Clip 5
   newVideoProxy->setVirtualMediaFlag(false);

   // If this is the first video proxy, i.e., normal video, then
   // add the Video-Frames and Film-Frames Framing Info to the Clip's
   // Video Framing List
   // Note: The framing info and frame-lists are not used by Clip 5,
   //       but are necessary to support 1) the legacy API and
   //       2) clip 3 video proxies
   if (newVideoProxyIndex == VIDEO_SELECT_NORMAL)
      {
      CFramingInfo* newFramingInfo;

      // Create Video-Frames Framing Info
      newFramingInfo = videoFramingInfoList.createVideoFramesFramingInfo(sharedThis,
                                                                      initInfo);
      if (newFramingInfo == 0)
         {
         // ERROR: Failed to add new video-frames Framing Info
         return -440;
         }
      }

   // Create a Frame-List List for the new video proxy
   retVal = newVideoProxy->createFrameList_List(videoFramingInfoList);
   if (retVal != 0)
      return retVal; // ERROR: Failed to create the list of Frame Lists

   C5MediaTrackHandle newTrackHandle;

   // TBD: Set frame rate

   // Calculate the number of frames based on difference between out and in
   // timecodes and the handles at the beginning and end of the track
   auto parentClip = newVideoProxy->getParentClip();
   int userFrameCount = parentClip->getOutTimecode() -
                        parentClip->getInTimecode();
   int totalFrameCount = userFrameCount + 2 * initInfo.handleCount;

   // Calculate the number of frames of media to allocate, can be less
   // than the total number of frames in the track
   int allocFrameCount;
   if (initInfo.mediaAllocationFrameCount < 0
       || initInfo.mediaAllocationFrameCount > totalFrameCount)
      {
      // Default is to allocate media for all of the frames
      allocFrameCount = totalFrameCount;
      }
   else
      {
      // Allocate media storage for less than the total number of
      // frames in the clip
      allocFrameCount = initInfo.mediaAllocationFrameCount;
      }
/*
   retVal = newTrackHandle.CreateSequenceTrack(parentClip->getClipPathName(),
                                               initInfo.getMediaType(),
                                               initInfo.getMediaIdentifier(),
                                               *newImageFormat,
                                               totalFrameCount,
                                               allocFrameCount);
*/

   retVal = newTrackHandle.CreateMediaTrack(parentClip->getClipPathName(),
                                            initInfo.getMediaType(),
                                            initInfo.getMediaIdentifier(),
                                            *newImageFormat,
                                            initInfo.getFrameRateEnum(),
                                            totalFrameCount * newImageFormat->getFieldCount(),
                                            allocFrameCount * newImageFormat->getFieldCount());
   if (retVal != 0)
      return retVal;

   newVideoProxy->setClip5TrackHandle(newTrackHandle);

   // Update the Clip File
   retVal = writeFile();
   if (retVal != 0)
      return retVal; // ERROR: Failed to write the Clip File

   return 0;

} // addVideoProxy_ClipVer5L

int CClip::addVideoProxy_ClipVer3(CVideoInitInfo &initInfo)
{
   int retVal;

   CImageFormat *newImageFormat = initInfo.getImageFormat();

   // Add a new Video Proxy object to the Clip
   auto sharedThis = CClip::RetrieveSharedPtr(this);
   if (!sharedThis)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   retVal = videoProxyList.createVideoProxy(sharedThis, initInfo.clipVer,
                                            initInfo.handleCount,
                                            *newImageFormat);
   if (retVal != 0)
      return retVal; // ERROR: Could not add the Video Proxy

   int newVideoProxyIndex = videoProxyList.getTrackCount() - 1;
   CVideoProxy *newVideoProxy
                           = videoProxyList.getVideoProxy(newVideoProxyIndex);

   // Set the Application ID
   newVideoProxy->setAppId(initInfo.getAppId());

   // Set the virtual media flag for this Video Proxy
   newVideoProxy->setVirtualMediaFlag(initInfo.virtualMediaFlag);

   // If this is the first video proxy, i.e., normal video, then
   // add the Video-Frames and Film-Frames Framing Info to the Clip's
   // Video Framing List
   if (newVideoProxyIndex == VIDEO_SELECT_NORMAL)
      {
      CFramingInfo* newFramingInfo;

      // Create Video-Frames Framing Info
      newFramingInfo = videoFramingInfoList.createVideoFramesFramingInfo(sharedThis,
                                                                      initInfo);
      if (newFramingInfo == 0)
         {
         // ERROR: Failed to add new video-frames Framing Info
         return -440;
         }

      // Create Film-Frames Framing Info
      // QQQ - HACK - Force "no pulldown" if the format is not interlaced...
      // because we override interlacing in the case where the video media
      // is in image files. I hope that doesn't screw up 720P-style pulldown!
      newFramingInfo = videoFramingInfoList.createFilmFramesFramingInfo(sharedThis,
                                             !newImageFormat->getInterlaced());
      if (newFramingInfo == 0)
         {
         // ERROR: Failed to add new film-frames Framing Info
         return -441;
         }

      // Create the three Video-Fields Framing Info.  If this clip
      // is not interlaced, three NULL pointers are added to the
      // videoFramingInfoList
      //
      // The three Video-Fields Frame Lists, if they exist, are not populated
      // with frames and fields until the first time they are used.  Since
      // these Frame Lists are never written to .trk files, there is no reason
      // to create them at this point
      if (newImageFormat->getInterlaced())
         {
            MTIassert(false);
//         videoFramingInfoList.createVideoFieldsFramingInfo(sharedThis,
//                                                       FRAMING_TYPE_FIELDS_1_2,
//                                                           -1);
//         videoFramingInfoList.createVideoFieldsFramingInfo(sharedThis,
//                                                     FRAMING_TYPE_FIELD_1_ONLY,
//                                                           -1);
//         videoFramingInfoList.createVideoFieldsFramingInfo(sharedThis,
//                                                     FRAMING_TYPE_FIELD_2_ONLY,
//                                                           -1);
         }
      }

   // Create the field list for the new video proxy
   retVal = newVideoProxy->createFieldList();

   // Create a Frame-List List for the new video proxy
   retVal = newVideoProxy->createFrameList_List(videoFramingInfoList);
   if (retVal != 0)
      return retVal; // ERROR: Failed to create the list of Frame Lists

   // Populate the new video proxy's video-frame frame list with frames and fields
   CVideoFrameList *videoFramesFrameList
                   = newVideoProxy->getVideoFrameList(FRAMING_SELECT_VIDEO);

   retVal = videoFramesFrameList->createVideoFrames_FrameList();
   if (retVal != 0)
      return retVal; // ERROR: Failed to create frames & fields for track

   // Allocate media storage if this isn't a virtual track
   CMediaAllocator *mediaAllocator = 0;
   CMediaAccess *mediaAccess = 0;
   if (!initInfo.virtualMediaFlag)
      {
      string relMediaPath;

      // if a CMediaAllocator instance has not been provided in the caller's
      // initInfo, then allocate the space from the Media Storage Type
      if (initInfo.mediaAllocator == NULL)
         {
         CMediaInterface mediaInterface;
         CTimecode frame0Timecode
                          = videoFramesFrameList->getTimecodeForFrameIndex(0);
         if (CMediaInterface::isMediaTypeImageFile(initInfo.mediaType))
            {
            // If media is stored in image files NOT in a repository, we
            // want to separate the frame 0 timecode (which is really the
            // file index number) from the frame list's timecode
            CTimecode imageFile0IndexTimecode(0);
            if (initInfo.getImageFile0Index() >= 0)
               {
               // Keep the image file indices separate from the timecode
               imageFile0IndexTimecode.setAbsoluteTime(initInfo.getImageFile0Index());
               }
            else if (frame0Timecode.getFramesPerSecond() != 0)
               {
               imageFile0IndexTimecode.setAbsoluteTime(0);
               }
            else
               {
               imageFile0IndexTimecode = frame0Timecode;
               }
            frame0Timecode = imageFile0IndexTimecode;
            }
         else if (CMediaInterface::isMediaTypeImageRepository(initInfo.mediaType))
            {

            const string &repositoryName = initInfo.mediaIdentifier;
            if (repositoryName.empty())
               {
               TRACE_0(errout << "ERROR: CClip::addVideoProxy:"
                              << " Missing media identifier");
               return CLIP_ERROR_VIDEO_MEDIA_DIR_NOT_AVAILABLE;
               }

            // QQQ THIS STUFF SHOULD BE HIDDEN IN THE MEDIA ACCESS CLASS!!
            // We want to build a new Media Identifier if the
            // files are in a repository. It consists of the names of the
            // repository, the "relative path" of the bin, and the name
            // of the clip.
            string videoMediaIdentifier;
            string binPath = getBinPath();

            // Make bin path relative to root bin
            // the following code stolen from BinManagerGUIUnit - uggggh
            StringList binList;
            ::CPMPGetBinRoots(&binList);
            string rootPath;
            if (binList.size() == 1)
               rootPath = binList[0];
            else
               {
               TRACE_0(errout << "TOO MANY ROOT BINS!!!  (" << binList.size() << ")");
               }

            if (binPath.substr(0,rootPath.length()) == rootPath)
               {
               binPath = binPath.substr(rootPath.length());
               }
            else
               {
               TRACE_0(errout << "ERROR: CClip::addVideoProxy:" << " Bad bin path:" << endl
                              << "       " << binPath << endl
                              << "       doesn't start with the root bin path:" << endl
                              << "       " << rootPath);
               return CLIP_ERROR_VIDEO_MEDIA_DIR_NOT_AVAILABLE;
               }

            // This is the default location of the clip's media files,
            // relative to the repository root directory
            relMediaPath = AddDirSeparator(binPath) + getClipName();

/****
            videoMediaIdentifier  = initInfo.mediaIdentifier + string(":") +
                                    AddDirSeparator(binPath) +
                                    getClipName();

            // Cleanup the resulting identifier
            SearchAndReplace(videoMediaIdentifier, DIR_DOUBLESEPARATOR,
                             sDIR_SEPARATOR);

            initInfo.mediaIdentifier = videoMediaIdentifier;
****/

            TRACE_3(errout << "CClip::addVideoProxy: new Media Identifier = "
                           << initInfo.mediaIdentifier << ":" << relMediaPath);
            }

         int totalFrameCount = videoFramesFrameList->getTotalFrameCount();
         int newFrameCount;
         if (initInfo.mediaAllocationFrameCount < 0
             || initInfo.mediaAllocationFrameCount > totalFrameCount)
            {
            newFrameCount = totalFrameCount;  // allocate full measure of frames
            }
         else
            {
            // Allocate media storage for less than the total number of
            // frames in the clip
            newFrameCount = initInfo.mediaAllocationFrameCount;
            }

         mediaAllocator = mediaInterface.allocateSpace(initInfo.mediaType,
                                                       initInfo.mediaIdentifier,
                                                       initInfo.historyDirectory,
                                                       *newImageFormat,
                                                       frame0Timecode,
                                                       newFrameCount,
                                                       relMediaPath);
         if (mediaAllocator == 0)
            return CLIP_ERROR_MEDIA_ALLOCATION_FAILED; // ERROR: Allocation failed

         initInfo.mediaAllocator = mediaAllocator;
         }

      // Media Storage Allocation and Assignment to Video-Frames Track
      retVal = newVideoProxy->assignMediaStorage(*mediaAllocator);
      if (retVal != 0)
         {
         if (mediaAllocator != 0)
            {
            // Cancel media storage allocation
            mediaAllocator->cancelAllocation();
            delete mediaAllocator;
            initInfo.mediaAllocator = 0;
            }
         return retVal; // ERROR: Failed to assign storage to Proxy
         }

      // Update the frame list's allocated frame count and allocation frontier
      videoFramesFrameList->getMediaAllocatedFrontier(true);

      // Remember the CMediaAccess object, in case this media storage comes
      // from the OGLV Bridge
      mediaAccess = initInfo.mediaAllocator->getMediaAccess();
      }
   else
      {
      // This is a virtual clip, so mark all of the fields as having
      // virtual media
      retVal
        = newVideoProxy->assignVirtualMedia(newImageFormat->getBytesPerField());
      if (retVal != 0)
         return retVal;
      }

   // If appropriate, create Film-Frames track based on default 3:2 pulldown
   CVideoFrameList *filmFramesFrameList;
   filmFramesFrameList = newVideoProxy->getVideoFrameList(FRAMING_SELECT_FILM);

   if (filmFramesFrameList != videoFramesFrameList)
      {
      // Distinct film-frames and video-frames frame lists

      // Calculate a default AA frame timecode based on the
      // whole hour portion of the clip's in timecode
      CTimecode AAFrameTimecode = GetWholeHourTimecode(getInTimecode());
      TRACE_2(errout << "INFO: CClip::addVideoProxy: AAFrameTimecode "
                     << AAFrameTimecode);

      int AAFrameIndex = AAFrameTimecode - getInTimecode();
      AAFrameIndex += initInfo.handleCount;
      int outFrameIndex = videoFramesFrameList->getTotalFrameCount();
      EImageFormatType imageFormatType = newImageFormat->getImageFormatType();
      EPulldown nominalPulldown
                           = CImageInfo::queryNominalPulldown(imageFormatType);
      EFrameLabel *frameLabels = new EFrameLabel[outFrameIndex];
      CVideoFrameList::create3_2PulldownFrameLabels(0, outFrameIndex,
                                                     AAFrameIndex,
                                                     nominalPulldown,
                                                     frameLabels);

      // Populate the normal proxy's film-frame track with frames and fields
      // based on video-frames track with 3:2 pull-down removed
      retVal
        = filmFramesFrameList->createFilmFrames_FrameList(videoFramesFrameList,
                                                          nominalPulldown,
                                                          frameLabels);

      delete [] frameLabels;
      if (retVal != 0)
         {
         if (mediaAllocator != 0)
            {
            // Cancel media storage allocation
            mediaAllocator->cancelAllocation();
            delete mediaAllocator;
            initInfo.mediaAllocator = 0;
            }
         return retVal;
         }
      }

   // Initialize frame lists for Video-Fields frame lists, but
   // don't populate with frames or fields
   CVideoFrameList *videoFieldsFrameList
                 = newVideoProxy->getVideoFrameList(FRAMING_SELECT_FIELDS_1_2);
   if (videoFieldsFrameList != 0)
      {
      retVal = videoFieldsFrameList->initVideoFields_FrameList();
      if (retVal != 0)
         {
         if (mediaAllocator != 0)
            {
            // Cancel media storage allocation
            mediaAllocator->cancelAllocation();
            delete mediaAllocator;
            initInfo.mediaAllocator = 0;
            }
         return retVal;
         }
      }

   videoFieldsFrameList
                = newVideoProxy->getVideoFrameList(FRAMING_SELECT_FIELD_1_ONLY);
   if (videoFieldsFrameList != 0)
      {
      retVal = videoFieldsFrameList->initVideoFields_FrameList();
      if (retVal != 0)
         {
         if (mediaAllocator != 0)
            {
            // Cancel media storage allocation
            mediaAllocator->cancelAllocation();
            delete mediaAllocator;
            initInfo.mediaAllocator = 0;
            }
         return retVal;
         }
      }

   videoFieldsFrameList
                = newVideoProxy->getVideoFrameList(FRAMING_SELECT_FIELD_2_ONLY);
   if (videoFieldsFrameList != 0)
      {
      retVal = videoFieldsFrameList->initVideoFields_FrameList();
      if (retVal != 0)
         {
         if (mediaAllocator != 0)
            {
            // Cancel media storage allocation
            mediaAllocator->cancelAllocation();
            delete mediaAllocator;
            initInfo.mediaAllocator = 0;
            }
         return retVal;
         }
      }

   videoFieldsFrameList
                = newVideoProxy->getVideoFrameList(FRAMING_SELECT_CADENCE_REPAIR);
   if (videoFieldsFrameList != 0)
      {
      retVal = videoFieldsFrameList->initVideoFields_FrameList();
      if (retVal != 0)
         {
         if (mediaAllocator != 0)
            {
            // Cancel media storage allocation
            mediaAllocator->cancelAllocation();
            delete mediaAllocator;
            initInfo.mediaAllocator = 0;
            }
         return retVal;
         }
      }

   // Write the Field List file
   retVal = newVideoProxy->getFieldList()->writeFieldListFile();
   if (retVal != 0)
      {
      if (mediaAllocator != 0)
         {
         // Cancel media storage allocation
         mediaAllocator->cancelAllocation();
         delete mediaAllocator;
         initInfo.mediaAllocator = 0;
         }
      return retVal; // ERROR: Failed to write Field List file
                     // in Video Proxy
      }

   // Write all of the new Video Proxy's Frame List files
   retVal = newVideoProxy->writeAllFrameListFiles();
   if (retVal != 0)
      {
      if (mediaAllocator != 0)
         {
         // Cancel media storage allocation
         mediaAllocator->cancelAllocation();
         delete mediaAllocator;
         initInfo.mediaAllocator = 0;
         }
      return retVal; // ERROR: Failed to write video Frame List files
                     // in Video Proxy
      }

   // Update the Clip File
   retVal = writeFile();
   if (retVal != 0)
      {
      if (mediaAllocator != 0)
         {
         // Cancel media storage allocation
         mediaAllocator->cancelAllocation();
         delete mediaAllocator;
         initInfo.mediaAllocator = 0;
         }

      // TBD: Need to delete the frame list files here

      return retVal; // ERROR: Failed to write the Clip File
      }

   // Delete the CMediaAllocator instance if it has been created locally
   if (mediaAllocator != 0)
      {
      delete mediaAllocator;
      initInfo.mediaAllocator = 0;
      }

   return 0; // Success

} // addVideoProxy_ClipVer3

// --------------------------------------------------------------------------

int CClip::fixPALFieldDominance(int videoProxyIndex)
{
   int retVal;

   CVideoProxy *videoProxy = getVideoProxy(videoProxyIndex);

   // If necessary, create Film-Frames framing info and frame list
   CVideoFrameList *videoFramesFrameList;
   videoFramesFrameList = videoProxy->getVideoFrameList(FRAMING_SELECT_VIDEO);
   CVideoFrameList *filmFramesFrameList;
   filmFramesFrameList = videoProxy->getVideoFrameList(FRAMING_SELECT_FILM);

   if (filmFramesFrameList == videoFramesFrameList)
      {
      // Distinct film-frames and video-frames frame lists
      // Make a new CFramingInfo for kludged film-frames by copying
      // the CFramingInfo for video-frames
      CVideoFramingInfo *newFilmFramesFramingInfo;
      newFilmFramesFramingInfo = new CVideoFramingInfo(0, 0);
      *newFilmFramesFramingInfo
            = *(videoFramingInfoList.getVideoFramingInfo(FRAMING_SELECT_VIDEO));

      // Change the new CFrameInfo so that it looks like film-frames
      newFilmFramesFramingInfo->setFramingType(FRAMING_TYPE_FILM);
//      int newFramingNumber = videoFramingInfoList.findNewFramingNumber();
      newFilmFramesFramingInfo->setFramingNumber(1);

      // Modify the framingInfoList so it points to the new CFramingInfo
      videoFramingInfoList.setFramingInfo(FRAMING_SELECT_FILM,
                                          newFilmFramesFramingInfo);

      // Create a new frame list for the kludged film-frames
      CFrameList *newFrameList
                        = videoProxy->createFrameList(newFilmFramesFramingInfo);
      filmFramesFrameList = static_cast<CVideoFrameList*>(newFrameList);

      // Modify the frameList_List to point to the new film-frames frame list
      videoProxy->setFrameList(FRAMING_SELECT_FILM, newFrameList);
      }

   // Populate the new film-frames framing list with the field dominance
   // corrected
   int outFrameIndex = videoFramesFrameList->getTotalFrameCount();
   // QQQ No delete[] for frameLabels[]?? QQQ
   EFrameLabel *frameLabels = new EFrameLabel[outFrameIndex];   // dummy
   retVal
        = filmFramesFrameList->createFilmFrames_FrameList(videoFramesFrameList,
                                                          IF_PULLDOWN_PAL_FD,
                                                          frameLabels);
   if (retVal != 0)
      return retVal;

   // Write out the new frame list into the track file
   retVal = filmFramesFrameList->writeFrameListFile();
   if (retVal != 0)
      return retVal;

   // Update the Clip File
   retVal = writeFile();
   if (retVal != 0)
      return retVal;

   return 0;

} // fixPALFieldDominance

//------------------------------------------------------------------------
//
// Function:    addAudioTrack
//
// Description:
//
// Arguments:   CAudioInitInfo& audioInitInfo
//
// Returns:     0 = Success
//
//------------------------------------------------------------------------
int CClip::addAudioTrack(CAudioInitInfo &initInfo)
{
   int retVal;

   if (initInfo.clipVer == CLIP_VERSION_3)
      {
      retVal = addAudioTrack_ClipVer3(initInfo);
      }
   else if (initInfo.clipVer == CLIP_VERSION_5L)
      {
      // Clip 5 "Lite"
      retVal = addAudioTrack_ClipVer5L(initInfo);
      }
   else
      {
      TRACE_0(errout << "ERROR: Invalid Clip Version: " << initInfo.clipVer);
      retVal = CLIP_ERROR_INVALID_CLIP_VERSION;
      }

   return retVal;
}

int CClip::addAudioTrack_ClipVer3(CAudioInitInfo &initInfo)
{
   int retVal;

   CAudioFormat *newAudioFormat = initInfo.getAudioFormat();

   // Set the samples per field if it has not already been set
   if (newAudioFormat->getSamplesPerField() <= 1.0)
      {
      double fieldRate;
      fieldRate = initInfo.getFrameRate() * newAudioFormat->getFieldCount();
      if (fieldRate > 1.0)
         {
         double newSamplesPerField;
         newSamplesPerField = newAudioFormat->getSampleRate() / fieldRate;
         newAudioFormat->setSamplesPerField(newSamplesPerField);
         }
      }

   // Add a new Audio Track object to the Clip
   auto sharedThis = CClip::RetrieveSharedPtr(this);
   if (!sharedThis)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   retVal = audioTrackList.createAudioTrack(sharedThis, initInfo.clipVer,
                                            initInfo.handleCount,
                                            *newAudioFormat);
   if (retVal != 0)
      return retVal; // ERROR: Could not add the Audio Track

   int newAudioTrackIndex = audioTrackList.getTrackCount() - 1;
   CAudioTrack *newAudioTrack
                           = audioTrackList.getAudioTrack(newAudioTrackIndex);

   // Set the Application ID
   newAudioTrack->setAppId(initInfo.getAppId());

   // Set the virtual media flag for this Audio Track
   newAudioTrack->setVirtualMediaFlag(initInfo.virtualMediaFlag);

   // Set the Media Directory
   newAudioTrack->setMediaDirectory(initInfo.mediaDirectory);

   // If this is the first Audio Track, then add the Video-Frames Info to
   //  the Clip's Audio Framing List
   if (newAudioTrackIndex == 0)
      {
      CFramingInfo* newFramingInfo;

      // Create Video-Frames Framing Info
      newFramingInfo = audioFramingInfoList.createVideoFramesFramingInfo(sharedThis,
                                                                     initInfo);
      if (newFramingInfo == 0)
         {
         // ERROR: Failed to add new video-frames Framing Info
         return -440;
         }
      }

   // Create the field list for the new audio track.  This field list
   // will not be written to a file, but it is necessary for creating
   // the fields in the frame lists
   retVal = newAudioTrack->createFieldList();

   // Create a Frame-List List for the new Audio Track
   retVal = newAudioTrack->createFrameList_List(audioFramingInfoList);
   if (retVal != 0)
      return retVal; // ERROR: Failed to create the Frame List list

   // Populate the new Audio Tracks's video-frame frame list with frames and fields
   CAudioFrameList *frameList
                      = newAudioTrack->getAudioFrameList(FRAMING_SELECT_VIDEO);

   retVal = frameList->createVideoFrames_FrameList();
   if (retVal != 0)
      return retVal; // ERROR: Failed to create frames & fields for track

   // if a CMediaAllocator instance has not been provided in the caller's
   // initInfo, then allocate the space from the Media Storage Type
   CMediaAllocator *mediaAllocator = 0;
   if (!initInfo.virtualMediaFlag)
      {
      if (initInfo.mediaAllocator == NULL)
         {
         string newMediaIdentifier;
         if (initInfo.mediaIdentifier.empty())
            {
            retVal = MakeAudioMediaIdentifier(initInfo,
                                              newAudioTrack->getTrackNumber(),
                                              newMediaIdentifier);
            if (retVal != 0)
               return retVal;

#ifdef OLDWAY
            // The init info does not contain an explicit media identifier, so
            // we must create a directory name for the audio media file.  The
            // directory name will look like mediaDirectory/binPath/clipName.
            // If the init info's mediaDirectory is blank, then the audio media
            // directory is the same as the clip's directory.
            string audioMediaDirectory;

            if (!initInfo.mediaDirectory.empty())
               {
               if (!DoesDirectoryExist(initInfo.mediaDirectory))
                  {
                  TRACE_0(errout << "ERROR: CClip::addAudioTrack: Media Directory "
                                 << endl
                                 << "       " << initInfo.mediaDirectory << endl
                                 << "       does not exist" << endl
                                 << "       " << strerror(errno));
                  return CLIP_ERROR_AUDIO_MEDIA_DIR_NOT_AVAILABLE;
                  }

               audioMediaDirectory = AddDirSeparator(initInfo.mediaDirectory);
               }
            audioMediaDirectory += getClipGUIDStr();

            // Cleanup the resulting audio media directory
            SearchAndReplace(audioMediaDirectory, DIR_DOUBLESEPARATOR,
                             sDIR_SEPARATOR);

            // Make the directory for the audio media.  If it already exists,
            // that is okay.  If some of the parent directories don't exist
            // then they are created too.  Permissions problems are probably the
            // only thing likely to cause a failure
            if (!MakeDirectory(audioMediaDirectory))
               {
               // Error: couldn't create the audio media directory
               TRACE_0(errout << "ERROR: CClip::addAudioTrack: Could not create"
                              << endl
                              << "       audio media directory "
                              << audioMediaDirectory << endl
                              << "because " << strerror(errno));
               return CLIP_ERROR_AUDIO_MEDIA_DIR_CREATE_FAILED;
               }

            // The file name will look like "at1_media.raw"
            MTIostringstream ostr;
            ostr << "at" << newAudioTrack->getTrackNumber() << "_media";
            if (initInfo.mediaType == MEDIA_TYPE_RAW_AUDIO)
               ostr << ".raw";  // Need to put this into a function

            newMediaIdentifier = AddDirSeparator(audioMediaDirectory)
                                 + ostr.str();

            TRACE_3(errout << "CClip::addAudioTrack: newMediaIdentifier = "
                           << endl << newMediaIdentifier);
#endif // #ifdef OLDWAY
            }
         else
            {
            // Use the media identifier in the init info
            newMediaIdentifier = initInfo.mediaIdentifier;
            }

         CTimecode frame0Timecode = frameList->getTimecodeForFrameIndex(0);
         int totalFrameCount = frameList->getTotalFrameCount();
         int newFrameCount;
         if (initInfo.mediaAllocationFrameCount < 0
             || initInfo.mediaAllocationFrameCount > totalFrameCount)
            newFrameCount = totalFrameCount;  // allocate full measure of frames
         else
            {
            // Allocate media storage for less than the total number of
            // frames in the clip
            newFrameCount = initInfo.mediaAllocationFrameCount;
            }
         CMediaInterface mediaInterface;   // singleton
         mediaAllocator = mediaInterface.allocateSpace(initInfo.mediaType,
                                                       newMediaIdentifier,
                                                       "geekAddAudioTrack",
                                                       *newAudioFormat,
                                                       frame0Timecode,
                                                       newFrameCount,
                                                       "");
         if (mediaAllocator == 0)
            return CLIP_ERROR_MEDIA_ALLOCATION_FAILED; // ERROR: Allocation failed

         initInfo.mediaAllocator = mediaAllocator;
         }

      // Media Storage Allocation and Assignment to Video-Frames Track
      retVal = newAudioTrack->assignMediaStorage(*(initInfo.mediaAllocator));
      if (retVal != 0)
         {
         if (mediaAllocator != 0)
            {
            // Cancel media storage allocation
            mediaAllocator->cancelAllocation();
            delete mediaAllocator;
            initInfo.mediaAllocator = 0;
            }
         return retVal; // ERROR: Failed to assign storage to Proxy
         }

      // Update the frame list's allocated frame count and allocation frontier
      frameList->getMediaAllocatedFrontier(true);
      }
   else
      {
      // This is a virtual clip, so mark all of the fields as having
      // virtual media
      retVal = newAudioTrack->assignVirtualMedia(1);  // bogus size
      if (retVal != 0)
         return retVal;
      }

   // Write the Field List file
   retVal = newAudioTrack->getFieldList()->writeFieldListFile();
   if (retVal != 0)
      {
      if (mediaAllocator != 0)
         {
         // Cancel media storage allocation
         mediaAllocator->cancelAllocation();
         delete mediaAllocator;
         initInfo.mediaAllocator = 0;
         }
      return retVal; // ERROR: Failed to write  Frame List files in Audio Track
      }

   // Write all of the new Track's Frame List files
   retVal = newAudioTrack->writeAllFrameListFiles();
   if (retVal != 0)
      {
      if (mediaAllocator != 0)
         {
         // Cancel media storage allocation
         mediaAllocator->cancelAllocation();
         delete mediaAllocator;
         initInfo.mediaAllocator = 0;
         }
      return retVal; // ERROR: Failed to write  Frame List files in Audio Track
      }

   // Update the Clip File
   retVal = writeFile();
   if (retVal != 0)
      {
      if (mediaAllocator != 0)
         {
         // Cancel media storage allocation
         mediaAllocator->cancelAllocation();
         delete mediaAllocator;
         initInfo.mediaAllocator = 0;
         }
      return retVal; // ERROR: Failed to write the Clip File
      }

   // Delete the CMediaAllocator instance if it has been created locally
   if (mediaAllocator != 0)
      {
      delete mediaAllocator;
      initInfo.mediaAllocator = 0;
      }

   return 0; // Success

} // addAudioTrack_ClipVer3

int CClip::addAudioTrack_ClipVer5L(CAudioInitInfo &initInfo)
{
   // Create an Audio Track with the Clip 5 "Lite" data structures underneath

   int retVal;

   // "Virtual Media" is obsolete in Clip 5
   if (initInfo.virtualMediaFlag)
      return CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;

   CAudioFormat *newAudioFormat = initInfo.getAudioFormat();

   // Set the samples per field if it has not already been set
   if (newAudioFormat->getSamplesPerField() <= 1.0)
      {
      double fieldRate;
      fieldRate = initInfo.getFrameRate() * newAudioFormat->getFieldCount();
      if (fieldRate > 1.0)
         {
         double newSamplesPerField;
         newSamplesPerField = newAudioFormat->getSampleRate() / fieldRate;
         newAudioFormat->setSamplesPerField(newSamplesPerField);
         }
      }

   // Add a new Audio Track object to the Clip
   auto sharedThis = CClip::RetrieveSharedPtr(this);
   if (!sharedThis)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   retVal = audioTrackList.createAudioTrack(sharedThis, initInfo.clipVer,
                                            initInfo.handleCount,
                                            *newAudioFormat);
   if (retVal != 0)
      return retVal; // ERROR: Could not add the Audio Track

   int newAudioTrackIndex = audioTrackList.getTrackCount() - 1;
   CAudioTrack *newAudioTrack
                           = audioTrackList.getAudioTrack(newAudioTrackIndex);

   // Set the Application ID
   newAudioTrack->setAppId(initInfo.getAppId());

   // Set the virtual media flag for this Audio Track to false, since
   // the virtual media concept is supplanted by sequences in Clip 5.
   newAudioTrack->setVirtualMediaFlag(false);

   // Set the Media Directory
   newAudioTrack->setMediaDirectory(initInfo.mediaDirectory);

   // If this is the first Audio Track, then add the Video-Frames Info to
   //  the Clip's Audio Framing List
   if (newAudioTrackIndex == 0)
      {
      CFramingInfo* newFramingInfo;

      // Create Video-Frames Framing Info
      newFramingInfo = audioFramingInfoList.createVideoFramesFramingInfo(sharedThis,
                                                                     initInfo);
      if (newFramingInfo == 0)
         {
         // ERROR: Failed to add new video-frames Framing Info
         return -440;
         }
      }

   // Create a Frame-List List for the new Audio Track
   retVal = newAudioTrack->createFrameList_List(audioFramingInfoList);
   if (retVal != 0)
      return retVal; // ERROR: Failed to create the Frame List list

   string newMediaIdentifier;
   if (initInfo.mediaIdentifier.empty())
      {
      retVal = MakeAudioMediaIdentifier(initInfo,
                                        newAudioTrack->getTrackNumber(),
                                        newMediaIdentifier);
      if (retVal != 0)
         return retVal;
      }
   else
      {
      // Use the media identifier in the init info
      newMediaIdentifier = initInfo.mediaIdentifier;
      }


   // TBD: Set frame rate

   // Calculate the number of frames based on difference between out and in
   // timecodes and the handles at the beginning and end of the track
   auto parentClip = newAudioTrack->getParentClip();
   int userFrameCount = parentClip->getOutTimecode() -
                        parentClip->getInTimecode();
   int totalFrameCount = userFrameCount + 2 * initInfo.handleCount;

   // Calculate the number of frames of media to allocate, can be less
   // than the total number of frames in the track
   int allocFrameCount;
   if (initInfo.mediaAllocationFrameCount < 0
       || initInfo.mediaAllocationFrameCount > totalFrameCount)
      {
      // Default is to allocate media for all of the frames
      allocFrameCount = totalFrameCount;
      }
   else
      {
      // Allocate media storage for less than the total number of
      // frames in the clip
      allocFrameCount = initInfo.mediaAllocationFrameCount;
      }

   C5MediaTrackHandle newTrackHandle;
/*
   retVal = newTrackHandle.CreateSequenceTrack(parentClip->getClipPathName(),
                                               initInfo.getMediaType(),
                                               newMediaIdentifier,
                                               *newAudioFormat,
                                               totalFrameCount,
                                               allocFrameCount);
*/
   retVal = newTrackHandle.CreateMediaTrack(parentClip->getClipPathName(),
                                            initInfo.getMediaType(),
                                            newMediaIdentifier,
                                            *newAudioFormat,
                                            initInfo.getFrameRateEnum(),
                                            totalFrameCount * newAudioFormat->getFieldCount(),
                                            allocFrameCount * newAudioFormat->getFieldCount());
   if (retVal != 0)
      return retVal;

   newAudioTrack->setClip5TrackHandle(newTrackHandle);

   // Update the frame list's allocated frame count and allocation frontier
   CAudioFrameList *frameList = newAudioTrack->getAudioFrameList(0);
   if (frameList == 0)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;
   frameList->getMediaAllocatedFrontier(true);

   // Update the Clip File
   retVal = writeFile();
   if (retVal != 0)
      return retVal; // ERROR: Failed to write the Clip File

   return 0; // Success

}

// Create a new audio media identifier from info in the clip and
// audio init info.  Also creates any directories that do not exist.
int CClip::MakeAudioMediaIdentifier(const CAudioInitInfo &initInfo,
                                    int newAudioTrackNumber,
                                    string &newMediaIdentifier)
{
   // REMOVE THIS METHOD

   return 0;   // success
}

//------------------------------------------------------------------------
//
// Function:    addTimeTrack
//
// Description: Add a time track to the clip
//
// Arguments:   CTimeInitInfo& initInfo
//
// Returns:     0 = Success
//
//------------------------------------------------------------------------
int CClip::addTimeTrack(CTimeInitInfo &initInfo)
{
   int retVal;

   if (initInfo.clipVer == CLIP_VERSION_5L &&
       initInfo.getTimeType() != TIME_TYPE_SMPTE_TIMECODE
       && initInfo.getTimeType() != TIME_TYPE_KEYKODE)
      {
      // Clip 5 only supports SMPTE Timecode and Keykode tracks,
      // all the rest created as Clip 3 time tracks
      initInfo.clipVer = CLIP_VERSION_3;
      }

   if (initInfo.clipVer == CLIP_VERSION_3)
      {
      retVal = addTimeTrack_ClipVer3(initInfo);
      }
   else if (initInfo.clipVer == CLIP_VERSION_5L)
      {
      // Clip 5 "Lite"
      retVal = addTimeTrack_ClipVer5L(initInfo);
      }
   else
      {
      TRACE_0(errout << "ERROR: Invalid Clip Version: " << initInfo.clipVer);
      retVal = CLIP_ERROR_INVALID_CLIP_VERSION;
      }

   return retVal;

}  // addTimeTrack

int CClip::addTimeTrack_ClipVer5L(CTimeInitInfo &initInfo)
{
   int retVal;

   // Add a new Time Track to this CClip
   auto sharedThis = CClip::RetrieveSharedPtr(this);
   if (!sharedThis)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   retVal = timeTrackList.createTimeTrack(initInfo.getTimeType(),
                                          initInfo.getClipVer(), sharedThis);
   if (retVal != 0)
      return retVal; // ERROR: Could not add the Time Track

   int newTimeTrackIndex = getTimeTrackCount() - 1;
   CTimeTrack *newTimeTrack = getTimeTrack(newTimeTrackIndex);

   // Set Application Id
   newTimeTrack->setAppId(initInfo.getAppId());

   // The number of frames in the new time track will be the total number
   // of frames in the Main Video.  If the Main Video is not available, then
   // the first audio track is used as the source
   CFrameList *frameList = 0;
   if (getVideoProxyCount() > 0)
      {
      frameList = getVideoFrameList(VIDEO_SELECT_NORMAL, FRAMING_SELECT_VIDEO);
      }
   else if (getAudioTrackCount() > 0)
      {
      frameList = getAudioFrameList(0);
      }

   if (frameList == 0)
      return CLIP_ERROR_INVALID_EMPTY_CLIP;

   EFrameRate editRate = frameList->getFrameRateEnum();

   int newFrameCount = frameList->getTotalFrameCount();
   if (newFrameCount < 1)
      return CLIP_ERROR_INVALID_EMPTY_CLIP;

   if (initInfo.getTimeType() == TIME_TYPE_SMPTE_TIMECODE)
      {
      EFrameRate timecodeFrameRate = initInfo.getTimecodeFrameRate();
      if (timecodeFrameRate == IF_FRAME_RATE_INVALID)
         timecodeFrameRate = editRate;

      C5SMPTETimecodeTrackHandle newTrackHandle;
//      retVal = newTrackHandle.CreateSequenceTrack(getClipPathName(),
//                                                  editRate, timecodeFrameRate,
//                                                  newFrameCount);
      retVal = newTrackHandle.CreateSMPTETimecodeTrack(getClipPathName(),
                                                       editRate,
                                                       timecodeFrameRate,
                                                       newFrameCount);
      if (retVal != 0)
         return retVal;

      newTimeTrack->setClip5TrackHandle(newTrackHandle);
      }
   else if (initInfo.getTimeType() == TIME_TYPE_KEYKODE)
      {
      C5KeycodeTrackHandle newTrackHandle;
      retVal = newTrackHandle.CreateKeycodeTrack(getClipPathName(), editRate,
                                                 newFrameCount);
      if (retVal != 0)
         return retVal;

      newTimeTrack->setClip5TrackHandle(newTrackHandle);
      }
   else
      return CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;

   // Update the Clip File
   retVal = writeFile();
   if (retVal != 0)
      return retVal; // ERROR: Failed to write the Clip File

   return 0;

} // addTimeTrack_ClipVer5L

int CClip::addTimeTrack_ClipVer3(CTimeInitInfo &initInfo)
{
   int retVal;

   // Add a new Time Track to this CClip
   auto sharedThis = CClip::RetrieveSharedPtr(this);
   if (!sharedThis)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   retVal = timeTrackList.createTimeTrack(initInfo.getTimeType(),
                                          initInfo.getClipVer(), sharedThis);
   if (retVal != 0)
      return retVal; // ERROR: Could not add the Time Track

   int newTimeTrackIndex = getTimeTrackCount() - 1;
   CTimeTrack *newTimeTrack = getTimeTrack(newTimeTrackIndex);

   // Set Application Id
   newTimeTrack->setAppId(initInfo.getAppId());

   // The number of frames in the new time track will be the total number
   // of frames in the Main Video.  If the Main Video is not available, then
   // the first audio track is used as the source
   CFrameList *frameList = 0;
   if (getVideoProxyCount() > 0)
      {
      frameList = getVideoFrameList(VIDEO_SELECT_NORMAL, FRAMING_SELECT_VIDEO);
      }
   else if (getAudioTrackCount() > 0)
      {
      frameList = getAudioFrameList(0);
      }

   if (frameList == 0)
      return CLIP_ERROR_INVALID_EMPTY_CLIP;

   int newFrameCount = frameList->getTotalFrameCount();
   if (newFrameCount < 1)
      return CLIP_ERROR_INVALID_EMPTY_CLIP;

   // Create the time track frames
   retVal = newTimeTrack->createFrameList(newFrameCount);

   // Write all of the new Track's Frame List files
   retVal = newTimeTrack->writeFrameListFile();
   if (retVal != 0)
      return retVal; // ERROR: Failed to write Frame List files for Time Track

   // Update the Clip File
   retVal = writeFile();
   if (retVal != 0)
      return retVal; // ERROR: Failed to write the Clip File

   return 0;

} // addTimeTrack_ClipVer3


//------------------------------------------------------------------------
//
// Function:    extendClip
//
// Description: Add frames to the end of the clip.  Adds frames to all
//              video proxies and audio tracks
//
// Arguments:   int frameCount          Number of frames to add
//              bool allocateMedia      Allocate media storage for new frames
//
//
// Returns:     0 = Success
//
//------------------------------------------------------------------------
int CClip::extendClip(int frameCount, bool allocateMedia)
{
   int retVal;

   int videoProxyCount = getVideoProxyCount();
   int audioTrackCount = getAudioTrackCount();

   if (allocateMedia)
      {
      // Check that there is sufficient media storage to extend the clip
      for (int i = 0; i < videoProxyCount; ++i)
         {
         retVal = getVideoProxy(i)->checkExtendMediaStorage(frameCount);
         if (retVal != 0)
            return retVal;   // Insufficient media storage to extend this track
         }

      for (int i = 0; i < audioTrackCount; ++i)
         {
         retVal = getAudioTrack(i)->checkExtendMediaStorage(frameCount);
         if (retVal != 0)
            return retVal;   // Insufficient media storage to extend this track
         }
      }

   // Iterate over the video proxies
   for (int i = 0; i < videoProxyCount; ++i)
      {
      retVal = getVideoProxy(i)->extendVideoProxy(frameCount, allocateMedia);
      if (retVal != 0)
         {
         // Need to free media allocated in other video proxies
         // TBD

         return retVal;  // ERROR
         }
      }

   // Iterate over the audio tracks
   for (int i = 0; i < audioTrackCount; ++i)
      {
      retVal = getAudioTrack(i)->extendAudioTrack(frameCount, allocateMedia);
      if (retVal != 0)
         {
         // Need to free media allocated in video proxies and other audio tracks
         // TBD

         return retVal;  // ERROR
         }
      }

   // Iterate over the time tracks
   int timeTrackCount = getTimeTrackCount();
   for (int i = 0; i < timeTrackCount; ++i)
      {
      retVal = getTimeTrack(i)->extendTimeTrack(frameCount);
      if (retVal != 0)
         {
         // Need to free media allocated in video proxies and other audio tracks
         // TBD

         return retVal; // ERROR
         }
      }

   // Adjust out timecode of clip
   outTimecode = outTimecode + frameCount;

   // Write Clip (.clp) file
   retVal = writeFile();
   if (retVal != 0)
      {
      // Need to free media allocated in video proxies and other audio tracks
      // TBD

      return retVal;  // ERROR: Failed to write the Clip File
      }

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    shortenClip
//
// Description: Remove frames from the end of the clip.  Removes frames
//              from all video proxies, audio tracks and audio tracks.
//              Media storage used by the deleted clips is released.
//
// Arguments:   int frameCount                 Number of frames to remove
//
// Returns:     0 = Success
//
//------------------------------------------------------------------------
int CClip::shortenClip(int frameCount)
{
   int retVal;

   // Iterate over the video proxies
   int videoProxyCount = getVideoProxyCount();
   for (int i = 0; i < videoProxyCount; ++i)
      {
      retVal = getVideoProxy(i)->shortenVideoProxy(frameCount);
      if (retVal != 0)
         return retVal;  // ERROR
      }

   // Iterate over the audio tracks
   int audioTrackCount = getAudioTrackCount();
   for (int i = 0; i < audioTrackCount; ++i)
      {
      retVal = getAudioTrack(i)->shortenAudioTrack(frameCount);
      if (retVal != 0)
         return retVal;  // ERROR
      }

   // Iterate over the time tracks
   int timeTrackCount = getTimeTrackCount();
   for (int i = 0; i < timeTrackCount; ++i)
      {
      retVal = getTimeTrack(i)->shortenTimeTrack(frameCount);
      if (retVal != 0)
         return retVal; // ERROR
      }

   // Adjust out timecode of clip
   outTimecode = outTimecode + (-1 * frameCount);

   // Write Clip (.clp) file
   retVal = writeFile();
   if (retVal != 0)
      return retVal;

   return 0;
}

// free all allocated clip audio media
int CClip::freeClipAudio()
{
    int retVal;

    // iterate over audio tracks; getMediaAllocationFrameCount;
    // releaseMediaStorage
    int audioTrackCount = getAudioTrackCount();
    for (int i = 0; i < audioTrackCount; ++i)
        {
        CAudioFrameList *frameList =
            getAudioTrack(i)->getAudioFrameList(FRAMING_SELECT_VIDEO);
        int frameCount = frameList->getTotalFrameCount();

        retVal = getAudioTrack(i)->releaseMediaStorage(0, frameCount);
        if (retVal != 0)
            return retVal;  // ERROR
        }

    return 0;
}


// free all allocated clip video media
int CClip::freeClipVideo()
{
    int retVal;

    // iterate over video tracks; getMediaAllocationFrameCount;
    // releaseMediaStorage
    int videoProxyCount = getVideoProxyCount();
    for (int i = 0; i < videoProxyCount; ++i)
        {
        CVideoFrameList *frameList =
            getVideoProxy(i)->getVideoFrameList(FRAMING_SELECT_VIDEO);
        int frameCount = frameList->getTotalFrameCount();

        retVal = getVideoProxy(i)->releaseMediaStorage(0, frameCount);
        if (retVal != 0)
            return retVal;  // ERROR
        }

    return 0;
}

//////////////////////////////////////////////////////////////////////
// Assignment Operator =
//////////////////////////////////////////////////////////////////////

CClip& CClip::operator=(const CClip &rhs)
{
   // Assignment operator is private and should not be used
   return *this;
}

//////////////////////////////////////////////////////////////////////
// Accessor Functions
//////////////////////////////////////////////////////////////////////

const CAudioFormat* CClip::getAudioFormat(int audioTrackIndex) const
{
   CAudioTrack *audioTrack = getAudioTrack(audioTrackIndex);
   if (audioTrack == 0)
      return 0; // Audio Track Index is out-of-range

   return audioTrack->getAudioFormat();
}

CAudioFrameList* CClip::getAudioFrameList(int audioTrackIndex,
                                            int framingIndex) const
{
   CAudioTrack *audioTrack = getAudioTrack(audioTrackIndex);
   if (audioTrack == 0)
      return 0; // Audio Track Index is out-of-range

   return audioTrack->getAudioFrameList(framingIndex);
}

int CClip::getAudioFramingCount() const
{
   return audioFramingInfoList.getFramingCount();
}

CAudioTrack* CClip::getAudioTrack(int audioTrackIndex) const
{
   return audioTrackList.getAudioTrack(audioTrackIndex);
}

int CClip::getAudioTrackCount() const
{
   return audioTrackList.getTrackCount();
}

string CClip::getBinPath() const
{
   return binPath;
}

string CClip::getClipFileName() const
{
   CBinManager binManager;
   return binManager.makeClipFileName(binPath, clipName);
}

string CClip::getClipFileNameWithExt() const
{
   CBinManager binManager;
   return binManager.makeClipFileNameWithExt(binPath, clipName);
}

CLIP_GUID CClip::getClipGUID() const
{
   return *clipGUID;
}

int CClip::getNewClipVersion()
{
   clipVersion++;

   int retVal = writeFile();
   if (retVal != 0)
      return -1;

   return clipVersion;
}

string CClip::getClipGUIDStr() const
{
   string strGUID;

   if (clipGUID == 0)
     strGUID = "";
   else
     strGUID = guid_to_string (*clipGUID);

   return strGUID;
}

int CClip::compareClipGUID(const CLIP_GUID &trialGUID)
{
// Compare clip's GUID with caller's GUID "lexically" and return
//       -1   trialGUID is lexically before clipGUID
//        0   trialGUID is equal to clipGUID
//        1   trialGUID is lexically after clipGUID
//  Note:   lexical ordering is not temporal ordering!

   return guid_compare(trialGUID, *clipGUID);
}

/* static */
void CClip::changeClipGUID(const string& binPath,
                           const string& clipName)
{
   // Change the GUID for the clip. Must do this without opening the clip!!
   // Should assert that clip is not open presently!!
   CBinManager binMgr;
   string dotClpFilePath = binMgr.makeClipFileNameWithExt(binPath, clipName);
   CIniFile *dotClpFile = CreateIniFile(dotClpFilePath);
   if (dotClpFile == 0)
      {
      TRACE_0(errout << "ERROR: CClip::changeClipGUID: "
                     << "Could not open Clip File " << dotClpFilePath
                     << endl << "       Because " << strerror(errno));
      return;
      }

   // Generate and write Clip GUID
   CLIP_GUID *newClipGUID = new CLIP_GUID;
   string clipGUIDStr = guid_to_string(*newClipGUID);
   dotClpFile->WriteString(clipInfoSectionName, clipGUIDKey, clipGUIDStr);
   delete newClipGUID;

   // Close the clip file (writes it, doesn't really delete it!!)
   if (!DeleteIniFile(dotClpFile))
      {
      TRACE_0(errout << "ERROR: CClip::changeClipGUID: " << endl
                     << "      Could not write Clip File " << dotClpFilePath
                     << endl << "      Because " << strerror(errno));
      }
}

string CClip::getClipName() const
{
   return clipName;
}

string CClip::getClipPathName() const
{
   CBinManager binManager;
   return binManager.makeClipPath(binPath, clipName);
}

string CClip::getRelativeClipPath() const
{
   return CPMPGetPathRelativeToBinRoot(getClipPathName());
}

string CClip::getCreateDateTime() const
{
   return createDateTime;
}

string CClip::getDescription() const
{
   return description;
}

ECDIngestType CClip::getCDIngestType() const
{
   return cdIngestType;
}

string CClip::getDiskScheme() const
{
   return diskScheme_;
}

string CClip::getArchiveDateTime() const
{
   return archiveDateTime;
}

//------------------------------------------------------------------------
//
// Function:     getDurationTimecode
//
// Description:
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
CTimecode CClip::getDurationTimecode() const
{
   // Create a CTimecode instance durationTimecode that has the same
   // framesPerSecond and flags as the Clip's In timecode.  Don't care
   // about the actual In time
   CTimecode durationTimecode = getInTimecode();

   // Force duration timecode to be non-dropframe so that
   // absolute time is reported as the duration
   durationTimecode.setFlags(durationTimecode.getFlags() & ~DROPFRAME);

   // The frame count is the difference between the in and out timecodes
   int frameCount = getOutTimecode().getAbsoluteTime()
                    - getInTimecode().getAbsoluteTime();

   // If frame count is less than 0 (in timecode after out timecode)
   // then force frame count to zero
   if (frameCount < 0)
      frameCount = 0;

   // Set duration timecode with the frame count
   durationTimecode.setAbsoluteTime(frameCount);

   return durationTimecode;

} // getDurationTimecode

const CImageFormat* CClip::getImageFormat(int proxyIndex) const
{
   CVideoProxy *videoProxy = getVideoProxy(proxyIndex);
   if (videoProxy == 0)
      {
      // Proxy Index is out-of-range
      return 0;
      }

   return videoProxy->getImageFormat();
}

void CClip::getInTimecode(CTimecode &tc) const
{
  tc = inTimecode;
}

CTimecode CClip::getInTimecode() const
{
   return(inTimecode);
}

void CClip::getOutTimecode(CTimecode &tc) const
{
   tc = outTimecode;
}

bool CClip::getLockRecordFlag() const
{
   return lockRecordFlag;
}

CTimecode CClip::getOutTimecode() const
{
   return(outTimecode);
}

int CClip::getTimeTrackCount() const
{
   return timeTrackList.getTrackCount();
}

CTimeTrack* CClip::getTimeTrack(int timeTrackIndex) const
{
   return timeTrackList.getTrack(timeTrackIndex);
}

CTrack* CClip::getTrack(ETrackType trackType, int trackIndex)
{
   CTrack *track;

   // Return the pointer to the CTrack subtype (Audio Track or Video Proxy)
   // that the caller has selected
   switch (trackType)
      {
      case TRACK_TYPE_AUDIO :
         // Return Audio Track (CAudioTrack) instance pointer
         track = getAudioTrack(trackIndex);
         break;

      case TRACK_TYPE_VIDEO :
         // Return Video Proxy (CVideoProxy) instance pointer
         track = getVideoProxy(trackIndex);
         break;

      default :
         // Unexpected track type, return NULL
         track = 0;
         theError.set(CLIP_ERROR_INVALID_TRACK_TYPE);
         break;
      }

   return track;
}
CVideoProxy* CClip::getVideoProxy(int proxyIndex) const
{
   return videoProxyList.getVideoProxy(proxyIndex);
}

int CClip::getVideoProxyCount() const
{
   return videoProxyList.getVideoProxyCount();
}

CVideoFrameList* CClip::getVideoFrameList(int proxyIndex,
                                            int framingIndex) const
{
   CVideoProxy *videoProxy = getVideoProxy(proxyIndex);
   if (videoProxy == 0)
      {
      // Proxy Index is out-of-range
      return 0;
      }

   return videoProxy->getVideoFrameList(framingIndex);
}

int CClip::getVideoFramingCount() const
{
   return videoFramingInfoList.getFramingCount();
}

//////////////////////////////////////////////////////////////////////
// AppId Search Functions
//////////////////////////////////////////////////////////////////////

CAudioTrack* CClip::findAudioTrack(const string& appId) const
{
   return audioTrackList.findAudioTrack(appId);
}

int CClip::findAudioTrackIndex(const string& appId) const
{
   return audioTrackList.findAudioTrackIndex(appId);
}

CTimeTrack* CClip::findTimeTrack(const string& appId) const
{
   return timeTrackList.findTrack(appId);
}

int CClip::findTimeTrackIndex(const string& appId) const
{
   return timeTrackList.findTrackIndex(appId);
}

CVideoProxy* CClip::findVideoProxy(const string& appId) const
{
   return videoProxyList.findVideoProxy(appId);
}

int CClip::findVideoProxyIndex(const string& appId) const
{
   return videoProxyList.findVideoProxyIndex(appId);
}

//////////////////////////////////////////////////////////////////////
// Mutator Functions
//////////////////////////////////////////////////////////////////////

void CClip::setBinPath(const string &newBinPath)
{
   binPath = newBinPath;
}

void CClip::setClipName(const string &newClipName)
{
   clipName = newClipName;
}

void CClip::setCreateDateTime(const string &newDate)
{
   createDateTime = newDate;

   // I'm too lazy to create a new call, so stashing this
   // here to reset the variable that helps us to detect
   // that someone else chenged the .clp out from under us!
   // This will be iniitalized at the first writeFile().
   // Sorry!
   _clipFileModifiedTime = 0;
}

void CClip::setArchiveDateTime(const string &newDate)
{
   archiveDateTime = newDate;
}

void CClip::setDescription(const string &newDescription)
{
   description = newDescription;
}

void CClip::setCDIngestType(const ECDIngestType &newCDIngestType)
{
   cdIngestType = newCDIngestType;
}

void CClip::setDiskScheme(const string &newDiskScheme)
{
   diskScheme_ = newDiskScheme;
}

void CClip::setInTimecode(const CTimecode &tc)
{
   // Caller's timecode is assumed to be well-formed
   inTimecode = tc;
}

void CClip::setOutTimecode(const CTimecode &tc)
{
   // Caller's timecode is assumed to be well-formed
   outTimecode = tc;
}

void CClip::setLockRecordFlag(bool newFlag)
{
   lockRecordFlag = newFlag;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    getLastMediaAction
//
// Description: Gets the Last Media Action status for a given Audio Track
//              or Video Proxy (Marked, Created, RecToCom, etc).
//
// Arguments:   ETrackType audioVideoSelect   Select Audio or Video
//                                              TRACK_TYPE_AUDIO  Audio Track
//                                              TRACK_TYPE_VIDEO  Video Proxy
//              int trackIndex                Index of Audio Track or Video
//                                            Proxy.  Both indices start at
//                                            zero.  Video Proxy 0 is always
//                                            the main video.
//
// Returns:     Media status value.  If function fails, then
//              MEDIA_STATUS_UNKNOWN is returned
//
//------------------------------------------------------------------------
EMediaStatus CClip::getLastMediaAction(ETrackType audioVideoSelect,
                                        int trackIndex)
{
   // Get the pointer to the CTrack subtype (Audio Track or Video Proxy)
   // that the caller has selected
   CTrack *track = getTrack(audioVideoSelect, trackIndex);
   if (track == 0)
      return MEDIA_STATUS_UNKNOWN; // ERROR: Invalid Track Type or Track Index

   // Get the Last Media Action in the Audio Track or Video Proxy in memory
   return track->getLastMediaAction();
}

string CClip::getLastMediaActionString(ETrackType audioVideoSelect,
                                        int trackIndex)
{
   // Get the pointer to the CTrack subtype (Audio Track or Video Proxy)
   // that the caller has selected
   CTrack *track = getTrack(audioVideoSelect, trackIndex);
   if (track == 0)
      return ""; // ERROR: Invalid Track Type or Track Index

   // Get the string for the Last Media Action in the Audio Track or
   // Video Proxy in memory
   return track->getLastMediaActionString();
}

void CClip::notifyMediaStatusChanged(ETrackType trackType, int trackNumber)
{
   TRACE_2(errout << "INFO: Clip " << getClipName() << "in bin "
                  << getBinPath() << endl
                  << "       was notified that status changed for "
                  << ((trackType == 1)? "audio" : "video")
                  << " track " << trackNumber);

   // Make sure the .clp file is always current
   writeFile();

   // Tell anyone who's interested that the clip status changed
   // Screen out all but changes to the main video track
   if (trackType == TRACK_TYPE_VIDEO && trackNumber == VIDEO_SELECT_NORMAL)
      NotifyClipStatusChanged(binPath, clipName);
}

/* static */
void CClip::NotifyClipStatusChanged(const string &binPath,
                                    const string &clipName)
{
   if (statusChangeNotifyHandler != NULL)
      (*statusChangeNotifyHandler)(binPath, clipName);
}

/* static */
void CClip::RegisterForClipStatusChangeNotifications(
                                    StatusChangeNotifyHandlerType newHandler)
{
    statusChangeNotifyHandler = newHandler;
}

/* static */
void CClip::UnregisterForClipStatusChangeNotifications()
{
    statusChangeNotifyHandler = NULL;
}

int CClip::setCadenceRepairFlag (bool bArg)
{
  // get the track associated with TRACK_TYPE_VIDEO and VIDEO_SELECT_NORMAL
   CTrack *track = getTrack(TRACK_TYPE_VIDEO, VIDEO_SELECT_NORMAL);
   if (track == 0)
      return MEDIA_STATUS_UNKNOWN; // ERROR: Invalid Track Type or Track Index

   CIniFile *clipIniFile = getClipIniFile();
   if (clipIniFile == 0)
   {
      DBTRACE(this->getClipFileNameWithExt());
      return CLIP_ERROR_CANNOT_OPEN_OR_CREATE_CLIP_FILE;
   }


   // Set the cadence repair flag in the track
   int iRet = track->setCadenceRepairFlag(clipIniFile,
	videoProxyList.getVideoSectionName(), bArg);


   // close the ini file
   delete clipIniFile;

   return iRet;
}

bool CClip::getCadenceRepairFlag ()
{
  // get the track associated with TRACK_TYPE_VIDEO and VIDEO_SELECT_NORMAL
   CTrack *track = getTrack(TRACK_TYPE_VIDEO, VIDEO_SELECT_NORMAL);
   if (track == 0)
      return false;

   // Get the cadence repair flag in the track
   return track->getCadenceRepairFlag();
}

//------------------------------------------------------------------------
//
// Function:    setRenderInfo
//
// Description: Set various information regarding how the clip was rendered
//
// Arguments:   string sourceClipName  Full name of the render source clip
//              int sourceFrameIndex   The start index in the source clip
//              int framingType        The render framing type (video vs film)
//
// Returns:     0 on success
//------------------------------------------------------------------------

int CClip::setRenderInfo(const string &sourceClipName,
                         int sourceFrameIndex, int framingType)
{
   // Open the .clp file
   CIniFile *clipIniFile = getClipIniFile();
   if (clipIniFile == 0)
   {
      DBTRACE(this->getClipFileNameWithExt());
      return CLIP_ERROR_CANNOT_OPEN_OR_CREATE_CLIP_FILE;
   }

   // Set the source clip name
   clipIniFile->WriteString(renderInfoSectionName, sourceClipNameKey,
                             sourceClipName);

   // Set the render framing type value
   clipIniFile->WriteInteger(renderInfoSectionName, renderFramingTypeKey,
                             framingType);

   // Set the source frame index value
   clipIniFile->WriteInteger(renderInfoSectionName, sourceFrameIndexKey,
                             sourceFrameIndex);

   // Close the .clp file
   delete clipIniFile;

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    getRenderInfo
//
// Description: Get various information regarding how the clip was rendered
//
// Arguments:   string &sourceClipName  Reference to source clip name output
//              int &sourceFrameIndex   Reference to source frame index output
//              int &framingType        Reference to framing type output
//
// Returns:     0 on success
//------------------------------------------------------------------------

int CClip::getRenderInfo(string &sourceClipName, int &sourceFrameIndex,
                         int &framingType)
{
   int retVal = 0;

   // Open the .clp file
   CIniFile *clipIniFile = getClipIniFile();
   if (clipIniFile == 0)
   {
      DBTRACE(this->getClipFileNameWithExt());
      return CLIP_ERROR_CANNOT_OPEN_OR_CREATE_CLIP_FILE;
   }
                                   // ERROR: clip IniFile is not available

   // Get the source clip name
   sourceClipName = clipIniFile->ReadString(renderInfoSectionName,
                                   sourceClipNameKey, "");
   if (sourceClipName.empty())
      retVal = CLIP_ERROR_RENDER_INFO_NOT_AVAILABLE;
   else
      {
      // Get the render framing type value
      framingType = clipIniFile->ReadInteger(renderInfoSectionName,
                                             renderFramingTypeKey,
                                             FRAMING_SELECT_FILM);
      // Get the source frame index value
      sourceFrameIndex = clipIniFile->ReadInteger(renderInfoSectionName,
                                                  sourceFrameIndexKey, 0);
      }

   // Close the .clp file
   delete clipIniFile;

   return retVal;
}


//------------------------------------------------------------------------
//
// Function:    setAlphaMatteTypes
//
// Description: Set the alpha matte type fields in the video proxy image
//              format section of the clip file
//
// Arguments:   EAlphaMatteType alphaType    The Alpha Matte Type to set
//              EAlphaMatteType alphaHiddenType
//                                           The Hidden Alpha Matte Type to set
//
// Returns:     0 on success
//------------------------------------------------------------------------

int CClip::setAlphaMatteTypes (EAlphaMatteType newAlphaMatteType,
                               EAlphaMatteType newAlphaMatteHiddenType)
{
   int retVal = 0;

   // Get the main video proxy
   CVideoProxy *videoProxy = getVideoProxy(VIDEO_SELECT_NORMAL);
   if (videoProxy == 0)
      return MEDIA_STATUS_UNKNOWN; // ERROR: Invalid Track Index

   // Open the .clp file
   CIniFile *clipIniFile = getClipIniFile();
   if (clipIniFile == 0)
   {
      DBTRACE(this->getClipFileNameWithExt());
      return CLIP_ERROR_CANNOT_OPEN_OR_CREATE_CLIP_FILE;
   }

   // Construct Video Proxy Information section prefix such as
   // "VideoProxyInfo0"
   MTIostringstream trackInfoSection;
   trackInfoSection << CClipInitInfo::videoProxySectionName
                    << "Info"   // QQQ magic string
                    << videoProxy->getProxyNumber();

   // Update the .clp file
   CImageFormat imageFormat;
   retVal = imageFormat.readSection(clipIniFile, trackInfoSection.str());
   if (retVal == 0)
      {
      imageFormat.setAlphaMatteType(newAlphaMatteType);
      imageFormat.setAlphaMatteHiddenType(newAlphaMatteHiddenType);
      retVal = imageFormat.writeSection(clipIniFile, trackInfoSection.str());
      }

   // Fix the in-memory copy of the main video image format
   CImageFormat clipImageFormat = *videoProxy->getImageFormat();
   clipImageFormat.setAlphaMatteType(newAlphaMatteType);
   clipImageFormat.setAlphaMatteHiddenType(newAlphaMatteHiddenType);
   videoProxy->setImageFormat(clipImageFormat);

   // Close the .clp file
   delete clipIniFile;

   return retVal;
}


//------------------------------------------------------------------------
//
// Function:    getAlphaMatteTypes
//
// Description: Get the alpha matte type fields from the video proxy
//              image format section of the clip file
//
// Arguments:   EAlphaMatteType alphaType   The Alpha Matte Type value
//                                          (output; passed by reference)
//              EAlphaMatteType alphaHiddenType
//                                          The Hidden Alpha Matte Type value
//                                          (output; passed by reference)
//
// Returns:     0 on success
//------------------------------------------------------------------------

int CClip::getAlphaMatteTypes (EAlphaMatteType &alphaMatteType,
                               EAlphaMatteType &alphaMatteHiddenType)
{
   int retVal = 0;

   // Get the main video proxy
   CVideoProxy *videoProxy = getVideoProxy(VIDEO_SELECT_NORMAL);
   if (videoProxy == 0)
      return MEDIA_STATUS_UNKNOWN; // ERROR: Invalid Track Index

   // Open the .clp file
   CIniFile *clipIniFile = getClipIniFile();
   if (clipIniFile == 0)
   {
      DBTRACE(this->getClipFileNameWithExt());
      return CLIP_ERROR_CANNOT_OPEN_OR_CREATE_CLIP_FILE;
   }

   // Construct Video Proxy Information section prefix such as
   // "VideoProxyInfo0"
   MTIostringstream trackInfoSection;
   trackInfoSection << CClipInitInfo::videoProxySectionName
                    << "Info" // QQQ magic string
                    << videoProxy->getProxyNumber();

   // Get the alpha type values
   CImageFormat imageFormat;
   retVal = imageFormat.readSection(clipIniFile, trackInfoSection.str());
   if (retVal == 0)
      {
      alphaMatteType = imageFormat.getAlphaMatteType();
      alphaMatteHiddenType = imageFormat.getAlphaMatteHiddenType();
      }

   // Close the .clp file
   delete clipIniFile;

   return retVal;
}


//------------------------------------------------------------------------
//
// Function:    setPixelComponents
//
// Description: Set the Pixel Components field in the video proxy image
//              format section of the clip file
//
// Arguments:   EPixelComponents newPixelComponents    The pixel components to set
//
// Returns:     0 on success
//------------------------------------------------------------------------

int CClip::setPixelComponents(EPixelComponents newPixelComponents)
{
   int retVal = 0;

   // Get the main video proxy
   CVideoProxy *videoProxy = getVideoProxy(VIDEO_SELECT_NORMAL);
   if (videoProxy == 0)
   {
      // ERROR: Invalid Track Index
      return MEDIA_STATUS_UNKNOWN;
   }

   // Open the .clp file
   CIniFile *clipIniFile = getClipIniFile();
   if (clipIniFile == 0)
   {
      DBTRACE(this->getClipFileNameWithExt());
      return CLIP_ERROR_CANNOT_OPEN_OR_CREATE_CLIP_FILE;
   }

   // Construct Video Proxy Information section prefix such as
   // "VideoProxyInfo0"
   MTIostringstream trackInfoSection;
   trackInfoSection << CClipInitInfo::videoProxySectionName
                    << "Info"   // QQQ magic string
                    << videoProxy->getProxyNumber();

   // Update the .clp file
   CImageFormat imageFormat;
   retVal = imageFormat.readSection(clipIniFile, trackInfoSection.str());
   if (retVal == 0)
   {
      imageFormat.setPixelComponents(newPixelComponents);
      retVal = imageFormat.writeSection(clipIniFile, trackInfoSection.str());
   }

   // Fix the in-memory copy of the main video image format
   CImageFormat clipImageFormat = *videoProxy->getImageFormat();
   clipImageFormat.setPixelComponents(newPixelComponents);
   videoProxy->setImageFormat(clipImageFormat);

   // Close the .clp file
   delete clipIniFile;

   return retVal;
}


//------------------------------------------------------------------------
//
// Function:    getPixelComponents
//
// Description: Get the Pixel Components field from the video proxy
//              image format section of the clip file
//
// Arguments:   EPixelComponents newPixelComponents    The returned pixel components
//                                                     (output; passed by reference)
//
// Returns:     0 on success
//------------------------------------------------------------------------

int CClip::getPixelComponents(EPixelComponents &pixelComponents)
{
   int retVal = 0;

   // Get the main video proxy
   CVideoProxy *videoProxy = getVideoProxy(VIDEO_SELECT_NORMAL);
   if (videoProxy == 0)
      return MEDIA_STATUS_UNKNOWN; // ERROR: Invalid Track Index

   // Open the .clp file
   CIniFile *clipIniFile = getClipIniFile();
   if (clipIniFile == 0)
   {
      DBTRACE(this->getClipFileNameWithExt());
      return CLIP_ERROR_CANNOT_OPEN_OR_CREATE_CLIP_FILE;
   }

   // Construct Video Proxy Information section prefix such as
   // "VideoProxyInfo0"
   MTIostringstream trackInfoSection;
   trackInfoSection << CClipInitInfo::videoProxySectionName
                    << "Info" // QQQ magic string
                    << videoProxy->getProxyNumber();

   // Get the pixel components.
   CImageFormat imageFormat;
   retVal = imageFormat.readSection(clipIniFile, trackInfoSection.str());
   if (retVal == 0)
   {
      pixelComponents = imageFormat.getPixelComponents();
   }

   // Close the .clp file
   delete clipIniFile;

   return retVal;
}


//------------------------------------------------------------------------
//
// Function:    getMainFrameListIndex
//
// Description: Identifies the "main" frame list - only used for version clips
//
// Arguments:   None
//
// Returns:     the index of the "main" frame list (0 or 1)
//------------------------------------------------------------------------

int CClip::getMainFrameListIndex()
{
   // Make sure videoFramingInfoList was initialized first!!
   return videoFramingInfoList.getMainFrameListIndex();
}


//------------------------------------------------------------------------
//
// Function:    setMainFrameListIndex
//
// Description: Identifies the "main" frame list - only used for version clips
//
// Arguments:   the index of the "main" frame list (0 or 1)
//
// Returns:     nothing
//------------------------------------------------------------------------

void CClip::setMainFrameListIndex(int newMainFrameListIndex)
{
   // Open the .clp file
   CIniFile *clipIniFile = getClipIniFile();
   if (clipIniFile == 0)
      return;
   clipIniFile->ReadOnly(false);

   // Write the track number
   videoFramingInfoList.setAndWriteMainFrameListIndex(clipIniFile,
                                                      newMainFrameListIndex);

   // Close the .clp file
   delete clipIniFile;
}


//------------------------------------------------------------------------
//
// Function:    getLockedFrameListIndex
//
// Description: Identifies the "locked" frame list - only used for versions
//
// Arguments:   None
//
// Returns:     the index of the "locked" frame list (0 or 1) or -1 for none
//------------------------------------------------------------------------

int CClip::getLockedFrameListIndex()
{
   // Make sure videoFramingInfoList was initialized first!!
   return videoFramingInfoList.getLockedFrameListIndex();
}


//------------------------------------------------------------------------
//
// Function:    setLockedFrameListIndex
//
// Description: Identifies the "locked" frame list - only used for versions
//
// Arguments:   the index of the "locked" frame list (0 or 1)  or -1 for none
//
// Returns:     nothing
//------------------------------------------------------------------------

void CClip::setLockedFrameListIndex(int newLockedFrameListIndex)
{
   // Open the .clp file
   CIniFile *clipIniFile = getClipIniFile();
   if (clipIniFile == 0)
      return;
   clipIniFile->ReadOnly(false);

   // Write the track number
   videoFramingInfoList.setAndWriteLockedFrameListIndex(clipIniFile,
                                                      newLockedFrameListIndex);

   // Close the .clp file
   delete clipIniFile;
}

//------------------------------------------------------------------------
//
// Function:    isMediaDestroyed
//
// Description: [VERSION CLIP HACK] Workaround for a video media double
//              deletion bug - find out if the clip's video media has
//              already been destroyed
//
// Arguments:   None
//
// Returns:     true if the clip's video media has been deleted
//------------------------------------------------------------------------

bool CClip::isMediaDestroyed(void)
{
   bool mediaDestroyed = true;

   CIniFile *clipIniFile = getClipIniFile();
   if (clipIniFile != 0)
      {
      mediaDestroyed = clipIniFile->ReadBool(clipInfoSectionName,
                                             mediaDestroyedKey, false);
      delete clipIniFile;
      }

   return mediaDestroyed;
}

//------------------------------------------------------------------------
//
// Function:    setMediaDestroyed
//
// Description: [VERSION CLIP HACK] Inform the clip that its video media
//              has been destroyed, so we can avoid double deletion
//              BONUS HACK: Also prevents us from destroying the parent's
//              audio media, which we share!
//
// Arguments:   the index of the "locked" frame list (0 or 1)  or -1 for none
//
// Returns:     nothing
//------------------------------------------------------------------------

void CClip::setMediaDestroyed(void)
{
   CIniFile *clipIniFile = getClipIniFile();
   if (clipIniFile != 0)
      {
      clipIniFile->WriteBool(clipInfoSectionName, mediaDestroyedKey, true);
      delete clipIniFile;
      }
}


//------------------------------------------------------------------------
//
// Function:    getRecordToComputerStatus
//
// Description: Gets the Record-to-Computer status for a given Audio Track
//              or Video Proxy
//
// Arguments:   ETrackType audioVideoSelect   Select Audio or Video
//                                              TRACK_TYPE_AUDIO  Audio Track
//                                              TRACK_TYPE_VIDEO  Video Proxy
//              int trackIndex                Index of Audio Track or Video
//                                            Proxy.  Both indices start at
//                                            zero.  Video Proxy 0 is always
//                                            the main video.
//
// Returns:     Record status value.  If function fails, then
//              RECORD_STATUS_INVALID is returned
//
//------------------------------------------------------------------------
ERecordStatus CClip::getRecordToComputerStatus(ETrackType audioVideoSelect,
                                                int trackIndex)
{
   // Get the pointer to the CTrack subtype (Audio Track or Video Proxy)
   // that the caller has selected
   CTrack *track = getTrack(audioVideoSelect, trackIndex);
   if (track == 0)
      return RECORD_STATUS_INVALID; // ERROR: Invalid Track Type or Track Index

   // Get the new Record Status in the Audio Track or Video Proxy in memory
   return track->getRecordToComputerStatus();
}

//------------------------------------------------------------------------
//
// Function:    getRecordToComputerStatus
//
// Description: Gets the Record-to-Computer status for a given Audio Track
//              or Video Proxy
//
//              This is a static member function that takes the name
//              of the clip as an argument.
//
// Arguments:   string& clipName             Clip Name in standard format:
//                                              BinPath/ClipName
//                                              BinPath/ClipName/ClipName
//                                              BinPath/ClipName/ClipName.clp
//              ETrackType audioVideoSelect   Select Audio or Video
//                                              TRACK_TYPE_AUDIO  Audio Track
//                                              TRACK_TYPE_VIDEO  Video Proxy
//              int trackIndex                Index of Audio Track or Video
//                                            Proxy.  Both indices start at
//                                            zero.  Video Proxy 0 is always
//                                            the main video.
//
// Returns:     Record status value.  If function fails, then
//              RECORD_STATUS_INVALID is returned
//
//------------------------------------------------------------------------
ERecordStatus CClip::getRecordToComputerStatus(const string& clipName,
                                                ETrackType audioVideoSelect,
                                                int trackIndex)
{
   int retVal;

   // Open the clip
   CBinManager binMgr;
   auto clip = binMgr.openClip(clipName, &retVal);
   if (retVal != 0)
      return RECORD_STATUS_INVALID; // ERROR: Could not open then clip

   // Get the new Record Status in the Audio Track or Video Proxy in memory
   ERecordStatus status = clip->getRecordToComputerStatus(audioVideoSelect,
                                                          trackIndex);

   binMgr.closeClip(clip);

   return status;
}

//------------------------------------------------------------------------
//
// Function:    getRecordToVTRStatus
//
// Description: Gets the Record-to-VTR status for a given Audio Track
//              or Video Proxy
//
// Arguments:   ETrackType audioVideoSelect   Select Audio or Video
//                                              TRACK_TYPE_AUDIO  Audio Track
//                                              TRACK_TYPE_VIDEO  Video Proxy
//              int trackIndex                Index of Audio Track or Video
//                                            Proxy.  Both indices start at
//                                            zero.  Video Proxy 0 is always
//                                            the main video.
//
// Returns:     Record status value.  If function fails, then
//              RECORD_STATUS_INVALID is returned
//
//------------------------------------------------------------------------
ERecordStatus CClip::getRecordToVTRStatus(ETrackType audioVideoSelect,
                                           int trackIndex)
{
   // Get the pointer to the CTrack subtype (Audio Track or Video Proxy)
   // that the caller has selected
   CTrack *track = getTrack(audioVideoSelect, trackIndex);
   if (track == 0)
      return RECORD_STATUS_INVALID; // ERROR: Invalid Track Type or Track Index

   // Get the new Record Status in the Audio Track or Video Proxy in memory
   return track->getRecordToVTRStatus();
}

//------------------------------------------------------------------------
//
// Function:    getRecordToVTRStatus
//
// Description: Gets the Record-to-VTR status for a given Audio Track
//              or Video Proxy
//
//              This is a static member function that takes the name
//              of the clip as an argument.
//
// Arguments:   string& clipName             Clip Name in standard format:
//                                              BinPath/ClipName
//                                              BinPath/ClipName/ClipName
//                                              BinPath/ClipName/ClipName.clp
//              ETrackType audioVideoSelect   Select Audio or Video
//                                              TRACK_TYPE_AUDIO  Audio Track
//                                              TRACK_TYPE_VIDEO  Video Proxy
//              int trackIndex                Index of Audio Track or Video
//                                            Proxy.  Both indices start at
//                                            zero.  Video Proxy 0 is always
//                                            the main video.
//
// Returns:     Record status value.  If function fails, then
//              RECORD_STATUS_INVALID is returned
//
//------------------------------------------------------------------------
ERecordStatus CClip::getRecordToVTRStatus(const string& clipName,
                                           ETrackType audioVideoSelect,
                                           int trackIndex)
{
   int retVal;

   // Open the clip
   CBinManager binMgr;
   auto clip = binMgr.openClip(clipName, &retVal);
   if (retVal != 0)
      return RECORD_STATUS_INVALID; // ERROR: Could not open then clip

   // Get the new Record Status in the Audio Track or Video Proxy in memory
   ERecordStatus status = clip->getRecordToVTRStatus(audioVideoSelect,
                                                     trackIndex);

   binMgr.closeClip(clip);

   return status;
}

//------------------------------------------------------------------------
//
// Function:    updateRecordToComputerStatus
//
// Description: Sets the Record-to-Computer status for a given Audio Track
//              or Video Proxy and writes out the clip file
//
// Arguments:   ETrackType audioVideoSelect   Select Audio or Video
//                                              TRACK_TYPE_AUDIO  Audio Track
//                                              TRACK_TYPE_VIDEO  Video Proxy
//              int trackIndex                Index of Audio Track or Video
//                                            Proxy.  Both indices start at
//                                            zero.  Video Proxy 0 is always
//                                            the main video.
//              ERecordStatus newRecordStatus New Record Status value.
//
// Returns:     0 = Success
//
//------------------------------------------------------------------------
int CClip::updateRecordToComputerStatus(ETrackType audioVideoSelect,
                                         int trackIndex,
                                         ERecordStatus newRecordStatus)
{
   int retVal;

   // Get the pointer to the CTrack subtype (Audio Track or Video Proxy)
   // that the caller has selected
   CTrack *track = getTrack(audioVideoSelect, trackIndex);
   if (track == 0)
      return theError.getError();  // ERROR: Invalid Track Type or Track Index

   // Set the new Record Status in the Audio Track or Video Proxy in memory
   track->setRecordToComputerStatus(newRecordStatus);

   // Write out the Clip file
   retVal = writeFile();
   if (retVal != 0)
      return retVal; // ERROR

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    updateRecordToComputerStatus
//
// Description: Sets the Record-to-Computer status for a given Audio Track
//              or Video Proxy and writes out the clip file.
//
//              This is a static member function that takes the name
//              of the clip as an argument.
//
// Arguments:   string& clipName             Clip Name in standard format:
//                                              BinPath/ClipName
//                                              BinPath/ClipName/ClipName
//                                              BinPath/ClipName/ClipName.clp
//              ETrackType audioVideoSelect   Select Audio or Video
//                                              TRACK_TYPE_AUDIO  Audio Track
//                                              TRACK_TYPE_VIDEO  Video Proxy
//              int trackIndex                Index of Audio Track or Video
//                                            Proxy.  Both indices start at
//                                            zero.  Video Proxy 0 is always
//                                            the main video.
//              ERecordStatus newRecordStatus New Record Status value.
//
// Returns:     0 = Success
//
//------------------------------------------------------------------------
int CClip::updateRecordToComputerStatus(const string& clipName,
                                         ETrackType audioVideoSelect,
                                         int trackIndex,
                                         ERecordStatus newRecordStatus)
{
   int retVal;

   // Open the clip
   CBinManager binMgr;
   auto clip = binMgr.openClip(clipName, &retVal);
   if (retVal != 0)
      return retVal; // ERROR: Could not open then clip

   // Update the Record-to-Computer status
   retVal = clip->updateRecordToComputerStatus(audioVideoSelect, trackIndex,
                                               newRecordStatus);
   if (retVal != 0)
      {
      binMgr.closeClip(clip);
      return retVal; // ERROR
      }

   binMgr.closeClip(clip);

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    updateRecordToVTRStatus
//
// Description: Sets the Record-to-VTR status for a given Audio Track
//              or Video Proxy and writes out the clip file.
//
// Arguments:   ETrackType audioVideoSelect   Select Audio or Video
//                                              TRACK_TYPE_AUDIO  Audio Track
//                                              TRACK_TYPE_VIDEO  Video Proxy
//              int trackIndex                Index of Audio Track or Video
//                                            Proxy.  Both indices start at
//                                            zero.  Video Proxy 0 is always
//                                            the main video.
//              ERecordStatus newRecordStatus New Record Status value.
//
// Returns:     0 = Success
//
//------------------------------------------------------------------------
int CClip::updateRecordToVTRStatus(ETrackType audioVideoSelect,
                                    int trackIndex,
                                    ERecordStatus newRecordStatus)
{
   int retVal;

   // Get the pointer to the CTrack subtype (Audio Track or Video Proxy)
   // that the caller has selected
   CTrack *track = getTrack(audioVideoSelect, trackIndex);
   if (track == 0)
      return theError.getError();  // ERROR: Invalid Track Type or Track Index

   // Set the new Record Status in the Audio Track or Video Proxy in memory
   track->setRecordToVTRStatus(newRecordStatus);

   // Write out the Clip file
   retVal = writeFile();
   if (retVal != 0)
      return retVal; // ERROR

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    updateRecordToVTRStatus
//
// Description: Sets the Record-to-VTR status for a given Audio Track
//              or Video Proxy and writes out the clip file.
//
//              This is a static member function that takes the name
//              of the clip as an argument.
//
// Arguments:   string& clipName             Clip Name in standard format:
//                                              BinPath/ClipName
//                                              BinPath/ClipName/ClipName
//                                              BinPath/ClipName/ClipName.clp
//              ETrackType audioVideoSelect   Select Audio or Video
//                                              TRACK_TYPE_AUDIO  Audio Track
//                                              TRACK_TYPE_VIDEO  Video Proxy
//              int trackIndex                Index of Audio Track or Video
//                                            Proxy.  Both indices start at
//                                            zero.  Video Proxy 0 is always
//                                            the main video.
//              ERecordStatus newRecordStatus New Record Status value.
//
// Returns:     0 = Success
//
//------------------------------------------------------------------------
int CClip::updateRecordToVTRStatus(const string& clipName,
                                    ETrackType audioVideoSelect,
                                    int trackIndex,
                                    ERecordStatus newRecordStatus)
{
   int retVal;

   // Open the clip
   CBinManager binMgr;
   auto clip = binMgr.openClip(clipName, &retVal);
   if (retVal != 0)
      return retVal; // ERROR: Could not open then clip

   // Update the Record-to-Computer status
   retVal = clip->updateRecordToVTRStatus(audioVideoSelect, trackIndex,
                                          newRecordStatus);
   if (retVal != 0)
      {
      binMgr.closeClip(clip);
      return retVal; // ERROR
      }

   binMgr.closeClip(clip);

   return 0;
}

int CClip::updateMediaModifiedStatus(ETrackType audioVideoSelect,
                                      int trackIndex)
{
   int retVal;

   EMediaStatus lastMediaAction = getLastMediaAction(audioVideoSelect,
                                                     trackIndex);

   if (lastMediaAction != MEDIA_STATUS_MODIFIED)
      {
      // Get the pointer to the CTrack subtype (Audio Track or Video Proxy)
      // that the caller has selected
      CTrack *track = getTrack(audioVideoSelect, trackIndex);
      if (track == 0)
         return theError.getError();  // ERROR: Invalid Track Type or Track Index

      // Set the new Record Status in the Audio Track or Video Proxy in memory
      track->setMediaModifierStatus();

      // Write out the clip file
      retVal = writeFile();
      if (retVal != 0)
         return retVal;
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////
//
// Set specified track status to MEDIA_STATUS_ARCHIVED.  This involves
//   both setting the in-memory copy & writing the .clp file.
//
//   Created: 6/16/2006 - Michael Russell
//
//   Typical Usage:
//      retVal = clip->updateArchivedStatus(TRACK_TYPE_VIDEO, 0);
//   Returns:
//      0        = OK
//      non-zero = error value from getTrack() or writeFile()
//
//////////////////////////////////////////////////////////////////////
int CClip::updateArchivedStatus(ETrackType audioVideoSelect,
                                int trackIndex)
{
   int retVal;

   EMediaStatus lastMediaAction = getLastMediaAction(audioVideoSelect,
                                                     trackIndex);

   // If already that status, then don't bother redoing this work
   if (lastMediaAction != MEDIA_STATUS_ARCHIVED)
      {
      // Get the pointer to the CTrack subtype (Audio Track or Video Proxy)
      // that the caller has selected
      CTrack *track = getTrack(audioVideoSelect, trackIndex);
      if (track == 0)
         return theError.getError();  // ERROR: Invalid Track Type or Track Index

      // Set the new Record Status in the Audio Track or Video Proxy in memory
      track->setArchivedStatus();

      // Write out the clip file
      retVal = writeFile();
      if (retVal != 0)
         return retVal;
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////
//
// Sets the status for each video and audio track to MEDIA_STATUS_CLONED
//
//   Yhis code stolen from updateAllTracksToCreatedStatusAndClearArchiveDate()
//
//   Typical Usage:
//      retVal = clip->updateAllTracksToClonedStatus(binPath, clipName);
//   Returns:
//      0        = OK
//      non-zero = error value from openClipOrClipIni() or
//                 updateClonedStatus()
//
//////////////////////////////////////////////////////////////////////
int CClip::updateAllTracksToClonedStatus(const string& binPath,
                                          const string& clipName)
{
   int            retVal;
   CBinManager    binMgr;
   CClipInitInfo *clipInitInfo;
   clipInitInfo = binMgr.openClipOrClipIni(binPath, clipName, &retVal);

   if (retVal != 0)
      {
      TRACE_0(errout << "ERROR: CClip::updateAllTracksToClonedStatus "
                     << "could not open: "
                     << clipName
                     << " (" << retVal << ")" << endl);
      return retVal;
      }

   // Set all Video Proxies (aka Tracks) to "Cloned"
   int videoProxyCount = clipInitInfo->getClipPtr()->getVideoProxyCount();
   for (int i = 0; i < videoProxyCount; i++)
      {
      retVal = clipInitInfo->getClipPtr()->updateClonedStatus(
                                                TRACK_TYPE_VIDEO, i);

      if (retVal != 0)
         {
         TRACE_0(errout << "ERROR: CClip::updateAllTracksToClonedStatus "
                        << " could not set Video track status to Cloned for "
                        << clipName
                        << " (" << retVal << ")" << endl);
         clipInitInfo->closeClip();   // no longer need opened clip
         return retVal;
         }
      }

   // Set all Audio Tracks to "Cloned"
   int audioTrackCount = clipInitInfo->getClipPtr()->getAudioTrackCount();
   for (int i = 0; i < audioTrackCount; i++)
      {
      retVal = clipInitInfo->getClipPtr()->updateClonedStatus(
                                               TRACK_TYPE_AUDIO, i);

      if (retVal != 0)
         {
         TRACE_0(errout << "ERROR: CClip::updateAllTracksToClonedStatus "
                        << " could not set Audio track status to Cloned for "
                        << clipName
                        << " (" << retVal << ")" << endl);
        clipInitInfo->closeClip();   // no longer need opened clip
        return retVal;
         }
      }

   clipInitInfo->closeClip();   // no longer need opened clip
   delete clipInitInfo;

   return 0;
}

//////////////////////////////////////////////////////////////////////
//
// Sets the status for each video and audio track to MEDIA_STATUS_CREATED
//   and resets the ArchiveDate to the empty string.
//
//   Created: 6/16/2006 - Michael Russell
//
//   Typical Usage:
//      retVal = clip->updateAllTracksToCreatedStatusAndClearArchiveDate(
//                      binPath, clipName);
//   Returns:
//      0        = OK
//      non-zero = error value from openClipOrClipIni() or
//                 updateCreatedStatus()
//
//////////////////////////////////////////////////////////////////////
int CClip::updateAllTracksToCreatedStatusAndClearArchiveDate(
               const string& binPath,
               const string& clipName)
{
   int            retVal;
   CBinManager    binMgr;
   CClipInitInfo *clipInitInfo;
   clipInitInfo = binMgr.openClipOrClipIni(binPath, clipName, &retVal);

   if (retVal != 0)
      {
      TRACE_0(errout << "updateAllTracksToCreatedStatusAndClearArchiveDate "
                     << "could not open: "
                     << clipName
                     << " (" << retVal << ")" << endl);
      return retVal;
      }

   // Set all Video Proxies (aka Tracks) to "Created"
   int videoProxyCount = clipInitInfo->getClipPtr()->getVideoProxyCount();
   for (int i = 0; i < videoProxyCount; i++)
      {
      retVal = clipInitInfo->getClipPtr()->updateCreatedStatus(TRACK_TYPE_VIDEO,
                 i);

      if (retVal != 0)
         {
         TRACE_0(errout << "updateAllTracksToCreatedStatusAndClearArchiveDate "
                        << " could not set Video track status to Created for "
                        << clipName
                        << " (" << retVal << ")" << endl);
         clipInitInfo->closeClip();   // no longer need opened clip
         return retVal;
         }
      }

   // Set all Audio Tracks to "Created"
   int audioTrackCount = clipInitInfo->getClipPtr()->getAudioTrackCount();
   for (int i = 0; i < audioTrackCount; i++)
      {
      retVal = clipInitInfo->getClipPtr()->updateCreatedStatus(TRACK_TYPE_AUDIO,
                 i);

      if (retVal != 0)
         {
         TRACE_0(errout << "updateAllTracksToCreatedStatusAndClearArchiveDate "
                        << " could not set Audio track status to Created for "
                        << clipName
                        << " (" << retVal << ")" << endl);
        clipInitInfo->closeClip();   // no longer need opened clip
        return retVal;
         }
      }

   // Set Archived Date to empty string
   clipInitInfo->getClipPtr()->updateArchiveDateTime("");

   clipInitInfo->closeClip();   // no longer need opened clip
   delete clipInitInfo;

   return 0;
}

//////////////////////////////////////////////////////////////////////
//
// Sets the status for each video and audio track to MEDIA_STATUS_ARCHIVED
//   and sets the ArchiveDate to the present time.
//
//   Created: 6/16/2006 - Michael Russell
//
//   Typical Usage:
//      retVal = clip->updateAllTracksToArchivedStatusAndSetArchiveDate(
//                      binPath, clipName);
//   Returns:
//      0        = OK
//      non-zero = error value from openClipOrClipIni() or
//                 updateArchivedStatus()
//
//////////////////////////////////////////////////////////////////////
int CClip::updateAllTracksToArchivedStatusAndSetArchiveDate(
               const string& binPath,
               const string& clipName)
{
   int            retVal;
   CBinManager    binMgr;
   CClipInitInfo *clipInitInfo;
   clipInitInfo = binMgr.openClipOrClipIni(binPath, clipName, &retVal);

   if (retVal != 0)
      {
      TRACE_0(errout << "updateAllTracksToArchivedStatusAndSetArchiveDate "
                     << "could not open: "
                     << clipName
                     << " (" << retVal << ")" << endl);
      return retVal;
      }

   // Set all Video Proxies (aka Tracks) to "Archived"
   int videoProxyCount = clipInitInfo->getClipPtr()->getVideoProxyCount();
   for (int i = 0; i < videoProxyCount; i++)
      {
      retVal = clipInitInfo->getClipPtr()->updateArchivedStatus(TRACK_TYPE_VIDEO,
                 i);

      if (retVal != 0)
         {
         TRACE_0(errout << "updateAllTracksToArchivedStatusAndSetArchiveDate "
                        << " could not set Video track status to Archived for "
                        << clipName
                        << " (" << retVal << ")" << endl);
         clipInitInfo->closeClip();   // no longer need opened clip
         return retVal;
         }
      }

   // Set all Audio Tracks to "Archived"
   int audioTrackCount = clipInitInfo->getClipPtr()->getAudioTrackCount();
   for (int i = 0; i < audioTrackCount; ++i)
      {
      retVal = clipInitInfo->getClipPtr()->updateArchivedStatus(TRACK_TYPE_AUDIO,
                 i);

      if (retVal != 0)
         {
         TRACE_0(errout << "updateAllTracksToArchivedStatusAndSetArchiveDate "
                        << " could not set Audio track status to Archived for "
                        << clipName
                        << " (" << retVal << ")" << endl);
        clipInitInfo->closeClip();   // no longer need opened clip
        return retVal;
        }
     }

   // Set Archived Date to now
   string now = binMgr.createDateAndTimeString();
   clipInitInfo->getClipPtr()->updateArchiveDateTime(now);

   clipInitInfo->closeClip();   // no longer need opened clip
   delete clipInitInfo;

   return 0;
}

//////////////////////////////////////////////////////////////////////
//
// Set specified track status to MEDIA_STATUS_CREATED.  This involves
//   both setting the in-memory copy & writing the .clp file.
//
//   Created: 6/16/2006 - Michael Russell
//
//   Typical Usage:
//      retVal = clip->updateCreatedStatus(TRACK_TYPE_VIDEO, 0);
//   Returns:
//      0        = OK
//      non-zero = error value from getTrack() or writeFile()
//
//////////////////////////////////////////////////////////////////////
int CClip::updateCreatedStatus(ETrackType audioVideoSelect,
                                int trackIndex)
{
   int retVal;

   // Get the pointer to the CTrack subtype (Audio Track or Video Proxy)
   // that the caller has selected
   CTrack *track = getTrack(audioVideoSelect, trackIndex);
   if (track == 0)
      return theError.getError();  // ERROR: Invalid Track Type or Track Index

   // Set the new Record Status in the Audio Track or Video Proxy in memory
   track->setCreatedStatus();

   // Write out the clip file
   retVal = writeFile();
   if (retVal != 0)
      return retVal;

   return 0;
}

//////////////////////////////////////////////////////////////////////
//
// Set specified track status to MEDIA_STATUS_CLONED.  This involves
//   both setting the in-memory copy & writing the .clp file.
//
//   Typical Usage:
//      retVal = clip->updateClonedStatus(TRACK_TYPE_VIDEO, 0);
//   Returns:
//      0        = OK
//      non-zero = error value from getTrack() or writeFile()
//
//////////////////////////////////////////////////////////////////////
int CClip::updateClonedStatus(ETrackType audioVideoSelect,
                                int trackIndex)
{
   int retVal;

   // Get the pointer to the CTrack subtype (Audio Track or Video Proxy)
   // that the caller has selected
   CTrack *track = getTrack(audioVideoSelect, trackIndex);
   if (track == 0)
      return theError.getError();  // ERROR: Invalid Track Type or Track Index

   // Set the new Record Status in the Audio Track or Video Proxy in memory
   track->setClonedStatus();

   // Write out the clip file
   retVal = writeFile();
   if (retVal != 0)
      return retVal;

   return 0;
}

//////////////////////////////////////////////////////////////////////
//
// Set specified ArchiveDate to the passed string.  This involves
//   both setting the in-memory copy & writing the .clp file.
//
//   Created: 6/16/2006 - Michael Russell
//
//   Typical Usage:
//      retVal = clip->updateArchiveDateTime("2006:10:08:06:20:00");
//   Returns:
//      0        = OK
//      non-zero = error value from writeFile()
//
//////////////////////////////////////////////////////////////////////
int CClip::updateArchiveDateTime(const string& dateTime)
{
   int retVal;

   setArchiveDateTime(dateTime);

   // Write out the clip file
   retVal = writeFile();
   if (retVal != 0)
      return retVal;

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Update Track Files
//////////////////////////////////////////////////////////////////////

// Update all field list and time track files
// The file I/O buffer is internally allocated and release within the call.
int CClip::updateAllTrackFiles()
{
   return updateAllTrackFiles(0, -1, 0, 0);
}

//------------------------------------------------------------------------
//
// Function:    updateAllTrackFiles
//
// Description: Update all field list and time track files
//              Caller can supply arguments to specify the range
//              of frames to update.
//              Caller can supply a buffer for file I/O.  This is
//              useful when this function is called many times and the
//              caller does not want repeated allocation/deallocation
//              of memory
//
// Arguments:   int firstFrameIndex   index of first frame to check
//              int frameCount        Number of frames to update.  If
//                                    set to -1, then all frames from
//                                    firstFrameIndex to end of track
//                                    are updated.
//              char *externalBuffer  Pointer to caller's file I/O buffer
//              unsigned int externalBufferSize
//                                    Size of caller's buffer, in bytes
//              If either buffer argument is zero, then the buffer is
//              internally allocated and released within the call.
//
// Returns:     0 = Success
//
//------------------------------------------------------------------------
int CClip::updateAllTrackFiles(int firstFrameIndex, int frameCount,
                               char *externalBuffer,
                               unsigned int externalBufferSize)
{
   int retVal;
   int index;


   TRACE_1(errout << "updateAllTrackFiles(index=" << firstFrameIndex << ","
                  << " count=" << frameCount << ")");

   // Update the field list files for each video proxy
   for (index = 0; index < getVideoProxyCount(); ++index)
      {
      CVideoProxy *videoProxy = getVideoProxy(index);
      if (videoProxy != 0)
         {
         EClipVersion clipVer = videoProxy->getClipVer();
         if (clipVer == CLIP_VERSION_3)
            {
            const CImageFormat *imageFormat = videoProxy->getImageFormat();
            int fieldCount = imageFormat->getFieldCount();
            int updateCount = frameCount;
            if (frameCount != -1)
               updateCount  *= fieldCount;
            retVal = videoProxy->updateFieldListFile(firstFrameIndex*fieldCount,
                                                     updateCount,
                                                     externalBuffer,
                                                     externalBufferSize);
            if (retVal != 0)
               return retVal;
            }
         else if (clipVer == CLIP_VERSION_5L)
            {
            retVal = videoProxy->UpdateMediaStatusTrackFile();
            if (retVal != 0)
               return retVal;
            }
         else
            return CLIP_ERROR_INVALID_CLIP_VERSION;
         }
      }

   // Update the field list files for each audio track
   for (index = 0; index < getAudioTrackCount(); ++index)
      {
      CAudioTrack *audioTrack = getAudioTrack(index);
      if (audioTrack != 0)
         {
         EClipVersion clipVer = audioTrack->getClipVer();
         if (clipVer == CLIP_VERSION_3)
            {
            const CAudioFormat *audioFormat = audioTrack->getAudioFormat();
            int fieldCount = audioFormat->getFieldCount();
            int updateCount = frameCount;
            if (frameCount != -1)
               updateCount  *= fieldCount;
            retVal = audioTrack->updateFieldListFile(firstFrameIndex*fieldCount,
                                                     updateCount,
                                                     externalBuffer,
                                                     externalBufferSize);
            if (retVal != 0)
               return retVal;
            }
         else if (clipVer == CLIP_VERSION_5L)
            {
            retVal = audioTrack->UpdateMediaStatusTrackFile();
            if (retVal != 0)
               return retVal;
            }
         else
            return CLIP_ERROR_INVALID_CLIP_VERSION;
         }
      }

   // Update the Time Track's frame list files
   for(index = 0; index < getTimeTrackCount(); ++index)
      {
      CTimeTrack *timeTrack = getTimeTrack(index);
      if (timeTrack != 0)
         {
         EClipVersion clipVer = timeTrack->getClipVer();
         if (clipVer == CLIP_VERSION_3)
            {
            retVal = timeTrack->updateFrameListFile(firstFrameIndex,
                                                    frameCount,
                                                    externalBuffer,
                                                    externalBufferSize);
            if (retVal != 0)
               return retVal;
            }
         else if (clipVer == CLIP_VERSION_5L)
            {
            retVal = timeTrack->UpdateTrackFile();
            if (retVal != 0)
               return retVal;
            }
         else
            return CLIP_ERROR_INVALID_CLIP_VERSION;
         }
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Clip File Read and Write Functions
//////////////////////////////////////////////////////////////////////

int CClip::readFile()
{
   int retVal;

   CBinManager binMgr;
   string clipFileName = getClipFileNameWithExt();
   if (!DoesFileExist(clipFileName))
      {
      TRACE_1 (errout << "Appears that clip does not exist:  "
                      << getClipFileNameWithExt());
      return CLIP_ERROR_CLIP_FILE_DOES_NOT_EXIST;
      }

   // Grab the "modified time" of the clip file so we can detect staleness.
   _clipFileModifiedTime = GetLastModifiedTime(clipFileName);

   CIniFile *clipIniFile = getClipIniFile();
   if (clipIniFile == 0)
   {
      DBTRACE(this->getClipFileNameWithExt());
      return CLIP_ERROR_CANNOT_OPEN_OR_CREATE_CLIP_FILE;
   }

   // Use STL's auto_ptr template to automatically delete clipIniFile
   // when clipIniFileAutoPtr goes out-of-scope (i.e., when function returns).
   // This save repeating clean-up code.  We also use clipIniFile as
   // a regular pointer.
   std::auto_ptr<CIniFile> clipIniFileAutoPtr(clipIniFile);

   // Make clipIniFile "read only" so it does not get accidentally written
   // back to the .clp file
   clipIniFile->ReadOnly(true);

   // Read the Clip Information Section
   retVal = readClipInfoSection(clipIniFile);
   if (retVal != 0)
      return retVal; // ERROR: Could not read clip information section

   // Read Framing Info List Sections
   retVal = videoFramingInfoList.readVideoFramingInfoListSection(clipIniFile);
   if (retVal != 0)
      return retVal; // ERROR: Could not read the Video Framing Info List

   // Read the Video Proxy Info Sections
   auto sharedThis = CClip::RetrieveSharedPtr(this);
   if (!sharedThis)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   retVal = videoProxyList.readVideoProxyListSection(clipIniFile, sharedThis,
                                                     videoFramingInfoList);
   if (retVal != 0)
      return retVal; // ERROR: Could not read the Video Proxy List or Info

   // Read the Audio Track Sections
   retVal = audioFramingInfoList.readAudioFramingInfoListSection(clipIniFile);
   if (retVal != 0)
      return retVal; // ERROR: Could not read the Audio Framing Info List

   retVal = audioTrackList.readAudioTrackListSection(clipIniFile, sharedThis,
                                                     audioFramingInfoList);
   if (retVal != 0)
      return retVal; // ERROR: Could not read the Audio Track List or Info

   // Read the Timecode/Keykode Track Sections
   retVal = timeTrackList.readTimeTrackListSection(clipIniFile, sharedThis);
   if (retVal != 0)
      return retVal; // ERROR: Could not read the Time Track List or Info

   return 0;
}

// Refresh some information for an open clip from its clip file if the
// clip file's modified time has changed.
int CClip::refreshFromFile()
{
   int retVal;

   CBinManager binMgr;
   string clipFileName = getClipFileNameWithExt();
   if (!DoesFileExist(clipFileName))
   {
      TRACE_1(errout << "Appears that clip does not exist:  " << getClipFileNameWithExt());
      return CLIP_ERROR_CLIP_FILE_DOES_NOT_EXIST;
   }

   // Don't do anything if the clip file wasn't modified.
   if (_clipFileModifiedTime == GetLastModifiedTime(clipFileName))
   {
      return 0;
   }

   // THIS CRASHED WHEN READING THE TRACK SECTIONS.
   // I think we need to clear those out before trying to replace them. QQQ
   // For now I will just refresh the
//   readFile();

   CIniFile *clipIniFile = getClipIniFile();
   if (clipIniFile == 0)
   {
      DBTRACE(this->getClipFileNameWithExt());
      return CLIP_ERROR_CANNOT_OPEN_OR_CREATE_CLIP_FILE;
   }

   // Use STL's auto_ptr template to automatically delete clipIniFile
   // when clipIniFileAutoPtr goes out-of-scope (i.e., when function returns).
   // This save repeating clean-up code.  We also use clipIniFile as
   // a regular pointer.
   std::auto_ptr<CIniFile> clipIniFileAutoPtr(clipIniFile);

   // Make clipIniFile "read only" so it does not get accidentally written
   // back to the .clp file
   clipIniFile->ReadOnly(true);

   // Read the Clip Information Section
   retVal = readClipInfoSection(clipIniFile);
   if (retVal != 0)
      return retVal; // ERROR: Could not read clip information section

//   // Read Framing Info List Sections
//   retVal = videoFramingInfoList.readVideoFramingInfoListSection(clipIniFile);
//   if (retVal != 0)
//      return retVal; // ERROR: Could not read the Video Framing Info List
//
//   // Read the Video Proxy Info Sections
//   auto sharedThis = CClip::RetrieveSharedPtr(this);
//   if (!sharedThis)
//      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;
//
//   retVal = videoProxyList.readVideoProxyListSection(clipIniFile, sharedThis,
//                                                     videoFramingInfoList);
//   if (retVal != 0)
//      return retVal; // ERROR: Could not read the Video Proxy List or Info
//
//   // Read the Timecode/Keykode Track Sections
//   retVal = timeTrackList.readTimeTrackListSection(clipIniFile, sharedThis);
//   if (retVal != 0)
//      return retVal; // ERROR: Could not read the Time Track List or Info

   return 0;
}

// ---------------------------------------------------------------------------

int CClip::CheckVideoResolutionLicense()
{
   int retVal;

   // Iterate over all of the video proxies
   int proxyCount = getVideoProxyCount();
   for (int i = 0; i < proxyCount; ++i)
      {
      CVideoProxy *videoProxy = getVideoProxy(i);
      const CImageFormat *imageFormat = videoProxy->getImageFormat();

      int pixelsPerLine = imageFormat->getPixelsPerLine();
      int linesPerFrame = imageFormat->getLinesPerFrame();
      if (!IsResolutionLicensed(pixelsPerLine, linesPerFrame))
         {
#ifdef _DEBUG
         if (i == 0)
            TRACE_1(errout << "Resolution not licensed for main video, "
                           << pixelsPerLine << "x" << linesPerFrame);
         else
            TRACE_1(errout << "Resolution not licensed for video proxy "
                           << i << ", "
                           << pixelsPerLine << "x" << linesPerFrame);
#endif
         return CLIP_ERROR_MAX_LICENSE_RESOLUTION_EXCEEDED;
         }
      }

   return 0;   // Yeah! all proxies passed
}

// ---------------------------------------------------------------------------

int CClip::renameClip(string newBinPath, string newClipName,
                      bool renameMediaFlag)
{
   int retVal = 0;

   // trim off spaces at beginning and end of newClipName because
   // Windows won't let you create a directory with leading or
   // trailing spaces.
   newClipName = StringTrim(newClipName);

   CBinManager binManager;
   string newClipPath = binManager.makeClipPath(newBinPath, newClipName);

   struct stat statbuf;
   if (stat(newClipPath.c_str(), &statbuf) == 0)
      {
      MTIostringstream ostr;
      static char const *func = "CClip::renameClip";
      ostr << func << " failed.  " << newClipPath << " already exists.";
      TRACE_3(errout << ostr.str());
      theError.set(CLIP_ERROR_CLIP_ALREADY_EXISTS, ostr);
      return CLIP_ERROR_CLIP_ALREADY_EXISTS;
      }

   // contained files
   if (newClipName != clipName)
      {
      retVal = renameClipReferenceTargets(newClipName);
      if (retVal != 0)
         {
         MTIostringstream ostr;
         static char const *func = "CClip::renameClip";
         ostr << func << " failed for clip: " << getClipPathName();
         TRACE_0(errout << ostr.str());
         return retVal;
         }

      // clp file name
      string newClipFilePath = AddDirSeparator(getClipPathName()) + newClipName +
			"." + CBinDir::getClipFileExtension();

      retVal = rename(getClipFileNameWithExt().c_str(),
                      newClipFilePath.c_str());
      if (retVal != 0)
         {
         // ERROR: Could not rename the file.  Cause is in errno
         static char const *func = "CClip::renameClip";
         TRACE_0(errout << func << " could not rename file "
                        << endl;
                 errout << "    " << getClipFileNameWithExt() << endl;
                 errout << " to " << newClipFilePath << endl;
                 errout << " because " << strerror(errno) << endl;);
         return CLIP_ERROR_FILE_RENAME_FAILED;
         }
      }

      _clipFileModifiedTime = GetLastModifiedTime(getClipFileNameWithExt());

   // media files -- can't short-circuit based on clip name not changing
   // because repository files are kept in same hierarchies as bins
   if (renameMediaFlag)
      {
      retVal = renameClipMedia(newBinPath, newClipName);
      if (retVal != 0)
         {
         MTIostringstream ostr;
         static char const *func = "CClip::renameClip";
         ostr << func << " failed for clip: " << getClipPathName();
         TRACE_0(errout << ostr.str());
         return retVal;
         }
      }

   // clip directory
   retVal = rename(getClipPathName().c_str(), newClipPath.c_str());
   if (retVal != 0)
      {
      // ERROR: Could not rename the file.  Cause is in errno
      static char const *func = "CClip::renameClip";
      TRACE_0(errout << func << " could not rename file "
                     << endl;
              errout << "    " << getClipPathName() << endl;
              errout << " to " << newClipPath << endl;
              errout << " because " << strerror(errno) << endl;);
      return CLIP_ERROR_FILE_RENAME_FAILED;
      }

   setBinPath(newBinPath);
   setClipName(newClipName);

   return retVal;
} // rename()

int CClip::writeFile()
{
   int retVal;

   CIniFile *clipIniFile = getClipIniFile();
   if (clipIniFile == 0)
   {
      DBTRACE(this->getClipFileNameWithExt());
      return CLIP_ERROR_CANNOT_OPEN_OR_CREATE_CLIP_FILE;
   }

   // This will delete clipIniFile when it falls out of scope.
   std::unique_ptr<CIniFile> uniqueClipIniFile(clipIniFile);

   // Write the Clip Information Section
   retVal = writeClipInfoSection(clipIniFile);
   if (retVal != 0)
      return retVal; // ERROR: Could not write the clip information section

   // Set the Video Framing Info Sections
   retVal = videoFramingInfoList.writeVideoFramingInfoListSection(clipIniFile);
   if (retVal != 0)
      return retVal; // ERROR: Could not write the Framing List or Framing Information

   // Set the Video Proxy Info Sections
   retVal = videoProxyList.writeVideoProxyListSection(clipIniFile);
   if (retVal != 0)
      return retVal; // ERROR: Could not write the Proxy List or Proxy Information

   // Set the Audio Track Sections
   retVal = audioFramingInfoList.writeAudioFramingInfoListSection(clipIniFile);
   if (retVal != 0)
      return retVal; // ERROR: Could not write the Audio Framing List

   retVal = audioTrackList.writeAudioTrackListSection(clipIniFile);
   if (retVal != 0)
      return retVal; // ERROR: Could not write the Audio Track List or  Info

   // Set the Timecode/Keycode Track Sections
   retVal = timeTrackList.writeTimeTrackListSection(clipIniFile);
   if (retVal != 0)
      return retVal; // ERROR: Could not write the Time Track List or Info

   if (clipIniFile->IsModified())
   {
      // These will not match if someone changed the clip file on another machine.
      auto oldClipFileModifiedTime = _clipFileModifiedTime;
      auto newClipFileModifiedTime = GetLastModifiedTime(getClipFileNameWithExt());

      if (!clipIniFile->SaveIfNecessary())
         return retVal; // ERROR: Could not save file

      // Warn user if someone else had modified the clip file.
      // Obviously a better way to do this would be to ask before committing
      // whether or not to blow away the other user's changes, but I think
      // it would be complicated to try to figure out the ramifications of
      // aborting the write here.
// THIS DOESN'T WORK - ALWAYS SAYS THE FILE IS MODIFIED!!
//      if (oldClipFileModifiedTime != 0 && newClipFileModifiedTime != oldClipFileModifiedTime)
//      {
//         CAutoErrorReporter autoErr("CClip::writeFile", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
//         autoErr.errorCode = CLIP_ERROR_INVALID_CLIP_NAME_CHARACTERS;
//         autoErr.msg << "Someone else had modified this clip," << endl << "so their changes may now be wiped out!";
//      }
   }

   // Semantically speaking, want this to always be the case after running
   // writeFile(), whether it actually wrote anything or not!
   _clipFileModifiedTime = GetLastModifiedTime(getClipFileNameWithExt());

   return 0;  // Success

} // writeFile()

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

void CClip::setVideoFrameListFileChanged(bool newFlag)
{
   videoFrameListFileChanged = newFlag;
}

bool CClip::didVideoFrameListFileChange()
{
   return videoFrameListFileChanged;
}

int CClip::updateAllVideoFrameListFiles()
{
   if (!didVideoFrameListFileChange())
      return 0;

   return writeAllVideoFrameListFiles();
}

int CClip::writeAllVideoFrameListFiles()
{
   int retVal;

   for (int proxyIndex = 0; proxyIndex < getVideoProxyCount(); ++proxyIndex)
      {
      CVideoProxy *videoProxy = getVideoProxy(proxyIndex);
      if (videoProxy != 0)
         {
         retVal = videoProxy->writeAllFrameListFiles();
         if (retVal != 0)
            {
            // ERROR: Failed to write video track files in Video Proxy
            return retVal;
            }
         }
      }
   setVideoFrameListFileChanged(false);

   return 0;

} // writeAllVideoTrackFiles

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

CIniFile* CClip::getClipIniFile()
{
   string clipFileName = getClipFileNameWithExt();
   CIniFile* clipIniFile;
   try
      {
      clipIniFile = new CIniFile(clipFileName.c_str());
      }
   catch (...)
      {
      DBTRACE(clipFileName);
      clipIniFile = 0;
      }

   return clipIniFile;
}

//////////////////////////////////////////////////////////////////////
// Read & Write Clip Information section of Clip File
//////////////////////////////////////////////////////////////////////

int CClip::readClipInfoSection(CIniFile *clipIniFile)
{
   int retVal;

   if (!clipIniFile->SectionExists(clipInfoSectionName))
      {
      // ERROR: Clip Information section does not exist
      return CLIP_ERROR_CLIP_FILE_INFO_SECTION_MISSING;
      }

   // Verify that the appropriate keys exist within the section
   // These keys are not optional and do not have default values
   if (!clipIniFile->KeyExists(clipInfoSectionName, descriptionKey)
       || !clipIniFile->KeyExists(clipInfoSectionName, createDateTimeKey)
       || !clipIniFile->KeyExists(clipInfoSectionName, projectNameKey)
       || !clipIniFile->KeyExists(clipInfoSectionName, dropFrameKey)
       || !clipIniFile->KeyExists(clipInfoSectionName, lockRecordKey)
       )
      {
      // ERROR: Clip Information section key does not exist
      return CLIP_ERROR_CLIP_FILE_INFO_SECTION_KEY_MISSING;
      }

   string emptyString, valueString, defaultValue;

   // Read Clip Description
   string newDescription = clipIniFile->ReadString(clipInfoSectionName,
                                                   descriptionKey, emptyString);

   // Read CD Ingest Type
   string defaultString = queryCDIngestTypeName(CD_INGEST_TYPE_CIRCLE_TAKES);
   ECDIngestType newCDIngestType;

   valueString = clipIniFile->ReadString(clipInfoSectionName, cdIngestTypeKey,
                                         defaultString);
   newCDIngestType = queryCDIngestType(valueString);

   // Read Create Date & Time
   string newCreateDateTime = clipIniFile->ReadString(clipInfoSectionName,
                                                      createDateTimeKey,
                                                      emptyString);

   // Read Project Name
   string newProjectName = clipIniFile->ReadString(clipInfoSectionName,
                                                   projectNameKey, emptyString);

   // Read Archive Date & Time
   string newArchiveDateTime = clipIniFile->ReadString(clipInfoSectionName,
                                                      archiveDateTimeKey,
                                                      emptyString);
   EImageFormatType newImageFormatType;
   bool default720P;
   EMediaType newMediaType;
   int defaultTimecodeFramesPerSecond;
   string tmpSectionPrefix = "VideoProxyInfo0";

   // Read ahead to find out the "clip version" of the main video track
   EClipVersion newClipVer = CTrack::readClipVer(clipIniFile, tmpSectionPrefix);
   if (newClipVer == CLIP_VERSION_3)
      {
      // Read the Image Format Type from main video's Image Format section
      newImageFormatType = CImageFormat::readImageFormatType(clipIniFile,
                                                          tmpSectionPrefix);
      retVal = CImageInfo::isImageFormatTypeValid(newImageFormatType);
      if (retVal != 0)
         return retVal; // ERROR: Invalid or missing Image Format Type

      // Default FRAME720P flag for backwards compatibility with existing
      // Clip version 3 clips that do not have the Timecode720P key.
      default720P = CImageInfo::queryIsFormat720P(newImageFormatType);

      newMediaType = CMediaStorageList::readMediaStorageType(clipIniFile,
                                                             tmpSectionPrefix);
      if (newMediaType == MEDIA_TYPE_INVALID)
         return theError.getError();

      defaultTimecodeFramesPerSecond =
               CImageInfo::queryNominalTimecodeFrameRate(newImageFormatType);
      }
   else if (newClipVer == CLIP_VERSION_5L)
      {
      // In version 5 clips, the image format is in the media track XML file
      // rather than in the image format section of the .clp file
      newImageFormatType = IF_TYPE_INVALID;
      default720P = false;
      newMediaType = MEDIA_TYPE_INVALID;
      defaultTimecodeFramesPerSecond = -1;
      }
   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   // Read Drop Frame
   valueString = clipIniFile->ReadString(clipInfoSectionName, dropFrameKey,
                                         emptyString);
   bool newDropFrame = queryDropFrame(valueString);

   // Read timecode 720P flag
   bool new720P = clipIniFile->ReadBool(clipInfoSectionName, timecode720PKey,
                                        default720P);

   // Create new timecode flags now that we know the drop frame and
   // 720P state
   int newTimecodeFlags = 0;
   if (newDropFrame)
      newTimecodeFlags |= DROPFRAME;
   if (new720P)
      newTimecodeFlags |= FRAME720P;

   // Read the timecode's frame rate.  This is a REQUIRED keyword for Clip 5
   int newTimecodeFramesPerSecond = clipIniFile->ReadInteger(clipInfoSectionName,
                                                         timecodeFrameRateKey,
                                                defaultTimecodeFramesPerSecond);
   if (newTimecodeFramesPerSecond < 0)
      return CLIP_ERROR_INVALID_FRAME_RATE;  // value is corrupted

   // Read In Timecode
   string timeString;
   if(!clipIniFile->KeyExists(clipInfoSectionName, inTimecodeKey))
      return CLIP_ERROR_CLIP_FILE_INFO_SECTION_BAD_IN_TIMECODE;
   timeString = clipIniFile->ReadString(clipInfoSectionName, inTimecodeKey,
                                        emptyString);
   timeString = TrimWhiteSpace(timeString);
   CTimecode newInTimecode(newTimecodeFlags, newTimecodeFramesPerSecond);
   if (!newInTimecode.setTimeASCII(timeString.c_str()))
      {
      // ERROR: Could not parse the In timecode string
      return CLIP_ERROR_CLIP_FILE_INFO_SECTION_BAD_IN_TIMECODE;
      }

   // Read Out Timecode
   if(!clipIniFile->KeyExists(clipInfoSectionName, outTimecodeKey))
      return CLIP_ERROR_CLIP_FILE_INFO_SECTION_BAD_OUT_TIMECODE;
   timeString = clipIniFile->ReadString(clipInfoSectionName, outTimecodeKey,
                                        emptyString);
   CTimecode newOutTimecode(newTimecodeFlags, newTimecodeFramesPerSecond);
   if (!newOutTimecode.setTimeASCII(timeString.c_str()))
      {
      // ERROR: Could not parse the Out timecode string
      return CLIP_ERROR_CLIP_FILE_INFO_SECTION_BAD_OUT_TIMECODE;
      }

   // Read Lock Record
   bool newLockRecordFlag = clipIniFile->ReadBool(clipInfoSectionName,
                                                  lockRecordKey, false);

   // Read Clip GUID
   string guidStr = clipIniFile->ReadString(clipInfoSectionName, clipGUIDKey,
                                            nullClipGUIDStr);
   CLIP_GUID newClipGUID;
   if (!guid_from_string(newClipGUID, guidStr))
      return CLIP_ERROR_CLIP_FILE_INFO_SECTION_BAD_CLIP_GUID;

   // Read Clip Version Number
   int newClipVersion = clipIniFile->ReadInteger(clipInfoSectionName, clipVersionKey,
                                          0);

   // Read Disk Scheme
   string newDiskScheme = clipIniFile->ReadString(clipInfoSectionName,
                                                  diskSchemeKey, "unknown");

   // Set new values in sharedThis
   description = newDescription;
   cdIngestType = newCDIngestType;
   diskScheme_ = newDiskScheme;
   createDateTime = newCreateDateTime;
   archiveDateTime = newArchiveDateTime;
   projectName = newProjectName;
   inTimecode = newInTimecode;
   outTimecode = newOutTimecode;
   lockRecordFlag = newLockRecordFlag;
   *clipGUID = newClipGUID;
   clipVersion = newClipVersion;

   return 0;

} // readClipInfoSection( )

int CClip::writeClipInfoSection(CIniFile *clipIniFile)
{
   // Write Clip Description
   clipIniFile->WriteString(clipInfoSectionName, descriptionKey, description);

   // Write CD Ingest Type
   clipIniFile->WriteString(clipInfoSectionName, cdIngestTypeKey,
                            queryCDIngestTypeName(cdIngestType));

   // Write Create Date & Time
   clipIniFile->WriteString(clipInfoSectionName, createDateTimeKey,
                            createDateTime);

   // Write Archive Date & Time
   clipIniFile->WriteString(clipInfoSectionName, archiveDateTimeKey,
                            archiveDateTime);

   // Write Project Name
   clipIniFile->WriteString(clipInfoSectionName, projectNameKey, projectName);


   // Write Drop Frame
   string dropFrameValue;
   dropFrameValue = (inTimecode.getFlags() & DROPFRAME) ? "DF" : "ND";
   clipIniFile->WriteString(clipInfoSectionName, dropFrameKey,
                            dropFrameValue);

   // Write Timecode 720P flag
   clipIniFile->WriteBool(clipInfoSectionName, timecode720PKey,
                          inTimecode.is720P());

   // Write timecode's frames-per-second
   clipIniFile->WriteInteger(clipInfoSectionName, timecodeFrameRateKey,
                             inTimecode.getFramesPerSecond());

   // Write In Timecode
   char tmpStr[100];
   inTimecode.getTimeASCII(tmpStr);
   string timeString = tmpStr;
   clipIniFile->WriteString(clipInfoSectionName, inTimecodeKey, timeString);

   // Write Out Timecode
   outTimecode.getTimeASCII(tmpStr);
   timeString = tmpStr;
   clipIniFile->WriteString(clipInfoSectionName, outTimecodeKey, timeString);

   // Write Lock Record
   clipIniFile->WriteBool(clipInfoSectionName, lockRecordKey, lockRecordFlag);

   // Write Clip GUID
   string clipGUIDStr = guid_to_string(*clipGUID);
   clipIniFile->WriteString(clipInfoSectionName, clipGUIDKey, clipGUIDStr);

   // Write Clip Version number
   clipIniFile->WriteInteger(clipInfoSectionName, clipVersionKey, clipVersion);

   // Write Disk Scheme
   clipIniFile->WriteString(clipInfoSectionName, diskSchemeKey, diskScheme_);

   return 0;

} // writeClipInfoSection( )

int CClip::renameClipReferenceTargets(string newClipName)
{
   // renames all files referred to by the clip as necessary,
   // including frame list files and history files contained in the
   // clip folder, and media files in parallel directory structures

   int retVal;

   int videoProxyCount = getVideoProxyCount();
   int audioTrackCount = getAudioTrackCount();

   for (int i = 0; i < videoProxyCount; ++i)
      {
      CVideoProxy* videoProxy = getVideoProxy(i);
      if (videoProxy != 0)
         {
         // frame list files
         retVal = videoProxy->renameAllFrameListFiles(newClipName);
         if (retVal != 0)
            {
            MTIostringstream ostr;
            static char const *func = "CClip::renameClipReferenceTargets";
            ostr << func << " failed for clip: " << getClipPathName() << endl
                 << "      Reason: can't rename track " << i
                 << " frame list files (" << retVal << ")";
            TRACE_0(errout << ostr.str());
            return retVal;
            }

         // history files
         int framingCount = videoProxy->getFramingCount();
         for (int j = 0; j < framingCount; ++j)
            {

            CVideoFrameList *videoFrameList = videoProxy->getVideoFrameList(j);

            if (videoFrameList != 0)
               {
               string frameListFileName =
                  videoFrameList->getFrameListFileName();
               string newFrameListFileName =
                  videoFrameList->getNewFrameListFileName(newClipName);

               if (DoesFileExist(frameListFileName + ".hed"))
                  {
                  // rename .hed file
                  rename((frameListFileName + ".hed").c_str(),
                         (newFrameListFileName + ".hed").c_str());
                  if (retVal != 0)
                     {
                     MTIostringstream ostr;
                     static char const *func = "CClip::renameClipReferenceTargets";
                     ostr << func << " failed for clip: " << getClipPathName() << endl
                          << "      Reason: can't rename track " << i
                          << " framing type " << j << " .hed file ("
                          << retVal << ")";
                     TRACE_0(errout << ostr.str());
                     return retVal;
                     }
                  }

               if (DoesFileExist(frameListFileName + ".rst"))
                  {
                  // rename .rst file
                  rename((frameListFileName + ".rst").c_str(),
                         (newFrameListFileName + ".rst").c_str());
                  if (retVal != 0)
                     {
                     MTIostringstream ostr;
                     static char const *func = "CClip::renameClipReferenceTargets";
                     ostr << func << " failed for clip: " << getClipPathName() << endl
                          << "      Reason: can't rename track " << i
                          << " framing type " << j << " .rst file ("
                          << retVal << ")";
                     TRACE_0(errout << ostr.str());
                     return retVal;
                     }
                  }

               if (DoesFileExist(frameListFileName + ".pdl"))
                  {
                  // rename .pdl file
                  rename((frameListFileName + ".pdl").c_str(),
                         (newFrameListFileName + ".pdl").c_str());
                  if (retVal != 0)
                     {
                     MTIostringstream ostr;
                     static char const *func = "CClip::renameClipReferenceTargets";
                     ostr << func << " failed for clip: " << getClipPathName() << endl
                          << "      Reason: can't rename track " << i
                          << " framing type " << j << " .pdl file ("
                          << retVal << ")";
                     TRACE_0(errout << ostr.str());
                     return retVal;
                     }
                  }

               if (DoesFileExist(frameListFileName + ".pix"))
                  {
                  // rename .pix file
                  rename((frameListFileName + ".pix").c_str(),
                         (newFrameListFileName + ".pix").c_str());
                  if (retVal != 0)
                     {
                     MTIostringstream ostr;
                     static char const *func = "CClip::renameClipReferenceTargets";
                     ostr << func << " failed for clip: " << getClipPathName() << endl
                          << "      Reason: can't rename track " << i
                          << " framing type " << j << " .pix file ("
                          << retVal << ")";
                     TRACE_0(errout << ostr.str());
                     return retVal;
                     }
                  }
               }
            }
         }
      }

   for (int i = 0; i < audioTrackCount; ++i)
      {
      CAudioTrack* audioTrack = getAudioTrack(i);
      if (audioTrack != 0)
         {
         retVal = audioTrack->renameAllFrameListFiles(newClipName);
         if (retVal != 0)
            {
            MTIostringstream ostr;
            static char const *func = "CClip::renameClipReferenceTargets";
            ostr << func << " failed for clip: " << getClipPathName() << endl
                          << "      Reason: can't rename audio track "
                          << i <<  "file ("
                          << retVal << ")";
            TRACE_0(errout << ostr.str());
            return retVal;
            }
         }
      }

   return retVal;
}

int CClip::renameClipMedia(string newBinPath, string newClipName)
{
   // renames all media container files referred to by the clip as necessary

   int retVal = 0;

   int videoProxyCount = getVideoProxyCount();

   for (int i = 0; i < videoProxyCount; ++i)
      {
      CVideoProxy* videoProxy = getVideoProxy(i);
      if (videoProxy != 0)
         {
         // media files
         retVal = videoProxy->renameClipMedia(newBinPath, newClipName);
         if (retVal != 0)
            {
            MTIostringstream ostr;
            static char const *func = "CClip::renameClipMedia";
            ostr << func << " failed for clip: " << getClipPathName() << endl
                          << "      Reason: can't rename track " << i
                           << " video media (" << retVal << ")";
            TRACE_0(errout << ostr.str());
            return retVal;
            }
         }
      }

   return retVal;
}

// This is very bad - supports hack for BinManager to rewrite the
// MediaStorage section of the clip file...
int CClip::writeMediaStorageSection(int proxyIndex,
                             CMediaStorageList *mediaStorageList)
{
   CIniFile *clipIniFile = getClipIniFile();
   if (clipIniFile == 0)
   {
      DBTRACE(this->getClipFileNameWithExt());
      return CLIP_ERROR_CANNOT_OPEN_OR_CREATE_CLIP_FILE;
   }

   string sectionPrefix = videoProxyList.getVideoSectionName() + "Info";
   MTIostringstream os;
   os << sectionPrefix << proxyIndex;
   int retVal = mediaStorageList->writeMediaStorageSection(clipIniFile, os.str());

   DeleteIniFile(clipIniFile);
   return retVal;
}

// This is very bad - supports hack for BinManager to replace the
// MediaStorage section of the clip and write it to the clip file...
int CClip::updateMediaStorageSection(int proxyIndex, CMediaStorageList *mediaStorageList)
{
   if (proxyIndex >= getVideoProxyCount())
   {
      DBTRACE(proxyIndex);
      return CLIP_ERROR_INVALID_VIDEO_PROXY_INDEX;
   }

   CIniFile *clipIniFile = getClipIniFile();
   if (clipIniFile == 0)
   {
      DBTRACE(this->getClipFileNameWithExt());
      return CLIP_ERROR_CANNOT_OPEN_OR_CREATE_CLIP_FILE;
   }

   MTIostringstream os;
   os << videoProxyList.getVideoSectionName() << "Info" << proxyIndex;
   string sectionPrefix = os.str();
   int retVal = getVideoProxy(proxyIndex)->updateMediaStorageList(*mediaStorageList, clipIniFile, sectionPrefix);

   DeleteIniFile(clipIniFile);
   return retVal;
}

bool CClip::queryDropFrame(const string& dropFrameName)
{
   if (EqualIgnoreCase(dropFrameName, queryDropFrameName(true)))
      return true;
   else
      return false;
}

string CClip::queryDropFrameName(bool dropFrameFlag)
{
   const char *dropFrameName;
   if (dropFrameFlag)
      dropFrameName = "DF";
   else
      dropFrameName = "ND";

   return string(dropFrameName);
}

//////////////////////////////////////////////////////////////////////
// Delete Clip
//////////////////////////////////////////////////////////////////////

int CClip::deleteClipFile()
{
   int retVal;

   // Accumulate the Video Proxies and Audio Tracks to be deleted
   vector<CTrack*> tracksToBeDeleted;
   if (!isMediaDestroyed())
      {
      for (int i = 0; i < getVideoProxyCount(); ++i)
         {
         CVideoProxy* videoProxy = getVideoProxy(i);
         if (videoProxy != 0)
            tracksToBeDeleted.push_back(videoProxy);
         }
      for (int i = 0; i < getAudioTrackCount(); ++i)
         {
         CAudioTrack* audioTrack = getAudioTrack(i);
         if (audioTrack != 0)
            tracksToBeDeleted.push_back(audioTrack);
         }
      }

   int trackCount = tracksToBeDeleted.size();

   // Determine, for each of the CTracks to be deleted, what
   // media storage needs to be deallocated
   vector<CMediaDeallocatorList> mediaDeallocatorMasterList(trackCount);
   for (int i = 0; i < trackCount; ++i)
      {
      CTrack* track = tracksToBeDeleted[i];
      retVal = track->deallocateMediaStorage(mediaDeallocatorMasterList[i]);
      if (retVal != 0)
         return retVal;  // ERROR: media storage deallocation failed
      }

   // Delete (actually rename) the clip's files
   CBinManager binMgr;
   string trashPrefix = binMgr.createTrashPrefix();
   string newPathName;

   // Rename the clip's directory from "binPath/clipName" to
   // "binPath/.mtitrash12345678-clipName"

   string geek = getClipPathName().c_str();

   newPathName = AddDirSeparator(getBinPath()) + trashPrefix + getClipName();
   retVal = rename(getClipPathName().c_str(), newPathName.c_str());
   if (retVal != 0)
      {
      // ERROR: Could not rename the file.  Cause is in errno
      TRACE_0(errout << "CClip::deleteClipFile could not rename file "
                     << endl;
              errout << "    " << getClipPathName() << endl;
              errout << " to " << newPathName << endl;
              errout << " because " << strerror(errno) << endl;);
      return CLIP_ERROR_FILE_RENAME_FAILED;
      }

#if defined(_WIN32)
   SetFileAttributes(newPathName.c_str(), FILE_ATTRIBUTE_HIDDEN);
#endif

   // Free the media storage
   for (int i = 0; i < (int)mediaDeallocatorMasterList.size(); ++i)
      {
      retVal = mediaDeallocatorMasterList[i].freeMediaStorage();
      if (retVal != 0)
         {
         // ERROR: Failed to free the media storage
         // TBD: handling the error so that the clip is not partially
         //      deleted ????
         return retVal;
         }
      }

   clipFilesDeleted = true;

   return 0;   // Success
}

bool CClip::isClipFileDeleted() const
{
   return clipFilesDeleted;
}

string CClip::queryCDIngestTypeName(ECDIngestType cdIngestType)
{
   for (int i = 0; i < (int)CD_INGEST_TYPE_COUNT; ++i)
      {
      if (cdIngestTypeMap[i].cdIngestType == cdIngestType)
         return string(cdIngestTypeMap[i].name);
      }

   return string("INVALID");
}

ECDIngestType CClip::queryCDIngestType(const string& cdIngestTypeName)
{
   for (int i = 0; i < (int)CD_INGEST_TYPE_COUNT; ++i)
      {
      if (EqualIgnoreCase(cdIngestTypeMap[i].name, cdIngestTypeName))
         return cdIngestTypeMap[i].cdIngestType;
      }

   return CD_INGEST_TYPE_INVALID;
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Test Functions
//////////////////////////////////////////////////////////////////////

void CClip::dump(MTIostringstream &str)
{
   str << "Clip Header" << std::endl;

   str << "  Description: <" << getDescription() << ">" << std::endl;

   str << "  CD Ingest Type: <" << getCDIngestType() << ">" << std::endl;

   str << "  Disk Scheme: <" << getDiskScheme() << ">" << std::endl;

   str << "  Create Date/Time: <" << getCreateDateTime() << ">" << std::endl;

   str << "  In Timecode: " << getInTimecode()
       << "  Out Timecode: " << getOutTimecode()
       << std::endl;

   str << "  lockRecordFlag: " << getLockRecordFlag() << std::endl;
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                           ####################################
// ###     CClipInitInfo Class     ###################################
// ####                           ####################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CClipInitInfo::CClipInitInfo()
: inTimecode(0), outTimecode(0),
  mainVideoInitInfo(0), clip(nullptr),
  sourceType(CLIP_INIT_SOURCE_UNKNOWN)
{
  init();
}

CClipInitInfo::~CClipInitInfo()
{
   closeClip();
   deleteInitInfo();
}

void CClipInitInfo::deleteInitInfo()
{
   int i;

   // Delete the Time Track Init Info if it exists
   deleteTimeTrackInitInfo();

   // Delete the Audio Track Init Info if it exists
   deleteAudioTrackInitInfo();

   // Delete the Video Proxy Init Info if it exists
   deleteVideoProxyInitInfo();

   // Delete the Main Video Init Info if it exists
   if (doesNormalVideoExist())
      {
      delete mainVideoInitInfo;
      }
   mainVideoInitInfo = 0;
}

void CClipInitInfo::deleteTimeTrackInitInfo()
{
   int i;

   // Delete the Time Track Init Info if it exists
   for (int i = 0; i < getTimeTrackCount(); ++i)
      {
      delete timeTrackInitInfoList[i];
      }
   timeTrackInitInfoList.clear();
}

void CClipInitInfo::deleteAudioTrackInitInfo()
{
   int i;

   // Delete the Audio Track Init Info if it exists
   for (int i = 0; i < getAudioTrackCount(); ++i)
      {
      delete audioTrackInitInfoList[i];
      }
   audioTrackInitInfoList.clear();
}

void CClipInitInfo::deleteVideoProxyInitInfo()
{
   int i;

   // Delete the Video Proxy Init Info if it exists
   for (int i = 0; i < getVideoProxyCount(); ++i)
      {
      delete videoProxyInitInfoList[i];
      }
   videoProxyInitInfoList.clear();
}

//////////////////////////////////////////////////////////////////////
// Initialization
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    init
//
// Description: Clears the contents of a CClipInitInfo instance
//
// Arguments:   None
//
// Returns:     None
//
//------------------------------------------------------------------------
void CClipInitInfo::init()
{
   string emptyString;

   setBinPath(emptyString);
   setClipName(emptyString);
   setDescription(emptyString);
   setCDIngestType(CD_INGEST_TYPE_CIRCLE_TAKES);
   setDiskScheme(emptyString);

   CTimecode dummyTC(0);
   setInTimecode(dummyTC);
   setOutTimecode(dummyTC);

   dropFrame = false;

   // Delete the init info for Normal Video, Video Proxies and Audio Tracks
   deleteInitInfo();

   closeClip();
   sourceType = CLIP_INIT_SOURCE_UNKNOWN;

} // init

//------------------------------------------------------------------------
//
// Function:    initFromClip
//
// Description: Initializes a CClipInitInfo and its constituents
//              based on a Clip
//
//              NB: A new clip created from a CClipInitInfo initialized
//                  from a source clip by this function may not be
//                  identical to the source clip.
//
//
// Arguments:   ClipSharedPtr &clip     Pointer to source clip
//
// Returns:     0 = Success, Non-zero if error
//
//------------------------------------------------------------------------
int CClipInitInfo::initFromClip(ClipSharedPtr &sourceClip)
{
   int retVal;

   // Clear out anything that is in the CClipInitInfo
   init();

   // Remember the source for this CClipInitInfo, which is a Clip
   sourceType = CLIP_INIT_SOURCE_CLIP;

   // This CClipInitInfo must be able to "own" the pointer to the clip.
   // Since this function does not know what the caller is doing with
   // the clip pointed to by the sourceClip argument, it must open
   // the clip again to increment the clip-open reference count.  This
   // makes it safe for this CClipInitInfo to close the clip later.
   setClipPtr(sourceClip);

   // Get the basic Clip Info
   setBinPath(clip->getBinPath());
   setClipName(clip->getClipName());
   createDateTime = clip->getCreateDateTime();
   archiveDateTime = clip->getArchiveDateTime();
   setDescription(clip->getDescription());
   setCDIngestType(clip->getCDIngestType());
   setDiskScheme(clip->getDiskScheme());
   setInTimecode(clip->getInTimecode());
   setOutTimecode(clip->getOutTimecode());
   dropFrame = getInTimecode().isDropFrame();

   CVideoProxy *videoProxy;

   // Add init info for the Main Video for the first Video Proxy in the clip
   int videoProxyCount = clip->getVideoProxyCount();
   if (videoProxyCount > 0)
      {
      videoProxy = clip->getVideoProxy(VIDEO_SELECT_NORMAL);

      // Create a new video init info
      mainVideoInitInfo = new CVideoInitInfo;

      retVal = mainVideoInitInfo->initFromVideoProxy(videoProxy);
      if (retVal != 0)
         return retVal;
      }

   // Iterate over the remaining Video Proxies in the clip and add Video Init
   // Info for each Video Proxy to this Clip Init Info
   for (int videoProxyIndex = 1; videoProxyIndex < videoProxyCount; ++videoProxyIndex)
      {
      videoProxy = clip->getVideoProxy(videoProxyIndex);

      // Create a new video init info
      CVideoInitInfo* videoInitInfo = new CVideoInitInfo;
      videoProxyInitInfoList.push_back(videoInitInfo);

      retVal = videoInitInfo->initFromVideoProxy(videoProxy);
      if (retVal != 0)
         return retVal;
      }

   // Iterate over the Audio Tracks in the clip and add Audio Init Info
   // for each Audio Track to this Clip Init Info
   int audioTrackCount = clip->getAudioTrackCount();
   for (int audioTrackIndex = 0; audioTrackIndex < audioTrackCount; ++audioTrackIndex)
      {
      CAudioTrack* audioTrack = clip->getAudioTrack(audioTrackIndex);

      // Create a new audio init info
      CAudioInitInfo* audioInitInfo = new CAudioInitInfo;
      audioTrackInitInfoList.push_back(audioInitInfo);

      retVal = audioInitInfo->initFromAudioTrack(audioTrack);
      if (retVal != 0)
         return retVal;
      }

   // Iterate over the Time Tracks in the clip and add Time Init Info
   // for each Time Track to this Clip Init Info
   int timeTrackCount = clip->getTimeTrackCount();
   for (int timeTrackIndex = 0; timeTrackIndex < timeTrackCount; ++timeTrackIndex)
      {
      CTimeTrack* timeTrack = clip->getTimeTrack(timeTrackIndex);

      // Create a new time init info
      CTimeInitInfo* timeInitInfo = new CTimeInitInfo;
      timeTrackInitInfoList.push_back(timeInitInfo);

      retVal = timeInitInfo->initFromTimeTrack(timeTrack);
      if (retVal != 0)
         return retVal;
      }

   return 0; // Success
}

//------------------------------------------------------------------------
//
// Function:    initFromClipIni
//
// Description: Initializes a CClipInitInfo and its constituents
//              based on a Clip Initialization file
//
// Arguments:   string& binPath
//              string& clipName
//
// Returns:     0 = Success, Non-zero if error
//
//------------------------------------------------------------------------
int CClipInitInfo::initFromClipIni(const string& binPath,
                                   const string& clipName)
{
   int retVal;

   // Clear out anything that is in the CClipInitInfo
   init();

   // Remember the source of this Clip Init Info, which is a Clip Ini file
   sourceType = CLIP_INIT_SOURCE_CLIP_INI;

   CBinManager binMgr;
   string clipIniFileName = binMgr.makeClipIniFileNameWithExt(binPath, clipName);
   retVal = readClipScheme(clipIniFileName);
   if (retVal != 0)
      return retVal;

   return 0; // Success
}


//------------------------------------------------------------------------
//
// Function:    initFromClipScheme
//
// Description: Initializes a CClipInitInfo and its constituents
//              based on a named Clip Scheme
//
//              NOTE: this function should be reading the Clip Scheme
//                    from Clip Scheme file or database.  Currently,
//                    it is faked with hard-coded values.
//
// Arguments:   string& newClipSchemeName   Name of the Clip Scheme
//
// Returns:     0 = Success
//
//------------------------------------------------------------------------
int CClipInitInfo::initFromClipScheme(const string& newClipSchemeName)
{
   int retVal;

   // Clear out anything that is in the CClipInitInfo
   init();

   // Short circuit empty name - can happen when there is no clip displayed
   // and for some reason the video store usage widget is trying to update
   if (newClipSchemeName.empty())
      {
      TRACE_2(errout << "WARNING: CClipInitInfo::initFromClipScheme: got empty scheme name");
      return CLIP_ERROR_UNEXPECTED_NULL_ARGUMENT;
      }

   // Remember the source for this CClipInitInfo, which is a Clip Scheme
   sourceType = CLIP_INIT_SOURCE_CLIP_SCHEME;

   // Find a clip scheme file that we can use.
   CClipSchemeManager clipSchemeMgr;
   CIniFile *clipSchemeIniFile
                       = clipSchemeMgr.getClipSchemeIniFile(newClipSchemeName);
   if (clipSchemeIniFile == 0)
      {
      TRACE_0(errout << "ERROR: CClipInitInfo::initFromClipScheme failed because"
                     << endl << "       " << theError.getFmtError()
                     << endl << "       Clip scheme: " <<  newClipSchemeName);
      return theError.getError();
      }

   // Set the clip init info from the contents of the clip initialization file
   retVal = readClipScheme(clipSchemeIniFile);
   if (retVal != 0)
      {
      TRACE_0(errout << "ERROR: Cannot parse Clip Scheme File for "
                     << newClipSchemeName << endl
                     << "       Return Code: " << retVal);

      clipSchemeIniFile->FileName = ""; // Don't write the ini file
      DeleteIniFile(clipSchemeIniFile);

      return retVal;
      }

   clipSchemeIniFile->FileName = ""; // Don't write the ini file
   DeleteIniFile(clipSchemeIniFile);  // Don't need ini file any more

   return 0;  // success

/*
   CClipScheme *clipScheme = clipSchemeMgr.findClipScheme(newClipSchemeName);
   if (clipScheme != 0)
      {
      string clipSchemeFileName = clipScheme->getClipSchemeFileName();
      if (!clipSchemeFileName.empty())
         {
         retVal = readClipScheme(clipSchemeFileName);
         if (retVal != 0)
            return retVal; // ERROR: failed to read clip scheme file

         return 0;  // We're done
         }
      }

   // If we reached this point, then the caller's clip scheme could
   // not be found
   return CLIP_ERROR_CANNOT_FIND_CLIP_SCHEME;
*/
}

int CClipInitInfo::initFromClipScheme2(const string& newClipSchemeName)
{
   int retVal;

   // Find a clip scheme file that we can use.
   CClipSchemeManager clipSchemeMgr;
   CIniFile *clipSchemeIniFile
                       = clipSchemeMgr.getClipSchemeIniFile(newClipSchemeName);
   if (clipSchemeIniFile == 0)
      {
      TRACE_0(errout << "ERROR: CClipInitInfo::initFromClipScheme2 failed because"
                     << endl << "       " << theError.getFmtError());
      return theError.getError();
      }

   // Set the clip init info from the contents of the clip initialization file
   retVal = readClipScheme2(clipSchemeIniFile);
   if (retVal != 0)
      {
      TRACE_0(errout << "ERROR: Cannot parse Clip Scheme File for "
                     << newClipSchemeName << endl
                     << "       Return Code: " << retVal);

      clipSchemeIniFile->FileName = ""; // Don't write the ini file
      DeleteIniFile(clipSchemeIniFile);

      return retVal;
      }

   clipSchemeIniFile->FileName = ""; // Don't write the ini file
   DeleteIniFile(clipSchemeIniFile);  // Don't need ini file any more

   return 0;  // success
}

//------------------------------------------------------------------------
//
// Function:    initFromImageFile
//
// Description: Initializes a CClipInitInfo and its constituents based
//              on the header of an Image File.  Searches for image files
//              that follow in sequence starting with the caller's
//              firstImageFileName.  Optionally checks the header consistency
//              of all of the image files that are found.
//
//              Sets the following items in the CClipInitInfo:
//                 In Timecode
//                    The frame number extracted from the caller's
//                    firstImageFileName.
//                 Out Timecode
//                    In Timecode + Frame Count, where Frame Count is the
//                    minimum of maxFrameCount or the number of image files
//                    found
//                 Drop Frame
//                    Set to Non-Drop
//                 Clip Name
//                    If the Clip Name is blank, then the clip name is set to
//                    the filename portion of the caller's firstImageFileName
//                    before the frame number. A trailing '.' character is
//                    removed if present.  Example: if the firstImageFileName
//                    is "/mtiimages/HelloDolly.000001.dpx", then the
//                    extracted clip name is "HelloDolly"
//
//                 Note: Bin Path and Description are not changed
//
//              This function also creates a CVideoInitInfo instance
//              for the Main Video init info if one doesn't already exist.
//              The Main Video Init Info is set with:
//                 Handle Count
//                    Set to 0 for UYVY image files.  Set to the nominal
//                    count (typically 0) for other image file types
//                 Image Format
//                    Derived from the image file's header by CImageFileIO
//                 Media Type
//                    Media Type that corresponds to the image file type
//                 Media Identifier
//                    Caller's firstImageFileName
//
//              If an error occurs, CClipInitInfo and Main Video Init Info
//              may be partially set.
//
// Arguments:   string& imageFileName
//                 Name of Image File, e.g. /mtiimages/HelloDolly.00001.dpx
//                 This can be any of the frames in the sequence
//              int maxFrameCount
//                 Maximum number of image files that will be considered.
//                 If maxFrameCount is set to 0 or less, then there is no limit.
//              bool checkHeaders
//                 If true, then checks the image file headers for consistency.
//                 If false, then does not check headers. No checking is faster.
//
// Returns:     0 = Success
//              CLIP_ERROR_IMAGE_FILE_MISSING
//                First image file does not exist
//              CLIP_ERROR_IMAGE_FILE_BAD_NAME
//                Cannot parse image file name, perhaps frame number is missing
//              CLIP_ERROR_IMAGE_FILE_INCONSISTENT_HEADER
//                An image file header does not match the first image file
//              CLIP_ERROR_INVALID_MEDIA_TYPE
//                The image file type does not correspond to a Media Type
//              CLIP_ERROR_INVALID_IMAGE_FORMAT_TYPE
//                A valid image format could not be created from the image
//                file's header. Possibly unknown file type or corrupted
//                image file
//              Other errors possible, see err_clip.h for description
//
//------------------------------------------------------------------------
int CClipInitInfo::initFromImageFile(const string& imageFileName, bool checkHeaders)
{
	// Use default search for frame number embedded in the image file name
	int frameNumber = CImageFileMediaAccess::getFrameNumberFromFileName(imageFileName);
	if (frameNumber < 0)
	{
		return CLIP_ERROR_IMAGE_FILE_BAD_NAME;
	}

	// Find in and out frames for the file sequence.
	string imageFileNameTemplate = CImageFileMediaAccess::MakeTemplate(imageFileName, false);
	int firstFrame = -1;
	int lastFrame = -1;
	CImageFileMediaAccess::FindAllFiles2(imageFileNameTemplate, frameNumber, firstFrame, lastFrame);
	int inFrame = firstFrame;
	int outFrame = lastFrame + 1;

	// NOTE we do NOT check that all the files in the sequence exist because
	// it's way too slow on the SAN. If the caller cares, checkHeaders must
	// be set to true.

	// Generate the clip info.
	int retVal = initFromImageFile2(imageFileNameTemplate, inFrame, outFrame, checkHeaders, "");
	if (retVal != 0)
	{
		return retVal;
	}

	return 0;
}

//------------------------------------------------------------------------
//
// Function:    initFromImageFile2
//
// Description: Initializes a CClipInitInfo and its constituents based
//              on the header of an Image File.  Searches for image files
//              that follow in sequence starting with the caller's
//              firstImageFileName.  Optionally checks the header consistency
//              of all of the image files that are found.
//
//              Sets the following items in the CClipInitInfo:
//                 In Timecode
//                    Set to inFrame argument.
//                 Out Timecode
//                    Set to outFrame argument
//                 Drop Frame
//                    Set to Non-Drop
//                 Clip Name
//                    If the Clip Name is blank, then the clip name is set to
//                    the filename portion of the caller's imageFileNameTemplate
//                    before the frame number. A trailing '.' character is
//                    removed if present.  Example: if the imageFileNameTemplate
//                    is "/mtiimages/HelloDolly.#####.dpx", then the
//                    extracted clip name is "HelloDolly"
//
//                 Note: Bin Path and Description are not changed
//
//              This function also creates a CVideoInitInfo instance
//              for the Main Video init info if one doesn't already exist.
//              The Main Video Init Info is set with:
//                 Handle Count
//                    Set to 0 for UYVY image files.  Set to the nominal
//                    count (typically 0) for other image file types
//                 Image Format
//                    Derived from the image file's header by CImageFileIO
//                 Media Type
//                    Media Type that corresponds to the image file type
//                 Media Identifier
//                    Caller's firstImageFileName
//
//              If an error occurs, CClipInitInfo and Main Video Init Info
//              may be partially set.
//
// Arguments:   string& imageFileNameTemplate
//                 Name of Image File, e.g. /mtiimages/HelloDolly.#####.dpx
//                 with formatting characters to indicate where the frame
//                 number is located
//              int inFrame
//                 Frame number of in frame
//              int outFrame
//                 Frame number of frame one past last frame (exclusive out)
//              bool checkHeaders
//                 If true, then checks the image file headers for consistency.
//                 If false, then does not check headers. No checking is faster.
//              bool checkHeaders
//              string &clipSchemeName
//              bool useFileTimecode
//              CTimecode &firstFrameTimecode
//
// Returns:     0 = Success
//              CLIP_ERROR_IMAGE_FILE_MISSING
//                First image file does not exist
//              CLIP_ERROR_IMAGE_FILE_BAD_NAME
//                Cannot parse image file name, perhaps frame number is missing
//              CLIP_ERROR_IMAGE_FILE_INCONSISTENT_HEADER
//                An image file header does not match the first image file
//              CLIP_ERROR_INVALID_MEDIA_TYPE
//                The image file type does not correspond to a Media Type
//              CLIP_ERROR_INVALID_IMAGE_FORMAT_TYPE
//                A valid image format could not be created from the image
//                file's header. Possibly unknown file type or corrupted
//                image file
//              Other errors possible, see err_clip.h for description
//
//------------------------------------------------------------------------
int CClipInitInfo::initFromImageFile2(const string& imageFileNameTemplate,
                                      int inFrame, int outFrame,
                                      bool checkHeaders,
                                      const string &clipSchemeName,
                                      bool useFileTimecode)
{
	return initFromImageFile3(imageFileNameTemplate, inFrame, outFrame,
                             checkHeaders, clipSchemeName, useFileTimecode,
                             CTimecode::NOT_SET);
}

int CClipInitInfo::initFromImageFile3(const string& imageFileNameTemplate,
                                      int inFrame, int outFrame,
                                      bool checkHeaders,
                                      const string &clipSchemeName,
                                      bool useFileTimecode,
                                      const CTimecode &firstFrameTimecode)
{
	int retVal;

	// Set information in this CClipInitInfo

   // Remember the source for this CClipInitInfo, which is an Image File
   sourceType = CLIP_INIT_SOURCE_IMAGE_FILE;

   // Set the in and out timecode as frame counts from caller's inFrame and
   // outFrame arguments.  If the caller wants to use SMPTE-style timecodes
   // instead, they can be set after this function returns

   // In Timecode = caller's inFrame argument
   CTimecode timecode(0);  // Frames-per-second = 0, Flags = 0
   inTimecode = timecode;
   inTimecode.setAbsoluteTime(inFrame);

   // Out Timecode = caller's outFrame argument
   outTimecode = timecode;
   outTimecode.setAbsoluteTime(outFrame);

   // Set dropFrame member variable to Non-Drop
   dropFrame = false;

   // Clip Name = If name is empty, Extract a clip name from the
   //             caller's Image File Name
   if (clipName.empty())
   {
      string fileNamePrefix, fileNameSuffix, fileName;
      CImageFileMediaAccess::SplitFileName(imageFileNameTemplate,
                                           fileNamePrefix,
                                           fileNameSuffix,
                                           fileName);
      string tmpClipName = GetFileName(fileNamePrefix);
      if (tmpClipName[tmpClipName.length() - 1] == '.')
         clipName = tmpClipName.substr(0, tmpClipName.length() - 1);
      else
         clipName = tmpClipName;
   }

   // Don't touch the Bin Path, it needs to be set by caller from user input
   // or a clip scheme.

   // Create a Main Video init info instance if one doesn't already exist
   if (!doesNormalVideoExist())
      mainVideoInitInfo = new CVideoInitInfo;

   // Set information in the Main Video CVideoInitInfo

   // Error if imageFileNameTemplate is not a properly formed template
   if (!CImageFileMediaAccess::IsTemplate(imageFileNameTemplate))
      return CLIP_ERROR_IMAGE_FILE_BAD_NAME;

   // Set Media Identifier to caller's imageFileNameTemplate
   mainVideoInitInfo->setMediaIdentifier(imageFileNameTemplate);

   // Derive the image file folder path from the template
   TrimWhiteSpace(imageFileNameTemplate);
   string imageFileDir = GetFilePath(imageFileNameTemplate);

   // This replaces all the commented-out stuff below.
   HistoryLocation historyLocation;
   auto historyDirectory = historyLocation.computeHistoryLocation(imageFileDir);
   if (!historyDirectory.empty())
   {
      mainVideoInitInfo->setHistoryDirectory(historyDirectory);
   }

////****WARNING****WARNING******WARNING*****WARNING*********
//// THE CODE BELOW IS DUPLICATED IN MediaStorage3.cpp !!!!
//// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
//
//   /////////////////////////////////////////////////////////////////////////////
//   //
//   // The history dir for the clip is made by taking the dir part of the
//   // imageFileNameTemplate and appending it (without its disk preface) to
//   // the main history path specified in MTILocalMachine.ini
//   //
//   // This will be replaced by an edit box to be filled-in by the user in
//   // the clip-creation process
//   //
//   ////////////////////////////////////////////////////////////////////////////////
//   // MONOLITHIC (per-clip) HISTORY IS NO LONGER SUPPORTED FOR NEWLY-CREATED CLIPS.
//   // So we now ignore the machineLocal.ini setting. Of course, existing
//   // clips with monolithic history will continue to work!
//   ////////////////////////////////////////////////////////////////////////////////
////   bool historyTypeIsFilePerFrame = true;
//   bool putMetadataIniInMetadataDir = false;
//   string metaDataDirectory;
//   string historyDirectory;
//   string metaDataIniFileName;
//   const bool OPEN_INI_FILE_READ_ONLY = true;
//	const bool OPEN_INI_FILE_READ_WRITE = false;
//
//   CIniFile *localMachineIniFile = CPMPOpenLocalMachineIniFile();
//   if (localMachineIniFile != NULL)
//   {
//      localMachineIniFile->ReadOnly(true);
//
//      ////////////////// in MTILocalMachine.ini
//      //
//      // [History]
//      // HistoryType=FILE_PER_FRAME     (default, or FILE_PER_CLIP) <<<<<---- NO LONGER SUPPORTED
//      //
//      // [Metadata]
//      // MetadataRootDirectory=$(CPMP_SHARED_DIR)MTIMetadata    (default is to use image folder)
//      // IniFileInMetadataDir=false     (default)
//      //////////////////
//
////      string historyType = localMachineIniFile->ReadString(historySectionName, historyTypeKey, CClipInitInfo::historyTypeFilePerFrame);
////      historyTypeIsFilePerFrame = historyType == historyTypeFilePerFrame;
//
//      metaDataDirectory = localMachineIniFile->ReadString(metaDataSectionName, metaDataDirectoryKey, metaDataDirectory);
//      ExpandEnviornmentString(metaDataDirectory);
//      ConformFileName(metaDataDirectory);
//
//      putMetadataIniInMetadataDir = localMachineIniFile->ReadBool(metaDataSectionName, iniFileInMetadataDir, false);
//
//      DeleteIniFile(localMachineIniFile);
//   }
//
////   if (historyTypeIsFilePerFrame)
//   {
//      if (metaDataDirectory.empty())
//      {
//         metaDataDirectory = imageFileDir;
//      }
//      else
//      {
//         // get directory portion of media identifier
//         string imageDirNoDriveOrLeadSlash = imageFileDir;
//         if ((((imageFileDir[0] >= 'a') && (imageFileDir[0] <= 'z')) || ((imageFileDir[0] >= 'A') && (imageFileDir[0] <= 'Z'))) &&
//               (imageFileDir[1] == ':') && (imageFileDir[2] == '\\'))
//         {
//            imageDirNoDriveOrLeadSlash = imageFileDir.substr(3);
//         }
//
//         metaDataDirectory = AddDirSeparator(metaDataDirectory) + imageDirNoDriveOrLeadSlash;
//      }
//
//      // Try to get directories out of MTIMetadata.ini -
//      // look for MTIMetadata.ini in both the metadata root directory and the
//      // image file directory.
//      metaDataIniFileName = AddDirSeparator(metaDataDirectory) + metaDataFileName;
//
//      bool foundIniInMetaDataDir = DoesFileExist(metaDataIniFileName);
//      bool foundIniInImageFileDir = DoesFileExist(AddDirSeparator(imageFileDir) + metaDataFileName);
//
//      if (!foundIniInMetaDataDir && (foundIniInImageFileDir || !putMetadataIniInMetadataDir))
//      {
//         metaDataIniFileName = AddDirSeparator(imageFileDir) + metaDataFileName;
//      }
//
//      // Make sure to not create the ini file at this point
//      CIniFile *metaDataIniFile = CreateIniFile(metaDataIniFileName, OPEN_INI_FILE_READ_ONLY);
//      if (metaDataIniFile != NULL)
//      {
//         // try to read history directory out of it
//         historyDirectory = metaDataIniFile->ReadString(historySectionName, historyDirectoryKey, historyDirectory);
//         ExpandEnviornmentString(historyDirectory);
//         ConformFileName(historyDirectory);
//
//         DeleteIniFile(metaDataIniFile);
//      }
//
//      // If we didn't find the location of any of the folders from the
//      // MTIMetadata.ini, compute them here
//      if (historyDirectory.empty())
//      {
//         historyDirectory = AddSubfolder(metaDataDirectory, "History");
//      }
//
//      // write directories back to MTIMetadata.ini - but don't create it if
//      // if all the paths are empty!!
//      if (!historyDirectory.empty() || DoesFileExist(metaDataIniFileName))
//      {
//         metaDataIniFile = CreateIniFile(metaDataIniFileName, OPEN_INI_FILE_READ_WRITE);
//         if (metaDataIniFile != NULL)
//         {
//            if (historyDirectory != metaDataIniFile->ReadString(historySectionName, historyDirectoryKey, ""))
//            {
//               if (!historyDirectory.empty())
//               {
//                  metaDataIniFile->WriteString(historySectionName, historyDirectoryKey, historyDirectory);
//                  if (!DoesDirectoryExist(historyDirectory))
//                  {
//                     if (!MakeDirectory(historyDirectory))
//                     {
//                        CAutoErrorReporter autoErr("CClipInitInfo::initFromImageFile3",
//                                                   AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
//                        autoErr.msg << "ERROR: Can't create history folder " << endl
//                                       << historyDirectory << "." << endl
//                                       << "History will not be saved for this clip.";
//                        autoErr.errorCode = CLIP_ERROR_CANT_MAKE_HISTORY_DIRECTORY;
//                     }
//                  }
//                  else
//                  {
//                     if (!DoesWritableDirectoryExist(historyDirectory))
//                     {
//                        CAutoErrorReporter autoErr("CClipInitInfo::initFromImageFile3",
//                                                   AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
//                        autoErr.msg << "ERROR: History folder"  << endl
//                                       << historyDirectory << endl
//                                       << "exists but is not writable." << endl
//                                       << "History will not be saved for this clip.";
//                        autoErr.errorCode = CLIP_ERROR_CANT_MAKE_HISTORY_DIRECTORY;
//                     }
//                  }
//               }
//               else
//               {
//                  metaDataIniFile->DeleteKey(historySectionName, historyDirectoryKey);
//               }
//            }
//
//            DeleteIniFile(metaDataIniFile);
//         }
//         else
//         {
//            CAutoErrorReporter autoErr("CClipInitInfo::initFromImageFile3",
//                                       AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
//            autoErr.msg << "ERROR: Metadata config file"  << endl
//                           << metaDataIniFileName << endl
//                           << "cannot be created." << endl
//                           << "History will not be saved for this clip!";
//            autoErr.errorCode = CLIP_ERROR_CANT_MAKE_HISTORY_DIRECTORY;
//         }
//      }
//	}
//// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//// THE ABOVE CODE IS DUPLICATED IN MediaStorage3.cpp !!!!
//// ****WARNING****WARNING******WARNING******WARNING******
//
////   if (historyTypeIsFilePerFrame)
//   {
//      mainVideoInitInfo->setHistoryDirectory(historyDirectory);
//   }

   /////////////////////////////////////////////////////////////////////////////

   // Create the name of the first image file based on caller's template
   // and in frame number
   string firstImageFileName
             = CImageFileMediaAccess::GenerateFileName2(imageFileNameTemplate, inFrame);

   // Determine if the first image file exists
   if (!DoesFileExist(firstImageFileName))
      return CLIP_ERROR_IMAGE_FILE_MISSING;

#if 0 // ????????
#ifdef DO_CINTEL_ALPHA_HACK
   //********* EMBEDDED ALPHA HACK-O-RAMA (aka CINTEL_HACK )*************
   // Need to initialize the ImageFileIO's ImageFormat because it can't
   // deduce the CINTEL HACK form of the alpha information being hidden
   // in unused bits of the data, so needs to be told that they are there.
   // This needs to be done BEFORE InitFromExistingFile() is called!
   //
   imageFileIO.OverrideImageFormat(*mainVideoInitInfo->getImageFormat());
   //********************************************************************
#endif
#endif

//	retVal = imageFileIO.InitFromExistingFile(firstImageFileName);
//	MediaFrameFileReader *reader = MediaFileIO::CreateMediaFrameFileReader(firstImageFileName);
	MediaFileIO mediaFileIO;
	MediaFrameBufferSharedPtr frameBuffer;
	retVal = mediaFileIO.readFrameBuffer(firstImageFileName, frameBuffer);
	if (retVal != 0)
		{
		if (frameBuffer && frameBuffer->getErrorInfo().code != 0)
			{
			MTIostringstream os;
			if (!frameBuffer->getErrorInfo().shortMessage.empty())
				{
				os << frameBuffer->getErrorInfo().shortMessage;
				if (!frameBuffer->getErrorInfo().longMessage.empty())
					{
					os << ": ";
					}
				}

			if (!frameBuffer->getErrorInfo().longMessage.empty())
				{
				os << frameBuffer->getErrorInfo().longMessage;
				}

			theError.set(frameBuffer->getErrorInfo().code, os.str());
			}

		return retVal;
		}

	// Use the passed in 'first frame timecode' if there is one
   CTimecode fileTC(inFrame);    // // might be bogus
   if (firstFrameTimecode != CTimecode::NOT_SET)
      {
      fileTC = firstFrameTimecode;
      }
   else
      {
      // Compute an IN Timecode for the clip; prefer to get it from the
      // header of the first file, else fall back to using the number from
      // the first file's name
//		if (useFileTimecode && (frameBuffer->getTimecode(0) != CTimecode(0)))
//			{
//			fileTC = frameBuffer->getTimecode(0);
//         }
//      }
		if (useFileTimecode && (frameBuffer->getTimecode() != CTimecode::NOT_SET))
			{
			fileTC = frameBuffer->getTimecode();
         }
      }

	// This is in case there was a timecode in the header of the first file
   setInTimecode(fileTC);
   // in the following, +1 because outFrame is inclusive
   // NO IT'S NOT....it's EXclusive... bug 2212
   setOutTimecode(getInTimecode() + (outFrame - inFrame /* + 1 */));

	// Set Media Type from the frame buffer's file type
	MediaFileType fileType = mediaFileIO.getFileType(firstImageFileName);
	EMediaType mediaType = CImageFileMediaAccess::queryMediaType(fileType);
   if (mediaType == MEDIA_TYPE_INVALID)
      return CLIP_ERROR_INVALID_MEDIA_TYPE;
   mainVideoInitInfo->setMediaType(mediaType);

	// Copy the image format from the frame buffer
	CImageFormat newImageFormat = frameBuffer->getImageFormat();
	if (retVal)
		return CLIP_ERROR_INVALID_IMAGE_FORMAT_TYPE;
	mainVideoInitInfo->setImageFormat(newImageFormat);

	EImageFormatType imageFormatType = newImageFormat.getImageFormatType();
	if (imageFormatType == IF_TYPE_INVALID)
      return CLIP_ERROR_INVALID_IMAGE_FORMAT_TYPE;

	// Set handleCount = 0 or whatever the default is
   int newHandleCount;
   newHandleCount = CImageInfo::queryNominalHandleCount(imageFormatType);
   mainVideoInitInfo->setHandleCount(newHandleCount);

   // Set the first image file index, caller can set it to be something else
   // if so desired
   mainVideoInitInfo->setImageFile0Index(inFrame);

	if (!clipSchemeName.empty())
      {
		// User has supplied a Clip Scheme name.  The clip scheme will
      // override a few parameters that the frame buffer has set.

      // First make sure the caller's clip scheme and image files are compatible.
      // If the caller's clip scheme does not contain a main video section,
      // then the entire clip scheme is ignored.
      CClipInitInfo clipSchemeInitInfo;
      clipSchemeInitInfo.initFromClipScheme(clipSchemeName);
      if (clipSchemeInitInfo.mainVideoInitInfo != 0)
         {
         // The caller's clip scheme is considered compatible with the
         // image file if the image format type and media type agree
         EImageFormatType clipSchemeImageFormatType = clipSchemeInitInfo.mainVideoInitInfo->getImageFormat()->getImageFormatType();
         EMediaType clipSchemeMediaType = clipSchemeInitInfo.mainVideoInitInfo->getMediaType();
         if ((imageFormatType != clipSchemeImageFormatType && clipSchemeImageFormatType != IF_TYPE_INVALID)
         || (mediaType != clipSchemeMediaType && clipSchemeMediaType != MEDIA_TYPE_INVALID))
            {
				MTIostringstream ostrm;
				ostrm << "Clip Scheme " << clipSchemeName << endl;
				ostrm << "is not compatible with Image File" << endl;
				ostrm << firstImageFileName;
				TRACE_0(errout << "ERROR: CClipInitInfo::initFromImageFile2:" << endl
									<< ostrm.str());
				theError.set(CLIP_ERROR_IMAGE_FILE_INCOMPATIBLE_CLIP_SCHEME, ostrm);
				return CLIP_ERROR_IMAGE_FILE_INCOMPATIBLE_CLIP_SCHEME;
				}

			// The clip scheme will override a few parameters that
			// the frame buffer has set.
         retVal = initFromClipScheme2(clipSchemeName);
         if (retVal != 0)
            return retVal;
         }
		}

   // If caller has requested it, then verify that the headers
   // of the image files match the first image file
	MTIassert(!checkHeaders);
//	if (checkHeaders)
//		{
//		for (int frameNumber = inFrame + 1; frameNumber < outFrame; ++frameNumber)
//			{
//			// Make next image file name
//			string imageFileName
//							  = CImageFileMediaAccess::GenerateFileName2(imageFileNameTemplate,
//																			frameNumber);
//
//			// Check if next image file exists
//			if (!DoesFileExist(imageFileName))
//				continue;   // Skip this, image file isn't here
//
//			// The image file exists, so now check that the image file is
//			// consistent with the first image file
//			retVal = imageFileIO.ReadImage(imageFileName, -1, 0);
//			if (retVal != 0)
//				{
//				MTIostringstream ostrm;
//				ostrm << "Header of image file " << imageFileName << endl;
//				ostrm << "is not consistent with first image file" << endl;
//				ostrm << firstImageFileName << endl;
//				TRACE_0(errout << "ERROR: CClipInitInfo::initFromImageFile2:" << endl
//									<< ostrm.str() << endl
//									<< "       CImageFileIO::ReadImage returned "
//									<< retVal);
//				theError.set(retVal, ostrm);
//				return CLIP_ERROR_IMAGE_FILE_INCONSISTENT_HEADER;
//				}
//			}
//		}

   return 0;

} // initFromImageFile

//------------------------------------------------------------------------
//
// Function:    checkNoImageFiles
//
// Description: Checks that no files exist within a sequence of image
//              file names, e.g. MyMovie.00000.rgb, MyMovie.00001.rgb, etc.
//              Parses first file name to find digits that represent the
//              frame number.  Other file names are created by incrementing
//              the initial frame number up to the caller's frame count.
//
// Arguments:   const string& firstImageFileName   Name of first file to check
//              int frameCount                     Number of file names to check
//
// Returns:     0 = Success
//              CLIP_ERROR_IMAGE_FILE_SOME_EXIST
//                At least one image file exists
//              CLIP_ERROR_IMAGE_FILE_BAD_NAME
//                Cannot parse image file name, perhaps frame number is missing
//
//------------------------------------------------------------------------
int CClipInitInfo::checkNoImageFiles(const string& firstImageFileName,
                                     int frameCount)
{
   if (frameCount < 1)
      return 0; // Nothing to check

   // Determine if the first image file exists
   if (DoesFileExist(firstImageFileName))
      return CLIP_ERROR_IMAGE_FILE_SOME_EXIST;

   int inFrame, outFrame;

   // Use default search for frame number embedded in the image file name
   inFrame = CImageFileMediaAccess::getFrameNumberFromFileName(firstImageFileName);
   if (inFrame < 0)
      return CLIP_ERROR_IMAGE_FILE_BAD_NAME;

   outFrame = inFrame + frameCount;

   string imageFileNameTemplate = CImageFileMediaAccess::MakeTemplate(firstImageFileName,
                                                             false);

   return checkNoImageFiles2(imageFileNameTemplate, inFrame, outFrame);
}

//------------------------------------------------------------------------
//
// Function:    checkNoImageFiles
//
// Description: Checks that no files exist within a sequence of image
//              file names, e.g. MyMovie.00000.rgb, MyMovie.00001.rgb, etc.
//              Parses first file name to find digits that represent the
//              frame number.  Other file names are created by incrementing
//              the initial frame number up to the caller's frame count.
//
// Arguments:   const string& imageFileNameTemplate
//              int inFrame
//              int outFrame
//
// Returns:     0 = Success
//              CLIP_ERROR_IMAGE_FILE_SOME_EXIST
//                At least one image file exists
//              CLIP_ERROR_IMAGE_FILE_BAD_NAME
//                Cannot parse image file name, perhaps frame number is missing
//
//------------------------------------------------------------------------
int CClipInitInfo::checkNoImageFiles2(const string& imageFileNameTemplate,
                                      int inFrame, int outFrame)
{
   if (!CImageFileMediaAccess::IsTemplate(imageFileNameTemplate))
      return CLIP_ERROR_IMAGE_FILE_BAD_NAME;

   for (int frameNumber = inFrame; frameNumber < outFrame; ++frameNumber)
      {
      // Make next image file name
      string imageFileName
                       = CImageFileMediaAccess::GenerateFileName2(imageFileNameTemplate,
                                                         frameNumber);

      // Check if next image file exists
      if (DoesFileExist(imageFileName))
         return CLIP_ERROR_IMAGE_FILE_SOME_EXIST;
      }

   // No files found, return success
   return 0;
}

//------------------------------------------------------------------------
//
// Function:    initMediaLocation
//
// Description: Initializes this CClipInitInfo with the audio and video
//              media locations for the Main Video, Proxy Video and
//              Audio Tracks.  Prior to calling this function, the
//              CClipInitInfo must have been previously initialized
//              with initFromClipScheme, openClipOrClipIni or similar
//              functions so that all of the video and audio init
//              sections are set.
//
//              N.B. As currently implemented, this function assumes that
//                   the main and proxy media goes onto raw or OGLV disks.
//                   This function does not handle media in image files.
//                   This function will have to be enhanced in the future
//                   to accomodate media in image files.
//
// Arguments:   string &diskSchemeName   Name of Disk Scheme containing the
//                                       media locations
//
// Returns:     0 = Success
//              Also sets theError with an error message
//
//------------------------------------------------------------------------
int CClipInitInfo::initMediaLocation(const string &diskSchemeName)
{
   int retVal;
   MTIostringstream ostr;

   CDiskSchemeList diskSchemeList;
   retVal = diskSchemeList.ReadDiskSchemeFile();
   if (retVal != 0)
      {
      ostr << "Could not read Disk Scheme file";
      theError.set(retVal, ostr);
      return retVal;
      }

   // Find the caller's Disk Scheme
   CDiskScheme diskScheme = diskSchemeList.Find(diskSchemeName);
   if (diskScheme.name.empty())
      {
      retVal = CLIP_ERROR_CANNOT_FIND_DISK_SCHEME;
      ostr << "Could not find Disk Scheme named " << diskSchemeName;
      theError.set(retVal, ostr);
      return retVal;
      }

   // Set the Clip Init Info with the media locations from the disk scheme
   retVal = initMediaLocation(diskScheme);
   if (retVal != 0)
      {
      ostr << "Could not set the clip's media locations " << endl
           << "from Disk Scheme " << diskSchemeName << endl
           << "Error code " << retVal;
      theError.set(retVal, ostr);
      return retVal;
      }

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    initMediaLocation
//
// Description: Initializes this CClipInitInfo with the audio and video
//              media locations for the Main Video, Proxy Video and
//              Audio Tracks.  Prior to calling this function, the
//              CClipInitInfo must have been previously initialized
//              with initFromClipScheme, openClipOrClipIni or similar
//              functions so that all of the video and audio init
//              sections are set.
//
//              N.B. As currently implemented, this function assumes that
//                   the main and proxy media goes onto raw or OGLV disks.
//                   This function does not handle media in image files.
//                   This function will have to be enhanced in the future
//                   to accomodate media in image files.
//
// Arguments:   CDiskScheme &diskScheme  Disk Scheme containing the media
//                                       locations
//
// Returns:     0 = Success
//
//------------------------------------------------------------------------
int CClipInitInfo::initMediaLocation(const CDiskScheme &diskScheme)
{
   int retVal;

   setDiskScheme(diskScheme.name);

   if (doesNormalVideoExist())
      {
      retVal = getMainVideoInitInfo()->initVideoMediaLocation(
                                            diskScheme.mainVideoMediaLocation);
      if (retVal != 0)
         return retVal;
      }

   int videoProxyCount = getVideoProxyCount();
   if (videoProxyCount > 0)
      {
      for (int i = 0; i < videoProxyCount; ++i)
         {
         CVideoInitInfo *videoInitInfo = getVideoProxyInitInfo(i);
         retVal = videoInitInfo->initVideoMediaLocation(
                                          diskScheme.proxyVideoMediaLocation);
         if (retVal != 0)
            return retVal;
         }
      }

   int audioTrackCount = getAudioTrackCount();
   if (audioTrackCount > 0)
      {
      for (int i = 0; i < audioTrackCount; ++i)
         {
         CAudioInitInfo *audioInitInfo = getAudioTrackInitInfo(i);
         retVal = audioInitInfo->initAudioMediaDirectory(
                                                diskScheme.audioMediaDirectory);
         if (retVal != 0)
            return retVal;
         }
      }

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    readClipScheme
//
// Description: Initializes a CClipInitInfo and its constituents
//              from a Clip Scheme file
//
//              CIniFile is used to  read the file.  See CIniFile in
//              core_code for considerations of permissions.
//
// Arguments:   string& clipSchemeFileName   Name of the Clip Scheme File
//
// Returns:     0 = Success
//
//------------------------------------------------------------------------
int CClipInitInfo::readClipScheme(const string& clipSchemeFileName)
{
   // Verify the existence of the clip scheme file
   if (!DoesFileExist(clipSchemeFileName))
      {
      TRACE_0(errout << "ERROR: Clip Scheme File " << clipSchemeFileName
                     << " cannot be found");
      return CLIP_ERROR_CLIP_SCHEME_FILE_NOT_FOUND;
      }

   // Read the clip initialization file and set the clip init info from it
   CIniFile *iniFile = CreateIniFile(clipSchemeFileName);
   if (iniFile == 0)
      {
      TRACE_0(errout << "ERROR: CIniFile cannot open Clip Scheme File "
                     << clipSchemeFileName);
      return CLIP_ERROR_CLIP_SCHEME_FILE_OPEN_FAILED;
      }

   int retVal;

   // Set the clip init info from the contents of the clip initialization file
   retVal = readClipScheme(iniFile);
   if (retVal != 0)
      {
      TRACE_0(errout << "ERROR: Cannot parse Clip Initialization File "
                     << clipSchemeFileName << endl
                     << "       Return Code: " << retVal);

      iniFile->FileName = ""; // Don't write the ini file
      DeleteIniFile(iniFile);

      return retVal;
      }

   iniFile->FileName = ""; // Don't write the ini file
   DeleteIniFile(iniFile);  // Don't need ini file any more

   return 0;  // success

} // readClipScheme

//------------------------------------------------------------------------
//
// Function:    writeClipScheme
//
// Description: Creates a Clip Scheme file given a CClipInitInfo instance
//              and its constituent Video and Audio init info.
//
//              CIniFile is used to  write the file.  See CIniFile in
//              core_code for considerations of permissions and if the
//              file already exists.
//
//              NB: If the file already exists, sections and keywords that
//                  are not explicitly written will be maintained.  You may
//                  want to delete the file before calling this function if
//                  you do not want that behavior.
//
// Arguments:   string& clipSchemeFileName   Name of the Clip Scheme File
//
// Returns:     0 = Success
//
//------------------------------------------------------------------------
int CClipInitInfo::writeClipScheme(const string& clipSchemeFileName)
{
   int retVal;

   // Create the CIniFile instance.  This will read the file if it
   // already exists, or create the file if it doesn't exist
   CIniFile *iniFile = CreateIniFile(clipSchemeFileName);
   if (iniFile == 0)
      {
      TRACE_0(errout << "ERROR: Cannot create CIniFile for Clip Scheme File "
                     << clipSchemeFileName);
      return CLIP_ERROR_CLIP_SCHEME_FILE_CREATE_FAILED;
      }

   // Set the Create Date and Time to be now
   createDateTime = CBinManager::createDateAndTimeString();

   // Write the clip scheme file from the contents of this clip init info
   retVal = writeClipScheme(iniFile);
   if (retVal != 0)
      {
      TRACE_0(errout << "ERROR: Cannot generate Clip Scheme File "
                     << clipSchemeFileName << "   Return Code: " << retVal);

      iniFile->FileName = ""; // Don't write the ini file
      DeleteIniFile(iniFile);

      return retVal; // ERROR
      }

   // Deleting the CIniFile instance forces it to write and close the file
   if (!DeleteIniFile(iniFile))
      return CLIP_ERROR_CLIP_SCHEME_FILE_WRITE_FAILED;

   return 0; // success

} // writeClipScheme

//------------------------------------------------------------------------
//
// Function:    initMediaLocation
//
// Description: Sets the media identifier for a file-based video clip
//              which has already been initialized from a video clip scheme.
//              If the files already exist, checks compatibility between
//              the CClipInitInfo settings and the first file header.
//              Optionally checks headers of each of the files that will
//              make up the clip.
//
//              Unlike initFromImageFile2, does NOT set the following items
//              in the CClipInitInfo:
//                 Clip Name, Handle Count, Image Format or  Media Type
//              THESE MUST ALREADY HAVE BEEN SET FROM THE CLIP SCHEME
//
//              DOES, however, set the following:
//                 Media Identifier
//                    Caller's image file name template
//                 In Timecode, Out Timecode, Drop Frame,
//              NOTE!!
//                 Main video must exist, and imageFile0Index set in it to the
//                 index number embedded in the first file's name
//
//
// Arguments:   string& imageFileNameTemplate
//                 Name of Image File, e.g. /mtiimages/HelloDolly.#####.dpx
//                 with formatting characters to indicate where the frame
//                 number is located.
//              bool checkHeaders
//                 If true, then checks the image file headers for consistency.
//                 If false, then does not check headers. No checking is faster.
//
// Returns:     0 = Success
//              CLIP_ERROR_IMAGE_FILE_MISSING
//                First image file does not exist - THIS IS NOT REALLY AN ERROR
//              CLIP_ERROR_IMAGE_FILE_BAD_NAME
//                Cannot parse image file name, perhaps frame number is missing
//              CLIP_ERROR_IMAGE_FILE_INCONSISTENT_HEADER
//                An image file header does not match the first image file
//              CLIP_ERROR_INVALID_MEDIA_TYPE
//                The image file type does not correspond to the
//                CClipInitInfo Media Type
//              CLIP_ERROR_INVALID_IMAGE_FORMAT_TYPE
//                A valid image format could not be created from the image
//                file's header. Possibly unknown file type or corrupted
//                image file
//              Other errors possible, see err_clip.h for description
//
//------------------------------------------------------------------------
int CClipInitInfo::initMediaLocation(
                           const string& imageFileNameTemplate,
                           int frame0Index,
                           int frameCount,
                           bool checkHeaders,
                           const string &clipSchemeName)
{
   int retVal;

   // Error if Main Video init info instance doesn't already exist
   if (!doesNormalVideoExist())
      return CLIP_ERROR_MAIN_VIDEO_NOT_AVAILABLE;

   // Error if imageFileNameTemplate is not a properly formed template
   if (!CImageFileMediaAccess::IsTemplate(imageFileNameTemplate))
      return CLIP_ERROR_IMAGE_FILE_BAD_NAME;

   // Set information in the Main Video CVideoInitInfo

   // Set Media Identifier to caller's imageFileNameTemplate
   mainVideoInitInfo->setMediaIdentifier(imageFileNameTemplate);

   // stash the first frame index
   mainVideoInitInfo->setImageFile0Index(frame0Index);

   // Create the name of the first image file based on caller's template
   // and first frame number
   string firstImageFileName
             = CImageFileMediaAccess::GenerateFileName2(imageFileNameTemplate,
                                                               frame0Index);

   // Determine if the first image file exists
   if (!DoesFileExist(firstImageFileName))
      return CLIP_ERROR_IMAGE_FILE_MISSING;  // NOT REALLY AN ERROR

//	// Attempt to create a CImageFileIO instance based on the caller's
//	// image file
//	CImageFileIO imageFileIO;

   // initialize the image file timecode because DPX files don't know
   // the frame rate; if there is a timecode in the first file, it will
   // replace the one we set here (01:00:00:00) but with our frame rate.
   // Oh, yeah, don't forget to figure in handles.... which I think are
   // a real bad idea for image file based clips.
   CTimecode tcDefault = getTimecodePrototype();
   int handleCount = mainVideoInitInfo->getHandleCount();
   if (frameCount < (2*handleCount))
      handleCount = 0;  // for sanity
   tcDefault.setHours(1);
   // there are no - or -= operators defined for timecodes!!
   tcDefault = tcDefault + -handleCount;
//	imageFileIO.setFrame0Timecode(tcDefault);     // xxxmfiozzz FIX ME

#if 0
#ifdef DO_CINTEL_ALPHA_HACK
   //********* EMBEDDED ALPHA HACK-O-RAMA (aka CINTEL_HACK )*************
   // Need to initialize the ImageFileIO's ImageFormat because it can't
   // deduce the CINTEL HACK form of the alpha information being hidden
   // in unused bits of the data, so needs to be told that they are there.
	// This needs to be done BEFORE InitFromExistingFile() is called!
   //
   imageFileIO.OverrideImageFormat(*mainVideoInitInfo->getImageFormat());
   //********************************************************************
#endif
#endif

//	retVal = imageFileIO.InitFromExistingFile(firstImageFileName);
	MediaFileIO mediaFileIO;
	MediaFrameBufferSharedPtr frameBuffer;
	retVal = mediaFileIO.readFrameBuffer(firstImageFileName, frameBuffer);
	if (retVal != 0)
	{
		return CLIP_ERROR_FILE_CREATE_FAILED;
	}

// xxxmfioxxx	frameBuffer->setTimecode(tcDefault);
	if (frameBuffer->getTimecode() == CTimecode::NOT_SET)
	{
		frameBuffer->setTimecode(tcDefault);
	}

   // Set the timecodes
//	CTimecode tcIn(imageFileIO.getTimecode(0) + handleCount);
	CTimecode tcIn(frameBuffer->getTimecode() + handleCount);
	CTimecode tcOut(tcIn + (frameCount - 2*handleCount));
   setInTimecode(tcIn);
   dropFrame = tcIn.isDropFrame();  // hunh - why doesn't setInTimecode do it?
   setOutTimecode(tcOut);

   // Check Media Type from CImageFileIO's file type
//	int fileType = imageFileIO.getFileType();
	MediaFileType fileType = mediaFileIO.getFileType(firstImageFileName);
	EMediaType mediaType = CImageFileMediaAccess::queryMediaType(fileType);
   if (mediaType == MEDIA_TYPE_INVALID ||
       mediaType != mainVideoInitInfo->getMediaType())
      {
      return CLIP_ERROR_INVALID_MEDIA_TYPE;
      }

   // Check the image file's format
//	CImageFormat *fileImageFormat = imageFileIO.getImageFormatPtr();
	CImageFormat fileImageFormat = frameBuffer->getImageFormat();
	if (retVal)
	{
		return CLIP_ERROR_INVALID_IMAGE_FORMAT_TYPE;
   }

	EImageFormatType imageFormatType = fileImageFormat.getImageFormatType();
   if (imageFormatType == IF_TYPE_INVALID)
      return CLIP_ERROR_INVALID_IMAGE_FORMAT_TYPE;

   if (!fileImageFormat.compare(*(mainVideoInitInfo->getImageFormat()), true))
      {
      MTIostringstream ostrm;
      ostrm << "Image file " << firstImageFileName << "    " << endl
            << "is not compatible with Clip Scheme "
            << clipSchemeName << "    "<< endl;
      TRACE_0(errout << "ERROR: CClipInitInfo::initMediaLocation:" << endl
                     << ostrm.str());
      theError.set(CLIP_ERROR_IMAGE_FILE_INCOMPATIBLE_CLIP_SCHEME, ostrm);
      return CLIP_ERROR_IMAGE_FILE_INCOMPATIBLE_CLIP_SCHEME;
      }

   if (getVideoProxyCount() > 0)
      {
      MTIostringstream ostrm;
      ostrm << "WARNING: Bad clip scheme" << clipSchemeName << "    "<< endl
            << "Reason: Clips with image file media cannot have proxies    " << endl;
      TRACE_0(errout << "ERROR: CClipInitInfo::initMediaLocation:" << endl
                     << ostrm.str());
      theError.set(retVal, ostrm);
      return retVal;
      }

   if (getAudioTrackCount() > 0)
      {
      MTIostringstream ostrm;
      ostrm << "WARNING: Bad clip scheme"  << clipSchemeName << "    "<< endl
            << "Reason: Clips with image file media cannot have audio    " << endl;
      TRACE_0(errout << "ERROR: CClipInitInfo::initMediaLocation:" << endl
                     << ostrm.str());
      theError.set(retVal, ostrm);
      return retVal;
      }

   // If caller has requested it, then verify that the headers
	// of the image files match the first image file
	MTIassert(!checkHeaders);
//	if (checkHeaders)
//		{
//		for (int frameNumber = frame0Index + 1;
//			  frameNumber < (frame0Index + frameCount);
//			  ++frameNumber)
//			{
//			// Make next image file name
//			string imageFileName
//							  = CImageFileMediaAccess::GenerateFileName2(
//																			imageFileNameTemplate,
//																			frameNumber);
//
//			// Check if next image file exists
//			if (!DoesFileExist(imageFileName))
//            continue;   // Skip this, image file isn't here
//
//         // The image file exists, so now check that the image file is
//			// consistent with the first image file
//			// ["0" --> no buffer, so header read only]
//			retVal = imageFileIO.ReadImage(imageFileName, -1, 0);
//			if (retVal != 0)
//				{
//				MTIostringstream ostrm;
//				ostrm << "Header of image file " << imageFileName << endl;
//				ostrm << "is not consistent with first image file" << endl;
//				ostrm << firstImageFileName << endl;
//				TRACE_0(errout << "ERROR: CClipInitInfo::initMediaLocation:" << endl
//									<< ostrm.str() << endl
//									<< "       CImageFileIO::ReadImage returned "
//									<< retVal);
//				theError.set(retVal, ostrm);
//				return CLIP_ERROR_IMAGE_FILE_INCONSISTENT_HEADER;
//				}
//			}
//		}

   return 0;

} // initMedialLocation

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    closeClip
//
// Description: Closes the clip referenced by the member variable 'clip'
//              If clip is a NULL pointer, then the function returns
//              without doing anything.
//              Sets the member variable clip to a NULL pointer (0).
//
// Arguments:   bool forceFlag   Ordinarily we really close the clip only
//                               when the ref count goes to 0, but if this
//                               flag is set we tell the bin manager to
//                               close the clip regardless of the ref count;
//                               of cource, this is bound to really mess
//                               things up!!
//
// Returns:     0 = Success, Non-zero error return from CBinManager::closeClip
//
//------------------------------------------------------------------------
int CClipInitInfo::closeClip(bool forceFlag)
{
   int retVal;

   if (clip == nullptr)
      return 0;      // No clip to close

   CBinManager binMgr;

   if (forceFlag)
      retVal = binMgr.closeClipForced(clip);
   else
      retVal = binMgr.closeClip(clip);

   if (retVal != 0)
      return retVal;

   clip = 0;

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Accessor
//////////////////////////////////////////////////////////////////////

bool CClipInitInfo::doesNormalVideoExist() const
{
   return (mainVideoInitInfo != 0) ? true : false;
}

int CClipInitInfo::getAudioTrackCount() const
{
   return (int)audioTrackInitInfoList.size();
}

CAudioInitInfo* CClipInitInfo::getAudioTrackInitInfo(int audioTrackIndex) const
{
   if (audioTrackIndex < 0 || audioTrackIndex >= getAudioTrackCount())
      return 0;   // index is out-of-range

   return audioTrackInitInfoList[audioTrackIndex];
}

string CClipInitInfo::getBinPath() const
{
   return binPath;
}

string CClipInitInfo::getClipName() const
{
   return clipName;
}

string CClipInitInfo::getClipPath() const
{
   CBinManager binManager;
   return binManager.makeClipPath(binPath, clipName);
}

string CClipInitInfo::getClipVersionDisplayName() const
{
   ClipVersionNumber versionNumber(getClipPath());
   return versionNumber.ToDisplayName();
}

string CClipInitInfo::getMasterClipPath() const
{
   CBinManager binManager;
   return binManager.getMasterClipPath(getClipPath());
}

ClipSharedPtr  CClipInitInfo::getClipPtr() const
{
   return clip;
}

EClipInitSource CClipInitInfo::getClipInitSource() const
{
   return sourceType;
}

//------------------------------------------------------------------------
//
// Function:     getClipStatus
//
// Description:
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
EMediaStatus CClipInitInfo::getClipStatus() const
{
   EMediaStatus clipStatus;

   if (sourceType == CLIP_INIT_SOURCE_CLIP)
      {
      clipStatus = clip->getLastMediaAction(TRACK_TYPE_VIDEO,
                                            VIDEO_SELECT_NORMAL);
      }
   else if (sourceType == CLIP_INIT_SOURCE_CLIP_INI)
      {
      clipStatus = MEDIA_STATUS_MARKED;
      }
   else
      {
      clipStatus = MEDIA_STATUS_UNKNOWN;
      }

   return clipStatus;
}

string CClipInitInfo::getClipStatusString() const
{
   EMediaStatus clipStatus = getClipStatus();

   return CTrack::queryMediaStatusName(clipStatus);
}

string CClipInitInfo::getCreateDateTime() const
{
   return createDateTime;
}

string CClipInitInfo::getDescription() const
{
   return description;
}

ECDIngestType CClipInitInfo::getCDIngestType() const
{
   return cdIngestType;
}

string CClipInitInfo::getDiskScheme() const
{
   return diskScheme_;
}

string CClipInitInfo::getArchiveDateTime() const
{
   return archiveDateTime;
}

// for display to user only; not to be used programmatically
string CClipInitInfo::getMediaLocation() const
{
   if (!diskScheme_.empty() && (diskScheme_ != "unknown"))
      {
      return diskScheme_;
      }
   else
      {
      CVideoInitInfo *videoInitInfo = getMainVideoInitInfo();
      if (videoInitInfo == 0)
         {
         return "unknown";
         }
      else
         {
         return videoInitInfo->getRepositoryIdentifier();
         }
      }
}

//------------------------------------------------------------------------
//
// Function:     getDurationTimecode
//
// Description:
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
CTimecode CClipInitInfo::getDurationTimecode() const
{
   // Create a CTimecode instance durationTimecode that has the same
   // framesPerSecond and flags as the Clip's In timecode.  Don't care
   // about the actual In time
   CTimecode durationTimecode = getInTimecode();

   // Force duration timecode to be non-dropframe so that
   // absolute time is reported as the duration
   durationTimecode.setDropFrame(false);

   // The frame count is the difference between the in and out timecodes
   int frameCount = getOutTimecode().getAbsoluteTime()
                    - getInTimecode().getAbsoluteTime();

   // If frame count is less than 0 (in timecode after out timecode)
   // then force frame count to zero
   if (frameCount < 0)
      frameCount = 0;

   // Set duration timecode with the frame count
   durationTimecode.setAbsoluteTime(frameCount);

   return durationTimecode;

} // getDurationTimecode

string CClipInitInfo::getImageFormatName() const
{
   // Squirrelly function that creates an "Image Format" name
   // from several parts

   string imageFormatName = "Unknown";   // Default name

   // Start by determining if there is a Main Video init info available
   if(doesNormalVideoExist())
      {
      // Get the image format for the main video
      CImageFormat* imageFormat = mainVideoInitInfo->getImageFormat();
      EImageFormatType imageFormatType = imageFormat->getImageFormatType();

      // Get a "simple" image format name for the main video
      imageFormatName
                 = CImageInfo::queryImageFormatTypeSimpleName(imageFormatType);

      // Add Drop Frame ND or DF for most 525 formats and 720P and 1080I formats
      // with 59.94 & 60 field rates.  Special 525 proxy formats and all other
      // formats are assumed to be Non-Drop
      switch(imageFormatType)
         {
         case IF_TYPE_525_5994 :     // Digital NTSC, "Standard Definition"
         case IF_TYPE_525_60 :       // Digital NTSC, "Standard Definition"
         case IF_TYPE_525_5994_DVS : // Digital NTSC, "Standard Definition"
         case IF_TYPE_525_60_DVS :   // Digital NTSC, "Standard Definition"
         case IF_TYPE_525IP_5994 :     // Digital NTSC, "Standard Def", Progressive
         case IF_TYPE_525IP_60 :       // Digital NTSC, "Standard Def", Progressive
         case IF_TYPE_525IP_5994_DVS : // Digital NTSC, "Standard Def", Progressive
         case IF_TYPE_525IP_60_DVS :   // Digital NTSC, "Standard Def", Progressive
         case IF_TYPE_525P_2398 :     // Digital NTSC, "Standard Def", Progressive
         case IF_TYPE_525P_24 :       // Digital NTSC, "Standard Def", Progressive
         case IF_TYPE_525P_2398_DVS : // Digital NTSC, "Standard Def", Progressive
         case IF_TYPE_525P_24_DVS :   // Digital NTSC, "Standard Def", Progressive
         case IF_TYPE_720P_5994 :    // 720 progressive-scan, 59.94 frames/second
         case IF_TYPE_720P_5994_DVS :// 720 progressive-scan, 59.94 frames/second
         case IF_TYPE_720P_60 :      // 720 progressive-scan, 60 frames/second
         case IF_TYPE_720P_60_DVS :  // 720 progressive-scan, 60 frames/second
         case IF_TYPE_1080I_5994 :   // 1080 interlaced, 59.94 fields/second
         case IF_TYPE_1080I_5994_DVS :   // 1080 interlaced, 59.94 fields/second
         case IF_TYPE_1080I_60 :     // 1080 interlaced, 60 fields/second
         case IF_TYPE_1080I_60_DVS :     // 1080 interlaced, 60 fields/second
            imageFormatName += CClip::queryDropFrameName(dropFrame);
			break;

		 default:
			break;
		 }

	  MTIostringstream ostr;

	  // Create a string for the frame rate
	  int frameRate;
	  switch(imageFormatType)
		 {
		 case IF_TYPE_1080I_50 :     // 1080 interlaced, 50 fields/second
		 case IF_TYPE_1080I_50_DVS :     // 1080 interlaced, 50 fields/second
		 case IF_TYPE_1080I_5994 :   // 1080 interlaced, 59.94 fields/second
		 case IF_TYPE_1080I_5994_DVS :   // 1080 interlaced, 59.94 fields/second
		 case IF_TYPE_1080I_60 :     // 1080 interlaced, 60 fields/second
		 case IF_TYPE_1080I_60_DVS :     // 1080 interlaced, 60 fields/second
			// Interlaced format, double the frame rate to get field rate
			frameRate
			 = (int)floor(2. * CImageInfo::queryNominalFrameRate(imageFormatType));
			ostr << frameRate;
			break;
		 case IF_TYPE_720P_50 :      // 720 progressive-scan, 50 frames/second
		 case IF_TYPE_720P_50_DVS :  // 720 progressive-scan, 50 frames/second
		 case IF_TYPE_720P_5994 :    // 720 progressive-scan, 59.94 frames/second
		 case IF_TYPE_720P_5994_DVS :// 720 progressive-scan, 59.94 frames/second
		 case IF_TYPE_720P_60 :      // 720 progressive-scan, 60 frames/second
		 case IF_TYPE_720P_60_DVS :  // 720 progressive-scan, 60 frames/second
		 case IF_TYPE_1080P_2398 :    // 1080 progressive-scan, 23.98 frames/second
		 case IF_TYPE_1080P_2398_DVS :// 1080 progressive-scan, 23.98 frames/second
		 case IF_TYPE_1080P_24 :      // 1080 progressive-scan, 24 frames/second
		 case IF_TYPE_1080P_24_DVS :  // 1080 progressive-scan, 24 frames/second
		 case IF_TYPE_1080P_25 :      // 1080 progressive-scan, 25 frames/second
		 case IF_TYPE_1080P_25_DVS :  // 1080 progressive-scan, 25 frames/second
		 case IF_TYPE_1080PSF_2398 :  // 1080 progressive-scan, 23.98 frames/second
		 case IF_TYPE_1080PSF_2398_DVS:// 1080 progressive-scan, 23.98 frames/second
		 case IF_TYPE_1080PSF_24 :    // 1080 progressive-scan, 24 frames/second
		 case IF_TYPE_1080PSF_24_DVS :// 1080 progressive-scan, 24 frames/second
		 case IF_TYPE_1080PSF_25 :    // 1080 progressive-scan, 25 frames/second
		 case IF_TYPE_1080PSF_25_DVS :// 1080 progressive-scan, 25 frames/second
		 case IF_TYPE_2048_1536PSF_24_DVS :
		 case IF_TYPE_2048_1536P_24_DVS :
		 case IF_TYPE_2048_1556PSF_1498_DVS :
		 case IF_TYPE_2048_1556PSF_15_DVS :
		 case IF_TYPE_2048_1556PSF_24_DVS :
		 case IF_TYPE_2048_1556P_24_DVS :
			// Progressive formats
			frameRate
			   = (int)floor(CImageInfo::queryNominalFrameRate(imageFormatType));
			ostr << frameRate;
			break;

		 default:
		 	break;
		 }

	  // Make a string holding the image dimensions for film formats or
	  // the number of bits per pixel component for video formats
	  switch (imageFormatType)
		 {
		 case IF_TYPE_FILE_DPX :     // SMPTE file format
         case IF_TYPE_FILE_CINEON :  // Kodak CINEON file format (similar to DPX)
         case IF_TYPE_FILE_TIFF :    // TIFF graphics file format
         case IF_TYPE_FILE_SGI :     // Silicon Graphics graphics file format
         case IF_TYPE_FILE_TGA :     // Targa file
         case IF_TYPE_FILE_TGA2 :    // Targa2 file
         case IF_TYPE_FILE_BMP :     // BMP file
         case IF_TYPE_FILE_EXR :     // EXR file
            ostr << imageFormat->getPixelsPerLine()
                 << 'x'
                 << imageFormat->getLinesPerFrame();
            break;

         default :
            ostr << '-' << (int)(imageFormat->getBitsPerComponent());
            break;
         }

      // Add the string
      imageFormatName += ostr.str();

      // If audio track exists, tack on "A" to indicate
      if (getAudioTrackCount() > 0)
         imageFormatName += "-A";
      }
   else
      {
      // Main video does not exist, so lets see if there is any audio
      if (getAudioTrackCount() > 0)
         imageFormatName = "Audio";
      }

   return imageFormatName;
}

CTimecode CClipInitInfo:: getInTimecode() const
{
   return inTimecode;
}

CVideoInitInfo* CClipInitInfo::getMainVideoInitInfo() const
{
   return mainVideoInitInfo;
}

CTimecode CClipInitInfo:: getOutTimecode() const
{
   return outTimecode;
}

CTimecode CClipInitInfo::getTimecodePrototype() const
{
   int tcFlags = (dropFrame) ? DROPFRAME : 0;
   int tcFps = 0;

   if (doesNormalVideoExist())
      {
      EImageFormatType imageFormatType
                  = mainVideoInitInfo->getImageFormat()->getImageFormatType();

      tcFps = CImageInfo::queryNominalTimecodeFrameRate(imageFormatType);

      tcFlags |= CImageInfo::queryIsFormat720P(imageFormatType) ? FRAME720P : 0;
      }

   return CTimecode(0, 0, 0, 0, tcFlags, tcFps);
}

int CClipInitInfo::getTimeTrackCount() const
{
   return (int)timeTrackInitInfoList.size();
}

CTimeInitInfo* CClipInitInfo::getTimeTrackInitInfo(int timeTrackIndex) const
{
   if (timeTrackIndex < 0 || timeTrackIndex >= getTimeTrackCount())
      return 0; // index is out-of-range

   return timeTrackInitInfoList[timeTrackIndex];
}

int CClipInitInfo::getVideoProxyCount() const
{
   return (int)videoProxyInitInfoList.size();
}

CVideoInitInfo* CClipInitInfo::getVideoProxyInitInfo(int videoProxyIndex) const
{
   if (videoProxyIndex < 0 || videoProxyIndex >= getVideoProxyCount())
      return 0;   // index is out-of-range

   return videoProxyInitInfoList[videoProxyIndex];
}

//////////////////////////////////////////////////////////////////////////////
////// THESE WERE MOVED HERE FROM TBinManagerForm by mbraca 2006-8-12/////////
//////////////////////////////////////////////////////////////////////////////

CTimecode CClipInitInfo::getVideoMediaDuration() const
{
   // Durations are supposed to be like like the in/outs, but with FPS = 0
   // I don't see the FPS being set to 0 here... mistake? QQQ
   CTimecode duration = getTimecodePrototype();
   // duration.setFramesPereSecond(0);   -- shouldn't we have to do this? QQQ
   duration.setFrameCount(0);

   auto pClip = getClipPtr();
   if (pClip == nullptr)
      {
      // Marked clips do this; there might be a better test for marked,
      // and pClip == 0 for non-marked clips would then be an error
      // In any case, the media duration is 0
      return duration;
      }

   // invariants: all non-virtual image tracks have equal amounts of
   // media allocated

   int videoProxyCount = pClip->getVideoProxyCount();
   vector<int> *allocedFrameCounts = new vector<int>(videoProxyCount);

   for (int i = 0; i < videoProxyCount; ++i)
      {
      CVideoFrameList *frameList =
         pClip->getVideoProxy(i)->getVideoFrameList(FRAMING_SELECT_VIDEO);
      (*allocedFrameCounts)[i] =
         frameList->getMediaAllocatedFrameCountCached();

      if (i > 0 && (*allocedFrameCounts)[i] != (*allocedFrameCounts)[i-1]
          && pClip->getVideoProxy(i)->getVirtualMediaFlag() != true)
         duration = CTimecode::NOT_SET;      // Error - should never happen?
      }

   // Here is a difference from the previous semantics (when the code was
   // hidden in TBinManagerForm)... on a media lengths mismatch error,
   // it used to return the duration timecode and a flag that said there
   // was an error. Now we just always return CTimecode::NOT_SET. In any
   // case, no caller made use of the returned timecode if the flag
   // indicated there was an error.

   if (duration == CTimecode::NOT_SET)
      {
      MTIostringstream ostr;
      ostr << "Warning: track media lengths differ:" << endl;
      for (int i = 0; i < videoProxyCount; ++i)
         {
         ostr << "    video track " << i << ": " << (*allocedFrameCounts)[i] << " frames" << endl;
         }

      TRACE_0(errout << ostr.str() );
      }
   else if (videoProxyCount > 0)
      {
      if ((*allocedFrameCounts)[0] >= 0)
         {
         duration.setFrameCount((*allocedFrameCounts)[0]);
         }
      else
         {
         // It seems we were unable to determine the media duration
         duration = CTimecode::NOT_SET;
         }
      }

   delete allocedFrameCounts;
   return duration;
}
//---------------------------------------------------------------------------

CTimecode  CClipInitInfo::getAudioMediaDuration() const
{
   // Durations are supposed to be like like the in/outs, but with FPS = 0
   // I don't see the FPS being set to 0 here... mistake? QQQ
   CTimecode duration = getTimecodePrototype();
   // duration.setFramesPereSecond(0);   -- shouldn't we have to do this? QQQ
   duration.setFrameCount(0);

   auto pClip = getClipPtr();
   if (pClip == nullptr)
      {
      // Marked clips do this; there might be a better test for marked,
      // and pClip == 0 for non-marked clips would then be an error
      // In any case, the media duration is 0
      return duration;
      }

   // invariants: all non-virtual audio tracks have equal amounts of
   // media allocated

   int audioTrackCount = pClip->getAudioTrackCount();
   vector<int> *allocedFrameCounts = new vector<int>(audioTrackCount);

   for (int i = 0; i < audioTrackCount; ++i)
      {
      CAudioFrameList *frameList =
         pClip->getAudioTrack(i)->getAudioFrameList(FRAMING_SELECT_VIDEO);
      (*allocedFrameCounts)[i] =
         frameList->getMediaAllocatedFrameCountCached();

      if (i > 0 && (*allocedFrameCounts)[i] != (*allocedFrameCounts)[i-1]
          && pClip->getAudioTrack(i)->getVirtualMediaFlag() != true)
         duration = CTimecode::NOT_SET;      // Error - should never happen?
      }

   // Here is a difference from the previous semantics (when the code was
   // hidden in TBinManagerForm)... on a media lengths mismatch error,
   // it used to return the duration timecode and a flag that said there
   // was an error. Now we just always return CTimecode::NOT_SET. In any
   // case, no caller made use of the returned timecode if the flag
   // indicated there was an error.

   if (duration == CTimecode::NOT_SET)
      {
      MTIostringstream ostr;
      ostr << "Warning: track media lengths differ:" << endl;
      for (int i = 0; i < audioTrackCount; ++i)
         {
         ostr << "    audio track " << i << ": " << (*allocedFrameCounts)[i] << " frames" << endl;
         }

      TRACE_0(errout << ostr.str() );
      }
   else if (audioTrackCount > 0)
      {
      if ((*allocedFrameCounts)[0] >= 0)
         {
         duration.setFrameCount((*allocedFrameCounts)[0]);
         }
      else
         {
         // It seems we were unable to determine the media duration
         duration = CTimecode::NOT_SET;
         }
      }

   delete allocedFrameCounts;
   return duration;
}

//////////////////////////////////////////////////////////////////////
// Mutators
//////////////////////////////////////////////////////////////////////

void CClipInitInfo::clearCreateDateTime()
{
   createDateTime.erase();
}

void CClipInitInfo::clearArchiveDateTime()
{
   archiveDateTime.erase();
}

void CClipInitInfo::setArchiveDateTime(const string& newDateTime)
{
   archiveDateTime = newDateTime;
}

void CClipInitInfo::setBinPath(const string& newBinPath)
{
   binPath = TrimWhiteSpace(newBinPath);
}

void CClipInitInfo::setClipName(const string& newClipName)
{
   clipName = TrimWhiteSpace(newClipName);
}

void CClipInitInfo::setClipPtr(ClipSharedPtr &newClipPtr)
{
   closeClip();
   CBinManager binMgr;
   binMgr.openClip(newClipPtr);    // bump ref count
   clip = newClipPtr;
}

void CClipInitInfo::setDescription(const string& newDescription)
{
   description = newDescription;
}

void CClipInitInfo::setCDIngestType(const ECDIngestType& newCDIngestType)
{
   cdIngestType = newCDIngestType;
}

void CClipInitInfo::setDiskScheme(const string& newDiskScheme)
{
   diskScheme_ = newDiskScheme;
}

void CClipInitInfo::setInTimecode(const CTimecode& newInTimecode)
{
   inTimecode = newInTimecode;
}

void CClipInitInfo::setOutTimecode(const CTimecode& newOutTimecode)
{
   outTimecode = newOutTimecode;
}

ClipSharedPtr CClipInitInfo::createClip(int *status)
{
   CBinManager binMgr;

   return binMgr.createClip(*this, status);
}

void CClipInitInfo::setMarkedClipMediaLoc(const string &newMediaLoc)
{
   markedClipMediaLoc = newMediaLoc;
}

string CClipInitInfo::getMarkedClipMediaLoc() const
{
   return markedClipMediaLoc;
}

//////////////////////////////////////////////////////////////////////
// Clip Init Info Validation
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    validate
//
// Description: Check the contents of this CClipInitInfo and
//
// Arguments:   None
//
// Returns:     0 = Success
//
//------------------------------------------------------------------------
int CClipInitInfo::validate()
{
   int i, retVal;
   CAutoErrorReporter autoErr("CClipInitInfo::validate",
                              AUTO_ERROR_NO_NOTIFY, AUTO_ERROR_TRACE_0);

   // Bin Path
   if (binPath.empty())
      {
      autoErr.errorCode = CLIP_ERROR_INVALID_BIN_PATH;
      autoErr.msg << "Missing Bin Path name";
      return autoErr.errorCode;
      }

   // Clip Name
   if (clipName.empty())
      {
      autoErr.errorCode = CLIP_ERROR_INVALID_CLIP_NAME;
      autoErr.msg << "Missing Clip name";
      return autoErr.errorCode;
      }

   // Path too long
   if ((AddDirSeparator(binPath).size() + clipName.size()) >= (MAX_PATH - 1))
   {
      autoErr.errorCode = CLIP_ERROR_INVALID_CLIP_NAME;
      autoErr.msg << "Clip path too long";
      return autoErr.errorCode;
   }

   // Check for characters that Windows and clip library does not like in clip names
   string badChars = ".\\/:*?\"<>|#";
   if (clipName.find_first_of(badChars) != string::npos)
      {
      autoErr.errorCode = CLIP_ERROR_INVALID_CLIP_NAME_CHARACTERS;
      autoErr.msg << "Invalid characters in clip name";
      return autoErr.errorCode;
      }

   // In and Out Timecodes
   if ((outTimecode - inTimecode) < 1)
      {
      autoErr.errorCode =  CLIP_ERROR_INVALID_TIME_CODE;
      autoErr.msg << "Invalid time codes";
      return autoErr.errorCode;
      }

   // Check for vacuous init info
   int audioTrackCount = getAudioTrackCount();
   if (!doesNormalVideoExist() && audioTrackCount == 0)
      {
      // HACK ALERT - Marked clips have no video or audio. I don't know how
      // it ever got past this check, but it doesn't for me, and I can't
      // figure out how it's supposed to work, so I'll just hack it to
      // check if we set the squirrely "marked clip media loc" and not crap
      // out here if so...

      if (markedClipMediaLoc.empty())
      {

      // The clip init info has neither Main Video or Audio Track init info,
      // so this is probably bogus
      autoErr.errorCode = CLIP_ERROR_INVALID_EMPTY_CLIP;
      autoErr.msg << "Bad clip initialization data";
      return autoErr.errorCode;
      }
      }

   // Check Video init info for Main video and perhaps video proxies
   if (doesNormalVideoExist())
      {
      // Check Licensing for Resolution Independence
      CImageFormat* imageFormat = mainVideoInitInfo->getImageFormat();
      int iNRow = imageFormat->getLinesPerFrame();
      int iNCol = imageFormat->getPixelsPerLine();

      if (!IsResolutionLicensed(iNCol, iNRow))
         {
         autoErr.errorCode = CLIP_ERROR_MAX_LICENSE_RESOLUTION_EXCEEDED;
         autoErr.msg << "Clip resolution not licensed, y " << iNCol << " x " << iNRow;
         return autoErr.errorCode;
         }

      // Check the Normal Video init info for the new clip
      retVal = mainVideoInitInfo->validate();
      if (retVal != 0)
         {
         autoErr.RepeatTheError();
         return retVal;
         }

      retVal = checkTimecodes(mainVideoInitInfo->getHandleCount());
      if (retVal != 0)
         {
         autoErr.RepeatTheError();
         return retVal;
         }

      // Now check Video Proxies
      int videoProxyCount = getVideoProxyCount();
      for (i = 0; i < videoProxyCount; ++i)
         {
         retVal = videoProxyInitInfoList[i]->validate();
         if (retVal != 0)
            {
            autoErr.RepeatTheError();
            return retVal;
            }
         }
      }

   // Now check Audio Tracks
   for (i = 0; i < audioTrackCount; ++i)
      {
      retVal = audioTrackInitInfoList[i]->validate();
      if (retVal != 0)
         {
         autoErr.RepeatTheError();
         return retVal;
         }

      retVal = checkTimecodes(audioTrackInitInfoList[i]->getHandleCount());
      if (retVal != 0)
         {
         autoErr.RepeatTheError();
         return retVal;
         }
      }

   // Check Time Tracks
   int timeTrackCount = getTimeTrackCount();
   for (i = 0; i < timeTrackCount; ++i)
      {
      retVal = timeTrackInitInfoList[i]->validate();
      if (retVal != 0)
         {
         autoErr.RepeatTheError();
         return retVal;
         }
      }

   return 0;
}

int CClipInitInfo::checkTimecodes(int handleCount)
{
   CAutoErrorReporter autoErr("CClipInitInfo::checkTimecodes",
                              AUTO_ERROR_NO_NOTIFY, AUTO_ERROR_NO_TRACE);

   if (handleCount < 1)
      return 0;

   // Determine if the timecodes are frame counters or regular HH:MM:SS:FF
   int framesPerSecond = inTimecode.getFramesPerSecond();
   int maxFrames;

   if (framesPerSecond == 0)
      {
      // In & Out Timecodes are Frame Counters
      maxFrames = MAXFRAMES;  // Constant from CTimecode
      }
   else
      {
      // In & Out Timecodes are regular HH:MM:SS:FF timecodes
      // Maximum allowed is 99:59:59:ff - handleCount
      maxFrames = 100 * 60 * 60 * framesPerSecond - 1;
      }

   if (inTimecode.getAbsoluteTime() < handleCount
       || maxFrames - outTimecode.getAbsoluteTime() < handleCount)
         {
         autoErr.errorCode = CLIP_ERROR_BAD_HANDLE_TIMECODE;
         autoErr.msg << "Invalid timecodes";
         return autoErr.errorCode;
         }

   return 0;
}

int CClipInitInfo::checkMediaStorage()
{
   int i, retVal;

   int frameCount;
   int clipFrameCount = getOutTimecode() - getInTimecode();

   if (doesNormalVideoExist())
      {
      frameCount = mainVideoInitInfo->getMediaAllocationFrameCount();
      if (frameCount == -1) frameCount = clipFrameCount;

      // Check the Normal Video (Proxy) to the new clip
      retVal = mainVideoInitInfo->checkMediaStorage(frameCount);
      if (retVal != 0)
         return retVal;

      // Now check Video Proxies
      int videoProxyCount = getVideoProxyCount();
      for (i = 0; i < videoProxyCount; ++i)
         {
         frameCount =
             videoProxyInitInfoList[i]->getMediaAllocationFrameCount();
         if (frameCount == -1) frameCount = clipFrameCount;

         retVal = videoProxyInitInfoList[i]->checkMediaStorage(frameCount);
         if (retVal != 0)
            return retVal;
         }
      }

   // Now check Audio Tracks
   int audioTrackCount = getAudioTrackCount();
   for (i = 0; i < audioTrackCount; ++i)
      {
      frameCount = audioTrackInitInfoList[i]->getMediaAllocationFrameCount();
      if (frameCount == -1) frameCount = clipFrameCount;

      retVal = audioTrackInitInfoList[i]->checkMediaStorage(frameCount);
      if (retVal != 0)
         return retVal;
      }

   return 0;
}

// Function: checkMetaDataStorage
//
// Description: checks that there is enough storage on the clip
// directory volume to safely create the clip
int CClipInitInfo::checkMetadataStorage()
{
   // don't know how much storage is really necessary; use a heuristic
   // instead: < 95% of volume in use --> success
#ifdef _WINDOWS
#ifndef _MSC_VER
#if 0 //qqqq
// MSVC++ 6 gives: error C2520: conversion from unsigned __int64 to
// double not implemented, use signed __int64; disabling this code in
// MSVC is the most expedient solution
   ULARGE_INTEGER lFreeBytesAvailableToCaller;	// variable to
                                                // receive free
                                                // bytes on disk
                                                // available to
                                                // the caller
   ULARGE_INTEGER lTotalNumberOfBytes; // variable to receive
   // number of bytes on disk
   ULARGE_INTEGER lTotalNumberOfFreeBytes; // variable to receive
   // free bytes on disk
   bool bRet = ::GetDiskFreeSpaceEx(binPath.c_str(),
                                    &lFreeBytesAvailableToCaller,
                                    &lTotalNumberOfBytes,
                                    &lTotalNumberOfFreeBytes);
   if (bRet == 0) // failure
      {
      return -1;
      }

   if ((double)lFreeBytesAvailableToCaller.QuadPart /
       (double)lTotalNumberOfBytes.QuadPart < .05)
      {
      return CLIP_ERROR_INSUFFICIENT_METADATA_STORAGE;
      }
#endif
#endif
#endif

   return 0;
}

int CClipInitInfo::CheckRFL()
{
   int retVal;

   if (doesNormalVideoExist())
      {
      // Check the Normal Video (Proxy) to the new clip
      retVal = mainVideoInitInfo->CheckRFL();
      if (retVal != 0)
         return retVal;

      // Now check Video Proxies
      int videoProxyCount = getVideoProxyCount();
      for (int i = 0; i < videoProxyCount; ++i)
         {
         retVal = videoProxyInitInfoList[i]->CheckRFL();
         if (retVal != 0)
            return retVal;
         }
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Clip Scheme File Interface
//////////////////////////////////////////////////////////////////////

int CClipInitInfo::readClipScheme(CIniFile *schemeFile)
{
   int retVal;

   retVal = readClipInfo(schemeFile);
   if (retVal != 0)
      return retVal;

   bool mainVideoInterlaced = false;
   double mainVideoFrameRate = 0.0;
   EFrameRate mainVideoFrameRateEnum = IF_FRAME_RATE_INVALID;

   if (schemeFile->SectionExists(mainVideoSectionName))
      {
      if (!doesNormalVideoExist())
         mainVideoInitInfo = new CVideoInitInfo;
      retVal = mainVideoInitInfo->readSection(schemeFile, mainVideoSectionName,
                                              sourceType);
      if (retVal != 0)
         return retVal;

      // Get the interlaced flag and frame-rate from the main video
      // init info, to be used later with the audio init
      CImageFormat *mainVideoImageFormat = mainVideoInitInfo->getImageFormat();
      EImageFormatType imageFormatType;
      imageFormatType = mainVideoImageFormat->getImageFormatType();
      if (imageFormatType != IF_TYPE_INVALID)
         {
         mainVideoInterlaced = mainVideoImageFormat->getInterlaced();
         mainVideoFrameRate = CImageInfo::queryNominalFrameRate(imageFormatType);
         mainVideoFrameRateEnum = CImageInfo::queryNominalFrameRateEnum(imageFormatType);
         }

      // If main video init info does not have its frame rate set, use
      // the default values from the main video image format
      if (mainVideoInitInfo->getFrameRateEnum() == IF_FRAME_RATE_INVALID)
         {
         mainVideoInitInfo->setFrameRate(mainVideoFrameRateEnum,
                                         mainVideoFrameRate);
         }
      }

   MTIostringstream ostr;
   string emptyString;
   int videoProxyIndex = 1;
   bool done = false;
   while(!done)
      {
      ostr.str(emptyString);
      ostr << videoProxySectionName << videoProxyIndex;
      if (schemeFile->SectionExists(ostr.str()))
         {
         CVideoInitInfo *videoInitInfo = new CVideoInitInfo;
         videoProxyInitInfoList.push_back(videoInitInfo);
         retVal = videoInitInfo->readSection(schemeFile, ostr.str(),
                                             sourceType);
         if (retVal != 0)
            return retVal;

         // If video init info does not have its frame rate set, use
         // the default values from the main video image format
         if (videoInitInfo->getFrameRateEnum() == IF_FRAME_RATE_INVALID)
            {
            videoInitInfo->setFrameRate(mainVideoFrameRateEnum,
                                        mainVideoFrameRate);
            }

         ++videoProxyIndex;
         }
      else
         done = true;
      }

   // TBD: Set interlace and samplesPerField based on image format of main
   //      video.


   done = false;
   int audioTrackIndex = 1;
   while(!done)
      {
      ostr.str(emptyString);
      ostr << audioTrackSectionName << audioTrackIndex;
      if (schemeFile->SectionExists(ostr.str()))
         {
         CAudioInitInfo *audioInitInfo = new CAudioInitInfo;
         audioTrackInitInfoList.push_back(audioInitInfo);

         // Set interlace and frame rate in audio init info prior
         // to reading audio track section
         if (doesNormalVideoExist())
            {
            audioInitInfo->getAudioFormat()->setInterlaced(mainVideoInterlaced);
            audioInitInfo->setFrameRate(mainVideoFrameRateEnum, mainVideoFrameRate);
            }

         retVal = audioInitInfo->readSection(schemeFile, ostr.str(),
                                             sourceType);
         if (retVal != 0)
            return retVal;

         ++audioTrackIndex;
         }
      else
         done = true;
      }

   done = false;
   int timeTrackIndex = 1;
   while(!done)
      {
      ostr.str(emptyString);
      ostr << timeTrackSectionName << timeTrackIndex;
      if (schemeFile->SectionExists(ostr.str()))
         {
         CTimeInitInfo *timeInitInfo = new CTimeInitInfo;
         timeTrackInitInfoList.push_back(timeInitInfo);

         retVal = timeInitInfo->readSection(schemeFile, ostr.str());
         if (retVal != 0)
            return retVal;

         ++timeTrackIndex;
         }
      else
         done = true;
      }

   return 0;

}  // readClipScheme

int CClipInitInfo::readClipScheme2(CIniFile *schemeFile)
{
   int retVal;

   if (schemeFile->SectionExists(mainVideoSectionName))
      {
      retVal = mainVideoInitInfo->readSection2(schemeFile, mainVideoSectionName,
                                               sourceType);
      if (retVal != 0)
         return retVal;
      }

   return 0;

}  // readClipScheme2

int CClipInitInfo::readClipInfo(CIniFile *schemeFile)
{
   string valueString, emptyString;

   // Read Bin Path, default is the current value
   valueString = schemeFile->ReadFileName(clipInfoSectionName, binPathKey,
                                          binPath);
   setBinPath(valueString);

   // Read the Clip Name, default is the current value
   valueString = schemeFile->ReadString(clipInfoSectionName, clipNameKey,
                                        clipName);
   setClipName(valueString);

   // Read the Create Date and Time
   valueString = schemeFile->ReadString(clipInfoSectionName, createDateTimeKey,
                                        emptyString);
   createDateTime = valueString;

   // Read the Archive Date and Time
   valueString = schemeFile->ReadString(clipInfoSectionName, archiveDateTimeKey,
                                        emptyString);
   archiveDateTime = valueString;

   // Read the Description, default is the current value
   // HACK: For 64 bits we changed the Bin Manager's DESCRIPTION column to
   // be editable COMMENTS, so we don't want to iniitalize that to
   // "Existing Image Files" (from the AutoDetect clip scheme) which doesn't
   // convey any information!
   // UPDATE: PER LARRY: Always set the description to ""
   ////valueString = schemeFile->ReadString(clipInfoSectionName, descriptionKey,
   ////                                     description);
   ////if (0 == stricmp(valueString.c_str(), "existing image files"))
   ////{
   ////   valueString = "";
   ////}
   ////
   ////setDescription(valueString);
   setDescription("");

   // Read the CDIngestType, default is the current value
   string defaultString = CClip::queryCDIngestTypeName(cdIngestType);
   ECDIngestType newCDIngestType;

   valueString = schemeFile->ReadString(clipInfoSectionName, cdIngestTypeKey,
                                        defaultString);
   newCDIngestType = CClip::queryCDIngestType(valueString);
   setCDIngestType(newCDIngestType);

   // Read the Drop Frame
   valueString = schemeFile->ReadString(clipInfoSectionName, dropFrameKey,
                                        CClip::queryDropFrameName(dropFrame));
   dropFrame = CClip::queryDropFrame(TrimWhiteSpace(valueString));

   // Read the Image Format Type from Image Format section
   EImageFormatType newImageFormatType;
   string mainVideoSection = "MainVideo";
   newImageFormatType = CImageFormat::readImageFormatType(schemeFile,
                                                          mainVideoSection);
   int timecodeFlags = 0;
   int timecodeFramesPerSecond;
   if (newImageFormatType != IF_TYPE_INVALID)
      {
      if (CImageInfo::queryIsFormat720P(newImageFormatType))
         timecodeFlags |= FRAME720P;
      timecodeFramesPerSecond
               = CImageInfo::queryNominalTimecodeFrameRate(newImageFormatType);
      }
   else
      {
      timecodeFlags = 0;
      timecodeFramesPerSecond = 0;
      }

   // Read the Frame Rate from the Main Video section if it is present
   string frameRateKey = "FrameRate";   // WTF?? why not use the class var??
   if (schemeFile->KeyExists(mainVideoSection, frameRateKey))
      {
      valueString = schemeFile->ReadString(mainVideoSection, frameRateKey,
                                           emptyString);
      double newFrameRate = 0.;  // avoid uninitialized variable
      EFrameRate newFrameRateEnum;
      newFrameRateEnum = CImageInfo::queryFrameRate(valueString, newFrameRate);
      if (newFrameRateEnum == IF_FRAME_RATE_INVALID)
         {
         TRACE_0(errout << "ERROR: Clip scheme has bogus frame rate:" << endl
                        << "       " << schemeFile->FileName);
         return CLIP_ERROR_INVALID_FRAME_RATE;
         }
      timecodeFramesPerSecond = (int)(newFrameRate + 0.5);
      }

   // Read the timecode frame rate.  If it doesn't exist, then just
   // use a default frame rate
   timecodeFramesPerSecond = schemeFile->ReadInteger(clipInfoSectionName,
                                                     timecodeFrameRateKey,
                                                     timecodeFramesPerSecond);

   if (dropFrame)
      timecodeFlags |= DROPFRAME;

   CTimecode newTimecode(timecodeFlags, timecodeFramesPerSecond);
   string timeString, defaultTimeString;
   char timecodeCString[20];

   // Read In Timecodes
   inTimecode.getTimeASCII(timecodeCString);
   defaultTimeString = timecodeCString;
   timeString = schemeFile->ReadString(clipInfoSectionName, inTimecodeKey,
                                       defaultTimeString);
   timeString = TrimWhiteSpace(timeString);
   newTimecode.setTimeASCII(timeString.c_str());
   setInTimecode(newTimecode);

   // Read Out Timecode
   outTimecode.getTimeASCII(timecodeCString);
   defaultTimeString = timecodeCString;
   timeString = schemeFile->ReadString(clipInfoSectionName, outTimecodeKey,
                                       defaultTimeString);
   timeString = TrimWhiteSpace(timeString);
   newTimecode.setTimeASCII(timeString.c_str());
   setOutTimecode(newTimecode);

   // HACK - read media location squirreled away for marked clip  ... UGGH ...
   valueString = schemeFile->ReadString(clipInfoSectionName, markedClipMediaLocKey,
                                        emptyString);
   markedClipMediaLoc = valueString; // What is the purpose of this indirection?

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    writeClipScheme
//
// Description: Creates a Clip Scheme file given a CClipInitInfo instance
//              and its constituents.
//              CIniFile is used to create and write the file.
//
// Arguments:   CIniFile *schemeFile
//
// Returns:     0 = Success
//
//------------------------------------------------------------------------
int CClipInitInfo::writeClipScheme(CIniFile *schemeFile)
{
   int retVal;

   retVal = writeClipInfo(schemeFile);
   if (retVal != 0)
      return retVal; // ERROR

   if (doesNormalVideoExist())
      {
      // Write the section for the main video init info
      retVal = mainVideoInitInfo->writeSection(schemeFile,
                                               mainVideoSectionName);
      if (retVal != 0)
         return retVal;
      }

      MTIostringstream ostr;
      string emptyString;

      // Loop over all of the Video Proxies
      int videoProxyCount = getVideoProxyCount();
      for (int videoProxyIndex = 0;
           videoProxyIndex < videoProxyCount;
           ++videoProxyIndex)
         {
         CVideoInitInfo *videoInitInfo = getVideoProxyInitInfo(videoProxyIndex);

         // Make name for section, e.g., "VideoProxy1"
         ostr.str(emptyString);
         ostr << videoProxySectionName << videoProxyIndex;

         // Write the section for this proxy Video Init Info
         retVal = videoInitInfo->writeSection(schemeFile, ostr.str());
         if (retVal != 0)
            return retVal;
         }

   int audioTrackCount = getAudioTrackCount();
   for (int audioTrackIndex = 0;
        audioTrackIndex < audioTrackCount;
        ++audioTrackIndex)
      {
      CAudioInitInfo *audioInitInfo = getAudioTrackInitInfo(audioTrackIndex);

      // Make name for section, e.g., "AudioTrack0"
      ostr.str(emptyString);
      // In the following, mbraca added "+1" to make the names 1-based
      // to match readClipScheme() -- bug 2338
      ostr << audioTrackSectionName << (audioTrackIndex + 1);

      // Write the section for this track Audio Init Info
      retVal = audioInitInfo->writeSection(schemeFile, ostr.str());
      if (retVal != 0)
         return retVal;
      }

   int timeTrackCount = getTimeTrackCount();
   for (int timeTrackIndex = 0;
        timeTrackIndex < timeTrackCount;
        ++timeTrackIndex)
      {
      CTimeInitInfo *timeInitInfo = getTimeTrackInitInfo(timeTrackIndex);

      // Make name for section, e.g., "TimeTrack0"
      ostr.str(emptyString);
      ostr << timeTrackSectionName << timeTrackIndex;

      // Write the section for this Time track Init Info
      retVal = timeInitInfo->writeSection(schemeFile, ostr.str());
      if (retVal != 0)
         return retVal;
      }

   return 0;   // Success

} // writeClipScheme

int CClipInitInfo::writeClipInfo(CIniFile *schemeFile)
{
   string valueString, emptyString;

   // Write Bin Path
   schemeFile->WriteString(clipInfoSectionName, binPathKey, getBinPath());

   // Write the Clip Name
   schemeFile->WriteString(clipInfoSectionName, clipNameKey, getClipName());

   // Write the Create Date and Time
   schemeFile->WriteString(clipInfoSectionName, createDateTimeKey,
                           createDateTime);

   // Write the Archive Date and Time
   schemeFile->WriteString(clipInfoSectionName, archiveDateTimeKey,
                           archiveDateTime);

   // Write the Description
   schemeFile->WriteString(clipInfoSectionName, descriptionKey,
                           getDescription());

   // Write the CDIngestType
   schemeFile->WriteString(clipInfoSectionName, cdIngestTypeKey,
                           CClip::queryCDIngestTypeName(getCDIngestType()));

   // Write the Drop Frame
   schemeFile->WriteString(clipInfoSectionName, dropFrameKey,
                           CClip::queryDropFrameName(dropFrame));

   // Write the Timecode Frame Rate
   schemeFile->WriteInteger(clipInfoSectionName, timecodeFrameRateKey,
                            inTimecode.getFramesPerSecond());

   string timecodeString;

   // Write In Timecodes
   inTimecode.getTimeString(timecodeString);
   timecodeString = TrimWhiteSpace(timecodeString);
   schemeFile->WriteString(clipInfoSectionName, inTimecodeKey, timecodeString);

   // Write Out Timecode
   outTimecode.getTimeString(timecodeString);
   timecodeString = TrimWhiteSpace(timecodeString);
   schemeFile->WriteString(clipInfoSectionName, outTimecodeKey, timecodeString);

   // HACK - Write the squirreled-away media loc for marked clips ... UGGH ...
   schemeFile->WriteString(clipInfoSectionName, markedClipMediaLocKey,
                           getMarkedClipMediaLoc());


   return 0;

} // writeClipInfo

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                             ##################################
// ###     CCommonInitInfo Class     #################################
// ####                             ##################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCommonInitInfo::CCommonInitInfo()
: clipVer(CLIP_VERSION_3),
  frameRate(0.0), frameRateEnum(IF_FRAME_RATE_INVALID),
  handleCount(0),
  virtualMediaFlag(false),
  mediaType(MEDIA_TYPE_INVALID),
  mediaAllocationFrameCount(-1),
  mediaAllocator(0),
  mediaFormat(0)
{
}

CCommonInitInfo::~CCommonInitInfo()
{
   delete mediaFormat;
}

//////////////////////////////////////////////////////////////////////
// Initialization
//////////////////////////////////////////////////////////////////////

void CCommonInitInfo::init()
{
   string emptyString;

   clipVer = CLIP_VERSION_3;

   setAppId(emptyString);

   handleCount = 0;

   virtualMediaFlag = false;

   setRepositoryIdentifier(emptyString);
   setMediaType(MEDIA_TYPE_INVALID);
   setMediaIdentifier(emptyString);
   setMediaAllocator(0);
}

int CCommonInitInfo::initFromTrack(CTrack* track)
{
   int retVal;

   // TBD: Figure out the track's schema version, should be 3 or 5L

   setAppId(track->getAppId());

   handleCount = track->getHandleCount();

   // mlm: use the track's flag to fix bug that prevents using
   // CClipInitInfo to determine video store usage of video stores of
   // a clip.
   virtualMediaFlag = track->getVirtualMediaFlag();

   if (track->getClipVer() == CLIP_VERSION_3)
      {
      const CMediaStorageList* mediaStorageList;
      mediaStorageList = track->getMediaStorageList();
      if (mediaStorageList->getMediaStorageCount() > 0)
         {
         CMediaStorage* mediaStorage = mediaStorageList->getMediaStorage(0);
         mediaType = mediaStorage->getMediaType();
         mediaIdentifier = mediaStorage->getMediaIdentifier();
         }
      }
   else if (track->getClipVer() == CLIP_VERSION_5L)
      {
      C5MediaTrackHandle trackHandle = track->GetTrackHandle();
      retVal = trackHandle.GetFirstMediaType(mediaType);
      if (retVal != 0)
         return retVal;
      retVal = trackHandle.GetFirstMediaIdentifier(mediaIdentifier);
      if (retVal != 0)
         return retVal;
      }
   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;


   string repositoryName(mediaIdentifier);

   // QQQ THIS SHOULD BE HIDDEN IN THE INDIVIDUAL MEDIA ACCESS CLASSES
   if (mediaIdentifier.size() > 2 && mediaIdentifier.substr(0,2) == "\\\\")
      {
      // remote file - show first three components (host/disk/folder)
      // REMOVED 2/28/2019 because browse to file folder was wrong
//      string::size_type slashPos = 1, oldSlashPos = 0;
//      for (int i = 0; i < 4; ++i)
//         {
//         if (slashPos == string::npos || slashPos >= mediaIdentifier.size())
//            break;
//         oldSlashPos = slashPos;
//         slashPos = mediaIdentifier.find('\\', slashPos + 1);
//         }
//      if (oldSlashPos > 0)
//         repositoryName = mediaIdentifier.substr(0, oldSlashPos);

               repositoryName = RemoveDirSeparator(GetFilePath(mediaIdentifier));
      }
   else if (mediaIdentifier.size() > 2 && mediaIdentifier.substr(1,2) == ":\\")
      {
      // local file - show full folder path
      repositoryName = RemoveDirSeparator(GetFilePath(mediaIdentifier));
      }
   else
      {
      // chop off at colon - image repository
      string::size_type colonPos = mediaIdentifier.find(':');
      if (colonPos != string::npos)
         repositoryName = mediaIdentifier.substr(0, colonPos);
      }
   setRepositoryIdentifier(repositoryName);

   // Set the frame rate
   CFrameList *frameList = track->getBaseFrameList(FRAMING_SELECT_VIDEO);
   setFrameRate(frameList->getFrameRateEnum(), frameList->getFrameRate());

   return 0; // Success
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

string CCommonInitInfo::getAppId() const
{
   return appId;
}

double CCommonInitInfo::getFrameRate() const
{
   return frameRate;
}

EFrameRate CCommonInitInfo::getFrameRateEnum() const
{
   return frameRateEnum;
}

int CCommonInitInfo::getHandleCount() const
{
   return handleCount;
}

string CCommonInitInfo::getRepositoryIdentifier() const
{
   return repositoryIdentifier;
}

EMediaType CCommonInitInfo::getMediaType() const
{
   return mediaType;
}

string CCommonInitInfo::getMediaIdentifier() const
{
   return mediaIdentifier;
}

string CCommonInitInfo::getHistoryDirectory() const
{
   return historyDirectory;
}

int CCommonInitInfo::getMediaAllocationFrameCount() const
{
   return mediaAllocationFrameCount;
}

CMediaAllocator* CCommonInitInfo::getMediaAllocator() const
{
   return mediaAllocator;
}

//////////////////////////////////////////////////////////////////////
// Mutators
//////////////////////////////////////////////////////////////////////

void CCommonInitInfo::setAppId(const string &newAppId)
{
   appId = newAppId;
}

void CCommonInitInfo::setClipVer(EClipVersion newClipVer)
{
   clipVer = newClipVer;
}

void CCommonInitInfo::setFrameRate(EFrameRate newFrameRateEnum,
                                   double newFrameRate)
{
   frameRateEnum = newFrameRateEnum;

   if (frameRateEnum == IF_FRAME_RATE_NON_STANDARD)
      {
      // Caller is supplying a non-standard frame rate,
      // so copy newFrameRate argument to frameRate member
      frameRate = newFrameRate;
      }
   else
      {
      // Caller is requesting a standard frame rate, so query for the
      // corresponding floating pt frame rate value and set frameRate member
      frameRate = CImageInfo::queryStandardFrameRate(newFrameRateEnum);
      }
}

void CCommonInitInfo::setHandleCount(int newHandleCount)
{
   handleCount = newHandleCount;
}

void CCommonInitInfo::setRepositoryIdentifier(const string & newRepositoryIdentifier)
{
   repositoryIdentifier = newRepositoryIdentifier;
}

// Media Stuff should be referenced to Video Proxies and Audio Tracks
void CCommonInitInfo::setMediaType(EMediaType newMediaType)
{
   mediaType = newMediaType;
}

void CCommonInitInfo::setMediaIdentifier(const string& newMediaIdentifier)
{
   mediaIdentifier = TrimWhiteSpace(newMediaIdentifier);
}

void CCommonInitInfo::setHistoryDirectory(const string& newHistoryDirectory)
{
   historyDirectory = TrimWhiteSpace(newHistoryDirectory);
}

void CCommonInitInfo::setMediaAllocationFrameCount(int newFrameCount)
{
   mediaAllocationFrameCount = newFrameCount;
}

void CCommonInitInfo::setMediaAllocator(CMediaAllocator *newMediaAllocator)
{
   mediaAllocator = newMediaAllocator;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CCommonInitInfo::validate()
{
   CAutoErrorReporter autoErr("CCommonInitInfo::validate",
                              AUTO_ERROR_NO_NOTIFY, AUTO_ERROR_NO_TRACE);

   if (clipVer != CLIP_VERSION_3 && clipVer != CLIP_VERSION_5L)
      {
      autoErr.errorCode = CLIP_ERROR_INVALID_CLIP_VERSION;
      autoErr.msg << "Invalid clip version " << clipVer;
      return autoErr.errorCode;
      }

   if (handleCount < 0)
      {
      autoErr.errorCode = CLIP_ERROR_INVALID_HANDLE_COUNT;
      autoErr.msg << "Invalid handle count " << handleCount;
      return autoErr.errorCode;
      }

   return 0;
}

int CCommonInitInfo::checkMediaStorage(int frameCount) const
{
   int retVal;
   CAutoErrorReporter autoErr("CCommonInitInfo::checkMediaStorage",
                              AUTO_ERROR_NO_NOTIFY, AUTO_ERROR_NO_TRACE);

   if (virtualMediaFlag)
      return 0;   // virtual media, don't check media space

   // If the media allocator is supplied in this init info, then
   // assume that the media allocator is okay
   if (mediaAllocator != 0)
      return 0;

   if (getMediaAllocationFrameCount() == -1)
       {
       // Frame count has not been overridden, so add the handle
       // frames to preserve behavior that existed before it was
       // possible to override frame count.  If the frame count was
       // overridden, respect the caller's wishes.
       frameCount += 2 * handleCount;
       }

   CMediaInterface mediaInterface;
   retVal = mediaInterface.checkSpace(mediaType, mediaIdentifier,
                                      *mediaFormat, frameCount);
   if (retVal != 0)
      {
      if (retVal == CLIP_ERROR_INSUFFICIENT_MEDIA_STORAGE)
         autoErr.msg << "Not enough disk space";
      else
         autoErr.msg << "Invalid media identifier " << mediaIdentifier;

      return retVal; // ERROR
      }

   return 0; // Success
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CCommonInitInfo::readSection(CIniFile *schemeFile,
                                 const string& sectionName,
                                 EClipInitSource sourceType)
{
   string emptyString, valueString;

   // Read the ClipVer, if it is present.  This is probably temporary
   // to support development and introduction of Clip 5
   valueString = schemeFile->ReadString(sectionName, clipVerKey,
                              CClip::clipVerLookup.FindByValue(CLIP_VERSION_3));
   clipVer = CClip::clipVerLookup.FindByStr(valueString, CLIP_VERSION_INVALID);
   if (clipVer == CLIP_VERSION_INVALID)
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   // Read the Application ID, if it is present
   if (schemeFile->KeyExists(sectionName, appIdKey))
      {
      valueString = schemeFile->ReadString(sectionName, appIdKey,
                                           emptyString);
      setAppId(valueString);
      }

   // Use the Image Format Type from Image Format section, if it exists,
   // to set a default value for Handle Count
   EImageFormatType newImageFormatType;
   newImageFormatType = CImageFormat::readImageFormatType(schemeFile,
                                                           sectionName);
   if (newImageFormatType != IF_TYPE_INVALID)
      {
      handleCount = CImageInfo::queryNominalHandleCount(newImageFormatType);
      }

   // QQQ WHY ARE ALL THESE READS PROTECTED BY if (KeyExists) ???

   // Read the Handle Count key, if it is present
   if (schemeFile->KeyExists(sectionName, handleCountKey))
      {
      handleCount = schemeFile->ReadInteger(sectionName, handleCountKey,
                                            -1);
      if (handleCount < 0)
         return CLIP_ERROR_INVALID_HANDLE_COUNT;
      }

   // Read the Frame Rate if it is present
   if (schemeFile->KeyExists(sectionName, frameRateKey))
      {
      valueString = schemeFile->ReadString(sectionName, frameRateKey,
                                           emptyString);
      double newFrameRate;
      EFrameRate newFrameRateEnum;
      newFrameRateEnum = CImageInfo::queryFrameRate(valueString, newFrameRate);
      if (newFrameRateEnum == IF_FRAME_RATE_INVALID)
         return CLIP_ERROR_INVALID_FRAME_RATE;

      setFrameRate(newFrameRateEnum, newFrameRate);
      }

   // Read the Virtual Media flag, if it is present
   if (schemeFile->KeyExists(sectionName, virtualMediaKey))
      {
      virtualMediaFlag = schemeFile->ReadBool(sectionName, virtualMediaKey,
                                              false);
      }

   // Read the Media Type Key, if it is present
   if (schemeFile->KeyExists(sectionName, mediaTypeKey))
      {
      valueString = schemeFile->ReadString(sectionName, mediaTypeKey,
                                           emptyString);
      mediaType = CMediaInterface::queryMediaType(TrimWhiteSpace(valueString));
      if (mediaType == MEDIA_TYPE_INVALID
          && sourceType != CLIP_INIT_SOURCE_CLIP_INI)
       {
         return CLIP_ERROR_INVALID_MEDIA_TYPE;
       }
      }

   // Read the Media Identifier Key, if it is present
   if (schemeFile->KeyExists(sectionName, mediaIdentifierKey))
      {
      valueString = schemeFile->ReadString(sectionName, mediaIdentifierKey,
                                           emptyString);
      setMediaIdentifier(valueString);
      }

   return 0;

} // readSection

int CCommonInitInfo::writeSection(CIniFile *schemeFile,
                                 const string& sectionName)
{
   string valueString;

   // Write the Clip Ver
   schemeFile->WriteString(sectionName, clipVerKey,
                           CClip::clipVerLookup.FindByValue(clipVer));

   // Write the Application Id
   if (appId.empty())
      schemeFile->DeleteKey(sectionName, appIdKey);
   else
      schemeFile->WriteString(sectionName, appIdKey, appId);

   // Write the Handle Count
   schemeFile->WriteInteger(sectionName, handleCountKey, getHandleCount());

   // Write Frame Rate as frames-per-second value
   if (frameRateEnum == IF_FRAME_RATE_INVALID)
      {
      // Don't write anything if we don't know the frame rate!
      schemeFile->DeleteKey(sectionName, frameRateKey);
      }
   else
      {
      string frameRateString;
      if (frameRateEnum == IF_FRAME_RATE_NON_STANDARD)
         {
         // Non-standard frame rate, so convert frame rate double to a string
         // using default formatting of MTIostringstream
         MTIostringstream ostr;
         ostr << frameRate;
         frameRateString = ostr.str();
         }
      else
         {
         // If the frameRateEnum is not non-standard, assume it is a standard
         // frame rate.  Given the standard frame rate, get a string that
         // corresponds to the standard frames-per-second (this avoids problems
         // with floating point formating and rounding)
         frameRateString = CImageInfo::queryFrameRateName(frameRateEnum);
         }
      schemeFile->WriteString(sectionName, frameRateKey, frameRateString);
      }

   // Write the Media Type
   valueString = CMediaInterface::queryMediaTypeName(getMediaType());
   schemeFile->WriteString(sectionName, mediaTypeKey, valueString);

   // Write the Media Identifier
   string mediaID = getMediaIdentifier();
   if (mediaID.empty())
      schemeFile->DeleteKey(sectionName, mediaIdentifierKey);
   else
      schemeFile->WriteString(sectionName, mediaIdentifierKey, mediaID);

   return 0;

} // writeSection

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                            ###################################
// ###     CAudioInitInfo Class     ##################################
// ####                            ###################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAudioInitInfo::CAudioInitInfo()
{
   mediaFormat = new CAudioFormat;
}

CAudioInitInfo::~CAudioInitInfo()
{
}

//////////////////////////////////////////////////////////////////////
// Initialization
//////////////////////////////////////////////////////////////////////

void CAudioInitInfo::init()
{
   CCommonInitInfo::init();

   frameRate = 0.0;

   mediaDirectory.erase();

   CAudioFormat dummyAudioFormat;
   *(getAudioFormat()) = dummyAudioFormat;
}

int CAudioInitInfo::initFromAudioTrack(CAudioTrack* audioTrack)
{
   int retVal;

   // Initialize the supertype portion of the Audio Init Info from
   // the supertype portion of the Audio Track
   retVal = CCommonInitInfo::initFromTrack(audioTrack);
   if (retVal != 0)
      return retVal;   // ERROR: Could not initialize common init info

   // Copy the contents of the Audio Track's audio format
   *(getAudioFormat()) = *(audioTrack->getAudioFormat());

   mediaDirectory = audioTrack->getMediaDirectory();

   // TBD: Set mediaDirectory.  Have to figure out how to extract this
   //      from media identifier

   return 0; // Success
}

int CAudioInitInfo::initAudioMediaDirectory(const string &newAudioMediaDirectory)
{
   if (!mediaDirectory.empty())
      return 0;   // Audio media directory is already set in this Audio
                  // Init Info, so just leave as-is

   // Just copy the caller's directory name
   mediaDirectory = newAudioMediaDirectory;

   return 0;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CAudioInitInfo::validate()
{
   int retVal;
   CAutoErrorReporter autoErr("CAudioInitInfo::validate",
                              AUTO_ERROR_NO_NOTIFY, AUTO_ERROR_NO_TRACE);

   // Validate parent class
   retVal = CCommonInitInfo::validate();
   if (retVal != 0)
      {
      autoErr.RepeatTheError();
      return retVal;
      }

   CAudioFormat *audioFormat = getAudioFormat();
   retVal = audioFormat->validate();
   if (retVal != 0)
      {
      autoErr.errorCode = retVal;
      autoErr.msg << "Invalid audio format";
      return retVal;
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

CAudioFormat* CAudioInitInfo::getAudioFormat()
{
   return static_cast<CAudioFormat*>(mediaFormat);
}

const string& CAudioInitInfo::getMediaDirectory()
{
   return mediaDirectory;
}


//////////////////////////////////////////////////////////////////////
// Mutators
//////////////////////////////////////////////////////////////////////

void CAudioInitInfo::setAudioFormat(const CAudioFormat &newAudioFormat)
{
   *(getAudioFormat()) = newAudioFormat;
}

void CAudioInitInfo::setMediaDirectory(const string& newMediaDirectory)
{
   mediaDirectory = TrimWhiteSpace(newMediaDirectory);
}

//////////////////////////////////////////////////////////////////////
// Query Functions
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//
// Function:   queryImageTimeRemaining
//
// Description:Queries Media Interface for the time remaining on a given
//             media storage device and the well-formed contents of this
//             CVideoInitInfo.
//
//             The media storage device is specified as a Media Type and
//             a Media Identifier.
//
//             The query returns both the total time remaining on the
//             media storage and the time remaining in the largest contiguous
//             free space.  To get remaining time in hours, minutes, seconds
//             and frames, use the CTimecode member functions getHours, getMinutes
//             getSeconds and getFrames.  To get the remaining time as the
//             the number of frames, use the CTimecode member function
//             getAbsoluteTime.
//
//             Note: This function only works on Raw disks (CPMP or OGLV
//                   Bridge).  It does not work for Image Files (DPX, SGI, etc)
//                   as I do not know how to query a file system for free
//                   space.  This may change in the future.
//
// Arguments:  EMediaType mediaType       Media type: MEDIA_TYPE_RAW, etc.
//             string& mediaIdentifier    Media identifier, e.g., "rd1"
//             CTimecode& totalTimeRemaining
//                                        Reference to caller's CTimecode to
//                                        accept the total time remaining
//             CTimecode& largestTimeRemaining
//                                        Reference to caller's CTimecode to
//                                        accept the time remaining in largest
//                                        contiguous space
//
//                                        Both timecodes are initialized
//                                        with the clip's frame rate
//                                        and Non-Drop.
//
// Returns:    Returns 0 if query succeed.  Non-zero if failed.
//
//////////////////////////////////////////////////////////////////////
int CAudioInitInfo::queryAudioTimeRemaining(EMediaType mediaType,
                                            const string& mediaIdentifier,
                                            CTimecode& totalTimeRemaining,
                                            CTimecode& largestTimeRemaining)
{
   int retVal;

   // Query time remaining on media storage device
   CMediaInterface mediaInterface;
   retVal = mediaInterface.queryAudioTimeRemaining(mediaType, mediaIdentifier,
                                                   *(getAudioFormat()),
                                                   getFrameRate(),
                                                   totalTimeRemaining,
                                                   largestTimeRemaining);
   if (retVal != 0)
      return retVal; // ERROR

   return 0; // Success
}

int CAudioInitInfo::queryAudioTimeTotal(EMediaType mediaType,
                                        const string& mediaIdentifier,
                                        CTimecode& totalTime)
{
   int retVal;

   // Query total time on media storage device
   CMediaInterface mediaInterface;
   retVal = mediaInterface.queryAudioTimeTotal(mediaType, mediaIdentifier,
                                               *(getAudioFormat()),
                                               getFrameRate(),
                                               totalTime);
   if (retVal != 0)
      return retVal; // ERROR

   return 0; // Success
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CAudioInitInfo::readSection(CIniFile *schemeFile,
                                const string& sectionName,
                                EClipInitSource sourceType)
{
   int retVal;
   string emptyString, valueString;

   // Read stuff in the common part of the section, e.g., Media Type, etc.
   retVal = CCommonInitInfo::readSection(schemeFile, sectionName, sourceType);
   if (retVal != 0)
      return retVal;

   // Read the Media Directory, if it is present
   if (schemeFile->KeyExists(sectionName, mediaDirectoryKey))
      {
      valueString = schemeFile->ReadFileName(sectionName, mediaDirectoryKey,
                                             emptyString);
      setMediaDirectory(valueString);
      }

   // Read the Audio Format section
   CAudioFormat *audioFormat = getAudioFormat();
   retVal = audioFormat->readClipInitSection(schemeFile, sectionName);
   if (retVal != 0)
      return retVal;

   // Calculate samples per field
   double fieldRate = getFrameRate() * audioFormat->getFieldCount();
   if (fieldRate > 0.0)
      {
      double newSamplesPerField = audioFormat->getSampleRate() / fieldRate;
      audioFormat->setSamplesPerField(newSamplesPerField);
      }

   return 0;

} // readSection

int CAudioInitInfo::writeSection(CIniFile *schemeFile,
                                 const string& sectionName)
{
   int retVal;

   // Write stuff in the common part of the section, e.g., Media Type, etc.
   retVal = CCommonInitInfo::writeSection(schemeFile, sectionName);
   if (retVal != 0)
      return retVal;

   // Write the Media Directory
   if (mediaDirectory.empty())
      {
      schemeFile->DeleteKey(sectionName, mediaDirectoryKey);
      }
   else
      {
      schemeFile->WriteString(sectionName, mediaDirectoryKey, mediaDirectory);
      }

   // Write the Audio Format section
   CAudioFormat *audioFormat = getAudioFormat();
   retVal = audioFormat->writeClipInitSection(schemeFile, sectionName);
   if (retVal != 0)
      return retVal;

   return 0;

} // writeSection


//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                            ###################################
// ###     CVideoInitInfo Class     ##################################
// ####                            ###################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CVideoInitInfo::CVideoInitInfo()
 : imageFile0Index(-1)
{
   mediaFormat = new CImageFormat;
}

CVideoInitInfo::~CVideoInitInfo()
{
}

//////////////////////////////////////////////////////////////////////
// Initialization
//////////////////////////////////////////////////////////////////////

void CVideoInitInfo::init()
{
   CCommonInitInfo::init();

   imageFile0Index = -1;

   CImageFormat dummyImageFormat;
   *(getImageFormat()) = dummyImageFormat;
}

int CVideoInitInfo::initFromVideoProxy(CVideoProxy* videoProxy)
{
   int retVal;
   imageFile0Index = -1;  // make sure it's always set when we return

   // Initialize the supertype portion of the Video Init Info from
   // the supertype portion of the Video Proxy
   retVal = CCommonInitInfo::initFromTrack(videoProxy);
   if (retVal != 0)
      return retVal;   // ERROR: Could not initialize common init info

   // Copy the contents of the Video Proxy's image format
   *(getImageFormat()) = *(videoProxy->getImageFormat());

   // If the media is stored in image files, then remember the index of the
   // first image file.  If the media is not stored in image files, then
   // set a default value
   if (CMediaInterface::isMediaTypeImageFile(getMediaType()))
      {
      // Media is stored in image files.  Look in field 0 of frame 0 of
      // source videoProxy to pull out the index of the first image file
      CVideoFrameList *frameList = videoProxy->getVideoFrameList(0);
      CVideoFrame *frame = frameList->getFrame(0);
      if (frame == NULL)
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN;
      CVideoField *field = frame->getField(0);
      if (field == NULL)
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN;
      MTIistringstream istrm;
      const char* fieldName(field->getMediaLocation().getFieldName());
      if (fieldName == NULL)
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN;
      istrm.str(fieldName);
      istrm >> imageFile0Index;
      }
   else
      {
      // Media is stored in something other than image files
      imageFile0Index = -1;
      }

   return 0; // Success
}

int CVideoInitInfo::initVideoMediaLocation(const string &newVideoMediaLocation)
{
   int retVal;
   string newMediaIdentifier(newVideoMediaLocation);

   if (getMediaType() != MEDIA_TYPE_INVALID && !getMediaIdentifier().empty())
      return 0;   // The media location is already set in this Video Init info,
                  // so leave it as-is

   if (newVideoMediaLocation.empty())
      return CLIP_ERROR_MISSING_VIDEO_MEDIA_LOCATION;

   // Set the Media storage location in the Video Init Info
   CMediaInterface mediaInterface;
   CMediaInterface::RawDiskInfoList rawDiskInfoList;

   retVal = mediaInterface.queryRawDiskInfo(rawDiskInfoList);
   if (retVal != 0)
      {
      TRACE_0(errout << "ERROR: CVideoInitInfo::initVideoMediaLocation: call to"
                     << endl
                     << "       CMediaInterface::queryRawDiskInfo failed, return code "
                     << retVal);
      return retVal;
      }

   // Match the media identifier with its Media Type
   EMediaType newMediaType = MEDIA_TYPE_INVALID;
   for (unsigned int i = 0; i < rawDiskInfoList.size(); ++i)
      {
      if (rawDiskInfoList[i].mediaIdentifier == newVideoMediaLocation)
         {
         newMediaType = rawDiskInfoList[i].mediaType;
         setRepositoryIdentifier(newVideoMediaLocation);
         }
      }

   if (newMediaType == MEDIA_TYPE_INVALID)
      {
      CMediaInterface::ImageRepositoryInfoList imageRepositoryInfoList;

      retVal = mediaInterface.queryImageRepositoryInfo(imageRepositoryInfoList);
      if (retVal != 0)
         {
         TRACE_1(errout << "WARNING: CVideoInitInfo::initVideoMediaLocation: call to"
                        << endl
                        << "       CMediaInterface::queryImageRepositoryInfo failed, return code "
                        << retVal);
         return 0;   // OK if not there
         }

      if (newMediaType == MEDIA_TYPE_INVALID)
         {
         TRACE_0(errout << "ERROR: CVideoInitInfo::initVideoMediaLocation: cannot find"
                        << endl
                        << "       a Video Media Repository configuration with the name "
                        << endl
                        << "       " << newVideoMediaLocation);
         return CLIP_ERROR_CANNOT_FIND_VIDEO_MEDIA_LOCATION;
         }
      }

   // Set the Media Identifier and Media Type
   setMediaType(newMediaType);
   setMediaIdentifier(newMediaIdentifier);

   return 0;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CVideoInitInfo::CheckRFL()
{
   // REMOVE THIS METHOD

   return 0;
}

int CVideoInitInfo::validate()
{
   int retVal;
   CAutoErrorReporter autoErr("CVideoInitInfo::validate",
                              AUTO_ERROR_NO_NOTIFY, AUTO_ERROR_NO_TRACE);

   // Validate parent class
   retVal = CCommonInitInfo::validate();
   if (retVal != 0)
      {
      autoErr.RepeatTheError();
      return retVal;
      }

   CImageFormat* imageFormat = getImageFormat();
   retVal = imageFormat->validate();
   if (retVal != 0)
      {
      autoErr.errorCode = retVal;
      autoErr.msg << "Invalid image format";
      return retVal;
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

int CVideoInitInfo::getImageFile0Index() const
{
   return imageFile0Index;
}

CImageFormat *CVideoInitInfo::getImageFormat()
{
   return static_cast<CImageFormat*>(mediaFormat);
}

bool CVideoInitInfo::isMediaFromImageFile()
{
   // Returns true if the media is stored in an Image File (DPX, etc)
   // The media is assumed to be in an Image File if either the image format
   // type suggests an Image File (e.g., IF_TYPE_FILE_DPX) or the media
   // type suggests an Image File (e.g., MEDIA_TYPE_IMAGE_FILE_UYVY).
   // **WARNING**WARNING**WARNING** Returns *false* if media in in an
   //                               Image File Repository!
   //

   CImageFormat *imageFormat = getImageFormat();
   return ((imageFormat != 0
            && CImageInfo::queryIsFormatFile(imageFormat->getImageFormatType()))
           || CMediaInterface::isMediaTypeImageFile(getMediaType()));
}

//////////////////////////////////////////////////////////////////////
// Mutators
//////////////////////////////////////////////////////////////////////

void CVideoInitInfo::setImageFile0Index(int newImageFile0Index)
{
   imageFile0Index = newImageFile0Index;
}

void CVideoInitInfo::setImageFormat(const CImageFormat &newImageFormat)
{
   *(getImageFormat()) = newImageFormat;
}

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                            ###################################
// ###     CTimeInitInfo Class     ##################################
// ####                            ###################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTimeInitInfo::CTimeInitInfo()
 : clipVer(CLIP_VERSION_3), timeType(TIME_TYPE_INVALID),
   timecodeFrameRate(IF_FRAME_RATE_INVALID)
{
}

CTimeInitInfo::~CTimeInitInfo()
{
}

//////////////////////////////////////////////////////////////////////
// Initialization
//////////////////////////////////////////////////////////////////////

void CTimeInitInfo::init()
{
   timeType = TIME_TYPE_INVALID;
   timecodeFrameRate = IF_FRAME_RATE_INVALID;
   appId.erase();
}

int CTimeInitInfo::initFromTimeTrack(CTimeTrack* timeTrack)
{
   setTimeType(timeTrack->getTimeType());

   setAppId(timeTrack->getAppId());

   if (timeTrack->getClipVer() == CLIP_VERSION_5L)
      {
      if (timeTrack->getTimeType() == TIME_TYPE_SMPTE_TIMECODE)
         {
         timecodeFrameRate = timeTrack->getDefaultTimecodeFrameRateEnum();
         }
      }

   return 0; // Success
}

//////////////////////////////////////////////////////////////////////
//  Validation
//////////////////////////////////////////////////////////////////////

int CTimeInitInfo::validate()
{
   if (timeType != TIME_TYPE_TIMECODE &&
       timeType != TIME_TYPE_KEYKODE  &&
       timeType != TIME_TYPE_SMPTE_TIMECODE &&
       timeType != TIME_TYPE_EVENT &&
       timeType != TIME_TYPE_AUDIO_PROXY)

      return CLIP_ERROR_INVALID_TIME_TRACK_TYPE;

   return 0;
}

//////////////////////////////////////////////////////////////////////
//  Read & Write Clip Scheme Section
//////////////////////////////////////////////////////////////////////

int CTimeInitInfo::readSection(CIniFile *schemeFile, const string& sectionName)
{
   string emptyString, valueString;

   // Read the ClipVer, if it is present.  This is probably temporary
   // to support development and introduction of Clip 5
   valueString = schemeFile->ReadString(sectionName, clipVerKey,
                              CClip::clipVerLookup.FindByValue(CLIP_VERSION_3));
   clipVer = CClip::clipVerLookup.FindByStr(valueString, CLIP_VERSION_INVALID);
   if (clipVer == CLIP_VERSION_INVALID)
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   // Read the Time Type if it is present
   if (schemeFile->KeyExists(sectionName, timeTypeKey))
      {
      valueString = schemeFile->ReadString(sectionName, timeTypeKey,
                                           emptyString);

      ETimeType newTimeType = CTimeTrack::queryTimeType(valueString);
      if (newTimeType == TIME_TYPE_INVALID)
         return CLIP_ERROR_INVALID_TIME_TRACK_TYPE;

      setTimeType(newTimeType);
      }

   // Read the Application ID, if it is present
   if (schemeFile->KeyExists(sectionName, appIdKey))
      {
      valueString = schemeFile->ReadString(sectionName, appIdKey,
                                             emptyString);
      setAppId(valueString);
      }

   // Read the timecode frame rate, if it is present
   EFrameRate newTimecodeFrameRateEnum;
   if (getTimeType() == TIME_TYPE_SMPTE_TIMECODE &&
       schemeFile->KeyExists(sectionName, timecodeFrameRateKey))
      {
      valueString = schemeFile->ReadString(sectionName, timecodeFrameRateKey,
                                           emptyString);
      double newFrameRate;
      newTimecodeFrameRateEnum = CImageInfo::queryFrameRate(valueString,
                                                         newFrameRate);
      if (newTimecodeFrameRateEnum == IF_FRAME_RATE_INVALID ||
          newTimecodeFrameRateEnum == IF_FRAME_RATE_NON_STANDARD)
         return CLIP_ERROR_INVALID_FRAME_RATE;
      }
   else
      newTimecodeFrameRateEnum = IF_FRAME_RATE_INVALID;

   setTimecodeFrameRate(newTimecodeFrameRateEnum);


   return 0;

} // readSection

int CTimeInitInfo::writeSection(CIniFile *schemeFile, const string& sectionName)
{
   // Write the Clip Ver
   schemeFile->WriteString(sectionName, clipVerKey,
                           CClip::clipVerLookup.FindByValue(clipVer));

   // Write Time Type as a string
   schemeFile->WriteString(sectionName, timeTypeKey,
                           CTimeTrack::queryTimeTypeName(getTimeType()));

   // Write the Application Id
   schemeFile->WriteString(sectionName, appIdKey, appId);

   if (getTimeType() == TIME_TYPE_SMPTE_TIMECODE &&
       getTimecodeFrameRate() != IF_FRAME_RATE_INVALID)
      {
      schemeFile->WriteString(sectionName, timecodeFrameRateKey,
                        CImageInfo::queryFrameRateName(getTimecodeFrameRate()));
      }

   return 0;

} // writeSection

//////////////////////////////////////////////////////////////////////
// Query Functions
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//
// Function:   queryImageTimeRemaining
//
// Description:Queries Media Interface for the time remaining on a given
//             media storage device and the well-formed contents of this
//             CVideoInitInfo.
//
//             The media storage device is specified as a Media Type and
//             a Media Identifier.
//
//             The query returns both the total time remaining on the
//             media storage and the time remaining in the largest contiguous
//             free space.  To get remaining time in hours, minutes, seconds
//             and frames, use the CTimecode member functions getHours, getMinutes
//             getSeconds and getFrames.  To get the remaining time as the
//             the number of frames, use the CTimecode member function
//             getAbsoluteTime.
//
//             Note: This function only works on Raw disks (CPMP or OGLV
//                   Bridge).  It does not work for Image Files (DPX, SGI, etc)
//                   as I do not know how to query a file system for free
//                   space.  This may change in the future.
//
// Arguments:  EMediaType mediaType       Media type: MEDIA_TYPE_RAW, etc.
//             string& mediaIdentifier    Media identifier, e.g., "rd1"
//             CTimecode& totalTimeRemaining
//                                        Reference to caller's CTimecode to
//                                        accept the total time remaining
//             CTimecode& largestTimeRemaining
//                                        Reference to caller's CTimecode to
//                                        accept the time remaining in largest
//                                        contiguous space
//
//                                        Both timecodes are initialized
//                                        with the clip's frame rate
//                                        and Non-Drop.
//
// Returns:    Returns 0 if query succeed.  Non-zero if failed.
//
//////////////////////////////////////////////////////////////////////
int CVideoInitInfo::queryImageTimeRemaining(EMediaType mediaType,
                                            const string& mediaIdentifier,
                                            CTimecode& totalTimeRemaining,
                                            CTimecode& largestTimeRemaining)
{
   int retVal;

   // Use default frame rate
   // Future: Allow non-standard frame rate to be set in the clip scheme
   EImageFormatType imageFormatType = getImageFormat()->getImageFormatType();
   double frameRate = CImageInfo::queryNominalFrameRate(imageFormatType);

   // Query time remaining on media storage device
   CMediaInterface mediaInterface;
   retVal = mediaInterface.queryImageTimeRemaining(mediaType, mediaIdentifier,
                                                   *(getImageFormat()),
                                                   frameRate,
                                                   totalTimeRemaining,
                                                   largestTimeRemaining);
   if (retVal != 0)
      return retVal; // ERROR

   return 0; // Success
}

int CVideoInitInfo::queryImageTimeTotal(EMediaType mediaType,
                                        const string& mediaIdentifier,
                                        CTimecode& totalTime)
{
   int retVal;

   // Use default frame rate
   // Future: Allow non-standard frame rate to be set in the clip scheme
   EImageFormatType imageFormatType = getImageFormat()->getImageFormatType();
   double frameRate = CImageInfo::queryNominalFrameRate(imageFormatType);

   // Query total time on media storage device
   CMediaInterface mediaInterface;
   retVal = mediaInterface.queryImageTimeTotal(mediaType, mediaIdentifier,
                                               *(getImageFormat()),
                                               frameRate,
                                               totalTime);
   if (retVal != 0)
      return retVal; // ERROR

   return 0; // Success
}
//////////////////////////////////////////////////////////////////////
//  Read & Write Clip Scheme Files
//////////////////////////////////////////////////////////////////////

int CVideoInitInfo::readSection(CIniFile *schemeFile,
                                const string& sectionName,
                                EClipInitSource sourceType)
{
   int retVal;
   string emptyString;

   // Read stuff in the common part of the section, e.g., Media Type, etc.
   retVal = CCommonInitInfo::readSection(schemeFile, sectionName, sourceType);
   if (retVal != 0)
      return retVal;

   // Read the Image Format section
   CImageFormat *imageFormat = getImageFormat();
   retVal = imageFormat->readClipInitSection(schemeFile, sectionName);
   if (retVal != 0)
      return retVal;

   return 0;

} // readSection

int CVideoInitInfo::readSection2(CIniFile *schemeFile,
                                const string& sectionName,
                                EClipInitSource sourceType)
{
   int retVal;

   // Read the Image Format section
   CImageFormat *imageFormat = getImageFormat();
   retVal = imageFormat->readClipInitSection2(schemeFile, sectionName);
   if (retVal != 0)
      return retVal;

   return 0;

} // readSection2

int CVideoInitInfo::writeSection(CIniFile *schemeFile,
                                 const string& sectionName)
{
   int retVal;

   // Write stuff in the common part of the section, e.g., Media Type, etc.
   retVal = CCommonInitInfo::writeSection(schemeFile, sectionName);
   if (retVal != 0)
      return retVal;

   // Write the Image Format section
   CImageFormat *imageFormat = getImageFormat();
   retVal = imageFormat->writeClipInitSection(schemeFile, sectionName);
   if (retVal != 0)
      return retVal;

   return 0;

} // readSection


//////////////////////////////////////////////////////////////////////
//  GLOBAL FUNCTIONS
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//
// Function:   QueryImageTimeRemaining
//
// Description:Queries Media Interface for the time remaining on a
//             media storage device given a Clip Scheme name
//
//             The media storage device is specified as a Media Type and
//             a Media Identifier.
//
//             The time remaining is calculated based on the image format type
//             found in the Main Video section of the Clip Scheme.  If the Clip
//             Scheme does not have a Main Video section (i.e., it is an Audio-
//             only Clip Scheme), then an error is returned.
//
//             The query returns both the total time remaining on the
//             media storage and the time remaining in the largest contiguous
//             free space.  To get remaining time in hours, minutes, seconds
//             and frames, use the CTimecode member functions getHours, getMinutes
//             getSeconds and getFrames.  To get the remaining time as the
//             the number of frames, use the CTimecode member function
//             getAbsoluteTime.
//
//             Note: This function only works on Raw disks (CPMP or OGLV
//                   Bridge).  It does not work for Image Files (DPX, SGI, etc)
//                   as I do not know how to query a file system for free
//                   space.  This may change in the future.
//
// Arguments:  EMediaType mediaType       Media type: MEDIA_TYPE_RAW, etc.
//             string& mediaIdentifier    Media identifier, e.g., "rd1"
//             string& clipSchemeName     Name of Clip Scheme
//             CTimecode& totalTimeRemaining
//                                        Reference to caller's CTimecode to
//                                        accept the total time remaining
//             CTimecode& largestTimeRemaining
//                                        Reference to caller's CTimecode to
//                                        accept the time remaining in largest
//                                        contiguous space
//
//                                        Both timecodes are initialized
//                                        with the clip scheme's frame rate
//                                        and Non-Drop.
//
// Returns:    Returns 0 if query succeed.  Non-zero if failed.
//
//////////////////////////////////////////////////////////////////////
int QueryImageTimeRemaining(EMediaType mediaType, const string& mediaIdentifier,
                            const string& clipSchemeName,
                            CTimecode& totalTimeRemaining,
                            CTimecode& largestTimeRemaining)
{
   int retVal;

   // Get the Clip Scheme
   CClipInitInfo clipInitInfo;
   retVal = clipInitInfo.initFromClipScheme(clipSchemeName);
   if (retVal != 0)
      return retVal; // ERROR: Could not find or read the Clip Scheme

   // Make sure the clip scheme has a Main Video section defined
   // so that we can get the proper image format type
   if (!clipInitInfo.doesNormalVideoExist())
      return CLIP_ERROR_MAIN_VIDEO_NOT_AVAILABLE;

   // Get the main video part of the clip scheme.  The time
   // remaining uses the Main Video's image format type
   CVideoInitInfo *videoInitInfo;
   videoInitInfo = clipInitInfo.getMainVideoInitInfo();

   // Get the time remaining
   retVal = videoInitInfo->queryImageTimeRemaining(mediaType, mediaIdentifier,
                                                   totalTimeRemaining,
                                                   largestTimeRemaining);
   if (retVal != 0)
      return retVal; // ERROR: Query failed

   return 0; // success

} // QueryImageTimeRemaining

//////////////////////////////////////////////////////////////////////
//
// Function:   QueryImageTimeRemaining
//
// Description:Queries Media Interface for the time remaining on a
//             set of media storage devices given a Clip Scheme name
//
//             The media storage devices are specified in a Disk Scheme
//
//             The time remaining is calculated based on the image format type
//             found in the Main Video and Proxy Video sections of the Clip
//             Scheme.  If the Clip Scheme does not have a Main Video section
//             (i.e., it is an Audio-only Clip Scheme), then an error is
//             returned.
//
//             The values returned are for the main-video/proxy-video and
//             media location that has the least remaining time.  For example,
//             if the media location (e.g. raw disk) for the main video has
//             15 minutes remaining and the media location for a proxy has
//             5 minutes remaining, then this function will return 5 minutes.
//
//             The query returns both the total time remaining on the
//             media storage and the time remaining in the largest contiguous
//             free space.  To get remaining time in hours, minutes, seconds
//             and frames, use the CTimecode member functions getHours, getMinutes
//             getSeconds and getFrames.  To get the remaining time as the
//             the number of frames, use the CTimecode member function
//             getAbsoluteTime.
//
//             Note: This function only works on Raw disks (CPMP or OGLV
//                   Bridge).  It does not work for Image Files (DPX, SGI, etc)
//                   or audio media disks as I do not know how to query
//                   a file system for free space. This may change in the
//                   future.
//
// Arguments:  string& diskSchemeName     Name of Disk Scheme
//             string& clipSchemeName     Name of Clip Scheme
//             CTimecode& totalTimeRemaining
//                                        Reference to caller's CTimecode to
//                                        accept the total time remaining
//             CTimecode& largestTimeRemaining
//                                        Reference to caller's CTimecode to
//                                        accept the time remaining in largest
//                                        contiguous space
//
//                                        Both timecodes are initialized
//                                        with the clip scheme's frame rate
//                                        and Non-Drop.
//
// Returns:    Returns 0 if query succeed.  Non-zero if failed.
//
//////////////////////////////////////////////////////////////////////
int QueryImageTimeRemaining(const string& diskSchemeName,
                            const string& clipSchemeName,
                            CTimecode& totalTimeRemaining,
                            CTimecode& largestTimeRemaining)
{
   int retVal;

   // Get the Clip Scheme
   CClipInitInfo clipInitInfo;
   retVal = clipInitInfo.initFromClipScheme(clipSchemeName);
   if (retVal != 0)
      return retVal; // ERROR: Could not find or read the Clip Scheme

   retVal = clipInitInfo.initMediaLocation(diskSchemeName);
   if (retVal != 0)
      return retVal; // ERROR: Could not set media locations from Disk Scheme

   return QueryImageTimeRemaining(clipInitInfo, totalTimeRemaining,
                                                largestTimeRemaining);


} // QueryImageTimeRemaining

//////////////////////////////////////////////////////////////////////
//
// Function:   QueryImageTimeRemaining
//
// Description:Same as above, except takes a CClipInitInfo instead of a
//             clip scheme name.
//
// Arguments:  CClipInitInfo& clipInitInfo
//                                        Info pertaining to a clip of the
//                                        type we are going to create
//             CTimecode& totalTimeRemaining
//                                        Reference to caller's CTimecode to
//                                        accept the total time remaining
//             CTimecode& largestTimeRemaining
//                                        Reference to caller's CTimecode to
//                                        accept the time remaining in largest
//                                        contiguous space
//
//                                        Both timecodes are initialized
//                                        with the clip scheme's frame rate
//                                        and Non-Drop.
//
// Returns:    Returns 0 if query succeed.  Non-zero if failed.
//
//////////////////////////////////////////////////////////////////////
int QueryImageTimeRemaining(const CClipInitInfo& clipInitInfo,
                            CTimecode& totalTimeRemaining,
                            CTimecode& largestTimeRemaining)
{
   int retVal;

   // Make sure the clip scheme has a Main Video section defined
   // so that we can get the proper image format type
   if (!clipInitInfo.doesNormalVideoExist())
      return CLIP_ERROR_MAIN_VIDEO_NOT_AVAILABLE;

   // Get the main video part of the clip scheme.  The time
   // remaining uses the Main Video's image format type
   CVideoInitInfo *videoInitInfo;
   videoInitInfo = clipInitInfo.getMainVideoInitInfo();

   // Get the time remaining
   retVal = videoInitInfo->queryImageTimeRemaining(
                                                  videoInitInfo->getMediaType(),
                                            videoInitInfo->getMediaIdentifier(),
                                                   totalTimeRemaining,
                                                   largestTimeRemaining);
   if (retVal != 0)
      return retVal; // ERROR: Query failed

   // Temporary timecodes to hold query results for proxies
   CTimecode tmpTotalTime = totalTimeRemaining;
   CTimecode tmpLargestTime = largestTimeRemaining;

   // Iterate over video proxies, looking for a media location with
   // least time remaining and less than the media location for the main video
   for (int i = 0; i < clipInitInfo.getVideoProxyCount(); ++i)
      {
      videoInitInfo = clipInitInfo.getVideoProxyInitInfo(i);
      if (!videoInitInfo->getVirtualMediaFlag()) // only for real media
         {
         retVal = videoInitInfo->queryImageTimeRemaining(
            videoInitInfo->getMediaType(),
            videoInitInfo->getMediaIdentifier(),
            tmpTotalTime,
            tmpLargestTime);
         if (retVal != 0)
            return retVal; // ERROR: Query failed

         if (tmpLargestTime.getAbsoluteTime()
             < largestTimeRemaining.getAbsoluteTime())
            {
            totalTimeRemaining = tmpTotalTime;
            largestTimeRemaining = tmpLargestTime;
            }
         }
      }

   return 0; // success

} // QueryImageTimeRemaining

//////////////////////////////////////////////////////////////////////
//
// Function:   QueryImageTimeTotal
//
// Description:Queries Media Interface for the total time on a
//             set of media storage devices given a Clip Scheme name
//
//             The media storage devices are specified in a Disk Scheme
//
//             The total time is calculated based on the image format type
//             found in the Main Video and Proxy Video sections of the Clip
//             Scheme.  If the Clip Scheme does not have a Main Video section
//             (i.e., it is an Audio-only Clip Scheme), then an error is
//             returned.
//
//             The values returned are for the main-video/proxy-video and
//             media location that has the least total time.  For example,
//             if the media location (e.g. raw disk) for the main video has
//             15 minutes total and the media location for a proxy has
//             5 minutes total, then this function will return 5 minutes.
//
//             Note: This function only works on Raw disks (CPMP or OGLV
//                   Bridge).  It does not work for Image Files (DPX, SGI, etc)
//                   or audio media disks as I do not know how to query
//                   a file system for free space. This may change in the
//                   future.
//
// Arguments:  string& diskSchemeName     Name of Disk Scheme
//             string& clipSchemeName     Name of Clip Scheme
//             CTimecode& totalTime
//                                        Reference to caller's CTimecode to
//                                        accept the total time remaining
//
// Returns:    Returns 0 if query succeed.  Non-zero if failed.
//
//////////////////////////////////////////////////////////////////////
int QueryImageTimeTotal(const string& diskSchemeName,
                        const string& clipSchemeName,
                        CTimecode& totalTimeRemaining)
{
   int retVal;

   // Get the Clip Scheme
   CClipInitInfo clipInitInfo;
   retVal = clipInitInfo.initFromClipScheme(clipSchemeName);
   if (retVal != 0)
      return retVal; // ERROR: Could not find or read the Clip Scheme

   retVal = clipInitInfo.initMediaLocation(diskSchemeName);
   if (retVal != 0)
      return retVal; // ERROR: Could not set media locations from Disk Scheme

   return QueryImageTimeTotal(clipInitInfo, totalTimeRemaining);
} // QueryImageTimeTotal

//////////////////////////////////////////////////////////////////////
//
// Function:   QueryImageTimeTotal
//
// Description:Same as above, except takes a CClipInitInfo instead of a
//             clip scheme name.
//
// Arguments:  CClipInitInfo& clipInitInfo
//                                        Info pertaining to a clip of the
//                                        type we are going to create
//             CTimecode& totalTime
//                                        Reference to caller's CTimecode to
//                                        accept the total time remaining
//
// Returns:    Returns 0 if query succeed.  Non-zero if failed.
//
//////////////////////////////////////////////////////////////////////
int QueryImageTimeTotal(const CClipInitInfo& clipInitInfo,
                        CTimecode& totalTime)
{
   int retVal;

   // Make sure the clip scheme has a Main Video section defined
   // so that we can get the proper image format type
   if (!clipInitInfo.doesNormalVideoExist())
      return CLIP_ERROR_MAIN_VIDEO_NOT_AVAILABLE;

   // Get the main video part of the clip scheme.  The time
   // uses the Main Video's image format type
   CVideoInitInfo *videoInitInfo;
   videoInitInfo = clipInitInfo.getMainVideoInitInfo();

   // Get the total time
   retVal = videoInitInfo->queryImageTimeTotal(
       videoInitInfo->getMediaType(),
       videoInitInfo->getMediaIdentifier(),
       totalTime);
   if (retVal != 0)
      return retVal; // ERROR: Query failed

   // Temporary timecodes to hold query results for proxies
   CTimecode tmpTotalTime = totalTime;

   // Iterate over video proxies, looking for a media location with
   // least time and less than the media location for the main video
   for (int i = 0; i < clipInitInfo.getVideoProxyCount(); ++i)
      {
      videoInitInfo = clipInitInfo.getVideoProxyInitInfo(i);
      if (!videoInitInfo->getVirtualMediaFlag()) // only for real media
         {
         retVal = videoInitInfo->queryImageTimeTotal(
            videoInitInfo->getMediaType(),
            videoInitInfo->getMediaIdentifier(),
            tmpTotalTime);
         if (retVal != 0)
            return retVal; // ERROR: Query failed

         if (tmpTotalTime.getAbsoluteTime()
             < totalTime.getAbsoluteTime())
            {
            totalTime = tmpTotalTime;
            }
         }
      }

   return 0; // success

} // QueryImageTimeTotal


//////////////////////////////////////////////////////////////////////
//
// Function:   QueryAudioTimeRemaining
//
// Description:Queries Media Interface for the time remaining on a
//             set of media storage devices given a Clip Scheme name
//
//             The media storage devices are specified in a Disk Scheme
//
//             The time remaining is calculated based on the image format type
//             found in the Main Video and Proxy Video sections of the Clip
//             Scheme.  If the Clip Scheme does not have a Main Video section
//             (i.e., it is an Audio-only Clip Scheme), then an error is
//             returned.
//
//             The values returned are for the main-video/proxy-video and
//             media location that has the least remaining time.  For example,
//             if the media location (e.g. raw disk) for the main video has
//             15 minutes remaining and the media location for a proxy has
//             5 minutes remaining, then this function will return 5 minutes.
//
//             The query returns both the total time remaining on the
//             media storage and the time remaining in the largest contiguous
//             free space.  To get remaining time in hours, minutes, seconds
//             and frames, use the CTimecode member functions getHours, getMinutes
//             getSeconds and getFrames.  To get the remaining time as the
//             the number of frames, use the CTimecode member function
//             getAbsoluteTime.
//
//             Note: This function only works on Raw disks (CPMP or OGLV
//                   Bridge).  It does not work for Image Files (DPX, SGI, etc)
//                   or audio media disks as I do not know how to query
//                   a file system for free space. This may change in the
//                   future.
//
// Arguments:  string& diskSchemeName     Name of Disk Scheme
//             string& clipSchemeName     Name of Clip Scheme
//             CTimecode& totalTimeRemaining
//                                        Reference to caller's CTimecode to
//                                        accept the total time remaining
//             CTimecode& largestTimeRemaining
//                                        Reference to caller's CTimecode to
//                                        accept the time remaining in largest
//                                        contiguous space
//
//                                        Both timecodes are initialized
//                                        with the clip scheme's frame rate
//                                        and Non-Drop.
//
// Returns:    Returns 0 if query succeed.  Non-zero if failed.
//
//////////////////////////////////////////////////////////////////////
int QueryAudioTimeRemaining(const string& diskSchemeName,
                            const string& clipSchemeName,
                            CTimecode& totalTimeRemaining,
                            CTimecode& largestTimeRemaining)
{
   int retVal;

   // Get the Clip Scheme
   CClipInitInfo clipInitInfo;
   retVal = clipInitInfo.initFromClipScheme(clipSchemeName);
   if (retVal != 0)
      return retVal; // ERROR: Could not find or read the Clip Scheme

   retVal = clipInitInfo.initMediaLocation(diskSchemeName);
   if (retVal != 0)
      return retVal; // ERROR: Could not set media locations from Disk Scheme

   return QueryAudioTimeRemaining(clipInitInfo, totalTimeRemaining,
                                                largestTimeRemaining);


} // QueryAudioTimeRemaining

//////////////////////////////////////////////////////////////////////
//
// Function:   QueryAudioTimeRemaining
//
// Description:Same as above, except takes a CClipInitInfo instead of a
//             clip scheme name.
//
// Arguments:  CClipInitInfo& clipInitInfo
//                                        Info pertaining to a clip of the
//                                        type we are going to create
//             CTimecode& totalTimeRemaining
//                                        Reference to caller's CTimecode to
//                                        accept the total time remaining
//             CTimecode& largestTimeRemaining
//                                        Reference to caller's CTimecode to
//                                        accept the time remaining in largest
//                                        contiguous space
//
//                                        Both timecodes are initialized
//                                        with the clip scheme's frame rate
//                                        and Non-Drop.
//
// Returns:    Returns 0 if query succeed.  Non-zero if failed.
//
//////////////////////////////////////////////////////////////////////
int QueryAudioTimeRemaining(const CClipInitInfo& clipInitInfo,
                            CTimecode& totalTimeRemaining,
                            CTimecode& largestTimeRemaining)
{
   int retVal;

   int audioTrackCount = clipInitInfo.getAudioTrackCount();
   if (audioTrackCount < 1)
      {
      totalTimeRemaining.setFrames(0);
      largestTimeRemaining.setFrames(0);
      return 0;
      }

   CAudioInitInfo *audioInitInfo = clipInitInfo.getAudioTrackInitInfo(0);

   retVal = audioInitInfo->queryAudioTimeRemaining(
      audioInitInfo->getMediaType(),
      audioInitInfo->getMediaDirectory(),
      totalTimeRemaining,
      largestTimeRemaining);

   if (retVal != 0)
      return retVal; // ERROR: Query failed

   // Temporary timecodes to hold query results for proxies
   CTimecode tmpTotalTime = totalTimeRemaining;
   CTimecode tmpLargestTime = largestTimeRemaining;

   // Iterate over video proxies, looking for a media location with
   // least time remaining and less than the media location for the main video
   for (int i = 1; i < audioTrackCount; ++i)
      {
      audioInitInfo = clipInitInfo.getAudioTrackInitInfo(i);
      if (!audioInitInfo->getVirtualMediaFlag()) // only for real media
         {
         retVal = audioInitInfo->queryAudioTimeRemaining(
            audioInitInfo->getMediaType(),
            audioInitInfo->getMediaDirectory(),
            tmpTotalTime,
            tmpLargestTime);
         if (retVal != 0)
            return retVal; // ERROR: Query failed

         if (tmpLargestTime.getAbsoluteTime()
             < largestTimeRemaining.getAbsoluteTime())
            {
            totalTimeRemaining = tmpTotalTime;
            largestTimeRemaining = tmpLargestTime;
            }
         }
      }

   return 0; // success

} // QueryAudioTimeRemaining

//////////////////////////////////////////////////////////////////////
//
// Function:   QueryAudioTimeTotal
//
// Description:Queries Media Interface for the total time on a
//             set of media storage devices given a Clip Scheme name
//
//             The media storage devices are specified in a Disk Scheme
//
//             The total time is calculated based on the image format type
//             found in the Main Video and Proxy Video sections of the Clip
//             Scheme.  If the Clip Scheme does not have a Main Video section
//             (i.e., it is an Audio-only Clip Scheme), then an error is
//             returned.
//
//             The values returned are for the main-video/proxy-video and
//             media location that has the least total time.  For example,
//             if the media location (e.g. raw disk) for the main video has
//             15 minutes total and the media location for a proxy has
//             5 minutes total, then this function will return 5 minutes.
//
//             Note: This function only works on Raw disks (CPMP or OGLV
//                   Bridge).  It does not work for Image Files (DPX, SGI, etc)
//                   or audio media disks as I do not know how to query
//                   a file system for free space. This may change in the
//                   future.
//
// Arguments:  string& diskSchemeName     Name of Disk Scheme
//             string& clipSchemeName     Name of Clip Scheme
//             CTimecode& totalTime
//                                        Reference to caller's CTimecode to
//                                        accept the total time remaining
//
// Returns:    Returns 0 if query succeed.  Non-zero if failed.
//
//////////////////////////////////////////////////////////////////////
int QueryAudioTimeTotal(const string& diskSchemeName,
                        const string& clipSchemeName,
                        CTimecode& totalTimeRemaining)
{
   int retVal;

   // Get the Clip Scheme
   CClipInitInfo clipInitInfo;
   retVal = clipInitInfo.initFromClipScheme(clipSchemeName);
   if (retVal != 0)
      return retVal; // ERROR: Could not find or read the Clip Scheme

   retVal = clipInitInfo.initMediaLocation(diskSchemeName);
   if (retVal != 0)
      return retVal; // ERROR: Could not set media locations from Disk Scheme

   return QueryAudioTimeTotal(clipInitInfo, totalTimeRemaining);
} // QueryAudioTimeTotal

//////////////////////////////////////////////////////////////////////
//
// Function:   QueryAudioTimeTotal
//
// Description:Same as above, except takes a CClipInitInfo instead of a
//             clip scheme name.
//
// Arguments:  CClipInitInfo& clipInitInfo
//                                        Info pertaining to a clip of the
//                                        type we are going to create
//             CTimecode& totalTime
//                                        Reference to caller's CTimecode to
//                                        accept the total time remaining
//
// Returns:    Returns 0 if query succeed.  Non-zero if failed.
//
//////////////////////////////////////////////////////////////////////
int QueryAudioTimeTotal(const CClipInitInfo& clipInitInfo,
                        CTimecode& totalTime)
{
   int retVal;

   int audioTrackCount = clipInitInfo.getAudioTrackCount();
   if (audioTrackCount < 1)
      {
      totalTime.setFrames(0);
      return 0;
      }

   CAudioInitInfo *audioInitInfo = clipInitInfo.getAudioTrackInitInfo(0);

   retVal = audioInitInfo->queryAudioTimeTotal(
      audioInitInfo->getMediaType(),
      audioInitInfo->getMediaDirectory(),
      totalTime);

   if (retVal != 0)
      return retVal; // ERROR: Query failed

   // Temporary timecodes to hold query results for proxies
   CTimecode tmpTotalTime = totalTime;

   // Iterate over video proxies, looking for a media location with
   // least time and less than the media location for the main video
   for (int i = 0; i < audioTrackCount; ++i)
      {
      audioInitInfo = clipInitInfo.getAudioTrackInitInfo(i);
      if (!audioInitInfo->getVirtualMediaFlag()) // only for real media
         {
         retVal = audioInitInfo->queryAudioTimeTotal(
            audioInitInfo->getMediaType(),
            audioInitInfo->getMediaDirectory(),
            tmpTotalTime);
         if (retVal != 0)
            return retVal; // ERROR: Query failed

         if (tmpTotalTime.getAbsoluteTime()
             < totalTime.getAbsoluteTime())
            {
            totalTime = tmpTotalTime;
            }
         }
      }

   return 0; // success

} // QueryAudioTimeTotal

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                           ####################################
// ###     CClipBackup Class       ###################################
// ####                           ####################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

// A hack to back up clip files before extending or shrinking

static std::string backupSetFileName = "LastBackupSet.txt";
static int numberBackupSetsToKeep = 5;

CClipBackup::CClipBackup(string const &clipDir)
   : clipDir_(clipDir), guidBackupSetPtr_(new CGuidBackupSet)
{}

CClipBackup::~CClipBackup()
{
   delete guidBackupSetPtr_;
}

CClipBackup::CClipBackup(CClipBackup const &rhs)
   : clipDir_(rhs.clipDir_),
     guidBackupSetPtr_(new CGuidBackupSet(*(rhs.guidBackupSetPtr_)))
{}

CClipBackup &CClipBackup::operator=(CClipBackup const &rhs)
{
   if (this == &rhs)
      return *this;

   clipDir_ = rhs.clipDir_;
   delete guidBackupSetPtr_;
   guidBackupSetPtr_ = new CGuidBackupSet(*(rhs.guidBackupSetPtr_));
   return *this;
}

void CClipBackup::SetClipDir(string path)
{
   clipDir_ = path;
}

int CClipBackup::BackupAllFiles()
{
   bool bRetVal;

   string clipDir = AddDirSeparator(clipDir_);

   bRetVal = WriteBackupSetIdentifier();
   if (bRetVal == false)
      return CLIP_ERROR_BACKUP_CLIP_FAILED;

   // files in the clip directory

   bRetVal = BackupFiles(clipDir + "*.xml");
   if (bRetVal == false)
      return CLIP_ERROR_BACKUP_CLIP_FAILED;

   bRetVal = BackupFiles(clipDir + "*.trk");
   if (bRetVal == false)
      return CLIP_ERROR_BACKUP_CLIP_FAILED;

   bRetVal = BackupFiles(clipDir + "*.fl1");
   if (bRetVal == false)
      return CLIP_ERROR_BACKUP_CLIP_FAILED;

   bRetVal = BackupFiles(clipDir + "*.clp");
   if (bRetVal == false)
      return CLIP_ERROR_BACKUP_CLIP_FAILED;

   // other critical files

   int retVal = BackupRFLFiles();
   if (retVal != 0)
      return retVal;

   return 0;
}

int CClipBackup::RestoreAllFiles()
{
   bool bRetVal;

   string clipDir = AddDirSeparator(clipDir_);

   bRetVal = ReadBackupSetIdentifier();
   if (bRetVal == false)
      return CLIP_ERROR_RESTORE_CLIP_FAILED;

   // files in the clip directory

   bRetVal = RestoreFiles(clipDir + "*.xml");
   if (bRetVal == false)
      return CLIP_ERROR_RESTORE_CLIP_FAILED;

   bRetVal = RestoreFiles(clipDir + "*.trk");
   if (bRetVal == false)
      return CLIP_ERROR_RESTORE_CLIP_FAILED;

   bRetVal = RestoreFiles(clipDir + "*.fl1");
   if (bRetVal == false)
      return CLIP_ERROR_RESTORE_CLIP_FAILED;

   bRetVal = RestoreFiles(clipDir + "*.clp");
   if (bRetVal == false)
      return CLIP_ERROR_RESTORE_CLIP_FAILED;

   // other critical files

   int retVal = RestoreRFLFiles();
   if (retVal != 0)
      return retVal;

   return 0;
}

bool CClipBackup::BackupFile(string file)
{
   CLogFile clipLogFile(guidBackupSetPtr_);
   clipLogFile.SetFileName(file);
   clipLogFile.SetKeep(numberBackupSetsToKeep);
   string backupFile = clipLogFile.NextFileName();
   return MTICopyFile(file.c_str(), backupFile.c_str(), true);
}

bool CClipBackup::RestoreFile(string file)
{
   CLogFile clipLogFile(guidBackupSetPtr_);
   clipLogFile.SetFileName(file);
   clipLogFile.SetKeep(numberBackupSetsToKeep);
   string backupFile = clipLogFile.LastExistingFileName();

   std::cout << "file:" << file << " <-- " << "backupFile:" << backupFile
             << std::endl;

   if (backupFile != "")
      return MTICopyFile(backupFile.c_str(), file.c_str(), false);
   else // no matching backup files exist
      return false;
}

int CClipBackup::GetRFLFileNames(vector<string> &RFLFileNames)
{
   CIniFile *ini;

   int iErrorCondition = 0;

   // open the LocalMachine ini file
   string strVideoStoreCfg = "$(CPMP_LOCAL_DIR)rawdisk.cfg";
   ini = CPMPOpenLocalMachineIniFile ();
   if (ini == 0)
      {
      iErrorCondition = CLIP_ERROR_FILE_OPEN_FAILED;
      return iErrorCondition;
      }

   strVideoStoreCfg = ini->ReadFileName ("MediaStorage", "RawDiskCfg",
                                         strVideoStoreCfg);

   delete ini;

   // open the raw disk config file
   if (DoesFileExist(strVideoStoreCfg) == false)
      {
      iErrorCondition = CLIP_ERROR_RAW_DISK_CFG_OPEN_FAILED;
      return iErrorCondition;
      }

   ini = CreateIniFile (strVideoStoreCfg);
   if (ini == 0)
      {
      iErrorCondition = CLIP_ERROR_RAW_DISK_CFG_OPEN_FAILED;
      return iErrorCondition;
      }

   // read the sections
   StringList listSection;
   ini->ReadSections (&listSection);

   // backup each free list file
   for (unsigned int iCnt = 0; iCnt < listSection.size(); iCnt++)
      {
      string strSection = listSection[iCnt];
      // do all the required keys exist?
      if (
         ini->KeyExists (strSection, "MediaIdentifier") &&
         ini->KeyExists (strSection, "FreeListFile") &&
         ini->KeyExists (strSection, "RawDiskSize")
         )
         {
         string strFreeListFile = ini->ReadFileName
            (strSection, "FreeListFile", "");
         if (DoesFileExist(strFreeListFile) == true)
            {
            RFLFileNames.push_back(strFreeListFile);
            }
         }
      }

   delete ini;

   return 0;
}

int CClipBackup::BackupRFLFiles()
{
  vector<string> RFLFileNames;
  int retVal = GetRFLFileNames(RFLFileNames);
  if (retVal != 0)
     return retVal;

  for (unsigned int i= 0; i < RFLFileNames.size(); ++i)
     {
     bool bRetVal = BackupFile(RFLFileNames[i]);
     if (bRetVal == false)
        return CLIP_ERROR_BACKUP_CLIP_FAILED;
     }

  return 0;
}

int CClipBackup::RestoreRFLFiles()
{
  vector<string> RFLFileNames;
  int retVal = GetRFLFileNames(RFLFileNames);
  if (retVal != 0)
     return retVal;

  for (unsigned int i = 0; i < RFLFileNames.size(); ++i)
     {
     bool bRetVal = RestoreFile(RFLFileNames[i]);
     if (bRetVal == false)
        return CLIP_ERROR_RESTORE_CLIP_FAILED;
     }

  return 0;
}

bool CClipBackup::WriteBackupSetIdentifier() const
{
   string backupSetFile = AddDirSeparator(clipDir_) + backupSetFileName;

   try
      {
      std::ofstream ofs(backupSetFile.c_str());
      ofs << guidBackupSetPtr_->GetBackupSetString();
      ofs.close();
      if (ofs == NULL)
         return false;
      }
   catch (...)
      {
      return false;
      }

   return true;
}

bool CClipBackup::ReadBackupSetIdentifier()
{
   string backupSetFile = AddDirSeparator(clipDir_) + backupSetFileName;
   string backupSetString;

   try
      {
      std::ifstream ifs(backupSetFile.c_str());
      ifs >> backupSetString;
      ifs.close();
      if (ifs == NULL)
         return false;
      }
   catch (...)
      {
      return false;
      }

   delete guidBackupSetPtr_;
   guidBackupSetPtr_ = new CGuidBackupSet(backupSetString);

   return true;
}
