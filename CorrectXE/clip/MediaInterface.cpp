// MediaInterface.cpp: implementation of the MediaInterface class.
//
/*
$Header: /usr/local/filmroot/clip/source/MediaInterface.cpp,v 1.43 2006/06/28 09:32:40 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifdef _MSC_VER
// Disable warning C4786: symbol greater than 255 characters,
// Work-around for problem when using Microsoft compiler
// with vector of strings or string pointers
#pragma warning(disable : 4786)
#endif // #ifdef _MSC_VER

#include "err_clip.h"
#include "MediaInterface.h"
#include "MediaAllocator.h"
#include "MediaLocation.h"
#include "MTIstringstream.h"
#include "ImageFormat3.h"
#include "ImageFileMediaAccess.h"
#include "timecode.h"


//////////////////////////////////////////////////////////////////////
// Static Data Member Initialization
//////////////////////////////////////////////////////////////////////

bool CMediaInterface::isInitialized = false;
CMediaInterface::MediaAccessList CMediaInterface::mediaAccessList;

// Sections and Keys in MTILocalMachine.ini for Media Storage
string CMediaInterface::mediaStorageSectionName      = "MediaStorage";
string CMediaInterface::diskSchemeFileKey            = "DiskSchemeFile";

string CDiskSchemeList::activeDiskSchemeSectionName  = "ActiveDiskSchemes";
string CDiskSchemeList::activeDiskSchemeKey          = "ActiveDiskScheme";
string CDiskSchemeList::diskSchemeSectionNamePrefix  = "DiskScheme";
string CDiskScheme::nameKey                          = "Name";
string CDiskScheme::mainVideoMediaRepositoryTypeKey  = "MainVideoMediaRepositoryType";
string CDiskScheme::mainVideoMediaLocationKey        = "MainVideoMediaLocation";
string CDiskScheme::proxyVideoMediaRepositoryTypeKey = "ProxyVideoMediaRepositoryType";
string CDiskScheme::proxyVideoMediaLocationKey       = "ProxyVideoMediaLocation";
string CDiskScheme::audioMediaDirectoryKey           = "AudioMediaDirectory";

// --------------------------------------------------------------------
// Media Type Name Map
   static EMediaType queryMediaTypeFromExtension(const string& mediaExtension);
   static string queryMediaExtension(EMediaType mediaType);

struct SMediaTypeMap
{
   const char* name;
   EMediaType type;
};

static const SMediaTypeMap mediaTypeMap[] =
{
   {"MEDIA_TYPE_IMAGE_FILE_DPX",    MEDIA_TYPE_IMAGE_FILE_DPX    }, // DPX Image File
   {"MEDIA_TYPE_IMAGE_FILE_TIFF",   MEDIA_TYPE_IMAGE_FILE_TIFF   }, // TIFF Image File
   {"MEDIA_TYPE_IMAGE_FILE_EXR",    MEDIA_TYPE_IMAGE_FILE_EXR    }, // EXR Image File
};

#define MEDIA_TYPE_COUNT (sizeof(mediaTypeMap)/sizeof(SMediaTypeMap))

struct SMediaTypeExtensionMap
{
   const char* extension;
   EMediaType type;
};

static const SMediaTypeExtensionMap mediaTypeExtensionMap[] =
{
   {"dpx",        MEDIA_TYPE_IMAGE_FILE_DPX    }, // DPX Image File
   {"tif",        MEDIA_TYPE_IMAGE_FILE_TIFF   }, // TIFF Image File
   {"exr",        MEDIA_TYPE_IMAGE_FILE_EXR    }, // EXR Image File
};

#define MEDIA_TYPE_EXTENSION_COUNT (sizeof(mediaTypeExtensionMap)/sizeof(SMediaTypeExtensionMap))

struct SMediaRepositoryTypeMap
{
   const char* name;
   EMediaRepositoryType type;
};

static const SMediaRepositoryTypeMap mediaRepositoryTypeMap[] =
{
   {"MEDIA_REPOSITORY_TYPE_FILE_SYSTEM", MEDIA_REPOSITORY_TYPE_FILE_SYSTEM}, // File system hierarchy
   {"MEDIA_REPOSITORY_TYPE_FILESYSTEM",  MEDIA_REPOSITORY_TYPE_FILE_SYSTEM}, //  alternate spelling
};

#define MEDIA_REPOSITORY_TYPE_COUNT (sizeof(mediaRepositoryTypeMap)/sizeof(SMediaRepositoryTypeMap))

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMediaInterface::CMediaInterface()
{
}

CMediaInterface::~CMediaInterface()
{
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//
// Function: close
//
// Description: Close the Media Interface object and its
//              constituent Media Access objects.  This is a static
//              function and should only be called once at the end of
//              the program
//
// Arguments:  none
//
// Returns:    bool, true if initialization succeeded
//                   false if failed, possibly due to missing or
//                   incorrectly formatted configuration file
//
//////////////////////////////////////////////////////////////////////
void CMediaInterface::close()
{

}

//////////////////////////////////////////////////////////////////////

bool CMediaInterface::isMediaTypeImageFile(EMediaType mediaType)
{
   switch (mediaType)
   {
      // Media Types that are loose image files
      case MEDIA_TYPE_IMAGE_FILE_DPX :
      case MEDIA_TYPE_IMAGE_FILE_TIFF :
      case MEDIA_TYPE_IMAGE_FILE_EXR :
         break;

      default:
         return false;;
   }

   return true;
}

bool CMediaInterface::isMediaTypeImageRepository(EMediaType mediaType)
{
   return false;
}

bool CMediaInterface::isMediaTypeValid(EMediaType mediaType)
{
   return isMediaTypeImageFile(mediaType);
}

//------------------------------------------------------------------------
//
// Function:    queryMediaType
//
// Description: Gets the media type given a media type name in a string.
//              This is a static member function which means that it can
//              be called without a CMediaInterface object instance.
//              See mediaTypeMap table for media type/media name pairs.
//
// Arguments    string & mediaTypeName    Name of media type
//
// Returns:     EMediaType enum value.  MEDIA_TYPE_INVALID if the name
//              is not recognized.
//
//------------------------------------------------------------------------
EMediaType CMediaInterface::queryMediaType(const string& mediaTypeName)
{
   for (int i = 0; i < (int)MEDIA_TYPE_COUNT; ++i)
      {
      if (EqualIgnoreCase(mediaTypeMap[i].name, mediaTypeName))
         return mediaTypeMap[i].type;
      }

   return MEDIA_TYPE_INVALID;
}

//------------------------------------------------------------------------
//
// Function:    queryMediaTypeName
//
// Description: Gets name string of media type.  This is a static
//              member function which means that it can be called without
//              a CMediaInterface object instance.
//              See mediaTypeMap table for media type/media name pairs.
//
// Arguments    EMediaType mediaType      Media Type
//
// Returns:     string instance containing media type name.  Returns
//              "MEDIA_TYPE_INVALID" if the media type is invalid or
//              unknown
//
//------------------------------------------------------------------------
string CMediaInterface::queryMediaTypeName(EMediaType mediaType)
{
   for (int i = 0; i < (int)MEDIA_TYPE_COUNT; ++i)
      {
      if (mediaTypeMap[i].type == mediaType)
         return string(mediaTypeMap[i].name);
      }

   return string("MEDIA_TYPE_INVALID");
}

//------------------------------------------------------------------------
//
// Function:    queryMediaTypeFromExtension
//
// Description: Gets the media type given a file extension in a string.
//              This is a static member function which means that it can
//              be called without a CMediaInterface object instance.
//              See mediaTypeExtensionMap table for type/extension pairs.
//
// Arguments    string & mediaExtension  extension, e.g. "dpx"
//
// Returns:     EMediaType enum value.  MEDIA_TYPE_INVALID if the extension
//              is not recognized.
//
//------------------------------------------------------------------------
EMediaType CMediaInterface::queryMediaTypeFromExtension(const string& mediaExtension)
{
   for (int i = 0; i < (int)MEDIA_TYPE_EXTENSION_COUNT; ++i)
	  {
      if (EqualIgnoreCase(mediaTypeExtensionMap[i].extension, mediaExtension))
         return mediaTypeExtensionMap[i].type;
      }

   return MEDIA_TYPE_INVALID;
}

//------------------------------------------------------------------------
//
// Function:    queryMediaTypeExtension
//
// Description: Gets file extension for media type.  This is a static
//              member function which means that it can be called without
//              a CMediaInterface object instance.
//              See mediaTypeExtensionMap table for type/extension pairs.
//
// Arguments    EMediaType mediaType      Media Type
//
// Returns:     string instance containing file extension.  Returns
//              "" if the media type is invalid or unknown.
//
//------------------------------------------------------------------------
string CMediaInterface::queryMediaTypeExtension(EMediaType mediaType)
{
   for (int i = 0; i < (int)MEDIA_TYPE_EXTENSION_COUNT; ++i)
      {
      if (mediaTypeExtensionMap[i].type == mediaType)
         return string(mediaTypeExtensionMap[i].extension);
      }

   return string("");
}

//////////////////////////////////////////////////////////////////////

bool CMediaInterface::isMediaRepositoryARawDisk(EMediaRepositoryType repType)
{
   // REMOVE THIS METHOD

   return false;
}

bool CMediaInterface::isMediaRepositoryTypeValid(EMediaRepositoryType repType)
{
   return repType == MEDIA_REPOSITORY_TYPE_FILE_SYSTEM;
}

//------------------------------------------------------------------------
//
// Function:    queryMediaRepositoryType
//
// Description: Gets the media repository type given a media repository
//              type name in a string.
//              This is a static member function which means that it can
//              be called without a CMediaInterface object instance.
//              See mediaRepositoryTypeMap table for media repository
//              type/media repository name pairs.
//
// Arguments    string & repTypeName    Name of media repository type
//
// Returns:     EMediaRepositoryType enum value.
//              MEDIA_REPOSITORY_TYPE_INVALID if the name is not recognized.
//
//------------------------------------------------------------------------
EMediaRepositoryType CMediaInterface::queryMediaRepositoryType(
                                                 const string& repTypeName)
{
   for (int i = 0; i < (int)MEDIA_REPOSITORY_TYPE_COUNT; ++i)
      {
      if (EqualIgnoreCase(mediaRepositoryTypeMap[i].name, repTypeName))
         return mediaRepositoryTypeMap[i].type;
      }

   return MEDIA_REPOSITORY_TYPE_INVALID;
}

//------------------------------------------------------------------------
//
// Function:    queryMediaRepositoryTypeName
//
// Description: Gets name string of media repository type. This is a static
//              member function which means that it can be called without
//              a CMediaInterface object instance.
//              See mediaRepositoryTypeMap table for media repository
//              type/media repository name pairs.
//
// Arguments    EMediaRepositoryType repType      Media Repository Type
//
// Returns:     string instance containing media repository type name.
//              Returns "MEDIA_TYPE_INVALID" if the media type is invalid or
//              unknown
//
//------------------------------------------------------------------------
string CMediaInterface::queryMediaRepositoryTypeName(EMediaRepositoryType repType)
{
   for (int i = 0; i < (int)MEDIA_REPOSITORY_TYPE_COUNT; ++i)
      {
      if (mediaRepositoryTypeMap[i].type == repType)
         return string(mediaRepositoryTypeMap[i].name);
      }

   return string("MEDIA_REPOSITORY_TYPE_INVALID");
}

//------------------------------------------------------------------------
//
// Function:    isMediaHackCommandValid
//
// Description: Checks validity of a media hack command
//
// Arguments    EMediaHackCommand hackCommand      Media Hack Command
//
// Returns:     true if the command is valid
//
//------------------------------------------------------------------------
bool isMediaHackCommandValid(EMediaHackCommand hackCommand)
{
   switch (hackCommand)
      {
      case MEDIA_HACK_HIDE_ALPHA_MATTE:
      case MEDIA_HACK_SHOW_ALPHA_MATTE:
      case MEDIA_HACK_DESTROY_ALPHA_MATTE:
      case MEDIA_HACK_CACHE_FRAME:
		 return true;

	  default:
	  	break;
	  }

   return false;
}

//////////////////////////////////////////////////////////////////////
//
// Function:    queryRawDiskInfo
//
// Description: This function retrieves information about CPMP Raw
//              Disks and OGLV Raw Disks.  The information is returned
//              as a list of SRawDiskInfo structures, each structure containing
//              information about one raw disk. The information for the
//              CPMP Raw Disks is obtained from the rawdisk.cfg and .rfl files.
//              The info about the OGLV Raw Disk is obtained via the OGLV Bridge.
//
//              The information returned for each in the SRawDiskInfo structure
//              is:
//                EMediaType mediaType
//                   Media type, typically MEDIA_TYPE_RAW or MEDIA_TYPE_RAW_OGLV
//                string mediaIdentifier
//                   Media identifier, e.g. the raw disk name for CPMP Raw Disks
//                   or "OGLV" if the media type is MEDIA_TYPE_RAW_OGLV
//                CMediaAccess *mediaAccess
//                   Pointer to the internal CMediaAccess subtype instance
//                   associated with the media type and identifier.  The
//                   CMediaAccess pointer can be used to make other queries, etc.
//                   This pointer value is only set when the Media Type is
//                   MEDIA_TYPE_RAW or MEDIA_TYPE_RAW_OGLV, 0 otherwise.
//                MTI_INT64 totalFreeBytes
//                   Total number of free bytes on the raw disk.  Divide by 512
//                   to get the number of free blocks
//                MTI_INT64 maxContiguousFreeBytes
//                   Size, in bytes, of the largest continguous free area on
//                   the raw disk
//
// Arguments:   RawDiskInfoList& resultList    STL vector of SRawDiskInfo
//                                             structures.
//
// Returns:     0 on success, non-zero otherwise
//
//////////////////////////////////////////////////////////////////////
int CMediaInterface::queryRawDiskInfo(RawDiskInfoList &resultList)
{
   resultList.clear(); // Clear the caller's list before anything else

   // REMOVE THIS METHOD

   return 0;
}

//////////////////////////////////////////////////////////////////////
//
// Function:    queryImageRepositoryInfo
//
// Description: This function retrieves information about video media
//              storage in image file repositories.  The information
//              is returned as a list of SImageRepositoryInfo
//              structures, each structure containing information about
//              configuration. The information is obtained from the
//              ImageFileMedia.cfg file.
//
//              The information returned for each in the SImageRepositoryInfo
//              structure is:
//                string repositoryName
//                   the name of the repository (used in DiskScheme.ini)
//                EMediaType mediaType
//                   Media type, typically MEDIA_TYPE_DPX_FILES
//                string mediaIdentifier
//                   Media identifier, which is the root folder for the
//                   file system hierarchy that holds the image files
//                   (just like the audio structure)
//
// Arguments:   SImageRepositoryInfoList& resultList
//                   STL vector of SImageRepositoryInfo structures.
//
// Returns:     0 on success, non-zero otherwise
//
//////////////////////////////////////////////////////////////////////

int CMediaInterface::queryImageRepositoryInfo(
                                   ImageRepositoryInfoList &resultList)
{
   resultList.clear(); // Clear the caller's list before anything else

   // REMOVE THIS METHOD

   return 0;

}

//////////////////////////////////////////////////////////////////////
//
// Function:   CheckRFL
//
// Description:Checks the free list for a particular rawdisk
//
// Arguments:  const string &mediaIdentifier   rawdisk name
//
// Returns:    Returns 0 if rfl is OK.  Non-zero if not.
//
//////////////////////////////////////////////////////////////////////
int CMediaInterface::CheckRFL(const string &mediaIdentifier)
{
   int retVal;

   // REMOVE THIS METHOD

   return 0;
}

//////////////////////////////////////////////////////////////////////
//
// Function:   queryImageTimeRemaining
//
// Description:Queries Media Interface for the time remaining on a given
//             media storage device, image format and frame rate.
//             The media storage device is specified as a Media Type and
//             a Media Identifier.  The image format is specified
//             as a well-formed instance of a CImageFormat class.
//
//             The query returns both the total time remaining on the
//             media storage and the time remaining in the largest contiguous
//             free space.
//
//             Note: This function only works on Raw disks (CPMP or OGLV
//                   Bridge).  It does not work for Image Files (DPX, SGI, etc)
//                   as I do not know how to query a file system for free
//                   space.  This may change in the future.
//
// Arguments:  EMediaType mediaType       Media type: MEDIA_TYPE_RAW, etc.
//             string& mediaIdentifier    Media identifier, e.g., "rd1"
//             CImageFormat &imageFormat Reference to a well-formed instance
//                                        of the image format class
//             double frameRate           Frame per second
//             CTimecode& totalTimeRemaining
//                                        Reference to caller's CTimecode to
//                                        accept the total time remaining
//             CTimecode& largestTimeRemaining
//                                        Reference to caller's CTimecode to
//                                        accept the time remaining in largest
//                                        contiguous space
//
// Returns:    Returns 0 if query succeed.  Non-zero if failed.
//
//////////////////////////////////////////////////////////////////////
int CMediaInterface::queryImageTimeRemaining(EMediaType mediaType,
                                             const string& mediaIdentifier,
                                             const CImageFormat& imageFormat,
                                             double frameRate,
                                             CTimecode& totalTimeRemaining,
                                             CTimecode& largestTimeRemaining)
{
   int retVal;

   // Query the number of fields available on the given media storage device
   MTI_UINT32 totalFields, largestContiguousFields;
   retVal = queryImageFieldsRemaining(mediaType, mediaIdentifier, imageFormat,
                                      &totalFields, &largestContiguousFields);
   if (retVal != 0)
      return retVal;

   // Convert field count to frame count if interlaced
   if (imageFormat.getInterlaced())
      {
      totalFields /= 2;
      largestContiguousFields /= 2;
      }

   // Set the caller's timecodes from the number of available frames
   int timecodeFlags = 0;
   int timecodeFrameRate = (int)(frameRate + .5);
   CTimecode protoTC(timecodeFlags, timecodeFrameRate);

   totalTimeRemaining = protoTC;
   totalTimeRemaining.setAbsoluteTime(totalFields);

   largestTimeRemaining = protoTC;
   largestTimeRemaining.setAbsoluteTime(largestContiguousFields);

   return 0;  // Success
}

//////////////////////////////////////////////////////////////////////
//
// Function:   queryImageTimeRemaining
//
// Description:Queries Media Interface for the time remaining on a given
//             media storage device, image format and frame rate.
//             The media storage device is specified as a Media Type and
//             a Media Identifier.  The image format is specified
//             as a well-formed instance of a CImageFormat class.
//
//             The query returns both the total time remaining on the
//             media storage and the time remaining in the largest contiguous
//             free space.
//
//             Note: This function only works on Raw disks (CPMP or OGLV
//                   Bridge).  It does not work for Image Files (DPX, SGI, etc)
//                   as I do not know how to query a file system for free
//                   space.  This may change in the future.
//
// Arguments:  EMediaType mediaType       Media type: MEDIA_TYPE_RAW, etc.
//             string& mediaIdentifier    Media identifier, e.g., "rd1"
//             CImageFormat &imageFormat Reference to a well-formed instance
//                                        of the image format class
//             double frameRate           Frame per second
//             CTimecode& totalTimeRemaining
//                                        Reference to caller's CTimecode to
//                                        accept the total time remaining
//             CTimecode& largestTimeRemaining
//                                        Reference to caller's CTimecode to
//                                        accept the time remaining in largest
//                                        contiguous space
//
// Returns:    Returns 0 if query succeed.  Non-zero if failed.
//
//////////////////////////////////////////////////////////////////////
int CMediaInterface::queryAudioTimeRemaining(EMediaType mediaType,
                                             const string& mediaIdentifier,
                                             const CAudioFormat& audioFormat,
                                             double frameRate,
                                             CTimecode& totalTimeRemaining,
                                             CTimecode& largestTimeRemaining)
{
   int retVal;

   // Query the number of fields available on the given media storage device
   MTI_UINT32 totalFields, largestContiguousFields;
   retVal = queryAudioFieldsRemaining(mediaType, mediaIdentifier, audioFormat,
                                      &totalFields, &largestContiguousFields);
   if (retVal != 0)
      return retVal;

   // Convert field count to frame count if interlaced
   if (audioFormat.getInterlaced())
      {
      totalFields /= 2;
      largestContiguousFields /= 2;
      }

   // Set the caller's timecodes from the number of available frames
   int timecodeFlags = 0;
   int timecodeFrameRate = (int)(frameRate + .5);
   CTimecode protoTC(timecodeFlags, timecodeFrameRate);

   totalTimeRemaining = protoTC;
   totalTimeRemaining.setAbsoluteTime(totalFields);

   largestTimeRemaining = protoTC;
   largestTimeRemaining.setAbsoluteTime(largestContiguousFields);

   return 0;  // Success
}

//////////////////////////////////////////////////////////////////////
//
// Function:   queryImageFieldsRemaining
//
// Description:
// Description:Queries Media Interface for the free space remaining on a given
//             media storage device and a given image format.
//             The media storage device is specified as a Media Type and
//             a Media Identifier.  The image format is specified
//             as a well-formed instance of a CImageFormat class.
//
//             The query returns both the total number of fields remaining on
//             the media storage and the fields remaining in the largest
//             contiguous free space.
//
//             Note: This function only works on Raw disks (CPMP or OGLV
//                   Bridge).  It does not work for Image Files (DPX, SGI, etc)
//                   as I do not know how to query a file system for free
//                   space.  This may change in the future.
//
// Arguments:  CMediaAccess *mediaAccess    Media storage
//             CImageFormat &imageFormat
//                                     Reference to a well-formed instance
//                                      of the image format class
//             MTI_UINT32 *totalFields
//                                     Ptr to caller's variable to accept
//                                     total number of available fields
//             MTI_UINT32 *largestContiguousFields
//                                     Ptr to caller's variable to accept
//                                     number of fields in largest contiguous
//                                     free space
// Returns:    Returns 0 if query succeed.  Non-zero if failed.
//             Returns non-zero if either of totalFields or maxContiguousFields
//             is 0
//
//////////////////////////////////////////////////////////////////////
int CMediaInterface::queryImageFieldsRemaining(EMediaType mediaType,
                                               const string& mediaIdentifier,
                                               const CImageFormat& imageFormat,
                                               MTI_UINT32 *totalFields,
                                           MTI_UINT32 *largestContiguousFields)
{
   int retVal;

   // Check output arguments for null pointers
   if (totalFields == 0 || largestContiguousFields == 0)
      return CLIP_ERROR_UNEXPECTED_NULL_ARGUMENT;

   CMediaAccess *mediaAccess = 0;
   retVal = queryImageFieldsPrepare(mediaType, mediaIdentifier, imageFormat,
                                    &mediaAccess);
   if (retVal) return retVal;

   // Query the amount of free space on the media storage
   MTI_INT64 totalFreeBytes, maxContiguousFreeBytes;
//   retVal = mediaAccess->queryFreeSpace(&totalFreeBytes,
//                                        &maxContiguousFreeBytes);
   retVal = mediaAccess->queryFreeSpaceEx(imageFormat, &totalFreeBytes,
                                          &maxContiguousFreeBytes);
   if (retVal != 0)
      return retVal; // ERROR: Free space query failed

   // Get the field size (in bytes) for the particular image format
   // and adjust for blocking factor specific to the mediaIdentifier
   MTI_INT64 fieldSize = imageFormat.getBytesPerField();
   fieldSize = mediaAccess->adjustToBlocking(fieldSize, true);
   if (fieldSize < 1) // Avoid divide-by-zero
      return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR;

   // Calculate the number of fields that will fit in the space available
   *totalFields = (MTI_UINT32)(totalFreeBytes / fieldSize);
   *largestContiguousFields
                       = (MTI_UINT32)(maxContiguousFreeBytes / fieldSize);

   return 0; // Success

} // queryImageFieldsRemaining

//////////////////////////////////////////////////////////////////////
//
// Function:   queryImageFieldsRemaining
//
// Description:
// Description:Queries Media Interface for the free space remaining on a given
//             media storage device and a given image format.
//             The media storage device is specified as a Media Type and
//             a Media Identifier.  The image format is specified
//             as a well-formed instance of a CImageFormat class.
//
//             The query returns both the total number of fields remaining on
//             the media storage and the fields remaining in the largest
//             contiguous free space.
//
//             Note: This function only works on Raw disks (CPMP or OGLV
//                   Bridge).  It does not work for Image Files (DPX, SGI, etc)
//                   as I do not know how to query a file system for free
//                   space.  This may change in the future.
//
// Arguments:  CMediaAccess *mediaAccess    Media storage
//             CImageFormat &imageFormat
//                                     Reference to a well-formed instance
//                                      of the image format class
//             MTI_UINT32 *totalFields
//                                     Ptr to caller's variable to accept
//                                     total number of available fields
//             MTI_UINT32 *largestContiguousFields
//                                     Ptr to caller's variable to accept
//                                     number of fields in largest contiguous
//                                     free space
// Returns:    Returns 0 if query succeed.  Non-zero if failed.
//             Returns non-zero if either of totalFields or maxContiguousFields
//             is 0
//
//////////////////////////////////////////////////////////////////////
int CMediaInterface::queryAudioFieldsRemaining(
    EMediaType mediaType,
    const string& mediaIdentifier,
    const CAudioFormat& audioFormat,
    MTI_UINT32 *totalFields,
    MTI_UINT32 *largestContiguousFields)
{
   int retVal;

   // Check output arguments for null pointers
   if (totalFields == 0 || largestContiguousFields == 0)
      return CLIP_ERROR_UNEXPECTED_NULL_ARGUMENT;

   CMediaAccess *mediaAccess = 0;
   // mlm: to-do: rename *Prepare() functions to indicate what they
   // really do: get a validated mediaAccess
   retVal = queryAudioFieldsPrepare(mediaType, mediaIdentifier, audioFormat,
                                    &mediaAccess);
   if (retVal) return retVal;

   // Query the amount of free space on the media storage
   MTI_INT64 totalFreeBytes, maxContiguousFreeBytes;
//   retVal = mediaAccess->queryFreeSpace(&totalFreeBytes,
//                                        &maxContiguousFreeBytes);
   retVal = mediaAccess->queryFreeSpaceEx(audioFormat, &totalFreeBytes,
                                          &maxContiguousFreeBytes);
   if (retVal != 0)
      return retVal; // ERROR: Free space query failed

   // Get the field size (in bytes) for the particular image format
   // and adjust for blocking factor specific to the mediaIdentifier
   double fieldSize =
       audioFormat.getBytesPerSample() * audioFormat.getSamplesPerField() *
       audioFormat.getChannelCount();
   if (fieldSize < 1) // Avoid divide-by-zero
      return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR;

   // Calculate the number of fields that will fit in the space available
   *totalFields = (MTI_UINT32)((double)totalFreeBytes / fieldSize);
   *largestContiguousFields
                       = (MTI_UINT32)((double)maxContiguousFreeBytes / fieldSize);

   return 0; // Success

} // queryAudioFieldsRemaining

int CMediaInterface::queryImageTimeTotal(EMediaType mediaType,
                                         const string &mediaIdentifier,
                                         const CImageFormat &imageFormat,
                                         double frameRate,
                                         CTimecode &totalTime)
{
   int retVal;

   // Query the number of fields available on the given media storage device
   MTI_UINT32 totalFields;
   retVal = queryImageFieldsTotal(mediaType, mediaIdentifier, imageFormat,
                                  &totalFields);
   if (retVal != 0)
      return retVal;

   // Convert field count to frame count if interlaced
   if (imageFormat.getInterlaced())
      {
      totalFields /= 2;
      }

   // Set the caller's timecodes from the number of available frames
   int timecodeFlags = 0;
   int timecodeFrameRate = (int)(frameRate + .5);
   CTimecode protoTC(timecodeFlags, timecodeFrameRate);

   totalTime = protoTC;
   totalTime.setAbsoluteTime(totalFields);

   return 0;  // Success
}

int CMediaInterface::queryAudioTimeTotal(EMediaType mediaType,
                                         const string &mediaIdentifier,
                                         const CAudioFormat &audioFormat,
                                         double frameRate,
                                         CTimecode &totalTime)
{
   int retVal;

   // Query the number of fields available on the given media storage device
   MTI_UINT32 totalFields;
   retVal = queryAudioFieldsTotal(mediaType, mediaIdentifier, audioFormat,
                                  &totalFields);
   if (retVal != 0)
      return retVal;

   // Convert field count to frame count if interlaced
   if (audioFormat.getInterlaced())
      {
      totalFields /= 2;
      }

   // Set the caller's timecodes from the number of available frames
   int timecodeFlags = 0;
   int timecodeFrameRate = (int)(frameRate + .5);
   CTimecode protoTC(timecodeFlags, timecodeFrameRate);

   totalTime = protoTC;
   totalTime.setAbsoluteTime(totalFields);

   return 0;  // Success
}

int CMediaInterface::queryImageFieldsTotal(EMediaType mediaType,
                                           const string& mediaIdentifier,
                                           const CImageFormat& imageFormat,
                                           MTI_UINT32 *totalFields)
{
   int retVal;

   // Check output arguments for null pointers
   if (totalFields == 0)
      return CLIP_ERROR_UNEXPECTED_NULL_ARGUMENT;

   CMediaAccess *mediaAccess = 0;
   retVal = queryImageFieldsPrepare(mediaType, mediaIdentifier, imageFormat,
                                    &mediaAccess);
   if (retVal) return retVal;

   // Query the amount of free space on the media storage
   MTI_INT64 totalBytes;
   retVal = mediaAccess->queryTotalSpace(&totalBytes);
   if (retVal != 0)
      return retVal; // ERROR: Free space query failed

   // Get the field size (in bytes) for the particular image format
   // and adjust for blocking factor specific to the mediaIdentifier
   MTI_INT64 fieldSize = imageFormat.getBytesPerField();
   fieldSize = mediaAccess->adjustToBlocking(fieldSize, true);
   if (fieldSize < 1) // Avoid divide-by-zero
      return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR;

   // Calculate the number of fields that will fit in the space available
   *totalFields = (MTI_UINT32)(totalBytes / fieldSize);

   return 0; // Success
} // queryImageFieldsTotal

int CMediaInterface::queryAudioFieldsTotal(EMediaType mediaType,
                                           const string& mediaIdentifier,
                                           const CAudioFormat& audioFormat,
                                           MTI_UINT32 *totalFields)
{
   int retVal;

   // Check output arguments for null pointers
   if (totalFields == 0)
      return CLIP_ERROR_UNEXPECTED_NULL_ARGUMENT;

   CMediaAccess *mediaAccess = 0;
   retVal = queryAudioFieldsPrepare(mediaType, mediaIdentifier, audioFormat,
                                    &mediaAccess);
   if (retVal) return retVal;

   // Query the amount of free space on the media storage
   MTI_INT64 totalBytes;
   retVal = mediaAccess->queryTotalSpace(&totalBytes);
   if (retVal != 0)
      return retVal; // ERROR: Free space query failed

   // Get the field size (in bytes) for the particular image format
   // and adjust for blocking factor specific to the mediaIdentifier
   double fieldSize =
       audioFormat.getBytesPerSample() * audioFormat.getSamplesPerField() *
       audioFormat.getChannelCount();
   if (fieldSize < 1) // Avoid divide-by-zero
      return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR;

   // Calculate the number of fields that will fit in the space available
   *totalFields = (MTI_UINT32)((double)totalBytes / fieldSize);

   return 0; // Success
} // queryAudioFieldsTotal

int CMediaInterface::queryImageFieldsPrepare(EMediaType mediaType,
                                             const string& mediaIdentifier,
                                             const CImageFormat& imageFormat,
                                             CMediaAccess **mediaAccess)
{
   // REMOVE THIS METHOD

   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

int CMediaInterface::queryAudioFieldsPrepare(EMediaType mediaType,
                                             const string& mediaIdentifier,
                                             const CAudioFormat& audioFormat,
                                             CMediaAccess **mediaAccess)
{
   // REMOVE THIS METHOD

   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//
// Function: allocateSpace
//
// Description: Allocates space in the requested media storage
//
// Arguments:  EMediaType mediaType    Type of media storage
//                                     MEDIA_TYPE_RAW
//                                     MEDIA_TYPE_OGLV
//                                     MEDIA_TYPE_RAW_AUDIO
//                                     MEDIA_TYPE_IMAGE_FILE_*
//             string mediaIdentifier  Name of media storage, depending
//                                     on media type.
//             string historyDirectory Name of history storage, depending
//                                     on media type
//             CMediaFormat& mediaFormat
//             CTimecode& inTimecode
//             MTI_UINT32 frameCount   Number of frames to be allocated
//
// Returns:    If allocation succeeded, returns a pointer to new
//             CMediaAllocator instance that is used obtain the locations
//             of individual fields.  Caller's responsibility to delete
//             returned CMediaAllocator object when done with it.
//             If allocation failed, 0 is returned
//
//////////////////////////////////////////////////////////////////////
CMediaAllocator* CMediaInterface::
allocateSpace(EMediaType mediaType,
              const string& mediaIdentifier,
              const string& historyDirectory,
              const CMediaFormat& mediaFormat,
              const CTimecode& frame0Timecode, MTI_UINT32 frameCount,
              const string &relMediaPath)
{
   CMediaAccess* mediaAccess;

   // Format the media identifier for image file media
   string newMediaIdentifier = adjustMediaIdentifier(mediaType, mediaIdentifier,
                                                     mediaFormat, frame0Timecode,
                                                     relMediaPath);

   // Get an instance of the correct subtype of CMediaAccess for
   // the caller's mediaType/mediaIdentifier pair.  May either
   // create a new instance or return a previously created instance.
   mediaAccess = registerMediaAccess(mediaType,
                                     newMediaIdentifier,
                                     historyDirectory,
                                     mediaFormat);
   if (mediaAccess == 0)
      {
      // Error: could not get a media access object
      return 0;
      }

   // Allocate via media access object
   return (mediaAccess->allocate(mediaFormat, frame0Timecode, frameCount));
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//
// Function: checkSpace
//
// Description: Checks if there is sufficient space in the requested
//              media storage
//
// Arguments:  EMediaType mediaType    Type of media storage
//                                     MEDIA_TYPE_RAW
//                                     MEDIA_TYPE_OGLV
//                                     MEDIA_TYPE_RAW_AUDIO
//                                     MEDIA_TYPE_IMAGE_FILE_*
//             string mediaIdentifier  Name of media storage, depending
//                                     on media type.
//             CMediaFormat& mediaFormat
//             CTimecode& inTimecode
//             MTI_UINT32 frameCount   Number of frames to be allocated
//
// Returns:
//
//////////////////////////////////////////////////////////////////////
int CMediaInterface::checkSpace(EMediaType mediaType,
                                const string& mediaIdentifier,
                                const CMediaFormat& mediaFormat,
                                MTI_UINT32 frameCount)
{
   // REMOVE THIS METHOD or implement it for image files!

   return 0;
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//
// Function:    queryMediaAccessList
//
// Description: An overloaded function that finds CMediaAccess subtype
//              instances in the mediaAccessList that meet the caller's
//              criteria for mediaType and/or mediaIdentifer.  Pointers
//              to the CMediaAccess instances are returned in the caller's
//              resultList.
//
// Arguments:   EMediaType mediaType             Search criteria
//              const string& mediaIdentifier    Search criteria
//              MediaAccessList& resultList      Caller's list into which
//                                               CMediaAccess instance pointers
//                                               are inserted
//
// Returns:     Count of matching CMediaAccess instance pointers in
//              resultList
//
//////////////////////////////////////////////////////////////////////
int CMediaInterface::queryMediaAccessList(EMediaType mediaType,
                                          MediaAccessList& resultList)
{
   resultList.clear();
   CMediaAccess *mediaAccess;

   for (MediaAccessListIterator mediaAccessIterator = mediaAccessList.begin();
        mediaAccessIterator < mediaAccessList.end();
        ++mediaAccessIterator)
      {
      mediaAccess = *mediaAccessIterator;
      if (mediaAccess != 0 && mediaAccess->getMediaType() == mediaType)
         {
         resultList.push_back(mediaAccess);
         }
      }

   return resultList.size();
}

int CMediaInterface::queryMediaAccessList(const string& mediaIdentifier,
                                          MediaAccessList& resultList)
{
   resultList.clear();
   CMediaAccess *mediaAccess;

   for (MediaAccessListIterator mediaAccessIterator = mediaAccessList.begin();
        mediaAccessIterator < mediaAccessList.end();
        ++mediaAccessIterator)
      {
      mediaAccess = *mediaAccessIterator;
      if (mediaAccess != 0
          && mediaAccess->getMediaIdentifier() == mediaIdentifier)
         {
         resultList.push_back(mediaAccess);
         }
      }

   return resultList.size();
}

int CMediaInterface::queryMediaAccessList(EMediaType mediaType,
                                          const string& mediaIdentifier,
                                          MediaAccessList& resultList)
{
   resultList.clear();
   CMediaAccess *mediaAccess;

   for (MediaAccessListIterator mediaAccessIterator = mediaAccessList.begin();
        mediaAccessIterator < mediaAccessList.end();
        ++mediaAccessIterator)
      {
      mediaAccess = *mediaAccessIterator;
      if (mediaAccess != 0
          && mediaAccess->getMediaType() == mediaType
          && mediaAccess->getMediaIdentifier() == mediaIdentifier)
         {
         resultList.push_back(mediaAccess);
         }
      }

   return resultList.size();
}

// queryMediaRepositoryList is like queryMediaAccessList, but
// queryMediaRepositoryList has another layer of indirection, it
// retrieves the repository identifier from the argument
// mediaIdentifier.  The purpose of this is to work around that the
// mediaIdentifier's purpose is different for raw disk mediaAccess
// versus other types of mediaAccess.  Raw disk mediaAccess is the
// same as a VideoMediaLocation from the DiskScheme file; other types
// of mediaAccess are full paths to image files, or to audio files,
// etc.
int CMediaInterface::queryMediaRepositoryList(EMediaType mediaType,
                                              const string& mediaIdentifier,
                                              MediaAccessList& resultList)
{
   // REMOVE THIS METHOD

   resultList.clear();
   return resultList.size();
}


//////////////////////////////////////////////////////////////////////

int CMediaInterface::initializeRawMediaAccess()
{
   // REMOVE THIS METHOD

   return 0;
}

string CMediaInterface::getRawDiskCfgFileName()
{
   // REMOVE THIS METHOD

   return "";
}

string CMediaInterface::getDiskSchemeFileName()
{
   // REMOVE THIS METHOD

   return "";
}

int CMediaInterface::initializeRawOGLVMediaAccess()
{
   // REMOVE THIS METHOD

   return 0;
}


//////////////////////////////////////////////////////////////////////

int CMediaInterface::initializeImageRepositoryMediaAccess()
{
   // REMOVE THIS METHOD

   return 0;
}

//////////////////////////////////////////////////////////////////////

int CMediaInterface::initializeAudioMediaAccess()
{
   // REMOVE THIS METHOD

   return 0;
}

//////////////////////////////////////////////////////////////////////


string CMediaInterface::getImageRepositoryCfgFileName()
{
   // REMOVE THIS METHOD

   return "";
}


//////////////////////////////////////////////////////////////////////
/* static */
int CMediaInterface::addNewImageRepository(
   CImageRepositoryMediaAccess *newMediaAccess,
   bool overwriteOkFlag)
{
   // REMOVE THIS METHOD

   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}


//////////////////////////////////////////////////////////////////////
/* static */
int CMediaInterface::addNewDiskScheme(
   CDiskScheme *newDiskScheme,
   bool overWriteOkFlag)
{
   // REMOVE THIS METHOD

   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

//////////////////////////////////////////////////////////////////////
/* static */
int CMediaInterface::renameBinMedia(string oldBinPath, string newBinPath)
{
   // When a bin is renamed/moved, any media repository that stores
   // its media in a directory path that uses the bin path or clip
   // name also needs renaming/moving.  RenameMediaFiles() iterates
   // over all mediaAccess and asks them to rename their files.

   for (MediaAccessListIterator mediaAccessIterator = mediaAccessList.begin();
        mediaAccessIterator < mediaAccessList.end();
        ++mediaAccessIterator)
      {
      CMediaAccess *mediaAccess = *mediaAccessIterator;
      int retVal = mediaAccess->renameBinMedia(oldBinPath, newBinPath);
      if (retVal != 0)
         {
         static char const *func = "CMediaInterface::renameClipMedia";
         TRACE_0(errout << func << " failed" << endl;
                 errout << "    " << "bin path: " << newBinPath << endl;);
         return CLIP_ERROR_FILE_RENAME_FAILED;
         }
      }

   return 0;
}


//////////////////////////////////////////////////////////////////////

CMediaAccess* CMediaInterface::makeMediaAccess(EMediaType mediaType,
                                               const string& mediaIdentifier,
                                               const string& historyDirectory,
                                               const CMediaFormat& mediaFormat)
{
   if (!isMediaTypeImageFile(mediaType))
   {
      return NULL;
   }

   CMediaAccess* mediaAccess;

   // Image File types accessed via CImageFileIO
   CImageFileMediaAccess *imageFileMediaAccess
               = new CImageFileMediaAccess(mediaIdentifier,
                                           historyDirectory,
                                           mediaFormat,
                                           mediaType);
   int retVal = imageFileMediaAccess->initialize(mediaFormat);
   if (retVal == 0)
   {
      mediaAccess = imageFileMediaAccess;
   }
   else
   {
      // Initialization of CImageFileMediaAccess failed.  Possibly
      // a problem with the Image File header

      // Register an error code & message
      theError.set(retVal, "Could not initialize image file access");

      delete imageFileMediaAccess;
      mediaAccess = 0;
   }

   return mediaAccess;
}

string CMediaInterface:: adjustMediaIdentifier(EMediaType mediaType,
                                               const string& mediaIdentifier,
                                               const CMediaFormat& mediaFormat,
                                               const CTimecode& frame0Timecode,
                                               const string &relMediaPath)
{
   string newMediaIdentifier;

   if (!isMediaTypeImageFile(mediaType))
   {
      return newMediaIdentifier;
   }

   newMediaIdentifier = CImageFileMediaAccess::adjustMediaIdentifier(mediaIdentifier, frame0Timecode);

   return newMediaIdentifier;
}

//////////////////////////////////////////////////////////////////////

void CMediaInterface::dump(MTIostringstream &str)
{
}

//////////////////////////////////////////////////////////////////////

CMediaAccess* CMediaInterface::
registerMediaAccess(EMediaType mediaType,
                    const string &mediaIdentifier,
                    const string &historyDirectory,
                    const CMediaFormat& mediaFormat)
{
   MediaAccessList resultList;
   CMediaAccess* mediaAccess;

   // Check if media access object has been registered already
   if (queryMediaAccessList(mediaType, mediaIdentifier, resultList) > 0)
      {
      // Media access object has already been registered,
      // so just return a pointer to it
      mediaAccess = *(resultList.begin());
      }
   else
      {
      // Media Access instance doesn't exist, so make a new
      // one of the appropriate type
      mediaAccess = makeMediaAccess(mediaType,
                                    mediaIdentifier,
                                    historyDirectory,
                                    mediaFormat);
      if (mediaAccess)
         {
         // Add to mediaAccessList
         mediaAccessList.push_back(mediaAccess);
         }
      }

   if (mediaAccess != 0)
      mediaAccess->incrRefCount();  // increment reference count

   return mediaAccess;
}

int CMediaInterface::unregisterMediaAccess(CMediaAccess *mediaAccess)
{
   if (mediaAccess == 0)
      return 0;   // NULL pointer, so there is nothing to do

   // Iterate through the media access list, searching for a match
   // with the caller's CMediaAccess instance pointer
   for (MediaAccessListIterator mediaAccessIterator = mediaAccessList.begin();
        mediaAccessIterator < mediaAccessList.end();
        ++mediaAccessIterator)
      {
      if (mediaAccess == *mediaAccessIterator)
         {
         // Check reference count for this media access instance,
         // remove from media access list only when ref count is zero
         if (mediaAccess->decrRefCount() == 0)
            {
            // Remove the list entry for the caller's CMediaAccess instance
            mediaAccessList.erase(mediaAccessIterator);

            // Delete the CMediaAccess instance so that corresponding
            // media storage (e.g., audio file) is properly closed
            // NB: I think this is safe, but I don't know why I never
            //     deleted the media access object instance
            delete mediaAccess;
            }
         return 0;   // Return success
         }
      }

   // Could not find the CMediaAccess instance pointer in the Media Access
   // list, so return an error code
   return -1;
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//
// CDiskSchemeList Class Implementation
//
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDiskSchemeList::CDiskSchemeList()
{
}

CDiskSchemeList::~CDiskSchemeList()
{
}

//////////////////////////////////////////////////////////////////////
// Read/Write Disk Scheme ini file
//////////////////////////////////////////////////////////////////////

int CDiskSchemeList::ReadDiskSchemeFile()
{
   int retVal;

   // Clear whatever is currently in this disk scheme list
   activeDiskSchemes.clear();
   diskSchemeList.clear();

// DISK SCHEMES NO LONGER USED
//   string diskSchemeFileName = CMediaInterface::getDiskSchemeFileName();
//   if (!DoesFileExist(diskSchemeFileName))
//      {
//      ////////////////HACK///ALERT///////////////////////////////////////////
//      // AVOID RECURSION from InitializeAudioMediaAccess() during creation
//      static bool UGLY_HACK_FLAG_TO_AVOID_RECURSION = false;
//      if (UGLY_HACK_FLAG_TO_AVOID_RECURSION)
//         return CLIP_ERROR_DISK_SCHEME_ALREADY_EXISTS; // Haha, well ALMOST!
//      UGLY_HACK_FLAG_TO_AVOID_RECURSION = true;
//      ////////////////HACK///ALERT///////////////////////////////////////////
//
//      retVal = makeDefaultDiskSchemeFile(diskSchemeFileName);
//
//      ////////////////HACK///ALERT///////////////////////////////////////////
//      UGLY_HACK_FLAG_TO_AVOID_RECURSION = false;
//      ////////////////HACK///ALERT///////////////////////////////////////////
//
//      if (retVal != 0)
//         return 0;   // ERROR
//      }
//
//   // Open the disk scheme file
//   CIniFile *iniFile = CreateIniFile(diskSchemeFileName);
//   if (iniFile == 0)
//      {
//      TRACE_0(errout << "ERROR: CDiskSchemeList::ReadDiskSchemeFile"
//                     << endl;
//              errout << "  Could not open Disk Scheme file "
//                     << diskSchemeFileName << endl);
//      return CLIP_ERROR_DISK_SCHEME_FILE_OPEN_FAILED;
//      }
//
//   // Read the Disk Schemes from the ini file
//   // Read all of the section names in the Disk Scheme ini file
//   StringList sectionNames;
//   iniFile->ReadSections(&sectionNames);
//
//   // Iterate over all of the sections in the ini file, looking for
//   // sections that are actually Disk Schemes
//   CDiskScheme tmpScheme;
//   for (unsigned i = 0; i < sectionNames.size(); i++)
//      {
//      // Check to see if they match the ini section
//      if (sectionNames[i].find(diskSchemeSectionNamePrefix, 0) == 0)
//         {
//         retVal = tmpScheme.ReadIni(iniFile, sectionNames[i]);
//         if (retVal != 0)
//            return retVal;   // ERROR
//
//         // add disk scheme to disk scheme list
//         diskSchemeList.push_back(tmpScheme);
//         }
//      }
//
//   // Read the active disk scheme list
//   iniFile->ReadStringList(activeDiskSchemeSectionName,  activeDiskSchemeKey,
//                           &activeDiskSchemes, 0);
//
//   iniFile->FileName = "";
//   DeleteIniFile(iniFile);

   return 0;
}

int CDiskSchemeList::makeDefaultDiskSchemeFile(const string& diskSchemeFileName)
{
   int retVal;

   // Create a default disk scheme file based on the raw disk names
   //
   // For each raw disk name obtained from the CMediaInterface,
   // create a disk scheme with the same name and with the
   // MainVideoMediaLocation set to the raw disk name
   //
   // All disk schemes are included in the active disk scheme list

   CMediaInterface mediaInterface;
   CMediaInterface::RawDiskInfoList rawDiskInfoList;

   retVal = mediaInterface.queryRawDiskInfo(rawDiskInfoList);
   if (retVal != 0)
      return retVal;

   CDiskSchemeList tmpDiskSchemeList;
   CDiskScheme diskScheme;
   // Iterate over the raw disk info list
   for (unsigned int i = 0; i < rawDiskInfoList.size(); ++i)
      {
      diskScheme.Init();  // clear previous contents
      diskScheme.name = "ds-" + rawDiskInfoList[i].mediaIdentifier;
      diskScheme.mainVideoMediaLocation = rawDiskInfoList[i].mediaIdentifier;

      tmpDiskSchemeList.diskSchemeList.push_back(diskScheme);
      tmpDiskSchemeList.activeDiskSchemes.push_back(diskScheme.name);
      }

   retVal = tmpDiskSchemeList.WriteDiskSchemeFile();
   if (retVal != 0)
      return 0;

   return 0;
}

int CDiskSchemeList::WriteDiskSchemeFile()
{
// DISK SCHEMES NO LONGER USED
//   int retVal;
//
//   string diskSchemeFileName = CMediaInterface::getDiskSchemeFileName();
//
//   // Open the disk scheme file
//   CIniFile *iniFile = CreateIniFile(diskSchemeFileName);
//   if (iniFile == 0)
//      {
//      TRACE_0(errout << "ERROR: CDiskSchemeList::WriteDiskSchemeFile"
//                     << endl;
//              errout << "  Could not open Disk Scheme file "
//                     << diskSchemeFileName << endl);
//      return CLIP_ERROR_DISK_SCHEME_FILE_OPEN_FAILED;
//      }
//
//   // Write the active disk scheme list
//   iniFile->WriteStringList(activeDiskSchemeSectionName,  activeDiskSchemeKey,
//                            &activeDiskSchemes);
//
//   // TBD Clear the existing Disk Schemes
//
//   // Write the Disk Schemes to the ini file
//   string sectionName;
//   for (unsigned i = 0; i < diskSchemeList.size(); i++)
//      {
//      sectionName = makeSectionName(i);
//      retVal = diskSchemeList[i].WriteIni(iniFile, sectionName);
//      if (retVal != 0)
//         return retVal;   // ERROR
//      }
//
//   // Deleting the ini file forces it to be written
//   if (!DeleteIniFile(iniFile))
//      {
//      TRACE_0(errout << "ERROR: CDiskSchemeList::WriteDiskSchemeFile"
//                     << endl;
//              errout << "  Could not write Disk Scheme file "
//                     << diskSchemeFileName << endl);
//      return CLIP_ERROR_DISK_SCHEME_FILE_WRITE_FAILED;
//      }

   return 0;
}

string CDiskSchemeList::makeSectionName(int sectionNumber)
{
   MTIostringstream ostr;

   ostr << diskSchemeSectionNamePrefix << sectionNumber;

   return (ostr.str());
}

//////////////////////////////////////////////////////////////////////
// Access Functions
//////////////////////////////////////////////////////////////////////

StringList CDiskSchemeList::GetActiveDiskSchemeNames()
{
   // Returns a copy of the Active Disk Scheme name list
   return activeDiskSchemes;
}


CDiskScheme CDiskSchemeList::Find(const string& name)
{
// Searches for the Disk Scheme with a name that matches the caller's
// name.  If the Disk Scheme was found, then a copy of the Disk Scheme
// is returned.  If the Disk Scheme was not found then an empty Disk Scheme
// was returned.  To determine success or failure, the caller must check
// the name string of the returned Disk Scheme.  If the name string is empty,
// then the search failed.

   for (unsigned int i = 0; i < diskSchemeList.size(); ++i)
      {
      if (diskSchemeList[i].name == name)
         return diskSchemeList[i];
      }

   // If we reached this far, then the Disk Scheme was not found.
   // so return an empty one
   CDiskScheme tmpDiskScheme;
   tmpDiskScheme.Init();
   return tmpDiskScheme;
}

//////////////////////////////////////////////////////////////////////
// Modifier Functions
//////////////////////////////////////////////////////////////////////

void CDiskSchemeList::Add(const CDiskScheme& newScheme)
{
// Searches for the Disk Scheme with a name that matches the caller's
// name.  If the Disk Scheme was found, then replace it with the new
// scheme.  If the Disk Scheme was not found then add the new scheme
// to the end of the list. Finally, activate the scheme. Usage note:
// to actually add a disk scheme permanently, you need to read the
// file, call this method, then write the file.

   bool foundIt = false;
   unsigned int i;

   for (i = 0; i < diskSchemeList.size(); ++i)
      {
      if (diskSchemeList[i].name == newScheme.name)
         {
         // found it, now replace it
         diskSchemeList[i] = newScheme;
         foundIt = true;
         break;
         }
      }
   if (foundIt)
      {
      // Found it and replaced it, now activate it if not already active
      foundIt = false;
      for (i = 0; i < activeDiskSchemes.size(); ++i)
         {
         if (activeDiskSchemes[i] == newScheme.name)
            {
            foundIt = true;
            break;
            }
         }
      if (!foundIt)
         {
         activeDiskSchemes.push_back(newScheme.name);
         }
      }
   else
      {
      // Didn't find it, so add it
      diskSchemeList.push_back(newScheme);
      activeDiskSchemes.push_back(newScheme.name);
      }
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//
// CDiskScheme Class Implementation
//
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDiskScheme::CDiskScheme()
{
}

CDiskScheme::CDiskScheme(const CDiskScheme &src)
 : name(src.name),
   mainVideoMediaLocation(src.mainVideoMediaLocation),
   mainVideoMediaRepositoryType(src.mainVideoMediaRepositoryType),
   proxyVideoMediaLocation(src.proxyVideoMediaLocation),
   proxyVideoMediaRepositoryType(src.proxyVideoMediaRepositoryType),
   audioMediaDirectory(src.audioMediaDirectory)
{
   // Copy constructor
}

CDiskScheme::~CDiskScheme()
{
}

//////////////////////////////////////////////////////////////////////
// Operators
//////////////////////////////////////////////////////////////////////

// Assignment operator
CDiskScheme& CDiskScheme::operator=(const CDiskScheme &src)
{
   // REMOVE THIS METHOD

   if (this == &src)
      return *this;  // avoid self-copy

   Init();  // Clear current contents of this Disk Scheme

   name = src.name;
   mainVideoMediaLocation = src.mainVideoMediaLocation;
   mainVideoMediaRepositoryType = src.mainVideoMediaRepositoryType;
   proxyVideoMediaLocation = src.proxyVideoMediaLocation;
   proxyVideoMediaRepositoryType = src.proxyVideoMediaRepositoryType;
   audioMediaDirectory = src.audioMediaDirectory;

   return *this;   // return reference to this
}

void CDiskScheme::Init()
{
   // REMOVE THIS METHOD
}

//////////////////////////////////////////////////////////////////////
// Read/Write Disk Scheme ini file

int CDiskScheme::ReadIni(CIniFile *iniFile, const string& sectionName)
{
   // REMOVE THIS METHOD

   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

int CDiskScheme::WriteIni(CIniFile *iniFile, const string& sectionName)
{
   // REMOVE THIS METHOD

   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}
