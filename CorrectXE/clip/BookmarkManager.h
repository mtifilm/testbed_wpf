#ifndef _BOOKMARK_MANAGER_H_
#define _BOOKMARK_MANAGER_H_

#include "cliplibint.h"
#include "ClipSharedPtr.h"
#include "BookmarkInfo.h"
#include "timecode.h"
#include "PropX.h"

#include <string>
using std::string;

class CClip;

class MTI_CLIPLIB_API BookmarkManager
{
public:

   void SelectBookmarkAt(int frameIndex);
   int GetSelectedBookmarkFrameIndex();
   void SetCurrentTimecode(CTimecode timecode);
   CTimecode GetCurrentTimecode();
   void SetCurrentClipInfo(
                 const string newMasterClipFolderPath,
                 ClipSharedPtr &currentClip,
                 int videoProxyIndex,
                 int videoFramingIndex);

   int GetBookmarkInfo(int frameIndex, BookmarkInfo &bookmarkInfo);
   int SaveBookmarkInfo(int frameIndex, const BookmarkInfo &bookmarkInfo);

   int AddBookmark(int frameIndex);
   int AddBookmark(const BookmarkInfo &bookmarkInfo);
   int DeleteBookmark(int frameIndex);

   bool DoesFrameHaveBookmarkInfo(int frameIndex);
   bool DoAnyFramesHaveBookmarkInfo();
   bool IsCurrentTimecodeInSelectedBookmark();

   int GetPreviousBookmarkFrameIndex(int frameIndex);
   int GetNextBookmarkFrameIndex(int frameIndex);

   BookmarkInfo *CreateBookmarkInfo(StringList headers, StringList row);

   static CBHook BookmarkAdded;
   static CBHook BookmarkSelectionChange;
   static CBHook SelectedBookmarkRangeEnteredOrExited;

private:
   void MaybeNotifySelectedBookmarkRangeEnteredOrExited();

   static CTimecode CurrentTimecode;
   static int SelectedBookmarkFrameIndex;
   static string MasterClipFolderPath;
   static string BookmarkIniFilePath;
   static int ClipTotalFrameCount;
   static CTimecode ClipTimecodeAtFrame0;
   static BookmarkInfo BookmarkInfoForDefaults;
   static bool oldInSelectedBookmarkRangeFlag;
};

// Return values for AddBookmark(bookmarkInfo)
#define BOOKMARK_ADDED_SUCCESSFULLY  0
#define BOOKMARKINFO_NOT_VALID      -1
#define BOOKMARKINFO_OUT_OF_RANGE   -2

#endif

