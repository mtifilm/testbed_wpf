// Clip3VideoTrack.cpp: implementation of the CVideoFrameList class.
//
//////////////////////////////////////////////////////////////////////

//#define _CLIP_3_VIDEO_TRACK_DATA_CPP #ifdef PRIVATE_STATIC

#include "Clip3VideoTrack.h"
#include "BinManager.h"
#include "Clip3VideoTrackFile.h"
#include "Clip3.h"
#include "err_clip.h"
#include "ImageFormat3.h"
#include "IniFile.h"
#include "MediaAccess.h"
#include "MediaInterface.h"
#include "MTIstringstream.h"

//////////////////////////////////////////////////////////////////////
// Static Member Variables
//////////////////////////////////////////////////////////////////////

string CVideoFramingInfoList::videoFramingInfoListSection = "VideoFramingList";
string CVideoFramingInfoList::videoFramingInfoSectionName = "VideoFramingInfo";
string CVideoFramingInfoList::mainFrameListKey = "MainFrameList";
string CVideoFramingInfoList::lockedFrameListKey = "LockedFrameList";

string CVideoFrameList::videoTrackSectionName = "VideoFrameList";

//////////////////////////////////////////////////////////////////////

#ifdef PRIVATE_STATIC
//////////////////////////////////////////////////////////////////////
// The following variables used to be static class members.
// I switched the to namespaced globals for compatability with
// visual c++.  -Aaron Geman  7/9/04
//////////////////////////////////////////////////////////////////////

namespace Clip3VideoTrackData {
   string videoProxySection = "VideoProxy";

   string videoFramingInfoListSection = "VideoFramingList";
   string videoFramingInfoSectionName = "VideoFramingInfo";
   string mainFrameListKey = "MainFrameList";
   string lockedFrameListKey = "LockedFrameList";

   string videoTrackSectionName = "VideoFrameList";
};
using namespace Clip3VideoTrackData;
#endif
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                               ################################
// ###    CVideoFramingInfo Class     ###############################
// ####                               ################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Construction/Destruction
//////////////////////////////////////////////////////////////////////

CVideoFramingInfo::CVideoFramingInfo(int newTrackNumber,
                                   int newFramingType)
: CFramingInfo(TRACK_TYPE_VIDEO, newTrackNumber, newFramingType)
{
}

CVideoFramingInfo::~CVideoFramingInfo()
{
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                                   ############################
// ###    CVideoFramingInfoList Class     ###########################
// ####                                   ############################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CVideoFramingInfoList::CVideoFramingInfoList()
: mainFrameListIndex(FRAMING_SELECT_VIDEO)
, lockedFrameListIndex(-1)
{
}

CVideoFramingInfoList::~CVideoFramingInfoList()
{
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

CVideoFramingInfo*
CVideoFramingInfoList::getVideoFramingInfo(int framingIndex) const
{
   return static_cast<CVideoFramingInfo*>(getBaseFramingInfo(framingIndex));
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////


CFramingInfo*
CVideoFramingInfoList::createDerivedFramingInfo(int newFramingNumber,
                                                 int newFramingType)
{
   // Create new instance of CVideoFramingInfo
   CFramingInfo* framingInfo = new CVideoFramingInfo(newFramingNumber,
                                                       newFramingType);

   return framingInfo;
}

//////////////////////////////////////////////////////////////////////
// Read/Write Video Track List Section of Clip File
//////////////////////////////////////////////////////////////////////

int CVideoFramingInfoList::readVideoFramingInfoListSection(CIniFile *iniFile)
{
   int retVal;

   if (!iniFile->SectionExists(videoFramingInfoListSection))
      return 0;  // Video Framing Info List section does not exist, so
                 // assume this clip has no Video

   retVal = readFramingInfoListSection(iniFile, videoFramingInfoListSection,
                                       videoFramingInfoSectionName);
   if (retVal != 0)
      return retVal; // ERROR: Could not read either the FramingInfoList
                     //        section or a Framing Info Section

   // Read the "main frame list" index for version clip hack
   mainFrameListIndex = iniFile->ReadInteger(videoFramingInfoListSection,
                                             mainFrameListKey,
                                             FRAMING_SELECT_VIDEO);

   // Read the "locked frame list" index for version clip hack
   lockedFrameListIndex = iniFile->ReadInteger(videoFramingInfoListSection,
                                               lockedFrameListKey,
                                               -1);

   return 0;   // Success

} // readVideoFramingInfoListSection

int CVideoFramingInfoList::writeVideoFramingInfoListSection(CIniFile *iniFile)
{
   int retVal;

   if (getFramingCount() == 0)
      return 0; // Framing Info List is empty, so nothing to write

   retVal = writeFramingInfoListSection(iniFile, videoFramingInfoListSection,
                                        videoFramingInfoSectionName);
   if (retVal != 0)
      return retVal; // ERROR: Could not read either the FramingInfoList
                     //        section or a Framing Info Section

   // Write the "main frame list" index for version clip hack - ignore errors
   iniFile->WriteInteger(videoFramingInfoListSection,
                         mainFrameListKey, mainFrameListIndex);

   // Write the "locked frame list" index for version clip hack - ignore errors
   iniFile->WriteInteger(videoFramingInfoListSection,
                         lockedFrameListKey, lockedFrameListIndex);

   return 0;   // Success

} // writeVideoFramingInfoListSection

//////////////////////////////////////////////////////////////////////
// VERSION CLIP HACK - Read/Write Readonly Track Number
//////////////////////////////////////////////////////////////////////

int CVideoFramingInfoList::getMainFrameListIndex()
{
   // BE SURE TO CALL readVideoFramingInfoListSection() FIRST!
   return mainFrameListIndex;
}

void CVideoFramingInfoList::setAndWriteMainFrameListIndex(CIniFile *iniFile,
                                                  int newMainFrameListIndex)
{
   mainFrameListIndex = newMainFrameListIndex;

   // Force a write to make sure it gets done
   iniFile->WriteInteger(videoFramingInfoListSection,
                         mainFrameListKey, mainFrameListIndex);
}

int CVideoFramingInfoList::getLockedFrameListIndex()
{
   // BE SURE TO CALL readVideoFramingInfoListSection() FIRST!
   return lockedFrameListIndex;
}

void CVideoFramingInfoList::setAndWriteLockedFrameListIndex(CIniFile *iniFile,
                                                  int newLockedFrameListIndex)
{
   lockedFrameListIndex = newLockedFrameListIndex;

   // Force a write to make sure it gets done
   iniFile->WriteInteger(videoFramingInfoListSection,
                         lockedFrameListKey, lockedFrameListIndex);
}
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                             ##################################
// ###    CVideoFrameList Class     #################################
// ####                             ##################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CVideoFrameList::CVideoFrameList(CVideoFramingInfo *newFramingInfo,
                                   CVideoProxy *newVideoProxy)
: CFrameList(newFramingInfo, newVideoProxy),
  maxPaddedFieldByteCount(0)
{
}

CVideoFrameList::CVideoFrameList(CVideoFramingInfo *newFramingInfo,
                                   CVideoProxy *newVideoProxy,
                                   MTI_UINT32 reserveFrameCount)
: CFrameList(newFramingInfo, newVideoProxy, reserveFrameCount),
  maxPaddedFieldByteCount(0)
{
}

CVideoFrameList::~CVideoFrameList()
{
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

CFrame* CVideoFrameList::createFrame(int fieldCount,
                                       CMediaFormat* mediaFormat,
                                       const CTimecode& timecode)
{
   if (getClipVer() == CLIP_VERSION_5L)
      {
      CAutoErrorReporter errorReporter("CVideoFrameList::createFrame");
      errorReporter.errorCode = CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;
      errorReporter.msg << "Function not supported by clip version 5";
      return 0;
      }

   // Create an instance of Video Frame, set its parameters and add it
   // to this Frame List

   CVideoFrame *frame = new CVideoFrame(fieldCount);

   frame->setImageFormat(static_cast<CImageFormat*>(mediaFormat));
   frame->setTimecode(timecode);

   addFrame(frame);

   return frame;
}

//////////////////////////////////////////////////////////////////////
// Accessor Functions
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    getFrame
//
// Description: Get a pointer to a particular frame in a clip given the
//              frame number.
//
// Arguments:   int frameNumber  Number of the frame to get.  First frame is 0
//
// Returns:     Pointer to a CVideoFrame object for the specified frame number
//              If the frame number is not valid, returns a NULL pointer
//
//------------------------------------------------------------------------
CVideoFrame* CVideoFrameList::getFrame(int frameNumber)
{
   return static_cast<CVideoFrame*>(getBaseFrame(frameNumber));
}

const CImageFormat* CVideoFrameList::getImageFormat() const
{
   return getVideoProxy()->getImageFormat();
}

//------------------------------------------------------------------------
//
// Function:    makeFrameListFileName
//
// Description: Constructs the file name for a Video Frame List File given
//               the clip name, proxy number and framing number.
//              Filename does not include the extension
//
//              Example:  "clipname_vp0_f1"
//                 where clipname is the clip's name, 0 is the proxy number
//                 and 1 is the framing number
//
// Arguments:   None
//
// Returns:     string containing constructed file name
//
//------------------------------------------------------------------------
string CVideoFrameList::makeFrameListFileName() const
{
   MTIostringstream ostr;

   // Compose Frame List File Name

   // Clip Name
   CBinManager binMgr;
   auto clip = getVideoProxy()->getParentClip();
   ostr << binMgr.makeClipFileName(clip->getBinPath(), clip->getClipName());

   // Video Proxy Number
   ostr << "_vp" << getVideoProxy()->getProxyNumber();

   // Framing Number
   ostr << "_f" << framingInfo->getFramingNumber();

   return ostr.str();
}

//------------------------------------------------------------------------
//
// Function:    makeNewFrameListFileName
//
// Description: Primarily useful during renaming a clip.
//              Constructs the file name for a Video Frame List File given
//              the clip name, proxy number and framing number.
//              Filename does not include the extension
//
//              Example:  "clipname_vp0_f1"
//                 where clipname is the clip's name, 0 is the proxy number
//                 and 1 is the framing number
//
// Arguments: string newClipName  the new clip name to form the frame
// list file name
//
// Returns:     string containing constructed file name
//
//------------------------------------------------------------------------
string CVideoFrameList::makeNewFrameListFileName(string newClipName) const
{
   MTIostringstream ostr;

   // Compose Frame List File Name

   auto clip = getVideoProxy()->getParentClip();
   ostr << AddDirSeparator(clip->getClipPathName()) << newClipName;

   // Video Proxy Number
   ostr << "_vp" << getVideoProxy()->getProxyNumber();

   // Framing Number
   ostr << "_f" << framingInfo->getFramingNumber();

   return ostr.str();
}

//////////////////////////////////////////////////////////////////////
// Mutator Functions
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:   getMaxPaddedFieldByteCount
//
// Description: Get the maximum padded byte count of all fields
//             in all frames in this clip.  This value is evaluated
//             on the first call to this function and then saved in
//             the clip object.  Recalculation can be forced calling
//             with forceRecalc argument set to true.
//
// Arguments:  bool forceRecalc  Force re-evaluation of maxPaddedFieldByteCount
//
// Returns:    Maximum padded byte count of all fields in clip
//
//------------------------------------------------------------------------
MTI_UINT32 CVideoFrameList::getMaxPaddedFieldByteCount(bool forceRecalc)
{
   MTI_UINT32 fieldMaxByteCount;

   // If maxPaddedFieldByteCount is zero, its value has not been
   // determined yet.
   if (maxPaddedFieldByteCount == 0 || forceRecalc)
      {
      maxPaddedFieldByteCount = 0;
      // Iterate through frame list, get max byte count for each frame in list
      int frameNumber;
		int frameCount = getTotalFrameCount();
		if (frameCount < 1)
		{
			return -1;   // Can't read the track file!
      }

      EClipVersion clipVer = getClipVer();

      if (clipVer == CLIP_VERSION_3)
         {
         for (frameNumber = 0; frameNumber < frameCount; ++frameNumber)
            {
            CVideoFrame* frame = getFrame(frameNumber);
            if (frame)
               {
               fieldMaxByteCount = frame->getMaxPaddedFieldByteCount();
               if (fieldMaxByteCount > maxPaddedFieldByteCount)
                  maxPaddedFieldByteCount = fieldMaxByteCount;
               }
            }

//         //********* EMBEDDED ALPHA HACK-O-RAMA (aka CINTEL_HACK )*************
//         // Alpha bits that were embedded in unused bits within the
//         // normal RGB pixel data are sucked out on the fly and appended
//         // to the RGB pixel data, so the max byte count increases by
//         // (number of pixels)/8 , or one alpha bit per pixel.
//         //
//         EAlphaMatteType alphaType = getImageFormat()->getAlphaMatteType();
//
//         if (alphaType == IF_ALPHA_MATTE_1_BIT_CINTEL_HACK)
//            {
//            maxPaddedFieldByteCount += getImageFormat()->getAlphaBytesPerField();
//            }
//         // *******************************************************************

         }
      else if (clipVer == CLIP_VERSION_5L)
         {
         C5MediaTrackHandle trackHandle = parentTrack->GetTrackHandle();
         const CMediaFormat *mediaFormat = trackHandle.GetMediaFormat();
         if (mediaFormat == 0)
            return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

         int fieldCount = mediaFormat->getFieldCount();
         maxPaddedFieldByteCount = trackHandle.getMaxPaddedFieldByteCount(0,
                                                      frameCount * fieldCount);
         }
      else
         maxPaddedFieldByteCount = 0;
      }

   // Stupid hack to handle clips that are marked as having an alpha matte,
   // but the alpha matte was stripped off by another tool.
   maxPaddedFieldByteCount = std::max<MTI_UINT32>(
                                    maxPaddedFieldByteCount,
                                    getImageFormat()->getBytesPerField());

   return maxPaddedFieldByteCount;
}

CVideoProxy* CVideoFrameList::getVideoProxy() const
{
   return static_cast<CVideoProxy*>(parentTrack);
}

bool CVideoFrameList::isLocked()
{
   auto clip = getParentTrack()->getParentClip();
   int framingSelect = (getFramingType() == FRAMING_TYPE_FILM)?
                         FRAMING_SELECT_FILM : FRAMING_SELECT_VIDEO;

   if (framingSelect == clip->getLockedFrameListIndex())
      return true;

   return false;
}

int CVideoFrameList::preLoadFiles()
{
   int retVal;

   if (getClipVer() == CLIP_VERSION_5L)
      return 0;   // No action for clip 5

   // The clip library does not load the frame list or field list from their
   // files until they are each accessed for the first time.  This causes
   // problems in the VTR emulator.  This function forces the frame list
   // and field list files to be read.  If the file has already been read,
   // this function does nothing

   // Force the read of the frame list file
   retVal = CFrameList::preLoadFiles();
   if (retVal != 0)
      return retVal;

   // Pre-calculate and cache some frame list statistics
   getMaxPaddedFieldByteCount();

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Virtual Copy
//////////////////////////////////////////////////////////////////////

int CVideoFrameList::virtualCopy(CVideoFrameList &srcFrameList,
                                 int srcIndex, int dstIndex, int frameCount,
                                 CMediaStorageList &dstMediaStorageList)
{
   int retVal;

   if (getClipVer() == CLIP_VERSION_5L)
      {
      CAutoErrorReporter errorReporter("CVideoFrameList::virtualCopy");
      errorReporter.errorCode = CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;
      errorReporter.msg << "Function not supported by clip version 5";
      return errorReporter.errorCode;
      }

   // Virtual copy frames
   // Note: assumes valid source and destination frame indices and sufficient
   //       source and destination frames to cover frameCount.  Doesn't check.
   for (int i = 0; i < frameCount; ++i)
      {
      CVideoFrame *dstFrame = getFrame(dstIndex + i);
      CVideoFrame *srcFrame = srcFrameList.getFrame(srcIndex + i);
      retVal = dstFrame->virtualCopy(*srcFrame, dstMediaStorageList);
      if (retVal != 0)
         return retVal;
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Read/Write Media Data Functions
//////////////////////////////////////////////////////////////////////

int CVideoFrameList::readFrameListFile()
{
   int retVal;

   // Make the Diff File name
   CBinManager binMgr;
   auto clip = getVideoProxy()->getParentClip();
   string diffFileName = binMgr.makeClipFileName(clip->getBinPath(),
                                                 clip->getClipName());
   diffFileName += ".diff";  // This shouldn't be hard-coded

   string cadenceFileName =
      binMgr.makeRepairedCadenceFilePath(clip->getBinPath(),
                                         clip->getClipName());

   const CImageFormat *imageFormat = getVideoProxy()->getImageFormat();
   EImageFormatType imageFormatType = imageFormat->getImageFormatType();

   int framingType = getFramingType();

   if (framingType == FRAMING_TYPE_FIELDS_1_2
       || framingType == FRAMING_TYPE_FIELD_1_ONLY
       || framingType == FRAMING_TYPE_FIELD_2_ONLY)
      {
      // The Video Field frame lists are never stored as files, rather
      // the frame lists are synthesized when they are needed from
      // the Video-Frames frame list
      bool do525FieldOrder = CImageInfo::queryIsFormat525(imageFormatType);
      retVal = createVideoFields_FrameList(do525FieldOrder);
      if (retVal != 0)
         return retVal;
      }
   else if (framingType == FRAMING_TYPE_FILM
            && isFileNewerThanFrameListFile(diffFileName))
      {
      // This track is film-frames and the "Diff File" is
      // newer than the .trk file, so construct a new
      // frame list based on the Diff file
      TRACE_3(errout << "CVideoFrameList::readFrameListFile: "
                     << "about to create film-frames frame list from Diff file"
                     << endl << diffFileName);
      EPulldown nominalPulldown
                           = CImageInfo::queryNominalPulldown(imageFormatType);

      retVal = createFrameListFromDiffFile(diffFileName, nominalPulldown);
      if (retVal != 0)
         {
         TRACE_0(errout << "ERROR: CVideoFrameList::readFrameListFile: " << endl
                    << " could not create film-frames frame list from Diff file"
                    << endl << "  " << diffFileName << endl
                    << "  Return code: " << retVal);

         return retVal;  // ERROR: Could not create a frame list from the
                         //        Diff File
         }

      // Write out the new frame list into the track file
      retVal = writeFrameListFile();
      if (retVal != 0)
         {
         TRACE_0(errout << "ERROR: CVideoFrameList::readFrameListFile: " << endl
                        << "       could not write new film-frames frame list file"
                        << endl
                        << "       Return code: " << retVal);

         return retVal;  // ERROR: Failed to write the frame list file
         }

      // Update the main clip file
      CVideoProxy *videoProxy = getVideoProxy();
      auto clip = videoProxy->getParentClip();
      retVal = clip->writeFile();
      if (retVal != 0)
         {
         TRACE_0(errout << "ERROR: CVideoFrameList::readFrameListFile: " << endl
                        << "       could not update the clip file with new film-frames info" << endl
                        << "       Return code: " << retVal);

         return retVal;  // ERROR: Failed to write the clip file
         }

      }
   else if (framingType == FRAMING_TYPE_CADENCE_REPAIR)
      {
      // This requested track is the cadence repair track.
      // Construct a new frame list based on the cadence file

      bool bPerformedCadenceRepair = false;
      if (clip->getCadenceRepairFlag() == true)
       {
        if (FileDateCompare(cadenceFileName, diffFileName) > 0)
         {
          // date of the .crt file is good.  Generate the list
           TRACE_3(errout << "CVideoFrameList::readFrameListFile: "
                     << "about to create video-frames frame list from cadence file"
                     << endl << diffFileName);

         // The Cadence Field frame lists are never stored as files, rather
         // the frame lists are synthesized when they are needed from
         // the Video-Frames frame list
         bool do525FieldOrder = CImageInfo::queryIsFormat525(imageFormatType);
         retVal = createFrameListFromCadenceFile(cadenceFileName, do525FieldOrder);
         if (retVal != 0)
            {
            TRACE_0(errout << "ERROR: CVideoFrameList::readFrameListFile: " << endl
                    << " could not create video-frames frame list from cadence file"
                    << endl << "  " << cadenceFileName << endl
                    << "  Return code: " << retVal);

            return retVal;  // ERROR: Could not create a frame list from the
                            //        cadence File
            }
          bPerformedCadenceRepair = true;
         }
        else
         {
          // newer material has been captured.  Disable cadence repair
          TRACE_3 (errout << "CVideoFrameList::readFrameListFile: "
                     << "A recent video capture has cancelled the cadence repair tool"
                     << endl << diffFileName);
         retVal = clip->setCadenceRepairFlag (false);
         if (retVal != 0)
          {
           TRACE_0(errout
		<< "ERROR: CVideoFrameList::readFrameListFile: " << endl
                << "  could not set cadence repair flag to false" << endl
                << "  Return code: " << retVal);
           return retVal;  // ERROR: Could not set the cadence repair flag
          }
         }
       }

     // if we were not successful in generating a list with repaired cadence,
     // return an error
     if (bPerformedCadenceRepair == false)
      {
       // make a copy of the video field list
       retVal = copyVideoFields_FrameList ();
       if (retVal != 0)
          {
          TRACE_0(errout << "ERROR: CVideoFrameList::readFrameListFile: " << endl
                    << " could not copy video-frames frame list"
                    << "  Return code: " << retVal);

          return retVal;  // ERROR: Could not create a frame list from the
                            //        cadence File
          }
        }
      }
   else
      {
      // This is either another framing type or it if Film-Frames but the
      // the diff file isn't newer than the .trk file.  Just read the file.
      CVideoFrameListFileReader frameListFileReader;

      retVal = frameListFileReader.readFrameListFile(*this);
      if (retVal != 0)
         {
         TRACE_0(errout << "ERROR: CVideoFrameList::readFrameListFile: " << endl
                        << "  could not read frame list file" << endl
                        << "  Return code: " << retVal);
         return retVal;  // ERROR: Could not read the frame list file
         }

       if (clip->getCadenceRepairFlag () == true
		&& FileDateCompare(cadenceFileName, diffFileName) <= 0)
        {
         // the time stamp of the cadence file has made the cadence repair
         // decisions void.  Cancel the cadence repair flag.
         retVal = clip->setCadenceRepairFlag (false);
         if (retVal != 0)
          {
           TRACE_0(errout
		<< "ERROR: CVideoFrameList::readFrameListFile: " << endl
                << "  could not set cadence repair flag to false" << endl
                << "  Return code: " << retVal);
           return retVal;  // ERROR: Could not set the cadence repair flag
          }
        }
      }

   return 0;

} // readFrameListFile

int CVideoFrameList::writeFrameListFile()
{
   int retVal;

   int framingType = getFramingType();

   if (framingType == FRAMING_TYPE_FIELDS_1_2
       || framingType == FRAMING_TYPE_FIELD_1_ONLY
       || framingType == FRAMING_TYPE_FIELD_2_ONLY
       || framingType == FRAMING_TYPE_CADENCE_REPAIR)
      {
      // The Video Field frame lists are never stored as files, rather
      // the frame lists are synthesized when they are needed from
      // the Video-Frames frame list.
      return 0;   // just return, nothing to do
      }

   CVideoFrameListFileWriter frameListFileWriter;
   TRACE_3(errout << "(((( writeFrameListFile " << framingType << " ("
                  << std::setbase(16) << std::setfill('0') << std::setw(8)
                  << ((int) this) << ") ");

   retVal = frameListFileWriter.writeFrameListFile(this);
   if (retVal != 0)
      {
      // ERROR: Frame List file write failed
      TRACE_0(errout << "ERROR: CVideoFrameList::writeFrameListFile: " << endl
                     << "  could not write frame list file" << endl
                     << "  Return code: " << retVal);
      return retVal;
      }

   return 0;

} // writeFrameListFile

int CVideoFrameList::renameFrameListFile(string newClipName)
{
   // rename the frame list file within the same directory; that is,
   // change the file name, but not the directory name

   int retVal;

   int framingType = getFramingType();

   if (framingType == FRAMING_TYPE_FIELDS_1_2
       || framingType == FRAMING_TYPE_FIELD_1_ONLY
       || framingType == FRAMING_TYPE_FIELD_2_ONLY
       || framingType == FRAMING_TYPE_CADENCE_REPAIR)
      {
      // The Video Field frame lists are never stored as files, rather
      // the frame lists are synthesized when they are needed from
      // the Video-Frames frame list.
      return 0;   // just return, nothing to do
      }

   string newFrameListFilePath = makeNewFrameListFileName(newClipName)
      + "." + getFrameListFileExtension();
   string oldFrameListFilePath =
      makeFrameListFileName() + "." + getFrameListFileExtension();

   retVal = rename(oldFrameListFilePath.c_str(), newFrameListFilePath.c_str());
   return retVal;
} // renameFrameListFile


//////////////////////////////////////////////////////////////////////
// Read/Write Media Data Functions
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    readMediaAllFieldsQuick
//
// Description: Read the media data from the media storage for the
//              specified frame in the clip.  Media data is read
//              into caller's buffers for all of the frame's visible
//              and invisible fields.
//
//              Media data is read in the same order as the fields
//              appear in the frame.  The read order is not optimized
//              for the media location.
//
// Arguments:  int frameNumber   Number of frame to read.  First frame is 0.
//             MTI_UINT8 *buffer[]   Array of pointers to buffers, one for
//                                   each field in frame
//             int quickReadFlag
//
// Returns:    0 for success, non-zero indicates an error
//
//------------------------------------------------------------------------
int CVideoFrameList::readMediaAllFieldsQuick(int frameNumber,
                                              MTI_UINT8 *buffer[],
                                              int quickReadFlag)
{
   // Get a pointer to the frame of interest
   CVideoFrame* frame = getFrame(frameNumber);

   // If NULL frame pointer was returned, then the frame number is invalid
   if(frame == 0)
      return -1;     // Return error

   // Read the frame's media data and return success/error status
   return(frame->readMediaAllFieldsQuick(buffer, quickReadFlag));
}

//------------------------------------------------------------------------
//
// Function:    readMediaAllFieldsOptimizedQuick
//
// Description: Read the media data from the media storage for the
//              specified frame in the clip.  Media data is read
//              into caller's buffers for all of the frame's visible
//              and invisible fields.
//
//              Media read is optimized by reading from the fields' media
//              storage in ascending media location order and by copying
//              buffers instead of re-reading for fields that share the
//              same media.
//
// Arguments:  int frameNumber   Number of frame to read.  First frame is 0.
//             MTI_UINT8 *buffer[]   Array of pointers to buffers, one for
//                                   each field in frame
//             int quickReadFlag
//
// Returns:    0 for success, non-zero indicates an error
//
//------------------------------------------------------------------------
int CVideoFrameList::readMediaAllFieldsOptimizedQuick(int frameNumber,
                                                       MTI_UINT8 *buffer[],
                                                       int quickReadFlag)
{
   int retVal;
   EClipVersion clipVer = getClipVer();

   if (clipVer == CLIP_VERSION_3)
      {
      if (!isFrameIndexValid(frameNumber))
         {
			initMediaBuffer(buffer);      // fill caller's buffers with something
         return CLIP_ERROR_INVALID_FRAME_NUMBER;
         }

      // Get a pointer to the frame of interest
      CVideoFrame* frame = getFrame(frameNumber);

      // If NULL frame pointer was returned, then the frame number is invalid
      if(frame == 0)
         return -1;     // Return error

      // Read the frame's media data and return success/error status
      retVal = frame->readMediaAllFieldsOptimizedQuick(buffer, quickReadFlag);
      if (retVal != 0)
         return retVal;
      }
   else if (clipVer == CLIP_VERSION_5L)
      {
      // NOTE: for Clip 5, media is read, but does not do quick read
      retVal = parentTrack->GetTrackHandle().ReadMedia(frameNumber, buffer);
      if (retVal != 0)
         return retVal;
      }
   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    readMediaVisibleFieldsQuick
//
// Description: Read the media data from the media storage for the
//              specified frame in the clip.  Media data is read
//              into caller's buffers for the frame's visible  fields only.
//
//              Media data is read in the same order as the fields
//              appear in the frame.  The read order is not optimized
//              for the media location.
//
// Arguments:  int frameNumber   Number of frame to read.  First frame is 0.
//             MTI_UINT8 *buffer[]   Array of pointers to buffers, one for each
//                               field in frame
//             int quickReadFlag
//
// Returns:    0 for success, non-zero indicates an error
//
//------------------------------------------------------------------------
int CVideoFrameList::readMediaVisibleFieldsQuick(int frameNumber,
                                                  MTI_UINT8 *buffer[],
                                                  int quickReadFlag)
{
   if (!isFrameIndexValid(frameNumber))
      {
		initMediaBuffer(buffer);      // fill caller's buffers with something
      return CLIP_ERROR_INVALID_FRAME_NUMBER;
      }

   // Get a pointer to the frame of interest
   CVideoFrame* frame = getFrame(frameNumber);

   // If NULL frame pointer was returned, then the frame number is invalid
   if(frame == 0)
      return -1;     // Return error

   // Read the frame's media data and return success/error status
	return(frame->readMediaVisibleFieldsQuick(buffer, quickReadFlag));
}

//------------------------------------------------------------------------
//
// Function:    readMediaVisibleFieldsOptimizedQuick
//
// Description: Read the media data from the media storage for the
//              specified frame in the clip.  Media data is read
//              into caller's buffers for the frame's visible  fields only.
//
//              Media data is read in the same order as the fields
//              appear in the frame.  The read order is not optimized
//              for the media location.
//
// Arguments:  int frameNumber   Number of frame to read.  First frame is 0.
//             MTI_UINT8 *buffer[]   Array of pointers to buffers, one for each
//                               field in frame
//             int quickReadFlag
//             int frameNumber1   When present, this means read field 0 out
//                                of frameNumber and field 1 out of frameNumber1
//
// Returns:    0 for success, non-zero indicates an error
//
//------------------------------------------------------------------------
int CVideoFrameList::readMediaVisibleFieldsOptimizedQuick(int frameNumber,
                                                           MTI_UINT8 *buffer[],
                                                           int quickReadFlag,
                                                           int frameNumber1)
{
   int retVal;

   retVal = readMediaVisibleFieldsOptimizedQuickMT(frameNumber, buffer,
                                                   quickReadFlag, 0,
                                                   frameNumber1);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CVideoFrameList::readMediaVisibleFieldsOptimizedQuickMT(int frameNumber,
                                                            MTI_UINT8 *buffer[],
                                                            int quickReadFlag,
                                                            int fileHandleIndex,
                                                            int frameNumber1)
{
   int retVal;

   EClipVersion clipVer = getClipVer();

   // If caller's frameNumber1 is -1, then set it to the caller's frameNumber
   // to default to an ordinary read media
   if (frameNumber1 == -1)
     frameNumber1 = frameNumber;

   // Check that the frame numbers are within range
   if (!isFrameIndexValid(frameNumber) ||
       (frameNumber != frameNumber1 && !isFrameIndexValid(frameNumber1)))
      {
      // TBD: proper blacken instead of just setting
		initMediaBuffer(buffer);      // fill caller's buffers with something
      return CLIP_ERROR_INVALID_FRAME_NUMBER;
      }

   if (clipVer == CLIP_VERSION_3)
      {
      if (frameNumber == frameNumber1)
         {
         // Get a pointer to the frame of interest
         CVideoFrame* frame = getFrame(frameNumber);

         // If NULL frame pointer was returned, then the frame number is invalid
         if(frame == 0)
            return -1;     // Return error

         // Read the frame's media data and return success/error status
         return(frame->readMediaVisibleFieldsOptimizedQuickMT(buffer,
                                                              quickReadFlag,
                                                              fileHandleIndex));
         }
      else
         {
         // Get a pointer to the frame of interest
         CVideoFrame* frame0 = getFrame(frameNumber);
         CVideoFrame* frame1 = getFrame(frameNumber1);

         // If NULL frame pointer was returned, then the frame number is invalid
         if(frame0 == 0 || frame1 == 0)
            return -1;     // Return error

         // Read the frame's media data and return success/error status
         int iRet0 = frame0->readMediaVisibleFieldsOptimizedQuickMT(buffer,
                                                              quickReadFlag,
                                                              fileHandleIndex, 0
                                                              );
         int iRet1 = frame1->readMediaVisibleFieldsOptimizedQuickMT(buffer,
                                                             quickReadFlag,
                                                             fileHandleIndex, 1
                                                             );
         if (iRet0)
            return iRet0;
         if (iRet1)
            return iRet1;
         }
      }

   else if (clipVer == CLIP_VERSION_5L)
      {
      if (frameNumber == frameNumber1)
         {
         // Two frame numbers are the same; this is the same as an ordinary
         // read
         retVal = parentTrack->GetTrackHandle().ReadMediaMT(frameNumber, buffer,
                                                            fileHandleIndex);
         if (retVal != 0)
            return retVal;
         }
      else
         {
         // The caller's two frame numbers are different, so assemble
         // the frame from fields from the caller's two arbitrary frames

         // Read both fields before checking status return so we get a
         // opportunity to blacken both fields
         int iRet0 = parentTrack->GetTrackHandle().ReadMediaOneFieldMT(
                                                               frameNumber, 0,
                                                               buffer[0],
                                                               fileHandleIndex);
         int iRet1 = parentTrack->GetTrackHandle().ReadMediaOneFieldMT(
                                                               frameNumber1, 1,
                                                               buffer[1],
                                                               fileHandleIndex);
         if (iRet0)
            return iRet0;
         if (iRet1)
            return iRet1;
         }
      }

   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;  // internal error

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    fetchMediaFramefile
//
// Description: tells the reader to synchronously fetch the frame file into memory.
//
// Arguments:  int frameNumber      Number of frame to read.  First frame is 0.
//
// Returns:    0 for success, non-zero indicates an error
//
//------------------------------------------------------------------------
int CVideoFrameList::fetchMediaFrameFile(int frameNumber)
{
	// Get a pointer to the frame of interest
	CVideoFrame* frame = getFrame(frameNumber);
	if (frame == nullptr)
	{
		return CLIP_ERROR_INVALID_FRAME_NUMBER;
	}

	// Read the frame's media data and return success/error status
	int retVal = frame->fetchMediaFrameFile();
	if (retVal != 0)
	{
		return retVal;
	}

	return 0;
}

//------------------------------------------------------------------------
//
// Function:    readMediaFrameBuffer
//
// Description: Read the media data from the media storage for the
//              specified frame in the clip.  Media data is read
//              into caller's buffers for the frame's visible  fields only.
//
// Arguments:  int frameNumber      Number of frame to read.  First frame is 0.
//             MediaFrameBuffer* &outFrameBuffer    The returned frame buffer.
//
// Returns:    0 for success, non-zero indicates an error
//
//------------------------------------------------------------------------
int CVideoFrameList::readMediaFrameBuffer(int frameNumber, MediaFrameBufferSharedPtr &outFrameBuffer)
{
	CVideoFrame* frame = getFrame(frameNumber);
	if (frame == nullptr)
	{
		return CLIP_ERROR_INVALID_FRAME_NUMBER;
	}

	int retVal = frame->readMediaFrameBuffer(outFrameBuffer);
	if (retVal != 0)
	{
		return retVal;
	}

	return 0;
}

//------------------------------------------------------------------------
//
// Function:    writeMediaFrameBuffer
//
// Description: Write the new media data to the media storage for the specified
//              frame in the clip. Only single-field frames are allowed.
//
// Arguments:   int frameNumber
//                       Number of frame to read.  First frame is 0.
//              const MediaFrameBufferSharedPtr &frameBuffer
//                       The media frame buffer with new data.
//
// Returns:    0 for success, non-zero indicates an error
//
//------------------------------------------------------------------------
int CVideoFrameList::writeMediaFrameBuffer(int frameNumber, const MediaFrameBufferSharedPtr &inFrameBuffer)
{
	if (!isFrameIndexValid(frameNumber))
	{
		return CLIP_ERROR_INVALID_FRAME_NUMBER;
	}

	CVideoFrame* videoFrame = getFrame(frameNumber);
	if (videoFrame == nullptr)
	{
		return CLIP_ERROR_INVALID_FRAME_NUMBER;
	}

	// Obviously, do this BEFORE assigning dirty media!
	string sourceImageFilePath = getImageFilePath(videoFrame);

	// Allocate storage for the frame if necessary (first write to this version
	// clip frame).
	DirtyMediaAssignmentUndoInfo undoInfo;
	int retVal = assignDirtyMediaStorage(frameNumber, undoInfo);
	if (retVal == CLIP_ERROR_FRAME_LIST_IS_LOCKED)
	{
		// This one's immediately fatal
		return retVal;
	}

   // QQQ what other errors can there be that we simply ignore??
	if (retVal != CLIP_ERROR_MEDIA_ALLOCATION_FAILED)
	{
		retVal = videoFrame->writeMediaFrameBuffer(inFrameBuffer, sourceImageFilePath);
	}

	if (retVal != 0)
	{
		unassignDirtyMediaStorage(frameNumber, undoInfo);
		return retVal;
	}

	return 0;
}

//------------------------------------------------------------------------
//
// Function:     initMediaBuffer
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
void CVideoFrameList::initMediaBuffer(MTI_UINT8* buffer[])
{
   int fieldCount;
   const CImageFormat *imageFormat = getImageFormat();
   int framingType = getFramingType();
   if (framingType == FRAMING_TYPE_VIDEO)
      {
      // For video-framing frame list, just assume 1 or 2 fields,
      // depending on whether its progressive or interlaced
      fieldCount = imageFormat->getFieldCount();
      }
   else if (framingType == FRAMING_TYPE_FILM
            || framingType == FRAMING_TYPE_CUSTOM)
      {
      // For film- or custom-framing, use the maximum number of fields
      // for any frame within the frame list
      fieldCount = getMaxTotalFieldCount();
      }
   else
      {
      // Unknown framing type, don't do anything
      fieldCount = 0;
      }

   // Get the number of bytes per field from the image format
   // (this number is more conservative than the byte count returned by
   //  getMaxPaddedFieldByteCount)
   MTI_UINT32 byteCount = imageFormat->getBytesPerField();

   // Iterate over the buffers for each field
   for (int i = 0; i < fieldCount; ++i)
      {
      memset(buffer[i], 0, byteCount);
      }
}

// --------------------------------------------------------------------------

//------------------------------------------------------------------------
//
// Function:     readMediaList
//
// Description:  This function reads media based on a list of frames
//               provided by the caller.  The function attempts
//               to globally optimize the multiple reads with four
//               techniques:
//                1) Multiple file handles to allow thread-safe reads
//                   from an alternate thread
//                2) Sort reads in ascending disk offset
//                3) Gang fields to read larger blocks
//                4) Use overlapped (asynchronous) reads to allow
//                   the disk subsystem to optimize the read order
//               These techniques apply to media on a VideoStore and
//               are not implemented for image files yet (although possible).
//               The use of ganged and overlapped reads is configured in
//               rawdisks.cfg.
//               This function is designed to work on video-frames frame
//               lists only and its behavior is UNDEFINED for film-frames
//               or other frame lists.  (In principle it could be made
//               to work with film-frames, but it is not implemented since the
//               current application of Control Dailes only needs video-frames.)
//
// Arguments     SMediaListEntry mediaList[]  Array of frame numbers and
//                                            buffer pointers
//               int count                    Count of entries in mediaList
//               int fileHandleIndex          Index of file handle for
//                                            opening the VideoStore multiple
//                                            times.  This value is 0 for
//                                            standard handle or a value
//                                            returned from CMediaAccess::
//                                            CreateNewFileHandle().
//
// Returns:      0 if successful, non-zero otherwise (see err_clip.h)
//
//------------------------------------------------------------------------
int CVideoFrameList::readMediaList(SMediaListEntry mediaList[], int count,
                                   int fileHandleIndex)
{
   int retVal;

   // If any frame's media has not been written or the media storage does
   // not exist, then don't bother trying to globally optimize the read.
   bool allMediaWritten = true;
   for (int i = 0; i < count; ++i)
      {
      if (!isMediaWritten(mediaList[i].frameIndex))
         {
         allMediaWritten = false;
         break;
         }
      }

   // One or more of the frames' media does not exist or has not been
   // written, so just read a single frame at a time
   if (!allMediaWritten)
      {
      for (int i = 0; i < count; ++i)
         {
         retVal = readMediaAllFieldsOptimizedMT(mediaList[i].frameIndex,
                                                mediaList[i].buffer,
                                                fileHandleIndex);
         if (retVal != 0)
            return retVal;
         }

      return 0;
      }

   // At this point we know that all of the frames have allocated media
   // storage and have been written already.

   // Gather up all of the media locations for the frames (and fields)
   const CImageFormat *imageFormat = getImageFormat();
   int fieldCount = imageFormat->getFieldCount();  // 1 for progressive
                                                   // 2 for interlaced
   int reserveCount = fieldCount * count;

   vector<CMediaLocation> mediaLocationList;
   mediaLocationList.reserve(reserveCount);
   vector<MTI_UINT8*> bufferList;
   bufferList.reserve(reserveCount);
   unsigned int previousSize = 0;
   for (int i = 0; i < count; ++i)
      {
      retVal = GetMediaLocationsAllFields(mediaList[i].frameIndex,
                                          mediaLocationList);
      if (retVal != 0)
         return retVal;

      for (unsigned int fieldIndex = 0;
           fieldIndex < mediaLocationList.size() - previousSize;
           ++fieldIndex)
         {
         bufferList.push_back(mediaList[i].buffer[fieldIndex]);
         }
      previousSize = mediaLocationList.size();
      }

   if (previousSize == 0)
      return 0;   // nothing to do

   // For now, assume that all of the media is referenced by a
   // single media access object.  As of this writing (December 2006),
   // this is always true for Clip 3 video proxies and Clip 5 media tracks.
   // When Clip 5 sequence tracks are fully implemented, multiple media
   // access objects can be expected.
   CMediaAccess *mediaAccess = mediaLocationList[0].getMediaAccess();

   // The media access object knows how to optimize the reading of
   // multiple fields for its particular media storage type
   retVal = mediaAccess->readMultipleMT(previousSize, &(bufferList[0]),
                                        &(mediaLocationList[0]),
                                        fileHandleIndex);
   if (retVal != 0)
      return retVal;

   return 0;
}

//------------------------------------------------------------------------
//
// Function:     writeMediaList
//
// Description:  This function writes media based on a list of frames
//               provided by the caller.  The function attempts
//               to globally optimize the multiple writes with three
//               techniques:
//                1) Sort reads in ascending disk offset
//                2) Gang fields to read larger blocks
//                3) Use overlapped (asynchronous) reads to allow
//                   the disk subsystem to optimize the read order
//               These techniques apply to media on a VideoStore and
//               are not implemented for image files yet (although possible).
//               The use of ganged and overlapped writes is configured in
//               rawdisks.cfg.
//               This function is designed to work on video-frames frame
//               lists only and its behavior is UNDEFINED for film-frames
//               or other frame lists.  (In principle it could be made
//               to work with film-frames, but it is not implemented since the
//               current application of Control Dailes only needs video-frames.)
//               Writes are always done with the main file handle (file
//               handle index 0).
//
// Arguments     SMediaListEntry mediaList[]  Array of frame numbers and
//                                            buffer pointers
//               int count                    Count of entries in mediaList
//
// Returns:      0 if successful, non-zero otherwise (see err_clip.h)
//
//------------------------------------------------------------------------
int CVideoFrameList::writeMediaList(SMediaListEntry mediaList[],
                                    int count)
{
   int retVal;

   // Gather up all of the media locations for the frames (and fields)
   const CImageFormat *imageFormat = getImageFormat();
   int fieldCount = imageFormat->getFieldCount();  // 1 for progressive
                                                   // 2 for interlaced
   int reserveCount = fieldCount * count;

   vector<CMediaLocation> mediaLocationList;
   mediaLocationList.reserve(reserveCount);
   vector<MTI_UINT8*> bufferList;
   bufferList.reserve(reserveCount);
   unsigned int previousSize = 0;
   for (int i = 0; i < count; ++i)
      {
      retVal = GetMediaLocationsAllFields(mediaList[i].frameIndex,
                                          mediaLocationList);
      if (retVal != 0)
         return retVal;

      for (unsigned int fieldIndex = 0;
           fieldIndex < mediaLocationList.size() - previousSize;
           ++fieldIndex)
         {
         bufferList.push_back(mediaList[i].buffer[fieldIndex]);
         }
      previousSize = mediaLocationList.size();
      }

   if (previousSize == 0)
      return 0;   // nothing to do

   // For now, assume that all of the media is referenced by a
   // single media access object.  As of this writing (December 2006),
   // this is always true for Clip 3 video proxies and Clip 5 media tracks.
   // When Clip 5 sequence tracks are fully implemented, multiple media
   // access objects can be expected.
   CMediaAccess *mediaAccess = mediaLocationList[0].getMediaAccess();

   // The media access object knows how to optimize the writing of
   // multiple fields for its particular media storage type
   retVal = mediaAccess->writeMultiple(previousSize, &(bufferList[0]),
                                       &(mediaLocationList[0]));
   if (retVal != 0)
      return retVal;

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    canBeReadAsynchronously
//
// Description: Let's caller know if asynch read is OK to try
//
// Arguments
//
// Returns:     true if OK
//
//------------------------------------------------------------------------
bool CVideoFrameList::canBeReadAsynchronously(int frameNumber)
{
   EClipVersion clipVer = getClipVer();

   if (clipVer != CLIP_VERSION_3)
      return false;

   // Get a pointer to the frame of interest
   CVideoFrame* frame = getFrame(frameNumber);
   if(frame == 0)
      return false;     // really an error

   // Read the frame's media data and return success/error status
   return (frame->canBeReadAsynchronously());
}

//------------------------------------------------------------------------
//
// Function:     startReadMediaVisibleFieldsWithOffsetOptimizedAsynchronous
//
// Description:  This hack implements a simple asynchronous read facility
//               that is suitable for readahead operation because you
//               still control reading of the frames independently, unlike
//               Starr's fascistic asynchronous I/O implementation for
//               Control Dailies where you always specify a list and lose
//               fine-grained control of the process (see readMediaList()).
//               Basically you kick off a read with this function,
//               startReadMediaVisibleFieldsWithOffsetOptimizedAsynchronous(),
//               which is the asynchronous counterpart to
//               readMediaVisibleFieldsWithOffsetOptimized(), then when you
//               actually need the results of the read, call
//               waitReadMediaVisibleFieldsWithOffsetOptimizedAsynchronous(),
//               passing it the "readContext" that was provided by the
//               start...() call!
//               NOTE: at the moment there is no way to cancel an asynch
//                     read in progress - you need to just wait until the
//                     pending read finishes!
//
// Arguments     int frameNumber               Frame number, duh
//               MTI_UINT8 *buffer[]           Start of buffers (one per field)
//               MTI_UINT8 *offsetBuffer[]     Start of actual data in buffers
//               void *&readContext            OUTPUT: Opaque pointer that you
//                                             need to pass to the "wait"
//                                             function
//               bool dontNeedAlpha            For extra fast turnaraound,
//                                             skip stupid CINTEL alpha hack
//
// Returns:      0 if successful, non-zero otherwise (see err_clip.h)
//
//------------------------------------------------------------------------
int CVideoFrameList::
startReadMediaVisibleFieldsWithOffsetOptimizedAsynchronous(
                                               int frameNumber,
                                               MTI_UINT8 *buffer[],
                                               MTI_UINT8 *offsetBuffer[],
                                               void *&readContext,
                                               bool dontNeedAlpha)
{
   int retVal;
   EClipVersion clipVer = getClipVer();

   if (clipVer != CLIP_VERSION_3)
      {
      return CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;
      }

   // Get a pointer to the frame of interest
   CVideoFrame* frame = getFrame(frameNumber);
   if(frame == 0)
      return CLIP_ERROR_INVALID_FRAME_NUMBER;     // Return error

   // Read the frame's media data and return success/error status
   retVal = frame->startReadMediaVisibleFieldsWithOffsetOptimizedAsynchronous(
                             buffer, offsetBuffer, readContext, dontNeedAlpha);
   if (retVal != 0)
      return retVal;

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    isReadMediaVisibleFieldsWithOffsetOptimizedAsynchronousDone
//
// Description: Let's caller know if asynch read is done
//
// Arguments     void *readContext   I/O: Opaque pointer that you
//                                   recieved from the "start" function
//
// Returns:     true if done
//
//------------------------------------------------------------------------
bool CVideoFrameList::
isReadMediaVisibleFieldsWithOffsetOptimizedAsynchronousDone(int frameNumber,
                                                            void *readContext)
{
   bool retVal;
   EClipVersion clipVer = getClipVer();

   if (clipVer != CLIP_VERSION_3)
      {
      return true;
      }

   // Get a pointer to the frame of interest
   CVideoFrame* frame = getFrame(frameNumber);
   if(frame == 0)
      return true;

   // Read the frame's media data and return success/error status
   retVal = frame->isReadMediaVisibleFieldsWithOffsetOptimizedAsynchronousDone(
                                                             readContext);
   return retVal;
}

//------------------------------------------------------------------------
//
// Function:     finishReadMediaVisibleFieldsWithOffsetOptimizedAsynchronous
//
// Description:  see start function
//
// Arguments     int frameNumber               Frame number, duh
//               MTI_UINT8 *offsetBuffer[]     Start of actual data in buffers
//               void *&readContext            I/O: Opaque pointer that you
//                                             recieved from the "start"
//                                             function (gets nulled out)
//
// Returns:      0 if successful, non-zero otherwise (see err_clip.h)
//
//------------------------------------------------------------------------
int CVideoFrameList::
finishReadMediaVisibleFieldsWithOffsetOptimizedAsynchronous(
                                               int frameNumber,
                                               MTI_UINT8 *offsetBuffer[],
                                               void *&readContext)
{
   int retVal;
   EClipVersion clipVer = getClipVer();

   if (clipVer != CLIP_VERSION_3)
      {
      return CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;
      }

   // Get a pointer to the frame of interest
   CVideoFrame* frame = getFrame(frameNumber);
   if(frame == 0)
      return CLIP_ERROR_INVALID_FRAME_NUMBER;     // Return error

   // Read the frame's media data and return success/error status
   retVal = frame->finishReadMediaVisibleFieldsWithOffsetOptimizedAsynchronous(
                                                             readContext,
                                                             offsetBuffer);
   if (retVal != 0)
      return retVal;

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    canBeReadAsFrameBuffers
//
// Description: Let's caller know if frame buffer read is OK to try
//
// Arguments
//
// Returns:     true if OK
//
//------------------------------------------------------------------------
bool CVideoFrameList::canBeReadAsFrameBuffers(int frameNumber)
{
   EClipVersion clipVer = getClipVer();

   if (clipVer != CLIP_VERSION_3)
      return false;

   // Get a pointer to the frame of interest
   CVideoFrame* frame = getFrame(frameNumber);
   if(frame == 0)
      return false;     // really an error

   // Read the frame's media data and return success/error status
   return (frame->canBeReadAsFrameBuffers());
}

//------------------------------------------------------------------------
//
// Function:     startReadMediaVisibleFieldFrameBuffersAsynchronous
//
// Description:  This hack implements a simple asynchronous read facility
//               that is suitable for readahead operation because you
//               still control reading of the frames independently, unlike
//               Starr's fascistic asynchronous I/O implementation for
//               Control Dailies where you always specify a list and lose
//               fine-grained control of the process (see readMediaList()).
//               Basically you kick off a read with this function,
//               startReadMediaVisibleFieldFrameBuffersAsynchronous(),
//               which is the asynchronous counterpart to
//               readMediaVisibleFieldsWithOffsetOptimized(), then when you
//               actually need the results of the read, call
//               finishReadMediaVisibleFieldFrameBuffersAsynchronous(),
//               passing it the "readContext" that was provided by the
//               start...() call!
//               NOTE: at the moment there is no way to cancel an asynch
//                     read in progress - you need to just wait until the
//                     pending read finishes!
//
// Arguments     int frameNumber               Frame number, duh
//               void *&readContext            OUTPUT: Opaque pointer that you
//                                             need to pass to the "wait"
//                                             function
//
// Returns:      0 if successful, non-zero otherwise (see err_clip.h)
//
//------------------------------------------------------------------------
int CVideoFrameList::
startReadMediaVisibleFieldFrameBuffersAsynchronous(int frameNumber, void *&readContext)
{
   int retVal;
   EClipVersion clipVer = getClipVer();

   if (clipVer != CLIP_VERSION_3)
      {
      return CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;
      }

   // Get a pointer to the frame of interest
   CVideoFrame* frame = getFrame(frameNumber);
   if(frame == 0)
      return CLIP_ERROR_INVALID_FRAME_NUMBER;     // Return error

   // Read the frame's media data and return success/error status
   retVal = frame->startReadMediaVisibleFieldFrameBuffersAsynchronous(readContext);
   if (retVal != 0)
      return retVal;

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    isReadMediaVisibleFieldFrameBuffersAsynchronousDone
//
// Description: Let's caller know if async frame buffer read is done
//
// Arguments     void *readContext   I/O: Opaque pointer that you
//                                   recieved from the "start" function
//
// Returns:     true if done
//
//------------------------------------------------------------------------
bool CVideoFrameList::
isReadMediaVisibleFieldFrameBuffersAsynchronousDone(int frameNumber, void *readContext)
{
   bool retVal;
   EClipVersion clipVer = getClipVer();

   if (clipVer != CLIP_VERSION_3)
      {
      return true;
      }

   // Get a pointer to the frame of interest
   CVideoFrame* frame = getFrame(frameNumber);
   if(frame == 0)
      return true;

   // Read the frame's media data and return success/error status
   retVal = frame->isReadMediaVisibleFieldFrameBuffersAsynchronousDone(readContext);
   return retVal;
}

//------------------------------------------------------------------------
//
// Function:     finishReadMediaVisibleFieldFrameBuffersAsynchronous
//
// Description:  see start function
//
// Arguments     int frameNumber               Frame number, duh
//               MediaFrameBuffer *frameBuffer Frame buffer received
//               void *&readContext            I/O: Opaque pointer that you
//                                             recieved from the "start"
//                                             function (gets nulled out)
//
// Returns:      0 if successful, non-zero otherwise (see err_clip.h)
//
//------------------------------------------------------------------------
int CVideoFrameList::
finishReadMediaVisibleFieldFrameBuffersAsynchronous(int frameNumber, void *&readContext, MediaFrameBuffer *frameBuffer[])
{
   int retVal;
   EClipVersion clipVer = getClipVer();

   if (clipVer != CLIP_VERSION_3)
      {
      return CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;
      }

   // Get a pointer to the frame of interest
   CVideoFrame* frame = getFrame(frameNumber);
   if(frame == 0)
      return CLIP_ERROR_INVALID_FRAME_NUMBER;     // Return error

   // Read the frame's media data and return success/error status
   retVal = frame->finishReadMediaVisibleFieldFrameBuffersAsynchronous(
                                                             readContext,
                                                             frameBuffer);
   if (retVal != 0)
      return retVal;

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Clip IniFile Interface
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    readFrameListInfoSection
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
int CVideoFrameList::readFrameListInfoSection(CIniFile *iniFile,
                                               const string& sectionPrefix,
                                             const CTimecode& timecodePrototype)
{
   int retVal;
   string sectionName = sectionPrefix + "\\" + videoTrackSectionName;

   retVal = CFrameList::readFrameListInfoSection(iniFile, sectionName,
                                                  timecodePrototype);
   if (retVal != 0)
      return retVal; // ERROR: Failed to read the CFrameList portion
                     //        of the Video Track section

   return 0;  // Success

} // readFrameListInfoSection( )

//------------------------------------------------------------------------
//
// Function:    writeFrameListInfoSection
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
int CVideoFrameList::writeFrameListInfoSection(CIniFile *iniFile,
                                                const string& sectionPrefix)
{
   int retVal;
   string sectionName = sectionPrefix + "\\" + videoTrackSectionName;

   retVal = CFrameList::writeFrameListInfoSection(iniFile, sectionName);
   if (retVal != 0)
      return retVal;  // ERROR: Failed to write the CFrameList portion
                      //        of the Video Track section

   return 0;  // Success

} // writeFrameListInfoSection


//////////////////////////////////////////////////////////////////////
// Miscellaneous Functions
//////////////////////////////////////////////////////////////////////

void CVideoFrameList::dump(MTIostringstream &str)
{
   str << "Video Clip Header" << std::endl;

   CFrameList::dump(str);

   str << "  maxPaddedFieldByteCount: " << getMaxPaddedFieldByteCount()
       << std::endl;

}


bool SCopyFrameSource::isAllSourceFramesUserMaterial() const
{
   for (int i = 0; i < fieldCount; ++i)
      {
      if (!srcTrack->isFrameIndexUserMaterial(fieldSrc[i].frameIndex))
         return false;
      }

   return true;
}
