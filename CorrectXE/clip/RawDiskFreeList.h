// RawDiskFreeList.h: interface for the CRawDiskFreeList class.
//
/*
$Header: /usr/local/filmroot/clip/include/RawDiskFreeList.h,v 1.15 2005/01/04 23:15:06 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef RAWDISKFREELIST_H
#define RAWDISKFREELIST_H

#include <vector>
#include <list>
#include <string>
#include <iostream>

using std::vector;
using std::list;
using std::string;
using std::ostream;

#include "machine.h"
#include "cliplibint.h"

#define NEW_DISK_ALLOCATION 1

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CMediaDeallocList;

//////////////////////////////////////////////////////////////////////

class CRawDiskFreeEntry
{
public:
   CRawDiskFreeEntry();
   CRawDiskFreeEntry(MTI_INT64 newOffset, MTI_INT64 newSize);
   ~CRawDiskFreeEntry();

   void dump(ostream& str);

   MTI_INT64 offset;   // offset to beginning of free block,
                       // in 512 byte blocks from start of file or partition
   MTI_INT64 size;     // size of free block, in 512 byte blocks
};

//////////////////////////////////////////////////////////////////////

#ifdef NEW_DISK_ALLOCATION

// Default minimum non-contiguous allocation unit, in number of frames
#define RF_DEFAULT_NON_CONTIGUOUS_MIN_FRAMES    100

struct SRawDiskBlock
{
   MTI_INT64 offset;   // offset to beginning of free block, in bytes
   MTI_INT64 size;     // size of free block, in bytes

   MTI_INT32 firstFieldNumber;
   MTI_UINT32 fieldCount;
};

class CRawDiskBlockList
{
public:
   CRawDiskBlockList(MTI_UINT32 newPaddedFieldSize);
   ~CRawDiskBlockList();

   void addBlock(MTI_INT64 newOffset, MTI_INT64 newSize);
   const SRawDiskBlock* getLastEntry();
   int getEntryCount();
   void sortByOffset();
   MTI_INT64 getFieldOffset(MTI_UINT32 fieldNumber);

   SRawDiskBlock *getEntry(int index);

private:
   vector<SRawDiskBlock*> rawDiskBlockList;

   SRawDiskBlock *currentRawDiskBlock;
   MTI_UINT32 paddedFieldSize;
   MTI_UINT32 maxFieldNumber;
   
private:
   static bool compareByOffset(SRawDiskBlock const * const &b1,
                               SRawDiskBlock const * const &b2);
};

// Raw Disk Allocation Method
enum ERawDiskAllocMethod
{
  RF_ALLOC_CONTIGUOUS_FIRST_AVAILABLE,
  RF_ALLOC_CONTIGUOUS_SMALLEST_AVAILABLE,
  RF_ALLOC_NON_CONTIGUOUS
};

#endif // #ifdef NEW_DISK_ALLOCATION

//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CRawDiskFreeList
{
public:
   CRawDiskFreeList(const string& newFileName);
   virtual ~CRawDiskFreeList();

   int queryFreeSpace(MTI_INT64 *totalBlocks, MTI_INT64 *maxContiguousBlocks);
   int queryFreeSpaceEx(MTI_UINT32 paddedFrameSize,
                        ERawDiskAllocMethod allocMethod, int minFrameCount,
                        MTI_INT64 *totalBlocks, MTI_INT64 *allocatableBlocks);

   int CheckRFL();
   
   void compressList();
   int readFreeListFile();
   int writeFreeListFile();
   void clearFreeList();
   void findFirst (MTI_INT64 &offset, MTI_INT64 &size);
   void findNext (MTI_INT64 &offset, MTI_INT64 &size);
   void add (MTI_INT64 &offset, MTI_INT64 &size);

   MTI_INT64 allocate(MTI_INT64 size);
#ifdef NEW_DISK_ALLOCATION
   int allocate(int fieldCount, int fieldSize, bool interlaced,
                ERawDiskAllocMethod allocMethod, int minFieldCount,
                CRawDiskBlockList &allocList);
#endif
   int deallocate(MTI_INT64 location, MTI_INT64 size);
   int deallocate(CRawDiskBlockList &allocList);
   int deallocate(CMediaDeallocList& mediaDeallocList);
   int resetFreeList(MTI_INT64 startingOffset, MTI_INT64 size);

   void dump(ostream& str);

private:
   // List of pointers to CRawDiskFreeEntry objects
   typedef vector<CRawDiskFreeEntry*> RawDiskFreeList;
   typedef RawDiskFreeList::iterator RawDiskFreeListIterator;

   RawDiskFreeList freeList;
   RawDiskFreeListIterator posSearch;
   string fileName;

   static const string freeEntryFormatString;

   void mergeList(RawDiskFreeList &newList);
   int newMergeList(list<CRawDiskFreeEntry*> &newList);

#ifdef NEW_DISK_ALLOCATION
   int findBlock(int fieldCount, int fieldSize, bool interlaced,
                 ERawDiskAllocMethod allocMethod, int minFieldCount,
                 CRawDiskBlockList &allocList);
   int findFirstBlock(int fieldCount, int fieldSize,
                      CRawDiskBlockList &allocList);
   int findLargestAvailable(int fieldSiz,  bool interlacede, int minFieldCount,
                            CRawDiskBlockList &allocList);
   int findSmallestAvailable(int fieldCount, int fieldSize,
                             CRawDiskBlockList &allocList);
#endif
};

#ifdef PRIVATE_STATIC
#ifndef _RAW_DISK_FREE_LIST_DATA_CPP
namespace RawDiskFreeListData {
   // Compiler-dependent format string for free list file entry
   extern const string freeEntryFormatString;
};
#endif
#endif
//////////////////////////////////////////////////////////////////////

#endif // #ifndef RAWDISKFREELIST_H
