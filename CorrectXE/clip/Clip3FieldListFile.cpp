// Clip3FieldListFile.cpp:
//
/* CVS Info:
*/
//////////////////////////////////////////////////////////////////////

#include "Clip3FieldListFile.h"
#include "Clip3Track.h"
#include "CommonHeaderFileRecord.h"
#include "err_clip.h"
#include "FileScanner.h"
#include "StructuredFileStream.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFieldListEntryFileRecord::CFieldListEntryFileRecord(MTI_UINT16 version)
: CFileRecord(FR_RECORD_TYPE_FIELD_LIST_ENTRY, version),
  flags(0), defaultMediaType(0), frameIndex(-1), fieldIndex(-1)

{
   for (int i = 0; i < FIELD_LIST_ENTRY_RECORD_RESERVED_COUNT; ++i)
      reserved[i] = 0;
}

CFieldListEntryFileRecord::~CFieldListEntryFileRecord()
{
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

MTI_INT32 CFieldListEntryFileRecord::getDefaultMediaType() const
{
   return defaultMediaType;
}

MTI_INT32 CFieldListEntryFileRecord::getFieldIndex() const
{
   return fieldIndex;
}

MTI_UINT32 CFieldListEntryFileRecord::getFlags() const
{
   return flags;
}

MTI_INT32 CFieldListEntryFileRecord::getFrameIndex() const
{
   return frameIndex;
}

//////////////////////////////////////////////////////////////////////
// Mutators
//////////////////////////////////////////////////////////////////////

void CFieldListEntryFileRecord::setDefaultMediaType(MTI_INT32 newType)
{
   defaultMediaType = newType;
}

void CFieldListEntryFileRecord::setFieldIndex(MTI_INT32 newIndex)
{
   fieldIndex = newIndex;
}

void CFieldListEntryFileRecord::setFlags(MTI_UINT32 newFlags)
{
   flags = newFlags;
}

void CFieldListEntryFileRecord::setFrameIndex(MTI_INT32 newIndex)
{
   frameIndex = newIndex;
}

//////////////////////////////////////////////////////////////////////
// Read and Write functions
//////////////////////////////////////////////////////////////////////

int CFieldListEntryFileRecord::read(CStructuredStream &stream)
{
   int retVal;

   // flags field
   if ((retVal = stream.readUInt32(&flags)) != 0)
      return retVal;

   // defaultMediaType field
   if ((retVal = stream.readInt32(&defaultMediaType)) != 0)
      return retVal;

   // Read frameIndex field
   if ((retVal = stream.readInt32(&frameIndex)) != 0)
      return retVal;

  // Read fieldIndex field
   if ((retVal = stream.readInt32(&fieldIndex)) != 0)
      return retVal;

   // Read the reserved words
   for (int i = 0; i < FIELD_LIST_ENTRY_RECORD_RESERVED_COUNT; ++i)
      {
      if((retVal = stream.readUInt32(&(reserved[i]))) != 0)
         return retVal;
      }

   return 0;
}

int CFieldListEntryFileRecord::write(CStructuredStream &stream)
{
   int retVal;

   // Write record preamble
   if (CFileRecord::write(stream) != 0)
      return CLIP_ERROR_TIME_TRACK_FILE_WRITE_ERROR;

   // flags field
   if ((retVal = stream.writeUInt32(flags)) != 0)
      return retVal;

   // defaultMediaType field
   if ((retVal = stream.writeInt32(defaultMediaType)) != 0)
      return retVal;

   // frameIndex field
   if ((retVal = stream.writeUInt32(frameIndex)) != 0)
      return retVal;

  // fieldIndex field
   if ((retVal = stream.writeInt32(fieldIndex)) != 0)
      return retVal;

   // Write the reserved words
   for (int i = 0; i < FIELD_LIST_ENTRY_RECORD_RESERVED_COUNT; ++i)
      {
      if((retVal = stream.writeUInt32(reserved[i])) != 0)
         return retVal;
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////

void CFieldListEntryFileRecord::calculateRecordByteCount()
{
   // Total the size of all the fields in the record

   CFileRecord::calculateRecordByteCount();

   MTI_UINT32 newByteCount = getRecordByteCount();

   newByteCount += sizeof(flags);

   newByteCount += sizeof(defaultMediaType);

   newByteCount += sizeof(frameIndex);
   newByteCount += sizeof(fieldIndex);

   newByteCount += sizeof(reserved);
   
   // Update the object with the calculated size
   setRecordByteCount(newByteCount);
}


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFieldListFileReader::CFieldListFileReader()
: externalBuffer(0), externalBufferSize(0),
  stream(0), fileScanner(0)
{

}

CFieldListFileReader::CFieldListFileReader(char *newBuffer,
                                           unsigned int newBufferSize)
: externalBuffer(newBuffer), externalBufferSize(newBufferSize),
  stream(0), fileScanner(0)
{

}

CFieldListFileReader::~CFieldListFileReader()
{
   closeFieldListFile();
}

//////////////////////////////////////////////////////////////////////
// Track Builder
//////////////////////////////////////////////////////////////////////

int CFieldListFileReader::readFieldListFile(CFieldList& fieldList)
{
   int retVal;

   retVal = openFieldListFile(fieldList.getFieldListFileNameWithExt());
   if (retVal != 0)
      {
      // ERROR: Could not open field list file, possibly because file did not
      //        exist or is the file does not have the MTI/CPMP file format
      return retVal;
      }

   // Check that track types of the file agree
   if (fileScanner->getFileType()!= FR_FILE_TYPE_FIELD_LIST)
      {
      // Error: file type is not a Field List
      return CLIP_ERROR_FIELD_LIST_FILE_INVALID_FILE_TYPE;
      }

   // Read all the rest of the records in the file and create
   // the Field List Entry objects and attach them to the Field List
   retVal = buildFieldListFromFile(fieldList);
   if(retVal != 0)
      return retVal; // Error: couldn't build all of the fields

   closeFieldListFile();

   return 0;  // Success

} // readFieldListFile

int CFieldListFileReader::reloadFieldListFile(CFieldList& fieldList)
{
   int retVal;

   retVal = openFieldListFile(fieldList.getFieldListFileNameWithExt());
   if (retVal != 0)
      {
      // ERROR: Could not open field list file, possibly because file did not
      //        exist or is the file does not have the MTI/CPMP file format
      return retVal;
      }

   // Check that track types of the file agree
   if (fileScanner->getFileType()!= FR_FILE_TYPE_FIELD_LIST)
      {
      // Error: file type is not a Field List
      return CLIP_ERROR_FIELD_LIST_FILE_INVALID_FILE_TYPE;
      }

   // Read all the rest of the records in the file and update
   // the Field List Entry objects in the field list
   retVal = updateFieldListFromFile(fieldList);
   if(retVal != 0)
      return retVal; // Error: couldn't update all of the fields

   closeFieldListFile();

   return 0;  // Success

} // reloadFieldListFile

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CFieldListFileReader::openFieldListFile(const string& fieldListFileName)
{
   int retVal;

   closeFieldListFile();   // If previously opened

   if (externalBuffer != 0 && externalBufferSize != 0)
      stream = new CStructuredFileStream(externalBuffer, externalBufferSize);
   else
      stream = new CStructuredFileStream;

   // Open file with stream
   if (stream->open(fieldListFileName) != 0)
      {
      // ERROR: Could not open stream with field list file name
      TRACE_0(errout << "ERROR: Unable to open Field List file "
                     << fieldListFileName << endl;
              errout << " because " << strerror(errno) << endl);
      delete stream;
      stream = 0;
      return CLIP_ERROR_FIELD_LIST_FILE_OPEN_ERROR;
      }

   // Create an instance of a CFileScanner and initialize
   fileScanner = new CFileScanner;
   retVal = fileScanner->initForReading(stream);
   if (retVal != 0)
      {
      // ERROR: Could not initialize CFileScanner for reading
      //        Possibly because file was not really a Field List File
      closeFieldListFile();    // Cleanup
      return retVal;
      }

   return 0;
}

void CFieldListFileReader::closeFieldListFile()
{
   if (fileScanner)
      {
      delete fileScanner;
      fileScanner = 0;
      }

   if (stream)
      {
      stream->close();
      delete stream;
      stream = 0;
      }
}

int CFieldListFileReader::buildFieldListFromFile(CFieldList& fieldList)
{
   CFileRecord *newRecordPtr;
   CFieldListEntry *newField = 0;

   // Read all records from field list file and convert to
   // CFieldListEntry  instances
   while (fileScanner->readNextRecord(&newRecordPtr) == 0)
      {
      switch (EFileRecordType(newRecordPtr->getRecordType()))
         {
         case FR_RECORD_TYPE_FIELD_LIST_ENTRY :

            // Create a new instance of a CFieldListEntry object and set
            // based on CFieldListEntryFileRecord instance
            newField
            = makeField(static_cast<CFieldListEntryFileRecord&>(*newRecordPtr));

            break;

         default :
            // Error: Unknown or unexpected record type
            delete newRecordPtr;
            return CLIP_ERROR_FIELD_LIST_FILE_INVALID_RECORD;
         }

      // Add the new field list entry to the field list
      fieldList.addField(newField);

      // Delete the file record just obtained
      delete newRecordPtr;
      }

   return 0;

} // buildFieldListFromFile

int CFieldListFileReader::updateFieldListFromFile(CFieldList& fieldList)
{
   int retVal;
   CFileRecord *newRecordPtr;

   // Read all records from field list file and convert to
   // CFieldListEntry  instances
   int fieldCount = fieldList.getFieldCount();
   for (int i = 0; i < fieldCount; ++i)
      {
      CFieldListEntry *field = fieldList.getField(i);
      retVal = fileScanner->readNextRecord(&newRecordPtr);
      if (retVal != 0)
         return retVal;  // TBD: Error code that indicates not enough records

      if (EFileRecordType(newRecordPtr->getRecordType())
                                            != FR_RECORD_TYPE_FIELD_LIST_ENTRY)
         return CLIP_ERROR_FIELD_LIST_FILE_INVALID_RECORD;

      // Set the existing instance of a CFieldListEntry object
      // based on CFieldListEntryFileRecord instance
      setField(static_cast<CFieldListEntryFileRecord&>(*newRecordPtr), *field);

      // Delete the file record just obtained
      delete newRecordPtr;
      }

   return 0;

} // buildFieldListFromFile

CFieldListEntry*
CFieldListFileReader::makeField(CFieldListEntryFileRecord& fileRecord)
{
   // Make a new CFieldListEntry instance
   CFieldListEntry* field = new CFieldListEntry;

   setField(fileRecord, *field);

   return field;
}

void CFieldListFileReader::setField(CFieldListEntryFileRecord& fileRecord,
                                    CFieldListEntry &field)
{
   // Initialize the field from the file record
   field.setFlags(fileRecord.getFlags());
   field.setDefaultMediaType(fileRecord.getDefaultMediaType());
   field.setFrameIndex(fileRecord.getFrameIndex());
   field.setFieldIndex(fileRecord.getFieldIndex());

   field.setNeedsUpdate(false);    // Data is fresh from disk, don't
                                    // need to write back
}

//////////////////////////////////////////////////////////////////////
/////////////                            /////////////////////////////
//////////     CFieldListFileWriter     //////////////////////////
////////////                            //////////////////////////////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFieldListFileWriter::CFieldListFileWriter()
{

}

CFieldListFileWriter::~CFieldListFileWriter()
{

}

//////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////

int
CFieldListFileWriter::writeFieldListFile(CFieldList& fieldList)
{
   int retVal;

   // Create appropriate instance of CStructuredStream subtype
   CStructuredFileStream stream;

   // Create file with magic word and Common Header Record
   string fieldListFileName = fieldList.getFieldListFileNameWithExt();
   if(fileBuilder.createFile(&stream, fieldListFileName,
                             FR_FILE_TYPE_FIELD_LIST,
                             FR_FIELD_LIST_FILE_TYPE_FILE_VERSION) != 0)
      return CLIP_ERROR_FIELD_LIST_FILE_CREATE_FAILED;

   // Field Records
   retVal = writeFieldList(fieldList);
   if (retVal != 0)
      return retVal; // ERROR: Failed to write fields to field list file

   // Close file
   if (stream.close() != 0)
      return CLIP_ERROR_FIELD_LIST_FILE_CLOSE_FAILED;

   return 0;   // Success
}

int CFieldListFileWriter::initForUpdate(CStructuredFileStream *newStream)
{
   fileBuilder.initForFileUpdate(newStream);

   return 0;
}

//////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////


int CFieldListFileWriter::writeField(CFieldListEntry& field)
{
   int retVal;

   CFieldListEntryFileRecord fileRecord;

   fileRecord.setFlags(field.getFlags());
   fileRecord.setDefaultMediaType(field.getDefaultMediaType());
   fileRecord.setFrameIndex(field.getFrameIndex());
   fileRecord.setFieldIndex(field.getFieldIndex());

   // Calculate and set the size of the record
   fileRecord.calculateRecordByteCount();

   retVal = fileBuilder.putRecord(fileRecord);
   if (retVal != 0)
      return retVal;

   field.setNeedsUpdate(false);

   return 0;
}

int CFieldListFileWriter::writeFieldList(CFieldList& fieldList)
{
   int retVal;

   int fieldCount = fieldList.getFieldCount();

   retVal = writeFieldList(fieldList, 0, fieldCount);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CFieldListFileWriter::writeFieldList(CFieldList& fieldList,
                                         int startFieldIndex, int fieldCount)
{
   int retVal;

   // Iterate over all entries in the field list
   int endIndex = startFieldIndex + fieldCount;
   for(int fieldIndex = startFieldIndex; fieldIndex < endIndex; ++fieldIndex)
      {
      CFieldListEntry *field = fieldList.getField(fieldIndex);

      // Write the field list entry to the file
      retVal = writeField(*field);
      if (retVal != 0)
         return retVal;
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////
/////////////                            /////////////////////////////
//////////     CFieldListFileUpdater     //////////////////////////
////////////                            //////////////////////////////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFieldListFileUpdater::CFieldListFileUpdater()
 : externalBuffer(0), externalBufferSize(0),
   fileReader(0), fileWriter(0), fileHeaderSize(0), fieldRecordSize(0)
{

}

CFieldListFileUpdater::CFieldListFileUpdater(char *newBuffer,
                                             unsigned int newBufferSize)
 : externalBuffer(newBuffer), externalBufferSize(newBufferSize),
   fileReader(0), fileWriter(0), fileHeaderSize(0), fieldRecordSize(0)
{

}

CFieldListFileUpdater::~CFieldListFileUpdater()
{
   closeFieldListFile();
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CFieldListFileUpdater::updateFieldListFile(CFieldList& fieldList,
                                               int firstFieldIndex,
                                               int fieldCount)
{
   int retVal;

   // Update the field list file with the marked entries in the field list
   retVal = updateFieldList(fieldList, firstFieldIndex, fieldCount);
   if(retVal != 0)
      return retVal; // Error: couldn't update all of the entries

   return 0;  // Success

} // updateFieldListFile

//////////////////////////////////////////////////////////////////////
//  Open/Close Field List File for Update
//////////////////////////////////////////////////////////////////////

int
CFieldListFileUpdater::openFieldListFile(const string &fieldListFileName)
{
   int retVal;

   // Use an instance of a  CFieldListFileReader to open the field
   // list file
   if (externalBuffer != 0 && externalBufferSize != 0)
      fileReader = new CFieldListFileReader(externalBuffer, externalBufferSize);
   else
      fileReader = new CFieldListFileReader;

   // ??? Do we need to separately check for the existence of the file
   
   retVal = fileReader->openFieldListFile(fieldListFileName);
   if (retVal != 0)
      {
      // ERROR: Could not open field list file, possibly because file did not
      //        exist or is the file does not have the MTI/CPMP file format
      return retVal;
      }

   // Check that the file agree
   if (fileReader->fileScanner->getFileType()!= FR_FILE_TYPE_FIELD_LIST)
      {
      // Error: file type is not a Field List
      return CLIP_ERROR_FIELD_LIST_FILE_INVALID_FILE_TYPE;
      }

   // Get the size of the Common File Header Record
   CCommonHeaderFileRecord *commonHeaderRecord
                            = fileReader->fileScanner->getCommonHeaderRecord();

   // Get the record size in bytes and add the size of the magic word.
   // This will be used as an offset past the file header
   fileHeaderSize = commonHeaderRecord->getRecordByteCount()
                    + sizeof(MTI_UINT32);

   // Read the first record.  This should be a field list entry of some sort
   // We want to know its size.
   CFileRecord *fileRecord = 0;
   retVal = fileReader->fileScanner->readNextRecord(&fileRecord);
   if (retVal != 0)
      return retVal;

   // Remember the size of the (assumed) field list entry record
   // We will use this to calculate the offset to the individual
   // records will will want to write
   fieldRecordSize = fileRecord->getRecordByteCount();

   delete fileRecord;  // we're done with the file record

   // Clear file reader's stream buffers
   fileReader->stream->initBuffer();

   // Use an instance of a CFieldListFileWriter to format and
   // write the field list entry records
   fileWriter = new CFieldListFileWriter;
   fileWriter->initForUpdate(fileReader->stream);

   return 0;

} // openFieldListFile

int CFieldListFileUpdater::closeFieldListFile()
{
   delete fileWriter;
   fileWriter = 0;

   delete fileReader;     // this will close the open file
   fileReader = 0;

   return 0;

} // closeFieldListFile

//////////////////////////////////////////////////////////////////////
//  Update From from Field List
//////////////////////////////////////////////////////////////////////

int CFieldListFileUpdater::updateFieldList(CFieldList& fieldList,
                                           int firstFieldIndex,
                                           int fieldCount)
{
   int retVal;

   bool fileOpen = false;
   int fieldIndex = firstFieldIndex;
   int startFieldIndex;
   int updateCount;
   CFieldListEntry *field;

   TRACE_3(errout << "CFieldListFileUpdater::updateFieldList(index=" << firstFieldIndex << ","
                  << " count=" << fieldCount << ")");

   if (!fieldList.isFieldListAvailable())
      return 0;   // field list is not available, so cannot update

   // If caller's fieldCount is -1, then check all fields starting
   // at firstFieldCount and going to the end of the field list
   if (fieldCount == -1)
      fieldCount = fieldList.getFieldCount() - firstFieldIndex;

   // Exclusive out field index
   int outField = firstFieldIndex + fieldCount;

   // Iterate over all of the entries in the field list, writing out
   // any entries that need to be updated.  The field list file is
   // not opened until we know that at least one record needs to be
   // updated.
   while (true)
      {
      if (fieldIndex >= outField)
         break;

      updateCount = 0;

      // Search for the first field list entry that needs to be updated
      for ( ; fieldIndex < outField; ++fieldIndex)
         {
         field = fieldList.getField(fieldIndex);
         if (field->getNeedsUpdate())
            {
            startFieldIndex = fieldIndex;
            updateCount = 1;
            ++fieldIndex;
            break;
            }
         }

      if (updateCount < 1)
         break;  // Nothing to update, so exit loop

      // Count the number of entries that need to be updated
      for ( ; fieldIndex < outField; ++fieldIndex)
         {
         field = fieldList.getField(fieldIndex);
         if (field->getNeedsUpdate())
            ++updateCount;
         else
            {
            ++fieldIndex;
            break;
            }
         }

      // Open the field list file if it is not already open
      if(!fileOpen)
         {
         retVal = openFieldListFile(fieldList.getFieldListFileNameWithExt());
         if (retVal != 0)
            {
            // ERROR: Could not open field list file, possibly because file
            //        did not exist or the file does not have the
            //        MTI/CPMP file format
            return retVal;
            }

         fileOpen = true;   // remember we opened the file
         }

      retVal = updateFields(fieldList, startFieldIndex, updateCount);
      if (retVal != 0)
         {
         // Error
         if (fileOpen)
            closeFieldListFile();
         return retVal;
         }
      }

   if (fileOpen)
      closeFieldListFile();

   return 0;

} // updateFieldList

int CFieldListFileUpdater::updateFields(CFieldList& fieldList,
                                        int startFieldIndex, int fieldCount)
{
   int retVal;

   if (fieldCount < 1)
      return 0;

   // Seek to target record in the file
   retVal = seekToRecord(startFieldIndex);
   if (retVal != 0)
      return retVal;

   // Write the fields to the file starting at the seek position
   retVal = fileWriter->writeFieldList(fieldList, startFieldIndex, fieldCount);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CFieldListFileUpdater::seekToRecord(int recordIndex)
{
   int retVal;

   // Calculate byte offset to target record in file
   MTI_INT64 recordOffset = fileHeaderSize + recordIndex * fieldRecordSize;

   // Seek to the record in the file (this will also flush any pending
   // writes)
   retVal = fileReader->stream->seek(recordOffset);
   if (retVal != 0)
      return retVal;

   return 0;
}



