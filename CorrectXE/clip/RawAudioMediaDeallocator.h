// RawAudioMediaDeallocator.h: interface for the CRawAudioMediaDeallocator class.
//
/* CVS Info:
$Header: /usr/local/filmroot/clip/include/RawAudioMediaDeallocator.h,v 1.4 2006/01/11 01:55:50 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#if !defined(RAWAUDIOMEDIADEALLOCATOR_H)
#define RAWAUDIOMEDIADEALLOCATOR_H

#include "MediaDeallocator.h"
#include "MediaDeallocList.h"

//////////////////////////////////////////////////////////////////////

class CRawAudioMediaDeallocator : public CMediaDeallocator  
{
public:
   CRawAudioMediaDeallocator();
   virtual ~CRawAudioMediaDeallocator();

   int freeMediaAllocation(const CMediaLocation& mediaLocation);
   int freeMediaAllocation(const CMediaLocation& startMediaLocation,
                           int itemCount);
                           
   CMediaDeallocList& getMediaDeallocList();

private:
   CMediaDeallocList deallocList;
};

//////////////////////////////////////////////////////////////////////

#endif // !defined(RAWAUDIOMEDIADEALLOCATOR_H)

