// FileRecordString.cpp: implementation of the CFileRecordString class.
//
/*
$Header: /usr/local/filmroot/clip/source/FileRecordString.cpp,v 1.2 2003/01/29 06:07:06 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "FileRecordString.h"
#include "err_clip.h"
#include "StructuredStream.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFileRecordString::CFileRecordString()
 : size(0)
{
    // Initializer list creates empty string
}

CFileRecordString::CFileRecordString(MTI_UINT16 newSize, 
                                     const string& inputString)
 : size(newSize), theString(newSize, '\0')
{
   // Copy contents of inputString to *theString, but no more than
   // newSize characters
   theString.assign(inputString, 0, newSize);
}

CFileRecordString::CFileRecordString(const string& inputString)
 : size(inputString.size()), theString(inputString)
{
        
}

CFileRecordString::CFileRecordString(MTI_UINT16 newSize)
 : size(newSize), theString(newSize, '\0')
{
}

CFileRecordString::~CFileRecordString()
{
}

#define LOCAL_BUFFER_SIZE 1024

int CFileRecordString::read(CStructuredStream& stream)
{
    int status = 0;
    char localBuffer[LOCAL_BUFFER_SIZE];
    char* buffer;

    // Read the character count
    if ((status = stream.readUInt16(&size)) != 0)
        return status;

    // Use local char buffer if it big enough, otherwise allocate
    // buffer space on the heap
    if (size <= LOCAL_BUFFER_SIZE)
        buffer = localBuffer;
    else
        buffer = new char[size];

    // Read the string from stream and put it into theString
    if (stream.read(buffer, size) == size)
        {
        theString.assign(buffer, size);
        }
    else
        {
        status = CLIP_ERROR_FILE_READ_FAILED;
        }

    
    // Deallocate buffer if it wasn't local
    if (size > LOCAL_BUFFER_SIZE)
        delete [] buffer;

    return status;
}

int CFileRecordString::write(CStructuredStream& stream) const
{
   int retVal;

   // Write the character count
   if ((retVal = stream.writeUInt16(size)) != 0)
      return retVal;

   // Write the string from theString out to the stream
   if (stream.write(theString.data(), size) != size)
      return CLIP_ERROR_FILE_WRITE_FAILED;

   return 0;
}

const string& CFileRecordString::getString() const
{
    return theString;
}

void CFileRecordString::setString(const string &newString)
{
    size = newString.size();
    theString = newString;
}

void CFileRecordString::setString(const char* newString)
{
   if (newString == 0)
      {
      setSize(0);
      }
   else
      {
      theString = newString;
      size = theString.size();
      }
}

void CFileRecordString::setString(const string &newString, MTI_UINT32 newSize)
{
    theString.assign(newString, 0, newSize);
    setSize(newSize);
}

void CFileRecordString::setSize(MTI_UINT16 newSize)
{
    theString.resize(newSize, '\0');
    size = newSize;
}

// Get size of string plus size of count
MTI_UINT16 CFileRecordString::getFieldSize()
{
    return(sizeof(size) + size);
}
