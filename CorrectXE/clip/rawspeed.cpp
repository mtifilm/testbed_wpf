// Disk read speed test driver for Media Storage Interface Classes
//
// Similar to rsysread
//
//////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <iostream>
#include <sys/time.h>
#include <malloc.h>

#include "MediaInterface.h"
#include "MediaLocation.h"
#include "MediaAllocator.h"

#define BLOCK_SIZE 512

//////////////////////////////////////////////////////////////////////

int main(int argc, char* argv[])
{

   //////////////////////////////////////////////////////////////////////
   //               Command Line Arguments 
   //////////////////////////////////////////////////////////////////////

   if (argc != 5)
      {
      cout << "Usage: " << endl;
      cout <<     argv[0] << " numBlocksToRead startBlock blocksPerRead mediaID"
           << endl;
      return 1;
      }

   // Read and convert the command line arguments
   int numberOfBlocksToRead = atoi(argv[1]);
   int startBlock = atoi(argv[2]);
   int blocksPerRead = atoi(argv[3]);
   string mediaID(argv[4]);

   //////////////////////////////////////////////////////////////////////
   //               Initialization 
   //////////////////////////////////////////////////////////////////////

   unsigned int bytesPerRead = (unsigned int)blocksPerRead * BLOCK_SIZE;
   const int bufferCount = 32;
   unsigned char* readBuffer[bufferCount];
   int i;
        struct timeval t_inits, t_local, t_new;

   // Allocate memory for read buffers
   for (i = 0; i < bufferCount; ++i)
      {
      if(!(readBuffer[i] = (unsigned char*)memalign(BLOCK_SIZE, bytesPerRead)))
         {
         perror("memalign");
         return 1;
         }

      // Clear the buffer for some reason
      memset(readBuffer[i], 0, bytesPerRead);
      }
      
   // Create a single instance of the CMediaInterface class
   // This instance provides the read, write and allocation interface 
   // for all media storage types (raw disk, file system,etc.)
   CMediaInterface mediaInterface; 

   // Initialize the media interface.  Among other things, this reads
   // in the raw disk configuration file rawdisk.cfg
   cerr << "Attempting to initialize the CMediaInterface object" << endl;
   if(!mediaInterface.initialize())
      {
      cerr << "ERROR: Initialization of CMediaInterface object failed"
           << endl;
      return 1;
      }

   //////////////////////////////////////////////////////////////////////
   //               Read Speed Test
   //////////////////////////////////////////////////////////////////////

   // Create a CMediaLocation instances to hold: 
   //    media type           only MEDIA_TYPE_RAW implemented
   //    media identifier     e.g. "rd2"
   //    offset 
   //    padded field size
   // This array will simulate a "clip" data structure
   CMediaLocation mediaLocation;

   //  Get the current time of day
        gettimeofday (&t_inits);
        t_local = t_inits;

   MTI_UINT64 currentOffset = startBlock * BLOCK_SIZE;

   // Start reading
   int passCount = 2;
   int j;
   int bufferIndex = 0;
   for (j = 1; j <= passCount; ++j)
      {
      cout << "Pass #" << j << endl;
   for (i = 1; i <= numberOfBlocksToRead; ++i)
      {
      mediaLocation.setMediaType(MEDIA_TYPE_RAW);
      mediaLocation.setMediaIdentifier(mediaID);
      mediaLocation.setOffset(currentOffset);
      mediaLocation.setPaddedSize(bytesPerRead);
//      if (mediaInterface.read(mediaLocation, readBuffer[rand()%bufferCount])==-1)
      if (mediaInterface.read(mediaLocation, readBuffer[bufferIndex])==-1)
         {
         cerr << "ERROR: Image read from disk failed for read number " 
              << i <<  endl;
         return 1;
         }

      // Advance ring buffer pointer
      bufferIndex = (++bufferIndex) % bufferCount;

      // seek sequentially
      currentOffset += bytesPerRead;

      // Every 50 reads print of read rate 
      if (i % 50 == 0)
         {
         gettimeofday(&t_new);
         float usec = 1000000 * (t_new.tv_sec - t_local.tv_sec) + 
                (t_new.tv_usec - t_local.tv_usec);

         cout << "Rate (during " << i-50 
              << " to " << i 
              << " of total " << numberOfBlocksToRead 
              << "): " 
              << ((float)(bytesPerRead*50))/usec << " MB/s" << endl;
         t_local = t_new;
         }

      } // for (i = 0; i < numberOfBlocksToRead, ++i)
      }

        (void)gettimeofday(&t_new);
   double dTotaluSec = 1000000.0*(double)(t_new.tv_sec - t_inits.tv_sec) 
                     + (double)(t_new.tv_usec - t_inits.tv_usec);

   double dTotalMB = ((double)numberOfBlocksToRead) * ((double)bytesPerRead);

   double dTotalRate = dTotalMB/dTotaluSec;

   cout << "Read " << dTotalMB/1000000.0 << " MB in "
        << dTotaluSec/1000000.0 << " seconds for "
        << dTotalRate << " MB/s" << endl;

        return 0;

} // main()
