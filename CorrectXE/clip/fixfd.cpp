//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ClipAPI.h"

//---------------------------------------------------------------------------

#pragma argsused
//---------------------------------------------------------------------------

void PrintUsage(char *cpArg)
{
  cerr << "Usage: " << "fixfd <clip name>" << endl;
}

//---------------------------------------------------------------------------

int main(int argc, char* argv[])
{
    int iIn;
    istringstream is("1");
    is >> iIn;
    
   CreateApplicationName(argv[0]);
   
   if (argc < 2)
      {
      PrintUsage (argv[0]);
      return(1);
      }

   CBinManager binMgr;
   int retVal;

   auto clip = binMgr.openClip(argv[1], &retVal);
   if (retVal != 0)
      {
      cout << "ERROR: Cannot open clip " << endl
           << "       " << argv[1] << endl
           << "       return code = " << retVal << endl;
      return 1;
      }

   retVal = clip->fixPALFieldDominance(0);
   if (retVal != 0)
      {
      cout << "ERROR: Field Dominance fix failed, error code = " << retVal << endl;
      }

   return 0;
}
//---------------------------------------------------------------------------
 