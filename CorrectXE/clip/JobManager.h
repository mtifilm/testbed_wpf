#ifndef JOBMANAGERH
#define JOBMANAGERH

#include "cliplibint.h"
#include "JobInfo.h"

#include <string>
using std::string;

class MTI_CLIPLIB_API JobManager
{
public:

   typedef enum
   {
      unknownType,
      dewarpType,
      masksType,
      pdlsType,
      reportsType,
      cutListsType,
      stabilizeType,
   } JobFolderType;

   void SetMarkStrings(const string &in, const string &out);
   void SetClipInOutStrings(const string &in, const string &out);
   void SetCurrentToolCode(const string &toolCode);

   void SetMasterClipFolderPath(const string &path);
   string GetMasterClipFolderPath();
   string GetJobRootFolderPath();

   int CreateJobFolders(const string &rootFolderPath, const string &jobFolderName, const JobInfo &jobInfo);
   bool DoesJobHaveJobFolderSubfolder(JobFolderType jobFolderType);
   string GetJobFolderSubfolderPath(JobFolderType jobFolderType);
   string GetJobFolderFileSavePath(const string &jobFolder, const string &acronym, bool useClipInOut, const string &extension);
   string GetJobFolderFileSaveName(const string &jobFolder, const string &acronym, bool useClipInOut, const string &extension);
   string GetJobFolderFileLoadPath(const string &jobFolder, const string &acronym, bool useClipInOut, const string &extension);
   string GetJobFolderFileLoadName(const string &jobFolder, const string &acronym, bool useClipInOut, const string &extension);
   string GetUnversionedJobFolderFilePath(const string &id, const string &jobFolder, const string &extension);
   string GetUnversionedJobFolderFileName(const string &id, const string &jobFolder, const string &extension);

   string GetReelName();

private:

   int MakeJobFolderDirectories(const string &jobBinPath, const string &jobFolderName, const JobInfo &jobInfo, bool wantReelDirectories = true);
   int MakeMediaFolderDirectories(const string &mediaFolderPath, const string &mediaId, const JobInfo &jobInfo);
   int MakeReelDirectories(const string &rootPath, const string &directoryNamePrefix, const JobInfo &jobInfo);
   int MakeOneReelBin(const string &rootPath, const string &binName, const string &reelName);
   int MakeReelBins(const string &rootPath, const string &binNamePrefix, const JobInfo &jobInfo);
   string ReadReelNameFromJobIniFile(const string &startingPath);
   void WriteReelNameToJobIniFile(const string &binPath, const string &reelName);
   string FindJobRootFolder(const string &startingPath);
   string GetJobFolderFileName(const string &jobFolder, const string &acronym, bool useClipInOut, const string &extension, bool loading);

   static string MarkInString;
   static string MarkOutString;
   static string ClipInString;
   static string ClipOutString;
   static string CurrentToolCode;
   static string MasterClipFolderPath;
};

#endif

