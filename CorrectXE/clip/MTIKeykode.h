// MTIKeykode.h:  MTI Keykode
//
// Created by: John Starr, March 14, 2006
//             Extracted from Clip3TimeTrack.h
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////

#ifndef MTIKEYKODEH
#define MTIKEYKODEH

#include "cliplibint.h"
#include "IniFile.h"

///////////////////////////////////////////////////////////////////////////////
// Forward Declarations


///////////////////////////////////////////////////////////////////////////////

enum EKeykodeFilmDimension
{
   KEYKODE_FILM_DIMENSION_INVALID = 0,
   KEYKODE_FILM_DIMENSION_35MM_4_PERF = 1,         // 4 perfs per frame
   KEYKODE_FILM_DIMENSION_35MM_3_PERF = 2,         // 3 perfs per frame
   KEYKODE_FILM_DIMENSION_16MM_1_PERF = 3,         // 1 perf per frame
   KEYKODE_FILM_DIMENSION_35MM_4_PERF_REVERSE = 4,         // -4 perfs per frame
   KEYKODE_FILM_DIMENSION_35MM_3_PERF_REVERSE = 5,         // -3 perfs per frame
   KEYKODE_FILM_DIMENSION_16MM_1_PERF_REVERSE = 6,         // -1 perf per frame
   KEYKODE_FILM_DIMENSION_35MM_FRAME = 7,         // 16 frames per foot
};

enum EKeykodeFormat
{
   MTI_KEYKODE_FORMAT_RAW,             // 20 characters, unformatted
   MTI_KEYKODE_FORMAT_KEYKODE_PERFS,   // KZ 23 5678 2000+45    feet+perfs
   MTI_KEYKODE_FORMAT_KEYKODE,         // KZ 23 5678 2000+21    feet+frames
   MTI_KEYKODE_FORMAT_KEYKODE_DOT_PERFS, // KZ 23 5678 2000+21.3 feet+frames.perf
   MTI_KEYKODE_FORMAT_FEET_PLUS_FRAMES, // Feet+Frames
   MTI_KEYKODE_FORMAT_FEET_PLUS_FRAMES_DOT_PERFS, // Feet+Frames.Perf
   MTI_KEYKODE_FORMAT_RP215,  // 14 char are raw, 2 are frame, 2 are format
};

enum EKeykodeStatus
{
  MTI_KEYKODE_STATUS_NONE,    	// no new keykode read
  MTI_KEYKODE_STATUS_RAW_READ,	// the reader has delivered a KK, but it is 
				// not reliable
  MTI_KEYKODE_STATUS_CONFIRMED_READ,   // the KK has been confirmed good
};

///////////////////////////////////////////////////////////////////////////////

// Positions and lengths of the elements within a keykode string
#define MTI_KEYKODE_MFG_POSITION       0
#define MTI_KEYKODE_MFG_LENGTH         2
#define MTI_KEYKODE_FILM_TYPE_POSITION (MTI_KEYKODE_MFG_POSITION+MTI_KEYKODE_MFG_LENGTH)
#define MTI_KEYKODE_FILM_TYPE_LENGTH   2
#define MTI_KEYKODE_PREFIXA_POSITION    (MTI_KEYKODE_FILM_TYPE_POSITION+MTI_KEYKODE_FILM_TYPE_LENGTH)
#define MTI_KEYKODE_PREFIXA_LENGTH      2
#define MTI_KEYKODE_PREFIXB_POSITION    (MTI_KEYKODE_PREFIXA_POSITION+MTI_KEYKODE_PREFIXA_LENGTH)
#define MTI_KEYKODE_PREFIXB_LENGTH      4
#define MTI_KEYKODE_FEET_POSITION      (MTI_KEYKODE_PREFIXB_POSITION+MTI_KEYKODE_PREFIXB_LENGTH)
#define MTI_KEYKODE_FEET_LENGTH        4
#define MTI_KEYKODE_PERFS_POSITION     (MTI_KEYKODE_FEET_POSITION+MTI_KEYKODE_FEET_LENGTH)
#define MTI_KEYKODE_PERFS_LENGTH       2

#define MTI_KEYKODE_LENGTH 20          // Total length of keykode string

///////////////////////////////////////////////////////////////////////////////
// Singleton class to hold in-memory tables of Film Emulsion Codes
// as found in film-edge Keykodes
//
// There are two levels of tables.  First level is the manufacturer's code
// and the second level is the film type.  The manufacturer's code is not
// unique for Eastman Kodak.

class MTI_CLIPLIB_API CMTIKeykodeFilmTypeTable
{
public:
   CMTIKeykodeFilmTypeTable();
   ~CMTIKeykodeFilmTypeTable();

   string lookup(char *fecStr);
   string getManufacturer (char *fecStr);
   int setNewCode (char *fecStr, char *cpDesc);
   int getNumDesc ();
   string getDesc (int i);
   string getCode (const string &strDesc);

private:
   // Private data types

   // Film Type lookup table entry
   struct SFECEntry
   {
      string filmCode;    // Emulsion Code digits (as 2 ASCII digits)
      string filmType;    // Manuf + Film Type letters (as 2 ASCII characters)
   };
   typedef vector<SFECEntry> FECTable;

   struct SFilmTypeTable
   {
      string tableName;
      FECTable fecTable;
   };
   typedef vector<SFilmTypeTable*> FilmTypeTableList;

   // Manufacturer lookup table entry
   struct SManufEntry
   {
      string manufName;    // Manufacturer's name, used as part of
                           // ini file section name [Manufacturer_<manufName>]
      string manufCode;    // Manufacturer's code (as 2 ASCII digits)
      char manufChar;      // Manuf's letter (as single ASCII character)
      SFilmTypeTable *filmTypeTable;   // Pointer to film type table for
                                 // this manufacturer
   };
   typedef vector<SManufEntry> ManufTable;

   // Recent search table entry
   struct SRecentSearch
   {
      string fecStr;
      string filmType;
   };

private:
   // Private functions

   CIniFile * openIniFile (int &iRet);
   int readIniFile();
   void readFilmTypeTable(CIniFile *iniFile, const string &sectionName);
   void readManufTable(CIniFile *iniFile);
   SFilmTypeTable* findFilmTypeTable(const string &filmTypeTableName);

   void Clear();

private:
   // Private data
   // All data members should all be static since this is a singleton class

   static bool iniFileRead;

   static ManufTable* manufTable;          // Manuf lookup table
   static FilmTypeTableList *allFilmTypeTables;  // Table of Film Type lookup tables

   // Cached table of recent search results
   static vector<SRecentSearch> *recentSearchList;

   // text string for KeykodeFilmTypes.ini
   static string mtiLocalMachineFileSection;
   static string mtiLocalMachineFileKey;
   static string defaultFileName;
   static string manufacturerCodesSectionName;
   static string manufacturerSectionPrefix;
   static string filmTypeTableSectionPrefix;
   static string filmTypeTableKey;
   static string manufacturerLetterKey;
};

///////////////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CMTIKeykode
{
public:
   CMTIKeykode();
   CMTIKeykode(const CMTIKeykode &srcKK);
//   explicit CMTIKeykode(const string &srcKKStr);
   virtual ~CMTIKeykode();

   CMTIKeykode& operator=(const CMTIKeykode &srcKK);
//   CMTIKeykode& operator=(const string &newKKStr)
//      {setKeykodeStr(newKKStr); return *this;};

   int getCalibrationOffset() const;
   EKeykodeFilmDimension getFilmDimension() const;
   string getFilmTypeStr();
   string getFormattedStr(EKeykodeFormat formatType, bool overrideInterp=false);
   bool getInterpolateFlag() const;
   MTI_INT64 getKeykodeFeet64() const;
   MTI_INT64 getKeykodeFrames64() const;
   int getKeykodePerfs() const;
   string getKeykodeStr() const {return string(m_keykodeString);};
   const char* getKeykodeCStr() {return m_keykodeString;};
   MTI_INT64 getPerfCount64() const;

   int getPerfsPerFoot() const;
   static int getPerfsPerFoot(EKeykodeFilmDimension newFilmDimension);
   int getPerfsPerFrame() const;
   static int getPerfsPerFrame(EKeykodeFilmDimension newFilmDimension);

   bool isDummyValue() const;
   bool isEqualTo(const CMTIKeykode &rhs) const;

   void addFrameOffset(int frameOffset);

   void setCalibrationOffset(int newOffset);
   void setFilmDimension(EKeykodeFilmDimension newFilmDimension);
   void setInterpolateFlag(bool newInterpolateFlag);
   void setKeykodeFeet64(MTI_INT64 newFeet);
   void setKeykodePerfs(int newPerfs);
   void setKeykodeStr(const string &newKKStr);
   void setKeykodeStr(const char *newKKStr);
   void setRawKeykode(const string &newKKStr, int interpolationOffset);
   void setRawKeykode(const char *newKKStr, int interpolationOffset);
   void setToDummyValue();
   void setPerfCount64(MTI_INT64 newPerfCount);

private:
   void init();
   void adjustForCalibrationOffset(int interpolationOffset);

private:
   int m_calibrationOffset;   // Calibration offset in perfs
   EKeykodeFilmDimension m_filmDimension;
   bool m_interpolateFlag;

   char m_keykodeString[MTI_KEYKODE_LENGTH+1];
   char m_keykodeUnknown[MTI_KEYKODE_LENGTH+1];

};

///////////////////////////////////////////////////////////////////////////////

#endif // #ifndef MTI_KEYKODE_H

