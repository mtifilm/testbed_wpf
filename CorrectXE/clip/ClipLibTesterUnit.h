//---------------------------------------------------------------------------

#ifndef ClipLibTesterUnitH
#define ClipLibTesterUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include <ComCtrls.hpp>

#include <string>
using std::string;

#include "Clip3TimeTrack.h"
class CClip;
class CTimeTrack;
class CBinMgrGUIInterface;

//---------------------------------------------------------------------------
class TClipLibTesterForm : public TForm
{
__published:	// IDE-managed Components
   TButton *OpenClipButton;
   TLabeledEdit *CurrentClipName;
   TMainMenu *MainMenu;
   TMenuItem *File1;
   TMenuItem *Edit1;
   TMenuItem *View1;
   TMenuItem *ShowClipFileMenuItem;
   TMenuItem *ExitMenuItem;
   TPageControl *PageControl;
   TTabSheet *VideoSheet;
   TTabSheet *AudioSheet;
   TTabSheet *TimeSheet;
   TListView *TimeTrackListView;
   TMenuItem *CloseClipMenuItem;
   TMenuItem *N1;
   TMenuItem *OpenClipMenuItem;
   TMenuItem *N2;
   TMenuItem *ReloadClipMenuItem;
   TGroupBox *TimecodeTrackGroup;
   TGroupBox *SMPTETimecodeTrackGroup;
   TGroupBox *KeykodeTrackGroup;
   TGroupBox *EventTrackGroup;
   TListView *EventFrameList;
   TListView *KeykodeFrameList;
   TListView *TimecodeFrameList;
   TLabel *TimecodeTrackIndexLabel;
   TLabel *TimecodeTrackIndexValue;
   TLabel *TimecodeTrackAppIdLabel;
   TLabel *TimecodeTrackAppIdValue;
   TButton *SetTimecodesButton;
   TButton *ClearTimecodesButton;
   TLabel *TimecodeTrackFrameCountLabel;
   TLabel *TimecodeTrackFrameCountValue;
   TButton *UpdateTimeTrackFileButton;
   TComboBox *UpdateGroupSize;
   TButton *UseFrameIndicesButton;
   TRadioGroup *KeykodeDisplayStyle;
   TButton *SetKeykodesButton;
   TLabeledEdit *FrameNumberEdit;
   TLabeledEdit *SampleOffsetEdit;
   TLabeledEdit *SampleCountEdit;
   TButton *Button1;
   void __fastcall OpenClipButtonClick(TObject *Sender);
   void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
   void __fastcall ShowClipFileMenuItemClick(TObject *Sender);
   void __fastcall ExitMenuItemClick(TObject *Sender);
   void __fastcall ReloadClipMenuItemClick(TObject *Sender);
   void __fastcall CloseClipMenuItemClick(TObject *Sender);
   void __fastcall TimeTrackListViewDblClick(TObject *Sender);
   void __fastcall SetTimecodesButtonClick(TObject *Sender);
   void __fastcall ClearTimecodesButtonClick(TObject *Sender);
   void __fastcall UseFrameIndicesButtonClick(TObject *Sender);
   void __fastcall KeykodeDisplayStyleClick(TObject *Sender);
   void __fastcall SetKeykodesButtonClick(TObject *Sender);
   void __fastcall Button1Click(TObject *Sender);
   
private:	// User declarations
   string currentClipName;
   CClip *currentClip;
   int currentTimeTrackIndex;
   CTimeTrack *currentTimeTrack;
   CBinMgrGUIInterface *binMgrGUIInterface;

   void UpdateTimeTrackSheet(CClip *clip);
   void UpdateTimeTrackList(CClip *clip);
   void AddTimeTrackToList(TListView *list, int itemIndex,
                           CTimeTrack *timeTrack);
   void OpenClipByName(const string newClipName);
   void CloseCurrentClip();
   void UpdateClipInfoDisplay();
   
   void ShowTimeTrackGroup(ETimeType timeType);
   void UpdateTimeTrackGroup(int timeTrackIndex,CTimeTrack *timeTrack);
   void UpdateEventTimeTrackGroup(int timeTrackIndex,CTimeTrack *timeTrack);
   void UpdateKeykodeTimeTrackGroup(int timeTrackIndex,CTimeTrack *timeTrack);
   void UpdateKeykodeFrameList(CTimeTrack *timeTrack);
   void AddKeykodeFrameToList(TListView *list, int frameIndex,
                              CKeykodeFrame *frame);
   void UpdateSMPTETimecodeTimeTrackGroup(int timeTrackIndex,CTimeTrack *timeTrack);
   void UpdateTimecodeTimeTrackGroup(int timeTrackIndex,CTimeTrack *timeTrack);
   void UpdateTimecodeFrameList(CTimeTrack *timeTrack);
   void AddTimecodeFrameToList(TListView *list, int frameIndex,
                               CTimecodeFrame *frame);

public:		// User declarations
   __fastcall TClipLibTesterForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TClipLibTesterForm *ClipLibTesterForm;
//---------------------------------------------------------------------------
#endif
