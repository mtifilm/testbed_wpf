#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "Bridge.h"
#include "ClipAPI.h"
#include "Flist.h"
#include "IniFile.h"

#define ENCRYPT_FACTOR 13

//--------------------------------------------------------------------
CBridge::CBridge()
{
   majorState = BRIDGE_STATE_CLOSED;

   // no master file lines allocated yet
   for (int i=0;i<MTI_MASTER_LINES;i++) 
      mline[i] = NULL;

   // no free blk storage yet 
   freeBlk = NULL;

   // master file not yet read
   mtiMasterLines = 0;

   // no storage waiting
   newLocation = -1;
   newBlocks   = -1;
}

CBridge::~CBridge()
{
   if (majorState == BRIDGE_STATE_OPEN) {

      // free any storage associated with free block entries
      delete [] freeBlk;

      // free any storage associated with the master file
      for (int i=0;i<mtiMasterLines+1;i++)
         delete [] mline[i];
   }
}

//--------------------------------------------------------------------

int CBridge::makeScratchDirectories(int iClipID)
{
#ifdef __sgi
   char dir1[256];
   char dir2[256];
   DIR *dirp;

   sprintf(dir1,"%s%s/%05d",environmentDir,
                            "/.mti_scratch",
                            iClipID);

   sprintf(dir2,"%s/%s",dir1,
                        ".pro");

   if ((dirp=opendir(dir1))==NULL) { // missing
      if (mkdir(dir1,00775)==-1)
         return(OGLV_BRIDGE_ERR_FAILURE_MAKING_SCRATCH_DIRS);
      if (mkdir(dir2,00775)==-1)
         return(OGLV_BRIDGE_ERR_FAILURE_MAKING_SCRATCH_DIRS);
   }
   else { // exists
      closedir(dirp);
      if ((dirp=opendir(dir2))==NULL) {
         if (mkdir(dir2,00775)==-1)
            return(OGLV_BRIDGE_ERR_FAILURE_MAKING_SCRATCH_DIRS);
      }
      else
         closedir(dirp);
   } 
#endif

   return(0);
}

//--------------------------------------------------------------------
int CBridge::removeScratchDirectories(int iClipID)
{
#ifdef __sgi
   char dir[256];
   char cmd[256];
   DIR *dirp;

   sprintf(dir,"%s%s/%05d",environmentDir,
                           "/.mti_scratch",
                           iClipID);

   if ((dirp=opendir(dir)) != NULL) { // dir exists
      closedir(dirp);
      sprintf(cmd,"%s %s","/bin/rm -r",
			  dir);
      if (system(cmd) != 0)
         return(OGLV_BRIDGE_ERR_FAILURE_REMOVING_SCRATCH_DIRS);
   }
#endif

   return(0);
}

//--------------------------------------------------------------------

int CBridge::makeCadenceDirectory(int iClipId)
{
#ifdef __sgi
   char dir[256];
   DIR *dirp;

   sprintf(dir,"%s%s/%05d",defaultImageDir,
                           "/.mti_cadence",
                           iClipId);

   if ((dirp=opendir(dir))==NULL) { // missing
      if (mkdir(dir,00775)==-1)
         return(OGLV_BRIDGE_ERR_FAILURE_MAKING_CADENCE_DIR);
   }
   else // exists
      closedir(dirp);
#endif

   return(0);
}

//--------------------------------------------------------------------

int CBridge::removeCadenceDirectory(int iClipId)
{
#ifdef __sgi
   char dir[256];
   char cmd[256];
   DIR *dirp;

   sprintf(dir,"%s%s/%05d",defaultImageDir,
                           "/.mti_cadence",
                           iClipId);

   if ((dirp=opendir(dir)) != NULL) { // dir exists
      closedir(dirp);
      sprintf(cmd,"%s %s","/bin/rm -r",
                          dir);
      if (system(cmd) != 0)
         return(OGLV_BRIDGE_ERR_FAILURE_REMOVING_CADENCE_DIR);
   }
#endif

   return(0);
} 

//--------------------------------------------------------------------
int CBridge::getLin(
                     FILE *fp,     // opened file to be read from
                     char *linbuf, // buffer to recv line
                     int leng      // length of line buffer
                   )
{
   int inchr;

   int cnt = 0;
   while (((inchr=getc(fp))!=EOF)&&(inchr!='\n'))
   {
      if ((inchr!=0)&&(cnt < (leng-1))) {
         *linbuf++ = (char)inchr;
         cnt++;
      }
   }
   *linbuf = 0;
   if ((cnt==0)&&(inchr==-1)) return(-1);
   return(cnt);
}

//--------------------------------------------------------------------
int CBridge::getBinHeader(
                          FILE *fp,     // opened file to be read from
                          BINHEADER *bh // bin header to recv data
                        )
{
   int i;
   char lineBuf[LINLENG];

   if (getLin(fp,lineBuf,LINLENG)==-1) return(-1);

   // capture the date at the end
   char *pos = lineBuf;
   while (*pos!=0) pos++;
   while (*(--pos)==' ');
   while (*(--pos)!=' ');
   while (*(--pos)==' ');
   while (*(--pos)!=' ');
   while (*(--pos)==' ');
   while (*(--pos)!='\t');
   *pos = 0;
   pos++;
   if (sscanf(pos,"%s %s %s",
              &bh->Month[0],&bh->Day[0],&bh->Year[0]) != 3)
      return(-1);

   // the day field must be two digits wide (leading space)
   if (bh->Day[1]==',') {
      bh->Day[1] = bh->Day[0];
      bh->Day[0] = ' ';
   }
   bh->Day[2] = 0;

   // find the end of the name string and terminate it
   pos--;
   while (*(--pos)!='\t');
   *pos = 0;

   // copy the BinName and terminate it
   pos++;
   i = 0;
   while ((*pos!=0)&&(i<31))
      bh->BinName[i++] = *pos++;
   bh->BinName[i] = 0;

   // the lineBuf contents are now terminated before BinName
   if (sscanf(lineBuf,"Clip=%d NClip=%d Crypt=%u BinID=%d",
       &(bh->Clip),&(bh->NClip),&(bh->Crypt),&(bh->BinID)) != 4)
      return(-1);

   // the second line is easier to grab
   if (fscanf(fp,"RecToComOpt=%d %d %d %d Reserved=%d %d %d %d",
       &bh->RecToComOpt[0],&bh->RecToComOpt[1],
       &bh->RecToComOpt[2],&bh->RecToComOpt[3],
       &bh->Reserved[0],&bh->Reserved[1],
       &bh->Reserved[2],&bh->Reserved[3]) !=8)
      return(-1);

   return(0);
}

//--------------------------------------------------------------------
int CBridge::putBinHeader(
                           FILE *fp,     // opened file to be written
                           BINHEADER *bh // bin header to send data
                          )
{
   if (fprintf(fp,"Clip=%d NClip=%d Crypt=%u BinID=%d\t%s\t%s %s, %s\n",
               bh->Clip,bh->NClip,bh->Crypt,bh->BinID,
               bh->BinName,bh->Month,bh->Day,bh->Year) < 0)
      return(-1);

   if (fprintf(fp,"RecToComOpt=%d %d %d %d Reserved=%d %d %d %d\n",
               bh->RecToComOpt[0],bh->RecToComOpt[1],
               bh->RecToComOpt[2],bh->RecToComOpt[3],
               bh->Reserved[0],bh->Reserved[1],
               bh->Reserved[2],bh->Reserved[3]) < 0)
      return(-1);

   return(0);
}

//--------------------------------------------------------------------
int CBridge::getClipRecord(
                           FILE *fp,      // opened file to be read from
			   CLIPRECORD *cr //lip record to recv data
                         )
{
   char lineBuf[128];

   // 1st line
   if (fscanf(fp,"%d:%d:%d:%d %d:%d:%d:%d Format=%d Status=%d Demo=%d ClipID=%d\n",
                 &cr->HoursIn,&cr->MinutesIn,&cr->SecondsIn,&cr->FramesIn,
                 &cr->HoursOut,&cr->MinutesOut,&cr->SecondsOut,&cr->FramesOut,
                 &cr->Format,&cr->Status,&cr->Demo,&cr->ClipID)!=12)
      return(-1);

   // 2nd line
   if (fscanf(fp,"Fix=%d Secs=%d %s %s %s\n",
                 &cr->Fix,&cr->Secs,cr->Month,cr->Day,cr->Year)!=5)
      return(-1);

   // the day field must be two digits wide (leading space) 
   if (cr->Day[1]==',') {
      cr->Day[1] = cr->Day[0];
      cr->Day[0] = ' ';
   }
   cr->Day[2] = 0;

   // 3rd line 
   if (fscanf(fp,"NItem=%d Location=%d ItemSize=%d ClipSize=%d\n",
                 &cr->NItem,&cr->Location,&cr->ItemSize,&cr->ClipSize)!=4)
      return(-1);

   // 4th line
   if (fscanf(fp,"Bits=%d PixelVals=%d Stride=%d RowReversed=%d\n",
                 &cr->Bits,&cr->PixelVals,&cr->Stride,&cr->RowReversed)!=4)
      return(-1);

   // 5th line
   if (fscanf(fp,"ValScale=%d NumPixelPerRow=%d NumRowsPerItem=%d RecInfo=%d",
                 &cr->ValScale,&cr->NumPixelPerRow,&cr->NumRowsPerItem,
                 &cr->RecInfo)!=4)
      return(-1);


   // 6th line
   while (getc(fp)!='\n');

   if (getc(fp)!='\t')
      return(-1);
   if (fscanf(fp,"Marks=%d:%d:%d:%d %d:%d:%d:%d",
                 &cr->HoursMarkIn,&cr->MinutesMarkIn,&cr->SecondsMarkIn,&cr->FramesMarkIn,
                 &cr->HoursMarkOut,&cr->MinutesMarkOut,&cr->SecondsMarkOut,&cr->FramesMarkOut)!=8)
      return(-1);

   // 7th line
   while (getc(fp)!='\n');

   if (getc(fp)!='\t')
      return(-1);
   if (getc(fp)!='\t')
      return(-1);
   if (getLin(fp,lineBuf,128)==-1)
      return(-1);

   strcpy(cr->Name,lineBuf);

   return(0);
}

//--------------------------------------------------------------------
int CBridge::getBlocksOccupiedByClip(CLIPRECORD *cr)
{
   if (((cr->Format & IF_NORMAL_FORMAT_MASK) == IF_D16_YUV_FRAME)
     ||((cr->Format & IF_NORMAL_FORMAT_MASK) == IF_D4_YUV_FRAME)
     ||((cr->PixelVals == PIXELVALS_YUV_422_10BIT_2_5)
      &&((cr->RecInfo & RECINFO_10BIT_NO_GAP)==0))) 

      return(cr->ClipSize);

    int isize = cr->ItemSize;
    if (cr->RecInfo & RECINFO_KEEP_VERTICAL_INTERVAL)
       isize += BLOCKS_PER_FIELD_VERTICAL_INTERVAL;

    return(isize*cr->NItem);
}

//--------------------------------------------------------------------
FREEBLOCK* CBridge::sortFreeBlocks(
                           FREEBLOCK *loptr, FREEBLOCK *hiptr)
{
   FREEBLOCK *midptr, *chn1, *chn2, *temp, *head, *tail;

   if (loptr==hiptr) {
      loptr->nxt = NULL;
      return loptr;
   }

   if ((hiptr - loptr)==1) {
      if (loptr->beg < hiptr->beg) {
         loptr->nxt = hiptr;
         hiptr->nxt = NULL;
         return loptr;
      }
      else {
         hiptr->nxt = loptr;
         loptr->nxt = NULL;
         return hiptr;
      }
   }

   /* split into two halves and sort separately */
   midptr = loptr + (hiptr - loptr)/2;
   chn1 = sortFreeBlocks(loptr,midptr);
   chn2 = sortFreeBlocks(midptr+1,hiptr);

   /* define the head of the merged chain */
   if (chn2->beg < chn1->beg) {
      temp = chn1;
      chn1 = chn2;
      chn2 = temp;
   }
   head = tail = chn1;
   chn1 = chn1->nxt;

   /* as above, CHN1 is always the chain that advances */
   while (chn1 != NULL) {
      if (chn2->beg < chn1->beg) {
         temp = chn1;
         chn1 = chn2;
         chn2 = temp;
      }
      tail->nxt = chn1;
      tail = chn1;
      chn1 = chn1->nxt;
   }
   tail->nxt = chn2;

   return head;

}

//--------------------------------------------------------------------
int CBridge::convert2d(int pos, char *out, int val)
{
   if (val < 0) {
      out[pos++] = '-';
      val = -val;
   }
   out[pos++] = ((val / 10)%10) + '0';
   out[pos++] =  (val % 10) + '0';
   return(pos);
}

//--------------------------------------------------------------------
void CBridge::convertTimecode(char *out,
                              int hrs, int mins, int secs, int frms)
{
   int plc = 0;
   plc = convert2d(plc,out, hrs); out[plc++] = ':';
   plc = convert2d(plc,out,mins); out[plc++] = ':';
   plc = convert2d(plc,out,secs); out[plc++] = ':';
   plc = convert2d(plc,out,frms); out[plc++] = 0;
}

//--------------------------------------------------------------------
int CBridge::putClipRecord(FILE *fp, CLIPRECORD *cr)
{
   if (fprintf(fp,"%02d:%02d:%02d:%02d %02d:%02d:%02d:%02d Format=%d Status=%d Demo=%d ClipID=%6d\n",
              cr->HoursIn,cr->MinutesIn,cr->SecondsIn,cr->FramesIn,
              cr->HoursOut,cr->MinutesOut,cr->SecondsOut,cr->FramesOut,
              cr->Format,cr->Status,cr->Demo,cr->ClipID) < 0)
      return(-1);

   if (fprintf(fp,"Fix=%6d Secs=%6d %s %s, %s\n",
              cr->Fix,cr->Secs,cr->Month,cr->Day,cr->Year) < 0)
      return(-1);

   // note that 0 is always output for 'ClipSize' - not the input value in cr
   if (fprintf(fp,"NItem=%6d Location=%10d ItemSize=%6d ClipSize=%10d\n",
              cr->NItem,cr->Location,cr->ItemSize,0) < 0)
      return(-1);

   if (fprintf(fp,"Bits=%2d PixelVals=%2d Stride=%6d RowReversed=%2d\n",
              cr->Bits,cr->PixelVals,cr->Stride,cr->RowReversed) < 0)
      return(-1);

   if (fprintf(fp,"ValScale=%2d NumPixelPerRow=%6d NumRowsPerItem=%6d RecInfo=%3d\n",
              cr->ValScale,cr->NumPixelPerRow,cr->NumRowsPerItem,cr->RecInfo) < 0)
      return(-1);

   char MarkIn[16]; 
   convertTimecode(MarkIn,cr->HoursMarkIn,cr->MinutesMarkIn,
                          cr->SecondsMarkIn,cr->FramesMarkIn);

   char MarkOut[16];
   convertTimecode(MarkOut,cr->HoursMarkOut,cr->MinutesMarkOut,
                           cr->SecondsMarkOut,cr->FramesMarkOut);

   if (fprintf(fp,"\tMarks=%s %s\n",MarkIn,MarkOut) < 0)
      return(-1);

   if (fprintf(fp,"\t\t%s\n", cr->Name) < 0)
      return(-1);

   return(0);
}

//--------------------------------------------------------------------
// defaults file path is usually "/usr/people/drs/.mti_defaults"
// but we will look for it in the current directory (see below)

//static char *MTIDefaults = "./.mti_defaults";
static const char *MTIDefaults = "/usr/people/drs/.mti_defaults";

int CBridge::getMTIDefaults()
{
   FILE *fpDefault;
   char lineBuf[PTHLENG], *pos;

   // get environment directory
   *environmentDir = 0;
   fpDefault = fopen(MTIDefaults, "r");
   if (fpDefault==NULL)
      return(OGLV_BRIDGE_ERR_MTI_DEFAULTS_OPEN_FAILURE);

   // find the '[Directory]' heading
   pos = NULL;
   while (true)
   {
      if (getLin(fpDefault,lineBuf,PTHLENG)==-1) break;
      if ((pos=strstr(lineBuf,"[Directory]"))!=NULL) break;
   }
   if (pos==NULL) {
      fclose(fpDefault);
      return(OGLV_BRIDGE_ERR_NO_DIRECTORY_HEADING_IN_MTI_DEFAULTS);
   }

   // look for the 'EnvironmentDirectory' entry between the heading and the next '['
   pos = NULL;
   while (true)
   {
      if (getLin(fpDefault,lineBuf,PTHLENG)==-1) break;
      if ((pos=strstr(lineBuf,"["))!=NULL) break;
      if ((pos=strstr(lineBuf,"EnvironmentDirectory="))!=NULL) break;
   }

   if ((pos==NULL)||(*pos=='[')||
       (sscanf(lineBuf,"EnvironmentDirectory=%s",environmentDir)!=1)) {

      fclose(fpDefault);
      return(OGLV_BRIDGE_ERR_NO_ENVIRONMENT_DIR_IN_MTI_DEFAULTS);
   }

   fclose(fpDefault);

   // get the raw video directory
   *rawVideoDir = 0;
   fpDefault = fopen(MTIDefaults, "r");
   if (fpDefault==NULL)
      return(OGLV_BRIDGE_ERR_MTI_DEFAULTS_OPEN_FAILURE);
 
   // find the '[Directory]' heading
   pos = NULL;
   while (true)
   {
      if (getLin(fpDefault,lineBuf,PTHLENG)==-1) break;
      if ((pos=strstr(lineBuf,"[Directory]"))!=NULL) break;
   }
   if (pos==NULL) {
      fclose(fpDefault);
      return(OGLV_BRIDGE_ERR_NO_DIRECTORY_HEADING_IN_MTI_DEFAULTS);
   }

   // look for the 'RawVideoDirectory' entry between the heading and the next '['
   pos = NULL;
   while (true)
   {
      if (getLin(fpDefault,lineBuf,PTHLENG)==-1) break;
      if ((pos=strstr(lineBuf,"["))!=NULL) break;
      if (strncmp(lineBuf, "R", 1) == 0)
       {
        if ((pos=strstr(lineBuf,"RawVideoDirectory="))!=NULL) break;
       }
   }
   if ((pos==NULL)||(*pos=='[')||
       (sscanf(lineBuf,"RawVideoDirectory=%s",rawVideoDir)!=1)) {

      fclose(fpDefault);
      return(OGLV_BRIDGE_ERR_NO_RAW_VIDEO_DIR_IN_MTI_DEFAULTS);
   }

   fclose(fpDefault);

   // get the default image directory
   *defaultImageDir = 0;
   fpDefault = fopen(MTIDefaults, "r");
   if (fpDefault==NULL)
      return(OGLV_BRIDGE_ERR_MTI_DEFAULTS_OPEN_FAILURE);

   // find the '[Directory]' heading
   pos = NULL;
   while (true)
   {
      if (getLin(fpDefault,lineBuf,PTHLENG)==-1) break;
      if ((pos=strstr(lineBuf,"[Directory]"))!=NULL) break;
   }
   if (pos==NULL) {
      fclose(fpDefault);
      return(OGLV_BRIDGE_ERR_NO_DIRECTORY_HEADING_IN_MTI_DEFAULTS);
   }

   // look for the 'DefaultImageDirectory' entry between the heading and the next '['
   pos = NULL;
   while (true)
   {
      if (getLin(fpDefault,lineBuf,PTHLENG)==-1) break;
      if ((pos=strstr(lineBuf,"["))!=NULL) break;
      if (strncmp(lineBuf, "D", 1) == 0)
       {
        if ((pos=strstr(lineBuf,"DefaultImageDirectory="))!=NULL) break;
       }
   }
   if ((pos==NULL)||(*pos=='[')||
       (sscanf(lineBuf,"DefaultImageDirectory=%s",defaultImageDir)!=1)) {

      fclose(fpDefault);
      return(OGLV_BRIDGE_ERR_NO_DEFAULT_IMAGE_DIR_IN_MTI_DEFAULTS);
   }

   fclose(fpDefault);

   // get the raw disk size
   rawDiskSize = 0;
   fpDefault = fopen(MTIDefaults, "r");
   if (fpDefault==NULL)
      return(OGLV_BRIDGE_ERR_MTI_DEFAULTS_OPEN_FAILURE);
   
   // find the '[Hardware]' heading
   pos = NULL;
   while (true)
   {
      if (getLin(fpDefault,lineBuf,PTHLENG)==-1) break;
      if ((pos=strstr(lineBuf,"[Hardware]"))!=NULL) break;
   }
   if (pos==NULL) {
      fclose(fpDefault);
      return(OGLV_BRIDGE_ERR_NO_HARDWARE_HEADING_IN_MTI_DEFAULTS);
   }

   // look for the 'RawDiskSize' entry between the heading and the next '['
   pos = NULL;
   while (true)
   {
      if (getLin(fpDefault,lineBuf,PTHLENG)==-1) break;
      if ((pos=strstr(lineBuf,"["))!=NULL) break;
      if (strncmp(lineBuf, "R", 1) == 0)
       {
        if ((pos=strstr(lineBuf,"RawDiskSize="))!=NULL) break;
       }
   }
   if ((pos==NULL)||(*pos=='[')||
       (sscanf(lineBuf,"RawDiskSize=%d",&rawDiskSize)!=1)) {
      fclose(fpDefault);
      return(OGLV_BRIDGE_ERR_NO_RAW_DISK_SIZE_IN_MTI_DEFAULTS);
   }

   fclose(fpDefault);

   // get the fast IO boundary
   fastIOBoundary = 0;
   fpDefault = fopen(MTIDefaults, "r");
   if (fpDefault==NULL)
      return(OGLV_BRIDGE_ERR_MTI_DEFAULTS_OPEN_FAILURE);

   // find the '[Hardware]' heading
   pos = NULL;
   while (true)
   {
      if (getLin(fpDefault,lineBuf,PTHLENG)==-1) break;
      if ((pos=strstr(lineBuf,"[Hardware]"))!=NULL) break;
   }
   if (pos==NULL) {
      fclose(fpDefault);
      return(OGLV_BRIDGE_ERR_NO_HARDWARE_HEADING_IN_MTI_DEFAULTS);
   }

   // look for the 'FastIOBoundary' entry between the heading and the next '['
   pos = NULL;
   while (true)
   {
      if (getLin(fpDefault,lineBuf,PTHLENG)==-1) break;
      if ((pos=strstr(lineBuf,"["))!=NULL) break;
      if (strncmp(lineBuf, "F", 1) == 0)
       {
        if ((pos=strstr(lineBuf,"FastIOBoundary="))!=NULL) break;
       }
   }
   if ((pos==NULL)||(*pos=='[')|| 
       (sscanf(lineBuf,"FastIOBoundary=%d",&fastIOBoundary)!=1)) {
      fclose(fpDefault);
      return(OGLV_BRIDGE_ERR_NO_FASTIOBOUNDARY_IN_MTI_DEFAULTS);
   }

   fclose(fpDefault);

   return(0);
}

//--------------------------------------------------------------------
//
// obtains the full pathname of the bin file associated with the bin index
//
int CBridge::getBinPathname(
                              char *dst, // output here
                              int binidx // index of bin file in master (0-(n-1)) 
                            )
{
   if ((binidx < 0)||(binidx >= masterNBin)) return(-1);

   int binID;
   char *pos;
   if ((pos=strstr(mline[binidx+masterNClipRet+2],"BinID="))==NULL)
      return(-1);
   sscanf(pos,"BinID=%d",&binID);

   sprintf(dst,"%s%s/%05d",environmentDir,
                           "/.mti_bin",
			   binID);

   return(0);
}

//-------------------------------------------------------------------
//
// obtains the (max 32-char) OGLV name of the bin
//
char *CBridge::getBinName(
                          int binidx // index of bin file in master (0-(n-1))
                        )
{
   char *pos;
   if ((pos=strstr(mline[binidx+masterNClipRet+2],"BinID="))==NULL)
      return(NULL);

   pos += 12;
   if (*pos != '\t')
      return(NULL);

   // string is at pos+1
   return(pos+1);
}

//-------------------------------------------------------------------

static const char *MTIBin = "MTIBin";

const char *CBridge::truncateBinName(
				char *binname
				)
{
   // strip off the bin root path prefix. If it's
   // not all there, we have a problem.  In  that
   // case a NULL will be returned
   char *brp = binRootPathname;
   char *beg = binname;
   while ((*brp!=0)&&(*beg!=0)&&(*brp==*beg)) {
      brp++;
      beg++;
   }
   if (*brp!=0) // prefix could not be stripped off
      return(NULL);

   // the prefix was stripped but nothing was left...
   if (*beg==0) return(MTIBin);

   // there's something left
   if (*(beg-1)=='/')
      beg--;

   // take the last 32 chars (max) of the binname
   // if there's a backslash in it, advance to just
   // past it. The rest of the string is the cor-
   // responding OGLV bin name

   // advance to null terminator
   char *pos = beg;
   while (*pos != 0) pos++;

   // go backwards, picking up a max of 32 chars
   // count the backslashes as you go
   int i=0;
   int j=0;
   while ((i<32)&&(pos > beg)) {
      i++;
      --pos;
      if (*pos == '/')
         j++;
   }

   // if there are backslashes, advance to just 
   // past the first one
   if (j > 0) {
      while (*pos != '/') pos++;
      pos++;
   }

   // the string starting here is the OGLV bin name
   return(pos);
}

//-------------------------------------------------------------------
//
// obtains the bin index for a given (complete) bin pathname
//
int CBridge::getBinIndex(
                           char *binname
			      )
{
   const char *src = truncateBinName(binname);
   if (src == NULL)
      return(-1);

   for (int i=0;i<masterNBin;i++) {

      char *beg = mline[2+masterNClipRet+i];
      char *dst = beg;
      while (*dst != 0) dst++;
      while ((dst > beg)&&(*dst != '\t')) dst--;
      if (*dst == '\t') {

         dst++;

         // src -> entry we're searching for
         // dst -> entry we're matching against 
         if (strcmp(dst,src)==0) { // found the entry
            return(i);
         }
      }
      // else you've got a bad line entry in .mti_master
   }

   return(-1);
}

//--------------------------------------------------------------------
int CBridge::getMTIMaster()
{
   int i,j;

   sprintf(masterPathname,"%s%s",environmentDir,
                                 "/.mti_master");

   mtiMasterLines = 0;

   FILE *fpMaster = fopen(masterPathname,"r");
   if (fpMaster==NULL)
      return(OGLV_BRIDGE_ERR_MTI_MASTER_OPEN_FAILURE);

   for (i=0;i<MTI_MASTER_LINES;i++) {
      mline[i] = new char[LINLENG];
      if (getLin(fpMaster,mline[i],LINLENG)==-1) break;
   }
   fclose(fpMaster);

   mtiMasterLines = i;

   if (i==MTI_MASTER_LINES) {
      for (i=0;i<mtiMasterLines;i++) {
         delete [] mline[i];
      }
      mtiMasterLines = 0; 
      return(OGLV_BRIDGE_ERR_TOO_MANY_LINES_IN_MTI_MASTER);
   }

   // read parameters of 1st line of master file 
   if (sscanf(mline[0],"ClipID=%d BinID=%d Bin=%d NClipRet=%d TriedRem=%d NBin=%d EDL=%d %d",
                       &masterClipID,&masterBinID,&masterBin,&masterNClipRet,
                       &masterTriedRem,&masterNBin,&masterEDL1,&masterEDL2)!=8)
      return(OGLV_BRIDGE_ERR_FAILURE_READING_1ST_LINE_OF_MTI_MASTER);

   newMasterClipID   = masterClipID;
   newMasterNClipRet = masterNClipRet;

   // count the number of clips in all the bins 
   int clipCount = 0;
   for (i=0;i<masterNBin;i++) {

      char binPathname[PTHLENG];

      // get the full pathname of the ith bin in .mti_master 
      if (getBinPathname(binPathname,i)==-1)
         return(OGLV_BRIDGE_ERR_FAILURE_GETTING_BIN_PATHNAME);

      // open the bin file
      FILE *fpBin = fopen(binPathname,"r");
      if (fpBin==NULL)
         return(OGLV_BRIDGE_ERR_BIN_FILE_OPEN_FAILURE);

      // read the bin file header 
      BINHEADER binHdr;
      if (getBinHeader(fpBin,&binHdr)==-1) {
         fclose(fpBin);
         return(OGLV_BRIDGE_ERR_BIN_HEADER_READ_FAILURE);
      }

      // close the bin file
      fclose(fpBin);

      // accumulate number of clips
      clipCount += binHdr.NClip; 

   }

   // allocate space for list of free blocks
   freeBlk = new FREEBLOCK[1+masterNClipRet+clipCount+EXTRA_FREE];

   // now collect all the storage -- free and clips -- controlled by OGLV
 
   // make an entry for the large free block at the end of the disk
   freePtr = freeBlk;
   if (sscanf(mline[1],"FreeBlock=%d",&(freePtr->beg))!=1)
      return(OGLV_BRIDGE_ERR_NO_FREEBLOCK_IN_MTI_MASTER);
   freePtr->end = rawDiskSize;
   freePtr->siz = freePtr->end - freePtr->beg;
   freePtr++;

   // make an entry for each of the "retired" clips listed
   for (i=2;i<2+masterNClipRet;i++) {
      if (sscanf(mline[i],"Location=%d NumBlock=%d",
          &(freePtr->beg),&(freePtr->siz))!=2)
         return(OGLV_BRIDGE_ERR_INSUFFICIENT_RETIRED_CLIP_BLOCKS);
      freePtr->end = freePtr->beg + freePtr->siz;
      freePtr++;
   }

   // for each bin file, open it up
   FREEBLOCK *clpPtr = freePtr;

   for (i=1;i<masterNBin;i++) {

      char binPathname[PTHLENG];

      // get the full pathname of the ith bin in .mti_master
      if (getBinPathname(binPathname,i)==-1)
         return(OGLV_BRIDGE_ERR_FAILURE_GETTING_BIN_PATHNAME);

      // open the bin file
      FILE *fpBin = fopen(binPathname,"r");
      if (fpBin==NULL) {
         return(OGLV_BRIDGE_ERR_BIN_FILE_OPEN_FAILURE);
      }

      // read the bin file header
      BINHEADER binHdr;
      if (getBinHeader(fpBin,&binHdr)==-1) {
         fclose(fpBin);
         return(OGLV_BRIDGE_ERR_BIN_HEADER_READ_FAILURE);
      }

      // for each clip in the bin file, make an entry
      // for the block of storage associated with it
      for (j=0;j<binHdr.NClip;j++) {

         CLIPRECORD clpRec;
         if (getClipRecord(fpBin,&clpRec)==-1) {
            fclose(fpBin);
            return(OGLV_BRIDGE_ERR_CLIP_ENTRY_READ_FAILURE);
         }

         // magic bits for filtering
         if ((clpRec.Status > CS_MARKED)                    &&
             ((clpRec.RecInfo & RECINFO_STORAGE_MEDIA_MASK)
             !=RECINFO_STORAGE_MEDIA_FILE_SYSTEM_AS_IMAGE)  &&
             ((clpRec.RecInfo & RECINFO_STORAGE_MEDIA_MASK)
             !=RECINFO_STORAGE_MEDIA_FILE_SYSTEM_AS_VIDEO)) {

            clpPtr->beg = clpRec.Location;
            clpPtr->siz = getBlocksOccupiedByClip(&clpRec);
            clpPtr->end = clpRec.Location + clpPtr->siz;

            clpPtr++;

         } 
      }

      // at the end of a bin file, we may have one extra linefeed
      int inchr = getc(fpBin);
      if (inchr != EOF) {
         if (inchr == '\n') {
            inchr = getc(fpBin);
            if (inchr != EOF) {
               fclose(fpBin);
               return(OGLV_BRIDGE_ERR_BIN_FILE_IMPROPERLY_TERMINATED);
            }
         }
      }

      fclose(fpBin);

   }

   // sort the free blocks and the clip blocks together
   // and then check to see that they span the entire disk
   FREEBLOCK *blkList = sortFreeBlocks(freeBlk,clpPtr-1);
   if (blkList->beg != 0) return(-1);
   while (blkList->nxt != NULL) {
      if (blkList->end != (blkList->nxt)->beg) {
         return(OGLV_BRIDGE_ERR_CONSISTENCY_FAILURE);
      }
      blkList = blkList->nxt;
   }
   if (blkList->end != rawDiskSize)
      return(OGLV_BRIDGE_ERR_CONSISTENCY_FAILURE);

   // leave a list of the free blocks only
   freeList = sortFreeBlocks(freeBlk,freePtr-1);

   // merge adjoining free blocks together
   FREEBLOCK *absorber = freeList;
   while (absorber!=NULL) {

      // loop thru blocks after absorber 
      FREEBLOCK *absorbed = absorber->nxt;
      while (absorbed!=NULL) {

         if (absorber->end==absorbed->beg) {

            // the absorber absorbs
            absorber->end  = absorbed->end;
            absorber->siz += absorbed->siz;
            absorber->nxt  = absorbed->nxt;

            // adjust this value downward
            newMasterNClipRet--;

            // advance
            absorbed = absorbed->nxt;

         }
         else break;

      }

      absorber = absorber->nxt;

   }

   // examine the pruned free list and derive
   // the total bytes and max contiguous btyes
   totalFreeBlocks = 0;
   maxContiguousFreeBlocks = -1;
   FREEBLOCK *fr = freeList;
   while (fr!=NULL) {
      totalFreeBlocks += fr->siz;
      if (fr->siz > maxContiguousFreeBlocks)
         maxContiguousFreeBlocks = fr->siz;
      fr = fr->nxt;
   }

   return(0);
}

//----------------------------------------------------------------------
int CBridge::putMTIMaster()
{
   if (majorState!=BRIDGE_STATE_OPEN) return(-1);

   FILE *fpMASTER = fopen(masterPathname,"wt");
   if (fpMASTER==NULL)
      return(OGLV_BRIDGE_ERR_MTI_MASTER_OPEN_FAILURE);

   // write out the first line of .mti_master
   if (fprintf(fpMASTER,"ClipID=%d BinID=%d Bin=%d NClipRet=%d TriedRem=%d NBin=%d EDL=%d %d\n",
                     newMasterClipID,masterBinID,masterBin,newMasterNClipRet,
                     masterTriedRem,masterNBin,masterEDL1,masterEDL2) < 0)
      return(OGLV_BRIDGE_ERR_FAILURE_WRITING_1ST_LINE_OF_MTI_MASTER);

   // the second line of .mti_master specifies the free pointer 
   FREEBLOCK *fr = freeList;

   // cycle to the last free block -- the one which ends at rawDiskSize
   // never drops from the list, even when its length is zero
   while (fr->nxt != NULL)
      fr = fr->nxt;

   // write 2nd line of master file
   if (fprintf(fpMASTER,"FreeBlock=%d\n",fr->beg) < 0)
      return(OGLV_BRIDGE_ERR_FAILURE_WRITING_2ND_LINE_OF_MTI_MASTER);;

   // now we make an entry for every free block which is non-zero
   // in size. The number of these must be = NEWMASTERNCLIPRET
   fr = freeList;
   while (fr->nxt != NULL) {
      // ignore the null free blocks and the last block
      if (fr->siz > 0) {
         if (fprintf(fpMASTER,"Location=%d NumBlock=%d\n",
                           fr->beg, fr->siz) < 0)
            return(OGLV_BRIDGE_ERR_RET_CLIP_WRITE_FAILURE);
      }
      fr = fr->nxt;
   }

   // the remaining lines specify the bins
   for (int i=2+masterNClipRet;i<mtiMasterLines;i++) {
      if (fprintf(fpMASTER,"%s\n",mline[i]) < 0)
         return(OGLV_BRIDGE_ERR_BIN_ENTRY_WRITE_FAILURE);
   }

   fclose(fpMASTER);

   return(0);
}

//----------------------------------------------------------------------
// translates CPMP formats into DRS formats
static int DRSTbl[] = {
  -1,
  -1,
  -1,
  -1,

   3, // 525_5994 8-bit ND
   5, //          8-bit DF
  19, //         10-bit ND
  21, //         10-bit DF

   8, // 625_50 8-bit ND
  -1, //        8-bit DF
  24, //       10-bit ND
  -1, //       10-bit DF

2304, // 720P_60 8-bit ND
4864, //         8-bit DF
4608, //        10-bit ND
5120, //        10-bit DF

  -1, // 720P_50 8-bit ND
  -1, //         8-bit DF
  -1, //        10-bit ND
  -1, //        10-bit DF

1792, // 1080I_60 8-bit ND
2816, //          8-bit DF
3072, //         10-bit ND
3328, //         10-bit DF

5632, // 1080I_50 8-bit ND
  -1, //          8-bit DF
5888, //         10-bit ND
  -1, //         10-bit DF

3584, // 1080P_24 8-bit ND
  -1, //          8-bit DF
4096, //         10-bit ND
  -1, //         10-bit DF

6144, // 1080P_25 8-bit ND
  -1, //          8-bit DF
6400, //         10-bit ND
  -1, //         10-bit DF

  -1, // DPX 8-bit ND
  -1, //     8-bit DF
  -1, //    10-bit ND
  -1, //    10-bit DF

  -1, // CINEON 8-bit ND
  -1, //        8-bit DF
  -1, //       10-bit ND
  -1, //       10-bit DF

  -1, // TIFF 8-bit ND
  -1, //      8-bit DF
  -1, //     10-bit ND
  -1, //     10-bit DF

  -1, // SGI 8-bit ND
  -1, //     8-bit DF
  -1, //    10-bit ND
  -1, //    10-bit DF

   3, // 525_60 8-bit ND
   5, //        8-bit DF
  19, //       10-bit ND
  21, //       10-bit DF

2304, // 720P_5994 8-bit ND
4864, //           8-bit DF
4608, //          10-bit ND
5120, //          10-bit DF

1792, // 1080I_5994 8-bit ND
2816, //            8-bit DF
3072, //           10-bit ND
3328, //           10-bit DF

3584, // 1080P_2398 8-bit ND
  -1, //            8-bit DF
4096, //           10-bit ND
  -1, //           10-bit DF

3584, // 1080PSF_2398 8-bit ND
  -1, //              8-bit DF
4096, //             10-bit ND
  -1, //             10-bit DF

3584, // 1080PSF_24   8-bit ND
  -1, //              8-bit DF
4096, //             10-bit ND
  -1, //             10-bit DF

6144, // 1080PSF_25 8-bit ND
  -1, //            8-bit DF
6400, //           10-bit ND
  -1, //           10-bit DF

};

int CBridge::getDRSFormat(const CImageFormat *infmt, CTimecode *intime)
{
   int fmtnum = 4*infmt->getImageFormatType();

   if (infmt->getBitsPerComponent()==10)
      fmtnum += 2;
   if ((intime->getFlags()&DROPFRAME)!=0) fmtnum++;

   if (fmtnum > 79) return(-1);

   return(DRSTbl[fmtnum]);
}

//----------------------------------------------------------------------
void CBridge::getExistingFlistPathname(char *dst, int clpid)
{
   sprintf(dst,"%s%s/%05d",environmentDir,
                           "/.mti_flist",
                           clpid);
}

//----------------------------------------------------------------------
int CBridge::getNewFlistPathname(char *dst)
{
   // allocate a new clip id
   int nuclpid = newMasterClipID++;

   sprintf(dst,"%s%s/%05d",environmentDir,
                           "/.mti_flist",
                           nuclpid);

   return(nuclpid);
}

//----------------------------------------------------------------------
void CBridge::getCurrentDate(char *mo, char *day, char *yr)
{
   time_t ltime;
   time(&ltime);
   struct tm *today = localtime(&ltime);

   strftime(mo,8,"%b",today);

   strftime(day,8,"%d",today);
   while (*day=='0') *day++ = ' ';

   strftime(yr,8,"%Y",today);

}

//----------------------------------------------------------------------
int CBridge::buildClipRecordFromCPMPClip(CLIPRECORD *cr, ClipSharedPtr &inclp)
{
   // get video frame list
   CVideoFrameList *inputFrameList =
                  inclp->getVideoFrameList(VIDEO_SELECT_NORMAL,
                                           FRAMING_SELECT_VIDEO);
   
   // get frame count
   int inputFrameCount = inputFrameList->getTotalFrameCount();

   // get input image format 
   const CImageFormat *inputFormat =
                  inputFrameList->getImageFormat();

   // get video fields per frame
   int inputFieldsPerFrame = (inputFormat->getInterlaced()?2:1);

   // line 1

   // TIMECODES
   if (inputFrameCount < 33) return(-1);

   CTimecode  inTimecode = inputFrameList->getTimecodeForFrameIndex(16);
   CTimecode outTimecode = inputFrameList->getTimecodeForFrameIndex(inputFrameCount-16);

   cr->HoursIn   = inTimecode.getHours();
   cr->MinutesIn = inTimecode.getMinutes();
   cr->SecondsIn = inTimecode.getSeconds();
   cr->FramesIn  = inTimecode.getFrames();

   cr->HoursOut   = outTimecode.getHours();
   cr->MinutesOut = outTimecode.getMinutes();
   cr->SecondsOut = outTimecode.getSeconds();
   cr->FramesOut  = outTimecode.getFrames();

   // FORMAT
   cr->Format = getDRSFormat(inputFormat,&inTimecode);
   if (cr->Format==-1) return(-1);

   // STATUS
   cr->Status = 2;

   // DEMO - will be modified later if target bin = "Demo Bin"
   cr->Demo = 0;

   // CLIPID will be deferred until the clip is accepted
   cr->ClipID = -1;

   // line 2 

   // FIX
   cr->Fix = 0;

   // SECS
   cr->Secs = 0;

   // DATE
   getCurrentDate(cr->Month,cr->Day,cr->Year);

   // line 3 

   // NITEM
   cr->NItem = inputFrameCount*inputFieldsPerFrame;

   // LOCATION
   CVideoFrame *begfrm = inputFrameList->getFrame(0);
   CField *begfld = begfrm->getBaseField(0);
   const CMediaLocation& mediaLoc = begfld->getMediaLocation();
   cr->Location = (mediaLoc.getDataIndex()/((MTI_INT64)512));

   // ITEMSIZE
   cr->ItemSize = mediaLoc.getDataSize()/512;

   // CLIPSIZE
   cr->ClipSize = cr->NItem * cr->ItemSize;

   // line 4 

   // BITS
   cr->Bits = inputFormat->getBitsPerComponent();

   // PIXELVALS
   cr->PixelVals = ((cr->Bits==8)?1:11);

   // STRIDE
   cr->Stride = 0;

   // ROWREVERSED
   cr->RowReversed = 0;

   // line 5 

   // VALSCALE
   cr->ValScale = 0;

   // NUMPIXELPERROW
   cr->NumPixelPerRow = inputFormat->getPixelsPerLine();

   // NUMROWSPERITEM
   cr->NumRowsPerItem = inputFormat->getLinesPerFrame() /
                              inputFieldsPerFrame;

   // RECINFO
   cr->RecInfo = 512;
   if (cr->Bits==10) cr->RecInfo = 520;
   if (inputFormat->getVerticalIntervalRows()==16) {
      cr->RecInfo += RECINFO_KEEP_VERTICAL_INTERVAL;
      cr->ItemSize -= BLOCKS_PER_FIELD_VERTICAL_INTERVAL;
   }
   else if (inputFormat->getVerticalIntervalRows()!=0)
      return(-1);

   // line 6 

   // MARKIN
   cr->HoursMarkIn    = -1;
   cr->MinutesMarkIn  = -1;
   cr->SecondsMarkIn  = -1;
   cr->FramesMarkIn   = -1;

   // MARKOUT
   cr->HoursMarkOut   = -1;
   cr->MinutesMarkOut = -1;
   cr->SecondsMarkOut = -1;
   cr->FramesMarkOut  = -1;

   // line 7 

   // NAME (do not modify this line!)
   int charcnt;
   charcnt = inclp->getDescription().copy(cr->Name,
                             OGLV_BRIDGE_DESCRIPTION_LENGTH-1);
   cr->Name[charcnt] = 0;


   return(0);
}

//--------------------------------------------------------------------
//
// The value of Crypt in the bin header is equal to the sum of
//
//   1) the ClipID of the last clip entry
//   2) the index of the last entry * 13
//   3) the index of the last entry * Demo
//   4) the Secs of the last entry
//
unsigned int CBridge::encrypt(unsigned int ulVal)
{
   unsigned int ulX,ulX0,ulX1,ulX2,ulX3,ulX4,ulX5,ulX6,ulX7,ulCrypt;

   ulX = (ulVal + 3141)*271;

   ulX0 = ulX&0xF;
   ulX1 = (ulX&0xF0)>>4;
   ulX2 = (ulX&0xF00)>>8;
   ulX3 = (ulX&0xF000)>>12;
   ulX4 = (ulX&0xF0000)>>16;
   ulX5 = (ulX&0xF00000)>>20;
   ulX6 = (ulX&0xF000000)>>24;
   ulX7 = (ulX&0xF0000000)>>28;

   ulCrypt = (ulX0<<28)+(ulX2<<24)+(ulX4<<20)+(ulX6<<16)+
             (ulX7<<12)+(ulX5<<8)+(ulX3<<4)+ulX1;

   return(ulCrypt);
}

//----------------------------------------------------------------------
int CBridge::openOGLV()
{
   int status;

   // get the .mti_defaults file contents
   status = getMTIDefaults();
   if (status != 0)
      return(status);

   // read the .mti_master file into RAM
   status = getMTIMaster();
   if (status != 0)
      return(status);

   // read the root pathname for the bins
   StringList binList;
   CPMPGetBinRoots(&binList);
   string binRootPath;
   if (binList.size()!=1)
      return(-1);
   binRootPath = binList[0];
   strcpy(binRootPathname,binRootPath.c_str());

   // init the number of clips deleted
   numClipsDeleted = 0;

   // success is an open bridge 
   majorState = BRIDGE_STATE_OPEN;

   return(0);
}

//----------------------------------------------------------------------
char *CBridge::getRawVideoDirectory()
{
   if (majorState != BRIDGE_STATE_OPEN) return(NULL);

   // return the addr of the string copied from .mti_defaults
   return(rawVideoDir);
}

//----------------------------------------------------------------------
int CBridge::getFastIOBoundary()
{
   if (majorState != BRIDGE_STATE_OPEN) return(-1);

   // return the value extracted from .mti_defaults
   if (fastIOBoundary == 0)
      return(1);
   else 
      return(fastIOBoundary);
}

//----------------------------------------------------------------------
int CBridge::getTotalFreeBlocks()
{
   return(totalFreeBlocks);
}

//----------------------------------------------------------------------
int CBridge::getMaxContiguousFreeBlocks()
{
   return(maxContiguousFreeBlocks);
}

//----------------------------------------------------------------------
int CBridge::allocateClipBlocks(int numblk)
{
   if (majorState!=BRIDGE_STATE_OPEN)
      return(-1);

   // search the free list for the 1st block large enough
   FREEBLOCK *fr = freeList;
   while (fr!=NULL) {

      if (numblk <= fr->siz) {
         // found a free block large enough
         newLocation = fr->beg;
         newBlocks   = numblk;
         return(newLocation);
      }
      fr = fr->nxt;
   }

   // no free block was large enough
   return(-1);
}

//----------------------------------------------------------------------
int CBridge::appendClip(int binidx, ClipSharedPtr &inclp)
{
   int status; 

   if (majorState!=BRIDGE_STATE_OPEN)
      return(OGLV_BRIDGE_ERR_BRIDGE_NOT_OPEN);

   if ((newLocation ==-1)&&(newBlocks ==-1))
      return(OGLV_BRIDGE_ERR_NO_STORAGE_ALLOCATED);

   if (binidx < 3) // the bin is reserved for OGLV
      return(OGLV_BRIDGE_ERR_INVALID_BIN);

   // get the full pathname of the ith bin in .mti_master
   char binPathname[PTHLENG];
   if (getBinPathname(binPathname,binidx)==-1)
      return(OGLV_BRIDGE_ERR_FAILURE_GETTING_BIN_PATHNAME);

   // build a new clip record from the input clip 
   CLIPRECORD newClpRec;
   if (buildClipRecordFromCPMPClip(&newClpRec,inclp)==-1)
      return(OGLV_BRIDGE_ERR_FAILURE_BUILDING_CLIP_RECORD);

   // if target is a DEMO bin then set the flag
   if (binidx == 2) newClpRec.Demo = 1;

   // make sure the new clip's storage requirements
   // match the memory which has just been allocated
   if ((newClpRec.Location != newLocation) ||
       ((newClpRec.ClipSize) != newBlocks))
      return(OGLV_BRIDGE_ERR_MEMORY_MISMATCH);

   // update the queue of free memory blocks - this will be
   // used to generate the memory blocks in .mti_master
   FREEBLOCK *fr = freeList;
   while (fr!=NULL) {
      if (fr->beg == newLocation) {
         fr->beg += newBlocks;
         fr->siz -= newBlocks;
         if ((fr->siz==0)&&
             (fr->end!=rawDiskSize))
            newMasterNClipRet--;
         break;
      }
      fr = fr->nxt;
   }
   if (fr==NULL)
      return(OGLV_BRIDGE_ERR_MEMORY_MISMATCH);

   // get the ClipID and pathname for the new FLIST to
   // be built from this clip
   char newFlistPathname[PTHLENG];
   newClpRec.ClipID = getNewFlistPathname(newFlistPathname);

   // create a new Flist with the pathname given 
   status = createFlistFromCPMPClip(newFlistPathname, inclp);
   if (status != 0)
      return(status);

   // create scratch directories for the new clip 
   status = makeScratchDirectories(newClpRec.ClipID);
   if (status != 0)
      return(status);

   // open work file
   FILE *fpBin = fopen(binPathname,"r");
   if (fpBin==NULL)
      return(OGLV_BRIDGE_ERR_BIN_FILE_OPEN_FAILURE);

   // the new bin file is built in a workfile
   char wrkBinPathname[PTHLENG];
   sprintf(wrkBinPathname,"%s%s%s",environmentDir,
				   "/.mti_bin",
                                   "/wrk.bin");

   FILE *fpWrk = fopen(wrkBinPathname,"wt");
   if (fpWrk==NULL)
      return(OGLV_BRIDGE_ERR_WRK_FILE_OPEN_FAILURE);

   // generate and write new bin header
   BINHEADER newBinHdr;

   // old bin header
   getBinHeader(fpBin,&newBinHdr);

   // bin will be non-empty 
   newBinHdr.Clip = 0; // 0 for a non-empty bin

   // new encrpyption value
   newBinHdr.Crypt = encrypt(newClpRec.ClipID+
                             ENCRYPT_FACTOR*newBinHdr.NClip);

   // bump the count
   newBinHdr.NClip++; // a clip is added

   // make the last clip current for display
   newBinHdr.Clip = newBinHdr.NClip - 1;

   // output the new header to the work file 
   if (putBinHeader(fpWrk,&newBinHdr)==-1)
      return(OGLV_BRIDGE_ERR_BIN_HEADER_WRITE_FAILURE);

   // write the rest of the original file 
   CLIPRECORD oldClpRec;
   for (int i=0;i<newBinHdr.NClip-1;i++) {
      if (getClipRecord(fpBin,&oldClpRec)==-1) {
         fclose(fpBin);
         return(OGLV_BRIDGE_ERR_CLIP_ENTRY_READ_FAILURE);
      }

      if (putClipRecord(fpWrk,&oldClpRec)==-1) {
         fclose(fpWrk);
         return(OGLV_BRIDGE_ERR_CLIP_ENTRY_WRITE_FAILURE);
      }
   }

   // now write the new entry
   if (binidx == 2) newClpRec.Demo = 1;
   if (putClipRecord(fpWrk,&newClpRec)==-1) {
      fclose(fpWrk);
      return(OGLV_BRIDGE_ERR_CLIP_ENTRY_WRITE_FAILURE);
   }

   // close the files 
   fclose(fpBin);
   fclose(fpWrk);

   // rename the work file to be the bin file
   if (rename(wrkBinPathname,binPathname)==-1)
      return(OGLV_BRIDGE_ERR_WRK_FILE_RENAME_FAILURE);

   // the new storage has been used
   newLocation = -1;
   newBlocks   = -1;

   // make the target bin current for display
   masterBin = binidx;

   // write out master file
   status = putMTIMaster();
   if (status != 0)
      return(status);

   return(0);
}

//----------------------------------------------------------------------
int CBridge::createFlistFromCPMPClip(char *flname, ClipSharedPtr &inclp)
{
//   int status;

   // create a new Flist with the given pathname
   CFlist fl(flname);
   if (fl.createFlist()!=0)
      return(OGLV_BRIDGE_ERR_FAILURE_CREATING_FLIST);

   ///////////////////////////////////////////////////////////////
   // first create the video part of the Flist
   // get video frame list
   CVideoFrameList *videoFrameList =
                  inclp->getVideoFrameList(VIDEO_SELECT_NORMAL,
                                           FRAMING_SELECT_VIDEO);

   // the absolute offset of the first field of the clip 
   CVideoFrame *begfrm = videoFrameList->getFrame(0);
   CField *begfld = begfrm->getBaseField(0);
   const CMediaLocation& mediaLoc = begfld->getMediaLocation();
   int blkloc = (mediaLoc.getDataIndex()/((MTI_INT64)512));

   // get video frame count
   int videoFrameCount = videoFrameList->getTotalFrameCount();

   // get video image format
   const CImageFormat *videoFormat = videoFrameList->getImageFormat();

   // get video fields per frame
   int fieldsPerFrame = (videoFormat->getInterlaced()?2:1);

   fl.openTrack(FLIST_TRACK_VIDEO);
   for (int i=0;i<videoFrameCount;i++) {

      CVideoFrame *frm = videoFrameList->getFrame(i);
      CTimecode tim = frm->getTimecode();
      fl.openFrame(tim.getHours(),tim.getMinutes(),
                   tim.getSeconds(),tim.getFrames());

      for (int j=0;j<fieldsPerFrame;j++) {
         CField *fld = frm->getBaseField(j);
         const CMediaLocation& mediaLoc = fld->getMediaLocation();
         int relBlk = (MTI_INT32)(mediaLoc.getDataIndex()/((MTI_INT64)512))
                      - blkloc;

         fl.addField(relBlk);
      }

      if (fl.addFrame(TC_NONE)==-1)
         return(OGLV_BRIDGE_ERR_FAILURE_CREATING_FLIST);
   }
   if (fl.closeTrack()==-1)
      return(OGLV_BRIDGE_ERR_FAILURE_CREATING_FLIST);

   ///////////////////////////////////////////////////////////////
   // now create the film part of the Flist
   // get film frame list
   CVideoFrameList *filmFrameList =
                  inclp->getVideoFrameList(VIDEO_SELECT_NORMAL,
                                           FRAMING_SELECT_FILM);

   int filmFramingType = filmFrameList->getFramingType();
   if ((filmFramingType != FRAMING_TYPE_INVALID)&&
       (filmFramingType != FRAMING_TYPE_VIDEO)) { // filmFramingType tested

      // get film frame count
      int filmFrameCount = filmFrameList->getTotalFrameCount();

      // get image format type
      EImageFormatType filmImageFormatType = videoFormat->getImageFormatType();

      ///////////////////////////////////////////////////////////////
      // now we generate the film track of the Flist, based on the
      // pulldown type

      switch(CImageInfo::queryNominalPulldown(filmImageFormatType)) {

         case IF_PULLDOWN_FIELD_3_2: // field-based 3:2 pulldown 

            fl.openTrack(FLIST_TRACK_FILM);

            for (int i=0;i<filmFrameCount;i++) {
               CVideoFrame *frm = filmFrameList->getFrame(i);
               CTimecode tim = frm->getTimecode();

               fl.openFrame(tim.getHours(),tim.getMinutes(),
                            tim.getSeconds(),tim.getFrames());

               // the half bit inthe timecode identifies the hybrid frame
               int iHalf = TC_NONE;
               if (tim.isHalfFrame()) {
                  iHalf = TC_HALF;
               }

               // watch carefully
               int j = 0;
               for (;j<frm->getVisibleFieldCount();j++) {
                  CField *fld = frm->getBaseField(j);
                  const CMediaLocation& mediaLoc = fld->getMediaLocation();
                  int relBlk = (MTI_INT32)(mediaLoc.getDataIndex()/((MTI_INT64)512))
                               - blkloc;

                  fl.addField(relBlk);
               }
               for (;j<frm->getTotalFieldCount();j++) {
                  CField *fld = frm->getBaseField(j);
                  const CMediaLocation& mediaLoc = fld->getMediaLocation();
                  int relBlk = (MTI_INT32)(mediaLoc.getDataIndex()/((MTI_INT64)512))
                               - blkloc;

                  fl.addField(relBlk);

                  // now the code bits for the invisible sibling
                  if (fld->getVisibleSiblingFieldIndex()==0)
                     iHalf |= TC_PAIR0;
                  else
                     iHalf |= TC_PAIR1;

               }

               if (fl.addFrame(iHalf)==-1)
                  return(OGLV_BRIDGE_ERR_FAILURE_CREATING_FLIST);
            }
            if (fl.closeTrack()==-1)
               return(OGLV_BRIDGE_ERR_FAILURE_CREATING_FLIST);

         break;


         case IF_PULLDOWN_FRAME_3_2: // frame-based 3:2 pulldown (720P_60 & 720P_5994)

            fl.openTrack(FLIST_TRACK_FILM);

            for (int i=0;i<filmFrameCount;i++) {
               CVideoFrame *frm = filmFrameList->getFrame(i);
               int frmFlds = frm->getTotalFieldCount();
               CTimecode tim = frm->getTimecode();

               fl.openFrame(tim.getHours(),tim.getMinutes(),
                            tim.getSeconds(),tim.getFrames());

               int iHalf = TC_NONE; 
               for (int j=0;j<frmFlds;j++) {


                  CField *fld = frm->getBaseField(j);
                  const CMediaLocation& mediaLoc = fld->getMediaLocation();
                  int relBlk = (MTI_INT32)(mediaLoc.getDataIndex()/((MTI_INT64)512))
                               - blkloc;

                  fl.addField(relBlk);

                  // mapping this pulldown is simple 
                  if (frmFlds == 2)
                     iHalf |= TC_PAIR_NEXT_1;
                  else // frmFlds = 3
                     iHalf |= TC_PAIR_NEXT_2;

               }

               if (fl.addFrame(iHalf)==-1)
                  return(OGLV_BRIDGE_ERR_FAILURE_CREATING_FLIST);
            }
            if (fl.closeTrack()==-1)
               return(OGLV_BRIDGE_ERR_FAILURE_CREATING_FLIST);

         break;

         default: // other types of film track

            fl.openTrack(FLIST_TRACK_FILM);

            for (int i=0;i<filmFrameCount;i++) {
               CVideoFrame *frm = filmFrameList->getFrame(i);
               CTimecode tim = frm->getTimecode();

               fl.openFrame(tim.getHours(),tim.getMinutes(),
                            tim.getSeconds(),tim.getFrames());

               for (int j=0;j<fieldsPerFrame;j++) {
                  CField *fld = frm->getBaseField(j);
                  const CMediaLocation& mediaLoc = fld->getMediaLocation();
                  int relBlk = (MTI_INT32)(mediaLoc.getDataIndex()/((MTI_INT64)512))
                               - blkloc;

                  fl.addField(relBlk);
               }

               if (fl.addFrame(TC_NONE)==-1)
                  return(OGLV_BRIDGE_ERR_FAILURE_CREATING_FLIST);
            }
            if (fl.closeTrack()==-1)
               return(OGLV_BRIDGE_ERR_FAILURE_CREATING_FLIST);

         break;
   }

   } // filmFramingType tested

   if (fl.closeFlist()==-1)
      return(OGLV_BRIDGE_ERR_FAILURE_CREATING_FLIST);

   return(0);
}

//----------------------------------------------------------------------
int CBridge::conformExistingFlist(ClipSharedPtr &inclp)
{
   int status;

   if (majorState!=BRIDGE_STATE_OPEN)
      return(OGLV_BRIDGE_ERR_BRIDGE_NOT_OPEN);

   // build a clip record from the input clip
   CLIPRECORD inClpRec;
   if (buildClipRecordFromCPMPClip(&inClpRec,inclp)==-1)
      return(OGLV_BRIDGE_ERR_FAILURE_BUILDING_CLIP_RECORD);

   // key stats for finding an existing clip in OGLV
   int blkloc = inClpRec.Location;
   int blkcnt = getBlocksOccupiedByClip(&inClpRec);

   // search all the bins for the clip entry which matches the
   // arguments. This will give us the filename of the Flist 

   int targetClipID = -1;
   char binPathname[PTHLENG];

   for (int i=1;i<masterNBin;i++) {

      // get the full pathname of the ith bin in .mti_master
      if (getBinPathname(binPathname,i)==-1)
         return(OGLV_BRIDGE_ERR_FAILURE_GETTING_BIN_PATHNAME);

      // open the bin file
      FILE *fpBin = fopen(binPathname,"r");
      if (fpBin==NULL)
         return(OGLV_BRIDGE_ERR_BIN_FILE_OPEN_FAILURE);

      // read the bin file header
      BINHEADER binHdr;
      if (getBinHeader(fpBin,&binHdr)==-1) {
         fclose(fpBin);
         return(OGLV_BRIDGE_ERR_BIN_HEADER_READ_FAILURE);
      }

      // loop thru the clips in the bin
      for (int j=0;j<binHdr.NClip;j++) {

         CLIPRECORD clpRec;
         getClipRecord(fpBin,&clpRec);

         if ((clpRec.Location==blkloc)&&
             (getBlocksOccupiedByClip(&clpRec)==blkcnt)) { // match

            targetClipID = clpRec.ClipID;
         }
      }

      // close the bin file
      fclose(fpBin);

      // if target is defined, exit
      if (targetClipID != -1) break;
   }

   if (targetClipID == -1)
      return(OGLV_BRIDGE_ERR_CLIP_NOT_FOUND_IN_ANY_BIN);

   // get the pathname of the asssociated Flist
   char oldFlistPathname[PTHLENG];
   getExistingFlistPathname(oldFlistPathname,targetClipID);

   // we'll create the new Flist in a work file
   char wrkFlistPathname[PTHLENG];
   strcpy(wrkFlistPathname,oldFlistPathname);
   strcat(wrkFlistPathname,".wrk");

   // now build a completely new Flist from the VIDEO
   // and FILM tracks as they are in the input clip
   status = createFlistFromCPMPClip(wrkFlistPathname,inclp);
   if (status != 0)
      return status;

   // now rename the work file to be the original file
   if (rename(wrkFlistPathname,oldFlistPathname)==-1)
      return(OGLV_BRIDGE_ERR_WRK_FILE_RENAME_FAILURE);

   // success
   return(0);

}

//----------------------------------------------------------------------
int CBridge::deallocateClipBlocks(int blkloc, int blkcnt)
{
   int status;

   if (majorState!=BRIDGE_STATE_OPEN)
      return(OGLV_BRIDGE_ERR_BRIDGE_NOT_OPEN);

   // if the number of clips deleted since the last MTIOPEN
   // is maxed out, write out .mti_master and read it in again
   if (numClipsDeleted == EXTRA_FREE) {

      // write out .mti_master as is
      putMTIMaster();

      // free storage associated with free blocks
      delete [] freeBlk;

      // free any storage associated with the master file
      for (int i=0;i<mtiMasterLines+1;i++)
         delete [] mline[i];

      // re-load .mti_master
      getMTIMaster();

      // reset number of clips deleted
      numClipsDeleted = 0;

   }

   // search all the bins for the clip entry which matches the
   // arguments. Simultaneously derive the encryption code
   // for the new version of the affected bin (i.e., with the
   // clip deleted).

   int targetBin = -1;
   int targetClipIndex = -1;
   int targetClipID;
   int targetEncryptValue;
   char binPathname[PTHLENG];

   for (int i=1;i<masterNBin;i++) {

      // get the full pathname of the ith bin in .mti_master
      if (getBinPathname(binPathname,i)==-1)
         return(OGLV_BRIDGE_ERR_FAILURE_GETTING_BIN_PATHNAME);

      // open the bin file
      FILE *fpBin = fopen(binPathname,"r");
      if (fpBin==NULL)
         return(OGLV_BRIDGE_ERR_BIN_FILE_OPEN_FAILURE);

      // read the bin file header
      BINHEADER binHdr;
      if (getBinHeader(fpBin,&binHdr)==-1) {
         fclose(fpBin);
         return(OGLV_BRIDGE_ERR_BIN_HEADER_READ_FAILURE);
      }

      // the targetEncryptValue will be determined by the number
      // of clips in the bin and the LAST clip entry which is not
      // deleted from the bin. Note that we have to consider the
      // possibility that only one clip is in the bin, and init
      // the encrypt value accordingly.

      targetEncryptValue = encrypt(0);
      for (int j=0;j<binHdr.NClip;j++) {

         CLIPRECORD clpRec;
         getClipRecord(fpBin,&clpRec);

         if ((clpRec.Location==blkloc)&&
             (getBlocksOccupiedByClip(&clpRec)==blkcnt)) { // match

            targetBin = i;
            targetClipIndex = j;
            targetClipID = clpRec.ClipID;

         }
         else {

            // the clip may be the last one which remains undeleted, in
            // which case it will determine the encryption code for the
            // modified bin file
            targetEncryptValue = encrypt(clpRec.ClipID+
                                         (ENCRYPT_FACTOR+clpRec.Demo)*(binHdr.NClip-2)+
                                         clpRec.Secs);
         }
      }

      // close the bin file
      fclose(fpBin);

      // if target is defined, exit
      if (targetBin != -1) break;
   }

   // if the clip was not found in any bin, the deletion fails
   if (targetBin == -1) return(OGLV_BRIDGE_ERR_CLIP_NOT_FOUND_IN_ANY_BIN);

   // now open both the bin file and a work file which
   // will be used for rebuilding it

   FILE *fpBin = fopen(binPathname,"r");
   if (fpBin==NULL)
      return(OGLV_BRIDGE_ERR_BIN_FILE_OPEN_FAILURE);

   // the new bin file is built in a workfile
   char wrkBinPathname[PTHLENG];
   sprintf(wrkBinPathname,"%s%s%s",environmentDir,
                                   "/.mti_bin",
                                   "/wrk.bin");

   FILE *fpWrk = fopen(wrkBinPathname,"wt");
   if (fpWrk==NULL)
      return(OGLV_BRIDGE_ERR_WRK_FILE_OPEN_FAILURE);

   // read the bin file header
   BINHEADER binHdr;
   if (getBinHeader(fpBin,&binHdr)==-1) {
      fclose(fpBin);
      fclose(fpWrk);
      return(OGLV_BRIDGE_ERR_BIN_HEADER_READ_FAILURE);
   }

   // we have one less clip now
   binHdr.NClip--;

   // adjust the clip currently displayed 
   if (binHdr.Clip > targetClipIndex)
      binHdr.Clip--;
   if (binHdr.Clip >= binHdr.NClip)
      binHdr.Clip--;

   // we've captured the new encrypt value already
   binHdr.Crypt = targetEncryptValue;
   if (putBinHeader(fpWrk,&binHdr)==-1)
      return(OGLV_BRIDGE_ERR_BIN_HEADER_WRITE_FAILURE);

   // read the clips in the bin and copy all but
   // the one to be deleted over to the work file

   for (int j=0;j<binHdr.NClip+1;j++) {

      CLIPRECORD clpRec;
      if (getClipRecord(fpBin,&clpRec)==-1)
         return(OGLV_BRIDGE_ERR_CLIP_ENTRY_READ_FAILURE);

      if (clpRec.ClipID != targetClipID) {

         if (putClipRecord(fpWrk,&clpRec)==-1)
            return(OGLV_BRIDGE_ERR_CLIP_ENTRY_WRITE_FAILURE);
      }
   }

   // close the files
   fclose(fpBin);
   fclose(fpWrk);

   // rename the work file to be the bin file
   if (rename(wrkBinPathname,binPathname)==-1)
      return(OGLV_BRIDGE_ERR_WRK_FILE_RENAME_FAILURE);

   // create a new free block to represent the clip
   // storage just released
   freePtr->beg = blkloc;
   freePtr->siz = blkcnt;
   freePtr->end = blkloc + freePtr->siz;
   if (freePtr->end != rawDiskSize)
      newMasterNClipRet++;
   freePtr++;

   // sort the free blocks again
   freeList = sortFreeBlocks(freeBlk,freePtr-1);

   // merge adjoining free blocks together
   FREEBLOCK *absorber = freeList;
   while (absorber!=NULL) {

      // loop thru blocks after absorber
      FREEBLOCK *absorbed = absorber->nxt;
      while (absorbed!=NULL) {

         if (absorber->end==absorbed->beg) {

            // the absorber absorbs
            absorber->end  = absorbed->end;
            absorber->siz += absorbed->siz;
            absorber->nxt  = absorbed->nxt;

            // adjust this value downward
            newMasterNClipRet--;

            // advance
            absorbed = absorbed->nxt;

         }
         else break;

      }

      absorber = absorber->nxt;

   }

   // make the target bin current for display
   masterBin = targetBin;

   // KT 6/19/02 - if the deleted clip is part of
   // an EDL then make both EDL components = -1
   if ( (targetClipID != -1)&&
        ((targetClipID==masterEDL1)||(targetClipID==masterEDL2)) ) {

      masterEDL1 = masterEDL2 = -1;
   }

   // output the new master file
   status = putMTIMaster();
   if (status != 0)
      return(status);

   // remove the SCRATCH directories for the clip
   status = removeScratchDirectories(targetClipID);
   if (status != 0)
      return(status);

   // delete the FLIST associated with the clip
   char oldFlistPathname[PTHLENG];
   getExistingFlistPathname(oldFlistPathname,targetClipID);

   if (remove(oldFlistPathname)==-1)
      return(OGLV_BRIDGE_ERR_FLIST_DELETE_FAILURE);

   // success - count the deleted clip
   numClipsDeleted++;

   return(0);
}

//----------------------------------------------------------------------
int CBridge::deleteClip(ClipSharedPtr &inclp)
{
   // build a clip record from the input clip
   CLIPRECORD inClpRec;
   if (buildClipRecordFromCPMPClip(&inClpRec,inclp)==-1)
      return(OGLV_BRIDGE_ERR_FAILURE_BUILDING_CLIP_RECORD);

   // call the deallocator for the location & size
   return(deallocateClipBlocks(inClpRec.Location,
			       getBlocksOccupiedByClip(&inClpRec)));
}

//----------------------------------------------------------------------
//
// creates a new bin in OGLV, including an empty bin file
// returns the OGLV index of the new bin, or < 0 in case of failure
//
int CBridge::appendBin(char *binname)
{
   if (majorState!=BRIDGE_STATE_OPEN)
      return(OGLV_BRIDGE_ERR_BRIDGE_NOT_OPEN);
   
   if (mtiMasterLines > (MTI_MASTER_LINES - 2))
      return(OGLV_BRIDGE_ERR_TOO_MANY_LINES_IN_MTI_MASTER);

   const char *pos = truncateBinName(binname);

   if (pos == NULL)
      return(OGLV_BRIDGE_ERR_BAD_BIN_ROOT_PATHNAME);

   // get the current date
   char month[4], day[4], year[6];
   getCurrentDate(month, day, year);

   // get the full pathname for the new bin file
   char binPathname[PTHLENG];
   sprintf(binPathname,"%s%s/%05d",environmentDir,
                                   "/.mti_bin",
                                   masterBinID);

   // open the new bin file
   FILE *fpBin = fopen(binPathname,"wt");
   if (fpBin==NULL)
      return(OGLV_BRIDGE_ERR_BIN_FILE_OPEN_FAILURE);

   // create the new bin header
   BINHEADER bh;
   bh.Clip = 0;
   bh.NClip = 0;
   bh.Crypt = encrypt(0);
   bh.BinID = masterBinID;

   // OGLV binname
   int i=0;
   while (*pos!=0)
      bh.BinName[i++] = *pos++;
   bh.BinName[i] = 0;

   // get the date for the new bin
   pos = month;
   for (i=0;i<4;)
      bh.Month[i++] = *pos++;

   pos = day;
   for (i=0;i<4;)
      bh.Day[i++] = *pos++;

   pos = year;
   for (i=0;i<6;)
      bh.Year[i++] = *pos++;

   bh.RecToComOpt[0] = 30;
   bh.RecToComOpt[1] = 6144;
   bh.RecToComOpt[2] = 1000;
   bh.RecToComOpt[3] = 6144;

   bh.Reserved[0] = 0;
   bh.Reserved[1] = 0;
   bh.Reserved[2] = 0;
   bh.Reserved[3] = 0;

   // output the new header to the new bin file
   if (putBinHeader(fpBin,&bh)==-1)
      return(OGLV_BRIDGE_ERR_BIN_HEADER_WRITE_FAILURE);

   // close the new bin file
   fclose(fpBin);

   // create the new line in the master file
   sprintf(mline[2+masterNClipRet+masterNBin],
           "%s %s, %s BinID=%6d\t%s",
           month,day,year,masterBinID,bh.BinName);

   // advance the values in master file header
   masterBinID++;
   masterNBin++;

   // allocate storage for a new line in master file
   mline[2+masterNClipRet+masterNBin] = new char[LINLENG];

   // bump count of master file lines
   mtiMasterLines++;

   // write out master file
   int status = putMTIMaster();
   if (status != 0)
      return(status);

   // return the index of the bin just created
   return(masterNBin-1);
}

//---------------------------------------------------------------------
int deleteBin(int binidx)
{
   return(0);
}

//----------------------------------------------------------------------
int CBridge::closeOGLV()
{
   int status;

   if (majorState!=BRIDGE_STATE_OPEN)
      return(OGLV_BRIDGE_ERR_BRIDGE_NOT_OPEN);

   // write out master file
   status = putMTIMaster();
   if (status != 0)
      return(status);

   majorState = BRIDGE_STATE_CLOSED;

   return(0);
}

