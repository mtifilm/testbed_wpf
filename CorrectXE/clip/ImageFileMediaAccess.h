// ImageFileMediaAccess.h: interface for the CImageFileMediaAccess class.
//
//////////////////////////////////////////////////////////////////////

#ifndef ImageFileMediaAccessH
#define ImageFileMediaAccessH

#include <string>
#include <iostream>

using std::string;
using std::ostream;

#include "machine.h"
#include "IniFile.h"
#include "MediaAccess.h"
#include "MediaAllocator.h"
#include "MediaDeallocator.h"
#include "MediaDeallocList.h"
#include "MediaFileIoDefs.h"
#include "MediaFrameBuffer.h"

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CImageFileIO;
class CMediaLocation;

//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CImageFileMediaAccess : public CMediaAccess
{
public:
   CImageFileMediaAccess(const string& newMediaIdentifier,
                         const string& newHistoryDirectory,
								 const CMediaFormat& newMediaFormat,
                         EMediaType newMediaType);
   virtual ~CImageFileMediaAccess();

   int initialize(const CMediaFormat& mediaFormat);

   int checkSpace(const CMediaFormat& mediaFormat, MTI_UINT32 frameCount);

   CMediaAllocator* allocate(const CMediaFormat& mediaFormat,
                             const CTimecode& frame0Timecode,
                             MTI_UINT32 frameCount);
   CMediaAllocator* allocateOneFrame(const CMediaFormat& mediaFormat,
                                     const CTimecode& frame0Timecode,
                                     MTI_UINT32 fieldCount);
   int cancelAllocation(CMediaAllocator *mediaAllocator);

   CMediaDeallocator* createDeallocator();
   int deallocate(CMediaDeallocator& deallocator);
   int destroy(const CMediaLocation &mediaLocation);

   MTI_UINT32 getDefaultDataOffset();

   MTI_UINT32 getAdjustedFieldSize(MTI_UINT32 fieldSize);
   MTI_INT64 adjustToBlocking(MTI_INT64 size, bool roundingFlag);

   int Peek(CMediaLocation &mediaLocation, string &filename, MTI_INT64 &offset);

   int queryFreeSpace(MTI_INT64 *totalFreeBytes,
                      MTI_INT64 *maxContiguousFreeBytes);

   bool isClipSpecific() const;

   int read(MTI_UINT8* buffer, CMediaLocation& mediaLocation);
   int readWithOffset(MTI_UINT8* buffer, MTI_UINT8* &offsetBuffer,
							 CMediaLocation& mediaLocation);

//	bool canBeReadAsynchronously(CMediaLocation &mediaLocation);
//	int startAsynchReadWithOffset(MTI_UINT8* buffer,
//											MTI_UINT8* &offsetBuffer,
//											CMediaLocation& mediaLocation,
//											void* &readContext,
//											bool dontNeedAlpha);
//	bool isAsynchReadWithOffsetDone(void* readContext);
//	int finishAsynchReadWithOffset(void* &readContext,
//											 MTI_UINT8* &offsetBuffer);

	int write(const MTI_UINT8* buffer, CMediaLocation& mediaLocation);
   int readMultiple(int count, MTI_UINT8* bufferArray[],
                    CMediaLocation mediaLocationArray[]);
   int readWithOffsetMultiple(int count, MTI_UINT8* bufferArray[],
                              MTI_UINT8* offsetBufferArray[],
                              CMediaLocation mediaLocationArray[]);
   int writeMultiple(int count, MTI_UINT8* bufferArray[],
                     CMediaLocation mediaLocationArray[]);

//	bool canReadFrameBuffers(CMediaLocation &mediaLocation);
//	int startAsyncFrameBufferRead(CMediaLocation& mediaLocation, void* &readContext);
//	bool isAsyncFrameBufferReadDone(void* readContext);
//	int finishAsynchFrameBufferRead(void* &readContext, MediaFrameBuffer* &frameBufferOut);

   int read(MTI_UINT8* buffer, MTI_INT64 offset, unsigned int size);
	int write(const MTI_UINT8* buffer, MTI_INT64 offset, unsigned int size);

	int fetchMediaFile(CMediaLocation &mediaLocation);
	int readMediaFrameBuffer(MediaFrameBufferSharedPtr &frameBuffer, CMediaLocation &mediaLocation);
	int writeMediaFrameBuffer(MediaFrameBufferSharedPtr frameBuffer, CMediaLocation &mediaLocation);

   int hack(EMediaHackCommand hackCommand, CMediaLocation &mediaLocation);
   int move(CMediaLocation &fromLocation, CMediaLocation &toLocation);

   string getImageFileName(const CMediaLocation &mediaLocation) const;
   string getHistoryFileName(const CMediaLocation &mediaLocation) const;
   string getTemplate() const;
   static string adjustMediaIdentifier(const string& mediaIdentifier,
                                       const CTimecode& frame0Timecode);

   void dump(MTIostringstream& str);

//   static int parseFileName(const string& inStr, int* digitIndexPtr = 0,
//                            int* digitCountPtr = 0);
   static int queryImageFileType(EMediaType mediaType);
   static EMediaType queryMediaType(MediaFileType fileType);

  // Static functions used to analyze and create image file names
  // and templates
  static void UseBinarySearchToFindAllFiles(bool flag);
  static int FindFirstFrameOfSequenceThatIncludesThisFrame(const string &fullPathTemplate, int existingFrameNumber);
  static int FindFirstExistingFrameInRange(const string &fullPathTemplate, int lowFrameNumber, int highFrameNumber);
  static int FindLastFrameOfSequenceThatIncludesThisFrame(const string &fullPathTemplate, int existingFrameNumber);
  static int FindLastExistingFrameInRange(const string &fullPathTemplate, int lowFrameNumber, int highFrameNumber);
  static void FindAllFiles (const string &strPrototype, long &lNDigit,
                            long &lFirstFrame, long &lLastFrame);
#if 0
  static void FindAllFilesSlowly(const string &imageFileNameTemplate,
                                 int startFrameNumber, int &firstFrame,
                                 int &lastFrame);
#endif
  static void FindAllFilesBinarily(const string &imageFileNameTemplate,
                                 int startFrameNumber, int &firstFrame,
                                 int &lastFrame);
  static void FindAllFiles2(const string &imageFileNameTemplate,
                            int startFrameNumber, int &firstFrame,
                            int &lastFrame);
  static void FindAllFiles3(const string &imageFileNameTemplate,
                            int startFrameNumber, int &firstFrame,
                            int &lastFrame);
  static void GenerateFileName (const string &strSampleFileName,
                                string &strNewFileName, long lFrame);
  static string GenerateFileName2(const string &fileNameTemplate,
                                  int frameNumber);
  static int GetDigitCount(const string &fileNameTemplate);
  static int getFrameNumberFromFileName(const string &fileName);
  static int getFrameNumberFromFileName2(const string &fileNameTemplate,
                                         const string &fileName);
  static bool IsTemplate(const string &fileName);
  static string MakeImageFileNameDigits(int digitCount, int frameNumber);
  static string MakeTemplate(const string &fileName, bool iniSafe);
  static string RemoveFrameNumberFromTemplate(const string &fileName);
  static int SplitFileName(const string &fileNameTemplate,
                           string &fileNamePrefix,
                           string &fileNameSuffix,
                           string &fileName);
private:

   int getFrameNumber() const;
   string makeImageFileName(const char *fieldName) const;
   string makeHistoryFileName(const char *fieldName) const;

   //  static void FindDigits (const string &str, long &lStartDigit, long &lMaxDigit);
  static void FindDigits2 (const string &str, int &lStartDigit, int &lMaxDigit);
  static void FindFormatting(const string &fileName, int &formatStart,
                      int &formatCount);

private:
   string fileNamePrefix;
   string fileNameSuffix;
   string fileName;

//   CImageFileIO *imageFileIO;
   CImageFormat clipImageFormat;

   static string frameNumberLeftDelimiter;
   static string frameNumberRightDelimiter;
   static string digitFormattingChars;

   static bool useBinarySearchFlag;
};
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class CImageFileMediaAllocator : public CMediaAllocator
{
public:
   CImageFileMediaAllocator(CImageFileMediaAccess *newMediaAccess,
                            const CImageFormat& imageFormat,
                            const CTimecode& inTimecode,
                            MTI_UINT32 newPaddedFieldSize,
                            MTI_UINT32 newFrameCount,
                            bool newInterlacedFlag);
   virtual ~CImageFileMediaAllocator();

   void getFieldMediaAllocation(MTI_UINT32 fieldNumber,
                                CMediaLocation& mediaLocation);

   void getNextFieldMediaAllocation(CMediaLocation& mediaLocation);

   int AssignMediaStorage(C5MediaTrack &mediaTrack);

private:
   string makeFieldName(int fieldNumber);

   CImageFileMediaAllocator();     // Default constructor is deliberately
                                   // inaccessible

   MTI_UINT32 paddedFieldSize;

   bool markFlag;
   bool interlacedFlag;
   int frame0FileNumber;           // File number of frame 0 file

   int minFrameDigitCount;          // Minimum number of digits in
                                    // frame count portion of file name
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class CImageFileMediaDeallocator : public CMediaDeallocator
{
public:
   CImageFileMediaDeallocator();
   virtual ~CImageFileMediaDeallocator();

   int freeMediaAllocation(const CMediaLocation& mediaLocation);
   int freeMediaAllocation(const CMediaLocation& startMediaLocation,
                           int itemCount);

private:
};

#ifdef PRIVATE_STATIC
#ifndef _IMAGE_FILE_MEDIA_ACCESS_DATA_CPP
namespace ImageFileMediaAccessData {
   extern string frameNumberLeftDelimiter;
   extern string frameNumberRightDelimiter;
};
#endif
#endif
//////////////////////////////////////////////////////////////////////

#endif // #ifndef IMAGE_FILE_MEDIA_ACCESS_H
