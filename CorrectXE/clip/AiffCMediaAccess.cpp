// AiffCMediaAccess.cpp: implementation of the CAiffCMediaAccess class.
//
/*
$Header: /usr/local/filmroot/clip/source/AiffCMediaAccess.cpp,v 1.8 2006/11/20 18:34:11 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "err_clip.h"
#include "AiffCMediaAccess.h"
#include "MediaInterface.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//CAiffCMediaAccess::CAiffCMediaAccess()
//: CMediaAccess(MEDIA_TYPE_AIFF_C)
//{
//
//}

CAiffCMediaAccess::CAiffCMediaAccess(const string& newMediaIdentifier,
                                     const string& newHistoryDirectory,
                                     const CMediaFormat& mediaFormat)
: CMediaAccess(MEDIA_TYPE_AIFF_C, newMediaIdentifier,
                                  newHistoryDirectory)
{

}

CAiffCMediaAccess::~CAiffCMediaAccess()
{

}

//////////////////////////////////////////////////////////////////////
// Read and Write Functions
//////////////////////////////////////////////////////////////////////


int CAiffCMediaAccess::read(MTI_UINT8* buffer, CMediaLocation &mediaLocation)
{
   // Not implemented yet
   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

int CAiffCMediaAccess::write(const MTI_UINT8* buffer,
                             CMediaLocation &mediaLocation)
{
   // Not implemented yet
   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

int CAiffCMediaAccess::read(MTI_UINT8* buffer, MTI_INT64 offset, 
                            unsigned int size)
{
   // Not implemented yet
   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

int CAiffCMediaAccess::write(const MTI_UINT8* buffer, MTI_INT64 offset, 
                             unsigned int size)
{
   // Not implemented yet
   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}


//////////////////////////////////////////////////////////////////////

MTI_UINT32 CAiffCMediaAccess::getAdjustedFieldSize(MTI_UINT32 fieldSize)
{
   // Not implemented yet
   return fieldSize;
}

//////////////////////////////////////////////////////////////////////

MTI_INT64 CAiffCMediaAccess::adjustToBlocking(MTI_INT64 size,
                                              bool roundingFlag)
{
   // Not implemented yet
   return size;
}

// ----------------------------------------------------------------------

int CAiffCMediaAccess::Peek(CMediaLocation &mediaLocation,
                            string &filename, MTI_INT64 &offset)
{
   filename = "";

   offset = 0;

   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//
// Function:   queryFreeSpace 
//
// Description:Queries the disk to determine the
//             total available free space on the disk and also the
//             largest available contiguous free area   
//
// Arguments:  MTI_INT64 *totalFreeBytes Ptr to caller's variable to accept
//                                       free size in bytes
//             MTI_INT64 *maxContiguousFreeBytes
//                                     Ptr to caller's variable to accept number of 
//                                     bytes in largest contiguous free space
// Returns:    Returns 0 if query succeed.  Non-zero if failed.
//             Returns non-zero if either of totalBlocks or 
//             maxContiguousBlocks is NULL pointer    
//
//////////////////////////////////////////////////////////////////////
int CAiffCMediaAccess::queryFreeSpace(MTI_INT64 *totalFreeBytes,
                                      MTI_INT64 *maxContiguousFreeBytes)
{
   // Check arguments for null pointers
   if (totalFreeBytes == 0 || maxContiguousFreeBytes == 0)
      return -1;

   *totalFreeBytes = 0;
   *maxContiguousFreeBytes = 0;

   // Not implemented yet
   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

//////////////////////////////////////////////////////////////////////

bool CAiffCMediaAccess::isClipSpecific() const
{
   return true;
}

//////////////////////////////////////////////////////////////////////
// Raw Disk Space Allocation and Deallocation functions
//////////////////////////////////////////////////////////////////////

int CAiffCMediaAccess::checkSpace(const CMediaFormat& mediaFormat,
                                  MTI_UINT32 frameCount)
{
   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

CMediaAllocator*  CAiffCMediaAccess::allocate(const CMediaFormat& mediaFormat,
                                              const CTimecode& frame0Timecode, 
                                              MTI_UINT32 frameCount)
{
   // Not implemented yet
   return 0;
}

int CAiffCMediaAccess::cancelAllocation(CMediaAllocator *mediaAllocator)
{
   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

CMediaDeallocator* CAiffCMediaAccess::createDeallocator()
{
   // Not implemented yet
   return 0;
}

int CAiffCMediaAccess::deallocate(CMediaDeallocator& deallocator)
{
   // Not implemented yet
   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

//////////////////////////////////////////////////////////////////////
// Testing functions
//////////////////////////////////////////////////////////////////////

void CAiffCMediaAccess::dump(MTIostringstream &str)
{
   str << "CAiffCMediaAccess " << std::endl;
   str << " Media Type: " << getMediaType();
   str << "  Media Identifier: <" << getMediaIdentifier() << ">" 
       << std::endl;
}


