// RawDiskMediaAllocator.cpp: implementation of the CRawDiskMediaAllocator class.
//
/*
$Header: /usr/local/filmroot/clip/source/RawDiskMediaAllocator.cpp,v 1.11 2006/03/15 00:28:51 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "RawDiskMediaAllocator.h"

#include "Clip5Proto.h"
#include "MediaLocation.h"
#include "RawDiskFreeList.h"
#include "RawMediaAccess.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRawDiskMediaAllocator::
CRawDiskMediaAllocator(CRawMediaAccess *newMediaAccess, 
                       MTI_INT64 newInitialOffset, 
                       MTI_INT64 newPaddedSize,
                       MTI_UINT32 newPaddedFieldSize,
                       MTI_UINT32 newFieldCount)
: CMediaAllocator(newMediaAccess, newFieldCount),
  initialOffset(newInitialOffset),
  paddedSize(newPaddedSize),
  paddedFieldSize(newPaddedFieldSize),
  allocList(0)
{
}

CRawDiskMediaAllocator::
CRawDiskMediaAllocator(CRawMediaAccess *newMediaAccess,
                       MTI_UINT32 newFieldCount,
                       MTI_UINT32 newPaddedFieldSize,
                       CRawDiskBlockList *newAllocList)
: CMediaAllocator(newMediaAccess, newFieldCount),
  initialOffset(0), paddedSize(0),    // not relevant
  paddedFieldSize(newPaddedFieldSize),
  allocList(newAllocList)
{
}

CRawDiskMediaAllocator::
CRawDiskMediaAllocator(CRawOGLVMediaAccess *newMediaAccess, 
                       MTI_INT64 newInitialOffset, 
                       MTI_INT64 newPaddedSize,
                       MTI_UINT32 newPaddedFieldSize, 
                       MTI_UINT32 newFieldCount)
: CMediaAllocator(newMediaAccess, newFieldCount),
  initialOffset(newInitialOffset),
  paddedSize(newPaddedSize),
  paddedFieldSize(newPaddedFieldSize),
  allocList(0)
{

}

CRawDiskMediaAllocator::~CRawDiskMediaAllocator()
{
   delete allocList;
}

//////////////////////////////////////////////////////////////////////

void CRawDiskMediaAllocator::
getFieldMediaAllocation(MTI_UINT32 fieldNumber,
                        CMediaLocation& mediaLocation)
{
   if (fieldNumber < fieldCount)
      {
      mediaLocation.setMediaAccess(mediaAccess);
      mediaLocation.setFieldName(0);
      mediaLocation.setFieldNumber(fieldNumber);
      mediaLocation.setDataSize(paddedFieldSize);

      if (allocList == 0)
         {
         mediaLocation.setDataIndex(initialOffset
                              + (MTI_INT64)fieldNumber * paddedFieldSize);
         }
      else
         {
         mediaLocation.setDataIndex(allocList->getFieldOffset(fieldNumber));
         }

      mediaLocation.setMarked(false);   // Media Storage is allocated
      }
   else
      {
      // The field number exceeds the number of fields allocated
      // Set mediaLocation to invalid state
      mediaLocation.setInvalid();
      mediaLocation.setMarked(true);
      }

}

void CRawDiskMediaAllocator::
getNextFieldMediaAllocation(CMediaLocation& mediaLocation)
{
   getFieldMediaAllocation(currentAllocationIndex, mediaLocation);
   
   currentAllocationIndex += 1;
}

int CRawDiskMediaAllocator::AssignMediaStorage(C5MediaTrack &mediaTrack)
{
   // for clip 5
   int entryCount = allocList->getEntryCount();
   for (int i = 0; i < entryCount; ++i)
      {
      SRawDiskBlock *rawDiskBlock = allocList->getEntry(i);
      CMediaLocation mediaLocation;
      mediaLocation.setMediaAccess(mediaAccess);
      mediaLocation.setDataIndex(rawDiskBlock->offset);
      mediaLocation.setDataSize(paddedFieldSize);    // one field
      // TBD: Do we care about the mediaLocation's field number?

      C5VideoStoreRange *videoStoreRange
             = new C5VideoStoreRange(mediaLocation, rawDiskBlock->fieldCount);
      mediaTrack.AppendRange(videoStoreRange);
      }

   return 0;
}

CRawDiskBlockList* CRawDiskMediaAllocator::getAllocList()
{
   return allocList;
}

MTI_INT64 CRawDiskMediaAllocator::getInitialOffset() const
{
   return initialOffset;
}

MTI_INT64 CRawDiskMediaAllocator::getPaddedSize() const
{
   return paddedSize;
}

