// -------------------------------------------------------------------------
#include "JobManager.h"

#include "BinManager.h"
#include "FileSweeper.h"
// -------------------------------------------------------------------------

const string JobFoldersName       = "0_job_folders";
const string DrsFolderNamePrefix  = "1_";
const string DrsFolderNameSuffix  = "_drs";
const string UserFolderNamePrefix = "2_";
const string DewarpFolderName     = "dewarp";
const string MasksFolderName      = "masks";
const string PdlsFolderName       = "pdls";
const string ReportsFolderName    = "reports";
const string CutListsFolderName   = "cutlists";
const string StabilizeFolderName  = "stabilize";

// -------------------------------------------------------------------------

string JobManager::MarkInString;
string JobManager::MarkOutString;
string JobManager::ClipInString;
string JobManager::ClipOutString;
string JobManager::CurrentToolCode;
string JobManager::MasterClipFolderPath;
// -------------------------------------------------------------------------

void JobManager::SetMarkStrings(const string &in, const string &out)
{
   MarkInString = in;
   MarkOutString = out;
}
// -------------------------------------------------------------------------

void JobManager::SetClipInOutStrings(const string &in, const string &out)
{
   ClipInString = in;
   ClipOutString = out;
}
// -------------------------------------------------------------------------

void JobManager::SetCurrentToolCode(const string &toolCode)
{
   CurrentToolCode = toolCode;
}
// -------------------------------------------------------------------------

void JobManager::SetMasterClipFolderPath(const string &path)
{
   MasterClipFolderPath = path;
}
// -------------------------------------------------------------------------

string JobManager::GetMasterClipFolderPath()
{
   return MasterClipFolderPath;
}
// -------------------------------------------------------------------------

string JobManager::GetJobRootFolderPath()
{
   return FindJobRootFolder(MasterClipFolderPath);
}
// -------------------------------------------------------------------------

string JobManager::GetReelName()
{
   return ReadReelNameFromJobIniFile(MasterClipFolderPath);
}
// -------------------------------------------------------------------------

int JobManager::CreateJobFolders(const string &rootFolderPath, const string &jobFolderName, const JobInfo &jobInfo)
{
   CBinManager binManager;
   string jobBinName     = jobInfo.GetJobRootFolderName();
   string jobRootBinPath = AddDirSeparator(rootFolderPath) + jobBinName;

   // Make the main job bin
   int retVal = binManager.createBin(rootFolderPath, jobBinName);
   if (retVal != 0)
   {
      TRACE_0(errout << "ERROR: Project Manager can't create bin " << jobRootBinPath);
      return retVal;
   }

   // Save the job info to job.ini
   jobInfo.SaveTo(jobRootBinPath);

   string jobBinPath = AddDirSeparator(rootFolderPath) + jobBinName;

   if (jobInfo.HasDewarpFolder
   || jobInfo.HasMasksFolder
   || jobInfo.HasPdlsFolder
   || jobInfo.HasReportsFolder
   || jobInfo.HasCutListsFolder
   || jobInfo.HasStabilizeFolder)
   {
      string jobFoldersPath = AddDirSeparator(jobBinPath) + JobFoldersName;
      if (!MakeDirectory(jobFoldersPath))
      {
         TRACE_0(errout << "ERROR: Project Manager can't make directory " << jobFoldersPath);
      }
      else
      {
         string notABinPath = AddDirSeparator(jobFoldersPath) + ".not_a_bin";
         FILE *fp = fopen(notABinPath.c_str(), "w");
         fprintf(fp, "This file intentionally left blank.");
         fclose(fp);

         if (jobInfo.HasDewarpFolder)
         {
            retVal = MakeJobFolderDirectories(jobFoldersPath, DewarpFolderName, jobInfo);
            if (retVal != 0)
            {
               return retVal;
            }
         }

         if (jobInfo.HasMasksFolder)
         {
            retVal = MakeJobFolderDirectories(jobFoldersPath, MasksFolderName, jobInfo);
            if (retVal != 0)
            {
               return retVal;
            }
         }

         if (jobInfo.HasPdlsFolder)
         {
            retVal = MakeJobFolderDirectories(jobFoldersPath, PdlsFolderName, jobInfo);
            if (retVal != 0)
            {
               return retVal;
            }
         }

         if (jobInfo.HasReportsFolder)
         {
            retVal = MakeJobFolderDirectories(jobFoldersPath, ReportsFolderName, jobInfo);
            if (retVal != 0)
            {
               return retVal;
            }
         }

         if (jobInfo.HasCutListsFolder)
         {
            retVal = MakeJobFolderDirectories(jobFoldersPath, CutListsFolderName, jobInfo, false);
            if (retVal != 0)
            {
               return retVal;
            }
         }

         if (jobInfo.HasStabilizeFolder)
         {
            retVal = MakeJobFolderDirectories(jobFoldersPath, StabilizeFolderName, jobInfo);
            if (retVal != 0)
            {
               return retVal;
            }
         }
      }
   }

   // Make the root job media folder
   string jobMediaRootPath = AddDirSeparator(jobInfo.MediaLocation) + jobBinName;
   if (!jobInfo.MediaLocation.empty())
   {
      if (!MakeDirectory(jobMediaRootPath))
      {
         TRACE_0(errout << "ERROR: Project Manager can't make directory " <<  jobMediaRootPath);
         return -1;
      }
   }

   // make the media sub-folders
   if (jobInfo.HasDrsMediaFolder)
   {
      string drsFolderName = DrsFolderNamePrefix + jobInfo.Nickname + DrsFolderNameSuffix;
      retVal               = binManager.createBin(jobBinPath, drsFolderName);
      if (retVal != 0)
      {
         TRACE_0(errout << "ERROR: Project Manager can't create bin " << jobBinPath << "\\" << drsFolderName);
         return retVal;
      }

      string reelBinNamePrefix = jobInfo.Nickname + DrsFolderNameSuffix + "_r";
      retVal = MakeReelBins(AddDirSeparator(jobBinPath) + drsFolderName, reelBinNamePrefix, jobInfo);
      if (retVal != 0)
      {
         return retVal;
      }

      if (!jobInfo.MediaLocation.empty())
      {
         string mediaFolder = AddDirSeparator(jobMediaRootPath) + drsFolderName;
         retVal = MakeMediaFolderDirectories(mediaFolder, DrsFolderNameSuffix, jobInfo);
         if (retVal != 0)
         {
            return retVal;
         }
      }
   }

   if (jobInfo.HasUserMediaFolder)
   {
      string userFolderName = UserFolderNamePrefix + jobInfo.Nickname + "_" + jobInfo.UserMediaFolderName;
      retVal                = binManager.createBin(jobBinPath, userFolderName);
      if (retVal != 0)
      {
         TRACE_0(errout << "ERROR: Project Manager can't create bin " << jobBinPath << "\\" << userFolderName);
         return retVal;
      }

      string reelBinNamePrefix = (jobInfo.Nickname.empty()
									? (string)""
                                    : (jobInfo.Nickname + "_"))
                                  + (jobInfo.UserMediaFolderName.empty()
                                      ? (string)"r"
                                      : (jobInfo.UserMediaFolderName + "_r"));
      retVal = MakeReelBins(AddDirSeparator(jobBinPath) + userFolderName, reelBinNamePrefix, jobInfo);
      if (retVal != 0)
      {
         return retVal;
      }

      if (!jobInfo.MediaLocation.empty())
      {
         string mediaFolder = AddDirSeparator(jobMediaRootPath) + userFolderName;
         retVal = MakeMediaFolderDirectories(mediaFolder, jobInfo.UserMediaFolderName, jobInfo);
         if (retVal != 0)
         {
            return retVal;
         }
      }
   }

   // If neither DRS nor user media folder was specified, media goes in the
   // job media root directory.
   if (!jobInfo.HasDrsMediaFolder && !jobInfo.HasUserMediaFolder)
   {
      string reelDirectoryNamePrefix = jobInfo.Nickname.empty() ? (string)"r" : jobInfo.Nickname + "_r";
      retVal = MakeReelBins(jobBinPath, reelDirectoryNamePrefix, jobInfo);
      if (retVal != 0)
      {
         return retVal;
      }

      if (!jobInfo.MediaLocation.empty())
      {
         retVal = MakeReelDirectories(jobMediaRootPath, reelDirectoryNamePrefix, jobInfo);
         if (retVal != 0)
         {
            return retVal;
         }
      }
   }

   return 0;
}
// -------------------------------------------------------------------------

int JobManager::MakeJobFolderDirectories(const string &jobBinPath, const string &jobFolderName, const JobInfo &jobInfo, bool wantReelDirectories)
{
   string jobFolderPath = AddDirSeparator(jobBinPath) + jobFolderName;
   if (!DoesDirectoryExist(jobFolderPath) && !MakeDirectory(jobFolderPath))
   {
      TRACE_0(errout << "ERROR: Project Manager can't make directory " << jobFolderPath);
      return -1;
   }

   if (!wantReelDirectories)
   {
      return 0;
   }

   return MakeReelDirectories(jobFolderPath, "r", jobInfo);
}
// -------------------------------------------------------------------------

int JobManager::MakeMediaFolderDirectories(const string &mediaFolderPath, const string &mediaId, const JobInfo &jobInfo)
{
   if (!DoesDirectoryExist(mediaFolderPath) && !MakeDirectory(mediaFolderPath))
   {
      TRACE_0(errout << "ERROR: Project Manager can't make directory " << mediaFolderPath);
      return -1;
   }

   string reelDirectoryNamePrefix = jobInfo.Nickname + "_" + mediaId + "_r";
   return MakeReelDirectories(mediaFolderPath, reelDirectoryNamePrefix, jobInfo);
}
// -------------------------------------------------------------------------

int JobManager::MakeOneReelBin(const string &rootPath, const string &binName, const string &reelName)
{
   string binPath  = AddDirSeparator(rootPath) + binName;
   CBinManager binManager;

   int retVal = binManager.createBin(rootPath, binName);
   if (retVal != 0)
   {
      TRACE_0(errout << "ERROR: Project Manager can't create bin " << binPath);
      return retVal;
   }

   WriteReelNameToJobIniFile(binPath, reelName);
   return 0;
}
// -------------------------------------------------------------------------

int JobManager::MakeReelBins(const string &rootPath, const string &binNamePrefix, const JobInfo &jobInfo)
{
   int retVal = 0;

   // This does nothing if reel count is 0
   for (int reel = 1; reel <= jobInfo.ReelCount; ++reel)
   {
      MTIostringstream os;
      os << reel;
      string binNameBase = binNamePrefix + os.str();
      string reelNameBase = "r" + os.str();

      if (jobInfo.IsASlashB && !jobInfo.IsCombined)
      {
         retVal = MakeOneReelBin(rootPath, binNameBase + "a", reelNameBase + "a");

         if (retVal == 0 && (reel != jobInfo.ReelCount || !jobInfo.IsLastReelAOnly))
         {
            retVal = MakeOneReelBin(rootPath, binNameBase + "b", reelNameBase + "b");
         }
      }
      else
      {
         retVal = MakeOneReelBin(rootPath, binNameBase, reelNameBase);
      }
   }

   return retVal;
}
// -------------------------------------------------------------------------

int JobManager::MakeReelDirectories(const string &rootPath, const string &directoryNamePrefix, const JobInfo &jobInfo)
{
   // This does nothing if reel count is 0
   for (int reel = 1; reel <= jobInfo.ReelCount; ++reel)
   {
      MTIostringstream os;
      os << directoryNamePrefix << reel;
      string reelName = os.str();

      if (jobInfo.IsASlashB && !jobInfo.IsCombined)
      {
         string directoryName = AddDirSeparator(rootPath) + reelName + "a";
         if (!DoesDirectoryExist(directoryName) && !MakeDirectory(directoryName))
         {
            TRACE_0(errout << "ERROR: Project Manager can't make directory " << directoryName);
            return -1;
         }

         if (reel != jobInfo.ReelCount || !jobInfo.IsLastReelAOnly)
         {
            directoryName = AddDirSeparator(rootPath) + reelName + "b";
            if (!DoesDirectoryExist(directoryName) && !MakeDirectory(directoryName))
            {
               TRACE_0(errout << "ERROR: Project Manager can't make directory " << directoryName);
               return -1;
            }
         }
      }
      else
      {
         string directoryName = AddDirSeparator(rootPath) + reelName;
         if (!DoesDirectoryExist(directoryName) && !MakeDirectory(directoryName))
         {
            TRACE_0(errout << "ERROR: Project Manager can't make directory " << directoryName);
            return -1;
         }
      }
   }

   return 0;
}
// -------------------------------------------------------------------------

// Compare bin paths without worrying about freakin' trailing separators
namespace
{
   inline bool IsSameBinPath(const string &path1, const string &path2)
   {
      return EqualIgnoreCase(AddDirSeparator(path1), AddDirSeparator(path2));
   }
}
// -------------------------------------------------------------------------

string JobManager::FindJobRootFolder(const string &startingPath)
{
   string retVal;
   string path = startingPath;

   while (!JobInfo::IsAJobFolder(path))
   {
      string folder = GetFileLastDir(AddDirSeparator(path));
      if (folder.empty())
      {
         // At disk root; can't go farther
         path = "";
         break;
      }

      path = RemoveDirSeparator(GetFilePath(path));
   }

   return path;
}
// -------------------------------------------------------------------------

string JobManager::GetJobFolderSubfolderPath(JobFolderType jobFolderType)
{
   if (MasterClipFolderPath.empty())
   {
      return "";
   }

   if (jobFolderType == unknownType)
   {
      return MasterClipFolderPath;
   }

   string jobRootFolder = FindJobRootFolder(MasterClipFolderPath);
   if (jobRootFolder.empty())
   {
      return MasterClipFolderPath;
   }

   JobInfo jobInfo;
   jobInfo.LoadFrom(jobRootFolder);
   bool useJobFolder = false;
   bool hasReelSubfolders = false;
   string folderName;

   switch (jobFolderType)
   {
      case dewarpType:
         if (jobInfo.HasDewarpFolder)
         {
            useJobFolder = true;
            folderName = DewarpFolderName;
            hasReelSubfolders = true;
         }
         break;

      case masksType:
         if (jobInfo.HasMasksFolder)
         {
            useJobFolder = true;
            folderName = MasksFolderName;
            hasReelSubfolders = true;
         }
         break;

      case pdlsType:
         if (jobInfo.HasPdlsFolder)
         {
            useJobFolder = true;
            folderName = PdlsFolderName;
            hasReelSubfolders = true;
         }
         break;

      case reportsType:
         if (jobInfo.HasReportsFolder)
         {
            useJobFolder = true;
            folderName = ReportsFolderName;
            hasReelSubfolders = true;
        }
         break;

      case cutListsType:
         if (jobInfo.HasCutListsFolder)
         {
            useJobFolder = true;
            folderName = CutListsFolderName;
            hasReelSubfolders = false;
         }
         break;

      case stabilizeType:
         if (jobInfo.HasStabilizeFolder)
         {
            useJobFolder = true;
            folderName = StabilizeFolderName;
            hasReelSubfolders = true;
         }
        break;

      case unknownType:
         // Making compiler stop bitching - can't actually get here.
         break;
   }

   if (!useJobFolder)
   {
      return MasterClipFolderPath;
   }

   string jobFolderPath = AddDirSeparator(AddDirSeparator(jobRootFolder) + JobFoldersName) + folderName;
   string reelName = ReadReelNameFromJobIniFile(MasterClipFolderPath);
   if (!reelName.empty() && hasReelSubfolders)
   {
      jobFolderPath = AddDirSeparator(jobFolderPath) + reelName;
   }

   return jobFolderPath;
}
// -------------------------------------------------------------------------

bool JobManager::DoesJobHaveJobFolderSubfolder(JobFolderType jobFolderType)
{
   string jobRootFolder = FindJobRootFolder(MasterClipFolderPath);
   if (jobRootFolder.empty())
   {
      return false;
   }

   JobInfo jobInfo;
   jobInfo.LoadFrom(jobRootFolder);
   bool retVal;

   switch (jobFolderType)
   {
      case dewarpType:
         retVal = jobInfo.HasDewarpFolder;
         break;

      case masksType:
         retVal = jobInfo.HasMasksFolder;
         break;

      case pdlsType:
         retVal = jobInfo.HasPdlsFolder;
         break;

      case reportsType:
         retVal = jobInfo.HasReportsFolder;
         break;

      case cutListsType:
         retVal = jobInfo.HasCutListsFolder;
         break;

      case stabilizeType:
         retVal = jobInfo.HasStabilizeFolder;
         break;

      default:
      case unknownType:
         retVal = false;
         break;
   }

   return retVal;
}
// -------------------------------------------------------------------------

void JobManager::WriteReelNameToJobIniFile(const string &binPath, const string &reelName)
{
   string jobIniFilePath = AddDirSeparator(binPath) + JOB_INI_FILE_NAME;
   CIniFile *jobIniFile = CreateIniFile(jobIniFilePath);
   if (jobIniFile == NULL)
   {
      TRACE_0(errout << "ERROR: Project Manager can't write ini file " << jobIniFilePath);
   }

   jobIniFile->WriteString("JobBinInfo", "ReelName", reelName);
   DeleteIniFile(jobIniFile);
}
// -------------------------------------------------------------------------

string JobManager::ReadReelNameFromJobIniFile(const string &startingPath)
{
   string retVal;
   string path = startingPath;

   while (true)
   {
      string jobIniFilePath = AddDirSeparator(path) + JOB_INI_FILE_NAME;
      CIniFile *jobIniFile = CreateIniFile(jobIniFilePath);
      if (jobIniFile != NULL)
      {
         if (jobIniFile->KeyExists("JobBinInfo", "ReelName"))
         {
            retVal = jobIniFile->ReadString("JobBinInfo", "ReelName", "");
            DeleteIniFile(jobIniFile);
            break;
         }

         DeleteIniFile(jobIniFile);
      }

      string folder = GetFileLastDir(AddDirSeparator(path));
      if (folder.empty())
      {
         // At disk root; can't go farther
         break;
      }

      path = RemoveDirSeparator(GetFilePath(path));
   }

   return retVal;
}
// -------------------------------------------------------------------------

string JobManager::GetJobFolderFileSavePath(const string &jobFolder, const string &acronym, bool useClipInOut, const string &extension)
{
   return AddDirSeparator(jobFolder) + GetJobFolderFileName(jobFolder, acronym, useClipInOut, extension, false);
}
// -------------------------------------------------------------------------

string JobManager::GetJobFolderFileSaveName(const string &jobFolder, const string &acronym, bool useClipInOut, const string &extension)
{
   return GetJobFolderFileName(jobFolder, acronym, useClipInOut, extension, false);
}
// -------------------------------------------------------------------------

string JobManager::GetJobFolderFileLoadPath(const string &jobFolder, const string &acronym, bool useClipInOut, const string &extension)
{
   return AddDirSeparator(jobFolder) + GetJobFolderFileName(jobFolder, acronym, useClipInOut, extension, true);
}
// -------------------------------------------------------------------------

string JobManager::GetJobFolderFileLoadName(const string &jobFolder, const string &acronym, bool useClipInOut, const string &extension)
{
   return GetJobFolderFileName(jobFolder, acronym, useClipInOut, extension, true);
}
// -------------------------------------------------------------------------

string JobManager::GetJobFolderFileName(const string &jobFolder, const string &acronym, bool useClipInOut, const string &extension, bool loading)
{
   MTIostringstream os;
   string reelName = ReadReelNameFromJobIniFile(MasterClipFolderPath);
   if (!reelName.empty())
   {
      os << reelName << "_";
   }

   os << (acronym.empty() ? (CurrentToolCode.empty() ? (string)"xx" : CurrentToolCode) : acronym);

   string inString = (useClipInOut || MarkInString.empty()) ? ClipInString : MarkInString;
   string outString = (useClipInOut || MarkOutString.empty()) ? ClipOutString : MarkOutString;

   if (!inString.empty())
   {
      os << "_" << inString;
   }

   if (!outString.empty())
   {
      os << "_" << outString;
   }

   os << "_";
   string filenameBase = os.str();
   int version;
   int highestVersion = 0;
   for (version = 1; version <= 98; ++version)
   {
      os.str("");
      os << AddDirSeparator(jobFolder) << filenameBase << version << "." << extension;
      if (DoesFileExist(os.str()))
      {
         highestVersion = version;
      }
   }

   os.str("");
   if (loading )
   {
      if (highestVersion > 0)
      {
         os << filenameBase << highestVersion << "." << extension;
      }
   }
   else
   {
      // Saving - bump version number
      os << filenameBase << (highestVersion + 1) << "." << extension;
   }

   return os.str();
}
// -------------------------------------------------------------------------

string JobManager::GetUnversionedJobFolderFilePath(
   const string &id,
   const string &jobFolder,
   const string &extension)
{
   return AddDirSeparator(jobFolder) + GetUnversionedJobFolderFileName(id, jobFolder, extension);
}
// -------------------------------------------------------------------------

string JobManager::GetUnversionedJobFolderFileName(
   const string &id,
   const string &jobFolder,
   const string &extension)
{
   MTIostringstream os;
   string reelName = ReadReelNameFromJobIniFile(MasterClipFolderPath);
   if (!reelName.empty())
   {
      os << reelName << "_";
   }

   os << id << "." << extension;
   return os.str();
}
// -------------------------------------------------------------------------


