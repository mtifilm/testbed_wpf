#include <iostream>

#include "MediaInterface.h"
#include "MediaLocation.h"
#include "MediaAllocator.h"

//////////////////////////////////////////////////////////////////////


int main(int argc, char* argv[])
{
   //////////////////////////////////////////////////////////////////////
   //               Initialization Test
   //////////////////////////////////////////////////////////////////////

   // Create a single instance of the CMediaInterface class
   // This instance provides the read, write and allocation interface 
   // for all media storage types (raw disk, file system,etc.)
   CMediaInterface mediaInterface; 

   // Initialize the media interface.  Among other things, this reads
   // in the raw disk configuration file rawdisk.cfg
   cout << "Attempting to initialize the CMediaInterface object" << endl;
   if(!mediaInterface.initialize())
      {
      cerr << "ERROR: Initialization of CMediaInterface object failed"
           << endl;
      return 1;
      }

   // Print out the information in the media interface
   // For each raw disk:
   //    Raw disk configuration
   //    Free List
   mediaInterface.dump(cout);

        return 0;
}
