#include "Flist.h"
#include <stdlib.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>

#if defined(__sgi) || defined(__linux) // UNIX compilers
#include <unistd.h>

#elif defined(_WIN32)   // Win32 API, MS VC++ & Borland C++Builder
#include <time.h>      // time_t is broken in sys/types.h in RadStudio XE4
#include <sys/types.h>
#include <io.h>

#else
#error Unknown Compiler
#endif

const int CFlist::blfactr = 1024;

CFlist::CFlist(char *flname)
{
   int i = 0;

   // register the FLIST filename (typ "nnnnn")

   while ((*flname)&&(i<MAX_LENG-1)) {
      flistName[i++] = *flname++;
   }
   while (i<MAX_LENG) flistName[i++] = 0;

   headerBuffer = NULL;
   fieldBuffer  = NULL; 
}

CFlist::~CFlist()
{
   delete headerBuffer;

   delete fieldBuffer;
}

int CFlist::createFlist()
{
  // init the field phase
  fieldPhase = 0;

  // memory for header

  headerBuffer = new Flist_Header;

  // memory for fields

  fieldBuffer = new Flist_Elem[blfactr];
  fieldBufferPtr = fieldBuffer;

  curTrack  = -1;

  // curOffset is the file offset corresponding to the
  // beg of the Flist_elem buffer. It is advanced during
  // generation of each track, and reset when each track
  // is closed. When the latter happens the filling ptr
  // must be reset to the beg of the Flist_elem buffer (!)

  curOffset =  sizeof(Flist_Header);

  // allocate the file

  if ((flistFileDescriptor=creat(flistName,0x1FF))==-1) return(-1);

  // init the header contents

  headerBuffer->laFirstElem[0] = -1;
  headerBuffer->laFirstElem[1] = -1;
  headerBuffer->laFirstElem[2] = -1;

  headerBuffer->laNElem[0] = -1;
  headerBuffer->laNElem[1] = -1;
  headerBuffer->laNElem[2] = -1;

  return(0);
}

void CFlist::openTrack(int trkidx)
{
   // set the current track
   // 0 for VIDEO, 1 for FILM, 2 for MANUAL 
   curTrack = trkidx; 

   // init the first track element's offset
   headerBuffer->laFirstElem[curTrack] = curOffset;

   // init the track's element count 
   headerBuffer->laNElem[curTrack]     = 0;

   // init the pointer for filling the Flist_elem buffer
   fieldBufferPtr = fieldBuffer;

   // init number of frame elements in buffer
   curFrames = 0;
}

void CFlist::openFrame(int h, int m, int s, int f)
{
   fieldBufferPtr->laBlockOffset[0] = 0;
   fieldBufferPtr->laBlockOffset[1] = 0;
   fieldBufferPtr->laBlockOffset[2] = 0;

   fieldBufferPtr->tcTime.iH = h;
   fieldBufferPtr->tcTime.iM = m;
   fieldBufferPtr->tcTime.iS = s;
   fieldBufferPtr->tcTime.iF = f;

   fieldBufferPtr->iHalf = TC_NONE;

   fieldPhase = 0;
}

void CFlist::addField(long blkoffs) 
{
   fieldBufferPtr->laBlockOffset[fieldPhase] = blkoffs;
   fieldPhase++;
}

int CFlist::addFrame(int hlfcod)
{
   // drop in the TC bits
   //
   // Field 3:2 pulldown (525, 1080i, etc)
   // Video:   AA  BB  BC  CD  DD
   // Film:    AA  BB    CC    DD
   // frames labelled as follows:
   //   BB is TC_PAIR0      0x4000
   //   CC is TC_HALF       0x20
   //   DD is TC_PAIR1      0x8000
   //
   // Frame 3:2 pulldown (720P_60 and 720P_5994)
   // Video:  A  A  B  B  B  C  C  D  D  D
   // Film:   A     B        C     D
   // frames labelled as follows:
   //   A is TC_PAIR_NEXT1 0x100
   //   B is TC_PAIR_NEXT2 0x200
   //
   fieldBufferPtr->iHalf = hlfcod;

   fieldBufferPtr++;
   if (++curFrames==blfactr) {
      lseek(flistFileDescriptor,curOffset,SEEK_SET);
      int bytsout = blfactr*sizeof(Flist_Elem);
      if (write(flistFileDescriptor,(char *)fieldBuffer,bytsout)!=bytsout)
         return(-1);
      curOffset += bytsout;
      curFrames = 0;
      fieldBufferPtr = fieldBuffer;
   }

   // bump the count of frame elements
   headerBuffer->laNElem[curTrack]++;

   return(0);
}

int CFlist::closeTrack()
{
   // if closing VIDEO track, make offset and element count
   // the same for the FILM track
   if (curTrack==FLIST_TRACK_VIDEO) {
      headerBuffer->laFirstElem[1] = headerBuffer->laFirstElem[0]; 
      headerBuffer->laNElem[1] = headerBuffer->laNElem[0];
   }
  
   // write out remaining frames

   lseek(flistFileDescriptor,curOffset,SEEK_SET);
   int bytsout = curFrames*sizeof(Flist_Elem);
   if (write(flistFileDescriptor,(char *)fieldBuffer,bytsout)!=bytsout)
      return(-1);
   curOffset += bytsout;

   return(0);
}

int CFlist::closeFlist()
{
   // write out header

   lseek(flistFileDescriptor,0,SEEK_SET);
   int bytsout = sizeof(Flist_Header);
   if (write(flistFileDescriptor,(char *)headerBuffer,bytsout)!=bytsout)
      return(-1);

   return(close(flistFileDescriptor));
}

