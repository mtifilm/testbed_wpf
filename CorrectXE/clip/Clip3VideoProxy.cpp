// Clip3VidoeProxy.cpp: implementation of the CVideoProxy class.
//
/*
$Header: /usr/local/filmroot/clip/source/Clip3VideoProxy.cpp,v 1.30 2007/08/02 10:41:43 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "BinManager.h"
#include "Clip3.h"
#include "Clip3VideoTrack.h"
#include "err_clip.h"
#include "ImageFormat3.h"
#include "IniFile.h"
#include "MediaAllocator.h"
#include "MediaAccess.h"

//////////////////////////////////////////////////////////////////////
// Static Data Members Initialization
//////////////////////////////////////////////////////////////////////

// Video Proxy List and Video Proxy Info Sections
string CVideoProxyList::videoProxySection = "VideoProxy";

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                        #######################################
// ###    CVideoProxy Class     ######################################
// ####                        #######################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CVideoProxy::CVideoProxy(ClipSharedPtr &newParentClip, EClipVersion newClipVer,
                         int newTrackNumber)
: CTrack(newParentClip, newClipVer, newTrackNumber)
{
   mediaFormat = new CImageFormat;
}

CVideoProxy::~CVideoProxy()
{
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

const CImageFormat* CVideoProxy::getImageFormat() const
{
   if (clipVer == CLIP_VERSION_3)
      return static_cast<const CImageFormat*>(mediaFormat);

   else if (clipVer == CLIP_VERSION_5L)
      return static_cast<const CImageFormat*>(clip5TrackHandle.GetMediaFormat());

   else
      return 0; // unexpected clip version
}

int CVideoProxy::getProxyNumber() const
{
   return trackNumber;
}

CVideoFrameList* CVideoProxy::getVideoFrameList(int framingIndex) const
{
   return static_cast<CVideoFrameList*>(getBaseFrameList(framingIndex));
}

//////////////////////////////////////////////////////////////////////
//  Mutators
//////////////////////////////////////////////////////////////////////

void CVideoProxy::setImageFormat(const CImageFormat& newImageFormat)
{
   CImageFormat *imageFormat = static_cast<CImageFormat*>(mediaFormat);
   *imageFormat = newImageFormat;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CVideoProxy::extendVideoProxy(int newFrameCount, bool allocateMedia)
{
   int retVal;

   if (getClipVer() == CLIP_VERSION_3)
      {
      retVal = extendVideoProxy_ClipVer3(newFrameCount, allocateMedia);
      }
   else if (getClipVer() == CLIP_VERSION_5L)
      {
      // Clip 5 "Lite"
      retVal = extendVideoProxy_ClipVer5L(newFrameCount, allocateMedia);
      }
   else
      {
      TRACE_0(errout << "ERROR: CVideoProxy::extendVideoProxy: Invalid Clip Version: "
                     << getClipVer());
      retVal = CLIP_ERROR_INVALID_CLIP_VERSION;
      }

   return retVal;
}

int CVideoProxy::extendVideoProxy_ClipVer5L(int newFrameCount,
                                            bool allocateMedia)
{
   int retVal;

   // Two scenarios:
   // 1) If the caller wants to allocate media (allocateMedia == true),
   //   then a new media track is created and media storage is allocated.
   //   A new pulldown range that points to the media track is appended
   //   to the existing sequence track
   // 2) If the caller does not want to allocate media, then a filler
   //    range is appended to the existing sequence track

   int newAllocItemCount = (allocateMedia) ? newFrameCount : 0;

   string clipPath = getParentClip()->getClipPathName();

   retVal = clip5TrackHandle.ExtendMediaTrack(clipPath, newFrameCount,
                                              newAllocItemCount);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CVideoProxy::extendVideoProxy_ClipVer3(int newFrameCount,
                                           bool allocateMedia)
{
   int retVal;

   // Extend the field list that is shared by all frame lists
   retVal = extendFieldList(newFrameCount);

   // Extend the video-frames frame list by adding new frames
   CVideoFrameList *videoFramesFrameList
                                      = getVideoFrameList(FRAMING_SELECT_VIDEO);
   retVal = videoFramesFrameList->extendVideoFrames_FrameList(newFrameCount);
   if (retVal != 0)
      return retVal; // ERROR

   int startFrame = videoFramesFrameList->getTotalFrameCount();

   CMediaStorage *mediaStorage = 0;
   CMediaAccess *mediaAccess = 0;
   CMediaAllocator *mediaAllocator = 0;
   if (getVirtualMediaFlag())
      {
      retVal = assignVirtualMedia(startFrame, newFrameCount,
                                  getImageFormat()->getBytesPerField());
      if (retVal != 0)
         return retVal; // ERROR
      }
   else if (allocateMedia)
      {
      // Allocate the new media storage from the first media storage item
      // in this track's media storage list.

      mediaStorage = mediaStorageList.getMediaStorage(0);
      if (mediaStorage == 0)
          return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR; // no media storage ??
      mediaAccess = mediaStorage->getMediaAccessObj();
      if (mediaAccess == 0)
          return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR; // no media access??

      CTimecode frame0Timecode
                           = videoFramesFrameList->getTimecodeForFrameIndex(0);
      mediaAllocator = mediaAccess->extend(*getMediaFormat(), frame0Timecode,
                                           newFrameCount);
      if (mediaAllocator == 0)
         return CLIP_ERROR_MEDIA_ALLOCATION_FAILED; // ERROR: Allocation failed

      // Assign the media storage to each frame/field in the frame list
      retVal = assignMediaStorage(*mediaAllocator, startFrame, newFrameCount);
      if (retVal != 0)
         {
         mediaAccess->cancelExtend(mediaAllocator);
         delete mediaAllocator;
         return retVal;
         }

      // Update the frame list's allocated frame count and allocation frontier
      videoFramesFrameList->getMediaAllocatedFrontier(true);
      }
   else
      {
      }

   // If relevant, extend film-frames frame list
   CVideoFrameList *filmFramesFrameList
                                     = getVideoFrameList(FRAMING_SELECT_FILM);
   if (filmFramesFrameList != videoFramesFrameList)
      {
      // Calculate a default AA frame timecode based on the
      // whole hour portion of the clip's in timecode
      CTimecode videoFramesInTimecode = videoFramesFrameList->getInTimecode();
      CTimecode AAFrameTimecode = GetWholeHourTimecode(videoFramesInTimecode);
      int AAFrameIndex = AAFrameTimecode - videoFramesInTimecode;
      AAFrameIndex += videoFramesFrameList->getTotalFrameCount();

      int startFrame = videoFramesFrameList->getTotalFrameCount();
      int outFrameIndex = startFrame + newFrameCount;
      EImageFormatType imageFormatType = getImageFormat()->getImageFormatType();
      EPulldown nominalPulldown
                           = CImageInfo::queryNominalPulldown(imageFormatType);
      EFrameLabel *frameLabels = new EFrameLabel[newFrameCount];
      CVideoFrameList::create3_2PulldownFrameLabels(startFrame, outFrameIndex,
                                                    AAFrameIndex,
                                                    nominalPulldown,
                                                    frameLabels);

      retVal
         = filmFramesFrameList->extendFilmFrames_FrameList(videoFramesFrameList,
                                                           startFrame,
                                                           newFrameCount,
                                                           nominalPulldown,
                                                           frameLabels);
      delete [] frameLabels;

      if (retVal != 0)
         {
         if (mediaAllocator != 0)
            {
            mediaAccess->cancelExtend(mediaAllocator);
            delete mediaAllocator;
            }
         return retVal;   // ERROR
         }
      }

   // If relevant, delete the video-fields framing frame lists.  Each
   // will be reconstructed when first read
   CVideoFrameList *videoFieldsFrameList
                                = getVideoFrameList(FRAMING_SELECT_FIELDS_1_2);
   if (videoFieldsFrameList != 0)
      {
      videoFieldsFrameList->deleteFrameList();
      retVal = videoFieldsFrameList->initVideoFields_FrameList();
      if (retVal != 0)
         {
         if (mediaAllocator != 0)
            {
            mediaAccess->cancelExtend(mediaAllocator);
            delete mediaAllocator;
            }
         return retVal;   // ERROR
         }
      }
   videoFieldsFrameList = getVideoFrameList(FRAMING_SELECT_FIELD_1_ONLY);
   if (videoFieldsFrameList != 0)
      {
      videoFieldsFrameList->deleteFrameList();
      retVal = videoFieldsFrameList->initVideoFields_FrameList();
      if (retVal != 0)
         {
         if (mediaAllocator != 0)
            {
            mediaAccess->cancelExtend(mediaAllocator);
            delete mediaAllocator;
            }
         return retVal;   // ERROR
         }
      }
   videoFieldsFrameList = getVideoFrameList(FRAMING_SELECT_FIELD_2_ONLY);
   if (videoFieldsFrameList != 0)
      {
      videoFieldsFrameList->deleteFrameList();
      retVal = videoFieldsFrameList->initVideoFields_FrameList();
      if (retVal != 0)
         {
         if (mediaAllocator != 0)
            {
            mediaAccess->cancelExtend(mediaAllocator);
            delete mediaAllocator;
            }
         return retVal;   // ERROR
         }
      }

   videoFieldsFrameList = getVideoFrameList(FRAMING_SELECT_CADENCE_REPAIR);
   if (videoFieldsFrameList != 0)
      {
      videoFieldsFrameList->deleteFrameList();
      retVal = videoFieldsFrameList->initVideoFields_FrameList();
      if (retVal != 0)
         {
         if (mediaAllocator != 0)
            {
            mediaAccess->cancelExtend(mediaAllocator);
            delete mediaAllocator;
            }
         return retVal;   // ERROR
         }
      }

   // Update the Field List file
   retVal = updateFieldListFile();
   if (retVal != 0)
      {
      if (mediaAllocator != 0)
         {
         // Cancel media storage allocation
         if (mediaAllocator != 0)
            {
            mediaAccess->cancelExtend(mediaAllocator);
            delete mediaAllocator;
            }
         return retVal; // ERROR: Failed to write Field List file
         }
      }

   // Write the updated frame list (.trk) files
   retVal = writeAllFrameListFiles();
   if (retVal != 0)
      {
      if (mediaAllocator != 0)
         {
         // Cancel media storage allocation
         mediaAccess->cancelExtend(mediaAllocator);
         delete mediaAllocator;
         }
      return retVal; // ERROR: Failed to write Frame List file
      }

   // Write the updated clip (.clp) file
   retVal = getParentClip()->writeFile();
   if (retVal != 0)
      {
      if (mediaAllocator != 0)
         {
         // Cancel media storage allocation
         mediaAccess->cancelExtend(mediaAllocator);
         delete mediaAllocator;
         }
      return retVal; // ERROR: Failed to write Frame List file
      }

   delete mediaAllocator;

   return 0;

} // extendVideoProxy

int CVideoProxy::shortenVideoProxy(int frameCount)
{
   int retVal;

   CVideoFrameList *videoFramesFrameList
                                      = getVideoFrameList(FRAMING_SELECT_VIDEO);
   int totalFrameCount = videoFramesFrameList->getTotalFrameCount();

   if (totalFrameCount < frameCount)
      return CLIP_ERROR_INVALID_FRAME_NUMBER;  // ERROR: arg too big

   if (frameCount == 0)
      return 0;   // Nothing to do

   int startFrame = totalFrameCount - frameCount;

   // Release the media storage
   retVal = releaseMediaStorage(startFrame, frameCount);
   if (retVal != 0)
      return retVal; // ERROR: could not release media storage

   // Shorten all of the frame lists:
   // 1) Shorten the video-frames frame list by removing the CVideoFrames from
   //    the frame list.
   // 2) Shorten the film-frames frame list (if it exists) by recreating
   //    it from the video-frames frame list based on either the .diff file
   //    or default 3:2 pull-down.
   // 3) Shorten the video-fields frame lists (if they exist) by deleting
   //    and letting them be re-created when accessed.

   // Truncate the video-frames frame list
   retVal = videoFramesFrameList->truncateFrameList(startFrame);
   if (retVal != 0)
      return retVal; // ERROR: could not truncate the frame list

   // If relevant, truncate the film-frames frame list
   CVideoFrameList *filmFramesFrameList
                                     = getVideoFrameList(FRAMING_SELECT_FILM);
   if (filmFramesFrameList != videoFramesFrameList)
      {
      // Make the Diff File name
      CBinManager binMgr;
      auto clip = getParentClip();
      string diffFileName = binMgr.makeClipFileName(clip->getBinPath(),
                                                    clip->getClipName());
      diffFileName += ".diff";  // This shouldn't be hard-coded

      EImageFormatType imageFormatType = getImageFormat()->getImageFormatType();
      EPulldown nominalPulldown
                            = CImageInfo::queryNominalPulldown(imageFormatType);

      if (DoesFileExist(diffFileName))
         {
         // Recreate the film-frames frame list based on the existing
         // diff file and video-frames frame list.
         retVal = filmFramesFrameList->createFrameListFromDiffFile(diffFileName,
                                                               nominalPulldown);
         if (retVal != 0)
            return retVal; // ERROR
         }
      else
         {
         // The diff file is not available, so we must recreate the film-frames
         // frame list based on a default 3:2 pull-down.  We may still have
         // good pull-down info in the film-frames frame list, but there is
         // not a good way to recover it

         // Calculate a default AA frame timecode based on the
         // whole hour portion of the clip's in timecode
         CTimecode videoFramesInTimecode = videoFramesFrameList->getInTimecode();
         CTimecode AAFrameTimecode = GetWholeHourTimecode(videoFramesInTimecode);
         int AAFrameIndex = AAFrameTimecode - videoFramesInTimecode;
         AAFrameIndex += videoFramesFrameList->getTotalFrameCount();

         int outFrameIndex = totalFrameCount - frameCount;
         int newFrameCount = outFrameIndex;
         EFrameLabel *frameLabels = new EFrameLabel[newFrameCount];
         CVideoFrameList::create3_2PulldownFrameLabels(0, outFrameIndex,
                                                       AAFrameIndex,
                                                       nominalPulldown,
                                                       frameLabels);

         retVal
         = filmFramesFrameList->createFilmFrames_FrameList(videoFramesFrameList,
                                                           nominalPulldown,
                                                           frameLabels);
         delete [] frameLabels;

         if (retVal != 0)
            return retVal;   // ERROR
         }
      }

   // If relevant, delete the video-fields framing frame lists.  Each
   // will be reconstructed when first accessed
   CVideoFrameList *videoFieldsFrameList
                                = getVideoFrameList(FRAMING_SELECT_FIELDS_1_2);
   if (videoFieldsFrameList != 0)
      {
      videoFieldsFrameList->deleteFrameList();
      retVal = videoFieldsFrameList->initVideoFields_FrameList();
      if (retVal != 0)
         return retVal;   // ERROR
      }
   videoFieldsFrameList = getVideoFrameList(FRAMING_SELECT_FIELD_1_ONLY);
   if (videoFieldsFrameList != 0)
      {
      videoFieldsFrameList->deleteFrameList();
      retVal = videoFieldsFrameList->initVideoFields_FrameList();
      if (retVal != 0)
         return retVal;   // ERROR
      }
   videoFieldsFrameList = getVideoFrameList(FRAMING_SELECT_FIELD_2_ONLY);
   if (videoFieldsFrameList != 0)
      {
      videoFieldsFrameList->deleteFrameList();
      retVal = videoFieldsFrameList->initVideoFields_FrameList();
      if (retVal != 0)
         return retVal;   // ERROR
      }

   // Update the Field List file
   retVal = updateFieldListFile();
   if (retVal != 0)
      return retVal; // ERROR: Failed to write Field List file

   // Write the updated frame list (.trk) files
   retVal = writeAllFrameListFiles();
   if (retVal != 0)
      return retVal; // ERROR: Failed to write Frame List file

   // Write the updated clip (.clp) file
   retVal = getParentClip()->writeFile();
   if (retVal != 0)
      return retVal; // ERROR: Failed to write Frame List file

   return 0;

} // shortenVideoProxy

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

CFrameList* CVideoProxy::createFrameList(CFramingInfo *newFramingInfo)
{
   CVideoFramingInfo* newVideoFramingInfo
                             = static_cast<CVideoFramingInfo*>(newFramingInfo);

   CFrameList* frameList = new CVideoFrameList(newVideoFramingInfo, this);

   return frameList;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

//----------------------------------------------------------------------------
// Function:    translateFrameIndex
//
// Description: Translates a frame index between two different frame-lists.
//              E.g., translate a frame index of a frame in the film-frames
//              frame list to a corresponding frame index in the video-frames
//              frame index.  This function uses timecodes to find the
//              corresponding frame in the target frame list.  Because it
//              uses timecodes, the result may be only approximate in the
//              case where the 3:2 pulldown cadence is broken.
//
//              Note: see the function mapFrameIndex if you need a
//                    field-accurate translation of a frame index
//
// Arguments:   int srcFrameIndex     Framing index of source frame
//              int fromFramingIndex  Framing index (in Video Proxy) of
//                                    source frame list
//              int toFramingIndex    Framing index (in Video Proxy) of
//                                    target frame list
//
// Return:      Frame index of matching frame.  If function fails,
//              then -1 is returned.
//----------------------------------------------------------------------------
int CVideoProxy::translateFrameIndex(int srcFrameIndex, int fromFramingIndex,
                                     int toFramingIndex) const
{
   const int errorFrameIndex = -1;

   if (clipVer == CLIP_VERSION_5L)
      {
      CAutoErrorReporter errorReporter("CVideoProxy::translateFrameIndex");
      errorReporter.errorCode = CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;
      errorReporter.msg << "Function not supported by clip version 5";
      return errorFrameIndex;
      }

    // short circuit
   if (fromFramingIndex == toFramingIndex)
      return srcFrameIndex;

   CVideoFrameList* fromVideoFrameList = getVideoFrameList(fromFramingIndex);
   CVideoFrameList* toVideoFrameList = getVideoFrameList(toFramingIndex);

   if (fromVideoFrameList == 0)
      {
      TRACE_1(errout << "CVideoProxy::translateFrameIndex: Bad from index "
                     << fromVideoFrameList);
      return errorFrameIndex;
      }

   if (toVideoFrameList == 0)
      {
      TRACE_2(errout << "CVideoProxy::translateFrameIndex: Bad to index "
                     << toVideoFrameList);
      return srcFrameIndex;  // pretend it's not an error (?)
      }

   // Framing indices point to the same frame list, so we can return
   // the srcFrameIndex as the result.  Slightly faster when there
   // is no pulldown and film-frames points to the same frame list
   // as video-frames
   if (fromVideoFrameList == toVideoFrameList)
      return srcFrameIndex;

   int hours = 0, minutes = 0, seconds = 0, frames = 0;
   fromVideoFrameList->getTimecodeForFrameIndex(srcFrameIndex,
                         &hours, &minutes, &seconds, &frames);
   if (srcFrameIndex != 0 &&
       hours == 0 && minutes == 0 && seconds == 0 && frames == 0)
      {
      TRACE_1(errout << "CVideoProxy::translateFrameIndex: Bad input index "
                     << srcFrameIndex << ", got 0:0:0:0");
      return errorFrameIndex;
      }

   int dstFrameIndex = toVideoFrameList->getFrameIndex(hours, minutes, seconds,
                                                       frames);
   if (dstFrameIndex < 0)
      {
      TRACE_1(errout << "CVideoProxy::translateFrameIndex: Bad input index "
                     << srcFrameIndex);
      return errorFrameIndex;
      }

   return dstFrameIndex;
}


//----------------------------------------------------------------------------
// Function:    mapFrameIndex
//
// Description: Maps a frame index between two different frame-lists.
//              E.g., translate a frame index of a frame in the film-frames
//              frame list to a corresponding frame index in the video-frames
//              frame index.  This function is field-accurate.  In the case of
//              video-frames and film-frames the caller gets to select whether
//              the returned frame index is the earliest or latest frame with
//              matching field indices.
//
//              Note: There is a similar function, translateFrameIndex, that
//                    uses the timecodes to find the corresponding frame
//                    index.  You may want this function if you want to
//                    translate to a frame index that has a close or identical
//                    timecode.
//
// Arguments:   int srcFrameIndex     Framing index of source frame
//              int fromFramingIndex  Framing index (in Video Proxy) of
//                                    source frame list
//              int toFramingIndex    Framing index (in Video Proxy) of
//                                    target frame list
//              bool findFirstFrame   If true, returns the earliest frame
//                                    with corresponding field indices.
//                                    If false, returns the latest frame
//                                    with corresponding field indices.
//
// Return:      Frame index of matching frame.  If function fails,
//              then -1 is returned.
//----------------------------------------------------------------------------
int CVideoProxy::mapFrameIndex(int srcFrameIndex, int fromFramingIndex,
                               int toFramingIndex, bool findFirstFrame) const
{
   const int errorFrameIndex = -1;
   int dstFrameIndex = -1;

   if (clipVer == CLIP_VERSION_5L)
      {
      CAutoErrorReporter errorReporter("CVideoProxy::mapFrameIndex");
      errorReporter.errorCode = CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;
      errorReporter.msg << "Function not supported by clip version 5";
      return errorFrameIndex;
      }

    // short circuit
   if (fromFramingIndex == toFramingIndex)
      return srcFrameIndex;

   CVideoFrameList* fromVideoFrameList = getVideoFrameList(fromFramingIndex);
   CVideoFrameList* toVideoFrameList = getVideoFrameList(toFramingIndex);

   if (fromVideoFrameList == 0)
      {
      TRACE_1(errout << "CVideoProxy::mapFrameIndex: Bad from index "
                     << fromFramingIndex);
      return errorFrameIndex;
      }

   if (toVideoFrameList == 0)
      {
      TRACE_2(errout << "CVideoProxy::mapFrameIndex: Bad to index "
                     << toFramingIndex);
      return srcFrameIndex;  // pretend it's not an error (?)
      }

   // Framing indices point to the same frame list, so we can return
   // the srcFrameIndex as the result
   if (fromVideoFrameList == toVideoFrameList)
      return srcFrameIndex;

   const CImageFormat *imageFormat = getImageFormat();
   if (imageFormat == 0)
      return errorFrameIndex;

   int fieldCount = imageFormat->getFieldCount();
   if (fieldCount == 0)
      return errorFrameIndex;

   CVideoFrame *srcFrame =  fromVideoFrameList->getFrame(srcFrameIndex);
   if (srcFrame == 0)
      return errorFrameIndex;

   if (fromFramingIndex == FRAMING_SELECT_FILM
       && toFramingIndex == FRAMING_SELECT_VIDEO)
      {
      // Map film-frames index to the video-frames index
      // This is easy because we know the video frames all have the same
      // number of fields

      int srcFieldCount = srcFrame->getTotalFieldCount();

      if (findFirstFrame)
         {
         int minSrcFieldListIndex = INT_MAX;
         for (int fieldIndex = 0; fieldIndex < srcFieldCount; ++fieldIndex)
            {
            CField *srcField = srcFrame->getBaseField(fieldIndex);
            if (srcField == 0)
                  return errorFrameIndex;
            int srcFieldListIndex = srcField->getFieldListEntryIndex();
            if (srcFieldListIndex < minSrcFieldListIndex)
               minSrcFieldListIndex = srcFieldListIndex;
            }
         dstFrameIndex = minSrcFieldListIndex / fieldCount;
         }
      else
         {
         int maxSrcFieldListIndex = INT_MIN;
         for (int fieldIndex = 0; fieldIndex < srcFieldCount; ++fieldIndex)
            {
            CField *srcField = srcFrame->getBaseField(fieldIndex);
            if (srcField == 0)
                  return errorFrameIndex;
            int srcFieldListIndex = srcField->getFieldListEntryIndex();
            if (srcFieldListIndex > maxSrcFieldListIndex)
               maxSrcFieldListIndex = srcFieldListIndex;
            }
         dstFrameIndex = maxSrcFieldListIndex / fieldCount;
         }
      }
   else if (fromFramingIndex == FRAMING_SELECT_VIDEO
       && toFramingIndex == FRAMING_SELECT_FILM)
      {
      ///////////////////////////////////////////////////////////
      // mbraca sez: I added this case, but then didn't use it,//
      // so it's untested. Use at your own risk.               //
      ///////////////////////////////////////////////////////////
      // Map the video-frames index to the film-frames index
      // This is hard because the film frames have differeing numbers of fields
      // We make the reasonable assumption that the dest frame will be within
      // a small number of frames of an approximation based on time codes
      const int searchRadius = 10; // frames
      const int srcTotalFrameCount = fromVideoFrameList->getTotalFrameCount();

      // Get approximate location in FILM frame list
      int approxDstFrameIndex = translateFrameIndex(srcFrameIndex,
                                                    FRAMING_SELECT_VIDEO,
                                                    FRAMING_SELECT_FILM);

      // Set up search parameters
      int searchFirst = (approxDstFrameIndex < searchRadius)? 0
                                    : (approxDstFrameIndex - searchRadius);
      int searchLast =
         (approxDstFrameIndex > (srcTotalFrameCount - 1 - searchRadius))?
            srcTotalFrameCount - 1 :
            approxDstFrameIndex + searchRadius;

      // Which field index are we looking for (min or max)?
      int srcFieldCount = srcFrame->getTotalFieldCount();
      int targetFieldListIndex;

      if (findFirstFrame)
         {
         targetFieldListIndex = INT_MAX;
         for (int fieldIndex = 0; fieldIndex < srcFieldCount; ++fieldIndex)
            {
            CField *srcField = srcFrame->getBaseField(fieldIndex);
            if (srcField == 0)
                  return errorFrameIndex;
            int srcFieldListIndex = srcField->getFieldListEntryIndex();
            if (srcFieldListIndex < targetFieldListIndex)
               targetFieldListIndex = srcFieldListIndex;
            }
         }
      else
         {
         targetFieldListIndex = INT_MIN;
         for (int fieldIndex = 0; fieldIndex < srcFieldCount; ++fieldIndex)
            {
            CField *srcField = srcFrame->getBaseField(fieldIndex);
            if (srcField == 0)
                  return errorFrameIndex;
            int srcFieldListIndex = srcField->getFieldListEntryIndex();
            if (srcFieldListIndex > targetFieldListIndex)
               targetFieldListIndex = srcFieldListIndex;
            }
         }

      // Search
      bool foundIt = false;
      if (findFirstFrame)
         {
         // Search from first to last to find first
         for (dstFrameIndex = searchFirst;
              (dstFrameIndex <= searchLast) && !foundIt;
              ++dstFrameIndex)
            {
            // REFACTOR ME (see identical loop body below)
            CFrame *dstFrame = toVideoFrameList->getBaseFrame(dstFrameIndex);
            if (dstFrame == 0)
               return errorFrameIndex;

            int dstFieldCount = dstFrame->getTotalFieldCount();
            for (int fieldIndex = 0;
                 (fieldIndex < dstFieldCount) && !foundIt;
                 ++fieldIndex)
               {
               CField *dstField = dstFrame->getBaseField(fieldIndex);
               if (dstField == 0)
                     return errorFrameIndex;
               int dstFieldListIndex = dstField->getFieldListEntryIndex();
               if (dstFieldListIndex == targetFieldListIndex)
                  foundIt = true;
               }
            }
         }
      else
         {
         // search from last to first to find last
         for (dstFrameIndex = searchLast;
              dstFrameIndex >= searchFirst;
              --dstFrameIndex)
            {
            // REFACTOR ME (see identical loop body above)
            CFrame *dstFrame = toVideoFrameList->getBaseFrame(dstFrameIndex);
            if (dstFrame == 0)
               return errorFrameIndex;

            int dstFieldCount = dstFrame->getTotalFieldCount();
            for (int fieldIndex = 0;
                 (fieldIndex < dstFieldCount) && !foundIt;
                 ++fieldIndex)
               {
               CField *dstField = dstFrame->getBaseField(fieldIndex);
               if (dstField == 0)
                     return errorFrameIndex;
               int dstFieldListIndex = dstField->getFieldListEntryIndex();
               if (dstFieldListIndex == targetFieldListIndex)
                  foundIt = true;
               }
            }
         }
      if (!foundIt)
         return errorFrameIndex;
      }
   else
      {
      // Not supported
      TRACE_1(errout << "CVideoProxy::mapFrameIndex: Mapping not supported yet "
                     << fromFramingIndex << "->" << toFramingIndex);
      return errorFrameIndex;
      }

   return dstFrameIndex;
}

//----------------------------------------------------------------------------
// Function:    mapFrameIndex
//
// Description: Maps a frame index between two different frame-lists.
//              E.g., translate a frame index of a frame in the film-frames
//              frame list to a corresponding frame index in the video-frames
//              frame index.  This function is field-accurate. All matching
//              results are returned in the form of a map from "from"
//              field indices to vectors of matching "to" frame/field index
//              pairs.
//
// Arguments:   int fromFrameIndex    Frame index of "from" frame
//              int fromFramingIndex  Framing index (in Video Proxy) of
//                                    source frame list
//              int toFramingIndex    Framing index (in Video Proxy) of
//                                    target frame list
//              bool findFirstFrame   If true, returns the earliest frame
//                                    with corresponding field indices.
//                                    If false, returns the latest frame
//                                    with corresponding field indices.
//              OUTPUT: <map<vector<<pair<int,int>>> &resultList
//                                    Map from "from" filed indices to
//                                    vectors of inger pairs:
//                                    ("to" frame index,"to" field index)
//
// Return:      0 if success, else error code
//----------------------------------------------------------------------------
int CVideoProxy::mapFrameIndex(int fromFrameIndex,
                               int fromFramingIndex, int toFramingIndex,
                               map<int, vector<std::pair<int,int> > > &resultList)
                               const
{
   const string funcName("CVideoProxy::mapFrameIndex");
   resultList.clear();

   if (clipVer == CLIP_VERSION_5L)
      {
      CAutoErrorReporter errorReporter(funcName);
      errorReporter.errorCode = CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;
      errorReporter.msg << "Function not supported by clip version 5";
      return CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;
      }

   CVideoFrameList* fromVideoFrameList = getVideoFrameList(fromFramingIndex);
   if (fromVideoFrameList == 0)
      {
      CAutoErrorReporter errorReporter(funcName);
      errorReporter.errorCode = CLIP_ERROR_INVALID_FRAMING_INDEX;
      errorReporter.msg << "Bad from index " << fromFramingIndex;
      return CLIP_ERROR_INVALID_FRAMING_INDEX;
      }
   CFrame *fromFrame = fromVideoFrameList->getBaseFrame(fromFrameIndex);
   if (fromFrame == 0)
      {
      CAutoErrorReporter errorReporter(funcName);
      errorReporter.errorCode = CLIP_ERROR_INVALID_FRAME_NUMBER;
      errorReporter.msg << "Bad frame number " << fromFrameIndex;
      return CLIP_ERROR_INVALID_FRAME_NUMBER;
      }

   int fromFieldCount = fromFrame->getTotalFieldCount();

   if (isSameVideoFrameList(fromFramingIndex, toFramingIndex))
      {
      // Special case - they are the same frame lists
      // make identity mapping
      vector<std::pair<int,int> > resultForOneField;
      std::pair<int,int> resultItem;
      resultItem.first = fromFrameIndex;
      for (int fieldIndex = 0; fieldIndex < fromFieldCount; ++fieldIndex)
         {
         resultItem.second = fieldIndex;
         resultForOneField.push_back(resultItem);
         resultList[fieldIndex] = resultForOneField;
         }
      }
   else
      {
      // Do real mapping
      vector<std::pair<int,int> > resultForOneField;
      for (int fromFieldIndex = 0; fromFieldIndex < fromFieldCount;
           ++fromFieldIndex)
         {
         int retVal = mapFrameAndFieldIndex(fromFrameIndex, fromFieldIndex,
                                            fromFramingIndex, toFramingIndex,
                                            resultForOneField);
         if (retVal != 0)
            return retVal;

         resultList[fromFieldIndex] = resultForOneField;
         }
      }

   return 0;
}

//----------------------------------------------------------------------------
// Function:    mapFrameAndFieldIndex
//
// Description: Finds all fields in the "to" frame list that match the
//              designated source field. The fields match if they point to
//              the same media (i.e. they have the same field list index).
//
// Arguments:   int srcFrameIndex     Framing index of source frame
//              int srcFieldIndex     Field index of source field in frame
//              int fromFramingIndex  Framing index (in Video Proxy) of
//                                    source frame list
//              int toFramingIndex    Framing index (in Video Proxy) of
//                                    target frame list
//              OUTPUT: vector<pair<int,int>>   Vector of integer pairs:
//                                    (frame index,field index) being the
//                                    matching frame/field indices in the "to"
//                                    frame list.
//
// Return:      0 if success, else error code
//----------------------------------------------------------------------------

int CVideoProxy::mapFrameAndFieldIndex(int fromFrameIndex, int fromFieldIndex,
                                       int fromFramingIndex, int toFramingIndex,
                                       vector<std::pair<int,int> > &resultList)
                                       const
{
   const string funcName("CVideoProxy::mapFrameAndFieldIndex");

   resultList.clear();

   if (clipVer == CLIP_VERSION_5L)
      {
      CAutoErrorReporter errorReporter(funcName);
      errorReporter.errorCode = CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;
      errorReporter.msg << "Function not supported by clip version 5";
      return CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;
      }

   // Get :from" field list index for this field -- we will return
   // a list of all "to" fields that match the field list index
   CVideoFrameList *fromFrameList = getVideoFrameList(fromFramingIndex);
   if (fromFrameList == 0)
      {
      CAutoErrorReporter errorReporter(funcName);
      errorReporter.errorCode = CLIP_ERROR_INVALID_FRAMING_INDEX;
      errorReporter.msg << "Bad \"from\" index " << fromFramingIndex;
      return CLIP_ERROR_INVALID_FRAMING_INDEX;
      }

   CFrame *fromFrame = fromFrameList->getBaseFrame(fromFrameIndex);
   if (fromFrame == 0)
      {
      CAutoErrorReporter errorReporter(funcName);
      errorReporter.errorCode = CLIP_ERROR_INVALID_FRAME_NUMBER;
      errorReporter.msg << "Bad frame number " << fromFrameIndex;
      return CLIP_ERROR_INVALID_FRAME_NUMBER;
      }

   CField *fromField = fromFrame->getBaseField(fromFieldIndex);
   if (fromField == 0)
      {
      CAutoErrorReporter errorReporter(funcName);
      errorReporter.errorCode = CLIP_ERROR_INVALID_FIELD_INDEX;
      errorReporter.msg << "Bad field index " << fromFieldIndex;
      return CLIP_ERROR_INVALID_FIELD_INDEX;
      }

   int fromFieldListIndex = fromField->getFieldListEntryIndex();

   CVideoFrameList *toFrameList = getVideoFrameList(toFramingIndex);
   if (toFrameList == 0)
      {
      CAutoErrorReporter errorReporter(funcName);
      errorReporter.errorCode = CLIP_ERROR_INVALID_FRAMING_INDEX;
      errorReporter.msg << "Bad \"to\" index " << toFramingIndex;
      return CLIP_ERROR_INVALID_FRAMING_INDEX;
      }

   // Special case - going to video frame world
   // Lots of assumptions here allow us to do a simple, direct calculation
   // to find the frame that contains the field we are looking for
   if (toFramingIndex == FRAMING_SELECT_VIDEO)
      {
      const CImageFormat *imageFormat = getImageFormat();
      if (imageFormat == 0)
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN;
      int fieldCount = imageFormat->getFieldCount();
      if (fieldCount == 0)
         return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR;

      // Direct calculation to find the exact video frame that contains
      // the desired field index
      int toFrameIndex = fromFieldListIndex / fieldCount;
      CFrame *toFrame = toFrameList->getBaseFrame(toFrameIndex);
      if (toFrame == 0)
         {
         CAutoErrorReporter errorReporter(funcName);
         errorReporter.errorCode = CLIP_ERROR_FRAME_LIST_IS_CORRUPT;
         errorReporter.msg << "Frame list mapping failed";
         return CLIP_ERROR_FRAME_LIST_IS_CORRUPT;
         }

      // Find the desired field index by scanning the fields of the frame
      int toFieldIndex;
      for (toFieldIndex = 0; toFieldIndex < fieldCount; ++toFieldIndex)
         {
         CField *toField = toFrame->getBaseField(toFieldIndex);
         if (toField == 0)
            {
            CAutoErrorReporter errorReporter(funcName);
            errorReporter.errorCode = CLIP_ERROR_FRAME_LIST_IS_CORRUPT;
            errorReporter.msg << "Badly formed frame" << toFrameIndex;
            return CLIP_ERROR_FRAME_LIST_IS_CORRUPT;
            }

         if (fromFieldListIndex == toField->getFieldListEntryIndex())
            break;
         }

      // If found, add it to the list
      if (toFieldIndex < fieldCount)
         resultList.push_back(std::pair<int,int>(toFrameIndex, toFieldIndex));

      }
   else
      {
      // Else need to some hunting and gathering...
      // The general case is harder because (a) there may be a variable
      // number of "to" fields per frame, and (b) one "from" field may map to
      // multiple "to" fields.

      // We make the reasonable assumption that the dest frame will be within
      // a small number of frames from an approximation based on time codes
      const int searchRadius = 10; // frames

      // Get approximate location in FILM frame list
      int approxDstFrameIndex = translateFrameIndex(fromFrameIndex,
                                                    fromFramingIndex,
                                                    toFramingIndex);

      // Find first and last frames to look at in the destination list
      int searchFirst = (approxDstFrameIndex < searchRadius)? 0
                                    : (approxDstFrameIndex - searchRadius);
      const int toTotalFrameCount = toFrameList->getTotalFrameCount();
      int searchLast =
         (approxDstFrameIndex > (toTotalFrameCount - 1 - searchRadius))?
            toTotalFrameCount - 1 :
            approxDstFrameIndex + searchRadius;

      // Search from first through last frame
      // The matching fields will share a common field list index
      for (int toFrameIndex = searchFirst; toFrameIndex <= searchLast;
           ++toFrameIndex)
         {
         CFrame *toFrame = toFrameList->getBaseFrame(toFrameIndex);
         if (toFrame == 0)
            {
            CAutoErrorReporter errorReporter(funcName);
            errorReporter.errorCode = CLIP_ERROR_FRAME_LIST_IS_CORRUPT;
            errorReporter.msg << "Frame list mapping failed";
            return CLIP_ERROR_FRAME_LIST_IS_CORRUPT;
            }

         int toFieldCount = toFrame->getTotalFieldCount();
         for (int toFieldIndex = 0; toFieldIndex < toFieldCount; ++toFieldIndex)
            {
            CField *toField = toFrame->getBaseField(toFieldIndex);
            if (toField == 0)
               {
               CAutoErrorReporter errorReporter(funcName);
               errorReporter.errorCode = CLIP_ERROR_FRAME_LIST_IS_CORRUPT;
               errorReporter.msg << "Badly formed frame" << toFrameIndex;
               return CLIP_ERROR_FRAME_LIST_IS_CORRUPT;
               }

            int toFieldListIndex = toField->getFieldListEntryIndex();
            if (toFieldListIndex == fromFieldListIndex)
               resultList.push_back(std::pair<int,int>(toFrameIndex, toFieldIndex));
            }
         }
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////
//
// Function:   updateMediaStorageList
//
// Description:
//
// Arguments:
//
// Returns:
//
//////////////////////////////////////////////////////////////////////
int CVideoProxy::updateMediaStorageList(
   const CMediaStorageList &newMediaStorageList,
   CIniFile *clipFile,
   const string &sectionPrefix)
{
   // Let the old one leak until I can determine it's safe to nuke it.
   //delete mediaStorageList;
   mediaStorageList = newMediaStorageList;
   return mediaStorageList.writeMediaStorageSection(clipFile, sectionPrefix, false);
}

//////////////////////////////////////////////////////////////////////
//  OGLV Bridge
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//
// Function:   isOGLVClip
//
// Description: Determines if the Video Proxy is also an OGLV clip
//              as created by the OGLV Bridge code
//
//              N.B.: This function may or may not function with "virtual"
//                    clips.
//
// Arguments:  None
//
// Returns:    Returns true if it is an OGLV clip, false otherwise
//
//////////////////////////////////////////////////////////////////////
bool CVideoProxy::isOGLVClip() const
{
   // REMOVE THIS METHOD

   return false;
}

//////////////////////////////////////////////////////////////////////
//  Is the proxy media contained in image files (as opposed to in
//  videostore or in image repository)
//////////////////////////////////////////////////////////////////////

bool CVideoProxy::isMediaFromImageFile() const
{
   // Returns true if the media is stored in an Image File (DPX, etc)
   // The media is assumed to be in an Image File if either the image format
   // type suggests an Image File (e.g., IF_TYPE_FILE_DPX) or the media
   // type suggests an Image File (e.g., MEDIA_TYPE_IMAGE_FILE_UYVY).

   const CImageFormat *imageFormat = getImageFormat();
   CMediaStorage *mediaStorage = mediaStorageList.getMediaStorage(0);
   return ((imageFormat != 0
            && CImageInfo::queryIsFormatFile(imageFormat->getImageFormatType()))
           || (mediaStorage != 0
               && CMediaInterface::isMediaTypeImageFile(mediaStorage->getMediaType())));
}

//////////////////////////////////////////////////////////////////////
//  Checks if the frame lists indicated by the given frame list
//  indices are, in fact, the same frame list (mainly to check if
//  film and video lists are the same).
//////////////////////////////////////////////////////////////////////

bool CVideoProxy::isSameVideoFrameList(int videoFrameListIndexA,
                                       int videoFrameListIndexB) const
{
   if (videoFrameListIndexA == videoFrameListIndexB)
      return true;

   CVideoFrameList *videoFrameListA = getVideoFrameList(videoFrameListIndexA);
   CVideoFrameList *videoFrameListB = getVideoFrameList(videoFrameListIndexB);
   if (videoFrameListA != 0 && videoFrameListB != 0 &&
       videoFrameListA == videoFrameListB)
      {
      return true;
      }

   return false;
}

//////////////////////////////////////////////////////////////////////
//  Virtual Copy
//////////////////////////////////////////////////////////////////////

int CVideoProxy::virtualCopy(CVideoProxy &srcTrack, int srcFrameIndex,
                             int dstFrameIndex, int frameCount)
{
   int retVal;

   if (clipVer == CLIP_VERSION_5L)
      {
      CAutoErrorReporter errorReporter("CVideoProxy::virtualCopy");
      errorReporter.errorCode = CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;
      errorReporter.msg << "Function not supported by clip version 5";
      return errorReporter.errorCode;
      }

   CVideoFrameList *dstFrameList = getVideoFrameList(0);
   CVideoFrameList *srcFrameList = srcTrack.getVideoFrameList(0);

   retVal = dstFrameList->virtualCopy(*srcFrameList, srcFrameIndex,
                                      dstFrameIndex, frameCount,
                                      mediaStorageList);
   if (retVal != 0)
      return retVal;

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Video Proxy Section Read/Write
//////////////////////////////////////////////////////////////////////

/*
int CVideoProxy::readVideoProxySections(CIniFile *clipFile,
                                        const string& sectionPrefix,
                                   CVideoFramingInfoList& videoFramingInfoList)
{
   int retVal;

   // Read the base class CTrack's portion of Video Proxy Sections
   retVal = readTrackSections(clipFile, sectionPrefix, videoFramingInfoList);
   if (retVal != 0)
      {
      // ERROR: Could not read the base class CTrack's portion of
      //        Video Proxy Sections
      return retVal;
      }

   return 0;

} // readVideoProxySections( )


int CVideoProxy::writeVideoProxySections(CIniFile *clipFile,
                                         const string& sectionPrefix)
{
   int retVal;

   // Write the base class CTrack's portion of Video Proxy Sections
   retVal = writeTrackSections(clipFile, sectionPrefix);
   if (retVal != 0)
      {
      // ERROR: Could not write the base class CTrack's portion of
      //        Video Proxy Sections
      return retVal;
      }

   return 0;

}  // writeVideoProxySections( )
*/


//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                            ###################################
// ###    CVideoProxyList Class     ##################################
// ####                            ###################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CVideoProxyList::CVideoProxyList()
{
}

CVideoProxyList::~CVideoProxyList()
{
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CVideoProxyList::createVideoProxy(ClipSharedPtr &newParentClip,
                                      EClipVersion newClipVer,
                                      int newHandleCount,
                                      const CImageFormat& newImageFormat)
{
   // Find a new proxy number
   int proxyNumber = getNewTrackNumber();

   CVideoProxy *videoProxy = new CVideoProxy(newParentClip, newClipVer,
                                             proxyNumber);

   // Set handles count, probably based on image format type
   videoProxy->setHandleCount(newHandleCount);

   // Set the image format in the video proxy
   videoProxy->setImageFormat(newImageFormat);

   addTrack(videoProxy);

   return 0;
}

CTrack* CVideoProxyList::createDerivedTrack(ClipSharedPtr &newParentClip,
                                            EClipVersion newClipVer,
                                            int newTrackNumber)
{
   CVideoProxy *videoProxy = new CVideoProxy(newParentClip, newClipVer,
                                             newTrackNumber);

   addTrack(videoProxy);

   return videoProxy;
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

CVideoProxy* CVideoProxyList::getVideoProxy(int proxyIndex) const
{
   CVideoProxy *videoProxy;
   videoProxy = static_cast<CVideoProxy*>(getBaseTrack(proxyIndex));

   if (videoProxy == 0)
   {
      DBTRACE(proxyIndex);
      theError.set(CLIP_ERROR_INVALID_VIDEO_PROXY_INDEX);
   }

   return videoProxy;
}

int CVideoProxyList::getVideoProxyCount() const
{
   return getTrackCount();
}

//////////////////////////////////////////////////////////////////////
// AppId Search
//////////////////////////////////////////////////////////////////////

CVideoProxy* CVideoProxyList::findVideoProxy(const string &appId) const
{
   // Find the first CVideoProxy in the Video Proxy List that has an appId
   // that matches (case-sensitive) the caller's appId
   // Returns pointer to CVideoProxy instance if found, NULL pointer if not found

   return static_cast<CVideoProxy*>(findBaseTrack(appId));
}

int CVideoProxyList::findVideoProxyIndex(const string &appId) const
{
   // Find the Track List index of the first CVideoProxy in the Video Proxy List
   // that has an appId that matches (case-sensitive) the caller's appId
   // Returns index into the Video Proxy List if found, -1 if not found.

   return findBaseTrackIndex(appId);
}

//////////////////////////////////////////////////////////////////////
// Read & Write Parameters
//////////////////////////////////////////////////////////////////////

int CVideoProxyList::readVideoProxyListSection(CIniFile *iniFile, ClipSharedPtr &clip,
                                   CVideoFramingInfoList& videoFramingInfoList)
{
   int retVal;

   if (videoFramingInfoList.getFramingCount() == 0)
      return 0;  // No video framing, so assume this clip has no video

   retVal = readTrackListSection(iniFile, videoProxySection, clip,
                                 videoFramingInfoList);
   if (retVal != 0)
      return retVal; // ERROR: Could not read base class CTrack's portion
                     //        of Video Proxy List or Info sections

   // The
   return 0;  // Success

} // readVideoProxyListSection( )

int CVideoProxyList::refreshVideoProxyListSection(CIniFile *iniFile)
{
   int retVal;

   if (getVideoProxyCount() == 0)
      return 0;  // No video proxies in list, so nothing to refresh

   retVal = refreshTrackListSection(iniFile, videoProxySection);
   if (retVal != 0)
      return retVal; // ERROR: Could not refresh base class CTrack's portion
                     //        of Video Proxy List or Info sections

   return 0;  // Success

} // refreshVideoProxyListSection( )

int CVideoProxyList::writeVideoProxyListSection(CIniFile *iniFile)
{
   int retVal;

   if (getVideoProxyCount() == 0)
      return 0;  // No video proxies in list, so nothing to write

   retVal = writeTrackListSection(iniFile, videoProxySection);
   if (retVal != 0)
      return retVal; // ERROR: Could not write base class CTrack's portion of
                     //        Video Proxy List or Info sections

   return 0;

} // writeVideoProxyListSection( )

string CVideoProxyList::getVideoSectionName () const
{
  return videoProxySection;
}
