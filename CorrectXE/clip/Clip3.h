// Clip3.h: interface for the CClip class.
//
//////////////////////////////////////////////////////////////////////

#ifndef Clip3H
#define Clip3H

#include "cliplibint.h"
#include "ClipSharedPtr.h"
#include "Clip3AudioTrack.h"
#include "Clip3TimeTrack.h"
#include "Clip3VideoTrack.h"
#include "Clip5Proto.h"
#include "machine.h"
#include "MediaInterface.h"
#include "timecode.h"

#include <string>
using std::string;

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CBinManager;
class CClip;
class CIniFile;
class CImageFormat;
class CMediaStorageList;

struct _guid_t;


//////////////////////////////////////////////////////////////////////

// Global Unique Identifier for Clip
// (a little bit of indirection in case underlying guid structure changes
typedef struct _guid_t CLIP_GUID;

//////////////////////////////////////////////////////////////////////

// Video Proxy Select Constants.  Selects between Normal and Proxies
#define VIDEO_SELECT_NORMAL         0
#define VIDEO_SELECT_PROXY_BASE     1
#define VIDEO_SELECT_PROXY_1        (VIDEO_SELECT_PROXY_BASE+0)
#define VIDEO_SELECT_PROXY_2        (VIDEO_SELECT_PROXY_BASE+1)
#define VIDEO_SELECT_PROXY_3        (VIDEO_SELECT_PROXY_BASE+2)
#define VIDEO_SELECT_PROXY_4        (VIDEO_SELECT_PROXY_BASE+3)
#define VIDEO_SELECT_PROXY_5        (VIDEO_SELECT_PROXY_BASE+4)
#define VIDEO_SELECT_PROXY_6        (VIDEO_SELECT_PROXY_BASE+5)
#define VIDEO_SELECT_PROXY_7        (VIDEO_SELECT_PROXY_BASE+6)
#define VIDEO_SELECT_PROXY_8        (VIDEO_SELECT_PROXY_BASE+7)

// Video Track Indices
// Frames Select Constants.  Selects between Video-Frames, Film-Frames,
// Field-Frames and Custom-Frames
#define FRAMING_SELECT_VIDEO        0   // Video Frames
#define FRAMING_SELECT_FILM         1   // Film Frames
#define FRAMING_SELECT_FIELDS_1_2   2   // Video Field Frames: 1, 2 sequence
#define FRAMING_SELECT_FIELD_1_ONLY 3   // Video Field Frames: Field 1 only
#define FRAMING_SELECT_FIELD_2_ONLY 4   // Video Field Frames: Field 2 only
#define FRAMING_SELECT_CADENCE_REPAIR 5  // cadence repair
#define FRAMING_SELECT_CUSTOM_BASE  6   // Custom Framing - TBD
#define FRAMING_SELECT_CUSTOM_1     (FRAMING_SELECT_CUSTOM_BASE+0)
#define FRAMING_SELECT_CUSTOM_2     (FRAMING_SELECT_CUSTOM_BASE+1)
#define FRAMING_SELECT_CUSTOM_3     (FRAMING_SELECT_CUSTOM_BASE+2)
#define FRAMING_SELECT_CUSTOM_4     (FRAMING_SELECT_CUSTOM_BASE+3)

// -------------------------------------------------------------------

// CClipInitInfo Source Type
enum EClipInitSource
{
   CLIP_INIT_SOURCE_UNKNOWN = 0,
   CLIP_INIT_SOURCE_CLIP_SCHEME,
   CLIP_INIT_SOURCE_IMAGE_FILE,
   CLIP_INIT_SOURCE_CLIP,
   CLIP_INIT_SOURCE_CLIP_INI
};

// CD Ingest Type for Control Dailies
enum ECDIngestType
{
   CD_INGEST_TYPE_INVALID = 0,             // Unknown ingest type
   CD_INGEST_TYPE_FULL_REEL,               // Ingest picture based on a fixed
                                           // relationship between source TC
                                           // and destination TC.  Usually
                                           // used when ingesting an entire
                                           // labroll.
   CD_INGEST_TYPE_CIRCLE_TAKES             // Ingest picture with an arbitrary
                                           // relationship between source and
                                           // destiation timecodes.  Usually
                                           // used for circle takes.
};

//////////////////////////////////////////////////////////////////////

// CCommonInitInfo - Initialization Information common to Audio and Video

class MTI_CLIPLIB_API CCommonInitInfo
{
public:
   friend class CClip;
   friend class CBinManager;

   CCommonInitInfo();
   virtual ~CCommonInitInfo();

   virtual void init();
   int initFromTrack(CTrack* track);

   string getAppId() const;
   double getFrameRate() const;
   EFrameRate getFrameRateEnum() const;
   int getHandleCount() const;
   string getRepositoryIdentifier() const;
   EMediaType getMediaType() const;
   string getMediaIdentifier() const;
   string getHistoryDirectory() const;
   int getMediaAllocationFrameCount() const;
   CMediaAllocator* getMediaAllocator() const;
   bool getVirtualMediaFlag() const {return virtualMediaFlag;};

   void setAppId(const string &newAppId);
   void setClipVer(EClipVersion newClipVer);
   void setFrameRate(EFrameRate newFrameRateEnum, double newFrameRate);
   void setHandleCount(int newHandleCount);
   void setRepositoryIdentifier(const string &newRepositoryIdentifier);
   void setMediaType(EMediaType newMediaType);
   void setMediaIdentifier(const string& newMediaIdentifier);
   void setHistoryDirectory(const string& newHistoryDirectory);
   void setMediaAllocationFrameCount(int newFrameCount);
   void setMediaAllocator(CMediaAllocator *newMediaAllocator);
   void setVirtualMediaFlag(bool newVirtualMediaFlag)
      {virtualMediaFlag = newVirtualMediaFlag;};

   virtual int readSection(CIniFile *schemeFile, const string& sectionName,
                           EClipInitSource sourceType);
   virtual int writeSection(CIniFile *schemeFile, const string& sectionName);

   virtual int validate();
   virtual int checkMediaStorage(int frameCount) const;

protected:
   EClipVersion clipVer;   // Clip version string
   string appId;
   double frameRate;
   EFrameRate frameRateEnum;
   int handleCount;

   bool virtualMediaFlag;
   EMediaType mediaType;
   string repositoryIdentifier;
   string mediaIdentifier;
   string historyDirectory;
   int mediaAllocationFrameCount;   // Number of frames for which to allocate
                                    // media storage.  If less than zero or
                                    // greater than the total number of frames
                                    // in the clip, the total number of frames
                                    // in the clip is used instead. If
                                    // virtualMediaFlag is set, this is ignored.
   CMediaAllocator *mediaAllocator;

   CMediaFormat *mediaFormat;

protected:
   static string clipVerKey;
   static string appIdKey;
   static string frameRateKey;
   static string handleCountKey;
   static string virtualMediaKey;
   static string mediaTypeKey;
   static string mediaIdentifierKey;
}; // class CCommonInitInfo

// ===================================================================
// -------------------------------------------------------------------

// CAudioInitInfo - Audio Track Initialization Information

class MTI_CLIPLIB_API CAudioInitInfo : public CCommonInitInfo
{
public:
   friend class CClip;
   friend class CBinManager;

   CAudioInitInfo();
   virtual ~CAudioInitInfo();

   virtual void init();
   int initFromAudioTrack(CAudioTrack* audioTrack);
   int initAudioMediaDirectory(const string &newAudioMediaDirectory);

   CAudioFormat* getAudioFormat();
   const string& getMediaDirectory();

   void setAudioFormat(const CAudioFormat &newAudioFormat);
   void setMediaDirectory(const string& newMediaDirectory);

   int readSection(CIniFile *schemeFile, const string& sectionName,
                   EClipInitSource sourceType);
   int writeSection(CIniFile *schemeFile, const string& sectionName);

   int queryAudioTimeTotal(EMediaType mediaType,
                           const string& mediaIdentifier,
                           CTimecode& totalTime);
   int queryAudioTimeRemaining(EMediaType mediaType,
                               const string& mediaIdentifier,
                               CTimecode& totalTimeRemaining,
                               CTimecode& largestTimeRemaining);
   int validate();

private:
   string mediaDirectory;

   static string mediaDirectoryKey;

}; // class CAudioInitInfo

// ===================================================================
// -------------------------------------------------------------------

// CVideoInitInfo - Main Video and Proxy Video Initialization Information

class MTI_CLIPLIB_API CVideoInitInfo  : public CCommonInitInfo
{
public:
   friend class CClip;
   friend class CBinManager;

   CVideoInitInfo();
   virtual ~CVideoInitInfo();

   virtual void init();
   int initFromVideoProxy(CVideoProxy* videoProxy);
   int initVideoMediaLocation(const string &newMediaLocation);

   bool isMediaFromImageFile();

   int getImageFile0Index() const;
   CImageFormat *getImageFormat();

   int CheckRFL();


   void setImageFile0Index(int newImageFile0Index);
   void setImageFormat(const CImageFormat &newImageFormat);

   int queryImageTimeRemaining(EMediaType mediaType,
                               const string& mediaIdentifier,
                               CTimecode& totalTimeRemaining,
                               CTimecode& largestTimeRemaining);

   int queryImageTimeTotal(EMediaType mediaType,
                           const string& mediaIdentifier,
                           CTimecode& totalTime);

   int readSection(CIniFile *schemeFile, const string& sectionName,
                   EClipInitSource sourceType);
   int readSection2(CIniFile *schemeFile, const string& sectionName,
                   EClipInitSource sourceType);
   int writeSection(CIniFile *schemeFile, const string& sectionName);

   int validate();

private:
   int imageFile0Index;

   static const string imageFile0IndexKey;

}; // class CVideoInitInfo

// ===================================================================
// -------------------------------------------------------------------

// CTimeInitInfo - Time Track Initialization Information

class MTI_CLIPLIB_API CTimeInitInfo
{
public:
   friend struct CClip;

   CTimeInitInfo();
   virtual ~CTimeInitInfo();

   void init();
   int initFromTimeTrack(CTimeTrack *timeTrack);

   string getAppId() const {return appId;};
   EClipVersion getClipVer() const {return clipVer;};
   EFrameRate getTimecodeFrameRate() const {return timecodeFrameRate;}
   ETimeType getTimeType() const {return timeType;};

   void setAppId(const string& newAppId) {appId = newAppId;};
   void setClipVer(EClipVersion newClipVer) {clipVer = newClipVer;};
   void setTimecodeFrameRate(EFrameRate newFrameRate) {timecodeFrameRate = newFrameRate;};
   void setTimeType(ETimeType newTimeType) {timeType = newTimeType;};

   int readSection(CIniFile *schemeFile, const string& sectionName);
   int writeSection(CIniFile *schemeFile, const string& sectionName);

   int validate();

private:
   EClipVersion clipVer;
   ETimeType timeType;
   EFrameRate timecodeFrameRate;
   string appId;

   static string timeTypeKey;
   static string appIdKey;
   static string clipVerKey;
   static string timecodeFrameRateKey;

}; // class CTimeInitInfo

// ===================================================================
// -------------------------------------------------------------------

// CClipInitInfo - Clip Initialization Information

class MTI_CLIPLIB_API CClipInitInfo
{
public:
   friend class CClip;
   friend class CBinManager;

   typedef vector<CAudioInitInfo*> AudioInitInfoList;
   typedef vector<CVideoInitInfo*> VideoInitInfoList;
   typedef vector<CTimeInitInfo*>  TimeInitInfoList;

public:
   CClipInitInfo();
   virtual ~CClipInitInfo();

   int closeClip(bool forceFlag=false);

   void init();  // Resets everything in CClipInitInfo
   int initFromClip(ClipSharedPtr &sourceClip);
   int initFromClipIni(const string& binPath, const string& clipName);
   int initFromClipScheme(const string& newClipSchemeName);
   int initFromImageFile(const string& ImageFileName, bool checkHeaders);
   int initFromImageFile2(const string& imageFileNameTemplate,
                          int inFrame, int outFrame, bool checkHeaders,
                          const string &clipSchemeName,
                          bool useFileTimecode=true);
   int initFromImageFile3(const string& imageFileNameTemplate,
                          int inFrame, int outFrame, bool checkHeaders,
                          const string &clipSchemeName,
                          bool useFileTimecode,
                          const CTimecode &firstFrameTimecode);
   int checkNoImageFiles(const string& firstImageFileName, int frameCount);
   int checkNoImageFiles2(const string& firstImageFileName,
                          int inFrame, int outFrame);
   int initMediaLocation(const string &diskSchemeName);
   int initMediaLocation(const CDiskScheme &diskScheme);
   int initMediaLocation(const string& imageFileNameTemplate,
                         int frame0Index, int frameCount,
                         bool checkHeaders, const string &clipSchemeName);

   bool doesNormalVideoExist() const;

   int getAudioTrackCount() const;
   CAudioInitInfo* getAudioTrackInitInfo(int audioTrackIndex) const;
   string getBinPath() const;
   string getClipName() const;
   string getClipPath() const;
   string getClipVersionDisplayName() const;
   string getMasterClipPath() const;
   ClipSharedPtr getClipPtr() const;
   EClipInitSource getClipInitSource() const;
   EMediaStatus getClipStatus() const;
   string getClipStatusString() const;
   string getCreateDateTime() const;
   string getArchiveDateTime() const;
   string getDescription() const;
   ECDIngestType getCDIngestType() const;
   string getDiskScheme() const;
   string getMediaLocation() const;
   CTimecode getDurationTimecode() const;
   string getImageFormatName() const;
   CTimecode getInTimecode() const;
   CVideoInitInfo* getMainVideoInitInfo() const;
   CTimecode getOutTimecode() const;
   int getTimeTrackCount() const;
   CTimeInitInfo* getTimeTrackInitInfo(int timeTrackIndex) const;
   CTimecode getTimecodePrototype() const;
   int getVideoProxyCount() const;
   CVideoInitInfo* getVideoProxyInitInfo(int videoProxyIndex) const;
   CTimecode getVideoMediaDuration() const;
   CTimecode getAudioMediaDuration() const;

   void clearCreateDateTime();
   void clearArchiveDateTime();
   void setArchiveDateTime(const string& newDateTime);
   void setBinPath(const string& newBinPath);
   void setClipName(const string& newClipName);
   void setClipPtr(ClipSharedPtr &newClipPtr);
   void setDescription(const string& newDescription);
   void setDiskScheme(const string& newDiskScheme);
   void setCDIngestType(const ECDIngestType& newCDIngestType);
   void setInTimecode(const CTimecode& newInTimecode);
   void setOutTimecode(const CTimecode& newOutTimecode);

   int validate();
   int checkMediaStorage();
   int checkMetadataStorage();
   int checkTimecodes(int handleCount);

   void deleteAudioTrackInitInfo();
   void deleteTimeTrackInitInfo();
   void deleteVideoProxyInitInfo();

   int CheckRFL();

   int readClipScheme(const string& clipSchemeFileName);
   int readClipScheme(CIniFile *schemeFile);
   int readClipScheme2(CIniFile *schemeFile);
   int readClipInfo(CIniFile *ini);
   int writeClipInfo(CIniFile *ini);
   int writeClipScheme(const string& newSchemeFileName);
   int writeClipScheme(CIniFile *schemeFile);

   ClipSharedPtr createClip(int *status);

   // A god-awful hack to squirrel away the Media Location to make
   // the 'marked' clip hack work better....UGGH ...!
   void setMarkedClipMediaLoc(const string &newMediaLoc);
   string getMarkedClipMediaLoc() const;

private:
   void deleteInitInfo();
   int initFromClipScheme2(const string& newClipSchemeName);

private:
   string binPath;            // Directory path for the clip's Bin
   string clipName;           // Clip's file name without extension
   string description;        // Clip description
   ECDIngestType cdIngestType;// Ingest type used by Control Dailies
   string createDateTime;     // Creation date and time
                               // [ASCII string YYYY:MM:DD:HH:MM:SS]
   string archiveDateTime;    // Archive date and time
                              // [ASCII string YYYY:MM:DD:HH:MM:SS]

   string diskScheme_;      // Disk Scheme used to create the clip

   CTimecode inTimecode;      // Starting timecode
   CTimecode outTimecode;     // End timecode (exclusive, video style)

   int timecodeFrameRate;
   bool dropFrame;            // Set from Clip Scheme

   CVideoInitInfo *mainVideoInitInfo;
   VideoInitInfoList videoProxyInitInfoList;

   AudioInitInfoList audioTrackInitInfoList;

   TimeInitInfoList timeTrackInitInfoList;

   ClipSharedPtr clip;
   EClipInitSource sourceType; // Where did clip init info come from?

   string markedClipMediaLoc;  // UGGH

private:
   // Clip Scheme File Clip Information Section Name and Keywords
   static string clipInfoSectionName;
   static string binPathKey;
   static string clipNameKey;
   static string createDateTimeKey;
   static string archiveDateTimeKey;
   static string descriptionKey;
   static string cdIngestTypeKey;
   static string diskSchemeKey;
   static string dropFrameKey;
   static string timecodeFrameRateKey;
   static string inTimecodeKey;
   static string outTimecodeKey;
   static string markedClipMediaLocKey; // clip ini file only ...UGGH...!

   static string mainVideoSectionName;
   static string videoProxySectionName;
   static string audioTrackSectionName;
   static string timeTrackSectionName;

   static string metaDataFileName;

   static string metaDataSectionName;
   static string metaDataDirectoryKey;
   static string iniFileInMetadataDir;

   static string historySectionName;
   static string historyDirectoryKey;

// YOU CAN'T CHOOSE MONOLITHIC HISTORY ANYMORE!
//   static string historyTypeKey;
//   static string historyTypeFilePerClip;
//   static string historyTypeFilePerFrame;

};  // class CClipInitInfo

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CClip
{
private:
   CClip();

public:
   virtual ~CClip();

   static ClipSharedPtr Create();
   static ClipSharedPtr RetrieveSharedPtr(CClip *rawPtr);
   static void Destroy(CClip *rawPtr);
   static void DumpMap();

   int initClip(CClipInitInfo& initInfo);

   int fixPALFieldDominance(int videoProxyIndex); // kludge

   int addAudioTrack(CAudioInitInfo& initInfo);
   int addVideoProxy(CVideoInitInfo& initInfo);
   int addTimeTrack(CTimeInitInfo& initInfo);

   // Functions to change length of clip
   int extendClip(int frameCount, bool allocateMedia);
   int shortenClip(int frameCount);
   int freeClipAudio();
   int freeClipVideo();

   const CAudioFormat* getAudioFormat(int audioTrackIndex) const;
   CAudioFrameList* getAudioFrameList(int audioTrackIndex,
                                      int framingIndex = 0) const;
   int getAudioFramingCount() const;
   CAudioTrack* getAudioTrack(int audioTrackIndex) const;
   int getAudioTrackCount() const;

   string getBinPath() const;
   string getClipFileName() const;         // Clip file name with path, but
                                           // without file extension
   string getClipFileNameWithExt() const;  // Complete clip file name with
                                           // path and extension .clp
   CLIP_GUID getClipGUID() const;
   int getNewClipVersion();
   string getClipGUIDStr() const;
   string getClipPathName() const;
   string getClipName() const;
   string getRelativeClipPath() const;  // hahah yet another path/name variant

   string getCreateDateTime() const;
   string getArchiveDateTime() const;
   string getDescription() const;
   ECDIngestType getCDIngestType() const;
   string getDiskScheme() const;
   CTimecode getDurationTimecode() const;
   const CImageFormat* getImageFormat(int proxyIndex) const;
   void getInTimecode(CTimecode &tc) const;
   CTimecode getInTimecode() const;
   bool getLockRecordFlag() const;
   void getOutTimecode(CTimecode &tc) const;
   CTimecode getOutTimecode() const;

   int getTimeTrackCount() const;
   CTimeTrack* getTimeTrack(int timeTrackIndex) const;

   CVideoFrameList* getVideoFrameList(int proxyIndex, int framingIndex) const;
   int getVideoFramingCount() const;
   CVideoProxy* getVideoProxy(int proxyIndex) const;
   int getVideoProxyCount() const;

   // Function to find track by App Id
   CAudioTrack* findAudioTrack(const string& appId) const;
   int findAudioTrackIndex(const string& appId) const;
   CTimeTrack* findTimeTrack(const string &appId) const;
   int findTimeTrackIndex(const string &appId) const;
   CVideoProxy* findVideoProxy(const string &appId) const;
   int findVideoProxyIndex(const string &appId) const;

   void setBinPath(const string& newBinPath);
   void setClipName(const string& newClipName);
   void setCreateDateTime(const string& newDate);
   void setArchiveDateTime(const string& newDate);
   void setDescription(const string& newClipName);
   void setCDIngestType(const ECDIngestType& newCDIngestType);
   void setDiskScheme(const string & newDiskScheme);
   void setInTimecode(const CTimecode &tc);
   void setOutTimecode(const CTimecode &tc);
   void setLockRecordFlag(bool newFlag);

   int CheckVideoResolutionLicense();

   int renameClip(string newBinPath, string newClipName,
                  bool renameMediaFlag=true);

   int readFile();
   int refreshFromFile();
   int refreshMediaStorageFromFile();
   int writeFile();

   int deleteClipFile();
   bool isClipFileDeleted() const;

   void setVideoFrameListFileChanged(bool newFlag);
   bool didVideoFrameListFileChange();
   int updateAllVideoFrameListFiles();
   int writeAllVideoFrameListFiles();

   EMediaStatus getLastMediaAction(ETrackType audioVideoSelect,
                                   int trackIndex);
   string getLastMediaActionString(ETrackType audioVideoSelect,
                                   int trackIndex);
   int setCadenceRepairFlag (bool bArg);
   bool getCadenceRepairFlag ();
   int setRenderInfo(const string &sourceClip, int sourceFrameIndex,
                     int framingType);
   int getRenderInfo(string &sourceClip, int &sourceFrameIndex,
                     int &framingType);
   int setAlphaMatteTypes (EAlphaMatteType alphaType,
                               EAlphaMatteType alphaHiddenType);
   int getAlphaMatteTypes (EAlphaMatteType &alphaType,
                               EAlphaMatteType &alphaHiddenType);
   int setPixelComponents(EPixelComponents newPixelComponents);
   int getPixelComponents(EPixelComponents &pixelComponents);

   // Version clip framelist selector hack
   int getMainFrameListIndex();
   void setMainFrameListIndex(int newMainFrameListIndex);
   int getLockedFrameListIndex();
   void setLockedFrameListIndex(int newLockedFrameListIndex);

   // Version clip hack to prevent double media deletion
   bool isMediaDestroyed(void);
   void setMediaDestroyed(void);

   // Functions to Get & Set the Record Status
   ERecordStatus getRecordToComputerStatus(ETrackType audioVideoSelect,
                                           int trackIndex);
   static ERecordStatus getRecordToComputerStatus(const string& clipName,
                                                  ETrackType audioVideoSelect,
                                                  int trackIndex);
   ERecordStatus getRecordToVTRStatus(ETrackType audioVideoSelect,
                                      int trackIndex);
   static ERecordStatus getRecordToVTRStatus(const string& clipName,
                                             ETrackType audioVideoSelect,
                                             int trackIndex);

   int compareClipGUID(const CLIP_GUID& trialGUID);
   static void changeClipGUID(const string &binPath, const string &clipName);

   int updateRecordToComputerStatus(ETrackType audioVideoSelect,
                                    int trackIndex,
                                    ERecordStatus newRecordStatus);
   static int updateRecordToComputerStatus(const string& clipName,
                                           ETrackType audioVideoSelect,
                                           int trackIndex,
                                           ERecordStatus newRecordStatus);
   int updateRecordToVTRStatus(ETrackType audioVideoSelect,
                               int trackIndex, ERecordStatus newRecordStatus);
   static int updateRecordToVTRStatus(const string& clipName,
                                      ETrackType audioVideoSelect,
                                      int trackIndex,
                                      ERecordStatus newRecordStatus);
   int updateMediaModifiedStatus(ETrackType audioVideoSelect, int trackIndex);
   int updateArchivedStatus(ETrackType audioVideoSelect, int trackIndex);
   int updateCreatedStatus(ETrackType audioVideoSelect, int trackIndex);
   int updateClonedStatus(ETrackType audioVideoSelect, int trackIndex);
   int updateAllTracksToClonedStatus(
               const string& binPath,
               const string& clipName);
   int updateAllTracksToCreatedStatusAndClearArchiveDate(
               const string& binPath,
               const string& clipName);
   int updateAllTracksToArchivedStatusAndSetArchiveDate(
               const string& binPath,
               const string& clipName);
   int updateArchiveDateTime(const string& dateTime);

   int updateAllTrackFiles();
   int updateAllTrackFiles(int firstFrameIndex, int frameCount,
                           char *externalBuffer,
                           unsigned int externalBufferSize);

   //** Bin Manager speedup hack - mbraca
   // I made my own little callback hack to notify one interested party
   // (the bin manager form) that a clip's status has changed because I
   // don't understand mertus' cbhook stuff and he isn't in today
   void notifyMediaStatusChanged(ETrackType trackType, int trackNumber);
   static void NotifyClipStatusChanged(const string &binPath,
                                       const string &clipName);
   typedef void(*StatusChangeNotifyHandlerType)(const string &binPath,
                                                const string &clipName);
   static void RegisterForClipStatusChangeNotifications(
                                       StatusChangeNotifyHandlerType);
   static void UnregisterForClipStatusChangeNotifications();
   //**//


   static bool queryDropFrame(const string& dropFrameName);
   static string queryDropFrameName(bool dropFrameFlag);

   void dump(MTIostringstream& str);      // for debugging
   static string queryCDIngestTypeName (ECDIngestType cdIngestType);
   static ECDIngestType queryCDIngestType (const string &cdIngestTypeName);

public:
   static CStringValueList<EClipVersion> clipVerLookup;
   
private:
   MTI_UINT64 _clipFileModifiedTime = 0;  // to detect staleness
   string description;        // Clip description
   ECDIngestType cdIngestType;// Ingest type used by Control Dailies
   string diskScheme_;        // Disk Scheme used to create clip
   string createDateTime;     // Creation date and time
                               // [ASCII string YYYY:MM:DD:HH:MM:SS]
   string archiveDateTime;    // Archive date and time
                              // [ASCII string YYYY:MM:DD:HH:MM:SS]

   string projectName;        // Clip's project
   string binPath;            // Directory path for the clip's Bin
   string clipName;           // Clip's file name without extension
   // Clip file's complete path is binPath/clipName/clipName.clp

   CTimecode inTimecode;       // Starting timecode
   CTimecode outTimecode;      // End timecode (exclusive, video style)

   bool lockRecordFlag;        // If set, media cannot be modified TBD

   bool clipFileUpgradeNeeded; // If set, clip file should be upgraded
   bool videoFrameListFileChanged; // If set, write frame list files at close

   // Video Proxies
   CVideoProxyList videoProxyList;
   CVideoFramingInfoList videoFramingInfoList;

   // Audio Tracks
   CAudioTrackList audioTrackList;
   CAudioFramingInfoList audioFramingInfoList;

   // Timecode Tracks
   CTimeTrackList timeTrackList;

   // GUID - Globally Unique Identifier for Clip
   CLIP_GUID *clipGUID;

   // Version number
   int clipVersion;

   // TBD: Clip-Level Metadata

   bool clipFilesDeleted;     // Set to true when clip files have been deleted

private:

   int addAudioTrack_ClipVer3(CAudioInitInfo& initInfo);
   int addAudioTrack_ClipVer5L(CAudioInitInfo& initInfo);
   int addTimeTrack_ClipVer3(CTimeInitInfo& initInfo);
   int addTimeTrack_ClipVer5L(CTimeInitInfo& initInfo);
   int addVideoProxy_ClipVer3(CVideoInitInfo& initInfo);
   int addVideoProxy_ClipVer5L(CVideoInitInfo& initInfo);
   int MakeAudioMediaIdentifier(const CAudioInitInfo &initInfo,
                                int newAudioTrackNumber,
                                string &newMediaIdentifier);

   CTrack* getTrack(ETrackType trackType, int trackIndex);
   int readClipInfoSection(CIniFile *clipIniFile);
   int writeClipInfoSection(CIniFile *clipIniFile);

   CIniFile* getClipIniFile();

public:  // bin manager wants to call these
   int renameClipReferenceTargets(string newClipName);
   int renameClipMedia(string newBinPath, string newClipName);
   int writeMediaStorageSection(int proxyIndex, CMediaStorageList *mediaStorageList);
   int updateMediaStorageSection(int proxyIndex, CMediaStorageList *mediaStorageList);

   static StatusChangeNotifyHandlerType statusChangeNotifyHandler;

private:
   // Clip File Clip Information Section Name and Keywords
   static string clipInfoSectionName;
   static string descriptionKey;
   static string cdIngestTypeKey;
   static string diskSchemeKey;
   static string createDateTimeKey;
   static string archiveDateTimeKey;
   static string projectNameKey;
   static string timecodeFrameRateKey;
   static string dropFrameKey;
   static string timecode720PKey;
   static string inTimecodeKey;
   static string outTimecodeKey;
   static string videoProxyCountKey;
   static string lockRecordKey;
   static string clipGUIDKey;
   static string clipVersionKey;
   static string mediaDestroyedKey; // Snowballing version clip hack, d'oh!

   static string nullClipGUIDStr;

   // Clip2Clip render info section
   static string renderInfoSectionName;
   static string sourceClipNameKey;
   static string sourceFrameIndexKey;
   static string renderFramingTypeKey;

private:
   // Make Copy Constructor and Assignment Operator private
   CClip(const CClip& rhs);
   CClip& operator=(const CClip& rhs);

}; // class CClip


class CGuidBackupSet;


class MTI_CLIPLIB_API CClipBackup
{
public:
   CClipBackup(string const &clipDir);
   ~CClipBackup();

   CClipBackup(CClipBackup const &rhs);
   CClipBackup &operator=(CClipBackup const &rhs);

   int BackupAllFiles();
   int RestoreAllFiles();
   void SetClipDir(string path);

private:
   DEFINE_FILE_SWEEPER(BackupFiles, BackupFile, true, CClipBackup);
   DEFINE_FILE_SWEEPER(RestoreFiles, RestoreFile, true, CClipBackup);
   bool BackupFile(string file);
   bool RestoreFile(string file);
   int GetRFLFileNames(vector<string> &RFLFileNames);
   int BackupRFLFiles();
   int RestoreRFLFiles();
   bool WriteBackupSetIdentifier() const;
   bool ReadBackupSetIdentifier();

   string clipDir_;
   CGuidBackupSet *guidBackupSetPtr_;
}; // class CClipBackup

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
// Global Functions

int MTI_CLIPLIB_API QueryImageTimeRemaining(EMediaType mediaType,
                                            const string& mediaIdentifier,
                                            const string& clipSchemeName,
                                            CTimecode& totalTimeRemaining,
                                            CTimecode& largestTimeRemaining);

int MTI_CLIPLIB_API QueryImageTimeRemaining(const string& diskSchemeName,
                                            const string& clipSchemeName,
                                            CTimecode& totalTimeRemaining,
                                            CTimecode& largestTimeRemaining);

int MTI_CLIPLIB_API QueryImageTimeRemaining(const CClipInitInfo& clipInitInfo,
                                            CTimecode& totalTimeRemaining,
                                            CTimecode& largestTimeRemaining);

int MTI_CLIPLIB_API QueryImageTimeTotal(const string& diskSchemeName,
                                        const string& clipSchemeName,
                                        CTimecode& totalTimeRemaining);

int MTI_CLIPLIB_API QueryImageTimeTotal(const CClipInitInfo& clipInitInfo,
                                        CTimecode& totalTimeRemaining);

#if 0
int MTI_CLIPLIB_API QueryAudioTimeRemaining(EMediaType mediaType,
                                            const string& mediaIdentifier,
                                            const string& clipSchemeName,
                                            CTimecode& totalTimeRemaining,
                                            CTimecode& largestTimeRemaining);
#endif

int MTI_CLIPLIB_API QueryAudioTimeRemaining(const string& diskSchemeName,
                                            const string& clipSchemeName,
                                            CTimecode& totalTimeRemaining,
                                            CTimecode& largestTimeRemaining);

int MTI_CLIPLIB_API QueryAudioTimeRemaining(const CClipInitInfo& clipInitInfo,
                                            CTimecode& totalTimeRemaining,
                                            CTimecode& largestTimeRemaining);

int MTI_CLIPLIB_API QueryAudioTimeTotal(const string& diskSchemeName,
                                        const string& clipSchemeName,
                                        CTimecode& totalTimeRemaining);

int MTI_CLIPLIB_API QueryAudioTimeTotal(const CClipInitInfo& clipInitInfo,
                                        CTimecode& totalTimeRemaining);

//////////////////////////////////////////////////////////////////////

#ifdef PRIVATE_STATIC
#ifndef _CLIP_DATA_CPP
namespace ClipData {
   extern string clipInfoSectionName;
   extern string descriptionKey;
   extern string cdIngestTypeKey;
   extern string diskSchemeKey;
   extern string createDateTimeKey;
   extern string archiveDateTimeKey;
   extern string projectNameKey;
   extern string dropFrameKey;
   extern string inTimecodeKey;
   extern string outTimecodeKey;
   extern string lockRecordKey;
   extern string clipGUIDKey;

   extern string nullClipGUIDStr;

   extern string renderInfoSectionName;
   extern string sourceClipNameKey;
   extern string sourceFrameIndexKey;
   extern string renderFramingTypeKey;

   extern string binPathKey;
   extern string clipNameKey;

   extern string mainVideoSectionName;
   extern string videoProxySectionName;
   extern string audioTrackSectionName;
   extern string timeTrackSectionName;

   extern string appIdKey;
   extern string handleCountKey;
   extern string virtualMediaKey;
   extern string mediaTypeKey;
   extern string mediaIdentifierKey;

   extern string imageFile0IndexKey;

   extern string frameRateKey;
   extern string mediaDirectoryKey;

   extern string timeTypeKey;
};
#endif
#endif

//////////////////////////////////////////////////////////////////////

#endif // #ifndef Clip3H


