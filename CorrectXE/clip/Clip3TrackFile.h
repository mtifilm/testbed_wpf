// Clip3TrackFile.h: interface for the CFrameListFileReader and
//                   CFrameListFileWriter classes.
//
// Created by: John Starr, November 5, 2001
//
/* CVS Info:
$Header: /usr/local/filmroot/clip/include/Clip3TrackFile.h,v 1.5 2003/04/30 20:21:52 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef CLIP3TRACKFILEH
#define CLIP3TRACKFILEH

#include "FileBuilder.h"
#include "FileRecord.h"
#include "FileRecordString.h"

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CClip;
class CFileRecord;
class CFileScanner;
class CField;
class CFieldFileRecord;
class CFieldList;
class CFrame;
class CFrameFileRecord;
class CMediaStorageList;
class CStructuredFileStream;
class CTimecode;
class CTimecodeFileRecord;

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

#define FRAME3_RECORD_RESERVED_COUNT    2

//////////////////////////////////////////////////////////////////////

class CFrameFileRecord : public CFileRecord
{
public:
   CFrameFileRecord(MTI_UINT32 type, MTI_UINT16 version);
   virtual ~CFrameFileRecord();

   virtual int read(CStructuredStream& stream);
   virtual int write(CStructuredStream &stream);

   MTI_UINT8 getFieldCount() const;
   void getTimecode(CTimecode &timecode) const;
   const CTimecode& getTimecode() const;

   void setFieldCount(MTI_UINT8 newFieldCount);
   void setTimecode(const CTimecode &newTimecode);

   void calculateRecordByteCount();

private:
   mutable CTimecodeFileRecord* timecodeRecord;
   MTI_UINT8 fieldCount;

   MTI_UINT32 frameReserved[FRAME3_RECORD_RESERVED_COUNT]; // Future use
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

// Space reserved for future use without changing the record size
#define FIELD3_RECORD_RESERVED_COUNT  2

//////////////////////////////////////////////////////////////////////

class CFieldFileRecord : public CFileRecord
{
public:
   CFieldFileRecord(MTI_UINT32 type, MTI_UINT16 version);
   virtual ~CFieldFileRecord();

   // Stream read and write functions
   int read(CStructuredStream& stream);
   virtual int write(CStructuredStream &stream);

   // Accessor functions
   MTI_INT64 getDataIndex() const;
   MTI_UINT32 getFieldFlags() const;
   MTI_INT32 getFieldListEntryIndex() const;
   const string& getFieldName() const;
   MTI_UINT16 getMediaStorageIndex() const;
   MTI_UINT32 getDataSize() const;

   void setDataIndex(MTI_INT64 newDataIndex);
   void setDataSize(MTI_UINT32 newDataSize);
   void setFieldFlags(MTI_UINT32 newFieldFlags);
   void setFieldListEntryIndex(MTI_INT32 newIndex);
   void setFieldName(const char* newFieldName);
   void setMediaStorageIndex(MTI_UINT16 newMediaStorageIndex);

   void calculateRecordByteCount();

private:
   MTI_UINT16 mediaStorageIndex;       // Index into clip file's mediaStorageList
   MTI_INT64 dataIndex;                // Index to start of media data in
                                       // media file
   MTI_UINT32 dataSize;                // Total size of media data, in units that
                                       // are dependent on the media storage type
   CFileRecordString fieldName;        // Field-specific portion of a filename
                                       // when combined with base name from 
                                       // clip file's mediaStorageList
   MTI_UINT32 flags;                   // Field flags
   MTI_UINT32 fieldReserved[FIELD3_RECORD_RESERVED_COUNT];  // Future use

   MTI_INT32 fieldListEntryIndex;
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class CFrameListFileReader  
{
public:
	CFrameListFileReader();
	virtual ~CFrameListFileReader();

protected:

   int openFrameListFile(const string& frameListFileName);
   void closeFrameListFile();

   void setBaseFieldFromFileRecord(CField& field,
                                   CFieldList *newFieldList,
                                   const CMediaStorageList& mediaStorageList,
                                   const CFieldFileRecord& fieldRecord);
   void setBaseFrameFromFileRecord(CFrame& frame,
                                   const CFrameFileRecord& frameRecord);

protected:
   CStructuredFileStream *stream;
   CFileScanner *fileScanner;
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class CFrameListFileWriter  
{
public:
   CFrameListFileWriter();
   virtual ~CFrameListFileWriter();

protected:
   void setBaseFieldToFileRecord(const CField& field,
                                 const CMediaStorageList &mediaStorageList,
                                 CFieldFileRecord &fieldRecord);
   void setBaseFrameToFileRecord(const CFrame& frame,
                                 CFrameFileRecord &frameRecord);

protected:
   CFileBuilder fileBuilder;
};

#endif // #ifndef CLIP3_TRACK_FILE_H
