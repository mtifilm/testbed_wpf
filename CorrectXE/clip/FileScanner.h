// FileScanner.h: interface for the FileScanner class.
//
/*
$Header: /usr/local/filmroot/clip/include/FileScanner.h,v 1.1.1.1 2001/06/18 13:38:25 mertus Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef FILESCANNERH
#define FILESCANNERH

#include "machine.h"
#include "FileRecord.h"

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CStructuredStream;
class CCommonHeaderFileRecord;

//////////////////////////////////////////////////////////////////////

class CFileScanner  
{
public:
   CFileScanner();
   virtual ~CFileScanner();

   int initForReading(CStructuredStream *stream);
   int readNextRecord(CFileRecord **newRecordPtr);

   CCommonHeaderFileRecord* getCommonHeaderRecord();
   EFileType getFileType() const;
   MTI_UINT16 getFileVersion() const;
   CStructuredStream* getStream() const;

private:
   MTI_UINT32 magicWord;
   CCommonHeaderFileRecord* commonHeaderRecord;
   CStructuredStream* stream;
   EFileType fileType;
   MTI_UINT16 fileVersion;
};

//////////////////////////////////////////////////////////////////////

#endif // #ifndef FILESCANNER_H
