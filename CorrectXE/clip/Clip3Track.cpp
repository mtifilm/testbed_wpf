// Clip3Track.cpp: implementation of the CFrameList class.
//
/*
$Header: /usr/local/filmroot/clip/source/Clip3Track.cpp,v 1.124.2.5 2009/10/26 19:58:50 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////

#define _CLIP_3_TRACK_DATA_CPP

#include "BinManager.h"
#include "Clip3.h"
#include "Clip3Track.h"
#include "Clip3FieldListFile.h"
#include "err_clip.h"
#include "FileSweeper.h"
#include "ImageFormat3.h"
#include "IniFile.h"
#include "MediaAccess.h"
#include "MediaAllocator.h"
#include "MediaDeallocator.h"
#include "MediaStorage3.h"
#include "MetaData.h"
#include "MTIstringstream.h"

#include <math.h>
#include <sys/stat.h>
#include <memory>          // for auto_ptr


#ifdef PRIVATE_STATIC
//////////////////////////////////////////////////////////////////////
// The following variables used to be static class members.
// I switched the to namespaced globals for compatability with
// visual c++.  -Aaron Geman  7/9/04
//////////////////////////////////////////////////////////////////////

namespace Clip3TrackData {
   string videoFramesTrackKey = "VideoFramesTrack";
   string filmFramesTrackKey = "FilmFramesTrack";
   string maxCustomFramesTracksKey = "MaxCustomFrameTracks";
   string customFramesTrackKey = "CustomFramesTrack";

   string trackTypeKey = "TrackType";
   string framingTypeKey = "FramingType";
   string descriptionKey = "Description";
   string frameRateKey = "FrameRate";

   string frameListFileExtension = "trk";

   string frame0TimecodeKey = "Frame0Timecode";
   string inTimecodeKey = "InTimecode";
   string outTimecodeKey = "OutTimecode";
   string inFrameIndexKey = "InFrameIndex";
   string outFrameIndexKey = "OutFrameIndex";
   string mediaAllocatedFrameCountKey = "MediaAllocatedFrameCount";

   string handleCountKey = "HandleCount";
   string recordToComputerStatusKey = "RecordToComputerStatus";
   string recordToVTRStatusKey = "RecordToVTRStatus";
   string lastMediaActionKey = "LastMediaAction";
   string virtualMediaKey = "VirtualMedia";
   string appIdKey = "AppId";

   string maxTrackIndexKey = "MaxTrackIndex";
};
using namespace Clip3TrackData;
#endif

//////////////////////////////////////////////////////////////////////
// Static Member Variables
//////////////////////////////////////////////////////////////////////

string CFramingInfoList::videoFramesTrackKey = "VideoFramesTrack";
string CFramingInfoList::filmFramesTrackKey = "FilmFramesTrack";
string CFramingInfoList::maxCustomFramesTracksKey = "MaxCustomFrameTracks";
string CFramingInfoList::customFramesTrackKey = "CustomFramesTrack";

string CFramingInfo::trackTypeKey = "TrackType";
string CFramingInfo::framingTypeKey = "FramingType";
string CFramingInfo::descriptionKey = "Description";
string CFramingInfo::frameRateKey = "FrameRate";

string CFrameList::frameListFileExtension = "trk";

string CFrameList::frame0TimecodeKey = "Frame0Timecode";
string CFrameList::inTimecodeKey = "InTimecode";
string CFrameList::outTimecodeKey = "OutTimecode";
string CFrameList::inFrameIndexKey = "InFrameIndex";
string CFrameList::outFrameIndexKey = "OutFrameIndex";
string CFrameList::mediaAllocatedFrameCountKey = "MediaAllocatedFrameCount";

string CTrack::clipVerKey = "ClipVer";
string CTrack::trackIDKey = "TrackID";
string CTrack::handleCountKey = "HandleCount";
string CTrack::recordToComputerStatusKey = "RecordToComputerStatus";
string CTrack::recordToVTRStatusKey = "RecordToVTRStatus";
string CTrack::lastMediaActionKey = "LastMediaAction";
string CTrack::cadenceRepairFlagKey = "CadenceRepairFlag";
string CTrack::virtualMediaKey = "VirtualMedia";
string CTrack::appIdKey = "AppId";

string CTrackList::maxTrackIndexKey = "MaxTrackIndex";

// ----------------------------------------------------------------------------
// Record Status/Name Map Table

struct SRecordStatusMap
{
   const char* name;
   ERecordStatus recordStatus;
};

static const SRecordStatusMap recordStatusMap[] =
{
   {"NotRecorded",       RECORD_STATUS_NOT_RECORDED  }, // Not recorded
   {"PartiallyRecorded", RECORD_STATUS_PART_RECORDED }, // Record failed part way in
   {"Recorded",          RECORD_STATUS_RECORDED      }, // Recorded
   {"Scratch",           RECORD_STATUS_SCRATCH       }, // Can be recorded at will
   {"Locked",            RECORD_STATUS_LOCKED        }, // Locked, cannot be changed
};
#define RECORD_STATUS_COUNT (sizeof(recordStatusMap)/sizeof(SRecordStatusMap))

// ----------------------------------------------------------------------------
// Media Status/Name Map Table

struct SMediaStatusMap
{
   const char* name;
   EMediaStatus mediaStatus;
};

static const SMediaStatusMap mediaStatusMap[] =
{
   {"Marked",             MEDIA_STATUS_MARKED                    },
   {"Created",            MEDIA_STATUS_CREATED                   },
   {"RecToCom-P",         MEDIA_STATUS_RECORD_TO_COMPUTER_PARTIAL},
   {"RecToCom",           MEDIA_STATUS_RECORD_TO_COMPUTER        },
   {"Modified",           MEDIA_STATUS_MODIFIED                  },
   {"RecToVTR-P",         MEDIA_STATUS_RECORD_TO_VTR_PARTIAL     },
   {"RecToVTR",           MEDIA_STATUS_RECORD_TO_VTR             },
   {"Retired",            MEDIA_STATUS_RETIRED                   },
   {"Deleted",            MEDIA_STATUS_DELETED                   },
   {"Cadence Fix",        MEDIA_STATUS_CADENCE_REPAIR            },
   {"Alpha Hidden",       MEDIA_STATUS_ALPHA_HIDDEN              },
	{"Alpha Shown",        MEDIA_STATUS_ALPHA_UNHIDDEN            },
   {"Alpha Removed",      MEDIA_STATUS_ALPHA_DESTROYED           },
	{"FPS Changed",        MEDIA_STATUS_FPS_CHANGED               },
	{"FPS --> 23.98",      MEDIA_STATUS_FPS_CHANGED_TO_2398       },
	{"FPS --> 24",         MEDIA_STATUS_FPS_CHANGED_TO_24         },
	{"FPS --> 25",         MEDIA_STATUS_FPS_CHANGED_TO_25         },
	{"FPS --> 29.97",      MEDIA_STATUS_FPS_CHANGED_TO_2997       },
	{"FPS --> 30",         MEDIA_STATUS_FPS_CHANGED_TO_30         },
	{"Archived",           MEDIA_STATUS_ARCHIVED                  },
   {"Cloned",             MEDIA_STATUS_CLONED                    }
};
#define MEDIA_STATUS_COUNT (sizeof(mediaStatusMap)/sizeof(SMediaStatusMap))


//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                          #####################################
// ###    CFramingInfo Class     ####################################
// ####                          #####################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFramingInfo::CFramingInfo(ETrackType newTrackType, int newFramingNumber,
                             int newFramingType)
: framingNumber(newFramingNumber), trackType(newTrackType),
  framingType(newFramingType),
  frameRateEnum(IF_FRAME_RATE_INVALID), frameRate(1),     // WTF?? 1?
  saveRecordFlag(false)
{

}

CFramingInfo::~CFramingInfo()
{
}

bool CFramingInfo::checkTrackType(ETrackType newTrackType)
{
   switch (newTrackType)
      {
      case TRACK_TYPE_VIDEO :
      case TRACK_TYPE_AUDIO :
         return true;
      case TRACK_TYPE_INVALID :
      default :
         return false;
      }
}

//////////////////////////////////////////////////////////////////////
// Accessor Functions
//////////////////////////////////////////////////////////////////////

string CFramingInfo::getDescription() const
{
   return description;
}

EFrameRate CFramingInfo::getFrameRateEnum() const
{
   return frameRateEnum;
}

int CFramingInfo::getFramingType() const
{
   return framingType;
}

MTI_REAL32 CFramingInfo::getFrameRate() const
{
   return frameRate;
}

bool CFramingInfo::getSaveRecordFlag() const
{
   return saveRecordFlag;
}

int CFramingInfo::getFramingNumber() const
{
   return framingNumber;
}

ETrackType CFramingInfo::getTrackType() const
{
   return trackType;
}

//////////////////////////////////////////////////////////////////////
// Mutator Functions
//////////////////////////////////////////////////////////////////////

void CFramingInfo::setDescription(const string &newDescription)
{
   description = newDescription;
}

void CFramingInfo::setFrameRate(EFrameRate newFrameRateEnum,
                                 double newFrameRate)
{
   // Note: this function does not set the frame rate within the clip's various
   //       timecodes.  In fact, the timecodes' frame rates may deliberately
   //       be different from the clips frame rate

   frameRateEnum = newFrameRateEnum;

   if (frameRateEnum == IF_FRAME_RATE_NON_STANDARD)
      {
      // Caller is supplying a non-standard frame rate,
      // so copy newFrameRate argument to frameRate member
      frameRate = newFrameRate;
      }
   else
      {
      // Caller is requesting a standard frame rate, so query for the
      // corresponding floating pt frame rate value and set frameRate member
      frameRate = CImageInfo::queryStandardFrameRate(newFrameRateEnum);
      }
}

void CFramingInfo::setFramingNumber(int newFramingNumber)
{
   framingNumber = newFramingNumber;
}

void CFramingInfo::setFramingType(int newFramingType)
{
   if (newFramingType < FRAMING_TYPE_INVALID)
      framingType = FRAMING_TYPE_INVALID;
   else
      framingType = newFramingType;
}

void CFramingInfo::setSaveRecordFlag(bool newFlag)
{
   saveRecordFlag = newFlag;
}

//////////////////////////////////////////////////////////////////////
// Clip IniFile Interface
//////////////////////////////////////////////////////////////////////

ETrackType CFramingInfo::getTrackTypeFromTrackSection(CIniFile *iniFile,
                                                     const string& sectionName)
{
   if (!iniFile->SectionExists(sectionName))
      {
      // ERROR: Track section does not exist
      return TRACK_TYPE_INVALID;
      }

   // Verify that the track type key exists within the section
   if (!iniFile->KeyExists(sectionName, trackTypeKey))
      {
      // ERROR: Track Type key is missing
      return TRACK_TYPE_INVALID;
      }

   // Read Track Type
   int newTrackType = iniFile->ReadInteger(sectionName, trackTypeKey,
                                           TRACK_TYPE_INVALID);

   // Validate the value just read from section
   if (!checkTrackType(ETrackType(newTrackType)))
      {
      // ERROR: Invalid track type
      return TRACK_TYPE_INVALID;
      }

   return ETrackType(newTrackType);
}

//------------------------------------------------------------------------
//
// Function:    readFramingInfoSection
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
int CFramingInfo::readFramingInfoSection(CIniFile *iniFile,
                                          const string& sectionName)
{
   if (iniFile == 0)
      {
      // CIniFile is not available
      return -1;
      }

   if (!iniFile->SectionExists(sectionName))
      {
      // ERROR: Track section does not exist
      return -2;
      }

   // Verify that the appropriate keys exist within the section
   // These keys are not optional and do not have default values
   if (!iniFile->KeyExists(sectionName, trackTypeKey)
       || !iniFile->KeyExists(sectionName, descriptionKey)
       || !iniFile->KeyExists(sectionName, frameRateKey)
       )
      {
      // ERROR: Clip Information section key does not exist
      return -30;
      }

   string emptyString, valueString, defaultValue;

   // Read Track Type
   int newTrackType = iniFile->ReadInteger(sectionName, trackTypeKey,
                                           TRACK_TYPE_INVALID);
   if (!checkTrackType(ETrackType(newTrackType)))
      {
      // ERROR: Missing or invalid track type
      return CLIP_ERROR_INVALID_TRACK_TYPE;
      }

   // Read Framing Type
   // Default value is the framing type that was set when the
   // this CFramingInfo instance was constructed
   int newFramingType = iniFile->ReadInteger(sectionName, framingTypeKey,
                                             getFramingType());

   // Read Track Description
   string newDescription = iniFile->ReadString(sectionName, descriptionKey,
                                               emptyString);

   // Read Frame Rate
   valueString = iniFile->ReadString(sectionName, frameRateKey, emptyString);
   double newFrameRate;
   EFrameRate newFrameRateEnum;
   newFrameRateEnum = CImageInfo::queryFrameRate(valueString, newFrameRate);
   if (newFrameRateEnum == IF_FRAME_RATE_INVALID)
      {
      TRACE_0(errout << "ERROR: Framing Info has bogus frame rate: " << endl
                     << "       " << iniFile->FileName);
      return CLIP_ERROR_INVALID_FRAME_RATE;
      }

   // Set new values in *this
   trackType = ETrackType(newTrackType);
   framingType = newFramingType;
   description = newDescription;
   frameRateEnum = newFrameRateEnum;
   frameRate = newFrameRate;

   return 0;

} // readFramingInfoSection( )

//------------------------------------------------------------------------
//
// Function:    writeFramingInfoSection
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
int CFramingInfo::writeFramingInfoSection(CIniFile *iniFile,
                                           const string& sectionName)
{
   if (iniFile == 0)
      {
      // CIniFile is not available
      return -1;
      }

   // Write Track Type enum as an integer
   iniFile->WriteInteger(sectionName, trackTypeKey, trackType);

   // Write Framing Type
   iniFile->WriteInteger(sectionName, framingTypeKey, framingType);

   // Write Clip Description
   iniFile->WriteString(sectionName, descriptionKey, description);

   // Write Frame Rate as frames-per-second value
   string frameRateString;
   if (frameRateEnum == IF_FRAME_RATE_NON_STANDARD)
      {
      // Non-standard frame rate, so convert frame rate double to a string
      // using default formatting of MTIostringstream
      MTIostringstream ostr;
      ostr << frameRate;
      frameRateString = ostr.str();
      }
   else
      {
      // If the frameRateEnum is not non-standard, assume it is a standard
      // frame rate.  Given the standard frame rate, get a string that
      // corresponds to the standard frames-per-second (this avoids problems
      // with floating point formating and rounding)
      frameRateString = CImageInfo::queryFrameRateName(frameRateEnum);
      }
   iniFile->WriteString(sectionName, frameRateKey, frameRateString);

   return 0;

} // writeFramingInfoSection

//////////////////////////////////////////////////////////////////////
// Testing Functions
//////////////////////////////////////////////////////////////////////

void CFramingInfo::dump(MTIostringstream &str)
{
   str << "Track Header" << std::endl;

   str << "  Description: <" << getDescription() << ">" << std::endl;

//   str << "  In Timecode: " << getInTimecode()
//       << "  Out Timecode: " << getOutTimecode()
//       << "  Out Timecode: " << getInTimecode() + getFrameCount() << std::endl
//       << "  Frame Count: " << getFrameCount()
//       <<  std::endl;

}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                              #################################
// ###    CFramingInfoList Class     ################################
// ####                              #################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFramingInfoList::CFramingInfoList()
{
}

CFramingInfoList::~CFramingInfoList()
{
   deleteFramingInfoList();
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

int CFramingInfoList::getFramingCount() const
{
   return (int)framingInfoList.size();
}

CFramingInfo* CFramingInfoList::getBaseFramingInfo(int framingIndex) const
{
   if (framingIndex < 0 || framingIndex >= getFramingCount())
      {
      // Framing Index is out-of-range, return NULL pointer
      return 0;
      }

   return framingInfoList[framingIndex];
}

int CFramingInfoList::setFramingInfo(int framingIndex,
                                     CFramingInfo *newFramingInfo)
{
   if (framingIndex < 0 || framingIndex >= getFramingCount())
      {
      // Framing Index is out-of-range, return NULL pointer
      return -1;
      }

   framingInfoList[framingIndex] = newFramingInfo;

   return 0;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

CFramingInfo* CFramingInfoList::createVideoFramesFramingInfo(ClipSharedPtr &clip,
                                          const CCommonInitInfo &commonInitInfo)
{
   // Check that video track info list is currently empty
   if (getFramingCount() != 0)
      {
      // ERROR: Framing Ifo list already has something in it,
      //        so cannot create a new video-frames Framing Info
      return 0;
      }

   // Construct a new CFramingInfo subtype and add it to the Framing Info list
   CFramingInfo* framingInfo = createFramingInfo(FRAMING_TYPE_VIDEO);
   addFramingInfo(framingInfo);

   // Set various parameters based on clip and "normal" video proxy

   // Get the frame rate from the track's init info.  If it is zero or
   // invalid, then use a default value based on the image format type
   EFrameRate newFrameRateEnum = commonInitInfo.getFrameRateEnum();

   if (newFrameRateEnum == IF_FRAME_RATE_INVALID)
      {

      const CImageFormat *imageFormat = clip->getImageFormat(VIDEO_SELECT_NORMAL);
      EImageFormatType imageFormatType = imageFormat->getImageFormatType();

      newFrameRateEnum = CImageInfo::queryNominalFrameRateEnum(imageFormatType);
      }

   framingInfo->setFrameRate(newFrameRateEnum, 0.0);

   framingInfo->setDescription("Video-Frames");

   return framingInfo;
}

CFramingInfo* CFramingInfoList::createFilmFramesFramingInfo(ClipSharedPtr &clip,
                                                           bool forceNoPulldown)
{
   // Check that the Framing Info list has exactly one item in it,
   // presumed to be the video-frames
   if (getFramingCount() != 1)
      {
      // ERROR: Framing Info info list is either missing the video-frames
      //        Framing Info or already has more than the video-frames
      return 0;
      }

   const CImageFormat *imageFormat = clip->getImageFormat(VIDEO_SELECT_NORMAL);
   EImageFormatType imageFormatType = imageFormat->getImageFormatType();
   EPulldown nominalPulldown
                           = CImageInfo::queryNominalPulldown(imageFormatType);

   CFramingInfo* videoFramesFramingInfo;
   videoFramesFramingInfo = getBaseFramingInfo(FRAMING_SELECT_VIDEO);

   if (   forceNoPulldown
       || nominalPulldown == IF_PULLDOWN_NONE
       || nominalPulldown == IF_PULLDOWN_INVALID)
      {
      // No pulldown, so the Framing Info list entry for film-frames
      // points to the video-frames Framing Info
      addFramingInfo(videoFramesFramingInfo);
      return videoFramesFramingInfo;
      }
   else
      {
      // Construct separate Framing Info for film-frames with
      // 3:2 pulldown removed
      CFramingInfo* filmFramesFramingInfo;
      filmFramesFramingInfo = createFramingInfo(FRAMING_TYPE_FILM);
      addFramingInfo(filmFramesFramingInfo);

      // Set various parameters derived from existing video-frames
      // track info

      filmFramesFramingInfo->setDescription("Film-Frames");

      // Set the frame rate, scaled for the 3:2 pulldown removal
      EFrameRate newFrameRateEnum = videoFramesFramingInfo->getFrameRateEnum();
      double newFrameRate = videoFramesFramingInfo->getFrameRate();
      CImageInfo::queryFilmFramesFrameRate(nominalPulldown, &newFrameRateEnum,
                                           &newFrameRate);
      filmFramesFramingInfo->setFrameRate(newFrameRateEnum, newFrameRate);

      return filmFramesFramingInfo;
      }

} // createFilmFramesFramingInfo

CFramingInfo* CFramingInfoList::createVideoFieldsFramingInfo(ClipSharedPtr &clip,
                                                             int newFramingType,
                                                            int newFramingIndex)
{
/*
   // Check that the Framing Info list has exactly two items in it,
   // presumed to be the video-frames and film-frames
   if (getFramingCount() < 2)
      {
      // ERROR: Framing Info info list is either missing the video-frames
      //        and film-frames Framing Info.  Possibly an audio track
      return 0;
      }
*/
   if (newFramingType != FRAMING_TYPE_FIELDS_1_2
       && newFramingType != FRAMING_TYPE_FIELD_1_ONLY
       && newFramingType != FRAMING_TYPE_FIELD_2_ONLY)
      {
      return 0;   // ERROR: bad framing type, shouldn't ever happen
      }
/*
   if (!(clip.getImageFormat(VIDEO_SELECT_NORMAL)->getInterlaced()))
      {
      // The image format is not interlaced, so the video fields framing
      // is not relevant.  Keep the NULL pointer.
      return 0;
      }
   else
*/
      {
      CFramingInfo* videoFramesFramingInfo;
      videoFramesFramingInfo = getBaseFramingInfo(FRAMING_SELECT_VIDEO);

      CFramingInfo* framingInfo = createFramingInfo(newFramingType);
      if (newFramingIndex == -1)
         addFramingInfo(framingInfo);  // Add to list
      else
         framingInfoList[newFramingIndex] = framingInfo; // Set in existing

      // Set the frame rate, possibly scaled for the field doubling
      EFrameRate frameRateEnum = videoFramesFramingInfo->getFrameRateEnum();
      EFrameRate newFrameRateEnum;
      double frameRate = videoFramesFramingInfo->getFrameRate();
      double newFrameRate;
      if (newFramingType == FRAMING_TYPE_FIELDS_1_2)
         {
         // The frame rate needs to be doubled for the field 1, 2 sequence
         // Do some trickery with CImageInfo
         EVideoFieldRate vidFieldRate
                = CImageInfo::convertFrameRateToFieldRate(frameRateEnum, true);
         newFrameRateEnum
                = CImageInfo::convertFieldRateToFrameRate(vidFieldRate, false);
         newFrameRate = 2.0 * frameRate;
         }
      else
         {
         // For FRAMING_TYPE_FIELD_1_ONLY and FRAMING_TYPE_FIELD_2_ONLY,
         // the fields are doubled, but there are only half as many fields,
         // so the normal frame rate is right.
         newFrameRateEnum = frameRateEnum;
         newFrameRate = frameRate;
         }
      framingInfo->setFrameRate(newFrameRateEnum, newFrameRate);

      string newDescription;
      if (newFramingType == FRAMING_TYPE_FIELDS_1_2)
         newDescription = "Video-Fields-1-2";
      else if (newFramingType == FRAMING_TYPE_FIELD_1_ONLY)
         newDescription = "Video-Fields-1-Only";
      else if (newFramingType == FRAMING_TYPE_FIELD_2_ONLY)
         newDescription = "Video-Fields-2-Only";

      framingInfo->setDescription(newDescription);

      return framingInfo;
      }

} // createVideoFieldsFramingInfo

CFramingInfo* CFramingInfoList::createCadenceRepairFramingInfo(ClipSharedPtr &clip,
                                                             int newFramingType,
                                                            int newFramingIndex)
{
   if (newFramingType != FRAMING_TYPE_CADENCE_REPAIR)
      {
      return 0;   // ERROR: bad framing type, shouldn't ever happen
      }

   const CImageFormat *imageFormat = clip->getImageFormat(VIDEO_SELECT_NORMAL);
   if (imageFormat == 0)
    {
     // no image track available
     return 0;
    }

   EImageFormatType imageFormatType = imageFormat->getImageFormatType();

   if (CImageInfo::queryIsCadenceRepairSupported(imageFormatType) == false)
    {
     // cadence tool not supported
     return 0;
    }

   CFramingInfo* videoFramesFramingInfo;
   videoFramesFramingInfo = getBaseFramingInfo(FRAMING_SELECT_VIDEO);

   CFramingInfo* framingInfo = createFramingInfo(newFramingType);
   if (newFramingIndex == -1)
     addFramingInfo(framingInfo);  // Add to list
   else
     framingInfoList[newFramingIndex] = framingInfo; // Set in existing

   // Set the frame rate
   EFrameRate frameRateEnum = videoFramesFramingInfo->getFrameRateEnum();
   EFrameRate newFrameRateEnum;
   double frameRate = videoFramesFramingInfo->getFrameRate();
   double newFrameRate;


   // The frame rate of the cadence corrected material will be the
   // same as the nominal values
   newFrameRateEnum = frameRateEnum;
   newFrameRate = frameRate;
   framingInfo->setFrameRate(newFrameRateEnum, newFrameRate);

   string newDescription;
   newDescription = "Cadence Repair";
   framingInfo->setDescription(newDescription);

   return framingInfo;

} // createCadenceRepairFramingInfo

CFramingInfo*
CFramingInfoList::createCustomFramesFramingInfo(ClipSharedPtr &clip)
{
   CFramingInfo* framingInfo;

   framingInfo = createFramingInfo(FRAMING_TYPE_CUSTOM);

   addFramingInfo(framingInfo);

   return framingInfo;
}


CFramingInfo* CFramingInfoList::createFramingInfo(int newFramingType)
{
   // Find a new framing number
   int newFramingNumber = findNewFramingNumber();

   // Create new instance of CFramingInfo subtype
   CFramingInfo* framingInfo = createDerivedFramingInfo(newFramingNumber,
                                                         newFramingType);

   return framingInfo;
}

int CFramingInfoList::findNewFramingNumber() const
{
   int maxFramingNumber = -1;
   for (int framingIndex = 0;
        framingIndex < (int)framingInfoList.size();
        ++framingIndex)
      {
      CFramingInfo* framingInfo = framingInfoList[framingIndex];
      if (framingInfo != 0
          && maxFramingNumber < framingInfo->getFramingNumber())
         maxFramingNumber = framingInfo->getFramingNumber();
      }
   return maxFramingNumber + 1;
}

//////////////////////////////////////////////////////////////////////
// Framing Info List Management
//////////////////////////////////////////////////////////////////////

void CFramingInfoList::addFramingInfo(CFramingInfo *newFramingInfo)
{
   framingInfoList.push_back(newFramingInfo);
}

void CFramingInfoList::deleteFramingInfoList()
{
   if (framingInfoList.empty())
      return;

   // When the concept of a "Film-Frames"  is not applicable
   // to a Clip, the Film-Frames entry in the FramingInfoList points to
   // the same CVideoFramingInfo instance as the Video-Frames entry.
   // We need to be careful we do not attempt to delete the CVideoFramingInfo
   // twice
   if (framingInfoList[FRAMING_SELECT_VIDEO]
                                       != framingInfoList[FRAMING_SELECT_FILM])
      {
      // Only delete the Video-Frames CVideoFramingInfo if it is not shared
      // with the Film-Frames track
      delete framingInfoList[FRAMING_SELECT_VIDEO];
      }

   // Delete the rest of the entries, assuming they are all unique
   for (int i = FRAMING_SELECT_FILM; i < (int)framingInfoList.size(); ++i)
      {
      delete framingInfoList[i];
      }

   framingInfoList.clear();
}

//////////////////////////////////////////////////////////////////////
// Read/Write Framing Info List Section of Clip File
//////////////////////////////////////////////////////////////////////

int CFramingInfoList::readFramingInfoListSection(CIniFile *iniFile,
                                       const string &framingInfoListSectionName,
                                       const string &framingInfoSectionPrefix)
{
   int retVal;

   int videoFramesTrackNumber, framingNumber;
   CFramingInfo *videoFramesFramingInfo, *newFramingInfo;
   MTIostringstream ostr;
   string emptyString;

   // Read the Video-Frames Framing number
   videoFramesTrackNumber = iniFile->ReadInteger(framingInfoListSectionName,
                                                 videoFramesTrackKey,
                                                 FRAMING_NUMBER_NO_TRACK);
   if (videoFramesTrackNumber == FRAMING_NUMBER_NO_TRACK)
      {
      videoFramesFramingInfo = 0;
      addFramingInfo(videoFramesFramingInfo);
      }
   else
      {
      videoFramesFramingInfo = createDerivedFramingInfo(videoFramesTrackNumber,
                                                        FRAMING_TYPE_VIDEO);
      // Add the new Framing Info instance to Framing Info List
      addFramingInfo(videoFramesFramingInfo);

      ostr.str(emptyString);
      ostr << framingInfoSectionPrefix << videoFramesTrackNumber;
      retVal = videoFramesFramingInfo->readFramingInfoSection(iniFile,
                                                              ostr.str());
      if (retVal != 0)
         {
         // ERROR: Could not read Video-Frames Framing Info section
         delete videoFramesFramingInfo;
         return -410;
         }
      }

   // Read the Film-Frames Framing number
   framingNumber = iniFile->ReadInteger(framingInfoListSectionName,
                                        filmFramesTrackKey,
                                        FRAMING_NUMBER_NO_TRACK);
   if (framingNumber == FRAMING_NUMBER_NO_TRACK)
      {
      newFramingInfo = 0;
      addFramingInfo(newFramingInfo);
      }
   else if (framingNumber == videoFramesTrackNumber)
      {
      // Same track numbers, so Film-Frames shares Track Info with Video-Frames
      newFramingInfo = videoFramesFramingInfo;
      addFramingInfo(newFramingInfo);
      }
   else
      {
      newFramingInfo = createDerivedFramingInfo(framingNumber,
                                                FRAMING_TYPE_FILM);
      addFramingInfo(newFramingInfo);

      ostr.str(emptyString);
      ostr << framingInfoSectionPrefix << framingNumber;
      retVal = newFramingInfo->readFramingInfoSection(iniFile, ostr.str());
      if (retVal != 0)
         {
         // ERROR: Could not read Film-Frames Track Info section
         delete newFramingInfo;
         deleteFramingInfoList();
         return -411;
         }
      }

   // The Video-Fields-Frames Info does not actually exist in the file,
   // rather it is synthesized from the Video-Frames info.
   // Add three NULL pointers to this framing info list to serve as
   // placeholders for the video fields framing info that will be added
   // later
   addFramingInfo(0);    // FRAMING_SELECT_FIELDS_1_2
   addFramingInfo(0);    // FRAMING_SELECT_FIELDS_1_ONLY
   addFramingInfo(0);    // FRAMING_SELECT_FIELDS_2_ONLY
   addFramingInfo(0);    // FRAMING_SELECT_CADENCE_REPAIR

   int maxCustomFramesTracks = iniFile->ReadInteger(framingInfoListSectionName,
                                                    maxCustomFramesTracksKey,
                                                    -1);

   // Read all of the Custom-Frames Track numbers
   for (int customTrack = 1;
        customTrack <= maxCustomFramesTracks;
        ++customTrack)
      {
      ostr.str(emptyString);
      ostr << customFramesTrackKey << customTrack;
      framingNumber = iniFile->ReadInteger(framingInfoListSectionName,
                                           ostr.str(), FRAMING_NUMBER_NO_TRACK);
      if (framingNumber == FRAMING_NUMBER_NO_TRACK)
         {
         newFramingInfo = 0;
         addFramingInfo(newFramingInfo);
         }
      else
         {
         newFramingInfo = createDerivedFramingInfo(framingNumber,
                                                   FRAMING_TYPE_CUSTOM);
         addFramingInfo(newFramingInfo);

         ostr.str(emptyString);
         ostr << framingInfoSectionPrefix << framingNumber;
         retVal = newFramingInfo->readFramingInfoSection(iniFile, ostr.str());
         if (retVal != 0)
            {
            // ERROR: Could not read Custom-Frames Track Info section
            delete newFramingInfo;
            deleteFramingInfoList();
            return -412;
            }
         }
      }

   return 0;   // Success

} // readFramingInfoListSection

int CFramingInfoList::writeFramingInfoListSection(CIniFile *iniFile,
                                       const string &framingInfoListSectionName,
                                       const string &framingInfoSectionPrefix)
{
   int retVal;

   int videoFramesFramingNumber, framingNumber;
   CFramingInfo *videoFramesFramingInfo, *framingInfo;
   MTIostringstream ostr;
   string emptyString;

   // Write out Video-Frames Framing information
   videoFramesFramingInfo = getBaseFramingInfo(FRAMING_SELECT_VIDEO);
   if (videoFramesFramingInfo == 0)
      {
      // No Video-Frames Framing
      videoFramesFramingNumber = FRAMING_NUMBER_NO_TRACK;
      }
   else
      {
      // There is a Video-Frames Framing Ino, so write the Framing Info
      // section for it
      videoFramesFramingNumber = videoFramesFramingInfo->getFramingNumber();

      // Create section name like "VideoFramingInfo0"
      ostr.str(emptyString);
      ostr << framingInfoSectionPrefix << videoFramesFramingNumber;
      retVal = videoFramesFramingInfo->writeFramingInfoSection(iniFile,
                                                               ostr.str());
      if (retVal != 0)
         {
         // ERROR: Could not write Video-Frames Track Info section
         return -420;
         }
      }
   // Write the Video-Frames Track number into the VideoTrackList section
   iniFile->WriteInteger(framingInfoListSectionName, videoFramesTrackKey,
                         videoFramesFramingNumber);

   // Write out Film-Frames Track information
   framingInfo = getBaseFramingInfo(FRAMING_SELECT_FILM);
   if (framingInfo == 0)
      {
      // No Film-Frames Track
      framingNumber = FRAMING_NUMBER_NO_TRACK;
      }
   else if (framingInfo == videoFramesFramingInfo)
      {
      // The Film-Frames shares the video track with Video-Frames,
      // so do not write a separate Video Track Info section
      framingNumber = videoFramesFramingNumber;
      }
   else
      {
      // There is a unique Film-Frames track, so write the Video Track Info
      // section for it
      framingNumber = framingInfo->getFramingNumber();

      // Create section name like "VideoFramingInfo0"
      ostr.str(emptyString);
      ostr << framingInfoSectionPrefix << framingNumber;
      retVal = framingInfo->writeFramingInfoSection(iniFile, ostr.str());
      if (retVal != 0)
         {
         // ERROR: Could not write Film-Frames Track Info section
         return -421;
         }
      }
   // Write the Film-Frames Framing number into the VideoFramingInfoList section
   iniFile->WriteInteger(framingInfoListSectionName, filmFramesTrackKey,
                         framingNumber);

   // Determine the highest index of a custom frames track
   int maxCustomFrameIndex = -1;
   int framingIndex;
   for (framingIndex = FRAMING_SELECT_CUSTOM_BASE;
        framingIndex < getFramingCount();
        ++framingIndex)
      {
      if (getBaseFramingInfo(framingIndex) != 0)
         maxCustomFrameIndex = framingIndex;
      }
   iniFile->WriteInteger(framingInfoListSectionName, maxCustomFramesTracksKey,
                         maxCustomFrameIndex);

   // Write out the Custom-Frames video track information
   for (framingIndex = FRAMING_SELECT_CUSTOM_BASE;
        framingIndex <= maxCustomFrameIndex;
        ++framingIndex)
      {
      framingInfo = getBaseFramingInfo(framingIndex);
      // Skip past entries with no track info
      if (framingInfo != 0)
         {
         // There is a Custom-Frames track, so write the Video Track Info
         // section for it
         framingNumber = framingInfo->getFramingNumber();

         // Create section name like "VideoFramingInfo1"
         ostr.str(emptyString);
         ostr << framingInfoSectionPrefix << framingNumber;
         retVal = framingInfo->writeFramingInfoSection(iniFile, ostr.str());
         if (retVal != 0)
            {
            // ERROR: Could not write Custom-Frames Track Info section
            return -422;
            }
         // Write the Custom-Frames Track number into the VideoTrackList section
         ostr.str(emptyString);
         ostr << customFramesTrackKey << framingIndex;
         iniFile->WriteInteger(framingInfoListSectionName, ostr.str(),
                               framingNumber);
         }
      }

   return 0;

} // writeFramingInfoListSection

//////////////////////////////////////////////////////////////////////
// VERSION CLIP HACK - Read/Write Readonly Track Number
//////////////////////////////////////////////////////////////////////

int CFramingInfoList::getMainFrameListIndex()
{
   return 0;
}

void CFramingInfoList::setAndWriteMainFrameListIndex(CIniFile *iniFile,
                                                     int newMainFrameListIndex)
{
   // Do nothing - not supported by base class
}

int CFramingInfoList::getLockedFrameListIndex()
{
   return 0;
}

void CFramingInfoList::setAndWriteLockedFrameListIndex(CIniFile *iniFile,
                                                   int newLockedFrameListIndex)
{
   // Do nothing - not supported by base class
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                      #########################################
// ###   CFrameList Class    ########################################
// ####                      #########################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFrameList::CFrameList(CFramingInfo *newFramingInfo, CTrack *newParentTrack)
: framingInfo(newFramingInfo),
  parentTrack(newParentTrack),
  frameListAvailable(false),
  frame0Timecode(0), inTimecode(0), outTimecode(0),
  inFrameIndex(-1), outFrameIndex(-1),
  maxTotalFieldCount(-1),
  mediaAllocatedFrameCount(-1), mediaAllocatedFrontier(-1)
{

}

CFrameList::CFrameList(CFramingInfo *newFramingInfo, CTrack *newParentTrack,
                         MTI_UINT32 reserveFrameCount)
: framingInfo(newFramingInfo),
  parentTrack(newParentTrack),
  frameListAvailable(false),
  frame0Timecode(0), inTimecode(0), outTimecode(0),
  inFrameIndex(-1), outFrameIndex(-1),
  maxTotalFieldCount(-1),
  mediaAllocatedFrameCount(-1), mediaAllocatedFrontier(-1)
{
   frameListReserve(reserveFrameCount);
}

CFrameList::~CFrameList()
{
   deleteFrameList();
}

bool CFrameList::isFrameListAvailable() const
{
   return frameListAvailable;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CFrameList::assignMediaStorage(CMediaAllocator &mediaAllocator,
                                   int startFrame, int frameCount)
{
   CMediaLocation mediaLocation;

   int totalFrameCount = getTotalFrameCount();

   if (startFrame < 0 || startFrame >= totalFrameCount)
      return CLIP_ERROR_INVALID_FRAME_NUMBER;

   // Number of frames from startFrame argument to end of frame list
   int framesToEnd = totalFrameCount - startFrame;

   // Adjust caller's frame count 1) if it is -1, which indicates assign to
   // end of frame list or 2) if it is larger than the number of frames
   // between the caller's start frame and the end  of the frame list.
   if (frameCount < 0 || frameCount > framesToEnd)
      frameCount = framesToEnd;

   int endFrame = startFrame + frameCount;

   for (int i = startFrame; i < endFrame; ++i)
      {
      CFrame* frame = getBaseFrame(i);
      int fieldCount = frame->getTotalFieldCount();

      for (int j = 0; j < fieldCount; ++j)
         {
         CField *field = frame->getBaseField(j);

         if (field->doesMediaStorageExist())
            return CLIP_ERROR_MEDIA_STORAGE_ALREADY_ASSIGNED;

         mediaAllocator.getNextFieldMediaAllocation(mediaLocation);
         field->setMediaLocation(mediaLocation);
         bool mediaStorageExists = !mediaLocation.isMarked();
         field->setMediaStorageExists(mediaStorageExists);
         // Assume the media has been written if it exists and is either
         // imported (e.g. image file) or initialized on allocation (raw audio)
         field->setMediaWritten(mediaStorageExists
                                && (mediaLocation.isImported()
                                    || mediaLocation.isInitialized()));
         }
      }

   return 0;

} // assignMediaStorage

void CFrameList::assignVirtualMedia(MTI_UINT32 newDataSize)
{
   int frameCount = getTotalFrameCount();

/*
   for (int i = 0; i < frameCount; ++i)
      {
      CFrame* frame = getBaseFrame(i);
      int fieldCount = frame->getTotalFieldCount();

      for (int j = 0; j < fieldCount; ++j)
         {
         CField *field = frame->getBaseField(j);
         field->assignVirtualMedia(newDataSize);
         }
      }
*/
   assignVirtualMedia(0, frameCount, newDataSize);

} // assignVirtualMedia

void CFrameList::assignVirtualMedia(int startFrame, int frameCount,
                                    MTI_UINT32 newDataSize)
{
   for (int i = startFrame; i < frameCount; ++i)
      {
      CFrame* frame = getBaseFrame(i);
      int fieldCount = frame->getTotalFieldCount();

      for (int j = 0; j < fieldCount; ++j)
         {
         CField *field = frame->getBaseField(j);
         field->assignVirtualMedia(newDataSize);
         }
      }

} // assignVirtualMedia


//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    PeekAtMediaLocation
//
// Description: This function gets the name of the file that holds the
//              media for a given field in the frame list.  The function
//              also provides the offset (in bytes) to the start of the
//              field media data in the file.  This function works for
//              all audio and video media storage types.
//
// Arguments:   int frameIndex    Index of frame within frame-list
//              int fieldIndex    Index of field within a frame
//              string &filename  Reference to caller's string to place
//                                name of file that holds the media
//              MTI_INT64& offset Reference to caller's MTI_INT64 to place
//                                the offset into the media file
//
// Returns:     0 if succcess, non-zero if failure (see err_clip.h)
//
//------------------------------------------------------------------------
int CFrameList::PeekAtMediaLocation(int frameIndex, int fieldIndex,
                                    string &filename, MTI_INT64 &offset)
{
   int retVal;
   EClipVersion clipVer = getClipVer();

   filename.erase();
   offset = 0;

   if (clipVer == CLIP_VERSION_3)
      {
      if (!isFrameIndexValid(frameIndex))
         return CLIP_ERROR_INVALID_FRAME_NUMBER;

      CFrame *frame = getBaseFrame(frameIndex);
      if (frame == 0)
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

      if (fieldIndex < 0 || fieldIndex >= frame->getTotalFieldCount())
         return CLIP_ERROR_INVALID_FIELD_INDEX;

      CField *field = frame->getBaseField(fieldIndex);
      if (field == 0)
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

      CMediaLocation mediaLocation = field->getMediaLocation();

      retVal = mediaLocation.Peek(filename, offset);
      if (retVal != 0)
         return retVal;
      }
   else if (clipVer == CLIP_VERSION_5L)
      {
      retVal = parentTrack->GetTrackHandle().PeekAtMediaLocation(frameIndex,
                                                 fieldIndex, filename, offset);
      if (retVal != 0)
         return retVal;
      }
   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   return 0;
}


//------------------------------------------------------------------------
//
// Function:    isLocked
//
// Description: Answers the question: is this frame list locked?
//
// Arguments:  None
//
// Returns:    false if not lockable or not locked
//
//------------------------------------------------------------------------
bool CFrameList::isLocked()
{
   return false;    // Only video can be locked, so CVideoFrameList overrides
}

//////////////////////////////////////////////////////////////////////
// Read/Write Media Data Functions
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    readMediaAllFields
//
// Description: Read the media data from the media storage for the
//              specified frame in the clip.  Media data is read
//              into caller's buffers for all of the frame's visible
//              and invisible fields.
//
//              Media data is read in the same order as the fields
//              appear in the frame.  The read order is not optimized
//              for the media location.
//
// Arguments:  int frameNumber   Number of frame to read.  First frame is 0.
//             MTI_UINT8 *buffer[]   Array of pointers to buffers, one for
//                                   each field in frame
//
// Returns:    0 for success, non-zero indicates an error
//
//------------------------------------------------------------------------
int CFrameList::readMediaAllFields(int frameNumber, MTI_UINT8 *buffer[])
{
   int retVal;
   EClipVersion clipVer = getClipVer();

   if (clipVer == CLIP_VERSION_3)
      {
      if (!isFrameIndexValid(frameNumber))
         {
         initMediaBuffer(buffer);      // fill caller's buffers with something
         return CLIP_ERROR_INVALID_FRAME_NUMBER;
         }

      // Get a pointer to the frame of interest
      CFrame* frame = getBaseFrame(frameNumber);

      // If NULL frame pointer was returned, then the frame number is invalid
      if(frame == 0)
         return -1;     // Return error

      // Read the frame's media data and return success/error status
      retVal = frame->readMediaAllFields(buffer);
      if (retVal != 0)
         return retVal;
      }

   else if (clipVer == CLIP_VERSION_5L)
      {
      retVal = parentTrack->GetTrackHandle().ReadMedia(frameNumber, buffer);
      if (retVal != 0)
         return retVal;
      }
   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    readMediaAllFieldsOptimized
//
// Description: Read the media data from the media storage for the
//              specified frame in the clip.  Media data is read
//              into caller's buffers for all of the frame's visible
//              and invisible fields.
//
//              Media read is optimized by reading from the fields' media
//              storage in ascending media location order and by copying
//              buffers instead of re-reading for fields that share the
//              same media.
//
// Arguments:  int frameNumber   Number of frame to read.  First frame is 0.
//             MTI_UINT8 *buffer[]   Array of pointers to buffers, one for
//                                   each field in frame
//
// Returns:    0 for success, non-zero indicates an error
//
//------------------------------------------------------------------------
int CFrameList::readMediaAllFieldsOptimized(int frameNumber, MTI_UINT8 *buffer[])
{
   int retVal;

   retVal = readMediaAllFieldsOptimizedMT(frameNumber, buffer, 0);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CFrameList::readMediaAllFieldsOptimizedMT(int frameNumber,
                                              MTI_UINT8 *buffer[],
                                              int fileHandleIndex)
{
   int retVal;
   EClipVersion clipVer = getClipVer();

   if (clipVer == CLIP_VERSION_3)
      {
      if (!isFrameIndexValid(frameNumber))
         {
         initMediaBuffer(buffer);      // fill caller's buffers with something
         return CLIP_ERROR_INVALID_FRAME_NUMBER;
         }

      // Get a pointer to the frame of interest
      CFrame* frame = getBaseFrame(frameNumber);

      // If NULL frame pointer was returned, then the frame number is invalid
      if(frame == 0)
         return -1;     // Return error

      // Read the frame's media data and return success/error status
      retVal = frame->readMediaAllFieldsOptimizedMT(buffer, fileHandleIndex);
      if (retVal != 0)
         return retVal;
      }

   else if (clipVer == CLIP_VERSION_5L)
      {
      retVal = parentTrack->GetTrackHandle().ReadMediaMT(frameNumber, buffer,
                                                         fileHandleIndex);
      if (retVal != 0)
         return retVal;
      }
   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    readMediaVisibleFields
//
// Description: Read the media data from the media storage for the
//              specified frame in the clip.  Media data is read
//              into caller's buffers for the frame's visible  fields only.
//
//              Media data is read in the same order as the fields
//              appear in the frame.  The read order is not optimized
//              for the media location.
//
// Arguments:  int frameNumber   Number of frame to read.  First frame is 0.
//             MTI_UINT8 *buffer[]   Array of pointers to buffers, one for each
//                               field in frame
//
// Returns:    0 for success, non-zero indicates an error
//
//------------------------------------------------------------------------
int CFrameList::readMediaVisibleFields(int frameNumber, MTI_UINT8 *buffer[])
{
   int retVal;
   EClipVersion clipVer = getClipVer();

   if (clipVer == CLIP_VERSION_3)
      {
      if (!isFrameIndexValid(frameNumber))
         {
         initMediaBuffer(buffer);      // fill caller's buffers with something
         return CLIP_ERROR_INVALID_FRAME_NUMBER;
         }

      // Get a pointer to the frame of interest
      CFrame* frame = getBaseFrame(frameNumber);

      // If NULL frame pointer was returned, then the frame number is invalid
      if(frame == 0)
         return -1;     // Return error

      // Read the frame's media data and return success/error status
      retVal = frame->readMediaVisibleFields(buffer);
      if (retVal != 0)
         return retVal;
      }

   else if (clipVer == CLIP_VERSION_5L)
      {
      retVal = parentTrack->GetTrackHandle().ReadMedia(frameNumber, buffer);
      if (retVal != 0)
         return retVal;
      }
   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    readMediaVisibleFieldsOptimized
//
// Description: Read the media data from the media storage for the
//              specified frame in the clip.  Media data is read
//              into caller's buffers for the frame's visible  fields only.
//
//              Media data is read in the same order as the fields
//              appear in the frame.  The read order is not optimized
//              for the media location.
//
// Arguments:  int frameNumber   Number of frame to read.  First frame is 0.
//             MTI_UINT8 *buffer[]   Array of pointers to buffers, one for each
//                               field in frame
//
// Returns:    0 for success, non-zero indicates an error
//
//------------------------------------------------------------------------
int CFrameList::readMediaVisibleFieldsOptimized(int frameNumber, MTI_UINT8 *buffer[])
{
   int retVal;
   EClipVersion clipVer = getClipVer();

   if (clipVer == CLIP_VERSION_3)
      {
      if (!isFrameIndexValid(frameNumber))
         {
         initMediaBuffer(buffer);      // fill caller's buffers with something
         return CLIP_ERROR_INVALID_FRAME_NUMBER;
         }

      // Get a pointer to the frame of interest
      CFrame* frame = getBaseFrame(frameNumber);

      // If NULL frame pointer was returned, then the frame number is invalid
      if(frame == 0)
         return -1;     // Return error

      // Read the frame's media data and return success/error status
      retVal = frame->readMediaVisibleFieldsOptimized(buffer);
      if (retVal != 0)
         return retVal;
      }

   else if (clipVer == CLIP_VERSION_5L)
      {
      retVal = parentTrack->GetTrackHandle().ReadMedia(frameNumber, buffer);
      if (retVal != 0)
         return retVal;
      }
   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    writeMediaAllFields
//
// Description: Write the media data to the media storage for the
//              specified frame in the clip.  Media data is written
//              from caller's buffers for all of the frame's visible
//              and invisible fields.
//
//              Media data is written in the same order as the fields
//              appear in the frame.  The write order is not optimized
//              for the media location.
//
// Arguments:  int frameNumber   Number of frame to read.  First frame is 0.
//             MTI_UINT8 *buffer[]   Array of pointers to buffers, one for each
//                               field in frame
//
// Returns:    0 for success, non-zero indicates an error
//
//------------------------------------------------------------------------
int CFrameList::writeMediaAllFields(int frameNumber, MTI_UINT8 *buffer[])
{
   int retVal;
   EClipVersion clipVer = getClipVer();

   if (clipVer == CLIP_VERSION_3)
      {
      if (!isFrameIndexValid(frameNumber))
         return CLIP_ERROR_INVALID_FRAME_NUMBER;

      // Get a pointer to the frame of interest
      CFrame* frame = getBaseFrame(frameNumber);

      // If NULL frame pointer was returned, then the frame number is invalid
      if(frame == 0)
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

      // make sure newly created DPX files will get the right timecode
		doDPXHeaderTimecodeHack(frame);

		// Obviously, do this BEFORE assigning dirty media!
		string oldImageFilePath = getImageFilePath(frame);

      // Allocate storage for the frame if necessary (version clip hack)
      DirtyMediaAssignmentUndoInfo undoInfo;
		retVal = assignDirtyMediaStorage(frameNumber, undoInfo);
      if (retVal == CLIP_ERROR_FRAME_LIST_IS_LOCKED)
         {
         // This one's immediately fatal
         return retVal;
         }
      if (retVal != CLIP_ERROR_MEDIA_ALLOCATION_FAILED)
         {
			string newImageFilePath = getImageFilePath(frame);
         if ( (!oldImageFilePath.empty()) &&
              (oldImageFilePath != newImageFilePath) )
				{
//          retVal = writeMediaWithDPXHeaderCopyHack(frameNumber, buffer, oldImageFilePath);
				retVal = frame->writeMediaWithDPXHeaderCopyHack(buffer, oldImageFilePath);
				}
         else
				{
            // Write the frame's media data and return success/error status
            retVal = frame->writeMediaAllFields(buffer);
            }
         }
      if (retVal != 0)
         {
         unassignDirtyMediaStorage(frameNumber, undoInfo);
         return retVal;
         }
      }

   else if (clipVer == CLIP_VERSION_5L)
      {
      retVal = parentTrack->GetTrackHandle().WriteMedia(frameNumber, buffer);
      if (retVal != 0)
         return retVal;
      }
   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    writeMediaAllFieldsOptimized
//
// Description: Write the media data to the media storage for the
//              specified frame in the clip.  Media data is written
//              from caller's buffers for all of the frame's visible
//              and invisible fields.
//
//              Media write is optimized by writing to the fields' media
//              storage in ascending media location order and by avoiding
//              duplicate writes for fields that share the same media.
//
// Arguments:  int frameNumber   Number of frame to read.  First frame is 0.
//             MTI_UINT8 *buffer[]   Array of pointers to buffers, one for each
//                               field in frame
//
// Returns:    0 for success, non-zero indicates an error
//
//------------------------------------------------------------------------
int CFrameList::writeMediaAllFieldsOptimized(int frameNumber, MTI_UINT8 *buffer[])
{
   int retVal;
   EClipVersion clipVer = getClipVer();

   if (clipVer == CLIP_VERSION_3)
      {
      if (!isFrameIndexValid(frameNumber))
         return CLIP_ERROR_INVALID_FRAME_NUMBER;

      // Get a pointer to the frame of interest
      CFrame* frame = getBaseFrame(frameNumber);

      // If NULL frame pointer was returned, then the frame number is invalid
      if(frame == 0)
         return -1;     // Return error

		// make sure newly created DPX files will get the right timecode
      doDPXHeaderTimecodeHack(frame);

		// Obviously, do this BEFORE assigning dirty media!
		string oldImageFilePath = getImageFilePath(frame);

      // Allocate storage for the frame if necessary (version clip hack)
      DirtyMediaAssignmentUndoInfo undoInfo;
      retVal = assignDirtyMediaStorage(frameNumber, undoInfo);
      if (retVal == CLIP_ERROR_FRAME_LIST_IS_LOCKED)
         {
         // This one's immediately fatal
         return retVal;
         }
      if (retVal != CLIP_ERROR_MEDIA_ALLOCATION_FAILED)
         {
			string newImageFilePath = getImageFilePath(frame);
         if ( (!oldImageFilePath.empty()) &&
              (oldImageFilePath != newImageFilePath) )
            {
//          retVal = writeMediaWithDPXHeaderCopyHack(frameNumber, buffer, oldImageFilePath);
				retVal = frame->writeMediaWithDPXHeaderCopyHack(buffer, oldImageFilePath);
				}
         else
				{
            // Write the frame's media data and return success/error status
            retVal = frame->writeMediaAllFieldsOptimized(buffer);
            }
         }
      if (retVal != 0)
         {
         unassignDirtyMediaStorage(frameNumber, undoInfo);
         return retVal;
         }
      }

   else if (clipVer == CLIP_VERSION_5L)
      {
      retVal = parentTrack->GetTrackHandle().WriteMedia(frameNumber, buffer);
      if (retVal != 0)
         return retVal;
      }
   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    writeMediaFilmFramesOptimized
//
// Description: Write the media data to the media storage for the
//              specified frame in the clip.  Media data is written
//              for both visible and invisible fields given
//              the caller's buffers for visible fields only.
//              Each invisible field is written from the buffer that
//              corresponds to the invisible field's visible sibling.
//
//              Media write is optimized by writing to the fields' media
//              storage in ascending media location order and by avoiding
//              duplicate writes for fields that share the same media.
//
// Arguments:  int frameNumber   Number of frame to read.  First frame is 0.
//             MTI_UINT8 *buffer[]   Array of pointers to buffers, one for each
//                               field in frame
//
// Returns:    0 for success, non-zero indicates an error
//
//------------------------------------------------------------------------
int CFrameList::writeMediaFilmFramesOptimized(int frameNumber, MTI_UINT8 *buffer[])
{
   int retVal;
   EClipVersion clipVer = getClipVer();

   if (clipVer == CLIP_VERSION_3)
      {
      if (!isFrameIndexValid(frameNumber))
         return CLIP_ERROR_INVALID_FRAME_NUMBER;

      // Get a pointer to the frame of interest
      CFrame* frame = getBaseFrame(frameNumber);

      // If NULL frame pointer was returned, then the frame number is invalid
      if(frame == 0)
         return -1;     // Return error

		// make sure newly created DPX files will get the right timecode
      doDPXHeaderTimecodeHack(frame);

		// Obviously, do this BEFORE assigning dirty media!
		string oldImageFilePath = getImageFilePath(frame);

      // Allocate storage for the frame if necessary (version clip hack)
      DirtyMediaAssignmentUndoInfo undoInfo;
      retVal = assignDirtyMediaStorage(frameNumber, undoInfo);
      if (retVal == CLIP_ERROR_FRAME_LIST_IS_LOCKED)
         {
         // This one's immediately fatal
         return retVal;
         }
      if (retVal != CLIP_ERROR_MEDIA_ALLOCATION_FAILED)
         {
			string newImageFilePath = getImageFilePath(frame);
			if ( (!oldImageFilePath.empty()) &&
              (oldImageFilePath != newImageFilePath) )
            {
//          retVal = writeMediaWithDPXHeaderCopyHack(frameNumber, buffer, oldImageFilePath);
				retVal = frame->writeMediaWithDPXHeaderCopyHack(buffer, oldImageFilePath);
				}
         else
            {
            // Write the frame's media data and return success/error status
            retVal = frame->writeMediaFilmFramesOptimized(buffer);
            }
         }
      if (retVal != 0)
         {
         unassignDirtyMediaStorage(frameNumber, undoInfo);
         return retVal;
         }
      }

   else if (clipVer == CLIP_VERSION_5L)
      {
      retVal = parentTrack->GetTrackHandle().WriteMedia(frameNumber, buffer);
      if (retVal != 0)
         return retVal;
      }
   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    writeMediaVisibleFields
//
// Description: Write the media data to the media storage for the
//              visible fields only of a specified frame in the clip.
//              Media data is written from caller's buffers for all of the
//              frame's visible fields.
//
//              Media data is written in the same order as the fields
//              appear in the frame.  The write order is not optimized
//              for the media location.
//
//           ////////////////////////////////////////////////////////////
//           // WARNING!!! THE CALLER MUST MAKE SURE THAT SOMETHING    //
//           // SENSIBLE GETS WRITTEN TO THE INVISIBLE FIELDS BECAUSE  //
//           // ON A FIRST WRITE TO THE FRAME IN A VERSION CLIP, WE    //
//           // DON'T COPY ANY MEDIA TO THE DIRTY STORAGE, so after    //
//           // this call the invisible fields will contain garbage.   //
//           // THIS METHOD SHOULD BE CHANGED TO PRESERVE THE CONTENTS //
//           // OF THE INVISIBLE FRAMES!                               //
//           ////////////////////////////////////////////////////////////
//
//
// Arguments:  int frameNumber   Number of frame to read.  First frame is 0.
//             MTI_UINT8 *buffer[]   Array of pointers to buffers, one for each
//                               visible field in frame
//
// Returns:    0 for success, non-zero indicates an error
//
//------------------------------------------------------------------------
int CFrameList::writeMediaVisibleFields(int frameNumber, MTI_UINT8 *buffer[])
{
   int retVal;
   EClipVersion clipVer = getClipVer();

   if (clipVer == CLIP_VERSION_3)
      {
      if (!isFrameIndexValid(frameNumber))
         return CLIP_ERROR_INVALID_FRAME_NUMBER;

      // Get a pointer to the frame of interest
      CFrame* frame = getBaseFrame(frameNumber);

      // If NULL frame pointer was returned, then the frame number is invalid
      if(frame == 0)
         return -1;     // Return error

      // make sure newly created DPX files will get the right timecode
      doDPXHeaderTimecodeHack(frame);

		// Obviously, do this BEFORE assigning dirty media!
		string oldImageFilePath = getImageFilePath(frame);

      // Allocate storage for the frame if necessary (version clip hack)
      DirtyMediaAssignmentUndoInfo undoInfo;
      retVal = assignDirtyMediaStorage(frameNumber, undoInfo);
      if (retVal == CLIP_ERROR_FRAME_LIST_IS_LOCKED)
         {
         // This one's immediately fatal
         return retVal;
         }
      if (retVal != CLIP_ERROR_MEDIA_ALLOCATION_FAILED)
         {
			string newImageFilePath = getImageFilePath(frame);
        if ( (!oldImageFilePath.empty()) &&
              (oldImageFilePath != newImageFilePath) )
            {
//          retVal = writeMediaWithDPXHeaderCopyHack(frameNumber, buffer, oldImageFilePath);
				retVal = frame->writeMediaWithDPXHeaderCopyHack(buffer, oldImageFilePath);
            }
         else
            {
            // Write the frame's media data and return success/error status
            retVal = frame->writeMediaVisibleFields(buffer);
            }
         }
      if (retVal != 0)
         {
         unassignDirtyMediaStorage(frameNumber, undoInfo);
         return retVal;
         }
      }

   else if (clipVer == CLIP_VERSION_5L)
      {
      retVal = parentTrack->GetTrackHandle().WriteMedia(frameNumber, buffer);
      if (retVal != 0)
         return retVal;
      }
   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    writeMediaVisibleFieldsOptimized
//
// Description: Write the media data to the media storage for the
//              visible fields of the specified frame in the clip.
//              Media data is written from caller's buffers for all of the
//              frame's visible fields.
//
//              Media write is optimized by writing to the fields' media
//              storage in ascending media location order and by avoiding
//              duplicate writes for fields that share the same media.
//
//           ////////////////////////////////////////////////////////////
//           // WARNING!!! THE CALLER MUST MAKE SURE THAT SOMETHING    //
//           // SENSIBLE GETS WRITTEN TO THE INVISIBLE FIELDS BECAUSE  //
//           // ON A FIRST WRITE TO THE FRAME IN A VERSION CLIP, WE    //
//           // DON'T COPY ANY MEDIA TO THE DIRTY STORAGE, so after    //
//           // this call the invisible fields will contain garbage.   //
//           // THIS METHOD SHOULD BE CHANGED TO PRESERVE THE CONTENTS //
//           // OF THE INVISIBLE FRAMES!                               //
//           ////////////////////////////////////////////////////////////
//
// Arguments:  int frameNumber   Number of frame to read.  First frame is 0.
//             MTI_UINT8 *buffer[]   Array of pointers to buffers, one for each
//                               visible field in frame
//
// Returns:    0 for success, non-zero indicates an error
//
//------------------------------------------------------------------------
int CFrameList::writeMediaVisibleFieldsOptimized(int frameNumber, MTI_UINT8 *buffer[])
{
   int retVal;
   EClipVersion clipVer = getClipVer();

   if (clipVer == CLIP_VERSION_3)
      {
      if (!isFrameIndexValid(frameNumber))
         return CLIP_ERROR_INVALID_FRAME_NUMBER;

      // Get a pointer to the frame of interest
      CFrame* frame = getBaseFrame(frameNumber);

      // If NULL frame pointer was returned, then the frame number is invalid
      if(frame == 0)
         return -1;     // Return error

      // make sure newly created DPX files will get the right timecode
      doDPXHeaderTimecodeHack(frame);

		// Obviously, do this BEFORE assigning dirty media!
		string oldImageFilePath = getImageFilePath(frame);

      // Allocate storage for the frame if necessary (version clip hack)
      DirtyMediaAssignmentUndoInfo undoInfo;
      retVal = assignDirtyMediaStorage(frameNumber, undoInfo);
      if (retVal == CLIP_ERROR_FRAME_LIST_IS_LOCKED)
         {
         // This one's immediately fatal
         return retVal;
         }
      if (retVal != CLIP_ERROR_MEDIA_ALLOCATION_FAILED)
         {
			string newImageFilePath = getImageFilePath(frame);
			if ( (!oldImageFilePath.empty()) &&
              (oldImageFilePath != newImageFilePath) )
            {
//          retVal = writeMediaWithDPXHeaderCopyHack(frameNumber, buffer, oldImageFilePath);
				retVal = frame->writeMediaWithDPXHeaderCopyHack(buffer, oldImageFilePath);
            }
         else
            {
            // Write the frame's media data and return success/error status
            retVal = frame->writeMediaVisibleFieldsOptimized(buffer);
            }
         }
      if (retVal != 0)
         {
         unassignDirtyMediaStorage(frameNumber, undoInfo);
         return retVal;
         }
      }

   else if (clipVer == CLIP_VERSION_5L)
      {
      retVal = parentTrack->GetTrackHandle().WriteMedia(frameNumber, buffer);
      if (retVal != 0)
         return retVal;
      }
   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   return 0;
}

//** Dave's DPX Header Copy Hack
//------------------------------------------------------------------------
//
// Function:    writeMediaWithDPXHeaderCopyHack
//
// Description: [HACK] Write a frame's media data where there is only
//              one field pewr frame and we enable Dave's DPX Header
//              Copy Hack in case the underlying media consists of
//              DPX files.
//
// Arguments    MTI_UINT8 *buffer    Pointer to field buffers (only 1 field)!
//              string     srcFile   Path of the file from which to copy
//                                   the DPX header
//
// Returns:     0 if success, non-zero if an error.
//
//------------------------------------------------------------------------
int CFrameList::writeMediaWithDPXHeaderCopyHack(
                                            int frameNumber,
                                            MTI_UINT8 *buffer[],
                                            const string &sourceImageFilePath)
{
   int retVal;
   EClipVersion clipVer = getClipVer();

	if (clipVer == CLIP_VERSION_3)
		{
		if (!isFrameIndexValid(frameNumber))
         return CLIP_ERROR_INVALID_FRAME_NUMBER;

      // Get a pointer to the frame of interest
      CFrame* frame = getBaseFrame(frameNumber);

      // If NULL frame pointer was returned, then the frame number is invalid
      if(frame == 0)
         return -1;     // Return error

      // make sure newly created DPX files will get the right timecode
      doDPXHeaderTimecodeHack(frame);

		// Obviously, do this BEFORE assigning dirty media!
		string sourceImageFilePath = getImageFilePath(frame);

      // Allocate storage for the frame if necessary (version clip hack)
      DirtyMediaAssignmentUndoInfo undoInfo;
      retVal = assignDirtyMediaStorage(frameNumber, undoInfo);
      if (retVal == CLIP_ERROR_FRAME_LIST_IS_LOCKED)
         {
         // This one's immediately fatal
         return retVal;
         }
      if (retVal != CLIP_ERROR_MEDIA_ALLOCATION_FAILED)
         {
         // Write the frame's media data and return success/error status
			retVal = frame->writeMediaWithDPXHeaderCopyHack(buffer, sourceImageFilePath);
         }
      if (retVal != 0)
         {
         unassignDirtyMediaStorage(frameNumber, undoInfo);
         return retVal;
         }
      }

   else if (clipVer == CLIP_VERSION_5L)
      {
      // No DPX header hack implemented
      retVal = parentTrack->GetTrackHandle().WriteMedia(frameNumber, buffer);
      if (retVal != 0)
         return retVal;
      }
   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   return 0;
}


//** clip2clip hack
int CFrameList::moveMediaForOneField(CField *sourceField,
                                     int destFrameIndex,
                                     int destFieldIndex)
{
   // Virtual method needs to be overridden by derived classes
   return CLIP_ERROR_MEDIA_CANNOT_BE_TRANSFERRED;
}
int CFrameList::copyMediaForOneField(CField *sourceField,
                                     int destFrameIndex,
                                     int destFieldIndex)
{
   // Virtual method needs to be overridden by derived classes
   return CLIP_ERROR_MEDIA_CANNOT_BE_TRANSFERRED;
}

int CFrameList::changeFieldMediaLocation(
                             const CMediaLocation &oldMediaLocation,
                             const CMediaLocation &newMediaLocation)
{
   TRACE_3(errout << "(((( changeFieldMediaLocation( FL="
                  << std::setbase(16) << std::setw(8) << std::setfill('0')
                  << ((int) this) << ", old="
                  << std::setbase(16) << std::setw(10) << std::setfill('0')
                  << oldMediaLocation.getDataIndex() << ", new="
                  << std::setbase(16) << std::setw(10) << std::setfill('0')
                  << newMediaLocation.getDataIndex() << " )" );

   // Absolutely HIDEOUS brute force method to scan each and every stinkin'
   // field referenced by this framelist and change the field's media
   // location to 'newMediaLocation' if its current media location matches
   // 'oldMediaLocation'

   int frameCount = getTotalFrameCount();
   for (int frameIndex = 0; frameIndex < frameCount; ++frameIndex)
      {
      CFrame *frame = getBaseFrame(frameIndex);
      assert(frame != NULL);

      int fieldCount = frame->getTotalFieldCount();
      for (int fieldIndex = 0; fieldIndex < fieldCount; ++fieldIndex)
         {
         CField *field = frame->getBaseField(fieldIndex);
         assert(field != NULL);

         const CMediaLocation mediaLocation = field->getMediaLocation();
         if (mediaLocation == oldMediaLocation)
            {
            TRACE_3(errout << "(((( FOUND IT --  Frame=" << frameIndex
                           << ", Field=" << fieldIndex << " ( "
                           << std::setw(8) << std::setfill('0') << std::setbase(16)
                           << ((int) frame) << ","
                           << std::setw(8) << std::setfill('0') << std::setbase(16)
                           << ((int) field) << " )");

            field->setMediaLocation(newMediaLocation);
            }
         else if (mediaLocation.getDataIndex() == oldMediaLocation.getDataIndex())
            {
            TRACE_3(errout << "(((( WTF -- media loc didn't match, but indexes do");
            }

         } // for each field in the frame

      } // for each frame in the frame list
   return 0;
}
//**//

//////////////////////////////////////////////////////////////////////
// Accessor Functions
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    doesMediaStorageExist
//
// Description: Determines if media storage has been allocated for
//              frame specified by the caller.  Media storage must exist
//              for all fields in the frame for this function to return true.
//
// Arguments:   int frameNumber  Number of the frame to get.  First frame is 0
//
// Returns:     Returns true if media storage exists for all fields in
//              the specified frame.  Returns false if one or more fields
//              do not have allocated media storage or if the fieldNumber
//              argument is out-of-range for the frame list
//
//------------------------------------------------------------------------
bool CFrameList::doesMediaStorageExist(int frameNumber)
{
   EClipVersion clipVer = getClipVer();

   if (clipVer == CLIP_VERSION_3)
      {
      CFrame *frame = getBaseFrame(frameNumber);
      if (frame == 0)
         return false;

      return frame->doesMediaStorageExist();
      }
   else if (clipVer == CLIP_VERSION_5L)
      {
      C5MediaTrackHandle trackHandle = parentTrack->GetTrackHandle();
      const CMediaFormat *mediaFormat = trackHandle.GetMediaFormat();
      if (mediaFormat == 0)
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

      int fieldCount = mediaFormat->getFieldCount();
      int fieldNumber = fieldCount * frameNumber;
      E5MediaStatus mediaStatus = trackHandle.GetMediaStatus(fieldNumber);
      if (mediaStatus == CLIP5_MEDIA_STATUS_ALLOCATED ||
          mediaStatus == CLIP5_MEDIA_STATUS_WRITTEN)
         {
         if (fieldCount > 1)
            {
            // Check the other field
            mediaStatus = trackHandle.GetMediaStatus(fieldNumber + 1);
            if (mediaStatus == CLIP5_MEDIA_STATUS_ALLOCATED ||
                mediaStatus == CLIP5_MEDIA_STATUS_WRITTEN)
               return true;
            else
               return false;
            }
         else
            return true;
         }
      else
         return false;
      }
   else
      {
      return false;
      }

} // doesMediaStorageExist

//------------------------------------------------------------------------
//
// Function:    isMediaWritten
//
// Description: Determines if media has been written for the frame
//              specified by the caller.  Media storage must exist
//              for all fields in the frame for this function to return true.
//
// Arguments:   int frameNumber  Number of the frame to get.  First frame is 0
//
// Returns:     Returns true if media storage exists and has been written
//              for all fields in the specified frame.  Returns false if one
//              or more fields do not have allocated media storage, if a field
//              has not been written or if the frameNumber argument is
//              out-of-range for the frame list
//
//------------------------------------------------------------------------
bool CFrameList::isMediaWritten(int frameNumber)
{
   EClipVersion clipVer = getClipVer();

   if (clipVer == CLIP_VERSION_3)
      {
      CFrame *frame = getBaseFrame(frameNumber);
      if (frame == 0)
         return false;

      return frame->isMediaWritten();
      }
   else if (clipVer == CLIP_VERSION_5L)
      {
      C5MediaTrackHandle trackHandle = parentTrack->GetTrackHandle();
      const CMediaFormat *mediaFormat = trackHandle.GetMediaFormat();
      if (mediaFormat == 0)
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

      int fieldCount = mediaFormat->getFieldCount();
      int fieldNumber = fieldCount * frameNumber;
      E5MediaStatus mediaStatus = trackHandle.GetMediaStatus(fieldNumber);
      if (mediaStatus == CLIP5_MEDIA_STATUS_WRITTEN)
         {
         if (fieldCount > 1)
            {
            // Check the other field
            mediaStatus = trackHandle.GetMediaStatus(fieldNumber + 1);
            if (mediaStatus == CLIP5_MEDIA_STATUS_WRITTEN)
               return true;
            else
               return false;
            }
         else
            return true;
         }
      else
         return false;
      }
   else
      {
      return false;
      }
}

//------------------------------------------------------------------------
//
// Function:    getBaseFrame
//
// Description: Get a pointer to a particular frame in a clip given the
//              frame number.
//
// Arguments:   int frameNumber  Number of the frame to get.  First frame is 0
//
// Returns:     Pointer to a CFrame object for the specified frame number
//              If the frame number is not valid, returns a NULL pointer
//
//------------------------------------------------------------------------
CFrame * CFrameList::getBaseFrame(int frameNumber)
{
   if (getClipVer() == CLIP_VERSION_5L)
      {
      CAutoErrorReporter errorReporter("CFrameList::getBaseFrame");
      errorReporter.errorCode = CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;
      errorReporter.msg << "Function not supported by clip version 5";
      return 0;
      }


   if (!isFrameIndexValid(frameNumber))
      return 0;

   if (!isFrameListAvailable())
      {
      // Read the framelist from the Frame List file
      int retVal = readFrameListFile();
      if (retVal != 0)
         {
         frameListAvailable = false;
         TRACE_0(errout << "ERROR: In CFrameList::getBaseFrame, call to " << endl
                        << "       CFrameList::readTrackFile failed with return value "
                        << retVal;
                );
         return 0;
         }
      }

   return (frameList)[frameNumber];
}

EClipVersion CFrameList::getClipVer() const
{
   return parentTrack->getClipVer();
}


ETrackType CFrameList::getTrackType() const
{
   return framingInfo->getTrackType();
}

int CFrameList::getDefaultAAFrameIndex()
{
   return GetWholeHourTimecode(inTimecode) - frame0Timecode;
}

string CFrameList::getDescription() const
{
   return framingInfo->getDescription();
}

EVideoFieldRate CFrameList::getFieldRateEnum() const
{
   return CImageInfo::convertFrameRateToFieldRate(getFrameRateEnum(),
                               parentTrack->getMediaFormat()->getInterlaced());
}

//------------------------------------------------------------------------
//
// Function:   getFrameIndex
//
// Description:
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
int CFrameList::getFrameIndex(const CTimecode &callersTC)
{
   if (getClipVer() == CLIP_VERSION_5L)
      {
      CAutoErrorReporter errorReporter("CFrameList::getFrameIndex");
      errorReporter.errorCode = CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;
      errorReporter.msg << "Function not supported by clip version 5";
      return -1;
      }

   int trialFrameIndex = callersTC - frame0Timecode;

   if (
	getFramingType() == FRAMING_TYPE_VIDEO ||
	getFramingType() == FRAMING_TYPE_CADENCE_REPAIR
      )
      {
      return trialFrameIndex;
      }
   else
      {
      // Adjust the trial frame index based on the ratio of frame-rates
      // between this track and the timecode frame rate

      double trackFrameRate = getFrameRate();
      double tcFrameRate = inTimecode.getFramesPerSecond();
      double frameRateRatio = 1.;
      if (tcFrameRate != 0.)
         frameRateRatio = trackFrameRate / tcFrameRate;

      trialFrameIndex
               = (int)floor(((double)trialFrameIndex * frameRateRatio) + .5);

      // Now search for frame with the caller's timecode
      int newFrameIndex = searchForTimecode(callersTC, trialFrameIndex);
      if (newFrameIndex == -1)
         {
         // The search failed, but maybe the timecode is the same as
         // the frame list's out timecode
         if (callersTC == outTimecode)
            {
            // Caller's timecode matches the out timecode, so return
            // the out frame index
            newFrameIndex = outFrameIndex;
            }
         }
      return newFrameIndex;
      }
}

//------------------------------------------------------------------------
//
// Function:   getFrameIndexSpecial
//
// Description:
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
int CFrameList::getFrameIndexSpecial(const CTimecode &callersTC)
{
   if (getClipVer() == CLIP_VERSION_5L)
      {
      CAutoErrorReporter errorReporter("CFrameList::getFrameIndexSpecial");
      errorReporter.errorCode = CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;
      errorReporter.msg << "Function not supported by clip version 5";
      return -1;
      }

   int trialFrameIndex = callersTC
               - frame0Timecode.convertTimecode(callersTC.getFramesPerSecond(),
                                                callersTC.isDropFrame());

   return trialFrameIndex;
}

int CFrameList::searchForTimecode(const CTimecode &callersTC,
                                  int trialFrameIndex)
{
   CFrame *frameN;
   CFrame *frameNPlus1;
   CTimecode tcFrameN(0), tcFrameNPlus1(0);
   int N = trialFrameIndex;

   // Search for frame index N, where the caller's timecode
   // is greater than or equal to frame N's timecode AND
   // caller's timecode is less than frame N+1's timecode

   // Note: Timecode comparisons ignore the half-frame flag

   frameN = getBaseFrame(N);
   if (frameN == 0)
      {
      // Caller's TC is out-of-range
      return -1;
      }
   tcFrameN = frameN->getTimecode();

   // Perform initial comparison of caller's timecode and the timecode
   // at the trial frame index.  This will determine the search
   // direction, nominally earlier or later in the track.
   if (callersTC < tcFrameN)
      {
      // Search backwards (nominally earlier timecodes) in track,
      // starting with frame N-1

      while (true)
         {
         N = N - 1;
         frameNPlus1 = frameN;
         tcFrameNPlus1 = tcFrameN;
         frameN = getBaseFrame(N);
         if (frameN == 0)
            {
            // out of frames without finding timecode
            return -1;
            }
         tcFrameN = frameN->getTimecode();
         if (tcFrameN <= callersTC && callersTC < tcFrameNPlus1)
            {
            // Frame N is the one
            return N;
            }
         }
      }
   else // callersTC >= tcFrameN
      {
      // Search forwards (nominally later timecodes) in track,
      // starting with frame N

      frameNPlus1 = getBaseFrame(N + 1);
      while (true)
         {
         if (frameNPlus1 == 0)
            {
            if (callersTC.getAbsoluteTime() == tcFrameN.getAbsoluteTime())
               {
               return N;
               }
            else
               {
               return -1;
               }
            }
         tcFrameNPlus1 = frameNPlus1->getTimecode();
         if (tcFrameN <= callersTC && callersTC < tcFrameNPlus1)
            {
            // Frame N is the one
            return N;
            }

         frameN = frameNPlus1;
         tcFrameN = tcFrameNPlus1;
         N = N + 1;
         frameNPlus1 = getBaseFrame(N + 1);
         }
      }

} // searchForTimecode

//------------------------------------------------------------------------
//
// Function:    getFrameIndex
//
// Description:
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
int
CFrameList::getFrameIndex(int hours, int minutes, int seconds, int frames)
{
   if (getClipVer() == CLIP_VERSION_5L)
      {
      CAutoErrorReporter errorReporter("CFrameList::getFrameIndex");
      errorReporter.errorCode = CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;
      errorReporter.msg << "Function not supported by clip version 5";
      return -1;
      }

   CTimecode callersTC(inTimecode);
   callersTC.setTime(hours, minutes, seconds, frames);

   return(getFrameIndex(callersTC));
}

//------------------------------------------------------------------------
//
// Function:    getFrameIndexSpecial
//
// Description:
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
int
CFrameList::getFrameIndexSpecial(int hours, int minutes, int seconds, int frames)
{
   if (getClipVer() == CLIP_VERSION_5L)
      {
      CAutoErrorReporter errorReporter("CFrameList::getFrameIndexSpecial");
      errorReporter.errorCode = CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;
      errorReporter.msg << "Function not supported by clip version 5";
      return -1;
      }

   CTimecode callersTC(inTimecode);
   callersTC.setTime(hours, minutes, seconds, frames);

   return(getFrameIndexSpecial(callersTC));
}

EFrameRate CFrameList::getFrameRateEnum() const
{
   return framingInfo->getFrameRateEnum();
}

MTI_REAL32 CFrameList::getFrameRate() const
{
   return framingInfo->getFrameRate();
}

int CFrameList::getFramingType() const
{
   return framingInfo->getFramingType();
}

int CFrameList::getInFrameIndex() const
{
   if (getClipVer() == CLIP_VERSION_5L)
      {
      // TEMPORARY CODE
      // This works with tracks without 3:2 (or otherwise) pulldown
      return parentTrack->getHandleCount();
      }
   else
      return inFrameIndex;
}

void CFrameList::getInTimecode(CTimecode &tc) const
{
   if (getClipVer() == CLIP_VERSION_3)
      {
      tc = inTimecode;
      }
   else if (getClipVer() == CLIP_VERSION_5L)
      {
      tc = getInTimecode();
      }
}

int CFrameList::getMaxTotalFieldCount(bool forceRecalc)
{
// Returns the maximum number of total fields (visible + hidden) for
// any frame in the frame list.  Caches value for repeated calls.

   if (maxTotalFieldCount < 1 || forceRecalc)
      {
      if (getClipVer() == CLIP_VERSION_5L)
         {
         maxTotalFieldCount = 0;
         const CMediaFormat *mediaFormat;
         mediaFormat = parentTrack->GetTrackHandle().GetMediaFormat();
         if (mediaFormat == 0)
            return 0;
         maxTotalFieldCount = mediaFormat->getFieldCount();
         return maxTotalFieldCount;
         }
      else
         {
         // If cached count maxTotalFieldCount has never been set or
         // caller wants a re-count, then iterate over all the frames,
         // keeping track of the highest field count.
         maxTotalFieldCount = 0;
         int totalFrameCount = getTotalFrameCount();
         for (int i = 0; i < totalFrameCount; ++i)
            {
            CFrame *frame = getBaseFrame(i);
            int totalFieldCount = frame->getTotalFieldCount();
            if (totalFieldCount > maxTotalFieldCount)
               maxTotalFieldCount = totalFieldCount;
            }
         }
      }

   return maxTotalFieldCount;
}

int CFrameList::getMediaAllocatedFrameCount(bool forceRecalc)
{
// Returns count of frames in this frame list where the media storage has
// been allocated.  This value can be:
//   - zero if media has not been allocated for any frame (e.g., prior to
//     creating image files)
//   - same as total frame count if media has been allocated for all frames
//   - more than zero but less than the total frame count if media has
//     only been allocated for some frames.
// The allocated frame count is determined when this function is first called
// or when the forceRecalc flag is true.  The count is then cached.
// This function also updates the mediaAllocatedFrontier, which is the index
// of the last frame in the frame list that has media storage allocated.  If
// no frame has media storage allocated, then mediaAllocatedFrontier is set
// to -1.  If all frames have media storage allocated, then
// mediaAllocatedFrontier is set to totalFrameCount-1

   int retVal;

   if (forceRecalc || mediaAllocatedFrameCount < 0)
      {
      mediaAllocatedFrameCount = 0;  // initialize
      mediaAllocatedFrontier = -1;

      if (getClipVer() == CLIP_VERSION_3)
         {
         // Iterate over all frames in frame list, checking if media
         // exists for each frame.  Assumes there are no holes in the
         // allocation.
         int totalFrameCount = getTotalFrameCount();
         for (int frameIndex = 0; frameIndex < totalFrameCount; ++frameIndex)
            {
            if (doesMediaStorageExist(frameIndex))
               {
               ++mediaAllocatedFrameCount;
               mediaAllocatedFrontier = frameIndex;
               }
            else
               break;
            }
         }
      else if (getClipVer() == CLIP_VERSION_5L)
         {
         // Ask the Clip 5 track how may frames have media
         // Clip 5 supports gaps in a track, but here we assume that
         // there are no gaps, i.e., if there is filler, then the filler
         // is at the end of the track and never somewhere in the middle.
         int itemCount = 0;
         retVal = parentTrack->GetTrackHandle().LSF1(itemCount);
         if (retVal != 0)
            {
            TRACE_0(errout << "ERROR: CFrameList::getMediaAllocatedFrameCount: LSF1 failed with "
                           << retVal);
            return 0;
            }

         mediaAllocatedFrameCount = itemCount;
         mediaAllocatedFrontier = itemCount - 1;
         }
      else
         {
         TRACE_0(errout << "ERROR: CFrameList::getMediaAllocatedFrameCount: Invalid clip version "
                        << getClipVer());
         }
      }

   return mediaAllocatedFrameCount;
}

int CFrameList::getMediaAllocatedFrameCountCached()
{
   if (getClipVer() == CLIP_VERSION_5L)
      {
      // In Clip 5, the media allocated frame count is not cached,
      // so calculate, but only if necessary
      getMediaAllocatedFrameCount(false);
      }

   return mediaAllocatedFrameCount;
}

int CFrameList::getMediaAllocatedFrontier(bool forceRecalc)
{
// This function returns the mediaAllocatedFrontier, which is the index
// of the last frame in the frame list that has media storage allocated.  If
// no frame has media storage allocated, then mediaAllocatedFrontier is set
// to -1.  If all frames have media storage allocated, then
// mediaAllocatedFrontier is set to totalFrameCount-1
// This function uses getMediaAllocatedFrameCount to update and cache the
// mediaAllocatedFrontier and so as a side effect the mediaAllocatedFrameCount
// may be updated.  See the description of getMediaAllocatedFrameCount

   getMediaAllocatedFrameCount(forceRecalc);

   return mediaAllocatedFrontier;
}

CTimecode CFrameList::getInTimecode() const
{
   if (getClipVer() == CLIP_VERSION_5L)
      {
      TRACE_3(errout << "WARNING: CFrameList::getInTimecode called for Clip 5 track");

      // Clip 5 video and audio tracks do not have timecodes, so
      // synthesize a time code based on the Clip's In Timecode and
      // the video format
      auto parentClip = parentTrack->getParentClip();
      CTimecode clipInTC = parentClip->getInTimecode();

      const CMediaFormat *mediaFormat = parentTrack->getMediaFormat();

      if (mediaFormat->getMediaFormatType() == MEDIA_FORMAT_TYPE_VIDEO3)
         {
         const CImageFormat* imageFormat
                                = static_cast<const CImageFormat*>(mediaFormat);
         EImageFormatType imageFormatType = imageFormat->getImageFormatType();

         int tcFps = CImageInfo::queryNominalTimecodeFrameRate(imageFormatType);
         int tcFlags = (clipInTC.isDropFrame()) ? DROPFRAME : 0;
         tcFlags |= CImageInfo::queryIsFormat720P(imageFormatType) ? FRAME720P : 0;

         return CTimecode(clipInTC.getHours(), clipInTC.getMinutes(),
                          clipInTC.getSeconds(), clipInTC.getFrames(),
                          tcFlags, tcFps);
         }
      else
         {
         // For audio tracks, just return the clip's in timecode
         return clipInTC;
         }
      }
   else
      {
      return inTimecode;
      }
}

CTimecode CFrameList::getOutTimecode() const
{
   if (getClipVer() == CLIP_VERSION_5L)
      {
      TRACE_3(errout << "WARNING: CFrameList::getOutTimecode called for Clip 5 track");

      CTimecode tmpInTC = getInTimecode();

      return tmpInTC + getUserFrameCount();
      }
   else
      {
      return(outTimecode);
      }
}

int CFrameList::getOutFrameIndex() const
{
   if (getClipVer() == CLIP_VERSION_5L)
      {
      int itemCount = parentTrack->GetTrackHandle().GetItemCount();
      itemCount -= parentTrack->getHandleCount();
      return itemCount;
      }
   else
      return outFrameIndex;
}

int CFrameList::GetMediaLocationsAllFields(int frameIndex,
                                      vector<CMediaLocation> &mediaLocationList)
{
   int retVal;

   if (!isFrameIndexValid(frameIndex))
      return CLIP_ERROR_INVALID_FRAME_NUMBER;

   if (getClipVer() == CLIP_VERSION_3)
      {
      CFrame *frame = getBaseFrame(frameIndex);
      if (frame == 0)
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

      retVal = frame->GetMediaLocationsAllFields(mediaLocationList);
      if (retVal != 0)
         return retVal;
      }
   else if (getClipVer() == CLIP_VERSION_5L)
      {
      retVal = parentTrack->GetTrackHandle().GetMediaLocationsAllFields(
                                                            frameIndex,
                                                            mediaLocationList);
      if (retVal != 0)
         return retVal;
      }
   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   return 0;
}

CTrack* CFrameList::getParentTrack() const
{
   return parentTrack;
}

bool CFrameList::getSaveRecordFlag() const
{
   return framingInfo->getSaveRecordFlag();
}

//------------------------------------------------------------------------
//
// Function:   getTimecodeForFrameIndex
//
// Description:
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
CTimecode CFrameList::getTimecodeForFrameIndex(int frameIndex)
{
   if (getClipVer() == CLIP_VERSION_5L)
      {
      CAutoErrorReporter errorReporter("CFrameList::getTimecodeForFrameIndex");
      errorReporter.errorCode = CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;
      errorReporter.msg << "Function not supported by clip version 5";
      return CTimecode(0);
      }

   if (getFramingType() == FRAMING_TYPE_VIDEO ||
       getFramingType() == FRAMING_TYPE_CADENCE_REPAIR)
      {
      // In a video-frame track, timecode consistently increments
      // by one frame (adjusted for Drop Frame) for each frame.
      // It is therefore safe to calculate the timecode of the user's
      // frame index by adding it to the timecode of the first
      // frame of the track.  This function does not need the track's
      // frame list and also works for indices that are beyond
      // the actual range of the track
      return(frame0Timecode + frameIndex);
      }
   else
      {
      // In non-video-frames tracks, the timecodes do not increment
      // consistently for each frame.  Therefore the timecode for
      // a frame must be obtained from the frame in the frame list.
      CFrame *frame = getBaseFrame(frameIndex);
      if (frame == 0)
         {
         // ERROR: frame index is out-of-range
         return CTimecode(0);
         }

      return frame->getTimecode();
      }
}

//------------------------------------------------------------------------
//
// Function:   getTimecodeForFrameIndexSpecial
//
// Description:
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
CTimecode CFrameList::getTimecodeForFrameIndexSpecial(int newFrameIndex,
                                                      int newFramesPerSecond,
                                                      bool dropFrame)
{
   if (getClipVer() == CLIP_VERSION_5L)
      {
      CAutoErrorReporter errorReporter("CFrameList::getTimecodeForFrameIndexSpecial");
      errorReporter.errorCode = CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;
      errorReporter.msg << "Function not supported by clip version 5";
      return CTimecode(0);
      }

   if (getFramingType() == FRAMING_TYPE_VIDEO)
      {
      return frame0Timecode.convertTimecode(newFramesPerSecond, dropFrame)
             + newFrameIndex;
      //
      }
   else
      {
      CTimecode tc(0);
      return tc;
      }
}
//------------------------------------------------------------------------
//
// Function:   getTimecodeForFrameIndex
//
// Description:
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
void CFrameList::
getTimecodeForFrameIndex(int frameIndex, int *hours, int *minutes,
                         int *seconds, int *frames, int *dropframe)
{
   CTimecode tc(0);

   if (getClipVer() == CLIP_VERSION_5L)
      {
      CAutoErrorReporter errorReporter("CFrameList::getTimecodeForFrameIndex");
      errorReporter.errorCode = CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;
      errorReporter.msg << "Function not supported by clip version 5";
      }
   else
      {
      tc = getTimecodeForFrameIndex(frameIndex);
      }

   *hours = tc.getHours();
   *minutes = tc.getMinutes();
   *seconds = tc.getSeconds();
   *frames = tc.getFrames();

   if (tc.getFlags() & DROPFRAME)
     *dropframe = 1;
   else
     *dropframe = 0;
}

//------------------------------------------------------------------------
//
// Function:   getTimecodeForFrameIndex
//
// Description:
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
void CFrameList::
getTimecodeForFrameIndex(int frameIndex, int *hours, int *minutes,
                         int *seconds, int *frames)
{
   CTimecode tc(0);

   if (getClipVer() == CLIP_VERSION_5L)
      {
      CAutoErrorReporter errorReporter("CFrameList::getTimecodeForFrameIndex");
      errorReporter.errorCode = CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;
      errorReporter.msg << "Function not supported by clip version 5";
      }
   else
      {
      tc = getTimecodeForFrameIndex(frameIndex);
      }

   *hours = tc.getHours();
   *minutes = tc.getMinutes();
   *seconds = tc.getSeconds();
   *frames = tc.getFrames();
}

int CFrameList::getTotalFrameCount()
{
   EClipVersion clipVer = getClipVer();

   if (clipVer == CLIP_VERSION_3)
      {
      if (!isFrameListAvailable())
         {
         // Read the framelist from the Frame List file
         int retVal = readFrameListFile();
         if (retVal != 0)
            {
            frameListAvailable = false;
            TRACE_0(errout << "ERROR: In CFrameList::getBaseFrame, call to " << endl
                           << "       CFrameList::readTrackFile failed with return value "
                           << retVal;);
            return 0;
            }
         }
      return frameList.size();
      }
   else if (clipVer == CLIP_VERSION_5L)
      {
      return parentTrack->GetTrackHandle().GetTotalFrameCount();
      }
   else
      {
      // TBD: error handling
      return 0;
      }

}

int CFrameList::getTotalFrameCountSpecial(int AFrameIndex)
{
   int totalFrameCount = getTotalFrameCount();

// TBD: For Clip 5, A-Frame index is obtained from Pulldown segment
//      rather than from AFrameIndex member variable.
   int specialTotalFrameCount
           = TranslateFrameIndex24To30(totalFrameCount - 1, AFrameIndex) + 1;

   return specialTotalFrameCount;


/* TTT - OLDWAY ----------------------------------------------------

   int totalFrameCount = getTotalFrameCount();

   if (totalFrameCount < 2)
      return totalFrameCount;

   int frame0Label = (-AFrameIndex) % 4;
   if (AFrameIndex > 0)
      frame0Label += 4;

   int lastFrame = totalFrameCount - 1;
   int lastFrameLabel = (lastFrame - AFrameIndex) % 4;
   if (AFrameIndex > lastFrame)
      lastFrameLabel += 4;

   int firstAFrame = frame0Label;
   int lastDFrame = lastFrame - (lastFrameLabel - 1);

   // 24 fps => 30 fps with pull-down
   int specialTotalFrameCount = ((lastDFrame - firstAFrame + 1) / 4) * 5;

   // Adjust if first frame is not an A Frame
   if (frame0Label == 1)
      specialTotalFrameCount += 4;   // B Frame
   else if (frame0Label == 2)
      specialTotalFrameCount += 2;   // C Frame
   else if (frame0Label == 3)
      specialTotalFrameCount += 1;   // D Frame

   // Adjust if last frame is not a D Frame
   if (lastFrameLabel != 3)
      specialTotalFrameCount += lastFrameLabel;  // A, B or C Frame

   return specialTotalFrameCount;

 ---------------------------------------       TTT - OLDWAY */
}

string CFrameList::getFrameListFileName() const
{
   return makeFrameListFileName();
}

string CFrameList::getNewFrameListFileName(string newClipName) const
{
   return makeNewFrameListFileName(newClipName);
}

string CFrameList::getFrameListFileNameWithExt() const
{
   string frameListFileName;
   frameListFileName = makeFrameListFileName() + '.'
                       + getFrameListFileExtension();

   return frameListFileName;
}

string CFrameList::getFrameListFileExtension()
{
   return frameListFileExtension;
}

int CFrameList::getUserFrameCount() const
{
   EClipVersion clipVer = getClipVer();

   if (clipVer == CLIP_VERSION_3)
      {
      return outFrameIndex - inFrameIndex;
      }
   else if (clipVer == CLIP_VERSION_5L)
      {
      return getOutFrameIndex() - getInFrameIndex();
      }
   else
      {
      // TBD: Error handling
      return 0;
      }
}

int CFrameList::getDirtyFrameCount()
{
   int retVal = 0;
   int frameCount = getTotalFrameCount();
   for (int i = 0; i < frameCount; ++i)
      {
      CFrame* frame = getBaseFrame(i);
      retVal += frame->isDirty()? 1 : 0;
      }

   return retVal;
}

bool CFrameList::isFrameIndexAHandle(int frameIndex)
{
   // Return true if frame index is in the range of the handles at the
   // beginning or end of the track.  Return false if the frame index
   // is within range of user material or outside total range of frames

   if (0 <= frameIndex && frameIndex < inFrameIndex)
      return true;  // Frame index within beginning handle
   if (outFrameIndex <= frameIndex && frameIndex < getTotalFrameCount())
      return true;  // Frame index within end handle

   return false;
}

bool CFrameList::isFrameIndexUserMaterial(int frameIndex) const
{
   // Return true if frame index is within range of user material (non-handle
   // frames)

   EClipVersion clipVer = getClipVer();

   if (clipVer == CLIP_VERSION_3)
      {
      if (inFrameIndex <= frameIndex && frameIndex < outFrameIndex)
         return true;
      else
         return false;
      }
   else if (clipVer == CLIP_VERSION_5L)
      {
      if (getInFrameIndex() <= frameIndex && frameIndex < getOutFrameIndex())
         return true;
      else
         return false;
      }
   else
      {
      // TBD: Error handling for bad clip version
      return false;
      }
}

bool CFrameList::isFrameIndexValid(int frameIndex)
{
   // Returns true if frame index is within range of total range of frames

   if (frameIndex < 0 || frameIndex >= getTotalFrameCount())
      return false;

   return true;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CFrameList::preLoadFiles()
{
   int retVal;

   if (getClipVer() == CLIP_VERSION_5L)
      return 0;   // No action for clip 5

   // The clip library does not load the frame list from the frame list file
   // until it is accessed for the first time.  This causes problems in
   // the VTR emulator.  This function forces the frame list file to be read.
   // If the file has already been read, this function does nothing.

   // Force the read of the frame list file if the frame list is not available
   if (!isFrameListAvailable())
      {
      // Read the framelist from the Frame List file
      int retVal = readFrameListFile();
      if (retVal != 0)
         {
         frameListAvailable = false;
         TRACE_0(errout << "ERROR: In CFrameList::getBaseFrame, call to " << endl
                        << "       CFrameList::readTrackFile failed with return value "
                        << retVal;
                );
         return retVal;
         }
      }

   if (parentTrack != 0)
      {
      // Force the read of the field list file
      retVal = parentTrack->preLoadFieldList();
      if (retVal != 0)
         return retVal;

      // Pre-load the media storage access. e.g., open the raw disk file
      const CMediaStorageList* mediaStorageList;
      mediaStorageList = parentTrack->getMediaStorageList();
      int mediaStorageCount = mediaStorageList->getMediaStorageCount();
      for (int i = 0; i < mediaStorageCount; ++i)
         {
         CMediaStorage *mediaStorage = mediaStorageList->getMediaStorage(i);
         CMediaAccess *mediaAccess = mediaStorage->getMediaAccessObj();
         retVal = mediaAccess->preload();
         if (retVal != 0)
            return retVal;
         }
      }

   // Pre-calculate and cache some frame list statistics
   getMaxTotalFieldCount();
   getMediaAllocatedFrameCount();

   return 0;
}

int CFrameList::reloadFieldList()
{
   int retVal;

   if (parentTrack != 0)
      {
      retVal = parentTrack->reloadFieldList();
      if (retVal != 0)
         return retVal;
      }

   return 0;
}
//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CFrameList::setMediaWritten(int frameNumber, bool newFlag)
{
   int retVal;
   EClipVersion clipVer = getClipVer();

   if (clipVer == CLIP_VERSION_3)
      {
      CFrame *frame = getBaseFrame(frameNumber);
      if (frame == 0)
         return CLIP_ERROR_INVALID_FRAME_NUMBER;

      frame->setMediaWritten(newFlag);
      }
   else if (clipVer == CLIP_VERSION_5L)
      {
      // In clip 5, application may clear but not set the media written
      // flag
      if (newFlag)
         return CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;
      else if (!isFrameIndexValid(frameNumber))
         return CLIP_ERROR_INVALID_FRAME_NUMBER;
      else
         {
         C5MediaTrackHandle trackHandle = parentTrack->GetTrackHandle();
         const CMediaFormat *mediaFormat = trackHandle.GetMediaFormat();
         if (mediaFormat == 0)
            return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

         int fieldCount = mediaFormat->getFieldCount();

         retVal = trackHandle.ClearMediaWritten(frameNumber * fieldCount,
                                                fieldCount);
         if (retVal != 0)
            return retVal;
         }
      }
   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   return 0;
}

int CFrameList::setMediaWritten(int startFrameNumber, int endFrameNumber,
                                bool newFlag)
{
   // If endFrameNumber is 0 or greater, then it is inclusive
   // If endFrameNumber is less than 0, then it indicates to set the
   // media written state until the end of the frame list

   int retVal;

   if (!isFrameIndexValid(startFrameNumber))
      return CLIP_ERROR_INVALID_FRAME_NUMBER;

   if (endFrameNumber < 0)
      endFrameNumber = getTotalFrameCount() - 1;

   if (endFrameNumber < startFrameNumber || !isFrameIndexValid(endFrameNumber))
      return CLIP_ERROR_INVALID_FRAME_NUMBER;

   EClipVersion clipVer = getClipVer();

   if (clipVer == CLIP_VERSION_3)
      {
      // If the endFrameNumber is less than 0, then set all frames from
      // the caller's start frame number to the end of the clip
      for (int i = startFrameNumber; i <= endFrameNumber; ++i)
         getBaseFrame(i)->setMediaWritten(newFlag);

      }
   else if (clipVer == CLIP_VERSION_5L)
      {
      // In Clip 5, the application may clear but not set the media written
      // flag
      if (newFlag)
         return CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;
      else
         {
         C5MediaTrackHandle trackHandle = parentTrack->GetTrackHandle();
         const CMediaFormat *mediaFormat = trackHandle.GetMediaFormat();
         if (mediaFormat == 0)
            return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

         int fieldCount = mediaFormat->getFieldCount();

         int itemCount = fieldCount * (endFrameNumber - startFrameNumber + 1);
         retVal = trackHandle.ClearMediaWritten(startFrameNumber * fieldCount,
                                                itemCount);
         if (retVal != 0)
            return retVal;
         }
      }
   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Framing Utilities
//////////////////////////////////////////////////////////////////////

int CFrameList::createVideoFrames_FrameList()
{
   int handleCount = parentTrack->getHandleCount();
   auto parentClip = parentTrack->getParentClip();
   CTimecode timecode = parentClip->getInTimecode();

   // Calculate the number of frames based on difference between out and in
   // timecodes and the handles at the beginning and end of the track
   int userFrameCount = parentClip->getOutTimecode() - timecode;
   int frameCount = userFrameCount + 2 * handleCount;

   // Timecode of first frame is the in timecode minus the handle count
   timecode = timecode + (-1 * handleCount);

   CMediaFormat* mediaFormat
                  = const_cast<CMediaFormat*>(parentTrack->getMediaFormat());
   int fieldCount = mediaFormat->getFieldCount();
   CFrame *frame;

   CFieldList *fieldList = parentTrack->getFieldList();
   int fieldListEntryIndex = 0;

   for (int i = 0; i < frameCount; ++i)
      {
      // Create an instance of Video Frame, set its parameters and add it
      // to this track's frame list
      frame = createFrame(fieldCount, mediaFormat, timecode);

      // Create and add the frame's fields
      for (int j = 0; j < fieldCount; ++j, ++fieldListEntryIndex)
         {
         // Create a proper CField subtype and add it to the frame
         frame->createField(fieldList, fieldListEntryIndex);
         fieldList->setVideoFrameListBackReference(fieldListEntryIndex, i, j);
         }

      // Increment the timecode for the next frame
      ++timecode;
      }

   // Remember indices of in and out frames
   inFrameIndex = handleCount;
   outFrameIndex = inFrameIndex + userFrameCount;

   if ((frame = getBaseFrame(0)) != 0)
      frame0Timecode = frame->getTimecode();

   if ((frame = getBaseFrame(inFrameIndex)) != 0)
      inTimecode = frame->getTimecode();

   if (outFrameIndex < frameCount)
      {
      // There is a frame at the outFrameIndex, so use that frame's
      // timecode as the out timecode
      if ((frame = getBaseFrame(outFrameIndex)) != 0)
         outTimecode = frame->getTimecode();
      }
   else
      {
      // There are no frames after the user's last frame, so
      // estimate the out timecode as the last frame's timecode plus one
      // frame
      if ((frame = getBaseFrame(frameCount - 1)) != 0)
         outTimecode = frame->getTimecode() + 1;
      }

   return 0;
} // createVideoFrames_FrameList

int CFrameList::extendVideoFrames_FrameList(int newFrameCount)
{
// Add frames to the end of the existing video-frames frame list

   CTimecode timecode = getOutTimecode();

   CMediaFormat* mediaFormat
                  = const_cast<CMediaFormat*>(parentTrack->getMediaFormat());
   int fieldCount = mediaFormat->getFieldCount();

   CFieldList *fieldList = parentTrack->getFieldList();
   int fieldListEntryIndex =
      (outFrameIndex + parentTrack->getHandleCount()) * fieldCount;

   int existingFrameCount = getTotalFrameCount();
   int totalFrameCount = existingFrameCount + newFrameCount;

   for (int i = existingFrameCount; i < totalFrameCount; ++i)
      {
      // Create an instance of CFrame subtype (video or audio), set its
      // parameters and add it to this track's frame list
      CFrame *frame = createFrame(fieldCount, mediaFormat, timecode);

      // Create and add the frame's fields
      for (int j = 0; j < fieldCount; ++j, ++fieldListEntryIndex)
         {
         // Create a proper CField subtype and add it to the frame
         frame->createField(fieldList, fieldListEntryIndex);
         fieldList->setVideoFrameListBackReference(fieldListEntryIndex, i, j);
         }

      // Increment the timecode for the next frame
      ++timecode;
      }

   // Update indices of  out frame and timecode
   outFrameIndex = outFrameIndex + newFrameCount;
   outTimecode = timecode;

   return 0;

} // extendVideoFrames_FrameList

int CFrameList::createFilmFrames_FrameList(CFrameList *videoFramesFrameList,
                                           EPulldown pulldown,
                                           EFrameLabel frameLabels[])
{
   int retVal;

   CMediaFormat* mediaFormat
                  = const_cast<CMediaFormat*>(parentTrack->getMediaFormat());

   // Initialize helper structure SFilmFramesSource, which is used to pass
   // arguments and results of video-frames to film-frames conversion
   SFilmFrameSource filmFrameSrc;
   filmFrameSrc.frameSrc.srcTrack = videoFramesFrameList;
   filmFrameSrc.inFrameIndex = 0;
   filmFrameSrc.outFrameIndex = videoFramesFrameList->getTotalFrameCount();
   filmFrameSrc.frameLabels = frameLabels;
   CTimecode timecode(0);
   filmFrameSrc.timecode = &timecode;

   int dstFrameIndex = 0;     //
   int dstInFrameIndex = filmFrameSrc.outFrameIndex;
   int dstOutFrameIndex = -1;

   CFrame *frame;

   // Iterate over all frames in the video-frames source track
   for (filmFrameSrc.frameIndex = filmFrameSrc.inFrameIndex;
        filmFrameSrc.frameIndex < filmFrameSrc.outFrameIndex;
        ++filmFrameSrc.frameIndex)
      {
      // Get the timecode of the source video frame.  This will be the
      // timecode of the resulting film frame, possibly adjusted while
      // removing 3:2 pulldown
      frame = videoFramesFrameList->getBaseFrame(filmFrameSrc.frameIndex);
      timecode = frame->getTimecode();

      // Determine how to construct a film-frame without 3:2 pulldown
      // from one or more source video-frames, steered by the caller's
      // "frame labels"
      switch(pulldown)
         {
         case IF_PULLDOWN_SWAPPED_FIELD_3_2 :
            // Interlaced 525 formats have field-based 3:2 pulldown
            // which makes 10 video fields from original 4 film frames.
            // 525 also has odd/even field order which is swapped on
            // input or output from CPMP.
            remove3_2SwappedFieldPulldown(filmFrameSrc);  // Field pulldown
            break;
         case IF_PULLDOWN_FIELD_3_2 :
            // Interlaced 1080I/60, etc. formats have field-based 3:2 pulldown
            // which makes 10 video fields from original 4 film frames, e.g.,
            // film frames Aa, Bb, Cc, Dd become video frames Aa, Bb, Bc, Cd, Dd.
            remove3_2FieldPulldown(filmFrameSrc);  // Field pulldown
            break;
         case IF_PULLDOWN_FRAME_3_2 :
            // 720P/60 progressive-scan format has frame-based 3:2 pulldown
            // which repeats two original film frames to get five video frames,
            // e.g., FRFRR
            remove3_2FramePulldown(filmFrameSrc);
            break;
         case IF_PULLDOWN_PAL_FD :
            // Kludge to correct PAL field dominance problem
            fixPALFieldDominance(filmFrameSrc);
            break;
         default :
            return -1;  // Bad pulldown argument, shouldn't ever happen
         }

      // Create a new film frame and new fields based on the source video
      // frames' field information
      if (filmFrameSrc.frameSrc.fieldCount > 0)
         {
         // Make a proper CFrame subtype to hold the film frame
         CFrame *newFilmFrame = createFrame(filmFrameSrc.frameSrc.fieldCount,
                                             mediaFormat, timecode);

         // Copy the appropriate fields from the video frame(s)
         // to the new film frame.
         retVal = newFilmFrame->copyFields(filmFrameSrc.frameSrc);
         if (retVal != 0)
            {
            // ERROR: Failed to copy fields from source track
            return retVal;
            }

         // The film-frames track's in and out frame indices are determined
         // by earliest and latest frames that have only user material
         // (i.e., fields do not include any handle material)
         if (filmFrameSrc.frameSrc.isAllSourceFramesUserMaterial())
            {
            if (dstFrameIndex < dstInFrameIndex)
               dstInFrameIndex = dstFrameIndex;

            if (dstFrameIndex > dstOutFrameIndex)
               dstOutFrameIndex = dstFrameIndex;
            }

         // Advance the frame index for next new film-frame
         dstFrameIndex += 1;
         }
      }

   // Set in and out frame indices in this track
   inFrameIndex = dstInFrameIndex;
   outFrameIndex = dstOutFrameIndex + 1;

   if ((frame = getBaseFrame(0)) != 0)
      frame0Timecode = frame->getTimecode();

   if ((frame = getBaseFrame(inFrameIndex)) != 0)
      inTimecode = frame->getTimecode();

   if (outFrameIndex < dstFrameIndex)
      {
      // There is a frame at the outFrameIndex, so use that frame's
      // timecode as the out timecode
      if ((frame = getBaseFrame(outFrameIndex)) != 0)
         outTimecode = frame->getTimecode();
      }
   else
      {
      // There are no frames after the user's last frame, so
      // estimate the out timecode as the last frame's timecode plus one
      // frame
      if ((frame = getBaseFrame(dstOutFrameIndex)) != 0)
         {
         outTimecode = frame->getTimecode() + 1;
         outTimecode.setHalfFrame(false);
         }
      }

   return 0; // return success

} // createFilmFrames_FrameList

int CFrameList::extendFilmFrames_FrameList(CFrameList *videoFramesFrameList,
                                           int startFrame, int frameCount,
                                           EPulldown pulldown,
                                           EFrameLabel frameLabels[])
{
   int retVal;

   CMediaFormat* mediaFormat
                  = const_cast<CMediaFormat*>(parentTrack->getMediaFormat());

   // Initialize helper structure SFilmFramesSource, which is used to pass
   // arguments and results of video-frames to film-frames conversion
   SFilmFrameSource filmFrameSrc;
   filmFrameSrc.frameSrc.srcTrack = videoFramesFrameList;
   filmFrameSrc.inFrameIndex = startFrame;
   filmFrameSrc.outFrameIndex = frameCount;
   filmFrameSrc.frameLabels = frameLabels;
   CTimecode timecode(0);
   filmFrameSrc.timecode = &timecode;

   int dstFrameIndex = 0;     //
   int dstOutFrameIndex = -1;

   CFrame *frame;

   // Iterate over all frames in the video-frames source track
   for (filmFrameSrc.frameIndex = filmFrameSrc.inFrameIndex;
        filmFrameSrc.frameIndex < filmFrameSrc.outFrameIndex;
        ++filmFrameSrc.frameIndex)
      {
      // Get the timecode of the source video frame.  This will be the
      // timecode of the resulting film frame, possibly adjusted while
      // removing 3:2 pulldown
      frame = videoFramesFrameList->getBaseFrame(filmFrameSrc.frameIndex);
      timecode = frame->getTimecode();

      // Determine how to construct a film-frame without 3:2 pulldown
      // from one or more source video-frames, steered by the caller's
      // "frame labels"
      switch(pulldown)
         {
         case IF_PULLDOWN_SWAPPED_FIELD_3_2 :
            // Interlaced 525 formats have field-based 3:2 pulldown
            // which makes 10 video fields from original 4 film frames.
            // 525 also has odd/even field order which is swapped on
            // input or output from CPMP.
            remove3_2SwappedFieldPulldown(filmFrameSrc);  // Field pulldown
            break;
         case IF_PULLDOWN_FIELD_3_2 :
            // Interlaced 1080I/60, etc. formats have field-based 3:2 pulldown
            // which makes 10 video fields from original 4 film frames, e.g.,
            // film frames Aa, Bb, Cc, Dd become video frames Aa, Bb, Bc, Cd, Dd.
            remove3_2FieldPulldown(filmFrameSrc);  // Field pulldown
            break;
         case IF_PULLDOWN_FRAME_3_2 :
            // 720P/60 progressive-scan format has frame-based 3:2 pulldown
            // which repeats two original film frames to get five video frames,
            // e.g., FRFRR
            remove3_2FramePulldown(filmFrameSrc);
            break;
         case IF_PULLDOWN_PAL_FD :
            // Kludge to correct PAL field dominance problem
            fixPALFieldDominance(filmFrameSrc);
            break;
         default :
            return -1;  // Bad pulldown argument, shouldn't ever happen
         }

      // Create a new film frame and new fields based on the source video
      // frames' field information
      if (filmFrameSrc.frameSrc.fieldCount > 0)
         {
         // Make a proper CFrame subtype to hold the film frame
         CFrame *newFilmFrame = createFrame(filmFrameSrc.frameSrc.fieldCount,
                                             mediaFormat, timecode);

         // Copy the appropriate fields from the video frame(s)
         // to the new film frame.
         retVal = newFilmFrame->copyFields(filmFrameSrc.frameSrc);
         if (retVal != 0)
            {
            // ERROR: Failed to copy fields from source track
            return retVal;
            }

         // The film-frames track's in and out frame indices are determined
         // by earliest and latest frames that have only user material
         // (i.e., fields do not include any handle material)
         if (filmFrameSrc.frameSrc.isAllSourceFramesUserMaterial())
            {
            if (dstFrameIndex > dstOutFrameIndex)
               dstOutFrameIndex = dstFrameIndex;
            }

         // Advance the frame index for next new film-frame
         dstFrameIndex += 1;
         }
      }

   // Update out frame index in this track
   outFrameIndex = dstOutFrameIndex + 1;

   if (outFrameIndex < dstFrameIndex)
      {
      // There is a frame at the outFrameIndex, so use that frame's
      // timecode as the out timecode
      if ((frame = getBaseFrame(outFrameIndex)) != 0)
         outTimecode = frame->getTimecode();
      }
   else
      {
      // There are no frames after the user's last frame, so
      // estimate the out timecode as the last frame's timecode plus one
      // frame
      if ((frame = getBaseFrame(dstOutFrameIndex)) != 0)
         {
         outTimecode = frame->getTimecode() + 1;
         outTimecode.setHalfFrame(false);
         }
      }

   return 0; // return success

} // extendFilmFrames_FrameList

int CFrameList::createFrameListFromClip(CFrameList *dstVideoFramesFrameList,
                                        CFrameList *srcFrameList,
                                        int srcStartFrameIndex,
                                        int srcFrameCount)
{
   int retVal;

   CMediaFormat* mediaFormat
                  = const_cast<CMediaFormat*>(parentTrack->getMediaFormat());
   bool interlaced = mediaFormat->getInterlaced();

   // Initialize helper structure SFilmFramesSource, which is used to pass
   // arguments and results of video-frames to film-frames conversion
   SFilmFrameSource filmFrameSrc;
   filmFrameSrc.frameSrc.srcTrack = dstVideoFramesFrameList;
   filmFrameSrc.inFrameIndex = 0;
   filmFrameSrc.outFrameIndex = dstVideoFramesFrameList->getTotalFrameCount();
   filmFrameSrc.frameLabels = 0;
   CTimecode timecode(0);
   filmFrameSrc.timecode = &timecode;

   CFrame *srcFrame = srcFrameList->getBaseFrame(srcStartFrameIndex);
   if (srcFrame == 0)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   int srcFieldCount = srcFrame->getTotalFieldCount();
   filmFrameSrc.frameSrc.fieldCount = srcFieldCount;
   int minSrcFieldListIndex = INT_MAX;
   for (int fieldIndex = 0; fieldIndex < srcFieldCount; ++fieldIndex)
      {
      CField *srcField = srcFrame->getBaseField(fieldIndex);
      if (srcField == 0)
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN;
      int srcFieldListIndex = srcField->getFieldListEntryIndex();
      if (srcFieldListIndex < minSrcFieldListIndex)
         minSrcFieldListIndex = srcFieldListIndex;
      }

   int offset = minSrcFieldListIndex;
   if (interlaced && (minSrcFieldListIndex & 1) != 0)
      offset -= 1;

   SCopyFieldSource *fieldSrc = filmFrameSrc.frameSrc.fieldSrc;
   int srcEndFrameIndex = srcStartFrameIndex + srcFrameCount - 1;
   for (int srcFrameIndex = srcStartFrameIndex;
        srcFrameIndex < srcEndFrameIndex;
        ++srcFrameIndex)
      {
      srcFrame = srcFrameList->getBaseFrame(srcFrameIndex);
      if (srcFrame == 0)
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

      srcFieldCount = srcFrame->getTotalFieldCount();
      filmFrameSrc.frameSrc.fieldCount = srcFieldCount;
      for (int fieldIndex = 0; fieldIndex < srcFieldCount; ++fieldIndex)
         {
         CField *srcField = srcFrame->getBaseField(fieldIndex);
         if (srcField == 0)
            return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

         int srcFieldListIndex = srcField->getFieldListEntryIndex();
         int newFieldListIndex = srcFieldListIndex - offset;

         if (interlaced)
            {
            fieldSrc[fieldIndex].frameIndex = newFieldListIndex / 2;
            fieldSrc[fieldIndex].fieldIndex = newFieldListIndex & 1; // odd/even
            }
         else
            {
            fieldSrc[fieldIndex].frameIndex = newFieldListIndex;
            fieldSrc[fieldIndex].fieldIndex = 0;
            }
         }


      // Create a new film frame and new fields based on the source video
      // frames' field information
      if (filmFrameSrc.frameSrc.fieldCount > 0)
         {
         // Make a proper CFrame subtype to hold the film frame
         srcFrame->getTimecode(timecode);
         CFrame *newFilmFrame = createFrame(filmFrameSrc.frameSrc.fieldCount,
                                             mediaFormat, timecode);

         // Copy the appropriate fields from the video frame(s)
         // to the new film frame.
         retVal = newFilmFrame->copyFields(filmFrameSrc.frameSrc);
         if (retVal != 0)
            {
            // ERROR: Failed to copy fields from source track
            return retVal;
            }
         }
      }

   return 0;
}


int CFrameList::createFrameListFromDiffFile(const string& diffFileName,
                                             EPulldown pulldown)
{
   int retVal;

   CFrameList *videoFramesFrameList;
   videoFramesFrameList = parentTrack->getBaseFrameList(FRAMING_SELECT_VIDEO);

   int videoFrameCount = videoFramesFrameList->getTotalFrameCount();

   int *diffFrameLabels = 0;
   EFrameLabel *frameLabels = 0;
   CMetaData *metaData = 0;
   try
      {
      // Allocate buffers for frame labels
      diffFrameLabels = new int[videoFrameCount];
      frameLabels = new EFrameLabel[videoFrameCount];
      metaData = new CMetaData(diffFileName);
      }
   catch (...)
      {
      // Memory allocation failed
      delete metaData;
      delete[] frameLabels;
      delete[] diffFrameLabels;
      return -430;
      }

   // Get frame labels from difference (Diff) file
   retVal = metaData->AssignFrameLabels(0, videoFrameCount,
                                        diffFrameLabels, pulldown);
   if (retVal != 0)
      {
      // ERROR: Could not get frame labels
      delete metaData;
      delete[] frameLabels;
      delete[] diffFrameLabels;
      return -431;
      }

   // Convert frame labels type to EFrameLabels
   convertFrameLabels(videoFrameCount, diffFrameLabels, frameLabels);

   retVal = createFilmFrames_FrameList(videoFramesFrameList, pulldown,
                                       frameLabels);
   if (retVal != 0)
      {
      // ERROR: Could not create the film-frames frame list
      delete metaData;
      delete[] frameLabels;
      delete[] diffFrameLabels;
      return -432;
      }

   delete metaData;
   delete[] diffFrameLabels;
   delete [] frameLabels;

   return 0;

} // createFrameListFromDiffFile

int CFrameList::createFrameListFromCadenceFile(const string& cadenceFileName,
                                             bool do525FieldOrder)
{
   int retVal;

   // the original video frame list is the basis of the cadence repaired list
   CFrameList *videoFramesFrameList;
   videoFramesFrameList = parentTrack->getBaseFrameList(FRAMING_SELECT_VIDEO);

   int iNFrame = videoFramesFrameList->getTotalFrameCount();

   // open the Cadence Repair Tool file
   int iFD = open (cadenceFileName.c_str(), O_RDONLY|O_BINARY);
   if (iFD == -1)
     return -1;

   // allocate storage for frame to use
   int *ipFrame0, *ipFrame1;
   ipFrame0 = (int *) malloc (iNFrame * sizeof(int));
   if (ipFrame0 == 0)
    {
     close (iFD);
     return -1;
    }

   ipFrame1 = (int *) malloc (iNFrame * sizeof(int));
   if (ipFrame1 == 0)
    {
     free (ipFrame0);
     close (iFD);
     return -1;
    }

   // skip over the banner
   if (lseek (iFD, 128, SEEK_SET) == -1)
    {
     close (iFD);
     free (ipFrame0);
     free (ipFrame1);
     return -1;
    }

   // skip over the source frame labels
   if (lseek (iFD, sizeof(int) * iNFrame, SEEK_CUR) == -1)
    {
     close (iFD);
     free (ipFrame0);
     free (ipFrame1);
     return -1;
    }

   // read in the frames to use
   if (read (iFD, ipFrame0, iNFrame * sizeof(int)) == -1)
    {
     close (iFD);
     free (ipFrame0);
     free (ipFrame1);
     return -1;
    }

   if (read (iFD, ipFrame1, iNFrame * sizeof(int)) == -1)
    {
     close (iFD);
     free (ipFrame0);
     free (ipFrame1);
     return -1;
    }

   close (iFD);

   int newHandleCount = parentTrack->getHandleCount();

   CMediaFormat* mediaFormat
                  = const_cast<CMediaFormat*>(parentTrack->getMediaFormat());

   // Initialize helper structure SFilmFramesSource, which is used to pass
   // arguments and results of video-frames to film-frames conversion
   SFilmFrameSource filmFrameSrc;
   filmFrameSrc.frameSrc.srcTrack = videoFramesFrameList;
   filmFrameSrc.inFrameIndex = 0;
   filmFrameSrc.outFrameIndex = videoFramesFrameList->getTotalFrameCount();
   filmFrameSrc.frameLabels = 0;
   CTimecode timecode(0);
   filmFrameSrc.timecode = &timecode;


   for (filmFrameSrc.frameIndex = filmFrameSrc.inFrameIndex;
	filmFrameSrc.frameIndex < filmFrameSrc.outFrameIndex;
	filmFrameSrc.frameIndex++)
      {
      CFrame *srcFrame = videoFramesFrameList->getBaseFrame
                        (filmFrameSrc.frameIndex);
      CTimecode timecode = srcFrame->getTimecode();

      filmFrameSrc.frameSrc.fieldCount = srcFrame->getVisibleFieldCount();

      // assign field 0
      filmFrameSrc.frameSrc.fieldSrc[0].frameIndex = ipFrame0
		[filmFrameSrc.frameIndex];
      filmFrameSrc.frameSrc.fieldSrc[0].fieldIndex = 0;

      // assign field 1
      filmFrameSrc.frameSrc.fieldSrc[1].frameIndex = ipFrame1
		[filmFrameSrc.frameIndex];
      filmFrameSrc.frameSrc.fieldSrc[1].fieldIndex = 1;

      // New frame is same as source frame
      CFrame *newFrame = createFrame(filmFrameSrc.frameSrc.fieldCount,
		mediaFormat, timecode);

      // copy the appropriate fields from the video frame
      retVal = newFrame->copyFields(filmFrameSrc.frameSrc);
      if (retVal != 0)
        {
         // ERROR: Failed to copy fields from source track
         free (ipFrame0);
         free (ipFrame1);
         return retVal;
        }
      }


   free (ipFrame0);
   free (ipFrame1);

   inFrameIndex = newHandleCount;
   outFrameIndex = getTotalFrameCount() - newHandleCount;

   frame0Timecode = videoFramesFrameList->getTimecodeForFrameIndex(0);
   inTimecode = videoFramesFrameList->getInTimecode();
   outTimecode = videoFramesFrameList->getOutTimecode();


   return 0;

} // createFrameListFromCadenceFile


void CFrameList::create3_2PulldownFrameLabels(int inFrameIndex,
                                              int outFrameIndex,
                                              int AAFrameIndex,
                                              EPulldown pulldown,
                                              EFrameLabel *frameLabelArray)
{
   EFrameLabel pulldownType;
   switch(pulldown)
      {
      case IF_PULLDOWN_SWAPPED_FIELD_3_2 :
         pulldownType = FRAME_LABEL_aA;
         break;
      case IF_PULLDOWN_FIELD_3_2 :
         pulldownType = FRAME_LABEL_Aa;
         break;
      case IF_PULLDOWN_FRAME_3_2:
         pulldownType = FRAME_LABEL_A0;
         break;
      default :
         return;     // Unknown or invalid pulldown
      }

   // Assign a frame label to each entry in caller's frameLabelArray
   EFrameLabel *frameLabels = frameLabelArray;
   for (int frameIndex = inFrameIndex; frameIndex < outFrameIndex; ++frameIndex)
      {
      *frameLabels++ = (EFrameLabel)(pulldownType
                          + Get3_2PulldownFrameLabel(frameIndex, AAFrameIndex));
      }

/* TTT OLD WAY
   // Determine the position in the 3:2 Pulldown sequence
   // of the "in" frame
   int pulldownOffset = (inFrameIndex - AAFrameIndex) % 5;
   if (AAFrameIndex > inFrameIndex)
      {
      pulldownOffset += 5;
      }

   EFrameLabel *frameLabels = frameLabelArray;

   EFrameLabel pulldownType;
   switch(pulldown)
      {
      case IF_PULLDOWN_SWAPPED_FIELD_3_2 :
         pulldownType = FRAME_LABEL_aA;
         break;
      case IF_PULLDOWN_FIELD_3_2 :
         pulldownType = FRAME_LABEL_Aa;
         break;
      case IF_PULLDOWN_FRAME_3_2:
         pulldownType = FRAME_LABEL_A0;
         break;
      default :
         return;     // Unknown or invalid pulldown
      }

   // Assign a frame label to each entry in caller's frameLabelArray
   for (int frameIndex = inFrameIndex; frameIndex < outFrameIndex; ++frameIndex)
      {
      // Generate a number between 0 and 4 which corresponds to the
      // pulldown sequence position
      int pulldownSequencePosition = (frameIndex + pulldownOffset) % 5;

      *frameLabels++ = (EFrameLabel)(pulldownType + pulldownSequencePosition);
      }
*/
} // create3_2FieldPulldownFrameLabels

void CFrameList::convertFrameLabels(int labelCount, int iFrameLabelArray[],
                                     EFrameLabel eFrameLabelArray[])
{
   int iFrameLabel;
   EFrameLabel eFrameLabel;

   // Frame labels returned from CMetaData::AssignFrameLabels function are
   // bit flags FL_Aa, FL_Bb, etc.  Convert these to EFrameLabel enum
   // values FRAME_LABEL_Aa, FRAME_LABEL_Bb, etc.

   for (int i = 0; i < labelCount; ++i)
      {
      iFrameLabel = iFrameLabelArray[i];

      if ((iFrameLabel & FL_Aa) == FL_Aa)             // Interlaced, non-525
         eFrameLabel = FRAME_LABEL_Aa;

      else if ((iFrameLabel & FL_Bb) == FL_Bb)
         eFrameLabel = FRAME_LABEL_Bb;

      else if ((iFrameLabel & FL_Bc) == FL_Bc)
         eFrameLabel = FRAME_LABEL_Bc;

      else if ((iFrameLabel & FL_Cd) == FL_Cd)
         eFrameLabel = FRAME_LABEL_Cd;

      else if ((iFrameLabel & FL_Dd) == FL_Dd)
         eFrameLabel = FRAME_LABEL_Dd;

      if ((iFrameLabel & FL_aA) == FL_aA)             // Interlaced, 525
         eFrameLabel = FRAME_LABEL_aA;                // swapped fields

      else if ((iFrameLabel & FL_bB) == FL_bB)
         eFrameLabel = FRAME_LABEL_bB;

      else if ((iFrameLabel & FL_cB) == FL_cB)
         eFrameLabel = FRAME_LABEL_cB;

      else if ((iFrameLabel & FL_dC) == FL_dC)
         eFrameLabel = FRAME_LABEL_dC;

      else if ((iFrameLabel & FL_dD) == FL_dD)
         eFrameLabel = FRAME_LABEL_dD;

      else if ((iFrameLabel & FL_A0) == FL_A0)        // 720P
         eFrameLabel = FRAME_LABEL_A0;

      else if ((iFrameLabel & FL_A1) == FL_A1)
         eFrameLabel = FRAME_LABEL_A1;

      else if ((iFrameLabel & FL_B0) == FL_B0)
         eFrameLabel = FRAME_LABEL_B0;

      else if ((iFrameLabel & FL_B1) == FL_B1)
         eFrameLabel = FRAME_LABEL_B1;

      else if ((iFrameLabel & FL_B2) == FL_B2)
         eFrameLabel = FRAME_LABEL_B2;

      // Put converted label in caller's output array
      eFrameLabelArray[i] = eFrameLabel;
      }

} // convertFrameLabels

void CFrameList::remove3_2SwappedFieldPulldown(SFilmFrameSource& filmFrameSrc)
{
   int labelIndex = filmFrameSrc.frameIndex - filmFrameSrc.inFrameIndex;
   SCopyFieldSource *fieldSrc = filmFrameSrc.frameSrc.fieldSrc;
   EFrameLabel *frameLabels = filmFrameSrc.frameLabels;

   // Determine how a new frame will be created from one or two Video-
   // Frames with 3:2 pull-down, depending on the position in the 5-frame
   // 3:2 pulldown sequence of aA, bB, cB, dC and dD frames
   switch (frameLabels[labelIndex])
      {
      case FRAME_LABEL_aA :     // aA Video Frame
         // Create aA frame from aA video frame

         // Visible field "a"  from source frame "aA"
         fieldSrc[0].frameIndex = filmFrameSrc.frameIndex;
         fieldSrc[0].fieldIndex = 0;
         // Visible field "A" from source frame "aA"
         fieldSrc[1].frameIndex = filmFrameSrc.frameIndex;
         fieldSrc[1].fieldIndex = 1;

         filmFrameSrc.frameSrc.fieldCount = 2;
         break;

      case FRAME_LABEL_bB :    // bB Video Frame
         // Create bB Film Frame
         // Visible field "b" from video-frame "bB"
         fieldSrc[0].frameIndex = filmFrameSrc.frameIndex;
         fieldSrc[0].fieldIndex = 0;
         // Visible field "B" from video-frame "bB"
         fieldSrc[1].frameIndex = filmFrameSrc.frameIndex;
         fieldSrc[1].fieldIndex = 1;

         filmFrameSrc.frameSrc.fieldCount = 2;

         if ((filmFrameSrc.frameIndex + 1) < filmFrameSrc.outFrameIndex
             && frameLabels[labelIndex+1] == FRAME_LABEL_cB)
            {
            // Create bBB film frame from bB and cB video frames
            // Invisible field "B" from video-frame "cB"
            fieldSrc[2].frameIndex = filmFrameSrc.frameIndex + 1;
            fieldSrc[2].fieldIndex = 1;
            fieldSrc[2].visibleSiblingIndex = 1;

            filmFrameSrc.frameSrc.fieldCount++;
            }
         break;

       case FRAME_LABEL_cB :     // cB Video Frame
         if ((filmFrameSrc.frameIndex + 1) < filmFrameSrc.outFrameIndex
             && frameLabels[labelIndex+1] == FRAME_LABEL_dC)
            {
            // Create cC film frame from cB and dC video frames
            // Visible field "c" from video-frame "cB"
            fieldSrc[0].frameIndex = filmFrameSrc.frameIndex;
            fieldSrc[0].fieldIndex = 0;
            // Visible field "C" from video-frame "dC"
            fieldSrc[1].frameIndex = filmFrameSrc.frameIndex + 1;
            fieldSrc[1].fieldIndex = 1;

            filmFrameSrc.frameSrc.fieldCount = 2;
            // Timecode from cB video frame
            // Set half bit in timecode, since the new frame was created
            // from equal parts of two video-frames
            filmFrameSrc.timecode->setHalfFrame(true);
            }
         else
            {
            // Cannot create cC frame because a dC frame is not available
            filmFrameSrc.frameSrc.fieldCount = 0;
            }
         break;

      case FRAME_LABEL_dC :     // dC Video Frame
         // Do nothing.  Fields in dC Video Frame are used to make
         // cC and dDd Film Frames
         filmFrameSrc.frameSrc.fieldCount = 0;
         break;

      case FRAME_LABEL_dD :     // dD Video Frame
         // Create Dd Film Frame
         // Visible field "d" from video-frame "dD"
         fieldSrc[0].frameIndex = filmFrameSrc.frameIndex;
         fieldSrc[0].fieldIndex = 0;
         // Visible field "D" from video-frame "dD"
         fieldSrc[1].frameIndex = filmFrameSrc.frameIndex;
         fieldSrc[1].fieldIndex = 1;

         filmFrameSrc.frameSrc.fieldCount = 2;

         if ((filmFrameSrc.frameIndex - 1) >= filmFrameSrc.inFrameIndex
             && frameLabels[labelIndex-1] == FRAME_LABEL_dC)
            {
            // Create dDd film frame from dD and previous dC video frames
            // Third invisible field

            // Invisible field "d" from video-field "dC"
            fieldSrc[2].frameIndex = filmFrameSrc.frameIndex - 1;
            fieldSrc[2].fieldIndex = 0;
            fieldSrc[2].visibleSiblingIndex = 0;

            filmFrameSrc.frameSrc.fieldCount++;
            }
		 break;

		 default:
		 	break;
      }

} // remove3_2SwappedFieldPulldown

void CFrameList::remove3_2FieldPulldown(SFilmFrameSource& filmFrameSrc)
{
   int labelIndex = filmFrameSrc.frameIndex - filmFrameSrc.inFrameIndex;
   SCopyFieldSource *fieldSrc = filmFrameSrc.frameSrc.fieldSrc;
   EFrameLabel *frameLabels = filmFrameSrc.frameLabels;

   // Determine how a new frame will be created from one or two Video-
   // Frames with 3:2 pull-down, depending on the position in the 5-frame
   // 3:2 pulldown sequence of Aa, Bb, Bc, Cd and Dd frames
   switch (frameLabels[labelIndex])
      {
      case FRAME_LABEL_Aa :     // Aa Video Frame
         // Create Aa frame from Aa video frames

         // Visible field "A" from source frame "Aa"
         fieldSrc[0].frameIndex = filmFrameSrc.frameIndex;
         fieldSrc[0].fieldIndex = 0;
         // Visible field "a"  from source frame "Aa"
         fieldSrc[1].frameIndex = filmFrameSrc.frameIndex;
         fieldSrc[1].fieldIndex = 1;

         filmFrameSrc.frameSrc.fieldCount = 2;
         break;

      case FRAME_LABEL_Bb :    // Bb Video Frame
         // Create Bb Film Frame
         // Visible field "B" from video-frame "Bb"
         fieldSrc[0].frameIndex = filmFrameSrc.frameIndex;
         fieldSrc[0].fieldIndex = 0;
         // Visible field "b" from video-frame "Bb"
         fieldSrc[1].frameIndex = filmFrameSrc.frameIndex;
         fieldSrc[1].fieldIndex = 1;

         filmFrameSrc.frameSrc.fieldCount = 2;

         if ((filmFrameSrc.frameIndex + 1) < filmFrameSrc.outFrameIndex
             && frameLabels[labelIndex+1] == FRAME_LABEL_Bc)
            {
            // Create BbB film frame from Bb and Bc video frames
            // Invisible field "B" from video-frame "Bc"
            fieldSrc[2].frameIndex = filmFrameSrc.frameIndex + 1;
            fieldSrc[2].fieldIndex = 0;
            fieldSrc[2].visibleSiblingIndex = 0;

            filmFrameSrc.frameSrc.fieldCount++;
            }
         break;

       case FRAME_LABEL_Bc :     // Bc Video Frame
         if ((filmFrameSrc.frameIndex + 1) < filmFrameSrc.outFrameIndex
             && frameLabels[labelIndex+1] == FRAME_LABEL_Cd)
            {
            // Create Cc film frame from Bc and Cd video frames
            // Visible field "C" from video-frame "Cd"
            fieldSrc[0].frameIndex = filmFrameSrc.frameIndex + 1;
            fieldSrc[0].fieldIndex = 0;
            // Visible field "c" from video-frame "Bc"
            fieldSrc[1].frameIndex = filmFrameSrc.frameIndex;
            fieldSrc[1].fieldIndex = 1;
            filmFrameSrc.frameSrc.fieldCount = 2;
            // Timecode from Bc video frame
            // Set half bit in timecode, since the new frame was created
            // from equal parts of two video-frames
            filmFrameSrc.timecode->setHalfFrame(true);
            }
         else
            {
            // Cannot create Cc frame because a Cd frame is not available
            filmFrameSrc.frameSrc.fieldCount = 0;
            }
         break;

      case FRAME_LABEL_Cd :     // Cd Video Frame
         // Do nothing.  Fields in Cd Video Frame are used to make
         // Cc and Ddd Film Frames
         filmFrameSrc.frameSrc.fieldCount = 0;
         break;

      case FRAME_LABEL_Dd :     // Dd Video Frame
         // Create Dd Film Frame
         // Visible field "D" from video-frame "Dd"
         fieldSrc[0].frameIndex = filmFrameSrc.frameIndex;
         fieldSrc[0].fieldIndex = 0;
         // Visible field "d" from video-frame "Dd"
         fieldSrc[1].frameIndex = filmFrameSrc.frameIndex;
         fieldSrc[1].fieldIndex = 1;

         filmFrameSrc.frameSrc.fieldCount = 2;

         if ((filmFrameSrc.frameIndex - 1) >= filmFrameSrc.inFrameIndex
             && frameLabels[labelIndex-1] == FRAME_LABEL_Cd)
            {
            // Create Ddd film frame from Dd and previous Cd video frames
            // Third invisible field

            // Invisible field "d" from video-field "Cd"
            fieldSrc[2].frameIndex = filmFrameSrc.frameIndex - 1;
            fieldSrc[2].fieldIndex = 1;
            fieldSrc[2].visibleSiblingIndex = 1;

            filmFrameSrc.frameSrc.fieldCount++;
            }
		 break;

	  default:
		break;
      }

} // remove3_2FieldPulldown

void CFrameList::remove3_2FramePulldown(SFilmFrameSource& filmFrameSrc)
{
   int labelIndex = filmFrameSrc.frameIndex - filmFrameSrc.inFrameIndex;
   SCopyFieldSource *fieldSrc = filmFrameSrc.frameSrc.fieldSrc;
   EFrameLabel *frameLabels = filmFrameSrc.frameLabels;

   // Determine how a new frame will be created from one, two or three Video-
   // Frames with 3:2 pull-down, depending on the position in the 5-frame
   // 3:2 pulldown sequence of A0, A1, B0, B1 and B2 frames
   switch (frameLabels[labelIndex])
      {
      case FRAME_LABEL_A0 :     // A0 Video Frame
         // Create A0A1 frame from A0 & A1 video frames, nominally
         // with one visible field and one invisible field

         // Visible field "A0" from source frame "A0"
         fieldSrc[0].frameIndex = filmFrameSrc.frameIndex;
         fieldSrc[0].fieldIndex = 0;

         if ((filmFrameSrc.frameIndex + 1) < filmFrameSrc.outFrameIndex
             && frameLabels[labelIndex+1] == FRAME_LABEL_A1)
            {
            // Invisible Field "A1"  from source frame "A1"
            fieldSrc[1].frameIndex = filmFrameSrc.frameIndex + 1;
            fieldSrc[1].fieldIndex = 0;
            fieldSrc[1].visibleSiblingIndex = 0;

            filmFrameSrc.frameSrc.fieldCount = 2;
            }
         else
            {
            // Source Frame A1 is not available, so no invisible fields
            filmFrameSrc.frameSrc.fieldCount = 1;
            }
         break;

      case FRAME_LABEL_A1:     // A1 Video Frame
         // This frame has already been used to construct A0A1 frame
         // so it is ignored now
         filmFrameSrc.frameSrc.fieldCount = 0;
         break;

      case FRAME_LABEL_B0 :     // B0 Video Frame
         // Create B0B1B2 frame from B0, B1 & B2 video frames, nominally
         // with one visible field and two invisible fields

         // Visible field "B0" from source frame "B0"
         fieldSrc[0].frameIndex = filmFrameSrc.frameIndex;
         fieldSrc[0].fieldIndex = 0;
         filmFrameSrc.frameSrc.fieldCount = 1;

         if ((filmFrameSrc.frameIndex + 1) < filmFrameSrc.outFrameIndex
             && frameLabels[labelIndex+1] == FRAME_LABEL_B1)
            {
            // Invisible field "B1"  from source frame "B1"
            fieldSrc[1].frameIndex = filmFrameSrc.frameIndex + 1;
            fieldSrc[1].fieldIndex = 0;
            fieldSrc[1].visibleSiblingIndex = 0;

            filmFrameSrc.frameSrc.fieldCount++;

            if ((filmFrameSrc.frameIndex + 2) < filmFrameSrc.outFrameIndex
                && frameLabels[labelIndex+2] == FRAME_LABEL_B2)
               {
               // Invisible field "B2"  from source frame "B2"
               fieldSrc[2].frameIndex = filmFrameSrc.frameIndex + 2;
               fieldSrc[2].fieldIndex = 0;
               fieldSrc[2].visibleSiblingIndex = 0;

               filmFrameSrc.frameSrc.fieldCount++;
               }
            }
         break;

      case FRAME_LABEL_B1:     // B1 Video Frame
      case FRAME_LABEL_B2:     // B2 Video Frame
         // These frames have already been used to construct B0B1B2 frame
         // so they are ignored now
         filmFrameSrc.frameSrc.fieldCount = 0;
		 break;

	  default:
	  	break;
      }

} // remove3_2FramePulldown

void CFrameList::fixPALFieldDominance(SFilmFrameSource& filmFrameSrc)
{
   SCopyFieldSource *fieldSrc = filmFrameSrc.frameSrc.fieldSrc;

   // Determine how the new frame will be created from two fields
   // from two source frames.  Basically if the two source frames
   // are Aa Bb, then the output frame will be Ba

   if ((filmFrameSrc.frameIndex + 1) < filmFrameSrc.outFrameIndex)
      {
      // Visible field "B" from source frame "Bb"
      fieldSrc[0].frameIndex = filmFrameSrc.frameIndex + 1;
      fieldSrc[0].fieldIndex = 0;
      }
   else
      {
      // Next source frame is not available, so just dup the field
      fieldSrc[0].frameIndex = filmFrameSrc.frameIndex;
      fieldSrc[0].fieldIndex = 1;
      }

   // Visible field "a"  from source frame "Aa"
   fieldSrc[1].frameIndex = filmFrameSrc.frameIndex;
   fieldSrc[1].fieldIndex = 1;

   filmFrameSrc.frameSrc.fieldCount = 2;
}

int CFrameList::createVideoFields_FrameList(CFrameList *videoFramesFrameList,
                                            bool do525FieldOrder)
{
   int retVal;

   // Get Framing Type, which will be FRAMING_TYPE_FIELDS_1_2,
   // FRAMING_TYPE_FIELD_1_ONLY or FRAMING_TYPE_FIELD_2_ONLY.
   int framingType = getFramingType();

   int newHandleCount = parentTrack->getHandleCount();
   if (framingType == FRAMING_TYPE_FIELDS_1_2)
      newHandleCount *= 2; // Double number of handles

   CMediaFormat* mediaFormat
                  = const_cast<CMediaFormat*>(parentTrack->getMediaFormat());

   // Determine which fields to include
   bool includeField1 = (framingType == FRAMING_TYPE_FIELDS_1_2
                         || framingType == FRAMING_TYPE_FIELD_1_ONLY);
   bool includeField2 = (framingType == FRAMING_TYPE_FIELDS_1_2
                         || framingType == FRAMING_TYPE_FIELD_2_ONLY);

   // Determine the source field indices (0 or 1) of the destination's
   // "Field 1" and "Field 2"
   int field1Index, field2Index;
   if (do525FieldOrder && framingType == FRAMING_TYPE_FIELDS_1_2)
      {
      // Special field order when source is 525 and the new frame list
      // holds 1, 2 field sequence.  525's temporal odd/even field sequence is
      // reverse of all other formats in the known universe.
      field1Index = 1;
      field2Index = 0;
      }
   else
      {
      // Natural field order as in source frame.
      field1Index = 0;
      field2Index = 1;
      }

   int srcFrameCount = videoFramesFrameList->getTotalFrameCount();
   for (int frameIndex = 0; frameIndex < srcFrameCount; ++frameIndex)
      {
      CFrame *srcFrame = videoFramesFrameList->getBaseFrame(frameIndex);
      CTimecode timecode = srcFrame->getTimecode();

      if (includeField1)
         {
         // New frame from doubled field 1 of source frame
         timecode.setField(false);
         CFrame *newFrame = createFrame(2, mediaFormat, timecode);
         retVal = newFrame->doubleField(*srcFrame, field1Index);
         if (retVal != 0)
            return retVal;
         }
      if (includeField2)
         {
         // New frame from doubled field 2 of source frame
         timecode.setField(true);
         CFrame *newFrame = createFrame(2, mediaFormat, timecode);
         retVal = newFrame->doubleField(*srcFrame, field2Index);
         if (retVal != 0)
            return retVal;
         }
      }

   inFrameIndex = newHandleCount;
   outFrameIndex = getTotalFrameCount() - newHandleCount;

   frame0Timecode = videoFramesFrameList->getTimecodeForFrameIndex(0);
   inTimecode = videoFramesFrameList->getInTimecode();
   outTimecode = videoFramesFrameList->getOutTimecode();

   return 0;

} // createVideoFields_FrameList

int CFrameList::createVideoFields_FrameList(bool do525FieldOrder)
{
   int retVal;

   CFrameList *videoFramesFrameList;
   videoFramesFrameList = parentTrack->getBaseFrameList(FRAMING_SELECT_VIDEO);

   retVal = createVideoFields_FrameList(videoFramesFrameList, do525FieldOrder);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CFrameList::copyVideoFields_FrameList(CFrameList *videoFramesFrameList)
{
   int retVal;

   int newHandleCount = parentTrack->getHandleCount();

   CMediaFormat* mediaFormat
                  = const_cast<CMediaFormat*>(parentTrack->getMediaFormat());

   // Initialize helper structure SFilmFramesSource, which is used to pass
   // arguments and results of video-frames to film-frames conversion
   SFilmFrameSource filmFrameSrc;
   filmFrameSrc.frameSrc.srcTrack = videoFramesFrameList;
   filmFrameSrc.inFrameIndex = 0;
   filmFrameSrc.outFrameIndex = videoFramesFrameList->getTotalFrameCount();
   filmFrameSrc.frameLabels = 0;
   CTimecode timecode(0);
   filmFrameSrc.timecode = &timecode;

   for (filmFrameSrc.frameIndex = filmFrameSrc.inFrameIndex;
	filmFrameSrc.frameIndex < filmFrameSrc.outFrameIndex;
	filmFrameSrc.frameIndex++)
      {
      CFrame *srcFrame = videoFramesFrameList->getBaseFrame
                        (filmFrameSrc.frameIndex);
      CTimecode timecode = srcFrame->getTimecode();

      filmFrameSrc.frameSrc.fieldCount = srcFrame->getVisibleFieldCount();

      for (int i = 0; i < filmFrameSrc.frameSrc.fieldCount; i++)
       {
        filmFrameSrc.frameSrc.fieldSrc[i].frameIndex = filmFrameSrc.frameIndex;
        filmFrameSrc.frameSrc.fieldSrc[i].fieldIndex = i;
       }

      // New frame is same as source frame
      CFrame *newFrame = createFrame(filmFrameSrc.frameSrc.fieldCount,
		mediaFormat, timecode);

      // copy the appropriate fields from the video frame
      retVal = newFrame->copyFields(filmFrameSrc.frameSrc);
      if (retVal != 0)
        {
         // ERROR: Failed to copy fields from source track
         return retVal;
        }
      }

   inFrameIndex = newHandleCount;
   outFrameIndex = getTotalFrameCount() - newHandleCount;

   frame0Timecode = videoFramesFrameList->getTimecodeForFrameIndex(0);
   inTimecode = videoFramesFrameList->getInTimecode();
   outTimecode = videoFramesFrameList->getOutTimecode();

   return 0;

} // copyVideoFields_FrameList

int CFrameList::copyVideoFields_FrameList()
{
   int retVal;

   CFrameList *videoFramesFrameList;
   videoFramesFrameList = parentTrack->getBaseFrameList(FRAMING_SELECT_VIDEO);

   retVal = copyVideoFields_FrameList(videoFramesFrameList);
   if (retVal != 0)
      return retVal;

   return 0;
}


int CFrameList::initVideoFields_FrameList()
{
   // Initialize this Video Fields CFrameList based on the
   // the Video-Frames frame list

   CFrameList *videoFramesFrameList;
   videoFramesFrameList = parentTrack->getBaseFrameList(FRAMING_SELECT_VIDEO);

   // Get Framing Type, which will be FRAMING_TYPE_FIELDS_1_2,
   // FRAMING_TYPE_FIELD_1_ONLY or FRAMING_TYPE_FIELD_2_ONLY.
   int framingType = getFramingType();

   inTimecode = videoFramesFrameList->getInTimecode();
   outTimecode = videoFramesFrameList->getOutTimecode();

   int newUserFrameCount = outTimecode - inTimecode;

   int newHandleCount = parentTrack->getHandleCount();
   if (framingType == FRAMING_TYPE_FIELDS_1_2)
      {
      newHandleCount *= 2; // Double number of handles
      newUserFrameCount *= 2;
      }

   inFrameIndex = newHandleCount;
   outFrameIndex = newUserFrameCount + newHandleCount;

   frame0Timecode = videoFramesFrameList->getTimecodeForFrameIndex(0);

   return 0;

} // createVideoFields_FrameList

//////////////////////////////////////////////////////////////////////
// Miscellaneous Functions
//////////////////////////////////////////////////////////////////////

void CFrameList::deleteFrameList()
{
   FrameList::iterator frame;

   // Iterate through frame list, deleting each frame in list
   for (frame = frameList.begin(); frame < frameList.end(); ++frame)
      {
      delete *frame;
      }

   // Clear all elements from the frame list
   frameList.clear();

   frameListAvailable = false;
}

int CFrameList::truncateFrameList(int startFrame)
{
   // Remove the frames in the frame list from startFrame to the
   // end of the frame list

   if (!isFrameIndexValid(startFrame))
      return CLIP_ERROR_INVALID_FRAME_NUMBER;

   FrameList::iterator beginFrame = frameList.begin() + startFrame;
   FrameList::iterator endFrame = frameList.end();

   // Iterate through frame list, deleting each frame in list.  This
   // will also delete any of the fields owned by the frame
   for (FrameList::iterator frame = beginFrame; frame < endFrame; ++frame)
      {
      delete *frame;
      }

   // Remove the frame pointers from the frame list in the range
   // of [beginFrame, endFrame)
   frameList.erase(beginFrame, endFrame);

   return 0;

} // truncateFrameList

// Add frame to frame list
void CFrameList::addFrame(CFrame* frame)
{
   // Add a frame to the frame list
   frameList.push_back(frame);

   frameListAvailable = true;
}

// Reserve memory in frame list for the expected number of frames
void CFrameList::frameListReserve(MTI_UINT32 size)
{
   if (getClipVer() == CLIP_VERSION_5L)
      return;  // No action for clip 5

   frameList.reserve(size);
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

bool CFrameList::isFileNewerThanFrameListFile(const string& fileName) const
{
   struct stat fileStatBuf;
   struct stat frameListFileStatBuf;

   if (stat(fileName.c_str(), &fileStatBuf) != 0)
      {
      // Can't get stat for the caller's file, maybe it doesn't exist.
      // Whatever the reason, assume it isn't newer than the track file
      return false;
      }

   if (stat(getFrameListFileNameWithExt().c_str(), &frameListFileStatBuf) != 0)
      {
      // Can't get stat for the track file, maybe it doesn't exit
      // So assume the caller's file is newer.
      return true;
      }

   // Compare the "modified time" of the two files
   // Since the granularity of the time stamp is 1 second, we use
   // greater than or equal to be certain we detect the newest file.
   if (fileStatBuf.st_mtime >= frameListFileStatBuf.st_mtime)
      return true;

   return false;
}

int CFrameList::FileDateCompare(const string& fileName0, const string &fileName1) const
/*
	Return 1 if fileName0 is newer than fileName1
	Return -1 if fileName1 is newer than fileName0
	Return 0 in other cases
*/
{
   struct stat fileStatBuf0, fileStatBuf1;

   if (stat(fileName0.c_str(), &fileStatBuf0) != 0)
      {
      // Can't get stat for the caller's file, maybe it doesn't exist.
      // Whatever the reason, return 0
      return 0;
      }

   if (stat(fileName1.c_str(), &fileStatBuf1) != 0)
      {
      // Can't get stat for the caller's file, maybe it doesn't exist.
      // Whatever the reason, return 0
      return 0;
      }

   // Compare the "modified time" of the two files
   // The granularity of the time stamp is 1 second, so this is enough
   // to detect changes caused by the cadence repair tool.
   if (fileStatBuf0.st_mtime > fileStatBuf1.st_mtime)
      return 1;
   else if (fileStatBuf0.st_mtime < fileStatBuf1.st_mtime)
      return -1;

   return 0;
}


bool CFrameList::isFrameListFileWritable() const
{
   // Returns true if frame list file is writable.
   // Returns false if frame list file is either not writable or does
   // not exist

   return (DoesWritableFileExist(getFrameListFileNameWithExt()));
}

void CFrameList::doDPXHeaderTimecodeHack(CFrame *frame)
{
   // This hack assumes all fields in the frame are from the same media
   // storage location (i.e. the media locations all share the same media
   // access).
   CField *field = frame->getBaseField(0);
   if (field != NULL)
      {
      const CMediaLocation& mediaLocation = field->getMediaLocation();
      CMediaAccess *mediaAccess = mediaLocation.getMediaAccess();
      if (mediaAccess != NULL)
         mediaAccess->setFrame0Timecode(getTimecodeForFrameIndex(0));
      }
}

string CFrameList::getImageFilePath(CFrame *frame)
{
   string imageFilePath;

   // This hack assumes all fields in the frame are from the same media
   // storage location (i.e. the media locations all share the same media
   // access).
   CField *field = frame->getBaseField(0);
   if (field != NULL)
		{
      const CMediaLocation& mediaLocation = field->getMediaLocation();
      CMediaAccess *mediaAccess = mediaLocation.getMediaAccess();
      if (mediaAccess != NULL)
         {
			EMediaType mediaType = mediaAccess->getMediaType();

         // returns "" if not using image files
         if (CMediaInterface::isMediaTypeImageFile(mediaType))
            {
            imageFilePath = mediaLocation.getImageFilePath();
            }
         }
      }

   return imageFilePath;
}

string CFrameList::getHistoryFilePath(CFrame *frame)
{
   string historyFilePath;

   // This hack assumes all fields in the frame are from the same media
   // storage location (i.e. the media locations all share the same media
   // access).
   CField *field = frame->getBaseField(0);
   if (field != NULL)
      {
      const CMediaLocation& mediaLocation = field->getMediaLocation();
      CMediaAccess *mediaAccess = mediaLocation.getMediaAccess();
      if (mediaAccess != NULL)
         {
         EMediaType mediaType = mediaAccess->getMediaType();
         // returns "" if not using image files
         if (CMediaInterface::isMediaTypeImageFile(mediaType))
            {
            historyFilePath = mediaLocation.getHistoryFilePath();
            }
         }
      }

   return historyFilePath;
}

string CFrameList::getMetadataDirectory()
{
   string metadataDirectory;

   auto frame = getBaseFrame(0);
   auto historyFilePath = getHistoryFilePath(frame);
   auto historyDirectory = GetFilePath(historyFilePath);
   metadataDirectory = GetFilePath(historyDirectory);

   return metadataDirectory;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////
int CFrameList::assignDirtyMediaStorage(
                               int frameNumber,
                               DirtyMediaAssignmentUndoInfo &undoInfo)
{
   return parentTrack->assignDirtyMediaStorage(*this, frameNumber,
                                               undoInfo);
}

int CFrameList::unassignDirtyMediaStorage(
                               int frameNumber,
                               DirtyMediaAssignmentUndoInfo &undoInfo)
{
   return parentTrack->unassignDirtyMediaStorage(*this, frameNumber,
                                                 undoInfo);
}


//////////////////////////////////////////////////////////////////////
// Clip IniFile Interface
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    readFrameListInfoSection
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
int CFrameList::readFrameListInfoSection(CIniFile *iniFile,
                                          const string& sectionPrefix,
                                          const CTimecode& timecodePrototype)
{
   // Construct Frame List section name, something like:
   // "VideoProxyInfo0\FrameListInfo1"
   MTIostringstream ostr;
   ostr << sectionPrefix << framingInfo->getFramingNumber();
   string sectionName = ostr.str();

   if (!iniFile->SectionExists(sectionName))
      {
      // ERROR: Track section does not exist
      return -490;
      }

   // Verify that the appropriate keys exist within the section
   // These keys are not optional and do not have default values
   if (   !iniFile->KeyExists(sectionName, frame0TimecodeKey)
       || !iniFile->KeyExists(sectionName, inTimecodeKey)
       || !iniFile->KeyExists(sectionName, outTimecodeKey)
       || !iniFile->KeyExists(sectionName, inFrameIndexKey)
       || !iniFile->KeyExists(sectionName, outFrameIndexKey)
       )
      {
      // ERROR: Track section key does not exist
      return -491;
      }

   string emptyString, valueString, defaultValue;
   string timeString;

   // Read "Frame 0" Timecode
   timeString = iniFile->ReadString(sectionName, frame0TimecodeKey,
                                    emptyString);
   CTimecode newFrame0Timecode = timecodePrototype;
   if (!newFrame0Timecode.setTimeASCII(timeString.c_str()))
      {
      // ERROR: Could not parse the Frame 0 timecode string
      return -492;
      }

   // Read In Timecode
   timeString = iniFile->ReadString(sectionName, inTimecodeKey, emptyString);
   CTimecode newInTimecode = timecodePrototype;
   if (!newInTimecode.setTimeASCII(timeString.c_str()))
      {
      // ERROR: Could not parse the In timecode string
      return -493;
      }

   // Read Out Timecode
   timeString = iniFile->ReadString(sectionName, outTimecodeKey, emptyString);
   CTimecode newOutTimecode = timecodePrototype;
   if (!newOutTimecode.setTimeASCII(timeString.c_str()))
      {
      // ERROR: Could not parse the Out timecode string
      return -494;
      }

   // Read In Frame Index
   int newInFrameIndex = iniFile->ReadInteger(sectionName, inFrameIndexKey, -1);
   if (newInFrameIndex < 0)
      {
      // ERROR: Unexpected In Frame Index.
      return -495;
      }

   // Read In Frame Index
   int newOutFrameIndex = iniFile->ReadInteger(sectionName, outFrameIndexKey,
                                               -1);
   if (newOutFrameIndex < 0)
      {
      // ERROR: Unexpected Out Frame Index.
      return -496;
      }

   int newMediaAllocatedFrameCount =
       iniFile->ReadInteger(sectionName, mediaAllocatedFrameCountKey, -1);

   // Set new values in *this
   frame0Timecode = newFrame0Timecode;
   inTimecode = newInTimecode;
   outTimecode = newOutTimecode;
   inFrameIndex = newInFrameIndex;
   outFrameIndex = newOutFrameIndex;
   mediaAllocatedFrameCount = newMediaAllocatedFrameCount;
   mediaAllocatedFrontier =
      (mediaAllocatedFrameCount >= 0) ? mediaAllocatedFrameCount - 1 : -1;

   return 0;

} // readFrameListInfoSection( )

//------------------------------------------------------------------------
//
// Function:    writeFrameListInfoSection
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
int CFrameList::writeFrameListInfoSection(CIniFile *iniFile,
                                           const string& sectionPrefix)
{
   // Construct Frame List Info section name, something like:
   // "VideoProxyInfo0\FrameListInfo1"
   MTIostringstream ostr;
   ostr << sectionPrefix << framingInfo->getFramingNumber();
   string sectionName = ostr.str();

   char tmpStr[100];
   string timeString;

   // Write "Frame 0" Timecode
   frame0Timecode.getTimeASCII(tmpStr);
   timeString = tmpStr;
   iniFile->WriteString(sectionName, frame0TimecodeKey, timeString);

   // Write In Timecode
   inTimecode.getTimeASCII(tmpStr);
   timeString = tmpStr;
   iniFile->WriteString(sectionName, inTimecodeKey, timeString);

   // Write Out Timecode
   outTimecode.getTimeASCII(tmpStr);
   timeString = tmpStr;
   iniFile->WriteString(sectionName, outTimecodeKey, timeString);

   // Write In Frame Index
   iniFile->WriteInteger(sectionName, inFrameIndexKey, inFrameIndex);

   // Write Out Frame Index
   iniFile->WriteInteger(sectionName, outFrameIndexKey, outFrameIndex);

   // Write Media Reserve Duration
   iniFile->WriteInteger(sectionName, mediaAllocatedFrameCountKey,
                         mediaAllocatedFrameCount);

   return 0;

} // writeFrameListInfoSection

//////////////////////////////////////////////////////////////////////
// Testing Functions
//////////////////////////////////////////////////////////////////////

void CFrameList::dump(MTIostringstream &str)
{
   str << "Track Header" << std::endl;

   str << "  Description: <" << getDescription() << ">" << std::endl;

   str << "  In Timecode: " << getInTimecode()
//       << "  Out Timecode: " << getInTimecode() + getFrameCount() << std::endl
//       << "  Frame Count: " << getFrameCount()
       << "  Frame List Count: " << frameList.size() << std::endl;

   FrameList::iterator beginFrame = frameList.begin();
   FrameList::iterator endFrame = frameList.end();
   for (FrameList::iterator frame = beginFrame; frame < endFrame; ++frame)
      {
      str << "====================================" << std::endl;
      (*frame)->dump(str);
      }
      str << "====================================" << std::endl;
}

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                        #######################################
// ###    CTrack Class     ######################################
// ####                        #######################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTrack::CTrack(ClipSharedPtr &newParentClip, EClipVersion newClipVer,
               int newTrackNumber)
: clipVer(newClipVer),
  _parentClip(newParentClip), trackNumber(newTrackNumber),
  handleCount(0),
  mediaFormat(0),
  recordToComputerStatus(RECORD_STATUS_NOT_RECORDED),
  recordToVTRStatus(RECORD_STATUS_NOT_RECORDED),
  lastMediaAction(MEDIA_STATUS_CREATED),
  cadenceRepairFlag (false),
  virtualMediaFlag(false)
{
   fieldList = new CFieldList(this);
}

CTrack::~CTrack()
{
   deleteFrameList_List();
   delete mediaFormat;
   delete fieldList;
}

//////////////////////////////////////////////////////////////////////
// Video Track List Management Functions
//////////////////////////////////////////////////////////////////////

void CTrack::deleteFrameList_List()
{
   int framingIndex;
   // Some image formats do not need a separate film-frames track (e.g. 625)
   // so the track list entry for the film-frames track points to the
   // the same CVideoFrameList as the video-frames track.  If this is the case,
   // must be careful not to delete the track twice

   if (frameList_List.size() > 1)
      {
      if (frameList_List[FRAMING_SELECT_VIDEO]
                                         == frameList_List[FRAMING_SELECT_FILM])
         framingIndex = FRAMING_SELECT_FILM;
      else
         framingIndex = FRAMING_SELECT_VIDEO;
      }
   else
      framingIndex = 0;

   for (; framingIndex < (int)frameList_List.size(); ++framingIndex)
      {
      delete frameList_List[framingIndex];
      }

   frameList_List.clear();
}

void CTrack::addFrameList(CFrameList *newFrameList)
{
   frameList_List.push_back(newFrameList);
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

string CTrack::getAppId() const
{
   // Get m_appId, which is an arbitrary string which an application
   // can use to identify a track

   return appId;
}

CFrameList* CTrack::getBaseFrameList(int framingIndex) const
{
   if (framingIndex < 0 || framingIndex >= (int)frameList_List.size())
      {
      // Framing Index is out-of-range
      return 0;
      }

	CFrameList *frameList = frameList_List[framingIndex];

#if 0
	static void *xxxxx = nullptr;
	if (xxxxx != (void *)frameList)
	{
		xxxxx = (void *)frameList;
		MTIostringstream ostr;
		frameList->dump(ostr);
		string wtf = ostr.str();
		TRACE_0(errout << wtf);
	}
#endif

   return frameList;
}

EClipVersion CTrack::getClipVer() const
{
   return clipVer;
}

int CTrack::setFrameList(int framingIndex, CFrameList* newFrameList)
{
   if (framingIndex < 0 || framingIndex >= (int)frameList_List.size())
      {
      // Framing Index is out-of-range
      return -1;
      }

   frameList_List[framingIndex] = newFrameList;

   return 0;
}

CFieldList* CTrack::getFieldList() const
{
   if (clipVer == CLIP_VERSION_5L)
      {
      CAutoErrorReporter errorReporter("CTrack::getFieldList");
      errorReporter.errorCode = CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;
      errorReporter.msg << "Function not supported by clip version 5";
      return 0;
      }

   return fieldList;
}

int CTrack::getFrameListFileNames(vector<string> &fileNameList) const
{
   int nameCount = 0;

   // Some image formats do not need a separate film-frames track (e.g. 625)
   // so the track list entry for the film-frames track points to the
   // the same CVideoFrameList as the video-frames track.  If this is the case,
   // must be careful not to return duplicate frame list file names
   int framingIndex;
   if (frameList_List.size() > 1)
      {
      if (frameList_List[FRAMING_SELECT_VIDEO]
                                         == frameList_List[FRAMING_SELECT_FILM])
         framingIndex = FRAMING_SELECT_FILM;
      else
         framingIndex = FRAMING_SELECT_VIDEO;
      }
   else
      framingIndex = 0;

   for (; framingIndex < (int)frameList_List.size(); ++framingIndex)
      {
      // Don't attempt to delete a frame-list file for video field
      // frame lists, since the files don't actually exist
      if (framingIndex == FRAMING_SELECT_FIELDS_1_2
          || framingIndex == FRAMING_SELECT_FIELD_1_ONLY
          || framingIndex == FRAMING_SELECT_FIELD_2_ONLY
          || framingIndex == FRAMING_SELECT_CADENCE_REPAIR)
         continue;   // skip this framing index

      CFrameList *frameList = frameList_List[framingIndex];
      if (frameList != 0)
         {
         fileNameList.push_back(frameList->getFrameListFileNameWithExt());
         nameCount++;
         }
      }
   return nameCount;
}

int CTrack::getFramingCount() const
{
   if (clipVer == CLIP_VERSION_5L)
      return 1;
   else
      return (int)frameList_List.size();
}

int CTrack::getHandleCount() const
{
   return handleCount;
}

EMediaStatus CTrack::getLastMediaAction() const
{
 EMediaStatus msReturn = lastMediaAction;

 // ?? WTF? WHY IS THIS SPECIAL??
 if (cadenceRepairFlag)
  {
   if (lastMediaAction != MEDIA_STATUS_RETIRED &&
       lastMediaAction != MEDIA_STATUS_DELETED)
    {
     msReturn = MEDIA_STATUS_CADENCE_REPAIR;
    }
  }

  return msReturn;
}

string CTrack::getLastMediaActionString() const
{
   return queryMediaStatusName(getLastMediaAction());
}

const CMediaFormat* CTrack::getMediaFormat() const
{
   if (clipVer == CLIP_VERSION_3)
      return mediaFormat;

   else if (clipVer == CLIP_VERSION_5L)
      return clip5TrackHandle.GetMediaFormat();

   else
      return 0; // unexpected clip version
}

const CMediaStorageList* CTrack::getMediaStorageList() const
{
   return &mediaStorageList;
}

int CTrack::setCadenceRepairFlag (CIniFile *clipFile,
	const string &sectionPrefix, bool bArg)
{

  cadenceRepairFlag = bArg;

  // Construct Video Proxy Information section prefix such as
  // "VideoProxyInfo0"
  MTIostringstream ostr;
  string emptyString;
  string trackInfoSection = sectionPrefix + "Info";

  ostr.str(emptyString);
  ostr << trackInfoSection << trackNumber;

  // update the .clp file
  clipFile->WriteBool(ostr.str(), cadenceRepairFlagKey, bArg);


  return 0;
}

bool CTrack::getCadenceRepairFlag ()
{
  return cadenceRepairFlag;
}

ClipSharedPtr CTrack::getParentClip() const
{
   return _parentClip.lock();
}

ERecordStatus CTrack::getRecordToComputerStatus() const
{
   return recordToComputerStatus;
}

ERecordStatus CTrack::getRecordToVTRStatus() const
{
   return recordToVTRStatus;
}

C5MediaTrackHandle CTrack::GetTrackHandle()
{
   return clip5TrackHandle;
}

int CTrack::getTrackNumber() const
{
   return trackNumber;
}

ETrackType CTrack::getTrackType() const
{
   CFrameList *frameList = getBaseFrameList(0);

   if (frameList == 0)
      return TRACK_TYPE_INVALID;

   return frameList->getTrackType();
}

//------------------------------------------------------------------------
//
// Function:    PeekAtMediaAllocation
//
// Description: Get information about this track's underlying
//              media allocation.  This is a backdoor function for
//              utility applications.
//
// Arguments:   vector<SMediaAllocation> &mediaAllocList  Caller's list to be
//                                                        filled with media
//                                                        allocation info
//              int& currentFrame  Pointer to caller's variable that
//                                 is set with the current frame index
//                                 as this function is working.
//              bool& stopRequest  Pointer to caller's variable that
//                                 signals a desire to stop collecting
//                                 the media allocation info
//
// Returns:     0 if succcess, non-zero if failure (see err_clip.h)
//
//------------------------------------------------------------------------
int CTrack::PeekAtMediaAllocation(vector<SMediaAllocation> &mediaAllocList,
                                  volatile int* currentFrame,
                                  volatile bool* stopRequest)
{
   int retVal;
   EClipVersion clipVer = getClipVer();

   mediaAllocList.clear();   // clear caller's list of any existing entries

   if (clipVer == CLIP_VERSION_3)
      {
      retVal = PeekAtMediaAllocation_ClipVer3(mediaAllocList, currentFrame,
                                              stopRequest);
      if (retVal != 0)
         return retVal;
      }
   else if (clipVer == CLIP_VERSION_5L)
      {
      retVal = GetTrackHandle().PeekAtMediaAllocation(mediaAllocList,
                                                      currentFrame,
                                                      stopRequest);
      if (retVal != 0)
         return retVal;
      }
   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   return 0;

} // PeekAtMediaAllocation

int CTrack::PeekAtMediaAllocation_ClipVer3(
                                     vector<SMediaAllocation> &mediaAllocList,
                                     volatile int* currentFrame,
                                     volatile bool* stopRequest)
{
   int retVal;

   // OLD COMMENT:
   // Get the 0th frame list in the track.  This is assumed to be
   // the Video-Frames frame list (in a Video Proxy) or an Audio Track
   // UPDATE:
   // Now, because of a hideous version clip hack, we may want to look
   // at the film frames instead of the video frame list

   int framingSelector = getParentClip()->getMainFrameListIndex();
   CFrameList *frameList = getBaseFrameList(framingSelector);
   if (frameList == 0)
      return 0;   // No frame list, so nothing to do

   // Only consider the fields that should be the "visible" fields
   // in the video-frames frame list
   // WTF??? Invisible frames are REAL ALLOCATED FRAMES too!!
   //int fieldCount = getMediaFormat()->getFieldCount();

   MTI_INT64 currentEnd = 0;
   CMediaAccess *currentMediaAccess = 0;

   SMediaAllocation mediaAllocation;
   bool mediaAllocationExists = false;

   // Iterate over all frames in the frame list or until caller's
   // stopRequest becomes true.
   int frameCount = frameList->getTotalFrameCount();
   for (int i = 0; i < frameCount; ++i)
      {
      if (stopRequest != 0 && *stopRequest)
         break;

      if (currentFrame != 0)
         *currentFrame = i;  // Hack to tell caller the frame that is being
                             // processed.  This is used to indicate progress
      CFrame *frame = frameList->getBaseFrame(i);

      // Iterate over fields in the frame
      // .... INCLUDING INVISIBLE FRAMES! ...
      int fieldCount = frame->getTotalFieldCount();
      for (int j = 0; j < fieldCount; ++j)
         {
         CField *field = frame->getBaseField(j);

         // If the field is dirty, then it must have been allocated.
         // Else check tha tis exists, is not virtual and is not borrowed.
         if (!field->isDirtyMedia() &&
             (!field->doesMediaStorageExist() ||
             field->isVirtualMedia() ||
             field->isBorrowedMedia()))
            continue;  // Don't count, because either no media is allocated
                       // for this field or the media is not owned by the field.

         const CMediaLocation& mediaLocation = field->getMediaLocation();

         CMediaAccess *mediaAccess = mediaLocation.getMediaAccess();
         if (mediaAccess == 0)
            continue;   // Invalid media location, so cannot peek
                        // This avoids crashes, but doesn't help find cause

         if (mediaAccess != currentMediaAccess)
            {
            // Add existing media allocation to caller's list
            if (mediaAllocationExists)
               mediaAllocList.push_back(mediaAllocation);

            // Gather info for the media allocation structure
            mediaAllocation.mediaLocation = mediaLocation;
            mediaAllocation.mediaAccess = mediaAccess;
            mediaAllocation.index = 0;
            mediaAllocation.size = 0;

            currentMediaAccess = mediaAccess;

            mediaAllocationExists = true;
            }
         }
      }

   if (mediaAllocationExists)
      mediaAllocList.push_back(mediaAllocation);

   return 0;

} // PeakAtMediaAllocation_ClipVer3

//////////////////////////////////////////////////////////////////////
//  Mutators
//////////////////////////////////////////////////////////////////////

void CTrack::setAppId(const string& newAppId)
{
   // Set m_appId, which is an arbitrary string which an application
   // can use to identify a track

   appId = newAppId;
}

void CTrack::setClip5TrackHandle(const C5MediaTrackHandle &newHandle)
{
   clip5TrackHandle = newHandle;
}

void CTrack::setClipVer(EClipVersion newClipVer)
{
   clipVer = newClipVer;
}

void CTrack::setHandleCount(int newHandleCount)
{
   handleCount = newHandleCount;
}


void CTrack::setLastMediaAction(EMediaStatus newLastMediaAction)
{
   if (lastMediaAction != newLastMediaAction)
      {
      lastMediaAction = newLastMediaAction;
      if (getParentClip() != nullptr)
         {
         // Goose the clip
         getParentClip()->notifyMediaStatusChanged(getTrackType(), trackNumber);
         }
      }
}

void CTrack::setRecordToComputerStatus(ERecordStatus newRecordStatus)
{
   recordToComputerStatus = newRecordStatus;

   if (newRecordStatus == RECORD_STATUS_PART_RECORDED)
      setLastMediaAction(MEDIA_STATUS_RECORD_TO_COMPUTER_PARTIAL);
   else if (newRecordStatus == RECORD_STATUS_RECORDED)
      setLastMediaAction(MEDIA_STATUS_RECORD_TO_COMPUTER);
}

void CTrack::setRecordToVTRStatus(ERecordStatus newRecordStatus)
{
   recordToVTRStatus = newRecordStatus;

   if (newRecordStatus == RECORD_STATUS_PART_RECORDED)
      setLastMediaAction(MEDIA_STATUS_RECORD_TO_VTR_PARTIAL);
   else if (newRecordStatus == RECORD_STATUS_RECORDED)
      setLastMediaAction(MEDIA_STATUS_RECORD_TO_VTR);
}

void CTrack::setMediaModifierStatus()
{
   setLastMediaAction(MEDIA_STATUS_MODIFIED);
}

void CTrack::setArchivedStatus()
{
   setLastMediaAction(MEDIA_STATUS_ARCHIVED);
}

void CTrack::setCreatedStatus()
{
  setLastMediaAction(MEDIA_STATUS_CREATED);
}

void CTrack::setClonedStatus()
{
  setLastMediaAction(MEDIA_STATUS_CLONED);
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CTrack::createFieldList()
{
   int retVal;

   retVal = fieldList->createFieldList();
   if (retVal != 0)
      return retVal;

   return 0;
}

int CTrack::extendFieldList(int newFrameCount)
{
   int retVal;

   retVal = fieldList->extendFieldList(newFrameCount);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CTrack::createFrameList_List(const CFramingInfoList& framingInfoList)
{

   string trackFileName;
   CFrameList* newFrameList;

   // Create Frame List for Video-Frames
   CFramingInfo* videoFramesFramingInfo
                 = framingInfoList.getBaseFramingInfo(FRAMING_SELECT_VIDEO);
   newFrameList = createFrameList(videoFramesFramingInfo);
   addFrameList(newFrameList);

   // Create Frame List for Film-Frames if relevant, or reuse Video-Frames
   // Frame List
   CFramingInfo* filmFramesFramingInfo
                  = framingInfoList.getBaseFramingInfo(FRAMING_SELECT_FILM);
   if (filmFramesFramingInfo != 0)
      {
      if(filmFramesFramingInfo != videoFramesFramingInfo)
         {
         newFrameList = createFrameList(filmFramesFramingInfo);
         }
      addFrameList(newFrameList);
      }

   if (filmFramesFramingInfo == 0)
      {
      // Since there is no film-frames entry in the frames list list, we
      // cannot add the video-fields or custom-frames frame lists.
      // Note: this is something of a kludge to make video-fields framing
      //       compatible with audio tracks
      return 0;
      }

   // Create three Frame Lists for Video-Field-Frames if relevant, three
   // NULL pointers otherwise
   CFramingInfo *videoFieldsFramingInfo
               = framingInfoList.getBaseFramingInfo(FRAMING_SELECT_FIELDS_1_2);
   if (videoFieldsFramingInfo)
      newFrameList = createFrameList(videoFieldsFramingInfo);
   else
      newFrameList = 0;
   addFrameList(newFrameList);

   videoFieldsFramingInfo
             = framingInfoList.getBaseFramingInfo(FRAMING_SELECT_FIELD_1_ONLY);
   if (videoFieldsFramingInfo)
      newFrameList = createFrameList(videoFieldsFramingInfo);
   else
      newFrameList = 0;
   addFrameList(newFrameList);

   videoFieldsFramingInfo
             = framingInfoList.getBaseFramingInfo(FRAMING_SELECT_FIELD_2_ONLY);
   if (videoFieldsFramingInfo)
      newFrameList = createFrameList(videoFieldsFramingInfo);
   else
      newFrameList = 0;
   addFrameList(newFrameList);

   // create framing list for cadence repair
   videoFieldsFramingInfo
             = framingInfoList.getBaseFramingInfo(FRAMING_SELECT_CADENCE_REPAIR);
   if (videoFieldsFramingInfo)
      newFrameList = createFrameList(videoFieldsFramingInfo);
   else
      newFrameList = 0;
   addFrameList(newFrameList);

   // Create Custom Frame Lists
   CFramingInfo* framingInfo;
   int framingCount = framingInfoList.getFramingCount();
   for (int framingIndex = FRAMING_SELECT_CUSTOM_BASE;
        framingIndex < framingCount;
        ++framingIndex)
      {
      framingInfo = framingInfoList.getBaseFramingInfo(framingIndex);
      if (framingInfo == 0)
         {
         // A track has not been defined for this track index, so
         // just add a NULL pointer to the VideoProxy's track list
         newFrameList = 0;
         }
      else
         {
         newFrameList = createFrameList(framingInfo);
         }

      // Add the new CFrameList instance (or NULL pointer) to this
      // CTrack's list of Frame Lists
      addFrameList(newFrameList);
      }

   return 0;

} // createFrameList_List

// -------------------------------------------------------------------
//
// Function:    assignMediaStorage
//
// Description: Assign the Media Locations for each frame and field in the
//              Video-Frames Frame List.
//
//              NB: This function should only be used on frames that do
//                  not already have media storage assigned.
//
// Inputs:      CMediaAllocator& mediaAllocator
//
// Returns:     0 => Success.  Non-zero => Error
//
// -------------------------------------------------------------------
int CTrack::assignMediaStorage(CMediaAllocator& mediaAllocator,
                               int startFrame, int frameCount)
{
   int retVal;

   // Get the Video-Frames Frame List
   CFrameList* frameList = getBaseFrameList(FRAMING_SELECT_VIDEO);
   if (frameList == 0)
      {
      // ERROR: Video-Frames Frame List does not exist
      return -460;
      }

   // Add the media access instance to the Media Storage List
   CMediaAccess* mediaAccess = mediaAllocator.getMediaAccess();
   mediaStorageList.addMediaStorage(mediaAccess->getMediaType(),
                                    mediaAccess->getMediaIdentifier(),
                                    mediaAccess->getHistoryDirectory(),
                                    mediaAccess);

   // The CMediaAllocator instance that we just obtained can now be used
   // to get the storage location for each individual field's image data
   retVal = frameList->assignMediaStorage(mediaAllocator, startFrame,
                                          frameCount);
   if (retVal != 0)
      return retVal;

   return 0;
}

// ---------------------------------------------------------------------------

int CTrack::extendMediaStorage(int startFrame, int frameCount)
{
   int retVal;

   if (getVirtualMediaFlag())
      return CLIP_ERROR_VIRTUAL_MEDIA_CANNOT_OWN_MEDIA_STORAGE;

   if (frameCount == 0)
      return 0;   // nothing to do

   if (getClipVer() == CLIP_VERSION_3)
      {
      retVal = extendMediaStorage_ClipVer3(startFrame, frameCount);
      }
   else if (getClipVer() == CLIP_VERSION_5L)
      {
      // Clip 5 "Lite"
      retVal = extendMediaStorage_ClipVer5L(startFrame, frameCount);
      }
   else
      {
      TRACE_0(errout << "ERROR: CTrack::extendMediaStorage: Invalid Clip Version: "
                     << getClipVer());
      retVal = CLIP_ERROR_INVALID_CLIP_VERSION;
      }

   return retVal;
}

int CTrack::extendMediaStorage_ClipVer3(int startFrame, int frameCount)
{
   int retVal;

   // Get the Video-Frames Frame List
   CFrameList* frameList = getBaseFrameList(FRAMING_SELECT_VIDEO);
   if (frameList == 0)
      return -460; // ERROR: Video-Frames Frame List does not exist

   int totalFrameCount = frameList->getTotalFrameCount();

   // Check and adjust the startFrame and frameCount arguments
   // startFrame == -1 means expand the media allocation after the
   //                  the last frame that already has media storage
   // frameCount == -1 means expand the media allocation to the end of
   //               the frame list

   if (startFrame == -1)
      {
      // Use media allocation frontier to determine the first frame
      int frontier = frameList->getMediaAllocatedFrontier();
      if (frontier == -1)
         startFrame = 0;
      else
         {
         startFrame = frontier + 1;
         if (startFrame >= totalFrameCount)
            return CLIP_ERROR_MEDIA_STORAGE_ALREADY_ASSIGNED;
         }
      }
   else if (startFrame < -1 || startFrame >= totalFrameCount)
      return CLIP_ERROR_INVALID_FRAME_NUMBER;

   int framesToEnd = totalFrameCount - startFrame;

   if (frameCount < 0 || frameCount > framesToEnd)
      frameCount = framesToEnd;

   int endFrame = startFrame + frameCount;

   // Check if any of the frames in the range already have media storage
   // If so, report error
   for (int frameIndex = startFrame; frameIndex < endFrame; ++frameIndex)
      {
      if (frameList->doesMediaStorageExist(frameIndex))
         return CLIP_ERROR_MEDIA_STORAGE_ALREADY_ASSIGNED;
      }

   // Allocate the new media storage from the first media storage item
   // in this track's media storage list.
   CMediaStorage *mediaStorage = mediaStorageList.getMediaStorage(0);
   if (mediaStorage == 0)
      return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR; // no media storage ??

   // Check that there is sufficient free media storage
   retVal = checkExtendMediaStorage(frameCount);
   if (retVal != 0)
      return retVal; // insufficient storage or an error

   CTimecode frame0Timecode = frameList->getTimecodeForFrameIndex(0);
   CMediaAccess *mediaAccess = mediaStorage->getMediaAccessObj();
   if (mediaAccess == 0)
      return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR; // no media access??
   CMediaAllocator *mediaAllocator = mediaAccess->extend(*getMediaFormat(),
                                                         frame0Timecode,
                                                         frameCount);
   if (mediaAllocator == 0)
      return CLIP_ERROR_MEDIA_ALLOCATION_FAILED; // ERROR: Allocation failed

   // Assign the media storage to each frame/field in the frame list
   retVal = assignMediaStorage(*mediaAllocator, startFrame, frameCount);
   if (retVal != 0)
      {
      mediaAccess->cancelExtend(mediaAllocator);
      delete mediaAllocator;
      return retVal;
      }

   // Update the frame list's allocated frame count and allocation frontier
   frameList->getMediaAllocatedFrontier(true);

   // Update the Field List file
   retVal = updateFieldListFile();
   if (retVal != 0)
      {
      // Cancel media storage allocation
      mediaAccess->cancelExtend(mediaAllocator);
      delete mediaAllocator;
      return retVal; // ERROR: Failed to write Field List file
      }

   // Write the updated frame list (.trk) file
   retVal = writeFrameListFile(FRAMING_SELECT_VIDEO);
   if (retVal != 0)
      {
      // Cancel media storage allocation
      mediaAccess->cancelExtend(mediaAllocator);
      delete mediaAllocator;
      return retVal; // ERROR: Failed to write Frame List file
      }

   retVal = getParentClip()->writeFile();
   if (retVal != 0)
      {
      // mlm: should I cancelExtend() here?  Or is it too late?
      delete mediaAllocator;
      return retVal;
      }

   delete mediaAllocator;

   return 0;

} // extendMediaStorage_ClipVer3

int CTrack::extendMediaStorage_ClipVer5L(int startFrame, int frameCount)
{
   int retVal;

   // Clip 5 current just supports the default mode of extending
   // the media at the point where the media current ends
   if (startFrame != -1 || frameCount < 1)
      return CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5;

   // Check that there is sufficient free media storage
   retVal = checkExtendMediaStorage(frameCount);
   if (retVal != 0)
      return retVal;  // insufficient storage or a media interface error

   string clipPath = getParentClip()->getClipPathName();
   retVal = clip5TrackHandle.ExtendMediaTrack(clipPath, 0, frameCount);
   if (retVal != 0)
      return retVal;  // Mon dieu, extending the track failed!

   // Update frontier
   CFrameList *frameList = getBaseFrameList(0);
   if (frameList != 0)
      frameList->getMediaAllocatedFrontier(true);

   return 0;

} // extendMediaStorage_ClipVer5L

// ---------------------------------------------------------------------------

int CTrack::checkExtendMediaStorage(int frameCount)
{
// Check if there is sufficient media storage to extend this track

   int retVal;
   CMediaInterface mediaInterface;

   if (getVirtualMediaFlag())
      return 0;   // virtual media track, doesn't own any media storage

   if (getClipVer() == CLIP_VERSION_3)
      {
      // Allocate the new media storage from the first media storage item
      // in this track's media storage list.
      CMediaStorage *mediaStorage = mediaStorageList.getMediaStorage(0);
      if (mediaStorage == 0)
         return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR; // no media storage ??

      // Check that there is sufficient free media storage
      retVal = mediaInterface.checkSpace(mediaStorage->getMediaType(),
                                         mediaStorage->getMediaIdentifier(),
                                         *getMediaFormat(), frameCount);
      if (retVal != 0)
         return retVal; // insufficient storage or an error
      }
   else if (getClipVer() == CLIP_VERSION_5L)
      {
      // Clip 5 "Lite"

      // Find out the media type and media identifier already used
      // by this video track.  If there is no media track, then we
      // are up the creek without a paddle.
      EMediaType mediaType;
      retVal = clip5TrackHandle.GetFirstMediaType(mediaType);
      if (retVal != 0)
         return retVal;
      string mediaIdentifier;
      retVal = clip5TrackHandle.GetFirstMediaIdentifier(mediaIdentifier);
      if (retVal != 0)
         return retVal;

      // Check that there is sufficient free media storage
      retVal = mediaInterface.checkSpace(mediaType, mediaIdentifier,
                                         *getMediaFormat(), frameCount);
      if (retVal != 0)
         return retVal;
      }
   else
      {
      TRACE_0(errout << "ERROR: CTrack::checkExtendMediaStorage: Invalid Clip Version: "
                     << getClipVer());
      return CLIP_ERROR_INVALID_CLIP_VERSION;
      }

   return 0;

} // checkExtendMediaStorage

// ---------------------------------------------------------------------------
//**Clip2clip hack
void CTrack::addDirtyMediaStorage(EMediaType mediaType,
                                  const string& mediaIdentifier,
                                  const string& historyDirectory,
                                  CMediaAccess* mediaAccess)
{
   mediaStorageList.setDirtyMediaStorageIndex(
                                     mediaStorageList.getMediaStorageCount()
                                             );
   mediaStorageList.addMediaStorage(mediaType,
                                    mediaIdentifier,
                                    historyDirectory,
                                    mediaAccess);
}

// ---------------------------------------------------------------------------
//**Clip2clip hack

// * TODO: Need to REFACTOR assignDirtyMediaStorage and
//   unassignDirtyMediaStorage - lots of duplicate code!

int CTrack::assignDirtyMediaStorage(CFrameList &frameList,
                                    int frameIndex,
                                    DirtyMediaAssignmentUndoInfo &undoInfo)
{
   int retVal = 0;
   undoInfo.undoFieldList.clear();
   undoInfo.undoHistoryFlag = false;
   int framingType = frameList.getFramingType();

   ///////////////////////////////////////////////////////
   // CAREFUL!!! FRAMING_SELECT_XXX != FRAMING_TYPE_XXX //
   ///////////////////////////////////////////////////////

   ///////////////////////////////////////////////////////
   // NOTE! We need to assign dirty media ONLY TO FILM  //
   //       or VIDEO frame lists, never to the oddball  //
   //       ones like FIELDS_1_2 etc.                   //
   ///////////////////////////////////////////////////////

   int framingSelect = (framingType == FRAMING_TYPE_FILM)?
                         FRAMING_SELECT_FILM : FRAMING_SELECT_VIDEO;

   // This sucks - should ask the framelist if it supports dirty media QQQ
   CVideoFrameList *videoFrameList = dynamic_cast<CVideoFrameList *>(&frameList);
   if (videoFrameList == 0)
      {
      //TRACE_2(errout << "WARNING: Tried to assign dirty media to non-video track");
      return 0;  // return error?  no - this is where audio tracks are screened
      }

   // Always look at the main video list, not the oddball ones (like fields 1,2)
   if (framingSelect == FRAMING_SELECT_VIDEO)
      {
      videoFrameList = dynamic_cast<CVideoFrameList *>(
                                      getBaseFrameList(FRAMING_SELECT_VIDEO));
      if (videoFrameList == 0)
         {
         TRACE_0(errout << "ERROR: can't find normal video frame list");
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN;
         }
      }

   // Special case - video1,2 framing has double the correct number of frames
   if (framingType == FRAMING_TYPE_FIELDS_1_2)
      frameIndex /= 2;

   CFrame *videoFrame = videoFrameList->getBaseFrame(frameIndex);
   if (videoFrame == NULL)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   if (!videoFrame->isWriteProtected())
      return 0; // No media will be dirtied here

   TRACE_3(errout << "(((( assignDirtyMediaStorage( FL="
                  << std::setbase(16) << std::setw(8) << std::setfill('0')
                  << ((int) &frameList) << std::setbase(10) << ", Frame="
                  << frameIndex << " )" );

   // Do we have permission to write to frames on this frame list?
   auto clip = getParentClip();
   if (framingSelect == clip->getLockedFrameListIndex())
      {
      // Oops, this framelist is locked out
      TRACE_0(errout << "ERROR: Frame list is locked. Cannot modify both "
                        "film and video frames");
      return CLIP_ERROR_FRAME_LIST_IS_LOCKED;
      }

   // The framing list we are hacking is now the "main" frame list
   clip->setMainFrameListIndex(framingSelect);

   // Before we muck with the media assignments, let's be prepared to
   // keep the other framelist (if there is one) in sync with this one
   CVideoProxy *videoProxy = videoFrameList->getVideoProxy();
   if (videoProxy == 0)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   map<int, vector <std::pair<int,int> > >  otherFrameListFieldMapping;
   int otherFramingSelect = (framingType == FRAMING_TYPE_FILM)?
                        FRAMING_SELECT_VIDEO : FRAMING_SELECT_FILM;
   bool fixOtherFrameList = !(videoProxy->isSameVideoFrameList(framingSelect,
                                                         otherFramingSelect));
   if (fixOtherFrameList)
      {
      // LOCK OUT THE OTHER FRAME LIST IF IT'S DIFFERENT!
      if (otherFramingSelect != clip->getLockedFrameListIndex())
         clip->setLockedFrameListIndex(otherFramingSelect);

      // Generate list of mappings of field we are hacking to frame/fields
      // in the other, different frame list so we can synchronize them later
      videoProxy->mapFrameIndex(frameIndex, framingSelect, otherFramingSelect,
                                otherFrameListFieldMapping);

      }

   // If the framelist is of the oddball video type, we need to copy
   // the media to the dirty frame, else the frames only get half written
   vector<CMediaLocation> oldMediaLocations;
   switch (framingType)
      {
      case FRAMING_TYPE_FIELDS_1_2:
      case FRAMING_TYPE_FIELD_1_ONLY:
      case FRAMING_TYPE_FIELD_2_ONLY:

      for (int i = 0; i < videoFrame->getTotalFieldCount(); i++)
         {
         oldMediaLocations.push_back(
            videoFrame->getBaseField(i)->getMediaLocation());
         }
      }

   // History may already have been updated elsewhere
   undoInfo.undoHistoryFlag = true;

   CMediaAllocator *mediaAllocator =
      videoFrame->getDirtyMediaStorageAllocator(mediaStorageList, retVal);
   if (retVal == CLIP_ERROR_NO_DIRTY_MEDIA_STORAGE_SPECIFIED)
      return 0; // Not an error =- it's simply not a versioned clip
   if (retVal != 0)
      return retVal;

   // Set flags for each field in the frame
   videoFrame->assignDirtyMediaStorage(*mediaAllocator, undoInfo.undoFieldList);
   int newlyAssignedFieldCount = undoInfo.undoFieldList.size();

   // If nothing was assigned (not sure how that could happen)
   // don't do anything else
   if (newlyAssignedFieldCount == 0)
      return 0;

   // For oddball video framing types (e.g. FIELD_!_ONLY,
   // need to actually copy the media from the parent because we're only
   // going to be changing half of the actual video frame
   for (int i = 0; i < (int) oldMediaLocations.size(); ++i)
      {
      CMediaLocation newMediaLocation = videoFrame->getBaseField(i)->getMediaLocation();
      CMediaAccess::copyMedia(oldMediaLocations[i], newMediaLocation);
      }
   if (oldMediaLocations.size() > 0)
      {
      for (int i = 0; i < videoFrame->getTotalFieldCount(); i++)
         {
         videoFrame->getBaseField(i)->setMediaWritten(true);
         }
      }

   // Synchronize frame lists
   // REFACTOR THIS (identical ugly code in unassignDirtyMedia())
   if (fixOtherFrameList)
      {
      CVideoFrameList *otherFrameList =
         videoProxy->getVideoFrameList(otherFramingSelect);
      if (otherFrameList != 0)
         {
         int fieldCount = videoFrame->getTotalFieldCount();
         for (int fieldIndex = 0; fieldIndex < fieldCount; ++fieldIndex)
            {
            CField *field = videoFrame->getBaseField(fieldIndex);
            if (field != 0)
               {
               vector< std::pair<int,int> > otherFieldMapping =
                     otherFrameListFieldMapping[fieldIndex];
               for (int listIndex = 0;
                    listIndex < (int) otherFieldMapping.size();
                    ++listIndex)
                  {
                  int otherFrameIndex = otherFieldMapping.at(listIndex).first;
                  CFrame *otherFrame = otherFrameList->getBaseFrame(otherFrameIndex);
                  if (otherFrame != 0)
                     {
                     int otherFieldIndex = otherFieldMapping.at(listIndex).second;
                     CField *otherField = otherFrame->getBaseField(otherFieldIndex);
                     if (otherField != 0)
                        {
                        otherField->setMediaLocation(field->getMediaLocation());

                        if (otherFramingSelect == FRAMING_SELECT_VIDEO)
                           {
                           // Sync up oddball video frame lists to master

                           // Video fields 1,2
                           syncVideo12FrameListFrameField(otherFrame,
                                                          otherFrameIndex,
                                                          otherFieldIndex);
                           if ((otherFieldIndex % 2) == 0)
                              {
                              // Video field 1 only
                              syncVideo1FrameListFrameField(otherFrame,
                                                            otherFrameIndex);
                              }
                           else
                              {
                              //Video field 2 only
                              syncVideo2FrameListFrameField(otherFrame,
                                                            otherFrameIndex);
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }

/////////////////////////////////////////////////////////////////
// FORCE ALL OF THE FRAME LISTS TO BE WRITTEN TO DISK EACH     //
// TIME WE ASSIGN DIRTY STORAGE TO A FRAME. THIS REALLY SUCKS. //
// This is necessary because we don't know when it actually    //
// makes sense to do the flushing, and we don't want to wait   //
// until the clip is closed to update the files because it is  //
// disastrous if the files don't get written.                  //
//                                                             //
   writeAllFrameListFiles();                                   //
//                                                             //
/////////////////////////////////////////////////////////////////


   // If we are hacking the Video frame list, sync up all the associated
   // framelists (fields 1,2 ; field 1 only ; field 2 only)
   if (framingSelect == FRAMING_SELECT_VIDEO)
      {
      syncVideoFramingFrameListFrameFields(frameIndex);
      }

   // Bump (increment) the "modified frame" count for the clip
   CBinManager binManager;
   binManager.bumpModifiedFrameCounter(getParentClip()->getClipPathName(), 1);

   return retVal;
}

// ---------------------------------------------------------------------------
//**Clip2clip hack
int CTrack::unassignDirtyMediaStorage(CFrameList &frameList,
                                      int frameIndex,
                                      DirtyMediaAssignmentUndoInfo &undoInfo)
{
   if (undoInfo.undoFieldList.empty())
      return 0;

   TRACE_3(errout << "(((( UNassignDirtyMediaStorage( FL="
                  << std::setbase(16) << std::setw(8) << std::setfill('0')
                  << ((int) &frameList) << std::setbase(10) << ", Frame="
                  << frameIndex << " )" );

   int framingType = frameList.getFramingType();
   int framingSelect = (framingType == FRAMING_TYPE_FILM)?
                         FRAMING_SELECT_FILM : FRAMING_SELECT_VIDEO;

   // This sucks - should ask the framelist if it supports dirty media QQQ
   CVideoFrameList *videoFrameList = dynamic_cast<CVideoFrameList *>(&frameList);
   if (videoFrameList == 0)
      {
      //TRACE_2(errout << "WARNING: Tried to unassign dirty media from non-video track");
      return 0;  // ignore it ... CLIP_ERROR_UNEXPECTED_NULL_RETURN;
      }

   if (framingSelect == FRAMING_SELECT_VIDEO)
      {
      videoFrameList = dynamic_cast<CVideoFrameList *>(
                                      getBaseFrameList(FRAMING_SELECT_VIDEO));
      if (videoFrameList == 0)
         {
         TRACE_0(errout << "ERROR: can't find normal video frame list");
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN;
         }
      }

   // Special case - video1,2 framing has double the correct number of frames
   if (framingType == FRAMING_TYPE_FIELDS_1_2)
      frameIndex /= 2;

   // Before we muck with the media assignments, let's be prepared to
   // keep the other framelist (if there is one) in sync with this one
   // TODO: CONSIDER KEEPING THE FIELD MAPPING INFO IN THE UNDO INFO OBJECT
   // Want video proxy... there must be a better way to do this...
   CVideoProxy *videoProxy = videoFrameList->getVideoProxy();
   if (videoProxy == 0)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   map<int, vector <std::pair<int,int> >  >  otherFrameListFieldMapping;
   int otherFramingSelect = (framingType == FRAMING_TYPE_FILM)?
                        FRAMING_SELECT_VIDEO : FRAMING_SELECT_FILM;
   bool fixOtherFrameList = !(videoProxy->isSameVideoFrameList(framingSelect,
                                                         otherFramingSelect));
   if (fixOtherFrameList)
       videoProxy->mapFrameIndex(frameIndex, framingSelect, otherFramingSelect,
                     otherFrameListFieldMapping);

   CFrame *videoFrame = videoFrameList->getBaseFrame(frameIndex);
   if (videoFrame == NULL)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   videoFrame->unassignDirtyMediaStorage(undoInfo.undoFieldList);

   // Synchronize frame lists
   // REFACTOR THIS (identical ugly code in assignDirtyMedia(), above)
   if (fixOtherFrameList)
      {
      CVideoFrameList *otherFrameList =
         videoProxy->getVideoFrameList(otherFramingSelect);
      if (otherFrameList != 0)
         {
         int fieldCount = videoFrame->getTotalFieldCount();
         for (int fieldIndex = 0; fieldIndex < fieldCount; ++fieldIndex)
            {
            CField *field = videoFrame->getBaseField(fieldIndex);
            if (field != 0)
               {
               vector<std::pair<int,int> > otherFieldMapping =
                     otherFrameListFieldMapping[fieldIndex];
               for (int listIndex = 0;
                    listIndex < (int) otherFieldMapping.size();
                    ++listIndex)
                  {
                  int otherFrameIndex = otherFieldMapping.at(listIndex).first;
                  CFrame *otherFrame = otherFrameList->getBaseFrame(otherFrameIndex);
                  if (otherFrame != 0)
                     {
                     int otherFieldIndex = otherFieldMapping.at(listIndex).second;
                     CField *otherField = otherFrame->getBaseField(otherFieldIndex);
                     if (otherField != 0)
                        {
                        otherField->setMediaLocation(field->getMediaLocation());

                        if (otherFramingSelect == FRAMING_SELECT_VIDEO)
                           {
                           // Sync up oddball video frame lists to master

                           // Video fields 1,2
                           syncVideo12FrameListFrameField(otherFrame,
                                                          otherFrameIndex,
                                                          otherFieldIndex);
                           if ((otherFieldIndex % 2) == 0)
                              {
                              // Video field 1 only
                              syncVideo1FrameListFrameField(otherFrame,
                                                            otherFrameIndex);
                              }
                           else
                              {
                              //Video field 2 only
                              syncVideo2FrameListFrameField(otherFrame,
                                                            otherFrameIndex);
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }

    // Flush
   writeAllFrameListFiles();

   // If we are hacking the Video frame list, sync up all the associated
   // framelists (fields 1,2 ; field 1 only ; field 2 only)
   if (framingSelect == FRAMING_SELECT_VIDEO)
      {
      syncVideoFramingFrameListFrameFields(frameIndex);
      }

   // Decrement the "modified frame" count for the clip
   CBinManager binManager;
   binManager.bumpModifiedFrameCounter(getParentClip()->getClipPathName(), -1);

   return 0;
}
// ---------------------------------------------------------------------------
void CTrack::syncVideoFramingFrameListFrameFields(int frameIndex)
{
   CFrameList *srcFrameList = getBaseFrameList(FRAMING_SELECT_VIDEO);
   CFrame *srcFrame = srcFrameList->getBaseFrame(frameIndex);

   // Sync "Video Fields 1,2" frame list  to "Video" frame list
   for (int i = 0; i < 2; ++i)
      {
      syncVideo12FrameListFrameField(srcFrame, frameIndex, i);
      }

   // Sync "Video Field 1 Only" frame list to "Video" frame list
   syncVideo1FrameListFrameField(srcFrame, frameIndex);

   // Sync "Video Field 2 Only" frame list to "Video" frame list
   syncVideo2FrameListFrameField(srcFrame, frameIndex);

}
// ---------------------------------------------------------------------------

void CTrack::syncVideo12FrameListFrameField(CFrame *srcFrame,
                                            int srcFrameIndex,
                                            int srcFieldIndex)
{
   // Sync one "Video Fields 1,2" frame list frame field

   //////////////////////////////////////////////////////////////
   // CAREFUL!! FIELDS ARE SWAPPED FOR 525i IN FIELDS 1,2 MODE //
   //////////////////////////////////////////////////////////////
   auto clip = getParentClip();
   const CImageFormat *imageFormat =
                       clip->getImageFormat(VIDEO_SELECT_NORMAL);
   EImageFormatType imageFormatType =
                       imageFormat->getImageFormatType();
   bool swapFields = (imageFormatType == IF_TYPE_525_5994);
   //////////////////////////////////////////////////////////////

   CFrameList *dstFrameList = getBaseFrameList(FRAMING_SELECT_FIELDS_1_2);
   if ((dstFrameList != 0) && dstFrameList->isFrameListAvailable())
      {
      CFrame *dstFrame = dstFrameList->getBaseFrame(srcFrameIndex * 2 +
                                                    (swapFields?
                                                       1 - srcFieldIndex :
                                                       srcFieldIndex) );
      if (dstFrame != 0)
         {
         CField* srcField = srcFrame->getBaseField(srcFieldIndex);

         // Both fields of the target frame get set to the same source field
         for (int j = 0; j < 2; ++j)
            {
            CField *dstField = dstFrame->getBaseField(j);
            if (dstField != 0 && srcField != 0)
               {
               dstField->setMediaLocation(srcField->getMediaLocation());
               }
            }
         }
      }
}
// ---------------------------------------------------------------------------

void CTrack::syncVideo1FrameListFrameField(CFrame *srcFrame,
                                           int srcFrameIndex)
{
   // Sync "Video Field 1 Only" frame list to "Video" frame list

   ////////////////////////////////////////////////////////////////
   // CAREFUL!! Fields are NOT swapped for 525i in field 1 mode  //
   ////////////////////////////////////////////////////////////////

   CFrameList *dstFrameList = getBaseFrameList(FRAMING_SELECT_FIELD_1_ONLY);
   if ((dstFrameList != 0) && dstFrameList->isFrameListAvailable())
      {
      CFrame *dstFrame = dstFrameList->getBaseFrame(srcFrameIndex);
      if (dstFrame != 0)
         {
         CField *srcField = srcFrame->getBaseField(0);

         // Both fields of the target frame get set to the same source field
         for (int j = 0; j < 2; ++j)
            {
            CField *dstField = dstFrame->getBaseField(j);
            if (dstField != 0 && srcField != 0)
               {
               dstField->setMediaLocation(srcField->getMediaLocation());
               }
            }
         }
      }
}
// ---------------------------------------------------------------------------

void CTrack::syncVideo2FrameListFrameField(CFrame *srcFrame,
                                           int srcFrameIndex)
{
   // Sync "Video Field 2 Only" frame list to "Video" frame list

   ////////////////////////////////////////////////////////////////
   // CAREFUL!! Fields are NOT swapped for 525i in field 2 mode  //
   ////////////////////////////////////////////////////////////////

   CFrameList *dstFrameList = getBaseFrameList(FRAMING_SELECT_FIELD_2_ONLY);
   if ((dstFrameList != 0) && dstFrameList->isFrameListAvailable())
      {
      CFrame *dstFrame = dstFrameList->getBaseFrame(srcFrameIndex);
      if (dstFrame != 0)
         {
         CField *srcField = srcFrame->getBaseField(1);

         // Both fields of the target frame get set to the same source field
         for (int j = 0; j < 2; ++j)
            {
            CField *dstField = dstFrame->getBaseField(j);
            if (dstField != 0 && srcField != 0)
               {
               dstField->setMediaLocation(srcField->getMediaLocation());
               }
            }
         }
      }
}
// ---------------------------------------------------------------------------

//**Clip2clip hack
CMediaAllocator *CTrack::getDirtyMediaStorageAllocator(CFrame &frame,
                                                       int fieldCount,
                                                       int &status)
{
   status = 0;

   int dirtyMediaStorageIndex = mediaStorageList.getDirtyMediaStorageIndex();
   if (dirtyMediaStorageIndex < 0)
      {
      status = CLIP_ERROR_NO_DIRTY_MEDIA_STORAGE_SPECIFIED; // there wasn't one
      return NULL;
      }

   const CMediaStorage *dirtyMediaStorage =
                    mediaStorageList.getMediaStorage(dirtyMediaStorageIndex);
   if (dirtyMediaStorage == NULL)
      {
      status =  CLIP_ERROR_NO_DIRTY_MEDIA_STORAGE_SPECIFIED; // shouldn't happen
      return NULL;
      }

   CMediaAccess *mediaAccess = dirtyMediaStorage->getMediaAccessObj();
   if (mediaAccess == NULL)
      {
      status =  CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR;        // shouldn't happen
      return NULL;
      }

   // Timecode is same for all fields, so only need to look at first one
   CField *field = frame.getBaseField(0);
   if (field == NULL)
      {
      status = CLIP_ERROR_UNEXPECTED_NULL_RETURN;
      return NULL;
      }

   CMediaLocation oldMediaLocation = field->getMediaLocation();
   int FieldNameAsInteger = 0;
   const char *fieldName = oldMediaLocation.getFieldName();
   if (fieldName != NULL)
      {
      MTIistringstream istr;
      istr.str(fieldName);
      istr >> FieldNameAsInteger;
      }
   CTimecode timecode(FieldNameAsInteger);

   CMediaAllocator *mediaAllocator = mediaAccess->allocateOneFrame(*mediaFormat,
                                                                   timecode,
                                                                   fieldCount);
   if (mediaAllocator == NULL)
      {
      int theErrorCode = theError.getError();
      if (theErrorCode == 0)
         theErrorCode = CLIP_ERROR_MEDIA_ALLOCATION_FAILED;
      status =  CLIP_ERROR_MEDIA_ALLOCATION_FAILED;
      return NULL;
      }

   return mediaAllocator;
}

#ifdef __OBSOLETE__
// ---------------------------------------------------------------------------
//**Clip2clip hack
int CTrack::assignDirtyMediaStorage(CField &field,
                                    CMediaAllocator &mediaAllocator)
{
   CMediaLocation oldMediaLocation = field.getMediaLocation();
   CMediaLocation newMediaLocation;
   mediaAllocator.getNextFieldMediaAllocation(newMediaLocation);
   newMediaLocation.setFieldName(oldMediaLocation.getFieldName());
   newMediaLocation.setFieldNumber(oldMediaLocation.getFieldNumber());

   field.assignDirtyMediaStorage(newMediaLocation);

   // Flush the modified field list entry to disk
   const unsigned int externalBufferSize(4096);  // recommended min size
   char externalBuffer[externalBufferSize];
   int retVal = getFieldList()->updateFieldListFile(
                                           field.getFieldListEntryIndex(),
                                           /* fieldCount = */ 1,
                                           externalBuffer,
                                           externalBufferSize);
   if (retVal != 0)
      return retVal;

   // GAACK - Need to search ALL framelists for any fields with the
   // same media location and change them because the media locations
   // ARE NOT SHARED!!  Careful... we need to even do the framelist
   // where the original field was moved because there may be
   // several fields pointing at the same media and their media
   // locations ARE NOT SHARED!!

   CFrameList *oldFrameList = NULL;   // Hack to deal with video=film
   for (int framingIndex = 0;
        framingIndex < getFramingCount();
        ++framingIndex)
      {
      CFrameList *otherFrameList = getBaseFrameList(framingIndex);

      if (otherFrameList != NULL && otherFrameList != oldFrameList)
         {
         TRACE_3(errout << "{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{"
                        << "(((( Do FrameList " << framingIndex
                        << " ( "
                        << std::setbase(16) << std::setw(8) << std::setfill('0')
                        << ((int) &otherFrameList) << " )" );

         otherFrameList->changeFieldMediaLocation(oldMediaLocation,
                                                  newMediaLocation);
         oldFrameList = otherFrameList;

         } // if it's a new, non-NULL frame list
      } // for each framing type

   return 0;
}
#endif // __OBSOLETE__

// ---------------------------------------------------------------------------

int CTrack::assignVirtualMedia(MTI_UINT32 newDataSize)
{
   // Get the Video-Frames Frame List
   CFrameList* frameList = getBaseFrameList(FRAMING_SELECT_VIDEO);
   if (frameList == 0)
      {
      // ERROR: Video-Frames Frame List does not exist
      return -460;
      }

   frameList->assignVirtualMedia(newDataSize);

   return 0;
}

int CTrack::assignVirtualMedia(int startFrame, int frameCount,
                               MTI_UINT32 newDataSize)
{
   // Get the Video-Frames Frame List
   CFrameList* frameList = getBaseFrameList(FRAMING_SELECT_VIDEO);
   if (frameList == 0)
      {
      // ERROR: Video-Frames Frame List does not exist
      return -460;
      }

   frameList->assignVirtualMedia(startFrame, frameCount, newDataSize);

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Track Info Section Read/Write
//////////////////////////////////////////////////////////////////////

int CTrack::readTrackInfoSection(CIniFile *clipFile, const string& sectionName)
{
   string valueString, defaultString, emptyString;

   // Read the clip version
   valueString = clipFile->ReadString(sectionName, clipVerKey,
                              CClip::clipVerLookup.FindByValue(CLIP_VERSION_3));
   clipVer = CClip::clipVerLookup.FindByStr(valueString, CLIP_VERSION_INVALID);
   if (clipVer == CLIP_VERSION_INVALID)
      return CLIP_ERROR_INVALID_CLIP_VERSION;

   if (clipVer == CLIP_VERSION_5L)
      {
      // Read the clip 5 track id
      valueString = clipFile->ReadString(sectionName, trackIDKey, GetNullGUIDStr());
      Clip5TrackID newTrackID;
      if (!guid_from_string(newTrackID, valueString))
         {
         // ERROR: invalid track ID (GUID)
         return -1;
         }
      if (IsNullGUID(newTrackID))
         {
         // ERROR: invalid track ID
         return -1;
         }
      clip5TrackHandle.SetTrackID(newTrackID);
      }

   // Read the Handle Count
   int newHandleCount = clipFile->ReadInteger(sectionName, handleCountKey, -1);
   if (newHandleCount == -1)
      {
      // ERROR: Handle Count is missing or ill-formed
      return CLIP_ERROR_INVALID_HANDLE_COUNT;
      }

   handleCount = newHandleCount;

   defaultString = queryRecordStatusName(RECORD_STATUS_NOT_RECORDED);
   ERecordStatus newRecordStatus;

   // Read the AppId string
   appId = clipFile->ReadString(sectionName, appIdKey, emptyString);

   // Read the Virtual Media flag
   virtualMediaFlag = clipFile->ReadBool(sectionName, virtualMediaKey, false);

   // Read the Record to Computer Status
   valueString = clipFile->ReadString(sectionName, recordToComputerStatusKey,
                                      defaultString);
   newRecordStatus = queryRecordStatus(valueString);
   if (newRecordStatus == RECORD_STATUS_INVALID)
      return CLIP_ERROR_INVALID_RECORD_TO_COMPUTER_STATUS;

   recordToComputerStatus = newRecordStatus;

   // Read the Record to VTR Status
   valueString = clipFile->ReadString(sectionName, recordToVTRStatusKey,
                                      defaultString);
   newRecordStatus = queryRecordStatus(valueString);
   if (newRecordStatus == RECORD_STATUS_INVALID)
      return CLIP_ERROR_INVALID_RECORD_TO_VTR_STATUS;

   recordToVTRStatus = newRecordStatus;

   // Read the Last Media Action
   defaultString = queryMediaStatusName(MEDIA_STATUS_CREATED);
   valueString = clipFile->ReadString(sectionName, lastMediaActionKey,
                                      defaultString);
   EMediaStatus newMediaAction = queryMediaStatus(valueString);
   if (newMediaAction == MEDIA_STATUS_UNKNOWN)
      return CLIP_ERROR_INVALID_LAST_MEDIA_ACTION;

   lastMediaAction = newMediaAction;

   // Read the cadence repair tool status
   cadenceRepairFlag = clipFile->ReadBool (sectionName, cadenceRepairFlagKey,
		false);

   return 0;
}

EClipVersion CTrack::readClipVer(CIniFile *clipFile, const string& sectionName)
{
   // Read the clip version from the track section
   string defaultString = CClip::clipVerLookup.FindByValue(CLIP_VERSION_3);
   string valueString = clipFile->ReadString(sectionName, clipVerKey,
                                             defaultString);
   EClipVersion newClipVer = CClip::clipVerLookup.FindByStr(valueString,
                                                          CLIP_VERSION_INVALID);

   return newClipVer;
}


int CTrack::writeTrackInfoSection(CIniFile *clipFile, const string& sectionName)
{
   // Write the ClipVer string
   clipFile->WriteString(sectionName, clipVerKey,
                         CClip::clipVerLookup.FindByValue(clipVer));

   if (clipVer == CLIP_VERSION_5L)
      {
      // Write the track ID
      clipFile->WriteString(sectionName, trackIDKey,
                            clip5TrackHandle.GetTrackIDStr());
      }

   // Write the AppId String
   clipFile->WriteString(sectionName, appIdKey, appId);

   // Write the Handle Count
   clipFile->WriteInteger(sectionName, handleCountKey, handleCount);

   // Write the Virtual Media Flag
   clipFile->WriteBool(sectionName, virtualMediaKey, getVirtualMediaFlag());

   // Write the Record to Computer Status
   clipFile->WriteString(sectionName, recordToComputerStatusKey,
                         queryRecordStatusName(recordToComputerStatus));

   // Write the Record to VTR Status
   clipFile->WriteString(sectionName, recordToVTRStatusKey,
                         queryRecordStatusName(recordToVTRStatus));

   // Write the Last Media Action
   clipFile->WriteString(sectionName, lastMediaActionKey,
                         queryMediaStatusName(lastMediaAction));

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Track Section Read/Write
//////////////////////////////////////////////////////////////////////

int CTrack::readTrackSections(CIniFile *clipFile, const string& sectionPrefix,
                              CFramingInfoList& framingInfoList)
{
   int retVal;

   // Read the Track Info Section
   retVal = readTrackInfoSection(clipFile, sectionPrefix);
   if (retVal != 0)
      return retVal; // ERROR: Could not read the Video Proxy Info section

   if (clipVer == CLIP_VERSION_3)
      {
      // Clip 3

      // Read the Media Format section
      retVal = mediaFormat->readSection(clipFile, sectionPrefix);
      if (retVal != 0)
         return retVal; // ERROR: Could not read the Media Format section

      // Read Media Storage List Section
     retVal = mediaStorageList.readMediaStorageSection(clipFile, sectionPrefix);
      if (retVal == 0)
         {
         retVal = mediaStorageList.registerMediaStorage(*mediaFormat);
         if (retVal != 0)
            return retVal; // ERROR: Could not register the Media Storage
         }
      else if (retVal == CLIP_ERROR_CLIP_FILE_MEDIA_STORAGE_MISSING)
         {
         if (!getVirtualMediaFlag())
            {
            // Non-virtual media track and Media Storage section is missing
            return retVal;
            }
         }
      else if (retVal != 0)
         return retVal; // ERROR: Could not read the Media Storage section

      auto parentLocal = getParentClip();

      // If this is an interlaced video proxy track, update the Framing Info List
      // with the info for the Video-Fields Framing Info
      if (mediaFormat->getMediaFormatType() == MEDIA_FORMAT_TYPE_VIDEO3
          && mediaFormat->getInterlaced())
         {
         framingInfoList.createVideoFieldsFramingInfo(parentLocal,
                                                   FRAMING_TYPE_FIELDS_1_2,
                                                   FRAMING_SELECT_FIELDS_1_2);
         framingInfoList.createVideoFieldsFramingInfo(parentLocal,
                                                   FRAMING_TYPE_FIELD_1_ONLY,
                                                   FRAMING_SELECT_FIELD_1_ONLY);
         framingInfoList.createVideoFieldsFramingInfo(parentLocal,
                                                   FRAMING_TYPE_FIELD_2_ONLY,
                                                   FRAMING_SELECT_FIELD_2_ONLY);
         }

      // If this is a 525 or 1080I track, update the Framing Info List
      // with the info necessary to support the cadence repair tool
      if (mediaFormat->getMediaFormatType() == MEDIA_FORMAT_TYPE_VIDEO3
          && mediaFormat->getInterlaced())
         {
         // there doesn't appear to be a way to get the image format from
         // the media format.  This just means we are creating a
         // cadence repair framingInfoList for 625 which will never be used
         framingInfoList.createCadenceRepairFramingInfo (parentLocal,
         FRAMING_TYPE_CADENCE_REPAIR, FRAMING_SELECT_CADENCE_REPAIR);
         }

      // Read Track's Frame List sections
      retVal = readFrameListSections(clipFile, sectionPrefix, framingInfoList);
      if (retVal != 0)
         return retVal; // ERROR: Could not read a Video Track section
      }

   else if (clipVer == CLIP_VERSION_5L)
      {
      // Clip 5 Lite

      // Create list of dummy frame-list(s) used by legacy API
      retVal = createFrameList_List(framingInfoList);
      if (retVal != 0)
         return retVal;

      // Read the clip 5 track files
      retVal = clip5TrackHandle.ReadTrackFile(getParentClip()->getClipPathName());
      if (retVal != 0)
         return retVal;
      }
   else
      {
      // ERROR: invalid clip version
      return CLIP_ERROR_INVALID_CLIP_VERSION;
      }

   return 0;

} // readTrackSections( )

int CTrack::refreshMediaStorageSection(CIniFile *clipFile,
                                       const string& sectionPrefix)
{
   // Read Media Storage List Section
   int retVal = mediaStorageList.readMediaStorageSection(clipFile, sectionPrefix);
   if (retVal == CLIP_ERROR_CLIP_FILE_MEDIA_STORAGE_MISSING)
   {
      // Eror if non-virtual media track and Media Storage section is missing
      return getVirtualMediaFlag() ? 0 : retVal;
   }
   else if (retVal != 0)
   {
      // ERROR: Could not read the Media Storage section
      return retVal;
   }

   retVal = mediaStorageList.registerMediaStorage(*mediaFormat);
   if (retVal != 0)
   {
      // ERROR: Could not register the Media Storage
      return retVal;
   }

   return 0;
}


int CTrack::refreshTrackSections(CIniFile *clipFile,
                                 const string& sectionPrefix)
{
   int retVal;

   // Read the Track Info Section
   retVal = readTrackInfoSection(clipFile, sectionPrefix);
   if (retVal != 0)
      return retVal; // ERROR: Could not read the Video Proxy Info section

   return 0;

} // refreshTrackSections( )

int CTrack::writeTrackSections(CIniFile *clipFile, const string& sectionPrefix)
{
   int retVal;

   // Write the Track Info Section
   retVal = writeTrackInfoSection(clipFile, sectionPrefix);
   if (retVal != 0)
      return retVal; // ERROR: Could not write the Track Info section

   if (clipVer == CLIP_VERSION_3)
      {
      // Clip 3

      // Write the Media Format section
      retVal = mediaFormat->writeSection(clipFile, sectionPrefix);
      if (retVal != 0)
         return retVal; // ERROR: Could not write the Media Format section

      // Write Media Storage List Section
      retVal = mediaStorageList.writeMediaStorageSection(clipFile, sectionPrefix);
      if (retVal != 0)
         return retVal; // ERROR: Could not write the Media Storage section

      // TBD: Write Source Clip List Section

      // Write all Frame List sections
      retVal = writeFrameListSections(clipFile, sectionPrefix);
      if (retVal != 0)
         return retVal; // ERROR: Could not write all of the Frame List sections
      }

   return 0;

}  // writeTrackSections( )

//////////////////////////////////////////////////////////////////////
// Frame List Section Read/Write
//////////////////////////////////////////////////////////////////////

int CTrack::readFrameListSections(CIniFile *clipFile,
                                  const string& sectionPrefix,
                                  const CFramingInfoList& framingInfoList)
{
   int retVal;

   // Create the Track's list of Frame Lists
   retVal = createFrameList_List(framingInfoList);
   if (retVal != 0)
      return retVal; // ERROR: Failed to create the Track's list of Frame Lists

   CTimecode timecodePrototype = getParentClip()->getInTimecode();
   timecodePrototype.setAbsoluteTime(0);

   // Some image formats do not need a separate film-frames Frame List,
   // e.g. 625, so the Frame List entry for =film-frames =points to the
   // the same CFrameList as the video-frames Frame List.  If this is the case,
   // then read the section only once
   int framingIndex;
   if (frameList_List.size() > 1)
      {
      if (frameList_List[FRAMING_SELECT_VIDEO]
                                         == frameList_List[FRAMING_SELECT_FILM])
         framingIndex = FRAMING_SELECT_FILM;
      else
         framingIndex = FRAMING_SELECT_VIDEO;
      }
   else
      framingIndex = 0;

   // Iterate over all distinct entries in the Track's list of Frame Lists,
   // reading the section for each into the respective CFrameList instance
   for (; framingIndex < (int)frameList_List.size(); ++framingIndex)
      {
      CFrameList *frameList = frameList_List[framingIndex];

      if (framingIndex == FRAMING_SELECT_FIELDS_1_2
          || framingIndex == FRAMING_SELECT_FIELD_1_ONLY
          || framingIndex == FRAMING_SELECT_FIELD_2_ONLY
          || framingIndex == FRAMING_SELECT_CADENCE_REPAIR)
         {
         // Don't attempt to read non-existent frame-list info section
         // for video field frame lists
         if (frameList == 0)
            continue;   // No frame list, go on to the next
         retVal = frameList->initVideoFields_FrameList();
         if (retVal != 0)
            return retVal;
         }
      else
         {
         retVal = frameList->readFrameListInfoSection(clipFile, sectionPrefix,
                                                   timecodePrototype);
         if (retVal != 0)
            return retVal; // ERROR: Could not read the Frame List section
         }
      }

   return 0;

} // readFrameListSections( )

int CTrack::writeFrameListSections(CIniFile *clipFile,
                                   const string& sectionPrefix)
{
   int retVal;

   // Some image formats do not need a separate film-frames Frame List,
   // e.g. 625, so the Frame List entry for film-frames points to the
   // the same CFrameList as the video-frames Frame List.  If this is the case,
   // then write the section only once
   int framingIndex;
   if (frameList_List.size() > 1)
      {
      if (frameList_List[FRAMING_SELECT_VIDEO]
                                         == frameList_List[FRAMING_SELECT_FILM])
         framingIndex = FRAMING_SELECT_FILM;
      else
         framingIndex = FRAMING_SELECT_VIDEO;
      }
   else
      framingIndex = 0;

   // Iterate over all distinct entries in the Track's list of Frame Lists,
   // writing the section for each respective CFrameList instance
   for (; framingIndex < (int)frameList_List.size(); ++framingIndex)
      {
      // Don't attempt to write a frame-list info section
      // for video field frame lists
      if (framingIndex == FRAMING_SELECT_FIELDS_1_2
          || framingIndex == FRAMING_SELECT_FIELD_1_ONLY
          || framingIndex == FRAMING_SELECT_FIELD_2_ONLY
          || framingIndex == FRAMING_SELECT_CADENCE_REPAIR)
         continue;   // skip this framing index

      CFrameList *frameList = frameList_List[framingIndex];
      retVal = frameList->writeFrameListInfoSection(clipFile, sectionPrefix);
      if (retVal != 0)
         return retVal; // ERROR: Could not write the Frame List Info section
      }

   return 0;

}  // writeFrameListSections( )

//////////////////////////////////////////////////////////////////////
// Read/Write Frame List File
//////////////////////////////////////////////////////////////////////

int CTrack::writeFrameListFile(int framingIndex)
{
   int retVal;

   CFrameList *frameList = getBaseFrameList(framingIndex);
   if (frameList != 0)
      {
      retVal = frameList->writeFrameListFile();
      if (retVal != 0)
         return retVal; // ERROR: Failed to write Frame List file
      }

   return 0;

} // writeFrameListFile


int CTrack::writeAllFrameListFiles()
{
   // Doesn't really write all... knows that only the film and video
   // framelists ever get written to disk, and that they might be the
   // same, so only want to be written once.

   int retVal1 = 0;
   int retVal2 = 0;

   // We knnow that only the video and film tracks
   // would ever want to get written.
   CFrameList *videoFramingFrameList = getBaseFrameList(FRAMING_SELECT_VIDEO);
   CFrameList *filmFramingFrameList = getBaseFrameList(FRAMING_SELECT_FILM);

   // Avoid writing frame list file twice when video-frames
   // and film-frames frame lists are actually the same list
   if (videoFramingFrameList != NULL && filmFramingFrameList != NULL
       && videoFramingFrameList == filmFramingFrameList)
      {
      filmFramingFrameList = NULL; // Skip the duplicate frame list
      }

   // Write the frame lists to disk
   // CAREFUL! Do not try to write a file if it was never read in
   // ("not available") else it will trash the file
   if ((videoFramingFrameList != NULL) &&
      videoFramingFrameList->isFrameListAvailable())
      {
      retVal1 = videoFramingFrameList->writeFrameListFile();
      }
   if ((filmFramingFrameList != NULL) &&
      filmFramingFrameList->isFrameListAvailable())
      {
      retVal2 = filmFramingFrameList->writeFrameListFile();
      }

   if (retVal1 != 0)
      return retVal1; // ERROR: Failed to write video Frame List file
   if (retVal2 != 0)
      return retVal2; // ERROR: Failed to write film Frame List file

   return 0;

} // writeAllFrameListFiles

//////////////////////////////////////////////////////////////////////
// Rename Frame List File
//////////////////////////////////////////////////////////////////////

int CTrack::renameFrameListFile(int framingIndex, string newClipName)
{
   int retVal;

   CFrameList *frameList = getBaseFrameList(framingIndex);
   if (frameList != 0)
      {
      retVal = frameList->renameFrameListFile(newClipName);
      if (retVal != 0)
         return retVal; // ERROR: Failed to write Frame List file
      }

   return 0;

} // renameFrameListFile

int CTrack::renameAllFrameListFiles(string newClipName)
{
   int retVal;

   if (getClipVer() != CLIP_VERSION_3)
       return 0; // Clip 5 doesn't put clip name in framelist file names

   // Avoid writing frame list file twice when video-frames
   // and film-frames frame lists are actually the same list
   CFrameList *videoFramingFrameList = getBaseFrameList(FRAMING_SELECT_VIDEO);
   CFrameList *filmFramingFrameList = getBaseFrameList(FRAMING_SELECT_FILM);
   int framingIndex = 0;
   if (videoFramingFrameList != 0 && filmFramingFrameList != 0
       && videoFramingFrameList == filmFramingFrameList)
      {
      framingIndex = 1; // Skip the duplicate frame list
      }

   for (; framingIndex < (int)frameList_List.size(); ++framingIndex)
      {
      retVal = renameFrameListFile(framingIndex, newClipName);
      if (retVal != 0)
         return retVal; // ERROR: Failed to write Frame List file
      }
   return 0;

} // renameAllFrameListFiles

//---------------------------------------------------------------------

int CTrack::preLoadFieldList()
{
   int retVal;

   if (clipVer == CLIP_VERSION_5L)
      return 0;   // do nothing, function obsolete for clipo 5

   // Force the read of the field list file if it has not been read yet
   retVal = fieldList->preLoadFieldList();
   if (retVal != 0)
      return retVal;

   return 0;
}

int CTrack::reloadFieldList()
{
   int retVal;

   if (clipVer == CLIP_VERSION_3)
      {
      // re-read of the field list file
      retVal = fieldList->reloadFieldList();
      if (retVal != 0)
         return retVal;
      }
   else if (clipVer == CLIP_VERSION_5L)
      {
      string clipPath = getParentClip()->getClipPathName();
      retVal = clip5TrackHandle.ReloadMediaStatusTrackFile(clipPath);
      if (retVal != 0)
         return retVal;
      }
   else
      return CLIP_ERROR_INVALID_CLIP_VERSION;


   return 0;
}

// Function: updateFieldListFile
//
// Update changed entries in field list back to field list file
// The file I/O buffer is internally allocated and release within the call.
// This is the original version of the function and is useful for
// infrequent calls, such as when the clip is created.
int CTrack::updateFieldListFile()
{
   int retVal;

   if (clipVer == CLIP_VERSION_5L)
      return 0;   // do nothing, function obsolete for clipo 5

   // Write back to file those field list entries that have changed
   retVal = fieldList->updateFieldListFile();
   if (retVal != 0)
      return retVal;

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    updateFieldListFile
//
// Description: Update changed entries in field list back to field list file.
//              Caller can supply arguments to specify the range
//              of frames to write to the file.
//              Caller can supply a buffer for file I/O.  This is
//              useful when this function is called many times and the
//              caller does not want repeated allocation/deallocation
//              of memory
//
// Arguments:   int firstFieldIndex   index of first field to check
//              int fieldCount        Number of fields to update.  If
//                                    set to -1, then all frames from
//                                    firstFrameIndex to end of track
//                                    are updated.
//              char *externalBuffer  Pointer to caller's file I/O buffer
//              unsigned int externalBufferSize
//                                    Size of caller's buffer, in bytes
//              If either buffer argument is zero, then the buffer is
//              internally allocated and released within the call.
//
// Returns:     0 = Success
//
//------------------------------------------------------------------------
int CTrack::updateFieldListFile(int firstFieldIndex, int fieldCount,
                                char *externalBuffer,
                                unsigned int externalBufferSize)
{
   int retVal;

   if (clipVer == CLIP_VERSION_5L)
      return 0;   // do nothing, function obsolete for clip 5

   // Write back to file those field list entries that have changed
   retVal = fieldList->updateFieldListFile(firstFieldIndex, fieldCount,
                                           externalBuffer, externalBufferSize);
   if (retVal != 0)
      return retVal;

   return 0;
}

// -----------------------------------------------------------------------
// Update Clip 5 Media Status Track File

int CTrack::UpdateMediaStatusTrackFile()
{
   int retVal;

   string clipPath = getParentClip()->getClipPathName();

   retVal = clip5TrackHandle.UpdateMediaStatusTrackFile(clipPath);
   if (retVal != 0)
      return retVal;

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Extra File Handles to Access Media
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    CreateNewFileHandle
//
// Description: Create a new file handle to allow multithreaded media reads.
//              The returned value is used as an argument to read media
//              functions.Each new file handle has a separate thread lock.
//              Multiple file handles are currently implemented for raw
//              video (VideoStore) and audio files only.  For other media
//              storage types, such as image files, this function always
//              returns 0, which is the index of the default file handle.
//              Note: Multiple file handles is feasible for image files,
//                    but was not implemented because it was not needed by
//                    Control Dailies.
//              When a thread is done with a file handle index for a
//              particular track, it can close the file handle by calling
//              ReleaseFileHandle.  Unreleased file handles are closed when
//              the application is closed.
//
// Arguments:   None
//
// Returns:     New file handle index.  0 is default value for
//              media types that have not implemented multiple file
//              handles.  -1 if an error.
//
//------------------------------------------------------------------------
int CTrack::CreateNewFileHandle()
{
   int retVal;
   CMediaAccess *mediaAccess = 0;

   if (clipVer == CLIP_VERSION_3)
      {
      // Scan through frames and fields, looking for the first field
      // with allocated media

      // Get the 0th frame list in the track.  This is assumed to be
      // the Video-Frames frame list (in a Video Proxy) or an Audio Track
      CFrameList *frameList = getBaseFrameList(0);
      if (frameList == 0)
         return -1;   // No frame list, so nothing to do

      // Only consider the fields that should be the "visible" fields
      // in the video-frames frame list
      int fieldCount = getMediaFormat()->getFieldCount();

      // Iterate over all frames in the frame list until mediaAccess is
      // non-NULL
      int frameCount = frameList->getTotalFrameCount();
      for (int i = 0; i < frameCount && mediaAccess == 0; ++i)
         {
         CFrame *frame = frameList->getBaseFrame(i);

         // Iterate over fields in the frame until mediaAccess is non-NULL
         for (int j = 0; j < fieldCount && mediaAccess == 0; ++j)
            {
            CField *field = frame->getBaseField(j);

            if (field->doesMediaStorageExist())
               {
               const CMediaLocation& mediaLocation = field->getMediaLocation();
               mediaAccess = mediaLocation.getMediaAccess();
               }
            }
         }
      }
   else if (clipVer == CLIP_VERSION_5L)
      {
      retVal = clip5TrackHandle.GetFirstMediaAccess(mediaAccess);
      if (retVal != 0)
         return -1;
      }

   if (mediaAccess != 0)
      return mediaAccess->CreateNewFileHandle();
   else
      return -1;    // Bad file handle index
}

//------------------------------------------------------------------------
//
// Function:    ReleaseFileHandle
//
// Description: Release a file handle previously created with
//              CreateNewFileHandle.  This function will not release the
//              default file handle (index = 0).  Except for index 0, after
//              a file handle is released, it will no longer be useable.
//
// Arguments:   int fileHandleIndex   Index of file handle previously
//                                    created by CreateNewFileHandle.
//
// Returns:     0 if success, non-zero if failure.
//
//------------------------------------------------------------------------
int CTrack::ReleaseFileHandle(int fileHandleIndex)
{
   int retVal;
   CMediaAccess *mediaAccess = 0;

   if (clipVer == CLIP_VERSION_3)
      {
      // Scan through frames and fields, looking for the first field
      // with allocated media

      // Get the 0th frame list in the track.  This is assumed to be
      // the Video-Frames frame list (in a Video Proxy) or an Audio Track
      CFrameList *frameList = getBaseFrameList(0);
      if (frameList == 0)
         return -1;   // No frame list, so nothing to do

      // Only consider the fields that should be the "visible" fields
      // in the video-frames frame list
      int fieldCount = getMediaFormat()->getFieldCount();

      // Iterate over all frames in the frame list until mediaAccess is
      // non-NULL
      int frameCount = frameList->getTotalFrameCount();
      for (int i = 0; i < frameCount && mediaAccess == 0; ++i)
         {
         CFrame *frame = frameList->getBaseFrame(i);

         // Iterate over fields in the frame until mediaAccess is non-NULL
         for (int j = 0; j < fieldCount && mediaAccess == 0; ++j)
            {
            CField *field = frame->getBaseField(j);

            if (field->doesMediaStorageExist())
               {
               const CMediaLocation& mediaLocation = field->getMediaLocation();
               mediaAccess = mediaLocation.getMediaAccess();
               }
            }
         }
      }
   else if (clipVer == CLIP_VERSION_5L)
      {
      retVal = clip5TrackHandle.GetFirstMediaAccess(mediaAccess);
      if (retVal != 0)
         return retVal;
      }

   if (mediaAccess == 0)
      return CLIP_ERROR_NO_MEDIA;

   retVal = mediaAccess->ReleaseFileHandle(fileHandleIndex);

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Media Storage Deallocation
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    deallocateMediaStorage
//
// Description: Deallocating the media storage owned by this CTrack.
//              This function does not delete the Frame List file(s).
//
// Arguments:
//
// Returns:     0 if success, non-zero if failure.
//
//------------------------------------------------------------------------
int CTrack::deallocateMediaStorage(CMediaDeallocatorList& mediaDeallocatorList)
{
   int retVal;

   if (clipVer == CLIP_VERSION_3)
      {
      retVal = deallocateMediaStorage_ClipVer3(mediaDeallocatorList);
      }
   else if (clipVer == CLIP_VERSION_5L)
      {
      retVal = deallocateMediaStorage_ClipVer5L(mediaDeallocatorList);
      }
   else
      retVal = CLIP_ERROR_INVALID_CLIP_VERSION;

   return retVal;
}

int CTrack::deallocateMediaStorage_ClipVer5L(CMediaDeallocatorList& mediaDeallocatorList)
{
   int retVal;

   int totalFrameCount = clip5TrackHandle.GetItemCount();

   string clipPath = getParentClip()->getClipPathName();
   retVal = clip5TrackHandle.FreeMedia(clipPath, 0, totalFrameCount);
   if (retVal != 0)
      return retVal;  // Releasing the media storage a failed!

   return 0;
}

int CTrack::deallocateMediaStorage_ClipVer3(CMediaDeallocatorList& mediaDeallocatorList)
{
   int retVal;

   mediaDeallocatorList.init(*getMediaStorageList());

   // Get the 0th frame list in the track.  This is presumed to be
   // the Video-Frames frame list (in a Video Proxy) or an Audio Track
   CFrameList *frameList = getBaseFrameList(0);
   if (frameList == 0)
      return 0;   // No frame list, so nothing to deallocate

   // Iterate over all frames in the frame list
   int frameCount = frameList->getTotalFrameCount();
   for (int i = 0; i < frameCount; ++i)
      {
      CFrame* frame = frameList->getBaseFrame(i);
      int fieldCount = frame->getTotalFieldCount();

      // Iterate over all fields in the frame
      for (int j = 0; j < fieldCount; ++j)
         {
         CField *field = frame->getBaseField(j);
         if (field->isVirtualMedia() || field->isWriteProtectedMedia()
             || (field->isSharedMedia() && !field->isSharedMediaFirst())
             || field->isBorrowedMedia())
            {
            // Media is not owned by this field or is write protected,
            // so don't deallocate
            continue;
            }

         const CMediaLocation& mediaLocation = field->getMediaLocation();
         retVal = mediaDeallocatorList.deallocate(mediaLocation);
         if (retVal != 0)
            return retVal; // ERROR: Could not free the media storage allocation
         }
      }

   return 0;
}

// ---------------------------------------------------------------------------

int CTrack::releaseMediaStorage(int startFrame, int frameCount)
{
   int retVal;

   if (clipVer == CLIP_VERSION_3)
      {
      retVal = releaseMediaStorage_ClipVer3(startFrame, frameCount);
      }
   else if (clipVer == CLIP_VERSION_5L)
      {
      // Clip 5 "Lite"
      retVal = releaseMediaStorage_ClipVer5L(startFrame, frameCount);
      }
   else
      {
      TRACE_0(errout << "ERROR: Invalid Clip Version: " << clipVer);
      retVal = CLIP_ERROR_INVALID_CLIP_VERSION;
      }

   return retVal;
}

int CTrack::releaseMediaStorage_ClipVer3(int startFrame, int frameCount)
{
   int retVal;

   if (getVirtualMediaFlag())
      return 0;   // virtual media track, cannot release media storage

   CMediaDeallocatorList mediaDeallocatorList;

   mediaDeallocatorList.init(*getMediaStorageList());

   // Get the 0th frame list in the track.  This is presumed to be
   // the Video-Frames frame list (in a Video Proxy) or an Audio Track
   CFrameList *frameList = getBaseFrameList(0);
   if (frameList == 0)
      return 0;   // No frame list, so nothing to deallocate

   int totalFrameCount = frameList->getTotalFrameCount();

   if (startFrame < 0 || startFrame >= totalFrameCount)
      return CLIP_ERROR_INVALID_FRAME_NUMBER;

   // Number of frames from startFrame argument to end of frame list
   int framesToEnd = totalFrameCount - startFrame;

   // Adjust caller's frame count 1) if it is -1, which indicates release to
   // end of frame list or 2) if it is larger than the number of frames
   // between the caller's start frame and the end  of the frame list.
   if (frameCount < 0 || frameCount > framesToEnd)
      frameCount = framesToEnd;

   int endFrame = startFrame + frameCount;

   // Iterate over all frames in the frame list
   for (int i = startFrame; i < endFrame; ++i)
      {
      CFrame* frame = frameList->getBaseFrame(i);
      int fieldCount = frame->getTotalFieldCount();

      // Iterate over all fields in the frame
      for (int j = 0; j < fieldCount; ++j)
         {
         CField *field = frame->getBaseField(j);
         if (field->isVirtualMedia() || field->isWriteProtectedMedia()
             || (field->isSharedMedia() && !field->isSharedMediaFirst())
             || field->isBorrowedMedia())
            {
            // Media is not owned by this field or is write protected,
            // so don't deallocate
            continue;
            }

         CMediaLocation mediaLocation = field->getMediaLocation();
         retVal = mediaDeallocatorList.deallocate(mediaLocation);
         if (retVal != 0)
            return retVal; // ERROR: Could not free the media storage allocation

         // Reset mediaLocation
         mediaLocation.setInvalid();
         mediaLocation.setMarked(true);
         field->setMediaLocation(mediaLocation);

         // Set field
         field->setMediaStorageExists(false);
         field->setMediaWritten(false);
         }
      }

   // Update the frame list's allocated frame count and allocation frontier
   frameList->getMediaAllocatedFrontier(true);

   if (getTrackType() == TRACK_TYPE_VIDEO)
      {
      // Free the video media storage
      retVal = mediaDeallocatorList.freeMediaStorage();
      if (retVal != 0)
         return retVal;
      }
   else if (getTrackType() == TRACK_TYPE_AUDIO)
      {
      // Free the audio media storage
      retVal = mediaDeallocatorList.freeMediaStorage();
      if (retVal != 0)
         return retVal;
      }

   // write clip files
   // Update the Field List file
   retVal = updateFieldListFile();
   if (retVal != 0)
      return retVal; // ERROR: Failed to write Field List file

   // Write the updated frame list (.trk) file
   retVal = writeFrameListFile(FRAMING_SELECT_VIDEO);
   if (retVal != 0)
      return retVal; // ERROR: Failed to write Frame List files

   retVal = getParentClip()->writeFile();
   if (retVal != 0)
      return retVal; // ERROR: Failed to write clip file

    return 0;

}  // releaseMediaStorage_ClipVer3

int CTrack::releaseMediaStorage_ClipVer5L(int startFrame, int frameCount)
{
   int retVal;

   const CMediaFormat *mediaFormat = clip5TrackHandle.GetMediaFormat();
   if (mediaFormat == 0)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   int fieldCount = mediaFormat->getFieldCount();
   if (fieldCount == 0)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   int totalFrameCount = clip5TrackHandle.GetItemCount() / fieldCount;

   if (startFrame < 0 || startFrame >= totalFrameCount)
      return CLIP_ERROR_INVALID_FRAME_NUMBER;

   // Number of frames from startFrame argument to end of frame list
   int framesToEnd = totalFrameCount - startFrame;

   // Adjust caller's frame count 1) if it is -1, which indicates release to
   // end of frame list or 2) if it is larger than the number of frames
   // between the caller's start frame and the end  of the frame list.
   if (frameCount < 0 || frameCount > framesToEnd)
      frameCount = framesToEnd;

   int itemCount = fieldCount * frameCount;

   string clipPath = getParentClip()->getClipPathName();
   retVal = clip5TrackHandle.FreeMedia(clipPath, startFrame * fieldCount,
                                       itemCount);
   if (retVal != 0)
      return retVal;  // Releasing the media storage a failed!

   // Update frontier
   CFrameList *frameList = getBaseFrameList(0);
   if (frameList != 0)
      frameList->getMediaAllocatedFrontier(true);

   return 0;

} // releaseMediaStorage_ClipVer5L

// ---------------------------------------------------------------------------

int CTrack::renameClipMedia(string newBinName, string newClipName)
{
   if (getClipVer() != CLIP_VERSION_3)
     return 0; // Clip 5 doesn't put clip name in these file names

   // Get the first media storage item in this track's media storage
   // list.
   CMediaStorage *mediaStorage = mediaStorageList.getMediaStorage(0);
   if (mediaStorage == 0)
      return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR; // no media storage ??

   CMediaAccess *mediaAccess = mediaStorage->getMediaAccessObj();
   if (mediaAccess == 0)
      return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR; // no media access??

   int retVal =
      mediaAccess->renameClipMedia(newBinName, newClipName);
   if (retVal != 0)
      {
      MTIostringstream ostr;
      static char const *func = "CTrack::renameClipMedia";
      ostr << func << " failed";
      TRACE_0(errout << ostr.str());
      return retVal;
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

EMediaStatus CTrack::queryMediaStatus(const string& mediaStatusName)
{
   for (int i = 0; i < (int)MEDIA_STATUS_COUNT; ++i)
      {
      if (EqualIgnoreCase(mediaStatusMap[i].name, mediaStatusName))
         return mediaStatusMap[i].mediaStatus;
      }

   return MEDIA_STATUS_UNKNOWN;
}

string CTrack::queryMediaStatusName(EMediaStatus mediaStatus)
{
   for (int i = 0; i < (int)MEDIA_STATUS_COUNT; ++i)
      {
      if (mediaStatusMap[i].mediaStatus == mediaStatus)
         return string(mediaStatusMap[i].name);
      }

   return string("Unknown");
}

namespace {
   inline int convertMediaStatusToInteger(EMediaStatus mediaStatus)
   {
      switch (mediaStatus)
         {
         default:
            return -1;                // really an error
         case MEDIA_STATUS_UNKNOWN:
            return 0;
         case MEDIA_STATUS_MARKED:
            return 1;
         case MEDIA_STATUS_CREATED:
            return 2;
         case MEDIA_STATUS_RECORD_TO_COMPUTER_PARTIAL:
            return 3;
         case MEDIA_STATUS_RECORD_TO_COMPUTER:
            return 4;
         case MEDIA_STATUS_MODIFIED:
            return 5;
         case MEDIA_STATUS_RECORD_TO_VTR_PARTIAL:
            return 6;
         case MEDIA_STATUS_RECORD_TO_VTR:
            return 7;
         case MEDIA_STATUS_RETIRED:
            return 8;
         case MEDIA_STATUS_DELETED:
            return 9;
         case MEDIA_STATUS_CADENCE_REPAIR:
            return 10;
         case MEDIA_STATUS_ALPHA_HIDDEN:
            return 11;
         case MEDIA_STATUS_ALPHA_UNHIDDEN:
            return 12;
			case MEDIA_STATUS_ALPHA_DESTROYED:
				return 13;
			case MEDIA_STATUS_FPS_CHANGED:
				return 14;
			case MEDIA_STATUS_FPS_CHANGED_TO_2398:
				return 15;
			case MEDIA_STATUS_FPS_CHANGED_TO_24:
				return 16;
			case MEDIA_STATUS_FPS_CHANGED_TO_25:
				return 17;
			case MEDIA_STATUS_FPS_CHANGED_TO_2997:
				return 18;
			case MEDIA_STATUS_FPS_CHANGED_TO_30:
				return 19;
			}
    }
} // End anonymous namespace

// Compare two media statuses
// Returns:
//    -1 = "left less than right"
//     0 = equal
//     1 = "left greater than right"
//
int CTrack::compareMediaStatus(EMediaStatus left, EMediaStatus right)
{
   int iLeft = convertMediaStatusToInteger(left);
   int iRight = convertMediaStatusToInteger(right);

   return ((iLeft < iRight)? -1 : ((iLeft == iRight)? 0 : 1));
}

ERecordStatus CTrack::queryRecordStatus(const string& recordStatusName)
{
   for (int i = 0; i < (int)RECORD_STATUS_COUNT; ++i)
      {
      if (EqualIgnoreCase(recordStatusMap[i].name, recordStatusName))
         return recordStatusMap[i].recordStatus;
      }

   return RECORD_STATUS_INVALID;
}

string CTrack::queryRecordStatusName(ERecordStatus recordStatus)
{
   for (int i = 0; i < (int)RECORD_STATUS_COUNT; ++i)
      {
      if (recordStatusMap[i].recordStatus == recordStatus)
         return string(recordStatusMap[i].name);
      }

   return string("INVALID");
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                       ########################################
// ###    CTrackList Class     #######################################
// ####                       ########################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTrackList::CTrackList()
{
}

CTrackList::~CTrackList()
{
   deleteTrackList();
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CTrackList::getNewTrackNumber() const
{
   // Search the Track List to find the highest Track number in use
   int maxTrackNumber = -1;
   for (int trackIndex = 0; trackIndex < getTrackCount(); ++trackIndex)
      {
      CTrack* track = getBaseTrack(trackIndex);
      int trackNumber = track->getTrackNumber();
      if (track != 0 && maxTrackNumber < trackNumber)
         {
         maxTrackNumber = trackNumber;
         }
      }

   // Add one to get a new Track number higher than all others
   return maxTrackNumber + 1;
}

//////////////////////////////////////////////////////////////////////
// Video Proxy List Management
//////////////////////////////////////////////////////////////////////

void CTrackList::addTrack(CTrack *newTrack)
{
   trackList.push_back(newTrack);
}

void CTrackList::deleteTrackList()
{
   if (trackList.empty())
      return;

   for (int i = 0; i < getTrackCount(); ++i)
      {
      delete trackList[i];
      }

   trackList.clear();
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

CTrack* CTrackList::getBaseTrack(int trackIndex) const
{
   if (trackIndex < 0 || trackIndex >= getTrackCount())
      {
      // Track Index is out-of-range
      return 0;
      }

   return trackList[trackIndex];
}

int CTrackList::getTrackCount() const
{
   return (int)trackList.size();
}

//////////////////////////////////////////////////////////////////////
// AppId Search
//////////////////////////////////////////////////////////////////////

CTrack* CTrackList::findBaseTrack(const string &appId) const
{
   // Find the first CTrack in the Track List that has an appId
   // that matches (case-sensitive) the caller's appId
   // Returns pointer to CTrack instance if found, NULL pointer if not found

   for (int index = 0; index < getTrackCount(); ++index)
      {
      CTrack *baseTrack = getBaseTrack(index);
      if (baseTrack->getAppId() == appId)
         return baseTrack;
      }

   return 0; // Match was not found
}

int CTrackList::findBaseTrackIndex(const string &appId) const
{
   // Find the Track List index of the first CTrack in the Track List
   // that has an appId that matches (case-sensitive) the caller's appId
   // Returns index into the Track List if found, -1 if not found.

   for (int index = 0; index < getTrackCount(); ++index)
      {
      CTrack *baseTrack = getBaseTrack(index);
      if (baseTrack->getAppId() == appId)
         return index;
      }

   return -1;  // Match was not found
}

//////////////////////////////////////////////////////////////////////
// Read & Write Parameters
//////////////////////////////////////////////////////////////////////

int CTrackList::readTrackListSection(CIniFile *iniFile,
                                     const string& sectionPrefix, ClipSharedPtr &clip,
                                     CFramingInfoList& framingInfoList)
{
   int retVal;
   int trackNumber;
   MTIostringstream ostr;
   string emptyString;
   CTrack *newTrack;

   string trackListSection = sectionPrefix + "List";
   string trackInfoSection = sectionPrefix + "Info";

   // Read the maximum track index
   int maxTrackIndex = iniFile->ReadInteger(trackListSection, maxTrackIndexKey,
                                            MAX_TRACK_INDEX_DEFAULT);

   // Get Proxy Information
   for (int trackIndex = 0; trackIndex <= maxTrackIndex; ++trackIndex)
      {
      // Construct Track key such as "VideoProxy0"
      ostr.str(emptyString);
      ostr << sectionPrefix << trackIndex;
      // Read the Track number from the Track List section
      trackNumber = iniFile->ReadInteger(trackListSection, ostr.str(),
                                         TRACK_NUMBER_NO_TRACK);
      if (trackNumber == TRACK_NUMBER_NO_TRACK)
         {
         newTrack = 0;
         addTrack(newTrack);
         }
      else
         {
         // Create instance of class derived from CTrack and add to
         // the track list
         newTrack = createDerivedTrack(clip, CLIP_VERSION_INVALID, trackNumber);

         // Construct Track Information section prefix such as
         // "VideoProxyInfo0"
         ostr.str(emptyString);
         ostr << trackInfoSection << trackNumber;
         // Read derived Track Info sections (Image Format, etc)
         retVal = newTrack->readTrackSections(iniFile, ostr.str(),
                                              framingInfoList);
         if (retVal != 0)
            {
            // ERROR: Failed to read Track information section
            return retVal;
            }
         }
      }

   return 0;

} // readTrackListSection( )

int CTrackList::refreshTrackListSection(CIniFile *iniFile,
                                        const string& sectionPrefix)
{
   int retVal;
   int trackNumber;
   MTIostringstream ostr;
   string emptyString;
   CTrack *track;

   string trackInfoSection = sectionPrefix + "Info";

   // Refresh Track Information for each track in track list
   int trackIndex;
   for (trackIndex = 0; trackIndex <= getTrackCount(); ++trackIndex)
      {
      track = getBaseTrack(trackIndex);

      if (track != 0)
         {
         trackNumber = track->getTrackNumber();

         // Construct Video Proxy Information section prefix such as
         // "VideoProxyInfo0"
         ostr.str(emptyString);
         ostr << trackInfoSection << trackNumber;
         // Write Track Info sections (Image Format, etc)
         retVal = track->refreshTrackSections(iniFile, ostr.str());
         if (retVal != 0)
            return retVal; // ERROR: Failed to refresh from Track information
                           //        section
         }
      }

   return 0;

} // refreshTrackListSection( )

int CTrackList::writeTrackListSection(CIniFile *iniFile,
                                      const string& sectionPrefix)
{
   int retVal;
   int trackIndex, trackNumber;
   MTIostringstream ostr;
   string emptyString;
   CTrack *track;

   string trackListSection = sectionPrefix + "List";
   string trackInfoSection = sectionPrefix + "Info";

   // Determine the highest index of all Tracks
   int maxTrackIndex = VIDEO_SELECT_NORMAL;
   for (trackIndex = 0; trackIndex < getTrackCount(); ++trackIndex)
      {
      track = getBaseTrack(trackIndex);
      if (track != 0)
         {
         maxTrackIndex = trackIndex;
         }
      }
   // Write the maximum Track index
   iniFile->WriteInteger(trackListSection, maxTrackIndexKey,
                         maxTrackIndex);

   // Set Track Information
   for (trackIndex = 0; trackIndex <= maxTrackIndex; ++trackIndex)
      {
      track = trackList[trackIndex];
      if (trackIndex > VIDEO_SELECT_NORMAL && track == 0)
         {
         // Skip past Video Proxy List entries without a proxy
         break;
         }

      if (track == 0)
         {
         trackNumber = TRACK_NUMBER_NO_TRACK;
         }
      else
         {
         trackNumber = track->getTrackNumber();

         // Construct Video Proxy Information section prefix such as
         // "VideoProxyInfo0"
         ostr.str(emptyString);
         ostr << trackInfoSection << trackNumber;
         // Write Track Info sections (Image Format, etc)
         retVal = track->writeTrackSections(iniFile, ostr.str());
         if (retVal != 0)
            {
            // ERROR: Failed to write Video Proxy information section
            return retVal;
            }
         }
      // Construct Video Proxy key such as "VideoProxy0"
      ostr.str(emptyString);
      ostr << sectionPrefix << trackIndex;
      // Write the proxy number to the Video Proxy List section
      iniFile->WriteInteger(trackListSection, ostr.str(), trackNumber);
      }

   return 0;

} // writeTrackListSection( )

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                            ###################################
// ###    CFieldListEntry Class     ##################################
// ####                            ###################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFieldListEntry::CFieldListEntry()
 : needsUpdate(false), flags(0), defaultMediaType(0),
   frameIndex(-1), fieldIndex(-1)
{
}

CFieldListEntry::~CFieldListEntry()
{
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

bool CFieldListEntry::doesMediaStorageExist() const
{
   return (flags & FIELD_LIST_MEDIA_EXISTS);
}

int CFieldListEntry::getDefaultMediaType() const
{
   return defaultMediaType;
}

int CFieldListEntry::getFieldIndex() const
{
   return fieldIndex;
}

MTI_UINT32 CFieldListEntry::getFlags() const
{
   return flags;
}

int CFieldListEntry::getFrameIndex() const
{
   return frameIndex;
}

bool CFieldListEntry::getNeedsUpdate() const
{
   return needsUpdate;
}

bool CFieldListEntry::isMediaBorrowed() const
{
   return (flags & FIELD_LIST_MEDIA_IS_BORROWED);
}

bool CFieldListEntry::isMediaDirty() const
{
   return (flags & FIELD_LIST_MEDIA_IS_DIRTY);
}

bool CFieldListEntry::isMediaWriteProtected() const
{
   return (flags & FIELD_LIST_WRITE_PROTECT);
}

bool CFieldListEntry::wasMediaWritten() const
{
   return (flags & FIELD_LIST_MEDIA_WRITTEN);
}

//////////////////////////////////////////////////////////////////////
// Mutators
//////////////////////////////////////////////////////////////////////

void CFieldListEntry::setDefaultMediaType(int newType)
{
   defaultMediaType = newType;

   setNeedsUpdate(true);
}

void CFieldListEntry::setFlags(MTI_UINT32 newFlags)
{
   flags = newFlags;

   setNeedsUpdate(true);
}

void CFieldListEntry::setMediaBorrowed(bool newMediaBorrowed)
{
   if (newMediaBorrowed)
      flags |= FIELD_LIST_MEDIA_IS_BORROWED;
   else
      flags &= ~FIELD_LIST_MEDIA_IS_BORROWED;

   setNeedsUpdate(true);
}

void CFieldListEntry::setMediaDirty(bool newMediaDirty)
{
   if (newMediaDirty)
      flags |= FIELD_LIST_MEDIA_IS_DIRTY;
   else
      flags &= ~FIELD_LIST_MEDIA_IS_DIRTY;

   setNeedsUpdate(true);
}

void CFieldListEntry::setMediaStorageExists(bool newMediaExists)
{
   if (newMediaExists)
      flags |= FIELD_LIST_MEDIA_EXISTS;
   else
      flags &= ~FIELD_LIST_MEDIA_EXISTS;

   setNeedsUpdate(true);
}

void CFieldListEntry::setMediaWriteProtected(bool newMediaWriteProtected)
{
   if (newMediaWriteProtected)
      flags |= FIELD_LIST_WRITE_PROTECT;
   else
      flags &= ~FIELD_LIST_WRITE_PROTECT;

   setNeedsUpdate(true);
}

void CFieldListEntry::setMediaWritten(bool newMediaWritten)
{
   if (newMediaWritten)
      flags |= FIELD_LIST_MEDIA_WRITTEN;
   else
      flags &= ~FIELD_LIST_MEDIA_WRITTEN;

   setNeedsUpdate(true);
}

void CFieldListEntry::setNeedsUpdate(bool newNeedsUpdate)
{
   needsUpdate = newNeedsUpdate;
}

void CFieldListEntry::setFieldIndex(int newIndex)
{
   fieldIndex = newIndex;
}

void CFieldListEntry::setFrameIndex(int newIndex)
{
   frameIndex = newIndex;
}

//////////////////////////////////////////////////////////////////////

void CFieldListEntry::dump(MTIostringstream& str)
{
   str << "  Field list entry:"
       << " type=" << getDefaultMediaType()
       << " index=(" << getFrameIndex() << ',' << getFieldIndex() << ')'
       << " flags=("
          << (doesMediaStorageExist()?"exists":"") << ','
          << (wasMediaWritten()?"written":"") << ','
          << (isMediaBorrowed()?"borrowed":"") << ','
          << (isMediaWriteProtected()?"protect":"") << ','
          << (isMediaDirty()?"dirty":"")
       << ')' << std::endl;
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                       ########################################
// ###    CFieldList Class     #######################################
// ####                       ########################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFieldList::CFieldList(CTrack *newParentTrack)
 : parentTrack(newParentTrack), fieldListAvailable(false)
{
}

CFieldList::~CFieldList()
{
   deleteFieldList();
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

CFieldListEntry *CFieldList::getField(int fieldIndex)
{
   if (fieldIndex < 0 || fieldIndex >= getFieldCount())
      return 0;

   if (!fieldListAvailable)
      {
      // Read the field list from the Field List file
      int retVal = readFieldListFile();
      if (retVal != 0)
         {
         fieldListAvailable = false;
         TRACE_0(errout << "ERROR: In CFieldList::getField, call to " << endl
                        << "       CFieldList::readFieldListFile failed with return value "
                        << retVal);
         string msg = "Could not read Field List file";
         theError.set(retVal, msg);

         return 0;
         }
      }

   return fieldList[fieldIndex];
}

int CFieldList::getFieldCount()
{
   if (!fieldListAvailable)
      {
      // Read the field list from the Field List file
      int retVal = readFieldListFile();
      if (retVal != 0)
         {
         fieldListAvailable = false;
         TRACE_0(errout << "ERROR: In CFieldList::getFieldCount, call to " << endl
                        << "       CFieldList::readFieldListFile failed with return value "
                        << retVal);
         string msg = "Could not read Field List file";
         theError.set(retVal, msg);

         return 0;
         }
      }

   return (int)fieldList.size();
}

string CFieldList::getFieldListFileExtension()
{
   return "fl1";
}

string CFieldList::getFieldListFileName() const
{
   return makeFieldListFileName();
}

string CFieldList::getFieldListFileNameWithExt() const
{
   string fileName = getFieldListFileName() + '.' + getFieldListFileExtension();

   return fileName;
}

CTrack* CFieldList::getParentTrack() const
{
   return parentTrack;
}

bool CFieldList::isFieldListAvailable() const
{
   return fieldListAvailable;
}

int CFieldList::preLoadFieldList()
{
   // read the field list file if it has not been read yet
   int retVal;

   if (!fieldListAvailable)
      {
      // Read the field list from the Field List file
      retVal = readFieldListFile();
      if (retVal != 0)
         {
         fieldListAvailable = false;
         TRACE_0(errout << "ERROR: In CFieldList::preLoadFieldList, call to " << endl
                        << "       CFieldList::readFieldListFile failed with return value "
                        << retVal);
         string msg = "Could not read Field List file";
         theError.set(retVal, msg);

         return retVal;
         }
      }

   return 0;
}

int CFieldList::reloadFieldList()
{
   // read the field list file if it has not been read yet
   int retVal;

   if (!fieldListAvailable)
      {
      // Read the field list from the Field List file
      retVal = readFieldListFile();
      if (retVal != 0)
         {
         fieldListAvailable = false;
         TRACE_0(errout << "ERROR: In CFieldList::reloadFieldList, call to " << endl
                        << "       CFieldList::readFieldListFile failed with return value "
                        << retVal);
         string msg = "Could not read Field List file";
         theError.set(retVal, msg);

         return retVal;
         }
      }
   else
      {
      // Reload the field list from the Field List file
      retVal = reloadFieldListFile();
      if (retVal != 0)
         {
         fieldListAvailable = false;
         TRACE_0(errout << "ERROR: In CFieldList::reloadFieldList, call to " << endl
                        << "       CFieldList::reloadFieldListFile failed with return value "
                        << retVal);
         string msg = "Could not reload Field List file";
         theError.set(retVal, msg);

         return retVal;
         }
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////
// List Management
//////////////////////////////////////////////////////////////////////

void CFieldList::addField(CFieldListEntry *field)
{
   fieldList.push_back(field);

   fieldListAvailable = true;
}

void CFieldList::deleteFieldList()
{
   // Delete CFieldListEntry in the field list
   int fieldCount = (int) fieldList.size();
   for (int i = 0; i < fieldCount; ++i)
      {
      delete fieldList[i];
      fieldList[i] = 0;
      }

   // Clear the field list
   fieldList.clear();

   fieldListAvailable = false;
}

string CFieldList::makeFieldListFileName() const
{
   MTIostringstream ostr;

   // Compose Field List File Name

   // Clip's Bin Path is directory
   ostr << AddDirSeparator(parentTrack->getParentClip()->getClipPathName());

   // Track Type: Video or Audio
   // NB: This isn't the right way to do this, should be subtyped or something
   ETrackType trackType = parentTrack->getTrackType();
   if (trackType == TRACK_TYPE_VIDEO)
      ostr << "vp";
   else if (trackType == TRACK_TYPE_AUDIO)
      ostr << "at";

   // Track Number
   ostr << parentTrack->getTrackNumber();

   // Field list
   ostr << "_fl";

   return ostr.str();
}

int CFieldList::createFieldList()
{
   int retVal;

   // Calculate the number of frames based on difference between out and in
   // timecodes and the handles at the beginning and end of the track
   auto parentClip = parentTrack->getParentClip();
   int userFrameCount = parentClip->getOutTimecode()
                         - parentClip->getInTimecode();
   int handleCount = parentTrack->getHandleCount();
   int totalFrameCount = userFrameCount + 2 * handleCount;

/*
   retVal = extendFieldList(totalFrameCount);
   if (retVal != 0)
      return retVal; // ERROR
*/

   const CMediaFormat* mediaFormat = parentTrack->getMediaFormat();
   int fieldCount = mediaFormat->getFieldCount();

   int totalFieldCount = totalFrameCount * fieldCount;

   for (int i = 0; i < totalFieldCount; ++i)
      {
      CFieldListEntry *field = new CFieldListEntry;
      addField(field);
      }

   return 0;

} // createFieldList

int CFieldList::extendFieldList(int newFrameCount)
{
   const CMediaFormat* mediaFormat = parentTrack->getMediaFormat();
   int fieldCount = mediaFormat->getFieldCount();

   int totalFieldCount = newFrameCount * fieldCount;

   // hack: call getFieldCount() to make sure the field list file has
   // been read
   getFieldCount();

   for (int i = 0; i < totalFieldCount; ++i)
      {
      CFieldListEntry *field = new CFieldListEntry;
      field->setNeedsUpdate(true);    // mark field to be written to file
      addField(field);
      }

   return 0;

} // extendFieldList

void CFieldList::setVideoFrameListBackReference(int fieldListEntryIndex,
                                                int frameIndex, int fieldIndex)
{
   CFieldListEntry *field = getField(fieldListEntryIndex);

   if (field == 0)
      return;  // entry doesn't exist, can't do anything

   field->setFrameIndex(frameIndex);
   field->setFieldIndex(fieldIndex);
}

//////////////////////////////////////////////////////////////////////
// Field List File Interface
//////////////////////////////////////////////////////////////////////

// Read the entire field list file
int CFieldList::readFieldListFile()
{
   int retVal;
   CFieldListFileReader fieldListFileReader;

   retVal = fieldListFileReader.readFieldListFile(*this);
   if (retVal != 0)
      return retVal;

   return 0;
}

// Reload the entire field list file
int CFieldList::reloadFieldListFile()
{
   int retVal;
   CFieldListFileReader fieldListFileReader;

   retVal = fieldListFileReader.reloadFieldListFile(*this);
   if (retVal != 0)
      return retVal;

   return 0;
}

// Write the entire field list file
int CFieldList::writeFieldListFile()
{
   int retVal;
   CFieldListFileWriter fieldListFileWriter;

   retVal = fieldListFileWriter.writeFieldListFile(*this);
   if (retVal != 0)
      return retVal;

   return 0;
}

// Update changed records in the field list file
int CFieldList::updateFieldListFile()
{
   int retVal;
   CFieldListFileUpdater fieldListFileUpdater;

   retVal = fieldListFileUpdater.updateFieldListFile(*this, 0, -1);
   if (retVal != 0)
      return retVal;

   return 0;
}

// Update changed records in the field list file
// Caller supplies a buffer for field list file I/O.  Buffer size is
// suggested to be at least 4096 bytes and a power of 2.
int CFieldList::updateFieldListFile(int firstFieldIndex, int fieldCount,
                                    char *externalBuffer,
                                    unsigned int externalBufferSize)
{
   int retVal;
   CFieldListFileUpdater fieldListFileUpdater(externalBuffer,
                                              externalBufferSize);

   retVal = fieldListFileUpdater.updateFieldListFile(*this, firstFieldIndex,
                                                     fieldCount);
   if (retVal != 0)
      return retVal;

   return 0;
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
// Global Functions
//////////////////////////////////////////////////////////////////////

/*
int TranslateFrameIndex24To30(int frameIndex)
{
   return (int)((30./24.) * (double)frameIndex + .5);
}

int TranslateFrameIndex30To24(int frameIndex)
{
   return (int)((24./30.) * (double)frameIndex + .5);
}
*/


int TranslateFrameIndex24To30(int frameIndex24, int AAFrameIndex)
{
   // Translate a frame index into a 24 frames-per-second frame list
   // into a frame index into a 30 fps frame list with 3:2 pulldown
   // AAFrameIndex argument is in 24 fps frame list

   // Translate frame labels: Aa -> Aa, Bb -> Bb, (Bc skipped), Cc -> Cd,
   // Dd -> Dd
   static int xlateFrameLabel[4] = {0, 1, 3, 4};

   int firstFrame24, lastFrame24;
   if (frameIndex24 == 0)
      return 0;
   else if (frameIndex24 > 0)
      {
      firstFrame24 = 0;
      lastFrame24 = frameIndex24;
      }
   else
      {
      firstFrame24 = frameIndex24;
      lastFrame24 = 0;
      }

   int firstFrameLabel = Get4FrameLabel(firstFrame24, AAFrameIndex);
   int firstAFrame = firstFrame24 + (4 - firstFrameLabel) % 4;

   int lastFrameLabel = Get4FrameLabel(lastFrame24, AAFrameIndex);
   int lastDFrameAdj = (lastFrameLabel + 1) % 4;
   int lastDFrame = lastFrame24 - lastDFrameAdj;

   int frameIndex30 = 0;

   // Whole blocks of Aa, Bb, Cc, Dd frames in 24 fps become whole
   // blocks of Aa, Bb, Bc, Cd, Dd in 30 fps with 3:2 pulldown
   if (lastDFrame >= firstAFrame + 3)
      frameIndex30 = (((lastDFrame - firstAFrame + 1) / 4) * 5);

   // Adjust output frame index if first frame is not an A Frame
   if (firstFrameLabel != 0)
      {
      frameIndex30 += 1;
      if (firstAFrame <= lastFrame24)
         {
         frameIndex30 += 4 - xlateFrameLabel[firstFrameLabel];
         }
      else
         {
         frameIndex30 += xlateFrameLabel[lastFrameLabel]
                         - xlateFrameLabel[firstFrameLabel];
         }
      }
   else if (lastFrame24 - firstFrame24 < 3)
      {
      frameIndex30 += 1;
      frameIndex30 += xlateFrameLabel[lastFrameLabel]
                      - xlateFrameLabel[firstFrameLabel];
      }

   if (lastFrameLabel != 3 && (lastDFrame >= firstFrame24))
      {
      frameIndex30 += lastDFrameAdj;
      if (lastFrameLabel == 2)
         frameIndex30 += 1;   // Last frame is a Cc Frame
      }

   frameIndex30 -= 1;

   if (frameIndex24 < 0)
      frameIndex30 = -frameIndex30;

   return frameIndex30;
}

//---------------------------------------------------------------------------


int TranslateFrameIndex30To24(int frameIndex30, int AAFrameIndex)
{
   // Translate a frame index into a 30 frames-per-second frame list
   // with 3:2 pulldown into a frame index into a 24 fps frame list
   // AAFrameIndex argument is in 30 fps frame list

   // Translation of frame labels Aa -> Aa, Bb -> Bb, Bc -> Cc,
   // Cd -> Cc, Dd -> Dd
   static int xlateFrameLabel[5] = {0, 1, 2, 2, 3};

   int firstFrame30, lastFrame30;
   if (frameIndex30 == 0)
      return 0;
   else if (frameIndex30 > 0)
      {
      firstFrame30 = 0;
      lastFrame30 = frameIndex30;
      }
   else
      {
      firstFrame30 = frameIndex30;
      lastFrame30 = 0;
      }

   int firstFrameLabel = Get3_2PulldownFrameLabel(firstFrame30, AAFrameIndex);
   int firstAFrame = firstFrame30 + (5 - firstFrameLabel) % 5;

   int lastFrameLabel = Get3_2PulldownFrameLabel(lastFrame30, AAFrameIndex);
   int lastDFrameAdj = (lastFrameLabel + 1) % 5;
   int lastDFrame = lastFrame30 - lastDFrameAdj;

   int frameIndex24 = 0;

   // Whole blocks of Aa, Bb, Bc, Cd, Dd in 30 fps with 3:2 pulldown
   // become whole blocks of Aa, Bb, Cc, Dd frames in 24 fps
   if (lastDFrame >= firstAFrame + 4)
      frameIndex24 = (((lastDFrame - firstAFrame + 1) / 5) * 4);

   // Adjust output frame index if first frame is not an A Frame
   if (firstFrameLabel != 0)
      {
      frameIndex24 += 1;
      if (firstAFrame <= lastFrame30)
         {
         frameIndex24 += 3 - xlateFrameLabel[firstFrameLabel];
         }
      else
         {
         frameIndex24 += xlateFrameLabel[lastFrameLabel]
                         - xlateFrameLabel[firstFrameLabel];
         }
      }
   else if (lastFrame30 - firstFrame30 < 4)
      {
      frameIndex24 += 1;
      frameIndex24 += xlateFrameLabel[lastFrameLabel]
                      - xlateFrameLabel[firstFrameLabel];
      }

   if (lastFrameLabel != 4 && (lastDFrame >= firstFrame30))
      {
      frameIndex24 += 1;
      frameIndex24 += xlateFrameLabel[lastFrameLabel];
      }

   frameIndex24 -= 1;

   if (frameIndex30 < 0)
      frameIndex24 = -frameIndex24;

   return frameIndex24;
}

int Get3_2PulldownFrameLabel(int frameIndex, int AAFrameIndex)
{
   // Determine the position in the 5-frame 3:2 Pulldown sequence of the
   // caller's frameIndex: Aa=0, Bb=1, Bc=2, Cd=3, Dd=4

   int label = (frameIndex - AAFrameIndex) % 5;
   if (label < 0)
      label += 5;

   return label;
}

int Get4FrameLabel(int frameIndex, int AAFrameIndex)
{
   // Determine the position in the 4-frame Non-Pulldown sequence of the
   // caller's frameIndex: Aa=0, Bb=1, Cc=2, Dd=3

   int label = (frameIndex - AAFrameIndex) % 4;
   if (label < 0)
      label += 4;

   return label;
}

CTimecode GetWholeHourTimecode(const CTimecode &timecode)
{
   int hours = timecode.getHours();
   int minutes = timecode.getMinutes();
   if (minutes >= 55 && hours < 99)
      {
      // Adjust the hours if the clip has some preroll, e.g.,
      // whole hour timecode for 01:59:00:00 will be 02:00:00:00 rather
      // than 01:00:00:00.  Threshold is 55 minutes.
      hours += 1;
      }

   CTimecode wholeHourTimecode = timecode;
   wholeHourTimecode.setTime(hours, 0, 0, 0);

   return wholeHourTimecode;
}

//////////////////////////////////////////////////////////////////////
