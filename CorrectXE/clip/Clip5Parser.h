// File: Clip5Parser.h
//
//       Parser implemented with XML Xerces Parser
//
// --------------------------------------------------------------------------

#ifndef CLIP5PARSERH
#define CLIP5PARSERH

// XERCES DOESN'T COMPILE under RAD Studio 10.3.3
#define NO_XERCES_HACK

// --------------------------------------------------------------------------

#ifndef NO_XERCES_HACK
#include <xercesc/sax2/DefaultHandler.hpp>

XERCES_CPP_NAMESPACE_USE
#endif

#include "machine.h"
#include "cliplibint.h"

#include <string>
#include <vector>
#if defined(__GNUG__) && (__GNUG__ < 3)
// g++ versions < 3 don't have the ostream header
#include <iostream>
#else
#include <ostream>
#endif

using std::string;
using std::vector;
using std::ostream;
using std::ostringstream;

// --------------------------------------------------------------------------
// Forward Declarations

class C5TrackHandle;
class CXMLStream;

// --------------------------------------------------------------------------

class C5XMLAttrib
{
public:
   C5XMLAttrib(const string& newName, const string &newValue);
   C5XMLAttrib(const C5XMLAttrib &src);
   virtual ~C5XMLAttrib();

   C5XMLAttrib& operator=(const C5XMLAttrib &src);

   void GetIntegerList(vector<int> &intList);// Comman-separated list of integers
   string GetValueString();
   MTI_INT64 GetValueInteger();
   MTI_UINT64 GetValueUnsignedInteger();
   bool GetValueBool();
   double GetValueDouble();

   void SetValueString(const string &newValue);
   void SetValueInteger(MTI_INT64 newValue);
   void SetValueUnsignedInteger(MTI_UINT64 newValue);
   void SetValueBool(bool newValue);
   void SetValueDouble(double newValue);

   void writeXMLStream(CXMLStream &xmlStream);

	void dump(ostringstream& ostrm, string &indentLevel);

public:
   string name;
   string value;
};

class C5XMLAttribList
{
public:
   C5XMLAttribList();
   C5XMLAttribList(const C5XMLAttribList &src);
   virtual ~C5XMLAttribList();

   C5XMLAttribList& operator=(const C5XMLAttribList &src);

   C5XMLAttrib* Add(const string &newName, const string &newValue);
   void Clear();
   bool DoesNameExist(const string &attribName);
   string Find(const string &attribName);
   C5XMLAttrib* FindAttrib(const string &attribName);
   unsigned int GetCount();
   C5XMLAttrib* GetAttrib(unsigned int index);

   void GetIntegerList(const string &attribName, vector<int> &intList);// Comman-separated list of integers
   string GetValueString(const string &attribName);
   string GetValueString(const string &attribName, const string &defaultValue);
   MTI_INT64 GetValueInteger(const string &attribName);
   MTI_INT64 GetValueInteger(const string &attribName, MTI_INT64 defaultValue);
   MTI_UINT64 GetValueUnsignedInteger(const string &attribName);
   MTI_UINT64 GetValueUnsignedInteger(const string &attribName,
									  MTI_UINT64 defaultValue);
   bool GetValueBool(const string &attribName);
   bool GetValueBool(const string &attribName, bool defaultValue);
   double GetValueDouble(const string &attribName);
   double GetValueDouble(const string &attribName, double defaultValue);

   void SetValueString(const string &attribName, const string &newValue);
   void SetValueInteger(const string &attribName, MTI_INT64 newValue);
   void SetValueUnsignedInteger(const string &attribName, MTI_UINT64 newValue);
   void SetValueBool(const string &attribName, bool newValue);
   void SetValueDouble(const string &attribName, double newValue);

   void writeXMLStream(CXMLStream &xmlStream);

   void dump(ostringstream& ostrm, string& indentLevel);

public:
   vector<C5XMLAttrib*> attributeList;  // list of attributes from this element

};

//-----------------------------------------------------------------------------

#ifndef NO_XERCES_HACK
class MTI_CLIPLIB_API C5SAXHandlers : public DefaultHandler
{
public:
    C5SAXHandlers();
    ~C5SAXHandlers();

    // Implementation of the SAX DocumentHandler interface
    void endDocument();
    void endElement( const XMLCh* const uri,
                     const XMLCh* const localname,
                     const XMLCh* const qname);
    void startDocument();
    void startElement(const   XMLCh* const    uri,
                      const   XMLCh* const    localname,
                      const   XMLCh* const    qname,
                      const   Attributes& attributes);

	void characters(const XMLCh* chars, const XMLSize_t length);
	void ignorableWhitespace(const XMLCh* chars, const unsigned long length);

    void comment(const XMLCh* chars, const unsigned long length) {};
	void startPrefixMapping(const XMLCh* , const XMLCh*) {};
	void endPrefixMapping(const XMLCh*) {};
	void processingInstruction(const XMLCh*, const XMLCh*) {};
	void elementDecl(const XMLCh*, const XMLCh*) {};
	InputSource* resolveEntity(const XMLCh*, const XMLCh*) { return NULL;};
	void internalEntityDecl(const XMLCh*, const XMLCh*) {};
	void skippedEntity(const XMLCh * const) {};
	void startEntity(const XMLCh * const) {};
	void endEntity(const XMLCh * const) {};
	void unparsedEntityDecl(const XMLCh*, const XMLCh *, const XMLCh *, const XMLCh *) {};
	void attributeDecl(const XMLCh*, const XMLCh*, const XMLCh*, const XMLCh *, const XMLCh *) {};
	void notationDecl(const XMLCh*, const XMLCh*, const XMLCh*) {};
	void startDTD(const XMLCh*, const XMLCh*, const XMLCh*) {};
   	void externalEntityDecl(const XMLCh*, const XMLCh*, const XMLCh*) {};

    // Implementation of the SAX ErrorHandler interface
    void warning(const SAXParseException& exception);
    void error(const SAXParseException& exception);
    void fatalError(const SAXParseException& exception);

private :
   int parseLevel;
   C5TrackHandle *currentTrackHandle;
};
#endif

//-----------------------------------------------------------------------------

MTI_CLIPLIB_API int ParseTrack(const string &filename);
MTI_CLIPLIB_API void C5TestSetup(void (*remoteClearMemo)(), void (*remoteAddLine)(const char*));

//-----------------------------------------------------------------------------

#endif // #ifndef CLIP5_PARSER_H



