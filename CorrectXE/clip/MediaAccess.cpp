// MediaAccess.cpp: implementation of the CMediaAccess abstract base class.
//
//////////////////////////////////////////////////////////////////////

#include "MediaAccess.h"
#include "MediaLocation.h"
#include "err_clip.h"
#include "MTImalloc.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMediaAccess::CMediaAccess(EMediaType newMediaType)
: mediaType(newMediaType), referenceCount(0)
{
}

CMediaAccess::CMediaAccess(EMediaType newMediaType,
                           const string& newMediaIdentifier,
                           const string& newHistoryDirectory)
: mediaType(newMediaType),
  mediaIdentifier(newMediaIdentifier),
  historyDirectory(newHistoryDirectory),
  referenceCount(0)
{
}

CMediaAccess::~CMediaAccess()
{
    // Close all the files in the raw disk list
}

//////////////////////////////////////////////////////////////////////
// Comparison Operators
//////////////////////////////////////////////////////////////////////

bool CMediaAccess::operator==(const CMediaAccess& rhs) const
{
   return (compare(rhs) == 0);
}

bool CMediaAccess::operator!=(const CMediaAccess& rhs) const
{
   return (compare(rhs) != 0);
}

bool CMediaAccess::operator<(const CMediaAccess& rhs) const
{
   return (compare(rhs) < 0);
}

bool CMediaAccess::operator>(const CMediaAccess& rhs) const
{
   return (compare(rhs) > 0);
}

int CMediaAccess::compare(const CMediaAccess& rhs) const
{
   // Comparison Order:
   //   media type       (numeric order of enum value)
   //   media identifier (case-sensitive lexicographical order)

   if (mediaType == rhs.mediaType)
      {
      // Return results of case-sensitive lexicographical string comparison
      return mediaIdentifier.compare(rhs.mediaIdentifier);
      }
   else if (mediaType < rhs.mediaType)
      {
      return -1;
      }
   else // if (mediaType > rhs.mediaType)
      {
      return 1;
      }
}

//////////////////////////////////////////////////////////////////////
// Accessor Member Functions
//////////////////////////////////////////////////////////////////////

MTI_UINT32 CMediaAccess::getDefaultDataOffset()
{
   // Default value for the Data Offset.  The Data Offset is the
   // offset from the start of a media buffer to the image or audio data
   // For now only media stored in Image Files uses this value, for
   // all others it is 0. 
   return 0;
}

const string& CMediaAccess::getMediaIdentifier() const
{
   return mediaIdentifier;
}

const string& CMediaAccess::getMediaRepositoryIdentifier() const
{
   return mediaIdentifier;
}

EMediaType CMediaAccess::getMediaType() const      
{
   return mediaType;
}

const string& CMediaAccess::getHistoryDirectory() const
{
   return historyDirectory;
}

string CMediaAccess::getImageFileName(const CMediaLocation &mediaLocation) const
{
   // Subclass must override where appropriate
   return string("");
}

string CMediaAccess::getHistoryFileName(const CMediaLocation &mediaLocation) const
{
   // Subclass must override where appropriate
   return string("");
}

//////////////////////////////////////////////////////////////////////
// Mutator Member Functions
//////////////////////////////////////////////////////////////////////

void CMediaAccess::setMediaIdentifier(const string& newMediaIdentifier)
{
   mediaIdentifier = newMediaIdentifier;
}

void CMediaAccess::setMediaType(EMediaType newMediaType)
{
   mediaType = newMediaType;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

bool CMediaAccess::supportsMediaType(EMediaType testMediaType) const
{
   if (testMediaType == mediaType)
      return true;
   else
      return false;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CMediaAccess::queryFreeSpaceEx(const CMediaFormat& mediaFormat,
                                   MTI_INT64 *totalFreeBytes,
                                   MTI_INT64 *maxContiguousFreeBytes)
{
   return queryFreeSpace(totalFreeBytes, maxContiguousFreeBytes);
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CMediaAccess::queryTotalSpace(MTI_INT64 *totalBytes)
{
    // derived classes must implement this to give a meaningful
    // response
    return CLIP_ERROR_MEDIA_ACCESS_INVALID;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

CMediaAllocator* CMediaAccess::extend(const CMediaFormat& mediaFormat,
                                      const CTimecode& frame0Timecode,
                                      MTI_UINT32 frameCount)
{
   // Implemented by subtypes
   theError.set(CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED);
   return 0;
}

int CMediaAccess::cancelExtend(CMediaAllocator *mediaAllocator)
{
   // Implemented by subtypes
   theError.set(CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED);
   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

MTI_UINT32 CMediaAccess::convertDataSizeToByteCount(MTI_UINT32 dataSize) const
{
   // Default conversion is 1-to-1
   return dataSize;
}

//////////////////////////////////////////////////////////////////////
// Reference Counting using with CMediaInterface's media access list
//////////////////////////////////////////////////////////////////////
// Note: incrRefCount and decrRefCount should only be called by
//       CMediaInterface

void CMediaAccess::incrRefCount()
{
   ++referenceCount;
}

int CMediaAccess::decrRefCount()
{
   --referenceCount;

   return referenceCount;

}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CMediaAccess::preload()
{
   // Base class behavior is to do nothing, derived classes can
   // preload if they want.

   return 0;
}

int CMediaAccess::CreateNewFileHandle()
{
   // This is the default function if the derived class does not implement
   // extra file handles.  The base class does not know about file handles,
   // so we just return 0, which is the default index that is always available.
   return 0;
}

int CMediaAccess::ReleaseFileHandle(int fileHandleIndex)
{
   // This is the default function if the derived class does not implement
   // extra file handles.  The base class does not know about file handles.
   // If the caller's fileHandleIndex is 0, then just do nothing as this is
   // the default handle that is never released.  If the caller's
   // fileHandleIndex is not zero, then something is awry.

   if (fileHandleIndex == 0)
      return 0;
   else
      {
      TRACE_0(errout << "ERROR: CMediaAccess:ReleaseFileHandle not implemented in base class");
      return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
      }
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CMediaAccess::readMT(MTI_UINT8* buffer, CMediaLocation &mediaLocation,
                         int fileHandleIndex)
{
   int retVal;

   retVal = read(buffer, mediaLocation);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CMediaAccess::readMT(MTI_UINT8* buffer, MTI_INT64 offset,
                         unsigned int size, int fileHandleIndex)
{
   int retVal;

   retVal = read(buffer, offset, size);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CMediaAccess::readWithOffset(MTI_UINT8* buffer, MTI_UINT8* &offsetBuffer,
                                 CMediaLocation &mediaLocation)
{
   int retVal;

   retVal = readWithOffsetMT(buffer, offsetBuffer, mediaLocation, 0);
   if (retVal != 0)
      return retVal;

   return 0;
}

// xxxmfioxxx
//bool CMediaAccess::canBeReadAsynchronously(CMediaLocation &mediaLocation)
//{
//	return false;
//}
//
//int CMediaAccess::startAsynchReadWithOffset(MTI_UINT8* buffer,
//														  MTI_UINT8* &offsetBuffer,
//														  CMediaLocation &mediaLocation,
//														  void* &readContext,
//														  bool dontNeedAlpha)
//{
//	readContext = NULL;
//	offsetBuffer = buffer;
//	return CLIP_ERROR_ASYNCH_IO_NOT_SUPPORTED_FOR_MEDIA;
//}
//
//bool CMediaAccess::isAsynchReadWithOffsetDone(void* readContext)
//{
//	return true;
//}
//
//int CMediaAccess::finishAsynchReadWithOffset(void* &readContext,
//															MTI_UINT8* &offsetBuffer)
//{
//	readContext = NULL;
//	return CLIP_ERROR_ASYNCH_IO_NOT_SUPPORTED_FOR_MEDIA;
//}
//
//bool CMediaAccess::canReadFrameBuffers(CMediaLocation &mediaLocation)
//{
//	return false;
//}
//
//int CMediaAccess::startAsyncFrameBufferRead(CMediaLocation& mediaLocation, void* &readContext)
//{
//	readContext = NULL;
//	return CLIP_ERROR_ASYNCH_IO_NOT_SUPPORTED_FOR_MEDIA;
//}
//
//bool CMediaAccess::isAsyncFrameBufferReadDone(void* readContext)
//{
//	return true;
//}
//
//int CMediaAccess::finishAsynchFrameBufferRead(void* &readContext, MediaFrameBuffer* &frameBufferOut)
//{
//	readContext = nullptr;
//	frameBufferOut = nullptr;
//	return CLIP_ERROR_ASYNCH_IO_NOT_SUPPORTED_FOR_MEDIA;
//}

int CMediaAccess::readWithOffsetMT(MTI_UINT8* buffer, MTI_UINT8* &offsetBuffer,
											  CMediaLocation &mediaLocation,
											  int fileHandleIndex)
{
	int retVal;

	offsetBuffer = buffer;       // no offset, return caller's buffer pointer

	retVal = readMT(buffer, mediaLocation, fileHandleIndex);
																// use readMT in derived type
	if (retVal != 0)
		return retVal;

	return 0;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CMediaAccess::readMultiple(int count, MTI_UINT8** bufferArray,
                               CMediaLocation mediaLocationArray[])
{
   return readMultipleMT(count, bufferArray, mediaLocationArray, 0);
}

int CMediaAccess::readMultipleMT(int count, MTI_UINT8** bufferArray,
                                 CMediaLocation mediaLocationArray[],
                                 int fileHandleIndex)
{
   int retVal = 0;

   for (int i = 0; i < count && retVal == 0; ++i)
      {
      retVal = readMT(bufferArray[i], mediaLocationArray[i], fileHandleIndex);
      }

   return retVal;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CMediaAccess::readWithOffsetMultiple(int count,
                                 MTI_UINT8** bufferArray,
                                 MTI_UINT8** offsetBufferArray,
                                 CMediaLocation mediaLocationArray[])
{
   int retVal = 0;

   for (int i = 0; i < count && retVal == 0; ++i)
      {
      retVal = readWithOffset(bufferArray[i], offsetBufferArray[i],
                              mediaLocationArray[i]);
      }

   return retVal;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CMediaAccess::writeMultiple(int count, MTI_UINT8** bufferArray,
										  CMediaLocation mediaLocationArray[])
{
   int retVal = 0;

   for (int i = 0; i < count && retVal == 0; ++i)
      {
      retVal = write(bufferArray[i], mediaLocationArray[i]);
      }

   return retVal;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CMediaAccess::fetchMediaFile(CMediaLocation &mediaLocation)
{
	return CLIP_ERROR_ASYNCH_IO_NOT_SUPPORTED_FOR_MEDIA;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CMediaAccess::readMediaFrameBuffer(MediaFrameBufferSharedPtr &frameBuffer,
													CMediaLocation &mediaLocation)
{
	frameBuffer = nullptr;
	return CLIP_ERROR_ASYNCH_IO_NOT_SUPPORTED_FOR_MEDIA;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CMediaAccess::writeMediaFrameBuffer(MediaFrameBufferSharedPtr frameBuffer,
													 CMediaLocation &mediaLocation)
{
	return CLIP_ERROR_ASYNCH_IO_NOT_SUPPORTED_FOR_MEDIA;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CMediaAccess::move(CMediaLocation &fromLocation,
							  CMediaLocation &toLocation)
{
   // Implemented by subtypes
   return CLIP_ERROR_MEDIA_CANNOT_BE_TRANSFERRED;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CMediaAccess::destroy(const CMediaLocation &mediaLocation)
{
   // Implemented by subtypes
   return CLIP_ERROR_MEDIA_CANNOT_BE_DESTROYED;
}

//////////////////////////////////////////////////////////////////////
//  a blatant hack
//////////////////////////////////////////////////////////////////////

int CMediaAccess::hack(EMediaHackCommand hackCommand,
                       CMediaLocation &mediaLocation)
{
   // Implemented by subtypes
   theError.set(CLIP_ERROR_MEDIA_CANNOT_BE_HACKED_AS_REQUESTED);
   return 0;  // QQQ Why not CLIP_ERROR_MEDIA_CANNOT_BE_HACKED_AS_REQUESTED
}

//** Clip-to-clip copyback hack
//////////////////////////////////////////////////////////////////////
//  Media movers
//////////////////////////////////////////////////////////////////////

/* static */
int CMediaAccess::moveMedia(CMediaLocation &fromLocation,
                            CMediaLocation &toLocation)
{
   int retVal;

   // If the MediaLocations have the same type of access, try to let the
   // corresponding MediaAccess class handle it, if it wants to
   CMediaAccess *fromMediaAccess = fromLocation.getMediaAccess();
   CMediaAccess *toMediaAccess = toLocation.getMediaAccess();
   if (fromMediaAccess->getMediaType() == toMediaAccess->getMediaType())
      {
      retVal = fromMediaAccess->move(fromLocation, toLocation);
      if (retVal == 0)
         return 0;
      }

   // Subtype refused to handle it, so we'll just use the generic
   // copy/delete method

   // Sanity
#if 0 // WTF ?!? These don't seem to always match!!
   MTI_UINT32 fromDataByteSize = fromLocation.getDataByteSize();
   if (fromDataByteSize != toLocation.getDataByteSize())
      {
      TRACE_0(errout << "ERROR: CMediaAccess::moveMedia: data size mismatch");
      return CLIP_ERROR_MEDIA_CANNOT_BE_TRANSFERRED;
      }
#endif

   // Else just copy the data to the "to" location, then try to delete
   // the media at the "from" location
   retVal = copyMedia(fromLocation, toLocation);
   if (retVal != 0)
      return retVal;

   // We ignore the return code from destroy() because some types of
   // media access won't actually destroy the data (e.g. videostore)
   fromLocation.destroy();

   return 0;
}

/* static */
int CMediaAccess::copyMedia(CMediaLocation &fromLocation,
                            CMediaLocation &toLocation)
{
   // QQQ WTF ?!? Magic number 4096 - how are we supposed to determine
   // the max offset and necessary alignment??
   static const int MAX_DATA_OFFSET = 4096;
   static const int MEMORY_ALIGNMENT = 4096;
   int fromLen = fromLocation.getDataByteSize();
   int toLen = toLocation.getDataByteSize();
   int bufferSize = MAX_DATA_OFFSET + ((fromLen > toLen)? fromLen : toLen);

   MTI_UINT8 *buffer = (MTI_UINT8*) MTImemalign(MEMORY_ALIGNMENT, bufferSize);
   if (buffer == NULL)
       return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

   MTI_UINT8 *data;
   int retVal = fromLocation.readWithOffset(buffer, data);
   if (retVal != 0)
      {
      MTIfree(buffer);
      return retVal;
      }

   //  HUH??? Ehat if there was an offset??? QQQ
   retVal = toLocation.write(data);
   MTIfree(buffer);
   buffer = NULL;
   data = NULL;
   if (retVal != 0)
      return retVal;

   return 0;
}
//**//

//** Stupid DPX Header Timecode Hack -- default is to do nothing
void CMediaAccess::setFrame0Timecode(const CTimecode &frame0Timecode)
{
}
//**//


