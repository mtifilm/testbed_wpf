#ifndef SaveRestoreH
#define SaveRestoreH

#include "cliplibint.h"
#include "machine.h"
#include "PixelRegions.h"
#include "ThreadLocker.h"
#include "time.h"

#include <string>
# pragma pack(push, 1)
using std::string;

enum EHistoryType
{
   HISTORY_TYPE_MONOLITHIC = 0,
   HISTORY_TYPE_FILE_PER_FRAME
};

// History error codes are defined in err_metadata.h

class CClip;
class CFrame;
class CImageFormat;
class CMask;
class CPixelChannel;
class CPixelRegionList;
class CThreadLock;
class CVideoFrameList;
struct RESTORE_RECORD;

////TODO WTF #define NULLEDG 0xffffffff

struct VERTICAL_EDGE
{
   int ifwd;
   int ibwd;
   MTI_UINT16 x;
   MTI_UINT16 ybot;
   MTI_UINT16 ytop;
};

class MTI_CLIPLIB_API CSaveRestore
{

public:

   CSaveRestore();

   ~CSaveRestore();

   // supply the ptr to the desired clip object
   int setClip(ClipSharedPtr &clip);

   // verify header file by regenerating .HED records
   int verifyHeaderFile();

   // get new global save number
   int newGlobalSaveNumber();

   // read the current value of the above
   int getGlobalSaveNumber();

   // Gets the history type.
   EHistoryType getHistoryType() { return historyType; }

protected:

   // New class that enforces atomicity of save/restore operations;
   // you cannot call openSave or openForRestore directly anymore!!
   friend class CAutoHistoryOpener;

   // open the facility for saving
   int openForSave();

   // close the facility after a series of save calls
   int flushSave();

   // close the facility after a series of restore calls
   int flushRestore();

   // get the history filepath for a frame
   string getHistoryFilePath(CFrame *frame);

   // get the history filepath for a frame number
   string getHistoryFilePath(const ClipSharedPtr &, int frmnum);

   // get the new history filepath for a frame
   string getNewHistoryFilePath(CFrame *frame);

   // get the history filepath for a frame number
   string getNewHistoryFilePath(const ClipSharedPtr &, int frmnum);

   // force the correct .hst file to be open
   int openHistoryFileForFrame(int frame);

   // if the .hst file exists then open it
   int openHistoryFileForFrameRestore(int frame);

   // open for sequencing through restore records, filtering
   // on a RECT or LASSO
   int openForRestore(
              int frame,     // index of affected frame
              int,           // index of affected field (-1 for wildcard)
              RECT *,        // -> restore box (NULL -> use entire frame)
              POINT *,       // -> restore lasso (NULL -> use rect)
              BEZIER_POINT * // -> restore bezier (NULL -> use lasso)
             );

   int openForRestore(
              int frame,   // index of affected frame
              int,         // index of affected field (-1 for wildcard)
              CPixelRegionList& // pixel region list
             );

   // discard N saves from open history file
   int discardNFixesFromHistoryFile(int frame, int numdscrd);

   bool differenceAt(MTI_UINT16 *iniFrame,
                     MTI_UINT16 *finFrame,
                     int x, int y);

   int codeAt(MTI_UINT16 *iniFrame,
              MTI_UINT16 *finFrame,
              int x, int y);
public:

   // for full-frame tools making no history
   int copyParentHistoryToVersion(int frame);

// values for field number
#define EVEN 0
#define ODD  1
#define EVEN_AND_ODD -1

   // save pixel data in the given pixel region
   int saveRegionList(
              int frame,      // frame index
              int fieldIndex, // field index
              int fieldNum,   // 0 even, 1 odd, -1 both
              int toolcod,    // tool code
              const char *pdlstr, // ->cmd string (PDL style)
              CPixelRegionList&
             );

   // save pixel data in the given pixel region
   int saveDifferenceRuns(
              int frame,      // frame index
              int fieldIndex, // field index
              int fieldNum,   // 0 even, 1 odd, -1 both
              int toolcod,    // tool code
              const char *pdlstr, // ->cmd string (PDL style)
              MTI_UINT16 *iniFrame, // unprocessed frame
              MTI_UINT16 *finFrame, // processed frame
              MTI_UINT16 *srcFrame, // src frame for pixels
              RECT *extent    // returns extent
             );

   // discard last N saves
   int discardNSaves(
		int numSaves // saves to discard
		);

   int nextRestoreRecord();

   void setRestoreWritebackFlag(bool);

   // methods to access current restore record
   RESTORE_RECORD *getCurrentRestoreRecord();
   int getCurrentRestoreRecordFrame();
   int getCurrentRestoreRecordFieldIndex();
   int getCurrentRestoreRecordFieldNumber();
   int getCurrentRestoreRecordGlobalSaveNumber();
   int getCurrentRestoreRecordToolCode();
   time_t getCurrentRestoreRecordSaveTime();
   int  getCurrentRestoreRecordRegionType();
   bool getCurrentRestoreRecordResFlag();
   void setCurrentRestoreRecordResFlag(bool);
   bool getCurrentRestoreRecordInvalidFlag();
   void setCurrentRestoreRecordInvalidFlag(bool);
   RECT *getCurrentRestoreRecordBoundingRectangle();
   char *getCurrentRestoreRecordPDLCmd();
   CPixelRegionList *getCurrentRestoreRecordPixelRegionList();

   // methods to control removal of bits from history after GOV
   int reviseAllGOVRegions();
   int reviseGOVRegions(int frameNumber);
   int discardGOVRevisions();

   // outputs a run to the GOV workfile relative to the useMask
   int govOutputRunResidue(MTI_UINT16 *runptr);

   // outputs the header and runs to the GOV workfile
   int govOutputHeaderAndRuns(MTI_UINT16 *rgnptr, int hdrlng);

   void copyRunToFrameBuffer(const MTI_UINT16 *run,
                             MTI_UINT16 *dstFrame,
                             int frameWidth, int frameHeight,
                             int frameComponentCount,
                             const MTI_UINT16 *mask);

   // restore pixel data from current restore record
   // to the given frame, marking the restore record
   int restoreCurrentRestoreRecordPixels(MTI_UINT16 *,bool mark=true);

   void copyRunToRectangleBufferSmall(const MTI_UINT16 *run,
                                      MTI_UINT16 *dstBuffer,
                                      RECT *rect,
                                      int frameComponentCount);

   // obtain the current restore record's pixel data
   // which applies to the argument rectangle and copy
   // it into a buffer representing ONLY the rectangle
   int obtainCurrentRestoreRecordPixelsSmall(MTI_UINT16 *);

   // obtain all original values from the history
   // for a given rectangle, in order most-recent
   // to least-recent. The destination buffer has
   // pitch equal to the inclusive width of the
   // rectangle (times pixel component count) and
   // height equal to the inclusive height of the
   // rectangle
   int obtainOriginalValuesSmall(MTI_UINT16 *, int frame, RECT *);

   // tests whether a history file contains fixes
   int HistoryFileContainsFixes(string &hstFilePath);

   // get the first PREVIOUS  frame with fixes on it,
   // given a current frame number. Return -1 if there
   // is none
   int getPreviousFrameWithFixes(int);

   // get the first FOLLOWING frame with fixes on it,
   // given a current frame number. Return -1 if there
   // is none
   int getNextFrameWithFixes(int);

   // Get the number of fixes for a specified clip
   int getNumberOfFixes(ClipSharedPtr &clip,
                        int *percentComplete = NULL,
                        int *partialResult = NULL,
                        int *continueCheckLHS = NULL,
                        int *continueCheckRHS = NULL);

   // replace history - used for committing version clips
   int replaceHistory(string const &srcFrameListFileName,
                      string const &dstFrameListFileName,
                      ClipSharedPtr &srcClip,
                      ClipSharedPtr &dstClip,
                      const vector<int> *frameIndexList,
                      int *progressPercent = NULL,
                      int *countDown=NULL);

   // trace the peripheries of the islands of difference
   vector <vector <POINT> *> * limnDifferenceRuns(MTI_UINT16 *iniFrame,
                                                  MTI_UINT16 *finFrame);

private:

   int openHistoryFiles();
   int closeHistoryFiles();

   int openHistoryFiles_MONO();
   int openHistoryFiles_FPF();
   int closeHistoryFiles_MONO();
   int closeHistoryFiles_FPF();
   int saveRegionList_MONO(
              int frame,      // frame index
              int fieldIndex, // field index
              int fieldNum,   // 0 even, 1 odd, -1 both
              int toolcod,    // tool code
              const char *pdlstr, // ->cmd string (PDL style)
              CPixelRegionList&
             );
   int saveRegionList_FPF(
              int frame,      // frame index
              int fieldIndex, // field index
              int fieldNum,   // 0 even, 1 odd, -1 both
              int toolcod,    // tool code
              const char *pdlstr, // ->cmd string (PDL style)
              CPixelRegionList&
             );
   int saveDifferenceRuns_MONO(
              int frame,      // frame index
              int fieldIndex, // field index
              int fieldNum,   // 0 even, 1 odd, -1 both
              int toolcod,    // tool code
              const char *pdlstr, // ->cmd string (PDL style)
              MTI_UINT16 *iniFrame, // unprocessed frame
              MTI_UINT16 *finFrame, // processed frame
              MTI_UINT16 *srcFrame, // src frame for pixels
              RECT *extent    // returns extent
             );
   int saveDifferenceRuns_FPF(
              int frame,      // frame index
              int fieldIndex, // field index
              int fieldNum,   // 0 even, 1 odd, -1 both
              int toolcod,    // tool code
              const char *pdlstr, // ->cmd string (PDL style)
              MTI_UINT16 *iniFrame, // unprocessed frame
              MTI_UINT16 *finFrame, // processed frame
              MTI_UINT16 *srcFrame, // src frame for pixels
              RECT *extent    // returns extent
             );
   int flushSave_MONO();
   int flushSave_FPF();
   int flushRestore_MONO();
   int flushRestore_FPF();
   int discardNSaves_MONO(int numSaves);
   int discardNSaves_FPF(int numSaves);
   int nextRestoreRecord_MONO();
   int nextRestoreRecord_FPF();
   int getPreviousFrameWithFixes_MONO(int frmnum);
   int getPreviousFrameWithFixes_FPF(int frmnum);
   int getNextFrameWithFixes_MONO(int frmnum);
   int getNextFrameWithFixes_FPF(int frmnum);
   int replaceHistory_MONO(string const &srcFrameListFileName,
                      string const &dstFrameListFileName,
                      ClipSharedPtr &srcClip,
                      ClipSharedPtr &dstClip,
                      const vector<int> *frameIndexList,
                      int *progressPercent = NULL,
                      int *countDown=NULL);
   int replaceHistory_FPF(string const &srcFrameListFileName,
                      string const &dstFrameListFileName,
                      ClipSharedPtr &srcClip,
                      ClipSharedPtr &dstClip,
                      const vector<int> *frameIndexList,
                      int *progressPercent = NULL,
                      int *countDown=NULL);

   // sort the edges by min vertex in lex order
   static bool compareEdges(VERTICAL_EDGE *edg1, VERTICAL_EDGE *edg2);

   // insert edge-record into scan line list
   // between two other edges
   void insertEdge(vector <VERTICAL_EDGE> &edgbuf, int edg, int preedg, int nxtedg);

   // delete edge-record from scan line list
   void deleteEdge(vector <VERTICAL_EDGE> &edgbuf, int edg);

   // Print out the passed in RESTORE_RECORD
   void dumpRestoreRecord (RESTORE_RECORD *vpResBuf);

   // Copy a RESTORE_RECORD
   void copyRestoreRecord (RESTORE_RECORD *rrResBufDst, RESTORE_RECORD *rrResBufSrc);

   /////////////////////////////////////////////////////////////////////////////

   // index of first member of scan-line list (used when generating
   // runs from the sorted vector of vertical edges obtained by
   // tracing around the outside of a blob). The scan-line algorithm
   // will enable us to collect all the runs belonging to the blob
   // in order left-to-right, top-to-bottom. This way the runs for a
   // blob will be entered into the History in proper order.
   int iEdgeList;

   // global save number
   int globalSaveNumber;

   /////////////////////////////////////////////////////////////

   EHistoryType historyType;

   /////////////////////////////////////////////////////////////

   ClipSharedPtr srcClip;
   CVideoFrameList *srcVideoFrameList;
   int srcInFrame;
   int srcOutFrame;

   /////////////////////////////////////////////////////////////

   ClipSharedPtr dstClip;
   CVideoFrameList *dstVideoFrameList;

   /////////////////////////////////////////////////////////////
   // clip image format dims
   int frmWdth;
   int frmHght;
   int pxlComponentCount;

   //////////////////////////////////////////////////////////////

   vector <int> FixDiscardList;

   //////////////////////////////////////////////////////////////
   //
   string historyPathName;       // monolithic

   string srcClipHistoryFolder;  // file-per-frame

   // 4 bytes to be read from beg of each .HST file
   //int resRecCount;

   /// .HDR FILE //////////////////////////////////////////////
   //
   // this file contains one record for an entire block of
   // RESTORE records. It is used to rapidly locate the
   // RESTORE records which are relevant to a given frame

   struct HEADER_RECORD {

      // minimum index of all frames referenced
      // by RESTORE records in this .res block
      int minFrame;

      // maximum index of all frames referenced
      // by RESTORE records in this .res block
      int maxFrame;

   };

   // the blocking factor determines the size of the
   // buffer used to access the .HDR file:
   // size = blocking factor * sizeof(HEADER_RECORD)

#define HEADER_BLOCKING_FACTOR 1024
#define HEADER_BUFFER_SIZE (HEADER_BLOCKING_FACTOR*sizeof(HEADER_RECORD))

   // full pathname for .hdr file
   string hdrFileName;

   // header file handle
   int hdrFileHandle;

   // header file ptr (cur write pos)
   int hdrFilePtr;

   // disk addr of cur block of .HDR records
   int hdrBaseFileOffset;

   // length of .HDR block
   int hdrBlkLeng;

   // ptr to cur .HDR record in its buffer
   HEADER_RECORD *hdrRecPtr;

   // the header file buffer
   HEADER_RECORD *hdrBuf;

   // -> just past the header file buffer
   HEADER_RECORD *hdrBufStop;

   /// .RES FILE //////////////////////////////////////////////
   //
   // this file contains one restore-record for every instance of
   // a saved rectangle in a FIELD . It is used to rapidly locate
   // the PDL CMD string which generated the save and the PIXEL
   // DATA for the specified BOX

   // the blocking factor determines the size of the
   // buffer used to access the .RES file:
   // size = blocking factor * sizeof(RESTORE_RECORD)

#define RESTORE_BLOCKING_FACTOR 64
#define RESTORE_BUFFER_SIZE (RESTORE_BLOCKING_FACTOR*sizeof(RESTORE_RECORD))

   // index of .RES block (and .HDR record)
   int resBlkIndex;

   // full pathname for .res file
   string resFileName;

   int resFileHandle;

   // restore file ptr (cur write pos)
   int resFilePtr;

   // disk addr of cur block of .RES records
   int resBaseFileOffset;

   // length of .RES block
   int resBlkLeng;

   // ptr to cur .RES record in its buffer
   RESTORE_RECORD *resRecPtr;

   // the restore file buffer
   RESTORE_RECORD *resBuf;

   // -> just past the restore file buffer
   RESTORE_RECORD *resBufStop;

   // writeback flag (after marking restore records "IS_RESTORED")
   bool resWriteback;

   /// .PXL FILE //////////////////////////////////////////////
   //
   // this file contains the 16-bit 444 data for the field-
   // specific SAVEs. It is indexed by the .RES file to
   // enable fast recovery of the pixel data corresp to a
   // given repair. It is not accessed directly, but instead
   // through the CPixelChannel class, which encapsulates the
   // file and its access buffer.

   // full pathname for .pxl file
   string pxlFileName;

   // The access buffer for the .PXL file no longer depends
   // on the image format (for 4K clips the buffer got as
   // large as 144mB = 4K * 3K * 3wds * 2byts  * 2 (safety).
   //
   // Must be >= 16 + 4*MAX_LASSO_PTS and >= 6 + 6*MAX_FRAME_WDTH;
   // So far the first is 24K+, the second is 24K+.
   //
   // KT-060226 - Bugzilla 2399
   // reduced this from 512K to 64K, saving 448K
   // with no apparent degradation in performance
#define PXLBUF_SIZE 0x10000 // 64K

   // class instance to manage .PXL file
   CPixelChannel *pxlChannel;

   int pxlFileHandle;

   MTI_INT64 pxlFilePtr;

   /// .HST FILE //////////////////////////////////////////////
   //
   // this file contains one restore-record for every instance of
   // a saved rectangle in a FIELD . It is used to rapidly locate
   // the PIXEL DATA for the specified BOX within the same file

   // the blocking factor determines the size of the
   // buffer used to access the .HST file:
   // size = blocking factor * sizeof(RESTORE_RECORD)

   // true if series of saves apply to different frames
   bool mixedFrames;

   // index of current .HST file open
   int currentHistoryFileOpen;

   // full pathname for .HST file
   string hstFilePath;

   // restore file offset (cur write pos)
   int resFileOffset;

   // disk addr of cur block of .RES records
   int resBaseOffset;

   ///////////////////////////////////////////////////////////
   //
   // filter variables for the restore
   MTI_INT32 filterFrame;
   MTI_INT8  filterField;
   RECT filterRegion;
   RECT clrFilterRegion;

   // MASK engine for creating filter masks
   CMask *maskEngine;

   // mask buffer
   int maskBufSize;
   MTI_UINT16 *maskBuf;

   // use mask - NULL if none is used
   MTI_UINT16 *useMask;

   // buffer for reading &
   // writing gov pxl data
   MTI_INT32 govFileHandle;
   MTI_INT32  govBufByts;
   MTI_UINT16 *govBuf;
   MTI_UINT16 *govBufStop;
   MTI_UINT16 *govBufPtr;
   MTI_UINT32 govBaseOffset;
   MTI_UINT32 govRunLengthOffset;

   // save the global save numbers
   // for the .RES records which
   // contribute to the GOV restore
struct GOVEntry
{
   int resFrameNum;
   int resFieldIndex;
   int resGlobalSaveNumber;
   string tmpFileName;
   int tmpFileLength;
};
   vector<GOVEntry> GOVRestoreList;

   // Revise a restored region
   int reviseOneGOVRegion(GOVEntry &govEntry);

   // Atomic Lock
   static CThreadLock *AtomicLock;
   static int LockCounter;
public:
   static CThreadLock *getLock();
};

bool historyExists(string const &frameListFileName);


//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

// HACK-A-DOODLE-DOO!!!  Necessitated by keeping around legacy code
enum EHistoryFlushType
{
   FLUSH_TYPE_NONE,
   FLUSH_TYPE_SAVE,
   FLUSH_TYPE_RESTORE
};


class MTI_CLIPLIB_API CAutoHistoryOpener : public CAutoThreadLocker
{
   int *retValPtr;
   CSaveRestore *saveRestorePtr;
   EHistoryFlushType historyFlushType;
   bool dontFlushFlag;

public:

   CAutoHistoryOpener(CSaveRestore &history, EHistoryFlushType closeType,
                      int &retVal);

   // frame        -> index of frame to restore
   // field        -> index of field to restore (-1 for wildcard)
   // rect         -> restore box (NULL -> use entire frame)
   // points       -> restore lasso (NULL -> use rect)
   // bezpoints    -> restore bezier (NULL -> use lasso)
   // retVal       -> OUTPUT: 0 is good!
   CAutoHistoryOpener(CSaveRestore &saveRestore, EHistoryFlushType closeType,
                      int frame, int field,
                      RECT *rect, POINT *points, BEZIER_POINT *bezpoints,
                      int &retVal);

   // frame        -> index of frame to restore
   // field        -> index of field to restore (-1 for wildcard)
   // pxlrgns      -> the pixels to be restored, in a list of separate regions
   // retVal       -> OUTPUT: 0 is good!
   CAutoHistoryOpener(CSaveRestore &saveRestore, EHistoryFlushType closeType,
                      int frame, int field,
                      CPixelRegionList& pxlrgns,
                      int &retVal);

   ~CAutoHistoryOpener();

   void dontFlush();
};

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

#pragma pack(pop)

#endif





