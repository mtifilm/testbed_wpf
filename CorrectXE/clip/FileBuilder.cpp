// FileBuilder.cpp: implementation of the CFileBuilder class.
//
/*
$Header: /usr/local/filmroot/clip/source/FileBuilder.cpp,v 1.6 2003/05/13 15:40:26 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#include <fcntl.h>      // for O_RDWR, O_CREAT, O_TRUNC
#include "FileBuilder.h"
#include "err_clip.h"
#include "StructuredStream.h"
#include "CommonHeaderFileRecord.h"

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFileBuilder::CFileBuilder()
: magicWord(FR_MAGIC_WORD_MTIX_LITTLE_ENDIAN), commonHeaderRecord(0), stream(0)
{

}

CFileBuilder::~CFileBuilder()
{
   delete commonHeaderRecord;
}

int CFileBuilder::
createFile(CStructuredStream *newStream, const string &fileName,
           EFileType fileType, MTI_UINT16 fileTypeVersion)
{
    // Remember pointer to the stream
    stream = newStream;

    // Endian fixup is not required by structured stream since
    // we are presumably creating the file in the system's native
    // endian
    stream->setEndianFixupRequired(false);

    // Create file
    if (stream->createFile(fileName) == -1)
        return CLIP_ERROR_FILE_CREATE_FAILED;

    // Write Magic Word
    if (stream->writeUInt32(magicWord) != 0)
        return CLIP_ERROR_FILE_WRITE_FAILED;

    // Create Common File Header File Record and write it to file
    commonHeaderRecord = new CCommonHeaderFileRecord();
    commonHeaderRecord->setFileType(fileType);
    commonHeaderRecord->setFileTypeVersion(fileTypeVersion);
    commonHeaderRecord->calculateRecordByteCount();
    if(putRecord(*commonHeaderRecord) != 0)
        return CLIP_ERROR_FILE_WRITE_FAILED;

    // Now ready to start writing file type specific records with
    // member function putRecord
    return 0;
}

int CFileBuilder::putRecord(CFileRecord &fileRecord)
{
    return (fileRecord.write(*stream));
}

int CFileBuilder::initForFileUpdate(CStructuredStream *newStream)
{
   // Initialize this CFileBuilder to write to a file that already
   // exists.  The newStream points to a CStructuredStream for a
   // file that has already been opened for writing.

   stream = newStream;

   return 0;
}
