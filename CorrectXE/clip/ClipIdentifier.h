#ifndef ClipIdentifierH
#define ClipIdentifierH

#include "cliplibint.h"
#include "UUIDSupport.h"

#include <vector>
#include <string>
using std::vector;
using std::string;

//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API ClipVersionNumber
{
public:
   ClipVersionNumber();
   ClipVersionNumber(const string &versionNameString);
   ClipVersionNumber(int subVersionNumber);
   ClipVersionNumber(const ClipVersionNumber &aVersionNumber);
   ClipVersionNumber operator=(const ClipVersionNumber &rhs);

   bool IsValid() const;

   int  GetFinalSubVersionNumber() const;

   ClipVersionNumber GetParentVersionNumber() const;
   ClipVersionNumber Next() const;
   ClipVersionNumber Previous() const;

   string ToDisplayName() const;
   string ToRelativePath() const;

   ClipVersionNumber operator+(const ClipVersionNumber &rhs) const;
   ClipVersionNumber operator+(int rhs) const;
   bool operator==(const ClipVersionNumber &rhs) const;
   bool operator!=(const ClipVersionNumber &rhs) const;

private:
   vector<int> versionVector;
   void initFromName(const string &versionNumberString);
};

//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API ClipIdentifier
{
public:
   ClipIdentifier();
   ClipIdentifier(const string &clipPath);
   ClipIdentifier(const string &parentPath, const string &clipName);
   ClipIdentifier(const string &clipPath, const ClipVersionNumber &versionNumber);
   ClipIdentifier(const ClipIdentifier &aClipIdentifier);
   ClipIdentifier operator=(const ClipIdentifier &rhs);

   bool IsEmpty() const;
   bool IsVersionClip() const;

   string GetBinPath() const;
   string GetBinName() const;
	string GetClipPath() const;
   string GetClipName() const;
   string GetMasterClipPath() const;
   string GetMasterClipName() const;
   string GetParentPath() const;
   string GetParentName() const;

   ClipIdentifier GetMasterClipId() const;
   ClipIdentifier GetParentClipId() const;
   ClipIdentifier GetNextSiblingClipId() const;
   ClipIdentifier GetPreviousSiblingClipId() const;

   string ToString() const;

   string GetVersionDisplayName() const;
	ClipVersionNumber GetVersionNumber() const;
   string GetFullDisplayString() const;

   ClipIdentifier operator+(const ClipVersionNumber &rhs) const;
   ClipIdentifier operator=(const ClipIdentifier &aClipIdentifier) const;
   bool operator==(const ClipIdentifier &rhs) const;
   bool operator!=(const ClipIdentifier &rhs) const;

   UUID getClipUuid();

private:
   string binPath;
   string clipName;
   ClipVersionNumber versionNumber;

   static string ExtractMasterClipPath(const string &fullPath);
   static void SplitFullPath(
         const string &fullPath,
         string &binPath,                   // Output
         string &clipName,                  // Output
         ClipVersionNumber &versionNumber); // Output
};

std::ostream& MTI_CLIPLIB_API operator<< (std::ostream& os, const ClipIdentifier& clipId);

//////////////////////////////////////////////////////////////////////

#endif


