//---------------------------------------------------------------------------

#include <string>
#include <iostream>

#include <errno.h>
#include "ClipAPI.h"

//////////////////////////////////////////////////////////////////////

void printUsage()
{
   cout << "Usage:" << endl;
   cout << "   1.  CopyClipMedia srcClipName dstFileName" << endl;
   cout << "         Copies the source clip's media to a file" << endl;
   cout << "   2.  CopyClipMedia srcFileName dstClipName" << endl;
   cout << "         Copies media from the source file to Clip's media" << endl;
}

//////////////////////////////////////////////////////////////////////

bool isItAClipName(const string& filename, string& binPath, string& clipName)
{
   CBinManager binMgr;

   if (binMgr.splitClipFileName(filename, binPath, clipName) != 0)
      {
      return false;
      }

   return binMgr.doesClipExist(binPath, clipName);
}

//---------------------------------------------------------------------------

int main(int argc, char* argv[])
{
   int retVal;

   CreateApplicationName(argv[0]);

   CBinManager binMgr;
   int status;
   string srcArg, dstArg;
   string binPath, clipName, filename;
   bool copyClipToFile = true;

   if (argc == 3)
      {
      srcArg = argv[1];
      dstArg = argv[2];
      
      string srcBinPath, srcClipName;
      bool srcIsClip = isItAClipName(srcArg, srcBinPath, srcClipName);
      string dstBinPath, dstClipName;
      bool dstIsClip = isItAClipName(dstArg, dstBinPath, dstClipName);

      if (srcIsClip && !dstIsClip)
         {
         copyClipToFile = true;
         binPath = srcBinPath;
         clipName = srcClipName;
         filename = dstArg;
         }
      else if (!srcIsClip && dstIsClip)
         {
         copyClipToFile = false;
         binPath = dstBinPath;
         clipName = dstClipName;
         filename = srcArg;
         }
      else if (srcIsClip && dstIsClip)
         {
         cout << "ERROR: Both source and destination are clip names" << endl;
         printUsage();
         return 1;
         }
      else if (!srcIsClip && !dstIsClip)
         {
         cout << "ERROR: Neither source nor destination are clip names" << endl;
         cout << "       or clip does not exist" << endl;
         printUsage();
         return 1;
         }
      }
   else 
      {
      // ERROR: wrong number of arguments
      printUsage();
      return 1;
      }

   if (copyClipToFile)
      {
      cout << "Copy Clip Media from Bin: " << binPath << endl;
      cout << "                    Clip: " << clipName << endl;
      cout << "                 to File: " << filename << endl;
      }
   else
      {
      cout << "Copy Clip Media from File: " << filename << endl;
      cout << "                   to Bin: " << binPath << endl;
      cout << "                     Clip: " << clipName << endl;
      }

   auto clip = binMgr.openClip(binPath, clipName, &status);
   if (status != 0)
      {
      cout << "ERROR: Could not open Clip " << clipName << endl;
      cout << "       in Bin " << binPath << endl;
      return 1;
      }

   CVideoProxy *mainVideo = clip->getVideoProxy(VIDEO_SELECT_NORMAL);
   if (mainVideo == 0)
      {
      cout << "ERROR: Clip does not have a main video proxy" << endl;
      return 1;
      }

   CVideoFrameList *videoFrameList
                           = mainVideo->getVideoFrameList(FRAMING_SELECT_VIDEO);
   if (videoFrameList == 0)
      {
      cout << "ERROR: Clip does not have a main video frame list" << endl;
      return 1;
      }

   int totalFrameCount = videoFrameList->getTotalFrameCount();

   cout << " " << totalFrameCount << " Frames to Copy" << endl;

   // Allocate a buffer
   MTI_UINT32 paddedFieldSize = videoFrameList->getMaxPaddedFieldByteCount();

   if (paddedFieldSize < 512)
      {
      cout << "ERROR: Bad field size " << paddedFieldSize << endl;
      return 1;
      }

   // Allocate a buffer for one field
   MTI_UINT8 *buffer;
   buffer = (MTI_UINT8 *)malloc(paddedFieldSize);

   if (buffer == 0)
      {
      cout << "ERROR: Could not allocate buffer " << endl;
      return 1;
      }

   int frameIndex, fieldIndex;
   CVideoFrame *frame;
   CVideoField *field;
   int fieldCount;

   if (copyClipToFile)
      {
      // Open the destination file
      int dstFile = creat(filename.c_str(), 0666);
      if (dstFile == -1)
         {
         cout << "ERROR: Unable to create file " << filename << endl;
         cout << "       because " << strerror(errno) << endl;
         return 1;
         }

      //
      for (frameIndex = 0; frameIndex < totalFrameCount; ++frameIndex)
         {
         frame = videoFrameList->getFrame(frameIndex);
         if (frame == 0)
            {
            cout << "ERROR: Could not get frame " << frameIndex << endl;
            return 1;
            }

         fieldCount = frame->getTotalFieldCount();

         for (fieldIndex = 0; fieldIndex < fieldCount; ++fieldIndex)
            {
            field = frame->getField(fieldIndex);
            if (field == 0)
               {
               cout << "ERROR: Could not get field " << fieldIndex
                    << " in frame " << frameIndex << endl;
               return 1;
               }

            retVal = field->readMedia(buffer);
            if (retVal != 0)
               {
               cout << "ERROR: Could not read media for field " << fieldIndex
                    << " in frame " << frameIndex
                    << " Return code " << retVal << endl;
               return 1;
               }

            // Write to the destination file
            if (write(dstFile, buffer, paddedFieldSize) == -1)
               {
               cout << "ERROR: Write to file " << filename << endl;
               cout << "       failed because " << strerror(errno) << endl;
               return 1;
               }
            }
         }

      // Close the destination file
      close(dstFile);
      }

   else // Copy media from file to Clip
      {
      // Open the source file
      int srcFile = open(filename.c_str(), O_RDONLY|O_BINARY, 0666);

      for (frameIndex = 0; frameIndex < totalFrameCount; ++frameIndex)
         {
         frame = videoFrameList->getFrame(frameIndex);
         if (frame == 0)
            {
            cout << "ERROR: Could not get frame " << frameIndex << endl;
            return 1;
            }

         fieldCount = frame->getTotalFieldCount();

         for (fieldIndex = 0; fieldIndex < fieldCount; ++fieldIndex)
            {
            field = frame->getField(fieldIndex);
            if (field == 0)
               {
               cout << "ERROR: Could not get field " << fieldIndex
                    << " in frame " << frameIndex << endl;
               return 1;
               }

            // Read from the source file
            retVal = read(srcFile, buffer, paddedFieldSize);
            if (retVal < (int)paddedFieldSize)
               {
               cout << "ERROR: Attempted to read beyond end-of-file of " << endl
                    << "       " << filename << endl;
               return 1;
               }
            else if (retVal == -1)
               {
               cout << "ERROR: Read from file " << filename << endl;
               cout << "       failed because " << strerror(errno) << endl;
               return 1;
               }

            // Write the media to the clip
            retVal = field->writeMedia(buffer);
            if (retVal != 0)
               {
               cout << "ERROR: Could not write media for field " << fieldIndex
                    << " in frame " << frameIndex
                    << " Return code "  << retVal << endl;
               return 1;
               }

            }
         }

      // Close the source file
      close(srcFile);
      }

   free (buffer);
   cout << "Copy completed" << endl << endl;
   return 0;
}
//---------------------------------------------------------------------------



 
