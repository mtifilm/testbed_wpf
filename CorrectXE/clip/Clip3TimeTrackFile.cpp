// Clip3TimeTrackFile.cpp:
//
/* CVS Info:
$Header: /usr/local/filmroot/clip/source/Clip3TimeTrackFile.cpp,v 1.17 2005/05/22 01:55:15 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "Clip3TimeTrackFile.h"
#include "Clip3.h"
#include "Clip3TimeTrack.h"
#include "CommonHeaderFileRecord.h"
#include "err_clip.h"
#include "FileScanner.h"
#include "TimecodeFileRecord.h"
#include "StructuredFileStream.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTimecodeFrameFileRecord::CTimecodeFrameFileRecord(MTI_UINT16 version)
: CFileRecord(FR_RECORD_TYPE_TIMECODE_FRAME, version),
  timecodeRecord(0)
{

}

CTimecodeFrameFileRecord::~CTimecodeFrameFileRecord()
{
   delete timecodeRecord;
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

CTimecode CTimecodeFrameFileRecord::getTimecode() const
{
   if (timecodeRecord == 0)
      timecodeRecord = new CTimecodeFileRecord;

   CTimecode timecode = timecodeRecord->getTimecode();
   return timecode;
}

//////////////////////////////////////////////////////////////////////
// Mutators
//////////////////////////////////////////////////////////////////////

void CTimecodeFrameFileRecord::setTimecode(const CTimecode &newTimecode)
{
   if (timecodeRecord == 0)
      timecodeRecord = new CTimecodeFileRecord;

   timecodeRecord->setTimecode(newTimecode);
}

//////////////////////////////////////////////////////////////////////
// Read and Write functions
//////////////////////////////////////////////////////////////////////

int CTimecodeFrameFileRecord::read(CStructuredStream &stream)
{
   int retVal;

   // Read a CFileRecord, presumably a CTimecodeFileRecord
   CFileRecord *newRecordPtr;
   if ((retVal = CFileRecord::readRecord(stream, &newRecordPtr)) != 0)
       return retVal;

   // Check to make sure new CFileRecord actually is a CTimecodeFileRecord
   //   If it is, assign pointer to member variable timecodeRecord
   //   If it is not, return error
   if (newRecordPtr == 0
       || !newRecordPtr->isRecordType(FR_RECORD_TYPE_TIMECODE))
      return CLIP_ERROR_TIME_TRACK_FILE_READ_ERROR;

   if (timecodeRecord != 0)
      delete timecodeRecord; // avoid memory leaks

   timecodeRecord = static_cast<CTimecodeFileRecord*>(newRecordPtr);

   return 0;
}

int CTimecodeFrameFileRecord::write(CStructuredStream &stream)
{
   // Write record preamble
   if (CFileRecord::write(stream) != 0)
      return CLIP_ERROR_TIME_TRACK_FILE_WRITE_ERROR;

   // Write Timecode File Record
   if (timecodeRecord == 0)
      timecodeRecord = new CTimecodeFileRecord;     // shouldn't happen

   if (timecodeRecord->write(stream) != 0)
      return CLIP_ERROR_TIME_TRACK_FILE_WRITE_ERROR;

   return 0;
}

//////////////////////////////////////////////////////////////////////

void CTimecodeFrameFileRecord::calculateRecordByteCount()
{
   // Total the size of all the fields in the record

   CFileRecord::calculateRecordByteCount();

   MTI_UINT32 newByteCount = getRecordByteCount();

   // Calculate size of timecode record before adding its size
   timecodeRecord->calculateRecordByteCount();
   newByteCount += timecodeRecord->getRecordByteCount();

   // Update the object with the calculated size
   setRecordByteCount(newByteCount);
}


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSMPTETimecodeFrameFileRecord::CSMPTETimecodeFrameFileRecord(MTI_UINT16 version)
: CFileRecord(FR_RECORD_TYPE_SMPTE_TIMECODE_FRAME, version),
  timecodeRecord(0), partialFrame(0.0)
{

}

CSMPTETimecodeFrameFileRecord::~CSMPTETimecodeFrameFileRecord()
{
   delete timecodeRecord;
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

float CSMPTETimecodeFrameFileRecord::getPartialFrame() const
{
   return partialFrame;
}

CSMPTETimecode CSMPTETimecodeFrameFileRecord::getSMPTETimecode() const
{
   if (timecodeRecord == 0)
      timecodeRecord = new CSMPTETimecodeFileRecord;

   CSMPTETimecode timecode = timecodeRecord->getSMPTETimecode();
   return timecode;
}

//////////////////////////////////////////////////////////////////////
// Mutators
//////////////////////////////////////////////////////////////////////

void CSMPTETimecodeFrameFileRecord::setPartialFrame(float newPartialFrame)
{
   partialFrame = newPartialFrame;
}

void CSMPTETimecodeFrameFileRecord::setSMPTETimecode(
                                             const CSMPTETimecode &newTimecode)
{
   if (timecodeRecord == 0)
      timecodeRecord = new CSMPTETimecodeFileRecord;

   timecodeRecord->setSMPTETimecode(newTimecode);
}

//////////////////////////////////////////////////////////////////////
// Read and Write functions
//////////////////////////////////////////////////////////////////////

int CSMPTETimecodeFrameFileRecord::read(CStructuredStream &stream)
{
   int retVal;

   // Read a CFileRecord, presumably a CSMPTETimecodeFileRecord
   CFileRecord *newRecordPtr;
   if ((retVal = CFileRecord::readRecord(stream, &newRecordPtr)) != 0)
       return retVal;

   // Check to make sure new CFileRecord actually is a CTimecodeFileRecord
   //   If it is, assign pointer to member variable timecodeRecord
   //   If it is not, return error
   if (newRecordPtr == 0
       || !newRecordPtr->isRecordType(FR_RECORD_TYPE_SMPTE_TIMECODE))
      return CLIP_ERROR_TIME_TRACK_FILE_READ_ERROR;

   if (timecodeRecord != 0)
      delete timecodeRecord; // avoid memory leaks

   timecodeRecord = static_cast<CSMPTETimecodeFileRecord*>(newRecordPtr);

   // Read Partial Frame field
   MTI_REAL32 tmpPartialFrame;
   if(stream.readReal32(&tmpPartialFrame) != 0)
      return CLIP_ERROR_TIME_TRACK_FILE_READ_ERROR;

   partialFrame = tmpPartialFrame;

   return 0;
}

int CSMPTETimecodeFrameFileRecord::write(CStructuredStream &stream)
{
   // Write record preamble
   if (CFileRecord::write(stream) != 0)
      return CLIP_ERROR_TIME_TRACK_FILE_WRITE_ERROR;

   // Write SMPTE Timecode File Record
   if (timecodeRecord == 0)
      timecodeRecord = new CSMPTETimecodeFileRecord;     // shouldn't happen

   if (timecodeRecord->write(stream) != 0)
      return CLIP_ERROR_TIME_TRACK_FILE_WRITE_ERROR;

   if (stream.writeReal32(partialFrame) != 0)
      return CLIP_ERROR_TIME_TRACK_FILE_WRITE_ERROR;

   return 0;
}

//////////////////////////////////////////////////////////////////////

void CSMPTETimecodeFrameFileRecord::calculateRecordByteCount()
{
   // Total the size of all the fields in the record

   CFileRecord::calculateRecordByteCount();

   MTI_UINT32 newByteCount = getRecordByteCount();

   // Calculate size of timecode record before adding its size
   timecodeRecord->calculateRecordByteCount();
   newByteCount += timecodeRecord->getRecordByteCount();

   newByteCount += sizeof(MTI_REAL32);  // partialFrame

   // Update the object with the calculated size
   setRecordByteCount(newByteCount);
}


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CKeykodeFrameFileRecord::CKeykodeFrameFileRecord(MTI_UINT16 version)
: CFileRecord(FR_RECORD_TYPE_KEYKODE_FRAME, version)
{

}

CKeykodeFrameFileRecord::~CKeykodeFrameFileRecord()
{
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
// Mutators
//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
// Read and Write functions
//////////////////////////////////////////////////////////////////////

int CKeykodeFrameFileRecord::read(CStructuredStream &stream)
{
   // Read Keykode string field
   if (keykodeStringFileRecord.read(stream) != 0)
      return CLIP_ERROR_TIME_TRACK_FILE_READ_ERROR;

   if (stream.readInt8(&filmDimension) != 0)
      return CLIP_ERROR_TIME_TRACK_FILE_READ_ERROR;

   if (stream.readInt8(&interpolateFlag) != 0)
      return CLIP_ERROR_TIME_TRACK_FILE_READ_ERROR;

   return 0;
}

int CKeykodeFrameFileRecord::write(CStructuredStream &stream)
{
   // Write record preamble
   if (CFileRecord::write(stream) != 0)
      return CLIP_ERROR_TIME_TRACK_FILE_WRITE_ERROR;

   // Write Keykode string record
   // NB: To keep this working, the keykode string must always be
   //     the same length, i.e., not a variable-length string
   if (keykodeStringFileRecord.write(stream) != 0)
      return CLIP_ERROR_TIME_TRACK_FILE_WRITE_ERROR;

   if (stream.writeInt8(filmDimension) != 0)
      return CLIP_ERROR_TIME_TRACK_FILE_WRITE_ERROR;

   if (stream.writeInt8(interpolateFlag) != 0)
      return CLIP_ERROR_TIME_TRACK_FILE_WRITE_ERROR;

   return 0;
}

//////////////////////////////////////////////////////////////////////

void CKeykodeFrameFileRecord::calculateRecordByteCount()
{
   // Total the size of all the fields in the record

   CFileRecord::calculateRecordByteCount();

   MTI_UINT32 newByteCount = getRecordByteCount();

   newByteCount += keykodeStringFileRecord.getFieldSize();

   newByteCount += sizeof(filmDimension);
   newByteCount += sizeof(interpolateFlag);

   // Update the object with the calculated size
   setRecordByteCount(newByteCount);
}


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CEventFrameFileRecord::CEventFrameFileRecord(MTI_UINT16 version)
: CFileRecord(FR_RECORD_TYPE_EVENT_FRAME, version)
{

}

CEventFrameFileRecord::~CEventFrameFileRecord()
{
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
// Mutators
//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
// Read and Write functions
//////////////////////////////////////////////////////////////////////

int CEventFrameFileRecord::read(CStructuredStream &stream)
{
   // Read event flags
   MTI_UINT64 tmpEventFlags;
   if(stream.readUInt64(&tmpEventFlags) != 0)
      return CLIP_ERROR_TIME_TRACK_FILE_READ_ERROR;

   eventFlags = tmpEventFlags;

   return 0;
}

int CEventFrameFileRecord::write(CStructuredStream &stream)
{
   // Write record preamble
   if (CFileRecord::write(stream) != 0)
      return CLIP_ERROR_TIME_TRACK_FILE_WRITE_ERROR;

   // Write event flags field
   if(stream.writeUInt64(eventFlags) != 0)
      return CLIP_ERROR_TIME_TRACK_FILE_WRITE_ERROR;

   return 0;
}

//////////////////////////////////////////////////////////////////////

void CEventFrameFileRecord::calculateRecordByteCount()
{
   // Total the size of all the fields in the record

   CFileRecord::calculateRecordByteCount();

   MTI_UINT32 newByteCount = getRecordByteCount();

   newByteCount += sizeof(MTI_UINT64);  // event flags

   // Update the object with the calculated size
   setRecordByteCount(newByteCount);
}


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAudioProxyFrameFileRecord::CAudioProxyFrameFileRecord(MTI_UINT16 version)
: CFileRecord(FR_RECORD_TYPE_AUDIO_PROXY_FRAME, version)
{

}

CAudioProxyFrameFileRecord::~CAudioProxyFrameFileRecord()
{
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

void CAudioProxyFrameFileRecord::getMinMaxAudio(int sample, int channel,
                                       MTI_INT16 *min, MTI_INT16 *max)
{
   *min = audioProxySample[sample][channel].minAudio;
   *max = audioProxySample[sample][channel].maxAudio;
}

//////////////////////////////////////////////////////////////////////
// Mutators
//////////////////////////////////////////////////////////////////////

void CAudioProxyFrameFileRecord::setMinMaxAudio(int sample, int channel,
                                       MTI_INT16 min, MTI_INT16 max)
{
   audioProxySample[sample][channel].minAudio = min;
   audioProxySample[sample][channel].maxAudio = max;
}

//////////////////////////////////////////////////////////////////////
// Read and Write functions
//////////////////////////////////////////////////////////////////////

int CAudioProxyFrameFileRecord::read(CStructuredStream &stream)
{
   // Read audio proxy sample min's and max's
   for (int isample=0; isample < MAX_AUDIO_PROXY_SAMPLES; ++isample) {
      for (int ichannel=0; ichannel < MAX_AUDIO_PROXY_CHANNELS; ++ichannel) {
         if (stream.readInt16(&audioProxySample[isample][ichannel].minAudio) != 0)
            return CLIP_ERROR_TIME_TRACK_FILE_READ_ERROR;
         if (stream.readInt16(&audioProxySample[isample][ichannel].maxAudio) != 0)
            return CLIP_ERROR_TIME_TRACK_FILE_READ_ERROR;
      }
   }

   return 0;
}

int CAudioProxyFrameFileRecord::write(CStructuredStream &stream)
{
   // Write record preamble
   if (CFileRecord::write(stream) != 0)
      return CLIP_ERROR_TIME_TRACK_FILE_WRITE_ERROR;

   // Write audio proxy sample min's and max's
   for (int isample=0; isample < MAX_AUDIO_PROXY_SAMPLES; ++isample) {
      for (int ichannel=0; ichannel < MAX_AUDIO_PROXY_CHANNELS; ++ichannel) {
         if (stream.writeInt16(audioProxySample[isample][ichannel].minAudio) != 0)
            return CLIP_ERROR_TIME_TRACK_FILE_WRITE_ERROR;
         if (stream.writeInt16(audioProxySample[isample][ichannel].maxAudio) != 0)
            return CLIP_ERROR_TIME_TRACK_FILE_WRITE_ERROR;
      }
   }

   return 0;
}

//////////////////////////////////////////////////////////////////////

void CAudioProxyFrameFileRecord::calculateRecordByteCount()
{
   // Total the size of all the fields in the record

   CFileRecord::calculateRecordByteCount();

   MTI_UINT32 newByteCount = getRecordByteCount();

   newByteCount += MAX_AUDIO_PROXY_CHANNELS*MAX_AUDIO_PROXY_SAMPLES*
                   (sizeof(MTI_INT16)+sizeof(MTI_INT16));

   // Update the object with the calculated size
   setRecordByteCount(newByteCount);
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTimeFrameListFileReader::CTimeFrameListFileReader()
: externalBuffer(0), externalBufferSize(0),
  stream(0), fileScanner(0)
{

}

CTimeFrameListFileReader::CTimeFrameListFileReader(char *newBuffer,
                                                   unsigned int newBufferSize)
: externalBuffer(newBuffer), externalBufferSize(newBufferSize),
  stream(0), fileScanner(0)
{

}

CTimeFrameListFileReader::~CTimeFrameListFileReader()
{
   closeFrameListFile();
}

//////////////////////////////////////////////////////////////////////
// Track Builder
//////////////////////////////////////////////////////////////////////

int CTimeFrameListFileReader::readFrameListFile(CTimeTrack& timeTrack)
{
   int retVal;

   retVal = openFrameListFile(timeTrack.getFrameListFileNameWithExt());
   if (retVal != 0)
      {
      // ERROR: Could not open track file, possibly because file did not
      //        exist or is the file does not have the MTI/CPMP file format
      return retVal;
      }

   // Check that track types of the file agree
   if (fileScanner->getFileType()!= FR_FILE_TYPE_TIME_TRACK)
      {
      // Error: file type is not a Time Track
      return CLIP_ERROR_TIME_TRACK_FILE_INVALID_FILE_TYPE;
      }

   // Read all the rest of the records in the file and create
   // the Time Frame and Field objects and attach them to the Time Track
   retVal = buildFramesFromFile(timeTrack);
   if(retVal != 0)
      return retVal; // Error: couldn't build all of the frames

   closeFrameListFile();

   return 0;  // Success

} // readFrameListFile

int CTimeFrameListFileReader::reloadFrameListFile(CTimeTrack& timeTrack)
{
   int retVal;

   retVal = openFrameListFile(timeTrack.getFrameListFileNameWithExt());
   if (retVal != 0)
      {
      // ERROR: Could not open track file, possibly because file did not
      //        exist or is the file does not have the MTI/CPMP file format
      return retVal;
      }

   // Check that track types of the file agree
   if (fileScanner->getFileType()!= FR_FILE_TYPE_TIME_TRACK)
      {
      // Error: file type is not a Time Track
      return CLIP_ERROR_TIME_TRACK_FILE_INVALID_FILE_TYPE;
      }

   // Read all the rest of the records in the file and update
   // the Time Frame and Field objects and attach them to the Time Track
   retVal = updateFramesFromFile(timeTrack);
   if(retVal != 0)
      return retVal; // Error: couldn't build all of the frames

   closeFrameListFile();

   return 0;  // Success

} // reloadFrameListFile

//////////////////////////////////////////////////////////////////////
// Private Clip Builder Functions
//////////////////////////////////////////////////////////////////////

int CTimeFrameListFileReader::openFrameListFile(const string& frameListFileName)
{
   int retVal;

   closeFrameListFile();

   if (externalBuffer != 0 && externalBufferSize != 0)
      stream = new CStructuredFileStream(externalBuffer, externalBufferSize);
   else
      stream = new CStructuredFileStream;

   // Open file with stream
   if (stream->open(frameListFileName) != 0)
      {
      // ERROR: Could not open stream with frame list file name
      TRACE_0(errout << "ERROR: Unable to open Frame List file "
                     << frameListFileName << endl;
              errout << " because " << strerror(errno) << endl);
      delete stream;
      stream = 0;
      return CLIP_ERROR_TIME_TRACK_FILE_OPEN_ERROR;
      }

   // Create an instance of a CFileScanner and initialize
   fileScanner = new CFileScanner;
   retVal = fileScanner->initForReading(stream);
   if (retVal != 0)
      {
      // ERROR: Could not initialize CFileScanner for reading
      //        Possibly because file was not really a Track File
      closeFrameListFile();    // Cleanup
      return retVal;
      }

   return 0;
}

void CTimeFrameListFileReader::closeFrameListFile()
{
   if (fileScanner)
      {
      delete fileScanner;
      fileScanner = 0;
      }

   if (stream)
      {
      stream->close();
      delete stream;
      stream = 0;
      }
}

int CTimeFrameListFileReader::buildFramesFromFile(CTimeTrack& timeTrack)
{
   CFileRecord *newRecordPtr;
   CTimeFrame *newFrame = 0;
   ETimeType timeType = timeTrack.getTimeType();

   // Read all records from track file and convert to
   // CTimecodeFrame and CKeykodeFrame instances
   while (fileScanner->readNextRecord(&newRecordPtr) == 0)
      {
      switch (EFileRecordType(newRecordPtr->getRecordType()))
         {
         case FR_RECORD_TYPE_TIMECODE_FRAME :
            if (timeType != TIME_TYPE_TIMECODE)
               {
               // Error: wrong type of file record when Timecode Frame was
               //        expected
               delete newRecordPtr;
               return CLIP_ERROR_TIME_TRACK_FILE_INVALID_RECORD;
               }

            // Create a new instance of a CTimecodeFrame object and set
            // based on CTimecodeFrameFileRecord instance
            newFrame
             = makeFrame(static_cast<CTimecodeFrameFileRecord&>(*newRecordPtr));

            break;

         case FR_RECORD_TYPE_KEYKODE_FRAME :
            if (timeType != TIME_TYPE_KEYKODE)
               {
               // Error: wrong type of file record when Keykode Frame was
               //        expected
               delete newRecordPtr;
               return CLIP_ERROR_TIME_TRACK_FILE_INVALID_RECORD;
               }

            // Create a new instance of a CKeykodeFrame and set it based
            // on the CKeykodeFrameFileRecord
            newFrame
              = makeFrame(static_cast<CKeykodeFrameFileRecord&>(*newRecordPtr));

             break;

         case FR_RECORD_TYPE_SMPTE_TIMECODE_FRAME :
            if (timeType != TIME_TYPE_SMPTE_TIMECODE)
               {
               // Error: wrong type of file record when SMPTE Timecode Frame was
               //        expected
               delete newRecordPtr;
               return CLIP_ERROR_TIME_TRACK_FILE_INVALID_RECORD;
               }

            // Create a new instance of a CSMPTETimecodeFrame object and set
            // based on CSMPTETimecodeFrameFileRecord instance
            newFrame = makeFrame(
                   static_cast<CSMPTETimecodeFrameFileRecord&>(*newRecordPtr));

            break;

         case FR_RECORD_TYPE_EVENT_FRAME :
            if (timeType != TIME_TYPE_EVENT)
               {
               // Error: wrong type of file record when Event Frame was
               //        expected
               delete newRecordPtr;
               return CLIP_ERROR_TIME_TRACK_FILE_INVALID_RECORD;
               }

            // Create a new instance of a CEventFrame and set it based
            // on the CEventFrameFileRecord
            newFrame
               = makeFrame(static_cast<CEventFrameFileRecord&>(*newRecordPtr));

             break;

         case FR_RECORD_TYPE_AUDIO_PROXY_FRAME :
            if (timeType != TIME_TYPE_AUDIO_PROXY)
               {
               // Error: wrong type of file record when Event Frame was
               //        expected
               delete newRecordPtr;
               return CLIP_ERROR_TIME_TRACK_FILE_INVALID_RECORD;
               }

            // Create a new instance of a CEventFrame and set it based
            // on the CEventFrameFileRecord
            newFrame
               = makeFrame(static_cast<CAudioProxyFrameFileRecord&>(*newRecordPtr));

             break;

         default :
            // Error: Unknown or unexpected record type
            delete newRecordPtr;
            return CLIP_ERROR_TIME_TRACK_FILE_INVALID_RECORD;
         }

      // Add the new frame to the time track
      timeTrack.addFrame(newFrame);

      // Delete the file record just obtained
      delete newRecordPtr;
      }

   return 0;

} // buildFramesFromFile

int CTimeFrameListFileReader::updateFramesFromFile(CTimeTrack& timeTrack)
{
   int retVal;
   CFileRecord *newRecordPtr;
   ETimeType timeType = timeTrack.getTimeType();

   // Read all records from track file and convert to
   // CTimecodeFrame and CKeykodeFrame instances
   int frameCount = timeTrack.getTotalFrameCount();
   for (int i = 0; i < frameCount; ++i)
      {
      retVal = fileScanner->readNextRecord(&newRecordPtr);
      if (retVal != 0)
         return retVal;  // ERROR

      switch (EFileRecordType(newRecordPtr->getRecordType()))
         {
         case FR_RECORD_TYPE_TIMECODE_FRAME :
            {
            if (timeType != TIME_TYPE_TIMECODE)
               {
               // Error: wrong type of file record when Timecode Frame was
               //        expected
               delete newRecordPtr;
               return CLIP_ERROR_TIME_TRACK_FILE_INVALID_RECORD;
               }

            // Set the existing CTimecodeFrame object
            // based on CTimecodeFrameFileRecord instance
            CTimecodeFrame *timecodeFrame = timeTrack.getTimecodeFrame(i);
            setFrame(static_cast<CTimecodeFrameFileRecord&>(*newRecordPtr),
                     timecodeFrame);
            }
            break;

         case FR_RECORD_TYPE_KEYKODE_FRAME :
            {
            if (timeType != TIME_TYPE_KEYKODE)
               {
               // Error: wrong type of file record when Keykode Frame was
               //        expected
               delete newRecordPtr;
               return CLIP_ERROR_TIME_TRACK_FILE_INVALID_RECORD;
               }

            // Set the existing instance of a CKeykodeFrame based
            // on the CKeykodeFrameFileRecord
            CKeykodeFrame *keykodeFrame = timeTrack.getKeykodeFrame(i);
            setFrame(static_cast<CKeykodeFrameFileRecord&>(*newRecordPtr),
                     keykodeFrame);
            }
            break;

         case FR_RECORD_TYPE_SMPTE_TIMECODE_FRAME :
            {
            if (timeType != TIME_TYPE_SMPTE_TIMECODE)
               {
               // Error: wrong type of file record when SMPTE Timecode Frame was
               //        expected
               delete newRecordPtr;
               return CLIP_ERROR_TIME_TRACK_FILE_INVALID_RECORD;
               }

            // Set an existing instance of a CSMPTETimecodeFrame object
            // based on CSMPTETimecodeFrameFileRecord instance
            CSMPTETimecodeFrame *smpteTimecodeFrame
                                          = timeTrack.getSMPTETimecodeFrame(i);
            setFrame(static_cast<CSMPTETimecodeFrameFileRecord&>(*newRecordPtr),
                     smpteTimecodeFrame);
            }
            break;

         case FR_RECORD_TYPE_EVENT_FRAME :
            {
            if (timeType != TIME_TYPE_EVENT)
               {
               // Error: wrong type of file record when Event Frame was
               //        expected
               delete newRecordPtr;
               return CLIP_ERROR_TIME_TRACK_FILE_INVALID_RECORD;
               }

            // Set the existing instance of a CEventFrame and set it based
            // on the CEventFrameFileRecord
            CEventFrame *eventFrame = timeTrack.getEventFrame(i);
            setFrame(static_cast<CEventFrameFileRecord&>(*newRecordPtr),
                     eventFrame);
            }
            break;

         case FR_RECORD_TYPE_AUDIO_PROXY_FRAME :
            {
            if (timeType != TIME_TYPE_AUDIO_PROXY)
               {
               // Error: wrong type of file record when Audio Proxy Frame was
               //        expected
               delete newRecordPtr;
               return CLIP_ERROR_TIME_TRACK_FILE_INVALID_RECORD;
               }

            // Set the existing instance of a CAudioProxyFrame and set it based
            // on the CAudioProxyFrameFileRecord
            CAudioProxyFrame *audioProxyFrame = timeTrack.getAudioProxyFrame(i);
            setFrame(static_cast<CAudioProxyFrameFileRecord&>(*newRecordPtr),
                     audioProxyFrame);
            }
            break;

         default :
            // Error: Unknown or unexpected record type
            delete newRecordPtr;
            return CLIP_ERROR_TIME_TRACK_FILE_INVALID_RECORD;
         }

      // Delete the file record just obtained
      delete newRecordPtr;
      }

   return 0;

} // buildFramesFromFile

CTimeFrame*
CTimeFrameListFileReader::makeFrame(CTimecodeFrameFileRecord& frameRecord)
{
   // Make a new CTimeFrame subtype
   CTimecodeFrame* frame = new CTimecodeFrame;

   // Initialize the new frame from the frame file record
   setFrame(frameRecord, frame);

   return frame;
}

CTimeFrame*
CTimeFrameListFileReader::makeFrame(CKeykodeFrameFileRecord& frameRecord)
{
   // Make a new CTimeFrame subtype
   CKeykodeFrame* frame = new CKeykodeFrame;

   // Initialize the new frame from the frame file record
   setFrame(frameRecord, frame);

   return frame;
}

CTimeFrame*
CTimeFrameListFileReader::makeFrame(CSMPTETimecodeFrameFileRecord& frameRecord)
{
   // Make a new CTimeFrame subtype
   CSMPTETimecodeFrame* frame = new CSMPTETimecodeFrame;

   // Initialize the new frame from the frame file record
   setFrame(frameRecord, frame);

   return frame;
}

CTimeFrame*
CTimeFrameListFileReader::makeFrame(CEventFrameFileRecord& frameRecord)
{
   // Make a new CTimeFrame subtype
   CEventFrame* frame = new CEventFrame;

   // Initialize the new frame from the frame file record
   setFrame(frameRecord, frame);

   return frame;
}


CTimeFrame*
CTimeFrameListFileReader::makeFrame(CAudioProxyFrameFileRecord& frameRecord)
{
   // Make a new CTimeFrame subtype
   CAudioProxyFrame* frame = new CAudioProxyFrame;

   // Initialize the new frame from the frame file record
   setFrame(frameRecord, frame);

   return frame;
}

void CTimeFrameListFileReader::setFrame(CTimecodeFrameFileRecord& frameRecord,
                                         CTimecodeFrame *frame)
{
   // Initialize the frame from the frame file record
   frame->setTimecode(frameRecord.getTimecode());

   frame->setNeedsUpdate(false);    // Data is fresh from disk, don't
                                    // need to write back
}

void CTimeFrameListFileReader::setFrame(CKeykodeFrameFileRecord& frameRecord,
                                        CKeykodeFrame *frame)
{
   // Initialize the frame from the frame file record
   frame->setKeykode(frameRecord.getKeykodeStr());

   frame->setFilmDimension(
                        (EKeykodeFilmDimension)frameRecord.getFilmDimension());
   frame->setInterpolateFlag((frameRecord.getInterpolateFlag() != 0));

   frame->setNeedsUpdate(false);    // Data is fresh from disk, don't
                                    // need to write back
}

void CTimeFrameListFileReader::setFrame(CSMPTETimecodeFrameFileRecord& frameRecord,
                                        CSMPTETimecodeFrame *frame)
{
   // Initialize the frame from the frame file record
   frame->setSMPTETimecode(frameRecord.getSMPTETimecode());

   frame->setPartialFrame(frameRecord.getPartialFrame());

   frame->setNeedsUpdate(false);    // Data is fresh from disk, don't
                                    // need to write back
}

void CTimeFrameListFileReader::setFrame(CEventFrameFileRecord& frameRecord,
                                        CEventFrame *frame)
{
   // Initialize the new frame from the frame file record
   frame->setEventFlags(frameRecord.getEventFlags());

   frame->setNeedsUpdate(false);    // Data is fresh from disk, don't
                                    // need to write back
}

void CTimeFrameListFileReader::setFrame(CAudioProxyFrameFileRecord& frameRecord,
                                        CAudioProxyFrame *frame)
{
   MTI_INT16 minaudio, maxaudio;

   // Initialize the new frame from the frame file record
   for (int isample=0; isample<MAX_AUDIO_PROXY_SAMPLES; ++isample) {
      for (int ichannel=0; ichannel<MAX_AUDIO_PROXY_CHANNELS; ++ichannel) {
         frameRecord.getMinMaxAudio(isample, ichannel, &minaudio, &maxaudio);
         frame->setMinMaxAudio(isample, ichannel, minaudio, maxaudio);
      }
   }

   frame->setNeedsUpdate(false);    // Data is fresh from disk, don't
                                    // need to write back
}

//////////////////////////////////////////////////////////////////////
/////////////                            /////////////////////////////
//////////     CTimeFrameListFileWriter     //////////////////////////
////////////                            //////////////////////////////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTimeFrameListFileWriter::CTimeFrameListFileWriter()
{

}

CTimeFrameListFileWriter::~CTimeFrameListFileWriter()
{

}

//////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////

int
CTimeFrameListFileWriter::writeFrameListFile(CTimeTrack* timeTrack)
{
   int retVal;

   // Create appropriate instance of CStructuredStream subtype
   CStructuredFileStream stream;

   // Create file with magic word and Common Header Record
   string frameListFileName = timeTrack->getFrameListFileNameWithExt();
   if(fileBuilder.createFile(&stream, frameListFileName,
                             FR_FILE_TYPE_TIME_TRACK,
                             FR_TIME_TRACK_FILE_TYPE_FILE_VERSION) != 0)
      return CLIP_ERROR_TIME_TRACK_FILE_CREATE_FAILED;

   // Frame Records
   retVal = writeFrameList(timeTrack);
   if (retVal != 0)
      return retVal; // ERROR: Failed to write frames to frame list file

   // Close file
   if (stream.close() != 0)
      return CLIP_ERROR_TIME_TRACK_FILE_CLOSE_FAILED;

   return 0;   // Success
}

int CTimeFrameListFileWriter::initForUpdate(CStructuredFileStream *newStream)
{
   fileBuilder.initForFileUpdate(newStream);

   return 0;
}

//////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////


int CTimeFrameListFileWriter::writeFrame(CTimecodeFrame& frame)
{
   int retVal;

   CTimecodeFrameFileRecord frameRecord;

   frameRecord.setTimecode(frame.getTimecode());

   // Calculate and set the size of the record
   frameRecord.calculateRecordByteCount();

   retVal = fileBuilder.putRecord(frameRecord);
   if (retVal != 0)
      return retVal;

   // Frame has been written to the file, so it has been updated
   frame.setNeedsUpdate(false);

   return 0;
}

int CTimeFrameListFileWriter::writeFrame(CKeykodeFrame& frame)
{
   int retVal;

   CKeykodeFrameFileRecord frameRecord;

   frameRecord.setKeykodeStr(frame.getKeykodeStr());
   frameRecord.setFilmDimension(frame.getFilmDimension());
   frameRecord.setInterpolateFlag(frame.getInterpolateFlag());

   // Calculate and set the size of the record
   frameRecord.calculateRecordByteCount();

   retVal = fileBuilder.putRecord(frameRecord);
   if (retVal != 0)
      return retVal;

   // Frame has been written to the file, so it has been updated
   frame.setNeedsUpdate(false);

   return 0;
}

int CTimeFrameListFileWriter::writeFrame(CSMPTETimecodeFrame& frame)
{
   int retVal;

   CSMPTETimecodeFrameFileRecord frameRecord;

   frameRecord.setSMPTETimecode(frame.getSMPTETimecode());
   frameRecord.setPartialFrame(frame.getPartialFrame());

   // Calculate and set the size of the record
   frameRecord.calculateRecordByteCount();

   retVal = fileBuilder.putRecord(frameRecord);
   if (retVal != 0)
      return retVal;

   // Frame has been written to the file, so it has been updated
   frame.setNeedsUpdate(false);

   return 0;
}

int CTimeFrameListFileWriter::writeFrame(CEventFrame& frame)
{
   int retVal;

   CEventFrameFileRecord frameRecord;

   frameRecord.setEventFlags(frame.getEventFlags());

   // Calculate and set the size of the record
   frameRecord.calculateRecordByteCount();

   retVal = fileBuilder.putRecord(frameRecord);
   if (retVal != 0)
      return retVal;

   // Frame has been written to the file, so it has been updated
   frame.setNeedsUpdate(false);

   return 0;
}

int CTimeFrameListFileWriter::writeFrame(CAudioProxyFrame& frame)
{
   int retVal;

   MTI_INT16 minaudio, maxaudio;

   CAudioProxyFrameFileRecord frameRecord;

   for (int isample=0; isample < MAX_AUDIO_PROXY_SAMPLES; ++isample) {
      for (int ichannel=0; ichannel < MAX_AUDIO_PROXY_CHANNELS; ++ichannel) {
         frame.getMinMaxAudio(isample, ichannel, &minaudio, &maxaudio);
         frameRecord.setMinMaxAudio(isample, ichannel, minaudio, maxaudio);
      }
   }

   // Calculate and set the size of the record
   frameRecord.calculateRecordByteCount();

   retVal = fileBuilder.putRecord(frameRecord);
   if (retVal != 0)
      return retVal;

   // Frame has been written to the file, so it has been updated
   frame.setNeedsUpdate(false);

   return 0;
}

int CTimeFrameListFileWriter::writeFrameList(CTimeTrack* timeTrack)
{
   int retVal;

   int frameCount = timeTrack->getTotalFrameCount();

   retVal = writeFrameList(timeTrack, 0, frameCount);
   if (retVal != 0)
      return retVal;

   return 0;
}
int CTimeFrameListFileWriter::writeFrameList(CTimeTrack* timeTrack,
                                             int startFrameIndex,
                                             int frameCount)
{
   int retVal;

   // Iterate over all frames in the frame list
   int endFrameIndex = startFrameIndex + frameCount;
   if (timeTrack->getTimeType() == TIME_TYPE_TIMECODE)
      {
      for(int frameIndex = startFrameIndex; frameIndex < endFrameIndex; ++frameIndex)
         {
         CTimecodeFrame *frame = timeTrack->getTimecodeFrame(frameIndex);

         // Write the frame to the file
         retVal = writeFrame(*frame);
         if (retVal != 0)
            return retVal;
         }
      }
   else if (timeTrack->getTimeType() == TIME_TYPE_KEYKODE)
      {
      for(int frameIndex = startFrameIndex; frameIndex < endFrameIndex; ++frameIndex)
         {
         CKeykodeFrame *frame = timeTrack->getKeykodeFrame(frameIndex);

         // Write the frame to the file
         retVal = writeFrame(*frame);
         if (retVal != 0)
            return retVal;
         }
      }
   else if (timeTrack->getTimeType() == TIME_TYPE_SMPTE_TIMECODE)
      {
      for(int frameIndex = startFrameIndex; frameIndex < endFrameIndex; ++frameIndex)
         {
         CSMPTETimecodeFrame *frame
                                = timeTrack->getSMPTETimecodeFrame(frameIndex);

         // Write the frame to the file
         retVal = writeFrame(*frame);
         if (retVal != 0)
            return retVal;
         }
      }
   else if (timeTrack->getTimeType() == TIME_TYPE_EVENT)
      {
      for(int frameIndex = startFrameIndex; frameIndex < endFrameIndex; ++frameIndex)
         {
         CEventFrame *frame = timeTrack->getEventFrame(frameIndex);

         // Write the frame to the file
         retVal = writeFrame(*frame);
         if (retVal != 0)
            return retVal;
         }
      }
   else if (timeTrack->getTimeType() == TIME_TYPE_AUDIO_PROXY)
      {
      for(int frameIndex = startFrameIndex; frameIndex < endFrameIndex; ++frameIndex)
         {
         CAudioProxyFrame *frame = timeTrack->getAudioProxyFrame(frameIndex);

         // Write the frame to the file
         retVal = writeFrame(*frame);
         if (retVal != 0)
            return retVal;
         }
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////
/////////////                            /////////////////////////////
//////////     CTimeFrameListFileUpdater     //////////////////////////
////////////                            //////////////////////////////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTimeFrameListFileUpdater::CTimeFrameListFileUpdater()
 : externalBuffer(0), externalBufferSize(0),
   fileReader(0), fileWriter(0), fileHeaderSize(0), frameRecordSize(0)
{

}

CTimeFrameListFileUpdater::CTimeFrameListFileUpdater(char *newBuffer,
                                                     unsigned int newBufferSize)
 : externalBuffer(newBuffer), externalBufferSize(newBufferSize),
   fileReader(0), fileWriter(0), fileHeaderSize(0), frameRecordSize(0)
{

}

CTimeFrameListFileUpdater::~CTimeFrameListFileUpdater()
{
   closeFrameListFile();
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CTimeFrameListFileUpdater::updateFrameListFile(CTimeTrack* timeTrack,
                                                   int firstFrameIndex,
                                                   int frameCount)
{
   int retVal;

   // Update the frame list file with the marked frames in the frame list
   retVal = updateFrameList(timeTrack, firstFrameIndex, frameCount);
   if(retVal != 0)
      return retVal; // Error: couldn't update all of the frames

   return 0;  // Success

} // updateFrameListFile

//////////////////////////////////////////////////////////////////////
//  Open/Close Track File for Update
//////////////////////////////////////////////////////////////////////

int
CTimeFrameListFileUpdater::openFrameListFile(const string &frameListFileName)
{
   int retVal;

   // Use an instance of a  CTimeFrameListFileReader to open the frame
   // list file
   if (externalBuffer != 0 && externalBufferSize != 0)
      fileReader = new CTimeFrameListFileReader(externalBuffer,
                                                externalBufferSize);
   else
      fileReader = new CTimeFrameListFileReader;

   // ??? Do we need to separately check for the existence of the file
   
   retVal =
        fileReader->openFrameListFile(frameListFileName);
   if (retVal != 0)
      {
      // ERROR: Could not open track file, possibly because file did not
      //        exist or is the file does not have the MTI/CPMP file format
      return retVal;
      }

   // Check that track types of the file agree
   if (fileReader->fileScanner->getFileType()!= FR_FILE_TYPE_TIME_TRACK)
      {
      // Error: file type is not a Time Track
      return CLIP_ERROR_TIME_TRACK_FILE_INVALID_FILE_TYPE;
      }

   // Get the size of the Common File Header Record
   CCommonHeaderFileRecord *commonHeaderRecord
                            = fileReader->fileScanner->getCommonHeaderRecord();

   // Get the record size in bytes and add the size of the magic word.
   // This will be used as an offset past the file header
   fileHeaderSize = commonHeaderRecord->getRecordByteCount()
                    + sizeof(MTI_UINT32);

   // Read the first record.  This should be a time frame of some sort
   // We want to know its size.
   CFileRecord *frameRecord = 0;
   retVal = fileReader->fileScanner->readNextRecord(&frameRecord);
   if (retVal != 0)
      return retVal;

   // Remember the size of the (assumed) time frame record
   // We will use this to calculate the offset to the individual
   // records will will want to write
   frameRecordSize = frameRecord->getRecordByteCount();

   // Delete the file record just obtained
   delete frameRecord;

   // Clear file reader's stream buffers
   fileReader->stream->initBuffer();

   // Use an instance of a CTimeFrameListFileWriter to format and
   // write the time frame records
   fileWriter = new CTimeFrameListFileWriter;
   fileWriter->initForUpdate(fileReader->stream);

   return 0;

} // openFrameListFile

int CTimeFrameListFileUpdater::closeFrameListFile()
{
   delete fileWriter;
   fileWriter = 0;

   delete fileReader;     // this will close the open file
   fileReader = 0;

   return 0;

} // closeFrameListFile

//////////////////////////////////////////////////////////////////////
//  Update From from Frame List
//////////////////////////////////////////////////////////////////////

int CTimeFrameListFileUpdater::updateFrameList(CTimeTrack* timeTrack,
                                               int firstFrameIndex,
                                               int frameCount)
{
   int retVal;

   bool fileOpen = false;
   int frameIndex = firstFrameIndex;
   int startFrameIndex;
   int updateCount;
   CTimeFrame *frame;

   if (!timeTrack->isFrameListAvailable())
      return 0;   // frame list is not available, so cannot update

   // If caller's frameCount is -1, then update all frames from
   // firstFrameIndex to end of frame list
   if (frameCount == -1)
      frameCount = timeTrack->getTotalFrameCount() - firstFrameIndex;

   // Exclusive out frame index
   int outFrame = firstFrameIndex + frameCount;

   // Iterate over all of the frame in the frame list, writing out
   // any frames that need to be updated
   while (true)
      {

      if (frameIndex >= outFrame)
         break;

      updateCount = 0;

      // Search for the first frame that needs to be updated
      for ( ; frameIndex < outFrame; ++frameIndex)
         {
         frame = timeTrack->getBaseFrame(frameIndex);
         if (frame->getNeedsUpdate())
            {
            startFrameIndex = frameIndex;
            updateCount = 1;
            ++frameIndex;
            break;
            }
         }

      if (updateCount < 1)
         break;  // Nothing to update, so exit loop

      // Count the number of frames that need to be updated
      for ( ; frameIndex < outFrame; ++frameIndex)
         {
         frame = timeTrack->getBaseFrame(frameIndex);
         if (frame->getNeedsUpdate())
            ++updateCount;
         else
            {
            ++frameIndex;
            break;
            }
         }

      // Open the field list file if it is not already open
      if(!fileOpen)
         {
         retVal = openFrameListFile(timeTrack->getFrameListFileNameWithExt());
         if (retVal != 0)
            {
            // ERROR: Could not open track file, possibly because file did not
            //        exist or the file does not have the MTI/CPMP file format
            return retVal;
            }
         fileOpen = true;  // remember file is open
         }

      retVal = updateFrames(timeTrack, startFrameIndex, updateCount);
      if (retVal != 0)
         {
         // Error
         if (fileOpen)
            closeFrameListFile();
         return retVal;
         }
      }

   if (fileOpen)
      closeFrameListFile();

   return 0;

} // updateFrameList

int CTimeFrameListFileUpdater::updateFrames(CTimeTrack* timeTrack,
                                            int startFrameIndex,
                                            int frameCount)
{
   int retVal;

   if (frameCount < 1)
      return 0;

   // Seek to target record in the file
   retVal = seekToRecord(startFrameIndex);
   if (retVal != 0)
      return retVal;

   // Write the frames to the file starting at the seek position
   retVal = fileWriter->writeFrameList(timeTrack, startFrameIndex, frameCount);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CTimeFrameListFileUpdater::seekToRecord(int recordIndex)
{
   int retVal;

   // Calculate byte offset to target record in file
   MTI_INT64 recordOffset = fileHeaderSize + recordIndex * frameRecordSize;

   // Seek to the record in the file (this will also flush any pending
   // writes)
   retVal = fileReader->stream->seek(recordOffset);
   if (retVal != 0)
      return retVal;

   return 0;
}



