// Clip3AudioTrack.h: interface for the CAudioField, CAudioFrame,
//                    and CAudioFrameList classes
//
// Created by: John Starr, December 12, 2001
//
//////////////////////////////////////////////////////////////////////

#ifndef AUDIOTRACK3H
#define AUDIOTRACK3H

#include "cliplibint.h"
#include "Clip3Track.h"

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CAudioFormat;
class CAudioTrack;
class CAudioFrameList;
class CTimeTrack;

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CAudioField : public CField
{
public:
   CAudioField();                                   // Default Constructor
   CAudioField(const CAudioField& sourceField);    // Copy Constructor
   virtual ~CAudioField();

   CAudioField& operator=(const CAudioField& rhs); // Assignment Operator

   int virtualCopy(const CAudioField &srcField,
                   CMediaStorageList &dstMediaStorageList);

   int readMedia(MTI_UINT8 *buffer);
   int writeMedia(MTI_UINT8 *buffer);

	void dump(MTIostringstream& str) const;

private:
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CAudioFrame : public CFrame
{
public:
   CAudioFrame(int reserveFieldCount = 2);
   virtual ~CAudioFrame();

   const CAudioFormat* getAudioFormat() const;
   CAudioField* getField(int fieldIndex);
   double getSlip() const;

   void setAudioFormat(const CAudioFormat* newAudioFormat);
   void setSlip(double newSlip);

   CField* createField(CFieldList *newFieldList,
                       int fieldListEntryIndex);
   CField* createField(const CField& srcField);

   int virtualCopy(CAudioFrame &srcFrame,
                   CMediaStorageList &dstMediaStorageList);

   virtual void blackenField(CField *field, int fieldIndex, MTI_UINT8 *buffer);
   
	void dump(MTIostringstream& str);

private:
   double slip;   // Audio Slip, in frames

};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CAudioFramingInfo : public CFramingInfo
{
public:
   CAudioFramingInfo(int newTrackNumber, int newFramingType);
   virtual ~CAudioFramingInfo();
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CAudioFramingInfoList : public CFramingInfoList
{
public:
   CAudioFramingInfoList();
   virtual ~CAudioFramingInfoList();

   CAudioFramingInfo* getAudioFramingInfo(int framingIndex) const;

   int readAudioFramingInfoListSection(CIniFile *iniFile);
   int writeAudioFramingInfoListSection(CIniFile *iniFile);

protected:
   CFramingInfo* createDerivedFramingInfo(int newFramingNumber,
                                           int newFramingType);

private:
   static string audioFramingInfoListSection;
   static string audioFramingInfoSectionName;
};
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CAudioFrameList : public CFrameList
{
public:
   CAudioFrameList(CAudioFramingInfo *newFramingInfo,
                    CAudioTrack *newAudioTrack);
   CAudioFrameList(CAudioFramingInfo *newFramingInfo,
                    CAudioTrack *newAudioTrack,
                    MTI_UINT32 reserveFrameCount);
   virtual ~CAudioFrameList();

   CFrame* createFrame(int fieldCount, CMediaFormat* mediaFormat,
                        const CTimecode& timecode);

   const CAudioFormat* getAudioFormat() const;
   CAudioTrack *getAudioTrack() const;
   CAudioFrame* getFrame(int frameNumber);
   int getSampleCountAtFrame(int frameNumber);
   int getSampleCountAtField(int frameNumber, int fieldNumber);
   int getSampleCountAtTimecode(const CTimecode& timecode, int fieldNumber);

   int virtualCopy(CAudioFrameList &srcFrameList, int srcIndex,
                   int dstIndex, int frameCount,
                   CMediaStorageList &dstMediaStorageList);

   virtual int readFrameListInfoSection(CIniFile *iniFile,
                                        const string& sectionPrefix,
                                        const CTimecode& timecodePrototype);
   virtual int writeFrameListInfoSection(CIniFile *iniFile,
                                         const string& sectionPrefix);

   string makeFrameListFileName() const;
   string makeNewFrameListFileName(string newClipName) const;

   virtual int readFrameListFile();
   virtual int writeFrameListFile();
   int renameFrameListFile(string newClipName);

   virtual int readMediaBySample(int frameNumber, int sampleOffset,
                                 MTI_INT64 sampleCount, MTI_UINT8 *buffer,
                                 int fileHandleIndex=0);
   virtual int writeMediaBySample(int frameNumber, int sampleOffset,
                                  MTI_INT64 sampleCount, MTI_UINT8 *buffer,
                                  CTimeTrack *audioProxy);
   virtual void initMediaBuffer(MTI_UINT8* buffer[]);

   void dump(MTIostringstream& str);      // for debugging

private:
   virtual int readMediaBySample_ClipVer3(int frameNumber, int sampleOffset,
                                 MTI_INT64 sampleCount, MTI_UINT8 *buffer,
                                 int fileHandleIndex);
   virtual int readMediaBySample_ClipVer5L(int frameNumber, int sampleOffset,
                                 MTI_INT64 sampleCount, MTI_UINT8 *buffer,
                                 int fileHandleIndex);
   virtual int writeMediaBySample_ClipVer3(int frameNumber, int sampleOffset,
                                  MTI_INT64 sampleCount, MTI_UINT8 *buffer,
                                  CTimeTrack *audioProxy);
   virtual int writeMediaBySample_ClipVer5L(int frameNumber, int sampleOffset,
                                  MTI_INT64 sampleCount, MTI_UINT8 *buffer,
                                  CTimeTrack *audioProxy);
   int getMediaLocationsBySample(int frameNumber, int sampleOffset,
                                 MTI_INT64 sampleCount,
                                 vector<CMediaLocation> &mediaLocationList,
                                 vector<CField*> &fieldsFound);
                                 
private:
   static string audioTrackSectionName;
};

//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CAudioTrack : public CTrack
{
public:
   CAudioTrack(ClipSharedPtr &newParentClip, EClipVersion newClipVer,
               int newTrackNumber);
   virtual ~CAudioTrack();

   int extendAudioTrack(int newFrameCount, bool allocateMedia);
   int shortenAudioTrack(int frameCount);

   CFrameList *createFrameList(CFramingInfo *newFramingInfo);

   const CAudioFormat* getAudioFormat() const;
   CAudioFrameList* getAudioFrameList(int framingIndex) const;

   void setAudioFormat(const CAudioFormat& newAudioFormat);

   const string& getMediaDirectory();
   void setMediaDirectory(const string& newMediaDirectory);

   int virtualCopy(CAudioTrack &srcTrack, int srcFrameIndex, int dstFrameIndex,
                   int frameCount);

   int readTrackSections(CIniFile *clipFile,
                         const string& sectionPrefix,
                         CFramingInfoList& framingInfoList);

   int readAudioTrackSections(CIniFile *clipFile, const string& sectionPrefix,
                              CAudioFramingInfoList& audioFramingInfoList);
   int writeAudioTrackSections(CIniFile *clipFile, const string& sectionPrefix);

private:
   int extendAudioTrack_ClipVer3(int newFrameCount, bool allocateMedia);
   int extendAudioTrack_ClipVer5L(int newFrameCount, bool allocateMedia);

private:
   string mediaDirectory;

   // TBD: Proxy-Level Metadata

}; // class CAudioTrack

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CAudioTrackList : public CTrackList
{
public:
   CAudioTrackList();
   virtual ~CAudioTrackList();

   int createAudioTrack(ClipSharedPtr &newParentClip,  EClipVersion clipVer,
                        int newHandleCount,
                        const CAudioFormat& newAudioFormat);
   CTrack* createDerivedTrack(ClipSharedPtr &newParentClip, EClipVersion newClipVer,
                              int newTrackNumber);

   CAudioTrack* getAudioTrack(int trackIndex) const;
   int getAudioTrackCount() const;

   CAudioTrack* findAudioTrack(const string &appId) const;
   int findAudioTrackIndex(const string &appId) const;

   int readAudioTrackListSection(CIniFile *iniFile, ClipSharedPtr &parentClip,
                                 CAudioFramingInfoList& audioFramingInfoList);
   int refreshAudioTrackListSection(CIniFile *iniFile);
   int writeAudioTrackListSection(CIniFile *iniFile);

private:
   static string audioTrackSection;

}; // class CAudioTrackList

#ifdef PRIVATE_STATIC
#ifndef _CLIP_3_AUDIO_TRACK_DATA
namespace Clip3AudioTrackData {
   extern string audioFramingInfoListSection;
   extern string audioFramingInfoSectionName;

   extern string audioTrackSectionName;

   extern string audioTrackSection;
};
#endif
#endif

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

#endif // #ifndef AUDIO_TRACK3_H


