// AiffCMediaAccess.h: interface for the CAiffCMediaAccess class.
//
/*
$Header: /usr/local/filmroot/clip/include/AiffCMediaAccess.h,v 1.9 2006/11/20 18:33:11 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef AIFFCMEDIAACCESS_H
#define AIFFCMEDIAACCESS_H

#include <string>
#include <iostream>

using std::string;
using std::ostream;

#include "MediaAccess.h"

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CMediaAllocator;
class CMediaFormat;

//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CAiffCMediaAccess : public CMediaAccess  
{
public:
//	CAiffCMediaAccess();
	CAiffCMediaAccess(const string& newMediaIdentifier,
                          const string& newHistoryDirectory,
                          const CMediaFormat& mediaFormat);
	virtual ~CAiffCMediaAccess();

   int queryFreeSpace(MTI_INT64 *totalFreeBytes,
                      MTI_INT64 *maxContiguousFreeBytes);

   bool isClipSpecific() const;

   MTI_UINT32 getAdjustedFieldSize(MTI_UINT32 fieldSize);
   MTI_INT64 adjustToBlocking(MTI_INT64 size, bool roundingFlag);

   int Peek(CMediaLocation &mediaLocation, string &filename, MTI_INT64 &offset);

   int checkSpace(const CMediaFormat& mediaFormat, MTI_UINT32 frameCount);

   CMediaAllocator*  allocate(const CMediaFormat& mediaFormat,
                              const CTimecode& frame0Timecode, 
                              MTI_UINT32 frameCount);
   int cancelAllocation(CMediaAllocator *mediaAllocator);

   CMediaDeallocator* createDeallocator();
   int deallocate(CMediaDeallocator& deallocator);

   int read(MTI_UINT8* buffer, CMediaLocation &mediaLocation);
   int write(const MTI_UINT8* buffer, CMediaLocation &mediaLocation);
   int read(MTI_UINT8* buffer, MTI_INT64 offset, unsigned int size);
   int write(const MTI_UINT8* buffer, MTI_INT64 offset, 
             unsigned int size);

   void dump(MTIostringstream &str);
};

//////////////////////////////////////////////////////////////////////

#endif // #ifndef AIFFCMEDIAACCESS_H
