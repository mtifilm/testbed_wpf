// RawMediaDeallocator.cpp: implementation of the CRawMediaDeallocator class.
//
/* CVS Info:
$Header: /usr/local/filmroot/clip/source/RawMediaDeallocator.cpp,v 1.3 2006/01/11 01:56:25 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "RawMediaDeallocator.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRawMediaDeallocator::CRawMediaDeallocator()
{

}

CRawMediaDeallocator::~CRawMediaDeallocator()
{

}

//////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////

int CRawMediaDeallocator::
freeMediaAllocation(const CMediaLocation& mediaLocation)
{
   deallocList.addToDeallocList(mediaLocation);

   return 0;
}

int CRawMediaDeallocator::
freeMediaAllocation(const CMediaLocation& startMediaLocation, int itemCount)
{
   deallocList.addToDeallocList(startMediaLocation, itemCount);

   return 0;
}


CMediaDeallocList& CRawMediaDeallocator::getMediaDeallocList()
{
   return deallocList;
}
