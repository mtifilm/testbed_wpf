// Clip3AudioTrackFile.cpp: implementation of the CAudioFrameListFileReader and
//                          CAudioFrameListFileWriter classes.
//
/* CVS Info:
$Header: /usr/local/filmroot/clip/source/Clip3AudioTrackFile.cpp,v 1.6 2005/01/24 18:40:15 mlm Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "Clip3AudioTrackFile.h"
#include "Clip3.h"
#include "Clip3AudioTrack.h"
#include "err_clip.h"
#include "FileScanner.h"
#include "MediaStorage3.h"
#include "StructuredFileStream.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAudioFieldFileRecord::CAudioFieldFileRecord(MTI_UINT16 version)
: CFieldFileRecord(FR_RECORD_TYPE_AUDIO_FIELD_R3, version)
{

}

CAudioFieldFileRecord::~CAudioFieldFileRecord()
{
}

//////////////////////////////////////////////////////////////////////
// Read and Write functions
//////////////////////////////////////////////////////////////////////

int CAudioFieldFileRecord::read(CStructuredStream &stream)
{
   // Read the supertype instance data
   if (CFieldFileRecord::read(stream) != 0)
      return CLIP_ERROR_AUDIO_TRACK_FILE_READ_ERROR;

   // Read the reserved words
   for (int i = 0; i < AUDIO_FIELD3_RECORD_RESERVED_COUNT; ++i)
      {
      if(stream.readUInt32(&(audioFieldReserved[i])) != 0)
         return CLIP_ERROR_AUDIO_TRACK_FILE_READ_ERROR;
      }

   return 0;
}

int CAudioFieldFileRecord::write(CStructuredStream &stream) 
{
   // Write record preamble
   if (CFieldFileRecord::write(stream) != 0)
      return CLIP_ERROR_AUDIO_TRACK_FILE_WRITE_ERROR;

   // Write zeroes to reserved area
   for (int i = 0; i < AUDIO_FIELD3_RECORD_RESERVED_COUNT; ++i)
      {
      if(stream.writeUInt32(0) != 0)
         return CLIP_ERROR_AUDIO_TRACK_FILE_WRITE_ERROR;
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////

void CAudioFieldFileRecord::calculateRecordByteCount()
{
   // Total the size of all the fields in the record

   CFieldFileRecord::calculateRecordByteCount();

   MTI_UINT32 newByteCount = getRecordByteCount();

   newByteCount += sizeof(audioFieldReserved);

   // Update the object with the calculated size
   setRecordByteCount(newByteCount);
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAudioFrameFileRecord::CAudioFrameFileRecord(MTI_UINT16 version)
: CFrameFileRecord(FR_RECORD_TYPE_AUDIO_FRAME_R3, version)
{

}

CAudioFrameFileRecord::~CAudioFrameFileRecord()
{
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

MTI_REAL64 CAudioFrameFileRecord::getSlip() const
{
   return slip;
}

//////////////////////////////////////////////////////////////////////
// Mutators
//////////////////////////////////////////////////////////////////////

void CAudioFrameFileRecord::setSlip(MTI_REAL64 newSlip)
{
   slip = newSlip;
}

//////////////////////////////////////////////////////////////////////
// Read and Write functions
//////////////////////////////////////////////////////////////////////

int CAudioFrameFileRecord::read(CStructuredStream &stream)
{
   // Read the supertype instance data
   if (CFrameFileRecord::read(stream) != 0)
      return CLIP_ERROR_AUDIO_TRACK_FILE_READ_ERROR;

   // Read Slip
   if (stream.readReal64(&slip) != 0)
      return CLIP_ERROR_AUDIO_TRACK_FILE_READ_ERROR;

   // Read the reserved words
   for (int i = 0; i < AUDIO_FRAME3_RECORD_RESERVED_COUNT; ++i)
      {
      if(stream.readUInt32(&(audioFrameReserved[i])) != 0)
         return CLIP_ERROR_AUDIO_TRACK_FILE_READ_ERROR;
      }

   return 0;
}

int CAudioFrameFileRecord::write(CStructuredStream &stream) 
{
   // Write record preamble
   if (CFrameFileRecord::write(stream) != 0)
      return CLIP_ERROR_AUDIO_TRACK_FILE_WRITE_ERROR;

   // Write Slip
   if (stream.writeReal64(slip) != 0)
      return CLIP_ERROR_AUDIO_TRACK_FILE_WRITE_ERROR;

   // Write zeroes to reserved area
   for (int i = 0; i < AUDIO_FRAME3_RECORD_RESERVED_COUNT; ++i)
      {
      if(stream.writeUInt32(0) != 0)
         return CLIP_ERROR_AUDIO_TRACK_FILE_WRITE_ERROR;
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////

void CAudioFrameFileRecord::calculateRecordByteCount()
{
   // Total the size of all the fields in the record

   CFrameFileRecord::calculateRecordByteCount();

   MTI_UINT32 newByteCount = getRecordByteCount();

   newByteCount += sizeof(slip);
   newByteCount += sizeof(audioFrameReserved);

   // Update the object with the calculated size
   setRecordByteCount(newByteCount);
}


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAudioFrameListFileReader::CAudioFrameListFileReader()
{

}

CAudioFrameListFileReader::~CAudioFrameListFileReader()
{

}

//////////////////////////////////////////////////////////////////////
// Track Builder
//////////////////////////////////////////////////////////////////////

int
CAudioFrameListFileReader::readFrameListFile(CAudioFrameList& audioFrameList)
{
   int retVal;

   retVal = openFrameListFile(audioFrameList.getFrameListFileNameWithExt());
   if (retVal != 0)
      {
      // ERROR: Could not open track file, possibly because file did not
      //        exist or is the file does not have the MTI/CPMP file format
      return retVal;
      }

   // Check that track types of the file agree
   if (fileScanner->getFileType()!= FR_FILE_TYPE_AUDIO_TRACK3)
      {
      // Error: file type is not a Audio Track
      return CLIP_ERROR_AUDIO_TRACK_FILE_INVALID_FILE_TYPE;
      }

   // Read all the rest of the records in the file and create
   // the Frame and Field objects and attach them to the Clip object
   retVal = buildFramesFromFile(audioFrameList);
   if(retVal != 0)
      return retVal; // Error: couldn't build all of the frames or fields

   closeFrameListFile();

   return 0;  // Success

} // readTrackFile

//////////////////////////////////////////////////////////////////////
// Private Clip Builder Functions
//////////////////////////////////////////////////////////////////////

int CAudioFrameListFileReader::
buildFramesFromFile(CAudioFrameList& audioFrameList)
{
   CFileRecord *newRecordPtr;
   CAudioFrame *newFrame = 0;
   CAudioField *newField;

   CAudioTrack *audioTrack = audioFrameList.getAudioTrack();
   const CMediaStorageList *mediaStorageList;
   mediaStorageList = audioTrack->getMediaStorageList();

   // Check file version to determine if the Field list was implemented
   CFieldList *fieldList;
   if (fileScanner->getFileVersion() >
       FR_AUDIO_TRACK3_FILE_TYPE_FILE_PRE_FIELD_LIST_VERSION)
      fieldList = audioTrack->getFieldList();
   else
      fieldList = 0;

   const CAudioFormat *audioFormat;
   audioFormat = audioTrack->getAudioFormat();

   // Read all records from track file and convert to
   // CAudioFrame and CAudioField instances
   while (fileScanner->readNextRecord(&newRecordPtr) == 0)
      {
      switch (EFileRecordType(newRecordPtr->getRecordType()))
         {
         case FR_RECORD_TYPE_AUDIO_FRAME_R3 :
            // Create a new instance of a CAudioFrame object and set
            // based on CAudioFrameFileRecord instance
            newFrame
              = readFrame(static_cast<CAudioFrameFileRecord&>(*newRecordPtr),
                          audioFormat);

            // Add the new frame to the audio track
            audioFrameList.addFrame(newFrame);
            break;

         case FR_RECORD_TYPE_AUDIO_FIELD_R3 :
            if (newFrame == 0)
               {
               // Error: a field record encountered before frame record
               delete newRecordPtr;
               return CLIP_ERROR_AUDIO_TRACK_FILE_INVALID_RECORD;
               }

            // Create a new instance of a CAudioField and set it based
            // on the CAudioFrameFileRecord
            newField
              = readField(static_cast<CAudioFieldFileRecord&>(*newRecordPtr),
                          fieldList, *mediaStorageList);

            // Add the new field to the current frame
            newFrame->addField(newField);
            break;

         default :
            // Error: Unknown record type
            delete newRecordPtr;
            return CLIP_ERROR_AUDIO_TRACK_FILE_INVALID_RECORD;
         }

      // Delete the file record just obtained
      delete newRecordPtr;
   }

   return 0;

} // buildFramesFromFile

CAudioFrame*
CAudioFrameListFileReader::readFrame(CAudioFrameFileRecord& frameRecord,
                                      const CAudioFormat *audioFormat)
{
   // Make a new CFrame subtype
   CAudioFrame* frame = new CAudioFrame(frameRecord.getFieldCount());

   // Initialize the new frame from the frame file record
   setAudioFrameFromFileRecord(*frame, frameRecord);

   // Set audio format in frame
   frame->setAudioFormat(audioFormat);

   return frame;
}

CAudioField*
CAudioFrameListFileReader::readField(CAudioFieldFileRecord& fieldRecord,
                                     CFieldList *newFieldList,
                                     const CMediaStorageList& mediaStorageList)
{
   // Make a new CField subtype
   CAudioField* newField = new CAudioField;

   // Initialize the new field from the field file record
   setAudioFieldFromFileRecord(*newField, newFieldList, mediaStorageList,
                               fieldRecord);

   // Success
   return newField;
}

//////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////

void CAudioFrameListFileReader::
setAudioFieldFromFileRecord(CAudioField& field,
                            CFieldList *newFieldList,
                            const CMediaStorageList& mediaStorageList,
                            const CAudioFieldFileRecord &fieldRecord)
{
   // Set base class, CField, from record
   setBaseFieldFromFileRecord(field, newFieldList, mediaStorageList,
                              fieldRecord);
}

void CAudioFrameListFileReader::
setAudioFrameFromFileRecord(CAudioFrame& frame,
                            const CAudioFrameFileRecord &frameRecord)

{
   // Set base class, CFrame, from record
   setBaseFrameFromFileRecord(frame, frameRecord);

   frame.setSlip(frameRecord.getSlip());
}

//////////////////////////////////////////////////////////////////////
////////////////                            //////////////////////////
/////////////     CAudioFrameListFileWriter     /////////////////////////
///////////////                            ///////////////////////////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAudioFrameListFileWriter::CAudioFrameListFileWriter()
{

}

CAudioFrameListFileWriter::~CAudioFrameListFileWriter()
{

}

//////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////

int
CAudioFrameListFileWriter::writeFrameListFile(CAudioFrameList* audioFrameList)
{
   int retVal;

   // Create appropriate instance of CStructuredStream subtype
   CStructuredFileStream stream;

   // Create file with magic word and Common Header Record
   string frameListFileName = audioFrameList->getFrameListFileNameWithExt();
   if(fileBuilder.createFile(&stream, frameListFileName,
                             FR_FILE_TYPE_AUDIO_TRACK3,
                             FR_AUDIO_TRACK3_FILE_TYPE_FILE_VERSION) != 0)
      return CLIP_ERROR_AUDIO_TRACK_FILE_CREATE_FAILED;

   // Frame and Field Records
   retVal = writeFrameList(audioFrameList);
   if (retVal != 0)
      return retVal; // ERROR: Failed to write frames and fields to track file

   // Close file
   if (stream.close() != 0)
      return CLIP_ERROR_AUDIO_TRACK_FILE_CLOSE_FAILED;

   return 0;   // Success
}

//////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////

void CAudioFrameListFileWriter::
setAudioFieldToFileRecord(const CAudioField& field,
                          const CMediaStorageList &mediaStorageList,
                          CAudioFieldFileRecord &fieldRecord)
{
   // Set base class, CField, to record
   setBaseFieldToFileRecord(field, mediaStorageList, fieldRecord);
}

void CAudioFrameListFileWriter::
setAudioFrameToFileRecord(const CAudioFrame& frame,
                          CAudioFrameFileRecord &frameRecord)

{
   // Set base class, CFrame, to record
   setBaseFrameToFileRecord(frame, frameRecord);

   frameRecord.setSlip(frame.getSlip());
}

//////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////

int CAudioFrameListFileWriter::writeField(CAudioField& field,
                                     const CMediaStorageList &mediaStorageList)
{
   int retVal;

   CAudioFieldFileRecord fieldRecord;
      
   setAudioFieldToFileRecord(field, mediaStorageList, fieldRecord);

   // Calculate and set the size of the record
   fieldRecord.calculateRecordByteCount();

   retVal = fileBuilder.putRecord(fieldRecord);
   if (retVal != 0)
      return retVal;  // ERROR

   return 0;
}

int CAudioFrameListFileWriter::writeFrame(CAudioFrame& frame)
{
   int retVal;

   CAudioFrameFileRecord frameRecord;

   setAudioFrameToFileRecord(frame, frameRecord);

   // Calculate and set the size of the record
   frameRecord.calculateRecordByteCount();

   retVal = fileBuilder.putRecord(frameRecord);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CAudioFrameListFileWriter::writeFrameList(CAudioFrameList* audioFrameList)
{
   int retVal;

   CAudioTrack *audioTrack = audioFrameList->getAudioTrack();
   const CMediaStorageList *mediaStorageList;
   mediaStorageList = audioTrack->getMediaStorageList();

   // Iterate over all frames in the frame list
   int frameCount = audioFrameList->getTotalFrameCount();
   for(int frameIndex = 0; frameIndex < frameCount; ++frameIndex)
      {
      CAudioFrame *frame = audioFrameList->getFrame(frameIndex);

      // Write the frame to the file
      retVal = writeFrame(*frame);
      if (retVal != 0)
         return retVal; // ERROR: Unable to write the frame

      // Write the frame's fields
      int fieldCount = frame->getTotalFieldCount();
      for(int fieldIndex = 0; fieldIndex < fieldCount; ++fieldIndex)
         {
         CAudioField *field = frame->getField(fieldIndex);

         // Write the field to the file
         retVal = writeField(*field, *mediaStorageList);
         if (retVal != 0)
            return retVal; // ERROR: Unable to write the field
         }
      }

   return 0;
}


