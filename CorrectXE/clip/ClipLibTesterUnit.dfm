object ClipLibTesterForm: TClipLibTesterForm
  Left = 291
  Top = 348
  Width = 673
  Height = 530
  Caption = 'Clip Library Tester'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu
  OldCreateOrder = False
  OnCloseQuery = FormCloseQuery
  DesignSize = (
    665
    476)
  PixelsPerInch = 96
  TextHeight = 13
  object OpenClipButton: TButton
    Left = 544
    Top = 24
    Width = 21
    Height = 21
    Caption = '...'
    TabOrder = 0
    OnClick = OpenClipButtonClick
  end
  object CurrentClipName: TLabeledEdit
    Left = 88
    Top = 24
    Width = 449
    Height = 21
    EditLabel.Width = 85
    EditLabel.Height = 13
    EditLabel.Caption = 'Current Clip Name'
    LabelPosition = lpLeft
    LabelSpacing = 3
    TabOrder = 1
  end
  object PageControl: TPageControl
    Left = 0
    Top = 72
    Width = 664
    Height = 406
    ActivePage = TimeSheet
    Anchors = [akLeft, akTop, akRight, akBottom]
    MultiLine = True
    TabIndex = 2
    TabOrder = 2
    object VideoSheet: TTabSheet
      Caption = 'Video'
    end
    object AudioSheet: TTabSheet
      Caption = 'Audio'
      ImageIndex = 1
      object FrameNumberEdit: TLabeledEdit
        Left = 40
        Top = 40
        Width = 121
        Height = 21
        EditLabel.Width = 69
        EditLabel.Height = 13
        EditLabel.Caption = 'Frame Number'
        LabelPosition = lpAbove
        LabelSpacing = 3
        TabOrder = 0
      end
      object SampleOffsetEdit: TLabeledEdit
        Left = 40
        Top = 88
        Width = 121
        Height = 21
        EditLabel.Width = 66
        EditLabel.Height = 13
        EditLabel.Caption = 'Sample Offset'
        LabelPosition = lpAbove
        LabelSpacing = 3
        TabOrder = 1
      end
      object SampleCountEdit: TLabeledEdit
        Left = 40
        Top = 136
        Width = 121
        Height = 21
        EditLabel.Width = 66
        EditLabel.Height = 13
        EditLabel.Caption = 'Sample Count'
        LabelPosition = lpAbove
        LabelSpacing = 3
        TabOrder = 2
      end
      object Button1: TButton
        Left = 232
        Top = 40
        Width = 75
        Height = 25
        Caption = 'Read'
        TabOrder = 3
        OnClick = Button1Click
      end
    end
    object TimeSheet: TTabSheet
      Caption = 'Time/Event'
      ImageIndex = 2
      DesignSize = (
        656
        378)
      object EventTrackGroup: TGroupBox
        Left = 8
        Top = 144
        Width = 641
        Height = 233
        Caption = 'Event Track'
        TabOrder = 4
        Visible = False
        object EventFrameList: TListView
          Left = 8
          Top = 16
          Width = 401
          Height = 137
          Columns = <
            item
              Caption = 'Index'
            end
            item
              Caption = 'Event'
              Width = 300
            end>
          TabOrder = 0
          ViewStyle = vsReport
        end
      end
      object SMPTETimecodeTrackGroup: TGroupBox
        Left = 8
        Top = 184
        Width = 641
        Height = 161
        Caption = 'SMPTE Timecode Track'
        TabOrder = 2
        Visible = False
      end
      object TimecodeTrackGroup: TGroupBox
        Left = 8
        Top = 136
        Width = 641
        Height = 241
        Caption = 'Timecode Track'
        TabOrder = 1
        Visible = False
        object TimecodeTrackIndexLabel: TLabel
          Left = 8
          Top = 16
          Width = 86
          Height = 13
          Caption = 'Time Track Index:'
        end
        object TimecodeTrackIndexValue: TLabel
          Left = 96
          Top = 16
          Width = 18
          Height = 13
          Caption = '999'
        end
        object TimecodeTrackAppIdLabel: TLabel
          Left = 120
          Top = 16
          Width = 31
          Height = 13
          Caption = 'AppId:'
        end
        object TimecodeTrackAppIdValue: TLabel
          Left = 152
          Top = 16
          Width = 210
          Height = 13
          Caption = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'
        end
        object TimecodeTrackFrameCountLabel: TLabel
          Left = 8
          Top = 32
          Width = 63
          Height = 13
          Caption = 'Frame Count:'
        end
        object TimecodeTrackFrameCountValue: TLabel
          Left = 74
          Top = 32
          Width = 18
          Height = 13
          Caption = '999'
        end
        object TimecodeFrameList: TListView
          Left = 8
          Top = 48
          Width = 201
          Height = 185
          Columns = <
            item
              Caption = 'Frame Index'
              Width = 75
            end
            item
              Caption = 'Timecode'
              Width = 100
            end>
          TabOrder = 0
          ViewStyle = vsReport
        end
        object SetTimecodesButton: TButton
          Left = 256
          Top = 48
          Width = 121
          Height = 25
          Caption = 'Copy Video Timecodes'
          TabOrder = 1
          OnClick = SetTimecodesButtonClick
        end
        object ClearTimecodesButton: TButton
          Left = 256
          Top = 88
          Width = 121
          Height = 25
          Caption = 'Clear Timecodes'
          TabOrder = 2
          OnClick = ClearTimecodesButtonClick
        end
        object UpdateTimeTrackFileButton: TButton
          Left = 416
          Top = 48
          Width = 113
          Height = 25
          Caption = 'Update Track File'
          TabOrder = 3
        end
        object UpdateGroupSize: TComboBox
          Left = 416
          Top = 88
          Width = 145
          Height = 21
          ItemHeight = 13
          ItemIndex = 4
          TabOrder = 4
          Text = '10'
          Items.Strings = (
            'All'
            '1'
            '2'
            '5'
            '10'
            '20'
            '50'
            '100')
        end
        object UseFrameIndicesButton: TButton
          Left = 256
          Top = 128
          Width = 121
          Height = 25
          Caption = 'Use Frame Indices'
          TabOrder = 5
          OnClick = UseFrameIndicesButtonClick
        end
      end
      object KeykodeTrackGroup: TGroupBox
        Left = 8
        Top = 136
        Width = 641
        Height = 241
        Caption = 'Keykode Track'
        TabOrder = 3
        Visible = False
        object KeykodeFrameList: TListView
          Left = 8
          Top = 64
          Width = 401
          Height = 169
          Columns = <
            item
              Caption = 'Index'
            end
            item
              Caption = 'Keykode'
              Width = 300
            end>
          TabOrder = 0
          ViewStyle = vsReport
        end
        object KeykodeDisplayStyle: TRadioGroup
          Left = 432
          Top = 24
          Width = 161
          Height = 89
          Caption = 'Display Style'
          ItemIndex = 0
          Items.Strings = (
            'Raw KK'
            'Formated KK'
            'Feet+Frames'
            'Feet+Frames+Perfs')
          TabOrder = 1
          OnClick = KeykodeDisplayStyleClick
        end
        object SetKeykodesButton: TButton
          Left = 432
          Top = 144
          Width = 75
          Height = 25
          Caption = 'Set Keykodes'
          TabOrder = 2
          OnClick = SetKeykodesButtonClick
        end
      end
      object TimeTrackListView: TListView
        Left = 8
        Top = 16
        Width = 640
        Height = 113
        Anchors = [akLeft, akTop, akRight]
        Columns = <
          item
            Caption = 'Index'
          end
          item
            Caption = 'Type'
            Width = 100
          end
          item
            Caption = 'AppId'
            Width = 200
          end>
        RowSelect = True
        TabOrder = 0
        ViewStyle = vsReport
        OnDblClick = TimeTrackListViewDblClick
      end
    end
  end
  object MainMenu: TMainMenu
    object File1: TMenuItem
      Caption = 'File'
      object OpenClipMenuItem: TMenuItem
        Caption = 'Open Clip'
        OnClick = OpenClipButtonClick
      end
      object ReloadClipMenuItem: TMenuItem
        Caption = 'Reload Clip'
        OnClick = ReloadClipMenuItemClick
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object CloseClipMenuItem: TMenuItem
        Caption = 'Close Clip'
        OnClick = CloseClipMenuItemClick
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object ExitMenuItem: TMenuItem
        Caption = 'Exit'
        OnClick = ExitMenuItemClick
      end
    end
    object Edit1: TMenuItem
      Caption = 'Edit'
    end
    object View1: TMenuItem
      Caption = 'View'
      object ShowClipFileMenuItem: TMenuItem
        Caption = 'Clip File'
        OnClick = ShowClipFileMenuItemClick
      end
    end
  end
end
