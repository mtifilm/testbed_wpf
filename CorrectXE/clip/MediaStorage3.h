// MediaStorage.h: interface for the CMediaStorage class.
//
/*
$Header: /usr/local/filmroot/clip/include/MediaStorage3.h,v 1.16 2007/07/15 23:10:26 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef MediaStorage3H
#define MediaStorage3H

#include <string>
#include <iostream>

using std::string;
using std::ostream;

#include "MediaInterface.h"

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CIniFile;
class CMediaAccess;
class CMediaFormat;
class CClip;

//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CMediaStorage
{
public:
   CMediaStorage();
   CMediaStorage(EMediaType newMediaType,
                 const string& newMediaIdentifier,
                 const string& newHistoryDirectory);
   virtual ~CMediaStorage();

   // Accessor functions
   CMediaAccess* getMediaAccessObj() const;
   const string& getMediaIdentifier() const;
   const string& getHistoryDirectory() const;
   EMediaType getMediaType() const;

   // Mutator functions
   void setMediaAccessObj(CMediaAccess* newMediaAccessObj);
   void setMediaIdentifier(const string& newMediaIdentifier);
   void setMediaType(EMediaType newMediaType);

	void dump(MTIostringstream& str) const;   // print object info to stream for debugging

private:
   EMediaType mediaType;    // Media type, e.g., raw disk, filesystem, etc
   string mediaIdentifier;  // Media identifier, e.g. file name
   string historyDirectory; // History storage directory
   CMediaAccess *mediaAccessObj;
};

//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CMediaStorageList
{
public:
   CMediaStorageList();
   virtual ~CMediaStorageList();

   int getMediaStorageCount() const;
   CMediaStorage* getMediaStorage(int mediaStorageIndex) const;
   CMediaStorage* getMediaStorage(const CMediaAccess *mediaAccess) const;
   int getMediaStorageIndex(const CMediaAccess *mediaAccess) const;


   //** Clip2clip hack
   int getDirtyMediaStorageIndex() const;
   void setDirtyMediaStorageIndex(int newIndex);
   void clear();
   //**//

   void addMediaStorage(EMediaType mediaType,
                        const string& mediaIdentifier,
                        const string& historyDirectory,
                        CMediaAccess* mediaAccess);
   void replaceMediaStorage(int mediaStorageIndex,
                             EMediaType mediaType,
                             const string &mediaIdentifier,
                             const string &historyDirectory,
                             CMediaFormat& mediaFormat);

   int registerMediaStorage(CMediaFormat& mediaFormat);

   int readMediaStorageSection(CIniFile *clipFile, const string& sectionPrefix);

   int writeMediaStorageSection(CIniFile *clipFile,
                                const string& sectionPrefix,
                                bool updateMetadataFile = true);

   static EMediaType readMediaStorageType(CIniFile *clipFile,
                                          const string& sectionPrefix);

   static const string &getMetadataIniFilename();

   void dump(MTIostringstream& str) const;   // print object info to stream for debugging

private:
   // Storage Media List and Iterator Type: Vector of pointers to CMediaStorage
   typedef vector<CMediaStorage*> MediaStorage3List;

   MediaStorage3List mediaStorageList;  // List of media storage

   //** Clip2clip hack
   int dirtyMediaStorageIndex;      // Where to allocate "dirty" frames
   //**//

private:
   static string mediaStorageSectionName;
   static string mediaStorageKey;
   static string dirtyMediaStorageIndexKey;

//   static string metaDataFileName;
//   static string metaDataSectionName;
//   static string metaDataDirectoryKey;
//   static string iniFileInMetadataDir;
//
//   static string historySectionName;
//   static string historyTypeKey;
//   static string historyDirectoryKey;
//
//   static string historyTypeFilePerClip;
//   static string historyTypeFilePerFrame;
//
//   static string enableAutoCleanKey;

   static string historyStorageSectionName;
   static string historyStorageKey;

   static string getMediaStorageKey(int index);
   static int parseMediaStorage(const string inString, EMediaType *newMediaType,
                                string& newMediaIdentifier);

   static string getHistoryStorageKey(int index);
   static int parseHistoryStorage(const string inString,
                                  string& newHistoryDirectory);

   int registerOneMediaStorage(int mediaStorageIndex, CMediaFormat& mediaFormat);
   void unregisterMediaStorage();
};

#ifdef PRIVATE_STATIC
#ifndef _MEDIA_STORAGE_3_DATA_CPP
namespace MediaStorage3Data {
   extern string mediaStorageSectionName;
   extern string mediaStorageKey;
   extern string dirtyMediaStorageIndexKey;
};
#endif
#endif
//////////////////////////////////////////////////////////////////////

#endif // #ifndef MediaStorage3H

