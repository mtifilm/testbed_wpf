// Clip3AudioTrackFile.h: interface for the CAudioFrameListFileReader and
//                        CAudioFrameListFileWriter classes.
//
// Created by: John Starr, December 12, 2001
//
/* CVS Info:
$Header: /usr/local/filmroot/clip/include/Clip3AudioTrackFile.h,v 1.5 2005/01/24 18:40:15 mlm Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef CLIP3AUDIOTRACKFILEH
#define CLIP3AUDIOTRACKFILEH

#include "Clip3TrackFile.h"

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CClip;
class CMediaStorageList;
class CAudioFormat;
class CAudioClip;
class CAudioField;
class CAudioFrame;
class CAudioFrameList;

//////////////////////////////////////////////////////////////////////
// Audio Track File Version
#define FR_AUDIO_TRACK3_FILE_TYPE_FILE_PRE_FIELD_LIST_VERSION (FR_INITIAL_FILE_TYPE_VERSION+2)
#define FR_AUDIO_TRACK3_FILE_TYPE_FILE_VERSION (FR_INITIAL_FILE_TYPE_VERSION+3)

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

// Frame file record schema version
#define FR_AUDIO_FRAME3_FILE_RECORD_PRE_FIELD_LIST_VERSION (FR_INITIAL_RECORD_VERSION+2)
#define FR_AUDIO_FRAME3_FILE_RECORD_LATEST_VERSION (FR_INITIAL_RECORD_VERSION+3)

#define AUDIO_FRAME3_RECORD_RESERVED_COUNT    2

//////////////////////////////////////////////////////////////////////

class CAudioFrameFileRecord : public CFrameFileRecord
{
public:
   CAudioFrameFileRecord(MTI_UINT16 version
                             = FR_AUDIO_FRAME3_FILE_RECORD_LATEST_VERSION);
   virtual ~CAudioFrameFileRecord();

   MTI_REAL64 getSlip() const;
   
   void setSlip(MTI_REAL64 newSlip);

   virtual int read(CStructuredStream& stream);
   virtual int write(CStructuredStream &stream);

   void calculateRecordByteCount();

private:
   MTI_REAL64 slip;
   MTI_UINT32 audioFrameReserved[AUDIO_FRAME3_RECORD_RESERVED_COUNT];
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

// Field file record schema version
#define FR_AUDIO_FIELD3_FILE_RECORD_PRE_FIELD_LIST_VERSION (FR_INITIAL_RECORD_VERSION+2)
#define FR_AUDIO_FIELD3_FILE_RECORD_LATEST_VERSION (FR_INITIAL_RECORD_VERSION+3)

#define AUDIO_FIELD3_RECORD_RESERVED_COUNT    2

//////////////////////////////////////////////////////////////////////

class CAudioFieldFileRecord : public CFieldFileRecord
{
public:
   CAudioFieldFileRecord(MTI_UINT16 version
                             = FR_AUDIO_FIELD3_FILE_RECORD_LATEST_VERSION);
   virtual ~CAudioFieldFileRecord();

   virtual int read(CStructuredStream& stream);
   virtual int write(CStructuredStream &stream);

   void calculateRecordByteCount();

private:
   MTI_UINT32 audioFieldReserved[AUDIO_FIELD3_RECORD_RESERVED_COUNT];
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class CAudioFrameListFileReader : public CFrameListFileReader
{
public:
	CAudioFrameListFileReader();
	virtual ~CAudioFrameListFileReader();

   int readFrameListFile(CAudioFrameList& audioFrameList);

private:
   int buildFramesFromFile(CAudioFrameList& audioFrameList);

   CAudioField* readField(CAudioFieldFileRecord& fieldRecord,
                          CFieldList *newFieldList,
                          const CMediaStorageList& mediaStorageList);
   CAudioFrame* readFrame(CAudioFrameFileRecord& frameRecord,
                          const CAudioFormat *audioFormat);

   void setAudioFieldFromFileRecord(CAudioField& field,
                                    CFieldList *newFieldList,
                                    const CMediaStorageList& mediaStorageList,
                                    const CAudioFieldFileRecord &fieldRecord);
   void setAudioFrameFromFileRecord(CAudioFrame& frame,
                                    const CAudioFrameFileRecord &frameRecord);
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class CAudioFrameListFileWriter : public CFrameListFileWriter
{
public:
	CAudioFrameListFileWriter();
	virtual ~CAudioFrameListFileWriter();

   int writeFrameListFile(CAudioFrameList* audioFrameList);

private:
   void setAudioFieldToFileRecord(const CAudioField& field,
                                  const CMediaStorageList &mediaStorageList,
                                  CAudioFieldFileRecord &fieldRecord);
   void setAudioFrameToFileRecord(const CAudioFrame& frame,
                                  CAudioFrameFileRecord &frameRecord);

   int writeField(CAudioField& field,
                  const CMediaStorageList &mediaStorageList);
   int writeFrame(CAudioFrame& frame);
   int writeFrameList(CAudioFrameList* audioFrameList);
};

//////////////////////////////////////////////////////////////////////

#endif // #ifndef CLIP3_AUDIO_TRACK_FILE_H
