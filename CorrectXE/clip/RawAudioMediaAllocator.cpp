// RawAudioMediaAllocator.cpp: implementation of the CRawAudioMediaAllocator class.
//
/*
$Header: /usr/local/filmroot/clip/source/RawAudioMediaAllocator.cpp,v 1.9 2006/06/09 16:46:20 mlm Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "RawAudioMediaAllocator.h"
#include "Clip5Proto.h"
#include "RawAudioMediaAccess.h"
#include "MediaLocation.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRawAudioMediaAllocator::
CRawAudioMediaAllocator(CRawAudioMediaAccess *newMediaAccess,
                        double newSamplesPerField,
                        MTI_UINT32 newFirstFieldOffset, MTI_UINT32 newFieldCount)
: CMediaAllocator(newMediaAccess, newFieldCount),
  firstFieldOffset(newFirstFieldOffset), samplesPerField(newSamplesPerField),
  allocSampleOffset(0)
{
   /** @todo It doesn't look to Matt as though firstSampleOffset is
    *  really needed. */
   firstSampleOffset = newMediaAccess->fieldsToSamples(0, firstFieldOffset);
}


CRawAudioMediaAllocator::
CRawAudioMediaAllocator(CRawAudioMediaAccess *newMediaAccess,
                        double newSamplesPerField,
                        MTI_UINT32 newFirstFieldOffset,
                        MTI_INT64 newAllocSampleOffset,
                        MTI_UINT32 newFieldCount)
: CMediaAllocator(newMediaAccess, newFieldCount),
  firstFieldOffset(newFirstFieldOffset), samplesPerField(newSamplesPerField),
  allocSampleOffset(newAllocSampleOffset)
{
   firstSampleOffset = newMediaAccess->fieldsToSamples(0, firstFieldOffset);
}


CRawAudioMediaAllocator::~CRawAudioMediaAllocator()
{
 
}

//////////////////////////////////////////////////////////////////////

void CRawAudioMediaAllocator::
getFieldMediaAllocation(MTI_UINT32 fieldNumber, 
                        CMediaLocation& mediaLocation)
{
   if (fieldNumber < fieldCount)
      {
      mediaLocation.setMediaAccess(mediaAccess);

      fieldNumber += firstFieldOffset;

      /** @todo The calculation here is a direct translation using
       *  fieldsToSamples() of the version that precedes this one.  I
       *  think there's a chance it's still not quite accurate.  My
       *  uncertainty arises from not knowing what the argument
       *  fieldNumber is relative to during a first allocation and
       *  during an extension respectively.  It's possible that the
       *  calculation should be:
       *  
       *  CRawAudioMediaAccess* rawAudioMediaAccess =
       *     dynamic_cast<CRawAudioMediaAccess*>(mediaAccess);
       *  MTI_UINT32 allocFieldOffset =
       *     rawAudioMediaAccess->samplesToFields(
       *        firstFieldOffset, allocSampleOffset);
       *  MTI_INT64 sampleIndex =
       *     rawAudioMediaAccess->fieldsToSamples(
       *        firstFieldOffset + allocFieldOffset,
       *        fieldNumber - firstFieldOffset);
       *  mediaLocation.setDataIndex(allocSampleOffset + sampleIndex);
       */

      CRawAudioMediaAccess* rawAudioMediaAccess =
         dynamic_cast<CRawAudioMediaAccess*>(mediaAccess);
      MTI_INT64 sampleIndex =
         rawAudioMediaAccess->fieldsToSamples(0, fieldNumber);
      mediaLocation.setDataIndex(allocSampleOffset +
                                 sampleIndex - firstSampleOffset);

      MTI_INT64 samplesInFieldAtIndex =
         rawAudioMediaAccess->fieldsToSamples(fieldNumber, 1);
      mediaLocation.setDataSize(samplesInFieldAtIndex);

      mediaLocation.setFieldName(0);

      /** @todo It doesn't look to Matt as though fieldNumber is used
       *  for anything in CMediaLocation */
      mediaLocation.setFieldNumber(fieldNumber);
      mediaLocation.setMarked(false);   // Media Storage is allocated

#ifdef INIT_AUDIO_FILE
      mediaLocation.setInitialized(true);  // Media is pre-initialized to
                                           // silence when allocated
#endif // #ifdef INIT_AUDIO_FILE
      }
   else
      {
      // The field number exceeds the number of fields allocated
      // Set mediaLocation to invalid state
      mediaLocation.setInvalid();
      mediaLocation.setMarked(true);
      }

}

void CRawAudioMediaAllocator::
getNextFieldMediaAllocation(CMediaLocation& mediaLocation)
{
   getFieldMediaAllocation(currentAllocationIndex, mediaLocation);
   
   currentAllocationIndex += 1;
}

int CRawAudioMediaAllocator::AssignMediaStorage(C5MediaTrack &mediaTrack)
{
   // for clip 5
   CMediaLocation mediaLocation;
   mediaLocation.setMediaAccess(mediaAccess);
   mediaLocation.setDataIndex(allocSampleOffset);
   CRawAudioMediaAccess* rawAudioMediaAccess =
      dynamic_cast<CRawAudioMediaAccess*>(mediaAccess);
   /** @todo Could we just use fieldCount instead of converting
    *  samples to fields?
    */
   MTI_UINT32 allocFieldOffset =
      rawAudioMediaAccess->samplesToFields(0, allocSampleOffset);
   MTI_INT64 samplesInFieldAtIndex =
      rawAudioMediaAccess->fieldsToSamples(allocFieldOffset, 1);
   mediaLocation.setDataSize(samplesInFieldAtIndex);
   // TBD: Do we care about the mediaLocation's field number?

   C5AudioStoreRange *audioStoreRange
                          = new C5AudioStoreRange(mediaLocation, fieldCount,
                                                  samplesPerField);
   mediaTrack.AppendRange(audioStoreRange);

   return 0;
}


