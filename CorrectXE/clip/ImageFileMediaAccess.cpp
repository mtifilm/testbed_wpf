// ImageFileMediaAccess.cpp: implementation of the CImageFileMediaAccess class.
//
//////////////////////////////////////////////////////////////////////

//#define _IMAGE_FILE_MEDIA_ACCESS_DATA_CPP //#ifdef PRIVATE_STATIC

#include "err_clip.h"
#include "err_file.h"
#include "Clip5Proto.h"
#include "HRTimer.h"         // For debugging
#include "ImageFormat3.h"
#include "ImageFileMediaAccess.h"
#include "IniFile.h"
#include "MediaFileIO.h"
#include "MediaFrameBuffer.h"
//#include "MediaFrameFileReader.h"
#include "MediaLocation.h"
#include "MTImalloc.h"
#include "MTIstringstream.h"
#include "ThreadLocker.h"
#include "timecode.h"
#include "FileSweeper.h"
#include <vector>
using std::vector;
#include <algorithm>
using std::sort;

string CImageFileMediaAccess::makeImageFileName(const char *fieldName) const
{
	static string bar;
	string *foo = new string("foo");
	*foo += "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";
	bar = *foo;
   delete foo;

   return fileNamePrefix + fieldName + fileNameSuffix;
}
//////////////////////////////////////////////////////////////////////
// Static Member Data

string CImageFileMediaAccess::frameNumberLeftDelimiter = ",@[";
string CImageFileMediaAccess::frameNumberRightDelimiter = "]";
// Characters used for formatting frame number digits in image file names
string CImageFileMediaAccess::digitFormattingChars = "@#";

bool CImageFileMediaAccess::useBinarySearchFlag = true;

//////////////////////////////////////////////////////////////////////////////

#ifdef PRIVATE_STATIC
//////////////////////////////////////////////////////////////////////
// The following variables used to be static class members.
// I switched the to namespaced globals for compatability with
// visual c++.  -Aaron Geman  7/9/04
//////////////////////////////////////////////////////////////////////

namespace ImageFileMediaAccessData {
   string frameNumberLeftDelimiter = ",@[";
   string frameNumberRightDelimiter = "]";
};
using namespace ImageFileMediaAccessData;
#endif

#define INVALID_FRAME_NUMBER (-1)

// This should be in a common DLL somewhere!!!
/*-----------GetSystemErrorMessage------------------------JAM--Mar 2000----*/

static  string GetSystemMessage(int ErrorCode)

/* This function returns a string that contains a human readable     */
/* version of the error message in ErrorCode                         */
/* Ususal usage is ErrorMessage = GetSystemMessage(GetLastError());  */
/*                                                                   */
/*********************************************************************/
{
  char cErrorMessage[255];

  string Result = "";

  // Do nothing if not an error
  if (ErrorCode == 0)
    {
       return(Result);
    }

  FormatMessage
          (
             FORMAT_MESSAGE_FROM_SYSTEM,
             NULL,
             ErrorCode,
             MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
             (LPTSTR) &cErrorMessage,
             255,
             NULL
          );

  Result = cErrorMessage;
  return(Result);
};

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CImageFileMediaAccess::CImageFileMediaAccess(const string& newMediaIdentifier,
                                             const string& newHistoryDirectory,
															const CMediaFormat& newMediaFormat,
                                             EMediaType newMediaType)
: CMediaAccess(newMediaType, newMediaIdentifier,
                             newHistoryDirectory)

{
	string imageFileName;
   if (CImageFileMediaAccess::IsTemplate(newMediaIdentifier))
      imageFileName = getTemplate();
   else
      imageFileName = newMediaIdentifier;

   CImageFileMediaAccess::SplitFileName(imageFileName,
                                        fileNamePrefix,
                                        fileNameSuffix,
                                        fileName);

// NB: A CImageFileMediaAccess must be initialized with a call to the
//     function CImageFileMediaAccess::initialize before it can be used
}


CImageFileMediaAccess::~CImageFileMediaAccess()
{
}

//////////////////////////////////////////////////////////////////////

int CImageFileMediaAccess::initialize(const CMediaFormat& newMediaFormat)
{
   int retVal;

	if (newMediaFormat.getMediaFormatType() != MEDIA_FORMAT_TYPE_VIDEO3)
      return CLIP_ERROR_INVALID_MEDIA_FORMAT_TYPE;

	clipImageFormat = static_cast<const CImageFormat&>(newMediaFormat);

#ifdef I_THINK_THIS_STUFF_IS_USELESS_AND_IT_SLOWS_DOWN_THE_BIN_MANAGER
   string imageFileName;
	if(CImageFileMediaAccess::IsTemplate(mediaIdentifier))
      {
		imageFileName = CImageFileMediaAccess::GenerateFileName2(getTemplate(),
                                                      getFrameNumber());
      }
   else
      {
		imageFileName = mediaIdentifier;
      }

   // Initialize the CImageFileIO class instance used to access the
   // image file.  Initialization depends on whether the image file
   // with the name in newMediaIdentifier exists yet
   if (DoesFileExist(imageFileName))
      {
#ifdef DO_CINTEL_ALPHA_HACK
      //********* EMBEDDED ALPHA HACK-O-RAMA (aka CINTEL_HACK )*************
		// Need to initialize the ImageFileIO's ImageFormat because it can't
      // deduce the CINTEL HACK form of the alpha information being hidden
      // in unused bits of the data, so needs to be told that they are there.
      // This needs to be done BEFORE InitFromExistingFile() is called!
		//
      imageFileIO->OverrideImageFormat(clipImageFormat);
      //********************************************************************
#endif

		// The image file exists, so initialize the CImageFileIO instance
//xxxmfioxxx      retVal = imageFileIO->InitFromExistingFile(imageFileName);

		MediaFileIO mediaFileIO;
		retVal = mediaFileIO.prefetchFrameFile(imageFileName, clipImageFormat);
		if (retVal)
			{
			TRACE_0(errout << "ERROR: CImageFileMediaAccess::initialize: fileReader->FetchFile()"
								<< endl
								<< "       failed on " << imageFileName << endl
								<< "       Return code: " << retVal);
			return retVal;  // This shouldn't happen.
			}

      // Compare the image format derived from the image file with the
      // clip's image format.
		CImageFormat fileImageFormat;
		retVal = mediaFileIO.getImageFormat(imageFileName, fileImageFormat);
		if (!clipImageFormat.compare(fileImageFormat, true))
         {
         TRACE_0(errout << "ERROR: CImageFileMediaAccess::initialize: Image Format derived from "
                        << "       Image File " << imageFileName << endl
                        << "       did not match clip's Image Format");
			return CLIP_ERROR_IMAGE_FILE_INCONSISTENT_HEADER;
         }
      }
	else
		{
		///////////////// UNNECESSARY - MediaFileIO is stateless!! //////////////
//		int imageFileType = queryImageFileType(getMediaType());
//		retVal = imageFileIO->InitFromImageFormat(imageFormat, imageFileType);
//		if (retVal)
//			{
//			TRACE_0(errout << "ERROR: CImageFileMediaAccess::initialize: imageFileIO->InitFromImageFormat"
//								<< endl
//								<< "       failed.  Return code: " << retVal);
//			return retVal;  // This shouldn't happen.
//			}
		}
#endif

   return 0;
}

//////////////////////////////////////////////////////////////////////

int CImageFileMediaAccess::read(MTI_UINT8* buffer, CMediaLocation& mediaLocation)
{
	int retVal;

   // Construct image file name
   string imageFileName = makeImageFileName(mediaLocation.getFieldName());

	// Check if image file exists
	// QQQ I think unnecessary - should parse retVal from getFrameBuffer() below
	if (!DoesFileExist(imageFileName))
      return CLIP_ERROR_IMAGE_FILE_MISSING;

   // Set up array of buffer pointers.
   MTI_UINT8* bufferArray[2];
   bufferArray[0] = buffer;
   bufferArray[1] = 0;

//xxxmfioxxx		retVal = imageFileIO->ReadImage(
//						imageFileName,
//						mediaLocation.getFieldNumber(),
//						bufferArray);
	MediaFrameBufferSharedPtr frameBuffer = nullptr;
	MediaFileIO mediaFileIO;
	retVal = mediaFileIO.readFrameBuffer(imageFileName, frameBuffer, clipImageFormat);
	if (retVal != 0)
	{
		return retVal;
	}

	CImageFormat imageFormat = frameBuffer->getImageFormat();

   // QQQ WHAT ABOUT COMPONENTS?
   if (imageFormat.getPixelsPerLine() != clipImageFormat.getPixelsPerLine()
   || imageFormat.getLinesPerFrame() != clipImageFormat.getLinesPerFrame())
   {
		return CLIP_ERROR_IMAGE_FILE_INCONSISTENT_HEADER;
   }

	// This is a start - prevent access violations!
	if (imageFormat.getBytesPerFieldExcAlpha() > clipImageFormat.getBytesPerFieldExcAlpha())
	{
		return CLIP_ERROR_IMAGE_FILE_INCONSISTENT_HEADER;
	}

	MTImemcpy(buffer, frameBuffer->getImageDataPtr(), imageFormat.getBytesPerFieldExcAlpha());

	// CAREFUL! It is OK for the clip to have a separate alpha matte and this
	// frame to NOT have one, and vice versa!
	if (frameBuffer->hasSeparateAlpha() && clipImageFormat.getAlphaMatteType() == imageFormat.getAlphaMatteType())
	{
		// They match! All is good.
		MTImemcpy(buffer + imageFormat.getBytesPerFieldExcAlpha(), frameBuffer->getAlphaDataPtr(), imageFormat.getAlphaBytesPerField());
	}
	else if (clipImageFormat.getAlphaMatteType() == IF_ALPHA_MATTE_1_BIT
	|| clipImageFormat.getAlphaMatteType() == IF_ALPHA_MATTE_8_BIT
	|| clipImageFormat.getAlphaMatteType() == IF_ALPHA_MATTE_16_BIT)
	{
      // They don't match. If the clip is expecting alpha, set it to all 0s.
		MTImemset(buffer + clipImageFormat.getBytesPerFieldExcAlpha(), 0, clipImageFormat.getAlphaBytesPerField());
	}

	return 0;
}

int CImageFileMediaAccess::readWithOffset(MTI_UINT8* buffer,
                                          MTI_UINT8* &offsetBuffer,
                                          CMediaLocation& mediaLocation)
{
//xxxmfioxxx	int retVal;
//	CHRTimer timer;
//	MTIostringstream os; os << "TID " << setw(6) << setfill(' ') << GetCurrentThreadId(); string tid = os.str();
//	TRACE_3(errout << "IMAGEFILEIO " << tid << "   TRY LOCK 2 WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
//	CAutoThreadLocker atl(imageFileIO->getLock());// Lock thread until return
//	TRACE_3(errout << "IMAGEFILEIO " << tid << "   WAIT LOCK 2 @ " << timer.Read() << " msec WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");

	offsetBuffer = buffer;
	return read(buffer, mediaLocation);

//xxxmfioxxx	// Construct image file name
//	string imageFileName = makeImageFileName(mediaLocation.getFieldName());
//
//	// Check if image file exists
//	//   - we are now skipping this check since CreateFile() does an
//	//     implicit check for file existence and that will return an
//	//     error if the file does not exist.  This hopefully will speed up
//	//     things relative to bug 1946 (creating image file sequence clips
//	//     hogs bandwidth of SAN partition) since we will be doing
//	//     one less open which seems to aggravate CXFS. - mpr 4/27/2005
//	//
//	//   if (!DoesFileExist(imageFileName))
//	//      return CLIP_ERROR_IMAGE_FILE_MISSING;
//
//	// Set up array of buffer pointers.
//	MTI_UINT8* bufferArray[2];
//	bufferArray[0] = buffer;
//	bufferArray[1] = 0;
//
//	size_t offset;
//		retVal = imageFileIO->ReadImageNew(
//						imageFileName,
//						mediaLocation.getFieldNumber(),
//						bufferArray,
//						offset);
//	if (retVal != 0)
//	{
//		return retVal;
//	}
//
//	TRACE_3(errout << "(=)(=)(=)(=) 1 ReadImageNew(" << imageFileName << ") returned offset " << offset << " (=)(=)(=)(=)");
//
//	offsetBuffer += (MTI_UINT32)offset;
//
//	TRACE_3(errout << "IMAGEFILEIO " << tid << "   UNLOCK 2 AFTER " << timer.Read() << " msec WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
//	return 0;
}

////xxxmfioxxx	// Construct image file name
//bool CImageFileMediaAccess::canBeReadAsynchronously(CMediaLocation &mediaLocation)
//{
////xxxmfioxxx	return imageFileIO->CanDoAsynchReadImage();
//	return true;
//}
//
//struct AsynchReadContext
//{
//	MTI_UINT8* buffer;
//	MediaFrameFileReader *reader;
//};
//
//int CImageFileMediaAccess::startAsynchReadWithOffset(MTI_UINT8* buffer,
//																	  MTI_UINT8* &offsetBuffer,
//																	  CMediaLocation& mediaLocation,
//                                                     void* &readContext,
//																	  bool dontNeedAlpha)
//{
//	int retVal;
////xxxmfioxxx   CHRTimer timer;
////   MTIostringstream os; os << "TID " << setw(6) << setfill(' ') << GetCurrentThreadId(); string tid = os.str();
////   TRACE_3(errout << "IMAGEFILEIO " << tid << "     TRY LOCK 3 WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
////   CAutoThreadLocker atl(imageFileIO->getLock());// Lock thread until return
////   TRACE_3(errout << "IMAGEFILEIO " << tid << "     WAIT LOCK 3 @ " << timer.Read() << " msec WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
//
//	// Initialize this - will be bumped at finish
//	offsetBuffer = buffer;
//
//	// Construct image file name
//	string imageFileName = makeImageFileName(mediaLocation.getFieldName());
//	TRACE_3(errout << "(=)(=)(=)(=) 1 startAsynchReadWithOffset(" << imageFileName << " (=)(=)(=)(=)");
//
////xxxmfioxxx
////	// Set up array of buffer pointers.
////	MTI_UINT8* bufferArray[2];
////	bufferArray[0] = buffer;
////	bufferArray[1] = 0;
////
////		retVal = imageFileIO->ReadImageNew_Asynch_Start(
////						imageFileName,
////						mediaLocation.getFieldNumber(),
////						bufferArray,
////						readContext,
////						dontNeedAlpha);
////   TRACE_3(errout << "IMAGEFILEIO " << tid << "     UNLOCK 3 AFTER " << timer.Read() << " msec WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
//
//		MediaFileIO mediaFileIO;
//		retVal = MediaFileIO.prefetchFrameFile(clipImageFormat);
//	if (retVal != 0)
//	{
//		delete reader;
//		return retVal;
//	}
//
//	readContext = new AsynchReadContext{buffer, reader};
//
////xxxmfioxxx
//
//	return 0;
//}
//
//bool CImageFileMediaAccess::isAsynchReadWithOffsetDone(void* readContext)
//{
////xxxmfioxxx
//	return true;
////xxxmfioxxx
////	bool retVal;
////	CHRTimer timer;
////	MTIostringstream os; os << "TID " << setw(6) << setfill(' ') << GetCurrentThreadId(); string tid = os.str();
////	TRACE_3(errout << "IMAGEFILEIO " << tid << "       TRY LOCK 4 WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
////	CAutoThreadLocker atl(imageFileIO->getLock());// Lock thread until return
////	TRACE_3(errout << "IMAGEFILEIO " << tid << "       WAIT LOCK 4 @ " << timer.Read() << " msec WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
////
////	retVal = imageFileIO->Is_ReadImageNew_Asynch_Done(readContext);
////
////	TRACE_3(errout << "IMAGEFILEIO " << tid << "       UNLOCK 4 AFTER " << timer.Read() << " msec WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
////	return retVal;
////xxxmfioxxx
//}
//
//int CImageFileMediaAccess::finishAsynchReadWithOffset(void* &readContext,
//																		MTI_UINT8* &offsetBuffer)
//{
//	AsynchReadContext *rc = static_cast<AsynchReadContext *>(readContext);
//	MTIassert(rc != nullptr);
//	if (rc == nullptr)
//	{
//		return CLIP_ERROR_FILE_READ_FAILED;
//	}
//
//	MTI_UINT8 *buffer = rc->buffer;
//	MediaFrameFileReader *reader = rc->reader;
//	readContext = nullptr;
//
//	MediaFrameBuffer *frameBuffer = nullptr;
//	int retVal = reader->getFrameBuffer(frameBuffer, clipImageFormat);
//	if (retVal != 0)
//	{
//		return retVal;
//	}
//
//	MTImemcpy(buffer, frameBuffer->getImageDataPtr(), frameBuffer->getImageDataSize());
//
//	delete frameBuffer;
//
//
////xxxmfioxxx
////	int retVal;
////	CHRTimer timer;
////	MTIostringstream os; os << "TID " << setw(6) << setfill(' ') << GetCurrentThreadId(); string tid = os.str();
////	TRACE_3(errout << "IMAGEFILEIO " << tid << "         TRY LOCK 5 WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
////	CAutoThreadLocker atl(imageFileIO->getLock());// Lock thread until return
////	TRACE_3(errout << "IMAGEFILEIO " << tid << "         WAIT LOCK 5 @ " << timer.Read() << " msec WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
////
////	size_t offset = 0;
////	retVal = imageFileIO->ReadImageNew_Asynch_Finish(readContext,
////																	 offset,
////																	 &imageFileIO->getLock());    // QQQ GET RID OF THISD
////	if (retVal != 0)
////	{
////		return retVal;
////	}
////
////	TRACE_3(errout << "(=)(=)(=)(=) 2 finishAsynchReadWithOffset() returned offset " << offset << " (=)(=)(=)(=)");
////
////	offsetBuffer += (MTI_UINT32)offset;
////
//////   TRACE_3(errout << "IMAGEFILEIO " << tid << "         UNLOCK 5 AFTER " << timer.Read() << " msec WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
////xxxmfioxxx
//
//	return 0;
//}
////---------------------------------------------------------------------------
//
//bool CImageFileMediaAccess::canReadFrameBuffers(CMediaLocation &mediaLocation)
//{
////xxxmfioxxx
//	return true;
////	return imageFileIO->canReadFrameBuffers();
//}
////---------------------------------------------------------------------------
//
//int CImageFileMediaAccess::startAsyncFrameBufferRead(
//			CMediaLocation& mediaLocation,
//			void* &readContext)
//{
//	int retVal;
//
//	// Construct image file name
//	string imageFileName = makeImageFileName(mediaLocation.getFieldName());
//	TRACE_3(errout << "(=)(=)(=)(=) 1 startAsyncFrameBufferRead(" << imageFileName << " (=)(=)(=)(=)");
//
//	MediaFileIO mediaFileIO;
//	retVal = MediaFileIO.prefetchFrameFile(clipImageFormat);
//	if (retVal != 0)
//	{
//		delete reader;
//		return retVal;
//	}
//
//	readContext = reader;
//
////xxxmfioxxx
////	int retVal;
////
////	// Construct image file name
////	string imageFileName = makeImageFileName(mediaLocation.getFieldName());
////	TRACE_3(errout << "(=)(=)(=)(=) 1 startFileBufferRead(" << imageFileName << " (=)(=)(=)(=)");
////
////	{
////      CHRTimer timer;
////      MTIostringstream os; os << "TID " << setw(6) << setfill(' ') << GetCurrentThreadId(); string tid = os.str();
////      TRACE_3(errout << "IMAGEFILEIO " << tid << "           TRY LOCK 6 WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
////      CAutoThreadLocker atl(imageFileIO->getLock());// Lock thread until return
////      TRACE_3(errout << "IMAGEFILEIO " << tid << "           WAIT LOCK 6 @ " << timer.Read() << " msec WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
////
////		retVal = imageFileIO->startAsyncFrameBufferRead(
////						imageFileName,
////						readContext);
//////      TRACE_3(errout << "IMAGEFILEIO " << tid << "           UNLOCK 6 AFTER " << timer.Read() << " msec WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
////	}
////
////	if (retVal != 0)
////		return retVal;
////xxxmfioxxx
//
//	return 0;
//}
////---------------------------------------------------------------------------
//
//bool CImageFileMediaAccess::isAsyncFrameBufferReadDone(void* readContext)
//{
//	return true;
////xxxmfioxxx
////	bool retVal;
////	CHRTimer timer;
////	MTIostringstream os; os << "TID " << setw(6) << setfill(' ') << GetCurrentThreadId(); string tid = os.str();
////	TRACE_3(errout << "IMAGEFILEIO " << tid << "               TRY LOCK 7 WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
////	CAutoThreadLocker atl(imageFileIO->getLock());// Lock thread until return
////	TRACE_3(errout << "IMAGEFILEIO " << tid << "               WAIT LOCK 7 @ " << timer.Read() << " msec WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
////
////	retVal = imageFileIO->isAsyncFrameBufferReadDone(readContext);
////
////	TRACE_3(errout << "IMAGEFILEIO " << tid << "               UNLOCK 7 AFTER " << timer.Read() << " msec WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
////	return retVal;
////xxxmfioxxx
//}
////---------------------------------------------------------------------------
//
//int CImageFileMediaAccess::finishAsynchFrameBufferRead(
//	void* &readContext,
//	MediaFrameBuffer* &frameBufferOut)
//{
//	AsynchReadContext *rc = static_cast<AsynchReadContext *>(readContext);
//	MTIassert(rc != nullptr);
//	if (rc == nullptr)
//	{
//		return CLIP_ERROR_FILE_READ_FAILED;
//	}
//
//	MTI_UINT8 *buffer = rc->buffer;
//	MediaFrameFileReader *reader = rc->reader;
//	readContext = nullptr;
//
//	MediaFrameBuffer *frameBuffer = nullptr;
//	int retVal = reader->getFrameBuffer(frameBufferOut, clipImageFormat);
//	if (retVal != 0)
//	{
//		return retVal;
//	}
//
////xxxmfioxxx
////	{
////		CHRTimer timer;
////		MTIostringstream os; os << "TID " << setw(6) << setfill(' ') << GetCurrentThreadId(); string tid = os.str();
////		TRACE_3(errout << "IMAGEFILEIO " << tid << "               TRY LOCK 8 WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
////		CAutoThreadLocker atl(imageFileIO->getLock());// Lock thread until return
////		TRACE_3(errout << "IMAGEFILEIO " << tid << "               WAIT LOCK 8 @ " << timer.Read() << " msec WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
////
////		retVal = imageFileIO->finishAsynchFrameBufferRead(
////						readContext,
////						frameBufferOut,
////						&imageFileIO->getLock());     // QQQ GET RID OF THIS
//////      TRACE_3(errout << "IMAGEFILEIO " << tid << "               UNLOCK 8 AFTER " << timer.Read() << " msec WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
////	}
////xxxmfioxxx
//
//	return 0;
//}
//---------------------------------------------------------------------------

int CImageFileMediaAccess::read(MTI_UINT8* buffer, MTI_INT64 offset, unsigned int size)
{
	MTIassert(false);
	return FILE_ERROR_UNSUPPORTED_COMMAND;
}

//---------------------------------------------------------------------------
int CImageFileMediaAccess::write(const MTI_UINT8* buffer,
                                 CMediaLocation& mediaLocation)
{
	// We always update an existing file, never create a new one. So we need
	// to read in the source file to modify it (presumably it'll be sitting
	// in cache). The source is usually the previous version of the frame we're
	// updating, but if we're in a version clip where the frame doesn't exist
   // yet, the source is the corresponding parent clip frame.
	string sourcePath = mediaLocation.getSourceImageFilePath();
	string destPath = makeImageFileName(mediaLocation.getFieldName());
	if (sourcePath.empty())
	{
		sourcePath = destPath;
	}

	MediaFileIO mediaFileIO;
	MediaFrameBufferSharedPtr sourceFrameBuffer = nullptr;
	int retVal = mediaFileIO.readFrameBuffer(sourcePath, sourceFrameBuffer);
	if (retVal != 0)
	{
		return retVal;
	}

	// Copy the non-image-data bits.
	auto destFrameBuffer = sourceFrameBuffer->cloneWithoutImageData();
	if (!destFrameBuffer)
	{
      // This is an internal error - shouldn't ever fail here.
		return FILE_ERROR_UNSUPPORTED_COMMAND;
	}

	// TODO:::: CHECK IMAGE FORMAT COMPATIBILITY HERE!!!
	// QQQ CAREFUL - getImageDataSize() returns the incorrect value, it includes the separate alpha matte!
//	DBTRACE((size_t)clipImageFormat.getBytesPerFieldExcAlpha());
//	DBTRACE(destFrameBuffer->getImageDataSize());
	MTIassert((size_t)clipImageFormat.getBytesPerFieldExcAlpha() <= destFrameBuffer->getImageDataSize());

	// Replace the image data/ Note that we try not to touch the original alpha data.
	size_t imageDataByteCount = std::min<size_t>(
											(size_t)clipImageFormat.getBytesPerFieldExcAlpha(),
											destFrameBuffer->getImageDataSize());
	MTImemcpy(destFrameBuffer->getImageDataPtr(), buffer, imageDataByteCount);

	// Write it out.
	retVal = mediaFileIO.writeFrameBuffer(destPath, destFrameBuffer);
	if (retVal)
	{
		return retVal;
	}

	return 0;

//xxxmfioxxx
//	CHRTimer timer;
//	MTIostringstream os; os << "TID " << setw(6) << setfill(' ') << GetCurrentThreadId(); string tid = os.str();
//	TRACE_3(errout << "IMAGEFILEIO " << tid << "                 TRY LOCK 9 WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
//	CAutoThreadLocker atl(imageFileIO->getLock());// Lock thread until return
//	TRACE_3(errout << "IMAGEFILEIO " << tid << "                 WAIT LOCK 9 @ " << timer.Read() << " msec WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
//
//	int retVal;
//
//	// Construct image file name
//	string imageFileName = makeImageFileName(mediaLocation.getFieldName());
//
//	bool marked = mediaLocation.isMarked();
//
//	// Set up array of buffer pointers.
//	// NB: 1) This only works for progressive scan image files
//	//     2) const_cast is done because CreateNewImage and WriteImage take
//	//        non-const buffer argument.
//	MTI_UINT8* bufferArray[2];
//	bufferArray[0] = const_cast<MTI_UINT8*>(buffer);
//	bufferArray[1] = 0;
//
//	if (marked)
//		{
//#ifdef NEEDS_MORE_WORK
//      // Check if file exists when it is should not
//      if (DoesFileExist(imageFileName))
//         return CLIP_ERROR_IMAGE_FILE_ALREADY_EXISTS;
//#endif // #ifdef NEEDS_MORE_WORK
//
//      //// Dave's DPX Header Copy Hack
//      string sourcePath = mediaLocation.getSourceImageFilePath();
//		if ((sourcePath.empty()) && DoesFileExist(imageFileName))
//      {
//          sourcePath = imageFileName;
//      }
//
//		imageFileIO->CopySourceFileHeaderHack(sourcePath.c_str());
//
//      // Create a new image file per CImageFileIO instance
//      retVal = imageFileIO->CreateNewImage(imageFileName,
//                                           mediaLocation.getFieldNumber(),
//                                           bufferArray);
//      if (retVal != 0)
//         return retVal;
//
//      mediaLocation.setMarked(false);
//		}
//	else
//		{
//		// Check if file is missing when it is expected to exist
//		if (!DoesFileExist(imageFileName))
//         return CLIP_ERROR_IMAGE_FILE_MISSING;
//
//      // Write to an existing image file via CImageFileIO
//		retVal = imageFileIO->WriteImage(
//                  imageFileName,
//						mediaLocation.getFieldNumber(),
//						bufferArray);
//		if (retVal != 0)
//			return retVal;
//		}
//
//	TRACE_3(errout << "IMAGEFILEIO " << tid << "                 UNLOCK 9 AFTER " << timer.Read() << " msec WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
//xxxmfioxxx
//
//	return 0;
}

//---------------------------------------------------------------------------
int CImageFileMediaAccess::readMultiple(int count, MTI_UINT8* bufferArray[],
													 CMediaLocation mediaLocationArray[])
{
	MTIassert(false);
	return FILE_ERROR_UNSUPPORTED_COMMAND;
}

//---------------------------------------------------------------------------
int CImageFileMediaAccess::readWithOffsetMultiple(int count,
                                          MTI_UINT8* bufferArray[],
														MTI_UINT8* offsetBufferArray[],
                                          CMediaLocation mediaLocationArray[])
{
	MTIassert(false);
	return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

//---------------------------------------------------------------------------
int CImageFileMediaAccess::writeMultiple(int count, MTI_UINT8* bufferArray[],
                                         CMediaLocation mediaLocationArray[])
{
	MTIassert(false);
	return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

//---------------------------------------------------------------------------
int CImageFileMediaAccess::write(const MTI_UINT8* buffer, MTI_INT64 offset,
                                 unsigned int size)
{
	MTIassert(false);
	return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

//---------------------------------------------------------------------------
int CImageFileMediaAccess::fetchMediaFile(CMediaLocation &mediaLocation)
{
	CHRTimer timer;
	MTIostringstream os; os << "TID " << std::setw(6) << std::setfill(' ') << GetCurrentThreadId(); string tid = os.str();

	string imageFileName = makeImageFileName(mediaLocation.getFieldName());
	MediaFileIO mediaFileIO;
	int retVal = mediaFileIO.prefetchFrameFile(imageFileName, clipImageFormat);

	TRACE_3(errout << "(=)(=)(=)(=) " << tid << " fetchMediaFile(" << imageFileName << ") ==> "
	               << retVal << " took " << timer.Read() << " msec (=)(=)(=)(=)");
	return retVal;
}
//---------------------------------------------------------------------------

int CImageFileMediaAccess::readMediaFrameBuffer(MediaFrameBufferSharedPtr &frameBuffer,
																CMediaLocation &mediaLocation)
{
	CHRTimer timer;
	MTIostringstream os; os << "TID " << std::setw(6) << std::setfill(' ') << GetCurrentThreadId(); string tid = os.str();

	string imageFileName = makeImageFileName(mediaLocation.getFieldName());
	MediaFileIO mediaFileIO;
	int retVal = mediaFileIO.readFrameBuffer(imageFileName, frameBuffer, clipImageFormat);
	if (retVal != 0)
	{
		return retVal;
	}

	CImageFormat imageFormat = frameBuffer->getImageFormat();
   bool widthsMatch = imageFormat.getPixelsPerLine() == clipImageFormat.getPixelsPerLine();
   bool heightsMatch = imageFormat.getLinesPerFrame() == clipImageFormat.getLinesPerFrame();
   bool componentsAreOk = imageFormat.getPixelComponents() == clipImageFormat.getPixelComponents();
// I would LOVE to make this work, but, sadly, it crashes in the fucking extractor. QQQ
//                        || (imageFormat.getPixelComponents() == IF_PIXEL_COMPONENTS_RGB
//                            && clipImageFormat.getPixelComponents() == IF_PIXEL_COMPONENTS_RGBA)
//                        || (imageFormat.getPixelComponents() == IF_PIXEL_COMPONENTS_RGBA
//                            && clipImageFormat.getPixelComponents() == IF_PIXEL_COMPONENTS_RGB);

   if (widthsMatch == false || heightsMatch == false || componentsAreOk == false)
   {
      MTIostringstream imageDimensions;
      MTIostringstream clipDimensions;
      imageDimensions << imageFormat.getPixelsPerLine() << "x" << imageFormat.getLinesPerFrame()
                      << "x" << imageFormat.getComponentsPerPixel();
      clipDimensions << clipImageFormat.getPixelsPerLine() << "x" << clipImageFormat.getLinesPerFrame()
                      << "x" << clipImageFormat.getComponentsPerPixel();
		int errorCode = CLIP_ERROR_IMAGE_FILE_INCONSISTENT_HEADER;
		TRACE_0(errout << "ERROR: Image dimensions for file " << imageFileName << " (" << imageDimensions.str() << ")"
                     << " do not match clip format (" << clipDimensions.str() << ")");

      string shortString = "FRAME DIMENSIONS DO NOT MATCH CLIP ";
      MTIostringstream errorMsg;
      errorMsg << "Frame: " << imageDimensions.str() << "      Clip: " << clipDimensions.str();
		MediaFrameBuffer::ErrorInfo errorInfo { errorCode, imageFileName, shortString, errorMsg.str() };
      frameBuffer = MediaFrameBuffer::CreateErrorBuffer(imageFormat, errorInfo);
		return CLIP_ERROR_IMAGE_FILE_INCONSISTENT_HEADER;
   }

	TRACE_3(errout << "(=)(=)(=)(=) " << tid << " readMediaFrameBuffer(" << imageFileName << ") ==> "
						<< retVal << " took " << timer.Read() << " msec (=)(=)(=)(=)");
	return retVal;
}
//---------------------------------------------------------------------------

int CImageFileMediaAccess::writeMediaFrameBuffer(MediaFrameBufferSharedPtr frameBuffer, CMediaLocation &mediaLocation)
{
//////////////////////////////////////////////////////////////////////////
// NOTE!!!! THIS IS NOT IMPLEMENTED CORRECTLY. HERE WE SHOULD EXPECT
// THAT THE FILE IS READY TO BE WRITTEN TO THE DISK, SO THE CALLER NEEDS
// TO DO THE SOURCE READING STUFF to avoid copying the data.
/////////////////////////////////////////////////////////////////////////

	return write(frameBuffer->getImageDataPtr(), mediaLocation);


//////////////////////////////////////////////////////////////////////////
// SO THIS IS WHAT THIS METHOD IS SUPPOSED TO LOOK LIKE!
//////////////////////////////////////////////////////////////////////////
//	string imageFilePath = makeImageFileName(mediaLocation.getFieldName());
//
//	MediaFileIO mediaFileIO;
//	int retVal = mediaFileIO.writeFrameBuffer(imageFilePath, frameBuffer);
//	if (retVal)
//	{
//		return retVal;
//	}
//
//	return 0;
}
//---------------------------------------------------------------------------

//////////////////////////////////////////////////////////////////////
//  Move Image Media
//////////////////////////////////////////////////////////////////////

int CImageFileMediaAccess::move(CMediaLocation &fromLocation,
                                CMediaLocation &toLocation)
{
//xxxmfioxxx
//	CHRTimer timer;
//	MTIostringstream os; os << "TID " << setw(6) << setfill(' ') << GetCurrentThreadId(); string tid = os.str();
//	TRACE_3(errout << "IMAGEFILEIO " << tid << "                   TRY LOCK 10 WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
//	CAutoThreadLocker atl(imageFileIO->getLock());// Lock thread until return
//	TRACE_3(errout << "IMAGEFILEIO " << tid << "                   WAIT LOCK 10 @ " << timer.Read() << " msec WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
//	int retVal;
//xxxmfioxxx

   // Construct image file names; note we are the "FROM" media access!!
   // FROM
   string fromFileName = makeImageFileName(fromLocation.getFieldName());

   // TO
   // OK this is a bloody hack... we need to reach into the other
   // CImageFileMediaAccess object to get it to calculate the name
   CMediaAccess *toMediaAccess = toLocation.getMediaAccess();
   CImageFileMediaAccess *toImageFileMediaAccess =
          dynamic_cast<CImageFileMediaAccess *>(toMediaAccess);
   if (toImageFileMediaAccess == NULL)
      return CLIP_ERROR_UNEXPECTED_NULL_RETURN; // something is horribly wrong
   string toFileName =
          toImageFileMediaAccess->makeImageFileName(toLocation.getFieldName());

   // I don't really understand this "marked" stuff
#if 0  // WTF?!? WHY IS THIS FRICKIN' THING MARKED?
   if (fromLocation.isMarked())
      {
      // Apparently, the "from" file does not yet exist?!?
      return CLIP_ERROR_MEDIA_CANNOT_BE_TRANSFERRED;
      }
#endif // 0

	// Move the image file
	int retVal = MediaFileIO::MoveImage(fromFileName, toFileName);
	if (retVal != 0)
	{
		return retVal;
	}

   // I still don't understand this "marked" stuff!
	toLocation.setMarked(false);

//   TRACE_3(errout << "IMAGEFILEIO " << tid << "                   UNLOCK 10 AFTER " << timer.Read() << " msec WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
   return 0;
}
//////////////////////////////////////////////////////////////////////
//  Hack Image Media
//////////////////////////////////////////////////////////////////////

int CImageFileMediaAccess::hack(EMediaHackCommand hackCommand,
                                CMediaLocation &mediaLocation)
{
//	CHRTimer timer;
//	MTIostringstream os; os << "TID " << setw(6) << setfill(' ') << GetCurrentThreadId(); string tid = os.str();
//	TRACE_3(errout << "IMAGEFILEIO " << tid << "                     TRY LOCK 11 WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
//	CAutoThreadLocker atl(imageFileIO->getLock());// Lock thread until return
//	TRACE_3(errout << "IMAGEFILEIO " << tid << "                     WAIT LOCK 11 @ " << timer.Read() << " msec WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");

	int fileHackCommand = 0;

   // Construct image file name
   string imageFileName = makeImageFileName(mediaLocation.getFieldName());

	//// Pushed this down into ImageFileIO
	////// Check if image file exists
	////if (!DoesFileExist(imageFileName))
      ////return CLIP_ERROR_IMAGE_FILE_MISSING;

   // Translate hack command
   switch (hackCommand)
      {
      case MEDIA_HACK_HIDE_ALPHA_MATTE:
			fileHackCommand = FILE_HACK_HIDE_ALPHA_MATTE;
         break;
      case MEDIA_HACK_SHOW_ALPHA_MATTE:
         fileHackCommand = FILE_HACK_SHOW_ALPHA_MATTE;
         break;
      case MEDIA_HACK_DESTROY_ALPHA_MATTE:
         fileHackCommand = FILE_HACK_DESTROY_ALPHA_MATTE;
         break;
      case MEDIA_HACK_CACHE_FRAME:
         fileHackCommand = FILE_HACK_CACHE_FRAME;
         break;
      default:
         return CLIP_ERROR_MEDIA_CANNOT_BE_HACKED_AS_REQUESTED;
      }

	int retVal = MediaFileIO::HackImage(imageFileName, fileHackCommand);
//   TRACE_3(errout << "IMAGEFILEIO " << tid << "                     UNLOCK 11 AFTER " << timer.Read() << " msec WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
	if (retVal != 0)
      {
      theError.set(retVal);   // QQQ is this how you're supposed to use this?
      return CLIP_ERROR_MEDIA_CANNOT_BE_HACKED_AS_REQUESTED;
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////
//  Destroy Image Media
//////////////////////////////////////////////////////////////////////

int CImageFileMediaAccess::destroy(const CMediaLocation &mediaLocation)
{
   string imageFilePath   = mediaLocation.getImageFilePath();
   string historyFilePath = mediaLocation.getHistoryFilePath();

   // Construct image file name
   string imageFileName = makeImageFileName(mediaLocation.getFieldName());

//	CHRTimer timer;
//	MTIostringstream os; os << "TID " << setw(6) << setfill(' ') << GetCurrentThreadId(); string tid = os.str();
//	TRACE_3(errout << "IMAGEFILEIO " << tid << "                       TRY LOCK 12 WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
//	CAutoThreadLocker atl(imageFileIO->getLock());// Lock thread until return
//	TRACE_3(errout << "IMAGEFILEIO " << tid << "                       WAIT LOCK 12 @ " << timer.Read() << " msec WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");

   // Now destroy it
	int retVal = MediaFileIO::DestroyImage(imageFileName);
	if (retVal != 0)
      {
      theError.set(retVal);   // QQQ is this how you're supposed to use this?
      return CLIP_ERROR_MEDIA_CANNOT_BE_DESTROYED;
      }

   if (!historyFilePath.empty()) {

      if (DoesFileExist(historyFilePath)) {

			retVal = MediaFileIO::DestroyImage(historyFilePath);
         if (retVal != 0)
            {
            theError.set(retVal);
            return CLIP_ERROR_HISTORY_CANNOT_BE_DESTROYED;
            }
      }
   }

//   TRACE_3(errout << "IMAGEFILEIO " << tid << "                       UNLOCK 12 AFTER " << timer.Read() << " msec WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");

   return 0;
}

//////////////////////////////////////////////////////////////////////

MTI_UINT32 CImageFileMediaAccess::getAdjustedFieldSize(MTI_UINT32 fieldSize)
{
// xxxmfioxxx
//	MediaFrameFileReader *reader = MediaFileIO::CreateMediaFrameFileReader(fileName);
//	int retVal = reader->FetchFrame();
//	if (retVal != 0)
//	{
//		delete reader;
//		return 0;
//	}
//
//	MTI_UINT32 alignedBufferSize = reader->getAlignedBufferSize();
//
//	// Return the bigger of the two
//	// NB: this is because the aligned buffer size is implemented for
//	//     DPX, but not yet for other file formats.
//	return max(alignedBufferSize, fieldSize);


	return 4096 * ((fieldSize + 4095) / 4096);      // QQQ need to revisit this.
}

//////////////////////////////////////////////////////////////////////

MTI_INT64 CImageFileMediaAccess::adjustToBlocking(MTI_INT64 size,
                                                  bool roundingFlag)
{
   // TBD: Call some function in CImageFileIO to get better padded size
   return size;
}

// ----------------------------------------------------------------------

int CImageFileMediaAccess::Peek(CMediaLocation &mediaLocation,
                                string &filename, MTI_INT64 &offset)
{
   filename = makeImageFileName(mediaLocation.getFieldName());

   offset = 0;

   return 0;
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//
// Function:   queryFreeSpace
//
// Description:Queries the raw disk's free list to determine the
//             total available free space on the disk and also the
//             largest available contiguous free area
//
// Arguments:  MTI_INT64 *totalFreeBytes Ptr to caller's variable to accept
//                                       free size in bytes
//             MTI_INT64 *maxContiguousFreeBytes
//                                     Ptr to caller's variable to accept number of
//                                     bytes in largest contiguous free space
// Returns:    Returns 0 if query succeed.  Non-zero if failed.
//             Returns non-zero if either of totalBlocks or
//             maxContiguousBlocks is NULL pointer
//
//////////////////////////////////////////////////////////////////////
int CImageFileMediaAccess::queryFreeSpace(MTI_INT64 *totalFreeBytes,
                                    MTI_INT64 *maxContiguousFreeBytes)
{
   if (totalFreeBytes == NULL || maxContiguousFreeBytes == NULL)
      return CLIP_ERROR_UNEXPECTED_NULL_ARGUMENT;

   if (fileNamePrefix.empty())
      return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR;

#ifdef _WINDOWS
   ULARGE_INTEGER lFreeBytesAvailableToCaller;	// variable to
                                                // receive free
                                                // bytes on disk
                                                // available to
                                                // the caller
   ULARGE_INTEGER lTotalNumberOfBytes; // variable to receive
   // number of bytes on disk
   ULARGE_INTEGER lTotalNumberOfFreeBytes; // variable to receive
   // free bytes on disk

   string folder = GetFilePath(fileNamePrefix);
   bool bRet = ::GetDiskFreeSpaceEx(folder.c_str(),
                                    &lFreeBytesAvailableToCaller,
                                    &lTotalNumberOfBytes,
                                    &lTotalNumberOfFreeBytes);
   if (bRet == 0) // failure
      return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR;

   *totalFreeBytes = lFreeBytesAvailableToCaller.QuadPart;
#else
   *totalFreeBytes =  0;  // This ain't gonna fly very far
#endif

   // Fudge this
   *maxContiguousFreeBytes = *totalFreeBytes;

   return 0;
}

//////////////////////////////////////////////////////////////////////

bool CImageFileMediaAccess::isClipSpecific() const
{
   return true;
}

//////////////////////////////////////////////////////////////////////
// Image File Space Allocation and Deallocation functions
//////////////////////////////////////////////////////////////////////

int CImageFileMediaAccess::checkSpace(const CMediaFormat& mediaFormat,
                                      MTI_UINT32 frameCount)
{
   return 0;
}

CMediaAllocator*
CImageFileMediaAccess::allocate(const CMediaFormat& mediaFormat,
                                const CTimecode& frame0Timecode,
                                MTI_UINT32 frameCount)
{
   if (mediaFormat.getMediaFormatType() != MEDIA_FORMAT_TYPE_VIDEO3)
      return 0;

	const CImageFormat& imageFormat
                              = static_cast<const CImageFormat&>(mediaFormat);

   MTI_UINT32 fieldSize = imageFormat.getBytesPerField();
   MTI_UINT32 paddedFieldSize = getAdjustedFieldSize(fieldSize);
   EImageFormatType imageType = imageFormat.getImageFormatType();
   bool interlacedFlag = CImageInfo::queryNominalInterlaced(imageType);

//xxxmfioxxx
//	// Stash the frame 0 timecode in the ImageFileIO object
//	if (frame0Timecode != CTimecode::NOT_SET &&
//		 frame0Timecode.getAbsoluteTime() > 0 &&
//		 frame0Timecode.getFramesPerSecond() > 0)
//		{
//		imageFileIO->setFrame0Timecode(frame0Timecode);
//		}
//xxxmfioxxx

   // Create a new media allocator object and return it to caller
	return(new CImageFileMediaAllocator(this, imageFormat, frame0Timecode,
                                       paddedFieldSize, frameCount,
                                       interlacedFlag));
}

CMediaAllocator*
CImageFileMediaAccess::allocateOneFrame(const CMediaFormat& mediaFormat,
                                        const CTimecode& frame0Timecode,
                                        MTI_UINT32 fieldCount)
{
   if (mediaFormat.getMediaFormatType() != MEDIA_FORMAT_TYPE_VIDEO3)
      return 0;

	const CImageFormat& imageFormat
                              = static_cast<const CImageFormat&>(mediaFormat);

   MTI_UINT32 fieldSize = imageFormat.getBytesPerField();
   MTI_UINT32 paddedFieldSize = getAdjustedFieldSize(fieldSize);
   EImageFormatType imageType = imageFormat.getImageFormatType();
   bool interlacedFlag = CImageInfo::queryNominalInterlaced(imageType);

//xxxmfioxxx
//	// Stash the frame 0 timecode in the ImageFileIO object
//	if (frame0Timecode != CTimecode::NOT_SET &&
//		 frame0Timecode.getAbsoluteTime() > 0 &&
//		 frame0Timecode.getFramesPerSecond() > 0)
//		{
//		imageFileIO->setFrame0Timecode(frame0Timecode);
//		}
//xxxmfioxxx

   // Create a new media allocator object and return it to caller
   return(new CImageFileMediaAllocator(this, imageFormat, frame0Timecode,
                                       paddedFieldSize, 1,
                                       interlacedFlag));
}

int CImageFileMediaAccess::cancelAllocation(CMediaAllocator *mediaAllocator)
{
   // Image files are not allocated until they are needed, so this
   // function does not have to deallocate anything.

   return 0;
}

CMediaDeallocator* CImageFileMediaAccess::createDeallocator()
{
   CImageFileMediaDeallocator *deallocator;

   deallocator = new CImageFileMediaDeallocator;

   return deallocator;
}

int CImageFileMediaAccess::deallocate(CMediaDeallocator& deallocator)
{
   // We don't do anything because image files are never deleted;
   // we still override the parent method so we can return "success"
   // instead of an error
   return 0;
}


//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

//string CImageFileMediaAccess::makeImageFileName(const char *fieldName) const
//{
//   return fileNamePrefix + fieldName + fileNameSuffix;
//}

string CImageFileMediaAccess::makeHistoryFileName(const char *fieldName) const
{
   if (historyDirectory.empty()) // legacy or video
      return historyDirectory;
   else                          // compose real .hst file path
      return AddDirSeparator(historyDirectory) + fileName + fieldName + ".hst";
}

//////////////////////////////////////////////////////////////////////
// Image File Name Parsing Functions
//////////////////////////////////////////////////////////////////////

string CImageFileMediaAccess::getImageFileName(
        const CMediaLocation &mediaLocation) const
{
   return string(makeImageFileName(mediaLocation.getFieldName()));
}

string CImageFileMediaAccess::getHistoryFileName(
        const CMediaLocation &mediaLocation) const
{
   return string(makeHistoryFileName(mediaLocation.getFieldName()));
}

string CImageFileMediaAccess::getTemplate() const
{
   // only call this if the media identifier is known to be a template

   string::size_type templateEnd
                             = mediaIdentifier.rfind(frameNumberLeftDelimiter);
   return mediaIdentifier.substr(0, templateEnd);
}

MTI_UINT32 CImageFileMediaAccess::getDefaultDataOffset()
{
//	// This CImageFileMediaAccess must be initialized before this function
//	// can be called.
//	// LOCKING NOT NEEDED HERE
//	MTI_UINT32 retVal = imageFileIO->getMaxDataOffset();
//
////   TRACE_3(errout << "IMAGEFILEIO " << tid << "                         UNLOCK 13 AFTER " << timer.Read() << " msec WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
//	return retVal;

	MTIassert(false);
	return 0;
}

int CImageFileMediaAccess::getFrameNumber() const
{
   // only call this if the media identifier is known to be a template

   // locate the position of the frame number within the media identifier
   // by finding the left and right delimiters
   string::size_type frameNumberStart
                              = mediaIdentifier.rfind(frameNumberLeftDelimiter)
                                + frameNumberLeftDelimiter.size();
   string::size_type frameNumberSize
                            = mediaIdentifier.rfind(frameNumberRightDelimiter)
                              - frameNumberStart;

   MTIistringstream istr;
   istr.str(mediaIdentifier.substr(frameNumberStart, frameNumberSize));

   int frameNumber;
   istr >> frameNumber;


   return frameNumber;
}

string CImageFileMediaAccess::adjustMediaIdentifier(
                                                const string& mediaIdentifier,
                                                const CTimecode& frame0Timecode)
{
   string newMediaIdentifier;

   if (CImageFileMediaAccess::IsTemplate(mediaIdentifier))
      {
      // Fix the digit formatting characters so they can be safely
      // saved in the .clp file
      newMediaIdentifier = CImageFileMediaAccess::MakeTemplate(mediaIdentifier, true);

      // Remove an existing frame number (typically one is not present,
      // but there may be one for clip-to-clip rendering hack)
      newMediaIdentifier = RemoveFrameNumberFromTemplate(newMediaIdentifier);

      // Tack on the frame number in form of ,@[100]
      int frame0 = frame0Timecode.getAbsoluteTime();
      MTIostringstream ostr;
      ostr << frameNumberLeftDelimiter << frame0 << frameNumberRightDelimiter;
      newMediaIdentifier += ostr.str();
      }
   else
      {
      // media identifier is a plain file name, so just pass along
      newMediaIdentifier = mediaIdentifier;
      }

   return newMediaIdentifier;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CImageFileMediaAccess::queryImageFileType(EMediaType mediaType)
{
	MediaFileType fileType;

   switch (mediaType)
      {
      case MEDIA_TYPE_IMAGE_FILE_DPX :
			fileType = MediaFileType::Dpx;
			break;

      case MEDIA_TYPE_IMAGE_FILE_TIFF :
			fileType = MediaFileType::Tiff;
			break;

      case MEDIA_TYPE_IMAGE_FILE_EXR :
			fileType = MediaFileType::Exr;
         break;

      // Media Types that are not image files are invalid, unknown
      // or not implemented yet
		default :
			fileType = MediaFileType::Invalid;
      }

   return fileType;
}

EMediaType CImageFileMediaAccess::queryMediaType(MediaFileType fileType)
{
   EMediaType mediaType;

	if (fileType == MediaFileType::Dpx)
      mediaType = MEDIA_TYPE_IMAGE_FILE_DPX;

	else if (fileType == MediaFileType::Tiff)
		mediaType = MEDIA_TYPE_IMAGE_FILE_TIFF;

	else if (fileType  == MediaFileType::Exr)
      mediaType = MEDIA_TYPE_IMAGE_FILE_EXR;

   else
      {
      // Media Types that are not image files are invalid, unknown
      // or not implemented yet
      mediaType = MEDIA_TYPE_INVALID;
      }

   return mediaType;
}

//////////////////////////////////////////////////////////////////////
// Testing functions
//////////////////////////////////////////////////////////////////////

void CImageFileMediaAccess::dump(MTIostringstream &str)
{
   str << "ImageFileMediaAccess " << std::endl;
   str << " Media Type: " << mediaType;
   str << "  Media Identifier: <" << mediaIdentifier << ">" << std::endl;

}

//////////////////////////////////////////////////////////////////////
// Template and file name functions
//////////////////////////////////////////////////////////////////////

void CImageFileMediaAccess::GenerateFileName (const string &strSampleFileName,
                                     string &strNewFileName, long lFrame)
/*
   This function uses strSampleFileName as a template for creating
   strNewFileName.
*/
{
   if (IsTemplate(strSampleFileName))
      {
      // The input file name is really a template, so use the new
      // function to generate the image file name
      strNewFileName = GenerateFileName2(strSampleFileName, lFrame);
      return;
      }
   else
      {
      // The input file name is a complete file name rather than a
      // template.  Extract a template using the default digit
      // search method
      string templateStr = MakeTemplate(strSampleFileName, false);
      strNewFileName = GenerateFileName2(templateStr, lFrame);
      }

}  /* GenerateFileName */

string CImageFileMediaAccess::GenerateFileName2(const string &fileNameTemplate,
                                       int frameNumber)
{
   // This function takes a image file name template and creates
   // a complete file name with the caller's frame number.
   // The fileNameTemplate argument may be either a complete file name
   // or a template with special formatting characters that indicate
   // the location and count of the frame number digits.

   // Split the file name into two pieces, before and after the
   // the frame number digits
   string fileNamePrefix, fileNameSuffix, fileName;
	int digitCount = SplitFileName(fileNameTemplate,
                                  fileNamePrefix,
                                  fileNameSuffix,
                                  fileName);
   if (digitCount == 0)
      {
      string emptyString;
		return emptyString;
      }

   // build the new file name with the caller's frame number digits
	return fileNamePrefix + MakeImageFileNameDigits(digitCount, frameNumber)
          + fileNameSuffix;
}

int CImageFileMediaAccess::SplitFileName(const string &fileNameTemplate,
                                         string &fileNamePrefix,
                                         string &fileNameSuffix,
                                         string &fileName)
{
   // Splits an image file name or image file name template into two
   // strings, the part of the name before the frame number digits and
   // the part after the frame number digits.  Also returns the
   // number of digits in the frame number.
   // If there is something wrong with the caller's file name, then
   // 0 is returned as the digit count

   int digitIndex, digitCount = 0;

   // Look for the special formatting characters
   FindFormatting(fileNameTemplate, digitIndex, digitCount);

   // If the special formatting characters could not be found,
   // then look for digits that could be the frame number
   if (digitCount == 0)
      FindDigits2(fileNameTemplate, digitIndex, digitCount);

   if (digitCount == 0)
      return digitCount; // ERROR: cannot find formatting or digits

   // Get pieces of the file name before and after the digit positions
   fileNamePrefix = fileNameTemplate.substr(0, digitIndex);
   fileNameSuffix = fileNameTemplate.substr(digitIndex + digitCount);

   // Pull the file name part off the fileNamePrefix
   fileName = fileNamePrefix;

   int j = fileName.length();
   int i = j - 1;
   while ((fileName[i] != '\\')&&(i > 0)) i--;

   fileName = fileName.substr(i+1, j);

   return digitCount;
}

string CImageFileMediaAccess::MakeImageFileNameDigits(int digitCount, int frameNumber)
{
   // Format the caller's frame number into a string with a minimum
   // of digitCount digits, left-filled with zeros.

   char tmpStr[40];
   sprintf(tmpStr, "%.*d", digitCount, frameNumber);

   return string(tmpStr);
}

void CImageFileMediaAccess::FindFormatting(const string &fileName, int &formatStart,
                                  int &formatCount)
{
   // Finds the special formatting characters, # or @, in the image
   // file name template.  Returns in the caller's arguments the
   // location of the first formatting character and the count of
   // formatting characters.  The results are used to locate the
   // the frame number digits in an image file name

   formatStart = 0;
   formatCount = 0;

   // Skip past the path
   string::size_type fileStart = GetFilePath(fileName).size();

   // Find the first instance of any of the formatting characters
   string::size_type startPos = fileName.find_first_of(digitFormattingChars,
                                                       fileStart);

   if (startPos == string::npos)
      return;  // No formatting characters are present in the file name

   formatStart = startPos;

   // Starting at the first formatting character, scan the file name
   // until we find a character that is not a formatting character or
   // we reach the end of the file name
   string::size_type endPos = fileName.find_first_not_of(digitFormattingChars,
                                                         startPos);
   // Calculate the number of formatting characters
   if (endPos == string::npos)
      formatCount = fileName.size() - startPos;   // past the end of string
   else
      formatCount = endPos - startPos;
}

bool CImageFileMediaAccess::IsTemplate(const string &fileName)
{
   // This function returns true if the caller's file name contains
   // the special formatting characters.

   // Skip past the path
   string::size_type fileStart = GetFilePath(fileName).size();

   // Find the first instance of any of the formatting characters
   string::size_type startPos = fileName.find_first_of(digitFormattingChars,
                                                       fileStart);

   return (startPos != string::npos);
}

string CImageFileMediaAccess::RemoveFrameNumberFromTemplate(const string &fileName)
{
   // Remove the frame number from the template
   // Example: input of "C:/imageFile/@@@@.dpx,@[0]"
   //          returns "C:/imageFile/@@@@.dpx"

   // If it is not a template, just return caller's string
   if (!IsTemplate(fileName))
      return fileName;

   string::size_type templateEnd = fileName.rfind(frameNumberLeftDelimiter);
   if (templateEnd == string::npos)
      return fileName;

   return fileName.substr(0, templateEnd);

}

string CImageFileMediaAccess::MakeTemplate(const string &fileName, bool iniSafe)
{
   // Create an image file name template from standard file name or
   // another template.  Allows choice of the special formatting character
   // to be either # or @.  The # character is best for a user-interface
   // but it is incompatible with ini files because it is considered
   // a comment delimiter.  The @ character is compatible with ini
   // files but is not as suggestive in a user-interface.

   // Call with iniSafe true to use @ character instead of # character
   // which is a comment character in ini files

   int digitStart, digitCount;

   FindFormatting(fileName, digitStart, digitCount);

   if (digitCount == 0)
      FindDigits2(fileName, digitStart, digitCount);

   string templateStr;

   if (digitCount == 0)
      return templateStr;  // no digits, return empty string

   char formatChar = (iniSafe) ? '@' : '#';
   templateStr = fileName;
   templateStr.replace(digitStart, digitCount, digitCount, formatChar);

   return templateStr;
}

#ifdef FIND_LONGEST_DIGIT_RUN // Old way
void CImageFileMediaAccess::FindDigits2(const string& inStr, int& digitIndex,
                               int& digitCount)
{
   digitIndex = 0;
   digitCount = 0;

   string::const_iterator strIterator = inStr.begin();
   strIterator += GetFilePath(inStr).size();  // skip past path

   string::const_iterator strEnd = inStr.end();

   int maxDigitCount = 0;
   int newDigitCount;
   string::const_iterator newDigitStart, maxDigitStart;

   // Search for the longest run of digits
   while (strIterator != strEnd)
      {
      // Skip over non-digits
      while (strIterator != strEnd && !isdigit(*strIterator))
         ++strIterator;

      // Count digits
      newDigitStart = strIterator;
      while (strIterator != strEnd && isdigit(*strIterator))
         ++strIterator;
      newDigitCount = strIterator - newDigitStart;

      if (newDigitCount > maxDigitCount)
         {
         maxDigitStart = newDigitStart;
         maxDigitCount = newDigitCount;
         }
      }

   digitCount = maxDigitCount;
   if (maxDigitCount > 0)
      digitIndex = maxDigitStart - inStr.begin();

} // FindDigits2

#else // NEW WAY - find RIGHTMOST digit run

void CImageFileMediaAccess::FindDigits2(const string& inStr, int& digitIndex,
                               int& digitCount)
{
   digitIndex = 0;
   digitCount = 0;

   string::const_iterator strIterator = inStr.begin();
   strIterator += GetFilePath(inStr).size();  // skip past path

   string::const_iterator strEnd = inStr.end();

   int newDigitCount = 0;
   string::const_iterator newDigitStart;

   // Search for the RIGHTMOST run of digits
   while (strIterator != strEnd)
      {
      // Skip over non-digits
      while (strIterator != strEnd && !isdigit(*strIterator))
         ++strIterator;

      if (strIterator != strEnd)
         {
         // Count digits
         newDigitStart = strIterator;
         while (strIterator != strEnd && isdigit(*strIterator))
            ++strIterator;
         newDigitCount = strIterator - newDigitStart;
         }
      }

   digitCount = newDigitCount;
   if (newDigitCount > 0)
      digitIndex = newDigitStart - inStr.begin();

} // FindDigits2

#endif // rightmost vs longest run

int CImageFileMediaAccess::GetDigitCount(const string &fileNameTemplate)
{
   // Determines the number of digits in the frame number given
   // an image file name or an image file name template

   int digitIndex, digitCount = 0;

   // Look for the special formatting characters
   FindFormatting(fileNameTemplate, digitIndex, digitCount);

   // If the special formatting characters could not be found,
   // then look for digits that could be the frame number
   if (digitCount == 0)
      FindDigits2(fileNameTemplate, digitIndex, digitCount);

   return digitCount;
}

void CImageFileMediaAccess::FindAllFiles (const string &strPrototype, long &lNDigit,
                                 long &lFirstFrame, long &lLastFrame)
{

   lNDigit = 0;       // set to 0 in case we fail
   lFirstFrame = INVALID_FRAME_NUMBER;  // set to -1 in case we fail
   lLastFrame = INVALID_FRAME_NUMBER;

   // Find the number of digits in the frame number
   lNDigit = GetDigitCount(strPrototype);

   // Extract a template using the default digit search method
   string templateStr = MakeTemplate(strPrototype, false);

   // Extract the frame number from the caller's file name
   int startFrameNumber = getFrameNumberFromFileName2(templateStr,
                                                      strPrototype);

   if (startFrameNumber == INVALID_FRAME_NUMBER)
      return;  // couldn't extract a starting frame number from file name

   int firstFrame, lastFrame;
   FindAllFiles2(templateStr, startFrameNumber, firstFrame, lastFrame);

   if (firstFrame == INVALID_FRAME_NUMBER || lastFrame == INVALID_FRAME_NUMBER)
      return;  // something wrong, perhaps no files

   lFirstFrame = firstFrame;
   lLastFrame = lastFrame;

}  /* FindAllFiles */

#if 0 // Replaced by FindAllFilesBinarily - same idea, much faster search
////////////////////////////////////////////////////////////////////////////
// This is old M6 code that we hope fixes a problem that Fotokem is seeing
// with NFS file systems, where the frame numbers somehow become negative --
// Our theory is that the Windows function FindFirstFile is failing for them,
// so in that case we'll fall back to running this old code instead of
// behaving stupidly!
////////////////////////////////////////////////////////////////////////////
void CImageFileMediaAccess::FindAllFilesSlowly(const string &imageFileNameTemplate,
                                 int startFrameNumber, int &firstFrame,
                                 int &lastFrame)
{
   // Finds all of the existing files within a continuous range of
   // frame numbers before and after the caller's startFrameNumber.
   // The caller's reference arguments firstFrame and lastFrame are set
   // to the lowest and highest frame numbers that were found in the
   // file names.  If no files are found, either because none exist or
   // if the imageFileNameTemplate is not well-formed, then firstFrame
   // and lastFrame are set to INVALID_FRAME_NUMBER (-1)

   string imageFileName;

   firstFrame = INVALID_FRAME_NUMBER;     // -1 indicates failure on return
   lastFrame = INVALID_FRAME_NUMBER;

   // Look for files before the startFrameNumber
   int frameNumber = startFrameNumber;
   while (frameNumber >= 0)
      {
      imageFileName = GenerateFileName2(imageFileNameTemplate, frameNumber);

      if (DoesFileExist(imageFileName))
         firstFrame = frameNumber;
      else
         break;

      --frameNumber; // subtract one to prepare for next iteration
      }

   // Look for files after the startFrameNumber
   frameNumber = startFrameNumber;
   while (true)
      {
      imageFileName = GenerateFileName2(imageFileNameTemplate, frameNumber);

      if (DoesFileExist(imageFileName))
         lastFrame = frameNumber;
      else
         break;

      ++frameNumber; // add one to prepare for next iteration
      }
}
#endif

void CImageFileMediaAccess::UseBinarySearchToFindAllFiles(bool flag)
{
   useBinarySearchFlag = flag;
}

////////////////////////////////////////////////////////////////////////////
// Recursively finds the first existing frame number in a sequence of frame files.
// Arguments are:
// - the filename template
// - a frame number that either IS the first file (it exists) or comes before
//   the first file (it does not exist).
// - a frame number within the sequence (which must exist if there are any
//   existing frames in the range).
////////////////////////////////////////////////////////////////////////////
int CImageFileMediaAccess::FindFirstFrameOfSequenceThatIncludesThisFrame(const string &fullPathTemplate, int existingFrameNumber)
{
	// If frame 0 exists, by definition it is the low end of this range!
	if (DoesFileExist(GenerateFileName2(fullPathTemplate, 0)))
	{
		return 0;
	}

	// 'Existing frame' must actually exist!
	if (!DoesFileExist(GenerateFileName2(fullPathTemplate, existingFrameNumber)))
	{
		TRACE_0(errout << "INTERNAL ERROR: bad parameter " << existingFrameNumber << " passed to FindFirstFrameOfSequenceThatIncludesThisFrame!");
		return INVALID_FRAME_NUMBER;
	}

	return FindFirstExistingFrameInRange(fullPathTemplate, 0, existingFrameNumber);
}

int CImageFileMediaAccess::FindFirstExistingFrameInRange(const string &fullPathTemplate, int lowFrameNumber, int highFrameNumber)
{
	// Figure out the new interval. If the distance between the high and the low
	// is 1, then we're done and the answer is the high frame.
	auto testFrameNumber = lowFrameNumber + ((highFrameNumber - lowFrameNumber) / 2);
	if (testFrameNumber == lowFrameNumber)
	{
		return highFrameNumber;
	}

	return DoesFileExist(GenerateFileName2(fullPathTemplate, testFrameNumber))
				? FindFirstExistingFrameInRange(fullPathTemplate, lowFrameNumber, testFrameNumber)
				: FindFirstExistingFrameInRange(fullPathTemplate, testFrameNumber, highFrameNumber);
}

////////////////////////////////////////////////////////////////////////////
// Recursively finds the last existing frame number in a sequence of frame files.
// Arguments are:
// - the filename template
// - a frame number within the sequence (which must exist if there are any
//   existing frames in the range).
// - a frame number that either IS the last file (it exists) or comes after
//   the last file (it does not exist).
////////////////////////////////////////////////////////////////////////////
int CImageFileMediaAccess::FindLastFrameOfSequenceThatIncludesThisFrame(const string &fullPathTemplate, int existingFrameNumber)
{
	const int MaxInt = 0x7FFFFFFF;

	// In theory we need to check if frame number MaxInt exists!
	if (DoesFileExist(GenerateFileName2(fullPathTemplate, MaxInt)))
	{
		return MaxInt;
	}

	// 'Existing frame' must actually exist!
	if (!DoesFileExist(GenerateFileName2(fullPathTemplate, existingFrameNumber)))
	{
		TRACE_0(errout << "INTERNAL ERROR: bad parameter " << existingFrameNumber << " passed to FindLastFrameOfSequenceThatIncludesThisFrame!");
		return INVALID_FRAME_NUMBER;
	}

	return FindLastExistingFrameInRange(fullPathTemplate, existingFrameNumber, MaxInt);
}

int CImageFileMediaAccess::FindLastExistingFrameInRange(const string &fullPathTemplate, int lowFrameNumber, int highFrameNumber)
{
	// Figure out the new interval. If the distance between the high and the low
	// is 1, then we're done and the answer is the low frame.
	auto testFrameNumber = highFrameNumber - ((highFrameNumber - lowFrameNumber) / 2);
	if (testFrameNumber == highFrameNumber)
	{
		return lowFrameNumber;
	}

	return DoesFileExist(GenerateFileName2(fullPathTemplate, testFrameNumber))
				? FindLastExistingFrameInRange(fullPathTemplate, testFrameNumber, highFrameNumber)
				: FindLastExistingFrameInRange(fullPathTemplate, lowFrameNumber, testFrameNumber);
}

////////////////////////////////////////////////////////////////////////////
// This is a replacement for FindAllFilesSlowly - uses binary search for
// greatly improved speed at the expense of not handling sequence gaps
// "correctly" - I think gaps are rare!
////////////////////////////////////////////////////////////////////////////
void CImageFileMediaAccess::FindAllFilesBinarily(
											const string &imageFileNameTemplate,
											int startFrameNumber,
											int &firstFrame,
											int &lastFrame)
{
	// Finds all of the existing files within a continuous range of
	// frame numbers before and after the caller's startFrameNumber.
	// The caller's reference arguments firstFrame and lastFrame are set
   // to the lowest and highest frame numbers that were found in the
	// file names.  If no files are found, either because none exist or
   // if the imageFileNameTemplate is not well-formed, then firstFrame
	// and lastFrame are set to INVALID_FRAME_NUMBER (-1)

	// Sanity check the template.
	auto testName = GenerateFileName2(imageFileNameTemplate, startFrameNumber);
	if (testName.empty())
	{
		firstFrame = lastFrame = INVALID_FRAME_NUMBER;
		return;
	}

	CHRTimer hrTimer;

	firstFrame = FindFirstFrameOfSequenceThatIncludesThisFrame(imageFileNameTemplate, startFrameNumber);
	if (firstFrame == INVALID_FRAME_NUMBER)
	{
		lastFrame = INVALID_FRAME_NUMBER;
		return;
	}

	lastFrame = FindLastFrameOfSequenceThatIncludesThisFrame(imageFileNameTemplate, startFrameNumber);

	double elapsed = hrTimer.Read();
	TRACE_3(errout << "OOOOOOOOO FFFB COUNT=" << (lastFrame - firstFrame + 1)
						<< " (" << firstFrame << "->" << lastFrame << ") "
						<< " MSEC=" << elapsed  << " OOOOOOOOOO");
}

namespace {

	struct SFoundFileCache
	{
      int firstFrame;
		int lastFrame;
      bool mightHaveGaps;
		FILETIME lastDirectoryWriteTime;
      string filenameTemplate;
      vector<int> frameList;

      SFoundFileCache()
      {
         Reset();
      };

      void Reset()
      {
         firstFrame = 0x7FFFFFFF;
         lastFrame = -1;
         mightHaveGaps = true;
         lastDirectoryWriteTime.dwLowDateTime = 0;
         lastDirectoryWriteTime.dwHighDateTime = 0;
         filenameTemplate = "";
         frameList.clear();
      };


      FILETIME GetDirectoryLastWriteTime(const string &fileNameTemplate)
      {
         FILETIME retVal;
         retVal.dwLowDateTime = retVal.dwHighDateTime = 0;

         string dirName = RemoveDirSeparator(GetFilePath(fileNameTemplate));

         // Open the directory to get the last write time
         LPCTSTR lpFileName = (LPCTSTR) dirName.c_str();
         DWORD dwDesiredAccess = 0; //GENERIC_READ;
         DWORD dwShareMode = FILE_SHARE_READ | FILE_SHARE_WRITE;
         LPSECURITY_ATTRIBUTES lpSecurityAttributes = NULL;
         DWORD dwCreationDisposition = OPEN_EXISTING;
         DWORD dwFlagsAndAttributes = FILE_FLAG_BACKUP_SEMANTICS;
         HANDLE hTemplateFile = INVALID_HANDLE_VALUE;
         HANDLE hDir = CreateFile(lpFileName, dwDesiredAccess, dwShareMode,
                                  lpSecurityAttributes, dwCreationDisposition,
                                  dwFlagsAndAttributes, hTemplateFile);

         if (hDir == INVALID_HANDLE_VALUE)
         {
            TRACE_0(errout << "ERROR: Can't get last write time for directory "
                           << lpFileName << endl
                           << "        Reason: " << GetSystemMessage(GetLastError()));
         }
         else
         {
            // Successfully opened - get the last write time
            FILETIME lastWriteTime;
            bool success = GetFileTime(hDir, NULL, NULL, &lastWriteTime);
            if (success)
               retVal = lastWriteTime;

            CloseHandle(hDir);
         }

         return retVal;
      };

      bool IsLastWriteTimeNewer(const FILETIME &testFileTime)
      {
         bool retVal = false;
         if (
            testFileTime.dwHighDateTime > lastDirectoryWriteTime.dwHighDateTime ||
            (testFileTime.dwHighDateTime == lastDirectoryWriteTime.dwHighDateTime &&
             testFileTime.dwLowDateTime > lastDirectoryWriteTime.dwLowDateTime)
            )
         {
            retVal = true;
         }

         return retVal;
      };

      bool IsCacheValid(const string &newFilenameTemplate,
                         const FILETIME &newLastWriteTime)
      {
         bool retVal = false;
         if ((newFilenameTemplate == filenameTemplate) &&
             !IsLastWriteTimeNewer(newLastWriteTime))
         {
            retVal = true;
         }

         return retVal;
      };

      void AddFrame(int frameNum)
      {
         frameList.push_back(frameNum);

         if (frameNum < firstFrame)
            firstFrame = frameNum;
         if (frameNum > lastFrame)
            lastFrame = frameNum;
      };

      void DoneAddingFrames()
      {
         sort(frameList.begin(), frameList.end());
      };

      void GetSequenceBounds(int includeFrame,
                             int &outFirstFrame, int &outLastFrame)
      {
         if (firstFrame == 0x7FFFFFFF)
         {
            outFirstFrame = INVALID_FRAME_NUMBER;
            outLastFrame = INVALID_FRAME_NUMBER;
         }
         else if ((includeFrame == INVALID_FRAME_NUMBER) || !mightHaveGaps)
         {
            outFirstFrame = firstFrame;
            outLastFrame = lastFrame;
         }
         else
         {
            // Need to find contiguous sequence that includes includeFrame
            int firstFrameOfContiguousSequence = frameList[0];
            bool foundGap = false;

            for (unsigned int i = 0; i < frameList.size(); ++i)
            {
               if ((i > 0) && (frameList[i] != (frameList[i - 1] + 1)))
               {
                  firstFrameOfContiguousSequence = frameList[i];
                  foundGap = true;
               }
               if (frameList[i] == includeFrame)
               {
                  for (i = i + 1; i < frameList.size(); ++i)
                  {
                     if (frameList[i] != (frameList[i - 1] + 1))
                     {
                        foundGap = true;
                        break;
                     }
                  }
                  outFirstFrame = firstFrameOfContiguousSequence;
                  outLastFrame = frameList[i - 1];
                  if (!foundGap)
                     mightHaveGaps = false;
                  break;
               }
            }
         }
      };

   } gFoundFileCache;
};

void CImageFileMediaAccess::FindAllFiles2(const string &imageFileNameTemplate,
                                 int startFrameNumber, int &firstFrame,
                                 int &lastFrame)
{
	CHRTimer hrTimer;

   // Check user preference for scan technique
   if (useBinarySearchFlag)
	{
		// This will be plenty fast so should be OK synchronous.
      FindAllFiles3(imageFileNameTemplate, startFrameNumber, firstFrame, lastFrame);
      TRACE_3(errout << "OOOOOOOOO FFF3 took " << hrTimer.Read() << " msec OOOOOOOOOO");
      return;
   }

   // Finds all of the existing files within a continuous range of
   // frame numbers before and after the caller's startFrameNumber.
   // The caller's reference arguments firstFrame and lastFrame are set
   // to the lowest and highest frame numbers that were found in the
   // file names.  If no files are found, either because none exist or
   // if the imageFileNameTemplate is not well-formed, then firstFrame
   // and lastFrame are set to INVALID_FRAME_NUMBER (-1)
   // SPECIAL CASE: if startFrameIndex is INVALID_FRAME_NUMBER, ignore gaps
   //                in the sequence, (otherwise get the continous sequence
   //                 that includes startFrameIndex)

   // Initialize return values in case we fail
   firstFrame = INVALID_FRAME_NUMBER;
   lastFrame = INVALID_FRAME_NUMBER;

   // First check the cache
   FILETIME lastDirectoryWriteTime = gFoundFileCache.GetDirectoryLastWriteTime(
                                                    imageFileNameTemplate);
   TRACE_3(errout << "OOOOOOOOO FFF2 DLWT check took " << hrTimer.Read(true) << " msec OOOOOOOOOO");
   bool cacheIsValid = gFoundFileCache.IsCacheValid(imageFileNameTemplate,
                                                    lastDirectoryWriteTime);
   TRACE_3(errout << "OOOOOOOOO FFF2 cache check took " << hrTimer.Read(true) << " msec OOOOOOOOOO");

   if (!cacheIsValid)
   {
      // Cache is invalid - have to do the hard work
      gFoundFileCache.Reset();
      gFoundFileCache.filenameTemplate = imageFileNameTemplate;
      gFoundFileCache.lastDirectoryWriteTime = lastDirectoryWriteTime;

      string searchTemplate = imageFileNameTemplate;
      string filenameOnlyTemplate = GetFileNameWithExt(imageFileNameTemplate);

      string::size_type firstPound = searchTemplate.find('#');
      if (firstPound != string::npos)
      {
         string::size_type lastPound = searchTemplate.rfind('#');
         searchTemplate.replace(firstPound, lastPound + 1 - firstPound, "*");
      }

      // typedef struct _WIN32_FIND_DATA {
      //   DWORD dwFileAttributes;
      //   FILETIME ftCreationTime;
      //   FILETIME ftLastAccessTime;
      //   FILETIME ftLastWriteTime;
      //   DWORD nFileSizeHigh;
      //   DWORD nFileSizeLow;
      //   DWORD dwReserved0;
      //   DWORD dwReserved1;
      //   TCHAR cFileName[MAX_PATH];
      //   TCHAR cAlternateFileName[14];
      // } WIN32_FIND_DATA;

      WIN32_FIND_DATA wfdFindFileData;
      LPCTSTR lpFileName = searchTemplate.c_str();
      HANDLE hFind = ::FindFirstFile(lpFileName, &wfdFindFileData);
      if (hFind == INVALID_HANDLE_VALUE)
      {
         if (::GetLastError() == ERROR_FILE_NOT_FOUND)
         {
            TRACE_1(errout << "$$$$$$$$$$$ FFF2: Not found: " << lpFileName << " $$$$$$$$$$$");
         }
         else
         {
            TRACE_0(errout << "$$$$$$$$$$$ FFF2 FAIL: " << lpFileName
                           << ": " << GetLastSystemErrorMessage());
         }

         // Try the binary search method
         FindAllFilesBinarily(imageFileNameTemplate, startFrameNumber,
                            firstFrame, lastFrame);

         gFoundFileCache.Reset();  // not used in this case
         return;
      }

      int opCount = 0;
      do {
         // We convert the filename back to a template and compare it with
         // the desired template in case there are some similarly named files
         // in the folder that are not part of the sequence

         string foundFileName(wfdFindFileData.cFileName);
         string templateStr = MakeTemplate(foundFileName, false);// not ini-safe

         if (templateStr == filenameOnlyTemplate)
         {
            // We have a match - stash the frame number and update first/last
            int frameNum = getFrameNumberFromFileName2(filenameOnlyTemplate,
                                                       foundFileName);
            gFoundFileCache.AddFrame(frameNum);
         }

         if (opCount == 1 || opCount == 10 || opCount == 100
         || ((opCount%1000) == 0 && opCount > 0))
         {
            double elapsed = hrTimer.Read();
            double average = elapsed/opCount;
            TRACE_3(errout << "OOOOOOOOO FFF2: " << opCount << " scanned in "
                           << elapsed << " msec, avg=" << average
                           << " OOOOOOOOOO");
         }

         ++opCount;
      }
      while (::FindNextFile(hFind, &wfdFindFileData));

      ::FindClose(hFind);

      double elapsed = hrTimer.Read(true);
      double average = elapsed/opCount;
      TRACE_3(errout << "OOOOOOOOO FFF2: " << opCount << " scanned in "
                     << elapsed << " msec, avg=" << average
                     << " OOOOOOOOOO");

      gFoundFileCache.DoneAddingFrames();

      TRACE_3(errout << "OOOOOOOOO FFF2 sort took " << hrTimer.Read(true)
                     << " msec OOOOOOOOOO");
   }

   // OK - cache is now up-to-date.... set the return values from it
   gFoundFileCache.GetSequenceBounds(startFrameNumber, firstFrame, lastFrame);

   TRACE_3(errout << "OOOOOOOOO FFF2 lookup took " << hrTimer.Read(true)
                  << " msec, found " << firstFrame << "->" << lastFrame << " OOOOOOOOOO");
}

namespace {

   struct SFoundFile3Cache
   {
      int firstFrame;
      int lastFrame;
      string filenameTemplate;

      SFoundFile3Cache()
      {
         Reset();
      };

      void Reset()
      {
         firstFrame = INVALID_FRAME_NUMBER;
         lastFrame = INVALID_FRAME_NUMBER;
         filenameTemplate = "";
      };

      void Add(const string &newFilenameTemplate,
               int newFirstFrame,
               int newLastFrame)
      {
         filenameTemplate = newFilenameTemplate;
         firstFrame = newFirstFrame;
         lastFrame = newLastFrame;
      };

      bool Retrieve(const string &desiredFilenameTemplate,
                    int &outFirstFrame,
                    int &outLastFrame)
      {
         bool retVal;

         if (desiredFilenameTemplate == filenameTemplate)
         {
            outFirstFrame = firstFrame;
            outLastFrame = lastFrame;
            retVal = true;
         }
         else
         {
            outFirstFrame = INVALID_FRAME_NUMBER;
            outLastFrame = INVALID_FRAME_NUMBER;
            retVal = false;
         }

         return retVal;
      };

      void Remove(const string &outdatedFilenameTemplate)
      {
         if (outdatedFilenameTemplate == filenameTemplate)
         {
            Reset();
         }
      };

   } gFoundFileCache3;
};

void CImageFileMediaAccess::FindAllFiles3(const string &imageFileNameTemplate,
                                 int startFrameNumber, int &firstFrame,
                                 int &lastFrame)
{
   // Finds the first and last frame numbers for the image files in a folder -
   // very fast, but, because it uses binary search, if there are gaps in the
   // sequence the behavior is undefined, unlike FindAllFiles2, where the
   // behavior is well defined, i.e. you get the first and last files of the
   // contiguous sequence that includes the startFrameNumber.

   // First check the cache
   if (gFoundFileCache3.Retrieve(imageFileNameTemplate, firstFrame, lastFrame))
   {
      // Test validity
      string cachedFirst = GenerateFileName2(imageFileNameTemplate, firstFrame);
      string cachedFirstMinusOne = GenerateFileName2(imageFileNameTemplate, firstFrame-1);
      string cachedLast = GenerateFileName2(imageFileNameTemplate, lastFrame);
      string cachedLastPlusOne = GenerateFileName2(imageFileNameTemplate, lastFrame+1);

      if (DoesFileExist(cachedFirst) && (!DoesFileExist(cachedFirstMinusOne))
      && DoesFileExist(cachedLast) && (!DoesFileExist(cachedLastPlusOne)))
      {
         // All good!
         return;
      }
   }

   // Else need to work a bit harder
   FindAllFilesBinarily(imageFileNameTemplate, startFrameNumber,
                        firstFrame, lastFrame);
   gFoundFileCache3.Add(imageFileNameTemplate, firstFrame, lastFrame);
}

int CImageFileMediaAccess::getFrameNumberFromFileName(const string &fileName)
{
   // Returns the frame number extracted from the caller's fileName
   // argument.
   // Returns -1 if the fileName is poorly-formed.

   // Extract a template using the default digit search method
   string templateStr = MakeTemplate(fileName, false);

   if (templateStr.empty())
      return -1;  // Could find digits

   return getFrameNumberFromFileName2(templateStr, fileName);

}  /* getFrameNumberFromFileName */

int CImageFileMediaAccess::getFrameNumberFromFileName2(const string &fileNameTemplate,
                                              const string &fileName)
{
   // Returns the frame number extracted from the caller's fileName
   // argument.  The caller's template argument is used to indicate
   // where the frame number digits are located within the file name.
   // Returns -1 if either the fileName or template are poorly-formed.

   int digitIndex, digitCount = 0;

   // Look for the special formatting characters
   FindFormatting(fileNameTemplate, digitIndex, digitCount);

   // If the special formatting characters could not be found,
   // then return error indicator
   if (digitCount == 0)
      return -1;

   int frameNumber = 0;
   for (int i = 0; i < digitCount; ++i)
      {
      char digitChar = fileName[digitIndex + i];
      if (!isdigit(digitChar))
         return -1;    // the character is not a digit!

      frameNumber = (frameNumber * 10) + (digitChar - '0');
      }

   return frameNumber;

}  /* getFrameNumberFromFileName2 */

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CImageFileMediaAllocator::
CImageFileMediaAllocator(CImageFileMediaAccess *newMediaAccess,
								 const CImageFormat& imageFormat,
                         const CTimecode& frame0Timecode,
                         MTI_UINT32 newPaddedFieldSize,
                         MTI_UINT32 newFrameCount,
                         bool newInterlacedFlag)
: CMediaAllocator(newMediaAccess, newFrameCount * (newInterlacedFlag? 2 : 1)),
  paddedFieldSize(newPaddedFieldSize),
  interlacedFlag(newInterlacedFlag)
{
   // Media Identifier is the name of the first image file or
   // a specially formatted string that shows where the frame
   // number digits are to be located
   string newMediaIdentifier = newMediaAccess->getMediaIdentifier();
   string imageFileNameTemplate;
   if (CImageFileMediaAccess::IsTemplate(newMediaIdentifier))
      imageFileNameTemplate = newMediaAccess->getTemplate();
   else
      imageFileNameTemplate = newMediaIdentifier;

   frame0FileNumber = frame0Timecode.getAbsoluteTime();

   string tmpStr1, tmpStr2, tmpStr3;
   minFrameDigitCount = CImageFileMediaAccess::SplitFileName(imageFileNameTemplate,
                                                    tmpStr1, tmpStr2, tmpStr3);

   // If the first image file does not exist, assume none of the
   // image files exist and that they will be created later.
   // The "Marked" flag will be set in each MediaLocation returned
   // by getField3MediaAllocation.
   // If the first image file does exist, assume that all of the
   // image files exist.  The "Marked" flag will be cleared in
   // each MediaLocation returned by getFieldMediaAllocation.
	string firstFileName = CImageFileMediaAccess::GenerateFileName2(imageFileNameTemplate,
                                                          frame0FileNumber);
   markFlag = !DoesFileExist(firstFileName);
}

CImageFileMediaAllocator::~CImageFileMediaAllocator()
{

}

//////////////////////////////////////////////////////////////////////


void CImageFileMediaAllocator::
getFieldMediaAllocation(MTI_UINT32 fieldNumber,
                        CMediaLocation& mediaLocation)
{
   if (fieldNumber < fieldCount)
      {
      mediaLocation.setMediaAccess(mediaAccess);

		// Data index is ignored for read & write media.
      mediaLocation.setDataIndex(0);

      mediaLocation.setDataSize(paddedFieldSize);

      // Create the field name for the new Media Location by converting
      // the frame number to a string.  The string has at least
      // minFrameDigitCount digits, with leading zeros to fill out the string
      int fileNumber = frame0FileNumber + fieldNumber;
//      tmpOStrStream.str("");
//      tmpOStrStream << std::setw(minFrameDigitCount) << frameNumber;

      string fieldName =
         CImageFileMediaAccess::MakeImageFileNameDigits(minFrameDigitCount,
                                                        fileNumber);
      mediaLocation.setFieldName(fieldName.c_str());
      mediaLocation.setFieldNumber(fieldNumber);

      mediaLocation.setMarked(markFlag);
      mediaLocation.setImported(!markFlag);
      bool isOddField = ((fieldNumber & 1) == 1);
      mediaLocation.setSecondField(interlacedFlag && isOddField);
      }
   else
      {
      // The field number exceeds the number of fields allocated
      // Set mediaLocation to invalid state
      mediaLocation.setInvalid();
      }

}

void CImageFileMediaAllocator::
getNextFieldMediaAllocation(CMediaLocation& mediaLocation)
{
   getFieldMediaAllocation(currentAllocationIndex, mediaLocation);

   currentAllocationIndex += 1;
}

// ---------------------------------------------------------------------------

int CImageFileMediaAllocator::AssignMediaStorage(C5MediaTrack &mediaTrack)
{
   // for clip 5
   CMediaLocation mediaLocation;
   mediaLocation.setMediaAccess(mediaAccess);
   mediaLocation.setDataIndex(0);
   mediaLocation.setDataSize(paddedFieldSize);    // one field

   string fieldName =
         CImageFileMediaAccess::MakeImageFileNameDigits(minFrameDigitCount,
                                                        frame0FileNumber);
   mediaLocation.setFieldName(fieldName.c_str());
   mediaLocation.setFieldNumber(0);

   // Set marked (not sure what this means, if anything, for Clip 5)
   mediaLocation.setMarked(markFlag);
   mediaLocation.setImported(!markFlag);

   C5ImageFileRange *imageFileRange
            = new C5ImageFileRange(mediaLocation, fieldCount, frame0FileNumber,
                                   minFrameDigitCount);
   mediaTrack.AppendRange(imageFileRange);

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CImageFileMediaDeallocator::CImageFileMediaDeallocator()
{

}

CImageFileMediaDeallocator::~CImageFileMediaDeallocator()
{

}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CImageFileMediaDeallocator::freeMediaAllocation(const CMediaLocation& mediaLocation)
{
   return 0;
}

int CImageFileMediaDeallocator::freeMediaAllocation(const CMediaLocation& startMediaLocation,
                                                    int itemCount)
{
   return 0;
}
