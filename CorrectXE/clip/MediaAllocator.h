// MediaAllocator.h: interface for the CMediaAllocator class.
//
// Abstract base class for subtypes that provide interface
// to particular media storage types (raw, file system, etc.)
//
// Main purpose of this class hierarchy is to assign allocated 
// media storage to individual fields based on rules appropriate
// for the media storage type
//  
/*
$Header: /usr/local/filmroot/clip/include/MediaAllocator.h,v 1.5 2006/03/15 00:29:31 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef MEDIAALLOCATORH
#define MEDIAALLOCATORH

#include "machine.h"
#include "cliplibint.h"
#include "err_clip.h"

//////////////////////////////////////////////////////////////////////
// Forward declarations

class C5MediaTrack;
class CMediaLocation;
class CMediaAccess;

//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CMediaAllocator  
{
public:
   CMediaAllocator(CMediaAccess *newMediaAccess, MTI_UINT32 newFieldCount);
   virtual ~CMediaAllocator();

   int cancelAllocation();

   CMediaAccess* getMediaAccess() const;

   virtual void getFieldMediaAllocation(MTI_UINT32 fieldNumber, 
                                CMediaLocation& mediaLocation) = 0;
   virtual void getNextFieldMediaAllocation(CMediaLocation& mediaLocation)
                                                                         = 0;

   virtual int AssignMediaStorage(C5MediaTrack &mediaTrack)
                                 {return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;};

protected:
   CMediaAccess* mediaAccess;
   MTI_UINT32 fieldCount;         // Number of fields allocated
   int currentAllocationIndex;
};

//////////////////////////////////////////////////////////////////////

#endif // #ifndef MEDIAALLOCATOR_H
