// MediaDeallocator.h: interface for the CMediaDeallocator class.
//
// Abstract base class for all media type-specific deallocation list
// classes
//
/* CVS Info:
$Header: /usr/local/filmroot/clip/include/MediaDeallocator.h,v 1.5 2006/01/11 01:55:49 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef MEDIADEALLOCATORH
#define MEDIADEALLOCATORH

#include "cliplibint.h"
#include <map>
using std::map;

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CMediaAccess;
class CMediaLocation;
class CMediaStorageList;

//////////////////////////////////////////////////////////////////////

class CMediaDeallocator  
{
public:
   CMediaDeallocator();
   virtual ~CMediaDeallocator();

   virtual int freeMediaAllocation(const CMediaLocation& mediaLocation) = 0;
   virtual int freeMediaAllocation(const CMediaLocation& mediaLocation,
                                   int itemCount) = 0;

private:
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CMediaDeallocatorList
{
public:
   CMediaDeallocatorList();
   virtual ~CMediaDeallocatorList();

   void init(const CMediaStorageList& mediaStorageList);
   int deallocate(const CMediaLocation &mediaLocation);
   int deallocate(const CMediaLocation &startMediaLocation, int itemCount);
   int freeMediaStorage();

private:
   void deleteList();

   typedef map<CMediaAccess*, CMediaDeallocator*> MediaDeallocatorMap;
   MediaDeallocatorMap mediaDeallocatorLookup;

   CMediaAccess* lastMediaAccess;
   CMediaDeallocator* lastMediaDeallocator;
};

//////////////////////////////////////////////////////////////////////

#endif // #ifndef MEDIADEALLOCATOR_H
