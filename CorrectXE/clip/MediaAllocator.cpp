// MediaAllocator.cpp: implementation of the CMediaAllocator class.
//
/*
$Header: /usr/local/filmroot/clip/source/MediaAllocator.cpp,v 1.3 2004/05/29 02:42:11 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "MediaAllocator.h"
#include "MediaAccess.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMediaAllocator::
CMediaAllocator(CMediaAccess* newMediaAccess, MTI_UINT32 newFieldCount)
: mediaAccess(newMediaAccess), fieldCount(newFieldCount), 
  currentAllocationIndex(0)                    
{

}

CMediaAllocator::~CMediaAllocator()
{

}

int CMediaAllocator::cancelAllocation()
{
   int retVal;

   // Cancel media allocation
   retVal = mediaAccess->cancelAllocation(this);
   if (retVal != 0)
      return retVal;    // ERROR

   return 0;   // Success
}

CMediaAccess* CMediaAllocator::getMediaAccess() const
{
   return mediaAccess;
}
