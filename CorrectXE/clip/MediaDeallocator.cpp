// MediaDeallocator.cpp: implementation of the CMediaDeallocator class.
//
/* CVS Info:
$Header: /usr/local/filmroot/clip/source/MediaDeallocator.cpp,v 1.5 2006/01/11 01:56:19 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "MediaAccess.h"
#include "MediaInterface.h"
#include "MediaDeallocator.h"
#include "MediaLocation.h"
#include "MediaStorage3.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMediaDeallocator::CMediaDeallocator()
{

}

CMediaDeallocator::~CMediaDeallocator()
{

}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMediaDeallocatorList::CMediaDeallocatorList()
{

}

CMediaDeallocatorList::~CMediaDeallocatorList()
{
   deleteList();
}

void CMediaDeallocatorList::deleteList()
{
   // Iterate through the mediaDeallocatorLookup map and delete each instance
   // of a CMediaDeallocator
   for (MediaDeallocatorMap::iterator iterator = mediaDeallocatorLookup.begin();
        iterator != mediaDeallocatorLookup.end();
        ++iterator)
      {
      CMediaDeallocator *mediaDeallocator = iterator->second;
      delete mediaDeallocator;
      }

   mediaDeallocatorLookup.clear();
}

void CMediaDeallocatorList::init(const CMediaStorageList& mediaStorageList)
{
   // Clean up mediaDeallocatorLookup in case this instance of
   // CMediaDeallocatorList has been used already
   deleteList();

   // Iterate through the media storage list
   int mediaStorageCount = mediaStorageList.getMediaStorageCount();
   for (int i = 0; i < mediaStorageCount; ++i)
      {
      CMediaStorage *mediaStorage = mediaStorageList.getMediaStorage(i);
      CMediaAccess *mediaAccess = mediaStorage->getMediaAccessObj();

      if (mediaAccess)
         {
         // Construct a deallocator for this media access type
         CMediaDeallocator *mediaDeallocator = mediaAccess->createDeallocator();

         // Insert pointers to the media access and media deallocator instances
         // into the lookup map
         mediaDeallocatorLookup.insert(std::make_pair(mediaAccess,
                                                      mediaDeallocator));
         }
      else
         {
            TRACE_0(errout << "INTERNAL ERROR: Found a media storage object with no media access!");
         }
      }

   // Initialize lookup accelerators
   lastMediaDeallocator = 0;
   lastMediaAccess = 0;
}

// ---------------------------------------------------------------------------

int CMediaDeallocatorList::deallocate(const CMediaLocation& mediaLocation)
{
   // Clip 3 Legacy API
// NOTE: The contents  of this function could be replaced with the code
//   retVal = deallocate(mediaLocation, 1);
//   if (retVal != 0)
//      return retVal;
//
//   return 0;


   if (!mediaLocation.isValid())
      return 0;  // Media Location is not valid, nothing to delete
      
   CMediaAccess* mediaAccess = mediaLocation.getMediaAccess();
   
   if (mediaAccess != lastMediaAccess)
      {
      // Lookup media deallocator in lookup table that corresponds
      // to the media access instance pointer
      lastMediaDeallocator = (mediaDeallocatorLookup.find(mediaAccess))->second;

      // Remember for next time
      lastMediaAccess = mediaAccess;
      }

   int retVal = lastMediaDeallocator->freeMediaAllocation(mediaLocation);
   if (retVal != 0)
      return retVal; // ERROR

   return 0;
}

int CMediaDeallocatorList::deallocate(const CMediaLocation& startMediaLocation,
                                      int itemCount)
{
   if (!startMediaLocation.isValid())
      return 0;  // Media Location is not valid, nothing to delete
      
   CMediaAccess* mediaAccess = startMediaLocation.getMediaAccess();
   
   if (mediaAccess != lastMediaAccess)
      {
      // Lookup media deallocator in lookup table that corresponds
      // to the media access instance pointer
      lastMediaDeallocator = (mediaDeallocatorLookup.find(mediaAccess))->second;

      // Remember for next time
      lastMediaAccess = mediaAccess;
      }

   int retVal = lastMediaDeallocator->freeMediaAllocation(startMediaLocation,
                                                          itemCount);
   if (retVal != 0)
      return retVal; // ERROR

   return 0;
}

// ---------------------------------------------------------------------------

int CMediaDeallocatorList::freeMediaStorage()
{
   // Do whatever to delete all the media storage for each
   // of the mediaAccess and mediaDeallocator instances
   MediaDeallocatorMap::iterator iterator;
   for (iterator = mediaDeallocatorLookup.begin();
        iterator != mediaDeallocatorLookup.end();
        ++iterator)
      {
      CMediaAccess *mediaAccess = iterator->first;
      CMediaDeallocator *mediaDeallocator = iterator->second;
      int retVal = mediaAccess->deallocate(*mediaDeallocator);
      if (retVal != 0)
         return retVal; // ERROR
      }

   return 0;
}

