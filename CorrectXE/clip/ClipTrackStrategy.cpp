#include "ClipTrackStrategy.h"
#include "ClipAPI.h"

CClipTrackStrategy::CClipTrackStrategy(ClipSharedPtr &pClip, int iVideoSelect,
                                       int iAudioSelect, int iFramingSelect)
  : mpClip(pClip), mVideoSelect(iVideoSelect), mAudioSelect(iAudioSelect),
    mFramingSelect(iFramingSelect)
{
}

/**
 * I believe the naive implementation of simply returning the argument
 * iFrame would work in most circumstances.  However, the
 * playlist/sequence implementation also handles the case that
 * playSpeed != 1.  Since this concrete strategy attempts to replace
 * the degenerate case of a single clip/sequence within a playlist,
 * perhaps it has to handle the playSpeed != 1 case specially also.
 * On the other hand, since the single clip case should only be in use
 * outside of Control Dailies, perhaps the playSpeed != 1 / single
 * clip/sequence case is never encountered anywhere.
 */
int CClipTrackStrategy::getAudioClipFrame(int iFrame)
{
  // validate mpClip and iFrame
  CAudioTrack *audioTrack = mpClip->getAudioTrack(mAudioSelect);
  CAudioFrameList *audioFrameList = 0;
  int totalFrameCount = 0;
  if (audioTrack != 0)
  {
    audioFrameList = audioTrack->getAudioFrameList(mAudioSelect);
    if (audioFrameList != 0)
    {
      totalFrameCount = audioFrameList->getTotalFrameCount();
    }
  }

  if (iFrame < 0 || iFrame >= totalFrameCount)
  {
    return NULL_TRACK_STRATEGY_VALUE;
  }
  // end validate mpClip and iFrame

  return iFrame;
}


int CClipTrackStrategy::getAudioFrameCount()
{
  CAudioTrack *audioTrack = mpClip->getAudioTrack(mAudioSelect);
  CAudioFrameList *audioFrameList = 0;
  int totalFrameCount = 0;
  if (audioTrack != 0)
  {
    audioFrameList = audioTrack->getAudioFrameList(mAudioSelect);
    if (audioFrameList != 0)
    {
      totalFrameCount = audioFrameList->getTotalFrameCount();
    }
  }

  return totalFrameCount;
}


bool CClipTrackStrategy::getAudioLtc(float fFrameArg, bool bRound, int iOffset,
                                     string &strAudioLtc)
{
  strAudioLtc = "Not available";
  return false;
}


int CClipTrackStrategy::getHandleCount()
{
  CVideoFrameList *videoFrameList =
    mpClip->getVideoFrameList(mVideoSelect, mFramingSelect);

  // validate mpClip
  if (videoFrameList == 0)
  {
    throw std::logic_error(""); // bug outside CClipTrackStrategy
  }
  // end validate mpClip

  return videoFrameList->getInFrameIndex();
}


bool CClipTrackStrategy::getTelecineKeykode(
  int iFrame, EKeykodeFormat iFormat, int iPerfOffset, string &strKeykode,
  EKeykodeFilmDimension *ipFilmDimension)
{
  strKeykode = "Not available";
  if (ipFilmDimension)
  {
    *ipFilmDimension = KEYKODE_FILM_DIMENSION_INVALID;
  }

  return false;
}


void CClipTrackStrategy::getVideoClipFrame(int iFrame, int &iClipFrame,
                                           int &iClipFramePartial)
{
  iClipFrame = iFrame;
  iClipFramePartial = 0;

  // validate mpClip and iFrame
  CVideoFrameList *videoFrameList =
    mpClip->getVideoFrameList(mVideoSelect, mFramingSelect);
  int totalFrameCount = 0;
  if (videoFrameList != 0)
  {
    totalFrameCount = videoFrameList->getTotalFrameCount();
  }

  if (videoFrameList == 0)
  {
    throw std::logic_error(""); // bug outside CClipTrackStrategy
  }

  if (iFrame < 0 || iFrame >= totalFrameCount)
  {
    iClipFrame = NULL_TRACK_STRATEGY_VALUE;
  }
  // end validate mpClip and iFrame
}


int CClipTrackStrategy::getVideoFrameCount()
{
  CVideoFrameList *videoFrameList =
    mpClip->getVideoFrameList(mVideoSelect, mFramingSelect);
  int totalFrameCount = 0;
  if (videoFrameList != 0)
  {
    totalFrameCount = videoFrameList->getTotalFrameCount();
  }

  // validate mpClip
  if (videoFrameList == 0)
  {
    throw std::logic_error(""); // bug outside CClipTrackStrategy
  }
  // end validate mpClip

  return totalFrameCount;
}

void CClipTrackStrategy::getTelecineTC(int iFrame, string &strTC)
{
  strTC = "Not available";

  return;
}

