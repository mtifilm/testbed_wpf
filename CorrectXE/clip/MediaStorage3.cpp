// MediaStorage.cpp: implementation of the CMediaStorage class.
//
//////////////////////////////////////////////////////////////////////

#include "MediaStorage3.h"

#include "Clip3.h"
#include "err_clip.h"
#include "HistoryLocation.h"
#include "IniFile.h"
#include "MediaAccess.h"
#include "MediaFormat.h"
#include "MTIstringstream.h"

//////////////////////////////////////////////////////////////////////
// Static Data

string CMediaStorageList::mediaStorageSectionName   = "MediaStorage";
string CMediaStorageList::mediaStorageKey           = "MediaStorage_";
//** Clip2clip hack
string CMediaStorageList::dirtyMediaStorageIndexKey = "DirtyMediaStorageIndex";
//**//

////****WARNING****WARNING******WARNING*********
//// THESE ARE DUPLICATED IN Clip3.cpp !!!!
//// and now historySectionName and enableAutoCleanKey are also defined in
//// NavSystemInterface.cpp!!!!!!   Woo hoo!
//// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
static string metaDataFileName     = "MTIMetadata.ini";
//string CMediaStorageList::historyTypeKey       = "HistoryType";
////string CMediaStorageList::enableAutoCleanKey   = "EnableAutoClean";
//
//string CMediaStorageList::metaDataSectionName   = "Metadata";
//string CMediaStorageList::metaDataDirectoryKey  = "MetadataRootDirectory";
//string CMediaStorageList::iniFileInMetadataDir  = "IniFileInMetadataDir";
//
//string CMediaStorageList::historySectionName   = "History";
//string CMediaStorageList::historyDirectoryKey  = "HistoryDirectory";
//
//string CMediaStorageList::historyTypeFilePerClip  = "FILE_PER_CLIP";
//string CMediaStorageList::historyTypeFilePerFrame = "FILE_PER_FRAME";
////  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//// THESE ARE DUPLICATED IN Clip3.cpp !!!!
////****WARNING****WARNING******WARNING*********

string CMediaStorageList::historyStorageSectionName = "History";
string CMediaStorageList::historyStorageKey = "HistoryDirectory";

#ifdef PRIVATE_STATIC
//////////////////////////////////////////////////////////////////////
// The following variables used to be static class members.
// I switched the to namespaced globals for compatability with
// visual c++.  -Aaron Geman  7/9/04
//////////////////////////////////////////////////////////////////////

namespace MediaStorage3Data {
   string mediaStorageSectionName = "MediaStorage";
   string mediaStorageKey = "MediaStorage_";
   //** Clip2clip hack
   string dirtyMediaStorageIndex = "DirtyMediaStorageIndex";
   //**//
};
using namespace MediaStorage3Data;
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMediaStorage::CMediaStorage()
: mediaType(MEDIA_TYPE_INVALID), mediaIdentifier(""),
  mediaAccessObj(0)
{

}

CMediaStorage::CMediaStorage(EMediaType newMediaType,
                             const string& newMediaIdentifier,
                             const string& newHistoryDirectory)
 : mediaType(newMediaType), mediaIdentifier(newMediaIdentifier),
   historyDirectory(newHistoryDirectory),
   mediaAccessObj(0)
{

}

CMediaStorage::~CMediaStorage()
{

}

//////////////////////////////////////////////////////////////////////
// Accessor Member Functions
//////////////////////////////////////////////////////////////////////

CMediaAccess* CMediaStorage::getMediaAccessObj() const
{
   return mediaAccessObj;
}

const string& CMediaStorage::getMediaIdentifier() const
{
   return mediaIdentifier;
}

const string& CMediaStorage::getHistoryDirectory() const
{
   return historyDirectory;
}

EMediaType CMediaStorage::getMediaType() const
{
   return mediaType;
}

//////////////////////////////////////////////////////////////////////
// Mutator Member Functions
//////////////////////////////////////////////////////////////////////

void CMediaStorage::setMediaAccessObj(CMediaAccess* newMediaAccessObj)
{
   mediaAccessObj = newMediaAccessObj;
}

void CMediaStorage::setMediaIdentifier(const string& newMediaIdentifier)
{
   mediaIdentifier = newMediaIdentifier;
}

void CMediaStorage::setMediaType(EMediaType newMediaType)
{
   mediaType = newMediaType;
}

//////////////////////////////////////////////////////////////////////

void CMediaStorage::dump(MTIostringstream &str) const
{
   str << "mediaType: ";
   str << CMediaInterface::queryMediaTypeName(mediaType);
   str << " (" << mediaType << ") ";

	std::ostringstream os;
	os << mediaAccessObj;
   str << "mediaId: <" << mediaIdentifier << "> "
		 << "mediaAccess: " << os.str() << std::endl;
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

CMediaStorageList::CMediaStorageList()
: dirtyMediaStorageIndex(-1)
{
}

CMediaStorageList::~CMediaStorageList()
{
   unregisterMediaStorage();

   clear();

   // CODEGUARD says the stuff in the mediastoragelist is leaking
   // NOTE!! Let it leak - something seems to be holding on to a copy and
   // crashes if you delete it! Anyhow they are relatively small.
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

CMediaStorage* CMediaStorageList::getMediaStorage(int mediaStorageIndex) const
{
   if (mediaStorageIndex < 0 || mediaStorageIndex >= (int)mediaStorageList.size())
   {
      return 0;
   }

   return mediaStorageList[mediaStorageIndex];
}

CMediaStorage* CMediaStorageList::
getMediaStorage(const CMediaAccess *mediaAccess) const
{
   int index = getMediaStorageIndex(mediaAccess);
   if (index < 0)
      {
      // ERROR: Could not find the CMediaAccess pointer in the
      //        Media Storage list
      return 0;
      }

   return mediaStorageList[index];
}

int CMediaStorageList::
getMediaStorageIndex(const CMediaAccess *mediaAccess) const
{
   // Perform linear search of Media Storage List to find entry that
   // references the CMediaAccess object in the argument list.
   // Returns index into the Media Storage List for matching entry, or
   // returns -1 if no entry was found
   for (unsigned int index = 0; index < mediaStorageList.size(); index++)
      {
      if (mediaStorageList[index]->getMediaAccessObj() == mediaAccess)
         {
         // Success, found the entry with matching media access pointer
         return (int)index;
         }
      }

   // The CMediaAccess pointer was not found in the Media Storage List,
   // so return -1 to indicate this.
   return -1;
}

int CMediaStorageList::getMediaStorageCount() const
{
   return mediaStorageList.size();
}

//////////////////////////////////////////////////////////////////////
//** Clip2clip hack
//////////////////////////////////////////////////////////////////////

int CMediaStorageList::getDirtyMediaStorageIndex() const
{
   return dirtyMediaStorageIndex;
}

void CMediaStorageList::setDirtyMediaStorageIndex(int newIndex)
{
   dirtyMediaStorageIndex = newIndex;
}

void CMediaStorageList::clear()
{
   mediaStorageList.clear();
   dirtyMediaStorageIndex = -1;
}


//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

void CMediaStorageList::addMediaStorage(EMediaType mediaType,
                            const string &mediaIdentifier,
                            const string &historyDirectory,
                            CMediaAccess* mediaAccess)
{
   // Search current Media Storage List to see if the Media Storage
   // entry with the same CMediaAccess pointer already exists
   if (mediaAccess != 0 && getMediaStorageIndex(mediaAccess) >= 0)
      {
      // Entry is already present in the table, so don't add a new one
      return;
      }

   // Construct a mediaStorage instance and set mediaType, mediaIdentifier
   // and mediaAccess
   CMediaStorage* mediaStorage = new CMediaStorage(mediaType,
                                                   mediaIdentifier,
                                                   historyDirectory);
   mediaStorage->setMediaAccessObj(mediaAccess);

   // Add the new mediaStorage instance to the clip's media storage list
   mediaStorageList.push_back(mediaStorage);
}


//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

void CMediaStorageList::replaceMediaStorage(int mediaStorageIndex,
                                            EMediaType mediaType,
                                            const string &mediaIdentifier,
                                            const string &historyDirectory,
                                            CMediaFormat& mediaFormat)
{
   CMediaInterface mediaInterface;     // singleton class for accessing media
                                       // storage management

   // Unregister the MediaAccess of the MediaStorage that we're replacing.
   CMediaStorage* mediaStorage = getMediaStorage(mediaStorageIndex);
   CMediaAccess *mediaAccess = mediaStorage->getMediaAccessObj();
   mediaInterface.unregisterMediaAccess(mediaAccess);

   // Delete the old media storage.
   // NO!! Let it leak - something is holding on to a copy and crashes if you delete it!
   //delete mediaStorageList[mediaStorageIndex];
   mediaStorageList[mediaStorageIndex] = nullptr;

   // Construct the replacement mediaStorage instance.
   CMediaStorage* newMediaStorage = new CMediaStorage(mediaType,
                                                   mediaIdentifier,
                                                   historyDirectory);
   mediaStorageList[mediaStorageIndex] = newMediaStorage;
   registerOneMediaStorage(mediaStorageIndex, mediaFormat);
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CMediaStorageList::readMediaStorageSection(CIniFile *mediaStorageIniFile,
                                              const string& sectionPrefix)
{
   int retVal;

   if (mediaStorageIniFile == 0)
   {
      // CIniFile is not available
      return -1;
   }

   // n case we are refreshing, clear out the old stuff!
   // I think we need to let the media storages leak here... QQQ
   unregisterMediaStorage();
   clear();

   // Compose section name, e.g., "VideoProxy0\MediaStorage"
   string mediaSectionName = sectionPrefix + "\\" + mediaStorageSectionName;

   string hstSectionName = sectionPrefix + "\\" + historyStorageSectionName;

   if (!mediaStorageIniFile->SectionExists(mediaSectionName))
      return CLIP_ERROR_CLIP_FILE_MEDIA_STORAGE_MISSING;

   string mediaKey;
   string value, defaultValue;
   string hstkey;
   string hstValue, hstDefaultValue;
   string motKey;
   string motValue, motDefaultValue;
   string debKey;
   string debValue, debDefaultValue;

   EMediaType newMediaType;
   string newMediaIdentifier;
   string newHistoryDirectory;

   int loopCount = 0;
   // Loop while reading keys of the form MediaStorage_#, where #
   // is the loop count. Loop ends when the desired media storage
   // key is missing
   //
   while(true)
   {
      mediaKey  = getMediaStorageKey(loopCount);

      // If this key does not exist, then assume we are done
      if (!mediaStorageIniFile->KeyExists(mediaSectionName, mediaKey))
         break;

      // Read something like "MediaStorage_2=1, rd1" from iniFile
      value = mediaStorageIniFile->ReadString(mediaSectionName, mediaKey, defaultValue);

      // Parse the media storage value, consisting of an integer for
      // the Media Type and a string for the Media Identifier
      retVal = parseMediaStorage(value, &newMediaType, newMediaIdentifier);
      if (retVal != 0)
      {
         // ERROR: Could not parse the Media Storage specification
         return retVal;
      }

      if (CMediaInterface::isMediaTypeImageFile(newMediaType))
      {
         // PER DJM instruction:
         //   If there is no MTIMetadata.ini, go and create it
         //   using the info from MTILocalMachine.ini
         string imageFileDir = GetFilePath(newMediaIdentifier);

         // Make sure we can access the media folder!
         if (DoesDirectoryExist(imageFileDir))
         {
            HistoryLocation historyLocation;
            newHistoryDirectory = historyLocation.computeHistoryLocation(imageFileDir);
         }

//         //****WARNING****WARNING******WARNING************
//         // THE CODE BELOW IS SIMILAR TO CODE IN Clip3.cpp !!!!
//         // And also to code in writeMediaStorageSection()!!!!!!
//         // Would be nice to refactor this stuff
//         // vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
//
//            /////////////////////////////////////////////////////////////////////////////
//            //
//            // The history dir for the clip is made by taking the dir part of the
//            // imageFileNameTemplate and appending it (without its disk preface) to
//            // the main history path specified in MTILocalMachine.ini
//            //
//            // This will be replaced by an edit box to be filled-in by the user in
//            // the clip-creation process
//            //
//            bool historyTypeIsFilePerFrame = false;
////            bool enableAutoClean = false;
//            bool putMetadataIniInMetadataDir = false;
//            string metaDataDirectory;
//            string historyDirectory;
//            string metaDataIniFileName;
//
//            CIniFile *localMachineIniFile = CPMPOpenLocalMachineIniFile();
//            if (localMachineIniFile != NULL)
//            {
//               localMachineIniFile->ReadOnly(true);
//
//               ////////////////// in MTILocalMachine.ini
//               //
//               // [History]
//               // HistoryType=FILE_PER_FRAME     (default, or FILE_PER_CLIP)
//               // EnableAutoClean=false          (default [DEFUNCT])
//               //
//               // [Metadata]
//               // MetadataRootDirectory=$(CPMP_SHARED_DIR)MTIMetadata    (default is to use image folder)
//               //
//               //////////////////
//
////               enableAutoClean = localMachineIniFile->ReadBool(historySectionName,
////                                                          enableAutoCleanKey,
////                                                          enableAutoClean);
//
//               metaDataDirectory = localMachineIniFile->ReadString(metaDataSectionName,
//                                                          metaDataDirectoryKey,
//                                                          metaDataDirectory);
//               ExpandEnviornmentString(metaDataDirectory);
//               ConformFileName(metaDataDirectory);
//
//               putMetadataIniInMetadataDir = localMachineIniFile->ReadBool(
//                                                          metaDataSectionName,
//                                                          iniFileInMetadataDir,
//                                                          putMetadataIniInMetadataDir);
//
//               DeleteIniFile(localMachineIniFile);
//            }
//
//            // Figure out where the metadata folders should go, either in
//            // the image file foilder itself or in a subfolder of folder
//            // specified in the MTIlocalMachine.ini.
//            if (metaDataDirectory.empty())
//            {
//               metaDataDirectory = imageFileDir;
//            }
//            else
//            {
//               // get directory portion of media identifier
//               string imageDirNoDriveOrLeadSlash = imageFileDir;
//               if ((((imageFileDir[0]>='a')&&(imageFileDir[0]<='z'))||
//                    ((imageFileDir[0]>='A')&&(imageFileDir[0]<='Z')))&&
//                   (imageFileDir[1]==':')&&
//                   (imageFileDir[2]=='\\'))
//               {
//                  imageDirNoDriveOrLeadSlash = imageFileDir.substr(3);
//               }
//
//               metaDataDirectory = AddDirSeparator(metaDataDirectory) + imageDirNoDriveOrLeadSlash;
//            }
//
//            // try to get directories out of MTIMetadata.ini -
//            // look for MTIMetadata.ini in both the metadata root directory and the
//            // image file directory
//
//            metaDataIniFileName = AddDirSeparator(metaDataDirectory) + metaDataFileName;
//
//            bool foundIniInMetaDataDir = DoesFileExist(metaDataIniFileName);
//            bool foundIniInImageFileDir = DoesFileExist(AddDirSeparator(imageFileDir) + metaDataFileName);
//
//            if (!foundIniInMetaDataDir
//            && (foundIniInImageFileDir || !putMetadataIniInMetadataDir))
//            {
//               metaDataIniFileName = AddDirSeparator(imageFileDir) + metaDataFileName;
//            }
//
//            if (DoesFileExist(metaDataIniFileName))
//            {
//               CIniFile *metaDataIniFile = CreateIniFile(metaDataIniFileName, true);  // readonly
//               if (metaDataIniFile != NULL)
//               {
//                  string temp = metaDataIniFile->ReadString(historyStorageSectionName, historyStorageKey, "");
//                  if (!temp.empty())
//                  {
//                     historyTypeIsFilePerFrame = true;
//                  }
//
//                  DeleteIniFile(metaDataIniFile);
//               }
//            }
//         // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//         // THE ABOVE CODE IS SIMILAR TO CODE IN Clip3.cpp !!!!
//         // ****WARNING****WARNING******WARNING******WARNING******
//
//            // now read History dir info from the MTIMetadata.ini
//            // in the MediaStorage directory.
//
//            // Make damned sure to not create the ini file here if it's missing
//            if (DoesFileExist(metaDataIniFileName))
//            {
//               CIniFile *metaDataIniFile = CreateIniFile(metaDataIniFileName, true);  // readonly
//               if (metaDataIniFile != NULL) {
//
//                  newHistoryDirectory = metaDataIniFile->ReadString(historyStorageSectionName, historyStorageKey, newHistoryDirectory);
//
//                  DeleteIniFile(metaDataIniFile);
//               }
//            }
//         }
      }

      // Add a new CMediaStorage instance to the Media Storage list
      // Note that we may register an empty history directory, in which
      // case SaveRestore.cpp will assume that we have a legacy clip
      // with MONOLITHIC history
      addMediaStorage(newMediaType,
                      newMediaIdentifier,
                      newHistoryDirectory,
                      0);

      // Prepare for next pass thru loop
      ++loopCount;
   }

   //** Clip2clip hack - read the Dirty Media Storage Index
   dirtyMediaStorageIndex = mediaStorageIniFile->ReadInteger(mediaSectionName,
                                                  dirtyMediaStorageIndexKey,
                                                  0);
   if (dirtyMediaStorageIndex >= loopCount)
      dirtyMediaStorageIndex = 0;    // Sanity
   //**//

   return 0;
}

EMediaType CMediaStorageList::readMediaStorageType(CIniFile *iniFile,
                                                   const string& sectionPrefix)
{
   int retVal;

   // Compose section name, e.g., "VideoProxy0\MediaStorage"
   string mediaSectionName = sectionPrefix + "\\" + mediaStorageSectionName;

   if (!iniFile->SectionExists(mediaSectionName))
      {
      theError.set(CLIP_ERROR_CLIP_FILE_MEDIA_STORAGE_MISSING);
      return MEDIA_TYPE_INVALID;
      }

   string mediaKey;
   string value, defaultValue;
   EMediaType newMediaType;
   string newMediaIdentifier;

   mediaKey = getMediaStorageKey(0);   // Get key

   // If this key does not exist, then assume we are done
   if (!iniFile->KeyExists(mediaSectionName, mediaKey))
      {
      theError.set(CLIP_ERROR_CLIP_FILE_MEDIA_STORAGE_MISSING);
      return MEDIA_TYPE_INVALID;
      }

   // Read something like "MediaStorage_0=1, rd1" from iniFile
   value = iniFile->ReadString(mediaSectionName, mediaKey, defaultValue);

   // Parse the media storage value, consisting of an integer for
   // the Media Type and a string for the Media Identifier
   retVal = parseMediaStorage(value, &newMediaType, newMediaIdentifier);
   if (retVal != 0)
      {
      // ERROR: Could not parse the Media Storage specification
      theError.set(retVal);
      return MEDIA_TYPE_INVALID;
      }

   return newMediaType;
}

int CMediaStorageList::writeMediaStorageSection(CIniFile *clipIniFile,
                                              const string& sectionPrefix,
                                              bool updateMetadataFile)
{
   if (clipIniFile == 0)
   {
      // CIniFile is not available
      return -1;
   }

   // Compose section name, e.g., "VideoProxy0\MediaStorage"
   string mediaSectionName = sectionPrefix + "\\" + mediaStorageSectionName;
   string mediaKey;
   string emptyString;
   MTIostringstream ostr;

   int mediaStorageCount = getMediaStorageCount();
   // For each entry in Media Storage list...
   for (int i = 0; i < mediaStorageCount; ++i)
   {
      mediaKey    = getMediaStorageKey(i);    // Get keys

      // Compose value
      CMediaStorage *mediaStorage = getMediaStorage(i);
      string localMediaIdentifier = mediaStorage->getMediaIdentifier();
      EMediaType localMediaType = mediaStorage->getMediaType();
      string localHistoryDirectory = mediaStorage->getHistoryDirectory();

      ostr.str(emptyString);
      ostr <<  localMediaType << ", " << localMediaIdentifier;

      // Write something like "MediaStorage_2=1, rd1" to clipIniFile
      clipIniFile->WriteString(mediaSectionName, mediaKey, ostr.str());

      // now write History dir info into the MTIMetadata.ini
      // in the MediaStorage directory
      string imageFileDir = GetFilePath(localMediaIdentifier);

      // I think this code is needed ONLY to set up the metadata for version
      // clips. Don't quote me on that, though

      // Sanity - screen out non-image-file clips and clips where we can't
      // find the media folder
      // **** THIS MAKES NO SENSE TO ME ***** QQQ *****
      if (!imageFileDir.empty() && DoesDirectoryExist(imageFileDir) && updateMetadataFile)
      {
         HistoryLocation historyLocation;
         auto mediaHistoryLocation = historyLocation.computeHistoryLocation(imageFileDir);
         if (localHistoryDirectory != mediaHistoryLocation)
         {
            auto metadataIniFilePath = historyLocation.computeMetadataIniFilePath(imageFileDir);
				historyLocation.updateHistoryLocationIfNecessary(metadataIniFilePath, localHistoryDirectory);
         }
      }

//         // Don't look at the global - look at whether or not a history
//         // directory was passed in to determine if we are in file-per-frame
//         // history mode
//         bool historyTypeIsFilePerFrame = !localHistoryDirectory.empty();
////         bool enableAutoClean = false;
//         bool putMetadataIniInMetadataDir = false;
//         string metaDataDirectory;
//         string metaDataIniFileName;
//
//         // Gather global info
//         CIniFile *localMachineIniFile = CPMPOpenLocalMachineIniFile();
//         if (localMachineIniFile != NULL)
//         {
////            string historyType = localMachineIniFile->ReadString(historySectionName, historyTypeKey, CClipInitInfo::historyTypeFilePerFrame);
////            bool defaultHistoryTypeIsFilePerFrame = historyType == historyTypeFilePerFrame;
//
////            enableAutoClean = localMachineIniFile->ReadBool(historySectionName,
////                                                                 enableAutoCleanKey,
////                                                                 enableAutoClean);
//
//            metaDataDirectory = localMachineIniFile->ReadString(metaDataSectionName,
//                                                                metaDataDirectoryKey,
//                                                                metaDataDirectory);
//            ExpandEnviornmentString(metaDataDirectory);
//            ConformFileName(metaDataDirectory);
//
//            putMetadataIniInMetadataDir = localMachineIniFile->ReadBool(
//                                                                metaDataSectionName,
//                                                                iniFileInMetadataDir,
//                                                                false);
//
//            DeleteIniFile(localMachineIniFile);
//         }
//
//         // Only create the metadata ini file IF we're in
//         // history-file-per-frame mode AND
//         // (1) this clip wants history file per frame OR
//         // [DEFUNCT] (2) enableAutoClean is TRUE
//         if (historyTypeIsFilePerFrame
//         && (!localHistoryDirectory.empty() /*|| enableAutoClean*/))
//         {
//            if (metaDataDirectory.empty())
//            {
//               metaDataDirectory = imageFileDir;
//            }
//            else
//            {
//               // get directory portion of media identifier
//               string imageDirNoDriveOrLeadSlash;
//
//               if ((((imageFileDir[0]>='a')&&(imageFileDir[0]<='z'))||
//                    ((imageFileDir[0]>='A')&&(imageFileDir[0]<='Z')))&&
//                   (imageFileDir[1]==':')&&
//                   (imageFileDir[2]=='\\'))
//               {
//                  imageDirNoDriveOrLeadSlash = imageFileDir.substr(3);
//               }
//               else
//               {
//                  imageDirNoDriveOrLeadSlash = imageFileDir;
//               }
//
//               metaDataDirectory = AddDirSeparator(metaDataDirectory) + imageDirNoDriveOrLeadSlash;
//            }
//
//            // We want to write directory paths to MTIMetadata.ini -
//            // look for MTIMetadata.ini in both the metadata root directory
//            // and the image file directory - obey global preference if we
//            // need to create the ini file. Is it even possible for the ini
//            // file to already exist at this point? Beats me!
//
//            metaDataIniFileName = AddDirSeparator(metaDataDirectory) + metaDataFileName;
//
//            bool foundIniInMetaDataDir = DoesFileExist(metaDataIniFileName);
//            bool foundIniInImageFileDir = DoesFileExist(AddDirSeparator(imageFileDir) + metaDataFileName);
//
//            if (!foundIniInMetaDataDir
//            && (foundIniInImageFileDir || !putMetadataIniInMetadataDir))
//            {
//               metaDataIniFileName = AddDirSeparator(imageFileDir) + metaDataFileName;
//            }
//
//            // Write directories back to MTIMetadata.ini
//            // We go through some trouble here to deal with a pre-exisiting
//            // ini file, although I don't know if that's possible. Also make
//            // sure the directories exist.
//            CIniFile *metaDataIniFile = CreateIniFile(metaDataIniFileName);
//            if (metaDataIniFile != NULL)
//            {
//               if (localHistoryDirectory !=
//                    metaDataIniFile->ReadString(historySectionName,
//                                                historyDirectoryKey,
//                                                ""))
//               {
//                  if (!localHistoryDirectory.empty())
//                  {
//                     metaDataIniFile->WriteString(historySectionName,
//                                                  historyDirectoryKey,
//                                                  localHistoryDirectory);
//                     if (!DoesDirectoryExist(localHistoryDirectory))
//                     {
//                        if (!MakeDirectory(localHistoryDirectory))
//                        {
//                           CAutoErrorReporter autoErr("CClipInitInfo::initFromImageFile3",
//                                                      AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
//                           autoErr.msg << "ERROR: Can't create history folder " << endl
//                                          << localHistoryDirectory << "." << endl
//                                          << "History will not be saved for this clip!";
//                           autoErr.errorCode = CLIP_ERROR_CANT_MAKE_HISTORY_DIRECTORY;
//                        }
//                     }
//                     else
//                     {
//                        if (!DoesWritableDirectoryExist(localHistoryDirectory))
//                        {
//                           CAutoErrorReporter autoErr("CClipInitInfo::initFromImageFile3",
//                                                      AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
//                           autoErr.msg << "ERROR: History folder"  << endl
//                                          << localHistoryDirectory << endl
//                                          << "exists but is not writable." << endl
//                                          << "History will not be saved for this clip!";
//                           autoErr.errorCode = CLIP_ERROR_CANT_MAKE_HISTORY_DIRECTORY;
//                        }
//                     }
//                  }
//                  else
//                  {
//                     metaDataIniFile->DeleteKey(historySectionName,
//                                                historyDirectoryKey);
//                  }
//               }
//
//               DeleteIniFile(metaDataIniFile);
//            }
//            else
//            {
//               CAutoErrorReporter autoErr("CClipInitInfo::initFromImageFile3",
//                                          AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
//               autoErr.msg << "ERROR: Metadata config file"  << endl
//                              << metaDataIniFileName << endl
//                              << "cannot be created." << endl
//                              << "History will not be saved for this clip!";
//               autoErr.errorCode = CLIP_ERROR_CANT_MAKE_HISTORY_DIRECTORY;
//            }
//         }
//      }
   }

   //** Clip2clip hack
   // write dirtyMediaStorageIndex if there is more than one media storage
   if (mediaStorageCount > 1)
      clipIniFile->WriteInteger(mediaSectionName, dirtyMediaStorageIndexKey,
                                              dirtyMediaStorageIndex);
   else if (clipIniFile->KeyExists(mediaSectionName, dirtyMediaStorageIndexKey))
      clipIniFile->DeleteKey(mediaSectionName, dirtyMediaStorageIndexKey);
   //**//

   return 0;
}

string CMediaStorageList::getMediaStorageKey(int index)
{
   // Create a string that looks like "MediaStorage_0"
   MTIostringstream ostr;
   ostr << mediaStorageKey << index;

   return ostr.str();
}

string CMediaStorageList::getHistoryStorageKey(int index)
{
   // Create a string that looks like "HistoryStorage_0"
   MTIostringstream ostr;
   ostr << historyStorageKey << index;

   return ostr.str();
}

int CMediaStorageList::parseMediaStorage(const string inString,
                                         EMediaType *newMediaType,
                                         string& newMediaIdentifier)
{
   // Iterator to input string
   string::const_iterator strIterator = inString.begin();

   // Skip over leading white space
   while (strIterator != inString.end() && isspace(*strIterator))
      {
      ++strIterator;
      }

   // Extract digits for Media Type
   string digitString;
   while (strIterator != inString.end() && isdigit(*strIterator))
      {
      digitString += *strIterator++;
      }
   if (digitString.empty())
      {
      // ERROR: Number is missing
      return -20;
      }
   // Convert digitString to a number value
   MTIistringstream istrm;
   istrm.str(digitString);
   int iMediaType = MEDIA_TYPE_INVALID;  // sentinel value
   istrm >> iMediaType;

   // Check Media Type
   if (!CMediaInterface::isMediaTypeValid(EMediaType(iMediaType)))
      {
      // ERROR: Media Type is not valid
      return -21;
      }

   // Skip any white space
   while (strIterator != inString.end() && isspace(*strIterator))
      {
      ++strIterator;
      }

   // Check for comma
   if (strIterator == inString.end() || *strIterator != ',')
      {
      // ERROR: comma is missing
      return -22;
      }
   ++strIterator;

   // Skip any leading white space
   while (strIterator != inString.end() && isspace(*strIterator))
      {
      ++strIterator;
      }

   if (strIterator == inString.end())
      {
      // ERROR: Media Identifier is missing
      return -23;
      }

   // Trim trailing white space
   string::const_iterator lastChar = inString.end() - 1;
   while (strIterator != lastChar && isspace(*lastChar))
      {
      --lastChar;
      }

   // Set caller's Media Type and Media Identifier arguments
   *newMediaType = EMediaType(iMediaType);

   newMediaIdentifier.erase();

   for ( ; strIterator != lastChar+1; ++strIterator)
      {
      newMediaIdentifier += *strIterator;
      }

   return 0;
}

int CMediaStorageList::parseHistoryStorage(const string inString,
                                           string& newHistoryDirectory)
{
   // Iterator to input string
   string::const_iterator strIterator = inString.begin();

   // Skip any leading white space
   while (strIterator != inString.end() && isspace(*strIterator))
      {
      ++strIterator;
      }

   if (strIterator == inString.end())
      {
      // KKKKK try removing this error
      return 0;
      // ERROR: Media Identifier is missing
      ///return -23;
      }

   // Trim trailing white space
   string::const_iterator lastChar = inString.end() - 1;
   while (strIterator != lastChar && isspace(*lastChar))
      {
      --lastChar;
      }

   newHistoryDirectory.erase();

   for ( ; strIterator != lastChar+1; ++strIterator)
      {
      newHistoryDirectory += *strIterator;
      }

   return 0;
}

int CMediaStorageList::registerOneMediaStorage(int mediaStorageIndex, CMediaFormat& mediaFormat)
{
   CMediaInterface mediaInterface;

   // Register one of a clip's media storages
   int mediaStorageCount = getMediaStorageCount();
   if (mediaStorageCount <= mediaStorageIndex)
   {
      return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR;
   }

   CMediaStorage* mediaStorage = getMediaStorage(mediaStorageIndex);
   CMediaAccess* mediaAccess
          = mediaInterface.registerMediaAccess(mediaStorage->getMediaType(),
                                         mediaStorage->getMediaIdentifier(),
                                         mediaStorage->getHistoryDirectory(),
                                         mediaFormat);

   if (mediaAccess == 0)
      {
      // ERROR: Media Access instance is not available for the Media Type &
      //        Media Identifier.  Cause should be included in the
      //        error message object
      if (theError.getError() != 0)
         return theError.getError();
      else
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN;
      }

   mediaStorage->setMediaAccessObj(mediaAccess);

   return 0;
}

int CMediaStorageList::registerMediaStorage(CMediaFormat& mediaFormat)
{
   CMediaInterface mediaInterface;
   int retVal = 0;

   // Register a clip's media storage
   int mediaStorageCount = getMediaStorageCount();
   for (int i = 0; i < mediaStorageCount; ++i)
      {
      retVal = registerOneMediaStorage(i, mediaFormat);
      if (retVal != 0)
         {
         break;
         }
      }

   return 0;
}

void CMediaStorageList::unregisterMediaStorage()
{
   CMediaInterface mediaInterface;     // singleton class for accessing media
                                       // storage management

   // "unregister" all of the media storage in this list
   int mediaStorageCount = getMediaStorageCount();
   for (int i = 0; i < mediaStorageCount; ++i)
      {
      CMediaStorage* mediaStorage = getMediaStorage(i);
      CMediaAccess *mediaAccess = mediaStorage->getMediaAccessObj();

      mediaInterface.unregisterMediaAccess(mediaAccess);
      }
}


const string& CMediaStorageList::getMetadataIniFilename()
{
   return metaDataFileName;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

void CMediaStorageList::dump(MTIostringstream &str) const
{
   str << "  Media Storage List: [.size: " << getMediaStorageCount() << "]"
       << std::endl;

   for (int i = 0; i < getMediaStorageCount(); ++i)
      {
      str << "    " << i << ": ";
      getMediaStorage(i)->dump(str);
      }
}
