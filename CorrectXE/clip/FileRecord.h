// FileRecord.h: interface for the CFileRecord class.
//
/*
$Header: /usr/local/filmroot/clip/include/FileRecord.h,v 1.9 2005/02/08 21:20:54 tolks Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef FILERECORDH
#define FILERECORDH

#include "machine.h"

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CStructuredStream;

// -----------------------------------------------------------------------
// Minimum record size, in bytes.  Total of size of recordByteCount, 
// recordType and recordTypeVersion
#define FR_MIN_RECORD_SIZE (sizeof(MTI_UINT32)+sizeof(MTI_UINT16)+sizeof(MTI_UINT16))

// Initial record type version
#define FR_INITIAL_RECORD_VERSION 1

// Initial File Type version
#define FR_INITIAL_FILE_TYPE_VERSION 1

// -----------------------------------------------------------------------

enum EFileRecordType
{
   FR_RECORD_TYPE_INVALID = 0,
   FR_RECORD_TYPE_COMMON_FILE_HEADER = 1,
   FR_RECORD_TYPE_VIDEO_CLIP_R2 = 2,      // Obsolete, Clip Rev 2
   FR_RECORD_TYPE_VIDEO_FRAME_R2 = 3,     // Obsolete, Clip Rev 2
   FR_RECORD_TYPE_VIDEO_FIELD_R2 = 4,     // Obsolete, Clip Rev 2
   FR_RECORD_TYPE_IMAGE_FORMAT_R2 = 5,
   FR_RECORD_TYPE_TIMECODE = 6,
   FR_RECORD_TYPE_AUDIO_CLIP_R2 = 7,      // Obsolete, Clip Rev 2
   FR_RECORD_TYPE_AUDIO_FRAME_R2 = 8,     // Obsolete, Clip Rev 2
   FR_RECORD_TYPE_AUDIO_FIELD_R2 = 9,     // Obsolete, Clip Rev 2
   FR_RECORD_TYPE_AUDIO_FORMAT_R2 = 10,   // Obsolete, Clip Rev 2
   FR_RECORD_TYPE_MEDIA_STORAGE_R2 = 11,  // Obsolete, Clip Rev 2

   FR_RECORD_TYPE_VIDEO_TRACK_R3 = 12,
   FR_RECORD_TYPE_VIDEO_FRAME_R3 = 13,
   FR_RECORD_TYPE_VIDEO_FIELD_R3 = 14,
   FR_RECORD_TYPE_AUDIO_TRACK_R3 = 15,
   FR_RECORD_TYPE_AUDIO_FRAME_R3 = 16,
   FR_RECORD_TYPE_AUDIO_FIELD_R3 = 17,

   FR_RECORD_TYPE_TIMECODE_FRAME = 18,
   FR_RECORD_TYPE_KEYKODE_FRAME = 19,
   FR_RECORD_TYPE_SMPTE_TIMECODE_FRAME = 20,
   FR_RECORD_TYPE_EVENT_FRAME = 21,

   FR_RECORD_TYPE_SMPTE_TIMECODE = 22,

   FR_RECORD_TYPE_FIELD_LIST_ENTRY = 23,

   FR_RECORD_TYPE_AUDIO_PROXY_FRAME = 24
 };

enum EFileType
{
   FR_FILE_TYPE_INVALID = 0,
   FR_FILE_TYPE_VIDEO_CLIP2 = 1,     // Obsolete, Clip Rev 2
   FR_FILE_TYPE_AUDIO_CLIP2 = 2,     // Obsolete, Clip Rev 2
   FR_FILE_TYPE_VIDEO_TRACK3 = 3,
   FR_FILE_TYPE_AUDIO_TRACK3 = 4,
   FR_FILE_TYPE_TIME_TRACK = 5,
   FR_FILE_TYPE_FIELD_LIST = 6
};

// -----------------------------------------------------------------------
// Define the "magic word" for the endian-ness of this machine
// ENDIAN comes from machine.h
// NOTE: The MTIX magic word is retained for backwards compatibility
//       with clips that were created before MTIY magic word
#define FR_MAGIC_WORD_MTIX_LITTLE_ENDIAN (0x5849544D)
#define FR_MAGIC_WORD_MTIX_BIG_ENDIAN (0x4D544958)
#define FR_MAGIC_WORD_MTIX (FR_MAGIC_WORD_MTIX_LITTLE_ENDIAN)

// NOTE: The MTIY magic word is retained for backwards compatibility
//       with clips with (short-lived) MTIY magic word
#define FR_MAGIC_WORD_MTIY_LITTLE_ENDIAN (0x5949544D)
#define FR_MAGIC_WORD_MTIY_BIG_ENDIAN (0x4D544959)
#define FR_MAGIC_WORD_MTIY (FR_MAGIC_WORD_MTIY_BIG_ENDIAN)

// -----------------------------------------------------------------------

class CFileRecord  
{
public:
   CFileRecord(MTI_UINT32 type, MTI_UINT16 version);    
   virtual ~CFileRecord();

   virtual int read(CStructuredStream& stream) = 0;
   virtual int write(CStructuredStream& stream);

   static int readRecord(CStructuredStream& stream, CFileRecord** newRecordPtr);
   static CFileRecord* make(EFileRecordType type, MTI_UINT16 version);
   
   virtual void calculateRecordByteCount();

   MTI_UINT32 getRecordByteCount() const;
   MTI_UINT16 getRecordType() const;
   MTI_UINT16 getRecordTypeVersion() const;

   bool isRecordType(EFileRecordType type);

protected:
   void setRecordByteCount(MTI_UINT32 newByteCount);

private:
   MTI_UINT32 recordByteCount;
   const MTI_UINT16 recordType;
   MTI_UINT16 recordTypeVersion;
};

// -----------------------------------------------------------------------

#endif // #ifndef FILERECORD_H
