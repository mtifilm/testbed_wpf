// StructuredFileStream.cpp: implementation of the CStructuredFileStream class.
//
/*
$Header: /usr/local/filmroot/clip/source/StructuredFileStream.cpp,v 1.5 2005/03/31 16:51:18 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "StructuredFileStream.h"
#include "err_clip.h"
#include "MTImalloc.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#define BUFFER_SIZE     (1024 * 1024)


CStructuredFileStream::CStructuredFileStream()
: internalBufferAllocation(true),
  bufferSize(BUFFER_SIZE), bufferHead(new char[BUFFER_SIZE])
{
    initBuffer();
}

CStructuredFileStream::CStructuredFileStream(char *newBuffer,
                                             unsigned int newBufferSize)
: internalBufferAllocation(false),
  bufferSize(newBufferSize), bufferHead(newBuffer)
{
    initBuffer();
}

CStructuredFileStream::~CStructuredFileStream()
{
   if (internalBufferAllocation)
      delete [] bufferHead;
}

//////////////////////////////////////////////////////////////////////
// File Creation, Open and Close Functions
//////////////////////////////////////////////////////////////////////

int CStructuredFileStream::createFile(const string &fileName)
{
   return (fileIO.createFile(fileName.c_str()));
}

int CStructuredFileStream::open(const string& fileName)
{
   return (fileIO.open(fileName.c_str()));
}

int CStructuredFileStream::close()
{
   int status;
   // Check if there is anything to write before closing the file
   // If there is something  to write in the buffer, then
   // flush the buffer
   if (writeAvailable < bufferSize)
      {
      if ((status = flushBuffer()) < 0)
         return status;
      }

   return (fileIO.close());
}

//////////////////////////////////////////////////////////////////////
// Read and Write Functions
//////////////////////////////////////////////////////////////////////

int CStructuredFileStream::read(void* buffer, unsigned int count)
{
    char *inPtr = (char*)buffer;
    unsigned int readCount = 0;

    while (count > readAvailable)
        {
        MTImemcpy((void *)inPtr, (void *)bufferPtr, readAvailable);
        count -= readAvailable;
        inPtr += readAvailable;
        bufferPtr += readAvailable;
        readCount += readAvailable;
        int status;
        if ((status = fillBuffer()) < 0)
            return status;              // Read error
        else if (status == 0)
            return -1;     // EOF
        }

    if (count > 0)
        {
        MTImemcpy((void *)inPtr, (void *)bufferPtr, count);
        readAvailable -= count;
        bufferPtr += count;
        readCount += count;
        }

    return (readCount);
}

int CStructuredFileStream::write(const void* buffer, unsigned int count)
{
    unsigned int writeCount = 0;
    const char *outPtr = (const char*)buffer;

    while(count >= writeAvailable)
        {
        memcpy(bufferPtr, outPtr, writeAvailable);
        count -= writeAvailable;
        outPtr += writeAvailable;
        bufferPtr += writeAvailable;
        writeCount += writeAvailable;
        int status;
        if ((status = flushBuffer()) < 0)
            return status;
        }

    if (count > 0)
        {
        memcpy(bufferPtr, outPtr, count);
        writeAvailable -= count;
        bufferPtr += count;
        writeCount += count;
        }
    
    return (writeCount);
}

int CStructuredFileStream::seek(MTI_INT64 offset)
{
   int retVal;

   // Flush whatever is in the write buffer
   retVal = flushBuffer();
   if (retVal < 0)
      return retVal;

   retVal = fileIO.seek(offset);
   if (retVal != 0)
      return retVal;

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Buffer Handling Functions
//////////////////////////////////////////////////////////////////////

void CStructuredFileStream::initBuffer()
{
    bufferPtr = bufferHead;
    writeAvailable = bufferSize;
    readAvailable = 0;
}

int CStructuredFileStream::fillBuffer()
{
    unsigned int readCount;

    initBuffer();

    readCount = fileIO.read(bufferHead, bufferSize);
    if (readCount > 0)
        {
        readAvailable = readCount;
        }
    return readCount;
}

int CStructuredFileStream::flushBuffer()
{
    unsigned int count = bufferPtr - bufferHead;
    if (count > 0)
        {
        if (fileIO.write(bufferHead, count) != (int)count)
            return CLIP_ERROR_FILE_WRITE_FAILED;

        initBuffer();
        }

    return count;
}
