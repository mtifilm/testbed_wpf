// Clip3VideoTrack.h: interface for the CVideoClip class.
//
//////////////////////////////////////////////////////////////////////

#ifndef CLIP3VIDEOTRACKH
#define CLIP3VIDEOTRACKH

#include "cliplibint.h"
#include "Clip3Track.h"
#include <map>
#include <vector>
using std::map;
using std::vector;

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CImageFormat;
class CVideoProxy;
class CVideoFrameList;

//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CVideoField : public CField
{
public:
   CVideoField();                                // Default Constructor
   CVideoField(const CVideoField& sourceField);  // Copy Constructor
   virtual ~CVideoField();

   CVideoField& operator=(const CVideoField& rhs);  // Assignment Operator

////   MTI_UINT32 getDataOffset();

   int readMedia(MTI_UINT8 *buffer);
   int readMediaQuick(MTI_UINT8 *buffer, int quickReadFlag);
   int readMediaQuickMT(MTI_UINT8 *buffer, int quickReadFlag,
                        int fileHandleIndex);
   int readMediaWithOffset(MTI_UINT8 *buffer, MTI_UINT8* &offsetBuffer);
   bool canBeReadAsynchronously();
   int startReadMediaWithOffsetAsynchronous(MTI_UINT8 *buffer,
                                            MTI_UINT8* &offsetBuffer,
                                            void* &readContext,
                                            bool dontNeedAlpha);
   bool isReadMediaWithOffsetAsynchronousDone(void* readContext);
   int finishReadMediaWithOffsetAsynchronous(void* &readContext,
                                             MTI_UINT8* &offsetBuffer);
   bool canBeReadAsFrameBuffer();
   int startReadMediaFrameBufferAsynchronous(void* &readContext);
   bool isReadMediaFrameBufferAsynchronousDone(void* readContext);
   int finishReadMediaFrameBufferAsynchronous(void* &readContext, MediaFrameBuffer* &frameBuffer);

	int writeMedia(MTI_UINT8 *buffer);
   int writeMediaWithDPXHeaderCopyHack(MTI_UINT8 *buffer,
													const string &sourceImageFilePath);

	int fetchMediaFile();
	int readMediaFrameBuffer(MediaFrameBufferSharedPtr &frameBuffer);
	int writeMediaFrameBuffer(const MediaFrameBufferSharedPtr &frameBuffer,
									  const string &sourceImageFilePath);

   int hackMedia(EMediaHackCommand hackCommand);
   int transferMediaFrom(CField *anotherField);
   int copyMediaFrom(CField *anotherField);
   int resetDirtyMetadata(CField *anotherField);
	int destroyMedia(CMediaAccess *mediaAccessOverride);

   int virtualCopy(const CVideoField &srcField,
                   CMediaStorageList &dstMediaStorageList);

	void dump(MTIostringstream& str) const;

private:
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CVideoFrame : public CFrame
{
public:
   CVideoFrame(int reserveFieldCount = 2);
   virtual ~CVideoFrame();

////   void getDataOffsetAllFields(MTI_UINT32 *dataOffset);
////   MTI_UINT32 getDataOffsetAtField(int fieldNumber);
////   void getDataOffsetVisibleFields(MTI_UINT32 *dataOffset);
   CVideoField* getField(int fieldIndex);
   EImageDataFlag getImageDataFlag() const;
   const CImageFormat* getImageFormat() const;
   MTI_UINT32 getMaxPaddedFieldByteCount();

   void setImageDataFlag(EImageDataFlag newFlag);
   void setImageFormat(const CImageFormat* newImageFormat);

   int readMediaAllFieldsOptimizedQuick(MTI_UINT8 *buffer[], int quickReadFlag);
	int readMediaAllFieldsQuick(MTI_UINT8 *buffer[], int quickReadFlag);
	bool canBeReadAsynchronously();
   int startReadMediaVisibleFieldsWithOffsetOptimizedAsynchronous(
                                                 MTI_UINT8 *buffer[],
                                                 MTI_UINT8 *offsetBuffer[],
                                                 void *&readContext,
                                                 bool dontNeedAlpha);
   bool isReadMediaVisibleFieldsWithOffsetOptimizedAsynchronousDone(
                                                 void* readContext);
   int finishReadMediaVisibleFieldsWithOffsetOptimizedAsynchronous(
                                                 void *&readContext,
                                                 MTI_UINT8 *offsetBuffer[]);
   bool canBeReadAsFrameBuffers();
   int startReadMediaVisibleFieldFrameBuffersAsynchronous(void *&readContext);
   bool isReadMediaVisibleFieldFrameBuffersAsynchronousDone(void* readContext);
   int finishReadMediaVisibleFieldFrameBuffersAsynchronous(void *&readContext, MediaFrameBuffer* frameBuffer[]);

   int readMediaVisibleFieldsOptimizedQuick(MTI_UINT8 *buffer[],
                                            int quickReadFlag,
                                            int explicitField=-1);
   int readMediaVisibleFieldsOptimizedQuickMT(MTI_UINT8 *buffer[],
															 int quickReadFlag,
                                              int fileHandleIndex,
															 int explicitField=-1);
	int readMediaVisibleFieldsQuick(MTI_UINT8 *buffer[], int quickReadFlag);

	int fetchMediaFrameFile();
	int readMediaFrameBuffer(MediaFrameBufferSharedPtr &frameBuffer);
	int writeMediaFrameBuffer(const MediaFrameBufferSharedPtr &frameBuffer,
									  const string &sourceImageFilePath);

   void blackenField(CField *field, int fieldIndex, MTI_UINT8 *buffer);
   void makeErrorFrame(MTI_UINT8 *buffer[], int errcod);

	CField* createField(CFieldList *newFieldList, int fieldListEntryIndex);
   CField* createField(const CField& srcField);

	int virtualCopy(CVideoFrame &srcFrame, CMediaStorageList &dstMediaStorageList);

   void DoFrameCacheHack();  // Low-level frame data cache hack

	void dump(MTIostringstream& str);

private:
   EImageDataFlag imageDataFlag;   // Real or simulated image data  TBD

   int readMediaOptimizedQuick(MTI_UINT8 *buffer[], int fieldCount,
                               int quickReadFlag, int explicitField=-1);
   int readMediaOptimizedQuickMT(MTI_UINT8 *buffer[], int fieldCount,
                                 int quickReadFlag, int fileHandleIndex,
                                 int explicitField=-1);
   int readMediaWithOffsetOptimized(MTI_UINT8 *buffer[],
                                    MTI_UINT8 *offsetBuffer[], int fieldCount);
};
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CVideoFramingInfo : public CFramingInfo
{
public:
   CVideoFramingInfo(int newTrackNumber, int newFramingType);
   virtual ~CVideoFramingInfo();
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CVideoFramingInfoList : public CFramingInfoList
{
public:
   CVideoFramingInfoList();
   virtual ~CVideoFramingInfoList();

   CVideoFramingInfo* getVideoFramingInfo(int framingIndex) const;

   int readVideoFramingInfoListSection(CIniFile *iniFile);
   int writeVideoFramingInfoListSection(CIniFile *iniFile);

   // Version clip framelist selector hack
   int getMainFrameListIndex();// readFramingInfoListSection() first!
   void setAndWriteMainFrameListIndex(CIniFile *iniFile,
                                       int newMainFrameListIndex);
   int getLockedFrameListIndex();// readFramingInfoListSection() first!
   void setAndWriteLockedFrameListIndex(CIniFile *iniFile,
                                       int newLockedFrameListIndex);

protected:
   int mainFrameListIndex;
   int lockedFrameListIndex;
   CFramingInfo* createDerivedFramingInfo(int newFramingNumber,
                                           int newFramingType);

private:
   static string videoFramingInfoListSection;
   static string videoFramingInfoSectionName;
   static string mainFrameListKey;
   static string lockedFrameListKey;
};
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////


struct SMediaListEntry
{
   int frameIndex;
   MTI_UINT8 *buffer[2];
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////


class MTI_CLIPLIB_API CVideoFrameList : public CFrameList
{
public:
   CVideoFrameList(CVideoFramingInfo *newFramingInfo,
                    CVideoProxy *newVideoProxy);
   CVideoFrameList(CVideoFramingInfo *newFramingInfo,
                    CVideoProxy *newVideoProxy,
                    MTI_UINT32 reserveFrameCount);
   virtual ~CVideoFrameList();

   CFrame* createFrame(int fieldCount, CMediaFormat* mediaFormat,
                        const CTimecode& timecode);

   virtual CVideoFrame* getFrame(int frameNumber);
   const CImageFormat* getImageFormat() const;
   MTI_UINT32 getMaxPaddedFieldByteCount(bool forceRecalc = false);
   CVideoProxy *getVideoProxy() const;
   virtual bool isLocked();

   int virtualCopy(CVideoFrameList &srcFrameList, int srcIndex,
                   int dstIndex, int frameCount,
                   CMediaStorageList &dstMediaStorageList);

   virtual int readFrameListInfoSection(CIniFile *iniFile,
                                        const string& sectionPrefix,
                                        const CTimecode& timecodePrototype);
   virtual int writeFrameListInfoSection(CIniFile *iniFile,
                                         const string& sectionPrefix);

   string makeFrameListFileName() const;
   string makeNewFrameListFileName(string newClipName) const;

   int preLoadFiles();

   virtual int readFrameListFile();
   virtual int writeFrameListFile();
   int renameFrameListFile(string newClipName);

   int readMediaAllFieldsQuick(int frameNumber, MTI_UINT8 *buffer[],
                               int quickReadFlag);
   int readMediaAllFieldsOptimizedQuick(int frameNumber, MTI_UINT8 *buffer[],
                                        int quickReadFlag);
   int readMediaVisibleFieldsQuick(int frameNumber, MTI_UINT8 *buffer[],
                                   int quickReadFlag);
   int readMediaVisibleFieldsOptimizedQuick(int frameNumber,
                                            MTI_UINT8 *buffer[],
                                            int quickReadFlag,
                                            int frameNumber1=-1);
   int readMediaVisibleFieldsOptimizedQuickMT(int frameNumber,
                                              MTI_UINT8 *buffer[],
                                              int quickReadFlag,
                                              int fileHandleIndex,
															 int frameNumber1=-1);

	int readMediaList(SMediaListEntry mediaList[], int count,
							int fileHandleIndex);

   int writeMediaList(SMediaListEntry mediaList[], int count);

   bool canBeReadAsynchronously(int frameNumber);
   int startReadMediaVisibleFieldsWithOffsetOptimizedAsynchronous(
                                                  int frameNumber,
                                                  MTI_UINT8 *buffer[],
                                                  MTI_UINT8 *offsetBuffer[],
                                                  void *&readContext,
                                                  bool dontNeedAlpha=false);
   bool isReadMediaVisibleFieldsWithOffsetOptimizedAsynchronousDone(
                                                  int frameNumber,
                                                  void* readContext);
   int finishReadMediaVisibleFieldsWithOffsetOptimizedAsynchronous(
                                                  int frameNumber,
                                                  MTI_UINT8 *offsetBuffer[],
                                                  void *&readContext);

   bool canBeReadAsFrameBuffers(int frameNumber);
   int startReadMediaVisibleFieldFrameBuffersAsynchronous(int frameNumber, void *&readContext);
   bool isReadMediaVisibleFieldFrameBuffersAsynchronousDone(int frameNumber, void* readContext);
	int finishReadMediaVisibleFieldFrameBuffersAsynchronous(int frameNumber, void *&readContext, MediaFrameBuffer* frameBuffer[]);

	int fetchMediaFrameFile(int frameNumber);
	int readMediaFrameBuffer(int frameNumber, MediaFrameBufferSharedPtr &outFrameBuffer);
	int writeMediaFrameBuffer(int frameNumber, const MediaFrameBufferSharedPtr &frameBuffer);

	virtual void initMediaBuffer(MTI_UINT8* buffer[]);

   void dump(MTIostringstream& str);      // for debugging

private:
   MTI_UINT32 maxPaddedFieldByteCount;  // Maximum of padded byte count
                                    // for all fields in the clip
                                    // Initialized to zero, set
                                    // when access function is first
                                    // called
private:
   static string videoTrackSectionName;
};

//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CVideoProxy : public CTrack
{
public:
   CVideoProxy(ClipSharedPtr &newParentClip, EClipVersion newClipVer,
               int newProxyNumber);
   virtual ~CVideoProxy();

   CFrameList *createFrameList(CFramingInfo *newFramingInfo);

   int extendVideoProxy(int frameCount, bool allocateMedia);
   int shortenVideoProxy(int frameCount);

   const CImageFormat* getImageFormat() const;
   int getProxyNumber() const;
   CVideoFrameList* getVideoFrameList(int framingIndex) const;

   bool isMediaFromImageFile() const;
   bool isSameVideoFrameList(int videoFrameListIndexA,
                             int videoFrameListIndexB) const;

   void setImageFormat(const CImageFormat& newImageFormat);

   int translateFrameIndex(int srcFrameIndex, int fromFramingIndex,
                           int toFramingIndex) const;
   int mapFrameIndex(int srcFrameIndex, int fromFramingIndex,
                     int toFramingIndex, bool findFirstFrame) const;
   int mapFrameIndex(int fromFrameIndex,
                     int fromFramingIndex, int toFramingIndex,
                     map<int, vector<std::pair<int,int> > > &resultList) const;
   int mapFrameAndFieldIndex(int fromFrameIndex, int fromFieldIndex,
                             int fromFramingIndex, int toFramingIndex,
                             vector<std::pair<int,int> > &resultList) const;

   int virtualCopy(CVideoProxy &srcTrack, int srcFrameIndex, int dstFrameIndex,
                   int frameCount);

   bool isOGLVClip() const;


   int updateMediaStorageList(
         const CMediaStorageList &newMediaStorageList,
         CIniFile *clipFile,
         const string &sectionPrefix);

private:

   int extendVideoProxy_ClipVer3(int frameCount, bool allocateMedia);
   int extendVideoProxy_ClipVer5L(int frameCount, bool allocateMedia);

}; // class CVideoProxy

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CVideoProxyList : public CTrackList
{
public:
   CVideoProxyList();
   virtual ~CVideoProxyList();

   int createVideoProxy(ClipSharedPtr &newParentClip, EClipVersion newClipVer,
                        int newHandleCount, const CImageFormat& newImageFormat);
   CTrack* createDerivedTrack(ClipSharedPtr &newParentClip, EClipVersion newClipVer,
                              int newTrackNumber);

   CVideoProxy* getVideoProxy(int proxyIndex) const;
   int getVideoProxyCount() const;

   CVideoProxy* findVideoProxy(const string &appId) const;
   int findVideoProxyIndex(const string &appId) const;

   int readVideoProxyListSection(CIniFile *iniFile, ClipSharedPtr &parentClip,
                                 CVideoFramingInfoList& videoFramingInfoList);
   int refreshVideoProxyListSection(CIniFile *iniFile);
   int writeVideoProxyListSection(CIniFile *iniFile);
   string getVideoSectionName () const;


private:
   static string videoProxySection;

}; // class CVideoProxyList

#ifdef PRIVATE_STATIC
#ifndef _CLIP_3_VIDEO_TRACK_DATA_CPP
namespace Clip3VideoTrackData {
   extern string videoProxySection;

   extern string videoFramingInfoListSection;
   extern string videoFramingInfoSectionName;

   extern string videoTrackSectionName;
};
#endif
#endif
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

#endif // #ifndef VIDEO_TRACK3_H

