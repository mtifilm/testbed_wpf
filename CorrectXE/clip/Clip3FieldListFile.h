// Clip3TimeTrackFile.h
//
// Created by: John Starr, April 29, 2003
//
/* CVS Info:
$Header: /usr/local/filmroot/clip/include/Clip3FieldListFile.h,v 1.4 2005/05/21 02:13:44 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef CLIP3FIELDLISTFILEH
#define CLIP3FIELDLISTFILEH

#include "FileBuilder.h"
#include "FileRecord.h"
#include "FileRecordString.h"

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CFileScanner;
class CFieldList;
class CFieldListEntry;
class CStructuredFileStream;

//////////////////////////////////////////////////////////////////////
// Time Track File Version
#define FR_FIELD_LIST_FILE_TYPE_FILE_VERSION (FR_INITIAL_FILE_TYPE_VERSION+2)

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

// Field List Entry file record schema version
#define FR_FIELD_LIST_ENTRY_FILE_RECORD_LATEST_VERSION (FR_INITIAL_RECORD_VERSION+2)

#define FIELD_LIST_ENTRY_RECORD_RESERVED_COUNT 2

//////////////////////////////////////////////////////////////////////

class CFieldListEntryFileRecord : public CFileRecord
{
public:
   CFieldListEntryFileRecord(MTI_UINT16 version
                              = FR_FIELD_LIST_ENTRY_FILE_RECORD_LATEST_VERSION);
   virtual ~CFieldListEntryFileRecord();

   MTI_INT32 getDefaultMediaType() const;
   MTI_INT32 getFieldIndex() const;
   MTI_UINT32 getFlags() const;
   MTI_INT32 getFrameIndex() const;

   void setDefaultMediaType(MTI_INT32 newType);
   void setFieldIndex(MTI_INT32 newIndex);
   void setFlags(MTI_UINT32 newFlags);
   void setFrameIndex(MTI_INT32 newIndex);

   virtual int read(CStructuredStream& stream);
   virtual int write(CStructuredStream &stream);

   void calculateRecordByteCount();

private:
   MTI_UINT32 flags;

   MTI_INT32 defaultMediaType;

   MTI_INT32 frameIndex; // Indices of frame and field in the video-frames frame
   MTI_INT32 fieldIndex; // list that uses this Field List Entry

   MTI_UINT32 reserved[FIELD_LIST_ENTRY_RECORD_RESERVED_COUNT]; // spare
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
// CFieldListFileReader
//   This class is used to read an entire field list file and populate
//   the Field List with Field List Entries

class CFieldListFileReader
{
friend class CFieldListFileUpdater;

public:
   CFieldListFileReader();
   CFieldListFileReader(char *newBuffer, unsigned int newBufferSize);
   virtual ~CFieldListFileReader();

   int readFieldListFile(CFieldList& fieldList);
   int reloadFieldListFile(CFieldList& fieldList);

private:
   int openFieldListFile(const string& fieldListFileName);
   void closeFieldListFile();

   int buildFieldListFromFile(CFieldList& fieldList);
   int updateFieldListFromFile(CFieldList& fieldList);

   CFieldListEntry* makeField(CFieldListEntryFileRecord& fieldRecord);

   void setField(CFieldListEntryFileRecord& fileRecord, CFieldListEntry &field);

private:
   char *externalBuffer;
   unsigned int externalBufferSize;

   CStructuredFileStream *stream;
   CFileScanner *fileScanner;
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
// CFieldListFileWriter
//   This class is used to write a new field list file or completely
//   overwrite an existing file

class CFieldListFileWriter
{
friend class CFieldListFileUpdater;

public:
   CFieldListFileWriter();
   virtual ~CFieldListFileWriter();

   int writeFieldListFile(CFieldList& fieldList);

private:
   int writeField(CFieldListEntry &field);
   int writeFieldList(CFieldList& fieldList);
   int writeFieldList(CFieldList& fieldList, int startFieldIndex,
                      int fieldCount);
   int initForUpdate(CStructuredFileStream *newStream);

private:
   CFileBuilder fileBuilder;
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
// CFieldListFileUpdater
//   This class is used to update individual records in an pre-exisiting
//   field list file.  We can update individual records given the
//   records are of fixed (not variable) length and there is
//   only a single record type (other than the headers) in the file.

class CFieldListFileUpdater
{
public:
   CFieldListFileUpdater();
   CFieldListFileUpdater(char *newBuffer, unsigned int newBufferSize);
   virtual ~CFieldListFileUpdater();

   int updateFieldListFile(CFieldList& fieldList, int firstFieldIndex,
                           int fieldCount);

private:
   int closeFieldListFile();
   int openFieldListFile(const string& fieldListFileName);
   int seekToRecord(int recordIndex);
   int updateFieldList(CFieldList& fieldList, int firstFieldCount,
                       int fieldCount);
   int updateFields(CFieldList& fieldList, int fieldIndex, int fieldCount);

private:
   char *externalBuffer;
   unsigned int externalBufferSize;

   CFieldListFileReader *fileReader;
   CFieldListFileWriter *fileWriter;
   unsigned long fileHeaderSize;
   unsigned long fieldRecordSize;
};

//////////////////////////////////////////////////////////////////////

#endif // #ifndef FIELD_LIST_TRACK_FILE_H
