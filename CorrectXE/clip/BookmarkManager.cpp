//------------------------------------------------------------------------

#include "BookmarkManager.h"

#include "ClipAPI.h"
#include "FileSweeper.h" // for StringTrim!!
#include "IniFile.h"
#include "pTimecode.h"

//------------------------------------------------------------------------

#define BOOKMARK_INI_FILE_NAME "Bookmark.ini"

//------------------------------------------------------------------------

CTimecode    BookmarkManager::CurrentTimecode;
int          BookmarkManager::SelectedBookmarkFrameIndex = -1;
string       BookmarkManager::MasterClipFolderPath;
string       BookmarkManager::BookmarkIniFilePath;
int          BookmarkManager::ClipTotalFrameCount = 0;
CTimecode    BookmarkManager::ClipTimecodeAtFrame0 = CTimecode::NOT_SET;
BookmarkInfo BookmarkManager::BookmarkInfoForDefaults;
bool         BookmarkManager::oldInSelectedBookmarkRangeFlag = false;

CBHook BookmarkManager::BookmarkAdded;
CBHook BookmarkManager::BookmarkSelectionChange;
CBHook BookmarkManager::SelectedBookmarkRangeEnteredOrExited;
//------------------------------------------------------------------------

void BookmarkManager::SetCurrentTimecode(CTimecode timecode)
{
   if (CurrentTimecode == timecode)
   {
      return;
   }

   CurrentTimecode = timecode;
   MaybeNotifySelectedBookmarkRangeEnteredOrExited();
}
//------------------------------------------------------------------------

CTimecode BookmarkManager::GetCurrentTimecode()
{
   return CurrentTimecode;
}
//------------------------------------------------------------------------

void BookmarkManager::MaybeNotifySelectedBookmarkRangeEnteredOrExited()
{
   bool inSelectedBookmarkRange = IsCurrentTimecodeInSelectedBookmark();

   if (inSelectedBookmarkRange == oldInSelectedBookmarkRangeFlag)
   {
      return;
   }

   oldInSelectedBookmarkRangeFlag = inSelectedBookmarkRange;
   SelectedBookmarkRangeEnteredOrExited.Notify();
}
//------------------------------------------------------------------------

void BookmarkManager::SetCurrentClipInfo(
                       const string newMasterClipFolderPath,
                       ClipSharedPtr &currentClip,
                       int videoProxyIndex,
                       int videoFramingIndex)
{
   MasterClipFolderPath = newMasterClipFolderPath;
   BookmarkIniFilePath = AddDirSeparator(MasterClipFolderPath) + BOOKMARK_INI_FILE_NAME;

   ClipTotalFrameCount = 0;
   ClipTimecodeAtFrame0 = CTimecode::NOT_SET;

   if (currentClip == nullptr)
   {
      return;
   }

   CVideoFrameList *videoFrameList = currentClip->getVideoFrameList(videoProxyIndex, videoFramingIndex);
   if (videoFrameList == 0)
   {
      return;
   }

  ClipTotalFrameCount = videoFrameList->getTotalFrameCount();
  ClipTimecodeAtFrame0 = videoFrameList->getTimecodeForFrameIndex(0);
}
//------------------------------------------------------------------------

void BookmarkManager::SelectBookmarkAt(int frameIndex)
{
   if (SelectedBookmarkFrameIndex == frameIndex)
   {
      return;
   }

   if (SelectedBookmarkFrameIndex >= 0)
   {
      SelectedBookmarkFrameIndex = -1;
      MaybeNotifySelectedBookmarkRangeEnteredOrExited();
   }

   SelectedBookmarkFrameIndex = frameIndex;
   BookmarkSelectionChange.Notify();
   MaybeNotifySelectedBookmarkRangeEnteredOrExited();
}
//------------------------------------------------------------------------

int BookmarkManager::GetSelectedBookmarkFrameIndex()
{
   return SelectedBookmarkFrameIndex;
}
//------------------------------------------------------------------------

int BookmarkManager::GetBookmarkInfo(int frameIndex, BookmarkInfo &bookmarkInfo)
{
   int retVal = bookmarkInfo.LoadFrom(BookmarkIniFilePath, frameIndex, ClipTimecodeAtFrame0);
   if (retVal != 0)
   {
      TRACE_0(errout << "ERROR: BookmarkManager: Can't get bookmark info for frame " << frameIndex
                     << " from " << MasterClipFolderPath << "(error code " << retVal << ")");
   }

   return retVal;
}
//------------------------------------------------------------------------

int BookmarkManager::SaveBookmarkInfo(int frameIndex, const BookmarkInfo &bookmarkInfo)
{
   int retVal = bookmarkInfo.SaveTo(BookmarkIniFilePath, frameIndex);
   if (retVal != 0)
   {
      TRACE_0(errout << "ERROR: BookmarkManager: Can't save bookmark info for frame " << frameIndex
                     << " in " << MasterClipFolderPath << "(error code " << retVal << ")");
   }

   BookmarkInfoForDefaults = bookmarkInfo;

   return retVal;
}
//------------------------------------------------------------------------

int BookmarkManager::AddBookmark(int frameIndex)
{
	BookmarkInfo newBookmarkInfo;
	newBookmarkInfo.InTimecode = ClipTimecodeAtFrame0 + frameIndex;
	newBookmarkInfo.OutTimecode = newBookmarkInfo.InTimecode;

	MTIostringstream os;
	time_t theTime = time(NULL);
	struct tm *aTime = localtime(&theTime);

	os << aTime->tm_year + 1900 << "-" << std::setw(2) << std::setfill('0') << aTime->tm_mon + 1
		<< "-" << std::setw(2) << std::setfill('0') << aTime->tm_mday;

	newBookmarkInfo.Date = os.str();

	// Save newly created bookmark info so we can select it.
	int retVal = newBookmarkInfo.SaveTo(BookmarkIniFilePath, frameIndex);
	if (retVal != 0)
	{
		TRACE_0(errout << "ERROR: BookmarkManager: Can't add bookmark info for frame " <<
			frameIndex << " to " << MasterClipFolderPath << "(error code " << retVal << ")");
		return retVal;
	}

	SelectBookmarkAt(frameIndex);
	BookmarkAdded.Notify();

	// Re-save the bookmark info in case someone changed some fields.
	newBookmarkInfo.SaveTo(BookmarkIniFilePath, frameIndex);

	return retVal;
}
//------------------------------------------------------------------------

int BookmarkManager::AddBookmark(const BookmarkInfo &bookmarkInfo)
{
   BookmarkInfo newBookmarkInfo(bookmarkInfo);

   if (newBookmarkInfo.InTimecode == CTimecode::NOT_SET
   || newBookmarkInfo.OutTimecode == CTimecode::NOT_SET
   || newBookmarkInfo.InTimecode > newBookmarkInfo.OutTimecode)
   {
      return BOOKMARKINFO_NOT_VALID;
   }

   newBookmarkInfo.InTimecode.setFlags(ClipTimecodeAtFrame0.getFlags());
   newBookmarkInfo.InTimecode.setFramesPerSecond(ClipTimecodeAtFrame0.getFramesPerSecond());
   int frameIndex = newBookmarkInfo.InTimecode - ClipTimecodeAtFrame0;
   if (frameIndex < 0 || frameIndex >= ClipTotalFrameCount)
   {
      return BOOKMARKINFO_OUT_OF_RANGE;
   }

   newBookmarkInfo.OutTimecode.setFlags(ClipTimecodeAtFrame0.getFlags());
   newBookmarkInfo.OutTimecode.setFramesPerSecond(ClipTimecodeAtFrame0.getFramesPerSecond());

   // Can't this fail? QQQ
   newBookmarkInfo.SaveTo(BookmarkIniFilePath, frameIndex);

   return BOOKMARK_ADDED_SUCCESSFULLY;
}
//------------------------------------------------------------------------

int BookmarkManager::DeleteBookmark(int frameIndex)
{
   int retVal = BookmarkInfo::Delete(BookmarkIniFilePath, frameIndex);
   if (retVal != 0)
   {
      TRACE_0(errout << "ERROR: BookmarkManager: Can't remove bookmark info for frame " << frameIndex
                     << " from " << MasterClipFolderPath << "(error code " << retVal << ")");
   }

   if (SelectedBookmarkFrameIndex == frameIndex)
   {
      SelectBookmarkAt(-1);
   }

   return retVal;
}
//------------------------------------------------------------------------

bool BookmarkManager::DoesFrameHaveBookmarkInfo(int frameIndex)
{
   set<int> indexSet = BookmarkInfo::GetSetOfBookmarkFrameIndices(BookmarkIniFilePath);
   return indexSet.count(frameIndex) > 0;
}
//------------------------------------------------------------------------

bool BookmarkManager::DoAnyFramesHaveBookmarkInfo()
{
   set<int> indexSet = BookmarkInfo::GetSetOfBookmarkFrameIndices(BookmarkIniFilePath);
   return indexSet.size() > 0;
}
//---------------------------------------------------------------------------

bool BookmarkManager::IsCurrentTimecodeInSelectedBookmark()
{
   if (SelectedBookmarkFrameIndex < 0
   || CurrentTimecode == NULL || CurrentTimecode == CTimecode::NOT_SET)
   {
      return false;
   }

   BookmarkInfo bookmarkInfo;
   int retVal = bookmarkInfo.LoadFrom(BookmarkIniFilePath, SelectedBookmarkFrameIndex, ClipTimecodeAtFrame0);
   if (retVal != 0)
   {
      TRACE_0(errout << "ERROR: BookmarkManager: Can't get bookmark info for frame " << SelectedBookmarkFrameIndex
                     << " from " << MasterClipFolderPath << "(error code " << retVal << ")");
      return false;
   }

   if (bookmarkInfo.InTimecode == CTimecode::NOT_SET
   || bookmarkInfo.OutTimecode == CTimecode::NOT_SET)
   {
      return false;
   }

   if (CurrentTimecode < bookmarkInfo.InTimecode || CurrentTimecode > bookmarkInfo.OutTimecode)
   {
      return false;
   }

   return true;
}
//------------------------------------------------------------------------

int BookmarkManager::GetPreviousBookmarkFrameIndex(int frameIndex)
{
   // Return the argument if there is no previous bookmark
   int retVal = frameIndex;
   set<int> indexSet = BookmarkInfo::GetSetOfBookmarkFrameIndices(BookmarkIniFilePath);

   // Find or temporarily insert the frame index
   std::pair<set<int>::iterator, bool> insertResult = indexSet.insert(frameIndex);
   std::set<int>::iterator &iter = insertResult.first;
   bool needToRemoveFrameIndex = insertResult.second;
   if (iter != indexSet.begin())
   {
      retVal = *(--iter);
   }

   if (needToRemoveFrameIndex)
   {
      indexSet.erase(frameIndex);
   }

   return retVal;
}
//------------------------------------------------------------------------

int BookmarkManager::GetNextBookmarkFrameIndex(int frameIndex)
{
   // Return the argument if there is no previous bookmark
   int retVal = frameIndex;
   set<int> indexSet = BookmarkInfo::GetSetOfBookmarkFrameIndices(BookmarkIniFilePath);

   // Find or temporarily insert the frame index
   std::pair<set<int>::iterator, bool> insertResult = indexSet.insert(frameIndex);
   std::set<int>::iterator &iter = insertResult.first;
   bool needToRemoveFrameIndex = insertResult.second;
   if (++iter != indexSet.end())
   {
      retVal = *iter;
   }

   if (needToRemoveFrameIndex)
   {
      indexSet.erase(frameIndex);
   }

   return retVal;
}
//------------------------------------------------------------------------

BookmarkInfo *BookmarkManager::CreateBookmarkInfo(StringList headers, StringList row)
{
   return BookmarkInfo::Create(headers, row, ClipTimecodeAtFrame0);
}
//------------------------------------------------------------------------
