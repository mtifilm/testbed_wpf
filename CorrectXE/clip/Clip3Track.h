// Clip3Track.h: interface for the CFrameList abstract base class.
//
//////////////////////////////////////////////////////////////////////

#ifndef Clip3TrackH
#define Clip3TrackH

#include <vector>
#include <string>
#include <map>
#if defined(__GNUG__) && (__GNUG__ < 3)
// g++ versions < 3 don't have the ostream header
#include <iostream>
#else
#include <ostream>
#endif

using std::vector;
using std::string;
using std::ostream;
using std::map;

#include "machine.h"
#include "cliplibint.h"
#include "ClipSharedPtr.h"
#include "Clip5Proto.h"
#include "ImageInfo.h"
#include "MediaInterface.h"
#include "MediaLocation.h"
#include "MediaStorage3.h"
#include "timecode.h"

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CClip;
class CCommonInitInfo;
class CFieldList;
class CFrameList;
class CIniFile;
class CMediaDeallocatorList;
class CMediaFormat;
class CTrack;
class CMediaAllocator;

//////////////////////////////////////////////////////////////////////

enum EClipVersion
{
   CLIP_VERSION_INVALID = 0,
   CLIP_VERSION_3 = 30,
   CLIP_VERSION_5L = 50
};

//////////////////////////////////////////////////////////////////////

// Video Frame Pulldown Label
enum EFrameLabel
{
   // 3:2 Field Pull-Down Sequence (non-525): Aa, Bb, Bc, Cd and Dd
   FRAME_LABEL_Aa = 0,
   FRAME_LABEL_Bb = 1,
   FRAME_LABEL_Bc = 2,
   FRAME_LABEL_Cd = 3,
   FRAME_LABEL_Dd = 4,

   // 3:2 Frame Pull-Down Sequence A0, A1, B0, B1, B2
   FRAME_LABEL_A0 = 5,
   FRAME_LABEL_A1 = 6,
   FRAME_LABEL_B0 = 7,
   FRAME_LABEL_B1 = 8,
   FRAME_LABEL_B2 = 9,

   // 3:2 Field Pull-Down Sequence (525 only): aA, bB, cB, dC and dD
   FRAME_LABEL_aA = 10,
   FRAME_LABEL_bB = 11,
   FRAME_LABEL_cB = 12,
   FRAME_LABEL_dC = 13,
   FRAME_LABEL_dD = 14
};

//////////////////////////////////////////////////////////////////////

struct SCopyFieldSource
{
   int frameIndex;
   int fieldIndex;
   int visibleSiblingIndex;
};

#define MAX_SOURCE_FIELDS 32

struct SCopyFrameSource
{
   int fieldCount;
   SCopyFieldSource fieldSrc[MAX_SOURCE_FIELDS];
   CFrameList  *srcTrack;

   bool isAllSourceFramesUserMaterial() const;
};

struct SFilmFrameSource
{
   EFrameLabel *frameLabels;
   int frameIndex;
   int inFrameIndex;
   int outFrameIndex;

   SCopyFrameSource frameSrc;

   CTimecode *timecode;
};

//////////////////////////////////////////////////////////////////////
// Field Flags

// int getVisibleSiblingFieldIndex()
// void setVisibleSiblingFieldIndex(int visibleSiblingFieldIndex)
//    0 - Visible sibling is first visible field
//    1 - Visible sibling is second visible field (interlaced only)
//  Identifies which visible field is the "sibling" to this field.
//  This flag is only relevant for invisible fields.
//  Bit-Mask Constant: FIELD_FLAG_VISIBLE_SIBLING

// bool isFieldInvisible()
// void setFieldInvisible(bool newFlag)
//    false - This field is visible
//    true - This field is invisible
//  Marks a field as visible or invisible.  In a progressive clip,
//  only one field can be visible.  In an interlaced clip exactly
//  two fields must be visible.  This information is redundant with
//  the implicit visibility/invisibility given by the position of
//  the field in a frame's field list.
//  Bit-Mask Constant: FIELD_FLAG_INVISIBLE

// bool isVirtualMedia() const;
// void setVirtualMedia(bool newFlag);
//    false - Field "owns" its media
//    true - Media is "virtual" and is owned by another clip
//  Indicates whether the field owns the media it references.  If
//  a field owns its media, then deleting the clip will deallocate
//  the media storage.  If the media is virtual, then another clip
//  controls the media.
//  Bit-Mask Constant: FIELD_FLAG_VIRTUAL_MEDIA

// bool isSharedMedia() const;
// void setSharedMedia(bool newFlag);
//    false - Field's media is not shared and is only referenced by this field
//    true - Field's media is also referenced by another field in the frame
//  Indicates whether the field's media is shared by another field in
//  the same frame.  This is used when two fields have identical
//  images, e.g., after perfect 3:2 pull-down is imposed.
//  Bit-Mask Constant: FIELD_FLAG_SHARED_MEDIA

// bool isSharedMediaFirst() const;
// void setSharedMediaFirst(bool newFlag);
//    false - Field is not the first of fields that share the same media
//    true - Field is first of fields that share the same media
//  Indicates whether the field is the first of two or more fields in a frame
//  that share the same media.  Only relevant when Shared Media flag is set.
//  If two or more fields in a frame have the same media location, one field
//  will have the Shared Media First flag set to true and the other
//  fields will have the flag set to false.
//  Bit-Mask Constant: FIELD_FLAG_SHARED_MEDIA_FIRST

// MTI_UINT16 getFlags()
// void setFlags(MTI_UINT16 newFlags)
//  Get and set all field flags using bit mask values.
//  Note: These functions can be used by application programs, but use of
//        query and change functions for individual flags is recommended.

//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CField
{
public:
   CField();                              // Default Constructor
   CField(const CField& sourceField);     // Copy Constructor
   virtual ~CField();

   // Comparison Operators
   virtual bool operator>(const CField& rhs) const;
   virtual int compare(const CField& rhs) const;

   // Accessors
   MTI_UINT32 getDataSize();
   virtual MTI_UINT16 getFieldFlags() const;
   virtual CFieldList* getFieldList() const;
   virtual int getFieldListEntryIndex() const;
   virtual const CMediaLocation& getMediaLocation() const;
   virtual int getVisibleSiblingFieldIndex() const;

   virtual bool doesMediaStorageExist() const;
   virtual bool isBorrowedMedia() const;
   virtual bool isDirtyMedia() const;
   virtual bool isFieldInvisible() const;
   virtual bool isMediaWritten() const;
   virtual bool isSharedMedia() const;
   virtual bool isSharedMediaFirst() const;
   virtual bool isVirtualMedia() const;
   virtual bool isWriteProtectedMedia() const;

   // Mutators
   virtual void setFieldInvisible(bool newFlag);
   virtual void setFieldFlags(MTI_UINT16 newFlags);
   virtual void setFieldListEntryIndex(CFieldList *newFieldList, int newIndex);
   virtual void setBorrowedMedia(bool newFlag);
   virtual void setDirtyMedia(bool newFlag);
   virtual void setMediaLocation(const CMediaLocation& newMediaLocation);
   virtual void setMediaStorageExists(bool newFlag);
   virtual void setMediaWritten(bool newFlag);
   virtual void setSharedMedia(bool newFlag);
   virtual void setSharedMediaFirst(bool newFlag);
   virtual void setVirtualMedia(bool newFlag);
   virtual void setVisibleSiblingFieldIndex(int visibleSiblingFieldIndex);
   virtual void setWriteProtectedMedia(bool newFlag);

   virtual void assignVirtualMedia(MTI_UINT32 newDataSize);

   int fixMediaStorageForVirtualCopy(CMediaStorageList &dstMediaStorageList);

   //** clip2clip hack
   int assignDirtyMediaStorage(CMediaLocation dirtyMediaLocation);
   void unassignDirtyMediaStorage(const CMediaLocation &cleanMediaLocation,
                                  bool doesCleanMediaStorageExist,
                                  bool isCleanMediaWritten);
   int allocateDirtyMediaStorage(
                  const CMediaStorageList *mediaStorageList,
                  const CMediaFormat& mediaFormat);
   //**//

   // Read & Write Media
	virtual int readMedia(MTI_UINT8 *buffer) = 0;
	virtual int writeMedia(MTI_UINT8 *buffer) = 0;
	virtual int writeMediaWithDPXHeaderCopyHack(MTI_UINT8 *buffer, const string &sourceImageFilePath);

   // Hack Media
   virtual int hackMedia(EMediaHackCommand hackCommand);

   // Transfer Media
   virtual int transferMediaFrom(CField *anotherField);
   virtual int copyMediaFrom(CField *anotherField);

   // Just another version clip copyback hack
   virtual int resetDirtyMetadata(CField *anotherField);

   // Destroy Media
   virtual int destroyMedia(CMediaAccess *mediaAccessOverride);

   // Test Functions
   virtual void dump(MTIostringstream& str) const;

// Field Flag Bit Values
// These are public so that they can be used by application programs,
// but use of query and change functions for individual flags is recommended
#define FIELD_FLAG_INVISIBLE           0x0001
#define FIELD_FLAG_VISIBLE_SIBLING     0x0002
#define FIELD_FLAG_RESERVED_BIT_3      0x0004
#define FIELD_FLAG_VIRTUAL_MEDIA       0x0008
#define FIELD_FLAG_SHARED_MEDIA        0x0010
#define FIELD_FLAG_SHARED_MEDIA_FIRST  0x0020
#define FIELD_FLAG_RESERVED_BITS    (0xFFC0 | FIELD_FLAG_RESERVED_BIT_3)
protected:
   // Assignment Operator (protected since CField is an abstract base class)
   void operator=(const CField& rhs);

   // Field flag helper functions
   bool isFieldFlagSet(MTI_UINT16 bitFlag) const;
   void setFieldFlag(MTI_UINT16 bitFlag, bool newValue);

protected:
   CMediaLocation mediaLocation;
   MTI_UINT16 flags;     // Field flags

   CFieldList *fieldList;
   int fieldListEntryIndex;

}; // class CField

//////////////////////////////////////////////////////////////////////

// Maximum number of fields that can be optimized in read & write media
// functions.  Number is arbitrary and much larger than should ever
// be used.
#define CFRAME_MAX_FIELD_COUNT  64

//////////////////////////////////////////////////////////////////////

// Hideous version clip undo crapola
struct DirtyMediaAssignmentUndoItem
{
    CMediaLocation mediaLocation;
    bool mediaStorageExists;
    bool mediaWritten;

    DirtyMediaAssignmentUndoItem()
    : mediaLocation()
    , mediaStorageExists(false)
    , mediaWritten(false)
    { };

    DirtyMediaAssignmentUndoItem(const CField &origField)
    : mediaLocation(origField.getMediaLocation())
    , mediaStorageExists(origField.doesMediaStorageExist())
    , mediaWritten(origField.isMediaWritten())
    { };

    DirtyMediaAssignmentUndoItem(const DirtyMediaAssignmentUndoItem &rhs)
    : mediaLocation(rhs.mediaLocation)
    , mediaStorageExists(rhs.mediaStorageExists)
    , mediaWritten(rhs.mediaWritten)
    { };

    void undo(CField &buggeredField)
    {
        buggeredField.unassignDirtyMediaStorage(mediaLocation,
                                                mediaStorageExists,
                                                mediaWritten);
    };
};

typedef map<int,DirtyMediaAssignmentUndoItem>  DirtyMediaAssignmentUndoList;

struct DirtyMediaAssignmentUndoInfo
{
    DirtyMediaAssignmentUndoList undoFieldList;
    bool undoHistoryFlag;
    DirtyMediaAssignmentUndoInfo() : undoHistoryFlag(false) {};
};


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CFrame
{
public:
   CFrame(int reserveFieldCount = 2);
   virtual ~CFrame();

   virtual bool doesMediaStorageExist();
   virtual bool isDirty();
   virtual CField* getBaseField(int fieldIndex);
   virtual MTI_UINT32 getDataSizeAtField(int fieldNumber);
	virtual int GetMediaLocationsAllFields(vector<CMediaLocation> &mediaLocationList);
   virtual int getTotalFieldCount() const;
   virtual int getVisibleFieldCount() const;
   virtual int getInvisibleFieldCount() const;
   virtual CTimecode getTimecode() const;
   virtual void getTimecode(CTimecode& destinationTimecode);
   virtual bool isMediaWritten();

   virtual void setMediaWritten(bool newFlag);
   virtual void setTimecode(const CTimecode &newTimecode);

   virtual void addField(CField *field);

   virtual CField* createField(CFieldList *newFieldList,
                               int fieldListEntryIndex) = 0;
   virtual CField* createField(const CField& srcField) = 0;
   int copyFields(SCopyFrameSource& frameSrc);
   int doubleField(CFrame &srcFrame, int fieldIndex);

   virtual int readMediaAllFields(MTI_UINT8 *buffer[]);
   virtual int readMediaAllFieldsOptimized(MTI_UINT8 *buffer[]);
   virtual int readMediaAllFieldsOptimizedMT(MTI_UINT8 *buffer[],
                                             int fileHandleIndex);
   virtual int readMediaVisibleFields(MTI_UINT8 *buffer[]);
	virtual int readMediaVisibleFieldsOptimized(MTI_UINT8 *buffer[]);

   virtual int writeMediaAllFields(MTI_UINT8 *buffer[]);
   virtual int writeMediaAllFieldsOptimized(MTI_UINT8 *buffer[]);
   virtual int writeMediaFilmFramesOptimized(MTI_UINT8 *buffer[]);
   virtual int writeMediaVisibleFields(MTI_UINT8 *buffer[]);
   virtual int writeMediaVisibleFieldsOptimized(MTI_UINT8 *buffer[]);
	virtual int writeMediaWithDPXHeaderCopyHack(MTI_UINT8 *buffer[], const string &sourceImageFilePath);

   virtual void sortFieldsByMediaLocation(int fieldCount, int *indexArray,
                                          CField **fieldArray);
   enum OPT_RESULT {CANNOT_OPTIMIZE, NEED_ONE_IO_OP, NEED_TWO_IO_OPS};
   OPT_RESULT getMediaLocationOptimized(MTI_UINT8 *buffer[], int fieldCount,
                                        CMediaLocation *mediaLocationArray,
                                        bool mustExistFlag);
   virtual void blackenField(CField *field, int fieldIndex, MTI_UINT8 *buffer)
                                                                            = 0;

   //** clip2clip hacks
   bool isWriteProtected();
   void allocateDirtyMediaStorageIfNecessary(
                                  const CMediaStorageList *mediaStorageList);
   CMediaAllocator *getDirtyMediaStorageAllocator(
                                  CMediaStorageList &mediaStorageList,
                                  int &status);
   int assignDirtyMediaStorage(CMediaAllocator &mediaAllocator,
                               DirtyMediaAssignmentUndoList &undoList);
   int unassignDirtyMediaStorage(DirtyMediaAssignmentUndoList &undoList);
   //**//

   virtual void dump(MTIostringstream& str);

protected:
   virtual const CMediaFormat* getMediaFormat() const;
   virtual void setMediaFormat(const CMediaFormat* newMediaFormat);

   void deleteFieldList();

   virtual int readMediaOptimized(MTI_UINT8 *buffer[], int fieldCount);
   virtual int readMediaOptimizedMT(MTI_UINT8 *buffer[], int fieldCount,
                                    int fileHandleIndex);
   virtual int writeMediaOptimized(MTI_UINT8 *buffer[], int fieldCount);

protected:
   CTimecode* timecode;                // Timecode for this frame
   const CMediaFormat* mediaFormat;    // Frame's media format

private:
   typedef vector<CField*> FieldList;

   FieldList fieldList;          // List of fields for this frame
   CFrame& operator=(CFrame& frame);   // Don't want to implement this
                                       // or allow default assignment

}; // class CFrame

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

// Track Type Enumeration
enum ETrackType
{
   TRACK_TYPE_INVALID = 0,
   TRACK_TYPE_AUDIO = 1,
   TRACK_TYPE_VIDEO = 2
};

// Framing Type Constants
#define FRAMING_TYPE_INVALID      0    // Invalid framing type
#define FRAMING_TYPE_VIDEO        1    // Video-Frames.  Field arrangement
                                       // determined by original source.  No
                                       // invisible fields.  Timecode is
                                       // monotonically increasing and timecode
                                       // arithmetic is equivalent to frame
                                       // index arithmetic.
#define FRAMING_TYPE_FILM         2    // Film-Frames.  Fields arranged to
                                       // eliminate the 3:2 pulldown present in
                                       // the video-frames source.  The
                                       // timecode for a frame is based on
                                       // timecode of source video-frames.
                                       // Half-frame flag may be set and the
                                       // difference between timecodes of
                                       // adjacent frames may be something
                                       // other than 1.  Timecode is generally
                                       // monotonically increasing, but may
                                       // repeat or reverse if the pulldown
                                       // sequence in the source is not
                                       // consistent.  Frame rate is generally
                                       // assumed to be 24 fps.
#define FRAMING_TYPE_FIELDS_1_2   3
#define FRAMING_TYPE_FIELD_1_ONLY 4
#define FRAMING_TYPE_FIELD_2_ONLY 5
#define FRAMING_TYPE_CADENCE_REPAIR 6  // framing is determined by cadence
                                       // repair tool.

// Add new framing types here.  Increase the value of
// FRAMING_TYPE_CUSTOM so it is 1 greater than the largest explicitly
// defined FRAMING_TYPE.  If you add new framing types, make sure
// special cases based on existing types are extended to handle your new
// types appropriately as well.

#define FRAMING_TYPE_CUSTOM       7    // Custom-Frames.  Fields arranged
                                       // to suit user's or application's
                                       // purpose.  Possible uses include
                                       // manually forcing pulldown removal
                                       // and combining repeat frames in
                                       // animation.  May have invisible
                                       // fields.  Timecode is (presumably)
                                       // derived from the source video-frames


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CFramingInfo
{
public:
   CFramingInfo(ETrackType newTrackType, int newFramingNumber,
                 int newFramingType);
   virtual ~CFramingInfo();

   static bool checkTrackType(ETrackType newTrackType);

   virtual ETrackType getTrackType() const;
   virtual string getDescription() const;
   virtual EFrameRate getFrameRateEnum() const;
   virtual MTI_REAL32 getFrameRate() const;
   virtual int getFramingType() const;
   virtual bool getSaveRecordFlag() const;
   virtual int getFramingNumber() const;

   virtual void setDescription(const string& newDescription);
   virtual void setFrameRate(EFrameRate newFrameRateEnum,
                             double newFrameRate);
   virtual void setFramingNumber(int newFramingNumber);
   virtual void setFramingType(int newFramingType);
   virtual void setSaveRecordFlag(bool newFlag);

   virtual int readFramingInfoSection(CIniFile *iniFile,
                                    const string& sectionName);
   static ETrackType getTrackTypeFromTrackSection(CIniFile *iniFile,
                                                  const string& sectionName);
   virtual int writeFramingInfoSection(CIniFile *iniFile,
                                     const string& sectionName);

   virtual void dump(MTIostringstream& str);      // for debugging

protected:
   int framingNumber;

   ETrackType trackType;       // Type of track: audio, video, etc.
                               // Set when Track instance is constructed
   int framingType;            // Framing type.  Type of framing sequence:
                               //    FRAMING_TYPE_VIDEO = 1
                               //    FRAMING_TYPE_FILM = 2
                               //    FRAMING_TYPE_CUSTOM = 1 greater
                               // than the largest explicity defined
                               // FRAMING_TYPE.  Values greater than or
                               // equal to FRAMING_TYPE_CUSTOM are
                               // assumed to be an user- or
                               // application-defined "custom" framing.
                               // Values 0 or less are assumed to be
                               // invalid.
   string description;         // Track description

   EFrameRate frameRateEnum;
   MTI_REAL32 frameRate;       // Accurate frame rate

   bool saveRecordFlag;        // If set, media cannot be modified TBD

private:
   static string trackTypeKey;
   static string framingTypeKey;
   static string descriptionKey;
   static string frameRateKey;

}; // class CFramingInfo

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

// Framing Number that indicates there is no track at this list position
#define FRAMING_NUMBER_NO_TRACK     (-1)

//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CFramingInfoList
{
public:
   CFramingInfoList();
   virtual ~CFramingInfoList();

   virtual CFramingInfo* createVideoFramesFramingInfo(ClipSharedPtr &clip,
                                        const CCommonInitInfo &commonInitInfo);
   virtual CFramingInfo* createFilmFramesFramingInfo(ClipSharedPtr &clip,
                                                      bool forceNoPulldown);
   virtual CFramingInfo* createVideoFieldsFramingInfo(ClipSharedPtr &clip,
                                                      int newFramingType,
                                                      int newFramingIndex);
   virtual CFramingInfo* createCadenceRepairFramingInfo(ClipSharedPtr &clip,
                                                      int newFramingType,
                                                      int newFramingIndex);
   virtual CFramingInfo* createCustomFramesFramingInfo(ClipSharedPtr &clip);

   virtual int getFramingCount() const;
   CFramingInfo* getBaseFramingInfo(int framingIndex) const;
   virtual int findNewFramingNumber() const;

   virtual int setFramingInfo(int framingIndex, CFramingInfo *newFramingInfo);

   virtual int readFramingInfoListSection(CIniFile *iniFile,
                                       const string &framingInfoListSectionName,
                                       const string &framingInfoSectionPrefix);
   virtual int writeFramingInfoListSection(CIniFile *iniFile,
                                       const string &framingInfoListSectionName,
                                       const string &framingInfoSectionPrefix);

   // Version clip framelist selector hack
   virtual int getMainFrameListIndex();// readFramingInfoListSection() first!
   virtual void setAndWriteMainFrameListIndex(CIniFile *iniFile,
                                               int newMainFrameListIndex);
   virtual int getLockedFrameListIndex();// readFramingInfoListSection() first!
   virtual void setAndWriteLockedFrameListIndex(CIniFile *iniFile,
                                               int newLockedFrameListIndex);

private:
   typedef vector<CFramingInfo*> FramingInfoList;
   FramingInfoList framingInfoList;

protected:
   virtual CFramingInfo* createFramingInfo(int newFramingType);
   virtual CFramingInfo* createDerivedFramingInfo(int newFramingNumber,
                                                   int newFramingType) = 0;

   void addFramingInfo(CFramingInfo *newFramingInfo);
   void deleteFramingInfoList();

private:
   static string videoFramesTrackKey;
   static string filmFramesTrackKey;
   static string maxCustomFramesTracksKey;
   static string customFramesTrackKey;
};
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CFrameList
{
public:
   CFrameList(CFramingInfo *newFramingInfo, CTrack *newParentTrack);
   CFrameList(CFramingInfo *newFramingInfo, CTrack *newParentTrack,
               MTI_UINT32 reserveFrameCount);
   virtual ~CFrameList();

   // Accessors
   virtual bool doesMediaStorageExist(int frameNumber);
   virtual CFrame* getBaseFrame(int frameNumber);
   EClipVersion getClipVer() const;
   virtual string getDescription() const;
   virtual int getDefaultAAFrameIndex();
   virtual EVideoFieldRate getFieldRateEnum() const;
   virtual string getFrameListFileName() const;       // Without file extension
   virtual string getNewFrameListFileName(string newClipName) const;
   virtual string getFrameListFileNameWithExt() const; // With file extension
   virtual EFrameRate getFrameRateEnum() const;
   virtual MTI_REAL32 getFrameRate() const;
   virtual int getFramingType() const;
   virtual int getInFrameIndex() const;
   virtual void getInTimecode(CTimecode &tc) const;
   virtual CTimecode getInTimecode() const;
   virtual int getMaxTotalFieldCount(bool forceRecalc = false);
   virtual int getMediaAllocatedFrameCount(bool forceRecalc = false);
   virtual int getMediaAllocatedFrameCountCached();
   virtual int getMediaAllocatedFrontier(bool forceRecalc = false);
   virtual int GetMediaLocationsAllFields(int frameIndex,
                                     vector<CMediaLocation> &mediaLocationList);
   virtual int getOutFrameIndex() const;
   virtual CTimecode getOutTimecode() const;
   virtual CTrack* getParentTrack() const;
   virtual bool getSaveRecordFlag() const;
   virtual int getTotalFrameCount();               // Count includes handles
   virtual int getTotalFrameCountSpecial(int AFrameIndex);
   virtual ETrackType getTrackType() const;
   virtual int getUserFrameCount() const;    // frames between in and out, without handles
   virtual int getDirtyFrameCount();         // frames that have been writtin in version clip
   virtual bool isLocked();

   // Timecode <=> Frame Index Conversion
   virtual int getFrameIndex(const CTimecode& tv);
   virtual int getFrameIndex(int hours, int minutes,
                             int seconds, int frames);
   int getFrameIndexSpecial(const CTimecode &callersTC);
   int getFrameIndexSpecial(int hours, int minutes,
                             int seconds, int frames);
   virtual void getTimecodeForFrameIndex(int frameIndex, int* hours,
                                         int* minutes, int* seconds,
                                         int* frames);
   virtual void getTimecodeForFrameIndex(int frameIndex, int* hours,
                                         int* minutes, int* seconds,
                                         int* frames, int* dropframe);
   virtual CTimecode getTimecodeForFrameIndexSpecial(int newFrameIndex,
                                                     int newFramesPerSecond,
                                                     bool dropFrame);
   virtual CTimecode getTimecodeForFrameIndex(int frameIndex);


   virtual bool isFrameIndexAHandle(int frameIndex);
   virtual bool isFrameIndexUserMaterial(int frameIndex) const;
   virtual bool isFrameIndexValid(int frameIndex);

   bool isFrameListAvailable() const;

   virtual bool isMediaWritten(int frameNumber);

   virtual int PeekAtMediaLocation(int frameIndex, int fieldIndex,
                                   string &filename, MTI_INT64 &offset);

   virtual int setMediaWritten(int frameNumber, bool newFlag);
   virtual int setMediaWritten(int startFrameNumber, int endFrameNumber,
                               bool newFlag);

   static bool checkTrackType(ETrackType newTrackType);

   int assignMediaStorage(CMediaAllocator &mediaAllocator,
                          int startFrame, int frameCount);
   void assignVirtualMedia(MTI_UINT32 newDataSize);
   void assignVirtualMedia(int startFrame, int frameCount,
                           MTI_UINT32 newDataSize);

   virtual int readFrameListInfoSection(CIniFile *iniFile,
                                        const string& sectionPrefix,
                                        const CTimecode& timecodePrototype);
   virtual int writeFrameListInfoSection(CIniFile *iniFile,
                                        const string& sectionPrefix);

   virtual int preLoadFiles();
   virtual int reloadFieldList();

   virtual int readFrameListFile() = 0;
   virtual int writeFrameListFile() = 0;

   virtual int renameFrameListFile(string newClipName) = 0;

   virtual string makeFrameListFileName() const = 0;
   virtual string makeNewFrameListFileName(string newClipName) const = 0;

   virtual bool isFileNewerThanFrameListFile(const string& fileName) const;
   virtual int FileDateCompare(const string& fileName0, const string &fileName1) const;
   virtual bool isFrameListFileWritable() const;

   // Read & Write Media
   virtual int readMediaAllFields(int frameNumber, MTI_UINT8 *buffer[]);
	virtual int readMediaAllFieldsOptimized(int frameNumber, MTI_UINT8 *buffer[]);
	virtual int readMediaAllFieldsOptimizedMT(int frameNumber, MTI_UINT8 *buffer[], int fileHandleIndex);
	virtual int readMediaVisibleFields(int frameNumber, MTI_UINT8 *buffer[]);
	virtual int readMediaVisibleFieldsOptimized(int frameNumber, MTI_UINT8 *buffer[]);

	virtual int writeMediaAllFields(int frameNumber, MTI_UINT8 *buffer[]);
	virtual int writeMediaAllFieldsOptimized(int frameNumber, MTI_UINT8 *buffer[]);
	virtual int writeMediaFilmFramesOptimized(int frameNumber, MTI_UINT8* buffer[]);
	virtual int writeMediaVisibleFields(int frameNumber, MTI_UINT8 *buffer[]);
	virtual int writeMediaVisibleFieldsOptimized(int frameNumber, MTI_UINT8 *buffer[]);
	virtual int writeMediaWithDPXHeaderCopyHack(int frameNumber, MTI_UINT8 *buffer[], const string &sourceImageFilePath);

   //** Version clip hackery
   virtual int moveMediaForOneField(CField *sourceField,
                                    int destFrameIndex,
                                    int destFieldIndex);
   virtual int copyMediaForOneField(CField *sourceField,
                                    int destFrameIndex,
                                    int destFieldIndex);
   virtual int changeFieldMediaLocation(const CMediaLocation &oldMediaLocation,
                                        const CMediaLocation &newMediaLocation);

   int assignDirtyMediaStorage(int frameNumber,
                               DirtyMediaAssignmentUndoInfo &undoInfo);
   int unassignDirtyMediaStorage(int frameNumber,
                               DirtyMediaAssignmentUndoInfo &undoInfo);
   //**//

   virtual void initMediaBuffer(MTI_UINT8* buffer[]) = 0;

   virtual void addFrame(CFrame* frame);
   virtual void frameListReserve(MTI_UINT32 size);

   static string getFrameListFileExtension();

   virtual CFrame* createFrame(int fieldCount, CMediaFormat* mediaFormat,
                                const CTimecode& timecode) = 0;

   int createVideoFrames_FrameList();
   int createFilmFrames_FrameList(CFrameList *videoFramesFrameList,
                                  EPulldown pulldown, EFrameLabel *frameLabels);
   int createFrameListFromClip(CFrameList *dstVideoFramesFrameList,
                               CFrameList *srcFrameList,
                               int srcStartFrameIndex, int srcFrameCount);
   int createFrameListFromDiffFile(const string& diffFileName,
                                   EPulldown pulldown);
   int createFrameListFromCadenceFile(const string& cadenceFileName,
                                   bool do525FieldOrder);
   int createVideoFields_FrameList(CFrameList *videoFramesFrameList,
                                   bool do525FieldOrder);
   int createVideoFields_FrameList(bool do525FieldOrder);
   int copyVideoFields_FrameList(CFrameList *videoFramesFrameList);
   int copyVideoFields_FrameList();
   int extendVideoFrames_FrameList(int newFrameCount);
   int extendFilmFrames_FrameList(CFrameList *videoFramesFrameList,
                                  int startFrame, int frameCount,
                                  EPulldown pulldown,
                                  EFrameLabel frameLabels[]);

   int initVideoFields_FrameList();
   void deleteFrameList();
   int truncateFrameList(int startFrame);

   static void create3_2PulldownFrameLabels(int inFrameIndex, int outFrameIndex,
                                            int AAFrameIndex,
                                            EPulldown pulldown,
                                            EFrameLabel frameLabelArray[]);
   static void convertFrameLabels(int labelCount, int iFrameLabelArray[],
                                  EFrameLabel eFrameLabelArray[]);

   virtual void dump(MTIostringstream& str);      // for debugging

   string getImageFilePath(CFrame *frame);
   string getHistoryFilePath(CFrame *frame);
   string getMetadataDirectory();

protected:
   virtual int searchForTimecode(const CTimecode &callersTC,
                                 int trialFrameIndex);

protected:
   CFramingInfo *framingInfo;
   CTrack *parentTrack;

   bool frameListAvailable;

   CTimecode frame0Timecode;
   CTimecode inTimecode;
   CTimecode outTimecode;
   int inFrameIndex;
   int outFrameIndex;

private:
   typedef vector<CFrame*> FrameList;

   FrameList frameList;        // List of frames in this clip

   int maxTotalFieldCount;     // Maximum number of fields (visible + hidden)
                               // on any frame.  Set by getMaxTotalFieldCount

   int mediaAllocatedFrameCount; // Count of frames in the frame list where
                                 // media has been allocated.  Set by
                                 // getMediaAllocatedFrameCount
   int mediaAllocatedFrontier;   // Last frame in frame list that has media
                                 // storage allocated. -1 if no media allocated
                                 // and totalFrameCount-1 if all media allocated

private:
   void remove3_2SwappedFieldPulldown(SFilmFrameSource& filmFrameSrc);
   void remove3_2FieldPulldown(SFilmFrameSource& filmFrameSrc);
   void remove3_2FramePulldown(SFilmFrameSource& filmFrameSrc);
   void fixPALFieldDominance(SFilmFrameSource& filmFrameSrc);

	//** Another day, another hack - this one is to get timecodes into DPX hdrs
	void doDPXHeaderTimecodeHack(CFrame *frame);
   //**//

private:
   static string frameListFileExtension;

   static string frame0TimecodeKey;
   static string inTimecodeKey;
   static string outTimecodeKey;
   static string inFrameIndexKey;
   static string outFrameIndexKey;
   static string mediaAllocatedFrameCountKey;
}; // class CFrameList
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

// Default value for maximum track index.  This number can be anything
// and the number of Tracks can be adjusted dynamically
#define MAX_TRACK_INDEX_DEFAULT   4

// Indicator that the maxTrackIndex in CTrackList has not been
// set yet
#define MAX_TRACK_INDEX_NOT_SET   (-1)

// Track Number that indicates there is no track at this list position
#define TRACK_NUMBER_NO_TRACK     (-1)

//////////////////////////////////////////////////////////////////////

// Record-to-Computer and Record-to-Video Status
enum ERecordStatus
{
   RECORD_STATUS_INVALID = 0,
   RECORD_STATUS_NOT_RECORDED,   // Not recorded
   RECORD_STATUS_PART_RECORDED,  // Record failed part way in
   RECORD_STATUS_RECORDED,       // Recorded
   RECORD_STATUS_SCRATCH,        // Can be recorded at will
   RECORD_STATUS_LOCKED          // Locked, cannot be changed
};

//////////////////////////////////////////////////////////////////////

enum EMediaStatus
{
   MEDIA_STATUS_UNKNOWN = 0,               // Unknown clip status
   MEDIA_STATUS_MARKED,                    // A clip that does not yet occupy
                                           // disk space
   MEDIA_STATUS_CREATED,                   // A clip that has been created,
                                           // but has not yet been recorded in
   MEDIA_STATUS_RECORD_TO_COMPUTER_PARTIAL,// A clip that has been partially
                                           // recorded in by the Edit Controller
   MEDIA_STATUS_RECORD_TO_COMPUTER,        // A clip that has been completely
                                           // recorded in by the Edit Controller
   MEDIA_STATUS_MODIFIED,                  // A clip that has been modified by
                                           // DRS or another tool
   MEDIA_STATUS_RECORD_TO_VTR_PARTIAL,     // A clip that has been partially
                                           // recorded back to the VTR by the
                                           // Edit Controller
   MEDIA_STATUS_RECORD_TO_VTR,             // A clip that has been completely
                                           // recorded back to the VTR by the
                                           // Edit Controller
   MEDIA_STATUS_RETIRED,                   // A clip that cannot be directly
                                           // accessed by the operator.  The
                                           // media storage used by this clip
                                           // has not yet been recorded over.
                                           // It is possible to recover a
                                           // retired clip
   MEDIA_STATUS_DELETED,                   // A clip that no longer has any
                                           // media storage.  A deleted clip
                                           // cannot be recovered.
   MEDIA_STATUS_CADENCE_REPAIR,            // A clip that has been through the
                                           // cadence repair tool.
   MEDIA_STATUS_ALPHA_HIDDEN,              // A clip that whose alpha matte has
                                           // been hidden
   MEDIA_STATUS_ALPHA_UNHIDDEN,            // A clip that whose alpha matte has
                                           // been unhidden
	MEDIA_STATUS_ALPHA_DESTROYED,           // A clip that whose alpha matte has
                                           // been destroyed
	MEDIA_STATUS_FPS_CHANGED,               // A clip that whose frame rate has
	MEDIA_STATUS_FPS_CHANGED_TO_2398,       // been changed.
	MEDIA_STATUS_FPS_CHANGED_TO_24,
	MEDIA_STATUS_FPS_CHANGED_TO_25,
	MEDIA_STATUS_FPS_CHANGED_TO_2997,
	MEDIA_STATUS_FPS_CHANGED_TO_30,

   MEDIA_STATUS_ARCHIVED,                  // A clip that has been archived
   MEDIA_STATUS_CLONED                     // A clip that was created by
                                           // cloning another clip
};

// Note: The numeric order of the about enum constants is used to
//       sort the status in the Bin Manager GUI.  Be careful when
//       adding, deleting or rearranging entries


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CTrack
{
public:
   CTrack(ClipSharedPtr &newParentClip, EClipVersion newClipVer, int newTrackNumber);
   virtual ~CTrack();

   void addFrameList(CFrameList *newFrameList);
   int createFieldList();
   int extendFieldList(int newFrameCount);
   int createFrameList_List(const CFramingInfoList& framingInfoList);
   virtual CFrameList *createFrameList(CFramingInfo *newFramingInfo) = 0;

   string getAppId() const;
   CFrameList* getBaseFrameList(int framingIndex) const;
   EClipVersion getClipVer() const;
   int setFrameList(int framingIndex, CFrameList *newFrameList);
   CFieldList* getFieldList() const;
   virtual int getFrameListFileNames(vector<string> &fileNameList) const;
   int getFramingCount() const;
   int getHandleCount() const;
   EMediaStatus getLastMediaAction() const;
   string getLastMediaActionString() const;
   int setCadenceRepairFlag (CIniFile *clipFile, const string &sectionPrefix,
                             bool bArg);
   bool getCadenceRepairFlag ();
   const CMediaFormat* getMediaFormat() const;
   const CMediaStorageList* getMediaStorageList() const;
   ClipSharedPtr getParentClip() const;
   ERecordStatus getRecordToComputerStatus() const;
   ERecordStatus getRecordToVTRStatus() const;
   C5MediaTrackHandle GetTrackHandle();
   int getTrackNumber() const;
   ETrackType getTrackType() const;
   bool getVirtualMediaFlag() const {return virtualMediaFlag;};

   virtual int PeekAtMediaAllocation(vector<SMediaAllocation> &mediaAllocList,
                                     volatile int* currentFrame,
                                     volatile bool* stopRequest);

   void setAppId(const string& newAppId);
   void setClip5TrackHandle(const C5MediaTrackHandle &newTrackHandle);
   void setClipVer(EClipVersion newClipVer);
   void setHandleCount(int newHandleCount);
   void setLastMediaAction(EMediaStatus newLastMediaAction);

   void setMediaModifierStatus();
   void setArchivedStatus();
   void setCreatedStatus();
   void setClonedStatus();
   void setRecordToComputerStatus(ERecordStatus newRecordStatus);
   void setRecordToVTRStatus(ERecordStatus newRecordStatus);
   void setVirtualMediaFlag(bool newVirtualMediaFlag)
      {virtualMediaFlag = newVirtualMediaFlag;};

   int assignMediaStorage(CMediaAllocator& mediaAllocator, int startFrame = 0,
                          int frameCount = -1);
   int extendMediaStorage(int startFrame, int frameCount);
   int checkExtendMediaStorage(int frameCount);
   void addDirtyMediaStorage(EMediaType mediaType,
                             const string& mediaIdentifier,
                             const string& historyDirectory,
									  CMediaAccess* mediaAccess);
   CMediaAllocator *getDirtyMediaStorageAllocator(CFrame &frame,
                                                  int frameCount,
                                                  int &status);
   int assignDirtyMediaStorage(CFrameList &frameList, int frameIndex,
                               DirtyMediaAssignmentUndoInfo &undoInfo);
   int unassignDirtyMediaStorage(CFrameList &frameList, int frameIndex,
                                 DirtyMediaAssignmentUndoInfo &undoInfo);
   int deallocateMediaStorage(CMediaDeallocatorList& mediaDeallocatorList);
   int assignVirtualMedia(MTI_UINT32 newDataSize);
   int assignVirtualMedia(int startFrame, int frameCount,
                           MTI_UINT32 newDataSize);
   int releaseMediaStorage(int startFrame, int frameCount);

   virtual int renameClipMedia(string newBinName, string newClipName);

   int writeFrameListFile(int trackIndex);
   int writeAllFrameListFiles();

   int renameFrameListFile(int framingIndex, string newClipName);
   int renameAllFrameListFiles(string newClipName);

   int preLoadFieldList(); // force read of field list file
   int reloadFieldList(); // force read of field list file
   int updateFieldListFile();
   int updateFieldListFile(int firstFieldIndex, int fieldCount,
                           char *externalBuffer,
                           unsigned int externalBufferSize);
   int UpdateMediaStatusTrackFile();

   virtual int CreateNewFileHandle();
   virtual int ReleaseFileHandle(int fileHandleIndex);

   virtual int readTrackInfoSection(CIniFile *clipFile,
                                    const string& sectionName);
   static EClipVersion readClipVer(CIniFile *clipFile,
                                   const string& sectionName);
   virtual int writeTrackInfoSection(CIniFile *clipFile,
                                     const string& sectionName);

   virtual int readTrackSections(CIniFile *clipFile,
                                 const string& sectionPrefix,
                                 CFramingInfoList& framingInfoList);
   virtual int refreshTrackSections(CIniFile *clipFile,
                                    const string& sectionPrefix);
   virtual int refreshMediaStorageSection(CIniFile *clipFile, const string& sectionPrefix);
   virtual int writeTrackSections(CIniFile *clipFile,
                                  const string& sectionPrefix);

   virtual int readFrameListSections(CIniFile *clipFile,
                                     const string& sectionPrefix,
                                     const CFramingInfoList& framingInfoList);
   virtual int writeFrameListSections(CIniFile *clipFile,
                                      const string& sectionPrefix);

   static EMediaStatus queryMediaStatus(const string& mediaStatusName);
   static string queryMediaStatusName(EMediaStatus mediaStatus);
   static int compareMediaStatus(EMediaStatus left, EMediaStatus right);
   static ERecordStatus queryRecordStatus(const string& recordStatusName);
   static string queryRecordStatusName(ERecordStatus recordStatus);

protected:
   EClipVersion clipVer;
   C5MediaTrackHandle clip5TrackHandle;  // If this Audio Track or Video Proxy
                                    // is a clip 5 track, then the clip5TrackHandle
                                    // is initialized to point to the clip 5 track

   ClipWeakPtr _parentClip;     // Clip that owns this Video Proxy

   string appId;    // arbitrary string that can be used by applications to
                    // identify the purpose of the track

   int trackNumber;        // Video Proxy Number or Audio Track Number

   int handleCount;        // Number of "handle" frames at the beginning and
                           // end of the track

   CMediaFormat *mediaFormat;     // Image Format or Audio Format

   CMediaStorageList mediaStorageList;    // Media Storage List

   ERecordStatus recordToComputerStatus;
   ERecordStatus recordToVTRStatus;
   EMediaStatus lastMediaAction;
   bool cadenceRepairFlag;

   bool virtualMediaFlag;     // This track was created to hold virtual
                              // media
   CFieldList *fieldList;

   void syncVideoFramingFrameListFrameFields(int frameIndex);
   void syncVideo12FrameListFrameField(CFrame *srcFrame,
                                       int srcFrameIndex,
                                       int srcFieldIndex);
   void syncVideo1FrameListFrameField(CFrame *srcFrame,
                                      int srcFrameIndex);
   void syncVideo2FrameListFrameField(CFrame *srcFrame,
                                      int srcFrameIndex);

private:
   // List of Frame Lists: Video-Frames, Film-Frames and Custom-Frames
   typedef vector<CFrameList*> FrameList_List;

   FrameList_List frameList_List;     // List of Frame Lists

private:
   void deleteFrameList_List();

   int extendMediaStorage_ClipVer3(int startFrame, int frameCount);
   int extendMediaStorage_ClipVer5L(int startFrame, int frameCount);
   int releaseMediaStorage_ClipVer3(int startFrame, int frameCount);
   int releaseMediaStorage_ClipVer5L(int startFrame, int frameCount);

   int deallocateMediaStorage_ClipVer3(CMediaDeallocatorList& mediaDeallocatorList);
   int deallocateMediaStorage_ClipVer5L(CMediaDeallocatorList& mediaDeallocatorList);

   int PeekAtMediaAllocation_ClipVer3(vector<SMediaAllocation> &mediaAllocList,
                                      volatile int* currentFrame,
                                      volatile bool* stopRequest);

private:
   static string trackSectionName;

   static string clipVerKey;
   static string trackIDKey;
   static string handleCountKey;
   static string recordToComputerStatusKey;
   static string recordToVTRStatusKey;
   static string lastMediaActionKey;
   static string cadenceRepairFlagKey;
   static string virtualMediaKey;
   static string appIdKey;

}; // class CTrack

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CTrackList
{
public:
   CTrackList();
   virtual ~CTrackList();

   virtual CTrack* createDerivedTrack(ClipSharedPtr &newParentClip,
                                      EClipVersion newClipVer,
                                      int newTrackNumber) = 0;

   CTrack* getBaseTrack(int trackIndex) const;
   int getTrackCount() const;

   CTrack* findBaseTrack(const string& appId) const;
   int findBaseTrackIndex(const string& appId) const;

   int readTrackListSection(CIniFile *iniFile, const string& sectionPrefix,
                            ClipSharedPtr &parentClip,
                            CFramingInfoList& framingInfoList);
   int refreshTrackListSection(CIniFile *iniFile, const string& sectionPrefix);
   int writeTrackListSection(CIniFile *iniFile, const string& sectionPrefix);

protected:
   void addTrack(CTrack *newTrack);
   int getNewTrackNumber() const;

private:
   typedef vector<CTrack*> TrackList;
   TrackList trackList;

private:
   void deleteTrackList();

private:
   static string maxTrackIndexKey;

}; // class CTrackList
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

// Field List Flags

// bool isMediaWriteProtected() const;
// void setMediaWriteProtected(bool newFlag);
//    false - It's OK to overwrite the media for this field
//    true - The field media may not be written to
//  Indicates that we are not allowed to write to the media for this
//  field. Typically this is a clone of another clip, and the Virtual
//  Media flag will also be set for this field. If the field gets
//  modified, we need to write the changes to alternate media (sort
//  of like "copy on write" operation).
//  Bit-Mask Constant: FIELD_LIST_WRITE_PROTECT

// bool isMediaDirty() const;
// void setMediaDirty(bool newFlag);
//    false - Field was never write protected or wasn't written to
//    true - Field used to be marked "write protected", but was written to
//  Indicates that we wrote to a field that was previously marked as being
//  write protected. So we wrote the field to an alternate storage location
//  and set this flag.
//  Bit-Mask Constant: FIELD_LIST_DIRTY_MEDIA

// bool isMediaBorrowed() const;
// void setMediaBorrowed(bool newFlag);
//    false - Field media belongs to this clip
//    true - Field media belongs to some other clip - DON'T DESTROY IT
//  This flag is identical in meaning to the "VirtualMedia" flag that
//  is kept elsewhere -- part of why I defined a new flag for clip-to-clip
//  is because I don't understand the logistics of saving and restoring
//  to virtual media flag. Also the virtual media flag is in the wrong place,
//  it really should be down at this level, but I am reluctant to move it
//  because I'm afraid to mess up existing clips (does anyone use virtual
//  clips?)
//  Bit-Mask Constant: FIELD_LIST_DIRTY_MEDIA

#define FIELD_LIST_MEDIA_EXISTS    0x0001 // Media storage exists, i.e.,
                                          // the Media Location points
                                          // to existing media storage such
                                          // an allocated raw disk
                                          // or existing Image File
#define FIELD_LIST_MEDIA_WRITTEN   0x0002 // Media data has been written
                                          // Media has been written at least
                                          // once by writeMedia.
#define FIELD_LIST_WRITE_PROTECT   0x0004 // Media data belongs to someone
                                          // else and we are not allowed to
                                          // write to it or delete it
#define FIELD_LIST_MEDIA_IS_DIRTY  0x0008 // This field, which was originally
                                          // marked "write protected", was
                                          // written to, so frame is "dirty"
                                          // relative to the original clip
#define FIELD_LIST_MEDIA_IS_BORROWED  0x0010 // We don't own the media, and
                                          // so we are not allowed to destroy
                                          // it (but we are allowed to modify
                                          // it if it's not also write-
                                          // protected)

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CFieldListEntry
{
public:
   CFieldListEntry();
   virtual ~CFieldListEntry();

   bool doesMediaStorageExist() const;
   int getDefaultMediaType() const;
   int getFieldIndex() const;
   MTI_UINT32 getFlags() const;
   int getFrameIndex() const;
   bool getNeedsUpdate() const;
   bool isMediaBorrowed() const;
   bool isMediaDirty() const;
   bool isMediaWriteProtected() const;
   bool wasMediaWritten() const;

   void setDefaultMediaType(int newType);
   void setFlags(MTI_UINT32 newFlags);
   void setMediaStorageExists(bool newMediaExists);
   void setMediaBorrowed(bool newMediaBorrowed);
   void setMediaDirty(bool newMediaDirty);
   void setMediaWriteProtected(bool newWriteProtect);
   void setMediaWritten(bool newMediaWritten);
   void setNeedsUpdate(bool newNeedsUpdate);
   void setFieldIndex(int newIndex);
   void setFrameIndex(int newIndex);

	void dump(MTIostringstream& str);

private:
   bool needsUpdate;

   MTI_UINT32 flags;

   int defaultMediaType;

   int frameIndex;      // Indices of frame and field in the video-frames frame
   int fieldIndex;      // list that uses this Field List Entry

};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CFieldList
{
public:
   CFieldList(CTrack *newParentTrack);
   virtual ~CFieldList();

   CFieldListEntry *getField(int fieldIndex);
   int getFieldCount();
   static string getFieldListFileExtension();
   string getFieldListFileName() const;
   string getFieldListFileNameWithExt() const;
   CTrack *getParentTrack() const;

   bool isFieldListAvailable() const;

   int preLoadFieldList(); // force read of field list file
   int reloadFieldList();  // refresh field list from field list file

   void setVideoFrameListBackReference(int fieldListEntryIndex, int frameIndex,
                                       int fieldIndex);

   void addField(CFieldListEntry *field);
   int createFieldList();
   int extendFieldList(int newFrameCount);

   int readFieldListFile();
   int reloadFieldListFile();
   int writeFieldListFile();
   int updateFieldListFile();
   int updateFieldListFile(int firstFieldIndex, int fieldCount,
                           char *externalBuffer,
                           unsigned int externalBufferSize);

private:
   void deleteFieldList();
   string makeFieldListFileName() const;

private:
   CTrack *parentTrack;

   typedef vector<CFieldListEntry*> FieldList;
   FieldList fieldList;

   bool fieldListAvailable;    // Has field list been read from file

};

#ifdef PRIVATE_STATIC
#ifndef _CLIP_3_TRACK_DATA_CPP
namespace Clip3TrackData {
   extern string videoFramesTrackKey;
   extern string filmFramesTrackKey;
   extern string maxCustomFramesTracksKey;
   extern string customFramesTrackKey;

   extern string trackTypeKey;
   extern string framingTypeKey;
   extern string descriptionKey;
   extern string frameRateKey;

   extern string frameListFileExtension;

   extern string frame0TimecodeKey;
   extern string inTimecodeKey;
   extern string outTimecodeKey;
   extern string inFrameIndexKey;
   extern string outFrameIndexKey;
   extern string mediaAllocatedFrameCountKey;

   extern string handleCountKey;
   extern string recordToComputerStatusKey;
   extern string recordToVTRStatusKey;
   extern string lastMediaActionKey;
   extern string virtualMediaKey;
   extern string appIdKey;

   extern string maxTrackIndexKey;
};
#endif
#endif

//////////////////////////////////////////////////////////////////////

int MTI_CLIPLIB_API Get3_2PulldownFrameLabel(int frameIndex, int AAFrameIndex);
int MTI_CLIPLIB_API Get4FrameLabel(int frameIndex, int AAFrameIndex);
CTimecode MTI_CLIPLIB_API GetWholeHourTimecode(const CTimecode &timecode);
//int MTI_CLIPLIB_API TranslateFrameIndex24To30(int frameIndex);
int MTI_CLIPLIB_API TranslateFrameIndex24To30(int frameIndex, int AAFrameIndex);
//int MTI_CLIPLIB_API TranslateFrameIndex30To24(int frameIndex);
int MTI_CLIPLIB_API TranslateFrameIndex30To24(int frameIndex, int AAFrameIndex);

//////////////////////////////////////////////////////////////////////



#endif // #ifndef Clip3TrackH
