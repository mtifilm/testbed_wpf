#ifndef CBRIDGE_H
#define CBRIDGE_H

#include "machine.h"
#include "ClipSharedPtr.h"

#include <cstdio> // needed for FILE

class CClip;
class CImageFormat;
class CTimecode;

//--------------------------------------------------------------------------
// content of first two lines of BIN file 
struct BINHEADER
{
   int Clip;

   int NClip;

   unsigned int Crypt;

   int BinID;

   char BinName[48];

   char Month[4];

   char Day[4];

   char Year[6];

   int RecToComOpt[4];

   int Reserved[4];
};

//-------------------------------------------------------------------------
// contents of a CLIP entry in a BIN file
struct CLIPRECORD
{
   // LINE 1--------------------------
   int HoursIn,
       MinutesIn,
       SecondsIn,
       FramesIn;

   int HoursOut,
       MinutesOut,
       SecondsOut,
       FramesOut;

#define IF_NORMAL_FORMAT_MASK 0xFFFF
#define IF_D16_YUV_FRAME 0x300
#define IF_D4_YUV_FRAME  0x200
   int Format;

#define CS_MARKED  1
   int Status;

   int Demo;

   int ClipID;

   // LINE 2--------------------------
   int Fix;

   int Secs;

   char Month[4];

   char Day[4];

   char Year[6];

   // LINE 3--------------------------
   int NItem;

   int Location;

#define BLOCKS_PER_FIELD_VERTICAL_INTERVAL         45 
   int ItemSize;

   int ClipSize;

   // LINE 4--------------------------
   int Bits;

#define PIXELVALS_YUV_422_10BIT_2_5                11
   int PixelVals;

   int Stride;

   int RowReversed;

   // LINE 5--------------------------
   int ValScale;

   int NumPixelPerRow;

   int NumRowsPerItem;

#define RECINFO_STORAGE_MEDIA_MASK                 0xF0
#define RECINFO_KEEP_VERTICAL_INTERVAL             0x02
#define RECINFO_10BIT_NO_GAP                       0x08
#define RECINFO_STORAGE_MEDIA_FILE_SYSTEM_AS_IMAGE 0x10
#define RECINFO_STORAGE_MEDIA_FILE_SYSTEM_AS_VIDEO 0x20
   int RecInfo;

   // LINE 6---------------------------

   int HoursMarkIn,
       MinutesMarkIn,
       SecondsMarkIn,
       FramesMarkIn;

   int HoursMarkOut,
       MinutesMarkOut,
       SecondsMarkOut,
       FramesMarkOut;

#define OGLV_BRIDGE_DESCRIPTION_LENGTH 128 
    // LINE 7--------------------------
    char Name[OGLV_BRIDGE_DESCRIPTION_LENGTH];
};

//-------------------------------------------------------------------------
// one block of raw disk storage 
struct FREEBLOCK
{
   int beg;

   int end;

   int siz;

   FREEBLOCK *nxt;
};

//------------------------------------------------------------------------
class CBridge
{

public:

   CBridge();

   ~CBridge();

   // open the OGLV bridge interface
   int openOGLV();

   // get the raw video directory
   char *getRawVideoDirectory();

   // get the fast IO boundary value
   int getFastIOBoundary();

   // get total free bytes
   int getTotalFreeBlocks();

   // get max contiguous free bytes
   int getMaxContiguousFreeBlocks();

   // allocate storage for a new clip
   int allocateClipBlocks(int);

   // append a new clip to a bin 
   int appendClip(int, ClipSharedPtr &);

   // create Flist from clip
   int createFlistFromCPMPClip(char *, ClipSharedPtr &);

   // conform an existing Flist based on the
   // latest version of the original CPMP clip
   int conformExistingFlist(ClipSharedPtr &inclp);

   // deallocate storage (fm an old clip)
   int deallocateClipBlocks(int, int);

   // delete an existing clip
   int deleteClip(ClipSharedPtr &);

   // append a new bin to OGLV
   int appendBin(char *nubinname);

   // get the name of the ith bin (i=0,1,2..)
   char *getBinName(int binidx);

   // get the bin index given the name
   int getBinIndex(char *);

   // delete an existing bin
   int deleteBin(int binidx); 

   // close the OGLV bridge interface
   int closeOGLV();

   // make this public for writing a support utility
   unsigned int encrypt(unsigned int);

/////////////////////////////////////////////////////////////////
//
//		E R R O R   C O D E S
//

#define OGLV_BRIDGE_ERR_BAD_BIN_ROOT_PATHNAME                    -9
#define OGLV_BRIDGE_ERR_BIN_HEADER_WRITE_FAILURE		-10
#define OGLV_BRIDGE_ERR_BIN_ENTRY_WRITE_FAILURE			-11
#define OGLV_BRIDGE_ERR_BIN_FILE_IMPROPERLY_TERMINATED		-12
#define OGLV_BRIDGE_ERR_BIN_FILE_OPEN_FAILURE			-13
#define OGLV_BRIDGE_ERR_BIN_HEADER_READ_FAILURE			-14
#define OGLV_BRIDGE_ERR_BRIDGE_NOT_OPEN				-15
#define OGLV_BRIDGE_ERR_CLIP_ENTRY_READ_FAILURE			-16
#define OGLV_BRIDGE_ERR_CLIP_ENTRY_WRITE_FAILURE		-17
#define OGLV_BRIDGE_ERR_CLIP_NOT_FOUND_IN_ANY_BIN		-18
#define OGLV_BRIDGE_ERR_CONSISTENCY_FAILURE			-19
#define OGLV_BRIDGE_ERR_FAILURE_BUILDING_CLIP_RECORD		-20
#define OGLV_BRIDGE_ERR_FAILURE_CREATING_FLIST			-21
#define OGLV_BRIDGE_ERR_FAILURE_MAKING_SCRATCH_DIRS             -22
#define OGLV_BRIDGE_ERR_FAILURE_REMOVING_SCRATCH_DIRS           -23
#define OGLV_BRIDGE_ERR_FAILURE_MAKING_CADENCE_DIR              -24
#define OGLV_BRIDGE_ERR_FAILURE_REMOVING_CADENCE_DIR            -25
#define OGLV_BRIDGE_ERR_FAILURE_GETTING_BIN_PATHNAME		-26
#define OGLV_BRIDGE_ERR_FAILURE_READING_1ST_LINE_OF_MTI_MASTER	-27
#define OGLV_BRIDGE_ERR_FAILURE_READING_2ND_LINE_OF_MTI_MASTER	-28
#define OGLV_BRIDGE_ERR_FAILURE_WRITING_1ST_LINE_OF_MTI_MASTER	-29
#define OGLV_BRIDGE_ERR_FAILURE_WRITING_2ND_LINE_OF_MTI_MASTER	-30
#define OGLV_BRIDGE_ERR_FLIST_DELETE_FAILURE			-31
#define OGLV_BRIDGE_ERR_INSUFFICIENT_RETIRED_CLIP_BLOCKS	-32
#define OGLV_BRIDGE_ERR_INVALID_BIN                             -33
#define OGLV_BRIDGE_ERR_MEMORY_MISMATCH				-34
#define OGLV_BRIDGE_ERR_MTI_DEFAULTS_OPEN_FAILURE		-35
#define OGLV_BRIDGE_ERR_MTI_MASTER_OPEN_FAILURE			-36
#define OGLV_BRIDGE_ERR_NO_DIRECTORY_HEADING_IN_MTI_DEFAULTS	-37
#define OGLV_BRIDGE_ERR_NO_ENVIRONMENT_DIR_IN_MTI_DEFAULTS	-38
#define OGLV_BRIDGE_ERR_NO_FASTIOBOUNDARY_IN_MTI_DEFAULTS	-39
#define OGLV_BRIDGE_ERR_NO_FREEBLOCK_IN_MTI_MASTER		-40
#define OGLV_BRIDGE_ERR_NO_HARDWARE_HEADING_IN_MTI_DEFAULTS	-41
#define OGLV_BRIDGE_ERR_NO_RAW_DISK_SIZE_IN_MTI_DEFAULTS	-42
#define OGLV_BRIDGE_ERR_NO_RAW_VIDEO_DIR_IN_MTI_DEFAULTS	-43
#define OGLV_BRIDGE_ERR_NO_DEFAULT_IMAGE_DIR_IN_MTI_DEFAULTS    -44
#define OGLV_BRIDGE_ERR_NO_STORAGE_ALLOCATED			-45
#define OGLV_BRIDGE_ERR_RET_CLIP_WRITE_FAILURE			-46
#define OGLV_BRIDGE_ERR_TOO_MANY_LINES_IN_MTI_MASTER		-47
#define OGLV_BRIDGE_ERR_WRK_FILE_OPEN_FAILURE			-48
#define OGLV_BRIDGE_ERR_WRK_FILE_RENAME_FAILURE			-49

private:

   int getMTIDefaults();

   int getMTIMaster();

   int getLin(FILE *, char *, int);

   const char *truncateBinName(char *);

   int getBinPathname(char *, int);

   int getBinHeader(FILE *, BINHEADER *);

   int getClipRecord(FILE *, CLIPRECORD *);

   int getBlocksOccupiedByClip(CLIPRECORD *cr);

   int convert2d(int, char *, int);

   void convertTimecode(char *, int, int, int, int);

   int getDRSFormat(const CImageFormat *, CTimecode *);

   void getCurrentDate(char *, char *, char *);

   void getExistingFlistPathname(char *, int);

   int getNewFlistPathname(char *);

   int buildClipRecordFromCPMPClip(CLIPRECORD *, ClipSharedPtr &);

   int putClipRecord(FILE *, CLIPRECORD *);

   int makeScratchDirectories(int clpid);

   int removeScratchDirectories(int clpid);

   int makeCadenceDirectory(int clpid);

   int removeCadenceDirectory(int clpid);

   int putBinHeader(FILE *, BINHEADER *);

   int putMTIMaster();

   //-------------------------------------------------------------------

#define BRIDGE_STATE_CLOSED              0
#define BRIDGE_STATE_OPEN                1
   int majorState;

#define PTHLENG 256

   // CPMP BIN ROOT PATHNAME
   char binRootPathname[PTHLENG];

   // MTI_DEFAULT CONTENTS

   char environmentDir[PTHLENG];
   char rawVideoDir[PTHLENG];
   char defaultImageDir[PTHLENG];
   int  rawDiskSize;
   int  fastIOBoundary;

   // MASTER FILE CONTENTS

   char masterPathname[PTHLENG+16];

#define MTI_MASTER_LINES 1024 
#define LINLENG 128

   int mtiMasterLines;
   char *mline[MTI_MASTER_LINES];

   int masterClipID,
       masterBinID,
       masterBin,
       masterNClipRet,
       masterTriedRem,
       masterNBin,
       masterEDL1,
       masterEDL2;

   int newMasterClipID,
       newMasterNClipRet;

#define EXTRA_FREE 1024

   int numClipsDeleted;

   int totalFreeBlocks;

   int maxContiguousFreeBlocks;

   FREEBLOCK *freeBlk;
   FREEBLOCK *freePtr;
   FREEBLOCK *freeList;

   FREEBLOCK *sortFreeBlocks(FREEBLOCK *, FREEBLOCK *);

   int newLocation; // location of newly allocated clip storage
   int newBlocks;   // blocks   of newly allocated clip storage

};

#endif

