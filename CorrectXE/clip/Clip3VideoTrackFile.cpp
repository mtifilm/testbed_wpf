// Clip3VideoTrackFile.cpp: implementation of the CVideoFrameListFileReader and
//                          CVideoFrameListFileWriter classes.
//
/* CVS Info:
$Header: /usr/local/filmroot/clip/source/Clip3VideoTrackFile.cpp,v 1.7 2007/06/30 01:29:17 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "Clip3VideoTrackFile.h"
#include "Clip3.h"
#include "Clip3VideoTrack.h"
#include "err_clip.h"
#include "FileScanner.h"
#include "MediaStorage3.h"
#include "StructuredFileStream.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CVideoFieldFileRecord::CVideoFieldFileRecord(MTI_UINT16 version)
: CFieldFileRecord(FR_RECORD_TYPE_VIDEO_FIELD_R3, version)
{

}

CVideoFieldFileRecord::~CVideoFieldFileRecord()
{
}

//////////////////////////////////////////////////////////////////////
// Read and Write functions
//////////////////////////////////////////////////////////////////////

int CVideoFieldFileRecord::read(CStructuredStream &stream)
{
   // Read the supertype instance data
   if (CFieldFileRecord::read(stream) != 0)
      return CLIP_ERROR_VIDEO_TRACK_FILE_READ_ERROR;

   // Read the reserved words
   for (int i = 0; i < VIDEO_FIELD3_RECORD_RESERVED_COUNT; ++i)
      {
      if(stream.readUInt32(&(videoFieldReserved[i])) != 0)
         return CLIP_ERROR_VIDEO_TRACK_FILE_READ_ERROR;
      }

   return 0;
}

int CVideoFieldFileRecord::write(CStructuredStream &stream) 
{
   // Write record preamble
   if (CFieldFileRecord::write(stream) != 0)
      return CLIP_ERROR_VIDEO_TRACK_FILE_WRITE_ERROR;

   // Write zeroes to reserved area
   for (int i = 0; i < VIDEO_FIELD3_RECORD_RESERVED_COUNT; ++i)
      {
      if(stream.writeUInt32(0) != 0)
         return CLIP_ERROR_VIDEO_TRACK_FILE_WRITE_ERROR;
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////

void CVideoFieldFileRecord::calculateRecordByteCount()
{
   // Total the size of all the fields in the record

   CFieldFileRecord::calculateRecordByteCount();

   MTI_UINT32 newByteCount = getRecordByteCount();

   newByteCount += sizeof(videoFieldReserved);

   // Update the object with the calculated size
   setRecordByteCount(newByteCount);
}


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CVideoFrameFileRecord::CVideoFrameFileRecord(MTI_UINT16 version)
: CFrameFileRecord(FR_RECORD_TYPE_VIDEO_FRAME_R3, version),
  imageDataFlag(II_IMAGE_DATA_FLAG_INVALID)
{

}

CVideoFrameFileRecord::~CVideoFrameFileRecord()
{
}

//////////////////////////////////////////////////////////////////////
// Read and Write functions
//////////////////////////////////////////////////////////////////////

int CVideoFrameFileRecord::read(CStructuredStream &stream)
{
   // Read the supertype instance data
   if (CFrameFileRecord::read(stream) != 0)
      return CLIP_ERROR_VIDEO_TRACK_FILE_READ_ERROR;

   // Read the imageDataFlag field
   if (stream.readUInt8(&imageDataFlag) != 0)
      return CLIP_ERROR_VIDEO_TRACK_FILE_READ_ERROR;

   // Read the reserved words
   for (int i = 0; i < VIDEO_FRAME3_RECORD_RESERVED_COUNT; ++i)
      {
      if(stream.readUInt32(&(videoFrameReserved[i])) != 0)
         return CLIP_ERROR_VIDEO_TRACK_FILE_READ_ERROR;
      }

   return 0;
}

int CVideoFrameFileRecord::write(CStructuredStream &stream) 
{
   // Write record preamble
   if (CFrameFileRecord::write(stream) != 0)
      return CLIP_ERROR_VIDEO_TRACK_FILE_WRITE_ERROR;

   // Write the imageDataFlag field
   if (stream.writeUInt8(imageDataFlag) != 0)
      return CLIP_ERROR_VIDEO_TRACK_FILE_WRITE_ERROR;

   // Write zeroes to reserved area
   for (int i = 0; i < VIDEO_FRAME3_RECORD_RESERVED_COUNT; ++i)
      {
      if(stream.writeUInt32(0) != 0)
         return CLIP_ERROR_VIDEO_TRACK_FILE_WRITE_ERROR;
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Accessor functions
//////////////////////////////////////////////////////////////////////

MTI_UINT8 CVideoFrameFileRecord::getImageDataFlag() const
{
   return imageDataFlag;
}

//////////////////////////////////////////////////////////////////////
// Mutator functions
//////////////////////////////////////////////////////////////////////

void CVideoFrameFileRecord::setImageDataFlag(MTI_UINT8 newImageDataFlag)
{
   imageDataFlag = newImageDataFlag;
}


void CVideoFrameFileRecord::calculateRecordByteCount()
{
   // Total the size of all the fields in the record

   CFrameFileRecord::calculateRecordByteCount();

   MTI_UINT32 newByteCount = getRecordByteCount();

   newByteCount += sizeof(imageDataFlag);

   newByteCount += sizeof(videoFrameReserved);

   // Update the object with the calculated size
   setRecordByteCount(newByteCount);
}


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CVideoFrameListFileReader::CVideoFrameListFileReader()
{

}

CVideoFrameListFileReader::~CVideoFrameListFileReader()
{

}

//////////////////////////////////////////////////////////////////////
// Track Builder
//////////////////////////////////////////////////////////////////////

int
CVideoFrameListFileReader::readFrameListFile(CVideoFrameList& videoFrameList)
{
   int retVal;

   retVal = openFrameListFile(videoFrameList.getFrameListFileNameWithExt());
   if (retVal != 0)
      {
      // ERROR: Could not open Frame List file, possibly because file did not
      //        exist, don't have access permission or the file does
      //        not have the MTI/CPMP file format
      return retVal;
      }

   // Check that track types of the file agree
   if (fileScanner->getFileType()!= FR_FILE_TYPE_VIDEO_TRACK3)
      {
      // Error: file type is not a Video Track
      return CLIP_ERROR_VIDEO_TRACK_FILE_INVALID_FILE_TYPE;
      }

   // Read all the rest of the records in the file and create
   // the Frame and Field objects and attach them to the CFrameList object
   retVal = buildFramesFromFile(videoFrameList);
   if(retVal != 0)
      {
      // Error: couldn't build all of the frames or fields
      return retVal;
      }

   closeFrameListFile();

   return 0;  // Success

} // readFrameListFile

//////////////////////////////////////////////////////////////////////
// Private Clip Builder Functions
//////////////////////////////////////////////////////////////////////

int CVideoFrameListFileReader::
buildFramesFromFile(CVideoFrameList& videoFrameList)
{
   CFileRecord *newRecordPtr;
   CVideoFrame *newFrame = 0;
   CVideoField *newField;

   CVideoProxy *videoProxy = videoFrameList.getVideoProxy();
   const CMediaStorageList *mediaStorageList;
   mediaStorageList = videoProxy->getMediaStorageList();

   // Check file version to determine if the Field list was implemented
   CFieldList *fieldList;
   if (fileScanner->getFileVersion()
                       > FR_VIDEO_TRACK3_FILE_TYPE_FILE_PRE_FIELD_LIST_VERSION)
      fieldList = videoProxy->getFieldList();
   else
      fieldList = 0;

   CImageFormat *imageFormat;
   imageFormat = const_cast<CImageFormat*>(videoProxy->getImageFormat());

   // Read all records from track file and convert to
   // CVideoFrame and CVideoField instances
   int retVal;
   while ((retVal = fileScanner->readNextRecord(&newRecordPtr)) == 0)
      {
      switch (EFileRecordType(newRecordPtr->getRecordType()))
         {
         case FR_RECORD_TYPE_VIDEO_FRAME_R3 :
            // Create a new instance of a CVideoFrame object and set
            // based on CVideoFrameFileRecord instance
            newFrame
              = readFrame(static_cast<CVideoFrameFileRecord&>(*newRecordPtr),
                          imageFormat);

            // Add the new frame to the video track
            videoFrameList.addFrame(newFrame);
            break;

         case FR_RECORD_TYPE_VIDEO_FIELD_R3 :
            if (newFrame == 0)
               {
               // Error: a field record encountered before frame record
               delete newRecordPtr;
               return CLIP_ERROR_VIDEO_TRACK_FILE_INVALID_RECORD;
               }

            // Create a new instance of a CVideoField and set it based
            // on the CVideoFrameFileRecord
            newField
              = readField(static_cast<CVideoFieldFileRecord&>(*newRecordPtr),
                          fieldList, *mediaStorageList);

            // Add the new field to the current frame
            newFrame->addField(newField);
            break;

         default :
            // Error: Unknown record type
            delete newRecordPtr;
            return CLIP_ERROR_VIDEO_TRACK_FILE_INVALID_RECORD;
         }

      // Delete the file record just obtained
      delete newRecordPtr;
   }
   // WOW this really fucks things up
   //if (retVal == CLIP_ERROR_FILE_READ_FAILED)
      //return retVal;
      
   return 0;
}

CVideoFrame*
CVideoFrameListFileReader::readFrame(CVideoFrameFileRecord& frameRecord,
                                  CImageFormat *imageFormat)
{
   // Make a new CFrame subtype
   CVideoFrame* frame = new CVideoFrame(frameRecord.getFieldCount());

   // Initialize the new frame from the frame file record
   setVideoFrameFromFileRecord(*frame, frameRecord);

   // Set image format in frame
   frame->setImageFormat(imageFormat);

   return frame;
}

CVideoField*
CVideoFrameListFileReader::readField(CVideoFieldFileRecord& fieldRecord,
                                     CFieldList *newFieldList,
                                     const CMediaStorageList& mediaStorageList)
{
   // Make a new CField subtype
   CVideoField* newField = new CVideoField;

   // Initialize the new field from the field file record
   setVideoFieldFromFileRecord(*newField, newFieldList, mediaStorageList,
                               fieldRecord);

   // Success
   return newField;
}

//////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////

void CVideoFrameListFileReader::
setVideoFieldFromFileRecord(CVideoField& field,
                            CFieldList *newFieldList,
                            const CMediaStorageList& mediaStorageList,
                            const CVideoFieldFileRecord &fieldRecord)
{
   // Set base class, CField, from record
   setBaseFieldFromFileRecord(field, newFieldList, mediaStorageList,
                              fieldRecord);
}

void CVideoFrameListFileReader::
setVideoFrameFromFileRecord(CVideoFrame& frame,
                            const CVideoFrameFileRecord &frameRecord)

{
   // Set base class, CFrame, from record
   setBaseFrameFromFileRecord(frame, frameRecord);

   frame.setImageDataFlag(EImageDataFlag(frameRecord.getImageDataFlag()));
}

//////////////////////////////////////////////////////////////////////
////////////////                            //////////////////////////
/////////////     CVideoFrameListFileWriter     /////////////////////////
///////////////                            ///////////////////////////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CVideoFrameListFileWriter::CVideoFrameListFileWriter()
{

}

CVideoFrameListFileWriter::~CVideoFrameListFileWriter()
{

}

//////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////

int
CVideoFrameListFileWriter::writeFrameListFile(CVideoFrameList* videoFrameList)
{
   int retVal;

   // Create appropriate instance of CStructuredStream subtype
   CStructuredFileStream stream;

   // Create file with magic word and Common Header Record
   string frameListFileName = videoFrameList->getFrameListFileNameWithExt();
   if(fileBuilder.createFile(&stream, frameListFileName,
                             FR_FILE_TYPE_VIDEO_TRACK3,
                             FR_VIDEO_TRACK3_FILE_TYPE_FILE_VERSION) != 0)
      return CLIP_ERROR_VIDEO_TRACK_FILE_CREATE_FAILED;

   // Frame and Field Records
   retVal = writeFrameList(videoFrameList);
   if (retVal != 0)
      return retVal; // ERROR: Failed to write frames and fields to track file

   // Close file
   if (stream.close() != 0)
      return CLIP_ERROR_VIDEO_TRACK_FILE_CLOSE_FAILED;

   return 0;   // Success
}

//////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////

void CVideoFrameListFileWriter::
setVideoFieldToFileRecord(const CVideoField& field,
                          const CMediaStorageList &mediaStorageList,
                          CVideoFieldFileRecord &fieldRecord)
{
   // Set base class, CField, to record
   setBaseFieldToFileRecord(field, mediaStorageList, fieldRecord);

}

void CVideoFrameListFileWriter::
setVideoFrameToFileRecord(const CVideoFrame& frame,
                          CVideoFrameFileRecord &frameRecord)

{
   // Set base class, CFrame, to record
   setBaseFrameToFileRecord(frame, frameRecord);

   frameRecord.setImageDataFlag(frame.getImageDataFlag());
}

//////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////

int CVideoFrameListFileWriter::writeField(CVideoField& field,
                                     const CMediaStorageList &mediaStorageList)
{
   int retVal;

   CVideoFieldFileRecord fieldRecord;
      
   setVideoFieldToFileRecord(field, mediaStorageList, fieldRecord);

   // Calculate and set the size of the record
   fieldRecord.calculateRecordByteCount();

   retVal = fileBuilder.putRecord(fieldRecord);
   if (retVal != 0)
      return retVal;  // ERROR

   return 0;
}

int CVideoFrameListFileWriter::writeFrame(CVideoFrame& frame)
{
   int retVal;

   CVideoFrameFileRecord frameRecord;

   setVideoFrameToFileRecord(frame, frameRecord);

   // Calculate and set the size of the record
   frameRecord.calculateRecordByteCount();

   retVal = fileBuilder.putRecord(frameRecord);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CVideoFrameListFileWriter::writeFrameList(CVideoFrameList* videoFrameList)
{
   int retVal;

   CVideoProxy *videoProxy = videoFrameList->getVideoProxy();
   const CMediaStorageList *mediaStorageList;
   mediaStorageList = videoProxy->getMediaStorageList();

   // Iterate over all frames in the clip's frame list
   int frameCount = videoFrameList->getTotalFrameCount();
   for(int frameIndex = 0; frameIndex < frameCount; ++frameIndex)
      {
      CVideoFrame *frame = videoFrameList->getFrame(frameIndex);

      // Write the frame to the file
      retVal = writeFrame(*frame);
      if (retVal != 0)
         return retVal; // ERROR: Unable to write the frame

      // Write the frame's fields
      int fieldCount = frame->getTotalFieldCount();
      for(int fieldIndex = 0; fieldIndex < fieldCount; ++fieldIndex)
         {
         CVideoField *field = frame->getField(fieldIndex);

         // Write the field to the file
         retVal = writeField(*field, *mediaStorageList);
         if (retVal != 0)
            return retVal; // ERROR: Unable to write the field
         }
      }

   return 0;
}


