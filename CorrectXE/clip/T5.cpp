#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <iostream>

#include "machine.h"
#include "ClipAPI.h"
#include "Clip5Proto.h"
#include "getopt.h"

////////////////////////////////////////////////////////////////////////

void PrintUsage(char *cpArg)
{
  cerr << "Usage: " << cpArg << " [-t video proxy index] <-A clip name>" << endl;
}

////////////////////////////////////////////////////////////////////////
//
//                         M A I N
//
////////////////////////////////////////////////////////////////////////

int main(int argc, char* argv[])
{
   int retVal;
   
   CreateApplicationName(argv[0]);
   string strClipName;

  InitClip5();

//   string binPath = "C:\\MTIBins\\NewBin";
//   string newClipName = "Logger_C5_KK_1";
   string binPath = "C:\\MTIBins";
   string newClipName = "Peek6";

//#define CREATE_CLIP_TEST
#ifdef CREATE_CLIP_TEST

   string newClipSchemeName = "HD422_525CT";
   string diskSchemeName = "ds-NoDiskLogger";
   int videoReservationCount = 240;
   int audioReservationCount = 240;
   ostringstream errMsg;

   retVal = MakeNewClip(binPath, newClipName, newClipSchemeName, diskSchemeName,
                        videoReservationCount, audioReservationCount, errMsg);

   cout << errMsg.str();

#endif  // #ifdef CREATE_CLIP_TEST

#define EXTEND_CLIP_TEST
#ifdef EXTEND_CLIP_TEST

   retVal = IncreaseClipMediaTest(binPath, newClipName, 0, 240);

#endif // #ifdef EXTEND_CLIP_TEST

//#define DECREASE_CLIP_TEST
#ifdef DECREASE_CLIP_TEST

   retVal = DecreaseClipMediaTest(binPath, newClipName, 20, 10);

#endif  // #ifdef DECREASE_CLIP_TEST

//#define GET_TIMECODE_TEST
#ifdef GET_TIMECODE_TEST
   CTimecode inTC(0), outTC(0);
   GetTimecodes(binPath, newClipName, 0, inTC, outTC);

   string inTCStr, outTCStr;
   inTC.getTimeString(inTCStr);
   outTC.getTimeString(outTCStr);
   cout << "In: " << inTCStr << " Out TC: " << outTCStr << endl;

#endif // #ifdef GET_TIMECODE_TEST

//#define SET_MEDIA_STATUS_TEST
#ifdef SET_MEDIA_STATUS_TEST

   retVal = SetMediaWritten(binPath, newClipName, 0, 100, -1);

#endif // #ifdef SET_MEDIA_STATUS_TEST

//#define RELOAD_MEDIA_STATUS_FILE_TEST
#ifdef RELOAD_MEDIA_STATUS_FILE_TEST

   retVal = ReloadMediaStatusTrackFile(binPath, newClipName, 0);

#endif // #ifdef RELOAD_MEDIA_STATUS_FILE_TEST

//#define SET_SMPTE_TIMECODE_TEST
#ifdef SET_SMPTE_TIMECODE_TEST

   retVal = SetSMPTETimecodeTest(binPath,  newClipName);
#endif

//#define SET_KEYKODE_TEST
#ifdef SET_KEYKODE_TEST

   retVal = SetKeykodeTest(binPath, newClipName);
#endif

//#define GET_AUDIO_SAMPLE_COUNT_AT_FRAME
#ifdef GET_AUDIO_SAMPLE_COUNT_AT_FRAME

   retVal = GetAudioSampleCountAtFrame(binPath, newClipName);

#endif

//#define DELETE_CLIP_TEST
#ifdef DELETE_CLIP_TEST
   CBinManager binMgr;
   retVal = binMgr.deleteClipOrClipIni(binPath, newClipName);
#endif

//#define VIDEO_PEEK_TEST
#ifdef VIDEO_PEEK_TEST
   string filename;
   MTI_INT64 offset;
   retVal = PeekAtVideoMediaLocation(binPath, newClipName, 0, 101, 1, filename, offset);
   if (retVal == 0)
      cout << "Peek filename " << filename << " Offset " << offset << endl;
#endif

//#define AUDIO_PEEK_TEST
#ifdef AUDIO_PEEK_TEST
   string filename;
   MTI_INT64 offset;
   retVal = PeekAtAudioMediaLocation(binPath, newClipName, 0, 2, 1, filename, offset);
   if (retVal == 0)
      cout << "Peek filename " << filename << " Offset " << offset << endl;
#endif

//#define CHECK_RFL_TEST
#ifdef CHECK_RFL_TEST

  retVal = PeekAtMediaAllocation(binPath, newClipName);
#endif


   cout << "Done: " << retVal << endl;

}

