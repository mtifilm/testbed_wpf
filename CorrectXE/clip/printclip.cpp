// printclip.cpp : Defines the entry point for the console application.
//

#include <string>
#include <iostream>

#include <errno.h>

#include "ClipAPI.h"

void printUsage()
{
   cerr << "Usage:" << endl;
   cerr << "       printclip [-f] clipFileName" << endl;
   cerr << "         -f  Print all frame information" << endl;
}

int main(int argc, char* argv[])
{
   bool printAllFrames = false;
   string clipFileName;
   string fSwitch = "-f";

   if (argc == 2)
      {
      clipFileName = argv[1]; 
      }
   else if (argc == 3)
      {
      clipFileName = argv[2];
      string arg1 = argv[1];
      if (fSwitch == arg1)
         printAllFrames = true;
      else
         {
         printUsage();
         return 1;
         } 
      }
   else 
      {
      printUsage();  
      return 1;
      }
 
   int status;

   // Read in the clip file and use it to create a clip data structure
   CClipBuilder clipBuilder;
   auto clip = clipBuilder.buildClipFromFile(clipFileName, &status);
 
   if (status != 0)
      {
      cerr << "ERROR: Unable to read clip file " << clipFileName << endl;
      cerr << "       Errno: " << errno << endl;
      return 1;
      }

   // Print the Clip instance data
   clip->dump(cout);

   // Get the image format and print it out
   const CMediaFormat* mediaFormat = clip->getMediaFormatPtr();

   mediaFormat->dump(cout);

   if (printAllFrames)
      {
      // Iterate over all frames in clip, dumping them
      for (int frameNumber = 0; frameNumber < clip->getFrameCount(); ++frameNumber)
         {
         cout << " Frame " << frameNumber << ": ";
         (clip->getBaseFrame(frameNumber))->dump(cout);
         }
      } 
   return 0;
}
