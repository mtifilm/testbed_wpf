// Frame.cpp: implementation of the CFrame abstract base class.
//
//////////////////////////////////////////////////////////////////////

#include "Clip3Track.h"
#include "MediaAccess.h"
#include "MediaAllocator.h"
#include "MediaFormat.h"
#include "MTIstringstream.h"
#include "err_clip.h"
#include "IniFile.h"
#include <string>
using std::string;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFrame::CFrame(int reserveFieldCount)
: timecode(new CTimecode(0, 0)),   // Dummy timecode initialization
  mediaFormat(0)                       // NULL pointer
{
   // Constructor that reserves a given number of entries
   // in the fieldList.  This allocates the required amount
   // of memory for the fieldList entries before any fields
   // are added to the fieldList.  This may save memory if
   // the default allocation is large. The actual number
   // of fields in the fieldList can be more or less than
   // the reserved number.
   fieldList.reserve(reserveFieldCount);
}

CFrame::~CFrame()
{
   delete timecode;
   deleteFieldList();
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    copyFields
//
// Description: .
//
// Arguments:
//
// Returns:
//
//------------------------------------------------------------------------
int CFrame::copyFields(SCopyFrameSource& frameSrc)
{
   if (frameSrc.fieldCount > MAX_SOURCE_FIELDS)
      {
      // ERROR: More source fields than this function can handle
      return -510;
      }

   int visibleFieldCount = getVisibleFieldCount();
   if (frameSrc.fieldCount < visibleFieldCount)
      {
      // ERROR: Insufficient number of source fields to create
      //        destination frame's visible fields
      return -511;
      }

   // Get pointers to all of the source fields
   CField *srcField[MAX_SOURCE_FIELDS];
   CFrame *srcFrame;
   int i;
   for (i = 0; i < frameSrc.fieldCount; ++i)
      {
      srcFrame
             = frameSrc.srcTrack->getBaseFrame(frameSrc.fieldSrc[i].frameIndex);
      if (srcFrame)
         srcField[i] = srcFrame->getBaseField(frameSrc.fieldSrc[i].fieldIndex);
      if (srcFrame == 0 || srcField[i] == 0)
         {
         // ERROR: Bad source frame or field index
         return -512;
         }
      }

   // Initialize array of pointers to new fields
   CField *newField[MAX_SOURCE_FIELDS];
   for (i = 0; i < MAX_SOURCE_FIELDS; ++i)
      {
      newField[i] = 0;
      }

   try
      {
      // Create new fields copied from source fields
      for (i = 0; i < frameSrc.fieldCount; ++i)
         {
         // Use CField derived class's copy-constructor to create
         // a new field based on the source field
         newField[i] = createField(*srcField[i]);

         // Set new field's flags
         bool newFieldInvisible = (i >= visibleFieldCount);
         newField[i]->setFieldInvisible(newFieldInvisible);
         newField[i]->setVirtualMedia(true);
         newField[i]->setSharedMedia(false);
         newField[i]->setSharedMediaFirst(false);
         if (newFieldInvisible)
            newField[i]->setVisibleSiblingFieldIndex(frameSrc.fieldSrc[i].visibleSiblingIndex);
         }

      // Compare media location in pairs of fields and set flags for
      // fields that share the same media
      for (i = 0; i < frameSrc.fieldCount-1; ++i)
         {
         if (newField[i]->isSharedMedia())
            break;  // Skip this field since it has already been established
                    // that it shares media

         for (int j = i+1; j < frameSrc.fieldCount; ++j)
            {
            if (newField[j]->isSharedMedia())
               break;  // Skip this field since it has already been established
                       // that it shares media

            if (newField[i]->getMediaLocation() == newField[j]->getMediaLocation())
               {
               newField[i]->setSharedMedia(true);
               newField[i]->setSharedMediaFirst(true);
               newField[j]->setSharedMedia(true);
               }
            }
         }
      }
   catch (...)
      {
      // Cleanup after an exception to avoid memory leaks
      // Delete any fields that were created but did not get added to
      // the frame.  If they were added to the frame, they will
      // be destroyed when the frame is destroyed
      for (i = 0; i < MAX_SOURCE_FIELDS; ++i)
         {
         delete newField[i];
         }

      TRACE_0(errout << "ERROR: CFrame::copyFields about to re-throw an exception");
      throw;  // Throw the same exception
      }

   return 0;

} // copyFields

int CFrame::doubleField(CFrame &srcFrame, int fieldIndex)
{
   // Create and add two fields to this frame from a single field in
   // the source frame

   CField *srcField = srcFrame.getBaseField(fieldIndex);

   for (int i = 0; i < 2; ++i)
      {
      CField *newField = createField(*srcField);

      newField->setVirtualMedia(true);
      newField->setSharedMedia(true);
      newField->setSharedMediaFirst((i==0));
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Read/Write Media Data Functions
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    readMediaAllFields
//
// Description: Read a frame's media data from media storage
//              into caller's buffers for all of the frame's visible
//              and invisible fields.  The number of fields to be read can
//              be obtained by calling the CFrame member function
//              getTotalFieldCount
//
//              Media data is read in the same order as the fields
//              appear in the frame.  The read order is not optimized
//              for the media location.
//
/// Arguments    MTI_UINT8 *buffer[]  Array of pointers to buffers, one for
//                                    each field in frame
//
// Returns:     0 for success, non-zero if an error.
//
//------------------------------------------------------------------------
int CFrame::readMediaAllFields(MTI_UINT8 *buffer[])
{
	int retVal;
	int totalFieldCount = getTotalFieldCount();

	for (int fieldIndex = 0; fieldIndex < totalFieldCount; ++fieldIndex)
		{
		// Read the field to buffer
		CField* field = getBaseField(fieldIndex);

		if (!field->isMediaWritten() || !field->doesMediaStorageExist())
			{
			// Nothing is really on the disk, so just synthesize a black field
			// into the caller's buffer.
			blackenField(field, fieldIndex, buffer[fieldIndex]);
			}
		else
			{
			retVal = field->readMedia(buffer[fieldIndex]);
			if (retVal != 0)
				{
				// ERROR: Could not read media of the field
				return retVal;  // Return error
				}
			}
		}

	return 0;      // Return success
}

//------------------------------------------------------------------------
//
// Function:    readMediaAllFieldsOptimized
//
// Description: Read a frame's media data from media storage
//              into caller's buffers for all of the frame's visible
//              and invisible fields.  The number of fields to be read can
//              be obtained by calling the CFrame member function
//              getTotalFieldCount.
//
//              Media read is optimized by reading from the fields' media
//              storage in ascending media location order and by copying
//              buffers instead of re-reading for fields that share the
//              same media.
//
// Arguments    MTI_UINT8 *buffer[]  Array of pointers to buffers, one for each
//                                   field in frame
//
// Returns:     0 if success, non-zero if an error.
//
//------------------------------------------------------------------------
int CFrame::readMediaAllFieldsOptimized(MTI_UINT8 *buffer[])
{
   int retVal;

   retVal = readMediaAllFieldsOptimizedMT(buffer, 0);
   if (retVal != 0)
      return retVal;

   return 0;

}

int CFrame::readMediaAllFieldsOptimizedMT(MTI_UINT8 *buffer[],
                                          int fileHandleIndex)
{
   int retVal;

   retVal = readMediaOptimizedMT(buffer, getTotalFieldCount(), fileHandleIndex);
   if (retVal != 0)
      {
      // ERROR: read media failed
      return retVal;
      }

   return 0;      // Return success
}

//------------------------------------------------------------------------
//
// Function:    readMediaVisibleFields
//
// Description: Read a frame's media data from media storage
//              into caller's buffers for the frame's visible fields.
//
//              Media data is read in the same order as the fields
//              appear in the frame.  The read order is not optimized
//              for the media location.
//
// Arguments    MTI_UINT8 *buffer[]  Array of pointers to buffers, one for each
//                                   visible field in frame
//
// Returns:     0 if success, non-zero if an error.
//
//------------------------------------------------------------------------
int CFrame::readMediaVisibleFields(MTI_UINT8 *buffer[])
{
   int retVal;

   // Read the first (and perhaps only) field to buffer
   CField* field = getBaseField(0);    // First field in field list

   if (!field->isMediaWritten() || !field->doesMediaStorageExist())
      {
      // Nothing is really on the disk, so just synthesize a black field
      // into the caller's buffer.
      blackenField(field, 0, buffer[0]);
      }
   else
      {
      retVal = field->readMedia(buffer[0]);
      if (retVal != 0)
         {
         // ERROR: Could not read media of the field
         return retVal;  // Return error
         }
      }

   // If this frame has two visible fields, read the second field to buffer
   if (getVisibleFieldCount() > 1)
      {
      field = getBaseField(1);      // Second field in field list

      if (!field->isMediaWritten() || !field->doesMediaStorageExist())
         {
         // Nothing is really on the disk, so just synthesize a black field
         // into the caller's buffer.
         blackenField(field, 1, buffer[1]);
         }
      else
         {
         retVal = field->readMedia(buffer[1]);
         if (retVal != 0)
            {
            // ERROR: Could not read media of the field
            return retVal;  // Return error
            }
         }
      }

   return 0;      // Return success
}

//------------------------------------------------------------------------
//
// Function:    readMediaVisibleFieldsOptimized
//
// Description: Read a frame's media data from media storage
//              into caller's buffers for the frame's visible fields.
//
//              Media read is optimized by reading from the fields' media
//              storage in ascending media location order and by copying
//              buffers instead of re-reading for fields that share the
//              same media.
//
// Arguments    MTI_UINT8 *buffer[]  Array of pointers to buffers, one for each
//                                   visible field in frame
//
// Returns:     0 if success, non-zero if an error.
//
//------------------------------------------------------------------------
int CFrame::readMediaVisibleFieldsOptimized(MTI_UINT8 *buffer[])
{
   int retVal;

   retVal = readMediaOptimized(buffer, getVisibleFieldCount());
   if (retVal != 0)
      {
      // ERROR: read media failed
      return retVal;
      }

   return 0;      // Return success
}

int CFrame::readMediaOptimized(MTI_UINT8 *buffer[], int fieldCount)
{
   int retVal;

   retVal = readMediaOptimizedMT(buffer, fieldCount, 0);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CFrame::readMediaOptimizedMT(MTI_UINT8 *buffer[], int fieldCount,
                                 int fileHandleIndex)
{
   int retVal;

   // Arrays to hold results of sort
   int indexArray[CFRAME_MAX_FIELD_COUNT];
   CField *fieldArray[CFRAME_MAX_FIELD_COUNT];
   CMediaLocation mediaLocationArray[CFRAME_MAX_FIELD_COUNT];

   // Sort fields by media location
   sortFieldsByMediaLocation(fieldCount, indexArray, fieldArray);

   OPT_RESULT optResult = getMediaLocationOptimized(buffer, fieldCount,
                                                    mediaLocationArray,
                                                    true /* must exist */);
   if (optResult == NEED_ONE_IO_OP || optResult == NEED_TWO_IO_OPS)
      {
      switch(optResult)
         {
         case NEED_ONE_IO_OP:
            retVal = mediaLocationArray[0].read(buffer[0], fileHandleIndex);
            break;

         case NEED_TWO_IO_OPS:
            {
            CMediaAccess *mediaAccess = mediaLocationArray[0].getMediaAccess();

            if (mediaAccess != 0)
               retVal = mediaAccess->readMultiple(2, buffer, mediaLocationArray);
            break;
			}

		default:
			break;
         }
      return retVal;
      }

   for (int i = 0; i < fieldCount; ++i)
      {
      // indexArray contains field indices ordered by
      // ascending media location
      int fieldIndex = indexArray[i];
      CField* field = fieldArray[fieldIndex];

      if (!field->isMediaWritten() || !field->doesMediaStorageExist())
         {
         // Nothing is really on the disk, so just synthesize a black field
         // into the caller's buffer.
         blackenField(field, fieldIndex, buffer[fieldIndex]);
         }
      else if (field->isSharedMedia() && !(field->isSharedMediaFirst()))
         {
         // Media storage is shared, presumably with the field
         // that was just read, so just copy the buffer
         unsigned char *srcBuffer = buffer[indexArray[i -1]];
         memcpy(buffer[fieldIndex], srcBuffer,
                field->getMediaLocation().getDataSize());
         }
      else
         {
         // Media storage is not shared, so read from media storage
         // or if it is shared, it is the first of fields that share this
         // media storage
         retVal = field->readMedia(buffer[fieldIndex]);
         if (retVal != 0)
            {
            // ERROR: Could not read media of the field
            return retVal;  // Return error
            }
         }
      }

   return 0;      // Return success
}

//------------------------------------------------------------------------
int CFrame::GetMediaLocationsAllFields(vector<CMediaLocation> &mediaLocationList)
{
	int retVal;

   for (int i = 0; i < getTotalFieldCount(); ++i)
      {
      CField *field = getBaseField(i);
      if (field == 0)
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

      CMediaLocation mediaLocation = field->getMediaLocation();
      mediaLocationList.push_back(mediaLocation);
      }

   return 0;
}

//------------------------------------------------------------------------
CFrame::OPT_RESULT CFrame::getMediaLocationOptimized(
                                     MTI_UINT8 *buffer[],
                                     int fieldCount,
                                     CMediaLocation *mediaLocationArray,
                                     bool mustExistFlag)
{
   OPT_RESULT result = CANNOT_OPTIMIZE;

   for (int i = 0; i < fieldCount; ++i)
      mediaLocationArray[i].setInvalid();

   if (fieldCount != 2)
      return CANNOT_OPTIMIZE;

   CField *field0 = getBaseField(0);
   CField *field1 = getBaseField(1);
   if (field0 == NULL || field1 == NULL)
      return CANNOT_OPTIMIZE;

   // Make sure nothing funky's going on
   if (field0->isWriteProtectedMedia() || field0->isDirtyMedia() ||
       field1->isWriteProtectedMedia() || field1->isDirtyMedia() )
      return CANNOT_OPTIMIZE;

   // If the "must exist" flag is set, check that there is media to read
   if (mustExistFlag
       && (!field0->isMediaWritten() ||!field0->doesMediaStorageExist()
          || !field1->isMediaWritten() || !field1->doesMediaStorageExist()))
      return CANNOT_OPTIMIZE;

   CMediaLocation mediaLocation0 = field0->getMediaLocation();
   CMediaLocation mediaLocation1 = field1->getMediaLocation();
   MTI_UINT32 field0DataSize = mediaLocation0.getDataSize();
   MTI_UINT32 field1DataSize = mediaLocation1.getDataSize();

   // Here's a dreadfully awful hack. Determine if the media is being kept
   // in either loose files or in an image repository, in which case we have
   // to optimize differently from rawdisks, where we somehow know that
   // adjacent fields may be readable in a single read().
   //
   if (!(mediaLocation0.isMediaTypeImageFile() ||
         mediaLocation0.isMediaTypeImageRepository() ||
         mediaLocation1.isMediaTypeImageFile() ||
         mediaLocation1.isMediaTypeImageRepository()))
      {
      MTI_UINT32 field0ByteSize = mediaLocation0.getDataByteSize();

      if (buffer[0] + field0ByteSize != buffer[1])
         return CANNOT_OPTIMIZE;

      mediaLocation0.setDataIndex(mediaLocation0.getDataIndex()
                                   + field0DataSize);
      mediaLocation0.setDataSize(field0DataSize + field1DataSize);
      mediaLocation1.setDataSize(field0DataSize + field1DataSize);

      if (mediaLocation0 != mediaLocation1)
         return CANNOT_OPTIMIZE;

      mediaLocationArray[0] = field0->getMediaLocation();
      mediaLocationArray[0].setDataSize(field0DataSize + field1DataSize);
      mediaLocationArray[0].setTwoFields(true);
      result = NEED_ONE_IO_OP;
      }
   else if ((mediaLocation0.isMediaTypeImageFile()
             &&  mediaLocation1.isMediaTypeImageFile())
        ||  (mediaLocation0.isMediaTypeImageRepository()
             &&  mediaLocation1.isMediaTypeImageRepository()))
      {
      if (field0DataSize != field1DataSize)
         return CANNOT_OPTIMIZE;

      // Ensure same file sequence
      if (mediaLocation0.getMediaAccess() != mediaLocation1.getMediaAccess())
         return CANNOT_OPTIMIZE;

      // Hack: Can't use the optimized ImageFileIO path if the buffer is not
      // 4K (actually system page) aligned
      if ((((int) buffer[0]) % 4096) != 0 || (((int) buffer[0]) % 4096) != 0)
         return CANNOT_OPTIMIZE;

      mediaLocationArray[0] = mediaLocation0;
      mediaLocationArray[1] = mediaLocation1;
      result = NEED_TWO_IO_OPS;
      }

   return result;
}

//------------------------------------------------------------------------
//
// Function:    writeMediaAllFields
//
// Description: Write a frame's media data from caller's buffers to
//              media storage
//
//              Media data is written in the same order as the fields
//              appear in the frame.  The write order is not optimized
//              for the media location.
//
// Arguments    MTI_UINT8 *buffer[]  Array of pointers to buffers, one for each
//                                   field in frame
//
// Returns:     0 if success, non-zero if an error.
//
//------------------------------------------------------------------------
int CFrame::writeMediaAllFields(MTI_UINT8 *buffer[])
{
   int retVal;
   int totalFieldCount = getTotalFieldCount();

   ////////////////////////////////////////////////////////////////////
   // clip2clip hack... I don't know why I need to flush the field list
   // here, but if i don't it sometiumes never seems to get flushed and
   // I am sick of this shit so I am in total brute force mode
   bool needToUpdateFieldListFile = false;
   ////////////////////////////////////////////////////////////////////

   for (int fieldIndex = 0; fieldIndex < totalFieldCount; ++fieldIndex)
      {
      // Write the field from buffer to media storage
      CField* field = getBaseField(fieldIndex);

      // Clip2clip crap
      if (field->isWriteProtectedMedia())
         return CLIP_ERROR_MEDIA_IS_WRITE_PROTECTED;

      if (field->isDirtyMedia() &&
            !(field->isMediaWritten() && field->doesMediaStorageExist()))
         {
         needToUpdateFieldListFile = true;
         }
      // end clip2clip crap

      ////////////////////////////////////////////////////////////
      // WTF? Why isn't there a test for shared media here??
      // This seems to only be called by the "stamper" utility,
      // but it still seems like it should obey the rules!!!!
      ////////////////////////////////////////////////////////////

      retVal = field->writeMedia(buffer[fieldIndex]);
      if (retVal != 0)
         {
         // ERROR: Could not write media of the field
         return retVal;  // Return error
         }
      }

   // clip2clip  - don't know why we need to do this here...
   if (needToUpdateFieldListFile)
      getBaseField(0)->getFieldList()->updateFieldListFile();

   return 0;      // Return success
}

//------------------------------------------------------------------------
//
// Function:    writeMediaAllFieldsOptimized
//
// Description: Write a frame's media data for all fields from the caller's
//              buffers to media storage
//
//              Media write is optimized by writing to the fields' media
//              storage in ascending media location order and by avoiding
//              duplicate writes for fields that share the same media.
//
// Arguments    MTI_UINT8 *buffer[]  Array of pointers to buffers, one for each
//                                   field in frame
//
// Returns:     0 if success, non-zero if an error.
//
//------------------------------------------------------------------------
int CFrame::writeMediaAllFieldsOptimized(MTI_UINT8 *buffer[])
{
   int retVal;

   retVal = writeMediaOptimized(buffer, getTotalFieldCount());
   if (retVal != 0)
      {
      // ERROR: read media failed
      return retVal;
      }

   return 0;      // Return success
}

int CFrame::writeMediaOptimized(MTI_UINT8 *buffer[], int fieldCount)
{
   int retVal;

   ////////////////////////////////////////////////////////////////////
   // clip2clip hack... I don't know why I need to flush the field list
   // here, but if i don't it sometiumes never seems to get flushed and
   // I am sick of this shit so I am in total brute force mode
   bool needToUpdateFieldListFile = false;
   ////////////////////////////////////////////////////////////////////

   if (fieldCount < 1) {
      TRACE_0(errout << "Called CFrame::writeMediaOptimized with "
                     << "0 fields to write!");
      return 0;     // Short circuit for sanity
   }

   // Arrays to hold results of sort
   int indexArray[CFRAME_MAX_FIELD_COUNT];
   CField *fieldArray[CFRAME_MAX_FIELD_COUNT];
   CMediaLocation mediaLocationArray[CFRAME_MAX_FIELD_COUNT];

   // Sort fields by media location
   sortFieldsByMediaLocation(fieldCount, indexArray, fieldArray);

   OPT_RESULT optResult = getMediaLocationOptimized(buffer, fieldCount,
                                                    mediaLocationArray,
                                                    false /* OK not exist */);
   if (optResult == NEED_ONE_IO_OP || optResult == NEED_TWO_IO_OPS)
      {
      // WORKAROUND FOR BUG 2348: There is a timing bug in the VTR emulator,
      // where it flushes the clip metadata in a thread separate from the
      // thread that is writing the media. The metadata thread computes the
      // frame diffs before flushing. On the Dell 650 this took long enough
      // so the write()'s below return and set the "media written" flag
      // before the metadata is flushed by the other thread. But on a Dell
      // 670, the diff calculation often finishes before the write() returns
      // so the "media written" flag is written to disk as "false". To work
      // around this problem (instead of trying to fix the VTR emulator) we
      // set the written flag BEFORE the write(), and undo it if the write
      // fails.
      //
      bool *oldMediaWrittenFlags = new bool[fieldCount];
      bool *oldMediaStorageExistsFlags = new bool[fieldCount];

      for (int i = 0; i < fieldCount; ++i)
         {
         // Bug 2519 - Save the old values in case we need to rewind
         oldMediaWrittenFlags[i] = fieldArray[i]->isMediaWritten();
         oldMediaStorageExistsFlags[i] = fieldArray[i]->doesMediaStorageExist();

         if (!(oldMediaWrittenFlags[i] && oldMediaStorageExistsFlags[i]))
            {
            // Presumably the media has been written to allocated media storage
            fieldArray[i]->setMediaWritten(true);
            fieldArray[i]->setMediaStorageExists(true);
            if (fieldArray[i]->isDirtyMedia())
               needToUpdateFieldListFile = true;
            }
         }
      // -- //

      switch(optResult)
         {
         case NEED_ONE_IO_OP:
            retVal = mediaLocationArray[0].write(buffer[0]);
            break;

         case NEED_TWO_IO_OPS:
            {
            CMediaAccess *mediaAccess = mediaLocationArray[0].getMediaAccess();

            if (mediaAccess != 0)
               retVal = mediaAccess->writeMultiple(2, buffer, mediaLocationArray);
            break;
			}

		 default:
		 	break;
         }

      if (retVal != 0)
         {
         for (int i = 0; i < fieldCount; ++i)
            {
            // The write failed... undo the flag setting
            fieldArray[i]->setMediaWritten(oldMediaWrittenFlags[i]);
            fieldArray[i]->setMediaStorageExists(oldMediaStorageExistsFlags[i]);
            }
         }
      else if (needToUpdateFieldListFile)
         {
         fieldArray[0]->getFieldList()->updateFieldListFile();
         }

      delete[] oldMediaWrittenFlags;
      delete[] oldMediaStorageExistsFlags;
      return retVal;
      }

   // You end up HERE if multi-field write optimization was not possible

   for (int i = 0; i < fieldCount; ++i)
      {
      // indexArray contains field indices ordered by
      // ascending media location
      int fieldIndex = indexArray[i];
      CField* field = fieldArray[fieldIndex];

      // NOTE regarding bug 2348... the equivalent change to the above
      // is in CVideoField::writeMedia(), called below. This may be
      // unnecessary, but I have not taken the time to analyze the
      // code paths.

      if (!(field->isSharedMedia()) || field->isSharedMediaFirst())
         {
         // Clip2clip crap
         if (field->isWriteProtectedMedia())
            return CLIP_ERROR_MEDIA_IS_WRITE_PROTECTED;

         if (field->isDirtyMedia() &&
               !(field->isMediaWritten() && field->doesMediaStorageExist()))
            {
            needToUpdateFieldListFile = true;
            }
         // end clip2clip crap

         // Media storage is not shared or it is first field of
         // those that do share media, so write to media storage
         retVal = field->writeMedia(buffer[fieldIndex]);
         if (retVal != 0)
            {
            // ERROR: Could not write media of the field
            return retVal;  // Return error
            }

         }  // shared media silent override
      }  // for each field in the frame

   // clip2clip  - don't know why we need to do this here...
   if (needToUpdateFieldListFile)
      fieldArray[0]->getFieldList()->updateFieldListFile();

   return 0;      // Return success
}

//------------------------------------------------------------------------
//
// Function:    writeMediaFilmFramesOptimized
//
// Description: Write a frame's media data for both visible and invisible
//              fields given caller's buffers for visible fields only.
//              Each invisible field is written from the buffer that
//              corresponds to the invisible field's visible sibling.
//
//              Media write is optimized by writing to the fields' media
//              storage in ascending media location order and by avoiding
//              duplicate writes for fields that share the same media.
//
// Arguments    MTI_UINT8 *buffer[]  Array of pointers to buffers, one for each
//                                   visible field in frame
//
// Returns:     0 if success, non-zero if an error.
//
//------------------------------------------------------------------------
int CFrame::writeMediaFilmFramesOptimized(MTI_UINT8 *buffer[])
{
   int retVal;
   int i;

   ////////////////////////////////////////////////////////////////////
   // clip2clip hack... I don't know why I need to flush the field list
   // here, but if i don't it sometiumes never seems to get flushed and
   // I am sick of this shit so I am in total brute force mode
   bool needToUpdateFieldListFile = false;
   ////////////////////////////////////////////////////////////////////

   // Arrays to hold results of sort
   int indexArray[CFRAME_MAX_FIELD_COUNT];
   CField *fieldArray[CFRAME_MAX_FIELD_COUNT];
   int fieldCount = getTotalFieldCount();

   // Sort fields by media location and field flags
   sortFieldsByMediaLocation(fieldCount, indexArray, fieldArray);

   // If none of the fields are invisible, do a normal optimized write
   for (i = 0; i < fieldCount; ++i)
      {
      int fieldIndex = indexArray[i];
      CField* field = fieldArray[fieldIndex];

      if (field->isFieldInvisible())
         break;
      }
   if (i == fieldCount)
      return writeMediaOptimized(buffer, fieldCount);

   // One of the fields is invisible
   for (i = 0; i < fieldCount; ++i)
      {
      // indexArray contains field indices ordered by
      // ascending media location
      int fieldIndex = indexArray[i];
      CField* field = fieldArray[fieldIndex];

      if (!(field->isSharedMedia()) || field->isSharedMediaFirst())
         {
         int bufferIndex;
         if (field->isFieldInvisible())
            {
            // This is an invisible field, so use the visible sibling
            // field's buffer (0 or 1)
            bufferIndex = field->getVisibleSiblingFieldIndex();
            }
         else
            {
            // This is a visible field, so use the input buffer with
            // the same index
            bufferIndex = fieldIndex;
            }

         // Clip2clip crap
         if (field->isWriteProtectedMedia())
            return CLIP_ERROR_MEDIA_IS_WRITE_PROTECTED;

         if (field->isDirtyMedia() &&
               !(field->isMediaWritten() && field->doesMediaStorageExist()))
            {
            needToUpdateFieldListFile = true;
            }
         // end clip2clip crap

         // Media storage is not shared, so write to media storage
         retVal = field->writeMedia(buffer[bufferIndex]);
         if (retVal != 0)
            {
            // ERROR: Could not write media of the field
            return retVal;  // Return error
            }
         }
      }

   // clip2clip  - don't know why we need to do this here...
   if (needToUpdateFieldListFile)
      fieldArray[0]->getFieldList()->updateFieldListFile();

   return 0;      // Return success
}

//------------------------------------------------------------------------
//
// Function:    writeMediaVisibleFields
//
// Description: Write a frame's media data for the visible fields only
//              from caller's buffers to media storage
//
//              Media data is written in the same order as the fields
//              appear in the frame.  The write order is not optimized
//              for the media location.
//
// Arguments    MTI_UINT8 *buffer[]  Array of pointers to buffers, one for each
//                                   visible field in frame
//
// Returns:     0 if success, non-zero if an error.
//
//------------------------------------------------------------------------
int CFrame::writeMediaVisibleFields(MTI_UINT8 *buffer[])
{
   int retVal;
   int totalFieldCount = getVisibleFieldCount();

   ////////////////////////////////////////////////////////////////////
   // clip2clip hack... I don't know why I need to flush the field list
   // here, but if i don't it sometiumes never seems to get flushed and
   // I am sick of this shit so I am in total brute force mode
   bool needToUpdateFieldListFile = false;
   ////////////////////////////////////////////////////////////////////

   for (int fieldIndex = 0; fieldIndex < totalFieldCount; ++fieldIndex)
      {
      // Write the field from buffer to media storage
      CField* field = getBaseField(fieldIndex);

      //////////////////////////////////////////////////////
      // WTF? Why is there no check for shared media here
      //     like in other cases?
      //////////////////////////////////////////////////////

      // Clip2clip crap
      if (field->isWriteProtectedMedia())
         return CLIP_ERROR_MEDIA_IS_WRITE_PROTECTED;

      if (field->isDirtyMedia() &&
            !(field->isMediaWritten() && field->doesMediaStorageExist()))
         {
         needToUpdateFieldListFile = true;
         }
      // end clip2clip crap

      retVal = field->writeMedia(buffer[fieldIndex]);
      if (retVal != 0)
         {
         // ERROR: Could not write media of the field
         return retVal;  // Return error
         }
      }

   // clip2clip  - don't know why we need to do this here...
   if (needToUpdateFieldListFile)
      getBaseField(0)->getFieldList()->updateFieldListFile();

   return 0;      // Return success
}

//------------------------------------------------------------------------
//
// Function:    writeMediaVisibleFieldsOptimized
//
// Description: Write a frame's media data for visible fields only from the
//              caller's buffers to media storage
//
//              Media write is optimized by writing to the fields' media
//              storage in ascending media location order and by avoiding
//              duplicate writes for fields that share the same media.
//
// Arguments    MTI_UINT8 *buffer[]  Array of pointers to buffers, one for each
//                                   field in frame
//
// Returns:     0 if success, non-zero if an error.
//
//------------------------------------------------------------------------
int CFrame::writeMediaVisibleFieldsOptimized(MTI_UINT8 *buffer[])
{
   int retVal;

   retVal = writeMediaOptimized(buffer, getVisibleFieldCount());
   if (retVal != 0)
      {
      // ERROR: read media failed
      return retVal;
      }

   return 0;      // Return success
}

//** Dave's DPX Header Copy Hack
//------------------------------------------------------------------------
//
// Function:    writeMediaWithDPXHeaderCopyHack
//
// Description: [HACK] Write a frame's media data where there is only
//              one field per frame and we enable Dave's DPX Header
//              Copy Hack in case the underlying media consists of
//              DPX files.
//
// Arguments    MTI_UINT8 *buffer    Pointer to field buffers (only 1 field)!
//              string     srcFile   Path of the file from which to copy
//                                   the DPX header
//
// Returns:     0 if success, non-zero if an error.
//
//------------------------------------------------------------------------
int CFrame::writeMediaWithDPXHeaderCopyHack(MTI_UINT8 *buffer[],
                                            const string &sourceImageFilePath)
{
   assert(getTotalFieldCount() == 1);

   ////////////////////////////////////////////////////////////////////
   // clip2clip hack... I don't know why I need to flush the field list
   // here, but if i don't it sometimes never seems to get flushed and
   // I am sick of this shit so I am in total brute force mode
   bool needToUpdateFieldListFile = false;
   ////////////////////////////////////////////////////////////////////

   int retVal;
   CField* field = getBaseField(0);

   //////////////////////////////////////////////////////
   // WTF? Why is there no check for shared media here
   //     like in other cases?
   //////////////////////////////////////////////////////

   // Clip2clip crap
   if (field->isWriteProtectedMedia())
      return CLIP_ERROR_MEDIA_IS_WRITE_PROTECTED;

   if (field->isDirtyMedia() &&
         !(field->isMediaWritten() && field->doesMediaStorageExist()))
      {
      needToUpdateFieldListFile = true;
      }
   // end clip2clip crap

   // Write the field from buffer to media storage
	retVal = field->writeMediaWithDPXHeaderCopyHack(buffer[0],
                                                   sourceImageFilePath);

   // clip2clip  - don't know why we need to do this here...
   if (needToUpdateFieldListFile)
      getBaseField(0)->getFieldList()->updateFieldListFile();

   return retVal;
}

//------------------------------------------------------------------------
//
// Function:     sortFieldsByMediaLocation
//
// Description:  Orders fields by ascending media location and field flags.
//               Used to determine optimal order of media storage disk I/O.
//               Sorting results are returned in caller's indexArray[] as
//               ordered field indices.  Caller's fieldArray[] is set to
//               Field object pointers from the Frame's field list.  The
//               fieldArray is ordered the same as the Frame's field list
//               and is not sorted.
//
//               Sorting Order:
//                1) Media Type         Numeric sort of media storage type enum
//                2) Media Identifier   Alpha sort of name
//                3) Field Name         Alpha sort field-specific portion of
//                                      media file name
//                4) Data Index         Numeric sort of data location in
//                                      media storage
//                5) Shared Media Flag  First shared media before non-first
//                                      shared media
//                6) Invisible Field Flag Visible field before invisible field
//
//               If two or more fields in a frame have the same media location,
//               one field will have the isSharedMediaFirst flag set to true
//               and the other fields will have the flag set to false.
//
// Arguments     int fieldCount         Number of fields of frame to sort
//               int *indexArray        Array to hold results of sort
//               CField **fieldArray    Array to hold pointers to fields
//
// Returns:      None.
//
// Side Effects: Caller's arrays indexArray[] and fieldArray[] are
//               set.
//
//------------------------------------------------------------------------
void CFrame::sortFieldsByMediaLocation(int fieldCount, int *indexArray,
                                       CField **fieldArray)
{
   int i;
   // Initialize the output arrays
   for (i = 0; i < fieldCount; i++)
      {
      indexArray[i] = i;
      fieldArray[i] = getBaseField(i);
      }

   // Now sort the fields in ascending media location
   if (fieldCount == 1)
      {
      // Only one field, so no need to sort anything, so return
      return;
      }
   else if (fieldCount == 2)
      {
      // Two fields, so sort with one comparison and possible swap
      if (*(fieldArray[0]) > *(fieldArray[1]))
         {
         // Swap the field indices
         indexArray[0] = 1;
         indexArray[1] = 0;
         }
      return;
      }
   else
      {
      // Three or more fields, so do a real sort

      // Sort below is an "Insertion" sort.  This sort algorithm has
      // the advantages of 1) simple, 2) O(n) performance when elements
      // are already sorted and 3) low overhead for small numbers of elements.
      // Disadvantages are O(n**2) worst-case performance, which is bad
      // for large number of elements, but mostly irrelevant for this
      // application

      // Initially, the first item is considered 'sorted'
      // i divides a into a sorted region, x<i, and an unsorted one, x >= i
      for(i = 1; i < fieldCount; i++)
         {
         // Select the item at the beginning of the as yet unsorted section
         int v = indexArray[i];
         CField *vField = fieldArray[v];

         // Work backwards through the array, finding where v should go
         int j = i;

         // If this element is greater than v, move it up one
         while (j > 0 && *(fieldArray[indexArray[j-1]]) > *vField)
            {
            indexArray[j] = indexArray[j-1];
            j--;
            }

         // Stopped when a[j-1] <= v, so put v at position j
         indexArray[j] = v;
         }
      }
}

//////////////////////////////////////////////////////////////////////
// Mutator Functions
//////////////////////////////////////////////////////////////////////

void CFrame::setMediaFormat(const CMediaFormat* newMediaFormat)
{
   mediaFormat = newMediaFormat;
}

void CFrame::setMediaWritten(bool newFlag)
{
   int fieldCount = getTotalFieldCount();

   for (int i = 0; i < fieldCount; ++i)
      {
      getBaseField(i)->setMediaWritten(newFlag);
      }
}


void CFrame::setTimecode(const CTimecode &newTimecode)
{
   *timecode = newTimecode;
}


//////////////////////////////////////////////////////////////////////
// Accessor Functions
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    doesMediaStorageExist
//
// Description: Determines if media storage has been allocated this frame.
//              Media storage must exist for all fields in the frame for
//              this function to return true.
//
// Arguments:   None
//
// Returns:     Returns true if media storage exists for all fields in
//              the this frame.  Returns false if one or more fields
//              do not have allocated media storage
//
//------------------------------------------------------------------------
bool CFrame::doesMediaStorageExist()
{
   int totalFieldCount = getTotalFieldCount();
   if (totalFieldCount < 1)
      return false;  // no fields, so there can't be media storage

   for (int fieldIndex = 0; fieldIndex < totalFieldCount; ++fieldIndex)
      {
      CField *field = getBaseField(fieldIndex);
      if (field == 0 || !field->doesMediaStorageExist())
         return false;  // one strike and yur out!
      }

   return true;  // all fields have media storage
}

//------------------------------------------------------------------------
//
// Function:    isMediaWritten
//
// Description: Determines if media has been written for this frame.
//              Media storage must exist for all fields in the frame for
//              this function to return true.
//
// Arguments:   None
//
// Returns:     Returns true if media storage exists and has been written
//              for all fields in this frame.  Returns false if one or more
//              fields do not have allocated media storage or have not
//              been written
//
//------------------------------------------------------------------------
bool CFrame::isMediaWritten()
{
   int totalFieldCount = getTotalFieldCount();
   if (totalFieldCount < 1)
      return false;  // no fields, so there can't be media storage

   for (int fieldIndex = 0; fieldIndex < totalFieldCount; ++fieldIndex)
      {
      CField *field = getBaseField(fieldIndex);
      if (field == 0 || !field->doesMediaStorageExist() ||
          !field->isMediaWritten())
         return false;  // one strike and yur out!
      }

   return true;  // all fields have media storage and have been written
}

//------------------------------------------------------------------------
//
// Function:    isDirty
//
// Description: Determines if any fields in this frame have the 'dirty'
//              bit set. This is only really useful for version clips.
//
// Arguments:   None
//
// Returns:     Returns true if any of the fields have the 'dirty' bit set,
//              else returns false.
//
//------------------------------------------------------------------------
bool CFrame::isDirty()
{
   int totalFieldCount = getTotalFieldCount();
   if (totalFieldCount < 1)
      return false;  // no fields, so can't be dirty!

   for (int fieldIndex = 0; fieldIndex < totalFieldCount; ++fieldIndex)
      {
      CField *field = getBaseField(fieldIndex);
      if (field != 0 && field->isDirtyMedia())
         return true;  // All we need is one dirty field!
      }

   return false;  // all fields are clean
}

//------------------------------------------------------------------------
//
// Function:    getBaseField
//
// Description: Get a pointer to a particular field in a frame given the
//              field index.
//
// Arguments:   int fieldIndex  Index of the field, 0 is the first field
//
// Returns:     Pointer to a CField object for the specified index
//              If the field index is out-of-range, returns a NULL pointer
//
//------------------------------------------------------------------------
CField* CFrame::getBaseField(int fieldIndex)
{
   // Range-check caller's fieldIndex argument
   if (fieldIndex < 0 || fieldIndex >= (int)fieldList.size())
      return 0;

   return fieldList[fieldIndex];
}

MTI_UINT32 CFrame::getDataSizeAtField(int fieldNumber)
{
   CField *field = getBaseField(fieldNumber);

   if (field == 0)
      return 0;

   return (field->getDataSize());
}

int CFrame::getInvisibleFieldCount() const
{
   // Return number of invisible fields in field list
   return getTotalFieldCount() - getVisibleFieldCount();
}

const CMediaFormat* CFrame::getMediaFormat() const
{
   // Return pointer to CMediaFormat subtype instance
   return mediaFormat;
}

CTimecode CFrame::getTimecode() const
{
   // Create a new timecode and return to caller
   return(CTimecode(*timecode));
}

void CFrame::getTimecode(CTimecode& destinationTimecode)
{
   // Copy frame's timecode to caller's timecode
   destinationTimecode = *timecode;
}

int CFrame::getTotalFieldCount() const
{
   // Return number of fields in the field list, which is the total
   // of the number of visible and invisible fields
   return fieldList.size();
}

int CFrame::getVisibleFieldCount() const
{
   // Return number of visible fields: 1 if progressive, 2 if interlaced
   // Needs access to frame's mediaFormat

   if (mediaFormat)
      return mediaFormat->getFieldCount();
   else
      return 0;
}


//////////////////////////////////////////////////////////////////////
// Miscellaneous Functions
//////////////////////////////////////////////////////////////////////

CFrame& CFrame::operator=(CFrame &frame)
{
   // Don't want to implement this assignment operator
   return *this;
}

void CFrame::deleteFieldList()
{
   // Iterate through field list, deleting each field
   for (FieldList::iterator field = fieldList.begin();
        field < fieldList.end();
        ++field)
      {
      delete *field;
      }

   // Delete all elements from the field list
   fieldList.clear();
}


void CFrame::addField(CField *field)
{
   fieldList.push_back(field);
}


//////////////////////////////////////////////////////////////////////
// Functions for clip2clip hack
//////////////////////////////////////////////////////////////////////

bool CFrame::isWriteProtected()
{
   // Iterate through field list, checking write protection on each field
   for (FieldList::iterator field = fieldList.begin();
        field < fieldList.end();
        ++field)
      {
      if ((*field)->isWriteProtectedMedia())
         return true;
      }
   return false;
}


void CFrame::allocateDirtyMediaStorageIfNecessary(
            const CMediaStorageList *mediaStorageList)
{
   // Iterate through field list, allocating dirty media for each field
   for (FieldList::iterator field = fieldList.begin();
        field < fieldList.end();
        ++field)
      {
      if ((*field)->isWriteProtectedMedia())
         (*field)->allocateDirtyMediaStorage(mediaStorageList,
                                             *getMediaFormat());
      }
}

CMediaAllocator *CFrame::getDirtyMediaStorageAllocator(
                                          CMediaStorageList &mediaStorageList,
                                          int &status)
{
   status = 0;
   int fieldCount = getTotalFieldCount();

   int dirtyMediaStorageIndex = mediaStorageList.getDirtyMediaStorageIndex();
   if (dirtyMediaStorageIndex < 0)
      {
      status = CLIP_ERROR_NO_DIRTY_MEDIA_STORAGE_SPECIFIED; // there wasn't one
      return NULL;
      }

   const CMediaStorage *dirtyMediaStorage =
                    mediaStorageList.getMediaStorage(dirtyMediaStorageIndex);
   if (dirtyMediaStorage == NULL)
      {
      status =  CLIP_ERROR_NO_DIRTY_MEDIA_STORAGE_SPECIFIED; // shouldn't happen
      return NULL;
      }

   CMediaAccess *mediaAccess = dirtyMediaStorage->getMediaAccessObj();
   if (mediaAccess == NULL)
      {
      status =  CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR;        // shouldn't happen
      return NULL;
      }

   // Timecode is same for all fields, so only need to look at first one
   CField *field = getBaseField(0);
   if (field == NULL)
      {
      status = CLIP_ERROR_UNEXPECTED_NULL_RETURN;
      return NULL;
      }

   CMediaLocation oldMediaLocation = field->getMediaLocation();
   int FieldNameAsInteger = 0;
   const char *fieldName = oldMediaLocation.getFieldName();
   if (fieldName != NULL)
      {
      MTIistringstream istr;
      istr.str(fieldName);
      istr >> FieldNameAsInteger;
      }
   CTimecode timecode(FieldNameAsInteger);

   CMediaAllocator *mediaAllocator = mediaAccess->allocateOneFrame(*mediaFormat,
                                                                   timecode,
                                                                   fieldCount);
   if (mediaAllocator == NULL)
      {
      int theErrorCode = theError.getError();
      if (theErrorCode == 0)
         theErrorCode = CLIP_ERROR_MEDIA_ALLOCATION_FAILED;
      status =  CLIP_ERROR_MEDIA_ALLOCATION_FAILED;
      return NULL;
      }

   return mediaAllocator;
}


// ---------------------------------------------------------------------------
//**Clip2clip hack
int CFrame::assignDirtyMediaStorage(CMediaAllocator &mediaAllocator,
                                    DirtyMediaAssignmentUndoList &undoList)
{
   undoList.clear();

   int retVal = 0;
   int fieldCount = getTotalFieldCount();

   // Set flags for each field in the frame
   for (int fieldIndex = 0;
        fieldIndex < fieldCount && retVal == 0;
        ++fieldIndex)
      {
      CField *field = getBaseField(fieldIndex);
      if (field == NULL)
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

      undoList[fieldIndex] = DirtyMediaAssignmentUndoItem(*field);

      CMediaLocation oldMediaLocation = field->getMediaLocation();
      CMediaLocation newMediaLocation;
      mediaAllocator.getNextFieldMediaAllocation(newMediaLocation);
      newMediaLocation.setFieldName(oldMediaLocation.getFieldName());
      newMediaLocation.setFieldNumber(oldMediaLocation.getFieldNumber());

      retVal = field->assignDirtyMediaStorage(newMediaLocation);

      } // for each field in the frame

   return retVal;
}


// ---------------------------------------------------------------------------
//**Clip2clip hack
int CFrame::unassignDirtyMediaStorage(DirtyMediaAssignmentUndoList &undoList)
{
   // Undo dirty media settings for each field in the frame
   int fieldCount = getTotalFieldCount();
   for (int fieldIndex = 0; fieldIndex < fieldCount; ++fieldIndex)
      {
      CField *field = getBaseField(fieldIndex);
      if (field == NULL)
         return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

      if (undoList.find(fieldIndex) != undoList.end())
          {
          undoList[fieldIndex].undo(*field);
          }
      } // for each field in the frame

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Testing and Debugging Functions
//////////////////////////////////////////////////////////////////////

void CFrame::dump(MTIostringstream& str)
{
   CTimecode tmpTimecode = getTimecode();
   int visibleFieldCount = getVisibleFieldCount();
   int invisibleFieldCount = getInvisibleFieldCount();
   int fieldListSize = (int) fieldList.size();

   str << tmpTimecode
       << " Field Counts: Visible: " << visibleFieldCount
       << " Invisible: " << invisibleFieldCount
       << " fieldList size: " << fieldListSize << std::endl;

   int indexArray[10];
   CField *fieldArray[10];
   int fieldCount = getTotalFieldCount();
   sortFieldsByMediaLocation(fieldCount, indexArray, fieldArray);
   str << " Fields Sorted by Media Location: ";

   for (int i = 0; i < fieldCount; i++)
      {
      str << indexArray[i] << " ";
      }
   str << std::endl;

   // Iterate through field list, dumping each field
   FieldList::iterator field;
   int i;
   for (field = fieldList.begin(), i = 0;
        field < fieldList.end();
        ++field, ++i)
      {
      str << "  Field " << i << ": ";
      (*field)->dump(str);
      }
}
