// Clip3TrackFile.cpp: implementation of the CFieldFileRecord, CFrameFileRecord,
//                     CFrameListFileReader and CFrameListFileWriter classes.
//
// Created by: John Starr, November 5, 2001
//
/* CVS Info:
$Header: /usr/local/filmroot/clip/source/Clip3TrackFile.cpp,v 1.10 2005/06/24 21:40:19 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "Clip3TrackFile.h"

#include "Clip3.h"
#include "Clip3Track.h"
#include "err_clip.h"
#include "FileScanner.h"
#include "IniFile.h"
#include "MediaStorage3.h"
#include "StructuredFileStream.h"
#include "TimecodeFileRecord.h"
#include "timecode.h"
#include <errno.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFieldFileRecord::CFieldFileRecord(MTI_UINT32 type, MTI_UINT16 version)
: CFileRecord(type, version),
  mediaStorageIndex(0), dataIndex(0), dataSize(0), fieldName(0), flags(0),
  fieldListEntryIndex(-1)
{
   // Write zeroes to reserved area
   for (int i = 0; i < FIELD3_RECORD_RESERVED_COUNT; ++i)
      {
      fieldReserved[i] = 0;
      }
}

CFieldFileRecord::~CFieldFileRecord()
{

}

//////////////////////////////////////////////////////////////////////
// Read/Write functions
//////////////////////////////////////////////////////////////////////

int CFieldFileRecord::read(CStructuredStream &stream)
{
   int retVal;

   // Read mediaStorageIndex field
   if ((retVal = stream.readUInt16(&mediaStorageIndex)) != 0)
      return retVal;

   // Read dataIndex field
   if ((retVal = stream.readInt64(&dataIndex)) != 0)
      return retVal;

   // Read dataSize field
   if ((retVal = stream.readUInt32(&dataSize)) != 0)
      return retVal;

   // Read fieldName field
   if ((retVal = fieldName.read(stream)) != 0)
      return retVal;

   // Read flags field
   if ((retVal = stream.readUInt32(&flags)) != 0)
      return retVal;

   // Read the reserved words
   for (int i = 0; i < FIELD3_RECORD_RESERVED_COUNT; ++i)
      {
      if((retVal = stream.readUInt32(&(fieldReserved[i]))) != 0)
         return retVal;
      }

   // This is only relevant when the record version is high enough,
   // but it is copied all of the time anyway
   fieldListEntryIndex = (MTI_INT32)fieldReserved[0];

   return 0;
}

int CFieldFileRecord::write(CStructuredStream &stream)
{
   int retVal;

   // Write record preamble
   if((retVal = CFileRecord::write(stream)) != 0)
      return retVal;

   // Write mediaStorageIndex field
   if ((retVal = stream.writeUInt16(mediaStorageIndex)) != 0)
      return retVal;

   // Write dataIndex field
   if ((retVal = stream.writeInt64(dataIndex)) != 0)
      return retVal;

   // Write dataSize field
   if ((retVal = stream.writeUInt32(dataSize)) != 0)
      return retVal;

   // Write fieldName field
   if ((retVal = fieldName.write(stream)) != 0)
      return retVal;

   // Write flags field
   if ((retVal = stream.writeUInt32(flags)) != 0)
      return retVal;

   // Write reserved area
   fieldReserved[0] = fieldListEntryIndex;  // real data, not reserved
   for (int i = 0; i < FIELD3_RECORD_RESERVED_COUNT; ++i)
      {
      if((retVal = stream.writeUInt32(fieldReserved[i])) != 0)
         return retVal;
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Accessor Functions
//////////////////////////////////////////////////////////////////////

MTI_INT64 CFieldFileRecord::getDataIndex() const
{
   return dataIndex;
}

MTI_UINT32 CFieldFileRecord::getDataSize() const
{
   return dataSize;
}

MTI_UINT32 CFieldFileRecord::getFieldFlags() const
{
   return flags;
}

MTI_INT32 CFieldFileRecord::getFieldListEntryIndex() const
{
   // WARNING: the returned value is only meaningful if the
   //          the derived classes' record version is high enough,
   //          otherwise the default value is 0
   
   return fieldListEntryIndex;
}

const string& CFieldFileRecord::getFieldName() const
{
   return fieldName.getString();
}

MTI_UINT16 CFieldFileRecord::getMediaStorageIndex() const
{
   return mediaStorageIndex;
}

//////////////////////////////////////////////////////////////////////
// Mutator Functions
//////////////////////////////////////////////////////////////////////

void CFieldFileRecord::setDataIndex(MTI_INT64 newDataIndex)
{
   dataIndex = newDataIndex;
}

void CFieldFileRecord::setDataSize(MTI_UINT32 newDataSize)
{
   dataSize = newDataSize;
}

void CFieldFileRecord::setFieldFlags(MTI_UINT32 newFieldFlags)
{
   flags = newFieldFlags;
}

void CFieldFileRecord::setFieldListEntryIndex(MTI_INT32 newIndex)
{
   fieldListEntryIndex = newIndex;
}

void CFieldFileRecord::setFieldName(const char* newFieldName)
{
   fieldName.setString(newFieldName);
}

void CFieldFileRecord::setMediaStorageIndex(MTI_UINT16 newMediaStorageIndex)
{
   mediaStorageIndex = newMediaStorageIndex;
}

//////////////////////////////////////////////////////////////////////

void CFieldFileRecord::calculateRecordByteCount()
{
   // Total the size of all the fields in the record

   CFileRecord::calculateRecordByteCount();

   MTI_UINT32 newByteCount = getRecordByteCount();

   newByteCount += sizeof(mediaStorageIndex);     

   newByteCount += sizeof(dataIndex); 
   newByteCount += sizeof(dataSize); 

   newByteCount += fieldName.getFieldSize();

   newByteCount += sizeof(flags);

   newByteCount += sizeof(fieldReserved);

   // Update the object with the calculated size
   setRecordByteCount(newByteCount);
}


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFrameFileRecord::CFrameFileRecord(MTI_UINT32 type, MTI_UINT16 version)
: CFileRecord(type, version), 
  fieldCount(0),
  timecodeRecord(0)
{

}

CFrameFileRecord::~CFrameFileRecord()
{
   delete timecodeRecord;
}

//////////////////////////////////////////////////////////////////////
// Read and Write functions
//////////////////////////////////////////////////////////////////////

// Note: generic file error codes are used because they will be replaced
//       by more meaningful codes by the types derived from CFrameFileRecord

int CFrameFileRecord::read(CStructuredStream &stream)
{
   int retVal;

   // Read a CFileRecord, presumably a CTimecodeFileRecord
   CFileRecord *newRecordPtr;
   if ((retVal = CFileRecord::readRecord(stream, &newRecordPtr)) != 0)
       return retVal;

   // Check to make sure new CFileRecord actually is a CTimecodeFileRecord
   //   If it is, assign pointer to member variable timecodeRecord
   //   If it is not, return error
   if (!(newRecordPtr) || !newRecordPtr->isRecordType(FR_RECORD_TYPE_TIMECODE))
      return CLIP_ERROR_FILE_READ_FAILED;

   timecodeRecord = static_cast<CTimecodeFileRecord*>(newRecordPtr);

   // Read the fieldCount field
   if ((retVal = stream.readUInt8(&fieldCount)) != 0)
      return retVal;

   // Read the reserved words
   for (int i = 0; i < FRAME3_RECORD_RESERVED_COUNT; ++i)
      {
      if((retVal = stream.readUInt32(&(frameReserved[i]))) != 0)
         return retVal;
      }

   return 0;
}

int CFrameFileRecord::write(CStructuredStream &stream) 
{
   int retVal;

   // Write record preamble
   if ((retVal = CFileRecord::write(stream)) != 0)
      return CLIP_ERROR_FILE_WRITE_FAILED;

   // Write the timecode field
   if ((retVal = timecodeRecord->write(stream)) != 0)
       return retVal;

   // Write the fieldCount field
   if ((retVal = stream.writeUInt8(fieldCount)) != 0)
      return retVal;

   // Write zeroes to reserved area
   for (int i = 0; i < FRAME3_RECORD_RESERVED_COUNT; ++i)
      {
      if((retVal = stream.writeUInt32(0)) != 0)
         return retVal;
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Accessor functions
//////////////////////////////////////////////////////////////////////

MTI_UINT8 CFrameFileRecord::getFieldCount() const
{
   return fieldCount;
}

void CFrameFileRecord::getTimecode(CTimecode &timecode) const
{
   if (timecodeRecord == 0)
      timecodeRecord = new CTimecodeFileRecord;

   timecodeRecord->getTimecode(timecode);
}

const CTimecode& CFrameFileRecord::getTimecode() const
{
   if (timecodeRecord == 0)
      timecodeRecord = new CTimecodeFileRecord;

   return timecodeRecord->getTimecode();
}

//////////////////////////////////////////////////////////////////////
// Mutator functions
//////////////////////////////////////////////////////////////////////

void CFrameFileRecord::setFieldCount(MTI_UINT8 newFieldCount)
{
   fieldCount = newFieldCount;
}

void CFrameFileRecord::setTimecode(const CTimecode &newTimecode)
{
   if (timecodeRecord == 0)
      timecodeRecord = new CTimecodeFileRecord;

   timecodeRecord->setTimecode(newTimecode);
}

void CFrameFileRecord::calculateRecordByteCount()
{
   // Total the size of all the fields in the record

   CFileRecord::calculateRecordByteCount();

   MTI_UINT32 newByteCount = getRecordByteCount();

   // Calculate size of timecode record before adding its size
   timecodeRecord->calculateRecordByteCount();
   newByteCount += timecodeRecord->getRecordByteCount();

   newByteCount += sizeof(fieldCount);

   newByteCount += sizeof(frameReserved);

   // Update the object with the calculated size
   setRecordByteCount(newByteCount);
}


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
// CFrameListFileReader Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFrameListFileReader::CFrameListFileReader()
: stream(0), fileScanner(0)
{

}

CFrameListFileReader::~CFrameListFileReader()
{
   closeFrameListFile();
}

//////////////////////////////////////////////////////////////////////
//  
//////////////////////////////////////////////////////////////////////

int CFrameListFileReader::openFrameListFile(const string& frameListFileName)
{
   int retVal;

   closeFrameListFile();

   stream = new CStructuredFileStream;

   // Open file with stream
   if (stream->open(frameListFileName) != 0)
      {
      // ERROR: Could not open stream with frame list file name
      TRACE_0(errout << "ERROR: Unable to open Frame List file "
                     << frameListFileName << endl;
              errout << " because " << strerror(errno) << endl);
      delete stream;
      stream = 0;
      return CLIP_ERROR_FILE_OPEN_FAILED;
      }

   // Create an instance of a CFileScanner and initialize
   fileScanner = new CFileScanner;
   retVal = fileScanner->initForReading(stream);
   if (retVal != 0)
      {
      // ERROR: Could not initialize CFileScanner for reading
      //        Possibly because file was not really a Track File
      closeFrameListFile();    // Cleanup
      return retVal;
      }

   return 0;
}

void CFrameListFileReader::closeFrameListFile()
{
   if (fileScanner)
      {
      delete fileScanner;
      fileScanner = 0;
      }

   if (stream)
      {
      stream->close();
      delete stream;
      stream = 0;
      }
}

//////////////////////////////////////////////////////////////////////
//  
//////////////////////////////////////////////////////////////////////

void CFrameListFileReader::
setBaseFieldFromFileRecord(CField& field, CFieldList *newFieldList,
                           const CMediaStorageList& mediaStorageList,
                           const CFieldFileRecord &fieldRecord)
{ 
   // Get CMediaAccess subtype pointer given the Media Storage Index
   // in the CFieldFileRecord
   int mediaStorageIndex = fieldRecord.getMediaStorageIndex();
   CMediaStorage* mediaStorage;
   mediaStorage = mediaStorageList.getMediaStorage(mediaStorageIndex);
   CMediaAccess* mediaAccess = 0;
   if (mediaStorage)
      mediaAccess = mediaStorage->getMediaAccessObj();

   // Set media location attributes from field record
   CMediaLocation mediaLocation;
   mediaLocation.setMediaAccess(mediaAccess);
   mediaLocation.setDataIndex(fieldRecord.getDataIndex());
   mediaLocation.setDataSize(fieldRecord.getDataSize());
   mediaLocation.setFieldName(fieldRecord.getFieldName().c_str());
   mediaLocation.setFieldNumber(fieldRecord.getFieldListEntryIndex());
   // The Field record's flags are in a 32-bit value that holds the
   // CField's flags in lower 16 bits and the CMediaLocation's flags
   // in the upper 16 bits
   MTI_UINT32 fieldRecordFlags = fieldRecord.getFieldFlags();
   field.setFieldFlags(fieldRecordFlags & 0xFFFF);
   mediaLocation.setFlags(fieldRecordFlags >> 16);

   // Set media location in new CField object
   field.setMediaLocation(mediaLocation);

   // Set Field List Entry reference, which is only relevant when
   // the .trk file's version is high enough to support the field list
   // entry index
   if (newFieldList == 0)
      {
      field.setFieldListEntryIndex(0, -1);   // old version .trk file
                                             // use dummy reference
      }
   else
      {
      field.setFieldListEntryIndex(newFieldList,
                                   fieldRecord.getFieldListEntryIndex());
      }
}

void CFrameListFileReader::
setBaseFrameFromFileRecord(CFrame& frame,
                           const CFrameFileRecord &frameRecord)
{
   // Copy the timecode from the frame record
   frame.setTimecode(frameRecord.getTimecode());
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//               CFrameListFileWriter Class
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFrameListFileWriter::CFrameListFileWriter()
{

}

CFrameListFileWriter::~CFrameListFileWriter()
{

}

//////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////

void CFrameListFileWriter::setBaseFieldToFileRecord(const CField& field,
                                    const CMediaStorageList &mediaStorageList,
                                               CFieldFileRecord &fieldRecord)
{
   // Set media location attributes from field to the file record
   const CMediaLocation& mediaLocation = field.getMediaLocation();

   CMediaAccess* mediaAccess = mediaLocation.getMediaAccess();
   int mediaStorageIndex = mediaStorageList.getMediaStorageIndex(mediaAccess);
   fieldRecord.setMediaStorageIndex(mediaStorageIndex);

   fieldRecord.setDataIndex(mediaLocation.getDataIndex());

   fieldRecord.setDataSize(mediaLocation.getDataSize());

   fieldRecord.setFieldName(mediaLocation.getFieldName());

   fieldRecord.setFieldListEntryIndex(field.getFieldListEntryIndex());

   // Field File record's flags are in a 32-bit value that holds the
   // CField's flags in lower 16 bits and the CMediaLocation's flags
   // in the upper 16 bits
   fieldRecord.setFieldFlags((MTI_UINT32)(field.getFieldFlags())
                             | ((MTI_UINT32)(mediaLocation.getFlags()) << 16));
}

void CFrameListFileWriter::setBaseFrameToFileRecord(const CFrame& frame,
                                              CFrameFileRecord &frameRecord)
{
   // Copy the timecode from the frame to the file record
   frameRecord.setTimecode(frame.getTimecode());
}


