// RawFileIO.cpp: implementation of the CRawFileIO class.
//
//////////////////////////////////////////////////////////////////////

#include <errno.h>

#if defined(__sgi) || defined(__linux)  // UNIX
#include <fcntl.h>
#include <unistd.h>

#elif defined(_WIN32)      // Win32 API

#else
#error Unknown Compiler
#endif

#include <iomanip>
#include "RawFileIO.h"
#include "err_clip.h"
#include "SafeClasses.h"
#include "HRTimer.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRawFileIO::CRawFileIO(int newOverlappedReadCount, int newOverlappedWriteCount,
                       int newFileHandleIndex, int newTimingReport)
 : m_totalTimeMin(1000000.), m_totalTimeMax(0.0),
   m_lockTimeMin(1000000.), m_lockTimeMax(0.0),
   m_readTimeMin(1000000.), m_readTimeMax(0.0),
   m_readCount(0)

{
   // Reporting/Debugging information
   m_fileHandleIndex = newFileHandleIndex;
   m_timingReport = newTimingReport;

   m_overlappedReadCount = newOverlappedReadCount;
   if (m_overlappedReadCount < 1)
      m_overlappedReadCount = 1;
   else if (m_overlappedReadCount > RF_MAX_OVERLAPPED_IO_COUNT)
      m_overlappedReadCount = RF_MAX_OVERLAPPED_IO_COUNT;

   m_overlappedWriteCount = newOverlappedWriteCount;
   if (m_overlappedWriteCount < 1)
      m_overlappedWriteCount = 1;
   else if (m_overlappedWriteCount > RF_MAX_OVERLAPPED_IO_COUNT)
      m_overlappedWriteCount = RF_MAX_OVERLAPPED_IO_COUNT;

   int overlappedIOCount = std::max(m_overlappedReadCount, m_overlappedWriteCount);

   m_doOverlappedIO = (overlappedIOCount > 1);

#if defined(__sgi) || defined(__linux)        // UNIX
   m_handle = -1;
#elif defined(_WIN32)      // Win32 API, VC++ or BC++
   m_handle = INVALID_HANDLE_VALUE;

   // Initialize for possible overlapped I/O
   for (int i = 0; i < RF_MAX_OVERLAPPED_IO_COUNT; ++i)
      m_overlappedEventArray[i] = INVALID_HANDLE_VALUE;

   m_overlappedArray = new OVERLAPPED[overlappedIOCount];

   for (int i = 0; i < overlappedIOCount; ++i)
      {
      m_overlappedEventArray[i] = CreateEvent(NULL, TRUE, FALSE, NULL);
      memset(&(m_overlappedArray[i]), 0, sizeof(OVERLAPPED));
      m_overlappedArray[i].hEvent = m_overlappedEventArray[i];
      }

#else
#error Unknown Compiler
#endif
}

CRawFileIO::~CRawFileIO()
{
#if defined(_WIN32)      // Win32 API, VC++ or BC++
   delete [] m_overlappedArray;

   int overlappedIOCount = std::max(m_overlappedReadCount, m_overlappedWriteCount);
   for (int i = 0; i < overlappedIOCount; ++i)
      CloseHandle(m_overlappedEventArray[i]);

#endif

   close();
}

//////////////////////////////////////////////////////////////////////

int CRawFileIO::open(const string& newFileName)
{
   m_fileName = newFileName;
   
#if defined(__sgi) || defined(__linux)        // UNIX
  m_handle = ::open(m_fileName.c_str(), O_RDWR|O_BINARY, 0666);
  if (m_handle == -1)
      {
      TRACE_3(errout << "ERROR: CRawFileIO::open: open failed for "
                     << "       VideoStore " << m_fileName << endl
                     << "       because " << strerror(errno) << endl);
      return CLIP_ERROR_RAW_DISK_OPEN_FAILED;
      }
  else
     return 0;

#elif defined(_WIN32)      // Win32 API, VC++ or BC++
   if (m_doOverlappedIO)
      {
      // Open file for overlapped I/O.  Once opened for overlapped I/O,
      // then all I/O must be overlapped
      m_handle = CreateFile(m_fileName.c_str(), GENERIC_READ | GENERIC_WRITE,
                            FILE_SHARE_READ | FILE_SHARE_WRITE,
                            0, OPEN_EXISTING,
                            FILE_FLAG_NO_BUFFERING | FILE_FLAG_OVERLAPPED, 0);
      }
   else
      {
      // Open file for non-overlapped I/O.
      m_handle = CreateFile(m_fileName.c_str(), GENERIC_READ | GENERIC_WRITE,
                            FILE_SHARE_READ | FILE_SHARE_WRITE,
                            0, OPEN_EXISTING, FILE_FLAG_NO_BUFFERING, 0);
      }

   if (m_handle == INVALID_HANDLE_VALUE)
      {
      TRACE_0(errout << "ERROR: CRawFileIO::open: CreateFile failed for "
                     << "VideoStore " << m_fileName << endl
                     << "       because "
                     << GetLastSystemErrorMessage() << endl);

      return CLIP_ERROR_RAW_DISK_OPEN_FAILED;
      }
   else
      return 0; // success

#else
#error Unknown Compiler
#endif

} // open

int CRawFileIO::openReadOnly(const string& newFileName)
{
   m_fileName = newFileName;
   
#if defined(__sgi) || defined(__linux)        // UNIX
  m_handle = ::open(m_fileName.c_str(), O_RD|O_BINARY, 0666);
  if (m_handle == -1)
      {
      TRACE_3(errout << "ERROR: CRawFileIO::open: open failed for "
                     << "       VideoStore " << m_fileName << endl
                     << "       because " << strerror(errno) << endl);
      return CLIP_ERROR_RAW_DISK_OPEN_FAILED;
      }
  else
     return 0;

#elif defined(_WIN32)      // Win32 API, VC++ or BC++
   if (m_doOverlappedIO)
      {
      // Open file for overlapped I/O.  Once opened for overlapped I/O,
      // then all I/O must be overlapped
      m_handle = CreateFile(m_fileName.c_str(), GENERIC_READ,
                            FILE_SHARE_READ | FILE_SHARE_WRITE,
                            0, OPEN_EXISTING,
                            FILE_FLAG_NO_BUFFERING | FILE_FLAG_OVERLAPPED, 0);
      }
   else
      {
      // Open file for non-overlapped I/O, read-only.
      m_handle = CreateFile(m_fileName.c_str(), GENERIC_READ,
                            FILE_SHARE_READ | FILE_SHARE_WRITE,
                            0, OPEN_EXISTING,
                            FILE_FLAG_NO_BUFFERING, 0);
      }

   if (m_handle == INVALID_HANDLE_VALUE)
      {
      TRACE_0(errout << "ERROR: CRawFileIO::openReadOnly: CreateFile failed for "
                     << "VideoStore " << m_fileName << endl
                     << "       because "
                     << GetLastSystemErrorMessage() << endl);

      return CLIP_ERROR_RAW_DISK_OPEN_FAILED;
      }
   else
      return 0; // success

#else
#error Unknown Compiler
#endif

} // openReadOnly

// ---------------------------------------------------------------------------
int CRawFileIO::close()
{
#if defined(__sgi) || defined(__linux)        // UNIX
   if (m_handle != -1)
      {
      int retVal = ::close(m_handle);
      m_handle = -1;
      return(retVal);
      }
   else
      return 0;

#elif defined(_WIN32)      // Win32 API, VC++ or BC++
   if (m_handle != INVALID_HANDLE_VALUE)
      {
      BOOL retVal = CloseHandle(m_handle);
      m_handle = INVALID_HANDLE_VALUE;
      if(retVal)
         return 0;
      else
         return GetLastError();
      }

#else
#error Unknown Compiler
#endif
   return -1;

} // close

//////////////////////////////////////////////////////////////////////

int CRawFileIO::read(unsigned char *dataBuffer, MTI_INT64 offset, int size)
{
   CHRTimer hrt;
   double t1, t2, t3;
   hrt.Start();

   CAutoThreadLocker atl(m_rawFileIOThreadLock);// Lock thread until return

   t1 = hrt.Read();

#if defined(__sgi) || defined(__linux)    // UNIX

   // Standard read function that can read either raw disk or regular file.
   // Equivalent to a lseek64 followed by read
   return pread64(m_handle, dataBuffer, size, offset);

#elif defined(_WIN32)   // Defined by Microsoft and Borland compilers

   DWORD numberOfBytesRead;

   if (!m_doOverlappedIO)
      {
      // Video Store is opened for non-overlapped I/O
      DWORD retVal;
      retVal = SetFilePointer(m_handle, LOWLONGLONG(offset), ((LONG*)&offset)+1,
                              FILE_BEGIN);
      if (retVal == INVALID_SET_FILE_POINTER && GetLastError() != NO_ERROR)
         {
         TRACE_0(errout << "ERROR: CRawFileIO::read: "
                        << "SetFilePointer failed for " << endl
                        << "       VideoStore " << m_fileName << endl
                        << "       because "
                        << GetLastSystemErrorMessage());
         return -1;
         }

      t2 = hrt.Read();
      if (!ReadFile(m_handle, dataBuffer, size, &numberOfBytesRead, 0))
         {
         TRACE_0(errout << "ERROR: CRawFileIO::read: "
                        << "ReadFile failed for " << endl
                        << "       VideoStore " << m_fileName << endl
                        << "       because "
                        << GetLastSystemErrorMessage());
         return -1;
         }


      }
   else
      {
      // Video Store has been opened for overlapped I/O

      OVERLAPPED *overlapped = &(m_overlappedArray[0]);
      LARGE_INTEGER llOffset;
      llOffset.QuadPart = offset;
      overlapped->Offset = llOffset.LowPart;
      overlapped->OffsetHigh = llOffset.HighPart;

      t2 = hrt.Read();
      if (!ReadFile(m_handle, dataBuffer, size, &numberOfBytesRead, overlapped))
         {
         DWORD lastError = GetLastError();
         if (lastError == ERROR_IO_PENDING)
            {
            // The I/O is now pending, so wait for the operation to complete
            if (!GetOverlappedResult(m_handle, overlapped, &numberOfBytesRead,
                                     TRUE))
               {
               // The operation failed
               TRACE_0(errout << "ERROR: CRawFileIO::read: "
                              << "GetOverlappedResult failed for " << endl
                              << "       VideoStore " << m_fileName << endl
                              << "       because "
                              << GetLastSystemErrorMessage());
               return -1;
               }
            }
         else
            {
            // A failure of some sort
            TRACE_0(errout << "ERROR: CRawFileIO::read: "
                           << "ReadFile failed for " << endl
                           << "       VideoStore " << m_fileName << endl
                           << "       because "
                           << GetLastSystemErrorMessage());
            return -1;
            }
         }
      }

   t3 = hrt.Read();
   double readTime = t3-t2;
   if (readTime > 100.0)
      {
      TRACE_0 (errout << "SLOW READ:  " << readTime << " " << m_fileName);
      }

   // Report the min and max timing every m_timingReport reads
   if (m_timingReport > 0)
      {
      if (t3 > m_totalTimeMax)
         m_totalTimeMax = t3;
      else if (t3 < m_totalTimeMin)
         m_totalTimeMin = t3;
      if (t1 > m_lockTimeMax)
         m_lockTimeMax = t1;
      else if (t1 < m_lockTimeMin)
         m_lockTimeMin = t1;
      if (readTime > m_readTimeMax)
         m_readTimeMax = readTime;
      else if(readTime < m_readTimeMin)
         m_readTimeMin = readTime;
      if (++m_readCount >= m_timingReport)
         {
         TRACE_0(errout << "INFO: Read Time: " << m_fileName 
                        << " [" << m_fileHandleIndex << "] "
                        << std::fixed << std::setprecision(2)
                        << "Lock: " << m_lockTimeMin << " to " << m_lockTimeMax
                        << std::setprecision(1)
                        << " Read: " << m_readTimeMin << " to " << m_readTimeMax
                        << " Total: " << m_totalTimeMin << " to " << m_totalTimeMax);

         // Reset for next time
         m_totalTimeMin=m_lockTimeMin=m_readTimeMin=1000000.;
         m_totalTimeMax=m_lockTimeMax=m_readTimeMax=0.0;
         m_readCount=0;
         }
      }

   return(numberOfBytesRead);

#else
#error Unknown compiler
#endif

} // read

//////////////////////////////////////////////////////////////////////

int CRawFileIO::readMulti(unsigned char* dataBuffer[], MTI_INT64 offset[],
                          int size[],   int count)
{
   CAutoThreadLocker atl(m_rawFileIOThreadLock);// Lock thread until return

#if defined(__sgi) || defined(__linux)    // UNIX

   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;

#elif defined(_WIN32)   // Defined by Microsoft and Borland compilers

   CHRTimer hrt;

   if (!m_doOverlappedIO)
      {
      // Video Store is opened for non-overlapped I/O

      // Just iterate with single reads
      for (int i = 0; i < count; ++i)
         {
         if (read(dataBuffer[i], offset[i], size[i]) == -1)
            {
            return CLIP_ERROR_RAW_DISK_READ_FAILED;
            }
         }

      return 0;
      }
   else
      {
      // Video Store has been opened for overlapped I/O

      hrt.Start();

      DWORD numberOfBytesRead;
      int remainingCount = count;
      int groupIndex = 0;

      while (remainingCount > 0)
         {
         int readCount = std::min(remainingCount, m_overlappedReadCount);

         // Start the overlapped I/O operations
         for (int i = 0; i < readCount; ++i)
            {
            OVERLAPPED *overlapped = &(m_overlappedArray[i]);
            LARGE_INTEGER llOffset;
            llOffset.QuadPart = offset[groupIndex + i];
            overlapped->Offset = llOffset.LowPart;
            overlapped->OffsetHigh = llOffset.HighPart;

            if (!ReadFile(m_handle, dataBuffer[groupIndex + i],
                          size[groupIndex + i], &numberOfBytesRead, overlapped))
               {
               DWORD lastError = GetLastError();
               if (lastError != ERROR_IO_PENDING)
                  {
                  // A failure of some sort
                  TRACE_0(errout << "ERROR: CRawFileIO::readMulti: "
                                 << "ReadFile failed for " << endl
                                 << "       VideoStore " << m_fileName << endl
                                 << "       because "
                                 << GetLastSystemErrorMessage());
                  return CLIP_ERROR_RAW_DISK_READ_FAILED;
                  }
               }
            }

         // Wait for overlapped I/O operations to complete
         for (int i = 0; i < readCount; ++i)
            {
            OVERLAPPED *overlapped = &(m_overlappedArray[i]);
            if (!GetOverlappedResult(m_handle, overlapped, &numberOfBytesRead,
                                     TRUE))
               {
               // The operation failed
               TRACE_0(errout << "ERROR: CRawFileIO::readMulti: "
                              << "GetOverlappedResult failed for " << endl
                              << "       VideoStore " << m_fileName << endl
                              << "       because "
                              << GetLastSystemErrorMessage());
               return CLIP_ERROR_RAW_DISK_READ_FAILED;
               }
            }

         remainingCount -= readCount;
         groupIndex += readCount;
         }
      
      int iMilliSec = hrt.Read();
      if (iMilliSec > count * 100)
         {
         TRACE_0 (errout << "SLOW MULTI READ:  " << iMilliSec
                         << " " << m_fileName);
         }

      return 0;
      }

#else
#error Unknown compiler
#endif

} // readMulti

//////////////////////////////////////////////////////////////////////

int CRawFileIO::write(const unsigned char *dataBuffer, MTI_INT64 offset,
                      int size)
{
   CAutoThreadLocker atl(m_rawFileIOThreadLock);// Lock thread until return

#if defined(__sgi) || defined(__linux)    // UNIX

   // Standard write function that can write to either raw disk or regular file.
   // Equivalent to a lseek64 followed by write
   return pwrite64(m_handle, dataBuffer, size, offset);

#elif defined(_WIN32)  // Win32 API, Defined by Microsoft and Borland compilers

   DWORD numberOfBytesWritten;

   if (!m_doOverlappedIO)
      {
      // Video Store is opened for non-overlapped I/O
      DWORD retVal;
      retVal = SetFilePointer(m_handle, LOWLONGLONG(offset), ((LONG*)&offset)+1,
                              FILE_BEGIN);
      if (retVal == INVALID_SET_FILE_POINTER && GetLastError() != NO_ERROR)
         {
         TRACE_0(errout << "ERROR: CRawFileIO::write: "
                        << "SetFilePointer failed for " << endl
                        << "       VideoStore " << m_fileName << endl
                        << "       because "
                        << GetLastSystemErrorMessage());
         return -1;
         }

      if (!WriteFile(m_handle, dataBuffer, size, &numberOfBytesWritten, 0))
         {
         // Write operation failed

         // Note: This produces "The parameter is incorrect" when attempting to
         // write to StoreNext with improper alignment.

         TRACE_0(errout << "ERROR: CRawFileIO::write: "
                        << "WriteFile failed for " << endl
                        << "       VideoStore " << m_fileName << endl
                        << "       because "
                        << GetLastSystemErrorMessage());
         TRACE_0 (errout << "       " << "handle: " << (int)m_handle
                         << " dataBuffer: " << (int)dataBuffer
                         << " size: " << size);
         return -1;
         }

      return(numberOfBytesWritten);
      }
   else
      {
      // Video Store has been opened for overlapped I/O

      OVERLAPPED *overlapped = &(m_overlappedArray[0]);
      LARGE_INTEGER llOffset;
      llOffset.QuadPart = offset;
      overlapped->Offset = llOffset.LowPart;
      overlapped->OffsetHigh = llOffset.HighPart;

      if (!WriteFile(m_handle, dataBuffer, size, &numberOfBytesWritten,
                     overlapped))
         {
         DWORD lastError = GetLastError();
         if (lastError == ERROR_IO_PENDING)
            {
            // The I/O is now pending, so wait for the operation to complete
            if (!GetOverlappedResult(m_handle, overlapped,
                                     &numberOfBytesWritten, TRUE))
               {
               // The operation failed
               TRACE_0(errout << "ERROR: CRawFileIO::write: "
                              << "GetOverlappedResult failed for " << endl
                              << "       VideoStore " << m_fileName << endl
                              << "       because "
                              << GetLastSystemErrorMessage());
               return -1;
               }
            }
         else
            {
            // A failure of some sort
            TRACE_0(errout << "ERROR: CRawFileIO::write: "
                           << "WriteFile failed for " << endl
                           << "       VideoStore " << m_fileName << endl
                           << "       because "
                           << GetLastSystemErrorMessage());
            return -1;
            }
         }

      return(numberOfBytesWritten);
      }

#else
#error Unknown compiler
#endif

} // write

//////////////////////////////////////////////////////////////////////

int CRawFileIO::writeMulti(unsigned char* dataBuffer[],
                           MTI_INT64 offset[], int size[],  int count)
{
   CAutoThreadLocker atl(m_rawFileIOThreadLock);// Lock thread until return

#if defined(__sgi) || defined(__linux)    // UNIX

   return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;

#elif defined(_WIN32)   // Defined by Microsoft and Borland compilers

   if (!m_doOverlappedIO)
      {
      // Video Store is opened for non-overlapped I/O

      // Just iterate with single writes
      for (int i = 0; i < count; ++i)
         {
         if (write(dataBuffer[i], offset[i], size[i]) == -1)
            {             
            return CLIP_ERROR_RAW_DISK_WRITE_FAILED;
            }
         }

      return 0;
      }
   else
      {
      // Video Store has been opened for overlapped I/O

      DWORD numberOfBytesWritten;
      int remainingCount = count;
      int groupIndex = 0;

      while (remainingCount > 0)
         {
         int writeCount = std::min(remainingCount, m_overlappedWriteCount);

         // Start the overlapped I/O operations
         for (int i = 0; i < writeCount; ++i)
            {
            OVERLAPPED *overlapped = &(m_overlappedArray[i]);
            LARGE_INTEGER llOffset;
            llOffset.QuadPart = offset[groupIndex + i];
            overlapped->Offset = llOffset.LowPart;
            overlapped->OffsetHigh = llOffset.HighPart;

            if (!WriteFile(m_handle, dataBuffer[groupIndex + i],
                           size[groupIndex + i],
                           &numberOfBytesWritten, overlapped))
               {
               DWORD lastError = GetLastError();
               if (lastError != ERROR_IO_PENDING)
                  {
                  // A failure of some sort
                  TRACE_0(errout << "ERROR: CRawFileIO::writeMulti: "
                                 << "WriteFile failed for " << endl
                                 << "       VideoStore " << m_fileName << endl
                                 << "       because "
                                 << GetLastSystemErrorMessage());
                  return CLIP_ERROR_RAW_DISK_READ_FAILED;
                  }
               }
            }

         // Wait for overlapped I/O operations to complete
         for (int i = 0; i < writeCount; ++i)
            {
            OVERLAPPED *overlapped = &(m_overlappedArray[i]);
            if (!GetOverlappedResult(m_handle, overlapped, &numberOfBytesWritten,
                                     TRUE))
               {
               // The operation failed
               TRACE_0(errout << "ERROR: CRawFileIO::writeMulti: "
                              << "GetOverlappedResult failed for " << endl
                              << "       VideoStore " << m_fileName << endl
                              << "       because "
                              << GetLastSystemErrorMessage());
               return CLIP_ERROR_RAW_DISK_READ_FAILED;
               }
            }

         remainingCount -= writeCount;
         groupIndex += writeCount;
         }
      
      return 0;
      }

#else
#error Unknown compiler
#endif

} // writeMulti

//////////////////////////////////////////////////////////////////////


