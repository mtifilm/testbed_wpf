// TimecodeFileRecord.cpp: implementation of the CTimecodeFileRecord class.
//
/*
$Header: /usr/local/filmroot/clip/source/TimecodeFileRecord.cpp,v 1.4 2003/03/24 19:33:06 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "TimecodeFileRecord.h"
#include "err_clip.h"
#include "IniFile.h"
#include "StructuredStream.h"

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//
// CTimecodeFileRecord Class Implementation
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTimecodeFileRecord::CTimecodeFileRecord(MTI_UINT16 version)
: CFileRecord(FR_RECORD_TYPE_TIMECODE, version),
  timecode(1, 0)
{

}

CTimecodeFileRecord::~CTimecodeFileRecord()
{

}

int CTimecodeFileRecord::read(CStructuredStream &stream)
{
   int retVal;

   MTI_UINT8 framesPerSecond, flags, hours, minutes, seconds;

   // Read framesPerSecond field
   if((retVal = stream.readUInt8(&framesPerSecond)) != 0)
      return retVal;

   // Read flags field
   if((retVal = stream.readUInt8(&flags)) != 0)
      return retVal;

   if (framesPerSecond != 0)
      {
      // Read separate 8-bit hours, minutes, seconds and frames
      // Read hours field
      if((retVal = stream.readUInt8(&hours)) != 0)
         return retVal;

      // Read minutes field
      if((retVal = stream.readUInt8(&minutes)) != 0)
         return retVal;

      // Read seconds field
      if((retVal = stream.readUInt8(&seconds)) != 0)
         return retVal;

      // Read frames field
      MTI_UINT8 frames;
      if((retVal = stream.readUInt8(&frames)) != 0)
         return retVal;

      timecode.setFramesPerSecond(framesPerSecond);
      timecode.setFlags(flags);
      timecode.setTime(hours, minutes, seconds, frames);
      }
   else
      {
      // Read single 32-bit frames

      // Read frames field
      MTI_UINT32 frames;
      if((retVal = stream.readUInt32(&frames)) != 0)
         return retVal;

      timecode.setFramesPerSecond(framesPerSecond);
      timecode.setFlags(flags);
      timecode.setAbsoluteTime(frames);
      }

   return 0;
}

int CTimecodeFileRecord::write(CStructuredStream &stream)
{
   int retVal;
   
   // Write record preamble
   if ((retVal = CFileRecord::write(stream)) != 0)
      return retVal;

   // Write framesPerSecond field
   int framesPerSecond = timecode.getFramesPerSecond();
   if((retVal = stream.writeUInt8(framesPerSecond)) != 0)
      return retVal;

   // Write flags field
   if((retVal = stream.writeUInt8(timecode.getFlags())) != 0)
      return retVal;

   if (framesPerSecond != 0)
      {
      // Write separate 8-bit hours, minutes, seconds and frames

      // Write hours field
      if((retVal = stream.writeUInt8(timecode.getHours())) != 0)
         return retVal;

      // Write minutes field
      if((retVal = stream.writeUInt8(timecode.getMinutes())) != 0)
         return retVal;

      // Write seconds field
      if((retVal = stream.writeUInt8(timecode.getSeconds())) != 0)
         return retVal;

      // Write frames field
      if((retVal = stream.writeUInt8(timecode.getFrames())) != 0)
         return retVal;
      }
   else
      {
      // Write individual 32-bit frame counter

      // Write frames field
      if((retVal = stream.writeUInt32(timecode.getAbsoluteTime())) != 0)
         return retVal;
      }

   return 0;
}

void CTimecodeFileRecord::getTimecode(CTimecode &tc) const
{
   tc = timecode;
}

const CTimecode& CTimecodeFileRecord::getTimecode() const
{
   return timecode;
}

void CTimecodeFileRecord::setTimecode(const CTimecode &tc)
{
   timecode = tc;
}

CTimecodeFileRecord& CTimecodeFileRecord::operator =(const CTimecodeFileRecord &tcRecord)
{
   if(this != &tcRecord)
      {
      timecode = tcRecord.timecode;
      }

   return *this;
}

void CTimecodeFileRecord::calculateRecordByteCount()
{
   CFileRecord::calculateRecordByteCount();

   MTI_UINT32 newByteCount = getRecordByteCount();

   newByteCount += sizeof(MTI_UINT8);  // frames per second
   newByteCount += sizeof(MTI_UINT8);  // flags
   if (timecode.getFramesPerSecond() == 0)
      {
      // Single 32-bit frame counter
      newByteCount += sizeof(MTI_UINT32);
      }
   else
      {
      // Separate 8-bit hours, minutes, seconds and frames
      newByteCount += sizeof(MTI_UINT8);  // hours
      newByteCount += sizeof(MTI_UINT8);  // minutes
      newByteCount += sizeof(MTI_UINT8);  // seconds
      newByteCount += sizeof(MTI_UINT8);  // frames
      }

   // Update the object with the calculated size
   setRecordByteCount(newByteCount);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//
// CSMPTETimecodeFileRecord Class Implementation
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSMPTETimecodeFileRecord::CSMPTETimecodeFileRecord(MTI_UINT16 version)
: CFileRecord(FR_RECORD_TYPE_SMPTE_TIMECODE, version)
{

}

CSMPTETimecodeFileRecord::~CSMPTETimecodeFileRecord()
{

}

int CSMPTETimecodeFileRecord::read(CStructuredStream &stream)
{
   int retVal;

   MTI_UINT32 tmpTimecode, tmpUserData;

   // Read time code field
   if((retVal = stream.readUInt32(&tmpTimecode)) != 0)
      return retVal;

   // Read User Data field
   if((retVal = stream.readUInt32(&tmpUserData)) != 0)
      return retVal;

   timecode.setRawTimecode(tmpTimecode);
   timecode.setRawUserData(tmpUserData);

   return 0;
}

int CSMPTETimecodeFileRecord::write(CStructuredStream &stream)
{
   int retVal;
   
   // Write record preamble
   if ((retVal = CFileRecord::write(stream)) != 0)
      return retVal;

   // Write time code field
   if((retVal = stream.writeUInt32(timecode.getRawTimecode())) != 0)
      return retVal;

   // Write User Data field
   if((retVal = stream.writeUInt32(timecode.getRawUserData())) != 0)
      return retVal;


   return 0;
}

void CSMPTETimecodeFileRecord::getSMPTETimecode(CSMPTETimecode &tc) const
{
   tc = timecode;
}

const CSMPTETimecode& CSMPTETimecodeFileRecord::getSMPTETimecode() const
{
   return timecode;
}

void CSMPTETimecodeFileRecord::setSMPTETimecode(const CSMPTETimecode &tc)
{
   timecode = tc;
}

CSMPTETimecodeFileRecord&
CSMPTETimecodeFileRecord::operator =(const CSMPTETimecodeFileRecord &tcRecord)
{
   if(this != &tcRecord)
      {
      timecode = tcRecord.timecode;
      }

   return *this;
}

void CSMPTETimecodeFileRecord::calculateRecordByteCount()
{
   CFileRecord::calculateRecordByteCount();

   MTI_UINT32 newByteCount = getRecordByteCount();

   newByteCount += sizeof(MTI_UINT32);  // time code
   newByteCount += sizeof(MTI_UINT32);  // user data

   // Update the object with the calculated size
   setRecordByteCount(newByteCount);
}



