// MediaInterface.h: interface for the MediaInterface class.
//
// This class provides the low-level read, write and allocation 
// interface for all media storage types (raw disk, file system, etc.)
// for image data or whatever.
// 
//
//////////////////////////////////////////////////////////////////////

#ifndef MediaInterfaceH
#define MediaInterfaceH

#include <vector>
#include <string>
#include <iostream>

using std::vector;
using std::string;
using std::ostream;

#include "machine.h"
#include "cliplibint.h"
#include "ImageInfo.h"
#include "AudioFormat3.h"
#include "IniFile.h"

//////////////////////////////////////////////////////////////////////
// Forward declarations

class CIniFile;
class CImageFormat;
class CMediaAccess;
class CMediaAllocator;
class CMediaFormat;
class CMediaLocation;
class CTimecode;
class CDiskScheme;
class CImageRepositoryMediaAccess;

//////////////////////////////////////////////////////////////////////

// Note: When a new type is added to EMediaType, the function
// isMediaValid must be updated also

enum EMediaType
{
   MEDIA_TYPE_INVALID = 0,
   MEDIA_TYPE_RAW = 1,           // Raw disk partition (or a huge ordinary
                                 // file used like raw partition).
                                 // Can hold any type of media
   MEDIA_TYPE_AIFF_C = 2,        // Audio contained in AIFF-C type file
   MEDIA_TYPE_RAW_AUDIO = 3,     // File with unformatted audio data without
                                 // a header.
   MEDIA_TYPE_IMAGE_FILE_UYVY =4, // UYVY Image File
   MEDIA_TYPE_RAW_OGLV = 5,      // Raw disk partition managed under
                                 // OGLV.  Used to share media with
                                 // OGLV clips via "Bridge Code"
   MEDIA_TYPE_IMAGE_FILE_CINEON = 6,   // Cineon Image File
   MEDIA_TYPE_IMAGE_FILE_DPX = 7,      // DPX Image File
   MEDIA_TYPE_IMAGE_FILE_SGI = 8,      // SGI Image File
   MEDIA_TYPE_IMAGE_FILE_TIFF = 9,     // TIFF Image File
   MEDIA_TYPE_IMAGE_FILE_TGA = 10,     // Targa Image File
   MEDIA_TYPE_IMAGE_FILE_TGA2 = 11,    // Targa2 Image File
   MEDIA_TYPE_IMAGE_FILE_BMP = 12,     // BMP Image File
   MEDIA_TYPE_IMAGE_FILE_EXR = 13,     // BMP Image File
   MEDIA_TYPE_REPOSITORY_DPX_YUV = 14, // DEPRECATED - DPX file repository (YUV422)
   MEDIA_TYPE_REPOSITORY_DPX_RGB = 15, // DEPRECATED - DPX file repository (RGB)
   MEDIA_TYPE_REPOSITORY_UYVY = 16,    // YUV file repository
   MEDIA_TYPE_REPOSITORY_DPX = 17,     // DPX file repository
   MEDIA_TYPE_FILE_REPOSITORY = 18,    // File repository (any type of file)
};

enum EMediaRepositoryType
{
   MEDIA_REPOSITORY_TYPE_INVALID = 0,
   MEDIA_REPOSITORY_TYPE_RAW_DISK = 1,  // a.k.a. rawdisks
   MEDIA_REPOSITORY_TYPE_FILE_SYSTEM = 2 // image files under a root directory
};

// I really have no idea where to put this crap QQQ
enum EMediaHackCommand
{
   MEDIA_HACK_INVALID = 0,
   MEDIA_HACK_HIDE_ALPHA_MATTE = 1,
   MEDIA_HACK_SHOW_ALPHA_MATTE = 2,
   MEDIA_HACK_DESTROY_ALPHA_MATTE = 3,
   MEDIA_HACK_CACHE_FRAME = 4
};

enum EMediaGangIO
{
   MEDIA_GANG_IO_INVALID = 0,
   MEDIA_GANG_IO_NONE,
   MEDIA_GANG_IO_TWO_FIELDS,
   MEDIA_GANG_IO_MULTI_FIELDS
};


//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CMediaInterface  
{
public:
   struct SRawDiskInfo
   {
      EMediaType mediaType;               // Media type, e.g., raw disk, OGLV,
                                          // filesystem, etc.
      string mediaIdentifier;             // Media identifier, e.g. raw disk
                                          // name, file name
      CMediaAccess *mediaAccess;          // Ptr to CMediaAccess subtype
                                          // associated with the media type
                                          // and identifier.  Only set when
                                          // the Media Type is MEDIA_TYPE_RAW
                                          // or MEDIA_TYPE_RAW_OGLV, 0 otherwise
      MTI_INT64 totalFreeBytes;           // Total number of free bytes on the
                                          // media device
      MTI_INT64 maxContiguousFreeBytes;   // Size of largest continguous free
                                          // area on the media device
   };

   struct SImageRepositoryInfo
   {
      string repositoryName;              // Short name of repository
      EMediaType mediaType;               // Media type e.g. DPX files...
      string mediaDirectory;             // Media identifier = filesys folder
   };

   typedef vector<SRawDiskInfo> RawDiskInfoList;
   typedef vector<SImageRepositoryInfo> ImageRepositoryInfoList;

   // List of pointers to CMediaAccess subtype instances
   typedef vector<CMediaAccess*> MediaAccessList;
   typedef MediaAccessList::iterator MediaAccessListIterator;

public:
   CMediaInterface();
   virtual ~CMediaInterface();

   static void close();

   static bool isMediaTypeValid(EMediaType mediaType);
   static bool isMediaTypeImageFile(EMediaType mediaType);
   static bool isMediaTypeImageRepository(EMediaType mediaType);

   static EMediaType queryMediaType(const string& mediaTypeName);
   static string queryMediaTypeName(EMediaType mediaType);
   static EMediaType queryMediaTypeFromExtension(const string& mediaExtension);
   static string queryMediaTypeExtension(EMediaType mediaType);

   static bool isMediaRepositoryTypeValid(EMediaRepositoryType repType);
   static bool isMediaRepositoryARawDisk(EMediaRepositoryType repType);

   static EMediaRepositoryType queryMediaRepositoryType(const string& repTypeName);
   static string queryMediaRepositoryTypeName(EMediaRepositoryType repType);

   static string getDiskSchemeFileName();

   static bool isMediaHackCommandValid(EMediaHackCommand hackCommand);

   int queryRawDiskInfo(RawDiskInfoList &resultList);
   int queryImageRepositoryInfo(ImageRepositoryInfoList &resultList);

   int queryMediaAccessList(EMediaType mediaType,
                            MediaAccessList& resultList);
   int queryMediaAccessList(const string& mediaIdentifier,
                            MediaAccessList& resultList);
   int queryMediaAccessList(EMediaType mediaType, 
                            const string& mediaIdentifier,
                            MediaAccessList& resultList);
   static int queryMediaRepositoryList(EMediaType mediaType, 
                                       const string& mediaIdentifier,
                                       MediaAccessList& resultList);

   int queryImageTimeRemaining(EMediaType mediaType,
                               const string& mediaIdentifier,
                               const CImageFormat& imageFormat,
                               double frameRate,
                               CTimecode& totalTimeRemaining,
                               CTimecode& largestTimeRemaining);
                               
   int queryAudioTimeRemaining(EMediaType mediaType,
                               const string& mediaIdentifier,
                               const CAudioFormat& audioFormat,
                               double frameRate,
                               CTimecode& totalTimeRemaining,
                               CTimecode& largestTimeRemaining);

   int queryImageFieldsRemaining(EMediaType mediaType,
                                 const string& mediaIdentifier,
                                 const CImageFormat& imageFormat,
                                 MTI_UINT32 *totalFields,
                                 MTI_UINT32 *largestContiguousFields);

   int queryAudioFieldsRemaining(EMediaType mediaType,
                                 const string& mediaIdentifier,
                                 const CAudioFormat& audioFormat,
                                 MTI_UINT32 *totalFields,
                                 MTI_UINT32 *largestContiguousFields);

   int queryImageFieldsPrepare(EMediaType mediaType,
                               const string& mediaIdentifier,
                               const CImageFormat& imageFormat,
                               CMediaAccess **mediaAccess);

   int queryAudioFieldsPrepare(EMediaType mediaType,
                               const string& mediaIdentifier,
                               const CAudioFormat& audioFormat,
                               CMediaAccess **mediaAccess);

   int queryImageTimeTotal(EMediaType mediaType,
                           const string &mediaIdentifier,
                           const CImageFormat &imageFormat,
                           double frameRate,
                           CTimecode &totalTime);

   int queryAudioTimeTotal(EMediaType mediaType,
                           const string &mediaIdentifier,
                           const CAudioFormat &audioFormat,
                           double frameRate,
                           CTimecode &totalTime);

   int queryImageFieldsTotal(EMediaType mediaType,
                             const string &mediaIdentifier,
                             const CImageFormat &imageFormat,
                             MTI_UINT32 *totalFields);

   int queryAudioFieldsTotal(EMediaType mediaType,
                             const string &mediaIdentifier,
                             const CAudioFormat &audioFormat,
                             MTI_UINT32 *totalFields);

   CMediaAccess* registerMediaAccess(EMediaType mediaType, 
                                     const string& mediaIdentifier,
                                     const string& historyDirectory,
                                     const CMediaFormat& mediaFormat);

   int unregisterMediaAccess(CMediaAccess *mediaAccess);

   int checkSpace(EMediaType mediaType, const string& mediaIdentifier,
                  const CMediaFormat& mediaFormat, MTI_UINT32 frameCount);

   CMediaAllocator* allocateSpace(EMediaType mediaType,
                                  const string& mediaIdentifier,
                                  const string& historyDirectory,
                                  const CMediaFormat& mediaFormat,
                                  const CTimecode& frame0Timecode, 
                                  MTI_UINT32 frameCount,
                                  const string &relMediaPath);

   int CheckRFL(const string& mediaIdentifier);

   static int addNewImageRepository(
                            CImageRepositoryMediaAccess *newMediaAccess,
                            bool overwriteOkFlag=false);
   static int addNewDiskScheme(CDiskScheme *newDiskScheme,
                            bool overwriteOkFlag=false);

   static int renameBinMedia(string oldBinPath, string newBinPath);

   void dump(MTIostringstream& str);

private:
   // Private Function Members
   
   string adjustMediaIdentifier(EMediaType mediaType,
                                const string& mediaIdentifier,
                                const CMediaFormat& mediaFormat,
                                const CTimecode& inTimecode,
                                const string &relMediaPath);
   static CMediaAccess* makeMediaAccess(EMediaType mediaType,
                                 const string& mediaIdentifier,
                                 const string& historyDirectory,
                                 const CMediaFormat& mediaFormat);

   static string getRawDiskCfgFileName();
   static string getImageRepositoryCfgFileName();


private:
   // Private Data Members

   static bool isInitialized;          // false if Media Interface has not be
                                       // initialized
   static MediaAccessList mediaAccessList; // List of pointers to CMediaAccess 
                                           // subtype instances

   static int initializeRawMediaAccess();
   static int initializeRawOGLVMediaAccess();
   static int initializeImageRepositoryMediaAccess();
   static int initializeAudioMediaAccess();

   static string mediaStorageSectionName;
   static string rawDiskCfgKey;
   static string imageRepositoryCfgKey;
   static string diskSchemeFileKey;
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CDiskScheme
{
public:
   CDiskScheme(); // DOESN'T INITIALIZE MEMBERS - need to call Init()
   CDiskScheme(const CDiskScheme &src);   // Copy constructor
   ~CDiskScheme();

   CDiskScheme& operator=(const CDiskScheme &src);

   void Init();  // Clears contents of Disk Scheme
   
   // Read/Write from/to Disk Scheme ini file
   int ReadIni(CIniFile *iniFile, const string& sectionName);
   int WriteIni(CIniFile *iniFile, const string& sectionName);

public:
   string name;                    // Name of Disk Scheme
   EMediaRepositoryType mainVideoMediaRepositoryType;
                                   // Main video repository type (default VIDEOSTORE)
   string mainVideoMediaLocation;  // Main video media location (raw disk or OGLV)
   EMediaRepositoryType proxyVideoMediaRepositoryType;
                                   // Proxy video repository type (default VIDEOSTORE)
   string proxyVideoMediaLocation; // Proxy video media location (raw disk or OGLV)
   string audioMediaDirectory;     // Root directory for audio files

private:
   // Static ini file keys
   static string nameKey;
   static string mainVideoMediaRepositoryTypeKey;
   static string mainVideoMediaLocationKey;
   static string proxyVideoMediaRepositoryTypeKey;
   static string proxyVideoMediaLocationKey;
   static string audioMediaDirectoryKey;
};
//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CDiskSchemeList
{
public:
   CDiskSchemeList();
   ~CDiskSchemeList();

   int ReadDiskSchemeFile();
   int WriteDiskSchemeFile();

   StringList GetActiveDiskSchemeNames();
   
   CDiskScheme Find(const string& name);
   void Add(const CDiskScheme &newScheme);  // Read file; Add; Write file

private:
   string makeSectionName(int sectionNumber);
   int makeDefaultDiskSchemeFile(const string &diskSchemeFileName);

private:
   StringList activeDiskSchemes;
   vector<CDiskScheme> diskSchemeList;

   static string activeDiskSchemeSectionName;
   static string activeDiskSchemeKey;
   static string diskSchemeSectionNamePrefix;
};

#ifdef PRIVATE_STATIC
#ifndef _MEDIA_INTERFACE_DATA_CPP
namespace MediaInterfaceData {
   extern CMediaInterface::MediaAccessList mediaAccessList;
   extern bool isInitialized;

   // Sections and Keys in MTILocalMachine.ini for Media Storage
   extern string mediaStorageSectionName;
   extern string rawDiskCfgKey;
   extern string imageRepositoryCfgKey;
   extern string diskSchemeFileKey;

   extern string activeDiskSchemeSectionName;
   extern string activeDiskSchemeKey;
   extern string diskSchemeSectionNamePrefix;
   extern string nameKey;
   extern string mainVideoMediaRepositoryTypeKey;
   extern string mainVideoMediaLocationKey;
   extern string mainVideoMediaDirectoryKey;
   extern string proxyVideoMediaRepositoryTypeKey;
   extern string proxyVideoMediaLocationKey;
   extern string proxyVideoMediaDirectoryKey;
   extern string audioMediaDirectoryKey;
};
#endif
#endif
//////////////////////////////////////////////////////////////////////

#endif // #ifndef MEDIAINTERFACE_H

