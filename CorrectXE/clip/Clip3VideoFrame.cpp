// Clip3VideoFrame.cpp: implementation of the CVideoFrame class.
//
//////////////////////////////////////////////////////////////////////

#include "Clip3VideoTrack.h"
#include "MediaAccess.h"
#include "timecode.h"
#include "ImageFormat3.h"
#include "LineEngine.h"
#include "MediaFrameBuffer.h"
#include "MTImalloc.h"
#include "err_clip.h"
#include "err_format.h"
#include "err_file.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CVideoFrame::CVideoFrame(int reserveFieldCount)
: CFrame(reserveFieldCount),
  imageDataFlag(II_IMAGE_DATA_FLAG_INVALID)
{

}

CVideoFrame::~CVideoFrame()
{
}

//////////////////////////////////////////////////////////////////////
// Mutator Functions
//////////////////////////////////////////////////////////////////////

void CVideoFrame::setImageDataFlag(EImageDataFlag newFlag)
{
   imageDataFlag = newFlag;
}

void CVideoFrame::setImageFormat(const CImageFormat *newImageFormat)
{
   mediaFormat = newImageFormat;
}

//////////////////////////////////////////////////////////////////////
// Accessor Functions
//////////////////////////////////////////////////////////////////////

////void CVideoFrame::getDataOffsetAllFields(MTI_UINT32 *dataOffset)
////{
////   int totalFieldCount = getTotalFieldCount();
////   for (int i = 0; i < totalFieldCount; ++i)
////      {
////      dataOffset[i] = getDataOffsetAtField(i);
////      }
////}

////MTI_UINT32 CVideoFrame::getDataOffsetAtField(int fieldNumber)
////{
////   CVideoField *field = getField(fieldNumber);
////
////   if (field == 0)
////      return 0;
////
////   return (field->getDataOffset());
////}

////void CVideoFrame::getDataOffsetVisibleFields(MTI_UINT32 *dataOffset)
////{
////   int totalFieldCount = getVisibleFieldCount();
////   for (int i = 0; i < totalFieldCount; ++i)
////      {
////      dataOffset[i] = getDataOffsetAtField(i);
////      }
////}

//------------------------------------------------------------------------
//
// Function:    getField
//
// Description: Get a pointer to a particular field in a frame given the
//              field index. 
//
// Arguments:   int fieldIndex  Index of the field, 0 is the first field
//
// Returns:     Pointer to a CVideoField object for the specified index
//              If the field index is not valid, returns a NULL pointer
//
//------------------------------------------------------------------------
CVideoField* CVideoFrame::getField(int fieldIndex)
{
   return static_cast<CVideoField*>(getBaseField(fieldIndex));
}

EImageDataFlag CVideoFrame::getImageDataFlag() const
{
   return imageDataFlag;
}

const CImageFormat* CVideoFrame::getImageFormat() const
{
   return static_cast<const CImageFormat*>(mediaFormat);
}

MTI_UINT32 CVideoFrame::getMaxPaddedFieldByteCount()
{
   MTI_UINT32 maxByteCount = 0;
   MTI_UINT32 fieldByteCount;
   int i;
   int fieldCount = getTotalFieldCount();

   // Iterate through field list, finding maximum byte count for each field
   for (i = 0; i  < fieldCount; ++i)
      {
      fieldByteCount = getField(i)->getMediaLocation().getDataSize();
      if (fieldByteCount > maxByteCount)
         maxByteCount = fieldByteCount;
      }
   return maxByteCount;
}


//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

CField* CVideoFrame::createField(CFieldList *newFieldList,
                                 int fieldListEntryIndex)
{
   // Create a new Video Field and add it to the Frame's Field List

   CVideoField *newField = new CVideoField;

   newField->setFieldListEntryIndex(newFieldList, fieldListEntryIndex);

   addField(newField);

   return newField;
}

CField* CVideoFrame::createField(const CField& srcField)
{
   // Create a new Video Field and add it to the Frame's Field List

   const CVideoField &srcVideoField
                                 = static_cast<const CVideoField&>(srcField);

   CVideoField *newField = new CVideoField(srcVideoField);

   addField(newField);

   return newField;
}

//////////////////////////////////////////////////////////////////////
// Read/Write Media Data Functions
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    readMediaAllFieldsQuick
//
// Description: Read a frame's media data from media storage
//              into caller's buffers for all of the frame's visible
//              and invisible fields.  The number of fields to be read can
//              be obtained by calling the CFrame member function
//              getTotalFieldCount
//
//              Media data is read in the same order as the fields
//              appear in the frame.  The read order is not optimized
//              for the media location.
//
/// Arguments    MTI_UINT8 *buffer[]  Array of pointers to buffers, one for 
//                                    each field in frame
//              int quickReadFlag
//
// Returns:     0 for success, non-zero if an error.
//
//------------------------------------------------------------------------
int CVideoFrame::readMediaAllFieldsQuick(MTI_UINT8 *buffer[],
                                          int quickReadFlag)
{
   int retVal;
	int totalFieldCount = getTotalFieldCount();

   for (int fieldIndex = 0; fieldIndex < totalFieldCount; ++fieldIndex)
      {
      // Read the field to buffer
      CVideoField* field = getField(fieldIndex);

      if (field->isDirtyMedia())
         {
         TRACE_3(errout << "(((( readMediaAllFieldsQuick: get dirty field "
                        << fieldIndex << "( " << std::setw(8) << std::setfill('0')
                        << std::setbase(16) << ((int) this) << "," << ((int) field)
                        << " )" );
         }

      if (!field->isMediaWritten() || !field->doesMediaStorageExist())
         {
         // Nothing is really on the disk, so just synthesize a black field
         // into the caller's buffer.
			blackenField(field, fieldIndex, buffer[fieldIndex]);
         }
      else
         {
			retVal = field->readMediaQuick(buffer[fieldIndex], quickReadFlag);
         if (retVal != 0)
            {
            // ERROR: Could not read media of the field
            makeErrorFrame(buffer, retVal);

            return retVal;  // Return error
            }
         }
      }

   return 0;      // Return success
}

//------------------------------------------------------------------------
//
// Function:    readMediaAllFieldsOptimizedQuick
//
// Description: Read a frame's media data from media storage
//              into caller's buffers for all of the frame's visible
//              and invisible fields.  The number of fields to be read can
//              be obtained by calling the CFrame member function
//              getTotalFieldCount.
//
//              Media read is optimized by reading from the fields' media
//              storage in ascending media location order and by copying
//              buffers instead of re-reading for fields that share the
//              same media.
//
// Arguments    MTI_UINT8 *buffer[]  Array of pointers to buffers, one for each
//                                   field in frame
//              int quickReadFlag
//
// Returns:     0 if success, non-zero if an error.
//
//------------------------------------------------------------------------
int CVideoFrame::readMediaAllFieldsOptimizedQuick(MTI_UINT8 *buffer[],
                                                   int quickReadFlag)
{
	int retVal;

	retVal = readMediaOptimizedQuick(buffer, getTotalFieldCount(),
                                    quickReadFlag);
   if (retVal != 0)
      {
      // ERROR: read media failed
      return retVal;
      }

   return 0;      // Return success
}

//------------------------------------------------------------------------
//
// Function:    readMediaVisibleFieldsQuick
//
// Description: Read a frame's media data from media storage
//              into caller's buffers for the frame's visible fields.
//
//              Media data is read in the same order as the fields
//              appear in the frame.  The read order is not optimized
//              for the media location.
//
// Arguments    MTI_UINT8 *buffer[]  Array of pointers to buffers, one for each
//                                   visible field in frame
//              int quickReadFlag
//
// Returns:     0 if success, non-zero if an error.
//
//------------------------------------------------------------------------
int CVideoFrame::readMediaVisibleFieldsQuick(MTI_UINT8 *buffer[],
                                              int quickReadFlag)
{
	int retVal;

	// Read the first (and perhaps only) field to buffer
	CVideoField* field = getField(0);    // First field in field list

   if (field->isDirtyMedia())
      {
      TRACE_3(errout << "(((( readMediaVisibleFieldsQuick: get dirty field "
                     << 0 << " ( " << std::setw(8) << std::setfill('0')
                     << std::setbase(16) << ((int) this) << "," << ((int) field)
                     << " )" );
      }

   if (!field->isMediaWritten() || !field->doesMediaStorageExist())
      {
      // Nothing is really on the disk, so just synthesize a black field
      // into the caller's buffer.
		blackenField(field, 0, buffer[0]);
      }
   else
      {
      retVal = field->readMediaQuick(buffer[0], quickReadFlag);
      if (retVal != 0)
         {
         // ERROR: Could not read media of the field
         makeErrorFrame(buffer, retVal);
         
         return retVal;  // Return error
         }
      }

   // If this frame has two visible fields, read the second field to buffer
   if (getVisibleFieldCount() > 1)
      {
      field = getField(1);      // Second field in field list

      if (field->isDirtyMedia())
         {
         TRACE_3(errout << "(((( readMediaVisibleFieldsQuick: get dirty field "
                        << 1 << " ( " << std::setw(8) << std::setfill('0')
                        << std::setbase(16) << ((int) this) << "," << ((int) field)
                        << " )" );
         }

      if (!field->isMediaWritten() || !field->doesMediaStorageExist())
         {
         // Nothing is really on the disk, so just synthesize a black field
         // into the caller's buffer.
			blackenField(field, 1, buffer[1]);
         }
      else
         {
         retVal = field->readMediaQuick(buffer[1], quickReadFlag);
         if (retVal != 0)
            {
            // ERROR: Could not read media of the field
            makeErrorFrame(buffer, retVal);
            
            return retVal;  // Return error
            }
         }
      }

   return 0;      // Return success
}

//------------------------------------------------------------------------
//
// Function:    readMediaVisibleFieldsOptimizedQuick
//
// Description: Read a frame's media data from media storage
//              into caller's buffers for the frame's visible fields.
//
//              Media read is optimized by reading from the fields' media
//              storage in ascending media location order and by copying
//              buffers instead of re-reading for fields that share the
//              same media.
//
// Arguments    MTI_UINT8 *buffer[]  Array of pointers to buffers, one for each
//                                   visible field in frame
//              int quickReadFlag
//              int explicitField    Used to read just one field
//
// Returns:     0 if success, non-zero if an error.
//
//------------------------------------------------------------------------
int CVideoFrame::readMediaVisibleFieldsOptimizedQuick(MTI_UINT8 *buffer[],
                                                       int quickReadFlag,
                                                       int explicitField)
{
   int retVal;

	retVal = readMediaVisibleFieldsOptimizedQuickMT(buffer, quickReadFlag,
                                                   0, explicitField);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CVideoFrame::readMediaVisibleFieldsOptimizedQuickMT(MTI_UINT8 *buffer[],
                                                        int quickReadFlag,
                                                        int fileHandleIndex,
                                                        int explicitField)
{
   int retVal;

   retVal = readMediaOptimizedQuickMT(buffer, getVisibleFieldCount(),
                                      quickReadFlag, fileHandleIndex,
                                      explicitField);

   if (retVal != 0)
      {
      // ERROR: read media failed
      return retVal;
      }

   return 0;      // Return success
}

int CVideoFrame::readMediaOptimizedQuick(MTI_UINT8 *buffer[], int fieldCount,
                                          int quickReadFlag, int explicitField)
{
   int retVal;

   retVal = readMediaOptimizedQuickMT(buffer, fieldCount, quickReadFlag, 0,
                                      explicitField);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CVideoFrame::readMediaOptimizedQuickMT(MTI_UINT8 *buffer[],
                                           int fieldCount,
                                           int quickReadFlag,
                                           int fileHandleIndex,
                                           int explicitField)
{
   int retVal;

   // Arrays to hold results of sort
	int indexArray[CFRAME_MAX_FIELD_COUNT];
   CField *fieldArray[CFRAME_MAX_FIELD_COUNT];

   if (quickReadFlag == 0 && explicitField == -1)
      {
      // Attempt to merge two fields that are adjacent on the disk
      // into a single large read
      CMediaLocation mediaLocationArray[CFRAME_MAX_FIELD_COUNT];

      OPT_RESULT optResult = getMediaLocationOptimized(buffer, fieldCount,
                                                       mediaLocationArray,
                                                       true /* must exist */);
      switch(optResult)
         {
         default:
         case CANNOT_OPTIMIZE:
            break;

         case NEED_ONE_IO_OP:
            retVal = mediaLocationArray[0].read(buffer[0]);
            if (retVal != 0)
               {
               // ERROR: media read failed
               makeErrorFrame(buffer, retVal);
               }
            return retVal;

         case NEED_TWO_IO_OPS:
            {
            CMediaAccess *mediaAccess = mediaLocationArray[0].getMediaAccess();

            if (mediaAccess == 0)
               break;
            retVal = mediaAccess->readMultipleMT(2, buffer, mediaLocationArray,
                                                 fileHandleIndex);
            if (retVal != 0)
               {
               // ERROR: media read failed
               makeErrorFrame(buffer, retVal);
               }
            return retVal;
            }
         }
      }

   // Sort fields by media location
   sortFieldsByMediaLocation(fieldCount, indexArray, fieldArray);

   for (int i = 0; i < fieldCount; ++i)
      {
      // indexArray contains field indices ordered by
      // ascending media location
      int fieldIndex = indexArray[i];

      if (explicitField == -1 || fieldIndex == explicitField)
       {
        CVideoField* field = static_cast<CVideoField*>(fieldArray[fieldIndex]);

         if (field->isDirtyMedia())
            {
            TRACE_3(errout << "(((( readMediaOptimizedQuick: get dirty field "
                           << fieldIndex << " ( " << std::setw(8) << std::setfill('0')
                           << std::setbase(16) << ((int) this) << "," << ((int) field)
                           << " )" );
            }

        if (!field->isMediaWritten() || !field->doesMediaStorageExist())
         {
         // Nothing is really on the disk, so just synthesize a black field
         // into the caller's buffer.
			blackenField(field, fieldIndex, buffer[fieldIndex]);
         }
        else if (field->isSharedMedia() && !(field->isSharedMediaFirst()))
         {
         // Media storage is shared, presumably with the field
         // that was just read, so just copy the buffer

         // Adjust the field's media location per the quickReadFlag
         CMediaLocation adjustedMediaLocation;
         field->getMediaLocation().getQuickReadLocation(quickReadFlag,
                                                        adjustedMediaLocation);

         MTI_UINT32 bufferOffset = adjustedMediaLocation.getDataIndex()
                                   - field->getMediaLocation().getDataIndex();

         unsigned char *srcBuffer = buffer[indexArray[i -1]] + bufferOffset;
         memcpy(buffer[fieldIndex], srcBuffer + bufferOffset,
                field->getMediaLocation().getDataSize());
         }
        else
         {
         // Media storage is not shared, so read from media storage
         // or if it is shared, it is the first of fields that share this
         // media storage
         retVal = field->readMediaQuickMT(buffer[fieldIndex], quickReadFlag,
                                          fileHandleIndex);
         if (retVal != 0)
            {
            // ERROR: Could not read media of the field
            makeErrorFrame(buffer, retVal);
            
            return retVal;  // Return error
            }
         }
        }
      }
   return 0;      // Return success
}

//------------------------------------------------------------------------
//
// Function:    canBeReadAsynchronously
//
// Description: Lets caller know if asynch read is OK to try
//
// Arguments
//
// Returns:     true if OK
//
//------------------------------------------------------------------------
bool CVideoFrame::canBeReadAsynchronously()
{
//   int fieldCount = getVisibleFieldCount();
//   CVideoField *field = static_cast<CVideoField *>( getBaseField(0) );
//   bool canDoAsync = ( (fieldCount == 1) &&
//                       (field != NULL) &&
//                        field->canBeReadAsynchronously()
//                      );
//
//   return canDoAsync;
	MTIassert(false);
	return true;
}

//------------------------------------------------------------------------
//
// Function:    startReadMediaVisibleFieldsWithOffsetOptimizedAsynchronous
//
// Description: Read a frame's media data from media storage
//              into caller's buffers for the frame's visible fields.
//
//              Supports asynchronous read of image files
//
//              Returns immediately after starting the read. Call
//              waitReadMediaVisibleFieldsWithOffsetOptimizedAsynchronous
//              when you actually need the data
//
// Arguments    MTI_UINT8 *buffer[]  Input array of pointers to buffers provided
//                                   by the caller, one for each visible field
//                                   in frame, that will receive the read media.
//              MTI_UINT8 *offsetBuffer[]  Output array of pointers to buffers,
//                                         one for each visible field in frame
//                                         that point to the start of the
//                                         frame in the caller's buffer.
//              void* &readContext   Gets filled in - pass to 'wait' function
//              bool dontNeedAlpha   For extra fast turnaraound, skip stupid
//                                   10-bit DPX hidden embedded alpha hack.
//
// Returns:     0 if success, non-zero if an error.
//
//------------------------------------------------------------------------
int CVideoFrame::startReadMediaVisibleFieldsWithOffsetOptimizedAsynchronous(
                                                      MTI_UINT8 *buffer[],
                                                      MTI_UINT8 *offsetBuffer[],
                                                      void *&readContext,
                                                      bool dontNeedAlpha)
{
//	int retVal;
//	int fieldCount = getVisibleFieldCount();
//	bool cantDoAsync = (fieldCount > 1);
//
//	// This is the default for if we don't actually start a read operation
//	readContext = NULL;
//
//   if (cantDoAsync)
//		{
//		// Read the video media synchronously
//		retVal = readMediaWithOffsetOptimized(buffer, offsetBuffer, fieldCount);
//
//		if (retVal != 0)
//         return retVal; // ERROR: read media failed
//      }
//	else
//		{
//      // Set the default offset buffer pointer.  This value will be
//		// overridden by lower level read media functions.
//		offsetBuffer[0] = buffer[0]; //// + getDataOffsetAtField(0);
//
//      // Do I/O via the video field
//		CVideoField *field = static_cast<CVideoField *>( getBaseField(0) );
//
//      // this stuff stolen from readMediaWithOffsetOptimized()
//
//		if (!field->isMediaWritten() || !field->doesMediaStorageExist())
//         {
//			// Nothing is really on the disk, so just synthesize a black field
//         // into the caller's buffer.
//         blackenField(field, 0, offsetBuffer[0]);
//			}
//		else if (field->isSharedMedia() && !(field->isSharedMediaFirst()))
//         {
//         // Can't happen - only one field!
//			MTIassert(!field->isSharedMedia());
//         }
//		else
//			{
//			// Start the read from media storage
//         retVal = field->startReadMediaWithOffsetAsynchronous(
//                                             buffer[0],
//															offsetBuffer[0],
//															readContext,
//                                             dontNeedAlpha);
//			if (retVal != 0)
//				{
//				// ERROR: Could not read media of the field
//				makeErrorFrame(offsetBuffer, retVal);
//
//				return retVal;  // Return error
//				}
//			}
//		}
//
//	return 0;      // Return success
	MTIassert(false);
	return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

//------------------------------------------------------------------------
//
// Function:    isReadMediaVisibleFieldsWithOffsetOptimizedAsynchronousDone
//
// Description: Tell caller if the asynchronous read is complete
//
// Arguments    void*      readContext   Output from 'start' function
//
// Returns:     true if done
//
//------------------------------------------------------------------------
bool CVideoFrame::isReadMediaVisibleFieldsWithOffsetOptimizedAsynchronousDone(
                                           void* readContext)
{
//	bool retVal = true;
//
//	// If there is no context, there was no asynch read (not an error,
//	// may be using an access method that doesn't do asynch0
//	if (readContext != NULL)
//	{
//		// Do I/O via the video field
//		CVideoField *field = static_cast<CVideoField *>( getBaseField(0) );
//
//		// Finish the read from media storage
//		retVal = field->isReadMediaWithOffsetAsynchronousDone(readContext);
//	}
//
//	return retVal;      // Return success
	MTIassert(false);
	return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

//------------------------------------------------------------------------
//
// Function:    finishReadMediaVisibleFieldsWithOffsetOptimizedAsynchronous
//
// Description: Finish an async read
//
// Arguments    void* &readContext         You got this from the 'start' func
//              MTI_UINT8 *offsetBuffer[]  Output array of pointers to buffers,
//                                         one for each visible field in frame
//                                         that point to the start of the
//                                         frame in the caller's buffer.
//
// Returns:     0 if success, non-zero if an error.
//
//------------------------------------------------------------------------
int CVideoFrame::finishReadMediaVisibleFieldsWithOffsetOptimizedAsynchronous(
                                                      void *&readContext,
                                                      MTI_UINT8* offsetBuffer[])
{
//	int retVal;
//
//	// If there is no context, there was no asynch read (not an error,
//	// may be using an access method that doesn't do asynch0
//	if (readContext != NULL)
//	{
//		// Do I/O via the video field
//      CVideoField *field = static_cast<CVideoField *>( getBaseField(0) );
//
//		// Finish the read from media storage
//		retVal = field->finishReadMediaWithOffsetAsynchronous(readContext,
//																				offsetBuffer[0]);
//
//		if (retVal != 0)
//			{
//			// ERROR: Could not read media of the field
//			makeErrorFrame(offsetBuffer, retVal);
//
//			return retVal;  // Return error
//			}
//	}
//
//	if (retVal != 0)
//		return retVal; // ERROR: read media failed
//
//
//	return 0;      // Return success
	MTIassert(false);
	return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

//------------------------------------------------------------------------
//
// Function:    canBeReadAsFrameBuffers
//
// Description: Lets caller know if asynch frame buffer read is OK to try
//
// Arguments
//
// Returns:     true if OK
//
//------------------------------------------------------------------------
bool CVideoFrame::canBeReadAsFrameBuffers()
{
//	int fieldCount = getVisibleFieldCount();
//	CVideoField *field = static_cast<CVideoField *>( getBaseField(0) );
//	bool canDoAsync = ( (fieldCount == 1) &&
//							  (field != NULL) &&
//								field->canBeReadAsFrameBuffer()
//							 );
//
//	return canDoAsync;
	MTIassert(false);
	return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

//------------------------------------------------------------------------
//
// Function:    startReadMediaVisibleFieldFrameBuffersAsynchronous
//
// Description: Read a frame's media data from media storage
//              into frame buffers buffers for the frame's visible fields.
//
//              Supports asynchronous read of image files
//
//              Returns immediately after starting the read. Call
//              finishReadMediaVisibleFieldFrameBuffersAsynchronous
//              when you actually need the data
//
// Arguments    void* &readContext   Gets filled in - pass to 'wait' function
//
// Returns:     0 if success, non-zero if an error.
//
//------------------------------------------------------------------------
int CVideoFrame::startReadMediaVisibleFieldFrameBuffersAsynchronous(void *&readContext)
{
//	int retVal;
//	int fieldCount = getVisibleFieldCount();
//	bool cantDoAsync = (fieldCount > 1);
//
//	// This is the default for if we don't actually start a read operation
//	readContext = NULL;
//
//	if (cantDoAsync)
//		{
//			return CLIP_ERROR_ASYNCH_IO_NOT_SUPPORTED_FOR_MEDIA;
//		}
//
//	// Do I/O via the video field
//	CVideoField *field = static_cast<CVideoField *>( getBaseField(0) );
//
//	// this stuff stolen from readMediaWithOffsetOptimized()
//
//	if (!field->isMediaWritten() || !field->doesMediaStorageExist())
//		{
//		MTIassert(field->isMediaWritten());
//		MTIassert(field->doesMediaStorageExist());
////      // Nothing is really on the disk, so just synthesize a black field
////      // into the caller's buffer.
////      blackenField(field, 0, offsetBuffer[0]);
//		}
//	else if (field->isSharedMedia() && !(field->isSharedMediaFirst()))
//		{
//		// Can't happen - only one field!
//		MTIassert(!field->isSharedMedia());
//		}
//	else
//		{
//		// Start the read from media storage
//		retVal = field->startReadMediaFrameBufferAsynchronous(readContext);
//		if (retVal != 0)
//			{
////         // ERROR: Could not read media of the field
////         makeErrorFrame(offsetBuffer, retVal);
//
//			return retVal;  // Return error
//			}
//		}
//
//   return 0;      // Return success
	MTIassert(false);
	return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

//------------------------------------------------------------------------
//
// Function:    isReadMediaVisibleFieldFrameBuffersAsynchronousDone
//
// Description: Tell caller if the asynchronous frame buffer read is complete
//
// Arguments    void*      readContext   Output from 'start' function
//
// Returns:     true if done
//
//------------------------------------------------------------------------
bool CVideoFrame::isReadMediaVisibleFieldFrameBuffersAsynchronousDone(void* readContext)
{
//	bool retVal = true;
//
//	// If there is no context, there was no asynch read (not an error,
//	// may be using an access method that doesn't do asynch0
//	if (readContext != NULL)
//	{
//		// Do I/O via the video field
//		CVideoField *field = static_cast<CVideoField *>( getBaseField(0) );
//
//		// Finish the read from media storage
//		retVal = field->isReadMediaFrameBufferAsynchronousDone(readContext);
//	}
//
//	return retVal;      // Return success
	MTIassert(false);
	return true;
}

//------------------------------------------------------------------------
//
// Function:    finishReadMediaVisibleFieldFrameBuffersAsynchronous
//
// Description: Finish an async read to frame buffer
//
// Arguments    void* &readContext         You got this from the 'start' func
//              MediaFrameBuffer *offsetBuffer[]  Output array of frame buffers,
//                                         one for each visible field in frame.
//
// Returns:     0 if success, non-zero if an error.
//
//------------------------------------------------------------------------
int CVideoFrame::finishReadMediaVisibleFieldFrameBuffersAsynchronous(void *&readContext, MediaFrameBuffer* frameBuffer[])
{
//	// If there is no context, there was no asynch read (not an error,
//	// may be using an access method that doesn't do asynch0
//	if (readContext == NULL)
//		{
//		return 0;
//		}
//
//	// Do I/O via the video field
//	CVideoField *field = static_cast<CVideoField *>( getBaseField(0) );
//
//	// Finish the read from media storage
//	int retVal = field->finishReadMediaFrameBufferAsynchronous(readContext, frameBuffer[0]);
//	if (retVal != 0)
//      {
//	  CImageFormat *imageFormatPtr = const_cast<CImageFormat *>(getImageFormat());
//      if (imageFormatPtr != nullptr)
//			{
//			unsigned char *fieldArray[1];
//			frameBuffer[0] = MediaFrameBuffer::CreatePrivateBuffer(*imageFormatPtr);
//			fieldArray[0] = frameBuffer[0]->getImageDataPtr();
//			makeErrorFrame(fieldArray, retVal);
//			}
//
//		return retVal;  // Return error
//		}
//
//	return 0;      // Return success
	MTIassert(false);
	return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

//------------------------------------------------------------------------
//
// Function:    readMediaWithOffsetOptimized
//
// Description:
//
// Arguments    MTI_UINT8 *buffer[],
//              MTI_UINT8 *offsetBuffer[],
//              int fieldCount
//
// Returns:     0 if success, non-zero if an error.
//
//------------------------------------------------------------------------
int CVideoFrame::readMediaWithOffsetOptimized(MTI_UINT8 *buffer[],
                                              MTI_UINT8 *offsetBuffer[],
                                              int fieldCount)
{
   int retVal;

   // Arrays to hold results of sort
   int indexArray[CFRAME_MAX_FIELD_COUNT];
   CField *fieldArray[CFRAME_MAX_FIELD_COUNT];

   // Set the default offset buffer pointers.  These values will be
   // overridden by lower level read media functions.
   for (int i = 0; i < fieldCount; ++i)
      {
      offsetBuffer[i] = buffer[i]; //// + getDataOffsetAtField(i);
      }

   // Sort fields by media location
   sortFieldsByMediaLocation(fieldCount, indexArray, fieldArray);

   for (int i = 0; i < fieldCount; ++i)
      {
      // indexArray contains field indices ordered by
      // ascending media location
      int fieldIndex = indexArray[i];

      CVideoField* field = static_cast<CVideoField*>(fieldArray[fieldIndex]);

      if (field->isDirtyMedia())
         {
         TRACE_3(errout << "(((( readMediaWithOffsetOptimized: get dirty field "
                        << fieldIndex << " ( " << std::setw(8) << std::setfill('0')
                        << std::setbase(16) << ((int) this) << "," << ((int) field)
                        << " )" );
         }

      if (!field->isMediaWritten() || !field->doesMediaStorageExist())
         {
         // Nothing is really on the disk, so just synthesize a black field
         // into the caller's buffer.
			blackenField(field, fieldIndex, offsetBuffer[fieldIndex]);
         }
      else if (field->isSharedMedia() && !(field->isSharedMediaFirst()))
         {
         // Test for a WTF. Theoretically, this can NEVER HAPPEN. But,of
         // course. it DID!
         if (i == 0)
            {
            TRACE_0(errout << "INTERNAL ERROR: Badly formed frame - "
                              "first field is SHARED but not SHARED FIRST!");
            makeErrorFrame(offsetBuffer, CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR);
				return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR;
            }

        // Media storage is shared, presumably with the field
         // that was just read, so just copy the buffer
         MTI_UINT8 *srcBuffer = buffer[indexArray[i - 1]];
         memcpy(buffer[fieldIndex], srcBuffer,
                field->getMediaLocation().getDataSize());
         }
      else
         {
         // Media storage is not shared, so read from media storage
         // or if it is shared, it is the first of fields that share this
         // media storage
         retVal = field->readMediaWithOffset(buffer[fieldIndex],
                                             offsetBuffer[fieldIndex]);
         if (retVal != 0)
            {
            // ERROR: Could not read media of the field
				makeErrorFrame(offsetBuffer, retVal);

            return retVal;  // Return error
            }
         }
      }

   return 0;      // Return success
}

//////////////////////////////////////////////////////////////////////
// Frame buffer stuff
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    readMediaFrameBuffer
//
// Description: Tells the reader to synchronously fetch the frame file into memory.
//
// Arguments    None.
//
// Returns:     0 if success, non-zero if an error.
//
//------------------------------------------------------------------------
int CVideoFrame::fetchMediaFrameFile()
{
	MTIassert(getTotalFieldCount() == 1);

	int retVal;
	CVideoField* field = dynamic_cast<CVideoField *>(getBaseField(0));
	if (field == 0)
	{
		return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR;
   }

	if (!field->isMediaWritten() || !field->doesMediaStorageExist())
	{
		// Nothing is really on the disk, so do nothing.
	}
	else
	{
		retVal = field->fetchMediaFile();
		if (retVal != 0)
		{
			// ERROR: Could not read media of the field
			return retVal; // Return error
		}
	}

	return 0; // Return success
}

//------------------------------------------------------------------------
//
// Function:    readMediaFrameBuffer
//
// Description: Read a frame's media data from media storage,
//              returning it in a Media Frame Buffer.
//
// Arguments    MediaFrameBuffer *outFrameBuffer   The returned frame buffer.
//
// Returns:     0 if success, non-zero if an error.
//
//------------------------------------------------------------------------
int CVideoFrame::readMediaFrameBuffer(MediaFrameBufferSharedPtr &frameBuffer)
{
	MTIassert(getTotalFieldCount() == 1);

	int retVal;
	CVideoField* field = dynamic_cast<CVideoField *>(getBaseField(0));
	if (field == 0)
	{
		return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR;
	}

   // Seems to be a timing issue here where we may try to read the frame while it is being written out.
   CHRTimer timer;
   auto neededToWait = false;
   const int MaxWaitMillisec = 500;
   while ((!field->isMediaWritten() || !field->doesMediaStorageExist()) && timer.elapsedMilliseconds() < MaxWaitMillisec)
   {
      neededToWait = true;
      MTImillisleep(10);

      field = dynamic_cast<CVideoField *>(getBaseField(0));
      if (field == 0)
      {
         TRACE_0(errout << "ERROR: getBaseField(0) returned NULL");
         return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR;
      }
   }

   if (neededToWait)
   {
      TRACE_0(errout << "Timing issue: readMediaFrameBuffer() needed to wait " << timer.ReadAsString() << " msec for frame to be written!");
   }

   if (!field->isMediaWritten() || !field->doesMediaStorageExist())
   {
      // Nothing is really on the disk, so just synthesize an error frame
      // into the caller's buffer.
      retVal = CLIP_ERROR_IMAGE_FILE_MISSING;
      MediaFrameBuffer::ErrorInfo errorInfo { retVal, "", "Media file hasn't been written yet!", "" };

      CImageFormat imageFormat = *getImageFormat();
      if (imageFormat.validate() != 0)
      {
         frameBuffer = nullptr;
         return retVal;
      }

      frameBuffer = MediaFrameBuffer::CreateErrorBuffer(imageFormat, errorInfo);

      if (!field->isMediaWritten())
      {
         TRACE_0(errout << "ERROR: Version clip file is marked as NOT WRITTEN!");
      }

      if (!field->doesMediaStorageExist())
      {
         TRACE_0(errout << "ERROR: Version clip file is marked as MEDIA STORAGE DOESN'T EXIST!");
      }
	}
   else
   {
      auto hadToRetry = false;

      // Frame is available, so go ahead and read it.
      retVal = field->readMediaFrameBuffer(frameBuffer);

      // Retry on failure - we may be writing the file.
      while (retVal != 0 && timer.elapsedMilliseconds() < MaxWaitMillisec)
      {
         hadToRetry = true;
         MTImillisleep(10);
      retVal = field->readMediaFrameBuffer(frameBuffer);
   }

      if (hadToRetry)
      {
         TRACE_0(errout << "INFO: readMediaFrameBuffer() " << (retVal? "gave up" : "succeeded") << " after waiting " << timer.ReadAsString() << " msec for frame to be written!");
      }
   }

   if (retVal != 0)
   {
      return retVal; // Return error
   }

	return retVal; // Return success
}

int CVideoFrame::writeMediaFrameBuffer(
	const MediaFrameBufferSharedPtr &frameBuffer,
	const string &sourceImageFilePath)
{
	MTIassert(getTotalFieldCount() == 1);

	////////////////////////////////////////////////////////////////////
	// for first-time writes to a version clip frame, I don't know why
	// I need to flush the field list here, but if i don't it sometimes
	// never seems to get flushed, which is very bad!!
	bool needToUpdateFieldListFile = false;  // Presume no flush will be needed.
	////////////////////////////////////////////////////////////////////

	int retVal;
	CVideoField* videoField = getField(0);
	if (videoField == 0)
	{
		return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR;
	}

	//////////////////////////////////////////////////////
	// WTF? Why is there no check for shared media here
	//     like in other cases?
	//////////////////////////////////////////////////////

	if (videoField->isWriteProtectedMedia())
	{
		return CLIP_ERROR_MEDIA_IS_WRITE_PROTECTED;
	}

	// Part 2 of version clip flush hack.
	if (videoField->isDirtyMedia()
	&& !(videoField->isMediaWritten()
	&& videoField->doesMediaStorageExist()))
	{
		needToUpdateFieldListFile = true;
	}

	retVal = videoField->writeMediaFrameBuffer(frameBuffer, sourceImageFilePath);
	if (retVal != 0)
	{
		// ERROR: Could not read or write media of the field
		return retVal;
	}

	// Final part of brute-force flush hack.
	if (needToUpdateFieldListFile)
	{
		// QQQ why isn't this videoField->getFieldList()->updateFieldListFile()?
		getBaseField(0)->getFieldList()->updateFieldListFile();
	}

	return 0;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

void CVideoFrame::blackenField(CField *field, int fieldIndex,
                               MTI_UINT8 *buffer)
{
	CImageFormat ifTmp = *getImageFormat();
   if (ifTmp.getInterlaced())
    {
     ifTmp.setInterlaced(false);
     ifTmp.setLinesPerFrame(ifTmp.getLinesPerFrame()/2);
	 }

   CLineEngineFactory lef;
   CLineEngine *cep = lef.makePixelEng (ifTmp);
   cep->setExternalFrameBuffer(&buffer);

   cep->setFGColor (40000, 40000, 40000);
   cep->setBGColor (40000, 40000, 40000);

   cep->fillField ();

   int iFontSize = (int)(3. * (float)ifTmp.getPixelsPerLine() / 720. + 0.5);
   cep->setFontSize(iFontSize);
   cep->setFGColor (10000,10000,10000);
   cep->drawString(ifTmp.getPixelsPerLine()/4,
        ifTmp.getLinesPerFrame()/4, "MTI EMPTY FRAME");

   delete cep;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

void CVideoFrame::makeErrorFrame(MTI_UINT8 *buffer[], int errcod)
{
   const CImageFormat *ifTmp = getImageFormat();

   CLineEngineFactory lef;
   CLineEngine *cep = lef.makePixelEng (*ifTmp);
   if (cep == NULL)
      return;
                                     
   cep->setExternalFrameBuffer(buffer);                                         

   cep->setFGColor (40000, 40000, 40000);                                       
   cep->setBGColor (40000, 40000, 40000);                                       

   cep->fillField ();

   int iFontSize = (int)(2.25 * (float)ifTmp->getPixelsPerLine() / 720. + 0.5); 
   cep->setFontSize(iFontSize);
   cep->setFGColor (10000,10000,10000);
   int iFontHeight = cep->getFontHeight();

   char errmsg[64];
   sprintf(errmsg, "MEDIA ERROR %5d READING FRAME", errcod);                    

   int iX = ifTmp->getPixelsPerLine() / 2 - 124 * iFontSize;
   int iY = ifTmp->getLinesPerFrame() / 2 -  10 * iFontSize;

   cep->drawString(iX, iY, errmsg);
   iY += iFontHeight + 2;

   switch (errcod)
   {
     case CLIP_ERROR_RAW_DISK_OPEN_FAILED:      // -2111
       strcpy(errmsg, "Videostore Open Failed");
       break;
     case CLIP_ERROR_RAW_DISK_READ_FAILED:      // -2112
       strcpy(errmsg, "Videostore Read Failed");
       break;
     case CLIP_ERROR_IMAGE_FILE_MISSING:        // -2146
       strcpy(errmsg, "Image File Missing");
       break;
     case CLIP_ERROR_IMAGE_FILE_INCONSISTENT_HEADER: // -2148
       strcpy(errmsg, "Image File Header Inconsistency");
       break;
     case FILE_ERROR_OPEN:                    // -5071
       strcpy(errmsg, "Image File Open Failed - File Missing?");
       break;
     case FILE_ERROR_READ:                    // -5075
       strcpy(errmsg, "Image File Read Failed");
       break;
   }

   cep->drawString(iX, iY, errmsg);

   delete cep;                                                                  
}                                                                               

//////////////////////////////////////////////////////////////////////
// Virtual Copy
//////////////////////////////////////////////////////////////////////

int CVideoFrame::virtualCopy(CVideoFrame &srcFrame,
                             CMediaStorageList &dstMediaStorageList)
{
   int retVal;

   // Virtual copy of field list
   // Note: assumes matching number of fields in src and dst, doesn't check
   int srcFieldCount = srcFrame.getTotalFieldCount();
   for (int i = 0; i < srcFieldCount; ++i)
      {
      CVideoField *dstField = getField(i);
      CVideoField *srcField = srcFrame.getField(i);
      retVal = dstField->virtualCopy(*srcField, dstMediaStorageList);
      if (retVal != 0)
         return retVal;
      }

   setTimecode(srcFrame.getTimecode());

   // Note: Image format pointer is not changed.  Image Format is
   //       assumed to be identical for both source and destination

   setImageDataFlag(srcFrame.getImageDataFlag());

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Low-level frame data cache hack
//////////////////////////////////////////////////////////////////////

void CVideoFrame::DoFrameCacheHack()
{
   if (getTotalFieldCount() == 1)
      {
      CVideoField *field = getField(0);
      if (field != NULL)
         {
         field->hackMedia(MEDIA_HACK_CACHE_FRAME);
         }
      }
}

//////////////////////////////////////////////////////////////////////
// Testing and Debugging Functions
//////////////////////////////////////////////////////////////////////

void CVideoFrame::dump(MTIostringstream& str)
{
   CTimecode tmpTimecode = getTimecode();

   CFrame::dump(str);

   str << "     imageDataFlag: " << imageDataFlag << std::endl;

}






