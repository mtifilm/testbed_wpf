#include <algorithm>
#include <vector>
#include <stdlib.h>
#include <stdio.h>
#include <deque>
#if defined(__sgi) || defined(__linux)
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#elif _WIN32
#include <time.h>      // time_t is broken in sys/types.h in RadStudio XE4
#include <sys/types.h>
#include <sys\stat.h>
#include <shlwapi.h>
#endif
#include "FileSweeper.h"
#include "HistoryLocation.h"
#include "MTIio.h"
#include "Mask.h"
#include "MediaAccess.h"
#include "PixelRegions.h"
#include "ClipAPI.h"
#include "ImageFormat3.h"
#include "MTImalloc.h"
#include "MTIstringstream.h"
#include "err_metadata.h"
#include "IniFile.h"        // for TRACE_0()

#include "SaveRestore.h"
////TODO is this required #pragma pack(1)
//#define OLD // use the old buffer

//*****************************
// For GOV threading issues a global "atomic lock"
CThreadLock *CSaveRestore::AtomicLock = NULL;
int CSaveRestore::LockCounter = 0;
//******************************

/* static */
CThreadLock *CSaveRestore::getLock()
{
   if (AtomicLock == NULL)
      AtomicLock = new CThreadLock;
   return AtomicLock;
}

////////////////////////////////////////////////////////////////
//
//                  C S a v e R e s t o r e
//
//        Author: Kurt Tolksdorf
//        Begun:  8/1/01
//        Modified: 9/08/02
//                 10/31/02 changed file extensions
//                  3/12/03 fixed writeback of .RES records
//
// Save/Restore facility for new-generation DRS. Implements a
// simple DBMS for saving pixel and command data for DRS com-
// mands.
//
// An individual instance of saved pixel data is recorded in
// a RESTORE RECORD. This holds the clip frame, field, region
// (box), and file offset pointers to find the command and
// pixel data.
//
// The cmd string which caused a particular region to be saved
// is recorded in the .CMD file. The .RES record points to the
// correct position in the .CMD file to locate the PDL-style
// command string. The command string for a given .RES record
// can be set to null, by setting the file ptr to NO_CMD (-1);
//
// The pixel data for the region saved is recorded in the
// .PXL file. The .RES record points to the correct position
// in the .PXL file to locate the 16-bit "444" pixel data
// for the region saved. The pixel data consist of serialized
// PIXELREGIONs (cf. "PixelRegions.h")
//
// The .RES records are indexed by the .HDR file. This file
// contains small .HDR records which specify the minimum
// and maximum frames represented in an entire block of
// .RES records. To locate the saved pixel and command data
// for a given frame, the .HDR file is searched for those
// .HDR records which cover a relevant range of frames. For
// each such .HDR record, the corresponding bock of .RES
// records is examined to find those .RES records which
// apply to the specific frame. The .RES records are then
// filtered for field and region. Searching of the .HDR and
// .RES records is done in the order most-recent to least-
// recent.
//
// When a relevant .RES record is found, the cmd data and
// the pixel data can be read from the .CMD and .PXL files,
// respectively.
//
// In practice, the facility is used to enable the DRS operator
// to "undo" the effect of successive commands on a given frame.
// The operator indicates where the DRS command is to be reversed
// by drawing a box or lasso over the displayed frame. The facility
// then locates the pixel data saved by the command prior to its
// completion, and restores it to the clip's frame. The restore
// is implemented only for the portion of the saved data which
// overlaps the stretched box (lasso), thus allowing fine control
// by the operator. Because the order of application of the
// operations is most-recent to least-recent, an entire sequence
// of DRS operations can be "undone" one at a time under inter-
// active review.
//
// The pixel data saved for a given .RES record may be that for
// even lines of the frame, odd lines, or both. It is up to the
// caller to indicate which. For progressive data, the caller
// will always indicate "both" by using an argument of -1 for
// the field number in the SAVREGIONLIST call.
//
// KT 10/31/02 - To ease compatibility with the older history
// files, the new one uses file extensions which correspond
// with the old as follows:
//
//	OLD	NEW		MEANING
//
//     .HDR    .HED	Index for .RES files
//     .RES    .RST	Individual save records
//     .PXL    .PIX	16-bit pixel data (serialized
//			PixelRegions)

#include "SaveRestore.h"

// constructor
CSaveRestore::CSaveRestore()
:  frmWdth(-1)
,  frmHght(-1)
,  pxlComponentCount(-1)
,  hdrFileHandle(-1)
,  hdrFilePtr(-1)
,  hdrBaseFileOffset(-1)
,  hdrBlkLeng(0)
,  hdrRecPtr(NULL)
,  hdrBuf(NULL)
,  hdrBufStop(NULL)
,  resBlkIndex(-1)
,  resFileHandle(-1)
,  resFilePtr(-1)
,  resBaseFileOffset(-1)    // poor name choice - not really a ptr
,  resBlkLeng(0)
,  resRecPtr(NULL)
,  resBuf(NULL)
,  resBufStop(NULL)
,  resWriteback(false)
,  currentHistoryFileOpen(-1)
,  pxlFileHandle(-1)
,  resFileOffset(-1)
,  resBaseOffset(-1)
,  maskBufSize(0)
,  maskBuf(NULL)
,  maskEngine(NULL)
,  pxlChannel(NULL)
,  pxlFilePtr(-1)    // poor name choice - not really a ptr
,  filterFrame(-1)
,  filterField(-1)
,  useMask(NULL)
,  globalSaveNumber(0)
,  govFileHandle(-1)
,  govBufByts(0)
,  govBuf(NULL)
,  govBufStop(NULL)
,  govBufPtr(NULL)
,  govBaseOffset(0)
,  govRunLengthOffset(-1)    // poor name choice - not really a ptr
{
   // access to .PXL file or individual .HST files
   pxlChannel = new CPixelChannel;

   // a mask engine is used to generate mask filters
   maskEngine = new CMask;

   // MONOLITHIC history only
   hdrBuf = (HEADER_RECORD *)MTImalloc(HEADER_BLOCKING_FACTOR*sizeof(HEADER_RECORD));
   hdrBufStop = hdrBuf + HEADER_BLOCKING_FACTOR;

   resBuf = (RESTORE_RECORD *)MTImalloc(RESTORE_BLOCKING_FACTOR*sizeof(RESTORE_RECORD));
   resBufStop = resBuf + RESTORE_BLOCKING_FACTOR;

   // buffer to use in reading & writing gov pxl data
   govBufByts = 32768;
   govBuf = (MTI_UINT16 *)MTImalloc(govBufByts);
   govBufStop = govBuf + (govBufByts/2);

   // construct the global atomic lock
   // THEORETICALLY SHOULD BE CREATED UNDER LOCK!
   ++LockCounter;
   getLock();      // creates it if no one has yet

   filterRegion.top = filterRegion.bottom =
      filterRegion.left = filterRegion.right = -1;
   clrFilterRegion.top = clrFilterRegion.bottom =
	  clrFilterRegion.left = clrFilterRegion.right = -1;
 }

// destructor
CSaveRestore::~CSaveRestore()
{
   // clear this list
   FixDiscardList.clear();

   // and this one too
   GOVRestoreList.clear();

   MTIfree(govBuf);

   MTIfree(maskBuf);

   MTIfree(resBuf);

   MTIfree(hdrBuf);

   delete maskEngine;

   delete pxlChannel;

   if (--LockCounter == 0)
   {
      delete AtomicLock;
      AtomicLock = nullptr;
   }
}

////////////////////////////////////////////////////////////////
//
//                       S e t C l i p
//
// feed the CLIP into the HISTORY

int CSaveRestore::setClip(ClipSharedPtr &clip)
{
   MTIassert(clip != nullptr);

   // save the => clip
   srcClip = clip;

   // always init these two variables for a new clip //
   pxlFileHandle = -1;
   currentHistoryFileOpen = -1;

   // and the => frame list
   srcVideoFrameList = clip->getVideoFrameList(VIDEO_SELECT_NORMAL,
                                               FRAMING_SELECT_VIDEO);

   srcInFrame  = srcVideoFrameList->getInFrameIndex();
   srcOutFrame = srcVideoFrameList->getOutFrameIndex();

   /////////////////////////////////////////////////////////////////////////////

    CImageFormat const *activeFormat = clip->getImageFormat(VIDEO_SELECT_NORMAL);

   // intermediate frame stats
   frmWdth           = activeFormat->getPixelsPerLine();
   frmHght           = activeFormat->getLinesPerFrame();
   pxlComponentCount = activeFormat->getComponentCountExcAlpha();

   // if image pixels grew, expand the mask buffer accordingly
   int newSize = frmWdth*frmHght;
   if (newSize > maskBufSize) {
      MTIfree(maskBuf);
      maskBuf = (MTI_UINT16*)MTImalloc(newSize * sizeof(MTI_UINT16));
      if (maskBuf == 0)
         return ERR_SAVE_RESTORE_MALLOC;
      maskBufSize = newSize;
   }

   // init the mask with the new frame stats
   maskEngine->initMask(frmWdth, frmHght, maskBuf);

   // the initial filter region is well-defined
   // so that the mask may be properly cleared
   clrFilterRegion.left   = 0;
   clrFilterRegion.top    = 0;
   clrFilterRegion.right  = frmWdth - 1;
   clrFilterRegion.bottom = frmHght - 1;

   ////////////////////////////////////////////////////////////
   //
   // H I S T O R Y   P A T H N A M E
   //
   // for MONOLITHIC history only

   string frameListFileName = srcVideoFrameList->getFrameListFileName();

   historyPathName = GetFilePath(frameListFileName);

   ////////////////////////////////////////////////////////////
   //
   // . H E D   F I L E N A M E
   //
   // get the full .HED filename

   hdrFileName = frameListFileName + ".hed";

   ////////////////////////////////////////////////////////////
   //
   // . R S T   F I L E N A M E
   //
   // get the full .RST filename
   resFileName = frameListFileName + ".rst";

   ////////////////////////////////////////////////////////////
   //
   // . P I X   F I L E N A M E
   //
   // get the full .PIX filename
   pxlFileName = frameListFileName + ".pix";


   // Determine history type - file per frame or monolithic.
   // Assume file per frame, unless we find the monolithic files in the clip
   // folder. Also need to do some weirdness for version clips.
   historyType = HISTORY_TYPE_FILE_PER_FRAME;

   if (DoesFileExist(hdrFileName) && DoesFileExist(resFileName) && DoesFileExist(pxlFileName))
   {
      historyType = HISTORY_TYPE_MONOLITHIC;
   }
   else
   {
      // I'm not really sure why version clips are special-cased.
      CBinManager bm;
      srcClipHistoryFolder = bm.getDirtyHistoryFolderPath(srcClip->getClipPathName());
      if (srcClipHistoryFolder.empty())
      {
         // It's not a version clip with history in the media folder (I guess).
         CFrame *iniframe = srcVideoFrameList->getBaseFrame(srcInFrame);
         MTIassert(iniframe != nullptr)
         if (iniframe == nullptr)
         {
            return -1;
         }

         auto srcClipInFrameHistoryPath = srcVideoFrameList->getHistoryFilePath(iniframe);
         if (srcClipInFrameHistoryPath.empty())
         {
            // I'm guessing we can only get here if the clip has monolithic history...
            // But I could be wrong!
            CField *field = iniframe->getBaseField(0);
            MTIassert(field != nullptr)
            if (field == nullptr)
            {
               return -1;
            }

            const CMediaLocation& mediaLocation = field->getMediaLocation();
            string imageFileDirectory = RemoveDirSeparator(GetFilePath(mediaLocation.getImageFilePath()));

            // Note: as a side effect, the History directory and MTImetadata.ini
            // files will be created in the image file directory or in the separate
            // metadata tree.
            HistoryLocation historyLocation;
            srcClipHistoryFolder = historyLocation.computeHistoryLocation(imageFileDirectory);
         }
         else
         {
            srcClipHistoryFolder = GetFilePath(srcClipInFrameHistoryPath);
         }
      }

      // CAREFUL!! Fucking srcClipHistoryFolder MUST END WITH A \ !!!!
      MTIassert(!srcClipHistoryFolder.empty());
      srcClipHistoryFolder = AddDirSeparator(srcClipHistoryFolder);
   }

   return 0;
}

/////////////////////////////////////////////////////////////////
//
//             v e r i f y H e a d e r F i l e
//
// Open the history, create a new .HED file, then close this history.
//
int CSaveRestore::verifyHeaderFile()
{
   if (historyType != HISTORY_TYPE_MONOLITHIC)
   {
      // File per frame, no HED file!
      return 0;
   }

   int bytcnt, result;
   CAutoThreadLocker(*getLock());

   //////////////////////////////////////////////////////////////////////////
   // open the .RES file
   if ((resFileHandle = open(resFileName.c_str(),
                             O_BINARY|O_CREAT|O_RDWR, S_IRUSR|S_IWUSR)) == -1) {

      TRACE_0(errout << "ERROR: CSaveRestore::verifyHeaderFile: "
                     << " can't open the .RES file because: " << endl
                     << strerror(errno));

      return HISTORY_ERROR_OPENING_RES_FILE;
   }

   // determine its length
   resFilePtr = lseek(resFileHandle,0,SEEK_END);
   if (resFilePtr == -1) {

      return HISTORY_ERROR_SEEKING_IN_RES_FILE;
   }

   // seek to beginning of .RES file
   if (lseek(resFileHandle, 0, SEEK_SET) == -1) {

      return HISTORY_ERROR_SEEKING_IN_RES_FILE;
   }

   // length must be a multiple of record size
   MTIassert((resFilePtr % sizeof(RESTORE_RECORD))==0);
   if ((resFilePtr % sizeof(RESTORE_RECORD))!=0) {

      TRACE_0(errout << "ERROR: CSaveRestore::verifyHeaderFile: "
                     << " .RES file length is wrong" << endl
                     << strerror(errno));
      // ugghhh
      closeHistoryFiles();
      return HISTORY_ERROR_OPENING_RES_FILE;
   }

   // the total restore records
   int remResRecs = resFilePtr / sizeof(RESTORE_RECORD);

   // the total number of .HDR records
   int totHdrRecs = (remResRecs + (RESTORE_BLOCKING_FACTOR - 1)) /
                                 RESTORE_BLOCKING_FACTOR ;

   //////////////////////////////////////////////////////////////////////////
   // open the .HDR file
   if ((hdrFileHandle = open(hdrFileName.c_str(),
                          O_BINARY|O_CREAT|O_WRONLY, S_IWUSR)) == -1) {

      TRACE_0(errout << "ERROR: CSaveRestore::verifyHeaderFile: "
                     << " can't open the .HDR file because: " << endl
                     << strerror(errno));
      // ugghh
      closeHistoryFiles();
      return HISTORY_ERROR_OPENING_HDR_FILE;
   }

   // seek to beginning of .HED file
   if (lseek(hdrFileHandle, 0, SEEK_SET) == -1) {

      return HISTORY_ERROR_SEEKING_IN_HDR_FILE;
   }

   // set the .HED ptr
   hdrRecPtr = hdrBuf;

   // for each block of .RES records do
   for (int i=0; i<totHdrRecs; i++, remResRecs -= RESTORE_BLOCKING_FACTOR) {

      // read in the block
      int blkResRecs = remResRecs;
      if (blkResRecs > RESTORE_BLOCKING_FACTOR)
         blkResRecs = RESTORE_BLOCKING_FACTOR;

      resBlkLeng = blkResRecs*sizeof(RESTORE_RECORD);

      // lseek(resFileHandle, i*RESTORE_BUFFER_SIZE, SEEK_SET);
      if (read(resFileHandle,resBuf,resBlkLeng)!=resBlkLeng) {

         // QQQ - isn't it only an error if negative??
         TRACE_0(errout << "ERROR: CSaveRestore::verifyHeaderFile: "
                     << " can't read the .RES file because: " << endl
                     << strerror(errno));
         MTIassert(false);
         // ugghh
         closeHistoryFiles();
         return HISTORY_ERROR_READING_RES_FILE;
      }

      //    init the corresponding .HED record
      hdrRecPtr->minFrame =  0x7fffffff;
      hdrRecPtr->maxFrame = -0x7fffffff;

      //    for each .RES record in the block do
      //       include the .RES record in the .HED record
      //    next .RES record

      int j;
      for (j=0, resRecPtr = resBuf; j < blkResRecs ; j++, resRecPtr++) {

         if (resRecPtr->frameNum < hdrRecPtr->minFrame)
            hdrRecPtr->minFrame = resRecPtr->frameNum;
         if (resRecPtr->frameNum > hdrRecPtr->maxFrame)
            hdrRecPtr->maxFrame = resRecPtr->frameNum;
      }

      // next block of .RES records

      hdrRecPtr++;
      if (hdrRecPtr == hdrBufStop) {

         // write out the block of .HED records
         result = write(hdrFileHandle, hdrBuf, HEADER_BUFFER_SIZE);
      if (result != (int)HEADER_BUFFER_SIZE) {

            TRACE_0(errout << "ERROR: CSaveRestore::verifyHeaderFile: "
                           << " can't write the .HDR file because: " << endl
                           << strerror(errno));
            // ugghh
            closeHistoryFiles();
            return HISTORY_ERROR_WRITING_HDR_FILE;
         }

         // reset the .HED ptr
         hdrRecPtr = hdrBuf;
      }
   }

   // close the .HDR file
   bytcnt = (hdrRecPtr - hdrBuf)*sizeof(HEADER_RECORD);
   result = write(hdrFileHandle, hdrBuf, bytcnt);
   if (result != bytcnt) {

      TRACE_0(errout << "ERROR: History RES file write failed because "
                     << ((result == -1)? strerror(errno) : " disk is full"));

      return HISTORY_ERROR_WRITING_RES_FILE;
   }

   closeHistoryFiles();

   return 0;
}

/////////////////////////////////////////////////////////////////
//
//   O P E N   F O R   A   S E R I E S   O F   S A V E S
//
// Open the history files and position the pointers as follows:
//
//     i) resRecPtr -> next free .RES record at end
//
// This positioning of the pointers serves as well to initialize
// for searching back-to-front.
//
int CSaveRestore::openForSave()
{
   return openHistoryFiles();
}
//------------------------------------------------------------------------------

// This used to be called openSave()
int CSaveRestore::openHistoryFiles()
{
   return (historyType == HISTORY_TYPE_MONOLITHIC)
            ? openHistoryFiles_MONO()
            : openHistoryFiles_FPF();
}
//------------------------------------------------------------------------------

int CSaveRestore::openHistoryFiles_MONO()
{
   int bytcnt, result;

   MTIassert(resFileHandle == -1);
   MTIassert(hdrFileHandle == -1);
   MTIassert(pxlFileHandle == -1);

   //////////////////////////////////////////////////////////////////////////
   // open the .RES file
   if ((resFileHandle = open(resFileName.c_str(),
                          O_BINARY|O_CREAT|O_RDWR, S_IRUSR|S_IWUSR)) == -1) {

      TRACE_0(errout << "ERROR: CSaveRestore::openHistoryFiles: "
                     << " can't open the .RES file because: " << endl
                     << strerror(errno));

      return HISTORY_ERROR_OPENING_RES_FILE;
   }

   // determine its length
   if ((resFilePtr = lseek(resFileHandle,0,SEEK_END))==-1) {

      return HISTORY_ERROR_SEEKING_IN_RES_FILE;
   }

   // length must be a multiple of record size
   if ((resFilePtr % sizeof(RESTORE_RECORD))!=0) {

      TRACE_0(errout << "ERROR: CSaveRestore::openHistoryFiles: "
                     << " .RES file is wrong size");
      // Shouldn't we try to recover?
      // ugghh
      closeHistoryFiles();
      return HISTORY_ERROR_OPENING_RES_FILE;
   }

   // calculate the total number of .HDR records
   int totHdrRecs = ((resFilePtr / sizeof(RESTORE_RECORD) +
                    (RESTORE_BLOCKING_FACTOR - 1)) /
                     RESTORE_BLOCKING_FACTOR);

   // the disk addr of the current block of .RES records
   resBlkIndex = resFilePtr  / RESTORE_BUFFER_SIZE;
   resBaseFileOffset  = resBlkIndex * RESTORE_BUFFER_SIZE;

   // the length of the data in the last .RES block
   resBlkLeng = resFilePtr - resBaseFileOffset;

   // if there's no data in the current block but there's
   // at least one previous block of .RES records
   //   go there and look at the last .RES record
   //   get the global save number from it and reset

   if (resBlkLeng == 0) { // no data in the current record

    if (resBaseFileOffset >= (int)RESTORE_BUFFER_SIZE ) {

         // go there and read the last block of .RES records
         int result = lseek(resFileHandle, resBaseFileOffset-RESTORE_BUFFER_SIZE, SEEK_SET);
         if (result == -1) {

            TRACE_0(errout << "ERROR: History RES file seek failed because "
                           << strerror(errno));
            MTIassert(result != -1);
            return HISTORY_ERROR_SEEKING_IN_RES_FILE;
         }
         MTIassert(result == (int) (resBaseFileOffset-RESTORE_BUFFER_SIZE));

         result = read(resFileHandle, resBuf, RESTORE_BUFFER_SIZE);
         if (result == -1) {

            TRACE_0(errout << "ERROR: CSaveRestore::openHistoryFiles: "
                           << " can't read the .RES file because: " << endl
                           << strerror(errno));
         MTIassert(result == (int)RESTORE_BUFFER_SIZE);
            closeHistoryFiles();
            return HISTORY_ERROR_READING_RES_FILE;
         }
      MTIassert(result == (int)RESTORE_BUFFER_SIZE);

         globalSaveNumber =
            (resBuf+(RESTORE_BLOCKING_FACTOR-1))->globalSaveNumber;
      }

      resRecPtr = resBuf;
   }
   else {

      // go there and read the last block of .RES records
      int result = lseek(resFileHandle, resBaseFileOffset, SEEK_SET);
      if (result == -1) {

         TRACE_0(errout << "ERROR: History RES file seek failed because "
                        << strerror(errno));
         MTIassert(result != -1);
         return HISTORY_ERROR_SEEKING_IN_RES_FILE;
      }
      MTIassert(result == resBaseFileOffset);

      if (read(resFileHandle,resBuf,resBlkLeng)!=resBlkLeng) {

         // QQQ Could be a short read which is technically legal
         TRACE_0(errout << "ERROR: CSaveRestore::openHistoryFiles: "
                        << " can't read the .RES file because: " << endl
                        << strerror(errno));
         MTIassert(false);
         closeHistoryFiles();
         return HISTORY_ERROR_OPENING_RES_FILE;
      }

      // set the pointer to the current .RES record (in its buffer)
      resRecPtr = resBuf + (resFilePtr - resBaseFileOffset)/sizeof(RESTORE_RECORD);

//      TRACE_2(errout << "========= openHistoryFiles RES RECORD=========" << endl);
//      TRACE_2(dumpRestoreRecord(resRecPtr));
//      TRACE_2(errout << "==============================================" << endl);

      globalSaveNumber =
         (resRecPtr-1)->globalSaveNumber;
   }

   //////////////////////////////////////////////////////////////////////////
   // open the .HDR file
   if ((hdrFileHandle = open(hdrFileName.c_str(),
                          O_BINARY|O_CREAT|O_RDWR, S_IRUSR|S_IWUSR)) == -1) {

      TRACE_0(errout << "ERROR: CSaveRestore::openHistoryFiles: "
                     << " can't open the .HDR file because: " << endl
                     << strerror(errno));
      closeHistoryFiles();
      return HISTORY_ERROR_OPENING_HDR_FILE;
   }

   // determine its length and check it
   hdrFilePtr = lseek(hdrFileHandle,0,SEEK_END);
   MTIassert(hdrFilePtr == totHdrRecs * (int)sizeof(HEADER_RECORD));
   if (hdrFilePtr != totHdrRecs * (int)sizeof(HEADER_RECORD)) {

      TRACE_0(errout << "ERROR: CSaveRestore::openHistoryFiles: "
                     << " .HDR file is wrong size");
      closeHistoryFiles();
      return HISTORY_ERROR_OPENING_HDR_FILE;
   }

   // get the file offset of the active .HDR record
   hdrFilePtr = resBlkIndex*sizeof(HEADER_RECORD);

   // the disk addr of the cur block of .HDR records
   hdrBaseFileOffset = (hdrFilePtr/HEADER_BUFFER_SIZE)*HEADER_BUFFER_SIZE;

   // get number of bytes to read in last block of .HDR records
   hdrBlkLeng = hdrFilePtr - hdrBaseFileOffset;
   if (resBlkLeng > 0)
      hdrBlkLeng += sizeof(HEADER_RECORD);

   // go there and read last block of .HDR records
   if (hdrBlkLeng > 0) {

      int result = lseek(hdrFileHandle, hdrBaseFileOffset, SEEK_SET);
      if (result == -1) {

         TRACE_0(errout << "ERROR: History HDR file seek failed because "
                        << strerror(errno));
         MTIassert(result != -1);
         return HISTORY_ERROR_SEEKING_IN_HDR_FILE;
      }
      MTIassert(result == hdrBaseFileOffset);

      result = read(hdrFileHandle, hdrBuf, hdrBlkLeng);
      if (result == -1) {
         TRACE_0(errout << "ERROR: CSaveRestore::openHistoryFiles: "
                        << " can't read the .HDR file because: " << endl
                        << strerror(errno));
         MTIassert(result != -1);
         closeHistoryFiles();
         return HISTORY_ERROR_OPENING_HDR_FILE;
      }
      MTIassert(result == hdrBlkLeng);
   }

   // set the .HDR record ptr to the last .HDR record
   hdrRecPtr = hdrBuf + (hdrFilePtr - hdrBaseFileOffset)/sizeof(HEADER_RECORD);

   //////////////////////////////////////////////////////////////////////////
   // open the .PXL channel and set the file ptr to the end for appending
   if ((pxlFileHandle = pxlChannel->openPixelChannel(PXLBUF_SIZE,
                                    pxlComponentCount,
                                    pxlFileName)) == -1) {

      TRACE_0(errout << "ERROR: CSaveRestore::openHistoryFiles: "
                     << " can't open the .PIX file because: " << endl
                     << strerror(errno));
      closeHistoryFiles();
      return HISTORY_ERROR_OPENING_PXL_FILE;
   }

   return 0;
}
//------------------------------------------------------------------------------

int CSaveRestore::openHistoryFiles_FPF()
{
   // clear the list of fixes made by frame
   FixDiscardList.clear();

   // make sure this exists
   // ** CAREFUL! MakeDirectory returns false on FAIL, NOT NON-ZERO!! **
   // And the error is passed back in the global struct theError.
   if (false == MakeDirectory(srcClipHistoryFolder))
   {
      TRACE_0(errout << "ERROR: CSaveRestore::openHistoryFiles: "
                     << "Can't open source clip history folder "
                     << srcClipHistoryFolder);
      TRACE_0(errout << theError.getFmtError());

      // Hmmm, now what?
   }

   MTIassert(pxlFileHandle < 0 && currentHistoryFileOpen < 0);
   if ((pxlFileHandle >= 0)||(currentHistoryFileOpen >= 0)) {

      TRACE_0(errout << "ERROR: CSaveRestore::openHistoryFiles: "
                     << "Bad file handle or current history file");
   }

   // no files should be open
   pxlFileHandle = -1;

   // clear the current file open
   currentHistoryFileOpen = -1;

   // clear the mixed frames flag
   mixedFrames = false;

   // read the global save number from file    ?????? WTF QQQ

   return 0;
}



/////////////////////////////////////////////////////////////////
//
//             c l o s e H i s t o r y F i l e s
//
// Try to close any history files which are open.
//
int CSaveRestore::closeHistoryFiles()
{
   return (historyType == HISTORY_TYPE_MONOLITHIC)
            ? closeHistoryFiles_MONO()
            : closeHistoryFiles_FPF();
}
//------------------------------------------------------------------------------

int CSaveRestore::closeHistoryFiles_MONO()
{
   if (hdrBaseFileOffset >= 0) { // not all .HDR records were done

      // make sure modified .RES records are flushed out
      if (resWriteback) {

         if (lseek(resFileHandle, resBaseFileOffset, SEEK_SET) == -1) {

            return HISTORY_ERROR_SEEKING_IN_RES_FILE;
         }

         int result = write(resFileHandle, resBuf, resBlkLeng);
         if (result != resBlkLeng) {

            return HISTORY_ERROR_WRITING_RES_FILE;
         }
      }
   }

   // close the .HDR file
   if (hdrFileHandle >= 0) {

      if (close(hdrFileHandle) < 0) {

         return HISTORY_ERROR_CLOSING_HDR_FILE;
      }
      hdrFileHandle = -1;
   }

   // close the .RES file
   if (resFileHandle >= 0) {

      if (close(resFileHandle) < 0) {

         return HISTORY_ERROR_CLOSING_RES_FILE;
      }
      resFileHandle = -1;
   }

   // close the .PXL file
   if (pxlFileHandle >= 0) {

      if (pxlChannel->closePixelChannel() < 0) {

         pxlFileHandle = -1;

         return HISTORY_ERROR_CLOSING_PXL_FILE;
      }

      pxlFileHandle = -1;
   }

   return 0;
}
//------------------------------------------------------------------------------

int CSaveRestore::closeHistoryFiles_FPF()
{
   bool closeCondition = ((currentHistoryFileOpen == -1)&&(pxlFileHandle == -1))||
                         ((currentHistoryFileOpen != -1)&&(pxlFileHandle >=  0));

   MTIassert(closeCondition);

   if (currentHistoryFileOpen == -1)
   {
      // Nothing to save!
      return 0;
   }

   auto closePixelChannelError = pxlChannel->closePixelChannel();
   pxlFileHandle = -1;
   currentHistoryFileOpen = -1;
   MTIassert(closePixelChannelError == 0);
   if (closePixelChannelError != 0)
   {
      return HISTORY_ERROR_CLOSING_PXL_FILE;
   }

   // write the current global save number back to file  ?????? WTF QQQ

   return 0;
}
//------------------------------------------------------------------------------

int CSaveRestore::newGlobalSaveNumber()
{
   CAutoThreadLocker lock(*getLock());

  return(++globalSaveNumber);
}

int CSaveRestore::getGlobalSaveNumber()
{
  return(globalSaveNumber);
}

////////////////////////////////////////////////////////////////////
//
//   C O P Y  P A R E N T  H I S T O R Y  T O  V E R S I O N
//
// for a version clip and file-per-frame, copies frame's history
// file from parent history directory to version history directory
int CSaveRestore::copyParentHistoryToVersion(int frmnum)
{
	if (historyType != HISTORY_TYPE_FILE_PER_FRAME)
	{
      // Operation not supported for monolithic files!
      return 0;
   }

   CFrame *frame = srcVideoFrameList->getBaseFrame(frmnum);
   MTIassert(frame != nullptr);
   if (frame == nullptr)
   {
      return HISTORY_ERROR_INTERNAL;
   }

   // may be in the parent folder if new media not allocated for frame
   string currentHistoryFilePath = getHistoryFilePath(frame);

   // this is always the path to the version media folder
   string versionHistoryFilePath = getNewHistoryFilePath(frame);

   if (currentHistoryFilePath != versionHistoryFilePath)
   {
      if (!DoesFileExist(versionHistoryFilePath.c_str()))
      {
         // init the history file from the parent folder
         if (false == MTICopyFile(currentHistoryFilePath, versionHistoryFilePath, false))
         {
         }
      }
   }

	return 0;
}

////////////////////////////////////////////////////////////////////
//
//   S A V E  R E G I O N  L I S T  F O R  F R A M E / F I E L D
//
// save the data  to restore a frame/field/region, including also
// the cmd string which generated the save (this can be null);
int CSaveRestore::saveRegionList(
        int srcfrm,         // frame index
        int fldindx,        // field index
        int oddeven,        // 0 even , 1 odd, -1 both
        int toolcod,        // tool code
        const char *pdlstr, // -> ASCIIZ cmd string
        CPixelRegionList& srclst
       )
{
   return (historyType == HISTORY_TYPE_MONOLITHIC)
            ? saveRegionList_MONO(srcfrm, fldindx, oddeven, toolcod, pdlstr, srclst)
            : saveRegionList_FPF(srcfrm, fldindx, oddeven, toolcod, pdlstr, srclst);
}
//------------------------------------------------------------------------------

int CSaveRestore::saveRegionList_MONO(
        int srcfrm,         // frame index
        int fldindx,        // field index
        int oddeven,        // 0 even , 1 odd, -1 both
        int toolcod,        // tool code
        const char *pdlstr, // -> ASCIIZ cmd string
        CPixelRegionList& srclst
       )
{
   // protocol error if files aren't open
   MTIassert(resFileHandle != -1);
   if (resFileHandle == -1)
      return HISTORY_ERROR_INTERNAL;

   // if no more room for .RES records do
   //    write out the current buffer
   //    advance the base ptr
   //    reset the fill ptr to the beg of the buffer
   //    advance to the next .HDR record
   //    if the buffer was full
   //       write it out
   //       advance the base ptr
   //       reset the fill ptr
   //
   if (resRecPtr == resBufStop) {

      int result = lseek(resFileHandle, resBaseFileOffset, SEEK_SET);
      if (result == -1) {

         TRACE_0(errout << "ERROR: History RES file seek failed because "
                        << strerror(errno));
         MTIassert(result != -1);
         return HISTORY_ERROR_SEEKING_IN_RES_FILE;
      }
      MTIassert(result == resBaseFileOffset);

      result = write(resFileHandle, resBuf, RESTORE_BUFFER_SIZE);
    if (result != (int)RESTORE_BUFFER_SIZE) {

         TRACE_0(errout << "ERROR: History RES file write failed because "
                        << ((result == -1)? strerror(errno) : " disk is full"));
      MTIassert(result == (int)RESTORE_BUFFER_SIZE);
         return HISTORY_ERROR_WRITING_RES_FILE;
      }
    MTIassert(result == (int)RESTORE_BUFFER_SIZE);

      resBaseFileOffset += RESTORE_BUFFER_SIZE;
      resRecPtr = resBuf;

      // advance to a new .HDR record
      // if none left in this buffer do
      //    write out buffer of .HDR records
      //    advance the base ptr to a new block of .HDR records
      //    reset the fill ptr
      //
      hdrRecPtr++;
      if (hdrRecPtr == hdrBufStop) {

         int result = lseek(hdrFileHandle, hdrBaseFileOffset, SEEK_SET);
         if (result == -1) {

            TRACE_0(errout << "ERROR: History HDR file seek failed because "
                           << strerror(errno));
            MTIassert(result != -1);
            return HISTORY_ERROR_SEEKING_IN_HDR_FILE;
         }
         MTIassert(result == hdrBaseFileOffset);

         result = write(hdrFileHandle, hdrBuf, HEADER_BUFFER_SIZE);
      if (result != (int)HEADER_BUFFER_SIZE) {

            TRACE_0(errout << "ERROR: History HDR file write failed because "
                           << ((result == -1)? strerror(errno) : " disk is full"));
         MTIassert(result == (int)HEADER_BUFFER_SIZE);
            return HISTORY_ERROR_WRITING_HDR_FILE;
         }
         MTIassert(result == (int)HEADER_BUFFER_SIZE);

         hdrBaseFileOffset += HEADER_BUFFER_SIZE;
         hdrRecPtr = hdrBuf;
      }
   }

   // include the frame in the .HDR record
   // if the .RES record is the first in a block do
   //    copy the frame number into both entries in the .HDR record
   // else
   //    include the frame number in the max and min of the .HDR rec
   //
   if (resRecPtr == resBuf) {
      hdrRecPtr->minFrame = srcfrm;
      hdrRecPtr->maxFrame = srcfrm;
   }
   else {
      if (srcfrm < hdrRecPtr->minFrame)
         hdrRecPtr->minFrame = srcfrm;
      if (srcfrm > hdrRecPtr->maxFrame)
         hdrRecPtr->maxFrame = srcfrm;
   }

   // create the  new .RES record
   resRecPtr->frameNum     = srcfrm;
   resRecPtr->fieldIndex   = fldindx;
   resRecPtr->fieldNumber  = oddeven;
   resRecPtr->toolCode     = toolcod;

   resRecPtr->globalSaveNumber = globalSaveNumber;

   // record absolute time of save
   resRecPtr->saveTime = time(nullptr);

   // init flag word
   resRecPtr->restoreFlag   = 0;

   // bounding rectangle from the pixel region list
   resRecPtr->extent.left   = 0x7fffffff;
   resRecPtr->extent.top    = 0x7fffffff;
   resRecPtr->extent.right  = 0xffffffff;
   resRecPtr->extent.bottom = 0xffffffff;

   if (srclst.IsBoundingBoxValid())
      resRecPtr->extent = srclst.GetBoundingBox();

   //////////////////////////////////////////////////////////////////
   // mark the place in the .PXL file in the .RES record. Extract
   // the pixels for the desired region/field. Write the pixels out
   // to the .PXL file. Advance the cur pos ptr in the .PXL file.

   bool evn = (oddeven==0)||(oddeven==-1);
   bool odd = (oddeven==1)||(oddeven==-1);

   // record the offset into the pixel file in the .RES record
   resRecPtr->pxlFileByteOffset = pxlChannel->getPixelChannelFilePtr();

   // now output the region list to the pixel channel
   int icnt = srclst.PutRegionListToChannel(pxlChannel,evn,odd);

   // record the count of bytes written to the .PXL file in the .RES record
   resRecPtr->pxlByteCount = icnt;

//      TRACE_2(errout << "=========TTT NEW RES RECORD=========" << endl);
//      TRACE_2(dumpRestoreRecord(resRecPtr));
//      TRACE_2(errout << "====================================" << endl);

   //////////////////////////////////////////////////////////////////
   // done with the new .RES record -- advance
   //
   resRecPtr++;

   return 0;
}
//------------------------------------------------------------------------------

int CSaveRestore::saveRegionList_FPF(
        int srcfrm,         // frame index
        int fldindx,        // field index
        int oddeven,        // 0 even , 1 odd, -1 both
        int toolcod,        // tool code
        const char *pdlstr, // -> ASCIIZ cmd string
        CPixelRegionList& srclst
       )
{
   CAutoErrorReporter autoErr("CSaveRestore::saveRegionList_FPF",
                              AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   if (openHistoryFileForFrame(srcfrm) == -1)
   {
      autoErr.msg << "ERROR: Can't open history file for frame " << srcfrm << "." << endl
                     << "History will not be saved for this frame!";
      autoErr.errorCode = HISTORY_ERROR_OPENING_HST_FILE;
      return autoErr.errorCode;
   }

   // get ptr from channel
   RESTORE_RECORD *restoreRecPtr = &pxlChannel->getRestoreRecord()->resRec;
   if (restoreRecPtr == nullptr)
   {
      autoErr.msg << "ERROR: Can't find restore records for frame " << srcfrm << "." << endl
                     << "History will not be saved for this frame!";
      autoErr.errorCode = HISTORY_ERROR_OPENING_HST_FILE;
      return autoErr.errorCode;
   }

//      TRACE_2(errout << "========= saveRegionList BEFORE      =========" << endl);
//      TRACE_2(dumpRestoreRecord(restoreRecPtr));
//      TRACE_2(errout << "==============================================" << endl);

   // create the  new .RES record
   restoreRecPtr->frameNum     = srcfrm;
   restoreRecPtr->fieldIndex   = fldindx;
   restoreRecPtr->fieldNumber  = oddeven;
   restoreRecPtr->toolCode     = toolcod;

   // record absolute time of save
   restoreRecPtr->saveTime = time(nullptr);

   // init flag word
   restoreRecPtr->restoreFlag   = 0;

   // bounding rectangle from the pixel region list
   restoreRecPtr->extent.left   = frmWdth-1;
   restoreRecPtr->extent.top    = frmHght-1;
   restoreRecPtr->extent.right  = 0;
   restoreRecPtr->extent.bottom = 0;

   if (srclst.IsBoundingBoxValid())
      restoreRecPtr->extent = srclst.GetBoundingBox();

   //////////////////////////////////////////////////////////////////
   // mark the place for the pixels in the RES record. Extract
   // the pixels for the desired region/field. Write the pixels out
   // to the .HST file. Advance the cur pos ptr in the .HST file.

   bool evn = (oddeven==0)||(oddeven==-1);
   bool odd = (oddeven==1)||(oddeven==-1);

   // now output the region list to the pixel channel
   int result = srclst.PutRegionListToChannel(pxlChannel,evn,odd);
   if (result == -1)
      return(-1);

//      TRACE_2(errout << "========= saveRegionList AFTER       =========" << endl);
//      TRACE_2(dumpRestoreRecord(restoreRecPtr));
//      TRACE_2(errout << "==============================================" << endl);

   // everything went out -- make an entry into the list
   FixDiscardList.push_back(srcfrm);

   return(0);
}

////////////////////////////////////////////////////////////////////////
//
//   S A V E  D I F F E R E N C E  R U N S  F O R  F R A M E / F I E L D
//
// save the data  to restore a frame/field/region, including also
// the cmd string which generated the save (this can be null);
int CSaveRestore::saveDifferenceRuns(
        int srcfrm,           // frame index
        int fldindx,          // field index
        int oddeven,          // 0 even , 1 odd, -1 both
        int toolcod,          // tool code
        const char *pdlstr,   // -> ASCIIZ cmd string
        MTI_UINT16 *iniFrame, // unprocessed frame
        MTI_UINT16 *finFrame, // processed frame
        MTI_UINT16 *srcFrame, // src frame for runs
        RECT *extent          // extent of diff runs
       )
{
   return (historyType == HISTORY_TYPE_MONOLITHIC)
            ? saveDifferenceRuns_MONO(srcfrm, fldindx, oddeven, toolcod, pdlstr, iniFrame, finFrame, srcFrame, extent)
            : saveDifferenceRuns_FPF(srcfrm, fldindx, oddeven, toolcod, pdlstr, iniFrame, finFrame, srcFrame, extent);
}
//------------------------------------------------------------------------------

int CSaveRestore::saveDifferenceRuns_MONO(
        int srcfrm,           // frame index
        int fldindx,          // field index
        int oddeven,          // 0 even , 1 odd, -1 both
        int toolcod,          // tool code
        const char *pdlstr,   // -> ASCIIZ cmd string
        MTI_UINT16 *iniFrame, // unprocessed frame
        MTI_UINT16 *finFrame, // processed frame
        MTI_UINT16 *srcFrame, // src frame for runs
        RECT *extent          // extent of diff runs
       )
{
   int bytcnt, result;

   // protocol error if files aren't open
   MTIassert(resFileHandle != -1);
   if (resFileHandle == -1)
      return HISTORY_ERROR_INTERNAL;

   // if no more room for .RES records do
   //    write out the current buffer
   //    advance the base ptr
   //    reset the fill ptr to the beg of the buffer
   //    advance to the next .HDR record
   //    if the buffer was full
   //       write it out
   //       advance the base ptr
   //       reset the fill ptr
   //
   if (resRecPtr == resBufStop) {

      if (lseek(resFileHandle, resBaseFileOffset, SEEK_SET) == -1) {

         TRACE_0(errout << "ERROR: History RES file seek failed because "
                        << strerror(errno));
         MTIassert(result != -1);
         return HISTORY_ERROR_SEEKING_IN_RES_FILE;
      }

      MTIassert(result == resBaseFileOffset);

      result = write(resFileHandle, resBuf, RESTORE_BUFFER_SIZE);
      if (result != (int)RESTORE_BUFFER_SIZE)
      {
         TRACE_0(errout << "ERROR: History RES file write failed because " << ((result == -1) ? strerror(errno) : " disk is full"));
         MTIassert(result == (int)RESTORE_BUFFER_SIZE);
         return HISTORY_ERROR_WRITING_RES_FILE;
      }

      MTIassert(result == (int)RESTORE_BUFFER_SIZE);

      resBaseFileOffset += RESTORE_BUFFER_SIZE;
      resRecPtr = resBuf;

      // advance to a new .HDR record
      // if none left in this buffer do
      //    write out buffer of .HDR records
      //    advance the base ptr to a new block of .HDR records
      //    reset the fill ptr
      //
      hdrRecPtr++;
      if (hdrRecPtr == hdrBufStop) {

         if (lseek(hdrFileHandle, hdrBaseFileOffset, SEEK_SET) == -1) {

            TRACE_0(errout << "ERROR: History HDR file seek failed because "
                           << strerror(errno));

            return HISTORY_ERROR_SEEKING_IN_HDR_FILE;
         }

         result = write(hdrFileHandle, hdrBuf, HEADER_BUFFER_SIZE);
      if (result != (int)HEADER_BUFFER_SIZE) {

            TRACE_0(errout << "ERROR: History HDR file write failed because "
                           << ((result == -1)? strerror(errno) : " disk is full"));
            MTIassert(result == (int)HEADER_BUFFER_SIZE);
            return HISTORY_ERROR_WRITING_HDR_FILE;
         }

         MTIassert(result == (int)HEADER_BUFFER_SIZE);

         hdrBaseFileOffset += HEADER_BUFFER_SIZE;
         hdrRecPtr = hdrBuf;
      }
   }

   // include the frame in the .HDR record
   // if the .RES record is the first in a block do
   //    copy the frame number into both entries in the .HDR record
   // else
   //    include the frame number in the max and min of the .HDR rec
   //
   if (resRecPtr == resBuf) {
      hdrRecPtr->minFrame = srcfrm;
      hdrRecPtr->maxFrame = srcfrm;
   }
   else {
      if (srcfrm < hdrRecPtr->minFrame)
         hdrRecPtr->minFrame = srcfrm;
      if (srcfrm > hdrRecPtr->maxFrame)
         hdrRecPtr->maxFrame = srcfrm;
   }

   // create the  new .RES record
   resRecPtr->frameNum     = srcfrm;
   resRecPtr->fieldIndex   = fldindx;
   resRecPtr->fieldNumber  = oddeven;
   resRecPtr->toolCode     = toolcod;

   resRecPtr->globalSaveNumber = globalSaveNumber;

   // record absolute time of save
   resRecPtr->saveTime = time(nullptr);

   // init flag word
   resRecPtr->restoreFlag   = 0;

   //////////////////////////////////////////////////////////////////
   // mark the place in the .PXL file in the .RES record. Extract
   // the pixels for the desired region/field. Write the pixels out
   // to the .PXL file. Advance the cur pos ptr in the .PXL file.

   bool evn = (oddeven==0)||(oddeven==-1);
   bool odd = (oddeven==1)||(oddeven==-1);

   // record the offset into the pixel file in the .RES record
   resRecPtr->pxlFileByteOffset = pxlChannel->getPixelChannelFilePtr();

   // put the runs out, get total bytes
   resRecPtr->pxlByteCount =

               pxlChannel->putDifferenceRunsToChannel(iniFrame,
                                                      finFrame,
                                                      srcFrame,
                                                      nullptr,
                                                      frmWdth,
                                                      frmHght,
                                                      evn, odd,
                                                      &(resRecPtr->extent));
   // advance the pointer
   resRecPtr++;

   return 0;
}
//------------------------------------------------------------------------------

int CSaveRestore::saveDifferenceRuns_FPF(
        int srcfrm,           // frame index
        int fldindx,          // field index
        int oddeven,          // 0 even , 1 odd, -1 both
        int toolcod,          // tool code
        const char *pdlstr,   // -> ASCIIZ cmd string
        MTI_UINT16 *iniFrame, // unprocessed frame
        MTI_UINT16 *finFrame, // processed frame
        MTI_UINT16 *srcFrame, // src frame for runs
        RECT *extent          // extent of diff runs
       )
{
   if (openHistoryFileForFrame(srcfrm) == -1)
      return HISTORY_ERROR_OPENING_HST_FILE;

   RESTORE_RECORD *restoreRecPtr = &pxlChannel->getRestoreRecord()->resRec;

//      TRACE_2(errout << "========= saveDifferenceRuns BEFORE  =========" << endl);
//      TRACE_2(dumpRestoreRecord(restoreRecPtr));
//      TRACE_2(errout << "==============================================" << endl);

   // create the  new .RES record
   restoreRecPtr->frameNum     = srcfrm;
   restoreRecPtr->fieldIndex   = fldindx;
   restoreRecPtr->fieldNumber  = oddeven;
   restoreRecPtr->toolCode     = toolcod;

   // record absolute time of save
   restoreRecPtr->saveTime = time(nullptr);

   // init flag word
   restoreRecPtr->restoreFlag   = 0;

   // init bounding rectangle
   restoreRecPtr->extent.left   = frmWdth-1;
   restoreRecPtr->extent.top    = frmHght-1;
   restoreRecPtr->extent.right  = 0;
   restoreRecPtr->extent.bottom = 0;

   //////////////////////////////////////////////////////////////////
   // mark the place in the .HST file in the RES record. Extract
   // the pixels for the desired region/field. Write the pixels out
   // to the .HST file. Advance the cur pos ptr in the .HST file.

   bool evn = (oddeven==0)||(oddeven==-1);
   bool odd = (oddeven==1)||(oddeven==-1);

   //   IF MODE is BLOBS then do
   //   limn the difference runs by BLOB
   //   for each BLOB do
   //      put the difference runs out to the frame's history file
   //      delete the BLOB edges
   //   next BLOB

   int result;

   /**********************
   if (false) {

      // get a vector of poly's, each one the outline of a blob
      //
      vector <vector <POINT> *> *BlobIslands =

                                    limnDifferenceRuns(iniFrame, finFrame);

      int nblobs = BlobIslands->size();

      for (int i=0; i < nblobs; i++) {

         RESTORE_RECORD *restoreRecPtr = pxlChannel->getRestoreRecord();

         // basics of new .RES record
         restoreRecPtr->frameNum     = srcfrm;
         restoreRecPtr->fieldIndex   = fldindx;
         restoreRecPtr->fieldNumber  = oddeven;
         restoreRecPtr->toolCode     = toolcod;

         // record absolute time of save
         restoreRecPtr->saveTime = time(nullptr);

         // init flag word
         restoreRecPtr->restoreFlag   = 0;

         // the polygon outline of the blob (explicitly closed)
         vector <POINT> *blobOutl = (*BlobIslands)[i];

         // create one history entry for this blob outline
         result = pxlChannel->putDifferenceRunsToChannel(iniFrame,
                                                         finFrame,
                                                         srcFrame,
                                                         blobOutl,
                                                         frmWdth,
                                                         frmHght,
                                                         evn, odd);
         // delete the blob poly
         delete (*BlobIslands)[i];
      }

      delete BlobIslands; // note this

   }
   else {   ********************/

      // basics of new .RES record
      restoreRecPtr->frameNum     = srcfrm;
      restoreRecPtr->fieldIndex   = fldindx;
      restoreRecPtr->fieldNumber  = oddeven;
      restoreRecPtr->toolCode     = toolcod;

      // record absolute time of save
      restoreRecPtr->saveTime = time(nullptr);

      // init flag word
      restoreRecPtr->restoreFlag   = 0;

      // create one history entry for ALL the altered pixels
      result = pxlChannel->putDifferenceRunsToChannel(iniFrame, // unprocessed frame
                                                      finFrame, // processed frame
                                                      srcFrame, // src frame for runs
                                                      nullptr,     // entire frame
                                                      frmWdth,
                                                      frmHght,
                                                      evn, odd);
   /* }  */

   if (result == -1)
      return(-1);

   // everything went out -- make an entry into the list
   FixDiscardList.push_back(srcfrm);

//      TRACE_2(errout << "========= saveDifferenceRuns AFTER   =========" << endl);
//      TRACE_2(dumpRestoreRecord(restoreRecPtr));
//      TRACE_2(errout << "==============================================" << endl);

   return(0);
}

////////////////////////////////////////////////////////////////
//
//                 f l u s h S a v e
//
int CSaveRestore::flushSave()
{
   return (historyType == HISTORY_TYPE_MONOLITHIC)
            ? flushSave_MONO()
            : flushSave_FPF();
}
//------------------------------------------------------------------------------

int CSaveRestore::flushSave_MONO()
{
   int bytcnt, result;

   //////////////////////////////////////////////////////////
   // flush the .RES file

   if (lseek(resFileHandle, resBaseFileOffset, SEEK_SET) == -1) {

      TRACE_0(errout << "ERROR: History RES file seek failed because "
                     << strerror(errno));
      MTIassert(false);
      return HISTORY_ERROR_SEEKING_IN_RES_FILE;
   }

   bytcnt = (resRecPtr - resBuf)*sizeof(RESTORE_RECORD);
   result = write(resFileHandle, resBuf, bytcnt);
   if (result != bytcnt) {

      TRACE_0(errout << "ERROR: History RES file write failed because "
                     << ((result == -1)? strerror(errno) : " disk is full"));
      MTIassert(result == bytcnt);
      return HISTORY_ERROR_WRITING_RES_FILE;
   }

   MTIassert(result == bytcnt);

   //////////////////////////////////////////////////////////
   // flush the .HDR file
   if (lseek(hdrFileHandle, hdrBaseFileOffset, SEEK_SET) == -1) {

      TRACE_0(errout << "ERROR: History HDR file seek failed because "
                     << strerror(errno));
      MTIassert(false);
      return HISTORY_ERROR_SEEKING_IN_HDR_FILE;
   }

   bytcnt = (hdrRecPtr - hdrBuf)*sizeof(HEADER_RECORD);
   if (resRecPtr > resBuf) bytcnt += sizeof(HEADER_RECORD);

   result = write(hdrFileHandle, hdrBuf, bytcnt);
   if (result != bytcnt) {

      TRACE_0(errout << "ERROR: History HDR file write failed because "
                     << ((result == -1)? strerror(errno) : " disk is full"));
      MTIassert(result == bytcnt);
      return HISTORY_ERROR_WRITING_HDR_FILE;
   }

   return 0;
}
//------------------------------------------------------------------------------

int CSaveRestore::flushSave_FPF()
{
   if (currentHistoryFileOpen == -1)
   {
      // Nothing to save!
      return 0;
   }

   auto closePixelChannelError = pxlChannel->closePixelChannel();
   pxlFileHandle = -1;
   currentHistoryFileOpen = -1;
   MTIassert(closePixelChannelError == 0);
   if (closePixelChannelError != 0)
   {
      return HISTORY_ERROR_CLOSING_PXL_FILE;
   }

   return 0;
}

////////////////////////////////////////////////////////////////
//
//                 f l u s h R e s t o r e
//
int CSaveRestore::flushRestore()
{
   return (historyType == HISTORY_TYPE_MONOLITHIC)
            ? flushRestore_MONO()
            : flushRestore_FPF();
}
//------------------------------------------------------------------------------

int CSaveRestore::flushRestore_MONO()
{
   int result;

   if (hdrBaseFileOffset >= 0) { // not all .HDR records were done

      // make sure modified .RES records are flushed out
      if (resWriteback) {

         result = lseek(resFileHandle, resBaseFileOffset, SEEK_SET);
         if (result == -1) {

            TRACE_0(errout << "ERROR: History RES file seek failed because "
                           << strerror(errno));
            MTIassert(result != -1);
            return HISTORY_ERROR_SEEKING_IN_RES_FILE;
         }

         MTIassert(result == resBaseFileOffset);

         result = write(resFileHandle, resBuf, resBlkLeng);
         if (result != resBlkLeng) {

            TRACE_0(errout << "ERROR: History RES file write failed because "
                           << ((result == -1)? strerror(errno) : " disk is full"));
            MTIassert(result == resBlkLeng);
            return HISTORY_ERROR_WRITING_RES_FILE;
         }
      }
   }

   return 0;
}
//------------------------------------------------------------------------------

int CSaveRestore::flushRestore_FPF()
{
   if (currentHistoryFileOpen == -1)
   {
      // Nothing to close.
      return 0;
   }

   pxlFileHandle = -1;
   currentHistoryFileOpen = -1;

   auto retVal = pxlChannel->closePixelChannel();
   return (retVal == -1) ? HISTORY_ERROR_CLOSING_PXL_FILE : retVal;
}
//------------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////
// get the history filepath for the frame whose index is frmnum
// this will be inside the parent clip's media folder if the
// frame in the version clip has not been written to

string CSaveRestore::getHistoryFilePath(CFrame *frame)
{
   return srcVideoFrameList->getHistoryFilePath(frame);
}

string CSaveRestore::getHistoryFilePath(const ClipSharedPtr &clip, int frmnum)
{
   string historyFilePath;

   // and the => frame list
   CVideoFrameList *frameList = clip->getVideoFrameList(VIDEO_SELECT_NORMAL,
                                                        FRAMING_SELECT_VIDEO);

   /////////////////////////////////////////////////////////////////////////////
   //
   // Only clips using image file media have file-per-frame history

   CFrame *frame = frameList->getBaseFrame(frmnum);
   if (frame == nullptr)
      return historyFilePath;

   return frameList->getHistoryFilePath(frame);
}

string CSaveRestore::getNewHistoryFilePath(CFrame *frame)
{
   string historyFilePath = srcVideoFrameList->getHistoryFilePath(frame);

   int n = historyFilePath.length() ;
   if (n == 0)
      return historyFilePath;

   // now split the historyfilePath into a folder and a file

   int i = (n-1);
   while ((historyFilePath[i] != '\\')&&(i > 0)) i--;

   string historyFileName = historyFilePath.substr((i+1), (n-1));

   return srcClipHistoryFolder + historyFileName;
}

string CSaveRestore::getNewHistoryFilePath(const ClipSharedPtr &clip, int frmnum)
{
   // and the => frame list
   CVideoFrameList *frameList = clip->getVideoFrameList(VIDEO_SELECT_NORMAL,
                                                        FRAMING_SELECT_VIDEO);
   CFrame *frame = frameList->getBaseFrame(frmnum);
   if (frame == nullptr)
      return "";

   CBinManager bm;

   string clipHistoryFolder = bm.getDirtyHistoryFolderPath(clip->getClipPathName());

   if (!clipHistoryFolder.empty()) { // we have it

      clipHistoryFolder = AddDirSeparator(clipHistoryFolder);
   }
   else { // not a version

      clipHistoryFolder = srcVideoFrameList->getHistoryFilePath(frame);

      int i = clipHistoryFolder.length() - 1;
      while ((clipHistoryFolder[i] != '\\')&&(i > 0)) i--;

      clipHistoryFolder = clipHistoryFolder.substr(0, i+1);
   }

   string historyFilePath = frameList->getHistoryFilePath(frame);

   int n = historyFilePath.length() ;
   if (n == 0) // should never happen
      return historyFilePath;

   // now split the imagefilePath into a folder and a file

   int i = (n-1);
   while ((historyFilePath[i] != '\\')&&(i > 0)) i--;

   string historyFileName = historyFilePath.substr((i+1), (n-1));

   return clipHistoryFolder + historyFileName;
}

////////////////////////////////////////////////////////////
//
//  F O R C E   F R A M E - S P E C I F I C  .H S T  O P E N
//
//  IF FILE IS OPEN BUT NOT FOR THE SPECIFIED FRAME
//     FLUSH PIXEL DATA TO OPEN FILE
//     COMPLETE RES RECORD AND FLUSH TO OPEN FILE
//     UPDATE THE RES REC COUNT AND WRITE TO OPEN FILE
//     CLOSE FILE
//  OPEN FILE FOR SPECIFIED FRAME
//  IF FILE IS BRAND-NEW INITIALIZE IT
//  READ RES REC COUNT FROM OPEN FILE
//  SET UP NEXT NEW RES REC
//  SET UP FILE ADDRESSES OF BEG AND END OF PIXEL DATA
//
int CSaveRestore::openHistoryFileForFrame(int frmnum)
{
   int result;
   int bytcnt;

   if (frmnum != currentHistoryFileOpen) {

      if (currentHistoryFileOpen != -1) {

         // write out RES records & resRecCount
         result = pxlChannel->closePixelChannel();
         if (result != 0) {

            pxlFileHandle = -1;

            currentHistoryFileOpen = -1;

            return result;
         }

         pxlFileHandle = -1;

         currentHistoryFileOpen = -1;
      }

      // get the current history file path
      // get the version history filepath (dirty folder)
      // if these are not identical do
      //    copy from the current to the vesion

      CFrame *frame = srcVideoFrameList->getBaseFrame(frmnum);
      MTIassert(frame != nullptr);
      if (frame == nullptr)
      {
         return HISTORY_ERROR_INTERNAL; // was -1;
      }

      // may be in the parent folder if new media not allocated for frame
      string currentHistoryFilePath = getHistoryFilePath(frame);

      // this is always the path to the version media folder
      string versionHistoryFilePath = getNewHistoryFilePath(frame);

      if (currentHistoryFilePath != versionHistoryFilePath) {

         // init the history file from the parent folder
         MTICopyFile(currentHistoryFilePath, versionHistoryFilePath, false);
      }

      pxlFileHandle = pxlChannel->openPixelChannel(0x100000,
                                            pxlComponentCount,
                                            versionHistoryFilePath,
                                            true);
      if (pxlFileHandle == -1)
      {
         // Probably bad history file path or the history file was removed.
         return HISTORY_WARNING_NO_HISTORY_FILE;
      }

      // now the file is officially open
      currentHistoryFileOpen = frmnum;
   }

   return 0;
}

int CSaveRestore::openHistoryFileForFrameRestore(int frmnum)
{
   int result;
   int bytcnt;

   if (frmnum != currentHistoryFileOpen) {

      if (currentHistoryFileOpen != -1) {

         // write out RES records & resRecCount
         result = pxlChannel->closePixelChannel();
         if (result != 0) {

            pxlFileHandle = -1;

            currentHistoryFileOpen = -1;

            return result;
         }

         pxlFileHandle = -1;

         currentHistoryFileOpen = -1;
      }

      // get the current history file path
      // get the version history filepath (dirty folder)
      // if these are not identical do
      //    copy from the current to the vesion

      CFrame *frame = srcVideoFrameList->getBaseFrame(frmnum);
      MTIassert(frame != nullptr);
      if (frame == nullptr)
      {
         return HISTORY_ERROR_INTERNAL; // was -1;
      }

      // this may be in the parent folder if new media not allocated for this frame
      string currentHistoryFilePath = getHistoryFilePath(frame);

      // this is always the path to the version media folder
      string versionHistoryFilePath = getNewHistoryFilePath(frame);

      string hstFilePath = versionHistoryFilePath;
      if (!DoesFileExist(versionHistoryFilePath))
      {
         // no history file for it yet
         hstFilePath = currentHistoryFilePath;
      }

		if (DoesFileExist(hstFilePath) == false)
		{
			return -1;
      }


		pxlFileHandle = pxlChannel->openPixelChannel(0x100000,
																		pxlComponentCount,
																		hstFilePath,
																		true);

		if (pxlFileHandle == -1)
		{
			// What a kludge, but no time
			auto ioError = pxlChannel->getIOError();
			return ioError;
		}

      // now the file is officially open
      currentHistoryFileOpen = frmnum;
   }

   return 0;
}

////////////////////////////////////////////////////////////
//
//  D I S C A R D   N   M O S T - R E C E N T   S A V E S
//
int CSaveRestore::discardNSaves(int numdiscrd)
{
   if (numdiscrd <= 0)
   {
      // Noop
      return 0;
   }

   CAutoThreadLocker lock(*getLock());

   return (historyType == HISTORY_TYPE_MONOLITHIC)
            ? discardNSaves_MONO(numdiscrd)
            : discardNSaves_FPF(numdiscrd);
}
//------------------------------------------------------------------------------

int CSaveRestore::discardNSaves_MONO(int numdiscrd)
{
   int bytcnt, result;

   // open all the files
   if ((hdrFileHandle = open(hdrFileName.c_str(), O_BINARY|O_CREAT|O_RDWR, S_IRUSR|S_IWUSR)) == -1) {

      TRACE_0(errout << "ERROR: CSaveRestore::discardNSaves: "
                     << " can't open the .HDR file because: " << endl
                     << strerror(errno));
      return HISTORY_ERROR_OPENING_HDR_FILE;
   }
   close(hdrFileHandle);
   hdrFileHandle = -1;

   if (pxlChannel->openPixelChannel(PXLBUF_SIZE, pxlComponentCount, pxlFileName)==-1) {

      TRACE_0(errout << "ERROR: CSaveRestore::discardNSaves: "
                     << " can't open the .PIX file because: " << endl
                     << strerror(errno));
      return HISTORY_ERROR_OPENING_PXL_FILE;
   }
   pxlChannel->closePixelChannel();
   // pxlFileHandle = -1;   no - we didn't save it

   if ((resFileHandle = open(resFileName.c_str(), O_BINARY|O_CREAT|O_RDWR, S_IRUSR|S_IWUSR)) == -1) {

      TRACE_0(errout << "ERROR: CSaveRestore::discardNSaves: "
                     << " can't open the .RES file because: " << endl
                     << strerror(errno));
      return HISTORY_ERROR_OPENING_RES_FILE;
   }

   // the .RES file is open - determine its length
   if ((resFilePtr = lseek(resFileHandle,0,SEEK_END)) == -1) {

      return HISTORY_ERROR_SEEKING_IN_RES_FILE;
   }

   // back it off by numdiscrd records
   resFilePtr -= numdiscrd * sizeof(RESTORE_RECORD);

   // if negative, truncate all files to 0 and return
   if (resFilePtr < 0) {

      int retVal = 0;

      close(resFileHandle);
      resFileHandle = -1;

      if (truncate64(hdrFileName.c_str(),0) != 0)
         retVal = HISTORY_ERROR_TRUNCATING_HDR_FILE;
      if (truncate64(resFileName.c_str(),0) != 0)
         retVal = HISTORY_ERROR_TRUNCATING_RES_FILE;
      if (truncate64(pxlFileName.c_str(),0) != 0)
         retVal = HISTORY_ERROR_TRUNCATING_PXL_FILE;

      TRACE_0(errout << "INFO: CSaveRestore::discardNSaves: "
                     << " Truncated all history files, retval: " << retVal);

      return retVal;
   }

   // resFilePtr = file offset of first .RES record truncated
   // go there and get the record before truncating the file
   result = lseek(resFileHandle, resFilePtr, SEEK_SET);
   if (result == -1) {

      TRACE_0(errout << "ERROR: History RES file seek failed because "
                     << strerror(errno));
      MTIassert(result != -1);
      return HISTORY_ERROR_SEEKING_IN_RES_FILE;
   }

   MTIassert(result == resFilePtr);

   result = read(resFileHandle, resBuf, sizeof(RESTORE_RECORD));
   resRecPtr = resBuf;
   if (result == -1) {

      TRACE_0(errout << "ERROR: History RES file read failed because "
                     << strerror(errno));
      MTIassert(result != -1);
      return HISTORY_ERROR_READING_RES_FILE;
   }

   MTIassert(result == (int)sizeof(RESTORE_RECORD));

   close(resFileHandle);
   resFileHandle = -1;
   if (truncate64(resFileName.c_str(),resFilePtr) != 0) {

      TRACE_0(errout << "ERROR: CSaveRestore::discardNSaves: "
                     << " can't truncate the .RES file because: " << endl
                     << strerror(errno));

      return HISTORY_ERROR_TRUNCATING_RES_FILE;
   }

   // the disk addr of the last remaining .RES record is used
   // to compute the length of the truncated .HDR file

   resFilePtr -= sizeof(RESTORE_RECORD);
   hdrFilePtr = 0;
   if (resFilePtr >= 0) { // at least one .RES record
      resBlkIndex = resFilePtr/(RESTORE_BUFFER_SIZE);
      hdrFilePtr = (resBlkIndex+1)*sizeof(HEADER_RECORD);
   }
   if (truncate64(hdrFileName.c_str(),hdrFilePtr) != 0) {

      TRACE_0(errout << "ERROR: CSaveRestore::discardNSaves: "
                     << " can't truncate the .HDR file because: " << endl
                     << strerror(errno));
      return HISTORY_ERROR_TRUNCATING_HDR_FILE;
   }

   // do same for the .PXL file - BUG was in not setting resRecPtr -> resBuf

   if (truncate64(pxlFileName.c_str(),resRecPtr->pxlFileByteOffset) != 0) {


      TRACE_0(errout << "ERROR: CSaveRestore::discardNSaves: "
                     << " can't truncate the .PIX file because: " << endl
                     << strerror(errno));
      return HISTORY_ERROR_TRUNCATING_PXL_FILE;
   }

   return 0;
}
//------------------------------------------------------------------------------

int CSaveRestore::discardNSaves_FPF(int numdiscrd)
{
   int numFixes = FixDiscardList.size();
   MTIassert(numFixes >= numdiscrd);
   if (numdiscrd > numFixes)
   {
      // ?? QQQ Why wouldn't you want to discard numfixes fixes?
      return -1;
   }

   while (numdiscrd > 0)
   {
      int curFrame = FixDiscardList.back();
      int dscrdThisFrame = 0;

      while ((numdiscrd > 0) && (FixDiscardList.back() == curFrame))
      {
         FixDiscardList.pop_back();
         dscrdThisFrame++;
         numdiscrd--;
      }

      discardNFixesFromHistoryFile(curFrame, dscrdThisFrame);
   }

   return (0);
}
//------------------------------------------------------------------------------

////////////////////////////////////////////////////////////////
//
//   O P E N   F O R   A   S E R I E S   O F   R E S T O R E S
//
// Initializes to search the database from most-recent to least-
// recent restore records satisfying a simple filter
int CSaveRestore::openForRestore(
										 int  dstfrm,         // dst frame
                               int  dstfld,         // dst field
                               RECT  *rect,         // -> rect
                               POINT *lasso,        // -> lasso
                               BEZIER_POINT *bezier // -> bezier
                             )
{
   // the frame to restore (IS THIS EVER -1?)
   filterFrame = dstfrm;

   // the field to restore (may be -1 for wildcard)
   filterField = (MTI_INT8) dstfld;

   // first determine the correct filter rectangle

   // assume that a mask will be used
   useMask = maskBuf;

   if (bezier == nullptr) { // no bezier -> get lasso

      if (lasso == nullptr) { // no lasso -> get rectangle

         // the region to restore
         if (rect == nullptr) { // no rectangle -> get everything

            // no mask is needed now
            useMask = nullptr;

            // .. but filter region must still be set
            filterRegion.left   = 0;
            filterRegion.top    = 0;
            filterRegion.right  = frmWdth - 1;
            filterRegion.bottom = frmHght - 1;
         }
         else {

            // set filter region from arg rectangle
            filterRegion.left   = rect->left;
            filterRegion.top    = rect->top;
            filterRegion.right  = rect->right;
            filterRegion.bottom = rect->bottom;

            // clip filter region to frame
            if (filterRegion.left < 0) filterRegion.left = 0;
            if (filterRegion.top  < 0) filterRegion.top  = 0;
            if (filterRegion.right >= frmWdth)
               filterRegion.right = frmWdth-1;
            if (filterRegion.bottom >= frmHght)
               filterRegion.bottom = frmHght-1;

         }
      }
      else { // set filter rectangle = bound of lasso

         filterRegion.left   = MAXFRAMEX;
         filterRegion.top    = MAXFRAMEY;
         filterRegion.right  = MINFRAMEX;
         filterRegion.bottom = MINFRAMEY;

         for (int i=0;;i++) {

            // include the point in the box
            if (lasso[i].x < filterRegion.left)
               filterRegion.left = lasso[i].x;
            if (lasso[i].y < filterRegion.top)
               filterRegion.top  = lasso[i].y;
            if (lasso[i].x > filterRegion.right)
               filterRegion.right = lasso[i].x;
            if (lasso[i].y > filterRegion.bottom)
               filterRegion.bottom = lasso[i].y;

            // if closed, you're done
            if ((i>2)&&
                (lasso[i].x == lasso[0].x)&&
                (lasso[i].y == lasso[0].y))
               break;
         }

         // clip filter region to frame
         if (filterRegion.left < 0) filterRegion.left = 0;
         if (filterRegion.top  < 0) filterRegion.top  = 0;
         if (filterRegion.right >= frmWdth)
            filterRegion.right = frmWdth-1;
         if (filterRegion.bottom >= frmHght)
            filterRegion.bottom = frmHght-1;
      }
   } // using bezier
   else {

      filterRegion.left   = MAXFRAMEX;
      filterRegion.top    = MAXFRAMEY;
      filterRegion.right  = MINFRAMEX;
      filterRegion.bottom = MINFRAMEY;

      for (int i=0;;i++) {

         // include the point in the box
         if (bezier[i].xctrlpre < filterRegion.left)
            filterRegion.left = bezier[i].xctrlpre;
         if (bezier[i].yctrlpre < filterRegion.top)
            filterRegion.top  = bezier[i].yctrlpre;
         if (bezier[i].xctrlpre > filterRegion.right)
            filterRegion.right = bezier[i].xctrlpre;
         if (bezier[i].yctrlpre > filterRegion.bottom)
            filterRegion.bottom = bezier[i].yctrlpre;
         if (bezier[i].x < filterRegion.left)
            filterRegion.left = bezier[i].x;
         if (bezier[i].y < filterRegion.top)
            filterRegion.top  = bezier[i].y;
         if (bezier[i].x > filterRegion.right)
            filterRegion.right = bezier[i].x;
         if (bezier[i].y > filterRegion.bottom)
            filterRegion.bottom = bezier[i].y;
         if (bezier[i].xctrlnxt < filterRegion.left)
            filterRegion.left = bezier[i].xctrlnxt;
         if (bezier[i].yctrlnxt < filterRegion.top)
            filterRegion.top  = bezier[i].yctrlnxt;
         if (bezier[i].xctrlnxt > filterRegion.right)
            filterRegion.right = bezier[i].xctrlnxt;
         if (bezier[i].yctrlnxt > filterRegion.bottom)
            filterRegion.bottom = bezier[i].yctrlnxt;

         // if closed, you're done
         if ((i>1)&&
             (bezier[i].xctrlpre == bezier[0].xctrlpre)&&
             (bezier[i].yctrlpre == bezier[0].yctrlpre)&&
             (bezier[i].x        == bezier[0].x       )&&
             (bezier[i].y        == bezier[0].y       )&&
             (bezier[i].xctrlnxt == bezier[0].xctrlnxt)&&
             (bezier[i].yctrlnxt == bezier[0].yctrlnxt))
            break;
      }

      // clip filter region to frame
      if (filterRegion.left < 0) filterRegion.left = 0;
      if (filterRegion.top  < 0) filterRegion.top  = 0;
      if (filterRegion.right >= frmWdth)
         filterRegion.right = frmWdth-1;
      if (filterRegion.bottom >= frmHght)
         filterRegion.bottom = frmHght-1;
   }

   // if we're using a mask, finish it up
   if (useMask != nullptr) { // use mask

      // clear only the previous mask's active region (!)

      maskEngine->setActiveRegion(clrFilterRegion.left,
                                  clrFilterRegion.top,
                                  clrFilterRegion.right+1,
                                  clrFilterRegion.bottom+1);

      maskEngine->setDrawColor(0);
      maskEngine->fillActiveRegion();

      // set up the new active region
      maskEngine->setActiveRegion(filterRegion.left,
											 filterRegion.top,
                                  filterRegion.right+1,
                                  filterRegion.bottom+1);

      // ...and get set to clear it next time thru
      clrFilterRegion = filterRegion;

      // now set up the mask. If a lasso is supplied,
      // use it to initialize the mask. Otherwise,
      // just fill in the active region's rectangle

      maskEngine->setDrawColor(1);

      if (bezier != nullptr)     // set mask by filling bezier

         maskEngine->drawBezier(bezier);

      else if (lasso != nullptr) // set mask by filling lasso

         maskEngine->drawLasso(lasso);

      else                    // set mask by filling rect

         maskEngine->fillActiveRegion();


   } // end use mask

   // the mask is now initialized with the collective
   // footprint of the members of the pixel region list

   if (historyType == HISTORY_TYPE_MONOLITHIC)
   {
      // init the .RES buffer writeback flag to FALSE
      resWriteback = false;

      // set the pointers to last .RES record + 1
      return openHistoryFiles();
   }

   // History type is File Per Frame.
   auto retVal = openHistoryFileForFrameRestore(dstfrm);
   return (retVal == -1) ? HISTORY_ERROR_OPENING_HST_FILE : retVal;
}

int CSaveRestore::openForRestore(
                               int  dstfrm,                // dst frame
                               int  dstfld,                // dst field
                               CPixelRegionList& pxlrgnlst // ->pixel region list
                             )
{
   // the frame to restore (may be -1 for wildcard)
   filterFrame = dstfrm;

   // the field to restore (may be -1 for wiildcard)
   filterField = dstfld;

   // first determine the correct filter rectangle

   // assume that a mask will be used
   useMask = maskBuf;

   if (pxlrgnlst.GetEntryCount() == 0) { // no entries -> get nothing

         // no mask is needed now
         useMask = nullptr;

         // .. and filter region catches nothing
         filterRegion.left   = frmWdth;
         filterRegion.top    = frmHght;
         filterRegion.right  = -1;
         filterRegion.bottom = -1;
   }
   else {

         // set filter region from bounding rect
         filterRegion = pxlrgnlst.GetBoundingBox();

         // but first make sure you clip it to the frame
         if (filterRegion.left < 0) filterRegion.left = 0;
         if (filterRegion.top  < 0) filterRegion.top  = 0;
         if (filterRegion.right >= frmWdth)
            filterRegion.right = frmWdth - 1;
         if (filterRegion.bottom >= frmHght)
            filterRegion.bottom = frmHght - 1;
   }

   // if we're using a mask, finish it up
   if (useMask!=nullptr) { // use mask

      // clear only the previous mask's active region (!)
      maskEngine->setActiveRegion(clrFilterRegion.left,
                                  clrFilterRegion.top,
                                  clrFilterRegion.right+1,
                                  clrFilterRegion.bottom+1);

      maskEngine->setDrawColor(0);
      maskEngine->fillActiveRegion();

      // set up the new active region
      maskEngine->setActiveRegion(filterRegion.left,
                                  filterRegion.top,
                                  filterRegion.right+1,
                                  filterRegion.bottom+1);

      // ...and get set to clear it next time thru
      clrFilterRegion = filterRegion;

      // now set up the mask
      maskEngine->setDrawColor(1);

      // we sequence through each element of the pixel
      // region list and record its footprint in the mask

      for (int i=0; i<pxlrgnlst.GetEntryCount();i++) {

         CPixelRegion *rgn = pxlrgnlst.GetEntry(i);

         switch(rgn->GetType()) {

            case PIXEL_REGION_TYPE_POINT:
            {
               CPointPixelRegion *pntrgn;
               POINT pnt;
               pntrgn = static_cast<CPointPixelRegion*>(rgn);
               pnt = pntrgn->GetPoint();
               maskEngine->drawDot(pnt.x,pnt.y);
            }
            break;

            case PIXEL_REGION_TYPE_RECT:
            {
               CRectPixelRegion  *rctrgn;
               RECT  rct;
               rctrgn = static_cast<CRectPixelRegion*>(rgn);
               rct = rctrgn->GetRect();
               maskEngine->drawRectangle(rct.left,rct.top,rct.right,rct.bottom);
            }
            break;

            case PIXEL_REGION_TYPE_MASK:
            {
               CMaskPixelRegion *mskrgn;
               MTI_UINT16 *run;

               mskrgn = static_cast<CMaskPixelRegion*>(rgn);
               mskrgn->OpenRuns();
               while ((run=mskrgn->GetNextRun())!=nullptr) {

                  MTI_UINT16 x = run[0];
                  MTI_UINT16 y = run[1];
                  MTI_UINT16 xstp = x + run[2];

                  for (;x < xstp;++x) {
                     maskEngine->drawDot(x,y);
                  }
               }
            }
            break;

            case PIXEL_REGION_TYPE_LASSO:
            {
               CLassoPixelRegion *lasrgn;
               POINT *las;
               lasrgn = static_cast<CLassoPixelRegion*>(rgn);
               las = lasrgn->GetLasso();
               maskEngine->drawLasso(las);
            }
            break;

            case PIXEL_REGION_TYPE_BEZIER:
            {
               CBezierPixelRegion *bezrgn;
               BEZIER_POINT *bez;

               bezrgn = static_cast<CBezierPixelRegion*>(rgn);
               bez = bezrgn->GetBezier();
               maskEngine->drawBezier(bez);
            }
            break;
         }

      } // end loop thru list elements

   } // end use mask

   // the mask is now initialized with the collective
   // footprint of the members of the pixel region list

   if (historyType == HISTORY_TYPE_MONOLITHIC)
   {
      // init the .RES buffer writeback flag to FALSE
      resWriteback = false;

      // set the pointers to last .RES record + 1
      return openHistoryFiles();
   }

   // History type is File Per Frame.
   auto retVal = openHistoryFileForFrame(dstfrm);
   return (retVal == -1) ? HISTORY_ERROR_OPENING_HST_FILE : retVal;
}

////////////////////////////////////////////////////////////////////////////
//
//   R E T R E A T   T O   N E X T   R E S T O R E   R E C O R D
//
// Searches (toward the oldest saves) and stops at the next restore record
// which satisfies the given filter.
int CSaveRestore::nextRestoreRecord()
{
   return (historyType == HISTORY_TYPE_MONOLITHIC)
            ? nextRestoreRecord_MONO()
            : nextRestoreRecord_FPF();
}
//------------------------------------------------------------------------------

int CSaveRestore::nextRestoreRecord_MONO()
{
   MTIassert(resFileHandle != -1);
   if (resFileHandle == -1)
      return HISTORY_ERROR_INTERNAL;

   while(true) {

      if (resRecPtr == resBuf) { // time for next .HDR record

         // if any .RES record flags have been modified,
         // write back the whole buffer's worth
         if (resWriteback) {

            int result = lseek(resFileHandle, resBaseFileOffset, SEEK_SET);
            if (result == -1) {

               TRACE_0(errout << "ERROR: History RES file seek failed because "
                              << strerror(errno));
               MTIassert(result != -1);
               return HISTORY_ERROR_SEEKING_IN_RES_FILE;
            }
            MTIassert(result == resBaseFileOffset);

            result = write(resFileHandle, resBuf, resBlkLeng);
            if (result != resBlkLeng) {

               TRACE_0(errout << "ERROR: History RES file write failed because "
                              << strerror(errno));
               MTIassert(result == resBlkLeng);
               return HISTORY_ERROR_WRITING_RES_FILE;
            }
            MTIassert(result == resBlkLeng);

            resWriteback = false;

         }

         while(true) {

            if (hdrRecPtr == hdrBuf) { // time for next .HDR block

               hdrBaseFileOffset -= HEADER_BUFFER_SIZE;
               if (hdrBaseFileOffset < 0) // no more .HDR records
                  return(-1);
               int result = lseek(hdrFileHandle, hdrBaseFileOffset, SEEK_SET);
               if (result == -1) {

                  TRACE_0(errout << "ERROR: History HDR file seek failed because "
                                 << strerror(errno));
                  MTIassert(result != -1);
                  return HISTORY_ERROR_SEEKING_IN_HDR_FILE;
               }
               MTIassert(result == hdrBaseFileOffset);

               result = read(hdrFileHandle, hdrBuf, HEADER_BUFFER_SIZE);
               if (result == -1) {

                  TRACE_0(errout << "ERROR: History HDR file read failed because "
                                 << strerror(errno));
                  MTIassert(result != -1);
                  return HISTORY_ERROR_READING_HDR_FILE;
               }
           MTIassert(result == (int)HEADER_BUFFER_SIZE);

               hdrRecPtr = hdrBufStop;

            } // next .HDR block

            hdrRecPtr--;

            // now check the .HDR record against the (frame) filter

            if ( (filterFrame == ALL_FRAMES) ||
                ((filterFrame >= hdrRecPtr->minFrame)&&
                 (filterFrame <= hdrRecPtr->maxFrame)) ) { // passes

               // get the corresp block of .RES records
               int hdrIndex = hdrBaseFileOffset/sizeof(HEADER_RECORD) +
                              (hdrRecPtr - hdrBuf);

               resBaseFileOffset = hdrIndex * RESTORE_BUFFER_SIZE;

               // go there and read the block of .RES records
               resBlkLeng = RESTORE_BUFFER_SIZE;
               int result = lseek(resFileHandle, resBaseFileOffset, SEEK_SET);
               if (result == -1) {

                  TRACE_0(errout << "ERROR: History RES file seek failed because "
                                 << strerror(errno));
                  MTIassert(result != -1);
                  return HISTORY_ERROR_SEEKING_IN_RES_FILE;
               }
               MTIassert(result == resBaseFileOffset);

               result = read(resFileHandle, resBuf, resBlkLeng);
               if (result == -1) {

                  TRACE_0(errout << "ERROR: History RES file read failed because "
                                 << strerror(errno));
                  MTIassert(result != -1);
                  return HISTORY_ERROR_READING_RES_FILE;
               }
               MTIassert(result == resBlkLeng);

               // set the .RES record ptr to the stop
          resRecPtr = resBufStop;

               // init the .RES writeback flag
               resWriteback = false;

               goto fndHdr;

            } // frames test on .HDR records

         } // while(true)

fndHdr:; // found the next .HDR record and inited for it

      } // next .HDR record

      resRecPtr--;

      // now check the .RES record against the filter

      if ( (filterFrame == ALL_FRAMES) ||
           (filterFrame == resRecPtr->frameNum) ) {

         if ( (filterField == ALL_FIELDS) ||
              (resRecPtr->fieldIndex == ALL_FIELDS) ||
              (filterField == resRecPtr->fieldIndex) ) {

            if ((resRecPtr->extent.left   <= filterRegion.right )&&
                (resRecPtr->extent.top    <= filterRegion.bottom)&&
                (resRecPtr->extent.right  >= filterRegion.left  )&&
                (resRecPtr->extent.bottom >= filterRegion.top   )) {

               // KT 10/11/05 - filter out INVALID records
               if (((resRecPtr->restoreFlag&IS_INVALID) == 0)&&
                   (resRecPtr->pxlByteCount > 0)) {  // runs are saved
                  return(0);
               }

            } // rectangle bounds test

         } // fields test

      } // frames test

   } // while(true)

   return 0;
}
//------------------------------------------------------------------------------

int CSaveRestore::nextRestoreRecord_FPF()
{
   while (pxlChannel->nextRestoreRecord() == 0)
   {
      resRecPtr = &pxlChannel->getRestoreRecord()->resRec;
      if (resRecPtr == nullptr)
      {
         // All done -- no more records.
         return 0;
      }

      // TRACE_2(errout << "========= nextRestoreRecord BEFORE   =========" << endl);
      // TRACE_2(dumpRestoreRecord(resRecPtr));
      // TRACE_2(errout << "==============================================" << endl);

      // force this for all subsequent processing
      // irrespective of what's in the .HST file
      resRecPtr->frameNum = currentHistoryFileOpen;

      if ((filterFrame == ALL_FRAMES) || (filterFrame == resRecPtr->frameNum))
      {
         if ((filterField == ALL_FIELDS)
         || (resRecPtr->fieldIndex == ALL_FIELDS)
         || (filterField == resRecPtr->fieldIndex))
         {
            if ((resRecPtr->extent.left <= filterRegion.right)
            && (resRecPtr->extent.top <= filterRegion.bottom)
            && (resRecPtr->extent.right >= filterRegion.left)
            && (resRecPtr->extent.bottom >= filterRegion.top))
            {
               // KT 10/11/05 - filter out INVALID records
               if (((resRecPtr->restoreFlag & IS_INVALID) == 0) && (resRecPtr->pxlByteCount > 0))
               {
                  // runs are saved

                  // TRACE_2(errout << "========= nextRestoreRecord AFTER    =========" << endl);
                  // TRACE_2(dumpRestoreRecord(resRecPtr));
                  // TRACE_2(errout << "==============================================" << endl);

                  return 0;
               }
            }
         }
      }
   }

   // TRACE_2(errout << "========= nextRestoreRecord FAIL!    =========" << endl);
   // TRACE_2(dumpRestoreRecord(resRecPtr));
   // TRACE_2(errout << "==============================================" << endl);

   return (-1);
}
//------------------------------------------------------------------------------

// discard N saves from open history file
int CSaveRestore::discardNFixesFromHistoryFile(int frame, int numdscrd)
{
   // figures out the history file name
   if (openHistoryFileForFrame(frame) < 0)
      return -1;

   // discards fixes, closes, and truncates file
   if (pxlChannel->discardNFixes(numdscrd) < 0)
	  return -1;

   // file is not open now
   pxlFileHandle = -1;
   currentHistoryFileOpen = -1;

   return 0;
}
//------------------------------------------------------------------------------

bool CSaveRestore::differenceAt(MTI_UINT16 *iniFrame,
                                MTI_UINT16 *finFrame,
                                int x, int y)
{
   int pxloff = (frmWdth*y + x)*pxlComponentCount;

   MTI_UINT16 *iniPtr = iniFrame + pxloff;
   MTI_UINT16 *finPtr = finFrame + pxloff;

   int k;
   for (k = 0; k < pxlComponentCount; k++) {
       if (finPtr[k] != iniPtr[k])
          return true;
   }

   return false;
}
//------------------------------------------------------------------------------

int CSaveRestore::codeAt(MTI_UINT16 *iniFrame,
                         MTI_UINT16 *finFrame,
                         int x, int y)
{
   int retVal = 0;

   int pxloff = (frmWdth*(y-1) + (x-1))*pxlComponentCount;

   MTI_UINT16 *iniPtr;
   MTI_UINT16 *finPtr;

   if ((x > 0)&&(y > 0)) {

      iniPtr = iniFrame + pxloff;
      finPtr = finFrame + pxloff;

      for (int k = 0; k < pxlComponentCount; k++) {
         if (finPtr[k] != iniPtr[k])
            retVal |= 8;
      }
   }

   pxloff += pxlComponentCount;

   if ((x < frmWdth)&&(y > 0)) {

      iniPtr = iniFrame + pxloff;
      finPtr = finFrame + pxloff;

      for (int k = 0; k < pxlComponentCount; k++) {
         if (finPtr[k] != iniPtr[k])
            retVal |= 4;
      }
   }

   pxloff += (frmWdth-1)*pxlComponentCount;

   if ((x > 0)&&(y < frmHght)) {

      iniPtr = iniFrame + pxloff;
      finPtr = finFrame + pxloff;

      for (int k = 0; k < pxlComponentCount; k++) {
          if (finPtr[k] != iniPtr[k])
             retVal |= 2;
      }
   }

   pxloff += pxlComponentCount;

   if ((x < frmWdth)&&(y < frmHght)) {

      iniPtr = iniFrame + pxloff;
      finPtr = finFrame + pxloff;

      for (int k = 0; k < pxlComponentCount; k++) {
          if (finPtr[k] != iniPtr[k])
             retVal |= 1;
      }
   }

   return retVal;
}
//------------------------------------------------------------------------------

vector <vector <POINT> *> * CSaveRestore::limnDifferenceRuns(MTI_UINT16 *iniFrame,
                                                             MTI_UINT16 *finFrame)
{
   // clear the mask buffer
   MTImemset(maskBuf, 0, frmWdth*frmHght*2);

   vector <vector <POINT> *> *DifferenceIsland = new vector <vector <POINT> *>;

   for (int i = 0; i < frmHght; i++) {

      for (int j = 0; j < frmWdth; j++) {

         // IF pixel is set AND IF it's the top pixel of a loop
         // AND if it hasn't already been visited, THEN we have
         // a new blob

         if (differenceAt(iniFrame, finFrame, j, i)&&
             (codeAt(iniFrame, finFrame, j, i) == 1)&&  // top or bot of loop
             (maskBuf[i*frmWdth + j]==0)) {

            int lastMotion;

            vector <POINT> *newTrace = new vector <POINT>;

            vector <VERTICAL_EDGE *> *newBlob = new vector <VERTICAL_EDGE *>;

            // difference found at pixel(j, i)

            // this point is the minimum pt in lex order
            POINT minpt;
            minpt.x = j; minpt.y = i;

            // this point traces the blob boundary
            POINT trpt;
            trpt.x = j; trpt.y = i;

            // 1st point of trace
            newTrace->push_back(trpt);

            // first move is always to right
            // no need to update minpt here

            trpt.x = j + 1;

            // 2nd point of trace
            newTrace->push_back(trpt);

            // this records the last movement of the trace
            lastMotion = 0;

            while ((trpt.x != j)||(trpt.y != i)) { // while trace not closed

               // trace process here - develop a 4-bit code
               // for the local environment of the point -
               // Bit 3 = NW, Bit 2 = NE, Bit 1 = SW, Bit 0 = SE
               //
               int code = codeAt(iniFrame, finFrame,
                                 trpt.x, trpt.y);

               // if this point is "code 1", mark it "VISITED ALREADY"
               // so that we don't initiate a new blob when we encounter
               // it later on
               //
               if (code == 1) {

                  maskBuf[trpt.y*frmWdth + trpt.x] = -1;
               }

               // based on the local environment of the point
               // move the tracer one unit at a time so that
               // the inside of the blob is to the RIGHT when
               // facing in the direction of the motion
               //
               switch(code) {

                  case 0:

                     // can't happen - not touching a blob

                  break;

                  case 1:  // motion is 0 (rt)
                  case 3:
                  case 11:

                     trpt.x++;

                     if (lastMotion == 0) {

                        newTrace->back() = trpt;
                     }
                     else {

                        newTrace->push_back(trpt);

                        lastMotion = 0;
                     }

                  break;

                  case 2:  // motion is 1 (dn)
                  case 10:
                  case 14:

                     trpt.y++;

                     if (lastMotion == 1) { // continuing

                        newTrace->back() = trpt;
                     }
                     else {

                        newTrace->push_back(trpt);

                        lastMotion = 1;
                     }

                  break;

                  case 4: // motion is 3 (up)
                  case 5:
                  case 7:

                     trpt.y--;

                     if (lastMotion == 3) { // continuing

                        newTrace->back() = trpt;
                     }
                     else {

                        newTrace->push_back(trpt);

                        lastMotion = 3;
                     }

                  break;

                  case 6:

                     if (lastMotion == 0) {

                        trpt.y--;

                        newTrace->push_back(trpt);

                        lastMotion = 3;
                     }
                     else if (lastMotion == 2) {

                        trpt.y++;

                        newTrace->push_back(trpt);

                        lastMotion = 1;
                     }

                  break;

                  case 8:  // motion is 2 (left)
                  case 12:
                  case 13:

                      trpt.x--;

                      if (lastMotion == 2) {

                         newTrace->back() = trpt;
                      }
                      else {

                         newTrace->push_back(trpt);

                         lastMotion = 2;
                      }

                  break;

                  case 9:

                     if (lastMotion == 1) {

                        trpt.x++;

                        newTrace->push_back(trpt);

                        lastMotion = 0;
                     }
                     else if (lastMotion == 3) {

                        trpt.x--;

                        newTrace->push_back(trpt);

                        lastMotion = 2;
                     }

                  break;

                  case 15:

                     // can't happen - in the interior of a blob

                  break;

               }

               // new trace pt -- see if min needs to be updated
               if ((trpt.y < minpt.y) ||
                   ((trpt.y == minpt.y)&&(trpt.x < minpt.x))) {  // lex compare

                  minpt.x = trpt.x;
                  minpt.y = trpt.y;
               }
            }

            // if the final pt is the lexicographic minimum
            // then the cycle is outside boundary, not hole
            //
            if ((trpt.x == minpt.x)&&(trpt.y == minpt.y)) { // bndry - not hole

               // sort the edges lexicographically (y, then x) on lower pt
               //sort( newBlob->begin(), newBlob->end(), CSaveRestore::compareEdges );

               // push ptr to the polygon vector
               DifferenceIsland->push_back(newTrace);
            }
            else {

               // discard this cycle
               delete newBlob;
            }
         }
      }
   }

   return DifferenceIsland;
}

//////////////////////////////////////////////////////////////////////
//
//  M E T H O D S   T O   A C C E S S   C U R   R E S T O R E   R E C
//
void CSaveRestore::setRestoreWritebackFlag(bool flag)
{
   if (historyType == HISTORY_TYPE_MONOLITHIC) {

      resWriteback = flag;
   }
   else {

      pxlChannel->setRestoreWritebackFlag(flag);
   }
}

RESTORE_RECORD *CSaveRestore::getCurrentRestoreRecord()
{
   if (historyType == HISTORY_TYPE_MONOLITHIC) {

      return resRecPtr;
   }
   else {

      RESTORE_RECORD *restoreRecPtr = &pxlChannel->getRestoreRecord()->resRec;

//      TRACE_2(errout << "========= getCurrentRestoreRecord    =========" << endl);
//      TRACE_2(dumpRestoreRecord(restoreRecPtr));
//      TRACE_2(errout << "==============================================" << endl);

      return restoreRecPtr;
   }
}

int CSaveRestore::getCurrentRestoreRecordFrame()
{
   RESTORE_RECORD *restoreRecPtr = getCurrentRestoreRecord();

   return(restoreRecPtr->frameNum);
}

int CSaveRestore::getCurrentRestoreRecordFieldIndex()
{
   RESTORE_RECORD *restoreRecPtr = getCurrentRestoreRecord();

   return(restoreRecPtr->fieldIndex);
}

int CSaveRestore::getCurrentRestoreRecordFieldNumber()
{
   RESTORE_RECORD *restoreRecPtr = getCurrentRestoreRecord();

   return(restoreRecPtr->fieldNumber);
}

int CSaveRestore::getCurrentRestoreRecordGlobalSaveNumber()
{
   RESTORE_RECORD *restoreRecPtr = getCurrentRestoreRecord();

   return(restoreRecPtr->globalSaveNumber);
}

time_t CSaveRestore::getCurrentRestoreRecordSaveTime()
{
   RESTORE_RECORD *restoreRecPtr = getCurrentRestoreRecord();

   return(restoreRecPtr->saveTime);
}

int CSaveRestore::getCurrentRestoreRecordToolCode()
{
   RESTORE_RECORD *restoreRecPtr = getCurrentRestoreRecord();

   return(restoreRecPtr->toolCode);
}

int CSaveRestore::getCurrentRestoreRecordRegionType()
{
   return(0);
}

bool CSaveRestore::getCurrentRestoreRecordResFlag()
{
   RESTORE_RECORD *restoreRecPtr = getCurrentRestoreRecord();

   return (restoreRecPtr->restoreFlag&IS_RESTORED);
}

void CSaveRestore::setCurrentRestoreRecordResFlag(bool val)
{
   RESTORE_RECORD *restoreRecPtr = getCurrentRestoreRecord();

   if (val) {
      restoreRecPtr->restoreFlag |= IS_RESTORED;
   }
   else {
      restoreRecPtr->restoreFlag &= ~IS_RESTORED;
   }

   setRestoreWritebackFlag(true);

   return;
}

bool CSaveRestore::getCurrentRestoreRecordInvalidFlag()
{
   RESTORE_RECORD *restoreRecPtr = getCurrentRestoreRecord();

   return (restoreRecPtr->restoreFlag & IS_INVALID);
}

void CSaveRestore::setCurrentRestoreRecordInvalidFlag(bool val)
{
   RESTORE_RECORD *restoreRecPtr = getCurrentRestoreRecord();

   if (val) {
      restoreRecPtr->restoreFlag |= IS_INVALID;
   }
   else {
      restoreRecPtr->restoreFlag &= ~IS_INVALID;
   }

   setRestoreWritebackFlag(true);

   return;
}

RECT* CSaveRestore::getCurrentRestoreRecordBoundingRectangle()
{
   RESTORE_RECORD *restoreRecPtr = getCurrentRestoreRecord();

   return (&(restoreRecPtr->extent));
}

char * CSaveRestore::getCurrentRestoreRecordPDLCmd()
{
   return(nullptr);
}

// Access the pxlChannel and obtain the data for the current restore
// record, turning it into a pixel region list with unpopulated runs,
// used for highlighting and/or as a template for copying pixels.
//
CPixelRegionList * CSaveRestore::getCurrentRestoreRecordPixelRegionList()
{
   POINT inpt;
   RECT  inrct;
   RECT  inmsk;
   MTI_INT32 temp[MAX_LASSO_PTS*2]; // MAX_LASSO_PTS >= 3*MAX_BEZIER_PTS

   int vtxcnt;
   MTI_INT32 *dstptr;

   // open an empty pixel region list
   CPixelRegionList *newlst = new CPixelRegionList;

   RESTORE_RECORD *restoreRecPtr = getCurrentRestoreRecord();

   if (pxlChannel->openPixelChannelForRestore(restoreRecPtr->pxlFileByteOffset,
                                              restoreRecPtr->pxlByteCount)==-1)
      return nullptr;

   // de-serialize the packed data in the pxlBuf
   // back into a RAM pixel region list, one
   // pixel region at a time

   MTI_UINT16 *rgnptr;

   try {

      while ((rgnptr = pxlChannel->getNextPixelRegion())!=nullptr) {

         switch(*rgnptr++) {

            case PIXEL_REGION_TYPE_POINT:

               // transform 16-bit point coords to 32-bits and
               // add the point to the growing pixel region list
               inpt.x = *(MTI_INT16 *)rgnptr++;
               inpt.y = *(MTI_INT16 *)rgnptr++;

               MTIassert(inpt.x < frmWdth);
               MTIassert(inpt.y < frmHght);

               newlst->Add(inpt,nullptr, frmWdth, frmHght, pxlComponentCount);

            break;

            case PIXEL_REGION_TYPE_RECT:

               // transform 16-bit rectangle coords to 32-bits and
               // add the rectangle to the growing pixel region list
               inrct.left   = *(MTI_INT16 *)rgnptr++;
               inrct.top    = *(MTI_INT16 *)rgnptr++;
               inrct.right  = *(MTI_INT16 *)rgnptr++;
               inrct.bottom = *(MTI_INT16 *)rgnptr++;

               newlst->Add(inrct,nullptr, frmWdth, frmHght, pxlComponentCount);

            break;

            case PIXEL_REGION_TYPE_MASK:

               // transform 16-bit rectangle coords to 32-bits and
               // add the rectangle to the growing pixel region list
               inmsk.left   = *(MTI_INT16 *)rgnptr++;
               inmsk.top    = *(MTI_INT16 *)rgnptr++;
               inmsk.right  = *(MTI_INT16 *)rgnptr++;
               inmsk.bottom = *(MTI_INT16 *)rgnptr++;

               newlst->Add(inmsk,nullptr, frmWdth, frmHght, pxlComponentCount);

            break;

            case PIXEL_REGION_TYPE_LASSO:

               // skip the bounding box
               rgnptr += 4;

               // get vertex count
               vtxcnt = *rgnptr++;

               // transform 16-bit lasso coords to 32-bits and
               // add the lasso to the growing pixel region list
               dstptr = temp;
               for (int i=0;i<vtxcnt;i++) {
                  *dstptr++ = *(MTI_INT16 *)rgnptr++; // x
                  *dstptr++ = *(MTI_INT16 *)rgnptr++; // y
               }

               newlst->Add((POINT *)temp,nullptr, frmWdth, frmHght, pxlComponentCount);

            break;

            case PIXEL_REGION_TYPE_BEZIER:

               // skip the bounding box
               rgnptr += 4;

               // get vertex count
               vtxcnt = *rgnptr++;

               // transform 16-bit bezier coords to 32-bits and
               // add the bezier to the growing pixel region list
               dstptr = temp;
               for (int i=0;i<vtxcnt;i++) {
                  *dstptr++ = *(MTI_INT16 *)rgnptr++; // xctrlpre
                  *dstptr++ = *(MTI_INT16 *)rgnptr++; // yctrlpre
                  *dstptr++ = *(MTI_INT16 *)rgnptr++; // x
                  *dstptr++ = *(MTI_INT16 *)rgnptr++; // y
                  *dstptr++ = *(MTI_INT16 *)rgnptr++; // xctrlnxt
                  *dstptr++ = *(MTI_INT16 *)rgnptr++; // yctrlnxt
               }

               newlst->Add((BEZIER_POINT *)temp,nullptr, frmWdth, frmHght, pxlComponentCount);

            break;

            default:
               MTIassert(false);
               TRACE_0(errout << "ERROR: Clip's history file is corrupt!");
               return nullptr; // AVOID FRICKIN' INFINITE LOOP ON CORRUPT HISTORY

         }
      }
   }
   catch (std::bad_alloc)
   {
      TRACE_0(errout << "ERROR: OUT OF MEMORY while reading history file");
   }

   // return ptr to completed pixel region list
   return newlst;
}

// GOV
int CSaveRestore::reviseAllGOVRegions()
{
   CAutoThreadLocker lock(*getLock());

   int retVal;

   for (unsigned int i = 0; i < GOVRestoreList.size(); ++i) {

      retVal = reviseOneGOVRegion(GOVRestoreList[i]);
      // Ignore the error!
      // if (retVal != 0)
         //break;
   }

   // now clear the GOV restore list out
   GOVRestoreList.clear();

   return retVal;
}

// GOV
int CSaveRestore::reviseGOVRegions(int frameNumber)
{
   CAutoThreadLocker lock(*getLock());

   int retVal;
   vector<GOVEntry>::iterator iter;

   for (iter = GOVRestoreList.begin(); iter != GOVRestoreList.end(); ) {

      if ((*iter).resFrameNum == frameNumber) {

         retVal = reviseOneGOVRegion(*iter);
         iter = GOVRestoreList.erase(iter);

         if (retVal != 0)
            return retVal;
      }
      else {

         ++iter;
      }
   }

   return(0);
}

// GOV
int CSaveRestore::reviseOneGOVRegion(GOVEntry &govEntry)
{
   // INTERNAL CALL - Lock the global Atomic Lock before calling!

   int retVal;

   {
      CAutoHistoryOpener opener(*this, FLUSH_TYPE_RESTORE,
                                govEntry.resFrameNum, ALL_FIELDS,
                                0, 0, 0, retVal);
      if (retVal != 0)
         return retVal;

      while (nextRestoreRecord()==0) {

         RESTORE_RECORD *restoreRecPtr = getCurrentRestoreRecord();

         if (restoreRecPtr->globalSaveNumber == govEntry.resGlobalSaveNumber) {

            if (govEntry.tmpFileLength==0) { // no residue/workfile

               // disable the entire .RES record
               // setCurrentRestoreRecordResFlag(true);
               setCurrentRestoreRecordInvalidFlag(true);
            }
            else { // there's a GOV residue and a workfile containing it

               // we'll take the contents of the workfile
               // & copy them into the history's .hst file

               int tmpFileHandleSrc;

               // now open the workfile and save the file handle
               if ((tmpFileHandleSrc = open(govEntry.tmpFileName.c_str(),
                               O_BINARY|O_CREAT|O_RDONLY, S_IRUSR|S_IWUSR)) == -1) {

                  TRACE_0(errout << "E3wsRROR: CSaveRestore::reviseOneGOVRegion: "
                                 << " can't open the work file " << govEntry.tmpFileName
                                 << " because: " << endl
                                 << strerror(errno));
                  return HISTORY_ERROR_RESTORING_PIXELS;
               }

               // .RES record reflects reduced residue...
               restoreRecPtr->pxlByteCount = govEntry.tmpFileLength;

               // set flag to write it back
               setRestoreWritebackFlag(true);

//   TRACE_2(errout << "=========TTT GOV residue RES RECORD=========" << endl);
//   TRACE_2(dumpRestoreRecord(restoreRecPtr));
//   TRACE_2(errout << "====================================" << endl);

               int result;

               // get absolute file addr in .HST file
               MTI_INT64 dest = pxlChannel->pxlSectionBeg +
                                 restoreRecPtr->pxlFileByteOffset;

               // write the GOV residue into the original place in .HST file
               MTI_INT64 result64 = lseek64(pxlFileHandle, dest, SEEK_SET);
               if (result64 == -1) {

                  TRACE_0(errout << "ERROR: History PXL file seek failed because "
                                 << strerror(errno));
                  MTIassert(result64 != -1);
                  return HISTORY_ERROR_SEEKING_IN_HST_FILE;
               }
               MTIassert(result64 == dest);

               int bytsrem = govEntry.tmpFileLength;
               while (bytsrem > govBufByts) {

                  result = read(tmpFileHandleSrc, govBuf, govBufByts);
                  if (result == -1) {

                     TRACE_0(errout << "ERROR: History TMP PXL file read failed because "
                                    << strerror(errno));
                     MTIassert(result != -1);
                     return HISTORY_ERROR_READING_HST_FILE;
                  }
                  MTIassert(result == govBufByts);

                  result = write(pxlFileHandle, govBuf, govBufByts);
                  if (result != govBufByts) {

                     TRACE_0(errout << "ERROR: History PXL file write failed because "
                                    << strerror(errno));
                     MTIassert(result != -1);
                     return HISTORY_ERROR_WRITING_HST_FILE;
                  }
                  MTIassert(result == govBufByts);

                  bytsrem -= govBufByts;
               }

               result = read(tmpFileHandleSrc, govBuf, bytsrem);
               if (result == -1) {

                  TRACE_0(errout << "ERROR: History TMP PXL file read failed because "
                                 << strerror(errno));
                  MTIassert(result != -1);
                  return HISTORY_ERROR_READING_HST_FILE;
               }
               MTIassert(result == bytsrem);

               result = write(pxlFileHandle, govBuf, bytsrem);
               if (result != bytsrem) {

                  TRACE_0(errout << "ERROR: History PXL file write failed because "
                                 << strerror(errno));
                  MTIassert(result == bytsrem);
                  return HISTORY_ERROR_WRITING_HST_FILE;
               }
               MTIassert(result == bytsrem);

               // close the workfile and delete it
               close(tmpFileHandleSrc);
               DeleteFile(govEntry.tmpFileName.c_str());
            }
         }
      }

      // everything closed when AutoHistoryOpener goes out of scope
   }

   return 0;
}

// GOV
int CSaveRestore::discardGOVRevisions()
{
   CAutoThreadLocker lock(*getLock());

   // delete the workfiles in the GOV restore list
   for (unsigned int i = 0; i < GOVRestoreList.size(); ++i) {
      if (GOVRestoreList[i].tmpFileLength > 0)
         DeleteFile(GOVRestoreList[i].tmpFileName.c_str());
   }

   GOVRestoreList.clear();

   return(0);
}

// Outputs the residue of a run, relative to the useMask,
// as one or more runs, into the GOV workfile. Returns the
// total number of words written out.
// GOV
int CSaveRestore::govOutputRunResidue(MTI_UINT16 *runptr)
{
  int wdsout = 0;

  MTIassert(frmWdth < 10000);
  MTIassert(frmHght < 7500);
  MTIassert(runptr[0] < frmWdth);
  MTIassert(runptr[1] < frmHght);
  MTIassert(runptr[2] <= (frmWdth - runptr[0]));

  int maxwds = 3 * (runptr[2]+1);

  if (govBufPtr > (govBufStop - maxwds)) { // realign
     int writeSize = (govBufPtr - govBuf)*2;
     int result = write(govFileHandle, govBuf, writeSize);
     if (result != writeSize) {

        TRACE_0(errout << "ERROR: History GOV TMP file write failed because "
                       << ((result == -1)? strerror(errno) : " disk is full"));
        MTIassert(result == writeSize);
        return HISTORY_ERROR_WRITING_HST_FILE;
     }
     govBaseOffset += (govBufPtr - govBuf)*2;
     govBufPtr = govBuf;
  }

  int pxlsrem;

  if (useMask == nullptr) { // just output the entire run

     *govBufPtr++ = *runptr++;
     *govBufPtr++ = *runptr++;
     pxlsrem = *runptr++;
     *govBufPtr++ = pxlsrem;
     for (int i=0;i<pxlsrem*pxlComponentCount;i++)
        *govBufPtr++ = *runptr++;
  }
  else {                 // the run must be trimmed using the mask

     MTI_UINT16 *mskptr = useMask + runptr[1]*frmWdth + runptr[0];

     int x = *runptr++;
     int y = *runptr++;
     pxlsrem = *runptr++;
     MTI_UINT16 *lengthPtr;

     while (pxlsrem > 0) {

        if (*mskptr == 0) {                     // starting a new run

           *govBufPtr++ = x;
           *govBufPtr++ = y;
           lengthPtr    = govBufPtr;
           *govBufPtr++ = 0;
           wdsout += 3;

           while ((*mskptr == 0)&&(pxlsrem > 0)) { // extending the run
              mskptr++;
              x++;
              for (int i=0;i<pxlComponentCount;i++)
                 *govBufPtr++ = *runptr++;
              wdsout += pxlComponentCount;
              *lengthPtr += 1;
              pxlsrem--;
           }
        }

        while ((*mskptr != 0)&&(pxlsrem > 0)) { // looking for new run
           mskptr++;
           x++;
           runptr += pxlComponentCount;
           pxlsrem--;
        }
     }
  }

  return(wdsout);
}

// Outputs the header and runs, relative to the useMask,
// into the GOV workfile. Returns the total number of
// words written out.
// GOV
int CSaveRestore::govOutputHeaderAndRuns(MTI_UINT16 *rgnptr, int hdrlng)
{
   int wdsout = 0;

   if (govBufPtr > (govBufStop - hdrlng)) { // realign
      int writeSize = (govBufPtr - govBuf)*2;
      int result = write(govFileHandle, govBuf, writeSize);
      if (result != writeSize) {

         TRACE_0(errout << "ERROR: History GOV TMP file write failed because "
                        << ((result == -1)? strerror(errno) : " disk is full"));
         MTIassert(result == writeSize);
         return HISTORY_ERROR_WRITING_HST_FILE;
      }
      govBaseOffset += (govBufPtr - govBuf)*2;
      govBufPtr = govBuf;
   }

   // output the header, incl runlength placeholder
   for (int i=0;i<hdrlng;i++)
      *govBufPtr++ = rgnptr[i];

   // offset of runlength placeholder in workfile
   govRunLengthOffset = govBaseOffset + ((govBufPtr-2) - govBuf)*2;

   wdsout += hdrlng;

   // get the runs and output them
   // relative to the useMask (!)
   int runwds = 0;
   MTI_UINT16 *runptr;
   while ((runptr = pxlChannel->getNextRun())!=nullptr) {
      runwds += govOutputRunResidue(runptr);
   }

   wdsout += runwds;

   if (runwds == 0) {

      // no runs have been output: remove the region header

      govBufPtr -= hdrlng;
      wdsout -= hdrlng;
   }
   else {

      // runs have been output: drop the runlength into place

      int wdoffs = govRunLengthOffset - govBaseOffset;
      if ((wdoffs >= 0)&&(wdoffs <= (govBufByts - 4))) {

         // the two runlength wds are in the buffer

         wdoffs >>= 1;
         *(govBuf + wdoffs)   = runwds&0xffff;
         *(govBuf + wdoffs+1) = runwds>>16;
      }
      else {

         // the two runlength wds are in the workfile

         MTI_UINT32 lohi[1];
         lohi[0] = runwds;
         MTI_INT64 result64 = lseek64(govFileHandle, govRunLengthOffset, SEEK_SET);
         if (result64 == -1) {

            TRACE_0(errout << "ERROR: History PXL file seek failed because "
                           << strerror(errno));
            MTIassert(result64 != -1);
            return(-1);
         }
         MTIassert(result64 == govRunLengthOffset);

         int result = write(govFileHandle, lohi, 4);
         if (result != 4) {

            TRACE_0(errout << "ERROR: History GOV TMP file write failed because "
                           << ((result == -1)? strerror(errno) : " disk is full"));
            MTIassert(result == 4);
            return HISTORY_ERROR_WRITING_HST_FILE;
         }
         lseek64(govFileHandle, 0, SEEK_END);
      }
   }

   return(wdsout);
}

void CSaveRestore::copyRunToFrameBuffer(const MTI_UINT16 *run,
                                        MTI_UINT16 *dstFrame,
                                        int frameWidth, int frameHeight,
                                        int pixelComponentCount,
                                        const MTI_UINT16 *mask)
{
   MTI_UINT16 runX   = run[0];
   MTI_UINT16 runY   = run[1];
   MTI_UINT16 runLen = run[2];

   // set runptr -> pixel values for run
   const MTI_UINT16 *runptr = run + 3;

   MTI_UINT16 *frmptr = dstFrame +
                        (runY * frameWidth + runX) * pixelComponentCount;

   if (mask==nullptr) { // just copy the data

      for (int i=0;i<runLen;i++) { // for each pixel in run do

         for (int j=0;j<pixelComponentCount;j++)
            *frmptr++ = *runptr++;

      } // for each pixel
   }
   else { // copy only where the mask is non-zero

      MTI_UINT16 *mskptr = (MTI_UINT16 *)(mask + runY * frameWidth + runX);

      for (int i=0;i<runLen;i++) { // for each pixel in run do

         if (*mskptr != 0) { // copy pixel values from run

            for (int j=0;j<pixelComponentCount;j++)
               *frmptr++ = *runptr++;
         }
         else { // skip over pixel values
               frmptr += pixelComponentCount;
               runptr += pixelComponentCount;
         }

         mskptr++;

      } // for each pixel
   }
}

///////////////////////////////////////////////////////////////////////
//
//   R E S T O R E   P I X E L   D A T A   F M   C U R   R E C
//
// Restores the pixel data for the current restore record into the
// frame whose image fields are supplied as argument. Only one
// field is typically touched
int CSaveRestore::restoreCurrentRestoreRecordPixels(
   MTI_UINT16 *dstint,
   bool mark)
{
   CAutoThreadLocker lock(*getLock());

   RESTORE_RECORD *restoreRecPtr = getCurrentRestoreRecord();

   if (restoreRecPtr == nullptr)
      return(-1);

   if (mark) {

      // capture the .PXL data NOT restored by the
      // GOV and save it in a temporary workfile

      // make a new entry in the GOV restore list
      GOVEntry *govEntry = new GOVEntry;

      // save the .RES record's frame and field numbers
      govEntry->resFrameNum   = restoreRecPtr->frameNum;
      govEntry->resFieldIndex = restoreRecPtr->fieldIndex;

      // save the .RES record's global save number
      govEntry->resGlobalSaveNumber = restoreRecPtr->globalSaveNumber;

      // use the global save number to make a workfile name
      // then save the workfile name in the new GOV entry
      MTIostringstream tmpstrm;

      tmpstrm << historyPathName << "tmp_" <<
                 govEntry->resFrameNum << "_" <<
                 govEntry->resGlobalSaveNumber << ".pix";

      govEntry->tmpFileName = tmpstrm.str();

      // now open the file and save the filehandle
      if ((govFileHandle = open(govEntry->tmpFileName.c_str(),
                            O_BINARY|O_CREAT|O_RDWR, S_IRUSR|S_IWUSR)) == -1){

         TRACE_0(errout << "ERROR: CSaveRestore::restoreCurrentRestoreRecordPixels: "
                        << " can't open the work file " << govEntry->tmpFileName
                        << " because: " << endl << strerror(errno));
         return HISTORY_ERROR_RESTORING_PIXELS;
      }

      // set buffer ptr at top and
      // file offset of buffer beg
      govBufPtr = govBuf;
      govBaseOffset = 0;

      MTI_UINT16 *rgnptr;
      bool retainPt;
      MTI_INT32   hdrlng;
      MTI_UINT16 *mskptr;
      MTI_UINT32  wdsout = 0;

//   TRACE_2(errout << "=========TTT restoreCurrentRestoreRecordPixels RES RECORD=========" << endl);
//   TRACE_2(dumpRestoreRecord(restoreRecPtr));
//   TRACE_2(errout << "====================================" << endl);

      // opens the current record's data for restore
      if (pxlChannel->openPixelChannelForRestore(restoreRecPtr->pxlFileByteOffset,
                                                 restoreRecPtr->pxlByteCount) == -1){

         TRACE_0(errout << "ERROR: CSaveRestore::restoreCurrentRestoreRecordPixels: "
                        << " can't read the work file " << govEntry->tmpFileName
                        << " because: " << endl << strerror(errno));
         return HISTORY_ERROR_RESTORING_PIXELS;
      }

      // copy the residual .PXL data for
      // this restore record to workfile
      // the contents of the workfile are
      // a valid pixel region list, serialized
      while ((rgnptr = pxlChannel->getNextPixelRegion())!=nullptr) {

         switch(*rgnptr) {

            case PIXEL_REGION_TYPE_POINT:

               // check the use mask to determine whether the point
               // should be retained in the .PXL residue workfile
               retainPt = (useMask == nullptr);
               if (!retainPt)
                  mskptr = useMask + rgnptr[2]*frmWdth + rgnptr[1];
                  retainPt = (*mskptr == 0);
               if (retainPt) {
                  if (govBufPtr > (govBufStop - 7)) { // realign
                     write(govFileHandle, govBuf, (govBufPtr - govBuf)*2);
                     govBaseOffset += (govBufPtr - govBuf)*2;
                     govBufPtr = govBuf;
                  }
                  govBufPtr[0] = rgnptr[0];
                  govBufPtr[1] = rgnptr[1];
                  govBufPtr[2] = rgnptr[2];
                  govBufPtr[3] = rgnptr[3];
                  govBufPtr += 4;
                  for (int i=0;i<pxlComponentCount;i++)
                     *govBufPtr++ = rgnptr[4+i];

                  wdsout += 4 + pxlComponentCount;
               }

            break;

            case PIXEL_REGION_TYPE_RECT:
            case PIXEL_REGION_TYPE_MASK:

               hdrlng = 7;

               wdsout += govOutputHeaderAndRuns(rgnptr, hdrlng);

            break;

            case PIXEL_REGION_TYPE_LASSO:

               hdrlng = (8 + rgnptr[5]*2);

               wdsout += govOutputHeaderAndRuns(rgnptr, hdrlng);

            break;

            case PIXEL_REGION_TYPE_BEZIER:

               hdrlng = (8 + rgnptr[5]*6);

               wdsout += govOutputHeaderAndRuns(rgnptr, hdrlng);

            break;

            default:
               TRACE_0(errout << "ERROR: CSaveRestore:: restoreCurrentRestoreRecordPixels: "
                              << "Clip's history file is corrupt!");
               MTIassert(false);
               // Avoid frickin' infinite loop
               return HISTORY_ERROR_RESTORING_PIXELS;

         }
      }

      // close the GOV temporary workfile
      write(govFileHandle, govBuf, (govBufPtr - govBuf)*2);
      close(govFileHandle);
      govFileHandle = -1;

      // record the length of the residue in the entry
      govEntry->tmpFileLength = wdsout*2;

      // if the residue was > original .pxl complement
      //   delete the file
      //   delete the entry
      // else if the residue was <= original .pxl complement
      //   if the residue was 0
      //      delete the file
      //   link the entry onto the list

      if (govEntry->tmpFileLength > restoreRecPtr->pxlByteCount) {
         DeleteFile(govEntry->tmpFileName.c_str());
         delete govEntry;
      }
      else {
         if (govEntry->tmpFileLength == 0) {
            DeleteFile(govEntry->tmpFileName.c_str());
            govEntry->tmpFileName = "";
         }
         GOVRestoreList.push_back(*govEntry);
      }
   }

//   TRACE_2(errout << "=========TTT restoreCurrentRestoreRecordPixels 3 RES RECORD=========" << endl);
//   TRACE_2(dumpRestoreRecord(resRecPtr));
//   TRACE_2(errout << "====================================" << endl);

   // opens the current record's data for restore
   if (pxlChannel->openPixelChannelForRestore(restoreRecPtr->pxlFileByteOffset,
                                              restoreRecPtr->pxlByteCount) == -1){

      TRACE_0(errout << "ERROR: CSaveRestore::restoreCurrentRestoreRecordPixels: "
                     << " can't read the .PIX file because: " << endl
                     << strerror(errno));
      return HISTORY_ERROR_RESTORING_PIXELS;
   }

   // implementation of the pixel channel means we no longer
   // have to specialize the restore code for region type

   MTI_UINT16 *rgnptr;
   MTI_UINT16 *runptr;

   while ((rgnptr = pxlChannel->getNextPixelRegion())!=nullptr) {

      switch(*rgnptr) {

         case PIXEL_REGION_TYPE_POINT:

            // pt coords must be within filter region
            if ((rgnptr[1] <= filterRegion.right )&&
                (rgnptr[2] <= filterRegion.bottom)&&
                (rgnptr[1] >= filterRegion.left  )&&
                (rgnptr[2] >= filterRegion.top   )) {

               while ((runptr = pxlChannel->getNextRun())!=nullptr) {

                  copyRunToFrameBuffer(runptr,
                                       dstint,
                                       frmWdth,
                                       frmHght,
                                       pxlComponentCount,
                                       useMask);
               }
            }

         break;

         case PIXEL_REGION_TYPE_RECT:
         case PIXEL_REGION_TYPE_MASK:
         case PIXEL_REGION_TYPE_LASSO:
         case PIXEL_REGION_TYPE_BEZIER:

            // all other region types have proper extent boxes
            if ((rgnptr[1] <= filterRegion.right )&&
                (rgnptr[2] <= filterRegion.bottom)&&
                (rgnptr[3] >= filterRegion.left  )&&
                (rgnptr[4] >= filterRegion.top   )) {

               while ((runptr = pxlChannel->getNextRun())!=nullptr) {

                  copyRunToFrameBuffer(runptr,
                                       dstint,
                                       frmWdth,
                                       frmHght,
                                       pxlComponentCount,
                                       useMask);
               }
            }

         break;

         default:
            // Avoid frickin' infinite loop
            TRACE_0(errout << "ERROR: Clip's history file is corrupt!");
            MTIassert(false);
            return HISTORY_ERROR_RESTORING_PIXELS;
      }
   }

   return 0; //success
}

void CSaveRestore::copyRunToRectangleBufferSmall(const MTI_UINT16 *run,
                                                 MTI_UINT16 *dstBuffer,
                                                 RECT *rect,
                                                 int pixelComponentCount)
{
   if ((run[1] < rect->top)||(run[1] > rect->bottom)) return;

   MTI_UINT16 runX   = run[0];
   MTI_UINT16 runY   = run[1];
   MTI_UINT16 runLen = run[2];

   // set runptr -> pixel values for run
   const MTI_UINT16 *runptr = run + 3;

   MTI_UINT16 *bufptr = dstBuffer + ((runY - rect->top)*(rect->right - rect->left + 1)
                                    +(runX - rect->left))*pixelComponentCount;

   for (int i=0;i<runLen;i++) { // for each pixel in run do

      if ((runX >= rect->left)&&(runX <= rect->right)) { // copy pixel values

         for (int j=0;j<pixelComponentCount;j++)
            *bufptr++ = *runptr++;
      }
      else { // skip over pixel values
            bufptr += pixelComponentCount;
            runptr += pixelComponentCount;
      }

      runX++;

   } // for each pixel

}

int CSaveRestore::obtainCurrentRestoreRecordPixelsSmall(MTI_UINT16 *dstbuf)
{
   CAutoThreadLocker lock(*getLock());

   RESTORE_RECORD *restoreRecPtr = getCurrentRestoreRecord();

   if (restoreRecPtr == nullptr)
      return(-1);

   // opens the current record's data for restore
   if (pxlChannel->openPixelChannelForRestore(restoreRecPtr->pxlFileByteOffset,
                                              restoreRecPtr->pxlByteCount) == -1){

      TRACE_0(errout << "ERROR: CSaveRestore::obtainCurrentRestoreRecordPixelsSmall: "
                     << " can't read the .PIX file because: " << endl
                     << strerror(errno));
      return HISTORY_ERROR_RESTORING_PIXELS;
   }

   // implementation of the pixel channel means we no longer
   // have to specialize the restore code for region type

   MTI_UINT16 *rgnptr;
   MTI_UINT16 *runptr;

   while ((rgnptr = pxlChannel->getNextPixelRegion())!=nullptr) {

      switch(*rgnptr) {

         case PIXEL_REGION_TYPE_POINT:

            // pt coords must be within filter region
            if ((rgnptr[1] <= filterRegion.right )&&
                (rgnptr[2] <= filterRegion.bottom)&&
                (rgnptr[1] >= filterRegion.left  )&&
                (rgnptr[2] >= filterRegion.top   )) {

               while ((runptr = pxlChannel->getNextRun())!=nullptr) {

                  copyRunToRectangleBufferSmall(runptr,
                                                dstbuf,
                                                &filterRegion,
                                                pxlComponentCount);
               }
            }

         break;

         case PIXEL_REGION_TYPE_RECT:
         case PIXEL_REGION_TYPE_MASK:
         case PIXEL_REGION_TYPE_LASSO:
         case PIXEL_REGION_TYPE_BEZIER:

            // all other region types have proper extent boxes
            if ((rgnptr[1] <= filterRegion.right )&&
                (rgnptr[2] <= filterRegion.bottom)&&
                (rgnptr[3] >= filterRegion.left  )&&
                (rgnptr[4] >= filterRegion.top   )) {

               while ((runptr = pxlChannel->getNextRun())!=nullptr) {

                  copyRunToRectangleBufferSmall(runptr,
                                                dstbuf,
                                                &filterRegion,
                                                pxlComponentCount);
               }
            }

         break;

         default:
            // Avoid frickin' infinite loop
            TRACE_0(errout << "ERROR: Clip's history file is corrupt!");
            MTIassert(false);
            return HISTORY_ERROR_RESTORING_PIXELS;
      }
   }

   return 0;
}


int CSaveRestore::obtainOriginalValuesSmall(MTI_UINT16 *dstbuf, int frame, RECT *rect)
{
   int retVal;

   retVal = openForRestore(frame, -1, rect, nullptr, nullptr);
   if (retVal != 0)
      return retVal;

   while (nextRestoreRecord() == 0) {

      retVal = obtainCurrentRestoreRecordPixelsSmall(dstbuf);
      if (retVal != 0)
         break;
   }

   if (retVal != 0) {
      flushRestore();
      return retVal;
   }

   retVal = flushRestore();
   return retVal;
}

bool CSaveRestore::compareEdges(VERTICAL_EDGE *edg1, VERTICAL_EDGE *edg2)
{
   if (edg1->ybot < edg2->ybot)
      return true;

   if (edg1->ybot == edg2->ybot) {

      return (edg1->x < edg2->x);
   }

   return false;
}

// insert an edge into the SCAN LIST
void CSaveRestore::insertEdge(vector <VERTICAL_EDGE> &edgbuf, int iedg, int ipre, int inxt)
{
  edgbuf[iedg].ibwd = ipre;
  if (ipre!=(int)NULLEDG) {
    edgbuf[ipre].ifwd = iedg;
  }
  else {
    iEdgeList = iedg;
  }
  edgbuf[iedg].ifwd = inxt;
  if (inxt!=(int)NULLEDG) {
    edgbuf[inxt].ibwd = iedg;
  }
}

// delete an edge from the SCAN LIST
void CSaveRestore::deleteEdge(vector <VERTICAL_EDGE> &edgbuf, int iedg)
{
  int ipre,inxt;

  ipre = edgbuf[iedg].ibwd;
  inxt = edgbuf[iedg].ifwd;
  if (ipre!=(int)NULLEDG) {
    edgbuf[ipre].ifwd = inxt;
  }
  else {
    iEdgeList = inxt;
  }
  if (inxt!=(int)NULLEDG) {
    edgbuf[inxt].ibwd = ipre;
  }
}

int CSaveRestore::HistoryFileContainsFixes(string &hstFilePath)
{
   MTIassert(currentHistoryFileOpen == -1);
   MTIassert(pxlFileHandle == -1);

   int retVal = 0;

   pxlFileHandle = pxlChannel->openPixelChannel(0x100000, pxlComponentCount, hstFilePath, true);
   if (pxlFileHandle != -1)
   {

      // count VALID restore records in the file

      while (pxlChannel->nextRestoreRecord() == 0)
      {
         retVal++;
      }

      pxlChannel->closePixelChannel();

      pxlFileHandle = -1;
   }

   return retVal;
}

///////////////////////////////////////////////////////////////////////
//
//     G E T   P R E V I O U S   F R A M E   W I T H   F I X E S
//
// Get the closest PREVIOUS frame that has fixes, given a frame num.
//
// Return -1 if there is none.
//
int CSaveRestore::getPreviousFrameWithFixes(int frmnum)
{
   return (historyType == HISTORY_TYPE_MONOLITHIC)
            ? getPreviousFrameWithFixes_MONO(frmnum)
            : getPreviousFrameWithFixes_FPF(frmnum);
}
//------------------------------------------------------------------------------

int CSaveRestore::getPreviousFrameWithFixes_MONO(int frmnum)
{
   int errorCode = HISTORY_ERROR_UNKNOWN_ERROR;
   CAutoHistoryOpener opener(*this, FLUSH_TYPE_RESTORE, errorCode);
   if (errorCode != 0)
      return -1; // This is a FRAME NUMBER, not an error code!!

   int retval = -1;

   while(true) {

      if (resRecPtr == resBuf) { // time for next .HDR record

         while(true) {

            if (hdrRecPtr == hdrBuf) { // time for next .HDR block

               hdrBaseFileOffset -= HEADER_BUFFER_SIZE;
               if (hdrBaseFileOffset < 0) { // no more .HDR records
                  //flushRestore();  // done in auto opener destructor
                  return retval; // This is a FRAME NUMBER, not an error code!!
               }
               int result = lseek(hdrFileHandle, hdrBaseFileOffset, SEEK_SET);
               if (result == -1) {

                  TRACE_0(errout << "ERROR: History HDR file seek failed because "
                                 << strerror(errno));
                  MTIassert(result != -1);
                  return HISTORY_ERROR_SEEKING_IN_HDR_FILE;
               }
               MTIassert(result == hdrBaseFileOffset);

               result = read(hdrFileHandle, hdrBuf, HEADER_BUFFER_SIZE);
               if (result == -1) {

                  TRACE_0(errout << "ERROR: History HDR file read failed because "
                                 << strerror(errno));
                  MTIassert(result != -1);
                  return HISTORY_ERROR_READING_HDR_FILE;
               }
			   MTIassert(result == (int)HEADER_BUFFER_SIZE);

               hdrRecPtr = hdrBufStop;

            } // next .HDR block

            hdrRecPtr--;

            // now check the .HDR record against the (frame) filter

            if ((hdrRecPtr->maxFrame > retval)&&
                (hdrRecPtr->minFrame < frmnum)) {

               // get the corresp block of .RES records
               int hdrIndex = hdrBaseFileOffset/sizeof(HEADER_RECORD) +
                                 (hdrRecPtr - hdrBuf);

               resBaseFileOffset = hdrIndex * RESTORE_BUFFER_SIZE;

               // go there and read the block of .RES records
               int result = lseek(resFileHandle, resBaseFileOffset, SEEK_SET);
               if (result == -1) {

                  TRACE_0(errout << "ERROR: History HDR file seek failed because "
                                 << strerror(errno));
                  MTIassert(result != -1);
                  return HISTORY_ERROR_SEEKING_IN_HDR_FILE;
               }
               MTIassert(result == resBaseFileOffset);

               result = read(resFileHandle, resBuf, RESTORE_BUFFER_SIZE);
               if (result == -1) {

                  TRACE_0(errout << "ERROR: History RES file read failed because "
                                 << strerror(errno));
                  MTIassert(result != -1);
                  return HISTORY_ERROR_READING_RES_FILE;
               }
			   MTIassert(result == (int)RESTORE_BUFFER_SIZE);

               // set the .RES record ptr to the stop
               resRecPtr = resBufStop;
               goto fndH0;

            } // maxFrame > retval

         } // while(true)

fndH0:; // found the next .HDR record and inited for it

      } // next .HDR record

      resRecPtr--;

      // now check the .RES record against the filter

      if (((resRecPtr->restoreFlag&IS_INVALID) == 0)&&
          ((resRecPtr->restoreFlag&IS_RESTORED) == 0)&&
          (resRecPtr->pxlByteCount > 0)&&
          (resRecPtr->frameNum < frmnum)&&
          (resRecPtr->frameNum > retval)) {

         retval = resRecPtr->frameNum;

      } // frames test

   } // while(true)

   return -1;  // THis is a FRAME NUMBER, not an error code!!
}
//------------------------------------------------------------------------------

int CSaveRestore::getPreviousFrameWithFixes_FPF(int frmnum)
{
   CAutoThreadLocker lock(*getLock());

   for (int preFrame = (frmnum - 1); preFrame >= srcInFrame; preFrame--)
   {
      string preHistoryFilePath = getHistoryFilePath(srcClip, preFrame);
      if (DoesFileExist(preHistoryFilePath))
      {
         if (HistoryFileContainsFixes(preHistoryFilePath))
         {
            return preFrame;
         }
      }
   }

   return -1;  // THis is a FRAME NUMBER, not an error code!!
}
//------------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////
//
//     G E T   N E X T   F R A M E   W I T H   F I X E S
//
// Get the closest NEXT frame that has fixes, given a frame num.
//
// Return -1 if there is none.
//
int CSaveRestore::getNextFrameWithFixes(int frmnum)
{
   return (historyType == HISTORY_TYPE_MONOLITHIC)
            ? getNextFrameWithFixes_MONO(frmnum)
            : getNextFrameWithFixes_FPF(frmnum);
}
//------------------------------------------------------------------------------

int CSaveRestore::getNextFrameWithFixes_MONO(int frmnum)
{
   int errorCode = HISTORY_ERROR_UNKNOWN_ERROR;
   CAutoHistoryOpener opener(*this, FLUSH_TYPE_RESTORE, errorCode);
   if (errorCode != 0)
      return -1;            // This is a FRAME NUMBER, not an error code!!

   int retval = 0x7fffffff;

   while(true) {

      if (resRecPtr == resBuf) { // time for next .HDR record

         while(true) {

            if (hdrRecPtr == hdrBuf) { // time for next .HDR block

               hdrBaseFileOffset -= HEADER_BUFFER_SIZE;
               if (hdrBaseFileOffset < 0) { // no more .HDR records
                  //flushRestore();  done in auto opener destructor
                  if (retval == 0x7fffffff) retval = -1;
                  return(retval);
               }
               int result = lseek(hdrFileHandle, hdrBaseFileOffset, SEEK_SET);
               if (result == -1) {

                  TRACE_0(errout << "ERROR: History HDR file seek failed because "
                                 << strerror(errno));
                  MTIassert(result != -1);
                  return HISTORY_ERROR_SEEKING_IN_HDR_FILE;
               }
               MTIassert(result == hdrBaseFileOffset);

               result = read(hdrFileHandle, hdrBuf, HEADER_BUFFER_SIZE);
               if (result == -1) {

                  TRACE_0(errout << "ERROR: History HDR file read failed because "
                                 << strerror(errno));
                  MTIassert(result != -1);
                  return HISTORY_ERROR_READING_HDR_FILE;
               }
			   MTIassert(result == (int)HEADER_BUFFER_SIZE);

               hdrRecPtr = hdrBufStop;

            } // next .HDR block

            hdrRecPtr--;

            // now check the .HDR record against the (frame) filter

            if ((hdrRecPtr->minFrame < retval)&&
                (hdrRecPtr->maxFrame > frmnum)) {

               // get the corresp block of .RES records
               int hdrIndex = hdrBaseFileOffset/sizeof(HEADER_RECORD) +
                                 (hdrRecPtr - hdrBuf);

               resBaseFileOffset = hdrIndex * RESTORE_BUFFER_SIZE;

               // go there and read the block of .RES records
               int result = lseek(resFileHandle, resBaseFileOffset, SEEK_SET);
               if (result == -1) {

                  TRACE_0(errout << "ERROR: History RES file seek failed because "
                                 << strerror(errno));
                  MTIassert(result != -1);
                  return HISTORY_ERROR_SEEKING_IN_RES_FILE;
               }
               MTIassert(result == resBaseFileOffset);

               result = read(resFileHandle, resBuf, RESTORE_BUFFER_SIZE);
               if (result == -1) {

                  TRACE_0(errout << "ERROR: History RES file read failed because "
                                 << strerror(errno));
                  MTIassert(result != -1);
                  return HISTORY_ERROR_READING_RES_FILE;
               }
			   MTIassert(result == (int)RESTORE_BUFFER_SIZE);

               // set the .RES record ptr to the stop
               resRecPtr = resBufStop;
               goto fndH1;

            } // minFrame < retval

         } // while(true)

fndH1:; // found the next .HDR record and inited for it

      } // next .HDR record

      resRecPtr--;

      // now check the .RES record against the filter

      if (((resRecPtr->restoreFlag&IS_INVALID) == 0)&&
          ((resRecPtr->restoreFlag&IS_RESTORED) == 0)&&
          (resRecPtr->pxlByteCount > 0 )&&
          (resRecPtr->frameNum > frmnum)&&
          (resRecPtr->frameNum < retval)) {

         retval = resRecPtr->frameNum;

      } // frames test

   } // while(true)

}
//------------------------------------------------------------------------------

int CSaveRestore::getNextFrameWithFixes_FPF(int frmnum)
{
   CAutoThreadLocker lock(*getLock());

   for (int nxtFrame = (frmnum + 1); nxtFrame < srcOutFrame; nxtFrame++)
   {
      string nxtHistoryFilePath = getHistoryFilePath(srcClip, nxtFrame);
      if (DoesFileExist(nxtHistoryFilePath))
      {
         if (HistoryFileContainsFixes(nxtHistoryFilePath))
         {
            return nxtFrame;
         }
      }
   }

   return -1;
}
//------------------------------------------------------------------------------

// For each frame in frameIndexList, replace the history pertaining to that
// frame in dstclp/track/framingtype with the history pertaining to that
// frame from srcclp/track/framingType. LEAVE ALL THE OTHER FRAMES IN
// dstclp/track/framingType AS-IS - DO NOT DELETE THEM

int CSaveRestore::replaceHistory(string const &srcFrameListFileName,
                                 string const &dstFrameListFileName,
                                 ClipSharedPtr &srcclip,
                                 ClipSharedPtr &dstclip,
                                 const vector<int> *frameIndexList,
                                 int *progressPercent,
                                 int *countDown)
{
   CAutoThreadLocker lock(*getLock());

   if (progressPercent != nullptr)
   {
      *progressPercent = 0;
   }

   // historyType is undefined if you don't do this. Careful: there are
   // member variables named srcClip and dstClip!!!
   setClip(srcclip);

   return (historyType == HISTORY_TYPE_MONOLITHIC)
            ? replaceHistory_MONO(srcFrameListFileName, dstFrameListFileName, srcclip, dstclip, frameIndexList, progressPercent, countDown)
            : replaceHistory_FPF(srcFrameListFileName, dstFrameListFileName, srcclip, dstclip, frameIndexList, progressPercent, countDown);
}
//------------------------------------------------------------------------------

int CSaveRestore::replaceHistory_MONO(string const &srcFrameListFileName,
                                       string const &dstFrameListFileName,
                                       ClipSharedPtr &srcclip,
                                       ClipSharedPtr &dstclip,
                                       const vector<int> *frameIndexList,
                                       int *progressPercent,
                                       int *countDown)
{
   // allocate tmp buffers
   HEADER_RECORD *tmpHdrBuf = new HEADER_RECORD[HEADER_BLOCKING_FACTOR];
   HEADER_RECORD *tmpHdrBufStop = tmpHdrBuf + HEADER_BLOCKING_FACTOR;

   RESTORE_RECORD *tmpResBuf = new RESTORE_RECORD[RESTORE_BLOCKING_FACTOR];
   RESTORE_RECORD *tmpResBufStop = tmpResBuf + RESTORE_BLOCKING_FACTOR;

   char *tmpPxlBuf = new char[PXLBUF_SIZE];

   int remResRecs;
   unsigned int totHdrRecs;
   int result;

   /////////////////////////////////////////////////////////////////////////////

   // determine the number of frames to replace
   //
   int framesToReplace = 0;
   if (frameIndexList != nullptr) {
      framesToReplace = frameIndexList->size();
   }

   // Short-circuit on no-op
   if (framesToReplace == 0)
      return 0;

   string tmpFrameListFileName = dstFrameListFileName + "_tmp";

   // get the tmp file names
   string tmpHdrFileName = tmpFrameListFileName + ".hed";
   string tmpResFileName = tmpFrameListFileName + ".rst";
   string tmpPxlFileName = tmpFrameListFileName + ".pix";

   // delete them if they exist
   DeleteFile(tmpHdrFileName.c_str());
   DeleteFile(tmpResFileName.c_str());
   DeleteFile(tmpPxlFileName.c_str());

   // open the .RES file
   int tmpResFileHandle;
   if ((tmpResFileHandle = open(tmpResFileName.c_str(),
                         O_BINARY|O_CREAT|O_WRONLY, S_IRUSR|S_IWUSR)) == -1)
   {
      delete [] tmpHdrBuf;
      delete [] tmpResBuf;
      delete [] tmpPxlBuf;
      TRACE_0(errout << "ERROR: CSaveRestore::deleteHistory "
                     << " can't open the .RES 1 file because: " << endl
                     << strerror(errno));
      return(HISTORY_ERROR_OPENING_RES_FILE);
   }

   // seek to beginning of .RES file
   lseek(tmpResFileHandle, 0, SEEK_SET);

   // open the .HDR file
   int tmpHdrFileHandle;
   if ((tmpHdrFileHandle = open(tmpHdrFileName.c_str(),
                           O_BINARY|O_CREAT|O_WRONLY, S_IRUSR|S_IWUSR)) == -1)
   {
      delete [] tmpHdrBuf;
      delete [] tmpResBuf;
      delete [] tmpPxlBuf;
      close(tmpResFileHandle);
      TRACE_0(errout << "ERROR: CSaveRestore::deleteHistory "
                     << " can't open the .HDR 1 file because: " << endl
                     << strerror(errno));
      return(HISTORY_ERROR_OPENING_HDR_FILE);
   }

   // seek to beginning of .HDR file
   lseek(tmpHdrFileHandle, 0, SEEK_SET);

   // open the .PXL file
   int tmpPxlFileHandle;
   if ((tmpPxlFileHandle = open(tmpPxlFileName.c_str(),
                           O_BINARY|O_CREAT|O_WRONLY, S_IRUSR|S_IWUSR)) == -1)
   {
      delete [] tmpHdrBuf;
      delete [] tmpResBuf;
      delete [] tmpPxlBuf;
      close(tmpResFileHandle);
      close(tmpHdrFileHandle);
      TRACE_0(errout << "ERROR: CSaveRestore::deleteHistory "
                     << " can't open the .CMD 1 file because: " << endl
                     << strerror(errno));
      return(HISTORY_ERROR_OPENING_PXL_FILE);
   }

   // seek to beginning of .PXL file
   lseek(tmpPxlFileHandle, 0, SEEK_SET);

   // set ptrs for adding to tmp history
   HEADER_RECORD *tmpHdrRecPtr = tmpHdrBuf;
   tmpHdrRecPtr->minFrame =  0x7fffffff;
   tmpHdrRecPtr->maxFrame = -0x7fffffff;

   RESTORE_RECORD *tmpResRecPtr = tmpResBuf;

   MTI_UINT64 tmpPxlFileOffset = 0;

   // first path takes dst history and purges it of
   // all records pertaining to the given frame range

   // if no dst history exists, there is nothing to delete!
   if (!historyExists(dstFrameListFileName))
      goto SKIP_PATH_1;

   // open the DST history files
   hdrFileName = dstFrameListFileName + ".hed";
   resFileName = dstFrameListFileName + ".rst";
   pxlFileName = dstFrameListFileName + ".pix";

   // open the .RES file
   if ((resFileHandle = open(resFileName.c_str(),
                        O_BINARY|O_CREAT|O_RDONLY, S_IRUSR|S_IWUSR)) == -1)
   {
      delete [] tmpHdrBuf;
      delete [] tmpResBuf;
      delete [] tmpPxlBuf;
      close(tmpResFileHandle);
      close(tmpHdrFileHandle);
      close(tmpPxlFileHandle);
      TRACE_0(errout << "ERROR: CSaveRestore::deleteHistory "
                     << " can't open the .RES 2 file because: " << endl
                     << strerror(errno));
      return(HISTORY_ERROR_OPENING_RES_FILE);
   }

   // determine its length
   resFilePtr = lseek(resFileHandle,0,SEEK_END);

   // length must be a multiple of record size
   if ((resFilePtr % sizeof(RESTORE_RECORD))!=0)
   {
      delete [] tmpHdrBuf;
      delete [] tmpResBuf;
      delete [] tmpPxlBuf;
      close(tmpResFileHandle);
      close(tmpHdrFileHandle);
      close(tmpPxlFileHandle);
      close(resFileHandle);
      resFileHandle = -1;
      TRACE_0(errout << "ERROR: CSaveRestore::deleteHistory "
                     << ".RES 2 file is corrupt");
      return(HISTORY_ERROR_OPENING_RES_FILE);
   }

   // the total restore records
   remResRecs = resFilePtr / sizeof(RESTORE_RECORD);

   // the total number of .HDR records
   totHdrRecs = (remResRecs + (RESTORE_BLOCKING_FACTOR - 1)) /
                              RESTORE_BLOCKING_FACTOR ;

   // open the .HDR file
   if ((hdrFileHandle = open(hdrFileName.c_str(),
                        O_BINARY|O_CREAT|O_RDONLY, S_IRUSR|S_IWUSR)) == -1)
   {
      delete [] tmpHdrBuf;
      delete [] tmpResBuf;
      delete [] tmpPxlBuf;
      close(tmpResFileHandle);
      close(tmpHdrFileHandle);
      close(tmpPxlFileHandle);
      //close(resFileHandle);
      closeHistoryFiles();
      TRACE_0(errout << "ERROR: CSaveRestore::deleteHistory "
                     << " can't open the .HDR 2 file because: " << endl
                     << strerror(errno));
      return(HISTORY_ERROR_OPENING_HDR_FILE);
   }

   // determine its length
   hdrFilePtr = lseek(hdrFileHandle,0,SEEK_END);

   // header records must match .RES file
   if (hdrFilePtr/sizeof(HEADER_RECORD) != totHdrRecs)
   {
      delete [] tmpHdrBuf;
      delete [] tmpResBuf;
      delete [] tmpPxlBuf;
      close(tmpResFileHandle);
      close(tmpHdrFileHandle);
      close(tmpPxlFileHandle);
      closeHistoryFiles();
      TRACE_0(errout << "ERROR: CSaveRestore::deleteHistory "
                     << ".HDR 2 file is corrupt");
      return(HISTORY_ERROR_OPENING_HDR_FILE);
   }

   // open the .PXL file
   if ((pxlFileHandle = open(pxlFileName.c_str(),
                        O_BINARY|O_CREAT|O_RDONLY, S_IRUSR|S_IWUSR)) == -1)
   {
      delete [] tmpHdrBuf;
      delete [] tmpResBuf;
      delete [] tmpPxlBuf;
      close(tmpResFileHandle);
      close(tmpHdrFileHandle);
      close(tmpPxlFileHandle);
      closeHistoryFiles();
      TRACE_0(errout << "ERROR: CSaveRestore::deleteHistory "
                     << " can't open the .PXL 2 file because: " << endl
                     << strerror(errno));
      return(HISTORY_ERROR_OPENING_PXL_FILE);
   }

   // for each dst hdr do
   hdrBaseFileOffset = 0;
   result = lseek(hdrFileHandle, hdrBaseFileOffset, SEEK_SET);
   if (result == -1) {

      TRACE_0(errout << "ERROR: History HDR file seek failed because "
                     << strerror(errno));
      MTIassert(result != -1);
      return HISTORY_ERROR_SEEKING_IN_HDR_FILE;
   }
   MTIassert(result == hdrBaseFileOffset);

   result = read(hdrFileHandle, hdrBuf, HEADER_BUFFER_SIZE);
   if (result == -1) {

      TRACE_0(errout << "ERROR: History HDR file read failed because "
                     << strerror(errno));
      MTIassert(result != -1);
      return HISTORY_ERROR_READING_HDR_FILE;
   }
   MTIassert(result == (int)HEADER_BUFFER_SIZE || result == (int)(totHdrRecs * sizeof(HEADER_RECORD)));

   hdrRecPtr = hdrBuf;

   for (unsigned int i=0; i< totHdrRecs; i++) {

      { // .HDR is relevant

         resBaseFileOffset = i*RESTORE_BUFFER_SIZE;

         result = lseek(resFileHandle, resBaseFileOffset, SEEK_SET);
         if (result == -1) {

            TRACE_0(errout << "ERROR: History RES file seek failed because "
                           << strerror(errno));
            MTIassert(result != -1);
            return HISTORY_ERROR_SEEKING_IN_RES_FILE;
         }
         MTIassert(result == resBaseFileOffset);

         result = read(resFileHandle, resBuf, RESTORE_BUFFER_SIZE);
         if (result == -1) {

            TRACE_0(errout << "ERROR: History RES file read failed because "
                           << strerror(errno));
            MTIassert(result != -1);
            return HISTORY_ERROR_READING_RES_FILE;
         }
         // I guess it's ok to be a short read
         //MTIassert(result == RESTORE_BUFFER_SIZE);

         resRecPtr = resBuf;

         // for each .RES of block do


         while (resRecPtr < resBufStop) {

            // assume it's included
            bool includeResRec = true;

            // check if it's to be deleted
            if (framesToReplace > 0) {
               for (int j=0;j<framesToReplace;j++) {
                  if (resRecPtr->frameNum == (*frameIndexList)[j]) {
                     includeResRec = false;
                     break;
                  }
               }
            }

            // if it's to be included do
            if (includeResRec) {

               // put dst .RES rec to tmp .RES output
               MTImemcpy((void *)tmpResRecPtr, (void *)resRecPtr,
                      sizeof(RESTORE_RECORD));

               // include frame number in current .HDR rec
               if (tmpResRecPtr->frameNum < tmpHdrRecPtr->minFrame)
                  tmpHdrRecPtr->minFrame = tmpResRecPtr->frameNum;
               if (tmpResRecPtr->frameNum > tmpHdrRecPtr->maxFrame)
                  tmpHdrRecPtr->maxFrame = tmpResRecPtr->frameNum;

               // add dst .PXL data to tmp .PXL
               tmpResRecPtr->pxlFileByteOffset = tmpPxlFileOffset;

//   TRACE_2(errout << "=========TTT tmpResRecPtr RES RECORD=========" << endl);
//   TRACE_2(dumpRestoreRecord(resRecPtr));
//   TRACE_2(errout << "====================================" << endl);

               MTI_INT64 result64 = lseek64(pxlFileHandle, resRecPtr->pxlFileByteOffset, SEEK_SET);
               if (result64 == -1) {

                  TRACE_0(errout << "ERROR: History PXL file seek failed because "
                                 << strerror(errno));
                  MTIassert(result64 != -1);
                  break;
               }
               MTIassert(result64 == resRecPtr->pxlFileByteOffset);

               int residue = resRecPtr->pxlByteCount;
               while (residue > 0) {

                  int numbyts = residue;
                  if (numbyts > PXLBUF_SIZE) {
                     numbyts = PXLBUF_SIZE;
                  }

                  result = read(pxlFileHandle, tmpPxlBuf, numbyts);
                  if (result == -1) {

                     TRACE_0(errout << "ERROR: History PXL file read failed because "
                                    << strerror(errno));
                     MTIassert(result != -1);
                     return HISTORY_ERROR_READING_PXL_FILE;
                  }
                  MTIassert(result == numbyts);

MTIassert((tmpPxlFileOffset != 0) || IsValidPixelRegionType(tmpPxlBuf[0]));

                  result = write(tmpPxlFileHandle, tmpPxlBuf, numbyts);
                  if (result == -1) {

                     TRACE_0(errout << "ERROR: History PXL TMP file write failed because "
                                    << strerror(errno));
                     MTIassert(result != -1);
                     return HISTORY_ERROR_WRITING_PXL_FILE;
                  }
                  MTIassert(result == numbyts);

                  tmpPxlFileOffset += numbyts;
                  residue -= numbyts;
               }

               // advance to next .RES record
               tmpResRecPtr++;
               if (tmpResRecPtr == tmpResBufStop) {

                  result = write(tmpResFileHandle, tmpResBuf, RESTORE_BUFFER_SIZE);
                  if (result == -1) {

                     TRACE_0(errout << "ERROR: History RES file write failed because "
                                    << strerror(errno));
                     MTIassert(result != -1);
                     return HISTORY_ERROR_WRITING_RES_FILE;
                  }
				  MTIassert(result == (int)RESTORE_BUFFER_SIZE);

                  tmpResRecPtr = tmpResBuf;

                  tmpHdrRecPtr++;
                  if (tmpHdrRecPtr == tmpHdrBufStop) {

                     result = write(tmpHdrFileHandle, tmpHdrBuf, HEADER_BUFFER_SIZE);
                     if (result == -1) {

                        TRACE_0(errout << "ERROR: History HDR file write failed because "
                                       << strerror(errno));
                        MTIassert(result != -1);
                        return HISTORY_ERROR_WRITING_HDR_FILE;
                     }
                     MTIassert(result == (int)HEADER_BUFFER_SIZE);

                     tmpHdrRecPtr = tmpHdrBuf;
                  }

                  // init new .HDR rec
                  tmpHdrRecPtr->minFrame =  0x7fffffff;
                  tmpHdrRecPtr->maxFrame = -0x7fffffff;
               }
            }

            // next rst
            resRecPtr++;
            if (--remResRecs <= 0) break;
         }

         //if (remResRecs <= 0) break;
      }

      // advance to next .HDR record
      hdrRecPtr++;
      if (hdrRecPtr == hdrBufStop) {
         hdrBaseFileOffset += HEADER_BUFFER_SIZE;
         result = lseek(hdrFileHandle, hdrBaseFileOffset, SEEK_SET);
         if (result == -1) {

            TRACE_0(errout << "ERROR: History HDR file seek failed because "
                           << strerror(errno));
            MTIassert(result != -1);
            return HISTORY_ERROR_SEEKING_IN_HDR_FILE;
         }
         MTIassert(result == hdrBaseFileOffset);

         result = read(hdrFileHandle, hdrBuf, hdrBlkLeng);
         if (result == -1) {

            TRACE_0(errout << "ERROR: History HDR file read failed because "
                           << strerror(errno));
            MTIassert(result != -1);
            return HISTORY_ERROR_READING_HDR_FILE;
         }
         MTIassert(result == hdrBlkLeng);

         hdrRecPtr = hdrBuf;
      }
   }

   // old:
   // close DST history files
   // close the .RES file
   //close(resFileHandle);

   // close the .HDR file
   //close(hdrFileHandle);

   // close the .CMD file
   //close(cmdFileHandle);

   // CAREFUL!!! WE OPENED THE .PIX FILE IN A NON-STANDARD WAY!
   //            So we have to close it before calling closeHistoryFiles()!
   // close the .PXL file
   close(pxlFileHandle);
   pxlFileHandle = -1;

   // new:
   closeHistoryFiles();

SKIP_PATH_1:;

   // second path takes src history and appends to tmp
   // all records pertaining to the given frame range

   // If there is no history to copy frames from, we're done
   if (!historyExists(srcFrameListFileName)) {
      // There's no history to copy from!
      goto SKIP_PATH_2;
   }

   // open the SRC history files
   hdrFileName = srcFrameListFileName + ".hed";
   resFileName = srcFrameListFileName + ".rst";
   pxlFileName = srcFrameListFileName + ".pix";

   // open the .RES file
   if ((resFileHandle = open(resFileName.c_str(),
                        O_BINARY|O_CREAT|O_RDONLY, S_IRUSR|S_IWUSR)) == -1)
   {
      delete [] tmpHdrBuf;
      delete [] tmpResBuf;
      delete [] tmpPxlBuf;
      close(tmpResFileHandle);
      close(tmpHdrFileHandle);
      close(tmpPxlFileHandle);
      TRACE_0(errout << "ERROR: CSaveRestore::deleteHistory "
                     << " can't open the .RES 2 file because: " << endl
                     << strerror(errno));
      return(HISTORY_ERROR_OPENING_RES_FILE);
   }

   // determine its length
   resFilePtr = lseek(resFileHandle,0,SEEK_END);

   // length must be a multiple of record size
   if ((resFilePtr % sizeof(RESTORE_RECORD))!=0)
   {
      delete [] tmpHdrBuf;
      delete [] tmpResBuf;
      delete [] tmpPxlBuf;
      close(tmpResFileHandle);
      close(tmpHdrFileHandle);
      close(tmpPxlFileHandle);
      closeHistoryFiles();
      TRACE_0(errout << "ERROR: CSaveRestore::deleteHistory "
                     << ".RES 2 file is corrupt");
      return(HISTORY_ERROR_OPENING_RES_FILE);
   }

   // the total restore records
   remResRecs = resFilePtr / sizeof(RESTORE_RECORD);

   // the total number of .HDR records
   totHdrRecs = (remResRecs + (RESTORE_BLOCKING_FACTOR - 1)) /
                              RESTORE_BLOCKING_FACTOR ;

   // open the .HDR file
   if ((hdrFileHandle = open(hdrFileName.c_str(),
                        O_BINARY|O_CREAT|O_RDONLY, S_IRUSR|S_IWUSR)) == -1)
   {
      delete [] tmpHdrBuf;
      delete [] tmpResBuf;
      delete [] tmpPxlBuf;
      close(tmpResFileHandle);
      close(tmpHdrFileHandle);
      close(tmpPxlFileHandle);
      //close(resFileHandle);
      closeHistoryFiles();
      TRACE_0(errout << "ERROR: CSaveRestore::deleteHistory "
                     << " can't open the .HDR 2 file because: " << endl
                     << strerror(errno));
      return(HISTORY_ERROR_OPENING_HDR_FILE);
   }

   // determine its length
   hdrFilePtr = lseek(hdrFileHandle,0,SEEK_END);

   // header records must match .RES file
   if (hdrFilePtr/sizeof(HEADER_RECORD) != totHdrRecs)
   {
      delete [] tmpHdrBuf;
      delete [] tmpResBuf;
      delete [] tmpPxlBuf;
      close(tmpResFileHandle);
      close(tmpHdrFileHandle);
      close(tmpPxlFileHandle);
      closeHistoryFiles();
      TRACE_0(errout << "ERROR: CSaveRestore::deleteHistory "
                     << ".HDR 2 file is corrupt");
      return(HISTORY_ERROR_OPENING_HDR_FILE);
   }

   // open the .PXL file
   if ((pxlFileHandle = open(pxlFileName.c_str(),
                        O_BINARY|O_CREAT|O_RDONLY, S_IRUSR|S_IWUSR)) == -1)
   {
      delete [] tmpHdrBuf;
      delete [] tmpResBuf;
      delete [] tmpPxlBuf;
      close(tmpResFileHandle);
      close(tmpHdrFileHandle);
      close(tmpPxlFileHandle);
      closeHistoryFiles();
      TRACE_0(errout << "ERROR: CSaveRestore::deleteHistory "
                     << " can't open the .PXL 2 file because: " << endl
                     << strerror(errno));
      return(HISTORY_ERROR_OPENING_PXL_FILE);
   }

   // for each src hdr do
   hdrBaseFileOffset = 0;
   result = lseek(hdrFileHandle, hdrBaseFileOffset, SEEK_SET);
   if (result == -1) {

      TRACE_0(errout << "ERROR: History HDR file seek failed because "
                     << strerror(errno));
      MTIassert(result != -1);
      return HISTORY_ERROR_SEEKING_IN_HDR_FILE;
   }
   MTIassert(result == hdrBaseFileOffset);

   result = read(hdrFileHandle, hdrBuf, HEADER_BUFFER_SIZE);
   if (result == -1) {

      TRACE_0(errout << "ERROR: History HDR file read failed because "
                     << strerror(errno));
      MTIassert(result != -1);
      return HISTORY_ERROR_READING_HDR_FILE;
   }
   MTIassert(result == (int)HEADER_BUFFER_SIZE || result == (int)(totHdrRecs * sizeof(HEADER_RECORD)));

   hdrRecPtr = hdrBuf;

   for (unsigned int i=0; i< totHdrRecs; i++) {

      { // .HDR is relevant

         resBaseFileOffset = i*RESTORE_BUFFER_SIZE;
         result = lseek(resFileHandle, resBaseFileOffset, SEEK_SET);
         if (result == -1) {

            TRACE_0(errout << "ERROR: History RES file seek failed because "
                           << strerror(errno));
            MTIassert(result != -1);
            return HISTORY_ERROR_SEEKING_IN_RES_FILE;
         }
         MTIassert(result == resBaseFileOffset);

         result = read(resFileHandle, resBuf, RESTORE_BUFFER_SIZE);
         if (result == -1) {

            TRACE_0(errout << "ERROR: History RES file read failed because "
                           << strerror(errno));
            MTIassert(result != -1);
            return HISTORY_ERROR_READING_RES_FILE;
         }
		 MTIassert(result == (int)RESTORE_BUFFER_SIZE);

         resRecPtr = resBuf;

         // for each .RES of block do


         while (resRecPtr < resBufStop) {

            // assume it's NOT included
            bool includeResRec = false;

            // check if it's to be replaced
            if (framesToReplace > 0) {
               for (int j=0;j<framesToReplace;j++) {
                  if (resRecPtr->frameNum == (*frameIndexList)[j]) {
                     includeResRec = true;
                     break;
                  }
               }
            }

            // if it's to be included do
            if (includeResRec) {

               // put src .RES rec to tmp .RES output
               MTImemcpy((void *)tmpResRecPtr, (void *)resRecPtr,
                      sizeof(RESTORE_RECORD));

               // include frame number in current .HDR rec
               if (tmpResRecPtr->frameNum < tmpHdrRecPtr->minFrame)
                  tmpHdrRecPtr->minFrame = tmpResRecPtr->frameNum;
               if (tmpResRecPtr->frameNum > tmpHdrRecPtr->maxFrame)
                  tmpHdrRecPtr->maxFrame = tmpResRecPtr->frameNum;

               // add src .PXL data to tmp .PXL
               tmpResRecPtr->pxlFileByteOffset = tmpPxlFileOffset;

//   TRACE_2(errout << "=========TTT tmpResRecPtr 2 RES RECORD=========" << endl);
//   TRACE_2(dumpRestoreRecord(resRecPtr));
//   TRACE_2(errout << "====================================" << endl);

               MTI_INT64 result64 = lseek64(pxlFileHandle, resRecPtr->pxlFileByteOffset, SEEK_SET);
               if (result64 == -1) {

                  TRACE_0(errout << "ERROR: History PXL file seek failed because "
                                 << strerror(errno));
                  MTIassert(result64 != -1);
                  break;
               }
               MTIassert(result64 == resRecPtr->pxlFileByteOffset);

               int residue = resRecPtr->pxlByteCount;
               while (residue > 0) {

                  int numbyts = residue;
                  if (numbyts > PXLBUF_SIZE) {
                     numbyts = PXLBUF_SIZE;
                  }

                  result = read(pxlFileHandle, tmpPxlBuf, numbyts);
                  if (result == -1) {

                     TRACE_0(errout << "ERROR: History PXL file read failed because "
                                    << strerror(errno));
                     MTIassert(result != -1);
                     return HISTORY_ERROR_READING_PXL_FILE;
                  }
                  MTIassert(result == numbyts);

MTIassert((tmpPxlFileOffset != 0) || IsValidPixelRegionType(tmpPxlBuf[0]));

                  result = write(tmpPxlFileHandle, tmpPxlBuf, numbyts);
                  if (result == -1) {

                     TRACE_0(errout << "ERROR: History TMP PXL file write failed because "
                                    << strerror(errno));
                     MTIassert(result != -1);
                     return HISTORY_ERROR_WRITING_PXL_FILE;
                  }
                  MTIassert(result == numbyts);

                  tmpPxlFileOffset += numbyts;
                  residue -= numbyts;
               }

               // advance to next .RES record
               tmpResRecPtr++;
               if (tmpResRecPtr == tmpResBufStop) {

                  write(tmpResFileHandle, tmpResBuf, RESTORE_BUFFER_SIZE);
                  tmpResRecPtr = tmpResBuf;

                  tmpHdrRecPtr++;
                  if (tmpHdrRecPtr == tmpHdrBufStop) {

                     write(tmpHdrFileHandle, tmpHdrBuf, HEADER_BUFFER_SIZE);
                     tmpHdrRecPtr = tmpHdrBuf;
                  }

                  // init new .HDR rec
                  tmpHdrRecPtr->minFrame =  0x7fffffff;
                  tmpHdrRecPtr->maxFrame = -0x7fffffff;
               }
            }

            // next rst
            resRecPtr++;
            if (--remResRecs <= 0) break;
         }

         //if (remResRecs <= 0) break;
      }

      // advance to next .HDR record
      hdrRecPtr++;
      if (hdrRecPtr == hdrBufStop) {
         hdrBaseFileOffset += HEADER_BUFFER_SIZE;
         result = lseek(hdrFileHandle, hdrBaseFileOffset, SEEK_SET);
         if (result == -1) {

            TRACE_0(errout << "ERROR: History HDR file seek failed because "
                           << strerror(errno));
            MTIassert(result != -1);
            return HISTORY_ERROR_SEEKING_IN_HDR_FILE;
         }
         MTIassert(result == hdrBaseFileOffset);

         result = read(hdrFileHandle, hdrBuf, hdrBlkLeng);
         if (result == -1) {

            TRACE_0(errout << "ERROR: History HDR file read failed because "
                           << strerror(errno));
            MTIassert(result != -1);
            return HISTORY_ERROR_SEEKING_IN_HDR_FILE;
         }
         MTIassert(result == hdrBlkLeng);

         hdrRecPtr = hdrBuf;
      }
   }

   // CAREFUL!!! WE OPENED THE .PIX FILE IN A NON-STANDARD WAY!
   //            So we have to close it before calling closeHistoryFiles()!
   // close the .PXL file
   close(pxlFileHandle);
   pxlFileHandle = -1;

   // new:
   closeHistoryFiles();

SKIP_PATH_2:;

   // close TMP history files
   // close the .RES file
   write(tmpResFileHandle,tmpResBuf,(tmpResRecPtr - tmpResBuf)*sizeof(RESTORE_RECORD));
   close(tmpResFileHandle);

   // close the .HDR file
   int numBytes = (tmpHdrRecPtr - tmpHdrBuf)*sizeof(HEADER_RECORD);
   if (tmpResRecPtr > tmpResBuf) numBytes += sizeof(HEADER_RECORD);
   write(tmpHdrFileHandle,tmpHdrBuf,numBytes);
   close(tmpHdrFileHandle);

   // close the .PXL file
   close(tmpPxlFileHandle);

   // delete any existing version of the DST files
   string oldDstHdrFileName = dstFrameListFileName + ".hed";
   string oldDstResFileName = dstFrameListFileName + ".rst";
   string oldDstPxlFileName = dstFrameListFileName + ".pix";

   DeleteFile(oldDstHdrFileName.c_str());
   DeleteFile(oldDstResFileName.c_str());
   DeleteFile(oldDstPxlFileName.c_str());

   // rename the temporary version of DST file
   MoveFile(tmpHdrFileName.c_str(), oldDstHdrFileName.c_str());
   MoveFile(tmpResFileName.c_str(), oldDstResFileName.c_str());
   MoveFile(tmpPxlFileName.c_str(), oldDstPxlFileName.c_str());

   //////////////////////////////////////////////////////////////////////////

   // free tmp buffers
   delete [] tmpPxlBuf;
   delete [] tmpResBuf;
   delete [] tmpHdrBuf;

   if (progressPercent != nullptr)
   {
      *progressPercent = 100;
   }

   if (countDown != nullptr)
   {
      *countDown = 0;
   }

   return 0;
}
//------------------------------------------------------------------------------

int CSaveRestore::replaceHistory_FPF(string const &srcFrameListFileName,
                                    string const &dstFrameListFileName,
                                    ClipSharedPtr &srcclip,
                                    ClipSharedPtr &dstclip,
                                    const vector<int> *frameIndexList,
                                    int *progressPercent,
                                    int *countDown)
{
   string srcFrameListPath = GetFilePath(srcFrameListFileName);
   string tstSrcPath = srcFrameListPath;
   tstSrcPath = GetFilePath(tstSrcPath.substr(0, tstSrcPath.length() - 1));

   string dstFrameListPath = GetFilePath(dstFrameListFileName);
   string tstDstPath = dstFrameListPath;
   tstDstPath = GetFilePath(tstDstPath.substr(0, tstDstPath.length() - 1));

   // TWO CASES:
   // 1) (Commit)  VERSION TO CLIP (copy ver history files to master)
   // 2) (Discard) CLIP TO VERSION (delete history files in version)

   int framesToReplace = 0;
   if (frameIndexList != nullptr)
   {
      framesToReplace = frameIndexList->size();
   }

   if (tstSrcPath == dstFrameListPath)
   {
      // COMMIT CHANGES

      // NEW: delete remaining files in MEDIA folder (e.g. MTIMetaData.ini)

      ///////////////////////////////////////////////////////////////////////
      // OK, on 2013-07-23 we decided that if we were committing a marked
      // range, then DO NOT DISCARD CHANGES OUTSIDE OF THE MARKED RANGE!!!!
      // So this code is completely bogus now and we have to figure out some
      // other place to try to remove ancillary files like MTIMetadata.ini
      ///////////////////////////////////////////////////////////////////////

//      CBinManager bm;
//      string srcClipImageFolder = AddDirSeparator(bm.getDirtyMediaFolderPath(srcclip->getClipPathName()));
//      string mask = srcClipImageFolder + "*";
//      WIN32_FIND_DATA w32findData;
//
//      HANDLE h = FindFirstFile(mask.c_str(), &w32findData);
//      if (h != INVALID_HANDLE_VALUE)
//      {
//         while (true)
//         {
//            // why do we get a file "." and a file ".." ?
//            string fName = srcClipImageFolder + w32findData.cFileName;
//
//            // delete the file
//            DeleteFile(fName.c_str());
//
//            // to next file
//            if (!FindNextFile(h, &w32findData))
//            {
//               break;
//            }
//         }
//
//         FindClose(h);
//      }

      //////////////////////////////////////////////////////////////////////////

      TRACE_3(errout << "HHHHHHHHHHHHHHHHHH START MOVING HISTORY HHHHHHHHHHHHHHHH");

      int moveFileFailCount = 0;
      for (int i = 0; i < framesToReplace; i++)
      {
         if (progressPercent != nullptr)
         {
            *progressPercent = (int)(100 * (i / (float)framesToReplace));
         }

         if (countDown != nullptr)
         {
            *countDown = framesToReplace - i;
         }

         int frmnum = (*frameIndexList)[i];

         // history file in the version clip
         string srcHistoryFilePath = getNewHistoryFilePath(srcclip, frmnum);

         // history file in the parent  clip
         string dstHistoryFilePath = getNewHistoryFilePath(dstclip, frmnum);

         if (srcHistoryFilePath != dstHistoryFilePath)
         {
            if (DoesFileExist(srcHistoryFilePath.c_str()))
            {
               // src hist exists
               if (DoesFileExist(dstHistoryFilePath.c_str()))
               {
                  string dstHistoryBackupPath = dstHistoryFilePath + "_bak";

                  // Need to remove the old parent history file.
                  // Just rename it for now so we could restor it if moving the
                  // noew file fails.
                  DeleteFile(dstHistoryBackupPath.c_str());
                  MoveFile(dstHistoryFilePath.c_str(), dstHistoryBackupPath.c_str());

                  if (!MoveFile(srcHistoryFilePath.c_str(), dstHistoryFilePath.c_str()))
                  {
                     // History file move failed... try to restore backup.
                     MoveFile(dstHistoryBackupPath.c_str(), dstHistoryFilePath.c_str());
                     ++moveFileFailCount;
                  }
                  else
                  {
                     DeleteFile(dstHistoryBackupPath.c_str());
                  }
               }
               else
               {
                  // Make sure the folder exists!
                  MakeDirectory(GetFilePath(dstHistoryFilePath.c_str()));
                  if (!MoveFile(srcHistoryFilePath.c_str(), dstHistoryFilePath.c_str()))
                  {
                     ++moveFileFailCount;
                  }
               }
            }
            else
            {
               // src history file = orphan & disappeared

               // WTF!! Why would we blindly delete the old history file???
               ////DeleteFile(dstHistoryFilePath.c_str());
            }
         }
      }

      if (moveFileFailCount > 0)
      {
         TRACE_0(errout << "ERROR: Failed to copy " << moveFileFailCount << " history file" << ((moveFileFailCount == 1) ? "!" : "s!"));
         return -1;
      }

      TRACE_3(errout << "HHHHHHHHHHHHHHHHHH DONE MOVING HISTORY HHHHHHHHHHHHHHHH");

      // for each frame of the version clip (incl outside of marks)
      // delete the frame's history file
      // remove the version's history directory
      // and the => frame list

      ///////////////////////////////////////////////////////////////////////
      // OK, on 2013-07-23 we decided that if we were committing a marked
      // range, then DO NOT DISCARD CHANGES OUTSIDE OF THE MARKED RANGE!!!!
      // So here we only try to delete the History folder if it no longer
      // contains any history files.
      ///////////////////////////////////////////////////////////////////////
      // UPDATE 2013-12-23: I think this may be slowing things down too much,
      // so for now just don't bother deleting the folder if there's anything
      // left in it!
      ///////////////////////////////////////////////////////////////////////

      // history folder in the version clip
      string historyFolderPath = RemoveDirSeparator(GetFilePath(getNewHistoryFilePath(srcclip, (*frameIndexList)[0])));
      BOOL historyFolderIsEmpty = PathIsDirectoryEmpty(historyFolderPath.c_str());

//      srcVideoFrameList = srcclip->getVideoFrameList(VIDEO_SELECT_NORMAL, FRAMING_SELECT_VIDEO);
//
//      srcInFrame = srcVideoFrameList->getInFrameIndex();
//      srcOutFrame = srcVideoFrameList->getOutFrameIndex();
//
//      for (int i = srcInFrame; i < srcOutFrame; i++)
//      {
//         // file path of history in version clip (whether it exists or not)
//         string versionHistoryFilePath = getNewHistoryFilePath(srcclip, i);
//
//         if (DoesFileExist(versionHistoryFilePath.c_str()))
//         {
//            // delete the version's history
//            //// NO NO NO NO !!! DeleteFile(versionHistoryFilePath.c_str());
//            //// Instead:
//            historyFolderIsEmpty = false;
//            break;
//         }
//      }

      if (historyFolderIsEmpty)
      {
         // no more history files -- remove directory
         RemoveDirectory(historyFolderPath.c_str());
      }

      TRACE_3(errout << "HHHHHHHHHHHHHHHHHH DONE CHECKING IF HISTORY FOLDER SHOULD BE REMOVED HHHHHHHHHHHHHHHH");

   }
   else if (tstDstPath == srcFrameListPath)
   {
      // DISCARD CHANGES
      for (int i = 0; i < framesToReplace; i++)
      {
         int frmnum = (*frameIndexList)[i];

         // history file in the version clip
         string versionHistoryFilePath = getNewHistoryFilePath(dstclip, frmnum);

         // restart version history with parent history
         DeleteFile(versionHistoryFilePath.c_str());
      }
   }

   if (progressPercent != nullptr)
   {
      *progressPercent = 100;
   }

   if (countDown != nullptr)
   {
      *countDown = 0;
   }

   return 0;
}
//------------------------------------------------------------------------------

bool historyExists(string const &frameListFileName)
{
   if (true) { // HISTORY_TYPE_MONOLITHIC ONLY

      string hdrFrameListFileName = frameListFileName + ".hed";
      if (DoesFileExist(hdrFrameListFileName)) {
         string resFrameListFileName = frameListFileName + ".rst";
         if (DoesFileExist(resFrameListFileName)) {
            string pxlFrameListFileName = frameListFileName + ".pix";
            if (DoesFileExist(pxlFrameListFileName)) {
               return(true);
           }
        }
      }
   }
   else {

      return(true);
   }

   return(true);
}

//-------------------------------------------------------------------------
//
// getNumberOfFixes - gets the number of fixes for a specified
//    frameListFileName (e.g. Clip001_vp0_f0) by appending the
//    file suffix (.rst), reading thru the records and counting
//    independent fixes.
//  Note: This assumes that the Tool user has properly set the
//    globalSaveNumber - this number is supposed to be incremented
//    per fix.
//
//  Creation Date: 8/5/2003
//  Author:        Michael Russell
//
//-------------------------------------------------------------------------
int CSaveRestore::getNumberOfFixes(
   ClipSharedPtr &clip,
   int *percentComplete,
   int *partialResult,
   int *continueCheckLHS,
   int *continueCheckRHS)
{
  CAutoThreadLocker lock(*getLock());

  //////////////////////////////////////////////////////////////////////////////

  if (percentComplete != nullptr)
     *percentComplete = 0;

  if (partialResult != nullptr)
     *partialResult = 0;

  CVideoFrameList *videoFrameList =
             clip->getVideoFrameList(VIDEO_SELECT_NORMAL, FRAMING_SELECT_VIDEO);

  int inFrame  = videoFrameList->getInFrameIndex();
  int outFrame = videoFrameList->getOutFrameIndex();

  CFrame *iniframe = videoFrameList->getBaseFrame(inFrame);
   MTIassert(iniframe != nullptr);
   if (iniframe == nullptr)
   {
      return -1;
   }

  // assume we have a legacy clip
  int hisType = HISTORY_TYPE_MONOLITHIC;

  CBinManager bm;
  string clipHistoryFolder = bm.getDirtyHistoryFolderPath(clip->getClipPathName());

  if (clipHistoryFolder.empty()) {

     clipHistoryFolder = videoFrameList->getHistoryFilePath(iniframe);
  }

  if (!clipHistoryFolder.empty()) {

     hisType = HISTORY_TYPE_FILE_PER_FRAME;
  }

  //////////////////////////////////////////////////////////////////////////////

  // init for counting fixes
  int iNumFixes = 0;

  if (hisType == HISTORY_TYPE_MONOLITHIC) {

     string frameListFileName = videoFrameList->getFrameListFileName();

     string     stResFileName;
     MTI_UINT64 u64ResFileLen = 0;

     // Build ".rst" filename
     stResFileName = frameListFileName + ".rst";

     TRACE_2(errout << "CSaveRestore::getNumberOfFixes() FN " << stResFileName
              << endl);

     if (stResFileName == "")
     {
       TRACE_0(errout << "CSaveRestore::getNumberOfFixes() FileName empty"
                << endl);
       return(HISTORY_ERROR_OPENING_RES_FILE);
     }

     // Check if File Exists - if clip has no history then it's normal for
     //   the file not to exist.
     if (!DoesFileExist(stResFileName))
     {
       TRACE_2(errout << "CSaveRestore::getNumberOfFixes() no such file"
                << endl);
       return(0);
     }

     // Determine File's length
     u64ResFileLen = GetFileLength(stResFileName);
     TRACE_2(errout << "CSaveRestore::getNumberOfFixes() file length: "
              << u64ResFileLen << endl);

     // Length must be a multiple of record size
     if ((u64ResFileLen % sizeof(RESTORE_RECORD)) != 0)
     {
       TRACE_0(errout << "CSaveRestore::getNumberOfFixes() bad file length: "
                 << u64ResFileLen << endl);
       return(HISTORY_ERROR_OPENING_RES_FILE);
     }

     // calculate the total number of fixes - this number is larger than user
     //   perception for clips which have hidden fields, and hence is not
     //   really used.  An optimization would be to use this number in
     //   the case where the clip does not have hidden fields.  In the case
     //   of a clip with hidden fields, when the user does a single fix,
     //   two entries are added to the history.  Consequently this simple
     //   calculation is therefore inflated in a manner that makes no sense
     //   to the user.
     int iTotFixes = u64ResFileLen / sizeof(RESTORE_RECORD);

     // calculate the number of fixes - weed out the ones which appear twice
     //   just because they are for hidden fields.
     RESTORE_RECORD *resBuffer             = nullptr;
     RESTORE_RECORD *lastResBuffer         = nullptr;
     size_t          resFileLoc;
     int             iLastGlobalSaveNumber = -1;
     int             iFDRes                = -1;

     // Allocation memory for these buffers
     resBuffer     = new RESTORE_RECORD;
     lastResBuffer = new RESTORE_RECORD;

     if ((resBuffer == nullptr) || (lastResBuffer == nullptr))
     {
       TRACE_0(errout << "ERROR: CSaveRestore::getNumberOfFixes() alloc failed"
                << endl);
       return(HISTORY_ERROR_OPENING_RES_FILE);
     }

     // Open .rst file
     if ((iFDRes = open(stResFileName.c_str(), O_BINARY|O_RDONLY)) == -1)
     {
       delete resBuffer;
       delete lastResBuffer;
       return(HISTORY_ERROR_OPENING_RES_FILE);
     }

     // Cycle thru each entry in the file in order to peruse fixes
	 for (resFileLoc = 0;
          resFileLoc < u64ResFileLen;
          resFileLoc += sizeof(RESTORE_RECORD))
    {
       // Continue?
       if (continueCheckLHS != nullptr && continueCheckRHS != nullptr
           && continueCheckLHS != continueCheckRHS)
       {
          // No.
          break;
       }

       // Read a Restore Record
       if (read(iFDRes, resBuffer, sizeof(RESTORE_RECORD))
			 != (int)sizeof(RESTORE_RECORD))
       {
         close(iFDRes);
         delete resBuffer;
         delete lastResBuffer;
         MTIassert(false);
         return(HISTORY_ERROR_READING_RES_FILE);
       }

       // Weed out fixes which were for hidden fields
       // and don't count fixes which have been GOV'd
       if ( (resBuffer->globalSaveNumber != iLastGlobalSaveNumber)&&
           ((resBuffer->restoreFlag&IS_INVALID) == 0)&&
           ((resBuffer->restoreFlag&IS_RESTORED) == 0) )
       {
         iNumFixes++;
         iLastGlobalSaveNumber = resBuffer->globalSaveNumber;

         if (partialResult != nullptr)
            *partialResult = iNumFixes;
       }
//       else        // if TRACE_2, then dump the records - for debugging purposes
//       {
//         if (lastResBuffer != nullptr)
//         {
//           TRACE_2(errout << "TTT Not Counting Fix - Probably Hidden Field" << endl);
//           TRACE_2(errout << "TTT lastResBuffer" << endl);
//           TRACE_2(dumpRestoreRecord(lastResBuffer));
//           TRACE_2(errout << "TTT resBuffer" << endl);
//           TRACE_2(dumpRestoreRecord(resBuffer));
//           TRACE_2(errout << endl);
//         }
//       }

       copyRestoreRecord(lastResBuffer, resBuffer);

       if (percentComplete != nullptr)
          *percentComplete = (100 * resFileLoc) / u64ResFileLen;
     }

     // Clean up
     close(iFDRes);
     delete resBuffer;
     delete lastResBuffer;

     TRACE_2(errout << "TTT Fixes " << iNumFixes << "/" << iTotFixes << endl);

  }
  else // hisType == HISTORY_TYPE_FILE_PER_FRAME
  {

     // for each per-frame history file do
     //    iNumUserFixes += fixes this history file
     // next file
     //
     int inFrame  = videoFrameList->getInFrameIndex();
     int outFrame = videoFrameList->getOutFrameIndex();

     for (int iframe = inFrame; iframe < outFrame; iframe++)
     {
        // Continue?
        if (continueCheckLHS != nullptr && continueCheckRHS != nullptr
           && *continueCheckLHS != *continueCheckRHS)
        {
           // No.
           break;
        }

        string historyFilePath = getHistoryFilePath(clip, iframe);

        iNumFixes += HistoryFileContainsFixes(historyFilePath);

        if (percentComplete != nullptr)
           *percentComplete = (100 * iframe) / outFrame;

        if (partialResult != nullptr)
           *partialResult = iNumFixes;
     }
  }

  if (percentComplete != nullptr)
     *percentComplete = 100;

  return(iNumFixes);
}

//-------------------------------------------------------------------------
//
// dumpRestoreRecord - use to dump out elements of a passed RESTORE_RECORD.
//
//  Creation Date: 8/13/2003
//  Author:        Michael Russell
//
//-------------------------------------------------------------------------
void CSaveRestore::dumpRestoreRecord (RESTORE_RECORD *resRecPtr)
{
  if (resRecPtr == 0)
  {
      TRACE_0(errout << "NO RESTORE RECORD TO DUMP!");
      return;
  }

  string stResFlag = (resRecPtr->restoreFlag & IS_INVALID)
                       ? "Invalid"
                       : (resRecPtr->restoreFlag & IS_RESTORED)
                          ? "Restored"
                          : (resRecPtr->restoreFlag == 0)
                             ? "Valid"
                             : "***Unknown***";

  TRACE_0(errout << "res->frameNum [" << offsetof(RESTORE_RECORD, frameNum) << "]        : " << resRecPtr->frameNum
            << endl);
  TRACE_0(errout << "   ->fieldIndex [" << offsetof(RESTORE_RECORD, fieldIndex) << "]      : " << resRecPtr->fieldIndex
            << endl);
  TRACE_0(errout << "   ->fieldNumber [" << offsetof(RESTORE_RECORD, fieldNumber) << "]     : " << resRecPtr->fieldNumber
            << endl);
  TRACE_0(errout << "   ->globalSaveNumber [" << offsetof(RESTORE_RECORD, globalSaveNumber) << "]: " << resRecPtr->globalSaveNumber
            << endl);
  TRACE_0(errout << "   ->saveTime [" << offsetof(RESTORE_RECORD, saveTime) << "]        : " << resRecPtr->saveTime
            << endl);
  TRACE_0(errout << "   ->toolCode [" << offsetof(RESTORE_RECORD, toolCode) << "]        : " << resRecPtr->toolCode
            << endl);
  TRACE_0(errout << "   ->restoreFlag [" << offsetof(RESTORE_RECORD, restoreFlag) << "]     : " << stResFlag << endl);
  TRACE_0(errout << "   ->extent.left [" << offsetof(RESTORE_RECORD, extent) << "]     : " << resRecPtr->extent.left
            << endl);
  TRACE_0(errout << "           .top       : " << resRecPtr->extent.top
            << endl);
  TRACE_0(errout << "           .right     : " << resRecPtr->extent.right
            << endl);
  TRACE_0(errout << "           .bottom    : " << resRecPtr->extent.bottom
            << endl);
  TRACE_0(errout << "   ->pxlFileByteOffset: [" << offsetof(RESTORE_RECORD, pxlFileByteOffset) << "]" << resRecPtr->pxlFileByteOffset
            << endl);
  TRACE_0(errout << "   ->pxlByteCount [" << offsetof(RESTORE_RECORD, pxlByteCount) << "]    : " << resRecPtr->pxlByteCount
            << endl);
  TRACE_0(errout << "SIZE: " << sizeof(*resRecPtr) << endl);
}

//-------------------------------------------------------------------------
//
// copyRestoreRecord - copy a RESTORE_RECORD.
//
//  Creation Date: 8/13/2003
//  Author:        Michael Russell
//
//-------------------------------------------------------------------------
void CSaveRestore::copyRestoreRecord (RESTORE_RECORD *rrResBufDst, RESTORE_RECORD *rrResBufSrc)
{
  // Return if either pointer is null
  if (rrResBufSrc == nullptr)
  {
    TRACE_0(errout << "CSaveRestore::copyRestoreRecord - nullptr Source" << endl);
    return;
  }

  if (rrResBufDst == nullptr)
  {
    TRACE_0(errout << "CSaveRestore::copyRestoreRecord - nullptr Destination"
              << endl);
    return;
  }

  // Copy the structure
  *rrResBufDst = *rrResBufSrc;
  /***********
  rrResBufDst->fieldIndex        = rrResBufSrc->fieldIndex;
  rrResBufDst->fieldNumber       = rrResBufSrc->fieldNumber;
  rrResBufDst->globalSaveNumber  = rrResBufSrc->globalSaveNumber;
  rrResBufDst->saveTime          = rrResBufSrc->saveTime;
  rrResBufDst->toolCode          = rrResBufSrc->toolCode;
  rrResBufDst->restoreFlag       = rrResBufSrc->restoreFlag;
  rrResBufDst->extent.left       = rrResBufSrc->extent.left;
  rrResBufDst->extent.top        = rrResBufSrc->extent.top;
  rrResBufDst->extent.right      = rrResBufSrc->extent.right;
  rrResBufDst->extent.bottom     = rrResBufSrc->extent.bottom;
  rrResBufDst->pxlFileByteOffset = rrResBufSrc->pxlFileByteOffset;
  rrResBufDst->pxlByteCount      = rrResBufSrc->pxlByteCount;
  ***********/

  return;
}

CAutoHistoryOpener::CAutoHistoryOpener(CSaveRestore &saveRestore,
                                       EHistoryFlushType flushType,
                                       int &retVal)
:CAutoThreadLocker(*(saveRestore.getLock()))
{
   retValPtr = &retVal;
   saveRestorePtr = &saveRestore;
   retVal = saveRestore.openForSave();
   if (retVal != 0)
   {
      MTImillisleep(100);
      retVal = saveRestore.openForSave();
      if (retVal == 0)
      {
         TRACE_0(errout << "CAutoHistoryOpener needed one retry");
      }
   }

   if (retVal != 0)
   {
      MTImillisleep(100);
      retVal = saveRestore.openForSave();
      if (retVal == 0)
      {
         TRACE_0(errout << "CAutoHistoryOpener worked on second retry");
      }
   }

   if (retVal != 0)
   {
      TRACE_0(errout << "CAutoHistoryOpener FAILED three times!");
   }
   historyFlushType = (retVal != 0) ? FLUSH_TYPE_NONE : flushType;
}

// this is Mike's very clever auto history opener - it opens
// when it's constructed and closes when it's destroyed on
// leaving a block's scope

CAutoHistoryOpener::CAutoHistoryOpener(CSaveRestore &saveRestore,
                                       EHistoryFlushType flushType,
                                       int frame,
                                       int field,
                                       RECT *rect,
                                       POINT *points,
                                       BEZIER_POINT *bezpoints,
                                       int &retVal)
:CAutoThreadLocker(*(saveRestore.getLock()))
{
   retValPtr = &retVal;
   saveRestorePtr = &saveRestore;
   retVal = saveRestore.openForRestore(frame, field, rect, points, bezpoints);
   historyFlushType = (retVal != 0) ? FLUSH_TYPE_NONE : flushType;
}


CAutoHistoryOpener::CAutoHistoryOpener(CSaveRestore &saveRestore,
                                       EHistoryFlushType flushType,
                                       int frame,
                                       int field,
                                       CPixelRegionList& pxlrgns,
                                       int &retVal)
:CAutoThreadLocker(*(saveRestore.getLock()))
{
   retValPtr = &retVal;
   saveRestorePtr = &saveRestore;
   retVal = saveRestore.openForRestore(frame, field, pxlrgns);
   historyFlushType = (retVal != 0) ? FLUSH_TYPE_NONE : flushType;
}

CAutoHistoryOpener::~CAutoHistoryOpener()
{
   switch (historyFlushType) {

     case FLUSH_TYPE_SAVE:

       *retValPtr = saveRestorePtr->flushSave();

     break;

     case FLUSH_TYPE_RESTORE:

        *retValPtr = saveRestorePtr->flushRestore();

     break;

     case FLUSH_TYPE_NONE:

        // Do nothing, duh

     break;
   }

   // QQQ do something sensible with the retVal if non-zero at this point ?
   // Always always ALWAYS close the history files!!
   *retValPtr = saveRestorePtr->closeHistoryFiles();
}

// allow overriding of flushing when there's an error
void CAutoHistoryOpener::dontFlush()
{
   historyFlushType = FLUSH_TYPE_NONE;
}
