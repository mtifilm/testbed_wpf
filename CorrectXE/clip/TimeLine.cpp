/*
	File:	TimeLine.cpp
	By:	Kevin Manbeck
	Date:	March 16, 2003

	The TimeLine class is used to hold many tracks of data that will be
	displayed in the time line GUI.

	See TimeLine.h for detailed description.
*/

#include "ClipAPI.h"
#include "TimeLine.h"
#include "err_metadata.h"
#include "TrackStrategy.h"
#include "ClipTrackStrategy.h"
#include <cmath>


// Constructor added for quick event lookup.
CTimeLine::CTimeLine (const ClipSharedPtr &clipArg)
{
  Init ();
  setClip(clipArg);
  ResetEvent();
}

CTimeLine::CTimeLine ()
{
  Init ();
}  /* CTimeLine */

CTimeLine::~CTimeLine ()
{
  Free ();
}  /* ~CTimeLine */

void CTimeLine::Init ()
{
  strClipName = "";
  app = NULL;
  ullpEvent = NULL;
  ucpRawFrame = NULL;
  iNFieldAudio = 0;
  iNChannelAudio = 0;
  iSampleSizeAudio = 0;
  iAllocAudio = 0;
  iAllocEvent = 0;

  clip = nullptr;
  vflp = 0;
  aflp = 0;
  ttpInkNumber = 0;
  ttpKeykode = 0;
  ttpAudioLTC = 0;
  ttpEvent = 0;
  ttpAudioProxy = 0;

  iFrameRateAudioLTC = 30;
  iScale = 2;
  iFramingSelected = 0; // = video
  iVideoSelected = 0; // proxy 0

  pTrackStrategy = 0;
  pDefaultTrackStrategy = 0;

  animateIFPtr = 0;

}  /* Init */

void CTimeLine::Free ()
{
  // free the audio memory
  FreeAudio ();

  free (ullpEvent);
  ullpEvent = NULL;

  // close the clip
  CBinManager binMgr;
  if (clip)
   {
    binMgr.closeClip (clip);
    clip = nullptr;
   }

  if (pDefaultTrackStrategy != 0)
   {
    delete pDefaultTrackStrategy;
    pDefaultTrackStrategy = 0;
   }

  Init ();
}  /* Free */

void CTimeLine::FreeAudio ()
{
  if (app)
   {
    for (int iFrame = 0; iFrame < iAllocAudio; iFrame++)
     {
      if (app[iFrame].sppMinValue)
       {
        for (int iSubFrame = 0; iSubFrame < AUDIO_PROXY_PER_FRAME; iSubFrame++)
         {
          free (app[iFrame].sppMinValue[iSubFrame]);
          app[iFrame].sppMinValue[iSubFrame] = NULL;
         }
       }
      if (app[iFrame].sppMaxValue)
       {
        for (int iSubFrame = 0; iSubFrame < AUDIO_PROXY_PER_FRAME; iSubFrame++)
         {
          free (app[iFrame].sppMaxValue[iSubFrame]);
          app[iFrame].sppMaxValue[iSubFrame] = NULL;
         }
       }

      free (app[iFrame].spMinValue);
      app[iFrame].spMinValue = NULL;
      free (app[iFrame].spMaxValue);
      app[iFrame].spMaxValue = NULL;
     }

    iAllocAudio = 0;
   }
  free (app);
  app = NULL;
  free (ucpRawFrame);
  ucpRawFrame = NULL;
 }  /* FreeAudio */

int CTimeLine::Alloc ()
{

  int iRet;

  iRet = AllocAudio ();
  if (iRet)
    return iRet;

  return 0;
}  /* Alloc */

int CTimeLine::AllocAudio ()
{

  FreeAudio();

  if (aflp == 0 || pTrackStrategy == 0)
   {
    return 0;
   }

  // allocate storage for the audio proxy data
  if (aflp->getTotalFrameCount())
   {
    app = (TL_AUDIO_PROXY *) malloc (aflp->getTotalFrameCount() *
		sizeof (TL_AUDIO_PROXY));
    if (app == NULL)
     {
      return ERR_TIMELINE_MALLOC;
     }

    iAllocAudio = aflp->getTotalFrameCount();

    for (int iFrame = 0; iFrame < iAllocAudio; iFrame++)
     {
      app[iFrame].sppMinValue = NULL;
      app[iFrame].sppMaxValue = NULL;
      app[iFrame].spMinValue = NULL;
      app[iFrame].spMaxValue = NULL;
      app[iFrame].bInitialized = false;
     }

    for (int iFrame = 0; iFrame < iAllocAudio; iFrame++)
     {
      app[iFrame].sppMinValue = (MTI_INT16 **) malloc (AUDIO_PROXY_PER_FRAME *
                sizeof (MTI_INT16 *));
      if (app[iFrame].sppMinValue == NULL)
       {
        return ERR_TIMELINE_MALLOC;
       }

      for (int iSubFrame = 0; iSubFrame < AUDIO_PROXY_PER_FRAME; iSubFrame++)
       {
        app[iFrame].sppMinValue[iSubFrame] = NULL;
       }

      for (int iSubFrame = 0; iSubFrame < AUDIO_PROXY_PER_FRAME; iSubFrame++)
       {
        app[iFrame].sppMinValue[iSubFrame] = (MTI_INT16 *) malloc (
                iNChannelAudio * sizeof (MTI_INT16));
        if (app[iFrame].sppMinValue[iSubFrame] == NULL)
         {
          return ERR_TIMELINE_MALLOC;
         }
       }

      app[iFrame].sppMaxValue = (MTI_INT16 **) malloc (AUDIO_PROXY_PER_FRAME *
                sizeof (MTI_INT16 *));
      if (app[iFrame].sppMaxValue == NULL)
       {
        return ERR_TIMELINE_MALLOC;
       }

      for (int iSubFrame = 0; iSubFrame < AUDIO_PROXY_PER_FRAME; iSubFrame++)
       {
        app[iFrame].sppMaxValue[iSubFrame] = NULL;
       }

      for (int iSubFrame = 0; iSubFrame < AUDIO_PROXY_PER_FRAME; iSubFrame++)
       {
        app[iFrame].sppMaxValue[iSubFrame] = (MTI_INT16 *) malloc (
                iNChannelAudio * sizeof (MTI_INT16));
        if (app[iFrame].sppMaxValue[iSubFrame] == NULL)
         {
          return ERR_TIMELINE_MALLOC;
         }
       }

      app[iFrame].spMinValue = (MTI_INT16 *) malloc (iNChannelAudio *
                sizeof (MTI_INT16));
      if (app[iFrame].spMinValue == NULL)
       {
        return ERR_TIMELINE_MALLOC;
       }

      app[iFrame].spMaxValue = (MTI_INT16 *) malloc (iNChannelAudio *
                sizeof (MTI_INT16));
      if (app[iFrame].spMaxValue == NULL)
       {
        return ERR_TIMELINE_MALLOC;
       }
     }
   }

  ClearAudioProxy ();

  // allocate storage for IO buffer
  int iNAudioByte = aflp->getAudioFormat()->getChannelCount() *
	(aflp->getAudioFormat()->getSampleDataSize() / 8) *
	std::ceil (aflp->getAudioFormat()->getSamplesPerField());

  ucpRawFrame = (MTI_UINT8 *) malloc (iNAudioByte * iNFieldAudio);
  if (ucpRawFrame == NULL)
   {
    return ERR_TIMELINE_MALLOC;
   }

  return 0;
}  /* AllocAudio */


// this method now includes opening the AudioProxy TimeTrack
//
int CTimeLine::setClipName (const string &strClipNameArg, int iVideoSelect,
                int iAudioSelect, int iFramingSelect)
{
  string strBin, strClip;

  // remove any previous memory
  Free ();

  strClipName = strClipNameArg;

  // open the clip
  CBinManager bmBinMgr;

  int iRet = bmBinMgr.splitClipFileName (strClipName, strBin, strClip);
  if (iRet)
   {
    char caMessage[256];
    sprintf (caMessage, "Unable to parse clip name:  %s  because: %d",
        strClipName.c_str(), iRet);
    theError.set (ERR_TIMELINE_CLIPNAME, caMessage);
    return ERR_TIMELINE_CLIPNAME;
   }

  auto clipSharedPtr = bmBinMgr.openClip (strBin, strClip, &iRet);
  if (iRet)
   {
    char caMessage[256];
    sprintf (caMessage, "Unable to open the clip:  %s  because: %d",
                strClipName.c_str(), iRet);
    theError.set (ERR_TIMELINE_OPENCLIP, caMessage);
    return iRet;
   }

   return setClip(clipSharedPtr, iVideoSelect, iAudioSelect, iFramingSelect);
}

int CTimeLine::setClip(const ClipSharedPtr &clipArg, int iVideoSelect,
                int iAudioSelect, int iFramingSelect)
{
   clip = clipArg;

  // remember what framing and video proxy were selected
  iFramingSelected = iFramingSelect;
  iVideoSelected = iVideoSelect;

  // get pointer to the video track
  vflp = clip->getVideoFrameList(iVideoSelect, iFramingSelect);
  if (vflp)
   {
    // force the video track to load
    vflp->getTotalFrameCount();
   }

  // get pointer to audio track
  CAudioTrack *atp = clip->getAudioTrack (iAudioSelect);
  if (atp)
   {
    aflp = atp->getAudioFrameList(iAudioSelect);
    if (aflp)
	 {
      // force the audio track to load
      iNFieldAudio = aflp->getAudioFormat()->getFieldCount();
      iNChannelAudio = aflp->getAudioFormat()->getChannelCount();
      iSampleSizeAudio = aflp->getAudioFormat()->getSampleDataSize();
     }
   }

  for (int iTrack = 0; iTrack < clip->getTimeTrackCount(); iTrack++)
   {
    // get pointer to key kode track
    if (clip->getTimeTrack(iTrack)->getTimeType() == TIME_TYPE_KEYKODE)
     {
      if (clip->getTimeTrack (iTrack)->getAppId() == "InkNumber")
       {
        ttpInkNumber = clip->getTimeTrack(iTrack);
        // force keykode track to load
        ttpInkNumber->getTotalFrameCount();
       }
      else
       {
        ttpKeykode = clip->getTimeTrack(iTrack);
        // force keykode track to load
        ttpKeykode->getTotalFrameCount();
       }
     }

    // get pointer to Audio Timecode
    if (clip->getTimeTrack(iTrack)->getTimeType() == TIME_TYPE_SMPTE_TIMECODE)
     {
      ttpAudioLTC = clip->getTimeTrack (iTrack);
      // force audio timecode track to load
      ttpAudioLTC->getTotalFrameCount();
     }

    // get pointer to Event track
    if (clip->getTimeTrack(iTrack)->getTimeType() == TIME_TYPE_EVENT)
     {
      ttpEvent = clip->getTimeTrack (iTrack);
      // force event timecode track to load
      ttpEvent->getTotalFrameCount();
     }

    // get pointer to Audio Proxy track
    if (clip->getTimeTrack(iTrack)->getTimeType() == TIME_TYPE_AUDIO_PROXY)
     {
      ttpAudioProxy = clip->getTimeTrack (iTrack);
      // force audio proxy track to load
      ttpAudioProxy->getTotalFrameCount();
     }
   }

  // install the default track strategy
  if (pDefaultTrackStrategy != 0)
   {
    delete pDefaultTrackStrategy;
   }

  pDefaultTrackStrategy = new CClipTrackStrategy(clip, iVideoSelect, iAudioSelect, iFramingSelect);
  int iRet = setTrackStrategy(pDefaultTrackStrategy);
  if (iRet)
    return iRet;

  return 0;
}

int CTimeLine::setTrackStrategy (CTrackStrategy *newTrackStrategy)
{
  pTrackStrategy = newTrackStrategy;

  int iRet;
  iRet = Alloc ();
  if (iRet)
   {
    char caMessage[256];
    sprintf (caMessage, "Unable to allocate memory for track");
    theError.set (ERR_TIMELINE_MALLOC, caMessage);
    return ERR_TIMELINE_MALLOC;
   }

  return 0;
}  /* setTrackStrategy */

void CTimeLine::setScale (int iScaleArg)
{
  iScale = iScaleArg;
}  /* setScale */

int  CTimeLine::getScale(void)
{
   return iScale;
}

string CTimeLine::getAudioLTC (float fFrameArg, bool bRound, int iOffset)
{
  string strReturn;

  if (pTrackStrategy == 0)
   {
    strReturn = "No track loaded";
    return strReturn;
   }

  bool bInterpolated = pTrackStrategy->getAudioLtc (fFrameArg, bRound, iOffset,
		strReturn);

  if (bInterpolated == true)
   {
    // time line does not use interpolated values
    strReturn = "Not available";
   }

  return strReturn;
}  /* getAudioLTC */

string CTimeLine::getClipAudioLTC (int newClipFrame, bool bRound, int iOffset)
{
  string strReturn;
  int iRet;

  if (ttpAudioLTC == 0 || newClipFrame < 0 ||
		newClipFrame >= ttpAudioLTC->getTotalFrameCount())
   {
    strReturn =  "Not available";
   }
  else
   {
    char caTimecode[32];
    char caPartial[8];
    CSMPTETimecode stcTmp;
    float fPartialFrame;
    iRet = ttpAudioLTC->getSMPTETimecode(newClipFrame, stcTmp, fPartialFrame);
    if (iRet != 0)
     {
      strReturn = "Not available";
     }

    else if (stcTmp.getRawTimecode() == 0xFFFFFFFF)
     {
      // not recorded yet
      strReturn = "Not recorded";
     }
    else
     {
      CTimecode tc = stcTmp.getTimecode(iFrameRateAudioLTC);
      if (tc.getFrameCount() == 0)
       {
        strReturn = "Not recorded";
       }
      else
       {

        while (fPartialFrame < 0)
         {
          tc--;
          fPartialFrame += 1.;
         }

        while (fPartialFrame > 1.)
         {
          tc++;
          fPartialFrame -= 1.;
         }

        if (bRound && fPartialFrame > .5)
         {
          tc++;
          fPartialFrame = 0.;
         }

        while (iOffset > 0)
         {
          tc++;
          iOffset--;
         }

        while (iOffset < 0)
         {
          tc--;
          iOffset++;
         }

        tc.getTimeASCII(caTimecode);
        sprintf (caPartial, ".%.2d", (int)(100. * fPartialFrame));
        strReturn = caTimecode;

        if (bRound == false)
         {
          strReturn += caPartial;
         }
       }
     }
   }

  return strReturn;
}  /* getClipAudioLTC */

string CTimeLine::getTelecineKeykode (int iFrameArg,
		EKeykodeFormat iFormat, int iPerfOffset,
		EKeykodeFilmDimension *ipFilmDimension)
{
  string strReturn;

  if (ipFilmDimension)
    *ipFilmDimension = KEYKODE_FILM_DIMENSION_INVALID;

  if (pTrackStrategy == 0)
   {
    strReturn = "No track loaded";
    return strReturn;
   }

  bool bInterpolated;
  bInterpolated = pTrackStrategy->getTelecineKeykode (iFrameArg,
		iFormat, iPerfOffset, strReturn, ipFilmDimension);

  if (bInterpolated)
   {
    // time line does not use interpolated values
    strReturn = "Not available";
   }

  return strReturn;
}  /* getTelecineKeykode */

string CTimeLine::getClipTelecineKeykode (int newClipFrame)
{
  string strReturn;
  int iRet;

  if (ttpKeykode == 0 || newClipFrame < 0 ||
		newClipFrame >= ttpKeykode->getTotalFrameCount())
   {
    strReturn =  "Not available";
   }
  else
   {
    CMTIKeykode kkTmp;
    iRet = ttpKeykode->getKeykode(newClipFrame, kkTmp);
    if (iRet != 0)
     {
      strReturn =  "Not available";
     }

    else
     {
      strReturn = kkTmp.getKeykodeStr();
     }
   }

  return strReturn;
}  /* getClipTelecineKeykode */

string CTimeLine::getTelecineTC (int iFrameArg)
{
  string strReturn;

  if (pTrackStrategy == 0)
   {
    strReturn = "No track loaded";
    return strReturn;
   }

  pTrackStrategy->getTelecineTC (iFrameArg, strReturn);

  return strReturn;
}  /* getTelecineTC */

string CTimeLine::getClipTelecineTC (int newClipFrame)
{
  string strReturn;

  strReturn = "Not available";

  return strReturn;
}  /* getClipTelecineTC */


string CTimeLine::getVideoTC (int iFrameArg)
{
  string strReturn;


  if (pTrackStrategy == 0)
   {
    strReturn = "No track loaded";
    return strReturn;
   }

  int iFrame, iPartial;
  pTrackStrategy->getVideoClipFrame (iFrameArg, iFrame, iPartial);

  if (vflp == 0 || iFrame < 0 ||
		iFrame >= vflp->getTotalFrameCount())
   {
    strReturn = "Not available";
   }
  else
   {
    vflp->getTimecodeForFrameIndex(iFrame).getTimeString(strReturn);
   }

  return strReturn;
}  /* getVideoTC */

bool CTimeLine::getVideoCTimecode(int iFrameArg, CTimecode &ctc)
{
  if (pTrackStrategy == 0)
    return false;

  int iFrame, iPartial;
  pTrackStrategy->getVideoClipFrame (iFrameArg, iFrame, iPartial);

  if (vflp == 0 || iFrame < 0 ||
		iFrame >= vflp->getTotalFrameCount())
   {
    return false;
   }
  else
   {
     ctc = vflp->getTimecodeForFrameIndex(iFrame);
     return true;
   }

}  /* getVideoCTimecode */

// This returns -1 if the TC is outside the clip in/out (e.g in handles)
int CTimeLine::getVideoFrameIndex(CTimecode ctc)
{
  if (pTrackStrategy == 0 || vflp == 0)
    return -1;

  int iFrame = vflp->getFrameIndex(ctc);

  // getFrameIndex() might return bogus index!!
  if (!vflp->isFrameIndexUserMaterial(iFrame))
    iFrame = -1;

  return iFrame;

}  /* getVideoFrameIndex */

// This returns true if there is a valid audio signal
bool CTimeLine::isAudioValid(int iChannel, int iFrame)
{
  return !(pTrackStrategy == 0 ||
	   pTrackStrategy->getAudioClipFrame (iFrame) < 0 ||
           iChannel < 0 ||
           iChannel >= iNChannelAudio);
}

void CTimeLine::getAudioProxy (int iChannel, int iFrame, MTI_INT16 &sMin,
        MTI_INT16 &sMax)
{
  if (
	pTrackStrategy == 0 ||
        pTrackStrategy->getAudioClipFrame (iFrame) < 0 ||
        iChannel < 0 ||
        iChannel >= iNChannelAudio
     )
   {
    sMin = 0;
    sMax = 0;
    return;
   }

  ReadAudio (iFrame, (iFrame+1));

  // set the return values
  sMin = app[iFrame].spMinValue[iChannel];
  sMax = app[iFrame].spMaxValue[iChannel];

  // Avoid out of bounds errors
  sMin = std::max((int)sMin*iScale, -32768);
  sMax = std::min((int)sMax*iScale, 32767);

  return;
}  /* getAudioProxy */

TL_AUDIO_PROXY * CTimeLine::getAudioProxyPointer (int iFrame)
{

  if (
	pTrackStrategy == 0 ||
	pTrackStrategy->getAudioClipFrame (iFrame) < 0
     )
   {
    return NULL;
   }

  ReadAudio (iFrame, (iFrame+1));

  // set the return values
  return app+pTrackStrategy->getAudioClipFrame (iFrame);
}  /* getAudioProxyPointer */

void CTimeLine::getAudioProxy (int iChannel, float fFrameStart,
		float fFrameStop, MTI_INT16 &sMin, MTI_INT16 &sMax)
{
  int iFrameStart, iSubFrameStart, iFrameStop, iSubFrameStop;

  if (
	pTrackStrategy == 0 ||
	fFrameStart >= fFrameStop ||
        iChannel < 0 ||
        iChannel >= iNChannelAudio
     )
   {
    sMin = 0;
    sMax = 0;
    return;
   }

  iFrameStart = (int)fFrameStart;
  iSubFrameStart = (int)((fFrameStart - (float)iFrameStart) *
		(float)AUDIO_PROXY_PER_FRAME) / AUDIO_PROXY_PER_FRAME;
  iFrameStop = (int)fFrameStop;
  iSubFrameStop = (int)((fFrameStop - (float)iFrameStop) *
		(float)AUDIO_PROXY_PER_FRAME) / AUDIO_PROXY_PER_FRAME;

  ReadAudio (iFrameStart, iFrameStop);

  // initialize the return values
  sMin = app[iFrameStart].sppMinValue[iSubFrameStart][iChannel];
  sMax = app[iFrameStart].sppMaxValue[iSubFrameStart][iChannel];

  if (iFrameStart == iFrameStop)
   {
    //  loop from SubFrameStart to SubFrameStop
    for (int iSubFrame = iSubFrameStart+1; iSubFrame < iSubFrameStop;
		iSubFrame++)
     {
      if (sMin > app[iFrameStart].sppMinValue[iSubFrame][iChannel])
        sMin = app[iFrameStart].sppMinValue[iSubFrame][iChannel];
      if (sMax < app[iFrameStart].sppMaxValue[iSubFrame][iChannel])
        sMax = app[iFrameStart].sppMaxValue[iSubFrame][iChannel];
     }
   }
  else
   {
    //  loop over the rest of the sub frames in Start
    for (int iSubFrame = iSubFrameStart+1; iSubFrame < AUDIO_PROXY_PER_FRAME;
		iSubFrame++)
     {
      if (sMin > app[iFrameStart].sppMinValue[iSubFrame][iChannel])
        sMin = app[iFrameStart].sppMinValue[iSubFrame][iChannel];
      if (sMax < app[iFrameStart].sppMaxValue[iSubFrame][iChannel])
        sMax = app[iFrameStart].sppMaxValue[iSubFrame][iChannel];
     }

    //  loop over the sub frames in Stop
    for (int iSubFrame = 0; iSubFrame < iSubFrameStop; iSubFrame++)
     {
      if (sMin > app[iFrameStop].sppMinValue[iSubFrame][iChannel])
        sMin = app[iFrameStop].sppMinValue[iSubFrame][iChannel];
      if (sMax < app[iFrameStop].sppMaxValue[iSubFrame][iChannel])
        sMax = app[iFrameStop].sppMaxValue[iSubFrame][iChannel];
     }
   }

  // loop over all frames between Start and Stop
  for (int iFrame = iFrameStart+1; iFrame < iFrameStop; iFrame++)
   {
    if (sMin > app[iFrame].spMinValue[iChannel])
      sMin = app[iFrame].spMinValue[iChannel];
    if (sMax < app[iFrame].spMaxValue[iChannel])
      sMax = app[iFrame].spMaxValue[iChannel];
   }


  return;
}  /* getAudioProxy */


int CTimeLine::ReadOneFrame (int iFrame)
{
  int iRet=0;
  int iAudioFrame;
  int iNSample = 0;

  if (aflp == 0 || pTrackStrategy == 0)
   {
    return iNSample;
   }

  int iFileHandle = aflp->getParentTrack()->CreateNewFileHandle();
  if (iFileHandle == -1)
   {
    return 0;
   }

  iAudioFrame = pTrackStrategy->getAudioClipFrame (iFrame);
  if (iAudioFrame >= 0)
   {
    iNSample = aflp->getSampleCountAtFrame (iAudioFrame);
    iRet = aflp->readMediaBySample (iAudioFrame, 0, iNSample, ucpRawFrame,
		iFileHandle);
    if (iRet)
     {
      aflp->getParentTrack()->ReleaseFileHandle (iFileHandle);
      return 0;
     }
   }

  aflp->getParentTrack()->ReleaseFileHandle (iFileHandle);
  return iNSample;
}  /* ReadOneFrame */

void CTimeLine::ReadAudio (int iFrameStart, int iFrameStop)
{
  int iReadCount = 0;
  int iRet=0;
  int iFrame, iAudioFrame;
  int iNSample;


  if (aflp == 0 || pTrackStrategy == 0)
   {
    return;
   }

  for (int iFrame = iFrameStart; iFrame <= iFrameStop; iFrame++)
   {
    iAudioFrame = pTrackStrategy->getAudioClipFrame (iFrame);

    if (
	iAudioFrame < iAllocAudio &&
	app[iAudioFrame].bInitialized == false &&
	iReadCount < MAX_READ_PER_CALL &&
	iAudioFrame >= 0
       )
     {

    if (ttpAudioProxy != NULL) // init arrays from audio proxy timetrack
     {
      // get the pointer to the current audio proxy frame
      CAudioProxyFrame *proxyFrame =
                                 ttpAudioProxy->getAudioProxyFrame(iAudioFrame);

      if (proxyFrame == NULL) // initialize proxy for this frame to zero
       {
        for (int iSubFrame = 0; iSubFrame < AUDIO_PROXY_PER_FRAME; iSubFrame++)
        for (int iChannel = 0; iChannel < iNChannelAudio; iChannel++)
         {
          app[iAudioFrame].sppMinValue[iSubFrame][iChannel] = 0;
          app[iAudioFrame].sppMaxValue[iSubFrame][iChannel] = 0;
         }
        for (int iChannel = 0; iChannel < iNChannelAudio; iChannel++)
         {
          app[iAudioFrame].spMinValue[iChannel] = 0;
          app[iAudioFrame].spMaxValue[iChannel] = 0;
         }
       }
      else // initialize proxy from timetrack's proxyFrame
       {
        // copy .sppMinValues and .sppMaxValues right out of proxy frame
        MTI_INT16 thisMin, thisMax;
        for (int iSubFrame = 0; iSubFrame < AUDIO_PROXY_PER_FRAME; iSubFrame++)
        for (int iChannel = 0; iChannel < iNChannelAudio; iChannel++)
         {
          proxyFrame->getMinMaxAudio(iSubFrame, iChannel, &thisMin, &thisMax);
          app[iAudioFrame].sppMinValue[iSubFrame][iChannel] = thisMin,
          app[iAudioFrame].sppMaxValue[iSubFrame][iChannel] = thisMax;
         }

        // use the values just copied to get .spMinValue and .spMaxValue
        MTI_INT16 *spMinValue = app[iAudioFrame].spMinValue;
        MTI_INT16 *spMaxValue = app[iAudioFrame].spMaxValue;
        for (int iChannel = 0; iChannel < iNChannelAudio; iChannel++)
         {
          spMinValue[iChannel] = app[iAudioFrame].sppMinValue[0][iChannel];
          spMaxValue[iChannel] = app[iAudioFrame].sppMaxValue[0][iChannel];
         }

        for (int iSubFrame = 1; iSubFrame < AUDIO_PROXY_PER_FRAME; iSubFrame++)
         {
          MTI_INT16 *sppMinValue = app[iAudioFrame].sppMinValue[iSubFrame];
          MTI_INT16 *sppMaxValue = app[iAudioFrame].sppMaxValue[iSubFrame];

          for (int iChannel = 0; iChannel < iNChannelAudio; iChannel++)
           {
            if (spMinValue[iChannel] > sppMinValue[iChannel])
         	   spMinValue[iChannel] = sppMinValue[iChannel];
            if (spMaxValue[iChannel] < sppMaxValue[iChannel])
            	spMaxValue[iChannel] = sppMaxValue[iChannel];
           }
         }
       }
     }
    else // no audio proxy timetrack - init arrays from main audio track
     {
      int iFileHandle = aflp->getParentTrack()->CreateNewFileHandle();
      if (iFileHandle != -1)
       {
        iNSample = aflp->getSampleCountAtFrame (iAudioFrame);
        iRet = aflp->readMediaBySample (iAudioFrame, 0, iNSample, ucpRawFrame,
		iFileHandle);
        aflp->getParentTrack()->ReleaseFileHandle(iFileHandle);
       }
      else
       {
        iRet = -1;
       }
      if (iRet)
       {
        for (int iSubFrame = 0; iSubFrame < AUDIO_PROXY_PER_FRAME; iSubFrame++)
        for (int iChannel = 0; iChannel < iNChannelAudio; iChannel++)
         {
          app[iAudioFrame].sppMinValue[iSubFrame][iChannel] = 0;
          app[iAudioFrame].sppMaxValue[iSubFrame][iChannel] = 0;
         }
        for (int iChannel = 0; iChannel < iNChannelAudio; iChannel++)
         {
          app[iAudioFrame].spMinValue[iChannel] = 0;
          app[iAudioFrame].spMaxValue[iChannel] = 0;
         }
       }
      else
       {
        MTI_INT16 *spRawData = (MTI_INT16 *)ucpRawFrame;
        iNSample = aflp->getSampleCountAtFrame (iAudioFrame);
        float fSamplePerSubFrame = (float)iNSample /
                                     (float)AUDIO_PROXY_PER_FRAME;
        for (int iSubFrame = 0; iSubFrame < AUDIO_PROXY_PER_FRAME; iSubFrame++)
         {
          int iSampleStart = (float)iSubFrame * fSamplePerSubFrame;
          int iSampleStop = (float)(iSubFrame+1) * fSamplePerSubFrame;

          MTI_INT16 *sppMinValue = app[iAudioFrame].sppMinValue[iSubFrame];
          MTI_INT16 *sppMaxValue = app[iAudioFrame].sppMaxValue[iSubFrame];

          for (int iChannel = 0; iChannel < iNChannelAudio; iChannel++)
           {
            sppMinValue[iChannel] = *spRawData;
            sppMaxValue[iChannel] = *spRawData;
            spRawData++;
           }
          for (int iSample = iSampleStart+1; iSample < iSampleStop; iSample++)
           {
            for (int iChannel = 0; iChannel < iNChannelAudio; iChannel++)
             {
              if (sppMinValue[iChannel] > *spRawData)
                sppMinValue[iChannel] = *spRawData;
              if (sppMaxValue[iChannel] < *spRawData)
                sppMaxValue[iChannel] = *spRawData;
              spRawData++;
             }
           }
         }
        MTI_INT16 *spMinValue = app[iAudioFrame].spMinValue;
        MTI_INT16 *spMaxValue = app[iAudioFrame].spMaxValue;
        for (int iChannel = 0; iChannel < iNChannelAudio; iChannel++)
         {
          spMinValue[iChannel] = app[iAudioFrame].sppMinValue[0][iChannel];
          spMaxValue[iChannel] = app[iAudioFrame].sppMaxValue[0][iChannel];
         }

        for (int iSubFrame = 1; iSubFrame < AUDIO_PROXY_PER_FRAME; iSubFrame++)
         {
          MTI_INT16 *sppMinValue = app[iAudioFrame].sppMinValue[iSubFrame];
          MTI_INT16 *sppMaxValue = app[iAudioFrame].sppMaxValue[iSubFrame];

          for (int iChannel = 0; iChannel < iNChannelAudio; iChannel++)
           {
            if (spMinValue[iChannel] > sppMinValue[iChannel])
         	   spMinValue[iChannel] = sppMinValue[iChannel];
            if (spMaxValue[iChannel] < sppMaxValue[iChannel])
            	spMaxValue[iChannel] = sppMaxValue[iChannel];
           }
         }
       }
     }
     app[iAudioFrame].bInitialized = true;
     iReadCount++;
     }
   }

}  /* ReadAudio */

MTI_UINT64 CTimeLine::getEventFlag (int iFrameArg)
{
  MTI_UINT64 ullRet = EVENT_NONE;


  if (pTrackStrategy == 0 || ullpEvent == 0)
   {
    return ullRet;
   }

  if (iFrameArg >= 0 && iFrameArg < iAllocEvent)
   {
    ullRet |= ullpEvent[iFrameArg];
   }

  return ullRet;
}  /* getEventFlag */

int CTimeLine::getNextEvent (int iFrame, MTI_UINT64 ullFlag)
{
  int iRet;

  iRet = iFrame;

  // if possible use the list of LTC breaks
  if ( ullFlag == EVENT_AUD_LTC_BREAK || ullFlag == EVENT_AUD_BOUNDARY ||
	ullFlag == (EVENT_AUD_LTC_BREAK | EVENT_AUD_BOUNDARY) )
   {
    for (unsigned int i = 0; i < listLTCBreak.size(); i++)
     {
      if (listLTCBreak.at(i) > iFrame)
       {
        iRet = listLTCBreak.at(i);
        break;
       }
     }

    return iRet;
   }

  while (iFrame+1 < iAllocEvent && ullpEvent && iFrame < iAllocEvent)
   {
    iFrame++;

    if (ullpEvent[iFrame] & ullFlag)
     {
      iRet = iFrame;
      break;  // out of while
     }
   }


  return iRet;
}  /* getNextEvent */

int CTimeLine::getPreviousEvent (int iFrame, MTI_UINT64 ullFlag)
{
  int iRet;

  iRet = iFrame;

  // if possible use the list of LTC breaks
  if ( ullFlag == EVENT_AUD_LTC_BREAK || ullFlag == EVENT_AUD_BOUNDARY ||
		ullFlag == (EVENT_AUD_LTC_BREAK | EVENT_AUD_BOUNDARY) )
   {
    for (int i = listLTCBreak.size() - 1; i >= 0; i--)
     {
      if (listLTCBreak.at(i) < iFrame)
       {
        iRet = listLTCBreak.at(i);
        break;
       }
     }

    return iRet;
   }

  while (--iFrame >= 0 && ullpEvent && iFrame < iAllocEvent)
   {
    if (ullpEvent[iFrame] & ullFlag)
     {
      iRet = iFrame;
      break;  // out of while
     }
   }

  return iRet;
}  /* getPreviousEvent */

// this call has been superseded by the two following it, since
// the frame index matching a given LTC is not always unique
int CTimeLine::getFrameIndexForLTC (CTimecode &tcArg)
{
  int iRet = -1;

  if (ttpAudioLTC == 0 || pTrackStrategy == 0)
    return iRet;

  int iFrameCountArg = tcArg.getFrameCount();

  int iFrameRateInClip = vflp->getFrameRate() + .5;

  float fRateFactor = (float)iFrameRateAudioLTC / (float)iFrameRateInClip;

  // cycle through all audio breaks and look for tcArg
  for (unsigned int i = 0; i < listLTCBreak.size(); i++)
   {
    int iFrame = pTrackStrategy->getAudioClipFrame (listLTCBreak.at(i));
    int iNFrame;
    if (i < listLTCBreak.size() - 1)
     {
      iNFrame = listLTCBreak.at(i+1) - listLTCBreak.at(i);
     }
    else
     {
      iNFrame = ttpAudioLTC->getTotalFrameCount() - iFrame;
     }

    iNFrame = (float)iNFrame * fRateFactor;

    if (iFrame >= 0 && iFrame < ttpAudioLTC->getTotalFrameCount())
     {
/**
      CSMPTETimecodeFrame *tfp = ttpAudioLTC->getSMPTETimecodeFrame(iFrame);

      if (tfp->getSMPTETimecode().getRawTimecode() != 0xFFFFFFFF)
       {
        CTimecode tc = tfp->getTimecode(iFrameRateAudioLTC);
**/
      CSMPTETimecode stcTmp;
      float fPartialTmp;
      iRet = ttpAudioLTC->getSMPTETimecode(iFrame, stcTmp, fPartialTmp);
      if (iRet != 0)
         return -1;

      if (stcTmp.getRawTimecode() != 0xFFFFFFFF)
       {
        CTimecode tc = stcTmp.getTimecode(iFrameRateAudioLTC);

        int iFrameCount = tc.getFrameCount();

        if (iFrameCountArg >= iFrameCount && iFrameCountArg <
		(iFrameCount + iNFrame))
         {
          iRet = listLTCBreak.at(i) +
		( (float)(iFrameCountArg - iFrameCount) / fRateFactor );

          break;
         }
       }
     }
   }

  return iRet;

}

// this call captures ALL the frame indices matching a given LTC and
// returns the number captured
int CTimeLine::getNumberOfMatchingFrameIndicesForLTC (CTimecode &tcArg)
{
  int iRet;

  listLTCFrameIndex.clear();

  if (ttpAudioLTC == 0 || pTrackStrategy == 0)
    return 0;

  int iFrameCountArg = tcArg.getFrameCount();

  int iFrameRateInClip = vflp->getFrameRate() + .5;

  float fRateFactor = (float)iFrameRateAudioLTC / (float)iFrameRateInClip;

  // cycle through all audio breaks and look for tcArg
  for (unsigned int i = 0; i < listLTCBreak.size(); i++)
   {
    int iFrame = pTrackStrategy->getAudioClipFrame (listLTCBreak.at(i));
    int iNFrame;
    if (i < listLTCBreak.size() - 1)
     {
      iNFrame = listLTCBreak.at(i+1) - listLTCBreak.at(i);
     }
    else
     {
      iNFrame = ttpAudioLTC->getTotalFrameCount() - iFrame;
     }

    iNFrame = (float)iNFrame * fRateFactor;

    if (iFrame >= 0 && iFrame < ttpAudioLTC->getTotalFrameCount())
     {
      CSMPTETimecode stcTmp;
      float fPartialFrame;
      iRet = ttpAudioLTC->getSMPTETimecode(iFrame, stcTmp, fPartialFrame);
      if (iRet != 0)
         return 0;

      if (stcTmp.getRawTimecode() != 0xFFFFFFFF)
       {
        CTimecode tc = stcTmp.getTimecode(iFrameRateAudioLTC);

        int iFrameCount = tc.getFrameCount();

        if (iFrameCountArg >= iFrameCount && iFrameCountArg <
		(iFrameCount + iNFrame))
         {
          float fDeltaFrame;
          fDeltaFrame = (float)(iFrameCountArg - iFrameCount) - fPartialFrame;
          fDeltaFrame /= fRateFactor;
          int iNewFrame = (float)listLTCBreak.at(i) + fDeltaFrame + .5;

          listLTCFrameIndex.push_back(iNewFrame);
         }
       }
     }
   }

   return(listLTCFrameIndex.size());
}

// immediately after the above call, returns the Nth frame index
// matching the LTC
int CTimeLine::getNthMatchingFrameIndexForLTC(int iNSeq)
{
   int iRet = -1;

   if ((iNSeq >= 0)&&(iNSeq < (int)listLTCFrameIndex.size())) {

      iRet = listLTCFrameIndex.at(iNSeq);
   }

   return(iRet);
}

void CTimeLine::addEvent (int iFrameArg, MTI_UINT64 ullFlag, bool bAudio,
		bool bTrackFile)
{

  if (pTrackStrategy == 0)
    return;

  int iFrame, iPartial;

  if (bAudio)
   {
    iFrame = pTrackStrategy->getAudioClipFrame (iFrameArg);
   }
  else
   {
    pTrackStrategy->getVideoClipFrame (iFrameArg, iFrame, iPartial);
   }

   if (iFrameArg >= 0 && iFrameArg < iAllocEvent && ullpEvent)
   {
      EventData.first = ullpEvent[iFrameArg];
      EventData.second = ullFlag;
      ullpEvent[iFrameArg] |= ullFlag;
      EventChanged.Notify();
   }

  if (bTrackFile && ttpEvent)
   {
    int iEventFrame = line2track(iFrame);

    if (iEventFrame >= 0 && iEventFrame < ttpEvent->getTotalFrameCount())
     {
      // change the event flag in the track file
      MTI_UINT64 ullOld = ttpEvent->getEventFrame(iEventFrame)->getEventFlags();
      ttpEvent->getEventFrame(iEventFrame)->setEventFlags (ullOld|ullFlag);
     }
   }

  // add to list of LTC breaks
  if (ullFlag & (EVENT_AUD_LTC_BREAK | EVENT_AUD_BOUNDARY))
   {
    bool bPushBack = true;
    // where does this fit in the list
    for (unsigned int i = 0; i < listLTCBreak.size(); i++)
     {
      if (listLTCBreak.at(i) > iFrameArg)
       {
        listLTCBreak.insert (listLTCBreak.begin() + i, iFrameArg);
        bPushBack = false;
        break;
       }
      else if (listLTCBreak.at(i) == iFrameArg)
       {
        // do not make a duplicate entry
        bPushBack = false;
        break;
       }
     }

    if (bPushBack)
     {
      listLTCBreak.push_back (iFrameArg);
     }
   }
}

void CTimeLine::deleteEvent (int iFrameArg, MTI_UINT64 ullFlag, bool bAudio,
		bool bTrackFile)
{
  if (pTrackStrategy == 0)
    return;

  int iFrame = 0, iPartial;

  if (bAudio)
   {
    iFrame = pTrackStrategy->getAudioClipFrame (iFrameArg);
   }
  else
   {
    pTrackStrategy->getVideoClipFrame (iFrameArg, iFrame, iPartial);
   }

   if (iFrameArg >= 0 && iFrameArg < iAllocEvent && ullpEvent)
   {
      EventData.first = ullpEvent[iFrameArg];
      EventData.second = ullFlag;
      ullpEvent[iFrameArg] &= ~ullFlag;
      EventChanged.Notify();
   }

  if (bTrackFile && ttpEvent)
   {
    int iEventFrame = line2track(iFrame);

    if (iEventFrame >= 0 && iEventFrame < ttpEvent->getTotalFrameCount())
     {
      // change the event flag in the track file
      MTI_UINT64 ullOld = ttpEvent->getEventFrame(iEventFrame)->getEventFlags();

      //Zeros out old event.
      ttpEvent->getEventFrame(iEventFrame)->setEventFlags (ullOld & ~ullFlag);
     }
   }

  // update the list of LTC breaks;
  if (ullFlag & (EVENT_AUD_LTC_BREAK | EVENT_AUD_BOUNDARY))
   {
    for (unsigned int i = 0; i < listLTCBreak.size(); i++)
     {
      if (listLTCBreak.at(i) == iFrameArg)
       {
        listLTCBreak.erase (listLTCBreak.begin() + i);
        break;
       }
     }
   }
}  /* deleteEvent */

int CTimeLine::updateEventFile ()
{
  if (ttpEvent == 0)
    return 0;

  return ttpEvent->updateFrameListFile ();
}  /* updateEventFile */

int CTimeLine::getTotalFrameCount ()
{
  int iRet = 0;
  int iVideoFrameCount = 0, iAudioFrameCount = 0;
  if (pTrackStrategy)
   {
    iVideoFrameCount = pTrackStrategy->getVideoFrameCount();
    iAudioFrameCount = pTrackStrategy->getAudioFrameCount();

    if (iVideoFrameCount > iAudioFrameCount)
      iRet = iVideoFrameCount;
    else
      iRet = iAudioFrameCount;
   }

  return iRet;
}  /* getTotalFrameCount */

ClipSharedPtr CTimeLine::getClip(void)
{
  return clip;
}

void CTimeLine::ClearAudioProxy (void)
{
  if (app == 0)
    return;

  for (int iFrame = 0; iFrame < iAllocAudio; iFrame++)
   {
    // whole frame proxy
    for (int iChannel = 0; iChannel < iNChannelAudio; iChannel++)
     {
      app[iFrame].spMinValue[iChannel] = 0;
      app[iFrame].spMaxValue[iChannel] = 0;
     }
    // sub frame proxy
    for (int iSubFrame = 0; iSubFrame < AUDIO_PROXY_PER_FRAME; iSubFrame++)
     {
      for (int iChannel = 0; iChannel < iNChannelAudio; iChannel++)
       {
        app[iFrame].sppMinValue[iSubFrame][iChannel] = 0;
        app[iFrame].sppMaxValue[iSubFrame][iChannel] = 0;
       }
     }

   }

  ResetAudioReader();
}  /* ClearAudioProxy */

void CTimeLine::ResetAudioReader (void)
{
/*
	The bInitialized flag is used to indicate which audio frames have
	been read and turned into a proxy version.

	This function resets the audio IO so that audio data is re-read.
*/

  if (app == 0)
    return;

  for (int iFrame = 0; iFrame < iAllocAudio; iFrame++)
   {
    app[iFrame].bInitialized = false;
   }

}  /* ResetAudioReader */

int CTimeLine::ResetEvent ()
{
  if (pTrackStrategy == 0)
    return 0;

  // do we need to allocate more storage for the events?

  if (getTotalFrameCount () > iAllocEvent)
   {
      // What is this fucking 1.25 for? QQQ
      // How do you realloc without ever having fucking malloced?
    iAllocEvent = 1.25 * getTotalFrameCount ();
    MTI_UINT64 *ullpTmp;
    ullpTmp = (MTI_UINT64 *) realloc (ullpEvent, iAllocEvent * sizeof (MTI_UINT64));
    if (ullpTmp == NULL)
     {
      return ERR_TIMELINE_REALLOC;
     }
    ullpEvent = ullpTmp;
   }

  // zero the timeline
  for (int iFrame = 0; iFrame < iAllocEvent; iFrame++)
   {
    ullpEvent[iFrame] = 0;
   }

  listLTCBreak.clear();

  ResetTrackFile ();

  return 0;
}  /* ResetEvent */

void CTimeLine::ResetTrackFile ()
{
  // copy data out of the track file
  if (ttpEvent && pTrackStrategy)
   {
    int iAudioFrame, iVideoFrame, iVideoFramePartial;
    MTI_UINT64 ullTmp;
    for (int iFrame = 0; iFrame < iAllocEvent; iFrame++)
     {
      ullTmp = EVENT_NONE;
      iAudioFrame = pTrackStrategy->getAudioClipFrame (iFrame);
      pTrackStrategy->getVideoClipFrame (iFrame, iVideoFrame, iVideoFramePartial);
      if (iAudioFrame >= 0 && iAudioFrame < ttpEvent->getTotalFrameCount())
       {
        ullTmp |= (ttpEvent->getEventFrame(iAudioFrame)->getEventFlags() &
		EVENT_AUDIO_MASK);
       }

      // make certain beginning and end of track has a break
      if (iAudioFrame == 0 || iAudioFrame == (ttpEvent->getTotalFrameCount()-1))
       {
        ullTmp |= EVENT_AUD_LTC_BREAK;
       }

      // track file framing may not match
      if (iVideoFrame >= 0)
       {
        iVideoFrame = line2track(iVideoFrame);
       }

      if (iVideoFrame >= 0 && iVideoFrame < ttpEvent->getTotalFrameCount())
       {
        ullTmp |= (ttpEvent->getEventFrame(iVideoFrame)->getEventFlags () &
		EVENT_VIDEO_MASK);
       }

      ullpEvent[iFrame] |= ullTmp;

      if (ullTmp & (EVENT_AUD_LTC_BREAK|EVENT_AUD_BOUNDARY))
       {
        listLTCBreak.push_back (iFrame);
       }
     }
   }
}

void CTimeLine::ClearEvent (MTI_UINT64 llMask)
{
  for (int iFrame = 0; iFrame < iAllocEvent; iFrame++)
   {
    ullpEvent[iFrame] &= (~llMask);
   }

  if (llMask & (EVENT_AUD_LTC_BREAK|EVENT_AUD_BOUNDARY))
   {
    listLTCBreak.clear();
   }
}

void CTimeLine::newMultiFrameEvent (int iFrame, int iNFrame, MTI_UINT64 ullFlag,
		const string &strAnnotation, const string &strAnnotationShort,
		bool bUnderlineAnnotation, MTI_UINT32 uiColor,
		MTI_UINT32 uiColorType, MTI_UINT32 uiAnnotationColor,
		void *vpPtr, bool bConsolidate)
{
  // check for errors
  if (iFrame < 0 || iNFrame <= 0 || iFrame + iNFrame > getTotalFrameCount())
   {
    return;
   }

  CMultiFrameEvent mfeNew;
  mfeNew.iFrame = iFrame;
  mfeNew.iNFrame = iNFrame;
  mfeNew.ullFlag = ullFlag;
  mfeNew.uiColor = uiColor;
  mfeNew.uiColorType = uiColorType;
  mfeNew.strAnnotation = strAnnotation;
  mfeNew.strAnnotationShort = strAnnotationShort;
  mfeNew.bUnderlineAnnotation = bUnderlineAnnotation;
  mfeNew.uiAnnotationColor = uiAnnotationColor;
  mfeNew.vpPtr = vpPtr;

  // where does this item fit in the list.  List is ordered by start frame
  vector <CMultiFrameEvent>::iterator pos;
  for (pos = listMFE.begin(); pos != listMFE.end(); pos++)
   {
    if (bConsolidate)
     {
      bool bMerged = false;
      do
       {
        // Look for overlap or simple adjacency, but only if the event
        // properties match exactly
        // NOTE: THIS DOES NOT WORK WELL IN THE CASE WHERE OVERLAPPING
        //       EVENTS HAVE DIFFERENT PROPERTIES. Oh, well...
        if ((iFrame <= (pos->iFrame + pos->iNFrame)) &&
            ((iFrame + iNFrame) >= pos->iFrame) &&
            (ullFlag == pos->ullFlag) &&
            (uiColor == pos->uiColor) &&
            (uiColorType == pos->uiColorType) &&
            (strAnnotation == pos->strAnnotation) &&
            (strAnnotationShort == pos->strAnnotationShort) &&
            (bUnderlineAnnotation == pos->bUnderlineAnnotation) &&
            (uiAnnotationColor == pos->uiAnnotationColor) &&
            (vpPtr == pos->vpPtr))
         {
          // Yes, consolidate rather than create a new event
          int iEndFrame = ((iFrame + iNFrame) > (pos->iFrame + pos->iNFrame))?
                            (iFrame + iNFrame) : (pos->iFrame + pos->iNFrame);
          iFrame = (iFrame < pos->iFrame)? iFrame : pos->iFrame;
          iNFrame = iEndFrame - iFrame;

          // I am counting on the fact here that if I erase the item at
          // which pos is pointing, it will end up pointing at the item
          // that was next in the list after the erased item. Just need
          // to be careful to handle the case where we erase the last
          // item in the list.
          listMFE.erase(pos);
          bMerged = true;
         }
       }
      while (bMerged && pos != listMFE.end());
     }

    if (iFrame < pos->iFrame)
     {
      // the new event belongs before pos
      break;
     }
   }

  if (pos == listMFE.end())
   {
    // add to end
    listMFE.push_back (mfeNew);
   }
  else
   {
    // insert before
    listMFE.insert (pos, mfeNew);
   }

}  /* newMultiFrameEvent */

void CTimeLine::clearMultiFrameEvent ()
{
  listMFE.clear();
}  /* clearMultiFrameEvent */

CMultiFrameEvent CTimeLine::getNextMultiFrameEvent (int iFrame,
		MTI_UINT64 ullFlag)
{
  vector <CMultiFrameEvent>::iterator pos;

  pos = listMFE.begin();
  while (pos != listMFE.end())
   {
    if ((pos->iFrame > iFrame) && (pos->ullFlag & ullFlag))
     {
      return *pos;
     }
    pos++;
   }

  // did not find one, return a dummy
  CMultiFrameEvent mfe;
  return mfe;

}  /* getNextMultiFrameEvent */

CMultiFrameEvent CTimeLine::getPreviousMultiFrameEvent (int iFrame,
		MTI_UINT64 ullFlag)
{
  vector <CMultiFrameEvent>::iterator pos;

  pos = listMFE.end();
  while (pos != listMFE.begin())
   {
    pos--;
    if ((pos->iFrame < iFrame) && (pos->ullFlag & ullFlag))
     {
      return *pos;
     }
   }

  // did not find one, return a dummy
  CMultiFrameEvent mfe;
  return mfe;

}  /* getPreviousMultiFrameEvent */

void CTimeLine::setSFEOverride (MTI_UINT64 ullFlag, MTI_UINT32 uiColor,
		MTI_UINT32 uiColorType, MTI_INT32 iSymbol)
{
  CSingleFrameEvent sfe;
  sfe.ullFlag = ullFlag;
  sfe.uiColor = uiColor;
  sfe.uiColorType = uiColorType;
  sfe.iSymbol = iSymbol;
  listSFEOverride.push_back (sfe);
}  /* setSFEOverride */

void CTimeLine::clearSFEOverride (MTI_UINT64 ullFlag)
{

  if (ullFlag == EVENT_NONE)
   {
    // set all events back to default
    listSFEOverride.clear();
   }

  vector <CSingleFrameEvent>::iterator pos;

  for (pos = listSFEOverride.begin(); pos != listSFEOverride.end(); pos++)
   {
    if (pos->ullFlag == ullFlag)
     {
      listSFEOverride.erase(pos);
     }
   }
}  /* clearSFEOverride */

void CTimeLine::setSFEAnnotation (int iFrame, MTI_UINT64 ullFlag,
	const string &str, const string &strShort)
{
  vector <CSingleFrameEvent>::iterator pos;

  // look for this iFrame/ullFlag entry

  for (pos = listSFEAnnotation.begin(); pos != listSFEAnnotation.end(); pos++)
   {
    if (pos->iFrame == iFrame && pos->ullFlag == ullFlag)
     {
      if (str == "")
       {
        listSFEAnnotation.erase(pos);
       }
      else
       {
        pos->strAnnotation = str;
       }
      return;
     }
   }

  // could not find this in the list, so make a new entry
  CSingleFrameEvent sfe;
  sfe.iFrame = iFrame;
  sfe.ullFlag = ullFlag;
  sfe.strAnnotation = str;
  sfe.strAnnotationShort = strShort;

  if (sfe.strAnnotation == "")
   {
    // no need to keep this annotation
    return;
   }

  listSFEAnnotation.push_back (sfe);

}  /* setSFEAnnotation */

void CTimeLine::clearSFEAnnotation ()
{
  listSFEAnnotation.clear();
}  /* clearSFEAnnotation */

CSingleFrameEvent CTimeLine::getNextSingleFrameEvent (int iFrame,
		MTI_UINT64 ullFlag)
{
  CSingleFrameEvent sfe;

  // get the next event
  sfe.iFrame = getNextEvent (iFrame, ullFlag);

  if (sfe.iFrame == iFrame)
   {
    // could not find one.  Just return
    return sfe;
   }

  AugmentSFE (sfe, ullFlag);


  return sfe;
}  /* getNextMultiFrameEvent */

CSingleFrameEvent CTimeLine::getPreviousSingleFrameEvent (int iFrame,
		MTI_UINT64 ullFlag)
{
  CSingleFrameEvent sfe;

  // get the next event
  sfe.iFrame = getPreviousEvent (iFrame, ullFlag);

  if (sfe.iFrame == iFrame)
   {
    // could not find one.  Just return
    return sfe;
   }

  AugmentSFE (sfe, ullFlag);

  return sfe;
}  /* getPreviousSingleFrameEvent */

int CTimeLine::getAudio (int iFrameStart, int iFrameStop,
	MTI_INT16 **spData, int iMaxSample, int iNChannelToRead,
		int *ipChannelToRead)
{
  int iRet=0;
  int iFrame, iAudioFrame;
  int iNSample;

  if (aflp == 0 || pTrackStrategy == 0)
   {
    return 0;
   }

  int iTotalSample = 0;
  bool bFillWithSilence;

  int iFileHandle = aflp->getParentTrack()->CreateNewFileHandle();
  for (int iFrame = iFrameStart; iFrame < iFrameStop; iFrame++)
   {
    iAudioFrame = pTrackStrategy->getAudioClipFrame (iFrame);
    if (iAudioFrame >= 0)
     {
      if (iFileHandle != -1)
       {
        iNSample = aflp->getSampleCountAtFrame (iAudioFrame);
        iRet = aflp->readMediaBySample (iAudioFrame, 0, iNSample, ucpRawFrame,
		iFileHandle);
       }
      else
       {
        iRet = -1;
       }
      if (iRet)
       {
        // error occurred, set to silence
        bFillWithSilence = true;
       }
      else
       {
        bFillWithSilence = false;
       }
     }
    else
     {
      // the playlist says this frame is out of range.  Fill with silence
      bFillWithSilence = true;
     }

    if (bFillWithSilence)
     {
      // if the frame is illegal, use frame 0
      if (iAudioFrame < 0)
        iAudioFrame = 0;

      iNSample = aflp->getSampleCountAtFrame (iAudioFrame);

      // do not read more than is allowed
      if (iTotalSample + iNSample > iMaxSample)
        iNSample = iMaxSample - iTotalSample;

      for (int iChan = 0; iChan < iNChannelToRead; iChan++)
       {
        MTI_INT16 *spDst = spData[iChan] + iTotalSample;
        for (int iSample = 0; iSample < iNSample; iSample++)
         {
          *spDst++ = 0;
         }
       }

      iTotalSample += iNSample;
     }
    else
     {
      // read was successful, copy to buffer
      MTI_INT16 *spRawData = (MTI_INT16 *)ucpRawFrame;
      iNSample = aflp->getSampleCountAtFrame (iAudioFrame);

      // do not read more than is allowed
      if (iTotalSample + iNSample > iMaxSample)
        iNSample = iMaxSample - iTotalSample;

      for (int iChan = 0; iChan < iNChannelToRead; iChan++)
       {
        int iChannel = ipChannelToRead[iChan];
        if (iChannel < 0 || iChannel > iNChannelAudio)
          iChannel = 0;

        MTI_INT16 *spSrc = spRawData + iChannel;
        MTI_INT16 *spDst = spData[iChan] + iTotalSample;

        for (int iSample = 0; iSample < iNSample; iSample++)
         {
          *spDst++ = *spSrc;
          spSrc += iNChannelAudio;
         }
       }

      iTotalSample += iNSample;
     }
   }

  if (iFileHandle != -1)
    aflp->getParentTrack()->ReleaseFileHandle(iFileHandle);

  return iTotalSample;
}  /* getAudio */

int CTimeLine::getAudioCount (int iFrameStart, int iFrameStop)
{
  int iRet;
  int iFrame, iAudioFrame;
  int iNSample;


  if (aflp == 0 || pTrackStrategy == 0)
   {
    return 0;
   }

  int iTotalSample = 0;

  for (int iFrame = iFrameStart; iFrame < iFrameStop; iFrame++)
   {
    iAudioFrame = pTrackStrategy->getAudioClipFrame (iFrame);

    if (iAudioFrame < 0)
      iAudioFrame = 0;

    iNSample = aflp->getSampleCountAtFrame (iAudioFrame);

    iTotalSample += iNSample;
   }

  return iTotalSample;
}  /* getAudioCount */

float CTimeLine::findClap (int iChannel, int iFrameStart, int iFrameStop)
// run through the frames looking for a clap.
{
  float fClap = (float)iFrameStart;
  float *fpEnergy;

  if (iChannel < 0 || iChannel >= iNChannelAudio)
   {
    iChannel = 0;
   }

  if (aflp == 0 || pTrackStrategy == 0 || iFrameStop < iFrameStart)
   {
    return fClap;
   }

  // Create bins to hold the energy.  Use BINS_PER_FRAME.
  int iNBin = BINS_PER_FRAME * (iFrameStop - iFrameStart);
  fpEnergy = (float *)malloc (iNBin * sizeof(float));
  if (fpEnergy == NULL)
    return fClap;

  // how many samples per bin
  int iSamplePerBin = (float)getAudioCount (iFrameStart, iFrameStop) /
		(float)iNBin;

  // calculate the energy in each bin
  int iFrame = iFrameStart;
  int iSampleRead = 0;
  int iSample = 0;
  for (int iBin = 0; iBin < iNBin; iBin++)
   {
    MTI_INT16 *spAudioFrame;
    int iSum = 0;
    for (int i = 0; i < iSamplePerBin; i++)
     {
      while (iSample >= iSampleRead && iFrame < iFrameStop)
       {
        // read the next frame
        iSampleRead += ReadOneFrame (iFrame);

        // advance counter
        iFrame++;

        // assign pointers
        spAudioFrame = (MTI_INT16 *)ucpRawFrame;
       }

      if (iSample >= iSampleRead)
       {
        // error has occurred, just return
        free (fpEnergy);
        return fClap;
       }

      // add this value to the energy in the bin
      int iNewVal;
      iNewVal = spAudioFrame[iChannel];
      spAudioFrame += iNChannelAudio;

      // add new value to the energy sum
      if (iNewVal < 0)
        iSum -= iNewVal;
      else
        iSum += iNewVal;

      // advance sample counter
      iSample++;
     }

    fpEnergy[iBin] = (float)iSum / (float)iSamplePerBin;

   }

/*

	now analyze the material looking for the best clap.

	Claps have the following characterists:

		1.  fast rise times.
		2.  fast decay times (but not as fast as the rise)
*/

  // as a quick test, just look for largest local difference in energy

  float fBest = fpEnergy[0];
  int iBest = 0;

  for (int iBin = 1; iBin < iNBin; iBin++)
   {
    if (fpEnergy[iBin] - fpEnergy[iBin-1] > fBest)
     {
      fBest = fpEnergy[iBin] - fpEnergy[iBin-1];
      iBest = iBin;
     }
   }

  // adjust iBest because it is better to mark clap slightly before
  // true clap position.  Otherwise a hint of an audio clap may be
  // heard while the clapper is still open in the image

  iBest -= 2;
  if (iBest < 0)
    iBest = 0;

  fClap = (float)iFrameStart + ( (float)(iBest * iSamplePerBin)
		/ (aflp->getAudioFormat()->getSamplesPerField()
			* (float)iNFieldAudio) );


  free (fpEnergy);

  return fClap;
}  /* findClap */

void CTimeLine::AugmentSFE (CSingleFrameEvent &sfe, MTI_UINT64 ullFlag)
{
  // does this event type have override values
  vector <CSingleFrameEvent>::iterator pos;

  for (pos = listSFEOverride.begin(); pos != listSFEOverride.end(); pos++)
   {
    if (pos->ullFlag == ullFlag)
     {
      sfe.uiColor = pos->uiColor;
      sfe.uiColorType = pos->uiColorType;
      sfe.iSymbol = pos->iSymbol;
      break;
     }
   }

  // does this event have an annotation
  for (pos = listSFEAnnotation.begin(); pos != listSFEAnnotation.end(); pos++)
   {
    if (pos->ullFlag == ullFlag && pos->iFrame == sfe.iFrame)
     {
      sfe.strAnnotation = pos->strAnnotation;
      break;
     }
   }
}  /* AugmentSFE */

int CTimeLine::getHandleCount ()
{
  int iRet=0;

  if (pTrackStrategy)
    iRet = pTrackStrategy->getHandleCount();


  return iRet;
}

namespace {
  bool isCompatibleFramingSelection(int fs1, int fs2)
  {
      // all we care about is film vs video.
      bool result = false;
      if ((fs1 == FRAMING_SELECT_FILM && fs2 == FRAMING_SELECT_FILM)
      || (fs1 != FRAMING_SELECT_FILM && fs2 != FRAMING_SELECT_FILM))
       {
        result = true;
       }
      return result;
  }
}  /* local namespace */


int CTimeLine::line2track(int iTimeLineFrame)
{
  int iTimeTrackFrame = iTimeLineFrame;
  int iTrackFramingSelect =
    (ttpEvent->getFramingType() == FRAMING_TYPE_FILM)?
      FRAMING_SELECT_FILM : FRAMING_SELECT_VIDEO;

  if (!isCompatibleFramingSelection(iTrackFramingSelect, iFramingSelected))
   {
    // framing doesn't match
    iTimeTrackFrame =  clip->getVideoProxy(iVideoSelected)
                            ->translateFrameIndex(iTimeLineFrame,
                                                  iFramingSelected,
                                                  iTrackFramingSelect);
   }

  return iTimeTrackFrame;

}  /* line2track */

int CTimeLine::track2line(int iTimeTrackFrame)
{
  int iTimeLineFrame = iTimeTrackFrame;
  int iTrackFramingSelect =
    (ttpEvent->getFramingType() == FRAMING_TYPE_FILM)?
      FRAMING_SELECT_FILM : FRAMING_SELECT_VIDEO;

  if (!isCompatibleFramingSelection(iTrackFramingSelect, iFramingSelected))
   {
    // framing doesn't match
    iTimeLineFrame =  clip->getVideoProxy(iVideoSelected)
                          ->translateFrameIndex(iTimeTrackFrame,
                                                iFramingSelected,
                                                iTrackFramingSelect);
   }

  return iTimeLineFrame;

}  /* track2line */

// ---------------------------------------------------------------------------
//
// Mask Animation Interface
//
// ---------------------------------------------------------------------------

bool CTimeLine::GetFirstKeyframe(int &keyframeIndex, bool &inclusion)
{
   if (animateIFPtr == NULL)
      return false;

   return animateIFPtr->GetFirstKeyframe(keyframeIndex, inclusion);
}

bool CTimeLine::GetNextKeyframe(int &keyframeIndex, bool &inclusion)
{
   if (animateIFPtr == NULL)
      return false;

   return animateIFPtr->GetNextKeyframe(keyframeIndex, inclusion);
}

bool CTimeLine::GetPreviousKeyframe(int &keyframeIndex, bool &inclusion)
{
   if (animateIFPtr == NULL)
      return false;

   return animateIFPtr->GetPreviousKeyframe(keyframeIndex, inclusion);
}

//----------------------------------------------------------------------------

bool CTimeLine::IsKeyframeHere(int frameIndex)
{
   if (animateIFPtr == NULL)
      return false;

   return animateIFPtr->IsKeyframeHere(frameIndex);
}
//----------------------------------------------------------------------------

void CTimeLine::DeleteKeyframe(int frameIndex)
{
   if (animateIFPtr == NULL)
      return;

   animateIFPtr->DeleteKeyframe(frameIndex);
}

//----------------------------------------------------------------------------

void CTimeLine::DeleteAllKeyframes()
{
   if (animateIFPtr == NULL)
      return;

   animateIFPtr->DeleteAllKeyframes();
}

//----------------------------------------------------------------------------

void CTimeLine::SetAnimateTimelineInterface(CAnimateTimelineInterface *newInterface)
{
   animateIFPtr = newInterface;
}

//----------------------------------------------------------------------------
