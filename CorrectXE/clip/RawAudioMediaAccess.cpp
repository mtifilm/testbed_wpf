// RawAudioMediaAccess.cpp: implementation of the CRawAudioMediaAccess class.
//
//////////////////////////////////////////////////////////////////////

#include "AudioFormat3.h"
#include "err_clip.h"
#include "MediaLocation.h"
#include "RawAudioMediaAccess.h"
#include "RawAudioMediaAllocator.h"
#include "RawAudioMediaDeallocator.h"
#include "RawAudioFileIO.h"
#include "timecode.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRawAudioMediaAccess::CRawAudioMediaAccess(const string& mediaIdentifier,
                                           const CMediaFormat& mediaFormat)
: CMediaAccess(MEDIA_TYPE_RAW_AUDIO, mediaIdentifier,
                                    "hist_dir_CRawAudioMediaAccess")
{
   switch(mediaFormat.getMediaFormatType())
      {
      case MEDIA_FORMAT_TYPE_AUDIO3 :
         {
         const CAudioFormat& audioFormat3
                              = static_cast<const CAudioFormat&>(mediaFormat);
         channelCount = audioFormat3.getChannelCount();
         bytesPerSample = audioFormat3.getBytesPerSample();
         samplesPerField = audioFormat3.getSamplesPerField();
         }
         break;
      default :
         // something really wrong, should throw an exception
         break;
      }

   fileIOList.push_back(0);
}


CRawAudioMediaAccess::~CRawAudioMediaAccess()
{
   DeleteFileIOList();
}

//////////////////////////////////////////////////////////////////////

int CRawAudioMediaAccess::initialize()
{

   return 0;
}

//////////////////////////////////////////////////////////////////////

// Read from a raw audio file a buffer's full of audio data given
// the media location
int CRawAudioMediaAccess::read(MTI_UINT8* buffer, CMediaLocation &mediaLocation)
{
   int retVal;

   retVal = readMT(buffer, mediaLocation, 0);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CRawAudioMediaAccess::readMT(MTI_UINT8* buffer, CMediaLocation &mediaLocation,
                               int fileHandleIndex)
{
   int retVal;

   retVal = readMT(buffer, mediaLocation.getDataIndex(),
                 mediaLocation.getDataSize(), fileHandleIndex);
   if (retVal != 0)
      return retVal;

   return 0;
}

// Read from a raw audio file a buffer's full of audio data given
// the offset and size, both in number of samples
// NOTE: The caller is responsible for making sure the offset and size are
//       valid for the audio file
int CRawAudioMediaAccess::read(MTI_UINT8* buffer, MTI_INT64 offset,
                               unsigned int size)
{
   int retVal;

   retVal = readMT(buffer, offset, size, 0);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CRawAudioMediaAccess::readMT(MTI_UINT8* buffer, MTI_INT64 offset,
                                 unsigned int size, int fileHandleIndex)
{
   int retVal;

   retVal = CheckFileHandleIndex(fileHandleIndex);
   if (retVal != 0)
      return retVal;

   // Open audio file if necessary
   if (fileHandleIndex == 0 && fileIOList[0] == 0)
      {
      if (openRawIO() != 0)
         return CLIP_ERROR_FILE_OPEN_FAILED;
      }

   // Convert offset and size from number of samples to number of bytes
   MTI_INT64 byteOffset = samplesToBytes(offset);
   MTI_UINT32 byteSize = samplesToBytes(size);

   int status = fileIOList[fileHandleIndex]->read(buffer, byteOffset, byteSize);
   if (status == -1 || (unsigned int)status != byteSize)
      return CLIP_ERROR_FILE_READ_FAILED;

#if ENDIAN == MTI_LITTLE_ENDIAN
  // if this is a little endian machine, swap the byte order
  MTI_UINT8 *ucpTmp, ucTmp;
  ucpTmp = (MTI_UINT8 *)buffer;
  for (int iCnt = 0; iCnt < (int)byteSize; iCnt+=2)
   {
    ucTmp = ucpTmp[0];
    ucpTmp[0] = ucpTmp[1];
    ucpTmp[1] = ucTmp;
    ucpTmp += 2;
   }
#endif

   return 0;
}

// ---------------------------------------------------------------------------

// Write to raw audio file from caller's buffer given the media location
// NOTE: on little-endian machines, the bytes a swapped in the caller's
//       buffer before being written to disk.  This violates the const
//       property on the buffer argument.
int CRawAudioMediaAccess::write(const MTI_UINT8* buffer,
                                CMediaLocation &mediaLocation)
{
   int retVal;

   retVal = write(buffer, mediaLocation.getDataIndex(),
                  mediaLocation.getDataSize());
   if (retVal != 0)
      return retVal; // ERROR

   return 0;  // success
}

// Write to raw audio file from caller's buffer given the sample offset
// and sample count
// NOTE: on little-endian machines, the bytes a swapped in the caller's
//       buffer before being written to disk.  This violates the const
//       property on the buffer argument.
// NOTE: The caller is responsible for making sure the offset and size are
//       valid for the audio file
int CRawAudioMediaAccess::write(const MTI_UINT8* buffer, MTI_INT64 offset,
                                unsigned int size)
{
   // Open raw disk if necessary
   if (fileIOList[0] == 0)
      {
      if (openRawIO() != 0)
         return CLIP_ERROR_FILE_OPEN_FAILED;
      }

   MTI_INT64 byteOffset = samplesToBytes(offset);
   MTI_UINT32 byteSize = samplesToBytes(size);

#if ENDIAN == MTI_LITTLE_ENDIAN
  // if this is a little endian machine, swap the byte order
  // NOTE: This swaps the bytes in the caller's buffer, violating
  //       the const property of the argument!
  MTI_UINT8 *ucpTmp, ucTmp;
  ucpTmp = (MTI_UINT8*)buffer;
  for (int iCnt = 0; iCnt < (int)byteSize; iCnt+=2)
   {
    ucTmp = ucpTmp[0];
    ucpTmp[0] = ucpTmp[1];
    ucpTmp[1] = ucTmp;
    ucpTmp += 2;
   }
#endif

   int status;
   status = fileIOList[0]->write(buffer, byteOffset, byteSize);
   if (status == -1 || (unsigned int)status != byteSize)
      {
      // Write error 
      return -1;
      }

   return 0;
}

// ----------------------------------------------------------------------------

int CRawAudioMediaAccess::setFileBytes(MTI_UINT8 value)
{
   // Open raw disk if necessary
   if (fileIOList[0] == 0)
      {
      if (openRawIO() != 0)
         return CLIP_ERROR_FILE_OPEN_FAILED;
      }

   int status;
   status = fileIOList[0]->setFileBytes(value);
   if (status != 0)
      return status;

   return 0;
}

// --------------------------------------------------------------------------

int CRawAudioMediaAccess::openRawIO()
{
   // Open the audio file for read/write access
   
   // Check first entry in fileIOList
   if (fileIOList[0] == 0)
      {
      CRawAudioFileIO *newFileIO = new CRawAudioFileIO();
      int retVal = newFileIO->open(mediaIdentifier);
      if (retVal != 0)
         {
         delete newFileIO;
         return retVal;
         }
      fileIOList[0] = newFileIO;
      }

   return 0;
}

int CRawAudioMediaAccess::CreateNewFileHandle()
{
   // Make sure the default file handle exists before making
   // an extra file handle
   if (fileIOList[0] == 0)
      {
      if (openRawIO() != 0)
         return -1;
      }

   // Create a new instance of CRawFileIO
   CRawAudioFileIO *newFileIO = new CRawAudioFileIO();

   // Open the audio file for reading only
   int retVal = newFileIO->openReadOnly(mediaIdentifier);
   if (retVal != 0)
      {
      delete newFileIO;
      return -1;
      }

   // Add an entry to the fileIOList
   fileIOList.push_back(newFileIO);

   // Return  index of new entry in fileIOList
   return fileIOList.size() - 1;
}

int CRawAudioMediaAccess::ReleaseFileHandle(int fileHandleIndex)
{
   if (fileHandleIndex == 0)
      return 0;   // Never release default file handle index

   if (fileHandleIndex < 0 || fileHandleIndex >= (int)fileIOList.size())
      return CLIP_ERROR_INVALID_MEDIA_FILE_HANDLE_INDEX;

   delete fileIOList[fileHandleIndex];    // Delete CFileIO, which closes file

   fileIOList[fileHandleIndex] = 0;       // Mark as released

   return 0;
}

int CRawAudioMediaAccess::DeleteFileIOList()
{
   for (int i = 0; i < (int)fileIOList.size(); ++i)
      {
      delete fileIOList[i];
      fileIOList[i] = 0;
      }

   return 0;
}

int CRawAudioMediaAccess::CheckFileHandleIndex(int fileHandleIndex)
{
   // Check if file handle index is out-of-range
   if (fileHandleIndex < 0 || fileHandleIndex >= (int)fileIOList.size())
      return CLIP_ERROR_INVALID_MEDIA_FILE_HANDLE_INDEX;

   // Check if file handle index has already been released
   if (fileHandleIndex > 0 && fileIOList[fileHandleIndex] == 0)
      return CLIP_ERROR_INVALID_MEDIA_FILE_HANDLE_INDEX;

   return 0;
}

//////////////////////////////////////////////////////////////////////

/** @brief fieldsToSamples changes units from fields to samples
 *
 *  @param startingFieldIndex the starting field index
 *
 *  @param fields the number of fields
 *
 *  The conversion from fields to samples is non-trivial because:
 *    (a) some formats have a non-integral number of samples per field, and
 *    (b) floating-point error prevents simply multiplying by samples per field
 *
 *  Since a given field of image should have an integral number of
 *  audio samples, because of (a) not all fields will have the same
 *  number audio samples.  For example, in 525, some fields will have
 *  800 samples, and others will have 801.  The nominal samples per
 *  frame in 525 is 800.8.  In particular, fields in 525 will have
 *  800, 801, 801, 801, 801, ... samples, where the pattern repeats
 *  after 5 fields.
 *
 *  A decision must be made where the pattern starts.  In clip3,
 *  predating this comment, it was decided that the pattern starts on
 *  "frame 0" as defined by the timecode 00:00:00:00.  In clip5, the
 *  pattern starts at the 0th field of the first range.  Since the
 *  "field of reference" differs in different versions of clip, the
 *  burden is on the caller to pass the appropriate
 *  startingFieldIndex.  In clip3, startingFieldIndex should be
 *  relative to 00:00:00:00.  In clip5, startingFieldIndex should be
 *  relative to field 0 of the clip.
 *  
 *  To compensate for the floating-point error, fieldsToSamples does
 *  the bulk of its calculation in the integer domain.
 *  samplesPerThousandFields is guaranteed to be an integer because
 *  samplesPerField is computed using a "magic" ratio (1001/1000).
 *  For example, for 525, samplesPerField = (48000 samples/second) /
 *  (60 * (1000/1001) fields/second).  The strategy of doing
 *  calculations in the integer domain is more generally applicable.
 *  In this case "1000" is a good choice of multiplication factor for
 *  converting from floating-point to integer because of the magic
 *  ratio.
 */
MTI_INT64 CRawAudioMediaAccess::fieldsToSamples(
   MTI_UINT32 startingFieldIndex,
   MTI_UINT32 fields) const
{
   MTI_INT64 samplesPerThousandFields =
       (MTI_INT64)((samplesPerField * 1000) + .5);
   MTI_INT64 startingSampleIndex =
      (MTI_INT64)((samplesPerThousandFields * startingFieldIndex) / 1000);
   MTI_UINT32 newFieldIndex = startingFieldIndex + fields;
   MTI_INT64 newSampleIndex =
      (MTI_INT64)((samplesPerThousandFields * newFieldIndex) / 1000);
   MTI_INT64 samples = newSampleIndex - startingSampleIndex;
   return samples;
}

//////////////////////////////////////////////////////////////////////

/** @brief samplesToFields changes units from samples to fields
 *
 *  See fieldsToSamples for a more thorough explanation of the
 *  intricacies involved.
 */
MTI_UINT32 CRawAudioMediaAccess::samplesToFields(
   MTI_UINT32 startingFieldIndex,
   MTI_INT64 samples) const
{
   // Let epsion be the floating-point error in samplesPerField.  Then
   // for positive epsilon, fieldsLowEstimate is fields - 1.  For
   // negative epsilon and for zero epsilon, fieldsLowEstimate is
   // fields.
   MTI_UINT32 fieldsLowEstimate = (int)(samples / samplesPerField);
   MTI_UINT32 fieldsConsumed = fieldsLowEstimate;
   MTI_INT64 samplesConsumed =
      fieldsToSamples(startingFieldIndex, fieldsConsumed);
   MTI_INT64 remainderSamples = samples - samplesConsumed;

   while (remainderSamples > 0)
   {
      MTI_INT64 samplesInNextField =
         fieldsToSamples(startingFieldIndex + fieldsConsumed, 1);
      remainderSamples -= samplesInNextField;
      fieldsConsumed++;
   }

   return fieldsConsumed;
}


//////////////////////////////////////////////////////////////////////

MTI_UINT32 CRawAudioMediaAccess::samplesToBytes(MTI_UINT32 samples) const
{
   return samples * bytesPerSample * channelCount;
}

MTI_INT64 CRawAudioMediaAccess::samplesToBytes(MTI_INT64 samples) const
{
   return samples * bytesPerSample * channelCount;
}

//////////////////////////////////////////////////////////////////////

MTI_UINT32 CRawAudioMediaAccess::getAdjustedFieldSize(MTI_UINT32 fieldSize)
{
   return fieldSize;
}

//////////////////////////////////////////////////////////////////////

MTI_INT64 CRawAudioMediaAccess::adjustToBlocking(MTI_INT64 size,
                                                 bool roundingFlag)
{
   return size;
}

MTI_UINT32
CRawAudioMediaAccess::convertDataSizeToByteCount(MTI_UINT32 dataSize) const
{
   return samplesToBytes(dataSize);
}

// ----------------------------------------------------------------------

int CRawAudioMediaAccess::Peek(CMediaLocation &mediaLocation,
                               string &filename, MTI_INT64 &offset)
{
   filename = mediaIdentifier;

   offset = samplesToBytes(mediaLocation.getDataIndex());

   return 0;
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//
// Function:   queryFreeSpace
//
// Description:Queries the raw disk's free list to determine the
//             total available free space on the disk and also the
//             largest available contiguous free area
//
// Arguments:  MTI_INT64 *totalFreeBytes Ptr to caller's variable to accept
//                                       free size in bytes
//             MTI_INT64 *maxContiguousFreeBytes
//                                     Ptr to caller's variable to accept number of
//                                     bytes in largest contiguous free space
// Returns:    Returns 0 if query succeed.  Non-zero if failed.
//             Returns non-zero if either of totalBlocks or
//             maxContiguousBlocks is NULL pointer
//
//////////////////////////////////////////////////////////////////////
int CRawAudioMediaAccess::queryFreeSpace(MTI_INT64 *totalFreeBytes,
                                    MTI_INT64 *maxContiguousFreeBytes)
{
#ifdef _WINDOWS
   ULARGE_INTEGER lFreeBytesAvailableToCaller;	// variable to
                                                // receive free
                                                // bytes on disk
                                                // available to
                                                // the caller
   ULARGE_INTEGER lTotalNumberOfBytes; // variable to receive
   // number of bytes on disk
   ULARGE_INTEGER lTotalNumberOfFreeBytes; // variable to receive
   // free bytes on disk
   bool bRet = ::GetDiskFreeSpaceEx(getMediaIdentifier().c_str(),
                                    &lFreeBytesAvailableToCaller,
                                    &lTotalNumberOfBytes,
                                    &lTotalNumberOfFreeBytes);
   if (bRet == 0) // failure
      {
      return -1;
      }

   *totalFreeBytes = lFreeBytesAvailableToCaller.QuadPart;
#endif

   return 0;
}

//////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////
int CRawAudioMediaAccess::queryFreeSpaceEx(
   const CMediaFormat& mediaFormat,
   MTI_INT64 *totalFreeBytes,
   MTI_INT64 *maxContiguousFreeBytes)
{
   // ignore mediaFormat

   return queryFreeSpace(totalFreeBytes, maxContiguousFreeBytes);
}

//////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////
int CRawAudioMediaAccess::queryTotalSpace(MTI_INT64 *totalBytes)
{
#ifdef _WINDOWS
   ULARGE_INTEGER lFreeBytesAvailableToCaller;	// variable to
                                                // receive free
                                                // bytes on disk
                                                // available to
                                                // the caller
   ULARGE_INTEGER lTotalNumberOfBytes; // variable to receive
   // number of bytes on disk
   ULARGE_INTEGER lTotalNumberOfFreeBytes; // variable to receive
   // free bytes on disk
   bool bRet = ::GetDiskFreeSpaceEx(getMediaIdentifier().c_str(),
                                    &lFreeBytesAvailableToCaller,
                                    &lTotalNumberOfBytes,
                                    &lTotalNumberOfFreeBytes);
   if (bRet == 0) // failure
      {
      return -1;
      }

   *totalBytes = lTotalNumberOfBytes.QuadPart;
#endif

   return 0;
}

//////////////////////////////////////////////////////////////////////

bool CRawAudioMediaAccess::isClipSpecific() const
{
   // if mediaIdentifier is a directory, it's not clip-specific; if it
   // is clip specific, mediaIdentifier is a full path to an audio
   // file
   if (DoesDirectoryExist(mediaIdentifier) == true)
      return false;
   else
      return true;
}

//////////////////////////////////////////////////////////////////////
// Raw Disk Space Allocation and Deallocation functions
//////////////////////////////////////////////////////////////////////

int CRawAudioMediaAccess::checkSpace(const CMediaFormat& mediaFormat,
                                     MTI_UINT32 frameCount)
{
   return 0;
}

CMediaAllocator* CRawAudioMediaAccess::allocate(const CMediaFormat& mediaFormat,
                                                const CTimecode& frame0Timecode,
                                                MTI_UINT32 frameCount)
{
   // Figure out the number of bytes that are needed in this file
   MTI_UINT32 firstFieldOffset 
              = frame0Timecode.getAbsoluteTime() * mediaFormat.getFieldCount();
   MTI_UINT32 fieldCount = frameCount * mediaFormat.getFieldCount();
   MTI_INT64 sampleCount = fieldsToSamples(firstFieldOffset, fieldCount);
   MTI_INT64 byteSize = samplesToBytes(sampleCount);

   // Check if desired media file exists already
   if (CRawAudioFileIO::checkFileExists(mediaIdentifier))
      {
      // Error: media file already exists
      TRACE_0(errout << "Error: audio media file " << mediaIdentifier
                     << " already exists");
      return 0;  // Return NULL media allocator pointer
      }

   // Create an empty file with the right size and name
   if (CRawAudioFileIO::createFile(mediaIdentifier, byteSize) != 0)
      {
      // Error: could not create file
      TRACE_0(errout << "Error: could not create audio file "
                     << mediaIdentifier);
      return 0;  // Return NULL media allocator pointer
      }

#ifdef INIT_AUDIO_FILE
   // Set all of the bytes in the file to 0.  This forces the
   // allocation of the entire file
   if (setFileBytes(0) != 0)
      {
      TRACE_0(errout << "ERROR: could not initialize audio file "
                     << mediaIdentifier);
      return 0;
      }
#endif // #ifdef INIT_AUDIO_FILE

   // Create a new media allocator object and return it to caller
   return(new CRawAudioMediaAllocator(this, samplesPerField, 
                                      firstFieldOffset, fieldCount));

} // allocate

int CRawAudioMediaAccess::cancelAllocation(CMediaAllocator *mediaAllocator)
{
   // delete the audio file after checking necessary pre-conditions

   int retVal;

   // Close the Raw Audio Media file if necessary
   DeleteFileIOList();

   // Don't attempt to delete the file if it doesn't exist!
   if (DoesFileExist(mediaIdentifier))
      {
      // Delete the Raw Audio Media file
      retVal = CRawAudioFileIO::deleteFile(mediaIdentifier);
      if (retVal != 0)
         {
         // ERROR: File delete failed
         return retVal;
         }
      }

   return 0;   // Return success

} // cancelAllocation

CMediaAllocator* CRawAudioMediaAccess::extend(const CMediaFormat& mediaFormat,
                                              const CTimecode& frame0Timecode,
                                              MTI_UINT32 frameCount)
{
   // Figure out the number of bytes that are needed in this file
   MTI_UINT32 firstFieldOffset 
      = frame0Timecode.getAbsoluteTime() * mediaFormat.getFieldCount();
   MTI_UINT32 fieldCount = frameCount * mediaFormat.getFieldCount();
   MTI_INT64 existingSamples = 0;

   // Check if desired media file exists already
   if (!CRawAudioFileIO::checkFileExists(mediaIdentifier))
      {
      existingSamples = 0;

      MTI_INT64 sampleCount = fieldsToSamples(firstFieldOffset, fieldCount);
      MTI_INT64 byteSize = samplesToBytes(sampleCount);

      // Create an empty file with the right size and name
      if (CRawAudioFileIO::createFile(mediaIdentifier, byteSize) != 0)
         {
         // Error: could not create file
         TRACE_0(errout << "Error: could not create audio file "
                 << mediaIdentifier);
         return 0;  // Return NULL media allocator pointer
         }
      }
   else
      {
      // Open the file if necessary
      if (fileIOList[0] == 0)
         {
         if (openRawIO() != 0)
            return 0;
         }

      existingSamples =
         (MTI_INT64)((fileIOList[0]->getFileSize() / bytesPerSample) / channelCount);
      MTI_UINT32 existingFields =
         samplesToFields(firstFieldOffset, existingSamples);
      MTI_INT64 sampleCount =
         fieldsToSamples(firstFieldOffset+existingFields, fieldCount);
      MTI_INT64 byteSize = samplesToBytes(sampleCount);

      // Extend the existing audio file and set the new bytes to zero
      if (fileIOList[0]->extendFile(byteSize) != 0)
         {
         // Error: could not create file
         TRACE_0(errout << "Error: could not extend audio file "
                 << mediaIdentifier);
         return 0;  // Return NULL media allocator pointer
         }
      }

   // Create a new media allocator object and return it to caller
   return(new CRawAudioMediaAllocator(this, samplesPerField, 
                                      firstFieldOffset, existingSamples,
                                      fieldCount));
} // extend

int CRawAudioMediaAccess::cancelExtend(CMediaAllocator *mediaAllocator)
{
   return -1; // not implemented yet

} // cancelExtend

CMediaDeallocator* CRawAudioMediaAccess::createDeallocator()
{
   CRawAudioMediaDeallocator *deallocator;

   deallocator = new CRawAudioMediaDeallocator;

   return deallocator;
}

int CRawAudioMediaAccess::deallocate(CMediaDeallocator& deallocator)
{
   int retVal;

   CMediaDeallocList mediaDeallocList =
      (static_cast<CRawAudioMediaDeallocator&>(
         deallocator)).getMediaDeallocList();

   CMediaDeallocList::DeallocList deallocList =
      mediaDeallocList.getDeallocList();

   if (deallocList.size() > 1)
      {
      // mlm: to-do: handle this error; there should only be one entry
      // since we don't intend to handle deleting from the middle of
      // the file.
      return -1;
      }
   else if (deallocList.size() == 0)
      {
      // nothing to do
      return 0;
      }
   else
      {
      if (fileIOList[0] == 0)
         {
         if (openRawIO() != 0)
            return CLIP_ERROR_FILE_OPEN_FAILED;
         }

      MTI_INT64 byteIndex = samplesToBytes(deallocList.front().dataIndex);
      MTI_INT64 byteSize = samplesToBytes(deallocList.front().dataSize);

      if (byteIndex == 0 && byteSize == fileIOList[0]->getFileSize())
         {
         DeleteFileIOList();

         if (DoesFileExist(mediaIdentifier))
            {
            // Delete the Raw Audio Media file
            retVal = CRawAudioFileIO::deleteFile(mediaIdentifier);
            if (retVal != 0)
               {
               // ERROR: File delete failed
               return retVal;
               }
            }
         }
      else
         {
         retVal = fileIOList[0]->shortenFile(byteIndex, byteSize);
         }
      }

   if (retVal != 0)
      return retVal;

   return 0;   // Return success

} // deallocate

//////////////////////////////////////////////////////////////////////
// Testing functions
//////////////////////////////////////////////////////////////////////

void CRawAudioMediaAccess::dump(MTIostringstream &str)
{
   str << "RawAudioMediaAccess " << std::endl;
   str << " Media Type: " << mediaType;
   str << "  Media Identifier: <" << mediaIdentifier << ">" << std::endl;
   str << " bytesPerSample: " << bytesPerSample;
   str << " channelCount: " << channelCount << std::endl;
}


