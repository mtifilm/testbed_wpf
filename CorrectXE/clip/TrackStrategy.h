// hey emacs, this is -*-c++-*-

#ifndef TRACKSTRATEGYH
#define TRACKSTRATEGYH


#include "Clip3TimeTrack.h"
#include <string>

/** @brief same as LogSequence.h:NULL_SEQUENCE_VALUE, but this file cannot depend on anything in playlist */
#define NULL_TRACK_STRATEGY_VALUE (-1)


/** @brief abstract strategy to provide track info to CTimeLine
 *
 */
class MTI_CLIPLIB_API CTrackStrategy
{
 public:
  virtual ~CTrackStrategy() {};

  virtual int getAudioClipFrame(int iFrame) = 0;
  virtual int getAudioFrameCount() = 0;
  virtual bool getAudioLtc(float fFrameArg, bool bRound, int iOffset,
                           std::string &strAudioLtc) = 0;
  virtual int getHandleCount() = 0;
  virtual bool getTelecineKeykode(int iFrame, EKeykodeFormat iFormat,
                                  int iPerfOffset,
                                  std::string &strKeykode,
                                  EKeykodeFilmDimension *ipFilmDimension) = 0;
  virtual void getVideoClipFrame(int iFrame, int &iClipFrame,
                                 int &iClipFramePartial) = 0;
  virtual int getVideoFrameCount() = 0;
  virtual void getTelecineTC (int iFrame, string &strTC) = 0;
};

#endif
