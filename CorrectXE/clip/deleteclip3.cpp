// deleteclip3.cpp : Defines the entry point for the console application.
//
// Test program for the CClipManager::deleteClip function
//
/*
$Header: /usr/local/filmroot/clip/source/deleteclip3.cpp,v 1.2 2002/02/11 22:24:28 tolks Exp $
*/
//////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include <errno.h>

#include "ClipAPI.h"

//////////////////////////////////////////////////////////////////////

void printUsage()
{
   cerr << "Usage:" << endl;
   cerr << "   1.  deleteclip3 binPath clipName" << endl;
   cerr << "   2.  deleteclip3 fullClipFileName" << endl;
}

//////////////////////////////////////////////////////////////////////

int main(int argc, char* argv[])
{
   CreateApplicationName(argv[0]);

   CBinManager binMgr;
   string binPath, clipName;

   if (argc == 3)
      {
      // Assume the two command line arguments are the Bin and Clip
      binPath = argv[1];
      clipName = argv[2];
      }
   else if (argc == 2)
      {
      // Assume that the single command line argument is the clip's full
      // file name
      string clipFileName = argv[1];
      if (binMgr.splitClipFileName(clipFileName, binPath, clipName) != 0)
         {
         printUsage();
         return 1;
         }
      }
   else 
      {
      // ERROR: wrong number of arguments
      printUsage();  
      return 1;
      }
 
   int status;

   // Initialize the CMediaInterface so media storage can be deallocated
   status = CMediaInterface::initialize();
   if (status !=0)
      {
      cerr << "ERROR: Unable to initialize CMediaInterface " << status << endl;
      return 1;
      }

   // Delete the clip
   status = binMgr.deleteClip(binPath, clipName);
   if (status !=0)
      {
      cerr << "ERROR: Unable to delete clip or deallocate media storage " 
           << status << endl;
      return 1;
      }

   cout << "Clip " << clipName << " in Bin " << binPath 
        << " has been deleted" << endl;

   return 0;
}
