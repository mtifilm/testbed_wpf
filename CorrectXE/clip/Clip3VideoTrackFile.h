// Clip3VideoTrackFile.h: interface for the CVideoFrameListFileReader and
//                        CVideoFrameListFileWriter classes.
//
// Created by: John Starr, November 5, 2001
//
/* CVS Info:
$Header: /usr/local/filmroot/clip/include/Clip3VideoTrackFile.h,v 1.6 2005/11/09 01:38:22 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef CLIP3VIDEOTRACKFILEH
#define CLIP3VIDEOTRACKFILEH

#include "Clip3TrackFile.h"

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CClip;
class CMediaStorageList;
class CImageFormat;
class CVideoClip;
class CVideoField;
class CVideoFrame;
class CVideoFrameList;

//////////////////////////////////////////////////////////////////////
// Video Track File Version
#define FR_VIDEO_TRACK3_FILE_TYPE_FILE_PRE_FIELD_LIST_VERSION (FR_INITIAL_FILE_TYPE_VERSION+2)
#define FR_VIDEO_TRACK3_FILE_TYPE_FILE_VERSION (FR_INITIAL_FILE_TYPE_VERSION+3)

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

// Frame file record schema version
#define FR_VIDEO_FRAME3_FILE_RECORD_PRE_FIELD_LIST_VERSION (FR_INITIAL_RECORD_VERSION+2)
#define FR_VIDEO_FRAME3_FILE_RECORD_LATEST_VERSION (FR_INITIAL_RECORD_VERSION+3)

#define VIDEO_FRAME3_RECORD_RESERVED_COUNT    2

//////////////////////////////////////////////////////////////////////

class CVideoFrameFileRecord : public CFrameFileRecord
{
public:
   CVideoFrameFileRecord(MTI_UINT16 version
                             = FR_VIDEO_FRAME3_FILE_RECORD_LATEST_VERSION);
   virtual ~CVideoFrameFileRecord();

   virtual int read(CStructuredStream& stream);
   virtual int write(CStructuredStream &stream);

   MTI_UINT8 getImageDataFlag() const;

   void setImageDataFlag(MTI_UINT8 newImageDataFlag);

   void calculateRecordByteCount();

private:
   MTI_UINT8 imageDataFlag;

   MTI_UINT32 videoFrameReserved[VIDEO_FRAME3_RECORD_RESERVED_COUNT];
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

// Field file record schema version
#define FR_VIDEO_FIELD3_FILE_RECORD_PRE_FIELD_LIST_VERSION (FR_INITIAL_RECORD_VERSION+2)
#define FR_VIDEO_FIELD3_FILE_RECORD_LATEST_VERSION (FR_INITIAL_RECORD_VERSION+3)

#define VIDEO_FIELD3_RECORD_RESERVED_COUNT    2

//////////////////////////////////////////////////////////////////////

class CVideoFieldFileRecord : public CFieldFileRecord
{
public:
   CVideoFieldFileRecord(MTI_UINT16 version
                             = FR_VIDEO_FIELD3_FILE_RECORD_LATEST_VERSION);
   virtual ~CVideoFieldFileRecord();

   virtual int read(CStructuredStream& stream);
   virtual int write(CStructuredStream &stream);

   void calculateRecordByteCount();

private:
   MTI_UINT32 videoFieldReserved[VIDEO_FIELD3_RECORD_RESERVED_COUNT];
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class CVideoFrameListFileReader : public CFrameListFileReader
{
public:
   CVideoFrameListFileReader();
   virtual ~CVideoFrameListFileReader();

   int readFrameListFile(CVideoFrameList& videoFrameList);

private:
   int buildFramesFromFile(CVideoFrameList& videoFrameList);

   CVideoField* readField(CVideoFieldFileRecord& fieldRecord,
                          CFieldList *newFieldList,
                          const CMediaStorageList& mediaStorageList);
   CVideoFrame* readFrame(CVideoFrameFileRecord& frameRecord,
                          CImageFormat *imageFormat);

   void setVideoFieldFromFileRecord(CVideoField& field,
                                    CFieldList *newFieldList,
                                    const CMediaStorageList& mediaStorageList,
                                    const CVideoFieldFileRecord &fieldRecord);
   void setVideoFrameFromFileRecord(CVideoFrame& frame,
                                    const CVideoFrameFileRecord &frameRecord);
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class CVideoFrameListFileWriter : public CFrameListFileWriter
{
public:
	CVideoFrameListFileWriter();
	virtual ~CVideoFrameListFileWriter();

   int writeFrameListFile(CVideoFrameList* videoFrameList);

private:
   void setVideoFieldToFileRecord(const CVideoField& field,
                                  const CMediaStorageList &mediaStorageList,
                                  CVideoFieldFileRecord &fieldRecord);
   void setVideoFrameToFileRecord(const CVideoFrame& frame,
                                  CVideoFrameFileRecord &frameRecord);

   int writeField(CVideoField& field,
                  const CMediaStorageList &mediaStorageList);
   int writeFrame(CVideoFrame& frame);
   int writeFrameList(CVideoFrameList* videoFrameList);
};

//////////////////////////////////////////////////////////////////////

#endif // #ifndef CLIP3_VIDEO_TRACK_FILE_H
