//---------------------------------------------------------------------------

#pragma hdrstop

#include "BinDir.h"

#include "BinManager.h"

#include <fileApi.h>

//---------------------------------------------------------------------------
#pragma package(smart_init)

const string clipFileExtension = "clp";     // Clip file extension ".clp"
const string clipIniFileExtension = "ini";  // Clip Initialization (Scheme)


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//
// CBinDir Class Implementation
//
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

//typedef struct _WIN32_FIND_DATA { // wfd
//	 DWORD dwFileAttributes;
//	 FILETIME ftCreationTime;
//	 FILETIME ftLastAccessTime;
//	 FILETIME ftLastWriteTime;
//	 DWORD    nFileSizeHigh;
//	 DWORD    nFileSizeLow;
//	 DWORD    dwReserved0;
//	 DWORD    dwReserved1;
//	 TCHAR    cFileName[ MAX_PATH ];
//	 TCHAR    cAlternateFileName[ 14 ];
//} WIN32_FIND_DATA;

//FILE_ATTRIBUTE_ARCHIVE 	   The file or directory is an archive file or directory.
//FILE_ATTRIBUTE_COMPRESSED 	The file or directory is compressed.
//FILE_ATTRIBUTE_DIRECTORY 	The handle identifies a directory.
//FILE_ATTRIBUTE_ENCRYPTED 	The file or directory is encrypted. For a file, this means that all data streams are encrypted. For a directory, this means that encryption is the default for newly created files and subdirectories.
//FILE_ATTRIBUTE_HIDDEN     	The file or directory is hidden. It is not included in an ordinary directory listing.
//FILE_ATTRIBUTE_NORMAL 	   The file or directory has no other attributes set. This attribute is valid only if used alone.
//FILE_ATTRIBUTE_OFFLINE    	The file data is not immediately available. Indicates that the file data has been physically moved to offline storage.
//FILE_ATTRIBUTE_READONLY   	The file or directory is read-only. Applications can read the file but cannot write to it or delete it. In the case of a directory, applications cannot delete it.
//FILE_ATTRIBUTE_REPARSE_POINT 	The file has an associated reparse point.
//FILE_ATTRIBUTE_SPARSE_FILE 	The file is a sparse file.
//FILE_ATTRIBUTE_SYSTEM 	   The file or directory is part of the operating system or is used exclusively by the operating system.
//FILE_ATTRIBUTE_TEMPORARY 	The file is being used for temporary storage. File systems attempt to keep all of the data in memory for quicker access, rather than flushing it back to mass storage. A temporary file should be deleted by the application as soon as it is no longer needed.



//---------------------------------------------------------------------------

CBinDir::CBinDir()
{
	_searchOpen = false;
	_searchHandle = INVALID_HANDLE_VALUE;
	_searchData = nullptr;
	_searchError = ERROR_SUCCESS;
}

CBinDir::~CBinDir()
{
   closeSearch();
	delete _searchData;
}

//---------------------------------------------------------------------------

void CBinDir::closeSearch()
{
	_searchError = ERROR_SUCCESS;

	if (_searchOpen)
	{
		if (FindClose(_searchHandle) == 0)
		{
			// ERROR: FindClose failed
			_searchError = GetLastError();
		}

		_searchHandle = INVALID_HANDLE_VALUE;
		_searchOpen = false;
	}
}
//------------------------------------------------------------------------

bool CBinDir::findFirst(const string &path, int filter)
{
	// Create a path ending with a slash
	_searchPath = AddDirSeparator(path);

	// searchFilter = (filter) ? filter : BIN_SEARCH_FILTER_ANY;
	_searchFilter = filter;

	// Close a previous search if one was previously started
	closeSearch();

	// Clear the binName string
	fileName.erase();
	_searchFileName.erase();

	// Allocate search data if necessary
	if (_searchData == 0)
	{
		_searchData = new WIN32_FIND_DATA;
	}

	// Create a path with * wildcard
	string localPath = _searchPath + "*";

	_searchHandle = FindFirstFile(localPath.c_str(), _searchData);
	if (_searchHandle == INVALID_HANDLE_VALUE)
	{
		// Search has failed
		_searchError = GetLastError();
		return false;
	}

	_searchFileName = _searchData->cFileName;

	_fullSearchFileName = _searchPath + _searchFileName;

	// Remember that a search has been started
	_searchOpen = true;

	// Determine whether the file found matches the search filter
	if (checkSearchFilter())
	{
		// This files matches the search filter, so return
		fileName = _searchFileName;
		return true;
	}
	else
	{
		// This is not a valid Bin directory, so continue searching
		return findNext();
	}

}
//------------------------------------------------------------------------

bool CBinDir::findNext()
{
	// Clear the binName string
	fileName.erase();

	if (!_searchOpen)
	{
		// A valid search has not been started with findFirstBin
		return false;
	}

	// Loop until either a valid Bin directory is found,
	// there are no more files or an error occurs
	while (true)
	{
		bool findStat = FindNextFile(_searchHandle, _searchData);
		if (!findStat)
		{
			// Search has failed, which could indicate either that
			// there are no more files that match search or that
			// there is a serious problem
			_searchError = GetLastError();
			return false;
		}

		_searchFileName = _searchData->cFileName;

		_fullSearchFileName = _searchPath + _searchFileName;

		// Determine whether the file found matches the search filter
		if (checkSearchFilter())
		{
			// This files matches the search filter, so return
			fileName = _searchFileName;
			return true;
		}
	}
}
//---------------------------------------------------------------------------

bool CBinDir::checkSearchFilter()
{
	if (!(_searchFilter & BIN_SEARCH_FILTER_HIDDEN))
	{
		// Reject hidden files
		if (_searchData->dwFileAttributes & FILE_ATTRIBUTE_HIDDEN)
		{
			return false;
		}
	}

	// Always reject current or parent directory
	if (_searchFileName == "." || _searchFileName == "..")
	{
		// This file name is current or parent directory
		return false;
	}

	// Determine whether the file found matches the search filter
	if ((_searchFilter&~BIN_SEARCH_FILTER_HIDDEN) == 0)
	{
		// No filtering, all file names are okay
		return true;
	}
	else if ((_searchFilter & BIN_SEARCH_FILTER_FILE) && checkFile())
	{
		// This is a plain file, so return
		return true;
	}
	else if ((_searchFilter & BIN_SEARCH_FILTER_ANY_DIR) && !checkFile())
	{
		// This is a directory, so return
		return true;
	}
	else if ((_searchFilter & BIN_SEARCH_FILTER_CLIP) && checkClip())
	{
		// This is a valid Clip directory, so return
		return true;
	}
	else if ((_searchFilter & BIN_SEARCH_FILTER_BIN) && checkBin())
	{
		// This is a valid Bin directory, so return
		return true;
	}

	return false;
}
//---------------------------------------------------------------------------

bool CBinDir::checkFile()
{
	if (_searchData->dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
	{
		// This file is a directory, so it cannot be a regular file
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------

bool CBinDir::checkBin()
{
	if ((_searchData->dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0)
	{
		// This file is not a directory, so it cannot be a Bin
		return false;
	}

	// If the directory contains a ".clp" file or a file named .not_a_bin,
	// then it's not a bin!
	CBinDir binDir;
	string clpFilename = _searchFileName + '.' + getClipFileExtension();
	const string notABinFilename = ".not_a_bin";
	bool foundAFile = binDir.findFirst(_fullSearchFileName, BIN_SEARCH_FILTER_FILE|BIN_SEARCH_FILTER_HIDDEN);
	while (foundAFile)
	{
		if (binDir.fileName == clpFilename || binDir.fileName == notABinFilename)
		{
			break;
		}

		foundAFile = binDir.findNext();
	}

	return !foundAFile;

	return true;
}
//---------------------------------------------------------------------------

bool CBinDir::checkClip()
{
	if ((_searchData->dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0)
	{
		// This file is not a directory, so it cannot be a Clip
		return false;
	}

	string localPath = AddDirSeparator(_fullSearchFileName) + _searchFileName
							 + '.' + getClipFileExtension();

	// Quick and dirty "DoesFileExist"
	return GetFileAttributes(localPath.c_str()) != INVALID_FILE_ATTRIBUTES;
}
//---------------------------------------------------------------------------

int CBinDir::findAll(const string &path, int filter, StringList& resultList)
{
	// Find all items in path that match the filter criteria.  Names are put
	// in resultList.  Number of items found is returned.

	int foundCount = 0;
	bool found;

	found = findFirst(path, filter);

	while (found)
	{
		resultList.push_back(fileName);
		++foundCount;

		found = findNext();
	}

	return foundCount;
}
//---------------------------------------------------------------------------

int CBinDir::countAll(const string &path, int filter)
{
	// Count all items in path that match the filter criteria.
	// Number of items found is returned.

	int foundCount = 0;
	bool found;

	found = findFirst(path, filter);

	while (found)
	{
		++foundCount;

		found = findNext();
	}

	return foundCount;
}
//---------------------------------------------------------------------------

/* static */
string CBinDir::getClipFileExtension()
{
	return clipFileExtension;
}
//---------------------------------------------------------------------------

/* static */
string CBinDir::getClipIniFileExtension()
{
	return clipIniFileExtension;
}
//---------------------------------------------------------------------------

/* static */
bool CBinDir::isBinEmpty(const string &binPath)
{
	CBinDir binDir;

	return !binDir.findFirst(binPath, BIN_SEARCH_FILTER_HIDDEN);
}
//---------------------------------------------------------------------------

/* static */
bool CBinDir::hasAtLeastOneNestedBin(const string &binPath)
{
	CBinDir binDir;

	return binDir.findFirst(binPath, BIN_SEARCH_FILTER_BIN);
}
//---------------------------------------------------------------------------


