/**********************************************************************/
/********************* ClipIdentifier.cpp *****************************/
/**********************************************************************/

#include "ClipIdentifier.h"

#include "BinDir.h"
#include "err_clip.h"
#include "FileSweeper.h"
#include "IniFile.h"
#include "machine.h"

/////////////////////////////////////////////////////////////////////
// Definition of prefix/suffix for version file names
static const string gVersionFilenamePrefix("(v");
static const string gVersionFilenameSuffix(")");
static const string gVersionFilenameDelim(".");

//////////////////////////////////////////////////////////////////////
//// ClipVersionNumber class implementation
//////////////////////////////////////////////////////////////////////

ClipVersionNumber::ClipVersionNumber()
{
}

// Init from a string like (v1.2.3) -
// This assumes that each version clip name has the full version number
// for that level, i.e. something likt bin/clip/(v1)/(v1.2)/(v1.2.1)
ClipVersionNumber::ClipVersionNumber(const string &versionNameString)
{
   // Only need to look at the final component
   string fileName = GetFileNameWithExt(versionNameString);
   this->initFromName(fileName);
}

ClipVersionNumber::ClipVersionNumber(int subVersionNumber)
{
   this->versionVector.push_back(subVersionNumber);
}

ClipVersionNumber::ClipVersionNumber(const ClipVersionNumber &aVersionNumber)
: versionVector(aVersionNumber.versionVector)
{
}

ClipVersionNumber ClipVersionNumber::operator=(const ClipVersionNumber &rhs)
{
   if (this != &rhs)
   {
      this->versionVector = rhs.versionVector;
   }

   return *this;
}

bool ClipVersionNumber::IsValid() const
{
   return this->versionVector.size() > 0;
}

ClipVersionNumber ClipVersionNumber::GetParentVersionNumber() const
{
   ClipVersionNumber retVal(*this);
   if (retVal.IsValid())
   {
      retVal.versionVector.pop_back();
   }

   return retVal;
}

int ClipVersionNumber::GetFinalSubVersionNumber() const
{
   MTIassert(this->IsValid());
   if (!this->IsValid())
   {
      return 0;
   }

   size_t deepestLevel = this->versionVector.size() - 1;
   return this->versionVector.at(deepestLevel);
}

ClipVersionNumber ClipVersionNumber::Next() const
{
   if (!this->IsValid())
   {
      return ClipVersionNumber(1);
   }

   int newFinalSubVersionNumber = this->GetFinalSubVersionNumber() + 1;
   return this->GetParentVersionNumber() + newFinalSubVersionNumber;
}

ClipVersionNumber ClipVersionNumber::Previous() const
{
   int newFinalSubVersionNumber = this->GetFinalSubVersionNumber() - 1;
   return (newFinalSubVersionNumber == 0)
          ? this->GetParentVersionNumber()
          : this->GetParentVersionNumber() + newFinalSubVersionNumber;
}

string ClipVersionNumber::ToDisplayName() const
{
   MTIostringstream os;

   os << gVersionFilenamePrefix;

   for (int i = 0; i < this->versionVector.size(); ++i)
   {
      if (i > 0)
      {
         os << ".";
      }

      os << this->versionVector.at(i);
   }

   os << gVersionFilenameSuffix;

   return os.str();
}

string ClipVersionNumber::ToRelativePath() const
{
   string path;
   ClipVersionNumber tempVersionNumber;

   for (int i = 0; i < this->versionVector.size(); ++i)
   {
      tempVersionNumber = tempVersionNumber + this->versionVector.at(i);

      MTIostringstream os;
      os << tempVersionNumber.ToDisplayName();
      if (i == 0)
      {
         path = os.str();
      }
      else
      {
         path = AddDirSeparator(path) + os.str();
      }
   }

   return path;
}

// Returns the concatenation of this version number and another one.
// For example, 1.2 + 3.4 = 1.2.3.4
ClipVersionNumber ClipVersionNumber::operator+(const ClipVersionNumber &rhs) const
{
   if (!this->IsValid())
   {
      // This version number is empty!
      return rhs;
   }

   ClipVersionNumber retVal(*this);
   for (int i = 0; i < rhs.versionVector.size(); ++i)
   {
      retVal.versionVector.push_back(rhs.versionVector[i]);
   }

   return retVal;
}

// Adds a new level to the version number
ClipVersionNumber ClipVersionNumber::operator+(int rhs) const
{
   ClipVersionNumber retVal(*this);
   retVal.versionVector.push_back(rhs);

   return retVal;
}

bool ClipVersionNumber::operator==(const ClipVersionNumber &rhs) const
{
   return this->ToDisplayName() == rhs.ToDisplayName();
}

bool ClipVersionNumber::operator!=(const ClipVersionNumber &rhs) const
{
   return !(*this == rhs);
}

// Init from a string like (v1.2.3) . Do NOT call with a path!
void ClipVersionNumber::initFromName(const string &versionNumberString)
{
   MTIassert(versionNumberString.find_first_of("/\\") == string::npos);
   this->versionVector.clear();

   const size_t prefixSize = gVersionFilenamePrefix.size();
   const size_t suffixSize = gVersionFilenameSuffix.size();
   const size_t suffixPos = versionNumberString.size() - suffixSize;

   if (versionNumberString.size() <= (prefixSize + suffixSize)
   || versionNumberString.substr(0, prefixSize) !=  gVersionFilenamePrefix
   || versionNumberString.substr(suffixPos, suffixSize) !=  gVersionFilenameSuffix)
   {
      return; // Invalid!!
   }

   // Here intentionally left out "0"
   size_t pos = versionNumberString.find_first_of("123456789");
   while (pos != string::npos)
   {
      string numberAsString;
      int number;
      size_t len;

      size_t newPos = versionNumberString.find(gVersionFilenameDelim, pos);
      if (newPos == string::npos)
      {
         len = versionNumberString.length() - pos;
      }
      else
      {
         len = newPos - pos;
      }

      numberAsString = versionNumberString.substr(pos, len);
      number = atoi(numberAsString.c_str());
      MTIassert(number > 0);
      if (number > 0)
      {
         this->versionVector.push_back(number);
      }

      if (newPos == string::npos)
      {
         pos = newPos;
      }
      else
      {
         pos = newPos + 1;
      }
   }
}

//////////////////////////////////////////////////////////////////////
//// ClipIdentifier class implementation
//////////////////////////////////////////////////////////////////////

ClipIdentifier::ClipIdentifier()
{
}

ClipIdentifier::ClipIdentifier(const string &clipPath)
{
   SplitFullPath(clipPath, this->binPath, this->clipName, this->versionNumber);
}

ClipIdentifier::ClipIdentifier(const string &parentPath, const string &clipName)
{
   string clipPath = AddDirSeparator(parentPath) + clipName;
   SplitFullPath(clipPath, this->binPath, this->clipName, this->versionNumber);
}

// Operator+ tacks the version number on the end of the one in the ID
// (i.e. doesn't REPLACE the version number!).
ClipIdentifier::ClipIdentifier(const string &clipPath, const ClipVersionNumber &aVersionNumber)
: ClipIdentifier(ClipIdentifier(clipPath) + aVersionNumber)
{
}

ClipIdentifier::ClipIdentifier(const ClipIdentifier &aClipIdentifier)
{
   this->binPath = aClipIdentifier.GetBinPath();
   this->clipName = aClipIdentifier.GetMasterClipName();
   this->versionNumber = aClipIdentifier.GetVersionNumber();
}

ClipIdentifier ClipIdentifier::operator=(const ClipIdentifier &rhs)
{
   if (this != &rhs)
   {
      this->binPath = rhs.GetBinPath();
      this->clipName = rhs.GetMasterClipName();
      this->versionNumber = rhs.GetVersionNumber();
   }

   return *this;
}

bool ClipIdentifier::IsEmpty() const
{
   return this->clipName.empty();
}

bool ClipIdentifier::IsVersionClip() const
{
   return this->versionNumber.IsValid();
}

string ClipIdentifier::GetBinPath() const
{
   return this->binPath;
}

string ClipIdentifier::GetBinName() const
{
   return GetFileNameWithExt(this->GetBinPath());
}

string ClipIdentifier::GetClipPath() const
{
   string masterClipPath = this->GetMasterClipPath();
   return this->versionNumber.IsValid()
          ? AddDirSeparator(masterClipPath) + this->versionNumber.ToRelativePath()
          : masterClipPath;
}

string ClipIdentifier::GetClipName() const
{
   return GetFileNameWithExt(this->GetClipPath());
}

string ClipIdentifier::GetMasterClipPath() const
{
   return AddDirSeparator(this->binPath) + this->clipName;
}

string ClipIdentifier::GetMasterClipName() const
{
   return this->clipName;
}

string ClipIdentifier::GetParentPath() const
{
   if (!this->versionNumber.IsValid())
   {
      return this->GetBinPath();
   }

   string masterClipPath = this->GetMasterClipPath();
   ClipVersionNumber parentVersionNumber(this->versionNumber.GetParentVersionNumber());

   return parentVersionNumber.IsValid()
          ? AddDirSeparator(masterClipPath) + parentVersionNumber.ToRelativePath()
          : masterClipPath;
}

string ClipIdentifier::GetParentName() const
{
   return GetFileNameWithExt(this->GetParentPath());
}

ClipIdentifier ClipIdentifier::GetMasterClipId() const
{
   return ClipIdentifier(this->GetMasterClipPath());
}

ClipIdentifier ClipIdentifier::GetParentClipId() const
{
   return ClipIdentifier(this->GetParentPath());
}

ClipIdentifier ClipIdentifier::GetNextSiblingClipId() const
{
   return ClipIdentifier(this->GetMasterClipPath(), this->versionNumber.Next());
}

ClipIdentifier ClipIdentifier::GetPreviousSiblingClipId() const
{
   return ClipIdentifier(this->GetMasterClipPath(), this->versionNumber.Previous());
}

string ClipIdentifier::ToString() const
{
   return this->GetClipPath();
}

string ClipIdentifier::GetVersionDisplayName() const
{
   return this->versionNumber.ToDisplayName();
}

ClipVersionNumber ClipIdentifier::GetVersionNumber() const
{
   return this->versionNumber;
}

string ClipIdentifier::GetFullDisplayString() const
{
	string result = GetClipName();
	if (IsVersionClip())
	{
		result = GetParentName() + string(" ") + GetVersionDisplayName();
	}

	return result;
}

ClipIdentifier ClipIdentifier::operator+(const ClipVersionNumber &rhs) const
{
   ClipVersionNumber newVersionNumber = this->versionNumber + rhs;
   return AddDirSeparator(this->GetMasterClipPath()) + newVersionNumber.ToRelativePath();
}

bool ClipIdentifier::operator==(const ClipIdentifier &rhs) const
{
   return this->GetClipPath() == rhs.GetClipPath();
}

bool ClipIdentifier::operator!=(const ClipIdentifier &rhs) const
{
   return !(*this == rhs);
}

/* static */ void ClipIdentifier::SplitFullPath(
   const string &stringToSplit,
   string &binPath,                   // Output
   string &masterClipName,            // Output
   ClipVersionNumber &versionNumber)  // Output
{
   // Canonicalize the input string by dealing with clip naming stupidity.
   // Input forms: directoryPath/clipName  (the canonical form)
   //              directoryPath/clipName/clipName
   //              directoryPath/clipName/clipName.clp
   //
   string directoryPath1 = RemoveDirSeparator(GetFilePath(stringToSplit));
   string directoryName1 = GetFileNameWithExt(directoryPath1);
	string directoryPath2 = RemoveDirSeparator(GetFilePath(directoryPath1));
   string canonicalName;

	string dotExt = "." + CBinDir::getClipFileExtension();
	if (GetFileExt(stringToSplit) == dotExt)
   {
      // There is a ".clp" extension, so the input string is of the form
      // "binPath/clipName/clipName.clp". Lop off extraneous "clipName.clp".
      MTIassert(GetFileName(stringToSplit) == directoryName1);
      canonicalName = directoryPath1;
   }
   else
   {
      // There is no extension or the extension is not .clp, so assume that
      // the clip file name is of the form "directoryPath/clipName/clipName"
      // or "directoryPath/clipName".
      // Completely bogus directory existence check is to allow clips that
      // are named the same as the bin they are in!!!
      string clipName = GetFileNameWithExt(stringToSplit);
      string dirPath = (clipName == directoryName1 && !DoesDirectoryExist(stringToSplit))
                        ? directoryPath2    // remove extraneous clipName
                        : directoryPath1;

      canonicalName = AddDirSeparator(dirPath) + clipName;
   }

   string masterClipPath = ExtractMasterClipPath(canonicalName);
   binPath = RemoveDirSeparator(GetFilePath(masterClipPath));
   masterClipName = GetFileNameWithExt(masterClipPath);
   versionNumber = ClipVersionNumber(canonicalName);
}

/* static */ string ClipIdentifier::ExtractMasterClipPath(const string &fullPath)
{
   string clipPath = RemoveDirSeparator(fullPath);
   for (ClipVersionNumber versionNumber = ClipVersionNumber(clipPath);
        versionNumber.IsValid() && !clipPath.empty();
        versionNumber = ClipVersionNumber(clipPath))
   {
      clipPath = RemoveDirSeparator(GetFilePath(clipPath));
   }

   return clipPath;
}

UUID ClipIdentifier::getClipUuid()
{
   auto clipPath = AddDirSeparator(GetClipPath()) + GetClipName() + ".clp";
   CIniFile *ini = CreateIniFile(clipPath, true); // true = R/O
   if (ini == nullptr)
   {
      return UuidSupport::NilUuid;
   }

   // The section and key should really com from the clip but that takes too much
   // refactoring.
   auto guidString = ini->ReadString("ClipInformation", "ClipGUID", UuidSupport::NullGuidString);
   auto c = guidString.c_str();

   // The guid is the same for some unknown reason;
   return UuidSupport::StdStringToUuid(guidString);
}


std::ostream& operator<< (std::ostream& os, const ClipIdentifier& clipId)
{
	os << clipId.GetClipPath();
	return os;
}
//---------------------------------------------------------------------------

