//---------------------------------------------------------------------------

#ifndef HistoryLocationH
#define HistoryLocationH
//---------------------------------------------------------------------------
#include <string>
using std::string;

//---------------------------------------------------------------------------

class HistoryLocation
{
public:

   string computeHistoryLocation(const string &imageFileDirectory);
   string computeMetadataIniFilePath(const string &imageFileDirectory);
   int updateHistoryLocationIfNecessary(const string &metaDataIniFilePath, const string &historyDirectory);
};
//---------------------------------------------------------------------------

#endif
