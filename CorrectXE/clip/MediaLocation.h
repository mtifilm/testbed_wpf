// MediaLocation.h: interface for the CMediaLocation class.
//
//////////////////////////////////////////////////////////////////////

#ifndef MEDIALOCATIONH
#define MEDIALOCATIONH

#include <iostream>

using std::ostream;

#include "machine.h"
#include "cliplibint.h"
#include "MediaInterface.h"
#include "MediaFrameBuffer.h"

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CMediaAccess;
//class MediaFrameBuffer;

//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CMediaLocation
{
public:
   CMediaLocation();
   CMediaLocation(CMediaAccess *newMediaAccess, MTI_INT64 newDataIndex,
                  MTI_UINT32 newDataSize, const char* newFieldName = 0);
   CMediaLocation(const CMediaLocation& mediaLocation);
   virtual ~CMediaLocation();

   // Assignment Operator
   CMediaLocation& operator=(const CMediaLocation& mediaLocation);

   // Comparison Operators
   bool operator==(const CMediaLocation& rhs) const;
   bool operator!=(const CMediaLocation& rhs) const;
   bool operator>(const CMediaLocation& rhs) const;
   bool operator<(const CMediaLocation& rhs) const;
   int compare(const CMediaLocation& rhs) const;

   // Read from/Write to Media Storage
   int read(MTI_UINT8* buffer);
   int read(MTI_UINT8* buffer, int fileHandleIndex);
	int readWithOffset(MTI_UINT8* buffer, MTI_UINT8* &offsetBuffer);
// xxxmfioxxx
//   bool canBeReadAsynchronously();
//   int startAsynchReadWithOffset(MTI_UINT8 *buffer,
//                                 MTI_UINT8* &offsetBuffer,
//                                 void* &readContext,
//                                 bool dontNeedAlpha);
//   bool isAsynchReadWithOffsetDone(void* readContext);
//   int finishAsynchReadWithOffset(void* &readContext,
//                                  MTI_UINT8* &offsetBuffer);
// xxxmfioxxx
	int readMediaAsQuicklyAsPossible(MTI_UINT8* buffer);
   int readMediaAsQuicklyAsPossibleMT(MTI_UINT8* buffer, int fileHandleIndex);
   int write(MTI_UINT8* buffer);

// xxxmfioxxx
//	bool canReadFrameBuffers();
//	int startAsyncFrameBufferRead(void* &readContext);
//	bool isAsyncFrameBufferReadDone(void* readContext);
//	int finishAsyncFrameBufferRead(void* &readContext, MediaFrameBuffer* &frameBufferOut);
// xxxmfioxxx

	int fetchMediaFile();
	int readMediaFrameBuffer(MediaFrameBufferSharedPtr &frameBuffer);
	int writeMediaFrameBuffer(MediaFrameBufferSharedPtr frameBuffer);

   // Hack Media Storage
   int hack(EMediaHackCommand hackCommand);

   //** Clip-to-clip copyback hack -- Destroy media storage
   int destroy();
   //**//

   // Accessor functions
   MTI_UINT32 getDataByteSize() const;
   MTI_UINT32 getDataOffset() const;
   MTI_INT64 getDataIndex() const;
   MTI_UINT32 getDataSize() const;
   const char* getFieldName() const;
   MTI_UINT32 getFieldNumber() const;
   MTI_UINT8 getFlags() const;           // Apps shouldn't call this
   CMediaAccess* getMediaAccess() const;

   void getQuickReadLocation(int quickReadFlag, 
                             CMediaLocation& adjustedMediaLocation) const;

   bool isImported() const;    // Was media data imported?
   bool isInitialized() const; // Was media storage initialized?
   bool isMarked() const;      // Is Media Storage "marked" or allocated?
   bool isTwoFields() const;   // Are we trying to read/write two fields here?
   bool isSecondField() const; // This is the second field in the frame
   bool isValid() const;       // Is CMediaLocation info valid?
   bool isMediaTypeImageFile();
   bool isMediaTypeImageRepository();  // HAAACK

   int Peek(string &filename, MTI_INT64 &offset);

   void setDataIndex(MTI_INT64 newDataIndex);
   void setDataSize(MTI_UINT32 newDataSize);
   void setFieldName(const char* newFieldName);
   void setFieldNumber(MTI_UINT32 newFieldNumber);
   void setFlags(MTI_UINT8 newFlags);   // Apps shouldn't call this
   void setImported(bool newFlag);
   void setInitialized(bool newFlag);
   void setMarked(bool newFlag);
   void setTwoFields(bool newFlag);
   void setSecondField(bool newFlag);
   void setMediaAccess(CMediaAccess* newMediaAccess);

   string getImageFilePath() const;   // returns "" if not image file based
   string getHistoryFilePath() const; // returns "" if not image file based

   // These are for copying DPX headers from master to version clip frames
   virtual string getSourceImageFilePath() const;
   virtual void setSourceImageFilePath(const string &path);

   void setInvalid();         // Invalidate a CMediaLocation instance

   void dump(MTIostringstream& str) const;   // print object info to stream for debugging

protected:

   CMediaLocation& assign(const CMediaLocation& mediaLocation);

private:

// Media Location Flags (3 defined out of 8 bits available)
#define MEDIA_LOCATION_FLAG_MARKED       0x01  // Media storage is "marked" and
                                                 // has not yet been allocated
#define MEDIA_LOCATION_FLAG_IMPORTED     0x02  // Media has been "imported"
                                                 // from an external file
#define MEDIA_LOCATION_FLAG_INITIALIZED  0x04  // Media has been initialized
                                                 // to some known value
#define MEDIA_LOCATION_FLAG_TWO_FIELDS   0x08  // HACK - location represents
                                                 // TWO fields, not one!!
#define MEDIA_LOCATION_FLAG_SECOND_FIELD 0x10  // HACK - this is the second
                                                 // field in its frame
#define MEDIA_LOCATION_FLAG_VALID_BITS   0x1F  // Mask of valid bits
                                                 // NB: Update when new flags
                                                 //     are added
   // Flag helper functions
   bool isFlagSet(MTI_UINT8 bitFlag) const;
   void setFlag(MTI_UINT8 bitFlag, bool newValue);

private:
   CMediaAccess* mediaAccess;  //
   MTI_INT64 dataIndex;    // Index of start of media data in media file
                           // Index represents units that are specific
                           // to the media storage type;
                           //    Raw file: byte offset
                           //    AIFF:     audio sample number
   MTI_UINT32 dataSize;    // Total size of media data.  Size is in units
                           // that are specific to the media storage type
                           //    Raw file: byte count, padded of efficient
                           //              raw disk access
                           //    AIFF:     audio sample count
   char *fieldName;        // Field-specific portion of media file name
                           // e.g., "0005" portion of "movie1.0005.sgi"
                           // If there is no field-specific name, then
                           // this should be a NULL pointer or point to
                           // an empty string
   unsigned fieldNumber : 24; // The zero-based index of this field. It is
                           // actually wrong to put this here because
                           // the index of the field within a track is
                           // really a property of the track, not the field.
                           // But it is convenient, and it's quite analogous
                           // to the field name as described above, so what
                           // the hell.
   unsigned flags : 8;


   char* copyFieldName(const char* newFieldName);
   void deleteFieldName();
};


///////////////////////////////////////////////////////////////////////////
// ** Dave's DPX Header Copy Hack
// There HAS to be a better way...
class MTI_CLIPLIB_API CMediaLocationExt : public CMediaLocation
{
   string DPXHeaderCopyHackSourcePath;

public:
   CMediaLocationExt();
   CMediaLocationExt(const CMediaLocationExt& mediaLocationExt);
   CMediaLocationExt(const CMediaLocation& mediaLocation);
   virtual ~CMediaLocationExt();

   CMediaLocationExt &operator=(const CMediaLocationExt &rhs);
   CMediaLocationExt &operator=(const CMediaLocation &rhs);

   virtual string getSourceImageFilePath() const;
   virtual void setSourceImageFilePath(const string &newPath);
};
// ** //


struct SMediaAllocation
{
   CMediaAccess *mediaAccess;
   CMediaLocation mediaLocation;     // Media location of first field in
                                     // block.  Most usefule for image
                                     // files.
   MTI_INT64 index;     // offset to beginning of allocation block, in bytes
                        // 0 for image files
   MTI_INT64 size;      // size of allocation block, in bytes
                        // 0 for image files
};

///////////////////////////////////////////////////////////////////////////

void SortMediaLocations(int count, int *indexArray,
                        CMediaLocation mediaLocationArray[]);
                        
///////////////////////////////////////////////////////////////////////////

#endif // #ifndef MEDIALOCATION_H

