// RawAudioMediaAccess.h: interface for the CRawAudioMediaAccess class.
//
//////////////////////////////////////////////////////////////////////

#ifndef RAWAUDIOMEDIAACCESS_H
#define RAWAUDIOMEDIAACCESS_H

#include <string>
#include <iostream>

using std::string;
using std::ostream;

#include "machine.h"
#include "MediaAccess.h"

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CMediaAllocator;
class CMediaFormat;
class CMediaLocation;
class CRawAudioFileIO;

//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CRawAudioMediaAccess : public CMediaAccess
{
public:
   CRawAudioMediaAccess(const string& mediaIdentifier, 
                        const CMediaFormat& mediaFormat);
   virtual ~CRawAudioMediaAccess();

   int initialize();

   virtual MTI_UINT32 convertDataSizeToByteCount(MTI_UINT32 dataSize) const;
   
   bool isClipSpecific() const;

   int checkSpace(const CMediaFormat& mediaFormat, MTI_UINT32 frameCount);

   CMediaAllocator* allocate(const CMediaFormat& mediaFormat,
                             const CTimecode& frame0Timecode, 
                             MTI_UINT32 frameCount);
   int cancelAllocation(CMediaAllocator *mediaAllocator);

   CMediaAllocator* extend(const CMediaFormat& mediaFormat,
                           const CTimecode& frame0Timecode,
                           MTI_UINT32 frameCount);
   int cancelExtend(CMediaAllocator *mediaAllocator);

   CMediaDeallocator* createDeallocator();
   int deallocate(CMediaDeallocator& deallocator);

   MTI_UINT32 getAdjustedFieldSize(MTI_UINT32 fieldSize);
   MTI_INT64 adjustToBlocking(MTI_INT64 size, bool roundingFlag);

   virtual int Peek(CMediaLocation &mediaLocation, string &filename,
                MTI_INT64 &offset);

   int queryFreeSpace(MTI_INT64 *totalFreeBytes,
                      MTI_INT64 *maxContiguousFreeBytes);
   int queryFreeSpaceEx(const CMediaFormat& mediaFormat,
                        MTI_INT64 *totalFreeBytes,
                        MTI_INT64 *maxContiguousFreeBytes);
   int queryTotalSpace(MTI_INT64 *totalBytes);

   int CreateNewFileHandle();
   int ReleaseFileHandle(int fileHandleIndex);
   
   int read(MTI_UINT8* buffer, CMediaLocation &mediaLocation);
   int read(MTI_UINT8* buffer, MTI_INT64 offset, unsigned int size);
   int readMT(MTI_UINT8* buffer, CMediaLocation &mediaLocation,
              int fileHandleIndex);
   int readMT(MTI_UINT8* buffer, MTI_INT64 offset, unsigned int size,
              int fileHandleIndex);
   int write(const MTI_UINT8* buffer, CMediaLocation &mediaLocation);
   int write(const MTI_UINT8* buffer, MTI_INT64 offset, unsigned int size);

   MTI_INT64 fieldsToSamples(MTI_UINT32 startingFieldIndex,
                             MTI_UINT32 fields) const;
   MTI_UINT32 samplesToFields(MTI_UINT32 startingFieldIndex,
                              MTI_INT64 samples) const;

   int setFileBytes(MTI_UINT8 value);

   void dump(MTIostringstream& str);

private:
   int openRawIO();
   MTI_UINT32 samplesToBytes(MTI_UINT32 samples) const;
   MTI_INT64 samplesToBytes(MTI_INT64 samples) const;
   int CheckFileHandleIndex(int fileHandleIndex);
   int DeleteFileIOList();

private:
   int bytesPerSample;
   int channelCount;
   double samplesPerField;
   vector<CRawAudioFileIO*> fileIOList;

}; // CRawAudioMediaAccess

//////////////////////////////////////////////////////////////////////

#endif // #ifndef RAWAUDIOMEDIAACCESS_H
