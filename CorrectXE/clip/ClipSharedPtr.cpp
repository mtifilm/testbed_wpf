#include "ClipSharedPtr.h"
#include "IniFile.h"
#include <map>

#ifdef FIND_CLIP_LEAK

static int ClipCreationIndex = 0;
static std::map<ClipSharedPtr *, int> ClipCreationIndexMap;
static bool fuckingCreateTraceEnabled = false;

static void AddToMap(ClipSharedPtr * this_, const char *args)
{
   int index = ++ClipCreationIndex;
   ClipCreationIndexMap[this_] = index;
   if (fuckingCreateTraceEnabled)
   {
      TRACE_0(errout << "CREATE ClipSharedPtr(" << args << ") #" << index << " - active=" << ClipCreationIndexMap.size());
   }
}

static void RemoveFromMap(ClipSharedPtr * this_)
{
   auto iter = ClipCreationIndexMap.find(this_);
   if (iter == ClipCreationIndexMap.end())
   {
      return;
   }

   int index = ClipCreationIndexMap[this_];
   ClipCreationIndexMap.erase(this_);
   TRACE_0(errout << "DELETE ~ClipSharedPtr() " << index << " - active=" << ClipCreationIndexMap.size());
   fuckingCreateTraceEnabled = true;
}

ClipSharedPtr::ClipSharedPtr()
: WrappedClipSharedPtr()
{
   AddToMap(this, "");
}

ClipSharedPtr::ClipSharedPtr(nullptr_t)
: WrappedClipSharedPtr(nullptr)
{
   AddToMap(this, "nullptr");
}

ClipSharedPtr::ClipSharedPtr(CClip *clip)
: WrappedClipSharedPtr(clip)
{
   AddToMap(this, "CClip *");
}

ClipSharedPtr::ClipSharedPtr(const WrappedClipSharedPtr& rhs)
: WrappedClipSharedPtr(rhs)
{
   AddToMap(this, "const WrappedClipSharedPtr&");
}

ClipSharedPtr::ClipSharedPtr(const WrappedClipSharedPtr&& rhs)
: WrappedClipSharedPtr(rhs)
{
   AddToMap(this, "const WrappedClipSharedPtr&&");
}

ClipSharedPtr::~ClipSharedPtr()
{
   RemoveFromMap(this);
}

ClipWeakPtr::ClipWeakPtr() : WrappedClipWeakPtr()
{
   TRACE_0(errout << "ClipWeakPtr()");
}

ClipWeakPtr::ClipWeakPtr(const WrappedClipWeakPtr& rhs)
: WrappedClipWeakPtr(rhs)
{
   TRACE_0(errout << "ClipWeakPtr(const WrappedClipWeakPtr&)");
}

ClipWeakPtr::ClipWeakPtr(const WrappedClipSharedPtr& sharedPtr)
: WrappedClipWeakPtr(sharedPtr)
{
   TRACE_0(errout << "ClipWeakPtr(const WrappedClipSharedPtr&)");
}

ClipWeakPtr::~ClipWeakPtr()
{
   WrappedClipWeakPtr::~WrappedClipWeakPtr();
   TRACE_0(errout << "~ClipWeakPtr()");
}

#endif

