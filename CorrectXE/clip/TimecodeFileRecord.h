// TimecodeFileRecord.h: interface for the CTimecodeFileRecord
//                       and CSMPTETimecodeFileRecord classes.
//
/*
$Header: /usr/local/filmroot/clip/include/TimecodeFileRecord.h,v 1.2 2003/03/24 19:33:29 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef TIMECODEFILERECORD_H
#define TIMECODEFILERECORD_H

#include "FileRecord.h"
#include "SMPTETimecode.h"
#include "timecode.h"

//////////////////////////////////////////////////////////////////////

#define FR_TIMECODE_FILE_RECORD_LATEST_VERSION (FR_INITIAL_RECORD_VERSION+2)

//////////////////////////////////////////////////////////////////////

class CTimecodeFileRecord : public CFileRecord  
{
public:
   CTimecodeFileRecord(
                   MTI_UINT16 version = FR_TIMECODE_FILE_RECORD_LATEST_VERSION);
   virtual ~CTimecodeFileRecord();

   CTimecodeFileRecord& operator=(const CTimecodeFileRecord &tcRecord);

   virtual int read(CStructuredStream& stream);
   virtual int write(CStructuredStream &stream);

   void getTimecode(CTimecode &tc) const;
   const CTimecode& getTimecode() const;

   void setTimecode(const CTimecode& tc);

   void calculateRecordByteCount();

private:
   CTimecode timecode;
};

//////////////////////////////////////////////////////////////////////

#define FR_SMPTE_TIMECODE_FILE_RECORD_LATEST_VERSION (FR_INITIAL_RECORD_VERSION+2)

//////////////////////////////////////////////////////////////////////

class CSMPTETimecodeFileRecord : public CFileRecord
{
public:
   CSMPTETimecodeFileRecord(
             MTI_UINT16 version = FR_SMPTE_TIMECODE_FILE_RECORD_LATEST_VERSION);
   virtual ~CSMPTETimecodeFileRecord();

   CSMPTETimecodeFileRecord& operator=(const CSMPTETimecodeFileRecord &tcRecord);

   virtual int read(CStructuredStream& stream);
   virtual int write(CStructuredStream &stream);

   void getSMPTETimecode(CSMPTETimecode &tc) const;
   const CSMPTETimecode& getSMPTETimecode() const;

   void setSMPTETimecode(const CSMPTETimecode& tc);

   void calculateRecordByteCount();

private:
   CSMPTETimecode timecode;
};

//////////////////////////////////////////////////////////////////////

#endif // #ifndef TIMECODEFILERECORD_H

