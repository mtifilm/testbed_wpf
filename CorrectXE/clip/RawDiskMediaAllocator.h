// RawDiskMediaAllocator.h: interface for the CRawDiskMediaAllocator class.
//
/*
$Header: /usr/local/filmroot/clip/include/RawDiskMediaAllocator.h,v 1.6 2006/03/15 00:29:35 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef RAWDISKMEDIAALLOCATOR_H
#define RAWDISKMEDIAALLOCATOR_H

#include "MediaAllocator.h"

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CRawDiskBlockList;
class CRawMediaAccess;
class CRawOGLVMediaAccess;

//////////////////////////////////////////////////////////////////////

class CRawDiskMediaAllocator : public CMediaAllocator  
{
public:
   CRawDiskMediaAllocator(CRawMediaAccess *mediaAccess, 
                          MTI_INT64 initialOffset,
                          MTI_INT64 newPaddedSize,
                          MTI_UINT32 newPaddedFieldSize,
                          MTI_UINT32 fieldCount);
   CRawDiskMediaAllocator(CRawMediaAccess *mediaAccess,
                          MTI_UINT32 newFieldCount,
                          MTI_UINT32 newPaddedFieldSize,
                          CRawDiskBlockList *allocList);
   CRawDiskMediaAllocator(CRawOGLVMediaAccess *mediaAccess,
                          MTI_INT64 initialOffset,
                          MTI_INT64 newPaddedSize,
                          MTI_UINT32 newPaddedFieldSize,
                          MTI_UINT32 fieldCount);
   virtual ~CRawDiskMediaAllocator();

   void getFieldMediaAllocation(MTI_UINT32 fieldNumber, 
                                CMediaLocation& mediaLocation);

   void getNextFieldMediaAllocation(CMediaLocation& mediaLocation);

   int AssignMediaStorage(C5MediaTrack &mediaTrack);

   CRawDiskBlockList *getAllocList();
   MTI_INT64 getInitialOffset() const;
   MTI_INT64 getPaddedSize() const;

private:
   MTI_INT64 initialOffset;
   MTI_INT64 paddedSize;          // Total size allocated
   MTI_UINT32 paddedFieldSize;    // Padded field size allocated

   CRawDiskBlockList *allocList;

   CRawDiskMediaAllocator();     // Default constructor is deliberately
                                 // inaccessible
};

//////////////////////////////////////////////////////////////////////

#endif // #ifndef RAWDISKMEDIAALLOCATOR_H
