// Clip3VideoField.cpp: implementation of the CVideoField class.
//
//////////////////////////////////////////////////////////////////////

#include "Clip3VideoTrack.h"
#include "MediaAccess.h"
#include "err_clip.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CVideoField::CVideoField()                               // Default Constructor
{

}

CVideoField::CVideoField(const CVideoField& sourceField) // Copy Constructor
: CField(sourceField)
{

}

CVideoField::~CVideoField()
{
}

//////////////////////////////////////////////////////////////////////
// Assignment Operator
//////////////////////////////////////////////////////////////////////

CVideoField& CVideoField::operator=(const CVideoField& rhs)
{
   if (this != &rhs)       // Check for self-assignment
      {
      // Copy base type CField
      CField::operator=(rhs);

      // Copy CVideoField members
      //   ... no members, so nothing to do.  If members are added to
      //       CVideoField, then assigments for those members will go here
      }

   return *this;   // Return reference to this
}

//////////////////////////////////////////////////////////////////////
// Virtual Copy
//////////////////////////////////////////////////////////////////////

int CVideoField::virtualCopy(const CVideoField &srcField,
                             CMediaStorageList &dstMediaStorageList)
{
   int retVal;

   // Make temporary copy of the Field List reference, since
   // the destination Video Proxy has its own Field List.
   CFieldList *tmpFieldList = getFieldList();
   int tmpFieldListEntryIndex = getFieldListEntryIndex();

   // Copy everything from source field
   operator=(srcField);

   // Restore the original Field List reference that was overwritten
   // by the assignment operator
   setFieldListEntryIndex(tmpFieldList, tmpFieldListEntryIndex);

   // Copy the media status from the source field list entry
   // NB: Since this is a static copy of the media status, the virtual
   //     clip will not know that the source field has been written
   //     at a later time.  A better approach would be to have a single
   //     status for a field that was shared by all clips that used that field.
   //     This may be a good application for a real database.
   setMediaWritten(srcField.isMediaWritten());
   setMediaStorageExists(srcField.doesMediaStorageExist());

   // Set virtual flag in field
   setVirtualMedia(true);

   // Make sure that the CTrack that owns this field knows about the
   // Media Access object that is used in this field
   retVal = fixMediaStorageForVirtualCopy(dstMediaStorageList);
   if (retVal != 0)
      return 0;

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

////MTI_UINT32 CVideoField::getDataOffset()
////{
////   return mediaLocation.getDataOffset();
////}


//////////////////////////////////////////////////////////////////////
// Read/Write Media Data Functions
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    readMedia
//
// Description: Read a field's media data from media storage
//              into caller's buffer
//
// Arguments    MTI_UINT8 *buffer  Pointer to media data buffer
//
// Returns:     0 if success
//
//------------------------------------------------------------------------
int CVideoField::readMedia(MTI_UINT8 *buffer)
{
   // Read the field's media data from the media storage into the caller's
   // buffer and return the success/error status
   return (mediaLocation.read(buffer));
}

//------------------------------------------------------------------------
//
// Function:    readMediaWithOffset
//
// Description: Read a field's media data from media storage
//              into caller's buffer
//
// Arguments    MTI_UINT8 *buffer  Pointer to media data buffer
//              MTI_UINT8* &offsetBuffer
//
// Returns:     0 if success
//
//------------------------------------------------------------------------
int CVideoField::readMediaWithOffset(MTI_UINT8 *buffer,
                                     MTI_UINT8* &offsetBuffer)
{
   // Read the field's media data from the media storage into the caller's 
   // buffer and return the success/error status
   return (mediaLocation.readWithOffset(buffer, offsetBuffer));
}

//------------------------------------------------------------------------
//
// Function:    canBeReadAsynchronously
//
// Description: Let's caller know if asynch read is OK to try
//
// Arguments
//
// Returns:     true if OK
//
//------------------------------------------------------------------------
bool CVideoField::canBeReadAsynchronously()
{
	MTIassert(false);
	return false;
}

//------------------------------------------------------------------------
//
// Function:    startReadMediaWithOffsetAsynchronous
//
// Description: Start an asynchronous read of a field's media data
//              from media storage into caller's buffer
//
// Arguments    MTI_UINT8 *buffer         Pointer to media data buffer
//              void*      &readContext   Needs to be passed to 'wait'
//
// Returns:     0 if success
//
//------------------------------------------------------------------------
int CVideoField::startReadMediaWithOffsetAsynchronous(MTI_UINT8 *buffer,
                                                      MTI_UINT8* &offsetBuffer,
                                                      void* &readContext,
                                                      bool dontNeedAlpha)
{
	MTIassert(false);
	return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

//------------------------------------------------------------------------
//
// Function:    isReadMediaWithOffsetAsynchronousDone
//
// Description: Tell caller if the asynchronous read is complete
//
// Arguments    void*      readContext   Output from 'start' function
//
// Returns:     true if done
//
//------------------------------------------------------------------------
bool CVideoField::isReadMediaWithOffsetAsynchronousDone(void* readContext)
{
	MTIassert(false);
	return true;
}

//------------------------------------------------------------------------
//
// Function:    finishReadMediaWithOffsetAsynchronous
//
// Description: Wait for an asynchronous read to finish
//
// Arguments    void*      &readContext   Output from 'start' function
//              MTI_UINT8* &offsetBuffer  Filled-in start of actual data
//
// Returns:     0 if success
//
//------------------------------------------------------------------------
int CVideoField::finishReadMediaWithOffsetAsynchronous(void* &readContext,
                                                     MTI_UINT8* &offsetBuffer)
{
	MTIassert(false);
	return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

//------------------------------------------------------------------------
//
// Function:    canBeReadAsFrameBuffer
//
// Description: Let's caller know if asynch frame buffer read is OK to try
//
// Arguments
//
// Returns:     true if OK
//
//------------------------------------------------------------------------
bool CVideoField::canBeReadAsFrameBuffer()
{
	MTIassert(false);
	return false;
}

//------------------------------------------------------------------------
//
// Function:    startReadMediaFrameBufferAsynchronous
//
// Description: Start an asynchronous read of a field's media data
//              from media storage into a frame buffer
//
// Arguments    void*      &readContext   Needs to be passed to 'wait'
//
// Returns:     0 if success
//
//------------------------------------------------------------------------
int CVideoField::startReadMediaFrameBufferAsynchronous(void* &readContext)
{
	MTIassert(false);
	return CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED;
}

//------------------------------------------------------------------------
//
// Function:    isReadMediaWithOffsetAsynchronousDone
//
// Description: Tell caller if the asynchronous read is complete
//
// Arguments    void*      readContext   Output from 'start' function
//
// Returns:     true if done
//
//------------------------------------------------------------------------
bool CVideoField::isReadMediaFrameBufferAsynchronousDone(void* readContext)
{
	MTIassert(false);
	return true;
}

//------------------------------------------------------------------------
//
// Function:    finishReadMediaFrameBufferAsynchronous
//
// Description: Wait for an asynchronous read to finish
//
// Arguments    void*      &readContext   Output from 'start' function
//              MediaFrameBuffer* &frameBuffer  Filled-in frame buffer
//
// Returns:     0 if success
//
//------------------------------------------------------------------------
int CVideoField::finishReadMediaFrameBufferAsynchronous(void* &readContext, MediaFrameBuffer* &frameBuffer)
{
	MTIassert(false);
	return false;
}

//------------------------------------------------------------------------
//
// Function:    readMediaQuick
//
// Description: Read a field's media data from media storage
//              into caller's buffer 
//
// Arguments    MTI_UINT8 *buffer  Pointer to media data buffer
//              int quickReadFlag
//
// Returns:     0 if success
//
//------------------------------------------------------------------------
int CVideoField::readMediaQuick(MTI_UINT8 *buffer, int quickReadFlag)
{
   int retVal;

   retVal = readMediaQuickMT(buffer, quickReadFlag, 0);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CVideoField::readMediaQuickMT(MTI_UINT8 *buffer, int quickReadFlag,
                                  int fileHandleIndex)
{
   // Adjust the field's media location per the quickReadFlag
   CMediaLocation adjustedMediaLocation;
   mediaLocation.getQuickReadLocation(quickReadFlag, adjustedMediaLocation);

   MTI_UINT32 bufferOffset = adjustedMediaLocation.getDataIndex()
                             - mediaLocation.getDataIndex();

   // Read the field's media data from the media storage into the caller's
   // buffer and return the success/error status
   if (bufferOffset == 0)
      {
      // hack for fast DPX outgest
      return adjustedMediaLocation.readMediaAsQuicklyAsPossibleMT(buffer,
                                                               fileHandleIndex);
      }

   return (adjustedMediaLocation.read(buffer + bufferOffset));
}

//------------------------------------------------------------------------
//
// Function:   writeMedia
//
// Description: Write a field's media data from caller's buffer to
//              media storage
//
// Arguments    MTI_UINT8 *buffer  Pointer to media data buffer
//
// Returns:    0 if success
//
//------------------------------------------------------------------------
int CVideoField::writeMedia(MTI_UINT8 *buffer)
{
   // Write the field's media data from the caller's buffer to the
   // the media storage given the field's media location.  Return
   // the success/error status;
   int retVal;

   // Clip2clip
   if (isWriteProtectedMedia())
      {
      // ERROR: should have been fixed at a higher level
      return CLIP_ERROR_MEDIA_IS_WRITE_PROTECTED;
      }

   // bug 2519 - restore original values on error
   bool oldMediaWrittenFlag = isMediaWritten();
   bool oldMediaStorageExistsFlag = doesMediaStorageExist();

   // bug 2348 - set these first.
   setMediaWritten(true);
   setMediaStorageExists(true);

   retVal = mediaLocation.write(buffer);
   if (retVal != 0)
      {
      // bug 2348 - undo on failure
      // bug 2519 - restore original values
      setMediaWritten(oldMediaWrittenFlag);
      setMediaStorageExists(oldMediaStorageExistsFlag);
		return retVal;
      }

   // bug 2348 - now set before writing
   // Presumably the media has been written to allocated media storage
   //setMediaWritten(true);
   //setMediaStorageExists(true);

   return 0;
}


//////////////////////////////////////////////////////////////////////
// Frame buffer stuff
//////////////////////////////////////////////////////////////////////

int CVideoField::fetchMediaFile()
{
	return mediaLocation.fetchMediaFile();
}

int CVideoField::readMediaFrameBuffer(MediaFrameBufferSharedPtr &frameBuffer)
{
	return mediaLocation.readMediaFrameBuffer(frameBuffer);
}

int CVideoField::writeMediaFrameBuffer(const MediaFrameBufferSharedPtr &frameBuffer,
													const string &sourceImageFilePath)
{
	int retVal;

	if (isWriteProtectedMedia())
	{
      // ERROR: should have been fixed at a higher level
      return CLIP_ERROR_MEDIA_IS_WRITE_PROTECTED;
	}

   CMediaLocationExt mediaLocationExt(getMediaLocation());

   // Presume success
   setMediaWritten(true);
   setMediaStorageExists(true);

	mediaLocationExt.setSourceImageFilePath(sourceImageFilePath);
	retVal = mediaLocationExt.writeMediaFrameBuffer(frameBuffer);
   if (retVal != 0)
	{
      // Undo the flags on failure
      setMediaWritten(false);
      setMediaStorageExists(false);
	}

   return retVal;
}

//** Clip-to-clip copyback hack
//------------------------------------------------------------------------
//
// Function:    transferMediaFrom
//
// Description: Write a field's media data by "moving" it from another field
//
// Arguments    CField *anotherField   The field from which to copy media data
//
// Returns:     0 if success
//
//------------------------------------------------------------------------
int CVideoField::transferMediaFrom(CField *anotherField)
{
   int retVal;

   // Clip2clip
   if (isWriteProtectedMedia())
      {
      // ERROR: should have been fixed at a higher level
      return CLIP_ERROR_MEDIA_IS_WRITE_PROTECTED;
      }

   // Make sure it's a video field!
   CVideoField *anotherVideoField = dynamic_cast<CVideoField *>(anotherField);
   if (anotherVideoField == NULL)
      {
      TRACE_0(errout << "ERROR: Tried to transfer video media from "
                        "non-video field");
      return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR;
      }

   CMediaLocation fromLocation = anotherVideoField->getMediaLocation();
   CMediaLocation toLocation = this->getMediaLocation();
   retVal = CMediaAccess::moveMedia(fromLocation, toLocation); // from, to
   if (retVal != 0)
      {
      TRACE_0(errout << "ERROR: Failed to transfer video media from "
                        "the version to the parent clip");
      return retVal;
      }

   // Ensure correctness of the media status
   setMediaWritten(true);
   setMediaStorageExists(true);

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    copyMediaFrom
//
// Description: Write a field's media data by copying it from another field
//
// Arguments    CField *anotherField   The field from which to copy media data
//
// Returns:     0 if success
//
//------------------------------------------------------------------------
int CVideoField::copyMediaFrom(CField *anotherField)
{
   int retVal;

   // Clip2clip
   if (isWriteProtectedMedia())
      {
      // ERROR: should have been fixed at a higher level
      return CLIP_ERROR_MEDIA_IS_WRITE_PROTECTED;
      }

   // Make sure it's a video field!
   CVideoField *anotherVideoField = dynamic_cast<CVideoField *>(anotherField);
   if (anotherVideoField == NULL)
      {
      TRACE_0(errout << "ERROR: Tried to transfer video media from "
                        "non-video field");
      return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR;
      }

   CMediaLocation fromLocation = anotherVideoField->getMediaLocation();
   CMediaLocation toLocation = this->getMediaLocation();
   retVal = CMediaAccess::copyMedia(fromLocation, toLocation);
   if (retVal != 0)
      {
      TRACE_0(errout << "ERROR: Failed to copy video media from "
                        "the version to the parent clip");
      return retVal;
      }

   return 0;
}

//** Clip-to-clip copyback hack-o-rama-lama-ding-dong
//------------------------------------------------------------------------
//
// Function:    resetDirtyMetadata
//
// Description: Reset version clip dirty field metadata to refer back to the
//              parent's media
//
// Arguments    CField *anotherField   The field from which to copy metadata
//
// Returns:     0 if success
//
//------------------------------------------------------------------------
int CVideoField::resetDirtyMetadata(CField *anotherField)
{
   // Make sure it's a video field!
   CVideoField *anotherVideoField = dynamic_cast<CVideoField *>(anotherField);

   // If no other video field is given, then the media no longer exists
   if (anotherVideoField == NULL)
      {
      // Change the source field to point at the target field as
      // virtual, write-protected media
      setBorrowedMedia(false);
      setWriteProtectedMedia(false);
      setMediaStorageExists(false);
      setMediaWritten(false);
		setDirtyMedia(false);
      // setMediaLocation(????);
      }
   else
      {
      // Change the source field to point at the target field as
      // virtual, write-protected media
      setBorrowedMedia(true);
      setWriteProtectedMedia(true);
      setMediaStorageExists(true);
      setMediaWritten(true);
      setDirtyMedia(false);
      setMediaLocation(anotherVideoField->getMediaLocation());

		const unsigned int externalBufferSize(4096);  // recommended min size
		char externalBuffer[externalBufferSize];
		int retVal1 = anotherVideoField->getFieldList()->updateFieldListFile(
											anotherVideoField->getFieldListEntryIndex(),
											/* fieldCount = */ 1,
											externalBuffer,
											externalBufferSize);
		int retVal2 = getFieldList()->updateFieldListFile(
											getFieldListEntryIndex(),
											/* fieldCount = */ 1,
											externalBuffer,
											externalBufferSize);
		if (retVal1 != 0)
         return retVal1;
      if (retVal2 != 0)
			return retVal2;
      }

   return 0;
}


//** Dave's DPX Header Copy Hack
//------------------------------------------------------------------------
//
// Function:    writeMediaWithDPXHeaderCopyHack
//
// Description: Write a field's media data from caller's buffer to
//              media storage. If it's a DPX file and there is a
//              file path passed in that is also a DPX file, we will
//              copy the DPX heaqder from that file to the one where
//              we are writing the media
//
// Arguments    MTI_UINT8 *buffer  Pointer to media data buffer
//
//             const string &sourceImageFilePath - File from which to copy
//                                 the DPX header, if possible
//
// Returns:    0 if success
//
//------------------------------------------------------------------------
int CVideoField::writeMediaWithDPXHeaderCopyHack(MTI_UINT8 *buffer,
                                         const string &sourceImageFilePath)
{
   int retVal;

   // Clip2clip
   if (isWriteProtectedMedia())
      {
      // ERROR: should have been fixed at a higher level
      return CLIP_ERROR_MEDIA_IS_WRITE_PROTECTED;
      }

	CMediaLocationExt mediaLocationExt(getMediaLocation());

   // Presume success
   setMediaWritten(true);
   setMediaStorageExists(true);

   mediaLocationExt.setSourceImageFilePath(sourceImageFilePath);
   retVal = mediaLocationExt.write(buffer);
   if (retVal != 0)
      {
      // Undo the flags on failure
      setMediaWritten(false);
      setMediaStorageExists(false);
      }

   return retVal;
}

//////////////////////////////////////////////////////////////////////
// Hack Media Data Functions
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    hackMedia
//
// Description: Hack a field's media data as specified by the command
//
// Arguments    EMediaHacks  hackCommand    Specifies how to hack the
//                                          media data
//
// Returns:     0 if success
//
//------------------------------------------------------------------------
int CVideoField::hackMedia(EMediaHackCommand hackCommand)
{
	// Verify hack command
   switch (hackCommand)
      {
      case MEDIA_HACK_HIDE_ALPHA_MATTE:
      case MEDIA_HACK_SHOW_ALPHA_MATTE:
      case MEDIA_HACK_DESTROY_ALPHA_MATTE:
      case MEDIA_HACK_CACHE_FRAME:
         // OK
         break;
      default:
         // Not OK
         return CLIP_ERROR_MEDIA_CANNOT_BE_HACKED_AS_REQUESTED;
      }

   // Verify hackability of this field
   if (isWriteProtectedMedia())
      {
      // ERROR: should have been fixed at a higher level
      return CLIP_ERROR_MEDIA_IS_WRITE_PROTECTED;
      }

	// Hack the field's media data and return the success/error status
   return (mediaLocation.hack(hackCommand));
}

//------------------------------------------------------------------------
//
// Function:    destroyMedia
//
// Description: Destroy a field's video media data
//
// Arguments    N/A
//
// Returns:     0 if success
//
//------------------------------------------------------------------------
int CVideoField::destroyMedia(CMediaAccess *mediaAccessOverride)
{
	int retVal;
	CMediaLocation mediaLocation = getMediaLocation();
	if (mediaAccessOverride)
		{
		MTIassert(*mediaAccessOverride == *(mediaLocation.getMediaAccess()));
		mediaLocation.setMediaAccess(mediaAccessOverride);
		}

	CMediaAccess *mediaAccess = mediaLocation.getMediaAccess();
	if (mediaAccess == NULL)
		return CLIP_ERROR_UNEXPECTED_NULL_RETURN;

	// Verify destroyability of this field
	MTIassert(!isWriteProtectedMedia());
	if (isWriteProtectedMedia())
      {
		// ERROR: should have been fixed at a higher level
		return CLIP_ERROR_MEDIA_IS_WRITE_PROTECTED;
		}

	return mediaAccess->destroy(mediaLocation);
}

//////////////////////////////////////////////////////////////////////
// Testing and Debugging Functions
//////////////////////////////////////////////////////////////////////

void CVideoField::dump(MTIostringstream& str) const
{
   CField::dump(str);
}
