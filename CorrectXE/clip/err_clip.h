// err_clip.h: Definitions of clip library error codes.
//
//////////////////////////////////////////////////////////////////////

#ifndef CLIP_ERRORS_H
#define CLIP_ERRORS_H

//////////////////////////////////////////////////////////////////////
// Error Code Defines (Numeric Order)
//
// Notes: 1) keep numbers in the range of -2000 to -2999
//        2) codes marked as "obsolete" have been moved to the format library

#define CLIP_ERROR_SUCCESS                                  0

// Generic error codes
// NB: Use of these is discouraged in favor of specific error codes
#define CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED                 -2000
#define CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR                -2001
#define CLIP_ERROR_UNEXPECTED_NULL_RETURN                   -2002
#define CLIP_ERROR_UNEXPECTED_NULL_ARGUMENT                 -2003
#define CLIP_ERROR_FILE_CLOSE_FAILED                        -2004
#define CLIP_ERROR_FILE_CREATE_FAILED                       -2005
#define CLIP_ERROR_FILE_OPEN_FAILED                         -2006
#define CLIP_ERROR_FILE_READ_FAILED                         -2007
#define CLIP_ERROR_FILE_WRITE_FAILED                        -2008
#define CLIP_ERROR_FILE_SEEK_FAILED                         -2009

// Specific error codes
#define CLIP_ERROR_INVALID_FRAME_NUMBER                     -2100
#define CLIP_ERROR_CLIP_FILE_DOES_NOT_EXIST                 -2101
#define CLIP_ERROR_CANNOT_OPEN_OR_CREATE_CLIP_FILE          -2102
#define CLIP_ERROR_CLIP_FILE_INFO_SECTION_MISSING           -2103
#define CLIP_ERROR_CLIP_FILE_INFO_SECTION_KEY_MISSING       -2104
#define CLIP_ERROR_CLIP_FILE_INFO_SECTION_BAD_IN_TIMECODE   -2105
#define CLIP_ERROR_CLIP_FILE_INFO_SECTION_BAD_OUT_TIMECODE  -2106
#define CLIP_ERROR_FILE_RENAME_FAILED                       -2107
#define CLIP_ERROR_CLIP_ALREADY_EXISTS                      -2108
#define CLIP_ERROR_MEDIA_ALLOCATION_FAILED                  -2109

#define CLIP_ERROR_CREATE_DIRECTORY_FAILED                  -2110
#define CLIP_ERROR_RAW_DISK_OPEN_FAILED                     -2111
#define CLIP_ERROR_RAW_DISK_READ_FAILED                     -2112
#define CLIP_ERROR_RAW_DISK_WRITE_FAILED                    -2113
#define CLIP_ERROR_RAW_OGLV_DISK_OPEN_FAILED                -2114
#define CLIP_ERROR_RAW_OGLV_DISK_READ_FAILED                -2115
#define CLIP_ERROR_RAW_OGLV_DISK_WRITE_FAILED               -2116
#define CLIP_ERROR_RAW_OGLV_DISK_NOT_AVAILABLE              -2117
#define CLIP_ERROR_RAW_DISK_CFG_OPEN_FAILED                 -2118
#define CLIP_ERROR_RAW_DISK_CFG_MISSING_KEYWORD             -2119

#define CLIP_ERROR_BIN_DOES_NOT_EXIST                       -2120
#define CLIP_ERROR_INVALID_IMAGE_FORMAT_TYPE                -2121
#define CLIP_ERROR_INVALID_PIXEL_PACKING                    -2122
#define CLIP_ERROR_INVALID_PIXEL_COMPONENTS                 -2123 // obsolete
#define CLIP_ERROR_INVALID_LINES_PER_FRAME                  -2124 // obsolete
#define CLIP_ERROR_INVALID_PIXELS_PER_LINE                  -2125 // obsolete
#define CLIP_ERROR_INVALID_PIXEL_ASPECT_RATIO               -2126 // obsolete
#define CLIP_ERROR_INVALID_FRAME_ASPECT_RATIO               -2127
#define CLIP_ERROR_INVALID_VERTICAL_INTERVAL_ROWS           -2128 // obsolete
#define CLIP_ERROR_INVALID_COLOR_SPACE                      -2129 // obsolete

#define CLIP_ERROR_INVALID_CHANNEL_COUNT                    -2130 // obsolete
#define CLIP_ERROR_INVALID_SAMPLE_DATA_PACKING              -2131 // obsolete
#define CLIP_ERROR_INVALID_SAMPLE_DATA_SIZE                 -2132 // obsolete
#define CLIP_ERROR_INVALID_SAMPLE_DATA_TYPE                 -2133 // obsolete
#define CLIP_ERROR_INVALID_SAMPLE_RATE                      -2134 // obsolete
#define CLIP_ERROR_INVALID_SAMPLES_PER_FIELD                -2135 // obsolete
#define CLIP_ERROR_INVALID_HANDLE_COUNT                     -2136
#define CLIP_ERROR_INVALID_MEDIA_TYPE                       -2137
#define CLIP_ERROR_INVALID_TIME_CODE                        -2138
#define CLIP_ERROR_INVALID_CLIP_NAME                        -2139

#define CLIP_ERROR_INVALID_BIN_PATH                         -2140
#define CLIP_ERROR_AUDIO_MEDIA_DIR_CREATE_FAILED            -2141
#define CLIP_ERROR_INVALID_FRAME_RATE                       -2142
#define CLIP_ERROR_BAD_CLIP_FILENAME                        -2143
#define CLIP_ERROR_AUDIO_MEDIA_DIR_NOT_AVAILABLE            -2144
#define CLIP_ERROR_IMAGE_FILE_ALREADY_EXISTS                -2145
#define CLIP_ERROR_IMAGE_FILE_MISSING                       -2146
#define CLIP_ERROR_IMAGE_FILE_BAD_NAME                      -2147
#define CLIP_ERROR_IMAGE_FILE_INCONSISTENT_HEADER           -2148
#define CLIP_ERROR_IMAGE_FILE_SOME_EXIST                    -2149

#define CLIP_ERROR_INVALID_MEDIA_FORMAT_TYPE                -2150
#define CLIP_ERROR_INSUFFICIENT_MEDIA_STORAGE               -2151
#define CLIP_ERROR_RAW_DISK_MEDIA_IDENTIFIER_NOT_AVAILABLE  -2152
#define CLIP_ERROR_RAW_DISK_DUPLICATE_MEDIA_ACCESS_OBJ      -2153
#define CLIP_ERROR_REMOVE_DIRECTORY_FAILED                  -2154
#define CLIP_ERROR_INVALID_SAMPLE_ENDIAN                    -2155 // obsolete
#define CLIP_ERROR_INVALID_HORIZONTAL_INTERVAL_PIXELS       -2156 // obsolete
#define CLIP_ERROR_CLIP_SCHEME_FILE_CREATE_FAILED           -2157
#define CLIP_ERROR_CLIP_SCHEME_FILE_WRITE_FAILED            -2158
#define CLIP_ERROR_CLIP_SCHEME_FILE_NOT_FOUND               -2159

#define CLIP_ERROR_CLIP_SCHEME_FILE_OPEN_FAILED             -2160
#define CLIP_ERROR_INVALID_RECORD_TO_COMPUTER_STATUS        -2161
#define CLIP_ERROR_INVALID_RECORD_TO_VTR_STATUS             -2162
#define CLIP_ERROR_INVALID_TRACK_TYPE                       -2163
#define CLIP_ERROR_INVALID_AUDIO_TRACK_INDEX                -2164
#define CLIP_ERROR_INVALID_VIDEO_PROXY_INDEX                -2165
#define CLIP_ERROR_MAIN_VIDEO_NOT_AVAILABLE                 -2166
#define CLIP_ERROR_INVALID_LAST_MEDIA_ACTION                -2167
#define CLIP_ERROR_INVALID_BITS_PER_COMPONENT               -2168 // obsolete 
#define CLIP_ERROR_CANNOT_OPEN_RFL_FILE                     -2169

#define CLIP_ERROR_CANNOT_PARSE_RFL_FILE                    -2170
#define CLIP_ERROR_CANNOT_WRITE_RFL_FILE                    -2171
#define CLIP_ERROR_DISK_SCHEME_FILE_OPEN_FAILED             -2172
#define CLIP_ERROR_DISK_SCHEME_FILE_WRITE_FAILED            -2173
#define CLIP_ERROR_CANNOT_FIND_CLIP_SCHEME                  -2174
#define CLIP_ERROR_INVALID_AUDIO_MEDIA_DIR                  -2175
#define CLIP_ERROR_CANNOT_FIND_VIDEO_MEDIA_LOCATION         -2176
#define CLIP_ERROR_MISSING_VIDEO_MEDIA_LOCATION             -2177
#define CLIP_ERROR_CANNOT_FIND_DISK_SCHEME                  -2178
#define CLIP_ERROR_AUDIO_TRACK_FILE_READ_ERROR              -2179

#define CLIP_ERROR_AUDIO_TRACK_FILE_WRITE_ERROR             -2180
#define CLIP_ERROR_AUDIO_TRACK_FILE_INVALID_FILE_TYPE       -2181
#define CLIP_ERROR_AUDIO_TRACK_FILE_INVALID_RECORD          -2182
#define CLIP_ERROR_AUDIO_TRACK_FILE_CREATE_FAILED           -2183
#define CLIP_ERROR_AUDIO_TRACK_FILE_CLOSE_FAILED            -2184
#define CLIP_ERROR_VIDEO_TRACK_FILE_READ_ERROR              -2185
#define CLIP_ERROR_VIDEO_TRACK_FILE_WRITE_ERROR             -2186
#define CLIP_ERROR_VIDEO_TRACK_FILE_INVALID_FILE_TYPE       -2187
#define CLIP_ERROR_VIDEO_TRACK_FILE_INVALID_RECORD          -2188
#define CLIP_ERROR_VIDEO_TRACK_FILE_CREATE_FAILED           -2189

#define CLIP_ERROR_VIDEO_TRACK_FILE_CLOSE_FAILED            -2190
#define CLIP_ERROR_INVALID_MAGIC_WORD                       -2191
#define CLIP_ERROR_INVALID_COMMON_HEADER_RECORD             -2192
#define CLIP_ERROR_TIME_TRACK_FILE_CLOSE_FAILED             -2193
#define CLIP_ERROR_TIME_TRACK_FILE_CREATE_FAILED            -2194
#define CLIP_ERROR_TIME_TRACK_FILE_INVALID_FILE_TYPE        -2195
#define CLIP_ERROR_TIME_TRACK_FILE_INVALID_RECORD           -2196
#define CLIP_ERROR_TIME_TRACK_FILE_OPEN_ERROR               -2197
#define CLIP_ERROR_TIME_TRACK_FILE_READ_ERROR               -2198
#define CLIP_ERROR_TIME_TRACK_FILE_WRITE_ERROR              -2199

#define CLIP_ERROR_INVALID_TIME_TRACK_TYPE                  -2200
#define CLIP_ERROR_INVALID_EMPTY_CLIP                       -2201
#define CLIP_ERROR_INVALID_MEDIA_LOCATION                   -2202
#define CLIP_ERROR_CLIP_FILE_MEDIA_STORAGE_MISSING          -2203
#define CLIP_ERROR_REMOVE_FILE_FAILED                       -2204
#define CLIP_ERROR_FIELD_LIST_FILE_CLOSE_FAILED             -2205
#define CLIP_ERROR_FIELD_LIST_FILE_CREATE_FAILED            -2206
#define CLIP_ERROR_FIELD_LIST_FILE_INVALID_FILE_TYPE        -2207
#define CLIP_ERROR_FIELD_LIST_FILE_INVALID_RECORD           -2208
#define CLIP_ERROR_FIELD_LIST_FILE_OPEN_ERROR               -2209

#define CLIP_ERROR_FIELD_LIST_FILE_READ_ERROR               -2210
#define CLIP_ERROR_FIELD_LIST_FILE_WRITE_ERROR              -2211
#define CLIP_ERROR_BAD_HANDLE_TIMECODE                      -2212
#define CLIP_ERROR_KEYKODE_FILM_TYPE_FILE                   -2213
#define CLIP_ERROR_INVALID_AUDIO_SAMPLE_COUNT               -2214
#define CLIP_ERROR_INVALID_AUDIO_SAMPLE_NUMBER              -2215
#define CLIP_ERROR_CLIP_FILE_INFO_SECTION_BAD_CLIP_GUID     -2216
#define CLIP_ERROR_IMAGE_FILE_INCOMPATIBLE_CLIP_SCHEME      -2217
#define CLIP_ERROR_MAX_LICENSE_RESOLUTION_EXCEEDED          -2218
#define CLIP_ERROR_RAW_DISK_CFG_BAD_ALLOC_METHOD            -2219

#define CLIP_ERROR_MEDIA_STORAGE_ALREADY_ASSIGNED           -2220
#define CLIP_ERROR_VIRTUAL_MEDIA_CANNOT_OWN_MEDIA_STORAGE   -2221
#define CLIP_ERROR_INVALID_FRAMING_INDEX                    -2222
#define CLIP_ERROR_INVALID_CLIP_NAME_CHARACTERS             -2223
#define CLIP_ERROR_MEDIA_ACCESS_INVALID                     -2224
#define CLIP_ERROR_MISSING_RENDER_DESTINATION_CLIP_NAME     -2225
#define CLIP_ERROR_RENDER_DESTINATION_CLIP_DOES_NOT_EXIST   -2226
#define CLIP_ERROR_DESTINATION_FORMAT_DOES_NOT_MATCH_SOURCE -2227
#define CLIP_ERROR_RENDER_DESTINATION_CLIP_ALREADY_EXISTS   -2228
#define CLIP_ERROR_MISSING_DISK_SCHEME_NAME                 -2229

#define CLIP_ERROR_RFL_ENTRIES_OVERLAPPING                  -2230
#define CLIP_ERROR_RFL_ENTRIES_OUT_OF_ORDER                 -2231
#define CLIP_ERROR_INVALID_AUDIO_PROXY_CHANNEL_INDEX        -2232
#define CLIP_ERROR_INVALID_AUDIO_PROXY_SAMPLE_INDEX         -2233
#define CLIP_ERROR_IMAGE_REPOSITORY_CFG_OPEN_FAILED         -2234
#define CLIP_ERROR_PULLDOWN_TYPE_UNKNOWN                    -2235
#define CLIP_ERROR_BACKUP_CLIP_FAILED                       -2236
#define CLIP_ERROR_RESTORE_CLIP_FAILED                      -2237
#define CLIP_ERROR_BAD_KEYKODE_MANUFACTURER                 -2238
#define CLIP_ERROR_INSUFFICIENT_METADATA_STORAGE            -2239

#define CLIP_ERROR_IMAGE_REPOSITORY_ALREADY_EXISTS          -2240
#define CLIP_ERROR_IMAGE_REPOSITORY_CFG_WRITE_FAILED        -2241
#define CLIP_ERROR_IMAGE_REPOSITORY_MEDIA_IDENTIFIER_NOT_AVAILABLE -2242
#define CLIP_ERROR_VIDEO_MEDIA_DIR_NOT_AVAILABLE            -2243
#define CLIP_ERROR_DISK_SCHEME_ALREADY_EXISTS               -2244
#define CLIP_ERROR_NO_SUCH_IMAGE_REPOSITORY                 -2245
#define CLIP_ERROR_INVALID_CLIP_VERSION                     -2246
#define CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5         -2247
#define CLIP_ERROR_TRACK_FILE_DOES_NOT_EXIST                -2248
#define CLIP_ERROR_INVALID_FIELD_INDEX                      -2249

#define CLIP_ERROR_NO_MEDIA                                 -2250
#define CLIP_ERROR_INVALID_MEDIA_STATUS                     -2251
#define CLIP_ERROR_CANNOT_CLOSE_RFL_FILE                    -2252
#define CLIP_ERROR_CANNOT_FIND_ACTIVE_CLIP_SCHEME_FILE      -2253
#define CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_3         -2254
#define CLIP_ERROR_MEDIA_CANNOT_BE_HACKED_AS_REQUESTED      -2255
#define CLIP_ERROR_BAD_PROJECT_BIN                          -2256
#define CLIP_ERROR_RENDER_INFO_NOT_AVAILABLE                -2257
#define CLIP_ERROR_MEDIA_CANNOT_BE_DESTROYED                -2258
#define CLIP_ERROR_MEDIA_CANNOT_BE_TRANSFERRED              -2259

#define CLIP_ERROR_MEDIA_IS_WRITE_PROTECTED                 -2260
#define CLIP_ERROR_CLIP_CLONE_SOURCE_DOES_NOT_EXIST         -2261
#define CLIP_ERROR_CLIP_CLONE_TARGET_ALREADY_EXISTS         -2262
#define CLIP_ERROR_CLIP_CLONE_FILE_COPY_FAILED              -2263
#define CLIP_ERROR_NO_DIRTY_MEDIA_STORAGE_SPECIFIED         -2264
#define CLIP_ERROR_UNBALANCED_CLIP_OPEN_CLOSED              -2265
#define CLIP_ERROR_INVALID_MEDIA_FILE_HANDLE_INDEX          -2266
#define CLIP_ERROR_FRAME_LIST_IS_LOCKED                     -2267
#define CLIP_ERROR_FRAME_LIST_IS_CORRUPT                    -2268
#define CLIP_ERROR_ASYNCH_IO_NOT_SUPPORTED_FOR_MEDIA        -2269

#define CLIP_ERROR_BAD_HISTORY_TYPE_OVERRIDE                -2270
#define CLIP_ERROR_CANT_MAKE_HISTORY_DIRECTORY              -2271
#define CLIP_ERROR_HISTORY_CANNOT_BE_DESTROYED              -2272

#define CLIP_ERROR_CLIP_FILE_WAS_MODIFIED                   -2273

//////////////////////////////////////////////////////////////////////
// Error Code Descriptions (Alpha Order)
/*

CLIP_ERROR_AUDIO_MEDIA_DIR_CREATE_FAILED
   Could not create a directory or one of its parent directories
   for a Audio Media file.  Returned by CBinManager::createClip from
   CClip::addAudioTrack.

CLIP_ERROR_AUDIO_MEDIA_DIR_NOT_AVAILABLE
   Could not access the Media Directory for a Audio Media file.
   The directory may not exist. Returned by CBinManager::createClip
   from CClip::addAudioTrack.

CLIP_ERROR_AUDIO_TRACK_FILE_CREATE_FAILED
   Unable to create an Audio Track (Frame List) file with the CFileBuilder

CLIP_ERROR_AUDIO_TRACK_FILE_CLOSE_FAILED
   Unable to close an Audio Track (Frame List) file

CLIP_ERROR_AUDIO_TRACK_FILE_INVALID_FILE_TYPE
   An invalid or unexpected track file type was encountered when
   reading an Audio Track (Frame List) file.

CLIP_ERROR_AUDIO_TRACK_FILE_INVALID_RECORD
   An invalid or unexpected record type was encountered while reading
   an Audio Track (Frame List) file

CLIP_ERROR_AUDIO_TRACK_FILE_READ_ERROR
   Error reading a record from an Audio Track (Frame List) file

CLIP_ERROR_AUDIO_TRACK_FILE_WRITE_ERROR
   Error writing a record to an Audio Track (Frame List) file

CLIP_ERROR_BACKUP_CLIP_FAILED
   Could not backup critical clip files

CLIP_ERROR_BAD_CLIP_FILENAME
   Could not parse a clip filename.  Possibly due to wrong file extension
   or clip directory not the same as the clip name.  Returned by
   CBinManager::splitClipFileName

CLIP_ERROR_BAD_HANDLE_TIMECODE
   A timecode, either in or out, will result in bad timecodes for the handle
   frames.  This could mean that the in timecode is too small, e.g.
   00:00:00:00, to allow handle frames before it or that the timecode is
   too large, e.g., 99:99:99:29, to allow handle frames after it.  This
   is checked in the CClipInitInfo class instance.

CLIP_ERROR_BAD_KEYKODE_MANUFACTURER
   The supplied keykode manufacturer code is illegal.  No such manufacturer
   exists in the keykode ini file.

CLIP_ERROR_BAD_PROJECT_BIN
   Could not parse a clip filename to deterime the name of the project
   directory.  Returned by CBinManager::getProjectBinName

CLIP_ERROR_BIN_DOES_NOT_EXIST
   The target Bin does not exist or is inaccessible.

CLIP_ERROR_CANNOT_FIND_CLIP_SCHEME
   Cannot locate the Clip Scheme of a given name.

CLIP_ERROR_CANNOT_FIND_DISK_SCHEME
   Cannot locate the named Disk Scheme
   
CLIP_ERROR_CANNOT_FIND_VIDEO_MEDIA_LOCATION
   Cannot find a Raw Disk or OGLV Raw Disk that matches the given
   video media location.  This may be caused by a Disk Scheme with a bad
   or obsolete main or proxy video media location.

CLIP_ERROR_CANNOT_OPEN_OR_CREATE_CLIP_FILE
   Attempt to open or create a clip file via CIniFile constructor failed, 
   possibly because the path did not exist or was not accessible.  Returned by
   CBinManager::createClip, CBinManager::deleteClip, CBinManager::openClip,
   CClip::readFile and CClip::writeFile.

CLIP_ERROR_CANNOT_CLOSE_RFL_FILE
   Cannot close the raw disk free list (RFL) file.  

CLIP_ERROR_CANNOT_OPEN_RFL_FILE
   Cannot open the raw disk free list (RFL) file.  Possibly the file
   does not exist (check rawdisk.cfg) or there is a permissions problem.

CLIP_ERROR_CANNOT_PARSE_RFL_FILE
   Cannot parse the contents of the raw disk free list (RFL) file.  The
   parsing of the RFL file is very simple-minded and easily gets confused.
   The file is not intended to be modified by users, but occassionally that
   proves necessary.  The file must be formatted as follows:
     1. Separate line for each free block entry as:
          Location=xxxxxx NumBlock=xxxxxx
     2. Each line, including the last, must be terminated with an end-of-line
     3. There cannot be any blank lines or comments in the files.  Watch
        out for blank lines at the end of the file.
     4. There must always be at least one entry in the file.

CLIP_ERROR_CANNOT_WRITE_RFL_FILE
   Cannot write to the raw disk free list (RFL) file.  Possibly there is
   a permissions problem.

CLIP_ERROR_CLIP_ALREADY_EXISTS
   Attempt to create a new clip that already exists.  Returned by
   CBinManager::createClip.

CLIP_ERROR_CLIP_FILE_DOES_NOT_EXIST
   The clip file does not exist or is not accessible.  Returned by
   CBinManager::openClip and CClip::readFile

CLIP_ERROR_CLIP_FILE_INFO_SECTION_BAD_CLIP_GUID
   A poorly formatted GUID was found in the clip file's ClipInformation
   section

CLIP_ERROR_CLIP_FILE_INFO_SECTION_BAD_IN_TIMECODE
CLIP_ERROR_CLIP_FILE_INFO_SECTION_BAD_OUT_TIMECODE
   The In (Out) Timecode is missing or could not be parsed in the clip
   file's ClipInformation section.

CLIP_ERROR_CLIP_FILE_INFO_SECTION_KEY_MISSING
   A required key is missing from the clip files ClipInformation section.

CLIP_ERROR_CLIP_FILE_INFO_SECTION_MISSING
   The ClipInformation section is missing from the clip file.  Could be
   caused by a corrupted clip file or an attempt to read a file that
   wasn't actually a clip file

CLIP_ERROR_CLIP_FILE_MEDIA_STORAGE_MISSING
   A MediaStorage section is missing from one or more Audio Tracks
   or Video Proxies.  This is normal for virtual clips where the media
   locations have not yet been set from other clips.

CLIP_ERROR_CLIP_SCHEME_FILE_CREATE_FAILED
   Unable to create a Clip Scheme file with the CIniFile class.

CLIP_ERROR_CLIP_SCHEME_FILE_NOT_FOUND
   Clip Scheme file does not exist

CLIP_ERROR_CLIP_SCHEME_FILE_OPEN_FAILED
   Could not open a Clip Scheme file or create a new instance
   of a CIniFile for reading the Clip Scheme file

CLIP_ERROR_CLIP_SCHEME_FILE_WRITE_FAILED
   Unable to write to a Clip Scheme file with the CIniFile class.
   
CLIP_ERROR_CREATE_DIRECTORY_FAILED
   Unable to create a new directory for a bin or a clip.  Returned by
   CBinManager::createBin and CBinManager::createClip.  Specific cause
   can be found in errno

CLIP_ERROR_DESTINATION_FORMAT_DOES_NOT_MATCH_SOURCE
   The render destination clip's format is not compatible with
   the source clip

CLIP_ERROR_DISK_SCHEME_FILE_OPEN_FAILED
   Could not open the Disk Scheme file with CreateIniFile.

CLIP_ERROR_DISK_SCHEME_FILE_WRITE_FAILED
   Could not write or possibly close the disk scheme ini file. Possibly a
   permissions problem.
   
CLIP_ERROR_FIELD_LIST_FILE_CREATE_FAILED
   Unable to create an Field List file with the CFileBuilder

CLIP_ERROR_FIELD_LIST_FILE_CLOSE_FAILED
   Unable to close an Field List file

CLIP_ERROR_FIELD_LIST_FILE_INVALID_FILE_TYPE
   An invalid or unexpected file type was encountered when
   reading an Field List file.

CLIP_ERROR_FIELD_LIST_FILE_INVALID_RECORD
   An invalid or unexpected record type was encountered while reading
   an Field List file

CLIP_ERROR_FIELD_LIST_FILE_OPEN_FAILED
   Unable to open an Field List file

CLIP_ERROR_FIELD_LIST_FILE_READ_ERROR
   Error reading a record from an Field List file

CLIP_ERROR_FIELD_LIST_FILE_WRITE_ERROR
   Error writing a record to an Field List file

CLIP_ERROR_FILE_CLOSE_FAILED
   File close performed by the clip library failed.  This is a generic file
   error code and so could be for a file operation on any of the file types
   that the clip library handles, including .clp files, .ini files,
   track (.trk) files, etc.  Use of this error code is discouraged.

CLIP_ERROR_FILE_CREATE_FAILED
   File create performed by the clip library failed.  This is a generic file
   error code and so could be for a file operation on any of the file types
   that the clip library handles, including .clp files, .ini files,
   track (.trk) files, etc.  Use of this error code is discouraged.

CLIP_ERROR_FILE_OPEN_FAILED
   File open performed by the clip library failed.  This is a generic file
   error code and so could be for a file operation on any of the file types
   that the clip library handles, including .clp files, .ini files,
   track (.trk) files, etc.  Use of this error code is discouraged.

CLIP_ERROR_FILE_READ_FAILED
   File read performed by the clip library failed.  This is a generic file
   error code and so could be for a file operation on any of the file types
   that the clip library handles, including .clp files, .ini files,
   track (.trk) files, etc.  Use of this error code is discouraged.

CLIP_ERROR_FILE_RENAME_FAILED
   Unable to rename a file.  Specific cause can be found in errno.
   Returned by CBinManager::deleteClip and CClip::deleteClipFile when
   they attempt to "delete" clip files by renaming them to obscure hidden files.

CLIP_ERROR_FILE_SEEK_FAILED
   File seek performed by the clip library failed. This is a generic file
   error code and so could be for a file operation on any of the file types
   that the clip library handles, including .clp files, .ini files,
   track (.trk) files, etc.  

CLIP_ERROR_FILE_WRITE_FAILED
   File write performed by the clip library failed.  This is a generic file
   error code and so could be for a file operation on any of the file types
   that the clip library handles, including .clp files, .ini files,
   track (.trk) files, etc.

CLIP_ERROR_FUNCTION_NOT_IMPLEMENTED
   A clip library function has been called that is not fully implemented.

CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_3
CLIP_ERROR_FUNCTION_NOT_SUPPORTED_BY_CLIP_5
   A clip library function has been called that is not supported by
   particular version of the clip data structure.

CLIP_ERROR_IMAGE_FILE_ALREADY_EXISTS
   An Image File (DPX, SGI, etc) was found to already exist when a new file
   with the same name was about to be created by CImageFileMediaAccess::write.

CLIP_ERROR_IMAGE_FILE_BAD_NAME
   An Image File name could not be parsed by the function
   CImageFileMediaAccess:parseFileName.  The file name may be missing a
   frame number.

CLIP_ERROR_IMAGE_FILE_INCOMPATIBLE_CLIP_SCHEME
   A attempt was made to used a clip scheme that is incompatible with
   an existing image file while initializing a CClipInitInfo, probably
   when attempting to create a clip.

CLIP_ERROR_IMAGE_FILE_INCONSISTENT_HEADER
   An Image File's header was found to be inconsistent with the header
   of the first Image File or inconsistent with the clip's image format.

CLIP_ERROR_IMAGE_FILE_MISSING
   An Image File (DPX, SGI, etc.) was found not to exist when it was about
   to be read from or written to by CImageFileMediaAccess::read or write.
   Also returned by CClipInitInfo::initFromImageFile if the caller's
   image file does not exist

CLIP_ERROR_IMAGE_FILE_SOME_EXIST
   One or more Image Files exist where none should exist.

CLIP_ERROR_IMAGE_REPOSITORY_CFG_OPEN_FAILED
   Could not open the Image Repository Configuration file while
   initializing the image repository access. The file may not exist or
   may not have sufficient permissions.

CLIP_ERROR_INSUFFICIENT_MEDIA_STORAGE
   Not enough free space on the media storage device for a new clip.
   Typically occurs when a clip is created.

CLIP_ERROR_INSUFFICIENT_METADATA_STORAGE
   Not enough free space on the metadata storage device for a new clip.

CLIP_ERROR_INVALID_AUDIO_MEDIA_DIR
   The Audio Media directory name in a CDiskScheme or CAudioInitInfo is
   not a proper directory name.

CLIP_ERROR_INVALID_AUDIO_SAMPLE_COUNT
   An excess audio sample count was encountered

CLIP_ERROR_INVALID_AUDIO_SAMPLE_NUMBER
   An out-of-range audio sample number was encountered

CLIP_ERROR_INVALID_AUDIO_TRACK_INDEX
   Audio Track Index is out-of-range, possibly as a CClip member function
   argument.

CLIP_ERROR_INVALID_BIN_PATH
   Missing or invalid Bin Path in a CClipInitInfo class instance

CLIP_ERROR_INVALID_CLIP_NAME
   Missing or invalid Clip Name in a CClipInitInfo class instance

CLIP_ERROR_INVALID_CLIP_NAME_CHARACTERS
   Clip Name in a CClipInitInfo contains invalid characters \/:*?"<>|

CLIP_ERROR_INVALID_CLIP_VERSION
   Invalid clip data structure version.

CLIP_ERROR_INVALID_COMMON_HEADER_RECORD
   One of the clip's binary files (e.g. track/frame list file) had an
   invalid Common Header record.  All MTI CPMP binary files start with
   a Common Header record which identifies the file type and type version.
   This error code is returned by CFileScanner::initForReading.

CLIP_ERROR_INVALID_EMPTY_CLIP
   When a CClipInitInfo class was validated, it was found to have neither
   Main Video nor Audio Tracks.  The clip scheme or .ini file that was
   the source of the CClipInitInfo may be missing MainVideo and AudioTrack
   sections.

CLIP_ERROR_INVALID_FIELD_INDEX
   A field index is less than 0 or exceeds the fields in a frame

CLIP_ERROR_INVALID_FRAME_ASPECT_RATIO
   The Frame Aspect Ratio is invalid in the Image Format section of a Clip
   File or a Clip Scheme

CLIP_ERROR_INVALID_FRAME_NUMBER
   Frame number (index) is out-of-range for the Frame List, that is, it
   is less than 0 or greater than the total frame count - 1.  Returned
   by CFrameList (and its derived types) member functions that read and write
   media.

CLIP_ERROR_INVALID_FRAME_RATE
   The Frame Rate is invalid in a Clip File or Clip Scheme

CLIP_ERROR_INVALID_FRAMING_INDEX
   The framing index for a frame list within a Video Proxy is out of
   range.

CLIP_ERROR_INVALID_HANDLE_COUNT
   The Handle Count is invalid in a Clip File or Clip Scheme file
   
CLIP_ERROR_INVALID_IMAGE_FORMAT_TYPE
   The Image Format type is invalid in the Image Format section of a Clip
   file or a Clip Scheme or as an argument to a CImageFormat class function

CLIP_ERROR_INVALID_LAST_MEDIA_ACTION
   The Last Media Action is invalid in the Video Proxy Info or Audio Track
   Info sections of a Clip file

CLIP_ERROR_INVALID_MAGIC_WORD
   One of the clip's binary files (e.g. track/frame list file) had an
   invalid "Magic Word".  The Magic Word is the first 32-bit word of a
   file and indicates both that the file is a valid MTI CPMP binary file
   and whether the file was written as "big endian" or "little endian".
   This error code is returned by CFileScanner::initForReading.  Valid
   magic words, if read as ASCII text characters, are: MTIX, XITM, MTIY
   or YITM.  Older software does not recognize the magic words MTIY or YITM.

CLIP_ERROR_INVALID_MEDIA_FILE_HANDLE_INDEX
   A media read or write function was called with an invalid media file
   handle index.

CLIP_ERROR_INVALID_MEDIA_STATUS
   An invalid media status was encountered.  This could be caused by a
   corrupted Media Status Track file or an internal error.

CLIP_ERROR_INVALID_MEDIA_TYPE
   The Media Type is invalid in a Clip File or a Clip Scheme
      
CLIP_ERROR_INVALID_MEDIA_FORMAT_TYPE
   Invalid or unexpected CMediaFormat type.  Possibly a corrupted clip
   or clip initialization info.

CLIP_ERROR_INVALID_MEDIA_LOCATION
   Invalid CMediaLocation was encountered.  Possible cause for this error
   is attempting to read or write media with an audio or video field that
   has uninitialized virtual media.

CLIP_ERROR_INVALID_PIXEL_PACKING
   The Pixel Packing type is invalid in the Image Format section of a Clip
   file or a Clip Scheme.

CLIP_ERROR_INVALID_RECORD_TO_COMPUTER_STATUS
   The Record to VTR Status flag is invalid in a Clip file

CLIP_ERROR_INVALID_RECORD_TO_VTR_STATUS
   The Record to Computer Status flag is invalid in a Clip file

CLIP_ERROR_INVALID_TRACK_TYPE
   The Track Type (Audio Track or Video Proxy) in invalid in a Clip File or
   in a function argument

CLIP_ERROR_INVALID_TIME_CODE
   A timecode, either in or out, is invalid in a CClipInitInfo class instance.

CLIP_ERROR_INVALID_VIDEO_PROXY_INDEX
   Video Proxy Index is out-of-range, possibly as a CClip member function
   argument.

CLIP_ERROR_MAIN_VIDEO_NOT_AVAILABLE
   A function that needed information from Main Video (or Video Proxy 0) was
   unable to get that information because the Main Video section was not
   available in a Clip Scheme or the Video Proxy 0 is not available in a Clip.
   This could occur if the Clip Scheme or Clip contain only Audio Tracks and
   no video.  It could also occur if the Clip Scheme or Clip is ill-formed.

CLIP_ERROR_MAX_LICENSE_RESOLUTION_EXCEEDED
   The Requested Clip exceeded the Maximum Licensed Resolution (e.g. it
   was greater than 1920 x 1080).

CLIP_ERROR_MEDIA_ACCESS_INVALID
   The Media Access type of this request is invalid.

CLIP_ERROR_MEDIA_ALLOCATION_FAILED
   Attempt to allocate media storage failed.  Possibly insufficient space
   on the chosen media storage device.  Returned by CBinManager::createClip.

CLIP_ERROR_MEDIA_STORAGE_ALREADY_ASSIGNED
   Attempt to assign media storage to a frame or field failed because the
   frame or field already had media storage assigned.

CLIP_ERROR_MISSING_DISK_SCHEME_NAME
   The disk scheme name is missing, i.e., an empty string

CLIP_ERROR_MISSING_RENDER_DESTINATION_CLIP_NAME
   The name of the destination clip is missing from CRenderDestinationClip,
   i.e., an empty string
   
CLIP_ERROR_MISSING_VIDEO_MEDIA_LOCATION
   A video media location required by a Clip Scheme is missing from
   the Disk Scheme.

CLIP_ERROR_NO_MEDIA
   An attempt to do something (such as Read or Write media) regarding the
   media, when no media exists.  Media may not exist because 1) has not
   been allocated yet 2) was allocated but then deallocated 3) internal error
   4) non-media track

CLIP_ERROR_PULLDOWN_TYPE_UNKNOWN
   An unknown pulldown type (3:2, etc) was found.

CLIP_ERROR_RAW_DISK_CFG_BAD_ALLOC_METHOD
   Bad allocation method for AllocMethod keyword in the Raw Disk Configuration
   file

CLIP_ERROR_RAW_DISK_CFG_MISSING_KEYWORD
   A required keyword was missing from the Raw Disk Configuration file.

CLIP_ERROR_RAW_DISK_CFG_OPEN_FAILED
   Could not open the Raw Disk Configuration file while initializing the
   raw media access. The file may not exist or may not have sufficient
   permissions.

CLIP_ERROR_RAW_DISK_DUPLICATE_MEDIA_ACCESS_OBJ
   More than one CRawMediaAccess instance exists in the Media Access List
   for a given Media Identifier.  This probably indicates an internal software
   problem.

CLIP_ERROR_RAW_DISK_MEDIA_IDENTIFIER_NOT_AVAILABLE
   A CRawMediaAccess instance for a given Media Identifier is not available
   in the Media Access List.  This error can occur if attempting to create
   or open a clip with a Raw Disk Media Identifier that is not in the
   rawdisk.cfg file.

CLIP_ERROR_RAW_DISK_OPEN_FAILED
   Could not open the Raw partion for media read/write.  Returned by
   CRawMediaAccess read and write functions and eventually by CField, CFrame
   and CFrameList read and write media functions.

CLIP_ERROR_RAW_DISK_READ_FAILED
   Could not read from the Raw partion.  Returned by CRawMediaAccess read
   and write functions and eventually by CField, CFrame and CFrameList
   read and write media functions.

CLIP_ERROR_RAW_DISK_WRITE_FAILED
   Could not write to the Raw partion.  Returned by CRawMediaAccess read
   and write functions and eventually by CField, CFrame and CFrameList
   read and write media functions.

CLIP_ERROR_RAW_OGLV_DISK_NOT_AVAILABLE
   The OGLV controlled Raw disk partition is not available on this system.
   Possibly OGLV has not been installed on the system.

CLIP_ERROR_RAW_OGLV_DISK_OPEN_FAILED
   Could not open OGLV's Raw partion for media read/write.  Returned by
   CRawOGLVMediaAccess read and write functions and eventually by CField,
   CFrame and CFrameList read and write media functions.

CLIP_ERROR_RAW_OGLV_DISK_READ_FAILED
   Could not read from OGLV's Raw partion.  Returned by CRawOGLVMediaAccess 
   read and write functions and eventually by CField, CFrame and CFrameList
   read and write media functions.

CLIP_ERROR_RAW_OGLV_DISK_WRITE_FAILED
   Could not write to OGLV's Raw partion.  Returned by CRawOGLVMediaAccess 
   read and write functions and eventually by CField, CFrame and CFrameList
   read and write media functions.              

CLIP_ERROR_REMOVE_FILE_FAILED
   Could not delete a file.  

CLIP_ERROR_REMOVE_DIRECTORY_FAILED
   Could not delete a clip or bin directory.  Under UNIX, the delete
   may have failed because the directory was not empty or because
   of insufficient permisssions.

CLIP_ERROR_RENDER_DESTINATION_CLIP_ALREADY_EXISTS
   The render destination clip already exists, so it cannot be created

CLIP_ERROR_RENDER_DESTINATION_CLIP_DOES_NOT_EXIST
   The render destination clip does not exist, even though it should not
   
CLIP_ERROR_RESTORE_CLIP_FAILED
   Could not restore critical clip files from backup

CLIP_ERROR_RFL_ENTRIES_OVERLAPPING
   Raw disk free list entries overlap those from freeing media.  Could
   happen if the RFL file is corrupted, the deallocation code
   has a problem or, very unlikely, the clip is corrupted.

CLIP_ERROR_RFL_ENTRIES_OUT_OF_ORDER
   Raw disk free list entries are not ordered.  Could
   happen if the RFL file is corrupted, the deallocation code
   has a problem or, very unlikely, the clip is corrupted.

CLIP_ERROR_TIME_TRACK_FILE_CREATE_FAILED
   Unable to create an Time Track (Frame List) file with the CFileBuilder

CLIP_ERROR_TIME_TRACK_FILE_CLOSE_FAILED
   Unable to close an Time Track (Frame List) file

CLIP_ERROR_TIME_TRACK_FILE_INVALID_FILE_TYPE
   An invalid or unexpected track file type was encountered when
   reading an Time Track (Frame List) file.

CLIP_ERROR_TIME_TRACK_FILE_INVALID_RECORD
   An invalid or unexpected record type was encountered while reading
   an Time Track (Frame List) file

CLIP_ERROR_TIME_TRACK_FILE_OPEN_FAILED
   Unable to open an Time Track (Frame List) file

CLIP_ERROR_TIME_TRACK_FILE_READ_ERROR
   Error reading a record from an Time Track (Frame List) file

CLIP_ERROR_TIME_TRACK_FILE_WRITE_ERROR
   Error writing a record to an Time Track (Frame List) file

CLIP_ERROR_TRACK_FILE_DOES_NOT_EXIST
   Clip 5 track file (in xml format) does not exist

CLIP_ERROR_UNBALANCED_CLIP_OPEN_CLOSED
   The clip-open reference count is less than zero, which means that
   CBinManager::closeClip was closed more times than CBinManager for
   a particular clip.  This is a coding error rather than a user error.

CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR
   An unexpected error occured for which there is not a more meaningful
   error code. This type of error should not be present in production code.  
   An error of this type is typically found in test or prototype code and
   should be replaced by a more meaningful code.

CLIP_ERROR_UNEXPECTED_NULL_ARGUMENT
   An unexpected NULL pointer passed as a function argument.
   This type of error should not be present in production code.  An error
   of this type is typically found in test or prototype code and
   should be replaced by a more meaningful code.

CLIP_ERROR_UNEXPECTED_NULL_RETURN
   An unexpected NULL pointer has been returned from a function.
   This type of error should not be present in production code.  An error
   of this type is typically found in test or prototype code and
   should be replaced by a more meaningful code.

CLIP_ERROR_VIDEO_TRACK_FILE_CREATE_FAILED
   Unable to create an Video Track (Frame List) file with the CFileBuilder

CLIP_ERROR_VIDEO_TRACK_FILE_CLOSE_FAILED
   Unable to close an Video Track (Frame List) file

CLIP_ERROR_VIDEO_TRACK_FILE_INVALID_FILE_TYPE
   An invalid or unexpected track file type was encountered when
   reading an Video Track (Frame List) file.

CLIP_ERROR_VIDEO_TRACK_FILE_INVALID_RECORD
   An invalid or unexpected record type was encountered while reading
   an Video Track (Frame List) file

CLIP_ERROR_VIDEO_TRACK_FILE_READ_ERROR
   Error reading a record from an Video Track (Frame List) file

CLIP_ERROR_VIDEO_TRACK_FILE_WRITE_ERROR
   Error writing a record to an Video Track (Frame List) file

CLIP_ERROR_VIRTUAL_MEDIA_CANNOT_OWN_MEDIA_STORAGE
   Attempt to assign media storage to a virtual media track.  A
   virtual media track cannot "own" media storage.

CLIP_ERROR_CANNOT_FIND_ACTIVE_CLIP_SCHEME_FILE
   The file containing the list of active clip schemes isn't where it's
   supposed to be according to the MTIcpmp.ini file

CLIP_ERROR_MEDIA_CANNOT_BE_HACKED_AS_REQUESTED
   A request to hack the media of the clip (e.g. to show or hide alpha
   channel masks in DPX files) did not succeed
   
CLIP_ERROR_RENDER_INFO_NOT_AVAILABLE
   Looked for [RenderInformation] section in the .clp file, but didn't
   find any

CLIP_ERROR_MEDIA_CANNOT_BE_DESTROYED
   Request to destroy the media at a particular Media Location was
   denied because the MediaAccess type does not support media destruction

CLIP_ERROR_MEDIA_CANNOT_BE_TRANSFERRED
   Request to transfer media from one media location to another was
   denied because the media types were incompatible or the operation
   is not supported by the media access type

CLIP_ERROR_MEDIA_IS_WRITE_PROTECTED
   We wanted to write to a write-protected fieed, but there is no
   alternate "dirty" storage defined

CLIP_ERROR_CLIP_CLONE_SOURCE_DOES_NOT_EXIST
   Can't find the clone source

CLIP_ERROR_CLIP_CLONE_TARGET_ALREADY_EXISTS
   There's already a directory with the name we were going to use as the
   cloning target

CLIP_ERROR_CLIP_CLONE_FILE_COPY_FAILED
   We were unable to copy all the files from the original clip to the
   clone

CLIP_ERROR_ASYNCH_IO_NOT_SUPPORTED_FOR_MEDIA
   Most media cannot be read asynchronously simply because we haven't
   bothered to implement it for those media

*/
//////////////////////////////////////////////////////////////////////

#endif // #ifndef CLIP_ERRORS_H
