// RawAudioMediaAllocator.h: interface for the CRawAudioMediaAllocator class.
//
/*
$Header: /usr/local/filmroot/clip/include/RawAudioMediaAllocator.h,v 1.3 2006/03/15 00:29:33 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef RAWAUDIOMEDIAALLOCATOR_H
#define RAWAUDIOMEDIAALLOCATOR_H

#include "MediaAllocator.h"

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CRawAudioMediaAccess;

//////////////////////////////////////////////////////////////////////

class CRawAudioMediaAllocator : public CMediaAllocator  
{
public:
   CRawAudioMediaAllocator(CRawAudioMediaAccess *mediaAccess,
                           double newSamplesPerField,
                           MTI_UINT32 newFirstFieldOffset, 
                           MTI_UINT32 newFieldCount);
   CRawAudioMediaAllocator(CRawAudioMediaAccess *mediaAccess,
                           double newSamplesPerField,
                           MTI_UINT32 newFirstFieldOffset, 
                           MTI_INT64 newAllocSampleOffset,
                           MTI_UINT32 newFieldCount);
   virtual ~CRawAudioMediaAllocator();

   int AssignMediaStorage(C5MediaTrack &mediaTrack);
   void getFieldMediaAllocation(MTI_UINT32 fieldNumber,
                                CMediaLocation& mediaLocation);

   void getNextFieldMediaAllocation(CMediaLocation& mediaLocation);

private:
   MTI_INT64 firstFieldOffset;
   double samplesPerField;       // Handles non-integral number of audio 
                                 // samples per video field
   MTI_INT64 firstSampleOffset;

   MTI_INT64 allocSampleOffset;  // offset to first field to be allocated

   CRawAudioMediaAllocator();     // Default constructor is deliberately
                                 // inaccessible
};

//////////////////////////////////////////////////////////////////////

#endif // #ifndef RAWAUDIOMEDIAALLOCATOR_H
