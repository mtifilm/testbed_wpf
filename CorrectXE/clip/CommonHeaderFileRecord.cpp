// CommonHeaderFileRecord.cpp: implementation of the CCommonHeaderFileRecord class.
//
/*
$Header: /usr/local/filmroot/clip/source/CommonHeaderFileRecord.cpp,v 1.2 2003/01/29 06:07:02 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "CommonHeaderFileRecord.h"
#include "err_clip.h"
#include "StructuredStream.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCommonHeaderFileRecord::CCommonHeaderFileRecord(MTI_UINT16 version)
: CFileRecord(FR_RECORD_TYPE_COMMON_FILE_HEADER, version),
  fileType(0), fileTypeVersion(FR_INITIAL_FILE_TYPE_VERSION)

{

}

CCommonHeaderFileRecord::~CCommonHeaderFileRecord()
{

}

//////////////////////////////////////////////////////////////////////
// Read and Write Functions
//////////////////////////////////////////////////////////////////////

int CCommonHeaderFileRecord::read(CStructuredStream &stream)
{
   int retVal;

   // Read fileType field
   if((retVal = stream.readUInt16(&fileType)) != 0)
      return retVal;

   // Read fileTypeVersion field
   if ((retVal = stream.readUInt16(&fileTypeVersion)) != 0)
      return retVal;

   return 0;
}

int CCommonHeaderFileRecord::write(CStructuredStream &stream)
{
   int retVal;

   // Write File record portion of this instance
   if((retVal = CFileRecord::write(stream)) != 0)
      return retVal;

   // Write fileType field
   if((retVal = stream.writeUInt16(fileType)) != 0)
      return retVal;

   // Write fileTypeVersion field
   if ((retVal = stream.writeUInt16(fileTypeVersion)) != 0)
      return retVal;

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Accessor Function
//////////////////////////////////////////////////////////////////////

MTI_UINT16 CCommonHeaderFileRecord::getFileType() const
{
   return fileType;
}

MTI_UINT16 CCommonHeaderFileRecord::getFileTypeVersion() const
{
   return fileTypeVersion;
}

//////////////////////////////////////////////////////////////////////
// Mutator Function
//////////////////////////////////////////////////////////////////////

void CCommonHeaderFileRecord::setFileType(MTI_UINT16 newFileType) 
{
   fileType = newFileType;
}

void CCommonHeaderFileRecord::setFileTypeVersion(MTI_UINT16 newFileTypeVersion) 
{
   fileTypeVersion = newFileTypeVersion;
}

void CCommonHeaderFileRecord::calculateRecordByteCount()
{
   // Total the size of all the fields in the record

   CFileRecord::calculateRecordByteCount();

   MTI_UINT32 newByteCount = getRecordByteCount();

   newByteCount += sizeof(fileType); 
   newByteCount += sizeof(fileTypeVersion); 

   // Update the object with the calculated size
   setRecordByteCount(newByteCount);
}

