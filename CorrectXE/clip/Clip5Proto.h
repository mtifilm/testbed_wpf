// File: Clip5Proto.h
//
// Header file for Clip 5 Prototype Code
//
//
//----------------------------------------------------------------------------

#ifndef CLIP5PROTOH
#define CLIP5PROTOH

#include "machine.h"
#include "cliplibint.h"
#include "guid_mti.h"
#include "IniFile.h"
#include "MediaLocation.h"
#include "MTIKeykode.h"
#include "SMPTETimecode.h"
#include "timecode.h"
#include "XMLWriter.h"

#include <vector>
#include <list>
#if defined(__GNUG__) && (__GNUG__ < 3)
// g++ versions < 3 don't have the ostream header
#include <iostream>
#else
#include <ostream>
#endif

using std::vector;
using std::list;
using std::ostream;

//----------------------------------------------------------------------------

#define CLIP5_MAX_FIELD_COUNT  5   // Maximum number of fields in a frame.
                                  // TBD: This should be increased if dealing
                                  //      with repeat frames in animation

//----------------------------------------------------------------------------

typedef _guid_t Clip5TrackID;

//----------------------------------------------------------------------------

class CMediaAccess;
class CMediaDeallocatorList;
class CTimeTrack;
class CVideoProxy;

class C5MediaTrack;
class C5MediaStatusTrack;
class C5RangeHandle;
class C5SequenceTrack;
class C5SMPTETimecodeTrack;
class C5Track;
class C5TrackRegistry;
class C5XMLAttribList;
struct S5RangeCover;

//----------------------------------------------------------------------------

enum E5TrackType
{
   CLIP5_TRACK_TYPE_INVALID = 0,
   CLIP5_TRACK_TYPE_SEQUENCE,
   CLIP5_TRACK_TYPE_MEDIA,
   CLIP5_TRACK_TYPE_MEDIA_STATUS,
   CLIP5_TRACK_TYPE_AUDIO_PROXY,
   CLIP5_TRACK_TYPE_KEYCODE,
   CLIP5_TRACK_TYPE_SMPTE_TIMECODE,
   CLIP5_TRACK_TYPE_TIMECODE,
};

enum E5RangeType
{
   CLIP5_RANGE_TYPE_INVALID = 0,
   CLIP5_RANGE_TYPE_FILLER,
   CLIP5_RANGE_TYPE_MEDIA_LOCATION_VIDEO_STORE,
   CLIP5_RANGE_TYPE_MEDIA_LOCATION_IMAGE_FILE,
   CLIP5_RANGE_TYPE_MEDIA_LOCATION_AUDIO_FILE,
   CLIP5_RANGE_TYPE_PULLDOWN,
   CLIP5_RANGE_TYPE_KEYCODE,
   CLIP5_RANGE_TYPE_TIMECODE,
   CLIP5_RANGE_TYPE_SMPTE_TIMECODE,
   CLIP5_RANGE_TYPE_MEDIA_STATUS,
//   CLIP5_RANGE_TYPE_MEDIA,
};

//----------------------------------------------------------------------------

enum E5ContentType
{
   CLIP5_CONTENT_TYPE_INVALID = 0,
   CLIP5_CONTENT_TYPE_NONE,
   CLIP5_CONTENT_TYPE_VIDEO,
   CLIP5_CONTENT_TYPE_AUDIO,
   CLIP5_CONTENT_TYPE_SMPTE_TIMECODE,
   CLIP5_CONTENT_TYPE_TIMECODE,
   CLIP5_CONTENT_TYPE_KEYCODE,
};

//----------------------------------------------------------------------------

enum EPulldownType
{
   PULLDOWN_TYPE_INVALID = 0,
   PULLDOWN_TYPE_3_2_FIELD,      // typically NTSC
   PULLDOWN_TYPE_3_2_FRAME,      // typically 720P/60
   PULLDOWN_TYPE_2_1,            // Interlaced Fields -> Frame
   PULLDOWN_TYPE_1_1,            // Progressive Frame or single field

   // TBD: Field 1 only or Field 2 only
   // TBD: Animation?
};

//----------------------------------------------------------------------------

enum EMediaLocationType
{
   CLIP_MEDIA_LOCATION_TYPE_INVALID = 0,
   CLIP_MEDIA_LOCATION_TYPE_VIDEO_STORE,
   CLIP_MEDIA_LOCATION_TYPE_IMAGE_FILE,
   CLIP_MEDIA_LOCATION_TYPE_AUDIO_FILE
};

//----------------------------------------------------------------------------

// Media Status
// Note: the items in this enum must be 'ordered' from worst to best
//       so that it is easy to combine the status of several fields.
//       The worst status dominates a better status.
enum E5MediaStatus
{
   CLIP5_MEDIA_STATUS_INVALID = 0,
   CLIP5_MEDIA_STATUS_NO_MEDIA,     // The track does not posess media
   CLIP5_MEDIA_STATUS_UNALLOCATED,  // Media storage never allocated
   CLIP5_MEDIA_STATUS_DEALLOCATED,  // Media storage used to exist, now
                                    // does not exist
   CLIP5_MEDIA_STATUS_ALLOCATED,    // Media storage exists, never written
   CLIP5_MEDIA_STATUS_WRITTEN,      // Written at least once
};

//----------------------------------------------------------------------------

enum E5ParsingLevel
{
   CLIP5_PARSING_LEVEL_TOP,
   CLIP5_PARSING_LEVEL_RANGE_LIST
};

//----------------------------------------------------------------------------
// Template Class for constructing handy maps between enum values and
// strings found in files

template <class TypeName> class CStringValueList
{
private:
   template <class TypeName2> class CInternalPair
   {
   public:
      CInternalPair(TypeName2 newValue, const char *newStr)
         : value(newValue), str(newStr) {};
      TypeName2 value;
      string str;
   };

public:

   template <class TypeName2> struct SInitPair
   {
      TypeName2 value;
      const char *str;
   };

   CStringValueList(const SInitPair<TypeName> p[])
      {
      const SInitPair<TypeName> *pp = p;
	  const char *sp= pp->str;
	  while((sp != nullptr) && (strlen(sp) != 0))
         {
         CInternalPair<TypeName> newPair(pp->value, sp);
         list.push_back(newPair);
         sp = (++pp)->str;
         }
      }

   TypeName FindByStr(const string &targetStr, TypeName defaultValue) const
      {
      for (typename vector<CInternalPair<TypeName> >::const_iterator iter = list.begin();
           iter != list.end(); ++iter)
         {
         if (iter->str == targetStr)
            return iter->value;
         }

      return defaultValue;
      };

   string FindByValue(TypeName targetValue) const
      {
      for (typename vector<CInternalPair<TypeName> >::const_iterator iter = list.begin();
           iter != list.end(); ++iter)
         {
         if (iter->value == targetValue)
            return iter->str;
         }

      string emptyStr;
      return emptyStr;
      };

private:
   vector<CInternalPair<TypeName> > list;
};

//----------------------------------------------------------------------------

struct S5FieldListEntry
{
   C5MediaTrack *mediaTrack;
   int index;
};

struct S5MediaTrackListEntry
{
   C5MediaTrack *mediaTrack;
   int index;
   int count;
};

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// Abstract Base classes for content-specific API
//

class C5ContentAPI
{
};

class C5MediaContentAPI : virtual public C5ContentAPI
{
public:
   virtual int GetFirstMediaAccess(CMediaAccess* &mediaAccess) const = 0;
   virtual int GetFirstMediaIdentifier(string &mediaIdentifier) const = 0;
   virtual int GetFirstMediaType(EMediaType &mediaType) const = 0;
   virtual MTI_UINT32 getMaxPaddedFieldByteCount(int index, int itemCount) = 0;
   virtual const CMediaFormat *GetMediaFormat() const = 0;
   virtual int GetMediaLocationsAllFields(int frameIndex,
                                vector<CMediaLocation> &mediaLocationList) = 0;
   virtual E5MediaStatus GetMediaStatus(int index) = 0;

   virtual int ReadField(int index, MTI_UINT8 *buffer) = 0;
   virtual int ReadMedia(int index, MTI_UINT8 *buffer[]) = 0;
   virtual int ReadMediaMT(int index, MTI_UINT8 *buffer[],
                           int fileHandleIndex) = 0;
   virtual int ReadMediaOneField(int frameIndex, int fieldIndex,
                                 MTI_UINT8 *buffer) = 0;
   virtual int ReadMediaOneFieldMT(int frameIndex, int fieldIndex,
                                 MTI_UINT8 *buffer, int fileHandleIndex) = 0;
   virtual int WriteField(int index, MTI_UINT8 *buffer) = 0;
   virtual int WriteMedia(int index, MTI_UINT8 *buffer[]) = 0;

   virtual int PeekAtMediaAllocation(vector<SMediaAllocation> &mediaAllocList,
                                     volatile int* currentFrame,
                                     volatile bool* stopRequest) = 0;
   virtual int PeekAtMediaLocation(int frameIndex, int fieldIndex,
                                   string &filename, MTI_INT64 &offset) = 0;

   virtual int LSF2(int startIndex, int itemCount, int &allocCount) = 0;

   virtual int FreeMedia(const string &clipPath, int startIndex,
                         int itemCount) = 0;

   virtual int ClearMediaWritten(int startIndex, int itemCount) = 0;
   virtual int UpdateMediaStatusTrackFile(const string &clipPath) = 0;
   virtual int ReloadMediaStatusTrackFile(const string &clipPath) = 0;
};

// --------------------------------------------------------------------------

class C5AudioContentAPI : virtual public C5ContentAPI
{
public:
   virtual const CAudioFormat *GetAudioFormat() const = 0;
   
   virtual int GetSampleCountAtField(int frameIndex, int fieldIndex) = 0;
   virtual int GetSampleCountAtFrame(int frameIndex) = 0;

   virtual int ReadMediaBySample(int index, int sampleOffset,
                                 MTI_INT64 sampleCount,
                                 MTI_UINT8 *buffer) = 0;
   virtual int ReadMediaBySampleMT(int index, int sampleOffset,
                                   MTI_INT64 sampleCount,
                                   MTI_UINT8 *buffer,
                                   int fileHandleIndex) = 0;
   virtual int WriteMediaBySample(int index, int sampleOffset,
                                  MTI_INT64 sampleCount,
                                  MTI_UINT8 *buffer,
                                  CTimeTrack *ttpAudioProxy) = 0;
};

// --------------------------------------------------------------------------
//class C5VideoContentAPI

class C5VideoContentAPI : virtual public C5ContentAPI
{
public:
   virtual const CImageFormat *GetImageFormat() const = 0;
};

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

class C5SMPTETimecodeContentAPI : public C5ContentAPI
{
public:

   virtual int GetTimecode(int index, CSMPTETimecode &timecode,
                           float &partialFrame) = 0;
   virtual int GetTimecodeFrameRate() const = 0;
   virtual EFrameRate GetTimecodeFrameRateEnum() const = 0;
   virtual EFrameRate GetTimecodeFrameRateEnum(int index) = 0;

   virtual int SetTimecode(int index, const CSMPTETimecode &newTimecode,
                           float partialFrame) = 0;
   virtual int SetTimecodeX(int index, int count,
                            const CSMPTETimecode &newTimecode,
                            float partialFrame) = 0;
   virtual int SetTimecodeFrameRate(EFrameRate newFrameRate) = 0;
   virtual int SetToDummyValue(int startIndex, int itemCount) = 0;
};

// --------------------------------------------------------------------------

#ifdef C5_TIMECODE
class C5TimecodeContentAPI : public C5ContentAPI
{
public:

   virtual int GetTimecode(int index, CTimecode &timecode) = 0;
   virtual int GetTimecodeFrameRate() const = 0;
   virtual EFrameRate GetTimecodeFrameRateEnum() const = 0;
   virtual EFrameRate GetTimecodeFrameRateEnum(int index) = 0;

   virtual int SetTimecode(int index, const CTimecode &newTimecode) = 0;
   virtual int SetTimecodeX(int index, int count,const CTimecode &newTimecode) = 0;
   virtual int SetTimecodeFrameRate(EFrameRate newFrameRate) = 0;
   virtual int SetToDummyValue(int startIndex, int itemCount) = 0;
};
#endif

// --------------------------------------------------------------------------

class C5KeycodeContentAPI : public C5ContentAPI
{
public:

   virtual int GetKeycode(int index, CMTIKeykode &keycode) = 0;

   virtual int SetKeycode(int index, const CMTIKeykode &newKeycode) = 0;
   virtual int SetKeycodeX(int index, int count, const CMTIKeykode &newKeycode) = 0;
   virtual int SetToDummyValue(int startIndex, int itemCount) = 0;
};

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

class C5TrackRegistryEntry
{
public:
   C5TrackRegistryEntry(const Clip5TrackID &newTrackID, C5Track *newTrack);
   virtual ~C5TrackRegistryEntry();

   C5Track* GetTrack() const;
   Clip5TrackID GetTrackID() const;
   string GetTrackIDStr() const;

   bool CompareTrackID(const Clip5TrackID &otherTrackID) const;

   void SetTrack(C5Track *newTrack);  // this is public, but should really
                                      // only be used by C5TrackRegistry

   void IncrRefCount();
   int DecrRefCount();

   int ReadTrackFile(const string &clipPath);
   int ReloadTrackFile(const string &clipPath);

private:
   Clip5TrackID trackID;
   C5Track *track;
   int referenceCount;
   CThreadLock refCountThreadLock;
};

// ----------------------------------------------------------------------------

typedef list<C5TrackRegistryEntry*> TrackRegistryEntryList;
typedef list<C5TrackRegistryEntry*>::iterator TrackRegistryEntryListIterator;

// ----------------------------------------------------------------------------

class MTI_CLIPLIB_API C5TrackHandle
{
friend class C5TrackRegistry;

public:
   C5TrackHandle();
   C5TrackHandle(const C5TrackHandle &rhs);
   C5TrackHandle(const Clip5TrackID &trackID);
   C5TrackHandle(C5Track *track);
   virtual ~C5TrackHandle();

   C5TrackHandle& operator=(const C5TrackHandle &rhs);
   bool operator==(const C5TrackHandle &rhs);
   bool operator!=(const C5TrackHandle &rhs);

   bool IsInitialized() const;

   int GetAllFieldList(int index, S5FieldListEntry fieldList[], int &count);
   E5ContentType GetContentType() const;
   double GetEditRate() const;
   EFrameRate GetEditRateEnum() const;
   int GetItemCount() const;
   bool GetNeedsUpdate() const;
   int GetTotalFrameCount() const;

   C5Track *GetTrack() const;
   Clip5TrackID GetTrackID() const;
   string GetTrackIDStr() const;
   E5TrackType GetTrackType() const;

   void SetTrackID(const Clip5TrackID &trackID);
   void SetTrackID(const string &trackIDStr);
   void SetTrack(C5Track *track);

   int ReadTrackFile(const string &clipPath);
   int ReloadTrackFile(const string &clipPath);
   int WriteTrackFile(const string &clipPath);

   // Factory method to create track from XML
   int CreateTrack(C5XMLAttribList &attribList);
   C5MediaTrack* MediaTrackFactory(E5ContentType newContentType);
   C5SequenceTrack* SequenceTrackFactory(E5ContentType newContentType);

   virtual void Dump(MTIostringstream &ostrm);


private:
   C5TrackRegistry* GetTrackRegistry();
   void DeleteRegistryEntry();
   void IncrRefCount();
   C5Track* TrackFactory(E5TrackType newTrackType,
                         E5ContentType newContentType);

private:
   bool initialized;
   TrackRegistryEntryListIterator registryEntry;

   static C5TrackRegistry *trackRegistry;
};

// ---------------------------------------------------------------------------

class MTI_CLIPLIB_API C5MediaTrackHandle : public C5TrackHandle,
                                           public C5MediaContentAPI
{

public:
   C5MediaTrackHandle();
   C5MediaTrackHandle(const C5TrackHandle &rhs);
   C5MediaTrackHandle(const Clip5TrackID &trackID);
   C5MediaTrackHandle(C5Track *track);
   virtual ~C5MediaTrackHandle();

   // assignment and implicit cast operator
//   C5MediaTrackHandle& operator=(const C5TrackHandle &rhs);
//   bool operator==(const C5TrackHandle &rhs);
//   bool operator!=(const C5TrackHandle &rhs);

//   virtual C5MediaTrack *GetMediaTrack() const;
   virtual C5MediaContentAPI* GetMediaContentAPI() const;

   int GetFirstMediaAccess(CMediaAccess* &mediaAccess) const;
   int GetFirstMediaIdentifier(string &mediaIdentifier) const;
   int GetFirstMediaType(EMediaType &mediaType) const;
   const CMediaFormat *GetMediaFormat() const;
   int GetMediaLocationsAllFields(int frameIndex,
                                  vector<CMediaLocation> &mediaLocationList);
   E5MediaStatus GetMediaStatus(int index);

   int PeekAtMediaAllocation(vector<SMediaAllocation> &mediaAllocList,
                             volatile int* currentFrame,
                             volatile bool* stopRequest);
   int PeekAtMediaLocation(int frameIndex, int fieldIndex,
                           string &filename, MTI_INT64 &offset);

   MTI_UINT32 getMaxPaddedFieldByteCount(int index, int count);

   int LSF1(int &count);
   int LSF2(int startIndex, int itemCount, int &allocCount);

   virtual int ReadField(int index, MTI_UINT8 *buffer);
   virtual int ReadMedia(int index, MTI_UINT8 *buffer[]);
   virtual int ReadMediaMT(int index, MTI_UINT8 *buffer[],
                           int fileHandleIndex);
   virtual int ReadMediaOneField(int frameIndex, int fieldIndex,
                                 MTI_UINT8 *buffer);
   virtual int ReadMediaOneFieldMT(int frameIndex, int fieldIndex,
                                   MTI_UINT8 *buffer, int fileHandleIndex);
   virtual int WriteField(int index, MTI_UINT8 *buffer);
   virtual int WriteMedia(int index, MTI_UINT8 *buffer[]);

   int ClearMediaWritten(int startIndex, int itemCount);
   int UpdateMediaStatusTrackFile(const string &clipPath);
   int ReloadMediaStatusTrackFile(const string &clipPath);

   int CreateSequenceTrack(const string &clipPath,
                           EMediaType mediaType,
                           const string& mediaIdentifier,
                           const CMediaFormat& mediaFormat,
                           EFrameRate newEditRate,
                           int totalItemCount,
                           int allocItemCount);

   // Factory method to create a media track and allocate media storage
   int CreateMediaTrack(const string &clipPath,
                        EMediaType mediaType, const string& mediaIdentifier,
                        const CMediaFormat& mediaFormat,
                        EFrameRate newEditRate,
                        int totalItemCount, int allocItemCount);
   int ExtendMediaTrack(const string &clipPath, int totalItemCount,
                        int allocItemCount);

   int FreeMedia(const string &clipPath, int startIndex, int itemCount);

protected:

   int AllocateMediaStorage(EMediaType mediaType, const string& mediaIdentifier,
                            const CMediaFormat& mediaFormat,
                            MTI_UINT32 itemCount);

private:

}; // class C5MediaTrackHandle

// ---------------------------------------------------------------------------

class MTI_CLIPLIB_API C5AudioTrackHandle : public C5MediaTrackHandle
{

public:
   C5AudioTrackHandle();
   C5AudioTrackHandle(const C5TrackHandle &rhs);
   C5AudioTrackHandle(const Clip5TrackID &trackID);
   C5AudioTrackHandle(C5Track *track);
   virtual ~C5AudioTrackHandle();

   const CAudioFormat *GetAudioFormat() const;
   C5AudioContentAPI *GetAudioContentAPI() const;
   virtual int GetSampleCountAtField(int frameIndex, int fieldIndex);
   virtual int GetSampleCountAtFrame(int frameIndex);

   int ReadMediaBySample(int index, int sampleOffset, MTI_INT64 sampleCount,
                         MTI_UINT8 *buffer);
   int ReadMediaBySampleMT(int index, int sampleOffset, MTI_INT64 sampleCount,
                           MTI_UINT8 *buffer, int fileHandleIndex);
   int WriteMediaBySample(int index, int sampleOffset, MTI_INT64 sampleCount,
                          MTI_UINT8 *buffer, CTimeTrack *ttpAudioProxy);

protected:

private:

}; // class C5AudioTrackHandle

// ---------------------------------------------------------------------------

class MTI_CLIPLIB_API C5VideoTrackHandle : public C5MediaTrackHandle
{

public:
   C5VideoTrackHandle();
   C5VideoTrackHandle(const C5TrackHandle &rhs);
   C5VideoTrackHandle(const Clip5TrackID &trackID);
   C5VideoTrackHandle(C5Track *track);
   virtual ~C5VideoTrackHandle();

   const CImageFormat *GetImageFormat() const;


protected:

private:

}; // class C5VideoTrackHandle

// ---------------------------------------------------------------------------

class MTI_CLIPLIB_API C5SMPTETimecodeTrackHandle : public C5TrackHandle,
                                               public C5SMPTETimecodeContentAPI
{
public:
   C5SMPTETimecodeTrackHandle();
   C5SMPTETimecodeTrackHandle(const C5TrackHandle &rhs);
   C5SMPTETimecodeTrackHandle(const Clip5TrackID &trackID);
   C5SMPTETimecodeTrackHandle(C5Track *track);
   virtual ~C5SMPTETimecodeTrackHandle();

   int GetTimecode(int index, CSMPTETimecode &timecode, float &partialFrame);
   C5SMPTETimecodeContentAPI *GetSMPTETimecodeContentAPI() const;
   int GetTimecodeFrameRate() const;
   EFrameRate GetTimecodeFrameRateEnum() const;
   EFrameRate GetTimecodeFrameRateEnum(int index);

   int SetTimecode(int index, const CSMPTETimecode &newTimecode,
                   float partialFrame);
   int SetTimecodeX(int startIndex, int itemCount,
                   const CSMPTETimecode &newTimecode, float newPartialFrame);
   int SetTimecodeFrameRate(EFrameRate newFrameRate);
   virtual int SetToDummyValue(int startIndex, int itemCount);

   int CreateSequenceTrack(const string &clipPath,
                           EFrameRate trackFrameRate,
                           EFrameRate timecodeFrameRate,
                           int itemCount);
   int CreateSMPTETimecodeTrack(const string &clipPath,
                                EFrameRate trackFrameRate,
                                EFrameRate timecodeFrameRate,
                                int itemCount);

   int ExtendTimeTrack(const string &clipPath, int itemCount);

protected:

private:

}; // class C5SMPTETimecodeTrackHandle

// ---------------------------------------------------------------------------

#ifdef C5_TIMECODE
class MTI_CLIPLIB_API C5SMPTETimecodeTrackHandle : public C5TrackHandle,
                                               public C5SMPTETimecodeContentAPI
{
public:
   C5TimecodeTrackHandle();
   C5TimecodeTrackHandle(const C5TrackHandle &rhs);
   C5TimecodeTrackHandle(const Clip5TrackID &trackID);
   C5TimecodeTrackHandle(C5Track *track);
   virtual ~C5TimecodeTrackHandle();

   int GetTimecode(int index, CTimecode &timecode);
   C5TimecodeContentAPI *GetSMPTEimecodeContentAPI() const;
   int GetTimecodeFrameRate() const;
   EFrameRate GetTimecodeFrameRateEnum() const;
   EFrameRate GetTimecodeFrameRateEnum(int index);

   int SetTimecode(int index, const CTimecode &newTimecode);
   int SetTimecodeX(int startIndex, int itemCount,
                    const CTimecode &newTimecode);
   int SetTimecodeFrameRate(EFrameRate newFrameRate);
   virtual int SetToDummyValue(int startIndex, int itemCount);

   int CreateSequenceTrack(const string &clipPath,
                           EFrameRate trackFrameRate,
                           EFrameRate timecodeFrameRate,
                           int itemCount);
   int CreateTimecodeTrack(const string &clipPath,
                           EFrameRate trackFrameRate,
                           EFrameRate timecodeFrameRate,
                           int itemCount);

   int ExtendTimeTrack(const string &clipPath, int itemCount);

protected:

private:

}; // class C5TimecodeTrackHandle

#endif

// ---------------------------------------------------------------------------

class MTI_CLIPLIB_API C5KeycodeTrackHandle : public C5TrackHandle,
                                             public C5KeycodeContentAPI
{
public:
   C5KeycodeTrackHandle();
   C5KeycodeTrackHandle(const C5TrackHandle &rhs);
   C5KeycodeTrackHandle(const Clip5TrackID &trackID);
   C5KeycodeTrackHandle(C5Track *track);
   virtual ~C5KeycodeTrackHandle();

   int GetKeycode(int index, CMTIKeykode &Keycode);
   C5KeycodeContentAPI *GetKeycodeContentAPI() const;

   int SetKeycode(int index, const CMTIKeykode &newKeycode);
   int SetKeycodeX(int index, int count, const CMTIKeykode &newKeycode);
   int SetToDummyValue(int index, int itemCount);

   int CreateKeycodeTrack(const string &clipPath, EFrameRate newEditRate,
                          int itemCount);

   int ExtendKeycodeTrack(const string &clipPath, int itemCount);

}; // class C5KeycodeTrackHandle

// ---------------------------------------------------------------------------

class C5TrackRegistry
{
public:
   C5TrackRegistry();
   virtual ~C5TrackRegistry();

   TrackRegistryEntryListIterator GetIterator(const Clip5TrackID &trackID,
                                              bool insertIfNotFound);
   TrackRegistryEntryListIterator GetIterator(C5Track *track,
                                              bool insertIfNotFound);

   void DeleteEntry(TrackRegistryEntryListIterator entry);
   void IncrRefCount(TrackRegistryEntryListIterator entry);

   int ReadTrackFiles(const string &clipPath);

private:

private:
   TrackRegistryEntryList trackList;
};

//----------------------------------------------------------------------------

class MTI_CLIPLIB_API C5Range
{
public:
   C5Range();
   C5Range(const C5Range &src);
   virtual ~C5Range();

   C5Range& operator=(const C5Range& rhs);

//   virtual int GetAllFieldList(int index, S5FieldListEntry fieldList[],
//                               int &count);

   virtual E5ContentType GetContentType() const = 0;
   virtual int GetDstStartIndex() const;
   virtual int GetItemCount() const;
   virtual bool GetRangeIntersection(int startIndexArg, int countArg,
                                     int &overlapStart, int &overlapEnd);
   virtual E5RangeType GetRangeType() const = 0;
                                     
   virtual void SetItemCount(int newCount);
   virtual void SetDstStartIndex(int newIndex);

   static C5Range* CreateRange(C5XMLAttribList &attribList); // Factory method
   virtual int initFromXMLAttributes(C5XMLAttribList &attribList);

   virtual void writeXMLStream(CXMLStream &xmlStream);

   virtual void Dump(ostream &ostrm);

protected:
   int itemCount;
   int dstStartIndex;

private:
};

class MTI_CLIPLIB_API C5FillerRange : public C5Range
{
public:
   C5FillerRange();
   virtual ~C5FillerRange();

   virtual E5RangeType GetRangeType() const {return CLIP5_RANGE_TYPE_FILLER;};
   virtual E5ContentType GetContentType() const {return CLIP5_CONTENT_TYPE_NONE;};

   virtual void writeXMLStream(CXMLStream &xmlStream);

protected:

private:
};

class MTI_CLIPLIB_API C5Segment : public C5Range
{
public:
   C5Segment();
   virtual ~C5Segment();
   
   C5TrackHandle GetSrcTrack() const;

   void SetSrcStartIndex(int newSrcStartIndex);
   void SetSrcTrack(const C5TrackHandle &newSrcTrack);

   virtual int initFromXMLAttributes(C5XMLAttribList &attribList);
   virtual void writeXMLStream(CXMLStream &xmlStream);

protected:
   C5TrackHandle srcTrackHandle;
   int srcStartIndex;
   
private:
};

//----------------------------------------------------------------------------

// Is audio & video pulldown-ness different enough so there should'
// be C5AudioPulldown and C5VideoPulldown, or can the pulldownType
// handle the difference?
class MTI_CLIPLIB_API C5Pulldown : public C5Segment
{
public:
   C5Pulldown();
   virtual ~C5Pulldown();

   virtual E5RangeType GetRangeType() const {return CLIP5_RANGE_TYPE_PULLDOWN;};

   virtual int GetAllFieldList(int index, S5FieldListEntry fieldList[],
                               int &count);
   virtual int GetAllFieldList(int index, int *fieldList, int *fieldCount) const;
   virtual int GetHiddenFieldCount(int index) const;
   int GetSrcRange(int dstIndex, int dstCount, int &srcIndex, int &srcCount);
   virtual int GetTotalFieldCount(int index) const;
   virtual int GetVisibleFieldCount(int index) const;
   virtual int GetVisibleFieldList(int index, int *fieldList, int *fieldCount) const;

   virtual void SetPulldownPhase(int newPulldownPhase);
   virtual void SetPulldownType(EPulldownType newPulldownType);

   virtual int initFromXMLAttributes(C5XMLAttribList &attribList);
   virtual void writeXMLStream(CXMLStream &xmlStream);

protected:
   EPulldownType pulldownType;
   int pulldownPhase;

private:

}; // C5Pulldown

//----------------------------------------------------------------------------

class C5MediaPulldown : public C5Pulldown
{
public:

   virtual int GetFirstMediaAccess(CMediaAccess* &mediaAccess) const;
   virtual int GetFirstMediaIdentifier(string &mediaIdentifier) const;
   virtual int GetFirstMediaType(EMediaType &mediaType) const;
   virtual MTI_UINT32 getMaxPaddedFieldByteCount(int index, int itemCount);
   virtual const CMediaFormat *GetMediaFormat() const;
   virtual E5MediaStatus GetMediaStatus(int index);
   virtual C5MediaTrackHandle GetSrcMediaTrack() const;

   virtual int ReadMedia(int index, MTI_UINT8 *buffer[]);
   virtual int ReadMediaMT(int index, MTI_UINT8 *buffer[], int fileHandleIndex);
   virtual int ReadMediaOneField(int frameIndex, int fieldIndex,
                                 MTI_UINT8 *buffer);
   virtual int ReadMediaOneFieldMT(int frameIndex, int fieldIndex,
                                   MTI_UINT8 *buffer, int fileHandleIndex);
   virtual int WriteMedia(int index, MTI_UINT8 *buffer[]);
   
   virtual int LSF2(int startIndex, int itemCount, int &allocCount);

   virtual int FreeMedia(const string &clipPath, int startIndex, int itemCount);

   virtual int ClearMediaWritten(int startIndex, int itemCount);

}; // C5MediaPulldown

//----------------------------------------------------------------------------

class C5AudioPulldown : public C5MediaPulldown
{
public:
   virtual E5ContentType GetContentType() const {return CLIP5_CONTENT_TYPE_AUDIO;};

   virtual const CAudioFormat* GetAudioFormat() const;

};

//----------------------------------------------------------------------------

class C5VideoPulldown : public C5MediaPulldown
{
public:
   virtual E5ContentType GetContentType() const {return CLIP5_CONTENT_TYPE_VIDEO;};

   virtual const CImageFormat* GetImageFormat() const;

};

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

class C5SMPTETimecodePulldown : public C5Pulldown
{
public:

   virtual E5ContentType GetContentType() const {return CLIP5_CONTENT_TYPE_SMPTE_TIMECODE;};

   virtual int GetTimecode(int index, CSMPTETimecode &timecode,
                           float &partialFrame);
   virtual int GetTimecodeFrameRate() const;
   virtual EFrameRate GetTimecodeFrameRateEnum() const;

   virtual int SetTimecode(int index, const CSMPTETimecode &newTimecode,
                           float partialFrame);
   virtual int SetTimecodeX(int index, int count,
                            const CSMPTETimecode &newTimecode,
                            float partialFrame);
   virtual int SetToDummyValue(int startIndex, int itemCount);

   C5SMPTETimecodeTrackHandle GetSrcSMPTETimecodeTrack() const;
};

//----------------------------------------------------------------------------

class MTI_CLIPLIB_API C5Track
{
public:
   C5Track();
   virtual ~C5Track();

   virtual E5TrackType GetTrackType() const = 0;

   bool IsIndexValid(int index);

   virtual int GetAllFieldList(int index, S5FieldListEntry fieldList[],
                               int &count);
   virtual E5ContentType GetContentType() const = 0;
   virtual double GetEditRate() const;
   virtual EFrameRate GetEditRateEnum() const;

   C5RangeHandle GetEndRangeHandle();
   int GetItemCount();
   virtual bool GetNeedsUpdate() const;
   virtual int GetTotalFrameCount();
   Clip5TrackID GetTrackID() const;
   string GetTrackIDStr() const;

   virtual void SetEditRate(EFrameRate newFrameRate);
   virtual void SetNeedsUpdate(bool newUpdateFlag);

   int AppendRange(C5Range *newRange);
   void RemoveRange(C5RangeHandle &rangeHandle);
   void InsertBefore(C5RangeHandle &position, C5Range *newRange);
   void InsertAfter(C5RangeHandle &position, C5Range *newRange);

   C5RangeHandle FindRangeHandle(int itemIndex);
   bool IsEnd(C5RangeHandle &rangeHandle);
   
   bool compareTrackID(const Clip5TrackID &trialID);

   virtual int initFromXMLAttributes(C5XMLAttribList &attribList);

   virtual int xmlParseStartElement(const string &elementName,
                                    C5XMLAttribList &attribList);
   virtual int xmlParseCharacters(const string &chars);
   virtual int xmlParseEndElement(const string &elementName);

   virtual void writeXMLAttributes(CXMLStream &xmlStream);
   virtual void writeXMLStream(CXMLStream &xmlStream);

   virtual int WriteTrackFile(const string &clipPath);

   virtual void Dump(ostream &ostrm);

public:
   typedef list<C5Range*>  RangeList;
   typedef RangeList::iterator RangeListIterator; // non-const, bidirection iterator

protected:

   int GetRangeCount() const;
   C5Range* FindRange(int itemIndex);
   int FindCoveringSet(int itemIndex, int count, list<S5RangeCover> &coverSet);
   void CalculateItemCount();
   void ResetItemCount();

protected:
   E5ParsingLevel parsingLevel;  // temporary used while parsing XML file
   bool needsUpdate;

   RangeList rangeList;

private:
   Clip5TrackID trackID;      // Unique identifier for this track
   MTI_UINT64 serialNumber;   // Serial number for this track, planned
                              // use is to track changes and guide reload
                              // from files and/or database

   string clipPath;

   int cachedItemCount;
   EFrameRate editRate;       // Edit rate, i.e., frame or field rate
                              // enumeration type.
};

// ---------------------------------------------------------------------------

class MTI_CLIPLIB_API C5RangeHandle
{
friend class C5Track;

public:
   C5RangeHandle();
   C5RangeHandle(const C5RangeHandle &rhs);
   C5RangeHandle(C5Track::RangeListIterator &rhs);
   virtual ~C5RangeHandle();

   C5RangeHandle& operator=(const C5RangeHandle &rhs);

   C5RangeHandle& operator++();          // ++handle
   C5RangeHandle& operator--();          // --handle

   C5RangeHandle Next();
   C5RangeHandle Previous();

   bool operator==(C5RangeHandle &rhs);
   bool operator!=(C5RangeHandle &rhs);
//   bool operator==(C5Track::RangeListIterator &rhs);
   bool operator==(list<C5Range*>::iterator &rhs);
   bool operator!=(C5Track::RangeListIterator &rhs);
   bool operator==(C5Range *rhs);
   bool operator!=(C5Range *rhs);

   C5Range* GetRange() const;
   
protected:

private:
   // Postfix operators not implemented
   C5RangeHandle& operator++(int) {return *this;};   // handle++
   C5RangeHandle& operator--(int) {return *this;};   // handle--


private:
   C5Track::RangeList::iterator iter;
};

struct S5RangeCover
{
   int startIndex;
   int itemCount;
   C5RangeHandle rangeHandle;
};



//----------------------------------------------------------------------------

class MTI_CLIPLIB_API C5MediaLocationRange : public C5Range
{
public:
   C5MediaLocationRange();
   C5MediaLocationRange(const C5MediaLocationRange &src);
   virtual ~C5MediaLocationRange();

   C5MediaLocationRange& operator=(const C5MediaLocationRange& rhs);

   // Virtual "default constructor" returns new instance with same type
   // as *this.
   virtual C5MediaLocationRange* construct() const = 0;
   // Virtual "copy constructor"
   virtual C5MediaLocationRange* clone() const = 0;
   // Virtual "assignment operator"
   virtual C5MediaLocationRange& assign(const C5MediaLocationRange& src) = 0;

   virtual int GetMediaIdentifier(string &mediaIdentifier) const;
   virtual int GetMediaType(EMediaType &mediaType) const;
   virtual MTI_UINT32 getMaxPaddedFieldByteCount(int index, int itemCount);
   virtual CMediaLocation GetMediaLocation(int index) = 0;
   virtual E5MediaStatus GetMediaStatus(int index) const;
   virtual CMediaLocation GetStartMediaLocation() const;

   virtual void SetStartMediaLocation(const CMediaLocation& newMediaLocation);

   virtual int RegisterMediaAccess(CMediaFormat &mediaFormat);

   virtual int ReadField(int index, MTI_UINT8 *buffer);
   virtual int ReadFieldMT(int index, MTI_UINT8 *buffer, int fileHandleIndex);
   virtual int WriteField(int index, MTI_UINT8 *buffer);
//   virtual int ReadMedia(int fieldIndex, MTI_UINT8* buffer[]);
//   virtual int WriteMedia(int fieldIndex, MTI_UINT8* buffer[]);

   virtual int initFromXMLAttributes(C5XMLAttribList &attribList);
   virtual void writeXMLStream(CXMLStream &xmlStream);

   virtual void Dump(ostream &ostrm);

protected:
   EMediaType mediaType;
   string mediaIdentifier;
   CMediaLocation startMediaLocation;

private:
};

//----------------------------------------------------------------------------

//#ifdef NOT_YET
class MTI_CLIPLIB_API C5ImageFileRange : public C5MediaLocationRange
{
public:
   C5ImageFileRange();
   C5ImageFileRange(const C5ImageFileRange &src);   // copy constructor
   C5ImageFileRange(const CMediaLocation &newMediaLocation, int newItemCount,
                    int newFirstFileNumber, int newMinFrameDigitCount);
   virtual ~C5ImageFileRange();

   virtual E5ContentType GetContentType() const {return CLIP5_CONTENT_TYPE_VIDEO;};
   virtual E5RangeType GetRangeType() const {return CLIP5_RANGE_TYPE_MEDIA_LOCATION_IMAGE_FILE;};

   C5ImageFileRange& operator=(const C5ImageFileRange &rhs);

   // Virtual "default constructor" returns new instance with same type
   // as *this.
   C5MediaLocationRange* construct() const {return new C5ImageFileRange;};
   // Virtual "copy constructor"
   C5MediaLocationRange* clone() const {return new C5ImageFileRange(*this);};
   // Virtual "assignment operator"
   C5MediaLocationRange& assign(const C5MediaLocationRange& src);

   virtual CMediaLocation GetMediaLocation(int index);

   void Dump(ostream &ostrm);

   virtual void writeXMLStream(CXMLStream &xmlStream);

private:

   // Image File Type?

   // First file number
   int firstFileNumber;
   int minFrameDigitCount;

};
//#endif // #ifdef NOT_YET

//----------------------------------------------------------------------------

class MTI_CLIPLIB_API C5AudioStoreRange : public C5MediaLocationRange
{
public:
   C5AudioStoreRange();
   C5AudioStoreRange(const C5AudioStoreRange &src);   // copy constructor
   C5AudioStoreRange(const CMediaLocation &newMediaLocation, int newItemCount,
                     double newSamplesPerField);
   virtual ~C5AudioStoreRange();

   C5AudioStoreRange& operator=(const C5AudioStoreRange &rhs);

   // Virtual "default constructor" returns new instance with same type
   // as *this.
   C5MediaLocationRange* construct() const {return new C5AudioStoreRange;};
   // Virtual "copy constructor"
   C5MediaLocationRange* clone() const {return new C5AudioStoreRange(*this);};
   // Virtual "assignment operator"
   C5MediaLocationRange& assign(const C5MediaLocationRange& src);

   virtual E5ContentType GetContentType() const {return CLIP5_CONTENT_TYPE_AUDIO;};
   virtual E5RangeType GetRangeType() const {return CLIP5_RANGE_TYPE_MEDIA_LOCATION_AUDIO_FILE;};

   virtual CMediaLocation GetMediaLocation(int index);

   void Dump(ostream &ostrm);

   virtual int initFromXMLAttributes(C5XMLAttribList &attribList);
   virtual void writeXMLStream(CXMLStream &xmlStream);

private:
   double samplesPerField;

};  // C5AudioStoreRange

//----------------------------------------------------------------------------

class MTI_CLIPLIB_API C5VideoStoreRange : public C5MediaLocationRange
{
public:
   C5VideoStoreRange();
   C5VideoStoreRange(const C5VideoStoreRange &src);   // copy constructor
   C5VideoStoreRange(const CMediaLocation &newMediaLocation, int newItemCount);
   virtual ~C5VideoStoreRange();

   C5VideoStoreRange& operator=(const C5VideoStoreRange &rhs);

   // Virtual "default constructor" returns new instance with same type
   // as *this.
   C5MediaLocationRange* construct() const {return new C5VideoStoreRange;};
   // Virtual "copy constructor"
   C5MediaLocationRange* clone() const {return new C5VideoStoreRange(*this);};
   // Virtual "assignment operator"
   C5MediaLocationRange& assign(const C5MediaLocationRange& src);

   virtual E5ContentType GetContentType() const {return CLIP5_CONTENT_TYPE_VIDEO;};
   virtual E5RangeType GetRangeType() const {return CLIP5_RANGE_TYPE_MEDIA_LOCATION_VIDEO_STORE;};

   virtual CMediaLocation GetMediaLocation(int index);

   void Dump(ostream &ostrm);

   virtual void writeXMLStream(CXMLStream &xmlStream);

private:

}; // C5VideoStoreRange

//----------------------------------------------------------------------------

class MTI_CLIPLIB_API C5SMPTETimecodeRange : public C5Range
{
public:
   C5SMPTETimecodeRange();
   virtual ~C5SMPTETimecodeRange();

   C5SMPTETimecodeRange& operator=(const C5SMPTETimecodeRange& rhs);

   virtual E5ContentType GetContentType() const {return CLIP5_CONTENT_TYPE_SMPTE_TIMECODE;};
   virtual E5RangeType GetRangeType() const {return CLIP5_RANGE_TYPE_SMPTE_TIMECODE;};

   int GetTimecodeFrameRate() const;
   EFrameRate GetTimecodeFrameRateEnum() const;
   float GetPartialFrame(int index);
   int GetTimecode(int index, CTimecode &timecode);
   int GetTimecode(int index, CSMPTETimecode &newTimecode, float &partialFrame);

   void SetStartPartialFrame(float newPartialFrame);
   void SetStartTimecode(const CSMPTETimecode &newTimecode);
   void SetTimecodeFrameRate(EFrameRate newFrameRate);
   void SetTrackEditRate(EFrameRate newEditRate);
   void SetToDummyValue();

   virtual int initFromXMLAttributes(C5XMLAttribList &attribList);
   virtual void writeXMLStream(CXMLStream &xmlStream);

   virtual void Dump(ostream &ostrm);

protected:
   CSMPTETimecode m_startTimecode;
   float m_startPartialFrame;
   EFrameRate m_trackEditRate;
   EFrameRate m_timecodeFrameRate;

private:

}; // class C5SMPTETimecodeRange

//----------------------------------------------------------------------------

#ifdef C5_TIMECODE
class MTI_CLIPLIB_API C5TimecodeRange : public C5Range
{
public:
   C5TimecodeRange();
   virtual ~C5TimecodeRange();

   C5TimecodeRange& operator=(const C5TimecodeRange& rhs);

   virtual E5ContentType GetContentType() const {return CLIP5_CONTENT_TYPE_TIMECODE;};
   virtual E5RangeType GetRangeType() const {return CLIP5_RANGE_TYPE_TIMECODE;};

   int GetTimecode(int index, CTimecode &timecode);

   void SetStartTimecode(const CTimecode &newTimecode);
   void SetTrackEditRate(EFrameRate newEditRate);
   void SetToDummyValue();

   virtual int initFromXMLAttributes(C5XMLAttribList &attribList);
   virtual void writeXMLStream(CXMLStream &xmlStream);

   virtual void Dump(ostream &ostrm);

protected:
   CTimecode m_startTimecode;
   EFrameRate m_trackEditRate;

private:

}; // class C5TimecodeRange
#endif

//----------------------------------------------------------------------------

class MTI_CLIPLIB_API C5KeycodeRange : public C5Range
{
public:
   C5KeycodeRange();
   virtual ~C5KeycodeRange();

   C5KeycodeRange& operator=(const C5KeycodeRange& rhs);

   virtual E5ContentType GetContentType() const {return CLIP5_CONTENT_TYPE_KEYCODE;};
   virtual E5RangeType GetRangeType() const {return CLIP5_RANGE_TYPE_KEYCODE;};

   int GetFormattedStr(int index, EKeykodeFormat formatType,
                       string &keykodeStrReturn, bool overrideInterp=false);
   int GetKeykode(int index, CMTIKeykode &keykodeReturn);
   int GetKeykodeStr(int index, string &keykodeStrReturn);

   int SetKeykode(const CMTIKeykode &newKeykode);
   int SetRawKeykode(const string &newKKStr, int newInterpolationOffset);
   int SetRawKeykode(const char *newKKStr, int newInterpolationOffset);
   void SetToDummyValue();

   virtual int initFromXMLAttributes(C5XMLAttribList &attribList);
   virtual void writeXMLStream(CXMLStream &xmlStream);

   virtual void Dump(ostream &ostrm);

protected:
   CMTIKeykode startKeycode;

private:
};

//----------------------------------------------------------------------------

class MTI_CLIPLIB_API C5MediaTrack : public C5Track, public C5MediaContentAPI
{
public:
   C5MediaTrack();
   virtual ~C5MediaTrack();

   E5TrackType GetTrackType() const {return CLIP5_TRACK_TYPE_MEDIA;};

   string GetDefaultMediaIdentifier() const;
   EMediaType GetDefaultMediaType() const;
   virtual int GetFirstMediaAccess(CMediaAccess* &mediaAccess) const;
   virtual int GetFirstMediaIdentifier(string &mediaIdentifier) const;
   virtual int GetFirstMediaType(EMediaType &mediaType) const;
   virtual MTI_UINT32 getMaxPaddedFieldByteCount(int index, int itemCount);
   virtual const CMediaFormat* GetMediaFormat() const;
   virtual int GetMediaLocation(int index, CMediaLocation &mediaLocation);
   virtual int GetMediaLocationsAllFields(int frameIndex,
                                    vector<CMediaLocation> &mediaLocationList);
   virtual E5MediaStatus GetMediaStatus(int index);
   virtual int GetTotalFrameCount();

   virtual int PeekAtMediaAllocation(vector<SMediaAllocation> &mediaAllocList,
                                     volatile int* currentFrame,
                                     volatile bool* stopRequest);
   virtual int PeekAtMediaLocation(int frameIndex, int fieldIndex,
                                   string &filename, MTI_INT64 &offset);

   virtual int LSF2(int startIndex, int itemCount, int &allocCount);

   void CopyMediaFormat(const CMediaFormat &newMediaFormat);

   void SetDefaultMediaIdentifier(const string &newMediaIdentifier);
   void SetDefaultMediaType(EMediaType newMediaType);
   int SetMediaStatus(int index, E5MediaStatus newStatus);
   int SetMediaStatus(int startIndex, int itemCount, E5MediaStatus newStatus,
                      bool allowStatusDecrease, bool allowStatusIncrease);
   void SetMediaStatusTrack(const C5TrackHandle &newMediaStatusTrack);

   virtual int ReadField(int index, MTI_UINT8 *buffer);
   virtual int ReadFieldMT(int index, MTI_UINT8 *buffer, int fileHandleIndex);
   virtual int WriteField(int index, MTI_UINT8 *buffer);
   virtual int ReadMedia(int index, MTI_UINT8 *buffer[]);
   virtual int ReadMediaMT(int index, MTI_UINT8 *buffer[], int fileHandleIndex);
   virtual int ReadMediaOneField(int frameIndex, int fieldIndex,
                                 MTI_UINT8 *buffer);
   virtual int ReadMediaOneFieldMT(int frameIndex, int fieldIndex,
                                   MTI_UINT8 *buffer, int fileHandleIndex);
   virtual int WriteMedia(int index, MTI_UINT8 *buffer[]);

   int BlackenBuffer(int index, MTI_UINT8 *buffer[]);

   int GatherRawVideoStore(CVideoProxy *videoProxy);

   virtual int initFromXMLAttributes(C5XMLAttribList &attribList);

   virtual int xmlParseStartElement(const string &elementName,
                                    C5XMLAttribList &attribList);
   virtual int xmlParseCharacters(const string &chars);
   virtual int xmlParseEndElement(const string &elementName);
   
   virtual int ClearMediaWritten(int startIndex, int itemCount);
   virtual int UpdateMediaStatusTrackFile(const string &clipPath);
   virtual int ReloadMediaStatusTrackFile(const string &clipPath);

   int ExtendMediaTrack(const string &clipPath, int totalItemCount,
                        int allocItemCount);
   virtual int FreeMedia(const string &clipPath, int startIndex, int itemCount);
   virtual int ReallocateMediaStorage(int itemCount);

   virtual void writeXMLAttributes(CXMLStream &xmlStream);
   virtual void writeXMLStream(CXMLStream &xmlStream);

   void Dump(ostream &ostrm);

protected:
   CMediaFormat *mediaFormat;
   bool parsingMediaFormat;
   std::ostringstream mediaFormatAccumulator;
   C5TrackHandle mediaStatusTrackHandle;
   EMediaType defaultMediaType;
   string defaultMediaIdentifier;

private:
   C5MediaLocationRange *FindBlock(int fieldIndex);
   C5MediaStatusTrack *GetMediaStatusTrack() const;

private:
};

//----------------------------------------------------------------------------

class MTI_CLIPLIB_API C5AudioMediaTrack : public C5MediaTrack,
                                          public C5AudioContentAPI
{
public:
   E5ContentType GetContentType() const {return CLIP5_CONTENT_TYPE_AUDIO;};

   const CAudioFormat* GetAudioFormat() const;
   int GetMediaLocationsBySample(int frameIndex, int sampleOffset,
                                 MTI_INT64 sampleCount,
                                 vector<CMediaLocation> &mediaLocationList,
                                 vector<int> &fieldsFound);
   int GetSampleCountAtField(int frameIndex, int fieldIndex);
   int GetSampleCountAtFrame(int frameIndex);

   int ReadMediaBySample(int index, int sampleOffset, MTI_INT64 sampleCount,
                         MTI_UINT8 *buffer);
   int ReadMediaBySampleMT(int index, int sampleOffset, MTI_INT64 sampleCount,
                           MTI_UINT8 *buffer, int fileHandleIndex);
   int WriteMediaBySample(int index, int sampleOffset, MTI_INT64 sampleCount,
                          MTI_UINT8 *buffer, CTimeTrack *ttpAudioProxy);
};

//----------------------------------------------------------------------------

class MTI_CLIPLIB_API C5VideoMediaTrack : public C5MediaTrack,
                                          public C5VideoContentAPI
{
public:
   E5ContentType GetContentType() const {return CLIP5_CONTENT_TYPE_VIDEO;};

   const CImageFormat* GetImageFormat() const;
};

//----------------------------------------------------------------------------

class MTI_CLIPLIB_API C5SMPTETimecodeTrack : public C5Track,
                                             public C5SMPTETimecodeContentAPI
{
public:
   C5SMPTETimecodeTrack();
   virtual ~C5SMPTETimecodeTrack();

   virtual E5ContentType GetContentType() const {return CLIP5_CONTENT_TYPE_SMPTE_TIMECODE;};
   virtual E5TrackType GetTrackType() const {return CLIP5_TRACK_TYPE_SMPTE_TIMECODE;};

   int GetTimecode(int index, CSMPTETimecode &timecode, float &partialFrame);
   int GetTimecodeFrameRate() const;
   EFrameRate GetTimecodeFrameRateEnum() const;
   EFrameRate GetTimecodeFrameRateEnum(int index);

   int SetTimecode(int index, const CSMPTETimecode &newTimecode,
                   float newPartialFrame);
   int SetTimecodeX(int startIndex, int itemCount,
                   const CSMPTETimecode &newTimecode, float newPartialFrame);
   int SetTimecodeFrameRate(EFrameRate newFrameRate);
   int SetToDummyValue(int startIndex, int itemCount);

   int ExtendTimeTrack(const string &clipPath, int newItemCount);

   virtual int initFromXMLAttributes(C5XMLAttribList &attribList);
   virtual void writeXMLAttributes(CXMLStream &xmlStream);
   virtual void writeXMLStream(CXMLStream &xmlStream);

//   void Dump(ostream &ostrm);

protected:
   EFrameRate timecodeFrameRate;

}; // C5SMPTETimecodeTrack

//----------------------------------------------------------------------------

#ifdef C5_TIMECODE
class MTI_CLIPLIB_API C5TimecodeTrack : public C5Track,
                                        public C5TimecodeContentAPI
{
public:
   C5TimecodeTrack();
   virtual ~C5TimecodeTrack();

   virtual E5ContentType GetContentType() const {return CLIP5_CONTENT_TYPE_TIMECODE;};
   virtual E5TrackType GetTrackType() const {return CLIP5_TRACK_TYPE_TIMECODE;};

   int GetTimecode(int index, CTimecode &timecode);
   int GetTimecodeFrameRate() const;
   EFrameRate GetTimecodeFrameRateEnum() const;
   EFrameRate GetTimecodeFrameRateEnum(int index);

   int SetTimecode(int index, const CTimecode &newTimecode);
   int SetTimecodeX(int startIndex, int itemCount,
                    const CTimecode &newTimecode);
   int SetTimecodeFrameRate(EFrameRate newFrameRate);
   int SetToDummyValue(int startIndex, int itemCount);

   int ExtendTimeTrack(const string &clipPath, int newItemCount);

   virtual int initFromXMLAttributes(C5XMLAttribList &attribList);
   virtual void writeXMLAttributes(CXMLStream &xmlStream);
   virtual void writeXMLStream(CXMLStream &xmlStream);

//   void Dump(ostream &ostrm);

protected:
   EFrameRate timecodeFrameRate;

};  // C5TimecodeTrack

#endif

//----------------------------------------------------------------------------

class MTI_CLIPLIB_API C5KeycodeTrack : public C5Track,
                                       public C5KeycodeContentAPI
{
public:
   C5KeycodeTrack();
   virtual ~C5KeycodeTrack();

   virtual E5ContentType GetContentType() const {return CLIP5_CONTENT_TYPE_KEYCODE;};
   virtual E5TrackType GetTrackType() const {return CLIP5_TRACK_TYPE_KEYCODE;};

   int GetKeycode(int index, CMTIKeykode &keycode);

   int SetKeycode(int index, const CMTIKeykode &newKeycode);
   int SetKeycodeX(int startIndex, int itemCount, const CMTIKeykode &newKeycode);
   int SetToDummyValue(int startIndex, int itemCount);

   int ExtendKeycodeTrack(const string &clipPath, int newItemCount);

   virtual void writeXMLStream(CXMLStream &xmlStream);

   void Dump(ostream &ostrm);

};

//----------------------------------------------------------------------------

class MTI_CLIPLIB_API C5SequenceTrack : public C5Track
{
public:
   C5SequenceTrack();
   virtual ~C5SequenceTrack();

   virtual E5TrackType GetTrackType() const {return CLIP5_TRACK_TYPE_SEQUENCE;};

   virtual int GetAllFieldList(int index, S5FieldListEntry fieldList[],
                               int &count);
   virtual int GetMediaTracks(int startIndex, int itemCount,
                              list<S5MediaTrackListEntry> &outList);

   virtual void writeXMLStream(CXMLStream &xmlStream);

};  // C5SequenceTrack

//----------------------------------------------------------------------------
// C5MediaSequenceTrack
//  Sub-type of C5SequenceTrack that provides an API for
//  audio and video media
//  Note: this class provides member functions only and should
//        not contain any data members

class MTI_CLIPLIB_API C5MediaSequenceTrack : public C5SequenceTrack,
                                             public C5MediaContentAPI
{
public:

   static C5MediaPulldown* CastToMediaPulldown(C5Range *range);
   
   virtual int GetFirstMediaAccess(CMediaAccess* &mediaAccess) const;
   virtual int GetFirstMediaIdentifier(string &mediaIdentifier) const;
   virtual int GetFirstMediaType(EMediaType &mediaType) const;
   virtual MTI_UINT32 getMaxPaddedFieldByteCount(int index, int itemCount);
   virtual const CMediaFormat *GetMediaFormat() const;
   virtual int GetMediaLocationsAllFields(int frameIndex,
                                  vector<CMediaLocation> &mediaLocationList);
   virtual E5MediaStatus GetMediaStatus(int index);

   virtual int PeekAtMediaAllocation(vector<SMediaAllocation> &mediaAllocList,
                                     volatile int* currentFrame,
                                     volatile bool* stopRequest);
   virtual int PeekAtMediaLocation(int frameIndex, int fieldIndex,
                                   string &filename, MTI_INT64 &offset);

   virtual int ReadField(int index, MTI_UINT8 *buffer);
   virtual int WriteField(int index, MTI_UINT8 *buffer);
   virtual int ReadMedia(int index, MTI_UINT8 *buffer[]);
   virtual int ReadMediaMT(int index, MTI_UINT8 *buffer[], int fileHandleIndex);
   virtual int ReadMediaOneField(int frameIndex, int fieldIndex,
                                 MTI_UINT8 *buffer);
   virtual int ReadMediaOneFieldMT(int frameIndex, int fieldIndex,
                                   MTI_UINT8 *buffer, int fileHandleIndex);
   virtual int WriteMedia(int index, MTI_UINT8 *buffer[]);

   virtual int LSF2(int startIndex, int itemCount, int &allocCount);

   virtual int FreeMedia(const string &clipPath, int startIndex, int itemCount);

   virtual int ClearMediaWritten(int startIndex, int itemCount);
   virtual int UpdateMediaStatusTrackFile(const string &clipPath);
   virtual int ReloadMediaStatusTrackFile(const string &clipPath);

};  // C5MediaSequenceTrack

//----------------------------------------------------------------------------

class MTI_CLIPLIB_API C5AudioSequenceTrack : public C5MediaSequenceTrack,
                                             public C5AudioContentAPI
{
public:

   virtual E5ContentType GetContentType() const {return CLIP5_CONTENT_TYPE_AUDIO;};

   virtual const CAudioFormat *GetAudioFormat() const;
   virtual int GetSampleCountAtField(int frameIndex, int fieldIndex);
   virtual int GetSampleCountAtFrame(int frameIndex);

   int ReadMediaBySample(int index, int sampleOffset, MTI_INT64 sampleCount,
                         MTI_UINT8 *buffer);
   int ReadMediaBySampleMT(int index, int sampleOffset, MTI_INT64 sampleCount,
                           MTI_UINT8 *buffer, int fileHandleIndex);
   int WriteMediaBySample(int index, int sampleOffset, MTI_INT64 sampleCount,
                          MTI_UINT8 *buffer, CTimeTrack *ttpAudioProxy);
                          
};  // C5AudioSequenceTrack

//----------------------------------------------------------------------------

class MTI_CLIPLIB_API C5VideoSequenceTrack : public C5MediaSequenceTrack,
                                             public C5VideoContentAPI
{
public:

   virtual E5ContentType GetContentType() const {return CLIP5_CONTENT_TYPE_VIDEO;};

   virtual const CImageFormat *GetImageFormat() const;

};  // C5VideoSequenceTrack

//----------------------------------------------------------------------------

class MTI_CLIPLIB_API C5SMPTETimecodeSequenceTrack : public C5SequenceTrack,
                                               public C5SMPTETimecodeContentAPI
{
public:

   static C5SMPTETimecodePulldown* CastToSMPTETimecodePulldown(C5Range *range);
   
   virtual E5ContentType GetContentType() const {return CLIP5_CONTENT_TYPE_SMPTE_TIMECODE;};

   virtual int GetTimecode(int index, CSMPTETimecode &timecode,
                           float &partialFrame);
   virtual int GetTimecodeFrameRate() const;
   virtual EFrameRate GetTimecodeFrameRateEnum() const;
   virtual EFrameRate GetTimecodeFrameRateEnum(int index);

   virtual int SetTimecode(int index, const CSMPTETimecode &newTimecode,
                           float partialFrame);
   virtual int SetTimecodeX(int index, int count,
                            const CSMPTETimecode &newTimecode,
                            float partialFrame);
   virtual int SetTimecodeFrameRate(EFrameRate newFrameRate);
   virtual int SetToDummyValue(int startIndex, int itemCount);

};  // C5SMPTETimecodeSequenceTrack

//----------------------------------------------------------------------------

class MTI_CLIPLIB_API C5MediaStatusRange : public C5Range
{
   friend class C5MediaStatusTrack;

public:
   C5MediaStatusRange();
   virtual ~C5MediaStatusRange();

   virtual E5ContentType GetContentType() const {return CLIP5_CONTENT_TYPE_NONE;};
   virtual E5RangeType GetRangeType() const {return CLIP5_RANGE_TYPE_MEDIA_STATUS;};

   E5MediaStatus GetMediaStatus(int index) const;
   void SetMediaStatus(E5MediaStatus newMediaStatus);

   virtual int initFromXMLAttributes(C5XMLAttribList &attribList);
   virtual void writeXMLStream(CXMLStream &xmlStream);

protected:
   E5MediaStatus mediaStatus;

private:

}; // C5MediaStatusRange

class MTI_CLIPLIB_API C5MediaStatusTrack : public C5Track
{
public:
   C5MediaStatusTrack();
   virtual ~C5MediaStatusTrack();

   E5ContentType GetContentType() const {return CLIP5_CONTENT_TYPE_NONE;};
   E5TrackType GetTrackType() const {return CLIP5_TRACK_TYPE_MEDIA_STATUS;};

   E5MediaStatus GetMediaStatus(int index);

   virtual int LSF2(int startIndex, int itemCount, int &allocCount);

   int SetMediaStatus(int index, E5MediaStatus newStatus);
   virtual int ClearMediaWritten(int startIndex, int itemCount);
   int SetMediaStatus(int startIndex, int itemCount, E5MediaStatus newStatus,
                      bool allowStatusDecrease, bool allowStatusIncrease);

   int ExtendMediaStatusTrack(int itemCount);

   virtual void writeXMLStream(CXMLStream &xmlStream);

   int GatherMediaStatus(CVideoProxy *videoProxy);

protected:

   C5MediaStatusRange* GetMediaStatusBlock(C5RangeHandle &rangeHandle);

private:

   CThreadLock updateThreadLock;


}; // C5MediaStatusTrack



//----------------------------------------------------------------------------

// Clip 5 Unit Test Functions
MTI_CLIPLIB_API int MakeNewClip(const string &path, const string &newClipName,
                                const string &newClipSchemeName,
                                const string &diskSchemeName,
                                int videoReservationCount,
                                int audioReservationCount,
                                std::ostringstream &errMsg);
MTI_CLIPLIB_API int IncreaseClipMediaTest(const string &binPath,
                                          const string &clipName,
                                          int imageAllocDeltaFrameCount,
                                          int audioAllocDeltaFrameCount);
MTI_CLIPLIB_API int DecreaseClipMediaTest(const string &binPath,
                                          const string &clipName,
                                          int imageAllocDeltaFrameCount,
                                          int audioAllocDeltaFrameCount);

MTI_CLIPLIB_API int ConvertVideoTrack(const string &clipName, int proxyIndex);
MTI_CLIPLIB_API int ConvertAllVideoTracks(const string &clipName);
MTI_CLIPLIB_API CTimecode GetTimecodes(const string &binPath,
                                       const string &clipName, int proxyIndex,
                                       CTimecode &inTC, CTimecode &outTC);
MTI_CLIPLIB_API int SetMediaWritten(const string &binPath, const string &clipName,
                                    int proxyIndex, int startFrameNumber,
                                    int endFrameNumber);
MTI_CLIPLIB_API int ReloadMediaStatusTrackFile(const string &binPath,
                                               const string &clipName,
                                               int proxyIndex);
MTI_CLIPLIB_API int SetSMPTETimecodeTest(const string &binPath,
                                        const string &clipName);
MTI_CLIPLIB_API int SetKeykodeTest(const string &binPath,
                                   const string &clipName);
MTI_CLIPLIB_API int GetAudioSampleCountAtFrame(const string &binPath,
                                               const string &clipName);
MTI_CLIPLIB_API int PeekAtVideoMediaLocation(const string &binPath,
                                             const string &clipName,
                             int proxyIndex, int frameIndex, int fieldIndex,
                             string &filename, MTI_INT64 &offset);
MTI_CLIPLIB_API int PeekAtAudioMediaLocation(const string &binPath, const string &clipName,
                             int trackIndex, int frameIndex, int fieldIndex,
                             string &filename, MTI_INT64 &offset);
MTI_CLIPLIB_API int PeekAtMediaAllocation(const string &binPath, const string &clipName);

//----------------------------------------------------------------------------

MTI_CLIPLIB_API void InitClip5();
guid_t GetNullGUID();
bool IsNullGUID(const guid_t &guid);
string GetNullGUIDStr();

//----------------------------------------------------------------------------
#endif // #ifndef CLIP_5_PROTO_H


