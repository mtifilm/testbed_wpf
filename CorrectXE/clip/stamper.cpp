#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <memory.h>
#include <time.h>

#include "machine.h"
#include "ClipAPI.h"
#include "LineEngine.h"
#include "getopt.h"

////////////////////////////////////////////////////////////////////////

ClipSharedPtr srcClip;

int fntsiz = 6;

int srcVideoProxyIndex = VIDEO_SELECT_NORMAL;
int srcVideoFramingIndex = FRAMING_SELECT_VIDEO;

const CImageFormat *srcVideoTrackFormat;

int srcImageWdth,
    srcImageHght;

int srcBitsPerComponent;

bool srcInterlaced;

int srcFieldCount;

int srcFieldSize;

CVideoFrameList *srcVideoFrameList;

int srcMinFrame,
    srcMaxFrame;

unsigned char *yuvImage[2];

CLineEngine *myEng;

void PrintUsage(char *cpArg)
{
  cerr << "Usage: " << cpArg << " [-s font size] <-A clip name>" << endl;
}


////////////////////////////////////////////////////////////////////////
//
//                         M A I N
//
////////////////////////////////////////////////////////////////////////

int main(int argc, char* argv[])
{
   CreateApplicationName(argv[0]);
   string strClipName;

   if (argc < 2) {

     PrintUsage (argv[0]);
     return(0);
   }

  // parse the input
  int iOpt;
  fntsiz = 2;
  while ( (iOpt = getopt (argc, argv, "s:A:")) != EOF)
   {
    switch (iOpt)
     {
      case 's':
        fntsiz = atoi(optarg);
      break;
      case 'A':
        strClipName = optarg;
      break;
      default:
        PrintUsage (argv[0]);
        return -1;
     }
   }

  printf ("This program will wipe out the data in clip: %s\n",
		strClipName.c_str());
  printf ("Press Y to continue:  ");
  int i = getchar();
  if (i != 'y' && i != 'Y')
    return 0;

   CMediaInterface::initialize();

   CBinManager binMgr;
   int stat;
   string strClip, strBin;


   int iRet = binMgr.splitClipFileName (strClipName, strBin, strClip);
   if (iRet)
    {
     printf ("Unable to parse clip name:  %s  because: %d",
        strClipName.c_str(), iRet);
     return -1;
    }

   srcClip = binMgr.openClip(strBin, strClip,&stat);

   if (stat !=0) {
      printf("Error opening clip\n");
      exit(1);
   }

  if ((fntsiz<1)||(fntsiz>10))
   {
    printf ("Bad font size:  %d\n", fntsiz);
    exit (-1);
   }

  srcVideoTrackFormat = srcClip->getImageFormat(srcVideoProxyIndex);
  srcImageWdth = srcVideoTrackFormat->getPixelsPerLine();
  srcImageHght = srcVideoTrackFormat->getLinesPerFrame();
  srcBitsPerComponent = srcVideoTrackFormat->getBitsPerComponent();
  srcInterlaced = srcVideoTrackFormat->getInterlaced();

  srcVideoFrameList = srcClip->getVideoFrameList(srcVideoProxyIndex,
					  srcVideoFramingIndex);

  srcMinFrame = srcVideoFrameList->getInFrameIndex();
  srcMaxFrame = srcVideoFrameList->getOutFrameIndex() - 1;

  myEng = CLineEngineFactory::makePixelEng(*srcVideoTrackFormat);

  srcFieldSize = srcVideoFrameList->getMaxPaddedFieldByteCount();

  yuvImage[0] = new unsigned char[srcFieldSize];
  yuvImage[1] = NULL;
  if (srcInterlaced)
     yuvImage[1] = new unsigned char[srcFieldSize];
  myEng->setExternalFrameBuffer(yuvImage);

  // clear the frame buffer and set up for writing timecodes

  myEng->setFGColor(0);
  myEng->drawRectangle(0,0,srcImageWdth-1,srcImageHght-1);
  myEng->setFGColor(1);
  myEng->setBGColor(0);
  myEng->setFontSize(fntsiz);

  // set the timecode for each frame, then write it in

  CTimecode curTime(0);

  char curTimeStr[16];

  for (int i=srcMinFrame;i<=srcMaxFrame;i++) {
    if ((i-srcMinFrame)%30 == 0)
       printf ("Doing frame %d out of %d\n", i, srcMaxFrame);

     CVideoFrame *curFrame = srcVideoFrameList->getFrame(i);

     curFrame->getTimecode(curTime);
     curTime.getTimeASCII(curTimeStr);
     myEng->drawString((srcImageWdth-11*8*fntsiz)/2,(srcImageHght-10*fntsiz)/2,curTimeStr);

     curFrame->writeMediaAllFields(yuvImage);

  }

  // Make sure field list is updated with the status of media
  srcClip->updateAllTrackFiles();
  
  // clean up storage

  delete [] yuvImage[0];
  delete [] yuvImage[1];

  CLineEngineFactory::destroyPixelEng(myEng);

  return(0);


}
  
