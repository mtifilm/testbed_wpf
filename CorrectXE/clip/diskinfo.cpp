//---------------------------------------------------------------------------
// diskinfo.cpp
//
// Prints out some information on Raw Disks, mainly a test driver for
// the function CMediaInterface::queryRawDiskInfo
//
//---------------------------------------------------------------------------

#include <stdio.h>
#include "ClipAPI.h"
#include "IniFile.h"

int main(int argc, char* argv[])
{
   int retVal;
   CreateApplicationName(argv[0]);

   printf("Raw Disk Information\n");

   CMediaInterface::initialize();

   CMediaInterface mediaInterface;
   CMediaInterface::RawDiskInfoList rawDiskInfoList;
   int rawDiskCount;
   retVal = mediaInterface.queryRawDiskInfo(rawDiskInfoList);
   if (retVal != 0)
      return 1;

   string mediaIdentifier;
   rawDiskCount = rawDiskInfoList.size();
   printf(   " MediaIdentifier          MediaType   Free Blocks   Largest Free Block\n");
   for (int i = 0; i < rawDiskCount; ++i)
      {
      printf("  %-24s ", rawDiskInfoList[i].mediaIdentifier.c_str());
      switch (rawDiskInfoList[i].mediaType)
         {
         case MEDIA_TYPE_RAW :
            printf(" RAW  ");
            break;
         case MEDIA_TYPE_RAW_OGLV :
            printf(" OGLV ");
            break;
         default :
            printf(" %d ", rawDiskInfoList[i].mediaType);
            break;
         }
#ifdef _WINDOWS
      printf("   %14I64d ", rawDiskInfoList[i].totalFreeBytes/512);
      printf(" %14I64d ", rawDiskInfoList[i].maxContiguousFreeBytes/512);
#else
      printf("   %14lld ", rawDiskInfoList[i].totalFreeBytes/512);
      printf(" %14lld ", rawDiskInfoList[i].maxContiguousFreeBytes/512);
#endif
      printf("\n");
      }

   return 0;
}
//---------------------------------------------------------------------------


 
