// cliplibint.h:  Clip library internal include file.
//
// This file holds various literals and other stuff used to control 
// compiler-specific compilation of the Clip Library
//
/*
$Header: /usr/local/filmroot/clip/include/cliplibint.h,v 1.3 2006/01/13 18:20:29 mlm Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef CLIPLIBINTH
#define CLIPLIBINTH

//////////////////////////////////////////////////////////////////////
// Clip Library API convention
//
// Defines MTI_CLIPLIB_API which is primarily of interest for building
// Windows DLL (dynamic linked library) version of the Clip Library
//
// Defines MTI_CLIPLIB_TEMPLATE which is also used in building DLLs.
// In particular, helps export/import "explicit instantiations" of
// templates.  Explanation for this can be found in Microsoft KnowledgeBase
// articles Q168958: Exporting STL Components Inside and Outside of a Class,
// Q172396: Access Violation When Accessing STL Object in DLL
// and Q263633: BUG: Error C2946 on Explicit Instantiation of Imported Templates
//
// Warning: lack of indentation of preprocessor directives makes this
//          difficult to read, be careful if you modify this
 
#if defined(_WIN32)         // Windows Win32 API

#if defined(MTI_CLIPLIB_NO_DLL)  // Non-DLL Win32 build
#define MTI_CLIPLIB_API
#define MTI_CLIPLIB_TEMPLATE
// End of #if defined(MTI_CLIPLIB_NO_DLL)

#else                            // DLL Win32 build

#if defined(_MSC_VER)         // Win32 API with Microsoft compilers
// MTI_CLIPLIB_API_EXPORT is defined to build the clip DLL and is
// not defined to build users of the clip DLL
#if defined(MTI_CLIPLIB_API_EXPORT)
#define MTI_CLIPLIB_API __declspec(dllexport)
#define MTI_CLIPLIB_TEMPLATE
#else
#define MTI_CLIPLIB_API __declspec(dllimport)
#define MTI_CLIPLIB_TEMPLATE extern
// Disable warning on extern before template instantiation
#pragma warning(disable: 4231)
#endif

// End of #if defined(_MSC_VER)

#else /* Win32 API with non-Microsoft compilers */
// MTI_CLIPLIB_API_EXPORT is defined to build the clip DLL and is
// not defined to build users of the clip DLL
#if defined(MTI_CLIPLIB_API_EXPORT)
#define MTI_CLIPLIB_API __declspec(dllexport)
#define MTI_CLIPLIB_TEMPLATE
#else
#define MTI_CLIPLIB_API __declspec(dllimport)
#define MTI_CLIPLIB_TEMPLATE
#endif

// End of #if defined(_MSC_VER) #else

#endif

// End of #if defined(MTI_CLIPLIB_NO_DLL) #else

#endif

// End of #if defined(_WIN32)

#else
#define MTI_CLIPLIB_API       
#define MTI_CLIPLIB_TEMPLATE
#endif

//////////////////////////////////////////////////////////////////////

#ifdef _MSC_VER
// Disable warning C4786: symbol greater than 255 characters,
// Work-around for problem when using Microsoft compiler 
// with vector of strings or string pointers
#pragma warning(disable : 4786)
#endif // #ifdef _MSC_VER

//////////////////////////////////////////////////////////////////////

#endif // #ifndef CLIP_LIB_INT_H

