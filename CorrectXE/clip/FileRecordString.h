// FileRecordString.h: interface for the CFileRecordString class.
//
/*
$Header: /usr/local/filmroot/clip/include/FileRecordString.h,v 1.1.1.1 2001/06/18 13:38:25 mertus Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef FILERECORDSTRINGH
#define FILERECORDSTRINGH

#include <string>
using std::string;

#include "machine.h"

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CStructuredStream;

//////////////////////////////////////////////////////////////////////

class CFileRecordString  
{
public:
   CFileRecordString();
   CFileRecordString(MTI_UINT16 size, const string& inputString);
   CFileRecordString(const string& inputString);
   CFileRecordString(MTI_UINT16 size);
   virtual ~CFileRecordString();

   int read(CStructuredStream& stream);
   int write(CStructuredStream& stream) const;

   void setSize(MTI_UINT16 newSize);
   void setString(const string& newString, MTI_UINT32 newSize);
   void setString(const string& newString);
   void setString(const char* newString);
   const string& getString() const;

   MTI_UINT16 getFieldSize();
private:
   MTI_UINT16 size;
   string theString;
};

//////////////////////////////////////////////////////////////////////

#endif // #ifndef FILERECORDSTRING_H
