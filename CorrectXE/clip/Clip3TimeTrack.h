// Clip3TimeTrack.h:  interface for the CTCField, CTCFrame,
//                    and CTCFrameList classes
//
// Created by: John Starr, January 24, 2003
//
//////////////////////////////////////////////////////////////////////

/*

A few things about Time Tracks:

1) Time Tracks hold auxillary timecodes, keykodes and events for video
   and audio tracks.

2) A clip can have any number of Time Tracks.

3) The number of frames in a Time Track matches the total number of frames in
   the Main Video track (video framing) or first Audio Track if there isn't
   any video in the clip.  A frame index into the Main Video video-framing
   frame list gets you the corresponding time frame in the Time Track.

4) There are five types of Time Tracks: Timecode, Keykode, SMPTE Timecode, Event,
   & AudioProxy.  A Timecode type Time Track holds run-of-the-mill CTimecodes.
   A SMPTE Timecode type holds timecode and user data a CSMPTETimecode.
   A Keykode type Time Track holds Keykodes in CMTIKeykodes.  An Event
   type Time Track holds 64-bit event flags. An AudioProxy Time Track holds
   10 min-max audio samples per channel (max 8 channels).

5) A Time Track has a single frame list, which holds ptrs to CAudioProxyFrame,
   CEventFrame, CKeykodeFrame, CSMPTETimecode or CTimecodeFrame instances,
   depending on the time type. (Sorry, no mixing of different frame instances in
   a single Time Track).

6) Time Track's have their sections, e.g., TimeTrackInfo0, in the .clp file.
   Time Track's have their frame lists (containing the timecodes/keykodes)
   in corresponding track files.  The Time track files are named something
   like "tt0_f0.trk" with the digit after the "tt" indicating the Time Track
   index.  The time track files are placed in the clip's directory.
   Note: the time track file name does not include the clip's name, unlike
   the other audio and video track files.

7) The CMTIKeykode holds a Kodak-style keykode with a bit of extra information.
   The keykode and extra info is stored in a 22-character string.  Default
   value for the keykode is 22 '0' characters.  CMTIKeykode has a variety
   of accessors and mutators that get/set the keykode to/from strings and
   char*.

8) Time Tracks are added to a clip when the clip is created.  The Time Tracks
   are specified in the Clip Scheme or createclip3 .ini file with sections
   that look like:

    [TimeTrack1]
    AppId=anything-you-want
    TimeType=Timecode

   AppId takes a string arguement that is stored in the Time Track.  The AppId
   provides a way for application code to identify particular Time Tracks.

   TimeType specifies the Time Track's type and must be either Timecode or
   Keykode.

   The TimeTrack section names must be numerically continuous and
   start with 1 (not 0).  E.g., [TimeTrack1], [TimeTrack2].  In this example,
   [TimeTrack5] would be ignored because 3 and 4 are skipped.

Programming with Time Tracks:

1) The number of Time Tracks owned by a clip is returned by:

    CClip::getTimeTrackCount()

2) The Time Track class is CTimeTrack.  To get a pointer to one of the
   clip's Time Tracks, call:

    CClip::getTimeTrack(trackIndex)

   Note: the trackIndex is one less than the track number specified in
         the Clip Scheme or createclip3 .ini file.  Example: TimeTrack1
         in the Clip Scheme has a track index of 0.  Sorry.

3) Given a CTimeTrack instance, the total frame count in the Time Track
   is returned by calling:

    CTimeTrack::getTotalFrameCount()

   Note: the total frame count includes handles if the Main Video track
         has handles.

3a) Mike Braca added 10/22/04:
   To set/get the framing type for the time track, call:

    CTimeTrack::setFramingType(int type)
    int CTimeTrack::getFramingType()

    which will, usually, return FRAMING_TYPE_VIDEO or FRAMING_TYPE_FILM;
    all of the values are defined in Clip3Track.h

4) The CTimeTrack's Application Id string is obtained by a call to:

    CTimeTrack::getAppId()

   This string, set when the clip was created, can be used by application
   code to identify the purpose of a Time Track, e.g., "OriginalAudioTimecode"

5) The CTimeTrack's type is obtained by a call to:

    CTimeTrack::getTimeType()

   Which returns the enumerated type of TIME_TYPE_EVENT, TIME_TYPE_KEYKODE,
   TIME_TYPE_SMPTE_TIMECODE or TIME_TYPE_TIMECODE.

6) If the CTimeTrack's time type is TIME_TYPE_TIMECODE, then an individual
   frame is an instance of CTimecodeFrame.  This a obtained by calling

    CTimeTrack::getTimecodeFrame(frameIndex)

   The CTimecodeFrame class contains a single CTimecode instance.  Calls
   to CTimecodeFrame::getTimecode() and CTimecodeFrame::setTimecode(timecode)
   allow you to obtain and modify the timecode.

7) If the CTimeTrack's time type is TIME_TYPE_KEYKODE, then an individual
   frame is an instance of CKeykodeFrame.  This is obtained by calling:

    CTimeTrack::getKeykodeFrame(frameIndex)

   The CKeykodeFrame class contains a single CMTIKeykode instance.  Getter
   and setter member functions allow you to obtain and modify the keykode as
   C++ strings or Null-terminated C-strings.

8) If the CSMPTETimeTrack's time type is TIME_TYPE_SMPTE_TIMECODE, then an
   individual frame is an instance of CSMPTETimecodeFrame.  This a obtained
   by calling:

    CTimeTrack::getTimecodeFrame(frameIndex)

   The CSMPTETimecodeFrame class contains a single CSMPTETimecode instance.

9) If the CTimeTrack's time type is TIME_TYPE_EVENT, then an individual
   frame is an instance of CEventFrame.  This is obtained by calling:

    CTimeTrack::getEventFrame(frameIndex)

   The CEventFrame class contains a 64-bit event flag.  The meaning
   of a flag is defined by the application.  Getter and setter functions
   allow you to get the entire event flag word or get/set bit combinations
   in the event flag.

10) If the CTimeTrack's time type is TIME_TYPE_AUDIO_PROXY, then an individual
   frame is an instance of CAud3ioProxyFrame. This is obtained by calling:

    CTimeTrack::getAudioProxyFrame(frameIndex)

    TheCAudioProxyFrame class contains eight channels of audio proxy data,
    each of which has ten min-max samples. Getter and setter functions
    allow you to get min, max, or both.

11) If a time frame is modified, e.g., by changing the timecode, a
    flag is set in the frame instance.  This flag indicates that this
    particular frame differs from the frame in the .trk and needs
    to be updated.  The status of this flag can accessed by calling

     CTimeFrame::getNeedsUpdate()

    This flag may also be forced by calling:

     CTimeFrame::setNeedsUpdate(bool newUpdateFlag)

12) If the events/timecodes/keykodes in a Time Track are modified, you can
    write the Time Track back to the .trk file in two ways.  First by
    writing back only those frames that have changed as indicated by
    the needsUpdate flag in the frame.  Second, by re-writing the entire
    .trk file including headers.  The first method is done by calling:

     CTimeTrack::updateFrameListFile()

    The .trk file is opened during the update and then immediately closed.
    The writes to the .trk file are buffered so that groups of frames
    are written simultaneously.

    The second method by calling:

     CTimeTrack::writeFrameListFile()

*/
//////////////////////////////////////////////////////////////////////

#ifndef CLIP3TIMETRACKH
#define CLIP3TIMETRACKH

#include "cliplibint.h"
#include "Clip3Track.h"
#include "Clip5Proto.h"
#include "IniFile.h"
#include "MTIKeykode.h"
#include "SMPTETimecode.h"
#include "timecode.h"

///////////////////////////////////////////////////////////////////////////////
// Forward Declarations

class CClip;

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

enum ETimeType
{
   TIME_TYPE_INVALID        = 0,
   TIME_TYPE_TIMECODE       = 1,
   TIME_TYPE_KEYKODE        = 2,
   TIME_TYPE_SMPTE_TIMECODE = 3,
   TIME_TYPE_EVENT          = 4,
   TIME_TYPE_AUDIO_PROXY    = 5
};

///////////////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CTimeFrame
{
public:
   CTimeFrame();
   virtual ~CTimeFrame();

   virtual ETimeType getTimeType() const = 0;

   virtual void initWithDummyValue() = 0;

   virtual bool getNeedsUpdate() const;
   virtual void setNeedsUpdate(bool newUpdateFlag);

protected:
   bool needsUpdate;
};

// -------------------------------------------------------------------

class MTI_CLIPLIB_API CTimecodeFrame : public CTimeFrame
{
public:
   CTimecodeFrame();
   virtual ~CTimecodeFrame();

   void initWithDummyValue();

   virtual ETimeType getTimeType() const {return TIME_TYPE_TIMECODE;};

   CTimecode getTimecode() const {return m_timecode;};
   void setTimecode(const CTimecode &newTimecode);

   int virtualCopy(CTimecodeFrame &srcFrame);

private:
   CTimecode m_timecode;
};

// -------------------------------------------------------------------

class MTI_CLIPLIB_API CKeykodeFrame : public CTimeFrame
{
public:
   CKeykodeFrame();
   virtual ~CKeykodeFrame();

   void initWithDummyValue();

   virtual ETimeType getTimeType() const {return TIME_TYPE_KEYKODE;};

   int getCalibrationOffset() const {return keykode.getCalibrationOffset();};
   EKeykodeFilmDimension getFilmDimension() const
      {return keykode.getFilmDimension();};
   string getFilmTypeStr() {return keykode.getFilmTypeStr();};
   string getFormattedStr(EKeykodeFormat formatType, bool overrideInterp=false)
      {return keykode.getFormattedStr(formatType, overrideInterp);};
   bool getInterpolateFlag() const {return keykode.getInterpolateFlag();};
   CMTIKeykode getKeykode() const { return keykode;};
   string getKeykodeStr() const {return keykode.getKeykodeStr();};

   void setCalibrationOffset(int newCalibrationOffset);
   void setFilmDimension(EKeykodeFilmDimension newFilmDimension);
   void setInterpolateFlag(bool newInterpolateFlag);

   void setKeykode(const CMTIKeykode &newKeykode);

   // TTT Don't use the following three functions anymore, use
   //     setRawKeykodeStr instead
   void setKeykode(const string &newKeykodeStr);
   void setKeykode(const char *newKeykodeStr);

   void setRawKeykode(const string &newKKStr, int interpolationOffset);
   void setRawKeykode(const char *newKKStr, int interpolationOffset);

   int virtualCopy(CKeykodeFrame &srcFrame);

private:
   CMTIKeykode keykode;
};

// -------------------------------------------------------------------

class MTI_CLIPLIB_API CSMPTETimecodeFrame : public CTimeFrame
{
public:
   CSMPTETimecodeFrame();
   virtual ~CSMPTETimecodeFrame();

   void initWithDummyValue();

   virtual ETimeType getTimeType() const {return TIME_TYPE_SMPTE_TIMECODE;};

   float getPartialFrame() const;
   CTimecode getTimecode(int frameRate) const;
   CSMPTETimecode getSMPTETimecode() const;

   void setPartialFrame(float newPartialFrame);
   void setRawTimecode(MTI_UINT32 newTimecode, MTI_UINT32 newUserData);
   void setSMPTETimecode(const CSMPTETimecode &newTimecode);

   int virtualCopy(CSMPTETimecodeFrame &srcFrame);

private:
   CSMPTETimecode m_smpteTimecode;

   float m_partialFrame;    // Fraction of a single frame
                            // Should be 0.0 <= partialFrame < 1.0,
                            // but no validation is performed
};

// -------------------------------------------------------------------

class MTI_CLIPLIB_API CEventFrame : public CTimeFrame
{
public:
   CEventFrame();
   virtual ~CEventFrame();

   void initWithDummyValue();

   virtual ETimeType getTimeType() const {return TIME_TYPE_EVENT;};

   MTI_UINT64 getEventFlags() const;
   bool getEventState(MTI_UINT64 eventMask) const;

   void setEventFlags(MTI_UINT64 newEventFlags);
   void setEventState(MTI_UINT64 eventMask, bool newValue);

   int virtualCopy(CEventFrame &srcFrame);

private:
   MTI_UINT64 m_eventFlags;
};

//--------------------------------------------------------------------
#define MAX_AUDIO_PROXY_CHANNELS  8
#define MAX_AUDIO_PROXY_SAMPLES  10
#define MAX_AUDIO_SAMPLE  0x7fffffff
#define MIN_AUDIO_SAMPLE  0x80000000

struct MTI_CLIPLIB_API SAudioProxySample
{
   MTI_INT16 minAudio,
             maxAudio;
};

class MTI_CLIPLIB_API CAudioProxyFrame : public CTimeFrame
{
public:
   CAudioProxyFrame();
   virtual ~CAudioProxyFrame();

   void initWithDummyValue();

   virtual ETimeType getTimeType() const {return TIME_TYPE_AUDIO_PROXY;};

   int getMinMaxAudio(int sample, int channel,
                      MTI_INT16 *min, MTI_INT16 *max) const;

   int setMinMaxAudio(int sample, int channel,
                      MTI_INT16 min, MTI_INT16 max);

   int addSampleAudio(int sample, int channel, MTI_INT16 sampleValue);

   int virtualCopy(CAudioProxyFrame &srcFrame);

private:
   SAudioProxySample
      audioProxySample[MAX_AUDIO_PROXY_SAMPLES][MAX_AUDIO_PROXY_CHANNELS];
};

///////////////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CTimeTrack
{
public:
   CTimeTrack(ETimeType newTimeType, EClipVersion newClipVer,
              ClipSharedPtr &newParentClip, int newTrackNumber);
   ~CTimeTrack();

   string getAppId() const {return m_appId;};
   CTimeFrame* getBaseFrame(int frameNumber);  // don't use this
   EClipVersion getClipVer() const {return clipVer;};

   CTimecodeFrame* getTimecodeFrame(int frameNumber);

   CKeykodeFrame* getKeykodeFrame(int frameNumber);
   int getKeykode(int frameNumber, CMTIKeykode &keykode);

   CSMPTETimecodeFrame* getSMPTETimecodeFrame(int frameNumber);
   int getSMPTETimecode(int frameNumber, CSMPTETimecode &timecode,
                        float &partialFrame);

   CEventFrame* getEventFrame(int frameNumber);
   CAudioProxyFrame* getAudioProxyFrame(int frameNumber);
   string getFrameListFileName() const;       // Without file extension
   string getFrameListFileNameWithExt() const; // With file extension
   ETimeType getTimeType() const {return m_timeType;};
   int getTotalFrameCount();

   // Clip 5 API

   C5SMPTETimecodeTrackHandle GetSMPTETimecodeTrackHandle();
   C5KeycodeTrackHandle GetKeycodeTrackHandle();

   EFrameRate getTimecodeFrameRateEnum(int frameNumber);
   EFrameRate getDefaultTimecodeFrameRateEnum();

   int getTrackNumber() const {return m_trackNumber;};

   void setFramingType(int type);
   int getFramingType();

   bool isFrameListAvailable() const;
   bool isFrameIndexValid(int frameIndex);

   void clearAllNeedsUpdate();

   void setAppId(const string &newAppId) {m_appId = newAppId;};

   void setClip5TrackHandle(const C5SMPTETimecodeTrackHandle &newTrackHandle);
   void setClip5TrackHandle(const C5KeycodeTrackHandle &newTrackHandle);


   int setDummySMPTETimecode(int frameNumber);
   int setDummySMPTETimecode(int frameNumber, int count);
   int setSMPTETimecode(int frameNumber, const CSMPTETimecode &newTimecode,
                        float newPartialFrame);
   int setSMPTETimecode(int frameNumber, int count,
                        const CSMPTETimecode &newTimecode,
                        float newPartialFrame);
   int setTimecodeFrameRate(EFrameRate newFrameRate);

   int setDummyKeykode(int frameNumber);
   int setDummyKeykode(int frameNumber, int count);
   int setKeykode(int frameNumber, const CMTIKeykode &newKeykode);
   int setKeykode(int frameNumber, int count, const CMTIKeykode &newKeykode);

   void addFrame(CTimeFrame* frame);
   CTimeFrame* createFrame();
   int createFrameList(int totalFrameCount);
   int extendFrameList(int newFrameCount);

   int extendTimeTrack(int newFrameCount);
   int shortenTimeTrack(int frameCount);

   int virtualCopy(CTimeTrack &srcTrack, int srcFrameIndex, int dstFrameIndex,
                   int frameCount);

   int readTimeTrackSection(CIniFile *iniFile, const string& sectionName);
   EClipVersion readClipVer(CIniFile *iniFile, const string& sectionName);
   int writeTimeTrackSection(CIniFile *iniFile, const string& sectionName);

   string makeFrameListFileName() const;
   string makeNewFrameListFileName(string newClipName) const;

   virtual int readFrameListFile();
   virtual int reloadFrameListFile();
   virtual int writeFrameListFile();
   virtual int updateFrameListFile();
   virtual int updateFrameListFile(int firstFrameIndex, int frameCount,
                                   char *externalBuffer,
                                   unsigned int externalBufferSize);
   virtual int UpdateTrackFile();

   static ETimeType queryTimeType(const string& timeTypeName);
   static string queryTimeTypeName(ETimeType timeType);

   int truncateFrameList(int startFrame);

private:
   void deleteFrameList();
   int extendTimeTrack_ClipVer_3(int newFrameCount);
   int extendTimeTrack_ClipVer_5L(int newFrameCount);

private:
   EClipVersion clipVer;
   
   C5TrackHandle clip5TrackHandle;

   int m_trackNumber;
   int m_framingType;

   bool m_frameListAvailable;

   typedef vector<CTimeFrame*> TimeFrameList;
   TimeFrameList m_frameList;

   ETimeType m_timeType;

   string m_appId;  // arbitrary string that can be used by applications to
                    // identify the purpose of the time track

   ClipWeakPtr m_parentClip;

private:
   static string frameListFileExtension;
   static string clipVerKey;
   static string trackIDKey;
   static string appIdKey;
   static string timeTypeKey;
   static string framingTypeKey;

}; // class CTimeTrack

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class MTI_CLIPLIB_API CTimeTrackList
{
public:
   CTimeTrackList();
   virtual ~CTimeTrackList();

   int createTimeTrack(ETimeType newTimeType, EClipVersion newClipVer,
                       ClipSharedPtr &newParentClip);

   CTimeTrack* getTrack(int trackIndex) const;
   int getTrackCount() const;

   CTimeTrack* findTrack(const string& appId) const;
   int findTrackIndex(const string& appId) const;

   int readTimeTrackListSection(CIniFile *iniFile, ClipSharedPtr &parentClip);
   int refreshTimeTrackListSection(CIniFile *iniFile);
   int writeTimeTrackListSection(CIniFile *iniFile);

protected:
   void addTrack(CTimeTrack *newTrack);
   int getNewTrackNumber() const;

private:
   typedef vector<CTimeTrack*> TrackList;
   TrackList m_trackList;

private:
   void deleteTrackList();

private:
   static string maxTrackIndexKey;
   static string timeTrackSectionPrefix;

}; // class CTimeTrackList

#ifdef PRIVATE_STATIC
#ifndef _CLIP_3_TIME_TRACK_DATA_CPP
namespace Clip3TimeTrackData {
   extern bool iniFileRead;
   extern CMTIKeykodeFilmTypeTable::ManufTable * manufTable;
   extern CMTIKeykodeFilmTypeTable::FilmTypeTableList * allFilmTypeTables;

   extern vector<CMTIKeykodeFilmTypeTable::SRecentSearch> * recentSearchList;

   extern string mtiLocalMachineFileSection;
   extern string mtiLocalMachineFileKey;
   extern string defaultFileName;
   extern string manufacturerCodesSectionName;
   extern string manufacturerSectionPrefix;
   extern string filmTypeTableSectionPrefix;
   extern string filmTypeTableKey;
   extern string manufacturerLetterKey;

   extern string maxTrackIndexKey;
   extern string timeTrackSectionPrefix;

   extern string frameListFileExtension;
   extern string appIdKey;
   extern string timeTypeKey;
   extern string framingTypeKey;
};
#endif
#endif

///////////////////////////////////////////////////////////////////////////////

#endif // #ifndef TIME_TRACK_H

