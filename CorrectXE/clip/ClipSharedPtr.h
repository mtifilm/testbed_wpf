#ifndef ClipSharedPtrH
#define ClipSharedPtrH

#include "cliplibint.h"

#include <memory>
using std::shared_ptr;
using std::weak_ptr;
using std::nullptr_t;
class CClip;

//#define FIND_CLIP_LEAK

#ifndef FIND_CLIP_LEAK

typedef shared_ptr<CClip> ClipSharedPtr;
typedef weak_ptr<CClip> ClipWeakPtr;

#else

typedef shared_ptr<CClip> WrappedClipSharedPtr;
class MTI_CLIPLIB_API ClipSharedPtr : public WrappedClipSharedPtr
{
public:
   ClipSharedPtr();
   ClipSharedPtr(nullptr_t);
   explicit ClipSharedPtr(CClip *clip);
   ClipSharedPtr(const WrappedClipSharedPtr& rhs);
   ClipSharedPtr(const WrappedClipSharedPtr&& rhs);
   ~ClipSharedPtr();
};

typedef weak_ptr<CClip> WrappedClipWeakPtr;
class MTI_CLIPLIB_API ClipWeakPtr : public WrappedClipWeakPtr
{
public:
   ClipWeakPtr();
   ClipWeakPtr(const WrappedClipWeakPtr& rhs);
   ClipWeakPtr(const WrappedClipSharedPtr& sharedPtr);
   ~ClipWeakPtr();
};

#endif

#endif