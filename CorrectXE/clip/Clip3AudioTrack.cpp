// Clip3AudioTrack.cpp: implementation of the CAudioFrameList class.
//
//////////////////////////////////////////////////////////////////////

#define _CLIP_3_AUDIO_TRACK_DATA_CPP //#ifdef PRIVATE_STATIC

#include "Clip3AudioTrack.h"
#include "AudioFormat3.h"
#include "BinManager.h"
#include "Clip3AudioTrackFile.h"
#include "Clip3.h"
#include "err_clip.h"
#include "ImageFormat3.h"
#include "IniFile.h"
#include "MediaAllocator.h"
#include "MediaAccess.h"
#include "MediaInterface.h"
#include "MTIstringstream.h"

#include <math.h>

#ifdef PRIVATE_STATIC
//////////////////////////////////////////////////////////////////////
// The following variables used to be static class members.
// I switched the to namespaced globals for compatability with
// visual c++.  -Aaron Geman  7/9/04
//////////////////////////////////////////////////////////////////////

namespace Clip3AudioTrackData {
   string audioFramingInfoListSection = "AudioFramingList";
   string audioFramingInfoSectionName = "AudioFramingInfo";

   string audioTrackSectionName = "AudioFrameList";

   string audioTrackSection = "AudioTrack";
};
using namespace Clip3AudioTrackData;
#endif

//////////////////////////////////////////////////////////////////////
// Static Member Variables
//////////////////////////////////////////////////////////////////////

string CAudioFramingInfoList::audioFramingInfoListSection = "AudioFramingList";
string CAudioFramingInfoList::audioFramingInfoSectionName = "AudioFramingInfo";

string CAudioFrameList::audioTrackSectionName = "AudioFrameList";






// Audio Track List and Audio Track Info Sections
string CAudioTrackList::audioTrackSection = "AudioTrack";

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                               ################################
// ###    CAudioFramingInfo Class     ###############################
// ####                               ################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAudioFramingInfo::CAudioFramingInfo(int newTrackNumber,
                                       int newFramingType)
: CFramingInfo(TRACK_TYPE_AUDIO, newTrackNumber, newFramingType)
{
}

CAudioFramingInfo::~CAudioFramingInfo()
{
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                                   ############################
// ###    CAudioFramingInfoList Class     ###########################
// ####                                   ############################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAudioFramingInfoList::CAudioFramingInfoList()
{
}

CAudioFramingInfoList::~CAudioFramingInfoList()
{
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

CAudioFramingInfo*
CAudioFramingInfoList::getAudioFramingInfo(int framingIndex) const
{
   return static_cast<CAudioFramingInfo*>(getBaseFramingInfo(framingIndex));
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////


CFramingInfo*
CAudioFramingInfoList::createDerivedFramingInfo(int newFramingNumber,
                                                 int newFramingType)
{
   // Create new instance of CAudioFramingInfo
   CFramingInfo* framingInfo = new CAudioFramingInfo(newFramingNumber,
                                                       newFramingType);

   return framingInfo;
}

//////////////////////////////////////////////////////////////////////
// Read/Write Audio Track List Section of Clip File
//////////////////////////////////////////////////////////////////////

int CAudioFramingInfoList::readAudioFramingInfoListSection(CIniFile *iniFile)
{
   int retVal;

   if (!iniFile->SectionExists(audioFramingInfoListSection))
      return 0;  // Audio Framing Info List section does not exist, so
                 // assume this clip has no Audio

   retVal = readFramingInfoListSection(iniFile, audioFramingInfoListSection,
                                       audioFramingInfoSectionName);
   if (retVal != 0)
      return retVal; // ERROR: Could not read either the FramingInfoList
                     //        section or a Framing Info Section

   return 0;   // Success

} // readAudioFramingInfoListSection

int CAudioFramingInfoList::writeAudioFramingInfoListSection(CIniFile *iniFile)
{
   int retVal;

   if (getFramingCount() == 0)
      return 0; // Framing Info List is empty, so nothing to write

   retVal = writeFramingInfoListSection(iniFile, audioFramingInfoListSection,
                                        audioFramingInfoSectionName);
   if (retVal != 0)
      return retVal; // ERROR: Could not read either the FramingInfoList
                     //        section or a Framing Info Section

   return 0;   // Success

} // writeAudioFramingInfoListSection

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                             ##################################
// ###    CAudioFrameList Class     #################################
// ####                             ##################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAudioFrameList::CAudioFrameList(CAudioFramingInfo *newFramingInfo,
                                   CAudioTrack *newAudioTrack)
: CFrameList(newFramingInfo, newAudioTrack)
{
}

CAudioFrameList::CAudioFrameList(CAudioFramingInfo *newFramingInfo,
                                   CAudioTrack *newAudioTrack,
                                   MTI_UINT32 reserveFrameCount)
: CFrameList(newFramingInfo, newAudioTrack, reserveFrameCount)
{
}

CAudioFrameList::~CAudioFrameList()
{
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

CFrame* CAudioFrameList::createFrame(int fieldCount,
                                       CMediaFormat* mediaFormat,
                                       const CTimecode& timecode)
{
   // Create an instance of Audio Frame, set its parameters and add it
   // to this Frame List

   CAudioFrame *frame = new CAudioFrame(fieldCount);

   frame->setAudioFormat(static_cast<CAudioFormat*>(mediaFormat));
   frame->setTimecode(timecode);

   addFrame(frame);

   return frame;
}

//////////////////////////////////////////////////////////////////////
// Accessor Functions
//////////////////////////////////////////////////////////////////////

const CAudioFormat* CAudioFrameList::getAudioFormat() const
{
   return getAudioTrack()->getAudioFormat();
}

CAudioTrack* CAudioFrameList::getAudioTrack() const
{
   return static_cast<CAudioTrack*>(parentTrack);
}

//------------------------------------------------------------------------
//
// Function:    getFrame
//
// Description: Get a pointer to a particular frame in a clip given the
//              frame number.
//
// Arguments:   int frameNumber  Number of the frame to get.  First frame is 0
//
// Returns:     Pointer to a CAudioFrame object for the specified frame number
//              If the frame number is not valid, returns a NULL pointer
//
//------------------------------------------------------------------------
CAudioFrame* CAudioFrameList::getFrame(int frameNumber)
{
   return static_cast<CAudioFrame*>(getBaseFrame(frameNumber));
}

//------------------------------------------------------------------------
//
// Function:    makeFrameListFileName
//
// Description: Constructs the file name for a Audio Frame List File given
//               the clip name, track number and framing number.
//              Filename does not include the extension
//
//              Example:  "clipname_at0_f1"
//                 where clipname is the clip's name, 0 is the track number
//                 and 1 is the framing number
//
// Arguments:   None
//
// Returns:     string containing constructed file name
//
//------------------------------------------------------------------------
string CAudioFrameList::makeFrameListFileName() const
{
   MTIostringstream ostr;

   // Compose Frame List File Name

   // Clip Name
   CBinManager binMgr;
   auto clip = getAudioTrack()->getParentClip();
   ostr << binMgr.makeClipFileName(clip->getBinPath(), clip->getClipName());

   // Audio Track Number
   ostr << "_at" << getAudioTrack()->getTrackNumber();

   // Framing Number
   ostr << "_f" << framingInfo->getFramingNumber();

   return ostr.str();
}

//------------------------------------------------------------------------
//
// Function:    makeNewFrameListFileName
//
// Description: Primarily useful during renaming a clip.
//              Constructs the file name for a Audio Frame List File given
//              the clip name, track number and framing number.
//              Filename does not include the extension
//
//              Example:  "clipname_at0_f1"
//                 where clipname is the clip's name, 0 is the track number
//                 and 1 is the framing number
//
// Arguments: string newClipName  the new clip name to form the frame
// list file name
//
// Returns:     string containing constructed file name
//
//------------------------------------------------------------------------
string CAudioFrameList::makeNewFrameListFileName(string newClipName) const
{
   MTIostringstream ostr;

   // Compose Frame List File Name

   // Clip Name
   CBinManager binMgr;
   auto clip = getAudioTrack()->getParentClip();
   ostr << AddDirSeparator(clip->getClipPathName()) << newClipName;

   // Audio Track Number
   ostr << "_at" << getAudioTrack()->getTrackNumber();

   // Framing Number
   ostr << "_f" << framingInfo->getFramingNumber();

   return ostr.str();
}

//------------------------------------------------------------------------
//
// Function:    getSampleCountAtFrame
//
// Description: Reads the number of audio samples in a particular frame
//              given a frame number
//
// Arguments    int frameNumber              Frame index in clip
//
// Returns:     Number of audio samples in the frame.  Returns 0 if
//              frameNumber is not within the clip
//
//------------------------------------------------------------------------
int CAudioFrameList::getSampleCountAtFrame(int frameNumber)
{
   if (getClipVer() == CLIP_VERSION_3)
      {
      int iTotalSample;

      CAudioFrame *frame = getFrame(frameNumber);
      if (frame == 0)
         return 0;

      iTotalSample = frame->getDataSizeAtField(0);

      // If this is an interlaced frame, add amount due to field 1
      if (getAudioFormat()->getInterlaced())
         iTotalSample += frame->getDataSizeAtField(1);

      return (iTotalSample);
      }

   else if (getClipVer() == CLIP_VERSION_5L)
      {
      C5AudioTrackHandle audioTrackHandle(getParentTrack()->GetTrackHandle());

      return audioTrackHandle.GetSampleCountAtFrame(frameNumber);
      }

   else
      return 0;
}

//------------------------------------------------------------------------
//
// Function:    getSampleCountAtField
//
// Description: Reads the number of audio samples in a particular field
//              given a frame number and field number
//
// Arguments    int frameNumber              Frame index in clip    
//              int fieldNumber              If clip is interlaced, selects
//                                           first or second field.
//                                             == 0 - first field
//                                             > 0 - second field
//
// Returns:     Number of audio samples in the field.  Returns 0 if 
//              frameNumber is not within the clip   
//
//------------------------------------------------------------------------
int CAudioFrameList::getSampleCountAtField(int frameNumber, int fieldNumber)
{
   if (getClipVer() == CLIP_VERSION_3)
      {
      CAudioFrame *frame = getFrame(frameNumber);
      if (frame == 0)
         return 0;

      // Adjust the field number: Force to 0 if not interlaced, 0 or 1 if
      // interlaced
      if (fieldNumber > 0 && getAudioFormat()->getInterlaced())
         fieldNumber = 1;
      else
         fieldNumber = 0;

      return (frame->getDataSizeAtField(fieldNumber));
      }

   else if (getClipVer() == CLIP_VERSION_5L)
      {
      C5AudioTrackHandle audioTrackHandle(getParentTrack()->GetTrackHandle());

      return audioTrackHandle.GetSampleCountAtField(frameNumber, fieldNumber);
      }

   else
      return 0;

}

//------------------------------------------------------------------------
//
// Function:    getSampleCountAtTimecode
//
// Description: Determines the number of audio samples for a given field
//              at a particular timecode. The number of samples is calculated
//              based on Audio Format rather than read from a field in a clip.
//
// Arguments    const CTimecode& timecode    
//              int fieldNumber              If clip is interlaced, selects
//                                           first or second field.
//                                             == 0 - first field
//                                             > 0 - second field
//
// Returns:     Number of audio samples in the field at the given timecode     
//
//------------------------------------------------------------------------
int CAudioFrameList::getSampleCountAtTimecode(const CTimecode& timecode,
                                               int fieldNumber)
{
   return (getAudioFormat()->getSampleCountAtTimecode(timecode, fieldNumber));
}

//////////////////////////////////////////////////////////////////////
// Virtual Copy
//////////////////////////////////////////////////////////////////////

int CAudioFrameList::virtualCopy(CAudioFrameList &srcFrameList,
                                 int srcIndex, int dstIndex, int frameCount,
                                 CMediaStorageList &dstMediaStorageList)
{
   int retVal;

   // Virtual copy frames
   // Note: assumes valid source and destination frame indices and sufficient
   //       source and destination frames to cover frameCount.  Doesn't check.
   for (int i = 0; i < frameCount; ++i)
      {
      CAudioFrame *dstFrame = getFrame(dstIndex + i);
      CAudioFrame *srcFrame = srcFrameList.getFrame(srcIndex + i);
      retVal = dstFrame->virtualCopy(*srcFrame, dstMediaStorageList);
      if (retVal != 0)
         return retVal;
      }

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Read/Write Media Functions
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:     readMediaBySample
//
// Description:  Reads a block of audio samples given a frame index,
//               sample offset and sample count.  The requested number
//               of samples is read into the caller's buffer, starting
//               at the sample offset from the start of the frame's
//               samples.  The block of samples may be larger than
//               a single field or frame and thus may cross frame and
//               field boundaries.  The read respects the media type and
//               location for each field, so it is safe to use with a
//               virtual media audio track.
//
//               The sample offset must be within the given frame.
//
//               This function attempts to minimize the number of media
//               reads.
//
// Arguments     int frameNumber           Frame index of starting point
//               int sampleOffset          Offset from first sample of
//                                         caller's frameNumber.
//               MTI_INT64 sampleCount     Number of samples to read
//               MTI_UINT8 *buffer         Buffer to accept audio samples
//               int fileHandleIndex       Index of file handle for multi-
//                                         threaded I/O.  Default is 0
//
// Returns:      Status code: 0 = success, other than 0 is error condition
//                 CLIP_ERROR_INVALID_AUDIO_SAMPLE_NUMBER
//                     Caller's sampleOffset is not within the given frame's
//                     audio media
//                 CLIP_ERROR_INVALID_FRAME_NUMBER
//                     Caller's frameNumber is out-of-range
//               Other errors may be returned from lower level functions
//               called for reading the media
//
//------------------------------------------------------------------------
int CAudioFrameList::readMediaBySample(int frameNumber, int sampleOffset,
                                       MTI_INT64 sampleCount, MTI_UINT8 *buffer,
                                       int fileHandleIndex)
{
   int retVal;

   if (getClipVer() == CLIP_VERSION_3)
      {
      retVal = readMediaBySample_ClipVer3(frameNumber, sampleOffset,
                                          sampleCount, buffer,
                                          fileHandleIndex);
      }
   else if (getClipVer() == CLIP_VERSION_5L)
      {
      // Clip 5 "Lite"
      retVal = readMediaBySample_ClipVer5L(frameNumber, sampleOffset,
                                           sampleCount, buffer,
                                           fileHandleIndex);
      }
   else
      {
      retVal = CLIP_ERROR_INVALID_CLIP_VERSION;
      }

   return retVal;
}

int CAudioFrameList::readMediaBySample_ClipVer5L(int frameNumber,
                                                 int sampleOffset,
                                                 MTI_INT64 sampleCount,
                                                 MTI_UINT8 *buffer,
                                                 int fileHandleIndex)
{
   int retVal;
   C5AudioTrackHandle audioTrackHandle(getParentTrack()->GetTrackHandle());

   retVal = audioTrackHandle.ReadMediaBySampleMT(frameNumber, sampleOffset,
                                                 sampleCount, buffer,
                                                 fileHandleIndex);
   if (retVal != 0)
      return 0;

   return 0;
}

int CAudioFrameList::readMediaBySample_ClipVer3(int frameNumber, int sampleOffset,
                                       MTI_INT64 sampleCount, MTI_UINT8 *buffer,
                                       int fileHandleIndex)
{
   int retVal;
   vector<CMediaLocation> mediaLocationList;
   vector<CField*> fieldsFound;   // list of fields that will contribute
                                  // something to the read of audio media

   retVal = getMediaLocationsBySample(frameNumber, sampleOffset, sampleCount,
                                      mediaLocationList, fieldsFound);
   if (retVal != 0)
      return retVal;

#ifndef INIT_AUDIO_FILE
   // Look at the first field that will contribute to the media read.
   // If the first field has its "Media Written" flag set, then assume
   // that all fields have had their media written.  If the first field's
   // "Media Written" flag is not set, assume that all fields have not had
   // their media written and just return a buffer filled with zeros.
   bool mediaWritten = fieldsFound[0]->isMediaWritten();
#else
   bool mediaWritten = true;   // Old way, always assume audio media written
#endif

   // Iterate over the list of accumulated media locations and perform
   // media reads.
   vector<CMediaLocation>::iterator iter;
   MTI_UINT8 *bufPtr = buffer;
   for (iter = mediaLocationList.begin();
        iter != mediaLocationList.end();
        ++iter)
      {
      CMediaLocation *mediaLocation = &(*iter);

      if (mediaWritten)
         {
         retVal = mediaLocation->read(bufPtr, fileHandleIndex);
         if (retVal != 0)
            return retVal;
         }
      else
         {
         // First field's media has never been written, so just fill caller's
         // buffer with zeros without attempting to read the audio media
         memset(bufPtr, 0, mediaLocation->getDataByteSize());
         }

      // Advance pointer into caller's data buffer
      bufPtr += mediaLocation->getDataByteSize();
      }

   return 0;
}

//------------------------------------------------------------------------
//
// Function:     writeMediaBySample
//
// Description:  Writes a block of audio samples given a frame index,
//               sample offset and sample count.  The requested number
//               of samples is written from the caller's buffer, starting
//               at the sample offset from the start of the frame's
//               samples.  The block of samples may be larger than a
//               single field or frame and thus may cross frame and
//               field boundaries.  The write respects the media type and
//               location for each field, so it is safe to use with a
//               virtual media audio track.
//
//               Updates the corresponding set of audio proxy frames
//               from the frame index, sample offset, and sample count
//
//               The sample offset must be within the given frame.
//
//               This function attempts to minimize the number of media
//               writes.
//
// Arguments     int frameNumber           Frame index of starting point
//               int sampleOffset          Offset from first sample of
//                                         caller's frameNumber.
//               MTI_INT64 sampleCount     Number of samples to write
//               MTI_UINT8 *buffer         Buffer to caller's audio samples
//                                         interleaved by channel (0, 1, 2, etc)
//               CTimeTrack *audioProxy    -> audio proxy time track (0 if none)
//
// Returns:      Status code: 0 = success, other than 0 is error condition
//                 CLIP_ERROR_INVALID_AUDIO_SAMPLE_NUMBER
//                     Caller's sampleOffset is not within the given frame's
//                     audio media
//                 CLIP_ERROR_INVALID_FRAME_NUMBER
//                       Caller's frameNumber is out-of-range
//               Other errors may be returned from lower level functions
//               called for writing the media
//
//------------------------------------------------------------------------
int CAudioFrameList::writeMediaBySample(int frameNumber, int sampleOffset,
                                        MTI_INT64 sampleCount,
                                        MTI_UINT8 *buffer,
                                        CTimeTrack *ttpAudioProxy)
{
   int retVal;

   if (getClipVer() == CLIP_VERSION_3)
      {
      retVal = writeMediaBySample_ClipVer3(frameNumber, sampleOffset,
                                           sampleCount, buffer,
                                           ttpAudioProxy);
      }
   else if (getClipVer() == CLIP_VERSION_5L)
      {
      // Clip 5 "Lite"
      retVal = writeMediaBySample_ClipVer5L(frameNumber, sampleOffset,
                                            sampleCount, buffer,
                                            ttpAudioProxy);
      }
   else
      {
      retVal = CLIP_ERROR_INVALID_CLIP_VERSION;
      }

   return retVal;
}

int CAudioFrameList::writeMediaBySample_ClipVer5L(int frameNumber,
                                                  int sampleOffset,
                                                  MTI_INT64 sampleCount,
                                                  MTI_UINT8 *buffer,
                                                  CTimeTrack *ttpAudioProxy)
{
   int retVal;

   C5AudioTrackHandle audioTrackHandle(getParentTrack()->GetTrackHandle());

   retVal = audioTrackHandle.WriteMediaBySample(frameNumber, sampleOffset,
                                                sampleCount, buffer,
                                                ttpAudioProxy);
   if (retVal != 0)
      return 0;

   return 0;
}

int CAudioFrameList::writeMediaBySample_ClipVer3(int frameNumber,
                                                 int sampleOffset,
                                                 MTI_INT64 sampleCount,
                                                 MTI_UINT8 *buffer,
                                                 CTimeTrack *ttpAudioProxy)
{
   int retVal;
   vector<CMediaLocation> mediaLocationList;
   vector<CField*> fieldsFound;   // list of fields that will be written

   retVal = getMediaLocationsBySample(frameNumber, sampleOffset, sampleCount,
                                      mediaLocationList, fieldsFound);
   if (retVal != 0)
      return retVal;

   // Iterate over the list of accumulated media locations and perform
   // media writes.
   vector<CMediaLocation>::iterator iter;
   MTI_UINT8 *bufPtr = buffer;
   for (iter = mediaLocationList.begin();
        iter != mediaLocationList.end();
        ++iter)
      {
      CMediaLocation *mediaLocation = &(*iter);

      retVal = mediaLocation->write(bufPtr);
      if (retVal != 0)
         return retVal;

      // Advance pointer into caller's data buffer
      bufPtr += mediaLocation->getDataByteSize();
      }

#ifndef INIT_AUDIO_FILE
   // For all of the CAudioFields that contributed to the above media writes,
   // set the "Media Written" flag in the field list.  Note that is results in
   // fields being set as "Media Written" even though the media for that field
   // is only partially written
   vector<CField*>::iterator fieldIter;
   for (fieldIter = fieldsFound.begin();
        fieldIter != fieldsFound.end();
        ++fieldIter)
      {
      (*fieldIter)->setMediaWritten(true);
      }
#endif // #ifndef INIT_AUDIO_FILE

// NOTE: The following code was copied to C5AudioMediaTrack::WriteMediaBySample.
//       Why is this code buried here instead of being in the CTimeTrack
//       class with the other audio proxy code?  - JS

   // if no audio proxy we're done
   if (ttpAudioProxy == 0) return 0;

   // at this point we know that the call to
   // getMediaLocationsBySample went thru OK

   // for each frame until samples exhausted do
   //   get frame from audio proxy timetrack
   //   get frame's sample count
   //   for each sample this frame do
   //      derive subframe index
   //      if sample is first for this subframe do
   //         min = sample; max = sample;
   //      else
   //         update min and max
   //   next sample (done?)
   // next frame

   const CAudioFormat *curFormat = getAudioFormat();
   int totalChannels = curFormat->getChannelCount();

   int curSampleOffset = sampleOffset;
   int samplesRemaining = sampleCount;

   MTI_UINT8 *curSamplePt8;
   MTI_INT16 curSampleValue;

   switch(curFormat->getSampleDataSize()) {

      case 16: // 16-bits per sample

         curSamplePt8 = buffer;

         for (int curFrameNumber = frameNumber; curFrameNumber < getTotalFrameCount();
              ++curFrameNumber ) {

            // set up an array to track filling of proxy subframes
            bool binFirst[MAX_AUDIO_PROXY_SAMPLES];
            for (int i=0;i<MAX_AUDIO_PROXY_SAMPLES;i++)
               binFirst[i] = false;

            // get the pointer to the current audio proxy frame
            CAudioProxyFrame *curFrame = ttpAudioProxy->getAudioProxyFrame(curFrameNumber);

            // this many samples for the frame (~ 48000 / fps)
            int curFrameSampleCount = getSampleCountAtFrame(curFrameNumber);

            while (curSampleOffset < curFrameSampleCount) { // within the frame

               // binIndex = 0, 1, 2,  , MAX_AUDIO_PROXY_SAMPLES-1
               int binIndex = MAX_AUDIO_PROXY_SAMPLES * curSampleOffset /
                              curFrameSampleCount;

               if (!binFirst[binIndex]) { // 1st of bin

                  for (int ichan=0;ichan<totalChannels;ichan++) {

                     // big-endian
                     curSampleValue = (curSamplePt8[2*ichan]<<8) +
                                      (curSamplePt8[2*ichan+1]);

                     curFrame->setMinMaxAudio(binIndex, ichan, curSampleValue, curSampleValue);
                  }

                  // mark the bin
                  binFirst[binIndex] = true;
               }
               else {                     // 2nd or later of bin

                  for (int ichan=0;ichan<totalChannels;ichan++) {

                     // big-endian - grab all 16 bits
                     curSampleValue = (curSamplePt8[2*ichan]<<8) +
                                      (curSamplePt8[2*ichan+1]);

                     curFrame->addSampleAudio(binIndex, ichan, curSampleValue);
                  }
               }

               curSamplePt8 += 2*totalChannels;
               curSampleOffset++;

               if (--samplesRemaining == 0) break;
            }

            curFrame->setNeedsUpdate(true);

            if (samplesRemaining == 0) break;

            // set for next frame
            curSampleOffset = 0;
         }

      break;

      case 32: // 32-bits per sample

         curSamplePt8 = buffer;

         for (int curFrameNumber = frameNumber; curFrameNumber < getTotalFrameCount();
              ++curFrameNumber ) {

            // set up an array to track filling of proxy subframes
            bool binFirst[MAX_AUDIO_PROXY_SAMPLES];
            for (int i=0;i<MAX_AUDIO_PROXY_SAMPLES;i++)
               binFirst[i] = false;

            // get the pointer to the current audio proxy frame
            CAudioProxyFrame *curFrame = ttpAudioProxy->getAudioProxyFrame(curFrameNumber);

            // this many samples for the frame (~ 48000 / fps)
            int curFrameSampleCount = getSampleCountAtFrame(curFrameNumber);

            while (curSampleOffset < curFrameSampleCount) { // within the frame

               // binIndex = 0, 1, 2,  , MAX_AUDIO_PROXY_SAMPLES-1
               int binIndex = MAX_AUDIO_PROXY_SAMPLES * curSampleOffset /
                              curFrameSampleCount;

               if (!binFirst[binIndex]) { // 1st of bin

                  for (int ichan=0;ichan<totalChannels;ichan++) {

                     // big-endian - grab the hi 16 bits out of 32
                     curSampleValue = (curSamplePt8[4*ichan]<<8) +
                                      (curSamplePt8[4*ichan+1]);

                     curFrame->setMinMaxAudio(binIndex, ichan,
                               curSampleValue, curSampleValue);
                  }

                  binFirst[binIndex] = true;
               }
               else {                     // 2nd or later of bin

                  for (int ichan=0;ichan<totalChannels;ichan++) {

                     // big-endian
                     curSampleValue = (curSamplePt8[4*ichan]<<24) +
                                      (curSamplePt8[4*ichan+1]<<16)+
                                      (curSamplePt8[4*ichan+2]<<8)+
                                      (curSamplePt8[4*ichan+3]);

                     curFrame->addSampleAudio(binIndex, ichan, curSampleValue);
                  }
               }

               curSamplePt8 += 4*totalChannels;
               curSampleOffset++;

               if (--samplesRemaining == 0) break;
            }

            curFrame->setNeedsUpdate(true);

            if (samplesRemaining == 0) break;

            // set for next frame
            curSampleOffset = 0;
         }

      break;
   }

   return 0;
}

int CAudioFrameList::getMediaLocationsBySample(int frameNumber,
                                               int sampleOffset,
                                               MTI_INT64 sampleCount,
                                      vector<CMediaLocation> &mediaLocationList,
                                               vector<CField*> &fieldsFound)
{
   int retVal;
   int fieldCount = getAudioFormat()->getFieldCount();  // visible field count

   // Check that the frameNumber falls within the frame list
   if (!isFrameIndexValid(frameNumber))
      return CLIP_ERROR_INVALID_FRAME_NUMBER;

   // Check that the caller's sampleOffset falls within the caller's frame
   if (sampleOffset >= getSampleCountAtFrame(frameNumber))
      {
      TRACE_0 (errout << "sampleOffset: " << sampleOffset
                      << " frameNumber: " << frameNumber
                      << " SampleCountAtFrame: "
                      << getSampleCountAtFrame(frameNumber));
      return CLIP_ERROR_INVALID_AUDIO_SAMPLE_NUMBER;
      }

   // Figure out if the sampleOffset falls in the first or second field
   // in an interlaced format.  If progressive, always the first field.
   int firstField;
   if (fieldCount == 1)
      firstField = 0;
   else if (sampleOffset < getSampleCountAtField(frameNumber, 0))
      firstField = 0;
   else
      {
      firstField = 1;
      // Adjust sampleOffset so that it is the offset from the beginning
      // of the second field (field 1)
      sampleOffset -= getSampleCountAtField(frameNumber, 0);
      }

   int fieldIndex = firstField;
   // Iterate over the frames and fields to accumulate media locations
   // that will contribute to the samples that the caller has requested
   int frameCount = getTotalFrameCount();
   bool done = false;
   for (int frameIndex = frameNumber;
        !done && frameIndex < frameCount;
        ++frameIndex)
      {
      CAudioFrame *frame = getFrame(frameIndex);
      for (; !done && fieldIndex < fieldCount; ++fieldIndex)
         {
         CAudioField *field = frame->getField(fieldIndex);

         // Remember the pointer to this field so caller can
         // check/set the "media written" flag
         fieldsFound.push_back(field);

         CMediaLocation tmpMediaLocation = field->getMediaLocation();
         if (frameIndex == frameNumber && fieldIndex == firstField)
            {
            // First pass, adjust tmpMediaLocation for the sampleOffset
            tmpMediaLocation.setDataIndex(tmpMediaLocation.getDataIndex()
                                          + sampleOffset);
            tmpMediaLocation.setDataSize(tmpMediaLocation.getDataSize()
                                         - sampleOffset);
            }
         if (tmpMediaLocation.getDataSize() < sampleCount)
            {
            // Subtract this field's contribution to the sample count
            sampleCount -= tmpMediaLocation.getDataSize();
            }
         else
            {
            // With this field we have all the samples we need.  Shorten
            // the data size of the media location if we do not need all
            // of the samples in this field
            tmpMediaLocation.setDataSize(sampleCount);
            done = true;
            }

         bool addNewMediaLocation = true;
         if (!mediaLocationList.empty())
            {
            // See if we can merge the last media location with the newest
            if (mediaLocationList.back().getMediaAccess()
                                          == tmpMediaLocation.getMediaAccess())
               {
               MTI_INT64 end = mediaLocationList.back().getDataIndex()
                               + mediaLocationList.back().getDataSize();
               if (end == tmpMediaLocation.getDataIndex())
                  {
                  // Append this to end of last media location
                  mediaLocationList.back().setDataSize(
                                         mediaLocationList.back().getDataSize()
                                         + tmpMediaLocation.getDataSize());
                  addNewMediaLocation = false;
                  }
               }
            }

         if (addNewMediaLocation)
            mediaLocationList.push_back(tmpMediaLocation);
         }

      fieldIndex = 0;
      }

   // We have iterated to the end of the frame list without getting all
   // of the samples that the caller requested.  This must be an error.
   if (!done)
      {
      // There are not enough samples available to satisfy the caller's request
      TRACE_0 (errout << "Not enough samples to satisfy the request");
      return CLIP_ERROR_INVALID_AUDIO_SAMPLE_COUNT;
      }

   return 0; // Success

} // getMediaLocationsBySample

//------------------------------------------------------------------------
//
// Function:     initMediaBuffer
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
void CAudioFrameList::initMediaBuffer(MTI_UINT8* buffer[])
{
   int fieldCount;
   const CAudioFormat *audioFormat = getAudioFormat();
   int framingType = getFramingType();
   if (framingType == FRAMING_TYPE_VIDEO)
      {
      // For video-framing frame list, just assume 1 or 2 fields,
      // depending on whether its progressive or interlaced
      fieldCount = audioFormat->getFieldCount();
      }
   else if (framingType == FRAMING_TYPE_FILM
            || framingType == FRAMING_TYPE_CUSTOM)
      {
      // For film- or custom-framing, use the maximum number of fields
      // for any frame within the frame list
      fieldCount = getMaxTotalFieldCount();
      }
   else
      {
      // Unknown framing type, don't do anything
      fieldCount = 0;
      }

   // Estimate the bytes per field.
   MTI_UINT32 byteCount = (MTI_UINT32)(ceil(audioFormat->getSamplesPerField()))
                          * audioFormat->getBytesPerSample()
                          * audioFormat->getChannelCount();

   // Iterate over the buffers for each field
   for (int i = 0; i < fieldCount; ++i)
      {
      memset(buffer[i], 0, byteCount);
      }
}

//////////////////////////////////////////////////////////////////////
// Read/Write File Functions
//////////////////////////////////////////////////////////////////////

int CAudioFrameList::readFrameListFile()
{
   int retVal;

   CAudioFrameListFileReader frameListFileReader;

   retVal = frameListFileReader.readFrameListFile(*this);
   if (retVal != 0)
      return retVal; // ERROR: Could not read the frame list file

   return 0;

} // readFrameListFile

int CAudioFrameList::writeFrameListFile()
{
   int retVal;
   CAudioFrameListFileWriter frameListFileWriter;

   retVal = frameListFileWriter.writeFrameListFile(this);
   if (retVal != 0)
      return retVal;  // ERROR: Frame List file write failed

   return 0;

} // writeFrameListFile

int CAudioFrameList::renameFrameListFile(string newClipName)
{
   // rename the frame list file within the same directory; that is,
   // change the file name, but not the directory name

   int retVal;

   string newFrameListFilePath = makeNewFrameListFileName(newClipName)
       + "." + getFrameListFileExtension();
   string oldFrameListFilePath = 
      makeFrameListFileName() + "." + getFrameListFileExtension();

   retVal = rename(oldFrameListFilePath.c_str(), newFrameListFilePath.c_str());
   return retVal;
} // renameFrameListFile

//////////////////////////////////////////////////////////////////////
// Clip IniFile Interface
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    readFrameListInfoSection
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
int CAudioFrameList::readFrameListInfoSection(CIniFile *iniFile,
                                               const string& sectionPrefix,
                                            const CTimecode& timecodePrototype)
{
   int retVal;
   string sectionName = sectionPrefix + "\\" + audioTrackSectionName;

   retVal = CFrameList::readFrameListInfoSection(iniFile, sectionName,
                                                  timecodePrototype);
   if (retVal != 0)
      return retVal; // ERROR: Failed to read the CFrameList portion of
                     //        the Audio Track section

   return 0;  // Success

} // readFrameListInfoSection( )

//------------------------------------------------------------------------
//
// Function:    writeFrameListInfoSection
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
int CAudioFrameList::writeFrameListInfoSection(CIniFile *iniFile,
                                                const string& sectionPrefix)
{
   int retVal;
   string sectionName = sectionPrefix + "\\" + audioTrackSectionName;

   retVal = CFrameList::writeFrameListInfoSection(iniFile, sectionName);
   if (retVal != 0)
      return retVal; // ERROR: Failed to write the CFrameList portion of
                     //        the Audio Track section

   return 0; // Success

} // writeFrameListInfoSection

//////////////////////////////////////////////////////////////////////
// Miscellaneous Functions
//////////////////////////////////////////////////////////////////////

void CAudioFrameList::dump(MTIostringstream &str)
{
   str << "Audio Frame List Info" << std::endl;

   CFrameList::dump(str);
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                        #######################################
// ###    CAudioTrack Class     ######################################
// ####                        #######################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAudioTrack::CAudioTrack(ClipSharedPtr &newParentClip, EClipVersion newClipVer,
                         int newTrackNumber)
: CTrack(newParentClip, newClipVer, newTrackNumber)
{
   mediaFormat = new CAudioFormat;
}

CAudioTrack::~CAudioTrack()
{
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

const CAudioFormat* CAudioTrack::getAudioFormat() const
{
   if (clipVer == CLIP_VERSION_3)
      return static_cast<const CAudioFormat*>(mediaFormat);

   else if (clipVer == CLIP_VERSION_5L)
      return static_cast<const CAudioFormat*>(clip5TrackHandle.GetMediaFormat());
      
   else
      return 0; // unexpected clip version
}

CAudioFrameList* CAudioTrack::getAudioFrameList(int framingIndex) const
{
   return static_cast<CAudioFrameList*>(getBaseFrameList(framingIndex));
}

const string& CAudioTrack::getMediaDirectory()
{
   if (getClipVer() == CLIP_VERSION_5L)
      {
      // Set the mediaDirectory -- chop off the audio file name and one
      // directory (the guid-named directory); use the rest as the
      // mediaDirectory
      string mediaIdentifier;
      clip5TrackHandle.GetFirstMediaIdentifier(mediaIdentifier);

      string::size_type slashPos = mediaIdentifier.size();
      for (int i = 0; i < 2; ++i)
         {
         if (slashPos == string::npos)
            break;
         slashPos = mediaIdentifier.rfind('\\', slashPos - 1);
         }
      if (slashPos != string::npos)
         mediaDirectory = mediaIdentifier.substr(0, slashPos);
      }

   return mediaDirectory;
}

//////////////////////////////////////////////////////////////////////
//  Mutators
//////////////////////////////////////////////////////////////////////

void CAudioTrack::setAudioFormat(const CAudioFormat& newAudioFormat)
{
   CAudioFormat *audioFormat = static_cast<CAudioFormat*>(mediaFormat);
   *audioFormat = newAudioFormat;
}

void CAudioTrack::setMediaDirectory(const string& newMediaDirectory)
{
   mediaDirectory = newMediaDirectory;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CAudioTrack::extendAudioTrack(int newFrameCount, bool allocateMedia)
{
   int retVal;

   if (getClipVer() == CLIP_VERSION_3)
      {
      retVal = extendAudioTrack_ClipVer3(newFrameCount, allocateMedia);
      }
   else if (getClipVer() == CLIP_VERSION_5L)
      {
      // Clip 5 "Lite"
      retVal = extendAudioTrack_ClipVer5L(newFrameCount, allocateMedia);
      }
   else
      {
      TRACE_0(errout << "ERROR: CAudioTrack::extendAudioTrack: Invalid Clip Version: "
                     << getClipVer());
      retVal = CLIP_ERROR_INVALID_CLIP_VERSION;
      }

   return retVal;
}

int CAudioTrack::extendAudioTrack_ClipVer5L(int newFrameCount, bool allocateMedia)
{
   int retVal;

   // Two scenarios:
   // 1) If the caller wants to allocate media (allocateMedia == true),
   //   then a new media track is created and media storage is allocated.
   //   A new pulldown range that points to the media track is appended
   //   to the existing sequence track
   // 2) If the caller does not want to allocate media, then a filler
   //    range is appended to the existing sequence track

   int newAllocItemCount = (allocateMedia) ? newFrameCount : 0;

   string clipPath = getParentClip()->getClipPathName();

   retVal = clip5TrackHandle.ExtendMediaTrack(clipPath, newFrameCount,
                                              newAllocItemCount);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CAudioTrack::extendAudioTrack_ClipVer3(int newFrameCount, bool allocateMedia)
{
   int retVal;

   // Extend the field list that is shared by all frame lists
   retVal = extendFieldList(newFrameCount);

   // Extend the video-frames frame list by adding new frames
   CAudioFrameList *frameList = getAudioFrameList(FRAMING_SELECT_VIDEO);
   retVal = frameList->extendVideoFrames_FrameList(newFrameCount);
   if (retVal != 0)
      return retVal; // ERROR

   int startFrame = frameList->getTotalFrameCount();

   CMediaStorage *mediaStorage = 0;
   CMediaAccess *mediaAccess = 0;
   CMediaAllocator *mediaAllocator = 0;
   if (getVirtualMediaFlag())
      {
      retVal = assignVirtualMedia(startFrame, newFrameCount, 1);
      if (retVal != 0)
         return retVal; // ERROR
      }
   else if (allocateMedia)
      {
      // Allocate the new media storage from the first media storage item
      // in this track's media storage list.

      mediaStorage = mediaStorageList.getMediaStorage(0);
      if (mediaStorage == 0)
          return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR; // no media storage ??
      mediaAccess = mediaStorage->getMediaAccessObj();
      if (mediaAccess == 0)
          return CLIP_ERROR_UNEXPECTED_INTERNAL_ERROR; // no media access??

      CTimecode frame0Timecode = frameList->getTimecodeForFrameIndex(0);
      mediaAllocator = mediaAccess->extend(*getMediaFormat(), frame0Timecode,
                                           newFrameCount);
      if (mediaAllocator == 0)
         return CLIP_ERROR_MEDIA_ALLOCATION_FAILED; // ERROR: Allocation failed

      // Assign the media storage to each frame/field in the frame list
      retVal = assignMediaStorage(*mediaAllocator, startFrame, newFrameCount);
      if (retVal != 0)
         {
         mediaAccess->cancelExtend(mediaAllocator);
         delete mediaAllocator;
         return retVal;
         }

      // Update the frame list's allocated frame count and allocation frontier
      frameList->getMediaAllocatedFrontier(true);
      }

   // Update the Field List file
   retVal = updateFieldListFile();
   if (retVal != 0)
      {
      if (mediaAllocator != 0)
         {
         // Cancel media storage allocation
         if (mediaAllocator != 0)
            {
            mediaAccess->cancelExtend(mediaAllocator);
            delete mediaAllocator;
            }
         return retVal; // ERROR: Failed to write Field List file
         }
      }

   // Write the updated frame list (.trk) file
   retVal = writeFrameListFile(FRAMING_SELECT_VIDEO);
   if (retVal != 0)
      {
      if (mediaAllocator != 0)
         {
         // Cancel media storage allocation
         mediaAccess->cancelExtend(mediaAllocator);
         delete mediaAllocator;
         }
      return retVal; // ERROR: Failed to write Frame List file
      }
      
   // Write the updated clip (.clp) file
   retVal = getParentClip()->writeFile();
   if (retVal != 0)
      {
      if (mediaAllocator != 0)
         {
         // Cancel media storage allocation
         mediaAccess->cancelExtend(mediaAllocator);
         delete mediaAllocator;
         }
      return retVal; // ERROR: Failed to write Frame List file
      }
      
   delete mediaAllocator;

   return 0;

} // extendAudioTrack_ClipVer3

// ---------------------------------------------------------------------------

int CAudioTrack::shortenAudioTrack(int frameCount)
{
   int retVal;

   CAudioFrameList *frameList = getAudioFrameList(FRAMING_SELECT_VIDEO);
   int totalFrameCount = frameList->getTotalFrameCount();

   if (totalFrameCount < frameCount)
      return CLIP_ERROR_INVALID_FRAME_NUMBER;  // ERROR: arg too big

   if (frameCount == 0)
      return 0;   // Nothing to do

   int startFrame = totalFrameCount - frameCount;

   // Release the media storage
   retVal = releaseMediaStorage(startFrame, frameCount);
   if (retVal != 0)
      return retVal; // ERROR: could not release media storage

   // Shorten all of the frame lists:
   // 1) Shorten the video-frames frame list by removing the CVideoFrames from
   //    the frame list.

   // Truncate the frame list
   retVal = frameList->truncateFrameList(startFrame);
   if (retVal != 0)
      return retVal; // ERROR: could not truncate the frame list


   // Update the Field List file
   retVal = updateFieldListFile();
   if (retVal != 0)
      return retVal; // ERROR: Failed to write Field List file

   // Write the updated frame list (.trk) files
   retVal = writeAllFrameListFiles();
   if (retVal != 0)
      return retVal; // ERROR: Failed to write Frame List file

   // Write the updated clip (.clp) file
   retVal = getParentClip()->writeFile();
   if (retVal != 0)
      return retVal; // ERROR: Failed to write Frame List file

   return 0;

} // shortenAudioTrack

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

CFrameList* CAudioTrack::createFrameList(CFramingInfo *newFramingInfo)
{
   CAudioFramingInfo* newAudioFramingInfo
                             = static_cast<CAudioFramingInfo*>(newFramingInfo);

   CFrameList* frameList = new CAudioFrameList(newAudioFramingInfo, this);

   return frameList;
}

//////////////////////////////////////////////////////////////////////
//  Virtual Copy
//////////////////////////////////////////////////////////////////////

int CAudioTrack::virtualCopy(CAudioTrack &srcTrack, int srcFrameIndex,
                             int dstFrameIndex, int frameCount)
{
   int retVal;

   CAudioFrameList *dstFrameList = getAudioFrameList(0);
   CAudioFrameList *srcFrameList = srcTrack.getAudioFrameList(0);

   retVal = dstFrameList->virtualCopy(*srcFrameList, srcFrameIndex,
                                      dstFrameIndex, frameCount,
                                      mediaStorageList);
   if (retVal != 0)
      return retVal;

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Audio Track Section Read/Write
//////////////////////////////////////////////////////////////////////

// override to to audio-track-specific things
int CAudioTrack::readTrackSections(CIniFile *clipFile,
                                   const string& sectionPrefix,
                                   CFramingInfoList& framingInfoList)
{
   int retVal;

   // Read the base class CTrack's portion of Audio Track Sections
   retVal = CTrack::readTrackSections(clipFile, sectionPrefix,
                                      framingInfoList);
   if (retVal != 0)
      return retVal; // ERROR: Could not read the base class CTrack's portion
                     //        of Audio Track Sections

   // Set the mediaDirectory -- chop off the audio file name and one
   // directory (the guid-named directory); use the rest as the
   // mediaDirectory
   const CMediaStorageList* mediaStorageList;
   mediaStorageList = getMediaStorageList();
   if (mediaStorageList->getMediaStorageCount() > 0)
      {
      CMediaStorage* mediaStorage = mediaStorageList->getMediaStorage(0);
      string mediaIdentifier = mediaStorage->getMediaIdentifier();

      string::size_type slashPos = mediaIdentifier.size();
      for (int i = 0; i < 2; ++i)
         {
         if (slashPos == string::npos)
            break;
         slashPos = mediaIdentifier.rfind('\\', slashPos - 1);
         }
      if (slashPos != string::npos)
         mediaDirectory = mediaIdentifier.substr(0, slashPos);
      }

   return 0; // Success

} // readAudioTrackSections( )

int CAudioTrack::readAudioTrackSections(CIniFile *clipFile,
                                        const string& sectionPrefix,
                                    CAudioFramingInfoList& audioFramingInfoList)
{
   int retVal;

   // Read the base class CTrack's portion of Audio Track Sections
   retVal = readTrackSections(clipFile, sectionPrefix, audioFramingInfoList);
   if (retVal != 0)
      return retVal; // ERROR: Could not read the base class CTrack's portion
                     //        of Audio Track Sections

   return 0; // Success

} // readAudioTrackSections( )

int CAudioTrack::writeAudioTrackSections(CIniFile *clipFile,
                                         const string& sectionPrefix)
{
   int retVal;

   // Write the base class CTrack's portion of Audio Track Sections
   retVal = writeTrackSections(clipFile, sectionPrefix);
   if (retVal != 0)
      return retVal; // ERROR: Could not write the base class CTrack's portion
                     //        of Audio Track Sections

   return 0; // Success

}  // writeAudioTrackSections( )



//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                            ###################################
// ###    CAudioTrackList Class     ##################################
// ####                            ###################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAudioTrackList::CAudioTrackList()
{
}

CAudioTrackList::~CAudioTrackList()
{
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CAudioTrackList::createAudioTrack(ClipSharedPtr &newParentClip,
                                      EClipVersion newClipVer,
                                      int newHandleCount,
                                      const CAudioFormat& newAudioFormat)
{
   // Find a new audio track number
   int trackNumber = getNewTrackNumber();

   CAudioTrack *audioTrack = new CAudioTrack(newParentClip, newClipVer,
                                             trackNumber);

   // Set handles count, probably based on image format type
   audioTrack->setHandleCount(newHandleCount);

   // Set the audio format in the audio track
   audioTrack->setAudioFormat(newAudioFormat);

   addTrack(audioTrack);

   return 0;
}

CTrack* CAudioTrackList::createDerivedTrack(ClipSharedPtr &newParentClip,
                                            EClipVersion newClipVer,
                                            int newTrackNumber)
{
   CAudioTrack *audioTrack = new CAudioTrack(newParentClip, newClipVer,
                                             newTrackNumber);

   addTrack(audioTrack);

   return audioTrack;
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

CAudioTrack* CAudioTrackList::getAudioTrack(int proxyIndex) const
{
   CAudioTrack *audioTrack;
   audioTrack = static_cast<CAudioTrack*>(getBaseTrack(proxyIndex));

   if (audioTrack == 0)
      theError.set(CLIP_ERROR_INVALID_AUDIO_TRACK_INDEX);

   return audioTrack;
}

int CAudioTrackList::getAudioTrackCount() const
{
   return getTrackCount();
}

//////////////////////////////////////////////////////////////////////
// AppId Search
//////////////////////////////////////////////////////////////////////

CAudioTrack* CAudioTrackList::findAudioTrack(const string &appId) const
{
   // Find the first CAudioTrack in the Audio Track List that has an appId
   // that matches (case-sensitive) the caller's appId
   // Returns pointer to CAudioTrack instance if found, NULL pointer if not found

   return static_cast<CAudioTrack*>(findBaseTrack(appId));
}

int CAudioTrackList::findAudioTrackIndex(const string &appId) const
{
   // Find the Track List index of the first CAudioTrack in the Audio Track List
   // that has an appId that matches (case-sensitive) the caller's appId
   // Returns index into the Audio Track List if found, -1 if not found.

   return findBaseTrackIndex(appId);
}

//////////////////////////////////////////////////////////////////////
// Read & Write Parameters
//////////////////////////////////////////////////////////////////////

int CAudioTrackList::readAudioTrackListSection(CIniFile *iniFile, ClipSharedPtr &clip,
                                   CAudioFramingInfoList& audioFramingInfoList)
{
   int retVal;

   if (audioFramingInfoList.getFramingCount() == 0)
      return 0;  // No audio framing, so assume this clip has no audio

   retVal = readTrackListSection(iniFile, audioTrackSection, clip,
                                 audioFramingInfoList);
   if (retVal != 0)
      return retVal; // ERROR: Could not read base class CTrack's portion of
                     //        Audio Track List or Info sections

   return 0; // Success

} // readAudioTrackListSection( )

int CAudioTrackList::refreshAudioTrackListSection(CIniFile *iniFile)
{
   int retVal;

   if (getTrackCount() == 0)
      return 0;  // No audio tracks in list, so nothing to refresh

   retVal = refreshTrackListSection(iniFile, audioTrackSection);
   if (retVal != 0)
      return retVal; // ERROR: Could not refresh base class CTrack's portion of
                     //        Audio Track List or Info sections

   return 0; // Success

} // refreshAudioTrackListSection( )

int CAudioTrackList::writeAudioTrackListSection(CIniFile *iniFile)
{
   int retVal;

   if (getTrackCount() == 0)
      return 0;  // No audio tracks in list, so nothing to write

   retVal = writeTrackListSection(iniFile, audioTrackSection);
   if (retVal != 0)
      return retVal; // ERROR: Could not write base class CTrack's portion of
                     //        Audio Track List or Info sections

   return 0; // Success

} // writeAudioTrackListSection( )















