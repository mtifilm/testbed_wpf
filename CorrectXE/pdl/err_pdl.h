// err_pdl.h: Definitions of tool library error codes.
//
/*
$Header: /usr/local/filmroot/pdl/include/err_pdl.h,v 1.1 2004/09/28 14:02:45 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef PDL_ERRORS_H
#define PDL_ERRORS_H

//////////////////////////////////////////////////////////////////////
// Error Code Defines (Numeric Order)
//
// Note: try to keep numbers in the range of -7600 to -7699

#define PDL_ERROR_SUCCESS                                  0

// Generic error codes
// NB: Use of these is discouraged in favor of specific error codes
#define PDL_ERROR_FUNCTION_NOT_IMPLEMENTED                  -7600
#define PDL_ERROR_UNEXPECTED_INTERNAL_ERROR                 -7601
#define PDL_ERROR_UNEXPECTED_NULL_RETURN                    -7602
#define PDL_ERROR_UNEXPECTED_NULL_ARGUMENT                  -7603

// Specific error codes
#define PDL_ERROR_MISSING_BIN_OR_CLIP_NAME                  -7610
#define PDL_ERROR_INVALID_IN_FRAME_INDEX                    -7611
#define PDL_ERROR_INVALID_OUT_FRAME_INDEX                   -7612

//////////////////////////////////////////////////////////////////////
// Error Code Descriptions (Alpha Order)
/*

PDL_ERROR_INVALID_IN_FRAME_INDEX
   The in frame index in a source or destination media description is
   out of range, i.e., beyond the frame list's in and out timecode

PDL_ERROR_INVALID_OUT_FRAME_INDEX
   The out frame index in a source or destination media description is
   out of range, i.e., beyond the frame list's in and out timecode

PDL_ERROR_MISSING_BIN_OR_CLIP_NAME
   The Bin or Clip name in a source or destination media description
   is blank.

*/
//////////////////////////////////////////////////////////////////////
#endif // #ifndef TOOL_ERRORS_H

