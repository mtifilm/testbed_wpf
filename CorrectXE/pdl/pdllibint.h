// pdllibint.h:  PDL library internal include file.
//
// This file holds various literals and other stuff used to control 
// compiler-specific compilation of the PDL Library
//
/*
$$
*/
//////////////////////////////////////////////////////////////////////

#ifndef PDL_LIB_INT_H
#define PDL_LIB_INT_H

//////////////////////////////////////////////////////////////////////
// PDL Library API convention
//
// Defines MTI_PDLLIB_API which is primarily of interest for building
// Windows DLL (dynamic linked library) version of the PDL Library
//
// Defines MTI_PDLLIB_TEMPLATE which is also used in building DLLs.
// In particular, helps export/import "explicit instantiations" of
// templates.  Explanation for this can be found in Microsoft KnowledgeBase
// articles Q168958: Exporting STL Components Inside and Outside of a Class,
// Q172396: Access Violation When Accessing STL Object in DLL
// and Q263633: BUG: Error C2946 on Explicit Instantiation of Imported Templates
//
// Warning: lack of indentation of preprocessor directives makes this
//          difficult to read, be careful if you modify this
 
#if defined(_WIN32)         // Windows Win32 API

#if defined(MTI_PDLLIB_NO_DLL)  // Non-DLL Win32 build
#define MTI_PDLLIB_API
#define MTI_PDLLIB_TEMPLATE
// End of #if defined(MTI_PDLLIB_NO_DLL)

#else                            // DLL Win32 build

#if defined(_MSC_VER)         // Win32 API with Microsoft compilers
// MTI_PDLLIB_API_EXPORT is defined to build the PDL DLL and is
// not defined to build users of the PDL DLL
#if defined(MTI_PDLLIB_API_EXPORT)
#define MTI_PDLLIB_API __declspec(dllexport)
#define MTI_PDLLIB_TEMPLATE
#else
#define MTI_PDLLIB_API __declspec(dllimport)
#define MTI_PDLLIB_TEMPLATE extern
// Disable warning on extern before template instantiation
#pragma warning(disable: 4231)
#endif

// End of #if defined(_MSC_VER)

#else /* Win32 API with non-Microsoft compilers */
// MTI_PDLLIB_API_EXPORT is defined to build the PDL DLL and is
// not defined to build users of the PDL DLL
#if defined(MTI_PDLLIB_API_EXPORT)
#define MTI_PDLLIB_API __declspec(dllexport)
#define MTI_PDLLIB_TEMPLATE
#else
#define MTI_PDLLIB_API __declspec(dllimport)
#define MTI_PDLLIB_TEMPLATE
#endif

// End of #if defined(_MSC_VER) #else

#endif

// End of #if defined(MTI_PDLLIB_NO_DLL) #else

#endif

// End of #if defined(_WIN32)

#else
#define MTI_PDLLIB_API       
#define MTI_PDLLIB_TEMPLATE
#endif

//////////////////////////////////////////////////////////////////////

#ifdef _MSC_VER
// Disable warning C4786: symbol greater than 255 characters,
// Work-around for problem when using Microsoft compiler 
// with vector of strings or string pointers
#pragma warning(disable : 4786)
#endif // #ifdef _MSC_VER

//////////////////////////////////////////////////////////////////////

#endif // #ifndef PDL_LIB_INT_H

