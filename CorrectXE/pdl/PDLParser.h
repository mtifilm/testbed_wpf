// File: PDLParser.h
//
//       PDL Parser implemented with XML Xerces Parser
//
// --------------------------------------------------------------------------

#ifndef PDLPARSERH
#define PDLPARSERH

// --------------------------------------------------------------------------

#include "pdllibint.h"
#include    "xercesc/sax2/DefaultHandler.hpp"

XERCES_CPP_NAMESPACE_USE

#include "machine.h"

#include <string>
#include <vector>
#include <ostream>

using std::string;
using std::vector;
using std::ostream;

// --------------------------------------------------------------------------
// Forward Declarations

class CPDL;
class CPDLElement;
class CXMLElement;

// --------------------------------------------------------------------------

class MTI_PDLLIB_API CSAXHandlers : public DefaultHandler
{
public:
    CSAXHandlers();
    ~CSAXHandlers();

    // Implementation of the SAX DocumentHandler interface
    void endDocument();
    void endElement( const XMLCh* const uri,
                     const XMLCh* const localname,
                     const XMLCh* const qname);
    void startDocument();
    void startElement(const   XMLCh* const    uri,
                      const   XMLCh* const    localname,
                      const   XMLCh* const    qname,
					  const   Attributes& attributes);
	void characters(const XMLCh* chars, const XMLSize_t length);

   void comment(const XMLCh* chars, const unsigned long length) {};
	void startPrefixMapping(const XMLCh* , const XMLCh*) {};
	void endPrefixMapping(const XMLCh*) {};
	void processingInstruction(const XMLCh*, const XMLCh*) {};
	void elementDecl(const XMLCh*, const XMLCh*) {};
	InputSource* resolveEntity(const XMLCh*, const XMLCh*) { return NULL;};
	void internalEntityDecl(const XMLCh*, const XMLCh*) {};
	void skippedEntity(const XMLCh * const) {};
	void startEntity(const XMLCh * const) {};
	void endEntity(const XMLCh * const) {};
	void unparsedEntityDecl(const XMLCh*, const XMLCh *, const XMLCh *, const XMLCh *) {};
	void attributeDecl(const XMLCh*, const XMLCh*, const XMLCh*, const XMLCh *, const XMLCh *) {};
	void notationDecl(const XMLCh*, const XMLCh*, const XMLCh*) {};
	void startDTD(const XMLCh*, const XMLCh*, const XMLCh*) {};
	void externalEntityDecl(const XMLCh*, const XMLCh*, const XMLCh*) {};

	// Added because XE4 seems to need them
	void ignorableWhitespace(const XMLCh* chars, const unsigned long length) {};

    // Implementation of the SAX ErrorHandler interface
    void warning(const SAXParseException& exception);
    void error(const SAXParseException& exception);
    void fatalError(const SAXParseException& exception);

    CPDL* GetPDL();

private :
   CXMLElement *rootXMLElement;      // Root of PDL class instance tree
   CXMLElement *currentXMLElement;   // Current position in PDL instance tree
   CPDL *rootPDL;
};

//-----------------------------------------------------------------------------

#endif // #ifndef PDL_PARSER_H



