// File: PDLParser.cpp
//
//    Implementation of PDL (Process Decision List) parser for PDL
//    stored as XML
//
// ---------------------------------------------------------------------------

#include <iostream>
#include <fstream>

#include "PDLParser.h"
#include "IniFile.h"
#include "MTIstringstream.h"
#include "PDL.h"

#ifdef __BORLANDC__
  #ifndef NO_XERCES_HACK
	#include <xercesc/sax2/SAX2XMLReader.hpp>
	#include <xercesc/sax2/XMLReaderFactory.hpp>
	#include <xercesc/sax2/Attributes.hpp>
  #endif
#endif

// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------
//  This is a simple class that lets us do easy (though not terribly efficient)
//  trancoding of XMLCh data to local code page for display.
// ---------------------------------------------------------------------------
class StrX
{
  public :
  // -----------------------------------------------------------------------
  //  Constructors and Destructor
  // -----------------------------------------------------------------------
  StrX(const XMLCh* const toTranscode)
  {
    // Call the private transcoding method
	fLocalForm = XMLString::transcode(toTranscode);
  }

  ~StrX()
	{
	   XMLString::release(&fLocalForm);
	}

  // Get a pointer to the local code page form of the XMLString
  const char* localForm() const {return fLocalForm;}

  private :
  char* fLocalForm;  //  This is the local code page form of the string.
};

inline ostream& operator<<(ostream& ostrm, const StrX& toDump)
{
  ostrm << toDump.localForm();
  return ostrm;
}

// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------
//  CSAXHandlers: Constructors and Destructor
// ---------------------------------------------------------------------------
CSAXHandlers::CSAXHandlers() :
   rootXMLElement(0), currentXMLElement(0), rootPDL(0)
{
}

CSAXHandlers::~CSAXHandlers()
{
}

// ---------------------------------------------------------------------------
//  CSAXHandlers: Overrides of the SAX ErrorHandler interface
// ---------------------------------------------------------------------------
void CSAXHandlers::error(const SAXParseException& e)
{
   TRACE_0(errout << "CSAXHandlers: Error at file " << StrX(e.getSystemId())
                  << ", line " << e.getLineNumber()
                  << ", char " << e.getColumnNumber() << endl
                  << "              Message: " << StrX(e.getMessage()));
}

void CSAXHandlers::fatalError(const SAXParseException& e)
{
   TRACE_0(errout << "CSAXHandlers: Fatal Error at file " << StrX(e.getSystemId())
                  << ", line " << e.getLineNumber()
                  << ", char " << e.getColumnNumber() << endl
                  << "              Message: " << StrX(e.getMessage()));
}

void CSAXHandlers::warning(const SAXParseException& e)
{
   TRACE_0(errout << "CSAXHandlers: Warning at file " << StrX(e.getSystemId())
                  << ", line " << e.getLineNumber()
                  << ", char " << e.getColumnNumber() << endl
                  << "              Message: " << StrX(e.getMessage()));
}

// ---------------------------------------------------------------------------
//  CSAXHandlers: Overrides of the SAX DocumentHandler interface
// ---------------------------------------------------------------------------

void CSAXHandlers::startDocument()
{
}

void CSAXHandlers::endDocument()
{
}

void CSAXHandlers::startElement(const XMLCh* const uri,
                                     const XMLCh* const localname,
                                     const XMLCh* const qname,
                                     const Attributes& attributes)
{
   // Convert to ASCII string (don't know if this is kosher)
   string elementName = StrX(qname).localForm();

//TRACE_0(errout << "startElement: " << elementName);

   if (currentXMLElement == 0)
      {
      // First thing, so we want to create a root
      if (elementName == "PDL")
         {
         if (rootPDL != 0)
            {
            TRACE_0(errout << "WARNING: CSAXHandlers::startElement more than one PDL element found");
            }
         rootPDL = new CPDL(NULL);
         currentXMLElement = rootPDL;
         if (rootXMLElement == 0)
            rootXMLElement = currentXMLElement;
         }
      else
         {
         TRACE_0(errout << "ERROR: CSAXHandlers::startElement - PDL parsing confused by "
                        << elementName << std::endl);
         }
      }
   else
      {
      // We've got a root, so send this start element event to
      // the current thing-a-ma-jig

      // Gather the attributes
      CPDLAttribList attributeList;
      string newAttribName, newAttribValue;
      unsigned int attribCount = attributes.getLength();
      for (unsigned int index = 0; index < attribCount; ++index)
         {
         newAttribName = StrX(attributes.getQName(index)).localForm();
         newAttribValue = StrX(attributes.getValue(index)).localForm();
         attributeList.Add(newAttribName, newAttribValue);
         }

      CXMLElement *newElement;
      newElement = currentXMLElement->startElement(elementName, attributeList);
      if (newElement != 0)
         currentXMLElement = newElement;
      }

}

void CSAXHandlers::characters(const XMLCh* chars, const XMLSize_t length)
{
   if (currentXMLElement == 0)
   {
      TRACE_0(errout << "WARNING: CSAXHandlers::characters called with no current element");
      return;
   }

   // This is certainly not kosher. This converts 16-bit XMLCh to ascii by simply
   // strippng the upper 8 bits.
   auto buffer = new char[length];
   auto sourceAsChars = reinterpret_cast<const char *>(chars);
   for (auto i = 0; i < length; ++i)
   {
      buffer[i] = sourceAsChars[i * 2];
   }

   currentXMLElement->characters(string(buffer, length));
   delete[] buffer;
}


void CSAXHandlers::endElement(const XMLCh* const uri,
                              const XMLCh* const localname,
                              const XMLCh* const qname)
{
   // Convert to ASCII string (don't know if this is kosher)
   string elementName = StrX(qname).localForm();
//TRACE_0(errout << "endElement: " << elementName);

   if (currentXMLElement == 0)
   {
      TRACE_0(errout << "WARNING: CSAXHandlers::endElement called with no current element");
      return;
   }

   if (currentXMLElement != 0)
      {
      bool done = currentXMLElement->endElement();
      if (done)
         {
//TRACE_0(errout << "POP " << currentXMLElement << " " <<  currentXMLElement->parent);
         currentXMLElement = currentXMLElement->parent;
         }
      }
}

CPDL* CSAXHandlers::GetPDL()
{
   return rootPDL;
}


// ---------------------------------------------------------------------------

CPDL* ParsePDL(const string &pdlFilename)
{
   MTIostringstream errMsg;

   // This function parses the PDL with the Xerces SAX XML Parser

   // Initialize the XML Parser system
   try
	  {
	  XMLPlatformUtils::Initialize();
	  }
//// Problem with char in XMLException  TODO: Remove when in X64 only
////	catch (const XMLException& toCatch)
	catch (...)
	  {
	  errMsg << "ERROR: ParsePDL Error during Xerces initialization!" << endl;
////             << StrX(toCatch.getMessage());
	  TRACE_0(errout << errMsg.str());
	  theError.set(-1, errMsg);
	  return 0;
	  }

   //  Create a XML parser instance.
   SAX2XMLReader *xmlParser = XMLReaderFactory::createXMLReader();

   // Set parser feature to validate if a DTD or schema is specified
   xmlParser->setFeature(XMLUni::fgSAX2CoreValidation, true);
   xmlParser->setFeature(XMLUni::fgXercesDynamic, true);

   int errorCount = 0;
   CPDL *newPDL = 0;
   try
      {
      // Create an instance of the SAX Event handler class that
      // is specific to parsing our PDL, then tell the parser about
      // the handlers
      CSAXHandlers pdlSAXHandler;
      xmlParser->setContentHandler(&pdlSAXHandler);
      xmlParser->setErrorHandler(&pdlSAXHandler);

      // Start the parsing
      auto exception = false;
      try
      {
         xmlParser->parse(pdlFilename.c_str());
      }
      catch(...)
      {
         exception = true;
      }

      if (exception)
         {
         errMsg << "EXCEPTION: " << errorCount
                << " was raised while parsing PDL " << pdlFilename;
         TRACE_0(errout << errMsg.str());
         //theError.set(-1, errMsg);
         return 0;
         }

      // We're done parsing, get the error count
      errorCount = xmlParser->getErrorCount();
      if (errorCount > 0)
         {
         errMsg << "ERRORS: " << errorCount
                << " were encountered while parsing PDL " << pdlFilename;
         TRACE_0(errout << errMsg.str());
         theError.set(-1, errMsg);
         return 0;
         }

      // Get the CPDL instance that was created by parsing the PDL
      newPDL = pdlSAXHandler.GetPDL();
      }

//// Problem with char in XMLException  TODO: Remove when in X64 only
////	catch (const XMLException& toCatch)
	catch (...)
      {
      errMsg << "ERROR: PDLParse: An error occurred while parsing PDL " << endl
             << "       " << pdlFilename << endl
			 << "       because of XERCES SAX parser error" << endl;
////             << "       " << StrX(toCatch.getMessage());

      // clean up and go home
      XMLPlatformUtils::Terminate();

      TRACE_0(errout << errMsg.str());
      theError.set(-1, errMsg);
      return 0;
      }

    //  Delete the parser instance and then call Terminate
    delete xmlParser;
    XMLPlatformUtils::Terminate();

    // Return the CPDL instance that was created by parsing the PDL
	return newPDL;
}

// ---------------------------------------------------------------------------




