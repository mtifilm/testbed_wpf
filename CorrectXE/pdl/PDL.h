// File: PDL.h
//
//       PDL - Process Decision List
//
// --------------------------------------------------------------------------

#ifndef PDLH
#define PDLH

// --------------------------------------------------------------------------

#include "machine.h"
#include "pdllibint.h"

#include <string>
#include <vector>
#include <list>
#include <ostream>

using std::list;
using std::ostream;
using std::string;
using std::vector;

// --------------------------------------------------------------------------
// Forward Declarations

#include "Clip3.h"
class CImageFormat;
class CPDL;
class CPDLElement;
class CTimecode;
class CVideoField;
class CVideoFrameList;
class CXMLElement;
class CXMLStream;

//-----------------------------------------------------------------------------

enum EPDLEntryStatus
{
   PDL_ENTRY_STATUS_INVALID = 0,             // Uninitialized
   PDL_ENTRY_STATUS_UNPROCESSED,             // Entry has not been processed
   PDL_ENTRY_STATUS_CHECK_FAILED,            // Missing clip, bad marks, etc.
   PDL_ENTRY_STATUS_PROCESSING_IN_PROGRESS,
   PDL_ENTRY_STATUS_PROCESSED_OK,
   PDL_ENTRY_STATUS_PROCESSED_FAILED,
   PDL_ENTRY_STATUS_PROCESSED_STOPPED_BY_USER   // Stop command
};

enum EGlobalInclusionMode
{
   USE_FLAGS = -1,
   ALL_EXCLUSIVE,
   ALL_INCLUSIVE
};

//-----------------------------------------------------------------------------

class MTI_PDLLIB_API CPDLAttrib
{
public:
   CPDLAttrib(const string& newName, const string &newValue);
   CPDLAttrib(const CPDLAttrib &src);
   virtual ~CPDLAttrib();

   CPDLAttrib& operator=(const CPDLAttrib &src);

   void GetIntegerList(vector<int> &intList);// Comman-separated list of integers
   string GetValueString();
   MTI_INT64 GetValueInteger();
   MTI_UINT64 GetValueUnsignedInteger();
   bool GetValueBool();
   double GetValueDouble();

   void SetValueString(const string &newValue);
   void SetValueInteger(MTI_INT64 newValue);
   void SetValueUnsignedInteger(MTI_UINT64 newValue);
   void SetValueBool(bool newValue);
   void SetValueDouble(double newValue);

   void writeXMLStream(CXMLStream &xmlStream);

   void dump(ostream& ostrm, string &indentLevel);

public:
   string name;
   string value;
};

class MTI_PDLLIB_API CPDLAttribList
{
public:
   CPDLAttribList();
   CPDLAttribList(const CPDLAttribList &src);
   virtual ~CPDLAttribList();

   CPDLAttribList& operator=(const CPDLAttribList &src);

   CPDLAttrib* Add(const string &newName, const string &newValue);
   void Clear();
   bool DoesNameExist(const string &attribName);
   string Find(const string &attribName);
   CPDLAttrib* FindAttrib(const string &attribName);
   unsigned int GetCount();
   CPDLAttrib* GetAttrib(unsigned int index);

   void GetIntegerList(const string &attribName, vector<int> &intList);// Comman-separated list of integers
   string GetValueString(const string &attribName);
   string GetValueString(const string &attribName, const string &defaultValue);
   MTI_INT64 GetValueInteger(const string &attribName);
   MTI_INT64 GetValueInteger(const string &attribName, MTI_INT64 defaultValue);
   MTI_UINT64 GetValueUnsignedInteger(const string &attribName);
   MTI_UINT64 GetValueUnsignedInteger(const string &attribName,
                                      MTI_UINT64 defaultValue);
   bool GetValueBool(const string &attribName);
   bool GetValueBool(const string &attribName, bool defaultValue);
   double GetValueDouble(const string &attribName);
   double GetValueDouble(const string &attribName, double defaultValue);

   void SetValueString(const string &attribName, const string &newValue);
   void SetValueInteger(const string &attribName, MTI_INT64 newValue);
   void SetValueUnsignedInteger(const string &attribName, MTI_UINT64 newValue);
   void SetValueBool(const string &attribName, bool newValue);
   void SetValueDouble(const string &attribName, double newValue);

   void writeXMLStream(CXMLStream &xmlStream);

   void dump(ostream& ostrm, string& indentLevel);

public:
   vector<CPDLAttrib*> attributeList;  // list of attributes from this element

};

//-----------------------------------------------------------------------------

class MTI_PDLLIB_API CPDLElement
{
public:
   CPDLElement();
   CPDLElement(const CPDLElement &src);
   virtual ~CPDLElement();

   CPDLElement& operator=(const CPDLElement &src);

   CPDLElement* FindChildElement(const string &targetName);
   CPDLAttribList& GetAttributeList();
   unsigned int GetChildElementCount() const;
   CPDLElement* GetChildElement(unsigned int index) const;
   string GetElementName() const;

   string GetAttribString(const string &attribName);
   string GetAttribString(const string &attribName, const string &defaultValue);
   MTI_INT64 GetAttribInteger(const string &attribName);
   void GetAttribIntegerList(const string &attribName, vector<int> &intList);// Comman-separated list of integers
   MTI_INT64 GetAttribInteger(const string &attribName, MTI_INT64 defaultValue);
   MTI_UINT64 GetAttribUnsignedInteger(const string &attribName);
   MTI_UINT64 GetAttribUnsignedInteger(const string &attribName,
                                       MTI_UINT64 defaultValue);
   bool GetAttribBool(const string &attribName);
   bool GetAttribBool(const string &attribName, bool defaultValue);
   double GetAttribDouble(const string &attribName);
   double GetAttribDouble(const string &attribName, double defaultValue);

   RECT GetAttribRect(const string &attribName, const RECT &defaultRect);

   string GetCdata();

   void SetAttributes(CPDLAttribList &newAttributeList);

   void SetAttribString(const string &attribName, const string &newValue);
   void SetAttribInteger(const string &attribName, MTI_INT64 newValue);
   void SetAttribUnsignedInteger(const string &attribName, MTI_UINT64 newValue);
   void SetAttribBool(const string &attribName, bool newValue);
   void SetAttribDouble(const string &attribName, double newValue);

   void SetAttribRect(const string &attribName, const RECT &newRect);

   void AppendCdata(const string &newCdata);

   CPDLElement* MakeNewChild(const string &newName);

   void writeXMLStream(CXMLStream &xmlStream);

   void dump(ostream& ostrm, string &indentLevel);

public:
   string name;                        // name of this element
   CPDLElement *parent;                // ptr to parent of this element, if
                                       // null, then we're at the root
   vector<CPDLElement*> childList;     // list of sub-elements within this
                                       // element
   CPDLAttribList attributeList;       // list of attributes from this element

   string cdata;                       // string of data associated with the element
   // TBD: characters

private:
   void clear();
   void copyContents(const CPDLElement &src);
};

//-----------------------------------------------------------------------------

class MTI_PDLLIB_API CXMLElement
{
public:
   CXMLElement(CXMLElement *newParent);

   virtual CXMLElement* startElement(const string &elementName,
                                     CPDLAttribList &attributeList) {return NULL;}
   virtual void characters(const string &chars) {}
   virtual bool endElement() {return true;}

   virtual void dump(ostream& ostrm, string &indentLevel) {}

   virtual void writeXMLStream(CXMLStream &xmlStream) {};

   virtual void writeXMLDeclaration(ostream &ostrm);
   virtual void writeXMLStartTag(ostream &ostrm, const string &tag);
   virtual void writeXMLEndTag(ostream &ostrm, const string &tag);
   virtual void writeXMLBeginEmptyElement(ostream &ostrm, const string &tag);
   virtual void writeXMLEndEmptyElement(ostream &ostrm);
   virtual void writeXMLCdata(ostream &ostrm, const string &cdata);

   template<class ValueType> void writeXMLAttr(ostream &ostrm,
                                               const string &attr,
                                               const ValueType& value)
      { ostrm << attr << '=' << value << ' '; };



public:
   CXMLElement *parent;    // use during parsing only
};

//-----------------------------------------------------------------------------

class MTI_PDLLIB_API CPDLEntry_Media : public CXMLElement
{
public:
   CPDLEntry_Media(CXMLElement *newParent);
   CPDLEntry_Media(const CPDLEntry_Media &src);
   virtual ~CPDLEntry_Media();

   CPDLEntry_Media& operator=(const CPDLEntry_Media &src);

   void SetAttributes(CPDLAttribList &attributeList);
   void dump(ostream& ostrm, string &indentLevel);

   int CheckMedia();

   ClipSharedPtr GetClip();
   string GetClipName();
   const CImageFormat* GetImageFormat();
   int GetInTimecode(CTimecode &timecode);
   CVideoField* GetInVideoField();
   int GetOutTimecode(CTimecode &timecode);
   CVideoFrameList *GetVideoFrameList();

   virtual void writeXMLStream(CXMLStream &xmlStream);

public:
   string binPath;          // need a locator for the binPath that is not
                            // dependent on the local disk & directory names
   string clipName;
   int videoProxyIndex;
   int videoFramingIndex;
   int inFrameIndex;        // in and out frame indices much easier to
   int outFrameIndex;       // handle, although not as human-readable
   ClipSharedPtr cachedClip;

private:
   static const string binPathAttr;
   static const string clipNameAttr;
   static const string videoProxyAttr;
   static const string videoFramingAttr;
   static const string inFrameIndexAttr;
   static const string outFrameIndexAttr;

private:
   void copyContent(const CPDLEntry_Media &src);
};

//-----------------------------------------------------------------------------

class MTI_PDLLIB_API CPDLEntry_Tool : public CXMLElement
{
public:
   CPDLEntry_Tool(CXMLElement *newParent);
   CPDLEntry_Tool(const CPDLEntry_Tool &src);
   virtual ~CPDLEntry_Tool();

   CPDLEntry_Tool& operator=(const CPDLEntry_Tool &src);

   CXMLElement* startElement(const string &elementName,
                             CPDLAttribList &attributeList);
   void characters(const string &chars);
   bool endElement();

   void SetAttributes(CPDLAttribList &attributeList);

   virtual void writeXMLStream(CXMLStream &xmlStream);

   void dump(ostream& ostrm, string &indentLevel);

public:
   string toolName;           // name of tool
   int inputPortCount;        // defaults to 1
   int outputPortCount;       // defaults to 1
   CPDLElement *parameters;   // semi-parsed tool parameters as tree of
                              // CPDLElement instances
   CPDLElement *currentElement; // tmp ptr used while parsing tool parameters

   // Maybe: input and output port names
   // Maybe: tool reference name for the edge list

private:
   static const string toolNameAttr;

private:
   void clear();
   void copyContents(const CPDLEntry_Tool &src);
};

//-----------------------------------------------------------------------------

class MTI_PDLLIB_API CPDLEntry_Mask : public CXMLElement
{
public:
   CPDLEntry_Mask(CXMLElement *newParent);
   CPDLEntry_Mask(const CPDLEntry_Mask &src);
   virtual ~CPDLEntry_Mask();

   CPDLEntry_Mask& operator=(const CPDLEntry_Mask &src);

   CPDLElement* NewMaskRegionList();
   CPDLElement* NewAnimatedMaskTimelineList();
   void AddBezier(bool inclusionFlag,
                  bool hiddenFlag,
                  int borderWidth,
                  double blendSlew,
                  BEZIER_POINT *bezierPoints);
   void AddLasso(bool inclusionFlag,
                 bool hiddenFlag,
                 int borderWidth,
                 double blendSlew,
                 POINT *lassoPoints);
   void AddRect(bool inclusionFlag,
                bool hiddenFlag,
                int borderWidth,
                double blendSlew,
                RECT &rect);

   void SetMaskEnabled(bool newFlag);
   bool GetMaskEnabled();
#ifndef USE_PER_REGION_INCLUSION_FLAGS
   void SetGlobalInclusionMode(EGlobalInclusionMode newMode);
   EGlobalInclusionMode GetGlobalInclusionMode();
#endif

   CXMLElement* startElement(const string &elementName,
                             CPDLAttribList &attributeList);
   bool endElement();

   void SetAttributes(CPDLAttribList &attributeList);

   virtual void writeXMLStream(CXMLStream &xmlStream);

   void dump(ostream& ostrm, string &indentLevel);

public:

   bool maskEnabled;
#ifndef USE_PER_REGION_INCLUSION_FLAGS
   // This can't be a bool because it needs 3 states for backward compatibility
   EGlobalInclusionMode globalInclusionMode;
#endif

   CPDLElement *maskRegionList;  // semi-parsed mask regions as tree of
                                 // CPDLElement instances
   CPDLElement *animatedMaskTimelineList;
   
   CPDLElement *currentElement;  // tmp ptr used while parsing mask regions
   
   static const string tag_MaskRegionList;
   static const string tag_MaskRegion;
   static const string tag_AnimatedMaskTimelineList;
   static const string enabledAttr;
   static const string inclusionModeAttr;
   static const string blendSlewAttr;
//   static const string blendPositionCentered;
//   static const string blendPositionInside;
//   static const string blendPositionOutside;
   static const string borderWidthAttr;
   static const string inclusionFlagAttr;
   static const string hiddenFlagAttr;
   static const string pointsAttr;
   static const string shapeTypeAttr;
   static const string shapeTypeBezier;
   static const string shapeTypeLasso;
   static const string shapeTypeRect;

private:
   void clear();
   void copyContents(const CPDLEntry_Mask &src);
   CPDLElement* AddShape(const string &shapeType,
                         bool inclusionFlag, bool hiddenFlag,
                         int borderWidth, double blendSlew);
};

//-----------------------------------------------------------------------------

class MTI_PDLLIB_API CPDLEntry : public CXMLElement
{
public:
   CPDLEntry(CXMLElement *newParent);
   CPDLEntry(const CPDLEntry &src);
   virtual ~CPDLEntry();

   CPDLEntry& operator=(const CPDLEntry& src);

   CXMLElement* startElement(const string &elementName,
                             CPDLAttribList &attributeList);
   bool endElement();

   int CheckEntry();
   void ClearStatus();

   EPDLEntryStatus GetEntryStatus();
   int GetErrorCode();
   string GetErrorMessage();

   bool IsModified();
   bool RefersToClipNamed(const string &clipName);

   void SetEntryStatus(EPDLEntryStatus newEntryStatus);
   void SetErrorCode(int newErrorCode);
   void SetErrorMessage(const string& newErrorMessage);

   CPDLElement* AddTool(const string &newToolName);
   CPDLEntry_Mask* AddMask();

   CPDLEntry_Media* MakeNewMediaSource(const string &binPath,
                                       const string &clipName,
                                       int videoProxyIndex,
                                       int videoFramingIndex,
                                       int inFrameIndex, int outFrameIndex);

   CPDLEntry_Media* MakeNewMediaDest(const string &binPath,
                                     const string &clipName,
                                     int videoProxyIndex, int videoFramingIndex,
                                     int inFrameIndex, int outFrameIndex);

   void SetAttributes(CPDLAttribList &attributeList);

   virtual void writeXMLStream(CXMLStream &xmlStream);

   void dump(ostream& ostrm, string &indentLevel);

private:
   CPDLEntry_Media* MakeNewMedia(const string &binPath, const string &clipName,
                                 int videoProxyIndex, int videoFramingIndex,
                                 int inFrameIndex, int outFrameIndex);

public:
   vector<CPDLEntry_Media*> srcList;
   vector<CPDLEntry_Media*> dstList;
   CPDLEntry_Mask *mask;
   vector<CPDLEntry_Tool*> toolList;
//   vector<CPDLEntry_Edge*> edgeList;


private:
   void clear();
   void copyContents(const CPDLEntry &src);

private:
   EPDLEntryStatus entryStatus;
   int errorCode;
   string errorMessage;
   bool hasChanged;

private:
   static const string entryStatusAttr;
   static const string entryStatusUnprocessed;
   static const string entryStatusCheckFailed;
   static const string entryStatusProcessingInProgress;
   static const string entryStatusProcessedOk;
   static const string entryStatusProcessedFailed;
   static const string entryStatusProcessedStoppedByUser;
   static const string errorCodeAttr;
   static const string errorMessageAttr;

};

//-----------------------------------------------------------------------------

class MTI_PDLLIB_API CPDL : public CXMLElement
{
public:
   typedef list<CPDLEntry*> PDLEntryList;
   typedef PDLEntryList::iterator PDLEntry;

public:
   CPDL(CXMLElement *newParent = nullptr);
   virtual ~CPDL();

   void Clear();

   bool Empty() const;
   int GetEntryCount() const;
   CPDLEntry *GetPDLEntry(PDLEntry pdlEntry);
   bool IsModified();

   // PDL List Manipulation functions
   void AddEntry(CPDLEntry *newEntry);
   PDLEntry AppendEntry(PDLEntry srcEntry);
   void AppendPDL(CPDL &srcPDL);
   void DeleteEntry(PDLEntry pdlEntry);
   PDLEntry InsertEntry(PDLEntry srcEntry, PDLEntry destPosition);
   void InsertPDL(CPDL &srcPDL, PDLEntry destPosition);
   void MoveEntry(PDLEntry pdlEntry, CPDL &destPDL);
   CPDLEntry* MakeNewEntry();
   void ReplaceEntry(CPDL &srcPDL, PDLEntry destPosition);

   // Iteration functions for PDL Rendering (should not be used by other code)
   void InitPDLEntryIterator();
   CPDLEntry *GetNextPDLEntry();

   // Factory Method
   static CPDL* MakeNewPDL();
   static void DestroyPDL(CPDL *pdl);

   // XML Parsing functions
   bool endElement();
   CXMLElement* startElement(const string &elementName,
                             CPDLAttribList &attributeList);

   // XML Writing functions
   void writeXML(ostream &ostrm);
   virtual void writeXMLStream(CXMLStream &xmlStream);

   void dump(ostream& ostrm, string &indentLevel);

public:
   PDLEntryList entryList;    // list of PDL entries

public:
   static const string tag_PDL;
   static const string tag_PDLEntry;
   static const string tag_Source;
   static const string tag_Dest;
   static const string tag_Tool;
   static const string tag_ToolParameters;
   static const string tag_Mask;

private:
   PDLEntry pdlRenderingIterator;   // iterator for GetFirstPDLEntry and
                                    // GetNextPDLEntry only.  This remains
                                    // uninitialized until GetFirstPDLEntry
                                    // is called

   bool hasChanged;
};

//-----------------------------------------------------------------------------

// CPDLRenderInterface
//   An Abstract Base Class which provides an interface between
//   PDL Rendering and the Application.  The Application
//   derives a class from CPDLRenderInterface and implements the
//   various functions that PDL Rendering needs.
//
//   GoToPDLEntry
//     Ask the application to initialize itself based on the instructions
//     in the PDL Entry.  This may include opening source and destination 
//     clips, setting the Marks, activating the Tool and setting the Tool's
//     parameters.  When this function returns success, the tool should be
//     ready to render the PDL Entry.
//
// TBD - Define error codes returned by GoToPDLEntry

class MTI_PDLLIB_API CPDLRenderInterface
{
public:
   CPDLRenderInterface() {};
   virtual ~CPDLRenderInterface() {};

   virtual int GoToPDLEntry(CPDLEntry &pdlEntry) = 0;
};

//---------------------------------------------------------------------------
// Global Functions

CPDL* MTI_PDLLIB_API ParsePDL(const string &pdlFilename);

//-----------------------------------------------------------------------------

#endif // #ifndef PDL_H



