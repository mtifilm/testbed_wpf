// File: PDL.cpp
//
//  Implementation of PDL (Process Decision List) Classes
//
// ---------------------------------------------------------------------------

#include "PDL.h"
#include "ClipAPI.h"
#include "err_pdl.h"
#include "FileSweeper.h"
#include "IniFile.h"
#include "MTIstringstream.h"
#include "XMLWriter.h"

#include <cctype> // for toupper
#include <algorithm>



// ---------------------------------------------------------------------------
// Static Data

const string CPDL::tag_PDL = "PDL";
const string CPDL::tag_PDLEntry = "PDLEntry";
const string CPDL::tag_Source = "Source";
const string CPDL::tag_Dest = "Dest";
const string CPDL::tag_Tool = "Tool";
const string CPDL::tag_ToolParameters = "ToolParameters";
const string CPDL::tag_Mask = "Mask";

const string CPDLEntry::entryStatusAttr = "EntryStatus";
const string CPDLEntry::entryStatusUnprocessed = "Unprocessed";
const string CPDLEntry::entryStatusCheckFailed = "CheckFailed";
const string CPDLEntry::entryStatusProcessingInProgress = "ProcessingInProgress";
const string CPDLEntry::entryStatusProcessedOk = "ProcessedOk";
const string CPDLEntry::entryStatusProcessedFailed = "ProcessedFailed";
const string CPDLEntry::entryStatusProcessedStoppedByUser = "ProcessedStoppedByUser";
const string CPDLEntry::errorCodeAttr = "ErrorCode";
const string CPDLEntry::errorMessageAttr = "ErrorMessage";

const string CPDLEntry_Media::binPathAttr = "BinPath";
const string CPDLEntry_Media::clipNameAttr = "ClipName";
const string CPDLEntry_Media::videoProxyAttr = "VideoProxy";
const string CPDLEntry_Media::videoFramingAttr = "VideoFraming";
const string CPDLEntry_Media::inFrameIndexAttr = "InFrameIndex";
const string CPDLEntry_Media::outFrameIndexAttr = "OutFrameIndex";

const string CPDLEntry_Mask::tag_MaskRegionList = "MaskRegionList";
const string CPDLEntry_Mask::tag_AnimatedMaskTimelineList = "MaskTimelineList";
const string CPDLEntry_Mask::tag_MaskRegion = "MaskRegion";
const string CPDLEntry_Mask::enabledAttr = "Enabled";
const string CPDLEntry_Mask::inclusionModeAttr = "Mode";
const string CPDLEntry_Mask::blendSlewAttr = "BlendSlew";
//const string CPDLEntry_Mask::blendPositionCentered = "Centered";
//const string CPDLEntry_Mask::blendPositionInside = "Inside";
//const string CPDLEntry_Mask::blendPositionOutside = "Outside";
const string CPDLEntry_Mask::borderWidthAttr = "BorderWidth";
const string CPDLEntry_Mask::inclusionFlagAttr = "InclusionFlag";
const string CPDLEntry_Mask::hiddenFlagAttr = "HiddenFlag";
const string CPDLEntry_Mask::pointsAttr = "Points";
const string CPDLEntry_Mask::shapeTypeAttr = "ShapeType";
const string CPDLEntry_Mask::shapeTypeBezier = "Bezier";
const string CPDLEntry_Mask::shapeTypeLasso = "Lasso";
const string CPDLEntry_Mask::shapeTypeRect = "Rect";


const string CPDLEntry_Tool::toolNameAttr = "ToolName";

// ---------------------------------------------------------------------------

void CXMLElement::writeXMLDeclaration(ostream &ostrm)
{
   // write standard XML declaration
   ostrm << "<?xml version=\"1.0\" ?>";
}

void CXMLElement::writeXMLStartTag(ostream &ostrm, const string &tag)
{
   ostrm << '<' << tag << '>';
}

void CXMLElement::writeXMLEndTag(ostream &ostrm, const string &tag)
{
   ostrm << "</" << tag << '>' << endl;
}

void CXMLElement::writeXMLBeginEmptyElement(ostream &ostrm, const string &tag)
{
   ostrm << '<' << tag << ' ';
}

void CXMLElement::writeXMLEndEmptyElement(ostream &ostrm)
{
   ostrm << "/>" << endl;
}

// WARNING: argument cdata MUST NOT CONTAIN the string "]]>"!
void CXMLElement::writeXMLCdata(ostream &ostrm, const string &cdata)
{
   if (isspace(cdata[0]) || isspace(cdata[cdata.length() - 1]))
   {
      auto trimmedCdata = StringTrim(cdata);
      if (trimmedCdata.empty() == false)
      {
         ostrm << "<![CDATA[" << trimmedCdata << "]]>";
      }
   }
   else
   {
      ostrm << "<![CDATA[" << cdata << "]]>";
   }
}

// ---------------------------------------------------------------------------

CPDLElement::CPDLElement()
 : parent(0)
{
}

CPDLElement::CPDLElement(const CPDLElement &src)
 : name(src.name), parent(0)
{
   copyContents(src);
}

CPDLElement::~CPDLElement()
{
   clear();
}

CPDLElement& CPDLElement::operator=(const CPDLElement &src)
{
   if (this == &src)   // check for self-copy
      return *this;

   clear(); // clear existing children

   copyContents(src);

   return *this;
}

void CPDLElement::clear()
{
   for (vector<CPDLElement*>::iterator iter = childList.begin();
        iter!= childList.end();
        ++iter)
      {
      delete *iter;
      }

   childList.clear();

   parent = 0;
}

void CPDLElement::copyContents(const CPDLElement &src)
{
   for (vector<CPDLElement*>::const_iterator iter = src.childList.begin();
        iter!= src.childList.end();
        ++iter)
      {
      CPDLElement *newChildElement = new CPDLElement(*(*iter));
      newChildElement->parent = this;
      childList.push_back(newChildElement);
      }

   attributeList = src.attributeList;
   cdata = src.cdata;
}

CPDLElement* CPDLElement::FindChildElement(const string &targetName)
{
   for (vector<CPDLElement*>::const_iterator iter = childList.begin();
        iter!= childList.end();
        ++iter)
      {
      if ((*iter)->name == targetName)
         return *iter;
      }

   return 0;
}

CPDLAttribList& CPDLElement::GetAttributeList()
{
   return attributeList;
}

CPDLElement* CPDLElement::MakeNewChild(const string &newName)
{
   // Create new child and initialize it
   CPDLElement *child = new CPDLElement;
   child->name = newName;
   child->parent = this;

   // Add new child to the child list
   childList.push_back(child);

   return child;
}

string CPDLElement::GetAttribString(const string &attribName)
{
   return attributeList.GetValueString(attribName);
}

string CPDLElement::GetAttribString(const string &attribName,
                                    const string &defaultValue)
{
   return attributeList.GetValueString(attribName, defaultValue);
}

MTI_INT64 CPDLElement::GetAttribInteger(const string &attribName)
{
   return attributeList.GetValueInteger(attribName);
}

MTI_INT64 CPDLElement::GetAttribInteger(const string &attribName,
                                        MTI_INT64 defaultValue)
{
   return attributeList.GetValueInteger(attribName, defaultValue);
}

void CPDLElement::GetAttribIntegerList(const string &attribName,
                                       vector<int> &intList)
{
   attributeList.GetIntegerList(attribName, intList);
}

MTI_UINT64 CPDLElement::GetAttribUnsignedInteger(const string &attribName)
{
   return attributeList.GetValueUnsignedInteger(attribName);
}

MTI_UINT64 CPDLElement::GetAttribUnsignedInteger(const string &attribName,
                                                 MTI_UINT64 defaultValue)
{
   return attributeList.GetValueUnsignedInteger(attribName, defaultValue);
}

bool CPDLElement::GetAttribBool(const string &attribName)
{
   return attributeList.GetValueBool(attribName);
}

bool CPDLElement::GetAttribBool(const string &attribName, bool defaultValue)
{
   return attributeList.GetValueBool(attribName, defaultValue);
}

RECT CPDLElement::GetAttribRect(const string &attribName,
                                const RECT &defaultRect)
{
   // Get attributes Left, Top, Right and Bottom to construct a Rectangle
   // Uses default values if attribute is not present.
   // Typically the four attributes will be inside a child attribute

   CPDLElement *subAttrib = FindChildElement(attribName);
   if (subAttrib == 0)
      return defaultRect;  // Couldn't find caller's attrib, use default

   RECT newRect;
   newRect.left = subAttrib->GetAttribInteger("Left", defaultRect.left);
   newRect.top = subAttrib->GetAttribInteger("Top", defaultRect.top);
   newRect.right = subAttrib->GetAttribInteger("Right", defaultRect.right);
   newRect.bottom = subAttrib->GetAttribInteger("Bottom", defaultRect.bottom);

   return newRect;
}

string CPDLElement::GetCdata()
{
   return StringTrim(cdata);
}

unsigned int CPDLElement::GetChildElementCount() const
{
   return childList.size();
}

CPDLElement* CPDLElement::GetChildElement(unsigned int index) const
{
   return childList[index];
}

double CPDLElement::GetAttribDouble(const string &attribName)
{
   return attributeList.GetValueDouble(attribName);
}

double CPDLElement::GetAttribDouble(const string &attribName, double defaultValue)
{
   return attributeList.GetValueDouble(attribName, defaultValue);
}

string CPDLElement::GetElementName() const
{
   return name;
}

void CPDLElement::SetAttributes(CPDLAttribList &newAttributeList)
{
   attributeList = newAttributeList;  // (deep) assignment operator
}

void CPDLElement::SetAttribString(const string &attribName,
                                  const string &newValue)
{
   attributeList.SetValueString(attribName, newValue);
}

void CPDLElement::SetAttribInteger(const string &attribName,
                                   MTI_INT64 newValue)
{
   attributeList.SetValueInteger(attribName, newValue);
}

void CPDLElement::SetAttribUnsignedInteger(const string &attribName,
                                           MTI_UINT64 newValue)
{
   attributeList.SetValueUnsignedInteger(attribName, newValue);
}

void CPDLElement::SetAttribBool(const string &attribName, bool newValue)
{
   attributeList.SetValueBool(attribName, newValue);
}

void CPDLElement::SetAttribDouble(const string &attribName, double newValue)
{
   attributeList.SetValueDouble(attribName, newValue);
}

void CPDLElement::SetAttribRect(const string &attribName, const RECT &newRect)
{
   // Construct a Rectangle attribute, which is a child of this CPDLElement
   // Construct a child element with the caller's attrib name and add
   // four attributes for Left, Top, Right and Bottom values of the rectangle

   CPDLElement *rectAttribs = MakeNewChild(attribName);
   rectAttribs->SetAttribInteger("Left", newRect.left);
   rectAttribs->SetAttribInteger("Top", newRect.top);
   rectAttribs->SetAttribInteger("Right", newRect.right);
   rectAttribs->SetAttribInteger("Bottom", newRect.bottom);
}

void CPDLElement::AppendCdata(const string &newCdata)
{
   cdata.append(newCdata);
//   DBTRACE(cdata.length());
//   if (cdata.length() < 20)
//   {
//      DBTRACE(cdata);
//   }
//   else
//   {
//      DBTRACE2(cdata.substr(0, 10), cdata.substr(cdata.length() - 10));
//   }
}

void CPDLElement::writeXMLStream(CXMLStream &xmlStream)
{
//   DBTIMER(CPDLElement__writeXMLStream);

   xmlStream << tag(name);

//   { DBTIMER(attributeList__writeXMLStream);

   attributeList.writeXMLStream(xmlStream);

   if (!childList.empty())
      {
      for (vector<CPDLElement*>::iterator iter = childList.begin();
           iter!= childList.end();
           ++iter)
         {
         (*iter)->writeXMLStream(xmlStream);
         }
      }
//   }

//   { DBTIMER(cdata__writeXMLStream);

   if (!cdata.empty())
      {
      xmlStream << chardata(cdata);
      }
//   }

   xmlStream << endTag();
}

void CPDLElement::dump(ostream& ostrm, string &indentLevel)
{
   ostrm << indentLevel << "E " << name << std::endl;

   string localIndentLevel = indentLevel + ".";

   attributeList.dump(ostrm, localIndentLevel);

   if (!childList.empty())
      {
      for (vector<CPDLElement*>::iterator iter = childList.begin();
           iter!= childList.end();
           ++iter)
         {
         (*iter)->dump(ostrm, localIndentLevel);
         }
      }
}

// ---------------------------------------------------------------------------

CPDLAttrib::CPDLAttrib(const string &newName, const string &newValue)
 : name(newName), value(newValue)
{
}

CPDLAttrib::CPDLAttrib(const CPDLAttrib &src)
 : name(src.name), value(src.value)
{
}

CPDLAttrib::~CPDLAttrib()
{
}

CPDLAttrib& CPDLAttrib::operator=(const CPDLAttrib &src)
{
   if (this != &src)    // check for self-copy
      {
      name = src.name;
      value = src.value;
      }

   return *this;
}

void CPDLAttrib::GetIntegerList(vector<int> &intList)
{
   // Parse a list of integers separated by whitespace or commas
   // Note: this function does not handle hex or 64-bit integers

   string value = GetValueString();

   std::istringstream istrm(value);

   while(!istrm.eof())
      {
      int i;
      istrm >> i;
      if (istrm.fail())
      {
        break;
      }

      intList.push_back(i);
      if (istrm.peek() == ',')
         istrm.ignore(); // skip over commas
      }
}

string CPDLAttrib::GetValueString()
{
   return value;
}

MTI_INT64 CPDLAttrib::GetValueInteger()
{
   MTIistringstream istr(value);
   MTI_INT64 result;

   // Check if value string is hex digits
   if (value.size() > 2)
      {
      // Borland's hex seems to be broken, use this one
      if ((value[0] == '0') && (toupper(value[1]) == 'X'))
         {
         char *p;
         result = strtol(value.c_str(), &p, 16);
         if (p == value.c_str())
             result = 0;  // error
         return result;
         }
      }

    // Parse as a decimal
    if (!(istr >> result))
       result = 0;
    return(result);
}

MTI_UINT64 CPDLAttrib::GetValueUnsignedInteger()
{
   MTIistringstream istr(value);
   MTI_UINT64 result;

   // Parse as a decimal
   if (!(istr >> result))
      result = 0;
   return(result);
}

bool CPDLAttrib::GetValueBool()
{
   return (value == "1")
           || EqualIgnoreCase(value, "true")
           || EqualIgnoreCase(value, "t");
}

double CPDLAttrib::GetValueDouble()
{
   MTIistringstream istr(value);
   double result;

   // Parse as a decimal
   if (!(istr >> result))
      result = 0.0;
   return(result);
}

void CPDLAttrib::SetValueString(const string &newValue)
{
   value = newValue;
}

void CPDLAttrib::SetValueInteger(MTI_INT64 newValue)
{
   MTIostringstream ostrm;
   ostrm << newValue;
   value = ostrm.str();
}

void CPDLAttrib::SetValueUnsignedInteger(MTI_UINT64 newValue)
{
   MTIostringstream ostrm;
   ostrm << newValue;
   value = ostrm.str();
}

void CPDLAttrib::SetValueBool(bool newValue)
{
   value = (newValue) ? "true" : "false";
}

void CPDLAttrib::SetValueDouble(double newValue)
{
   MTIostringstream ostrm;
   ostrm << newValue;
   value = ostrm.str();
}

void CPDLAttrib::writeXMLStream(CXMLStream &xmlStream)
{
   xmlStream << attr(name) << escape(value);
}

void CPDLAttrib::dump(ostream& ostrm, string &indentLevel)
{
   ostrm << indentLevel << "A " << name << "=" << value << std::endl;
}

// ---------------------------------------------------------------------------

CPDLEntry::CPDLEntry(CXMLElement *newParent)
: CXMLElement(newParent), mask(0),
  entryStatus(PDL_ENTRY_STATUS_UNPROCESSED), errorCode(0), hasChanged(false)
{
}

CPDLEntry::CPDLEntry(const CPDLEntry &src)
: CXMLElement(0), mask(0),
  entryStatus(PDL_ENTRY_STATUS_UNPROCESSED), errorCode(0), hasChanged(false)
{
   copyContents(src);
}

CPDLEntry::~CPDLEntry()
{
   clear();
}

CPDLEntry& CPDLEntry::operator=(const CPDLEntry &src)
{
   if (this == &src)  // check for self-copy
      return *this;

   clear(); // clear existing contents, if any

   copyContents(src);

   return *this;
}

bool CPDLEntry::RefersToClipNamed(const string &clipName)
{
   // Check the source list
   vector<CPDLEntry_Media*>::iterator iter;
   for (iter = srcList.begin(); iter != srcList.end(); ++iter)
      {
      if (*iter && (*iter)->GetClipName() == clipName)
         {
         return true;
         }
      }

   // Check the destination list
   for (iter = dstList.begin(); iter != dstList.end(); ++iter)
      {
      if (*iter && (*iter)->GetClipName() == clipName)
         {
         return true;
         }
      }

   return false;
}

void CPDLEntry::clear()
{
   // Clear status, error code and error message
   ClearStatus();

   // delete the source list
   vector<CPDLEntry_Media*>::iterator iter;
   for (iter = srcList.begin(); iter != srcList.end(); ++iter)
      {
      delete *iter;
      }
   srcList.clear();

   // delete the destination list
   for (iter = dstList.begin(); iter != dstList.end(); ++iter)
      {
      delete *iter;
      }
   dstList.clear();

   // delete the mask
   delete mask;

   // delete the tool list
   vector<CPDLEntry_Tool*>::iterator toolIter;
   for (toolIter = toolList.begin(); toolIter != toolList.end(); ++toolIter)
      {
      delete *toolIter;
      }
   toolList.clear();
}

void CPDLEntry::copyContents(const CPDLEntry &src)
{
   // Note: entryStatus, errorCode and errorMessage are not copied

   // Copy the Source List
   vector<CPDLEntry_Media*>::const_iterator iter;
   for (iter = src.srcList.begin(); iter != src.srcList.end(); ++iter)
      {
      CPDLEntry_Media *newMedia = new CPDLEntry_Media(*(*iter));
      srcList.push_back(newMedia);
      }

   // Copy the Destination List
   for (iter = src.dstList.begin(); iter != src.dstList.end(); ++iter)
      {
      CPDLEntry_Media *newMedia = new CPDLEntry_Media(*(*iter));
      dstList.push_back(newMedia);
      }

   // Copy the Mask
   mask = (src.mask != 0) ? new CPDLEntry_Mask(*(src.mask)) : 0;

   // Copy the Tool List
   vector<CPDLEntry_Tool*>::const_iterator toolIter;
   for (toolIter = src.toolList.begin(); toolIter != src.toolList.end(); ++toolIter)
      {
      CPDLEntry_Tool *newTool = new CPDLEntry_Tool(*(*toolIter));
      toolList.push_back(newTool);
      }
}

CXMLElement* CPDLEntry::startElement(const string &elementName,
                                     CPDLAttribList &attributeList)
{
   if (elementName == CPDL::tag_Source)
      {
      CPDLEntry_Media *src = new CPDLEntry_Media(this);
      // set the attributes
      src->SetAttributes(attributeList);
      // add to source list
      srcList.push_back(src);
      return src;
      }
   else if (elementName == CPDL::tag_Dest)
      {
      CPDLEntry_Media *dst = new CPDLEntry_Media(this);
      // set the attributes
      dst->SetAttributes(attributeList);
      // add to source list
      dstList.push_back(dst);
      return dst;
      }
   else if (elementName == CPDL::tag_Mask)
      {
      // Mask
      CPDLEntry_Mask *newMask = new CPDLEntry_Mask(this);
      newMask->SetAttributes(attributeList);
      mask = newMask;
      return newMask;
      }
   else if (elementName == CPDL::tag_Tool)
      {
      CPDLEntry_Tool *tool = new CPDLEntry_Tool(this);
      tool->SetAttributes(attributeList);
      toolList.push_back(tool);
      return tool;
      }
/* TTT Will assume default edge list for now
   else if (elementName == "Edge")
      {

      }
*/
   else
      {
      TRACE_0(errout << "CPDLEntry::startElement - PDL parsing confused by element "
                     << elementName);
      }

   return 0;
}

bool CPDLEntry::endElement()
{
   return true;
}

CPDLElement* CPDLEntry::AddTool(const string &newToolName)
{
   CPDLEntry_Tool *newTool = new CPDLEntry_Tool(this);
   newTool->toolName = newToolName;
   toolList.push_back(newTool);

   CPDLElement *toolParams = new CPDLElement;
   toolParams->name = "ToolAttributes";
   newTool->parameters = toolParams;

   return toolParams;
}

CPDLEntry_Mask* CPDLEntry::AddMask()
{
   CPDLEntry_Mask *newMask = new CPDLEntry_Mask(this);
   mask = newMask;

   return newMask;
}

int CPDLEntry::CheckEntry()
{
   // Being optimistic, we start with no errors
   int retVal = 0;
   theError.set(0);

   vector<CPDLEntry_Media*>::iterator iter;

   // Iterate over the source media, quit on first error
   for (iter = srcList.begin(); retVal == 0 && iter != srcList.end(); ++iter)
      {
      retVal = (*iter)->CheckMedia();
      }

   // Iterate over the destination media, quit if already an error or a new error
   for (iter = dstList.begin(); retVal == 0 &&iter != dstList.end(); ++iter)
      {
      retVal = (*iter)->CheckMedia();
      }

   if (retVal == 0)
      {
      // Entry is okay, no errors
      // If a previous check failed, clear the status.  Other status types,
      // e.g., PDL_ENTRY_STATUS_PROCESSED, are left unchanged
      if (GetEntryStatus() == PDL_ENTRY_STATUS_CHECK_FAILED)
         ClearStatus();
      }
   else
      {
      // An error was detected, so mark this PDL Entry
      SetEntryStatus(PDL_ENTRY_STATUS_CHECK_FAILED);
      SetErrorCode(retVal);
      MTIostringstream tmpMsg;
      tmpMsg << theError.getMessage();
      if (tmpMsg.str().empty())
         tmpMsg << "Invalid PDL Entry, Error code = " << errorCode;
      SetErrorMessage(tmpMsg.str());
      }

   return retVal;
}

void CPDLEntry::ClearStatus()
{
   // Clear status, error code and error message
   SetEntryStatus(PDL_ENTRY_STATUS_UNPROCESSED);
   SetErrorCode(0);
   SetErrorMessage("");
}

//-----------------------------------------------------------------------------

EPDLEntryStatus CPDLEntry::GetEntryStatus()
{
   return entryStatus;
}

int CPDLEntry::GetErrorCode()
{
   return errorCode;
}

string CPDLEntry::GetErrorMessage()
{
   return errorMessage;
}

bool CPDLEntry::IsModified()
{
   return hasChanged;
}

//-----------------------------------------------------------------------------

void CPDLEntry::SetEntryStatus(EPDLEntryStatus newEntryStatus)
{
   if (GetEntryStatus() != newEntryStatus)
      hasChanged = true;

   entryStatus = newEntryStatus;
}

void CPDLEntry::SetErrorCode(int newErrorCode)
{
   if (GetErrorCode() != newErrorCode)
      hasChanged = true;

   errorCode = newErrorCode;
}

void CPDLEntry::SetErrorMessage(const string& newErrorMessage)
{
   if (GetErrorMessage() != newErrorMessage)
      hasChanged = true;

   errorMessage = newErrorMessage;
}

//-----------------------------------------------------------------------------

CPDLEntry_Media* CPDLEntry::MakeNewMedia(const string &binPath,
                                         const string &clipName,
                                         int videoProxyIndex,
                                         int videoFramingIndex,
                                         int inFrameIndex, int outFrameIndex)
{
   CPDLEntry_Media *newMedia = new CPDLEntry_Media(this);

   newMedia->binPath = binPath;
   newMedia->clipName = clipName;
   newMedia->videoProxyIndex = videoProxyIndex;
   newMedia->videoFramingIndex = videoFramingIndex;
   newMedia->inFrameIndex = inFrameIndex;
   newMedia->outFrameIndex = outFrameIndex;

   return newMedia;
}

CPDLEntry_Media* CPDLEntry::MakeNewMediaDest(const string &binPath,
                                             const string &clipName,
                                             int videoProxyIndex,
                                             int videoFramingIndex,
                                             int inFrameIndex,
                                             int outFrameIndex)
{
   CPDLEntry_Media *newMedia = MakeNewMedia(binPath, clipName,
                                            videoProxyIndex, videoFramingIndex,
                                            inFrameIndex, outFrameIndex);
   dstList.push_back(newMedia);

   return newMedia;
}

CPDLEntry_Media* CPDLEntry::MakeNewMediaSource(const string &binPath,
                                               const string &clipName,
                                               int videoProxyIndex,
                                               int videoFramingIndex,
                                               int inFrameIndex,
                                               int outFrameIndex)
{
   CPDLEntry_Media *newMedia = MakeNewMedia(binPath, clipName,
                                            videoProxyIndex, videoFramingIndex,
                                            inFrameIndex, outFrameIndex);
   srcList.push_back(newMedia);

   return newMedia;
}

void CPDLEntry::SetAttributes(CPDLAttribList &attributeList)
{
   // No error checking since we're assuming the PDL file was written by
   // a machine rather than a person

   string entryStatusStr = attributeList.Find(entryStatusAttr);
   if (entryStatusStr.empty() || entryStatusStr == entryStatusUnprocessed)
      entryStatus = PDL_ENTRY_STATUS_UNPROCESSED;
   else if (entryStatusStr == entryStatusCheckFailed)
      entryStatus = PDL_ENTRY_STATUS_CHECK_FAILED;
   else if (entryStatusStr == entryStatusProcessingInProgress)
      entryStatus = PDL_ENTRY_STATUS_PROCESSING_IN_PROGRESS;
   else if (entryStatusStr == entryStatusProcessedOk)
      entryStatus = PDL_ENTRY_STATUS_PROCESSED_OK;
   else if (entryStatusStr == entryStatusProcessedFailed)
      entryStatus = PDL_ENTRY_STATUS_PROCESSED_FAILED;
   else if (entryStatusStr == entryStatusProcessedStoppedByUser)
      entryStatus = PDL_ENTRY_STATUS_PROCESSED_STOPPED_BY_USER;

   string errorCodeStr = attributeList.Find(errorCodeAttr);
   if (errorCodeStr.empty())
      errorCode = 0;
   else
      {
      MTIistringstream istrm;
      istrm.clear();
      istrm.str(errorCodeStr);
      istrm >> errorCode;
      }

   errorMessage = attributeList.Find(errorMessageAttr);
}

void CPDLEntry::writeXMLStream(CXMLStream &xmlStream)
{
   // Start tag for PDL Entry
   xmlStream << tag(CPDL::tag_PDLEntry);

   // Write entry status, error code and error message
   string attrValueStr;
   switch(entryStatus)
      {
      case PDL_ENTRY_STATUS_INVALID:
      case PDL_ENTRY_STATUS_UNPROCESSED:
      default:
         attrValueStr = entryStatusUnprocessed;
         break;
      case PDL_ENTRY_STATUS_CHECK_FAILED:
         attrValueStr = entryStatusCheckFailed;
         break;
      case PDL_ENTRY_STATUS_PROCESSING_IN_PROGRESS:
         attrValueStr = entryStatusProcessingInProgress;
         break;
      case PDL_ENTRY_STATUS_PROCESSED_OK:
         attrValueStr = entryStatusProcessedOk;
         break;
      case PDL_ENTRY_STATUS_PROCESSED_FAILED:
         attrValueStr = entryStatusProcessedFailed;
         break;
      case PDL_ENTRY_STATUS_PROCESSED_STOPPED_BY_USER:
         attrValueStr = entryStatusProcessedStoppedByUser;
         break;
      }
   xmlStream << attr(entryStatusAttr) << escape(attrValueStr);
   xmlStream << attr(errorCodeAttr) << errorCode;
   xmlStream << attr(errorMessageAttr) << escape(errorMessage);

   // Write Source List
   vector<CPDLEntry_Media*>::iterator iter;
   for (iter = srcList.begin(); iter != srcList.end(); ++iter)
      {
      xmlStream << tag(CPDL::tag_Source);
      (*iter)->writeXMLStream(xmlStream);
      xmlStream << endTag();
      }

   // Write Destination List
   for (iter = dstList.begin(); iter != dstList.end(); ++iter)
      {
      xmlStream << tag(CPDL::tag_Dest);
      (*iter)->writeXMLStream(xmlStream);
      xmlStream << endTag();
      }

   // Write Mask
   if (mask != 0)
      mask->writeXMLStream(xmlStream);

   // Write Tool List
   vector<CPDLEntry_Tool*>::iterator toolIter;
   for (toolIter = toolList.begin(); toolIter != toolList.end(); ++toolIter)
      {
      (*toolIter)->writeXMLStream(xmlStream);
      }

   xmlStream << endTag();

   // This entry has been written to a file, so we'll call it unchanged
   hasChanged = false;
}

void CPDLEntry::dump(ostream& ostrm, string &indentLevel)
{
   ostrm << indentLevel << "PDLEntry" << std::endl;

   string localIndentLevel1 = indentLevel + " .";
   string localIndentLevel2 = localIndentLevel1 + " ";

   // Dump the source list
   vector<CPDLEntry_Media*>::iterator iter;
   for (iter = srcList.begin(); iter != srcList.end(); ++iter)
      {
      ostrm << localIndentLevel1 << "Source" << std::endl;
      (*iter)->dump(ostrm, localIndentLevel2);
      }

   // Dump the destination list
   for (iter = dstList.begin(); iter != dstList.end(); ++iter)
      {
      ostrm << localIndentLevel1 << "Destination" << std::endl;
      (*iter)->dump(ostrm, localIndentLevel2);
      }

   // Dump the Mask
   if (mask != 0)
      mask->dump(ostrm, localIndentLevel1);

   // Dump the tool list
   vector<CPDLEntry_Tool*>::iterator toolIter;
   for (toolIter = toolList.begin(); toolIter != toolList.end(); ++toolIter)
      {
      (*toolIter)->dump(ostrm, localIndentLevel1);
      }
}

// ---------------------------------------------------------------------------

CPDLAttribList::CPDLAttribList()
{
}

CPDLAttribList::CPDLAttribList(const CPDLAttribList &src)
{
   vector<CPDLAttrib*>::const_iterator iter;
   for (iter = src.attributeList.begin(); iter != src.attributeList.end(); ++iter)
      {
      attributeList.push_back(new CPDLAttrib(*(*iter)));
      }
}

CPDLAttribList::~CPDLAttribList()
{
   Clear();
}

CPDLAttribList& CPDLAttribList::operator=(const CPDLAttribList &src)
{
   if (this == &src)  // check for self-copy
      return *this;

   Clear();  // clear existing attribute list

   vector<CPDLAttrib*>::const_iterator iter;
   for (iter = src.attributeList.begin();
        iter != src.attributeList.end();
        ++iter)
      {
      attributeList.push_back(new CPDLAttrib(*(*iter)));
      }

   return *this;
}

void CPDLAttribList::Clear()
{
   vector<CPDLAttrib*>::iterator iter;
   for (iter = attributeList.begin(); iter != attributeList.end(); ++iter)
      {
      delete *iter;
      }

   attributeList.clear();
}

CPDLAttrib* CPDLAttribList::Add(const string &newName, const string &newValue)
{
   CPDLAttrib *newAttrib = new CPDLAttrib(newName, newValue);
   attributeList.push_back(newAttrib);

   return newAttrib;
}

bool CPDLAttribList::DoesNameExist(const string &attribName)
{
   vector<CPDLAttrib*>::iterator iter;
   for (iter = attributeList.begin(); iter != attributeList.end(); ++iter)
      {
      if ((*iter)->name == attribName)
         return true; // yeah, found it!
      }

   return false;   // nothing found, return false
}

string CPDLAttribList::Find(const string &attribName)
{
   vector<CPDLAttrib*>::iterator iter;
   for (iter = attributeList.begin(); iter != attributeList.end(); ++iter)
      {
      if ((*iter)->name == attribName)
         return (*iter)->value;
      }

   return ("");   // empty string if attribName was not found
}

CPDLAttrib* CPDLAttribList::FindAttrib(const string &attribName)
{
   vector<CPDLAttrib*>::iterator iter;
   for (iter = attributeList.begin(); iter != attributeList.end(); ++iter)
      {
      if ((*iter)->name == attribName)    // case sensitive
         return (*iter); // found it!
      }

   return 0;   // boo hoo, didn't find nuthin
}

unsigned int CPDLAttribList::GetCount()
{
   return attributeList.size();
}

CPDLAttrib* CPDLAttribList::GetAttrib(unsigned int index)
{
   if (index >= GetCount())
      return 0;

   return attributeList[index];
}

string CPDLAttribList::GetValueString(const string &attribName)
{
   CPDLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      return "";  // didn't find the attribute with the given name

   return attrib->GetValueString();
}

string CPDLAttribList::GetValueString(const string &attribName,
                                      const string &defaultValue)
{
   CPDLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      return defaultValue;  // didn't find the attribute with the given name

   return attrib->GetValueString();
}

void CPDLAttribList::GetIntegerList(const string &attribName,
                                    vector<int> &intList)
{
   intList.clear();

   CPDLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      return;  // didn't find the attribute with the given name

   attrib->GetIntegerList(intList);
}

MTI_INT64 CPDLAttribList::GetValueInteger(const string &attribName)
{
   CPDLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      return 0;  // didn't find the attribute with the given name

   return attrib->GetValueInteger();
}

MTI_INT64 CPDLAttribList::GetValueInteger(const string &attribName,
                                          MTI_INT64 defaultValue)
{
   CPDLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      return defaultValue;  // didn't find the attribute with the given name

   string valueString = StringTrimLeft(attrib->GetValueString());
   MTIistringstream istrm;
   istrm.str(valueString);
   if (valueString[0] == '0' && (valueString[1] == 'x' || valueString[1] == 'X'))
      istrm.unsetf(std::ios::dec);     // let base of value be determined by the
                                       // leading characters, in this case
                                       // leading 0x or 0X means Hex.

   MTI_INT64 value;
   if (istrm >> value)
      return value;
   else
      return defaultValue; // conversion failed, used default value
}

MTI_UINT64 CPDLAttribList::GetValueUnsignedInteger(const string &attribName)
{
   CPDLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      return 0;  // didn't find the attribute with the given name

   return attrib->GetValueUnsignedInteger();
}

MTI_UINT64 CPDLAttribList::GetValueUnsignedInteger(const string &attribName,
                                                   MTI_UINT64 defaultValue)
{
   CPDLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      return defaultValue;  // didn't find the attribute with the given name

   string valueString = StringTrimLeft(attrib->GetValueString());
   MTIistringstream istrm;
   istrm.str(valueString);
   if (valueString[0] == '0' && (valueString[1] == 'x' || valueString[1] == 'X'))
      istrm.unsetf(std::ios::dec);     // let base of value be determined by the
                                       // leading characters, in this case
                                       // leading 0x or 0X means Hex.

   MTI_UINT64 value;
   if (istrm >> value)
      return value;
   else
      return defaultValue; // conversion failed, used default value
}

bool CPDLAttribList::GetValueBool(const string &attribName)
{
   CPDLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      return 0;  // didn't find the attribute with the given name

   return attrib->GetValueBool();
}

bool CPDLAttribList::GetValueBool(const string &attribName, bool defaultValue)
{
   CPDLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      return defaultValue;  // didn't find the attribute with the given name

   return attrib->GetValueBool();
}

double CPDLAttribList::GetValueDouble(const string &attribName)
{
   CPDLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      return 0.0;  // didn't find the attribute with the given name

   return attrib->GetValueDouble();
}

double CPDLAttribList::GetValueDouble(const string &attribName,
                                    double defaultValue)
{
   CPDLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      return defaultValue;  // didn't find the attribute with the given name

   return attrib->GetValueDouble();
}

void CPDLAttribList::SetValueString(const string &attribName,
                                    const string &newValue)
{
   CPDLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      Add(attribName, newValue);
   else
      attrib->SetValueString(newValue);
}

void CPDLAttribList::SetValueInteger(const string &attribName,
                                     MTI_INT64 newValue)
{
   string emptyStr;
   CPDLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      attrib = Add(attribName, emptyStr);

   attrib->SetValueInteger(newValue);
}

void CPDLAttribList::SetValueUnsignedInteger(const string &attribName,
                                             MTI_UINT64 newValue)
{
   string emptyStr;
   CPDLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      attrib = Add(attribName, emptyStr);

   attrib->SetValueUnsignedInteger(newValue);
}

void CPDLAttribList::SetValueBool(const string &attribName,
                                  bool newValue)
{
   string emptyStr;
   CPDLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      attrib = Add(attribName, emptyStr);

   attrib->SetValueBool(newValue);
}

void CPDLAttribList::SetValueDouble(const string &attribName,
                                    double newValue)
{
   string emptyStr;
   CPDLAttrib *attrib = FindAttrib(attribName);
   if(attrib == 0)
      attrib = Add(attribName, emptyStr);

   attrib->SetValueDouble(newValue);
}

void CPDLAttribList::writeXMLStream(CXMLStream &xmlStream)
{
   if (!attributeList.empty())
      {
      for (vector<CPDLAttrib*>::iterator iter = attributeList.begin();
           iter!= attributeList.end();
           ++iter)
         {
         (*iter)->writeXMLStream(xmlStream);
         }
      }
}

void CPDLAttribList::dump(ostream& ostrm, string &indentLevel)
{
   if (!attributeList.empty())
      {
      for (vector<CPDLAttrib*>::iterator iter = attributeList.begin();
           iter!= attributeList.end();
           ++iter)
         {
         (*iter)->dump(ostrm, indentLevel);
         }
      }
}

// ---------------------------------------------------------------------------

CXMLElement::CXMLElement(CXMLElement *newParent)
 : parent(newParent)
{
}

// ---------------------------------------------------------------------------
//
//    CPDLEntry_Media class
//
// ---------------------------------------------------------------------------

CPDLEntry_Media::CPDLEntry_Media(CXMLElement *newParent)
 : CXMLElement(newParent),
   videoProxyIndex(0), videoFramingIndex(0),
   inFrameIndex(-1), outFrameIndex(-1), cachedClip(nullptr)
{
}

CPDLEntry_Media::CPDLEntry_Media(const CPDLEntry_Media &src)
 : CXMLElement(0)
{
   copyContent(src);
}

CPDLEntry_Media::~CPDLEntry_Media()
{
   if (cachedClip)
   {
      CBinManager binMgr;
      binMgr.closeClip(cachedClip);
      cachedClip = nullptr;
   }
}

CPDLEntry_Media& CPDLEntry_Media::operator=(const CPDLEntry_Media &src)
{
   if (this == &src)   // avoid self-copy
      return *this;

   copyContent(src);

   return *this;
}

void CPDLEntry_Media::copyContent(const CPDLEntry_Media &src)
{
   binPath = src.binPath;
   clipName = src.clipName;
   videoProxyIndex = src.videoProxyIndex;
   videoFramingIndex = src.videoFramingIndex;
   inFrameIndex = src.inFrameIndex;
   outFrameIndex = src.outFrameIndex;

   // Don't copy the cached clip, let the new instance open it itself.
   cachedClip = ClipSharedPtr();
}

ClipSharedPtr CPDLEntry_Media::GetClip()
{
   if (cachedClip == nullptr)
   {
      if (binPath.empty() || clipName.empty() || inFrameIndex < 0)
      {
         return cachedClip;
      }

      CBinManager binMgr;
      int retVal;
      cachedClip = binMgr.openClip(binPath, clipName, &retVal);
      if (retVal != 0)
      {
         cachedClip = nullptr;
      }
   }

   return cachedClip;
}
\
string CPDLEntry_Media::GetClipName()
{
   if (binPath.empty() || clipName.empty() || inFrameIndex < 0)
   {
      return "";
   }

   return AddDirSeparator(binPath) + clipName;
}

const CImageFormat* CPDLEntry_Media::GetImageFormat()
{
   auto clip = GetClip();
   if (clip == nullptr)
      return 0;

   return clip->getImageFormat(videoProxyIndex);
}

int CPDLEntry_Media::GetInTimecode(CTimecode &timecode)
{
   CVideoFrameList *frameList = GetVideoFrameList();
   if (frameList == 0)
      return -1;  // replace with theError's error code

   // In frame index must be within frame list's range
   if (inFrameIndex < frameList->getInFrameIndex()
       || inFrameIndex >= frameList->getOutFrameIndex())
      return -2;  // TBD: replace with proper error code

   timecode = frameList->getTimecodeForFrameIndex(inFrameIndex);

   return 0;
}

CVideoField* CPDLEntry_Media::GetInVideoField()
{
   CVideoFrameList *frameList = GetVideoFrameList();
   if (frameList == 0)
      return 0;

   CVideoFrame *frame = frameList->getFrame(inFrameIndex);
   if (frame == 0)
      return 0;

   return frame->getField(0);
}

int CPDLEntry_Media::GetOutTimecode(CTimecode &timecode)
{
   CVideoFrameList *frameList = GetVideoFrameList();
   if (frameList == 0)
      return -1;  // TBD: replace with theError's error code

   // Out frame index must be within frame list's range
   if (outFrameIndex <= frameList->getInFrameIndex()
       || outFrameIndex > frameList->getOutFrameIndex())
      return -2;  // TBD: replace with proper error code

   if (outFrameIndex == frameList->getOutFrameIndex())
      timecode = frameList->getOutTimecode();
   else
      timecode = frameList->getTimecodeForFrameIndex(outFrameIndex);

   return 0;
}

CVideoFrameList* CPDLEntry_Media::GetVideoFrameList()
{
   auto clip = GetClip();
   if (clip == nullptr)
      return 0;

   return clip->getVideoFrameList(videoProxyIndex, videoFramingIndex);
}

// ---------------------------------------------------------------------------

int CPDLEntry_Media::CheckMedia()
{
   // Check the media description to make sure it is valid.
   // The items checked are:
   //  1. Does clip exist
   //  2. Can clip be opened
   //  3. Are proxy and framing indices valid
   //  4. Are in and out frame indices valid
   //
   // Returns 0 if all okay.  Returns error code otherwise along with
   // descriptive message in theError

   int retVal;
   MTIostringstream ostrm;

   if (binPath.empty() || clipName.empty())
      {
      // ERROR: Missing bin or clip name
      theError.set(PDL_ERROR_MISSING_BIN_OR_CLIP_NAME,
                   "Missing Bin or Clip Name");
      return PDL_ERROR_MISSING_BIN_OR_CLIP_NAME;
      }

   CBinManager binMgr;
   if (!binMgr.doesClipExist(binPath, clipName))
      {
      // ERROR: Clip does not exist
      theError.set(CLIP_ERROR_CLIP_FILE_DOES_NOT_EXIST,
                   "Clip does not exist");
      return CLIP_ERROR_CLIP_FILE_DOES_NOT_EXIST;
      }

   auto clip = binMgr.openClip(binPath, clipName, &retVal);
   if (retVal != 0)
      {
      // ERROR: Could not open the clip
      ostrm << "Could not open clip, error code " << retVal;
      theError.set(retVal, ostrm);
      return retVal;
      }

   CVideoProxy *videoProxy = clip->getVideoProxy(videoProxyIndex);
   if (videoProxy == 0)
      {
      // ERROR: Invalid video proxy index
      ostrm << "Invalid Video Proxy Index (" << videoProxyIndex << ")";
      theError.set(CLIP_ERROR_INVALID_VIDEO_PROXY_INDEX, ostrm);
      binMgr.closeClip(clip);
      return CLIP_ERROR_INVALID_VIDEO_PROXY_INDEX;
      }

   CVideoFrameList *frameList
                  = clip->getVideoFrameList(videoProxyIndex, videoFramingIndex);
   if (frameList == 0)
      {
      // ERROR: Invalid framing index
      ostrm << "Invalid Framing Index (" << videoFramingIndex << ")";
      theError.set(CLIP_ERROR_INVALID_FRAMING_INDEX, ostrm);
      binMgr.closeClip(clip);
      return CLIP_ERROR_INVALID_FRAMING_INDEX;
      }

   CTimecode tc(0);  // scratch timecode
   retVal = GetInTimecode(tc);
   if (retVal != 0)
      {
      // ERROR: Invalid in frame index
      ostrm << "Invalid In frame index (" << inFrameIndex << ")";
      theError.set(PDL_ERROR_INVALID_IN_FRAME_INDEX, ostrm);
      binMgr.closeClip(clip);
      return PDL_ERROR_INVALID_IN_FRAME_INDEX;
      }

   retVal = GetOutTimecode(tc);
   if (retVal != 0 || inFrameIndex >= outFrameIndex)
      {
      // ERROR: Invalid out frame index
      ostrm << "Invalid Out frame index (" << outFrameIndex << ")";
      theError.set(PDL_ERROR_INVALID_OUT_FRAME_INDEX, ostrm);
      binMgr.closeClip(clip);
      return PDL_ERROR_INVALID_OUT_FRAME_INDEX;
      }

   binMgr.closeClip(clip);
   return 0;

} // CheckMedia()

// ---------------------------------------------------------------------------

void CPDLEntry_Media::SetAttributes(CPDLAttribList &attributeList)
{
   // No error checking since we're assuming the PDL file was written by
   // a machine rather than a person

   binPath = attributeList.Find(binPathAttr);
   clipName = attributeList.Find(clipNameAttr);

   MTIistringstream istrm;

   istrm.clear();
   istrm.str(attributeList.Find(videoProxyAttr));
   istrm >> videoProxyIndex;

   istrm.clear();
   istrm.str(attributeList.Find(videoFramingAttr));
   istrm >> videoFramingIndex;

   istrm.clear();
   istrm.str(attributeList.Find(inFrameIndexAttr));
   istrm >> inFrameIndex;

   istrm.clear();
   istrm.str(attributeList.Find(outFrameIndexAttr));
   istrm >> outFrameIndex;
}

void CPDLEntry_Media::writeXMLStream(CXMLStream &xmlStream)
{
   xmlStream << attr(binPathAttr) << escape(binPath);
   xmlStream << attr(clipNameAttr) << escape(clipName);
   xmlStream << attr(videoProxyAttr) << videoProxyIndex;
   xmlStream << attr(videoFramingAttr) << videoFramingIndex;
   xmlStream << attr(inFrameIndexAttr) << inFrameIndex;
   xmlStream << attr(outFrameIndexAttr) << outFrameIndex;
}

void CPDLEntry_Media::dump(ostream& ostrm, string &indentLevel)
{
   ostrm << indentLevel << "BinPath:         " << binPath << std::endl;
   ostrm << indentLevel << "ClipName:        " << clipName << std::endl;
   ostrm << indentLevel << "Vid Proxy:       " << videoProxyIndex << std::endl;
   ostrm << indentLevel << "Vid Framing:     " << videoFramingIndex << std::endl;
   ostrm << indentLevel << "In Frame Index:  " << inFrameIndex << std::endl;
   ostrm << indentLevel << "Out Frame Index: " << outFrameIndex << std::endl;
}

// ---------------------------------------------------------------------------
//
//    CPDLEntry_Mask class
//
// ---------------------------------------------------------------------------

CPDLEntry_Mask::CPDLEntry_Mask(CXMLElement *newParent)
 : CXMLElement(newParent),
   maskEnabled(false),
#ifndef USE_PER_REGION_INCLUSION_FLAGS
   globalInclusionMode(USE_FLAGS),
#endif
   maskRegionList(0),
   animatedMaskTimelineList(0),
   currentElement(0)
{
}

CPDLEntry_Mask::CPDLEntry_Mask(const CPDLEntry_Mask &src)
 : CXMLElement(0),
   maskEnabled(false),
#ifndef USE_PER_REGION_INCLUSION_FLAGS
   globalInclusionMode(USE_FLAGS),
#endif
   maskRegionList(0),
   animatedMaskTimelineList(0),
   currentElement(0)
{
   copyContents(src);
}

CPDLEntry_Mask::~CPDLEntry_Mask()
{
   clear();
}

CPDLEntry_Mask& CPDLEntry_Mask::operator=(const CPDLEntry_Mask &src)
{
   if (this == &src)    // avoid self-copy
      return *this;

   clear();

   copyContents(src);

   return *this;
}

void CPDLEntry_Mask::clear()
{
   // Delete the tree of parameters
   delete maskRegionList;
   maskRegionList = 0;

   delete animatedMaskTimelineList;
   animatedMaskTimelineList = 0;

   maskEnabled = false;
#ifndef USE_PER_REGION_INCLUSION_FLAGS
   globalInclusionMode = USE_FLAGS;
#endif
   currentElement = 0;
   parent = 0;
}

void CPDLEntry_Mask::copyContents(const CPDLEntry_Mask &src)
{
   maskEnabled = src.maskEnabled;
#ifndef USE_PER_REGION_INCLUSION_FLAGS
   globalInclusionMode = src.globalInclusionMode;
#endif

   if (src.maskRegionList != 0)
      maskRegionList = new CPDLElement(*(src.maskRegionList));

   if (src.animatedMaskTimelineList != 0)
      animatedMaskTimelineList = new CPDLElement(*(src.animatedMaskTimelineList));
}

// ----------------------------------------------------------------------------
// Parsing Mask Entry

CXMLElement* CPDLEntry_Mask::startElement(const string &elementName,
                                          CPDLAttribList &attributeList)
{
   // Handle random mask region list by putting them into a tree
   // of CPDLElement instances

   // create a new PDL element
   CPDLElement *newElement = new CPDLElement;
   newElement->name = elementName;
   newElement->parent = currentElement;
   if (currentElement == 0)
      {
      if (maskRegionList == 0 && elementName == tag_MaskRegionList)
         maskRegionList = newElement;
      else if (animatedMaskTimelineList == 0 &&
               elementName == tag_AnimatedMaskTimelineList)
         animatedMaskTimelineList = newElement;
      }
   else
      {
      currentElement->childList.push_back(newElement);
      }

   currentElement = newElement;

   newElement->SetAttributes(attributeList);

   return 0;
}

bool CPDLEntry_Mask::endElement()
{
   bool done;
   if (currentElement != 0)
      {
      // Pop up a level in our tree of PDL elements
      currentElement = currentElement->parent;
      done = false;
      }
   else
      {
      done = true;
      }

   return (done);   // return done if at root
}

void CPDLEntry_Mask::SetAttributes(CPDLAttribList &attributeList)
{
   // No error checking since we're assuming the PDL file was written by
   // a machine rather than a person

   maskEnabled = attributeList.GetValueBool(enabledAttr);

#ifndef USE_PER_REGION_INCLUSION_FLAGS
   string modeString = attributeList.GetValueString(inclusionModeAttr, "flags");
   transform(modeString.begin(), modeString.end(), modeString.begin(),
                                 (int(*)(int)) tolower);


   if (modeString == "inclusive")
   {
      globalInclusionMode = ALL_INCLUSIVE;
   }
   else if (modeString == "exclusive")
   {
      globalInclusionMode = ALL_EXCLUSIVE;
   }
   else
   {
      globalInclusionMode = USE_FLAGS;
   }
#endif
}

// ---------------------------------------------------------------------------

void CPDLEntry_Mask::writeXMLStream(CXMLStream &xmlStream)
{
   // Start Tool element
   xmlStream << tag(CPDL::tag_Mask);

   string value = (maskEnabled) ? "true" : "false";
   xmlStream << attr(enabledAttr) << value;

#ifndef USE_PER_REGION_INCLUSION_FLAGS
   value = "";
   if (globalInclusionMode == ALL_INCLUSIVE)
   {
      value = "inclusive";
   }
   else if (globalInclusionMode == ALL_EXCLUSIVE)
   {
      value = "exclusive";
   }
   if (!value.empty())
   {
      xmlStream << attr(inclusionModeAttr) << value;
   }
#endif

   // Write mask regions elements
   if (maskRegionList != 0)
      maskRegionList->writeXMLStream(xmlStream);

   if (animatedMaskTimelineList != 0)
      animatedMaskTimelineList->writeXMLStream(xmlStream);

   xmlStream << endTag();   // End of Tool element
}

// ---------------------------------------------------------------------------

void CPDLEntry_Mask::dump(ostream& ostrm, string &indentLevel)
{
   string value = (maskEnabled) ? "true" : "false";
   ostrm << indentLevel << CPDL::tag_Mask << ": " << value << std::endl;

#ifndef USE_PER_REGION_INCLUSION_FLAGS
   value = "";
   if (globalInclusionMode == ALL_INCLUSIVE)
   {
      value = "inclusive";
   }
   else if (globalInclusionMode == ALL_EXCLUSIVE)
   {
      value = "exclusive";
   }
   if (!value.empty())
   {
      ostrm << indentLevel << CPDL::tag_Mask << ": " << value << std::endl;
   }
#endif

   string localIndentLevel = indentLevel + " ";
   maskRegionList->dump(ostrm, localIndentLevel);
}

// ---------------------------------------------------------------------------

CPDLElement* CPDLEntry_Mask::NewMaskRegionList()
{
   maskRegionList = new CPDLElement;
   maskRegionList->name = tag_MaskRegionList;

   return maskRegionList;
}

CPDLElement* CPDLEntry_Mask::NewAnimatedMaskTimelineList()
{
   animatedMaskTimelineList = new CPDLElement;
   animatedMaskTimelineList->name = tag_AnimatedMaskTimelineList;

   return animatedMaskTimelineList;
}

void CPDLEntry_Mask::AddBezier(bool inclusionFlag, bool hiddenFlag,
                              int borderWidth,
                               double blendSlew,
                               BEZIER_POINT *bezier)
{
   CPDLElement *newRegion = AddShape(shapeTypeBezier,
                                     inclusionFlag, hiddenFlag,
                                     borderWidth, blendSlew);

   MTIostringstream ostrm;

   // Create a comman-separated string of all of the bezier points by
   // looping over all of the points in the bezier curve, stopping after the
   // last point matches the first point
   for (int i = 0; ; ++i)
      {
      if (i > 0)
         ostrm << ", ";  // comma between bezier points

      ostrm << bezier[i].xctrlpre << "," << bezier[i].yctrlpre << ","
            << bezier[i].x << "," << bezier[i].y << ","
            << bezier[i].xctrlnxt << "," << bezier[i].yctrlnxt;

      if (i > 0 &&
          bezier[i].xctrlpre == bezier[0].xctrlpre &&
          bezier[i].yctrlpre == bezier[0].yctrlpre &&
          bezier[i].x == bezier[0].x &&
          bezier[i].y == bezier[0].y &&
          bezier[i].xctrlnxt == bezier[0].xctrlnxt &&
          bezier[i].yctrlnxt == bezier[0].yctrlnxt)
         break;
      }

   newRegion->SetAttribString(pointsAttr, ostrm.str());
}

void CPDLEntry_Mask::AddLasso(bool inclusionFlag, bool hiddenFlag,
                              int borderWidth,
                              double blendSlew, POINT *lassoPoints)
{
   CPDLElement *newRegion = AddShape(shapeTypeLasso, inclusionFlag, hiddenFlag,
                                     borderWidth, blendSlew);

   MTIostringstream ostrm;

   // Create a comman-separated string of all of the lasso points by
   // looping over all of the points in the lasso, stopping after the last
   // point matches the first point
   for (int i = 0; ; ++i)
      {
      if (i > 0)
         ostrm << ", ";  // comma between each point

      ostrm << lassoPoints[i].x << "," << lassoPoints[i].y;

      if (i > 0 &&
          lassoPoints[i].x == lassoPoints[0].x &&
          lassoPoints[i].y == lassoPoints[0].y)
         break;
      }

   newRegion->SetAttribString(pointsAttr, ostrm.str());

}

void CPDLEntry_Mask::AddRect(bool inclusionFlag, bool hiddenFlag,
                             int borderWidth,
                             double blendSlew, RECT &rect)
{
   CPDLElement *newRegion = AddShape(shapeTypeRect, inclusionFlag, hiddenFlag,
                                     borderWidth, blendSlew);

   MTIostringstream ostrm;

   ostrm << rect.left << "," << rect.top << ","
         << rect.right << "," << rect.bottom;

   newRegion->SetAttribString(pointsAttr, ostrm.str());
}

CPDLElement* CPDLEntry_Mask::AddShape(const string &shapeType,
                                      bool inclusionFlag,
                                      bool hiddenFlag,
                                      int borderWidth,
                                      double blendSlew)
{
   CPDLElement *newRegion = maskRegionList->MakeNewChild(tag_MaskRegion);

   newRegion->SetAttribString(shapeTypeAttr, shapeType);
#ifdef USE_PER_REGION_INCLUSION_FLAGS
   newRegion->SetAttribBool(inclusionFlagAttr, inclusionFlag);
#endif
   newRegion->SetAttribBool(hiddenFlagAttr, hiddenFlag);
   newRegion->SetAttribInteger(borderWidthAttr, borderWidth);
   newRegion->SetAttribDouble(blendSlewAttr, blendSlew);

   return newRegion;
}

void CPDLEntry_Mask::SetMaskEnabled(bool newFlag)
{
   maskEnabled = newFlag;
}

void CPDLEntry_Mask::SetGlobalInclusionMode(EGlobalInclusionMode newMode)
{
   globalInclusionMode = newMode;
}

bool CPDLEntry_Mask::GetMaskEnabled()
{
   return maskEnabled;
}

EGlobalInclusionMode CPDLEntry_Mask::GetGlobalInclusionMode()
{
   return globalInclusionMode;
}

// ---------------------------------------------------------------------------
//
//      CPDLEntry_Tool class
//
// ---------------------------------------------------------------------------

CPDLEntry_Tool::CPDLEntry_Tool(CXMLElement *newParent)
 : CXMLElement(newParent),
   inputPortCount(1), outputPortCount(1),
   parameters(0), currentElement(0)
{
}

CPDLEntry_Tool::CPDLEntry_Tool(const CPDLEntry_Tool &src)
 : CXMLElement(0),
   parameters(0), currentElement(0)
{
   copyContents(src);
}

CPDLEntry_Tool::~CPDLEntry_Tool()
{
   clear();
}

CPDLEntry_Tool& CPDLEntry_Tool::operator=(const CPDLEntry_Tool &src)
{
   if (this == &src)    // avoid self-copy
      return *this;

   clear();

   copyContents(src);

   return *this;
}

void CPDLEntry_Tool::clear()
{
   // Delete the tree of parameters
   delete parameters;
   parameters = 0;

   currentElement = 0;
   parent = 0;
}

void CPDLEntry_Tool::copyContents(const CPDLEntry_Tool &src)
{
   toolName = src.toolName;
   inputPortCount = src.inputPortCount;
   outputPortCount = src.outputPortCount;

   if (src.parameters != 0)
      parameters = new CPDLElement(*(src.parameters));
}

CXMLElement* CPDLEntry_Tool::startElement(const string &elementName,
                                          CPDLAttribList &attributeList)
{
   // Handle random tool parameters by putting them into a tree
   // of CPDLElement instances

   // create a new PDL element
   CPDLElement *newElement = new CPDLElement;
   newElement->name = elementName;
   newElement->parent = currentElement;
   if (currentElement == 0)
      {
      if (parameters == 0)
         parameters = newElement;
      }
   else
      {
      currentElement->childList.push_back(newElement);
      }

   currentElement = newElement;

   newElement->SetAttributes(attributeList);

   return 0;
}

void CPDLEntry_Tool::characters(const string &chars)
{
   if (currentElement != 0)
      {
      currentElement->AppendCdata(chars);
      }
}

bool CPDLEntry_Tool::endElement()
{
   bool done;
   if (currentElement != 0)
      {
      // Pop up a level in our tree of PDL elements
      currentElement = currentElement->parent;
      done = false;
      }
   else
      done = true;

   return (done);   // return done if at root
}

void CPDLEntry_Tool::SetAttributes(CPDLAttribList &attributeList)
{
   // No error checking since we're assuming the PDL file was written by
   // a machine rather than a person

   toolName = attributeList.Find(toolNameAttr);
}

void CPDLEntry_Tool::writeXMLStream(CXMLStream &xmlStream)
{
//   DBTIMER(CPDLEntry_Tool_writeXMLStream);

   // Start Tool element
   xmlStream << tag(CPDL::tag_Tool);

   xmlStream << attr(toolNameAttr) << escape(toolName);

   // Start ToolParameters element
   parameters->writeXMLStream(xmlStream);

   xmlStream << endTag();   // End of Tool element
}

void CPDLEntry_Tool::dump(ostream& ostrm, string &indentLevel)
{
   ostrm << indentLevel << "Tool: " << toolName << std::endl;

   string localIndentLevel = indentLevel + " ";
   parameters->dump(ostrm, localIndentLevel);
}

// ---------------------------------------------------------------------------
//
//    CPDL class
//
// ---------------------------------------------------------------------------

CPDL::CPDL(CXMLElement *newParent)
 : CXMLElement(newParent), hasChanged(false)
{
}

CPDL::~CPDL()
{
   Clear();
}

CPDL* CPDL::MakeNewPDL()
{
   // Static Factory Method
   // (use DestroyPDL() to delete CPDL instances returned by this function

   return new CPDL(0);
}

void CPDL::DestroyPDL(CPDL *pdl)
{
   // Static Anti-Factory Method
   delete pdl;
}

void CPDL::Clear()
{
   PDLEntry entryIter;
   for (entryIter = entryList.begin(); entryIter != entryList.end(); ++entryIter)
      {
      delete *entryIter;
      }

   entryList.clear();
}

// XML Parsing functions

bool CPDL::endElement()
{
   return true;
}

CXMLElement* CPDL::startElement(const string &elementName,
                                CPDLAttribList &attributeList)
{
   CPDLEntry *newPDLEntry;

   if (elementName == "PDLEntry")
      {
      // We've found the start of a new PDL Entry
      CPDLEntry *newPDLEntry = new CPDLEntry(this);
      newPDLEntry->SetAttributes(attributeList);
      entryList.push_back(newPDLEntry);
      return newPDLEntry;
      }
   else
      {
      TRACE_0(errout << "ERROR: CPDL::startElement - PDL parsing confused by "
                     << elementName << std::endl);
      return 0;
      }
}

bool CPDL::Empty() const
{
   return entryList.empty();
}

int CPDL::GetEntryCount() const
{
   return entryList.size();
}

CPDLEntry* CPDL::GetPDLEntry(PDLEntry pdlEntry)
{
   return *pdlEntry;
}

bool CPDL::IsModified()
{
   if(hasChanged)
      return true;

   PDLEntry entryIter;
   for (entryIter = entryList.begin(); entryIter != entryList.end(); ++entryIter)
      {
      if ((*entryIter)->IsModified())
         {
         hasChanged = true;
         return true;
         }
      }

   return false;
}

//----------------------------------------------------------------------------
// Iterator functions for PDL Rendering.  Call InitPDLEntryIterator to
// intialize the internal private iterator to this PDL's first Entry.
// Call GetNextPDLEntry to get pointers to successive PDL Entries.
// Call GetCurrentPDLEntry to get a pointer to the current PDL entry.
// Both functions return NULL pointers when there are no more PDL Entries
// available.
// Warnings:
//    1. Never call GetNextPDLEntry or GetCurrentPDLEntry before calling
//       InitPDLEntryIterator as the private iterator may not be initialized.
//    2. These functions are for use by the PDL Rendering Engine only
//       and should not be used by other code.
//    3. These functions are not thread-safe.
//    4. The CPDLEntry instances returned by these functions are owned
//       by this CPDL and should not be deleted or modified by the caller.

void CPDL::InitPDLEntryIterator()
{
   // No good if the PDL Entry list is empty
   if (Empty())
      pdlRenderingIterator = entryList.end();
   else
      pdlRenderingIterator = entryList.begin();
}

CPDLEntry* CPDL::GetNextPDLEntry()
{
   // Return NULL pointer if the PDL Entry list is empty
   if (Empty())
      return 0;

   // Return NULL pointer if the iterator is already pointing at the
   // end of the PDL Entry list.
   if (pdlRenderingIterator == entryList.end())
      return 0;

   CPDLEntry *pdlEntry = *pdlRenderingIterator;

   // Increment the iterator to the next entry
   ++pdlRenderingIterator;

   return pdlEntry;    // retuyrn pointer to next CPDLEntry
}

//-----------------------------------------------------------------------------

void CPDL::AddEntry(CPDLEntry *newEntry)
{
   if (newEntry == 0)
      return;

   entryList.push_back(newEntry);

   hasChanged = true;
}

void CPDL::DeleteEntry(PDLEntry pdlEntry)
{
   entryList.erase(pdlEntry);

   hasChanged = true;
}

void CPDL::MoveEntry(PDLEntry pdlEntry, CPDL &destPDL)
{
   // Move a PDL Entry from this PDL to the destination PDL

   CPDLEntry *entry = *pdlEntry;
   entry->parent = 0;   // Parent CXMLElement no longer relevant

   // Remove the entry from this PDL's entry list
   entryList.erase(pdlEntry);

   hasChanged = true;

   // Append the CPDLEntry to the end of the destination PDL's entry list
   destPDL.entryList.push_back(entry);
}

CPDL::PDLEntry CPDL::AppendEntry(PDLEntry srcEntry)
{
   // Copy the source entry to the end of this PDL

   CPDLEntry *newEntry = new CPDLEntry(*(*srcEntry));
   AddEntry(newEntry);

   PDLEntry newPos = entryList.end();
   return --newPos;
}

void CPDL::AppendPDL(CPDL &srcPDL)
{
   // Copy all of the entries from the source PDL and append to the
   // end of this PDL

   for (PDLEntry iter = srcPDL.entryList.begin();
        iter != srcPDL.entryList.end();
        ++iter)
      {
      AppendEntry(iter);
      }
}

CPDL::PDLEntry CPDL::InsertEntry(PDLEntry srcEntry, PDLEntry destPosition)
{
   // Copy the source entry and insert at the destination position in this PDL
   // Returns the position of the inserted entry

   CPDLEntry *newEntry = new CPDLEntry(*(*srcEntry));
   PDLEntry pos = entryList.insert(destPosition, newEntry);

   hasChanged = true;

   return pos;
}

void CPDL::InsertPDL(CPDL &srcPDL, PDLEntry destPosition)
{
   // Copy all of the entries in the source PDL and insert
   // at the destination position in this PDL

   for (PDLEntry iter = srcPDL.entryList.begin();
        iter != srcPDL.entryList.end();
        ++iter)
      {
      InsertEntry(iter, destPosition);
      }
}

CPDLEntry* CPDL::MakeNewEntry()
{
   CPDLEntry *pdlEntry = new CPDLEntry(this);
   AddEntry(pdlEntry);
   return pdlEntry;
}

void CPDL::ReplaceEntry(CPDL &srcPDL, PDLEntry destPosition)
{
   // Replace the PDL Entry at destPosition with the first PDL Entry
   // from srcPDL

   // If the destPosition is not in this PDL, it will either generate
   // an exception or replace the entry in the wrong PDL.

   if (srcPDL.Empty())
      return;

   CPDLEntry *previousEntry = *destPosition;
   *destPosition = new CPDLEntry(*(*(srcPDL.entryList.begin())));
   delete previousEntry;

   hasChanged = true;
}

//-----------------------------------------------------------------------------

void CPDL::writeXML(ostream &ostrm)
{
//   DBTIMER(CPDL__writeXML);

   CXMLStream xmlStream(ostrm);

   writeXMLStream(xmlStream);
}

void CPDL::writeXMLStream(CXMLStream &xmlStream)
{
   // Write XML declaration
   xmlStream << prolog();

   // Write PDL Start Tag
   xmlStream << tag(tag_PDL);

   // Write PDL Entries
   PDLEntry entryIter;
   for (entryIter = entryList.begin(); entryIter != entryList.end(); ++entryIter)
      {
      (*entryIter)->writeXMLStream(xmlStream);
      }

   // Write PDL End Tag
   xmlStream << endTag();

   hasChanged = false;
}

void CPDL::dump(ostream& ostrm, string &indentLevel)
{
   string localIndentLevel = indentLevel + ".";
   ostrm << indentLevel << "PDL Entries" << endl;

   PDLEntry entryIter;
   for (entryIter = entryList.begin(); entryIter != entryList.end(); ++entryIter)
      {
      (*entryIter)->dump(ostrm, localIndentLevel);
      }
}

// ---------------------------------------------------------------------------
