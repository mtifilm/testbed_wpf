//
// pch.h
// Header for standard system include files.
//

#pragma once

#include "gtest/gtest.h"
#include <string>
#include "Ippheaders.h"
#include "HRTimer.h"
#include <algorithm>
#include <sstream>
#include <random>

extern MtiSize boxSize;
extern float BoxArray[];
extern float boxProbTruth[];

extern std::string getTempDirectory();
extern std::string getTestDataDirectory();
extern std::string getTempFileName(const std::string &baseName);
extern std::string getTestFileName(const std::string &baseName);

template <typename T> 
static void AssertVectorEqual(const std::vector<T> &v1, const std::vector<T> &v2)
{
	ASSERT_EQ(v1.size(), v2.size());

	for (auto i = 0; i < v1.size(); i++)
	{
		auto s1 = v1[i];
		auto s2 = v2[i];

		if (s1 != s2)
		{
			auto s = s1 - s2;
		}

		ASSERT_EQ(v1[i], v2[i]);
	}
}

static void AssertArrayEqual16u(const IppArray<Ipp16u> &a1, const IppArray<Ipp16u> &a2)
{
	ASSERT_EQ(a1.getHeight(), a2.getHeight());
	ASSERT_EQ(a1.getWidth(), a2.getWidth());
	ASSERT_EQ(a1.getComponents(), a2.getComponents());

	for (auto r = 0; r < a1.getHeight(); r++)
	{
		auto source1 = a1.getRowPointer(r);
		auto source2 = a1.getRowPointer(r);
		for (auto c = 0; c < a1.getWidth(); c++)
		{
			for (auto p = 0; p < a1.getComponents(); p++)
			{
				auto v1 = *source1++;
				auto v2 = *source2++;
				ASSERT_EQ(int(v1), int(v2));
			}
		}
	}
}

template<typename T>
static T getMedianTruth(vector<T> vec)
{
	std::sort(vec.begin(), vec.end());
	auto n = vec.size();
	auto m = n / 2;
	if ((n % 2) == 1)
	{
		return vec[m];
	}

	//	return vec[m - 1];
	return (vec[m - 1] + vec[m]) / 2;
}

////template<typename T>
static int64_t divideFinancial(int64_t numerator, int64_t denominator)
{
	auto a = int64_t(numerator / denominator);
	auto b = int64_t(numerator % denominator);

	auto t = a;
	if (b > (denominator / 2))
	{
		t = t + 1;
	}
	else if (b == (denominator / 2))
	{
		if ((a % 2) != 0)
		{
			t = t + 1;
		}
	}
	return t;
}

template<typename T>
void compareIppArray(const T &expectedArray, const T &testArray)
{
	ASSERT_EQ(expectedArray.getSize(), testArray.getSize()) << "Size do not match";
	auto n = expectedArray.volume();
	auto sz = expectedArray.getSize();
	for (auto i = 0; i < expectedArray.volume(); i++)
	{
		//ASSERT_EQ(expected[i], testArray[i]) << " at index " << i;
		if (expectedArray[i] != testArray[i])
		{
			auto x = expectedArray[i];
			auto y = testArray[i];
			auto z = x - y;
		}
	}
}