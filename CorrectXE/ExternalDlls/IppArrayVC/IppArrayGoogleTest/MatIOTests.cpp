#include "pch.h"
#include "filesystem";

string getTempDirectory()
{
	return "c:\\temp\\";
}

string getTestDataDirectory()
{
	// This is pretty stupid, because the directory in Teamcity is different than
	// under VS2017, we need to see if the data dir exists and if not fallback
	string testData = "..\\..\\IppArrayVC\\TestData\\";
	if (std::filesystem::exists(testData) != 0)
	{
		return testData;
	}

	// This may not exist, but let someone else deal with error
	return "..\\..\\..\\ExternalDlls\\IppArrayVC\\TestData\\";
}

string getTempFileName(const string &baseName)
{
	return getTempDirectory() + baseName;
}

string getTestFileName(const string &baseName)
{
	return getTestDataDirectory() + baseName;
}

TEST(MatIOTests, Ipp8uWriteReadMatOneChannel)
{
	Ipp8uArray image1({ 7, 13 });
	image1.iota();
	auto imageRoi = image1(MtiRect({ 1, 1, 5, 11 }));

	auto fileName = getTempFileName("Ipp8uWriteMatOneChannel.mat");
	MatIO::saveListToMatFile(fileName, image1, "image1");
	AppendVariablesToMatFile1(fileName, imageRoi);

	auto testImageRoi = MatIO::readIpp8uArray(fileName, "imageRoi");
	EXPECT_EQ(imageRoi, testImageRoi);
   
	auto testImage1 = MatIO::readIpp8uArray(fileName, "image1");
	EXPECT_EQ(image1, testImage1);
}

TEST(MatIOTests, IppPlaner32fWriteReadTest)
{
	auto nFrames = 10;
	auto rows = 6;
	auto cols = 7;

	MtiPlanar<Ipp32fArray> planerTest;
	for (auto n : range(nFrames))
	{
		Ipp32fArray a({ cols, rows });
		a.iota(n * 100.0f);
		planerTest.push_back(a);
	}

	auto fileName = getTempFileName("IppPlaner32fWriteReadTest.mat");

	SaveVariablesToMatFile1(fileName, planerTest);
	auto result = MatIO::readIpp32fPlanar(fileName, "planerTest");
	auto rit = result.begin();
	for (auto &t : planerTest)
	{
		auto &r = *rit++;
		ASSERT_EQ(t, r);
	}
}

TEST(MatIOTests, Ipp8uWriteMatThreeChannel)
{
	Ipp8uArray baseImage({7, 13});
	baseImage.iota();

	MtiPlanar<Ipp8uArray> planes = { baseImage, baseImage + 100, baseImage - 40 };
	Ipp8uArray image3Channel;
	image3Channel.copyFromPlanar(planes);

	auto image3ChannelRoi = image3Channel({ 1,1, 5, 11 });
	auto fileName = "c:\\temp\\Ipp8uWriteMatThreeChannel.mat";
	SaveVariablesToMatFile2(fileName, image3Channel, image3ChannelRoi);
}

TEST(MatIOTests, Ipp8uWriteMatMultiple)
{
	Ipp8uArray baseImage({9, 10 });
	baseImage.iota();

	MtiPlanar<Ipp8uArray> planes = { baseImage, baseImage + 100, baseImage - 40 };
	Ipp8uArray image1;
	image1.copyFromPlanar(planes);

	auto imageRoi = image1({ 1, 1, 7, 8 });

	auto fileName = getTempFileName("Ipp8uWriteMatMultiple.mat");
	MatIO::saveListToMatFile(fileName, image1, "m_3", imageRoi, "mRoi_3");
}


TEST(MatIOTests, IppMatIoScalarWriteTest)
{
	int i_int = 37;
	double x_double = 3.14159;
	float f_float = -1.6666f;

	std::string infoLabel = "This is an string of chars to test";
	std::string fileName = getTempFileName("IppMatIoScalarWriteTest.mat");

	// This shows how to create and append
	MatIO::saveListToMatFile(fileName);
	AppendVariablesToMatFile1(fileName, i_int);
	AppendVariablesToMatFile2(fileName, x_double, f_float);
	AppendVariablesToMatFile2(fileName, infoLabel, fileName);

	auto names = MatIO::getVariableNames(fileName);
}

TEST(MatIOTests, IppMatIoMtiRectWriteTest)
{
	MtiRect boxTest = { 1, 2, 10, 20 };
	std::string fileName = getTempFileName("IppMatIoMtiRectWriteTest.mat");
	MatIO::saveListToMatFile(fileName, boxTest, "boxTest");
}

TEST(MatIOTests, IppPlanerWriteReadTest)
{
	auto nFrames = 10;
	auto rows = 6;
	auto cols = 7;

	MtiPlanar<Ipp32fArray> planerTest;
	for (auto n : range(nFrames))
	{
		Ipp32fArray a({cols, rows});
		a.iota(n * 100);
		planerTest.push_back(a);
	}

	auto fileName = getTempFileName("IppPlanerWriteTest.mat");

	SaveVariablesToMatFile1(fileName, planerTest);

	auto result = MatIO::readIpp32fPlanar(fileName, "planerTest");

	auto rit = result.begin();
	for (auto &t : planerTest)
	{
		auto &r = *rit++;
		ASSERT_EQ(t, r);
	}
}

TEST(MatIOTests, IppMatIoVectorIppArrayWriteTest)
{
	const int N = 10;
	const int R = 6;
	const int C = 9;

	vector<Ipp32fArray> planerTest(N);

	auto c = 100.0f;
	for (auto &a : planerTest)
	{
		a = Ipp32fArray({ C, R });
		a.iota();
		a += c;
		c += c;
	}

	std::string fileName = getTempFileName("IppMatIoVectorIppArrayWriteTest.mat");
	SaveVariablesToMatFile1(fileName, planerTest);
}

//
//TEST(MatIOTests, IppPlaner32fRead64fTest)
//{
//	//auto fileName = "c:\\temp\\deltaValuesSmoothed.mat";
//	//auto result = MatIO::readIpp32fPlaner(fileName);
//	//
//	//SaveVariablesToMatFile1("c:\\temp\\junk.mat", result);
//	//auto rit = result.begin();
//	//for (auto &t : planerTest)
//	//{
//	//	auto &r = *rit++;
//	//	ASSERT_EQ(t, r);
//	//}
//}

TEST(MatIOTests, IppMatReadDoubleArrayTest)
{
	auto fileName = getTestFileName("DoubleIota.mat");
	auto iotaTest = MatIO::readIpp32fArrayFromDouble(fileName, "doubleIota");

	Ipp32fArray truth({8, 10 });
	truth.iota();
	truth += 1;
	ASSERT_EQ(truth, iotaTest);

	auto iotaTest3 = MatIO::readIpp32fArrayFromDouble(fileName, "doubleIota3");
	MtiPlanar<Ipp32fArray> planes = { truth - 1, truth + 99, truth - 41 };
	Ipp32fArray truth3;
	truth3.copyFromPlanar(planes);
   SaveVariablesToMatFile1("c:\\temp\\junk.mat", truth3);
	ASSERT_EQ(truth3, iotaTest3);
}

TEST(MatIOTests, IppMatReadWrite8uTest)
{
	Ipp8uArray baseImage({ 9, 10 });
	baseImage.iota();

	MtiPlanar<Ipp8uArray> planes = { baseImage, baseImage + 100, baseImage - 40 };
	Ipp8uArray image1;
	image1.copyFromPlanar(planes);
	auto imageRoi = image1({ 1, 1, 7, 8 });

	auto fileName = getTempFileName("IppMatReadWrite8uTest.mat");
	MatIO::saveListToMatFile(fileName, image1, "m_3", baseImage, "baseImage");

	// read it back in
	auto image1Test = MatIO::readIpp8uArray(fileName);
	ASSERT_EQ(image1, image1Test);

	AppendVariablesToMatFile1(fileName, imageRoi);
	auto imageRoiTest = MatIO::readIpp8uArray(fileName, "imageRoi");
	ASSERT_EQ(imageRoi, imageRoiTest);

	auto baseImageTest = MatIO::readIpp8uArray(fileName, "baseImage");
	ASSERT_EQ(baseImage, baseImageTest);
}
