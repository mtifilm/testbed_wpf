#include "pch.h"
#include "deltaValues.h"

TEST(MtiPlaneTest, CumSumTest)
{
	auto nFrames = 10;
	auto rows = 6;
	auto cols = 7;

	MtiPlanar<Ipp32fArray> sourceArray;
	for (auto n : range(nFrames))
	{
		Ipp32fArray a({ cols, rows });
		a.iota(n * 100.0f);
		sourceArray.push_back(a);
	}

	auto result = sourceArray.cumulativeSum();

	// Check for truth using brute force
	for (auto sn : range(sourceArray.size()))
	{
		auto &cumSum = result[sn];
		for (auto r = 0; r < rows; r++)
		{
			for (auto c = 0; c < cols; c++)
			{
				auto sum = 0.0f;
				for (auto d : range(sn + 1))
				{
					sum += *sourceArray[d].getElementPointer({ c, r });
				}

				//				ASSERT_EQ(sum, cumSum({ c, r }));
			}
		}
	}
}

MtiPlanar<Ipp32fArray> getPlanerTestData()
{
	const auto nFrames = cumSumTestData.size();
	MtiPlanar<Ipp32fArray> result;

	for (auto n : range(nFrames))
	{
		result.emplace_back({ cumArraySize, cumSumTestData[n].data() });
	}

	return result;
}

TEST(MtiPlaneTest, CumSumWriteReadDataTest)
{
	MtiPlanar<Ipp32fArray> testArray = getPlanerTestData();

	auto fileName = getTempFileName("CumSumWriteDataTest.mat");

	SaveVariablesToMatFile1(fileName, testArray);

	auto cumSumArray = testArray.cumulativeSum();

	AppendVariablesToMatFile1(fileName, cumSumArray);

	// Read them back in, of course this doesn't make
	// it a Matlab file but that can be checked
	auto testArrayFromFile = MatIO::readIpp32fPlanar(fileName, "testArray");
	ASSERT_EQ(testArray.size(), testArrayFromFile.size());

	auto testIter = testArrayFromFile.begin();
	for (auto &truth : testArray)
	{
		auto p = *testIter++;
		EXPECT_EQ(truth, p);
	}

	auto cumSumArrayFromFile = MatIO::readIpp32fPlanar(fileName, "cumSumArray");

	auto cumSumIter = cumSumArrayFromFile.begin();
	for (auto &truth : cumSumArray)
	{
		auto p = *cumSumIter++;
		EXPECT_EQ(truth, p);
	}
}

TEST(MtiPlaneTest, CumulativeSumInplaceTest)
{
	MtiPlanar<Ipp32fArray> testArray = getPlanerTestData();
	auto notInplaceSum = testArray.cumulativeSum();
	testArray.cumulativeSumInplace();
	ASSERT_EQ(notInplaceSum, testArray);
}

TEST(MtiPlaneTest, FrontBackTest)
{
	Ipp16uArray A({ 3,4 });
	A.set({ 1 });
	MtiPlanar<Ipp16uArray> planes = { A, A + 1, A + 2, A + 3 };

	EXPECT_EQ(1, planes.front()[0]);
	EXPECT_EQ(4, planes.back()[0]);
}

TEST(MtiPlaneTest, AssignVectorTest)
{
	auto nFrames = 10;
	auto width = 2;
	vector<Ipp8uArray> vectorTest;
	for (Ipp8u i : range(nFrames))
	{
		vectorTest.push_back({ width });
		vectorTest.back().set({ i });
	}

	// this tests the Constructor, not assignment
	MtiPlanar<Ipp8uArray> planeTest = vectorTest;
	EXPECT_EQ(vectorTest.size(), planeTest.size());

	auto iter = vectorTest.begin();
	for (auto i : range(planeTest.size()))
	{
		auto p = *iter++;
		EXPECT_EQ(p, planeTest[i]);
	}

	// This tests assignment
	planeTest.clear();
	planeTest = vectorTest;

	iter = vectorTest.begin();
	for (auto i : range(planeTest.size()))
	{
		auto p = *iter++;
		EXPECT_EQ(p, planeTest[i]);
	}
}

TEST(MtiPlaneTest, IndexTest)
{
	auto nFrames = 10;
	auto width = 2;
	MtiPlanar<Ipp16uArray> planeTest;
	for (Ipp16u i : range(nFrames))
	{
		planeTest.push_back({ width });
		planeTest.back().set({ i });
	}

	for (auto i : range(planeTest.size()))
	{
		EXPECT_EQ(i, planeTest[i][0]);
	}
}

TEST(MtiPlaneTest, Ipp32fPlanerToInterleavedTest)
{
	auto rows = 40;
	auto cols = 151;

	MtiPlanar<Ipp32fArray> planes;
	for (auto i : range(3))
	{
		planes.push_back(Ipp32fArray({ cols, rows }));
		planes.back().normalDistribution(i * 100, 10.4);
	}

	Ipp32fArray image({ cols, rows, 3 });
	image.copyFromPlanar(planes);

	auto backPlanes = image.copyToPlanar();
	auto x = backPlanes.size();
	for (auto i : range(3))
	{
		ASSERT_EQ(planes[i], backPlanes[i]);
	}
}

TEST(MtiPlaneTest, SizeTest)
{
	//auto planerArray = getPlanerTestData();
	//MtiSize truthSize(planerArray.getSize().width, planerArray.getSize().height);
	//ASSERT_EQ(truthSize, planerArray.getSize());
}

TEST(MtiPlaneTest, TraceTest)
{
	MtiPlanar<Ipp32fArray> planerArray = getPlanerTestData();
	auto size = planerArray.getPlaneSize();

	MtiPoint center(size.width / 2, size.height / 2);

	auto testTrace = planerArray.trace(center);

	// Test the ugly way
	int i = 0;
	for (auto &p : planerArray)
	{
		auto v = testTrace[i++];
		auto t = *p.getElementPointer(center);
		ASSERT_EQ(t, v) << "Expected " << t << ", does not match computed " << v;
	}
}


vector<int> mirrorIndexBegin(int beginPad, const vector<int> &center)
{
	auto centerSize = int(center.size());
	auto d = std::min<int>(beginPad, centerSize);
	vector<int> result(d);
	for (auto i : range(d))
	{
		result[i] = center[d - i - 1];
	}

	result.insert(result.end(), center.begin(), center.end());
	if (d >= beginPad)
	{
		return result;
	}

	return mirrorIndexBegin(beginPad - d, result);
}

TEST(MtiPlaneTest, padTest)
{
	MtiPlanar<Ipp16uArray> core({ 2, 3, 5 });
	core[0].iota(1);
	core[1].iota(2);
	core[2].iota(4);
	core[3].iota(8);
	core[4].iota(16);

	//   SaveVariablesToMatFile1("C:\\temp\\test.mat", core);
	auto result = core.duplicateAndPadEnds(13, 19);
	ASSERT_EQ(5 + 13 + 19, result.size()); 
	auto trace = result.trace({ 0,0 });

	vector<int> truth = { 4, 2, 1, 1, 2, 4, 8, 16, 16, 8, 4, 2, 1, 1, 2, 4, 8, 16,
				  16, 8, 4, 2, 1, 1, 2, 4, 8, 16, 16, 8, 4, 2, 1, 1, 2, 4, 8 };

	for (auto i : range(truth.size()))
	{
		ASSERT_EQ(truth[i], trace[i]) << " i = " << i;
	}
}

TEST(MtiPlaneTest, sort5Test)
{
	vector<Ipp16uArray> A;
	vector<Ipp16uArray> B;
	for (auto i : range(5))
	{
		Ipp16uArray A0({ 5, 4, 3 });
		A0.set({ Ipp16u(i), Ipp16u(i), Ipp16u(i) });
		A.push_back(A0);

		Ipp16uArray B0({ 5, 4 });
		B0.uniformDistribution(3, 20);
		B.push_back(B0);
	}

	//SortAbyB(A[0], A[1], B[0], B[1]);
	bubbleSortAbyB(A, B);

	auto A0 = A[0];
	auto A1 = A[1];
	auto A2 = A[2];

	auto B0 = B[0];
	auto B1 = B[1];
	auto B2 = B[2];

	//SaveVariablesToMatFile4("C:\\temp\\test.mat", A1, A2, B1, B2);
	//AppendVariablesToMatFile2("C:\\temp\\test.mat", A0, B0);
}