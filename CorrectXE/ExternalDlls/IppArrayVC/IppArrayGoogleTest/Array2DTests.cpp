#include "pch.h"
#include "Array2D.h"

TEST(Array2D, Array2DIntTest)
{
	Array2D<int> arr(4, 3);
	ASSERT_EQ(4 * 3, arr.size());
	ASSERT_EQ(4, arr.rows());
	ASSERT_EQ(3, arr.columns());
	ASSERT_EQ(0, arr.sub2ind(0, 0));
	ASSERT_EQ(7, arr.sub2ind(2, 1));
	ASSERT_EQ(1, arr.ind2sub(7).x);
	ASSERT_EQ(2, arr.ind2sub(7).y);
	arr(1, 2) = 42;
	assert(arr(1, 2) == 42);
	arr[8] = 100;
	ASSERT_EQ(100, arr[8]);
}

TEST(Array2D, Array2DIpp16uArrayTest)
{
	Ipp16uArray t1;
	t1.iota();

	Array2D<Ipp16uArray> test2D(4, 5);
	{
		Ipp16uArray tmpArray({ 3, 7 });
		tmpArray.iota();

		test2D[4] = tmpArray;
		test2D[5] = tmpArray + 1;
		test2D(2, 3) = tmpArray + 10;
	}

	auto s = test2D(2, 3);
	auto t = test2D[2 * 5 + 3];
	ASSERT_EQ(s, t);
	ASSERT_NE(s, test2D[4]);

	auto p5 = test2D.ind2sub(5);
	ASSERT_EQ(231.0, test2D[p5].sum()[0]);
}

TEST(Array2D, Array2DAssignmentTest)
{
	Ipp16uArray t1({ 7, 5 });
	t1.iota();
	Array2D<Ipp16uArray> test2D(4, 5);
	int r = 0;
	for (auto &a : test2D)
	{
		a = t1 + r++;
	}

	Array2D<Ipp16uArray> copy2D;
	copy2D = test2D;

	ASSERT_EQ(test2D.size(), copy2D.size());
	ASSERT_EQ(test2D.columns(), copy2D.columns());
	ASSERT_EQ(test2D.rows(), copy2D.rows());
	ASSERT_EQ(test2D(3, 4), copy2D(3, 4));
}