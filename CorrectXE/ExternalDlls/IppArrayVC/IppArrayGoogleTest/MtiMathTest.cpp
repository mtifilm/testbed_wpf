#include "pch.h"
#include "MtiAncillaryTemplates.h"

using std::vector;
using std::string;

TEST(MtiMath, PClipSlopeTest)
{
	vector<double> x = { 1, 100, 101};
	vector<double> y = { 1, 100, 104.2759732462142 };
	vector<double> t(100);
	for (auto i = 0; i < t.size() ;i++)
	{
		t[i] = i+1;
	}
	
	auto result = MtiMath::pchip(x, y, t);
	//MatIO::saveListToMatFile("y:\\temp\\test1.mat", result, "test_front");
}

TEST(MtiMath, StandardizeLutTest)
{
	const auto fileName = getTestFileName("LutStandardizationData.mat");
	auto means = MatIO::readVectorIpp64f(fileName, "m2");
	auto sds = MatIO::readVectorIpp64f(fileName, "sd2");
	auto truthLut = MatIO::readVectorIpp64f(fileName, "LUT");

	auto result = MtiMath::standardizeLut(means, sds);
	Ipp32fArray lut;
	lut <<= result;
	///MatIO::saveListToMatFile("y:\\temp\\test1.mat", result, "lutC");
}

TEST(MtiMath, MeanAndSDFromHistogramTest)
{
	const auto fileName = getTestFileName("meanSdHistogramTest.mat");
	const auto histograms = MatIO::readIpp32sArray(fileName, "histograms");
	const auto meanTruth = MatIO::readVectorIpp64f(fileName, "means");
	const auto stdevTruth = MatIO::readVectorIpp64f(fileName, "stdev");
	// Each row of the array is a histogram
	const auto numHistograms = histograms.getHeight();
	vector<double> meansC(numHistograms);
	vector<double> stdevC(numHistograms);
	vector<int> numC(numHistograms);

	double lastStdev = 0.0;
	for (auto r : range(histograms.getHeight()))
	{
		MtiRect rowBox = { 0, r, histograms.getWidth(), 1 };
		auto rowVector = histograms.toVector({ 0, r, histograms.getWidth(), 1 })[0];
		std::tie(meansC[r], stdevC[r], numC[r]) = MtiMath::meanAndSdFromHistogram(rowVector, 1024, r, lastStdev);

		// Normally we feed the last sd back in so we don't have zero sd's but the matlab code truth doesn't
		//lastStdev = stdev[r];
	}

	ASSERT_TRUE(meansC == meanTruth);
	ASSERT_TRUE(stdevC == stdevTruth);
}
//
//TEST(MtiMath, LutViaPolyregularize)
//{
//	auto M80 = MatIO::readIpp32fArray(R"(c:\temp\m8020.mat)", "M80");
//}