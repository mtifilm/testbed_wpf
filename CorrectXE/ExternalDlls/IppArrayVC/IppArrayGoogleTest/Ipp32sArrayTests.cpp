#include "pch.h"



		TEST(Ipp32s, Ipp32sSetTest)
		{
			Ipp32sArray image({ 10, 20, 3 });
			image.set({ 1, 2, 3 });

			for (auto r = 0; r < image.getHeight(); r++)
			{
				auto rp = image.getRowPointer(r);
				for (auto c = 0; c < image.getWidth(); c++)
				{
					ASSERT_EQ(1, *rp++);
					ASSERT_EQ(2, *rp++);
					ASSERT_EQ(3, *rp++);
				}
			}
		}

		TEST(Ipp32s, Ipp32sZeroTest)
		{
			Ipp32sArray image({ 10, 211, 3 });
			image.iota();
			auto zees = image.sum();
			for (auto &z : zees)
			{
				ASSERT_NE(0.0, z);
			}

			image.zero();

			zees = image.sum();
			for (auto &z : zees)
			{
				ASSERT_EQ(0.0, z);
			}
		}

		TEST(Ipp32s, Ipp32sSumTest)
		{
			Ipp32sArray image({ 16, 20 });
			image.zero();
			auto z = image.sum();
			ASSERT_EQ(0.0, z[0]);

			image.iota();

			auto v = image.sum();
			auto truth = image.getHeight() * image.getWidth();
			truth = (truth - 1)*(truth) / 2;
			ASSERT_EQ(double(truth), v[0]);
		}


		TEST(Ipp32s, Ipp32sArithmiticArrayTest)
		{
			Ipp32sArray image1({ 16, 20 });
			image1.iota();
			image1 = image1 + 1;

			Ipp32sArray image2({ 16, 20 });
			image2.uniformDistribution(-10, 10);

			auto vec1 = image1.toVector()[0];
			auto vec2 = image2.toVector()[0];

			auto test = image1 + image2;
			auto vect = test.toVector()[0];
			for (auto i = 0; i < vect.size(); i++)
			{
				ASSERT_EQ(vec1[i] + vec2[i], vect[i]);
			}

			test = image1 - image2;
			vect = test.toVector()[0];
			for (auto i = 0; i < vect.size(); i++)
			{
				ASSERT_EQ(vec1[i] - vec2[i], vect[i]);
			}

			test = image2 / image1;
			vect = test.toVector()[0];
			for (auto i = 0; i < vect.size(); i++)
			{
				ASSERT_EQ(vec2[i] / vec1[i], vect[i]);
			}

			test = image2 * image1;
			vect = test.toVector()[0];
			for (auto i = 0; i < vect.size(); i++)
			{
				ASSERT_EQ(vec1[i] * vec2[i], vect[i]);
			}
		}

		TEST(Ipp32s, Ipp32sArithmiticScalerTest)
		{
			Ipp32sArray image1({ 16, 20 });
			image1.normalDistribution(10, 3);

			auto vec1 = image1.toVector()[0];

			auto test = image1 + 32;
			auto vect = test.toVector()[0];
			for (auto i = 0; i < vect.size(); i++)
			{
				ASSERT_EQ(vec1[i] + 32, vect[i]);
			}

			test = image1 - 10;
			vect = test.toVector()[0];
			for (auto i = 0; i < vect.size(); i++)
			{
				ASSERT_EQ(vec1[i] - 10, vect[i]);
			}

			test = image1 / 22;
			vect = test.toVector()[0];
			for (auto i = 0; i < vect.size(); i++)
			{
				ASSERT_EQ(vec1[i] / 22, vect[i]);
			}

			test = image1 * 10;
			vect = test.toVector()[0];
			for (auto i = 0; i < vect.size(); i++)
			{
				ASSERT_EQ(vec1[i] * 10, vect[i]);
			}
		}

		TEST(Ipp32s, Ipp32sAssignmentTest)
		{
			Ipp32sArray image({ 16, 20 });
			image.normalDistribution(1000, 200);

			IppiRect roi = { 5, 6, 8, 10 };
			Ipp32sArray subImage = image(roi);
			auto truthSum = subImage.sum()[0];

			auto subImage2 = subImage;
			auto &roi2 = subImage2.getRoi();

			ASSERT_EQ(roi.x, roi2.x);
			ASSERT_EQ(roi.y, roi2.y);
			ASSERT_EQ(roi.width, roi2.width);
			ASSERT_EQ(roi.height, roi2.height);

			ASSERT_EQ(truthSum, subImage2.sum()[0]);
		}

		TEST(Ipp32s, Ipp32sL1NormTest)
		{
			Ipp32fArray image({ 10, 20, 3 });
			image.set({ 1, 2, 3 });
			auto norm1 = image.L1Norm();
			auto sum1 = image.sum();
			AssertVectorEqual(sum1, norm1);

			Ipp32fArray box(boxSize);
			box.copyFromPod(BoxArray, box.area());
			auto norm0 = box.L1Norm();
			auto sum0 = box.sum();
			AssertVectorEqual(sum0, norm0);

			Ipp32sArray normal({ 20, 10, 3 });
			normal.iota();

			normal = (normal - 100);
			auto norm2 = normal.L1Norm();
			auto sum2 = normal.sum();
			auto lvec = normal.toVector();
			auto ls = lvec.size();
			for (auto i : range(ls))
			{
				auto &vec = lvec[i];
				auto t = std::accumulate(vec.begin(), vec.end(), 0.0f, [](float x, float y) {return x + abs(y); });
				ASSERT_EQ(double(t), norm2[i]);
			}
		}

		TEST(Ipp32s, Ipp32sL2NormTest)
		{
			Ipp32sArray normal({ 20, 10, 3 });
			normal.iota();
			//			normal = normal - 100;
			auto norm2 = normal.L2Norm();
			auto lvec = normal.toVector();
			auto ls = lvec.size();
			for (auto i : range(ls))
			{
				auto &vec = lvec[i];
				auto t = std::accumulate(vec.begin(), vec.end(), 0.0f, [](float x, float y) {return x + (y*y); });
				auto truth = sqrt(double(t));
				ASSERT_NE(truth, 0.0);
				ASSERT_EQ(truth, norm2[i]);
			}
		}

		TEST(Ipp32s, Ipp32sRoiTest)
		{
			Ipp32sArray Image({ 10, 20, 3 });
			Image.set({ 1, 2, 3 });

			MtiRect roi = { 5, 10, 3, 4 };
			auto roiImage = Image(roi);

			ASSERT_EQ(roi.height, roiImage.getHeight());
			ASSERT_EQ(roi.width, roiImage.getWidth());

			// Change the image ROI should change
			auto p = Image.getElementPointer(roi.tl());
			p[0] = 11;
			p[1] = 12;
			p[2] = 13;

			auto s = roiImage.getRowPointer(0);

			for (auto i = 0; i < 3; i++)
			{
				ASSERT_EQ(p[i], s[i]);
			}

			roiImage.set({ 100, 200, 300 });
			for (auto i = 0; i < 3; i++)
			{
				ASSERT_EQ(p[i], s[i]);
			}
		}

		TEST(Ipp32s, Ipp32sArrayPodTest)
		{
			auto rows = 10;
			auto cols = 30;
			auto p = ippsMalloc_32s(rows * cols);

			// Make the index and values the same
			for (auto i = 0; i < rows*cols; i++)
			{
				p[i] = i;
			}

			{
				// See if data agrees, if it was copied
				// the alignment of IppArray allocator would be off
				// and the results would be 32 instead of 30 for row(1, 0, 0)
				Ipp32sArray PodBacking({ cols, rows }, p);
				auto v1 = PodBacking[{10, 1}];
				ASSERT_EQ(1 * cols + 10, v1);
				auto v2 = PodBacking[{13, 7}];
				ASSERT_EQ(7 * cols + 13, v2);

				// Now blow away some data, this should change
				// underlying allocated array.
				auto dep = PodBacking.getElementPointer({13, 7});
				*dep = -345;

				ASSERT_EQ(-345, p[7 * cols + 13]);
			}

			// IppArray is blown away, free should work
			ippsFree(p);
		}

		TEST(Ipp32s, Ipp32sMedianTestEven)
		{
			Ipp32sArray image({ 100, 110, 3 });
			float mean = 10000;
			float stddev = 10000.0f / 3.0f;

			image.normalDistribution(mean, stddev);

			auto medians = image.median();

			auto vecs = image.toVector({ 0,0, image.getWidth(), image.getHeight() });
			for (auto vec : vecs)
			{
				auto t0 = getMedianTruth(vec);
				auto v0 = image.median(vec);
				ASSERT_EQ(t0, v0);
			}
		}

		TEST(Ipp32s, Ipp32sMedianTestOdd)
		{
			Ipp32sArray image({ 101, 111, 3 });
			float mean = 10000;
			float stddev = 10000.0f / 3.0f;

			image.normalDistribution(mean, stddev);

			auto vecs = image.toVector({ 0,0, image.getWidth(), image.getHeight() });
			for (auto vec : vecs)
			{
				auto t0 = getMedianTruth(vec);
				auto v0 = image.median(vec);
				ASSERT_EQ(t0, v0);
			}
		}

		TEST(Ipp32s, Ipp32sExtractVectorTest)
		{
			Ipp32sArray image({ 10, 6, 3 });
			image.set({ 1,2,3 });
			IppiRect roi = { 2, 3, 4, 2 };
			auto subImage = image(roi);

			vector<Ipp32s> inserts = { 11,12,13 };
			subImage.set(inserts);

			// Not a great test
			auto vecs = image.toVector(roi);
			auto i = 0;
			for (auto vec : vecs)
			{
				ASSERT_EQ(roi.height*roi.width, int(vec.size()));
				auto t0 = inserts[i++] * roi.height*roi.width;
				auto v0 = std::accumulate(vec.begin(), vec.end(), 0);
				ASSERT_EQ(t0, v0);
			}
		}

		TEST(Ipp32s, Ipp32sPlanerToInterleavedTest)
		{
			auto rows = 40;
			auto cols = 151;

			MtiPlanar<Ipp32sArray> planes;
			for (auto i : range(3))
			{
				planes.push_back(Ipp32sArray({ cols, rows }));
				planes.back().normalDistribution(i * 100, 10.4);
			}

			Ipp32sArray image({ cols, rows, 3 });
			image.copyFromPlanar(planes);

			auto backPlanes = image.copyToPlanar();
			auto x = backPlanes.size();
			for (auto i : range(3))
			{
				ASSERT_EQ(planes[i], backPlanes[i]);
			}
		}

		TEST(Ipp32s, Ipp32sIotaVectorTest)
		{
			Ipp32sArray image({ 11, 12, 3 });
			image.iota();

			// Not a great test
			auto vecs = image.toVector();
			auto i = 0;
			vector<Ipp32s> truth(image.area());
			for (auto i = 0; i < truth.size(); i++)
			{
				truth[i] = i;
			}

			for (auto vec : vecs)
			{
				AssertVectorEqual(truth, vec);
			}
		}

		TEST(Ipp32s, Ipp32sCopyConstructorTest)
		{
			Ipp32sArray image({ 13, 12, 3 });
			image.iota();

			Ipp32sArray copyOfImage(image);
			auto truth = image.area();
			truth = (truth - 1)*truth / 2;

			ASSERT_EQ((Ipp64f)truth, copyOfImage.sum()[0]);
		}

		TEST(Ipp32s, Ipp32sCopyConstructorRoiTest)
		{
			Ipp32sArray image({ 13, 12, 3 });
			image.iota();

			IppiRect roi = { 3, 2, 8, 7 };
			Ipp32sArray subImage = image(roi);

			Ipp64f truth = 0;
			for (auto r = roi.y; r < roi.y + roi.height; r++)
			{
				for (auto c = roi.x; c < roi.x + roi.width; c++)
				{
					truth += image[{c, r}];
				}
			}

			ASSERT_EQ((Ipp64f)truth, subImage.sum()[0]);

			Ipp32sArray subSubImage =subImage({ 2, 3, 4, 3 });

			roi = { roi.x + 2, roi.y + 3, 4, 3 };
			auto &r1 = subSubImage.getRoi();
			ASSERT_EQ(roi.x, r1.x);
			ASSERT_EQ(roi.y, r1.y);
			ASSERT_EQ(roi.width, r1.width);
			ASSERT_EQ(roi.height, r1.height);

			truth = 0;
			for (auto r = roi.y; r < roi.y + roi.height; r++)
			{
				for (auto c = roi.x; c < roi.x + roi.width; c++)
				{
					truth += image[{c, r}];
				}
			}

			ASSERT_EQ((Ipp64f)truth, subSubImage.sum()[0]);
		}

		TEST(Ipp32s, Ipp32sRowIteratorTest)
		{
			Ipp32sArray image({ 100, 50 });
			image.iota();

			auto t = 0.0;
			for (auto p : image.rowIterator())
			{
				for (auto c : range(image.getWidth()))
				{
					t += *p++;
				}
			}

			auto s = image.sum()[0];
			ASSERT_EQ(s, t);
		}

		TEST(Ipp32s, Ipp32sMeanTest)
		{
			Ipp32sArray normal({ 20, 11, 3 });
			normal.normalDistribution(101, 200);
			auto mean = normal.mean();
			auto lvec = normal.toVector();
			for (auto i : range(lvec.size()))
			{
				auto &vec = lvec[i];
				auto t = std::accumulate(vec.begin(), vec.end(), 0.0f, [](float x, float y) {return x + y; });
				t = t / normal.area();

				// We want FAST not accurate
				ASSERT_NEAR(double(t), mean[i], 10E-5);
			}
		}

		TEST(Ipp32s, Ipp32sDotProductTest)
		{
			Ipp32sArray n1({ 21, 11, 3 });
			n1.normalDistribution(1, 0.1);
			auto n2 = n1 + 1;

			auto dotProduct = n1.dotProduct(n2);

			vector<Ipp64f> t = { 0,0,0 };
			for (auto r = 0; r < n1.getHeight(); r++)
			{
				auto p1 = n1.getRowPointer(r);
				auto p2 = n2.getRowPointer(r);
				for (auto c = 0; c < n1.getWidth(); c++)
				{
					for (auto k = 0; k < n1.getComponents(); k++)
					{
						t[k] += *p1++ * *p2++;
					}
				}
			}

			for (auto i : range(dotProduct.size()))
			{
				ASSERT_NEAR(t[i], dotProduct[i], 10E-6);
			}
		}

		TEST(Ipp32s, Ipp32sL1NormDiffTest)
		{
			Ipp32sArray n1({ 13, 21, 3 });
			n1.normalDistribution(1, 0.1);
			auto n2 = n1 + 1;

			vector<Ipp64f> dotProduct(3);
			n1.L1NormDiff(n2, dotProduct.data());

			vector<Ipp64f> t = { 0,0,0 };
			for (auto r = 0; r < n1.getHeight(); r++)
			{
				auto p1 = n1.getRowPointer(r);
				auto p2 = n2.getRowPointer(r);
				for (auto c = 0; c < n1.getWidth(); c++)
				{
					for (auto k = 0; k < n1.getComponents(); k++)
					{
						t[k] += abs(*p1++ - *p2++);
					}
				}
			}

			for (auto i : range(dotProduct.size()))
			{
				ASSERT_NEAR(t[i], dotProduct[i], 10E-6);
			}
		}

		template<typename T>
		class myvect : public vector<T>
		{
		public:
			T stupid(int n) { return (*this)[n]; }
		};

		TEST(Ipp32s, Ipp32sLinearIteratorTest)
		{
			// Test one channel
			Ipp32sArray n0({ 5, 7 });
			n0.iota();
			int idx = 0;
			for (auto f : n0)
			{
				// This is iota, so index is value
				ASSERT_EQ(f, idx);
				idx++;
			}

			// test 3 channels;
			Ipp32sArray n1({ 5, 7, 3 });
			n1.normalDistribution(0, 30);
			n1.iota();

			idx = 0;
			for (auto f : n1)
			{
				// This is iota, so index is value
				ASSERT_EQ(f, idx / 3);
				idx++;
			}
		}
		TEST(Ipp32s, Ipp32sIndexTest)
		{
			Ipp32sArray n1({ 5, 7, 3 });
			n1.normalDistribution(0, 30);
			n1.iota();
			auto idx = 0;
			auto it = n1.begin();
			for (auto r = 0; r < n1.getHeight(); r++)
			{
				for (auto c = 0; c < n1.getWidth(); c++)
				{
					for (auto d = 0; d < n1.getComponents(); d++)
					{
						auto v = n1[{c, r, d}];
						ASSERT_EQ(v, n1[idx]);
						ASSERT_EQ(v, it[idx]);
						++it;
						idx++;
					}
				}
			}

			auto i = 0;
			for (auto f : n1)
			{
				auto x = n1[i];
				idx = i / n1.getComponents();
				if (idx != f)
				{
					x = n1[i];
				}
				if (x != f)
				{
					x = n1[i];
				}

				ASSERT_EQ(f, n1[i]);
				i++;
			}
		}

		TEST(Ipp32s, Ipp32sConvert32fTest)
		{
			Ipp32sArray image({ 100, 4, 3 });
			image.iota({ 1, -100, 30});

			auto v = image.toVector()[0];
			auto floatArray = Ipp32fArray(image);
			auto f = floatArray.toVector()[0];
			for (auto i : range(image.volume()))
			{
				auto a = floatArray[i];
				auto b = image[i];
				ASSERT_FLOAT_EQ(floatArray[i], image[i]);
			}
		}