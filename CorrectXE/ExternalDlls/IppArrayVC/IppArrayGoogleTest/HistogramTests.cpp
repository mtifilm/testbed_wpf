#include "pch.h"
#include "Windows.h"

template<typename T>
vector<vector<Ipp32u>> GetHistogramTruth(const T &image, const HistogramIpp &histogram)
{
	auto bins = histogram.getBins();
	auto dataMin = histogram.getDataMin();
	auto dataMax = histogram.getDataMax();

	vector <vector<Ipp32u>> truth;
	for (auto c = 0; c < image.getComponents(); c++)
	{
		truth.emplace_back(bins, 0);
	}

	auto truthLevels = histogram.getLevels();
	auto truthLevelSize = truthLevels.size();
	auto f = (dataMax - dataMin) / double(bins);

	for (auto r = 0; r < image.getSize().height; r++)
	{
		auto inp = image.getRowPointer(r);
		for (auto cols = 0; cols < image.getWidth(); cols++)
		{
			for (auto c = 0; c < image.getComponents(); c++)
			{
				auto p = *inp++;
				if ((p < dataMin) || (p >= dataMax))
				{
					continue;
				}

				// guess at level, usually perfect but
				// round off errors can make it one off
				// So underestimate 
				auto l = std::max<int>(0, (int)((p - dataMin) / f) - 1);
				while ((p >= truthLevels[l]) && (l < truthLevelSize)) l++;
				truth[c][l - 1]++;
			}
		}
	}

	return truth;
}

		TEST(IppHistogramTest, CreateHistogramTest)
		{
			IppiSize size{ 30, 20 };
			auto histogram = new HistogramIpp(size, 3, ipp32f, 256, 0, 65536.0f);
			ASSERT_EQ(size.height, histogram->getSize().height);
			ASSERT_EQ(size.width, histogram->getSize().width);

			ASSERT_EQ(256, histogram->getBins());
			ASSERT_EQ(0.0f, histogram->getDataMin());
			ASSERT_EQ(65536.0f, histogram->getDataMax());
		}

		TEST(IppHistogramTest, CreateHistogramDataLimitsTest)
		{
			IppiSize size{ 300, 200 };
			auto histogram = new HistogramIpp(size, 1, ipp32f, 100, -500, 1500);
			ASSERT_EQ(size.height, histogram->getSize().height);
			ASSERT_EQ(size.width, histogram->getSize().width);

			ASSERT_EQ(100, histogram->getBins());
			ASSERT_EQ(-500.0f, histogram->getDataMin());
			ASSERT_EQ(1500.0f, histogram->getDataMax());
		}

		TEST(IppHistogramTest, FullImage_32f_C3)
		{
			Ipp32fArray image({ 500, 1024, 3 });
			float mean = 0.0;
			float stddev = 10000.0f / 3.0f; // 99.7% of values will be inside [0, +10000.0] interval

			image.normalDistribution(mean, stddev);
			// Bins have to be power of 2 for my truth to work
			const int Bins = 64;

			HistogramIpp histogram(image, Bins, -32768, 32768);
			auto truth = GetHistogramTruth(image, histogram);

			std::wostringstream os;
			CHRTimer computeHistogramFullImageTimer;
			auto results = histogram.compute(image);
			os << computeHistogramFullImageTimer;

			results = histogram.compute(image);
			os << ", 2nd: " << computeHistogramFullImageTimer << std::endl;

//			Logger::WriteMessage(os.str().c_str());

			for (int i = 0; i < histogram.getChannels(); i++)
			{
				ASSERT_EQ(truth[i], results[i]);
			}
		}

		TEST(IppHistogramTest, BoxCountTest)
		{
			Ipp32fArray box(boxSize);
			box.copyFromPod(BoxArray, box.getWidth() * box.getHeight());

			HistogramIpp histogram(box, 1024, -0.5, 65535.5);
			auto results = histogram.compute(box);
			auto counts = histogram.counts(results);

			//MatIO::saveListToMatFile("c:\\temp\\histogramBoxTest.mat", results[0], "histogramBox");

			ASSERT_EQ(box.getHeight() * box.getWidth(), (int)counts[0]);

			auto lowCount = 0;
			auto testPoint = 37521; // This value is in box
			for (auto r = 0; r < box.getHeight(); r++)
			{
				auto p = box.getRowPointer(r);
				for (auto c = 0; c < box.getWidth(); c++)
				{
					if (*p++ < testPoint)
					{
						lowCount++;
					}
				}
			}

			HistogramIpp histoLow(box, 1024, 0.0f, float(testPoint));
			results = histoLow.compute(box);
			counts = histoLow.counts(results);
			ASSERT_EQ(lowCount, int(counts[0]));
		}

		TEST(IppHistogramTest, ProbabilityBoxTest)
		{
			Ipp32fArray box(boxSize, BoxArray);
			HistogramIpp histogram(box, 1024, -0.5, 65535.5);
			auto probBoxResult = histogram.computeProbability(box)[0];

			auto sum = std::accumulate(probBoxResult.begin(), probBoxResult.end(), 0.0f);

			auto sumTruth = 0.0f;
			for (auto i = 0; i < probBoxResult.size(); i++)
			{
				auto d = abs(probBoxResult[i] - boxProbTruth[i]);
				sumTruth += boxProbTruth[i];
				if (d > 0.5f / 65536.0)
				{
					ASSERT_EQ(probBoxResult[i], boxProbTruth[i]);
				}
			}

			SaveVariablesToMatFile1("c:\\temp\\ProbBoxTest.mat", probBoxResult);
		}

		TEST(IppHistogramTest, FastHistogramTest)
		{
			Ipp32fArray box(boxSize, BoxArray);

			FastHistogramIpp::compute(box, 65536, -0.5, 65535.5);
			CHRTimer hrt;

			auto result = FastHistogramIpp::compute(box, 1024, -0.5, 65535.5)[0];
			auto t0 = hrt.intervalMilliseconds();

			auto vec = box.toVector();
			auto m = Ipp32fArray::median(vec[0]);
			auto t1 = hrt.intervalMilliseconds();

			std::wostringstream os;
			os << "histogram " << t0 << ", median " << t1;
//			Logger::WriteMessage(os.str().c_str());
			auto result2 = FastHistogramIpp::compute(box, 512, -0.5, 65535.5);
		}

		TEST(IppHistogramTest, Ipp32fHistogramTest)
		{
			Ipp32fArray box(boxSize, BoxArray);

			// Prime the flyweight histogram
			FastHistogramIpp::computeIpp(box, 65536, -0.5, 65535.5);
			CHRTimer hrt;

			Ipp32sArray result = FastHistogramIpp::computeIpp(box, 1024, -0.5, 65535.5);
			Ipp32fArray histogramBoxData;
			histogramBoxData <<= result;
			auto t0 = hrt.intervalMilliseconds();
			SaveVariablesToMatFile1(getTempFileName("HistogramBoxData.mat"), histogramBoxData);

			auto vec = box.toVector();
			auto m = Ipp32fArray::median(vec[0]);
			auto t1 = hrt.intervalMilliseconds();

			std::wostringstream os;
			os << "histogram " << t0 << ", median " << t1;
//			Logger::WriteMessage(os.str().c_str());
			auto result2 = FastHistogramIpp::compute(box, 512, -0.5, 65535.5);
		}

		//vector<vector<Ipp32u>> GetHistogramTruth_16U_C3(const cv::Mat &image, const mcv::HistogramMcv &histogram)
		//{
		//	auto bins = histogram.getBins();
		//	auto dataMin = histogram.getDataMin();
		//	auto dataMax = histogram.getDataMax();

		//	vector <vector<Ipp32u>> truth = { vector<Ipp32u>(bins, 0), vector<Ipp32u>(bins, 0), vector<Ipp32u>(bins, 0) };
		//	auto f = (dataMax - dataMin) / (double)bins;

		//	vector<float> truthLevels(bins + 1);
		//	for (auto i = 0; i < bins; i++)
		//	{
		//		truthLevels[i] = (float)(int)(i * f + dataMin);
		//	}

		//	truthLevels[bins] = dataMax;

		//	for (auto r = 0; r < image.size().height; r++)
		//	{
		//		for (auto c = 0; c < image.size().width; c++)
		//		{
		//			auto &b = image.at<cv::Vec3w>(r, c);
		//			for (auto i = 0; i < 3; i++)
		//			{
		//				auto p = b[i];
		//				if ((p < dataMin) || (p >= dataMax))
		//				{
		//					continue;
		//				}

		//				// guess at level
		//				auto l = std::max<int>(0, (int)((p - dataMin) / f) - 1);
		//				while (p >= truthLevels[l]) l++;
		//				truth[i][l - 1]++;
		//			}
		//		}
		//	}

		//	return truth;
		//}

		//vector<vector<Ipp32u>> GetHistogramTruth_16U_C1(const cv::Mat &image, const mcv::HistogramMcv &histogram)
		//{
		//	auto bins = histogram.getBins();
		//	auto dataMin = histogram.getDataMin();
		//	auto dataMax = histogram.getDataMax();

		//	vector <vector<Ipp32u>> truth = { vector<Ipp32u>(bins, 0) };
		//	vector<float> truthLevels(bins + 1);

		//	auto f = (dataMax - dataMin) / (double)bins;
		//	for (auto i = 0; i < bins; i++)
		//	{
		//		truthLevels[i] = (float)(int)(i * f + dataMin);
		//	}

		//	truthLevels[bins] = dataMax;

		//	for (auto r = 0; r < image.size().height; r++)
		//	{
		//		for (auto c = 0; c < image.size().width; c++)
		//		{
		//			auto p = image.at<ushort>(r, c);
		//			if ((p < dataMin) || (p >= dataMax))
		//			{
		//				continue;
		//			}

		//			// guess at level
		//			auto l = max(0, (int)((p - dataMin) / f) - 1);
		//			while (p >= truthLevels[l]) l++;
		//			truth[0][l - 1]++;
		//		}
		//	}

		//	return truth;
		//}

		//vector<vector<Ipp32u>> GetHistogramTruth_32F_C1(const IppArray &image, const HistogramIpp &histogram)
		//{
		//	auto bins = histogram.getBins();
		//	auto dataMin = histogram.getDataMin();
		//	auto dataMax = histogram.getDataMax();

		//	vector <vector<Ipp32u>> truth = { vector<Ipp32u>(bins, 0) };

		//	// I realize this assumes getLevels are correct but I can't figure it out
		//	auto f = (dataMax - dataMin) / (double)bins;
		//	auto truthLevels = histogram.getLevels();

		//	for (auto r = 0; r < image.size().height; r++)
		//	{
		//		for (auto c = 0; c < image.size().width; c++)
		//		{
		//			auto p = image.at<float>(r, c);
		//			if ((p < dataMin) || (p >= dataMax))
		//			{
		//				continue;
		//			}

		//			// guess at level
		//			auto l = max(0, (int)((p - dataMin) / f) - 1);
		//			while (p >= truthLevels[l]) l++;
		//			truth[0][l - 1]++;
		//		}
		//	}

		//	return truth;
		//}

		//vector<vector<Ipp32u>> GetHistogramTruth_8U_C1(const cv::Mat &image, const mcv::HistogramMcv &histogram)
		//{
		//	auto bins = histogram.getBins();
		//	auto dataMin = histogram.getDataMin();
		//	auto dataMax = histogram.getDataMax();

		//	vector <vector<Ipp32u>> truth = { vector<Ipp32u>(bins, 0) };
		//	vector<float> truthLevels(bins + 1);

		//	auto f = (dataMax - dataMin) / (double)bins;
		//	for (auto i = 0; i < bins; i++)
		//	{
		//		truthLevels[i] = (float)(int)(i * f + dataMin);
		//	}

		//	truthLevels[bins] = dataMax;

		//	for (auto r = 0; r < image.size().height; r++)
		//	{
		//		for (auto c = 0; c < image.size().width; c++)
		//		{
		//			auto p = image.at<uchar>(r, c);
		//			if ((p < dataMin) || (p >= dataMax))
		//			{
		//				continue;
		//			}

		//			// guess at level
		//			auto l = max(0, (int)((p - dataMin) / f) - 1);
		//			while (p >= truthLevels[l]) l++;
		//			truth[0][l - 1]++;
		//		}
		//	}

		//	return truth;
		//}

		//vector<vector<Ipp32u>> GetHistogramTruth_8U_C3(const cv::Mat &image, const mcv::HistogramMcv &histogram)
		//{
		//	auto bins = histogram.getBins();
		//	auto dataMin = histogram.getDataMin();
		//	auto dataMax = histogram.getDataMax();

		//	// Someday this truth needs to be fixed
		//	vector <vector<Ipp32u>> truth = { vector<Ipp32u>(bins, 0), vector<Ipp32u>(bins, 0), vector<Ipp32u>(bins, 0) };
		//	auto f = (dataMax - dataMin) / bins;
		//	for (auto r = 0; r < image.size().height; r++)
		//	{
		//		for (auto c = 0; c < image.size().width; c++)
		//		{
		//			auto &p = image.at<cv::Vec3b>(r, c);
		//			truth[0][(int)(p[0] / f)]++;
		//			truth[1][(int)(p[1] / f)]++;
		//			truth[2][(int)(p[2] / f)]++;
		//		}
		//	}

		//	return truth;
		//}