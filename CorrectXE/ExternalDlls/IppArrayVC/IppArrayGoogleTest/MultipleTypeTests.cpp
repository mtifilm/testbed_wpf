#include "pch.h"

//////////////////////////////////////////////////////////////////////////

template<typename T>
struct IppSingleArrayTest : public testing::Test
{
	// This allows the type to be used in the inherited class
	using ippArrayType = T;
};

using IppArrayTypes = testing::Types<Ipp8uArray, Ipp16uArray, Ipp32fArray, Ipp32sArray>;
using IppSignedArrayTypes = testing::Types<Ipp32fArray, Ipp32sArray>;
using IppUnsignedArrayTypes = testing::Types<Ipp8uArray, Ipp16uArray>;
using IppIntegerArrayTypes = testing::Types<Ipp8uArray, Ipp16uArray, Ipp32sArray>;
using IppNativeSupportedArrayType = testing::Types<Ipp8uArray, Ipp16uArray, Ipp32fArray>;

TYPED_TEST_CASE(IppSingleArrayTest, IppArrayTypes);

TYPED_TEST(IppSingleArrayTest, ScalarAddNoSaturation)
{
	using ippArrayType = typename TestFixture::ippArrayType;

	ippArrayType image1({ 16, 20 });
	image1.uniformDistribution(10, 100);

	float testValue = 32;

	auto testArray = image1 + testValue;
	auto baseIterator = image1.begin();

	for (auto tv : testArray)
	{
		auto v = *baseIterator++;
		ASSERT_EQ(int(v + testValue), int(tv));
	}

	baseIterator = image1.begin();
	testArray = image1 - 10;
	for (auto tv : testArray)
	{
		auto v = *baseIterator++;
		ASSERT_EQ(int(v - 10), int(tv));
	}

	//test = image1 / 22;
	//vect = test.toVector()[0];
	//for (auto i = 0; i < vect.size(); i++)
	//{
	//	int v = divideFinancial(vec1[i], 22ui16);
	//	ASSERT_EQ(v, int(vect[i]));
	//}

	baseIterator = image1.begin();
	testArray = image1 * 2;
	for (auto tv : testArray)
	{
		auto v = *baseIterator++;
		ASSERT_EQ(int(v * 2), int(tv));
	}
}

template<typename T>
struct IppIntegerArrayTest : public testing::Test
{
	// This allows the type to be used in the inherited class
	using ippArrayType = T;
};

TYPED_TEST_CASE(IppIntegerArrayTest, IppUnsignedArrayTypes);

TYPED_TEST(IppIntegerArrayTest, ScalarDivideSaturation)
{
	using ippArrayType = typename TestFixture::ippArrayType;
	ippArrayType image1({ 16, 20 });
	image1.uniformDistribution(0, 255);

	auto baseIterator = image1.begin();
	auto testArray = image1 / 22;
	for (auto tv : testArray)
	{
		auto v = *baseIterator++;
		v = divideFinancial(v, 22);
		ASSERT_EQ(v, int(tv));
	}
}

template<typename T>
struct MatIOTest : public testing::Test
{
	// This allows the type to be used in the inherited class
	using ippArrayType = T;

	void SetUp() override
	{
		// The name will be class <IppArray>
		auto fullName = string(typeid(T).name());
		typeName = fullName.substr(6);
	}

	std::string typeName;
};

TYPED_TEST_CASE(MatIOTest, IppNativeSupportedArrayType);

TYPED_TEST(MatIOTest, IppArrayReadWrite)
{
	ippArrayType baseImage({ 9, 10 });
	baseImage.iota();

	MtiPlanar<ippArrayType> planes = { baseImage, baseImage + 100, baseImage - 40 };
	ippArrayType image1;
	image1.copyFromPlanar(planes);
	auto imageRoi = image1({ 1, 1, 7, 8 });

	auto fileName = getTempFileName(typeName + "WriteTest.mat");
	MatIO::saveListToMatFile(fileName, image1, typeName + "Planer", baseImage, typeName + "Base", imageRoi, typeName + "PlanerRoi");
	MatIO::appendListToMatFile(fileName, MtiRect(1, 1, 7, 8), "MtiRect" + typeName);

	// read it back in
	ippArrayType baseImageRead;
	MatIO::readIppArray(fileName, baseImageRead, typeName + "Base");

	ASSERT_EQ(baseImage, baseImageRead);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
///  RGB to gray tests

template<typename T>
struct IppRGBToGrayTest : public testing::Test
{
	// This allows the type to be used in the inherited class
	using ippArrayType = T;

	void SetUp() override
	{
		// Create integer values similar to what is often sed
		Ipp16uArray testArray16u(testArray.getSize());
		testArray16u.uniformDistribution(0, 0xFFFF);
		testArray <<= testArray16u;

		// Compute truth in float
		Ipp32fArray testArray32f;
		testArray32f <<= testArray;
		auto planes = testArray32f.copyToPlanar();

		auto grayTruth32f = planes[0] * 0.299f + planes[1] * 0.587f + planes[2] * 0.114f;

		// Create truth back to type
		grayTruth <<= grayTruth32f;
	}

	ippArrayType testArray{ { 20, 15, 3 } };
	ippArrayType grayTruth;
};

TYPED_TEST_CASE(IppRGBToGrayTest, IppNativeSupportedArrayType);

TYPED_TEST(IppRGBToGrayTest, convertNTSC)
{
	using ippArrayType = typename TestFixture::ippArrayType;

	auto result = testArray.toGray();
	ASSERT_EQ(MtiSize({ result.getSize(), 1 }), result.getSize());

	auto it = grayTruth.begin();
	for (auto vTest : result)
	{
		auto vExpected = *it++;
		if (abs(vTest - vExpected) > 1)
		{
			EXPECT_EQ(vExpected, vTest);
		}
	}
	//ASSERT_EQ(grayTruth, result);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/// RGB generic convert

TYPED_TEST(IppRGBToGrayTest, convertGeneric)
{
	using ippArrayType = typename TestFixture::ippArrayType;
	float weights[] = { 0.299f,  0.587f, 0.114f };

	// First test the default is the same using NTSC coefficients
	// Note: on NTSC, the coefficients are internal

	auto result = testArray.toGray(weights);
	ASSERT_EQ(testArray.toGray(), testArray.toGray(weights));

	auto planes = testArray.copyToPlanar();
	float weights1[] = { 1, 0, 0};
	ASSERT_EQ(planes[0], testArray.toGray(weights1));

	float weights2[] = { 0, 1, 0 };
	ASSERT_EQ(planes[1], testArray.toGray(weights2));

	float weights3[] = { 0, 0, 1 };
	ASSERT_EQ(planes[2], testArray.toGray(weights3));
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
// Planar tests

TYPED_TEST(IppSingleArrayTest, ConvertToPlanar1)
{
	using ippArrayType = typename TestFixture::ippArrayType;
	auto rows = 40;
	auto cols = 151;

	MtiPlanar<ippArrayType> planes;
	auto n = 1;

	for (auto i : range(n))
	{
		planes.push_back(ippArrayType({ cols, rows }));
		planes.back().uniformDistribution(0, 255);
	}

	ippArrayType image({ cols, rows, n });
	image.copyFromPlanar(planes);

	auto backPlanes = image.copyToPlanar();
	ASSERT_EQ(n, int(backPlanes.size()));
	for (auto i : range(n))
	{
		ASSERT_EQ(planes[i], backPlanes[i]);
	}
}

TYPED_TEST(IppSingleArrayTest, ConvertToPlanar3)
{
	using ippArrayType = typename TestFixture::ippArrayType;
	auto rows = 40;
	auto cols = 151;

	MtiPlanar<ippArrayType> planes;

	for (auto i : range(3))
	{
		planes.push_back(ippArrayType({ cols, rows }));
		planes.back().uniformDistribution(0, 255);
	}

	ippArrayType image({ cols, rows, 3 });
	image.copyFromPlanar(planes);

	auto backPlanes = image.copyToPlanar();
	ASSERT_EQ(3, int(backPlanes.size()));
	for (auto i : range(3))
	{
		ASSERT_EQ(planes[i], backPlanes[i]);
	}
}

TYPED_TEST(IppSingleArrayTest, ConvertToPlanar4)
{
	using ippArrayType = typename TestFixture::ippArrayType;
	auto rows = 40;
	auto cols = 151;

	MtiPlanar<ippArrayType> planes;
	auto n = 4;

	for (auto i : range(n))
	{
		planes.push_back(ippArrayType({ cols, rows }));
		planes.back().uniformDistribution(0, 255);
	}

	ippArrayType image({ cols, rows, n });
	image.copyFromPlanar(planes);

	auto backPlanes = image.copyToPlanar();
	ASSERT_EQ(n, int(backPlanes.size()));
	for (auto i : range(n))
	{
		ASSERT_EQ(planes[i], backPlanes[i]);
	}
}

