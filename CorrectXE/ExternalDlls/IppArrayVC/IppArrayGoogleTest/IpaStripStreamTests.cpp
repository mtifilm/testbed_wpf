#include "pch.h"
#include "Windows.h"
#include "IpaStreamDataItem.h"
#include "IpaStripeStream.h"


std::vector<Ipp64f> _v;

// Pile of crap, this needs to be redone but since the interface is not
	// completed, just leave old crap here

		static void testFcn(const IppArray<Ipp8u> &rhs)
		{
			auto w = rhs.mean();
		}

		static void testFcn32f(const IppArray<Ipp32f> &rhs)
		{
			auto w = rhs.area();
		}

		void nonStaticTest(const IppArray<Ipp8u> &rhs)
		{
			_v = rhs.mean();
		}

		void nonStaticTest16u(IppArray<Ipp16u> &rhs)
		{
			_v = rhs.mean();
		};

		void nonStaticTest16u(IppArray<Ipp32s> &rhs)
		{
			_v = rhs.mean();
		};

		class IpaStripedStream : public ::testing::Test
		{
		protected:
			void SetUp() override
			{;
			}

#ifdef _DEBUG
			MtiSize bigSize = { 1024, 760 };
#else
			MtiSize bigSize = { 4096, 3112 };
#endif
			MtiSize smallSize = { 400, 200 };
		};

		TEST_F(IpaStripedStream, DataConversionTest)
		{
			// Incomplete, but no time for now
			IpaStreamDataItem data(364.45);
			ASSERT_EQ(364.45, data.getIpp64f());
			ASSERT_EQ(364.45f, data.getIpp32f());
			ASSERT_EQ(364, data.getIpp32s());
			ASSERT_EQ(364, int(data.getIpp16u()));
			ASSERT_EQ(108, int(data.getIpp8u()));

			IpaStreamDataItem dataf(364.85f);
			ASSERT_NEAR(364.85, dataf.getIpp64f(), 0.00001);
			ASSERT_EQ(364.85f, dataf.getIpp32f());
			ASSERT_EQ(364, dataf.getIpp32s());
			ASSERT_EQ(364, int(dataf.getIpp16u()));
			ASSERT_EQ(108, int(dataf.getIpp8u()));

			IpaStreamDataItem datai(-364);
			ASSERT_NEAR(-364.0, datai.getIpp64f(), 0.00001);
			ASSERT_EQ(-364.0f, datai.getIpp32f());
			ASSERT_EQ(-364, datai.getIpp32s());
			ASSERT_EQ(65172, int(datai.getIpp16u()));
			ASSERT_EQ(148, int(datai.getIpp8u()));
		}

		TEST_F(IpaStripedStream, CreateStreamTest)
		{
			//IpaStripeStream iss;
			//Ipp32fArray image({ 400, 200 });
			//Ipp8uArray image8({ 400, 200 });
			//image8.uniformDistribution(0, 200);
			//image <<= image8;

			//iss << image8;
			//iss << &testFcn;

			//std::invoke(testFcn, image8);

			//auto w = image8.mean();
			////iss << [](const Ipp8uArray &image8)
			////{
			////	auto w = image8.mean();
			////	std::wostringstream os;
			////};

			////iss << [this](const Ipp8uArray &image) 
			////          { 
			////	        _v = image.mean();
			////          };

			//auto x = _v;
		}

		template<class T>
		static void copyFrom(T &A)
		{
			auto w = A.mean();
		}

		TEST_F(IpaStripedStream, CreateStreamSimpleRunTest)
		{
			IpaStripeStream iss;
			Ipp8uArray image8(bigSize);
			image8.uniformDistribution(0, 200);

			Ipp8uArray i8({ 400, 200 });
			i8.uniformDistribution(100, 200);

			auto i16uArray = Ipp16uArray(image8);
			auto i32fArray = Ipp32fArray(i16uArray.getSize());

			CHRTimer hrt;
			i32fArray <<= image8;
			auto ss = hrt.elapsedMilliseconds();

			iss << i32fArray;
			iss << [=](Ipp32fArray &image)
			{
				auto roi = image.getRoi();;
				i16uArray(roi) <<= image;
			};

			hrt.start();
			iss.stripe();

			auto st = hrt.elapsedMilliseconds();

			std::wostringstream os;
			os << "IpaStripe Single threaded convert: " << ss << ", Multi threaded: " << st << ", ";
			os << " , speedup : " << ss / st;
	////		Microsoft::VisualStudio::CppUnitTestFramework::Logger::WriteMessage(os.str().c_str());

			iss << i32fArray;
			Ipp32fArray O32Array(i32fArray.getSize());
			O32Array.zero();
			auto *p = O32Array.data();

			iss << O32Array;

			auto *i = i32fArray.data();

			iss << [=](Ipp32fArray &in, Ipp32fArray &out)
			{
				out <<= in + 10;
			};

			iss.stripe();
		}

		TEST_F(IpaStripedStream, UnaryStreamStripeTest)
		{
			IpaStripeStream iss;
			Ipp16uArray source16u(bigSize);
			source16u.uniformDistribution(0, 200);

			auto source32f = Ipp32fArray(source16u);
			Ipp32fArray i32fArray(source16u.getSize());
			Ipp16uArray i16uArray(source16u.getSize());

			i32fArray.zero();
			i16uArray.zero();

			iss << source32f
				<< [&](Ipp32fArray &image)
			{
				auto roi = image.getRoi();;
				i16uArray(roi) <<= image;
			};

			std::wostringstream os;
			os << std::endl;
			CHRTimer hrt;

			auto numberOfThreads = std::thread::hardware_concurrency();

			hrt.start();
			iss.stripe();
			os << "Convert, strips same as threads: " << hrt.intervalMilliseconds() << std::endl;

			ASSERT_TRUE(source16u == i16uArray) << "Threads == stripes test";
			i16uArray.zero();
			iss << source32f;
			iss << [&](Ipp32fArray &image)
			{
				auto roi = image.getRoi();;
				i16uArray(roi) <<= image;
			};

			hrt.start();
			iss.stripe(numberOfThreads - 2);
			os << "Convert, 2 Less strips than threads: " << hrt.intervalMilliseconds() << std::endl;

			ASSERT_TRUE(source16u == i16uArray) << "Threads > stripes test";
			i16uArray.zero();
			iss << source32f;
			iss << [&](Ipp32fArray &image)
			{
				auto roi = image.getRoi();;
				i16uArray(roi) <<= image;
			};

			hrt.start();
			iss.stripe(2 * numberOfThreads);
			os << "Convert, twice strips than threads: " << hrt.intervalMilliseconds() << std::endl;
			ASSERT_TRUE(source16u == i16uArray) << "Threads < stripes test";

			hrt.start();
			i16uArray <<= source32f;
			os << "Convert, single threaded: " << hrt.intervalMilliseconds() << std::endl;

		}

		TEST_F(IpaStripedStream, BinaryStreamStripeTest)
		{
			IpaStripeStream iss;
			Ipp16uArray source16u(bigSize);
			source16u.uniformDistribution(0, 200);

			auto source32f = Ipp32fArray(source16u);
			auto output32f = Ipp32fArray(source16u.getSize());

			output32f.zero();

			iss << source32f << output32f;

			iss << [](Ipp32fArray &in, Ipp32fArray &out)
			{
				// This is a copy and add in place, no allocation
				out <<= in;
				out += 10;
			};

			std::wostringstream os;
			os << std::endl;

			CHRTimer hrt;
			hrt.start();
			iss.stripe();
			os << "Copy & add inplace, striped: " << hrt.intervalMilliseconds() << std::endl;

			auto source10 = Ipp32fArray(source32f.getSize());
			hrt.start();
			source10 <<= source32f;
			source10 += 10;
			os << "Copy & add in-place, single threaded: " << hrt.intervalMilliseconds() << std::endl;

			ASSERT_TRUE(source10 == output32f, L"Add and copy");

			iss << source32f
				<< output32f
				<< [](Ipp32fArray &in, Ipp32fArray &out)
			{
				// This allocates an array on RHS before copy
				out <<= in + 10;
			};

			hrt.start();
			iss.stripe();
			os << "Add then copy, striped: " << hrt.intervalMilliseconds() << std::endl;

			hrt.start();
			source10 = source32f + 10;
			os << "Add then copy, single threaded: " << hrt.intervalMilliseconds() << std::endl;

			iss << source32f << output32f;
			iss << [](Ipp32fArray &in, Ipp32fArray &out)
			{
				// This is a copy and add in place, preallocation
				for (auto r = 0; r < in.getHeight(); r++)
				{
					auto inp = in.getRowPointer(r);
					auto outp = out.getRowPointer(r);
					for (auto c = 0; c < in.getWidth(); c++)
					{
						*outp++ = *inp++ + 10;
					}
				}
			};

			hrt.start();
			iss.stripe();
			os << "Brute add, prealloced memory, stripped: " << hrt.intervalMilliseconds() << std::endl;

			output32f <<= source32f;
			hrt.start();

			iss << output32f;
			iss << [&](Ipp32fArray &in)
			{
				in += 10;
			};

			iss.stripe();

			os << "add inplace, stripped: " << hrt.intervalMilliseconds() << std::endl;
			ASSERT_TRUE(source10 == output32f, L"Add and copy");
//			Logger::WriteMessage(os.str().c_str());
		}

		std::tuple<int, string, float> stupid(double v)
		{
			std::ostringstream os;
			os << v;
			return std::make_tuple((int)v, os.str(), v / 33);
		}

		TEST_F(IpaStripedStream, JobStripeTest)
		{
			IpaStripeStream iss;
			std::map<int, int> map;
			int maxJob = -1;
			std::mutex mutexLock;

			iss << [&](int jobNumber)
			{
				std::lock_guard<std::mutex> lock(mutexLock);
				maxJob = std::max<int>(maxJob, jobNumber);
				map[jobNumber] = jobNumber;
			};

			iss << 24;
			iss.stripe();
			ASSERT_EQ(23, maxJob);
			for (auto j : range(24))
			{
				ASSERT_EQ(j, map[j]);
			}
		}

		TEST_F(IpaStripedStream, JobRunTest)
		{
			// This tests the execution of run in a stripe stream

			IpaStripeStream iss;
			std::map<int, int> map;
			int maxJob = -1;
			std::mutex mutexLock;

			iss << [&](int jobNumber)
			{
				std::lock_guard<std::mutex> lock(mutexLock);
				maxJob = std::max<int>(maxJob, jobNumber);
				map[jobNumber] = jobNumber;
			};

			iss << 24;
			iss.run();
			ASSERT_EQ(23, maxJob);
			for (auto j : range(24))
			{
				ASSERT_EQ(j, map[j]);
			}
		}

		TEST_F(IpaStripedStream, UnaryJobStripeTest)
		{
			IpaStripeStream iss;
			std::map<int, int> map;
			int maxJob = -1;
			std::mutex mutexLock;
			Ipp32fArray testArray({ 400, 300 });
			Ipp32fArray truth(testArray.getSize());
			truth.iota();

			iss << testArray;
			iss << [&](Ipp32fArray &in, int jobNumber)
			{
				// Strip data does not need to be locked
				in.iota();
				in += in.getRoi().y * in.getWidth();

				// We need to lock global variables
				std::lock_guard<std::mutex> lock(mutexLock);
				maxJob = std::max<int>(maxJob, jobNumber);
				map[jobNumber] = jobNumber;
			};

			const int strips = 24;
			iss.stripe(strips, 4);
			ASSERT_EQ(strips-1, maxJob);

			// Just check they were all done with expected jobNumber
			for (auto j : range(strips))
			{
				ASSERT_EQ(j, map[j]);
			}

			compareIppArray(truth, testArray);
			ASSERT_EQ(truth, testArray);
		}

		TEST_F(IpaStripedStream, RoiStripeTest)
		{
			MtiRect roi = { 96, bigSize.height/3, bigSize.width - 200, bigSize.height/2 };
			IpaStripeStream iss;

			// fill in an ROI with an increasing set of number
			Ipp32sArray truth(bigSize);
			truth.set({ 0x7FFF });
			truth(roi).iota();
			truth(roi) += roi.y;

			Ipp32sArray testArray(bigSize);
			testArray.set({ 0x7FFF });

			iss << roi
				<< [&](const MtiRect &stripedRoi)
			{
				auto stripedArray = testArray(stripedRoi);
				stripedArray.iota();
				stripedArray += (stripedRoi.y - roi.y) * stripedRoi.width + roi.y;
			};

			iss.stripe();
			ASSERT_TRUE(truth == testArray);
		}

		TEST_F(IpaStripedStream, RoiStripeJobNumberTest)
		{
			IpaStripeStream iss;

			// fill in an ROI with an increasing set of number
			Ipp16uArray truth({ 512, 396 });
			Ipp16uArray testArray(truth.getSize());
			testArray.set({ 0xFFFF });

			int totalJobs = 8;
			vector<IppiRect> stripedRois(totalJobs);

			iss << testArray.getSize()
				<< [&testArray, &stripedRois](const IppiRect &stripedRoi, int jobNumber)
			{
				stripedRois[jobNumber] = stripedRoi;
				testArray(stripedRoi).set({ Ipp16u(jobNumber) });
			};

			// This is not dynamic, so use a fixed number of jobs
			// of course we could use a thread safe map here instead of a vector 
			// but let us not do overkill
			iss.stripe(totalJobs);

			for (auto i = 0; i < totalJobs; i++)
			{
				auto stripeRoi = stripedRois[i];
				auto stripeTruth = truth(stripeRoi);
				stripeTruth.set({ Ipp16u(i) });
				ASSERT_TRUE(stripeTruth == testArray(stripeRoi));
			}
		}

		TEST_F(IpaStripedStream, SizeStripeTest)
		{
			Ipp16uArray baseArray(bigSize);
			baseArray.uniformDistribution(100, 200);

			// create an roi
			MtiRect testRoi = { 90, 112, bigSize.width - 180, bigSize.height - 200 };
			auto testArray = baseArray(testRoi);

			Ipp32fArray testArray32f(testArray.getSize());
			IpaStripeStream iss;
			iss << testArray.getSize()
				<< [&](const IppiRect &roi)
			{
				testArray32f(roi) <<= testArray(roi);
			};

			iss.stripe();

			Ipp32fArray baseArray32f;
			baseArray32f <<= baseArray;

			ASSERT_TRUE(baseArray32f(testRoi) == testArray32f);
		}



#include <execution>
		TEST_F(IpaStripedStream, ValuePushTest)
		{

			// permitting parallel execution
			Ipp32fArray test({ 1000, 500 });
			test.uniformDistribution(-100000, 100000);


			auto v = test.toVector()[0];
			CHRTimer hrt;
			std::sort(std::execution::seq, v.begin(), v.end());
			auto t1 = hrt.intervalMilliseconds();

			v = test.toVector()[0];
			hrt.start();
			std::sort(std::execution::par_unseq, v.begin(), v.end());
			auto t2 = hrt.intervalMilliseconds();
			auto x = t2 - t1;

			v = test.toVector()[0];
			hrt.start();
			auto t = test.medianQuick(v);
			auto t3 = hrt.intervalMilliseconds();

			IpaStripeStream iss;
			iss << unsigned char(7);
			iss << short(-33);
			iss << 3;
			iss << 33.4f;
			iss << 64.3;
			// TODO? iss << long long(1234);
			iss << IppiSize({ 10, 20 });
			iss << IppiRect({ 2, 3, 10, 20 });
		}


