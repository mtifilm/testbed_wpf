#include "pch.h"
#include "MtiAncillaryTemplates.h"

struct InflateData
{
	MtiRect startRoi;
	MtiRect expectedRoi;
	vector<int> args;
};

struct InflateDataTest : testing::TestWithParam<InflateData>
{
	InflateDataTest() {}
};

TEST_P(InflateDataTest, ParameterInflateTest)
{
	auto as = GetParam();
	auto v = as.args;

	// This is an experiment on how to use data with parameters
	// not efficient
	switch (v.size())
	{
	case 1:
		EXPECT_EQ(as.expectedRoi, as.startRoi.inflate(v[0]));
		break;

	case 2:
		EXPECT_EQ(as.expectedRoi, as.startRoi.inflate(v[0], v[1]));
		break;

	case 4:
		EXPECT_EQ(as.expectedRoi, as.startRoi.inflate(v[0], v[1], v[2], v[3]));
		break;

	default:
		EXPECT_TRUE(false);
		break;
	}
}

INSTANTIATE_TEST_CASE_P(Default, InflateDataTest,
	testing::Values(InflateData{ { 7, 8, 20, 30 }, { 5, 6, 24, 34 },  {2} },
		InflateData{ { 7, 8, 20, 30 }, { 9, 10, 16, 26 }, {-2} },
		InflateData{ { 7, 8, 20, 30 }, { 8, 5, 18, 36  },  {-1, 3} },
		InflateData{ { 7, 8, 20, 30 }, { 8, 5, 23, 31  },  {-1, 3, 4, -2} },
		InflateData{ { 7, 8, 20, 30 }, { -3, -2, 40, 50  },  {10} },
		InflateData{ { 7, 8, 20, 30 }, { 22, 23, -10, 0  },  {-15} }
));

////////////////////////////////////////////////////////

struct ContainsPointData
{
	MtiRect roi;
	MtiPoint point;
	bool contains;
};

// Class to test
typedef testing::TestWithParam<ContainsPointData> ContainsPointTest;

TEST_P(ContainsPointTest, ParameterInflateTest)
{
	auto as = GetParam();
	EXPECT_EQ(as.contains, as.roi.contains(as.point));
}

const MtiRect containsRoi{ 3, 4, 10, 20 };
INSTANTIATE_TEST_CASE_P(Default, ContainsPointTest,
	testing::Values
	(
		ContainsPointData{ containsRoi, { 3 + 5, 4 + 10} , true },
		ContainsPointData{ containsRoi, { 3 + 1, 4 + 1} , true },
		ContainsPointData{ containsRoi, { 3 + 5, 4} , true },
		ContainsPointData{ containsRoi, { 3, 4 + 10} , true },
		ContainsPointData{ containsRoi, containsRoi.tl() , true },
		ContainsPointData{ containsRoi, containsRoi.bl() , false },
		ContainsPointData{ containsRoi, containsRoi.tr() , false },
		ContainsPointData{ containsRoi, containsRoi.br() , false },
		ContainsPointData{ containsRoi, { 5, 44 } , false },
		ContainsPointData{ containsRoi, { 44, 5 } , false }
	)
);

//////////////////////////////////////////////////////////////////////

struct RoiOperationData
{
	MtiRect roi0;
	MtiRect roi1;
	bool suceeds;
};

// Classifier fixture
typedef testing::TestWithParam<RoiOperationData> RoiOperationTest;

// Actual test
TEST_P(RoiOperationTest, RectEqualsTest)
{
	auto as = GetParam();
	auto isEqual = as.suceeds;
	EXPECT_EQ(isEqual, as.roi0 == as.roi1) << as.roi0 << " must equal " << as.roi1;
}

const MtiRect equalRoi{ 2, 0, 10, 20 };

INSTANTIATE_TEST_CASE_P(Default, RoiOperationTest,
	testing::Values
	(
		RoiOperationData{ equalRoi, { 2, 0, 10, 20 }, true },
		RoiOperationData{ equalRoi, { 2, 1, 10, 20 }, false }
	)
);

TEST(PointOperationTest, PointEqualsTest)
{
	EXPECT_EQ(MtiPoint({ 2, 4 }), MtiPoint({ 2, 4 }));
	EXPECT_NE(MtiPoint({ 2, 3 }), MtiPoint({ 2, 4 }));
}
//////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////

struct RoiIntersectionData
{
	MtiRect expected;
	MtiRect roi0;
	MtiRect roi1;
};

// Classifier fixture
typedef testing::TestWithParam<RoiIntersectionData> RoiIntersectionTest;

// Actual test
TEST_P(RoiIntersectionTest, IntersectionTest)
{
	auto as = GetParam();
	EXPECT_EQ(as.expected, as.roi0 & as.roi1);
}

const MtiRect interRoi0{ 3, 4, 10, 20 };
const MtiRect interRoi1{ 2, 0, 10, 20 };

INSTANTIATE_TEST_CASE_P(Default, RoiIntersectionTest,
	testing::Values
	(
		RoiIntersectionData{ interRoi0, interRoi0, interRoi0 },
		RoiIntersectionData{ interRoi0, interRoi0, interRoi0.inflate(4) },
		RoiIntersectionData{ interRoi0.inflate(-3), interRoi0, interRoi0.inflate(-3) },
		RoiIntersectionData{ { 0, 0, 0, 0 }, interRoi0, { 50, 40, 10, 10 } },
		RoiIntersectionData{ { 3, 4, 7, 8 }, interRoi0, { 2, 0, 8, 12 } },
		RoiIntersectionData{ { 3, 5, 10, 19 }, interRoi0, interRoi0.inflate(2, -1, 2, 2) },
		RoiIntersectionData{ { 0, 0, 0, 0 }, interRoi0, { 30, 40, 10, 10 } }
	)
);

//////////////////////////////////////////////////////////////////////////

// Classifier fixture
typedef testing::TestWithParam<RoiOperationData> RoiIsEmptyTest;

// Actual test
TEST_P(RoiIsEmptyTest, RoiIsEmpty)
{
	auto as = GetParam();
	auto isEqual = as.suceeds;
	EXPECT_EQ(isEqual, as.roi0.isEmpty()) << "Test roi: " << as.roi0;
}

INSTANTIATE_TEST_CASE_P(Default, RoiIsEmptyTest,
	testing::Values
	(
		RoiOperationData{ { 3, 4, 10, 20 }, {}, false },
		RoiOperationData{ { -3, 4, 1, 1 }, {}, false },
		RoiOperationData{ {  0, 0, 1, 0 }, {}, true },
		RoiOperationData{ { 0, 0, -10, -10}, {}, true },
		RoiOperationData{ { 0, 0, 0, -20 }, {}, true }
	)
);

//////////////////////////////////////////////////////////////////////////
// Classifier fixture

struct FactorRectsData
{
	MtiRect probe;
	FactorRects factors;
	
	FactorRectsData(const MtiRect &p, const vector<MtiRect> &values)
	{
		probe = p;
		factors = FactorRects(values);
	}
};

typedef testing::TestWithParam<FactorRectsData> FactorRectsTest;

// Actual test
TEST_P(FactorRectsTest, PaddingRect)
{
	auto truth = GetParam();
	auto full = truth.factors.full;

	// We are assuming probles are relative
	FactorRects test(full, truth.probe, true);
	// We need to test this also
	// auto test = full % truth.probe;
	EXPECT_EQ(truth.factors, test) << "Full roi: " << full << ", center " << truth.probe;
}

INSTANTIATE_TEST_CASE_P(Default, FactorRectsTest,
	testing::Values
	(
		FactorRectsData{ { 2, 3, 5, 4 },
			{ { 0, 0, 11, 9 }, { 0, 0, 11, 3 }, { 0, 0, 2, 9 }, { 7, 0, 4, 9 }, { 0, 7, 11, 2 },
			{ 0, 0, 2, 3 }, { 2, 0, 5, 3 }, { 7, 0, 4, 3 },
			{ 0, 3, 2, 4 }, { 2, 3, 5, 4 }, { 7, 3, 4, 4 },
			{ 0, 7, 2, 2 }, { 2, 7, 5, 2 }, { 7, 7, 4, 2 } }
		},
		FactorRectsData{ { -2, -3, 6, 5 },
			{ { 0, 0, 11, 9 } , { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 4, 0, 7, 9 }, { 0, 2, 11, 7 },
			{ 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 },
			{ 0, 0, 0, 0 }, { 0, 0, 4, 2 }, { 4, 0, 7, 2 },
			{ 0, 0, 0, 0 }, { 0, 2, 4, 7 }, { 4, 2, 7, 7 } }
		},
		FactorRectsData{ { 2, -3, 6, 5 },
			{ { 0, 0, 11, 9 } , { 0, 0, 0, 0 }, { 0, 0, 2, 9 }, { 8, 0, 3, 9 }, { 0, 2, 11, 7 },
			{ 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 },
			{ 0, 0, 2, 2 }, { 2, 0, 6, 2 }, { 8, 0, 3, 2 },
			{ 0, 2, 2, 7 }, { 2, 2, 6, 7 }, { 8, 2, 3, 7 } }
		},
		FactorRectsData{ { 6, -2, 6, 5 },
			{ { 0, 0, 11, 9 } , { 0, 0, 0, 0 }, { 0, 0, 6, 9 }, { 0, 0, 0, 0 }, { 0, 3, 11, 6 },
			{ 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 },
			{ 0, 0, 6, 3 }, { 6, 0, 5, 3 }, { 6, 0, 0, 3 },
			{ 0, 3, 6, 6 }, { 6, 3, 5, 6 }, { 11, 3, 0, 6 } }
		},
		FactorRectsData{ { -2, 3, 6, 4 },
			{{ 0, 0, 11, 9 } , { 0, 0, 11, 3 }, { 0, 0, 0, 0 }, { 4, 0, 7, 9 }, { 0, 7, 11, 2 },
			{ 0, 0, 0, 0 }, { 0, 0, 4, 3 }, { 4, 0, 7, 3 },
			{ 0, 0, 0, 4 }, { 0, 3, 4, 4 }, { 4, 3, 7, 4 },
			{ 0, 0, 0, 0 }, { 0, 7, 4, 2 }, { 4, 7, 7, 2 } }
		},
		FactorRectsData{ { 7, 3, 6, 4 },
			{ { 0, 0, 11, 9 } , { 0, 0, 11, 3 }, { 0, 0, 7, 9 }, { 4, 0, 0, 9 }, { 0, 7, 11, 2 },
			{ 0, 0, 7, 3 }, { 7, 0, 4, 3 }, { 0, 0, 0, 0 },
			{ 0, 3, 7, 4 }, { 7, 3, 4, 4 }, { 0, 0, 0, 0 },
			{ 0, 7, 7, 2 }, { 7, 7, 4, 2 }, { 0, 0, 0, 0 } }
		},
		FactorRectsData{ { -2, 6, 6, 5 },
			{ { 0, 0, 11, 9 } , { 0, 0, 11, 6 }, { 0, 0, 0, 0 }, { 4, 0, 7, 9 }, { 0, 0, 0, 0 },
			{ 0, 0, 0, 0 }, { 0, 0, 4, 6 }, { 4, 0, 7, 6 },
			{ 0, 0, 0, 0 }, { 0, 6, 4, 3 }, { 4, 6, 7, 3 },
			{ 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 } }
		},
		FactorRectsData{ { 3, 6, 6, 5 },
			{{ 0, 0, 11, 9 } , { 0, 0, 11, 6 }, { 0, 0, 3, 9 }, { 9, 0, 2, 9 }, { 0, 0, 0, 0 },
			{ 0, 0, 3, 6 }, { 3, 0, 6, 6 }, { 9, 0, 2, 6 },
			{ 0, 6, 3, 3 }, { 3, 6, 6, 3 }, { 9, 6, 2, 3 },
			{ 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 } }
		},
		FactorRectsData{ { 7, 6, 6, 5 },
			{ { 0, 0, 11, 9 } , { 0, 0, 11, 6 }, { 0, 0, 7, 9 }, { 9, 0, 0, 9 }, { 0, 0, 0, 0 },
			{ 0, 0, 7, 6 }, { 7, 0, 4, 6 }, { 0, 0, 0, 0 },
			{ 0, 6, 7, 3 }, { 7, 6, 4, 3 }, { 9, 6, 0, 0 },
			{ 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 } }
		},
		FactorRectsData{ { -10, -10, 21, 29 },
			{ { 0, 0, 11, 9 } , { 0, 0, 0, 0 }, {  0, 0, 0, 0 }, {  0, 0, 0, 0 }, { 0, 0, 0, 0 },
			{ 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 },
			{ 0, 0, 0, 0 }, { 0, 0, 11, 9}, { 0, 0, 0, 0 },
			{ 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 } }
		}
	)
);

//////////////////////////////////////////////////////////////////////////

TEST(FactorRectsTest, PaddingTestRelativeSimple)
{
	FactorRects truth0{ { { 0, 0, 11, 9 }, { 0, 0, 11, 3 }, { 0, 0, 2, 9 }, { 7, 0, 4, 9 }, { 0, 7, 11, 2 },
		{ 0, 0, 2, 3 }, { 2, 0, 5, 3 }, { 7, 0, 4, 3 },
		{ 0, 3, 2, 4 }, { 2, 3, 5, 4 }, { 7, 3, 4, 4 },
		{ 0, 7, 2, 2 }, { 2, 7, 5, 2 }, { 7, 7, 4, 2 } } };

	FactorRects test0(truth0.full, truth0.center, true);
	EXPECT_EQ(truth0, test0) << "Full roi: " << truth0.full << ", center " << truth0.center;
}

TEST(FactorRectsTest, PaddingTestAbsoluteSimple)
{
	// This is an action often used in processing edges
	MtiRect fullRoi(0, 0, 250, 172);
	MtiRect edgeRoi(-44, -32, 100, 128);
	auto validRoi = fullRoi & edgeRoi;

	auto test0 =edgeRoi % validRoi;

	FactorRects truth0{ { { -44, -32, 100, 128 }, { -44, -32, 100, 32 }, { 0, 0, 2, 9 }, { 7, 0, 4, 9 }, { 0, 7, 11, 2 },
	{ 0, 0, 2, 3 }, { 2, 0, 5, 3 }, { 7, 0, 4, 3 },
	{ 0, 3, 2, 4 }, { 2, 3, 5, 4 }, { 7, 3, 4, 4 },
	{ 0, 7, 2, 2 }, { 2, 7, 5, 2 }, { 7, 7, 4, 2 } } };
}


TEST(MtiRect, MtiSizeCastTest)
{
	MtiRect base(3, 4, 7, 18);
	const auto size = MtiSize(base);
	ASSERT_EQ(base.width, size.width);
	ASSERT_EQ(base.height, size.height);
}

TEST(MtiRect, MtiPointCastTest)
{
	MtiRect base(3, 4, 7, 18);
	const auto size = MtiPoint(base);
	ASSERT_EQ(base.x, size.x);
	ASSERT_EQ(base.y, size.y);
}

TEST(MtiRect, ExpandTo50PercentTest)
{
	// This is an ad hoc test.  Two rectangles are created, large call target, the other probe
	// the intersection is computed and if the intersection is less than 50% of probe, the
	// probe is expanded so it is 50%
	// This has to do with how to mirror a probe and not general purpose
	// We really only care about zero based(relative) rectangles
	// Should be a parameter based test

	MtiRect target = { 0, 0, 356, 256 };

	// ul
	{
		MtiRect probe = { -72, -120, 128, 172 };
		auto[expandedProbe, validProbe] = expandTo50Percent(target, probe);
		ASSERT_EQ(MtiRect(probe.x, probe.y, 144, 240), expandedProbe);
	}

	// uc
	{
		MtiRect probe = { 65, -72, 172, 128 };
		auto[expandedProbe, validProbe] = expandTo50Percent(target, probe);
		ASSERT_EQ(MtiRect(probe.x, probe.y, probe.width, 144), expandedProbe);
	}

	// ur
	{
		MtiRect probe = { 292, -72, 172, 128 };
		auto[expandedProbe, validProbe] = expandTo50Percent(target, probe);
		ASSERT_EQ(MtiRect(248, probe.y, 216, 144), expandedProbe);
	}

	// cl
	{
		MtiRect probe = { -72, 64, 128, 172 };
		auto[expandedProbe, validProbe] = expandTo50Percent(target, probe);
		ASSERT_EQ(MtiRect(probe.x, probe.y, 144, probe.height), expandedProbe);
	}

	// cr
	{
		MtiRect probe = { 292, 64, 172, 128 };
		auto[expandedProbe, validProbe] = expandTo50Percent(target, probe);
		ASSERT_EQ(MtiRect(248, probe.y, 216, probe.height), expandedProbe);
	}

	// ll
	{
		MtiRect probe = { -72, 192, 128, 172 };
		auto[expandedProbe, validProbe] = expandTo50Percent(target, probe);
		ASSERT_EQ(MtiRect(probe.x, 148, 144, 216), expandedProbe);
	}

	// lc
	{
		MtiRect probe = { 64, 256 - 64, 128, 172 };
		auto [expandedProbe, validProbe] = expandTo50Percent(target, probe);
		ASSERT_EQ(MtiRect(probe.x, 148, probe.width, 216), expandedProbe); 
	}

	// lr
	{
		MtiRect probe = { 292, 256 - 64, 172, 172 };
		auto [expandedProbe, validProbe] = expandTo50Percent(target, probe);
		ASSERT_EQ(MtiRect(248, 148, 216, 216), expandedProbe);
	}

	////// Do one with no adjustments needed
	// uc (no adjustment)
	{
		MtiRect probe = { 64, -64, 128, 172 };
		auto [expandedProbe, validProbe] = expandTo50Percent(target, probe);
		ASSERT_EQ(probe, expandedProbe);
	}
}