#include "pch.h"

#pragma warning(push)
#pragma warning( disable : 4244)

TEST(Ipp8uTest, Ipp8uSetTest)
{
	Ipp8uArray image({ 20, 10, 3 });
	image.set({ 1, 2, 3 });

	for (auto r = 0; r < image.getHeight(); r++)
	{
		auto rp = image.getRowPointer(r);
		for (auto c = 0; c < image.getWidth(); c++)
		{
			ASSERT_EQ(1, int(*rp++));
			ASSERT_EQ(2, int(*rp++));
			ASSERT_EQ(3, int(*rp++));
		}
	}
}

TEST(Ipp8uTest, Resize8uDownTest)
{
	auto width = 64 * 40;
	auto height = 64 * 10;
	Ipp8uArray dstArray({ width, height, 3 });
	dstArray.iota();

	auto smallArray = dstArray.resize({ width / 4, height / 4 });
	ASSERT_EQ(smallArray.getWidth(), width / 4);
	ASSERT_EQ(smallArray.getHeight(), height / 4);
}

TEST(Ipp8uTest, Resize8uUpTest)
{
	auto width = 500;
	auto height = 200;
	Ipp8uArray dstArray({ width, height, 3 });
	dstArray.iota();

	auto smallArray = dstArray.resize({ width * 4, height * 4 });
	ASSERT_EQ(smallArray.getWidth(), width * 4);
	ASSERT_EQ(smallArray.getHeight(), height * 4);
}

TEST(Ipp8uTest, Ipp8uSaturationArrayTest)
{
	Ipp8uArray image1({ 16, 20 });
	image1.iota();

	image1 = image1 + 500.0f;
	const int v1 = image1[{10, 10}];
	ASSERT_EQ(0xff, v1);

	image1 = image1 - 10;
	const int v2 = image1[{10, 10}];
	ASSERT_EQ(0xf5, v2);

	image1.set({ 11, });
	image1 = image1 - 20;
	const int v3 = image1[{10, 10}];
	ASSERT_EQ(0, v3);
}

TEST(Ipp8uTest, Ipp8uCopyRoiArrayTest)
{
	Ipp8uArray image1({ 10, 13, 3 });
	image1.iota();

	// Do all
	auto image2 = image1.duplicate();
	ASSERT_EQ(image1.getHeight(), image2.getHeight());
	ASSERT_EQ(image1.getWidth(), image2.getWidth());

	ASSERT_TRUE(image1 == image2);
	ASSERT_FALSE(image1 != image2);

	// Clones must not have same data
	image1.set({ 33, 34, 35 });
	ASSERT_TRUE(image1 != image2);
	ASSERT_FALSE(image1 == image2);

	// Do a sub-image
	image2 = image1.duplicate({ 2, 3, 5, 6 });

	// fucking width and height swapped with rows and cols
	ASSERT_EQ(6, image2.getHeight());
	ASSERT_EQ(5, image2.getWidth());

	// Truth is truly ugly			
	auto truth = image1.toVector({ 2, 3, 5, 6 });
	auto test = image2.toVector();

	for (auto comp = 0; comp < image1.getComponents(); comp++)
	{
		AssertVectorEqual<Ipp8u>(truth[comp], test[comp]);
	}
}

TEST(Ipp8uTest, Ipp8uArithmiticInPlaceArrayTest)
{
	Ipp8uArray image1({ 10, 13 });
	image1.iota();
	image1 = image1 + 1.0f;

	Ipp8uArray image2({ 10, 13 });
	image2.set({ 0x09 });

	auto image1Orig = image1.duplicate();

	image1 += image2;
	for (auto r : image1.rowRange())
	{
		auto rp3 = image1.getRowPointer(r);
		auto rp2 = image1Orig.getRowPointer(r);
		auto rp1 = image2.getRowPointer(r);

		for (auto c : image1.colRange())
		{
			int rv1 = *rp1++;
			int rv2 = *rp2++;
			int rv3 = *rp3++;

			ASSERT_EQ(min(0xff, rv1 + rv2), int(rv3));
		}
	}

	image1 = image1Orig.duplicate();
	image1 -= image2;
	for (auto r : image1.rowRange())
	{
		auto rp3 = image1.getRowPointer(r);
		auto rp2 = image1Orig.getRowPointer(r);
		auto rp1= image2.getRowPointer(r);

		for (auto c : image1.colRange())
		{
			int rv1 = *rp1++;
			int rv2 = *rp2++;
			int rv3 = *rp3++;

			ASSERT_EQ(max(0, rv2 - rv1), int(rv3));
		}
	}

	image1 = image1Orig.duplicate();
	image1 *= image2;
	for (auto r : image1.rowRange())
	{
		auto rp3 = image1.getRowPointer(r);
		auto rp2 = image1Orig.getRowPointer(r);
		auto rp1= image2.getRowPointer(r);

		for (auto c : image1.colRange())
		{
			int rv1= *rp1++;
			int rv2 = *rp2++;
			int rv3 = *rp3++;

			ASSERT_EQ(min(0xFF, rv2 * rv1), int(rv3));
		}
	}

	image1 = image1Orig.duplicate();
	image2.set({ 21, });
	image1 /= image2;
	for (auto r : image1.rowRange())
	{
		auto rp3 = image1.getRowPointer(r);
		auto rp2 = image1Orig.getRowPointer(r);
		auto rp1= image2.getRowPointer(r);

		for (auto c : image1.colRange())
		{
			int rv1= *rp1++;
			int rv2 = *rp2++;
			int rv3 = *rp3++;

			std::wostringstream os;
			os << int(rv2) << "/" << int(rv1) << " <> " << int(rv3);
			ASSERT_EQ(int(float(rv2) / rv1+ 0.5f), int(rv3), os.str().c_str());
		}
	}
}

TEST(Ipp8uTest, Ipp8uArithmiticInPlaceOverlapArrayTest)
{
	Ipp8uArray image({ 25, 20 });
	image.iota();
	ASSERT_FALSE(image.isDirty());

	auto i1 = image({ 0, 0, 10, 9 });
	auto i2 = image({ 1, 1, 10, 9 });

	ASSERT_FALSE(i2.isDirty());
	ASSERT_FALSE(i2.isDirty());

	Ipp8uArray i2Orig;
	i2Orig <<= i2;
	i1 += i2;

	ASSERT_FALSE(i1.isDirty());
	ASSERT_TRUE(i2.isDirty());

	// To work with an invalid array one must reinstate
	i2.reinstate();
	ASSERT_TRUE(i2 != i2Orig);

	auto i3 = image({ 10, 9, 10, 9 });

	Ipp8uArray i3Orig;
	i3Orig <<= i3;

	i1 += i3;
	ASSERT_TRUE(i3 == i3Orig);
	ASSERT_FALSE(i1.isDirty());
	ASSERT_FALSE(i3.isDirty());
}

TEST(Ipp8uTest, Ipp8uArithmiticArrayTest)
{
	Ipp8uArray image1({ 10, 13 });
	image1.iota();
	image1 = image1 + 1.0f;

	Ipp8uArray image2({ 10, 13 });
	image2.set({ 0xF0 });

	auto image3 = image1 + image2;
	for (auto r : image3.rowRange())
	{
		auto rp1= image1.getRowPointer(r);
		auto rp2 = image2.getRowPointer(r);
		auto rp3 = image3.getRowPointer(r);

		for (auto c : image3.colRange())
		{
			int rv1= *rp1++;
			int rv2 = *rp2++;
			int rv3 = *rp3++;

			ASSERT_EQ(min(0xff, rv1+ rv2), int(rv3));
		}
	}

	image3 = image1 - image2;
	for (auto r : image3.rowRange())
	{
		auto rp1= image1.getRowPointer(r);
		auto rp2 = image2.getRowPointer(r);
		auto rp3 = image3.getRowPointer(r);

		for (auto c : image3.colRange())
		{
			int rv1= *rp1++;
			int rv2 = *rp2++;
			int rv3 = *rp3++;

			ASSERT_EQ(max(0, rv1- rv2), int(rv3));
		}
	}

	auto test = image1 + image2;
	auto resultVector = test.toVector()[0];
	auto lhsVector = image1.toVector()[0];
	auto rhsVector = image2.toVector()[0];
	for (auto i = 0; i < resultVector.size(); i++)
	{
		int t = max(0, min(0xFF, lhsVector[i] + rhsVector[i]));
		ASSERT_EQ(t, int(resultVector[i]));
	}

	test = image1 - image2;
	resultVector = test.toVector()[0];
	for (auto i = 0; i < resultVector.size(); i++)
	{
		int t = int(max(0, min(0xFF, lhsVector[i] - rhsVector[i]) - 0.0001));
		ASSERT_EQ(t, int(resultVector[i]));
	}

	test = image2 / image1;
	resultVector = test.toVector()[0];
	for (auto i = 0; i < resultVector.size(); i++)
	{
		std::wostringstream os;
		os << int(rhsVector[i]) << "/" << int(lhsVector[i]) << " <> " << int(resultVector[i]);
		ASSERT_EQ(divideFinancial(int(rhsVector[i]), int(lhsVector[i])), int(resultVector[i]), os.str().c_str());
	}

	test = image2 * image1;
	resultVector = test.toVector()[0];
	for (auto i = 0; i < resultVector.size(); i++)
	{
		int t = max(0, min(0xFF, lhsVector[i] * rhsVector[i]));
		ASSERT_EQ(t, int(resultVector[i]));
	}
}

TEST(Ipp8uTest, CastTest)
{
	Ipp8uArray image1({ 10, 13 });
	image1.iota();
	auto I = image1;

	Ipp32fArray floatImage({ 10, 13 });
	floatImage.iota();

	auto castImage = Ipp32fArray(image1);
	ASSERT_TRUE(floatImage == castImage);

	auto ii = Ipp8uArray(image1);
}

TEST(Ipp8uTest, nullify8uTest)
{
	Ipp8uArray image1({ 10, 13 });
	image1.iota();

	auto subImage = image1({ 2, 3, 6, 3 });
	auto subTruth = subImage.sum();

	ASSERT_FALSE(image1.isEmpty());

	// We make the original image null
	image1.clear();
	ASSERT_TRUE(image1.isEmpty());

	// The subimage should not be null
	ASSERT_FALSE(subImage.isEmpty());

	// And data exists
	ASSERT_EQ(subTruth[0], subImage.sum()[0]);
}

TEST(Ipp8uTest, Ipp8uSubscriptTest)
{
	Ipp8uArray baseImage({ 6, 5, 3 });
	baseImage.zero();
	auto image = baseImage(baseImage.getRoi().inflate(-1));
	image.uniformDistribution(0, 255);

	auto i = 0;
	auto it = image.begin();
	for (auto r : range(image.getHeight()))
	{
		for (auto c : range(image.getWidth()))
		{
			for (auto d : range(image.getComponents()))
			{
				// For debugging
				if (image[i] != image[{ c, r, d }])
				{
					auto x = r;
				}

				ASSERT_EQ(image[i], (image[{ c, r, d }]));

				auto v = *it++;
				ASSERT_EQ(image[i], v);
				i++;
			}
		}

	}

}

TEST(Ipp8uTest, Ipp8uIndexWriteTest)
{
	Ipp8uArray baseImage({ 6, 5 });
	auto image = baseImage(baseImage.getRoi().inflate(-1));
	image.iota();

	// Read test
	for (auto i = 0; i < image.volume(); i++)
	{
		ASSERT_EQ(i, int(image[i]));
	}

	// write test
	Ipp8uArray randomImage(image.getSize());
	randomImage.uniformDistribution(0, 255);

	for (auto i = 0; i < image.volume(); i++)
	{
		image[i] = randomImage[i];
	}

	ASSERT_EQ(randomImage, image);
}

TEST(Ipp8uTest, Ipp8uStrTest)
{
	// not a good test
	Ipp8u data4[] =
	{ 0, 0, 1, 1, 1, 0, 0,
	  0, 1, 1, 1, 1, 1, 0,
	  1, 1, 1, 1, 1, 1, 1,
	  1, 1, 1, 1, 1, 1, 1,
	  1, 1, 1, 1, 1, 1, 1,
	  0, 1, 1, 1, 1, 1, 0,
	  0, 0, 1, 1, 1, 0, 0 };
	Ipp8uArray truth4({ 7, 7 }, data4);
	auto disk4 = Ipp8uArray::createDisk(4);
	ASSERT_EQ(truth4, disk4);

	Ipp8u data9[] =
	{
		0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0,
		0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
		0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
		0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
		0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
		0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
		0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
		0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0
	};

	Ipp8uArray truth9({ 17, 17 }, data9);
	auto disk9 = Ipp8uArray::createDisk(9);
	ASSERT_EQ(truth9, disk9);

	auto disk3 = Ipp8uArray::createDisk(3);
	ASSERT_EQ(MtiSize(5, 5), disk3.getSize());
	ASSERT_EQ(25, disk3.sum()[0]);

	Ipp8u data2[] =
	{
	  0,  0,  1,  0,  0,
	  0,  1,  1,  1,  0,
	  1,  1,  1,  1,  1,
	  0,  1,  1,  1,  0,
	  0,  0,  1,  0,  0
	};

	Ipp8uArray truth2({ 5, 5 }, data2);
	auto disk2 = Ipp8uArray::createDisk(2);
	ASSERT_EQ(truth2, disk2);

	//SaveVariablesToMatFile4("c:\\temp\\testStrel.mat", disk4, disk9, disk2, disk3);
}
#pragma warning(pop)