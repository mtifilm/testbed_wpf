#include "pch.h"

extern MtiSize boxSize;
extern float BoxArray[];

static Ipp16uArray get16uBox()
{
	Ipp16uArray box(boxSize);

	auto bsize = boxSize.height * boxSize.width;
	vector<Ipp16u> ipp16uBoxVector(bsize);
	for (auto i : range(bsize))
	{
		ipp16uBoxVector[i] = Ipp16u(BoxArray[i]);
	}

	box.copyFromPod(ipp16uBoxVector.data(), box.getWidth() * box.getHeight());

	return box;
}

TEST(Ipp16uTest, Ipp16uSetTest)
{
	Ipp16uArray image({ 10, 20, 3 });
	image.set({ 1, 2, 3 });

	for (auto r = 0; r < image.getHeight(); r++)
	{
		auto rp = image.getRowPointer(r);
		for (auto c = 0; c < image.getWidth(); c++)
		{
			ASSERT_EQ(1, int(*rp++));
			ASSERT_EQ(2, int(*rp++));
			ASSERT_EQ(3, int(*rp++));
		}
	}
}

TEST(Ipp16uTest, Ipp16uZeroTest)
{
	Ipp16uArray image({ 10, 211, 3 });
	image.iota();
	image.zero();

	auto zees = image.sum();
	for (auto &z : zees)
	{
		ASSERT_EQ(0.0, z);
	}
}

TEST(Ipp16uTest, Ipp16uSumTest)
{
	Ipp16uArray image({ 16, 20 });
	image.zero();
	auto z = image.sum();
	ASSERT_EQ(0.0, z[0]);

	image.iota();

	auto v = image.sum();
	auto truth = image.getHeight() * image.getWidth();
	truth = (truth - 1)*(truth) / 2;
	ASSERT_EQ(double(truth), v[0]);
}


TEST(Ipp16uTest, Ipp8uArithmiticInPlaceArrayTest)
{
	Ipp16uArray image1({ 10, 13 });
	image1.normalDistribution(32384, 5000);
	image1 = image1 + 1;

	Ipp16uArray image2({ 10, 13 });
	image2.set({ 0x09 });

	auto image1Orig = image1.duplicate();

	image1 += image2;
	for (auto r : image1.rowRange())
	{
		auto rp3 = image1.getRowPointer(r);
		auto rp2 = image1Orig.getRowPointer(r);
		auto rp1 = image2.getRowPointer(r);

		for (auto c : image1.colRange())
		{
			int rv1 = *rp1++;
			int rv2 = *rp2++;
			int rv3 = *rp3++;

			ASSERT_EQ(min(0xffff, rv1 + rv2), int(rv3));
		}
	}

	image1 = image1Orig.duplicate();
	image1 -= image2;
	for (auto r : image1.rowRange())
	{
		auto rp3 = image1.getRowPointer(r);
		auto rp2 = image1Orig.getRowPointer(r);
		auto rp1 = image2.getRowPointer(r);

		for (auto c : image1.colRange())
		{
			int rv1 = *rp1++;
			int rv2 = *rp2++;
			int rv3 = *rp3++;

			ASSERT_EQ(max(0, rv2 - rv1), int(rv3));
		}
	}

	image1 = image1Orig.duplicate();
	image1 *= image2;
	for (auto r : image1.rowRange())
	{
		auto rp3 = image1.getRowPointer(r);
		auto rp2 = image1Orig.getRowPointer(r);
		auto rp1 = image2.getRowPointer(r);

		for (auto c : image1.colRange())
		{
			int rv1 = *rp1++;
			int rv2 = *rp2++;
			int rv3 = *rp3++;

			ASSERT_EQ(min(0xFFFF, rv2 * rv1), int(rv3));
		}
	}

	image1 = image1Orig.duplicate();
	image2.set({ 21 });
	image1 /= image2;
	for (auto r : image1.rowRange())
	{
		auto rp3 = image1.getRowPointer(r);
		auto rp2 = image1Orig.getRowPointer(r);
		auto rp1 = image2.getRowPointer(r);

		for (auto c : image1.colRange())
		{
			int rv1 = *rp1++;
			int rv2 = *rp2++;
			int rv3 = *rp3++;

			std::wostringstream os;
			os << int(rv2) << "/" << int(rv1) << " <> " << int(rv3);
			ASSERT_EQ(int(float(rv2) / rv1 + 0.5f), int(rv3)) << os.str();
		}
	}
}

TEST(Ipp16uTest, Ipp16uArithmiticInPlaceScalerTest)
{
	Ipp16uArray image1({ 10, 13 });
	image1.iota();

	auto image1Orig = image1.duplicate();

	auto testValue = 12;
	image1 += testValue;
	for (auto r : image1.rowRange())
	{
		auto rpResult = image1.getRowPointer(r);
		auto rpOrig = image1Orig.getRowPointer(r);

		for (auto c : image1.colRange())
		{
			float rvOrig = *rpOrig++;
			float rvResult = *rpResult++;

			ASSERT_EQ(min(0xFFFF, rvOrig + testValue), rvResult);
		}
	}

	image1 = image1Orig.duplicate();
	image1 -= testValue;

	for (auto r : image1.rowRange())
	{
		auto rpResult = image1.getRowPointer(r);
		auto rpOrig = image1Orig.getRowPointer(r);

		for (auto c : image1.colRange())
		{
			float rvOrig = *rpOrig++;
			float rvResult = *rpResult++;

			ASSERT_EQ(max(0, rvOrig - testValue), rvResult);
		}
	}

	image1 = image1Orig.duplicate();
	image1 *= testValue;
	for (auto r : image1.rowRange())
	{
		auto rpResult = image1.getRowPointer(r);
		auto rpOrig = image1Orig.getRowPointer(r);

		for (auto c : image1.colRange())
		{
			float rvOrig = *rpOrig++;
			float rvResult = *rpResult++;

			ASSERT_EQ(min(0xFFFF, rvOrig * testValue), rvResult);
		}
	}

	image1 = image1Orig.duplicate();
	image1 /= testValue;
	for (auto r : image1.rowRange())
	{
		auto rpResult = image1.getRowPointer(r);
		auto rpOrig = image1Orig.getRowPointer(r);

		for (auto c : image1.colRange())
		{
			int rvOrig = *rpOrig++;
			int rvResult = *rpResult++;

			ASSERT_EQ(divideFinancial(rvOrig, testValue), rvResult);
		}
	}
}

TEST(Ipp16uTest, ConvertOperatorToVoidTest)
{
	Ipp32fArray box({ 200, 100 });
	box.iota();

	Ipp16uArray lhs;
	lhs <<= box;

	auto fbox = (Ipp16uArray)box;

	ASSERT_EQ(lhs, fbox);
}

TEST(Ipp16uTest, ConvertOperatorFromZeroTest)
{
	Ipp16uArray box({ 200, 100 });
	box.iota();

	auto zeroLengthArray = box({ 10,20,0,5 });

	// Success is this not throwing an error
	box <<= zeroLengthArray;
}

TEST(Ipp16uTest, ConvertFromTest)
{
	Ipp32fArray box({ 200, 100 });
	box.iota();

	Ipp16uArray lhs;
	lhs <<= box;

	auto fbox = Ipp16uArray(box);

	ASSERT_TRUE(lhs == fbox);

	Ipp16uArray bigImage({ 400, 200, 3 });
	bigImage.normalDistribution(0, 3000);

	Ipp32fArray bigLhs(bigImage.getSize());
	bigLhs <<= bigImage;
	ASSERT_TRUE(bigLhs == Ipp32fArray(bigImage));
}

TEST(Ipp16uTest, Ipp16uArithmiticArrayTest)
{
	Ipp16uArray image1({ 16, 20 });
	image1.iota();
	image1 = image1 + 10000;

	Ipp16uArray image2(image1.getSize());
	image2.uniformDistribution(0, 65535);

	auto vec1 = image1.toVector()[0];
	auto vec2 = image2.toVector()[0];

	auto test = image1 + image2;
	auto vect = test.toVector()[0];
	for (auto i = 0; i < vect.size(); i++)
	{
		int vsum = int(vec1[i]) + int(vec2[i]);
		vsum = std::clamp(vsum, 0, int(UINT16_MAX));
		ASSERT_EQ(vsum, int(vect[i])) << "Add " << "vec1[i]=" << vec1[i] << ", vec2[i]=" << vec2[i];
	}

	test = image1 - image2;
	vect = test.toVector()[0];
	for (auto i = 0; i < vect.size(); i++)
	{
		int vsum = int(vec1[i]) - int(vec2[i]);
		vsum = std::clamp(vsum, 0, int(UINT16_MAX));
		ASSERT_EQ(vsum, int(vect[i])) << "Subtract" << "vec1[i]=" << vec1[i] << ", vec2[i]=" << vec2[i];
	}

	test = image2 / image1;
	vect = test.toVector()[0];
	for (auto i = 0; i < vect.size(); i++)
	{
		int v = divideFinancial(vec2[i], vec1[i]);
		ASSERT_EQ(v, int(vect[i])) << "Divide" << "vec1[i]=" << vec1[i] << ", vec2[i]=" << vec2[i];
	}

	test = image2 * image1;
	vect = test.toVector()[0];
	for (auto i = 0ui64; i < vect.size(); i++)
	{
		int vsum = vec1[i] * vec2[i];

		// We don't have to clamp
		vsum = std::clamp(vsum, 0, int(UINT16_MAX));
		ASSERT_EQ(vsum, int(vect[i])) << "Multiply" << "vec1[i]=" << vec1[i] << ", vec2[i]=" << vec2[i];;
	}
}

TEST(Ipp16uTest, Ipp16uArithmiticScalerTest)
{
	Ipp16uArray image1({ 16, 20 });
	image1.normalDistribution(10000, 3);

	auto vec1 = image1.toVector()[0];

	auto test = image1 + 32;
	auto vect = test.toVector()[0];
	for (auto i = 0; i < vect.size(); i++)
	{
		ASSERT_EQ(int(vec1[i] + 32), int(vect[i]));
	}

	test = image1 - 10;
	vect = test.toVector()[0];
	for (auto i = 0; i < vect.size(); i++)
	{
		ASSERT_EQ(int(vec1[i] - 10), int(vect[i]));
	}

	test = image1 / 22;
	vect = test.toVector()[0];
	for (auto i = 0; i < vect.size(); i++)
	{
		int v = divideFinancial(vec1[i], 22ui16);
		ASSERT_EQ(v, int(vect[i]));
	}

	test = image1 * 10;
	vect = test.toVector()[0];
	for (auto i = 0; i < vect.size(); i++)
	{
		int v = int(vec1[i] * 10);
		v = std::clamp(v, 0, int(UINT16_MAX));
		ASSERT_EQ(v, int(vect[i]));
	}
}

TEST(Ipp16uTest, Ipp16uAssignmentTest)
{
	Ipp16uArray image({ 16, 20 });
	image.normalDistribution(1000, 200);

	IppiRect roi = { 5, 6, 8, 10 };
	Ipp16uArray subImage = image(roi);
	auto truthSum = subImage.sum()[0];

	auto subImage2 = subImage;
	auto &roi2 = subImage2.getRoi();

	ASSERT_EQ(roi.x, roi2.x);
	ASSERT_EQ(roi.y, roi2.y);
	ASSERT_EQ(roi.width, roi2.width);
	ASSERT_EQ(roi.height, roi2.height);

	ASSERT_EQ(truthSum, subImage2.sum()[0]);
}

TEST(Ipp16uTest, Ipp16uL1NormTest)
{
	Ipp16uArray image({ 10, 20, 3 });
	image.set({ 1, 2, 3 });
	auto norm1 = image.L1Norm();
	auto sum1 = image.sum();
	AssertVectorEqual(sum1, norm1);

	auto box = get16uBox();

	auto norm0 = box.L1Norm();
	auto sum0 = box.sum();
	ASSERT_NE(0.0, sum0[0]);
	AssertVectorEqual(sum0, norm0);

	Ipp16uArray normal({ 20,10,3 });
	normal.iota();
	auto norm2 = normal.L1Norm();
	auto sum2 = normal.sum();
	auto lvec = normal.toVector();
	auto ls = lvec.size();
	for (auto i : range(ls))
	{
		auto &vec = lvec[i];
		auto t = std::accumulate(vec.begin(), vec.end(), 0, [](Ipp16u x, Ipp16u y) {return x + abs(y); });
		ASSERT_EQ(double(t), norm2[i]);
	}
}

TEST(Ipp16uTest, Ipp16uL2NormTest)
{
	Ipp16uArray normal({ 20, 10, 3 });
	normal.iota();

	auto ivec = normal.toVector();
	auto norm2 = normal.L2Norm();
	auto lvec = normal.toVector();
	auto ls = lvec.size();
	for (auto i : range(ls))
	{
		auto &vec = lvec[i];
		auto t = std::accumulate(vec.begin(), vec.end(), 0, [](int x, int y) {return x + (y*y); });
		auto truth = sqrt(double(t));
		ASSERT_NE(truth, 0.0);
		ASSERT_EQ(truth, norm2[i]);
	}
}

TEST(Ipp16uTest, Ipp16uRoiTest)
{
	Ipp16uArray Image({ 10, 20, 3 });
	Image.set({ 1, 2, 3 });

	MtiRect roi = { 5, 10, 3, 4 };
	auto &roiImage = Image(roi);

	ASSERT_EQ(roi.height, roiImage.getHeight());
	ASSERT_EQ(roi.width, roiImage.getWidth());

	// Change the image ROI sould change
	auto p = Image.getElementPointer(roi.tl());
	p[0] = 11;
	p[1] = 12;
	p[2] = 13;

	auto s = roiImage.data();

	for (auto i = 0; i < 3; i++)
	{
		ASSERT_EQ(int(p[i]), int(s[i]));
	}

	roiImage.set({ 100, 200, 300 });
	for (auto i = 0; i < 3; i++)
	{
		ASSERT_EQ(int(p[i]), int(s[i]));
	}
}

TEST(Ipp16uTest, Ipp16uArrayPodTest)
{
	auto rows = 10;
	auto cols = 30;
	auto p = ippsMalloc_16u(rows * cols);

	// Make the index and values the same
	for (auto i = 0; i < rows*cols; i++)
	{
		p[i] = Ipp16u(i);
	}

	{
		// See if data agrees, if it was copied
		// the alignment of IppArray allocator woudl be off
		// and the results would be 32 instead of 30 for row(1, 0, 0)
		Ipp16uArray PodBacking({ cols, rows }, p);
		auto v1 = *PodBacking.getElementPointer({ 10, 1 });
		ASSERT_EQ(1 * cols + 10, (int)v1);
		auto v2 = *PodBacking.getElementPointer({13, 7});
		ASSERT_EQ(7 * cols + 13, (int)v2);

		// Now blow away some data, this should change
		// underlying allocated array.
		auto dep = PodBacking.getElementPointer({ 13, 7 });
		*dep = 345;

		ASSERT_EQ(345, int(p[7 * cols + 13]));
	}

	// IppArray is blown away, free should work
	ippsFree(p);
}

TEST(Ipp16uTest, Ipp16uMedianTestEven)
{
	Ipp16uArray image({ 100, 110, 3 });
	float mean = 10000;
	float stddev = 10000.0f / 3.0f;

	image.normalDistribution(mean, stddev);

	auto medians = image.median();
	SaveVariablesToMatFile2("C:\\temp\\median.mat", image, medians);

	auto vecs = image.toVector();
	for (auto vec : vecs)
	{
		auto t0 = getMedianTruth(vec);
		auto v0 = image.median(vec);
		ASSERT_EQ(int(t0), int(v0));
	}

	// Matlab test for even
	Ipp16uArray mTest({ 10, 1 });
	mTest.iota();
	auto v = mTest.toVector();
	auto t1 = getMedianTruth(v[0]);
	auto v1 = mTest.median()[0];
	ASSERT_EQ(int(t1), int(v1));
}

TEST(Ipp16uTest, Ipp16uMedianTestOdd)
{
	Ipp16uArray image({ 101, 111, 3 });
	float mean = 10000;
	float stddev = 10000.0f / 3.0f;

	image.normalDistribution(mean, stddev);

	auto vecs = image.toVector({ 0,0, image.getWidth(), image.getHeight() });
	for (auto vec : vecs)
	{
		auto t0 = getMedianTruth(vec);
		auto v0 = image.median(vec);
		ASSERT_EQ(int(t0), int(v0));
	}

	Ipp16uArray mTest({ 9, 1 });
	mTest.iota();
	auto v = mTest.toVector();
	auto t1 = getMedianTruth(v[0]);
	auto v1 = mTest.median()[0];
	ASSERT_EQ(int(t1), int(v1));
}


TEST(Ipp16uTest, Ipp16uBoxMedianTest)
{
	auto box = get16uBox();
	auto median = box.median();

	// Computed from matlab
	ASSERT_EQ(38193, int(median[0]));

}

TEST(Ipp16uTest, Ipp16uExtractVectorTest)
{
	Ipp16uArray image({ 10, 6, 3 });
	image.set({ 1,2,3 });
	IppiRect roi = { 2, 3, 4, 2 };
	auto subImage = image(roi);

	vector<Ipp16u> inserts = { 11,12,13 };
	subImage.set(inserts);

	// Not a great test
	auto vecs = image.toVector(roi);
	auto i = 0;
	for (auto vec : vecs)
	{
		ASSERT_EQ(roi.height*roi.width, int(vec.size()));
		auto t0 = inserts[i++] * roi.height*roi.width;
		auto v0 = std::accumulate(vec.begin(), vec.end(), 0);
		ASSERT_EQ(t0, v0);
	}
}

TEST(Ipp16uTest, Ipp16uIotaVectorTest)
{
	Ipp16uArray image({ 11, 12, 3 });
	image.iota();

	// Not a great test
	auto vecs = image.toVector();
	auto i = 0;
	vector<Ipp16u> truth(image.area());
	for (auto i = 0; i < truth.size(); i++)
	{
		truth[i] = (Ipp16u)i;
	}

	for (auto vec : vecs)
	{
		for (auto i : range(vec.size()))
		{
			ASSERT_EQ(int(truth[i]), int(vec[i]));
		}
	}
}

TEST(Ipp16uTest, Ipp16uCopyConstructorTest)
{
	Ipp16uArray image({ 11, 12, 3 });
	image.iota();

	Ipp16uArray copyOfImage(image);
	auto truth = image.area();
	truth = (truth - 1)*truth / 2;

	ASSERT_EQ((Ipp64f)truth, copyOfImage.sum()[0]);
}

TEST(Ipp16uTest, Ipp16uCopyConstructorRoiTest)
{
	Ipp16uArray image({ 13, 12, 3 });
	image.iota();

	IppiRect roi = { 3, 2, 8, 7 };
	Ipp16uArray subImage = image(roi);

	Ipp64f truth = 0;
	for (auto r = roi.y; r < roi.y + roi.height; r++)
	{
		for (auto c = roi.x; c < roi.x + roi.width; c++)
		{
			truth += image[{c, r}];
		}
	}

	ASSERT_EQ((Ipp64f)truth, subImage.sum()[0]);

	Ipp16uArray subSubImage = subImage({ 2, 3, 4, 3 });

	roi = { roi.x + 2, roi.y + 3, 4, 3 };
	auto &r1 = subSubImage.getRoi();
	ASSERT_EQ(roi.x, r1.x);
	ASSERT_EQ(roi.y, r1.y);
	ASSERT_EQ(roi.width, r1.width);
	ASSERT_EQ(roi.height, r1.height);

	truth = 0;
	for (auto r = roi.y; r < roi.y + roi.height; r++)
	{
		for (auto c = roi.x; c < roi.x + roi.width; c++)
		{
			truth += image[{c, r}];
		}
	}

	ASSERT_EQ((Ipp64f)truth, subSubImage.sum()[0]);
}


TEST(Ipp16uTest, Ipp16uRowIteratorTest)
{
	Ipp16uArray image({ 100, 50 });
	image.iota();

	auto t = 0.0;
	for (auto p : image.rowIterator())
	{
		for (auto c : range(image.getWidth()))
		{
			t += *p++;
		}
	}

	auto s = image.sum()[0];
	ASSERT_EQ(s, t);
}

TEST(Ipp16uTest, Ipp16uMeanTest)
{
	Ipp16uArray normal({ 20, 11, 3 });
	normal.uniformDistribution(100, 200);
	auto mean = normal.mean();
	auto x = normal.sum();
	auto lvec = normal.toVector();
	for (auto i : range(lvec.size()))
	{
		auto &vec = lvec[i];
		auto t = std::accumulate(vec.begin(), vec.end(), 0.0, [](float x, Ipp16u y) {return x + y; });
		t = t / normal.area();

		// We want FAST not accurate
		ASSERT_NEAR(t, mean[i], 10E-5);
	}
}

TEST(Ipp16uTest, Ipp16uDotProductTest)
{
	Ipp16uArray n1({ 21, 11, 3 });
	n1.normalDistribution(1, 0.1);
	auto n2 = n1 + 1;

	auto dotProduct = n1.dotProduct(n2);

	vector<Ipp64f> t = { 0,0,0 };
	for (auto r = 0; r < n1.getHeight(); r++)
	{
		auto p1 = n1.getRowPointer(r);
		auto p2 = n2.getRowPointer(r);
		for (auto c = 0; c < n1.getWidth(); c++)
		{
			for (auto k = 0; k < n1.getComponents(); k++)
			{
				t[k] += *p1++ * *p2++;
			}
		}
	}

	for (auto i : range(dotProduct.size()))
	{
		ASSERT_NEAR(t[i], dotProduct[i], 10E-6);
	}
}

TEST(Ipp16uTest, Ipp16uL1NormDiffTest)
{
	Ipp16uArray n1({ 21, 13, 3 });
	n1.normalDistribution(1, 0.1);
	auto n2 = n1 + 1;

	vector<Ipp64f> dotProduct(3);
	n1.L1NormDiff(n2, dotProduct.data());

	vector<Ipp64f> t = { 0,0,0 };
	for (auto r = 0; r < n1.getHeight(); r++)
	{
		auto p1 = n1.getRowPointer(r);
		auto p2 = n2.getRowPointer(r);
		for (auto c = 0; c < n1.getWidth(); c++)
		{
			for (auto k = 0; k < n1.getComponents(); k++)
			{
				t[k] += abs(*p1++ - *p2++);
			}
		}
	}

	for (auto i : range(dotProduct.size()))
	{
		ASSERT_NEAR(t[i], dotProduct[i], 10E-6);
	}
}

TEST(Ipp16uTest, Ipp16uL2NormDiffTest)
{
	//Ipp16uArray n1({21, 13, 3});
	//n1.normalDistribution(1, 0.1);
	//auto n2 = n1 + 1;

	//vector<Ipp64f> result(3);
 //   n1.L2NormDiff(n2, result.data());

	//vector<Ipp64f> t = { 0,0,0 };
	//for (auto r = 0; r < n1.getHeight(); r++)
	//{
	//	auto p1 = n1.getRowPointer(r);
	//	auto p2 = n2.getRowPointer(r);
	//	for (auto c = 0; c < n1.getWidth(); c++)
	//	{
	//		for (auto k = 0; k < n1.getComponents(); k++)
	//		{
	//			auto v = (*p1++ - *p2++);
	//			t[k] += v*v;
	//		}
	//	}
	//}

	//for (auto i : range(result.size()))
	//{
	//	ASSERT_EQ(sqrt(t[i]), result[i], 10E-6);
	//}
}

TEST(Ipp16uTest, Ipp16uConvertIpp32fTest)
{
	auto testBox = get16uBox();
	Ipp32fArray truth(boxSize);
	truth.copyFromPod(BoxArray, truth.area());

	auto boxConverted = Ipp32fArray(testBox);
	ASSERT_EQ(truth, boxConverted);
}

TEST(Ipp16uTest, Ipp16uMinEveryTest)
{
	Ipp16uArray s1({ 12, 10 });
	Ipp16uArray s2({ 12, 10 });
	s1.uniformDistribution(0, 0xFFFF);
	s2.uniformDistribution(0, 0xFFFF);

	Ipp16uArray s3;
	s3 <<= s1;

	s3.minEveryInplace(s2);

	auto it1 = s1.begin();
	auto it2 = s2.begin();

	for (auto f : s3)
	{
		if (f != std::min<Ipp16u>(*it1++, *it2++))
		{
			ASSERT_TRUE(false) << "no match";
		}
	}
}

TEST(Ipp16uTest, Ipp16uAbsDiffInplaceTest)
{
	Ipp16uArray s1({ 12, 10 });
	Ipp16uArray s2({ 12, 10 });
	s1.uniformDistribution(0, 0xFFFF);
	s2.uniformDistribution(0, 0xFFFF);

	Ipp16uArray s3;
	s1.absDiff(s2, s3);

	auto it1 = s1.begin();
	auto it2 = s2.begin();

	for (auto f : s3)
	{
		if (f != abs(*it1++ - *it2++))
		{
			ASSERT_TRUE(false) << "no match";
		}
	}
}

TEST(Ipp16uTest, Ipp16uTransposeTest)
{
	Ipp16uArray sourceArray({ 4, 3, 3 });
	sourceArray.uniformDistribution(0, 255);

	auto testArray = sourceArray.transpose();

	ASSERT_EQ(sourceArray.getWidth(), testArray.getHeight());
	ASSERT_EQ(sourceArray.getHeight(), testArray.getWidth());

	for (auto r = 0; r < sourceArray.getHeight(); r++)
	{
		for (auto c = 0; c < sourceArray.getWidth(); c++)
		{
			for (auto z = 0; z < sourceArray.getHeight(); z++)
			{
				int sp = *sourceArray.getElementPointer({ c, r, z });
				int tp = *testArray.getElementPointer({ r, c, z });
				ASSERT_EQ(sp, tp) << "[r=" << r << ", c=" << c << ", z=" << z << "] " << int(sp) << "!=" << int(tp);
			}
		}
	}
}

TEST(Ipp16uTest, Ipp16uLinearIteratorTest)
{
	Ipp16uArray sourceArray({ 2048, 1556 });
	sourceArray.uniformDistribution(0, 0xFFFF);

	auto testArray = sourceArray({ 100, 100, 2048 - 200, 1556 - 200 });

	CHRTimer hrt;

	double sum0 = 0;
	for (auto r = 0; r < testArray.getHeight(); r++)
	{
		auto *p = testArray.getRowPointer(r);
		for (auto c = 0; c < testArray.getWidth(); c++)
		{
			sum0 += *p++;
		}
	}

	auto t0 = hrt.intervalMilliseconds();

	double sum1 = 0;
	for (auto f : testArray)
	{
		sum1 += f;
	}

	auto t1 = hrt.intervalMilliseconds();

	double sum11 = 0;
	double diff11 = 0;
	for (auto f : testArray)
	{
		diff11 -= f;
		sum11 += f;
	}

	auto t2 = hrt.intervalMilliseconds();

	std::wostringstream os;
	os << diff11 << ", " << sum0 << ", " << sum1 - sum11;
	os << std::endl << "Ipp16uLinearIteratorTest:  LinearIterator: " << t1 << ", Linear, 2 ops: " << t2 << ", C loop: " << t0 << ", Ratio " << t1 / t0 << " as slow";;
	auto cost = t2 - t1;

	// I realize this is not correct as t0 is not pure overhead of C
	os << ", iterator additional overhead: " << t1 - cost - t0;
	////			Logger::WriteMessage(os.str().c_str());

	ASSERT_EQ(sum0, sum1);
}

TEST(Ipp16uTest, IppMatIoWriteReadTest)
{
	Ipp16uArray sourceArray({ 4, 5, 3 });
	sourceArray.uniformDistribution(0, 4096);

	const std::string fileName = getTempFileName("IppMatIoWriteReadTest.mat");
	SaveVariablesToMatFile1(fileName, sourceArray);

	auto testArray = MatIO::readIpp16uArray(fileName, "sourceArray");
	auto b = testArray == sourceArray;
}

TEST(Ipp16uTest, Ipp16uFliplrTest)
{
	Ipp16uArray bigSource({ 20, 12 });
	bigSource.set({ 0xFFFF });
	auto source = bigSource({ 2, 3, 7, 9 });
	source.iota();

	auto test = source.fliplr();
	ASSERT_EQ(test.getSize(), source.getSize());

	for (auto r = 0; r < test.getHeight(); r++)
	{
		auto p = test.getRowPointer(r);
		auto q = source.getRowPointer(r);
		for (auto c = 0; c < source.getWidth(); c++)
		{
			ASSERT_EQ(q[test.getWidth() - c - 1], p[c]);
		}
	}

	Ipp16uArray bigSource3({ 20, 12, 3 });
	bigSource3.set({ 0xFFFF, 0xFFFE, 0xFFFD });
	auto source3 = bigSource3({ 3, 2, 9, 6 });
	source3.uniformDistribution(0, 10);
	source3.iota();

	auto test3 = source3.fliplr();
	for (auto r = 0; r < test3.getHeight(); r++)
	{
		auto q = test3.getRowPointer(r);
		auto p = source3.getRowPointer(r);
		for (auto c = 0; c < source.getWidth(); c++)
		{
			for (auto d = 0; d < test3.getComponents(); d++)
			{
				if (q[3 * (test3.getWidth() - c - 1) + d] != p[3 * c + d])
				{
					auto a = q[3 * (test3.getWidth() - c - 1) + d];
					auto b = p[3 * c + d];
					auto cc = a - b;
				}

				ASSERT_EQ(q[3 * (test3.getWidth() - c - 1) + d], p[3 * c + d]);
			}
		}
	}
}

TEST(Ipp16uTest, OneDTest)
{
	Ipp16uArray test({ 100 });
	ASSERT_EQ(MtiSize({ 100, 1, 1 }), test.getSize());
}

TEST(Ipp16uTest, EmptyOnCreateTest)
{
	Ipp16uArray test;
	ASSERT_TRUE(test.isEmpty());
}

TEST(Ipp16uTest, EmptyOnClearTest)
{
	Ipp16uArray notEmptyTest({ 10 });
	ASSERT_FALSE(notEmptyTest.isEmpty());

	notEmptyTest.clear();
	ASSERT_TRUE(notEmptyTest.isEmpty());
}

TEST(Ipp16uTest, ResizeAntiAliasingDownTest)
{
	// Not a test, just to make sure no crash
	auto width = 200;
	auto height = 140;
	Ipp16uArray dstArray({ width, height, 1 });
	dstArray.iota();

	width = dstArray.getWidth();
	height = dstArray.getHeight();
	const auto smallArray = dstArray.resizeAntiAliasing({ width / 2, height / 2 });
	ASSERT_EQ(smallArray.getWidth(), width / 2);
	ASSERT_EQ(smallArray.getHeight(), height / 2);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
// We want to test with some real data, but not read it in all the time 
//
class TestEnvironment : public ::testing::Environment 
{
public:
	static Ipp16uArray getMomTestImage() 
	{
		return momImage;
	}

	// Initialise the array.
	virtual void SetUp()
	{
		auto fileName = getTestFileName("mom_112657.mat");
		momImage = MatIO::readIpp16uArray(fileName);
	}

	static Ipp16uArray momImage;
};

Ipp16uArray TestEnvironment::momImage;

///////////////////////////////////////////////////////////////////////////////////////////////////////////
////  Test fixture for Arithmetic Scalar tests

class MtiSelectModeTest : public ::testing::Test
{
protected:
	void SetUp() override
	{
		TestEnvironment::momImage = MatIO::readIpp16uArray(getTestFileName("mom_112657.mat"));
		imageData = TestEnvironment::getMomTestImage()({ 50, 50, 256, 256 });
	}

	Ipp16uArray imageData;
};

TEST(Ipp16uTest, Ipp16uLogLutTest)
{
	int lutSize = 1024;
	auto logLut = Ipp32sArray::logLut(lutSize, 65535,100);
	auto axisLut = Ipp32sArray::linspace(0.0f, 65535.0f, lutSize);

	auto fileName = getTestFileName("mom_112657.mat");

	// Mom is 16 bits
	auto momImage = MatIO::readIpp16uArray(fileName);
	momImage = momImage.resize({ 2048, 1556 });
	auto testImage = momImage.duplicate();

	Ipp8uArray scratch;
	CHRTimer hrt;
	testImage.applyLutInPlace({ logLut }, axisLut, scratch);
	auto x = hrt.intervalMilliseconds();
	ASSERT_NE(momImage[0], testImage[0]);
//		MatIO::saveListToMatFile("c:\\temp\\test.mat", testImage, "momImageLut", logLut, "logLut");
	testImage.applyLutInPlace({ axisLut }, logLut, scratch);
	auto y = hrt.intervalMilliseconds();
//			MatIO::appendListToMatFile("c:\\temp\\test.mat", testImage, "momImageInverse");
	for (auto i : range(momImage.volume()))
	{
		ASSERT_NEAR(momImage[i], testImage[i], 1) << i;
	}
}

TEST_F(MtiSelectModeTest, MtiSelectModeIntersectTest)
{
	// Simple test to make sure we truncate correctly
	// We assume the copy is correct because it is tested

	const MtiRect roi(0, 0, 128, 100);

	auto copyType = MtiSelectMode::intersect;
	auto utilizeExtrinsicData = false;

	MtiRect wholeRoi({ 0,0 }, imageData.getSize());
	auto testRoi = roi + MtiPoint({ -64, -44 });
	const auto iul = imageData(testRoi, copyType, utilizeExtrinsicData);
	EXPECT_EQ(MtiRect(50, 50, 64, 56), iul.getRoi());

	//const auto iuc = imageData(roi + MtiPoint({ 64, -44 }), copyType, utilizeExtrinsicData);
	//AppendVariablesToMatFile1(fileName, iuc);
	//const auto muc = MatIO::readIpp16uArray(matLabTruthName, "muc");
	//EXPECT_EQ(muc, iuc);

	//const auto iur = imageData(roi + MtiPoint({ 256 - 64, -44 }), copyType, utilizeExtrinsicData);
	//AppendVariablesToMatFile1(fileName, iur);
	//const auto mur = MatIO::readIpp16uArray(matLabTruthName, "mur");
	//compareIppArray(mur, iur);
	//EXPECT_EQ(mur, iur);

	//const auto icenter = imageData(roi + MtiPoint({ 64, 44 }), copyType, utilizeExtrinsicData);
	//AppendVariablesToMatFile1(fileName, icenter);
	//const auto mcenter = MatIO::readIpp16uArray(matLabTruthName, "mcenter");
	//EXPECT_EQ(mcenter, icenter);

	//const auto ill = imageData(roi + MtiPoint({ -64, 256 - 64 }), copyType, utilizeExtrinsicData);
	//AppendVariablesToMatFile1(fileName, ill);
	//const auto mll = MatIO::readIpp16uArray(matLabTruthName, "mll");
	//EXPECT_EQ(mll, ill);

	//const auto ilc = imageData(roi + MtiPoint({ 64, 256 - 64 }), copyType, utilizeExtrinsicData);
	//AppendVariablesToMatFile1(fileName, ilc);
	//const auto mlc = MatIO::readIpp16uArray(matLabTruthName, "mlc");
	//EXPECT_EQ(mlc, ilc);

	//const auto ilr = imageData(roi + MtiPoint({ 256 - 64, 256 - 64 }), copyType, utilizeExtrinsicData);
	//AppendVariablesToMatFile1(fileName, ilr);
	//const auto mlr = MatIO::readIpp16uArray(matLabTruthName, "mlr");
	//ASSERT_EQ(mlr, ilr);
}

//TEST_F(MtiSelectModeTest, MtiSelectModeMirror60PercentTest)
//{
//	// This test uses a box whose side lies about 60% outside
//		// Because the computations are very hard to check, we use Matlab
//	// to generate the truth.  This means the Matlab routines must be in sync
//	// with these. 
//	auto fileName = getTempFileName("MtiSelectModeMirror60PercentModes.mat");
//	auto matLabTruthName = getTestFileName("matlatReplicateData.mat");
//
//	const MtiRect roi(0, 0, 128, 172);
//	SaveVariablesToMatFile1(fileName, imageData);
//
//	auto copyType = MtiSelectMode::mirror;
//	auto utilizeExtrinsicData = false;
//
//	const auto ilc = imageData(roi + MtiPoint({ 64, 256 - 64 }), copyType, utilizeExtrinsicData);
//	AppendVariablesToMatFile1(fileName, ilc);
////	const auto mlc = MatIO::readIpp16uArray(matLabTruthName, "mlc");
////	EXPECT_EQ(mlc, ilc);
//
//	const auto ilr = imageData(roi + MtiPoint({ 256 - 64, 256 - 64 }), copyType, utilizeExtrinsicData);
//	AppendVariablesToMatFile1(fileName, ilr);
//}

TEST_F(MtiSelectModeTest, MtiSelectModeReplicateBad)
{
	ASSERT_THROW(imageData({-64, -72, 60, 70}, MtiSelectMode::replicate), MtiRuntimeError);
}

TEST_F(MtiSelectModeTest, MtiSelectModeIntersectEmptyTest)
{
	auto test = imageData({ -64, -72, 60, 70 }, MtiSelectMode::intersect);
	ASSERT_TRUE(test.isEmpty());
}

TEST_F(MtiSelectModeTest, MtiSelectModeValidBad)
{
	ASSERT_THROW(imageData({ -64, -72, 60, 70 }), MtiRuntimeError);
	ASSERT_THROW(imageData({ imageData.getWidth() + 1, 72, 60, 70 }, MtiSelectMode::valid), MtiRuntimeError);
}

struct SelectModeData
{
	string inputTestName;
	string outputTestName;
	MtiSelectMode copyType;
	bool utilizeExtrinsicData = false;
};

struct SelectModeDataTest : testing::TestWithParam<SelectModeData>
{
protected:
	void SetUp() override
	{
		TestEnvironment::momImage = MatIO::readIpp16uArray(getTestFileName("mom_112657.mat"));
		imageData = TestEnvironment::getMomTestImage()({ 50, 50, 256, 256 });
	}

	Ipp16uArray imageData;
};

TEST_P(SelectModeDataTest, ParameterSelectModeTest)
{
	auto as = GetParam();

	// This test uses a box whose side lies about 60% outside
		// Because the computations are very hard to check, we use Matlab
	// to generate the truth.  This means the Matlab routines must be in sync
	// with these. 
	auto fileName = getTempFileName(as.outputTestName);
	auto matLabTruthName = getTestFileName(as.inputTestName);

	const MtiRect roi(0, 0, 128, 100);
	SaveVariablesToMatFile1(fileName, imageData);

	auto copyType = as.copyType;
	auto utilizeExtrinsicData = as.utilizeExtrinsicData;

	auto mul = MatIO::readIpp16uArray(matLabTruthName, "mul");
	auto mulRect = MatIO::readMtiRect(matLabTruthName, "mulRect");
	const auto iul = imageData(mulRect, copyType, utilizeExtrinsicData);
	AppendVariablesToMatFile1(fileName, iul);
	EXPECT_EQ(mul, iul);

	auto muc = MatIO::readIpp16uArray(matLabTruthName, "muc");
	auto mucRect = MatIO::readMtiRect(matLabTruthName, "mucRect");
	const auto iuc = imageData(mucRect, copyType, utilizeExtrinsicData);
	AppendVariablesToMatFile1(fileName, iuc);
	EXPECT_EQ(muc, iuc);

	auto mur = MatIO::readIpp16uArray(matLabTruthName, "mur");
	auto murRect = MatIO::readMtiRect(matLabTruthName, "murRect");
	const auto iur = imageData(murRect, copyType, utilizeExtrinsicData);
	AppendVariablesToMatFile1(fileName, iur);
	EXPECT_EQ(mur, iur);

	auto mcl = MatIO::readIpp16uArray(matLabTruthName, "mcl");
	auto mclRect = MatIO::readMtiRect(matLabTruthName, "mclRect");
	const auto icl = imageData(mclRect, copyType, utilizeExtrinsicData);
	AppendVariablesToMatFile1(fileName, icl);
	EXPECT_EQ(mcl, icl);

	auto mcc = MatIO::readIpp16uArray(matLabTruthName, "mcc");
	auto mccRect = MatIO::readMtiRect(matLabTruthName, "mccRect");
	const auto icc = imageData(mccRect, copyType, utilizeExtrinsicData);
	AppendVariablesToMatFile1(fileName, icc);
	EXPECT_EQ(mcc, icc);

	auto mcr = MatIO::readIpp16uArray(matLabTruthName, "mcr");
	auto mcrRect = MatIO::readMtiRect(matLabTruthName, "mcrRect");
	const auto icr = imageData(mcrRect, copyType, utilizeExtrinsicData);
	AppendVariablesToMatFile1(fileName, icr);
	EXPECT_EQ(mcr, icr);

	auto mll = MatIO::readIpp16uArray(matLabTruthName, "mll");
	auto mllRect = MatIO::readMtiRect(matLabTruthName, "mllRect");
	const auto ill = imageData(mllRect, copyType, utilizeExtrinsicData);
	AppendVariablesToMatFile1(fileName, ill);
	EXPECT_EQ(mll, ill);

	auto mlc = MatIO::readIpp16uArray(matLabTruthName, "mlc");
	auto mlcRect = MatIO::readMtiRect(matLabTruthName, "mlcRect");
	const auto ilc = imageData(mlcRect, copyType, utilizeExtrinsicData);
	AppendVariablesToMatFile1(fileName, ilc);
	EXPECT_EQ(mlc, ilc);

	auto mlr = MatIO::readIpp16uArray(matLabTruthName, "mlr");
	auto mlrRect = MatIO::readMtiRect(matLabTruthName, "mlrRect");
	const auto ilr = imageData(mlrRect, copyType, utilizeExtrinsicData);
	AppendVariablesToMatFile1(fileName, ilr);
	EXPECT_EQ(mlr, ilr);
}

INSTANTIATE_TEST_CASE_P(Default, SelectModeDataTest,
	    testing::Values(SelectModeData{"matlabCircularData.mat", "MtiSelectModeCircular.mat",  MtiSelectMode::circular, false },
		SelectModeData{ "matlabMirrorData.mat", "MtiSelectModeMirror.mat",  MtiSelectMode::mirror, false },
		SelectModeData{ "matlabMirror2Data.mat", "MtiSelectModeMirror2.mat",  MtiSelectMode::mirror, false },
		SelectModeData{ "matlabReplicateData.mat", "MtiSelectModeReplicate.mat",  MtiSelectMode::replicate, false }
));

