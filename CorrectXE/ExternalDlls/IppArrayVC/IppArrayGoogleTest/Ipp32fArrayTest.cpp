#include "pch.h"
#include "IpaStripeStream.h"

TEST(Ipp32fTest, mirrorFillTest)
{
	Ipp32fArray smallArray(MtiSize{ 20 , 18 });
	smallArray.iota();

	MtiRect roi = { 3, 5, 4, 6 };
	smallArray.mirrorFill(roi);
	auto fileName = getTempFileName("symmetricFillTest.mat");

	SaveVariablesToMatFile1(fileName, smallArray);
	auto frame = MatIO::readIpp16uArray(getTestFileName("mom_112657.mat"));
	auto inflatedRoi = frame.getRoi().inflate(0, 0, 100, 100);

	Ipp16uArray paddedFrame({ inflatedRoi.getSize(), 3 });
	auto centerRoi = frame.getRoi() + MtiPoint({ 50, 50 });
	paddedFrame(centerRoi) <<= frame;
	paddedFrame.mirrorFill(centerRoi);
	AppendVariablesToMatFile2(fileName, frame, paddedFrame);
}

TEST(Ipp32fTest, mirrorHorizontal)
{
	auto frame = Ipp32fArray(MatIO::readIpp16uArray(getTestFileName("mom_112657.mat")));
	auto reverseFrame = frame.mirror(IppiAxis::ippAxsHorizontal);

	ASSERT_EQ(frame.getSize(), reverseFrame.getSize()) << "Shapes must match";

	const auto d = frame.getComponents();

	for (auto r = 0; r < frame.getHeight(); r++)
	{
		auto q = reverseFrame.getRowPointer(r);
		auto p = frame.getRowPointer(frame.getHeight() - r - 1);
		for (auto c = 0; c < frame.getWidth()*d; c++)
		{
			ASSERT_EQ(*p++, *q++);
		}
	}
	//	auto fileName = getTempFileName("mirrorHorizontal.mat");
	//	SaveVariablesToMatFile2(fileName, frame, reverseFrame);
}

TEST(Ipp32fTest, mirrorVertical)
{
	auto frame = Ipp32fArray(MatIO::readIpp16uArray(getTestFileName("mom_112657.mat")));
	auto reverseFrame = frame.mirror(IppiAxis::ippAxsVertical);
	ASSERT_EQ(frame.getSize(), reverseFrame.getSize()) << "Shapes must match";

	for (auto r = 0; r < frame.getHeight(); r++)
	{
		auto q = reverseFrame.getRowPointer(r);
		auto p = frame.getRowPointer(r);

		for (auto c = 0; c < frame.getWidth(); c++)
		{
			for (auto d = 0; d < reverseFrame.getComponents(); d++)
			{
				ASSERT_EQ(q[3 * (reverseFrame.getWidth() - c - 1) + d], p[3 * c + d]);
			}
		}
	}
}

TEST(Ipp32fTest, ConjoinedTest)
{
	Ipp32fArray image({ 200, 400 });
	IppiRect roi1 = { 20, 10, 100, 50 };
	auto imageRoi1 = image({ 20, 10, 100, 50 });

	ASSERT_TRUE(imageRoi1.doesRoiOverlap({ 20, 10, 100, 50 }));
	ASSERT_TRUE(imageRoi1.doesRoiOverlap({ 0, 10, 100, 50 }));
	ASSERT_TRUE(imageRoi1.doesRoiOverlap({ 0, 0, 100, 50 }));
	ASSERT_TRUE(imageRoi1.doesRoiOverlap({ 20, 0, 100, 50 }));

	ASSERT_FALSE(imageRoi1.doesRoiOverlap({ 120, 10, 100, 50 }));
	ASSERT_FALSE(imageRoi1.doesRoiOverlap({ 0, 61, 100, 50 }));
	ASSERT_FALSE(imageRoi1.doesRoiOverlap({ 0, 0, 10, 20 }));
	ASSERT_FALSE(imageRoi1.doesRoiOverlap({ 0, 60, 10, 50 }));

}

TEST(Ipp32fTest, FullRestoreTest)
{
	Ipp32fArray image({ 200, 400 });
	image.iota();

	IppiRect roi1 = { 20, 10, 100, 50 };
	auto imageRoi1 = image(roi1);
	auto fullImage = image.baseArray();

	ASSERT_TRUE(image == fullImage);
}

TEST(Ipp32fTest, Ipp32fSetTest)
{
	Ipp32fArray image({ 10, 20, 3 });
	image.set({ 1, 2, 3 });

	for (auto r = 0; r < image.getHeight(); r++)
	{
		auto rp = image.getRowPointer(r);
		for (auto c = 0; c < image.getWidth(); c++)
		{
			ASSERT_EQ(1.0f, *rp++);
			ASSERT_EQ(2.0f, *rp++);
			ASSERT_EQ(3.0f, *rp++);
		}
	}
}

TEST(Ipp32fTest, Ipp32fZeroTest)
{
	Ipp32fArray image({ 10, 211, 3 });
	image.iota();
	image.zero();

	auto zees = image.sum();
	for (auto &z : zees)
	{
		ASSERT_EQ(0.0, z);
	}
}

TEST(Ipp32fTest, Ipp32fSumTest)
{
	Ipp32fArray image({ 16,20 });
	image.zero();
	auto z = image.sum();
	ASSERT_EQ(0.0, z[0]);

	image.iota();

	auto v = image.sum();
	auto truth = image.getHeight() * image.getWidth();
	truth = (truth - 1)*(truth) / 2;
	ASSERT_EQ(double(truth), v[0]);
}

TEST(Ipp32fTest, Ipp32fSumRoiTest)
{
	Ipp32fArray image({ 20,16 });
	image.uniformDistribution(0, 100);

	auto subImage = image({ 2, 2, 12, 14 });
	subImage.zero();
	auto z = subImage.sum();
	ASSERT_EQ(0.0, z[0]);

	subImage.iota();

	auto v = subImage.sum();
	auto truth = subImage.getHeight() * subImage.getWidth();
	truth = (truth - 1)*(truth) / 2;
	ASSERT_EQ(double(truth), v[0]);
}

TEST(Ipp32fTest, Ipp32fArithmiticArrayTest)
{
	Ipp32fArray image1({ 16,20 });
	image1.iota();
	image1 = image1 + 1.0f;

	Ipp32fArray image2({ 16,20 });
	image2.uniformDistribution(-10, 10);

	auto vec1 = image1.toVector()[0];
	auto vec2 = image2.toVector()[0];

	auto test = image1 + image2;
	auto vect = test.toVector()[0];
	for (size_t i = 0; i < vect.size(); i++)
	{
		ASSERT_EQ(vec1[i] + vec2[i], vect[i]);
	}

	test = image1 - image2;
	vect = test.toVector()[0];
	for (auto i = 0; i < vect.size(); i++)
	{
		ASSERT_EQ(vec1[i] - vec2[i], vect[i]);
	}

	test = image2 / image1;
	vect = test.toVector()[0];
	for (auto i = 0; i < vect.size(); i++)
	{
		ASSERT_NEAR(vec2[i] / vec1[i], vect[i], float(10E-6));
	}

	test = image2 * image1;
	vect = test.toVector()[0];
	for (auto i = 0; i < vect.size(); i++)
	{
		ASSERT_EQ(vec1[i] * vec2[i], vect[i]);
	}
}


TEST(Ipp32fTest, Ipp32fArithmiticInPlaceScalerTest)
{
	Ipp32fArray image1({ 10, 13 });
	image1.iota();

	auto image1Orig = image1.duplicate();

	auto testValue = 12.34f;
	image1 += testValue;
	for (auto r : image1.rowRange())
	{
		auto rpResult = image1.getRowPointer(r);
		auto rpOrig = image1Orig.getRowPointer(r);

		for (auto c : image1.colRange())
		{
			float rvOrig = *rpOrig++;
			float rvResult = *rpResult++;

			ASSERT_EQ(rvOrig + testValue, rvResult);
		}
	}

	image1 = image1Orig.duplicate();
	image1 -= testValue;

	for (auto r : image1.rowRange())
	{
		auto rpResult = image1.getRowPointer(r);
		auto rpOrig = image1Orig.getRowPointer(r);

		for (auto c : image1.colRange())
		{
			float rvOrig = *rpOrig++;
			float rvResult = *rpResult++;

			ASSERT_EQ(rvOrig - testValue, rvResult);
		}
	}

	image1 = image1Orig.duplicate();
	image1 *= testValue;
	for (auto r : image1.rowRange())
	{
		auto rpResult = image1.getRowPointer(r);
		auto rpOrig = image1Orig.getRowPointer(r);

		for (auto c : image1.colRange())
		{
			float rvOrig = *rpOrig++;
			float rvResult = *rpResult++;

			ASSERT_EQ(rvOrig * testValue, rvResult);
		}
	}

	image1 = image1Orig.duplicate();
	image1 /= testValue;
	for (auto r : image1.rowRange())
	{
		auto rpResult = image1.getRowPointer(r);
		auto rpOrig = image1Orig.getRowPointer(r);

		for (auto c : image1.colRange())
		{
			float rvOrig = *rpOrig++;
			float rvResult = *rpResult++;

			ASSERT_NEAR(rvOrig / testValue, rvResult, 0.00001f);
		}
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
////  Test fixture for Arithmetic Scalar tests

class Ipp32fArrayArithmeticTest : public ::testing::Test
{
protected:
	void SetUp() override
	{
		imageNormal.normalDistribution(10, 3);
		imageIota.iota(1);
		imageNormal3.normalDistribution(10, 3);
		imageIota3.iota({ 10.0f, 100.0f, 200.0f });
	}

	Ipp32fArray imageNormal{ { 16, 20 } };
	Ipp32fArray imageIota{ { 16, 20 } };

	Ipp32fArray imageNormal3{ { 16, 20, 3 } };
	Ipp32fArray imageIota3{ { 16, 20, 3 } };
};

TEST_F(Ipp32fArrayArithmeticTest, ScalarAdd)
{
	auto testArray = imageNormal + 32.1f;
	ASSERT_EQ(imageNormal.getSize(), testArray.getSize());

	auto it = testArray.begin();
	for (auto p : imageNormal)
	{
		ASSERT_EQ(p + 32.1f, *it);
		++it;
	}

	testArray = imageNormal3 + 32.1f;
	ASSERT_EQ(imageNormal3.getSize(), testArray.getSize());

	it = testArray.begin();
	for (auto p : imageNormal3)
	{
		ASSERT_EQ(p + 32.1f, *it);
		++it;
	}
}

TEST_F(Ipp32fArrayArithmeticTest, ScalarDivide)
{
	auto testArray = imageNormal / 22;
	ASSERT_EQ(imageNormal.getSize(), testArray.getSize());

	auto it = testArray.begin();
	for (auto p : imageNormal)
	{
		ASSERT_NEAR(p / 22.0f, *it, 10E-7);
		++it;
	}

	testArray = imageNormal3 / 22;
	ASSERT_EQ(imageNormal3.getSize(), testArray.getSize());

	it = testArray.begin();
	for (auto p : imageNormal3)
	{
		ASSERT_NEAR(p / 22.0f, *it, 10E-7);
		++it;
	}
}

TEST_F(Ipp32fArrayArithmeticTest, ScalarMultiply)
{
	auto testArray = imageNormal * 32.1f;
	ASSERT_EQ(imageNormal.getSize(), testArray.getSize());

	auto it = testArray.begin();
	for (auto p : imageNormal)
	{
		ASSERT_EQ(p * 32.1f, *it);
		++it;
	}

	testArray = imageNormal3 * 32.1f;
	ASSERT_EQ(imageNormal3.getSize(), testArray.getSize());

	it = testArray.begin();
	for (auto p : imageNormal3)
	{
		ASSERT_EQ(p * 32.1f, *it);
		++it;
	}
}

TEST_F(Ipp32fArrayArithmeticTest, ScalarSubtract)
{
	auto testArray = imageNormal - 10.34f;
	ASSERT_EQ(imageNormal.getSize(), testArray.getSize());

	auto it = testArray.begin();
	for (auto p : imageNormal)
	{
		ASSERT_EQ(p - 10.34f, *it);
		++it;
	}

	testArray = imageNormal3 - 32.1f;
	ASSERT_EQ(imageNormal3.getSize(), testArray.getSize());

	it = testArray.begin();
	for (auto p : imageNormal3)
	{
		ASSERT_EQ(p - 32.1f, *it);
		++it;
	}
}

TEST_F(Ipp32fArrayArithmeticTest, ArrayAdd)
{
	auto testArray = imageNormal + imageIota;
	ASSERT_EQ(imageNormal.getSize(), testArray.getSize());

	auto it = testArray.begin();
	auto it0 = imageIota.begin();
	for (auto p : imageNormal)
	{
		ASSERT_EQ(p + *it0, *it);
		++it;
		++it0;
	}

	testArray = imageNormal3 + imageIota3;
	ASSERT_EQ(imageNormal3.getSize(), testArray.getSize());

	it = testArray.begin();
	auto is0 = imageNormal3.begin();
	auto is1 = imageIota3.begin();

	for (auto i : range(testArray.volume()))
	{
		ASSERT_EQ(*is0 + *is1, *it);
		++it;
		++is0;
		++is1;
	}
}

TEST_F(Ipp32fArrayArithmeticTest, ArraySubtract)
{
	auto testArray = imageNormal - imageIota;
	ASSERT_EQ(imageNormal.getSize(), testArray.getSize());

	auto it = testArray.begin();
	auto it1 = imageIota.begin();
	for (auto p : imageNormal)
	{
		ASSERT_EQ(p - *it1, *it);
		++it;
		++it1;
	}

	testArray = imageNormal3 - imageIota3;
	ASSERT_EQ(imageNormal3.getSize(), testArray.getSize());

	it = testArray.begin();
	auto is0 = imageNormal3.begin();
	auto is1 = imageIota3.begin();

	for (auto i : range(testArray.volume()))
	{
		ASSERT_EQ(*is0 - *is1, *it);
		++it;
		++is0;
		++is1;
	}
}

TEST_F(Ipp32fArrayArithmeticTest, ArrayDivide)
{
	auto testArray = imageNormal / imageIota;
	ASSERT_EQ(imageNormal.getSize(), testArray.getSize());

	auto it = testArray.begin();
	auto it1 = imageIota.begin();
	for (auto p : imageNormal)
	{
		ASSERT_NEAR(p / *it1, *it, 10E-7);
		++it;
		++it1;
	}

	testArray = imageNormal3 / imageIota3;
	ASSERT_EQ(imageNormal3.getSize(), testArray.getSize());

	it = testArray.begin();
	auto is0 = imageNormal3.begin();
	auto is1 = imageIota3.begin();

	for (auto i : range(testArray.volume()))
	{
		ASSERT_NEAR(*is0 / *is1, *it, 10E-7);
		++it;
		++is0;
		++is1;
	}
}

TEST_F(Ipp32fArrayArithmeticTest, ArrayMultiply)
{
	auto testArray = imageNormal * imageIota;
	ASSERT_EQ(imageNormal.getSize(), testArray.getSize());

	auto it = testArray.begin();
	auto it1 = imageIota.begin();
	for (auto p : imageNormal)
	{
		ASSERT_NEAR(p * *it1, *it, 10E-7);
		++it;
		++it1;
	}

	testArray = imageNormal3 * imageIota3;
	ASSERT_EQ(imageNormal3.getSize(), testArray.getSize());

	it = testArray.begin();
	auto is0 = imageNormal3.begin();
	auto is1 = imageIota3.begin();

	for (auto i : range(testArray.volume()))
	{
		ASSERT_NEAR(*is0 * *is1, *it, 10E-7);
		++it;
		++is0;
		++is1;
	}
}
TEST(Ipp32fTest, Ipp32fAssignmentTest)
{
	Ipp32fArray image({ 16,20 });
	image.normalDistribution(1000, 200);

	IppiRect roi = { 5, 6, 8, 10 };
	Ipp32fArray subImage = image(roi);
	auto truthSum = subImage.sum()[0];

	auto subImage2 = subImage;
	auto &roi2 = subImage2.getRoi();

	ASSERT_EQ(roi.x, roi2.x);
	ASSERT_EQ(roi.y, roi2.y);
	ASSERT_EQ(roi.width, roi2.width);
	ASSERT_EQ(roi.height, roi2.height);

	ASSERT_EQ(truthSum, subImage2.sum()[0]);
}

TEST(Ipp32fTest, Ipp32fL1NormTest)
{
	Ipp32fArray image({ 10, 20, 3 });
	image.set({ 1, 2, 3 });
	auto norm1 = image.L1Norm();
	auto sum1 = image.sum();
	AssertVectorEqual(sum1, norm1);

	Ipp32fArray box(boxSize);
	box.copyFromPod(BoxArray, box.area());
	auto norm0 = box.L1Norm();
	auto sum0 = box.sum();
	AssertVectorEqual(sum0, norm0);

	Ipp32fArray normal({ 20, 10, 3 });
	normal.iota();
	normal = normal - 100;
	auto norm2 = normal.L1Norm();
	auto sum2 = normal.sum();
	auto lvec = normal.toVector();
	auto ls = lvec.size();
	for (auto i : range(ls))
	{
		auto &vec = lvec[i];
		auto t = std::accumulate(vec.begin(), vec.end(), 0.0f, [](float x, float y) {return x + abs(y); });
		ASSERT_EQ(double(t), norm2[i]);
	}
}

TEST(Ipp32fTest, Ipp32fL2NormTest)
{
	Ipp32fArray normal({ 20, 10, 3 });
	normal.iota();
	normal = normal - 100;
	auto norm2 = normal.L2Norm();
	auto lvec = normal.toVector();
	auto ls = lvec.size();
	for (auto i : range(ls))
	{
		auto &vec = lvec[i];
		auto t = std::accumulate(vec.begin(), vec.end(), 0.0f, [](float x, float y) {return x + (y*y); });
		auto truth = sqrt(double(t));
		ASSERT_NE(truth, 0.0);
		ASSERT_EQ(truth, norm2[i]);
	}
}

TEST(Ipp32fTest, Ipp32fRoiTest)
{
	Ipp32fArray Image({ 10, 20, 3 });
	Image.set({ 1, 2, 3 });

	MtiRect roi = { 5, 10, 3, 4 };
	auto &roiImage = Image(roi);

	ASSERT_EQ(roi.height, roiImage.getHeight());
	ASSERT_EQ(roi.width, roiImage.getWidth());

	// Change the image ROI should change
	auto p = Image.getElementPointer(roi.tl());
	p[0] = 11;
	p[1] = 12;
	p[2] = 13;

	auto s = roiImage.getRowPointer(0);

	// Check the channels are correct at the pixel
	for (auto i = 0; i < 3; i++)
	{
		ASSERT_EQ(p[i], s[i]);
	}

	// To check set at least changes the proper one
	roiImage.set({ 100.0f, 200.0f, 300.0f });
	auto sout = s - 3;
	for (auto i = 0; i < 3; i++)
	{
		ASSERT_EQ(p[i], s[i]);
		ASSERT_NE(p[i], sout[i]);
	}
}


TEST(Ipp32fTest, Ipp32fRoiOfRoiTest)
{
	Ipp32fArray Image({ 10, 20, 3 });
	Image.set({ 1, 2, 3 });

	IppiRect roi = { 5, 10, 3, 4 };
	auto &roiImage = Image(roi);

	ASSERT_EQ(roi.height, roiImage.getHeight());
	ASSERT_EQ(roi.width, roiImage.getWidth());

	roiImage = roiImage({ 1, 1, 2, 3 });

	// Change the image ROI should change
	// NOTE the +1 is the subsubroiImage roi
	auto p = Image.getElementPointer({ roi.x + 1, roi.y + 1 });
	p[0] = 11;
	p[1] = 12;
	p[2] = 13;

	auto s = roiImage.getRowPointer(0);

	// Check the channels are correct at the pixel
	for (auto i = 0; i < 3; i++)
	{
		ASSERT_EQ(p[i], s[i]);
	}

	// To check set at least changes the proper one
	roiImage.set({ 100.0f, 200.0f, 300.0f });
	auto sout = s - 3;
	for (auto i = 0; i < 3; i++)
	{
		ASSERT_EQ(p[i], s[i]);
		ASSERT_NE(p[i], sout[i]);
	}
}

TEST(Ipp32fTest, Ipp32fArrayPodTest)
{
	auto rows = 10;
	auto cols = 30;
	auto p = ippsMalloc_32f(rows * cols);

	// Make the index and values the same
	for (auto i = 0; i < rows*cols; i++)
	{
		p[i] = float(i);
	}

	{
		// See if data agrees, if it was copied
		// the alignment of IppArray allocator woudl be off
		// and the results would be 32 instead of 30 for row(1, 0, 0)
		Ipp32fArray PodBacking({ cols, rows }, p);
		auto v1 = PodBacking[{10, 1}];
		ASSERT_EQ(1.0f * cols + 10.0f, v1);
		auto v2 = PodBacking[{13, 7}];
		ASSERT_EQ(7.0f * cols + 13.0f, v2);

		// Now blow away some data, this should change
		// underlying allocated array.
		auto dep = PodBacking.getElementPointer({ 13, 7 });
		*dep = -345.5f;

		ASSERT_EQ(-345.5f, p[7 * cols + 13]);
	}

	// IppArray is blown away, free should work
	ippsFree(p);
}

TEST(Ipp32fTest, Ipp32fMedianTestEven)
{
	Ipp32fArray image({ 100, 110, 3 });
	float mean = 10000;
	float stddev = 10000.0f / 3.0f;

	image.normalDistribution(mean, stddev);

	auto medians = image.median();

	auto vecs = image.toVector({ 0,0, image.getWidth(), image.getHeight() });
	for (auto vec : vecs)
	{
		auto t0 = getMedianTruth(vec);
		auto v0 = image.median(vec);
		ASSERT_EQ(t0, v0);
	}
}

TEST(Ipp32fTest, Ipp32fMedianTestOdd)
{
	Ipp32fArray image({ 101, 111, 3 });
	float mean = 10000;
	float stddev = 10000.0f / 3.0f;

	image.normalDistribution(mean, stddev);

	auto vecs = image.toVector({ 0,0, image.getWidth(), image.getHeight() });
	for (auto vec : vecs)
	{
		auto t0 = getMedianTruth(vec);
		auto v0 = image.median(vec);
		ASSERT_EQ(t0, v0);
	}
}

TEST(Ipp32fTest, Ipp32fMedianSpeedTestO)
{
	Ipp32fArray image({ 241, 251 });
	float mean = 10000;
	float stddev = 10000.0f / 3.0f;

	image.uniformDistribution(0, 65535);

	CHRTimer hrt;
	auto median2 = 0;
	for (auto i = 0; i < 10; i++)
	{
		auto v0 = image.medianQuick1D();
		median2 = int(v0);
	}

	auto t1 = hrt.intervalMilliseconds();

	// prime the histogram
	FastHistogramIpp::compute(image, 1024, 0, 65536);

	auto counts = image.area();

	hrt.start();
	auto median = 0;
	for (auto i = 0; i < 10; i++)
	{
		auto v0 = FastHistogramIpp::computeIpp(image, 65535, 0, 65536);
		auto sum = 0;
		auto j = 0;
		auto p = v0.data();
		while (sum < counts / 2)
		{
			sum += *p++;
			median = j++;
		}

		median = j;
	}

	auto t2 = hrt.intervalMilliseconds();
	std::wostringstream os;
	os << "Median speed test: " << t1 << ", histogram " << t2 << ", median " << median << ", " << median2;
}

TEST(Ipp32fTest, Ipp32fExtractVectorTest)
{
	Ipp32fArray image({ 10, 6, 3 });
	image.set({ 1,2,3 });
	IppiRect roi = { 2, 3, 4, 2 };
	auto subImage = image(roi);

	vector<Ipp32f> inserts = { 11,12,13 };
	subImage.set(inserts);

	// Not a great test
	auto vecs = image.toVector(roi);
	auto i = 0;
	for (auto vec : vecs)
	{
		ASSERT_EQ(roi.height*roi.width, int(vec.size()));
		auto t0 = inserts[i++] * roi.height*roi.width;
		auto v0 = std::accumulate(vec.begin(), vec.end(), 0.0f);
		ASSERT_EQ(t0, v0);
	}
}

TEST(Ipp32fTest, Ipp32fIotaVectorTest)
{
	Ipp32fArray image({ 11, 12, 3 });
	image.iota();

	// Not a great test
	auto vecs = image.toVector();

	vector<Ipp32f> truth(image.area());
	for (size_t i = 0; i < truth.size(); i++)
	{
		truth[i] = Ipp32f(i);
	}

	for (auto vec : vecs)
	{
		AssertVectorEqual(truth, vec);
	}
}

TEST(Ipp32fTest, SubscriptTest)
{
	Ipp32fArray baseImage({ 6, 5, 3 });
	baseImage.zero();
	auto image = baseImage(baseImage.getRoi().inflate(-1));
	image.uniformDistribution(0, 255);

	auto i = 0;
	auto it = image.begin();
	for (auto r : range(image.getHeight()))
	{
		for (auto c : range(image.getWidth()))
		{
			for (auto d : range(image.getComponents()))
			{
				// For debugging
				if (image[i] != image[{ c, r, d }])
				{
					auto x = r;
				}

				// For some reason, the () are needed
				ASSERT_EQ(image[i], (image[{ c, r, d }]));

				auto v = *it++;
				ASSERT_EQ(image[i], v);
				i++;
			}
		}
	}
}


TEST(Ipp32fTest, IndexWriteTest)
{
	Ipp32fArray baseImage({ 6, 5 });
	baseImage.set({ -1 });
	auto image = baseImage(baseImage.getRoi().inflate(-1));
	image.iota();

	// Read test
	for (auto i = 0; i < image.volume(); i++)
	{
		auto x = image[i];
		ASSERT_EQ(i, int(image[i]));
	}

	// write test
	Ipp32fArray randomImage(image.getSize());
	randomImage.uniformDistribution(0, 255);

	for (auto i = 0; i < image.volume(); i++)
	{
		image[i] = randomImage[i];
	}

	ASSERT_EQ(randomImage, image);
}

/////////////////////////////////////////////
TEST(Ipp32fTest, Ipp32fCopyConstructorTest)
{
	Ipp32fArray image({ 13, 12, 3 });
	image.iota();

	Ipp32fArray copyOfImage(image);
	auto truth = image.area();
	truth = (truth - 1)*truth / 2;

	ASSERT_EQ((Ipp64f)truth, copyOfImage.sum()[0]);
}

TEST(Ipp32fTest, Ipp32fCopyConstructorRoiTest)
{
	Ipp32fArray image({ 13, 12, 3 });
	image.iota();

	IppiRect roi = { 3, 2, 8, 7 };
	Ipp32fArray subImage = image(roi);

	Ipp64f truth = 0;
	for (auto r = roi.y; r < roi.y + roi.height; r++)
	{
		for (auto c = roi.x; c < roi.x + roi.width; c++)
		{
			truth += image[{c, r}];
		}
	}

	ASSERT_EQ((Ipp64f)truth, subImage.sum()[0]);

	Ipp32fArray subSubImage = subImage({ 2, 3, 4, 3 });

	roi = { roi.x + 2, roi.y + 3, 4, 3 };
	auto &r1 = subSubImage.getRoi();
	ASSERT_EQ(roi.x, r1.x);
	ASSERT_EQ(roi.y, r1.y);
	ASSERT_EQ(roi.width, r1.width);
	ASSERT_EQ(roi.height, r1.height);

	truth = 0;
	for (auto r = roi.y; r < roi.y + roi.height; r++)
	{
		for (auto c = roi.x; c < roi.x + roi.width; c++)
		{
			truth += image[{c, r}];
		}
	}

	ASSERT_EQ((Ipp64f)truth, subSubImage.sum()[0]);
}

TEST(Ipp32fTest, Ipp32fRowIteratorTest)
{
	Ipp32fArray image({ 100,50 });
	image.iota();

	auto t = 0.0;
	for (auto p : image.rowIterator())
	{
		for (auto c : range(image.getWidth()))
		{
			t += *p++;
		}
	}

	auto s = image.sum()[0];
	ASSERT_EQ(s, t);
}

TEST(Ipp32fTest, Ipp32fMeanTest)
{
	Ipp32fArray normal({ 20, 11, 3 });
	normal.normalDistribution(101, 200);
	auto mean = normal.mean();

	auto lvec = normal.toVector();
	for (auto i : range(lvec.size()))
	{
		auto &vec = lvec[i];
		auto t = std::accumulate(vec.begin(), vec.end(), 0.0f);
		t = t / normal.area();

		// We want FAST not accurate
		ASSERT_NEAR(double(t), mean[i], 10E-5);
	}

	auto meansOfMeans = std::accumulate(mean.begin(), mean.end(), 0.0);
	auto meansOfMeansTruth = mean[0] + mean[1] + mean[2];
	ASSERT_EQ(meansOfMeansTruth, meansOfMeans);
}

TEST(Ipp32fTest, Ipp32fDotProductTest)
{
	Ipp32fArray n1({ 21, 11, 3 });
	n1.normalDistribution(1, 0.1);
	auto n2 = n1 + 1;

	auto dotProduct = n1.dotProduct(n2);

	vector<Ipp64f> t = { 0,0,0 };
	for (auto r = 0; r < n1.getHeight(); r++)
	{
		auto p1 = n1.getRowPointer(r);
		auto p2 = n2.getRowPointer(r);
		for (auto c = 0; c < n1.getWidth(); c++)
		{
			for (auto k = 0; k < n1.getComponents(); k++)
			{
				t[k] += *p1++ * *p2++;
			}
		}
	}

	for (auto i : range(dotProduct.size()))
	{
		ASSERT_NEAR(t[i], dotProduct[i], 10E-6);
	}
}

TEST(Ipp32fTest, Ipp32fL1NormDiffTest)
{
	Ipp32fArray n1({ 13, 21, 3 });
	n1.normalDistribution(1, 0.1);
	Ipp32fArray n2 = n1 + 1;

	vector<Ipp64f> dotProduct(3);
	n1.L1NormDiff(n2, dotProduct.data());

	vector<Ipp64f> t = { 0,0,0 };
	for (auto r = 0; r < n1.getHeight(); r++)
	{
		auto p1 = n1.getRowPointer(r);
		auto p2 = n2.getRowPointer(r);
		for (auto c = 0; c < n1.getWidth(); c++)
		{
			for (auto k = 0; k < n1.getComponents(); k++)
			{
				t[k] += abs(*p1++ - *p2++);
			}
		}
	}

	for (auto i : range(dotProduct.size()))
	{
		ASSERT_NEAR(t[i], dotProduct[i], 10E-6);
	}
}

TEST(Ipp32fTest, Ipp32fL2NormDiffTest)
{
	Ipp32fArray n1({ 13, 21, 3 });
	n1.normalDistribution(1, 0.1);
	auto n2 = n1 + 1;

	vector<Ipp64f> result(3);
	n1.L2NormDiff(n2, result.data());

	vector<Ipp64f> t = { 0,0,0 };
	for (auto r = 0; r < n1.getHeight(); r++)
	{
		auto p1 = n1.getRowPointer(r);
		auto p2 = n2.getRowPointer(r);
		for (auto c = 0; c < n1.getWidth(); c++)
		{
			for (auto k = 0; k < n1.getComponents(); k++)
			{
				auto v = (*p1++ - *p2++);
				t[k] += v * v;
			}
		}
	}

	for (auto i : range(result.size()))
	{
		ASSERT_NEAR(sqrt(t[i]), result[i], 10E-6);
	}
}

TEST(Ipp32fTest, Ipp32fStupidTest)
{
	Ipp32fArray image({ 100, 4 });
	image.iota();

	auto k1 = 0;
#define N 10000000

	CHRTimer hrt;
	for (auto i : range(N))
	{
		k1++;
		k1 += k1;
	}

	const auto t0 = hrt.intervalMilliseconds();

	int k2 = 0;
	for (auto i = 0; i < N; i++)
	{
		k2++;
		k2 += k2;
	}

	auto t1 = hrt.intervalMilliseconds();
	std::wostringstream os;
	os << "Range Loop: " << t0 << ", C loop: " << t1 << ", " << k1 - k2;
	//	Logger::WriteMessage(os.str().c_str());
}

TEST(Ipp32fTest, ImportNormalizedYuvTest)
{
	auto width = 2000;
	auto height = 720;
	Ipp32fArray dstArray({ width, height, 3 });
	auto numberOfJobs = 4;

	vector<Ipp16u> srcVector(width*height * 3);
	for (auto i : range(srcVector.size()))
	{
		srcVector[i] = i % 1024;
	}

	const auto src = srcVector.data();

	IppiRect jobRoi;
	jobRoi.width = width;

	// Evenly distribute n extra rows over the first n jobs.
	for (auto jobNumber = 0; jobNumber < numberOfJobs; jobNumber++)
	{
		const auto minRowsPerJob = height / numberOfJobs;
		auto extraRows = height % numberOfJobs;
		jobRoi.y = (minRowsPerJob * jobNumber) + std::min<int>(extraRows, jobNumber);
		jobRoi.x = 0;

		auto jobSrc = src + (jobRoi.y * width);
		jobRoi.height = minRowsPerJob + ((jobNumber < extraRows) ? 1 : 0);

		auto jobArray = dstArray(jobRoi);
		jobArray.importNormalizedYuvFromRgb(jobSrc, jobRoi.height, jobRoi.width, 1023);
	}
}

TEST(Ipp32fTest, ResizeDownTest)
{
	auto width = 2000;
	auto height = 1440;
	Ipp32fArray dstArray({ width, height, 3 });
	dstArray.iota();

	auto smallArray = dstArray.resize({ width / 4, height / 4 });
	ASSERT_EQ(smallArray.getWidth(), width / 4);
	ASSERT_EQ(smallArray.getHeight(), height / 4);
}

TEST(Ipp32fTest, ResizeUpTest)
{
	auto width = 500;
	auto height = 200;
	Ipp32fArray dstArray({ width, height, 3 });
	dstArray.iota();

	auto smallArray = dstArray.resize({ width * 4, height * 4 });
	ASSERT_EQ(smallArray.getWidth(), width * 4);
	ASSERT_EQ(smallArray.getHeight(), height * 4);
}

TEST(Ipp32fTest, ResizeAntiAliasingDownTest)
{
	auto width = 2000;
	auto height = 1440;
	Ipp32fArray dstArray({ width, height, 3 });
	dstArray.iota();

	auto smallArray = dstArray.resizeAntiAliasing({ width / 4, height / 4 });
	ASSERT_EQ(smallArray.getWidth(), width / 4);
	ASSERT_EQ(smallArray.getHeight(), height / 4);
}

TEST(Ipp32fTest, CrossCorrNormTest)
{
	Ipp32f sourceData[5 * 4] = { 1.0f, 2.0f, 1.5f, 4.1f,  3.6f,
		0.2f, 3.2f, 2.5f, 1.5f, 10.0f,
		5.0f, 6.8f, 0.5f, 4.1f,  1.1f,
		7.1f, 4.2f, 2.2f, 8.7f, 10.0f };

	Ipp32f probeData[3 * 3] = { 2.1f, 3.5f, 7.7f,
		0.4f, 2.3f, 5.5f,
		1.4f, 2.8f, 3.1f };

	Ipp32fArray source({ 5, 4 }, sourceData, true);
	Ipp32fArray probe({ 3, 3 }, probeData, true);

	auto result = source.crossCorrNorm(probe, IppiROIShape::ippiROISame);

	// This data is taken from Intel but we want the corr coeffs.
	//Ipp32f truthData[] = { 
	//	0.53f, 0.54f, 0.58f, 0.50f, 0.30f,
	//	0.68f, 0.62f, 0.68f, 0.83f, 0.38f,
	//	0.77f, 0.55f, 0.60f, 0.81f, 0.42f,
	//	0.81f, 0.46f, 0.70f, 0.62f, 0.24f };
	Ipp32f truthData[] =
	{
		0.1615f, -0.1421f, -0.1700f, -0.1019f, -0.3599f,
		0.3138f, -0.0835f, -0.1168f,    0.5483f, -0.4280f,
		0.4235f, -0.4948f, -0.3357f,    0.4562f, -0.3942f,
		0.6340f, -0.3785f,    0.3052f,    0.2141f, -0.5274f
	};
	auto resultVector = result.toVector()[0];

	ASSERT_EQ(source.area(), (int)resultVector.size());
	for (auto i : range(resultVector.size()))
	{
		auto x = truthData[i] - resultVector[i];
		ASSERT_NEAR(truthData[i], resultVector[i], 0.01f);
	}

	result = source.crossCorrNorm(probe, IppiROIShape::ippiROIValid);
	resultVector = result.toVector()[0];
	Ipp32f vaildTruthData[] = {
		-0.0835f, -0.1168f,    0.5483f,
		-0.4948f, -0.3357f,    0.4562f
	};

	ASSERT_EQ(6, (int)resultVector.size());
	for (auto i : range(resultVector.size()))
	{
		ASSERT_NEAR(vaildTruthData[i], resultVector[i], 0.01f);
	}
}


TEST(Ipp32fTest, MaxAndIndex1CTest)
{
	Ipp32f testData[] =
	{
		0.1615f, -0.1421f, -0.1700f, -0.1019f, -0.3599f,
		0.3138f, -0.0835f, -0.1168f,  0.5483f, -0.4280f,
		0.4235f, -0.4948f, -0.3357f,  0.4562f, -0.3942f,
		-0.3785f, 0.6340f,  0.3052f,  0.2141f, -0.5274f
	};

	Ipp32fArray testArray({ 5, 4 }, testData, true);
	float mx = -1;
	MtiPoint idx;

	testArray.maxAndIndex(&mx, &idx);

	ASSERT_EQ(0.6340f, mx);
	ASSERT_EQ(1, idx.x);
	ASSERT_EQ(3, idx.y);
}

TEST(Ipp32fTest, MaxAndIndex3CTest)
{
	Ipp32f testData0[] =
	{
		0.1615f, -0.1421f, -0.1700f, -0.1019f, -0.3599f,
		0.3138f, -0.0835f, -0.1168f,  0.5483f, -0.4280f,
		0.4235f, -0.4948f, -0.3357f,  0.4562f, -0.3942f,
		-0.3785f, 0.6340f,  0.3052f,  0.2141f, -0.5274f
	};

	Ipp32f testData1[] =
	{
		-0.1615f, 0.1421f, 0.1700f, 0.1019f, 0.3599f,
		-0.3138f, 0.0835f, 0.5274f,  -0.5483f, 0.4280f,
		-0.4235f, 0.4948f, 0.3357f,  -0.4562f, 0.3942f,
		0.3785f, -0.6340f, -0.3052f,  -0.2141f,  0.1168f
	};

	Ipp32f testData2[] =
	{
		1615, 1421, -1700, -1019, -3599,
		138,  835, -1168,  5483, -4280,
		4235, 4948, -3357, 6340,  -3942,
		-3785, 4562,  3052,  2141, -5274
	};

	Ipp32fArray a0({ 5, 4 }, testData0, true);
	Ipp32fArray a1({ 5, 4 }, testData1, true);
	Ipp32fArray a2({ 5, 4 }, testData2, true);

	MtiPlanar<Ipp32fArray> planes = { a0, a1, a2 };

	float mx[] = { -1, -1, -1 };
	MtiPoint idx[3];

	Ipp32fArray testArray({ a0.getWidth(), a0.getHeight(), 3 });
	testArray.copyFromPlanar(planes);
	testArray.maxAndIndex(mx, idx);

	float truthMx[] = { 0.6340f,  0.5274f, 6340 };
	IppiPoint truthIdx[] = { {1,3}, {2, 1}, {3,2} };

	for (auto i : range(3))
	{
		ASSERT_EQ(truthMx[i], mx[i]);
		ASSERT_EQ(truthIdx[i].x, idx[i].x);
		ASSERT_EQ(truthIdx[i].y, idx[i].y);
	}
}

TEST(Ipp32fTest, CopyBitsOperatorToVoidTest)
{
	Ipp32fArray box(boxSize, BoxArray);
	Ipp32fArray lhs = box.duplicate();

	ASSERT_EQ(boxSize.height, lhs.getHeight());
	ASSERT_EQ(boxSize.width, lhs.getWidth());
	ASSERT_EQ(box.getComponents(), lhs.getComponents());

	ASSERT_TRUE(lhs == box);

	Ipp32fArray bigImage({ 400, 200, 3 });
	bigImage.normalDistribution(0, 3000);

	Ipp32fArray bigLhs;
	bigLhs <<= bigImage;
	ASSERT_TRUE(bigLhs == bigImage);
}

TEST(Ipp32fTest, CopyBitsOperatorToRoiTest)
{
	Ipp32fArray bigImage({ 400, 200, 3 });
	bigImage.normalDistribution(0, 3000);
	auto bigImageOriginalSum = bigImage.sum();

	Ipp32fArray rhs({ 100,50, 3 });
	rhs.iota();

	auto roiBigImage = bigImage(rhs.getRoi());
	auto roiBigImageSum = roiBigImage.sum();

	roiBigImage <<= rhs;

	ASSERT_TRUE(roiBigImage == rhs);

	auto rhsSum = rhs.sum();
	auto bigImageSum = bigImage.sum();

	for (auto c = 0; c < bigImage.getComponents(); c++)
	{
		ASSERT_EQ(bigImageSum[c], bigImageOriginalSum[c] - roiBigImageSum[c] + rhsSum[c]);
	}
}

TEST(Ipp32fTest, CopyBitsOverlapRoiTest)
{
	Ipp32fArray bigImage({ 400, 200 });
	bigImage.normalDistribution(0, 3000);

	// P1 and P2 overlap and refer to same base data
	auto p1 = bigImage({ 0, 0, 100, 50 });
	auto p2 = bigImage({ 10, 10, 100, 50 });

	// Store for later
	auto p1OriginalValues = p1.duplicate();

	// Because we created a new array, the base pointers are not equal
	ASSERT_NE(p1OriginalValues.baseData(), p1.baseData());

	p2 <<= p1;

	// The base pointers should be equal
	ASSERT_EQ(p1.baseData(), p2.baseData());

	//p2 is destroyed and this not equal
	ASSERT_TRUE(p2 != p1OriginalValues);

	// Same tests, just reverse directions
	bigImage.normalDistribution(10, 3000);
	auto p2OriginalValues = p2.duplicate();

	p1 <<= p2;
	// NOTE P2 may have changed!!!!
	ASSERT_TRUE(p1 == p2OriginalValues);
}

TEST(Ipp32fTest, CopyBitsOverlapRoiSpeedTest)
{
#ifdef _DEBUG
	const int width = 1024;
	const int height = 871;
#else
	const int width = 4096;
	const int height = 3351;
#endif 

	Ipp32fArray bigImage({ width, height, 3 });
	bigImage.normalDistribution(0, 3000);

	// P1 and P2 overlap and refer to same base data
	auto p1 = bigImage({ 0, 0, width - 1, height - 1 });
	auto p2 = bigImage({ 1, 1, width - 1, height - 1 });

	CHRTimer hrt;
	for (auto i : range(10))
	{
		p2 <<= p1;
	}

	auto speed1 = hrt.intervalMilliseconds();

	Ipp32fArray noShareArray;
	noShareArray <<= p1;

	hrt.start();

	for (auto i : range(10))
	{
		noShareArray <<= p1;
	}

	auto speed2 = hrt.intervalMilliseconds();

	// Same tests, just reverse directions
	bigImage.iota();

	hrt.start();
	for (auto i : range(10))
	{
		p1 <<= p2;
	}

	auto speed3 = hrt.intervalMilliseconds();

	hrt.start();
	for (auto i : range(10))
	{
		Ipp32fArray temp;
		temp <<= p2;
		p1 <<= temp;
	}

	auto speed4 = hrt.intervalMilliseconds();

	std::wostringstream os;
	os << "Speed overlap 1: " << speed1 << std::endl;
	os << "Speed no overlap: " << speed2 << std::endl;
	os << "Speed overlap 2: " << speed3 << std::endl;
	os << "Speed copy to temp " << speed4 << std::endl;

	//	Logger::WriteMessage(os.str().c_str());

}


TEST(Ipp32fTest, Ipp32fMinEveryTest)
{
	Ipp32fArray s1({ 12, 10 });
	Ipp32fArray s2({ 12, 10 });
	s1.uniformDistribution(0, 0xFFFF);
	s2.uniformDistribution(0, 0xFFFF);

	Ipp32fArray s3;
	s3 <<= s1;

	s3.minEveryInplace(s2);

	auto it1 = s1.begin();
	auto it2 = s2.begin();

	for (auto f : s3)
	{
		auto f1 = *it1++;
		auto f2 = *it2++;

		if (f != std::min<Ipp32f>(f1, f2))
		{
			ASSERT_TRUE(false) << " no match";
		}
	}
}


TEST(Ipp32fTest, Ipp32fAbsDiffInplaceTest)
{
	Ipp32fArray s1({ 12, 10 });
	Ipp32fArray s2({ 12, 10 });
	s1.uniformDistribution(0, 0xFFFF);
	s2.uniformDistribution(0, 0xFFFF);

	Ipp32fArray s3;
	s1.absDiff(s2, s3);

	auto it1 = s1.begin();
	auto it2 = s2.begin();

	for (auto f : s3)
	{
		if (f != abs(*it1++ - *it2++))
		{
			ASSERT_TRUE(false) << " no match";
		}
	}
}

TEST(Ipp32fTest, ConvertOperatorToVoidTest)
{
	Ipp16uArray box({ 200, 100 });
	box.iota();

	Ipp32fArray lhs;
	lhs <<= box;

	auto fbox = (Ipp32fArray)box;

	ASSERT_TRUE(lhs == fbox);

	Ipp16uArray bigImage({ 400, 200, 3 });
	bigImage.normalDistribution(0, 3000);

	Ipp32fArray bigLhs;
	bigLhs <<= bigImage;
	ASSERT_TRUE(bigLhs == (Ipp32fArray)bigImage);
}

TEST(Ipp32fTest, ConvertOperatorFromZeroTest)
{
	Ipp32fArray box({ 200, 100 });
	box.iota();

	auto zeroLengthArray = box({ 10,20,0,5 });

	// Success is this not throwing an error
	box <<= zeroLengthArray;
}

TEST(Ipp32fTest, ConvertFromTest)
{
	Ipp16uArray box({ 200, 100 });
	box.iota();

	Ipp32fArray lhs;
	lhs <<= box;

	auto fbox = Ipp32fArray(box);

	ASSERT_TRUE(lhs == fbox);

	Ipp16uArray bigImage({ 400, 200, 3 });
	bigImage.normalDistribution(0, 3000);

	Ipp32fArray bigLhs(bigImage.getSize());
	bigLhs <<= bigImage;
	ASSERT_TRUE(bigLhs == Ipp32fArray(bigImage));
}

TEST(Ipp32fTest, SharpenC1Test)
{
	Ipp32fArray box(boxSize, BoxArray, true);

	// Not a true test, just making sure it retursn something
	auto sharpenedBox = box.sharpen();
	ASSERT_EQ(box.getWidth(), sharpenedBox.getWidth());
	ASSERT_EQ(box.getHeight(), sharpenedBox.getHeight());
}

TEST(Ipp32fTest, Ipp32fSimpleBoxFilterTest)
{
	Ipp32fArray image({ 512, 376 });
	image.uniformDistribution(0, 200);
	const int KERNEL_RADIUS = 10;

	IppiSize boxSize = { 2 * KERNEL_RADIUS + 1, 2 * KERNEL_RADIUS + 1 };
	auto filteredImage = image.applyBoxFilter(boxSize);

	// Check the box filter is computing the correct valid section
	IppiRect convRoi = { KERNEL_RADIUS, KERNEL_RADIUS, image.getWidth() - 2 * KERNEL_RADIUS, image.getHeight() - 2 * KERNEL_RADIUS };
	for (auto r = 0; r < convRoi.height; r++)
	{
		for (auto c = 0; c < convRoi.width; c++)
		{
			auto s = image({ c, r, 21, 21 }).sum()[0] / (21 * 21);
			auto p = filteredImage.getElementPointer({ c + convRoi.x , r + convRoi.y });
			if (abs(s - *p) >= 0.001f)
			{
				ASSERT_EQ((double)*p, s);
			}
		}
	}
}

#define BOX_COLS 512
#define BOX_ROWS 376

TEST(Ipp32fTest, Ipp32fValidBoxFilterTest)
{
	Ipp32fArray image({ BOX_COLS, BOX_ROWS });
	image.uniformDistribution(0, 200);
	const int KERNEL_RADIUS = 10;

	MtiSize boxSize = { 2 * KERNEL_RADIUS + 1, 2 * KERNEL_RADIUS + 1 };

	auto convRoi = image.getRoi().inflate(-KERNEL_RADIUS);
	Ipp32fArray filteredImage;
	Ipp8uArray scratch;

	image.applyBoxFilterValid(boxSize, filteredImage, scratch);

	// Check the box filter is computing the correct valid section
	for (auto r = 0; r < convRoi.height; r++)
	{
		for (auto c = 0; c < convRoi.width; c++)
		{
			auto s = image({ c, r, boxSize.width, boxSize.height }).sum()[0] / boxSize.area();
			auto p = filteredImage.getElementPointer({ c, r });
			if (abs(s - *p) >= 0.001f)
			{
				ASSERT_EQ((double)*p, s);
			}
		}
	}
}
TEST(Ipp32fTest, BoxL1NormTest)
{
	Ipp32fArray image({ 512, 376 });
	image.normalDistribution(100, 3);
	Ipp32fArray L1Norm;
	Ipp8uArray scratch;

	// First call does an allocation
	auto convRoi = image.applyBoxFilter({ 21, 21 }, L1Norm, scratch);

	std::ostringstream os;
	os << std::endl;

	CHRTimer hrt;
	const int loops = 100;
	for (auto i = 0; i < loops; i++)
	{
		image.applyBoxFilter({ 21, 21 }, L1Norm, scratch);
	}

	auto t0 = hrt.elapsedMilliseconds() / loops;
	os << "In-place box filter time: " << t0 << std::endl;

	auto boxFilter = image.applyBoxFilter({ 21, 21 });
	ASSERT_TRUE(boxFilter == L1Norm) << " The flyweight and functional are basically the same";

	// Check the box filter is computing the correct valid section
	hrt.start();
	for (auto r = 0; r < convRoi.height; r++)
	{
		for (auto c = 0; c < convRoi.width; c++)
		{
			auto s = image({ c, r, 21, 21 }).sum()[0] / (21 * 21);
			auto p = L1Norm.getElementPointer({ c + convRoi.x, r + convRoi.y });
			ASSERT_NEAR(double(*p), s, 0.001f);
		}
	}

	auto t1 = hrt.elapsedMilliseconds();

	Ipp8uArray scratch8u;
	Ipp32fArray L1NormValid;
	auto validRoi = image.applyBoxFilterValid({ 21, 21 }, L1NormValid, scratch8u);

	// Check the valid box filter is computing the correct valid section
	for (auto r = 0; r < validRoi.height; r++)
	{
		for (auto c = 0; c < validRoi.width; c++)
		{
			auto s = image({ c, r, 21, 21 }).sum()[0] / (21 * 21);
			auto p = L1NormValid.getElementPointer({ c, r });
			ASSERT_NEAR(double(*p), s, 0.001f);
		}
	}

	auto c = os.str();
	auto d = c;
}

TEST(Ipp32fTest, Ipp32fSqrtInPlaceTest)
{
	Ipp32fArray image({ 512, 376 });
	image.iota();

	Ipp32fArray image2;
	image2 <<= image;

	image2.sqrtInPlace();

	auto p = image2.begin();
	for (auto r : image)
	{
		auto sq = *p++;
		auto tsq = ::sqrt(r);
		if (abs(sq - tsq) >= 10E-5f)
		{
			ASSERT_EQ(tsq, sq);
		}
	}
}

TEST(Ipp32fTest, Ipp32fSqrtTest)
{
	Ipp32fArray image({ 512, 376 });
	image.iota();

	auto image2 = image.sqrt();
	auto p = image2.begin();
	for (auto r : image)
	{
		auto sq = *p++;
		auto tsq = ::sqrt(r);
		if (abs(sq - tsq) >= 10E-3f)
		{
			ASSERT_EQ(tsq, sq);
		}
	}
}

TEST(Ipp32fTest, Ipp32fWriteMultipleMat)
{
	Ipp8uArray base8uImage({ 7,13 });
	base8uImage.iota();

	Ipp32fArray base32fImage({ 10, 6, 3 });
	base32fImage.iota();

	Ipp16uArray base16uImage({ 16, 10, 3 });
	base16uImage.set({ 100, 200, 300 });

	base16uImage({ 3, 2, 10, 6 }) <<= base32fImage;
	auto width = base16uImage.getWidth();
	auto height = base16uImage.getHeight();
	std::string fileName = "c:\\temp\\Ipp32fWriteMultipleMat.mat";

	SaveVariablesToMatFile3(fileName, base8uImage, base32fImage, base16uImage);
	AppendVariablesToMatFile2(fileName, width, height);

	std::string infoString = "This is a test of saving a string";
	AppendVariablesToMatFile1(fileName, infoString);
}

TEST(Ipp32fTest, IppMatReadWrite32fTest)
{
	Ipp32fArray baseImage({ 9, 10 });
	baseImage.iota();

	MtiPlanar<Ipp32fArray> planes = { baseImage, baseImage + 100, baseImage - 40 };
	Ipp32fArray image1;
	image1.copyFromPlanar(planes);
	auto imageRoi = image1({ 1, 1, 7, 8 });

	auto fileName = "c:\\temp\\IppMatReadWrite32fTest.mat";
	MatIO::saveListToMatFile(fileName, image1, "m_3", baseImage, "baseImage");

	// read it back in
	auto image1Test = MatIO::readIpp32fArray(fileName);
	ASSERT_TRUE(image1 == image1Test);

	AppendVariablesToMatFile1(fileName, imageRoi);
	auto imageRoiTest = MatIO::readIpp32fArray(fileName, "imageRoi");
	ASSERT_TRUE(imageRoi == imageRoiTest);

	auto baseImageTest = MatIO::readIpp32fArray(fileName, "baseImage");
	ASSERT_TRUE(baseImage == baseImageTest);
}

TEST(Ipp32fTest, Ipp32fConvSameTest)
{
	Ipp32fArray source({ 20, 1 });
	source.iota();
	source += 1;

	Ipp32fArray kernel({ 5, 1 });
	kernel.iota();
	kernel += 1;

	Ipp8uArray scratch;
	Ipp32fArray fullConv;
	auto sameConv = source.convFullReturnCenter(kernel, fullConv, scratch);

	vector<float> truthFullVect = { 1, 4, 10,  20,  35,  50,  65,  80,  95,  110,  125,  140,  155,  170,  185,  200,  215,  230,  245,  260,  254,  226, 175, 100 };
	Ipp32fArray truthFull({ int(truthFullVect.size()), 1 }, truthFullVect.data());
	auto truthSame = truthFull({ 2, 0, 20, 1 });
	ASSERT_EQ(truthFull, fullConv);
	ASSERT_EQ(truthSame, sameConv);
}

TEST(Ipp32fTest, Ipp32fFliplrTest)
{
	Ipp32fArray bigSource({ 20, 12 });
	bigSource.set({ -1 });
	auto source = bigSource({ 2, 3, 7, 9 });
	source.iota();

	auto test = source.fliplr();
	ASSERT_EQ(test.getSize(), source.getSize());

	for (auto r = 0; r < test.getHeight(); r++)
	{
		auto p = test.getRowPointer(r);
		auto q = source.getRowPointer(r);
		for (auto c = 0; c < source.getWidth(); c++)
		{
			ASSERT_EQ(q[test.getWidth() - c - 1], p[c]);
		}
	}

	Ipp32fArray bigSource3({ 20, 12, 3 });
	bigSource3.set({ -1, -2, -3 });
	auto source3 = bigSource3({ 3, 2, 9, 6 });
	source3.uniformDistribution(0, 10);
	source3.iota();

	auto test3 = source3.fliplr();
	for (auto r = 0; r < test3.getHeight(); r++)
	{
		auto q = test3.getRowPointer(r);
		auto p = source3.getRowPointer(r);
		for (auto c = 0; c < source.getWidth(); c++)
		{
			for (auto d = 0; d < test3.getComponents(); d++)
			{
				ASSERT_EQ(q[3 * (test3.getWidth() - c - 1) + d], p[3 * c + d]);
			}
		}
	}
}

TEST(Ipp32fTest, isSontiguousTest)
{
	// References to raw data are contiguous
	vector<float> vectorData(2200);
	Ipp32fArray testArray0({ 20,11 }, vectorData.data());
	ASSERT_TRUE(testArray0.isContiguous());

	// Sub ROI may not be
	auto testArray1 = testArray0({ 1, 2, 5, 6 });
	ASSERT_FALSE(testArray1.isContiguous());

	// A slice is contiguous
	auto testArray2 = testArray0({ 0, 3, testArray0.getWidth(), 6 });
	ASSERT_TRUE(testArray2.isContiguous());

	// The default new array contiguous
	// We need a flag to force it V2.0
	Ipp32fArray testArray3({ 11, 12, 3 });
	ASSERT_TRUE(testArray3.isContiguous());
}

TEST(Ipp32fTest, solveSymmetric)
{
	float dataA[] =
	{
		 0.050099999999893f, -0.027541143283212f, 0.027132572777289f, -0.028100594301911f, 0.031487074161912f, -0.036640349374337f,
		-0.027541143283208f, 0.383799477862760f, -0.243130868116934f, 0.338590656749362f, -0.393319017681636f, 0.488486987793473f,
		 0.027132572777284f, -0.243130868116931f, 0.440892790181596f, -0.571627301504749f, 0.755167384913644f, -0.974205268664738f,
		-0.028100594301911f, 0.338590656749366f, -0.571627301504763f, 0.844047254077451f, -1.158251741944014f, 1.554348191524827f,
		 0.031487074161897f, -0.393319017681623f, 0.755167384913643f, -1.158251741943984f, 1.653486992495334f, -2.281472069968222f,
		-0.036640349374334f, 0.488486987793476f, -0.974205268664755f, 1.554348191524812f, -2.281472069968250f, 3.222723690424949f
	};

	float dataY[] = { 0.1753500f, 0.8206430f, -0.6341400f, 0.9056530f, -1.1315700f, 1.4811211f };

	auto n = 6;
	Ipp32fArray A({ n,n }, dataA);
	Ipp32fArray Y(n, dataY);

	auto X = A.solve(Y);

	for (auto i : range(n))
	{
		auto row = A({ 0, i, n, 1 });
		auto t = (row * X).sum()[0];
		EXPECT_NEAR(t, Y[i], 10E-5);
	}
}

TEST(Ipp32fTest, solveGeneral)
{
	float dataA[] =
	{
	 1.0f, 1.0f, 2.0f,
	 2.0f, 3.0f, 2.0f,
	 3.0f, 3.0f, 2.0f
	};

	float dataY[] = { 1.0f, 4.0f, 15.0f };

	auto n = 3;
	Ipp32fArray A({ n,n }, dataA);
	Ipp32fArray Y(n, dataY);

	auto X = A.solve(Y);

	for (auto i : range(n))
	{
		auto row = A({ 0, i, n, 1 });
		auto t = (row * X).sum()[0];
		auto x = t - Y[i];
		EXPECT_NEAR(t, Y[i], 10E-6);
	}
}

TEST(Ipp32fTest, NonUniformLut1D)
{
	// example from ipp doc, we should get better truth
	Ipp32f rawTruth[] =
	{
		0.20f, 0.61f, 0.85f, 0.93f, 0.93f, 0.85f, 0.61f, 0.20f,
		0.61f, 0.93f, 1.00f, 0.99f, 0.99f, 1.00f, 0.93f, 0.61f,
		0.85f, 1.00f, 0.95f, 0.89f, 0.89f, 0.95f, 1.00f, 0.85f,
		0.93f, 0.99f, 0.89f, 0.82f, 0.82f, 0.89f, 0.99f, 0.93f,
		0.93f, 0.99f, 0.89f, 0.82f, 0.82f, 0.89f, 0.99f, 0.93f,
		0.85f, 1.00f, 0.95f, 0.89f, 0.89f, 0.95f, 1.00f, 0.85f,
		0.61f, 0.93f, 1.00f, 0.99f, 0.99f, 1.00f, 0.93f, 0.61f,
		0.20f, 0.61f, 0.85f, 0.93f, 0.93f, 0.85f, 0.61f, 0.20f,
	};

	Ipp32f rawValues[] = { 0.2f, 0.4f, 0.6f, 0.8f, 1.0f };
	Ipp32f rawLevels[] = { 0.0f, 0.128f, 0.256f, 0.512f, 1.0f };
	Ipp32f rawData[8 * 8];
	
	ippiImageJaehne_32f_C1R(rawData, 8 * sizeof(float), { 8,8 });

	Ipp32fArray testArray({ 8, 8 }, rawData);
	Ipp32fArray lut(5, rawValues);
	Ipp32fArray levels(5, rawLevels);
	Ipp8uArray scratch;
	auto result = testArray.applyLut({ lut }, levels, scratch);
	for (auto i : range(result.area()))
	{
		ASSERT_NEAR(rawTruth[i], result[i], 0.005f);
	}
}

TEST(Ipp32fTest, UniformLut1D)
{
	Ipp32fArray testArray({ 512, 384 });
	testArray.iota();
	auto maxValue = 512 * 384;

	// Do a lot of equally spaced points at integer positions
	auto lutVector = MtiMath::linspace(float(maxValue), 0.0f, 1025);
	Ipp32fArray lut(lutVector.size(), lutVector.data());
	Ipp8uArray scratch;
	
	auto result = testArray.applyLut({ lut }, scratch, maxValue);
	auto t = result.toVector()[0];
	for (auto i : range(result.area()))
	{
		ASSERT_EQ(maxValue - i, result[i]);
	}
}

TEST(Ipp32fTest, UniformLut3D)
{
	Ipp32fArray testArray({ 512, 384, 3 });
	testArray.iota({0, 0, 0});
	auto maxValue = 512 * 384;

	// Do a lot of equally spaced points at integer positions
	auto lutVector = MtiMath::linspace(float(maxValue), 0.0f, 1025);
	Ipp32fArray lut(lutVector.size(), lutVector.data());
	Ipp8uArray scratch;

	auto result = testArray.applyLut({ lut }, scratch, maxValue);
	
	auto i = 0;
	for (auto r : range(result.getHeight()))
	{
		auto rp = result.getRowPointer(r);
		for (auto c : range(result.getWidth()))
		{
			ASSERT_EQ(maxValue - i, *rp++);
			ASSERT_EQ(maxValue - i, *rp++);
			ASSERT_EQ(maxValue - i, *rp++);
			++i;
		}
	}

	// Now apply different (do nothing) lut
	auto lutVector1 = MtiMath::linspace(0.0f, float(maxValue), 1025);
	Ipp32fArray lut1(lutVector1.size(), lutVector1.data());
	
	auto minValue2 = maxValue / 4.0f;
	auto lutVector2 = MtiMath::linspace(minValue2, 3* minValue2, 1025);
	Ipp32fArray lut2(lutVector2.size(), lutVector2.data());

	result = testArray.applyLut({ lut, lut1, lut2 }, scratch, maxValue);
	i = 0;
	for (auto r : range(result.getHeight()))
	{
		auto rp = result.getRowPointer(r);
		for (auto c : range(result.getWidth()))
		{
			ASSERT_EQ(maxValue - i, *rp++);
			ASSERT_EQ(i, *rp++);

			auto m = (3.0 * minValue2 - minValue2) / maxValue;
			ASSERT_EQ(float(m*i + minValue2), *rp++);
			//ASSERT_NEAR(float(m*i + minValue2), *rp++, 0.00001f);
			++i;
		}
	}
}

TEST(Ipp32fTest, UniformLut3DInPlace)
{
	Ipp32fArray testArray({ 512, 384, 3 });
	testArray.iota({ 0, 0, 0 });
	auto maxValue = 512 * 384;

	// Do a lot of equally spaced points at integer positions
	auto lutVector = MtiMath::linspace(float(maxValue), 0.0f, 1025);
	Ipp32fArray lut(lutVector.size(), lutVector.data());

	auto lutVector1 = MtiMath::linspace(0.0f, float(maxValue), 1025);
	Ipp32fArray lut1(lutVector1.size(), lutVector1.data());

	auto minValue2 = maxValue / 4.0f;
	auto lutVector2 = MtiMath::linspace(minValue2, 3 * minValue2, 1025);
	Ipp32fArray lut2(lutVector2.size(), lutVector2.data());

	Ipp8uArray scratch;
	testArray.applyLutInPlace({ lut, lut1, lut2 }, scratch, maxValue);
	auto t0 = testArray.toVector()[0];
	auto i = 0;
	for (auto r : range(testArray.getHeight()))
	{
		auto rp = testArray.getRowPointer(r);
		for (auto c : range(testArray.getWidth()))
		{
			ASSERT_EQ(maxValue - i, *rp++);
			ASSERT_EQ(i, *rp++);

			auto m = (3.0 * minValue2 - minValue2) / maxValue;
			ASSERT_EQ(float(m*i + minValue2), *rp++);
			++i;
		}
	}
}

TEST(Ipp32fTest, LutInvertLutTest)
{
	auto fileName = getTestFileName("WadingBirdLut.mat");
	auto lut = MatIO::readIpp32fArray(fileName, "wadingBirdLut");
	Ipp32fArray testArray({ 512, 384 });
	testArray.uniformDistribution(0, 1023);
	Ipp8uArray scratch;
	auto lutted = testArray.applyLut({ lut }, scratch, 1023.0f);

	auto lutVector2 = MtiMath::linspace(0.0f, 1023.0f, 1024);
	Ipp32fArray inverseLut(1024, lutVector2.data());
	lutted.applyLutInPlace({ inverseLut }, lut, scratch);

	for (auto i : range(testArray.area()))
	{
		ASSERT_NEAR(testArray[i], lutted[i], 0.0001f);
	}
}

TEST(Ipp32fTest, Ipp32fConvert32sTest)
{
	Ipp32fArray image({ 100, 4, 3 });
	image.iota({ 1, -100, 30 });

	auto v = image.toVector()[0];
	auto intArray = Ipp32sArray(image);
	auto f = intArray.toVector()[0];
	for (auto i : range(image.volume()))
	{
		ASSERT_FLOAT_EQ(intArray[i], image[i]);
	}
}

TEST(Ipp32fTest, Ipp32fLogLutTest)
{
	int lutSize = 1024;
	auto logLut = Ipp32fArray::logLut(lutSize, 65535, 100);
	auto axisLut = Ipp32fArray::linspace(0.0f, 65535.0f, lutSize);

	auto fileName = getTestFileName("mom_112657.mat");

	// Mom is 16 bits
	auto momImage = Ipp32fArray(MatIO::readIpp16uArray(fileName));
	momImage = momImage.resize({ 2048, 1556 });
	auto testImage = momImage.duplicate();
	
	Ipp8uArray scratch;
	CHRTimer hrt;
	testImage.applyLutInPlace({ logLut }, axisLut, scratch);
	auto x = hrt.intervalMilliseconds();
//	MatIO::appendListToMatFile("c:\\temp\\test.mat", testImage, "momImageLut");
	testImage.applyLutInPlace({ axisLut }, logLut, scratch);
//	MatIO::appendListToMatFile("c:\\temp\\test.mat", testImage, "momImageInverse");
	auto y = hrt.intervalMilliseconds();
	for (auto i : range(momImage.volume()))
	{
		ASSERT_NEAR(momImage[i], testImage[i], 1.0) << i;
	}	
}
//TEST(Ipp32fTest, convertSpeedTest)
//{
//	const int R = 3112;
//	const int C = 4096;
//
//	Ipp32sArray test32s({ C, R });
//	test32s.uniformDistribution(-10000, 10000);
//
//	Ipp32fArray test32f({ C, R });
//	CHRTimer hrt;
//	test32f <<= test32s;
//	auto t1 = hrt.elapsedMilliseconds();
//
//	IpaStripeStream iss;
//	iss << test32s;
//	iss << [&](const Ipp32sArray &stripe)
//	{
//		test32f(stripe.getRoi()) <<= stripe;
//	};
//
//	hrt.start();
//	iss.stripe();
//	auto t2 = hrt.elapsedMilliseconds();
//
//	DBTRACE(t1);
//	DBTRACE(t2);
//}

