﻿#include "pch.h"

#include "LabelMarker.h"
#include "IpaStripeStream.h"
#include "ImageBinSorter.h"
TEST(LableMarkers, constructorTest)
{
	LabelMarker labelMarker;
	ASSERT_EQ(IppiNorm::ippiNormInf, labelMarker.getConnectNorm()) << "Default must be ippiNormInf";

	LabelMarker labelMarker2(IppiNorm::ippiNormL1);
	ASSERT_EQ(IppiNorm::ippiNormL1, labelMarker2.getConnectNorm()) << "Must be set to ippiNormL1";

}

TEST(LableMarkers, equivalenctIndicesTest)
{
   Ipp16uArray image({ 300, 200 });
   image.uniformDistribution(0, 99);

   LabelMarker labelMarker;
   auto result = labelMarker.getEquivalenceIndices(image);
   ASSERT_EQ(100, int(result.size()));

   auto sum = 0;
   for (auto &bin : result)
   {
      sum += int(bin.size());
   }

   ASSERT_EQ(image.area(), sum);

   for (auto &bin :result)
   {
      // check each bin is non-zero and has same value
      auto binV = image.fromPixelIndices(bin);
      auto v = binV[0];
      for (auto w : binV)
      {
            ASSERT_EQ(v, w);
      }
   }
}

TEST(LableMarkers, connectedTest)
{
	Ipp8uArray smallArray({ 20, 20 });

	auto n = smallArray.area();
	srand(1000);
	for (auto i : range(20*4))
	{
		smallArray[rand() % n] = 1;
	}

	LabelMarker labelMarker;
	auto result = labelMarker.label(smallArray);
	n = labelMarker.getNumberOfLabels();
	auto r = Ipp32fArray(result);
	SaveVariablesToMatFile1(R"(c:\temp\test.mat)", r);
}

//TEST(LableMarkers, largeConnectedTest)
//{
//	auto fileName = getTestFileName("repairMask8u.mat");
//	auto mask8u = MatIO::readIpp8uArray(fileName);
//
//	LabelMarker labelMarker;
//	CHRTimer hrt;
//	auto result = labelMarker.label(mask8u);
//	auto t = hrt.elapsedMilliseconds();
//	auto n = labelMarker.getNumberOfLabels();
//	auto r = Ipp32fArray(result);
//	SaveVariablesToMatFile1(R"(c:\temp\test.mat)", r);
//}

TEST(LableMarkers, indexSort)
{
	auto fileName = getTestFileName(R"(IppDisplay\111289.mat)");
	auto imageRgb = MatIO::readIpp16uArray(fileName);
	auto planes = imageRgb.copyToPlanar();
	auto diff = Ipp32fArray(planes[1]) - Ipp32fArray(planes[2]);
	diff /= 65535.0f;

	auto grayImage = imageRgb.toGray();
	SaveVariablesToMatFile2(R"(c:\temp\data.mat)", grayImage, diff);
	CHRTimer hrt;
	auto t0 = hrt.intervalMilliseconds();
	auto edges = FastHistogramIpp::compute(grayImage, 65536, -0.5, 65535.5)[0];
	auto t11 = hrt.intervalMilliseconds();

	auto p = edges.data();    

	for (auto c : range(edges.size() - 1))
	{
		p[c+1] = p[c+1] + p[c];
	}

	vector<Ipp32s> storage(grayImage.area());
	auto storagePtr = storage.data();
	vector<Ipp32s> count(65535);
	Ipp32s idx = 0;
	auto cp = count.data();
	auto t4 = hrt.intervalMilliseconds();
	for (auto r = 0; r < grayImage.getHeight(); r++)
	{
		auto sp = grayImage.getRowPointer(r);
		for (auto c = 0; c < grayImage.getWidth(); c++)
		{
			auto v = *sp++;
			*(storagePtr + edges[v] + count[v]) = idx;
			idx++;
			count[v]++;
		}
	}

	SaveVariablesToMatFile2(R"(c:\temp\edges.mat)", storage, count);

	auto t1 = hrt.intervalMilliseconds();
	SaveVariablesToMatFile2(R"(c:\temp\histogram.mat)", edges, count);

	auto contrastBins = 100;
	auto xInc = double(grayImage.area()) / double(contrastBins);
	auto lowIndex = 0;

	// This can be done before hand
	//auto contrastBinPointer = count.data();

	//for (auto c : range(edges.size() - 1))
	//{
	//	contrastBinPointer[c + 1] = contrastBinPointer[c + 1] + contrastBinPointer[c];
	//}

	vector<Ipp32sArray> intervals;
	vector<Ipp32fArray> bins;
	for (auto i = 0; i < contrastBins; i++)
	{
		int highIndex = round((i + 1) * xInc);
		intervals.push_back({ { highIndex - lowIndex }, storagePtr + lowIndex });
		bins.emplace_back(highIndex - lowIndex);
		lowIndex = highIndex;
		//, storagePtr + edges[i], true);
	}

	auto t13 = hrt.intervalMilliseconds();

	IpaStripeStream iss;

	int maxIndex = 0;
	std::mutex g_i_mutex;
	iss << [&](const int i)
	{
		auto intervalPtr = intervals[i].data();
		auto binPtr = bins[i].data();
		const auto diffPtr = diff.data();

		for (auto c = 0; c < intervals[i].getWidth(); c++)
		{
			*binPtr++ = *(diffPtr + *intervalPtr++);
		}

		IppThrowOnError(ippsSortAscend_32f_I(bins[i].data(), bins[i].getWidth()));
		std::lock_guard<std::mutex> lock(g_i_mutex);
		maxIndex = std::max<int>(maxIndex, i);
	};

	iss << contrastBins;
	iss.stripe();

	auto t14 = hrt.intervalMilliseconds();
	//Ipp32sArray A(image.getSize(), storage.data());
	Ipp32fArray A = bins[82];
	Ipp32fArray B = bins[21];

	ImageBinSorter imageBinSorter;
	auto myBins = imageBinSorter.sortDataToBins(grayImage, diff, contrastBins);
	t1 = hrt.intervalMilliseconds();
	myBins = imageBinSorter.sortDataToBins(grayImage, diff, contrastBins);
	auto t2 = hrt.intervalMilliseconds();
	Ipp32fArray AA = myBins[82];
	Ipp32fArray BB = myBins[21];
//	auto c = myBins[82] == A;
	 SaveVariablesToMatFile4(R"(c:\temp\test.mat)", A, B, AA, myBins);


	auto t21 = hrt.intervalMilliseconds();
	auto status = ippsSortAscend_16u_I(grayImage.data(), grayImage.area()/2);
	//auto d = 3;
	//for (auto r = 0; r < imageRgb.getHeight(); r++)
	//{
	//	auto p = imageRgb.getRowPointer(r);
	//	for (auto c = 0; c < imageRgb.getWidth(); c++)
	//	{
	//	}
	//}
	auto t3 = hrt.intervalMilliseconds();

	auto x = t0 + t21 + t11+ t1 + t2 + t3 + t14 + t4 + t13;
//	SaveVariablesToMatFile1(R"(c:\temp\test.mat)", x);
}

TEST(LableMarkers, dilateErodeTest)
{
	auto diskSmall = Ipp8uArray::createDisk(20);
	
	// invert disk
	for (auto &p : diskSmall)
	{
		p = (p > 0) ? 0 : 1;
	}

	auto diskBig = Ipp8uArray::createDisk(22);

	Ipp32fArray dilateBaseArray({ 200, 150 });
	dilateBaseArray.zero();

	dilateBaseArray(diskBig.getRoi()) <<= diskBig;

	auto bigRoi = diskBig.getRoi();
	auto smallRoi = diskSmall.getRoi();

	dilateBaseArray(bigRoi + MtiPoint({50,50})) <<= diskBig;
	dilateBaseArray(smallRoi + MtiPoint({58,54})) <<= diskSmall;

//	SaveVariablesToMatFile3(R"(c:\temp\test.mat)", diskSmall, diskBig, dilateBaseArray);

	Ipp8uArray scratchArray;
	auto maskDisk = Ipp8uArray::createDisk(2);
	auto erodeBaseArray = dilateBaseArray.duplicate();

	auto testDilate = dilateBaseArray.dilate(maskDisk, scratchArray);
	auto testErode = dilateBaseArray.erode(Ipp8uArray::createDisk(2), scratchArray);
//	AppendVariablesToMatFile2(R"(c:\temp\test.mat)", testDilate, testErode);
}

//TEST(LableMarkers, indexSort32f)
//{
//	auto fileName = getTestFileName(R"(IppDisplay\111289.mat)");
//	auto imageRgb = MatIO::readIpp16uArray(fileName);
//	auto planes = imageRgb.copyToPlanar();
//
//	auto diff = Ipp32fArray(planes[1]) - Ipp32fArray(planes[2]);
//	auto grayImage = Ipp32fArray(imageRgb.toGray()) / 65535.0f;
//
//	int contrastBins = 100;
//	ImageBinSorter imageBinSorter;
//	auto myBins = imageBinSorter.sortDataToBins(grayImage, diff, contrastBins);
//}