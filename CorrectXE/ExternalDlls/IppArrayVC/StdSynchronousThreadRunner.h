//---------------------------------------------------------------------------

#ifndef SynchronousThreadRunnerH
#define SynchronousThreadRunnerH
//---------------------------------------------------------------------------

#include <atomic>
#include <mutex>

#include "MtiCore.h"
#include "Ippheaders.h"

//---------------------------------------------------------------------------

class SynchronousThreadRunner
{
	typedef int (*ThreadWorkFunctionType)(void *, int job, int totalJobs);

public:
	SynchronousThreadRunner(int nJobs, void *parameter, ThreadWorkFunctionType primaryFunctionPtr);
	SynchronousThreadRunner(int nJobs, int nThreads, void *parameter, ThreadWorkFunctionType primaryFunctionPtr);
	SynchronousThreadRunner(void *parameter, ThreadWorkFunctionType primaryFunctionPtr);
	~SynchronousThreadRunner();

	int GetNumberOfThreads();
	static MtiRect findSliceRoi(int jobNumber, int totalJobs, const IppiSize & size);

	int Run();

private:

	ThreadWorkFunctionType _workFunction = nullptr;
	void *_workFunctionParam = nullptr;
	int _totalNumberOfJobs = 0;
	int _numberOfThreadsToUse = 64;
	int _numberOfJobsRun = 0;
    int _numberOfThreadsStillRunning = 0;
	int _firstError;
	std::mutex _jobLock;
	bool _allWorkIsComplete = true;

	static void BounceToWorkerThread(void *vp, void *vpReserved);
	void RunWorkerThread();
};

#endif
