//---------------------------------------------------------------------------

#pragma hdrstop

#include <vector>
#include <exception>

#ifdef __BORLANDC__
// use bthreads
#include "SynchronousThreadRunner.h"
#include "SysInfo.h"
#define AvailableProcessors SysInfo::AvailableProcessorCount
#else
// use std threads
#include "StdSynchronousThreadRunner.h"
#define AvailableProcessors std::thread::hardware_concurrency
#endif

#pragma warning(suppress: 4068)
#pragma package(smart_init)

//---------------------------------------------------------------------------

////#define NO_MULTITHREADING

//---------------------------------------------------------------------------
SynchronousThreadRunner::SynchronousThreadRunner(int nJobs, void *param, ThreadWorkFunctionType workFunction)
	: SynchronousThreadRunner(nJobs, std::min<int>(nJobs, AvailableProcessors()), param, workFunction)
{
}

//---------------------------------------------------------------------------

SynchronousThreadRunner::SynchronousThreadRunner(int nJobs, int nThreads, void *param, ThreadWorkFunctionType workFunction)
	: _numberOfThreadsToUse(nThreads)
	, _totalNumberOfJobs(nJobs)
	, _workFunction(workFunction)
	, _workFunctionParam(param)
{
}

//---------------------------------------------------------------------------

SynchronousThreadRunner::SynchronousThreadRunner(void *param, ThreadWorkFunctionType workFunction)
	: SynchronousThreadRunner(AvailableProcessors(), param, workFunction)
{
}
//---------------------------------------------------------------------------

SynchronousThreadRunner::~SynchronousThreadRunner()
{
	while (!_allWorkIsComplete)
	{
		MTImillisleep(1);
	}
}
//---------------------------------------------------------------------------

int SynchronousThreadRunner::GetNumberOfThreads()
{
	return AvailableProcessors();
}
//---------------------------------------------------------------------------

int SynchronousThreadRunner::Run()
{
	_numberOfJobsRun = 0;
	_firstError = 0;
	_allWorkIsComplete = false;

#ifdef NO_MULTITHREADING

	_numberOfThreadsStillRunning = 1;
	RunWorkerThread();

#else

	int numberOfThreadsToUse = _numberOfThreadsToUse;
	_numberOfThreadsStillRunning = numberOfThreadsToUse;

	std::vector <std::thread *> threads;
	for (int threadNumber = 0; threadNumber < numberOfThreadsToUse; ++threadNumber)
	{
		threads.push_back(new std::thread(BounceToWorkerThread, this, nullptr));
	}

	for (auto p : threads)
	{
		p->join();
		delete p;
	}
#endif

	_allWorkIsComplete = true;
	return _firstError;
}

//---------------------------------------------------------------------------

/* static trampoline */
void SynchronousThreadRunner::BounceToWorkerThread(void *vp, void *vpReserved)
{
	SynchronousThreadRunner *runner = (SynchronousThreadRunner *)vp;
	if (runner == nullptr)
	{
		return;
	}

	runner->RunWorkerThread();

	// Do NOT try to access runner past this point -- it may have been deleted!
}
//---------------------------------------------------------------------------

void SynchronousThreadRunner::RunWorkerThread()
{
	_jobLock.lock();
	while (_numberOfJobsRun < _totalNumberOfJobs)
	{
		int myJobNumber = _numberOfJobsRun++;
		int retVal;

		try
		{
			_jobLock.unlock();
			retVal = _workFunction(_workFunctionParam, myJobNumber, _totalNumberOfJobs);
			_jobLock.lock();
		}
		catch (std::exception &ex)
		{
			MTIostringstream os;
			os << "***ERROR**** a SyncThread has caught std::exception: " << ex.what()
			TRACE_0(errout << os.str());
			retVal = -10010;
		}
		catch (...)
		{
			TRACE_0(errout << "***ERROR**** a SyncThread has thrown an unknown error");
			retVal = -10011;
		}

		if (retVal && !_firstError)
		{
			_firstError = retVal;
		}
	}

	_jobLock.unlock();
}

//---------------------------------------------------------------------------

MtiRect SynchronousThreadRunner::findSliceRoi(int jobNumber, int totalJobs, const IppiSize &size)
{
	MtiRect jobRoi;

	jobRoi.width = size.width;

	// Evenly distribute n extra rows over the first n jobs.
	auto minRowsPerJob = size.height / totalJobs;
	auto extraRows = size.height % totalJobs;
	jobRoi.y = (minRowsPerJob * jobNumber) + std::min<int>(extraRows, jobNumber);
	jobRoi.x = 0;
	jobRoi.height = minRowsPerJob + ((jobNumber < extraRows) ? 1 : 0);

	return jobRoi;
}


