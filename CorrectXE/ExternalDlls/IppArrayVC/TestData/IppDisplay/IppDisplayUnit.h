//---------------------------------------------------------------------------

#ifndef IppDisplayUnitH
#define IppDisplayUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include "Ippheaders.h"
#include <Vcl.Dialogs.hpp>
#include <Vcl.ExtDlgs.hpp>

//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TImage *displayImage;
	TButton *LoadFileButton;
	TImage *ProbeDisplay;
	TRadioGroup *SelectionModeRadioGroup;
	TLabel *ProbeStatusLabel;
	TTimer *UpdateTimer;
	TLabel *RoiStatusLabel;
	TCheckBox *UtilizeExtrinsicDataCheckBox;
	TButton *Load1Button;
	TButton *Load2Button;
	TButton *Load3Button;
	TButton *Load4Button;
	TOpenDialog *OpenDialog1;
	TButton *ClearRoiButton;
	void __fastcall LoadButtonClick(TObject *Sender);
	void __fastcall displayImageMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall displayImageMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall displayImageMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall UpdateTimerTimer(TObject *Sender);
   void __fastcall MyWndProc(TMessage &Message);
	void __fastcall LoadFileButtonClick(TObject *Sender);
	void __fastcall ClearRoiButtonClick(TObject *Sender);

private:	// User declarations

   Ipp8uArray _mainImage;
   bool _mouseDown = false;
   MtiRect _roi;
   MtiRect _probe;
   MtiRect _displayProbe;
   MtiPoint _mouseDownPoint;
   TMouseButton _button;
   TBitmap *_mainBitmap = nullptr;

   MtiSelectMode _selectionMode = MtiSelectMode::valid;
   bool  _utilizeExtrinsicData = false;
   string _probeStatusMessage;

public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
   void updateDisplay();
   void readGui();
   void updateGui();

   TWndMethod OldWndProc;

//    void __fastcall WMEraseBkgnd(TMessage &Message) {}
//    BEGIN_MESSAGE_MAP
//       VCL_MESSAGE_HANDLER(WM_ERASEBKGND , TMessage, WMEraseBkgnd)
//    END_MESSAGE_MAP(TImage)
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
