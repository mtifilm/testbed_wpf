// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "IppDisplayUnit.h"
#include "Ippheaders.h"

// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;

// ---------------------------------------------------------------------------
void __fastcall TForm1::MyWndProc(TMessage &Message) {
    if (Message.Msg == WM_ERASEBKGND)
	return;

    // provide default handling for everything else.
    OldWndProc(Message);
}

__fastcall TForm1::TForm1(TComponent* Owner) : TForm(Owner) {
    MTIostringstream os;
    os << "this";

    OldWndProc = ProbeDisplay->WindowProc;
    // ProbeDisplay->WindowProc = MyWndProc;
}

TBitmap *createBitmap(const Ipp8uArray &ippData) {
    auto bm = new TBitmap();
    bm->PixelFormat = pf24bit;
    bm->Width = ippData.getWidth();
    bm->Height = ippData.getHeight();

    for (auto h = 0; h < ippData.getHeight(); h++) {
	auto scanLine = reinterpret_cast<Ipp8u*>(bm->ScanLine[h]);
	auto rp = ippData.getRowPointer(h);
	for (auto w = 0; w < ippData.getWidth(); w++) {
	    *scanLine++ = rp[2];
	    *scanLine++ = rp[1];
	    *scanLine++ = rp[0];

	    rp += 3;
	}
    }

    return bm;
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::LoadButtonClick(TObject *Sender) {
    try {
	auto button = (TButton*)Sender;
	if (button == nullptr) {
	    Beep();
	    return;
	}

	string fileName;
	string path =
	    "..\\..\\..\\ExternalDlls\\IppArrayVC\\TestData\\IppDisplay\\";

	int n = static_cast<int>(button->Tag);
	switch (n) {
	case 1:
	    fileName = path + "RAR_REEL_03_04_001094.mat";
	    break;

	case 2:
	    fileName = path + "0003000.mat";
	    break;

	case 3:
	    fileName = path + "FourDaysWonder_0086400-2018-05-04_nc.091411.mat";
	    break;

	case 4:
	    fileName = path + "111289.mat";
	    break;
	}

	auto temp16u = MatIO::readIpp16uArray(fileName);
        temp16u /= 256;
        _mainImage.clear();
        _mainImage <<= temp16u;
	_mainBitmap = createBitmap(_mainImage);
	displayImage->Picture->Assign(_mainBitmap);
	_roi = _mainImage.getRoi();

	ProbeDisplay->Picture->Assign(_mainBitmap);
        _displayProbe = {-1, -1, 1, 1};
        updateDisplay();
    }
    catch (const exception &ex) {
	ShowMessage(ex.what());
    }

}

MtiRect makeValidRect(const MtiPoint &tl, const MtiPoint &br) {
    auto w = abs(tl.x - br.x);
    auto h = abs(tl.y - br.y);
    auto x = std::min<int>(tl.x, br.x);
    auto y = std::min<int>(tl.y, br.y);
    return {
	x, y, w, h
    };
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::displayImageMouseDown(TObject *Sender,
    TMouseButton Button, TShiftState Shift, int X, int Y) {
    _mouseDown = true;
    auto scaleX = double(_mainImage.getWidth()) / displayImage->Width;
    auto scaleY = double(_mainImage.getHeight()) / displayImage->Height;

    MtiPoint top = {int(scaleX * X), int(scaleY * Y)};

    if (Button == TMouseButton::mbMiddle) {
	_roi = {top, {0, 0}};
	_button = Button;
	_probe = {0, 0, 0, 0};
	return;
    }

    if (Button == TMouseButton::mbRight) {
	_probe = {top, {0, 0}};
	_button = Button;
    }

    if (Button == TMouseButton::mbLeft) {
	// Set the mark
	_mouseDownPoint = top;
	_button = Button;
    }
}

// ---------------------------------------------------------------------------

void __fastcall TForm1::displayImageMouseUp(TObject *Sender,
    TMouseButton Button, TShiftState Shift, int X, int Y) {

    _mouseDownPoint = {0, 0};
    _mouseDown = false;

}

// ---------------------------------------------------------------------------

void __fastcall TForm1::displayImageMouseMove(TObject *Sender,
    TShiftState Shift, int X, int Y) {
    auto scaleX = double(_mainImage.getWidth()) / displayImage->Width;
    auto scaleY = double(_mainImage.getHeight()) / displayImage->Height;
    if (_mouseDown == false) {
	return;
    }

    MtiPoint br = {int(scaleX * X), int(scaleY * Y)};
    if (_button == TMouseButton::mbMiddle) {
	auto tl = _roi.tl();
	_roi = makeValidRect(tl, br);
	updateDisplay();
	return;
    }

    if (_button == TMouseButton::mbRight) {
	auto tl = _probe.tl();
	_probe = makeValidRect(tl, br);
	updateDisplay();
	return;
    }

    if (_button == TMouseButton::mbLeft) {
	_probe += br - _mouseDownPoint;
	_mouseDownPoint = br;
	updateDisplay();
	return;
    }
}
// ---------------------------------------------------------------------------

void TForm1::updateGui() {
}

void TForm1::readGui() {
    switch (SelectionModeRadioGroup->ItemIndex) {
    case 0:
	_selectionMode = MtiSelectMode::valid;
	break;

    case 1:
	_selectionMode = MtiSelectMode::intersect;
	break;

    case 2:
	_selectionMode = MtiSelectMode::mirror;
	break;

    case 3:
	_selectionMode = MtiSelectMode::replicate;
	break;

    case 4:
	_selectionMode = MtiSelectMode::circular;
	break;
    }

    _utilizeExtrinsicData = UtilizeExtrinsicDataCheckBox->Checked;
}

void TForm1::updateDisplay()
{
    auto canvas = displayImage->Canvas;
    canvas->Pen->Color = clWhite;
    canvas->Pen->Width = 2;

    auto scaleX = double(_mainImage.getWidth()) / displayImage->Width;
    auto scaleY = double(_mainImage.getHeight()) / displayImage->Height;

    // Pictures autoscale
    scaleX = 1;
    scaleY = 1;

    auto x = int(_roi.x / scaleX);
    auto y = int(_roi.y / scaleY);
    auto w = int(_roi.width / scaleX);
    auto h = int(_roi.height / scaleY);
    MtiRect b = {x, y, w, h};

    TRect rect = {0, 0, _mainImage.getWidth(), _mainImage.getHeight()};
    canvas->StretchDraw(rect, _mainBitmap);
    canvas->MoveTo(x, y);
    canvas->LineTo(x + w, y);
    canvas->LineTo(x + w, y + h);
    canvas->LineTo(x, y + h);
    canvas->LineTo(x, y);

    auto xp = int(_probe.x / scaleX);
    auto yp = int(_probe.y / scaleY);
    auto wp = int(_probe.width / scaleX);
    auto hp = int(_probe.height / scaleY);

    canvas->Pen->Color = clYellow;
    canvas->Pen->Width = 2;

    canvas->MoveTo(xp, yp);
    canvas->LineTo(xp + wp, yp);
    canvas->LineTo(xp + wp, yp + hp);
    canvas->LineTo(xp, yp + hp);
    canvas->LineTo(xp, yp);

    readGui();

    try {
	auto relativeProbe = _probe - _roi.tl();

	auto roiImage = _mainImage(_roi);
	auto probeImage = roiImage(relativeProbe, _selectionMode,
	    _utilizeExtrinsicData);
	auto probeBitmap = createBitmap(probeImage);

	if (_displayProbe != _probe) {
	    // ProbeDisplay->Picture->Assign(probeBitmap);
	    ProbeDisplay->Canvas->FillRect(
	    {0, 0, ProbeDisplay->Width, ProbeDisplay->Height});
	    _displayProbe = _probe;
	    ProbeDisplay->Canvas->Draw(0, 0, probeBitmap);
	}
	else {
	    ProbeDisplay->Canvas->Draw(0, 0, probeBitmap);
	}

	MTIostringstream os;
	os << probeBitmap->Width << ", " << probeBitmap->Height;
	_probeStatusMessage = "No exception";
    }
    catch (exception &ex) {
	_displayProbe = {0, 0, 0, 0};
	ProbeDisplay->Canvas->FillRect(
	{0, 0, ProbeDisplay->Width, ProbeDisplay->Height});

	auto mtiRunTimeError = dynamic_cast<MtiRuntimeError*>(&ex);
	if (mtiRunTimeError != nullptr) {
	    _probeStatusMessage = mtiRunTimeError->getMessage();
	}
	else {
	    _probeStatusMessage = ex.what();
	}
    }
}

void __fastcall TForm1::UpdateTimerTimer(TObject *Sender)
{
    MTIostringstream os;
    os << "Probe: " << _probe << ", ROI: " << _roi << ", Image Size: " << _mainImage.getSize();
    RoiStatusLabel->Caption = os.str().c_str();
    ProbeStatusLabel->Caption = _probeStatusMessage.c_str();
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::LoadFileButtonClick(TObject *Sender)
{
    if (OpenDialog1->Execute()) {
	auto wic = new TWICImage();
	wic->LoadFromFile(OpenDialog1->FileName);
	auto bm = new TBitmap();
	bm->Assign(wic);
	bm->PixelFormat = pf24bit;
	auto w = bm->Width;
	auto h = bm->Height;
        delete wic;

	// We are always the same
	Ipp8uArray ippData({w, h, 3});

	for (auto h = 0; h < ippData.getHeight(); h++)
        {
	    auto scanLine = reinterpret_cast<Ipp8u*>(bm->ScanLine[h]);
	    auto rp = ippData.getRowPointer(h);
	    for (auto w = 0; w < ippData.getWidth(); w++) {
		rp[2] = *scanLine++;
		rp[1] = *scanLine++;
		rp[0] = *scanLine++;
		rp += 3;
	    }
	}

        delete bm;

        // Sanity check
        _mainImage = ippData;
       	_mainBitmap = createBitmap(_mainImage);
	displayImage->Picture->Assign(_mainBitmap);
	_roi = _mainImage.getRoi();
        _probe = {0,0,0,0};

        // Empty rectangle are the same
        _displayProbe = {-1, -1, 1, 1};
	ProbeDisplay->Picture->Assign(_mainBitmap);
        updateDisplay();
    }
}
// ---------------------------------------------------------------------------
void __fastcall TForm1::ClearRoiButtonClick(TObject *Sender)
{
  	_roi = _mainImage.getRoi();
        updateDisplay();
}
//---------------------------------------------------------------------------

