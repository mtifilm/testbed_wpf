object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 785
  ClientWidth = 1139
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    1139
    785)
  PixelsPerInch = 106
  TextHeight = 14
  object displayImage: TImage
    Left = 16
    Top = 24
    Width = 794
    Height = 665
    Anchors = [akLeft, akTop, akRight, akBottom]
    Stretch = True
    OnMouseDown = displayImageMouseDown
    OnMouseMove = displayImageMouseMove
    OnMouseUp = displayImageMouseUp
    ExplicitWidth = 785
  end
  object ProbeDisplay: TImage
    Left = 825
    Top = 288
    Width = 302
    Height = 265
    Anchors = [akTop, akRight]
  end
  object ProbeStatusLabel: TLabel
    Left = 40
    Top = 730
    Width = 521
    Height = 47
    Anchors = [akLeft, akBottom]
    AutoSize = False
    Caption = 'ProbeStatusLabel'
    WordWrap = True
  end
  object RoiStatusLabel: TLabel
    Left = 40
    Top = 710
    Width = 59
    Height = 14
    Anchors = [akLeft, akBottom]
    Caption = 'ROI Status'
  end
  object LoadFileButton: TButton
    Left = 1036
    Top = 216
    Width = 42
    Height = 25
    Anchors = [akTop, akRight]
    Caption = '...'
    TabOrder = 0
    OnClick = LoadFileButtonClick
    ExplicitLeft = 1027
  end
  object SelectionModeRadioGroup: TRadioGroup
    Left = 930
    Top = 24
    Width = 185
    Height = 130
    Anchors = [akTop, akRight]
    Caption = 'Selection Mode'
    ItemIndex = 0
    Items.Strings = (
      'Valid'
      'Intersection'
      'Mirror'
      'Replicate'
      'Circular')
    TabOrder = 1
    ExplicitLeft = 838
  end
  object UtilizeExtrinsicDataCheckBox: TCheckBox
    Left = 940
    Top = 160
    Width = 165
    Height = 17
    Anchors = [akTop, akRight]
    Caption = 'Utilize Extrinsic Data'
    TabOrder = 2
    ExplicitLeft = 848
  end
  object Load1Button: TButton
    Tag = 1
    Left = 896
    Top = 216
    Width = 29
    Height = 25
    Anchors = [akTop, akRight]
    Caption = '1'
    TabOrder = 3
    OnClick = LoadButtonClick
    ExplicitLeft = 804
  end
  object Load2Button: TButton
    Tag = 2
    Left = 931
    Top = 216
    Width = 29
    Height = 25
    Anchors = [akTop, akRight]
    Caption = '2'
    TabOrder = 4
    OnClick = LoadButtonClick
    ExplicitLeft = 839
  end
  object Load3Button: TButton
    Tag = 3
    Left = 966
    Top = 216
    Width = 29
    Height = 25
    Anchors = [akTop, akRight]
    Caption = '3'
    TabOrder = 5
    OnClick = LoadButtonClick
    ExplicitLeft = 874
  end
  object Load4Button: TButton
    Tag = 4
    Left = 1001
    Top = 216
    Width = 29
    Height = 25
    Anchors = [akTop, akRight]
    Caption = '4'
    TabOrder = 6
    OnClick = LoadButtonClick
    ExplicitLeft = 909
  end
  object ClearRoiButton: TButton
    Left = 1036
    Top = 745
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Clear Roi'
    TabOrder = 7
    OnClick = ClearRoiButtonClick
    ExplicitLeft = 1030
  end
  object UpdateTimer: TTimer
    Interval = 200
    OnTimer = UpdateTimerTimer
    Left = 720
    Top = 200
  end
  object OpenDialog1: TOpenDialog
    Filter = 'Graphic Files|*.jpg;*.tif;*.bmp|All File|*.*'
    Options = [ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 904
    Top = 648
  end
end
