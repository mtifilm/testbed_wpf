#include "stdafx.h"
#include "ipp8uTest.h"
#include "CppUnitTest.h"
#include "IppArray.h"
#include "Windows.h"
#include <iostream>
#include <sstream>
#include <string>
#include <numeric>
#include "CommonTestHeader.h"

#pragma warning( disable : 4244)
extern vector<vector<float>> cumsumSumTestData;
extern MtiSize cumArraySize;

namespace ZoneCoreTest
{
	TEST_CLASS(IppPlanarTest)
	{
		TEST_METHOD(IppPlanarCumSumTest)
		{
			auto nFrames = 10;
			auto rows = 6;
			auto cols = 7;

			IppPlaner<Ipp32fArray> sourceArray;
			for (auto n : range(nFrames))
			{
				Ipp32fArray a(rows, cols);
				a.iota(n * 100);
				sourceArray.push_back(a);
			}

			auto result = sourceArray.cumsum();

			// Check for truth using brute force
			for (auto sn : range(sourceArray.size()))
			{
				auto &cumSum = result[sn];
				for (auto r = 0; r < rows; r++)
				{
					for (auto c = 0; c < cols; c++)
					{
						auto sum = 0.0f;
						for (auto d : range(sn+1))
						{
							sum += *sourceArray[d].getPointerToElement(r, c);
						}
						
						Assert::AreEqual(sum, cumSum(r, c));
					}
				}
			}
		}

		TEST_METHOD(PlanerCumSumWriteDataTest)
		{
			auto nFrames = cumsumSumTestData.size();
			IppPlaner<Ipp32fArray> testArray;
			for (auto n : range(nFrames))
			{
				testArray.push_back(Ipp32fArray(cumArraySize.height, cumArraySize.width, 1, cumsumSumTestData[n].data()));
			}

			auto fileName = "c:\\temp\\PlanerCumSumTest.mat";

			SaveVariablesToMatFile1(fileName, testArray);

			IppPlaner<Ipp32fArray> cumSumArray;
			cumSumArray.emplace_back(testArray[0]);

			for (auto i = 1; i < testArray.size(); i++)
			{
				auto sumArray = cumSumArray[i - 1] + testArray[i];
				cumSumArray.push_back(sumArray);
			}

			AppendVariablesToMatFile1(fileName, cumSumArray);
		}
	};
}
