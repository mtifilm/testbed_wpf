#include "stdafx.h"
#include "CppUnitTest.h"
#include "IppHeaders.h"
#include "Windows.h"
#include <iostream>
#include <sstream>
#include <string>
#include <numeric>
#include "CommonTestHeader.h"
#include <random>

extern IppiSize boxSize;
extern float BoxArray[];
extern vector<vector<float>> cumsumSumTestData;
extern MtiSize cumArraySize;

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ZoneCoreTest
{
	TEST_CLASS(MtiIOTest)
	{
		TEST_METHOD(Ipp8uWriteMatThreeChannel)
		{
			Ipp8uArray baseImage(13, 7);
			baseImage.iota();

			vector<Ipp8uArray> planes = { baseImage, baseImage + 100, baseImage - 40 };
			Ipp8uArray image3Channel;
			image3Channel.copyFromPlanar(planes);

			auto image3ChannelRoi = image3Channel({ 1,1, 5, 11 });
			SaveVariablesToMatFile2("Ipp8uWriteMatThreeChannel.mat", image3Channel, image3ChannelRoi);
		}

		TEST_METHOD(Ipp8uWriteMatMultiple)
		{
			Ipp8uArray baseImage(10, 9);
			baseImage.iota();

			vector<Ipp8uArray> planes = { baseImage, baseImage + 100, baseImage - 40 };
			Ipp8uArray image1;
			image1.copyFromPlanar(planes);

			auto imageRoi = image1({ 1, 1, 7, 8 });

			MatIO::saveListToMatFile("c:\\temp\\Ipp8uWriteMatMultiple.mat", image1, "m_3", imageRoi, "mRoi_3");
		}

		TEST_METHOD(IppMatReadWrite8uTest)
		{
			Ipp8uArray baseImage(10, 9);
			baseImage.iota();

			vector<Ipp8uArray> planes = { baseImage, baseImage + 100, baseImage - 40 };
			Ipp8uArray image1;
			image1.copyFromPlanar(planes);
			auto imageRoi = image1({ 1, 1, 7, 8 });

			auto fileName = "c:\\temp\\IppMatReadWrite8uTest.mat";
			MatIO::saveListToMatFile(fileName, image1, "m_3", baseImage, "baseImage");

			// read it back in
			auto image1Test = MatIO::readIpp8uArray(fileName);
			Assert::IsTrue(image1 == image1Test);

			AppendVariablesToMatFile1(fileName, imageRoi);
			auto imageRoiTest = MatIO::readIpp8uArray(fileName, "imageRoi");
			Assert::IsTrue(imageRoi == imageRoiTest);

			auto baseImageTest = MatIO::readIpp8uArray(fileName, "baseImage");
			Assert::IsTrue(baseImage == baseImageTest);
		}

		TEST_METHOD(IppMatIoScalarWriteTest)
		{
			int i_int = 37;
			double x_double = 3.14159;
			float f_float = -1.6666f;

			std::string infoLabel = "This is an string of chars to test";
			std::string fileName = "c:\\temp\\IppMatIoScalarWriteTest.mat";

			// This shows how to create and append
			MatIO::saveListToMatFile(fileName);
			AppendVariablesToMatFile1(fileName, i_int);
			AppendVariablesToMatFile2(fileName, x_double, f_float);
			AppendVariablesToMatFile2(fileName, infoLabel, fileName);

			auto names = MatIO::getVariableNames(fileName);
		}

		TEST_METHOD(IppMatIoMtiRectWriteTest)
		{
			MtiRect boxTest = { 1, 2, 10, 20 };
			std::string fileName = "c:\\temp\\IppMatIoMtiRectWriteTest.mat";
			MatIO::saveListToMatFile(fileName, boxTest, "boxTest");
		}

		TEST_METHOD(IppPlanerWriteReadTest)
		{
			auto nFrames = 10;
			auto rows = 6;
			auto cols = 7;

			IppPlaner<Ipp32fArray> planerTest;
			for (auto n : range(nFrames))
			{
				Ipp32fArray a(rows, cols);
				a.iota(n * 100);
				planerTest.push_back(a);
			}

			auto fileName = "c:\\temp\\IppPlanerWriteTest.mat";

			SaveVariablesToMatFile1(fileName, planerTest);

			auto result = MatIO::readIpp32fPlaner(fileName, "planerTest");

			auto rit = result.begin();
			for (auto &t : planerTest)
			{
				auto &r = *rit++;
				Assert::AreEqual(t, r);
			}
		}

		TEST_METHOD(IppMatIoVectorIppArrayWriteTest)
		{
			const int N = 10;
			const int R = 6;
			const int C = 9;

			vector<Ipp32fArray> planerTest(N);

			auto c = 100.0f;
			for (auto &a : planerTest)
			{
				a = Ipp32fArray(R, C);
				a.iota();
				a += c;
				c += c;
			}

			std::string fileName = "c:\\temp\\IppMatIoVectorIppArrayWriteTest.mat";
			SaveVariablesToMatFile1(fileName, planerTest);
		}
	};
}