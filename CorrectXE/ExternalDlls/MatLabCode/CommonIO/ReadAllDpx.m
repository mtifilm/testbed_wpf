function M = ReadAllDpx(path)
  x = dir(strcat(path,'\', '*.dpx'));
  fileName = strcat(path,'\', x(1).name);
  header = dpxinfo(fileName);
  s = size(x);
  n = s(1);
  M = zeros(header.Height, header.Width, 3, n,'uint16');
  for i = 1:n
      fileName = strcat(path, '\', x(i).name);
      I = ReadNormalizedDpx(fileName);
      M(:, :, :, i) = I;
  end
end
  