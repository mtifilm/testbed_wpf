function images = ReadRoiRgbFiles(parameters)
    imageStore = GetImageDataStore(parameters);
    totalFiles = numel(imageStore.Files);
    height = parameters.ROI.height;
    width = parameters.ROI.width;
    roiX = parameters.ROI.x + 1;
    roiY = parameters.ROI.y + 1; 

    images=zeros(height,width, 3, totalFiles, 'uint16');
    
    for i = 1:totalFiles
        imageFull = readimage(imageStore, i);
        images(:,:,:,i) = imageFull(roiY:height + roiY - 1, roiX:width + roiX - 1, :);
    end
end