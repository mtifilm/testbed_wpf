function showBoxes(boxes, color)
hold on
k = 0;
if ~exist('color','var')
    color = 'b';
end
w = 1;
for i = 1:numel(boxes)
    b = boxes(i);
    line([b.x, b.x + b.width],[b.y, b.y], 'Color',color, 'LineWidth', w);
    line([b.x, b.x],[b.y, b.y+b.height], 'Color',color, 'LineWidth', w );
    line([b.x + b.width, b.x + b.width],[b.y, b.y+b.height], 'Color',color, 'LineWidth', w );
    line([b.x, b.x + b.width],[b.y+b.height, b.y+b.height], 'Color',color, 'LineWidth', w );
    k = k + 1;
end
hold off
end

