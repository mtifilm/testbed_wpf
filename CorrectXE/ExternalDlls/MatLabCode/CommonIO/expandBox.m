function largeBox = expandBox(box, sz)
% This expands a box around the center with size sz

rx = sz(2);
ry = sz(1);

rw = round(rx * box.width);
if (mod(rw, 2) == 0) 
    rw = rw+1;
end

rh = round(ry * box.height);
if (mod(rh, 2) == 0) 
    rh = rh+1;
end

% This is only for Matlab
largeBox = box;

largeBox.xc = round(rx * (double(box.xc) - 0.5)) + 1;
largeBox.yc = round(ry * (double(box.yc) - 0.5)) + 1;
largeBox.width = rw;
largeBox.height = rh;

% the width, heigth are always odd
% For matlab, point = x + width - 1
% thus we want floor here.
largeBox.x = largeBox.xc - floor(rw/2);
largeBox.y = largeBox.yc - floor(rh/2);
end
