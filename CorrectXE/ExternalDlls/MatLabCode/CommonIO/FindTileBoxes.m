% this creates an cell of MtiRects
% NOTE: MtiRects is width, height not row col
function boxes = FindTileBoxes(rect, radius)
ulx = rect.x;
uly = rect.y;
diameter = 2*radius + 1;
cols = round(rect.width / diameter);
rows = round(rect.height/ diameter);
swidth = rect.width / cols;
sheight = rect.height / rows;
boxes = cell(rows, cols);

% the box structure 
for r= 1:rows
    for c = 1:cols
        x = ulx + round(swidth * (c-1));
        y = uly + round(sheight * (r-1));
        boxes{r, c} = struct('x', x, 'y', y, 'width', round(swidth), 'height', round(sheight));
    end
end

boxes = cell2mat(boxes);
end