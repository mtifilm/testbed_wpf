function [Y, frames] = ReadYFromDpxList(parameters)
  listOfNames = parameters.Images;
  roi = parameters.ROI;
  fid = fopen(listOfNames);
  line = fgetl(fid);
  list = line;
  while line ~= -1 
     line = fgetl(fid);
     if (line ~= -1)
       list = [list; string(line)];
     end
  end
  fclose(fid);

  s = size(list);
  n = s(1);
  header = dpxinfo(char(list(1)));
  A = zeros(header.Height, header.Width,'uint16');
  
  width = roi.width;
  height = roi.height;
  x = roi.x + 1;
  y = roi.y + 1;
  Y = zeros(height, width, n,'uint16');
  
  frames = parameters.SequenceNumber; 
  for i = 1:n
      fileName = char(list(i));
      I = ReadNormalizedDpx(fileName);
      A(:, :) = 0.299 * I(:,:,1) + 0.587 *  I(:,:,2) + 0.114 *  I(:,:,3);
      Y(:, :, i) = A(y:height + y - 1, x:width + x - 1);
      disp(['Pass 1, Collecting Data ' ': ' int2str(frames)])
      frames = frames + 1;
  end
end