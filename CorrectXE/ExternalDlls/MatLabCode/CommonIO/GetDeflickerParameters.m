function controlSegments = GetDeflickerParameters(parameterFileName)
     controlSegments = [];
     fid = fopen(parameterFileName);
     line = fgetl(fid);
     while (line ~= -1)
         eline = [line ';'];
         eval(eline);
         line = fgetl(fid);
     end
     fclose(fid);
end