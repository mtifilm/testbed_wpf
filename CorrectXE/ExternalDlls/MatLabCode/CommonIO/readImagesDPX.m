function [images, bits] = readImagesDPX( filePath, startFrame, maxFrames )
    %   Read the mat files from the path
    %  images are cells containing a structure frameInfo
    %  this is added to at later dates, works for Matlab not C++
    filePath = [filePath, '\'];
    fileList = dir(strcat(filePath, '\*.DPX'));
    
    if (numel(fileList) == 0)
        error('Error:  No files found');
    end
    
    % for debugging
    maxFrames = startFrame + maxFrames - 1;
    totalFrames = min(numel(fileList), maxFrames);

 %   images = cell(totalFrames - startFrame + 1,1);
    disp('starting');
    for i = startFrame:totalFrames
        
        fprintf_r('Frame: %d', i);
      
        f = fileList(i);
        name = f.name;
        fileName = strcat(filePath, name);
        dpxData = dpxRead(fileName);
        bits = dpxData{3};
        dpxImage = dpxData{2}; % / bitshift(1, bits);
        dpxImage = permute(dpxImage, [2, 1, 3]);
        tmp = dpxImage(:,:,1);
        dpxImage(:,:,1) = dpxImage(:,:,3);
        dpxImage(:,:,3) = tmp;
        if (size(dpxImage, 3) == 4)     
            dpxImage = dpxImage(:,:,[1,2,3]);
        end
        
% The first time, the images are created       
       if ~exist('images','var')
           sz = size(dpxImage);
           images = zeros(sz(1), sz(2), sz(3), totalFrames - startFrame + 1);
       end;
%        frameInfo.fullRgbImage = single(dpxImage) / 65535.0;
                
        %% This code only works with RGB images, a grayscale is saved
        if (size(dpxImage, 3) == 1)
            grayImage = rgb2gray(dpxImage);
            dpxImage = cat(3, grayImage, grayImage, grayImage);   
        end
        
        images(:,:, :, i-startFrame+1) = dpxImage;
    end

    disp(' ');
end