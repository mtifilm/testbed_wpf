function sf = scaleToInt(f)
  sf = int32(f * 65535);
end