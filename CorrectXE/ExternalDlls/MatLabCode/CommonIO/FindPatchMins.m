function patchMins = FindPatchMins(proxyImage, params)
    radius = params.boxRadius;
    roi = MtiRect(params.boxRadius + 1, params.boxRadius + 1, size(proxyImage,2) - 2*params.boxRadius, size(proxyImage, 1) - 2*params.boxRadius);
    interiorImage = extractImage(proxyImage, roi);
    
    patchMins = zeros(size(interiorImage, 1)-2*radius, size(interiorImage,2) - 2*radius);
    patchMins(1:size(patchMins,1), 1:size(patchMins,2)) = 10000;
    
    % since we dont' have a box filter
    d = 2*radius + 1;
    kernel = ones(d,d)/d/d;
    
    for x = [-radius:radius]
        for y = [-radius:radius]
            if (x == 0) && (y == 0)
                continue;
            end
            offsetImage = extractImage(proxyImage, AddPointToMtiRect(roi, x, y));
            absDiffImage = abs(offsetImage - interiorImage);
            %      L1Image = imboxfilt(absDiffImage, 2*radius + 1);
            
            normArray = conv2(absDiffImage, kernel, 'valid');
            patchMins = min(normArray, patchMins);
        end
    end
    
end