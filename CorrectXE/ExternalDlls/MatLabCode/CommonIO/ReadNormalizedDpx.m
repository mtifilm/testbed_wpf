function y = ReadNormalizedDpx(fileName)
  header = dpxinfo(fileName);
  maxOfDataRange = 2^header.ChannelBitDepths - 1;
  %maxOfDataRange = 1023;
  scaleFactor = intmax('uint16') / maxOfDataRange;
  y = dpxread(fileName )* scaleFactor;
end
