% Return the valid part of the normed cross correlation
% nxc returns with valid crosscor
% [rc] is the pixels movement relative to the center of the source
% Thus if the is a taken symmetrically around the template, rc = [0,0]
% mx is the max
function [nxc, delta, mx] = validNormXCorr(template, source)
  % normxcorr2 returns size  [(Ws + Wt - 1),  (Hs + Ht - 1)]
  nxc = normxcorr2(template, source);
  %  valid size is [Ws -Wt + 1), (Hs -Ht +1)]
  
  vs = size(source) - size(template) + [1,1];
  ts = size(template);
  nxc = nxc(ts(1) : (ts(1) + vs(1) - 1), ts(2) : (ts(2) + vs(2) - 1));
  
  [mx, idx] = max(nxc(:));
  [r, c] = ind2sub(size(nxc), idx);
  %offset = floor(vs/2) + 1;
  offset = ceil(vs/2);  
  delta = [r, c] - offset;
end