function imageStore = GetImageDataStore(parameters)
    % Read in the list of files
    l = fileread(parameters.Images);
    fileNames = strsplit(l, '\r\n');
    
    % The last is blank so get rid of it
    fileNames = fileNames(1:end-1);
    imageStore = imageDatastore(fileNames,'FileExtensions','.dpx', 'ReadFcn', @ReadNormalizedDpx);    
end

