function BOX4 = BlobCode2 (BOX5, MaskSpatial)
% 
% The input to this function is:
%    BOX5 - the five-frame motion box assigned at each pixel
%    MaskSpatial - the mask of spatial contrast
%
% Connected components of candidate pixels are analyzed and the nearest
% non-candidate pixel is found.  The nearest non-candidate pixel is 
% used for this purpose:   
%    (a) update the motion vectors
%
% This function returns:
%     BOX4 - the four-frame motion box assigned to each pixel

BOX4 = BOX5;

% calculate the distance to candidtate pixels
DistToBlob = bwdist (MaskSpatial);

% connected components
stats = regionprops(DistToBlob<=3, 'PixelIdxList');

% process each connected component independently
for i = 1:numel(stats)
    Idx = stats(i).PixelIdxList;
    PixInt = Idx(DistToBlob(Idx)<=2);    % interior pixels
    PixExt = Idx(DistToBlob(Idx)>2);     % exterior pixels

    % for each interior pixel, find the nearest exterior
    [rowInt, colInt] = ind2sub(size(DistToBlob), PixInt);
    [rowExt, colExt] = ind2sub(size(DistToBlob), PixExt);
    for j = 1:numel(PixInt)
        rInt = rowInt(j);
        cInt = colInt(j);
        delta = [rowExt, colExt] - [rInt, cInt];
        dist = sqrt(delta(:,1).^2 + delta(:,2).^2);
        [MinDist, k] = min(dist);
        
        % (rowExt(k), colExt(k)) is the closest exterior point.  Assign
        % the nearest neighbor motion
        rExt = rowExt(k);
        cExt = colExt(k);
        BOX4(rInt, cInt) = BOX5(rExt, cExt);
    end

end

end
        