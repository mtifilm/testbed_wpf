function ShotInfo = getShotInfo (shot_number)

if shot_number == 1
    ShotInfo.proto = 'D:/Temp/ShineTests/bad_fix/%.6d.dpx';
    ShotInfo.TrackFile = 'D:/Temp/ShineTests/bad_fix/bad_fix_tracks.txt';
    ShotInfo.FRAMES = 86603:86613;
    ShotInfo.ProcessFrameIdx = 7;
elseif shot_number == 2
    ShotInfo.proto = 'f:/MovieData/shine/AlphaFilterStressTest/C3P_R02_%.7d.dpx';
    ShotInfo.TrackFile = 'f:/MovieData/shine/AlphaFilterStressTest/wheatField.txt';
    ShotInfo.FRAMES = 172800:172819;
    ShotInfo.ProcessFrameIdx = 20;   % out of 20
end

% load in the images
[ShotInfo.RGB, ShotInfo.Y, ShotInfo.y] = LoadImg (ShotInfo);
ShotInfo.FullSize = size(ShotInfo.Y{ShotInfo.ProcessFrameIdx}); % row, col
ShotInfo.PxySize = size(ShotInfo.y{ShotInfo.ProcessFrameIdx}); % row, col
ShotInfo.FACTOR_SCALE = ShotInfo.FullSize(2) / ShotInfo.PxySize(2);


% load the tracks
tracks = loadTracksFromText (ShotInfo.TrackFile);
ShotInfo.ProxyBoxWidth = tracks(1).boxes(1).width/2;

% find the tracks passing through this frame
frameIndex = ShotInfo.ProcessFrameIdx;
endTracks = arrayfun(@(x) numel(x.boxes)+x.startFrameIndex - 1, tracks);
tracksAtFrameIndex = tracks(([tracks.startFrameIndex] <= frameIndex) & (frameIndex <= endTracks ));
      
% the tracks passing through this frame represet the valid optical flow
% through this point in time.  We add a BOX structure to ShotInfo which is
% a collection of unique five frame offsets through space and time.
%

%  The first BOX is the zeros motion offset.  This is the artifical 
%  offset that is applied throughout the frame

% the five nearest frames
iIdx0 = frameIndex-2;
iIdx1 = frameIndex+2;

% legalize the frames
while iIdx0 < 1
    iIdx0 = iIdx0 + 1;
    iIdx1 = iIdx1 + 1;
end
while iIdx1 > numel(ShotInfo.FRAMES)
    iIdx1 = iIdx1-1;
    iIdx0 = iIdx0-1;
end

ShotInfo.BOX{1}.Frame = iIdx0:iIdx1;
ShotInfo.BOX{1}.Offset = zeros(5,2);  % (x,y) offset
ShotInfo.BOX{1}.Center = [-1, -1];  % artificial center


% extract the legitimate box centers in this frame
%   BoxCen:  high resolution boxes
%   boxCen:  low resolution boxes
%   flowIndex:  frames that make up the local flow
%   flowFlag:  flag to indicate valid or artificial flow

for iTrk = 1:numel(tracksAtFrameIndex)
    % find the five nearest frames
    iIdx = frameIndex - tracksAtFrameIndex(iTrk).startFrameIndex + 1;
    iIdx0 = iIdx-2;
    iIdx1 = iIdx+2;
   
    % legalize the frames
    while iIdx0 < 1
        iIdx0 = iIdx0 + 1;
        iIdx1 = iIdx1 + 1;
    end
    while iIdx1 > numel(tracksAtFrameIndex(iTrk).boxes)
        iIdx1 = iIdx1-1;
        iIdx0 = iIdx0-1;
    end
    
    BoxCen = zeros(5,2);
    for i = iIdx0:iIdx1
        BoxCen(i-iIdx0+1,:) = [tracksAtFrameIndex(iTrk).boxes(i).xSubpixelCenter, ...
        tracksAtFrameIndex(iTrk).boxes(i).ySubpixelCenter];
    end
    
%     Offset = round(BoxCen - BoxCen(iIdx-iIdx0+1,:));
    Offset = zeros(5,2);
    for i = 1:size(BoxCen,1)
         Offset(i,:) = round(BoxCen(i,:) - BoxCen(iIdx-iIdx0+1,:));
    end
%  replaced because old matlab
    Center = round(BoxCen(iIdx-iIdx0+1,:));
    IdxList = (iIdx0:iIdx1) + tracksAtFrameIndex(iTrk).startFrameIndex-1;
    Frame = IdxList;
       
    % does this BOX already exist?
    ExistFlag = false;
    for iBox = 1:numel(ShotInfo.BOX)
        OffsetDiff = sum(sum(abs(ShotInfo.BOX{iBox}.Offset - Offset)));
        FrameDiff = sum(abs(ShotInfo.BOX{iBox}.Frame - Frame));
        if (OffsetDiff + FrameDiff) == 0
            % this motion already exist in the list
            ExistFlag = true;
            CenterDiff = sum(abs(ShotInfo.BOX{iBox}.Center(1,:) - [-1,-1]));
            if CenterDiff == 0
                % this motion is already addressed by the 
                % artifical zero motion
            else
                % this offset already exists.  Add to the list of centers
                ShotInfo.BOX{iBox}.Center(end+1,:) = Center;
            end
        end
    end

    if ExistFlag == false
        % we have not seen this offset yet, so add it
        iBox = numel(ShotInfo.BOX) + 1;
        ShotInfo.BOX{iBox}.Frame = Frame;
        ShotInfo.BOX{iBox}.Offset = Offset;
        ShotInfo.BOX{iBox}.Center = Center;
    end
    
end

end
