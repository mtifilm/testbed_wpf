function mtiRect = MtiRect(x, y, w, h)
    mtiRect.x = x;
    mtiRect.y = y;
    mtiRect.width = w;
    mtiRect.height = h;
    mtiRect.xc = round(x + floor(w/2));
    mtiRect.yc = round(y + floor(h/2));  
    
    mtiRect.xSubpixelCenter = -1;    % Invalid Value
    mtiRect.ySubpixelCenter = -1;
end