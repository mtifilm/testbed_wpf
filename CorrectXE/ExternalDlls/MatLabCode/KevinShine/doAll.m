
SHOT = 1;   % train station
%SHOT = 2;   % wheat field

SMOOTH_RAD = 4;
CONTR_THRESH_POS = 1000000000;
CONTR_THRESH_NEG = 25;
MOTION_TOLERANCE = 100;
MIN_SIZE = 0;
MAX_SIZE = 10;

ShotInfo = getShotInfo (SHOT);
[Z5, BOX5] = PixelMotion (ShotInfo, SMOOTH_RAD);

MaskContrast = BlobCode1 (ShotInfo, Z5, CONTR_THRESH_POS, CONTR_THRESH_NEG);

[BOX4, MaskDetect] = BlobCode2 (ShotInfo, BOX5, MaskContrast, MIN_SIZE, MAX_SIZE);

MaskMotion = BlobCode3 (ShotInfo, BOX4, MOTION_TOLERANCE);

MaskProcess = BlobCode4 (MaskDetect, MaskMotion);

RGB_new = BlobCode5 (ShotInfo, BOX4, MaskProcess);
RGB_old = ShotInfo.RGB{ShotInfo.ProcessFrameIdx};