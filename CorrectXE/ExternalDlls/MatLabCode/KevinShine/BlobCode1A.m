function [MaskTemporal, ContrTemporal] = BlobCode1A (ShotInfo, Z5, ...
    TemporalContrastPos, TemporalContrastNeg)
% 
% The input to this function is:
%    ShotInfo - information about the shot
%    Z5 - the 5 motion compensated frames
%    TemporalContrastPos - the value between 0 and 100 for the positive 
%    temporal contrast
%    TemporalContrastNeg - the value between 0 and 100 for the negative 
%    temporal contrast
%
% BlobCode1A applies a threshold to the contrast image.  Pixels that satisfy 
% the threshold are candidates for repair.
%
% This function returns the MaskTemporal, the Mask of candidate pixels
% based on contrast.   MaskTemporal=1 means the pixel exhibits positive 
% contrast.  MaskTemporal=-1 means the pixels exhibits negative contrast.
%
% This function also returns ContrTemporal, which can be used for
% debugging purposes.

Y = ShotInfo.Y{ShotInfo.ProcessFrameIdx};
Median = median(Z5, 3);
DiffRaw = Y - Median;
MedianSort = sort(Median(:));
DiffScale = zeros(size(DiffRaw));


% The range of difference values depends on the median intensity.  e.g. 
% brighter areas typically have a larger range than darker areas.  
% Normalize the difference to create a contrast image.

NBIN = 100;
diff20 = zeros(NBIN,1);
diff80 = zeros(NBIN,1);
range = zeros(NBIN,1);
for i = 1:NBIN
    i0 = round(((i-1)/NBIN) * numel(MedianSort)) + 1;
    i1 = round((i/NBIN)*numel(MedianSort));

    med0 = MedianSort(i0);
    med1 = MedianSort(i1);

    if i == NBIN
        med1 = med1 + .01;
    end
    
    if med1 == med0
        idx = find (Median(:) == med0);
    else
        idx = find((Median(:) >= med0) & (Median(:) < med1));
    end
    diff = DiffRaw(idx);

    diff_sort = sort(diff);

    diff20(i) = diff_sort (round(.2*numel(diff_sort)));
    diff80(i) = diff_sort (round(.8*numel(diff_sort)));

    range(i) = diff80(i) - diff20(i);

    DiffScale(idx) = DiffRaw(idx) / range(i);
end

% We use the ContrastPos and ContrastNeg to make sure the scaled diff 
% value is sufficiently large

MIN_SCALED_CONTRAST = 2;
MAX_SCALED_CONTRAST = 10;

% when ContrastVal = 0, ScaledThresh = MIN_SCALED_CONTRAST
% when ContrastVal = 100, ScaledThresh = MAX_SCALED_CONTRAST
SlopeScale = (MAX_SCALED_CONTRAST-MIN_SCALED_CONTRAST) / (100-0);
InterScale = MIN_SCALED_CONTRAST - SlopeScale*0;

% Calculate the thresholds for the scaled diff
ScaleThreshPos = SlopeScale*TemporalContrastPos + InterScale;
ScaleThreshNeg = SlopeScale*TemporalContrastNeg + InterScale;

% create the mask
MaskTemporal = double(DiffScale > ScaleThreshPos) - double(DiffScale < -ScaleThreshNeg);
ContrTemporal = DiffScale;

end
