function [MaskSpatial, ContrSpatial, ValSpatial] = BlobCode1B (...
    ShotInfo, MaskTemporal, MinSize, MaxSize, ...
    SpatialContrastPos, SpatialContrastNeg)
% 
% The input to this function is:
%    ShotInfo - information about the shot
%    MaskTemporal - the mask of temporal contrast (1 = positive, -1 =
%    negative)
%    MinSize - the value of the minimum debris size
%    MaxSize - the value of the maximum debris size
%    SpatialContrastPos - the value between 0 and 100 for the positive 
%    spatial contrast
%    SpatialContrastNeg - the value between 0 and 100 for the negative 
%    spatial contrast
%
% Connected components of candidate pixels are analyzed and the nearest
% non-candidate pixel is found.  The nearest non-candidate pixel is 
% used for three purposes:   
%    (a) compute debris size
%    (b) compute spatial contrast
%
% This function returns:
%     MaskSpatial - the mask of defective pixels
%
% This function also returns ContrSpatial, which can be used for debugging
% purposes.
% This function also returns AveSpatial, which can be used for debugging
% purposes.

Y = ShotInfo.Y{ShotInfo.ProcessFrameIdx};
MaskSpatial = false(size(Y));
ContrSpatial = zeros(size(Y));
ValSpatial = zeros(size(Y));

% calculate the distance to candidtate pixels
DistToBlob = bwdist (MaskTemporal);

% connected components
stats = regionprops(DistToBlob<=1, 'PixelIdxList');

% We use the ContrastPos and ContrastNeg to make sure the scaled diff 
% value is sufficiently large

MIN_SPATIAL_CONTRAST = 0;
MAX_SPATIAL_CONTRAST = .5;

% when ContrastVal = 0, SpatialThresh = MIN_SCALED_CONTRAST
% when ContrastVal = 100, SpatialThresh = MAX_SCALED_CONTRAST
SlopeContrast = (MAX_SPATIAL_CONTRAST-MIN_SPATIAL_CONTRAST) / (100-0);
InterContrast = MIN_SPATIAL_CONTRAST - SlopeContrast*0;

% Calculate the thresholds for the spatial diff
SpatialThreshPos = SlopeContrast*SpatialContrastPos + InterContrast;
SpatialThreshNeg = SlopeContrast*SpatialContrastNeg + InterContrast;


% Use the size of the defect to promote the average spatial contrast.
% Small defects are evaluated on their observed contrast.  
% Large defects are evaluated on a promoted value of the average contrast.
MIN_PROMOTE_SIZE = 1;
MAX_PROMOTE_SIZE = 100;
MIN_PROMOTE_VAL = 1;
MAX_PROMOTE_VAL = 10;
SlopePromote = (MAX_PROMOTE_VAL-MIN_PROMOTE_VAL) / ...
    (MAX_PROMOTE_SIZE-MIN_PROMOTE_SIZE);
InterPromote = MIN_PROMOTE_VAL - SlopePromote*MIN_PROMOTE_SIZE;

% process each connected component independently
for i = 1:numel(stats)
    Idx = stats(i).PixelIdxList;
    PixInt = Idx(DistToBlob(Idx)==0);    % detected (interior) pixels
    PixExt = Idx(DistToBlob(Idx)>0);     % exterior pixels
    
    % keep track of the size of the debris
    DebrisSize = 0;
    
    % for each interior pixel, find the nearest exterior
    [rowInt, colInt] = ind2sub(size(DistToBlob), PixInt);
    [rowExt, colExt] = ind2sub(size(DistToBlob), PixExt);
    nPos = 0;
    nNeg = 0;
    SpatialCuml = 0;
    for j = 1:numel(PixInt)
        rInt = rowInt(j);
        cInt = colInt(j);
        delta = [rowExt, colExt] - [rInt, cInt];
        dist = sqrt(delta(:,1).^2 + delta(:,2).^2);
        [MinDist, k] = min(dist);
        
        % (rowExt(k), colExt(k)) is the closest exterior point.  
        rExt = rowExt(k);
        cExt = colExt(k);
        
        % the size of the debris is the largest distance to the exterior
        % this is useful for long pieces of hair since many pixels may 
        % be involved, but locally the hair always appears small.
        DebrisSize = max(DebrisSize, MinDist);

        % the spatial contast
        if MaskTemporal(rInt,cInt) > 0
            nPos = nPos + 1;
            SpatialDiff = Y(rInt,cInt) - Y(rExt,cExt);
        elseif MaskTemporal(rInt,cInt) < 0
            nNeg = nNeg + 1;
            SpatialDiff = Y(rExt,cExt) - Y(rInt,cInt);
        else
            SpatialDiff = 0;
        end
        
        SpatialCuml = SpatialCuml + SpatialDiff;
        ContrSpatial(rInt,cInt) = SpatialDiff;
    end

    % decide if this candidate should be processed
    UseIt = true;
    if (DebrisSize < MinSize) || (DebrisSize > MaxSize)
        UseIt = false;
    end
    
    SpatialAve = SpatialCuml / numel(PixInt);
    SpatialPromote = min(MAX_PROMOTE_VAL, ...
        SlopePromote * numel(PixInt) + InterPromote);
    SpatialVal = SpatialPromote * SpatialAve;
    
    
    % since a blob can contain both positive and negative contrast pixels,
    % use majority rules to decide which threshold to use
    if nPos > nNeg
        % mostly a positive contrast defect
        if SpatialVal < SpatialThreshPos
            UseIt = false;
        end
    else
        % mostly a negative contrast defect
        if SpatialVal < SpatialThreshNeg
            UseIt = false;
        end
    end

    if UseIt == true
        MaskSpatial(PixInt) = true;
    end
    
    ValSpatial(PixInt) = SpatialVal;
end

end
        