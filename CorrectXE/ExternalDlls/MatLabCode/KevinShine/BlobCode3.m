function MaskMotion = BlobCode3 (ShotInfo, BOX4, MotionTolerance)
    %
    % The input to this function is:
    %    ShotInfo - information about the shot
    %    BOX4 - the four-frame motion box assigned at each pixel
    %    MaskSize - the mask of candidate defects
    %    MotionTolerance - the value between 0 and 100 for the motion tolerance
    %
    % This function computes Z4, four motion compensated frames where the
    % current frame is not included.  Z4 is used to generate the motion
    % difference frame.
    %
    % The MaskMotion image is returned.  It indicates areas with unreliable
    % motion.
    
    
    Y = ShotInfo.Y{ShotInfo.ProcessFrameIdx};
    [nRow, nCol] = size(Y);
    Z4 = zeros(nRow,nCol,4);
    
    % create the Z4 from the boxes assigned to the four frame offsets.
    for box = 1:max(BOX4(:))
        idx = find(BOX4(:) == box);
        if numel(idx) > 0
            Frame = ShotInfo.BOX{box}.Frame;
            xoff = ShotInfo.BOX{box}.Offset(:,1);
            yoff = ShotInfo.BOX{box}.Offset(:,2);
            i4 = 0;
            for i5 = 1:5
                if Frame(i5) ~= ShotInfo.ProcessFrameIdx
                    i4 = i4 + 1;
                    
                    [rowSrc,colSrc] = ind2sub([nRow,nCol], idx);
                    rowOff = rowSrc + yoff(i5);
                    colOff = colSrc + xoff(i5);
                    
                    % legalize offset pixel
                    rowOff = min(max(1,rowOff),nRow);
                    colOff = min(max(1,colOff),nCol);
                    
                    idxOff = sub2ind([nRow,nCol], rowOff, colOff);
                    
                    z4 = Z4(:,:,i4);
                    z4(idx) = ShotInfo.Y{Frame(i5)}(idxOff);
                    Z4(:,:,i4) = z4;
                end
            end
        end
    end
    
    % the motion image is maximal discrepency found in Z4.  The value is
    % small when the motion is reliable.  The value is large when the motion
    % is unreliable.
    
    MotionRaw = max(Z4,[],3) - min(Z4,[],3);
    
    % The range of motion values depends on the median intensity.  e.g.
    % brighter areas typically have a larger range than darker areas.
    % Normalize the difference to create a scaled motion image.
    Median = median (Z4, 3);
    MedianSort = sort(Median(:));
    MotionScale = zeros(size(MotionRaw));
    
    NBIN = 100;
    for i = 1:NBIN
        i0 = round(((i-1)/NBIN) * numel(MedianSort)) + 1;
        i1 = round((i/NBIN)*numel(MedianSort));
        
        med0 = MedianSort(i0);
        med1 = MedianSort(i1);
        
        if i == NBIN
            med1 = med1 + .01;
        end
        
        if med1 == med0
            idx = find (Median(:) == med0);
        else
            idx = find((Median(:) >= med0) & (Median(:) < med1));
        end
        mot = MotionRaw(idx);
        
        mot_sort = sort(mot);
        
        mot20(i) = mot_sort (round(.2*numel(mot_sort)));
        mot80(i) = mot_sort (round(.8*numel(mot_sort)));
        
        range(i) = mot80(i) - mot20(i);
        
        MotionScale(idx) = MotionRaw(idx) / range(i);
    end
    
    MOTION_SMOOTH = ones(9,9) / (9*9);
    MotionSmooth = conv2(MotionScale, MOTION_SMOOTH, 'same');
    
    
    % We use the MotionVal to set a threshold for MotionSmooth
    %
    
    MIN_MOTION_THRESH = 1;
    MAX_MOTION_THRESH = 3;
    
    % when MotionVal = 0, Thresh = MIN_MOTION_THRESH
    % when MotionVal = 100, Thresh = MAX_MOTION_THRESH
    Slope = (MAX_MOTION_THRESH-MIN_MOTION_THRESH) / (100-0);
    Inter = MIN_MOTION_THRESH - Slope*0;
    
    Thresh = Slope*MotionTolerance + Inter;
    
    MaskMotion = MotionSmooth > Thresh;
    
end