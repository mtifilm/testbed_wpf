function [RGB,Y,y] = LoadImg (ShotInfo)
    
nFrame = numel(ShotInfo.FRAMES);

RGB{nFrame} = [];
Y{nFrame} = [];
y{nFrame} = [];

% the longest run of frames we'll need is ProcessFrameIdx-4 to 
% ProcessFrameIdx+4
i0 = max(1, ShotInfo.ProcessFrameIdx-4);
i1 = min(nFrame, ShotInfo.ProcessFrameIdx+4);

for i = i0:i1
    frame = ShotInfo.FRAMES(i);
    fname = sprintf (ShotInfo.proto, frame);
    RGB{i} = readdpx(fname);
    Y{i} = rgb2gray(RGB{i});
    y{i} = imresize(Y{i}, 512/size(Y{i},2), 'lanczos3');
end

end