function [Z5, BOX5] = PixelMotion (ShotInfo, SMOOTH_RAD)
%
%  This function assigns a BOX ID to each pixel and stores the result in
%  BOX.  The 5 frames (including the target frame) are motion corrected and
%  placed in Z5.  The 4 frames (not including the target frame) are motion
%  corrected and placed in Z4.

frameIndex = ShotInfo.ProcessFrameIdx;

close all
imshow(ShotInfo.y{frameIndex});
hold on
for iBox = 1:numel(ShotInfo.BOX)
    for iCen = 1:size(ShotInfo.BOX{iBox}.Center,1)
        plot(ShotInfo.BOX{iBox}.Center(iCen,1)/ShotInfo.FACTOR_SCALE, ...
            ShotInfo.BOX{iBox}.Center(iCen,2)/ShotInfo.FACTOR_SCALE, 'go');
    end
end


nX = ShotInfo.FullSize(2);
nY = ShotInfo.FullSize(1);

% create the 5 replacement frames
Z5 = zeros(nY,nX,5);
MSE = zeros(nY,nX);
BOX5 = ones(nY,nX);


% the SMOOTH_RAD can be 0 or larger.  Larger values will reduce effect of
% grain noise on motion assignment.
MSE_Smooth = ones(2*SMOOTH_RAD+1, 2*SMOOTH_RAD+1);
MSE_Smooth = MSE_Smooth / sum(MSE_Smooth(:));

% initialize MSE5 with the zero offset
fullMean = zeros(nY,nX);
for i = 1:5
    idx = ShotInfo.BOX{1}.Frame(i);
    Z5(:,:,i) = ShotInfo.Y{idx};
    fullMean = fullMean + ShotInfo.Y{idx};
end
fullMean = fullMean / numel(ShotInfo.BOX{1}.Frame);
for i = 1:5
    idx = ShotInfo.BOX{1}.Frame(i);
    fullDiff = (ShotInfo.Y{idx}-fullMean);
    MSE = MSE + fullDiff .* fullDiff;
end
MSE = sqrt(MSE/numel(ShotInfo.BOX{1}.Frame));
MSE = conv2(MSE, MSE_Smooth, 'same');
%MSE = imboxfilt(MSE,[2*SMOOTH_RAD+1, 2*SMOOTH_RAD+1];

% enlarge the box for search purposes
SEARCH_FACTOR = 3.0;
BoxRad = round(SEARCH_FACTOR * ShotInfo.ProxyBoxWidth * ShotInfo.FACTOR_SCALE);

Mask = zeros(nY,nX);
for iBox = 2:numel(ShotInfo.BOX)
      
    % motion defined by this box
    Frame = ShotInfo.BOX{iBox}.Frame;
    xoff = ShotInfo.BOX{iBox}.Offset(:,1);
    yoff = ShotInfo.BOX{iBox}.Offset(:,2);
    
    % Where does the current frame land in the five frame trajectory
    iCurr = find(Frame == frameIndex);
    
    % where in the full resolution image can this motion be applied
    for iCenter = 1:size(ShotInfo.BOX{iBox}.Center,1)
        Center = ShotInfo.BOX{iBox}.Center(iCenter,:);
        % define the search region for this box
        x0 = zeros(5,1); x1 = zeros(5,1); y0 = zeros(5,1); y1 = zeros(5,1);
        for j = 1:5
            x0(j) = Center(1) - BoxRad + xoff(j);
            x1(j) = Center(1) + BoxRad + xoff(j);
            y0(j) = Center(2) - BoxRad + yoff(j);
            y1(j) = Center(2) + BoxRad + yoff(j);
        end
        
        % legalize the regions
        if min(x0) < 1
            delta = 1-min(x0);
            x0 = x0 + delta;
        end
        if max(x1) > nX
            delta = nX-max(x1);
            x1 = x1 + delta;
        end
        if min(y0) < 1
            delta = 1-min(y0);
            y0 = y0 + delta;
        end
        if max(y1) > nY
            delta = nY-max(y1);
            y1 = y1 + delta;
        end
        
        xyDat = zeros(y1(1)-y0(1)+1,x1(1)-x0(1)+1,5);
        
        % extract the five regions
        for j = 1:5
            xx = x0(j):x1(j);
            yy = y0(j):y1(j);
            xyDat(:,:,j) = ShotInfo.Y{Frame(j)}(yy,xx);
        end
        
        % average the five pixels
        xySum = zeros(size(xyDat,1), size(xyDat,2));
        for j = 1:5
            xySum = xySum + squeeze(xyDat(:,:,j));
        end
        xyMean = xySum / 5;
        
        % at each pixel find the squared error to the mean
        xyMSE = zeros(size(xyDat,1),size(xyDat,2));
        for j = 1:5
            xyDiff = squeeze(xyDat(:,:,j)) - xyMean;
            xyMSE = xyMSE + xyDiff.*xyDiff;
        end
        
        xyMSE = sqrt(xyMSE/5);
        xyMSE = conv2(xyMSE, MSE_Smooth, 'same');
        
        % for each pixel count number of motions considered
        xxCurr = x0(iCurr):x1(iCurr);
        yyCurr = y0(iCurr):y1(iCurr);
        Mask(yyCurr,xxCurr) = Mask(yyCurr,xxCurr) + 1;
        
        % keep track of the best MSE
        mse = MSE(yyCurr,xxCurr);
        idx = find(xyMSE < mse);
        mse(idx) = xyMSE(idx);
        MSE(yyCurr,xxCurr) = mse;
        bx = BOX5(yyCurr,xxCurr);
        bx(idx) = iBox;
        BOX5(yyCurr,xxCurr) = bx;
        
        for j = 1:5
            dat = squeeze(xyDat(:,:,j));
            z = squeeze(Z5(yyCurr,xxCurr, j));
            z(idx) = dat(idx);
            Z5(yyCurr,xxCurr, j) = z;
        end
        
    end
    
end

end