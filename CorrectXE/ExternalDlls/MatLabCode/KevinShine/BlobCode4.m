function MaskProcess = BlobCode4 (MaskDetect, MaskMotion)
%
% The input to this function is:
%    MaskDetect - the mask of candidate defects
%    MaskMotion - the mask of unreliable motion
%
% The MaskDefect image is returned.  It indicates areas which should be
% processed.

MaskProcess = false(size(MaskDetect));

% calculate the distance to candidtate pixels
DistToBlob = bwdist (MaskDetect);

% connected components
stats = regionprops(DistToBlob<=2, 'PixelIdxList');

for i = 1:numel(stats)
    Idx = stats(i).PixelIdxList;
    
    if sum(MaskMotion(Idx)) == 0
        % this blob does not intersect an area of bad motion
        MaskProcess(Idx) = true;
    end
end

end