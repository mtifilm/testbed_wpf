function [BOX4, MaskDetect] = BlobCode2 (...
    ShotInfo, BOX5, MaskContrast, MinSize, MaxSize)
% 
% The input to this function is:
%    ShotInfo - information about the shot
%    BOX5 - the five-frame motion box assigned at each pixel
%    MaskContrast - the mask of candidate defects
%    MinSize - the value of the minimum debris size
%    MaxSize - the value of the maximum debris size
%
% Connected components of candidate pixels are analyzed and the nearest
% non-candidate pixel is found.  The nearest non-candidate pixel is 
% used for two purposes:   (a) compute debris size and
% (b) update the motion vectors.
%
% This function returns:
%     BOX4 - the four-frame motion box assigned to each pixel
%     MaskDefect - the mask of defective pixels
%     MaskFidelity - the mask with low motion fidelity
%

Y = ShotInfo.Y{ShotInfo.ProcessFrameIdx};
BOX4 = BOX5;
MaskDetect = false(size(Y));

% calculate the distance to candidtate pixels
DistToBlob = bwdist (MaskContrast);

% connected components
stats = regionprops(DistToBlob<=3, 'PixelIdxList');

% process each connected component independently
for i = 1:numel(stats)
    Idx = stats(i).PixelIdxList;
    PixDetect = Idx(DistToBlob(Idx) == 0); % the detected pixels
    PixInt = Idx(DistToBlob(Idx)<=2);    % interior pixels
    PixExt = Idx(DistToBlob(Idx)>2);     % exterior pixels
    
    % keep track of the size of the debris
    DebrisSize = 0;
    
    % for each interior pixel, find the nearest exterior
    [rowInt, colInt] = ind2sub(size(DistToBlob), PixInt);
    [rowExt, colExt] = ind2sub(size(DistToBlob), PixExt);
    for j = 1:numel(rowInt)
        rInt = rowInt(j);
        cInt = colInt(j);
        
       % replace jam 
        delta = [rowExt - rInt, colExt - cInt];
%        delta = [rowExt, colExt] - [rInt, cInt];        
        dist = sqrt(delta(:,1).^2 + delta(:,2).^2);
        [MinDist, k] = min(dist);
        
        % (rowExt(k), colExt(k)) is the closest exterior point.  Assign
        % the nearest neighbor motion
        rExt = rowExt(k);
        cExt = colExt(k);
        BOX4(rInt, cInt) = BOX5(rExt, cExt);
        
        % the size of the debris is the largest distance to the exterior
        % this is useful for long pieces of hair since many pixels may 
        % be involved, but locally the hair always appears small.
        DebrisSize = max(DebrisSize, MinDist);
    end
    
    % disp([num2str(i), ') ', num2str(DebrisSize)]);
    % decide if this candidate should be processed
    UseIt = true;
    if (DebrisSize <= MinSize) || (DebrisSize >= MaxSize)
        UseIt = false;
    end
    
    if UseIt == true
        MaskDetect(PixDetect) = true;
    end
    
end

end
        