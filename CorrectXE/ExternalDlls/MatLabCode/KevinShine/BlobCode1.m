function MaskContrast = BlobCode1 (ShotInfo, Z5, ContrastPos, ContrastNeg)
% 
% The input to this function is:
%    ShotInfo - information about the shot
%    Z5 - the 5 motion compensated frames
%    ContrastPos - the value between 0 and 100 for the positive contrast
%    ContrastNeg - the value between 0 and 100 for the negative contrast
%
% BlobCode1 applies a threshold to the contrast image.  Pixels that satisfy 
% the threshold are candidates for repair.
%
%
% This function returns the MaskContast, the Mask of candidate pixels
% based on contrast.
%

Y = ShotInfo.Y{ShotInfo.ProcessFrameIdx};
Median = median(Z5, 3);
DiffRaw = Y - Median;
MedianSort = sort(Median(:));
DiffScale = zeros(size(DiffRaw));


% The range of difference values depends on the median intensity.  e.g. 
% brighter areas typically have a larger range than darker areas.  
% Normalize the difference to create a contrast image.

NBIN = 100;
med_n = zeros(NBIN,1);
range = zeros(NBIN,1);

for i = 1:NBIN
    i0 = round(((i-1)/NBIN) * numel(MedianSort)) + 1;
    i1 = round((i/NBIN)*numel(MedianSort));

    med0 = MedianSort(i0);
    med1 = MedianSort(i1);

    if i == NBIN
        med1 = med1 + .01;
    end
   
    med_n(i) = (med0 + med1)/2;
    
    if med1 == med0
        idx = find (Median(:) == med0);
    else
        idx = find((Median(:) >= med0) & (Median(:) < med1));
    end
    diff = DiffRaw(idx);

    diff_sort = sort(diff);

    diff20(i) = diff_sort (round(.2*numel(diff_sort)));
    diff80(i) = diff_sort (round(.8*numel(diff_sort)));

    range(i) = diff80(i) - diff20(i);

    DiffScale(idx) = DiffRaw(idx) / range(i);
end

% We use the ContrastPos and ContrastNeg in two ways:
%
%  1.  Make sure the raw diff value is sufficiently large
%  2.  Make sure the scaled diff value is sufficiently large

MIN_RAW_CONTRAST = .1;
MAX_RAW_CONTRAST = .3;
MIN_SCALED_CONTRAST = 1;
MAX_SCALED_CONTRAST = 10;

% when ContrastVal = 0, RawThresh = MIN_RAW_CONTRAST
% when ContrastVal = 100, RawThresh = MAX_RAW_CONTRAST
SlopeRaw = (MAX_RAW_CONTRAST-MIN_RAW_CONTRAST) / (100-0);
InterRaw = MIN_RAW_CONTRAST - SlopeRaw*0;

% when ContrastVal = 0, ScaledThresh = MIN_SCALED_CONTRAST
% when ContrastVal = 100, ScaledThresh = MAX_SCALED_CONTRAST
SlopeScale = (MAX_SCALED_CONTRAST-MIN_SCALED_CONTRAST) / (100-0);
InterScale = MIN_SCALED_CONTRAST - SlopeScale*0;

% Calculate the thresholds for the raw diff
RawThreshPos = SlopeRaw*ContrastPos + InterRaw;
RawThreshNeg = SlopeRaw*ContrastNeg + InterRaw;

% Calculate the thresholds for the scaled diff
ScaleThreshPos = SlopeScale*ContrastPos + InterScale;
ScaleThreshNeg = SlopeScale*ContrastNeg + InterScale;

% create the masks
MaskRaw = (DiffRaw > RawThreshPos) | (DiffRaw < -RawThreshNeg);
MaskScale = (DiffScale > ScaleThreshPos) | (DiffScale < -ScaleThreshNeg);

MaskContrast = MaskRaw & MaskScale;

end
