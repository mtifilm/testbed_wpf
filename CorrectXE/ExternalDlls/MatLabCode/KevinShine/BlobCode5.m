function Dst = BlobCode5 (ShotInfo, BOX4, MaskProcess)
    %
    % The input to this function is:
    %    ShotInfo - information about the shot
    %    BOX4 - the four-frame motion box assigned at each pixel
    %    MaskProcess - the mask of pixels to process
    %
    % This function computes Z4, four motion compensated frames where the
    % current frame is not included.  Z4 is used to generate the motion
    % difference frame.
    %
    % The MaskMotion image is returned.  It indicates areas with unreliable
    % motion.
    %
    % The Dst image is returned.
    
    [nRow, nCol] = size(MaskProcess);
    
    % calculate the distance to candidtate pixels
    DistToBlob = bwdist (MaskProcess);
    
    % connected components.  Group close neighbors into a single blob
    stats = regionprops(DistToBlob<=8, 'PixelIdxList');
    
    Src = ShotInfo.RGB{ShotInfo.ProcessFrameIdx};
    Dst = ShotInfo.RGB{ShotInfo.ProcessFrameIdx};
    
    
    % when distance = 2, the weight is 1
    % when distance = 6, the weight is 0
    Slope = (1-0) / (2-6);
    Inter = 1-Slope*2;
    
    for i = 1:numel(stats)
        
        Idx = stats(i).PixelIdxList;
        
        % there will be four observations for every pixel to be processed
        RGB4 = zeros(4,3);
        Y4 = zeros(4,1);
        count = 0;
        for j = 1:numel(Idx)
            idx = Idx(j);
            Wei = min(1,max(0,Slope*DistToBlob(idx)+Inter));
            
            % only process pixels with positive weight
            if Wei > 0
                count = count + 1;
                box = BOX4(idx);
                Frame = ShotInfo.BOX{box}.Frame;
                xoff = ShotInfo.BOX{box}.Offset(:,1);
                yoff = ShotInfo.BOX{box}.Offset(:,2);
                
                [rowSrc,colSrc] = ind2sub([nRow,nCol], idx);
                i4 = 0;
                for i5 = 1:5
                    if Frame(i5) ~= ShotInfo.ProcessFrameIdx
                        i4 = i4 + 1;
                        
                        rowOff = rowSrc + yoff(i5);
                        colOff = colSrc + xoff(i5);
                        
                        % legalize offset pixel
                        rowOff = min(max(1,rowOff),nRow);
                        colOff = min(max(1,colOff),nCol);
                        
                        RGB4(i4,:) = ShotInfo.RGB{Frame(i5)}(rowOff,colOff,:);
                        Y4(i4) = ShotInfo.Y{Frame(i5)}(rowOff,colOff);
                    end
                end
                
                % sort Y4 and randomly select from middle two
                [~, idx] = sort(Y4);
                if rand(1) < .5
                    Dst(rowSrc,colSrc,:) = Wei*RGB4(idx(2),:)' + ...
                        (1-Wei)*squeeze(Src(rowSrc,colSrc,:));
                else
                    Dst(rowSrc,colSrc,:) = Wei*RGB4(idx(3),:)' + ...
                        (1-Wei)*squeeze(Src(rowSrc,colSrc,:));
                end
            end
        end
%         disp([num2str(i), ' ', num2str(numel(Idx)),' ', num2str(count)])

    end
    
end