function l=poly_regularized(a,b,mm,MM,epsilon,order,fignum)

% NOTE ON PARAMETERS:  I'VE HAD NO PROBLEMS USING 

% epsilon=.0001
% order=5 or order=4 (order of the polynomial fit for the central range of
%                     lut values)

% (Actually, it's very stable to both order and epsilon.  From what I've
% seen of this version, epsilon can go smaller and order can go higher with
% very little change.  But for sure there's trouble if epsilon is zero
% (since the solution can float up or down) and there has to be trouble at
% very high orders, though even order 10 behaves very well.)

% mm and MM are the minimum and maximum intensitites over which there is
% good data, meaning large sample.  These can be set quite aggressively
% (maximize the range).

% fignum is the first figure in a series of figures used to display some 
% of the results.  

% a and b have length equal to the number of intensity levels
% a(n) is the original intensity level that achieves the third 
% quartile of the neighbors of n, and b(n) is the intensity that 
% achieves the first quartile.  a(n) and b(n) are set to zero for n out
% of range of useful data (see m and M, below).

% REMARK:  NOT CLEAR THAT WE WANT TO USE THESE PARTICULAR MEASURES OF
% THE 'ABOVE' AND 'BELOW' DISTRIBUTIONS.  FOR EXAMPLE, IT MIGHT MAKE
% MORE SENSE TO USE MEDIAN OF EVERYTHING ABOVE CURRENT LEVEL FOR A, AND
% MEDIAN OF EVERYTHING BELOW FOR B.  OR MAYBE OTHER PERCENTILES, OR EVEN
% ADDITIONAL PERCENTILES, MIGHT BE BETTER.  (CAN EASILY ADD ADDITIONAL
% QUADRADIC CONSTRAINTS.)

% a and b are column vectors

% Let N be the total number of intensities:

N = numel(a);

% Need to put a and be on the same scale as will be used during the
% computations the lut, l.  It's easiest to make the initial value for l be
% one: l(n)=1, so that, e.g., '5', refers to the gray level 4.

a=a+1;
b=b+1;

% At the end, the lut values are decremented by 1 so that the first value
% is zero and the last is N-1.

% The maximum gap is the maximum over all n between mm and MM of the
% gap between a(n) and n and between n and b(n).  It is used to define the
% range of n that will contribute to the cost function.

max_gap = round(max( max(a(mm:MM)-(mm:MM)'), max((mm:MM)'-b(mm:MM)) ));

% The values of n from m to M (defined next) each contribute a term in the
% cost function, which tries to minimize the difference between the average
% gap and each of the above and below gaps for this set of n.  (This does
% not mean that the lut, l, at other values of n are not changed in 
% minimizing the cost.  Some of
% these enter because they are the position pointed to by a or b for some
% values within the range m to M.)

m=mm+max_gap;
M=MM-max_gap;

m = mm;
M = MM;
% Will only use the "above" and "below" values for n=m,m+1,...,M.

a(1:m-1)=0;
b(1:m-1)=0;
a(M+1:N)=0;
b(M+1:N)=0;

% Referring to a and b following the substitution of these zeros:
% na(n) is the number of intensities i for which n=a(i), some value i, and
% nb(n) is the number for which n=b(i). Either or both could be zero.

% A(n,k), k=1,2,...,na(n), is the value of the k'th intensity i for which 
% a(i)=n.
% B(n,k), k=1,2,...,nb(n), is the value of the k'th intensity i for which
% b(i)=n.

% Thus a(A(n,j)) = b(B(n,k)) = n, for allowed values of j and k.

% l is a vector of length N. The goal is to compute l(n), 
% n=1,2,...,N, so as to make all of the gaps, to the above and below 
% values (a(n) and b(n)), across all intensities, as nearly equal as 
% possible.  

% Gaps are made close by making them all close to a common number, for
% which we take the average gap over all active elements of l, when l is
% the identitiy lut:

G=( sum(a(m:M)-(m:M)') + sum((m:M)'-b(m:M)) )/(2*(M-m)+2);

% The new gap, after applying l, is (l(a(n))-l(n)), for the a values,
% and (l(n)-l(b(n))) for the b values.  The cost is the sum over all
% legitimate gaps (see remarks above about m and M) of the square of the 
% gap minus G:   sum (gap - G)^2 .

% The minimizer is not unique (e.g. same cost by adding a constant to
% every element of l, and there may be other ways to preserve
% cost, I'm not sure).  So cost is "regularized" by adding 
% epsilon ||l(m:M)-(m:M)||^2

% In this version (as opposed to solve-regularized.m), l(m:M) is directly
% fit with a polynomial of order 'order' (power of highest term).

% The cost is quadradic, so solution is by matrix inverse.

% The last step is to do extension from m down to 1 and from M up to N, by
% using third-order polynomials that require l(1)=1, l(N)=N, and matches of
% values and derivatives at m and M.


% Initializations

na=zeros(1,N);
nb=zeros(1,N);

A=zeros(N,N);
B=zeros(N,N);

% Construct the inverse functions, A and B

count=1:N;

for n=1:N
    
    places=(a==n);
    indices=count(places);
    na(n)=sum(places);
    A(n,1:na(n))=indices;
    
    places=(b==n);
    indices=count(places);
    nb(n)=sum(places);
    B(n,1:nb(n))=indices;

end

% A and B are big.  Make them as small as possible:

mosta=max(na);
mostb=max(nb);

A=A(:,1:mosta);
B=B(:,1:mostb);

% Build the matrix (T) and vector (V) of the linear system that solves for
% the polynomial coefficients c=(c(1),c(2),...,c(order+1)) corresponding to
% exponents 0,1,...,order.  This polynomial will be used for the fit on the
% interval m to M.  But it is computed using a change of variables, l(n) is
% given by the polynomial evaluated at the standardized value of n on the
% interval m to M.

% Initialize T and V

O=order+1;   % Capital o for order+1

T=zeros(O,O);
V=zeros(O,1);

% Polynomial will be in terms of centered and scaled value of n, which
% might improve stability

mu=(N+1)/2;
sigma2=N*(N+1)*(2*N+1)/(6*N) - mu^2;
sigma=sqrt(sigma2);
x=( (1:N)-mu )/sigma;

% Fill in the vector V

for q=1:O

    qexp=q-1;   % annoying problem with indexing not starting at zero
                % this is the exponent in the polynomial
    
    for n=1:m-1
        V(q)=V(q)-G*nb(n)*x(n)^qexp;
    end
    
     for n=M+1:N
         V(q)=V(q)+G*na(n)*x(n)^qexp;
     end
     
     for n=m:M
         V(q)=V(q)+G*(na(n)-nb(n))*x(n)^qexp + epsilon*n*x(n)^qexp;
     end
    
end
 
% Fill in the matrix T

for q=1:O
for r=1:O

    qexp=q-1;   % annoying problem with indexing not starting at zero
                % this is the exponent in the polynomial
    rexp=r-1;   % annoying problem with indexing not starting at zero
                % this is the exponent in the polynomial
    
    for n=1:m-1
        
        Hold=0;
        for k=1:nb(n)
            Hold=Hold+x(B(n,k))^rexp;
        end
        
        T(q,r)=T(q,r)+x(n)^qexp*(nb(n)*x(n)^rexp - Hold);
        
    end
    
    for n=M+1:N
        
        Hold=0;
        for k=1:na(n)
            Hold=Hold+x(A(n,k))^rexp;
        end
        
        T(q,r)=T(q,r)+x(n)^qexp*(na(n)*x(n)^rexp - Hold);
        
    end
        
    for n=m:M
        
        Holdb=0;
        for k=1:nb(n)
            Holdb=Holdb+x(B(n,k))^rexp;
        end
        
        Holda=0;
        for k=1:na(n)
            Holda=Holda+x(A(n,k))^rexp;
        end      
        
        f1 = (na(n)+nb(n)+2+epsilon)*x(n)^rexp;
        f2 = - x(b(n))^rexp - x(a(n))^rexp ;
%         T(q,r) = T(q,r) + x(n)^qexp * (f1 + f2) ; %* (f1 + f2 - Holdb - Holda);
        T(q,r) = T(q,r) + x(n)^qexp * (f1 + f2 - Holdb - Holda);
    end
    
end    % end r loop
end    % end q loop
        
% Solve for the polynomial coefficients ('c'):

c=T^(-1)*V;

% Fill in lut for n=m,...,M

l=zeros(1,N);

for n=m:M
    
    for q=1:O
        l(n)=l(n)+c(q)*x(n)^(q-1);
    end
    
end

% Now extrapolate with two quadratic functions, one for each end.  These
% are determined by matching the derivative and value at m (or M), and then
% demanding that l(1)=1 (or l(N)=N). (But all of this is done in the scaled
% x values, and later turned back to integer gray levels.)

% Equation is Ae=f, where e has the coeficients of the extrapolating
% polynomial.

% extrapolate for n from 1 to m (i.e. extrapolate to x(1),...,x(m))

A=zeros(3,3);
f=zeros(3,1);

% make extrapolation agree with l at n=m

A(1,1)=1;
A(1,2)=x(m);
A(1,3)=x(m)^2;

f(1)=0;
for q=1:O
    f(1)=f(1)+c(q)*x(m)^(q-1);
end

% make l(1)=1

A(2,1)=1;
A(2,2)=x(1);
A(2,3)=x(1)^2;

f(2)=1;

% match derivative of extrapolation to derivative of l at n=m

A(3,1)=0;
A(3,2)=1;
A(3,3)=2*x(m);

f(3)=0;
for q=2:O
    f(3)=f(3)+c(q)*(q-1)*x(m)^(q-2);
end

% solve for coefficients

e=A^(-1)*f;

% fill in lut for n<m

for n=1:m-1
    
    l(n)=e(1)+e(2)*x(n)+e(3)*x(n)^2;
    
end

% extrapolate for n from M to M+1 (i.e. extrapolate to x(M),...,x(N))

A=zeros(3,3);
f=zeros(3,1);

% make extrapolation agree with l at n=M

A(1,1)=1;
A(1,2)=x(M);
A(1,3)=x(M)^2;

f(1)=0;
for q=1:O
    f(1)=f(1)+c(q)*x(M)^(q-1);
end

% make l(N)=N

A(2,1)=1;
A(2,2)=x(N);
A(2,3)=x(N)^2;

f(2)=N;

% match derivative of extrapolation to derivative of l at n=m

A(3,1)=0;
A(3,2)=1;
A(3,3)=2*x(M);

f(3)=0;
for q=2:O
    f(3)=f(3)+c(q)*(q-1)*x(M)^(q-2);
end

% solve for coefficients

e=A^(-1)*f;

% fill in lut for n<m

for n=M+1:N
    
    l(n)=e(1)+e(2)*x(n)+e(3)*x(n)^2;
    
end

% Force monotonicity

l=max(1,l);
l=min(N,l);
    
for n=2:N
    l(n)=max(l(n),l(n-1));
end

% Fix l to be integer and to have range [0,N-1] instead of [1,N]

l=round(l)-1;

% Plot some diagnostic figures

figure(fignum);plot(l)
figure(fignum+1);plot(l:N);hold on;plot(m:M,a(m:M)+1);plot(m:M,b(m:M)+1);hold off
figure(fignum+2);plot(l);hold on;plot(m:M,l(a(m:M)+1));plot(m:M,l(b(m:M)+1));hold off
smallest=min(a(m:M)-b(m:M));
largest=max(a(m:M)-b(m:M));
figure(fignum+3);plot(m:M,a(m:M)-b(m:M))
axis([m M smallest largest])
figure(fignum+4);plot(m:M,l(a(m:M)+1)-l(b(m:M)+1))
axis([m M smallest largest])

% Here's a typical run:

% Get 'above' and 'below' values:

%              a=RES_lol(:,2);b=RES_lol(:,1);

% Plot a and b and the diagonal and determine the cutoffs mm and MM:

%              figure(1);plot(1:N);hold on;plot(a);plot(b);hold off
%              mm=75;MM=775;

% Set parameters and a first figure number for the output diagnostics:

%              epsilon=.0001;order=5

% Run the code.  The output is a full lut that maps 1,2,....,N (the maximum
% possible gray level) into 0,1,...,N-1 (think of the first entry, l(1), as
% the destination of intensity 0 - which happens to always be 0 - so this
% is just the usual matlab versus c problem).

%              fignum=1;
%              l=solve_regularized(a,b,mm,MM,epsilon,order,fignum);