load D:\Temp\mom\111286.mat;
Y0 = double(rgb2gray(dpxImage))/65535;

load D:\Temp\mom\111287.mat;
Y1 = double(rgb2gray(dpxImage))/65535;

[X0, M0, S0] = CollectStats (Y0);

LUT = standardize(X0,S0);

y0 = LUT(round(1023*Y0)+1) / 1023;
y1 = LUT(round(1023*Y1)+1) / 1023;

[x0, m0, s0] = CollectStats(y0);
[x1, m1, s1] = CollectStats(y1);

hold off
plot(X0,S0,'r')
hold on
plot(x0,s0,'g')
plot(x1,s1,'b')
