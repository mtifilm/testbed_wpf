function [m, sd] = missingMeanAndSd(m, sd)
% this removes any missing values in m and sd
oldm = m(1);
oldsd = sd(1);
for i = m : numel(m)
    if (m(i) == 0)
        m(i) = oldm;
    else
        oldm = m(i);
    end
    if (sd(i) == 0)
        sd(i) = oldsd;
    else
        oldsd = sd(i);
    end
end
end