function readWriteLut(lut, filePath, prefix, startFrame, maxFrames )
    %   Read the mat files from the path
    %  images are cells containing a structure frameInfo
    %  this is added to at later dates, works for Matlab not C++
    filePath = [filePath, '\'];
    outFilePath = [filePath, prefix];
    fileList = dir(strcat(filePath, '\*.DPX'));
    
    if (numel(fileList) == 0)
        error('Error:  No files found');
    end
    
    % for debugging
    maxFrames = startFrame + maxFrames - 1;
    totalFrames = min(numel(fileList), maxFrames);

 %   images = cell(totalFrames - startFrame + 1,1);
    disp('starting');
    for i = startFrame:totalFrames
       % fprintf_r('Frame: %d', i);
        f = fileList(i);
        name = f.name;
        fileName = strcat(filePath, name);
        outFileName = strcat(outFilePath, name);
        fprintf('Frame: %d, input: %s\n', i, fileName);
        fprintf('Out file: %s\n', outFileName);
                
        dpxData = dpxRead(fileName);
        %bits = dpxData{3};
        dpxImage = dpxData{2}; % / bitshift(1, bits);
        lutImage = round(lut(dpxImage+1));
        dpxData{2} = lutImage;
        dpxWrite(outFileName, dpxData);
    end