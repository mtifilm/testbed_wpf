% counts are the averages near a given intensity value
% maxValue is 1024 for 10 bits
% value is intensity value that collected the histogram. 
 %[m, sd, s] = arrayfun(@(x) meanAndSdFromHistogram(monoCounts{x},1024, x), [1:1024]);
function [m, sd, s] = meanAndSdFromHistogram(counts, maxValue, defaultMean)
    numBins = numel(counts);
    factor = maxValue / numBins;
    base = factor * [0:numBins-1];
    p = double(counts) .* base;
    n = sum(counts);
    if (n == 0)
        m = defaultMean;
        sd = 0;
        s = 0;
        return;
    end
    
    m = sum(p) / n;
    q = double(counts) .* ((base - m).^2);
    sd = sqrt(sum(q) / n);
    s = n;
end