function L=standardize(Means,SEs)

% Means     what Kevin calls Xk, various k's for various examples
% SEs       what Kevin calls Sk, various k's for various examples


n=numel(Means);
% Means=1023*Means+1;
Means = Means+1;
umin=ceil(Means(1))-1;  % last value of t (see below) that will not be used
umax=floor(Means(n));   % last value of t that will be used

t=zeros(1,1024);   % the standard error function in Miller's notation - the
                   % indexing is just for convenience - generally only a
                   % small sub-interval of the indices are used

l=umin+1;
SEs1 = SEs(1);
SEs2 = SEs(2);
for k=2:n
    gap=Means(k)-Means(k-1);
    while(l<=Means(k))
        if (gap <= 0)
           wt = 0;
        else                
           wt=(l-Means(k-1))/gap;
        end
        %hack
        if (SEs(k-1) ~= 0), SEs1 = SEs(k-1);, end;
        if (SEs(k) ~= 0), SEs2 = SEs(k);, end;
        %% t(l)=(1-wt)*SEs(k-1)+wt*SEs(k);
        t(l)=(1-wt)*SEs1+wt*SEs2;        
        l=l+1;
    end
end

integral=zeros(1,1024);  % here too, only a sub-interval of the indices are
                         % used - rest if for indexing convenience
    
for l=umin+1:umax
    integral(l)=integral(l-1)+(1/t(l));
end

% Will scale the integral to get the look-up table (L) to satisfy 

%     L(umin)=umin
%     L(umax)=umax

ScaleFactor=(umax-umin)/(integral(umax));

% Build the LUT (L)

L=zeros(1,1024);
L(umin)=umin;
% L(umin+1:umax)=round(umin+ScaleFactor*integral(umin+1:umax));
L(umin+1:umax)= umin+ScaleFactor*integral(umin+1:umax);


% Finally, extrapolate with monotone increasing cubic spline to L(1)=1 and 
% L(1024)=1024.

% 1 to umin

x=[1 umin umin+1];
y=[1 L(umin) L(umin+1)];
ts=1:1:(umin-1);

p=pchip(x,y,ts);
L(ts)=p;

% umax+1 to 1024

x=[umax-1 umax 1024];
y=[L(umax-1) L(umax) 1024];
te=umax+1:1024;

p=pchip(x,y,te);
L(te)=p;

% % Finally, extrapolate with cubic to L(1)=1 and L(1024)=1024 (each
% % extrapolation is just a matter of solving three linear equations in three
% % unknowns)
% 
% % 1 to umin
% 
% A=zeros(3,3);
% A(1,:)=[1 1 1];
% A(2,:)=[1 umin umin^2];
% A(3,:)=[1 (umin+1) (umin+1)^2];
% 
% b=[1 L(umin) L(umin+1)]';
% 
% c=A\b;
% 
% for l=1:umin-1
%     L(l)=round(c(1)+c(2)*l+c(3)*l^2);
% end
% 
% % umax+1 to 1024
% 
% A=zeros(3,3);
% A(1,:)=[1 (umax-1) (umax-1)^2];
% A(2,:)=[1 umax umax^2];
% A(3,:)=[1 1024 1024^2];
% 
% b=[L(umax-1) L(umax) 1024]';
% 
% c=A\b;
% 
% for l=umax+1:1024
%     L(l)=round(c(1)+c(2)*l+c(3)*l^2);
% end