function [X, M, S] = CollectStats (SRC, rows, cols)
%
%  This function collect statistics for the middle pixel
%  conditioned on the 8 surrounding pixels.
%
%  Usage:
%            [X, M, S] = CollectStats (SRC, rows, cols)
%  or
%            [X, M, S] = CollectStats (SRC)
%
%  where:
%
%        X = average of the 8 surrounding pixels
%        M = mean center pixel, conditioned on the surrounding values
%        S = standard deviation of center pixel, conditioned on surrounding
%            values
%
%        SRC = source image in range 0 to 1.  Can be Y or RGB.
%        rows =  ROI, vertical
%        cols =  ROI, horizontal
%

if nargin == 1,
    rows = 1:size(SRC,1);
    cols = 1:size(SRC,2);
end

if ndims(SRC) == 2,
    %  source is two dimensions with a single color component
    Y = SRC(rows, cols);
else
    % must be a three dimensional source.  Assume RGB
    Y = rgb2gray (SRC(rows, cols, :));
end

Neigh = [1 1 1; 1 0 1; 1 1 1];
Neigh = Neigh / sum(Neigh(:));

% compute the average of the 8 neighbors at each pixel
YN = conv2(Y, Neigh, 'same');

DISCRETE_FACTOR = 100;

% discretize the neighbor averages
YN = round (DISCRETE_FACTOR*YN);

%  trim the Y and YN to avoid edge effects
[m,n] = size(Y);
Y = Y(2:m-1, 2:n-1);
YN = YN(2:m-1, 2:n-1);

X = zeros(DISCRETE_FACTOR+1,1);
M = zeros(DISCRETE_FACTOR+1,1);
S = zeros(DISCRETE_FACTOR+1,1);

Cnt = 0;

for i = 0:DISCRETE_FACTOR,
    
    ind = find (YN == i);
    
    if numel(ind) > .001*numel(Y),
        Cnt = Cnt + 1;
        X(Cnt) = i / DISCRETE_FACTOR;
        M(Cnt) = mean(Y(ind));
        S(Cnt) = std(Y(ind));
    end
end

X = X(1:Cnt);
M = M(1:Cnt);
S = S(1:Cnt);

end



