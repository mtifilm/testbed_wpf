function inverseLut = invertLut(lut)
  n = numel(lut);
  
  % lut is 0 to n-1, make it 1 to n because of matlab indexing
  % we assume 0 -> 0
  lut = lut+1;
  il = 1;
  lut(n+1) = 100000;
  inverseLut = zeros(n, 1);
  for i = 1:n-1  
      while (lut(il) < i), il = il + 1; end;
      jl = il + 1;
        
      while (lut(jl) < i), jl = jl + 1; end;
     % disp([i, il, jl, lut(il), lut(jl)]);
      inverseLut(i) = il;
  end
  
  inverseLut(n) = lut(n);
  inverseLut = inverseLut - 1;
end