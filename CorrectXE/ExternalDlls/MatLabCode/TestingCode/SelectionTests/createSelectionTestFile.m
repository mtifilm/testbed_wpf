function createSelectionTestFile(imageData, rects, copyMode, outputFileName);
% for testing Ipparray
% Use the image from mom_112657.mat in nova extracted by (51:304, 51:04)
% or what the MtiCopyTMtiSelectTypeMirrorModes.matypeMirrorTest uses for imagecreate
    imageSize = size(imageData);
    save(outputFileName,'imageSize');
    
    for i = 1:size(rects,1)
        rect = rects(i);
        S.(rect.name) = extractPaddedImage(imageData, rect, copyMode);
        S.(strcat(rect.name, 'Rect')) = rect;
        save(outputFileName, '-struct', 'S', '-append');
    end;
end