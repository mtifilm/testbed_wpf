opticFlow = opticalFlowFarneback;
opticFlow.reset();
for i = 1:166
    i
    frameRGB = double(readimage(imageStore, i))/65535;

    frameGray = rgb2gray(frameRGB);
    flow = estimateFlow(opticFlow,frameGray);
    imshow(frameGray)
    hold on
%     plot(flow,'DecimationFactor',[20 20],'ScaleFactor',5,'Parent',hPlot);
    plot(flow,'DecimationFactor',[10 10],'ScaleFactor',5);

    hold off
     pause(10^-3)
end
