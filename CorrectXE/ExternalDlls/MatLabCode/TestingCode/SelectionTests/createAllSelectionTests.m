function createAllSelectionTests(imageData)
%  mirror test
rects = createSelectionRects(size(imageData), 44, 32, 128, 100);
createSelectionTestFile(imageData, rects, 'symmetric', 'd:\temp\matlabMirrorData.mat');
createSelectionTestFile(imageData, rects, 'circular', 'd:\temp\matlabCircularData.mat');
createSelectionTestFile(imageData, rects, 'replicate', 'd:\temp\matlabReplicateData.mat');

% mirror is special, there are two paths in code depending if the valid
% arrary is less than 50% or greater.
rects = createSelectionRects(size(imageData), 70, 64, 128, 100);
createSelectionTestFile(imageData, rects, 'symmetric', 'd:\temp\matlabMirror2Data.mat');
end
