function croppedImage = extractPaddedImage(imageData, rect, copyMode)
% Create 
x = rect.x;
y = rect.y;
w = rect.width;
h = rect.height;

pad = max(w, h);
paddedArray = padarray(imageData, [pad, pad], copyMode);

x1 = x + pad + 1;
y1 = y + pad + 1;
x2 = x1 + w - 1;
y2 = y1 + h - 1;
croppedImage = paddedArray( y1:y2, x1:x2,:);

end
