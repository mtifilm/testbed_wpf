% Given an image, this creates nine boxes with names
% used to test the selection process
% dx, dy represent the distance from upper right hand box to 0,0
% for example
%   rects = createSelectionRects(size(imageData), 44, 32, 128, 100);
function rects = createSelectionRects(imageSize, dx, dy, w, h)
    imageWidth = imageSize(2);
    imageHeight  = imageSize(1);
    imageCenterX = round(imageWidth - w)/2;
    imageCenterY = round(imageHeight - h)/2;
    
    rects = cell(9, 1);
    box = MtiRect(-dx, -dy, w, h);
    box.name = 'mul';
    rects{1} = box;
    
    box = MtiRect(imageCenterX, -dy, w, h);
    box.name = 'muc';
    rects{2} = box;

    box = MtiRect(imageWidth-dx, -dy, w, h);
    box.name = 'mur';
    rects{3} = box;
    
    box = MtiRect(-dx, imageCenterY, w, h);
    box.name = 'mcl';
    rects{4} = box;
    
    box = MtiRect(imageCenterX, imageCenterY, w, h);
    box.name = 'mcc';
    rects{5} = box;
    
    box = MtiRect(imageWidth-dx, imageCenterY, w, h);
    box.name = 'mcr';
    rects{6} = box;
    
    box = MtiRect(-dx, imageHeight-dy, w, h);
    box.name = 'mll';
    rects{7} = box;
    
    box = MtiRect(imageCenterX, imageHeight-dy, w, h);
    box.name = 'mlc';
    rects{8} = box;
    
    box = MtiRect(imageWidth-dx, imageHeight-dy, w, h);
    box.name = 'mlr';
    rects{9} = box;
    
    rects = cell2mat(rects);
end
