% to convert from the histogram data to the M20, M80 lines 
% [m, sd, M20, M80] =cellfun(@(x) meanAndSd(x), counts);
function [m, sd, m2, m8] = meanAndSd(D)
    p = zeros(sum(D), 1);
    k = 1;
    for i = 1:numel(D)
        n = D(i);
        for j=1:n
            p(k) = i - 1;
            k = k + 1;
        end
    end
    
    n = numel(p);
    if (n > 1)
        m2 = p(round(0.2 * n)+1);
        m8 = p(round(0.8 * n));
    else
        m2 = 0;
        m8 = 0;
    end
    
    m = mean(p);
    sd = std(p);
end