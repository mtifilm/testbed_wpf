% load c:\temp\mvalues.mm.mat
function test(C20, C80)
    low = 150;
    high = 500;
    
    centerLine = transpose(1:size(C20, 1));
    m20_s = C20(low:high) - centerLine(low:high);
    m80_s = C80(low:high) - centerLine(low:high);
    
    figure
    hold off
    plot(m20_s); hold; plot(m80_s);
    plot(centerLine(low:high) - centerLine(low:high), 'black');
    plot(m80_s - m20_s - (m80_s(1) - m20_s(1)),'red');
    
    binSize = cellfun(@(x)sum(x), counts);
    plot(min(40,binSize(low:high) * 3*max(m80_s)/max(binSize)),'black');
    hold off
    % plot(m80); hold; plot(m20); plot(binSize * max(m80)/max(binSize),'red'); hold
    % plot(log(m80_s),'blue');
    % hold
    % plot(log(-m20_s),'yellow');
end
