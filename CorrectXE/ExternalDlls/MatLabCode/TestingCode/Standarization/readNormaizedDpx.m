function rgb = readNormaizedDpx(filename)
    [folder, baseFileName, ~] = fileparts(filename)
    % Ignore extension and replace it with .txt
    newBaseFileName = sprintf('%s.mat', baseFileName)
    % Make sure the folder is prepended (if it has one).
    newFullFileName = fullfile(folder, newBaseFileName)
    
    dpxImage = [];   % jam
    load(newFullFileName);    % jam
    rgb = double(dpxImage)/ 65535.0;  % jam
end
