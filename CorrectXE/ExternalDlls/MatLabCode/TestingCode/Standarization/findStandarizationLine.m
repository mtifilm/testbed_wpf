function [m20, m80] = findStandarizationLine(framesInfo)
    counts = cell(65536,1);
    for i = 1:2
        image = int32(framesInfo(i).fullGrayImage);
        width = size(image, 2);
        height = size(image, 1);
        for r = 1:height
            for c = 2:width-1
                v = image(r,c);
                v0 = image(r,c-1);
                v1 = image(r,c+1);
                counts{v+1}(end+1) = (v0 + v1) / 2;
            end
            if (mod(r,10) == 0)
                disp(int2str(r));
            end
        end
    end
    
    m20 = counts;
    m80 = counts;
end