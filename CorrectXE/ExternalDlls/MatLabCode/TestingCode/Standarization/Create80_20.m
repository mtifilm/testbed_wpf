function [m20, m80, counts] = Create80_20 (SRC, rows, cols)
    %
    %  This function collect statistics for the middle pixel
    %  conditioned on the 2 neighbors
    %
    %  Usage:
    %            [X, M, S] = CollectStats (SRC, rows, cols)
    %  or
    %            [X, M, S] = CollectStats (SRC)
    %
    %  where:
    %
    %        X = average of the 8 surrounding pixels
    %        M = mean center pixel, conditioned on the surrounding values
    %        S = standard deviation of center pixel, conditioned on surrounding
    %            values
    %
    %        SRC = source image in range 0..1023
    %        rows =  ROI, vertical
    %        cols =  ROI, horizontal
    %
    
    if nargin == 1,
        rows = 1:size(SRC,1);
        cols = 1:size(SRC,2);
    end
    
    if ndims(SRC) == 2,
        %  source is two dimensions with a single color component
        Y = SRC(rows, cols);
    else
        % must be a three dimensional source.  Assume RGB
        Y = rgb2gray (SRC(rows, cols, :));
    end
    
%     Y = Y / 64;
    
    Neigh = [0 0 0; 1 0 1; 0 0 0];
    Neigh = Neigh / sum(Neigh(:));
    
    % compute the average of the 8 neighbors at each pixel
    YN = conv2(single(Y), Neigh, 'same');
    YN = int32(YN);
    
    DISCRETE_FACTOR = 1023;
    NBINS = 1024;  % for now mush match discrete factor
    
    %  trim the Y and YN to avoid edge effects
    [m,n] = size(Y);
    Y = Y(2:m-1, 2:n-1);
    YN = YN(2:m-1, 2:n-1);
    
    M20 = zeros(DISCRETE_FACTOR+1,1);
    M80 = zeros(DISCRETE_FACTOR+1,1);
    H = zeros(DISCRETE_FACTOR+1, NBINS);
    
    YN_min = min(YN(:));
    YN_max = max(YN(:));
    counts = cell(NBINS, 1);
    for i = YN_min:YN_max
        
        ind = find (YN == i);
        if numel(ind) > 0,
            YH = Y(ind);
            for k=1:numel(YH)
                H(YH(k)+1, i+1) = H(YH(k)+1, i+1) + 1;
            end
        end
        
        counts{i+1} = H(:, i+1);
    end
    
    
    [m, sd, m20, m80] = cellfun(@(x)meanAndSd(x), counts);
end



