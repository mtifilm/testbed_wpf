function tracks = makeStartTracks(framesInfo, patchBoxes, frameIndex, params)
% This just initializes a set of tracks
% A track has the start proxy patch image and original patch image,
% intensity scaled
    proxyImage = framesInfo(frameIndex).proxyImage;
    
    tracks = cell(numel(patchBoxes), 1);
    for patchIndex = 1:numel(patchBoxes);
        patch = patchBoxes(patchIndex);
        track.boxes(1) = patch;
        track.startProxyImage = extractImage(proxyImage, patch);
        track.startFrameIndex = frameIndex;
        track.active = true;

        % We need the full size start patch
        rx = double(params.width) / double(params.proxyWidth);
        ry = double(params.height) / double(params.proxyHeight);
        largeBox = expandBox(patch, [ry, rx]);
        track.startFullImage = extractImage(framesInfo(frameIndex).fullGrayImage, largeBox) * 0.5 / framesInfo(frameIndex).averageIntensity;        
        
        % store the track in the vector
        tracks{patchIndex} = track;
    end
    
    tracks = cell2mat(tracks);
end
