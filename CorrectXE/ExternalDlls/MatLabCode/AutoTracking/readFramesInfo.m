function [framesInfo, params] = readFramesInfo(pathNameTrailingBackSlash, maxFrames)
    if ~exist('maxFrames','var')
        maxFrames = 1000;
    end
    
    disp(['Reading directory ', pathNameTrailingBackSlash, ', max frames',maxFrames]);
    [framesInfo, params] = ReadGrayScaleImagesMat(pathNameTrailingBackSlash, maxFrames);
    
    disp(['Frames read: ', num2str(numel(framesInfo))]);
    framesInfo = createProxiesAndAverageIntensity(framesInfo, params);
    disp(['Proxies created: ', num2str(numel(framesInfo))]);
end