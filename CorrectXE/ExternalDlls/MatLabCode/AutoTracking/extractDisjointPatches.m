function newDisjointPatches = extractDisjointPatches(activeTracks, patchCenters, params)

% this is ugly, but I have not idea how to do this without a loop
slice = cell(numel(activeTracks), 1);
for i = 1:numel(activeTracks)
    slice{i} = activeTracks(i).boxes(end);
end

slice = cell2mat(slice);

% we are assuming all boxes are same size
w = 2*params.boxRadius + 1;
h = 2*params.boxRadius + 1;

valid = false(numel(patchCenters), 1);
for i = 1:numel(patchCenters)
    p = patchCenters(i);
    x0 = p.x;
    y0 = p.y;
    x = [slice.x];
    s = slice(min(x + w, x0 + w) - max(x, x0) > 0);
    
    % make sure there are nothing nearby 
    if (numel(s) == 0)
        valid(i) = true;
        continue;
    end
    y = [s.y];
    if (numel(s) == 0)
        valid(i) = true;        
        continue;
    end
    s = s(min(y + h, y0 + h) - max(y, y0) > 0);
    valid(i) = numel(s) == 0;
end

newDisjointPatches = patchCenters(valid);
end
