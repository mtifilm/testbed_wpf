function plotTracks(tracks, downSample, color)
if nargin == 1
    downSample = 1;
    color = 'blue';
end;

if nargin == 2
    color = 'blue';
end;

    tracks = nestedSortStruct(tracks, 'startFrameIndex');

    for t = tracks([1:downSample:numel(tracks)])
        b = t.boxes;
        c = [t.startFrameIndex:(t.startFrameIndex + numel(b) -1)];
         plot(c, [b.x], '-s', 'color', color, 'MarkerSize', 2);
%        plot3(c, [b.xc], [b.yc], 'color', 'blue', 'MarkerSize', 2);
        hold on
    end
    hold off
    
   figure
   colors = {'red','magenta','blue', 'black'};
   i = 0;
    for t = tracks([1:downSample:numel(tracks)])
        b = t.boxes;
        c = [t.startFrameIndex:(t.startFrameIndex + numel(b) -1)];
        plot3(c, [b.xc], [b.yc], 'color', colors{i+1});
        i = mod(i + 1, 4);
        hold on
    end   
    
    drawnow;
end