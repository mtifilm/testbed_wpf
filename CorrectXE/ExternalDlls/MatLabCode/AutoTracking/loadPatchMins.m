function patchMins = loadPatchMins(i)
    fileName = strcat('c:\temp\patchMins_', num2str(i), '.mat');
    load(fileName);
    patchMins = patchMinsC;
end
