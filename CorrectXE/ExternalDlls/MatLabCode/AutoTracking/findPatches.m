function patchCenters = findPatches(patchMins, params)
    
% first sort the matchMins, largest first
[P, indexP] = sort(patchMins(:), 'descend');

% Just keep track of the index and r,c, keeping both makes loop easier
Q = zeros(size(P,1), 3);
rows = size(patchMins,1);
Q(:,1) = P;
% Q(:,2) = mod(indexP,rows);
% Q(:,3) = floor(indexP /rows);
[Q(:,2), Q(:,3)]  = ind2sub(size(patchMins), indexP);
Q(:,4) = indexP;

% we could do it this way
% Q1 = zeros(size(P,1), 3);
% Q1(:,1) = P;

numberPatchCenters = 0;
patchCenters = cell(1, 1);
i = 1;
threshold = params.patchThreshold;
radius = params.boxRadius;
Q = Q(Q(:,1) > threshold, :);
n = size(Q,1);
boxWidth = 2*radius+1;

boxOffsetX = floor(params.proxyWidth - size(patchMins,2))/2; 
boxOffsetY = floor(params.proxyHeight - size(patchMins,1))/2; 

rx = double(params.width) / double(params.proxyWidth);
ry = double(params.height) / double(params.proxyHeight);

while (i <= n)
    if (Q(i,1) > threshold)
        % Now zap every index that is near the
        numberPatchCenters = numberPatchCenters + 1;
        r = Q(i,2);
        c = Q(i,3);
        patchCenters{numberPatchCenters} = MtiRect(c-radius + boxOffsetX, r-radius + boxOffsetY, boxWidth, boxWidth);
        patchCenters{numberPatchCenters}.xc = c + boxOffsetX;
        patchCenters{numberPatchCenters}.yc = r + boxOffsetY;
        
        % because start index is 1, to resize, we must do the following
        patchCenters{numberPatchCenters}.xSubpixelCenter = single(round(rx*(double(patchCenters{numberPatchCenters}.xc) - 0.5))) + 1;
        patchCenters{numberPatchCenters}.ySubpixelCenter = single(round(ry*(double(patchCenters{numberPatchCenters}.yc) - 0.5))) + 1;
        
        d = (abs(Q(:,2) - r) <= boxWidth) & (abs(Q(:,3)-c) <= boxWidth);
        Q(d,1) = -1;
% the above two lines can be replaced by the ones below for a visual display
% of how the patches are found.
%         d = (abs(Q(:,2) - r) <= boxWidth) & (abs(Q(:,3)-c) <= boxWidth)  & (Q(:,1) ~= -2);
%         Q(d,1) = -1;
%         d = (abs(Q(:,2) - r) <= radius) & (abs(Q(:,3)-c) <= radius);
%         Q(d,1) = -2;        
%         R = sortrows(Q,4);
%         R = reshape(R(:,1), size(patchMins));
%         imshow(R,[])
%        refresh;
    end
    i = i + 1;
end

patchCenters = cell2mat(patchCenters);

end