function framesInfoProxy = createProxiesAndAverageIntensity(framesInfo, params)
% This takes an cell struc of frameInfo and creates the proxies
framesInfoProxy = cell(size(framesInfo));
for frameNumber = 1:numel(framesInfo)
    frame = framesInfo(frameNumber);
    proxy = imresize(frame.fullGrayImage,[params.proxyHeight params.proxyWidth]);
    %%%%%%%%%%%%%%%%%
    % remove debuggins
%         fileName = strcat('c:\temp\frame_', num2str(frameNumber), '.mat');
%         load(fileName);
%         proxy = proxyBeforeScale;
    %
    [frame.proxyImage, frame.averageIntensity] = balanceIntensity(proxy, params);
    frame.paddedProxyImage = padarray(frame.proxyImage, params.proxyRadiusSearch, 'symmetric', 'both');
    framesInfoProxy{frameNumber} = frame;
end
framesInfoProxy = cell2mat(framesInfoProxy);
end