function [subImage, valid, isFullyOutside] = extractImage(image, rect, reportBoundaryError)

validPre = true;
validPost = true;
isFullyOutside = false;

if nargin == 3
    localReportBoundaryError = reportBoundaryError;
else
    localReportBoundaryError = false;
end;

% The padding necessary
pad = [0,0,0,0];
newSize = [rect.height, rect.width];

if (rect.y < 1)
    pad(1) = -rect.y + 1;
    newSize(1) = newSize(1) - pad(1);
    if (localReportBoundaryError)
        disp(['Box hit top ', num2str(rect.y)]);
    end;
    rect.y = 1;
    validPre = false;
end

if (rect.x < 1)
    pad(2) = -rect.x + 1;
    newSize(2) = newSize(2) - pad(2);
    if (localReportBoundaryError)
        disp(['Box hit left ', num2str(rect.x)]);
    end
    rect.x = 1;
    validPre = false;
end

if (rect.y + rect.height-1 > size(image, 1))
    pad(3) =  newSize(1) -( size(image, 1) - rect.y + 1);
    newSize(1) = newSize(1) - pad(3);
    
    if (localReportBoundaryError)
        disp(['Box hit bottom ', num2str(rect.y + rect.height-1)]);
    end
    validPost = false;
end

if (rect.x + rect.width-1 > size(image, 2))
    pad(4) = newSize(2) - (size(image, 2) - rect.x + 1);
    newSize(2) = newSize(2) - pad(4);
    if (localReportBoundaryError)
        disp(['Box hit right ', num2str(rect.x + rect.width-1)]);
    end
    validPost = false;
end

% test if rectangle is outsize of image, if so just return
% bad and all zeros
if (any(newSize <= 0))
    isFullyOutside = true;
    valid = false;
    subImage = zeros(rect.height, rect.width, size(image, 3));
    return;
end

subImage = image(rect.y : (rect.y + newSize(1)-1), rect.x : (rect.x + newSize(2) -1), :);

if validPre == false
    subImage = padarray(subImage, pad(1:2), 'symmetric', 'pre');
end

if validPost == false
    subImage = padarray(subImage, pad(3:4), 'symmetric', 'post');
end

valid = validPost & validPost;
end


%    rect.height = size(image, 1) + 1 - rect.y ;
%     disp(['Box hit bottom ', num2str(rect.y + rect.height-1)]);
%     rect.y = size(image, 1) + 1 - rect.height ;