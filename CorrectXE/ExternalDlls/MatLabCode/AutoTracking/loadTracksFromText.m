function tracks = loadTracksFromText(fileName)
    %rack data has siz columns, the first is a header 
    % no frames, number of tracks, large image (width, height), proxy (width, height;
    % next are  track no, track start index, (x, y), (proxy x, proxy y)
    trackData = importdata(fileName, ',');    

    totalFrames = trackData(1,1);
    totalTracks = trackData(1,2);
    width = trackData(1,3);
    height = trackData(1,4);
    
    % remove header
    trackData(1,:) = [];
    
    ratio = width/height;
    proxyWidth = 512;
    proxyHeight = round(proxyWidth/ratio);
    
    rw = width/proxyWidth;
    rh = height/proxyHeight;
    
    tracks = cell(1,totalTracks);
    for i = 0:totalTracks-1
        oneTrackData = trackData(trackData(:,1)==i,:);
        tr.startProxyImage = single(zeros(21,21));  % dummy image
        tr.startFrameIndex = oneTrackData(1,2) + 1;
        tr.startFullIamge = single(zeros(169, 169));
        tr.active = true;
        
        trackLength = size(oneTrackData,1);
        boxes = cell(1,trackLength);
        
        % we could do this without a loop but I dont' care
        for bn = 1: trackLength
            xs = oneTrackData(bn,3) + 1;
            ys = oneTrackData(bn,4) + 1;
            xc = oneTrackData(bn,5) + 1; %round(xs/rw);
            yc = oneTrackData(bn,6) + 1; %round(ys/rh);
            box = MtiRect(xc - 10, yc - 10, 21, 21);
            box.xSubpixelCenter = xs;
            box.ySubpixelCenter = ys;
            boxes{bn} = box;
        end
        tr.boxes = cell2mat(boxes);
        tracks{i+1} = tr;
    end
    tracks = cell2mat(tracks);
end
