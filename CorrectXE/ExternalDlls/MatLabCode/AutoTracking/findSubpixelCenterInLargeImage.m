% If track passes through frameIndex image, this finds the motion with
% subpixel accuracy.
function [ySubpixelCenter, xSubpixelCenter] = findSubpixelCenterInLargeImage(framesInfo, track, frameIndex, params)
% compute some ratios
rx = double(params.width) / double(params.proxyWidth);
ry = double(params.height) / double(params.proxyHeight);
boxIndex = frameIndex - track.startFrameIndex + 1;

if (boxIndex == 1)
    xSubpixelCenter = double(rx*(double(track.boxes(1).xc) - 0.5)) + 1;
    ySubpixelCenter = double(ry*(double(track.boxes(1).yc) - 0.5)) + 1;
    return;
end

proxyBox = track.boxes(boxIndex);
fullEndBox = expandBox(proxyBox, [ry, rx]);

% track using larger box but only search in small area
searchRange = round(params.boxRadius);
startFullImage = track.startFullImage;

searchBox = fullEndBox;
searchBox.x = searchBox.x - searchRange;
searchBox.width = searchBox.width + 2*searchRange;
searchBox.y = searchBox.y - searchRange;
searchBox.height = searchBox.height + 2*searchRange;
searchImage = extractImage(framesInfo(frameIndex).fullGrayImage, searchBox) * (0.5/framesInfo(frameIndex).averageIntensity);
[nxc, delta, mx] = validNormXCorr(startFullImage, searchImage);

% compute nxc maximum
nxc = imresize(nxc, 5);

[~, idx] = max(nxc(:));

%     ii = ii + 1;
%     ma(ii) = mx;
% disp([num2str(mx)]);
[r, c] = ind2sub(size(nxc), idx);
r = r - ceil(size(nxc, 1)/2);
c = c - ceil(size(nxc, 2)/2);

xSubpixelCenter = double(fullEndBox.xc) + c / 5.0;
ySubpixelCenter = double(fullEndBox.yc) + r / 5.0;
end
