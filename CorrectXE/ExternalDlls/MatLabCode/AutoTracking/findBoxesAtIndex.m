function findBoxesAtIndex(tracks, frameIndex)
  endTracks = arrayfun(@(x) numel(x.boxes)+x.startFrameIndex - 1, tracks);
%       imshow(framesInfo(frameIndex).proxyImage);
      tracksAtFrameIndex = tracks(([tracks.startFrameIndex] <= frameIndex) & (frameIndex <= endTracks ));
      
      % Show Boxes
      boxesAtIndex = arrayfun(@(t)t.boxes(frameIndex - t.startFrameIndex + 1) , tracksAtFrameIndex);
      showBoxes(boxesAtIndex);
      drawnow;
end