%This builds the parameters needed to generate the autofilter
function params = CreateAutoFilterParams(imageSize)

    % these are for tracking
    params.width = int32(imageSize(2));
    params.height = int32(imageSize(1));
    ratio = imageSize(2)/imageSize(1);
    params.proxyWidth = int32(512);
    params.proxyHeight = int32(params.proxyWidth/ratio);
    params.centerScale = 0.75;
    params.boxRadius = 10;
    params.patchThreshold = 0.02;
    params.proxyRadiusSearch = [15, 15];
    params.trajectoryMinLength = 10;
    params.trajectoryMaxDiscrepancy = 2;
    params.crossNormThreshold = 0.6;
    
    % These are for auto filter
    params.proxyTileBoxRadius = 8;
    params.proxyTileBoxSize = 2*params.proxyTileBoxRadius + 1;
    params.trackCloseness = 0.25;
    params.contrastBins = 100;
end