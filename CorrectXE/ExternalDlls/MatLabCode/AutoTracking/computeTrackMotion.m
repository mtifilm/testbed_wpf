function [activeTracks, validTracks] = computeTrackMotion(framesInfo, frameIndex, activeTracks, params)
%
%  To avoid boundary complications and to use IppArrays, we pad
%proxyImage = framesInfo(frameIndex).proxyImage;
paddedCurrent = framesInfo(frameIndex).paddedProxyImage;
proxyOffsetHorizontal = params.proxyRadiusSearch(2);
boxRadius = params.boxRadius;
proxyOffsetVertical = params.proxyRadiusSearch(1);
ma = zeros(numel(activeTracks), 1);
t = 0;
for trackIndex = 1:numel(activeTracks)
    track = activeTracks(trackIndex);
    
    endBox = track.boxes(end);
    newWidth = endBox.width + 2*proxyOffsetHorizontal;
    newHeight = endBox.height + 2*proxyOffsetVertical;
    
    % move back and then add on padding
    x = endBox.x - proxyOffsetHorizontal + proxyOffsetHorizontal;
    y = endBox.y - proxyOffsetVertical + proxyOffsetVertical;
    
    % because we are padding by proxyRadiusSearch.
    searchBox = MtiRect(x, y, newWidth, newHeight);
    searchImage = extractImage(paddedCurrent, searchBox);
    
    % Compute the valid normed correlation
% old way
%     nxcfull = normxcorr2(track.firstPatchImage,searchImage);
%     nxc = nxcfull((2*boxRadius+1):(2*boxRadius+1+2*proxyOffsetHorizontal), (2*boxRadius+1):(2*boxRadius+1+2*proxyOffsetVertical));

    [~, rc, mf] = validNormXCorr(track.startProxyImage,searchImage);
   ma(trackIndex) = mf;
   
   if (mf < params.crossNormThreshold)
        activeTracks(trackIndex).active = false;
        t = t + 1;
        continue;
   end;
       
    newEndBox = endBox;   
    newEndBox.x = endBox.x + rc(2);
    newEndBox.y = endBox.y + rc(1);
    newEndBox.xc = newEndBox.x + boxRadius;
    newEndBox.yc = newEndBox.y + boxRadius;
    
    % if box goes off edge, stop track
    if (newEndBox.x < 1)
        activeTracks(trackIndex).active = false;
        continue;
    end
    if (newEndBox.y < 1)
        activeTracks(trackIndex).active = false;
        continue;
    end
    if (newEndBox.x > params.proxyWidth - newEndBox.width)
        activeTracks(trackIndex).active = false;
        continue;
    end
    if (newEndBox.y > params.proxyHeight - newEndBox.height)
        activeTracks(trackIndex).active = false;
        continue;
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Track backwards filling in the subpixel tracking
    endBox = track.boxes(1);
%     [endBox.ySubpixelCenter, endBox.xSubpixelCenter] = findSubpixelCenterInLargeImage(framesInfo, activeTracks(trackIndex), frameIndex, params);
%     track.boxes(1) = endBox;
%         
    newWidth = endBox.width + 2*proxyOffsetHorizontal;
    newHeight = endBox.height + 2*proxyOffsetVertical;
    
    %add on and subtrack padding
    x = endBox.x - proxyOffsetHorizontal + proxyOffsetVertical;
    y = endBox.y - proxyOffsetVertical + proxyOffsetVertical;
    searchBox = MtiRect(x, y, newWidth, newHeight);
    paddedTarget = framesInfo(track.startFrameIndex).paddedProxyImage;
    searchImage1 = extractImage(paddedTarget, searchBox);
    newProbeBox = newEndBox;
    
    % adjust for padding
    newProbeBox.x = newProbeBox.x +proxyOffsetHorizontal;
    newProbeBox.y = newProbeBox.y +proxyOffsetVertical;
    
    probeImage = extractImage(paddedCurrent, newProbeBox);
    
% old way
%      nxcfull = normxcorr2(probeImage,searchImage1);
%      nxc = nxcfull(2*boxRadius+1:2*boxRadius+1+2*proxyOffsetHorizontal, 2*boxRadius+1:2*boxRadius+1+2*proxyOffsetVertical);
%     [m, idx] = max(nxc(:));
%     [r, c] = ind2sub(size(nxc), idx);
%     x = endBox.x + c - proxyOffsetHorizontal - 1;
%     y = endBox.y + r - proxyOffsetHorizontal - 1;

    [nxc, rc, mb] = validNormXCorr(probeImage,searchImage1);  

    x = endBox.x + rc(2);
    y = endBox.y + rc(1);
    
    d = (y - endBox.y)^2 + (x - endBox.x)^2;
    d = sqrt(double(d));
%     disp([num2str(trackIndex), ', ', num2str(d)])
    
    if (d > params.trajectoryMaxDiscrepancy)
        activeTracks(trackIndex).active = false;
        continue;
    end
    
    % store next box
    activeTracks(trackIndex).boxes(end + 1) = newEndBox;
    [newEndBox.ySubpixelCenter, newEndBox.xSubpixelCenter] = findSubpixelCenterInLargeImage(framesInfo, activeTracks(trackIndex), frameIndex, params);
    activeTracks(trackIndex).boxes(end) = newEndBox;
 
    % find subpixel value
end

% Display info
%histogram(ma, 10)
disp([num2str(t), ' rejected by norm']);
end