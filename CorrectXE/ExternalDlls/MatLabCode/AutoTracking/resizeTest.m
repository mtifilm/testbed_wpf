function outTracks = resizeTest(framesInfo, tracks, params)

rp = 1.0;

% compute some ratios
rx = size(framesInfo(1).image, 2) / double(params.proxyWidth);
ry = size(framesInfo(1).image, 1) / double(params.proxyHeight);
rw = round(rx*(2*params.boxRadius + 1));
rh = round(ry*(2*params.boxRadius + 1));
iii = 0;
iii2 = 0;
iiit = 0;
outTracks = tracks;

ma = zeros(0);
ii = 0;
for trackNo = 1:numel(tracks)
    track = tracks(trackNo);
    outTrack = track;
    largeBoxes = arrayfun(@(b)expandBox(b, [ry, rx]), track.boxes);
    trackIndex = track.startFrameIndex;
    
    % First box starts on integeral pixel
    largeBox = largeBoxes(1);
    outTrack.boxes(1).xSubpixelCenter = double(largeBox.xc); 
    outTrack.boxes(1).ySubpixelCenter = double(largeBox.yc);
            
    % track using larger box but only search in small area
    searchRange = round(params.boxRadius);
    firstImage = framesInfo(trackIndex).image;
    firstPatchImage = extractImage(firstImage, largeBoxes(1)) * (0.5/framesInfo(trackIndex).averageIntensity);

    for k = 2:numel(largeBoxes)
        largeBox = largeBoxes(k);
        searchBox = largeBox;
        searchBox.x = searchBox.x - searchRange;
        searchBox.width = searchBox.width + 2*searchRange;
        searchBox.y = searchBox.y - searchRange;
        searchBox.height = searchBox.height + 2*searchRange;
        kTrackIndex = trackIndex + k - 1;
        searchImage = extractImage(framesInfo(kTrackIndex).image, searchBox) * (0.5/framesInfo(kTrackIndex).averageIntensity);
        
        [nxc, ~] = validNormXCorr(firstPatchImage, searchImage);

        % compute nxc maximum
        nxc = imresize(nxc, 5);
        
        [mx, idx] = max(nxc(:));
        
        ii = ii + 1;
        ma(ii) = mx;
       % disp([num2str(mx)]);
        [r, c] = ind2sub(size(nxc), idx);
        
        outTrack.boxes(k).xSubpixelCenter = double(largeBox.xc) + c / 5.0 - searchRange; 
        outTrack.boxes(k).ySubpixelCenter = double(largeBox.yc) + r / 5.0 - searchRange; 
    end
    
    outTracks(trackNo) = outTrack;
end
end

        
%         searchImage5 = imresize(searchImage, 5);
%         [~, rcB] = validNormXCorr(firstPatchImage5, searchImage5); 
%         rcB = rcB / 5.0 - 37;
%         
%         dd = max(abs(rcB - rc5));
%         if (dd >= 0.2)
%             if (dd >= 0.3) 
%                 iii2 = iii2 + 1;
%             else
%                  iii = iii + 1;
%             end
%             if (~sameTrack)
%                 iiit = iiit + 1;
%                 sameTrack = true;
%             end
%             
%             disp([num2str(iiit), ', ', num2str(iii2), ', ' , num2str(iii), ', ', num2str(trackNo)])
%         end
