function [prunedTracks, validTracks] = pruneTracks(activeTracks, validTracks, params);
    
prunedTracks = activeTracks([activeTracks.active]);
finishedTracks = activeTracks([activeTracks.active] == false);

for i = 1:numel(finishedTracks)
    track = finishedTracks(i);
    
    % reject any track that is less than min
    if numel(track.boxes) < params.trajectoryMinLength
        continue;
    end
    
    track.active = true;
    % I don't know how to do this in matlab without 
    if (numel(validTracks) == 0)
            validTracks = track;
    else
            validTracks(end+1) = track;        
    end

end
end   