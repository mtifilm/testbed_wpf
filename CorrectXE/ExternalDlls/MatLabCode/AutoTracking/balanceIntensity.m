function [balancedImage, ai] = balanceIntensity(image, params)
    cw = int32(params.proxyWidth * params.centerScale);
    ch = int32(params.proxyHeight * params.centerScale);
    rect = MtiRect(int32((params.proxyWidth - cw)/2)+1, int32((params.proxyHeight - ch)/2)+1, cw, ch);
    center = extractImage(image, rect);
    ai = sum(sum(center)) / numel(center);
    if ai == 0
        ai = 0.5;
    end
    
    balancedImage = image * (0.5/ai);
end