function [framesInfo, params] = ReadGrayScaleImagesMat( filePath, maxFrames )
    %   Read the mat files from the path
    %  images are cells containing a structure frameInfo
    %  this is added to at later dates, works for Matlab not C++
    fileList = dir(strcat(filePath, '\*.mat'));
    
    if (numel(fileList) == 0)
        error('Error:  No files found')
    end
    
    % for debugging
    totalFrames = min(numel(fileList), maxFrames);
    framesInfo = cell(totalFrames,1);
    disp('starting');
    for i = 1:totalFrames
        
        fprintf_r('Frame: %d', i);
      
        f = fileList(i);
        name = f.name;
        fileName = strcat(filePath, name);
        load(fileName);
        if (size(dpxImage, 3) == 4)
            dpxImage = dpxImage(:,:,1:3);    
        end
        frameInfo.fullRgbImage = single(dpxImage) / 65535.0;
        
        %% This code only works with RGB images, a grayscale is saved
        if (size(dpxImage, 3) == 1)
            frameInfo.fullGrayImage = frameInfo.fullRgbImage;
            frameInfo.fullRgbImage = cat(3, frameInfo.fullGrayImage, frameInfo.fullGrayImage, frameInfo.fullGrayImage);       
        else
            frameInfo.fullGrayImage = rgb2gray(frameInfo.fullRgbImage);
        end
        
        framesInfo(i) = {frameInfo};
    end

    image0 = framesInfo{1}.fullGrayImage;
    params = CreateAutoFilterParams(size(image0));
    
    framesInfo = cell2mat(framesInfo);
    disp(' ');
end