function [validTracks, params] = ProcessAllTracks(framesInfo, params)

    %%%%%%%%%%%%%%%%%%%%%%%%
 patchMins = FindPatchMins(framesInfo(1).proxyImage, params);
% for debugging from C data
 % patchMins = loadPatchMins(1);

patchCenters = findPatches(patchMins, params);
disp(['patchCenters found ', int2str(numel(patchCenters))])

% Initialize the tracks
startActiveTracks = makeStartTracks(framesInfo, patchCenters, 1, params);
activeTracks = startActiveTracks;

validTracks = struct.empty;
for frameIndex = 2:numel(framesInfo)
    display(['Frame: ', num2str(frameIndex)]);
    activeTracks = computeTrackMotion(framesInfo, frameIndex, activeTracks, params);
    imshow(framesInfo(frameIndex).proxyImage);
    
    for i = 1:numel(activeTracks)
        a = activeTracks(i);
        if (a.active)
            if numel(a.boxes) < params.trajectoryMinLength
                showBoxes(a.boxes(end), 'black');
            else
                showBoxes(a.boxes(end));
            end
        end
    end
    
    [activeTracks, validTracks] = pruneTracks(activeTracks, validTracks, params);
    
    % We don't need to do patches when close to end
    %
    if (numel(framesInfo) - frameIndex) >= params.trajectoryMinLength
%        patchMins = FindPatchMins(framesInfo(frameIndex).proxyImage, params);
         patchMins = loadPatchMins(frameIndex);
        patchCenters = findPatches(patchMins, params);
        disp(['patchCenters found ', int2str(numel(patchCenters))])
        newDisjointPatches = extractDisjointPatches(activeTracks, patchCenters, params);
        
        showBoxes(newDisjointPatches, 'y');
        newActiveTracks = makeStartTracks(framesInfo, newDisjointPatches, frameIndex, params);
        activeTracks(end+1:end + numel(newActiveTracks)) = newActiveTracks;
    end
    
    drawnow;
end;

% done, add any remaining tracks
% notice, because we have added NO patches within the min, the
% active tracks will have been pruned
validTracks(end+1:end+numel(activeTracks)) = activeTracks;
plotTracks(validTracks, 1)
end