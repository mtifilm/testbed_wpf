%% This weird function creates a set of tile boxes on the proxy with
%% proxyNominalSize that cover the image.It also returns boxes on the
%% full image that correspond but do not overlap.
function fullSizeBoxes = createFullSizeTiles(proxyBoxes, params)
    imageWidth = params.width;
    imageHeight = params.height;
    proxyWidth = params.proxyWidth;
    proxyHeight = params.proxyHeight;
    
    boxNominalSize = 2 * params.proxyTileBoxRadius + 1;
    
    % this must compute to the proxy sizes
    nx = int32(ceil(double(proxyWidth) / boxNominalSize));
    ny = int32(ceil(double(proxyHeight) / boxNominalSize));
    
    scaleX = double(imageWidth) / double(proxyWidth);
    scaleY = double(imageHeight) / double(proxyHeight);
    
    % We should have tiled the centers here and then gone back DUH!
    % Translate the centers to the big images
    cx = zeros(nx+1, 1);
    cx(1) = 1;
    
    for i = 2:nx
        cx(i) = int32(round(proxyBoxes(1,i-1).xc + proxyBoxes(1,i).xc - 2) / 2 * scaleX) + 1;
    end
    
    cx(nx+1) = imageWidth + 1;
    
    % do the y's
    cy = zeros(ny+1, 1);
    cy(1) = 1;
    
    for i = 2:ny
        cy(i) = int32(round(proxyBoxes(i-1,1).yc + proxyBoxes(i,1).yc - 2) / 2 * scaleY) + 1;
    end
    
    cy(ny+1) = imageHeight + 1;
    
    % Create the widths, what we are really interested in
    cw = zeros(nx,1);
    for i = 1:nx
        cw(i) = cx(i + 1) - cx(i);
    end
    
    ch = zeros(ny,1);
    for i = 1:ny
        ch(i) = cy(i + 1) - cy(i);
    end
    
    fullSizeBoxes = cell(ny, nx);
    for r = 1:ny
        for c = 1:nx
            bigBox = MtiRect(cx(c), cy(r), cw(c), ch(r));
            centerX = proxyBoxes(r, c).xc;
            centerY = proxyBoxes(r, c).yc;
            bigBox.xc = floor((centerX - 1) * scaleX + 1);
            bigBox.yc = floor((centerY - 1) * scaleY + 1);

            fullSizeBoxes{r, c} = bigBox;
        end
    end
    
    
    fullSizeBoxes = cell2mat(fullSizeBoxes);
end
