function trackBlocks = findProxyMotionBlocks(framesInfo, frameIndex, validTracks, params)
    % This creates blocks and assigns tracks to them, blocks
    %
    % The line below needs only to be done once, but since we don't have private
    % variables in matlab, and this is cheap, do it all the time
    % [fullImageBoxes, proxyBoxes] = createTilesFromFrameInfo(framesInfo, params.proxyTileBoxSize);
    proxyBoxes = createEqualSizeTiles(size(framesInfo(1).proxyImage), params.proxyTileBoxSize);
    fullSizeBoxes = createFullSizeTiles(proxyBoxes, params); 
    % build a dummy track, this one is entire shot with zero motion
    % We don't care about the start images as they are never used here
    
    % this is matlab's poor mans constructor
    % we only care the boxes have 0 movement
    dummyTrack = validTracks(1);
    d = validTracks(1).boxes(1);
    dummyBox = MtiRect(-100, -100, d.width, d.height);
    dummyTrack.boxes = repmat(dummyBox, numel(framesInfo), 1); % arrayfun(@(x)dummyBox, [1:numel(framesInfo)]);
    dummyTrack.startFrameIndex = 1;
    dummyTrack.active = true;
    
    endTracks = arrayfun(@(x) numel(x.boxes)+x.startFrameIndex - 1, validTracks);
    selectedTracksThroughFrameIndex = validTracks(([validTracks.startFrameIndex] <= frameIndex) & (frameIndex <= endTracks ));
    
    proxyBaseImage = framesInfo(frameIndex).proxyImage;
    
    nx = size(proxyBoxes, 2);
    ny = size(proxyBoxes, 1);
    closenessDistance = params.proxyWidth * params.trackCloseness;
    
    debugNumberNoMotion = 0;
    debugNumberEdgeBoxes = 0;
    debugTotalTests = 0;
    
    for r = 1:ny
        for c = 1:nx
            currentBox = proxyBoxes(r, c);
            currentBoxImage = extractImage(proxyBaseImage, currentBox);
            variableTracks = selectedTracksThroughFrameIndex(                                        ...
                arrayfun(@(x)boxDistance(x.boxes(frameIndex - x.startFrameIndex + 1),currentBox), ...
                selectedTracksThroughFrameIndex)                                               ...
                < closenessDistance);
            
            % add the dummy track
            closeTracks = dummyTrack;
            closeTracks(1, 2:numel(variableTracks)+1) = variableTracks;
            
            % find the min
            minL2 = 10000;
            minTrack = [];
            for track = closeTracks;
                % find the next 4 frame indexes
                % ugly, must be a better way
                testDeltas = rollingIndex(track, frameIndex);
                l2 = 0;
                numberOfValidCompares = 0;
                for i = 1:4
                    delta = testDeltas(i);
                    nextBox = currentBox;
                    
                    debugTotalTests = debugTotalTests + 1;
                    % delta(2:3) is proxy motion delta
                    nextBox.x = currentBox.x + delta.proxyDeltaX;
                    nextBox.y = currentBox.y + delta.proxyDeltaY;
                    
                    % delta(1) is frame number
                    [deltaBoxImage, ~, isFullyOutside] = extractImage(framesInfo(delta.frameIndex).proxyImage, nextBox);
                    if (isFullyOutside == false)
                        di = currentBoxImage - deltaBoxImage;
                        l2 = l2 + norm(di(:));
                        numberOfValidCompares = numberOfValidCompares + 1;
                    else
                        debugNumberEdgeBoxes = debugNumberEdgeBoxes + 1;
                        break;
                    end
                end
                
                % numberOfValidCompares cannot be zero as the 0-th track will
                % always work
                
                l2 = l2 / numberOfValidCompares;
                if (c == 15 && r == 12)
                    disp([num2str(l2), ', (', num2str(testDeltas(1).proxyDeltaX), ',' ,num2str(testDeltas(1).proxyDeltaY),')']);                end
                if (numberOfValidCompares ~= 4)
                     disp(['rejected by boundary ',num2str(numberOfValidCompares)]);
                end
                if (l2 <= minL2) && (numberOfValidCompares == 4)
                    minTrack = track;
                    minL2 = l2;
                    minTrack.deltas = testDeltas;
                end
            end
            
            % dump debug info
            %disp(['(c,r):(x,y) ', num2str(c-1),',',num2str(r-1),' : ', num2str(minTrack.boxes(1).xc),', ', num2str(minTrack.boxes(1).yc)])
            
            proxyBoxes(r, c).track = minTrack;
            proxyBoxes(r, c).fullImageBox = fullSizeBoxes(r,c);
            if (minTrack.boxes(1).x == -100)
                debugNumberNoMotion = debugNumberNoMotion + 1;
            else
            end
        end
    end
    
    display(['Total Blocks ', num2str(nx*ny), ', no motion ', num2str(debugNumberNoMotion),', over edge ',  num2str(debugNumberEdgeBoxes),', of ', num2str(debugTotalTests)]);
    trackBlocks = proxyBoxes;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%

function testDeltas = rollingIndex(track, frameIndex)
    %
    %  A delta is 5 structs, each struct has the frame and movements for the
    %  proxy image and subpixel accuract for the full image.
    
    startFrameIndex = track.startFrameIndex;
    
    testIndexes = [frameIndex-2, frameIndex - 1, frameIndex + 1, frameIndex + 2];
    if (frameIndex - 2) < startFrameIndex
        if (frameIndex - 1) < startFrameIndex
            testIndexes = (frameIndex + 1):(frameIndex+4);
        else
            testIndexes(1) = frameIndex - 1;
            testIndexes(2:4) = [(frameIndex + 1):(frameIndex+3)];
        end
    end
    
    endTrackIndex = numel(track.boxes)+track.startFrameIndex-1;
    if (frameIndex + 2) > endTrackIndex
        if (frameIndex + 1) > endTrackIndex
            testIndexes = (frameIndex - 4):(frameIndex - 1);
        else
            testIndexes(4) = frameIndex + 1;
            testIndexes(1:3) = (frameIndex - 3):(frameIndex - 1);
        end
    end
    
    % now find the deltas
    startBoxIndex = frameIndex - startFrameIndex + 1;
    
    delta.frameIndex = 0;
    delta.proxyDeltaX = int32(0);
    delta.proxyDeltaY = int32(0);
    delta.fullDeltaX = 0.0;
    delta.fullDeltaY = 0.0;
    
    testDeltas = repmat(delta,4,1);
    
    for i = 1:numel(testIndexes)
        delta.frameIndex = testIndexes(i);
        
        % di is delta of the frameS
        di =  delta.frameIndex - frameIndex;
        delta.proxyDeltaX = track.boxes(di + startBoxIndex).x - track.boxes(startBoxIndex).x;
        delta.proxyDeltaY = track.boxes(di + startBoxIndex).y - track.boxes(startBoxIndex).y;
        
        delta.fullDeltaX = track.boxes(di + startBoxIndex).xSubpixelCenter - track.boxes(startBoxIndex).xSubpixelCenter;
        delta.fullDeltaY = track.boxes(di + startBoxIndex).ySubpixelCenter - track.boxes(startBoxIndex).ySubpixelCenter;
        
        testDeltas(i) = delta;
    end
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%

function d = boxDistance(box1, box2)
    d = max(abs(box1.xc - box2.xc), abs(box1.yc - box2.yc));
end