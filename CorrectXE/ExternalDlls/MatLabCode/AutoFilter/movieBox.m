function movieBox(framesInfo, tracks, tracks2)
    secondTracks = exist('tracks2','var');
    
    endTracks = arrayfun(@(x) numel(x.boxes)+x.startFrameIndex - 1, tracks);
    if (secondTracks)
        endTracks2 = arrayfun(@(x) numel(x.boxes)+x.startFrameIndex - 1, tracks2);
    end
    
    for frameIndex = 1:numel(framesInfo)
        imshow(framesInfo(frameIndex).proxyImage)
        tracksAtFrameIndex = tracks(([tracks.startFrameIndex] <= frameIndex) & (frameIndex <= endTracks ));
        % Show Boxes
        boxesAtIndex = arrayfun(@(t)t.boxes(frameIndex - t.startFrameIndex + 1) , tracksAtFrameIndex);
        showBoxes(boxesAtIndex);
        drawnow;
        pause(0.5); 
        % process second track
        if (secondTracks)
            tracksAtFrameIndex = tracks2(([tracks2.startFrameIndex] <= frameIndex) & (frameIndex <= endTracks2 ));
            boxesAtIndex = arrayfun(@(t)t.boxes(frameIndex - t.startFrameIndex + 1) , tracksAtFrameIndex);
            showBoxes(boxesAtIndex, 'yellow');
        end
        
        drawnow;
        pause(0.5);
    end
end