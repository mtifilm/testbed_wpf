% this creates an array of MtiRects
% that form a disjoint set of rectangles.  
function boxes = createDisjointTiles(framesInfo, boxNominalSize, params)

imageSize = size(framesInfo(1).image);
proxySize = size(framesInfo(1).proxyImage);
scales = imageSize ./ proxySize;
bs = round(boxNominalSize*scales);

%matlab starts at 1
ulx = 1;
uly = 1;
width = imageSize(2);
height = imageSize(1);

nx = floor(width / bs(2));
ny = floor(height/ bs(1));
swidth = width / nx;
sheight = height / ny;
boxes = cell(ny, nx);

% We could do this using a few lines of matlab code but
% will be needing to translating to C++
xc = ulx;
yc = uly;
for r = 1:ny
    xc = ulx;
    y = uly + round(sheight * r);
    h = y - yc;
    for c = 1:nx
        x = ulx + round(swidth * c);
        w = x - xc;
        boxes{(c-1)*ny + r} = MtiRect(xc, yc, w, h);
        xc = x;
    end
    
    yc = y;
end

boxes = cell2mat(boxes);

end