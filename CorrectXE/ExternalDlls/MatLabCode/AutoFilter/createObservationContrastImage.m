function [scaledContrastImage, observedFidelity] = createObservationContrastImage(framesInfo, frameIndex, detectionImages, params)
   
    % sort M, we only need indexes
    M = detectionImages(:,:,3);
    OC = framesInfo(frameIndex).fullGrayImage - M;
    [~, index] = sort(M(:));
   
    sortedOC = OC(:);
    sortedOC = sortedOC(index);
    
    xInc = double(numel(OC))/params.contrastBins;
    intervals = [round(xInc * [0:(params.contrastBins)]) + 1, numel(OC)];
    bins = arrayfun(@(i)sortedOC(intervals(i):intervals(i+1)-1), [1:params.contrastBins], 'UniformOutput', false);
    bins = cellfun(@(bin)sort(bin), bins, 'UniformOutput', false);
    binEdges = cellfun(@(bin)bin([1, numel(bin)]), bins, 'UniformOutput', false);
    binEdges = cell2mat(binEdges);
    
    binValues = cellfun(@(bin)bin([round(0.2*numel(bin)), round(0.8*numel(bin))]), bins, 'UniformOutput', false);
    binValues = cell2mat(binValues);
    diff = binValues(2,:) - binValues(1,:);
    
    SC = zeros(size(OC));
    for i = 1:numel(bins)
        if (diff(i) > 0)
            v = (OC >= binEdges(1,i)) & (OC < binEdges(2,i));         
            SC(v) = OC(v) / diff(i);
        end;
    end
    
    OF1 = abs(detectionImages(:,:,5) - detectionImages(:,:,3));
    OF2 = abs(detectionImages(:,:,4) - detectionImages(:,:,2));
    OF3 = abs(detectionImages(:,:,3) - detectionImages(:,:,1));    
    observedFidelity = min(OF1, OF2);
    observedFidelity = min(observedFidelity, OF3);
    
    scaledContrastImage = SC;    
end