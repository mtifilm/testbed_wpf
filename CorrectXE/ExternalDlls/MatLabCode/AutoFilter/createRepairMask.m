function repairMask = createRepairMask(detectionMask)
    
    % Find the boundary of the debris
    DM = detectionMask;
    repairMask = double(DM);
    
    disk = strel( 'disk', 4);
    boundary = imdilate(DM,disk)-DM;
    
    height = size(DM,1);
    width = size(DM,2);
    
    subs =[-3*height, ...
        -2*height-1, -2*height, -2*height+1, ...
        -height-2,   -height-1, -height, -height+1, -height+2, ...
        -3, -2, -1, 0, 1, 2, 3, ...
        height-2,   height-1, height, height+1, height+2, ...
        2*height-1, 2*height, 2*height+1, ...
        3*height];
    
    sq5 = sqrt(5);
    sq2 = sqrt(2);
    
    weights = [3, ...
        sq5, 2, sq5, ...
        sq5, sq2, 1, sq2, sq5, ...
        3, 2, 1, 0, 1, 2, 3, ...
        sq5, sq2, 1, sq2, sq5, ...
        sq5, 2, sq5, ...
        3]/4;
    weights = 1 - weights;
    
    w = zeros(7,7);
      
    boundaryIndex = find(boundary > 0);
    [R, C] =  ind2sub(size(DM), boundaryIndex); 
    RC = [R, C];
    
    n = numel(R);
    v = (R > 3 & R < (height-3));
    v = (C > 3 & C < (width-3)) & v;
    
    boundaryIndex = boundaryIndex(v);
    n = numel(boundaryIndex);
    
    for (i = 1:n)
        newSubs = boundaryIndex(i) + subs;
        m = max(DM(newSubs) .* weights);
        repairMask(boundaryIndex(i)) = m;
    end
end