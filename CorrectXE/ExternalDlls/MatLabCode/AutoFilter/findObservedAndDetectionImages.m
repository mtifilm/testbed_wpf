function [observedImage, detectionImages] = findObservedAndDetectionImages(framesInfo, frameIndex, validTracks, params)
    
    % The trackblocks decompose the full image into smaller blocks and
    % assigns a track to each box.
    trackBlocks = findProxyMotionBlocks(framesInfo, frameIndex, validTracks, params);
    disp('Done with tracks to blocks');
    frameInfo = framesInfo(frameIndex);
    averageIntensity = frameInfo.averageIntensity;
    
    % Build the 5 observations here
    % The first is the current frame, the other 4 filled in later
    sz = size(frameInfo.fullRgbImage);
    detectionImages = zeros(sz(1), sz(2), 5);
    %observedImages = zeros(sz(1), sz(2), 3, 5);
    observedImage = zeros(sz(1), sz(2), 3);
    for trackBoxIndex = 1 : numel(trackBlocks)
        trackBlock = trackBlocks(trackBoxIndex);
        track = trackBlock.track;
        fullImageBox = trackBlock.fullImageBox;
        
        % We build the observation images for this group.
        % Note: we don't care about the RGB to Gray cost here
        % In c++ code we need to store them.
        di = zeros(fullImageBox.height, fullImageBox.width, 5);
        di(:,:, 1) = extractImage(framesInfo(frameIndex).fullGrayImage, fullImageBox);
        
        rgbi = zeros(fullImageBox.height, fullImageBox.width, 3, 5);
        rgbi(:,:, :, 1) = extractImage(framesInfo(frameIndex).fullRgbImage, fullImageBox);
        deltas = track.deltas;
        
        for i = 1:numel(deltas)
            delta = deltas(i);
            factor = averageIntensity / framesInfo(delta.frameIndex).averageIntensity;
            fullTestBox = fullImageBox;
            fullTestBox.x = round(fullTestBox.x + delta.fullDeltaX);
            fullTestBox.y = round(fullTestBox.y + delta.fullDeltaY);
            
            di(:,:, i+1) =  factor * extractImage(framesInfo(delta.frameIndex).fullGrayImage, fullTestBox);
            rgbi(:,:, :, i+1) =  factor * extractImage(framesInfo(delta.frameIndex).fullRgbImage, fullTestBox);
        end
        
        diSorted = sort(di(:,:,:), 3);
        
        fib = fullImageBox;
        for k=1:5
            detectionImages(fib.y : (fib.y+fib.height-1), fib.x : (fib.x+fib.width-1), k) = diSorted(:,:, k);
        end
        
        % create the observed image
        [~,indx] = sort(di(:,:,[2,3,4,5]), 3);
        
        m = zeros(fib.height, fib.width, 3);
        randInt = randi(2, fib.height, fib.width) + 1;
        for c = 1:fib.width
            for r = 1:fib.height
%               m(r, c, :) = (rgbi(r, c, :,  indx(:,:,2)+1) + rgbi(r, c, :,  indx(:,:,3))+1))/2;
                m(r, c, :) = rgbi(r, c, :,  indx(r,c,randInt(r,c))+1);
            end
        end
        
        observedImage(fib.y : (fib.y+fib.height-1), fib.x : (fib.x+fib.width-1), 1:3) = m;
    end
end


