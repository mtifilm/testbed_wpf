% this creates an array of rectangle structs
% NOTE: rows, cols are row first, not matlab col first
function [boxes] = createEqualSizeTiles(sz, boxNominalSize)
ulx = 1;
uly = 1;
width = sz(2);
height = sz(1);
nx = ceil(width / boxNominalSize);
ny = ceil(height / boxNominalSize);
swidth = width / nx;
sheight = height / ny;

boxes = cell(ny, nx);

for r = 1:ny
    y = uly + round(sheight * (r-1));
    for c = 1:nx
        x = ulx + round(swidth * (c-1));
        boxes{r, c} = MtiRect(x, y, boxNominalSize, boxNominalSize);
    end
end

boxes = cell2mat(boxes);
end