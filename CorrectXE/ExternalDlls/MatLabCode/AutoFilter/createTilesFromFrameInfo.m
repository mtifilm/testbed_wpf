% This weird function creates a set of tile boxes on the proxy with
% proxyNominalSize that cover the image.  It also returns boxes on the
% full image that correspond but do not overlap.
function [imageBoxes, proxyBoxes] = createTilesFromFrameInfo(framesInfo, proxyBoxNominalSize)

proxyBoxes =  createEqualSizeTiles(size(framesInfo(1).proxyImage), proxyBoxNominalSize);
imageSize = size(framesInfo(1).fullGrayImage);
proxySize = size(framesInfo(1).proxyImage);
scales = imageSize ./ proxySize;

%matlab starts at 1
width = imageSize(2);
height = imageSize(1);

% We could do this using a few lines of matlab code but
% will be needing to translating to C++
nx = size(proxyBoxes,2);
ny = size(proxyBoxes,1);
centerPoints = cell(ny, nx);

% Translate centers into full image, these are not true boxes
% but just (x,y) of the centers
for r = 1:ny
    for c = 1:nx
        centerBox = proxyBoxes(r, c);
        xc = floor((centerBox.xc-1) * scales(2))+1;
        yc = floor((centerBox.yc-1) * scales(1))+1;
        centerPoints{r, c} = MtiRect(xc, yc, 0, 0);
    end
end

centerPoints = cell2mat(centerPoints);

% 

xcells = cell(ny, nx);

y = 1;
for r = 1:ny-1
    centerBox = centerPoints(r, 1);
    xc0 = centerBox.x;
    yc0 = centerBox.y;
    h = centerPoints(r + 1, 1).y - yc0;
    x = 1;
    for c = 1:nx-1
        centerBox = centerPoints(r, c+1);
        w = centerBox.x - xc0;
        xcells{r, c} = MtiRect(x, y, w, h);
        x = x + w;
        xc0 = centerBox.x;
    end
    
    % add last cell
    xcells{r, nx} = MtiRect(x, y, width - x, h);
    
    y = y + h;
end

% Copy last row
    yc0 = xcells{ny-1, 1}.y + xcells{ny-1, 1}.height;
    h = height - yc0;
    for c = 1:nx
        % all x values the same
        w = xcells{1, c};
        xcells{ny, c} = MtiRect(w.x, y, w.width, h);
    end

imageBoxes = cell2mat(xcells);

end