function repairedImage = repairFrameAtIndex(framesInfo, frameIndex, observedImage, repairMask, params)
    repairedImage = framesInfo(frameIndex).fullRgbImage;
    repairedImage(:, :, 1) = observedImage(:,:,1) .* repairMask + repairedImage(:,:,1) .*(1 - repairMask);
    repairedImage(:, :, 2) = observedImage(:,:,2) .* repairMask + repairedImage(:,:,2) .*(1 - repairMask);
    repairedImage(:, :, 3) = observedImage(:,:,3) .* repairMask + repairedImage(:,:,3) .*(1 - repairMask);
end
   