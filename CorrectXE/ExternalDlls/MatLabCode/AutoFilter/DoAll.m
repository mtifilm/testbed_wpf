% Compute motiom

%[framesInfo, params] = readFramesInfo('D:\Temp\ShineTests\bw_plane\', 30);
%[framesInfo, params] = readFramesInfo('D:\Temp\ShineTests\bad_fix\', 30);

% [validTracks, params] = ProcessAllTracks(framesInfo, params);
%
% Loop for frame
frameIndex = 6;
%
% Create observed images at frameIndex
[observedImage, detectionImages] = findObservedAndDetectionImages(framesInfo, frameIndex, validTracks, params);
[scaledContrastImage, observedFidelity] = createObservationContrastImage(framesInfo, frameIndex, detectionImages, params);

hc = 4.1;
lc = -4.1;
f = 0.05;

[detectionMask, clusters, clusterIndexes] = createDetectionMask(hc, lc, f, scaledContrastImage, observedFidelity, params);
repairMask = createRepairMask(detectionMask);
repairedImage = repairFrameAtIndex(framesInfo, frameIndex, observedImage, repairMask, params);