image6 = double(framesInfo(6).fullRgbImage);
ycbcr6 = rgb2ycbcr(image6);
ycbcrObserved = rgb2ycbcr(double(observed1C));

for i = clusterIndexes
    fixedPixelIndices = clusters.PixelIdxList{i};
    if (numel(fixedPixelIndices) == 2493)
       image = zeros(size(image6,1), size(image6,2));
       image(fixedPixelIndices) = 1;
       fixData = ycbcrObserved(fixedPixelIndices);
       averageFix = sum(fixData(:)) / numel(fixData);
       DM = image;
       disk = strel( 'disk', 4);
       boundary = imdilate(DM,disk)-DM;
       boundaryData = ycbcr6(boundary > 0);
       averageBoundary = sum(boundaryData(:)) / numel(boundaryData);
       for i = 1:numel(fixedPixelIndices)
           [r,c] = ind2sub(size(image), fixedPixelIndices(i));
           ycbcr6(r,c, 1) = ycbcrObserved(r, c, 1);% * averageBoundary/averageFix;
           image6(r,c,:) = observed1C(r, c, :);
       end
%        image6 = ycbcr2rgb(ycbcr6);
              imshow(image6);
    end
end   

