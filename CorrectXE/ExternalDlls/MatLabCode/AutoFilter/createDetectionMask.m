function [detectionMask, clusters, clusterIndexes] = createDetectionMask(hc, lc, f, scaledContrastImage, observedFidelity, params)
    OF = observedFidelity;
    SC = scaledContrastImage;
    fidelityMask0 = double(OF > f);
    fidelityMask1 = imerode(fidelityMask0,strel( 'disk', 2 ));
    fidelityMask = imdilate(fidelityMask1,strel( 'disk', 10 )); 
    
    defectMask0 = ((SC > (hc/2)) | (SC < (lc/2))) & (fidelityMask == 0);
    
    %defectMask1 = imdilate(defectMask0,strel( 'disk', 2 )); 
    clusters = bwconncomp(defectMask0);
    DM = zeros(size(defectMask0));
    clusterIndexes = [];
    for i = 1:clusters.NumObjects
       pixelList = clusters.PixelIdxList{i};
       C = SC(pixelList);
       P = C(C > 0);
       N = C(C < 0);
       if (sum(P) > hc)
           DM(pixelList) = 1;   
           clusterIndexes(end+1) = i; 
       elseif (sum(N) < lc)
           DM(pixelList) = 1;
           clusterIndexes(end+1) = i; 
       end
    end

    detectionMask = DM;
end