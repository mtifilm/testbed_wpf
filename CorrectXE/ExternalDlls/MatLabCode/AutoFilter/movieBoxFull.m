function movieBoxFull(framesInfo, tracks, tracks2)
    firstTracks = exist('tracks','var');
    secondTracks = exist('tracks2','var');
    
    if (firstTracks)
       endTracks = arrayfun(@(x) numel(x.boxes)+x.startFrameIndex - 1, tracks);
       rx  = double(size(framesInfo(1).fullRgbImage, 2))/double(size(framesInfo(1).proxyImage, 2));
       ry  = double(size(framesInfo(1).fullRgbImage, 1))/double(size(framesInfo(1).proxyImage, 1));
    end
    
    if (secondTracks)
        endTracks2 = arrayfun(@(x) numel(x.boxes)+x.startFrameIndex - 1, tracks2);
    end
    
    for frameIndex = 1:numel(framesInfo)
        imshow(framesInfo(frameIndex).fullRgbImage)
        
        if (firstTracks)
            tracksAtFrameIndex = tracks(([tracks.startFrameIndex] <= frameIndex) & (frameIndex <= endTracks ));
            % Show Boxes
            boxesAtIndex = arrayfun(@(t)t.boxes(frameIndex - t.startFrameIndex + 1) , tracksAtFrameIndex);
            % transform to full image
            for i = 1:numel(boxesAtIndex)
                boxesAtIndex(i) = inflateBoxAroundFullCenter(boxesAtIndex(i), rx, ry);
            end
            showBoxes(boxesAtIndex);
            drawnow;
        end
        
        pause(0.5);
        % process second track
        if (secondTracks)
            tracksAtFrameIndex = tracks2(([tracks2.startFrameIndex] <= frameIndex) & (frameIndex <= endTracks2 ));
            boxesAtIndex2 = arrayfun(@(t)t.boxes(frameIndex - t.startFrameIndex + 1) , tracksAtFrameIndex);
            % transform to full image
            for i = 1:numel(boxesAtIndex2)
                boxesAtIndex2(i) = inflateBoxAroundFullCenter(boxesAtIndex2(i), rx, ry);
            end
            showBoxes(boxesAtIndex2, 'yellow');
        end
        
        drawnow;
        pause(0.5);
    end
end

% this isn't exactly correct but good enough for display as error is consistent
function newBox = inflateBoxAroundFullCenter(box, rx, ry)
    newBox = box;
    newBox.width = round(box.width * rx);
    newBox.height = round(box.height * ry);
    w2 = newBox.width/2;
    h2 = newBox.height/2;
    newBox.x = round(box.xSubpixelCenter - w2);
    newBox.y = round(box.ySubpixelCenter - h2);
end