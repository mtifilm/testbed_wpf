#include "HRTimer.h"
#include <iomanip>

CHRTimer::CHRTimer()
{
	start();
}

void CHRTimer::start()
{
	_started = true;
	_start = std::chrono::system_clock::now();
	_intervalStart = _start;
	_end = _start;
}

void CHRTimer::stop()
{
	_started = false;
	_end = std::chrono::system_clock::now();
}

double CHRTimer::elapsedMilliseconds()
{
	auto end = _end;
	if (_started)
	{
		end = std::chrono::system_clock::now();
	}

	return std::chrono::duration_cast<std::chrono::microseconds>(end - _start).count() / 1000.0;
}


double CHRTimer::intervalMilliseconds()
{
	auto end = std::chrono::system_clock::now();
	auto start = _intervalStart;
	_intervalStart = end;

	return std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() / 1000.0;
}

std::ostream & operator<<(std::ostream & os, CHRTimer & hrt)
{
	os << std::fixed << std::setprecision(3) << hrt.intervalMilliseconds();
	return os;
}

std::wostream & operator<<(std::wostream & os, CHRTimer & hrt)
{
	os << std::fixed << std::setprecision(3) << hrt.intervalMilliseconds();
	return os;
}

