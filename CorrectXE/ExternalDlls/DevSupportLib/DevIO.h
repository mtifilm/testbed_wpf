#pragma once
#include <vector>
#include <time.h>
#include <sstream>
#include <iostream>
#include <string>
#include "Ippheaders.h"

namespace DevIO
{
	//void dumpMatlabBinary(const Ipp16uArray &inputArray, int32_t bits, const std::string fileName);

	//Ipp16uArray readMatlabBinary(const string &fileName, bool normalize = true);
	void *getPinnedMemory(size_t bytes);

	std::pair<Ipp16uArray, int> readDpxFile(const string &fileName, bool normalize = true, int imageIndex=0);

   // This writes a basic DPX file.  
   void writeDpxFile(const string &fileName, const Ipp16uArray &data, bool normalize = true, int imageIndex = 0);
}

