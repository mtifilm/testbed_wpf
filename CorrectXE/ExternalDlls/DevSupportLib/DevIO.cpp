#include "DevIO.h"
#include <exception>
#include "cuda_runtime.h"
#include <assert.h>
#include "dpx/dpx.h"

cudaError_t checkCuda(cudaError_t result)
{
#if defined(DEBUG) || defined(_DEBUG)
	if (result != cudaSuccess) {
		fprintf(stderr, "CUDA Runtime Error: %s\n",
			cudaGetErrorString(result));
		assert(result == cudaSuccess);
	}
#endif
	return result;
}

void *DevIO::getPinnedMemory(size_t bytes)
{

	void *h_aPinned = nullptr;

	// allocate and initialize
	checkCuda(cudaMallocHost(&h_aPinned, bytes)); // host pinned

	return h_aPinned;

	//// Prepare for locking.
	//DWORD pid = GetCurrentProcessId();
	//HANDLE hProcess = OpenProcess(PROCESS_SET_QUOTA, FALSE, pid);
	//if (hProcess == NULL)
	//	throw std::runtime_error("Failed to open process");

	//size_t requestInBytes = 2 * 512 * 1024 * 1024;
	//BOOL result = SetProcessWorkingSetSize(hProcess,
	//	requestInBytes,   // min
	//	requestInBytes);  // max
	//CloseHandle(hProcess);

	//auto bufferSize = 4096 * ((bytes + 4095) / 4096); 
	//auto bufferPtr = (void *)::VirtualAlloc(nullptr, bufferSize, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
	//if (bufferPtr == nullptr)
	//{
	//	throw std::runtime_error("Failed to alloc memory");
	//}
	//auto b = VirtualLock(bufferPtr, bufferSize);
	//if (!b)
	//{
	//	std::cout << GetLastError() << std::endl;
	//	throw std::runtime_error("Failed to lock memory");
	//}
	//return bufferPtr;
}

std::pair<Ipp16uArray, int> DevIO::readDpxFile(const string & fileName, bool normalize, int imageIndex)
{
	// open the image
	InStream img;
	if (!img.Open(fileName.c_str()))
	{
		std::ostringstream os;
		os << "Unable to open file " << fileName << std::endl;
		throw std::runtime_error(os.str());
	}

	dpx::Reader dpx;
	dpx.SetInStream(&img);
	if (!dpx.ReadHeader())
	{
		std::ostringstream os;
		os << "Unable to read header" << std::endl;
		throw std::runtime_error(os.str());
	}

	// data size, override if user specifies
	dpx::DataSize size = dpx.header.ComponentDataSize(imageIndex);
	int bd = dpx.header.BitDepth(imageIndex);

	if ((dpx.header.ComponentByteCount(imageIndex) != 2) && (bd != 1))
	{
		std::ostringstream os;
		os << "Unsupported DPX component data size " << bd << std::endl;
		throw std::runtime_error(os.str());
	}

	auto width =int( dpx.header.Width());
	auto height = int(dpx.header.Height());
	auto channels = int(dpx.header.ImageElementComponentCount(imageIndex));
	auto bitDepth = dpx.header.BitDepth(imageIndex);

	auto bytesToRead = channels * height * width * sizeof(Ipp16u);
	//auto p = (Ipp16u *)getPinnedMemory(bytesToRead);
	Ipp16uArray result({ width, height, channels });

	auto p = result.data();
	// Special case, 1 bit
	if (bitDepth == 1)
	{
		vector<Ipp8u> bi((channels * height * width)/8 + 1);
		if (dpx.ReadImage(imageIndex, bi.data()) == false)
		{
			std::ostringstream os;
			os << "Failed to read image data " << std::endl;
			throw std::runtime_error(os.str());
		}

		// This is ugly as sin
		auto i = 0;
		memset(p, 0, bytesToRead);
		for (auto &v : result)
		{
			auto byteIndex = i / 8;
			auto bitIndex = i % 8;
			i++;
			auto mask = (1 << bitIndex);
			if ((mask & bi[byteIndex]) != 0)
			{
				v = 1;
			}
		}
	}
	else
	{
		if (dpx.ReadImage(imageIndex, p) == false)
		{
			std::ostringstream os;
			os << "Failed to read image data " << std::endl;
			throw std::runtime_error(os.str());
		}
	}

	img.Close();

	// Early out
	if (bitDepth == 16)
	{
		return { result, bitDepth };
	}
	
	if (normalize == false)
	{
		if (bitDepth != 1)
		{
			auto s = 1 << (16 - bitDepth);
			result /= s;
		}
	}
	else
	{
		if (bitDepth == 1)
		{
			result *= 0xFFFF;
		}

		bitDepth = 16;
	}

	return { result, bitDepth};
}
