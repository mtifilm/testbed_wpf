#pragma once

#include <algorithm>
#include <numeric>
#define __funcS__ string(__func__)

#include <sstream>
#include <assert.h>
#include <chrono>
#include <thread>
#include <cctype>
#include <algorithm>

#include "HRTimer.h"
#include <filesystem>
#include <fstream>

// Just use the standard code rather then ours
#define MTIostringstream std::ostringstream
#define MTIassert assert
#define MTI_UINT16 unsigned short
#define MTImemcpy memcpy
#define TRACE_0(A) 
#define TRACE_1(A)
#define TRACE_2(A)
#define TRACE_3(A)

#define DBTRACE(A) std::cout << "Line: " << __LINE__ << ", " << #A << " = " << A << std::endl;
#define DBTRACE2(A, B) std::cout << "Line: " << __LINE__ << ", " << #A << " = " << A << ", " << #B << " = " << B  << std::endl;
#define DBTIMER(name) DBTimer name##_TIMER(#name)
#define DBCOMMENT(A) std::cout << "Line: " << __LINE__ << ", " << #A << std::endl;

class DBTimer
{
	CHRTimer _timer;
	std::string _name;

public:
	DBTimer(const std::string &name) : _name(name) {}
	~DBTimer()
	{
		std::cout << _name << " took " << _timer << " msec"  << std::endl;
	}
};
//#define DUMP_TIME(A)

#define MTImillisleep(A) std::this_thread::sleep_for(std::chrono::milliseconds(A)) 

//#ifdef ZONALCORE_EXPORTS
//#define ZonalExport __declspec(dllexport)
//#else
//#define ZonalExport __declspec(dllimport)
//#endif

// This is a simple iterator for integer and loops
// simple replacement for for loops
// In release mode it is slightly slower than for loops so be careful
#include <iterator>     // std::iterator, std::input_iterator_tag
class loopIterator : public std::iterator<std::input_iterator_tag, int>
{
public:
	loopIterator(int x) :_q(x) {}
	loopIterator(const loopIterator& mit) : _q(mit._q) {}
	loopIterator& operator++() { ++_q; return *this; }
	loopIterator operator++(int) { loopIterator tmp(*this); operator++(); return tmp; }
	bool operator==(const loopIterator& rhs) const { return _q == rhs._q; }
	bool operator!=(const loopIterator& rhs) const { return _q != rhs._q; }
	int& operator*() { return _q; }
	void setQ(int n) { _q = n; }

private:
	int _q;
};

// range(low, high) goes from low to high, 
// including low but excluding high.
// for (i = low; i < high; i++)
// usage
//   	for (auto i : range(low, high))
class range
{
public:
	range(int high) : range(0, high) {}
	range(size_t high) : range(0, int(high)) {}

	range(size_t low, size_t high) : range(int(low), int(high)) {}
	range(int low, int high)
	{
		// if high < low, we would never stop
		// so, if so, just set to no loop
		high = std::max<int>(low, high);
		_begin.setQ(low);
		_end.setQ(high);
	}

	loopIterator begin() { return _begin; }
	loopIterator end() { return _end; }

private:
	loopIterator _begin{ 0 };
	loopIterator _end{ 0 };
};

// loop(start, reps) does reps repeditions starting at start
// for (i=start; i < start+reps; i++)
// usage
//     for (auto i : loop(start, reps))
// or
//     for (auto i : loop(reps))  // Same as range(reps);
class loop : public range
{
	loop(size_t start, size_t reps) : range(int(start), int(start + reps)) {}
	loop(int start, int reps) : range(start, start + reps) {	}
	loop(int reps) : range(reps) {	}
};

//-------------EqualIgnoreCase-------------John Mertus--Aug 2019---------

static bool EqualIgnoreCase(const std::string &str1, const std::string &str2)

// This checks if the strings S1 and S2 are the same, case insensitive.
//
//***********************************************************************
{
	return
	(str1.size() == str2.size()) && std::equal(str1.begin(), str1.end(), str2.begin(),
		[](const char & c1, const char & c2) 
		{
		   return (c1 == c2 || std::toupper(c1) == std::toupper(c2));
		}
	);
}

static bool DoesFileExist(const std::string &fileName)
{
	//return std::filesystem::exists(std::filesystem::path(fileName));
	std::ifstream f(fileName);
	return f.good();
}