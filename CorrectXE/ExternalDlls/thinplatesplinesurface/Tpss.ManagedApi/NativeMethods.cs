﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NativeMethods.cs" company="MTI Film LLC">
// Copyright (c) 2017 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>MTI\hans.lehmann</author>
// <date>12/14/2017 4:41:44 PM</date>
// <summary></summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ThinPlateSplineSurface.ManagedApi
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Runtime.InteropServices;
    using System.Security;

    /// <summary>
    /// Thin plate spline surface processing class
    /// </summary>
    public static class NativeMethods
    {
        /// <summary>
        /// The DLL to use
        /// </summary>
        private const string TpssLibName = "TpssDLL.dll";

        //////////////////////////////////////////////////////////////////////
        //  The Thin Plate Spline Surface DLL interfaces
        //////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Allocates the APi
        /// </summary>
        /// <param name="gpuIndex">Index of the GPU.</param>
        /// <returns>
        /// The handle
        /// </returns>
        [SuppressUnmanagedCodeSecurity]
        [SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", Justification = "The DLL requires this")]
        [DllImport(TpssLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint = "Tpss_Open")]
        public static extern IntPtr TpssOpen(int gpuIndex);

        /// <summary>
        /// closes this instance
        /// </summary>
        /// <param name="instanceHandle">The instance handle.</param>
        /// <returns></returns>
        [SuppressUnmanagedCodeSecurity]
        [SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", Justification = "The DLL requires this")]
        [DllImport(TpssLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint = "Tpss_Close")]
        public static extern int TpssClose(IntPtr instanceHandle);

        /// <summary>
        /// Process a frame
        /// </summary>
        /// <param name="instanceHandle">The instance handle.</param>
        /// <param name="surface">The surface.</param>
        /// <param name="sourceWidth">Width of the source.</param>
        /// <param name="sourceHeight">Height of the source.</param>
        /// <param name="roiColumnStart">The roi column start.</param>
        /// <param name="roiColumnEnd">The roi column end.</param>
        /// <param name="roiRowStart">The roi row start.</param>
        /// <param name="roiRowEnd">The roi row end.</param>
        /// <param name="channel">The channel.</param>
        /// <param name="samplesPerPixel">The samples per pixel.</param>
        /// <param name="weights">The weights.</param>
        /// <param name="controlPoints">The control points.</param>
        /// <param name="pointCount">The point count.</param>
        /// <returns>
        /// error code
        /// </returns>
        [SuppressUnmanagedCodeSecurity]
        [SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", Justification = "The DLL requires this")]
        [DllImport(TpssLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint = "Tpss_ProcessFrame")]
        public static extern int TpssProcessFrame(
            IntPtr instanceHandle,
            byte[] surface,
            int sourceWidth,
            int sourceHeight,
            int roiColumnStart,
            int roiColumnEnd,
            int roiRowStart,
            int roiRowEnd,
            float[] weights,
            int[] controlPoints,
            int pointCount);

        /// <summary>
        /// Process a frame
        /// </summary>
        /// <param name="instanceHandle">The instance handle.</param>
        /// <param name="surface">The surface.</param>
        /// <param name="sourceWidth">Width of the source.</param>
        /// <param name="sourceHeight">Height of the source.</param>
        /// <param name="roiColumnStart">The roi column start.</param>
        /// <param name="roiColumnEnd">The roi column end.</param>
        /// <param name="roiRowStart">The roi row start.</param>
        /// <param name="roiRowEnd">The roi row end.</param>
        /// <param name="channel">The channel.</param>
        /// <param name="samplesPerPixel">The samples per pixel.</param>
        /// <param name="weights">The weights.</param>
        /// <param name="controlPoints">The control points.</param>
        /// <param name="pointCount">Size of the weights.</param>
        /// <returns>
        /// error code
        /// </returns>
        [SuppressUnmanagedCodeSecurity]
        [SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", Justification = "The DLL requires this")]
        [DllImport(TpssLibName, CallingConvention = CallingConvention.Cdecl, EntryPoint = "Tpss_ProcessFrame")]
        public static extern int TpssProcessFrame(
            IntPtr instanceHandle,
            IntPtr surface,
            int sourceWidth,
            int sourceHeight,
            int roiColumnStart,
            int roiColumnEnd,
            int roiRowStart,
            int roiRowEnd,
            float[] weights,
            int[] controlPoints,
            int pointCount);
    }
}
