﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ThinPlateSplineSurfaceApi.cs" company="MTI Film LLC">
// Copyright (c) 2017 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>MTI\hans.lehmann</author>
// <date>12/14/2017 5:02:54 PM</date>
// <summary></summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ThinPlateSplineSurface.ManagedApi
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// </summary>
    public class ThinPlateSplineSurfaceApi
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields
        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////
        #region Private fields
        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the ThinPlateSplineSurfaceApi class
        /// </summary>
        public ThinPlateSplineSurfaceApi()
        {
        }

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties
        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods
        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods
        #endregion
    }
}
