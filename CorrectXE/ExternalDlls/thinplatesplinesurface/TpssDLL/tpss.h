#pragma once
#ifndef TPSS_H
#define TPSS_H

#define DOWNSAMPLE 1
class CTpss
{
public:

    CTpss(int gpuIndex);
    ~CTpss(); 

    int ProcessFrame(
        float *fpSurface, 
        int iSrcWidth,
        int iSrcHeight,
        int iRoiColumnStart,
        int iRoiColumnEnd, 
        int iRoiRowStart,
        int iRoiRowEnd,
        const float *fpWeights,
        const int *ipControlPoints,
        int iPointCount);

private:
    int m_iGpuIndex;
    int m_iRoiWidth;
    int m_iRoiHeight;
    bool m_bInitialized;
    float *m_d_fpSurface;
    float *m_d_fpWeights;
    int *m_d_ipControlPoints;
    int m_iPointCount;

    int Initialize(
        int iRoiWidth,
        int iRoiHeight,
        int iPointCount);

    void FreeStorage();
};

int gpGetDeviceCount();
int gpGetDeviceProp(int index, void *result, int size);

extern "C"
{
    int gpProcessFrame
    (
        float *d_fpSurface,
        const float *d_fpWeights,
        const int *d_ipControlPoints,   // X,Y are interleaved
        int iPointCount,    // the number of control points
        int iWidth,
        int iHeight,
        int iRoiColumnStart,
        int iRoiColumnEnd,
        int iRoiRowStart,
        int iRoiRowEnd,
        float a1,
        float ax,
        float ay);
}


#endif