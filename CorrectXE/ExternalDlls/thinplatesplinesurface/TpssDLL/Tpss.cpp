// tpss.cpp

#include "tpss.h"
#include "cuda_runtime.h"
#include <iostream>
#include "TpssDLL.h"

/// <summary>
/// Initializes a new instance of the <see cref="CTpss"/> class.
/// </summary>
/// <param name="gpuIndex">Index of the GPU.</param>
CTpss::CTpss(int gpuIndex)
{
    m_iGpuIndex = gpuIndex;
    m_bInitialized = false;
    m_d_fpWeights = NULL;
    m_d_fpSurface = NULL;
    m_d_ipControlPoints = NULL;
    m_iPointCount = 0;
    m_iRoiWidth = 0;
    m_iRoiHeight = 0;
}

/// <summary>
/// Finalizes an instance of the <see cref="CTpss"/> class.
/// </summary>
CTpss::~CTpss()
{
    FreeStorage();
}

/// <summary>
/// Processes the frame.
/// </summary>
/// <param name="fpSurface">The surface.</param>
/// <param name="iSrcWidth">Width of the ource.</param>
/// <param name="iSrcHeight">Height of the ource.</param>
/// <param name="iRoiColumnStart">The roi column start.</param>
/// <param name="iRoiColumnEnd">The roi column end.</param>
/// <param name="iRoiRowStart">The roi row start.</param>
/// <param name="iRoiRowEnd">The roi row end.</param>
/// <param name="fpWeights">The weights array.</param>
/// <param name="ipControlPoints">The ip control points.</param>
/// <param name="iPointCount">the number of points</param>
/// <returns>
/// the error code
/// </returns>
int CTpss::ProcessFrame(
    float* fpSurface,
    int iSrcWidth,
    int iSrcHeight,
    int iRoiColumnStart,
    int iRoiColumnEnd,
    int iRoiRowStart,
    int iRoiRowEnd,
    const float* fpWeights,
    const int *ipControlPoints,
    int iPointCount)
{
#ifdef _DEBUG
	HRTimer hrt;
	hrt.start();
#endif

    int iRet;
    if (fpSurface == NULL
        || fpWeights == NULL
        || iRoiColumnStart > iRoiColumnEnd
        || iRoiRowStart > iRoiRowEnd
        || iRoiColumnStart < 0
        || iRoiColumnEnd > iSrcWidth - 1
        || iRoiRowEnd > iSrcHeight - 1
        || iPointCount <= 0)
    {
        return TPSS_ERR_BAD_PARAMETER;
    }

    int roiWidth = iRoiColumnEnd - iRoiColumnStart + 1;
    int roiHeight = iRoiRowEnd - iRoiRowStart + 1;

    if (iPointCount > m_iPointCount
        || roiWidth > m_iRoiWidth
        || roiHeight > m_iRoiHeight)
    {
        m_bInitialized = false;
    }

    if (!m_bInitialized)
    {
        iRet = Initialize(
            roiWidth,
            roiHeight,
            iPointCount);
        if (iRet != 0)
        {
            return iRet;
        }
#ifdef _DEBUG
 	std::cout << "Initialization: " << hrt.IntervalMilliseconds() << std::endl;
#endif
	}

    cudaError error = cudaSetDevice(m_iGpuIndex);
    if (error != cudaSuccess)
    {
        return error;
    }

    error = cudaMemcpy(m_d_fpWeights, fpWeights, m_iPointCount * sizeof(float), cudaMemcpyHostToDevice);
    if (error != cudaSuccess)
    {
        return error;
    }

    error = cudaMemcpy(m_d_ipControlPoints, ipControlPoints, m_iPointCount * 2 * sizeof(int), cudaMemcpyHostToDevice);
    if (error != cudaSuccess)
    {
        return error;
    }

    cudaDeviceProp prop;
    error = cudaGetDeviceProperties(&prop, m_iGpuIndex);
    if (error != cudaSuccess)
    {
        return error;
    }

    int sharedMemorySize = m_iPointCount * (sizeof(float) + 2 * sizeof(int));
    if (sharedMemorySize > prop.sharedMemPerBlock)
    {
        return TPSS_ERR_INSUFFICENT_GPU_RESOURCES;
    }

    if (m_iPointCount > prop.maxThreadsPerBlock)
    {
        return TPSS_ERR_INSUFFICENT_GPU_RESOURCES;
    }
#ifdef _DEBUG
	std::cout << "Input data copy: " << hrt.IntervalMilliseconds() << std::endl;
	hrt.start();
#endif
    iRet = gpProcessFrame(
        m_d_fpSurface,
        m_d_fpWeights,
        m_d_ipControlPoints,
        m_iPointCount,
        iSrcWidth,
        iSrcHeight,
        iRoiColumnStart,
        iRoiColumnEnd,
        iRoiRowStart,
        iRoiRowEnd,
        fpWeights[iPointCount],
        fpWeights[iPointCount + 1],
        fpWeights[iPointCount + 2]);
    if (iRet != cudaSuccess)
    {
        return error;
    }

#ifdef _DEBUG
	std::cout << "Process frame: " << hrt.IntervalMilliseconds() << std::endl;
	hrt.start();
#endif

    // retrieve results
	int surfaceSizeInFloats = (m_iRoiWidth / DOWNSAMPLE) * (m_iRoiHeight / DOWNSAMPLE);
	std::cout << "surfaceSizeInFloats " << surfaceSizeInFloats << std::endl;
    error = cudaMemcpy(fpSurface, m_d_fpSurface, surfaceSizeInFloats * sizeof(float), cudaMemcpyDeviceToHost);
	if (error != cudaSuccess)
    {
        return error;
    }

#ifdef _DEBUG
	std::cout << "Output memory copy: " << hrt.IntervalMilliseconds() << std::endl;
#endif

    return TPSS_RET_SUCCESS;
}

/// <summary>
/// Initializes this instance
/// </summary>
/// <param name="iRoiWidth">Width of the ROI.</param>
/// <param name="iRoiHeight">Height of the ROI.</param>
/// <param name="iPointCount">point count.</param>
/// <returns>error code></returns>
int CTpss::Initialize(
    int iRoiWidth,
    int iRoiHeight,
    int iPointCount)
{
    int count;
    cudaGetDeviceCount(&count);

    if (m_iGpuIndex >= count)
    {
        return TPSS_ERR_GPU_NOT_FOUND;
    }

    cudaError error = cudaSetDevice(m_iGpuIndex);
    if (error != cudaSuccess)
    {
        return error;
    }

    FreeStorage();
    error = cudaMalloc(&m_d_fpWeights, iPointCount * sizeof(float));
    if (error != cudaSuccess)
    {
        return error;
    }

    error = cudaMalloc(&m_d_ipControlPoints, iPointCount * 2 * sizeof(float));
    if (error != cudaSuccess)
    {
        return error;
    }

	int surfaceSizeInFloats = (iRoiWidth / DOWNSAMPLE) * (iRoiHeight / DOWNSAMPLE);
    error = cudaMalloc(&m_d_fpSurface, surfaceSizeInFloats * sizeof(float));
//	error = cudaMallocHost(&m_d_fpSurface, surfaceSizeInFloats * sizeof(float));
	if (error != cudaSuccess)
    {
        return error;
    }

    m_iRoiWidth = iRoiWidth;
    m_iRoiHeight = iRoiHeight;
    m_iPointCount = iPointCount;

    m_bInitialized = true;
    return TPSS_RET_SUCCESS;
}

/// <summary>
/// Frees the storage.
/// </summary>
void CTpss::FreeStorage()
{
    if (m_d_fpWeights != NULL)
    {
        cudaFree(m_d_fpWeights);
        m_d_fpWeights = NULL;
    }

    if (m_d_ipControlPoints != NULL)
    {
        cudaFree(m_d_ipControlPoints);
        m_d_ipControlPoints = NULL;
    }

    if (m_d_fpSurface != NULL)
    {
		cudaFree(m_d_fpSurface);
//        cudaFreeHost(m_d_fpSurface);
        m_d_fpSurface = NULL;
    }
}

int gpGetDeviceCount()
{
	int count = 0;
	auto error = cudaGetDeviceCount(&count);
	if (error == cudaSuccess)
	{
		return count;
	}

	return -error;
}

int gpGetDeviceProp(int index, void *result, int size)
{
	cudaDeviceProp prop;
	cudaGetDeviceProperties(&prop, index);

	auto cs = sizeof(cudaDeviceProp);
	memcpy_s(result, size, &prop, cs);
	return cs;
}

