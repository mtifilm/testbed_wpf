//
//  Tpss_cuda.cu
//

#include <vector_types.h>
#include <vector_functions.hpp>
#include <math.h>
#include <math_functions.hpp>
#include <assert.h>
#include <stdio.h>
#include "device_launch_parameters.h"
#include "tpss.h"

// Division, rounding up to next higher integer
#define iDivUp(a, b) (((a)%(b)!= 0)?((a)/(b)+1):((a)/(b)))

extern __shared__ float shared[];

////__device__ __inline__ int repack_rgb(int R, int G, int B)
////{
////    struct
////    {
////        char rP[4];
////    }srP;
////    unsigned int *rePacked = (unsigned int*)&srP;
////
////    srP.rP[0] = ((int)R >> 2);
////    srP.rP[1] = ((R & 0x03) << 6) + (G >> 4);
////    srP.rP[2] = ((G & 0x0F) << 4) + (B >> 6);
////    srP.rP[3] = ((B & 0x3F) << 2);
////
////    return *rePacked;
////}

//------------------------------------------------------------------
__device__ __inline__ float distance(int2 p, int2 q)
{
    int2 diff = make_int2(p.x - q.x, p.y - q.y);
    float r = sqrtf((float)((diff.x * diff.x) + (diff.y * diff.y)));
    if (r == 0)
    {
        return 0;
    }

    return r * logf(r);
}

//------------------------------------------------------------------
__global__ void ProcessFrame(
    float *d_fpSurface,
    const float *d_fpWeights,
    const int2* d_ipControlPoints,
    int pointCount,
    int srcWidth,
    int srcHeight,
    int roiColumnStart,
    int roiColumnEnd,
    int roiRowStart,
    int roiRowEnd,
    float a1,
    float ax,
    float ay)
{
    int dstX = threadIdx.x + (blockIdx.x * blockDim.x);
    int dstY = threadIdx.y + (blockIdx.y * blockDim.y);

    int srcX = roiColumnStart + dstX;
    int srcY = roiRowStart + dstY;

    float *sharedWeights = shared;
    int2 *sharedControlPoints = (int2*)(sharedWeights + pointCount);

    if (threadIdx.x < pointCount)
    {
        sharedWeights[threadIdx.x] = d_fpWeights[threadIdx.x];
        sharedControlPoints[threadIdx.x] = d_ipControlPoints[threadIdx.x];
    }

    __syncthreads();

    if (srcX >= srcWidth)
    {
        return;
    }

    if (srcY >= srcHeight)
    {
        return;
    }

    if (dstX >= roiColumnEnd - roiColumnStart + 1)
    {
        return;
    }

    if (dstY >= roiRowEnd - roiRowStart + 1)
    {
        return;
    }

	if ((dstY % DOWNSAMPLE != 0) || (dstX % DOWNSAMPLE != 0))
	{
		return;
	}

    int roiWidth = (roiColumnEnd - roiColumnStart + 1);
    int dstIndex = (dstX/DOWNSAMPLE) + (roiWidth/DOWNSAMPLE) * (dstY/DOWNSAMPLE);

    float affine = a1 + ax * srcX + ay * srcY;
    float nonRigid = 0;
    for (int i = 0; i < pointCount; i++)
    {
        // the distance from this image point to each of the control points
        float d = distance(sharedControlPoints[i], make_int2(srcX, srcY));
        nonRigid += sharedWeights[i] * d;
    }

    d_fpSurface[dstIndex] = affine + nonRigid;
}

//------------------------------------------------------------------
extern "C"
int gpProcessFrame
(
    float *d_fpSurface,
    const float *d_fpWeights,
    const int *d_ipControlPoints,
    int pointCount,
    int width,
    int height,
    int roiColumnStart,
    int roiColumnEnd,
    int roiRowStart,
    int roiRowEnd,
    float a1,
    float ax,
    float ay)
{
    dim3 threadBlock(pointCount, 1, 1);
    dim3 blockGrid(1, 1, 1);
    int roiWidth = roiColumnEnd - roiColumnStart + 1;
    int roiHeight = roiRowEnd - roiRowStart + 1;
    blockGrid.x = iDivUp(roiWidth, threadBlock.x);
    blockGrid.y = iDivUp(roiHeight, threadBlock.y);
    int sharedMemorySize = pointCount * (sizeof(float) + 2 * sizeof(int));

    ProcessFrame << < blockGrid, threadBlock, sharedMemorySize, 0 >> > (
        d_fpSurface,
        d_fpWeights,
        (int2*)d_ipControlPoints,
        pointCount,
        width,
        height,
        roiColumnStart,
        roiColumnEnd,
        roiRowStart,
        roiRowEnd,
        a1,
        ax,
        ay);

    return 0;
}
