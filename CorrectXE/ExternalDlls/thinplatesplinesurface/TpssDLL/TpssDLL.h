#pragma once
#ifndef TPSS_DLL_H
#define TPSS_DLL_H

#ifdef TPSS_EXPORTS
#define DLL_TPSS_API extern "C" __declspec(dllexport)
#else
#ifdef TPSSDLL_IMPORTS
#define DLL_TPSS_API extern "C" __declspec(dllimport)
#else
#define DLL_TPSS_API [System::Runtime::InteropServices::DllImport("TpssDLL.dll")] extern "C"
#endif
#endif

#include "chrono"
#include "iostream"
class HRTimer
{
public:
	HRTimer()
	{
		start();
	}

	void start()
	{
		_started = true;
		_start = std::chrono::system_clock::now();
		_intervalStart = _start;
		_end = _start;
	}

	void stop()
	{
		_started = false;
		_end = std::chrono::system_clock::now();
	}

	double ElapsedMilliseconds()
	{
		auto end = _end;
		if (_started)
		{
			end = std::chrono::system_clock::now();
		}

		return std::chrono::duration_cast<std::chrono::microseconds>(end - _start).count() / 1000.0;
	}


	double IntervalMilliseconds()
	{
		auto end = std::chrono::system_clock::now();
		auto start = _intervalStart;
		_intervalStart = end;

		return std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() / 1000.0;
	}

private:
	bool _started;
	std::chrono::system_clock::time_point _start;
	std::chrono::system_clock::time_point _intervalStart;
	std::chrono::system_clock::time_point _end;
};


#define TPSS_RET_SUCCESS              0

#define TPSS_ERR_ALLOC                      -1000
#define TPSS_ERR_DIMENSION                  -1001
#define TPSS_ERR_CHANNEL                    -1002
#define TPSS_ERR_GPU_NOT_FOUND              -1003
#define TPSS_ERR_BAD_PARAMETER              -1004
#define TPSS_ERR_INSUFFICENT_GPU_RESOURCES  -1005

/// <summary>
/// Opens this instance
/// </summary>
/// <param name="gpuIndex">Index of the GPU. First GPU is index 0</param>
/// <returns>a handle to this instance</returns>
DLL_TPSS_API void *Tpss_Open(int gpuIndex);

/// <summary>
/// Process the frame
/// </summary>
/// <param name="vp">The instance handle</param>
/// <param name="fpSurface">The surface.  Single layer floating point. Same dimensions as the ROI</param>
/// <param name="iSrcWidth">Width of the source image.</param>
/// <param name="iSrcHeight">Height of the source image.</param>
/// <param name="iRoiColumnStart">First column of ROI within the source image</param>
/// <param name="iRoiColumnEnd">Last column of ROI within the source image</param>
/// <param name="iRoiRowStart">First row of ROI within the source image</param>
/// <param name="iRoiRowEnd">Last row of ROI within the source image</param>
/// <param name="fpWeights">The weights. iPointCount floats, plus A1, AX, and AY tacked on at the end</param>
/// <param name="ipControlPoints">The control points.  Interleaved, e.g. x0, y0, x1, y1...</param>
/// <param name="iPointCount">the number of control points and number of weights</param>
/// <returns>
/// error code
/// </returns>
DLL_TPSS_API int Tpss_ProcessFrame(
    void *vp,
    float *fpSurface,
    int iSrcWidth,
    int iSrcHeight,
    int iRoiColumnStart,
    int iRoiColumnEnd,
    int iRoiRowStart,
    int iRoiRowEnd,
    const float *fpWeights,       // 3 larger than iPointCount, also contains a1, ax, ay
    const int *ipControlPoints,   // X,Y values are interleaved
    int iPointCount);

/// <summary>
/// Closes this instance and frees memory
/// </summary>
/// <param name="vp">the handle to this instance</param>
/// <returns>an error code</returns>
DLL_TPSS_API int Tpss_Close(void *vp);

DLL_TPSS_API int Tpss_GetDeviceCount();

DLL_TPSS_API int Tpss_GetDeviceProp(int index, char *result, int size);
#endif