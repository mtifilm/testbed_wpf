// TpssDLL.cpp : Defines the exported functions for the Thin Plate Spline Surface DLL application.
//

#include <stddef.h>
#include "TpssDLL.h"
#include "tpss.h"

/// <summary>
/// Opens this instance
/// </summary>
/// <param name="gpuIndex">Index of the GPU. First GPU is index 0</param>
/// <returns>a handle to this instance</returns>
void *Tpss_Open(int gpuIndex)
{
    CTpss *vp = new CTpss(gpuIndex);
    return (void*)vp;
}

/// <summary>
/// Closes this instance and frees memory
/// </summary>
/// <param name="vp">the handle to this instance</param>
/// <returns>an error code</returns>
int Tpss_Close(void *vp)
{
    CTpss *tp = (CTpss*)vp;
    if (tp == NULL)
    {
        return TPSS_ERR_BAD_PARAMETER;
    }

    delete tp;
    return TPSS_RET_SUCCESS;
}

/// <summary>
/// Process the frame
/// </summary>
/// <param name="vp">The instance handle</param>
/// <param name="fpSurface">The surface.  Single layer floating width, same dimensions as the ROI</param>
/// <param name="iSrcWidth">Width of the source image.</param>
/// <param name="iSrcHeight">Height of the source image.</param>
/// <param name="iRoiColumnStart">First column of ROI within the source image</param>
/// <param name="iRoiColumnEnd">Last column of ROI within the source image</param>
/// <param name="iRoiRowStart">First Row of ROI within the source image</param>
/// <param name="iRoiRowEnd">Last row of ROI within the source image</param>
/// <param name="fpWeights">The weights. iPointCount floats, plus a1, ax, and ay tacked on at the end</param>
/// <param name="ipControlPoints">The control points.  Interleaved, e.g. x0, y0, x1, y1...</param>
/// <param name="iPointCount">the number of control points and number of weights</param>
/// <returns>
/// error code
/// </returns>
int Tpss_ProcessFrame(
    void* vp,
    float* fpSurface,
    int iSrcWidth,
    int iSrcHeight,
    int iRoiColumnStart,
    int iRoiColumnEnd,
    int iRoiRowStart,
    int iRoiRowEnd,
    const float* fpWeights,
    const int *ipControlPoints,
    int iPointCount)
{
    if (vp == NULL)
    {
        return TPSS_ERR_BAD_PARAMETER;
    }

    CTpss *pTpss = (CTpss*)vp;
    int iRet = pTpss->ProcessFrame(
        fpSurface,
        iSrcWidth,
        iSrcHeight,
        iRoiColumnStart,
        iRoiColumnEnd,
        iRoiRowStart,
        iRoiRowEnd,
        fpWeights,
        ipControlPoints,
        iPointCount);

    return iRet;
}

int Tpss_GetDeviceCount()
{
	int count = 0;
	return gpGetDeviceCount();
}

int Tpss_GetDeviceProp(int index, char *result, int size)
{
	return gpGetDeviceProp(index, result, size);
}
