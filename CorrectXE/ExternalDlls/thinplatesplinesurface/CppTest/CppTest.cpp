// CppTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "TpssDLL.h"
#include "ippcore.h"
#include "ippi.h"
#include "chrono"
#include "iostream"
#include <algorithm>
#include <cctype>
#include <iostream>
#include <string>
#include <vector>

std::vector<IppiPoint> getPositions()
{
	std::vector<IppiPoint> result(176);
	result[0] = { 2, 1 };
	result[1] = { 10, 1 };
	result[2] = { 18, 1 };
	result[3] = { 26, 1 };
	result[4] = { 34, 1 };
	result[5] = { 42, 1 };
	result[6] = { 50, 1 };
	result[7] = { 58, 1 };
	result[8] = { 66, 1 };
	result[9] = { 75, 1 };
	result[10] = { 83, 1 };
	result[11] = { 91, 1 };
	result[12] = { 99, 1 };
	result[13] = { 107, 1 };
	result[14] = { 115, 1 };
	result[15] = { 123, 1 };
	result[16] = { 2, 10 };
	result[17] = { 10, 10 };
	result[18] = { 18, 10 };
	result[19] = { 26, 10 };
	result[20] = { 34, 10 };
	result[21] = { 42, 10 };
	result[22] = { 50, 10 };
	result[23] = { 58, 10 };
	result[24] = { 66, 10 };
	result[25] = { 75, 10 };
	result[26] = { 83, 10 };
	result[27] = { 91, 10 };
	result[28] = { 99, 10 };
	result[29] = { 107, 10 };
	result[30] = { 115, 10 };
	result[31] = { 123, 10 };
	result[32] = { 2, 19 };
	result[33] = { 10, 19 };
	result[34] = { 18, 19 };
	result[35] = { 26, 19 };
	result[36] = { 34, 19 };
	result[37] = { 42, 19 };
	result[38] = { 50, 19 };
	result[39] = { 58, 19 };
	result[40] = { 66, 19 };
	result[41] = { 75, 19 };
	result[42] = { 83, 19 };
	result[43] = { 91, 19 };
	result[44] = { 99, 19 };
	result[45] = { 107, 19 };
	result[46] = { 115, 19 };
	result[47] = { 123, 19 };
	result[48] = { 2, 28 };
	result[49] = { 10, 28 };
	result[50] = { 18, 28 };
	result[51] = { 26, 28 };
	result[52] = { 34, 28 };
	result[53] = { 42, 28 };
	result[54] = { 50, 28 };
	result[55] = { 58, 28 };
	result[56] = { 66, 28 };
	result[57] = { 75, 28 };
	result[58] = { 83, 28 };
	result[59] = { 91, 28 };
	result[60] = { 99, 28 };
	result[61] = { 107, 28 };
	result[62] = { 115, 28 };
	result[63] = { 123, 28 };
	result[64] = { 2, 37 };
	result[65] = { 10, 37 };
	result[66] = { 18, 37 };
	result[67] = { 26, 37 };
	result[68] = { 34, 37 };
	result[69] = { 42, 37 };
	result[70] = { 50, 37 };
	result[71] = { 58, 37 };
	result[72] = { 66, 37 };
	result[73] = { 75, 37 };
	result[74] = { 83, 37 };
	result[75] = { 91, 37 };
	result[76] = { 99, 37 };
	result[77] = { 107, 37 };
	result[78] = { 115, 37 };
	result[79] = { 123, 37 };
	result[80] = { 2, 45 };
	result[81] = { 10, 45 };
	result[82] = { 18, 45 };
	result[83] = { 26, 45 };
	result[84] = { 34, 45 };
	result[85] = { 42, 45 };
	result[86] = { 50, 45 };
	result[87] = { 58, 45 };
	result[88] = { 66, 45 };
	result[89] = { 75, 45 };
	result[90] = { 83, 45 };
	result[91] = { 91, 45 };
	result[92] = { 99, 45 };
	result[93] = { 107, 45 };
	result[94] = { 115, 45 };
	result[95] = { 123, 45 };
	result[96] = { 2, 54 };
	result[97] = { 10, 54 };
	result[98] = { 18, 54 };
	result[99] = { 26, 54 };
	result[100] = { 34, 54 };
	result[101] = { 42, 54 };
	result[102] = { 50, 54 };
	result[103] = { 58, 54 };
	result[104] = { 66, 54 };
	result[105] = { 75, 54 };
	result[106] = { 83, 54 };
	result[107] = { 91, 54 };
	result[108] = { 99, 54 };
	result[109] = { 107, 54 };
	result[110] = { 115, 54 };
	result[111] = { 123, 54 };
	result[112] = { 2, 63 };
	result[113] = { 10, 63 };
	result[114] = { 18, 63 };
	result[115] = { 26, 63 };
	result[116] = { 34, 63 };
	result[117] = { 42, 63 };
	result[118] = { 50, 63 };
	result[119] = { 58, 63 };
	result[120] = { 66, 63 };
	result[121] = { 75, 63 };
	result[122] = { 83, 63 };
	result[123] = { 91, 63 };
	result[124] = { 99, 63 };
	result[125] = { 107, 63 };
	result[126] = { 115, 63 };
	result[127] = { 123, 63 };
	result[128] = { 2, 72 };
	result[129] = { 10, 72 };
	result[130] = { 18, 72 };
	result[131] = { 26, 72 };
	result[132] = { 34, 72 };
	result[133] = { 42, 72 };
	result[134] = { 50, 72 };
	result[135] = { 58, 72 };
	result[136] = { 66, 72 };
	result[137] = { 75, 72 };
	result[138] = { 83, 72 };
	result[139] = { 91, 72 };
	result[140] = { 99, 72 };
	result[141] = { 107, 72 };
	result[142] = { 115, 72 };
	result[143] = { 123, 72 };
	result[144] = { 2, 81 };
	result[145] = { 10, 81 };
	result[146] = { 18, 81 };
	result[147] = { 26, 81 };
	result[148] = { 34, 81 };
	result[149] = { 42, 81 };
	result[150] = { 50, 81 };
	result[151] = { 58, 81 };
	result[152] = { 66, 81 };
	result[153] = { 75, 81 };
	result[154] = { 83, 81 };
	result[155] = { 91, 81 };
	result[156] = { 99, 81 };
	result[157] = { 107, 81 };
	result[158] = { 115, 81 };
	result[159] = { 123, 81 };
	result[160] = { 2, 89 };
	result[161] = { 10, 89 };
	result[162] = { 18, 89 };
	result[163] = { 26, 89 };
	result[164] = { 34, 89 };
	result[165] = { 42, 89 };
	result[166] = { 50, 89 };
	result[167] = { 58, 89 };
	result[168] = { 66, 89 };
	result[169] = { 75, 89 };
	result[170] = { 83, 89 };
	result[171] = { 91, 89 };
	result[172] = { 99, 89 };
	result[173] = { 107, 89 };
	result[174] = { 115, 89 };
	result[175] = { 123, 89 };

	for (auto &p : result)
	{
		p.x = 16 * p.x;
		p.y = 16 * p.y;
	}

	return result;
}
std::vector<float> getWeights()
{
	std::vector<float> result(179);
	result[0] = 1.74613e-06f;
	result[1] = 9.25492e-07f;
	result[2] = -5.66757e-07f;
	result[3] = 1.02143e-06f;
	result[4] = -6.55517e-07f;
	result[5] = 9.98996e-07f;
	result[6] = -5.32687e-07f;
	result[7] = 7.99017e-07f;
	result[8] = 1.99745e-07f;
	result[9] = -1.75933e-06f;
	result[10] = -5.75707e-07f;
	result[11] = 4.15458e-06f;
	result[12] = 9.58047e-08f;
	result[13] = -2.98456e-06f;
	result[14] = 1.82558e-06f;
	result[15] = -6.36503e-07f;
	result[16] = -3.61426e-06f;
	result[17] = -2.65731e-06f;
	result[18] = 5.44359e-07f;
	result[19] = 1.74078e-06f;
	result[20] = -1.13467e-06f;
	result[21] = -3.19186e-06f;
	result[22] = -2.53842e-06f;
	result[23] = -2.81646e-07f;
	result[24] = -1.94966e-06f;
	result[25] = 5.04157e-06f;
	result[26] = -3.30535e-06f;
	result[27] = 5.70186e-07f;
	result[28] = -4.58602e-08f;
	result[29] = 2.48299e-06f;
	result[30] = -2.74892e-06f;
	result[31] = 1.01871e-06f;
	result[32] = 3.22374e-06f;
	result[33] = -9.79967e-07f;
	result[34] = 9.50834e-07f;
	result[35] = 1.03478e-06f;
	result[36] = 1.65277e-07f;
	result[37] = 7.14863e-07f;
	result[38] = 2.80345e-06f;
	result[39] = 5.7346e-06f;
	result[40] = 3.84875e-06f;
	result[41] = -9.04188e-06f;
	result[42] = 2.56042e-06f;
	result[43] = -1.41249e-06f;
	result[44] = -9.04329e-07f;
	result[45] = -7.42735e-07f;
	result[46] = 1.37408e-06f;
	result[47] = -1.18179e-06f;
	result[48] = 7.42362e-07f;
	result[49] = 3.46827e-07f;
	result[50] = -1.63649e-06f;
	result[51] = 1.95201e-06f;
	result[52] = -2.11148e-06f;
	result[53] = -2.75996e-07f;
	result[54] = -4.0992e-06f;
	result[55] = 6.51872e-06f;
	result[56] = -4.16698e-06f;
	result[57] = -1.16236e-06f;
	result[58] = 3.71459e-06f;
	result[59] = 1.09072e-06f;
	result[60] = -5.62667e-07f;
	result[61] = 2.25234e-06f;
	result[62] = -1.15636e-06f;
	result[63] = 2.17493e-06f;
	result[64] = 3.35875e-07f;
	result[65] = -1.11015e-06f;
	result[66] = -2.59779e-06f;
	result[67] = 2.48361e-06f;
	result[68] = -1.52978e-06f;
	result[69] = 1.28303e-06f;
	result[70] = -1.362e-07f;
	result[71] = 9.8314e-06f;
	result[72] = -1.40016e-05f;
	result[73] = -6.26559e-06f;
	result[74] = 3.85647e-06f;
	result[75] = -3.18277e-06f;
	result[76] = 8.75908e-07f;
	result[77] = -1.52232e-06f;
	result[78] = -2.4966e-06f;
	result[79] = 4.16518e-08f;
	result[80] = -1.81696e-07f;
	result[81] = 1.18437e-06f;
	result[82] = -1.83183e-06f;
	result[83] = 1.52933e-06f;
	result[84] = 1.25107e-06f;
	result[85] = 9.13671e-07f;
	result[86] = -3.51261e-06f;
	result[87] = -8.75225e-06f;
	result[88] = 1.51328e-05f;
	result[89] = 1.54807e-06f;
	result[90] = 5.82615e-08f;
	result[91] = 1.43498e-06f;
	result[92] = 7.99208e-07f;
	result[93] = 3.33049e-06f;
	result[94] = -1.55367e-07f;
	result[95] = 1.2705e-06f;
	result[96] = 2.45533e-07f;
	result[97] = 2.15021e-07f;
	result[98] = -1.33909e-07f;
	result[99] = 3.3681e-07f;
	result[100] = -3.37915e-06f;
	result[101] = 7.98211e-07f;
	result[102] = 1.37626e-06f;
	result[103] = -2.90688e-06f;
	result[104] = 1.6483e-06f;
	result[105] = 5.04102e-07f;
	result[106] = -3.00093e-06f;
	result[107] = -1.05554e-06f;
	result[108] = -1.44602e-06f;
	result[109] = -1.19856e-07f;
	result[110] = -2.51862e-06f;
	result[111] = 1.26549e-07f;
	result[112] = 2.19743e-06f;
	result[113] = -2.89093e-06f;
	result[114] = -1.99736e-07f;
	result[115] = -2.36853e-06f;
	result[116] = 2.59265e-06f;
	result[117] = 1.20369e-06f;
	result[118] = 5.00538e-06f;
	result[119] = -3.11176e-06f;
	result[120] = 2.66447e-06f;
	result[121] = -1.23055e-06f;
	result[122] = 5.10317e-07f;
	result[123] = 6.05047e-06f;
	result[124] = -2.79559e-06f;
	result[125] = 3.08533e-06f;
	result[126] = 2.99037e-07f;
	result[127] = 1.44804e-07f;
	result[128] = -1.93427e-06f;
	result[129] = -1.55079e-06f;
	result[130] = 3.61687e-06f;
	result[131] = 1.31694e-10f;
	result[132] = 1.31238e-05f;
	result[133] = -1.38546e-05f;
	result[134] = 2.40352e-06f;
	result[135] = 5.70617e-06f;
	result[136] = -2.52555e-06f;
	result[137] = 2.86478e-07f;
	result[138] = 4.41851e-06f;
	result[139] = -1.00124e-05f;
	result[140] = -4.55093e-06f;
	result[141] = 1.37303e-07f;
	result[142] = -2.59497e-06f;
	result[143] = -6.33893e-07f;
	result[144] = -2.31698e-07f;
	result[145] = -4.45397e-07f;
	result[146] = 1.11077e-05f;
	result[147] = -2.34401e-05f;
	result[148] = 8.24754e-06f;
	result[149] = -1.68605e-07f;
	result[150] = -3.06584e-06f;
	result[151] = -5.13247e-06f;
	result[152] = -4.03406e-06f;
	result[153] = 1.83193e-06f;
	result[154] = -3.28755e-06f;
	result[155] = -1.5542e-07f;
	result[156] = 6.45443e-06f;
	result[157] = 6.5216e-06f;
	result[158] = -2.65573e-06f;
	result[159] = 2.45226e-06f;
	result[160] = 2.35196e-06f;
	result[161] = 1.11482e-06f;
	result[162] = -5.18263e-06f;
	result[163] = 2.94774e-07f;
	result[164] = 3.93926e-07f;
	result[165] = -1.21454e-06f;
	result[166] = 9.45268e-06f;
	result[167] = -3.22663e-07f;
	result[168] = 5.41175e-06f;
	result[169] = -8.21133e-06f;
	result[170] = 9.95511e-06f;
	result[171] = -8.0295e-06f;
	result[172] = 6.78697e-06f;
	result[173] = -4.44823e-06f;
	result[174] = -3.3892e-06f;
	result[175] = 1.5167e-06f;
	result[176] = -0.00237437f;
	result[177] = 1.32917e-06f;
	result[178] = -4.63424e-07f;
	return result;
}

inline float distance(IppiPoint p, IppiPoint q)
{
	int dx = abs(q.x - p.x);
	int dy = abs(q.y - p.y);

	auto r = sqrt(dx * dx + dy * dy);
	return r == 0 ? 0 : float(r * log(r));
}

float applyTransformation(const std::vector<IppiPoint> &controlPoints, const IppiPoint point,
	const std::vector<float> &weights)
{
	float a1 = weights[weights.size() - 3];
	float ax = weights[weights.size() - 2];
	float ay = weights[weights.size() - 1];

	double affine = a1 + ax * point.x + ay * point.y;
	double nonrigid = 0;
	for (int j = 0; j < controlPoints.size(); j++)
	{
		nonrigid += weights[j] * distance(controlPoints[j], point);
	}

	return (float)(affine + nonrigid);
}

int main()
{
	auto n = Tpss_GetDeviceCount();

	std::vector<char> props(648);
	auto m = Tpss_GetDeviceProp(0, props.data(), int(props.size()));
	auto name = std::string(props.data());

	auto gpuIndex = 0;
	auto gpuHandle = Tpss_Open(gpuIndex);

//	auto width = 2000/16;
//	auto height = 1440/16;
	
	auto width = 2000;
	auto height = 1440;

	std::vector<float> weight = getWeights();

	std::vector<IppiPoint> controlPoint = getPositions();
	auto points = int(controlPoint.size());

	HRTimer hrt;
	hrt.start();
	std::vector<float> surface(height * width);
	auto error = Tpss_ProcessFrame(gpuHandle,
		surface.data(),
		width,
		height,
		0,
		width - 1,
		0,
		height - 1,
		weight.data(),
		(int *)(controlPoint.data()),
		points
	);

	if (error != 0)
	{
		std::cout << "Error using GPU " << error;
		return error;
	}

	std::cout << hrt.IntervalMilliseconds() << std::endl;
	error = Tpss_ProcessFrame(gpuHandle,
		surface.data(),
		width,
		height,
		0,
		width - 1,
		0,
		height - 1,
		weight.data(),
		(int *)(controlPoint.data()),
		points
	);

	if (error != 0)
	{
		std::cout << "Error using GPU " << error;
		return error;
	}

	std::cout << "Tpss_ProcessFrame: " << hrt.IntervalMilliseconds() << std::endl;
	auto c = 32;
	auto r = 16;
	auto index = r/16 * (width /16) + c/16;

	auto s = surface[index] * 1023;
	auto f = applyTransformation(controlPoint, { c, r }, weight) * 1023;

	Tpss_Close(gpuHandle);
	return 0;
}

