#ifndef CudaMtiCoreH
#define CudaMtiCoreH

// This is a visual Studio DLL that interface to either managed
// or Embarcadero via a C-Interface

// Define usual imports/exports
#ifdef CUDAMTICORE_EXPORTS
#define CUDA_MTI_CORE_API extern "C" __declspec(dllexport)
#else
#ifdef CUDAMTICORE_MANAGED
#define CUDA_MTI_CORE_API [System::Runtime::InteropServices::DllImport("MtiCudaCore.dll")] extern "C"
#else
#define CUDA_MTI_CORE_API extern "C" __declspec(dllimport)
#endif
#endif

#include "ippcore.h"
#include "ippi.h"
#include <type_traits>

//**** WARNING this is a raw cuda header, so we fake saying its MSC 2017
// Needed for the Embarcardo Compiler
#ifndef _MSC_VER
#define _MSC_VER 1914
#endif

#include "driver_types.h"

// Check if the CUDA changes size, ugly but forces us to make any changes if CUDA changes
static_assert(sizeof(cudaDeviceProp) >= 680, "Device props wrong size");
static_assert(std::is_trivially_copyable<cudaDeviceProp>::value, "Device props must be copied via mem copy");

//***  END KLUDGE

// These are just mnemonics for arrays
enum class CudaArrayMnemonic
{
	Input = 0,
	Output = 1,
	Ancillary = 2,
	Scratch = 3
};

CUDA_MTI_CORE_API int MtiNativeCudaGetLastError();

CUDA_MTI_CORE_API const char *MtiNativeCudaGetErrorCharPointer(int error);

CUDA_MTI_CORE_API int MtiNativeCudaGetDeviceName(int deviceIndex, char p[], int maxSize);

CUDA_MTI_CORE_API int MtiNativeCudaGetDeviceComputeCapability(int &major, int &minor, int deviceIndex);

CUDA_MTI_CORE_API int MtiNativeCudaGetDeviceCount(int &count);

CUDA_MTI_CORE_API int MtiNativeCudatGetDeviceProp(cudaDeviceProp &prop, int deviceIndex);

CUDA_MTI_CORE_API int MtiNativeCudaMallocHost(void **ptr, size_t bytes);

CUDA_MTI_CORE_API int MtiNativeCudaFreeHost(void *ptr);

CUDA_MTI_CORE_API int MtiNativeCudaSetDevice(int deviceIndex);

CUDA_MTI_CORE_API int MtiNativeCudaDriverGetVersion();

CUDA_MTI_CORE_API int MtiNativeCudaRuntimeGetVersion();

// Include the other interfaces
#include "SubpixelResampler\SubpixelResamplerNativeInterface.h"
#include "ThinPlateSpline\ThinplateSplineNativeInterface.h"
#include "CudaNativeBaseNativeInterface.h"
#endif
