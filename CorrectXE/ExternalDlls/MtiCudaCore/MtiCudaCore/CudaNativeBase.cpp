#include "CudaNativeBase.h"
#include <sstream>
#include <exception>
#include <assert.h>

#include "MtiCudaCoreSupport.h"
CudaNativeBase::CudaNativeBase(int gpuIndex)
{
	_gpuIndex = gpuIndex;

	ThrowOnCudaError(cudaSetDevice(_gpuIndex));

	// This really should be built on demand
	// and have a setter for an external stream
	ThrowOnCudaError(cudaStreamCreate(&_defaultStream));
	_ownsStream = true;
}

CudaNativeBase::~CudaNativeBase()
{
	try
	{
		if (_ownsStream)
		{
			ThrowOnCudaError(cudaStreamDestroy(_defaultStream));
			_defaultStream = 0;  // A stream is just a handle
		}

		for (auto &desc : d_32fDesc)
		{
			freeCudaBuffer(desc.second);
		}

		for (auto &desc : d_16uDesc)
		{
			freeCudaBuffer(desc.second);
		}
	}
	catch (...)
	{
		// This should never happen!
		// since a destructor should not throw an error, just do this in debug mode
		assert(false);
	}
}

void CudaNativeBase::setMinMax(Ipp16u minValue, Ipp16u maxValue)
{
	if (maxValue <= minValue)
	{
		ThrowOnCudaError(cudaErrorInvalidValue);
	}

	_minValue = minValue;
	_maxValue = maxValue;
}

void CudaNativeBase::wait()
{
	ThrowOnCudaError(cudaStreamSynchronize(_defaultStream));
}

void CudaNativeBase::setInputArray(Ipp16u *source, int sourceBytePitch, int height, int width, int channels)
{
	setArray(CudaArrayMnemonic::Input, source, sourceBytePitch, height, width, channels);
}

void CudaNativeBase::setOutputArray(Ipp16u *source, int sourceBytePitch, int height, int width, int channels)
{
	setArray(CudaArrayMnemonic::Output, source, sourceBytePitch, height, width, channels);
}

void CudaNativeBase::setArray(const CudaArrayMnemonic mnemonic, Ipp16u * source, int sourceBytePitch, int height, int width, int channels)
{
	h_16uDesc.insert_or_assign(mnemonic, ImageDesc<Ipp16u>({source, size_t(sourceBytePitch), height, width, channels }));
	allocCudaBufferIfNecessary(h_16uDesc[mnemonic], d_16uDesc[mnemonic]);
}

void CudaNativeBase::setArray(const CudaArrayMnemonic mnemonic, Ipp32f *source, int sourceBytePitch, int height, int width, int channels)
{
	h_32fDesc.insert_or_assign(mnemonic, ImageDesc<Ipp32f>({ source, size_t(sourceBytePitch), height, width, channels }));
	allocCudaBufferIfNecessary(h_32fDesc[mnemonic], d_32fDesc[mnemonic]);
}

void CudaNativeBase::loadArrayToDeviceAsync(const CudaArrayMnemonic mnemonic, Ipp16u * source, int sourceBytePitch)
{
	copyHostToDevice2DAsync(h_16uDesc[mnemonic], d_16uDesc[mnemonic]);
}

//void CudaNativeBase::loadDeviceOutputArrayAsync(Ipp16u *source, int sourceBytePitch)
//{
//	auto desc = h_outImage;
//	desc.data = source;
//	desc.bytePitch = sourceBytePitch;
//	copyHostToDevice2DAsync(desc, d_outImage);
//}

void CudaNativeBase::setMaskArray(Ipp8u *source, int sourceBytePitch, int height, int width, int channels)
{
	h_maskImage = { source, size_t(sourceBytePitch), height, width, channels };
	allocCudaBufferIfNecessary(h_maskImage, d_maskImage);
}

void CudaNativeBase::allocCudaBufferIfNecessary(const ImageDesc<Ipp16u> &h_imageDef, ImageDesc<Ipp16u> &d_imageDesc, cudaTextureReadMode textureReadMode)
{
	auto h_width = h_imageDef.width;
	auto h_height = h_imageDef.height;
	auto h_channels = h_imageDef.channels;

	// nothing to do if already the right size
	if ((h_width == d_imageDesc.width) && (h_height == d_imageDesc.height) && (h_channels == d_imageDesc.channels))
	{
		return;
	}

	freeCudaBuffer(d_imageDesc);

	// Copy common data, data is wrong but will be corrected later
	d_imageDesc = h_imageDef;
	d_imageDesc.memoryType = cudaMemoryTypeDevice;

	auto sizeInBytes = h_width * h_channels * sizeof(Ipp16u);
	ThrowOnCudaError(cudaMallocPitch(&d_imageDesc.data, &d_imageDesc.bytePitch, sizeInBytes, d_imageDesc.height));
	d_imageDesc.texture = createTextureObject(d_imageDesc, textureReadMode);
}

void CudaNativeBase::allocCudaBufferIfNecessary(const ImageDesc<Ipp8u> &h_imageDesc, ImageDesc<Ipp8u> &d_imageDesc)
{
	auto h_width = h_imageDesc.width;
	auto h_height = h_imageDesc.height;
	auto h_channels = h_imageDesc.channels;

	// nothing to do if already the right size
	if ((h_width == d_imageDesc.width) && (h_height == d_imageDesc.height) && (h_channels == d_imageDesc.channels))
	{
		return;
	}

	freeCudaBuffer(d_imageDesc);

	// Copy common data, data is wrong but will be corrected later
	d_imageDesc = h_imageDesc;
	d_imageDesc.memoryType = cudaMemoryTypeDevice;

	const auto sizeInBytes = h_width * h_channels * sizeof(Ipp8u);
	const auto errorStatus = cudaMallocPitch(&d_imageDesc.data, &d_imageDesc.bytePitch, sizeInBytes, d_imageDesc.height);
	ThrowOnCudaError(errorStatus);
	d_imageDesc.texture = createTextureObject(d_imageDesc);
}

void CudaNativeBase::allocCudaBufferIfNecessary(const ImageDesc<Ipp32f> &h_imageDesc, ImageDesc<Ipp32f> &d_imageDesc, cudaTextureFilterMode filterModeType)
{
	auto h_width = h_imageDesc.width;
	auto h_height = h_imageDesc.height;
	auto h_channels = h_imageDesc.channels;

	// nothing to do if already the right size
	if ((h_width == d_imageDesc.width) && (h_height == d_imageDesc.height) && (h_channels == d_imageDesc.channels))
	{
		return;
	}

	if (h_imageDesc.memoryType != cudaMemoryTypeHost)
	{
		ThrowOnCudaError(cudaErrorHostMemoryNotRegistered);
	}

	freeCudaBuffer(d_imageDesc);

	// Copy common data, data is wrong but will be corrected later
	d_imageDesc = h_imageDesc;
	d_imageDesc.memoryType = cudaMemoryTypeDevice;

	auto sizeInBytes = h_width * h_channels * sizeof(Ipp32f);
	ThrowOnCudaError(cudaMallocPitch(&d_imageDesc.data, &d_imageDesc.bytePitch, sizeInBytes, d_imageDesc.height));
	d_imageDesc.texture = createTextureObject(d_imageDesc, filterModeType);
}

cudaTextureObject_t CudaNativeBase::createTextureObject(ImageDesc<Ipp16u>& d_imageDef, cudaTextureReadMode textureReadMode)
{
	cudaResourceDesc resDesc;
	memset(&resDesc, 0, sizeof(resDesc));
	resDesc.resType = cudaResourceTypePitch2D;
	resDesc.res.pitch2D.desc = cudaCreateChannelDesc<Ipp16u>();
	resDesc.res.pitch2D.devPtr = d_imageDef.data;
	resDesc.res.pitch2D.pitchInBytes = d_imageDef.bytePitch;
	resDesc.res.pitch2D.width = d_imageDef.width * d_imageDef.channels;
	resDesc.res.pitch2D.height = d_imageDef.height;

	cudaTextureDesc texDesc;
	memset(&texDesc, 0, sizeof(texDesc));
	
	// Normalized forces to linear
	texDesc.readMode = textureReadMode;
	texDesc.filterMode = textureReadMode == cudaTextureReadMode::cudaReadModeNormalizedFloat ? cudaTextureFilterMode::cudaFilterModeLinear : cudaTextureFilterMode::cudaFilterModePoint;

	cudaTextureObject_t surfaceTex = 0;
	ThrowOnCudaError(cudaCreateTextureObject(&surfaceTex, &resDesc, &texDesc, nullptr));

	return surfaceTex;
}

cudaTextureObject_t CudaNativeBase::createTextureObject(ImageDesc<float>& d_imageDef, cudaTextureFilterMode filterModeType)
{
	cudaResourceDesc resDesc;
	memset(&resDesc, 0, sizeof(resDesc));
	resDesc.resType = cudaResourceTypePitch2D;
	resDesc.res.pitch2D.desc = cudaCreateChannelDesc<float>();
	resDesc.res.pitch2D.devPtr = d_imageDef.data;
	resDesc.res.pitch2D.pitchInBytes = d_imageDef.bytePitch;
	resDesc.res.pitch2D.width = d_imageDef.width * d_imageDef.channels;
	resDesc.res.pitch2D.height = d_imageDef.height;

	cudaTextureDesc texDesc;
	memset(&texDesc, 0, sizeof(texDesc));
	texDesc.readMode = cudaReadModeElementType;
	texDesc.filterMode = filterModeType;
	cudaTextureObject_t surfaceTex = 0;
	ThrowOnCudaError(cudaCreateTextureObject(&surfaceTex, &resDesc, &texDesc, nullptr));

	return surfaceTex;
}

cudaTextureObject_t CudaNativeBase::createTextureObject(ImageDesc<Ipp8u>& d_imageDef, cudaTextureFilterMode filterModeType)
{
	cudaResourceDesc resDesc;
	memset(&resDesc, 0, sizeof(resDesc));
	resDesc.resType = cudaResourceTypePitch2D;
	resDesc.res.pitch2D.desc = cudaCreateChannelDesc<Ipp8u>();
	resDesc.res.pitch2D.devPtr = d_imageDef.data;
	resDesc.res.pitch2D.pitchInBytes = d_imageDef.bytePitch;
	resDesc.res.pitch2D.width = d_imageDef.width * d_imageDef.channels;
	resDesc.res.pitch2D.height = d_imageDef.height;

	cudaTextureDesc texDesc;
	memset(&texDesc, 0, sizeof(texDesc));
	texDesc.readMode = cudaReadModeNormalizedFloat;
	texDesc.filterMode = filterModeType;
	cudaTextureObject_t surfaceTex = 0;

	// If we have no size, don't bother because this creates an error
	if ((resDesc.res.pitch2D.pitchInBytes != 0) &&
		(resDesc.res.pitch2D.width != 0) &&
		(resDesc.res.pitch2D.height != 0) )
	{
		ThrowOnCudaError(cudaCreateTextureObject(&surfaceTex, &resDesc, &texDesc, nullptr));
	}
	
	return surfaceTex;
}

void CudaNativeBase::allocNormalizedCudaPlanesIfNecessary(const ImageDesc<Ipp16u> &d_imageDesc, ImageDesc<Ipp16u> d_planes[], int planes)
{
	auto desc = d_imageDesc;
	desc.channels = 1;
	for (auto i = 0; i < planes; i++)
	{
		allocCudaBufferIfNecessary(desc, d_planes[i], cudaTextureReadMode::cudaReadModeNormalizedFloat);
	}
}

void CudaNativeBase::copyOutputFromDeviceAsync(const IppiRect &deviceRoi, const IppiRect &hostRoi)
{
	// Copy only the ROI
	auto din = d_16uDesc[CudaArrayMnemonic::Output](deviceRoi);
	auto hout = h_16uDesc[CudaArrayMnemonic::Output](hostRoi);

	// Kernel is running under _kernelStream so this waits
	copyDeviceToHost2DAsync(din, hout, _defaultStream);
}
