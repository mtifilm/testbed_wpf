// CudaMtiCore.cpp : Defines the exported functions for the DLL application.
//

#include "MtiCudaCore.h"
#include "MtiCudaCoreSupport.h"
#include "cuda_runtime.h"
#include <cmath>

int MtiNativeCudaGetLastError()
{
	return cudaPeekAtLastError();
}

const char *MtiNativeCudaGetErrorCharPointer(int error)
{
	return cudaGetErrorString(cudaError_t(error));
}

int MtiNativeCudaGetDeviceName(int deviceIndex, char name[], int maxSize)
{
	// This is a little stupid, but expedient
	cudaDeviceProp prop;
	auto status = cudaGetDeviceProperties(&prop, deviceIndex);
	if (status != cudaSuccess)
	{
		name[0] = 0;
	}
	else
	{
		strcpy_s(name, maxSize, prop.name);
	}

	return int(status);
}

int MtiNativeCudaGetDeviceComputeCapability(int &major, int &minor, int deviceIndex)
{
	cudaDeviceProp prop;
	auto status = cudaGetDeviceProperties(&prop, deviceIndex);
	if (status == cudaSuccess)
	{
		major = prop.major;
		minor = prop.minor;
	}

	return int(status);
}

int MtiNativeCudaGetDeviceCount(int &count)
{
	return cudaGetDeviceCount(&count);
}

int MtiNativeCudaDriverGetVersion()
{
	int driverVersion = 0;
	cudaDriverGetVersion(&driverVersion);
	return driverVersion;
}

int MtiNativeCudaRuntimeGetVersion()
{
	int runtimeVersion = 0;
	cudaRuntimeGetVersion(&runtimeVersion);
	return runtimeVersion;
}

int MtiNativeCudatGetDeviceProp(cudaDeviceProp &prop, int deviceIndex)
{
	return cudaGetDeviceProperties(&prop, deviceIndex);
}

int MtiNativeCudaMallocHost(void **ptr, size_t bytes)
{
	return cudaMallocHost(ptr, bytes); // host pinned
}

int MtiNativeCudaFreeHost(void *ptr)
{
	return cudaFreeHost(ptr);
}

int MtiNativeCudaSetDevice(int deviceIndex)
{
	return cudaSetDevice(deviceIndex);
}
