// Since this is VC C++ compiler, we don't need guard code
#pragma once

#include "MtiCudaCore.h"
#include "MtiCudaCoreSupport.h"
#include "cuda_runtime.h"
#include <vector>
#include <map> 

// Maybe this should be templated
class CudaNativeBase
{
public:
	CudaNativeBase(int gpuIndex);
	virtual ~CudaNativeBase();

	void setMinMax(Ipp16u minValue, Ipp16u maxValue);
	Ipp16u getMinValue() const { return _minValue; }
	Ipp16u getMaxValue() const { return _maxValue; }

	void setInputArray(Ipp16u *source, int sourceBytePitch, int height, int width, int channels);
	void setOutputArray(Ipp16u *source, int sourceBytePitch, int height, int width, int channels);

	void setArray(const CudaArrayMnemonic mnemonic, Ipp16u * source, int sourceBytePitch, int height, int width, int channels);
	void setArray(const CudaArrayMnemonic mnemonic, Ipp32f *source, int sourceBytePitch, int height, int width, int channels);

	void loadArrayToDeviceAsync(const CudaArrayMnemonic mnemonic, Ipp16u *source, int sourceBytePitch);
//	void loadDeviceOutputArrayAsync(Ipp16u *source, int sourceBytePitch);

	void setMaskArray(Ipp8u *source, int sourceBytePitch, int height, int width, int channels);
	void setOutputRoi(const IppiRect &roi) { _outRoi = roi; }
	IppiRect getOutputRoi() const { return _outRoi; }
	void setInputRoi(const IppiRect &roi) { _inRoi = roi; }
	IppiRect getInputRoi() const { return _inRoi; }

	void copyOutputFromDeviceAsync(const IppiRect &deviceRoi, const IppiRect &hostRoi);
	void wait();

protected:
	int _gpuIndex = 0;
	cudaStream_t _defaultStream = nullptr;
	bool _ownsStream = false;

	// The h_imageDef is only used as a template, so data & pitch are ignored
	// Should these be templated???
	void allocCudaBufferIfNecessary(const ImageDesc<Ipp16u> &h_imageDesc, ImageDesc<Ipp16u> &d_imageDesc, cudaTextureReadMode textureReadMode = cudaTextureReadMode::cudaReadModeElementType);
	void allocCudaBufferIfNecessary(const ImageDesc<Ipp32f> &h_imageDesc, ImageDesc<Ipp32f> &d_imageDesc, cudaTextureFilterMode filterModeType = cudaTextureFilterMode::cudaFilterModePoint);
	void allocCudaBufferIfNecessary(const ImageDesc<Ipp8u> &h_imageDesc, ImageDesc<Ipp8u> &d_imageDesc);

	cudaTextureObject_t createTextureObject(ImageDesc<Ipp16u>& d_imageDef, cudaTextureReadMode textureReadMode = cudaTextureReadMode::cudaReadModeElementType);
	cudaTextureObject_t createTextureObject(ImageDesc<float>& d_imageDef, cudaTextureFilterMode filterModeType = cudaTextureFilterMode::cudaFilterModePoint);
	cudaTextureObject_t createTextureObject(ImageDesc<Ipp8u>& d_imageDef, cudaTextureFilterMode filterModeType = cudaTextureFilterMode::cudaFilterModePoint);

	// Note: source is first arg, dest is second, this is reverse of cuda but more left to right functional 
	template<class T>
	void copyHostToDevice2D(const ImageDesc<T> &h_imageDesc, const ImageDesc<T> &d_imageDesc)
	{
		if (h_imageDesc.data == nullptr)
		{
			return;
		}

		// Sanity check
		if ((h_imageDesc.memoryType != cudaMemoryTypeHost) || (d_imageDesc.memoryType != cudaMemoryTypeDevice))
		{
			throw cudaErrorInvalidMemcpyDirection;
		}

		auto combinedWidthInBytes = h_imageDesc.width * h_imageDesc.channels * sizeof(T);
		ThrowOnCudaError(cudaMemcpy2D(d_imageDesc.data, d_imageDesc.bytePitch, h_imageDesc.data, h_imageDesc.bytePitch, combinedWidthInBytes, h_imageDesc.height, cudaMemcpyHostToDevice));
	}

	template<class T>
	void copyHostToDevice2DAsync(const ImageDesc<T> &h_imageDesc, const ImageDesc<T> &d_imageDesc, cudaStream_t stream = 0)
	{
		if (h_imageDesc.data == nullptr)
		{
			return;
		}

		auto combinedWidthInBytes = h_imageDesc.width * h_imageDesc.channels * sizeof(T);
		if (stream == 0)
		{
			stream = _defaultStream;
		}

		ThrowOnCudaError(cudaMemcpy2DAsync(d_imageDesc.data, d_imageDesc.bytePitch, h_imageDesc.data, h_imageDesc.bytePitch, combinedWidthInBytes, h_imageDesc.height, cudaMemcpyHostToDevice, stream));
	}

	template<class T>
	void copyDeviceToDevice2DAsync(const ImageDesc<T> &d_inDesc, const ImageDesc<T> &d_outDesc, cudaStream_t stream = 0)
	{
		// We want the algorithms to support multiple methods and these may result in a noop
		if (d_inDesc.data == nullptr || d_outDesc.data == nullptr)
		{
			return;
		}

		auto combinedWidthInBytes = d_inDesc.width * d_inDesc.channels * sizeof(T);
		if (stream == 0)
		{
			stream = _defaultStream;
		}

		ThrowOnCudaError(cudaMemcpy2DAsync(d_outDesc.data, d_outDesc.bytePitch, d_inDesc.data, d_inDesc.bytePitch, combinedWidthInBytes, d_inDesc.height, cudaMemcpyDeviceToDevice, stream));
	}

	// Note: source is first arg, dest is second, this is reverse of cuda but more left to right functional 
	template<class T>
	void copyDeviceToHost2D(const ImageDesc<T> &d_imageDesc, const ImageDesc<T> &h_imageDesc)
	{
		if (d_imageDesc.data == nullptr)
		{
			return;
		}

		auto combinedWidthInBytes = h_imageDesc.width * h_imageDesc.channels * sizeof(T);
		ThrowOnCudaError(cudaMemcpy2D(h_imageDesc.data, h_imageDesc.bytePitch, d_imageDesc.data, d_imageDesc.bytePitch, combinedWidthInBytes, d_imageDesc.height, cudaMemcpyDeviceToHost));
	}

	template<class T>
	void copyDeviceToHost2DAsync(const ImageDesc<T> &d_imageDesc, const ImageDesc<T> &h_imageDesc, cudaStream_t stream = 0)
	{
		if (d_imageDesc.data == nullptr)
		{
			return;
		}

		auto combinedWidthInBytes = h_imageDesc.width * h_imageDesc.channels * sizeof(T);
		if (stream == 0)
		{
			stream = _defaultStream;
		}

		ThrowOnCudaError(cudaMemcpy2DAsync(h_imageDesc.data, h_imageDesc.bytePitch, d_imageDesc.data, d_imageDesc.bytePitch, combinedWidthInBytes, d_imageDesc.height, cudaMemcpyDeviceToHost, stream));
	}

	template<class T>
	void freeCudaBuffer(ImageDesc<T> &d_imageDesc)
	{
		if (d_imageDesc.data == nullptr)
		{
			return;
		}

		if (d_imageDesc.texture != 0)
		{
			ThrowOnCudaError(cudaDestroyTextureObject(d_imageDesc.texture));
		}

		ThrowOnCudaError(cudaFree(d_imageDesc.data));

		// Reset to our default pointer
		d_imageDesc = { nullptr, 0, 0, 0, 0, 0, cudaMemoryTypeHost };
	}

	void allocNormalizedCudaPlanesIfNecessary(const ImageDesc<Ipp16u> &d_imageDesc, ImageDesc<Ipp16u> d_planes[], int planes);

	// The derived class can decide how to use these 
	//ImageDesc<Ipp16u> h16u_inDesc;
	//ImageDesc<Ipp16u> d_inImage;
	//ImageDesc<Ipp16u> h_outImage;
	//ImageDesc<Ipp16u> d_outImage;
	//ImageDesc<Ipp16u> h_ancImage;
	//ImageDesc<Ipp16u> d_ancImage;

	ImageDesc<Ipp8u> h_maskImage;
	ImageDesc<Ipp8u> d_maskImage;

	// Dynamic 
	std::map<CudaArrayMnemonic, ImageDesc<Ipp32f>> h_32fDesc;
	std::map<CudaArrayMnemonic, ImageDesc<Ipp32f>> d_32fDesc;

	std::map<CudaArrayMnemonic, ImageDesc<Ipp16u>> h_16uDesc;
	std::map<CudaArrayMnemonic, ImageDesc<Ipp16u>> d_16uDesc;

private:

	Ipp16u _minValue = 0;
	Ipp16u _maxValue = 0xFFFF;
	IppiRect _outRoi = { 0,0,0,0 };
	IppiRect _inRoi = { 0,0,0,0 };
};

