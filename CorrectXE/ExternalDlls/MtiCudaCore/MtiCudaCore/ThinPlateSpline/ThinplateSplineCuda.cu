#include "..\MtiCudaCore.h"
#include "..\MtiCudaCoreSupport.h"
#include "ippcore.h"
#include <iostream>
#include <stdio.h>
#include <sstream>
#include <cmath>

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define BLOCKSIZE_x 16
#define BLOCKSIZE_y 16

/*******************/
/* iDivUp FUNCTION */
/*******************/
extern int iDivUp(int hostPtr, int b); //{ return ((hostPtr % b) != 0) ? (hostPtr / b + 1) : (hostPtr / b); }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

////__device__ __inline__ int repack_rgb(int R, int G, int B)
////{
////    struct
////    {
////        char rP[4];
////    }srP;
////    unsigned int *rePacked = (unsigned int*)&srP;
////
////    srP.rP[0] = ((int)R >> 2);
////    srP.rP[1] = ((R & 0x03) << 6) + (G >> 4);
////    srP.rP[2] = ((G & 0x0F) << 4) + (B >> 6);
////    srP.rP[3] = ((B & 0x3F) << 2);
////
////    return *rePacked; 
////}

//------------------------------------------------------------------
//
// DO NOT CHANGE THIS, this MUST be the same AS CPU code that computes weights
// 
__device__ __inline__ float distance(float2 p, int2 q)
{
	float diffx = p.x - q.x;
	float diffy = p.y - q.y;
    float r = sqrtf((diffx * diffx) + (diffy * diffy));
    if (r == 0)
    {
        return 0;
    }

    return r * logf(r);
}

//------------------------------------------------------------------

__global__ void createSurfaceKernelDownSample
(
	int imageWidth,
	int imageHeight,
	int channels,
	//cudaTextureObject_t weightsTex,
	float *weights,
	int weightsPitchBytes,
	const float2 *d_ipControlPoints,
	int pointCount,
	float *surface0,
	float *surface1,
	float *surface2,
	int downSurfacePitch,
	unsigned int channelMask,
	int downSample,
	Ipp16u minValue,
	Ipp16u maxValue
	)
{
	int tidx = threadIdx.x + (blockIdx.x * blockDim.x);
    int tidy = threadIdx.y + (blockIdx.y * blockDim.y);

	int width = imageWidth / downSample;
	int height = imageHeight / downSample;

	if ((tidx >= width) || (tidy >= height))
	{
		return;
	}

	int tidx_orig = downSample * tidx;                      // Actual position on original image
	int tidy_orig = downSample * tidy;

	// At most we are going to process 3 channels
	float nonRigid[3] = {0, 0, 0};
	int weightsPitch = weightsPitchBytes/sizeof(float);

	// We process every down sampled point and create three planes
	for (int i = 0; i < pointCount; i++)
	{
		// the distance from this image point to each of the control 
		float d = distance(d_ipControlPoints[i], make_int2(tidx_orig, tidy_orig));
		nonRigid[0] += weights[i] * d;

		// Process channels
		if (channels >= 2)
		{
			nonRigid[1] += weights[i  + weightsPitch] * d ; 
			if (channels == 3)
			{
   				nonRigid[2] += weights[i + 2 * weightsPitch] * d;
			}
		}
	}

	// Create the output surfaces
	// Notice mask must match channel number so, channel == 1 => mask == 1;
	int dstIndex = tidx + (downSurfacePitch /sizeof(float)) * tidy;  

	float affine;
	if ((channelMask & 1) != 0)
	{
		affine = weights[pointCount] + weights[pointCount+1] * tidx_orig + weights[pointCount+2] * tidy_orig;
		surface0[dstIndex] =  maxValue * (affine + nonRigid[0]);
	}

	if ((channelMask & 2) != 0)
	{
		auto startRow1 = pointCount + weightsPitch; 
		affine = weights[startRow1] + weights[startRow1 +1] * tidx_orig + weights[startRow1+2] * tidy_orig;
		surface1[dstIndex] = maxValue * (affine + nonRigid[1]);
	}

	if ((channelMask & 4) != 0)
	{
		auto startRow2 = pointCount + 2*weightsPitch; 
		affine = weights[startRow2] + weights[startRow2+1] * tidx_orig + weights[startRow2+2] * tidy_orig;
		surface2[dstIndex] =  maxValue * (affine + nonRigid[2]);
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////

__global__ void addSurfaceKernelDownSample
(
	int imageWidth,
	int imageHeight,
	int channels,
	cudaTextureObject_t imageTex,
	Ipp16u *outImage,
	int outPitch,
	cudaTextureObject_t surface0,
	cudaTextureObject_t surface1,
	cudaTextureObject_t surface2,
	unsigned int channelMask,
	cudaTextureObject_t maskTex,
	int downSample,
	Ipp16u minValue,
	Ipp16u maxValue
	)
{
	int tidx = threadIdx.x + (blockIdx.x * blockDim.x);
    int tidy = threadIdx.y + (blockIdx.y * blockDim.y);

	if ((tidx >= imageWidth) || (tidy >= imageHeight))
	{
		return;
	}

	auto tidxDs = tidx / downSample;
	auto tidyDs = tidy / downSample;

	float tidxOffset = tidx % downSample / float(downSample);
	float tidyOffset = tidy % downSample / float(downSample);

	// Points to start channel of surface
	int dstIndex = (channels*tidx) + (outPitch /sizeof(Ipp16u)) * tidy; 
	
	// Use mask if one is defined
	float maskValue = maskTex == 0 ? 1.0f : tex2D<float>(maskTex, tidx, tidy);

	// R Channel
	float v = tex2D<Ipp16u>(imageTex, channels*tidx + 0, tidy);
	float maxValuef = float(maxValue);
	float minValuef = float(minValue);
	if ((channelMask & 1) != 0)
	{
		v += maskValue * tex2D<float>(surface0, tidxDs + tidxOffset + 0.5f, tidyDs + tidyOffset + 0.5f);
		v = max(minValuef, min(v, maxValuef));
	}

	outImage[dstIndex + 0] = int(v);

	// G Channel
	v = tex2D<Ipp16u>(imageTex, channels*tidx + 1, tidy);
	if ((channelMask & 2) != 0)
	{
		v += maskValue * tex2D<float>(surface1, tidxDs + tidxOffset + 0.5f, tidyDs + tidyOffset + 0.5f);
		v = max(minValuef, min(v, maxValuef));
	}

	outImage[dstIndex + 1] = int(v);

	// B channel
	v = tex2D<Ipp16u>(imageTex, channels*tidx + 2, tidy);
	if ((channelMask & 4) != 0)
	{
		v += maskValue * tex2D<float>(surface2, tidxDs + tidxOffset + 0.5f, tidyDs + tidyOffset + 0.5f);
		v = max(minValuef, min(v, maxValuef));
	}

	outImage[dstIndex + 2] = int(v);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

cudaError_t createThinplateSplineSurfaceDownSample
(
	ImageDesc<Ipp16u> &d_inArray,
	ImageDesc<float> &d_weights,
	ImageDesc<float> d_downSurface[3],
	const float2 *d_controlPoints,
	int pointCount,
	ImageDesc<Ipp16u> &d_outArray,
	ImageDesc<Ipp8u> &d_maskArray,
	unsigned int channelMask,
	int downSample,
	Ipp16u minValue,
	Ipp16u maxValue,
	cudaStream_t stream,
	cudaStream_t kernelStream
	)
{
	auto width = d_inArray.width / downSample;
	auto height = d_inArray.height / downSample;

	// Step one is to create the correction surfaces
	dim3 gridSize(iDivUp(width, BLOCKSIZE_x), iDivUp(height, BLOCKSIZE_y));
	dim3 blockSize(BLOCKSIZE_x, BLOCKSIZE_y);

	if (kernelStream == 0)
	{
		kernelStream = stream;
	}

	// Step one is to create the correction surfaces
	createSurfaceKernelDownSample << <gridSize, blockSize, 0, kernelStream>> >
	(
		d_inArray.width,
		d_inArray.height,
		d_inArray.channels,
		d_weights.data,
		d_weights.bytePitch,
		d_controlPoints,
		pointCount,
		d_downSurface[0].data,
		d_downSurface[1].data,
		d_downSurface[2].data,
		d_downSurface[0].bytePitch,
		channelMask,
		downSample,
		minValue,
		maxValue
	); 

	// Step two is to add surfaces to image
	// Kernal calls are for each pixel
	// The input and output are assumed to be interleaved
	dim3 gridSize2(iDivUp(d_inArray.width, BLOCKSIZE_x), iDivUp(d_inArray.height, BLOCKSIZE_y));
	dim3 blockSize2(BLOCKSIZE_x, BLOCKSIZE_y);

	// Wait for IO to finish
	cudaStreamSynchronize(stream);

	addSurfaceKernelDownSample << <gridSize2, blockSize2, 0, kernelStream>> >
	(
		d_inArray.width,
		d_inArray.height,
		d_inArray.channels,
		d_inArray.texture,
		d_outArray.data,
		d_outArray.bytePitch,
		d_downSurface[0].texture,
		d_downSurface[1].texture,
		d_downSurface[2].texture,
		channelMask,
		d_maskArray.texture,
		downSample,
		minValue,
		maxValue
	);

    return cudaSuccess;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

cudaError_t createWarpLocations
(
	ImageDesc<Ipp16u> &d_inArray,
	ImageDesc<float> &d_weights,
	ImageDesc<float> d_downSurface[],
	const float2 *d_controlPoints,
	int pointCount,
	ImageDesc<Ipp16u> &d_outArray,
	ImageDesc<Ipp8u> &d_maskArray,
	int downSample,
	Ipp16u minValue,
	Ipp16u maxValue,
	cudaStream_t stream
	)
{
	auto width = d_inArray.width / downSample;
	auto height = d_inArray.height / downSample;

	// Step one is to create the correction surfaces
	dim3 gridSize(iDivUp(width, BLOCKSIZE_x), iDivUp(height, BLOCKSIZE_y));
	dim3 blockSize(BLOCKSIZE_x, BLOCKSIZE_y);

	// Step one is to create the correction surfaces
	// Note, there are two warps, x and y

	createSurfaceKernelDownSample << <gridSize, blockSize, 0, stream>> >
	(
		d_inArray.width,
		d_inArray.height,
		2,
		d_weights.data,
		d_weights.bytePitch,
		d_controlPoints,
		pointCount,
		d_downSurface[0].data,
		d_downSurface[1].data,
		nullptr,
		d_downSurface[0].bytePitch,
		0x3,
		downSample,
		0,
		1
	); 

    return cudaSuccess;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

__global__ void createPlanerKernel
(
	int imageWidth,
	int imageHeight,
	int channels,
	cudaTextureObject_t imageTex,
	Ipp16u *plane0,
	Ipp16u *plane1,
	Ipp16u *plane2,
	int planePitch
)
{
	int tidx = threadIdx.x + (blockIdx.x * blockDim.x);
    int tidy = threadIdx.y + (blockIdx.y * blockDim.y);

	if ((tidx >= imageWidth) || (tidy >= imageHeight))
	{
		return;
	}

	int tidx3 = 3 * tidx;
	int dstIndex = tidx + (planePitch/sizeof(Ipp16u)) * tidy; 

	plane0[dstIndex] = tex2D<Ipp16u>(imageTex, tidx3 + 0, tidy);
	plane1[dstIndex] = tex2D<Ipp16u>(imageTex, tidx3 + 1, tidy);
	plane2[dstIndex] = tex2D<Ipp16u>(imageTex, tidx3 + 2, tidy);
}

//-------------------------------------------------------------------------------------------------------

cudaError_t createPlaner
(
	ImageDesc<Ipp16u> &d_inArray,
	ImageDesc<Ipp16u> d_planes[],
	cudaStream_t stream = nullptr
)
{
	dim3 gridSize(iDivUp(d_inArray.width, BLOCKSIZE_x), iDivUp(d_inArray.height, BLOCKSIZE_y));
	dim3 blockSize(BLOCKSIZE_x, BLOCKSIZE_y);

	createPlanerKernel << <gridSize, blockSize, 0, stream>> >
	(
		d_inArray.width,
		d_inArray.height,
		d_inArray.channels,
		d_inArray.texture,
		d_planes[0].data,
		d_planes[1].data,
		d_planes[2].data,
		d_planes[0].bytePitch
	);

    return cudaSuccess;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

__global__ void warpImageKernel
(
	int imageWidth,
	int imageHeight,
	Ipp16u *imageData,
	int imagePitch,
	cudaTextureObject_t downTexX,
	cudaTextureObject_t downTexY,
	cudaTextureObject_t planeTex0,
	cudaTextureObject_t planeTex1,
	cudaTextureObject_t planeTex2,
	int fill
)
{
	int tidx = threadIdx.x + (blockIdx.x * blockDim.x);
    int tidy = threadIdx.y + (blockIdx.y * blockDim.y);

	if ((tidx >= imageWidth) || (tidy >= imageHeight))
	{
		return;
	}

	int tidx3 = 3 * tidx;
	int dstIndex = tidx3 + (imagePitch/sizeof(Ipp16u)) * tidy;

	// Note: these textures all are linear interpolated hence the +0.5 are needed
	float x = tex2D<float>(downTexX, tidx + 0.5f, tidy + 0.5f) + 0.5f;
	float y = tex2D<float>(downTexY, tidx + 0.5f, tidy + 0.5f) + 0.5f;

	if ((x <= -1) || x >= (imageWidth + 1) || (y <= -1) || y >= (imageHeight + 1))
	{
		if (fill >= 0)
		{
			imageData[dstIndex + 0] = fill;
			imageData[dstIndex + 1] = fill;
			imageData[dstIndex + 2] = fill;
		}

		return;
	}

	imageData[dstIndex + 0] = tex2D<float>(planeTex0, x, y) * 65535;
	imageData[dstIndex + 1] = tex2D<float>(planeTex1, x, y) * 65535;
	imageData[dstIndex + 2] = tex2D<float>(planeTex2, x, y) * 65535;
}

//-------------------------------------------------------------------------------------------------------

cudaError_t warpImage
(
	ImageDesc<float> d_downSurfaces[],
	ImageDesc<Ipp16u> d_planes[],
	ImageDesc<Ipp16u> &d_outArray,
	int fill,
	cudaStream_t stream = nullptr
)
{
	dim3 gridSize(iDivUp(d_outArray.width, BLOCKSIZE_x), iDivUp(d_outArray.height, BLOCKSIZE_y));
	dim3 blockSize(BLOCKSIZE_x, BLOCKSIZE_y);

	warpImageKernel << <gridSize, blockSize, 0, stream>> >
	(
		d_outArray.width,
		d_outArray.height,
		d_outArray.data,
		d_outArray.bytePitch,
		d_downSurfaces[0].texture,
		d_downSurfaces[1].texture,
		d_planes[0].texture,
		d_planes[1].texture,
		d_planes[2].texture,
		fill
	);

    return cudaSuccess;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define M_PI 3.14159265358979323846264338327950288f

__host__ __device__ static float sinc(float x)
{
	x = (x * M_PI);

	if ((x < 0.01f) && (x > -0.01f))
		return 1.0f + x * x*(-1.0f / 6.0f + x * x*1.0f / 120.0f);

	return sin(x) / x;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

// This does the x interpolation
//  The three input planes (planeTex<n>) are warped by downTex<X,Y> to produce new plane data outPlane<n>
__global__ void warpLanczosImageKernel_x
(
	int imageWidth,
	int imageHeight,
	float *outPlane0,
	float *outPlane1,
	float *outPlane2,
	int planesPitch,
	cudaTextureObject_t downTexX,
	cudaTextureObject_t downTexY,
	cudaTextureObject_t planeTex0,
	cudaTextureObject_t planeTex1,
	cudaTextureObject_t planeTex2
)
{
	int tidx = threadIdx.x + (blockIdx.x * blockDim.x);
    int tidy = threadIdx.y + (blockIdx.y * blockDim.y);

	if ((tidx >= imageWidth) || (tidy >= imageHeight))
	{
		return;
	}

	int dstIndex = tidx + (planesPitch/sizeof(float)) * tidy;

	// Note: these textures all are linear interpolated hence the +0.5 are needed
	float x = tex2D<float>(downTexX, tidx + 0.5f, tidy + 0.5f) + 0.5f;
	float y = tex2D<float>(downTexY, tidx + 0.5f, tidy + 0.5f) + 0.5f;

	if ((x <= -1) || x >= (imageWidth + 1) || (y <= -1) || y >= (imageHeight + 1))
	{
		return;
	}

	float xi;
	float yi;
	float deltaX = modf (x, &xi);

	modf (y, &yi);

	auto sum0 = 0.0f;
	auto sum1 = 0.0f;
	auto sum2 = 0.0f;
	auto wx = 0.0f;

   for (auto i = -3; i <= 3; i++)
   {
		float sc = sinc(i + deltaX);
		sum0 += tex2D<float>(planeTex0, xi+i + 0.5f, yi + 0.5f) * sc;
		sum1 += tex2D<float>(planeTex1, xi+i + 0.5f, yi + 0.5f) * sc;
		sum2 += tex2D<float>(planeTex2, xi+i + 0.5f, yi + 0.5f) * sc;		
		wx += sc;
   }
   
   // The input is normalized float
	outPlane0[dstIndex] = 65535*sum0/wx;
	outPlane1[dstIndex] = 65535*sum1/wx;
	outPlane2[dstIndex] = 65535*sum2/wx;
}

//-------------------------------------------------------------------------------------------------------

__global__ void warpLanczosImageKernel_y
(
	int imageWidth,
	int imageHeight,
	Ipp16u *outImageData,
	int outImagePitch,
	cudaTextureObject_t downTexX,
	cudaTextureObject_t downTexY,
	cudaTextureObject_t planeTex0,
	cudaTextureObject_t planeTex1,
	cudaTextureObject_t planeTex2,
	Ipp16u minValue,
	Ipp16u maxValue,
	int fill
)
{
	int tidx = threadIdx.x + (blockIdx.x * blockDim.x);
    int tidy = threadIdx.y + (blockIdx.y * blockDim.y);

	if ((tidx >= imageWidth) || (tidy >= imageHeight))
	{
		return;
	}

	int tidx3 = 3 * tidx;
	int dstIndex = tidx3 + (outImagePitch/sizeof(Ipp16u)) * tidy;

	// Note: these textures all are linear interpolated hence the +0.5 are needed
	float x = tex2D<float>(downTexX, tidx + 0.5f, tidy + 0.5f) + 0.5f;
	float y = tex2D<float>(downTexY, tidx + 0.5f, tidy + 0.5f) + 0.5f;

	if ((x <= -1) || x >= (imageWidth + 1) || (y <= -1) || y >= (imageHeight + 1))
	{
		if (fill >= 0)
		{
			outImageData[dstIndex + 0] = fill;
			outImageData[dstIndex + 1] = fill;
			outImageData[dstIndex + 2] = fill;
		}

		return;
	}

	float yi;
	float deltaY = modf (y, &yi);

	auto sum0 = 0.0f;
	auto sum1 = 0.0f;
	auto sum2 = 0.0f;
	auto wx = 0.0f;

   for (auto i = -3; i <= 3; i++)
   {
		float sc = sinc(i + deltaY);
		sum0 += tex2D<float>(planeTex0, tidx, tidy + i) * sc;
		sum1 += tex2D<float>(planeTex1, tidx, tidy + i) * sc;
		sum2 += tex2D<float>(planeTex2, tidx, tidy + i) * sc;		
		wx += sc;
   }

   outImageData[dstIndex + 0] = min(maxValue, max(minValue, Ipp16u(sum0/wx)));	
   outImageData[dstIndex + 1] = min(maxValue, max(minValue, Ipp16u(sum1/wx)));	
   outImageData[dstIndex + 2] = min(maxValue, max(minValue, Ipp16u(sum2/wx)));	
}

//-------------------------------------------------------------------------------------------------------
cudaError_t warpLanczosImage
(
	ImageDesc<float> d_downSurfaces[],
	ImageDesc<Ipp16u> d16u_planes[],
	ImageDesc<Ipp16u> &d_outArray,
	ImageDesc<float> d_scratchArrays[],
	Ipp16u minValue,
	Ipp16u maxValue,
	int fill,
	cudaStream_t stream = nullptr
)
{
	dim3 gridSize(iDivUp(d_outArray.width, BLOCKSIZE_x), iDivUp(d_outArray.height, BLOCKSIZE_y));
	dim3 blockSize(BLOCKSIZE_x, BLOCKSIZE_y);

	warpLanczosImageKernel_x << <gridSize, blockSize, 0, stream>> >
	(
		d_scratchArrays[0].width,
		d_scratchArrays[0].height,
		d_scratchArrays[0].data,
		d_scratchArrays[1].data,
		d_scratchArrays[2].data,
		d_scratchArrays[0].bytePitch,
		d_downSurfaces[0].texture,
		d_downSurfaces[1].texture,
		d16u_planes[0].texture,
		d16u_planes[1].texture,
		d16u_planes[2].texture
	);

	warpLanczosImageKernel_y << <gridSize, blockSize, 0, stream>> >
	(
		d_outArray.width,
		d_outArray.height,
		d_outArray.data,
		d_outArray.bytePitch,
		d_downSurfaces[0].texture,
		d_downSurfaces[1].texture,
		d_scratchArrays[0].texture,
		d_scratchArrays[1].texture,
		d_scratchArrays[2].texture,
		minValue,
		maxValue,
		fill
	);

    return cudaSuccess;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

