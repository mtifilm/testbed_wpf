#pragma once
#ifndef CudaNativeThinplateSpline16uH
#define CudaNativeThinplateSpline16uH

// This is the master file that calls this one, but that is ok
#include "..\MtiCudaCore.h"
#include "..\CudaNativeBase.h"

class CudaNativeThinplateSpline16u : public CudaNativeBase
{
public:
	CudaNativeThinplateSpline16u(int gpuIndex);
	~CudaNativeThinplateSpline16u();
	void warpIntensity(int numberOfControlPoints, int numberOfChannels, const float *weights, const IppiPoint_32f *controlPoints, unsigned int channelMask, int downSample);
	void warpIntensityAsync(int numberOfControlPoints, int numberOfChannels, const float *weights, const IppiPoint_32f *controlPoints, unsigned int channelMask, int downSample);
	void warpIntensityCopyResultAsync();

	void warpLocationAsync(int numberOfControlPoints, const float *weights, const IppiPoint_32f *controlPoints, int fill, int downSample);
	void warpLocationCopyResultAsync();

	void warpLocation(int numberOfControlPoints, const float *weights, const IppiPoint_32f *controlPoints, int fill, int downSample)
	{
		warpLocationAsync(numberOfControlPoints, weights, controlPoints, fill, downSample);
		warpLocationCopyResultAsync();
		wait();
	}

private:
	// Do we want to get rid of these and use N x 2 descriptor?
	float *_d_controlPoints = nullptr;
	float *_h_controlPoints = nullptr;
	int	_numberOfControlPoints = 0;
	int _numberOfChannels = 0;
	int	_downSample = 0;
	cudaStream_t _kernelStream = 0;

	ImageDesc<float> h_weightsDesc;
	ImageDesc<float> d_weightsDesc;

	// We use h_downSurface as a template for the d_downSurfaces
	// so we only need one h, for the three real d_downsurfaces
	ImageDesc<float> h32f_downSurface;
	ImageDesc<float> d32f_downSurfaces[3];

	// This is for downsample 1 and lanczos
	ImageDesc<float> d32f_scratchPlanes[3];

	// Decomposing the input into planer
	ImageDesc<Ipp16u> d16u_planes[3];

	void freeStorage();
	void allocIfNecessary(int numberOfControlPoints, int numberOfChannels, int downSample, const ImageDesc<Ipp16u> &h_desc);
};

#endif

