#include "CudaNativeThinplateSpline16u.h"
#include "..\MtiCudaCoreSupport.h"
#include <stdio.h>

//#include "DevIo.h"
#include <vector>
#include <time.h>
#include <sstream>
#include <iostream>
#include <string>

CudaNativeThinplateSpline16u::CudaNativeThinplateSpline16u(int gpuIndex) : CudaNativeBase(gpuIndex)
{
	ThrowOnCudaError(cudaStreamCreate(&_kernelStream));
}


CudaNativeThinplateSpline16u::~CudaNativeThinplateSpline16u()
{
	cudaStreamDestroy(_kernelStream);
	_kernelStream = 0;
	freeStorage();
}

extern cudaError_t createThinplateSplineSurfaceDownSample
(
	ImageDesc<Ipp16u> &d_inArray,
	ImageDesc<float> &d_weights,
	ImageDesc<float> d_downSurface[3],
	const float2 *d_ipControlPoints,
	int pointCount,
	ImageDesc<Ipp16u> &d_outArray,
	ImageDesc<Ipp8u> &d_maskArray,
	unsigned int channelMask,
	int downSample,
	Ipp16u minValue,
	Ipp16u maxValue,
	cudaStream_t stream = nullptr,
	cudaStream_t kernelStream = nullptr
);

extern cudaError_t createWarpLocations
(
	ImageDesc<Ipp16u> &d_inArray,
	ImageDesc<float> &d_weights,
	ImageDesc<float> d_downSurface[],
	const float2 *d_controlPoints,
	int pointCount,
	ImageDesc<Ipp16u> &d_outArray,
	ImageDesc<Ipp8u> &d_maskArray,
	int downSample,
	Ipp16u minValue,
	Ipp16u maxValue,
	cudaStream_t stream = nullptr
);

extern cudaError_t createPlaner
(
	ImageDesc<Ipp16u> &d_inArray,
	ImageDesc<Ipp16u> d_plane[],
	cudaStream_t stream = nullptr
);

extern cudaError_t warpImage
(
	ImageDesc<float> d_downSurfaces[],
	ImageDesc<Ipp16u> d_planes[],
	ImageDesc<Ipp16u> &d_outArray,
	int fill,
	cudaStream_t stream = nullptr
);

extern cudaError_t warpLanczosImage
(
	ImageDesc<float> d_downSurfaces[],
	ImageDesc<Ipp16u> d_planes[],
	ImageDesc<Ipp16u> &d_outArray,
	ImageDesc<float> d_scratchPlanes[],
	Ipp16u minValue,
	Ipp16u maxValue,
	int fill,
	cudaStream_t stream = nullptr
);

void CudaNativeThinplateSpline16u::freeStorage()
{
	// Scratch buffer is freed in parent class

	_numberOfControlPoints = 0;
	ThrowOnCudaError(cudaFree(_d_controlPoints));
	_d_controlPoints = nullptr;

	ThrowOnCudaError(cudaFreeHost(_h_controlPoints));
	_h_controlPoints = nullptr;

	// get rid of any planes or tps surfaces
	for (auto ch = 0; ch < 3; ch++)
	{
		freeCudaBuffer(d32f_downSurfaces[ch]);
		freeCudaBuffer(d32f_scratchPlanes[ch]);
		freeCudaBuffer(d16u_planes[ch]);
	}

	//  Normally h descriptors don't manage data
	// We should use CUDA arrays
	cudaFreeHost(h_weightsDesc.data);
	h_weightsDesc.data = 0;
	freeCudaBuffer(d_weightsDesc);
}

void CudaNativeThinplateSpline16u::allocIfNecessary(int numberOfControlPoints, int numberOfChannels, int downSample, const ImageDesc<Ipp16u> &h_desc)
{
	if ((numberOfControlPoints == _numberOfControlPoints) && (numberOfChannels == _numberOfChannels) && downSample == _downSample)
	{
		return;
	}

	freeStorage();

	// If lanczos, create a scratch buffer
	setArray(CudaArrayMnemonic::Scratch, (Ipp32f *)nullptr, 0, h_desc.height, h_desc.width, numberOfChannels);

	// create the 1 channel 2d weight array  Weights x3 
	// Logically should be a Wx1xC array, but that is too complex, so do this
	h_weightsDesc.width = numberOfControlPoints + 3;
	h_weightsDesc.height = numberOfChannels;
	h_weightsDesc.bytePitch = h_weightsDesc.width * sizeof(float);
	h_weightsDesc.channels = 1;
	cudaMallocHost(&h_weightsDesc.data, h_weightsDesc.width *h_weightsDesc.height * sizeof(float));

	allocCudaBufferIfNecessary(h_weightsDesc, d_weightsDesc);

	// To do, convert to a descriptor
	ThrowOnCudaError(cudaMalloc(&_d_controlPoints, numberOfControlPoints * sizeof(float2)));
	ThrowOnCudaError(cudaMallocHost(&_h_controlPoints, numberOfControlPoints *  sizeof(float2)));

	// FreeStorage sets all values to 0 (undefined)
	h32f_downSurface.width = h_desc.width / downSample;
	h32f_downSurface.height = h_desc.height / downSample;
	h32f_downSurface.channels = 1;
	h32f_downSurface.bytePitch = h32f_downSurface.width * sizeof(float);

	for (auto ch = 0; ch < numberOfChannels; ch++)
	{
		allocCudaBufferIfNecessary(h32f_downSurface, d32f_downSurfaces[ch], cudaTextureFilterMode::cudaFilterModeLinear);
	}

	// May be faster to do this interleaved
	for (auto ch = 0; ch < 3; ch++)
	{
		allocCudaBufferIfNecessary(h32f_downSurface, d32f_scratchPlanes[ch], cudaTextureFilterMode::cudaFilterModePoint);
	}

	_numberOfControlPoints = numberOfControlPoints;
	_numberOfChannels = numberOfChannels;
	_downSample = downSample;
}

void CudaNativeThinplateSpline16u::warpIntensity(int numberOfControlPoints, int numberOfChannels, const float *weights, const IppiPoint_32f *controlPoints, unsigned int channelMask, int downSample)
{
	downSample = 4;

	auto &h_inImage = h_16uDesc[CudaArrayMnemonic::Input];
	auto &d_inImage = d_16uDesc[CudaArrayMnemonic::Input];

	auto &h_outImage = h_16uDesc[CudaArrayMnemonic::Output];
	auto &d_outImage = d_16uDesc[CudaArrayMnemonic::Output];

	// Sanity check, the outArray must be defined before hand and of correct size
	if (numberOfChannels != d_outImage.channels)
	{
		ThrowOnCudaError(cudaErrorInvalidValue);
	}

	// This allocates everything if something has changed, the h_inImage is just a template
	allocIfNecessary(numberOfControlPoints, numberOfChannels, downSample, h_inImage);

	// Copy control points, weights, need this before kernel starts
	memcpy(_h_controlPoints, controlPoints, _numberOfControlPoints * 2 * sizeof(int));
	ThrowOnCudaError(cudaMemcpyAsync(_d_controlPoints, _h_controlPoints, _numberOfControlPoints * 2 * sizeof(int), cudaMemcpyHostToDevice));
//	ThrowOnCudaError(cudaMemcpy(_d_controlPoints, controlPoints, _numberOfControlPoints * sizeof(float2), cudaMemcpyHostToDevice));
	
	// Need to put it in aligned memory
	memcpy(h_weightsDesc.data, weights, h_weightsDesc.width *h_weightsDesc.height * sizeof(float));
	copyHostToDevice2DAsync(h_weightsDesc, d_weightsDesc);

	// Copy input image
	copyHostToDevice2DAsync(h_inImage, d_inImage);

	// Copy mask
	copyHostToDevice2DAsync(h_maskImage, d_maskImage);

	createThinplateSplineSurfaceDownSample(
		d_inImage,
		d_weightsDesc,
		d32f_downSurfaces,
		(float2 *)_d_controlPoints,
		_numberOfControlPoints,
		d_outImage,
		d_maskImage,
		channelMask,
		downSample,
		getMinValue(),
		getMaxValue(),
		_defaultStream,
		_defaultStream  //was _kernelStream
	);

	// Copy only the ROI
	auto dout = d_outImage(getInputRoi());
	auto hout = h_outImage(getOutputRoi());
	
	// Kernel is running under _kernelStream so this waits
	//string origfileName = "C:\\temp\\dllOriginal.mbin";
	//Ipp16uArray input(h_inImage.height, h_inImage.width, 3, h_inImage.data);
	//auto bitDepth = 10;
	//DevIO::dumpMatlabBinary(input, bitDepth, origfileName);
	
	ThrowOnCudaError(cudaStreamSynchronize(_kernelStream));
	copyDeviceToHost2D(dout, hout);

	//Ipp16uArray ds;
	//Ipp32fArray df(h_inImage.height, h_inImage.width, 1, h_downSurface.data);
	//ds <<= df;

	//string outfileName = "C:\\temp\\surfaceDLL.mbin";
	//DevIO::dumpMatlabBinary(ds, bitDepth, outfileName);

	//string correctedfileName = "C:\\temp\\correcteDLL.mbin";
	//Ipp16uArray output(h_inImage.height, h_inImage.width, 3, h_outImage.data);
	//DevIO::dumpMatlabBinary(output, bitDepth, correctedfileName);
}

void CudaNativeThinplateSpline16u::warpIntensityAsync(int numberOfControlPoints, int numberOfChannels, const float *weights, const IppiPoint_32f *controlPoints, unsigned int channelMask, int downSample)
{
	// This follows the old C++ code for dewarp.
	downSample = 1;

	auto &h_inImage = h_16uDesc[CudaArrayMnemonic::Input];
	auto &d_inImage = d_16uDesc[CudaArrayMnemonic::Input];

	auto &h_outImage = h_16uDesc[CudaArrayMnemonic::Output];
	auto &d_outImage = d_16uDesc[CudaArrayMnemonic::Output];

	// Sanity check, the outArray must be defined before hand and of correct size
	if (numberOfChannels != d_outImage.channels)
	{
		ThrowOnCudaError(cudaErrorInvalidValue);
	}

	allocIfNecessary(numberOfControlPoints, numberOfChannels, downSample, h_inImage);

	h_weightsDesc.data = (float *)weights;
	copyHostToDevice2DAsync(h_weightsDesc, d_weightsDesc);

	// Copy control points
	ThrowOnCudaError(cudaMemcpyAsync(_d_controlPoints, controlPoints, _numberOfControlPoints * sizeof(float2), cudaMemcpyHostToDevice, _defaultStream));

	// Copy input image
	copyHostToDevice2DAsync(h_inImage, d_inImage);

	// Note, compute in place
	createThinplateSplineSurfaceDownSample(
		d_inImage,
		d_weightsDesc,
		d32f_downSurfaces,
		(float2 *)_d_controlPoints,
		_numberOfControlPoints,
		d_inImage,
		d_maskImage,
		channelMask,
		downSample,
		getMinValue(),
		getMaxValue(),
		_defaultStream
	);
}

void CudaNativeThinplateSpline16u::warpIntensityCopyResultAsync()
{
	auto &h_outImage = h_16uDesc[CudaArrayMnemonic::Output];
	auto &d_outImage = d_16uDesc[CudaArrayMnemonic::Output];
	copyDeviceToHost2DAsync(d_outImage, h_outImage);
}

void CudaNativeThinplateSpline16u::warpLocationAsync(int numberOfControlPoints, const float *weights, const IppiPoint_32f *controlPoints, int fill, int downSample)
{
	// Hasn't been tested with ds > 1 but may actually work for non-lanzcos
	bool doLanczos = downSample == 1;
	downSample = 1;

	auto &h_inImage = h_16uDesc[CudaArrayMnemonic::Input];
	auto &d_inImage = d_16uDesc[CudaArrayMnemonic::Input];

	auto &h_outImage = h_16uDesc[CudaArrayMnemonic::Output];
	auto &d_outImage = d_16uDesc[CudaArrayMnemonic::Output];

	// We compute x and y warps, so there are 2 channels
	auto numberOfChannels = 2;
	allocIfNecessary(numberOfControlPoints, numberOfChannels, downSample, h_inImage);
	allocNormalizedCudaPlanesIfNecessary(h_inImage, d16u_planes, 3);

	// Copy control points, weights, need this before kernel starts
	ThrowOnCudaError(cudaMemcpyAsync(_d_controlPoints, controlPoints, _numberOfControlPoints * sizeof(float2), cudaMemcpyHostToDevice, _kernelStream));

	//	Copy weights
	memcpy(h_weightsDesc.data, weights, h_weightsDesc.width *h_weightsDesc.height * sizeof(float));
	copyHostToDevice2DAsync(h_weightsDesc, d_weightsDesc, _kernelStream);

	// Copy input image
	copyHostToDevice2DAsync(h_inImage, d_inImage);

	// Copy mask if any
	copyHostToDevice2DAsync(h_maskImage, d_maskImage);

	createWarpLocations(
		d_inImage,
		d_weightsDesc,
		d32f_downSurfaces,
		(float2 *)_d_controlPoints,
		_numberOfControlPoints,
		d_inImage,
		d_maskImage,
		downSample,
		getMinValue(),
		getMaxValue(),
		_kernelStream
	);

	// This copies the previous image to the out image
	copyDeviceToDevice2DAsync(d_16uDesc[CudaArrayMnemonic::Ancillary], d_outImage, _kernelStream);

	// This waits for the data to be copied
	createPlaner(
		d_inImage,
		d16u_planes,
		_defaultStream
	);
	
	ThrowOnCudaError(cudaStreamSynchronize(_kernelStream));

	//if (doLanczos)
	//{
	//	warpLanczosImage(
	//		d32f_downSurfaces,
	//		d16u_planes,
	//		d_outImage,
	//		d32f_scratchPlanes,
	//		getMinValue(),
	//		getMaxValue(),
	//		fill,
	//		_defaultStream
	//	);
	//}
	//else
	//{
		warpImage(
			d32f_downSurfaces,
			d16u_planes,
			d_outImage,
			fill,
			_defaultStream
		);
//	}

	// Save the previous image
	// This waits for kernel to finish
	copyDeviceToDevice2DAsync( d_outImage, d_16uDesc[CudaArrayMnemonic::Ancillary], _defaultStream);
}

void CudaNativeThinplateSpline16u::warpLocationCopyResultAsync()
{
	auto &h_outImage = h_16uDesc[CudaArrayMnemonic::Output];
	auto &d_outImage = d_16uDesc[CudaArrayMnemonic::Output];

	// Copy only the ROI
	auto din = d_outImage(getInputRoi());
	auto hout = h_outImage(getOutputRoi());

	// Kernel is running under _defaultStream so this waits for kernel
	copyDeviceToHost2DAsync(din, hout, _defaultStream);
}


