#pragma once
#ifndef ThinplateSplineInterfaceH
#define ThinplateSplineInterfaceH

// This is the master file that calls this one, but that is ok
#include "..\MtiCudaCore.h"

/// <summary>
/// Opens this instance
/// </summary>
/// <param name="gpuIndex">Index of the GPU. First GPU is index 0</param>
/// <returns>a handle to this instance</returns>
CUDA_MTI_CORE_API void *MtiNativeCudaThinplateSpline16uOpen(int gpuIndex);

/// <summary>
/// Closes this instance and frees memory
/// </summary>
/// <param name="vp">the handle to this instance</param>
/// <returns>an error code</returns>
CUDA_MTI_CORE_API int MtiNativeCudaThinplateSpline16uClose(void *vp);

CUDA_MTI_CORE_API int MtiNativeCudaThinplateSpline16uWarpIntensity(void *vp, int numberOfControlPoints, int channels, const float *weights, const IppiPoint_32f *controlPoints, unsigned int channelMask, int downSample);
CUDA_MTI_CORE_API int MtiNativeCudaThinplateSpline16uWarpIntensityAsync(void *vp, int numberOfControlPoints, int channels, const float *weights, const IppiPoint_32f *controlPoints, unsigned int channelMask, int downSample);
CUDA_MTI_CORE_API int MtiNativeCudaThinplateSpline16uCopyResultAsync(void *vp);

CUDA_MTI_CORE_API int MtiNativeCudaThinplateSpline16uWarpLocation(void *vp, int numberOfControlPoints, const float *weights, const IppiPoint_32f *controlPoints, int fill, int downSample);

#endif
