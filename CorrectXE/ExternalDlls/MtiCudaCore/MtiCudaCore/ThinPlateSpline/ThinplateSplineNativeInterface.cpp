#include "ThinplateSplineNativeInterface.h"
#include "CudaNativeThinplateSpline16u.h"

void *MtiNativeCudaThinplateSpline16uOpen(int gpuIndex)
{
	try
	{
		return new CudaNativeThinplateSpline16u(gpuIndex);
	}
	catch (...)
	{
		return nullptr;
	}
}

int MtiNativeCudaThinplateSpline16uClose(void * vp)
{
	try
	{
		auto rp = (CudaNativeThinplateSpline16u *)vp;
		delete rp;
	}
	catch (const cudaError_t &result)
	{
		return int(result);
	}
	catch (...)
	{
		return cudaErrorUnknown;  // generic error
	}

	return 0;
}

int MtiNativeCudaThinplateSpline16uWarpIntensity(void *vp, int numberOfControlPoints, int channels, const float *weights, const IppiPoint_32f *controlPoints, unsigned int channelMask, int downSample)
{
	try
	{
		auto rp = (CudaNativeThinplateSpline16u *)vp;
		rp->warpIntensity(numberOfControlPoints, channels, weights, controlPoints, channelMask, downSample);
	}
	catch (const cudaError_t &result)
	{
		return int(result);
	}
	catch (...)
	{
		return cudaErrorUnknown;  // generic error
	}

	return 0;
}

int MtiNativeCudaThinplateSpline16uWarpIntensityAsync(void *vp, int numberOfControlPoints, int channels, const float *weights, const IppiPoint_32f *controlPoints, unsigned int channelMask, int downSample)
{
	try
	{
		auto rp = (CudaNativeThinplateSpline16u *)vp;
		rp->warpIntensityAsync(numberOfControlPoints, channels, weights, controlPoints, channelMask, downSample);
	}
	catch (const cudaError_t &result)
	{
		return int(result);
	}
	catch (...)
	{
		return cudaErrorUnknown;  // generic error
	}

	return 0;
}

int MtiNativeCudaThinplateSpline16uCopyResultAsync(void *vp)
{
	try
	{
		auto rp = (CudaNativeThinplateSpline16u *)vp;
		rp->warpIntensityCopyResultAsync();
	}
	catch (const cudaError_t &result)
	{
		return int(result);
	}
	catch (...)
	{
		return cudaErrorUnknown;  // generic error
	}

	return cudaSuccess;
}

int MtiNativeCudaThinplateSpline16uWarpLocation(void *vp, int numberOfControlPoints, const float *weights, const IppiPoint_32f *controlPoints, int fill, int downSample)
{
	try
	{
		auto rp = (CudaNativeThinplateSpline16u *)vp;
		rp->warpLocation(numberOfControlPoints, weights, controlPoints, fill, downSample);
	}
	catch (const cudaError_t &result)
	{
		return int(result);
	}
	catch (...)
	{
		return cudaErrorUnknown;  // generic error
	}

	return 0;
}