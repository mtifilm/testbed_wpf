#pragma once

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>
#include <cuda_runtime.h>
#include <type_traits>
#include <ippcore.h>

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define returnOnError(call)                                                    \
do {                                                                           \
    cudaError_t status = call;                                                 \
    if (status != cudaSuccess)                                                 \
	{                                                                          \
		return status;                                                         \
	}                                                                          \
} while (0)

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define ThrowOnCudaError(call)                                                 \
do {                                                                           \
    cudaError_t status = call;                                                 \
    if (status != cudaSuccess)                                                 \
	{                                                                          \
        throw status;														   \
	}                                                                          \
} while (0)

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define ThrowRuntimeCudaError(call)                                            \
do {                                                                           \
    cudaError_t status = call;                                                 \
    if (status != cudaSuccess)                                                 \
	{                                                                          \
		std::ostringstream os;												   \
		os << __func__ << ":" << __LINE__ << "error: function " << #call << "failed with error: " << cudaGetErrorString(status) << std::endl;  \
        throw std::runtime_error(os.str());                                    \
	}                                                                          \
} while (0)

// This makes life a little shorter for calling functions
template<typename T>
struct ImageDesc
{
	T *data = nullptr;
	size_t bytePitch = 0;
	int height = 0;
	int width = 0;
	int channels = 0;
	cudaTextureObject_t texture = 0;  // Note, this is just a unsigned long long
	cudaMemoryType memoryType = cudaMemoryTypeHost;  // Assume host

	ImageDesc<T> operator()(const IppiRect &roi)
	{
		if ((roi.width == 0) || (roi.height == 0))
		{
			return *this;
		}

		auto result = *this;
		auto x = roi.x;
		auto y = roi.x;
		result.width = roi.width;
		result.height = roi.height;
		result.data = data + channels * roi.x + roi.y * bytePitch/sizeof(T);
		result.texture = 0;

		return result;
	}

	ImageDesc<T>() = default;
};

// If one is, all are
static_assert(std::is_trivially_copyable<ImageDesc<float>>::value, "Device props must be copied via mem copy");

