#include "SubpixelResamplerNativeInterface.h"
#include "CudaNativeResampler16u.h"
void *MtiNativeCudaResampler16uOpen(int gpuIndex)
{
	try
	{
		return new CudaNativeResampler16u(gpuIndex);
	}
	catch (...)
	{
		return nullptr;
	}
}

int MtiNativeCudaResampler16uClose(void * vp)
{
	try
	{
		auto rp = (CudaNativeResampler16u *)vp;
		delete rp;
	}
	catch (const cudaError_t &result)
	{
		return int(result);
	}
	catch (...)
	{
		return cudaErrorUnknown;  // generic error
	}

	return 0;
}

int MtiNativeCudaTranslate16uStartAsync(void *vp, const float deltaX, const float deltaY)
{
	auto rp = (CudaNativeResampler16u *)vp;

	try
	{
		rp->translateStartAsync(
			deltaX,
			deltaY
		);
	}
	catch (const cudaError_t &result)
	{
		return int(result);
	}
	catch (...)
	{
		return cudaErrorUnknown;
	}

	return 0;
}

int MtiNativeCudaTranslate16uCopyResultAsync(void *vp)
{
	auto rp = (CudaNativeResampler16u *)vp;
	try
	{
		rp->translateCopyResultAsync();
	}
	catch (const cudaError_t &result)
	{
		return int(result);
	}
	catch (...)
	{
		return cudaErrorUnknown;
	}

	return 0;
}

int  MtiNativeCudaTranslate16u(void *vp, const float deltaX, const float deltaY)
{
	auto rp = (CudaNativeResampler16u *)vp;

	try
	{
		rp->translate(
			deltaX,
			deltaY
		);
	}
	catch (const cudaError_t &result)
	{
		return int(result);
	}
	catch (...)
	{
		return cudaErrorUnknown;
	}

	return 0;
}
