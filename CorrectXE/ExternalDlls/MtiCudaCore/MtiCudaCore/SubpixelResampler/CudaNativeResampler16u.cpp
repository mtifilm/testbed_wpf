#include "CudaNativeResampler16u.h"
#include "cuda_runtime.h"
#include <sstream>
#include <exception>
#include <assert.h>

#include "..\MtiCudaCoreSupport.h"


CudaNativeResampler16u::CudaNativeResampler16u(int gpuDevice) : CudaNativeBase(gpuDevice)
{
}

CudaNativeResampler16u::~CudaNativeResampler16u()
{
	try
	{
		freeAllBuffers();
	}
	catch (...)
	{
		// This should never happen!  We put it in debug mode
	}
}

void CudaNativeResampler16u::freeAllBuffers()
{
	//ThrowOnCudaError(cudaFree(_d_ioBuffer));
	//_d_ioBuffer = nullptr;

	// scratch buffer is freed in parent

	//_height = 0;
	//_width = 0;
}

void CudaNativeResampler16u::allocIfNecessary(int width, int height, int channels)
{
	allocCudaBufferIfNecessary({ nullptr, 0, height, width, channels }, d_16uDesc[CudaArrayMnemonic::Scratch]);
}

extern cudaError_t runTranslate(const MtiCudaResampler16uData &h_data, const MtiCudaResampler16uData &d_data);
extern cudaError_t runTranslateStartAsync(const MtiCudaResampler16uData &data, cudaStream_t &stream);
///extern cudaError_t setMimMaxValuesResampler(Ipp16u minValue, Ipp16u maxValue, cudaStream_t &stream);

void CudaNativeResampler16u::translate(const float deltaX, const float deltaY)
{
	float intx = 0.0f;
	float inty = 0.0f;

	auto fractX = modf(deltaX, &intx);
	auto fractY = modf(deltaY, &inty);

	int startX = int(floor(abs(deltaX)));
	int startY = int(floor(abs(deltaY)));

	auto &h_inImage = h_16uDesc[CudaArrayMnemonic::Input];
	auto &h_outImage = h_16uDesc[CudaArrayMnemonic::Output];

	auto &d_inImage = d_16uDesc[CudaArrayMnemonic::Input];
	auto &d_outImage = d_16uDesc[CudaArrayMnemonic::Output];

	// Build the data structures (Should host be done in calling program??)
	MtiCudaResampler16uData h_resampleData;
	h_resampleData.channels = h_inImage.channels;
	h_resampleData.height = h_inImage.height;
	h_resampleData.width = h_inImage.width;
	h_resampleData.source = h_inImage.data;
	h_resampleData.sourceBytePitch = h_inImage.bytePitch;
	h_resampleData.deltaX = fractX;
	h_resampleData.deltaY = fractY;
	h_resampleData.destination = h_outImage.data;
	h_resampleData.destBytePitch = h_outImage.bytePitch;
	h_resampleData.maxValue = getMaxValue();
	h_resampleData.minValue = getMinValue();

	// This calculate d_inImage in place!!!
	// uses d_outImage as scratch buffer!!
	translate(h_resampleData);

	auto destPitch = h_outImage.bytePitch / sizeof(Ipp16u);
	auto h_output = deltaY < 0 ? h_outImage.data : h_outImage.data + (startY * destPitch);
	h_output = deltaX < 0 ? h_output : h_output + (startX * h_inImage.channels);
	
	auto d_ioBuffer = d_inImage.data;
	auto ioPitch = d_inImage.bytePitch;
	auto d_source = deltaY >= 0 ? d_ioBuffer : d_ioBuffer + (startY * (ioPitch / sizeof(Ipp16u)));
	d_source = deltaX >= 0 ? d_source : d_source + (startX * h_inImage.channels);
	auto newWidth = (h_inImage.width - startX)* h_inImage.channels * sizeof(Ipp16u);
	auto newHeight = (h_inImage.height - startY);
	ThrowOnCudaError(cudaMemcpy2D(h_output, h_outImage.bytePitch, d_source, ioPitch, newWidth, newHeight, cudaMemcpyDeviceToHost));
}

void CudaNativeResampler16u::translate(const MtiCudaResampler16uData &data)
{
	MtiCudaResampler16uData cudaData(data);


	auto &d_inImage = d_16uDesc[CudaArrayMnemonic::Input];
	auto &d_outImage = d_16uDesc[CudaArrayMnemonic::Output];

	cudaData.source = d_inImage.data;
	cudaData.sourceBytePitch = d_inImage.bytePitch;
	cudaData.destination = d_outImage.data;
	cudaData.destBytePitch = d_outImage.bytePitch;

	ThrowOnCudaError(runTranslate(data, cudaData));

	cudaDeviceSynchronize();
	ThrowOnCudaError(cudaPeekAtLastError());
}

void CudaNativeResampler16u::translateStartAsync(float deltaX, const float deltaY)
{

	auto &h_inImage = h_16uDesc[CudaArrayMnemonic::Input];
	auto &d_inImage = d_16uDesc[CudaArrayMnemonic::Input];

	auto &h_outImage = h_16uDesc[CudaArrayMnemonic::Output];
	auto &d_outImage = d_16uDesc[CudaArrayMnemonic::Output];


	auto width = h_inImage.width;
	auto height = h_inImage.height;
	auto channels = h_inImage.channels;

	// Test that the output is same size
	if ((width != h_outImage.width) || (height != h_outImage.height) || (channels != h_outImage.channels))
	{
		ThrowOnCudaError(cudaErrorUnknown);
	}

	_deltaX = deltaX;
	_deltaY = deltaY;

	// Start Async memory transfer, host to device
	auto totalRowBytes = channels * width * sizeof(Ipp16u);
	ThrowOnCudaError(cudaMemcpy2DAsync(d_inImage.data, d_inImage.bytePitch, h_inImage.data, h_inImage.bytePitch, totalRowBytes, height, cudaMemcpyHostToDevice, _defaultStream));

	float intx = 0.0f;
	float inty = 0.0f;

	auto fractX = modf(_deltaX, &intx);
	auto fractY = modf(_deltaY, &inty);

	// Save some args
	MtiCudaResampler16uData data;
	data.channels = channels;
	data.height = height;
	data.width = width;
	data.source = d_inImage.data;
	data.sourceBytePitch = d_inImage.bytePitch;
	data.destination = d_outImage.data;
	data.destBytePitch = d_outImage.bytePitch;
	data.deltaX = fractX;
	data.deltaY = fractY;
	data.minValue = getMinValue();
	data.maxValue = getMaxValue();

	ThrowOnCudaError(runTranslateStartAsync(data, _defaultStream));
}

void  CudaNativeResampler16u::translateCopyResultAsync()
{
	int startX = int(floor(abs(_deltaX)));
	int startY = int(floor(abs(_deltaY)));
	
	auto &h_inImage = h_16uDesc[CudaArrayMnemonic::Input];
	auto &d_inImage = d_16uDesc[CudaArrayMnemonic::Input];

	auto &h_outImage = h_16uDesc[CudaArrayMnemonic::Output];
	auto &d_outImage = d_16uDesc[CudaArrayMnemonic::Output];

	auto destPitch = h_outImage.bytePitch / sizeof(Ipp16u);
	auto h_output = _deltaY < 0 ? h_outImage.data : h_outImage.data + (startY * destPitch);
	h_output = _deltaX < 0 ? h_output : h_output + (startX * h_inImage.channels);

	auto d_ioBuffer = d_inImage.data;
	auto ioPitch = d_inImage.bytePitch;
	auto d_source = _deltaY >= 0 ? d_ioBuffer : d_ioBuffer + (startY * (ioPitch / sizeof(Ipp16u)));
	d_source = _deltaX >= 0 ? d_source : d_source + (startX * h_inImage.channels);
	auto newWidth = (h_inImage.width - startX)* h_inImage.channels * sizeof(Ipp16u);
	auto newHeight = (h_inImage.height - startY);
	ThrowOnCudaError(cudaMemcpy2DAsync(h_output, h_outImage.bytePitch, d_source, ioPitch, newWidth, newHeight, cudaMemcpyDeviceToHost, _defaultStream));
}

