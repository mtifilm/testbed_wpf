#pragma once
#ifndef SubpixelResamplerNativeInterfaceH
#define SubpixelResamplerNativeInterfaceH

// This is the master file that calls this one, but that is ok
#include "..\MtiCudaCore.h"

struct alignas(8) MtiCudaResampler16uData
{
	Ipp16u *source;
	Ipp16u *destination;
	size_t sourceBytePitch;
	size_t destBytePitch;
	int height;
	int width;
	int channels;
	float deltaX;
	float deltaY;
	Ipp16u minValue;
	Ipp16u maxValue;
};

/// <summary>
/// Opens this instance
/// </summary>
/// <param name="gpuIndex">Index of the GPU. First GPU is index 0</param>
/// <returns>a handle to this instance</returns>
CUDA_MTI_CORE_API void *MtiNativeCudaResampler16uOpen(int gpuIndex);

/// <summary>
/// Closes this instance and frees memory
/// </summary>
/// <param name="vp">the handle to this instance</param>
/// <returns>an error code</returns>
CUDA_MTI_CORE_API int MtiNativeCudaResampler16uClose(void *vp);

// Note, setInputArray and set Output array must be called first
CUDA_MTI_CORE_API int MtiNativeCudaTranslate16u(void *vp, const float deltaX, const float deltaY);

// Note, setInputArray and set Output array must be called BEFORE the start Async
CUDA_MTI_CORE_API int MtiNativeCudaTranslate16uStartAsync(void *vp, const float deltaX, const float deltaY);
CUDA_MTI_CORE_API int MtiNativeCudaTranslate16uCopyResultAsync(void *vp);

#endif