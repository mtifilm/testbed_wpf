#include "MtiCudaCore.h"
#include "MtiCudaCoreSupport.h"
#include "ippcore.h"
#include <iostream>
#include <stdio.h>
#include <sstream>

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define BLOCKSIZE_x 16
#define BLOCKSIZE_y 16

/*******************/
/* iDivUp FUNCTION */
/*******************/
int iDivUp(int hostPtr, int b){ return ((hostPtr % b) != 0) ? (hostPtr / b + 1) : (hostPtr / b); }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define M_PI 3.14159265358979323846264338327950288f

__host__ __device__ static float sinc(float x)
{
	x = (x * M_PI);

	if ((x < 0.01f) && (x > -0.01f))
		return 1.0f + x * x*(-1.0f / 6.0f + x * x*1.0f / 120.0f);

	return sin(x) / x;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

// REFACTOR: USE TEXTURE OBJECT HERE!!!!!
texture<Ipp16u, cudaTextureType2D, cudaReadModeElementType> texRef16u;
texture<Ipp16u, cudaTextureType2D, cudaReadModeElementType> texRef16uScratch;

#define min(a, b) ((a) > (b))? (b): (a)
#define max(a, b) ((a) > (b))? (a): (b) 

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

__global__ void do_x_translate(size_t pitch, int nCols, int nRows, Ipp16u *outPtr, int g_channels, float deltaX, Ipp16u minValue, Ipp16u maxValue)
{
   int tidx = blockIdx.x*blockDim.x + threadIdx.x;
   int tidy = blockIdx.y*blockDim.y + threadIdx.y; 

   if ((tidx >= nCols) || (tidy >= nRows))
   {
	   return;
   }

   auto sum = 0.0f;
   auto wx = 0.0f;

   for (auto i = -3; i <= 3; i++)
   {
		float sc = sinc(i + deltaX);
		sum += tex2D(texRef16u, tidx + g_channels*i, tidy) * sc;
		wx += sc;
   }

   Ipp16u *row_o = (Ipp16u *)((char*)outPtr + tidy * pitch); 
   row_o[tidx] = min(maxValue, max(minValue, int(sum/wx)));	
}

__global__ void do_y_translate(size_t pitch, int nCols, int nRows, Ipp16u *outPtr, int g_channels, float deltaY, Ipp16u minValue, Ipp16u maxValue)
{
   int tidx = blockIdx.x*blockDim.x + threadIdx.x;
   int tidy = blockIdx.y*blockDim.y + threadIdx.y;

   if ((tidx >= nCols) || (tidy >= nRows))
   {
	   return;
   }

   auto sum = 0.0f;
   auto wy = 0.0f;

	for (auto i = -3; i <= 3; i++)
	{
		float sc = sinc(i + deltaY);
		sum += tex2D(texRef16uScratch, tidx, tidy + i) * sc;
		wy += sc;
    }

   Ipp16u *row_o = (Ipp16u *)((char*)outPtr + tidy * pitch); 
   row_o[tidx] = min(maxValue, max(minValue, int(sum/wy)));
}

//cudaError_t setMimMaxValuesResampler(Ipp16u minValue, Ipp16u maxValue, cudaStream_t &stream)
//{
//   returnOnError(cudaMemcpyToSymbolAsync (g_minValue, &minValue, sizeof(Ipp16u), 0, cudaMemcpyHostToDevice, stream)); 
//   returnOnError(cudaMemcpyToSymbolAsync (g_maxValue, &maxValue, sizeof(Ipp16u), 0, cudaMemcpyHostToDevice, stream)); 
//
//   return cudaSuccess;
//}

cudaError_t runTranslate(const MtiCudaResampler16uData &h_data, const MtiCudaResampler16uData &d_data)
{
   auto width = h_data.width;
   auto height = h_data.height;
   int channels = h_data.channels;
   cudaStream_t defaultStream = nullptr;

   auto combinedWidth = channels * width;

   // Copy memory to device
   returnOnError(cudaMemcpy2D(d_data.source, d_data.sourceBytePitch, h_data.source, h_data.sourceBytePitch, combinedWidth*sizeof(Ipp16u), height, cudaMemcpyHostToDevice));
   
   // Because the data is interleaved, we pretend one channel
   cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc(16, 0, 0, 0, cudaChannelFormatKindUnsigned);

   texRef16u.addressMode[0] = cudaAddressModeClamp;
   texRef16u.addressMode[1] = cudaAddressModeClamp;
   texRef16u.filterMode = cudaFilterModePoint;

   size_t offset = -1;
   returnOnError(cudaBindTexture2D(&offset, &texRef16u, d_data.source, &channelDesc, combinedWidth, height, d_data.sourceBytePitch));
   returnOnError(cudaBindTexture2D(&offset, &texRef16uScratch, d_data.destination, &channelDesc, combinedWidth, height, d_data.destBytePitch));

   dim3 gridSize(iDivUp(combinedWidth, BLOCKSIZE_x), iDivUp(height, BLOCKSIZE_y));
   dim3 blockSize(BLOCKSIZE_x, BLOCKSIZE_y);

   do_x_translate << <gridSize, blockSize >> >(d_data.sourceBytePitch, combinedWidth, height, d_data.destination, channels, d_data.deltaX, h_data.minValue, h_data.maxValue);  
   returnOnError(cudaPeekAtLastError());

   do_y_translate << <gridSize, blockSize >> >(d_data.destBytePitch, combinedWidth, height, d_data.source, channels, d_data.deltaY, h_data.minValue, h_data.maxValue);   
   
   // Assume success
   return cudaSuccess;
}

cudaError_t runTranslateStartAsync(const MtiCudaResampler16uData &d_data, cudaStream_t &stream)
{
   auto width = d_data.width;
   auto height = d_data.height;
   int channels = d_data.channels;

   auto rowWidth = channels * width;

   // Because the data is interleaved, we pretend one channel
   cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc(16, 0, 0, 0, cudaChannelFormatKindUnsigned);

   texRef16u.addressMode[0] = cudaAddressModeClamp;
   texRef16u.addressMode[1] = cudaAddressModeClamp;
   texRef16u.filterMode = cudaFilterModePoint;

   size_t offset = -1;
   returnOnError(cudaBindTexture2D(&offset, &texRef16u, d_data.source, &channelDesc, rowWidth, height, d_data.sourceBytePitch));
   returnOnError(cudaBindTexture2D(&offset, &texRef16uScratch, d_data.destination, &channelDesc, rowWidth, height, d_data.destBytePitch));

   dim3 gridSize(iDivUp(rowWidth, BLOCKSIZE_x), iDivUp(height, BLOCKSIZE_y));
   dim3 blockSize(BLOCKSIZE_y, BLOCKSIZE_x);

   do_x_translate << <gridSize, blockSize, 0, stream >> >(d_data.sourceBytePitch, rowWidth, height, d_data.destination, channels, d_data.deltaX, d_data.minValue, d_data.maxValue);  
   do_y_translate << <gridSize, blockSize, 0, stream >> >(d_data.destBytePitch, rowWidth, height, d_data.source, channels, d_data.deltaY, d_data.minValue, d_data.maxValue);  

   // Assume success
   return cudaSuccess;
}