#pragma once
#ifndef CudaNativeResampler16uH
#define CudaNativeResampler16uH

#include "..\MtiCudaCore.h"
#include "..\CudaNativeBase.h"
#include "cuda_runtime.h"

// Note, this class only supports one resample at a time.
// Thus a second async cannot be started before the first is done.
// The design is to use multiple classes to overlap the I/O
class CudaNativeResampler16u : public CudaNativeBase
{
public:
	CudaNativeResampler16u(int gpuDevice);
	~CudaNativeResampler16u();

	void translate(const float deltaX, const float deltaY);
	void translate(const MtiCudaResampler16uData &data);

	void translateStartAsync(const float deltaX, const float deltaY);
	void translateCopyResultAsync();

private:
	void freeAllBuffers();
	void allocIfNecessary(int width, int height, int channels);

private:
	float _deltaX = 0;
	float _deltaY = 0;

	//Ipp16u *_d_ioBuffer = nullptr;
};

#endif