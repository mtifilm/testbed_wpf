#pragma once
#ifndef CudaNativeBaseNativeInterfaceH
#define CudaNativeBaseNativeInterfaceH

// These should be moved to own interface
#include "MtiCudaCore.h"

CUDA_MTI_CORE_API int MtiNativeCudaWait(void *vp);
CUDA_MTI_CORE_API int MtiNativeCudaSetMinMax(void *vp, Ipp16u minValue, Ipp16u maxValue);
CUDA_MTI_CORE_API int MtiNativeCudaSetInputArray(void *vp, Ipp16u *source, int sourceBytePitch, int height, int width, int channels);
CUDA_MTI_CORE_API int MtiNativeCudaSetOutputArray(void *vp, Ipp16u *source, int sourceBytePitch, int height, int width, int channels);
CUDA_MTI_CORE_API int MtiNativeCudaSetMaskArray(void *vp, Ipp8u *source, int sourceBytePitch, int height, int width, int channels);
CUDA_MTI_CORE_API int MtiNativeCudaSetInputRoi(void *vp, const IppiRect &roi);
CUDA_MTI_CORE_API int MtiNativeCudaSetOutputRoi(void *vp, const IppiRect &roi);
CUDA_MTI_CORE_API int MtiNativeCudaCopyOutputFromDeviceAsync(void *vp, const IppiRect &deviceRoi, const IppiRect &hostRoi);
CUDA_MTI_CORE_API int MtiNativeLoadArrayToDeviceAsync(void *vp, CudaArrayMnemonic n,Ipp16u *source, int sourceBytePitch);
CUDA_MTI_CORE_API int MtiNativeCudaSetArray(void *vp, CudaArrayMnemonic n, Ipp16u *source, int sourceBytePitch, int height, int width, int channels);

#endif
