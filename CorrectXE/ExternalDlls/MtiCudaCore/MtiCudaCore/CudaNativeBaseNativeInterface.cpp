#include "CudaNativeBase.h"

int MtiNativeCudaWait(void * vp)
{
	auto rp = (CudaNativeBase *)vp;
	try
	{
		rp->wait();
	}
	catch (const cudaError_t &result)
	{
		return int(result);
	}
	catch (...)
	{
		return cudaErrorUnknown;
	}

	return cudaSuccess;
}

int MtiNativeCudaSetMinMax(void * vp, Ipp16u minValue, Ipp16u maxValue)
{
	auto rp = (CudaNativeBase *)vp;

	try
	{
		rp->setMinMax(minValue, maxValue);
	}
	catch (const cudaError_t &result)
	{
		return int(result);
	}
	catch (...)
	{
		return cudaErrorUnknown;
	}

	return cudaSuccess;
}

int MtiNativeCudaSetInputArray(void *vp, Ipp16u *source, int sourceBytePitch, int height, int width, int channels)
{
	auto rp = (CudaNativeBase *)vp;

	try
	{
		rp->setInputArray(source, sourceBytePitch, height, width, channels);
	}
	catch (const cudaError_t &result)
	{
		return int(result);
	}
	catch (...)
	{
		return cudaErrorUnknown;
	}

	return cudaSuccess;
}

int MtiNativeCudaSetOutputArray(void *vp, Ipp16u *source, int sourceBytePitch, int height, int width, int channels)
{
	auto rp = (CudaNativeBase *)vp;

	try
	{
		rp->setOutputArray(source, sourceBytePitch, height, width, channels);
	}
	catch (const cudaError_t &result)
	{
		return int(result);
	}
	catch (...)
	{
		return cudaErrorUnknown;
	}

	return cudaSuccess;
}

int MtiNativeCudaSetArray(void *vp, CudaArrayMnemonic n, Ipp16u *source, int sourceBytePitch, int height, int width, int channels)
{
	auto rp = (CudaNativeBase *)vp;

	try
	{
		rp->setArray(n, source, sourceBytePitch, height, width, channels);
	}
	catch (const cudaError_t &result)
	{
		return int(result);
	}
	catch (...)
	{
		return cudaErrorUnknown;
	}

	return cudaSuccess;
}

int MtiNativeCudaSetMaskArray(void *vp, Ipp8u *source, int sourceBytePitch, int height, int width, int channels)
{
	auto rp = (CudaNativeBase *)vp;

	try
	{
		rp->setMaskArray(source, sourceBytePitch, height, width, channels);
	}
	catch (const cudaError_t &result)
	{
		return int(result);
	}
	catch (...)
	{
		return cudaErrorUnknown;
	}

	return cudaSuccess;
}

int MtiNativeCudaSetInputRoi(void * vp, const IppiRect &roi)
{
	auto rp = (CudaNativeBase *)vp;
	try
	{
		rp->setInputRoi(roi);
	}
	catch (const cudaError_t &result)
	{
		return int(result);
	}
	catch (...)
	{
		return cudaErrorUnknown;
	}

	return cudaSuccess;
}

int MtiNativeCudaSetOutputRoi(void * vp, const IppiRect &roi)
{
	auto rp = (CudaNativeBase *)vp;
	try
	{
		rp->setOutputRoi(roi);
	}
	catch (const cudaError_t &result)
	{
		return int(result);
	}
	catch (...)
	{
		return cudaErrorUnknown;
	}

	return cudaSuccess;
}

int MtiNativeCudaCopyOutputFromDeviceAsync(void * vp, const IppiRect & deviceRoi, const IppiRect & hostRoi)
{
	auto rp = (CudaNativeBase *)vp;
	try
	{
		rp->copyOutputFromDeviceAsync(deviceRoi, hostRoi);
	}
	catch (const cudaError_t &result)
	{
		return int(result);
	}
	catch (...)
	{
		return cudaErrorUnknown;
	}

	return cudaSuccess;
}

int  MtiNativeLoadArrayToDeviceAsync(void * vp, CudaArrayMnemonic n, Ipp16u * source, int sourceBytePitch)
{
	auto rp = (CudaNativeBase *)vp;
	try
	{
		rp->loadArrayToDeviceAsync(n, source, sourceBytePitch);
	}
	catch (const cudaError_t &result)
	{
		return int(result);
	}
	catch (...)
	{
		return cudaErrorUnknown;
	}

	return cudaSuccess;
}
