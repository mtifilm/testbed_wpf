#pragma once
#ifndef CudaBaseClassH
#define CudaBaseClassH

#include "MtiCudaCoreBasicInterface.h"

class MTI_CUDA_CORE_CPP_API CudaBaseClass
{
public:
	CudaBaseClass(int gpuIndex);
	virtual ~CudaBaseClass();

	void wait();
	void setMinMax(Ipp16u minValue, Ipp16u maxValue);
	void setInputArray(const Ipp16uArray &inArray);
	void setOutputArray(Ipp16uArray &outArray);
	void setAndLoadAncillaryArrayAsync(Ipp16uArray &outArray);

   void clearMaskArray();
	void setMaskArray(const Ipp8uArray &mask);

	void setInputRoi(const IppiRect &roi);
	IppiRect getInputRoi() const { return _inRoi; }
	void setOutputRoi(const IppiRect &roi);
	IppiRect getOutputRoi() const { return _outRoi; }
	void copyOutputFromDeviceAsync(const IppiRect &deviceRoi, const IppiRect &hostRoi);
	void copyOutputFromDeviceAsync();

	Ipp16uArray getInArray() const { return  _inArray; }
	Ipp16uArray getOutArray() const { return  _outArray; }

	

protected:
	void *_nativeClass = nullptr;
	int _gpuIndex = 0;
	Ipp16uArray _inArray;
	Ipp16uArray _outArray;

	// We should get rid of the shadow privates
	IppiRect _inRoi = { 0, 0, 0, 0 };
	IppiRect _outRoi = { 0, 0, 0, 0 };
};

#endif

