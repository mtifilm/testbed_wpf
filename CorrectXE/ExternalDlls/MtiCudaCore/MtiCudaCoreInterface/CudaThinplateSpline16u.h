#ifndef CudaThinplateSpline16uH
#define CudaThinplateSpline16uH

#include "MtiCudaCoreInterface.h"

#define DEFAULT_TPS_DOWNSAMPLE 4
#define DEFAULT_LOCATION_DOWNSAMPLE 1  // NOT USE FOR NOW

class MTI_CUDA_CORE_CPP_API CudaThinplateSpline16u : public CudaBaseClass
{
public:
	// Create the native ThinplateSpline
	CudaThinplateSpline16u(int gpuDevice);
	~CudaThinplateSpline16u();

	// This adds to each point the intensity warp used in zonal deflicker 
	// Note, the Asyn versions need pinned memory backing in the ipp arrays
	void warpIntensity(const Ipp16uArray &inArray, Ipp16uArray &outArray, const vector<vector<float>> &weights, const vector<IppiPoint_32f> &controlPoints, unsigned int channelMask, int downSample = DEFAULT_TPS_DOWNSAMPLE);

	void warpIntensityAsync(const Ipp16uArray &inArray, Ipp16uArray &outArray, const vector<vector<float>> &weights, const vector<IppiPoint_32f> &controlPoints, unsigned int channelMask, int downSample = DEFAULT_TPS_DOWNSAMPLE);
	void warpIntensityCopyResultAsync();

	void warpLocation(const Ipp16uArray &inArray, Ipp16uArray &outArray, const vector<vector<float>> &weights, const vector<IppiPoint_32f> &controlPoints, int fill = -1, int downSample = DEFAULT_LOCATION_DOWNSAMPLE);
};
#endif

