#include "MtiCudaCoreInterface.h"
#include "MtiCudaCore.h"
#include <sstream>
#include <stdexcept>

CudaThinplateSpline16u::CudaThinplateSpline16u(int gpuDevice) : CudaBaseClass(gpuDevice)
{
	_nativeClass = MtiNativeCudaThinplateSpline16uOpen(gpuDevice);
	if (_nativeClass == nullptr)
	{
		auto status = MtiNativeCudaGetLastError();
		std::ostringstream os;
		os << "Error opening GPU index " << gpuDevice << std::endl;
		os << MtiCudaGetErrorString(status) << std::endl;
		throw std::runtime_error(os.str());
	}
}

CudaThinplateSpline16u::~CudaThinplateSpline16u()
{
	auto status = MtiNativeCudaThinplateSpline16uClose(_nativeClass);
	if (status != 0)
	{
		std::ostringstream os;
		os << "Error closing " << __func__ << ", code: " << status;
      TRACE_0(errout << os.str());
	}
}

void CudaThinplateSpline16u::CudaThinplateSpline16u::warpIntensity(const Ipp16uArray &inArray, Ipp16uArray &outArray, const vector<vector<float>> &weights, const vector<IppiPoint_32f> &controlPoints, unsigned int channelMask, int downSample)
{
	// If a null array, create it
	outArray.createStorageIfEmpty(inArray);
	inArray.throwOnArraySizeChannelMismatch(outArray);

	// Sanity check
	if (inArray.getComponents() != weights.size())
	{
		std::ostringstream os;
		os << "The channels and weights vectors do not have same size " << __func__ << ", weights = " << weights.size() << ", Input channesl = " << inArray.getComponents();
		throw std::runtime_error(os.str());
	}

	for (auto &w : weights)
	{
		// The last 3 are the affine transformation
		if (w.size() != (controlPoints.size() + 3))
		{
			std::ostringstream os;
			os << "Number of weights does not match number of controls points " << __func__ << ", weights = " << weights.size() << ", controlPoints = " << controlPoints.size();
			throw std::runtime_error(os.str());
		}
	}

	setInputArray(inArray);
	setOutputArray(outArray);

 //	setInputRoi(inArray.getRoi());
 //	setOutputRoi(outArray.getRoi());
	// To go native, we need one contig array
	// We are assuming 3 channels
	auto sizeOneWeight = controlPoints.size() + 3;
	auto totalWeights = sizeOneWeight * weights.size();

	// Create one vector
	vector<float> vw;
	auto inserter = std::back_inserter(vw);
	for (auto &w : weights)
	{
		std::copy(w.begin(), w.end(), inserter);
	}

	auto status = MtiNativeCudaThinplateSpline16uWarpIntensity
	(
		_nativeClass,
		controlPoints.size(),
		inArray.getComponents(),
		vw.data(),
		controlPoints.data(),
		channelMask,
		downSample
	);

	if (status != 0)
	{
		std::ostringstream os;
		os << "Error in " << __func__ << ", code: " << status << ", error " << MtiNativeCudaGetErrorCharPointer(status);
		throw std::runtime_error(os.str());
	}
}

void CudaThinplateSpline16u::warpIntensityAsync(const Ipp16uArray & inArray, Ipp16uArray & outArray, const vector<vector<float>>& weights, const vector<IppiPoint_32f>& controlPoints, unsigned int channelMask, int downSample)
{
	// If a null array, create it
	outArray.createStorageIfEmpty(inArray);
	inArray.throwOnArraySizeChannelMismatch(outArray);

	// Sanity check
	if (inArray.getComponents() != weights.size())
	{
		std::ostringstream os;
		os << "The channels and weights vectors do not have same size " << __func__ << ", weights = " << weights.size() << ", Input channesl = " << inArray.getComponents();
		throw std::runtime_error(os.str());
	}

	auto &w1 = weights[0];
	for (auto &w : weights)
	{
		// The last 3 are the affine transformation
		if (w.size() != (controlPoints.size() + 3))
		{
			std::ostringstream os;
			os << "Number of weights does not match number of controls points " << __func__ << ", weights = " << weights.size() << ", controlPoints = " << controlPoints.size();
			throw std::runtime_error(os.str());
		}
	}

	setInputArray(inArray);
	setOutputArray(outArray);

	// To go native, we need one contig array
	// We are assuming 3 channels
	auto sizeOneWeight = controlPoints.size() + 3;
	auto totalWeights = sizeOneWeight * weights.size();

	// Create one vector
	vector<float> vw;
	auto inserter = std::back_inserter(vw);
	for (auto &w : weights)
	{
		std::copy(w.begin(), w.end(), inserter);
	}

	auto status = MtiNativeCudaThinplateSpline16uWarpIntensityAsync
	(
		_nativeClass,
		controlPoints.size(),
		inArray.getComponents(),
		vw.data(),
		controlPoints.data(),
		channelMask,
		downSample
	);

	if (status != 0)
	{
		std::ostringstream os;
		os << "Error in " << __func__ << ", code: " << status << ", error " << MtiNativeCudaGetErrorCharPointer(status);
		throw std::runtime_error(os.str());
	}
}

void CudaThinplateSpline16u::warpIntensityCopyResultAsync()
{
	auto status = MtiNativeCudaThinplateSpline16uCopyResultAsync(_nativeClass);
	if (status != 0)
	{
		std::ostringstream os;
		os << "Error in " << __func__ << ", code: " << status << ", error " << MtiNativeCudaGetErrorCharPointer(status);
		throw std::runtime_error(os.str());
	}
}


void CudaThinplateSpline16u::CudaThinplateSpline16u::warpLocation(const Ipp16uArray &inArray, Ipp16uArray &outArray, const vector<vector<float>> &weights, const vector<IppiPoint_32f> &controlPoints, int fill, int downSample)
{
	// If a null array, create it
	outArray.createStorageIfEmpty(inArray);
	inArray.throwOnArraySizeChannelMismatch(outArray);

	// Sanity check
	if (2 != weights.size())
	{
		std::ostringstream os;
		os << "There must be exactly 2 weight vectors " << __func__ << ", weights = " << weights.size();
		throw std::runtime_error(os.str());
	}

	auto &w1 = weights[0];
	for (auto &w : weights)
	{
		// The last 3 are the affine transformation
		if (w.size() != (controlPoints.size() + 3))
		{
			std::ostringstream os;
			os << "Number of weights does not match number of controls points " << __func__ << ", weights = " << weights.size() << ", controlPoints = " << controlPoints.size();
			throw std::runtime_error(os.str());
		}
	}

	setInputArray(inArray);
	setOutputArray(outArray);

	// To go native, we need one contig array
	// We are assuming 3 channels
	auto sizeOneWeight = controlPoints.size() + 3;
	auto totalWeights = sizeOneWeight * weights.size();

	// Create one vector
	vector<float> vw;
	auto inserter = std::back_inserter(vw);
	for (auto &w : weights)
	{
		std::copy(w.begin(), w.end(), inserter);
	}

	auto status = MtiNativeCudaThinplateSpline16uWarpLocation
	(
		_nativeClass,
		controlPoints.size(),
		vw.data(),
		controlPoints.data(),
		fill,
		downSample
	);

	if (status != 0)
	{
		std::ostringstream os;
		os << "Error in " << __func__ << ", code: " << status << ", error " << MtiNativeCudaGetErrorCharPointer(status);
		throw std::runtime_error(os.str());
	}
}
