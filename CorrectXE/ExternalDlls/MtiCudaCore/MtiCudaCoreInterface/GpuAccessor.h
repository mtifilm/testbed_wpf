//---------------------------------------------------------------------------

#ifndef GpuAccessorH
#define GpuAccessorH

#include "MtiCudaCoreBasicInterface.h"

//---------------------------------------------------------------------------

class MTI_CUDA_CORE_CPP_API GpuAccessor
{
public:
	GpuAccessor();

public:
	bool doesGpuExist() const;
	bool useGpu() const;
	void setUseIfAvailable(bool value) { _defaultUseGpu = value; }
	int getDeviceCount() const;

   bool isDefaultGpuSet() const;
	string getDeviceName(int index) const;
	vector<string> getDeviceNames() const;

	int getDefaultGpu() const { return _defaultIndex; };
	void setDefaultGpu(int value) { _defaultIndex = value; }

	string getDeviceName() const;
	int getMajorComputeCapability() const;
	int getMinorComputeCapability() const;

	int getMajorDriverVersion() const;
	int getMinorDriverVersion() const;

	int getMajorRuntimeVersion() const;
	int getMinorRuntimeVersion() const;

	string getInfoString(int deviceInde) const;
	
	// This will return the reason no devices were found, else an error
	string getErrorMessage() const { return _errorMessage;  }

   bool isDriverVersionOutOfData() const;
   bool noNVidiaHardware() const;

private:
	static GpuAccessor _gpuAccessor;
	static int _defaultIndex;
	static bool _defaultUseGpu;
   static int _errorStatus;
	static string _errorMessage;
	};

#endif
