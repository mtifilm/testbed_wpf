#include "MtiCudaCoreInterface.h"
#include "MtiCudaCore.h"
#include <sstream>
#include <stdexcept>

CudaResampler16u::CudaResampler16u(int gpuIndex) : CudaBaseClass(gpuIndex)
{
	_nativeClass = MtiNativeCudaResampler16uOpen(gpuIndex);
	if (_nativeClass == nullptr)
	{
		auto status = MtiNativeCudaGetLastError();
		std::ostringstream os;
		os << "Error opening GPU index " << gpuIndex << std::endl;
		os << MtiCudaGetErrorString(status) << std::endl;
		throw std::runtime_error(os.str());
	}
}

CudaResampler16u::~CudaResampler16u()
{
	auto status = MtiNativeCudaResampler16uClose(_nativeClass);
	if (status != 0)
	{
		std::ostringstream os;
		os << "Error closing resampler, code: " << status;
		TRACE_0(errout << os.str());
	}
}

int CudaResampler16u::translate(const Ipp16uArray &inArray, Ipp16uArray &outArray, float deltaX, float deltaY)
{
	// If a null array, create it
	outArray.createStorageIfEmpty(inArray); 
	inArray.throwOnArraySizeChannelMismatch(outArray);

	setInputArray(inArray);
	setOutputArray(outArray);

	auto status = MtiNativeCudaTranslate16u(
		_nativeClass,
		deltaX,
		deltaY);

	if (status != 0)
	{
		std::ostringstream os;
		os << "Error in resampler::translate, code: " << status << ", error " << MtiNativeCudaGetErrorCharPointer(status);
		throw std::runtime_error(os.str());
	}

	return status;
}

int CudaResampler16u::translateStartAsync(const Ipp16uArray &inArray, Ipp16uArray &outArray, float deltaX, float deltaY)
{
	setInputArray(inArray);
	
	// If a null array, create it
//	outArray.createStorageIfEmpty(_inArray);
	inArray.throwOnArraySizeChannelMismatch(outArray);
	setOutputArray(outArray);

	auto status = MtiNativeCudaTranslate16uStartAsync(
		_nativeClass,
		deltaX,
		deltaY);

	if (status != 0)
	{
		std::ostringstream os;
		os << "Error in resampler::translate, code: " << status << ", error " << MtiNativeCudaGetErrorCharPointer(status);
		throw std::runtime_error(os.str());
	}

	_inArray = inArray;
	_outArray = outArray;
	return status;
}


int CudaResampler16u::translateCopyResultAsync()
{
	auto outArray = _outArray;
	auto status = MtiNativeCudaTranslate16uCopyResultAsync(_nativeClass);
	if (status != 0)
	{
		std::ostringstream os;
		os << "Error in resampler::translate, code: " << status << ", error " << MtiNativeCudaGetErrorCharPointer(status);
		throw std::runtime_error(os.str());
	}

	return status;
}

