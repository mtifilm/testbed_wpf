// ---------------------------------------------------------------------------

#pragma hdrstop

#include "GpuAccessor.h"
#include "MtiCudaCore.h"
#include "MtiCudaCoreInterface.h"
#include <vector>

// **** WARNING THIS IS TEMPORARY UNTIL DECIDED HOW TO DO CUDA
#include "driver_types.h"

// ---------------------------------------------------------------------------
static std::vector<cudaDeviceProp>deviceProps;

static bool _first = true;

int GpuAccessor::_defaultIndex = -1;
bool GpuAccessor::_defaultUseGpu = true;
int GpuAccessor::_errorStatus = 0;
string GpuAccessor::_errorMessage;

GpuAccessor::GpuAccessor()
{
	if (_first == false)
	{
		return;
	}

	_first = false;

	// This is called once and only once
	// Really should have a static constructor
	int count = 0;
	auto status = MtiCudaGetDeviceCount(count);

	_errorStatus = status;
	if (status != 0)
	{
		_errorMessage = MtiCudaGetErrorString(status);
		return;
	}

	// No gpu
	if (count <= 0)
	{
		return;
	}

	for (auto i = 0; i < count; i++)
	{
		cudaDeviceProp props;

		status = MtiNativeCudatGetDeviceProp(props, i);
		if ((status == 0) && (props.major >= 3) && (props.minor >= 0))
		{
			deviceProps.push_back(props);
		}
	}

	if (deviceProps.size() == 0)
	{
		MTIostringstream os;
		os << "No GPU has sufficent compute capabilities" << std::endl;
		os << "Requires 3.0 ";
		_errorMessage = os.str();
	}

	_defaultIndex = 0;
}

bool GpuAccessor::doesGpuExist() const {return deviceProps.size() > 0;}

int GpuAccessor::getDeviceCount() const {return deviceProps.size();}

bool GpuAccessor::useGpu() const {return _defaultUseGpu && isDefaultGpuSet();}

string GpuAccessor::getDeviceName(int index) const
{
	if (index >= getDeviceCount())
	{
		return "No GPU found";
	}

	return string(deviceProps[index].name);
}

string GpuAccessor::getDeviceName() const {return getDeviceName(getDefaultGpu());}

vector<string>GpuAccessor::getDeviceNames() const
{
	vector<string>result;
	for (auto&deviceProp : deviceProps)
	{
		result.emplace_back(deviceProp.name);
	}

	return result;
}

bool GpuAccessor::isDefaultGpuSet() const {return (_defaultIndex >= 0) && (_defaultIndex < deviceProps.size());}

int GpuAccessor::getMajorComputeCapability() const
{
	if ((doesGpuExist() == false) || (isDefaultGpuSet() == false))
	{
		return -1;
	}

	auto &props = deviceProps.at(getDefaultGpu());
	return props.major;
}

int GpuAccessor::getMinorComputeCapability() const
{
	if ((doesGpuExist() == false) || (isDefaultGpuSet() == false))
	{
		return -1;
	}

	auto &props = deviceProps.at(getDefaultGpu());
	return props.minor;
}

int GpuAccessor::getMajorDriverVersion() const
{
	int driverVersion = MtiCudaDriverGetVersion();
	return driverVersion / 1000;
}

int GpuAccessor::getMinorDriverVersion() const
{
	int driverVersion = MtiCudaDriverGetVersion();
	return (driverVersion % 100) / 10;
}

int GpuAccessor::getMajorRuntimeVersion() const
{
	int runtimeVersion = MtiCudaRuntimeGetVersion();
	return runtimeVersion / 1000;
}

int GpuAccessor::getMinorRuntimeVersion() const
{
	int runtimeVersion = MtiCudaRuntimeGetVersion();
	return (runtimeVersion % 100) / 10;
}

// This is defined in the samples, but those may not be installed, so hard code it
// here.  This means it may not be valid with an increase of toolkit
inline int _ConvertSMVer2Cores(int major, int minor)
{
	// Defines for GPU Architecture types (using the SM version to determine
	// the # of cores per SM
	typedef struct
	{
		int SM; // 0xMm (hexidecimal notation), M = SM Major version,
		// and m = SM minor version
		int Cores;
	} sSMtoCores;

	sSMtoCores nGpuArchCoresPerSM[] =
	{ {0x30, 192}, // Kepler Generation (SM 3.0) GK10x class
		{0x32, 192}, // Kepler Generation (SM 3.2) GK10x class
		{0x35, 192}, // Kepler Generation (SM 3.5) GK11x class
		{0x37, 192}, // Kepler Generation (SM 3.7) GK21x class
		{0x50, 128}, // Maxwell Generation (SM 5.0) GM10x class
		{0x52, 128}, // Maxwell Generation (SM 5.2) GM20x class
		{0x53, 128}, // Maxwell Generation (SM 5.3) GM20x class
		{0x60, 64}, // Pascal Generation (SM 6.0) GP100 class
		{0x61, 128}, // Pascal Generation (SM 6.1) GP10x class
		{0x62, 128}, // Pascal Generation (SM 6.2) GP10x class
		{0x70, 64}, // Volta Generation (SM 7.0) GV100 class
		{0x72, 64}, // Volta Generation (SM 7.2) GV11b class
		{-1, -1}};

	int index = 0;

	while (nGpuArchCoresPerSM[index].SM != -1)
	{
		if (nGpuArchCoresPerSM[index].SM == ((major << 4) + minor))
		{
			return nGpuArchCoresPerSM[index].Cores;
		}

		index++;
	}

	// If we don't find the values, we default use the previous one
	// to run properly
	return nGpuArchCoresPerSM[index - 1].Cores;
}

bool GpuAccessor::isDriverVersionOutOfData() const {return _errorStatus == cudaErrorInsufficientDriver;}

bool GpuAccessor::noNVidiaHardware() const {return _errorStatus == cudaErrorNoDevice;}

string GpuAccessor::getInfoString(int deviceIndex) const
{
	if ((doesGpuExist() == false) || (isDefaultGpuSet() == false))
	{
		return "";
	}

	cudaDeviceProp deviceProp = deviceProps.at(deviceIndex);

	MTIostringstream os;

	os << "Device " << deviceIndex << ": '" << deviceProp.name << "'" << std::endl;

	// Console log
	os << "  CUDA Driver Version / Runtime Version          ";
	os << getMajorDriverVersion() << "." << getMinorDriverVersion() << "\\ ";
	os << getMajorRuntimeVersion() << "." << getMinorRuntimeVersion() << std::endl;
	os << "  CUDA Capability Major/Minor version number:    ";
	os << deviceProp.major << "." << deviceProp.minor << std::endl;

	os << "  Total amount of global memory:                 ";
	os << static_cast<float>(deviceProp.totalGlobalMem / 1048576.0f) << " MBytes ";
	os << "(" << (unsigned long long)deviceProp.totalGlobalMem << ") bytes)" << std::endl;

	os << "  (" << deviceProp.multiProcessorCount << ") Multiprocessors, (";
	os << _ConvertSMVer2Cores(deviceProp.major, deviceProp.minor);
	os << ") CUDA Cores/MP:      " << _ConvertSMVer2Cores(deviceProp.major, deviceProp.minor)
		 * deviceProp.multiProcessorCount << " CUDA Cores" << std::endl;
	os << "  GPU Max Clock rate:                            " << deviceProp.clockRate * 1e-3f;
	os << " MHz (" << std::fixed << std::setprecision(2) << deviceProp.clockRate * 1e-6f << " GHz)" << std::endl;
	os << "  Memory Clock rate:                             " << std::setprecision(0);
	os << deviceProp.memoryClockRate * 1e-3f << " Mhz" << std::endl;
	os << "  Memory Bus Width:                              " << deviceProp.memoryBusWidth << "-bit" << std::endl;

	if (deviceProp.l2CacheSize)
	{
		os << "  L2 Cache Size:                                 " << deviceProp.l2CacheSize << " bytes" << std::endl;
	}

	os << "  Maximum Texture Dimension Size (x,y,z)         1D=(" << deviceProp.maxTexture1D << "),";
	os << " 2D = (" << deviceProp.maxTexture2D[0] << ", " << deviceProp.maxTexture2D[1] << "),";
	os << " 3D=(" << deviceProp.maxTexture3D[0] << ", " << deviceProp.maxTexture3D[1] << ", " << deviceProp.maxTexture3D
		 [2] << ")" << std::endl;

	os << "  Maximum Layered 1D Texture Size, (num) layers  1D=(" << deviceProp.maxTexture1DLayered[0]
		 << "), " << deviceProp.maxTexture1DLayered[1] << " layers" << std::endl;
	os << "  Maximum Layered 2D Texture Size, (num) layers  2D=(";
	os << deviceProp.maxTexture2DLayered[0] << ", " << deviceProp.maxTexture2DLayered[1] << "), ";
	os << deviceProp.maxTexture2DLayered[2] << " layers" << std::endl;

	os << "  Total amount of constant memory:               " << deviceProp.totalConstMem << " bytes" << std::endl;
	os << "  Total amount of shared memory per block:       " << deviceProp.sharedMemPerBlock << " bytes" << std::endl;
	os << "  Total number of registers available per block: " << deviceProp.regsPerBlock << "" << std::endl;
	os << "  Warp size:                                     " << deviceProp.warpSize << "" << std::endl;
	os << "  Maximum number of threads per multiprocessor:  " << deviceProp.maxThreadsPerMultiProcessor << "" <<
		 std::endl;
	os << "  Maximum number of threads per block:           " << deviceProp.maxThreadsPerBlock << "" << std::endl;

	os << "  Max dimension size of a thread block (x,y,z): (" << deviceProp.maxThreadsDim[0];
	os << ", " << deviceProp.maxThreadsDim[1] << ", " << deviceProp.maxThreadsDim[2] << ") " << std::endl;

	os << "  Max dimension size of a grid size    (x,y,z): (" << deviceProp.maxGridSize[0];
	os << ", " << deviceProp.maxGridSize[1] << ", " << deviceProp.maxGridSize[2] << ")" << std::endl;

	os << "  Maximum memory pitch:                          " << deviceProp.memPitch << " bytes" << std::endl;

	os << "  Texture alignment:                             " << deviceProp.textureAlignment << " bytes" << std::endl;

	auto yn = (deviceProp.deviceOverlap ? "Yes" : "No");
	os << "  Concurrent copy and kernel execution:          " << yn << " with " << deviceProp.asyncEngineCount <<
		 " copy engine(s)" << std::endl;

	os << "  Run time limit on kernels:                     " << (deviceProp.kernelExecTimeoutEnabled ? "Yes" : "No")
		 << std::endl;
	os << "  Integrated GPU sharing Host Memory:            " << (deviceProp.integrated ? "Yes" : "No") << std::endl;
	os << "  Support host page-locked memory mapping:       " << (deviceProp.canMapHostMemory ? "Yes" : "No")
		 << std::endl;
	os << "  Alignment requirement for Surfaces:            " << (deviceProp.surfaceAlignment ? "Yes" : "No")
		 << std::endl;
	os << "  Device has ECC support:                        " << (deviceProp.ECCEnabled ? "Enabled" : "Disabled")
		 << std::endl;

	auto ds = deviceProp.tccDriver ? "TCC (Tesla Compute Cluster Driver)" : "WDDM (Windows Display Driver Model)";
	os << "  CUDA Device Driver Mode (TCC or WDDM):         " << ds << std::endl;

	os << "  Device supports Unified Addressing (UVA):      " << (deviceProp.unifiedAddressing ? "Yes" : "No")
		 << std::endl;
	os << "  Device supports Compute Preemption:            " << (deviceProp.computePreemptionSupported ? "Yes" : "No")
		 << std::endl;
	os << "  Supports Cooperative Kernel Launch:            " << (deviceProp.cooperativeLaunch ? "Yes" : "No")
		 << std::endl;
	os << "  Supports MultiDevice Co-op Kernel Launch:      " << (deviceProp.cooperativeMultiDeviceLaunch ? "Yes" : "No")
		 << std::endl;
	os << "  Device PCI Domain ID / Bus ID / location ID:   " << deviceProp.pciDomainID << " / " <<
		 deviceProp.pciBusID << " / " << deviceProp.pciDeviceID << std::endl;
	const char *sComputeMode[] =
	{"Default (multiple host threads can use ::cudaSetDevice() with device " "simultaneously)",
		"Exclusive (only one host thread in one process is able to use " "::cudaSetDevice() with this device)",
		"Prohibited (no host thread can use ::cudaSetDevice() with this " "device)",
		"Exclusive Process (many threads in one process is able to use " "::cudaSetDevice() with this device)",
		"Unknown", NULL};
	os << "  Compute Mode:" << std::endl;
	os << "     < " << sComputeMode[deviceProp.computeMode] << " >" << std::endl;

	return os.str();
}
