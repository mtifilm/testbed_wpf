#ifndef CudaResampler16uH
#define CudaResampler16uH

#include "MtiCudaCoreInterface.h"

class MTI_CUDA_CORE_CPP_API CudaResampler16u : public CudaBaseClass
{
public:
	// Create the native resampler
	CudaResampler16u(int gpuDevice);
	~CudaResampler16u();

	// This translates the inArray to outArray by the delta, 
	// In and out arrays can be the same
	// The "missing" pixels are not changed; e.g. if a row is moved down by 1.5 pixels, 
	//    the top row of the outArray is not changed and the last row of the inArray is lost
	int translate(const Ipp16uArray &inArray, Ipp16uArray &outArray, float deltaX, float deltaY);
	int translateStartAsync(const Ipp16uArray &inArray, Ipp16uArray &outArray, float deltaX, float deltaY);
	int translateCopyResultAsync();
};
#endif
