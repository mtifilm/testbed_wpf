#ifndef MtiCudaCoreInterfaceH
#define MtiCudaCoreInterfaceH

// Just include all the interface files 
#include "MtiCudaCoreBasicInterface.h"
//#include "CudaException.h"
#include "GpuAccessor.h"
#include "CudaBaseClass.h"
#include "CudaResampler16u.h"
#include "CudaThinplateSpline16u.h"
#endif
