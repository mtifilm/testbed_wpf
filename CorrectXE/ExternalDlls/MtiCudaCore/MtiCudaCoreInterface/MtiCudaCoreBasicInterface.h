#ifndef MtiCudaCoreBasicInterfaceH
#define MtiCudaCoreBasicInterfaceH

// This is a native interface into the Visual stuido MtiCudaCore DLL
#include "Ippheaders.h"
#include <string>

// Define usual imports/exports
#ifdef MTICUDACOREINTERFACE_EXPORTS
#define MTI_CUDA_CORE_CPP_API __declspec(dllexport)
#else
#ifdef CUDAMTICORE_MANAGED
#define MTI_CUDA_CORE_CPP_API [System::Runtime::InteropServices::DllImport("TpssDLL.dll")] extern "C"
#else
#define MTI_CUDA_CORE_CPP_API __declspec(dllimport)
#endif
#endif

MTI_CUDA_CORE_CPP_API std::string MtiCudaGetErrorString(int status);

MTI_CUDA_CORE_CPP_API int MtiCudaGetDeviceCount(int &count);

MTI_CUDA_CORE_CPP_API std::string MtiCudaGetDeviceName(int deviceIndex);

MTI_CUDA_CORE_CPP_API std::pair<int, int> MtiCudaGetDeviceComputeCapability(int deviceIndex);

MTI_CUDA_CORE_CPP_API void *MtiCudaMallocHost(size_t bytes);

MTI_CUDA_CORE_CPP_API void MtiCudaFreeHost(void *ptr);

MTI_CUDA_CORE_CPP_API void MtiCudaSetDevice(int deviceIndex);

MTI_CUDA_CORE_CPP_API int MtiCudaDriverGetVersion();

MTI_CUDA_CORE_CPP_API int MtiCudaRuntimeGetVersion();
#endif
