	// MtiCudaCoreInterface.cpp : Defines the exported functions for the DLL application.
//
#include "MtiCudaCoreInterface.h"
#include "MtiCudaCore.h"
#include <sstream>
#include <stdexcept>
// We need to fix this

#ifdef __BORLANDC__
#include "ThreadLocker.h"
static CSpinLock MemLock;
#else

#include "MtiCore.h"
int MemLock;
class CAutoSpinLocker
{
public:
	CAutoSpinLocker(int MemLock) {}
};
#endif

std::string MtiCudaGetErrorString(int status)
{
	std::ostringstream os;
	os << "Cuda error: " << status << ", '" << MtiNativeCudaGetErrorCharPointer(status) << "'" << std::endl;
#ifdef __BORLANDC__
   theError.set(status, MtiNativeCudaGetErrorCharPointer(status));
#endif
	return os.str();
}

int MtiCudaGetDeviceCount(int &count)
{
	return MtiNativeCudaGetDeviceCount(count);
}

std::string MtiCudaGetDeviceName(int deviceIndex)
{
	// Cuda seems to define the max as 256
	char name[256];
	MtiNativeCudaGetDeviceName(deviceIndex, name, 256);
	if (name[0] == 0)
	{
		return "";
	}

	std::ostringstream os;
	os << name;
	return os.str();
}

MTI_CUDA_CORE_CPP_API std::pair<int, int> MtiCudaGetDeviceComputeCapability(int deviceIndex)
{
	std::pair<int, int> result;
	auto status = MtiNativeCudaGetDeviceComputeCapability(result.first, result.second, deviceIndex);
	if (status != 0)
	{
 		std::ostringstream os;
		os << "Could not get compute Capability" << std::endl;
		os << MtiCudaGetErrorString(status);
		throw std::runtime_error(os.str());
	}

	return result;
}

void *MtiCudaMallocHost(size_t bytes)
{
   CAutoSpinLocker lock(MemLock);
	void *h_pinned = nullptr;
   TRACE_2(errout << "Call MtiNativeCudaMallocHost(" << bytes << ")");
	auto status = MtiNativeCudaMallocHost((void**)&h_pinned, bytes);
   TRACE_2(errout << "Return from MtiNativeCudaMallocHost() --> " << status);
	if (status != 0)
	{
		std::ostringstream os;
		os << "Error allocating pinned memory " << std::endl;
		os << MtiCudaGetErrorString(status);
		throw std::runtime_error(os.str());
	}

	return h_pinned;

}

void MtiCudaFreeHost(void * ptr)
{
   CAutoSpinLocker lock(MemLock);
   TRACE_2(errout << "Call MtiNativeCudaFreeHost()");
	auto status = MtiNativeCudaFreeHost(ptr);
   TRACE_2(errout << "Return from MtiNativeCudaFreeHost() --> " << status);
	if (status != 0)
	{
		std::ostringstream os;
		os << "Error freeing pinned memory " << std::endl;
		os << MtiCudaGetErrorString(status);
		throw std::runtime_error(os.str());
	}
}

MTI_CUDA_CORE_CPP_API void MtiCudaSetDevice(int deviceIndex)
{
	auto status = MtiNativeCudaSetDevice(deviceIndex);
	if (status != 0)
	{
		std::ostringstream os;
		os << "Error setting GPU device: " << deviceIndex << std::endl;
		os << MtiCudaGetErrorString(status);
		throw std::runtime_error(os.str());
	}
}

int MtiCudaDriverGetVersion()
{
	return MtiNativeCudaDriverGetVersion();
}

int MtiCudaRuntimeGetVersion()
{
	return MtiNativeCudaRuntimeGetVersion();
}
