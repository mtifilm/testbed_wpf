#include "CudaBaseClass.h"

#include "CudaNativeBaseNativeInterface.h"

CudaBaseClass::CudaBaseClass(int gpuIndex)
{
	_gpuIndex = gpuIndex;
}


CudaBaseClass::~CudaBaseClass()
{
}

void CudaBaseClass::wait()
{
	auto status = MtiNativeCudaWait(_nativeClass);
	if (status != 0)
	{
		std::ostringstream os;
		os << "Error in " << __func__ << ", code: " << status << ", error " << MtiNativeCudaGetErrorCharPointer(status);
		throw std::runtime_error(os.str());
	}

	return;
}

void CudaBaseClass::setMinMax(Ipp16u minValue, Ipp16u maxValue)
{
	auto status = MtiNativeCudaSetMinMax(_nativeClass, minValue, maxValue);
	if (status != 0)
	{
		std::ostringstream os;
		os << "Error in " <<  __func__ << ", code: " << status << ", error " << MtiNativeCudaGetErrorCharPointer(status);
		throw std::runtime_error(os.str());
	}
}

void CudaBaseClass::setInputArray(const Ipp16uArray &inArray)
{
	auto status = MtiNativeCudaSetInputArray(
		_nativeClass,
		inArray.data(),
		inArray.getRowPitchInBytes(),
		inArray.getHeight(),
		inArray.getWidth(),
		inArray.getComponents()
		);

	if (status != 0)
	{
		std::ostringstream os;
		os << "Error in " << __func__ << ", code: " << status << ", error " << MtiNativeCudaGetErrorCharPointer(status);
		throw std::runtime_error(os.str());
	}

	_inArray = inArray;
}

void CudaBaseClass::setOutputArray(Ipp16uArray & outArray)
{
	auto status = MtiNativeCudaSetOutputArray(
		_nativeClass,
		outArray.data(),
		outArray.getRowPitchInBytes(),
		outArray.getHeight(),
		outArray.getWidth(),
		outArray.getComponents()
	);

	if (status != 0)
	{
		std::ostringstream os;
		os << "Error in " << __func__ << ", code: " << status << ", error " << MtiNativeCudaGetErrorCharPointer(status);
		throw std::runtime_error(os.str());
	}

	_outArray = outArray;
}

void CudaBaseClass::setAndLoadAncillaryArrayAsync(Ipp16uArray & outArray)
{
	// the 3 is temp for now, must decide how to do it
	auto status = MtiNativeCudaSetArray(
		_nativeClass,
		CudaArrayMnemonic::Ancillary,
		outArray.data(),
		outArray.getRowPitchInBytes(),
		outArray.getHeight(),
		outArray.getWidth(),
		outArray.getComponents()
	);

	if (status != 0)
	{
		std::ostringstream os;
		os << "Error in " << __func__ << "-setArray, code: " << status << ", error " << MtiNativeCudaGetErrorCharPointer(status);
		throw std::runtime_error(os.str());
	}

	status = MtiNativeLoadArrayToDeviceAsync(
		_nativeClass,
		CudaArrayMnemonic::Ancillary,
		outArray.data(),
		outArray.getRowPitchInBytes()
	);

	if (status != 0)
	{
		std::ostringstream os;
		os << "Error in " << __func__ << "-loadArrayToDeviceAsync, code: " << status << ", error " << MtiNativeCudaGetErrorCharPointer(status);
		throw std::runtime_error(os.str());
	}
}

void CudaBaseClass::clearMaskArray()
{
	auto status = MtiNativeCudaSetMaskArray(_nativeClass, nullptr, 0, 0, 0, 0);
	if (status != 0)
	{
		std::ostringstream os;
		os << "Error in " << __func__ << ", code: " << status << ", error " << MtiNativeCudaGetErrorCharPointer(status);
      DBTRACE(os.str());
		throw std::runtime_error(os.str());
	}
}

void CudaBaseClass::setMaskArray(const Ipp8uArray & maskArray)
{
	auto status = MtiNativeCudaSetMaskArray(
		_nativeClass,
		maskArray.data(),
		maskArray.getRowPitchInBytes(),
		maskArray.getHeight(),
		maskArray.getWidth(),
		maskArray.getComponents()
	);

	if (status != 0)
	{
		std::ostringstream os;
		os << "Error in " << __func__ << ", code: " << status << ", error " << MtiNativeCudaGetErrorCharPointer(status);
		throw std::runtime_error(os.str());
	}
}

void CudaBaseClass::setInputRoi(const IppiRect & roi)
{
	auto status = MtiNativeCudaSetInputRoi(_nativeClass, roi);
	if (status != 0)
	{
		std::ostringstream os;
		os << "Error in " << __func__ << ", code: " << status << ", error " << MtiNativeCudaGetErrorCharPointer(status);
		throw std::runtime_error(os.str());
	}
}

void CudaBaseClass::setOutputRoi(const IppiRect & roi)
{
	auto status = MtiNativeCudaSetOutputRoi(_nativeClass, roi);
	if (status != 0)
	{
		std::ostringstream os;
		os << "Error in " << __func__ << ", code: " << status << ", error " << MtiNativeCudaGetErrorCharPointer(status);
		throw std::runtime_error(os.str());
	}
}

void CudaBaseClass::copyOutputFromDeviceAsync(const IppiRect &deviceRoi, const IppiRect &hostRoi)
{
	auto status = MtiNativeCudaCopyOutputFromDeviceAsync(_nativeClass, deviceRoi, hostRoi);
	if (status != 0)
	{
		std::ostringstream os;
		os << "Error in " << __func__ << ", code: " << status << ", error " << MtiNativeCudaGetErrorCharPointer(status);
		throw std::runtime_error(os.str());
	}
}

void CudaBaseClass::copyOutputFromDeviceAsync()
{
	copyOutputFromDeviceAsync({ 0, 0, 0 ,0 }, { 0, 0, 0 ,0 });
}
