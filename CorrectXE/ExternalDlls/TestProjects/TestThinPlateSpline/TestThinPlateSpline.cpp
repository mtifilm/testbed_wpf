// TestCudaMtiCoreC.cpp : Defines the entry point for the console application.
//

#include "MtiCudaCoreInterface.h"
#include "DevSupportLib.h"
#include "Ippheaders.h"
#include "HRTimer.h"
#include "Dpx\dpx.h"

// Data for test
int test_data_width = 2000;
int test_data_height = 1440;

//std::vector<IppiPoint> getPositions2()
//{
//	std::vector<IppiPoint> result(176);
//}

std::vector<IppiPoint> getPositions()
{
	std::vector<IppiPoint> result(176);

	result[0] = { 34, 29 };
	result[1] = { 164, 29 };
	result[2] = { 293, 29 };
	result[3] = { 423, 29 };
	result[4] = { 553, 29 };
	result[5] = { 683, 29 };
	result[6] = { 812, 29 };
	result[7] = { 942, 29 };
	result[8] = { 1072, 29 };
	result[9] = { 1202, 29 };
	result[10] = { 1331, 29 };
	result[11] = { 1461, 29 };
	result[12] = { 1591, 29 };
	result[13] = { 1721, 29 };
	result[14] = { 1850, 29 };
	result[15] = { 1980, 29 };
	result[16] = { 34, 170 };
	result[17] = { 164, 170 };
	result[18] = { 293, 170 };
	result[19] = { 423, 170 };
	result[20] = { 553, 170 };
	result[21] = { 683, 170 };
	result[22] = { 812, 170 };
	result[23] = { 942, 170 };
	result[24] = { 1072, 170 };
	result[25] = { 1202, 170 };
	result[26] = { 1331, 170 };
	result[27] = { 1461, 170 };
	result[28] = { 1591, 170 };
	result[29] = { 1721, 170 };
	result[30] = { 1850, 170 };
	result[31] = { 1980, 170 };
	result[32] = { 34, 311 };
	result[33] = { 164, 311 };
	result[34] = { 293, 311 };
	result[35] = { 423, 311 };
	result[36] = { 553, 311 };
	result[37] = { 683, 311 };
	result[38] = { 812, 311 };
	result[39] = { 942, 311 };
	result[40] = { 1072, 311 };
	result[41] = { 1202, 311 };
	result[42] = { 1331, 311 };
	result[43] = { 1461, 311 };
	result[44] = { 1591, 311 };
	result[45] = { 1721, 311 };
	result[46] = { 1850, 311 };
	result[47] = { 1980, 311 };
	result[48] = { 34, 452 };
	result[49] = { 164, 452 };
	result[50] = { 293, 452 };
	result[51] = { 423, 452 };
	result[52] = { 553, 452 };
	result[53] = { 683, 452 };
	result[54] = { 812, 452 };
	result[55] = { 942, 452 };
	result[56] = { 1072, 452 };
	result[57] = { 1202, 452 };
	result[58] = { 1331, 452 };
	result[59] = { 1461, 452 };
	result[60] = { 1591, 452 };
	result[61] = { 1721, 452 };
	result[62] = { 1850, 452 };
	result[63] = { 1980, 452 };
	result[64] = { 34, 593 };
	result[65] = { 164, 593 };
	result[66] = { 293, 593 };
	result[67] = { 423, 593 };
	result[68] = { 553, 593 };
	result[69] = { 683, 593 };
	result[70] = { 812, 593 };
	result[71] = { 942, 593 };
	result[72] = { 1072, 593 };
	result[73] = { 1202, 593 };
	result[74] = { 1331, 593 };
	result[75] = { 1461, 593 };
	result[76] = { 1591, 593 };
	result[77] = { 1721, 593 };
	result[78] = { 1850, 593 };
	result[79] = { 1980, 593 };
	result[80] = { 34, 735 };
	result[81] = { 164, 735 };
	result[82] = { 293, 735 };
	result[83] = { 423, 735 };
	result[84] = { 553, 735 };
	result[85] = { 683, 735 };
	result[86] = { 812, 735 };
	result[87] = { 942, 735 };
	result[88] = { 1072, 735 };
	result[89] = { 1202, 735 };
	result[90] = { 1331, 735 };
	result[91] = { 1461, 735 };
	result[92] = { 1591, 735 };
	result[93] = { 1721, 735 };
	result[94] = { 1850, 735 };
	result[95] = { 1980, 735 };
	result[96] = { 34, 876 };
	result[97] = { 164, 876 };
	result[98] = { 293, 876 };
	result[99] = { 423, 876 };
	result[100] = { 553, 876 };
	result[101] = { 683, 876 };
	result[102] = { 812, 876 };
	result[103] = { 942, 876 };
	result[104] = { 1072, 876 };
	result[105] = { 1202, 876 };
	result[106] = { 1331, 876 };
	result[107] = { 1461, 876 };
	result[108] = { 1591, 876 };
	result[109] = { 1721, 876 };
	result[110] = { 1850, 876 };
	result[111] = { 1980, 876 };
	result[112] = { 34, 1017 };
	result[113] = { 164, 1017 };
	result[114] = { 293, 1017 };
	result[115] = { 423, 1017 };
	result[116] = { 553, 1017 };
	result[117] = { 683, 1017 };
	result[118] = { 812, 1017 };
	result[119] = { 942, 1017 };
	result[120] = { 1072, 1017 };
	result[121] = { 1202, 1017 };
	result[122] = { 1331, 1017 };
	result[123] = { 1461, 1017 };
	result[124] = { 1591, 1017 };
	result[125] = { 1721, 1017 };
	result[126] = { 1850, 1017 };
	result[127] = { 1980, 1017 };
	result[128] = { 34, 1158 };
	result[129] = { 164, 1158 };
	result[130] = { 293, 1158 };
	result[131] = { 423, 1158 };
	result[132] = { 553, 1158 };
	result[133] = { 683, 1158 };
	result[134] = { 812, 1158 };
	result[135] = { 942, 1158 };
	result[136] = { 1072, 1158 };
	result[137] = { 1202, 1158 };
	result[138] = { 1331, 1158 };
	result[139] = { 1461, 1158 };
	result[140] = { 1591, 1158 };
	result[141] = { 1721, 1158 };
	result[142] = { 1850, 1158 };
	result[143] = { 1980, 1158 };
	result[144] = { 34, 1299 };
	result[145] = { 164, 1299 };
	result[146] = { 293, 1299 };
	result[147] = { 423, 1299 };
	result[148] = { 553, 1299 };
	result[149] = { 683, 1299 };
	result[150] = { 812, 1299 };
	result[151] = { 942, 1299 };
	result[152] = { 1072, 1299 };
	result[153] = { 1202, 1299 };
	result[154] = { 1331, 1299 };
	result[155] = { 1461, 1299 };
	result[156] = { 1591, 1299 };
	result[157] = { 1721, 1299 };
	result[158] = { 1850, 1299 };
	result[159] = { 1980, 1299 };
	result[160] = { 34, 1439 };
	result[161] = { 164, 1439 };
	result[162] = { 293, 1439 };
	result[163] = { 423, 1439 };
	result[164] = { 553, 1439 };
	result[165] = { 683, 1439 };
	result[166] = { 812, 1439 };
	result[167] = { 942, 1439 };
	result[168] = { 1072, 1439 };
	result[169] = { 1202, 1439 };
	result[170] = { 1331, 1439 };
	result[171] = { 1461, 1439 };
	result[172] = { 1591, 1439 };
	result[173] = { 1721, 1439 };
	result[174] = { 1850, 1439 };
	result[175] = { 1980, 1439 };

	return result;
}
std::vector<std::vector<float>> getWeights()
{
	std::vector<float> result(179);
	result[0] = -5.80903e-07f;
	result[1] = 1.62723e-06f;
	result[2] = -9.89892e-07f;
	result[3] = -1.66343e-06f;
	result[4] = 1.32829e-06f;
	result[5] = -1.27414e-06f;
	result[6] = 7.58721e-07f;
	result[7] = 1.96334e-06f;
	result[8] = -1.44268e-06f;
	result[9] = -6.97943e-07f;
	result[10] = 1.44769e-06f;
	result[11] = -6.93183e-08f;
	result[12] = -1.18817e-06f;
	result[13] = 1.57678e-06f;
	result[14] = -9.65562e-07f;
	result[15] = 1.69979e-08f;
	result[16] = -1.04502e-06f;
	result[17] = 1.82761e-06f;
	result[18] = 3.00078e-07f;
	result[19] = -4.20548e-07f;
	result[20] = 1.02544e-06f;
	result[21] = -1.18929e-06f;
	result[22] = 1.84311e-06f;
	result[23] = -1.16292e-06f;
	result[24] = 2.55702e-08f;
	result[25] = -3.76702e-07f;
	result[26] = 2.72862e-07f;
	result[27] = -4.67867e-07f;
	result[28] = -6.87159e-07f;
	result[29] = 2.46603e-06f;
	result[30] = -1.0819e-06f;
	result[31] = 1.78666e-07f;
	result[32] = -9.84247e-07f;
	result[33] = 1.43297e-06f;
	result[34] = -4.65639e-07f;
	result[35] = -1.02614e-06f;
	result[36] = 2.29334e-08f;
	result[37] = -3.83681e-07f;
	result[38] = 6.79249e-07f;
	result[39] = -9.99735e-07f;
	result[40] = 5.59896e-08f;
	result[41] = -8.95351e-07f;
	result[42] = 2.36473e-06f;
	result[43] = -1.07917e-06f;
	result[44] = -1.30944e-06f;
	result[45] = 3.96229e-07f;
	result[46] = -7.07272e-07f;
	result[47] = 1.56471e-07f;
	result[48] = -9.13295e-07f;
	result[49] = 1.98538e-06f;
	result[50] = -7.31881e-07f;
	result[51] = 2.51768e-07f;
	result[52] = -1.82759e-07f;
	result[53] = -7.10389e-07f;
	result[54] = 6.26146e-07f;
	result[55] = 2.50967e-07f;
	result[56] = 1.90638e-07f;
	result[57] = -5.31685e-07f;
	result[58] = -2.63353e-07f;
	result[59] = 1.57994e-06f;
	result[60] = -1.75849e-06f;
	result[61] = 3.03504e-06f;
	result[62] = -1.37365e-06f;
	result[63] = 2.27972e-07f;
	result[64] = -4.46377e-07f;
	result[65] = 4.72929e-07f;
	result[66] = -1.77817e-07f;
	result[67] = -7.31748e-07f;
	result[68] = 2.0431e-06f;
	result[69] = -6.70851e-07f;
	result[70] = -2.58833e-07f;
	result[71] = 7.6993e-07f;
	result[72] = -1.39446e-07f;
	result[73] = -5.48059e-07f;
	result[74] = -4.07856e-08f;
	result[75] = 4.00071e-07f;
	result[76] = -2.34172e-06f;
	result[77] = 2.39689e-06f;
	result[78] = -1.48924e-06f;
	result[79] = 2.60529e-07f;
	result[80] = -4.29333e-07f;
	result[81] = 6.72631e-07f;
	result[82] = 1.24665e-07f;
	result[83] = -9.58983e-07f;
	result[84] = 4.32264e-07f;
	result[85] = -1.15617e-06f;
	result[86] = 1.37668e-06f;
	result[87] = -3.53771e-07f;
	result[88] = -8.0147e-07f;
	result[89] = 7.00526e-07f;
	result[90] = -9.81563e-07f;
	result[91] = 3.37237e-06f;
	result[92] = -2.8398e-06f;
	result[93] = 2.82597e-06f;
	result[94] = -1.39891e-06f;
	result[95] = 2.37538e-07f;
	result[96] = -7.25935e-07f;
	result[97] = 1.23765e-06f;
	result[98] = -5.3787e-07f;
	result[99] = -4.46527e-07f;
	result[100] = -8.97365e-08f;
	result[101] = -2.38999e-07f;
	result[102] = 1.07132e-06f;
	result[103] = -6.466e-08f;
	result[104] = -3.36254e-07f;
	result[105] = -3.33191e-07f;
	result[106] = -1.15965e-07f;
	result[107] = 5.76853e-07f;
	result[108] = -2.0743e-06f;
	result[109] = 1.30848e-06f;
	result[110] = -1.06342e-06f;
	result[111] = 2.46632e-07f;
	result[112] = -6.58213e-07f;
	result[113] = 1.42412e-06f;
	result[114] = 1.20343e-06f;
	result[115] = -3.35491e-07f;
	result[116] = -4.85523e-07f;
	result[117] = 8.29957e-07f;
	result[118] = -5.32108e-07f;
	result[119] = 3.21916e-07f;
	result[120] = -3.82851e-07f;
	result[121] = -1.71991e-07f;
	result[122] = 3.92084e-07f;
	result[123] = 1.00273e-06f;
	result[124] = -2.25849e-06f;
	result[125] = 4.62126e-06f;
	result[126] = -2.45524e-06f;
	result[127] = 5.07178e-07f;
	result[128] = -5.75811e-07f;
	result[129] = 1.1834e-07f;
	result[130] = -2.01015e-06f;
	result[131] = -6.24906e-07f;
	result[132] = 6.39346e-08f;
	result[133] = 1.1839e-06f;
	result[134] = -1.40715e-06f;
	result[135] = 1.28658e-06f;
	result[136] = -1.03476e-06f;
	result[137] = -2.68454e-07f;
	result[138] = 7.38423e-07f;
	result[139] = -1.17124e-06f;
	result[140] = -1.3113e-06f;
	result[141] = 1.42303e-06f;
	result[142] = -1.61272e-06f;
	result[143] = 4.87752e-07f;
	result[144] = -4.09066e-07f;
	result[145] = 1.76831e-06f;
	result[146] = 1.13975e-06f;
	result[147] = -1.48137e-06f;
	result[148] = 2.18463e-06f;
	result[149] = -2.11514e-06f;
	result[150] = 1.32982e-06f;
	result[151] = -2.50698e-07f;
	result[152] = 2.5832e-07f;
	result[153] = -6.95665e-07f;
	result[154] = 4.04638e-07f;
	result[155] = 2.13644e-06f;
	result[156] = -2.06249e-06f;
	result[157] = 3.04957e-06f;
	result[158] = -1.73921e-06f;
	result[159] = 2.09756e-07f;
	result[160] = -1.76294e-06f;
	result[161] = 3.58386e-06f;
	result[162] = -2.46739e-06f;
	result[163] = -1.18828e-06f;
	result[164] = 2.21688e-06f;
	result[165] = -1.44585e-06f;
	result[166] = 1.40162e-07f;
	result[167] = 6.89167e-07f;
	result[168] = 4.32966e-07f;
	result[169] = -9.65986e-07f;
	result[170] = 4.19226e-07f;
	result[171] = -6.55518e-07f;
	result[172] = -1.27877e-06f;
	result[173] = 2.50164e-06f;
	result[174] = -2.02521e-06f;
	result[175] = 7.39741e-07f;
	result[176] = 0.00251856f;
	result[177] = -4.97436e-07f;
	result[178] = -8.36642e-08f;

	//vector<float> result2;
	//for (auto w : result)
	//{
	//	result2.push_back(2 * w);
	//}

	//auto result3 = result;
	//std::reverse(result3.begin(), result3.end());

	return { result, result, result };
}

std::vector<IppiPoint> getPositionsArmyMan()
{
	vector<IppiPoint> result = {
		{ 0,0 },
	{ 118,0 },
	{ 235,0 },
	{ 353,0 },
	{ 470,0 },
	{ 588,0 },
	{ 706,0 },
	{ 823,0 },
	{ 941,0 },
	{ 1058,0 },
	{ 1176,0 },
	{ 1293,0 },
	{ 1411,0 },
	{ 1529,0 },
	{ 1646,0 },
	{ 1764,0 },
	{ 1881,0 },
	{ 1999,0 },
	{ 0,131 },
	{ 118,131 },
	{ 235,131 },
	{ 353,131 },
	{ 470,131 },
	{ 588,131 },
	{ 706,131 },
	{ 823,131 },
	{ 941,131 },
	{ 1058,131 },
	{ 1176,131 },
	{ 1293,131 },
	{ 1411,131 },
	{ 1529,131 },
	{ 1646,131 },
	{ 1764,131 },
	{ 1881,131 },
	{ 1999,131 },
	{ 0,262 },
	{ 118,262 },
	{ 235,262 },
	{ 353,262 },
	{ 470,262 },
	{ 588,262 },
	{ 706,262 },
	{ 823,262 },
	{ 941,262 },
	{ 1058,262 },
	{ 1176,262 },
	{ 1293,262 },
	{ 1411,262 },
	{ 1529,262 },
	{ 1646,262 },
	{ 1764,262 },
	{ 1881,262 },
	{ 1999,262 },
	{ 0,392 },
	{ 118,392 },
	{ 235,392 },
	{ 353,392 },
	{ 470,392 },
	{ 588,392 },
	{ 706,392 },
	{ 823,392 },
	{ 941,392 },
	{ 1058,392 },
	{ 1176,392 },
	{ 1293,392 },
	{ 1411,392 },
	{ 1529,392 },
	{ 1646,392 },
	{ 1764,392 },
	{ 1881,392 },
	{ 1999,392 },
	{ 0,523 },
	{ 118,523 },
	{ 235,523 },
	{ 353,523 },
	{ 470,523 },
	{ 588,523 },
	{ 706,523 },
	{ 823,523 },
	{ 941,523 },
	{ 1058,523 },
	{ 1176,523 },
	{ 1293,523 },
	{ 1411,523 },
	{ 1529,523 },
	{ 1646,523 },
	{ 1764,523 },
	{ 1881,523 },
	{ 1999,523 },
	{ 0,654 },
	{ 118,654 },
	{ 235,654 },
	{ 353,654 },
	{ 470,654 },
	{ 588,654 },
	{ 706,654 },
	{ 823,654 },
	{ 941,654 },
	{ 1058,654 },
	{ 1176,654 },
	{ 1293,654 },
	{ 1411,654 },
	{ 1529,654 },
	{ 1646,654 },
	{ 1764,654 },
	{ 1881,654 },
	{ 1999,654 },
	{ 0,785 },
	{ 118,785 },
	{ 235,785 },
	{ 353,785 },
	{ 470,785 },
	{ 588,785 },
	{ 706,785 },
	{ 823,785 },
	{ 941,785 },
	{ 1058,785 },
	{ 1176,785 },
	{ 1293,785 },
	{ 1411,785 },
	{ 1529,785 },
	{ 1646,785 },
	{ 1764,785 },
	{ 1881,785 },
	{ 1999,785 },
	{ 0,916 },
	{ 118,916 },
	{ 235,916 },
	{ 353,916 },
	{ 470,916 },
	{ 588,916 },
	{ 706,916 },
	{ 823,916 },
	{ 941,916 },
	{ 1058,916 },
	{ 1176,916 },
	{ 1293,916 },
	{ 1411,916 },
	{ 1529,916 },
	{ 1646,916 },
	{ 1764,916 },
	{ 1881,916 },
	{ 1999,916 },
	{ 0,1047 },
	{ 118,1047 },
	{ 235,1047 },
	{ 353,1047 },
	{ 470,1047 },
	{ 588,1047 },
	{ 706,1047 },
	{ 823,1047 },
	{ 941,1047 },
	{ 1058,1047 },
	{ 1176,1047 },
	{ 1293,1047 },
	{ 1411,1047 },
	{ 1529,1047 },
	{ 1646,1047 },
	{ 1764,1047 },
	{ 1881,1047 },
	{ 1999,1047 },
	{ 0,1177 },
	{ 118,1177 },
	{ 235,1177 },
	{ 353,1177 },
	{ 470,1177 },
	{ 588,1177 },
	{ 706,1177 },
	{ 823,1177 },
	{ 941,1177 },
	{ 1058,1177 },
	{ 1176,1177 },
	{ 1293,1177 },
	{ 1411,1177 },
	{ 1529,1177 },
	{ 1646,1177 },
	{ 1764,1177 },
	{ 1881,1177 },
	{ 1999,1177 },
	{ 0,1308 },
	{ 118,1308 },
	{ 235,1308 },
	{ 353,1308 },
	{ 470,1308 },
	{ 588,1308 },
	{ 706,1308 },
	{ 823,1308 },
	{ 941,1308 },
	{ 1058,1308 },
	{ 1176,1308 },
	{ 1293,1308 },
	{ 1411,1308 },
	{ 1529,1308 },
	{ 1646,1308 },
	{ 1764,1308 },
	{ 1881,1308 },
	{ 1999,1308 },
	{ 0,1439 },
	{ 118,1439 },
	{ 235,1439 },
	{ 353,1439 },
	{ 470,1439 },
	{ 588,1439 },
	{ 706,1439 },
	{ 823,1439 },
	{ 941,1439 },
	{ 1058,1439 },
	{ 1176,1439 },
	{ 1293,1439 },
	{ 1411,1439 },
	{ 1529,1439 },
	{ 1646,1439 },
	{ 1764,1439 },
	{ 1881,1439 },
	{ 1999,1439 }
};

	return result;
};

std::vector<std::vector<float>> getWeightsArmyMan()
{
	vector<float> result = {
		1.25264e-05f,
		-2.34798e-05f,
		-2.85481e-07f,
		4.57549e-06f,
		-1.43126e-05f,
		7.73333e-06f,
		-1.61499e-05f,
		2.96351e-05f,
		-5.50467e-06f,
		2.7318e-07f,
		1.30984e-06f,
		-2.05064e-06f,
		-3.77705e-06f,
		-5.94099e-07f,
		1.38919e-05f,
		4.40046e-06f,
		-1.03938e-05f,
		3.91672e-07f,
		-3.53538e-06f,
		1.41307e-05f,
		1.79531e-05f,
		1.08332e-05f,
		-1.9781e-05f,
		1.07222e-05f,
		9.02084e-07f,
		5.64126e-06f,
		-2.767e-05f,
		1.66976e-05f,
		-1.37153e-05f,
		-6.09668e-08f,
		1.13317e-05f,
		1.38511e-06f,
		-1.83693e-05f,
		-1.67944e-05f,
		1.94599e-05f,
		-6.79935e-06f,
		4.41032e-06f,
		-1.74817e-05f,
		-1.20856e-05f,
		1.24866e-05f,
		-2.286e-05f,
		3.50389e-05f,
		-9.73488e-06f,
		-9.14144e-06f,
		1.5201e-05f,
		1.34043e-05f,
		-1.22625e-05f,
		-5.59511e-06f,
		1.86262e-05f,
		-1.33572e-05f,
		-4.68356e-06f,
		3.04745e-05f,
		1.15687e-06f,
		1.01824e-06f,
		-1.58737e-07f,
		3.99493e-07f,
		6.98227e-06f,
		3.0665e-06f,
		8.73039e-07f,
		-4.54364e-05f,
		2.88586e-05f,
		-8.44537e-06f,
		-2.44339e-05f,
		-1.53336e-05f,
		1.28424e-05f,
		-4.64492e-06f,
		1.6296e-05f,
		-2.82548e-05f,
		1.80403e-05f,
		-2.49723e-05f,
		7.75301e-06f,
		-1.54324e-05f,
		-3.89866e-06f,
		1.37021e-05f,
		-7.15955e-06f,
		-1.27321e-05f,
		1.83616e-06f,
		5.52082e-05f,
		-3.52209e-05f,
		2.38083e-05f,
		-3.14887e-05f,
		6.39727e-05f,
		8.08131e-06f,
		-1.50677e-05f,
		1.0036e-06f,
		-1.31071e-06f,
		2.2601e-05f,
		-1.28762e-05f,
		4.09926e-06f,
		1.39578e-05f,
		8.61465e-06f,
		-1.77978e-05f,
		3.66612e-06f,
		8.55061e-06f,
		-9.91804e-06f,
		-1.74652e-05f,
		2.63781e-06f,
		-6.42943e-08f,
		1.50181e-06f,
		-6.94649e-06f,
		-2.33113e-05f,
		-1.2575e-05f,
		1.84079e-06f,
		4.45931e-06f,
		-8.89066e-07f,
		-1.39244e-05f,
		-1.04763e-05f,
		1.59295e-06f,
		-1.136e-05f,
		1.55725e-05f,
		-1.13276e-05f,
		-2.66363e-08f,
		1.87897e-05f,
		1.69277e-05f,
		3.17241e-06f,
		-1.29046e-07f,
		-8.01327e-06f,
		-1.367e-05f,
		1.46397e-05f,
		9.66035e-06f,
		-3.91685e-06f,
		1.41607e-05f,
		-8.80948e-06f,
		1.9779e-05f,
		1.28707e-06f,
		-5.75326e-07f,
		1.15626e-05f,
		-2.21694e-05f,
		4.64929e-06f,
		1.19793e-05f,
		-6.94643e-06f,
		-3.4857e-05f,
		2.74661e-06f,
		3.43455e-05f,
		-6.74002e-05f,
		9.15929e-05f,
		-1.67891e-06f,
		-2.06254e-05f,
		1.81441e-05f,
		-7.06893e-06f,
		-1.9222e-06f,
		-5.17409e-06f,
		4.7226e-07f,
		3.26537e-06f,
		-2.57644e-06f,
		-1.2464e-05f,
		4.28524e-05f,
		-2.63048e-05f,
		2.94151e-06f,
		1.21474e-06f,
		-7.22872e-06f,
		-2.99378e-05f,
		-1.32532e-05f,
		1.46501e-05f,
		-3.74837e-05f,
		7.11847e-06f,
		-1.48235e-05f,
		-1.02148e-05f,
		9.24379e-06f,
		2.48722e-07f,
		-5.7223e-06f,
		3.11064e-06f,
		8.49575e-06f,
		1.50718e-06f,
		-1.65486e-05f,
		-3.40456e-05f,
		3.50984e-05f,
		6.05761e-05f,
		-3.02514e-05f,
		4.46362e-05f,
		-6.88603e-06f,
		8.83654e-06f,
		-5.98969e-05f,
		0.000116112f,
		-3.8434e-05f,
		3.97089e-06f,
		-1.80115e-06f,
		-2.74114e-06f,
		7.41417e-06f,
		-1.14231e-05f,
		-1.2802e-06f,
		2.80547e-06f,
		1.46992e-05f,
		2.28963e-06f,
		-7.42243e-06f,
		-3.32391e-05f,
		-1.21222e-05f,
		-1.06484e-05f,
		1.40429e-05f,
		-7.52144e-06f,
		1.65877e-05f,
		-3.74432e-05f,
		6.01742e-07f,
		-6.49144e-06f,
		7.70073e-06f,
		5.12408e-06f,
		-4.06128e-07f,
		9.2043e-06f,
		7.89013e-06f,
		-2.9803e-05f,
		2.28947e-05f,
		-8.95747e-06f,
		-2.57296e-06f,
		-3.26517e-06f,
		3.23237e-05f,
		8.70233e-06f,
		-1.71014e-05f,
		-1.5895e-05f,
		1.64577e-05f,
		-2.26221e-06f,
		1.70986e-05f,
		-2.65237e-06f,
		3.2868e-06f,
		-1.25929e-05f,
		-1.6713e-06f,
		-2.57443e-06f,
		0.0119999f,
		-3.60488e-06f,
		2.74939e-06f
	};

	return { result, result, result };
}


using namespace std;

int main(int argc, const char * argv[])
{
	GpuAccessor gpuAccessor; 

	if (gpuAccessor.doesGpuExist() == false)
	{
		std::cout << "No GPU found " << std::endl;
	}

	std::cout << "Number of gpus: " << gpuAccessor.getDeviceCount() << std::endl;
	std::cout << "Default gpu: " << gpuAccessor.getDefaultGpu() << ", name: " << gpuAccessor.getDeviceName() << std::endl;
	std::cout << "Compute Capability: " << gpuAccessor.getMajorComputeCapability() << "." << gpuAccessor.getMinorComputeCapability() << std::endl;

	std::string fileName = "D:\\CorrectData\\dewarp\\man_r3.017714.dpx";
	fileName = "D:\\CorrectData\\Flicker\\do_testow_mti_pancerni\\tvp_pancerni_mti_test_footage_000862.dpx";
	///fileName = "Y:\\tmp\\matlab\\C3P_R12_1041007.dpx";
	if (argc > 1)
	{
		fileName = string(argv[1]);
	}

	auto[input, bitDepth] = DevIO::readDpxFile(fileName, false);
	auto maxValue = (1 << bitDepth) - 1;
	auto width = input.getWidth();
	auto height = input.getHeight();

	cout << "Testing " << fileName << std::endl;
	cout << "(" << input.getWidth() << "," << input.getHeight() << ")" << std::endl;

	auto sizeInBytes = input.area()*input.getComponents() * sizeof(Ipp16u);
	auto p = (Ipp16u *)DevIO::getPinnedMemory(sizeInBytes);
	Ipp16uArray output({ input.getSize(), 3 }, p);
	output.set({ 512, 0, 0 });

	string outfileName = "C:\\temp\\vsOriginal.mbin";
	//DevIO::dumpMatlabBinary(input, bitDepth, outfileName);

	int deviceId = 0;

	auto mp = (Ipp8u *)DevIO::getPinnedMemory(input.area()*input.getComponents());
	Ipp8uArray mask({ width, height }, mp);
	mask.zero();
	int N = 320;
	mask({ N, N, width - 2 * N, height - 2 * N }).set({ 255 });

	auto weights = getWeightsArmyMan();
	std::vector<IppiPoint> controlPoints = getPositionsArmyMan();

	// The control positions are coded for the test data, so fit to size 
	auto xpScale = float(width) / float(test_data_width);
	auto ypScale = float(height) / float(test_data_height);

	for (auto &p : controlPoints)
	{
		p.x *= int(xpScale);
		p.y *= int(ypScale);

	}

	auto points = controlPoints.size();

	CudaThinplateSpline16u thinplateSpline(deviceId);
	thinplateSpline.setMinMax(0, maxValue);

	CudaThinplateSpline16u thinplateSpline2(deviceId);
	thinplateSpline2.setMinMax(0, maxValue);

	try
	{
		CHRTimer hrt;
		CudaThinplateSpline16u *current = &thinplateSpline2;
		CudaThinplateSpline16u *next = nullptr;
//		thinplateSpline.setMaskArray(mask);
//		IppiRect roi = { width / 8, height / 16, width - width / 4, height - height / 8 };
//		thinplateSpline.setInputRoi(roi);
//		thinplateSpline.setOutputRoi(roi);

		unsigned int channelMask = 0x7;

		vector<IppiPoint_32f> floatControlPoints;
		for (auto &p : controlPoints)
		{
			floatControlPoints.push_back({ float(p.x), float(p.y) });
		}

		thinplateSpline.warpIntensity(input, output, weights, floatControlPoints, channelMask);

		auto p = output.getElementPointer({ 800, 160 });
		//for (auto i = 0; i < 20; i++)
		//{
		//	if (next == nullptr)
		//	{
		//		next = &thinplateSpline;
		//		next->warpIntensityAsync(input, output, weights, controlPoints, mask);
		//		next->warpIntensityCopyResultAsync();
		//	}

		//	swap(current, next);
		//	next->warpIntensityAsync(input, output2, weights, controlPoints, mask);
		//	next->warpIntensityCopyResultAsync();

		//	current->wait();
		//}

		//	auto p = input.getRowPointer();

		//	translator.translate(input, output, xoff, yoff);
		//	outfileName = "C:\\temp\\cuda.mbin";
		//	DevIO::dumpMatlabBinary(output, 10, outfileName);
		//	hrt.start();

		std::cout << hrt << std::endl;
	}
	catch (std::exception &ex)
	{
		std::cout << "Error: " << ex.what() << std::endl;
	}

	string tempfileName = "C:\\temp\\vsCorrected.mat";
	//DevIO::dumpMatlabBinary(output, bitDepth, tempfileName);

	// We have leak with input array, but this is debug code
	MtiCudaFreeHost(p);

	return 0;
}

