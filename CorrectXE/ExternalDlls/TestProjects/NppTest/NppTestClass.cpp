#include "stdafx.h"
#include "NppTestClass.h"
#include "npp.h"
#include "MtiCudaCoreBasicInterface.h"
#include <vector>
#include <time.h>
#include <sstream>
#include <iostream>
#include <string>
#include "HRTimer.h"
#include "Ippheaders.h"
#include <limits>

#define LOOPS 100

NppTestClass::NppTestClass(int gpuIndex) : CudaNativeBase(gpuIndex)
{
}


NppTestClass::~NppTestClass()
{
}

void NppTestClass::setArrays(const Ipp32fArray &input, const Ipp32fArray &output)
{
	_inputArray32f = input;
	_outputArray32f = output;
	setArray(CudaArrayMnemonic::Input, input.data(), input.getRowPitchInBytes(), input.getHeight(), input.getWidth(), input.getComponents());
	setArray(CudaArrayMnemonic::Output, output.data(), output.getRowPitchInBytes(), output.getHeight(), output.getWidth(), output.getComponents());
}

void NppTestClass::setArrays(const Ipp16uArray & input, const Ipp16uArray & output)
{
	_inputArray16u = input;
	_outputArray16u = output;
	setArray(CudaArrayMnemonic::Input, input.data(), input.getRowPitchInBytes(), input.getHeight(), input.getWidth(), input.getComponents());
	setArray(CudaArrayMnemonic::Output, output.data(), output.getRowPitchInBytes(), output.getHeight(), output.getWidth(), output.getComponents());

}

void NppTestClass::testConvGpu()
{
	try
	{
		NppiSize kernalSize = { 21, 21 };
		Ipp32fArray kernel({ kernalSize.width, kernalSize.height });
		kernel.set({ (1.0f / kernalSize.width) / kernalSize.height });
		set32fArray(CudaArrayMnemonic::Ancillary, kernel);

		auto &dInDesc = d_32fDesc[CudaArrayMnemonic::Input];
		auto &dOutDesc = d_32fDesc[CudaArrayMnemonic::Output];
		auto &dKernel = d_32fDesc[CudaArrayMnemonic::Ancillary];

		auto &hInDesc = h_32fDesc[CudaArrayMnemonic::Input];
		auto &hOutDesc = h_32fDesc[CudaArrayMnemonic::Output];
		auto &hKernel = h_32fDesc[CudaArrayMnemonic::Ancillary];

		copyHostToDevice2D(hKernel, dKernel);
		copyHostToDevice2D(hInDesc, dInDesc);

		// Copy doesn't have to be done, just for debugging
		copyHostToDevice2D(hOutDesc, dOutDesc);

		NppiSize oSizeROI = { dInDesc.width - kernalSize.width + 1, dInDesc.height - kernalSize.height + 1 };
		NppiPoint anchor = { kernalSize.width - 1 , kernalSize.height - 1 };
		CHRTimer hrt;
		for (auto j = 0; j < 3; j++)
		{
			for (auto i = 0; i < LOOPS; i++)
			{
				auto status = nppiAbsDiff_32f_C1R(dInDesc.data, dInDesc.bytePitch, dInDesc.data + 100, dInDesc.bytePitch,
					dOutDesc.data, dOutDesc.bytePitch, { 460, 386 });

				auto status2 = nppiFilter_32f_C1R
				(
					dInDesc.data,
					dInDesc.bytePitch,
					dOutDesc.data,
					dOutDesc.bytePitch,
					oSizeROI,
					dKernel.data,
					kernalSize,
					anchor
				);
			}

			cudaDeviceSynchronize();
			std::cout << __func__ << ": " << hrt.intervalMilliseconds() << std::endl;
		}

		auto error = cudaGetLastError();
		///copyDeviceToHost2D(dOutDesc, h_f32Desc[OUTPUT_ARRAY]);
	}
	catch (cudaError_t &error)
	{
		std::ostringstream os;
		os << MtiCudaGetErrorString(error);
		std::cout << os.str() << std::endl;
	}
}

void myConvIpp(float *in, int inPitchBytes, int rows, int cols, float *out, int outPitchBytes, const IppiSize &kernelSize, float *tmp)
{
	auto kw = kernelSize.width;
	auto kh = kernelSize.height;
	tmp = out;

	// First do column sums of kernel height for each point.
	// Produce the first row
	memcpy(tmp, in, cols * sizeof(float));
	for (auto r = 1; r < kernelSize.height; r++)
	{
		auto outr = tmp;
		auto inr = in + r * inPitchBytes / sizeof(float);
		ippsAdd_32f_I(inr, outr, cols);
	}

	// Do each additional row
	for (auto r = 1; r < rows - kernelSize.height; r++)
	{
		auto outr = tmp + r * outPitchBytes / sizeof(float);
		auto inr = in + (r - 1) * inPitchBytes / sizeof(float);
		auto inrl = inr + kernelSize.height * inPitchBytes / sizeof(float);
		ippsSub_32f_I(inr, outr, cols);
		ippsAdd_32f_I(inrl , outr, cols);
	}

	// Now partial sums of rows
	memcpy(out, tmp, cols * sizeof(float));
	for (auto r = 0; r < rows - kernelSize.height; r++)
	{
		auto outr = out + r * outPitchBytes / sizeof(float);
		auto inr = in + r * inPitchBytes / sizeof(float);

		auto s = 0.0f;
		for (auto c = 0; c < kernelSize.width; c++)
		{
			s += *(inr + c);
		}

		*outr++ = s;

		for (auto c = 1; c < cols - kernelSize.width + 1; c++)
		{
			s -= *inr;
			s += *(inr + kernelSize.width);
			inr++;
			*outr += s;
		}
	}
}

void myConvIpp2(float *in, int inPitchBytes, int rows, int cols, float *out, int outPitchBytes, const IppiSize &kernelSize, float *tmp)
{
	auto kw = kernelSize.width;
	auto kh = kernelSize.height;

	// First do column sums of kernel height for each point.
	// Produce the first row
	memcpy(tmp, in, cols * sizeof(float));
	for (auto r = 1; r < kernelSize.height; r++)
	{
		auto outr = tmp;
		auto inr = in + r * inPitchBytes / sizeof(float);
		ippsAdd_32f_I(inr, outr, cols);
	}

	// Do each additional row
	for (auto r = 1; r < rows - kernelSize.height; r++)
	{
		auto outr = tmp + r * outPitchBytes / sizeof(float);
		auto inr = in + (r - 1) * inPitchBytes / sizeof(float);
		auto inrl = inr + kernelSize.height * inPitchBytes / sizeof(float);
		ippsSub_32f_I(inr, outr, cols);
		ippsAdd_32f_I(inrl, outr, cols);
	}
}

void myConvNpp(float *in, int inPitchBytes, int rows, int cols, float *out, int outPitchBytes, const IppiSize &kernelSize, float *tmp)
{
	auto kw = kernelSize.width;
	auto kh = kernelSize.height;

	// First do column sums of kernel height for each point.
	// Produce the first row
	////memcpy(tmp, in, cols * sizeof(float));
	for (auto r = 1; r < kernelSize.height; r++)
	{
		auto outr = tmp;
		auto inr = in + r * inPitchBytes / sizeof(float);
		nppsAdd_32f_I(inr, outr, cols);
	}

	// Do each additional row
	for (auto r = 1; r < rows - kernelSize.height; r++)
	{
		auto outr = tmp + r * outPitchBytes / sizeof(float);
		auto inr = in + (r - 1) * inPitchBytes / sizeof(float);
		auto inrl = inr + kernelSize.height * inPitchBytes / sizeof(float);
		nppsSub_32f_I(inr, outr, cols);
		nppsAdd_32f_I(inrl, outr, cols);
	}

	//// Now partial sums of rows
	//memcpy(out, tmp, cols * sizeof(float));
	//for (auto r = 0; r < rows - kernelSize.height; r++)
	//{
	//	auto outr = out + r * outPitchBytes / sizeof(float);
	//	auto inr = in + r * inPitchBytes / sizeof(float);

	//	auto s = 0.0f;
	//	for (auto c = 0; c < kernelSize.width; c++)
	//	{
	//		s += *(inr + c);
	//	}

	//	*outr++ = s;

	//	for (auto c = 1; c < cols - kernelSize.width; c++)
	//	{
	//		s -= *inr;
	//		s += *(inr + kernelSize.width);
	//		inr++;
	//		*outr += s;
	//	}
	//}
}

void myConv(float *in, int inPitchBytes, int rows, int cols, float *out, int outPitchBytes, const IppiSize &kernelSize, float *tmp)
{
	auto convHeight = rows - kernelSize.height + 1;
	auto convWidth = cols - kernelSize.width + 1;

	// First do column sums of kernel height for each point.
	// Produce the first row
	memcpy(tmp, in, cols * sizeof(float));
	for (auto r = 1; r < kernelSize.height; r++)
	{
		auto tmpr = tmp;
		auto inr = in + r * inPitchBytes / sizeof(float);

		for (auto c = 0; c < cols; c++)
		{
			*tmpr++ += *inr++;
		}
	}

	// Do each additional row
	for (auto r = 1; r < convHeight; r++)
	{
		auto tmpr = tmp + r * cols;
		auto sumr = tmp + (r - 1) * cols;
		auto inr = in + (r - 1) * inPitchBytes / sizeof(float);
		auto inrl = inr + kernelSize.height * inPitchBytes / sizeof(float);

		for (auto c = 0; c < cols; c++)
		{
			*tmpr = *sumr++;            // get summed row
			*tmpr -= *inr++;		    // subtract top row
			*tmpr++ += *inrl++;         // add bottom row
		}
	}

	// Now partial sums of cols
	for (auto r = 0; r < convHeight; r++)
	{
		auto outr = out + r * outPitchBytes / sizeof(float);
		auto tmpr = tmp + r * cols;

		auto s = tmpr[0];
		for (auto c = 1; c < kernelSize.width; c++)
		{
			s += tmpr[c];
		}

		*outr++ = s;

		for (auto c = 1; c < convWidth; c++)
		{
			s -= *tmpr;
			s += *(tmpr + kernelSize.width);
			tmpr++;
			*outr++ = s;
		}
	}
}

void myConvDiff(float *in0, int in0PitchBytes, float *in1, int in1PitchBytes, int rows, int cols, float *out, int outPitchBytes, const IppiSize &kernelSize, float *tmp)
{
	auto tempHeight = rows - kernelSize.height + 1;
	auto tempWidth = cols;

	// First do column sums of kernel height for each point.
	// Produce the first row
	memset(tmp, 0, cols * sizeof(float));
	for (auto r = 0; r < kernelSize.height; r++)
	{
		auto tmpr = tmp;
		auto inr0 = in0 + r * in0PitchBytes / sizeof(float);
		auto inr1 = in1 + r * in1PitchBytes / sizeof(float);
		for (auto c = 0; c < cols; c++)
		{
			*tmpr++ += abs(*inr0++ - *inr1++);
		}
	}

	// Do each additional row
	for (auto r = 1; r < tempHeight; r++)
	{
		auto tmpr = tmp + r * tempWidth;
		auto sumr = tmp + (r - 1) * tempWidth;
		auto inr0 = in0 + (r - 1) * in0PitchBytes / sizeof(float);
		auto inr0l = inr0 + kernelSize.height * in0PitchBytes / sizeof(float);
		auto inr1 = in1 + (r - 1) * in1PitchBytes / sizeof(float);
		auto inr1l = inr1 + kernelSize.height * in1PitchBytes / sizeof(float);

		for (auto c = 0; c < cols; c++)
		{
			*tmpr = *sumr++;            // get summed row
			*tmpr -= abs(*inr0++ - *inr1++);		    // subtract top row
			*tmpr++ += abs(*inr0l++ - *inr1l++);         // add bottom row
		}
	}

	// Now partial sums of cols
	for (auto r = 0; r < tempHeight; r++)
	{
		auto outr = out + r * outPitchBytes / sizeof(float);
		auto tmpr = tmp + r * tempWidth;

		auto s = tmpr[0];
		for (auto c = 1; c < kernelSize.width; c++)
		{
			s += tmpr[c];
		}

		if (r == tempHeight - 1)
		{
			auto v = s;
		}

		*outr++ = s;

		for (auto c = 1; c < tempWidth; c++)
		{
			s -= *tmpr;
			s += *(tmpr + kernelSize.width);
			tmpr++;
			*outr++ = s;
		}
	}
}

void NppTestClass::test32fCpu()
{
	try
	{
		IppiSize kernelSize = { 21, 21 };
		vector<Ipp32f> kernel(kernelSize.height *kernelSize.width, 1.0);

		auto &hInDesc = h_32fDesc[CudaArrayMnemonic::Input];
		auto &hOutDesc = h_32fDesc[CudaArrayMnemonic::Output];
		auto &hAncDesc = h_32fDesc[CudaArrayMnemonic::Ancillary];
		auto &hDiffDesc = h_32fDesc[CudaArrayMnemonic::Scratch];

		IppiSize sizeImage = { hInDesc.width, hInDesc.height };
		IppiSize sizeConv = { hInDesc.width - kernelSize.width + 1, hInDesc.height - kernelSize.height + 1 };

		Ipp32fArray Image2(sizeImage);
		Image2.uniformDistribution(0, 1);
		set32fArray(CudaArrayMnemonic::Ancillary, Image2);
		Ipp32fArray diffImage(sizeImage);
		set32fArray(CudaArrayMnemonic::Scratch, diffImage);

		int iTmpBufSize = -1;

		IppEnum funCfgValid = (IppEnum)(ippAlgAuto | ippiROIValid | ippiNormNone);
		auto istatus = ippiConvGetBufferSize(sizeImage, kernelSize, ipp32f, 1, funCfgValid, &iTmpBufSize);
		auto pBuffer = ippsMalloc_8u(iTmpBufSize);

		CHRTimer hrt;
		Ipp32fArray conv(sizeConv);
		Ipp32fArray convC(sizeConv);
		Ipp32fArray minDiffImageFull(sizeImage);
		minDiffImageFull.set({ FLT_MAX });
		auto minDiffImage = minDiffImageFull({ kernelSize.width / 2, kernelSize.height / 2, sizeConv.width, sizeConv.height });
		auto tmp = ippsMalloc_32f(hInDesc.width * hInDesc.height);

#define BIG_LOOPS 8
		auto averageTime = 0.0;
		for (auto j = 0; j < BIG_LOOPS; j++)
		{
			for (auto i = 0; i < LOOPS; i++)
			{
				auto status = ippiAbsDiff_32f_C1R(hInDesc.data, hInDesc.bytePitch, hAncDesc.data, hAncDesc.bytePitch,
					hDiffDesc.data, hDiffDesc.bytePitch, sizeImage);

//				status = ippiConv_32f_C1R(hDiffDesc.data, hDiffDesc.bytePitch, sizeImage, kernel.data(), 4 * kernelSize.width,
//					kernelSize, conv.data(), conv.getRowPitchInBytes(), funCfgValid, pBuffer);

				myConv(hDiffDesc.data, hDiffDesc.bytePitch, hDiffDesc.height, hInDesc.width,
					convC.data(), convC.getRowPitchInBytes(), kernelSize, tmp);

				//for (auto r = 0; r < conv.getHeight(); r++)
				//{
				//	auto crp = conv.getPointerToElement(r);
				//	auto mrp = hOutDesc.data + r * hOutDesc.bytePitch / sizeof(float);
				//	for (auto c = 0; c < conv.getWidth(); c++)
				//	{
				//		auto x1 = abs(*crp++ - *mrp++);
				//		if (x1 > 0.001f)
				//		{
				//			auto x1 = *(crp - 1) - *(mrp - 1);
				//			x1 = abs(x1);
				//		}
				//	}
				//}
			}

			auto t = hrt.intervalMilliseconds();
			averageTime += t;
			std::cout << __func__ << ": " << t << std::endl;
		}
		std::cout << __FUNCSIG__ << "Average: " << averageTime/BIG_LOOPS << std::endl;
		auto error = cudaGetLastError();
		///copyDeviceToHost2D(dOutDesc, h_f32Desc[OUTPUT_ARRAY]);
	}
	catch (cudaError_t &error)
	{
		std::ostringstream os;
		os << MtiCudaGetErrorString(error);
		std::cout << os.str() << std::endl;
	}
}

void NppTestClass::test16uGpu()
{
	try
	{
		NppiSize kernalSize = { 21, 21 };
		Ipp16uArray kernel({ kernalSize.width, kernalSize.height });
		kernel.set({ 1 });
		setArray(CudaArrayMnemonic::Ancillary, kernel.data(), kernel.getRowPitchInBytes(), kernel.getHeight(), kernel.getWidth(), kernel.getComponents());

		auto &dInDesc = d_16uDesc[CudaArrayMnemonic::Input];
		auto &dOutDesc = d_16uDesc[CudaArrayMnemonic::Output];
//		auto &dKernel = d_ancImage;

		auto &hInDesc = h_16uDesc[CudaArrayMnemonic::Input];
		auto &hOutDesc = h_16uDesc[CudaArrayMnemonic::Output];
//		auto &hKernel = h_ancImage;

//		copyHostToDevice2D(hKernel, dKernel);
		copyHostToDevice2D(hInDesc, dInDesc);

		// Copy doesn't have to be done, just for debugging
		copyHostToDevice2D(hOutDesc, dOutDesc);

		NppiSize oSizeROI = { dInDesc.width - kernalSize.width + 1, dInDesc.height - kernalSize.height + 1 };
		NppiPoint anchor = { kernalSize.width - 1 , kernalSize.height - 1 };

		vector<Npp32s> hkernel(21 * 21, 1);
		Npp32s *dKernel;
		cudaMalloc((void**)&dKernel, 21*21*sizeof(Npp32s));

		ThrowOnCudaError(cudaMemcpy(dKernel, hkernel.data(), 21 * 21 * sizeof(Npp32s), cudaMemcpyHostToDevice));

		CHRTimer hrt;
		for (auto j = 0; j < 3; j++)
		{
			for (auto i = 0; i < LOOPS; i++)
			{
				auto status = nppiAbsDiff_16u_C1R(dInDesc.data, dInDesc.bytePitch, dInDesc.data + 100, dInDesc.bytePitch,
					dOutDesc.data, dOutDesc.bytePitch, { 460, 386 });

				auto status2 = nppiFilter_16u_C1R
				(
					dInDesc.data,
					dInDesc.bytePitch,
					dOutDesc.data,
					dOutDesc.bytePitch,
					oSizeROI,
					dKernel,
					kernalSize,
					anchor,
					21*21
				);
			}

			cudaDeviceSynchronize();
			std::cout << __func__ << ": " << hrt.intervalMilliseconds() << std::endl;
		}

		auto error = cudaGetLastError();
		///copyDeviceToHost2D(dOutDesc, h_f32Desc[OUTPUT_ARRAY]);
	}
	catch (cudaError_t &error)
	{
		std::ostringstream os;
		os << MtiCudaGetErrorString(error);
		std::cout << os.str() << std::endl;
	}
}

void NppTestClass::TestConvCpu()
{
	try
	{
		IppiSize kernelSize = { 21, 21 };
		vector<Ipp32f> kernel(kernelSize.height *kernelSize.width, (1.0f/kernelSize.height)/kernelSize.width);

		auto &hInDesc = h_32fDesc[CudaArrayMnemonic::Input];
		auto &hOutDesc = h_32fDesc[CudaArrayMnemonic::Output];

		int iTmpBufSize;
		IppiSize imageSize = { hInDesc.width, hInDesc.height };

		IppEnum funCfgValid = (IppEnum)(ippAlgAuto | ippiROIValid | ippiNormNone);
		auto istatus = ippiConvGetBufferSize(imageSize, { 21, 21 }, ipp32f, 1, funCfgValid, &iTmpBufSize);
		auto pBuffer = ippsMalloc_8u(iTmpBufSize);

		CHRTimer hrt;
		for (auto j = 0; j < 3; j++)
		{
			for (auto i = 0; i < LOOPS; i++)
			{
				auto status = ippiAbsDiff_32f_C1R(hInDesc.data, hInDesc.bytePitch, hInDesc.data + 100, hInDesc.bytePitch,
					hOutDesc.data, hOutDesc.bytePitch, { 460, 386 });

				istatus = ippiConv_32f_C1R(hInDesc.data, hInDesc.bytePitch, imageSize, kernel.data(), 21 * 4, { 20, 20 }, hOutDesc.data, hOutDesc.bytePitch, funCfgValid, pBuffer);
			}

			std::cout << __func__ << ": " << hrt.intervalMilliseconds() << std::endl;
		}

	}
	catch (cudaError_t &error)
	{
		std::ostringstream os;
		os << MtiCudaGetErrorString(error);
		std::cout << os.str() << std::endl;
	}
}

void NppTestClass::nppDiffMeanTest()
{
	auto &dInDesc = d_32fDesc[CudaArrayMnemonic::Input];
	auto &dOutDesc = d_32fDesc[CudaArrayMnemonic::Output];
	auto &hInDesc = h_32fDesc[CudaArrayMnemonic::Input];
	auto &hOutDesc = h_32fDesc[CudaArrayMnemonic::Output];

	copyHostToDevice2D(hInDesc, dInDesc);

	int bufferSize;
	nppiMeanGetBufferHostSize_32f_C1R({ 21, 21 }, &bufferSize);
	Npp8u *pBuffer = nullptr;
	auto status = cudaMalloc((void **)(&pBuffer), bufferSize);

	Npp64f *pMean = nullptr;
	cudaMalloc((void **)(&pMean), sizeof(Npp64f));
	CHRTimer hrt;

	for (auto k = 0; k < 200; k++)
	{
		auto fpDst = dInDesc.data + k;

		for (int i = 0; i < 31; i++)
		{
			IppStatus status;
			auto fpSrc = dInDesc.data + k * dInDesc.bytePitch / 4;

			status = nppiAbsDiff_32f_C1R(fpSrc, 4 * dInDesc.width, fpDst, 4 * dInDesc.width, dOutDesc.data, 4 * dOutDesc.width, { 21, 21 });
			if (status)
			{
				throw status;
			}

			// find the mean of the patch
			status = nppiMean_32f_C1R(dOutDesc.data, 4 * dOutDesc.width, { 21, 21 }, pBuffer, pMean);
			if (status)
			{
				throw status;
			}

			double dMean;
			status = cudaMemcpy(&dMean, pMean, sizeof(Npp64f) * 1, cudaMemcpyDeviceToHost);
			//if ((float)dMean < fScoreBest)
			//{
			//	fScoreBest = (float)dMean;
			//	iRowOffBest = iRowOff;
			//	iColOffBest = iColOff;
			//}

			fpDst++;
		}
	}

	std::cout << __func__ << ": " << hrt.intervalMilliseconds() << std::endl;
}

void NppTestClass::smallTest()
{
	try
	{
		Ipp32fArray inArray({ 9, 9 });
		inArray.set({ 0 });
		inArray({ 3,3,3,3 }).iota();
		inArray({ 3,3,3,3 }) += 1;
		Ipp32fArray outArray({ 7, 7 });
		outArray.set({ 10 });
		NppiSize oSize = { 7, 7 };
		NppiSize oKernalSize = { 3, 3 };
		//	NppiSize oSizeROI = { inArray.getWidth() - oKernalSize.width + 1, inArray.getHeight() - oKernalSize.height + 1 };

		setArrays(inArray, outArray);
		Npp32f kernel[] = { 0.1f, 0.2f, 0.3f,
			0.4f, 0.5f, 0.6f,
			0.7f, 0.8f, 0.9f };
		IppiPoint anchor = { 1, 1 };

		auto &dInDesc = d_32fDesc[CudaArrayMnemonic::Input];
		auto &dOutDesc = d_32fDesc[CudaArrayMnemonic::Output];
		auto &hInDesc = h_32fDesc[CudaArrayMnemonic::Input];
		auto &hOutDesc = h_32fDesc[CudaArrayMnemonic::Output];

		//int iTmpBufSize;
		//IppEnum funCfgValid = (IppEnum)(ippAlgAuto | ippiROIValid | ippiNormNone);
		//auto istatus = ippiConvGetBufferSize({ 9,9 }, { 3, 3 }, ipp32f, 1, funCfgValid, &iTmpBufSize);
		//auto pBuffer = ippsMalloc_8u(iTmpBufSize);
		//istatus = ippiConv_32f_C1R(hInDesc.data, hInDesc.bytePitch, { 9,9 }, kernel, 12, { 3,3 }, hOutDesc.data, hOutDesc.bytePitch, funCfgValid, pBuffer);
		//std::cout << dumpArray(outArray.data(), 7, 7) << std::endl;

		copyHostToDevice2D(hInDesc, dInDesc);
		copyHostToDevice2D(hOutDesc, dOutDesc);

		Npp32f *d_kernel;
		ThrowOnCudaError(cudaMalloc((void **)&d_kernel, 3 * 3 * sizeof(float)));
		ThrowOnCudaError(cudaMemcpy(d_kernel, kernel, 3 * 3 * sizeof(float), cudaMemcpyHostToDevice));

		auto status = nppiFilter_32f_C1R
		(
			dInDesc.data,
			dInDesc.bytePitch,
			dOutDesc.data,
			dOutDesc.bytePitch,
			oSize,
			d_kernel,
			{ 3,3 },
			{ 2, 2 }
		);

		cudaDeviceSynchronize();
		auto error = cudaGetLastError();
		copyDeviceToHost2D(dOutDesc, hOutDesc);
		std::cout << dumpArray(inArray.data(), 9, 9) << std::endl << std::endl;
		std::cout << dumpArray(outArray.data(), 7, 7) << std::endl;
	}
	catch (cudaError_t &error)
	{
		std::ostringstream os;
		os << MtiCudaGetErrorString(error);
		std::cout << os.str() << std::endl;
	}

	//NppiSize roiSize = { _inputArray.getSize().width, _inputArray.getSize().height };
	//int hpBufferSize = 0;
	//auto status = nppiMeanStdDevGetBufferHostSize_16u_C3CMR(roiSize, &hpBufferSize);

	//Npp8u *pDeviceBuffer;
	//ThrowOnCudaError(cudaMalloc((void **)&pDeviceBuffer, hpBufferSize * sizeof(Npp8u)));

	//Npp64f *dbuf = nullptr; // Note: Npp64f is double (see NPP_library.pdf)
	//ThrowOnCudaError(cudaMalloc((void **)&dbuf, 16 * sizeof(Npp64f)));
	//Npp64f hbuf[32];

	//Npp64f mean=0;
	//Npp64f stdDev=0;

	//status = nppiMean_StdDev_16u_C3CR((Npp16u *)d_inImage.data, // src2
	//	static_cast<int>(d_inImage.bytePitch), // pitch
	//	roiSize,
	//	3,
	//	pDeviceBuffer,
	//	dbuf,
	//	dbuf+1);

	//auto s = cudaMemcpy(hbuf, dbuf, 2*sizeof(Npp64f), cudaMemcpyDeviceToHost);
}