#pragma once
#include "MtiCudaCore.h"
#include "CudaNativeBase.h"
#include "Ippheaders.h"

class NppTestClass : public CudaNativeBase
{

public:
	NppTestClass(int gpuIndex);
	virtual ~NppTestClass();
	void setArrays(const Ipp32fArray &input, const Ipp32fArray &output);
	void setArrays(const Ipp16uArray &input, const Ipp16uArray &output);
	void testConvGpu();

	void test16u();

	void test32fCpu();

	void test16uGpu();

	void TestConvCpu();

	void nppDiffMeanTest();

	void smallTest();
	void set32fArray(CudaArrayMnemonic n, const Ipp32fArray &hostArray)
	{
		setArray(n, hostArray.data(), hostArray.getRowPitchInBytes(), hostArray.getHeight(), hostArray.getWidth(), hostArray.getComponents());
	}

	Ipp32fArray _inputArray32f;
	Ipp32fArray _outputArray32f;

	Ipp16uArray _inputArray16u;
	Ipp16uArray _outputArray16u;
};

