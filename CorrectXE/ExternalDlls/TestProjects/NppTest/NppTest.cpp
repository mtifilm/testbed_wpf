// NppTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "npp.h"
#include "CudaNativeBase.h"
#include <filesystem>
#include "DevSupportLib.h"
#include "Ippheaders.h"
#include "HRTimer.h"
#include <optional>
#include "Dpx\dpx.h"
#include "GpuAccessor.h"
#include "NppTestClass.h"
#include "ImagePatch.h"
#include "IpaStripeStream.h"
#include "ZonalFlickerOneChannelAnalysis.h"
#include "../../DpxToMatlab/DpxSequenceReader.h"

using namespace std;

#define PROXY_WIDTH 512

//int main(int argc, const char * argv[])
//{
//	//GpuAccessor gpuAccessor;
//
//	//if (gpuAccessor.doesGpuExist() == false)
//	//{
//	//	std::cout << "No GPU found " << std::endl;
//	//}
//
//	////	std::string file = "D:\\CorrectData\\moon_over_miami\\112657.dpx";
//
//	////string outfileName = "C:\\temp\\moon_over_miami.112657.mbin";
//	////DevIO::dumpMatlabBinary(image, 16, outfileName);
//
//	//std::cout << "Number of gpus: " << gpuAccessor.getDeviceCount() << std::endl;
//	//std::cout << "Default gpu: " << gpuAccessor.getDefaultGpu() << ", name: " << gpuAccessor.getDeviceName() << std::endl;
//	//std::cout << "Compute Capability: " << gpuAccessor.getMajorComputeCapability() << "." << gpuAccessor.getMinorComputeCapability() << std::endl;
//
//	//std::cout << gpuAccessor.getInfoString(gpuAccessor.getDefaultGpu());
//
//	//std::string fileName0 = "D:\\CorrectData\\dewarp\\man_r3.018121.dpx";
//	//std::string fileName1 = "D:\\CorrectData\\dewarp\\man_r3.018122.dpx";
//	std::string fileName0 = R"(D:\CorrectData\Flicker\do_testow_mti_pancerni\tvp_pancerni_mti_test_footage_000551.dpx)";
//	std::string fileName1 = R"(D:\CorrectData\Flicker\do_testow_mti_pancerni\tvp_pancerni_mti_test_footage_000552.dpx)";
//	if (argc > 1)
//	{
//		fileName0 = string(argv[1]);
//	}
//
//	auto[input16u, bitDepth] = DevIO::readDpxFile(fileName0, true);
//
//	Ipp32fArray yArray0;
//	//auto planes = input16u.copyToPlanar();
//	//auto frameRoi0 = planes[0]({ 561, 78, 3437, 3013 });
//
//	yArray0 <<= input16u;
//
//	auto [input16u1, bd] = DevIO::readDpxFile(fileName1, true);
//	//auto planes1 = input16u1.copyToPlanar();
//	//auto frameRoi1 = planes1[0]({ 561, 78, 3437, 3013 });
//
//	Ipp32fArray yArray1;
//	yArray1 <<= input16u1;
//
//	//SaveVariablesToMatFile2("c:\\temp\\TestImages.mat", yArray0, input16u);
//
//	auto proxyHeight = (int)((float)(input16u.getHeight() * PROXY_WIDTH) /
//		(float)input16u.getWidth() + .5f);
//
//	ZonalFlickerAnalysis zonalFlickerAnalysis;
//	zonalFlickerAnalysis.setZonalProcessType(ZonalProcessType::Intensity);
//	zonalFlickerAnalysis.setHorizontalBlocks(22);
//	zonalFlickerAnalysis.setVerticalBlocks(15);
//	zonalFlickerAnalysis.setPdfSigma(20);
//	zonalFlickerAnalysis.setSmoothingSigma(10);
//	zonalFlickerAnalysis.setDownsample(1);
//
//	// Wrong, fix
//	///zonalFlickerAnalysis.setRoi(yArray1.getRoi());
//	zonalFlickerAnalysis.setImageSize(yArray0.getSize());
//	zonalFlickerAnalysis.initialize(10);
//
//	CHRTimer hrt;
//	zonalFlickerAnalysis.preprocess(yArray0, yArray1);
//	std::cout << hrt << std::endl;
//
//	//yArray = yArray.resize(nominalSize);
//	//Ipp32fArray diffArray(yArray.getSize());
//
//	//Ipp16uArray y16uArray;
//	//y16uArray <<= (yArray * 65535);
//	//
//	//Ipp16uArray d16uArray(y16uArray.getSize());
//
//	try
//	{
// 	//	ImagePatch imagePatch;
//		//imagePatch.setImage(yArray);
//		//imagePatch.calcMinimumImage();
//
//		//for (auto &metric : imagePatch.metrics)
//		//{
//		//	std::cout << metric.first << ": " << metric.second << endl;
//		//}
//
////		NppTestClass nppTest(gpuAccessor.getDefaultGpu());//
////		nppTest.setArrays(yArray, diffArray);
////		nppTest.nppDiffMeanTest();
////		nppTest.testConvGpu();
////		nppTest.TestConvCpu();
////		nppTest.setArrays(y16uArray, d16uArray);
////		nppTest.test32fCpu();
//
//	}
//	catch (std::exception &ex)
//	{
//		std::cout << "Error: " << ex.what() << std::endl;
//	}
//
//	system("pause");
//	return 0;
//}

int computeDeltaValues(const std::string &filePath, ZonalFlickerOneChannelAnalysis &zonalFlickerAnalysis)
{
	int readStart = 551;

	DpxSequenceReader dpxSequenceReader(filePath, false);
	dpxSequenceReader.setShotTimecodesStartEnd(readStart, readStart + 36);
	CHRTimer hrt;
	auto t = 0.0;
	auto timeIter = 0;
	int duration = dpxSequenceReader.getShotDuration();

	auto currentPlanes = dpxSequenceReader.getFirstImage().copyToPlanar();
	zonalFlickerAnalysis.setNumIntensityLevels(1 << dpxSequenceReader.getBitDepth());
	auto fullImageSize = currentPlanes.getPlaneSize();
	MtiSize downSampleSize(fullImageSize.width / zonalFlickerAnalysis.getDownsample(), fullImageSize.height / zonalFlickerAnalysis.getDownsample());
	auto currentFrame = Ipp32fArray(currentPlanes[0]).resizeAntiAliasing(downSampleSize);

	// Wrong, fix
	zonalFlickerAnalysis.setImageSize(downSampleSize);
	zonalFlickerAnalysis.initialize(duration);

	auto totalTime = 0.0;
	auto totalResize = 0.0;
	auto totalProcess = 0.0;
	// Initialize the first call, this initializes the start
	zonalFlickerAnalysis.preprocess(currentFrame, currentFrame);

	while (auto frame = dpxSequenceReader.getNextImage())
	{
		// Work on the first channel
		auto nextPlanes = (*frame).copyToPlanar();
		hrt.start();
		auto nextFrame = Ipp32fArray(nextPlanes[0]).resizeAntiAliasing(downSampleSize);
		totalResize += hrt.intervalMilliseconds();
		zonalFlickerAnalysis.preprocess(currentFrame, nextFrame);
		currentFrame = nextFrame;
		totalProcess += hrt.elapsedMilliseconds();
		totalTime = totalResize + totalProcess;
		DBTRACE2(totalResize, totalProcess);
		DBTRACE2(totalTime, zonalFlickerAnalysis.getDeltaValues().size());
	}
	try
	{
		auto averageTime = totalTime / zonalFlickerAnalysis.getDeltaValues().size();
		auto averageProcess = totalProcess / zonalFlickerAnalysis.getDeltaValues().size();
		auto averageResize = totalResize / zonalFlickerAnalysis.getDeltaValues().size();
		DBTRACE2(averageResize, averageProcess);
		DBTRACE(averageTime);
		MatIO::saveListToMatFile(R"(c:\temp\deltaValues.mat)", zonalFlickerAnalysis.getDeltaValues(), "deltaValuesC");
	}
	catch (std::exception &ex)
	{
		std::cout << "Error: " << ex.what() << std::endl;
	}

	return 0;
}

int main(int argc, const char * argv[])
{
	std::string filePath = R"(D:\CorrectData\Flicker\do_testow_mti_pancerni\)";
	//std::string filePath = R"(D:\Temp\ShineTests\AlphaFilterStressTest\)";
	if (argc > 1)
	{
		filePath = string(argv[1]);
	}

	ZonalFlickerOneChannelAnalysis _zonalFlickerAnalysis;
	_zonalFlickerAnalysis.setZonalProcessType(ZonalProcessType::Intensity);
	_zonalFlickerAnalysis.setHorizontalBlocks(22);
	_zonalFlickerAnalysis.setVerticalBlocks(18);
	_zonalFlickerAnalysis.setFilterRadius(50);

	_zonalFlickerAnalysis.setPdfSigma(20);
	_zonalFlickerAnalysis.setSmoothingSigma(6);
	_zonalFlickerAnalysis.setDownsample(4);

	computeDeltaValues(filePath, _zonalFlickerAnalysis);
	//auto deltaValues = MatIO::readIpp32fPlanar(R"(c:\temp\deltaValues.mat)");
	//auto deltaValues = _zonalFlickerAnalysis.getDeltaValues();
	//auto smoothedDeltaValues = _zonalFlickerAnalysis.smoothing(deltaValues);

	// Debug 
	MtiPlanar<Ipp32fArray> junk({ 22, 18, 177 });
	for (auto &j : junk)
	{
		j.uniformDistribution(-10, 10);
	}
	vector<int> anchors = { 11, 20, 35 }; //{0, 19, 49, 89, 169, 176}; 
	auto smoothedDeltaValues = _zonalFlickerAnalysis.smoothWithAnchors(junk, anchors);
	SaveVariablesToMatFile2("c:\\temp\\smoothedDeltaValues.mat", smoothedDeltaValues, junk);
}
