#include "FastL1Min.h"
#include <algorithm>

void MinConvDiff(float *in0, int in0PitchBytes, float *in1, int in1PitchBytes, int rows, int cols, float *out, int outPitchBytes, const IppiSize &kernelSize, float *tmp)
{
	auto tempHeight = rows - kernelSize.height + 1;
	auto tempWidth = cols;

	// First do column sums of kernel height for each point.
	// Produce the first row
	memset(tmp, 0, cols * sizeof(float));
	for (auto r = 0; r < kernelSize.height; r++)
	{
		auto tmpr = tmp;
		auto inr0 = in0 + r * in0PitchBytes / sizeof(float);
		auto inr1 = in1 + r * in1PitchBytes / sizeof(float);
		for (auto c = 0; c < cols; c++)
		{
			*tmpr++ += *inr0++ - *inr1++;
		}
	}

	// Do each additional row
	for (auto r = 1; r < tempHeight; r++)
	{
		auto tmpr = tmp + r * tempWidth;
		auto sumr = tmp + (r - 1) * tempWidth;
		auto inr0 = in0 + (r - 1) * in0PitchBytes / sizeof(float);
		auto inr0l = inr0 + kernelSize.height * in0PitchBytes / sizeof(float);
		auto inr1 = in1 + (r - 1) * in1PitchBytes / sizeof(float);
		auto inr1l = inr1 + kernelSize.height * in1PitchBytes / sizeof(float);

		for (auto c = 0; c < cols; c++)
		{
			*tmpr = *sumr++;            // get summed row
			*tmpr -= *inr0++ - *inr1++;		    // subtract top row
			*tmpr++ += *inr0l++ - *inr1l++;         // add bottom row
		}
	}

	// Now partial sums of cols
	for (auto r = 0; r < tempHeight; r++)
	{
		// sum up first column
		auto outr = out + r * outPitchBytes / sizeof(float);
		auto tmpr = tmp + r * tempWidth;

		auto s = tmpr[0];
		for (auto c = 1; c < kernelSize.width; c++)
		{
			s += tmpr[c];
		}

		if (r == tempHeight - 1)
		{
			auto v = s;
		}

		*outr++ = std::min<float>(*outr, s);

		// Now do remainder of columns
		for (auto c = 1; c < tempWidth; c++)
		{
			s -= *tmpr;
			s += *(tmpr + kernelSize.width);
			tmpr++;
			*outr++ = std::min<float>(*outr, s);
		}
	}
}


FastL1Min::FastL1Min()
{
}


FastL1Min::~FastL1Min()
{
}
