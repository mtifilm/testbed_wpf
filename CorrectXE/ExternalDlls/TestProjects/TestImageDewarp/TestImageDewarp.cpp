// TestCudaMtiCoreC.cpp : Defines the entry point for the console application.
//

#include "MtiCudaCoreInterface.h"
#include "DevSupportLib.h"
#include "Ippheaders.h"
#include "HRTimer.h"
#include "Dpx\dpx.h"

// Data for test
int test_data_width = 2000;
int test_data_height = 1440;

//std::vector<IppiPoint> getPositions2()
//{
//	std::vector<IppiPoint> result(176);
//}

std::vector<IppiPoint_32f> getPositions()
{
	vector<IppiPoint_32f> result(7);
	result[0] = { 675.444f, 898.167f };
	result[1] = { 1414.39f, 692.649f };
	result[2] = { 695.988f, 2181.1f };
	result[3] = { 1460.46f, 2606.1f };
	result[4] = { 2800.41f, 2012.32f };
	result[5] = { 3058.74f, 556.057f };
	result[6] = { 2448.0f, 942.566f };
	result[6] = { 2448.0f, 942.566f };

	return result;
}


std::vector<std::vector<Ipp32f>> getWeights()
{
	vector<Ipp32f> result(10);
	result[0] = -0.00355804f;
	result[1] = 0.00341952f;
 	result[2] = -0.000201444f;
 	result[3] = -0.0011658f;
  	result[4] = 0.000138472f;
 	result[5] = -0.00708221f;
 	result[6] = 0.00844951f;
  	result[7] = 7.37548f;
  	result[8] = 0.999504f;
	result[9] = 0.000171242f;

	vector<Ipp32f> result2(10);
	result2[0] = -0.00201727f;
	result2[1] = -0.00403575f;
	result2[2] = -0.00355469f;
	result2[3] = -0.00362929f;
	result2[4] = -0.00136951f;
	result2[5] = -0.0279603f;
	result2[6] = 0.0425668f;
	result2[7] = 7.72637f;
	result2[8] = 0.00698071f;
	result2[9] = 0.99457f;

	return { result, result2 };
}

std::vector<IppiPoint_32f> getPositionsRotation()
{
	vector<IppiPoint_32f> result = { { 1000.0f, 720.0f } };
	result.clear();
	return result;
}

std::vector<std::vector<Ipp32f>> getWeightsRotation()
{
	float pi = 4*atan(1.0f);
	float theta = (pi * 5.0f) / 180.0f;

	vector<float> xrot = { -112, cos(theta), sin(theta) };
	vector<float> yrot = { -40, -sin(theta), cos(theta) };

	return { xrot, yrot };
}

using namespace std;

int main(int argc, const char * argv[])
{
	int count;
	GpuAccessor gpuAccessor;

	if (gpuAccessor.doesGpuExist() == false)
	{
		std::cout << "No GPU found " << std::endl;
	}

	std::cout << "Number of gpus: " << gpuAccessor.getDeviceCount() << std::endl;
	std::cout << "Default gpu: " << gpuAccessor.getDefaultGpu() << ", name: " << gpuAccessor.getDeviceName() << std::endl;
	std::cout << "Compute Capability: " << gpuAccessor.getMajorComputeCapability() << "." << gpuAccessor.getMinorComputeCapability() << std::endl;

	std::string fileName = "D:\\CorrectData\\dewarp\\man_r3.017714.dpx";
	///fileName = "Y:\\tmp\\matlab\\C3P_R12_1041007.dpx";
	if (argc > 1)
	{
		fileName = string(argv[1]);
	}

	auto[input, bitDepth] = DevIO::readDpxFile(fileName, false);
	Ipp16u maxValue = (1 << bitDepth) - 1;

	auto width = input.getWidth();
	auto height = input.getHeight();
	auto sizeInBytes = input.area()*input.getComponents() * sizeof(Ipp16u);

	cout << "Testing " << fileName << std::endl;
	cout << "(" << input.getWidth() << "," << input.getHeight() << ")" << std::endl;

	auto p = (Ipp16u *)DevIO::getPinnedMemory(sizeInBytes);
	Ipp16uArray output({ input.getSize(), 3 }, p);
	output.set({ 0, 0 ,maxValue });

	auto p2 = (Ipp16u *)DevIO::getPinnedMemory(sizeInBytes);
	Ipp16uArray output2({input.getSize(), 3}, p2);

	int deviceId = 0;

	auto mp = (Ipp8u *)DevIO::getPinnedMemory(input.area()*input.getComponents());
	Ipp8uArray mask({ width, height }, mp);
	//mask.zero();
	//int N = 320;
	//mask({ N, N, width - 2 * N, height - 2 * Nf }).set({ 255f });

	// The control positions are coded for the test data, so fit to size 
	auto xpScale = float(width) / float(test_data_width);
	auto ypScale = float(height) / float(test_data_height);

	CudaThinplateSpline16u thinplateSpline(deviceId);
	thinplateSpline.setMinMax(0, maxValue);
	thinplateSpline.setInputRoi(input.getRoi());
	thinplateSpline.setOutputRoi(output.getRoi());
	// This primes the output buffer, the warpLocations just writes the warped values
	// When all is done, the last frame is in the output so we don't need to do this again
	thinplateSpline.setAndLoadAncillaryArrayAsync(output);
	thinplateSpline.wait();

	try
	{
		CHRTimer hrt;
		//CudaThinplateSpline16u *current = &thinplateSpline2;
		//CudaThinplateSpline16u *next = nullptr;
		//thinplateSpline.setMaskArray(mask);
		//IppiRect roi = { width / 8, height / 16, width - width / 4, height - height / 8f };
		//thinplateSpline.setInputRoi(roi);
		//thinplateSpline.setOutputRoi(roi);

		thinplateSpline.warpLocation(input, output, getWeightsRotation(), getPositionsRotation(), -1, 1);

		//for (auto i = 0; i < 20; i++)
		//{
		//	if (next == nullptr)
		//	{
		//		next = &thinplateSpline;
		//		next->warpIntensityAsync(input, output, weights, controlPoints, mask);
		//		next->warpIntensityCopyResultAsync();
		//	}

		//	swap(current, next);
		//	next->warpIntensityAsync(input, output2, weights, controlPoints, mask);
		//	next->warpIntensityCopyResultAsync();

		//	current->wait();
		//}

		//	auto p = input.getRowPointer();

		//	translator.translate(input, output, xoff, yoff);
		//	outfileName = "C:\\temp\\cuda.mbin";
		//	DevIO::dumpMatlabBinary(output, 10, outfileName);
		//	hrt.start();------

		std::cout << hrt << std::endl;

		string outfileName = "C:\\temp\\vSOriginal.mbin";
		//DevIO::dumpMatlabBinary(input, bitDepth, outfileName);
	}
	catch (std::exception &ex)
	{
		std::cout << "Error: " << ex.what() << std::endl;
	}

	string outfileName = "C:\\temp\\vsCorrected.mbin";
	//DevIO::dumpMatlabBinary(output, bitDepth, outfileName);

	// We have leak with input array, but this is debug code
	MtiCudaFreeHost(p);

	system("pause");
	return 0;
}
