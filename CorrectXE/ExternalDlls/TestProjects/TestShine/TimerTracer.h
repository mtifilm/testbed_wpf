#pragma once
#include "HRTimer.h"
#include <vector>

#define MTI_START_TIMER(timerTracer)
#define MTI_STAMP_ELAPSED(timerTracer)
#define MTI_STAMP_INTERVAL(timerTracer)

class TimerTracer : public CHRTimer
{
	struct TimerData
	{
		std::string functionName;
		int lineNumber = -1;
		std::string infoString;
		double elapsedMilliseconds = -1;
		int tag = 0;
	};

public:
	TimerTracer() = default;
	~TimerTracer() = default;

	void stamp(const std::string &name, int line, const std::string &info, double milliseconds, int newTag = 0) 
	{
		TimerData timerData;
		timerData.functionName = name;
		timerData.lineNumber = line;
		timerData.infoString = info;
		timerData.elapsedMilliseconds = milliseconds;
		timerData.tag = newTag;

		times.push_back(timerData);
	}


private:
	std::vector<TimerData> times;
};

