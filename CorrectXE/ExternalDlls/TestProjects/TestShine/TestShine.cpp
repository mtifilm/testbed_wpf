// TestShine.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "Ippheaders.h"
#include <filesystem>
#include <string>
#include <cctype>
#include <iostream>
#include "DevIO.h"
#include "AutoTrackingHeaders.h"
#include "HRTimer.h"
#include <locale>
#include "AutoTrackingPreprocessor.h"
#include "ShineRepairMask.h"
#include "../../DpxToMatlab/DpxSequenceReader.h"
#include "../../IppArrayBCB/polyRegularize.h"
#include "MtiCore.h"

//TrackFrameInfo makeTrackInfo(const string &fileName, AutoTrackParameters &params)
//{
//	auto[ccRgbFull16u, bitDepth] = DevIO::readDpxFile(fileName, true);
//	auto ccRgbFull32f = Ipp32fArray(ccRgbFull16u);
//	ccRgbFull32f /= 65535.0f;
//
//	TrackFrameInfo trackFrameInfo;;
//	auto ccGrayFull32f = trackFrameInfo.fullGrayImage;
//
//	auto proxyBeforeScale = trackFrameInfo.fullGrayImage.resize(params.getProxySize());
//	std::tie(trackFrameInfo.proxyImage, trackFrameInfo.averageIntensity) = AutoTrackSupport::balanceIntensity(proxyBeforeScale, params);
//
//	return trackFrameInfo;
//}

bool caseInSensitiveStringCompare(std::string &str1, std::string &&str2)
{
	return ((str1.size() == str2.size()) && std::equal(str1.begin(), str1.end(), str2.begin(), [](char & c1, char & c2) {
		return (c1 == c2 || std::toupper(c1) == std::toupper(c2));
	}));
}
struct ClipData
{
	std::string dpxFilePath;
	std::string outputFileName;
	int repairTimecode = 0;
	int startTimeCode = 0;
	int endTimeCode = INT_MAX;
};

int doTracking(ClipData &clipData)
{
	DpxSequenceReader dpxSequenceReader(clipData.dpxFilePath, false);
	dpxSequenceReader.setShotTimecodesStartEnd(clipData.startTimeCode, clipData.endTimeCode);
	int duration = dpxSequenceReader.getShotDuration();

	AutoTrackingPreprocessor autoTrackingPreprocessor;
	autoTrackingPreprocessor.initSceneTracking(duration);
	CHRTimer hrt;
	auto t = 0.0;
	auto timeIter = 0;
	int frameIndex = 0;
	while (auto frame = dpxSequenceReader.getNextImage())
	{
		Ipp16uArray ccRgbFull16u = frame.value();
		auto bitDepth = dpxSequenceReader.getBitDepth();

		// we should be able to process gray scale
		if (ccRgbFull16u.getComponents() == 1)
		{
			MtiPlanar<Ipp16uArray> planes({ ccRgbFull16u ,ccRgbFull16u ,ccRgbFull16u });

			Ipp16uArray dummy;
			dummy.copyFromPlanar(planes);
			ccRgbFull16u = dummy;
		}

		// Normalization is done here, could be done and expanded above
		float scale = 1.0f / ((1 << bitDepth) - 1);
		auto ccGrayFull32f = Ipp32fArray(ccRgbFull16u.toGray());
		ccGrayFull32f *= scale;

		hrt.start();
		auto done = autoTrackingPreprocessor.processFrame(ccGrayFull32f, frameIndex);
		t += hrt.elapsedMilliseconds();
		timeIter++;
		std::cout << "Frame: " << frameIndex << ", done = " << std::boolalpha << done << ", " << t / timeIter << std::endl;
		frameIndex++;
	}


	if (!autoTrackingPreprocessor.getOpticalFlowAvailable())
	{
		throw std::runtime_error("no optical flow available, did you send enough files");
	}

	auto opticalFlow = autoTrackingPreprocessor.getOpticalFlow();

	std::ofstream binaryFile;
	binaryFile.open(clipData.outputFileName, std::ios::out | std::ios::binary);
	binaryFile.write((const char *)opticalFlow.data(), opticalFlow.size() * sizeof(OpticalFlowDatum));
	binaryFile.close();

	std::ofstream outFile(R"(c:\temp\test.txt)");
	for (auto &o : opticalFlow)
	{
		outFile << o.trackIndex << ", " << o.sceneStartIndex << ", " << o.fullCenterX << ", " << o.fullCenterY << ", " << o.proxyCenterX << ", " << o.proxyCenterY << std::endl;
	}

	//	SaveVariablesToMatFile1(R"(c:\temp\proxyMins.mat)", ccGrayProxy32f);
	return 0;
}

int doStandardizationOld(ClipData &clipData)
{
	DpxSequenceReader dpxSequenceReader(clipData.dpxFilePath, false);
	dpxSequenceReader.setShotTimecodesStartEnd(clipData.startTimeCode, clipData.endTimeCode);

	auto duration = dpxSequenceReader.getShotDuration();
	auto nbins = 1024;

	vector<vector<int>> counts(nbins);
	int totalPixels = 0;
	int downSample = 0;
	while (auto frame = dpxSequenceReader.getNextImage())
	{
		auto fullImage16u = frame.value();
		auto bitDepth = dpxSequenceReader.getBitDepth();
		downSample = bitDepth - 10;
		auto currentIndex = dpxSequenceReader.getLastReadIndex();
		DBTRACE2(dpxSequenceReader.getLastReadFileName(), currentIndex);
		auto grayImage16u = fullImage16u.toGray(); // .resize({ 512, 389 });
		auto width = grayImage16u.getWidth();
		auto height = grayImage16u.getHeight();
		for (auto r : range(height))
		{
			auto rp = grayImage16u.getRowPointer(r);
			for (auto c = 1; c < width - 1; c++)
			{
				auto v = rp[c] >> downSample;
				if (v >= nbins) v = nbins - 1;
				auto a = (rp[c - 1] + rp[c + 1]) / 2;
				counts[v].push_back(a);
			}
		}

		totalPixels += height * (width - 2);
	}

	vector<int> m20(nbins);
	vector<int> m80(nbins);
	vector<int> binSize(nbins);
	vector<int> centerLine(nbins);

	auto i = 0;
	auto binsTotals = 0;
	for (auto &bini : counts)
	{
		binSize[i] = bini.size();
		binsTotals += binSize[i];
		sort(bini.begin(), bini.end());
		auto n = int(bini.size());
		auto n20 = lround(0.2*n);
		auto n80 = lround(0.8*n);
		if ((n20 < n) && (n20 >= 0) && (n80 < n))
		{
			m20[i] = bini[n20];
			m80[i] = bini[n80];
		}
		centerLine[i] = i << downSample;
		++i;
	}
	SaveVariablesToMatFile4(R"(c:\temp\mvalues.mat)", m20, m80, binSize, centerLine);
	return 0;
}

int doStandardization(ClipData &clipData)
{
	DpxSequenceReader dpxSequenceReader(clipData.dpxFilePath, false);
	dpxSequenceReader.setShotTimecodesStartEnd(clipData.startTimeCode, clipData.endTimeCode);

	auto duration = dpxSequenceReader.getShotDuration();
	auto frame0 = dpxSequenceReader.getFirstImage();
	auto bitDepth = dpxSequenceReader.getBitDepth();
	auto downSampleBins = bitDepth - 10;
	auto nbins = 1 << 10;
	auto num_discrete_elements = 1 << bitDepth;

	vector <Ipp32sArray> counts;
	for (auto i : range(nbins))
	{
		Ipp32sArray v(num_discrete_elements);
		v.zero();
		counts.emplace_back(v);
	}

	dpxSequenceReader.restartReading();
	Ipp32sArray intensityHistogram(num_discrete_elements);
	while (auto frame = dpxSequenceReader.getNextImage())
	{
		auto fullImage16u = frame.value();
		auto currentIndex = dpxSequenceReader.getLastReadIndex();
		DBTRACE2(dpxSequenceReader.getLastReadFileName(), currentIndex);
		auto grayImage16u = fullImage16u.toGray(); // .resize({ 512, 389 });
		auto width = grayImage16u.getWidth();
		auto height = grayImage16u.getHeight();
		for (auto r : range(1, height - 1))
		{
			auto rpt = grayImage16u.getRowPointer(r - 1);
			auto rp = grayImage16u.getRowPointer(r);
			auto rpl = grayImage16u.getRowPointer(r + 1);

			for (auto c = 1; c < width - 1; c++)
			{
				auto v = rp[c] >> downSampleBins;
				if (v >= nbins) v = nbins - 1;
				auto a = int(rpt[c - 1]) + rpt[c + 1];
				a += rpl[c - 1] + rpl[c + 1];
				a /= 4;
				counts[v][a]++;
				intensityHistogram[v]++;
			}
		}
	}

	vector<int> m20(nbins);
	vector<int> m80(nbins);
	vector<int> binSize(nbins);
	vector<int> centerLine(nbins);

	auto i = 0;
	auto binsTotals = 0;
	for (auto i : range(counts.size()))
	{
		auto histogram = counts[i].toVector()[0];
		vector<int> bin(histogram.size());
		std::partial_sum(histogram.begin(), histogram.end(), bin.begin());

		auto n = bin.back();
		binSize[i] = bin.size();
		binsTotals += binSize[i];

		auto n20 = lround(0.2*n);
		auto n80 = lround(0.8*n);
		for (auto k : range(bin.size()))
		{
			if (bin[k] >= n20)
			{
				m20[i] = k;
				break;
			}
		}

		for (auto k : range(bin.size()))
		{
			if (bin[k] >= n80)
			{
				m80[i] = k;
				break;
			}
		}

		//if ((n20 < n) && (n20 >= 0) && (n80 < n))
		//{
		//   m20[i] = bin[n20];
		//   m80[i] = bin[n80];
	 //  }

	//   centerLine[i] = i << downSampleBins;
	}

	//DBTRACE2(totalPixels, binsTotals)
	SaveVariablesToMatFile1(R"(y:\temp\Histograms.mat)", counts);
	SaveVariablesToMatFile4(R"(y:\temp\mvalues.mat)", m20, m80, binSize, intensityHistogram);
	return 0;
}

//struct My16BitTestImage
//{
//	int ncols;
//	int nrows;
//	int nchannels;
//	int stride;
//	uint16_t * data;
//};

//struct My16BitTestImage
//{
//	MtiRect _roi;
//	int _components;
//	int stride;
//	uint16_t * _firstRoiElementPtr;
//};
template<class T>
class MyTestImage
{
public:
	MyTestImage(const MtiSize &size, int stride, T *data, int maxValue = 4095) :
		_size(size), _stride(stride), _firstRoiElementPtr(data)
	{
	}

private:
	MtiSize _size;
	int _stride;
	T * _firstRoiElementPtr;
	T _maxValue;
};

class My16BitTestImage : public MyTestImage<Ipp16u>
{
public:
	My16BitTestImage(const Ipp16uArray &image) : MyTestImage(image.getSize(), image.getRowPitchInBytes(), image.data())
	{
		auto x = (1 << image.getOriginalBits()) - 1;
	}
	
	My16BitTestImage(const MtiSize &size, int stride, Ipp16u *data) :
	        MyTestImage<Ipp16u>(size, stride, data)
	{

	}
};

int doStandardizationColor(ClipData &clipData)
{
	DpxSequenceReader dpxSequenceReader(clipData.dpxFilePath, false);
	dpxSequenceReader.setShotTimecodesStartEnd(clipData.startTimeCode, clipData.endTimeCode);

	auto duration = dpxSequenceReader.getShotDuration();
	auto frame0 = dpxSequenceReader.getFirstImage();
	auto bitDepth = dpxSequenceReader.getBitDepth();
	auto downSampleBins = bitDepth - 10;
	auto nbins = 1 << 10;
	auto num_discrete_elements = 1 << bitDepth;
	Ipp32sArray intensityHistogram(num_discrete_elements);
	intensityHistogram.zero();

	vector <Ipp32sArray> counts[3];
	for (auto i : range(nbins))
	{
		counts[0].emplace_back(num_discrete_elements);
		counts[0].back().zero();

		counts[1].emplace_back(num_discrete_elements);
		counts[1].back().zero();

		counts[2].emplace_back(num_discrete_elements);
		counts[2].back().zero();
	}

	vector <Ipp32sArray> monoCounts;
	for (auto i : range(nbins))
	{
		monoCounts.emplace_back(num_discrete_elements);
		monoCounts.back().zero();
	}

	dpxSequenceReader.restartReading();

	//	Ipp32sArray intensityHistogram(num_discrete_elements);
	auto totalPixels = 0;
	while (auto frame = dpxSequenceReader.getNextImage())
	{
		auto fullImage16u = frame.value();
		My16BitTestImage testFull = { fullImage16u};

		auto currentIndex = dpxSequenceReader.getLastReadIndex();
		DBTRACE2(dpxSequenceReader.getLastReadFileName(), currentIndex);
		auto width = fullImage16u.getWidth();
		auto height = fullImage16u.getHeight();

		auto image16uPlanes = fullImage16u.copyToPlanar(); // .resize({ 512, 389 });

		for (auto planeIndex : range(3))
		{
			auto total = 0;
			auto grayPlane16u = image16uPlanes[planeIndex];
			auto m = grayPlane16u.maxAndIndex();
			My16BitTestImage testImage(grayPlane16u({ 200, 200, 700, 500}));

			for (auto r : range(1, height - 1))
			{
				auto rpt = grayPlane16u.getRowPointer(r - 1);
				auto rp = grayPlane16u.getRowPointer(r);
				auto rpl = grayPlane16u.getRowPointer(r + 1);

				for (auto c = 1; c < width - 1; c++)
				{
					auto v = rp[c] >> downSampleBins;
					if (v >= nbins) v = nbins - 1;
					if (v < 0) v = 0;

					// avoid overflow					
					auto a = rpt[c - 1] / 4.0 + rpt[c + 1] / 4.0;
					a += rpl[c - 1] / 4.0 + rpl[c + 1] / 4.0;

					counts[planeIndex][v][int(a)]++;
					intensityHistogram[v]++;
					total++;
				}
			}
		}

		auto monoFullImage = fullImage16u.toGray(); // .resize({ 512, 389
		for (auto r : range(1, height - 1))
		{
			auto rpt = monoFullImage.getRowPointer(r - 1);
			auto rp = monoFullImage.getRowPointer(r);
			auto rpl = monoFullImage.getRowPointer(r + 1);

			for (auto c = 1; c < width - 1; c++)
			{
				auto v = int(rp[c]) >> downSampleBins;

				// These should never happen
				if (v >= nbins)
					v = nbins - 1;
				if (v < 0)
					v = 0;
				// avoid overflow
				auto a = rpt[c - 1] / 4.0 + rpt[c + 1] / 4.0;
				a += rpl[c - 1] / 4.0 + rpl[c + 1] / 4.0;
				monoCounts[v][int(a)]++;
			}
		}
	}

	auto countTotal = 0;
	for (auto k : range(nbins))
	{
		auto v = monoCounts[k];
		auto m0 = std::accumulate(v.begin(), v.end(), 0);
		if (m0 == 0)
		{
			DBTRACE2(countTotal, k);
		}

		countTotal += m0;
	}

	//	DBTRACE2(totalPixels, countTotal);
	MatIO::saveListToMatFile(R"(y:\temp\HistogramsColor.mat)", counts[0], "countR", counts[1], "countG", counts[2], "countB", monoCounts, "monoCounts");
	MatIO::saveListToMatFile(clipData.outputFileName, counts[0], "countR", counts[1], "countG", counts[2], "countB", monoCounts, "monoCounts");
	return 0;
}

void testStandardization()
{
	CHRTimer sortSpeed;
	auto m20 = Ipp16uArray(MatIO::readIpp32fArray(R"(c:\temp\mvalues.mat)", "m20"));
	auto m80 = Ipp16uArray(MatIO::readIpp32fArray(R"(c:\temp\mvalues.mat)", "m80"));

	auto lut = MtiMath::polyRegularize(m80, m20, 100 - 1, 725 - 1, 0.0001, 5);
}

int doRepair(ClipData &clipData, const ShineInteractiveParams &shineInteractiveParams)
{
	/////////////////  MOTION
	ShineRepairMask shineRepairMask;
	auto opticalFlowFileName = clipData.outputFileName;

	auto size = std::filesystem::file_size(opticalFlowFileName);
	auto elements = size / sizeof(OpticalFlowDatum);
	OpticalFlowData opticalFlow;
	opticalFlow.resize(elements);

	std::ifstream binaryFile;
	binaryFile.open(opticalFlowFileName, std::ios::in | std::ios::binary);
	binaryFile.read(reinterpret_cast<char *>(opticalFlow.data()), size);

	if (!binaryFile)
	{
		return -1;
	}

	shineRepairMask.setOpticalFlow(opticalFlow);

	DpxSequenceReader dpxSequenceReader(clipData.dpxFilePath);
	dpxSequenceReader.setShotTimecodesStartEnd(clipData.startTimeCode, clipData.endTimeCode);
	auto duration = dpxSequenceReader.getShotDuration();

	int repairTimecode = clipData.repairTimecode;
	auto repairFrameIndex = dpxSequenceReader.getIndexFromTimecode(repairTimecode);
	if (repairFrameIndex < 0)
	{
		THROW_MTI_RUNTIME_ERROR("No timecode found");
	}

	auto startFrameIndex = std::min<int>(std::max<int>(0, repairFrameIndex - 4), duration - 1);

	dpxSequenceReader.seekToShotIndex(startFrameIndex);
	while (auto frame = dpxSequenceReader.getNextImage())
	{
		Ipp16uArray ccRgbFull16u = frame.value();
		auto bitDepth = dpxSequenceReader.getBitDepth();
		auto currentIndex = dpxSequenceReader.getLastReadIndex();
		DBTRACE2("read frame", currentIndex)

			//      TestSortSpeed(ccRgbFull16u);

			   // we should be able to process gray scale
			if (ccRgbFull16u.getComponents() == 1)
			{
				MtiPlanar<Ipp16uArray> planes({ ccRgbFull16u ,ccRgbFull16u ,ccRgbFull16u });

				Ipp16uArray dummy;
				dummy.copyFromPlanar(planes);
				ccRgbFull16u = dummy;
			}

		// This is fucked up
		if (ccRgbFull16u.getComponents() == 1)
		{
			MtiPlanar<Ipp16uArray> planes({ ccRgbFull16u ,ccRgbFull16u ,ccRgbFull16u });

			Ipp16uArray dummy;
			dummy.copyFromPlanar(planes);
			ccRgbFull16u = dummy;
		}

		if (currentIndex == repairFrameIndex)
		{
			SaveVariablesToMatFile1(R"(c:\temp\test.mat)", ccRgbFull16u);
		}

		if (shineRepairMask.pushFrames({ ccRgbFull16u }, currentIndex, repairFrameIndex))
		{
			CHRTimer hrt;
			auto repairedFrame = shineRepairMask.repairImage(repairFrameIndex, shineInteractiveParams);
			std::cout << "Time: " << hrt << ", Repaired frame: " << repairFrameIndex << std::endl;
			repairFrameIndex++;
			auto repairMask = shineRepairMask.getRepairMask();
			AppendVariablesToMatFile2(R"(c:\temp\test.mat)", repairedFrame, repairMask);
			return 0;
		}
	}

	return 0;
}

int main(int argc, const char * argv[])
{
	//      ClipData clipData = { R"(Y:\correct\seinfeld_r1a\)",  R"(Y:\correct\seinfeld_r1a\ShineTestOpticalFlow.bin)", 4935, 4899, 4899 + 44 };
	//      ClipData clipData = { R"(D:\CorrectData\ShineTests\seinfeld_s02\)",  R"(D:\CorrectData\ShineTests\seinfeld_s02\ShineTestOpticalFlow.bin)", 5680, 5671, 5691 };
	//      ClipData clipData = { R"(D:\CorrectData\ShineTests\seinfeld_alpha_size_problem\)",  R"(D:\CorrectData\ShineTests\seinfeld_alpha_size_problem\ShineTestOpticalFlow.bin)", 447, 440, 450 };
	//     ClipData clipData = { R"(D:\CorrectData\Flicker\skrzydlaci_flicker_example\before\02\)",  R"(D:\CorrectData\Flicker\skrzydlaci_flicker_example\before\02\ShineTestOpticalFlow.bin)", 91543,  91543, 91682 };
		  //string inPath = R"(D:\CorrectData\ShineTests\bw_plane\vematr_1a_606811_plane_4k\)";
		  //string inPath = R"(D:\CorrectData\AlphaFilterStressTest\)";
	//   ClipData clipData = { R"(D:\CorrectData\ShineTests\bad_fix\)",  R"(D:\CorrectData\ShineTests\bad_fix\ShineTestOpticalFlow.bin)", 86608, 86603 };
	//   ClipData clipData = { R"(D:\CorrectData\ShineTests\watloo_motion\)",  R"(D:\CorrectData\ShineTests\watloo_motion\ShineTestOpticalFlow.bin)", 2641,  2627, 2757 };
	  // ClipData clipData = { R"(D:\CorrectData\ShineTests\bw_plane\vematr_1a_606811_plane_4k\)",  "", 5103,  5103, 5178 };
	ClipData clipData = { R"(D:\CorrectData\moon_over_miami\)",  "",112642, 112642, 112642 + 250 };
	//	ClipData clipData = { R"(D:\CorrectData\Flicker\skrzydlaci_flicker_example\before\02\)",  R"(D:\CorrectData\Flicker\skrzydlaci_flicker_example\before\02\ShineTestOpticalFlow.bin)", 91543,  91543, 91543 + 140 };
	//	ClipData clipData = { R"(D:\CorrectData\ColorBumps\All\)",  R"(c:\temp\HistogramsOldMan.mat)",       86902,        86902,       86902 + 10 };

	Ipp16uArray bigArray({ 10, 6 });
	bigArray.iota();
	auto testArray = bigArray({ 3, 2, 4, 2 });
	testArray.iota(3);
	Ipp8uArray junk({ 4 });
	junk.uniformDistribution(10, 255);
	junk.clear();
	Ipp32fArray dummy({ 4,3,3 });
	dummy.iota({ 0, 100, 1000 });
	auto array2 = Ipp32fArray(testArray) / 3.0;
	auto vectorData = Ipp16uArray(array2).toVector()[0];
	auto vectorData2 = array2.toVector()[0];
	Ipp16uArray array3({ 3, 4 }, vectorData.data());
	ShineInteractiveParams shineInteractiveParams;
	shineInteractiveParams.highScaledContrast = 10;  // Temporal
	shineInteractiveParams.lowScaledContrast = 100000000;
	shineInteractiveParams.highSpatialContrast = 10;  // Relative
	shineInteractiveParams.lowSpatialContrast = 100000000;
	shineInteractiveParams.fidelity = 80;
	shineInteractiveParams.maxSize = 50;
	shineInteractiveParams.minSize = 1;

	try
	{
		// doTracking(clipData);
//		doStandardization(clipData);
		doStandardizationColor(clipData);
		//testStandardization();
		 // doRepair(clipData, shineInteractiveParams);

	}
	catch (const std::exception &ex)
	{
		std::cout << "************************************  ERROR ************************************" << std::endl;
		std::cout << ex.what() << std::endl;
		return -1;
	}

	return 0;
}
