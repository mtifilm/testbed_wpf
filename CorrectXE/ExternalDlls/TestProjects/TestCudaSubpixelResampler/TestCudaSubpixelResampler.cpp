// TestCudaMtiCoreC.cpp : Defines the entry point for the console application.
//

#include "MtiCudaCoreInterface.h"
#include "DevSupportLib.h"
#include "Ippheaders.h"
#include "HRTimer.h"
#include "Dpx\dpx.h"

using namespace std;

int main(int argc, const char * argv[])
{
	int count;
	GpuAccessor gpuAccessor;

	if (gpuAccessor.doesGpuExist() == false)
	{
		std::cout << "No GPU found " << std::endl;
	}

//	std::string file = "D:\\CorrectData\\moon_over_miami\\112657.dpx";
	
	//string outfileName = "C:\\temp\\moon_over_miami.112657.mbin";
	//DevIO::dumpMatlabBinary(image, 16, outfileName);

	std::cout << "Number of gpus: " << gpuAccessor.getDeviceCount() << std::endl;
	std::cout << "Default gpu: " << gpuAccessor.getDefaultGpu() << ", name: " << gpuAccessor.getDeviceName() << std::endl;
	std::cout << "Compute Capability: " << gpuAccessor.getMajorComputeCapability() << "." << gpuAccessor.getMinorComputeCapability() << std::endl;
	
	std::cout << gpuAccessor.getInfoString(gpuAccessor.getDefaultGpu());

	std::string fileName = "D:\\CorrectData\\dewarp\\man_r3.017714.dpx";

	if (argc > 1)
	{
		fileName = string(argv[1]);
	}

	auto [input, bitDepth] = DevIO::readDpxFile(fileName, false); // ({ 500, 500, 10, 10 });
	auto sizeInBytes = input.area()*input.getComponents() * sizeof(Ipp16u);

	auto p = (Ipp16u *)DevIO::getPinnedMemory(sizeInBytes);
	Ipp16uArray output({ input.getSize(), 3 }, p);
	output *= 1 << (16 - bitDepth);

	int deviceId = 0;

	// Assume 10 bits, we need other info.
	CudaResampler16u translator(deviceId);
	translator.setMinMax(0, 0x03FF);
	CudaResampler16u translator2(deviceId);
	translator2.setMinMax(0, 0x03FF);
	CHRTimer hrt;

	cout << "Testing " << fileName << std::endl;
	cout << "(" << input.getWidth() << "," << input.getHeight() << ")" << std::endl;
	try
	{
		auto xoff = -0.5f;
		auto yoff = +.5f;

		string outfileName = "C:\\temp\\original.mbin";

		translator.translate(input, output, xoff, yoff);
		translator2.translate(input, output, xoff, yoff);

		outfileName = "C:\\temp\\cuda.mbin";
		//DevIO::dumpMatlabBinary(output, bitDepth, outfileName);
		hrt.start();

		for (auto i = 0; i < 10; i++)
		{
			translator.translateStartAsync(input, output, -5.44f, 1.84f);
			translator.translateCopyResultAsync();
			translator2.translateStartAsync(input, output, 405.44f, -350.84f);
			translator2.translateCopyResultAsync();
			translator.wait();
		}


		translator2.wait();
		std::cout << hrt << std::endl;
	}
	catch (std::exception &ex)
	{
		std::cout << "Error: " << ex.what() << std::endl;
	}

	string outfileName = "C:\\temp\\temp.mbin";
	//DevIO::dumpMatlabBinary(output, bitDepth, outfileName);

	// We have leak with input array
 	MtiCudaFreeHost(p);
	return 0;
}

