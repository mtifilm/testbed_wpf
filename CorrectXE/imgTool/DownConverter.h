#ifndef CDWNCNRTR
#define CDWNCNRTR

#include "machine.h"
#include "imgToolDLL.h"
#include "mthread.h"

class CImageFormat;

// the number of taps is the same as MAX_WEIGHTS
//
#define MAX_WEIGHTS 15
#define MIDPT        7

// wgts are 22-bit integers
#define WGTONE 4194304
#define WGTSHFT 22

//#define NO_MULTI

struct KCoeff
{
   int wgt[MAX_WEIGHTS];
};

class MTI_IMGTOOLDLL_API CDownConverter
{
public:

   CDownConverter(int numStripes = 1);

   ~CDownConverter();

#define DOWNCONVERT_ERROR_INVALID_KERNEL_SIZE   -1
#define DOWNCONVERT_ERROR_INVALID_FORMAT        -2
#define DOWNCONVERT_ERROR_INVALID_SRC_RECTANGLE -3
#define DOWNCONVERT_ERROR_INVALID_DST_RECTANGLE -4
#define DOWNCONVERT_ERROR_INSUFFICIENT_MEMORY   -5

   int initialize(const CImageFormat *srcFmt, RECT *srcRect, RECT *dstRect,
                                              int horzkern, int vertkern);

   void convert(MTI_UINT8 **srcBuf, MTI_UINT8 **dstBuf);

   /////////////////////////////////////////////////////////////////////////////

#define SD_FRAME_PITCH 1440

   MTI_UINT8 *srcbuf[2];  // source buffer of HD video (3 poss formats)

   int pitch1;            // pitch is 2*srcwdth + 4*KERNEL
   MTI_UINT8 *frmBuf1;    // target buffer for unpacked native frame


   int hmod;              // size of horz kernel repeat cycle
   KCoeff *hkern;         // horz kernel coeffs

   KCoeff **huvTbl;       // uv wgts selector table
   KCoeff **hy0Tbl;       // y0 wgts selector table
   KCoeff **hy1Tbl;       // y1 wgts selector table

   int *cntruvTbl;        // center  uv    pos table
   int *cntry0Tbl;        // center even y pos table
   int *cntry1Tbl;        // center odd  y pos table


   int vmod;              // size of vert kernel repeat cycle
   KCoeff *vkern;         // vert kernel coeffs

   KCoeff **vrtTbl;       // vertical coeff selector table

   int *cntrVTbl;         // center vertical pos table


   int pitch2;            // pitch is srchght + 2*KERNEL
   MTI_UINT8 *frmBuf2;    // target buffer after horizontal conversion

   MTI_UINT8 *dstbuf[2];  // target buffer after vertical conversion (interlaced)

   int frmwdth;
   int frmhght;
   int frmpitch;
   int frmcolorspace;
   int frmpelcomponents;
   int frmpelpacking;
   bool frmil;

   RECT srcrect;
   int srcwdth;
   int srchght;

   int xoffs;
   int phase;

   RECT dstrect;
   int dstwdth;
   int dsthght;

   /////////////////////////////////////////////////////////////////////////////

   int nStripes; // number of stripes in frame 1,2,4 etc.

   MTHREAD_STRUCT unpackThread;

   MTHREAD_STRUCT horzThread;

   MTHREAD_STRUCT vertThread;

private:

   int gcd(int, int);

   double Kernel(double, int, int, int);
};
#endif






