/*
    fc.cpp

    NOTE: All of the references to MAGNIFY_CONTROL structure
          member variables have "this->" in front of them.
          That's because I did a quick and dirty transformation
          to moive all the functions into the MAGNIFY_CONTROL
          structure to fix some thread safety issues. The structure
          variables used to all have "mcp->" prepended, so the
          safest way to change that to a member variable reference
          is to globally change the "mcp" to "this", to avoid
          massive name clash problems with variables that are
          declared locally in the methods.
    mbraca 2/28/03
*/

#include <iostream>
#include <string>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <time.h>      // time_t is broken in sys/types.h in RadStudio XE4
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "IniFile.h"
#include "MTImalloc.h"

#include <time.h>

#include "fc.h"
#include "bthread.h"

/**********************************************************************/
#define DOUBLE_TO_FLOAT_ROUNDING_OFFSET ((double)0.0005)
#define UPPER_CONVERSION_MATHOD_BEST 0
#define UPPER_CONVERSION_MATHOD_MEDIAN 1
#define UPPER_CONVERSION_MATHOD_FASTEST 2
#define UPPER_CONVERSION_MATHOD_SUPER_FASTEST 3

#define MAX_TABLE_SIZE 500

#define NEIGHBOR_TYPE_4X4 1
#define NEIGHBOR_TYPE_4X4_OVELAP_FOR_Y 2
#define NEIGHBOR_TYPE_2x2 3

#define PRE_TABLE_ENTRY_SIZE_IN_FLOATS 56

/* The computed coefficients must be put in the correct location:
                  x  0  1  2  3  x
                  4  5  6  7  8  9
                 10 11 12 13 14 15
                 16 17 18 19 20 21
                 22 23 24 25 26 27
                  x 28 29 30 31  x */ 
int iUpLoc[]={0,1,2,3,5,6,7,8,11,12,13,14,17,18,19,20};
int iDownLoc[]={11,12,13,14,17,18,19,20,23,24,25,26,28,29,30,31};
int iLeftLoc[]={4,5,6,7,10,11,12,13,16,17,18,19,22,23,24,25};
int iRightLoc[]={6,7,8,9,12,13,14,15,18,19,20,21,24,25,26,27};
int iMeLoc[]={5,6,7,8,11,12,13,14,17,18,19,20,23,24,25,26};

namespace {   /*********** BEGIN LOCAL NAMESPACE **************/

/* BEGIN: MODIFIED from Numerical Recipes for exepriment only. */
void nrerror(const char *error_text)
{
	fprintf(stderr,"Numerical Recipes run-time error...\n");
	fprintf(stderr,"%s\n",error_text);
	fprintf(stderr,"...now exiting to system...\n");
	exit(1);
}

void free_dvector(float *v,int nl,int nh)
{
	free((char*) (v+nl));
}

float *dvector(int nl,int nh)
{
	float *v;

#ifdef _DEBUG_ALLOC
printf ("%s %d allocating %d\n", __FILE__, __LINE__, (nh-nl+1)*sizeof(float));
#endif
	v=(float *)malloc((unsigned) (nh-nl+1)*sizeof(float));
	if (!v) nrerror("allocation failure in dvector()");
	return v-nl;
}

/*
**  mat_trans
**
**      Transpose a matrix.
**
**  Input:
**      a       -   the matrix to transpose
**      m, n    -   the size of the matrix. (mxn)
**
**  Output:
**      at      -   the transpose of a.
*/

int mat_trans(float **at, float **a, int m, int n)
{
    int     i, j;

    for (i = 0; i < m; i++)
        for (j = 0; j < n; j++)
            at[j][i] = a[i][j];
    return 0;
}

/*
**  mat_mult
**
**      Multiply two matrices.
**
**  Input:
**      a, b    -   the two matrices to multiply.
**      m, n, p -   the size of the matrices. (mxn and nxp)
**
**  Output:
**      prod    -   the product of two matrices.
*/

int mat_mult(float **prod, float **a, float **b, int m, int n, int p)
{
    int     i, j, k;

    for (i = 0; i < m; i++)
        for (j = 0; j < p; j++)
        {
            prod[i][j] = 0;
            for (k = 0; k < n; k++)
                prod[i][j] += a[i][k] * b[k][j];
        }
  return 0;
}

#include <math.h>

#define TINY 1.0e-20;

void ludcmp(float **a,int n,int *indx,float *d)
{
	int i,imax,j,k;
	float big,dum,sum,temp;
	float *vv;

	vv=dvector(0,n);
	*d=1.0;
	for (i=0;i<n;i++) {
		big=0.0;
		for (j=0;j<n;j++)
			if ((temp=fabs(a[i][j])) > big) big=temp;
		if (big == 0.0) nrerror("Singular matrix in routine LUDCMP");
		vv[i]=1.0/big;
	}
	for (j=0;j<n;j++) {
		for (i=0;i<j;i++) {
			sum=a[i][j];
			for (k=0;k<i;k++) sum -= a[i][k]*a[k][j];
			a[i][j]=sum;
		}
		big=0.0;
		for (i=j;i<n;i++) {
			sum=a[i][j];
			for (k=0;k<j;k++)
				sum -= a[i][k]*a[k][j];
			a[i][j]=sum;
			if ( (dum=vv[i]*fabs(sum)) >= big) {
				big=dum;
				imax=i;
			}
		}
		if (j != imax) {
			for (k=0;k<n;k++) {
				dum=a[imax][k];
				a[imax][k]=a[j][k];
				a[j][k]=dum;
			}
			*d = -(*d);
			vv[imax]=vv[j];
		}
		indx[j]=imax;
		if (a[j][j] == 0.0) a[j][j]=TINY;
		if (j != n-1) {
			dum=1.0/(a[j][j]);
			for (i=j+1;i<n;i++) a[i][j] *= dum;
		}
	}
	free_dvector(vv,0,n);
}

#undef TINY
void lubksb(float **a,int n,int *indx,float *b)
{
	int i,ii=(-1),ip,j;
	float sum;

	for (i=0;i<n;i++) {
		ip=indx[i];
		sum=b[ip];
		b[ip]=b[i];
		if (ii>=0)
			for (j=ii;j<=i-1;j++) sum -= a[i][j]*b[j];
		else if (sum) ii=i;
		b[i]=sum;
	}
	for (i=n-1;i>=0;i--) {
		sum=b[i];
		for (j=i+1;j<n;j++) sum -= a[i][j]*b[j];
		b[i]=sum/a[i][i];
	}
}

/* END: MODIFIED from Numerical Recipes for exepriment only. */

}  /*********************** END Local Namespace **************/

/** Compute pre-computed table size for one dimension. 
    This function does not depend on the image format. */
void MAGNIFY_CONTROL::ComputePreCalTableSize(float *fpStartPosSrc,float *fpSrcLength,
  int iDstLength,int *ipMinNumDesPointsInTable,
  float *fpInitialOffsetInTable)
  /* Determine how many items are needed in the table. 
     Imagine the following two lines:
     Src:  *       *       *       *       *       *...
     Dst:  x     x     x     x     x     x     x...
           |<-Repeatable pattern ->|
           |<- ->|  <=Cumulative size in src when dst has two pixels.
           |<-       ->| <=Cumulative size in src when dst has 3 pixels.
           |<-             ->| <=Cumulative size in src when dst has 4 pixels.
           ...
     The table only need to keep one repeatable pattern. */
{
  float fFactor, fFactorOrig, fSrcLengthOrig, fBestDiff, fDiff;
  float fCumulativeSizeInSrc;
  int iBestTableLen, iTableLen, iCumulativeSizeInSrc, iMaxTableSize;

/*
	The first step is to adjust the source length so as to constrain
	the repetition with the destination.
*/

  fSrcLengthOrig = *fpSrcLength;
  fFactorOrig=((float)iDstLength)/fSrcLengthOrig;

/*
	Run through all possible table sizes and find the smallest size that
	is closest to the source.
*/

  iBestTableLen = -1;
  fBestDiff = 1000.;
  iMaxTableSize = MAX_TABLE_SIZE;
  if (iMaxTableSize > iDstLength)
    iMaxTableSize = iDstLength;
  for (iTableLen = 1; iTableLen <= iMaxTableSize; iTableLen++)
   {
    fCumulativeSizeInSrc = (float)iTableLen / fFactorOrig;
    fDiff = fCumulativeSizeInSrc - (int)(fCumulativeSizeInSrc + 0.5);
    if (fDiff < 0)
      fDiff = -fDiff;

    if (fDiff < fBestDiff)
     {
      fBestDiff = fDiff;
      iBestTableLen = iTableLen;
     }
   }

/*
	Now we know the best table to use.  Reset the size of the source
	to match this.
*/

  iCumulativeSizeInSrc = ((float)iBestTableLen / fFactorOrig) + 0.5;

  fFactor = (float)iBestTableLen / (float)iCumulativeSizeInSrc;
  *fpSrcLength=((float)iDstLength)/fFactor;

/*
	Adjust the starting position of the source to balance the 
	change in source size.
*/

  *fpStartPosSrc -= (*fpSrcLength - fSrcLengthOrig) / 2.;

  *ipMinNumDesPointsInTable = iBestTableLen;

  /* Determine what is the offset in the destinatin for the first
     source input. 
     The above computation assumes that the first pixel in src and the
     first pixel in dst are aligned. It is not true in reality. */
  *fpInitialOffsetInTable=*fpStartPosSrc-(float)((int)*fpStartPosSrc);
}
 
/** */
void MAGNIFY_CONTROL::MakeParFilename(char *cpFilename)
{
  char caLine[1024];

  // this naming handles all SrcWidth, SrcHeight, DesWidth, DesHeight <= 9999
  // as long as FC_ALLOWED_OFFSET_ACCURACY <= 100
  sprintf(caLine,"%06d%06d%06d%06d%04d%04d.fc%d",
      (int)(this->fFirstDesRowOffsetInSrcStart*FC_ALLOWED_OFFSET_ACCURACY),
      (int)(this->fFirstDesColOffsetInSrcStart*FC_ALLOWED_OFFSET_ACCURACY),
      (int)(this->fSrcConvertHeight*FC_ALLOWED_OFFSET_ACCURACY),
      (int)(this->fSrcConvertWidth*FC_ALLOWED_OFFSET_ACCURACY),
      this->iDesConvertHeight,this->iDesConvertWidth,
      this->iSearchFlatRegion);

  if(this->strParfileDir=="")
    strcpy(cpFilename,caLine);
  else
   {
#ifdef _WINDOWS
    sprintf(cpFilename,"%s\\%s",this->strParfileDir.c_str(),caLine);
#else
    sprintf(cpFilename,"%s/%s",this->strParfileDir.c_str(),caLine);
#endif
   }
}

/** */
void MAGNIFY_CONTROL::DoSaveParFile()
{
  FILE *fp;
  int iSize;

  iSize=this->iMinColsInPreCalTable*this->iMinRowsInPreCalTable
    *PRE_TABLE_ENTRY_SIZE_IN_FLOATS;

  fp=fopen(this->caParFileName,"wb+");
  if(fp==NULL)
   {
    fprintf (stderr, "Unable to open par file: %s\n", this->caParFileName);
    TRACE_0(errout << "Unable to open the par file");
   }
  if((int)fwrite(this->fpTotalPreCal,sizeof(float),iSize,fp)!=iSize)
   {
    fprintf (stderr, "Unable to write to par file: %s\n", this->caParFileName);
    TRACE_0(errout << "Unable to open the par file");
   }
  fclose(fp);
}

/** */
void MAGNIFY_CONTROL::SaveParFile()
{
  if (this->vpCriticalSectionParFileIO)
   {
    if (CriticalSectionEnter(this->vpCriticalSectionParFileIO))
     {
      fprintf (stderr, "Unable to enter critical section\n");
      exit (1);
     }

    DoSaveParFile();

    if (CriticalSectionExit(this->vpCriticalSectionParFileIO))
     {
      fprintf (stderr, "Unable to exit critical section\n");
      exit (1);
     }
   }
  else
   {
    // no critical section, so just call directly
    DoSaveParFile();
   }
}

/** */
int MAGNIFY_CONTROL::DoLoadParFile()
{
  FILE *fp;
  int iSize;

  iSize=this->iMinColsInPreCalTable *this->iMinRowsInPreCalTable
    *PRE_TABLE_ENTRY_SIZE_IN_FLOATS;
#ifdef _DEBUG_EDL
  printf("Trying to load par file %s\n",this->caParFileName);
#endif
  fp=fopen(this->caParFileName,"rb");

  if(fp==NULL)
    return FALSE;
  if((int)fread(this->fpTotalPreCal,sizeof(float),iSize,fp)!=iSize)
    TRACE_0(errout << "Unable to open the par file");
  fclose(fp);

  return TRUE;
}

int MAGNIFY_CONTROL::LoadParFile()
{
  int iRet;

  if (this->vpCriticalSectionParFileIO)
   {
    if (CriticalSectionEnter(this->vpCriticalSectionParFileIO))
     {
      fprintf (stderr, "Unable to enter critical section\n");
      exit (1);
     }

    iRet=DoLoadParFile();

    if (CriticalSectionExit(this->vpCriticalSectionParFileIO))
     {
      fprintf (stderr, "Unable to exit critical section\n");
      exit (1);
     }
   }
  else
   {
    // no critical section, so just call directly
    iRet=DoLoadParFile();
   }

  return iRet;
}

/** For a given down-conversion factor value,
    decide what size of the filtering mask should be used. */
void MAGNIFY_CONTROL::DetermineLowFilterSize(float fFactor,int *ipSize)
{
  if(fFactor>0.8)
    *ipSize=1;
  else
    *ipSize=(int)(1.0/fFactor+0.5);
}

/** */
void MAGNIFY_CONTROL::Fill2DLowPassFilerParamters(int iNumLowPassFilterRows,
  int iNumLowPassFilterCols,float fRowFactor,float fColFactor,
  float *fpLowPassFilerParamters)
{
  int iRow,iCol;
  int iNumLowPassFilterRowsIdeal;
  int iNumLowPassFilterColsIdeal;
  float fTotalWeight,fWeight;

  DetermineLowFilterSize(fRowFactor,&iNumLowPassFilterRowsIdeal);
  DetermineLowFilterSize(fColFactor,&iNumLowPassFilterColsIdeal);

  fTotalWeight=(float)0.0;
  for(iRow=0;iRow<iNumLowPassFilterRows;iRow++)
    {
    for(iCol=0;iCol<iNumLowPassFilterCols;iCol++)
      {
      fWeight=(float)1.0;
      /* The filter size must be odd. When the ideal size is even,
         we force it to be odd with compensated parameters. */
      if((iCol==0 || iCol==iNumLowPassFilterCols-1) &&
         iNumLowPassFilterCols!=iNumLowPassFilterColsIdeal)
         fWeight/=(float)2.0;
      if((iRow==0 || iRow==iNumLowPassFilterRows-1) &&
         iNumLowPassFilterRows!=iNumLowPassFilterRowsIdeal)
         fWeight/=(float)2.0;

      *(fpLowPassFilerParamters+iRow*iNumLowPassFilterCols+iCol)
        =fWeight;
      fTotalWeight+=fWeight;
    }
  }
  /* Normalize them. */
  for(iRow=0;iRow<iNumLowPassFilterRows;iRow++)
    {
    for(iCol=0;iCol<iNumLowPassFilterCols;iCol++)
      {
      *(fpLowPassFilerParamters+iRow*iNumLowPassFilterCols+iCol)
        /=fTotalWeight;
    }
  }
}

#define SET_ROW_POINTER_FOR_FILTER \
if(iInterlacing && (iRow & 1)) \
  ucpDst=ucpDst1Corrected; \
else \
  ucpDst=ucpDst0; \
  ucpDst+=iNumBytesPerRow*iRow

#define FILTER_A_BORDER_PIXEL \
  fWeightY=fWeightUV=fTotalWeightY=fTotalWeightUV=(float)0.0; \
  fpParmRow=fpLowPassFilerParamters; \
  iC0Y=iCol-iHalfFilterCols; \
  iC1Y=iCol+iHalfFilterCols; \
  iC0UV=iCol-(iHalfFilterCols<<1); \
  iC1UV=iCol+(iHalfFilterCols<<1); \
  ucpSrcColReady0Y=ucpSrc0+(iC0Y<<2)+2; \
  ucpSrcColReady1Y=ucpSrc1Corrected+(iC0Y<<2)+2; \
  ucpSrcColReady0UV=ucpSrc0+(iC0UV<<2); \
  ucpSrcColReady1UV=ucpSrc1Corrected+(iC0UV<<2); \
  for(iR=iRow-iRowBorder;iR<=iRow+iRowBorder;++iR) \
    { \
    if(iR>=0 && iR<iTotalNumRows) \
      { \
      if(iInterlacing && (iR & 1)) \
        { \
        ucpSrcY=ucpSrcColReady1Y; \
        ucpSrcUV=ucpSrcColReady1UV; \
      } \
      else \
        { \
        ucpSrcY=ucpSrcColReady0Y; \
        ucpSrcUV=ucpSrcColReady0UV; \
      } \
      ucpSrcY+=(iR*iNumBytesPerRow); \
      ucpSrcUV+=(iR*iNumBytesPerRow); \
      fpParmCol=fpParmRow; \
      for(iC=iC0Y;iC<=iC1Y;++iC) \
        { \
        if(iC>=0 && iC<iTotalNumCols) \
          { \
          fWeightY+=((float)(*((unsigned short *)ucpSrcY)))*(*fpParmCol); \
          fTotalWeightY+=(*fpParmCol); \
        } \
        ucpSrcY+=4; \
        fpParmCol++; \
      } /* iC */ \
      fpParmCol=fpParmRow; \
      for(iC=iC0UV;iC<=iC1UV;iC+=2) \
        { \
        if(iC>=0 && iC<iTotalNumCols) \
          { \
          fWeightUV+=((float)(*((unsigned short *)ucpSrcUV)))*(*fpParmCol); \
          fTotalWeightUV+=(*fpParmCol); \
        } \
        ucpSrcUV+=8; \
        fpParmCol++; \
      } /* iC */ \
   } \
    fpParmRow+=iNumLowPassFilterCols; \
  } /* iR */ \
  *((unsigned short *)ucpDst)=(unsigned short)(fWeightUV/fTotalWeightUV); \
  *((unsigned short *)(ucpDst+2)) \
    =(unsigned short)(fWeightY/fTotalWeightY); \
  ucpDst+=4

#define DO_3X3_FILTER_INTERIOR \
        for(iCol=iColBorder+2;iCol<iTotalNumCols-iColBorder;++iCol) \
	  { \
          *((unsigned short *)(ucpDst+2)) \
	    =(unsigned short)(( \
           (*((unsigned short *)(ucpSrcY0-4))) + \
           (*((unsigned short *)(ucpSrcY0))) + \
           (*((unsigned short *)(ucpSrcY0+4))) + \
           (*((unsigned short *)(ucpSrcY1-4))) + \
           (*((unsigned short *)(ucpSrcY1))) + \
           (*((unsigned short *)(ucpSrcY1+4))) + \
           (*((unsigned short *)(ucpSrcY2-4))) + \
           (*((unsigned short *)(ucpSrcY2))) + \
           (*((unsigned short *)(ucpSrcY2+4)))) \
           /iTotalWeight); \
          *((unsigned short *)ucpDst) \
	    =(unsigned short)(( \
           (*((unsigned short *)(ucpSrcUV0-8))) + \
           (*((unsigned short *)(ucpSrcUV0))) + \
           (*((unsigned short *)(ucpSrcUV0+8))) + \
           (*((unsigned short *)(ucpSrcUV1-8))) + \
           (*((unsigned short *)(ucpSrcUV1))) + \
           (*((unsigned short *)(ucpSrcUV1+8))) + \
           (*((unsigned short *)(ucpSrcUV2-8))) + \
           (*((unsigned short *)(ucpSrcUV2))) + \
           (*((unsigned short *)(ucpSrcUV2+8)))) \
           /iTotalWeight); \
          ucpDst+=4; \
          ucpSrcY0+=4; \
          ucpSrcY1+=4; \
          ucpSrcY2+=4; \
          ucpSrcUV0+=4; \
          ucpSrcUV1+=4; \
          ucpSrcUV2+=4; \
	}

/** Low-pass filter on one frame, assuming the image format is
    YUV 422, 16-bit. */
void MAGNIFY_CONTROL::LowPassFilterOnThisFrame(unsigned char *ucpSrc0,
  unsigned char *ucpSrc1,unsigned char *ucpDst0,unsigned char *ucpDst1,
  int iTotalNumRows,int iTotalNumCols,int iInterlacing,
  int iNumLowPassFilterRows,int iNumLowPassFilterCols,
  float *fpLowPassFilerParamters)
{
  int iRow,iCol;
  unsigned char *ucpSrcY,*ucpSrcUV,
    *ucpSrcColReady0Y,*ucpSrcColReady0UV,
    *ucpSrcColReady1Y,*ucpSrcColReady1UV,
    *ucpDst,*ucpSrc1Corrected,*ucpDst1Corrected;
  unsigned char *ucpSrcY0,*ucpSrcUV0;
  unsigned char *ucpSrcY1,*ucpSrcUV1;
  unsigned char *ucpSrcY2,*ucpSrcUV2;
  int iRowBorder,iColBorder;
  int iHalfFilterCols;
  int iNumBytesPerRow;
  int iColBytesBorder;
  int iR,iC,iC0Y,iC1Y,iC0UV,iC1UV;
  float fTotalWeightY,fTotalWeightUV,*fpParmRow,*fpParmCol;
  float fWeightY,fWeightUV;
  int iWeightY,iTotalWeight;
  int *ipWeightUV,iWeightU,iWeightV;
  int *ipFilterFistColSumY,*ipFilterFistColSumUV;
  int *ipFilterFistColSumU,*ipFilterFistColSumV;
  int iFilterFistColSumY,iFilterFistColSumUV;

  iTotalWeight=iNumLowPassFilterRows*iNumLowPassFilterCols;

#ifdef _DEBUG_ALLOC
printf ("%s %d allocating %d\n", __FILE__, __LINE__, 3*iTotalNumCols*sizeof(int));
#endif
  ipFilterFistColSumY=(int *)malloc(iTotalNumCols*sizeof(int));
  ipFilterFistColSumU=(int *)malloc(iTotalNumCols*sizeof(int));
  ipFilterFistColSumV=(int *)malloc(iTotalNumCols*sizeof(int));

  if(ipFilterFistColSumY==NULL || ipFilterFistColSumU==NULL
      || ipFilterFistColSumV==NULL)
    TRACE_0(errout << "Unable to open the par file");

  if(iInterlacing)
    {
    iNumBytesPerRow=iTotalNumCols<<1;
    ucpSrc1Corrected=ucpSrc1-iNumBytesPerRow;
    ucpDst1Corrected=ucpDst1-iNumBytesPerRow;
  }
  else
    {
    iNumBytesPerRow=iTotalNumCols<<2;
    ucpSrc1Corrected=ucpSrc1;
    ucpDst1Corrected=ucpDst1;
  }
  iRowBorder=iNumLowPassFilterRows/2;
  iHalfFilterCols=iColBorder=iNumLowPassFilterCols/2;
  /* iColBorder must be doubled in order to help interior inside. */
  iColBorder*=2;

  iColBytesBorder=iColBorder<<2;

  /* Do border first. */
  for(iRow=0;iRow<iRowBorder;++iRow)
    {
    SET_ROW_POINTER_FOR_FILTER;
    for(iCol=0;iCol<iTotalNumCols;++iCol)
      {
      FILTER_A_BORDER_PIXEL;
    }
  }

  for(iRow=iTotalNumRows-iRowBorder;iRow<iTotalNumRows;++iRow)
    {
    SET_ROW_POINTER_FOR_FILTER;
    for(iCol=0;iCol<iTotalNumCols;++iCol)
      {
      FILTER_A_BORDER_PIXEL;
    }  
  }

  for(iRow=0;iRow<iTotalNumRows;++iRow)
    {
    SET_ROW_POINTER_FOR_FILTER;
    for(iCol=0;iCol<iColBorder;++iCol)
      {
      FILTER_A_BORDER_PIXEL;
    }
  }

  for(iRow=0;iRow<iTotalNumRows;++iRow)
    {
    SET_ROW_POINTER_FOR_FILTER;
    ucpDst+=((iTotalNumCols-iColBorder)<<2);
    for(iCol=iTotalNumCols-iColBorder;iCol<iTotalNumCols;++iCol)
      {
      FILTER_A_BORDER_PIXEL;
    }
  }

  /* Filter the interior. */
  for(iRow=iRowBorder;iRow<iTotalNumRows-iRowBorder;++iRow)
    {
    SET_ROW_POINTER_FOR_FILTER;
    ucpDst+=iColBytesBorder;

    /* Interior will be treated as uniform only for speed. */

    /* First two points are fully computed. */
    for(iCol=iColBorder;iCol<iColBorder+2;++iCol)
      {
      iC0Y=iCol-iHalfFilterCols;
      iC1Y=iCol+iHalfFilterCols;
      iC0UV=iCol-(iHalfFilterCols<<1);
      iC1UV=iCol+(iHalfFilterCols<<1);
      ucpSrcColReady0Y=ucpSrc0+(iC0Y<<2)+2;
      ucpSrcColReady1Y=ucpSrc1Corrected+(iC0Y<<2)+2;
      ucpSrcColReady0UV=ucpSrc0+(iC0UV<<2);
      ucpSrcColReady1UV=ucpSrc1Corrected+(iC0UV<<2);
      if(iCol & 1)
        {
	ipWeightUV=&iWeightV;
        ipFilterFistColSumUV=ipFilterFistColSumV;
      }
      else
        {
	ipWeightUV=&iWeightU;
        ipFilterFistColSumUV=ipFilterFistColSumU;
      }
      iWeightY=0;
      *ipWeightUV=0;
      for(iC=iC0Y;iC<=iC1Y;++iC)
        ipFilterFistColSumY[iC]=0;
      for(iC=iC0UV;iC<=iC1UV;iC+=2)
        ipFilterFistColSumUV[iC]=0;

      for(iR=iRow-iRowBorder;iR<=iRow+iRowBorder;++iR)
        {
        if(iInterlacing && (iR & 1))
          {
          ucpSrcY=ucpSrcColReady1Y;
          ucpSrcUV=ucpSrcColReady1UV;
        }
        else
          {
          ucpSrcY=ucpSrcColReady0Y;
          ucpSrcUV=ucpSrcColReady0UV;
        }
        ucpSrcY+=(iR*iNumBytesPerRow);
        ucpSrcUV+=(iR*iNumBytesPerRow);
        for(iC=iC0Y;iC<=iC1Y;++iC)
          {
          iWeightY+=(*((unsigned short *)ucpSrcY));
          ipFilterFistColSumY[iC]+=(*((unsigned short *)ucpSrcY));
          ucpSrcY+=4;
        } /* iC */
        for(iC=iC0UV;iC<=iC1UV;iC+=2)
          {
          (*ipWeightUV)+=(*((unsigned short *)ucpSrcUV));
          ipFilterFistColSumUV[iC]+=(*((unsigned short *)ucpSrcUV));
          ucpSrcUV+=8;
        } /* iC */
      } /* iR */
      *((unsigned short *)ucpDst)=(unsigned short)((*ipWeightUV)/iTotalWeight);
      *((unsigned short *)(ucpDst+2))=(unsigned short)(iWeightY/iTotalWeight);
      ucpDst+=4;
    } 

    /* After two points, we subtract each first-col sum and 
       add last-col sum to get the total in general. 3x3 case
       is treated differently because direct computing is faster. */
    if(iNumLowPassFilterRows==3 && iNumLowPassFilterCols==3)
      {
      /* Unwrap 3x3 case. */
      if(iInterlacing && (iRow & 1))
	{
        ucpSrcY0=ucpSrc0+(iRow-1)*iNumBytesPerRow
          +((iColBorder+2)<<2)+2;
        ucpSrcY1=ucpSrc1Corrected+(iRow)*iNumBytesPerRow
          +((iColBorder+2)<<2)+2;
        ucpSrcY2=ucpSrc0+(iRow+1)*iNumBytesPerRow
          +((iColBorder+2)<<2)+2;
        ucpSrcUV0=ucpSrc0+(iRow-1)*iNumBytesPerRow
          +((iColBorder+2)<<2);
        ucpSrcUV1=ucpSrc1Corrected+(iRow)*iNumBytesPerRow
          +((iColBorder+2)<<2);
        ucpSrcUV2=ucpSrc0+(iRow+1)*iNumBytesPerRow
          +((iColBorder+2)<<2);
        DO_3X3_FILTER_INTERIOR
      }
      else if(iInterlacing)
	{
        ucpSrcY0=ucpSrc1Corrected+(iRow-1)*iNumBytesPerRow
          +((iColBorder+2)<<2)+2;
        ucpSrcY1=ucpSrc0+(iRow)*iNumBytesPerRow
          +((iColBorder+2)<<2)+2;
        ucpSrcY2=ucpSrc1Corrected+(iRow+1)*iNumBytesPerRow
          +((iColBorder+2)<<2)+2;
        ucpSrcUV0=ucpSrc1Corrected+(iRow-1)*iNumBytesPerRow
          +((iColBorder+2)<<2);
        ucpSrcUV1=ucpSrc0+(iRow)*iNumBytesPerRow
          +((iColBorder+2)<<2);
        ucpSrcUV2=ucpSrc1Corrected+(iRow+1)*iNumBytesPerRow
          +((iColBorder+2)<<2);
        DO_3X3_FILTER_INTERIOR
      }
      else
	{
        ucpSrcY0=ucpSrc0+(iRow-1)*iNumBytesPerRow
          +((iColBorder+2)<<2)+2;
        ucpSrcY1=ucpSrc0+(iRow)*iNumBytesPerRow
          +((iColBorder+2)<<2)+2;
        ucpSrcY2=ucpSrc0+(iRow+1)*iNumBytesPerRow
          +((iColBorder+2)<<2)+2;
        ucpSrcUV0=ucpSrc0+(iRow-1)*iNumBytesPerRow
          +((iColBorder+2)<<2);
        ucpSrcUV1=ucpSrc0+(iRow)*iNumBytesPerRow
          +((iColBorder+2)<<2);
        ucpSrcUV2=ucpSrc0+(iRow+1)*iNumBytesPerRow
          +((iColBorder+2)<<2);
        DO_3X3_FILTER_INTERIOR
      }
    }
    else
      {
      /* Any size case */
      iC0Y=iColBorder-iHalfFilterCols;
      iC1Y=iColBorder+1+iHalfFilterCols;
      iC0UV=iColBorder-1-(iHalfFilterCols<<1);
      iC1UV=iColBorder+1+(iHalfFilterCols<<1);
      ucpSrcColReady0Y=ucpSrc0+(iC1Y<<2)+2;
      ucpSrcColReady1Y=ucpSrc1Corrected+(iC1Y<<2)+2;
      ucpSrcColReady0UV=ucpSrc0+(iC1UV<<2);
      ucpSrcColReady1UV=ucpSrc1Corrected+(iC1UV<<2);
      for(iCol=iColBorder+2;iCol<iTotalNumCols-iColBorder;++iCol)
        {
        ++iC0Y;
        ++iC1Y;
        ++iC0UV;
        ++iC1UV;
        ucpSrcColReady0Y+=4;
        ucpSrcColReady1Y+=4;
        ucpSrcColReady0UV+=4;
        ucpSrcColReady1UV+=4;
        if(iCol & 1)
          {
          ipWeightUV=&iWeightV;
          ipFilterFistColSumUV=ipFilterFistColSumV;
        }
        else
          {
          ipWeightUV=&iWeightU;
          ipFilterFistColSumUV=ipFilterFistColSumU;
        }

        /* Compute the last contribute col sum. */
        iFilterFistColSumY=0;
        iFilterFistColSumUV=0;
        for(iR=iRow-iRowBorder;iR<=iRow+iRowBorder;++iR)
          {
          if((iR & 1)==0 || iInterlacing==0)
            {
            ucpSrcY=ucpSrcColReady0Y;
            ucpSrcUV=ucpSrcColReady0UV;
          }
          else
            {
            ucpSrcY=ucpSrcColReady1Y;
            ucpSrcUV=ucpSrcColReady1UV;
          }
          ucpSrcY+=(iR*iNumBytesPerRow);
          ucpSrcUV+=(iR*iNumBytesPerRow);
          iFilterFistColSumY += (*((unsigned short *)(ucpSrcY)));
          iFilterFistColSumUV += (*((unsigned short *)(ucpSrcUV)));
        }

        /* Remember it for the next time. */
        ipFilterFistColSumY[iC1Y]=iFilterFistColSumY;
        ipFilterFistColSumUV[iC1UV]=iFilterFistColSumUV;
        *ipWeightUV=(*ipWeightUV)
          -ipFilterFistColSumUV[iC0UV]+iFilterFistColSumUV;
        iWeightY=iWeightY-ipFilterFistColSumY[iC0Y]+iFilterFistColSumY;
      
        *((unsigned short *)ucpDst)
          =(unsigned short)((*ipWeightUV)/iTotalWeight);
        *((unsigned short *)(ucpDst+2))
          =(unsigned short)(iWeightY/iTotalWeight);
        ucpDst+=4;
      } /* iCol */
    } /* Special size cases. */
  } /* iRow */
  free(ipFilterFistColSumY);
  free(ipFilterFistColSumU);
  free(ipFilterFistColSumV);
}

#define FILTER_A_BORDER_PIXEL_444 \
  fWeightY=fWeightU=fWeightV=fTotalWeightY=(float)0.0; \
  fpParmRow=fpLowPassFilerParamters; \
  iC0Y=iCol-iHalfFilterCols; \
  iC1Y=iCol+iHalfFilterCols; \
  ucpSrcColReady0Y=ucpSrc0+(iC0Y*6); \
  ucpSrcColReady1Y=ucpSrc1Corrected+(iC0Y*6); \
  for(iR=iRow-iRowBorder;iR<=iRow+iRowBorder;++iR) \
    { \
    if(iR>=0 && iR<iTotalNumRows) \
      { \
      if(iInterlacing && (iR & 1)) \
        { \
        ucpSrcY=ucpSrcColReady1Y; \
      } \
      else \
        { \
        ucpSrcY=ucpSrcColReady0Y; \
      } \
      ucpSrcY+=(iR*iNumBytesPerRow); \
      fpParmCol=fpParmRow; \
      for(iC=iC0Y;iC<=iC1Y;++iC) \
        { \
        if(iC>=0 && iC<iTotalNumCols) \
          { \
          fWeightY+=((float)(*((unsigned short *)ucpSrcY)))*(*fpParmCol); \
          ucpSrcY+=2; \
          fWeightU+=((float)(*((unsigned short *)ucpSrcY)))*(*fpParmCol); \
          ucpSrcY+=2; \
          fWeightV+=((float)(*((unsigned short *)ucpSrcY)))*(*fpParmCol); \
          ucpSrcY+=2; \
          fTotalWeightY+=(*fpParmCol); \
        } \
        else \
          ucpSrcY+=6; \
        fpParmCol++; \
      } /* iC */ \
    } \
    fpParmRow+=iNumLowPassFilterCols; \
  } /* iR */ \
  *((unsigned short *)ucpDst)=(unsigned short)(fWeightY/fTotalWeightY); \
  ucpDst +=2 ; \
  *((unsigned short *)ucpDst)=(unsigned short)(fWeightU/fTotalWeightY); \
  ucpDst +=2 ; \
  *((unsigned short *)ucpDst)=(unsigned short)(fWeightV/fTotalWeightY); \
  ucpDst +=2

#define DO_3X3_FILTER_INTERIOR_444 \
          *((unsigned short *)(ucpDst)) \
	    =(unsigned short)(( \
           (*((unsigned short *)(ucpSrcY0-6))) + \
           (*((unsigned short *)(ucpSrcY0))) + \
           (*((unsigned short *)(ucpSrcY0+6))) + \
           (*((unsigned short *)(ucpSrcY1-6))) + \
           (*((unsigned short *)(ucpSrcY1))) + \
           (*((unsigned short *)(ucpSrcY1+6))) + \
           (*((unsigned short *)(ucpSrcY2-6))) + \
           (*((unsigned short *)(ucpSrcY2))) + \
           (*((unsigned short *)(ucpSrcY2+6)))) \
           /iTotalWeight); \
          ucpDst+=2; \
          ucpSrcY0+=2; \
          ucpSrcY1+=2; \
          ucpSrcY2+=2;

/** Low-pass filter on one frame, assuming the image format is
    YUV 444, 16-bit. Three components are treated in the same way. */
void MAGNIFY_CONTROL::LowPassFilterOnThisFrame444(unsigned char *ucpSrc0,
  unsigned char *ucpSrc1,unsigned char *ucpDst0,unsigned char *ucpDst1,
  int iTotalNumRows,int iTotalNumCols,int iInterlacing,
  int iNumLowPassFilterRows,int iNumLowPassFilterCols,
  float *fpLowPassFilerParamters)
{
  int iRow,iCol;
  unsigned char *ucpSrcY,
    *ucpSrcColReady0Y,
    *ucpSrcColReady1Y,
    *ucpDst,*ucpSrc1Corrected,*ucpDst1Corrected;
  unsigned char *ucpSrcY0;
  unsigned char *ucpSrcY1;
  unsigned char *ucpSrcY2;
  int iRowBorder,iColBorder;
  int iHalfFilterCols;
  int iNumBytesPerRow;
  int iColBytesBorder;
  int iR,iC,iC0Y,iC1Y;
  float fTotalWeightY,*fpParmRow,*fpParmCol;
  float fWeightY,fWeightU,fWeightV;
  int iWeightY,iTotalWeight;
  int iWeightU,iWeightV;
  int *ipFilterFistColSumY;
  int *ipFilterFistColSumU,*ipFilterFistColSumV;
  int iFilterFistColSumY,iFilterFistColSumU,iFilterFistColSumV;

  iTotalWeight=iNumLowPassFilterRows*iNumLowPassFilterCols;

#ifdef _DEBUG_ALLOC
printf ("%s %d allocating %d\n", __FILE__, __LINE__, 3*iTotalNumCols*sizeof(int));
#endif
  ipFilterFistColSumY=(int *)malloc(iTotalNumCols*sizeof(int));
  ipFilterFistColSumU=(int *)malloc(iTotalNumCols*sizeof(int));
  ipFilterFistColSumV=(int *)malloc(iTotalNumCols*sizeof(int));

  if(ipFilterFistColSumY==NULL || ipFilterFistColSumU==NULL
      || ipFilterFistColSumV==NULL)
    TRACE_0(errout << "Unable to open the par file");

  if(iInterlacing)
    {
    iNumBytesPerRow=iTotalNumCols*3;
    ucpSrc1Corrected=ucpSrc1-iNumBytesPerRow;
    ucpDst1Corrected=ucpDst1-iNumBytesPerRow;
  }
  else
    {
    iNumBytesPerRow=iTotalNumCols*6;
    ucpSrc1Corrected=ucpSrc1;
    ucpDst1Corrected=ucpDst1;
  }
  iRowBorder=iNumLowPassFilterRows/2;
  iHalfFilterCols=iColBorder=iNumLowPassFilterCols/2;
  /* iColBorder must be doubled in order to help interior inside. */
  iColBorder*=2;

  iColBytesBorder=iColBorder*6;

  /* Do border first. */
  for(iRow=0;iRow<iRowBorder;++iRow)
    {
    SET_ROW_POINTER_FOR_FILTER;
    for(iCol=0;iCol<iTotalNumCols;++iCol)
      {
      FILTER_A_BORDER_PIXEL_444;
    }
  }
  for(iRow=iTotalNumRows-iRowBorder;iRow<iTotalNumRows;++iRow)
    {
    SET_ROW_POINTER_FOR_FILTER;
    for(iCol=0;iCol<iTotalNumCols;++iCol)
      {
      FILTER_A_BORDER_PIXEL_444;
    }
  }

  for(iRow=0;iRow<iTotalNumRows;++iRow)
    {
    SET_ROW_POINTER_FOR_FILTER;
    for(iCol=0;iCol<iColBorder;++iCol)
      {
      FILTER_A_BORDER_PIXEL_444;
    }
  }
  for(iRow=0;iRow<iTotalNumRows;++iRow)
    {
    SET_ROW_POINTER_FOR_FILTER;
    ucpDst+=((iTotalNumCols-iColBorder)*6);
    for(iCol=iTotalNumCols-iColBorder;iCol<iTotalNumCols;++iCol)
      {
      FILTER_A_BORDER_PIXEL_444;
    }
  }

  /* Filter the interior. */
  for(iRow=iRowBorder;iRow<iTotalNumRows-iRowBorder;++iRow)
    {
    SET_ROW_POINTER_FOR_FILTER;
    ucpDst+=iColBytesBorder;

    /* Interior will be treated as uniform only for speed. */

    /* First two points are fully computed. */
    for(iCol=iColBorder;iCol<iColBorder+2;++iCol)
      {
      iC0Y=iCol-iHalfFilterCols;
      iC1Y=iCol+iHalfFilterCols;
      ucpSrcColReady0Y=ucpSrc0+(iC0Y*6);
      ucpSrcColReady1Y=ucpSrc1Corrected+(iC0Y*6);
      iWeightY=0;
      iWeightU=0;
      iWeightV=0;
      for(iC=iC0Y;iC<=iC1Y;++iC)
        {
        ipFilterFistColSumY[iC]=0;
        ipFilterFistColSumU[iC]=0;
        ipFilterFistColSumV[iC]=0;
      }
      for(iR=iRow-iRowBorder;iR<=iRow+iRowBorder;++iR)
        {
        if(iInterlacing && (iR & 1))
          {
          ucpSrcY=ucpSrcColReady1Y;
        }
        else
          {
          ucpSrcY=ucpSrcColReady0Y;
        }
        ucpSrcY+=(iR*iNumBytesPerRow);
        for(iC=iC0Y;iC<=iC1Y;++iC)
          {
          iWeightY+=(*((unsigned short *)ucpSrcY));
          ipFilterFistColSumY[iC]+=(*((unsigned short *)ucpSrcY));
          ucpSrcY += 2;
          iWeightU+=(*((unsigned short *)ucpSrcY));
          ipFilterFistColSumU[iC]+=(*((unsigned short *)ucpSrcY));
          ucpSrcY += 2;
          iWeightV+=(*((unsigned short *)ucpSrcY));
          ipFilterFistColSumV[iC]+=(*((unsigned short *)ucpSrcY));
          ucpSrcY += 2;
        } /* iC */
      } /* iR */
      *((unsigned short *)ucpDst)=(unsigned short)(iWeightY/iTotalWeight);
      ucpDst += 2;
      *((unsigned short *)ucpDst)=(unsigned short)(iWeightU/iTotalWeight);
      ucpDst += 2;
      *((unsigned short *)ucpDst)=(unsigned short)(iWeightV/iTotalWeight);
      ucpDst += 2;
    } 

    /* After two points, we subtract each first-col sum and 
       add last-col sum to get the total in general. 3x3 case
       is treated differently because direct computing is faster. */
    if(iNumLowPassFilterRows==3 && iNumLowPassFilterCols==3)
      {
      /* Unwrap 3x3 case. */
      if(iInterlacing && (iRow & 1))
	{
        ucpSrcY0=ucpSrc0+(iRow-1)*iNumBytesPerRow
          +((iColBorder+2)*6);
        ucpSrcY1=ucpSrc1Corrected+(iRow)*iNumBytesPerRow
          +((iColBorder+2)*6);
        ucpSrcY2=ucpSrc0+(iRow+1)*iNumBytesPerRow
          +((iColBorder+2)*6);
        for(iCol=iColBorder+2;iCol<iTotalNumCols-iColBorder;++iCol)
          {
          DO_3X3_FILTER_INTERIOR_444;
          DO_3X3_FILTER_INTERIOR_444;
          DO_3X3_FILTER_INTERIOR_444;
        }
      }
      else if(iInterlacing)
	{
        ucpSrcY0=ucpSrc1Corrected+(iRow-1)*iNumBytesPerRow
          +((iColBorder+2)*6);
        ucpSrcY1=ucpSrc0+(iRow)*iNumBytesPerRow
          +((iColBorder+2)*6);
        ucpSrcY2=ucpSrc1Corrected+(iRow+1)*iNumBytesPerRow
          +((iColBorder+2)*6);
        for(iCol=iColBorder+2;iCol<iTotalNumCols-iColBorder;++iCol)
          {
          DO_3X3_FILTER_INTERIOR_444;
          DO_3X3_FILTER_INTERIOR_444;
          DO_3X3_FILTER_INTERIOR_444;
        }
      }
      else
	{
        ucpSrcY0=ucpSrc0+(iRow-1)*iNumBytesPerRow
          +((iColBorder+2)*6);
        ucpSrcY1=ucpSrc0+(iRow)*iNumBytesPerRow
          +((iColBorder+2)*6);
        ucpSrcY2=ucpSrc0+(iRow+1)*iNumBytesPerRow
          +((iColBorder+2)*6);
        for(iCol=iColBorder+2;iCol<iTotalNumCols-iColBorder;++iCol)
          {
          DO_3X3_FILTER_INTERIOR_444;
          DO_3X3_FILTER_INTERIOR_444;
          DO_3X3_FILTER_INTERIOR_444;
        }
      }
    }
    else
      {
      /* Any size case */
      iC0Y=iColBorder-iHalfFilterCols;
      iC1Y=iColBorder+1+iHalfFilterCols;
      ucpSrcColReady0Y=ucpSrc0+(iC1Y<<2)+2;
      ucpSrcColReady1Y=ucpSrc1Corrected+(iC1Y<<2)+2;
      for(iCol=iColBorder+2;iCol<iTotalNumCols-iColBorder;++iCol)
        {
        ++iC0Y; /* The column just leaves the current block. */
        ++iC1Y; /* The column just joins the current block. */
        ucpSrcColReady0Y+=6;
        ucpSrcColReady1Y+=6;

        /* Compute the last contribute col sum. */
        iFilterFistColSumY=0;
        iFilterFistColSumU=0;
        iFilterFistColSumV=0;
        for(iR=iRow-iRowBorder;iR<=iRow+iRowBorder;++iR)
          {
          if((iR & 1)==0 || iInterlacing==0)
            {
            ucpSrcY=ucpSrcColReady0Y;
          }
          else
            {
            ucpSrcY=ucpSrcColReady1Y;
          }
          ucpSrcY+=(iR*iNumBytesPerRow);
          iFilterFistColSumY += (*((unsigned short *)(ucpSrcY)));
          iFilterFistColSumU += (*((unsigned short *)(ucpSrcY+2)));
          iFilterFistColSumV += (*((unsigned short *)(ucpSrcY+4)));
        }

        /* Remember it for the next time. */
        ipFilterFistColSumY[iC1Y]=iFilterFistColSumY;
        ipFilterFistColSumU[iC1Y]=iFilterFistColSumU;
        ipFilterFistColSumV[iC1Y]=iFilterFistColSumV;
	/* Re-compute the weights. */
        iWeightY=iWeightY-ipFilterFistColSumY[iC0Y]+iFilterFistColSumY;
        iWeightU=iWeightU-ipFilterFistColSumU[iC0Y]+iFilterFistColSumU;
        iWeightV=iWeightV-ipFilterFistColSumV[iC0Y]+iFilterFistColSumV;

	/* Get destination values. */
        *((unsigned short *)(ucpDst))
          =(unsigned short)(iWeightY/iTotalWeight);
        ucpDst+=2;
        *((unsigned short *)(ucpDst))
          =(unsigned short)(iWeightU/iTotalWeight);
        ucpDst+=2;
        *((unsigned short *)(ucpDst))
          =(unsigned short)(iWeightV/iTotalWeight);
        ucpDst+=2;
      } /* iCol */
    } /* Special size cases. */
  } /* iRow */
  free(ipFilterFistColSumY);
  free(ipFilterFistColSumU);
  free(ipFilterFistColSumV);
}

/** Fill the black for a frame. 
    Support YUV422(16-bit), YUV444(16-bit), RGB10. */
void MAGNIFY_CONTROL::BlackOutThisFrame(unsigned char *ucpCurrent0,
  unsigned char *ucpCurrent1,int iNFrameRows,int iNPixelCols,
  int iInterlacing,int iPixelValsSrc,int iPixelValsDes, int iBlackVal0,
  int iBlackVal1, int iBlackVal2)
{
  int iRow,iCol;
  unsigned char *ucpYUV;
  int iNumBytesPerPixel;

  if(iPixelValsSrc != iPixelValsDes)
    TRACE_0(errout << "Unable to open the par file");
  if(iPixelValsSrc==PIXELVALS_YUV_444)
    iNumBytesPerPixel = 6;
  else if(iPixelValsSrc==PIXELVALS_YUV_422)
    iNumBytesPerPixel = 4;
  else
    TRACE_0(errout << "Unable to open the par file");

  for(iRow=0;iRow<iNFrameRows;++iRow)
    {
    if(iInterlacing && (iRow & 1)==1)
      ucpYUV=ucpCurrent1+(iRow>>iInterlacing)*iNPixelCols*iNumBytesPerPixel;
    else
      ucpYUV=ucpCurrent0+(iRow>>iInterlacing)*iNPixelCols*iNumBytesPerPixel;
    if(iPixelValsSrc==PIXELVALS_YUV_422)
      {
      for(iCol=0;iCol<iNPixelCols;++iCol)
        {
        *((unsigned short *)ucpYUV) =iBlackVal1;	// U,V
        ucpYUV+=2;
        *((unsigned short *)ucpYUV) =iBlackVal0;
        ucpYUV+=2;
      }
    }
    else if(iPixelValsSrc==PIXELVALS_YUV_444)
      {
      for(iCol=0;iCol<iNPixelCols;++iCol)
        {
        *((unsigned short *)ucpYUV) =iBlackVal0;
        ucpYUV+=2;
        *((unsigned short *)ucpYUV) =iBlackVal1;
        ucpYUV+=2;
        *((unsigned short *)ucpYUV) =iBlackVal2;
        ucpYUV+=2;
      }
    }
    else
      TRACE_0(errout << "Unable to open the par file");
  }
}

/** */
void MAGNIFY_CONTROL::ComputeInvertMatrix(int iBaseX,int iNumDataTerms,int iNumPowerTerms,
  float *dResultMatrix,float *dResultMatrixTranspose)
{
  int i,j,k;
  float dFlag;

  float *daX,*daXt,*daXtAgain,*daXX,*daXXi,**pdaX,**pdaXt,**pdaXtAgain,
         **pdaXX,**pdaXXi,**pfaInverseMatrix,*daCol;
  int *iaIndex;

#ifdef _DEBUG_ALLOC
printf ("%s %d allocating %d\n", __FILE__, __LINE__, iNumDataTerms*(iNumPowerTerms+1)*sizeof(float)*5);
#endif
  daX=(float *)malloc(iNumDataTerms*(iNumPowerTerms+1)*sizeof(float));
  daXt=(float *)malloc((iNumPowerTerms+1)*iNumDataTerms*sizeof(float));
  daXtAgain=(float *)malloc((iNumPowerTerms+1)*iNumDataTerms*sizeof(float));
  daXX=(float *)malloc((iNumPowerTerms+1)*(iNumPowerTerms+1)*sizeof(float));
  daXXi=(float *)malloc((iNumPowerTerms+1)*(iNumPowerTerms+1)*sizeof(float));

#ifdef _DEBUG_ALLOC
printf ("%s %d allocating %d\n", __FILE__, __LINE__, (iNumPowerTerms+1)*sizeof(float)*8);
#endif
  daCol=(float *)malloc((iNumPowerTerms+1)*sizeof(float));
  pdaX=(float **)malloc(iNumDataTerms*sizeof(float *));
  pdaXt=(float **)malloc((iNumPowerTerms+1)*sizeof(float *));
  pdaXtAgain=(float **)malloc((iNumPowerTerms+1)*sizeof(float *));
  pdaXX=(float **)malloc((iNumPowerTerms+1)*sizeof(float *));
  pdaXXi=(float **)malloc((iNumPowerTerms+1)*sizeof(float *));
  pfaInverseMatrix=(float **)malloc((iNumPowerTerms+1)*sizeof(float *));
  iaIndex=(int *)malloc((iNumPowerTerms+1)*sizeof(int *));

  if(daX==NULL || daXt==NULL || daXtAgain==NULL || daXX==NULL ||
     daXXi==NULL || daCol==NULL || pdaX==NULL || pdaXt==NULL ||
     pdaXtAgain==NULL || pdaXX==NULL || pdaXXi==NULL || pfaInverseMatrix
     ==NULL || iaIndex==NULL)
    {
    printf("Failed to allocate memory in ComputeInvertMatrix().\n");
    TRACE_0(errout << "Unable to open the par file");
  }

  /* Compute (X'X)^{-1} X' where
     The content of X depends on the linear model used.
     For one-dimension case:

     X= [1  (-n+1) (-n+1)^{2} (-n+1)^{3} ...... (-n+1)^{M}]
        [1  (-n+2) (-n+2)^{2} (-n+2)^{3} ...... (-n+2)^{M}]
        ....
        [1     0        0          0                 0    ]
        [1     1        1          1                 1    ]
        [1     2        4          8                 2^{M}]
        ....
        [1    (n)    (n)^{2}     (n)^{3} ......    (n)^{M}]
     where n=N/2=iNumDataTerms/2
           M=iNumPowerTerms
     so that X is an N by (M+1) matrix. */
  /* Form X */
  switch(iBaseX)
    {
    case BASE_MATRIX_2D_4X4_PLANE: /* a+bX+cY is the function.*/
      for(i=0;i<iNumDataTerms;++i)
        {
        pdaX[i]=&(daX[i*(iNumPowerTerms+1)+0]);
        daX[i*(iNumPowerTerms+1)+0]=1.0;
      }
      daX[0*(iNumPowerTerms+1)+1]
       =daX[1*(iNumPowerTerms+1)+1]
       =daX[2*(iNumPowerTerms+1)+1]
       =daX[3*(iNumPowerTerms+1)+1]=-1.0;
      daX[4*(iNumPowerTerms+1)+1]
       =daX[5*(iNumPowerTerms+1)+1]
       =daX[6*(iNumPowerTerms+1)+1]
       =daX[7*(iNumPowerTerms+1)+1]=0.0;
      daX[8*(iNumPowerTerms+1)+1]
       =daX[9*(iNumPowerTerms+1)+1]
       =daX[10*(iNumPowerTerms+1)+1]
       =daX[11*(iNumPowerTerms+1)+1]=1.0;
      daX[12*(iNumPowerTerms+1)+1]
       =daX[13*(iNumPowerTerms+1)+1]
       =daX[14*(iNumPowerTerms+1)+1]
       =daX[15*(iNumPowerTerms+1)+1]=2.0;
      daX[0*(iNumPowerTerms+1)+2]
       =daX[4*(iNumPowerTerms+1)+2]
       =daX[8*(iNumPowerTerms+1)+2]
       =daX[12*(iNumPowerTerms+1)+2]=-1.0;
      daX[1*(iNumPowerTerms+1)+2]
       =daX[5*(iNumPowerTerms+1)+2]
       =daX[9*(iNumPowerTerms+1)+2]
       =daX[13*(iNumPowerTerms+1)+2]=0.0;
      daX[2*(iNumPowerTerms+1)+2]
       =daX[6*(iNumPowerTerms+1)+2]
       =daX[10*(iNumPowerTerms+1)+2]
       =daX[14*(iNumPowerTerms+1)+2]=1.0;
      daX[3*(iNumPowerTerms+1)+2]
       =daX[7*(iNumPowerTerms+1)+2]
       =daX[11*(iNumPowerTerms+1)+2]
       =daX[15*(iNumPowerTerms+1)+2]=2.0;
      break;
    case BASE_MATRIX_2D_4X4_FULL_PLANE: /* a+
                                      bX+cXX+dXXX+
                                      eY+fYY+gYYY+
                                      hXY+iXXY+jXYY+kXXYY is the function.*/
      for(i=0;i<iNumDataTerms;++i)
        {
        pdaX[i]=&(daX[i*(iNumPowerTerms+1)+0]);
        daX[i*(iNumPowerTerms+1)+0]=1.0;
        j=1;
        daX[i*(iNumPowerTerms+1)+j]=i/4-1; ++j;
        daX[i*(iNumPowerTerms+1)+j]=(i/4-1)*(i/4-1); ++j;
        if(iNumPowerTerms>=10)
	  {
          daX[i*(iNumPowerTerms+1)+j]=(i/4-1)*(i/4-1)*(i/4-1); ++j;
	}
        daX[i*(iNumPowerTerms+1)+j]=i%4-1; ++j;
        daX[i*(iNumPowerTerms+1)+j]=(i%4-1)*(i%4-1); ++j;
        if(iNumPowerTerms>=10)
	  {
          daX[i*(iNumPowerTerms+1)+j]=(i%4-1)*(i%4-1)*(i%4-1); ++j;
	}
        daX[i*(iNumPowerTerms+1)+j]=(i/4-1)*(i%4-1); ++j;
        if(iNumPowerTerms>=10)
	  {
          daX[i*(iNumPowerTerms+1)+j]=(i/4-1)*(i/4-1)*(i%4-1); ++j;
          daX[i*(iNumPowerTerms+1)+j]=(i/4-1)*(i%4-1)*(i%4-1); ++j;
          daX[i*(iNumPowerTerms+1)+j]=(i/4-1)*(i/4-1)*(i%4-1)*(i%4-1); ++j;
	}
        if(iNumPowerTerms>=12)
	  {
          daX[i*(iNumPowerTerms+1)+j]=(i/4-1)*(i/4-1)*(i/4-1)*(i%4-1); ++j;
          daX[i*(iNumPowerTerms+1)+j]=(i/4-1)*(i%4-1)*(i%4-1)*(i%4-1); ++j;
	}
        if(iNumPowerTerms>=14)
	  {
          daX[i*(iNumPowerTerms+1)+j]=(i/4-1)*(i/4-1)*(i/4-1)*(i%4-1)*(i%4-1);
          ++j;
          daX[i*(iNumPowerTerms+1)+j]=(i/4-1)*(i/4-1)*(i%4-1)*(i%4-1)*(i%4-1);
          ++j;
	}
        if(iNumPowerTerms>=15)
	  {
          daX[i*(iNumPowerTerms+1)+j]=
           (i/4-1)*(i/4-1)*(i/4-1)*(i%4-1)*(i%4-1)*(i%4-1);
          ++j;
	}
      }
      break;
    case BASE_MATRIX_1D_SPLINE:
      for(i=0;i<iNumDataTerms;++i)
        {
        pdaX[i]=&(daX[i*(iNumPowerTerms+1)+0]);
        daX[i*(iNumPowerTerms+1)+0]=1.0;
        for(j=1;j<iNumPowerTerms+1;++j)
          {
          daX[i*(iNumPowerTerms+1)+j]=1.0;
          for(k=1;k<=j;++k)
            {
            daX[i*(iNumPowerTerms+1)+j]
              *=(i+1-iNumDataTerms/2);
          }
        }  
      }
      break;
    defaults:
      TRACE_0(errout << "Unable to open the par file");
  }

  /* Get X' */
  for(j=0;j<iNumPowerTerms+1;++j)
    {
    pdaXt[j]=&(daXt[j*iNumDataTerms+0]);
    pdaXtAgain[j]=&(daXtAgain[j*iNumDataTerms
     +0]);
  }

  mat_trans(pdaXt,pdaX,iNumDataTerms,
    iNumPowerTerms+1);

  for(i=0;i<iNumPowerTerms+1;++i)
    {
    for(j=0;j<iNumDataTerms;++j)
      {
      daXtAgain[i*iNumDataTerms+j]
       =pdaXt[i][j];
    }  
  }

  /* Get X'X */
  for(j=0;j<iNumPowerTerms+1;++j)
    pdaXX[j]=&(daXX[j*(iNumPowerTerms+1)+0]);
  mat_mult(pdaXX,pdaXt,pdaX,iNumPowerTerms+1,
   iNumDataTerms,
   iNumPowerTerms+1);

  /* Get (X'X)^{-1} */
  ludcmp(pdaXX,
    iNumPowerTerms+1,iaIndex,&dFlag);
  for(i=0;i<iNumPowerTerms+1;++i)
    {
    for(j=0;j<iNumPowerTerms+1;++j)
      {
      daCol[j]=0.0;
    }
    daCol[i]=1.0;
    lubksb(pdaXX,iNumPowerTerms+1,
      iaIndex,daCol);
    for(j=0;j<iNumPowerTerms+1;++j)
      {
      daXXi[i*(iNumPowerTerms+1)+j]=daCol[j];
    }
  }

  /* Get (X'X)^{-1}X' */
  for(i=0;i<iNumPowerTerms+1;++i)
    {
    pfaInverseMatrix[i]=(&(dResultMatrix[i*(iNumDataTerms)+0]));
    pdaXXi[i]=(&(daXXi[i*(iNumPowerTerms+1)+0]));
  }
  mat_mult(pfaInverseMatrix,pdaXXi,pdaXtAgain,
   iNumPowerTerms+1,
   iNumPowerTerms+1,
   iNumDataTerms);

  /* Get transpose of (X'X)^{-1}X' */
  for(i=0;i<iNumDataTerms;++i)
    pdaX[i]=(&(dResultMatrixTranspose[i*(iNumPowerTerms+1)+0]));
  for(i=0;i<iNumPowerTerms+1;++i)
    pfaInverseMatrix[i]=(&(dResultMatrix[i*(iNumDataTerms)+0]));
  mat_trans(pdaX,pfaInverseMatrix,iNumPowerTerms+1,iNumDataTerms);

  free(daX);
  free(daXt);
  free(daXtAgain);
  free(daXX);
  free(daXXi);
  free(daCol);
  free(pdaX);
  free(pdaXt);
  free(pdaXtAgain);
  free(pdaXX);
  free(pdaXXi);
  free(pfaInverseMatrix);
  free(iaIndex);
}

void MAGNIFY_CONTROL::MakeWeightFunctionTable()
{
  int iCnt;
  float dD;

  /* f(x)= (1/2)(2x)**power              if 0<x<1/2
           (1/2)(1+(2x-1)**(1/power))    if 1/2<x<1 */
  for(iCnt=0;iCnt<1024;++iCnt)
    {
    dD=((float)iCnt)*2.0/1023.0;
    if(dD<1.0)
      fWeighTable[iCnt]=pow((double)dD,(double)WEIGHT_FUNCTION_TYPE_1_POWER)/2.0;
    else
      fWeighTable[iCnt]=(pow((double)(dD-1.0),(double)(1.0/WEIGHT_FUNCTION_TYPE_1_POWER))+1.0)/2.0;
  }

  CODE_TRACE("MakeWeightFunctionTable() done");
}

/** */
float MAGNIFY_CONTROL::WeightFunction(float dFraction)
{
  long lTableIndex;

  if(iTableIsMade==FALSE)
    {
    MakeWeightFunctionTable();
    iTableIsMade=TRUE;
  }

  lTableIndex=(long)(dFraction*1023.0);
  if(lTableIndex<0 || lTableIndex>1023)
   {
    fprintf (stderr, "The value of dFraction is %f\n", dFraction);
    TRACE_0(errout << "Unable to open the par file");
   }

  return fWeighTable[lTableIndex];
}

/** The function assumes a very specific 2-D function.
    o              o              o              o

    o              o(ucpSrc00)    o              o
                       x(ucpDes)
    o              o(ucpSrc10)    o              o

    o              o              o              o
*/
void MAGNIFY_CONTROL::TwoDimension4x4Spline(
  float *ufDes, /* Destination byte location. */
  int iDataInc,          /* How much jump in bytes between the data of the 
                            same row. */
  float fRowFraction, /* Row location of destination relative to Src00 */
  float fColFraction, /* Col location of destination relative to Src00 */
  float *dpCoefficient /* Where to store fitted surface ? */
  )
  {
  float fTemp1;

  /* Linear combination to get the result. */
  int iMagnifyExperimentGenericSplineNumPower =
                MAGNIFY_EXPERIMENT_GENERIC_SPLINE_NUM_POWER;
  if(iMagnifyExperimentGenericSplineNumPower==15)
    {
    fTemp1=dpCoefficient[0]
    +dpCoefficient[1]*fRowFraction
    +dpCoefficient[2]*fRowFraction*fRowFraction
    +dpCoefficient[3]*fRowFraction*fRowFraction*fRowFraction
    +dpCoefficient[4]*fColFraction
    +dpCoefficient[5]*fColFraction*fColFraction
    +dpCoefficient[6]*fColFraction*fColFraction*fColFraction
    +dpCoefficient[7]*fRowFraction*fColFraction
    +dpCoefficient[8]*fRowFraction*fRowFraction*fColFraction
    +dpCoefficient[9]*fRowFraction*fColFraction*fColFraction
    +dpCoefficient[10]*fRowFraction*fRowFraction*fColFraction*fColFraction
    +dpCoefficient[11]*fRowFraction*fRowFraction*fRowFraction*fColFraction
    +dpCoefficient[12]*fRowFraction*fColFraction*fColFraction*fColFraction
    +dpCoefficient[13]*fRowFraction*fRowFraction*fRowFraction*
      fColFraction*fColFraction
    +dpCoefficient[14]*fRowFraction*fRowFraction
      *fColFraction*fColFraction*fColFraction
    +dpCoefficient[15]*fRowFraction*fRowFraction*fRowFraction
      *fColFraction*fColFraction*fColFraction;
  }
  else if(iMagnifyExperimentGenericSplineNumPower==14)
    {
    fTemp1=dpCoefficient[0]
    +dpCoefficient[1]*fRowFraction
    +dpCoefficient[2]*fRowFraction*fRowFraction
    +dpCoefficient[3]*fRowFraction*fRowFraction*fRowFraction
    +dpCoefficient[4]*fColFraction
    +dpCoefficient[5]*fColFraction*fColFraction
    +dpCoefficient[6]*fColFraction*fColFraction*fColFraction
    +dpCoefficient[7]*fRowFraction*fColFraction
    +dpCoefficient[8]*fRowFraction*fRowFraction*fColFraction
    +dpCoefficient[9]*fRowFraction*fColFraction*fColFraction
    +dpCoefficient[10]*fRowFraction*fRowFraction*fColFraction*fColFraction
    +dpCoefficient[11]*fRowFraction*fRowFraction*fRowFraction*fColFraction
    +dpCoefficient[12]*fRowFraction*fColFraction*fColFraction*fColFraction
    +dpCoefficient[13]*fRowFraction*fRowFraction*fRowFraction*
      fColFraction*fColFraction
    +dpCoefficient[14]*fRowFraction*fRowFraction
      *fColFraction*fColFraction*fColFraction;
  }
  else if(iMagnifyExperimentGenericSplineNumPower==12)
    {
    fTemp1=dpCoefficient[0]
    +dpCoefficient[1]*fRowFraction
    +dpCoefficient[2]*fRowFraction*fRowFraction
    +dpCoefficient[3]*fRowFraction*fRowFraction*fRowFraction
    +dpCoefficient[4]*fColFraction
    +dpCoefficient[5]*fColFraction*fColFraction
    +dpCoefficient[6]*fColFraction*fColFraction*fColFraction
    +dpCoefficient[7]*fRowFraction*fColFraction
    +dpCoefficient[8]*fRowFraction*fRowFraction*fColFraction
    +dpCoefficient[9]*fRowFraction*fColFraction*fColFraction
    +dpCoefficient[10]*fRowFraction*fRowFraction*fColFraction*fColFraction
    +dpCoefficient[11]*fRowFraction*fRowFraction*fRowFraction*fColFraction
    +dpCoefficient[12]*fRowFraction*fColFraction*fColFraction*fColFraction;
  }
  else if(iMagnifyExperimentGenericSplineNumPower==10)
    {
    fTemp1=dpCoefficient[0]
    +dpCoefficient[1]*fRowFraction
    +dpCoefficient[2]*fRowFraction*fRowFraction
    +dpCoefficient[3]*fRowFraction*fRowFraction*fRowFraction
    +dpCoefficient[4]*fColFraction
    +dpCoefficient[5]*fColFraction*fColFraction
    +dpCoefficient[6]*fColFraction*fColFraction*fColFraction
    +dpCoefficient[7]*fRowFraction*fColFraction
    +dpCoefficient[8]*fRowFraction*fRowFraction*fColFraction
    +dpCoefficient[9]*fRowFraction*fColFraction*fColFraction
    +dpCoefficient[10]*fRowFraction*fRowFraction*fColFraction*fColFraction;
  }
  else if(iMagnifyExperimentGenericSplineNumPower==5)
    {
    fTemp1=dpCoefficient[0]
    +dpCoefficient[1]*fRowFraction
    +dpCoefficient[2]*fRowFraction*fRowFraction
    +dpCoefficient[3]*fColFraction
    +dpCoefficient[4]*fColFraction*fColFraction
    +dpCoefficient[5]*fRowFraction*fColFraction;
  }
  *ufDes=fTemp1;
}

/** */
float MAGNIFY_CONTROL::OverLapWeightFunction(float dFraction)
{
  if(dFraction>0.5)
    return 0;
  else
    return 0.5-WeightFunction(dFraction);
}

/** */
void MAGNIFY_CONTROL::Generic2dSplineForUYorVY(
  float *fpTableForUV,float *fpTableForY,
  float fRowFraction,   /* In [0,1), exact row source location for
                            the destination.*/
  float fColFractionUV, /* In [0,2), exact column source location 
                            relative to the location of the pixel where
                            ucpUV00 is from, for the destination pixel. */
  float fColFractionY,  /* In [0,1), exact column source location
                            relative to the location of the pixel where
                            ucpY00 is from, for the destination pixel. */
  int iNeighborType      /* What kind of neighbor. */
  )
  {
  int iCnt;
  int iNumGetResults;
  float fUp,fDown,fLeft,fRight,fMe,fTemp;
  float dWeightLeft,dWeightRight,dWeightUp,dWeightDown,dWeightMe;
  float *fpGlobalCoefficientYUsed,*fpGlobalCoefficientUVUsed;

  if(iNeighborType==NEIGHBOR_TYPE_2x2)
    {
    *fpTableForUV=(1.0-fRowFraction)*(1.0-fColFractionUV);
    *(fpTableForUV+1)=(1.0-fRowFraction)*fColFractionUV;
    *(fpTableForUV+2)=fRowFraction*(1.0-fColFractionUV);
    *(fpTableForUV+3)=fRowFraction*fColFractionUV;
    *fpTableForY=(1.0-fRowFraction)*(1.0-fColFractionY);
    *(fpTableForY+1)=(1.0-fRowFraction)*fColFractionY;
    *(fpTableForY+2)=fRowFraction*(1.0-fColFractionY);
    *(fpTableForY+3)=fRowFraction*fColFractionY;
    return;
  }

  iNumGetResults=16;

  /* Get for UV */
  for(iCnt=0;iCnt<iNumGetResults;++iCnt)
    {
    fpGlobalCoefficientUVUsed=&(faInverseMatrixTranspose[iCnt][0]);

    TwoDimension4x4Spline(&fTemp,4,
      fRowFraction,fColFractionUV,fpGlobalCoefficientUVUsed);
    *(fpTableForUV+iCnt)=fTemp;
  }

  /* Get for Y */
  for(iCnt=0;iCnt<32;++iCnt)
    *(fpTableForY+iCnt)=0.0;


  for(iCnt=0;iCnt<iNumGetResults;++iCnt)
    {
      /* Get each column of (X'X)^{-1} X' */
      fpGlobalCoefficientYUsed=&(faInverseMatrixTranspose[iCnt][0]);

    TwoDimension4x4Spline(&fUp,2,
      1.0+fRowFraction,fColFractionY,fpGlobalCoefficientYUsed);

    TwoDimension4x4Spline(&fDown,2,
      fRowFraction-1.0,fColFractionY,fpGlobalCoefficientYUsed);

    TwoDimension4x4Spline(&fLeft,2,
      fRowFraction,fColFractionY+1.0,fpGlobalCoefficientYUsed);

    TwoDimension4x4Spline(&fRight,2,
      fRowFraction,fColFractionY-1.0,fpGlobalCoefficientYUsed);

    TwoDimension4x4Spline(&fMe,2,
      fRowFraction,fColFractionY,fpGlobalCoefficientYUsed);

    /* Average among five functions. */
    dWeightLeft=OverLapWeightFunction(fColFractionY);
    dWeightRight=OverLapWeightFunction(1.0-fColFractionY);
    dWeightUp=OverLapWeightFunction(fRowFraction);
    dWeightDown=OverLapWeightFunction(1.0-fRowFraction);
    dWeightMe=1.5-dWeightDown-dWeightUp-dWeightRight-dWeightLeft;

    /* The computed coefficients must be put in the correct location:
              x  0  1  2  3  x
              4  5  6  7  8  9
             10 11 12 13 14 15
             16 17 18 19 20 21
             22 23 24 25 25 27
              x 28 29 30 31  x */
    if(iNeighborType==NEIGHBOR_TYPE_4X4_OVELAP_FOR_Y)
      {
      *(fpTableForY+iUpLoc[iCnt]) += dWeightUp*fUp/1.5;
      *(fpTableForY+iDownLoc[iCnt]) += dWeightDown*fDown/1.5;
      *(fpTableForY+iLeftLoc[iCnt]) += dWeightLeft*fLeft/1.5;
      *(fpTableForY+iRightLoc[iCnt]) += dWeightRight*fRight/1.5;
      *(fpTableForY+iMeLoc[iCnt]) += dWeightMe*fMe/1.5;
    }
    else if(iNeighborType==NEIGHBOR_TYPE_4X4)
      {
      *(fpTableForY+iMeLoc[iCnt]) += (fMe);
    }
    else
      TRACE_0(errout << "Unable to open the par file");
  } /* iCnt */
}

/** */
void MAGNIFY_CONTROL::Free()
{
  if (this->fpTotalPreCal)
   {
    free(this->fpTotalPreCal);
    this->fpTotalPreCal=NULL;
   }
}

/** */
void MAGNIFY_CONTROL::Quit()
{
  Free ();

  if (this->ucpTemp0)
   {
    free(this->ucpTemp0);
    this->ucpTemp0=NULL;
   }

  if (this->ucpTemp1)
   {
    free(this->ucpTemp1);
    this->ucpTemp1=NULL;
   }
  if (this->ucpSrcMask)
   {
    free(this->ucpSrcMask);
    this->ucpSrcMask=NULL;
   }
}


/** */
int MAGNIFY_CONTROL::Initialize()
{
  int iRet=TRUE;

  Free();

  ComputeInvertMatrix(BASE_MATRIX_2D_4X4_FULL_PLANE,
    MAGNIFY_EXPERIMENT_GENERIC_SPLINE_NUM_DATA,
    MAGNIFY_EXPERIMENT_GENERIC_SPLINE_NUM_POWER,&(faInverseMatrix[0][0]),
    &(faInverseMatrixTranspose[0][0]));

  iInverseMatrixComputed=TRUE;
  CODE_TRACE("ComputeInvertMatrix() done\n");

  if(this->fpTotalPreCal!=NULL)
    TRACE_0(errout << "Unable to open the par file");

#ifdef _DEBUG_ALLOC
printf ("%s %d allocating %d\n", __FILE__, __LINE__, this->iMinRowsInPreCalTable*
    this->iMinColsInPreCalTable*PRE_TABLE_ENTRY_SIZE_IN_FLOATS*sizeof(float));
printf ("MinRows:  %d  MinCols:  %d  ENTRY_SIZE: %d\n", 
	this->iMinRowsInPreCalTable, this->iMinColsInPreCalTable,
		PRE_TABLE_ENTRY_SIZE_IN_FLOATS);
#endif
  this->fpTotalPreCal=(float *)malloc(this->iMinRowsInPreCalTable*
    this->iMinColsInPreCalTable*PRE_TABLE_ENTRY_SIZE_IN_FLOATS*sizeof(float));

  if(this->fpTotalPreCal==NULL)
   {
    TRACE_0(errout << "Unable to allocate memory");
   }

  this->iRoundOneNeeded=TRUE;
  this->iParFileExists=FALSE;

  return iRet;
}

#define SET_PIXEL_POINTERS \
  ucpUYorVYSrc00=ucpLineSrcCut+ipSrcByteColCut[iPixel]; \
  ucpUYorVYSrc10=ucpLineSrcAdd+ipSrcByteColCut[iPixel]; \
  ucpUV00=(ipUvMatch[iPixel]==1 ? ucpUYorVYSrc00 : (ucpUYorVYSrc00 - 4)); \
  ucpUV10=(ipUvMatch[iPixel]==1 ? ucpUYorVYSrc10 : (ucpUYorVYSrc10 - 4))

#define SET_PIXELWISE_LOCATION_VARIBLES \
  iDstByte=iPixel*iNumBytesPerPixelDst; \
  dTemp=((double)(iPixel-iStartPixelColDst))/(double)fColFactor \
    +(double)fStartPixelColSrc; \
  fSrcPixelLocation=(float)(dTemp+DOUBLE_TO_FLOAT_ROUNDING_OFFSET); \
  iSrcPixelColCut=((int)fSrcPixelLocation); \
  iSrcByteColCut=iSrcPixelColCut*iNumBytesPerPixelSrc; \
  /* iSrcByteColCut is not guaranteed to be at UV's position. */ \
  ipSrcByteColCut[iPixel]=iSrcByteColCut; \
  ipUvMatch[iPixel]=((iDstByte & iUvMatchCheckingMaskDst) \
    ==(iSrcByteColCut & iUvMatchCheckingMaskSrc)); \
  SET_PIXEL_POINTERS

#define SET_SIMPLE_VALUES_UV \
  fTemp=(*fpPreCalPointerUV++)*((float)(*((unsigned short *)(ucpUV00)))); \
  fTemp+=(*fpPreCalPointerUV++)*((float)(*((unsigned short *)(ucpUV00+8)))); \
  fTemp+=(*fpPreCalPointerUV++)*((float)(*((unsigned short *)(ucpUV10)))); \
  fTemp+=(*fpPreCalPointerUV++)*((float)(*((unsigned short *)(ucpUV10+8)))); \
  *((unsigned short *)ucpUYorVYDst)=(unsigned short)(fTemp+0.5)

#define SET_SIMPLE_VALUES_Y \
  fTemp=(*fpPreCalPointerY++)* \
     ((float)(*((unsigned short *)(ucpUYorVYSrc00+2)))); \
  fTemp+=(*fpPreCalPointerY++)* \
     ((float)(*((unsigned short *)(ucpUYorVYSrc00+6)))); \
  fTemp+=(*fpPreCalPointerY++)* \
     ((float)(*((unsigned short *)(ucpUYorVYSrc10+2)))); \
  fTemp+=(*fpPreCalPointerY++)* \
     ((float)(*((unsigned short *)(ucpUYorVYSrc10+6)))); \
  /* For testing, pink the simple Y block.  \
  *((unsigned short *)(ucpUYorVYDst))=150<<8; */ \
  *((unsigned short *)(ucpUYorVYDst+2))=(unsigned short)(fTemp+0.5);

#define SET_LARGER_NEIGHBORED_VALUES_UV \
  /* Form the UV data */ \
  fTemp=(*fpPreCalPointerUV++)* \
    ((float)(*((unsigned short *)(ucpUV10-iNByteColsSrc-8))));\
  fTemp+=(*fpPreCalPointerUV++)* \
    ((float)(*((unsigned short *) \
    (ucpUV10-iNByteColsSrc)))); \
  fTemp+=(*fpPreCalPointerUV++)* \
    ((float)(*((unsigned short *) \
    (ucpUV10-iNByteColsSrc+8)))); \
  fTemp+=(*fpPreCalPointerUV++)* \
    ((float)(*((unsigned short *) \
    (ucpUV10-iNByteColsSrc+16)))); \
  fTemp+=(*fpPreCalPointerUV++)* \
    ((float)(*((unsigned short *)(ucpUV00-8)))); \
  fTemp+=(*fpPreCalPointerUV++)* \
    ((float)(*((unsigned short *)(ucpUV00)))); \
  fTemp+=(*fpPreCalPointerUV++)* \
    ((float)(*((unsigned short *)(ucpUV00+8)))); \
  fTemp+=(*fpPreCalPointerUV++)* \
    ((float)(*((unsigned short *)(ucpUV00+16)))); \
  fTemp+=(*fpPreCalPointerUV++)* \
    ((float)(*((unsigned short *)(ucpUV10-8)))); \
  fTemp+=(*fpPreCalPointerUV++)* \
    ((float)(*((unsigned short *)(ucpUV10)))); \
  fTemp+=(*fpPreCalPointerUV++)* \
    ((float)(*((unsigned short *)(ucpUV10+8)))); \
  fTemp+=(*fpPreCalPointerUV++)* \
    ((float)(*((unsigned short *)(ucpUV10+16)))); \
  fTemp+=(*fpPreCalPointerUV++)* \
    ((float)(*((unsigned short *) \
    (ucpUV00+iNByteColsSrc-8)))); \
  fTemp+=(*fpPreCalPointerUV++)* \
    ((float)(*((unsigned short *) \
    (ucpUV00+iNByteColsSrc)))); \
  fTemp+=(*fpPreCalPointerUV++)* \
    ((float)(*((unsigned short *) \
    (ucpUV00+iNByteColsSrc+8)))); \
  fTemp+=(*fpPreCalPointerUV++)* \
    ((float)(*((unsigned short *) \
    (ucpUV00+iNByteColsSrc+16)))); \
  iTemp=(int)(fTemp+0.5); \
  if(iTemp & 0xFFFF0000) \
    { \
    if(iTemp<0) \
      iTemp=0; \
    else if(iTemp>65535) \
      iTemp=65535; \
  } \
  *((unsigned short *)ucpUYorVYDst)=(unsigned short)iTemp

#define SET_LARGER_NEIGHBORED_VALUES_Y \
  /* Form the Y data */ \
  /* Row -2, 4 pixels. */ \
  fTemp=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc00-iNByteColsSrc-2)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc00-iNByteColsSrc+2)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc00-iNByteColsSrc+6)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc00-iNByteColsSrc+10)))); \
  /* Row -1, 6 pixels. */ \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc10-iNByteColsSrc-6)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc10-iNByteColsSrc-2)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc10-iNByteColsSrc+2)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc10-iNByteColsSrc+6)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc10-iNByteColsSrc+10)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc10-iNByteColsSrc+14)))); \
  /* Row 0, 6 pixels. */ \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *)(ucpUYorVYSrc00-6)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *)(ucpUYorVYSrc00-2)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *)(ucpUYorVYSrc00+2)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *)(ucpUYorVYSrc00+6)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *)(ucpUYorVYSrc00+10)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *)(ucpUYorVYSrc00+14)))); \
  /* Row 1, 6 pixels. */ \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *)(ucpUYorVYSrc10-6)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *)(ucpUYorVYSrc10-2)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *)(ucpUYorVYSrc10+2)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *)(ucpUYorVYSrc10+6)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *)(ucpUYorVYSrc10+10)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *)(ucpUYorVYSrc10+14)))); \
  /* Row 2, 6 pixels. */ \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc00+iNByteColsSrc-6)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc00+iNByteColsSrc-2)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc00+iNByteColsSrc+2)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc00+iNByteColsSrc+6)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc00+iNByteColsSrc+10)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc00+iNByteColsSrc+14)))); \
  /* Row 3, 4 pixels. */ \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc10+iNByteColsSrc-2)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc10+iNByteColsSrc+2)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc10+iNByteColsSrc+6)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc10+iNByteColsSrc+10)))); \
  iTemp=(int)(fTemp+0.5); \
  if(iTemp & 0xFFFF0000) \
    { \
    if(iTemp<0) \
      iTemp=0; \
    else if(iTemp>65535) \
      iTemp=65535; \
  } \
  *((unsigned short *)(ucpUYorVYDst+2))=(unsigned short)iTemp

#define FIND_ROW_ADDRESSES_PART_1 \
  fpInitialRowTablePos=this->fpTotalPreCal+ \
    ((iRow-iRowLoop0)%this->iMinRowsInPreCalTable) \
    *this->iMinColsInPreCalTable*PRE_TABLE_ENTRY_SIZE_IN_FLOATS; \
  fppInitialRowTablePos[iRow]=fpInitialRowTablePos; \
  if(iInterlacingDst) \
    ucpLineDst=((iRow & 1)==1) ? ucpResult1 : ucpResult0; \
  else \
    ucpLineDst=ucpResult0; \
  ucpLineDst += (iNumBytesToJumpPerRowDst*iRow); \
  /* To overcome the cumulative rounding error, double must be used. */ \
  dTemp=((double)(iRow-iStartFrameRowDst))/(double)fRowFactor \
    +(double)fStartFrameRowSrc; \
  fTemp=(float)(dTemp+DOUBLE_TO_FLOAT_ROUNDING_OFFSET); \
  iSrcRowCut=(int)(fTemp); \
  fRowFraction=(fTemp-(float)iSrcRowCut); \
  ucppLineDst[iRow]=ucpLineDst

#define FIND_ROW_ADDRESSES_PART_2 \
  if(iSrcRowCut<iFrameRowsSrc-1) \
    iSrcRowAdd=iSrcRowCut+1; \
  else if(iSrcRowCut==iFrameRowsSrc-1) \
    iSrcRowAdd=iSrcRowCut; /* On the border. */ \
  else \
    { \
    printf("iSrcRowCut=%d,iFrameRowsSrc=%d\n", \
    iSrcRowCut,iFrameRowsSrc); \
    TRACE_0(errout << "Unable to open the par file"); \
  } \
  if(iInterlacingSrc) \
    ucpLineSrcCut=(iSrcRowCut & 1) ? ucpSrc1_corrected : ucpSrc0; \
  else \
    ucpLineSrcCut=ucpSrc0; \
  ucpLineSrcCut += (iNumBytesToJumpPerRowSrc*iSrcRowCut); \
  if(iInterlacingSrc) \
    ucpLineSrcAdd=(iSrcRowAdd & 1) ? ucpSrc1_corrected : ucpSrc0; \
  else \
    ucpLineSrcAdd=ucpSrc0; \
  ucpLineSrcAdd += (iNumBytesToJumpPerRowSrc*iSrcRowAdd); \
  ucppLineSrcCut[iRow]=ucpLineSrcCut; \
  ucppLineSrcAdd[iRow]=ucpLineSrcAdd

#define FIND_ROW_ADDRESSES_PART_LOAD_ONLY \
  fpInitialRowTablePos=fppInitialRowTablePos[iRow]; \
  ucpLineDst=ucppLineDst[iRow]; \
  ucpLineSrcCut=ucppLineSrcCut[iRow]; \
  ucpLineSrcAdd=ucppLineSrcAdd[iRow]

#define DO_REGIONIZED_UPPER_CONVERSION_PART_FLAT \
  for(iR=0;iR<iNumDstRegionRows;++iR) \
    { \
    iRegionRow0=iDstInteriorY0+(iR<<iSearchFlatRegionSizeDstPower2); \
    iRegionRow1=iRegionRow0+iSearchFlatRegionSizeDst; \
    if(iRegionRow1>iDstInteriorY1) \
      iRegionRow1=iDstInteriorY1; \
    for(iC=0;iC<iNumDstRegionCols;++iC) \
      { \
      iRegionCol0=iDstInteriorX0+(iC<<iSearchFlatRegionSizeDstPower2); \
      iRegionCol1=iRegionCol0+iSearchFlatRegionSizeDst; \
      if(iRegionCol1>iDstInteriorX1) \
      iRegionCol1=iDstInteriorX1; \
      if(*(ucpFlatRegionMask+iR*iNumDstRegionCols+iC)==1) \
	{ \
	/* This is a flat region. */ \
        for(iRow=iRegionRow0;iRow<iRegionRow1;++iRow) \
          { \
          FIND_ROW_ADDRESSES_PART_1; \
          FIND_ROW_ADDRESSES_PART_2; \
          ucpUYorVYDst=ucpLineDst+iNumBytesPerPixelDst*iRegionCol0; \
          for(iPixel=iRegionCol0;iPixel<iRegionCol1;iPixel++) \
            { \
            SET_PIXEL_POINTERS; \
            fpPreCalPointerUV=fpInitialRowTablePos \
              +ipTablePixelOffsetSimpleUV[iPixel]; \
            fpPreCalPointerY=fpPreCalPointerUV+4; \
            SET_SIMPLE_VALUES_UV; \
            SET_SIMPLE_VALUES_Y; \
            ucpUYorVYDst+=iNumBytesPerPixelDst; \
	  } \
	} \
      }

/** */
void MAGNIFY_CONTROL::MagnifyThisFrameYuv422()
{
  float fTemp;
  float *fpPreCalPointer,*fpPreCalPointerY,*fpPreCalPointerUV;
  unsigned char *ucpSrc0,*ucpSrc1;
  int iNPixelColsSrc,iNByteColsSrc,iTemp;
  unsigned char *ucpDst0,*ucpDst1;
  int iNPixelColsDst;
  float fStartPixelColSrc;
  int iStartPixelColDst;
  float fRowFactor,fColFactor,fMeanFactor;
  int iInterlacingSrc,iInterlacingDst;
  unsigned char *ucpResult0;
  unsigned char *ucpResult1;
  int iRow,iPixel;
  int iPixelLoop0,iPixelLoop1;
  unsigned char *ucpLineSrcCut;
  unsigned char *ucpLineSrcAdd;
  unsigned char *ucpLineDst;
  unsigned char *ucpUYorVYDst;
  unsigned char *ucpUYorVYSrc00,*ucpUYorVYSrc10;
  int iSrcRowCut,iSrcRowAdd;
  float fStartFrameRowSrc,fSrcPixelLocation;
  float fSrcConvertWidth;
  int iStartFrameRowDst;
  int iRowLoop0,iRowLoop1;
  int iY,iUV,iRegionRow0,iRegionRow1,iRegionCol0,iRegionCol1;
  int iUvMatchCheckingMaskSrc,iUvMatchCheckingMaskDst;
  int iSrcPixelColCut,iSrcByteColCut;
  int iRound,iSrcPixelColAdd;
  unsigned char *ucpSrc1_corrected;
  unsigned char *ucpUV00,*ucpUV10,*ucpFlatRegionMask;
  float fRowFraction,fColFraction;
  float fColFractionUV;
  int iFlatRegionVaryingLimitPercentY;
  int iFlatRegionVaryingLimitPercentUV;
  int iSearchFlatRegionSizeDst,iSearchFlatRegionSizeDstPower2;
  int iSearchFlatRegionSizeSrcHoped;
  int iSearchFlatRegionSizeSrcActualRow,iSearchFlatRegionSizeSrcActualCol;
  int iNumDstRegionRows,iNumDstRegionCols;
  float *fpInitialRowTablePos;
  int iDstByte;
  int iFrameRowsSrc,iFrameRowsDst;
  int iDstConvertWidth,iDstConvertHeight;
  int iNumBytesToJumpPerRowSrc,iNumBytesToJumpPerRowDst;
  int iR,iC,iTotalRound;
  int iRegionMaxY,iRegionMinY;
  int iRegionMaxU,iRegionMinU;
  int iRegionMaxV,iRegionMinV;
  int iDstInteriorY0,iDstInteriorX0,iDstInteriorY1,iDstInteriorX1;
  int iPixelValSrc,iPixelValDst;
  int iNumBytesPerPixelSrc,iNumBytesPerPixelDst;
  int iSearchFlatRegion;
  double dTemp;
  float **fppInitialRowTablePos=NULL;
  unsigned char **ucppLineDst=NULL;
  unsigned char **ucppLineSrcCut=NULL;
  unsigned char **ucppLineSrcAdd=NULL;
  int *ipSrcByteColCut=NULL;
  int *ipUvMatch=NULL;
  int *ipTablePixelOffsetSimpleUV=NULL;
  int *ipTablePixelOffsetLagerUV=NULL;
  int iNumLowPassFilterRows,iNumLowPassFilterCols;
  float *fpLowPassFilerParamters=NULL;

#ifdef _DEBUG_EDL
  int iNumFlatRegions=0;
  int iNumNonFlatRegions=0;
#endif

  CODE_TRACE("MagnifyThisFrame() entered");

#ifdef _DEBUG_EDL
  printf("MAGNIFY_CONTROL:\n");
  printf("iDoResetEngine=%d\n",this->iDoResetEngine);
  printf("iSearchFlatRegion=%d\n",this->iSearchFlatRegion);
  printf("iSearchFlatRegionSize=%d\n",this->iSearchFlatRegionSize);
  printf("iFlatRegionVaryingLimitPercentY=%d\n",
    this->iFlatRegionVaryingLimitPercentY);
  printf("iFlatRegionVaryingLimitPercentUV=%d\n",
    this->iFlatRegionVaryingLimitPercentUV);
  printf("iActive=%d iDoShrink=%d\n", 
    this->iActive, this->iDoShrink);
  printf("fStartFrameRowSrc=%f fStartPixelColSrc=%f\n",
    this->fStartFrameRowSrc,this->fStartPixelColSrc);
  printf("iStartFrameRowDes=%d iStartPixelColDes=%d\n",
    this->iStartFrameRowDes,this->iStartPixelColDes);
  printf("fSrcConvertHeight=%f fSrcConvertWidth=%f\n",
    this->fSrcConvertHeight,this->fSrcConvertWidth);
  printf("iDesConvertHeight=%d iDesConvertWidth=%d\n",
    this->iDesConvertHeight,this->iDesConvertWidth);
  printf("iInterlacingSrc=%d,iInterlacingDes=%d\n",
    this->iInterlacingSrc,this->iInterlacingDes);
  printf("Src size=%d %d, Des size=%d %d\n",
    this->iNFrameRowsSrc,this->iNPixelColsSrc,
    this->iNFrameRowsDes,this->iNPixelColsDes);
  printf("ucpSrc0=%d,ucpSrc1=%d ucpDes0=%d,ucpDes1=%d\n",
    this->ucpSrc0,this->ucpSrc1,this->ucpDes0,this->ucpDes1);

  printf("ucpTemp0,ucpTemp1=%d %d\n",this->ucpTemp0,this->ucpTemp1);
  printf("ucpSrcMask=%d\n",this->ucpSrcMask);
  printf("fpTotalPreCal=%d\n",this->fpTotalPreCal);
  printf("iRoundOneNeeded=%d iParFileExists=%d\n",
    this->iRoundOneNeeded,this->iParFileExists);

#endif

#ifdef _DEBUG_EDL
  printf("iMinRowsInPreCalTable=%d,fFirstDesRowOffsetInSrcStart=%f\n",
    this->iMinRowsInPreCalTable,this->fFirstDesRowOffsetInSrcStart);
  printf("iMinColsInPreCalTable=%d,fFirstDesColOffsetInSrcStart=%f\n",
    this->iMinColsInPreCalTable,this->fFirstDesColOffsetInSrcStart);
#endif

  /* Copy local version of passed parameters. */
  iPixelValSrc=iPixelValDst=this->iPixelVals;
  if(this->iDoShrink==TRUE)
    {
    iSearchFlatRegion=UPPER_CONVERSION_MATHOD_SUPER_FASTEST;
  }
  else
    iSearchFlatRegion=this->iSearchFlatRegion;

#ifdef _DEBUG_EDL
  printf("The actual iSearchFlatRegion=%d\n",iSearchFlatRegion);
#endif

  iSearchFlatRegionSizeSrcHoped=this->iSearchFlatRegionSize;
  iFlatRegionVaryingLimitPercentY=this->iFlatRegionVaryingLimitPercentY;
  iFlatRegionVaryingLimitPercentUV=this->iFlatRegionVaryingLimitPercentUV;

  iNumBytesPerPixelSrc=4;
  iNumBytesPerPixelDst=4;
  iUvMatchCheckingMaskSrc=3*(iNumBytesPerPixelSrc/2);
  iUvMatchCheckingMaskDst=3*(iNumBytesPerPixelDst/2);

  ucpSrc0=this->ucpSrc0;
  ucpSrc1=this->ucpSrc1;
  ucpDst0=this->ucpDes0;
  ucpDst1=this->ucpDes1;
  ucpFlatRegionMask=this->ucpSrcMask;

  iFrameRowsSrc=this->iNFrameRowsSrc; 
  iFrameRowsDst=this->iNFrameRowsDes; 
  fStartFrameRowSrc=this->fStartFrameRowSrc;
  iInterlacingSrc=this->iInterlacingSrc;
  iInterlacingDst=this->iInterlacingDes;
  iNPixelColsSrc=this->iNPixelColsSrc;
  iNByteColsSrc=iNPixelColsSrc*iNumBytesPerPixelSrc;
  iNPixelColsDst=this->iNPixelColsDes;
  iStartPixelColDst=this->iStartPixelColDes;
  fStartPixelColSrc=this->fStartPixelColSrc;
  fSrcConvertWidth=this->fSrcConvertWidth;
  iDstConvertHeight=this->iDesConvertHeight;
  iDstConvertWidth=this->iDesConvertWidth;

#ifdef _DEBUG_ALLOC
printf ("%s %d allocating %d\n", __FILE__, __LINE__, iFrameRowsDst*sizeof(float*)*4);
#endif
  fppInitialRowTablePos=(float **)malloc(iFrameRowsDst*sizeof(float *));
  ucppLineDst=(unsigned char **)malloc(iFrameRowsDst*sizeof(unsigned char *));
  ucppLineSrcCut=
    (unsigned char **)malloc(iFrameRowsDst*sizeof(unsigned char *));
  ucppLineSrcAdd=
    (unsigned char **)malloc(iFrameRowsDst*sizeof(unsigned char *));
  if(fppInitialRowTablePos==NULL || ucppLineDst==NULL || ucppLineSrcCut==NULL
     || ucppLineSrcAdd==NULL)
    TRACE_0(errout << "Unable to open the par file");

#ifdef _DEBUG_ALLOC
printf ("%s %d allocating %d\n", __FILE__, __LINE__, iNPixelColsDst*sizeof(int)*2);
#endif
  ipSrcByteColCut=(int *)malloc(iNPixelColsDst*sizeof(int));
  ipUvMatch=(int *)malloc(iNPixelColsDst*sizeof(int));
  if(ipSrcByteColCut==NULL || ipUvMatch==NULL)
    TRACE_0(errout << "Unable to open the par file");

#ifdef _DEBUG_ALLOC
printf ("%s %d allocating %d\n", __FILE__, __LINE__, iNPixelColsDst*sizeof(int)*2);
#endif
  ipTablePixelOffsetSimpleUV=(int *)malloc(iNPixelColsDst*sizeof(int));
  ipTablePixelOffsetLagerUV=(int *)malloc(iNPixelColsDst*sizeof(int));
  if(ipTablePixelOffsetSimpleUV==NULL || ipTablePixelOffsetLagerUV==NULL)
    TRACE_0(errout << "Unable to open the par file");

  fRowFactor=(float)(this->iDesConvertHeight)/(this->fSrcConvertHeight);
  fColFactor=(float)(this->iDesConvertWidth)/(this->fSrcConvertWidth);

  if (fRowFactor < 1. && fColFactor < 1.)
    this->iDoShrink = TRUE;
  else
    this->iDoShrink = FALSE;

#ifdef _DEBUG_EDL
  printf("Computed fRowFactor=%f fColFactor=%f DoShrink=%d\n",
	fRowFactor,fColFactor,this->iDoShrink);
#endif

  if(this->iActive==TRUE)
    {
    BlackOutThisFrame(ucpDst0,ucpDst1,this->iNFrameRowsDes,iNPixelColsDst,
      iInterlacingDst,iPixelValSrc,iPixelValDst,this->iBlackVal0,
	this->iBlackVal1,this->iBlackVal2);
  }

  if(this->iDoShrink==TRUE)
    {
    DetermineLowFilterSize(fRowFactor,&iNumLowPassFilterRows);
    if((iNumLowPassFilterRows & 1)==0)
      ++iNumLowPassFilterRows;
    DetermineLowFilterSize(fColFactor,&iNumLowPassFilterCols);
    if((iNumLowPassFilterCols & 1)==0)
      ++iNumLowPassFilterCols;

#ifdef _DEBUG_EDL
    printf("Down converting filter size is %d rows %d cols\n",
      iNumLowPassFilterRows,iNumLowPassFilterCols);
#endif

    if(this->iDoShrinkFilter==TRUE &&
      (iNumLowPassFilterRows > 1 || iNumLowPassFilterCols>1))
      {
#ifdef _DEBUG_ALLOC
printf ("%s %d allocating %d\n", __FILE__, __LINE__, iNumLowPassFilterRows*
        iNumLowPassFilterCols*sizeof(float));
#endif
      fpLowPassFilerParamters=(float *)malloc(iNumLowPassFilterRows*
        iNumLowPassFilterCols*sizeof(float));
      if(fpLowPassFilerParamters==NULL)
        TRACE_0(errout << "Unable to open the par file");
      Fill2DLowPassFilerParamters(iNumLowPassFilterRows,
        iNumLowPassFilterCols,fRowFactor,fColFactor,
        fpLowPassFilerParamters);
      LowPassFilterOnThisFrame(this->ucpSrc0,this->ucpSrc1,
        this->ucpTemp0,this->ucpTemp1,this->iNFrameRowsSrc,
        this->iNPixelColsSrc,this->iInterlacingSrc,
        iNumLowPassFilterRows,iNumLowPassFilterCols,fpLowPassFilerParamters);
      free(fpLowPassFilerParamters);
      ucpSrc0=this->ucpTemp0;
      ucpSrc1=this->ucpTemp1;
    }
  }
  if(iInterlacingSrc)
    iNumBytesToJumpPerRowSrc=iNPixelColsSrc*iNumBytesPerPixelSrc/2;
  else
    iNumBytesToJumpPerRowSrc=iNPixelColsSrc*iNumBytesPerPixelSrc;

  iStartFrameRowDst=this->iStartFrameRowDes;
  iDstInteriorY0=iStartFrameRowDst+4;
  iDstInteriorX0=iStartPixelColDst+4;
  iDstInteriorY1=iStartFrameRowDst+iDstConvertHeight-4;
  iDstInteriorX1=iStartPixelColDst+iDstConvertWidth-4;

#ifdef _DEBUG_EDL
  printf("Destination interior (l,r,t,b)=%d %d %d %d\n",
    iDstInteriorX0,iDstInteriorX1,iDstInteriorY0,iDstInteriorY1);
#endif

  if(iInterlacingDst)
    iNumBytesToJumpPerRowDst=iNPixelColsDst*iNumBytesPerPixelDst/2;
  else
    iNumBytesToJumpPerRowDst=iNPixelColsDst*iNumBytesPerPixelDst;

  if(this->iDoResetEngine==TRUE)
    Initialize();

  /* watch out for rounding errors in these checks */
  if((this->iDoShrink!=TRUE && fRowFactor<1.0 && fColFactor<1.0)
     || fRowFactor<0.001 || fColFactor<0.001
    || (this->iDoShrink==TRUE && (fRowFactor>1.0 || fColFactor>1.0))
    || 
    (int)(fStartFrameRowSrc + ((float)(iDstConvertHeight))/fRowFactor) >
     iFrameRowsSrc
    ||
    (int)(fStartPixelColSrc +fSrcConvertWidth) > iNPixelColsSrc)
    {
    printf("Source out of bound ? %f %f %d %f %f %d\n",
      fStartFrameRowSrc,(((float)(iDstConvertHeight))/fRowFactor),
      iFrameRowsSrc,
      fStartPixelColSrc,fSrcConvertWidth,
      iNPixelColsSrc);
    printf("fRowFactor=%f, fColFactor=%f iDoShrink=%d\n",fRowFactor,fColFactor,
      this->iDoShrink);
    printf("fStartPixelColSrc=%f\n",fStartPixelColSrc);
    TRACE_0(errout << "Unable to open the par file");
  }

  ucpResult0=ucpDst0;
  ucpResult1=ucpDst1 - iNPixelColsDst*iNumBytesPerPixelDst/2;
  ucpSrc1_corrected=ucpSrc1-iNPixelColsSrc*iNumBytesPerPixelSrc/2;

  iTotalRound=2;

  if(iSearchFlatRegion==UPPER_CONVERSION_MATHOD_MEDIAN ||
     iSearchFlatRegion==UPPER_CONVERSION_MATHOD_FASTEST)
    {
    /* First, determine the size of search region in destination. */
    fMeanFactor=(fRowFactor+fColFactor)/2.0;
    iSearchFlatRegionSizeDst=(int)(fMeanFactor
      *(float)(iSearchFlatRegionSizeSrcHoped-6)+0.5);
    /* Go to the next 2 to power. */
    iSearchFlatRegionSizeDstPower2=0;
    while((1<<iSearchFlatRegionSizeDstPower2)<=iSearchFlatRegionSizeDst)
      iSearchFlatRegionSizeDstPower2++;
    iSearchFlatRegionSizeDst=1<<iSearchFlatRegionSizeDstPower2;
    iNumDstRegionRows=iFrameRowsDst/iSearchFlatRegionSizeDst;
    if(iFrameRowsDst%iSearchFlatRegionSizeDst>0)
      iNumDstRegionRows++;
    iNumDstRegionCols=iNPixelColsDst/iSearchFlatRegionSizeDst;
    if(iNPixelColsDst%iSearchFlatRegionSizeDst>0)
      iNumDstRegionCols++;

    iSearchFlatRegionSizeSrcActualRow=
      (int)((float)iSearchFlatRegionSizeDst/fRowFactor+0.5)+6;
    iSearchFlatRegionSizeSrcActualCol=
      (int)((float)iSearchFlatRegionSizeDst/fColFactor+0.5)+6;

#ifdef _DEBUG_EDL
    printf("iSearchFlatRegionSizeDst=%d\n",iSearchFlatRegionSizeDst);
    printf("iNumDstRegionRows,Cols=%d %d\n",
      iNumDstRegionRows,iNumDstRegionCols);
    printf("iSearchFlatRegionSizeSrcActualRow,Col=%d %d\n",
      iSearchFlatRegionSizeSrcActualRow,
      iSearchFlatRegionSizeSrcActualCol);
#endif

    /* For each destination region, determine its source is flat or
       not. */
    if(iSearchFlatRegion==UPPER_CONVERSION_MATHOD_MEDIAN)
      {
      for(iR=0;iR<iNumDstRegionRows;++iR)
        {
        iRegionRow0=iDstInteriorY0+(iR<<iSearchFlatRegionSizeDstPower2);
        fTemp=((float)(iRegionRow0-iStartFrameRowDst))/fRowFactor
          +fStartFrameRowSrc;
        iSrcRowCut=(int)(fTemp)-3;
        iSrcRowAdd=iSrcRowCut+iSearchFlatRegionSizeSrcActualRow;
        if(iSrcRowCut>=0 && iSrcRowAdd<=iFrameRowsSrc)
	{
        for(iC=0;iC<iNumDstRegionCols;++iC)
    	  {
          iRegionCol0=iDstInteriorX0+(iC<<iSearchFlatRegionSizeDstPower2);
          fTemp=((float)(iRegionCol0-iStartPixelColDst))/fColFactor
            +fStartPixelColSrc;
          iSrcPixelColCut=(int)fTemp-3;
          iSrcPixelColAdd=iSrcPixelColCut+iSearchFlatRegionSizeSrcActualCol;
          iRegionMaxY=iRegionMaxU=iRegionMaxV=0;
          iRegionMinY=iRegionMinU=iRegionMinV=65536;
          if(iSrcPixelColCut>=0 && iSrcPixelColAdd<=iNPixelColsSrc)
          {
          for(iRow=iSrcRowCut;iRow<iSrcRowAdd && iRow<iFrameRowsSrc;++iRow)
    	    {
            if(iInterlacingSrc)
              ucpUV00=(iRow & 1) ? ucpSrc1_corrected : ucpSrc0;
            else
              ucpUV00=ucpSrc0;
            ucpUV00+=iNumBytesToJumpPerRowSrc*iRow;
            ucpUV00+=iSrcPixelColCut*iNumBytesPerPixelSrc;
            for(iPixel=iSrcPixelColCut;iPixel<iSrcPixelColAdd && 
              iPixel<iNPixelColsSrc;++iPixel)
    	      {
              iY=*((unsigned short *)(ucpUV00+2));
              iUV=*((unsigned short *)(ucpUV00));
              if(iY>iRegionMaxY)
                iRegionMaxY=iY;
              else if(iY<iRegionMinY)
                iRegionMinY=iY;
              if(iPixel & 1)
    	        {
                if(iUV>iRegionMaxV)
                  iRegionMaxV=iUV;
                else if(iUV<iRegionMinV)
                  iRegionMinV=iUV;
    	      }
              else
    	        {
                if(iUV>iRegionMaxU)
                  iRegionMaxU=iUV;
                else if(iUV<iRegionMinU)
                  iRegionMinU=iUV;
    	      }
              ucpUV00+=iNumBytesPerPixelSrc;
    	    }
    	  }
          }
          if((iRegionMaxY-iRegionMinY)*100/(iRegionMaxY+iRegionMinY+1)
             <iFlatRegionVaryingLimitPercentY &&
             (iRegionMaxU-iRegionMinU)*100/(iRegionMaxU+iRegionMinU+1)
             <iFlatRegionVaryingLimitPercentUV &&
             (iRegionMaxV-iRegionMinV)*100/(iRegionMaxV+iRegionMinV+1)
             <iFlatRegionVaryingLimitPercentUV)
	    {
#ifdef _DEBUG_EDL
            ++iNumFlatRegions;
#endif
            *(ucpFlatRegionMask+iR*iNumDstRegionCols+iC)=1;
          }
          else
	    {
#ifdef _DEBUG_EDL
            iNumNonFlatRegions++;
#endif
            *(ucpFlatRegionMask+iR*iNumDstRegionCols+iC)=0;
          }
	}
        }
      }
    } /* if(iSearchFlatRegion==UPPER_CONVERSION_MATHOD_MEDIAN) */
    else
      {
      for(iR=0;iR<iNumDstRegionRows;++iR)
        {
        iRegionRow0=iDstInteriorY0+(iR<<iSearchFlatRegionSizeDstPower2);
        fTemp=((float)(iRegionRow0-iStartFrameRowDst))/fRowFactor
          +fStartFrameRowSrc;
        iSrcRowCut=(int)(fTemp)-3;
        iSrcRowAdd=iSrcRowCut+iSearchFlatRegionSizeSrcActualRow;
        if(iSrcRowCut>=0 && iSrcRowAdd<=iFrameRowsSrc)
	{
        for(iC=0;iC<iNumDstRegionCols;++iC)
    	  {
          iRegionCol0=iDstInteriorX0+(iC<<iSearchFlatRegionSizeDstPower2);
          fTemp=((float)(iRegionCol0-iStartPixelColDst))/fColFactor
            +fStartPixelColSrc;
          iSrcPixelColCut=(int)fTemp-3;
          iSrcPixelColAdd=iSrcPixelColCut+iSearchFlatRegionSizeSrcActualCol;
          iRegionMaxY=0;
          iRegionMinY=65536;
          if(iSrcPixelColCut>=0 && iSrcPixelColAdd<=iNPixelColsSrc)
          {
          for(iRow=iSrcRowCut;iRow<iSrcRowAdd && iRow<iFrameRowsSrc;++iRow)
    	    {
            if(iInterlacingSrc)
              ucpUV00=(iRow & 1) ? ucpSrc1_corrected : ucpSrc0;
            else
              ucpUV00=ucpSrc0;
            ucpUV00+=iNumBytesToJumpPerRowSrc*iRow;
            ucpUV00+=iSrcPixelColCut*iNumBytesPerPixelSrc+2;
            for(iPixel=iSrcPixelColCut;iPixel<iSrcPixelColAdd && 
              iPixel<iNPixelColsSrc;++iPixel)
    	      {
              iY=*((unsigned short *)(ucpUV00));
              if(iY>iRegionMaxY)
                iRegionMaxY=iY;
              else if(iY<iRegionMinY)
                iRegionMinY=iY;
              ucpUV00+=iNumBytesPerPixelSrc;
    	    }
    	  }
          }
          if((iRegionMaxY-iRegionMinY)*100/(iRegionMaxY+iRegionMinY+1)
             <iFlatRegionVaryingLimitPercentY)
	    {
#ifdef _DEBUG_EDL
            ++iNumFlatRegions;
#endif
            *(ucpFlatRegionMask+iR*iNumDstRegionCols+iC)=1;
          }
          else
	    {
#ifdef _DEBUG_EDL
            iNumNonFlatRegions++;
#endif
            *(ucpFlatRegionMask+iR*iNumDstRegionCols+iC)=0;
          }
	}
        }
      }
    }
#ifdef _DEBUG_EDL
    printf("Destination image has %d flat regions, %d varying regions.\n",
      iNumFlatRegions,iNumNonFlatRegions);
#endif
  }

  for(iRound=0;iRound<iTotalRound;++iRound)
    {
    if(iRound==0)
      {
      /* If the source box is changed, force it to reload. */
      if(this->iRoundOneNeeded==FALSE)
        goto next_round;
      else
        {
        if(LoadParFile()==TRUE)
    	  {
#ifdef _DEBUG_EDL
          printf("Using existing parameter files for up conversion.\n");
#endif
          this->iParFileExists=TRUE;
          this->iRoundOneNeeded=FALSE;
          goto next_round;
    	}
        else
    	  {
#ifdef _DEBUG_EDL
          printf("Generating new parameter files for up conversion.\n");
#endif
          this->iParFileExists=FALSE;
          this->iRoundOneNeeded=FALSE;
        }
      }
    }

    iRowLoop0=iStartFrameRowDst;

    if(iRound==0 && this->iParFileExists==FALSE)
      iRowLoop1=iRowLoop0+this->iMinRowsInPreCalTable;
    else
      iRowLoop1=iRowLoop0+iDstConvertHeight;

    iPixelLoop0=iStartPixelColDst;

    if(iRound==0 && this->iParFileExists==FALSE)
      iPixelLoop1=iPixelLoop0+this->iMinColsInPreCalTable;
    else
      iPixelLoop1=iPixelLoop0+iDstConvertWidth;

    if(iRound==0)
      {
      for(iRow=iRowLoop0;iRow<iRowLoop1;++iRow)
        {
        FIND_ROW_ADDRESSES_PART_1;
        FIND_ROW_ADDRESSES_PART_2;
   
        ucpUYorVYDst=ucpLineDst+iNumBytesPerPixelDst*iPixelLoop0;
        for(iPixel=iPixelLoop0;iPixel<iPixelLoop1;iPixel++)
   	  {
          iDstByte=iPixel*iNumBytesPerPixelDst;
          dTemp=((double)(iPixel-iStartPixelColDst))/(double)fColFactor
            +(double)fStartPixelColSrc;
          fSrcPixelLocation=(float)(dTemp+DOUBLE_TO_FLOAT_ROUNDING_OFFSET);
          iSrcPixelColCut=((int)fSrcPixelLocation);
          fColFraction=fSrcPixelLocation-(float)iSrcPixelColCut;
          iSrcByteColCut=iSrcPixelColCut*iNumBytesPerPixelSrc;
          /* iSrcByteColCut is not guaranteed to be at UV's position. */
          ucpUYorVYSrc00=ucpLineSrcCut+iSrcByteColCut;
          ucpUYorVYSrc10=ucpLineSrcAdd+iSrcByteColCut;
          /* Chengda 000709. Deleting the next two lines should have no 
             problem. Leav them here in case wrong judgement. 
          ucpUV00=((iDstByte & iUvMatchCheckingMaskDst)
            ==(iSrcByteColCut & iUvMatchCheckingMaskSrc)) ? ucpUYorVYSrc00 :
            (ucpUYorVYSrc00 - 4);
          ucpUV10=((iDstByte & iUvMatchCheckingMaskDst)
            ==(iSrcByteColCut & iUvMatchCheckingMaskSrc)) ? ucpUYorVYSrc10 :
            (ucpUYorVYSrc10 - 4);
          */
          fColFractionUV=((iDstByte & iUvMatchCheckingMaskDst)
            ==((iSrcByteColCut) & iUvMatchCheckingMaskSrc))
            ? (fColFraction/2) : ((1.0+fColFraction)/2.0);
          fpPreCalPointer=fpInitialRowTablePos
            +((iPixel-iPixelLoop0)%this->iMinColsInPreCalTable)*
            PRE_TABLE_ENTRY_SIZE_IN_FLOATS;
          Generic2dSplineForUYorVY(fpPreCalPointer,
            fpPreCalPointer+16,
            fRowFraction,fColFractionUV,fColFraction,
            NEIGHBOR_TYPE_4X4_OVELAP_FOR_Y);
          Generic2dSplineForUYorVY(fpPreCalPointer+48,
            fpPreCalPointer+52,
            fRowFraction,fColFractionUV,fColFraction,
            NEIGHBOR_TYPE_2x2); 
          ucpUYorVYDst+=iNumBytesPerPixelDst;
        } /* iPixel */
      } /* iRow */
      if(iRound==0 && this->iParFileExists==FALSE)
	{
        SaveParFile();
        this->iParFileExists=TRUE;
      }
    } /* if(iRound==0) */
    else
      {
      /* Real computing to generate data. */
      /* First, go through all borders. */
      for(iRow=iRowLoop0;iRow<iDstInteriorY0;++iRow)
	{
        FIND_ROW_ADDRESSES_PART_1;
        FIND_ROW_ADDRESSES_PART_2;
        ucpUYorVYDst=ucpLineDst+iNumBytesPerPixelDst*iPixelLoop0;
        for(iPixel=iPixelLoop0;iPixel<iPixelLoop1;iPixel++)
          {
          SET_PIXELWISE_LOCATION_VARIBLES;
          ipTablePixelOffsetLagerUV[iPixel]
            =((iPixel-iPixelLoop0)%this->iMinColsInPreCalTable)
            *PRE_TABLE_ENTRY_SIZE_IN_FLOATS;
          ipTablePixelOffsetSimpleUV[iPixel]=
            ipTablePixelOffsetLagerUV[iPixel]+48;
          fpPreCalPointerUV=fpInitialRowTablePos+
            ipTablePixelOffsetSimpleUV[iPixel];
          fpPreCalPointerY=fpPreCalPointerUV+4;
          SET_SIMPLE_VALUES_UV;
          SET_SIMPLE_VALUES_Y;
          ucpUYorVYDst+=iNumBytesPerPixelDst;
	}
      }
      for(iRow=iDstInteriorY1;iRow<iRowLoop1;++iRow)
	{
        FIND_ROW_ADDRESSES_PART_1;
        FIND_ROW_ADDRESSES_PART_2;
        ucpUYorVYDst=ucpLineDst+iNumBytesPerPixelDst*iPixelLoop0;
        for(iPixel=iPixelLoop0;iPixel<iPixelLoop1;iPixel++)
          {
          SET_PIXEL_POINTERS;
          fpPreCalPointerUV=fpInitialRowTablePos
            +ipTablePixelOffsetSimpleUV[iPixel];
          fpPreCalPointerY=fpPreCalPointerUV+4;
          SET_SIMPLE_VALUES_UV;
          SET_SIMPLE_VALUES_Y;
          ucpUYorVYDst+=iNumBytesPerPixelDst;
	}
      }
      for(iRow=iRowLoop0;iRow<iRowLoop1;++iRow)
	{
        FIND_ROW_ADDRESSES_PART_1;
        FIND_ROW_ADDRESSES_PART_2;
        ucpUYorVYDst=ucpLineDst+iNumBytesPerPixelDst*iPixelLoop0;
        for(iPixel=iPixelLoop0;iPixel<iDstInteriorX0;iPixel++)
          {
          SET_PIXEL_POINTERS;
          fpPreCalPointerUV=fpInitialRowTablePos
            +ipTablePixelOffsetSimpleUV[iPixel];
          fpPreCalPointerY=fpPreCalPointerUV+4;
          SET_SIMPLE_VALUES_UV;
          SET_SIMPLE_VALUES_Y;
          ucpUYorVYDst+=iNumBytesPerPixelDst;
	}
      }
      for(iRow=iRowLoop0;iRow<iRowLoop1;++iRow)
	{
        FIND_ROW_ADDRESSES_PART_LOAD_ONLY;
        ucpUYorVYDst=ucpLineDst+iNumBytesPerPixelDst*iDstInteriorX1;
        for(iPixel=iDstInteriorX1;iPixel<iPixelLoop1;iPixel++)
          {
          SET_PIXEL_POINTERS;
          fpPreCalPointerUV=fpInitialRowTablePos
            +ipTablePixelOffsetSimpleUV[iPixel];
          fpPreCalPointerY=fpPreCalPointerUV+4;
          SET_SIMPLE_VALUES_UV;
          SET_SIMPLE_VALUES_Y;
          ucpUYorVYDst+=iNumBytesPerPixelDst;
	}
      }
      /* Go into ineterior */
      switch(iSearchFlatRegion)
        {
        case UPPER_CONVERSION_MATHOD_SUPER_FASTEST:
          /* Y uses 4 neighbors, UV uses 4 neighbors. */
          for(iRow=iDstInteriorY0;iRow<iDstInteriorY1;++iRow)
	    {
            FIND_ROW_ADDRESSES_PART_LOAD_ONLY;
            ucpUYorVYDst=ucpLineDst+iNumBytesPerPixelDst*iDstInteriorX0;
            for(iPixel=iDstInteriorX0;iPixel<iDstInteriorX1;iPixel++)
              {
              SET_PIXEL_POINTERS;
              fpPreCalPointerUV=fpInitialRowTablePos
                +ipTablePixelOffsetSimpleUV[iPixel];
              fpPreCalPointerY=fpPreCalPointerUV+4;
              SET_SIMPLE_VALUES_UV;
              SET_SIMPLE_VALUES_Y;
              ucpUYorVYDst+=iNumBytesPerPixelDst;
	    }
          }
          break;
        case UPPER_CONVERSION_MATHOD_BEST:
          /* Y uses 32 neighbors, UV uses 16 neighbors. */
          for(iRow=iDstInteriorY0;iRow<iDstInteriorY1;++iRow)
	    {
            FIND_ROW_ADDRESSES_PART_LOAD_ONLY;
            ucpUYorVYDst=ucpLineDst+iNumBytesPerPixelDst*iDstInteriorX0;
            for(iPixel=iDstInteriorX0;iPixel<iDstInteriorX1;iPixel++)
              {
              SET_PIXEL_POINTERS;
              fpPreCalPointerUV=fpInitialRowTablePos
                +ipTablePixelOffsetLagerUV[iPixel];
              fpPreCalPointerY=fpPreCalPointerUV+16;
              SET_LARGER_NEIGHBORED_VALUES_UV;
              SET_LARGER_NEIGHBORED_VALUES_Y;
              ucpUYorVYDst+=iNumBytesPerPixelDst;
	    }
          }
          break;
        case UPPER_CONVERSION_MATHOD_MEDIAN:
          /* Y uses 32 or 4 neighbors, UV uses 16 or 4 neighbors,
             depending on the flatness of the region. */
          DO_REGIONIZED_UPPER_CONVERSION_PART_FLAT
              else /* This is a varying region. */
                {
                for(iRow=iRegionRow0;iRow<iRegionRow1;++iRow)
                  {
                  FIND_ROW_ADDRESSES_PART_LOAD_ONLY;
                  ucpUYorVYDst=ucpLineDst+iNumBytesPerPixelDst*iRegionCol0;
                  for(iPixel=iRegionCol0;iPixel<iRegionCol1;iPixel++)
                    {
                    SET_PIXEL_POINTERS;
                    fpPreCalPointerUV=fpInitialRowTablePos
                      +ipTablePixelOffsetLagerUV[iPixel];
                    fpPreCalPointerY=fpPreCalPointerUV+16;
                    SET_LARGER_NEIGHBORED_VALUES_UV;
                    SET_LARGER_NEIGHBORED_VALUES_Y;
                    ucpUYorVYDst+=iNumBytesPerPixelDst;
		  }
		}  
	      }
	    }
          }
          break;
        case UPPER_CONVERSION_MATHOD_FASTEST:
          /* Y uses 32 or 4 neighbors, UV uses 4 neighbors,
             depending on the flatness of the region. */
          DO_REGIONIZED_UPPER_CONVERSION_PART_FLAT
              else /* This is a varying region. */
                {
                for(iRow=iRegionRow0;iRow<iRegionRow1;++iRow)
                  {
                  FIND_ROW_ADDRESSES_PART_LOAD_ONLY;
                  ucpUYorVYDst=ucpLineDst+iNumBytesPerPixelDst*iRegionCol0;
                  for(iPixel=iRegionCol0;iPixel<iRegionCol1;iPixel++)
                    {
                    SET_PIXEL_POINTERS;
                    fpPreCalPointerUV=fpInitialRowTablePos
                      +ipTablePixelOffsetSimpleUV[iPixel];
                    fpPreCalPointerY=fpPreCalPointerUV-32;
                    SET_SIMPLE_VALUES_UV;
                    SET_LARGER_NEIGHBORED_VALUES_Y;
                    ucpUYorVYDst+=iNumBytesPerPixelDst;
		  }
		}  
	      }
	    }
          }
          break;
        default:
          TRACE_0(errout << "Unable to open the par file");
      }
    } /* if(iRound==1) */
next_round:
  ;
  } /* for(iRound=0;iRunnd<iTotalRound;++iRunnd) */
  if(fppInitialRowTablePos!=NULL)
    free(fppInitialRowTablePos);
  if(ucppLineDst!=NULL)
    free(ucppLineDst);
  if(ucppLineSrcCut!=NULL)
    free(ucppLineSrcCut);
  if(ucppLineSrcAdd!=NULL)
    free(ucppLineSrcAdd);
  if(ipSrcByteColCut!=NULL)
    free(ipSrcByteColCut);
  if(ipUvMatch!=NULL)
    free(ipUvMatch);
  if(ipTablePixelOffsetSimpleUV!=NULL)
    free(ipTablePixelOffsetSimpleUV);
  if(ipTablePixelOffsetLagerUV!=NULL)
    free(ipTablePixelOffsetLagerUV);

#ifdef _DEBUG_EDL
  printf("MagnifyThisFrame() exiting\n");
#endif
}


#define SET_PIXEL_POINTERS_444 \
  ucpUYorVYSrc00=ucpLineSrcCut+ipSrcByteColCut[iPixel]; \
  ucpUYorVYSrc10=ucpLineSrcAdd+ipSrcByteColCut[iPixel]

#define SET_PIXELWISE_LOCATION_VARIBLES_444 \
  dTemp=((double)(iPixel-iStartPixelColDst))/(double)fColFactor \
    +(double)fStartPixelColSrc; \
  fSrcPixelLocation=(float)(dTemp+DOUBLE_TO_FLOAT_ROUNDING_OFFSET); \
  iSrcPixelColCut=((int)fSrcPixelLocation); \
  iSrcByteColCut=iSrcPixelColCut*iNumBytesPerPixelSrc; \
  /* iSrcByteColCut is not guaranteed to be at UV's position. */ \
  ipSrcByteColCut[iPixel]=iSrcByteColCut; \
  SET_PIXEL_POINTERS_444

#define SET_SIMPLE_VALUES_Y_444 \
  fTemp=(*fpPreCalPointerY++)* \
     ((float)(*((unsigned short *)(ucpUYorVYSrc00)))); \
  fTemp+=(*fpPreCalPointerY++)* \
     ((float)(*((unsigned short *)(ucpUYorVYSrc00+6)))); \
  fTemp+=(*fpPreCalPointerY++)* \
     ((float)(*((unsigned short *)(ucpUYorVYSrc10)))); \
  fTemp+=(*fpPreCalPointerY++)* \
     ((float)(*((unsigned short *)(ucpUYorVYSrc10+6)))); \
  *((unsigned short *)(ucpUYorVYDst))=(unsigned short)(fTemp+0.5); \
  fpPreCalPointerY-=4; \
  ucpUYorVYSrc00 += 2; \
  ucpUYorVYSrc10 += 2; \
  ucpUYorVYDst += 2
  
#define SET_LARGER_NEIGHBORED_VALUES_Y_444 \
  /* Form the Y data */ \
  /* Row -2, 4 pixels. */ \
  fTemp=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc00-iNByteColsSrc-6)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc00-iNByteColsSrc)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc00-iNByteColsSrc+6)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc00-iNByteColsSrc+12)))); \
  /* Row -1, 6 pixels. */ \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc10-iNByteColsSrc-12)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc10-iNByteColsSrc-6)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc10-iNByteColsSrc)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc10-iNByteColsSrc+6)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc10-iNByteColsSrc+12)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc10-iNByteColsSrc+18)))); \
  /* Row 0, 6 pixels. */ \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *)(ucpUYorVYSrc00-12)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *)(ucpUYorVYSrc00-6)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *)(ucpUYorVYSrc00)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *)(ucpUYorVYSrc00+6)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *)(ucpUYorVYSrc00+12)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *)(ucpUYorVYSrc00+18)))); \
  /* Row 1, 6 pixels. */ \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *)(ucpUYorVYSrc10-12)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *)(ucpUYorVYSrc10-6)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *)(ucpUYorVYSrc10)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *)(ucpUYorVYSrc10+6)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *)(ucpUYorVYSrc10+12)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *)(ucpUYorVYSrc10+18)))); \
  /* Row 2, 6 pixels. */ \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc00+iNByteColsSrc-12)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc00+iNByteColsSrc-6)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc00+iNByteColsSrc)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc00+iNByteColsSrc+6)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc00+iNByteColsSrc+12)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc00+iNByteColsSrc+18)))); \
  /* Row 3, 4 pixels. */ \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc10+iNByteColsSrc-6)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc10+iNByteColsSrc)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc10+iNByteColsSrc+6)))); \
  fTemp+=(*fpPreCalPointerY++)* \
    ((float)(*((unsigned short *) \
    (ucpUYorVYSrc10+iNByteColsSrc+12)))); \
  iTemp=(int)(fTemp+0.5); \
  if(iTemp & 0xFFFF0000) \
    { \
    if(iTemp<0) \
      iTemp=0; \
    else if(iTemp>65535) \
      iTemp=65535; \
  } \
  *((unsigned short *)(ucpUYorVYDst))=(unsigned short)iTemp; \
  ucpUYorVYDst += 2; \
  ucpUYorVYSrc00 += 2; \
  ucpUYorVYSrc10 += 2; \
  fpPreCalPointerY -= 32

#define DO_REGIONIZED_UPPER_CONVERSION_PART_FLAT_444 \
  for(iR=0;iR<iNumDstRegionRows;++iR) \
    { \
    iRegionRow0=iDstInteriorY0+(iR<<iSearchFlatRegionSizeDstPower2); \
    iRegionRow1=iRegionRow0+iSearchFlatRegionSizeDst; \
    if(iRegionRow1>iDstInteriorY1) \
      iRegionRow1=iDstInteriorY1; \
    for(iC=0;iC<iNumDstRegionCols;++iC) \
      { \
      iRegionCol0=iDstInteriorX0+(iC<<iSearchFlatRegionSizeDstPower2); \
      iRegionCol1=iRegionCol0+iSearchFlatRegionSizeDst; \
      if(iRegionCol1>iDstInteriorX1) \
      iRegionCol1=iDstInteriorX1; \
      if(*(ucpFlatRegionMask+iR*iNumDstRegionCols+iC)==1) \
	{ \
	/* This is a flat region. */ \
        for(iRow=iRegionRow0;iRow<iRegionRow1;++iRow) \
          { \
          FIND_ROW_ADDRESSES_PART_1; \
          FIND_ROW_ADDRESSES_PART_2; \
          ucpUYorVYDst=ucpLineDst+iNumBytesPerPixelDst*iRegionCol0; \
          for(iPixel=iRegionCol0;iPixel<iRegionCol1;iPixel++) \
            { \
            SET_PIXEL_POINTERS_444; \
            fpPreCalPointerY=fpInitialRowTablePos \
              +ipTablePixelOffsetSimpleUV[iPixel]+4; \
            SET_SIMPLE_VALUES_Y_444; \
            SET_SIMPLE_VALUES_Y_444; \
            SET_SIMPLE_VALUES_Y_444; \
	  } \
	} \
      }

/** */
void MAGNIFY_CONTROL::MagnifyThisFrameYuv444()
{
  float fTemp;
  float *fpPreCalPointer,*fpPreCalPointerY;
  unsigned char *ucpSrc0,*ucpSrc1;
  int iNPixelColsSrc,iNByteColsSrc,iTemp;
  unsigned char *ucpDst0,*ucpDst1;
  int iNPixelColsDst;
  float fStartPixelColSrc;
  int iStartPixelColDst;
  float fRowFactor,fColFactor,fMeanFactor;
  int iInterlacingSrc,iInterlacingDst;
  unsigned char *ucpResult0;
  unsigned char *ucpResult1;
  int iRow,iPixel;
  int iPixelLoop0,iPixelLoop1;
  unsigned char *ucpLineSrcCut;
  unsigned char *ucpLineSrcAdd;
  unsigned char *ucpLineDst;
  unsigned char *ucpUYorVYDst;
  unsigned char *ucpUYorVYSrc00,*ucpUYorVYSrc10;
  int iSrcRowCut,iSrcRowAdd;
  float fStartFrameRowSrc,fSrcPixelLocation;
  float fSrcConvertWidth;
  int iStartFrameRowDst;
  int iRowLoop0,iRowLoop1;
  int iY,iU,iV,iRegionRow0,iRegionRow1,iRegionCol0,iRegionCol1;
  int iSrcPixelColCut,iSrcByteColCut;
  int iRound,iSrcPixelColAdd;
  unsigned char *ucpSrc1_corrected;
  unsigned char *ucpUV00,*ucpFlatRegionMask;
  float fRowFraction,fColFraction;
  int iFlatRegionVaryingLimitPercentY;
  int iSearchFlatRegionSizeDst,iSearchFlatRegionSizeDstPower2;
  int iSearchFlatRegionSizeSrcHoped;
  int iSearchFlatRegionSizeSrcActualRow,iSearchFlatRegionSizeSrcActualCol;
  int iNumDstRegionRows,iNumDstRegionCols;
  float *fpInitialRowTablePos;
  int iFrameRowsSrc,iFrameRowsDst;
  int iDstConvertWidth,iDstConvertHeight;
  int iNumBytesToJumpPerRowSrc,iNumBytesToJumpPerRowDst;
  int iR,iC,iTotalRound;
  int iRegionMaxY,iRegionMinY;
  int iRegionMaxU,iRegionMinU;
  int iRegionMaxV,iRegionMinV;
  int iDstInteriorY0,iDstInteriorX0,iDstInteriorY1,iDstInteriorX1;
  int iPixelValSrc,iPixelValDst;
  int iNumBytesPerPixelSrc,iNumBytesPerPixelDst;
  int iSearchFlatRegion;
  double dTemp;
  float **fppInitialRowTablePos=NULL;
  unsigned char **ucppLineDst=NULL;
  unsigned char **ucppLineSrcCut=NULL;
  unsigned char **ucppLineSrcAdd=NULL;
  int *ipSrcByteColCut=NULL;
  int *ipTablePixelOffsetSimpleUV=NULL;
  int *ipTablePixelOffsetLagerUV=NULL;
  int iNumLowPassFilterRows,iNumLowPassFilterCols;
  float *fpLowPassFilerParamters=NULL;

#ifdef _DEBUG_EDL
  int iNumFlatRegions=0;
  int iNumNonFlatRegions=0;
#endif

  CODE_TRACE("MagnifyThisFrameYuv444() entered");

#ifdef _DEBUG_EDL
  printf("MAGNIFY_CONTROL (for YUV 444):\n");
  printf("iDoResetEngine=%d\n",this->iDoResetEngine);
  printf("iSearchFlatRegion=%d\n",this->iSearchFlatRegion);
  printf("iSearchFlatRegionSize=%d\n",this->iSearchFlatRegionSize);
  printf("iFlatRegionVaryingLimitPercentY=%d\n",
    this->iFlatRegionVaryingLimitPercentY);
  printf("iFlatRegionVaryingLimitPercentUV=%d\n",
    this->iFlatRegionVaryingLimitPercentUV);
  printf("iActive=%d iDoShrink=%d\n", 
    this->iActive, this->iDoShrink);
  printf("fStartFrameRowSrc=%f fStartPixelColSrc=%f\n",
    this->fStartFrameRowSrc,this->fStartPixelColSrc);
  printf("iStartFrameRowDes=%d iStartPixelColDes=%d\n",
    this->iStartFrameRowDes,this->iStartPixelColDes);
  printf("fSrcConvertHeight=%f fSrcConvertWidth=%f\n",
    this->fSrcConvertHeight,this->fSrcConvertWidth);
  printf("iDesConvertHeight=%d iDesConvertWidth=%d\n",
    this->iDesConvertHeight,this->iDesConvertWidth);
  printf("iInterlacingSrc=%d,iInterlacingDes=%d\n",
    this->iInterlacingSrc,this->iInterlacingDes);
  printf("Src size=%d %d, Des size=%d %d\n",
    this->iNFrameRowsSrc,this->iNPixelColsSrc,
    this->iNFrameRowsDes,this->iNPixelColsDes);
  printf("ucpSrc0=%d,ucpSrc1=%d ucpDes0=%d,ucpDes1=%d\n",
    this->ucpSrc0,this->ucpSrc1,this->ucpDes0,this->ucpDes1);

  printf("ucpTemp0,ucpTemp1=%d %d\n",this->ucpTemp0,this->ucpTemp1);
  printf("ucpSrcMask=%d\n",this->ucpSrcMask);
  printf("fpTotalPreCal=%d\n",this->fpTotalPreCal);
  printf("iRoundOneNeeded=%d iParFileExists=%d\n",
    this->iRoundOneNeeded,this->iParFileExists);

#endif

#ifdef _DEBUG_EDL
  printf("iMinRowsInPreCalTable=%d,fFirstDesRowOffsetInSrcStart=%f\n",
    this->iMinRowsInPreCalTable,this->fFirstDesRowOffsetInSrcStart);
  printf("iMinColsInPreCalTable=%d,fFirstDesColOffsetInSrcStart=%f\n",
    this->iMinColsInPreCalTable,this->fFirstDesColOffsetInSrcStart);
#endif

  /* Copy local version of passed parameters. */
  iPixelValSrc=iPixelValDst=this->iPixelVals;
  if(this->iDoShrink==TRUE)
    {
    iSearchFlatRegion=UPPER_CONVERSION_MATHOD_SUPER_FASTEST;
  }
  else
    iSearchFlatRegion=this->iSearchFlatRegion;

#ifdef _DEBUG_EDL
  printf("The actual iSearchFlatRegion=%d\n",iSearchFlatRegion);
#endif

  iSearchFlatRegionSizeSrcHoped=this->iSearchFlatRegionSize;
  iFlatRegionVaryingLimitPercentY=this->iFlatRegionVaryingLimitPercentY;

  /* UYV, two bytes for each component. */
  iNumBytesPerPixelSrc=6;
  iNumBytesPerPixelDst=6;

  ucpSrc0=this->ucpSrc0;
  ucpSrc1=this->ucpSrc1;
  ucpDst0=this->ucpDes0;
  ucpDst1=this->ucpDes1;
  ucpFlatRegionMask=this->ucpSrcMask;

  iFrameRowsSrc=this->iNFrameRowsSrc; 
  iFrameRowsDst=this->iNFrameRowsDes; 
  fStartFrameRowSrc=this->fStartFrameRowSrc;
  iInterlacingSrc=this->iInterlacingSrc;
  iInterlacingDst=this->iInterlacingDes;
  iNPixelColsSrc=this->iNPixelColsSrc;
  iNByteColsSrc=iNPixelColsSrc*iNumBytesPerPixelSrc;
  iNPixelColsDst=this->iNPixelColsDes;
  iStartPixelColDst=this->iStartPixelColDes;
  fStartPixelColSrc=this->fStartPixelColSrc;
  fSrcConvertWidth=this->fSrcConvertWidth;
  iDstConvertHeight=this->iDesConvertHeight;
  iDstConvertWidth=this->iDesConvertWidth;

#ifdef _DEBUG_ALLOC
printf ("%s %d allocating %d\n", __FILE__, __LINE__, iFrameRowsDst*sizeof(float*)*4);
#endif
  fppInitialRowTablePos=(float **)malloc(iFrameRowsDst*sizeof(float *));
  ucppLineDst=(unsigned char **)malloc(iFrameRowsDst*sizeof(unsigned char *));
  ucppLineSrcCut=
    (unsigned char **)malloc(iFrameRowsDst*sizeof(unsigned char *));
  ucppLineSrcAdd=
    (unsigned char **)malloc(iFrameRowsDst*sizeof(unsigned char *));
  if(fppInitialRowTablePos==NULL || ucppLineDst==NULL || ucppLineSrcCut==NULL
     || ucppLineSrcAdd==NULL)
    TRACE_0(errout << "Unable to open the par file");

#ifdef _DEBUG_ALLOC
printf ("%s %d allocating %d\n", __FILE__, __LINE__, iNPixelColsDst*sizeof(int));
#endif
  ipSrcByteColCut=(int *)malloc(iNPixelColsDst*sizeof(int));
  if(ipSrcByteColCut==NULL)
    TRACE_0(errout << "Unable to open the par file");

#ifdef _DEBUG_ALLOC
printf ("%s %d allocating %d\n", __FILE__, __LINE__, iNPixelColsDst*sizeof(int)*2);
#endif
  ipTablePixelOffsetSimpleUV=(int *)malloc(iNPixelColsDst*sizeof(int));
  ipTablePixelOffsetLagerUV=(int *)malloc(iNPixelColsDst*sizeof(int));
  if(ipTablePixelOffsetSimpleUV==NULL || ipTablePixelOffsetLagerUV==NULL)
    TRACE_0(errout << "Unable to open the par file");
  fRowFactor=(float)(this->iDesConvertHeight)/(this->fSrcConvertHeight);
  fColFactor=(float)(this->iDesConvertWidth)/(this->fSrcConvertWidth);

  if (fRowFactor < 1. && fColFactor < 1.)
    this->iDoShrink = TRUE;
  else
    this->iDoShrink = FALSE;

#ifdef _DEBUG_EDL
  printf("Computed fRowFactor=%f fColFactor=%f DoShrink=%d\n",
	fRowFactor,fColFactor,this->iDoShrink);
#endif

  if(this->iActive==TRUE)
    {
    BlackOutThisFrame(ucpDst0,ucpDst1,this->iNFrameRowsDes,iNPixelColsDst,
      iInterlacingDst,iPixelValSrc,iPixelValDst,this->iBlackVal0,
	this->iBlackVal1,this->iBlackVal2);
  }

  if(this->iDoShrink==TRUE)
    {
    DetermineLowFilterSize(fRowFactor,&iNumLowPassFilterRows);
    if((iNumLowPassFilterRows & 1)==0)
      ++iNumLowPassFilterRows;
    DetermineLowFilterSize(fColFactor,&iNumLowPassFilterCols);
    if((iNumLowPassFilterCols & 1)==0)
      ++iNumLowPassFilterCols;

#ifdef _DEBUG_EDL
    printf("Down converting filter size is %d rows %d cols\n",
      iNumLowPassFilterRows,iNumLowPassFilterCols);
#endif

    if(this->iDoShrinkFilter==TRUE &&
      (iNumLowPassFilterRows > 1 || iNumLowPassFilterCols>1))
      {
#ifdef _DEBUG_ALLOC
printf ("%s %d allocating %d\n", __FILE__, __LINE__, iNumLowPassFilterRows*
        iNumLowPassFilterCols*sizeof(float));
#endif
      fpLowPassFilerParamters=(float *)malloc(iNumLowPassFilterRows*
        iNumLowPassFilterCols*sizeof(float));
      if(fpLowPassFilerParamters==NULL)
        TRACE_0(errout << "Unable to open the par file");

      Fill2DLowPassFilerParamters(iNumLowPassFilterRows,
        iNumLowPassFilterCols,fRowFactor,fColFactor,
        fpLowPassFilerParamters);
#ifdef _DEBUG_EDL
      printf("Calling LowPassFilterOnThisFrame444\n");
      printf(" with iNumLowPassFilterRows=%d,iNumLowPassFilterCols=%d\n",
        iNumLowPassFilterRows,iNumLowPassFilterCols);
#endif
      LowPassFilterOnThisFrame444(this->ucpSrc0,this->ucpSrc1,
        this->ucpTemp0,this->ucpTemp1,this->iNFrameRowsSrc,
        this->iNPixelColsSrc,this->iInterlacingSrc,
        iNumLowPassFilterRows,iNumLowPassFilterCols,fpLowPassFilerParamters);
      free(fpLowPassFilerParamters);
      ucpSrc0=this->ucpTemp0;
      ucpSrc1=this->ucpTemp1;
    }
  }
  if(iInterlacingSrc)
    iNumBytesToJumpPerRowSrc=iNPixelColsSrc*iNumBytesPerPixelSrc/2;
  else
    iNumBytesToJumpPerRowSrc=iNPixelColsSrc*iNumBytesPerPixelSrc;

  iStartFrameRowDst=this->iStartFrameRowDes;
  iDstInteriorY0=iStartFrameRowDst+4;
  iDstInteriorX0=iStartPixelColDst+4;
  iDstInteriorY1=iStartFrameRowDst+iDstConvertHeight-4;
  iDstInteriorX1=iStartPixelColDst+iDstConvertWidth-4;

#ifdef _DEBUG_EDL
  printf("Destination interior (l,r,t,b)=%d %d %d %d\n",
    iDstInteriorX0,iDstInteriorX1,iDstInteriorY0,iDstInteriorY1);
#endif

  if(iInterlacingDst)
    iNumBytesToJumpPerRowDst=iNPixelColsDst*iNumBytesPerPixelDst/2;
  else
    iNumBytesToJumpPerRowDst=iNPixelColsDst*iNumBytesPerPixelDst;

  if(this->iDoResetEngine==TRUE)
    Initialize();

  /* watch out for rounding errors in these checks */
  if((this->iDoShrink!=TRUE && fRowFactor<1.0 && fColFactor<1.0)
     || fRowFactor<0.001 || fColFactor<0.001
    || (this->iDoShrink==TRUE && (fRowFactor>1.0 || fColFactor>1.0))
    || 
    (int)(fStartFrameRowSrc + ((float)(iDstConvertHeight))/fRowFactor) >
     iFrameRowsSrc
    ||
    (int)(fStartPixelColSrc +fSrcConvertWidth) > iNPixelColsSrc)
    {
    printf("Source out of bound ? %f %f %d %f %f %d\n",
      fStartFrameRowSrc,(((float)(iDstConvertHeight))/fRowFactor),
      iFrameRowsSrc,
      fStartPixelColSrc,fSrcConvertWidth,
      iNPixelColsSrc);
    printf("fRowFactor=%f, fColFactor=%f iDoShrink=%d\n",fRowFactor,fColFactor,
      this->iDoShrink);
    printf("fStartPixelColSrc=%f\n",fStartPixelColSrc);
    TRACE_0(errout << "Unable to open the par file");
  }

  ucpResult0=ucpDst0;
  ucpResult1=ucpDst1 - iNPixelColsDst*iNumBytesPerPixelDst/2;
  ucpSrc1_corrected=ucpSrc1-iNPixelColsSrc*iNumBytesPerPixelSrc/2;

  iTotalRound=2;

  if(iSearchFlatRegion==UPPER_CONVERSION_MATHOD_MEDIAN ||
     iSearchFlatRegion==UPPER_CONVERSION_MATHOD_FASTEST)
    {
    /* First, determine the size of search region in destination. */
    fMeanFactor=(fRowFactor+fColFactor)/2.0;
    iSearchFlatRegionSizeDst=(int)(fMeanFactor
      *(float)(iSearchFlatRegionSizeSrcHoped-6)+0.5);
    /* Go to the next 2 to power. */
    iSearchFlatRegionSizeDstPower2=0;
    while((1<<iSearchFlatRegionSizeDstPower2)<=iSearchFlatRegionSizeDst)
      iSearchFlatRegionSizeDstPower2++;
    iSearchFlatRegionSizeDst=1<<iSearchFlatRegionSizeDstPower2;
    iNumDstRegionRows=iFrameRowsDst/iSearchFlatRegionSizeDst;
    if(iFrameRowsDst%iSearchFlatRegionSizeDst>0)
      iNumDstRegionRows++;
    iNumDstRegionCols=iNPixelColsDst/iSearchFlatRegionSizeDst;
    if(iNPixelColsDst%iSearchFlatRegionSizeDst>0)
      iNumDstRegionCols++;

    iSearchFlatRegionSizeSrcActualRow=
      (int)((float)iSearchFlatRegionSizeDst/fRowFactor+0.5)+6;
    iSearchFlatRegionSizeSrcActualCol=
      (int)((float)iSearchFlatRegionSizeDst/fColFactor+0.5)+6;

#ifdef _DEBUG_EDL
    printf("YUV444 iSearchFlatRegionSizeDst=%d\n",iSearchFlatRegionSizeDst);
    printf("YUV444 iNumDstRegionRows,Cols=%d %d\n",
      iNumDstRegionRows,iNumDstRegionCols);
    printf("YUV444 iSearchFlatRegionSizeSrcActualRow,Col=%d %d\n",
      iSearchFlatRegionSizeSrcActualRow,
      iSearchFlatRegionSizeSrcActualCol);
#endif

    /* For each destination region, determine its source is flat or
       not. */
    if(iSearchFlatRegion==UPPER_CONVERSION_MATHOD_MEDIAN)
      {
      for(iR=0;iR<iNumDstRegionRows;++iR)
        {
        iRegionRow0=iDstInteriorY0+(iR<<iSearchFlatRegionSizeDstPower2);
        fTemp=((float)(iRegionRow0-iStartFrameRowDst))/fRowFactor
          +fStartFrameRowSrc;
        iSrcRowCut=(int)(fTemp)-3;
        iSrcRowAdd=iSrcRowCut+iSearchFlatRegionSizeSrcActualRow;
        if(iSrcRowCut>=0 && iSrcRowAdd<=iFrameRowsSrc)
	{
        for(iC=0;iC<iNumDstRegionCols;++iC)
    	  {
          iRegionCol0=iDstInteriorX0+(iC<<iSearchFlatRegionSizeDstPower2);
          fTemp=((float)(iRegionCol0-iStartPixelColDst))/fColFactor
            +fStartPixelColSrc;
          iSrcPixelColCut=(int)fTemp-3;
          iSrcPixelColAdd=iSrcPixelColCut+iSearchFlatRegionSizeSrcActualCol;
          iRegionMaxY=iRegionMaxU=iRegionMaxV=0;
          iRegionMinY=iRegionMinU=iRegionMinV=65536;
          if(iSrcPixelColCut>=0 && iSrcPixelColAdd<=iNPixelColsSrc)
          {
          for(iRow=iSrcRowCut;iRow<iSrcRowAdd && iRow<iFrameRowsSrc;++iRow)
    	    {
            if(iInterlacingSrc)
              ucpUV00=(iRow & 1) ? ucpSrc1_corrected : ucpSrc0;
            else
              ucpUV00=ucpSrc0;
            ucpUV00+=iNumBytesToJumpPerRowSrc*iRow;
            ucpUV00+=iSrcPixelColCut*iNumBytesPerPixelSrc;
            for(iPixel=iSrcPixelColCut;iPixel<iSrcPixelColAdd && 
              iPixel<iNPixelColsSrc;++iPixel)
    	      {
              iY=*((unsigned short *)(ucpUV00));
              iU=*((unsigned short *)(ucpUV00+2));
              iV=*((unsigned short *)(ucpUV00+4));
              if(iY>iRegionMaxY)
                iRegionMaxY=iY;
              else if(iY<iRegionMinY)
                iRegionMinY=iY;
              if(iU>iRegionMaxU)
                iRegionMaxU=iU;
              else if(iV<iRegionMinV)
                iRegionMinY=iV;
              ucpUV00+=iNumBytesPerPixelSrc;
    	    }
    	  }
          }
          if((iRegionMaxY-iRegionMinY)*100/(iRegionMaxY+iRegionMinY+1)
             <iFlatRegionVaryingLimitPercentY &&
             (iRegionMaxU-iRegionMinU)*100/(iRegionMaxU+iRegionMinU+1)
             <iFlatRegionVaryingLimitPercentY &&
             (iRegionMaxV-iRegionMinV)*100/(iRegionMaxV+iRegionMinV+1)
             <iFlatRegionVaryingLimitPercentY)
	    {
#ifdef _DEBUG_EDL
            ++iNumFlatRegions;
#endif
            *(ucpFlatRegionMask+iR*iNumDstRegionCols+iC)=1;
          }
          else
	    {
#ifdef _DEBUG_EDL
            iNumNonFlatRegions++;
#endif
            *(ucpFlatRegionMask+iR*iNumDstRegionCols+iC)=0;
          }
	}
        }
      }
    } /* if(iSearchFlatRegion==UPPER_CONVERSION_MATHOD_MEDIAN) */
    else
      {
      /* Go through regions. */
      for(iR=0;iR<iNumDstRegionRows;++iR)
        {
        iRegionRow0=iDstInteriorY0+(iR<<iSearchFlatRegionSizeDstPower2);
        fTemp=((float)(iRegionRow0-iStartFrameRowDst))/fRowFactor
          +fStartFrameRowSrc;
        iSrcRowCut=(int)(fTemp)-3;
        iSrcRowAdd=iSrcRowCut+iSearchFlatRegionSizeSrcActualRow;
        if(iSrcRowCut>=0 && iSrcRowAdd<=iFrameRowsSrc)
	{
        for(iC=0;iC<iNumDstRegionCols;++iC)
    	  {
          iRegionCol0=iDstInteriorX0+(iC<<iSearchFlatRegionSizeDstPower2);
          fTemp=((float)(iRegionCol0-iStartPixelColDst))/fColFactor
            +fStartPixelColSrc;
          iSrcPixelColCut=(int)fTemp-3;
          iSrcPixelColAdd=iSrcPixelColCut+iSearchFlatRegionSizeSrcActualCol;
          iRegionMaxY=0;
          iRegionMinY=65536;
          if(iSrcPixelColCut>=0 && iSrcPixelColAdd<=iNPixelColsSrc)
          {
          /* Now we fixed one region. Go through every pixel in the region. */
          for(iRow=iSrcRowCut;iRow<iSrcRowAdd;++iRow)
    	    {
            if(iInterlacingSrc)
              ucpUV00=(iRow & 1) ? ucpSrc1_corrected : ucpSrc0;
            else
              ucpUV00=ucpSrc0;
            ucpUV00+=iNumBytesToJumpPerRowSrc*iRow;
            ucpUV00+=iSrcPixelColCut*iNumBytesPerPixelSrc;
            for(iPixel=iSrcPixelColCut;iPixel<iSrcPixelColAdd;++iPixel)
    	      {
              iY=*((unsigned short *)(ucpUV00));
              if(iY>iRegionMaxY)
                iRegionMaxY=iY;
              else if(iY<iRegionMinY)
                iRegionMinY=iY;
              ucpUV00+=iNumBytesPerPixelSrc;
   	    }
    	  }
          }
          if((iRegionMaxY-iRegionMinY)*100/(iRegionMaxY+iRegionMinY+1)
             <iFlatRegionVaryingLimitPercentY)
	    {
#ifdef _DEBUG_EDL
            ++iNumFlatRegions;
#endif
            *(ucpFlatRegionMask+iR*iNumDstRegionCols+iC)=1;
          }
          else
	    {
#ifdef _DEBUG_EDL
            iNumNonFlatRegions++;
#endif
            *(ucpFlatRegionMask+iR*iNumDstRegionCols+iC)=0;
          }
	}
        }
      }
    }
#ifdef _DEBUG_EDL
    printf("Destination image has %d flat regions, %d varying regions.\n",
      iNumFlatRegions,iNumNonFlatRegions);
#endif
  }

  for(iRound=0;iRound<iTotalRound;++iRound)
    {
    if(iRound==0)
      {
      /* If the source box is changed, force it to reload. */
      if(this->iRoundOneNeeded==FALSE)
        goto next_round;
      else
        {
        if(LoadParFile()==TRUE)
    	  {
#ifdef _DEBUG_EDL
          printf("Using existing parameter files for up conversion.\n");
#endif
          this->iParFileExists=TRUE;
          this->iRoundOneNeeded=FALSE;
          goto next_round;
    	}
        else
    	  {
#ifdef _DEBUG_EDL
          printf("Generating new parameter files for up conversion.\n");
#endif
          this->iParFileExists=FALSE;
          this->iRoundOneNeeded=FALSE;
        }
      }
    }

    iRowLoop0=iStartFrameRowDst;

    if(iRound==0 && this->iParFileExists==FALSE)
      iRowLoop1=iRowLoop0+this->iMinRowsInPreCalTable;
    else
      iRowLoop1=iRowLoop0+iDstConvertHeight;

    iPixelLoop0=iStartPixelColDst;

    if(iRound==0 && this->iParFileExists==FALSE)
      iPixelLoop1=iPixelLoop0+this->iMinColsInPreCalTable;
    else
      iPixelLoop1=iPixelLoop0+iDstConvertWidth;

    if(iRound==0)
      {
      for(iRow=iRowLoop0;iRow<iRowLoop1;++iRow)
        {
        FIND_ROW_ADDRESSES_PART_1;
        FIND_ROW_ADDRESSES_PART_2;
   
        ucpUYorVYDst=ucpLineDst+iNumBytesPerPixelDst*iPixelLoop0;
        for(iPixel=iPixelLoop0;iPixel<iPixelLoop1;iPixel++)
   	  {
          dTemp=((double)(iPixel-iStartPixelColDst))/(double)fColFactor
            +(double)fStartPixelColSrc;
          fSrcPixelLocation=(float)(dTemp+DOUBLE_TO_FLOAT_ROUNDING_OFFSET);
          iSrcPixelColCut=((int)fSrcPixelLocation);
          fColFraction=fSrcPixelLocation-(float)iSrcPixelColCut;
          iSrcByteColCut=iSrcPixelColCut*iNumBytesPerPixelSrc;
          /* iSrcByteColCut is not guaranteed to be at UV's position. */
          ucpUYorVYSrc00=ucpLineSrcCut+iSrcByteColCut;
          ucpUYorVYSrc10=ucpLineSrcAdd+iSrcByteColCut;
          fpPreCalPointer=fpInitialRowTablePos
            +((iPixel-iPixelLoop0)%this->iMinColsInPreCalTable)*
            PRE_TABLE_ENTRY_SIZE_IN_FLOATS;
          Generic2dSplineForUYorVY(fpPreCalPointer,
            fpPreCalPointer+16,
            fRowFraction,fColFraction,fColFraction,
            NEIGHBOR_TYPE_4X4_OVELAP_FOR_Y);
          /* Simple neighbor will be used for border pixels. */
          Generic2dSplineForUYorVY(fpPreCalPointer+48,
            fpPreCalPointer+52,
            fRowFraction,fColFraction,fColFraction,
            NEIGHBOR_TYPE_2x2); 
          ucpUYorVYDst+=iNumBytesPerPixelDst;
        } /* iPixel */
      } /* iRow */
      if(iRound==0 && this->iParFileExists==FALSE)
	{
        SaveParFile();
        this->iParFileExists=TRUE;
      }
    } /* if(iRound==0) */
    else
      {
      /* Real computing to generate data. */
      /* First, go through all borders. */
      for(iRow=iRowLoop0;iRow<iDstInteriorY0;++iRow) /* Top */
	{
        FIND_ROW_ADDRESSES_PART_1;
        FIND_ROW_ADDRESSES_PART_2;
        ucpUYorVYDst=ucpLineDst+iNumBytesPerPixelDst*iPixelLoop0;
        for(iPixel=iPixelLoop0;iPixel<iPixelLoop1;iPixel++)
          {
          SET_PIXELWISE_LOCATION_VARIBLES_444;
          ipTablePixelOffsetLagerUV[iPixel]
            =((iPixel-iPixelLoop0)%this->iMinColsInPreCalTable)
            *PRE_TABLE_ENTRY_SIZE_IN_FLOATS;
          ipTablePixelOffsetSimpleUV[iPixel]=
            ipTablePixelOffsetLagerUV[iPixel]+48;
          fpPreCalPointerY=fpInitialRowTablePos+
            ipTablePixelOffsetSimpleUV[iPixel]+4;
          SET_SIMPLE_VALUES_Y_444;
          SET_SIMPLE_VALUES_Y_444;
          SET_SIMPLE_VALUES_Y_444;
	}
      }
      for(iRow=iDstInteriorY1;iRow<iRowLoop1;++iRow) /* Bottom */
	{
        FIND_ROW_ADDRESSES_PART_1;
        FIND_ROW_ADDRESSES_PART_2;
        ucpUYorVYDst=ucpLineDst+iNumBytesPerPixelDst*iPixelLoop0;
        for(iPixel=iPixelLoop0;iPixel<iPixelLoop1;iPixel++)
          {
          SET_PIXEL_POINTERS_444;
          fpPreCalPointerY=fpInitialRowTablePos
            +ipTablePixelOffsetSimpleUV[iPixel]+4;
          SET_SIMPLE_VALUES_Y_444;
          SET_SIMPLE_VALUES_Y_444;
          SET_SIMPLE_VALUES_Y_444;
	}
      }
      for(iRow=iRowLoop0;iRow<iRowLoop1;++iRow) /* Left. */
	{
        FIND_ROW_ADDRESSES_PART_1;
        FIND_ROW_ADDRESSES_PART_2;
        ucpUYorVYDst=ucpLineDst+iNumBytesPerPixelDst*iPixelLoop0;
        for(iPixel=iPixelLoop0;iPixel<iDstInteriorX0;iPixel++)
          {
          SET_PIXEL_POINTERS_444;
          fpPreCalPointerY=fpInitialRowTablePos
            +ipTablePixelOffsetSimpleUV[iPixel]+4;
          SET_SIMPLE_VALUES_Y_444;
          SET_SIMPLE_VALUES_Y_444;
          SET_SIMPLE_VALUES_Y_444;
	}
      }
      for(iRow=iRowLoop0;iRow<iRowLoop1;++iRow) /* Right */
	{
        FIND_ROW_ADDRESSES_PART_LOAD_ONLY;
        ucpUYorVYDst=ucpLineDst+iNumBytesPerPixelDst*iDstInteriorX1;
        for(iPixel=iDstInteriorX1;iPixel<iPixelLoop1;iPixel++)
          {
          SET_PIXEL_POINTERS_444;
          fpPreCalPointerY=fpInitialRowTablePos
            +ipTablePixelOffsetSimpleUV[iPixel]+4;
          SET_SIMPLE_VALUES_Y_444;
          SET_SIMPLE_VALUES_Y_444;
          SET_SIMPLE_VALUES_Y_444;
	}
      }

      /* Go into ineterior */
      switch(iSearchFlatRegion)
        {
        case UPPER_CONVERSION_MATHOD_SUPER_FASTEST:
          /* Y uses 4 neighbors, UV uses 4 neighbors. */
          for(iRow=iDstInteriorY0;iRow<iDstInteriorY1;++iRow)
	    {
            FIND_ROW_ADDRESSES_PART_LOAD_ONLY;
            ucpUYorVYDst=ucpLineDst+iNumBytesPerPixelDst*iDstInteriorX0;
            for(iPixel=iDstInteriorX0;iPixel<iDstInteriorX1;iPixel++)
              {
              SET_PIXEL_POINTERS_444;
              fpPreCalPointerY=fpInitialRowTablePos
                +ipTablePixelOffsetSimpleUV[iPixel]+4;
              SET_SIMPLE_VALUES_Y_444;
              SET_SIMPLE_VALUES_Y_444;
              SET_SIMPLE_VALUES_Y_444;
	    }
          }
          break;
        case UPPER_CONVERSION_MATHOD_BEST:
          /* Y uses 32 neighbors, UV uses 32 neighbors. */
          for(iRow=iDstInteriorY0;iRow<iDstInteriorY1;++iRow)
	    {
            FIND_ROW_ADDRESSES_PART_LOAD_ONLY;
            ucpUYorVYDst=ucpLineDst+iNumBytesPerPixelDst*iDstInteriorX0;
            for(iPixel=iDstInteriorX0;iPixel<iDstInteriorX1;iPixel++)
              {
              SET_PIXEL_POINTERS_444;
              fpPreCalPointerY=fpInitialRowTablePos
                +ipTablePixelOffsetLagerUV[iPixel]+16;
              SET_LARGER_NEIGHBORED_VALUES_Y_444;
              SET_LARGER_NEIGHBORED_VALUES_Y_444;
              SET_LARGER_NEIGHBORED_VALUES_Y_444;
	    }
          }
          break;
        case UPPER_CONVERSION_MATHOD_MEDIAN:
        case UPPER_CONVERSION_MATHOD_FASTEST:
          /* Y uses 32 or 4 neighbors, UV uses 32 or 4 neighbors,
             depending on the flatness of the region. */
          DO_REGIONIZED_UPPER_CONVERSION_PART_FLAT_444
              else /* This is a varying region. */
                {
                for(iRow=iRegionRow0;iRow<iRegionRow1;++iRow)
                  {
                  FIND_ROW_ADDRESSES_PART_LOAD_ONLY;
                  ucpUYorVYDst=ucpLineDst+iNumBytesPerPixelDst*iRegionCol0;
                  for(iPixel=iRegionCol0;iPixel<iRegionCol1;iPixel++)
                    {
                    SET_PIXEL_POINTERS_444;
                    fpPreCalPointerY=fpInitialRowTablePos
                      +ipTablePixelOffsetLagerUV[iPixel]+16;
                    SET_LARGER_NEIGHBORED_VALUES_Y_444;
                    SET_LARGER_NEIGHBORED_VALUES_Y_444;
                    SET_LARGER_NEIGHBORED_VALUES_Y_444;
		  }
		}  
	      }
	    }
          }
          break;
        default:
          TRACE_0(errout << "Unable to open the par file");
      }
    } /* if(iRound==1) */
next_round:
  ;
  } /* for(iRound=0;iRunnd<iTotalRound;++iRunnd) */
  if(fppInitialRowTablePos!=NULL)
    free(fppInitialRowTablePos);
  if(ucppLineDst!=NULL)
    free(ucppLineDst);
  if(ucppLineSrcCut!=NULL)
    free(ucppLineSrcCut);
  if(ucppLineSrcAdd!=NULL)
    free(ucppLineSrcAdd);
  if(ipSrcByteColCut!=NULL)
    free(ipSrcByteColCut);
  if(ipTablePixelOffsetSimpleUV!=NULL)
    free(ipTablePixelOffsetSimpleUV);
  if(ipTablePixelOffsetLagerUV!=NULL)
    free(ipTablePixelOffsetLagerUV);

#ifdef _DEBUG_EDL
  printf("MagnifyThisFrame444() exiting\n");
#endif

}

/** */
int MAGNIFY_CONTROL::Allocate()
/*
	Allocate all intermediate storage used by conversion
*/
{
  int iTempSizeRow, iTempSizeCol;

  if (this->ucpTemp0==NULL)
   {
    iTempSizeRow=(this->iNFrameRowsDes > this->iNFrameRowsSrc) ? 
      this->iNFrameRowsDes :  this->iNFrameRowsSrc;
    iTempSizeCol=(this->iNPixelColsDes > this->iNPixelColsSrc) ? 
      this->iNPixelColsDes :  this->iNPixelColsSrc;
#ifdef _DEBUG_ALLOC
printf ("%s %d allocating %d\n", __FILE__, __LINE__, iTempSizeRow*iTempSizeCol*8*3);
#endif
    this->ucpTemp0=(unsigned char *)malloc(iTempSizeRow*iTempSizeCol*8);
    if(this->ucpTemp0==NULL)
      return 1;
    this->ucpTemp1=(unsigned char *)malloc(iTempSizeRow*iTempSizeCol*8);
    if(this->ucpTemp1==NULL)
      return 1;
    this->ucpSrcMask=(unsigned char *)malloc(iTempSizeRow*iTempSizeCol*8);
    if(this->ucpSrcMask==NULL)
      return 1;
   }

  return 0;
}  /* Allocate */

void MAGNIFY_CONTROL::ConformDimension (int iForConform)
{
  // These were moved to the MAGNIFY_CONTROL struct
  //static float fOrigRow, fOrigCol, fOrigHeight, fOrigWidth;
  //static int iOrigRow, iOrigCol, iOrigHeight, iOrigWidth;
  float fSrcRow0, fSrcCol0, fSrcRow1, fSrcCol1;
  int iDstRow0, iDstCol0, iDstRow1, iDstCol1;
  float fNewRow, fNewCol, fNewHeight, fNewWidth;
  int iNewRow, iNewCol, iNewHeight, iNewWidth;
  float fFactor, fN;
  int iN, iSrcRow0Tmp, iSrcCol0Tmp, iSrcRow1Tmp, iSrcCol1Tmp;

  if (iForConform)
   {
    fOrigRow = this->fStartFrameRowSrc;
    fOrigCol = this->fStartPixelColSrc;
    fOrigHeight = this->fSrcConvertHeight;
    fOrigWidth = this->fSrcConvertWidth;

    iOrigRow = this->iStartFrameRowDes;
    iOrigCol = this->iStartPixelColDes;
    iOrigHeight = this->iDesConvertHeight;
    iOrigWidth = this->iDesConvertWidth;

    fSrcRow0 = fOrigRow;
    fSrcCol0 = fOrigCol;
    fSrcRow1 = fSrcRow0 + fOrigHeight;
    fSrcCol1 = fSrcCol0 + fOrigWidth;

    iDstRow0 = iOrigRow;
    iDstCol0 = iOrigCol;
    iDstRow1 = iDstRow0 + iOrigHeight;
    iDstCol1 = iDstCol0 + iOrigWidth;

/*
	Is the source box out of bounds?
*/

    if (fSrcRow0 < 0.)
     {
/*
	Find the smallest offset, X, such that

	fSrcRow0 + X >= 0.       AND
	fDstRow0 + X*(fDstRow1-fDstRow0)/(fSrcRow1-fSrcRow0) is an integer.

	The second condition enforces the destination box remains integer
	valued.

	Thus, the equation we use is
	
	let N = the integer X*(fDstRow1-fDstRow0)/(fSrcRow1-fSrcRow0).
	So, N >= -fSrcRow0 * (fDstRow1-fDstRow0)/(fSrcRow1-fSrcRow0)
*/
      fFactor = (float)(iDstRow1-iDstRow0)/(fSrcRow1-fSrcRow0);
      fN = -fSrcRow0 * fFactor;
      iN = (int)fN;
      if (fN > iN)
        iN++;
      
      fSrcRow0 += (float)iN / fFactor;
      iDstRow0 += iN;
     }

    if (fSrcRow1 > this->iNFrameRowsSrc)
     {
/*
	Find the smallest offset, X, such that

	fSrcRow1 - X <= iNFrameRowsSrc       AND
	fDstRow1 - X*(fDstRow1-fDstRow0)/(fSrcRow1-fSrcRow0) is an integer.

	The second condition enforces the destination box remains integer
	valued.

	Thus, the equation we use is
	
	let N = the integer X*(fDstRow1-fDstRow0)/(fSrcRow1-fSrcRow0).
	So, N >= (fSrcRow1 - iNFrameRowsSrc) *
			(fDstRow1-fDstRow0)/(fSrcRow1-fSrcRow0)
*/
      fFactor = (float)(iDstRow1-iDstRow0)/(fSrcRow1-fSrcRow0);
      fN = (fSrcRow1 - (float)this->iNFrameRowsSrc) * fFactor;
      iN = (int)fN;
      if (fN > iN)
        iN++;
      
      fSrcRow1 -= (float)iN / fFactor;
      iDstRow1 -= iN;
     }

    if (fSrcCol0 < 0.)
     {
/*
	Find the smallest offset, X, such that

	fSrcCol0 + X >= 0.       AND
	fDstCol0 + X*(fDstCol1-fDstCol0)/(fSrcCol1-fSrcCol0) is an integer.

	The second condition enforces the destination box remains integer
	valued.

	Thus, the equation we use is
	
	let N = the integer X*(fDstCol1-fDstCol0)/(fSrcCol1-fSrcCol0).
	So, N >= -fSrcCol0 * (fDstCol1-fDstCol0)/(fSrcCol1-fSrcCol0)
*/
      fFactor = (float)(iDstCol1-iDstCol0)/(fSrcCol1-fSrcCol0);
      fN = -fSrcCol0 * fFactor;
      iN = (int)fN;
      if (fN > iN)
        iN++;
      
      fSrcCol0 += (float)iN / fFactor;
      iDstCol0 += iN;
     }

    if (fSrcCol1 > this->iNPixelColsSrc)
     {
/*
	Find the smallest offset, X, such that

	fSrcCol1 - X <= iNPixelColsSrc       AND
	fDstCol1 - X*(fDstCol1-fDstCol0)/(fSrcCol1-fSrcCol0) is an integer.

	The second condition enforces the destination box remains integer
	valued.

	Thus, the equation we use is
	
	let N = the integer X*(fDstCol1-fDstCol0)/(fSrcCol1-fSrcCol0).
	So, N >= (fSrcCol1 - iNPixelColsSrc) *
			(fDstCol1-fDstCol0)/(fSrcCol1-fSrcCol0)
*/
      fFactor = (float)(iDstCol1-iDstCol0)/(fSrcCol1-fSrcCol0);
      fN = (fSrcCol1 - (float)this->iNPixelColsSrc) * fFactor;
      iN = (int)fN;
      if (fN > iN)
        iN++;
      
      fSrcCol1 -= (float)iN / fFactor;
      iDstCol1 -= iN;
     }

/*
	Is the destination box out of bounds?
*/

    if (iDstRow0 < 0)
     {
      fFactor = (fSrcRow1-fSrcRow0) / (float)(iDstRow1-iDstRow0);
      fSrcRow0 += (float)(0-iDstRow0) * fFactor;
      iDstRow0 = 0;
     }

    if (iDstRow1 > this->iNFrameRowsDes)
     {
      fFactor = (fSrcRow1-fSrcRow0) / (float)(iDstRow1-iDstRow0);
      fSrcRow1 += (float)(this->iNFrameRowsDes - iDstRow1) * fFactor;
      iDstRow1 = this->iNFrameRowsDes;
     }

    if (iDstCol0 < 0)
     {
      fFactor = (fSrcCol1-fSrcCol0) / (float)(iDstCol1-iDstCol0);
      fSrcCol0 += (float)(0-iDstCol0) * fFactor;
      iDstCol0 = 0;
     }

    if (iDstCol1 > this->iNPixelColsDes)
     {
      fFactor = (fSrcCol1-fSrcCol0) / (float)(iDstCol1-iDstCol0);
      fSrcCol1 += (float)(this->iNPixelColsDes - iDstCol1) * fFactor;
      iDstCol1 = this->iNPixelColsDes;
     }

/*
	Set the new values of the destination
*/

    iNewRow = iDstRow0;
    iNewCol = iDstCol0;
    iNewHeight = iDstRow1 - iDstRow0;
    iNewWidth = iDstCol1 - iDstCol0;

    this->iStartFrameRowDes = iNewRow;
    this->iStartPixelColDes = iNewCol;
    this->iDesConvertHeight = iNewHeight;
    this->iDesConvertWidth = iNewWidth;

/*
	Enforce the accuracy value on the source dimension
*/

    iSrcRow0Tmp = fSrcRow0 * FC_ALLOWED_OFFSET_ACCURACY + 0.5;
    iSrcCol0Tmp = fSrcCol0 * FC_ALLOWED_OFFSET_ACCURACY + 0.5;
    iSrcRow1Tmp = fSrcRow1 * FC_ALLOWED_OFFSET_ACCURACY + 0.5;
    iSrcCol1Tmp = fSrcCol1 * FC_ALLOWED_OFFSET_ACCURACY + 0.5;

    fSrcRow0 = (float)iSrcRow0Tmp / FC_ALLOWED_OFFSET_ACCURACY;
    fSrcCol0 = (float)iSrcCol0Tmp / FC_ALLOWED_OFFSET_ACCURACY;
    fSrcRow1 = (float)iSrcRow1Tmp / FC_ALLOWED_OFFSET_ACCURACY;
    fSrcCol1 = (float)iSrcCol1Tmp / FC_ALLOWED_OFFSET_ACCURACY;

/*
	The format conversion  code uses look up tables to make the
	computation faster.

	ComputePreCalTableSize() will change the dimensions of the
	source image inorder to control the size of the LUTs
*/

    fNewRow = fSrcRow0;
    fNewCol = fSrcCol0;
    fNewHeight = fSrcRow1 - fSrcRow0;
    fNewWidth = fSrcCol1 - fSrcCol0;

    ComputePreCalTableSize(&fNewRow, &fNewHeight,
	this->iDesConvertHeight,&this->iMinRowsInPreCalTable,
	&this->fFirstDesRowOffsetInSrcStart);

    ComputePreCalTableSize(&fNewCol, &fNewWidth,
	this->iDesConvertWidth,&this->iMinColsInPreCalTable,
	&this->fFirstDesColOffsetInSrcStart);

    if (fNewRow < 0.)
      fNewRow = 0.;
    if (fNewCol < 0.)
      fNewCol = 0.;

    if (fNewRow + fNewHeight > this->iNFrameRowsSrc)
      fNewHeight = this->iNFrameRowsSrc - fNewRow;

    if (fNewCol + fNewWidth > this->iNPixelColsSrc)
      fNewWidth = this->iNPixelColsSrc - fNewCol;

    this->fStartFrameRowSrc = fNewRow;
    this->fStartPixelColSrc = fNewCol;
    this->fSrcConvertHeight = fNewHeight;
    this->fSrcConvertWidth = fNewWidth;
   }
  else
   {
/*

	Put back the original box dimensions
*/

    this->fStartFrameRowSrc = fOrigRow;
    this->fStartPixelColSrc = fOrigCol;
    this->fSrcConvertHeight = fOrigHeight;
    this->fSrcConvertWidth = fOrigWidth;

    this->iStartFrameRowDes = iOrigRow;
    this->iStartPixelColDes = iOrigCol;
    this->iDesConvertHeight = iOrigHeight;
    this->iDesConvertWidth = iOrigWidth;
   }

  return;
}  /* ConformDimension */

/** */
void MAGNIFY_CONTROL::MagnifyThisFrame()
{

  if (Allocate())
   {
    TRACE_0(errout << "Unable to open the par file");
   }

	// make boxes consistent with this algorithm
  ConformDimension (TRUE);

  // Resetting the engine must occur each time the parameters change
  char caFileNameLocal[256];
  MakeParFilename(caFileNameLocal);
  if (strcmp (caFileNameLocal, this->caParFileName) == 0)
   {
    // same parameters, no need to reset engine
    this->iDoResetEngine = FALSE;
   }
  else
   {
    // different parameters, must reset engine
    this->iDoResetEngine = TRUE;
    strcpy (this->caParFileName, caFileNameLocal);
   }
   
  if(this->iPixelVals == PIXELVALS_YUV_444)
    MagnifyThisFrameYuv444();
  else
    MagnifyThisFrameYuv422();

  // put back original boxes
  ConformDimension (FALSE);

  // clip to the allowed range
  ClipToRange ();
}

void MAGNIFY_CONTROL::ClipToRange ()
{
  long lRow, lCol;
  MTI_UINT16 *usp;

  for (lRow = 0; lRow < iNFrameRowsDes; lRow++)
   {
    usp = (MTI_UINT16 *)ucpDes0 + (lRow * iNPixelColsDes * 3);

    for (lCol = 0; lCol < iNPixelColsDes; lCol++)
     {
      // component 0
      if (*usp < iMinVal0)
        *usp = iMinVal0;
      else if (*usp > iMaxVal0)
        *usp = iMaxVal0;
      usp++;

      // component 1
      if (*usp < iMinVal1)
        *usp = iMinVal1;
      else if (*usp > iMaxVal1)
        *usp = iMaxVal1;
      usp++;

      // component 2
      if (*usp < iMinVal2)
        *usp = iMinVal2;
      else if (*usp > iMaxVal2)
        *usp = iMaxVal2;
      usp++;
     }
   }

  return;
}  /* ClipToRange */

/** */
void MAGNIFY_CONTROL::Init ()
/*
	This function sets the parameters to a resonable value
*/
{
  this->iSize = 0;
  this->iDoResetEngine = TRUE;

		// SearchFlatRegion = 2 is slow.  3 is fast.
  this->iSearchFlatRegion = 2;
  this->iSearchFlatRegionSize = 16;
  this->iFlatRegionVaryingLimitPercentY = 20;
  this->iFlatRegionVaryingLimitPercentUV = 5;
  this->iActive = TRUE;
  this->iDoShrink = FALSE;
  this->iDoShrinkFilter = TRUE;
  this->fStartFrameRowSrc=0.0;
  this->fStartPixelColSrc=0.0;
  this->iStartFrameRowDes=0;
  this->iStartPixelColDes=0;
  this->fSrcConvertHeight=0.0;
  this->fSrcConvertWidth=0.0;
  this->iDesConvertHeight=0;
  this->iDesConvertWidth=0;
  this->iInterlacingSrc=FALSE;
  this->iInterlacingDes=FALSE;
  this->iPixelVals=0;
  this->iNFrameRowsSrc=0;
  this->iNPixelColsSrc=0;
  this->iNFrameRowsDes=0;
  this->iNPixelColsDes=0;
  this->iMinRowsInPreCalTable=20;
  this->iMinColsInPreCalTable=8;

  this->fFirstDesRowOffsetInSrcStart=0.0;
  this->fFirstDesColOffsetInSrcStart=0.0;

  this->strParfileDir = "";

  this->ucpSrc0=NULL;
  this->ucpSrc1=NULL;
  this->ucpDes0=NULL;
  this->ucpDes1=NULL;
  this->ucpTemp0=NULL;
  this->ucpTemp1=NULL;
  this->ucpSrcMask=NULL;

  this->iRoundOneNeeded = FALSE;
  this->iParFileExists = FALSE;

  this->caParFileName[0] = 0;

  this->fpTotalPreCal = NULL;

  this->vpCriticalSectionParFileIO = NULL;

  this->iTableIsMade = FALSE;
  this->iInverseMatrixComputed = FALSE;

  return;
}  /* Init */

