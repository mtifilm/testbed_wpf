//
// include file for class CExtractor
//
#ifndef CBLITTER_H
#define CBLITTER_H

#include "machine.h"
#include "imgToolDLL.h"

////////////////////////////////////////////////////////////

class CImageFormat;

class MTI_IMGTOOLDLL_API CBlitter
{

public:

   CBlitter();

   ~CBlitter();

   // all three surfaces have the same dims
   void setSurfaceDimensions(int w, int h, int pitch);
   void setSurfaceDimensions(const CImageFormat&);

   // set the main surface (target for BLT's)
   void setMainSurface(MTI_UINT16 *);
 
   // set the restore surface (copy of orig frame)
   void setRestoreSurface(MTI_UINT16 *);

   // set the surface containing the fix data
   void setDataSurface(MTI_UINT16 *);

   // copies main surface to restore surface
   void initRestoreSurface();

   // BLT's from data (DRS fix) surface to main
   // surface after restoring pixels at the CURRENT
   // position of the BLTed fix
   void bltDataToMainSurface(int xdst, int ydst,
                        RECT *srcrct);

   // same as above but BLT only those pixels
   // having a corresponding mask value equal to
   // 'mskenbl'
   void bltDataToMainSurface(int xdst, int ydst,
                        RECT *srcrct,
                        MTI_UINT16 *mask,
                        MTI_UINT16 mskenbl);

private:

   int surfWdth; // width  in pixels of surfaces
   int surfHght; // height in pixels of surfaces
   int surfPitch; // pitch in SHORT INTEGERS (mult of 3)

   MTI_UINT16 *mainSurface;
   MTI_UINT16 *restoreSurface;
   MTI_UINT16 *dataSurface;

   bool dataIsOnMainSurface;

   RECT curRect;

};
#endif



