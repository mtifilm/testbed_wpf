
#ifndef CONVERT_COMMON_H
#define CONVERT_COMMON_H

struct COLENTRY
{
   int offs;

   int phase;
};

struct ROWENTRY
{
   int fld;

   int offs;

   int phase;
};

#endif
