// BufferPool.cpp
//
// implementation file for class CBufferPool
//
//////////////////////////////////////////////////////////////////////

#include "BufferPool.h"

#include "err_imgtool.h"
#include "IniFile.h"
#include "bthread.h"
#include "machine.h"
#include "MTImalloc.h"
#include "MtiCudaCoreInterface.h"
#include <iomanip>

//////////////////////////////////////////////////////////////
//
//                  C B u f f e r P o o l
//
//        Author: Kurt Tolksdorf
//        Begun:  7/20/02
//
// Buffer facility for managing a pool of uniform-sized buffers.
//------------------------------------------------------------------------

CBufferPool *CBufferPool::Create(size_t bufferSizeInBytes)
{
   return new CBufferPool(bufferSizeInBytes);
}
//------------------------------------------------------------------------

// &&&&&&&&&&&& DEPRECATED because synchronization is buggy &&&&&&&&&&&&& //
void CBufferPool::Destroy(CBufferPool *bufferPool)
{
   MTIassert(false);
   if (bufferPool == NULL)
   {
      return;
   }

   bool okToDeleteThePool;
   {
      CAutoSpinLocker lock(bufferPool->threadLock);

      bufferPool->poolWasDestroyed = true;

      // deallocateAllInactiveBuffers() returns false if any of the buffers
      // in the pool are still in use!
      okToDeleteThePool = !bufferPool->deallocateAllInactiveBuffers();
   }

// There seem to be issues here so don't delete the pool; just let it leak. QQQ
//   if (okToDeleteThePool
//   {
//      delete bufferPool;
//   }
}
//------------------------------------------------------------------------

bool CBufferPool::DestroyAllUnusedBuffers(CBufferPool *bufferPool)
{
   if (bufferPool == NULL)
   {
      return true;
   }

   CAutoSpinLocker lock(bufferPool->threadLock);
   bool retVal;

   // deallocateAllInactiveBuffers() returns false if any of the buffers
   // in the pool are still in use!
   retVal = bufferPool->deallocateAllInactiveBuffers();

   return retVal;
}
//------------------------------------------------------------------------

CBufferPool::CBufferPool(size_t bufferSizeInBytes)
: bytesPerBuffer(bufferSizeInBytes)

{
	TRACE_3(errout << "BUFFERPOOL(" << bytesPerBuffer << "): CREATED");
   MTIassert(bufferSizeInBytes > 0);
}
//------------------------------------------------------------------------

CBufferPool::~CBufferPool()
{
	TRACE_3(errout << "BUFFERPOOL(" << bytesPerBuffer << "): DELETED");
   MTIassert(false);  // should not be called, synchronization is buggy
}
//------------------------------------------------------------------------

size_t CBufferPool::getBufferSizeInBytes()
{
//   MTIassert(!poolWasDestroyed);
   return bytesPerBuffer;
}
//------------------------------------------------------------------------

int CBufferPool::increasePoolSize(int additionalBufferCount)
{
   int retval = 0;

	TRACE_3(errout << "BUFFERPOOL(" << bytesPerBuffer << "): INCREASE: "
						<< bufferSlots.size() << " --> "
						<< (bufferSlots.size() + additionalBufferCount));

   CAutoSpinLocker lock(threadLock);

//   MTIassert(!poolWasDestroyed);
//   if (poolWasDestroyed)
//   {
//      return IMGTOOL_ERROR_BUFFER_POOL_WAS_DESTROYED;
//   }

   int i;
   for (i = 0; i < additionalBufferCount; ++i)
   {
      BufferSlot bufferSlot;

      // In the following, bufferSlot is passed by reference and is
      // modified by the callee
      if (!allocateMemoryForABuffer(bufferSlot))
      {
         break;
      }

      bufferSlots.push_back(bufferSlot);
	}

	TRACE_3(errout << *this);

   if (i < additionalBufferCount)
   {
		TRACE_3(errout << "BUFFERPOOL(" << bytesPerBuffer << "): INCREASE FAILED");
		retval = IMGTOOL_ERROR_BUFFER_POOL_ALLOC_FAILED;
   }

   return(retval);
}
//------------------------------------------------------------------------

int CBufferPool::allocateBuffer(void** bufferPtrPtr)
{
   if (bufferPtrPtr == NULL)
   {
      return IMGTOOL_ERROR_NULL_PTR;
   }

   void* &bufferPtr = *bufferPtrPtr;
   bufferPtr = nullptr;

   CAutoSpinLocker lock(threadLock);

//   MTIassert(!poolWasDestroyed);
//   if (poolWasDestroyed)
//   {
//      return IMGTOOL_ERROR_BUFFER_POOL_WAS_DESTROYED;
//   }

   BufferSlotIterator iter;
   if (findFreeSlot(iter))
   {
      bufferPtr = (*iter).bufAddr;
      (*iter).inUseFlag = true;
      (*iter).clipGUID = "";
      (*iter).frameIndex = -1;
      (*iter).fieldIndex = -1;
   }

	TRACE_3(errout << "BUFFERPOOL(" << bytesPerBuffer << "): ALLOC buffer[" << (iter - bufferSlots.begin()) << "] " << (bufferPtr ? "" : "FAILED"));
	TRACE_3(errout << *this);

	return bufferPtr ? 0 : IMGTOOL_ERROR_NO_AVAILABLE_BUFFERS;
}
//------------------------------------------------------------------------

void CBufferPool::freeBuffer(void *bufferPtr)
{
   BufferSlotIterator iter;
   bool deleteThePool = false;

   {
      CAutoSpinLocker lock(threadLock);

      // Since our buffer was marked "in use" the buffer pool wasn't actually
      // destroyed, so we'll need to to it here.
//      if (poolWasDestroyed)
//      {
//			TRACE_3(errout << "BUFFERPOOL(" << bytesPerBuffer << "): FREEING BUFFER AFTER THE POOL WAS DESTROYED!! Slots left = " << bufferSlots.size());
//      }

      bool found = findSlotByBufAddr(bufferPtr, iter);
      MTIassert(found);
      if (!found)
		{
			TRACE_3(errout << "BUFFERPOOL(" << bytesPerBuffer << "): FAILED TO FREE Buffer @ 0x" << std::setw(8) << std::setfill('0') << std::setbase(16) << bufferPtr);
			TRACE_3(errout << *this);
			return;
      }

      (*iter).inUseFlag = false;

//      if (poolWasDestroyed)
//      {
//         deallocateMemoryForABuffer(*iter);
//         bufferSlots.erase(iter);
//         if (bufferSlots.empty())
//         {
//            deleteThePool = true;
//         }
//      }
   }

	TRACE_3(errout << "BUFFERPOOL(" << bytesPerBuffer << "): FREE Buffer[" << (iter - bufferSlots.begin()) << "] @ 0x" << std::setw(8) << std::setfill('0') << std::setbase(16) << bufferPtr);
	TRACE_3(errout << *this);

	// MUST BE LAST because "this" will be invalid!!
// There saeem to be issues here so don't delete the pool; just let it leak. QQQ
//   if (deleteThePool)
//   {
//      delete this;
//   }
}
//------------------------------------------------------------------------

bool CBufferPool::allocateMemoryForABuffer(BufferSlot &slot)
{
   GpuAccessor gpuAccessor;

   // threadLock must be locked before calling!
   // Assuming alignment must be the same as CUDA default
   // DEBUG, always use cuda to alloc host memory
   slot.cudaMemory = gpuAccessor.doesGpuExist();
   if (slot.cudaMemory)
   {
      slot.bufAddr = MtiCudaMallocHost(getBufferSizeInBytes());
   }
   else
   {
	  // slot.bufAddr = _aligned_malloc(getBufferSizeInBytes(), MTI_MEMALIGN_DEFAULT);
     slot.bufAddr = MTImemalign(MTI_MEMALIGN_DEFAULT, getBufferSizeInBytes());
   }

   if (slot.bufAddr == nullptr)
   {
      return false;
   }

   return true;
}

//------------------------------------------------------------------------

void CBufferPool::deallocateMemoryForABuffer(BufferSlot &slot)
{
	// threadLock must be locked before calling!
	if (slot.cudaMemory)
	{
		MtiCudaFreeHost(slot.bufAddr); ;
	}
	else
	{
		// _aligned_free(slot.bufAddr);
  		MTIfree(slot.bufAddr);
	}

	slot.bufAddr = nullptr;
}

//------------------------------------------------------------------------

// This returns TRUE if all buffers were inactive so were all deallocated!
bool CBufferPool::deallocateAllInactiveBuffers()
{
   // threadLock must be locked before calling!
   bool retVal = true;
   BufferSlotIterator iter;

#ifdef _DEBUG
   size_t oldBufferCount = bufferSlots.size();
#endif

   for (iter = bufferSlots.begin(); iter != bufferSlots.end(); )
   {
      if ((*iter).inUseFlag)
      {
         retVal = false;
         ++iter;
         continue;
      }

      deallocateMemoryForABuffer(*iter);
      iter = bufferSlots.erase(iter);
   }

#ifdef _DEBUG
   size_t newBufferCount = bufferSlots.size();
	MTIassert(newBufferCount >= 0 && newBufferCount <= oldBufferCount);
	TRACE_3(errout << "BUFFERPOOL(" << bytesPerBuffer << "): deallocateAllInactiveBuffers (" << (oldBufferCount - newBufferCount) << "/" << oldBufferCount << " buffers deleted)");
	TRACE_3(errout << *this);
#endif

   return retVal;
}
//------------------------------------------------------------------------

bool CBufferPool::findSlotByBufAddr(void *bufAddr, BufferSlotIterator &iter)
{
   // threadLock must be locked before calling!
   for (iter = bufferSlots.begin();
        iter != bufferSlots.end() && (*iter).bufAddr != bufAddr;
        ++iter)
   {
      // Just looking for a this particular buffer
   }

   MTIassert(iter != bufferSlots.end());
   return (iter != bufferSlots.end());
}
//------------------------------------------------------------------------

bool CBufferPool::findFreeSlot(BufferSlotIterator &iter)
{
   // threadLock must be locked before calling!
   for (iter = bufferSlots.begin();
        iter != bufferSlots.end() && (*iter).inUseFlag != false;
        ++iter)
   {
      // Just looking for a free buffer
   }

   // MTIassert(iter != bufferSlots.end());   it's OK - caller will wait
   return (iter != bufferSlots.end());
}
//------------------------------------------------------------------------

bool CBufferPool::findSlotByCacheInfo(const string &clipGUID,
                                      int frameIndex, int fieldIndex,
                                      BufferSlotIterator &iter)
{
   // threadLock must be locked before calling!
	for (iter = bufferSlots.begin();
		  (iter != bufferSlots.end()) &&
				((*iter).clipGUID != clipGUID ||
             (*iter).frameIndex != frameIndex ||
             (*iter).fieldIndex != fieldIndex);
        ++iter)
   {
      // Just looking for a buffer with this info
   }

   return (iter != bufferSlots.end());
}

//==========================================================================
//======================== CACHING STUFF ===================================
//==========================================================================

int CBufferPool::identifyBufferContents(void* bufAddr, const string &clipGUID,
   int frameIndex, int fieldIndex)
{
   CAutoSpinLocker lock(threadLock);

   MTIassert(!poolWasDestroyed);
   if (poolWasDestroyed)
   {
      return IMGTOOL_ERROR_BUFFER_POOL_WAS_DESTROYED;
   }

   int retVal = IMGTOOL_ERROR_BUFFER_POOL_BUFFER_NOT_FOUND;
   BufferSlotIterator iter;

   if (findSlotByBufAddr(bufAddr, iter))
   {
      (*iter).clipGUID = clipGUID;
      (*iter).frameIndex = frameIndex;
      (*iter).fieldIndex = fieldIndex;
      retVal = 0;
   }

	TRACE_3(errout << "BUFFERPOOL(" << bytesPerBuffer << "): ID Buffer (" << clipGUID << "," << frameIndex << "," << fieldIndex << ") @ 0x" << std::setw(8) << std::setfill('0')
		<< std::setbase(16) << bufAddr);

   return retVal;
}
//------------------------------------------------------------------------

void *CBufferPool::retrieveCachedBuffer(const string &clipGUID,
   int frameIndex, int fieldIndex)
{
   CAutoSpinLocker lock(threadLock);

   MTIassert(!poolWasDestroyed);
   if (poolWasDestroyed)
   {
      return NULL;
   }

   void *retVal = NULL;
   BufferSlotIterator iter;

   if (findSlotByCacheInfo(clipGUID, frameIndex, fieldIndex, iter))
   {
      (*iter).inUseFlag = true;
      retVal = (*iter).bufAddr;
   }

	TRACE_3(errout << "BUFFERPOOL(" << bytesPerBuffer << "): RETRIEVE Buffer (" << clipGUID << "," << frameIndex << "," << fieldIndex << ") @ 0x" << std::setw(8)
		<< std::setfill('0') << std::setbase(16) << retVal);

   return retVal;
}
//------------------------------------------------------------------------

int CBufferPool::invalidateBufferContents(void* bufAddr)
{
   CAutoSpinLocker lock(threadLock);

   MTIassert(!poolWasDestroyed);
   if (poolWasDestroyed)
   {
      return IMGTOOL_ERROR_BUFFER_POOL_WAS_DESTROYED;
   }

   int retVal = IMGTOOL_ERROR_BUFFER_POOL_BUFFER_NOT_FOUND;
   BufferSlotIterator iter;

   if (findSlotByBufAddr(bufAddr, iter))
   {
		(*iter).clipGUID = "";
		(*iter).frameIndex = -1;
		(*iter).fieldIndex = -1;
		retVal = 0;
	}

	TRACE_3(errout << "BUFFERPOOL(" << bytesPerBuffer << "): INVALIDATE Buffer @ 0x" << std::setw(8) << std::setfill('0') << std::setbase(16) << bufAddr);
	TRACE_3(errout << *this);

	return retVal;
}
//------------------------------------------------------------------------

int CBufferPool::invalidateBufferContents(const string &clipGUID,
   int frameIndex, int fieldIndex)
{
   CAutoSpinLocker lock(threadLock);

   MTIassert(!poolWasDestroyed);
   if (poolWasDestroyed)
   {
      return IMGTOOL_ERROR_BUFFER_POOL_WAS_DESTROYED;
   }

   int retVal = IMGTOOL_ERROR_BUFFER_POOL_BUFFER_NOT_FOUND;
   BufferSlotIterator iter;

   if (findSlotByCacheInfo(clipGUID, frameIndex, fieldIndex, iter))
   {
		(*iter).clipGUID = "";
      (*iter).frameIndex = -1;
      (*iter).fieldIndex = -1;
      retVal = 0;
   }

	TRACE_3(errout << "BUFFERPOOL(" << bytesPerBuffer << "): INVALIDATE Buffer (" << clipGUID << "," << frameIndex << "," << fieldIndex << ")");
	TRACE_3(errout << *this);

	return retVal;
}
//------------------------------------------------------------------------

std::ostream& operator<< (std::ostream& os, const CBufferPool& bufferPool)
{
	os << "BUFFERPOOL(" << bufferPool.bytesPerBuffer << "): DUMP";
	for (auto iter = bufferPool.bufferSlots.begin(); iter != bufferPool.bufferSlots.end(); ++iter)
	{
		os << endl << "[" << (iter - bufferPool.bufferSlots.begin()) << "] is " << ((*iter).inUseFlag ? "IN USE" : "AVAILABLE");
		if ((*iter).frameIndex != -1)
		{
			os << ", contains " << (*iter).clipGUID << "[" << (*iter).frameIndex << "]";
		}
	}

	return os;
}
//------------------------------------------------------------------------
