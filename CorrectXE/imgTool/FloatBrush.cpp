#include "FloatBrush.h"
#include "Ippheaders.h"

CFloatBrush::CFloatBrush(int numStripes) : CBrush(numStripes), _floatBrush(0), _floatBrushWidth(-1),
	_floatBrushHeight(-1)
{

}

CFloatBrush::CFloatBrush(const BrushParameters &brushParameters, int numStripes) : CFloatBrush()
{
	this->setBrushParameters(brushParameters.MaxRadius, brushParameters.Radius,
		brushParameters.Blend, brushParameters.Opacity, brushParameters.Aspect,
		brushParameters.Angle, brushParameters.Shape, brushParameters.Type);
}

CFloatBrush::~CFloatBrush()
{
	ippFree(this->_floatBrush);
}

void CFloatBrush::setBrushParameters(int newMaxRadius, int newRadius, int newBlend, int newOpacity,
	int newAspect, int newAngle, bool newShape, EBrushType newType)
{
	CBrush *base = (CBrush*)this;

	base->setBrushParameters(newMaxRadius, newRadius, newBlend, newOpacity, newAspect, newAngle,
		newShape, newType);

	// Force the brush to recompute the weights because of side effects.
	this->getWeight(0, 0);

	this->ComputeFloatBrushWeights();
}

void CFloatBrush::setBrushParametersFromBrush(CFloatBrush *brush)
{
	CBrush *base = (CBrush*)this;
	base->setBrushParametersFromBrush(brush);

	// Force the brush to recompute the weights because of side effects.
	this->getWeight(0, 0);

	this->ComputeFloatBrushWeights();
}

int CFloatBrush::getBrushWidth()
{
	// Brush already has +1 built in as it is centered on (0,0)
	return this->getMaxCol()-this->getMinCol();
}

int CFloatBrush::getBrushHeight()
{
	// Brush already has +1 built in as it is centered on (0,0)
	return this->getMaxRow()-this->getMinRow();
}

int CFloatBrush::getBrushSizeInFloats()
{
	return this->getBrushHeight() * this->getBrushWidth();
}

void CFloatBrush::ComputeFloatBrushWeights()
{
	if (getBrushSizeInFloats() != (this->_floatBrushHeight*this->_floatBrushWidth))
	{
		// Realloc it
		ippFree(this->_floatBrush);

		this->_floatBrushWidth = getBrushWidth();
		this->_floatBrushHeight = getBrushHeight();

		this->_floatBrush = ippsMalloc_32f(this->_floatBrushHeight*this->_floatBrushWidth);
		IppThrowOnMemoryError(this->_floatBrush);
	}

	// Now load the brush, done because center is at (0,0)
	Ipp32f *fp = this->_floatBrush;
	int maxCols = this->getMaxCol();
	int minCols = this->getMinCol();
	int maxRows = this->getMaxRow();
	int minRows = this->getMinRow();

	for (int r = minRows; r < maxRows; r++)
	{
		for (int c = minCols; c < maxCols; c++)
		{
			*fp++ = (this->getWeightFast(r, c) / 32768.0);
		}
	}
}

MTI_REAL32 *CFloatBrush::getFloatWeights()
{
	return this->_floatBrush;
}

// --------------------------------------------------------------------------------------

bool BrushParameters:: operator == (const BrushParameters &b) const
{
	return (b.MaxRadius == this->MaxRadius) && (b.Radius == this->Radius) &&
		(b.Blend == this->Blend) && (b.Opacity == this->Opacity) && (b.Aspect == this->Aspect) &&
		(b.Angle == this->Angle) && (b.Shape == this->Shape) && (b.Type == this->Type);
}

bool BrushParameters:: operator != (const BrushParameters &b) const
{
	return !(*this == b);
}

BrushParameters& BrushParameters:: operator = (const BrushParameters & b)
{
	this->MaxRadius = b.MaxRadius;
	this->Radius = b.Radius;
	this->Blend = b.Blend;
	this->Opacity = b.Opacity;
	this->Aspect = b.Aspect;
	this->Angle = b.Angle;
	this->Shape = b.Shape;
	this->Type = b.Type;
	return *this;
}
