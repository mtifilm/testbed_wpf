// OverlayItem.cpp: implementation of the COverlayItem class.
//
/*
$Header: /usr/local/filmroot/imgTool/source/OverlayItem.cpp,v 1.4 2003/06/06 16:46:23 manbeck Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "OverlayItem.h"
/*
#ifdef INCLUDE_CLIP_REV_2
#include "ImageFormat2.h"
#endif
*/
#include "ImageFormat3.h"
#include "timecode.h"
#include "LineEngine.h"

// =========================================================================
// =========================================================================
//
// COverlayItem
//
// =========================================================================

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

COverlayItem::COverlayItem()
: x(0), y(0),
  foregroundColor(1), backgroundColor(0)
{

}

COverlayItem::~COverlayItem()
{
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
// Public Interface
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    init
//
// Description: 
//
// Arguments    
//
// Returns:     none
//
//------------------------------------------------------------------------
void COverlayItem::init(int xLoc, int yLoc, int fgColor, int bgColor)
{
   x = xLoc;
   y = yLoc;
   foregroundColor = fgColor;
   backgroundColor = bgColor;
}

// =========================================================================
// =========================================================================
//
// COverlayText
//
// =========================================================================

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

COverlayText::COverlayText()
: fontSize(1)
{

}

COverlayText::~COverlayText()
{

}

//------------------------------------------------------------------------
//
// Function:    init
//
// Description: 
//
// Arguments    
//
// Returns:     none
//
//------------------------------------------------------------------------

void COverlayText::init(int xLoc, int yLoc, int fgColor, int bgColor, 
                        int newFontSize)
{
   COverlayItem::init(xLoc, yLoc, fgColor, bgColor);
   fontSize = newFontSize;
}

//------------------------------------------------------------------------
//
// Function:    process
//
// Description: Draws an overlay item into the caller's buffer(s)
//              into caller's buffers.  Reads one or two fields 
//
// Arguments    CVideoFrame &frame    Reference to frame object
//              MTI_UINT8 *buffers[] Array of pointers to buffers, one for each
//                               field in frame
//
// Returns:     none
//
//------------------------------------------------------------------------
/*
#ifdef INCLUDE_CLIP_REV_2
void COverlayText::process(const CVideoFrame2 &frame, MTI_UINT8 *buffers[])
{

}
#endif // #ifdef INCLUDE_CLIP_REV_2
*/

void COverlayText::process(CImageFormat const *imageFormat,
                           CTimecode const &tc, MTI_UINT8 *buffers[])
{

}

// =========================================================================
// =========================================================================
//
// COverlayTimecode
//
// =========================================================================

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

COverlayTimecode::COverlayTimecode()
{

}

COverlayTimecode::~COverlayTimecode()
{

}

//------------------------------------------------------------------------
//
// Function:    init
//
// Description: 
//
// Arguments    
//
// Returns:     none
//
//------------------------------------------------------------------------
void COverlayTimecode::init(int xLoc, int yLoc, int fgColor, int bgColor, 
                            int newFontSize)
{
   COverlayText::init(xLoc, yLoc, fgColor, bgColor, newFontSize);
}

//------------------------------------------------------------------------
//
// Function:    process
//
// Description: Draws an overlay item into the caller's buffer(s)
//              into caller's buffers.  Reads one or two fields 
//
// Arguments    CVideoFrame &frame   Reference to frame object
//              MTI_UINT8 *buffers[] Array of pointers to buffers, one for each
//                               field in frame
//
// Returns:     none
//
//------------------------------------------------------------------------
/*
#ifdef INCLUDE_CLIP_REV_2
void COverlayTimecode::process(const CVideoFrame2 &frame, MTI_UINT8 *buffers[])
{
   CImageFormat2 *imageFormat = frame.getImageFormat();
   CPixelEng *pixelEng = imageFormat->getPixelEng();

   if (pixelEng)
      {
      CTimecode tc = frame.getTimecode();
      char tcString[20];
      tc.getTimeASCII(tcString);

      pixelEng->setFontSize(fontSize);

      pixelEng->setFrameBuffer(buffers[0]);
      drawText(tcString, pixelEng);

      if (imageFormat->getInterlaced())
         {
         pixelEng->setFrameBuffer(buffers[1]);
         drawText(tcString, pixelEng);
         }
      }
   else
      {
      }
}
#endif // #ifdef INCLUDE_CLIP_REV_2
*/

void COverlayTimecode::process(CImageFormat const *imageFormat, CTimecode const &tc,
                               MTI_UINT8 *buffers[])
{
   CLineEngine *pixelEng = CLineEngineFactory::makePixelEng(*imageFormat);

   if (pixelEng)
      {
      char tcString[20];
      tc.getTimeASCII(tcString);

      pixelEng->setFontSize(fontSize);

      pixelEng->setExternalFrameBuffer(buffers);
      drawText(tcString, pixelEng);
      CLineEngineFactory::destroyPixelEng(pixelEng);
      }
   else
      {
      }
}


//void COverlayText::drawText(const char* str, CPixelEng *pixelEng)
void COverlayText::drawText(const char* str, CLineEngine *pixelEng)
{
   if (backgroundColor != -1)
      {
      pixelEng->setFGColor(backgroundColor);
      pixelEng->hiliteString(x, y, str);
      }

   pixelEng->setFGColor(foregroundColor);
   pixelEng->setBGColor(backgroundColor);
   pixelEng->drawString(x, y, str);
}

///////////////////////////////////////////////////////////////////////
//
//  Code for CScaledOverlay
//
//  overlay values are scaled to match the image format
//
///////////////////////////////////////////////////////////////////////
CScaledOverlay::CScaledOverlay()
{
  lepLineEngine = 0;

  fFontSize = 1.;
  fRowPos = .10;
  fColPos = .10;

  iColorFG = 1;
  iColorBG = 0;

  iNRow = 0;
  iNCol = 0;
}  /* CScaledOverlay */

CScaledOverlay::~CScaledOverlay()
{
  CLineEngineFactory::destroyPixelEng(lepLineEngine);
  lepLineEngine = 0;
}  /* ~CScaledOverlay */

void CScaledOverlay::setImageFormat (CImageFormat const &ifArg)
{
  // create the line engine
  lepLineEngine = CLineEngineFactory::makePixelEng(ifArg);

  // make copy of internal variables
  iNRow = ifArg.getLinesPerFrame();
  iNCol = ifArg.getPixelsPerLine();
}  /* setImageFormat */

void CScaledOverlay::setFontSize (float fFontSizeArg)
{
  fFontSize = fFontSizeArg;
}  /* setFontSize */

void CScaledOverlay::setImagePosition (float fRowPosArg, float fColPosArg)
{
  fRowPos = fRowPosArg;
  fColPos = fColPosArg;
}  /* setImagePosition */

void CScaledOverlay::setColor (int iColorFGArg, int iColorBGArg)
{
  iColorFG = iColorFGArg;
  iColorBG = iColorBGArg;
}  /* setColor */

void CScaledOverlay::Process (MTI_UINT8 *ucpData[], const string &strArg)
{
  int x,y, z;

  // x is pixel position from left of image
  x = fColPos * (float)iNCol + .5;

  // y is pixel position from top of image
  y = fRowPos * (float)iNRow + .5;

  // z is font size.  Take nominal size to based on 525 material
  z = fFontSize * (float)iNRow / 486. + 0.5;
  if (z < 1)
    z = 1;

  lepLineEngine->setExternalFrameBuffer(ucpData);
  lepLineEngine->setFontSize (z);

  if (iColorBG != -1)
   {
    lepLineEngine->setFGColor(iColorBG);
    lepLineEngine->hiliteString(x, y, strArg.c_str());
   }

   lepLineEngine->setFGColor(iColorFG);
   lepLineEngine->setBGColor(iColorBG);
   lepLineEngine->drawString(x, y, strArg.c_str());
}  /* Process */

void CScaledOverlay::Process (MTI_UINT8 *ucpData[], const CTimecode &tcArg)
{
  char caTC[32];

  tcArg.getTimeASCII (caTC);

  Process (ucpData, caTC);
}  /* Process */
