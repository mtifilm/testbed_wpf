/*
	File:	RegionOfInterest.cpp
	By:	Kevin Manbeck & Kurt Tolksdorf
	Date:	Oct 6, 2003

	The RegionOfInterest class is used to pass information to the
	image processing algorithms.  This information describes which
	pixels should be processed and how they should be processed.

	The RegionOfInterest is basically two 8-bit arrays:  one containing
	blending instructions for combining the original image and
	the processed image.  The second array is a set of labels which may
 	be used by the algorithms.

	The 8-bit arrays are the same dimension as the image.  The values
	in the arrays are generated relative to a shape supplied by the
	user.  The shape can be rectangle, lasso, or bezier.
*/
#include "RegionOfInterest.h"
#include "ROIMask.h"
#include "err_imgtool.h"
#include "MTImalloc.h"
#include <math.h>
#include "IniFile.h"
#include "Ippheaders.h"

CUserRegion::CUserRegion (bool newInclusiveRegionFlag)
{
  bInclusiveRegionFlag = newInclusiveRegionFlag;
  iBlendType = BLEND_TYPE_LINEAR;
  iBlendRadiusInterior = 0;
  iBlendRadiusExterior = 0;
  iSurroundRadius = 0;
}

CUserRegion::~CUserRegion()
{
}

void CUserRegion::setBlendType (EBlendType newBlend)
{
  iBlendType = newBlend;
}

void CUserRegion::setBlendRadiusInterior (int newRadius)
{
  iBlendRadiusInterior = newRadius;
}

void CUserRegion::setBlendRadiusExterior (int newRadius)
{
  iBlendRadiusExterior = newRadius;
}

void CUserRegion::setSurroundRadius (int newRadius)
{
  iSurroundRadius = newRadius;
}

// this calculates the value for successive layers inside and outside
MTI_UINT8 CUserRegion::getBlendLevel(int layer)
{
   if (layer <= - iBlendRadiusInterior) return 255;
   if (layer >=   iBlendRadiusExterior) return   0;

   double fstep = 255. / (iBlendRadiusInterior + iBlendRadiusExterior);
   double intcp = fstep*iBlendRadiusExterior;

   return intcp - (double)layer * fstep + .5;
}

// this returns the bounding rectangle fattened by the exterior blend radius
RECT CUserRegion::getBlendBox()
{
   RECT blendRectangle = boundingRectangle;

   if (bInclusiveRegionFlag) {
      blendRectangle.left   = boundingRectangle.left - iBlendRadiusExterior;
      blendRectangle.top    = boundingRectangle.top  - iBlendRadiusExterior;
      blendRectangle.right  = boundingRectangle.right + iBlendRadiusExterior;
      blendRectangle.bottom = boundingRectangle.bottom + iBlendRadiusExterior;
   }
   else {

      blendRectangle.left   = 0;
      blendRectangle.top    = 0;
      blendRectangle.right  = MAXFRAMEX;
      blendRectangle.bottom = MAXFRAMEY;
   }

   return blendRectangle;
}

// this returns the bounding rectangle fattened by the surround radius
RECT CUserRegion::getLabelBox()
{
   RECT labelRectangle = boundingRectangle;

   if (bInclusiveRegionFlag) {
      labelRectangle.left   = boundingRectangle.left - iSurroundRadius;
      labelRectangle.top    = boundingRectangle.top  - iSurroundRadius;
      labelRectangle.right  = boundingRectangle.right + iSurroundRadius;
      labelRectangle.bottom = boundingRectangle.bottom + iSurroundRadius;
   }
   else {

      labelRectangle.left   = 0;
      labelRectangle.top    = 0;
      labelRectangle.right  = MAXFRAMEX;
      labelRectangle.bottom = MAXFRAMEY;
   }

   return labelRectangle;
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                              #################################
// ###     CRectUserRegion Class      ################################
// ####                              #################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRectUserRegion::CRectUserRegion(const RECT &newRect,
                                 bool newInclusiveRegionFlag) :
                                 CUserRegion (newInclusiveRegionFlag)
{
  // the bounding rectangle
  boundingRectangle = newRect;

  // the defining rectangle
  rect = newRect;
}

CRectUserRegion::~CRectUserRegion()
{
}

//////////////////////////////////////////////////////////////////////
// Render Blend and Label Bytemaps
//////////////////////////////////////////////////////////////////////
int CRectUserRegion::renderBlendMask(CROIMask *blendEngine)
{
   // fill the rectangle (or its complement)
   if (blendEngine->initFill(4) < 0) return IMGTOOL_ERROR_MALLOC;
   blendEngine->moveToFill(rect.left,  rect.top);
   blendEngine->lineToFill(rect.right, rect.top);
   blendEngine->lineToFill(rect.right, rect.bottom);
   blendEngine->lineToFill(rect.left,  rect.bottom);
   blendEngine->lineToFill(rect.left,  rect.top);

   // the edge buffer is ready; do the filling
   blendEngine->setDrawColor(255);
   if (bInclusiveRegionFlag)
      blendEngine->fillPolygon();
   else {
      blendEngine->fillPolygonComplement();
   }

   if ((iBlendType == BLEND_TYPE_LINEAR)||(iBlendType == BLEND_TYPE_DITHER)) {
      blendEngine->setPadRadii(iBlendRadiusInterior, iBlendRadiusExterior);
      blendEngine->blendLastFillContour();
      if (iBlendType == BLEND_TYPE_DITHER)
         blendEngine->ditherLastFillContour();
   }

   return 0;
}

int CRectUserRegion::renderLabelMask(CROIMask *labelEngine)
{
   // fill the rectangle (or its complement)
   if (labelEngine->initFill(4) < 0) return IMGTOOL_ERROR_MALLOC;
   labelEngine->moveToFill(rect.left,  rect.top);
   labelEngine->lineToFill(rect.right, rect.top);
   labelEngine->lineToFill(rect.right, rect.bottom);
   labelEngine->lineToFill(rect.left,  rect.bottom);
   labelEngine->lineToFill(rect.left,  rect.top);

   // the edge buffer is ready; do the filling
   labelEngine->setDrawColor(LABEL_INTERIOR);
   if (bInclusiveRegionFlag)
      labelEngine->fillPolygon();
   else {
      labelEngine->fillPolygonComplement();
   }

   // render the surround region
   labelEngine->setPadRadius(iSurroundRadius);
   labelEngine->setEnableColor(LABEL_NONE);
   labelEngine->setReplaceColor(LABEL_SURROUND);
   labelEngine->labelLastFillContour();

   return 0;
}
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                              #################################
// ###    CLassoUserRegion Class      ################################
// ####                              #################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLassoUserRegion::CLassoUserRegion (const POINT *newLasso,
                                    bool newInclusiveRegionFlag) :
                                    CUserRegion (newInclusiveRegionFlag)
{
   // one pass through determines lasso extent and no of vertices
   boundingRectangle.left   = MAXFRAMEX;
   boundingRectangle.top    = MAXFRAMEY;
   boundingRectangle.right  = MINFRAMEX;
   boundingRectangle.bottom = MINFRAMEY;

   int i;
   for (i=0;;i++) {

      if (newLasso[i].x < boundingRectangle.left)
         boundingRectangle.left = newLasso[i].x;
      if (newLasso[i].y < boundingRectangle.top)
         boundingRectangle.top = newLasso[i].y;
      if (newLasso[i].x > boundingRectangle.right)
         boundingRectangle.right = newLasso[i].x;
      if (newLasso[i].y > boundingRectangle.bottom)
         boundingRectangle.bottom = newLasso[i].y;

      if ((i > 1)&& // last point duplicates first point
          (newLasso[i].x == newLasso[0].x)&&
          (newLasso[i].y == newLasso[0].y))
         break;
   }

   vertexCount = i + 1;

   // allocate space for the captive lasso
   lasso = new POINT[vertexCount];

   // copy the vertices of the lasso from the argument
   memcpy(lasso,&newLasso[0],sizeof(POINT)*vertexCount);
}

CLassoUserRegion::~CLassoUserRegion()
{
  delete [] lasso;
}

//////////////////////////////////////////////////////////////////////
// Render Blend and Label Bytemaps
//////////////////////////////////////////////////////////////////////
int CLassoUserRegion::renderBlendMask(CROIMask *blendEngine)
{
   // fill the lasso
   if (blendEngine->initFill(vertexCount) < 0) return IMGTOOL_ERROR_MALLOC;
   blendEngine->moveToFill(lasso[0].x,lasso[0].y);
   for (int i=1;i<vertexCount;i++) {
      blendEngine->lineToFill(lasso[i].x,lasso[i].y);
   }

   // the edge buffer is ready; do the filling
   blendEngine->setDrawColor(255);
   if (bInclusiveRegionFlag)
      blendEngine->fillPolygon();
   else {
      blendEngine->fillPolygonComplement();
   }

   if ((iBlendType == BLEND_TYPE_LINEAR)||(iBlendType == BLEND_TYPE_DITHER)) {
      blendEngine->setPadRadii(iBlendRadiusInterior, iBlendRadiusExterior);
      blendEngine->blendLastFillContour();
      if (iBlendType == BLEND_TYPE_DITHER)
         blendEngine->ditherLastFillContour();
   }

   return 0;
}

int CLassoUserRegion::renderLabelMask(CROIMask *labelEngine)
{
   // fill the lasso
   if (labelEngine->initFill(vertexCount) < 0) return IMGTOOL_ERROR_MALLOC;
   labelEngine->moveToFill(lasso[0].x,lasso[0].y);
   for (int i=1;i<vertexCount;i++) {
      labelEngine->lineToFill(lasso[i].x,lasso[i].y);
   }

   // the edge buffer is ready; do the filling
   labelEngine->setDrawColor(LABEL_INTERIOR);
   if (bInclusiveRegionFlag)
      labelEngine->fillPolygon();
   else {
      labelEngine->fillPolygonComplement();
   }

   // render the surround region
   labelEngine->setPadRadius(iSurroundRadius);
   labelEngine->setEnableColor(LABEL_NONE);
   labelEngine->setReplaceColor(LABEL_SURROUND);
   labelEngine->labelLastFillContour();

   return 0;
}


//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                              #################################
// ###    CBezierUserRegion Class     ################################
// ####                              #################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBezierUserRegion::CBezierUserRegion (const BEZIER_POINT *newBezier,
                                      bool newInclusiveRegionFlag) :
                                      CUserRegion (newInclusiveRegionFlag)
{
   // one pass through determines spline extent and no of vertices
   boundingRectangle.left   = MAXFRAMEX;
   boundingRectangle.top    = MAXFRAMEY;
   boundingRectangle.right  = MINFRAMEX;
   boundingRectangle.bottom = MINFRAMEY;

   int i;
   for (i=0;;i++) {

      if (newBezier[i].xctrlpre < boundingRectangle.left)
         boundingRectangle.left = newBezier[i].xctrlpre;
      if (newBezier[i].x < boundingRectangle.left)
         boundingRectangle.left = newBezier[i].x;
      if (newBezier[i].xctrlnxt < boundingRectangle.left)
         boundingRectangle.left = newBezier[i].xctrlnxt;

      if (newBezier[i].yctrlpre < boundingRectangle.top)
         boundingRectangle.top = newBezier[i].yctrlpre;
      if (newBezier[i].y < boundingRectangle.top)
         boundingRectangle.top = newBezier[i].y;
      if (newBezier[i].yctrlnxt < boundingRectangle.top)
         boundingRectangle.top = newBezier[i].yctrlnxt;

      if (newBezier[i].xctrlpre > boundingRectangle.right)
         boundingRectangle.right = newBezier[i].xctrlpre;
      if (newBezier[i].x > boundingRectangle.right)
         boundingRectangle.right = newBezier[i].x;
      if (newBezier[i].xctrlnxt > boundingRectangle.right)
         boundingRectangle.right = newBezier[i].xctrlnxt;

      if (newBezier[i].yctrlpre > boundingRectangle.bottom)
         boundingRectangle.bottom = newBezier[i].yctrlpre;
      if (newBezier[i].y > boundingRectangle.bottom)
         boundingRectangle.bottom = newBezier[i].y;
      if (newBezier[i].yctrlnxt > boundingRectangle.bottom)
         boundingRectangle.bottom = newBezier[i].yctrlnxt;

      if ((i > 1)&& // last point duplicates first point
          (newBezier[i].xctrlpre == newBezier[0].xctrlpre)&&
          (newBezier[i].yctrlpre == newBezier[0].yctrlpre)&&
          (newBezier[i].x        == newBezier[0].x       )&&
          (newBezier[i].y        == newBezier[0].y       )&&
          (newBezier[i].xctrlnxt == newBezier[0].xctrlnxt)&&
          (newBezier[i].yctrlnxt == newBezier[0].yctrlnxt))
         break;
   }

   vertexCount = i + 1;

   // allocate space for the captive spline
   bezier = new BEZIER_POINT[vertexCount];

   // copy the vertices of the spline from the argument
   memcpy(bezier,&newBezier[0],sizeof(BEZIER_POINT)*vertexCount);
}

CBezierUserRegion::~CBezierUserRegion ()
{
  delete [] bezier;
}

//////////////////////////////////////////////////////////////////////
// Render Blend and Label Bytemaps
//////////////////////////////////////////////////////////////////////
int CBezierUserRegion::getTotalChords(int chordleng)
{
   int delx, dely;
   int segmentLength;
   int totalChords = 0;

   // Estimate the length of each spline segment by
   // summing the absolute deltas from attach pt to
   // ctrl pt to ctrl pt to attach pt. Divide this
   // value by the chord length supplied as arg to
   // get the number of chords which will be used
   // by the spline fill algorithm to outline the
   // spline. (this works just like ROIMask).

   for (int i=0;i<vertexCount-1;i++) {

      segmentLength = 0;
      if ((delx = bezier[i].xctrlnxt - bezier[i].x)<0) delx = -delx;
      if ((dely = bezier[i].yctrlnxt - bezier[i].y)<0) dely = -dely;
      segmentLength += (delx + dely);
      if ((delx = bezier[i+1].xctrlpre - bezier[i].xctrlnxt)<0) delx = -delx;
      if ((dely = bezier[i+1].yctrlpre - bezier[i].yctrlnxt)<0) dely = -dely;
      segmentLength += (delx + dely);
      if ((delx = bezier[i+1].x - bezier[i+1].xctrlpre)<0) delx = -delx;
      if ((dely = bezier[i+1].y - bezier[i+1].yctrlpre)<0) dely = -dely;
      segmentLength += (delx + dely);

      totalChords += segmentLength/chordleng;

   }

   return(totalChords);
}

int CBezierUserRegion::renderBlendMask(CROIMask *blendEngine)
{
   // fill the spline using a very small chord length
   int chordLength = 4;
   if (blendEngine->initFill(getTotalChords(chordLength))!=0) { // not enough storage
      return IMGTOOL_ERROR_MALLOC;
   }

   // the method "splineToFill" uses the same algorithm
   // to determine the no of chords per segment as the
   // "getTotalChords" method here.
   blendEngine->moveToFill(bezier[0].x,bezier[0].y);
   for (int i=1;i<vertexCount;i++) {
      blendEngine->splineToFill(bezier[i-1].xctrlnxt, // control pt
                                bezier[i-1].yctrlnxt,
                                bezier[i].xctrlpre,   // control pt
                                bezier[i].yctrlpre,
                                bezier[i].x,          // attach  pt
                                bezier[i].y,
                                chordLength);         // chord length
   }

   // the edge buffer is ready; do the filling
   blendEngine->setDrawColor(255);
   if (bInclusiveRegionFlag)
      blendEngine->fillPolygon();
   else {
      blendEngine->fillPolygonComplement();
   }

   if ((iBlendType == BLEND_TYPE_LINEAR)||(iBlendType == BLEND_TYPE_DITHER)) {
      blendEngine->setPadRadii(iBlendRadiusInterior, iBlendRadiusExterior);
      blendEngine->blendLastFillContour();
      if (iBlendType == BLEND_TYPE_DITHER)
         blendEngine->ditherLastFillContour();
   }

   return 0;
}

int CBezierUserRegion::renderLabelMask(CROIMask *labelEngine)
{
   // fill the spline using a small chord-length
   int chordLength = 2;
   if (labelEngine->initFill(getTotalChords(chordLength))!=0) { // not enough edges
      chordLength = 4;
   if (labelEngine->initFill(getTotalChords(chordLength))!=0) { // still not enough
      chordLength = 8;
   if (labelEngine->initFill(getTotalChords(chordLength))!=0) return IMGTOOL_ERROR_MALLOC;
   }
   }

   labelEngine->moveToFill(bezier[0].x,bezier[0].y);
   for (int i=1;i<vertexCount;i++) {
      labelEngine->splineToFill(bezier[i-1].xctrlnxt, // control pt
                                bezier[i-1].yctrlnxt,
                                bezier[i].xctrlpre,   // control pt
                                bezier[i].yctrlpre,
                                bezier[i].x,          // attach  pt
                                bezier[i].y,
                                chordLength);         // chord length
   }

   // the edge buffer is ready; do the filling
   labelEngine->setDrawColor(LABEL_INTERIOR);
   if (bInclusiveRegionFlag)
      labelEngine->fillPolygon();
   else {
      labelEngine->fillPolygonComplement();
   }

   // render the surround region
   labelEngine->setPadRadius(iSurroundRadius);
   labelEngine->setEnableColor(LABEL_NONE);
   labelEngine->setReplaceColor(LABEL_SURROUND);
   labelEngine->labelLastFillContour();

   return 0;
}

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                              #################################
// ###    CBezierListUserRegion Class     ################################
// ####                              #################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBezierListUserRegion::CBezierListUserRegion (const BEZIER_POINT *newBezier,
                                          int vtxcnt,
                                          bool newInclusiveRegionFlag) :
                                          CUserRegion (newInclusiveRegionFlag)
{
   // one pass through determines spline extent and no of vertices
   boundingRectangle.left   = MAXFRAMEX;
   boundingRectangle.top    = MAXFRAMEY;
   boundingRectangle.right  = MINFRAMEX;
   boundingRectangle.bottom = MINFRAMEY;

   // set the vertex count
   vertexCount = vtxcnt;

   int i;
   for (i=0;i < vertexCount;i++) {

      if (newBezier[i].xctrlpre < boundingRectangle.left)
         boundingRectangle.left = newBezier[i].xctrlpre;
      if (newBezier[i].x < boundingRectangle.left)
         boundingRectangle.left = newBezier[i].x;
      if (newBezier[i].xctrlnxt < boundingRectangle.left)
         boundingRectangle.left = newBezier[i].xctrlnxt;

      if (newBezier[i].yctrlpre < boundingRectangle.top)
         boundingRectangle.top = newBezier[i].yctrlpre;
      if (newBezier[i].y < boundingRectangle.top)
         boundingRectangle.top = newBezier[i].y;
      if (newBezier[i].yctrlnxt < boundingRectangle.top)
         boundingRectangle.top = newBezier[i].yctrlnxt;

      if (newBezier[i].xctrlpre > boundingRectangle.right)
         boundingRectangle.right = newBezier[i].xctrlpre;
      if (newBezier[i].x > boundingRectangle.right)
         boundingRectangle.right = newBezier[i].x;
      if (newBezier[i].xctrlnxt > boundingRectangle.right)
         boundingRectangle.right = newBezier[i].xctrlnxt;

      if (newBezier[i].yctrlpre > boundingRectangle.bottom)
         boundingRectangle.bottom = newBezier[i].yctrlpre;
      if (newBezier[i].y > boundingRectangle.bottom)
         boundingRectangle.bottom = newBezier[i].y;
      if (newBezier[i].yctrlnxt > boundingRectangle.bottom)
         boundingRectangle.bottom = newBezier[i].yctrlnxt;
   }

   // allocate space for the captive spline
   bezier = new BEZIER_POINT[vertexCount];

   // copy the vertices of the spline from the argument
   memcpy(bezier,&newBezier[0],sizeof(BEZIER_POINT)*vertexCount);
}

CBezierListUserRegion::~CBezierListUserRegion ()
{
  delete [] bezier;
}

//////////////////////////////////////////////////////////////////////
// Render Blend and Label Bytemaps
//////////////////////////////////////////////////////////////////////
int CBezierListUserRegion::getTotalChords(int chordleng)
{
   int delx, dely;
   int segmentLength;
   int totalChords = 0;

   // Estimate the length of each spline segment by
   // summing the absolute deltas from attach pt to
   // ctrl pt to ctrl pt to attach pt. Divide this
   // value by the chord length supplied as arg to
   // get the number of chords which will be used
   // by the spline fill algorithm to outline the
   // spline. (this works just like ROIMask).

   for (int i=0;i<vertexCount-1;i++) {

      segmentLength = 0;
      if ((delx = bezier[i].xctrlnxt - bezier[i].x)<0) delx = -delx;
      if ((dely = bezier[i].yctrlnxt - bezier[i].y)<0) dely = -dely;
      segmentLength += (delx + dely);
      if ((delx = bezier[i+1].xctrlpre - bezier[i].xctrlnxt)<0) delx = -delx;
      if ((dely = bezier[i+1].yctrlpre - bezier[i].yctrlnxt)<0) dely = -dely;
      segmentLength += (delx + dely);
      if ((delx = bezier[i+1].x - bezier[i+1].xctrlpre)<0) delx = -delx;
      if ((dely = bezier[i+1].y - bezier[i+1].yctrlpre)<0) dely = -dely;
      segmentLength += (delx + dely);

      totalChords += segmentLength/chordleng;
   }

   return(totalChords);
}

int CBezierListUserRegion::renderBlendMask(CROIMask *blendEngine)
{
   // fill the spline using a very small chord length
   int chordLength = 4;
   if (blendEngine->initFill(getTotalChords(chordLength))!=0) { // not enough storage
      return IMGTOOL_ERROR_MALLOC;
   }

   // the method "splineToFill" uses the same algorithm
   // to determine the no of chords per segment as the
   // "getTotalChords" method here.
   int ibeg = 0;
   for (int i=0; i<vertexCount;) {

      ibeg = i;
      blendEngine->moveToFill(bezier[ibeg].x,bezier[ibeg].y);
      i++;

      while (true) {

         blendEngine->splineToFill(bezier[i-1].xctrlnxt, // control pt
                                   bezier[i-1].yctrlnxt,
                                   bezier[i].xctrlpre,   // control pt
                                   bezier[i].yctrlpre,
                                   bezier[i].x,          // attach  pt
                                   bezier[i].y,
                                   chordLength);         // chord length

         if ((bezier[i].xctrlpre == bezier[ibeg].xctrlpre)&&
             (bezier[i].yctrlpre == bezier[ibeg].yctrlpre)&&
             (bezier[i].x        == bezier[ibeg].x       )&&
             (bezier[i].y        == bezier[ibeg].y       )&&
             (bezier[i].xctrlnxt == bezier[ibeg].xctrlnxt)&&
             (bezier[i].yctrlnxt == bezier[ibeg].yctrlnxt)) {

            i++;
            ibeg = i;
            break;
         }
         else
            i++;
      }
   }

   // loops must be closed
   if (ibeg != vertexCount)
      return IMGTOOL_ERROR_MALLOC;

   // the edge buffer is ready; do the filling
   blendEngine->setDrawColor(255);
   if (bInclusiveRegionFlag)
      blendEngine->fillPolygon();
   else {
      blendEngine->fillPolygonComplement();
   }

   if ((iBlendType == BLEND_TYPE_LINEAR)||(iBlendType == BLEND_TYPE_DITHER)) {
      blendEngine->setPadRadii(iBlendRadiusInterior, iBlendRadiusExterior);
      blendEngine->blendLastFillContour();
      if (iBlendType == BLEND_TYPE_DITHER)
         blendEngine->ditherLastFillContour();
   }

   return 0;
}

int CBezierListUserRegion::renderLabelMask(CROIMask *labelEngine)
{
   // fill the spline using a small chord-length
   int chordLength = 2;
   if (labelEngine->initFill(getTotalChords(chordLength))!=0) { // not enough edges
      chordLength = 4;
   if (labelEngine->initFill(getTotalChords(chordLength))!=0) { // still not enough
      chordLength = 8;
   if (labelEngine->initFill(getTotalChords(chordLength))!=0) return IMGTOOL_ERROR_MALLOC;
   }
   }

   // the method "splineToFill" uses the same algorithm
   // to determine the no of chords per segment as the
   // "getTotalChords" method here.
   int ibeg = 0;
   for (int i=0; i<vertexCount;) {

      ibeg = i;
      labelEngine->moveToFill(bezier[ibeg].x,bezier[ibeg].y);
      i++;

      while (true) {

         labelEngine->splineToFill(bezier[i-1].xctrlnxt, // control pt
                                   bezier[i-1].yctrlnxt,
                                   bezier[i].xctrlpre,   // control pt
                                   bezier[i].yctrlpre,
                                   bezier[i].x,          // attach  pt
                                   bezier[i].y,
                                   chordLength);         // chord length

         if ((bezier[i].xctrlpre == bezier[ibeg].xctrlpre)&&
             (bezier[i].yctrlpre == bezier[ibeg].yctrlpre)&&
             (bezier[i].x        == bezier[ibeg].x       )&&
             (bezier[i].y        == bezier[ibeg].y       )&&
             (bezier[i].xctrlnxt == bezier[ibeg].xctrlnxt)&&
             (bezier[i].yctrlnxt == bezier[ibeg].yctrlnxt)) {

            i++;
            ibeg = i;
            break;
         }
         else
            i++;
      }
   }

   // loops must be closed
   if (ibeg != vertexCount)
      return IMGTOOL_ERROR_MALLOC;

   // the edge buffer is ready; do the filling
   labelEngine->setDrawColor(LABEL_INTERIOR);
   if (bInclusiveRegionFlag)
      labelEngine->fillPolygon();
   else {
      labelEngine->fillPolygonComplement();
   }

   // render the surround region
   labelEngine->setPadRadius(iSurroundRadius);
   labelEngine->setEnableColor(LABEL_NONE);
   labelEngine->setReplaceColor(LABEL_SURROUND);
   labelEngine->labelLastFillContour();

   return 0;
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                              #################################
// ###    CRegionOfInterestClass      ################################
// ####                              #################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRegionOfInterest::CRegionOfInterest(bool needLabelByteMapArg)
{
  listUserRegion.clear();

  maskEngine = new CROIMask;

  ucpBlend = 0;
  ucpLabel = 0;
  needLabelByteMap = needLabelByteMapArg;


  iBlendType = BLEND_TYPE_LINEAR;
  iBlendRadiusInterior = 0;
  iBlendRadiusExterior = 0;
  iSurroundRadius = 0;

  iPitch = 0;
  iMaskAllocSize = 0;

  activePictureRect.left   = MAXFRAMEX;
  activePictureRect.top    = MAXFRAMEY;
  activePictureRect.right  = 0;
  activePictureRect.bottom = 0;
}

CRegionOfInterest::CRegionOfInterest(const CRegionOfInterest &rhs)
{
   *this = rhs;
   maskEngine = new CROIMask;  // Each ROI gets its own!
}

CRegionOfInterest &CRegionOfInterest::operator=(const CRegionOfInterest &rhs)
{
   if (this != &rhs)
   {
      listUserRegion = rhs.listUserRegion;
      //maskEngine = rhs.maskEngine;         NO! Each ROI gets its own!
      ucpBlend = NULL; // NOT: rhs.ucpBlend;
      ucpLabel = NULL; // NOT: rhs.ucpLabel;
      needLabelByteMap = rhs.needLabelByteMap;
      iBlendType = rhs.iBlendType;
      iBlendRadiusInterior = rhs.iBlendRadiusInterior;
      iBlendRadiusExterior = rhs.iBlendRadiusExterior;
      iSurroundRadius = rhs.iSurroundRadius;
      activePictureRect = rhs.activePictureRect;
      iPitch = rhs.iPitch;
      iMaskAllocSize = 0; // NOT: rhs.iMaskAllocSize;

      // Allocate maps if needed
      if (rhs.iMaskAllocSize > 0)
      {
         allocateBytemaps(iPitch, activePictureRect);
      }
   }
   return *this;
}

CRegionOfInterest::~CRegionOfInterest()
{
  MTIfree (ucpBlend);              // was free()
  ucpBlend = 0;
  MTIfree (ucpLabel);              // was free()
  ucpLabel = 0;

  clearRegionList ();

  delete maskEngine;
}

int CRegionOfInterest::allocateBytemaps (int newPitch,
        const RECT &newSubImage)
{
  // set the new active picture
  activePictureRect = newSubImage;

  // set up the bytemap dimensions
  iPitch = newPitch;

  if (iPitch * (activePictureRect.bottom+1) <= iMaskAllocSize)
    return 0;  // allocated storage is already large enough

  // free previously allocated storage
  MTIfree (ucpBlend);                                    // was free
  MTIfree (ucpLabel);                                    // was free

  iMaskAllocSize = iPitch * (activePictureRect.bottom+1);

  // allocate new storage
  ucpBlend = (MTI_UINT8 *) MTImalloc (iMaskAllocSize);   // was malloc
  if (ucpBlend == NULL)
    return IMGTOOL_ERROR_MALLOC;

  if (needLabelByteMap)
   {
    ucpLabel = (MTI_UINT8 *) MTImalloc (iMaskAllocSize);  // was malloc
    if (ucpLabel == NULL)
      return IMGTOOL_ERROR_MALLOC;
   }

  return 0;
}

int CRegionOfInterest::allocateBytemaps (const RECT &newActivePictureRect)
{
  // the pitch is the same as the horizontal dimension of the active picture
  return allocateBytemaps (newActivePictureRect.right+1, newActivePictureRect);
}

void CRegionOfInterest::setBlendType(EBlendType newBlend)
{
  iBlendType = newBlend;
}

void CRegionOfInterest::setBlendRadiusInterior(int newRadius)
{
  if (newRadius < 0)
    newRadius = 0;

  iBlendRadiusInterior = newRadius;

}

void CRegionOfInterest::setBlendRadiusExterior(int newRadius)
{
  if (newRadius < 0)
    newRadius = 0;

  iBlendRadiusExterior = newRadius;

}

void CRegionOfInterest::setSurroundRadius(int newRadius)
{
  if (newRadius < 0)
    newRadius = 0;

  iSurroundRadius = newRadius;

}

void CRegionOfInterest::clearRegionList(bool okToDeleteRegions)
{
  if (okToDeleteRegions)
   {
    // run through each entry and delete the underlying storage
    vector<CUserRegion *>::iterator pos;
    for (pos = listUserRegion.begin(); pos != listUserRegion.end(); pos++)
     {
      delete (*pos);
     }
   }

  // clear the list
  listUserRegion.clear();

}

void CRegionOfInterest::addRegion(const RECT &newRect, bool newInclusionFlag)
{
  CRectUserRegion *urp;
  urp = new CRectUserRegion (newRect, newInclusionFlag);
  urp->setBlendType (iBlendType);
  urp->setBlendRadiusInterior (iBlendRadiusInterior);
  urp->setBlendRadiusExterior (iBlendRadiusExterior);
  urp->setSurroundRadius (iSurroundRadius);
  listUserRegion.push_back(urp);

}

void CRegionOfInterest::addRegion(const POINT *newLasso, bool newInclusionFlag)
{
  CLassoUserRegion *urp;
  urp = new CLassoUserRegion (newLasso, newInclusionFlag);
  urp->setBlendType (iBlendType);
  urp->setBlendRadiusInterior (iBlendRadiusInterior);
  urp->setBlendRadiusExterior (iBlendRadiusExterior);
  urp->setSurroundRadius (iSurroundRadius);
  listUserRegion.push_back(urp);
}

void CRegionOfInterest::addRegion(const BEZIER_POINT *newBezier,
        bool newInclusionFlag)
{
  CBezierUserRegion *urp;
  urp = new CBezierUserRegion (newBezier, newInclusionFlag);
  urp->setBlendType (iBlendType);
  urp->setBlendRadiusInterior (iBlendRadiusInterior);
  urp->setBlendRadiusExterior (iBlendRadiusExterior);
  urp->setSurroundRadius (iSurroundRadius);
  listUserRegion.push_back(urp);
}

void CRegionOfInterest::addRegion(const BEZIER_POINT *newBezier, int vtxcnt,
        bool newInclusionFlag)
{
  CBezierListUserRegion *urp;
  urp = new CBezierListUserRegion (newBezier, vtxcnt, newInclusionFlag);
  urp->setBlendType (iBlendType);
  urp->setBlendRadiusInterior (iBlendRadiusInterior);
  urp->setBlendRadiusExterior (iBlendRadiusExterior);
  urp->setSurroundRadius (iSurroundRadius);
  listUserRegion.push_back(urp);
}

int CRegionOfInterest::renderBlendAndLabelBytemaps ()
{
  // if storage has not been allocated, simply return
  if ((ucpBlend == 0) || (ucpLabel == 0 && needLabelByteMap))
    return -1;

  // clear the blend and label maps
  maskEngine->initMask(iPitch, activePictureRect.bottom+1, ucpBlend);
  maskEngine->clearMask();
  if (needLabelByteMap)
  {
     maskEngine->initMask(iPitch, activePictureRect.bottom+1, ucpLabel);
     maskEngine->clearMask();
  }

  // run through each of the user defined regions
  vector<CUserRegion *>::iterator pos;
  for (pos = listUserRegion.begin(); pos != listUserRegion.end(); pos++)
  {
     // render next region to the ucpBlend...
     maskEngine->initMask(iPitch, activePictureRect.bottom+1, ucpBlend);
     (*pos)->renderBlendMask(maskEngine);

     if (needLabelByteMap)
     {
        // ...and ucpLabel
        maskEngine->initMask(iPitch, activePictureRect.bottom+1, ucpLabel);
       (*pos)->renderLabelMask(maskEngine);
     }

     break; // first version does only one user region (!)
  }

  return 0;
}

int CRegionOfInterest::renderFullFrameBlendAndLabelBytemaps ()
{
  // if storage has not been allocated, simply return
  if (ucpBlend == 0)
    return -1;

  // blend and label maps are all 255's
  maskEngine->initMask(iPitch, activePictureRect.bottom+1, ucpBlend);
  maskEngine->clearMask(0xff);

  if ((ucpLabel != 0) && needLabelByteMap)
  {
     maskEngine->initMask(iPitch, activePictureRect.bottom+1, ucpLabel);
     maskEngine->clearMask(0xff);
  }

  return 0;
}

RECT CRegionOfInterest::getBlendBox ()
{
  RECT retval;
  retval.left   = MAXFRAMEX;
  retval.top    = MAXFRAMEY;
  retval.right  = MINFRAMEX;
  retval.bottom = MINFRAMEY;

  // if storage has not been allocated, simply return
  if (ucpBlend == 0)
    return retval;

  if (listUserRegion.empty())
  {
     retval.left   = activePictureRect.left;
     retval.top    = activePictureRect.top;
     retval.right  = activePictureRect.right;
     retval.bottom = activePictureRect.bottom;
  }
  else
  {
     // run through each of the user defined regions
     vector<CUserRegion *>::iterator pos;
     for (pos = listUserRegion.begin(); pos != listUserRegion.end(); pos++)
     {
        RECT posrct = (*pos)->getBlendBox();

        // accumulate blend box
        if (posrct.left   < retval.left)   retval.left = posrct.left;
        if (posrct.top    < retval.top)    retval.top  = posrct.top;
        if (posrct.right  > retval.right)  retval.right = posrct.right;
        if (posrct.bottom > retval.bottom) retval.bottom = posrct.bottom;

        break; // first version does only one user region (!)
     }

     // clip the resultant box to the limits of the mask
     if (retval.left   < activePictureRect.left)
        retval.left   = activePictureRect.left;
     if (retval.top    < activePictureRect.top)
        retval.top    = activePictureRect.top;
     if (retval.right  > activePictureRect.right)
        retval.right  = activePictureRect.right;
     if (retval.bottom > activePictureRect.bottom)
        retval.bottom = activePictureRect.bottom;
  }

  // return a bounding box for the set {i: ucpBlend[i] > 0}
  return retval;
}

RECT CRegionOfInterest::getLabelBox ()
{
  RECT retval;
  retval.left   = MAXFRAMEX;
  retval.top    = MAXFRAMEY;
  retval.right  = MINFRAMEX;
  retval.bottom = MINFRAMEY;

  // if storage has not been allocated, simply return
  if (ucpLabel == 0)
    return retval;

  if (listUserRegion.empty())
  {
     retval.left   = activePictureRect.left;
     retval.top    = activePictureRect.top;
     retval.right  = activePictureRect.right;
     retval.bottom = activePictureRect.bottom;
  }
  else {

     // run through each of the user defined regions
     vector<CUserRegion *>::iterator pos;
     for (pos = listUserRegion.begin(); pos != listUserRegion.end(); pos++)
     {
        RECT posrct = (*pos)->getLabelBox();

        // accumulate blend box
        if (posrct.left   < retval.left)   retval.left = posrct.left;
        if (posrct.top    < retval.top)    retval.top  = posrct.top;
        if (posrct.right  > retval.right)  retval.right = posrct.right;
        if (posrct.bottom > retval.bottom) retval.bottom = posrct.bottom;

        break; // first version does only one user region (!)
     }

     // clip the resultant box to the limits of the mask
     if (retval.left   < activePictureRect.left  )
        retval.left   = activePictureRect.left;
     if (retval.top    < activePictureRect.top   )
        retval.top    = activePictureRect.top;
     if (retval.right  > activePictureRect.right)
        retval.right  = activePictureRect.right;
     if (retval.bottom > activePictureRect.bottom)
        retval.bottom = activePictureRect.bottom;
  }

  // return a bounding box for the set {i: ucpLabel[i] > 0}
  return retval;
}

#ifdef __NOW_INLINED__

const MTI_UINT8 *CRegionOfInterest::getBlendPtr()
{
  return ucpBlend;
}

MTI_UINT8 *CRegionOfInterest::getLabelPtr()
{
  return ucpLabel;
}

#endif

