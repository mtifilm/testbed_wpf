#ifndef PANSCAN 
#define PANSCAN

#include "imgToolDLL.h"             // Export/Import Defs
//#include "MTIio.h"                  // Defines lseek64, MultiProc headers
#include "machine.h"
#include "mthread.h"
#include "Vertex.h"

class CLineEngine;

class CGraphicEngine;

// this struct will be used to give the field and
// offset for each row of the src and dst frames
struct OFENTRY
{
   int rowfld;
   int rowoff;
};

#define ONE ((MTI_INT32)0x00010000)
#define HLF ((MTI_INT32)0x00008000)

///////////////////////////////////////////////////////////////////////


class MTI_IMGTOOLDLL_API CPanAndScan
{

public:

#define MAXWDTH 2048
#define MAXHGHT 1536

   // wdth of src bitmap
   int wsrc;

   // hght of src bitmap
   int hsrc;

   // src bits per pixel
   int srcBitsPerPixel;

   // src interlaced flag
   bool srcInterlaced;

   // the src row table
   OFENTRY *srcTbl;

   // wdth of dst bitmap
   int wdst;

   // hght of dst bitmap
   int hdst;

   // dst bits per pixel
   int dstBitsPerPixel;

   // dst interlaced flag
   bool dstInterlaced;

   // the dst row table
   OFENTRY *dstTbl;

   // src window
   VERTEX fsrcWin[5];

   // the LR scanning vector
   MTI_INT32 horzX,horzY;

   // the LR per-pixel step
   MTI_INT32 hdelX,hdelY;

   // the UD scanning vector
   MTI_INT32 vertX,vertY;

   // the UD per-pixel step
   MTI_INT32 vdelX,vdelY;

   // the BEG PT for all scans
   MTI_INT32 begX,begY;

   // dst window (holds converted image)
   int dstMinRow;
   int dstMinCol;
   int dstMaxRow;
   int dstMaxCol;
   int dstWinHght;
   int dstWinWdth;

   // rct window (for mat rectangles)
   int rctMinRow;
   int rctMinCol;
   int rctMaxRow;
   int rctMaxCol;
   int rctWinHght;
   int rctWinWdth;

   // number of horiz stripes
   int nStripes;

   // pitch of src line
   int srcPitch;

   // pitch of dst line
   int dstPitch;

   MTI_UINT8   bgnd8[4];

   MTI_UINT8  bgnd10[5];

   MTI_UINT16 bgnd16[3];

   // input  buffer 
   MTI_UINT8 **srcbuf;

   // output buffer
   MTI_UINT8 **dstbuf;

   // one line of mat BGND color
   MTI_UINT8 *linBuf;

   // line-drawing engine

   CLineEngine *linEng;

   // clip-and-scale engine

   CGraphicEngine *grEng;

   // flag to indicate parallel sourcing
   bool parLin;

   // flag to indicate "draw SRC outl"

   bool srcOutl;

   // field buffers for XFORM DVS to YUV
   MTI_UINT8 *xfmBuf[2];

   // multi-thread for XFORM DVS to YUV
   MTHREAD_STRUCT xfmThread;

   // multi-thread for MAT RECTANGLES
   MTHREAD_STRUCT rctThread;

   // multi-thread for STRIPES 
   MTHREAD_STRUCT cvtThread;

   // constructor
   CPanAndScan(
                 int wsrc,  // src wdth
                 int hsrc,  // src hght
                 int srcbp, // bits per pixel
                bool srcil, // interlaced flag
                 int wdst,  // dst wdth
                 int hdst,  // dst hght
                 int dstbp, // bits per pixel
                bool dstil, // interlaced flag
                 int &iRet
              );

   // destructor
   ~CPanAndScan();

/*
	NOTE:  The upper left coordinates of each source and destination
	window are INCLUSIVE.  The lower right coordinates are EXCLUSIVE.
	That is, the upper left pixel location gets rendered, but the 
	lower right pixel location does not.
*/

   // set the source window
   void setSrcWindow(VERTEX *srcwin);

   void setSrcWindow(

                      double x0, double y0,	// upper left corner
                      double x1, double y1,	// upper right corner
                      double x2, double y2,	// lower right corner
                      double x3, double y3	// lower left corner
                    );

   // set the destination window
   void setDstWindow(int minRow,int minCol,int maxRow,int maxCol);

   // set source AND destination windows
   void setWindows(
                        VERTEX *srcwin,

			int minRowDst,
                        int minColDst,
                        int maxRowDst,
                        int maxColDst
                   );

   // set the color to be used for BGND
   void setBGNDColor(
			int Y,
                        int U,
                        int V
                     );

   // render the mat in the specified BGND color
   void renderMat(); 

   // execute the conversion from src to dst buffers
   void convert(MTI_UINT8 **srcbuf, MTI_UINT8 **dstbuf, bool parlin=false);

   // highlight the src window on a display of the orig frame
   void displaySource(MTI_UINT8 **srcbuf, MTI_UINT8 **dstbuf, bool parlin=false);

   // transform DVS 10-bit to YUV 8-bit (no multiprocessing)
   void xfmDVS10toYUV8NoMulti(MTI_UINT8 **srcbf, MTI_UINT8 **dstbf);

   private:

   // set scan vectors (frm src window) 
   void setScanVectors();

};

// prototype of test code
unsigned char *TestPanAndScan();

#endif
