#ifndef CPXLENG
#define CPXLENG
#include "imgToolDLL.h"

// PIXEL-ENGINE class for graphical operations on RGBA frame-buffers  

class MTI_IMGTOOLDLL_API CPixelEngine
{

protected:

   unsigned int 
      *frameBuffer,
      *curaddr;

   int
      dwide,
      dhigh,
      dpitch,

      curx,
      cury;

   unsigned char linePattern;

   unsigned int
      fgndColor,
      bgndColor;

public:

   CPixelEngine();

   ~CPixelEngine();

   void setFrameBuffer(int wdth, int hght, unsigned int *frmbuf);
   unsigned int *getFrameBuffer() const;

   void setLinePattern(unsigned char pat);

#ifdef __BORLANDC_
#define COLOR_RED   0x00ff0000
#define COLOR_GREEN 0x0000ff00
#define COLOR_BLUE  0x000000ff
#define COLOR_BLACK 0x00000000
#define COLOR_WHITE 0x00ffffff
#endif
#ifdef __sgi
#define COLOR_RED   0xff000000
#define COLOR_GREEN 0x00ff0000
#define COLOR_BLUE  0x0000ff00
#define COLOR_BLACK 0x00000000
#define COLOR_WHITE 0xffffff00
#endif

   void setFGColor(unsigned int);
   void setBGColor(unsigned int);

   void clearFrameBuffer();
   void drawRectangle(int l,int b, int r, int t);
   void drawBitmap(int l, int t, int wdth, int hght, unsigned int *bitmap);

   void moveTo(int x, int y);
   void lineTo(int x, int y);
   void arcTo(int x, int y, double xctr, double yctr);
   void drawDot();
   void drawText(int x, int y, char *txt);
};

class MTI_IMGTOOLDLL_API CPixelEngine8
{

protected:

   unsigned char
      *frameBuffer,
      *curaddr;

   int
      dwide,
      dhigh,
      dpitch,

      curx,
      cury;

   unsigned char linePattern;

   unsigned char
      fgndColor,
      bgndColor;

public:

   CPixelEngine8();

   ~CPixelEngine8();

   void setFrameBuffer(int wdth, int hght, unsigned char *frmbuf);
   unsigned char *getFrameBuffer() const;

   void setLinePattern(unsigned char pat);

   void setFGColor(unsigned char);
   void setBGColor(unsigned char);

   void clearFrameBuffer();
   void drawRectangle(int l,int b, int r, int t);
   void drawBitmap(int l, int t, int wdth, int hght, unsigned char *bitmap);

   void moveTo(int x, int y);
   void lineTo(int x, int y);
   void arcTo(int x, int y, double xctr, double yctr);
   void drawDot();
   void drawText(int x, int y, char *txt);
};
#endif

