/*
	File:	AspectRatioAM.h
	By:	Kevin Manbeck
	Date:	Oct 5, 2004

	AspectRatioAM is used to convert anamorphic images into
	their anamorphic version.  The conversion must be very
	efficient so that it can be done in real time.
*/

#include "AspectRatioAM.h"
#include "ImageFormat3.h"
#include "IniFile.h"      // for TRACE_
#include "err_imgtool.h"
#include "MTImalloc.h"

CAspectRatioAM::CAspectRatioAM()
{
  bInit = false;
}

CAspectRatioAM::~CAspectRatioAM()
{
}

void CAspectRatioAM::Free()
{
  bInit = false;
}

int CAspectRatioAM::Init (const CImageFormat *ifp)
{
  int iRet;

  // make local copy of parameters
  iNRow = ifp->getLinesPerFrame();
  iNCol = ifp->getPixelsPerLine();
  iPitch = ifp->getFramePitch();
  iPixelPacking = ifp->getPixelPacking();
  iColorSpace = ifp->getColorSpace();
  iNField = ifp->getFieldCount();

  // check for error conditions
  if (iPixelPacking != IF_PIXEL_PACKING_8Bits_IN_1Byte)
   {
    TRACE_0 (errout << "This pixel packing (" << iPixelPacking <<
		") is not supported by CAspectRatioAM");
    return ERR_ASPECT_RATIO_PIXEL_PACKING;
   }

  if (
	iColorSpace != IF_COLOR_SPACE_CCIR_601_BG &&
	iColorSpace != IF_COLOR_SPACE_CCIR_601_M &&
	iColorSpace != IF_COLOR_SPACE_CCIR_709
     )
   {
    TRACE_0 (errout << "This color space (" << iColorSpace <<
		") is not supported by CAspectRatioAM");
    return ERR_ASPECT_RATIO_COLOR_SPACE;
   }

  if (iNField != 2)
   {
    TRACE_0 (errout << "This field count (" << iNField <<
		") is not supported by CAspectRatioAM");
    return ERR_ASPECT_RATIO_NUM_FIELD;
   }

  // the size of each line of data
  iBytePerLine = iNCol * 2;

  bInit = true;

  // the pixel aspect ratio is a = (16 * image_height) / (9 * image_width).
  // When doing a matte, the new height = (image_width*a) / (matte_ratio)
  // or new height = (16/9)*image_height / matte_ratio.  Define fMatteFactor
  // to be (16/9)*image_height.

  fRowMatteFactor = (16. * (float)iNRow) / 9.;

  // when matte_ratio < 1.778, we use the formula (9/16)*image_width

  fColMatteFactor = (9.* (float)iNCol) / 16.;

  return 0;
}
int CAspectRatioAM::Render (MTI_UINT8 *ucpSrc[2], MTI_UINT8 *ucpDst[2])
{
  int iRet = 0;

  if (bInit == false)
   {
    return ERR_ASPECT_RATIO_INIT;
   }
 
  if (
	iPixelPacking == IF_PIXEL_PACKING_8Bits_IN_1Byte &&
       (iColorSpace == IF_COLOR_SPACE_CCIR_601_BG ||
	iColorSpace == IF_COLOR_SPACE_CCIR_601_M ||
	iColorSpace == IF_COLOR_SPACE_CCIR_709) &&
	iNField == 2
     )
   {
    // this is 8 bit YUV data with 2 fields
    iRet = Render_8_YUV_2 (ucpSrc, ucpDst);
   }
  else
   {
    iRet = ERR_ASPECT_RATIO_RENDER;
   }

  return iRet;
}

int CAspectRatioAM::Render_8_YUV_2 (MTI_UINT8 *ucpSrc[2], MTI_UINT8 *ucpDst[2])
{
  // this code is used to render Y 8-bit YUV data with two fields

  int iRow, iField, iR;
  MTI_UINT8 *ucpS, *ucpD;

  for (iRow = 0; iRow < iNRow; iRow++)
   {
    iField = iRow % 2;
    iR = iRow / 2;

    ucpD = ucpDst[iField] + (iR * iPitch);
    ucpS = ucpSrc[iField] + (iR * iPitch);

    // copy from the source to the destination
    MTImemcpy ((void *)ucpD, (void *)ucpS, iBytePerLine);
   }

  return 0;
}

int CAspectRatioAM::getMatteCoordinates (int iJustify, float fMatteRatio,
	int &iRow0, int &iRow1, int &iCol0, int &iCol1)
{

  iRow0 = 0;
  iRow1 = 0;
  iCol0 = 0;
  iCol1 = 0;

  if (bInit == false)
   {
    return ERR_ASPECT_RATIO_INIT;
   }

  if (fMatteRatio >= 1.78)
   {
    int iNewRow = fRowMatteFactor / fMatteRatio + .5;

    if (iJustify == -1)
     {
      // top justify
      iRow0 = 0;
     }
    else if (iJustify == 0)
     {
      // center
      iRow0 = (iNRow - iNewRow)/2;
     }
    else
     {
      // bottom align
      iRow0 = iNRow - iNewRow;
     }
    iRow1 = iRow0 + iNewRow;
   }
  else if (fMatteRatio > 0.)
   {
    int iNewCol = fColMatteFactor * fMatteRatio + .5;

    // always perform a center justification for side mattes
    iCol0 = (iNCol - iNewCol)/2;
    iCol1 = iCol0 + iNewCol;
   }
  else
   {
    // no mattes when ratio is zero
   }


  return 0;
}
