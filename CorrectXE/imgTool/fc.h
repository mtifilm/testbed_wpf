/*
     fc.h
*/

#ifndef FC_H
#define FC_H

#include "imgToolDLL.h"


#ifndef CODE_TRACE
#ifdef CODE_TRACER
#define CODE_TRACE(x) {printf(x); printf("\n");}
#else
#define CODE_TRACE(x)
#endif
#endif

#define PIXELVALS_UNKNOWN 0
#define PIXELVALS_YUV_422 1
#define PIXELVALS_YUV_444 2

#define TRUE 1
#define FALSE 0

#define BASE_MATRIX_1D_SPLINE 1
#define BASE_MATRIX_2D_4X4_PLANE 2
#define BASE_MATRIX_2D_4X4_FULL_PLANE 3

/* Never be bigger than 99.0 and also use power of 2 */
#define FC_ALLOWED_OFFSET_ACCURACY (16.0)

/***********************************************************************/
/* These must be defined correctly. 
   Options: 
     16,14 or 16,10 (best for up convertion) 
           or 16,5 (best for up convertion text) for 2-D splines. */
/* After 11/23/98 The following values can NOT be changed !!! */
#define MAGNIFY_EXPERIMENT_GENERIC_SPLINE_NUM_DATA 16
#define MAGNIFY_EXPERIMENT_GENERIC_SPLINE_NUM_POWER 10 /* At most 1 less than
                                                         above */
/* Must be carefully defined (2.0, 0.2 is the best). */
#define WEIGHT_FUNCTION_TYPE_1_POWER 2.0
#define FRACTION_BOUND 0.2

/***********************************************************************
Edit the ratio by searching MagnifyThisFrame backward. */


struct MTI_IMGTOOLDLL_API MAGNIFY_CONTROL
{
  int iSize; /* Size of the structure. */
  int iDoResetEngine;
  int iSearchFlatRegion;
  int iSearchFlatRegionSize;
  int iFlatRegionVaryingLimitPercentY;
  int iFlatRegionVaryingLimitPercentUV;
  int iActive; /* Indicate whether this struct has been filled or not. */
  int iDoShrink;
  int iDoShrinkFilter;
  int iBlackVal0, iBlackVal1, iBlackVal2;
  int iMinVal0, iMinVal1, iMinVal2;
  int iMaxVal0, iMaxVal1, iMaxVal2;

  float fStartFrameRowSrc,fStartPixelColSrc;
  int iStartFrameRowDes,iStartPixelColDes;
  float fSrcConvertHeight,fSrcConvertWidth;
  int iDesConvertHeight,iDesConvertWidth;
  int iInterlacingSrc,iInterlacingDes;
  int iPixelVals; /* Same for both src and dst. */
  int iNFrameRowsSrc,iNPixelColsSrc;
  int iNFrameRowsDes,iNPixelColsDes;
  int iMinRowsInPreCalTable,iMinColsInPreCalTable;
  float fFirstDesRowOffsetInSrcStart;
  float fFirstDesColOffsetInSrcStart;

  string strParfileDir;

  unsigned char *ucpSrc0,*ucpSrc1;
  unsigned char *ucpDes0,*ucpDes1;
  unsigned char *ucpTemp0,*ucpTemp1;
  unsigned char *ucpSrcMask;

  int iRoundOneNeeded;
  int iParFileExists;

/* Table layout:
     0-15: UV parameters for 4x4 neighborhood.
     16-47: Y parameters for overlapped 4x4 neighborhoods (total 32);
     48-51: UV parameters for 2x2 neighborhood.
     52-55: Y parameters for 2x2 neighborhood.
*/
  float *fpTotalPreCal;

  char caParFileName[256];

  void
    *vpCriticalSectionParFileIO;
		// used to prevent simultanous access to par files


  // PUBLIC METHODS
  void MagnifyThisFrame();

  // Added constructor/destructor to do auto InitMagnify()
  // & QuitMagify() so the caller no longer has to (unless
  // they want to re-use the control, I guess).
  MAGNIFY_CONTROL() { Init(); };
  ~MAGNIFY_CONTROL() { Quit(); };

private:

  int iTableIsMade;

  float fWeighTable[1024];

  // This doesn't seem to be used
  //unsigned char ucaResultsOf1DSpline
    //[MAGNIFY_EXPERIMENT_GENERIC_SPLINE_NUM_DATA];

  float faInverseMatrix
    [MAGNIFY_EXPERIMENT_GENERIC_SPLINE_NUM_POWER+1]
    [MAGNIFY_EXPERIMENT_GENERIC_SPLINE_NUM_DATA];

  float faInverseMatrixTranspose
    [MAGNIFY_EXPERIMENT_GENERIC_SPLINE_NUM_DATA]
    [MAGNIFY_EXPERIMENT_GENERIC_SPLINE_NUM_POWER+1];

  int iInverseMatrixComputed;

  void ComputePreCalTableSize(float *fpStartPosSrc,float *fpSrcLength,
    int iDstLength,int *ipMinNumDesPointsInTable,
    float *fpInitialOffsetInTable);

  void DetermineLowFilterSize(float fFactor,int *ipSize);

  void Fill2DLowPassFilerParamters(int iNumLowPassFilterRows,
    int iNumLowPassFilterCols,float fRowFactor,float fColFactor,
    float *fpLowPassFilerParamters);

  void LowPassFilterOnThisFrame(unsigned char *ucpSrc0,
    unsigned char *ucpSrc1,unsigned char *ucpDst0,unsigned char *ucpDst1,
    int iTotalNumRows,int iTotalNumCols,int iInterlacing,
    int iNumLowPassFilterRows,int iNumLowPassFilterCols,
    float *fpLowPassFilerParamters);

  void LowPassFilterOnThisFrame444(unsigned char *ucpSrc0,
    unsigned char *ucpSrc1,unsigned char *ucpDst0,unsigned char *ucpDst1,
    int iTotalNumRows,int iTotalNumCols,int iInterlacing,
    int iNumLowPassFilterRows,int iNumLowPassFilterCols,
    float *fpLowPassFilerParamters);

  void BlackOutThisFrame(unsigned char *ucpCurrent0,
    unsigned char *ucpCurrent1,int iNFrameRows,int iNPixelCols,
    int iInterlacing,int iPixelValsSrc,int iPixelValsDes, int iBlackVal0,
    int iBlackVal1, int iBlackVal2);

  void ComputeInvertMatrix(int iBaseX,int iNumDataTerms,int iNumPowerTerms,
    float *dResultMatrix,float *dResultMatrixTranspose);

  void MakeWeightFunctionTable();

  float WeightFunction(float dFraction);

  void TwoDimension4x4Spline(
    float *ufDes,
    int iDataInc,
    float fRowFraction,
    float fColFraction,
    float *dpCoefficient);

  float OverLapWeightFunction(float dFraction);

  void Generic2dSplineForUYorVY(
    float *fpTableForUV,
    float *fpTableForY,
    float fRowFraction,
    float fColFractionUV,
    float fColFractionY,
    int iNeighborType);

  void MakeParFilename(char *cpFilename);
  void DoSaveParFile();
  void SaveParFile();
  int DoLoadParFile();
  int LoadParFile();

  int Initialize();
  void Free();
  void MagnifyThisFrameYuv422();
  void MagnifyThisFrameYuv444();
  int Allocate();
  void ClipToRange();

  float fOrigRow, fOrigCol, fOrigHeight, fOrigWidth;
  int iOrigRow, iOrigCol, iOrigHeight, iOrigWidth;

  void ConformDimension (int iForConform);

  // I hid these because there really isn't much reason for
  // calling them, now that the constructor and destructor
  // init and clean up.
  void Init ();
  void Quit();
  friend void InitMagnify (MAGNIFY_CONTROL *mcp);
  friend void QuitMagnify(MAGNIFY_CONTROL *mcp);


};  /* MAGNIFY_CONTROL */


/* legacy entry points */

inline void InitMagnify (MAGNIFY_CONTROL *mcp)
{
  mcp->Init ();
};

inline void MagnifyThisFrame (MAGNIFY_CONTROL *mcp)
{
  mcp->MagnifyThisFrame ();
};

inline void QuitMagnify (MAGNIFY_CONTROL *mcp)
{
  mcp->Quit ();
};

#endif // FC_H
