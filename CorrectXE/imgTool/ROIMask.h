//
// include file for class CROIMask
//
#ifndef ROIMASK_H
#define ROIMASK_H

#include "machine.h"
#include "imgToolDLL.h"             // Export/Import Defs

//
// First version modelled after original CMask, but with 8-bit frame buffer
// second version will use a different method to do the fill and will support
// Bezier splines as an intrinsic datatype
//

#define SMALL
#ifdef SMALL
typedef MTI_UINT16 EDGINDEX;
#define NULLEDG (EDGINDEX) 0xffff
#else
typedef MTI_UINT32 EDGINDEX;
#define NULLEDG (EDGINDEX) 0xffffffff
#endif

// using 16-bit edge indices, size is 26 bytes
// using 32-bit edge indices, size is 32 bytes

struct EDGE {

   EDGINDEX inxtlex;
   EDGINDEX ifwd;
   EDGINDEX ibwd;

   MTI_INT16 xbot;
   MTI_INT16 ybot;
   MTI_INT16 xtop;
   MTI_INT16 ytop;
   MTI_INT16 xmin;
   MTI_INT16 xmax;

   MTI_INT16 pen;
   MTI_INT16 brx;
   MTI_INT16 bry;

   MTI_UINT8 edgCod;
};

class MTI_IMGTOOLDLL_API CROIMask
{

public:

   CROIMask();

   ~CROIMask();

   void initMask(int wdth, int hght, MTI_UINT8 *frm);

   void clearMask(MTI_UINT8 col=0);

   void setActiveRegion(int lft, int top,
                        int rgt, int bot);

   void setDrawColor(MTI_UINT8);

   void fillActiveRegion();

   void fillRectangle(int lft, int top,
                      int rgt, int bot);

#define ROIMASK_ERR_INSUFF_EDGES -1
   int initFill(int edges);

   void moveToFill(int, int);

   void lineToFill(int, int);

   void splineToFill(int x1, int y1,
                     int x2, int y2,
                     int x3, int y3,
                     int chordleng);

   void fillPolygon();

   void fillPolygonComplement();

   void setPadRadius(int);

   void setPadRadii(int, int);

   MTI_UINT8 getBlendLevel(double);

   void setEnableColor(MTI_UINT8);

   void setReplaceColor(MTI_UINT8);

   void moveTo(int, int);

   void blendLastFillContour(); // blend after polygonFill

   void ditherLastFillContour(); // dither after blending

   void labelLastFillContour(); // blend after polygonFill

private:

   // sort the edges in lexicographic order
   EDGINDEX iedgeSort(EDGINDEX first, EDGINDEX last);

   // insert an edge-record into the scan
   // line list between two other edges
   void insertEdge(EDGINDEX iedg, EDGINDEX ipre, EDGINDEX inxt);

   // delete an edge-record from the
   // scan line list
   void deleteEdge(EDGINDEX);

   // draw a horizontal line from minx
   // to maxx at the current cury level
   void hatchLine(int lft, int rgt);

   // edge stretch methods
   static void StretchXEdge0(EDGE *);
   static void StretchXEdge1(EDGE *);
   static void StretchXEdge2(EDGE *);
   static void StretchXEdge3(EDGE *);
   static void StretchXEdge4(EDGE *);
   static void StretchXEdge5(EDGE *);
   static void StretchXEdge6(EDGE *);
   static void StretchXEdge7(EDGE *);

   // dispatch table for same
   void (*strTbl[8])(EDGE *);

   // edge step methods
   static void StepYEdge0(EDGE *);
   static void StepYEdge1(EDGE *);
   static void StepYEdge2(EDGE *);
   static void StepYEdge3(EDGE *);
   static void StepYEdge4(EDGE *);
   static void StepYEdge5(EDGE *);
   static void StepYEdge6(EDGE *);
   static void StepYEdge7(EDGE *);

   // dispatch table for same
   void (*stpTbl[8])(EDGE *);

private:

   int maskWdth,
       maskHght;

   // active region of frame buffer
   // (all coords INCLUSIVE)
   RECT region;

   int dwide,
       dhigh,
       dpitch;

   MTI_UINT8 *bytbuf;

   int curx,
       cury;

   MTI_UINT8 *curptr;

   MTI_UINT8 fcolr;

#define EDGES_INIT 2048
   int  edgcnt;
   EDGE *edgbuf;

   EDGINDEX iedgptr;
   EDGINDEX ilexlst;
   EDGINDEX iedglst;

   // padding radius
   int padRadius,
       padRadiusSq;

   int padRadiusInterior,
       padRadiusExterior;

   int padWidthTotal;

   double blendStep;
   double blendIntercept;

   // enable color
   MTI_UINT8 enableColor;

   // replace color
   MTI_UINT8 replaceColor;
};

#endif
