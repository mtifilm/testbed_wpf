#ifndef ERRIMGTOOL_H
#define ERRIMGTOOL_H

// allowed range for error values:  -4850 -- -4999


		// -4850 to -4874 used for ImageFileIO
#define IMGTOOL_ERROR_SEEK                       -4850 // obsolete
#define IMGTOOL_ERROR_READ                       -4851 // obsolete
#define IMGTOOL_ERROR_UNSUPPORTED_OPTION         -4852 // obsolete
#define IMGTOOL_ERROR_UNKNOWN_OPTION             -4853 // obsolete
#define IMGTOOL_ERROR_BAD_BIT_DEPTH              -4854 // obsolete
#define IMGTOOL_ERROR_BAD_IMAGE_SIZE             -4855 // obsolete
#define IMGTOOL_ERROR_BAD_COMPONENTS             -4856 // obsolete
#define IMGTOOL_ERROR_UNKNOWN_FILE_TYPE          -4857 // obsolete
#define IMGTOOL_ERROR_PARAMETER_MISMATCH         -4858 // obsolete
#define IMGTOOL_ERROR_WRITE                      -4859 // obsolete
#define IMGTOOL_ERROR_MALLOC                     -4860
#define IMGTOOL_ERROR_OPEN                       -4861 // obsolete
#define IMGTOOL_ERROR_BAD_DIMENSION              -4862 // obsolete
#define IMGTOOL_ERROR_UNSUPPORTED_COMMAND        -4863 // obsolete
#define IMGTOOL_ERROR_BAD_MAGIC_NUMBER           -4864 // obsolete

#define ERR_ASPECT_RATIO_PIXEL_PACKING           -4900
#define ERR_ASPECT_RATIO_COLOR_SPACE             -4901
#define ERR_ASPECT_RATIO_MALLOC                  -4902
#define ERR_ASPECT_RATIO_BLACK_LINE              -4903
#define ERR_ASPECT_RATIO_INIT                    -4904
#define ERR_ASPECT_RATIO_RENDER                  -4905
#define ERR_ASPECT_RATIO_NUM_FIELD               -4906
#define ERR_ASPECT_RATIO_SIZE_TOO_BIG            -4907
#define ERR_ASPECT_RATIO_FACTOR_TOO_BIG          -4908
#define ERR_ASPECT_RATIO_UNPACKED                -4909
#define ERR_ASPECT_RATIO_UNPACKED_V              -4910


/*
  Alpha order
IMGTOOL_ERROR_BAD_BIT_DEPTH - OBSOLETE
	the bit depth or packing of the file is not supported
IMGTOOL_ERROR_BAD_COMPONENTS - OBSOLETE
	the number of components in the file is not supported
IMGTOOL_ERROR_BAD_IMAGE_SIZE - OBSOLETE
	the size of the image does not match the header info
IMGTOOL_ERROR_BAD_MAGIC_NUMBER - OBSOLETE
	the image does not have a valid MAGIC number in the header
IMGTOOL_ERROR_BAD_DIMENSION - OBSOLETE
	The image dimensions are bad
IMGTOOL_ERROR_MALLOC
	Trouble allocating storage
IMGTOOL_ERROR_OPEN - OBSOLETE
	trouble opening file
IMGTOOL_ERROR_PARAMETER_MISMATCH - OBSOLETE
	current file parameters do not match prototype, or first, image
IMGTOOL_ERROR_READ - OBSOLETE
	trouble reading from file
IMGTOOL_ERROR_SEEK - OBSOLETE
	trouble seeking in file
IMGTOOL_ERROR_UNSUPPORTED_OPTION - OBSOLETE
	an option in the header is not supported
IMGTOOL_ERROR_UNKNOWN_FILE_TYPE - OBSOLETE
	this file type is not supported
IMGTOOL_ERROR_UNKNOWN_OPTION - OBSOLETE
	this option is not known
IMGTOOL_ERROR_UNSUPPORTED_COMMAND - OBSOLETE
	this command is not supported
IMGTOOL_ERROR_WRITE - OBSOLETE
	trouble writing to hard disk
*/

		// -4875 to -4889 used for ColorSpaceConvert
#define IMGTOOL_ERROR_VERTICAL_DIM		    -4875
#define IMGTOOL_ERROR_HORIZONTAL_DIM	    -4876
#define IMGTOOL_ERROR_BAD_COLORSPACE	    -4877
#define IMGTOOL_ERROR_PIXEL_COMPONENT	    -4878
#define IMGTOOL_ERROR_BAD_PACKING		    -4879
#define IMGTOOL_ERROR_BAD_PROCESS_FLOW	    -4880
#define IMGTOOL_ERROR_NOT_INITIALIZED	    -4881

/*
  Alpha order
IMGTOOL_ERROR_VERTICAL_DIM
	source and destination do not have same vertical size
IMGTOOL_ERROR_HORIZONTAL_DIM
	source and destination do not have same horizontal size
IMGTOOL_ERROR_BAD_COLORSPACE
	bad value for color space parameter
IMGTOOL_ERROR_PIXEL_COMPONENT
	bad value for pixel component parameter
IMGTOOL_ERROR_BAD_PACKING
	bad value for pixel packing
IMGTOOL_ERROR_BAD_PROCESS_FLOW
	process flow is not correct
IMGTOOL_ERROR_NOT_INITIALIZED
	color space converter was not initialized
*/

        // -4890 to -4899 used for CallPanScan and PanAndScan
#define IMGTOOL_ERROR_PAN_SCAN_FILE_CREATE  -4890
#define IMGTOOL_ERROR_PAN_SCAN_FILE_OPEN    -4891
#define IMGTOOL_ERROR_PAN_SCAN_FILE_WRITE   -4892
#define IMGTOOL_ERROR_PAN_SCAN_FILE_READ    -4893
#define IMGTOOL_ERROR_PAN_SCAN_UNSUPPORTED_BIT_DEPTH_COMBO -4894
#define IMGTOOL_ERROR_PAN_SCAN_CANT_SPAWN_THREAD           -4895

        //  -4900 to -4909 used for MultiProcess
#define IMGTOOL_ERROR_CANT_SPAWN_THREAD     -4900
#define IMGTOOL_ERROR_CANT_ALLOC_CRIT_SECT  -4901

        // -4910 to -4919 used for MediaIO
#define IMGTOOL_ERROR_CLIP_DOES_NOT_EXIST   -4910
#define IMGTOOL_ERROR_CLIP_IS_CORRUPT       -4911
#define IMGTOOL_ERROR_BAD_FILE_TYPE         -4912
#define IMGTOOL_ERROR_BAD_FILE_EXTENSION    -4913
#define IMGTOOL_ERROR_CANT_GEN_FILENAME     -4914
#define IMGTOOL_ERROR_NO_AUDIO              -4915

        // -4920 to -4925 used for CBufferPool
#define IMGTOOL_ERROR_BUFFER_POOL_ALLOC_FAILED      -4920
#define IMGTOOL_ERROR_BUFFER_POOL_BUFFER_NOT_FOUND  -4921
#define IMGTOOL_ERROR_BUFFER_POOL_WAS_DESTROYED     -4922
#define IMGTOOL_ERROR_NO_AVAILABLE_BUFFERS          -4923
#define IMGTOOL_ERROR_NULL_PTR                      -4924

        // -4926 to -4930 used for CSaveRestore
#define IMGTOOL_ERROR_SAVE_RESTORE_MALLOC       -4926

        // -4931 to -4940 used for format conversion
#define IMGTOOL_ERROR_BAD_IN_TIMECODE       -4931
#define IMGTOOL_ERROR_BAD_OUT_TIMECODE      -4932
#define IMGTOOL_ERROR_BOGUS_PARAMETER       -4933
#define IMGTOOL_ERROR_CANT_OPEN_CLIP        -4934



/*
IMGTOOL_ERROR_BUFFER_POOL_ALLOC_FAILED
   Memory allocation failed in CBufferPool, probably not enough available
   memory
*/

#endif
