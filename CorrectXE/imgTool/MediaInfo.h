#include "timecode.h"
#include <string>

enum EMediaClass
 {
  MEDIA_CLASS_UNKNOWN,
  MEDIA_CLASS_CLIP,
  MEDIA_CLASS_FILES
 };

enum EDefClass
 {
  DEF_CLASS_UNKNOWN,
  DEF_CLASS_SD,      // Standard def
  DEF_CLASS_HD,      // High def
  DEF_CLASS_2K       // 2k wide, 4:3
 };


struct SMediaInfo
 {
  EMediaClass mediaClass;
  string formatName;    // QQQ Passing strings around is a really bad idea
  CTimecode timeIn;
  CTimecode timeOut;
  int handleCount;

  // Video info
  EDefClass defClass;
  float aspectRatio;    // frame aspect ratio
  int linesPerFrame;
  int pixelsPerLine;

  // Audio info
  int audioChannelCount;
  int bytesPerSample;
  float samplesPerField;  // not necessarily an integer!
  float sampleFrequency;


  SMediaInfo() :  timeIn(0), timeOut(0) {};
 };

