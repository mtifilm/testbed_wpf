
#ifndef MaskToolRegionsH
#define MaskToolRegionsH

// MaskToolRegions.h: interface for the CMaskToolRegionList class.
//
/*
$Header: /usr/local/filmroot/imgTool/include/MaskToolRegions.h,v 1.7.2.4 2009/02/06 22:32:22 mbraca Exp $
*/
///////////////////////////////////////////////////////////////////////////////

#include "machine.h"
#include "imgToolDLL.h"             // Export/Import Defs
#include <vector>
using std::vector;

///////////////////////////////////////////////////////////////////////////////
// Forward Declarations


//////////////////////////////////////////////////////////////////////


enum EMaskRegionShape
{
   MASK_REGION_INVALID = 0,
   MASK_REGION_RECT,
   MASK_REGION_LASSO,
   MASK_REGION_BEZIER,
   MASK_REGION_QUAD
};

//enum EMaskBlendPosition
//{
//   MASK_BLEND_POSITION_INVALID = 0,
//   MASK_BLEND_OUTSIDE,     // Blend zone is outside of outline of mask region
//   MASK_BLEND_INSIDE,      // Blend zone is inside of outline of mask region
//   MASK_BLEND_CENTERED,     // Blend zone is equally inside and outside of the
//                           // outline of the mask region
//};

#define LEFT  1
#define ABOVE 2

//////////////////////////////////////////////////////////////////////

class MTI_IMGTOOLDLL_API CMaskToolRegion
{
public:
   CMaskToolRegion();
   CMaskToolRegion(const CMaskToolRegion &src);
   virtual ~CMaskToolRegion();

   CMaskToolRegion& operator=(const CMaskToolRegion& rhs);

   // Virtual "copy constructor"
   virtual CMaskToolRegion* clone() const = 0;
   // Virtual "assignment operator"
   virtual CMaskToolRegion& assign(const CMaskToolRegion& src) = 0;

   virtual EMaskRegionShape GetRegionShape() const = 0;

   virtual bool IsInside(int x, int y) = 0;

   int GetTCode(int x, int y);

   int GetBorderWidth() const;
   RECT GetBoundingBox() const;
   ////EMaskBlendPosition GetBlendPosition() const;
   double GetBlendSlew() const;

#ifdef USE_PER_REGION_INCLUSION_FLAGS
   bool GetInclusionFlag() const;
#endif
   bool GetHiddenFlag() const;

#ifdef USE_PER_REGION_INCLUSION_FLAGS
   void SetInclusionFlag(bool newInclusionFlag);
#endif
   void SetHiddenFlag(bool newHiddenFlag);
   void SetBorderWidth(int newBorderWidth);
////   void SetBlendPosition(EMaskBlendPosition newBlendPosition);
   void SetBlendSlew(double value);

   virtual CMaskToolRegion* Interpolate(const CMaskToolRegion &region2,
                                        double intFactor) = 0;

   int InterpolateInt(int d1, int d2, double t);

protected:
   RECT boundingBox;  // derived types must set the bounding box in constructor
   
private:
   bool isSelected;
#ifdef USE_PER_REGION_INCLUSION_FLAGS
   bool inclusionFlag;
#endif
   bool hiddenFlag;
   int _borderWidth = 0;
   double _blendSlew = 0.0;
//   EMaskBlendPosition blendPosition;
};

//////////////////////////////////////////////////////////////////////

// NB: for now this is just a place-holder for a Bezier region
//     in the region list

class MTI_IMGTOOLDLL_API CBezierMaskToolRegion : public CMaskToolRegion
{
public:
   CBezierMaskToolRegion(BEZIER_POINT *newBezier);
   CBezierMaskToolRegion(const CBezierMaskToolRegion &src);
   virtual ~CBezierMaskToolRegion();

   CBezierMaskToolRegion& operator=(const CBezierMaskToolRegion &rhs);

   // Virtual "copy constructor"
   CMaskToolRegion* clone() const {return new CBezierMaskToolRegion(*this);};
   // Virtual "assignment operator"
   CMaskToolRegion& assign(const CMaskToolRegion& src);

   BEZIER_POINT* GetBezierPointer();
   int GetBezierPointCount();
   virtual EMaskRegionShape GetRegionShape() const {return MASK_REGION_BEZIER;};

   virtual bool IsInside(int x, int y);

   virtual CMaskToolRegion* Interpolate(const CMaskToolRegion &region2,
                                        double intFactor);

   void SetBezierPoints(BEZIER_POINT *newBezier);

private:

#define LEFT 1
#define ABOV 2
   int  tCode(double, double);
   int  jordanCrossing(double, double, double, double);
   void copyBezierPoints(BEZIER_POINT *newBezier);
   void deleteBezierPoints();
   void setBoundingBox();

private:
   BEZIER_POINT *bezierPoints;
   int pointCount;
};

//////////////////////////////////////////////////////////////////////

class MTI_IMGTOOLDLL_API CLassoMaskToolRegion : public CMaskToolRegion
{
public:
   CLassoMaskToolRegion(POINT *newLasso);
   CLassoMaskToolRegion(const CLassoMaskToolRegion &src);
   virtual ~CLassoMaskToolRegion();

   CLassoMaskToolRegion& operator=(const CLassoMaskToolRegion &rhs);

   // Virtual "copy constructor"
   CMaskToolRegion* clone() const {return new CLassoMaskToolRegion(*this);};
   // Virtual "assignment operator"
   CMaskToolRegion& assign(const CMaskToolRegion& src);

   virtual EMaskRegionShape GetRegionShape() const {return MASK_REGION_LASSO;};

   virtual bool IsInside(int x, int y);

   POINT* GetLassoPointer();
   int GetLassoVertexCount();

   virtual CMaskToolRegion* Interpolate(const CMaskToolRegion &region2,
                                        double intFactor);

private:
   void copyLassoPoints(POINT *newLasso);
   void deleteLassoPoints();
   void setBoundingBox();

private:
   POINT *lasso;
   int pointCount;
};

//////////////////////////////////////////////////////////////////////

class MTI_IMGTOOLDLL_API CRectMaskToolRegion : public CMaskToolRegion
{
public:
   CRectMaskToolRegion(const RECT &newRect);
   CRectMaskToolRegion(const CRectMaskToolRegion &src);
   virtual ~CRectMaskToolRegion();

   CRectMaskToolRegion& operator=(const CRectMaskToolRegion &rhs);

   // Virtual "copy constructor"
   CMaskToolRegion* clone() const {return new CRectMaskToolRegion(*this);};
   // Virtual "assignment operator"
   CMaskToolRegion& assign(const CMaskToolRegion& src);

   virtual EMaskRegionShape GetRegionShape() const {return MASK_REGION_RECT;};

   virtual bool IsInside(int x, int y);

   RECT GetRect();

   virtual CMaskToolRegion* Interpolate(const CMaskToolRegion &region2,
                                        double intFactor);

private:
   void copyRect(const RECT &newRect);

private:
   RECT rect;
};

// ---------------------------------------------------------------------------

// Four-sided region.

class MTI_IMGTOOLDLL_API CQuadMaskToolRegion : public CMaskToolRegion
{
public:
   CQuadMaskToolRegion(const POINT *newQuad);
   CQuadMaskToolRegion(const RECT &newRect);
   CQuadMaskToolRegion(const CQuadMaskToolRegion &src);
   virtual ~CQuadMaskToolRegion();

   CQuadMaskToolRegion& operator=(const CQuadMaskToolRegion &rhs);

   // Virtual "copy constructor"
   CMaskToolRegion* clone() const {return new CQuadMaskToolRegion(*this);};
   // Virtual "assignment operator"
   CMaskToolRegion& assign(const CMaskToolRegion& src);

   virtual EMaskRegionShape GetRegionShape() const {return MASK_REGION_QUAD;};

   virtual bool IsInside(int x, int y);

   POINT* GetQuadPointer();  // Get pointer to four points (Actually the array
                             // is 5 points, with the last point equal to the
                             // first, so the the array can be treated as
                             // a lasso)

   virtual CMaskToolRegion* Interpolate(const CMaskToolRegion &region2,
                                        double intFactor);

private:
   void copyQuadPoints(const POINT *newQuad);
   void setBoundingBox();
   
private:
   POINT quad[5];   // Four points plus an extra that closes the shape
                    // like a lasso
};

//////////////////////////////////////////////////////////////////////

class MTI_IMGTOOLDLL_API CMaskToolRegionList
{
public:
   CMaskToolRegionList();
   virtual ~CMaskToolRegionList();

   int GetMaskToolRegionCount() const;
   CMaskToolRegion* GetMaskToolRegion(int index) const;

   void AddMaskToolRegion(CMaskToolRegion *newMaskToolRegion);

   void ClearRegionList();

private:
   // Private types
   typedef vector<CMaskToolRegion*> RegionList;

private:
   // Private data
   RegionList regionList;
};

///////////////////////////////////////////////////////////////////////////////

#endif // #if !defined(MASK_TOOL_REGIONS_H)




