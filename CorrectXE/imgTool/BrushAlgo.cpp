
#include "BrushAlgo.h"
#include "IniFile.h"
#include "math.h"
#include "MTImalloc.h"
#include "MTIsleep.h"
#include "stdlib.h"
#include "SysInfo.h"

//#define NO_MULTI
#define JIT_THREAD_CREATION

// Since we aren't worried about how big the brushes get anymore,
// we allow them to handle a 10K wide brush (radius 5120) without decimating.
#define LRG_THRESHOLD (10 * 1024 / 2)
#define LRG_SHIFT   1
#define LRG_FACTOR (1 << LRG_SHIFT)
#define MAX_RADIUS (LRG_THRESHOLD << LRG_SHIFT)

#define PI 3.1415927

// static variables to manage internal LUT tables
int CBrush::instanceCount = 0;
MTI_UINT16* CBrush::expTbl = NULL;

CBrush::CBrush (int numStripes)
{
   nStripes = numStripes;

   if (nStripes == 0) {
      nStripes = SysInfo::AvailableProcessorCoreCount();
   }

#ifdef NO_MULTI
   nStripes = 1;
#endif

	tsThread.PrimaryFunction   = (void (*)(void *,int))&computeStripeWeight;
   tsThread.SecondaryFunction = NULL;
   tsThread.CallBackFunction  = NULL;
   tsThread.vpApplicationData = this;
   tsThread.iNThread          = nStripes;
   tsThread.iNJob             = nStripes;

#if !defined( NO_MULTI) && !defined(JIT_THREAD_CREATION)
	if (nStripes > 1) {
      int iRet = MThreadAlloc(&tsThread);
      if (iRet) {
         TRACE_0(errout << "Brush: MThreadAlloc failed, iRet=" << iRet);
      }
   }
#endif

  iMinRow = 0;
  iMinCol = 0;
  iMaxRow = 0;
  iMaxCol = 0;

  ipWeightStorage = NULL;
  ipWeight = NULL;

  // make sure first time OK
  iLastRadius  = -1;
  iLastBlend   = -1;
  iLastOpacity = -1;
  iLastAspect  = -1;
  iLastAngle   = -180;

  setBrushParameters(MAX_RADIUS,
                     50,
                     100,
                     100,
                     100,
                     0,
                     false,
                     BT_SMOOTH);

  // build one copy of the table: 16385 entries
  if (++instanceCount == 1) {
     initGaussianTable();
  }

  Allocate();
  Reallocate();
  ComputeWeight();
}

CBrush::CBrush(const CBrush& srcBr)
{
  nStripes = srcBr.nStripes;

   tsThread.PrimaryFunction   = (void (*)(void *,int))&computeStripeWeight;
   tsThread.SecondaryFunction = NULL;
   tsThread.CallBackFunction  = NULL;
   tsThread.vpApplicationData = this;
   tsThread.iNThread          = nStripes;
   tsThread.iNJob             = nStripes;

#if !defined( NO_MULTI) && !defined(JIT_THREAD_CREATION)
	if (nStripes > 1) {
		int iRet = MThreadAlloc(&tsThread);
		if (iRet) {
			TRACE_0(errout << "Brush: MThreadAlloc failed, iRet=" << iRet);
		}
	}
#endif

  iMinRow = 0;
  iMinCol = 0;
  iMaxRow = 0;
  iMaxCol = 0;

  ipWeightStorage = NULL;
  ipWeight = NULL;

  // make sure first time OK
  iLastRadius  = -1;
  iLastBlend   = -1;
  iLastOpacity = -1;
  iLastAspect  = -1;
  iLastAngle   = -180;

  setBrushParameters(MAX_RADIUS,
                     50,
                     100,
                     100,
                     100,
                     0,
                     false,
                     BT_SMOOTH);

  // build one copy of the table: 16385 entries
  if (++instanceCount == 1) {
     initGaussianTable();
  }

  Allocate();
  Reallocate();
  ComputeWeight();
}

CBrush::~CBrush()
{
   if (--instanceCount == 0) {
      MTIfree(expTbl);
	}

#if !defined( NO_MULTI) && !defined(JIT_THREAD_CREATION)
	if (nStripes > 1) {
		int iRet = MThreadFree(&tsThread);
		if (iRet) {
			TRACE_0(errout << "Brush: MThreadFree failed, iRet=" << iRet);
		}
	}
#endif

  MTIfree(ipWeightStorage);
  ipWeightStorage = 0;

  if (ipWeight != 0)
  {
     // ipWeight pointed to middle of row
     ipWeight += iFullMinRow;
     MTIfree(ipWeight);
     ipWeight = 0;
  }
}

void CBrush::setMaxRadius (int newMaxRadius)
{
   if (newMaxRadius != iLastMaxRadius) {
      float scale = (float)newMaxRadius / iLastMaxRadius;
      int newRadius = (int)(scale * (float)getRadius()) + 0.5;
      iLastMaxRadius = newMaxRadius;
      setRadius(newRadius);
   }
}

void CBrush::setRadius (int newRadius)
{
   if (newRadius <= 0)
      newRadius = 1;
   else if (newRadius > iLastMaxRadius)
      newRadius = iLastMaxRadius;

   if (newRadius != iLastRadius) {

      // save the new one
      iLastRadius = newRadius;

      if (newRadius < 1)
         newRadius = 1;

      iShift = 0;
      fRadius = (float) newRadius;

      // Ideally this "if" should be a "while", but the code that's receiving
      // the brush mask only handles the full size and the half size cases....
      if (fRadius > LRG_THRESHOLD) {
         iShift = LRG_SHIFT;
         fRadius /= LRG_FACTOR;
      }

      reAllocate = true;
      reCompute  = true;
   }
}

void CBrush::setBlend (int newBlend)
{
   if (newBlend < 0)
      newBlend = 0;
   else if (newBlend > 100)
      newBlend = 100;

   if (newBlend != iLastBlend) {

      // save the new one
      iLastBlend = newBlend;

      fBlend = ((float)newBlend) / 100.;

      reCompute = true;
   }
}

void CBrush::setOpacity (int newOpacity)
{
   if (newOpacity < 0)
      newOpacity = 0;
   else if (newOpacity > 100)
      newOpacity = 100;

   if (newOpacity != iLastOpacity) {

      // save the new one
      iLastOpacity = newOpacity;

      fOpacity = ((float)newOpacity) / 100.;

      reCompute = true;
   }
}

void CBrush::setAspect (int newAspect)
{
   if (newAspect < 1)
      newAspect = 1;
   else if (newAspect > 100)
      newAspect = 100;

   if (newAspect != iLastAspect) {

      // save the new one
      iLastAspect = newAspect;

      fAspect = ((float)newAspect) / 100.;

      reAllocate = true;
      reCompute  = true;
   }
}

void CBrush::setAngle (int newAngle)
{
   if (newAngle < -90)
      newAngle = -90;
   else if (newAngle > 90)
      newAngle = 90;

   if (newAngle != iLastAngle) {

      // save the new one
      iLastAngle = newAngle;

      fAngle = (float)newAngle;

      reAllocate = true;
      reCompute  = true;
   }
}

void CBrush::setShape (bool newShape)
{
   if (newShape != bSquare) {

      bSquare = newShape;

      reCompute = true;
   }
}

void CBrush::setType (EBrushType newType)
{
   if (newType != iType) {

      iType = newType;

      reCompute = true;
   }
}

void CBrush::setBrushParameters(int  newMaxRadius,
                                int  newRadius,
                                int  newBlend,
                                int  newOpacity,
                                int  newAspect,
                                int  newAngle,
                                bool newShape,
                                EBrushType newType)
{
   iLastMaxRadius = newMaxRadius;

   if (newRadius <= 0)
      newRadius = 1;
   else if (newRadius > iLastMaxRadius)
      newRadius = iLastMaxRadius;

   if (newRadius != iLastRadius) {

      // save the new one
      iLastRadius = newRadius;

      if (newRadius < 1)
         newRadius = 1;

      iShift = 0;
      fRadius = (float) newRadius;

      // Ideally this "if" should be a "while", but the code that's receiving
      // the brush mask only handles the full size and the half size cases....
      if (fRadius > LRG_THRESHOLD) {
         iShift = LRG_SHIFT;
         fRadius /= LRG_FACTOR;
      }

      reAllocate = true;
      reCompute  = true;
   }

   if (newBlend < 0)
      newBlend = 0;
   else if (newBlend > 100)
      newBlend = 100;

   if (newBlend != iLastBlend) {

      // save the new one
      iLastBlend = newBlend;

      fBlend = ((float)newBlend) / 100.;

      reCompute = true;
   }

   if (newOpacity < 0)
      newOpacity = 0;
   else if (newOpacity > 100)
      newOpacity = 100;

   if (newOpacity != iLastOpacity) {

      // save the new one
      iLastOpacity = newOpacity;

      fOpacity = ((float)newOpacity) / 100.;

      reCompute = true;
   }

   if (newAspect < 1)
      newAspect = 1;
   else if (newAspect > 100)
      newAspect = 100;

   if (newAspect != iLastAspect) {

      // save the new one
      iLastAspect = newAspect;

      fAspect = ((float)newAspect) / 100.;

      reAllocate = true;
      reCompute  = true;
   }

   if (newAngle < -90)
      newAngle = -90;
   else if (newAngle > 90)
      newAngle = 90;

   if (newAngle != iLastAngle) {

      // save the new one
      iLastAngle = newAngle;

      fAngle = (float)newAngle;

      reAllocate = true;
      reCompute  = true;
   }

   if (newShape != bSquare) {

      bSquare = newShape;

      reCompute = true;
   }

   if (newType != iType) {

      iType = newType;

      reCompute = true;
   }
}

void CBrush::setBrushParametersFromBrush(CBrush *srcbrsh)
{
   setBrushParameters(srcbrsh->getMaxRadius(),
                      srcbrsh->getRadius(),
                      srcbrsh->getBlend(),
                      srcbrsh->getOpacity(),
                      srcbrsh->getAspect(),
                      srcbrsh->getAngle(),
                      srcbrsh->getShape(),
                      srcbrsh->getType());
}

int CBrush::getMaxRadius ()
{
   return iLastMaxRadius;
}

int CBrush::getRadius ()
{
  return std::max(1, iLastRadius);
}

int CBrush::getBlend ()
{
  return iLastBlend;
}

int CBrush::getOpacity ()
{
  return iLastOpacity;
}

int CBrush::getAspect ()
{
  return iLastAspect;
}

int CBrush::getAngle ()
{
  return iLastAngle;
}

bool CBrush::getShape()
{
   return bSquare;
}

EBrushType CBrush::getType ()
{
  return iType;
}

int CBrush::getMinRow ()
{
  return iMinRow<<iShift;
}

int CBrush::getMinCol ()
{
  return iMinCol<<iShift;
}

int CBrush::getMaxRow ()
{
  return iMaxRow<<iShift;
}

int CBrush::getMaxCol ()
{
  return iMaxCol<<iShift;
}

MTI_UINT16 CBrush::getWeight (int iRow, int iCol)
{
  if (reAllocate) {
     Reallocate();
     reAllocate = false;
  }

  if (reCompute) {
     ComputeWeight();
     reCompute = false;
  }

  iRow >>= iShift;
  iCol >>= iShift;

  if (iRow < iMinRow || iRow >= iMaxRow)
     return 0;

  if (iCol < iMinCol || iCol >= iMaxCol)
     return 0;

  return ipWeight[iRow][iCol];
}

MTI_UINT16 CBrush::getWeightFast (int iRow, int iCol)
{
   return ipWeight[iRow>>iShift][iCol>>iShift];
}

void CBrush::Allocate()
{
  MTIfree(ipWeightStorage);
  ipWeightStorage = 0;

  if (ipWeight != 0)
  {
     // ipWeight was offset to point to the middle of the row
     ipWeight += iFullMinRow;
     MTIfree(ipWeight);
     ipWeight = 0;
  }

  // note that if we didn't allow rectangular brushes
  // the memory requirements would be HALF -- and the
  // sqrt(2) factor appearing below wouldn't be needed
  //
  iFullMinCol = - (LRG_THRESHOLD*1.415 + 1.0);
  iFullMaxCol = -iFullMinCol + 1;

  iFullMinRow = - (LRG_THRESHOLD*1.415 + 1.0);
  iFullMaxRow = -iFullMinRow + 1;

  int iFullNRow = iFullMaxRow - iFullMinRow;
  int iFullNCol = iFullMaxCol - iFullMinCol;

  ipWeightStorage = (MTI_UINT16 *)MTImalloc(iFullNRow * iFullNCol * sizeof(MTI_UINT16));
  ipWeight = (MTI_UINT16 **)MTImalloc(iFullNRow*sizeof(MTI_UINT16 *));

  for (int iRow = 0; iRow < iFullNRow; ++iRow)
   {
    // each of these ptrs => middle of the row
    ipWeight[iRow] = &ipWeightStorage[(iRow * iFullNCol) - iFullMinCol];
   }

  // => middle of row ptrs
  ipWeight -= iFullMinRow;
}

void CBrush::Reallocate()
{
  fSinAngle = sin (PI * fAngle / 180.);
  fCosAngle = cos (PI * fAngle / 180.);

  float xcorner, ycorner;

  xformCorner( fAspect, -1.0, fabs(fCosAngle), fabs(fSinAngle), &xcorner, &ycorner);

  iMinCol = - (fRadius*xcorner + 1.0);
  iMaxCol = -iMinCol + 1;

  xformCorner( fAspect, 1.0, fabs(fCosAngle), fabs(fSinAngle), &xcorner, &ycorner);

  iMinRow = - (fRadius*ycorner + 1.0);
  iMaxRow = -iMinRow + 1;

  // we want to be able to paint a single pixel
  if (fRadius < 1.0) {
     iMinCol = 0;
     iMaxCol = 1;
     iMinRow = 0;
     iMaxRow = 1;
  }
}

void CBrush::xformCorner(float x, float y, float fcosang, float fsinang, float *xfmx, float *xfmy)
{
   *xfmx = (fcosang*x - fsinang*y);
   *xfmy = (fsinang*x + fcosang*y);
}

void CBrush::ComputeWeight()
{
	// compute the wgts by stripe
#ifdef NO_MULTI
   for (int i=0;i<nStripes;i++)
		tsThread.PrimaryFunction(this,i);
#else

	if (nStripes == 1) {

      // if we're only doing one stripe, call the function directly
      tsThread.PrimaryFunction(this,0);
   }
	else {

		int iRet;
#ifdef JIT_THREAD_CREATION
		iRet = MThreadAlloc(&tsThread);
		if (iRet) {
			TRACE_0(errout << "Brush: MThreadAlloc failed, iRet=" << iRet);
		}
#endif

      // fire up one thread for each stripe
		iRet = MThreadStart(&tsThread);
      if (iRet) {
         TRACE_0(errout << "Brush: MThreadStart failed, iRet=" << iRet);
      }

#ifdef JIT_THREAD_CREATION
		iRet = MThreadFree(&tsThread);
		if (iRet) {
			TRACE_0(errout << "Brush: MThreadFree failed, iRet=" << iRet);
		}
#endif // JIT_THREAD_CREATION
	}

#endif // NO_MULTI
}

void CBrush::initGaussianTable()
{
   expTbl = (MTI_UINT16*)MTImalloc((2 * EXPDIVS + 1)*sizeof(MTI_UINT16));
   for (int i = 0; i <= 2 * EXPDIVS; i++)
   {
      float ifloat = (float)i / (float)EXPDIVS;
      expTbl[i] = UNITY * exp(-.5 * ifloat);
   }
}

void CBrush::computeStripeWeight(void *v, int iJob)
{
  CBrush *vp = (CBrush *) v;

  MTI_UINT16 **ipWeight = vp->ipWeight;

  float fRadius  = vp->fRadius;
  float fBlend   = vp->fBlend;
  float fOpacity = vp->fOpacity;
  float fAspect  = vp->fAspect;
  bool  bSquare  = vp->bSquare;
  EBrushType iType = vp->iType;

  int iMinRow    = vp->iMinRow;
  int iMaxRow    = vp->iMaxRow;
  int iMinCol    = vp->iMinCol;
  int iMaxCol    = vp->iMaxCol;

  float fSinAngle = vp->fSinAngle;
  float fCosAngle = vp->fCosAngle;

  int iNRow = iMaxRow - iMinRow;
  int rowsPerStripe = iNRow / vp->nStripes;
  int begRow = iMinRow + iJob*rowsPerStripe;
  int stripeRows = rowsPerStripe;
  if (iJob == (vp->nStripes-1))
     stripeRows = iNRow - iJob*rowsPerStripe;

/*
   the shape of the curve is described as:

   col_new = fRadius * fAspect * (fCosAngle * col + fSinAngle * row)
   row_new = fRadius * (fSinAngle * col - fCosAngle * row);

   (row, col) range between -1 and 1.

   Fill in the weights for each pixel.  Do this by inverting the
   formulas above:

   col = (fCosAngle/(fRadius*fAspect))*col_new-(fSinAngle/fRadius)*row_new
   row = (-fSinAngle/(fRadius*fAspect))*col_new-(fCosAngle/fRadius)*row_new

   Then, the Gaussian weight = exp (-.5 * (row*row + col*col))
*/

  float fColCol =  fCosAngle / (fRadius * fAspect);
  float fColRow = -fSinAngle / (fRadius * fAspect);

  float fRowCol =  fSinAngle / fRadius;
  float fRowRow =  fCosAngle / fRadius;

  // these are needed to scale the weights
  MTI_UINT16 fMinThresh = UNITY*exp (-.5); // at corners
  MTI_UINT16 fMaxThresh = UNITY;           // at center
  MTI_UINT16 fBlendLocal;

  int ifOpacity = UNITY*fOpacity;

  if (iType == BT_SOLID)
   {
    fBlendLocal = fMinThresh;
   }
  else
   {
    fBlendLocal = (fMaxThresh-fMinThresh) * fBlend + fMinThresh;
   }

   for (int iRow = begRow; iRow < (begRow + stripeRows); iRow++)
   {
      for (int iCol = iMinCol; iCol < iMaxCol; iCol++)
      {
         float fCol = fColCol * (float)iCol + fColRow * (float)iRow;
         float fRow = fRowCol * (float)iCol + fRowRow * (float)iRow;

         float fColSq = fCol * fCol;
         float fRowSq = fRow * fRow;

         if ((fColSq > 1) || (fRowSq > 1))
         {
            ipWeight[iRow][iCol] = 0;
         }
         else
         {
            int ifColSq = fColSq * EXPDIVS;
            int ifRowSq = fRowSq * EXPDIVS;

            int wgt;

            if (bSquare) // vary from ellipse in the center to rectangle at edges
            {
               if (ifRowSq > ifColSq)
               {
                  wgt = (ifRowSq * (int)expTbl[ifRowSq] + (EXPDIVS - ifRowSq) * (int)expTbl[ifRowSq + ifColSq]) / EXPDIVS;
               }
               else
               {
                  wgt = (ifColSq * (int)expTbl[ifColSq] + (EXPDIVS - ifColSq) * (int)expTbl[ifRowSq + ifColSq]) / EXPDIVS;
               }
            }
            else // pure ellipse
            {
               wgt = (int)expTbl[ifRowSq + ifColSq];
            }

            if (wgt < fMinThresh)
            {
               wgt = 0;
            }
            else if (wgt >= fBlendLocal)
            {
               wgt = UNITY;
            }
            else
            {
               wgt = UNITY * (wgt - fMinThresh) / (fBlendLocal - fMinThresh);
            }

            ipWeight[iRow][iCol] = (MTI_UINT16)((wgt * ifOpacity) / UNITY);
         }
      }
   }

   return;
}
