//
// implementation file for class CZoomFractional
//
//
#include "ZoomFractional.h"
#include "ImageFormat3.h"
#include "ImageInfo.h"
#include "MTImalloc.h"
#include <iostream>
#include <stdio.h>
#include <math.h>

// define this for debugging without multithreading
//#define NO_MULTI

// we'll want to optimize this for 3 components (RGB or YUV) and 1 component (LUM)

////////////////////////////////////////////////////////////////////////////////
//
// The arrangement of memory for the successive stages is as follows for HD:
// (KERNEL = 2)
//
//              _______1920_x_3______
//             |RGB etc              |
//             |RGB etc              |
//             |                     |
//        1080 |                     |   SRCBUF (non-interlaced)
//             |                     |
//             |                     |
//             |_____________________|
//
//                        |
//                   ADD MARGINS
//                        |
//                        V
//
//              _______1924_x 3______
//             |RGB etc              |
//             |RGB etc              |
//             |                     |
//        1080 |                     |   FRMBUF1 (non-interlaced)
//             |                     |
//             |                     |
//             |_____________________|
//
//                        |
//       HORZ CONVERT, TRANSPOSE, ADD MARGINS
//                        |
//                        V
//
//                _______1084_______
//               |RRRRRR etc        |
//               |GGGGGG etc        |
//      1920 x 3 |BBBBBB etc        |    FRMBUF2 (non-interlaced)
//               |                  |
//               |__________________|
//
//                        |
//             VERT CONVERT, TRANSPOSE
//                        |
//                        V
//
//              ______1920_x 3_______
//             |RGB etc              |
//             |RGB etc              |
//             |                     |
//        1080 |                     |   DSTBUF (non-interlaced)
//             |                     |
//             |                     |
//             |_____________________|
//
//

////////////////////////////////////////////////////////////////////////////////
//
//    C O N S T R U C T O R
//
CZoomFractional::
CZoomFractional(int numStripes)
: nStripes(numStripes)
{
   frmBuf1 = NULL;

   frmBuf2 = NULL;

   vctr = NULL;

   vTbl = NULL;

   vkern = NULL;

   hctr = NULL;

   hTbl = NULL;

   hkern = NULL;

   // step 1 is UNPACK
   unpackThread.PrimaryFunction   = (void (*)(void *,int))&null;
   unpackThread.SecondaryFunction = NULL;
   unpackThread.CallBackFunction  = NULL;
   unpackThread.vpApplicationData = this;
   unpackThread.iNThread          = nStripes;
   unpackThread.iNJob             = nStripes;

   // step 2 is HORZ DOWNCONVERT
   horzThread.PrimaryFunction   = (void (*)(void *,int))&null;
   horzThread.SecondaryFunction = NULL;
   horzThread.CallBackFunction  = NULL;
   horzThread.vpApplicationData = this;
   horzThread.iNThread          = nStripes;
   horzThread.iNJob             = nStripes;

   // step 3 is VERT DOWNCONVERT
   vertThread.PrimaryFunction   = (void (*)(void *,int))&null;
   vertThread.SecondaryFunction = NULL;
   vertThread.CallBackFunction  = NULL;
   vertThread.vpApplicationData = this;
   vertThread.iNThread          = nStripes;
   vertThread.iNJob             = nStripes;

#ifndef NO_MULTI
   if (nStripes > 1) {
      /* int iRet; */

      /* iRet = */ MThreadAlloc(&unpackThread);
      //if (iRet) {
      //   TRACE_0(errout << "Convert: MThreadAlloc failed, iRet=" << iRet);
      //}
      /* iRet = */ MThreadAlloc(&horzThread);
      //if (iRet) {
      //   TRACE_0(errout << "Convert: MThreadAlloc failed, iRet=" << iRet);
      //}
      /* iRet = */ MThreadAlloc(&vertThread);
      //if (iRet) {
      //   TRACE_0(errout << "Convert: MThreadAlloc failed, iRet=" << iRet);
      //}
   }
#endif

}

////////////////////////////////////////////////////////////////////////////////
//
//    D E S T R U C T O R
//
CZoomFractional::
~CZoomFractional()
{
#ifndef NO_MULTI
   // now deallocate the mthreads
   if (nStripes > 1) {

      MThreadFree(&unpackThread);

      MThreadFree(&horzThread);

      MThreadFree(&vertThread);
   }
#endif

   delete [] vctr;

   delete [] vkern;

   delete [] hctr;

   delete [] hkern;

   MTIfree(frmBuf2);
   //delete [] frmBuf2;

   MTIfree(frmBuf1);
   //delete [] frmBuf1;
}


int CZoomFractional::initialize(const CImageFormat *srcFmt,
                                int horkern, int vrtkern,
                                unsigned char matred,
                                unsigned char matgrn,
                                unsigned char matblu)
{
   if ((horkern < 1)||(horkern > 2)||(vrtkern < 1)||(vrtkern > 2))
      return(ZOOMFRACTIONAL_ERROR_INVALID_KERNEL_SIZE);

   horzkern = horkern;
   vertkern = vrtkern;

   // for rendering the MAT
   matRed = matred;
   matGrn = matgrn;
   matBlu = matblu;
   matLum = (matred + matgrn + matblu) / 3;

   // extract the following from the src image-format
   frmwdth          = srcFmt->getPixelsPerLine();
   frmhght          = srcFmt->getLinesPerFrame();

   frmpitch         = frmwdth;
   frmpelpacking    = srcFmt->getPixelPacking();
   frmpelcomponents = srcFmt->getPixelComponents();

   // when color space values are finally included
   // in image formats, this calculation will work

   int bitwdth = 0;
   maxcomponent = 0;

    /////////////////////////////////////////////////////////////////////////
   // mbraca: the colorspace values look fine to me, so I will try to use
   // them, but fall back to the stupid method if they don't make sense.
   // NOTE that the zooming code only wants to use one max, so use only the
   // one for component 0!
   ////////////////////////////////////////////////////////////////////////

   const CImageColorSpace *clrSpc = srcFmt->getColorSpaceValues();
   maxcomponent = (int)clrSpc->componentMax[0];

   // till then do this:
   //
   switch(frmpelpacking) {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:
      case IF_PIXEL_PACKING_1_8Bits_IN_2Bytes:

         // 200 is arbitrary
         if (maxcomponent < 200 || maxcomponent > 255)
         {
            maxcomponent = 255;
         }

         // Always believe data is 8 bit.
         bitwdth = 8;

         break;

      case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:
      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R:
      case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L:
      case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R:
      case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes:
      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:
      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVSa:

         // 200 is arbitrary minimum for max component.
         if (maxcomponent < 200 || maxcomponent > 1023)
         {
            maxcomponent = 1023;
         }

         // Always believe data is 10 bit.
         bitwdth = 10;

         break;

      case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes:
      case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes_LR:
      case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_LE:
      case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_BE:
      case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes:

         // 200 is arbitrary minimum for max component.
         if (maxcomponent < 200 || maxcomponent > 4095)
         {
            maxcomponent = 4095;
         }

         // Always believe data is 12 bit.
         bitwdth = 12;
         break;

      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:

         // 200 is arbitrary minimum for max component.
         if (maxcomponent < 200 || maxcomponent > 65535)
         {
            maxcomponent = 65535;
         }

         // Always believe data is 16 bit.
         bitwdth = 16;

         break;

      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_INT:
      {
         // 16-bit internal format may hold data with fewer real bits.
         // Let's count the bits indicated by the max component.
         int cmpmax = maxcomponent;
         while (cmpmax > 0)
         {
            cmpmax >>= 1;
            bitwdth++;
         }

         // Bit width is always even.
         if ((bitwdth % 2) != 0)
         {
            ++bitwdth;
         }

         // Protect against bogus max component.
         if (bitwdth < 8 || bitwdth > 16)
         {
            bitwdth = 16;
            maxcomponent = (1 << bitwdth) - 1;
         }
      }
      break;

      case IF_PIXEL_PACKING_1_Half_IN_2Bytes:

         // 0x8000 (0,0) is arbitrary minimum for max component.
         if (maxcomponent < 0x8000)
         {
            maxcomponent = 0x8000;
         }

         // Always believe data is 16 bit.
         bitwdth = 16;
         break;

      default:
         // mbraca thinks this should be an error!
         bitwdth = 10;
   }

   wgtone = (1 << (30 - bitwdth));
   wgtshft = (30 - bitwdth);
   ////maxcomponent = (1 << bitwdth) - 1;

   // based on bitwdth, promote the mat color
   int colorPromote = (bitwdth - 8);
   matRed <<= colorPromote;
   matGrn <<= colorPromote;
   matBlu <<= colorPromote;
   matLum <<= colorPromote;


   pitch1  = frmwdth + 2*horzkern;
   hght2   = frmwdth;

//   if (frmpelcomponents == IF_PIXEL_COMPONENTS_LUMINANCE) { // 1 component
//
//      unpackThread.PrimaryFunction = (void (*)(void *,int))&unpack1;
//      horzThread.PrimaryFunction   = (void (*)(void *,int))&horizontalDownconvert1;
//      vertThread.PrimaryFunction   = (void (*)(void *,int))&verticalDownconvert1;
//   }
//   else {                                                   // 3 components

      frmpitch *= 3;
      pitch1   *= 3;
      hght2    *= 3;

      unpackThread.PrimaryFunction = (void (*)(void *,int))&unpack3;
      horzThread.PrimaryFunction   = (void (*)(void *,int))&horizontalDownconvert3;
      vertThread.PrimaryFunction   = (void (*)(void *,int))&verticalDownconvert3;
//   }

   // allocate a frame buffer for the unpacked native frame
   // with room for margins at both ends of the rows

   MTIfree(frmBuf1);
   frmBuf1 = (MTI_UINT16 *)MTImalloc(pitch1*frmhght*sizeof(MTI_UINT16));
   //delete[] frmBuf1;
   //frmBuf1 = new MTI_UINT16[pitch1*frmhght];
   if (frmBuf1 == 0)
      return(ZOOMFRACTIONAL_ERROR_INSUFFICIENT_MEMORY);

   // allocate weights for each pel of frmwdth
   hkern = new KCoeff[frmwdth];
   hctr  = new int[frmwdth];

   // allocate a frame buffer to receive the horizontal conversion
   // this will be transposed, so that the data is RGB columnwise
   pitch2 = frmhght + 2*vertkern;
   MTIfree(frmBuf2);
   frmBuf2 = (MTI_INT16 *)MTImalloc(pitch2*hght2*sizeof(MTI_INT16));
   //delete[] frmBuf2;
   //frmBuf2 = new MTI_INT16[pitch2*hght2];
   if (frmBuf2 == 0)
      return(ZOOMFRACTIONAL_ERROR_INSUFFICIENT_MEMORY);

   vkern = new KCoeff[frmhght];
   vctr  = new int[frmhght];

   return 0;
}

// Note: the dstRect to be supplied here is of the INclusive type; e.g., the
// full HD frame would be (0, 0, 1919, 1079). However, the floating point
// srcRect is on R2, so the full frame would be (0.0, 0.0, 1920.0, 1080.0)
//
int CZoomFractional::convert(DRECT *srcRect, RECT *dstRect, MTI_UINT16 *srcBuf, MTI_UINT16 *dstBuf)
{
   srcbuf = srcBuf;
   dstbuf = dstBuf;

   // check the source rectangle
   if (!((0 <= srcRect->left)&&
        (srcRect->left < srcRect->right)&&
        (srcRect->right <= (double)frmwdth)&&
        (0 <= srcRect->top)&&
        (srcRect->top < srcRect->bottom)&&
        (srcRect->bottom <= (double)frmhght)))
      return(ZOOMFRACTIONAL_ERROR_INVALID_SRC_RECTANGLE);

   // source rectangle is OK
   srcrect          = *srcRect;
   srcwdth          = srcRect->right - srcRect->left;
   srchght          = srcRect->bottom - srcRect->top;

   // check the destination rectangle
   if (!((0 <= dstRect->left)&&
        (dstRect->left < dstRect->right)&&
        (dstRect->right < frmwdth)&&
        (0 <= dstRect->top)&&
        (dstRect->top < dstRect->bottom)&&
        (dstRect->bottom < frmhght)))
      return(ZOOMFRACTIONAL_ERROR_INVALID_DST_RECTANGLE);

   // destination rectangle is OK
   dstrect          = *dstRect;
   dstwdth          = (double)(dstRect->right - dstRect->left + 1);
   dsthght          = (double)(dstRect->bottom - dstRect->top + 1);
   dstoff           = (int)(dstrect.top * frmwdth + dstrect.left);

   /////////////////////////////////////////////////////////////////////////////
   //
   int hmod = SUBDIVS;
   int hsubdivs = SUBDIVS;

   // size of each subdivision
   double hsubsize = 1.0 / (double)hsubdivs;

   delete[] hkern;
   hkern = new KCoeff[hmod+1];
   double hcoeff[MAX_WEIGHTS];

   for (int i=0;i<=hmod;i++) {

      double krnlCtr = (double)i / (double)hmod;

      for (int j=-horzkern; j<=horzkern; j++) {

         // numerical integration of the displaced filter over the pixel
         //
         double wgt = 0.0;
         for (int k=0; k < hsubdivs; k += 1) {

            double karg = ((double)k + 0.5)*hsubsize;

            wgt += Kernel((double)j + karg - krnlCtr, srcwdth, dstwdth, horzkern);
         }

         hcoeff[MIDPT+j] = wgt;
      }

      // normalize
      double negsum = 0.0;
      double possum = 0.0;
      for (int j=-horzkern;j<=horzkern;j++) {
         if (hcoeff[MIDPT+j] < 0.0)
            negsum -= hcoeff[MIDPT+j];
         else
            possum += hcoeff[MIDPT+j];
      }
      double sum = possum - negsum;
      for (int j=-horzkern;j<=horzkern;j++) {
         hcoeff[MIDPT+j] /= sum;
      }

      // convert to signed integer
      for (int j=-horzkern;j<=horzkern;j++) {
         hkern[i].wgt[MIDPT+j] = (int)(wgtone* hcoeff[MIDPT+j]);
      }

      // sharpen up the normalization
      int tstsum = 0;
      for (int j=-horzkern;j<=horzkern;j++) {
         tstsum += hkern[i].wgt[MIDPT+j];
      }

      int k = 0;
      while ((k < 5) && ((tstsum < wgtone-2)||(tstsum > wgtone+2))) {

         double adj = (double)wgtone/(double)tstsum;

         for (int j=-horzkern;j<=horzkern;j++) {
            hkern[i].wgt[MIDPT+j] *= adj;
         }

         tstsum = 0;
         for (int j=-horzkern;j<=horzkern;j++) {
            tstsum += hkern[i].wgt[MIDPT+j];
         }

         k++;
      }

      // put any residue in the middle wgt
      hkern[i].wgt[MIDPT] += wgtone - tstsum;
   }

   /////////////////////////////////////////////////////////////////////////////
   //
   int vmod = SUBDIVS;
   int vsubdivs = SUBDIVS;

   // now divide each of the vmod subpixels enough so
   // that the total subdivisions per pel is >= 10
   //
   double vsubsize = 1.0 / (double)vsubdivs;

   delete[] vkern;
   vkern = new KCoeff[vmod+1];
   double vcoeff[MAX_WEIGHTS];

   for (int i=0;i<=vmod;i++) {

      double krnlCtr = (double)i / (double)vmod;

      for (int j=-vertkern; j<=vertkern; j++) {

         // numerical integration of the displaced filter over the pixel
         //
         double wgt = 0.0;
         for (int k=0; k < vsubdivs; k += 1) {

            double karg = ((double)k + 0.5)*vsubsize;

            wgt += Kernel((double)j + karg - krnlCtr, srchght, dsthght, vertkern);
         }

         vcoeff[MIDPT+j] = wgt;
      }

      // normalize
      double negsum = 0.0;
      double possum = 0.0;
      for (int j=-vertkern;j<=vertkern;j++) {
         if (vcoeff[MIDPT+j] < 0.0)
            negsum -= vcoeff[MIDPT+j];
         else
            possum += vcoeff[MIDPT+j];
      }
      double sum = possum - negsum;
      for (int j=-vertkern;j<=vertkern;j++) {
         vcoeff[MIDPT+j] /= sum;
      }

      // convert to signed integer
      for (int j=-vertkern;j<=vertkern;j++) {
         vkern[i].wgt[MIDPT+j] = (int)(wgtone * vcoeff[MIDPT+j]);
      }


      // sharpen up the normalization
      int tstsum = 0;
      for (int j=-vertkern;j<=vertkern;j++) {
         tstsum += vkern[i].wgt[MIDPT+j];
      }

      int k = 0;
      while ((k<5) && ((tstsum < wgtone-2)||(tstsum > wgtone+2))) {

         double adj = (double)wgtone/(double)tstsum;

         for (int j=-vertkern;j<=vertkern;j++) {
            vkern[i].wgt[MIDPT+j] *= adj;
         }

         tstsum = 0;
         for (int j=-vertkern;j<=vertkern;j++) {
            tstsum += vkern[i].wgt[MIDPT+j];
         }

         k++;
      }

      // put any residue in the middle wgt
      vkern[i].wgt[MIDPT] += wgtone - tstsum;
   }

   delete[] hTbl;
   hTbl = new KCoeff *[dstwdth];
   delete[] hctr;
   hctr = new int[dstwdth];

   double hcvtfactor = srcwdth/(double)dstwdth;

   for (int i=0; i<dstwdth; i++) {

      double cntr0 = (i + 0.5)*hcvtfactor + srcRect->left;
      double cntr1 = (int)cntr0;
      double arg = cntr0 - cntr1;

      int iarg = (arg * (double)hmod) + 0.5;

      // -> correct KCoeff
      hTbl[i] = hkern + iarg;

      // gives correct central pixel
      hctr[i] = cntr1;
   }

   delete[] vTbl;
   vTbl = new KCoeff *[dsthght];
   delete[] vctr;
   vctr = new int[dsthght];

   double vcvtfactor = srchght/(double)dsthght;

   for (int i=0; i<dsthght; i++) {

      double cntr0 = (i + 0.5)*vcvtfactor + srcRect->top;
      double cntr1 = (int)cntr0;
      double arg = cntr0 - cntr1;

      int iarg = (arg * (double)vmod) + 0.5;

      // -> correct KCoeff
      vTbl[i] = vkern + iarg;

      // gives correct central pixel
      vctr[i] = cntr1;
   }

   int iRet = 0;
#ifdef NO_MULTI
   for (int i=0;i<nStripes;i++) {
      unpackThread.PrimaryFunction(this,i);
   }
   for (int i=0;i<nStripes;i++) {
      horzThread.PrimaryFunction(this,i);
   }
   for (int i=0;i<nStripes;i++) {
      vertThread.PrimaryFunction(this,i);
   }
#else
   if (nStripes == 1) {
      unpackThread.PrimaryFunction(this,0);
      horzThread.PrimaryFunction(this,0);
      vertThread.PrimaryFunction(this,0);
   }
   else {
      // fire up one thread for each stripe
      iRet = MThreadStart(&unpackThread);
      if (iRet)
         return(iRet);
      //if (iRet) {
      //   TRACE_0(errout << "Convert: MThreadStart failed, iRet=" << iRet);
      //}
      iRet = MThreadStart(&horzThread);
      if (iRet)
         return(iRet);
      //if (iRet) {
      //   TRACE_0(errout << "Convert: MThreadStart failed, iRet=" << iRet);
      //}
      iRet = MThreadStart(&vertThread);
      //if (iRet) {
      //   TRACE_0(errout << "Convert: MThreadStart failed, iRet=" << iRet);
      //}
   }
#endif

   return iRet;
}

void CZoomFractional::drawRectangle1(int left, int top, int right, int bottom)
{
   int rctwd = right - left + 1;
   int rctht = bottom - top + 1;

   unsigned short *rowbeg = dstbuf +  (top * frmwdth + left);

   for (int i=0; i < rctht; i++) {

      unsigned short *dst = rowbeg;

      for (int j=0; j < rctwd; j++) {

         dst[0] = matLum;

         dst += 1;
      }

      rowbeg += frmpitch;
   }
}

void CZoomFractional::drawRectangle3(int left, int top, int right, int bottom)
{
   int rctwd = right - left + 1;
   int rctht = bottom - top + 1;

   unsigned short *rowbeg = dstbuf +  (top * frmwdth + left)*3;

   for (int i=0; i < rctht; i++) {

      unsigned short *dst = rowbeg;

      for (int j=0; j < rctwd; j++) {

         dst[0] = matRed;
         dst[1] = matGrn;
         dst[2] = matBlu;

         dst += 3;
      }

      rowbeg += frmpitch;
   }
}

void CZoomFractional::drawMat1()
{
   drawRectangle1( 0, 0, frmwdth - 1, dstrect.top - 1);
   drawRectangle1( 0, dstrect.top, dstrect.left - 1, dstrect.bottom);
   drawRectangle1(dstrect.right + 1, dstrect.top, frmwdth - 1, dstrect.bottom);
   drawRectangle1( 0, dstrect.bottom + 1, frmwdth - 1, frmhght - 1);
}

void CZoomFractional::drawMat3()
{
   drawRectangle3( 0, 0, frmwdth - 1, dstrect.top - 1);
   drawRectangle3( 0, dstrect.top, dstrect.left - 1, dstrect.bottom);
   drawRectangle3(dstrect.right + 1, dstrect.top, frmwdth - 1, dstrect.bottom);
   drawRectangle3( 0, dstrect.bottom + 1, frmwdth - 1, frmhght - 1);
}

////////////////////////////////////////////////////////////////////////////////

void CZoomFractional::null(void *v, int iJob)
{
}

////////////////////////////////////////////////////////////////////////////////
// These format-specific segments are called from the first thread. They may be
// compiled to call the horizontal downconvert directly when they are done.
//
// Note that the unpack is limited to the source rectangle, which may not be the
// entire HD frame. This may save a (very) little effort
//


void CZoomFractional::unpack1(void *v, int iJob)
{
   CZoomFractional *vp = (CZoomFractional *)v;

   int kernel = vp->horzkern;

   int frmpitch = vp->frmpitch;
   MTI_UINT16 *srcbuf = vp->srcbuf;
   int pitch1 = vp->pitch1;
   MTI_UINT16 *frmBuf1 = vp->frmBuf1;

   int rowsPerStripe = vp->frmhght / vp->nStripes;
   int begRow = iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = vp->frmhght;

   MTI_UINT16 *src = srcbuf  + begRow*frmpitch;
   MTI_UINT16 *dst = frmBuf1 + begRow*pitch1;
   for (int i=begRow; i<endRow;i++) {

      for (int j=0; j<kernel; j++) {
         dst[0] = src[0];
         dst++;
      }

      memcpy((MTI_UINT8 *)dst, (MTI_UINT8* *)src, frmpitch<<1);

      src += frmpitch;
      dst += frmpitch;

      for (int j=0; j<kernel; j++) {

         dst[0] = src[-1];
         dst++;
      }
   }
}

void CZoomFractional::unpack3(void *v, int iJob)
{
   CZoomFractional *vp = (CZoomFractional *)v;

   int kernel = vp->horzkern;

   int frmpitch = vp->frmpitch;
   MTI_UINT16 *srcbuf = vp->srcbuf;
   int pitch1 = vp->pitch1;
   MTI_UINT16 *frmBuf1 = vp->frmBuf1;

   /////////////////////////////////////////////////////////////////////////////
   //
   // HORZ Unpack
   //

   int rowsPerStripe = vp->frmhght / vp->nStripes;
   int begRow = iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = vp->frmhght;

   MTI_UINT16 *src = srcbuf  + begRow*frmpitch;
   MTI_UINT16 *dst = frmBuf1 + begRow*pitch1;
   for (int i=begRow; i<endRow;i++) {

      for (int j=0; j<kernel; j++) {
         dst[0] = src[0];
         dst[1] = src[1];
         dst[2] = src[2];
         dst += 3;
      }

      memcpy((MTI_UINT8 *)dst, (MTI_UINT8* *)src, frmpitch<<1);

      src += frmpitch;
      dst += frmpitch;

      for (int j=0; j<kernel; j++) {
         dst[0] = src[-3];
         dst[1] = src[-2];
         dst[2] = src[-1];
         dst += 3;
      }
   }
}

void CZoomFractional::horizontalDownconvert1(void *v, int iJob)
{
   CZoomFractional *vp = (CZoomFractional *)v;

   int kernel = vp->horzkern;

   int wgtshft = vp->wgtshft;
   int dstwdth = vp->dstwdth;
   int frmhght = vp->frmhght;

   int pitch1 = vp->pitch1;
   MTI_UINT16 *frmBuf1 = vp->frmBuf1;
   int pitch2 = vp->pitch2;
   MTI_INT16 *frmBuf2 = vp->frmBuf2;

   /////////////////////////////////////////////////////////////////////////////
   //
   // HORZ Downconvert
   //
   // This generates a destination-resolution COLUMN from each row, setting the
   // stage for vertical downconvert. Row margins are added afterwards.

   MTI_UINT16 *unprow0, *unprow1, *unprow2, *unprow3, *unprow4, *unprow5;

   MTI_INT16 *dst0;

   KCoeff **hTbl = vp->hTbl;

   int *hctr = vp->hctr;

   int yval0, yval1;
   int yval2, yval3;
   int yval4, yval5;

   int t0;

   // we've unpacked the source, so adjust the begRow & endRow
   int rowsPerStripe = frmhght / vp->nStripes;
   int begRow = iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = frmhght;

   // downconvert the stripe 6 rows at a time
   int irow;
   for (irow=begRow;irow<endRow-6;irow+=6) {

      unprow0 = frmBuf1 + irow*pitch1 + kernel;
      unprow1 = unprow0 + pitch1;
      unprow2 = unprow1 + pitch1;
      unprow3 = unprow2 + pitch1;
      unprow4 = unprow3 + pitch1;
      unprow5 = unprow4 + pitch1;

      // dst0 points to the leftmost of 6 COLUMNS
      dst0 = frmBuf2 + kernel + irow;

      // generate the output pixel-pairs
      for (int j=0;j<dstwdth;j+=1) {

         KCoeff *iw = hTbl[j];
         int ctr = hctr[j];

         yval0 = 0;
         yval1 = 0;
         yval2 = 0;
         yval3 = 0;
         yval4 = 0;
         yval5 = 0;

         for (int k=-kernel;k<=kernel;k++) {

            int wgt = iw->wgt[MIDPT+k];

            int ir = (ctr+k);

            yval0 += unprow0[ir  ]*wgt;
            yval1 += unprow1[ir  ]*wgt;
            yval2 += unprow2[ir  ]*wgt;
            yval3 += unprow3[ir  ]*wgt;
            yval4 += unprow4[ir  ]*wgt;
            yval5 += unprow5[ir  ]*wgt;
         }

         dst0[0] = yval0>>wgtshft;
         dst0[1] = yval1>>wgtshft;
         dst0[2] = yval2>>wgtshft;
         dst0[3] = yval3>>wgtshft;
         dst0[4] = yval4>>wgtshft;
         dst0[5] = yval5>>wgtshft;
         dst0 += pitch2;
      }
   }

   // downconvert the residual lines
   irow -=5;
   for (;irow<endRow;irow+=1) {

      unprow0 = frmBuf1 + irow*pitch1 + kernel;

      dst0 = frmBuf2 + kernel + irow;

      // generate the output pixel-pairs
      for (int j=0;j<dstwdth;j+=1) {

         KCoeff *iw = hTbl[j];
         int ctr = hctr[j];

         yval0 = 0;

         for (int k=-kernel;k<=kernel;k++) {

            int wgt = iw->wgt[MIDPT+k];

            int ir = (ctr+k);

            // note that making separate variables
            // int ig and ib causes the execution
            // time to inflate. Leave to compiler!

            yval0 += unprow0[ir  ]*wgt;
         }

         // store the results COLUMNWISE
         dst0[0] = yval0>>wgtshft;
         dst0 += pitch2;
      }
   }

   // in frmBuf2, the original (horz-converted) rows form columns.
}

void CZoomFractional::horizontalDownconvert3(void *v, int iJob)
{
   CZoomFractional *vp = (CZoomFractional *)v;

   int kernel = vp->horzkern;

   int wgtshft = vp->wgtshft;
   int dstwdth = vp->dstwdth;
   int frmhght = vp->frmhght;
   int pitch1 = vp->pitch1;
   MTI_UINT16 *frmBuf1 = vp->frmBuf1;
   int pitch2 = vp->pitch2;
   MTI_INT16 *frmBuf2 = vp->frmBuf2;

   /////////////////////////////////////////////////////////////////////////////
   //
   // HORZ Downconvert
   //
   // This generates a destination-resolution COLUMN from each row, setting the
   // stage for vertical downconvert. Row margins are added afterwards.

   MTI_UINT16 *unprow0, *unprow1, *unprow2, *unprow3, *unprow4, *unprow5;

   MTI_INT16 *colDst;

   KCoeff **hTbl = vp->hTbl;

   int *hctr = vp->hctr;

   int rval0, gval0, bval0, rval1, gval1, bval1;
   int rval2, gval2, bval2, rval3, gval3, bval3;
   int rval4, gval4, bval4, rval5, gval5, bval5;

   // we've unpacked the source, so adjust the begRow & endRow
   int rowsPerStripe = frmhght / vp->nStripes;
   int begRow = iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = frmhght;

   // downconvert the stripe 6 rows at a time
   int irow;
   for (irow=begRow;irow<endRow-6;irow+=6) {

      unprow0 = frmBuf1 + irow*pitch1 + kernel*3;
      unprow1 = unprow0 + pitch1;
      unprow2 = unprow1 + pitch1;
      unprow3 = unprow2 + pitch1;
      unprow4 = unprow3 + pitch1;
      unprow5 = unprow4 + pitch1;

      // dst0 points to the leftmost of 6 COLUMNS
      colDst = frmBuf2 + kernel + irow;

      // generate the output pixel-pairs
      for (int j=0;j<dstwdth;j+=1) {

         KCoeff *iw = hTbl[j];
         int ctr = hctr[j];

         rval0 = 0;
         gval0 = 0;
         bval0 = 0;

         rval1 = 0;
         gval1 = 0;
         bval1 = 0;

         rval2 = 0;
         gval2 = 0;
         bval2 = 0;

         rval3 = 0;
         gval3 = 0;
         bval3 = 0;

         rval4 = 0;
         gval4 = 0;
         bval4 = 0;

         rval5 = 0;
         gval5 = 0;
         bval5 = 0;

         for (int k=-kernel;k<=kernel;k++) {

            int wgtrgb = iw->wgt[MIDPT+k];

            int ir = (ctr+k)*3;

            rval0 += unprow0[ir ]*wgtrgb;
            rval1 += unprow1[ir ]*wgtrgb;
            rval2 += unprow2[ir ]*wgtrgb;
            rval3 += unprow3[ir ]*wgtrgb;
            rval4 += unprow4[ir ]*wgtrgb;
            rval5 += unprow5[ir ]*wgtrgb;

            ir++;
            gval0 += unprow0[ir ]*wgtrgb;
            gval1 += unprow1[ir ]*wgtrgb;
            gval2 += unprow2[ir ]*wgtrgb;
            gval3 += unprow3[ir ]*wgtrgb;
            gval4 += unprow4[ir ]*wgtrgb;
            gval5 += unprow5[ir ]*wgtrgb;

            ir++;
            bval0 += unprow0[ir ]*wgtrgb;
            bval1 += unprow1[ir ]*wgtrgb;
            bval2 += unprow2[ir ]*wgtrgb;
            bval3 += unprow3[ir ]*wgtrgb;
            bval4 += unprow4[ir ]*wgtrgb;
            bval5 += unprow5[ir ]*wgtrgb ;
         }

         // store the results 6 cols at a time
         colDst[0] = rval0>>wgtshft;
         colDst[1] = rval1>>wgtshft;
         colDst[2] = rval2>>wgtshft;
         colDst[3] = rval3>>wgtshft;
         colDst[4] = rval4>>wgtshft;
         colDst[5] = rval5>>wgtshft;
         colDst += pitch2;

         colDst[0] = gval0>>wgtshft;
         colDst[1] = gval1>>wgtshft;
         colDst[2] = gval2>>wgtshft;
         colDst[3] = gval3>>wgtshft;
         colDst[4] = gval4>>wgtshft;
         colDst[5] = gval5>>wgtshft;
         colDst += pitch2;

         colDst[0] = bval0>>wgtshft;
         colDst[1] = bval1>>wgtshft;
         colDst[2] = bval2>>wgtshft;
         colDst[3] = bval3>>wgtshft;
         colDst[4] = bval4>>wgtshft;
         colDst[5] = bval5>>wgtshft;
         colDst += pitch2;
      }
   }

   // downconvert the residual lines
   irow -=5;
   for (;irow<endRow;irow+=1) {

      unprow0 = frmBuf1 + irow*pitch1 + kernel*3;

      colDst = frmBuf2 + kernel + irow;

      // generate the output pixel-pairs
      for (int j=0;j<dstwdth;j+=1) {

         KCoeff *iw = hTbl[j];
         int ctr = hctr[j];

         rval0 = 0;
         gval0 = 0;
         bval0 = 0;

         for (int k=-kernel;k<=kernel;k++) {

				int wgtrgb = iw->wgt[MIDPT+k];

            int ir = (ctr+k)*3;

            rval0 += unprow0[ir    ]*wgtrgb;
            gval0 += unprow0[ir + 1]*wgtrgb;
            bval0 += unprow0[ir + 2]*wgtrgb;
         }

         // store the results 1 col at a time
         colDst[0] = rval0>>wgtshft;
         colDst += pitch2;
         colDst[0] = gval0>>wgtshft;
         colDst += pitch2;
         colDst[0] = bval0>>wgtshft;
         colDst += pitch2;
      }
   }

   // in frmBuf2 we now have, in order, an R-row, a G-row, a B-row,
   // etc.., so that the original (horz-converted) rows form columns.
}

void CZoomFractional::verticalDownconvert1(void *v, int iJob)
{
   CZoomFractional *vp = (CZoomFractional *)v;

   int kernel = vp->vertkern;

   int wgtshft = vp->wgtshft;
   int maxcomponent = vp->maxcomponent;
   int pitch2 = vp->pitch2;
   MTI_INT16 *frmBuf2 = vp->frmBuf2;
   int frmpitch = vp->frmpitch;
   int dsthght = vp->dsthght;
   MTI_UINT16 *dstptr = vp->dstbuf + vp->dstoff;

   /////////////////////////////////////////////////////////////////////////////
   //
   // VERT Downconvert
   //
   // This generates a reduced-resolution column from each row, completing
   // the conversion.

   int rowsPerStripe = (vp->dstwdth) / vp->nStripes;
   int begRow = iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = (vp->dstwdth);

   MTI_INT16 *yrow0, *yrow1, *yrow2, *yrow3;

   MTI_UINT16 *dst;

   KCoeff **vTbl = vp->vTbl;

   int *vctr = vp->vctr;

   int yval0, yval1, yval2, yval3;

   int t0;

   // make sure the rows are terminated properly
   MTI_INT16 *rowbeg = frmBuf2 + begRow*pitch2;
   MTI_INT16 *rowend = rowbeg + pitch2 - kernel;
   for (int i=begRow;i<endRow;i++) {
      for (int j=0;j<kernel;j++) {
           rowbeg[j] = rowbeg[kernel];
           rowend[j] = rowend[-1];
      }
      rowbeg += pitch2;
      rowend += pitch2;
   }

   // each time through loop generates a column 4 pixels wide
   int irow;
   for (irow=begRow;irow<endRow-4;irow+=4) {

      yrow0 = frmBuf2 + irow*pitch2 + kernel;
      yrow1 = yrow0 + pitch2;
      yrow2 = yrow1 + pitch2;
      yrow3 = yrow2 + pitch2;

      dst = dstptr + irow;

      // generate the output
      for (int j=0;j<dsthght;j+=1) {

         KCoeff *iw = vTbl[j];
         int ctr = vctr[j];

         yval0 = 0;
         yval1 = 0;
         yval2 = 0;
         yval3 = 0;

         for (int k=-kernel;k<=kernel;k++) {

            int wgt = iw->wgt[MIDPT+k];

            int ivrt  = (ctr+k);

            yval0 += yrow0[ivrt]*wgt;
            yval1 += yrow1[ivrt]*wgt;
            yval2 += yrow2[ivrt]*wgt;
            yval3 += yrow3[ivrt]*wgt;
         }

         t0 = yval0>>wgtshft;
         if (t0 < 0) t0 = 0;
         if (t0 > maxcomponent) t0 = maxcomponent;
         dst[0] = t0;

         t0 = yval1>>wgtshft;
         if (t0 < 0) t0 = 0;
         if (t0 > maxcomponent) t0 = maxcomponent;
         dst[1] = t0;

         t0 = yval2>>wgtshft;
         if (t0 < 0) t0 = 0;
         if (t0 > maxcomponent) t0 = maxcomponent;
         dst[2] = t0;

         t0 = yval3>>wgtshft;
         if (t0 < 0) t0 = 0;
         if (t0 > maxcomponent) t0 = maxcomponent;
         dst[3] = t0;

         dst += frmpitch;
      }
   }

   // downconvert the residual lines
   irow -= 3;
   for (;irow<endRow;irow+=1) {

      yrow0 = frmBuf2 + irow*pitch2 + kernel;

      dst = dstptr + irow;

      // generate the output
      for (int j=0;j<dsthght;j+=1) {

         KCoeff *iw = vTbl[j];

         int ctr = vctr[j];

         yval0 = 0;

         for (int k=-kernel;k<=kernel;k++) {

            int wgt = iw->wgt[MIDPT+k];

            int ivrt  = (ctr+k);

            yval0 += yrow0[ivrt]*wgt;
         }

         t0 = yval0>>wgtshft;
         if (t0 < 0) t0 = 0;
         if (t0 > maxcomponent) t0 = maxcomponent;
         dst[0] = t0;

         dst += frmpitch;
      }
   }

#if 0
   // after last stripe, render MAT
   if (iJob == (vp->nStripes-1)) {
      vp->drawMat1();
   }
#endif

}

void CZoomFractional::verticalDownconvert3(void *v, int iJob)
{
   CZoomFractional *vp = (CZoomFractional *)v;

   int kernel = vp->vertkern;

   int wgtshft = vp->wgtshft;
   int maxcomponent = vp->maxcomponent;
   int pitch2 = vp->pitch2;
   MTI_INT16 *frmBuf2 = vp->frmBuf2;
   int frmpitch = vp->frmpitch;
   int dsthght = vp->dsthght;
   MTI_UINT16 *dstptr = vp->dstbuf + 3*vp->dstoff;

   /////////////////////////////////////////////////////////////////////////////
   //
   // VERT Downconvert
   //
   // This generates a reduced-resolution column from each row, completing
   // the conversion.

   int rowsPerStripe = (vp->dstwdth) / vp->nStripes;
   int begRow = iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = (vp->dstwdth);

   // 8 rgb rows => 4 RGB triples of output
   MTI_INT16      *rrow0, *grow0, *brow0,
                  *rrow1, *grow1, *brow1,
                  *rrow2, *grow2, *brow2,
                  *rrow3, *grow3, *brow3;

   MTI_UINT16 *dst;

   MTI_UINT16 *dstrgb;

   KCoeff **vTbl = vp->vTbl;

   int *vctr = vp->vctr;

   int rval0, gval0, bval0,
       rval1, gval1, bval1,
       rval2, gval2, bval2,
       rval3, gval3, bval3;

   int t0;

   // make sure the rows are terminated properly
   MTI_INT16 *rowbeg = frmBuf2 + begRow*3*pitch2;
   MTI_INT16 *rowend = rowbeg + pitch2 - kernel;
   for (int i=begRow*3;i<endRow*3;i++) {
      for (int j=0;j<kernel;j++) {
           rowbeg[j] = rowbeg[kernel];
           rowend[j] = rowend[-1];
      }
      rowbeg += pitch2;
      rowend += pitch2;
   }

   // each time through loop generates a column 4 RGB triples wide
   int irow;
   for (irow=begRow;irow<endRow-4;irow+=4) {

      rrow0 = frmBuf2 + 3*irow*pitch2 + kernel;
      grow0 = rrow0 + pitch2;
      brow0 = grow0 + pitch2;

      rrow1 = brow0 + pitch2;
      grow1 = rrow1  + pitch2;
      brow1 = grow1 + pitch2;

      rrow2 = brow1 + pitch2;
      grow2 = rrow2  + pitch2;
      brow2 = grow2 + pitch2;

      rrow3 = brow2 + pitch2;
      grow3 = rrow3  + pitch2;
      brow3 = grow3 + pitch2;

      dst = dstptr + 3*irow;

      // generate the output
      for (int j=0;j<dsthght;j+=1) {

         KCoeff *iw = vTbl[j];

         int ctr = vctr[j];

         rval0 = 0;
         gval0 = 0;
         bval0 = 0;

         rval1 = 0;
         gval1 = 0;
         bval1 = 0;

         rval2 = 0;
         gval2 = 0;
         bval2 = 0;

         rval3 = 0;
         gval3 = 0;
         bval3 = 0;

         for (int k=-kernel;k<=kernel;k++) {

            int wgt = iw->wgt[MIDPT+k];

            int ivrt  = (ctr+k);

            rval0 += rrow0[ivrt]*wgt;
            gval0 += grow0[ivrt]*wgt;
            bval0 += brow0[ivrt]*wgt;

            rval1 += rrow1[ivrt]*wgt;
            gval1 += grow1[ivrt]*wgt;
            bval1 += brow1[ivrt]*wgt;

            rval2 += rrow2[ivrt]*wgt;
            gval2 += grow2[ivrt]*wgt;
            bval2 += brow2[ivrt]*wgt;

            rval3 += rrow3[ivrt]*wgt;
            gval3 += grow3[ivrt]*wgt;
            bval3 += brow3[ivrt]*wgt;
         }

         t0 = rval0>>wgtshft;
         if (t0 < 0) t0 = 0;
         if (t0 > maxcomponent) t0 = maxcomponent;
         dst[0] = t0;
         t0 = gval0>>wgtshft;
         if (t0 < 0) t0 = 0;
         if (t0 > maxcomponent) t0 = maxcomponent;
         dst[1] = t0;
         t0 = bval0>>wgtshft;
         if (t0 < 0) t0 = 0;
         if (t0 > maxcomponent) t0 = maxcomponent;
         dst[2] = t0;

         t0 = rval1>>wgtshft;
         if (t0 < 0) t0 = 0;
         if (t0 > maxcomponent) t0 = maxcomponent;
         dst[3] = t0;
         t0 = gval1>>wgtshft;
         if (t0 < 0) t0 = 0;
         if (t0 > maxcomponent) t0 = maxcomponent;
         dst[4] = t0;
         t0 = bval1>>wgtshft;
         if (t0 < 0) t0 = 0;
         if (t0 > maxcomponent) t0 = maxcomponent;
         dst[5] = t0;

         t0 = rval2>>wgtshft;
         if (t0 < 0) t0 = 0;
         if (t0 > maxcomponent) t0 = maxcomponent;
         dst[6] = t0;
         t0 = gval2>>wgtshft;
         if (t0 < 0) t0 = 0;
         if (t0 > maxcomponent) t0 = maxcomponent;
         dst[7] = t0;
         t0 = bval2>>wgtshft;
         if (t0 < 0) t0 = 0;
         if (t0 > maxcomponent) t0 = maxcomponent;
         dst[8] = t0;

         t0 = rval3>>wgtshft;
         if (t0 < 0) t0 = 0;
         if (t0 > maxcomponent) t0 = maxcomponent;
         dst[9] = t0;
         t0 = gval3>>wgtshft;
         if (t0 < 0) t0 = 0;
         if (t0 > maxcomponent) t0 = maxcomponent;
         dst[10] = t0;
         t0 = bval3>>wgtshft;
         if (t0 < 0) t0 = 0;
         if (t0 > maxcomponent) t0 = maxcomponent;
         dst[11] = t0;

         dst += frmpitch;
      }
   }

   // downconvert the residual lines
   irow -= 3;
   for (;irow<endRow;irow+=1) {

      rrow0 = frmBuf2 + 3*irow*pitch2 + kernel;
      grow0 = rrow0  + pitch2;
      brow0 = grow0 + pitch2;

      dst = dstptr + 3*irow;

      // generate the output
      for (int j=0;j<dsthght;j+=1) {

         KCoeff *iw = vTbl[j];

         int ctr = vctr[j];

         rval0 = 0;
         gval0 = 0;
         bval0 = 0;

         for (int k=-kernel;k<=kernel;k++) {

            int wgt = iw->wgt[MIDPT+k];

            int ivrt  = (ctr+k);

            rval0 += rrow0[ivrt]*wgt;
            gval0 += grow0[ivrt]*wgt;
            bval0 += brow0[ivrt]*wgt;
         }

         t0 = rval0>>wgtshft;
         if (t0 < 0) t0 = 0;
         if (t0 > maxcomponent) t0 = maxcomponent;
         dst[0] = t0;
         t0 = gval0>>wgtshft;
         if (t0 < 0) t0 = 0;
         if (t0 > maxcomponent) t0 = maxcomponent;
         dst[1] = t0;
         t0 = bval0>>wgtshft;
         if (t0 < 0) t0 = 0;
         if (t0 > maxcomponent) t0 = maxcomponent;
         dst[2] = t0;

         dst += frmpitch;
      }
   }

#if 0
   // after last stripe, render MAT
   if (iJob == (vp->nStripes-1)) {
      vp->drawMat3();
   }
#endif

}

////////////////////////////////////////////////////////////////////////////////
//
//                 KERNELS - using the Hamming for now
//
#define PI 3.1415927

#if 1 // Experimental - slight sharpening
double CZoomFractional::Kernel(double arg, int srcpts, int dstpts, int kern)
{
   if (fabs(arg) > .50) return 0.0;
   if (fabs(arg) > .25) return -.1;
   return 1.0;
}
#endif

#if 0 // Hamming
double CZoomFractional::Kernel(double arg, int srcpts, int dstpts, int kern)
{
   double retVal;

   if ((arg<-kern)||(arg>kern)) return(0.0);

   // km "zoom factor"
   double fFactor = ((double)dstpts/(double)srcpts);

   double argp = PI * arg;

   // km "sinc"
   if (argp==0.0) {

      retVal = fFactor;

   }
   else {

      retVal = sin(fFactor * argp) / argp;

   }

   // km "window"
   retVal *= (0.54 + 0.46*cos(argp/(double)kern));

   return retVal;
}
#endif

#if 0 // Blackman
double CZoomFractional::Kernel(double arg, int srcpts, int dstpts, int kern)
{
   double retVal;

   if ((arg<-(kern))||(arg>kern)) return(0.0);

   // km "zoom factor"
   double fFactor = ((double)dstpts/(double)srcpts);

   double argp = PI * arg;

   // km "sinc"
   if (argp==0.0) {

      retVal = fFactor;

   }
   else {

      retVal = sin(fFactor * argp) / argp;

   }

   // km "window"
   retVal *= (0.42 + 0.5*cos(argp/(double)kern) + .08*cos(2.0*argp/(double)kern));

   return retVal;
}
#endif




