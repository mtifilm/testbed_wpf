/*
	Name:	Repair.cpp
	By:	Kevin Manbeck
	Date:	June 29, 2001

	The Repair class is the base class for image analysis and repair.
*/

#include "IniFile.h"
#include "MTIio.h"
#include "MTImalloc.h"
#include <stdio.h>
#include <errno.h>
#include <time.h>      // time_t is broken in sys/types.h in RadStudio XE4
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>

#include "Repair.h"
#include "Mask.h"
#include "SysInfo.h"      // for SysInfo::AvailableProcessorCount()

static CPixelRegionList NULL_PixelRegionList;
CPixelRegionList *CRepair::NULL_pPixelRegionList = &NULL_PixelRegionList;


CRepair::CRepair()
: roi(true)                    // By default use label mask
{
  long lFrame, lCom;

  lNProcessor = SysInfo::AvailableProcessorCount(); // was hardwired to 4;
  lRegionULCRow = 0;
  lRegionULCCol = 0;
  lRegionLRCRow = 0;
  lRegionLRCCol = 0;
  lNRow = 0;
  lNCol = 0;
  lPitch = 0;
  lNCom = 0;
  lPrincipalCom = 0;
  lNFrame = 0;
  for (lFrame = 0; lFrame < MAX_INPUT_FRAME; lFrame++)
   {
    laTemporalIndex[lFrame] = 0;
    uspData[lFrame] = NULL;
   }
  lRepairFrame = 0;
  for (lCom = 0; lCom < MAX_COMPONENT; lCom++)
   {
    laMinValue[lCom] = 0;
    laMaxValue[lCom] = 0;
    laFillValue[lCom] = 0;
    baProcessCom[lCom] = false;
   }
  bDebugFlag = false;
  bCommandFileFlag = false;
  bFieldRepeatFlag = false;

  lDefaultResultSize = 0;
  uspDefaultResult = NULL;

  iPixelComponents = IF_PIXEL_COMPONENTS_INVALID;

  bUseRECT = false;
  bUseLASSO = false;
  bUseBEZIER = false;
  lasso = 0;
  iLassoAlloc = 0;
  bezier = 0;
  iBezierAlloc = 0;

  ucpROIBlend = 0;
  ucpROILabel = 0;

  pPixelRegionList = &NULL_PixelRegionList;

}  /* CRepair */

CRepair::~CRepair()
{
  MTIfree (uspDefaultResult);
  uspDefaultResult = NULL;

  free (lasso);
  lasso = 0;
}  /* ~CRepair */

int CRepair::setImageDimension (long RegionULCRow, long RegionULCCol,
	long RegionLRCRow, long RegionLRCCol, long nImageRows, long Pitch)
{
  long lNRowLoc, lNColLoc;

  lNRowLoc = RegionLRCRow - RegionULCRow;
  lNColLoc = RegionLRCCol - RegionULCCol;

  if (lNRowLoc <= 0 || lNColLoc <= 0 || Pitch < lNColLoc)
    return REPAIR_ERROR_BAD_PARAMETER;

  lRegionULCRow = RegionULCRow;
  lRegionULCCol = RegionULCCol;
  lRegionLRCRow = RegionLRCRow;
  lRegionLRCCol = RegionLRCCol;
  lNRow = lNRowLoc;
  lNCol = lNColLoc;
  lNImageRows = nImageRows;
  lPitch = Pitch;

  int iRet = AllocDefaultStorage ();
  if (iRet)
    return iRet;
 

  return 0;
}  /* setImageDimension */

int CRepair::setPixelComponents (EPixelComponents newPixelComponents)
{
  iPixelComponents = newPixelComponents;

  return 0;
}  /* setPixelComponents */

int CRepair::setImageComponent (long NCom, long PrincipalCom,
	MTI_UINT16 *pMinValue, MTI_UINT16 *pMaxValue, MTI_UINT16 *pFillValue)
{
  long lCom;

  if (NCom > MAX_COMPONENT || PrincipalCom >= NCom)
    return REPAIR_ERROR_BAD_PARAMETER;

  lNCom = NCom;
  lPrincipalCom = PrincipalCom;

  for (lCom = 0; lCom < lNCom; lCom++)
   {
    laMinValue[lCom] = (long)pMinValue[lCom];
    laMaxValue[lCom] = (long)pMaxValue[lCom];
    laFillValue[lCom] = (long)pFillValue[lCom];
    baProcessCom[lCom] = true;
   }

  int iRet = AllocDefaultStorage ();
  if (iRet)
    return iRet;

  return 0;
}  /* setImageComponent */

int CRepair::setImageComponent (long NCom, long PrincipalCom, long *pMinValue,
                                long *pMaxValue, long *pFillValue)
{
  long lCom;

  if (NCom > MAX_COMPONENT || PrincipalCom >= NCom)
    return REPAIR_ERROR_BAD_PARAMETER;

  lNCom = NCom;
  lPrincipalCom = PrincipalCom;

  for (lCom = 0; lCom < lNCom; lCom++)
   {
    laMinValue[lCom] = pMinValue[lCom];
    laMaxValue[lCom] = pMaxValue[lCom];
    laFillValue[lCom] = pFillValue[lCom];
    baProcessCom[lCom] = true;
   }

  int iRet = AllocDefaultStorage ();
  if (iRet)
    return iRet;

  return 0;
}  /* setImageComponent */

int CRepair::setNFrame (long NFrame)
{
  if (NFrame <= 0 || NFrame > MAX_INPUT_FRAME)
    return REPAIR_ERROR_BAD_PARAMETER;

  lNFrame = NFrame;

  return 0;
}  /* setNFrame */

int CRepair::setRepairFrame (long Frame)
{
  if (Frame < 0 || Frame >= MAX_INPUT_FRAME)
    return REPAIR_ERROR_BAD_PARAMETER;

  lRepairFrame = Frame;

  return 0;
}  /* setRepairFrame */

int CRepair::setDataPointer (long Frame, MTI_UINT16 *pData, long TemporalIndex)
{
  if (Frame < 0 || Frame >= MAX_INPUT_FRAME)
    return REPAIR_ERROR_BAD_PARAMETER;

  uspData[Frame] = pData;

  laTemporalIndex[Frame] = TemporalIndex;

  return 0;
}  /* setDataPointer */

int CRepair::setResultPointer (MTI_UINT16 *pData)
{
  uspResult = pData;

  return 0;
}  /* setResultPointer */

void CRepair::setDataTypeIsReallyHalfFloat (bool flag)
{
	bDataTypeIsReallyHalfFloat = flag;
}

void CRepair::setPixelRegionList (CPixelRegionList *newPixelRegionList,
											 long *newGlobalSerial, long newLocalSerial)
{
  pPixelRegionList = newPixelRegionList;
  lpGlobalPixelRegionListSerial = newGlobalSerial;
  lLocalPixelRegionListSerial = newLocalSerial;
}  /* setPixelRegionList */

int CRepair::setSuspendComponent (long Com)
{
  if (Com < 0 || Com >= MAX_COMPONENT)
    return REPAIR_ERROR_BAD_PARAMETER;

  baProcessCom[Com] = false;

  return 0;
}  /* setSuspendComponent */

int CRepair::setNProcessor (long NProcessor)
{
  if (NProcessor < 0)
    return REPAIR_ERROR_BAD_PARAMETER;

  lNProcessor = NProcessor;

  return 0;
}  /* setNProcessor */

void CRepair::setDebugFlag (bool DebugFlag)
{
  bDebugFlag = DebugFlag;

  return;
}  /* setDebugFlag */

void CRepair::setCommandFileFlag (bool CommandFileFlag)
{
  bCommandFileFlag = CommandFileFlag;

  return;
}  /* setCommandFileFlag */

void CRepair::setFieldRepeatFlag (bool bArg)
{
  bFieldRepeatFlag = bArg;
}  /* setFieldRepeatFlag */

void CRepair::PerformFieldRepeat ()
{
  long lRow, lCol, lRowColEven, lRowColOdd, lRowColComEven,
        lRowColComOdd, lComponent;
  int iNew;
  bool bModifiedEven, bModifiedOdd;

  if (bFieldRepeatFlag == false)
   {
    // no need to make the two fields identical, so return
    return;
   }

  // run through all the even pixels and force the odd pixel to have
  // the same value as the even pixel.

  for (lRow = (lRegionULCRow & ~0x1); lRow < lRegionLRCRow; lRow+=2)
  for (lCol = lRegionULCCol; lCol < lRegionLRCCol; lCol++)
   {
    lRowColEven = lRow * lPitch + lCol;
    lRowColOdd = (lRow+1) * lPitch + lCol;
    lRowColComEven = lRowColEven * lNCom;
    lRowColComOdd = lRowColOdd * lNCom;

    if (lRow+1 >= lRegionLRCRow)
     {
      // the accompanying ODD is out of bounds.  Set the EVEN value
      // back to its original value.

      if (uspResult != uspData[lRepairFrame])
       {
        for (lComponent = 0; lComponent < lNCom; lComponent++)
         {
          uspResult[lRowColComEven] = uspData[lRepairFrame][lRowColComEven];
          lRowColComEven++;
         }
       }
      else
       {
        // We don't have access to the original value at this point.  
        // The ODD value should not be out of bounds in normal operation.
       }
     }
    else
     {

      if (roi.getLabelPtr()[lRowColEven] & LABEL_MODIFIED)
        bModifiedEven = true;
      else
        bModifiedEven = false;

      if (roi.getLabelPtr()[lRowColOdd] & LABEL_MODIFIED)
        bModifiedOdd = true;
      else
        bModifiedOdd = false;

      if (bModifiedEven && bModifiedOdd)
       {
        // both pixels have been modified.  Replace with the average
        for (lComponent = 0; lComponent < lNCom; lComponent++)
         {
          iNew = (uspResult[lRowColComEven] + uspResult[lRowColComOdd]) / 2;
          uspResult[lRowColComEven] = iNew;
          uspResult[lRowColComOdd] = iNew;
          lRowColComEven++;
          lRowColComOdd++;
         }
       }
      else if (bModifiedEven)
       {
        // only the even pixel has been modified.  force it to equal the ODD
        for (lComponent = 0; lComponent < lNCom; lComponent++)
         {
          uspResult[lRowColComEven] = uspResult[lRowColComOdd];
          lRowColComEven++;
          lRowColComOdd++;
         }
       }
      else if (bModifiedOdd)
       {
        // only the odd pixel has been modified.  force it to equal the Even
        for (lComponent = 0; lComponent < lNCom; lComponent++)
         {
          uspResult[lRowColComOdd] = uspResult[lRowColComEven];
          lRowColComEven++;
          lRowColComOdd++;
         }
       }
     }
   }
}  /* PerformFieldRepeat */


int CRepair::AllocDefaultStorage ()
{
  if (lNRow <= 0 || lNCol <= 0 || lNCom <= 0)
  {
     return 0;
  }

  long newResultSize = (lRegionLRCRow + 1) * lPitch * lNCom;
  if (newResultSize <= lDefaultResultSize)
  {
     return 0;
  }

  lDefaultResultSize = newResultSize;

  MTIfree (uspDefaultResult);
  uspDefaultResult = NULL;

  uspDefaultResult = (MTI_UINT16 *) MTImalloc(lDefaultResultSize * sizeof (MTI_UINT16));
  if (uspDefaultResult == NULL)
  {
     return REPAIR_ERROR_MALLOC;
  }

  return 0;
}  /* AllocDefaultStorage */

int CRepair::setSelectRegion (const RECT &newRect)
{
  bUseRECT = true;
  bUseLASSO = false;
  bUseBEZIER = false;
  rect = newRect;
  iRenderBlendSize = 0;
  iRenderBlendType = BLEND_TYPE_UNKNOWN;
  iRenderSurroundSize = 0;

  return 0;
}

int CRepair::setSelectRegion (const POINT *newLasso)
{
  bUseRECT = false;
  bUseLASSO = true;
  bUseBEZIER = false;
  
  // how many points in lasso?
  int iCount = 2;
  while (newLasso[iCount].x != newLasso[0].x ||
        newLasso[iCount].y != newLasso[0].y)
   {
    iCount++;
   }

  iCount++;  // force closure
  
  // allocate storage for the lasso
  if (iCount > iLassoAlloc)
   {
    free (lasso);
    lasso = (POINT *) malloc (iCount * sizeof(POINT));
    if (lasso == NULL)
      return REPAIR_ERROR_MALLOC;
    iLassoAlloc = iCount;
   }

  // copy over the lasso
  for (int i = 0; i < iCount; i++)
    lasso[i] = newLasso[i];

  iRenderBlendSize = 0;
  iRenderBlendType = BLEND_TYPE_UNKNOWN;
  iRenderSurroundSize = 0;

  return 0;
}

int CRepair::setSelectRegion (const BEZIER_POINT *newBezier)
{
  bUseRECT = false;
  bUseLASSO = false;
  bUseBEZIER = true;
  
  // how many points in bezier? minimum is 3
  int iCount = 2;
  while (newBezier[iCount].xctrlpre != newBezier[0].xctrlpre ||
         newBezier[iCount].yctrlpre != newBezier[0].yctrlpre ||
         newBezier[iCount].x        != newBezier[0].x        ||
         newBezier[iCount].y        != newBezier[0].y        ||
         newBezier[iCount].xctrlnxt != newBezier[0].xctrlnxt ||
         newBezier[iCount].yctrlnxt != newBezier[0].yctrlnxt  )
   {
    iCount++;
   }

  iCount++;  // force closure
  
  // allocate storage for the bezier
  if (iCount > iBezierAlloc)
   {
    free (bezier);
    bezier = (BEZIER_POINT *) malloc (iCount * sizeof(BEZIER_POINT));
    if (bezier == NULL)
      return REPAIR_ERROR_MALLOC;
    iBezierAlloc = iCount;
   }

  // copy over the bezier
  for (int i = 0; i < iCount; i++)
    bezier[i] = newBezier[i];

  iRenderBlendSize = 0;
  iRenderBlendType = BLEND_TYPE_UNKNOWN;
  iRenderSurroundSize = 0;

  return 0;
}

void CRepair::setBorderValues (int newBlendSize, EBlendType newBlendType,
		int newSurroundSize)
{
  iBlendSize = newBlendSize;
  iBlendType = newBlendType;
  iSurroundSize = newSurroundSize;
}

void CRepair::setRegionOfInterestPtr (const MTI_UINT8 *ucpBlend,
		MTI_UINT8 *ucpLabel)
{
  ucpROIBlend = ucpBlend;
  ucpROILabel = ucpLabel;
}

int CRepair::renderMask ()
{
  // if nothing has changed, just return
  if (
	(iBlendSize == iRenderBlendSize) && 
	(iBlendType == iRenderBlendType) && 
	(iSurroundSize == iRenderSurroundSize)
     )
   {
    return 0;	// nothing has changed, so just return
   }

  // allocate storage for the masks
//  int iRet = roi.allocateBytemaps (lPitch, lRegionLRCRow);
  RECT regionRect;
  regionRect.left = lRegionULCCol;
  regionRect.top = lRegionULCRow;
  regionRect.right = lRegionLRCCol - 1;
  regionRect.bottom = lRegionLRCRow - 1;
  int iRet = roi.allocateBytemaps (lPitch, regionRect);
  if (iRet)
    return iRet;

  // set the render parameters
  roi.setBlendType (iBlendType);
  roi.setBlendRadiusInterior (iBlendSize);
  roi.setBlendRadiusExterior (0);  // DRS always blends to inside
  roi.setSurroundRadius (iSurroundSize);

  // set the shape
  roi.clearRegionList();
  if (bUseRECT)
    roi.addRegion (rect, true);  // DRS regions are always inclusive
  else if (bUseLASSO)
    roi.addRegion (lasso, true);  // DRS regions are always inclusive
  else if (bUseBEZIER)
    roi.addRegion(bezier, true);
  else
    return -1;	// should never happen

  // do the render
  roi.renderBlendAndLabelBytemaps ();

  // keep track of rendered parameters
  iRenderBlendSize = iBlendSize;
  iRenderBlendType = iBlendType;
  iRenderSurroundSize = iSurroundSize;

  return 0;
}
