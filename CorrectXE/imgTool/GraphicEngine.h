#ifndef GRENG
#define GRENG
#include <memory.h>
#include "Vertex.h"

#define MAXWORLDX 0x40000000
#define MAXWORLDY 0x40000000

#define TLFT 0x8
#define TTOP 0x4
#define TRGT 0x2
#define TBOT 0x1

class CLineEngine;

// CLIP-ENGINE class for world-to-screen graphics.

class CGraphicEngine
{

private:

   CLineEngine *l;

   int
	worldWinL,
        worldWinB,
        worldWinR,
        worldWinT;

   double
        xScaleFactor,
        yScaleFactor;

   int
        screenWinL,
        screenWinB,
        screenWinR,
        screenWinT;

   int
        VPreX, VPreY,
        VCurX, VCurY;

   int
        CpX, CpY;

   // calculates scale factors
   void setScaleFactors();

   // simple scaling of coords to screen
   int scaleX(double x);
   int scaleY(double y); 

   // calculates Cohn-Sutherland codes
   unsigned char tCode();

public:

   CGraphicEngine(CLineEngine *linEng);

   ~CGraphicEngine();

   void setWorldWindow(int l, int b, int r, int t);

   void setScreenWindow(int l, int b, int r, int t);

   void clipAndScale(VERTEX *vtxarry, int vtxcnt);

};

#endif

