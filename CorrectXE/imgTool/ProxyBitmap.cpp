//
// implementation file for class CProxyBitmap
//
#include "ProxyBitmap.h"
#include "Convert.h"
#include "ImageFormat3.h"
#include "Clip3.h"
#include "err_imgtool.h"
#include "LUTManager.h"
#include "MTImalloc.h"

//
//  C O N S T R U C T O R
//
ProxyBitmap::ProxyBitmap(CConvert *sharedConverter)
{

   // handle to memory device context for DIB
   hBitmapDC = NULL;

   // clear dimensions of bitmap
   bitmapWdth = 0;
   bitmapHght = 0;

   // handle to bitmap
   hBitmap = NULL;

   // -> frame buffer
   bitmapFB = NULL;

   // allocate a display LUT manager
   lutmgr = new CLUTManager;

   // display lut to deal with gamma
   currentDisplayLUT = NULL;

   // Proxy converter - either shared or lazily instantiated
   converter = sharedConverter;
   converterIsShared = (converter != NULL);

   // image format - lazily instantiated
   myImageFormat = NULL;
}

//
//  D E S T R U C T O R
//
ProxyBitmap::~ProxyBitmap()
{
#ifdef __BORLANDC__
   // delete the DIB section
   if (hBitmap != NULL)
      DeleteObject(hBitmap);

   // delete memory device context
   if (hBitmapDC != NULL)
      DeleteDC(hBitmapDC);
#else
   delete [] bitmapFB;
#endif

   // delete conversion stuff
   if (!converterIsShared)
      delete converter;
   delete myImageFormat;
}

//
// initialize the bitmap
//
int ProxyBitmap::initBitmap(HDC hndl, int wdth, int hght)
{
   // assume failure
   int retval = -1;

#ifdef __BORLANDC__
   // put instance into known state:
   //   delete bitmap handle
   //   zero bitmaps dims
   //   delete bitmap device context
   //
   DeleteObject(hBitmap);
   hBitmap = NULL;

   bitmapWdth = 0;
   bitmapHght = 0;

   DeleteDC(hBitmapDC);
   hBitmapDC = NULL;

   // get parent device context
   HDC screenDC = GetDC((HWND)hndl);

   // create the memory device context
   hBitmapDC = CreateCompatibleDC(screenDC);

   if (hBitmapDC != NULL) { // valid memory DC

      // create the DIB bitmap
      unsigned char bminfo[sizeof(BITMAPINFOHEADER)+256*sizeof(RGBQUAD)];

      BITMAPINFO *bmi = (BITMAPINFO *) bminfo;
      bmi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
      bmi->bmiHeader.biWidth  =  wdth;
      bmi->bmiHeader.biHeight = -hght;
      bmi->bmiHeader.biPlanes = 1;
      bmi->bmiHeader.biBitCount = 32;
      bmi->bmiHeader.biCompression = 0;
      bmi->bmiHeader.biSizeImage = wdth*hght*4;
      bmi->bmiHeader.biXPelsPerMeter = 40*GetDeviceCaps(screenDC,LOGPIXELSX);
      bmi->bmiHeader.biYPelsPerMeter = 40*GetDeviceCaps(screenDC,LOGPIXELSY);
      bmi->bmiHeader.biClrUsed      = 256;
      bmi->bmiHeader.biClrImportant = 256;

      // -> bitmap frame buffer
      bitmapFB  = NULL;

      // create the in-memory bitmap & get its handle
      hBitmap = CreateDIBSection(screenDC,
                                 (const BITMAPINFO *)bminfo,
                                 DIB_RGB_COLORS,
			         (void **)&bitmapFB,
                                 NULL,
			          0);

      // if DIB creation failed return error code
      if (hBitmap == NULL) { // DIB allocation failure

         // unneeded now
         DeleteDC(hBitmapDC);

         // clear this
         hBitmapDC = NULL;

      }  // DIB allocation failure

      else { // have DIB

         // select the DIB into the memory device context
         SelectObject(hBitmapDC,hBitmap);

         // register dimensions of bitmap
         bitmapWdth = wdth;
         bitmapHght = hght;

         // select the DIB into the memory device context
         SelectObject(hBitmapDC,hBitmap);

         // clear the new bitmap
         clearBitmap();

         // success
         retval = 0;

      } // valid DIB

   } // valid memory DC

   // done with this
   ReleaseDC((HWND)hndl,screenDC);

#else // SGI version

   // allocate bitmap & clear it
   bitmapFB = new unsigned int[wdth*hght];
   clearBitmap();

   // success
   retval = 0;
#endif

   return(retval);
}

// clear the bitmap
void ProxyBitmap::clearBitmap()
{
   int pels = bitmapWdth*bitmapHght;
   MTImemset(bitmapFB, 0, pels*4);
}

//
// renders frame data into the bitmap
// this version passes in a clip scheme name to specify image format
//
int ProxyBitmap::renderToBitmap(MTI_UINT8 **frameData,
                                const string &imageFormatName,
                                const RECT *srcRect, const RECT *dstRect)
{
    int retVal = 0;

    if (myImageFormat == NULL)
        myImageFormat = new CImageFormat;

    if (imageFormatName != oldImageFormatName && myImageFormat != NULL) {
        CClipInitInfo clipInitInfo;
        CVideoInitInfo* mainVideoInitInfo = 0;
        CImageFormat *pImageFormat = 0;

        retVal = clipInitInfo.initFromClipScheme(imageFormatName);
        if (retVal == 0) {
            mainVideoInitInfo = clipInitInfo.getMainVideoInitInfo();
            pImageFormat = mainVideoInitInfo->getImageFormat();
            if (pImageFormat != 0) {
                *myImageFormat = *pImageFormat;
                oldImageFormatName = imageFormatName;
            } else {
                TRACE_0(errout << "ProxyBitmap::render: "
                               << "getImageFormat returned NULL");
                retVal = -1; // "Can't happen"
            }
        } else {
            TRACE_0(errout << "ProxyBitmap::render: "
                           << "InitClipFromClipScheme(" << imageFormatName
                           << ") failed, retVal=" << retVal);
        }

    }
    if (retVal == 0) {
        retVal = renderToBitmap(frameData, myImageFormat, srcRect, dstRect);
    }
    return retVal;
}


//
// renders frame data into the bitmap
// this version passes in an actual image format
//
int ProxyBitmap::renderToBitmap(MTI_UINT8 **frameData,
                                const CImageFormat *imageFormat,
                                const RECT *srcRect, const RECT *dstRect)
{
    int retVal = 0;
    MTI_UINT32 *dstAddress = getBitmapFrameBuffer();
    int dstPitch = bitmapWdth;
    int dstWdth = bitmapWdth;
    int dstHght = bitmapHght;
    MTI_UINT8 *currentLUTPtr = 0;
    CDisplayFormat currentDisplayFormat;
    EColorSpace colsp = IF_COLOR_SPACE_INVALID;

    if (converter == NULL) {
        converter = new CConvert(1);
    }
    if (converter != NULL && imageFormat != NULL) {
        RECT localSrcRect;

        // adjust src rect if needed
        if (srcRect == NULL) {
            localSrcRect.left = localSrcRect.top = 0;
            localSrcRect.right = localSrcRect.bottom = -1;
        } else {
            localSrcRect.left   = srcRect->left;
            localSrcRect.top    = srcRect->top;
            localSrcRect.right  = srcRect->right;
            localSrcRect.bottom = srcRect->bottom;
        }
        if (localSrcRect.right < 0) {
            localSrcRect.right = imageFormat->getPixelsPerLine() - 1;  // incl
        }
        if (localSrcRect.bottom < 0) {
            localSrcRect.bottom = imageFormat->getLinesPerFrame() - 1; // incl
        }

        // adjust dest params if needed
        if (dstRect != 0) {
            clearBitmap();
            dstAddress += ((dstRect->top * dstPitch) + dstRect->left);
            dstWdth = dstRect->right - dstRect->left + 1;
            dstHght = dstRect->bottom - dstRect->top + 1;
        } else { /**
            dstHght = (int) (dstWdth
                                / imageFormat->getFrameAspectRatio());
            if (dstHght > bitmapHght) {
                dstHght = bitmapHght;
            }
            **/
        }

        // now create a LUT based on the image format & display format
        // I copied this from the Displayer
        colsp = imageFormat->getColorSpace();
        currentDisplayFormat.bSourceLogSpace = (colsp == IF_COLOR_SPACE_LOG || colsp == IF_COLOR_SPACE_EXR);
        currentDisplayFormat.faDisplayMin[0] = 0.;
        currentDisplayFormat.faDisplayMin[1] = 0.;
        currentDisplayFormat.faDisplayMin[2] = 0.;
        currentDisplayFormat.faDisplayMax[0] = 1.;
        currentDisplayFormat.faDisplayMax[1] = 1.;
        currentDisplayFormat.faDisplayMax[2] = 1.;
        // gamma is the only thing we really care about; compute value
        // needed to get to a disp[lay gamma of 1.7 ( = 1.7/(imageGamma) )
        // QQQ Should use preferernce default value instead of 1.7!!
        // UPDATE: I found out that this calls ColorSPaceCOnvert, which
        //         takes into account the imageGamma, so just use 1.7 here!
        currentDisplayFormat.fDisplayGamma = 1.7;

        lutmgr->Release(currentDisplayLUT);
        currentDisplayLUT = lutmgr->Create(*imageFormat,
                                           currentDisplayFormat);

        if (currentDisplayLUT != 0) {
            currentLUTPtr = currentDisplayLUT->getTablePtr();
        }
        // END OF stuff copied from Displayer

        // convert YUV to RGB in the proxy bitmap
         TRACE_3(errout << "CONVERT 4");
        converter->convertWtf(frameData,
                           imageFormat,
                           &localSrcRect,
                           dstAddress,
                           dstWdth,
                           dstHght,
                           dstPitch * 4,    // in bytes!!
                           currentLUTPtr);
    } else {
        retVal = IMGTOOL_ERROR_MALLOC;
    }
    return retVal;
}

