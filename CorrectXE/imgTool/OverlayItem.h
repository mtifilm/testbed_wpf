// OverlayItem.h: interface for the COverlayItem class.
//
//////////////////////////////////////////////////////////////////////

#ifndef OVERLAYITEM_H
#define OVERLAYITEM_H

#include "imgToolDLL.h"
#include "machine.h"

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CLineEngine;
class CImageFormat;
class CTimecode;

//////////////////////////////////////////////////////////////////////

class MTI_IMGTOOLDLL_API COverlayItem  
{
public:
   COverlayItem();
   virtual ~COverlayItem();

   void init(int xLoc, int yLoc, int fgColor, int bgColor);
   virtual void process(CImageFormat const *imageFormat, CTimecode const &tc,
                        MTI_UINT8 *buffers[]) = 0;

protected:
   int x;               // Left edge of overlay item
   int y;               // Upper edge of overlay item (assuming increasing y
                        // is lower on the image)
   int foregroundColor; // Foreground color
   int backgroundColor; // Background color

private:
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class MTI_IMGTOOLDLL_API COverlayText : public COverlayItem
{
public:
   COverlayText();
   virtual ~COverlayText();

   void init(int xLoc, int yLoc, int fgColor, int bgColor, 
             int newFontSize);
   virtual void process(CImageFormat const *imageFormat, CTimecode const &tc,
                        MTI_UINT8 *buffers[]);

protected:
   int fontSize;        // Font scale factor

   void drawText(const char* str, CLineEngine *pixelEng);
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////


class MTI_IMGTOOLDLL_API COverlayTimecode : public COverlayText  
{
public:
   COverlayTimecode();
   virtual ~COverlayTimecode();

   void init(int xLoc, int yLoc, int fgColor, int bgColor,
             int fontSize);
   virtual void process(CImageFormat const *imageFormat, CTimecode const &tc,
                        MTI_UINT8 *buffers[]);
};

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//
//  The ScaledOverlay class is used to burn data into an image.
//  ScaledOverlay uses the image format to scale the position and
//  font size.
//
//////////////////////////////////////////////////////////////////////


class MTI_IMGTOOLDLL_API CScaledOverlay
{
public:
   CScaledOverlay();
   ~CScaledOverlay();

   void setImageFormat (CImageFormat const &imageFormat);

   // Setting the font size to 1. will create a small font in 525 material.
   // it will be proportionally the same size in other formats.
   // setting it smaller will make smaller fonts.  Setting it larger will
   // make larger fonts
   void setFontSize (float fFontSizeArg);

   // setImagePosition takes arguments that are percent of image dimension
   // with (0., 0.) corresponding to upper-left-corner.  (1., 1.) is the
   // lower-right-corner
   void setImagePosition (float fRowPosArg, float fColPosArg);

   void setColor (int iColorFGArg, int iColorBGArg);

   void Process (MTI_UINT8 *ucpData[], const string &strArg);
   void Process (MTI_UINT8 *ucpData[], const CTimecode &tcArg);

private:
   CLineEngine *lepLineEngine;

   float fFontSize;
   int iColorFG;
   int iColorBG;
   float fRowPos;
   float fColPos;
   int iNRow;
   int iNCol;
};

//////////////////////////////////////////////////////////////////////

#endif // #ifndef OVERLAYITEM_H

