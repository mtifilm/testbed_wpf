/*
	File:	AspectRatioLB.h
	By:	Kevin Manbeck
	Date:	Apr 9, 2005

	AspectRatioLB is used to convert anamorphic images into
	the letter boxed version.  The conversion must be very
	efficient so that it can be done in real time.

*/

#ifndef ASPECT_RATIO_LB
#define ASPECT_RATIO_LB

#include "imgToolDLL.h"
#include "machine.h"

class CImageFormat;
class CAspectRatio;

class MTI_IMGTOOLDLL_API CAspectRatioLB
{
public:
  CAspectRatioLB();
  ~CAspectRatioLB();

  void Free ();
  int Init (const CImageFormat *ifp, int iQuality);
  int Render (MTI_UINT8 *ucpSrc[2], MTI_UINT8 *ucpDst[2]);

  int getMatteCoordinates (int iJustify, float fMatteRatio,
		int &iRow0, int &iRow1, int &iCol0, int &iCol1);

private:

  CAspectRatio 
    *arp;
};

#endif
	

