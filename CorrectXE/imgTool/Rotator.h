//
// include file for class CRotator
//
#ifndef RotatorH
#define RotatorH

#include "machine.h"
#include "imgToolDLL.h"
#include "mthread.h"
#include "ImageInfo.h"

#define TLFT 0x8
#define TTOP 0x4
#define TRGT 0x2
#define TBOT 0x1

////////////////////////////////////////////////////////////

class CImageFormat;

class MTI_IMGTOOLDLL_API CRotator
{

public:

   CRotator(int nStripes = 0);

   ~CRotator();

   void rotateFast (MTI_UINT16 *dst, MTI_UINT16 *src, const CImageFormat *srcfmt,
                    double angle,
                    double stretchX,
                    double stretchY,
                    double skew,
                    double xoffs = 0.0, double yoffs = 0.0);

   void rotateExact(MTI_UINT16 *dst, MTI_UINT16 *src, const CImageFormat *srcfmt,
                    double angle,
                    double stretchX,
                    double stretchY,
                    double skew,
                    double xoffs = 0.0, double yoffs = 0.0);

private:

   void deriveForwardMatrix();

   void deriveInverseMatrix();

   void transformPoint(int xin, int yin, int& xout, int& yout);

   void inverseTransformPoint(int xin, int yin, int& xout, int& yout);

   void initializeForFastScan();

   void initializeForExactScan();

   static void rowLoopCounts(int xbeg, int ybeg,
                             int xend, int yend,
                             int xlim, int ylim,
                             int w,
                             int *z1, int *d, int *z2);

   static void null(void *v, int iJob);
   static void rotateFastYUV_8   (void *v, int iJob);
   static void rotateExactYUV_8  (void *v, int iJob);
   static void rotateFastYUV_10  (void *v, int iJob);
   static void rotateExactYUV_10 (void *v, int iJob);
   static void rotateFastRGB     (void *v, int iJob);
   static void rotateExactRGB    (void *v, int iJob);
   static void rotateFastRGB_A1  (void *v, int iJob);
   static void rotateExactRGB_A1 (void *v, int iJob);
   static void rotateFastRGB_A2  (void *v, int iJob);
   static void rotateExactRGB_A2 (void *v, int iJob);
   static void rotateFastRGB_A8  (void *v, int iJob);
   static void rotateExactRGB_A8 (void *v, int iJob);
   static void rotateFastRGB_A16 (void *v, int iJob);
   static void rotateExactRGB_A16(void *v, int iJob);
   static void rotateFastMON     (void *v, int iJob);
   static void rotateExactMON    (void *v, int iJob);

   // destination bufffer
   MTI_UINT16 *dstpels;

   // source buffer
   MTI_UINT16 *srcpels;

   // width of frame in pels
   int frmWdth;

   // height of frame in pels
   int frmHght;

   // net rotation for transform
   double angle;

   // x and y stretch factors
   double stretchX,
          stretchY;

   // skew (rel to unit square)
   double skew;

   // fractional-pixel offset
   double xoffs,
          yoffs;

   // fwd matrix
   double fwd00,
          fwd01,
          fwd10,
          fwd11;

   // inv matrix
   double bwd00,
          bwd01,
          bwd10,
          bwd11;

   // (lft, top) of scan
   int xlt, ylt;

   // (rgt, top) of scan
   int xrt, yrt;

   // (lft, bot) of scan
   int xlb, ylb;

   // unit vector for rowwise pels
   int rowdelx, rowdely;

   // unit vector for colwise pels
   int coldelx, coldely;

   ///////////////////////////////////////////////////////////////

   int nStripes; // number of stripes in frame (1,2,4,8, or 16)

   MTHREAD_STRUCT tsThread;

   ///////////////////////////////////////////////////////////////

};

#endif

