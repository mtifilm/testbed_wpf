/*
	File:	RegionOfInterest.h
	By:	Kevin Manbeck
	Date:	Oct 6, 2003

	The RegionOfInterest class is used to pass information to the
	image processing algorithms.  This information describes which 
	pixels should be processed and how they should be processed.

	The RegionOfInterest is basically two 8-bit arrays:  one containing
	blending instructions for combining the original image and 
	the processed image.  The second array is a set of labels which may
 	be used by the algorithms.

	The 8-bit arrays are the same dimension as the image.  The values
	in the arrays are generated relative to a shape supplied by the 
	user.  The shape can be rectangle, lasso, or bezier.
*/
//
// include file for class CROIMask
//
#ifndef RegionOfInterestH
#define RegionOfInterestH

#include <vector>
#include "machine.h"
#include "imgToolDLL.h"             // Export/Import Defs

class CROIMask;

enum EBlendType
{
  BLEND_TYPE_UNKNOWN = 0,
  BLEND_TYPE_NONE = 0,              // Alias for FRAMEWRX
  BLEND_TYPE_LINEAR  = 1,
  BLEND_TYPE_DITHER  = 2
};

#define LABEL_NONE         0x0
#define LABEL_INTERIOR     0x1
#define LABEL_SURROUND     0x2  // a surrounding pixel used by DRS
#define LABEL_FIRST_UNUSED 0x4  // first unused bit.  It is available for
                                // use by algorithms.

// CUserRegion is a support class which allows the user to input
// several different types of shapes.
class CUserRegion
{
public:
  CUserRegion(bool newInclusiveRegionFlag);
  virtual ~CUserRegion();

  void setBlendType(EBlendType newType);       // type of blend
  void setBlendRadiusInterior(int newRadius);  // radius of INTERIOR blend
  void setBlendRadiusExterior(int newRadius);  // radius of EXTERIOR blend

  MTI_UINT8 getBlendLevel(int layer);          // layer is neg going inside
                                               // pos going outside

  RECT getBlendBox();                          // box around blend mask
  RECT getLabelBox();                          // box around label mask

  void setSurroundRadius(int newRadius);       // radius of region surrounding
                                               // user's shape.  Used by DRS

  virtual int renderBlendMask(CROIMask *blendEngine)=0; // these methods assume that the
  virtual int renderLabelMask(CROIMask *labelEngine)=0; // resp masks have already been
                                                        // attached to the engines

  RECT getBoundingRectangle();                 // bounding rectangle of region

protected:
  bool bInclusiveRegionFlag; // true = inclusive, false = exclusive

  RECT boundingRectangle;

  EBlendType
    iBlendType;

  int
    iBlendRadiusInterior,
    iBlendRadiusExterior,
    iSurroundRadius;
};

class CRectUserRegion : public CUserRegion
{
public:
  CRectUserRegion(const RECT &newRect, bool newInclusiveRegionFlag);
  virtual ~CRectUserRegion();

  int renderBlendMask(CROIMask *blendEngine);
  int renderLabelMask(CROIMask *labelEngine);

private:
  RECT rect;
};

class CLassoUserRegion : public CUserRegion
{
public:
  CLassoUserRegion (const POINT *newLasso, bool newInclusiveRegionFlag);
  virtual ~CLassoUserRegion ();

  int renderBlendMask(CROIMask *blendEngine);
  int renderLabelMask(CROIMask *labelEngine);

private:
  POINT *lasso;
  int vertexCount;
};

class CBezierUserRegion : public CUserRegion
{
public:
  CBezierUserRegion (const BEZIER_POINT *newBezier, bool newInclusiveRegionFlag);
  virtual ~CBezierUserRegion ();

  int getTotalChords(int leng); // get total no of chords to render spline,
                                // given the avg length of a chord

  int renderBlendMask(CROIMask *blendEngine);
  int renderLabelMask(CROIMask *labelEngine);

private:
  BEZIER_POINT *bezier;
  int vertexCount;
};

class CBezierListUserRegion : public CUserRegion
{
public:
  CBezierListUserRegion(const BEZIER_POINT *newBezier, int vtxcnt, bool newInclusiveRegionFlag);
  virtual ~CBezierListUserRegion ();

  int getTotalChords(int leng);

  int renderBlendMask(CROIMask *blendEngine);
  int renderLabelMask(CROIMask *labelEngine);

private:
  BEZIER_POINT *bezier;
  int vertexCount;
};

class MTI_IMGTOOLDLL_API CRegionOfInterest
{
public:
  // mbraca added arg to suppress label bytemap - only DRS uses it!
  CRegionOfInterest(bool needLabelByteMapArg=false );
  CRegionOfInterest(const CRegionOfInterest &rhs);
  CRegionOfInterest &operator=(const CRegionOfInterest &rhs);
  ~CRegionOfInterest();

  int allocateBytemaps(int iPitch, const RECT &newSubImage);
            // allocate bytemaps for a sub region of the image.

  int allocateBytemaps(const RECT &activePictureRect);
        // allocate blend and label bytemaps for entire image

  void setBlendType(EBlendType newType);       // type of blend
  void setBlendRadiusInterior(int newRadius);  // radius of INTERIOR blend
  void setBlendRadiusExterior(int newRadius);  // radius of EXTERIOR blend

  void setSurroundRadius(int newRadius);       // radius of region surrounding
                                               // user's shape.  Used by DRS

  void clearRegionList(bool okToDeleteRegions=true);
                                               // clears list of user regions
                                               // comprising the ROI

  void addRegion(const RECT  &newRect,  bool newInclusionFlag);
  void addRegion(const POINT *newLasso, bool newInclusionFlag);
  void addRegion(const BEZIER_POINT *newBezier, bool newInclusionFlag);
  void addRegion(const BEZIER_POINT *newBezier, int vtxcnt, bool newInclusionFlag);

  int renderBlendAndLabelBytemaps(); // render the constituent user regions
                                     // into the ROI's blend and label bytemaps

  int renderFullFrameBlendAndLabelBytemaps();

  RECT getBlendBox();	    // bounding box surrounding ucpBlend > 0
  RECT getLabelBox();	    // bounding box surrounding ucpLabel != LABEL_NONE

  inline const MTI_UINT8 *getBlendPtr() const { return ucpBlend; };
  inline MTI_UINT8 *getLabelPtr() const  { return ucpLabel;  };


private:
  vector <CUserRegion *> listUserRegion;

  CROIMask *maskEngine;         // engine to perform fills, blending, & labels

  MTI_UINT8 *ucpBlend;		// use W = (float)ucpBlend[x]/255. to determine
				// weight of processed pixel.  (1-W) is the
				// weight of the unprocessed pixel.
  MTI_UINT8 *ucpLabel;		// labels used to control some algorithms.
                                // see LABEL_* for values.
  bool needLabelByteMap;        // mbraca added this cause only DRS needs the
                                // label mask!!
                                
  EBlendType
    iBlendType;

  int
    iBlendRadiusInterior,
    iBlendRadiusExterior,
    iSurroundRadius;

  RECT
    activePictureRect;		// active portion of the image

  int
    iPitch;	// pitch of the image

  int
    iMaskAllocSize;  // largest mask allocated
};

#endif


