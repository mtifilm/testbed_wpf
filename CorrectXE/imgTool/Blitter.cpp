//
//  implementation file for class CBlitter
//
#include <stdio.h>
#include <stdlib.h>

#include "ImageFormat3.h"
#include "ImageInfo.h"
#include "Blitter.h"
#include "machine.h"
#include "MTImalloc.h"
#ifdef __sgi
  #include <sys/sysmp.h>
  #include <unistd.h>
#endif

//////////////////////////////////////////////////////////////
//
//                  C B l i t t e r
//
//        Author: Kurt Tolksdorf
//        Begun:  7/14/02
//
// Blit facility for adjusting position of DRS repair data on a
// pixel-by-pixel basis, using the arrow keys. It does this
// by means of successive BLT's from a DATA SURFACE containing
// the repaired pixels to a MAIN SURFACE which is a copy of the
// original frame. To change the position of the repaired pixels,
// the original pixels are restored from a RESTORE SURFACE
// (again a copy of the original frame) and the fixed pixels
// are BLTed to the MAIN SURFACE at the new position. The
// BLT operation will accept a MASK so that only selected
// pixels within the source rectangle are copied. 
//
// To use the facility the user does the following:
//
//	  i) calls 'setSurfaceDimensions' to establish
//           the dimensions of ALL the 16-bit 444 surfaces
//           to be used (we assume they're the same)
//
//       ii) calls 'setMainSurface' to pass the address of
//           the buffer holding the main surface pixels
//
//      iii) calls 'setRestoreSurface' to pass the address
//           of the buffer to hold the backup of the original
//           frame
//
//       iv) calls 'initRestoreSurface' to copy the original
//           frame's data to the restore surface (the user
//           could do this but it was thought convenient
//           to provide an entry point)
//
//        v) calls 'setDataSurface' to establish the source
//           buffer for repaired pixel data
//
//       vi) makes successive calls to 'bltDataToMainSurface'
//           to overlay repaired pixels onto the main
//           surface at an arbitrary position. A mask may
//           be supplied to determine which pixels within
//           the BLTed region are to be copied.
//
// After each call to 'bltDataToMainSurface', the user will
// call the EXTRACTOR's method REPLACEACTIVEPICTURE to generate
// a full frame in the original format, for display on the
// computer screen and/or video monitor.
//
// To summarize, the implementation of ARROW KEYS will include
// the following:
//
//    [INITIALIZATION]
//       Call Blt->setSurfaceDimensions to establish dimensions
//          of main surface, restore surface, data surface, and
//          mask surface (if used)
//       Call Blt->setMainSurface
//       Call Blt->setRestoreSurface
//       Call Blt->setDataSurface
//       Call Blt->initRestoreSurface to make a backup copy of 
//          the (unaltered) main surface
//       Establish initial (x,y) position of repair pixels
//
//    [BEGIN LOOP]
//       Call Blt->bltDataToMainSurface to overlay the repair
//          pixels onto the main surface at (x,y)
//       Call Extractor->replaceActivePicture to generate a
//          frame in the original format from the 16-bit 444
//          main surface with repair pixels overlaid on it
//          (the buffer used to hold the new frame belongs to the
//          Player and is the same one that held  the original)
//       Call Displayer->displayFrame to display the frame
//          with overlaid repair pixels on the computer screen
//       Call Player->outputVideo to display the frame with
//          overlaid repair pixels on the video monitor
//       Wait for user key input 
//       If user accepted the fix, BREAK
//       Else if input is ARROw key, establish new (x,y)
//          position of repair pixels
//   [END LOOP]

CBlitter::CBlitter()
{
   mainSurface = NULL;
   restoreSurface = NULL;
   dataSurface = NULL;
}

CBlitter::~CBlitter()
{
}

void CBlitter::setSurfaceDimensions(int wdth, int hght, int pitch)
{
   surfWdth  = wdth;
   surfHght  = hght;
   surfPitch = pitch/2;
}

void CBlitter::setSurfaceDimensions(const CImageFormat& fmt)
{
   surfWdth = fmt.getPixelsPerLine();
   surfHght = fmt.getLinesPerFrame();
   surfPitch = surfWdth*3;
}

void CBlitter::setMainSurface(MTI_UINT16 *mainsurf)
{
   mainSurface = mainsurf;

   // set flag to indicate that no repair data
   // has been BLTed to the main surface yet
   dataIsOnMainSurface = false;
}

void CBlitter::setRestoreSurface(MTI_UINT16 *restsurf)
{
   restoreSurface = restsurf;
}

void CBlitter::setDataSurface(MTI_UINT16 *datasurf)
{
   dataSurface = datasurf;
}

// make a copy of the main surface to use for restoring
// it when repositioning the repaired data
void CBlitter::initRestoreSurface()
{
   if ((mainSurface!=NULL)&&(restoreSurface!=NULL)) 
      MTImemcpy((void *)restoreSurface, (void *)mainSurface, surfWdth*surfHght*6);
}

// blt pixels from data surface to main surface, after
// first restoring the main surface from the previous BLT
void CBlitter::bltDataToMainSurface(int xdst, int ydst,
                               RECT *srcrct)
{
   if (dataIsOnMainSurface) {

      // copy from restore surface to main surface
      int rowCnt = curRect.bottom - curRect.top + 1;
      int pelCnt = curRect.right - curRect.left + 1;
      int rowAdv = surfPitch - pelCnt*3;

      MTI_UINT16 *src = restoreSurface + 
                        curRect.top*surfPitch +
                        curRect.left*3;

      MTI_UINT16 *dst = mainSurface +
                        curRect.top*surfPitch +
                        curRect.left*3;

      for (int i=0;i<rowCnt;i++) {
         for (int j=0;j<pelCnt;j++) {
            *dst++ = *src++;
            *dst++ = *src++;
            *dst++ = *src++; 
         }
         src += rowAdv;
         dst += rowAdv;
      }

   }

   int rowCnt = srcrct->bottom - srcrct->top + 1;
   int pelCnt = srcrct->right - srcrct->left + 1;
   int rowAdv = surfPitch - pelCnt*3;

   MTI_UINT16 *src = dataSurface + 
                     srcrct->top*surfPitch +
                     srcrct->left*3;

   MTI_UINT16 *dst = mainSurface +
                     ydst*surfPitch +
                     xdst*3;

   for (int i=0;i<rowCnt;i++) {
      for (int j=0;j<pelCnt;j++) {
         *dst++ = *src++;
         *dst++ = *src++;
         *dst++ = *src++; 
      }
      src += rowAdv;
      dst += rowAdv;
   }

   curRect.left   = xdst;
   curRect.top    = ydst;
   curRect.right  = xdst + pelCnt - 1;
   curRect.bottom = ydst + rowCnt - 1;

   dataIsOnMainSurface = true;
}

// blt pixels from data surface to main surface, after
// first restoring the main surface from the previous BLT
// A mask may be supplied to control the copying of pixels
void CBlitter::bltDataToMainSurface(int xdst, int ydst,
                               RECT *srcrct,
                               MTI_UINT16 *mask,
                               MTI_UINT16 mskenbl)
{
   if (dataIsOnMainSurface) {

      // copy from restore surface to main surface
      int rowCnt = curRect.bottom - curRect.top + 1;
      int pelCnt = curRect.right - curRect.left + 1;
      int rowAdv = surfPitch - pelCnt*3;

      MTI_UINT16 *src = restoreSurface +
                        curRect.top*surfPitch +
                        curRect.left*3;

      MTI_UINT16 *dst = mainSurface +
                        curRect.top*surfPitch +
                        curRect.left*3;

      for (int i=0;i<rowCnt;i++) {
         for (int j=0;j<pelCnt;j++) {
            *dst++ = *src++;
            *dst++ = *src++;
            *dst++ = *src++;
         }
         src += rowAdv;
         dst += rowAdv;
      }
   }

   int rowCnt = srcrct->bottom - srcrct->top + 1;
   int pelCnt = srcrct->right - srcrct->left + 1;
   int rowAdv = surfPitch - pelCnt*3;

   if (mask==NULL) { // simple copy from data surface

      MTI_UINT16 *src = dataSurface +
                        srcrct->top*surfPitch +
                        srcrct->left*3;

      MTI_UINT16 *dst = mainSurface +
                        ydst*surfPitch +
                        xdst*3;

      for (int i=0;i<rowCnt;i++) {
         for (int j=0;j<pelCnt;j++) {
            *dst++ = *src++;
            *dst++ = *src++;
            *dst++ = *src++;
         }
         src += rowAdv;
         dst += rowAdv;
      }
   }
   else {            // copy respecting mask contents

      int mskAdv = surfPitch/3 - pelCnt;

      MTI_UINT16 *src = dataSurface +
                        srcrct->top*surfPitch +
                        srcrct->left*3;

      MTI_UINT16 *msk = mask +
                        srcrct->top*(surfPitch/3) +
                        srcrct->left;

      MTI_UINT16 *dst = mainSurface +
                        ydst*surfPitch +
                        xdst*3;

      // here we copy only those pixels whose corresp
      // value in the mask is equal to the enable value
      for (int i=0;i<rowCnt;i++) {
         for (int j=0;j<pelCnt;j++) {
            if (*msk++ == mskenbl) {
               *dst++ = *src++;
               *dst++ = *src++;
               *dst++ = *src++;
            }
            else {
               src += 3;
               dst += 3;
            }
         }
         msk += mskAdv;
         src += rowAdv;
         dst += rowAdv;
      }
   }

   curRect.left   = xdst;
   curRect.top    = ydst;
   curRect.right  = xdst + pelCnt - 1;
   curRect.bottom = ydst + rowCnt - 1;

   dataIsOnMainSurface = true;

}

