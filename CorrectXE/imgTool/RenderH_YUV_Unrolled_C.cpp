/*
	File:	RenderH_YUV_Unrolled_C.cpp
	By:	Kevin Manbeck
	Date:	April 15, 2005

	Improve the efficiency of the code by unrolling the innermost loops
*/

#include "AspectRatio.h"

void CAspectRatio::RenderH_YUV_1 (MTI_UINT8 *ucpD, int &iCol, int iStopDstLocal,
		int iJob)
/*
	Do HORIZONTAL rendering with 1 neighbors
*/
{
  int *ipSP, *ipP, iC, iPixel;
  int **ipLUT;
  MTI_UINT8 *ucpUnpackY, *ucpUnpackUV;
  int iSumY, iSumUV, *ipWei;
  int iNei;

  // local copies
  int iHalfPrecisionLoc = iHalfPrecision;
  int iClampMinLoc = iClampMin;
  int iClampMaxLoc = iClampMax;

  // check for illegal args
  if ((iCol & 0x1) || (iStopDstLocal & 0x1))
    return;

  ipSP = ipLUTSubPixel + iCol;
  ipP = ipLUTPixel + iCol;
  for (; iCol < iStopDstLocal; iCol+=2)
   {
    // Do the Y,U combination

    ipLUT = ipLUTWei[ipSP[0]];
    iPixel = ipP[0];
    ucpUnpackY = ucpUnpacked[iJob][0];   // Y values
    ucpUnpackUV = ucpUnpacked[iJob][1];  // U value

    ucpUnpackY += iPixel;
    ucpUnpackUV += iPixel;

#if !MTI_ASM_X86_INTEL
    iSumY = iHalfPrecision;
    iSumUV = iHalfPrecision;

    // run through the 1 neighbors
    ipWei = ipLUT[0];
    iSumY += ipWei[ucpUnpackY[0]];
    iSumUV += ipWei[ucpUnpackUV[0]];

    // clamp the result
    if (iSumY > iClampMax)
      iSumY = iClampMax;
    else if (iSumY < iClampMin)
      iSumY = iClampMin;

    if (iSumUV > iClampMax)
      iSumUV = iClampMax;
    else if (iSumUV < iClampMin)
      iSumUV = iClampMin;
#else
    _asm {
    mov    esi,iHalfPrecisionLoc // iSumY
    mov    edi,iHalfPrecisionLoc // iSumUV

    mov    eax,ipLUT

    mov    ebx,ucpUnpackY

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    esi,[ecx+edx*4]

    mov    ebx,ucpUnpackUV

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    edi,[ecx+edx*4]
    //
    // clamp the results
    //
    mov    ecx,iClampMaxLoc
    mov    edx,iClampMinLoc
    cmp    esi,ecx
    jle    $L0
    mov    esi,ecx
    jmp    SHORT $L1
$L0:cmp    esi,edx
    jge    $L1
    mov    esi,edx
$L1:mov    iSumY,esi
    cmp    edi,ecx
    jle    $L2
    mov    edi,ecx
    jmp    SHORT $L3
$L2:cmp    edi,edx
    jge    $L3
    mov    edi,edx
$L3:mov    iSumUV,edi
    }
#endif

    // round the result
    // was pre-rounded

    ucpD[0] = (iSumUV >> PRECISION);  // UV
    ucpD[1] = (iSumY >> PRECISION);  // Y

    // Do the Y,V combination

    ipLUT = ipLUTWei[ipSP[1]];
    iPixel = ipP[1];
    ucpUnpackY = ucpUnpacked[iJob][0];   // Y values
    ucpUnpackUV = ucpUnpacked[iJob][2];  // V value

    ucpUnpackY += iPixel;
    ucpUnpackUV += iPixel;

#if !MTI_ASM_X86_INTEL
    iSumY = iHalfPrecision;
    iSumUV = iHalfPrecision;

    // run through the 1 neighbors
    ipWei = ipLUT[0];
    iSumY += ipWei[ucpUnpackY[0]];
    iSumUV += ipWei[ucpUnpackUV[0]];

    // clamp the result
    if (iSumY > iClampMax)
      iSumY = iClampMax;
    else if (iSumY < iClampMin)
      iSumY = iClampMin;

    if (iSumUV > iClampMax)
      iSumUV = iClampMax;
    else if (iSumUV < iClampMin)
      iSumUV = iClampMin;
#else
    _asm {
    mov    esi,iHalfPrecisionLoc // iSumY
    mov    edi,iHalfPrecisionLoc // iSumUV

    mov    eax,ipLUT

    mov    ebx,ucpUnpackY

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    esi,[ecx+edx*4]

    mov    ebx,ucpUnpackUV

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    edi,[ecx+edx*4]
    //
    // clamp the results
    //
    mov    ecx,iClampMaxLoc
    mov    edx,iClampMinLoc
    cmp    esi,ecx
    jle    $L4
    mov    esi,ecx
    jmp    SHORT $L5
$L4:cmp    esi,edx
    jge    $L5
    mov    esi,edx
$L5:mov    iSumY,esi
    cmp    edi,ecx
    jle    $L6
    mov    edi,ecx
    jmp    SHORT $L7
$L6:cmp    edi,edx
    jge    $L7
    mov    edi,edx
$L7:mov    iSumUV,edi
    }
#endif

    // round the result
    // was pre-rounded

    ucpD[2] = (iSumUV >> PRECISION);  // UV
    ucpD[3] = (iSumY >> PRECISION);  // Y

    // bump pointers once
    ipSP += 2;
    ipP += 2;
    ucpD += 4;
   }
}

void CAspectRatio::RenderH_YUV_3 (MTI_UINT8 *ucpD, int &iCol, int iStopDstLocal,
		int iJob)
/*
	Do HORIZONTAL rendering with 3 neighbors
*/
{
  int *ipSP, *ipP, iC, iPixel;
  int **ipLUT;
  MTI_UINT8 *ucpUnpackY, *ucpUnpackUV;
  int iSumY, iSumUV, *ipWei;
  int iNei;

  // local copies
  int iHalfPrecisionLoc = iHalfPrecision;
  int iClampMinLoc = iClampMin;
  int iClampMaxLoc = iClampMax;

  // check for illegal args
  if ((iCol & 0x1) || (iStopDstLocal & 0x1))
    return;

  ipSP = ipLUTSubPixel + iCol;
  ipP = ipLUTPixel + iCol;
  for (; iCol < iStopDstLocal; iCol+=2)
   {
    // Do the Y,U combination

    ipLUT = ipLUTWei[ipSP[0]];
    iPixel = ipP[0];
    ucpUnpackY = ucpUnpacked[iJob][0];   // Y values
    ucpUnpackUV = ucpUnpacked[iJob][1];  // U value

    ucpUnpackY += iPixel;
    ucpUnpackUV += iPixel;

#if !MTI_ASM_X86_INTEL
    iSumY = iHalfPrecision;
    iSumUV = iHalfPrecision;

    // run through the 3 neighbors
    ipWei = ipLUT[0];
    iSumY += ipWei[ucpUnpackY[0]];
    iSumUV += ipWei[ucpUnpackUV[0]];

    ipWei = ipLUT[1];
    iSumY += ipWei[ucpUnpackY[1]];
    iSumUV += ipWei[ucpUnpackUV[1]];

    ipWei = ipLUT[2];
    iSumY += ipWei[ucpUnpackY[2]];
    iSumUV += ipWei[ucpUnpackUV[2]];

    // clamp the result
    if (iSumY > iClampMax)
      iSumY = iClampMax;
    else if (iSumY < iClampMin)
      iSumY = iClampMin;

    if (iSumUV > iClampMax)
      iSumUV = iClampMax;
    else if (iSumUV < iClampMin)
      iSumUV = iClampMin;
#else
    _asm {
    mov    esi,iHalfPrecisionLoc // iSumY
    mov    edi,iHalfPrecisionLoc // iSumUV

    mov    eax,ipLUT

    mov    ebx,ucpUnpackY

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x04]
    movzx  edx,BYTE PTR[ebx+0x01]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x08]
    movzx  edx,BYTE PTR[ebx+0x02]
    add    esi,[ecx+edx*4]

    mov    ebx,ucpUnpackUV

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x04]
    movzx  edx,BYTE PTR[ebx+0x01]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x08]
    movzx  edx,BYTE PTR[ebx+0x02]
    add    edi,[ecx+edx*4]
    //
    // clamp the results
    //
    mov    ecx,iClampMaxLoc
    mov    edx,iClampMinLoc
    cmp    esi,ecx
    jle    $L0
    mov    esi,ecx
    jmp    SHORT $L1
$L0:cmp    esi,edx
    jge    $L1
    mov    esi,edx
$L1:mov    iSumY,esi
    cmp    edi,ecx
    jle    $L2
    mov    edi,ecx
    jmp    SHORT $L3
$L2:cmp    edi,edx
    jge    $L3
    mov    edi,edx
$L3:mov    iSumUV,edi
    }
#endif

    // round the result
    // was pre-rounded

    ucpD[0] = (iSumUV >> PRECISION);  // UV
    ucpD[1] = (iSumY >> PRECISION);  // Y

    // Do the Y,V combination

    ipLUT = ipLUTWei[ipSP[1]];
    iPixel = ipP[1];
    ucpUnpackY = ucpUnpacked[iJob][0];   // Y values
    ucpUnpackUV = ucpUnpacked[iJob][2];  // V value

    ucpUnpackY += iPixel;
    ucpUnpackUV += iPixel;

#if !MTI_ASM_X86_INTEL
    iSumY = iHalfPrecision;
    iSumUV = iHalfPrecision;

    // run through the 3 neighbors
    ipWei = ipLUT[0];
    iSumY += ipWei[ucpUnpackY[0]];
    iSumUV += ipWei[ucpUnpackUV[0]];

    ipWei = ipLUT[1];
    iSumY += ipWei[ucpUnpackY[1]];
    iSumUV += ipWei[ucpUnpackUV[1]];

    ipWei = ipLUT[2];
    iSumY += ipWei[ucpUnpackY[2]];
    iSumUV += ipWei[ucpUnpackUV[2]];

    // clamp the result
    if (iSumY > iClampMax)
      iSumY = iClampMax;
    else if (iSumY < iClampMin)
      iSumY = iClampMin;

    if (iSumUV > iClampMax)
      iSumUV = iClampMax;
    else if (iSumUV < iClampMin)
      iSumUV = iClampMin;
#else
    _asm {
    mov    esi,iHalfPrecisionLoc // iSumY
    mov    edi,iHalfPrecisionLoc // iSumUV

    mov    eax,ipLUT

    mov    ebx,ucpUnpackY

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x04]
    movzx  edx,BYTE PTR[ebx+0x01]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x08]
    movzx  edx,BYTE PTR[ebx+0x02]
    add    esi,[ecx+edx*4]

    mov    ebx,ucpUnpackUV

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x04]
    movzx  edx,BYTE PTR[ebx+0x01]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x08]
    movzx  edx,BYTE PTR[ebx+0x02]
    add    edi,[ecx+edx*4]
    //
    // clamp the results
    //
    mov    ecx,iClampMaxLoc
    mov    edx,iClampMinLoc
    cmp    esi,ecx
    jle    $L4
    mov    esi,ecx
    jmp    SHORT $L5
$L4:cmp    esi,edx
    jge    $L5
    mov    esi,edx
$L5:mov    iSumY,esi
    cmp    edi,ecx
    jle    $L6
    mov    edi,ecx
    jmp    SHORT $L7
$L6:cmp    edi,edx
    jge    $L7
    mov    edi,edx
$L7:mov    iSumUV,edi
    }
#endif

    // round the result
    // was pre-rounded

    ucpD[2] = (iSumUV >> PRECISION);  // UV
    ucpD[3] = (iSumY >> PRECISION);  // Y

    // bump pointers once
    ipSP += 2;
    ipP += 2;
    ucpD += 4;
   }
}

void CAspectRatio::RenderH_YUV_5 (MTI_UINT8 *ucpD, int &iCol, int iStopDstLocal,
		int iJob)
/*
	Do HORIZONTAL rendering with 5 neighbors
*/
{
  int *ipSP, *ipP, iC, iPixel;
  int **ipLUT;
  MTI_UINT8 *ucpUnpackY, *ucpUnpackUV;
  int iSumY, iSumUV, *ipWei;
  int iNei;

  // local copies
  int iHalfPrecisionLoc = iHalfPrecision;
  int iClampMinLoc = iClampMin;
  int iClampMaxLoc = iClampMax;

  // check for illegal args
  if ((iCol & 0x1) || (iStopDstLocal & 0x1))
    return;

  ipSP = ipLUTSubPixel + iCol;
  ipP = ipLUTPixel + iCol;
  for (; iCol < iStopDstLocal; iCol+=2)
   {
    // Do the Y,U combination

    ipLUT = ipLUTWei[ipSP[0]];
    iPixel = ipP[0];
    ucpUnpackY = ucpUnpacked[iJob][0];   // Y values
    ucpUnpackUV = ucpUnpacked[iJob][1];  // U value

    ucpUnpackY += iPixel;
    ucpUnpackUV += iPixel;

#if !MTI_ASM_X86_INTEL
    iSumY = iHalfPrecision;
    iSumUV = iHalfPrecision;

    // run through the 5 neighbors
    ipWei = ipLUT[0];
    iSumY += ipWei[ucpUnpackY[0]];
    iSumUV += ipWei[ucpUnpackUV[0]];

    ipWei = ipLUT[1];
    iSumY += ipWei[ucpUnpackY[1]];
    iSumUV += ipWei[ucpUnpackUV[1]];

    ipWei = ipLUT[2];
    iSumY += ipWei[ucpUnpackY[2]];
    iSumUV += ipWei[ucpUnpackUV[2]];

    ipWei = ipLUT[3];
    iSumY += ipWei[ucpUnpackY[3]];
    iSumUV += ipWei[ucpUnpackUV[3]];

    ipWei = ipLUT[4];
    iSumY += ipWei[ucpUnpackY[4]];
    iSumUV += ipWei[ucpUnpackUV[4]];

    // clamp the result
    if (iSumY > iClampMax)
      iSumY = iClampMax;
    else if (iSumY < iClampMin)
      iSumY = iClampMin;

    if (iSumUV > iClampMax)
      iSumUV = iClampMax;
    else if (iSumUV < iClampMin)
      iSumUV = iClampMin;
#else
    _asm {
    mov    esi,iHalfPrecisionLoc // iSumY
    mov    edi,iHalfPrecisionLoc // iSumUV

    mov    eax,ipLUT

    mov    ebx,ucpUnpackY

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x04]
    movzx  edx,BYTE PTR[ebx+0x01]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x08]
    movzx  edx,BYTE PTR[ebx+0x02]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x0C]
    movzx  edx,BYTE PTR[ebx+0x03]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x10]
    movzx  edx,BYTE PTR[ebx+0x04]
    add    esi,[ecx+edx*4]

    mov    ebx,ucpUnpackUV

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x04]
    movzx  edx,BYTE PTR[ebx+0x01]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x08]
    movzx  edx,BYTE PTR[ebx+0x02]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x0C]
    movzx  edx,BYTE PTR[ebx+0x03]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x10]
    movzx  edx,BYTE PTR[ebx+0x04]
    add    edi,[ecx+edx*4]
    //
    // clamp the results
    //
    mov    ecx,iClampMaxLoc
    mov    edx,iClampMinLoc
    cmp    esi,ecx
    jle    $L0
    mov    esi,ecx
    jmp    SHORT $L1
$L0:cmp    esi,edx
    jge    $L1
    mov    esi,edx
$L1:mov    iSumY,esi
    cmp    edi,ecx
    jle    $L2
    mov    edi,ecx
    jmp    SHORT $L3
$L2:cmp    edi,edx
    jge    $L3
    mov    edi,edx
$L3:mov    iSumUV,edi
    }
#endif

    // round the result
    // was pre-rounded

    ucpD[0] = (iSumUV >> PRECISION);  // UV
    ucpD[1] = (iSumY >> PRECISION);  // Y

    // Do the Y,V combination

    ipLUT = ipLUTWei[ipSP[1]];
    iPixel = ipP[1];
    ucpUnpackY = ucpUnpacked[iJob][0];   // Y values
    ucpUnpackUV = ucpUnpacked[iJob][2];  // V value

    ucpUnpackY += iPixel;
    ucpUnpackUV += iPixel;

#if !MTI_ASM_X86_INTEL
    iSumY = iHalfPrecision;
    iSumUV = iHalfPrecision;

    // run through the 5 neighbors
    ipWei = ipLUT[0];
    iSumY += ipWei[ucpUnpackY[0]];
    iSumUV += ipWei[ucpUnpackUV[0]];

    ipWei = ipLUT[1];
    iSumY += ipWei[ucpUnpackY[1]];
    iSumUV += ipWei[ucpUnpackUV[1]];

    ipWei = ipLUT[2];
    iSumY += ipWei[ucpUnpackY[2]];
    iSumUV += ipWei[ucpUnpackUV[2]];

    ipWei = ipLUT[3];
    iSumY += ipWei[ucpUnpackY[3]];
    iSumUV += ipWei[ucpUnpackUV[3]];

    ipWei = ipLUT[4];
    iSumY += ipWei[ucpUnpackY[4]];
    iSumUV += ipWei[ucpUnpackUV[4]];

    // clamp the result
    if (iSumY > iClampMax)
      iSumY = iClampMax;
    else if (iSumY < iClampMin)
      iSumY = iClampMin;

    if (iSumUV > iClampMax)
      iSumUV = iClampMax;
    else if (iSumUV < iClampMin)
      iSumUV = iClampMin;
#else
    _asm {
    mov    esi,iHalfPrecisionLoc // iSumY
    mov    edi,iHalfPrecisionLoc // iSumUV

    mov    eax,ipLUT

    mov    ebx,ucpUnpackY

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x04]
    movzx  edx,BYTE PTR[ebx+0x01]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x08]
    movzx  edx,BYTE PTR[ebx+0x02]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x0C]
    movzx  edx,BYTE PTR[ebx+0x03]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x10]
    movzx  edx,BYTE PTR[ebx+0x04]
    add    esi,[ecx+edx*4]

    mov    ebx,ucpUnpackUV

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x04]
    movzx  edx,BYTE PTR[ebx+0x01]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x08]
    movzx  edx,BYTE PTR[ebx+0x02]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x0C]
    movzx  edx,BYTE PTR[ebx+0x03]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x10]
    movzx  edx,BYTE PTR[ebx+0x04]
    add    edi,[ecx+edx*4]
    //
    // clamp the results
    //
    mov    ecx,iClampMaxLoc
    mov    edx,iClampMinLoc
    cmp    esi,ecx
    jle    $L4
    mov    esi,ecx
    jmp    SHORT $L5
$L4:cmp    esi,edx
    jge    $L5
    mov    esi,edx
$L5:mov    iSumY,esi
    cmp    edi,ecx
    jle    $L6
    mov    edi,ecx
    jmp    SHORT $L7
$L6:cmp    edi,edx
    jge    $L7
    mov    edi,edx
$L7:mov    iSumUV,edi
    }
#endif

    // round the result
    // was pre-rounded

    ucpD[2] = (iSumUV >> PRECISION);  // UV
    ucpD[3] = (iSumY >> PRECISION);  // Y

    // bump pointers once
    ipSP += 2;
    ipP += 2;
    ucpD += 4;
   }
}

void CAspectRatio::RenderH_YUV_7 (MTI_UINT8 *ucpD, int &iCol, int iStopDstLocal,
		int iJob)
/*
	Do HORIZONTAL rendering with 7 neighbors
*/
{
  int *ipSP, *ipP, iC, iPixel;
  int **ipLUT;
  MTI_UINT8 *ucpUnpackY, *ucpUnpackUV;
  int iSumY, iSumUV, *ipWei;
  int iNei;

  // local copies
  int iHalfPrecisionLoc = iHalfPrecision;
  int iClampMinLoc = iClampMin;
  int iClampMaxLoc = iClampMax;

  // check for illegal args
  if ((iCol & 0x1) || (iStopDstLocal & 0x1))
    return;

  ipSP = ipLUTSubPixel + iCol;
  ipP = ipLUTPixel + iCol;
  for (; iCol < iStopDstLocal; iCol+=2)
   {
    // Do the Y,U combination

    ipLUT = ipLUTWei[ipSP[0]];
    iPixel = ipP[0];
    ucpUnpackY = ucpUnpacked[iJob][0];   // Y values
    ucpUnpackUV = ucpUnpacked[iJob][1];  // U value

    ucpUnpackY += iPixel;
    ucpUnpackUV += iPixel;

#if !MTI_ASM_X86_INTEL
    iSumY = iHalfPrecision;
    iSumUV = iHalfPrecision;

    // run through the 7 neighbors
    ipWei = ipLUT[0];
    iSumY += ipWei[ucpUnpackY[0]];
    iSumUV += ipWei[ucpUnpackUV[0]];

    ipWei = ipLUT[1];
    iSumY += ipWei[ucpUnpackY[1]];
    iSumUV += ipWei[ucpUnpackUV[1]];

    ipWei = ipLUT[2];
    iSumY += ipWei[ucpUnpackY[2]];
    iSumUV += ipWei[ucpUnpackUV[2]];

    ipWei = ipLUT[3];
    iSumY += ipWei[ucpUnpackY[3]];
    iSumUV += ipWei[ucpUnpackUV[3]];

    ipWei = ipLUT[4];
    iSumY += ipWei[ucpUnpackY[4]];
    iSumUV += ipWei[ucpUnpackUV[4]];

    ipWei = ipLUT[5];
    iSumY += ipWei[ucpUnpackY[5]];
    iSumUV += ipWei[ucpUnpackUV[5]];

    ipWei = ipLUT[6];
    iSumY += ipWei[ucpUnpackY[6]];
    iSumUV += ipWei[ucpUnpackUV[6]];

    // clamp the result
    if (iSumY > iClampMax)
      iSumY = iClampMax;
    else if (iSumY < iClampMin)
      iSumY = iClampMin;

    if (iSumUV > iClampMax)
      iSumUV = iClampMax;
    else if (iSumUV < iClampMin)
      iSumUV = iClampMin;
#else
    _asm {
    mov    esi,iHalfPrecisionLoc // iSumY
    mov    edi,iHalfPrecisionLoc // iSumUV

    mov    eax,ipLUT

    mov    ebx,ucpUnpackY

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x04]
    movzx  edx,BYTE PTR[ebx+0x01]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x08]
    movzx  edx,BYTE PTR[ebx+0x02]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x0C]
    movzx  edx,BYTE PTR[ebx+0x03]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x10]
    movzx  edx,BYTE PTR[ebx+0x04]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x14]
    movzx  edx,BYTE PTR[ebx+0x05]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x18]
    movzx  edx,BYTE PTR[ebx+0x06]
    add    esi,[ecx+edx*4]

    mov    ebx,ucpUnpackUV

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x04]
    movzx  edx,BYTE PTR[ebx+0x01]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x08]
    movzx  edx,BYTE PTR[ebx+0x02]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x0C]
    movzx  edx,BYTE PTR[ebx+0x03]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x10]
    movzx  edx,BYTE PTR[ebx+0x04]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x14]
    movzx  edx,BYTE PTR[ebx+0x05]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x18]
    movzx  edx,BYTE PTR[ebx+0x06]
    add    edi,[ecx+edx*4]
    //
    // clamp the results
    //
    mov    ecx,iClampMaxLoc
    mov    edx,iClampMinLoc
    cmp    esi,ecx
    jle    $L0
    mov    esi,ecx
    jmp    SHORT $L1
$L0:cmp    esi,edx
    jge    $L1
    mov    esi,edx
$L1:mov    iSumY,esi
    cmp    edi,ecx
    jle    $L2
    mov    edi,ecx
    jmp    SHORT $L3
$L2:cmp    edi,edx
    jge    $L3
    mov    edi,edx
$L3:mov    iSumUV,edi
    }
#endif

    // round the result
    // was pre-rounded

    ucpD[0] = (iSumUV >> PRECISION);  // UV
    ucpD[1] = (iSumY >> PRECISION);  // Y

    // Do the Y,V combination

    ipLUT = ipLUTWei[ipSP[1]];
    iPixel = ipP[1];
    ucpUnpackY = ucpUnpacked[iJob][0];   // Y values
    ucpUnpackUV = ucpUnpacked[iJob][2];  // V value

    ucpUnpackY += iPixel;
    ucpUnpackUV += iPixel;

#if !MTI_ASM_X86_INTEL
    iSumY = iHalfPrecision;
    iSumUV = iHalfPrecision;

    // run through the 7 neighbors
    ipWei = ipLUT[0];
    iSumY += ipWei[ucpUnpackY[0]];
    iSumUV += ipWei[ucpUnpackUV[0]];

    ipWei = ipLUT[1];
    iSumY += ipWei[ucpUnpackY[1]];
    iSumUV += ipWei[ucpUnpackUV[1]];

    ipWei = ipLUT[2];
    iSumY += ipWei[ucpUnpackY[2]];
    iSumUV += ipWei[ucpUnpackUV[2]];

    ipWei = ipLUT[3];
    iSumY += ipWei[ucpUnpackY[3]];
    iSumUV += ipWei[ucpUnpackUV[3]];

    ipWei = ipLUT[4];
    iSumY += ipWei[ucpUnpackY[4]];
    iSumUV += ipWei[ucpUnpackUV[4]];

    ipWei = ipLUT[5];
    iSumY += ipWei[ucpUnpackY[5]];
    iSumUV += ipWei[ucpUnpackUV[5]];

    ipWei = ipLUT[6];
    iSumY += ipWei[ucpUnpackY[6]];
    iSumUV += ipWei[ucpUnpackUV[6]];

    // clamp the result
    if (iSumY > iClampMax)
      iSumY = iClampMax;
    else if (iSumY < iClampMin)
      iSumY = iClampMin;

    if (iSumUV > iClampMax)
      iSumUV = iClampMax;
    else if (iSumUV < iClampMin)
      iSumUV = iClampMin;
#else
    _asm {
    mov    esi,iHalfPrecisionLoc // iSumY
    mov    edi,iHalfPrecisionLoc // iSumUV

    mov    eax,ipLUT

    mov    ebx,ucpUnpackY

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x04]
    movzx  edx,BYTE PTR[ebx+0x01]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x08]
    movzx  edx,BYTE PTR[ebx+0x02]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x0C]
    movzx  edx,BYTE PTR[ebx+0x03]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x10]
    movzx  edx,BYTE PTR[ebx+0x04]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x14]
    movzx  edx,BYTE PTR[ebx+0x05]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x18]
    movzx  edx,BYTE PTR[ebx+0x06]
    add    esi,[ecx+edx*4]

    mov    ebx,ucpUnpackUV

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x04]
    movzx  edx,BYTE PTR[ebx+0x01]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x08]
    movzx  edx,BYTE PTR[ebx+0x02]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x0C]
    movzx  edx,BYTE PTR[ebx+0x03]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x10]
    movzx  edx,BYTE PTR[ebx+0x04]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x14]
    movzx  edx,BYTE PTR[ebx+0x05]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x18]
    movzx  edx,BYTE PTR[ebx+0x06]
    add    edi,[ecx+edx*4]
    //
    // clamp the results
    //
    mov    ecx,iClampMaxLoc
    mov    edx,iClampMinLoc
    cmp    esi,ecx
    jle    $L4
    mov    esi,ecx
    jmp    SHORT $L5
$L4:cmp    esi,edx
    jge    $L5
    mov    esi,edx
$L5:mov    iSumY,esi
    cmp    edi,ecx
    jle    $L6
    mov    edi,ecx
    jmp    SHORT $L7
$L6:cmp    edi,edx
    jge    $L7
    mov    edi,edx
$L7:mov    iSumUV,edi
    }
#endif

    // round the result
    // was pre-rounded

    ucpD[2] = (iSumUV >> PRECISION);  // UV
    ucpD[3] = (iSumY >> PRECISION);  // Y

    // bump pointers once
    ipSP += 2;
    ipP += 2;
    ucpD += 4;
   }
}

void CAspectRatio::RenderH_YUV_9 (MTI_UINT8 *ucpD, int &iCol, int iStopDstLocal,
		int iJob)
/*
	Do HORIZONTAL rendering with 9 neighbors
*/
{
  int *ipSP, *ipP, iC, iPixel;
  int **ipLUT;
  MTI_UINT8 *ucpUnpackY, *ucpUnpackUV;
  int iSumY, iSumUV, *ipWei;
  int iNei;

  // local copies
  int iHalfPrecisionLoc = iHalfPrecision;
  int iClampMinLoc = iClampMin;
  int iClampMaxLoc = iClampMax;

  // check for illegal args
  if ((iCol & 0x1) || (iStopDstLocal & 0x1))
    return;

  ipSP = ipLUTSubPixel + iCol;
  ipP = ipLUTPixel + iCol;
  for (; iCol < iStopDstLocal; iCol+=2)
   {
    // Do the Y,U combination

    ipLUT = ipLUTWei[ipSP[0]];
    iPixel = ipP[0];
    ucpUnpackY = ucpUnpacked[iJob][0];   // Y values
    ucpUnpackUV = ucpUnpacked[iJob][1];  // U value

    ucpUnpackY += iPixel;
    ucpUnpackUV += iPixel;

#if !MTI_ASM_X86_INTEL
    iSumY = iHalfPrecision;
    iSumUV = iHalfPrecision;

    // run through the 9 neighbors
    ipWei = ipLUT[0];
    iSumY += ipWei[ucpUnpackY[0]];
    iSumUV += ipWei[ucpUnpackUV[0]];

    ipWei = ipLUT[1];
    iSumY += ipWei[ucpUnpackY[1]];
    iSumUV += ipWei[ucpUnpackUV[1]];

    ipWei = ipLUT[2];
    iSumY += ipWei[ucpUnpackY[2]];
    iSumUV += ipWei[ucpUnpackUV[2]];

    ipWei = ipLUT[3];
    iSumY += ipWei[ucpUnpackY[3]];
    iSumUV += ipWei[ucpUnpackUV[3]];

    ipWei = ipLUT[4];
    iSumY += ipWei[ucpUnpackY[4]];
    iSumUV += ipWei[ucpUnpackUV[4]];

    ipWei = ipLUT[5];
    iSumY += ipWei[ucpUnpackY[5]];
    iSumUV += ipWei[ucpUnpackUV[5]];

    ipWei = ipLUT[6];
    iSumY += ipWei[ucpUnpackY[6]];
    iSumUV += ipWei[ucpUnpackUV[6]];

    ipWei = ipLUT[7];
    iSumY += ipWei[ucpUnpackY[7]];
    iSumUV += ipWei[ucpUnpackUV[7]];

    ipWei = ipLUT[8];
    iSumY += ipWei[ucpUnpackY[8]];
    iSumUV += ipWei[ucpUnpackUV[8]];

    // clamp the result
    if (iSumY > iClampMax)
      iSumY = iClampMax;
    else if (iSumY < iClampMin)
      iSumY = iClampMin;

    if (iSumUV > iClampMax)
      iSumUV = iClampMax;
    else if (iSumUV < iClampMin)
      iSumUV = iClampMin;
#else
    _asm {
    mov    esi,iHalfPrecisionLoc // iSumY
    mov    edi,iHalfPrecisionLoc // iSumUV

    mov    eax,ipLUT

    mov    ebx,ucpUnpackY

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x04]
    movzx  edx,BYTE PTR[ebx+0x01]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x08]
    movzx  edx,BYTE PTR[ebx+0x02]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x0C]
    movzx  edx,BYTE PTR[ebx+0x03]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x10]
    movzx  edx,BYTE PTR[ebx+0x04]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x14]
    movzx  edx,BYTE PTR[ebx+0x05]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x18]
    movzx  edx,BYTE PTR[ebx+0x06]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x1C]
    movzx  edx,BYTE PTR[ebx+0x07]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x20]
    movzx  edx,BYTE PTR[ebx+0x08]
    add    esi,[ecx+edx*4]

    mov    ebx,ucpUnpackUV

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x04]
    movzx  edx,BYTE PTR[ebx+0x01]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x08]
    movzx  edx,BYTE PTR[ebx+0x02]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x0C]
    movzx  edx,BYTE PTR[ebx+0x03]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x10]
    movzx  edx,BYTE PTR[ebx+0x04]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x14]
    movzx  edx,BYTE PTR[ebx+0x05]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x18]
    movzx  edx,BYTE PTR[ebx+0x06]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x1C]
    movzx  edx,BYTE PTR[ebx+0x07]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x20]
    movzx  edx,BYTE PTR[ebx+0x08]
    add    edi,[ecx+edx*4]
    //
    // clamp the results
    //
    mov    ecx,iClampMaxLoc
    mov    edx,iClampMinLoc
    cmp    esi,ecx
    jle    $L0
    mov    esi,ecx
    jmp    SHORT $L1
$L0:cmp    esi,edx
    jge    $L1
    mov    esi,edx
$L1:mov    iSumY,esi
    cmp    edi,ecx
    jle    $L2
    mov    edi,ecx
    jmp    SHORT $L3
$L2:cmp    edi,edx
    jge    $L3
    mov    edi,edx
$L3:mov    iSumUV,edi
    }
#endif
    // round the result
    // was pre-rounded

    ucpD[0] = (iSumUV >> PRECISION);  // UV
    ucpD[1] = (iSumY >> PRECISION);  // Y

    // Do the Y,V combination

    ipLUT = ipLUTWei[ipSP[1]];
    iPixel = ipP[1];
    ucpUnpackY = ucpUnpacked[iJob][0];   // Y values
    ucpUnpackUV = ucpUnpacked[iJob][2];  // V value

    ucpUnpackY += iPixel;
    ucpUnpackUV += iPixel;

#if !MTI_ASM_X86_INTEL
    iSumY = iHalfPrecision;
    iSumUV = iHalfPrecision;

    // run through the 9 neighbors
    ipWei = ipLUT[0];
    iSumY += ipWei[ucpUnpackY[0]];
    iSumUV += ipWei[ucpUnpackUV[0]];

    ipWei = ipLUT[1];
    iSumY += ipWei[ucpUnpackY[1]];
    iSumUV += ipWei[ucpUnpackUV[1]];

    ipWei = ipLUT[2];
    iSumY += ipWei[ucpUnpackY[2]];
    iSumUV += ipWei[ucpUnpackUV[2]];

    ipWei = ipLUT[3];
    iSumY += ipWei[ucpUnpackY[3]];
    iSumUV += ipWei[ucpUnpackUV[3]];

    ipWei = ipLUT[4];
    iSumY += ipWei[ucpUnpackY[4]];
    iSumUV += ipWei[ucpUnpackUV[4]];

    ipWei = ipLUT[5];
    iSumY += ipWei[ucpUnpackY[5]];
    iSumUV += ipWei[ucpUnpackUV[5]];

    ipWei = ipLUT[6];
    iSumY += ipWei[ucpUnpackY[6]];
    iSumUV += ipWei[ucpUnpackUV[6]];

    ipWei = ipLUT[7];
    iSumY += ipWei[ucpUnpackY[7]];
    iSumUV += ipWei[ucpUnpackUV[7]];

    ipWei = ipLUT[8];
    iSumY += ipWei[ucpUnpackY[8]];
    iSumUV += ipWei[ucpUnpackUV[8]];

    // clamp the result
    if (iSumY > iClampMax)
      iSumY = iClampMax;
    else if (iSumY < iClampMin)
      iSumY = iClampMin;

    if (iSumUV > iClampMax)
      iSumUV = iClampMax;
    else if (iSumUV < iClampMin)
      iSumUV = iClampMin;
#else
    _asm {
    mov    esi,iHalfPrecisionLoc // iSumY
    mov    edi,iHalfPrecisionLoc // iSumUV

    mov    eax,ipLUT

    mov    ebx,ucpUnpackY

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x04]
    movzx  edx,BYTE PTR[ebx+0x01]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x08]
    movzx  edx,BYTE PTR[ebx+0x02]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x0C]
    movzx  edx,BYTE PTR[ebx+0x03]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x10]
    movzx  edx,BYTE PTR[ebx+0x04]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x14]
    movzx  edx,BYTE PTR[ebx+0x05]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x18]
    movzx  edx,BYTE PTR[ebx+0x06]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x1C]
    movzx  edx,BYTE PTR[ebx+0x07]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x20]
    movzx  edx,BYTE PTR[ebx+0x08]
    add    esi,[ecx+edx*4]

    mov    ebx,ucpUnpackUV

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x04]
    movzx  edx,BYTE PTR[ebx+0x01]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x08]
    movzx  edx,BYTE PTR[ebx+0x02]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x0C]
    movzx  edx,BYTE PTR[ebx+0x03]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x10]
    movzx  edx,BYTE PTR[ebx+0x04]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x14]
    movzx  edx,BYTE PTR[ebx+0x05]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x18]
    movzx  edx,BYTE PTR[ebx+0x06]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x1C]
    movzx  edx,BYTE PTR[ebx+0x07]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x20]
    movzx  edx,BYTE PTR[ebx+0x08]
    add    edi,[ecx+edx*4]
    //
    // clamp the results
    //
    mov    ecx,iClampMaxLoc
    mov    edx,iClampMinLoc
    cmp    esi,ecx
    jle    $L4
    mov    esi,ecx
    jmp    SHORT $L5
$L4:cmp    esi,edx
    jge    $L5
    mov    esi,edx
$L5:mov    iSumY,esi
    cmp    edi,ecx
    jle    $L6
    mov    edi,ecx
    jmp    SHORT $L7
$L6:cmp    edi,edx
    jge    $L7
    mov    edi,edx
$L7:mov    iSumUV,edi
    }
#endif

    // round the result
    // was pre-rounded

    ucpD[2] = (iSumUV >> PRECISION);  // UV
    ucpD[3] = (iSumY >> PRECISION);  // Y

    // bump pointers once
    ipSP += 2;
    ipP += 2;
    ucpD += 4;
   }
}

void CAspectRatio::RenderH_YUV_11 (MTI_UINT8 *ucpD, int &iCol, int iStopDstLocal,
		int iJob)
/*
	Do HORIZONTAL rendering with 11 neighbors
*/
{
  int *ipSP, *ipP, iC, iPixel;
  int **ipLUT;
  MTI_UINT8 *ucpUnpackY, *ucpUnpackUV;
  int iSumY, iSumUV, *ipWei;
  int iNei;

  // local copies
  int iHalfPrecisionLoc = iHalfPrecision;
  int iClampMinLoc = iClampMin;
  int iClampMaxLoc = iClampMax;

  // check for illegal args
  if ((iCol & 0x1) || (iStopDstLocal & 0x1))
    return;

  ipSP = ipLUTSubPixel + iCol;
  ipP = ipLUTPixel + iCol;
  for (; iCol < iStopDstLocal; iCol+=2)
   {
    // Do the Y,U combination

    ipLUT = ipLUTWei[ipSP[0]];
    iPixel = ipP[0];
    ucpUnpackY = ucpUnpacked[iJob][0];   // Y values
    ucpUnpackUV = ucpUnpacked[iJob][1];  // U value

    ucpUnpackY += iPixel;
    ucpUnpackUV += iPixel;

#if !MTI_ASM_X86_INTEL
    iSumY = iHalfPrecision;
    iSumUV = iHalfPrecision;

    // run through the 11 neighbors
    ipWei = ipLUT[0];
    iSumY += ipWei[ucpUnpackY[0]];
    iSumUV += ipWei[ucpUnpackUV[0]];

    ipWei = ipLUT[1];
    iSumY += ipWei[ucpUnpackY[1]];
    iSumUV += ipWei[ucpUnpackUV[1]];

    ipWei = ipLUT[2];
    iSumY += ipWei[ucpUnpackY[2]];
    iSumUV += ipWei[ucpUnpackUV[2]];

    ipWei = ipLUT[3];
    iSumY += ipWei[ucpUnpackY[3]];
    iSumUV += ipWei[ucpUnpackUV[3]];

    ipWei = ipLUT[4];
    iSumY += ipWei[ucpUnpackY[4]];
    iSumUV += ipWei[ucpUnpackUV[4]];

    ipWei = ipLUT[5];
    iSumY += ipWei[ucpUnpackY[5]];
    iSumUV += ipWei[ucpUnpackUV[5]];

    ipWei = ipLUT[6];
    iSumY += ipWei[ucpUnpackY[6]];
    iSumUV += ipWei[ucpUnpackUV[6]];

    ipWei = ipLUT[7];
    iSumY += ipWei[ucpUnpackY[7]];
    iSumUV += ipWei[ucpUnpackUV[7]];

    ipWei = ipLUT[8];
    iSumY += ipWei[ucpUnpackY[8]];
    iSumUV += ipWei[ucpUnpackUV[8]];

    ipWei = ipLUT[9];
    iSumY += ipWei[ucpUnpackY[9]];
    iSumUV += ipWei[ucpUnpackUV[9]];

    ipWei = ipLUT[10];
    iSumY += ipWei[ucpUnpackY[10]];
    iSumUV += ipWei[ucpUnpackUV[10]];

    // clamp the result
    if (iSumY > iClampMax)
      iSumY = iClampMax;
    else if (iSumY < iClampMin)
      iSumY = iClampMin;

    if (iSumUV > iClampMax)
      iSumUV = iClampMax;
    else if (iSumUV < iClampMin)
      iSumUV = iClampMin;
#else
    _asm {
    mov    esi,iHalfPrecisionLoc // iSumY
    mov    edi,iHalfPrecisionLoc // iSumUV

    mov    eax,ipLUT

    mov    ebx,ucpUnpackY

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x04]
    movzx  edx,BYTE PTR[ebx+0x01]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x08]
    movzx  edx,BYTE PTR[ebx+0x02]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x0C]
    movzx  edx,BYTE PTR[ebx+0x03]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x10]
    movzx  edx,BYTE PTR[ebx+0x04]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x14]
    movzx  edx,BYTE PTR[ebx+0x05]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x18]
    movzx  edx,BYTE PTR[ebx+0x06]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x1C]
    movzx  edx,BYTE PTR[ebx+0x07]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x20]
    movzx  edx,BYTE PTR[ebx+0x08]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x24]
    movzx  edx,BYTE PTR[ebx+0x09]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x28]
    movzx  edx,BYTE PTR[ebx+0x0A]
    add    esi,[ecx+edx*4]

    mov    ebx,ucpUnpackUV

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x04]
    movzx  edx,BYTE PTR[ebx+0x01]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x08]
    movzx  edx,BYTE PTR[ebx+0x02]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x0C]
    movzx  edx,BYTE PTR[ebx+0x03]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x10]
    movzx  edx,BYTE PTR[ebx+0x04]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x14]
    movzx  edx,BYTE PTR[ebx+0x05]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x18]
    movzx  edx,BYTE PTR[ebx+0x06]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x1C]
    movzx  edx,BYTE PTR[ebx+0x07]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x20]
    movzx  edx,BYTE PTR[ebx+0x08]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x24]
    movzx  edx,BYTE PTR[ebx+0x09]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x28]
    movzx  edx,BYTE PTR[ebx+0x0A]
    add    edi,[ecx+edx*4]
    //
    // clamp the results
    //
    mov    ecx,iClampMaxLoc
    mov    edx,iClampMinLoc
    cmp    esi,ecx
    jle    $L0
    mov    esi,ecx
    jmp    SHORT $L1
$L0:cmp    esi,edx
    jge    $L1
    mov    esi,edx
$L1:mov    iSumY,esi
    cmp    edi,ecx
    jle    $L2
    mov    edi,ecx
    jmp    SHORT $L3
$L2:cmp    edi,edx
    jge    $L3
    mov    edi,edx
$L3:mov    iSumUV,edi
    }
#endif

    // round the result
    // was pre-rounded

    ucpD[0] = (iSumUV >> PRECISION);  // UV
    ucpD[1] = (iSumY >> PRECISION);  // Y

    // Do the Y,V combination

    ipLUT = ipLUTWei[ipSP[1]];
    iPixel = ipP[1];
    ucpUnpackY = ucpUnpacked[iJob][0];   // Y values
    ucpUnpackUV = ucpUnpacked[iJob][2];  // V value

    ucpUnpackY += iPixel;
    ucpUnpackUV += iPixel;

#if !MTI_ASM_X86_INTEL
    iSumY = iHalfPrecision;
    iSumUV = iHalfPrecision;

    // run through the 11 neighbors
    ipWei = ipLUT[0];
    iSumY += ipWei[ucpUnpackY[0]];
    iSumUV += ipWei[ucpUnpackUV[0]];

    ipWei = ipLUT[1];
    iSumY += ipWei[ucpUnpackY[1]];
    iSumUV += ipWei[ucpUnpackUV[1]];

    ipWei = ipLUT[2];
    iSumY += ipWei[ucpUnpackY[2]];
    iSumUV += ipWei[ucpUnpackUV[2]];

    ipWei = ipLUT[3];
    iSumY += ipWei[ucpUnpackY[3]];
    iSumUV += ipWei[ucpUnpackUV[3]];

    ipWei = ipLUT[4];
    iSumY += ipWei[ucpUnpackY[4]];
    iSumUV += ipWei[ucpUnpackUV[4]];

    ipWei = ipLUT[5];
    iSumY += ipWei[ucpUnpackY[5]];
    iSumUV += ipWei[ucpUnpackUV[5]];

    ipWei = ipLUT[6];
    iSumY += ipWei[ucpUnpackY[6]];
    iSumUV += ipWei[ucpUnpackUV[6]];

    ipWei = ipLUT[7];
    iSumY += ipWei[ucpUnpackY[7]];
    iSumUV += ipWei[ucpUnpackUV[7]];

    ipWei = ipLUT[8];
    iSumY += ipWei[ucpUnpackY[8]];
    iSumUV += ipWei[ucpUnpackUV[8]];

    ipWei = ipLUT[9];
    iSumY += ipWei[ucpUnpackY[9]];
    iSumUV += ipWei[ucpUnpackUV[9]];

    ipWei = ipLUT[10];
    iSumY += ipWei[ucpUnpackY[10]];
    iSumUV += ipWei[ucpUnpackUV[10]];

    // clamp the result
    if (iSumY > iClampMax)
      iSumY = iClampMax;
    else if (iSumY < iClampMin)
      iSumY = iClampMin;

    if (iSumUV > iClampMax)
      iSumUV = iClampMax;
    else if (iSumUV < iClampMin)
      iSumUV = iClampMin;
#else
    _asm {
    mov    esi,iHalfPrecisionLoc // iSumY
    mov    edi,iHalfPrecisionLoc // iSumUV

    mov    eax,ipLUT

    mov    ebx,ucpUnpackY

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x04]
    movzx  edx,BYTE PTR[ebx+0x01]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x08]
    movzx  edx,BYTE PTR[ebx+0x02]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x0C]
    movzx  edx,BYTE PTR[ebx+0x03]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x10]
    movzx  edx,BYTE PTR[ebx+0x04]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x14]
    movzx  edx,BYTE PTR[ebx+0x05]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x18]
    movzx  edx,BYTE PTR[ebx+0x06]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x1C]
    movzx  edx,BYTE PTR[ebx+0x07]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x20]
    movzx  edx,BYTE PTR[ebx+0x08]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x24]
    movzx  edx,BYTE PTR[ebx+0x09]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x28]
    movzx  edx,BYTE PTR[ebx+0x0A]
    add    esi,[ecx+edx*4]

    mov    ebx,ucpUnpackUV

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x04]
    movzx  edx,BYTE PTR[ebx+0x01]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x08]
    movzx  edx,BYTE PTR[ebx+0x02]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x0C]
    movzx  edx,BYTE PTR[ebx+0x03]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x10]
    movzx  edx,BYTE PTR[ebx+0x04]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x14]
    movzx  edx,BYTE PTR[ebx+0x05]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x18]
    movzx  edx,BYTE PTR[ebx+0x06]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x1C]
    movzx  edx,BYTE PTR[ebx+0x07]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x20]
    movzx  edx,BYTE PTR[ebx+0x08]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x24]
    movzx  edx,BYTE PTR[ebx+0x09]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x28]
    movzx  edx,BYTE PTR[ebx+0x0A]
    add    edi,[ecx+edx*4]
    //
    // clamp the results
    //
    mov    ecx,iClampMaxLoc
    mov    edx,iClampMinLoc
    cmp    esi,ecx
    jle    $L4
    mov    esi,ecx
    jmp    SHORT $L5
$L4:cmp    esi,edx
    jge    $L5
    mov    esi,edx
$L5:mov    iSumY,esi
    cmp    edi,ecx
    jle    $L6
    mov    edi,ecx
    jmp    SHORT $L7
$L6:cmp    edi,edx
    jge    $L7
    mov    edi,edx
$L7:mov    iSumUV,edi
    }
#endif

    // round the result
    // was pre-rounded

    ucpD[2] = (iSumUV >> PRECISION);  // UV
    ucpD[3] = (iSumY >> PRECISION);  // Y

    // bump pointers once
    ipSP += 2;
    ipP += 2;
    ucpD += 4;
   }
}

void CAspectRatio::RenderH_YUV_13 (MTI_UINT8 *ucpD, int &iCol, int iStopDstLocal,
		int iJob)
/*
	Do HORIZONTAL rendering with 13 neighbors
*/
{
  int *ipSP, *ipP, iC, iPixel;
  int **ipLUT;
  MTI_UINT8 *ucpUnpackY, *ucpUnpackUV;
  int iSumY, iSumUV, *ipWei;
  int iNei;

  // local copies
  int iHalfPrecisionLoc = iHalfPrecision;
  int iClampMinLoc = iClampMin;
  int iClampMaxLoc = iClampMax;

  // check for illegal args
  if ((iCol & 0x1) || (iStopDstLocal & 0x1))
    return;

  ipSP = ipLUTSubPixel + iCol;
  ipP = ipLUTPixel + iCol;
  for (; iCol < iStopDstLocal; iCol+=2)
   {
    // Do the Y,U combination

    ipLUT = ipLUTWei[ipSP[0]];
    iPixel = ipP[0];
    ucpUnpackY = ucpUnpacked[iJob][0];   // Y values
    ucpUnpackUV = ucpUnpacked[iJob][1];  // U value

    ucpUnpackY += iPixel;
    ucpUnpackUV += iPixel;

#if !MTI_ASM_X86_INTEL
    iSumY = iHalfPrecision;
    iSumUV = iHalfPrecision;

    // run through the 13 neighbors
    ipWei = ipLUT[0];
    iSumY += ipWei[ucpUnpackY[0]];
    iSumUV += ipWei[ucpUnpackUV[0]];

    ipWei = ipLUT[1];
    iSumY += ipWei[ucpUnpackY[1]];
    iSumUV += ipWei[ucpUnpackUV[1]];

    ipWei = ipLUT[2];
    iSumY += ipWei[ucpUnpackY[2]];
    iSumUV += ipWei[ucpUnpackUV[2]];

    ipWei = ipLUT[3];
    iSumY += ipWei[ucpUnpackY[3]];
    iSumUV += ipWei[ucpUnpackUV[3]];

    ipWei = ipLUT[4];
    iSumY += ipWei[ucpUnpackY[4]];
    iSumUV += ipWei[ucpUnpackUV[4]];

    ipWei = ipLUT[5];
    iSumY += ipWei[ucpUnpackY[5]];
    iSumUV += ipWei[ucpUnpackUV[5]];

    ipWei = ipLUT[6];
    iSumY += ipWei[ucpUnpackY[6]];
    iSumUV += ipWei[ucpUnpackUV[6]];

    ipWei = ipLUT[7];
    iSumY += ipWei[ucpUnpackY[7]];
    iSumUV += ipWei[ucpUnpackUV[7]];

    ipWei = ipLUT[8];
    iSumY += ipWei[ucpUnpackY[8]];
    iSumUV += ipWei[ucpUnpackUV[8]];

    ipWei = ipLUT[9];
    iSumY += ipWei[ucpUnpackY[9]];
    iSumUV += ipWei[ucpUnpackUV[9]];

    ipWei = ipLUT[10];
    iSumY += ipWei[ucpUnpackY[10]];
    iSumUV += ipWei[ucpUnpackUV[10]];

    ipWei = ipLUT[11];
    iSumY += ipWei[ucpUnpackY[11]];
    iSumUV += ipWei[ucpUnpackUV[11]];

    ipWei = ipLUT[12];
    iSumY += ipWei[ucpUnpackY[12]];
    iSumUV += ipWei[ucpUnpackUV[12]];

    // clamp the result
    if (iSumY > iClampMax)
      iSumY = iClampMax;
    else if (iSumY < iClampMin)
      iSumY = iClampMin;

    if (iSumUV > iClampMax)
      iSumUV = iClampMax;
    else if (iSumUV < iClampMin)
      iSumUV = iClampMin;
#else
    _asm {
    mov    esi,iHalfPrecisionLoc // iSumY
    mov    edi,iHalfPrecisionLoc // iSumUV

    mov    eax,ipLUT

    mov    ebx,ucpUnpackY

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x04]
    movzx  edx,BYTE PTR[ebx+0x01]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x08]
    movzx  edx,BYTE PTR[ebx+0x02]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x0C]
    movzx  edx,BYTE PTR[ebx+0x03]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x10]
    movzx  edx,BYTE PTR[ebx+0x04]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x14]
    movzx  edx,BYTE PTR[ebx+0x05]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x18]
    movzx  edx,BYTE PTR[ebx+0x06]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x1C]
    movzx  edx,BYTE PTR[ebx+0x07]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x20]
    movzx  edx,BYTE PTR[ebx+0x08]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x24]
    movzx  edx,BYTE PTR[ebx+0x09]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x28]
    movzx  edx,BYTE PTR[ebx+0x0A]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x2C]
    movzx  edx,BYTE PTR[ebx+0x0B]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x30]
    movzx  edx,BYTE PTR[ebx+0x0C]
    add    esi,[ecx+edx*4]

    mov    ebx,ucpUnpackUV

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x04]
    movzx  edx,BYTE PTR[ebx+0x01]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x08]
    movzx  edx,BYTE PTR[ebx+0x02]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x0C]
    movzx  edx,BYTE PTR[ebx+0x03]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x10]
    movzx  edx,BYTE PTR[ebx+0x04]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x14]
    movzx  edx,BYTE PTR[ebx+0x05]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x18]
    movzx  edx,BYTE PTR[ebx+0x06]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x1C]
    movzx  edx,BYTE PTR[ebx+0x07]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x20]
    movzx  edx,BYTE PTR[ebx+0x08]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x24]
    movzx  edx,BYTE PTR[ebx+0x09]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x28]
    movzx  edx,BYTE PTR[ebx+0x0A]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x2C]
    movzx  edx,BYTE PTR[ebx+0x0B]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x30]
    movzx  edx,BYTE PTR[ebx+0x0C]
    add    edi,[ecx+edx*4]
    //
    // clamp the results
    //
    mov    ecx,iClampMaxLoc
    mov    edx,iClampMinLoc
    cmp    esi,ecx
    jle    $L0
    mov    esi,ecx
    jmp    SHORT $L1
$L0:cmp    esi,edx
    jge    $L1
    mov    esi,edx
$L1:mov    iSumY,esi
    cmp    edi,ecx
    jle    $L2
    mov    edi,ecx
    jmp    SHORT $L3
$L2:cmp    edi,edx
    jge    $L3
    mov    edi,edx
$L3:mov    iSumUV,edi
    }
#endif

    // round the result
    // was pre-rounded

    ucpD[0] = (iSumUV >> PRECISION);  // UV
    ucpD[1] = (iSumY >> PRECISION);  // Y

    // Do the Y,V combination

    ipLUT = ipLUTWei[ipSP[1]];
    iPixel = ipP[1];
    ucpUnpackY = ucpUnpacked[iJob][0];   // Y values
    ucpUnpackUV = ucpUnpacked[iJob][2];  // V value

    ucpUnpackY += iPixel;
    ucpUnpackUV += iPixel;

#if !MTI_ASM_X86_INTEL
    iSumY = iHalfPrecision;
    iSumUV = iHalfPrecision;

    // run through the 13 neighbors
    ipWei = ipLUT[0];
    iSumY += ipWei[ucpUnpackY[0]];
    iSumUV += ipWei[ucpUnpackUV[0]];

    ipWei = ipLUT[1];
    iSumY += ipWei[ucpUnpackY[1]];
    iSumUV += ipWei[ucpUnpackUV[1]];

    ipWei = ipLUT[2];
    iSumY += ipWei[ucpUnpackY[2]];
    iSumUV += ipWei[ucpUnpackUV[2]];

    ipWei = ipLUT[3];
    iSumY += ipWei[ucpUnpackY[3]];
    iSumUV += ipWei[ucpUnpackUV[3]];

    ipWei = ipLUT[4];
    iSumY += ipWei[ucpUnpackY[4]];
    iSumUV += ipWei[ucpUnpackUV[4]];

    ipWei = ipLUT[5];
    iSumY += ipWei[ucpUnpackY[5]];
    iSumUV += ipWei[ucpUnpackUV[5]];

    ipWei = ipLUT[6];
    iSumY += ipWei[ucpUnpackY[6]];
    iSumUV += ipWei[ucpUnpackUV[6]];

    ipWei = ipLUT[7];
    iSumY += ipWei[ucpUnpackY[7]];
    iSumUV += ipWei[ucpUnpackUV[7]];

    ipWei = ipLUT[8];
    iSumY += ipWei[ucpUnpackY[8]];
    iSumUV += ipWei[ucpUnpackUV[8]];

    ipWei = ipLUT[9];
    iSumY += ipWei[ucpUnpackY[9]];
    iSumUV += ipWei[ucpUnpackUV[9]];

    ipWei = ipLUT[10];
    iSumY += ipWei[ucpUnpackY[10]];
    iSumUV += ipWei[ucpUnpackUV[10]];

    ipWei = ipLUT[11];
    iSumY += ipWei[ucpUnpackY[11]];
    iSumUV += ipWei[ucpUnpackUV[11]];

    ipWei = ipLUT[12];
    iSumY += ipWei[ucpUnpackY[12]];
    iSumUV += ipWei[ucpUnpackUV[12]];

    // clamp the result
    if (iSumY > iClampMax)
      iSumY = iClampMax;
    else if (iSumY < iClampMin)
      iSumY = iClampMin;

    if (iSumUV > iClampMax)
      iSumUV = iClampMax;
    else if (iSumUV < iClampMin)
      iSumUV = iClampMin;
#else
    _asm {
    mov    esi,iHalfPrecisionLoc // iSumY
    mov    edi,iHalfPrecisionLoc // iSumUV

    mov    eax,ipLUT

    mov    ebx,ucpUnpackY

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x04]
    movzx  edx,BYTE PTR[ebx+0x01]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x08]
    movzx  edx,BYTE PTR[ebx+0x02]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x0C]
    movzx  edx,BYTE PTR[ebx+0x03]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x10]
    movzx  edx,BYTE PTR[ebx+0x04]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x14]
    movzx  edx,BYTE PTR[ebx+0x05]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x18]
    movzx  edx,BYTE PTR[ebx+0x06]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x1C]
    movzx  edx,BYTE PTR[ebx+0x07]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x20]
    movzx  edx,BYTE PTR[ebx+0x08]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x24]
    movzx  edx,BYTE PTR[ebx+0x09]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x28]
    movzx  edx,BYTE PTR[ebx+0x0A]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x2C]
    movzx  edx,BYTE PTR[ebx+0x0B]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x30]
    movzx  edx,BYTE PTR[ebx+0x0C]
    add    esi,[ecx+edx*4]

    mov    ebx,ucpUnpackUV

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x04]
    movzx  edx,BYTE PTR[ebx+0x01]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x08]
    movzx  edx,BYTE PTR[ebx+0x02]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x0C]
    movzx  edx,BYTE PTR[ebx+0x03]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x10]
    movzx  edx,BYTE PTR[ebx+0x04]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x14]
    movzx  edx,BYTE PTR[ebx+0x05]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x18]
    movzx  edx,BYTE PTR[ebx+0x06]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x1C]
    movzx  edx,BYTE PTR[ebx+0x07]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x20]
    movzx  edx,BYTE PTR[ebx+0x08]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x24]
    movzx  edx,BYTE PTR[ebx+0x09]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x28]
    movzx  edx,BYTE PTR[ebx+0x0A]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x2C]
    movzx  edx,BYTE PTR[ebx+0x0B]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x30]
    movzx  edx,BYTE PTR[ebx+0x0C]
    add    edi,[ecx+edx*4]
    //
    // clamp the results
    //
    mov    ecx,iClampMaxLoc
    mov    edx,iClampMinLoc
    cmp    esi,ecx
    jle    $L4
    mov    esi,ecx
    jmp    SHORT $L5
$L4:cmp    esi,edx
    jge    $L5
    mov    esi,edx
$L5:mov    iSumY,esi
    cmp    edi,ecx
    jle    $L6
    mov    edi,ecx
    jmp    SHORT $L7
$L6:cmp    edi,edx
    jge    $L7
    mov    edi,edx
$L7:mov    iSumUV,edi
    }
#endif

    // round the result
    // was pre-rounded

    ucpD[2] = (iSumUV >> PRECISION);  // UV
    ucpD[3] = (iSumY >> PRECISION);  // Y

    // bump pointers once
    ipSP += 2;
    ipP += 2;
    ucpD += 4;
   }
}

void CAspectRatio::RenderH_YUV_15 (MTI_UINT8 *ucpD, int &iCol, int iStopDstLocal,
		int iJob)
/*
	Do HORIZONTAL rendering with 15 neighbors
*/
{
  int *ipSP, *ipP, iC, iPixel;
  int **ipLUT;
  MTI_UINT8 *ucpUnpackY, *ucpUnpackUV;
  int iSumY, iSumUV, *ipWei;
  int iNei;

  // local copies
  int iHalfPrecisionLoc = iHalfPrecision;
  int iClampMinLoc = iClampMin;
  int iClampMaxLoc = iClampMax;

  // check for illegal args
  if ((iCol & 0x1) || (iStopDstLocal & 0x1))
    return;

  ipSP = ipLUTSubPixel + iCol;
  ipP = ipLUTPixel + iCol;
  for (; iCol < iStopDstLocal; iCol+=2)
   {
    // Do the Y,U combination

    ipLUT = ipLUTWei[ipSP[0]];
    iPixel = ipP[0];
    ucpUnpackY = ucpUnpacked[iJob][0];   // Y values
    ucpUnpackUV = ucpUnpacked[iJob][1];  // U value

    ucpUnpackY += iPixel;
    ucpUnpackUV += iPixel;

#if !MTI_ASM_X86_INTEL
    iSumY = iHalfPrecision;
    iSumUV = iHalfPrecision;

    // run through the 15 neighbors
    ipWei = ipLUT[0];
    iSumY += ipWei[ucpUnpackY[0]];
    iSumUV += ipWei[ucpUnpackUV[0]];

    ipWei = ipLUT[1];
    iSumY += ipWei[ucpUnpackY[1]];
    iSumUV += ipWei[ucpUnpackUV[1]];

    ipWei = ipLUT[2];
    iSumY += ipWei[ucpUnpackY[2]];
    iSumUV += ipWei[ucpUnpackUV[2]];

    ipWei = ipLUT[3];
    iSumY += ipWei[ucpUnpackY[3]];
    iSumUV += ipWei[ucpUnpackUV[3]];

    ipWei = ipLUT[4];
    iSumY += ipWei[ucpUnpackY[4]];
    iSumUV += ipWei[ucpUnpackUV[4]];

    ipWei = ipLUT[5];
    iSumY += ipWei[ucpUnpackY[5]];
    iSumUV += ipWei[ucpUnpackUV[5]];

    ipWei = ipLUT[6];
    iSumY += ipWei[ucpUnpackY[6]];
    iSumUV += ipWei[ucpUnpackUV[6]];

    ipWei = ipLUT[7];
    iSumY += ipWei[ucpUnpackY[7]];
    iSumUV += ipWei[ucpUnpackUV[7]];

    ipWei = ipLUT[8];
    iSumY += ipWei[ucpUnpackY[8]];
    iSumUV += ipWei[ucpUnpackUV[8]];

    ipWei = ipLUT[9];
    iSumY += ipWei[ucpUnpackY[9]];
    iSumUV += ipWei[ucpUnpackUV[9]];

    ipWei = ipLUT[10];
    iSumY += ipWei[ucpUnpackY[10]];
    iSumUV += ipWei[ucpUnpackUV[10]];

    ipWei = ipLUT[11];
    iSumY += ipWei[ucpUnpackY[11]];
    iSumUV += ipWei[ucpUnpackUV[11]];

    ipWei = ipLUT[12];
    iSumY += ipWei[ucpUnpackY[12]];
    iSumUV += ipWei[ucpUnpackUV[12]];

    ipWei = ipLUT[13];
    iSumY += ipWei[ucpUnpackY[13]];
    iSumUV += ipWei[ucpUnpackUV[13]];

    ipWei = ipLUT[14];
    iSumY += ipWei[ucpUnpackY[14]];
    iSumUV += ipWei[ucpUnpackUV[14]];

    // clamp the result
    if (iSumY > iClampMax)
      iSumY = iClampMax;
    else if (iSumY < iClampMin)
      iSumY = iClampMin;

    if (iSumUV > iClampMax)
      iSumUV = iClampMax;
    else if (iSumUV < iClampMin)
      iSumUV = iClampMin;
#else
    _asm {
    mov    esi,iHalfPrecisionLoc // iSumY
    mov    edi,iHalfPrecisionLoc // iSumUV

    mov    eax,ipLUT

    mov    ebx,ucpUnpackY

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x04]
    movzx  edx,BYTE PTR[ebx+0x01]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x08]
    movzx  edx,BYTE PTR[ebx+0x02]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x0C]
    movzx  edx,BYTE PTR[ebx+0x03]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x10]
    movzx  edx,BYTE PTR[ebx+0x04]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x14]
    movzx  edx,BYTE PTR[ebx+0x05]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x18]
    movzx  edx,BYTE PTR[ebx+0x06]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x1C]
    movzx  edx,BYTE PTR[ebx+0x07]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x20]
    movzx  edx,BYTE PTR[ebx+0x08]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x24]
    movzx  edx,BYTE PTR[ebx+0x09]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x28]
    movzx  edx,BYTE PTR[ebx+0x0A]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x2C]
    movzx  edx,BYTE PTR[ebx+0x0B]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x30]
    movzx  edx,BYTE PTR[ebx+0x0C]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x34]
    movzx  edx,BYTE PTR[ebx+0x0D]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x38]
    movzx  edx,BYTE PTR[ebx+0x0E]
    add    esi,[ecx+edx*4]

    mov    ebx,ucpUnpackUV

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x04]
    movzx  edx,BYTE PTR[ebx+0x01]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x08]
    movzx  edx,BYTE PTR[ebx+0x02]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x0C]
    movzx  edx,BYTE PTR[ebx+0x03]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x10]
    movzx  edx,BYTE PTR[ebx+0x04]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x14]
    movzx  edx,BYTE PTR[ebx+0x05]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x18]
    movzx  edx,BYTE PTR[ebx+0x06]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x1C]
    movzx  edx,BYTE PTR[ebx+0x07]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x20]
    movzx  edx,BYTE PTR[ebx+0x08]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x24]
    movzx  edx,BYTE PTR[ebx+0x09]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x28]
    movzx  edx,BYTE PTR[ebx+0x0A]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x2C]
    movzx  edx,BYTE PTR[ebx+0x0B]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x30]
    movzx  edx,BYTE PTR[ebx+0x0C]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x34]
    movzx  edx,BYTE PTR[ebx+0x0D]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x38]
    movzx  edx,BYTE PTR[ebx+0x0E]
    add    edi,[ecx+edx*4]
    //
    // clamp the results
    //
    mov    ecx,iClampMaxLoc
    mov    edx,iClampMinLoc
    cmp    esi,ecx
    jle    $L0
    mov    esi,ecx
    jmp    SHORT $L1
$L0:cmp    esi,edx
    jge    $L1
    mov    esi,edx
$L1:mov    iSumY,esi
    cmp    edi,ecx
    jle    $L2
    mov    edi,ecx
    jmp    SHORT $L3
$L2:cmp    edi,edx
    jge    $L3
    mov    edi,edx
$L3:mov    iSumUV,edi
    }
#endif

    // round the result
    // was pre-rounded

    ucpD[0] = (iSumUV >> PRECISION);  // UV
    ucpD[1] = (iSumY >> PRECISION);  // Y

    // Do the Y,V combination

    ipLUT = ipLUTWei[ipSP[1]];
    iPixel = ipP[1];
    ucpUnpackY = ucpUnpacked[iJob][0];   // Y values
    ucpUnpackUV = ucpUnpacked[iJob][2];  // V value

    ucpUnpackY += iPixel;
    ucpUnpackUV += iPixel;

#if !MTI_ASM_X86_INTEL
    iSumY = iHalfPrecision;
    iSumUV = iHalfPrecision;

    // run through the 15 neighbors
    ipWei = ipLUT[0];
    iSumY += ipWei[ucpUnpackY[0]];
    iSumUV += ipWei[ucpUnpackUV[0]];

    ipWei = ipLUT[1];
    iSumY += ipWei[ucpUnpackY[1]];
    iSumUV += ipWei[ucpUnpackUV[1]];

    ipWei = ipLUT[2];
    iSumY += ipWei[ucpUnpackY[2]];
    iSumUV += ipWei[ucpUnpackUV[2]];

    ipWei = ipLUT[3];
    iSumY += ipWei[ucpUnpackY[3]];
    iSumUV += ipWei[ucpUnpackUV[3]];

    ipWei = ipLUT[4];
    iSumY += ipWei[ucpUnpackY[4]];
    iSumUV += ipWei[ucpUnpackUV[4]];

    ipWei = ipLUT[5];
    iSumY += ipWei[ucpUnpackY[5]];
    iSumUV += ipWei[ucpUnpackUV[5]];

    ipWei = ipLUT[6];
    iSumY += ipWei[ucpUnpackY[6]];
    iSumUV += ipWei[ucpUnpackUV[6]];

    ipWei = ipLUT[7];
    iSumY += ipWei[ucpUnpackY[7]];
    iSumUV += ipWei[ucpUnpackUV[7]];

    ipWei = ipLUT[8];
    iSumY += ipWei[ucpUnpackY[8]];
    iSumUV += ipWei[ucpUnpackUV[8]];

    ipWei = ipLUT[9];
    iSumY += ipWei[ucpUnpackY[9]];
    iSumUV += ipWei[ucpUnpackUV[9]];

    ipWei = ipLUT[10];
    iSumY += ipWei[ucpUnpackY[10]];
    iSumUV += ipWei[ucpUnpackUV[10]];

    ipWei = ipLUT[11];
    iSumY += ipWei[ucpUnpackY[11]];
    iSumUV += ipWei[ucpUnpackUV[11]];

    ipWei = ipLUT[12];
    iSumY += ipWei[ucpUnpackY[12]];
    iSumUV += ipWei[ucpUnpackUV[12]];

    ipWei = ipLUT[13];
    iSumY += ipWei[ucpUnpackY[13]];
    iSumUV += ipWei[ucpUnpackUV[13]];

    ipWei = ipLUT[14];
    iSumY += ipWei[ucpUnpackY[14]];
    iSumUV += ipWei[ucpUnpackUV[14]];

    // clamp the result
    if (iSumY > iClampMax)
      iSumY = iClampMax;
    else if (iSumY < iClampMin)
      iSumY = iClampMin;

    if (iSumUV > iClampMax)
      iSumUV = iClampMax;
    else if (iSumUV < iClampMin)
      iSumUV = iClampMin;
#else
    _asm {
    mov    esi,iHalfPrecisionLoc // iSumY
    mov    edi,iHalfPrecisionLoc // iSumUV

    mov    eax,ipLUT

    mov    ebx,ucpUnpackY

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x04]
    movzx  edx,BYTE PTR[ebx+0x01]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x08]
    movzx  edx,BYTE PTR[ebx+0x02]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x0C]
    movzx  edx,BYTE PTR[ebx+0x03]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x10]
    movzx  edx,BYTE PTR[ebx+0x04]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x14]
    movzx  edx,BYTE PTR[ebx+0x05]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x18]
    movzx  edx,BYTE PTR[ebx+0x06]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x1C]
    movzx  edx,BYTE PTR[ebx+0x07]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x20]
    movzx  edx,BYTE PTR[ebx+0x08]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x24]
    movzx  edx,BYTE PTR[ebx+0x09]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x28]
    movzx  edx,BYTE PTR[ebx+0x0A]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x2C]
    movzx  edx,BYTE PTR[ebx+0x0B]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x30]
    movzx  edx,BYTE PTR[ebx+0x0C]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x34]
    movzx  edx,BYTE PTR[ebx+0x0D]
    add    esi,[ecx+edx*4]

    mov    ecx,[eax+0x38]
    movzx  edx,BYTE PTR[ebx+0x0E]
    add    esi,[ecx+edx*4]

    mov    ebx,ucpUnpackUV

    mov    ecx,[eax]
    movzx  edx,BYTE PTR[ebx]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x04]
    movzx  edx,BYTE PTR[ebx+0x01]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x08]
    movzx  edx,BYTE PTR[ebx+0x02]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x0C]
    movzx  edx,BYTE PTR[ebx+0x03]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x10]
    movzx  edx,BYTE PTR[ebx+0x04]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x14]
    movzx  edx,BYTE PTR[ebx+0x05]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x18]
    movzx  edx,BYTE PTR[ebx+0x06]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x1C]
    movzx  edx,BYTE PTR[ebx+0x07]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x20]
    movzx  edx,BYTE PTR[ebx+0x08]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x24]
    movzx  edx,BYTE PTR[ebx+0x09]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x28]
    movzx  edx,BYTE PTR[ebx+0x0A]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x2C]
    movzx  edx,BYTE PTR[ebx+0x0B]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x30]
    movzx  edx,BYTE PTR[ebx+0x0C]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x34]
    movzx  edx,BYTE PTR[ebx+0x0D]
    add    edi,[ecx+edx*4]

    mov    ecx,[eax+0x38]
    movzx  edx,BYTE PTR[ebx+0x0E]
    add    edi,[ecx+edx*4]
    //
    // clamp the results
    //
    mov    ecx,iClampMaxLoc
    mov    edx,iClampMinLoc
    cmp    esi,ecx
    jle    $L4
    mov    esi,ecx
    jmp    SHORT $L5
$L4:cmp    esi,edx
    jge    $L5
    mov    esi,edx
$L5:mov    iSumY,esi
    cmp    edi,ecx
    jle    $L6
    mov    edi,ecx
    jmp    SHORT $L7
$L6:cmp    edi,edx
    jge    $L7
    mov    edi,edx
$L7:mov    iSumUV,edi
    }
#endif

    // round the result
    // was pre-rounded

    ucpD[2] = (iSumUV >> PRECISION);  // UV
    ucpD[3] = (iSumY >> PRECISION);  // Y

    // bump pointers once
    ipSP += 2;
    ipP += 2;
    ucpD += 4;
   }
}

void CAspectRatio::RenderH_YUV (MTI_UINT8 *ucpD, int &iCol, int iStopDstLocal,
		int iJob)
/*
	Do HORIZONTAL rendering with arbitrary neighbors
*/
{
  int *ipSP, *ipP, iC, iPixel;
  int **ipLUT;
  MTI_UINT8 *ucpUnpackY, *ucpUnpackUV;
  int iSumY, iSumUV, *ipWei;
  int iNei;

  // check for illegal args
  if ((iCol & 0x1) || (iStopDstLocal & 0x1))
    return;

  ipSP = ipLUTSubPixel + iCol;
  ipP = ipLUTPixel + iCol;
  for (; iCol < iStopDstLocal; iCol+=2)
   {
    // Do the Y,U combination

    ipLUT = ipLUTWei[*ipSP++];
    iPixel = *ipP++;
    ucpUnpackY = ucpUnpacked[iJob][0];   // Y values
    ucpUnpackUV = ucpUnpacked[iJob][1];  // U value

    ucpUnpackY += iPixel;
    ucpUnpackUV += iPixel;

    iSumY = 0;
    iSumUV = 0;

    for (iNei = 0; iNei < iTotalNeighbor; iNei++)
     {
      ipWei = *ipLUT++;

      iSumY += ipWei[*ucpUnpackY++];
      iSumUV += ipWei[*ucpUnpackUV++];
     }

    // clamp the result
    if (iSumY > iClampMax)
      iSumY = iClampMax;
    else if (iSumY < iClampMin)
      iSumY = iClampMin;

    if (iSumUV > iClampMax)
      iSumUV = iClampMax;
    else if (iSumUV < iClampMin)
      iSumUV = iClampMin;

    // round the result
    iSumY = ((iSumY + iHalfPrecision) >> PRECISION);
    iSumUV = ((iSumUV + iHalfPrecision) >> PRECISION);

    *ucpD++ = iSumUV;  // UV
    *ucpD++ = iSumY;  // Y

    // Do the Y,V combination

    ipLUT = ipLUTWei[*ipSP++];
    iPixel = *ipP++;
    ucpUnpackY = ucpUnpacked[iJob][0];   // Y values
    ucpUnpackUV = ucpUnpacked[iJob][2];  // V value

    ucpUnpackY += iPixel;
    ucpUnpackUV += iPixel;

    iSumY = 0;
    iSumUV = 0;

    for (iNei = 0; iNei < iTotalNeighbor; iNei++)
     {
      ipWei = *ipLUT++;

      iSumY += ipWei[*ucpUnpackY++];
      iSumUV += ipWei[*ucpUnpackUV++];
     }

    // clamp the result 
    if (iSumY > iClampMax)
      iSumY = iClampMax;
    else if (iSumY < iClampMin)
      iSumY = iClampMin;

    if (iSumUV > iClampMax)
      iSumUV = iClampMax;
    else if (iSumUV < iClampMin)
      iSumUV = iClampMin;

    // round the result
    iSumY = ((iSumY + iHalfPrecision) >> PRECISION);
    iSumUV = ((iSumUV + iHalfPrecision) >> PRECISION);

    *ucpD++ = iSumUV;  // UV
    *ucpD++ = iSumY;  // Y
   }
}
