/*
   File Name:  imgToolDLL.h
   Created Aug 22/2001 by John Mertus

   This just defines the export/import macros
   if MTI_IMGTOOLDLL is defined, exports are defined
   else imports are defined
*/

#ifndef IMGTOOLDLL_H
#define IMGTOOLDLL_H

//////////////////////////////////////////////////////////////////////
#include "machine.h"

#ifdef _WIN32                  // Window compilers need export/import

// Define Import/Export for windows
#ifdef MTI_IMGTOOLDLL
#define MTI_IMGTOOLDLL_API  __declspec(dllexport)
#else
#define MTI_IMGTOOLDLL_API  __declspec(dllimport)
#endif

//--------------------UNIX---------------------------
#else

// Needs nothing for SGI, UNIX, LINUX
#define MTI_IMGTOOLDLL_API
#endif   // End of UNIX

#endif   // Header file


