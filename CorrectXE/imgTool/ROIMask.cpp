#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mt_math.h"
#include "ROIMask.h"
#include "MTImalloc.h"

// edge orientations:
// 0 - horizontal to the right
// 1 - less than 45 deg below the horizon to the right
// 2 - 45 deg below horizon to the right
// 3 - more than 45 deg below the horizon to the right
// 4 - vertical down
// 5 - more than 45 deg below the horizon to the left
// 6 - 45 deg below horizon to the left
// 7 - less than 45 deg below the horizon to the left

CROIMask::CROIMask()
{
   // dispatch table for stretch methods
   // indexed by edge orientation (edgCod)
   strTbl[0] = CROIMask::StretchXEdge0;
   strTbl[1] = CROIMask::StretchXEdge1;
   strTbl[2] = CROIMask::StretchXEdge2;
   strTbl[3] = CROIMask::StretchXEdge3;
   strTbl[4] = CROIMask::StretchXEdge4;
   strTbl[5] = CROIMask::StretchXEdge5;
   strTbl[6] = CROIMask::StretchXEdge6;
   strTbl[7] = CROIMask::StretchXEdge7;

   // dispatch table for step methods
   // indexed by edge orientation (edgCod)
   stpTbl[0] = CROIMask::StepYEdge0;
   stpTbl[1] = CROIMask::StepYEdge1;
   stpTbl[2] = CROIMask::StepYEdge2;
   stpTbl[3] = CROIMask::StepYEdge3;
   stpTbl[4] = CROIMask::StepYEdge4;
   stpTbl[5] = CROIMask::StepYEdge5;
   stpTbl[6] = CROIMask::StepYEdge6;
   stpTbl[7] = CROIMask::StepYEdge7;

   // allocate buffer for edge records
   edgcnt = EDGES_INIT;
   edgbuf = (EDGE *)malloc(EDGES_INIT*sizeof(EDGE));   // Was MTImalloc
   if (edgbuf==NULL) edgcnt = 0;
}

CROIMask::~CROIMask()
{
   // free the edge buffer
   if (edgbuf!=NULL) free(edgbuf);                   // Was MTIfree
}

// attaches a mask buffer to the engine
void CROIMask::initMask(int wdth, int hght, MTI_UINT8 *frm)
{
   // pitch is bytes in 1 row of mask buffer
   maskWdth = wdth;
   maskHght = hght;
   dpitch   = wdth;

   // set the frame buffer
   bytbuf = frm;

   // init active region to the whole mask
   region.left   = 0;
   region.top    = 0;
   region.right  = maskWdth-1;
   region.bottom = maskHght-1;
}

// clears the whole mask to a color
void CROIMask::clearMask(MTI_UINT8 color)
{
   int pels = maskWdth * maskHght;
   MTImemset(bytbuf, color, pels);
}

// here UL is inclusive, LR is exclusive
void CROIMask::setActiveRegion(int lft, int top, int rgt, int bot)
{
   region.left   = lft;
   region.top    = top;
   region.right  = rgt-1;
   region.bottom = bot-1;
}

void CROIMask::setDrawColor(MTI_UINT8 col)
{
   fcolr = col;
}

// fills the active region with the draw color
void CROIMask::fillActiveRegion()
{
   fillRectangle(region.left, region.top, region.right, region.bottom);
}

// fills a rectangle with the draw color
void CROIMask::fillRectangle(int lft, int top, int rgt, int bot)
{
   MTI_UINT8 *dst = top * dpitch + lft + bytbuf;
   int rowinc = dpitch - (rgt - lft + 1);
   for (int i=0;i<(bot - top + 1);i++) {
      for (int j=0;j<(rgt - lft + 1);j++) {
         *dst++ = fcolr;
      }
      dst += rowinc;
   }
}

int CROIMask::initFill(int edges)
{

#ifdef SMALL
   if ((edges+4) > 65535)
      return ROIMASK_ERR_INSUFF_EDGES;
#endif

   if ((edges+4) > edgcnt) { // edge-buffer not big enough, need to reallocate

      if (edgbuf!=NULL) free(edgbuf);                 // Was MTIfree
      edgcnt = 0;
      edgbuf = (EDGE *)malloc((edges+4)*sizeof(EDGE));  // Was MTImalloc
      if (edgbuf == NULL)
         return ROIMASK_ERR_INSUFF_EDGES;
      edgcnt = (edges+4);
   }

   //edgptr = edgbuf;
   iedgptr = 0;
   return 0;
}

void CROIMask::moveToFill(int x, int y)
{
   curx = x;
   cury = y;
}

void CROIMask::lineToFill(int x, int y)
{
   int delx,dely;

   if (y==cury) { // horizontal edge

      // ALLOW NULL EDGES - Bugzilla ref #1556
      //if (x==curx) return;

      edgbuf[iedgptr].ybot = edgbuf[iedgptr].ytop = y;
      if (x > curx) {
         edgbuf[iedgptr].xbot = curx;
         edgbuf[iedgptr].xtop = x;
      }
      else {
         edgbuf[iedgptr].xbot = x;
         edgbuf[iedgptr].xtop = curx;
      }
   }
   else { // vertical or oblique edge

      if (y>cury) {
         edgbuf[iedgptr].xbot = curx;
         edgbuf[iedgptr].ybot = cury;
         edgbuf[iedgptr].xtop = x;
         edgbuf[iedgptr].ytop = y;
      }
      else {
         edgbuf[iedgptr].xbot = x;
         edgbuf[iedgptr].ybot = y;
         edgbuf[iedgptr].xtop = curx;
         edgbuf[iedgptr].ytop = cury;
      }
   }

   // before the first stretch, init XMIN & XMAX this
   edgbuf[iedgptr].xmin = edgbuf[iedgptr].xmax = edgbuf[iedgptr].xbot;


   dely=edgbuf[iedgptr].ytop-edgbuf[iedgptr].ybot;
   if (dely==0) { // horizontal
      edgbuf[iedgptr].edgCod = 0;
   }
   else { // motion in y
      delx=edgbuf[iedgptr].xtop-edgbuf[iedgptr].xbot;
      if (delx==0) { // vertical
         edgbuf[iedgptr].edgCod = 4;
      }
      else {
         if (delx > 0) {
            edgbuf[iedgptr].edgCod = 2;
            if (delx>dely) {
               edgbuf[iedgptr].edgCod = 1;
               edgbuf[iedgptr].pen = delx;
            }
            else {
               if (delx<dely) {
                  edgbuf[iedgptr].edgCod = 3;
                  edgbuf[iedgptr].pen = dely;
               }
            }
         }
         else {
            delx = -delx;
            edgbuf[iedgptr].edgCod = 6;
            if (delx<dely) {
               edgbuf[iedgptr].edgCod = 5;
               edgbuf[iedgptr].pen = dely;
            }
            else {
               if (delx>dely) {
                  edgbuf[iedgptr].edgCod = 7;
                  edgbuf[iedgptr].pen = delx;
               }
            }
         }

         // Bresenham constants
         edgbuf[iedgptr].brx = delx<<1;
         edgbuf[iedgptr].bry = dely<<1;
      }
   }

   iedgptr++; // advance filling ptr

   // advance CP x and y
   curx = x;
   cury = y;
   return;
}

// draw a spline segment from (curx, cury) to (x3, y3), using
// (x1, y1)  and ( x2, y2) as the intermediate control points.
// To draw an entire spline, call moveToFill(x0, y0), followed
// by a series of calls to splineToFill, supplying control points
// and attachment points
//
// KT-060224 Bugzilla ref #2186 - the number of calls to "lineToFill"
// in the draw loop was too large by 1, owing to the incorrect way
// the loop was written. This fixes a problem with overflow of the edge
// buffer.
//
void CROIMask::splineToFill(int x1, int y1, // nxt ctrl point, current  vertex
                            int x2, int y2, // pre ctrl point, terminal vertex
                            int x3, int y3, // terminal vertex
                            int chordleng)
{
   int x0 = curx; int y0 = cury;
   int delx, dely;

   // coefficients for X
   double u0 = x0;
   double u1 = 3*(x1 - x0);
   double u2 = 3*(x2 - 2*x1 + x0);
   double u3 = (x3 - 3*x2 + 3*x1 - x0);

   // coefficients for Y
   double v0 = y0;
   double v1 = 3*(y1 - y0);
   double v2 = 3*(y2 - 2*y1 + y0);
   double v3 = (y3 - 3*y2 + 3*y1 - y0);

   // "length" of spline
   int length = 0;
   if ((delx = x1 - x0)<0) delx = -delx;
   length += delx;
   if ((dely = y1 - y0)<0) dely = -dely;
   length += dely;
   if ((delx = x2 - x1)<0) delx = -delx;
   length += delx;
   if ((dely = y2 - y1)<0) dely = -dely;
   length += dely;
   if ((delx = x3 - x2)<0) delx = -delx;
   length += delx;
   if ((dely = y3 - y2)<0) dely = -dely;
   length += dely;

   // number of segments
   int nseg = length / chordleng;

   // must be at least one
   if (nseg == 0) nseg = 1;

   // now draw the spline segments
   double t;
   for (int i=1; i<nseg; i++) {
      t = (double)i/(double)nseg;
      lineToFill((((u3*t+u2)*t+u1)*t+u0)+.5, (((v3*t+v2)*t+v1)*t+v0)+.5);
   }

   // get the last chord
   lineToFill(x3, y3);
}

void CROIMask::fillPolygon()
{
   EDGINDEX icuredg, ipreedg, inxtedg;
   int rowcov, lft;

   if (iedgptr > 0) { // we have edges in the buffer

      //     S O R T   E D G E S

      // the edges in scan order (increasing y = down)
      ilexlst = iedgeSort(0,iedgptr-1);

      // the edges crossing a scan-line
      iedglst = NULLEDG;

      do {  //     M A I N   L O O P  for scan-lines

         // edge list is empty, advance the scan to next edge
         if (iedglst==NULLEDG) {

            // set CP incl cury
            moveTo(0, edgbuf[ilexlst].ybot);
        }

         //     I N S E R T   P H A S E

         // pull in all the edges which have lower y at this scan=line and
         // insert them into the edge-list in their proper order
         while ((ilexlst!=NULLEDG)&&(edgbuf[ilexlst].ybot==cury)) {

            // stretch the edge to define xmin and xmax at cury
            strTbl[edgbuf[ilexlst].edgCod](&edgbuf[ilexlst]);

            if (edgbuf[ilexlst].ybot==edgbuf[ilexlst].ytop) { // horz edge

               // we draw it but do not insert into the edge-list
               hatchLine(edgbuf[ilexlst].xmin,edgbuf[ilexlst].xmax);

            }
            else {

               // the edge is vertical or oblique. Insert into edge-list
               ipreedg = NULLEDG;
               icuredg = iedglst;
               while ((icuredg!=NULLEDG)&&(edgbuf[ilexlst].xmin>edgbuf[icuredg].xmin)) {
                  ipreedg = icuredg;
                  icuredg = edgbuf[icuredg].ifwd;
               }
               insertEdge(ilexlst,ipreedg,icuredg);
            }

            // advance to the next edge in y-order
            ilexlst = edgbuf[ilexlst].inxtlex;
         }

         //     D R A W   P H A S E

         if (cury >= region.top) { // draw the scan line

            // beginning with the left edge of the mask, trace the edge-list in
            // x-order drawing the hatch lines when the covering is 1
            rowcov = 0;
            lft = region.left;
            icuredg = iedglst;
            while (icuredg!=NULLEDG) {
               if (rowcov) {
                  hatchLine(lft,edgbuf[icuredg].xmax);
               }
               lft = edgbuf[icuredg].xmin;
               rowcov = 1 - rowcov;
               icuredg = edgbuf[icuredg].ifwd;
            }
            if (rowcov) hatchLine(lft,region.right);

         }

         // done with this scan-line; advance CP's y-value
         curptr += dpitch;

         // if we pass the bot y, we're all done
         if (++cury > region.bottom) iedglst = ilexlst = NULLEDG;

         //     U P D A T E   P H A S E
         //
         // Go through the edge list and update all the edges to
         // reflect their xmin's and xmax's at the next y-level

         icuredg = iedglst;
         while (icuredg!=NULLEDG) {

            inxtedg = edgbuf[icuredg].ifwd;

            // step in y, then stretch in x
            stpTbl[edgbuf[icuredg].edgCod](&edgbuf[icuredg]);
            strTbl[edgbuf[icuredg].edgCod](&edgbuf[icuredg]);

            if (edgbuf[icuredg].ytop==cury) { // TTT - see Mask.cpp

               // we're at the end of this edge's relevance
               hatchLine(edgbuf[icuredg].xmin, edgbuf[icuredg].xmax);
               deleteEdge(icuredg);
            }
            else {

               // if the update causes the edge-list to be disordered in x,
               // re-insert the edge into the edge-list in the correct place
               ipreedg = edgbuf[icuredg].ibwd;
               while ((ipreedg!=NULLEDG)&&(edgbuf[ipreedg].xmin > edgbuf[icuredg].xmin))
                  ipreedg = edgbuf[ipreedg].ibwd;
               if (ipreedg!=edgbuf[icuredg].ibwd) { // re-insert into list
                  deleteEdge(icuredg);
                  if (ipreedg==NULLEDG) insertEdge(icuredg,ipreedg,iedglst);
                  else insertEdge(icuredg,ipreedg,edgbuf[ipreedg].ifwd);
               }
            }
            icuredg = inxtedg;
         }

      } while ((ilexlst!=NULLEDG)||(iedglst!=NULLEDG));
   }
}

void CROIMask::fillPolygonComplement()
{
   // add the boundary of the universe
   moveToFill(region.left,  region.top);
   lineToFill(region.right, region.top);
   lineToFill(region.right, region.bottom);
   lineToFill(region.left,  region.bottom);
   lineToFill(region.left,  region.top);

   // and you have the filled complement
   fillPolygon();

   // remove the boundary of the universe so that
   // subsequent calls to "expandLastFillContour"
   // don't include it
   iedgptr -= 4;
}

// edge-node sort using recursive algorithm
EDGINDEX CROIMask::iedgeSort(EDGINDEX iloptr, EDGINDEX ihiptr)
{
  EDGINDEX imidptr, ichn1, ichn2, itemp, ihead, itail;

  if (iloptr==ihiptr) { // only one node
    edgbuf[iloptr].inxtlex = NULLEDG;
    return iloptr;
  }

  if ((ihiptr - iloptr)==1) { // only two nodes

    if ((edgbuf[iloptr].ybot<edgbuf[ihiptr].ybot)||
       ((edgbuf[iloptr].ybot==edgbuf[ihiptr].ybot)&&
       (edgbuf[iloptr].xbot<=edgbuf[ihiptr].xbot))) {

        edgbuf[iloptr].inxtlex = ihiptr;
        edgbuf[ihiptr].inxtlex = NULLEDG;
        return iloptr;
    }
    else {
        edgbuf[ihiptr].inxtlex = iloptr;
        edgbuf[iloptr].inxtlex = NULLEDG;
        return ihiptr;
    }
  }

  // more than two nodes: split into two halves and sort each separately
  imidptr = (iloptr + (ihiptr - iloptr)/2);
  ichn1 = iedgeSort(iloptr,imidptr);
  ichn2 = iedgeSort(imidptr+1,ihiptr);

  // define the head of the merged chain
  if ((edgbuf[ichn2].ybot<edgbuf[ichn1].ybot)||
     ((edgbuf[ichn2].ybot==edgbuf[ichn1].ybot)&&
     (edgbuf[ichn2].xbot<edgbuf[ichn1].xbot))) {
    itemp = ichn1;
    ichn1 = ichn2;
    ichn2 = itemp;
  }
  ihead = itail = ichn1;
  ichn1 = edgbuf[ichn1].inxtlex;

  // as above, CHN1 is always the chain that advances
  while (ichn1 != NULLEDG) {
    if ((edgbuf[ichn2].ybot<edgbuf[ichn1].ybot)||
       ((edgbuf[ichn2].ybot==edgbuf[ichn1].ybot)&&
       (edgbuf[ichn2].xbot<edgbuf[ichn1].xbot))) {

      itemp = ichn1;
      ichn1 = ichn2;
      ichn2 = itemp;
    }
    edgbuf[itail].inxtlex = ichn1;
    itail = ichn1;
    ichn1 = edgbuf[ichn1].inxtlex;
  }
  edgbuf[itail].inxtlex = ichn2;

  return ihead;
}

// insert an edge into the SCAN LIST
void CROIMask::insertEdge(EDGINDEX iedg, EDGINDEX ipre, EDGINDEX inxt)
{
  edgbuf[iedg].ibwd = ipre;
  if (ipre!=NULLEDG) {
    edgbuf[ipre].ifwd = iedg;
  }
  else {
    iedglst = iedg;
  }
  edgbuf[iedg].ifwd = inxt;
  if (inxt!=NULLEDG) {
    edgbuf[inxt].ibwd = iedg;
  }
}

// delete an edge from the SCAN LIST
void CROIMask::deleteEdge(EDGINDEX iedg)
{
  EDGINDEX ipre,inxt;

  ipre = edgbuf[iedg].ibwd;
  inxt = edgbuf[iedg].ifwd;
  if (ipre!=NULLEDG) {
    edgbuf[ipre].ifwd = inxt;
  }
  else {
    iedglst = inxt;
  }
  if (inxt!=NULLEDG) {
    edgbuf[inxt].ibwd = ipre;
  }
}

// Draw a hatchline. Entered with curptr set for
// the correct y value, with a 0 value for curx.
//
void CROIMask::hatchLine(int lft, int rgt)
{
   // detect null hatchlines
   if (lft > rgt) return;

   // Bezier splines may generate edges  outside the limits
   // of the mask's active region. So we'll clip hatchlines.
   if ((cury < region.top)||(cury > region.bottom)) return;
   if (lft < region.left)  lft = region.left;
   if (rgt > region.right) rgt = region.right;

   // draw the hatchline for the fill
   MTI_UINT8 *dst = curptr + lft;
   for (int i=0;i<(rgt - lft + 1);i++)
      *dst++ = fcolr;
}

// code to stretch XMIN and XMAX for a just-set Y-value
void CROIMask::StretchXEdge0(EDGE *edg)
{
   // for a horizontal edge, XMAX is set to XTOP
   edg->xmax = edg->xtop;
   return;
}

void CROIMask::StretchXEdge1(EDGE *edg)
{
   // for a shallow edge to lower right, we
   // use as many Bresenham cycles as we need
   while (((edg->pen -= edg->bry)>0)&&(edg->xmax<edg->xtop)) {
      edg->xmax++;
   }
   return;
}

void CROIMask::StretchXEdge2(EDGE *edg)
{
   // line at 45 edg to lower right
   return;
}

void CROIMask::StretchXEdge3(EDGE *edg)
{
   // for edges from 45 to 135, XMAX = XMIN
   // hence there is no stretching needed
   return;
}

void CROIMask::StretchXEdge4(EDGE *edg)
{
   // for edges from 45 to 135, XMAX = XMIN
   // hence there is no stretching needed
   return;
}

void CROIMask::StretchXEdge5(EDGE *edg)
{
   // for edges from 45 to 135, XMAX = XMIN
   // hence there is no stretching needed
   return;
}

void CROIMask::StretchXEdge6(EDGE *edg)
{
   // for edges from 45 to 135, XMAX = XMIN
   // hence there is no stretching needed
   return;
}

void CROIMask::StretchXEdge7(EDGE *edg)
{
   // shallow line to lower left
   while (((edg->pen -= edg->bry)>0)&&(edg->xmin>edg->xtop)) {
      edg->xmin--;
   }
   return;
}

// code to reset XMIN and XMAX for a Bresenham step in Y
void CROIMask::StepYEdge0(EDGE *edg)
{
   // horizontal lines live for only one scan line
   return;
}

void CROIMask::StepYEdge1(EDGE *edg)
{
   // shallow line to lower right
   edg->pen += edg->brx;
   edg->xmin = edg->xmax += 1;
   return;
}

void CROIMask::StepYEdge2(EDGE *edg)
{
   // line at 45 deg to lower right
   edg->xmin = edg->xmax += 1;
   return;
}

void CROIMask::StepYEdge3(EDGE *edg)
{
   // steep line to lower right
   if ((edg->pen -= edg->brx) <= 0) {
      edg->pen += edg->bry;
      edg->xmin = edg->xmax += 1;
   }
   return;
}

void CROIMask::StepYEdge4(EDGE *edg)
{
   // vertical line
   return;
}

void CROIMask::StepYEdge5(EDGE *edg)
{
   // steep line to lower left
   if ((edg->pen -= edg->brx) <= 0) {
      edg->pen += edg->bry;
      edg->xmax = edg->xmin -= 1;
   }
   return;
}

void CROIMask::StepYEdge6(EDGE *edg)
{
   // 45 deg line to lower left
   edg->xmax = edg->xmin -= 1;
   return;
}

void CROIMask::StepYEdge7(EDGE *edg)
{
   // shallow line to lower left
   edg->pen += edg->brx;
   edg->xmax = edg->xmin -= 1;
   return;
}

void CROIMask::setPadRadius(int rad)
{
   padRadius = rad;
   padRadiusSq = rad*rad;
}

void CROIMask::setPadRadii(int radinterior, int radexterior)
{
   if (radinterior < 0) radinterior = -radinterior;
   if (radexterior < 0) radexterior = -radexterior;

   // copy these in
   padRadiusInterior = radinterior;
   padRadiusExterior = radexterior;

   // save the sum of the two
   padWidthTotal = padRadiusInterior + padRadiusExterior;

   // and the max of the two
   padRadius = padRadiusInterior;
   if (padRadiusExterior > padRadius)
      padRadius = padRadiusExterior;

   // these parameters determine the (linear) boundary blend
   blendStep = 0.;
   if (padWidthTotal != 0)
      blendStep = 255. / (padRadiusInterior + padRadiusExterior);
   blendIntercept = blendStep * padRadiusExterior;
}

MTI_UINT8 CROIMask::getBlendLevel(double dist)
{
   // If padWidthTotal == 0  then  both
   // padRadiusInterior & padRadiusExterior
   // The next two lines will determine the
   // value returned in that case...
   if (dist <= - padRadiusInterior) return 255;
   if (dist >    padRadiusExterior) return   0;

   // and this line will never be executed
   return blendIntercept - dist * blendStep + .5;
}

void CROIMask::setEnableColor(MTI_UINT8 enblcol)
{
   // before a pixel is set by padPixel, it
   // is tested to see what value it already
   // contains. If the existing value matches
   // the ENABLE color, the pixel is set to
   // the REPLACE color.
   enableColor = enblcol;
}

void CROIMask::setReplaceColor(MTI_UINT8 replcol)
{
   // (see above)
   replaceColor = replcol;
}

void CROIMask::moveTo(int x, int y)
{
   curx = x;
   cury = y;
   curptr = dpitch * y + x + bytbuf;
}

// take each edge in the buffer (following
// the last fill operation) and assign blend
// values to the pixels close to it within a
// a sausage contour
void CROIMask::blendLastFillContour()
{
   for (EDGE *curedg = edgbuf; curedg < &edgbuf[iedgptr]; curedg++) {

      int x0 = curedg->xbot;
      int y0 = curedg->ybot;
      int x1 = curedg->xtop;
      int y1 = curedg->ytop;

      int delx = (x1 - x0); int dely = (y1 - y0);

      double edgeLength = sqrt((double)(delx*delx+dely*dely));

      RECT lineExtent;
      lineExtent.left = x0; lineExtent.right = x1;
      if (x1 < x0) {
         lineExtent.left = x1; lineExtent.right = x0;
      }
      lineExtent.top = y0; lineExtent.bottom = y1;

      if ((lineExtent.left -= padRadius)<0)
         lineExtent.left = 0;
      if ((lineExtent.right += padRadius)>(maskWdth-1))
         lineExtent.right = (maskWdth-1);
      if ((lineExtent.top -= padRadius)<0)
         lineExtent.top = 0;
      if ((lineExtent.bottom += padRadius)>(maskHght-1))
         lineExtent.bottom = (maskHght-1);

      MTI_UINT8 *rowbeg = lineExtent.top*dpitch + bytbuf;
      for (int y=lineExtent.top;y<=lineExtent.bottom;y++) {

         for (int x=lineExtent.left;x<=lineExtent.right;x++) {

            double edgeDistance;

            int delx0 = (x0 - x); int dely0 = (y0 - y);
            int delx1 = (x1 - x); int dely1 = (y1 - y);

            int v0dotedge = delx0*delx + dely0*dely;
            int v1dotedge = delx1*delx + dely1*dely;
            if (((v0dotedge < 0)&&(v1dotedge < 0))||
                ((v0dotedge > 0)&&(v1dotedge > 0))) {

               int e0sq = delx0*delx0+dely0*dely0;
               int e1sq = delx1*delx1+dely1*dely1;
               if (e0sq < e1sq)
                  edgeDistance = sqrt((double)e0sq);
               else
                  edgeDistance = sqrt((double)e1sq);

            }
            else {

               int v0crsv1 = delx0*dely1 - delx1*dely0;
               if (v0crsv1 < 0) v0crsv1 = -v0crsv1;
               edgeDistance = (double)v0crsv1 / edgeLength;
            }

            if (edgeDistance < padRadius) {

               MTI_UINT8 val;;
               MTI_UINT8 *dst = (rowbeg + x);
               if (*dst <= blendIntercept) { // pixel is outside poly

                  if ((val=getBlendLevel(edgeDistance)) > *dst)
                     *dst = val;
               }
               else {                        // pixel is inside poly

                  if ((val=getBlendLevel(-edgeDistance)) < *dst)
                     *dst = val;
               }
            }
         }

         rowbeg += dpitch;
      }
   }
}

// use the blend values set by a prior call to
// blendLastFillContour() to introduce graded
// dithering into the mask.
void CROIMask::ditherLastFillContour()
{
   long lseed = 1357935; // odd val for good performance

   for (EDGE *curedg = edgbuf; curedg < &edgbuf[iedgptr]; curedg++) {

      int x0 = curedg->xbot;
      int y0 = curedg->ybot;
      int x1 = curedg->xtop;
      int y1 = curedg->ytop;

      RECT lineExtent;
      lineExtent.left = x0; lineExtent.right = x1;
      if (x1 < x0) {
         lineExtent.left = x1; lineExtent.right = x0;
      }
      lineExtent.top = y0; lineExtent.bottom = y1;

      if ((lineExtent.left -= padRadius)<0)
         lineExtent.left = 0;
      if ((lineExtent.right += padRadius)>(maskWdth-1))
         lineExtent.right = (maskWdth-1);
      if ((lineExtent.top -= padRadius)<0)
         lineExtent.top = 0;
      if ((lineExtent.bottom += padRadius)>(maskHght-1))
         lineExtent.bottom = (maskHght-1);

      MTI_UINT8 *rowbeg = lineExtent.top*dpitch + bytbuf;
      for (int y=lineExtent.top;y<=lineExtent.bottom;y++) {

         for (int x=lineExtent.left;x<=lineExtent.right;x++) {

            MTI_UINT8 *dst = (rowbeg + x);

            MTI_UINT8 val = *dst;
            if ((val > 0)&&(val < 255)) {
               if (ran266(&lseed) < ((double)val/255.))
                  *dst = 128;		// a 50/50 average is used for dither
               else
                  *dst = 0;
            }
         }

         rowbeg += dpitch;
      }
   }
}

// take each edge in the buffer (following
// the last fill operation) and assign label
// values to the pixels close to it within a
// a sausage contour
void CROIMask::labelLastFillContour()
{
   for (EDGE *curedg = edgbuf; curedg < &edgbuf[iedgptr]; curedg++) {

      int x0 = curedg->xbot;
      int y0 = curedg->ybot;
      int x1 = curedg->xtop;
      int y1 = curedg->ytop;

      RECT lineExtent;
      lineExtent.left = x0; lineExtent.right = x1;
      if (x1 < x0) {
         lineExtent.left = x1; lineExtent.right = x0;
      }
      lineExtent.top = y0; lineExtent.bottom = y1;

      if ((lineExtent.left -= padRadius)<0)
         lineExtent.left = 0;
      if ((lineExtent.right += padRadius)>(maskWdth-1))
         lineExtent.right = (maskWdth-1);
      if ((lineExtent.top -= padRadius)<0)
         lineExtent.top = 0;
      if ((lineExtent.bottom += padRadius)>(maskHght-1))
         lineExtent.bottom = (maskHght-1);

      int delx = (x1 - x0); int dely = (y1 - y0);

      if ((delx == 0)&&(dely==0)) {  // zero-length edge

         MTI_UINT8 *rowbeg = lineExtent.top*dpitch + bytbuf;
         for (int y=lineExtent.top;y<=lineExtent.bottom;y++) {

            for (int x=lineExtent.left;x<=lineExtent.right;x++) {

               MTI_UINT8 *dst = (rowbeg + x);
               if (*dst != enableColor) continue;

               double edgeDistance;

               int delx0 = (x0 - x); int dely0 = (y0 - y);

               int e0sq = delx0*delx0+dely0*dely0;
               edgeDistance = sqrt((double)e0sq);

               if (edgeDistance < padRadius) {

                  *dst = replaceColor;
               }
            }

            rowbeg += dpitch;
         }
      }
      else {                         // nonzero-length edge

         double edgeLength = sqrt((double)(delx*delx+dely*dely));

         MTI_UINT8 *rowbeg = lineExtent.top*dpitch + bytbuf;
         for (int y=lineExtent.top;y<=lineExtent.bottom;y++) {

            for (int x=lineExtent.left;x<=lineExtent.right;x++) {

               MTI_UINT8 *dst = (rowbeg + x);
               if (*dst != enableColor) continue;

               double edgeDistance;

               int delx0 = (x0 - x); int dely0 = (y0 - y);
               int delx1 = (x1 - x); int dely1 = (y1 - y);

               int v0dotedge = delx0*delx + dely0*dely;
               int v1dotedge = delx1*delx + dely1*dely;
               if (((v0dotedge < 0)&&(v1dotedge < 0))||
                   ((v0dotedge > 0)&&(v1dotedge > 0))) {

                  int e0sq = delx0*delx0+dely0*dely0;
                  int e1sq = delx1*delx1+dely1*dely1;
                  if (e0sq < e1sq)
                     edgeDistance = sqrt((double)e0sq);
                  else
                     edgeDistance = sqrt((double)e1sq);

               }
               else {

                  int v0crsv1 = delx0*dely1 - delx1*dely0;
                  if (v0crsv1 < 0) v0crsv1 = -v0crsv1;
                  edgeDistance = (double)v0crsv1 / edgeLength;
               }

               if (edgeDistance < padRadius) {

                  *dst = replaceColor;
               }
            }

            rowbeg += dpitch;
         }
      }
   }
}

