
// implementation file for class CMAGNIFIER ------------------------------------
#include "Magnifier.h"

#include "Convert.h"
#include "Extractor.h"
#include "ImageFormat3.h"
#include "IniFile.h"

CMagnifier::CMagnifier()
{
   // allocate a converter for the magnifier
   cnvt = new CConvert;

   // allocate an extractor for the intermediate data
   extr = new CExtractor;

   // no magnifier image format
   magFmt = NULL;

   // no auxiliary bitmap
   auxMagFB = NULL;

   // no frame buffer
   magFB = NULL;
   magBMWdth = 0;
   magBMHght = 0;

   // no auxiliary frame buffer
   auxMagFB = NULL;

   // no intermediate frame buffer
   intFB = NULL;

   // no intermediate auxiliary bitmap
   auxIntFB = NULL;

   // magnifier inactive
   magnifierCallback = NULL;
   magnifierCallbackObj = NULL;

   // no rotation
  rotateBM = false;

   // magnifier is visible
   visible = true;

   // Presume not doing autoclean
   autoCleanHackFlag = false;

   // no active rectangle
   actWin.left = 0;
   actWin.top = 0;
   actWin.right = 0;
   actWin.bottom = 0;

   // no magnifier rectangle
   magWin.left = 0;
   magWin.top = 0;
   magWin.right = 0;
   magWin.bottom = 0;
}

CMagnifier::~CMagnifier()
{
   // delete the converter
   delete cnvt;

   // delete the extractor
   delete extr;

   if (magFB != NULL)
      delete [] magFB;

   // if there's an auxiliary bitmap, delete it
   if (auxMagFB != NULL)
      delete [] auxMagFB;

   // if there's an intermediate bitmap, delete it
   if (intFB != NULL)
      delete [] intFB;

   // if there's an intermediate auxiliary bitmap, delete it
   if (auxIntFB != NULL)
      delete [] auxIntFB;
}

int CMagnifier::initMagnifier(int bmwdth, int bmhght,
                              const CImageFormat *magfmt,
                              RECT *magwin)
//
//  bmwdth      is the width  of the resultant bitmap
//  bmhght      is the height of the resultant bitmap
//  magfmt      is the image format of the loaded clip
//  magwin      is the window, in image coordinates, to be magnified. Its
//              aspect ratio must be either EQUAL to that of the resultant
//              bitmap, or RECIPROCAL to that of the resultant bitmap. In
//              the latter case the coordinate system of MAGWIN is rotated
//              90 degrees from that of the bitmap.
//
{
   ////////////////////////////
   // THESE ARE NOT REUSABLE!!
   MTIassert(magFB == NULL);
   ////////////////////////////

   unsigned int *fill;
   unsigned int pels;

   // set the bitmap dimensions
   magBMWdth = bmwdth;
   magBMHght = bmhght;

   // set the total number of pels
   pels = magBMWdth*magBMHght;

   // set the image format
   magFmt = magfmt;

   // if it's non-null, set the active window
   if (magFmt != NULL)
      actWin = magFmt->getActivePictureRect();

   // init the magnifier window
   int retVal = setMagnifierWindow(magwin);
   if (retVal == -1)
      return retVal;

   // Allocate space for the magnifier frame buffer "bitmap"
   magFB = new unsigned int[pels];

   // zero the new bitmap
   fill = magFB;
   memset(fill,0,pels<<2);

   // create the auxiliary bitmap frame buffer
   auxMagFB = new unsigned int[pels];

   // zero the aux bitmap
   fill = auxMagFB;
   memset(fill,0,pels<<2);
   
   // create the intermediate bitmap frame buffer
   intFB = new MTI_UINT16[3*pels];

   // create the auxiliary intermediate bitmap frame buffer
   auxIntFB = new MTI_UINT16[3*pels];

   // success
   return 0;
}

// activate the magnifier by supplying a refresh callback vector
void CMagnifier::activateMagnifier(
     void (*magcallback)(void *obj, void *mag),
     void *strobj)
{
   magnifierCallback = magcallback;
   magnifierCallbackObj = strobj;
}

// deactivate the magnifier by nulling out the callback vector
void CMagnifier::deactivateMagnifier()
{
   magnifierCallback = NULL;
   magnifierCallbackObj = NULL;
}

// TRUE if magnifier active, FALSE if inactive
bool CMagnifier::isActive()
{
   if (magnifierCallback == NULL)
      return false;
   return true;
}

void CMagnifier::show()
{
   visible = true;
}

void CMagnifier::hide()
{
   visible = false;
}

bool CMagnifier::isVisible()
{
   return visible;
}

// refresh the magnifier bitmap by drawing from the current
// MAGWIN into the (entire) bitmap. Call the active callback
// vector, passing it the ptr to the magnifier.
void CMagnifier::refreshMagnifier(unsigned char **fld)
{
   // if (magFmt == NULL) return; // should never happen...

   // Stupid frickin' off by ones!!  QQQ
   RECT hackWin = magWin;
   if (!rotateBM) { // orientation same - go direct to mag bitmap

      // convert YUV to RGB, generating a new bitmap
      TRACE_3(errout << "CONVERT 2");
      cnvt->convertWtf( fld,
                     magFmt,
                     &hackWin,
                     (unsigned int *)magFB,
                     magBMWdth,
                     magBMHght,
                     magBMWdth*4 );

      // call the extractor and load the intermediate
      // data into the intemediate frame buffer
      extr->extractSubregion(
		       intFB,     // array of 16-bit "444 data" (YUV or RGB)
		       fld,       // -> source field(s) (YUV422 or RGB variant)
		       magFmt,    // image format of source frame
		       &hackWin);  // region to be extracted
   }
   else { // orientation 90 deg - go to aux bitmap first, then rotate

      // convert YUV to RGB into the aux bitmap
      TRACE_3(errout << "CONVERT 3");
      cnvt->convertWtf( fld,
                     magFmt,
                     &hackWin,
                     (unsigned int *)auxMagFB,
                     magBMHght,
                     magBMWdth,
                     magBMHght*4 );


      // rotate aux bitmap 90 deg into mag bitmap
      unsigned int *dst = magFB;
      for (int i = magBMHght - 1; i >=0 ; i--)
      {
         unsigned int *col = auxMagFB + i;
         for (int j = 0; j < magBMWdth * magBMHght ; j += magBMHght)
         {
            *dst++ = col[j];
         }
      }

      // call the extractor and load the intermediate
      // data into the intermediate frame buffer

      extr->extractSubregion(
		       auxIntFB,  // array of 16-bit "444 data" (YUV or RGB)
		       fld,       // -> source field(s) (YUV422 or RGB variant)
		       magFmt,    // image format of source frame
		       &hackWin);  // region to be extracted

      // rotate aux int bitmap 90 deg into int bitmap
      int wd = hackWin.right - hackWin.left + 1;
      int ht = hackWin.bottom - hackWin.top + 1;;
      MTI_UINT16 *out = intFB;
      int pitch = wd*3;
      for (int i=(pitch-3);i>=0;i-=3) {
         MTI_UINT16 *col = auxIntFB + i;
         for (int j=0;j<ht*pitch;j+=pitch) {
            *out++ = col[j  ];
            *out++ = col[j+1];
            *out++ = col[j+2];
         }
      }
   }

   // now call the code which will display the new bitmap
   // we pass back the address of the object where we act-
   // ivated the magnifier, and the address of the magnifier
   // itself
   (*magnifierCallback)(magnifierCallbackObj, this);
}

// give the magnifier a new MAGFMT and reset the window
void CMagnifier::setMagnifierImageFormat(const CImageFormat *magfmt)
{
   // the format pointer
   magFmt = magfmt;

   // set the limits of the active rectangle
   actWin = magFmt->getActivePictureRect();

   // home the magnifier window without
   // changing its dimensions
   int wd = magWin.right - magWin.left + 1;
   int ht = magWin.bottom - magWin.top + 1;
   magWin.left   = 0;
   magWin.top    = 0;
   magWin.right  = wd - 1;
   magWin.bottom = ht - 1;
}

// give the magnifier a new MAGWIN
int CMagnifier::setMagnifierWindow(RECT *magwin)
{
   magWin = *magwin;

   int wd = magWin.right - magWin.left + 1;
   int ht = magWin.bottom - magWin.top + 1;

   if ((wd <= 0) || (ht <= 0))
   {
      return -1;
   }

   // compare bitmap and window aspect ratios
   if ((magBMWdth * ht == magBMHght * wd))
   {
      // window oriented same as bitmap
      rotateBM = false;
   }
   else if ((magBMWdth * wd == magBMHght * ht))
   {
      // window oriented 90 deg to bitmp
      rotateBM = true;
   }
   else
   {
      // window and bitmap aspect ratios don't match
      return -1;
   }

   // now the window may be inited before a valid image format
   // is available. In that case the image format will be NULL,
   // and we can go ahead and set up a magnifier window which is
   // valid for any active picture rectangle. If we do have an
   // image format at this point, we use the active picture rect
   // from it
   if (magFmt == NULL)
   {
      // this is OK irrespective of active rect
      magWin.left   = 0;
      magWin.right  = wd - 1;
      magWin.top    = 0;
      magWin.bottom = ht - 1;
   }
   else
   {
      // force window to lie within active picture rect
      if (magWin.left < actWin.left)
      {
         magWin.left = actWin.left;
         magWin.right = magWin.left + wd - 1;
      }
      if (magWin.top < actWin.top)
      {
         magWin.top = actWin.top;
         magWin.bottom = magWin.top + ht - 1;
      }
      if (magWin.right > actWin.right)
      {
         magWin.right = actWin.right;
         magWin.left = magWin.right - wd + 1;
      }
      if (magWin.bottom > actWin.bottom)
      {
         magWin.bottom = actWin.bottom;
         magWin.top = magWin.bottom - ht + 1;
      }
   }

   return 0;
}

// re-center the magnifier's MAGWIN
void CMagnifier::recenterMagnifierWindow(int x, int y)
{
   int wd = magWin.right - magWin.left + 1;
   int ht = magWin.bottom - magWin.top + 1;

   /////////////////////////////////////////////////////////////////////
   // NOTE: If the width of the window is ODD, add the odd pixel to
   // the RIGHT side; similarly add the odd pixel to the BOTTOM if the
   // height is odd! This code used to make sure thye left edge of the
   // magnifier starts on an EVEN COLUMN, probably due to some
   // vestigial windows bitmap crap that I removed.
   /////////////////////////////////////////////////////////////////////

   magWin.left = x - wd/2;
   magWin.right = magWin.left + wd - 1;

   magWin.top  = y - ht/2;
   magWin.bottom = magWin.top + ht - 1;

   ///////////// HACK //////////// HACK ////////////////HACK //////////////
   // AutoClean wants to let the magnifier partially leave the active area
   // else we get fucked up trying to center the magnifier over debris near
   // the edge. Of course we have to make sure the magnifier shows blackness
   // for the off-screen portion
   ///////////// HACK //////////// HACK ////////////////HACK //////////////

   if (!autoCleanHackFlag)
   {
      if (magWin.left < actWin.left)
      {
         magWin.left = actWin.left;
         magWin.right = magWin.left + wd - 1;
      }
      if (magWin.top < actWin.top)
      {
         magWin.top = actWin.top;
         magWin.bottom = magWin.top + ht - 1;
      }
      if (magWin.right > actWin.right)
      {
         magWin.right = actWin.right;
         magWin.left = magWin.right - wd + 1;
      }
      if (magWin.bottom > actWin.bottom)
      {
         magWin.bottom = actWin.bottom;
         magWin.top = magWin.bottom - ht + 1;
      }
   }
}

// get the current MAGWIN
RECT CMagnifier::getMagnifierWindow()
{
   return magWin;
}

// Get coordinates of the center of the magnifier window
void CMagnifier::getPosition(int &x, int &y)
{
   x = (magWin.left + magWin.right) / 2;
   y = (magWin.top + magWin.bottom) / 2;
}

// Get dimensions of the current image format
RECT CMagnifier::getImageRect()
{
   return actWin;
}

// bitmap dimensions
int CMagnifier::getMagnifierBitmapWdth()
{
   return magBMWdth;
}

int CMagnifier::getMagnifierBitmapHght()
{
   return magBMHght;
}

// get the address of the magnifier bitmap in memory (the
// SGI uses this to call DisplayPixels to display the
// magnifier bitmap
unsigned int * CMagnifier::getMagnifierFrameBuffer()
{
   return magFB;
}

// get the address of the intermediate bitmap in memory.
// This is used to generate the (accurate) Y, R, G, & B
// oscillographs
MTI_UINT16 * CMagnifier::getIntermediateFrameBuffer()
{
   return(intFB);
}

// a hack flag for autoclean because there is no time to do things right!
void CMagnifier::setAutoCleanHackFlag()
{
   autoCleanHackFlag = true;
}

bool CMagnifier::getAutoCleanHackFlag()
{
   return autoCleanHackFlag;
}

RECT CMagnifier::getDisplayRect()
{
   RECT displayRect = magWin;;

   if (autoCleanHackFlag)
   {
      // Compute outer dimensions of the square white cursor for autoclean mode
      const int minRadius = 24;
      const int minMargin = 6;

      int cursorRadius = minRadius;

      int magWidth = ((magWin.right + 1) - magWin.left);
      int magHeight = ((magWin.bottom + 1) - magWin.top);
      int magMaxRadius = (magWidth > magHeight)? (magWidth / 2) : (magHeight / 2);
      int magCenterX = magWin.left + magWidth / 2;
      int magCenterY = magWin.top + magHeight / 2;

      if (magMaxRadius > (cursorRadius - minMargin))
         cursorRadius = magMaxRadius + minMargin;

      int cursorWidth = cursorRadius * 2 + (magWidth % 2);

      displayRect.left = magCenterX - cursorRadius;
      displayRect.right = displayRect.left + cursorWidth - 1;
      displayRect.top = magCenterY - cursorRadius;
      displayRect.bottom = displayRect.top + cursorWidth - 1;
   }

   return displayRect;
}




