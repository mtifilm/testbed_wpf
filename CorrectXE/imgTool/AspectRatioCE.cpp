/*
	File:	AspectRatioCE.h
	By:	Kevin Manbeck
	Date:	Apr 9, 2004

	AspectRatioCE is used to convert anamorphic images into
	their center extracted version.  The conversion must be very
	efficient so that it can be done in real time.
*/

#include "AspectRatioCE.h"
#include "AspectRatio.h"
#include "ImageFormat3.h"
#include "AspectRatio.h"
#include "IniFile.h"      // for TRACE_
#include "err_imgtool.h"

CAspectRatioCE::CAspectRatioCE()
{
  arp = 0;
}

CAspectRatioCE::~CAspectRatioCE()
{
  Free();
}

void CAspectRatioCE::Free()
{
  delete arp;
  arp = 0;
}

int CAspectRatioCE::Init (const CImageFormat *ifp, int iQuality)
{
  int iRet;
  float fZoom;
  int iSize;
  bool bHoriz;

  // free storage from a previous run
  Free ();

  // a center extract keeps the vertical dimension constant and zooms
  // the horizontal by 4/3
  fZoom = 4./3.;
  iSize = ifp->getPixelsPerLine();
  bHoriz = true;

  // allocate a CAspectRatio
  arp = new CAspectRatio;

  // initialize the CAspectRatio
  iRet = arp->Init (ifp, ifp);
  if (iRet)
   {
    Free ();
    return iRet;
   }

  // make the filter
  iRet = arp->MakeFilter (fZoom, iQuality, 10);
  if (iRet)
   {
    Free ();
    return iRet;
   }

  // make the spatial LUT
  iRet = arp->MakeSpatial (iSize, bHoriz);
  if (iRet)
   {
    Free ();
    return iRet;
   }

  // the pixel aspect ratio is a = (4 * image_height) / (3 * image_width).
  // When doing a matte, the new height = (image_width*a) / (matte_ratio)
  // or new height = (4/3)*image_height / matte_ratio.  Define fMatteFactor
  // to be (4/3)*image_height.

  arp->fRowMatteFactor = (4. * (float)ifp->getLinesPerFrame()) / 3.;

  // when matte_ratio < 4/3, we use the formula (3/4)*image_width

  arp->fColMatteFactor = (3.* (float)ifp->getPixelsPerLine()) / 4.;

  
  return 0;
}


int CAspectRatioCE::Render (MTI_UINT8 *ucpSrc[2], MTI_UINT8 *ucpDst[2])
{
  int iRet = 0;

  if (arp == 0)
   {
    return ERR_ASPECT_RATIO_INIT;
   }

  iRet = arp->Render (ucpSrc, ucpDst);
  if (iRet)
    return iRet;
 
  return iRet;
}

int CAspectRatioCE::getMatteCoordinates (int iJustify, float fMatteRatio,
	int &iRow0, int &iRow1, int &iCol0, int &iCol1)
{

  iRow0 = 0;
  iRow1 = 0;
  iCol0 = 0;
  iCol1 = 0;

  if (arp == 0)
   {
    return ERR_ASPECT_RATIO_INIT;
   }

  if (fMatteRatio >= 1.333333)
   {
    int iNewRow = arp->fRowMatteFactor / fMatteRatio + .5;

    if (iJustify == -1)
     {
      // top justify
      iRow0 = arp->iRowStart;
     }
    else if (iJustify == 0)
     {
      // center
      iRow0 = arp->iRowStart + (arp->iRowStop - arp->iRowStart - iNewRow)/2;
     }
    else
     {
      // bottom align
      iRow0 = arp->iRowStop - iNewRow;
     }
    iRow1 = iRow0 + iNewRow;
   }
  else if (fMatteRatio > 0.)
   {
    int iNewCol = arp->fColMatteFactor * fMatteRatio + .5;

    // always perform a center justification for side mattes
    iCol0 = arp->iColStart + (arp->iColStop - arp->iColStart - iNewCol)/2;
    iCol1 = iCol0 + iNewCol;
   }
  else
   {
    // no mattes when ratio is zero
   }


  return 0;
}
