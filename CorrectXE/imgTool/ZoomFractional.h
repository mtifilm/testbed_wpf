#ifndef ZOOM_FRACTIONAL
#define ZOOM_FRACTIONAL

#include "machine.h"
#include "mthread.h"
#include "imgToolDLL.h"

class CImageFormat;

// the number of taps is the same as MAX_WEIGHTS
//
#define MAX_WEIGHTS  5
#define MIDPT        2
#define SUBDIVS     20

// wgts are 10-bit integers
//

struct KCoeff
{
   int wgt[MAX_WEIGHTS];
};


class MTI_IMGTOOLDLL_API CZoomFractional
{
public:

   CZoomFractional(int numStripes = 1);

   ~CZoomFractional();

#define ZOOMFRACTIONAL_ERROR_INVALID_COMPONENT_WDTH -1
#define ZOOMFRACTIONAL_ERROR_INVALID_KERNEL_SIZE    -2
#define ZOOMFRACTIONAL_ERROR_INVALID_FORMAT         -3
#define ZOOMFRACTIONAL_ERROR_INVALID_SRC_RECTANGLE  -4
#define ZOOMFRACTIONAL_ERROR_INVALID_DST_RECTANGLE  -5
#define ZOOMFRACTIONAL_ERROR_INSUFFICIENT_MEMORY    -6

   int initialize(const CImageFormat *srcFmt,
                  int horkern, int vrtkern,
                  unsigned char matred = 128,
                  unsigned char matgrn = 128,
                  unsigned char matblu = 128);

   int convert(DRECT *srcRect, RECT *dstRect, MTI_UINT16 *srcBuf, MTI_UINT16 *dstBuf);

private:

   void drawRectangle1(int left, int top, int right, int bottom);
   void drawRectangle3(int left, int top, int right, int bottom);

   void drawMat1();
   void drawMat3();

   static void null(void *v, int iJob);
   static void unpack1(void *v, int iJob);
   static void unpack3(void *v, int iJob);
   static void horizontalDownconvert1(void *v, int iJob);
   static void horizontalDownconvert3(void *v, int iJob);
   static void verticalDownconvert1(void *v, int iJob);
   static void verticalDownconvert3(void *v, int iJob);

   double Kernel(double, int, int, int);

   /////////////////////////////////////////////////////////////////////////////

private:

   int wgtone;
   int wgtshft;
   int maxcomponent;

   MTI_UINT16 *srcbuf;    // source buffer of 16-bit intermediates

   int pitch1;            // pitch is 3*frmwdth + 6*KERNEL
   MTI_UINT16 *frmBuf1;   // height is frmhght

   int horzkern;          // horz kernel size
   KCoeff *hkern;         // horz kernel coeffs
   KCoeff **hTbl;         // wgts selector tbl ?
   int *hctr;             // center pos table

   int vertkern;          // vert  kernel size
   KCoeff *vkern;         // vert kernel coeffs
   KCoeff **vTbl;         // wgts selector tbl ?
   int *vctr;             // center pos table

   int pitch2;            // pitch is frmhght + 2*KERNEL
   int hght2;
   MTI_INT16 *frmBuf2;    // width is frmwdth

   MTI_UINT16 *dstbuf;    // destination buffer
   int         dstoff;    // offset within target buffer of (L,T) dest

   int frmwdth;
   int frmhght;
   int frmpitch;
   int frmpelpacking;
   int frmpelcomponents;

   DRECT srcrect;
   double srcwdth;
   double srchght;

   RECT dstrect;
   int dstwdth;
   int dsthght;

   unsigned short matLum;

   unsigned short matRed,
                  matGrn,
                  matBlu;

   /////////////////////////////////////////////////////////////////////////////

   int nStripes; // number of stripes in frame 1,2,4 etc.

   MTHREAD_STRUCT unpackThread;

   MTHREAD_STRUCT horzThread;

   MTHREAD_STRUCT vertThread;

private:

};
#endif






