//
// include file for class CHDProxyConvert
//
#ifndef HDPROXYCONVERT_H
#define HDPROXYCONVERT_H

#include "machine.h"
#include "imgToolDLL.h"
#include "mthread.h"
#include "ImageFormat3.h"
#include "ImageInfo.h"
#include <math.h>

#define BITS8
//#define BITS16

#ifdef BITS8
#define WGTMAX 255.
#define WGTSHFT8   8
#define WGTSHFT10 10
typedef unsigned char WGT;
#endif
#ifdef BITS16
#define WGTMAX 65535.
#define WGTSHFT8  16
#define WGTSHFT10 18
typedef unsigned short WGT;
#endif

class CLineEngine;

struct PXLPAIRENTRY
{
   int offsetlo;
   int offsethi;

   int offsetu;
   int offsety0;
   int offsetv;
   int offsety1;

   unsigned char fldlo;
   unsigned char fldhi;

   unsigned char phaseu;
   unsigned char phasey0;
   unsigned char phasev;
   unsigned char phasey1;

   WGT uwgt0;
   WGT uwgt1;
   WGT uwgt2;
   WGT uwgt3;

   WGT y0wgt0;
   WGT y0wgt1;
   WGT y0wgt2;
   WGT y0wgt3;

   WGT vwgt0;
   WGT vwgt1;
   WGT vwgt2;
   WGT vwgt3;

   WGT y1wgt0;
   WGT y1wgt1;
   WGT y1wgt2;
   WGT y1wgt3;
};

class MTI_IMGTOOLDLL_API CHDProxyConvert
{

public:

   CHDProxyConvert(int numStripes = 0);

   ~CHDProxyConvert();

   int initDimensions(RECT *SDDstRect, const CImageFormat *SDFmt,
                      RECT *HDSrcRect, const CImageFormat *HDFmt);

   void generateProxy(unsigned char **sdFld, unsigned char **hdFld);

private:

   CLineEngine *matteEng;       // line engine to blacken the matte

   int nStripes;                // no of stripes in dest rectangle

   MTHREAD_STRUCT tsThread;     // struct for multithreading

   PXLPAIRENTRY *genTbl;        // proxy generator table

   int hdWidth;
   EPixelComponents hdPelComponents;
   EPixelPacking hdPelPacking;

   RECT hdRect;

   int hdPitch;

   int hdWdth,
       hdHght;

   int sdWidth;
   int sdHeight;
   EPixelComponents sdPelComponents;
   EPixelPacking sdPelPacking;

   RECT sdRect;

   int sdPitch;

   int sdWdth,
       sdHght;

   unsigned char *sdField[2];

   unsigned char *hdField[2];

   unsigned char pelscl[256];

   friend void genstripe8to8(  void *v, int iJob);
   friend void genstripe10to8( void *v, int iJob);
   friend void genstripe10to10(void *v, int iJob);
};

#endif
