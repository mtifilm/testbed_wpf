//
// implementation file for class CHDProxyConvert
//
#include "HDProxyConvert.h"
#include "LineEngine.h"
#include "IniFile.h"

static void null(void *v, int iJob);
// GCC doesn't allow a friend (which implies extern) to be static;
// some on comp.lang.c++.moderated seem to agree that you can't give a
// function two linkages.  Nevertheless, it's a good intent: these
// three functions are not meant to be called from outside this unit.
/* static */ void genstripe8to8(  void *v, int iJob);
/* static */ void genstripe10to8( void *v, int iJob);
/* static */ void genstripe10to10(void *v, int iJob);

////////////////////////////////////////////////////////////////////
//
//    C O N S T R U C T O R
//
CHDProxyConvert::
CHDProxyConvert(int numStripes)
: nStripes(numStripes)
{
   // our Dell 530's, 650's, & 4600's have 2 processors
   if (nStripes != 1 && nStripes != 2 && nStripes != 4
   && nStripes != 8 && nStripes != 16) {
      nStripes = 1;
   }

   tsThread.PrimaryFunction   = (void (*)(void *,int))&null;
   tsThread.SecondaryFunction = NULL;
   tsThread.CallBackFunction  = NULL;
   tsThread.vpApplicationData = this;
   tsThread.iNThread          = nStripes;
   tsThread.iNJob             = nStripes;

   // allocate the mthreads
   if (nStripes > 1) {
      int iRet = MThreadAlloc(&tsThread);
      if (iRet) {
         TRACE_0(errout << "Convert: MThreadAlloc failed, iRet=" << iRet);
         unexpected ();
      }
   }

   // no control table yet
   genTbl = NULL;

   // no matte engine yet
   matteEng = NULL;
}

/////////////////////////////////////////////////////////////////////
//
//    D E S T R U C T O R
//
CHDProxyConvert::
~CHDProxyConvert()
{
   if (nStripes > 1) {
      MThreadFree(&tsThread);
   }

   delete [] genTbl;

   delete matteEng;
}

/////////////////////////////////////////////////////////////////////
//
//    I N I T D I M E N S I O N S
//
int CHDProxyConvert::
initDimensions(RECT *sdrct, const CImageFormat *sdfmt,
               RECT *hdrct, const CImageFormat *hdfmt)
//
// pitch arguments are in pixels - 720 for SD, 1920 for HD
//
{
   hdWidth         = hdfmt->getTotalFrameWidth();
   hdPelComponents = hdfmt->getPixelComponents();
   hdPelPacking    = hdfmt->getPixelPacking();

   sdWidth         = sdfmt->getTotalFrameWidth();
   sdHeight        = sdfmt->getTotalFrameHeight();
   sdPelComponents = sdfmt->getPixelComponents();
   sdPelPacking    = sdfmt->getPixelPacking();

   if ((hdPelComponents != IF_PIXEL_COMPONENTS_YUV422)||
       (sdPelComponents != IF_PIXEL_COMPONENTS_YUV422))return(-1);
   if ((hdPelPacking != IF_PIXEL_PACKING_8Bits_IN_1Byte)&&
       (hdPelPacking != IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS))
      return(-1);
   if ((sdPelPacking != IF_PIXEL_PACKING_8Bits_IN_1Byte)&&
       (sdPelPacking != IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS))
      return(-1);

   delete [] genTbl;

   // even num of pixel pairs in row
   if (hdrct->left & 1)     return(-1);
   if (!(hdrct->right & 1)) return(-1);

   hdRect = *hdrct;

   hdWdth = hdRect.right - hdRect.left + 1;
   hdHght = hdRect.bottom - hdRect.top + 1;

   // even num of pixel pairs in row
   if (sdrct->left & 1)     return(-1);
   if (!(sdrct->right & 1)) return(-1);

   sdRect = *sdrct;

   sdWdth = sdRect.right - sdRect.left + 1;
   sdHght = sdRect.bottom - sdRect.top + 1;

   // mapping from SD frame to HD frame
   double coldel = (double) hdWdth / sdWdth;
   double rowdel = (double) hdHght / sdHght;

   PXLPAIRENTRY *dst;
   double y, yadv;
   int yint;
   double x, y0wgt, uwgt, y1wgt, vwgt;
   int xint, uint, vint;

   switch(hdPelPacking) {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:

         tsThread.PrimaryFunction = &genstripe8to8;

         sdPitch = (sdWidth/2)*4;  // pitch in bytes
         hdPitch = (hdWidth/2)*4;

         // build the generation table

         genTbl = new PXLPAIRENTRY[sdHght*(sdWdth/2)];
         dst = genTbl;

         for (int j=0; j<sdHght; j++) {

            y = hdRect.top + rowdel * (j + 0.5) - 0.5;
            yint = y;
            yadv = (y - yint);

            for (int i=0; i<sdWdth; i+=2) { // one pixel pair

               dst->fldlo = yint&1;
               dst->offsetlo = (yint/2)*hdPitch;

               dst->fldhi = (yint+1)&1;
               dst->offsethi = ((yint+1)/2)*hdPitch;


               // entries for UY
               x = hdRect.left + coldel * (i + 0.5) - 0.5;

               xint = x;

               uint = xint & 0xfffffffe;

               dst->offsetu = (uint/2)*4;
               dst->phaseu  = 0;
               dst->offsety0 = (xint/2)*4;
               dst->phasey0 = xint%2;

               uwgt = (x - uint)/2.;

               dst->uwgt0 = (1.0 - yadv)*(1.0 - uwgt)*WGTMAX;
               dst->uwgt1 = (1.0 - yadv)*uwgt*WGTMAX;
               dst->uwgt2 = yadv * (1.0 - uwgt)*WGTMAX;
               dst->uwgt3 = WGTMAX - dst->uwgt0 - dst->uwgt1 - dst->uwgt2;

               y0wgt = (x - xint);

               dst->y0wgt0 = (1.0 - yadv) * (1.0 - y0wgt)*WGTMAX;
               dst->y0wgt1 = (1.0 - yadv) * y0wgt*WGTMAX;
               dst->y0wgt2 = yadv * (1.0 - y0wgt)*WGTMAX;
               dst->y0wgt3 = WGTMAX - dst->y0wgt0 - dst->y0wgt1 - dst->y0wgt2;


               // entries for VY
               x += coldel;

               xint = x;

               vint = ((xint+1) & 0xfffffffe) - 1;

               dst->offsetv = (vint/2)*4;
               dst->phasev  = 1;
               dst->offsety1 = (xint/2)*4;
               dst->phasey1 = xint%2;

               vwgt = (x - vint)/2.;

               dst->vwgt0 = (1 - yadv)*(1 - vwgt)*WGTMAX;
               dst->vwgt1 = (1 - yadv)*vwgt*WGTMAX;
               dst->vwgt2 = yadv * (1 - vwgt)*WGTMAX;
               dst->vwgt3 = WGTMAX - dst->vwgt0 - dst->vwgt1 - dst->vwgt2;

               y1wgt = (x - xint);

               dst->y1wgt0 = (1 - yadv) * (1 - y1wgt)*WGTMAX;
               dst->y1wgt1 = (1 - yadv) * y1wgt*WGTMAX;
               dst->y1wgt2 = yadv * (1 - y1wgt)*WGTMAX;
               dst->y1wgt3 = WGTMAX - dst->y1wgt0 - dst->y1wgt1 - dst->y1wgt2;

               dst++; // advance the ptr
            }
         }

      break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:

         hdPitch = (hdWidth/6)*16; // pitch in bytes

         tsThread.PrimaryFunction = &genstripe10to8;
         sdPitch = (sdWidth/2)*4; // pitch in bytes
         if (sdPelPacking == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS) {
            tsThread.PrimaryFunction = &genstripe10to10;
            sdPitch = (sdWidth/6)*16; // pitch in bytes
         }

         // build the generation table

         genTbl = new PXLPAIRENTRY[sdHght*(sdWdth/2)];
         dst = genTbl;

         for (int j=0; j<sdHght; j++) {

            y = hdRect.top + rowdel * (j + 0.5) - 0.5;
            yint = y;
            yadv = (y - yint);

            for (int i=0; i<sdWdth; i+=2) { // one pixel pair

               dst->fldlo = yint&1;
               dst->offsetlo = (yint/2)*hdPitch;

               dst->fldhi = (yint+1)&1;
               dst->offsethi = ((yint+1)/2)*hdPitch;


               // entries for UY
               x = hdRect.left + coldel * (i + 0.5) - 0.5;

               xint = x;

               uint = xint & 0xfffffffe;

               dst->offsetu = (uint/6)*16;
               dst->phaseu  = uint%6;
               dst->offsety0 = (xint/6)*16;
               dst->phasey0 = xint%6;

               uwgt = (x - uint)/2.;

               dst->uwgt0 = (1.0 - yadv)*(1.0 - uwgt)*WGTMAX;
               dst->uwgt1 = (1.0 - yadv)*uwgt*WGTMAX;
               dst->uwgt2 = yadv * (1.0 - uwgt)*WGTMAX;
               dst->uwgt3 = WGTMAX - dst->uwgt0 - dst->uwgt1 - dst->uwgt2;

               y0wgt = (x - xint);

               dst->y0wgt0 = (1.0 - yadv) * (1.0 - y0wgt)*WGTMAX;
               dst->y0wgt1 = (1.0 - yadv) * y0wgt*WGTMAX;
               dst->y0wgt2 = yadv * (1.0 - y0wgt)*WGTMAX;
               dst->y0wgt3 = WGTMAX - dst->y0wgt0 - dst->y0wgt1 - dst->y0wgt2;


               // entries for VY
               x += coldel;

               xint = x;

               vint = ((xint+1) & 0xfffffffe) - 1;

               dst->offsetv = (vint/6)*16;
               dst->phasev = vint%6;
               dst->offsety1 = (xint/6)*16;
               dst->phasey1 = xint%6;

               vwgt = (x - vint)/2.;

               dst->vwgt0 = (1 - yadv)*(1 - vwgt)*WGTMAX;
               dst->vwgt1 = (1 - yadv)*vwgt*WGTMAX;
               dst->vwgt2 = yadv * (1 - vwgt)*WGTMAX;
               dst->vwgt3 = WGTMAX - dst->vwgt0 - dst->vwgt1 - dst->vwgt2;

               y1wgt = (x - xint);

               dst->y1wgt0 = (1 - yadv) * (1 - y1wgt)*WGTMAX;
               dst->y1wgt1 = (1 - yadv) * y1wgt*WGTMAX;
               dst->y1wgt2 = yadv * (1 - y1wgt)*WGTMAX;
               dst->y1wgt3 = WGTMAX - dst->y1wgt0 - dst->y1wgt1 - dst->y1wgt2;

               dst++; // advance the ptr
            }
         }

      break;

	  default:
	  	break;
   }

   // the genTbl is done, now make a line
   // engine for blackening out the matte
   CLineEngineFactory matteFactory;
   matteEng = matteFactory.makePixelEng(*sdfmt);

   int pitch;

   switch(sdPelPacking) {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:

         pitch = (sdWidth/2)*4;

      break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:

         pitch = (sdWidth/6)*16;

	  break;

	  default:
		break;
   }

   // set the dimensions of the (SD) output
   matteEng->setFrameBufferDimensions(sdWidth, sdHeight, pitch, true);

   // the matte will be black - (but color could be an argument)
   matteEng->setFGColor(0);

   return(0); // success
}

void CHDProxyConvert::
generateProxy(unsigned char **sdfld, unsigned char **hdfld)
{
   sdField[0] = sdfld[0];
   sdField[1] = sdfld[1];

   hdField[0] = hdfld[0];
   hdField[1] = hdfld[1];


   if (nStripes == 1) {

      // call the function directly
      tsThread.PrimaryFunction(this,0);
   }
   else {

      // fire up one thread for each stripe
      MThreadStart(&tsThread);
   }

   // now pass the fields to the line engine
   // and draw the (four) matte rectangles
   matteEng->setExternalFrameBuffer(sdfld);

   if (sdRect.top > 0)
      matteEng->drawRectangle(0, 0,
                              (sdWidth-1), (sdRect.top-1));
   if (sdRect.left > 0)
      matteEng->drawRectangle(0, sdRect.top,
                              (sdRect.left-1), sdRect.bottom);

   if (sdRect.right < (sdWidth-1))
      matteEng->drawRectangle((sdRect.right+1), sdRect.top,
                              (sdWidth-1), sdRect.bottom);

   if (sdRect.bottom < (sdHeight-1))
      matteEng->drawRectangle(0, (sdRect.bottom+1),
                              (sdWidth-1), (sdHeight-1));
}

static void null(void *v, int iJob)
{
  return;
}

///////////////////////////////////////////////////////////////

void genstripe8to8(void *v, int iJob)
{
   CHDProxyConvert *vp = (CHDProxyConvert *) v;

   int rowsPerStripe = vp->sdHght / vp->nStripes;

   int begRow = iJob*rowsPerStripe;

   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      stripeRows = vp->sdHght - iJob*rowsPerStripe;

   for (int i=begRow;i<(begRow+stripeRows);i++) {

      PXLPAIRENTRY *row = vp->genTbl + i * (vp->sdWdth/2);

      int y = vp->sdRect.top + i;

      unsigned char *dst = vp->sdField[y&1] + (y/2)*vp->sdPitch + vp->sdRect.left*2;

      unsigned int *srclo, *srchi;

      for (int j=0; j<vp->sdWdth; j+=2, row++) { // for each pixel pair

         srclo = (unsigned int *)(vp->hdField[row->fldlo] + row->offsetlo + row->offsetu);
         srchi = (unsigned int *)(vp->hdField[row->fldhi] + row->offsethi + row->offsetu);

         *dst++  = (((srclo[0]      ) & 0xff)*(int)row->uwgt0 +
                    ((srclo[1]      ) & 0xff)*(int)row->uwgt1 +
                    ((srchi[0]      ) & 0xff)*(int)row->uwgt2 +
                    ((srchi[1]      ) & 0xff)*(int)row->uwgt3)>>WGTSHFT8;

         srclo = (unsigned int *)(vp->hdField[row->fldlo] + row->offsetlo + row->offsety0);
         srchi = (unsigned int *)(vp->hdField[row->fldhi] + row->offsethi + row->offsety0);

         switch(row->phasey0) {

            case 0:

               *dst++ = (((srclo[0] >>  8) & 0xff)*(int)row->y0wgt0 +
                         ((srclo[0] >> 24) & 0xff)*(int)row->y0wgt1 +
                         ((srchi[0] >>  8) & 0xff)*(int)row->y0wgt2 +
                         ((srchi[0] >> 24) & 0xff)*(int)row->y0wgt3)>>WGTSHFT8;
            break;

            case 1:

               *dst++ = (((srclo[0] >> 24) & 0xff)*(int)row->y1wgt0 +
                         ((srclo[1] >>  8) & 0xff)*(int)row->y1wgt1 +
                         ((srchi[0] >> 24) & 0xff)*(int)row->y1wgt2 +
                         ((srchi[1] >>  8) & 0xff)*(int)row->y1wgt3)>>WGTSHFT8;
            break;
         }

         srclo = (unsigned int *)(vp->hdField[row->fldlo] + row->offsetlo + row->offsetv);
         srchi = (unsigned int *)(vp->hdField[row->fldhi] + row->offsethi + row->offsetv);

         *dst++  = (((srclo[0] >> 16) & 0xff)*(int)row->uwgt0 +
                    ((srclo[1] >> 16) & 0xff)*(int)row->uwgt1 +
                    ((srchi[0] >> 16) & 0xff)*(int)row->uwgt2 +
                    ((srchi[1] >> 16) & 0xff)*(int)row->uwgt3)>>WGTSHFT8;

         srclo = (unsigned int *)(vp->hdField[row->fldlo] + row->offsetlo + row->offsety1);
         srchi = (unsigned int *)(vp->hdField[row->fldhi] + row->offsethi + row->offsety1);

         switch(row->phasey1) {

            case 0:

               *dst++ = (((srclo[0] >>  8) & 0xff)*(int)row->y0wgt0 +
                         ((srclo[0] >> 24) & 0xff)*(int)row->y0wgt1 +
                         ((srchi[0] >>  8) & 0xff)*(int)row->y0wgt2 +
                         ((srchi[0] >> 24) & 0xff)*(int)row->y0wgt3)>>WGTSHFT8;
            break;

            case 1:

               *dst++ = (((srclo[0] >> 24) & 0xff)*(int)row->y1wgt0 +
                         ((srclo[1] >>  8) & 0xff)*(int)row->y1wgt1 +
                         ((srchi[0] >> 24) & 0xff)*(int)row->y1wgt2 +
                         ((srchi[1] >>  8) & 0xff)*(int)row->y1wgt3)>>WGTSHFT8;
            break;
         }

      } // for each pixel pair in row

   } // for each row of stripe

}

///////////////////////////////////////////////////////////////

void genstripe10to8(void *v, int iJob)
{
   CHDProxyConvert *vp = (CHDProxyConvert *) v;

   int rowsPerStripe = vp->sdHght / vp->nStripes;

   int begRow = iJob*rowsPerStripe;

   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      stripeRows = vp->sdHght - iJob*rowsPerStripe;

   for (int i=begRow;i<(begRow+stripeRows);i++) {

      PXLPAIRENTRY *row = vp->genTbl + i * (vp->sdWdth/2);

      int y = vp->sdRect.top + i;

      unsigned char *dst = vp->sdField[y&1] + (y/2)*vp->sdPitch + vp->sdRect.left*2;

      unsigned int r0;

      unsigned int *srclo, *srchi;

      for (int j=0; j<vp->sdWdth; j+=2, row++) { // for each pixel pair

         srclo = (unsigned int *)(vp->hdField[row->fldlo] + row->offsetlo + row->offsetu);
         srchi = (unsigned int *)(vp->hdField[row->fldhi] + row->offsethi + row->offsetu);

         switch(row->phaseu) { // based on phase do a U

            case 0:

               *dst++  = (((srclo[0]      ) & 0x3ff)*(int)row->uwgt0 +
                          ((srclo[1] >> 10) & 0x3ff)*(int)row->uwgt1 +
                          ((srchi[0]      ) & 0x3ff)*(int)row->uwgt2 +
                          ((srchi[1] >> 10) & 0x3ff)*(int)row->uwgt3)>>WGTSHFT10;
            break;

            case 2:

               *dst++ = (((srclo[1] >> 10) & 0x3ff)*(int)row->uwgt0 +
                         ((srclo[2] >> 20) & 0x3ff)*(int)row->uwgt1 +
                         ((srchi[1] >> 10) & 0x3ff)*(int)row->uwgt2 +
                         ((srchi[2] >> 20) & 0x3ff)*(int)row->uwgt3)>>WGTSHFT10;
            break;

            case 4:

               *dst++ = (((srclo[2] >> 20) & 0x3ff)*(int)row->uwgt0 +
                         ((srclo[4]      ) & 0x3ff)*(int)row->uwgt1 +
                         ((srchi[2] >> 20) & 0x3ff)*(int)row->uwgt2 +
                         ((srchi[4]      ) & 0x3ff)*(int)row->uwgt3)>>WGTSHFT10;
            break;
         }

         srclo = (unsigned int *)(vp->hdField[row->fldlo] + row->offsetlo + row->offsety0);
         srchi = (unsigned int *)(vp->hdField[row->fldhi] + row->offsethi + row->offsety0);

         switch(row->phasey0) { // based on phase do 1st Y

            case 0:

               *dst++ = (((srclo[0] >> 10) & 0x3ff)*(int)row->y0wgt0 +
                         ((srclo[1]      ) & 0x3ff)*(int)row->y0wgt1 +
                         ((srchi[0] >> 10) & 0x3ff)*(int)row->y0wgt2 +
                         ((srchi[1]      ) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT10;
            break;

            case 1:

               *dst++ = (((srclo[1]      ) & 0x3ff)*(int)row->y0wgt0 +
                         ((srclo[1] >> 20) & 0x3ff)*(int)row->y0wgt1 +
                         ((srchi[1]      ) & 0x3ff)*(int)row->y0wgt2 +
                         ((srchi[1] >> 20) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT10;
            break;

            case 2:

               *dst++ = (((srclo[1] >> 20) & 0x3ff)*(int)row->y0wgt0 +
                         ((srclo[2] >> 10) & 0x3ff)*(int)row->y0wgt1 +
                         ((srchi[1] >> 20) & 0x3ff)*(int)row->y0wgt2 +
                         ((srchi[2] >> 10) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT10;

            break;

            case 3:

               *dst++ = (((srclo[2] >> 10) & 0x3ff)*(int)row->y0wgt0 +
                         ((srclo[3]      ) & 0x3ff)*(int)row->y0wgt1 +
                         ((srchi[2] >> 10) & 0x3ff)*(int)row->y0wgt2 +
                         ((srchi[3]      ) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT10;

            break;

            case 4:

               *dst++ = (((srclo[3]      ) & 0x3ff)*(int)row->y0wgt0 +
                         ((srclo[3] >> 20) & 0x3ff)*(int)row->y0wgt1 +
                         ((srchi[3]      ) & 0x3ff)*(int)row->y0wgt2 +
                         ((srchi[3] >> 20) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT10;
            break;

            case 5:

               *dst++ = (((srclo[3] >> 20) & 0x3ff)*(int)row->y0wgt0 +
                         ((srclo[4] >> 10) & 0x3ff)*(int)row->y0wgt1 +
                         ((srchi[3] >> 20) & 0x3ff)*(int)row->y0wgt2 +
                         ((srchi[4] >> 10) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT10;
            break;
         }

         srclo = (unsigned int *)(vp->hdField[row->fldlo] + row->offsetlo + row->offsetv);
         srchi = (unsigned int *)(vp->hdField[row->fldhi] + row->offsethi + row->offsetv);

         switch(row->phasev) { // based on phase do a V

            case 1:

               *dst++ = (((srclo[0] >> 20) & 0x3ff)*(int)row->vwgt0 +
                         ((srclo[2]      ) & 0x3ff)*(int)row->vwgt1 +
                         ((srchi[0] >> 20) & 0x3ff)*(int)row->vwgt2 +
                         ((srchi[2]      ) & 0x3ff)*(int)row->vwgt3)>>WGTSHFT10;
            break;

            case 3:

               *dst++ = (((srclo[2]      ) & 0x3ff)*(int)row->vwgt0 +
                         ((srclo[3] >> 10) & 0x3ff)*(int)row->vwgt1 +
                         ((srchi[2]      ) & 0x3ff)*(int)row->vwgt2 +
                         ((srchi[3] >> 10) & 0x3ff)*(int)row->vwgt3)>>WGTSHFT10;
            break;

            case 5:

               *dst++ = (((srclo[3] >> 10) & 0x3ff)*(int)row->vwgt0 +
                         ((srclo[4] >> 20) & 0x3ff)*(int)row->vwgt1 +
                         ((srchi[3] >> 10) & 0x3ff)*(int)row->vwgt2 +
                         ((srchi[4] >> 20) & 0x3ff)*(int)row->vwgt3)>>WGTSHFT10;
            break;
         }

         srclo = (unsigned int *)(vp->hdField[row->fldlo] + row->offsetlo + row->offsety1);
         srchi = (unsigned int *)(vp->hdField[row->fldhi] + row->offsethi + row->offsety1);

         switch(row->phasey1) { // based on phase do 2nd Y

            case 0:

               *dst++ = (((srclo[0] >> 10) & 0x3ff)*(int)row->y0wgt0 +
                         ((srclo[1]      ) & 0x3ff)*(int)row->y0wgt1 +
                         ((srchi[0] >> 10) & 0x3ff)*(int)row->y0wgt2 +
                         ((srchi[1]      ) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT10;
            break;

            case 1:

               *dst++ = (((srclo[1]      ) & 0x3ff)*(int)row->y0wgt0 +
                         ((srclo[1] >> 20) & 0x3ff)*(int)row->y0wgt1 +
                         ((srchi[1]      ) & 0x3ff)*(int)row->y0wgt2 +
                         ((srchi[1] >> 20) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT10;
            break;

            case 2:

               *dst++ = (((srclo[1] >> 20) & 0x3ff)*(int)row->y0wgt0 +
                         ((srclo[2] >> 10) & 0x3ff)*(int)row->y0wgt1 +
                         ((srchi[1] >> 20) & 0x3ff)*(int)row->y0wgt2 +
                         ((srchi[2] >> 10) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT10;

            break;

            case 3:

               *dst++ = (((srclo[2] >> 10) & 0x3ff)*(int)row->y0wgt0 +
                         ((srclo[3]      ) & 0x3ff)*(int)row->y0wgt1 +
                         ((srchi[2] >> 10) & 0x3ff)*(int)row->y0wgt2 +
                         ((srchi[3]      ) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT10;

            break;

            case 4:

               *dst++ = (((srclo[3]      ) & 0x3ff)*(int)row->y0wgt0 +
                         ((srclo[3] >> 20) & 0x3ff)*(int)row->y0wgt1 +
                         ((srchi[3]      ) & 0x3ff)*(int)row->y0wgt2 +
                         ((srchi[3] >> 20) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT10;
            break;

            case 5:

               *dst++ = (((srclo[3] >> 20) & 0x3ff)*(int)row->y0wgt0 +
                         ((srclo[4] >> 10) & 0x3ff)*(int)row->y0wgt1 +
                         ((srchi[3] >> 20) & 0x3ff)*(int)row->y0wgt2 +
                         ((srchi[4] >> 10) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT10;
            break;
         }

      } // for each pixel pair in row

   } // for each row of stripe

}


///////////////////////////////////////////////////////////////

void genstripe10to10(void *v, int iJob)
{
   CHDProxyConvert *vp = (CHDProxyConvert *) v;

   int rowsPerStripe = vp->sdHght / vp->nStripes;

   int begRow = iJob*rowsPerStripe;

   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      stripeRows = vp->sdHght - iJob*rowsPerStripe;

   for (int i=begRow;i<(begRow+stripeRows);i++) {

      PXLPAIRENTRY *row = vp->genTbl + i * (vp->sdWdth/2);

      int y = vp->sdRect.top + i;

      unsigned char *dst = vp->sdField[y&1] + (y/2)*vp->sdPitch + ((vp->sdRect.left/6)<<4);
      unsigned int phase = vp->sdRect.left % 6;

      unsigned int ru, ry0, rv, ry1;

      unsigned int *srclo, *srchi;

      int pelprs = vp->sdWdth/2;

      if (phase == 0) goto p0;
      if (phase == 2) goto p2;
      goto p4;

      while(true) {

         p0:;

            srclo = (unsigned int *)(vp->hdField[row->fldlo] + row->offsetlo + row->offsetu);
            srchi = (unsigned int *)(vp->hdField[row->fldhi] + row->offsethi + row->offsetu);

            switch(row->phaseu) { // based on phase do a U

               case 0:

                  ru      = (((srclo[0]      ) & 0x3ff)*(int)row->uwgt0 +
                             ((srclo[1] >> 10) & 0x3ff)*(int)row->uwgt1 +
                             ((srchi[0]      ) & 0x3ff)*(int)row->uwgt2 +
                             ((srchi[1] >> 10) & 0x3ff)*(int)row->uwgt3)>>WGTSHFT8;
               break;

               case 2:

                  ru     = (((srclo[1] >> 10) & 0x3ff)*(int)row->uwgt0 +
                            ((srclo[2] >> 20) & 0x3ff)*(int)row->uwgt1 +
                            ((srchi[1] >> 10) & 0x3ff)*(int)row->uwgt2 +
                            ((srchi[2] >> 20) & 0x3ff)*(int)row->uwgt3)>>WGTSHFT8;
               break;

               case 4:

                  ru     = (((srclo[2] >> 20) & 0x3ff)*(int)row->uwgt0 +
                            ((srclo[4]      ) & 0x3ff)*(int)row->uwgt1 +
                            ((srchi[2] >> 20) & 0x3ff)*(int)row->uwgt2 +
                            ((srchi[4]      ) & 0x3ff)*(int)row->uwgt3)>>WGTSHFT8;
               break;
            }

            srclo = (unsigned int *)(vp->hdField[row->fldlo] + row->offsetlo + row->offsety0);
            srchi = (unsigned int *)(vp->hdField[row->fldhi] + row->offsethi + row->offsety0);

            switch(row->phasey0) { // based on phase do 1st Y

               case 0:

                  ry0    = (((srclo[0] >> 10) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[1]      ) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[0] >> 10) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[1]      ) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 1:

                  ry0    = (((srclo[1]      ) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[1] >> 20) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[1]      ) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[1] >> 20) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 2:

                  ry0    = (((srclo[1] >> 20) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[2] >> 10) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[1] >> 20) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[2] >> 10) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 3:

                  ry0    = (((srclo[2] >> 10) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[3]      ) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[2] >> 10) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[3]      ) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 4:

                  ry0    = (((srclo[3]      ) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[3] >> 20) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[3]      ) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[3] >> 20) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 5:

                  ry0    = (((srclo[3] >> 20) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[4] >> 10) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[3] >> 20) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[4] >> 10) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;
            }

            srclo = (unsigned int *)(vp->hdField[row->fldlo] + row->offsetlo + row->offsetv);
            srchi = (unsigned int *)(vp->hdField[row->fldhi] + row->offsethi + row->offsetv);

            switch(row->phasev) { // based on phase do a V

               case 1:

                  rv     = (((srclo[0] >> 20) & 0x3ff)*(int)row->vwgt0 +
                            ((srclo[2]      ) & 0x3ff)*(int)row->vwgt1 +
                            ((srchi[0] >> 20) & 0x3ff)*(int)row->vwgt2 +
                            ((srchi[2]      ) & 0x3ff)*(int)row->vwgt3)>>WGTSHFT8;
               break;

               case 3:

                  rv     = (((srclo[2]      ) & 0x3ff)*(int)row->vwgt0 +
                            ((srclo[3] >> 10) & 0x3ff)*(int)row->vwgt1 +
                            ((srchi[2]      ) & 0x3ff)*(int)row->vwgt2 +
                            ((srchi[3] >> 10) & 0x3ff)*(int)row->vwgt3)>>WGTSHFT8;
               break;

               case 5:

                  rv     = (((srclo[3] >> 10) & 0x3ff)*(int)row->vwgt0 +
                            ((srclo[4] >> 20) & 0x3ff)*(int)row->vwgt1 +
                            ((srchi[3] >> 10) & 0x3ff)*(int)row->vwgt2 +
                            ((srchi[4] >> 20) & 0x3ff)*(int)row->vwgt3)>>WGTSHFT8;
               break;
            }

            srclo = (unsigned int *)(vp->hdField[row->fldlo] + row->offsetlo + row->offsety1);
            srchi = (unsigned int *)(vp->hdField[row->fldhi] + row->offsethi + row->offsety1);

            switch(row->phasey1) { // based on phase do 2nd Y

               case 0:

                  ry1    = (((srclo[0] >> 10) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[1]      ) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[0] >> 10) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[1]      ) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 1:

                  ry1    = (((srclo[1]      ) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[1] >> 20) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[1]      ) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[1] >> 20) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 2:

                  ry1    = (((srclo[1] >> 20) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[2] >> 10) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[1] >> 20) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[2] >> 10) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 3:

                  ry1    = (((srclo[2] >> 10) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[3]      ) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[2] >> 10) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[3]      ) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 4:

                  ry1    = (((srclo[3]      ) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[3] >> 20) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[3]      ) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[3] >> 20) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 5:

                  ry1    = (((srclo[3] >> 20) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[4] >> 10) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[3] >> 20) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[4] >> 10) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;
            }

            *((unsigned int *)(dst   )) = (rv<<20)+(ry0<<10)+(ru);

            *((unsigned int *)(dst+ 4)) = (ry1)
                                        +(*((unsigned int *)(dst+ 4))&0xfffffc00);

            row++;

            if (--pelprs == 0) break;

         p2:;

            srclo = (unsigned int *)(vp->hdField[row->fldlo] + row->offsetlo + row->offsetu);
            srchi = (unsigned int *)(vp->hdField[row->fldhi] + row->offsethi + row->offsetu);

            switch(row->phaseu) { // based on phase do a U

               case 0:

                  ru      = (((srclo[0]      ) & 0x3ff)*(int)row->uwgt0 +
                             ((srclo[1] >> 10) & 0x3ff)*(int)row->uwgt1 +
                             ((srchi[0]      ) & 0x3ff)*(int)row->uwgt2 +
                             ((srchi[1] >> 10) & 0x3ff)*(int)row->uwgt3)>>WGTSHFT8;
               break;

               case 2:

                  ru     = (((srclo[1] >> 10) & 0x3ff)*(int)row->uwgt0 +
                            ((srclo[2] >> 20) & 0x3ff)*(int)row->uwgt1 +
                            ((srchi[1] >> 10) & 0x3ff)*(int)row->uwgt2 +
                            ((srchi[2] >> 20) & 0x3ff)*(int)row->uwgt3)>>WGTSHFT8;
               break;

               case 4:

                  ru     = (((srclo[2] >> 20) & 0x3ff)*(int)row->uwgt0 +
                            ((srclo[4]      ) & 0x3ff)*(int)row->uwgt1 +
                            ((srchi[2] >> 20) & 0x3ff)*(int)row->uwgt2 +
                            ((srchi[4]      ) & 0x3ff)*(int)row->uwgt3)>>WGTSHFT8;
               break;
            }

            srclo = (unsigned int *)(vp->hdField[row->fldlo] + row->offsetlo + row->offsety0);
            srchi = (unsigned int *)(vp->hdField[row->fldhi] + row->offsethi + row->offsety0);

            switch(row->phasey0) { // based on phase do 1st Y

               case 0:

                  ry0    = (((srclo[0] >> 10) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[1]      ) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[0] >> 10) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[1]      ) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 1:

                  ry0    = (((srclo[1]      ) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[1] >> 20) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[1]      ) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[1] >> 20) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 2:

                  ry0    = (((srclo[1] >> 20) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[2] >> 10) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[1] >> 20) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[2] >> 10) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 3:

                  ry0    = (((srclo[2] >> 10) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[3]      ) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[2] >> 10) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[3]      ) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 4:

                  ry0    = (((srclo[3]      ) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[3] >> 20) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[3]      ) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[3] >> 20) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 5:

                  ry0    = (((srclo[3] >> 20) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[4] >> 10) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[3] >> 20) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[4] >> 10) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;
            }

            srclo = (unsigned int *)(vp->hdField[row->fldlo] + row->offsetlo + row->offsetv);
            srchi = (unsigned int *)(vp->hdField[row->fldhi] + row->offsethi + row->offsetv);

            switch(row->phasev) { // based on phase do a V

               case 1:

                  rv     = (((srclo[0] >> 20) & 0x3ff)*(int)row->vwgt0 +
                            ((srclo[2]      ) & 0x3ff)*(int)row->vwgt1 +
                            ((srchi[0] >> 20) & 0x3ff)*(int)row->vwgt2 +
                            ((srchi[2]      ) & 0x3ff)*(int)row->vwgt3)>>WGTSHFT8;
               break;

               case 3:

                  rv     = (((srclo[2]      ) & 0x3ff)*(int)row->vwgt0 +
                            ((srclo[3] >> 10) & 0x3ff)*(int)row->vwgt1 +
                            ((srchi[2]      ) & 0x3ff)*(int)row->vwgt2 +
                            ((srchi[3] >> 10) & 0x3ff)*(int)row->vwgt3)>>WGTSHFT8;
               break;

               case 5:

                  rv     = (((srclo[3] >> 10) & 0x3ff)*(int)row->vwgt0 +
                            ((srclo[4] >> 20) & 0x3ff)*(int)row->vwgt1 +
                            ((srchi[3] >> 10) & 0x3ff)*(int)row->vwgt2 +
                            ((srchi[4] >> 20) & 0x3ff)*(int)row->vwgt3)>>WGTSHFT8;
               break;
            }

            srclo = (unsigned int *)(vp->hdField[row->fldlo] + row->offsetlo + row->offsety1);
            srchi = (unsigned int *)(vp->hdField[row->fldhi] + row->offsethi + row->offsety1);

            switch(row->phasey1) { // based on phase do 2nd Y

               case 0:

                  ry1    = (((srclo[0] >> 10) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[1]      ) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[0] >> 10) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[1]      ) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 1:

                  ry1    = (((srclo[1]      ) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[1] >> 20) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[1]      ) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[1] >> 20) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 2:

                  ry1    = (((srclo[1] >> 20) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[2] >> 10) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[1] >> 20) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[2] >> 10) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 3:

                  ry1    = (((srclo[2] >> 10) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[3]      ) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[2] >> 10) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[3]      ) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 4:

                  ry1    = (((srclo[3]      ) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[3] >> 20) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[3]      ) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[3] >> 20) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 5:

                  ry1    = (((srclo[3] >> 20) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[4] >> 10) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[3] >> 20) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[4] >> 10) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;
            }

            *((unsigned int *)(dst+ 4)) = (*((unsigned int *)(dst+ 4))&0x3ff)
                                        +(ry0<<20)+(ru<<10);

            *((unsigned int *)(dst+ 8)) = +(ry1<<10)+(rv)
                                        +(*((unsigned int *)(dst+ 8))&0x3ff00000);

            row++;

            if (--pelprs == 0) break;

         p4:;

            srclo = (unsigned int *)(vp->hdField[row->fldlo] + row->offsetlo + row->offsetu);
            srchi = (unsigned int *)(vp->hdField[row->fldhi] + row->offsethi + row->offsetu);

            switch(row->phaseu) { // based on phase do a U

               case 0:

                  ru      = (((srclo[0]      ) & 0x3ff)*(int)row->uwgt0 +
                             ((srclo[1] >> 10) & 0x3ff)*(int)row->uwgt1 +
                             ((srchi[0]      ) & 0x3ff)*(int)row->uwgt2 +
                             ((srchi[1] >> 10) & 0x3ff)*(int)row->uwgt3)>>WGTSHFT8;
               break;

               case 2:

                  ru     = (((srclo[1] >> 10) & 0x3ff)*(int)row->uwgt0 +
                            ((srclo[2] >> 20) & 0x3ff)*(int)row->uwgt1 +
                            ((srchi[1] >> 10) & 0x3ff)*(int)row->uwgt2 +
                            ((srchi[2] >> 20) & 0x3ff)*(int)row->uwgt3)>>WGTSHFT8;
               break;

               case 4:

                  ru     = (((srclo[2] >> 20) & 0x3ff)*(int)row->uwgt0 +
                            ((srclo[4]      ) & 0x3ff)*(int)row->uwgt1 +
                            ((srchi[2] >> 20) & 0x3ff)*(int)row->uwgt2 +
                            ((srchi[4]      ) & 0x3ff)*(int)row->uwgt3)>>WGTSHFT8;
               break;
            }

            srclo = (unsigned int *)(vp->hdField[row->fldlo] + row->offsetlo + row->offsety0);
            srchi = (unsigned int *)(vp->hdField[row->fldhi] + row->offsethi + row->offsety0);

            switch(row->phasey0) { // based on phase do 1st Y

               case 0:

                  ry0    = (((srclo[0] >> 10) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[1]      ) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[0] >> 10) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[1]      ) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 1:

                  ry0    = (((srclo[1]      ) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[1] >> 20) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[1]      ) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[1] >> 20) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 2:

                  ry0    = (((srclo[1] >> 20) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[2] >> 10) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[1] >> 20) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[2] >> 10) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 3:

                  ry0    = (((srclo[2] >> 10) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[3]      ) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[2] >> 10) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[3]      ) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 4:

                  ry0    = (((srclo[3]      ) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[3] >> 20) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[3]      ) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[3] >> 20) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 5:

                  ry0    = (((srclo[3] >> 20) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[4] >> 10) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[3] >> 20) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[4] >> 10) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;
            }

            srclo = (unsigned int *)(vp->hdField[row->fldlo] + row->offsetlo + row->offsetv);
            srchi = (unsigned int *)(vp->hdField[row->fldhi] + row->offsethi + row->offsetv);

            switch(row->phasev) { // based on phase do a V

               case 1:

                  rv     = (((srclo[0] >> 20) & 0x3ff)*(int)row->vwgt0 +
                            ((srclo[2]      ) & 0x3ff)*(int)row->vwgt1 +
                            ((srchi[0] >> 20) & 0x3ff)*(int)row->vwgt2 +
                            ((srchi[2]      ) & 0x3ff)*(int)row->vwgt3)>>WGTSHFT8;
               break;

               case 3:

                  rv     = (((srclo[2]      ) & 0x3ff)*(int)row->vwgt0 +
                            ((srclo[3] >> 10) & 0x3ff)*(int)row->vwgt1 +
                            ((srchi[2]      ) & 0x3ff)*(int)row->vwgt2 +
                            ((srchi[3] >> 10) & 0x3ff)*(int)row->vwgt3)>>WGTSHFT8;
               break;

               case 5:

                  rv     = (((srclo[3] >> 10) & 0x3ff)*(int)row->vwgt0 +
                            ((srclo[4] >> 20) & 0x3ff)*(int)row->vwgt1 +
                            ((srchi[3] >> 10) & 0x3ff)*(int)row->vwgt2 +
                            ((srchi[4] >> 20) & 0x3ff)*(int)row->vwgt3)>>WGTSHFT8;
               break;
            }

            srclo = (unsigned int *)(vp->hdField[row->fldlo] + row->offsetlo + row->offsety1);
            srchi = (unsigned int *)(vp->hdField[row->fldhi] + row->offsethi + row->offsety1);

            switch(row->phasey1) { // based on phase do 2nd Y

               case 0:

                  ry1    = (((srclo[0] >> 10) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[1]      ) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[0] >> 10) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[1]      ) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 1:

                  ry1    = (((srclo[1]      ) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[1] >> 20) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[1]      ) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[1] >> 20) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 2:

                  ry1    = (((srclo[1] >> 20) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[2] >> 10) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[1] >> 20) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[2] >> 10) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 3:

                  ry1    = (((srclo[2] >> 10) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[3]      ) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[2] >> 10) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[3]      ) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 4:

                  ry1    = (((srclo[3]      ) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[3] >> 20) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[3]      ) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[3] >> 20) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;

               case 5:

                  ry1    = (((srclo[3] >> 20) & 0x3ff)*(int)row->y0wgt0 +
                            ((srclo[4] >> 10) & 0x3ff)*(int)row->y0wgt1 +
                            ((srchi[3] >> 20) & 0x3ff)*(int)row->y0wgt2 +
                            ((srchi[4] >> 10) & 0x3ff)*(int)row->y0wgt3)>>WGTSHFT8;
               break;
            }

            *((unsigned int *)(dst+ 8)) = (*((unsigned int *)(dst+ 8))&0xfffff)
                                        +(ru<<20);

            *((unsigned int *)(dst+12)) = (ry1<<20)+(rv<<10)+(ry0);

            dst += 16;

            row++;

            if (--pelprs == 0) break;

      }

   } // for each row of stripe

}

