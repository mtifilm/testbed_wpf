//---------------------------------------------------------------------------
#ifndef CMAGNIFIER_H
#define CMAGNIFIER_H
//---------------------------------------------------------------------------

////////////////////////////////////////////////////////////

#include "machine.h"
#include "imgToolDLL.h"

class CConvert;
class CExtractor;
class CImageFormat;

// class for tools to implement magnifiers
//
class MTI_IMGTOOLDLL_API CMagnifier
{
public:

   CMagnifier();

   ~CMagnifier();

   // takes the screen device context, the initial magnifier window, and
   // the dimensions of the destination magnifier bitmap
   int initMagnifier(int bmwdth, int bmhght,
                     const CImageFormat *magfmt,
                     RECT *magwin);

   // activates the magnifier, supplying a callback which is
   // called whenever the magnifier bitmap is refreshed
   void activateMagnifier(void (*magcallback)(void *obj, void *mag),
                                      void *rfrobj);

   // deactivate the magnifier
   void deactivateMagnifier();

   // TRUE if magnifier is active, FALSE if inactive
   bool isActive();

   // show/hide the magnifier
   void show();
   void hide();
   bool isVisible();

   // update the bitmap from the native frame given as arg
   void refreshMagnifier(unsigned char **fld);

   // set the image format
   void setMagnifierImageFormat(const CImageFormat *magfmt);

   // set the cur magnifier rect
   int setMagnifierWindow(RECT *magwin);

   // recenter the cur magnifier rect
   void recenterMagnifierWindow(int x, int y);

   // get the cur rect magnified
   RECT getMagnifierWindow();

   // get frame buffer dimensions
   int getMagnifierBitmapWdth();
   int getMagnifierBitmapHght();

   // get the actual frame buffer
   unsigned int *getMagnifierFrameBuffer();

   // get the intermediate frame buffer
   MTI_UINT16 *getIntermediateFrameBuffer();

   // callback vector for magnifier refresh
   void (*magnifierCallback)(void *,void *);
   void *magnifierCallbackObj;

   // set autoclean hack flag
   void setAutoCleanHackFlag();
   bool getAutoCleanHackFlag();

   // For autoclean hack
   RECT getDisplayRect();
   void getPosition(int &x, int &y);
   RECT getImageRect();

private:

   // YUV-to-RGB converter
   CConvert *cnvt;

   // YUV-to-intermediate extractor
   CExtractor *extr;

   // image format
   const CImageFormat *magFmt;

   // magnifier bitmap dimensions
   int magBMWdth, magBMHght;

   // -> magnifier frame buffer
   unsigned int *magFB;

   // auxiliary frame buffer for 90 deg rotation
   unsigned int *auxMagFB;

   // flag to enable 90 deg rotation
   bool rotateBM;

   // 16-bit intermediate YUV or RGB frame buffer
   MTI_UINT16 *intFB;

   // auxiliary intermediate frame buffer for rotation
   MTI_UINT16 *auxIntFB;

   // active rectangle of image
   RECT actWin;

   // current magnifier rectangle (frame coordinates)
   RECT magWin;

   // show/hide control
   bool visible;

   // autoclean hack flag because there's no time to do things right!
   bool autoCleanHackFlag;
};
#endif
