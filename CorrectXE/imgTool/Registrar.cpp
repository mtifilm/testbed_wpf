//
//  implementation file for class CRegistrar
//
#include <stdio.h>
#include <stdlib.h>

#include "IniFile.h"

#include "ImageFormat3.h"
#include "machine.h"
#include "MTImalloc.h"
#include "Registrar.h"
#include "SynchronousThreadRunner.h"
#include "SysInfo.h"

#include <math.h>

// define this for debugging without multithreading
//#define NO_MULTI

#define MAX_REGISTRAR_THREADS 64

#define SCALER 0x8000
#define SHIFTR 15

//////////////////////////////////////////////////////////////
//
//                  C R e g i s t r a r
//
//        Author: Kurt Tolksdorf
//        Begun:  11/28/08
//
// Performs bilinear subpixel shifting of RED and BLUE channels
// for the purpose of color registration. The operation creates
// a new intermediate out of the raw, unregistered one.
//
// CRegistrar is multithreaded for speed.

CRegistrar::CRegistrar(int numStripes)
{
   _nStripes = (numStripes == 0)
             ? std::min<int>(SysInfo::AvailableProcessorCount(), MAX_REGISTRAR_THREADS)
             : numStripes;

   // clear this to avoid a crash
   // on exit in the destructor
   auxpels = NULL;
   auxWds = 0;

}

CRegistrar::~CRegistrar()
{
   MTIfree(auxpels);
}

// this implements the general case of an affine
// transformation for each of channels RED and BLUE
//
void CRegistrar::registerChannels(MTI_UINT16 *dst, MTI_UINT16 *src,
                                  const CImageFormat *srcfmt,
                                  RECT *matBox,
                                  AffineCoeff *xfrm)
{
   dstpels          = dst;
   srcpels          = src;
   frmWdth          = srcfmt->getPixelsPerLine();
   frmHght          = srcfmt->getLinesPerFrame();
   xform            = xfrm;

//#define TEST_FRAME
#ifdef TEST_FRAME
   makeTestPattern();
   return;
#endif

   int totPels = frmWdth*frmHght;

   int alphaByts = 0;
   int frmPelComponents = srcfmt->getPixelComponents();
   if (frmPelComponents == IF_PIXEL_COMPONENTS_RGB) {

      switch(srcfmt->getAlphaMatteType()) {

         case IF_ALPHA_MATTE_1_BIT:
            alphaByts = (totPels+7)/8;
         break;

         case IF_ALPHA_MATTE_2_BIT_EMBEDDED:
            alphaByts = (totPels+3)/4;
         break;

         case IF_ALPHA_MATTE_8_BIT:
            alphaByts = totPels;
         break;

         case IF_ALPHA_MATTE_16_BIT:
            alphaByts = totPels*2;
		 break;

		 default:
		 	break;
      }
   }
   else if (frmPelComponents == IF_PIXEL_COMPONENTS_RGBA) {

      alphaByts = totPels*2;
   }
   else
      return; // can't operate

   /////////////////////////////////////////////////////////////////////////////
   //
   // get the ptr to the alpha matte and copy it to destination
   //
   MTI_UINT16 *srcAlpha = src + totPels*3;
   MTI_UINT16 *dstAlpha = dst + totPels*3;
   memcpy(dstAlpha, srcAlpha, alphaByts);

   /////////////////////////////////////////////////////////////////////////////
   //
   // if using a MATBOX copy the pixels
   // framing the matbox from src to dst
   if (matBox != NULL) {

      MTI_UINT16 *srcpel = src;
      MTI_UINT16 *dstpel = dst;

      int i=0;
      for (; i < matBox->top; i++) {

         for (int j=0; j < frmWdth; j++) {
            dstpel[3*j  ] = srcpel[3*j  ];
            dstpel[3*j+1] = srcpel[3*j+1];
            dstpel[3*j+2] = srcpel[3*j+2];
         }

         srcpel += frmWdth*3;
         dstpel += frmWdth*3;
      }

      for (; i <= matBox->bottom; i++) {

         for (int j=0; j < matBox->left; j++) {

            dstpel[3*j  ] = srcpel[3*j  ];
            dstpel[3*j+1] = srcpel[3*j+1];
            dstpel[3*j+2] = srcpel[3*j+2];
         }
         for (int j=matBox->right + 1; j < frmWdth; j++) {

            dstpel[3*j  ] = srcpel[3*j  ];
            dstpel[3*j+1] = srcpel[3*j+1];
            dstpel[3*j+2] = srcpel[3*j+2];
         }

         srcpel += frmWdth*3;
         dstpel += frmWdth*3;
      }

      for (; i < frmHght; i++) {

         for (int j=0; j < frmWdth; j++) {
            dstpel[3*j  ] = srcpel[3*j  ];
            dstpel[3*j+1] = srcpel[3*j+1];
            dstpel[3*j+2] = srcpel[3*j+2];
         }

         srcpel += frmWdth*3;
         dstpel += frmWdth*3;
      }
   }

   /////////////////////////////////////////////////////////////////////////////
   //
   // Here we initialize for two independent scans, one for RED, one for BLUE.
   // The coordinates are the source pixels for (0,0), (frmWdth-1,0), and
   // (0, frmHght-1) multiplied by 2**15
   scnBox.left   = 0;
   scnBox.top    = 0;
   scnBox.right  = frmWdth - 1;
   scnBox.bottom = frmHght - 1;
   if (matBox != NULL) {
      scnBox = *matBox;
   }
   scnWdth = scnBox.right - scnBox.left + 1;
   scnHght = scnBox.bottom - scnBox.top + 1;

   // put output into this box
   dstBox = scnBox;

   /////////////////////////////////////////////////////////////////////////////
   //
   // coordinates of RED scanning lozenge
   //
   // L, T
   double xltr = (scnBox.left   -
                  (xfrm->AredX*scnBox.left + xfrm->BredX*scnBox.top + xfrm->CredX));
   double yltr = (scnBox.top    -
                  (xfrm->AredY*scnBox.left + xfrm->BredY*scnBox.top + xfrm->CredY));
   // R, T
   double xrtr = (scnBox.right  -
                  (xfrm->AredX*scnBox.right + xfrm->BredX*scnBox.top + xfrm->CredX));
   double yrtr = (scnBox.top    -
                  (xfrm->AredY*scnBox.right + xfrm->BredY*scnBox.top + xfrm->CredY));
   // R, B
   double xrbr = (scnBox.right  -
                  (xfrm->AredX*scnBox.right + xfrm->BredX*scnBox.bottom + xfrm->CredX));
   double yrbr = (scnBox.bottom -
                  (xfrm->AredY*scnBox.right + xfrm->BredY*scnBox.bottom + xfrm->CredY));
   // L, B
   double xlbr = (scnBox.left   -
                  (xfrm->AredX*scnBox.left + xfrm->BredX*scnBox.bottom + xfrm->CredX));
   double ylbr = (scnBox.bottom -
                  (xfrm->AredY*scnBox.left + xfrm->BredY*scnBox.bottom + xfrm->CredY));

   // get the integer extent box surrounding these pts
   RECT redBox;
   redBox.left   = std::min<long>((int)xltr, (int)xlbr);
   redBox.top    = std::min<long>((int)yltr, (int)yrtr);
   redBox.right  = std::max<long>((int)(xrtr+0.5), (int)(xrbr+0.5));
   redBox.bottom = std::max<long>((int)(ylbr+0.5), (int)(yrbr+0.5));

   /////////////////////////////////////////////////////////////////////////////
   //
   // coordinates of BLUE scanning lozenge
   //
   // L, T
   double xltb = (scnBox.left   -
                  (xfrm->AbluX*scnBox.left + xfrm->BbluX*scnBox.top + xfrm->CbluX));
   double yltb = (scnBox.top    -
                  (xfrm->AbluY*scnBox.left + xfrm->BbluY*scnBox.top + xfrm->CbluY));
   // R, T
   double xrtb = (scnBox.right  -
                  (xfrm->AbluX*scnBox.right + xfrm->BbluX*scnBox.top + xfrm->CbluX));
   double yrtb = (scnBox.top    -
                  (xfrm->AbluY*scnBox.right + xfrm->BbluY*scnBox.top + xfrm->CbluY));
   // R, B
   double xrbb = (scnBox.right  -
                  (xfrm->AbluX*scnBox.right + xfrm->BbluX*scnBox.bottom + xfrm->CbluX));
   double yrbb = (scnBox.bottom -
                  (xfrm->AbluY*scnBox.right + xfrm->BbluY*scnBox.bottom + xfrm->CbluY));
   // L, B
   double xlbb = (scnBox.left   -
                  (xfrm->AbluX*scnBox.left + xfrm->BbluX*scnBox.bottom + xfrm->CbluX));
   double ylbb = (scnBox.bottom -
                  (xfrm->AbluY*scnBox.left + xfrm->BbluY*scnBox.bottom + xfrm->CbluY));

   // get the integer extent box surrounding these pts
   RECT bluBox;
   bluBox.left   = std::min<long>((int)xltb, (int)xlbb);
   bluBox.top    = std::min<long>((int)yltb, (int)yrtb);
   bluBox.right  = std::max<long>((int)(xrtb+0.5), (int)(xrbb+0.5));
   bluBox.bottom = std::max<long>((int)(ylbb+0.5), (int)(yrbb+0.5));

   // the fatBox gives the coords of a box which
   // is big enough to surround all the scan pts
   RECT fatBox;
   fatBox.left   = std::min<long>(redBox.left,   bluBox.left);
   fatBox.top    = std::min<long>(redBox.top,    bluBox.top);
   fatBox.right  = std::max<long>(redBox.right,  bluBox.right);
   fatBox.bottom = std::max<long>(redBox.bottom, bluBox.bottom);

   // and also surround the original scnBox
   fatBox.left   = std::min<long>(fatBox.left,   scnBox.left);
   fatBox.top    = std::min<long>(fatBox.top,    scnBox.top);
   fatBox.right  = std::max<long>(fatBox.right,  scnBox.right);
   fatBox.bottom = std::max<long>(fatBox.bottom, scnBox.bottom);

   // add an extra pixel around the edges
   fatBox.left   -= 2;
   fatBox.top    -= 2;
   fatBox.right  += 2;
   fatBox.bottom += 2;

   // now allocate a new frame big enough for the fatBox
   FrmWdth = fatBox.right - fatBox.left + 1;
   FrmHght = fatBox.bottom - fatBox.top + 1;
   auxpels = (MTI_UINT16 *)MTImalloc(FrmWdth*FrmHght*3*sizeof(MTI_UINT16));

   // copy the original frame into the new one, taking
   // care to relocate it properly
   MTI_UINT16 *fm = srcpels + (scnBox.top*frmWdth + scnBox.left)*3;
   MTI_UINT16 *to = auxpels + ((scnBox.top - fatBox.top)*FrmWdth + (scnBox.left - fatBox.left))*3;
   for (int i=0;i<scnHght; i++) {
      for (int j=0;j<scnWdth; j++) {
         to[3*j  ] = fm[3*j  ];
         to[3*j+1] = fm[3*j+1];
         to[3*j+2] = fm[3*j+2];
      }

      fm += frmWdth*3;
      to += FrmWdth*3;
   }

   // now switch the scnBox to the
   // coordinate system of the auxpels
   scnBox.left   -= fatBox.left;
   scnBox.top    -= fatBox.top;
   scnBox.right  -= fatBox.left;
   scnBox.bottom -= fatBox.top;

   // relocate the scan pts to the new coordinate system
   xltr -= fatBox.left;
   yltr -= fatBox.top;
   xrtr -= fatBox.left;
   yrtr -= fatBox.top;
   xrbr -= fatBox.left;
   yrbr -= fatBox.top;
   xlbr -= fatBox.left;
   ylbr -= fatBox.top;

   xltb -= fatBox.left;
   yltb -= fatBox.top;
   xrtb -= fatBox.left;
   yrtb -= fatBox.top;
   xrbb -= fatBox.left;
   yrbb -= fatBox.top;
   xlbb -= fatBox.left;
   ylbb -= fatBox.top;

   // and then do the same with the fatBox
   fatBox.left   = 0;
   fatBox.top    = 0;
   fatBox.right  = FrmWdth - 1;
   fatBox.bottom = FrmHght - 1;

   // now bleed the pixels perp outward
   // from the boundaries of the scnBox
   // into the surrounding frame zone

   MTI_UINT16 *fatrow;
   MTI_UINT16 *scnrow;
   for (int i=scnBox.top; i<=scnBox.bottom; i++) {

      fatrow = auxpels + i*FrmWdth*3;

      for (int j=fatBox.left;j<scnBox.left; j++) {
         fatrow[3*j  ] = fatrow[3*scnBox.left  ];
         fatrow[3*j+2] = fatrow[3*scnBox.left+2];
      }
      for (int j=scnBox.right+1; j<=fatBox.right; j++) {
         fatrow[3*j  ] = fatrow[3*scnBox.right  ];
         fatrow[3*j+2] = fatrow[3*scnBox.right+2];
      }
   }

   scnrow = auxpels + scnBox.top*FrmWdth*3;
   for (int i=fatBox.top; i<scnBox.top; i++) {

      fatrow = auxpels + i*FrmWdth*3;

      for (int j=fatBox.left; j<=fatBox.right; j++) {
         fatrow[3*j  ] = scnrow[3*j  ];
         fatrow[3*j+2] = scnrow[3*j+2];
      }
   }

   scnrow = auxpels + scnBox.bottom*FrmWdth*3;
   for (int i=scnBox.bottom+1; i<=fatBox.bottom; i++) {

      fatrow = auxpels + i*FrmWdth*3;

      for (int j=fatBox.left; j<=fatBox.right; j++) {
         fatrow[3*j  ] = scnrow[3*j  ];
         fatrow[3*j+2] = scnrow[3*j+2];
      }
   }

   // now initialize for the (red and blue) scanning
   // scaled integer-fraction versions of the pts
   xltRed = SCALER*xltr;
   yltRed = SCALER*yltr;
   xrtRed = SCALER*xrtr;
   yrtRed = SCALER*yrtr;
   xlbRed = SCALER*xlbr;
   ylbRed = SCALER*ylbr;

   rowdelxRed = (xrtRed - xltRed) / (scnWdth - 1);
   rowdelyRed = (yrtRed - yltRed) / (scnWdth - 1);
   coldelxRed = (xlbRed - xltRed) / (scnHght - 1);
   coldelyRed = (ylbRed - yltRed) / (scnHght - 1);

   // scaled integer-fraction versions of the pts
   xltBlu = SCALER*xltb;
   yltBlu = SCALER*yltb;
   xrtBlu = SCALER*xrtb;
   yrtBlu = SCALER*yrtb;
   xlbBlu = SCALER*xlbb;
   ylbBlu = SCALER*ylbb;

   rowdelxBlu = (xrtBlu - xltBlu) / (scnWdth - 1);
   rowdelyBlu = (yrtBlu - yltBlu) / (scnWdth - 1);
   coldelxBlu = (xlbBlu - xltBlu) / (scnHght - 1);
   coldelyBlu = (ylbBlu - yltBlu) / (scnHght - 1);

   /////////////////////////////////////////////////////////////////////////////

   switch(srcfmt->getPixelComponents()) {

      case IF_PIXEL_COMPONENTS_RGB:
      case IF_PIXEL_COMPONENTS_RGBA:
      case IF_PIXEL_COMPONENTS_BGR:
      case IF_PIXEL_COMPONENTS_BGRA:
         break;

      default:

         TRACE_0(errout << "REGISTRAR FAIL: PIXEL FORMAT IS NOT RGB!");
         return;
   }

   // register the frame by stripe
#ifdef NO_MULTI
   for (int i = 0; i < _nStripes; i++)
   {
      regAffineRGB(this, i, _nStripes);
   }
#else
   // fire up one thread for each stripe
   SynchronousThreadRunner threadRunner(_nStripes, this, regAffineRGB);
   int iRet = threadRunner.Run();
   if (iRet)
   {
      TRACE_0(errout << "Registrar: regAffineRGB FAIL, code " << iRet);
   }
#endif

   MTIfree(auxpels);
   auxpels = NULL;
}

void CRegistrar::null(void *v, int iJob)
{
}

int CRegistrar::regAffineRGB(void *v, int iJob, int iTotalJobs)
{
   CRegistrar *vp = (CRegistrar *) v;
   MTI_UINT16 *dstpels  = vp->dstpels;
   int frmWdth          = vp->frmWdth;
   int dstpitch         = vp->frmWdth*3;
   RECT dstBox          = vp->dstBox;
   int scnWdth          = vp->scnWdth;
   int scnHght          = vp->scnHght;

   MTI_UINT16 *auxpels  = vp->auxpels;
   int FrmWdth          = vp->FrmWdth;
   int srcpitch         = vp->FrmWdth*3;
   RECT scnBox          = vp->scnBox;
   int xlim             = (vp->FrmWdth - 2)<<SHIFTR;
   int ylim             = (vp->FrmHght - 2)<<SHIFTR;

   int nStripes = std::min<int>(scnHght, vp->_nStripes);
   if (iJob >= nStripes)
   {
      // More jobs than rows, so skip this one.
      return 0;
   }

   // Standard pattern for distributing "extra" rows to threads evenly
   int nominalRowsPerStripe = scnHght / nStripes;
   int extraRows = scnHght - (nStripes * nominalRowsPerStripe);
   int numberOfRowsInThisStripe = nominalRowsPerStripe + ((iJob < extraRows) ? 1 : 0);
   int stripeStartOffset = (iJob * nominalRowsPerStripe) + std::min<int>(extraRows, iJob);
   int stripeStartRow = dstBox.top + stripeStartOffset;

   /////////////////////////////////////////////////////////////////////////////
   //
   // import the scan variables
   //
   int xltRed = vp->xltRed;
   int yltRed = vp->yltRed;
   int xrtRed = vp->xrtRed;
   int yrtRed = vp->yrtRed;

   int rowdelxRed = vp->rowdelxRed;
   int rowdelyRed = vp->rowdelyRed;
   int coldelxRed = vp->coldelxRed;
   int coldelyRed = vp->coldelyRed;

   int xltBlu = vp->xltBlu;
   int yltBlu = vp->yltBlu;
   int xrtBlu = vp->xrtBlu;
   int yrtBlu = vp->yrtBlu;

   int rowdelxBlu = vp->rowdelxBlu;
   int rowdelyBlu = vp->rowdelyBlu;
   int coldelxBlu = vp->coldelxBlu;
   int coldelyBlu = vp->coldelyBlu;

   xltRed += stripeStartOffset*coldelxRed;
   yltRed += stripeStartOffset*coldelyRed;
   xrtRed += stripeStartOffset*coldelxRed;
   yrtRed += stripeStartOffset*coldelyRed;

   xltBlu += stripeStartOffset*coldelxBlu;
   yltBlu += stripeStartOffset*coldelyBlu;
   xrtBlu += stripeStartOffset*coldelxBlu;
   yrtBlu += stripeStartOffset*coldelyBlu;

   /////////////////////////////////////////////////////////////////////////////

   MTI_UINT16 *dstRed;
   MTI_UINT16 *dstGrn;
   MTI_UINT16 *dstBlu;

   for (int i = stripeStartRow; i < (stripeStartRow + numberOfRowsInThisStripe); i++)
   {
      // destination ptrs
      dstRed = dstGrn = dstBlu = dstpels + (i*frmWdth + dstBox.left)*3;

      // endpoints of the RED and BLUE scanlines (* SCALER)
      int xrowRed = xltRed;
      int yrowRed = yltRed;
      int xendRed = xrtRed;
      int yendRed = yrtRed;

      int xrowBlu = xltBlu;
      int yrowBlu = yltBlu;
      int xendBlu = xrtBlu;
      int yendBlu = yrtBlu;

      // R E D /////////////////////////////////////////////////////////////////

      int preBlacksRed, framePelsRed, postBlacksRed;

      // derive the loop counts using a Cohn-Sutherland clipping algorithm
      rowLoopCounts(xrowRed, yrowRed, xendRed, yendRed,
                    xlim, ylim,
                    scnWdth,
                    &preBlacksRed, &framePelsRed, &postBlacksRed);

      // the general output row has some initial black pels,
      // followed by some data pels, followed by black pels

      for (int j=0;j<preBlacksRed;j++) {

         dstRed[0]       = 0;
         dstRed[1]       = 0;
         dstRed[2]       = 0;

         // advancing by 1 pel
         dstRed += 3;
      }

      xrowRed += preBlacksRed*rowdelxRed;
      yrowRed += preBlacksRed*rowdelyRed;

      for (int j=0;j<framePelsRed;j++) {

         // here we do a bilinear interpolation
         int frx = (xrowRed>>(SHIFTR-8))&0xff;
         int fry = (yrowRed>>(SHIFTR-8))&0xff;
         int grx = 256 - frx;
         int gry = 256 - fry;

         MTI_UINT16 *src = auxpels + ((yrowRed>>SHIFTR)*FrmWdth + (xrowRed>>SHIFTR))*3;

         dstRed[0] =((src[0         ]*grx + src[3         ]*frx)*gry +
                     (src[0+srcpitch]*grx + src[3+srcpitch]*frx)*fry)>>16;

         xrowRed += rowdelxRed;
         yrowRed += rowdelyRed;

         // advancing by 1 pel
         dstRed += 3;
      }

      for (int j=0;j<postBlacksRed;j++) {

         dstRed[0]       = 0;
         dstRed[1]       = 0;
         dstRed[2]       = 0;

         // advancing by 1 pel
         dstRed += 3;
      }

      xltRed += coldelxRed;
      yltRed += coldelyRed;
      xrtRed += coldelxRed;
      yrtRed += coldelyRed;

      // G R E E N /////////////////////////////////////////////////////////////

      MTI_UINT16 *src = auxpels + ((scnBox.top + (i - dstBox.top))*FrmWdth +
                                   scnBox.left)*3;

      for (int j=0;j<scnWdth;j++) {

         // copy green
         dstGrn[1] = src[1];

         // advancing by 1 pel
         src    += 3;
         dstGrn += 3;
      }

      // B L U E ///////////////////////////////////////////////////////////////

      int preBlacksBlu, framePelsBlu, postBlacksBlu;

      // derive the loop counts using a Cohn-Sutherland clipping algorithm
      rowLoopCounts(xrowBlu, yrowBlu, xendBlu, yendBlu,
                    xlim, ylim,
                    scnWdth,
                    &preBlacksBlu, &framePelsBlu, &postBlacksBlu);

      // the general output row has some initial black pels,
      // followed by some data pels, followed by black pels

      for (int j=0;j<preBlacksBlu;j++) {

         dstBlu[0]       = 0;
         dstBlu[1]       = 0;
         dstBlu[2]       = 0;

         // advancing by 1 pel
         dstBlu += 3;
      }

      xrowBlu += preBlacksBlu*rowdelxBlu;
      yrowBlu += preBlacksBlu*rowdelyBlu;

      for (int j=0;j<framePelsBlu;j++) {

         // here we do a bilinear interpolation
         int frx = (xrowBlu>>(SHIFTR-8))&0xff;
         int fry = (yrowBlu>>(SHIFTR-8))&0xff;
         int grx = 256 - frx;
         int gry = 256 - fry;

         MTI_UINT16 *src = auxpels + ((yrowBlu>>SHIFTR)*FrmWdth + (xrowBlu>>SHIFTR))*3;

         dstBlu[2] =((src[2         ]*grx + src[5         ]*frx)*gry +
                     (src[2+srcpitch]*grx + src[5+srcpitch]*frx)*fry)>>16;

         xrowBlu += rowdelxBlu;
         yrowBlu += rowdelyBlu;

         // advancing by 1 pel
         dstBlu += 3;
      }

      for (int j=0;j<postBlacksBlu;j++) {

         dstBlu[0]       = 0;
         dstBlu[1]       = 0;
         dstBlu[2]       = 0;

         // advancing by 1 pel
         dstBlu += 3;
      }

      xltBlu += coldelxBlu;
      yltBlu += coldelyBlu;
      xrtBlu += coldelxBlu;
      yrtBlu += coldelyBlu;
   }

   return 0;
}

// makes a simple test pattern consisting of a gray 80x60 rectangle
void CRegistrar::makeTestPattern()
{
   // zero the whole frame
   MTImemset(dstpels, 0, frmWdth * frmHght * 3 * sizeof(MTI_UINT16));

   // now make a gray 80x60 rectangle smack in mid-frame
   for (int i=frmHght/2 - 30; i<=frmHght/2 + 30; i++) {

      MTI_UINT16 *row = dstpels + i*3*frmWdth;

      for (int j=frmWdth/2 - 40; j<=frmWdth/2 + 40; j++) {

         row[3*j]   = 768;
         row[3*j+1] = 768;
         row[3*j+2] = 768;
      }
   }

   return;
}

// takes begin and end points and generates loop counts
void CRegistrar::rowLoopCounts(int xbeg, int ybeg,    // first endpt of scanline
                             int xend, int yend,    // last  endpt of scanline
                             int xlim, int ylim,    // size of clipping rect
                             int dstw,              // total pts in scanline
                             int *z1, int *d, int *z2)
{
   int rbeg = 0;
   if (xbeg < 0)    rbeg |= TLFT;
   if (xbeg > xlim) rbeg |= TRGT;
   if (ybeg < 0)    rbeg |= TTOP;
   if (ybeg > ylim) rbeg |= TBOT;

   int rend = 0;
   if (xend < 0)    rend |= TLFT;
   if (xend > xlim) rend |= TRGT;
   if (yend < 0)    rend |= TTOP;
   if (yend > ylim) rend |= TBOT;

   if ((rbeg&rend) != 0) { // the row doesn't intersect the source frame

      *z1 = dstw;
      *d  = 0;
      *z2 = 0;
   }
   else {

      double delx  = xend - xbeg;
      double dely  = yend - ybeg;

      int xclip, yclip;

      *z1 = 0;
      if (rbeg) {

         if (rbeg&TLFT) {

            yclip = ybeg - (int)(dely*(double)xbeg/delx);
            rbeg &= ~TLFT;
            if (yclip < 0) {
               rbeg |= TTOP;
               if (rbeg&rend) {
                  *z1 = dstw;
                  *d  = 0;
                  *z2 = 0;
                  return;
               }
            }
            else if (yclip > ylim) {
               rbeg |= TBOT;
               if (rbeg&rend) {
                  *z1 = dstw;
                  *d  = 0;
                  *z2 = 0;
                  return;
               }
            }
            else {
               rbeg &= ~(TTOP + TBOT);
               *z1 = -(double)dstw*(double)xbeg/delx + 0.5;
               if ((rbeg|rend)==0) {
                  *d  = dstw - *z1;
                  *z2 = 0;
                  goto check;
               }
            }
         }

         if (rbeg&TTOP) {

            xclip = xbeg - (int)(delx*(double)ybeg/dely);
            rbeg &= ~TTOP;
            if (xclip < 0) {
               rbeg |= TLFT;
               if (rbeg&rend) {
                  *z1 = dstw;
                  *d  = 0;
                  *z2 = 0;
                  return;
               }
            }
            else if (xclip > xlim) {
               rbeg |= TRGT;
               if (rbeg&rend) {
                  *z1 = dstw;
                  *d  = 0;
                  *z2 = 0;
                  return;
               }
            }
            else {
               rbeg &= ~(TLFT + TRGT);
               *z1 = -(double)dstw*(double)ybeg/dely + 0.5;
               if ((rbeg|rend)==0) {
                  *d  = dstw - *z1;
                  *z2 = 0;
                  goto check;
               }
            }
         }

         if (rbeg&TRGT) {

            yclip = ybeg - (int)(dely*(double)(xbeg-xlim)/delx);
            rbeg &= ~TRGT;
            if (yclip < 0) {
               rbeg |= TTOP;
               if (rbeg&rend) {
                  *z1 = dstw;
                  *d  = 0;
                  *z2 = 0;
                  return;
               }
            }
            else if (yclip > ylim) {
               rbeg |= TBOT;
               if (rbeg&rend) {
                  *z1 = dstw;
                  *d  = 0;
                  *z2 = 0;
                  return;
               }
            }
            else {
               rbeg &= ~(TTOP + TBOT);
               *z1 = -(double)dstw*(double)(xbeg-xlim)/delx + 0.5;
               if ((rbeg|rend)==0) {
                  *d  = dstw - *z1;
                  *z2 = 0;
                  goto check;
               }
            }
         }

         if (rbeg&TBOT) {

            xclip = xbeg - (int)(delx*(double)(ybeg-ylim)/dely);
            rbeg &= ~TTOP;
            if (xclip < 0) {
               rbeg |= TLFT;
               if (rbeg&rend) {
                  *z1 = dstw;
                  *d  = 0;
                  *z2 = 0;
                  return;
               }
            }
            else if (xclip > xlim)
               rbeg |= TRGT;
               if (rbeg&rend) {
                  *z1 = dstw;
                  *d  = 0;
                  *z2 = 0;
                  return;
            }
            else {
               rbeg &= (TLFT + TRGT);
               *z1 = (double)dstw*(double)(ylim-ybeg)/dely + 0.5;
               if ((rbeg|rend)==0) {
                  *d  = dstw - *z1;
                  *z2 = 0;
                  goto check;
               }
            }
         }
      }

      *z2 = 0;
      if (rend) {

         if (rend&TLFT) {

            yclip = yend - (int)(dely*(double)xend/delx);
            rend &= ~TLFT;
            if (yclip < 0) {
               rend |= TTOP;
               if (rbeg&rend) {
                  *z1 = 0;
                  *d  = 0;
                  *z2 = dstw;
                  return;
               }
            }
            else if (yclip > ylim) {
               rend |= TBOT;
               if (rbeg&rend) {
                  *z1 = 0;
                  *d  = 0;
                  *z2 = dstw;
                  return;
               }
            }
            else {
               rend &= ~(TTOP + TBOT);
               *z2 = (int)dstw*(double)xend/delx + 0.5;
               if ((rbeg|rend)==0) {
                  *d  = dstw - (*z1 + *z2);
                  goto check;
               }
            }
         }

         if (rend&TTOP) {

            xclip = xend - (int)(delx*(double)yend/dely);
            rend &= ~TTOP;
            if (xclip < 0) {
               rend |= TLFT;
               if (rbeg&rend) {
                  *z1 = dstw;
                  *d  = 0;
                  *z2 = 0;
                  return;
               }
            }
            else if (xclip > xlim) {
               rend |= TRGT;
               if (rbeg&rend) {
                  *z1 = dstw;
                  *d  = 0;
                  *z2 = 0;
                  return;
               }
            }
            else {
               rend &= ~(TLFT + TRGT);
               *z2 = (double)dstw*(double)yend/dely + 0.5;
               if ((rbeg|rend)==0) {
                  *d  = dstw - (*z1 + *z2);
                  goto check;
               }
            }
         }

         if (rend&TRGT) {

            yclip = yend - (int)(dely*(double)(xend-xlim)/delx);
            rend &= ~TRGT;
            if (yclip < 0) {
               rend |= TTOP;
               if (rbeg&rend) {
                  *z1 = 0;
                  *d  = 0;
                  *z2 = dstw;
                  return;
               }
            }
            else if (yclip > ylim) {
               rend |= TBOT;
               if (rbeg&rend) {
                  *z1 = 0;
                  *d  = 0;
                  *z2 = dstw;
                  return;
               }
            }
            else {
               rend &= ~(TTOP + TBOT);
               *z2 = (double)dstw*(double)(xend-xlim)/delx + 0.5;
               if ((rbeg|rend)==0) {
                  *d  = dstw - (*z1 + *z2);
                  goto check;
               }
            }
         }

         if (rend&TBOT) {

            xclip = xend - (int)(delx*(double)(yend-ylim)/dely);
            rend &= ~TTOP;
            if (xclip < 0) {
               rend |= TLFT;
               if (rbeg&rend) {
                  *z1 = 0;
                  *d  = 0;
                  *z2 = dstw;
                  return;
               }
            }
            else if (xclip > xlim) {
               rend |= TRGT;
               if (rbeg&rend) {
                  *z1 = 0;
                  *d  = 0;
                  *z2 = dstw;
                  return;
               }
            }
            else {
               rend &= ~(TLFT + TRGT);
               *z2 = (double)dstw*(double)(yend-ylim)/dely + 0.5;
               if ((rbeg|rend)==0) {
                  *d  = dstw - (*z1 + *z2);
                  goto check;
               }
            }
         }
      }

      *d = (dstw - (*z1 + *z2));

check:;

      // here we do an explicit check of the loop counts to
      // ensure that the data pels fall within the source

      // note that the calculation of these deltas uses
      // (dstw - 1), since this is the number of intervals
      // between the pts (dstw in number) of the scanline

      int idelx = (xend - xbeg) / (dstw - 1);
      int idely = (yend - ybeg) / (dstw - 1);

      // advance (xbeg, ybeg) to the start of data pels, then
      // add pre-zeroes until you're inside the src frame
      xbeg += (*z1)*idelx;
      ybeg += (*z1)*idely;

      while (((xbeg>=0)&&(xbeg<=xlim)&&(ybeg>=0)&&(ybeg<=ylim))&&(*z1 > 0)) {
         (*z1)--;
         (*d )++;
         xbeg -= idelx;
         ybeg -= idely;
      }

      while (((xbeg<0)||(xbeg>xlim)||(ybeg<0)||(ybeg>ylim))&&(*d > 0)) {
         (*z1)++;
         (*d )--;
         xbeg += idelx;
         ybeg += idely;
      }

      // advance (xbeg, ybeg) to the end of data pels, then
      // add post-zeroes until you're inside the src frame
      xbeg += (*d)*idelx;
      ybeg += (*d)*idely;

      while (((xbeg<0)||(xbeg>xlim)||(ybeg<0)||(ybeg>ylim))&&(*d > 0)) {
         (*z2)++;
         (*d )--;
         xbeg -= idelx;
         ybeg -= idely;
      }

      while (((xbeg>=0)&&(xbeg<=xlim)&&(ybeg>=0)&&(ybeg<=ylim))&&(*z2 > 0)) {
         (*z2)--;
         (*d )++;
         xbeg += idelx;
         ybeg += idely;
      }
   }
}

