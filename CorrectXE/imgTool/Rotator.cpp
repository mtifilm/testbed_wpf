//
//  implementation file for class CRotator
//
#include <stdio.h>
#include <stdlib.h>

#include "IniFile.h"

#include "ImageFormat3.h"
#include "machine.h"
#include "MTImalloc.h"
#include <math.h>
#include "Rotator.h"
#include "SysInfo.h"
#ifdef __sgi
  #include <sys/sysmp.h>
  #include <unistd.h>
#endif

#ifdef __linux
#include "MTIio.h"
#endif

// define this for debugging without multithreading
////#define NO_MULTI

#define SCALER 0x8000
#define SHIFTR 15

// define this to force the registration of the exact
// scan to give a smooth gray field in the reset pos
#define SMOOTH_POSNEG

//////////////////////////////////////////////////////////////
//
//                  C R o t a t o r
//
//        Author: Kurt Tolksdorf
//        Begun:  12/15/06
//        Modified: 12/03/08
//
// Transform engine for 16-bit intermediates, to be used by Paint.
// Transforms a source frame buffer about its center and places the
// result into a destination frame buffer of the same size as the
// original intermediate. Independent stretch factors can be spe-
// cified for x- and y-axes. A skew parameter determines the extent
// of skewing of the transformed source buffer.
//
// CRotator is now responsible for all simple translations of the
// import frame, which were originally done using explicit offsets.
//
// Algorithm uses Cohn-Sutherland clipping to avoid checking the
// registration of every source pixel with the source frame.
//
// CRotator is multithreaded for speed.

CRotator::CRotator(int numStripes)
{
   nStripes = numStripes;

   if (nStripes == 0) {
      nStripes = SysInfo::AvailableProcessorCoreCount();
   }

#ifdef NO_MULTI
   nStripes = 2;
#endif

   tsThread.PrimaryFunction   = (void (*)(void *,int))&null;
   tsThread.SecondaryFunction = NULL;
   tsThread.CallBackFunction  = NULL;
   tsThread.vpApplicationData = this;
   tsThread.iNThread          = nStripes;
   tsThread.iNJob             = nStripes;

#ifndef NO_MULTI
   if (nStripes > 1) {
      int iRet = MThreadAlloc(&tsThread);
      if (iRet) {
         TRACE_0(errout << "Rotator: MThreadAlloc failed, iRet=" << iRet);
      }
   }
#endif

}

CRotator::~CRotator()
{

#ifndef NO_MULTI
   if (nStripes > 1) {
      int iRet = MThreadFree(&tsThread);
      if (iRet) {
         TRACE_0(errout << "Rotator: MThreadFree failed, iRet=" << iRet);
      }
   }
#endif

}

void CRotator::deriveForwardMatrix()
{
   //
   // |        cos * strX           sin * strY + cos * strX * skew   |
   // |                                                              |
   // |                                                              |
   // |       -sin * strX           cos * strY - sin * strX * skew   |
   //

   double fcos = cos(angle);
   double fsin = sin(angle);

   fwd00 =  fcos*stretchX;
   fwd01 =  fsin*stretchY + skew*fcos*stretchX;
   fwd10 = -fsin*stretchX;
   fwd11 =  fcos*stretchY - fsin*stretchX*skew;
}

void CRotator::deriveInverseMatrix()
{
   //
   // |  cos/strX - skew * sin / strY     -sin/strX - skew * cos / strY   |
   // |                                                                   |
   // |                                                                   |
   // |             sin / strY                        cos / strY          |
   //

   double fcos = cos(angle);
   double fsin = sin(angle);

   bwd00 =  fcos/stretchX - skew*fsin/stretchY;
   bwd01 = -fsin/stretchX - skew*fcos/stretchY;
   bwd10 =  fsin/stretchY;
   bwd11 =  fcos/stretchY;
}

void CRotator::transformPoint(int xin, int yin, int& xout, int& yout)
{
   double xfm = (double)(xin - frmWdth/2);
   double yfm = (double)(yin - frmHght/2);

   xout = SCALER*(fwd00*xfm + fwd01*yfm + frmWdth/2 + xoffs);
   yout = SCALER*(fwd10*xfm + fwd11*yfm + frmHght/2 + yoffs);
}

void CRotator::inverseTransformPoint(int xin, int yin, int& xout, int& yout)
{
   double xfm = (double)(xin - frmWdth/2) - xoffs;
   double yfm = (double)(yin - frmHght/2) - yoffs;

   xout = SCALER*(bwd00*xfm + bwd01*yfm + frmWdth/2);
   yout = SCALER*(bwd10*xfm + bwd11*yfm + frmHght/2);
}

void CRotator::initializeForFastScan()
{
   deriveInverseMatrix();

   // pel pairs
   int xspan = frmWdth/2 - 1;
   int yspan = frmHght/2 - 1;

   // develop the inv-transformed corners of the
   // frame multiplied by the scale factor 2**15

   inverseTransformPoint(        1,         1, xlt, ylt);
   inverseTransformPoint(frmWdth-1,         1, xrt, yrt);
   inverseTransformPoint(        1, frmHght-1, xlb, ylb);

   // twice the rowwise scaled unit vectors / pel-pair
   rowdelx = (xrt - xlt) / xspan;
   rowdely = (yrt - ylt) / xspan;

   // twice the colwise scaled unit vectors / pel-pair
   coldelx = (xlb - xlt) / yspan;
   coldely = (ylb - ylt) / yspan;

#ifdef OLD
   double cosine = cos(angle);
   double sine   = sin(angle);

   int cosstrx   = SCALER*cosine/stretchX;
   int sinstrx   = SCALER*sine  /stretchX;
   int cosstry   = SCALER*cosine/stretchY;
   int sinstry   = SCALER*sine  /stretchY;
   int cosstrydx = SCALER*cosine*skew/stretchY;
   int sinstrydx = SCALER*sine  *skew/stretchY;

   // span is distance from midpt of
   // first pel to midpt of last pel
   int span = dstWH - 2;

   // (lft, top) is transform of (-span/2, -span/2)
   xlt = ((-span)*cosstrx   - (-span)*sinstrx -
          (-span)*sinstrydx - (-span)*cosstrydx) / 2;

   ylt = ((-span)*sinstry + (-span)*cosstry) / 2;

   // (rgt, top) is transform of ( span/2, -span/2)
   xrt = (( span)*cosstrx   - (-span)*sinstrx -
          ( span)*sinstrydx - (-span)*cosstrydx) / 2;

   yrt = (( span)*sinstry + (-span)*cosstry) / 2;

   // (lft, bot) is transform of (-span/2, span/2)
   xlb = ((-span)*cosstrx   - ( span)*sinstrx -
          (-span)*sinstrydx - ( span)*cosstrydx) / 2;

   ylb = ((-span)*sinstry + ( span)*cosstry) / 2;

   // the rowwise scaled unit vectors / pel
   rowdelx = (xrt - xlt) / span;
   rowdely = (yrt - ylt) / span;

   // the colwise scaled unit vectors / pel
   coldelx = (xlb - xlt) / span;
   coldely = (ylb - ylt) / span;

   // translate these values to the coord
   // system with (0,0) at upper left
   xlt += frmWdth<<(SHIFTR-1);
   ylt += frmHght<<(SHIFTR-1);

   xrt += frmWdth<<(SHIFTR-1);
   yrt += frmHght<<(SHIFTR-1);

   xlb += frmWdth<<(SHIFTR-1);
   ylb += frmHght<<(SHIFTR-1);
#endif
}

void CRotator::initializeForExactScan()
{
   deriveInverseMatrix();

   // pels
   int xspan = frmWdth - 1;
   int yspan = frmHght - 1;

   // develop the inv-transformed corners of the
   // frame multiplied by the scale factor 2**15

   inverseTransformPoint(        0,         0, xlt, ylt);
   inverseTransformPoint(frmWdth-1,         0, xrt, yrt);
   inverseTransformPoint(        0, frmHght-1, xlb, ylb);

   // the rowwise scaled unit vectors / pel
   rowdelx = (xrt - xlt) / xspan;
   rowdely = (yrt - ylt) / xspan;

   // the colwise scaled unit vectors / pel
   coldelx = (xlb - xlt) / yspan;
   coldely = (ylb - ylt) / yspan;

#ifdef OLD
   double cosine = cos(angle);
   double sine   = sin(angle);

   int cosstrx   = SCALER*cosine/stretchX;
   int sinstrx   = SCALER*sine  /stretchX;
   int cosstry   = SCALER*cosine/stretchY;
   int sinstry   = SCALER*sine  /stretchY;
   int cosstrydx = SCALER*cosine*skew/stretchY;
   int sinstrydx = SCALER*sine  *skew/stretchY;

   int span = (dstWH - 1);

#ifdef SMOOTH_POSNEG
   // This lays a grid which in the reset position
   // stretches from (0,0) to (dstWH-1, dstWH-1).
   // The Pos-Neg display looks perfectly smooth.

   int xspan1 = frmWdth/2;
   int xspan2 = frmWdth/2 - 1;
   int yspan1 = frmHght/2;
   int yspan2 = frmHght/2 - 1;

   // (lft, top) is transform of (-xspan/2, -yspan/2)
   xlt = (-span1)*cosstrx   - (-span1)*sinstrx -
         (-span1)*sinstrydx - (-span1)*cosstrydx;

   ylt = (-span1)*sinstry + (-span1)*cosstry;

   // (rgt, top) is transform of ( span/2, -span/2)
   xrt = ( span2)*cosstrx   - (-span1)*sinstrx -
         ( span2)*sinstrydx - (-span1)*cosstrydx;

   yrt = ( span2)*sinstry + (-span1)*cosstry;

   // (lft, bot) is transform of (-span/2, span/2)
   xlb = (-span1)*cosstrx   - ( span2)*sinstrx -
         (-span1)*sinstrydx - ( span2)*cosstrydx;

   ylb = (-span1)*sinstry + ( span2)*cosstry;
#else
   // This lays a grid with dstWH pts and one less
   // intervals, between the terminal half-pixels.
   // Because we have bilinear interpolation, this
   // is slightly inexact when in the reset position,
   // making the Pos-Neg display slightly imperfect.

   // (lft, top) is transform of (-span/2, -span/2)
   xlt = ((-span)*cosstrx   - (-span)*sinstrx -
          (-span)*sinstrydx - (-span)*cosstrydx) / 2;

   ylt = ((-span)*sinstry + (-span)*cosstry)/2;

   // (rgt, top) is transform of ( span/2, -span/2)
   xrt = (( span)*cosstrx   - (-span)*sinstrx -
          ( span)*sinstrydx - (-span)*cosstrydx) / 2;

   yrt = (( span)*sinstry + (-span)*cosstry) / 2;

   // (lft, bot) is transform of (-span/2, span/2)
   xlb = ((-span)*cosstrx   - ( span)*sinstrx -
          (-span)*sinstrydx - ( span)*cosstrydx) / 2;

   ylb = ((-span)*sinstry + ( span)*cosstry) / 2;
#endif

   // the rowwise scaled unit vectors / pel
   rowdelx = (xrt - xlt) / span;
   rowdely = (yrt - ylt) / span;

   // the colwise scaled unit vectors / pel
   coldelx = (xlb - xlt) / span;
   coldely = (ylb - ylt) / span;

   // scan-grid translation is neg of offsets
   int rowoffx = -(int)((double)rowdelx * xoffs);
   int rowoffy = -(int)((double)rowdely * xoffs);

   int coloffx = -(int)((double)coldelx * yoffs);
   int coloffy = -(int)((double)coldely * yoffs);

   // apply the fractional-pixel translation
   xlt += rowoffx + coloffx;
   ylt += rowoffy + coloffy;

   xrt += rowoffx + coloffx;
   yrt += rowoffy + coloffy;

   xlb += rowoffx + coloffx;
   ylb += rowoffy + coloffy;

   // translate these values to the coord
   // system with (0,0) at upper left
   xlt += frmWdth<<(SHIFTR-1);
   ylt += frmHght<<(SHIFTR-1);

   xrt += frmWdth<<(SHIFTR-1);
   yrt += frmHght<<(SHIFTR-1);

   xlb += frmWdth<<(SHIFTR-1);
   ylb += frmHght<<(SHIFTR-1);
#endif
}

void CRotator::rotateFast(MTI_UINT16 *dst, MTI_UINT16 *src,
                          const CImageFormat *srcfmt,
                          double ang,
                          double strx, double stry,
                          double skw,
                          double xoff, double yoff)
{
   dstpels          = dst;
   srcpels          = src;
   frmWdth          = srcfmt->getPixelsPerLine();
   frmHght          = srcfmt->getLinesPerFrame();
   angle            = ang;
   stretchX         = strx;
   stretchY         = stry;
   skew             = skw;
   xoffs            = xoff;
   yoffs            = yoff;

   if ((fabs(strx) < .0625)||(fabs(stry) < .0625)) return;

   switch(srcfmt->getPixelComponents()) {

      case IF_PIXEL_COMPONENTS_YUV422:
      case IF_PIXEL_COMPONENTS_YUV444:

         tsThread.PrimaryFunction   = (void (*)(void *,int))&rotateFastYUV_8;
         if (srcfmt->getBitsPerComponent() == 10)
            tsThread.PrimaryFunction   = (void (*)(void *,int))&rotateFastYUV_10;

      break;

      case IF_PIXEL_COMPONENTS_RGB:
      case IF_PIXEL_COMPONENTS_BGR:
      case IF_PIXEL_COMPONENTS_Y:
      case IF_PIXEL_COMPONENTS_YYY:

         switch(srcfmt->getAlphaMatteType()) {

            case IF_ALPHA_MATTE_1_BIT:

               tsThread.PrimaryFunction   = (void (*)(void *,int))&rotateFastRGB_A1;

            break;

            case IF_ALPHA_MATTE_2_BIT_EMBEDDED:

               tsThread.PrimaryFunction   = (void (*)(void *,int))&rotateFastRGB_A2;

            break;

            case IF_ALPHA_MATTE_8_BIT:

               tsThread.PrimaryFunction   = (void (*)(void *,int))&rotateFastRGB_A8;

            break;

            case IF_ALPHA_MATTE_16_BIT:

               tsThread.PrimaryFunction   = (void (*)(void *,int))&rotateFastRGB_A16;

            break;

            default:

               tsThread.PrimaryFunction   = (void (*)(void *,int))&rotateFastRGB;

            break;
         }

      break;

      case IF_PIXEL_COMPONENTS_RGBA:
      case IF_PIXEL_COMPONENTS_BGRA:

         MTIassert(srcfmt->getAlphaMatteType() == IF_ALPHA_MATTE_8_BIT_INTERLEAVED
                || srcfmt->getAlphaMatteType() == IF_ALPHA_MATTE_10_BIT_INTERLEAVED
                || srcfmt->getAlphaMatteType() == IF_ALPHA_MATTE_16_BIT_INTERLEAVED);

         switch(srcfmt->getAlphaMatteType()) {

            case IF_ALPHA_MATTE_8_BIT_INTERLEAVED:

               tsThread.PrimaryFunction   = (void (*)(void *,int))&rotateFastRGB_A8;

            break;

            case IF_ALPHA_MATTE_10_BIT_INTERLEAVED:
            case IF_ALPHA_MATTE_16_BIT_INTERLEAVED:

               tsThread.PrimaryFunction   = (void (*)(void *,int))&rotateFastRGB_A16;

            break;

            default:

               // Really an error!
               tsThread.PrimaryFunction   = (void (*)(void *,int))&rotateFastRGB;

            break;
         }

      break;

//      case IF_PIXEL_COMPONENTS_LUMINANCE:
//
//         //tsThread.PrimaryFunction   = (void (*)(void *,int))&rotateFastMON;
//
//	  break;

	  default:
		break;
   }

   // initialize for the transformed scan
   initializeForFastScan();

   // rotate the frame by stripe
#ifdef NO_MULTI
   for (int i=0;i<nStripes;i++)
      tsThread.PrimaryFunction(this,i);
#else
   if (nStripes == 1) {

      // if we're only doing one stripe, call the function directly
      tsThread.PrimaryFunction(this,0);
   }
   else {

      // fire up one thread for each stripe
      int iRet = MThreadStart(&tsThread);
      if (iRet) {
         TRACE_0(errout << "Rotator: MThreadStart failed, iRet=" << iRet);
      }
   }
#endif

}

void CRotator::rotateExact(MTI_UINT16 *dst, MTI_UINT16 *src,
                           const CImageFormat *srcfmt,
                           double ang,
                           double strx, double stry,
                           double skw,
                           double xoff, double yoff)
{
   dstpels          = dst;
   srcpels          = src;
   frmWdth          = srcfmt->getPixelsPerLine();
   frmHght          = srcfmt->getLinesPerFrame();
   angle            = ang;
   stretchX         = strx;
   stretchY         = stry;
   skew             = skw;
   xoffs            = xoff;
   yoffs            = yoff;

   if ((fabs(strx) < .0625)||(fabs(stry) < .06255)) return;

   switch(srcfmt->getPixelComponents()) {

      case IF_PIXEL_COMPONENTS_YUV422:
      case IF_PIXEL_COMPONENTS_YUV444:

         tsThread.PrimaryFunction   = (void (*)(void *,int))&rotateExactYUV_8;
         if (srcfmt->getBitsPerComponent() == 10)
            tsThread.PrimaryFunction   = (void (*)(void *,int))&rotateExactYUV_10;

      break;

      case IF_PIXEL_COMPONENTS_RGB:
      case IF_PIXEL_COMPONENTS_BGR:
      case IF_PIXEL_COMPONENTS_Y:
      case IF_PIXEL_COMPONENTS_YYY:

         switch(srcfmt->getAlphaMatteType()) {

            case IF_ALPHA_MATTE_1_BIT:

               tsThread.PrimaryFunction   = (void (*)(void *,int))&rotateExactRGB_A1;

            break;

            case IF_ALPHA_MATTE_2_BIT_EMBEDDED:

               tsThread.PrimaryFunction   = (void (*)(void *,int))&rotateExactRGB_A2;

            break;

            case IF_ALPHA_MATTE_8_BIT:

               tsThread.PrimaryFunction   = (void (*)(void *,int))&rotateExactRGB_A8;

            break;

            case IF_ALPHA_MATTE_16_BIT:

               tsThread.PrimaryFunction   = (void (*)(void *,int))&rotateExactRGB_A16;

            break;

            default:

               tsThread.PrimaryFunction   = (void (*)(void *,int))&rotateExactRGB;

            break;
         }

      break;

      case IF_PIXEL_COMPONENTS_RGBA:
      case IF_PIXEL_COMPONENTS_BGRA:

         MTIassert(srcfmt->getAlphaMatteType() == IF_ALPHA_MATTE_8_BIT_INTERLEAVED
                || srcfmt->getAlphaMatteType() == IF_ALPHA_MATTE_10_BIT_INTERLEAVED
                || srcfmt->getAlphaMatteType() == IF_ALPHA_MATTE_16_BIT_INTERLEAVED);

         switch(srcfmt->getAlphaMatteType()) {

            case IF_ALPHA_MATTE_8_BIT_INTERLEAVED:

               tsThread.PrimaryFunction   = (void (*)(void *,int))&rotateExactRGB_A8;

            break;

            case IF_ALPHA_MATTE_10_BIT_INTERLEAVED:
            case IF_ALPHA_MATTE_16_BIT_INTERLEAVED:

               tsThread.PrimaryFunction   = (void (*)(void *,int))&rotateExactRGB_A16;

            break;

            default:

               // Really an error!
               tsThread.PrimaryFunction   = (void (*)(void *,int))&rotateExactRGB;

            break;
         }

      break;
//
//      case IF_PIXEL_COMPONENTS_LUMINANCE:
//
//         //tsThread.PrimaryFunction   = (void (*)(void *,int))&rotateExactMON;
//
//	  break;

	  default:
		break;
   }

   // initialize for the transformed scan
   initializeForExactScan();

   // rotate the frame by stripe
#ifdef NO_MULTI
   for (int i=0;i<nStripes;i++)
      tsThread.PrimaryFunction(this,i);
#else
   if (nStripes == 1) {

      // if we're only doing one stripe, call the function directly
      tsThread.PrimaryFunction(this,0);
   }
   else {

      // fire up one thread for each stripe
      int iRet = MThreadStart(&tsThread);
      if (iRet) {
         TRACE_0(errout << "Rotator: MThreadStart failed, iRet=" << iRet);
      }
   }
#endif
}

// takes begin and end points and generates loop counts
void CRotator::rowLoopCounts(int xbeg, int ybeg,    // first endpt of scanline
                             int xend, int yend,    // last  endpt of scanline
                             int xlim, int ylim,    // size of clipping rect
                             int dstw,              // total pts in scanline
                             int *z1, int *d, int *z2)
{
   int rbeg = 0;
   if (xbeg < 0)    rbeg |= TLFT;
   if (xbeg > xlim) rbeg |= TRGT;
   if (ybeg < 0)    rbeg |= TTOP;
   if (ybeg > ylim) rbeg |= TBOT;

   int rend = 0;
   if (xend < 0)    rend |= TLFT;
   if (xend > xlim) rend |= TRGT;
   if (yend < 0)    rend |= TTOP;
   if (yend > ylim) rend |= TBOT;

   if ((rbeg&rend) != 0) { // the row doesn't intersect the source frame

      *z1 = dstw;
      *d  = 0;
      *z2 = 0;
   }
   else {

      double delx  = xend - xbeg;
      double dely  = yend - ybeg;

      int xclip, yclip;

      *z1 = 0;
      if (rbeg) {

         if (rbeg&TLFT) {

            yclip = ybeg - (int)(dely*(double)xbeg/delx);
            rbeg &= ~TLFT;
            if (yclip < 0) {
               rbeg |= TTOP;
               if (rbeg&rend) {
                  *z1 = dstw;
                  *d  = 0;
                  *z2 = 0;
                  return;
               }
            }
            else if (yclip > ylim) {
               rbeg |= TBOT;
               if (rbeg&rend) {
                  *z1 = dstw;
                  *d  = 0;
                  *z2 = 0;
                  return;
               }
            }
            else {
               rbeg &= ~(TTOP + TBOT);
               *z1 = -(double)dstw*(double)xbeg/delx + 0.5;
               if ((rbeg|rend)==0) {
                  *d  = dstw - *z1;
                  *z2 = 0;
                  goto check;
               }
            }
         }

         if (rbeg&TTOP) {

            xclip = xbeg - (int)(delx*(double)ybeg/dely);
            rbeg &= ~TTOP;
            if (xclip < 0) {
               rbeg |= TLFT;
               if (rbeg&rend) {
                  *z1 = dstw;
                  *d  = 0;
                  *z2 = 0;
                  return;
               }
            }
            else if (xclip > xlim) {
               rbeg |= TRGT;
               if (rbeg&rend) {
                  *z1 = dstw;
                  *d  = 0;
                  *z2 = 0;
                  return;
               }
            }
            else {
               rbeg &= ~(TLFT + TRGT);
               *z1 = -(double)dstw*(double)ybeg/dely + 0.5;
               if ((rbeg|rend)==0) {
                  *d  = dstw - *z1;
                  *z2 = 0;
                  goto check;
               }
            }
         }

         if (rbeg&TRGT) {

            yclip = ybeg - (int)(dely*(double)(xbeg-xlim)/delx);
            rbeg &= ~TRGT;
            if (yclip < 0) {
               rbeg |= TTOP;
               if (rbeg&rend) {
                  *z1 = dstw;
                  *d  = 0;
                  *z2 = 0;
                  return;
               }
            }
            else if (yclip > ylim) {
               rbeg |= TBOT;
               if (rbeg&rend) {
                  *z1 = dstw;
                  *d  = 0;
                  *z2 = 0;
                  return;
               }
            }
            else {
               rbeg &= ~(TTOP + TBOT);
               *z1 = -(double)dstw*(double)(xbeg-xlim)/delx + 0.5;
               if ((rbeg|rend)==0) {
                  *d  = dstw - *z1;
                  *z2 = 0;
                  goto check;
               }
            }
         }

         if (rbeg&TBOT) {

            xclip = xbeg - (int)(delx*(double)(ybeg-ylim)/dely);
            rbeg &= ~TTOP;
            if (xclip < 0) {
               rbeg |= TLFT;
               if (rbeg&rend) {
                  *z1 = dstw;
                  *d  = 0;
                  *z2 = 0;
                  return;
               }
            }
            else if (xclip > xlim)
               rbeg |= TRGT;
               if (rbeg&rend) {
                  *z1 = dstw;
                  *d  = 0;
                  *z2 = 0;
                  return;
            }
            else {
               rbeg &= (TLFT + TRGT);
               *z1 = (double)dstw*(double)(ylim-ybeg)/dely + 0.5;
               if ((rbeg|rend)==0) {
                  *d  = dstw - *z1;
                  *z2 = 0;
                  goto check;
               }
            }
         }
      }

      *z2 = 0;
      if (rend) {

         if (rend&TLFT) {

            yclip = yend - (int)(dely*(double)xend/delx);
            rend &= ~TLFT;
            if (yclip < 0) {
               rend |= TTOP;
               if (rbeg&rend) {
                  *z1 = 0;
                  *d  = 0;
                  *z2 = dstw;
                  return;
               }
            }
            else if (yclip > ylim) {
               rend |= TBOT;
               if (rbeg&rend) {
                  *z1 = 0;
                  *d  = 0;
                  *z2 = dstw;
                  return;
               }
            }
            else {
               rend &= ~(TTOP + TBOT);
               *z2 = (int)dstw*(double)xend/delx + 0.5;
               if ((rbeg|rend)==0) {
                  *d  = dstw - (*z1 + *z2);
                  goto check;
               }
            }
         }

         if (rend&TTOP) {

            xclip = xend - (int)(delx*(double)yend/dely);
            rend &= ~TTOP;
            if (xclip < 0) {
               rend |= TLFT;
               if (rbeg&rend) {
                  *z1 = dstw;
                  *d  = 0;
                  *z2 = 0;
                  return;
               }
            }
            else if (xclip > xlim) {
               rend |= TRGT;
               if (rbeg&rend) {
                  *z1 = dstw;
                  *d  = 0;
                  *z2 = 0;
                  return;
               }
            }
            else {
               rend &= ~(TLFT + TRGT);
               *z2 = (double)dstw*(double)yend/dely + 0.5;
               if ((rbeg|rend)==0) {
                  *d  = dstw - (*z1 + *z2);
                  goto check;
               }
            }
         }

         if (rend&TRGT) {

            yclip = yend - (int)(dely*(double)(xend-xlim)/delx);
            rend &= ~TRGT;
            if (yclip < 0) {
               rend |= TTOP;
               if (rbeg&rend) {
                  *z1 = 0;
                  *d  = 0;
                  *z2 = dstw;
                  return;
               }
            }
            else if (yclip > ylim) {
               rend |= TBOT;
               if (rbeg&rend) {
                  *z1 = 0;
                  *d  = 0;
                  *z2 = dstw;
                  return;
               }
            }
            else {
               rend &= ~(TTOP + TBOT);
               *z2 = (double)dstw*(double)(xend-xlim)/delx + 0.5;
               if ((rbeg|rend)==0) {
                  *d  = dstw - (*z1 + *z2);
                  goto check;
               }
            }
         }

         if (rend&TBOT) {

            xclip = xend - (int)(delx*(double)(yend-ylim)/dely);
            rend &= ~TTOP;
            if (xclip < 0) {
               rend |= TLFT;
               if (rbeg&rend) {
                  *z1 = 0;
                  *d  = 0;
                  *z2 = dstw;
                  return;
               }
            }
            else if (xclip > xlim) {
               rend |= TRGT;
               if (rbeg&rend) {
                  *z1 = 0;
                  *d  = 0;
                  *z2 = dstw;
                  return;
               }
            }
            else {
               rend &= ~(TLFT + TRGT);
               *z2 = (double)dstw*(double)(yend-ylim)/dely + 0.5;
               if ((rbeg|rend)==0) {
                  *d  = dstw - (*z1 + *z2);
                  goto check;
               }
            }
         }
      }

      *d = (dstw - (*z1 + *z2));

check:;

      // here we do an explicit check of the loop counts to
      // ensure that the data pels fall within the source

      // note that the calculation of these deltas uses
      // (dstw - 1), since this is the number of intervals
      // between the pts (dstw in number) of the scanline

      int idelx = (xend - xbeg) / (dstw - 1);
      int idely = (yend - ybeg) / (dstw - 1);

      // advance (xbeg, ybeg) to the start of data pels, then
      // add pre-zeroes until you're inside the src frame
      xbeg += (*z1)*idelx;
      ybeg += (*z1)*idely;

      while (((xbeg>=0)&&(xbeg<=xlim)&&(ybeg>=0)&&(ybeg<=ylim))&&(*z1 > 0)) {
         (*z1)--;
         (*d )++;
         xbeg -= idelx;
         ybeg -= idely;
      }

      while (((xbeg<0)||(xbeg>xlim)||(ybeg<0)||(ybeg>ylim))&&(*d > 0)) {
         (*z1)++;
         (*d )--;
         xbeg += idelx;
         ybeg += idely;
      }

      // advance (xbeg, ybeg) to the end of data pels, then
      // add post-zeroes until you're inside the src frame
      xbeg += (*d)*idelx;
      ybeg += (*d)*idely;

      while (((xbeg<0)||(xbeg>xlim)||(ybeg<0)||(ybeg>ylim))&&(*d > 0)) {
         (*z2)++;
         (*d )--;
         xbeg -= idelx;
         ybeg -= idely;
      }

      while (((xbeg>=0)&&(xbeg<=xlim)&&(ybeg>=0)&&(ybeg<=ylim))&&(*z2 > 0)) {
         (*z2)--;
         (*d )++;
         xbeg += idelx;
         ybeg += idely;
      }
   }
}

void CRotator::null(void *v, int iJob)
{
}

void CRotator::rotateFastYUV_8(void *v, int iJob)
{
   CRotator *vp = (CRotator *) v;

   MTI_UINT16 *dst      = vp->dstpels;
   int dstpitch         = vp->frmWdth*3;
   int frmWdth          = vp->frmWdth;
   int frmHght          = vp->frmHght;

   MTI_UINT16 *srcPels  = vp->srcpels;
   int frameWidth       = vp->frmWdth;
   int xlim             = (vp->frmWdth - 2)<<SHIFTR; // 2 pixels per inner loop iteration
   int ylim             = (vp->frmHght - 2)<<SHIFTR; // 2 rows per outer loop iteration

   int rowsPerStripe = (frmHght / vp->nStripes + 1)&0xfffffffe;

   int begRow = iJob*rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      //stripeRows = vp->dstWH - iJob*rowsPerStripe;
      stripeRows = vp->frmHght - iJob*rowsPerStripe;

   int xlt = vp->xlt;
   int ylt = vp->ylt;
   int xrt = vp->xrt;
   int yrt = vp->yrt;

   int rowdelx = vp->rowdelx;
   int rowdely = vp->rowdely;
   int coldelx = vp->coldelx;
   int coldely = vp->coldely;

   xlt += (iJob * rowsPerStripe * coldelx) / 2;
   ylt += (iJob * rowsPerStripe * coldely) / 2;

   xrt += (iJob * rowsPerStripe * coldelx) / 2;
   yrt += (iJob * rowsPerStripe * coldely) / 2;
   dst += iJob * rowsPerStripe * dstpitch;

   for (int i=begRow;i<(begRow+stripeRows);i+=2) {

      int xrow = xlt;
      int yrow = ylt;

      int xend = xrt;
      int yend = yrt;

      int preBlacks, framePels, postBlacks;

      // derive the loop counts using a Cohn-Sutherland clipping algorithm
      rowLoopCounts(xrow, yrow, xend, yend,
                    xlim, ylim,
                    frmWdth/2,
                    &preBlacks, &framePels, &postBlacks);

      // the general output row has some initial black pels,
      // followed by some data pels, followed by black pels

      for (int j=0;j<preBlacks;j++) {

         dst[0]          = 0;
         dst[1]          = 128;
         dst[2]          = 128;

         dst[3]          = 0;
         dst[4]          = 128;
         dst[5]          = 128;

         dst[dstpitch]   = 0;
         dst[dstpitch+1] = 128;
         dst[dstpitch+2] = 128;

         dst[dstpitch+3] = 0;
         dst[dstpitch+4] = 128;
         dst[dstpitch+5] = 128;

         // advancing by 2 pels
         dst += 6;
      }

      xrow += preBlacks*rowdelx;
      yrow += preBlacks*rowdely;

      for (int j=0;j<framePels;j++) {

         MTI_UINT16 *src = srcPels + ((yrow>>SHIFTR)*frameWidth + (xrow>>SHIFTR))*3;

         MTI_UINT16 val = src[0];

         dst[0]       = val;
         dst[3]       = val;
         dst[dstpitch]   = val;
         dst[dstpitch+3] = val;

         val = src[1];

         dst[1]       = val;
         dst[4]       = val;
         dst[dstpitch+1] = val;
         dst[dstpitch+4] = val;

         val = src[2];

         dst[2]       = val;
         dst[5]       = val;
         dst[dstpitch+2] = val;
         dst[dstpitch+5] = val;

         xrow += rowdelx;
         yrow += rowdely;

         // advancing by 2 pels
         dst += 6;
      }

      for (int j=0;j<postBlacks;j++) {

         dst[0]          = 0;
         dst[1]          = 128;
         dst[2]          = 128;

         dst[3]          = 0;
         dst[4]          = 128;
         dst[5]          = 128;

         dst[dstpitch]   = 0;
         dst[dstpitch+1] = 128;
         dst[dstpitch+2] = 128;

         dst[dstpitch+3] = 0;
         dst[dstpitch+4] = 128;
         dst[dstpitch+5] = 128;

         // advancing by 2 pels
         dst += 6;
      }

      xlt += coldelx;
      ylt += coldely;

      xrt += coldelx;
      yrt += coldely;

      // advancing by 2 rows (one
      // accounted for by above)
      dst += dstpitch;
   }
}

void CRotator::rotateExactYUV_8(void *v, int iJob)
{
   CRotator *vp = (CRotator *) v;

   MTI_UINT16 *dst      = vp->dstpels;
   int dstpitch         = vp->frmWdth*3;
   int frmWdth          = vp->frmWdth;
   int frmHght          = vp->frmHght;

   MTI_UINT16 *srcPels  = vp->srcpels;
   int frameWidth       = vp->frmWdth;
   int srcpitch         = vp->frmWdth*3;
   int xlim             = (vp->frmWdth - 1)<<SHIFTR;
   int ylim             = (vp->frmHght - 1)<<SHIFTR;

   int rowsPerStripe = frmHght / vp->nStripes;

   int begRow = iJob*rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      stripeRows = vp->frmHght - iJob*rowsPerStripe;

   int xlt = vp->xlt;
   int ylt = vp->ylt;
   int xrt = vp->xrt;
   int yrt = vp->yrt;

   int rowdelx = vp->rowdelx;
   int rowdely = vp->rowdely;
   int coldelx = vp->coldelx;
   int coldely = vp->coldely;

   xlt += iJob*rowsPerStripe*coldelx;
   ylt += iJob*rowsPerStripe*coldely;

   xrt += iJob*rowsPerStripe*coldelx;
   yrt += iJob*rowsPerStripe*coldely;

   dst += iJob*rowsPerStripe*dstpitch;

   for (int i=begRow;i<(begRow+stripeRows);i++) {

      int xrow = xlt;
      int yrow = ylt;

      int xend = xrt;
      int yend = yrt;

      int preBlacks, framePels, postBlacks;

      // derive the loop counts using a Cohn-Sutherland clipping algorithm
      rowLoopCounts(xrow, yrow, xend, yend,
                    xlim, ylim,
                    frmWdth,
                    &preBlacks, &framePels, &postBlacks);

      // the general output row has some initial black pels,
      // followed by some data pels, followed by black pels

      for (int j=0;j<preBlacks;j++) {

         dst[0]       = 0;
         dst[1]       = 128;
         dst[2]       = 128;

         // advancing by 1 pel
         dst += 3;
      }

      xrow += preBlacks*rowdelx;
      yrow += preBlacks*rowdely;

      for (int j=0;j<framePels;j++) {

         // here we do a bilinear interpolation
         int frx = (xrow>>(SHIFTR-8))&0xff;
         int fry = (yrow>>(SHIFTR-8))&0xff;
         int grx = 256 - frx;
         int gry = 256 - fry;

         MTI_UINT16 *src = srcPels + ((yrow>>SHIFTR)*frameWidth + (xrow>>SHIFTR))*3;

         dst[0] =((src[0         ]*grx + src[3         ]*frx)*gry +
                  (src[0+srcpitch]*grx + src[3+srcpitch]*frx)*fry)>>16;

         dst[1] =((src[1         ]*grx + src[4         ]*frx)*gry +
                  (src[1+srcpitch]*grx + src[4+srcpitch]*frx)*fry)>>16;

         dst[2] =((src[2         ]*grx + src[5         ]*frx)*gry +
                  (src[2+srcpitch]*grx + src[5+srcpitch]*frx)*fry)>>16;

         xrow += rowdelx;
         yrow += rowdely;

         // advancing by 1 pel
         dst += 3;
      }

      for (int j=0;j<postBlacks;j++) {

         dst[0]       = 0;
         dst[1]       = 128;
         dst[2]       = 128;

         // advancing by 1 pel
         dst += 3;
      }

      xlt += coldelx;
      ylt += coldely;

      xrt += coldelx;
      yrt += coldely;
   }
}

void CRotator::rotateFastYUV_10(void *v, int iJob)
{
   CRotator *vp = (CRotator *) v;

   MTI_UINT16 *dst      = vp->dstpels;
   int dstpitch         = vp->frmWdth*3;
   int frmWdth          = vp->frmWdth;
   int frmHght          = vp->frmHght;

   MTI_UINT16 *srcPels  = vp->srcpels;
   int frameWidth       = vp->frmWdth;
   int xlim             = (vp->frmWdth - 1)<<SHIFTR;
   int ylim             = (vp->frmHght - 1)<<SHIFTR;

   int rowsPerStripe = (frmHght / vp->nStripes + 1)&0xfffffffe;

   int begRow = iJob*rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      stripeRows = frmHght - iJob*rowsPerStripe;

   int xlt = vp->xlt;
   int ylt = vp->ylt;
   int xrt = vp->xrt;
   int yrt = vp->yrt;

   int rowdelx = vp->rowdelx;
   int rowdely = vp->rowdely;
   int coldelx = vp->coldelx;
   int coldely = vp->coldely;

   xlt += (iJob*rowsPerStripe*coldelx)/2;
   ylt += (iJob*rowsPerStripe*coldely)/2;

   xrt += (iJob*rowsPerStripe*coldelx)/2;
   yrt += (iJob*rowsPerStripe*coldely)/2;

   dst += iJob*rowsPerStripe*dstpitch;

   for (int i=begRow;i<(begRow+stripeRows);i+=2) {

      int xrow = xlt;
      int yrow = ylt;

      int xend = xrt;
      int yend = yrt;

      int preBlacks, framePels, postBlacks;

      // derive the loop counts using a Cohn-Sutherland clipping algorithm
      rowLoopCounts(xrow, yrow, xend, yend,
                    xlim, ylim,
                    frmWdth/2,
                    &preBlacks, &framePels, &postBlacks);

      int xtest = xrow + preBlacks*rowdelx;
      int ytest = yrow + preBlacks*rowdely;

      if (ytest < 0) {
         ytest = 0;
      }

      // the general output row has some initial black pels,
      // followed by some data pels, followed by black pels

      for (int j=0;j<preBlacks;j++) {

         dst[0]          = 0;
         dst[1]          = 512;
         dst[2]          = 512;

         dst[3]          = 0;
         dst[4]          = 512;
         dst[5]          = 512;

         dst[dstpitch]   = 0;
         dst[dstpitch+1] = 512;
         dst[dstpitch+2] = 512;

         dst[dstpitch+3] = 0;
         dst[dstpitch+4] = 512;
         dst[dstpitch+5] = 512;

         // advancing by 2 pels
         dst += 6;
      }

      xrow += preBlacks*rowdelx;
      yrow += preBlacks*rowdely;

      for (int j=0;j<framePels;j++) {

         MTI_UINT16 *src = srcPels + ((yrow>>SHIFTR)*frameWidth + (xrow>>SHIFTR ))*3;

         MTI_UINT16 val = src[0];

         dst[0]       = val;
         dst[3]       = val;
         dst[dstpitch]   = val;
         dst[dstpitch+3] = val;

         val = src[1];

         dst[1]       = val;
         dst[4]       = val;
         dst[dstpitch+1] = val;
         dst[dstpitch+4] = val;

         val = src[2];

         dst[2]       = val;
         dst[5]       = val;
         dst[dstpitch+2] = val;
         dst[dstpitch+5] = val;

         xrow += rowdelx;
         yrow += rowdely;

         // advancing by 2 pels
         dst += 6;
      }

      for (int j=0;j<postBlacks;j++) {

         dst[0]          = 0;
         dst[1]          = 512;
         dst[2]          = 512;

         dst[3]          = 0;
         dst[4]          = 512;
         dst[5]          = 512;

         dst[dstpitch]   = 0;
         dst[dstpitch+1] = 512;
         dst[dstpitch+2] = 512;

         dst[dstpitch+3] = 0;
         dst[dstpitch+4] = 512;
         dst[dstpitch+5] = 512;

         // advancing by 2 pels
         dst += 6;
      }

      xlt += coldelx;
      ylt += coldely;

      xrt += coldelx;
      yrt += coldely;

      // advancing by 2 rows (one
      // accounted for by above)
      dst += dstpitch;
   }
}

void CRotator::rotateExactYUV_10(void *v, int iJob)
{
   CRotator *vp = (CRotator *) v;

   MTI_UINT16 *dst      = vp->dstpels;
   int dstpitch         = vp->frmWdth*3;
   int frmWdth          = vp->frmWdth;
   int frmHght          = vp->frmHght;

   MTI_UINT16 *srcPels  = vp->srcpels;
   int frameWidth       = vp->frmWdth;
   int srcpitch         = vp->frmWdth*3;
   int xlim             = (vp->frmWdth - 1)<<SHIFTR;
   int ylim             = (vp->frmHght - 1)<<SHIFTR;

   int rowsPerStripe = frmHght / vp->nStripes;

   int begRow = iJob*rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      stripeRows = frmHght - iJob*rowsPerStripe;

   int xlt = vp->xlt;
   int ylt = vp->ylt;
   int xrt = vp->xrt;
   int yrt = vp->yrt;

   int rowdelx = vp->rowdelx;
   int rowdely = vp->rowdely;
   int coldelx = vp->coldelx;
   int coldely = vp->coldely;

   xlt += iJob*rowsPerStripe*coldelx;
   ylt += iJob*rowsPerStripe*coldely;

   xrt += iJob*rowsPerStripe*coldelx;
   yrt += iJob*rowsPerStripe*coldely;

   dst += iJob*rowsPerStripe*dstpitch;

   for (int i=begRow;i<(begRow+stripeRows);i++) {

      int xrow = xlt;
      int yrow = ylt;

      int xend = xrt;
      int yend = yrt;

      int preBlacks, framePels, postBlacks;

      // derive the loop counts using a Cohn-Sutherland clipping algorithm
      rowLoopCounts(xrow, yrow, xend, yend,
                    xlim, ylim,
                    frmWdth,
                    &preBlacks, &framePels, &postBlacks);

      // the general output row has some initial black pels,
      // followed by some data pels, followed by black pels

      for (int j=0;j<preBlacks;j++) {

         dst[0]       = 0;
         dst[1]       = 512;
         dst[2]       = 512;

         // advancing by 1 pel
         dst += 3;
      }

      xrow += preBlacks*rowdelx;
      yrow += preBlacks*rowdely;

      for (int j=0;j<framePels;j++) {

         // here we do a bilinear interpolation
         int frx = (xrow>>(SHIFTR-8))&0xff;
         int fry = (yrow>>(SHIFTR-8))&0xff;
         int grx = 256 - frx;
         int gry = 256 - fry;

         MTI_UINT16 *src = srcPels + ((yrow>>SHIFTR)*frameWidth + (xrow>>SHIFTR))*3;

         if (src < srcPels) {
            src = srcPels;
         }

         dst[0] =((src[0         ]*grx + src[3         ]*frx)*gry +
                  (src[0+srcpitch]*grx + src[3+srcpitch]*frx)*fry)>>16;

         dst[1] =((src[1         ]*grx + src[4         ]*frx)*gry +
                  (src[1+srcpitch]*grx + src[4+srcpitch]*frx)*fry)>>16;

         dst[2] =((src[2         ]*grx + src[5         ]*frx)*gry +
                  (src[2+srcpitch]*grx + src[5+srcpitch]*frx)*fry)>>16;

         xrow += rowdelx;
         yrow += rowdely;

         // advancing by 1 pel
         dst += 3;
      }

      for (int j=0;j<postBlacks;j++) {

         dst[0]       = 0;
         dst[1]       = 512;
         dst[2]       = 512;

         // advancing by 1 pel
         dst += 3;
      }

      xlt += coldelx;
      ylt += coldely;

      xrt += coldelx;
      yrt += coldely;
   }
}

void CRotator::rotateFastRGB(void *v, int iJob)
{
   CRotator *vp = (CRotator *) v;

   MTI_UINT16 *dst      = vp->dstpels;
   int dstpitch         = vp->frmWdth*3;
   int frmWdth          = vp->frmWdth;
   int frmHght          = vp->frmHght;

   MTI_UINT16 *srcPels  = vp->srcpels;
   int frameWidth       = vp->frmWdth;
   int xlim             = (vp->frmWdth - 2)<<SHIFTR; // 2 pixels per inner loop iteration
   int ylim             = (vp->frmHght - 2)<<SHIFTR; // 2 rows per outer loop iteration

   int rowsPerStripe = (frmHght / vp->nStripes + 1)&0xfffffffe;

   int begRow = iJob*rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      stripeRows = frmHght - iJob*rowsPerStripe;

   int xlt = vp->xlt;
   int ylt = vp->ylt;
   int xrt = vp->xrt;
   int yrt = vp->yrt;

   int rowdelx = vp->rowdelx;
   int rowdely = vp->rowdely;
   int coldelx = vp->coldelx;
   int coldely = vp->coldely;

   xlt += (iJob*rowsPerStripe*coldelx)/2;
   ylt += (iJob*rowsPerStripe*coldely)/2;

   xrt += (iJob*rowsPerStripe*coldelx)/2;
   yrt += (iJob*rowsPerStripe*coldely)/2;

   dst += iJob*rowsPerStripe*dstpitch;

   for (int i=begRow;i<(begRow+stripeRows);i+=2) {

      int xrow = xlt;
      int yrow = ylt;

      int xend = xrt;
      int yend = yrt;

      int preBlacks, framePels, postBlacks;

      // derive the loop counts using a Cohn-Sutherland clipping algorithm
      rowLoopCounts(xrow, yrow, xend, yend,
                    xlim, ylim,
                    frmWdth/2,
                    &preBlacks, &framePels, &postBlacks);

      // the general output row has some initial black pels,
      // followed by some data pels, followed by black pels

      for (int j=0;j<preBlacks;j++) {

         dst[0]          = 0;
         dst[1]          = 0;
         dst[2]          = 0;

         dst[3]          = 0;
         dst[4]          = 0;
         dst[5]          = 0;

         dst[dstpitch]   = 0;
         dst[dstpitch+1] = 0;
         dst[dstpitch+2] = 0;

         dst[dstpitch+3] = 0;
         dst[dstpitch+4] = 0;
         dst[dstpitch+5] = 0;

         // advancing by 2 pels
         dst += 6;
      }

      xrow += preBlacks*rowdelx;
      yrow += preBlacks*rowdely;

      for (int j=0;j<framePels;j++) {

         MTI_UINT16 *src = srcPels + ((yrow>>SHIFTR)*frameWidth + (xrow>>SHIFTR))*3;

         MTI_UINT16 val = src[0];

         dst[0]       = val;
         dst[3]       = val;
         dst[dstpitch]   = val;
         dst[dstpitch+3] = val;

         val = src[1];

         dst[1]       = val;
         dst[4]       = val;
         dst[dstpitch+1] = val;
         dst[dstpitch+4] = val;

         val = src[2];

         dst[2]       = val;
         dst[5]       = val;
         dst[dstpitch+2] = val;
         dst[dstpitch+5] = val;

         xrow += rowdelx;
         yrow += rowdely;

         // advancing by 2 pels
         dst += 6;
      }

      for (int j=0;j<postBlacks;j++) {

         dst[0]          = 0;
         dst[1]          = 0;
         dst[2]          = 0;

         dst[3]          = 0;
         dst[4]          = 0;
         dst[5]          = 0;

         dst[dstpitch]   = 0;
         dst[dstpitch+1] = 0;
         dst[dstpitch+2] = 0;

         dst[dstpitch+3] = 0;
         dst[dstpitch+4] = 0;
         dst[dstpitch+5] = 0;

         // advancing by 2 pels
         dst += 6;
      }

      xlt += coldelx;
      ylt += coldely;

      xrt += coldelx;
      yrt += coldely;

      // advancing by 2 rows (one
      // accounted for by above)
      dst += dstpitch;
   }
}

void CRotator::rotateExactRGB(void *v, int iJob)
{
   CRotator *vp = (CRotator *) v;

   MTI_UINT16 *dst      = vp->dstpels;
   int dstpitch         = vp->frmWdth*3;
   int frmWdth          = vp->frmWdth;
   int frmHght          = vp->frmHght;

   MTI_UINT16 *srcPels  = vp->srcpels;
   int frameWidth       = vp->frmWdth;
   int srcpitch         = vp->frmWdth*3;
   int xlim             = (vp->frmWdth - 1)<<SHIFTR;
   int ylim             = (vp->frmHght - 1)<<SHIFTR;

   int rowsPerStripe = frmHght / vp->nStripes;

   int begRow = iJob*rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      stripeRows = frmHght - iJob*rowsPerStripe;

   int xlt = vp->xlt;
   int ylt = vp->ylt;
   int xrt = vp->xrt;
   int yrt = vp->yrt;

   int rowdelx = vp->rowdelx;
   int rowdely = vp->rowdely;
   int coldelx = vp->coldelx;
   int coldely = vp->coldely;

   xlt += iJob*rowsPerStripe*coldelx;
   ylt += iJob*rowsPerStripe*coldely;

   xrt += iJob*rowsPerStripe*coldelx;
   yrt += iJob*rowsPerStripe*coldely;

   dst += iJob*rowsPerStripe*dstpitch;

   for (int i=begRow;i<(begRow+stripeRows);i++) {

      int xrow = xlt;
      int yrow = ylt;

      int xend = xrt;
      int yend = yrt;

      int preBlacks, framePels, postBlacks;

      // derive the loop counts using a Cohn-Sutherland clipping algorithm
      rowLoopCounts(xrow, yrow, xend, yend,
                    xlim, ylim,
                    frmWdth,
                    &preBlacks, &framePels, &postBlacks);

      // the general output row has some initial black pels,
      // followed by some data pels, followed by black pels

      for (int j=0;j<preBlacks;j++) {

         dst[0]       = 0;
         dst[1]       = 0;
         dst[2]       = 0;

         // advancing by 1 pel
         dst += 3;
      }

      xrow += preBlacks*rowdelx;
      yrow += preBlacks*rowdely;

      for (int j=0;j<framePels;j++) {

         // here we do a bilinear interpolation
         int frx = (xrow>>(SHIFTR-8))&0xff;
         int fry = (yrow>>(SHIFTR-8))&0xff;
         int grx = 256 - frx;
         int gry = 256 - fry;

         MTI_UINT16 *src = srcPels + ((yrow>>SHIFTR)*frameWidth + (xrow>>SHIFTR))*3;

         dst[0] =((src[0         ]*grx + src[3         ]*frx)*gry +
                  (src[0+srcpitch]*grx + src[3+srcpitch]*frx)*fry)>>16;

         dst[1] =((src[1         ]*grx + src[4         ]*frx)*gry +
                  (src[1+srcpitch]*grx + src[4+srcpitch]*frx)*fry)>>16;

         dst[2] =((src[2         ]*grx + src[5         ]*frx)*gry +
                  (src[2+srcpitch]*grx + src[5+srcpitch]*frx)*fry)>>16;

         xrow += rowdelx;
         yrow += rowdely;

         // advancing by 1 pel
         dst += 3;
      }

      for (int j=0;j<postBlacks;j++) {

         dst[0]       = 0;
         dst[1]       = 0;
         dst[2]       = 0;

         // advancing by 1 pel
         dst += 3;
      }

      xlt += coldelx;
      ylt += coldely;

      xrt += coldelx;
      yrt += coldely;
   }
}

void CRotator::rotateFastRGB_A1(void *v, int iJob)
{
   CRotator *vp = (CRotator *) v;

   MTI_UINT16 *dst      = vp->dstpels;
   MTI_UINT8 *alphaBeg  = (MTI_UINT8 *)(dst + vp->frmHght*vp->frmWdth*3);

   MTI_UINT8 *dstAlpha;
   MTI_UINT8 dstAlphaMask;
   MTI_UINT8 *nxtAlpha;
   MTI_UINT8 nxtAlphaMask;

   int dstpitch         = vp->frmWdth*3;
   int frmWdth          = vp->frmWdth;
   int frmHght          = vp->frmHght;

   MTI_UINT16 *srcPels  = vp->srcpels;
   MTI_UINT8 *srcAlpha  = (MTI_UINT8 *)(srcPels + vp->frmWdth*vp->frmHght*3);
   int frameWidth       = vp->frmWdth;
   int xlim             = (vp->frmWdth - 2)<<SHIFTR; // 2 pixels per inner loop iteration
   int ylim             = (vp->frmHght - 2)<<SHIFTR; // 2 rows per outer loop iteration

   int rowsPerStripe = (frmHght / vp->nStripes + 1)&0xfffffffe;

   int begRow = iJob*rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      stripeRows = frmHght - iJob*rowsPerStripe;

   int xlt = vp->xlt;
   int ylt = vp->ylt;
   int xrt = vp->xrt;
   int yrt = vp->yrt;

   int rowdelx = vp->rowdelx;
   int rowdely = vp->rowdely;
   int coldelx = vp->coldelx;
   int coldely = vp->coldely;

   xlt += (iJob*rowsPerStripe*coldelx)/2;
   ylt += (iJob*rowsPerStripe*coldely)/2;

   xrt += (iJob*rowsPerStripe*coldelx)/2;
   yrt += (iJob*rowsPerStripe*coldely)/2;

   dst += iJob*rowsPerStripe*dstpitch;

   for (int i=begRow;i<(begRow+stripeRows);i+=2) {

      int xrow = xlt;
      int yrow = ylt;

      int xend = xrt;
      int yend = yrt;

      int preBlacks, framePels, postBlacks;

      // derive the loop counts using a Cohn-Sutherland clipping algorithm
      rowLoopCounts(xrow, yrow, xend, yend,
                    xlim, ylim,
                    frmWdth/2,
                    &preBlacks, &framePels, &postBlacks);

      // the general output row has some initial black pels,
      // followed by some data pels, followed by black pels

      MTI_UINT8 *dstAlpha     = alphaBeg + (  i  * frmWdth / 8);
      MTI_UINT8  dstAlphaMask = ((MTI_UINT8)3)<< (  i*frmWdth %8);
      MTI_UINT8 *nxtAlpha     = alphaBeg + ((i+1)* frmWdth / 8);
      MTI_UINT8  nxtAlphaMask = ((MTI_UINT8)3)<< ((i+1)*frmWdth %8);

      for (int j=0;j<preBlacks;j++) {

         dst[0]          = 0;
         dst[1]          = 0;
         dst[2]          = 0;

         dst[3]          = 0;
         dst[4]          = 0;
         dst[5]          = 0;

         dst[dstpitch]   = 0;
         dst[dstpitch+1] = 0;
         dst[dstpitch+2] = 0;

         dst[dstpitch+3] = 0;
         dst[dstpitch+4] = 0;
         dst[dstpitch+5] = 0;

         // advancing by 2 pels
         dst += 6;

         // mark 0 in the even alpha row
         *dstAlpha &= ~(dstAlphaMask);
         dstAlphaMask <<= 2;
         if (dstAlphaMask == 0) {
            dstAlphaMask = 3;
            dstAlpha++;
         }

         // mark 0 in the odd  alpha row
         *nxtAlpha &= ~(nxtAlphaMask);
         nxtAlphaMask <<= 2;
         if (nxtAlphaMask == 0) {
            nxtAlphaMask = 3;
            nxtAlpha++;
         }
      }

      xrow += preBlacks*rowdelx;
      yrow += preBlacks*rowdely;

      for (int j=0;j<framePels;j++) {

         int pel = ((yrow>>SHIFTR)*frameWidth + (xrow>>SHIFTR));

         MTI_UINT16 *src = srcPels + pel*3;

         MTI_UINT16 val = src[0];

         dst[0]       = val;
         dst[3]       = val;
         dst[dstpitch]   = val;
         dst[dstpitch+3] = val;

         val = src[1];

         dst[1]       = val;
         dst[4]       = val;
         dst[dstpitch+1] = val;
         dst[dstpitch+4] = val;

         val = src[2];

         dst[2]       = val;
         dst[5]       = val;
         dst[dstpitch+2] = val;
         dst[dstpitch+5] = val;

         xrow += rowdelx;
         yrow += rowdely;

         // advancing by 2 pels
         dst += 6;

         // get the alpha value
         MTI_UINT8 *srcalpha = srcAlpha + (pel/8);
         MTI_UINT8 srcmask  = ((MTI_UINT8)1)<<(pel%8);
         if (*srcalpha&srcmask) {
            *dstAlpha |= dstAlphaMask;
            *nxtAlpha |= nxtAlphaMask;
         }
         else {
            *dstAlpha &= ~dstAlphaMask;
            *nxtAlpha &= ~nxtAlphaMask;
         }

         // advance the even row mask
         dstAlphaMask <<= 2;
         if (dstAlphaMask == 0) {
            dstAlphaMask = 3;
            dstAlpha++;
         }

         // advance the  odd row mask
         nxtAlphaMask <<= 2;
         if (nxtAlphaMask == 0) {
            nxtAlphaMask = 3;
            nxtAlpha++;
         }
      }

      for (int j=0;j<postBlacks;j++) {

         dst[0]          = 0;
         dst[1]          = 0;
         dst[2]          = 0;

         dst[3]          = 0;
         dst[4]          = 0;
         dst[5]          = 0;

         dst[dstpitch]   = 0;
         dst[dstpitch+1] = 0;
         dst[dstpitch+2] = 0;

         dst[dstpitch+3] = 0;
         dst[dstpitch+4] = 0;
         dst[dstpitch+5] = 0;

         // advancing by 2 pels
         dst += 6;

         // mark 0 in the even alpha row
         *dstAlpha &= ~(dstAlphaMask);
         dstAlphaMask <<= 2;
         if (dstAlphaMask == 0) {
            dstAlphaMask = 3;
            dstAlpha++;
         }

         // mark 0 in the  odd alpha row
         *nxtAlpha &= ~(nxtAlphaMask);
         nxtAlphaMask <<= 2;
         if (nxtAlphaMask == 0) {
            nxtAlphaMask = 3;
            nxtAlpha++;
         }
      }

      xlt += coldelx;
      ylt += coldely;

      xrt += coldelx;
      yrt += coldely;

      // advancing by 2 rows (one
      // accounted for by above)
      dst += dstpitch;
   }
}

void CRotator::rotateExactRGB_A1(void *v, int iJob)
{
   CRotator *vp = (CRotator *) v;

   MTI_UINT16 *dst      = vp->dstpels;
   MTI_UINT8 *dstAlpha  = (MTI_UINT8 *)(dst + vp->frmHght*vp->frmWdth*3);

   MTI_UINT8 dstAlphaMask;
   int dstpitch         = vp->frmWdth*3;
   int frmWdth          = vp->frmWdth;
   int frmHght          = vp->frmHght;

   MTI_UINT16 *srcPels  = vp->srcpels;
   MTI_UINT8 *srcAlpha = (MTI_UINT8 *)(srcPels + vp->frmWdth*vp->frmHght*3);
   int frameWidth       = vp->frmWdth;
   int srcpitch         = vp->frmWdth*3;
   int xlim             = (vp->frmWdth - 1)<<SHIFTR;
   int ylim             = (vp->frmHght - 1)<<SHIFTR;

   int rowsPerStripe = frmHght / vp->nStripes;

   int begRow = iJob*rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      stripeRows = frmHght - iJob*rowsPerStripe;

   int xlt = vp->xlt;
   int ylt = vp->ylt;
   int xrt = vp->xrt;
   int yrt = vp->yrt;

   int rowdelx = vp->rowdelx;
   int rowdely = vp->rowdely;
   int coldelx = vp->coldelx;
   int coldely = vp->coldely;

   xlt += iJob*rowsPerStripe*coldelx;
   ylt += iJob*rowsPerStripe*coldely;

   xrt += iJob*rowsPerStripe*coldelx;
   yrt += iJob*rowsPerStripe*coldely;

   dst += iJob*rowsPerStripe*dstpitch;

   dstAlpha += (iJob * rowsPerStripe * frmWdth / 8);
   dstAlphaMask = ((MTI_UINT8)(1) << ((iJob * rowsPerStripe * frmWdth) % 8));

   for (int i=begRow;i<(begRow+stripeRows);i++) {

      int xrow = xlt;
      int yrow = ylt;

      int xend = xrt;
      int yend = yrt;

      int preBlacks, framePels, postBlacks;

      // derive the loop counts using a Cohn-Sutherland clipping algorithm
      rowLoopCounts(xrow, yrow, xend, yend,
                    xlim, ylim,
                    frmWdth,
                    &preBlacks, &framePels, &postBlacks);

      // the general output row has some initial black pels,
      // followed by some data pels, followed by black pels

      for (int j=0;j<preBlacks;j++) {

         dst[0]       = 0;
         dst[1]       = 0;
         dst[2]       = 0;

         // advancing by 1 pel
         dst += 3;

         // mark 0 in the alpha mask
         *dstAlpha &= ~(dstAlphaMask);
         dstAlphaMask <<= 1;
         if (dstAlphaMask == 0) {
            dstAlphaMask = 1;
            dstAlpha++;
         }
      }

      xrow += preBlacks*rowdelx;
      yrow += preBlacks*rowdely;

      for (int j=0;j<framePels;j++) {

         // here we do a bilinear interpolation
         int frx = (xrow>>(SHIFTR-8))&0xff;
         int fry = (yrow>>(SHIFTR-8))&0xff;
         int grx = 256 - frx;
         int gry = 256 - fry;

         int pel = ((yrow>>SHIFTR)*frameWidth + (xrow>>SHIFTR));

         MTI_UINT16 *src = srcPels + pel*3;

         dst[0] =((src[0         ]*grx + src[3         ]*frx)*gry +
                  (src[0+srcpitch]*grx + src[3+srcpitch]*frx)*fry)>>16;

         dst[1] =((src[1         ]*grx + src[4         ]*frx)*gry +
                  (src[1+srcpitch]*grx + src[4+srcpitch]*frx)*fry)>>16;

         dst[2] =((src[2         ]*grx + src[5         ]*frx)*gry +
                  (src[2+srcpitch]*grx + src[5+srcpitch]*frx)*fry)>>16;

         xrow += rowdelx;
         yrow += rowdely;

         // advancing by 1 pel
         dst += 3;

         // get the alpha value
         MTI_UINT8 *srcalpha = srcAlpha + (pel/8);
         MTI_UINT8 srcmask  = ((MTI_UINT8)1)<<(pel%8);
         if (*srcalpha&srcmask)
            *dstAlpha |=  dstAlphaMask;
         else
            *dstAlpha &= ~dstAlphaMask;

         // advance the alpha mask
         dstAlphaMask <<= 1;
         if (dstAlphaMask == 0) {
            dstAlphaMask = 1;
            dstAlpha++;
         }
      }

      for (int j=0;j<postBlacks;j++) {

         dst[0]       = 0;
         dst[1]       = 0;
         dst[2]       = 0;

         // advancing by 1 pel
         dst += 3;

         // mark 0 in the alpha mask
         *dstAlpha &= ~(dstAlphaMask);
         dstAlphaMask <<= 1;
         if (dstAlphaMask == 0) {
            dstAlphaMask = 1;
            dstAlpha++;
         }
      }

      xlt += coldelx;
      ylt += coldely;

      xrt += coldelx;
      yrt += coldely;
   }
}

void CRotator::rotateFastRGB_A2(void *v, int iJob)
{
   return rotateFastRGB(v, iJob);

   CRotator *vp = (CRotator *) v;

   MTI_UINT16 *dst      = vp->dstpels;
   MTI_UINT8 *alphaBeg  = (MTI_UINT8 *)(dst + vp->frmHght*vp->frmWdth*3);

   int dstpitch         = vp->frmWdth*3;
   int frmWdth          = vp->frmWdth;
   int frmHght          = vp->frmHght;

   MTI_UINT16 *srcPels  = vp->srcpels;
   MTI_UINT8 *srcAlpha  = (MTI_UINT8 *)(srcPels + vp->frmWdth*vp->frmHght*3);
   int frameWidth       = vp->frmWdth;
   int xlim             = (vp->frmWdth - 2)<<SHIFTR; // 2 pixels per inner loop iteration
   int ylim             = (vp->frmHght - 2)<<SHIFTR; // 2 rows per outer loop iteration

   int rowsPerStripe = (frmHght / vp->nStripes + 1)&0xfffffffe;

   int begRow = iJob*rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      stripeRows = frmHght - iJob*rowsPerStripe;

   int xlt = vp->xlt;
   int ylt = vp->ylt;
   int xrt = vp->xrt;
   int yrt = vp->yrt;

   int rowdelx = vp->rowdelx;
   int rowdely = vp->rowdely;
   int coldelx = vp->coldelx;
   int coldely = vp->coldely;

   xlt += (iJob*rowsPerStripe*coldelx)/2;
   ylt += (iJob*rowsPerStripe*coldely)/2;

   xrt += (iJob*rowsPerStripe*coldelx)/2;
   yrt += (iJob*rowsPerStripe*coldely)/2;

   dst += iJob*rowsPerStripe*dstpitch;

   for (int i=begRow;i<(begRow+stripeRows);i+=2) {

      int xrow = xlt;
      int yrow = ylt;

      int xend = xrt;
      int yend = yrt;

      int preBlacks, framePels, postBlacks;

      // derive the loop counts using a Cohn-Sutherland clipping algorithm
      rowLoopCounts(xrow, yrow, xend, yend,
                    xlim, ylim,
                    frmWdth/2,
                    &preBlacks, &framePels, &postBlacks);

      // the general output row has some initial black pels,
      // followed by some data pels, followed by black pels

      MTI_UINT8 *dstAlpha = alphaBeg + ((i * frmWdth) / 4);
      MTI_UINT8 dstAlphaMaskShift = (i * frmWdth) % 4;
      MTI_UINT8 dstAlphaMask = ((MTI_UINT8) 0x0F) << dstAlphaMaskShift;
      MTI_UINT8 *nxtAlpha = alphaBeg + (((i + 1) * frmWdth) / 4);
      MTI_UINT8 nxtAlphaMaskShift = ((i + 1) * frmWdth) % 4;
      MTI_UINT8 nxtAlphaMask = ((MTI_UINT8) 0x0F) << nxtAlphaMaskShift;

      for (int j=0;j<preBlacks;j++) {

         dst[0]          = 0;
         dst[1]          = 0;
         dst[2]          = 0;

         dst[3]          = 0;
         dst[4]          = 0;
         dst[5]          = 0;

         dst[dstpitch]   = 0;
         dst[dstpitch+1] = 0;
         dst[dstpitch+2] = 0;

         dst[dstpitch+3] = 0;
         dst[dstpitch+4] = 0;
         dst[dstpitch+5] = 0;

         // advancing by 2 pels
         dst += 6;

         // mark 0 in the even alpha row
         *dstAlpha &= ~(dstAlphaMask);
         dstAlphaMaskShift += 4;
         dstAlphaMask <<= 4;
         if (dstAlphaMask == 0) {
            dstAlphaMaskShift = 0;
            dstAlphaMask = (MTI_UINT8) 0x0F;
            dstAlpha++;
         }

         // mark 0 in the odd  alpha row
         *nxtAlpha &= ~(nxtAlphaMask);
         nxtAlphaMaskShift += 4;
         nxtAlphaMask <<= 4;
         if (nxtAlphaMask == 0) {
            nxtAlphaMaskShift = 0;
            nxtAlphaMask = (MTI_UINT8) 0x0F;
            nxtAlpha++;
         }
      }

      xrow += preBlacks*rowdelx;
      yrow += preBlacks*rowdely;

      for (int j=0;j<framePels;j++) {

         int pel = ((yrow>>SHIFTR)*frameWidth + (xrow>>SHIFTR));

         MTI_UINT16 *src = srcPels + pel*3;

         MTI_UINT16 val = src[0];

         dst[0]       = val;
         dst[3]       = val;
         dst[dstpitch]   = val;
         dst[dstpitch+3] = val;

         val = src[1];

         dst[1]       = val;
         dst[4]       = val;
         dst[dstpitch+1] = val;
         dst[dstpitch+4] = val;

         val = src[2];

         dst[2]       = val;
         dst[5]       = val;
         dst[dstpitch+2] = val;
         dst[dstpitch+5] = val;

         xrow += rowdelx;
         yrow += rowdely;

         // advancing by 2 pels
         dst += 6;

         // get the alpha value
         MTI_UINT8 *srcalpha = srcAlpha + (pel / 4);
         MTI_UINT8 srcmask = ((MTI_UINT8) 0x03) << (pel % 4);
         *dstAlpha &= ~dstAlphaMask;
         *dstAlpha |= ((*srcalpha & srcmask) >> (pel % 4)) << dstAlphaMaskShift;
         *nxtAlpha &= ~nxtAlphaMask;
         *nxtAlpha |= ((*srcalpha & srcmask) >> (pel % 4)) << nxtAlphaMaskShift;

         // advance the even row mask
         dstAlphaMaskShift += 4;
         dstAlphaMask <<= 4;
         if (dstAlphaMask == 0) {
            dstAlphaMaskShift = 0;
            dstAlphaMask = (MTI_UINT8) 0x0F;
            dstAlpha++;
         }

         // advance the  odd row mask
         nxtAlphaMaskShift += 4;
         nxtAlphaMask <<= 4;
         if (nxtAlphaMask == 0) {
            nxtAlphaMaskShift = 0;
            nxtAlphaMask = (MTI_UINT8) 0x0F;
            nxtAlpha++;
         }
      }

      for (int j=0;j<postBlacks;j++) {

         dst[0]          = 0;
         dst[1]          = 0;
         dst[2]          = 0;

         dst[3]          = 0;
         dst[4]          = 0;
         dst[5]          = 0;

         dst[dstpitch]   = 0;
         dst[dstpitch+1] = 0;
         dst[dstpitch+2] = 0;

         dst[dstpitch+3] = 0;
         dst[dstpitch+4] = 0;
         dst[dstpitch+5] = 0;

         // advancing by 2 pels
         dst += 6;

         // mark 0 in the even alpha row
         *dstAlpha &= ~(dstAlphaMask);
         dstAlphaMaskShift += 4;
         dstAlphaMask <<= 4;
         if (dstAlphaMask == 0) {
            dstAlphaMaskShift = 0;
            dstAlphaMask = (MTI_UINT8) 0x0F;
            dstAlpha++;
         }

         // mark 0 in the  odd alpha row
         *nxtAlpha &= ~(nxtAlphaMask);
         nxtAlphaMaskShift += 4;
         nxtAlphaMask <<= 4;
         if (nxtAlphaMask == 0) {
            nxtAlphaMaskShift = 0;
            nxtAlphaMask = (MTI_UINT8) 0x0F;
            nxtAlpha++;
         }
      }

      xlt += coldelx;
      ylt += coldely;

      xrt += coldelx;
      yrt += coldely;

      // advancing by 2 rows (one
      // accounted for by above)
      dst += dstpitch;
   }
}

void CRotator::rotateExactRGB_A2(void *v, int iJob)
{
   return rotateExactRGB(v, iJob);

   CRotator *vp = (CRotator *) v;

   MTI_UINT16 *dst      = vp->dstpels;
   MTI_UINT8 *dstAlpha  = (MTI_UINT8 *)(dst + vp->frmHght*vp->frmWdth*3);

   int dstpitch         = vp->frmWdth*3;
   int frmWdth          = vp->frmWdth;
   int frmHght          = vp->frmHght;

   MTI_UINT16 *srcPels  = vp->srcpels;
   MTI_UINT8 *srcAlpha = (MTI_UINT8 *)(srcPels + vp->frmWdth*vp->frmHght*3);
   int frameWidth       = vp->frmWdth;
   int srcpitch         = vp->frmWdth*3;
   int xlim             = (vp->frmWdth - 1)<<SHIFTR;
   int ylim             = (vp->frmHght - 1)<<SHIFTR;

   int rowsPerStripe = frmHght / vp->nStripes;

   int begRow = iJob*rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      stripeRows = frmHght - iJob*rowsPerStripe;

   int xlt = vp->xlt;
   int ylt = vp->ylt;
   int xrt = vp->xrt;
   int yrt = vp->yrt;

   int rowdelx = vp->rowdelx;
   int rowdely = vp->rowdely;
   int coldelx = vp->coldelx;
   int coldely = vp->coldely;

   xlt += iJob*rowsPerStripe*coldelx;
   ylt += iJob*rowsPerStripe*coldely;

   xrt += iJob*rowsPerStripe*coldelx;
   yrt += iJob*rowsPerStripe*coldely;

   dst += iJob*rowsPerStripe*dstpitch;

   dstAlpha += (iJob * rowsPerStripe * frmWdth) / 4;
   MTI_UINT8 dstAlphaMaskShift = (iJob * rowsPerStripe * frmWdth) % 4;
   MTI_UINT8 dstAlphaMask = ((MTI_UINT8) 0x03) << dstAlphaMaskShift;

   for (int i=begRow;i<(begRow+stripeRows);i++) {

      int xrow = xlt;
      int yrow = ylt;

      int xend = xrt;
      int yend = yrt;

      int preBlacks, framePels, postBlacks;

      // derive the loop counts using a Cohn-Sutherland clipping algorithm
      rowLoopCounts(xrow, yrow, xend, yend,
                    xlim, ylim,
                    frmWdth,
                    &preBlacks, &framePels, &postBlacks);

      // the general output row has some initial black pels,
      // followed by some data pels, followed by black pels

      for (int j=0;j<preBlacks;j++) {

         dst[0]       = 0;
         dst[1]       = 0;
         dst[2]       = 0;

         // advancing by 1 pel
         dst += 3;

         // mark 0 in the alpha mask
         *dstAlpha &= ~(dstAlphaMask);
         dstAlphaMask <<= 2;
         if (dstAlphaMask == 0) {
            dstAlphaMask = (MTI_UINT8) 0x03;
            dstAlpha++;
         }
      }

      xrow += preBlacks*rowdelx;
      yrow += preBlacks*rowdely;

      for (int j=0;j<framePels;j++) {

         // here we do a bilinear interpolation
         int frx = (xrow>>(SHIFTR-8))&0xff;
         int fry = (yrow>>(SHIFTR-8))&0xff;
         int grx = 256 - frx;
         int gry = 256 - fry;

         int pel = ((yrow>>SHIFTR)*frameWidth + (xrow>>SHIFTR));

         MTI_UINT16 *src = srcPels + pel*3;

         dst[0] =((src[0         ]*grx + src[3         ]*frx)*gry +
                  (src[0+srcpitch]*grx + src[3+srcpitch]*frx)*fry)>>16;

         dst[1] =((src[1         ]*grx + src[4         ]*frx)*gry +
                  (src[1+srcpitch]*grx + src[4+srcpitch]*frx)*fry)>>16;

         dst[2] =((src[2         ]*grx + src[5         ]*frx)*gry +
                  (src[2+srcpitch]*grx + src[5+srcpitch]*frx)*fry)>>16;

         xrow += rowdelx;
         yrow += rowdely;

         // advancing by 1 pel
         dst += 3;

         // get the alpha value
         MTI_UINT8 *srcalpha = srcAlpha + (pel / 4);
         MTI_UINT8 srcmask = ((MTI_UINT8)0x03) << (pel % 4);
         *dstAlpha &= ~dstAlphaMask;
         *dstAlpha |= ((*srcalpha & srcmask) >> (pel % 4)) << dstAlphaMaskShift;

         // advance the alpha mask
         dstAlphaMaskShift += 2;
         dstAlphaMask <<= 2;
         if (dstAlphaMask == 0) {
            dstAlphaMaskShift = 0;
            dstAlphaMask = (MTI_UINT8)0x03;
            dstAlpha++;
         }
      }

      for (int j=0;j<postBlacks;j++) {

         dst[0]       = 0;
         dst[1]       = 0;
         dst[2]       = 0;

         // advancing by 1 pel
         dst += 3;

         // mark 0 in the alpha mask
         *dstAlpha &= ~(dstAlphaMask);
         dstAlphaMaskShift += 2;
         dstAlphaMask <<= 2;
         if (dstAlphaMask == 0) {
            dstAlphaMaskShift = 0;
            dstAlphaMask = (MTI_UINT8)0x03;
            dstAlpha++;
         }
      }

      xlt += coldelx;
      ylt += coldely;

      xrt += coldelx;
      yrt += coldely;
   }
}

void CRotator::rotateFastRGB_A8(void *v, int iJob)
{
   CRotator *vp = (CRotator *) v;

   MTI_UINT16 *dst      = vp->dstpels;
   MTI_UINT8  *dstAlpha = (MTI_UINT8 *)(dst + vp->frmHght*vp->frmWdth*3);

   int dstpitch         = vp->frmWdth*3;
   int frmWdth          = vp->frmWdth;
   int frmHght          = vp->frmHght;

   MTI_UINT16 *srcPels  = vp->srcpels;
   MTI_UINT8  *srcAlpha = (MTI_UINT8 *)(srcPels + vp->frmWdth*vp->frmHght*3);
   int frameWidth       = vp->frmWdth;
   int xlim             = (vp->frmWdth - 2)<<SHIFTR; // 2 pixels per inner loop iteration
   int ylim             = (vp->frmHght - 2)<<SHIFTR; // 2 rows per outer loop iteration

   int rowsPerStripe = (frmHght / vp->nStripes + 1)&0xfffffffe;

   int begRow = iJob*rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      stripeRows = frmHght - iJob*rowsPerStripe;

   int xlt = vp->xlt;
   int ylt = vp->ylt;
   int xrt = vp->xrt;
   int yrt = vp->yrt;

   int rowdelx = vp->rowdelx;
   int rowdely = vp->rowdely;
   int coldelx = vp->coldelx;
   int coldely = vp->coldely;

   xlt += (iJob*rowsPerStripe*coldelx)/2;
   ylt += (iJob*rowsPerStripe*coldely)/2;

   xrt += (iJob*rowsPerStripe*coldelx)/2;
   yrt += (iJob*rowsPerStripe*coldely)/2;

   dst      += iJob*rowsPerStripe*dstpitch;
   dstAlpha += iJob*rowsPerStripe*frmWdth;

   for (int i=begRow;i<(begRow+stripeRows);i+=2) {

      int xrow = xlt;
      int yrow = ylt;

      int xend = xrt;
      int yend = yrt;

      int preBlacks, framePels, postBlacks;

      // derive the loop counts using a Cohn-Sutherland clipping algorithm
      rowLoopCounts(xrow, yrow, xend, yend,
                    xlim, ylim,
                    frmWdth/2,
                    &preBlacks, &framePels, &postBlacks);

      // the general output row has some initial black pels,
      // followed by some data pels, followed by black pels

      for (int j=0;j<preBlacks;j++) {

         dst[0]          = 0;
         dst[1]          = 0;
         dst[2]          = 0;

         dst[3]          = 0;
         dst[4]          = 0;
         dst[5]          = 0;

         dst[dstpitch]   = 0;
         dst[dstpitch+1] = 0;
         dst[dstpitch+2] = 0;

         dst[dstpitch+3] = 0;
         dst[dstpitch+4] = 0;
         dst[dstpitch+5] = 0;

         // advancing by 2 pels
         dst += 6;

         dstAlpha[0]          = 0;
         dstAlpha[1]          = 0;
         dstAlpha[frmWdth]   = 0;
         dstAlpha[frmWdth+1] = 0;

         dstAlpha += 2;
      }

      xrow += preBlacks*rowdelx;
      yrow += preBlacks*rowdely;

      for (int j=0;j<framePels;j++) {

         int pel = ((yrow>>SHIFTR)*frameWidth + (xrow>>SHIFTR));

         MTI_UINT16 *src = srcPels + pel*3;

         MTI_UINT16 val = src[0];

         dst[0]          = val;
         dst[3]          = val;
         dst[dstpitch]   = val;
         dst[dstpitch+3] = val;

         val = src[1];

         dst[1]          = val;
         dst[4]          = val;
         dst[dstpitch+1] = val;
         dst[dstpitch+4] = val;

         val = src[2];

         dst[2]          = val;
         dst[5]          = val;
         dst[dstpitch+2] = val;
         dst[dstpitch+5] = val;

         xrow += rowdelx;
         yrow += rowdely;

         // advancing by 2 pels
         dst += 6;

         MTI_UINT8 alphaval = srcAlpha[pel];

         dstAlpha[0]          = alphaval;
         dstAlpha[1]          = alphaval;
         dstAlpha[frmWdth]   = alphaval;
         dstAlpha[frmWdth+1] = alphaval;

         dstAlpha += 2;
      }

      for (int j=0;j<postBlacks;j++) {

         dst[0]          = 0;
         dst[1]          = 0;
         dst[2]          = 0;
         dst[3]          = 0;
         dst[4]          = 0;
         dst[5]          = 0;

         dst[dstpitch]   = 0;
         dst[dstpitch+1] = 0;
         dst[dstpitch+2] = 0;

         dst[dstpitch+3] = 0;
         dst[dstpitch+4] = 0;
         dst[dstpitch+5] = 0;

         // advancing by 2 pels
         dst += 6;

         dstAlpha[0]          = 0;
         dstAlpha[1]          = 0;
         dstAlpha[frmWdth]   = 0;
         dstAlpha[frmWdth+1] = 0;

         dstAlpha += 2;
      }

      xlt += coldelx;
      ylt += coldely;

      xrt += coldelx;
      yrt += coldely;

      // advancing by 2 rows (one
      // accounted for by above)
      dst += dstpitch;
      dstAlpha += frmWdth;
   }
}

void CRotator::rotateExactRGB_A8(void *v, int iJob)
{
   CRotator *vp = (CRotator *) v;

   MTI_UINT16 *dst      = vp->dstpels;
   MTI_UINT8 *dstAlpha  = (MTI_UINT8 *)(dst + vp->frmHght*vp->frmWdth*3);
   int dstpitch         = vp->frmWdth*3;
   int frmWdth          = vp->frmWdth;
   int frmHght          = vp->frmHght;

   MTI_UINT16 *srcPels  = vp->srcpels;
   MTI_UINT8 *srcAlpha = (MTI_UINT8 *)(srcPels + vp->frmWdth*vp->frmHght*3);
   int frameWidth       = vp->frmWdth;
   int srcpitch         = vp->frmWdth*3;
   int xlim             = (vp->frmWdth - 1)<<SHIFTR;
   int ylim             = (vp->frmHght - 1)<<SHIFTR;

   int rowsPerStripe = frmHght / vp->nStripes;

   int begRow = iJob*rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      stripeRows = frmHght - iJob*rowsPerStripe;

   int xlt = vp->xlt;
   int ylt = vp->ylt;
   int xrt = vp->xrt;
   int yrt = vp->yrt;

   int rowdelx = vp->rowdelx;
   int rowdely = vp->rowdely;
   int coldelx = vp->coldelx;
   int coldely = vp->coldely;

   xlt += iJob*rowsPerStripe*coldelx;
   ylt += iJob*rowsPerStripe*coldely;

   xrt += iJob*rowsPerStripe*coldelx;
   yrt += iJob*rowsPerStripe*coldely;

   dst += iJob*rowsPerStripe*dstpitch;

   dstAlpha += (iJob*rowsPerStripe*frmWdth);

   for (int i=begRow;i<(begRow+stripeRows);i++) {

      int xrow = xlt;
      int yrow = ylt;

      int xend = xrt;
      int yend = yrt;

      int preBlacks, framePels, postBlacks;

      // derive the loop counts using a Cohn-Sutherland clipping algorithm
      rowLoopCounts(xrow, yrow, xend, yend,
                    xlim, ylim,
                    frmWdth,
                    &preBlacks, &framePels, &postBlacks);

      // the general output row has some initial black pels,
      // followed by some data pels, followed by black pels

      for (int j=0;j<preBlacks;j++) {

         dst[0]       = 0;
         dst[1]       = 0;
         dst[2]       = 0;

         // advancing by 1 pel
         dst += 3;

         // mark 0 in the alpha mask
         *dstAlpha++ = 0;
      }

      xrow += preBlacks*rowdelx;
      yrow += preBlacks*rowdely;

      for (int j=0;j<framePels;j++) {

         // here we do a bilinear interpolation
         int frx = (xrow>>(SHIFTR-8))&0xff;
         int fry = (yrow>>(SHIFTR-8))&0xff;
         int grx = 256 - frx;
         int gry = 256 - fry;

         int pel = ((yrow>>SHIFTR)*frameWidth + (xrow>>SHIFTR));

         MTI_UINT16 *src = srcPels + pel*3;

         dst[0] =((src[0         ]*grx + src[3         ]*frx)*gry +
                  (src[0+srcpitch]*grx + src[3+srcpitch]*frx)*fry)>>16;

         dst[1] =((src[1         ]*grx + src[4         ]*frx)*gry +
                  (src[1+srcpitch]*grx + src[4+srcpitch]*frx)*fry)>>16;

         dst[2] =((src[2         ]*grx + src[5         ]*frx)*gry +
                  (src[2+srcpitch]*grx + src[5+srcpitch]*frx)*fry)>>16;

         xrow += rowdelx;
         yrow += rowdely;

         // advancing by 1 pel
         dst += 3;

         // get the alpha value
         *dstAlpha++ = srcAlpha[pel];
      }

      for (int j=0;j<postBlacks;j++) {

         dst[0]       = 0;
         dst[1]       = 0;
         dst[2]       = 0;

         // advancing by 1 pel
         dst += 3;

         // mark 0 in the alpha mask
         *dstAlpha++ = 0;
      }

      xlt += coldelx;
      ylt += coldely;

      xrt += coldelx;
      yrt += coldely;
   }
}

void CRotator::rotateFastRGB_A16(void *v, int iJob)
{
   CRotator *vp = (CRotator *) v;

   MTI_UINT16 *dst      = vp->dstpels;
   MTI_UINT16 *dstAlpha = (MTI_UINT16 *)(dst + vp->frmHght*vp->frmWdth*3);

   int dstpitch         = vp->frmWdth*3;
   int frmWdth          = vp->frmWdth;
   int frmHght          = vp->frmHght;

   MTI_UINT16 *srcPels  = vp->srcpels;
   MTI_UINT16 *srcAlpha = (MTI_UINT16 *)(srcPels + vp->frmWdth*vp->frmHght*3);
   int frameWidth       = vp->frmWdth;
   int xlim             = (vp->frmWdth - 2)<<SHIFTR; // 2 pixels per inner loop iteration
   int ylim             = (vp->frmHght - 2)<<SHIFTR; // 2 rows per outer loop iteration

   int rowsPerStripe = (frmHght / vp->nStripes + 1)&0xfffffffe;

   int begRow = iJob*rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      stripeRows = vp->frmHght - iJob*rowsPerStripe;

   int xlt = vp->xlt;
   int ylt = vp->ylt;
   int xrt = vp->xrt;
   int yrt = vp->yrt;

   int rowdelx = vp->rowdelx;
   int rowdely = vp->rowdely;
   int coldelx = vp->coldelx;
   int coldely = vp->coldely;

   xlt += (iJob*rowsPerStripe*coldelx)/2;
   ylt += (iJob*rowsPerStripe*coldely)/2;

   xrt += (iJob*rowsPerStripe*coldelx)/2;
   yrt += (iJob*rowsPerStripe*coldely)/2;

   dst      += iJob*rowsPerStripe*dstpitch;
   dstAlpha += iJob*rowsPerStripe*frmWdth;

   for (int i=begRow;i<(begRow+stripeRows);i+=2) {

      int xrow = xlt;
      int yrow = ylt;

      int xend = xrt;
      int yend = yrt;

      int preBlacks, framePels, postBlacks;

      // derive the loop counts using a Cohn-Sutherland clipping algorithm
      rowLoopCounts(xrow, yrow, xend, yend,
                    xlim, ylim,
                    frmWdth/2,
                    &preBlacks, &framePels, &postBlacks);

      // the general output row has some initial black pels,
      // followed by some data pels, followed by black pels

      for (int j=0;j<preBlacks;j++) {

         dst[0]          = 0;
         dst[1]          = 0;
         dst[2]          = 0;

         dst[3]          = 0;
         dst[4]          = 0;
         dst[5]          = 0;

         dst[dstpitch]   = 0;
         dst[dstpitch+1] = 0;
         dst[dstpitch+2] = 0;

         dst[dstpitch+3] = 0;
         dst[dstpitch+4] = 0;
         dst[dstpitch+5] = 0;

         // advancing by 2 pels
         dst += 6;

         dstAlpha[0]          = 0;
         dstAlpha[1]          = 0;
         dstAlpha[frmWdth]   = 0;
         dstAlpha[frmWdth+1] = 0;

         dstAlpha += 2;
      }

      xrow += preBlacks*rowdelx;
      yrow += preBlacks*rowdely;

      for (int j=0;j<framePels;j++) {

         int pel = ((yrow>>SHIFTR)*frameWidth + (xrow>>SHIFTR));

         MTI_UINT16 *src = srcPels + pel*3;

         MTI_UINT16 val = src[0];

         dst[0]          = val;
         dst[3]          = val;
         dst[dstpitch]   = val;
         dst[dstpitch+3] = val;

         val = src[1];

         dst[1]          = val;
         dst[4]          = val;
         dst[dstpitch+1] = val;
         dst[dstpitch+4] = val;

         val = src[2];

         dst[2]          = val;
         dst[5]          = val;
         dst[dstpitch+2] = val;
         dst[dstpitch+5] = val;

         xrow += rowdelx;
         yrow += rowdely;

         // advancing by 2 pels
         dst += 6;

         MTI_UINT16 alphaval = srcAlpha[pel];

         dstAlpha[0]          = alphaval;
         dstAlpha[1]          = alphaval;
         dstAlpha[frmWdth]   = alphaval;
         dstAlpha[frmWdth+1] = alphaval;

         dstAlpha += 2;
      }

      for (int j=0;j<postBlacks;j++) {

         dst[0]          = 0;
         dst[1]          = 0;
         dst[2]          = 0;

         dst[3]          = 0;
         dst[4]          = 0;
         dst[5]          = 0;

         dst[dstpitch]   = 0;
         dst[dstpitch+1] = 0;
         dst[dstpitch+2] = 0;

         dst[dstpitch+3] = 0;
         dst[dstpitch+4] = 0;
         dst[dstpitch+5] = 0;

         // advancing by 2 pels
         dst += 6;

         dstAlpha[0]          = 0;
         dstAlpha[1]          = 0;
         dstAlpha[frmWdth]   = 0;
         dstAlpha[frmWdth+1] = 0;

         dstAlpha += 2;
      }

      xlt += coldelx;
      ylt += coldely;

      xrt += coldelx;
      yrt += coldely;

      // advancing by 2 rows (one
      // accounted for by above)
      dst += dstpitch;
      dstAlpha += frmWdth;
   }
}

void CRotator::rotateExactRGB_A16(void *v, int iJob)
{
   CRotator *vp = (CRotator *) v;

   MTI_UINT16 *dst      = vp->dstpels;
   MTI_UINT16 *dstAlpha  = (dst + vp->frmHght*vp->frmWdth*3);
   int dstpitch         = vp->frmWdth*3;
   int frmWdth          = vp->frmWdth;
   int frmHght          = vp->frmHght;

   MTI_UINT16 *srcPels  = vp->srcpels;
   MTI_UINT16 *srcAlpha = (srcPels + vp->frmWdth*vp->frmHght*3);
   int frameWidth       = vp->frmWdth;
   int srcpitch         = vp->frmWdth*3;
   int xlim             = (vp->frmWdth - 1)<<SHIFTR;
   int ylim             = (vp->frmHght - 1)<<SHIFTR;

   int rowsPerStripe = frmHght / vp->nStripes;

   int begRow = iJob*rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      stripeRows = frmHght - iJob*rowsPerStripe;

   int xlt = vp->xlt;
   int ylt = vp->ylt;
   int xrt = vp->xrt;
   int yrt = vp->yrt;

   int rowdelx = vp->rowdelx;
   int rowdely = vp->rowdely;
   int coldelx = vp->coldelx;
   int coldely = vp->coldely;

   xlt += iJob*rowsPerStripe*coldelx;
   ylt += iJob*rowsPerStripe*coldely;

   xrt += iJob*rowsPerStripe*coldelx;
   yrt += iJob*rowsPerStripe*coldely;

   dst += iJob*rowsPerStripe*dstpitch;

   dstAlpha += (iJob*rowsPerStripe*frmWdth);

   for (int i=begRow;i<(begRow+stripeRows);i++) {

      int xrow = xlt;
      int yrow = ylt;

      int xend = xrt;
      int yend = yrt;

      int preBlacks, framePels, postBlacks;

      // derive the loop counts using a Cohn-Sutherland clipping algorithm
      rowLoopCounts(xrow, yrow, xend, yend,
                    xlim, ylim,
                    frmWdth,
                    &preBlacks, &framePels, &postBlacks);

      // the general output row has some initial black pels,
      // followed by some data pels, followed by black pels

      for (int j=0;j<preBlacks;j++) {

         dst[0]       = 0;
         dst[1]       = 0;
         dst[2]       = 0;

         // advancing by 1 pel
         dst += 3;

         // mark 0 in the alpha mask
         *dstAlpha++ = 0;
      }

      xrow += preBlacks*rowdelx;
      yrow += preBlacks*rowdely;

      for (int j=0;j<framePels;j++) {

         // here we do a bilinear interpolation
         int frx = (xrow>>(SHIFTR-8))&0xff;
         int fry = (yrow>>(SHIFTR-8))&0xff;
         int grx = 256 - frx;
         int gry = 256 - fry;

         int pel = ((yrow>>SHIFTR)*frameWidth + (xrow>>SHIFTR));

         MTI_UINT16 *src = srcPels + pel*3;

         dst[0] =((src[0         ]*grx + src[3         ]*frx)*gry +
                  (src[0+srcpitch]*grx + src[3+srcpitch]*frx)*fry)>>16;

         dst[1] =((src[1         ]*grx + src[4         ]*frx)*gry +
                  (src[1+srcpitch]*grx + src[4+srcpitch]*frx)*fry)>>16;

         dst[2] =((src[2         ]*grx + src[5         ]*frx)*gry +
                  (src[2+srcpitch]*grx + src[5+srcpitch]*frx)*fry)>>16;

         xrow += rowdelx;
         yrow += rowdely;

         // advancing by 1 pel
         dst += 3;

         // get the alpha value
         *dstAlpha++ = srcAlpha[pel];
      }

      for (int j=0;j<postBlacks;j++) {

         dst[0]       = 0;
         dst[1]       = 0;
         dst[2]       = 0;

         // advancing by 1 pel
         dst += 3;

         // mark 0 in the alpha mask
         *dstAlpha++ = 0;
      }

      xlt += coldelx;
      ylt += coldely;

      xrt += coldelx;
      yrt += coldely;
   }
}

#if 0
void CRotator::rotateFastMON(void *v, int iJob)
{
   CRotator *vp = (CRotator *) v;

   MTI_UINT16 *dst      = vp->dstpels;
   int dstpitch         = vp->frmWdth*3; // ????
   int frmWdth          = vp->frmWdth;
   int frmHght          = vp->frmHght;

   MTI_UINT16 *srcPels  = vp->srcpels;
   int frameWidth       = vp->frmWdth;
   int xlim             = (vp->frmWdth - 2)<<SHIFTR; // 2 pixels per inner loop iteration
   int ylim             = (vp->frmHght - 2)<<SHIFTR; // 2 rows per outer loop iteration

   int rowsPerStripe = (frmHght / vp->nStripes + 1)&0xfffffffe;

   int begRow = iJob*rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      stripeRows = frmHght - iJob*rowsPerStripe;

   int thecos = 32768*cos(vp->angle)/vp->stretch;
   int thesin = 32768*sin(vp->angle)/vp->stretch;

   // x' =  x cos - y sin
   // y' =  x sin + y cos

   int xlt = (-importWH/2)*thecos + ( importWH/2)*thesin;
   int ylt = (-importWH/2)*thesin + (-importWH/2)*thecos;

   int xrt = ( importWH/2)*thecos + ( importWH/2)*thesin;
   int yrt = ( importWH/2)*thesin + (-importWH/2)*thecos;

   int delx = 2*(xrt - xlt) / importWH;
   int dely = 2*(yrt - ylt) / importWH;

   xlt += (vp->frmWdth<<14);
   ylt += (vp->frmHght<<14);

   xrt += (vp->frmWdth<<14);
   yrt += (vp->frmHght<<14);

   xlt -= iJob*(rowsPerStripe/2)*dely;
   ylt += iJob*(rowsPerStripe/2)*delx;

   xrt -= iJob*(rowsPerStripe/2)*dely;
   yrt += iJob*(rowsPerStripe/2)*delx;

   dst += iJob*rowsPerStripe*dstpitch;

   for (int i=begRow;i<(begRow+stripeRows);i+=2) {

      int xrow = xlt;
      int yrow = ylt;

      int xend = xrt;
      int yend = yrt;

      int preBlacks, framePels, postBlacks;

      // derive the loop counts using a Cohn-Sutherland clipping algorithm
      rowLoopCounts(xrow, yrow, xend, yend,
                    xlim, ylim,
                    frmWdth/2,
                    &preBlacks, &framePels, &postBlacks);

      // the general output row has some initial black pels,
      // followed by some data pels, followed by black pels

      for (int j=0;j<preBlacks;j++) {

         dst[0]          = 0;

         dst[1]          = 0;

         dst[dstpitch]   = 0;

         dst[dstpitch+1] = 0;

         // advancing by 2 pels
         dst += 2;
      }

      xrow += preBlacks*delx;
      yrow += preBlacks*dely;

      for (int j=0;j<framePels;j++) {

         MTI_UINT16 *src = srcPels + ((yrow>>15)*frameWidth + (xrow>>15))*3;

         MTI_UINT16 val = src[0];

         dst[0]       = val;

         dst[1]       = val;

         dst[dstpitch]   = val;

         dst[dstpitch+1] = val;

         xrow += delx;
         yrow += dely;

         // advancing by 2 pels
         dst += 2;
      }

      for (int j=0;j<postBlacks;j++) {

         dst[0]          = 0;

         dst[1]          = 0;

         dst[dstpitch]   = 0;

         dst[dstpitch+1] = 0;

         // advancing by 2 pels
         dst += 2;
      }

      xlt -= dely;
      ylt += delx;

      xrt -= dely;
      yrt += delx;

      // advancing by 2 rows (one
      // accounted for by above)
      dst += dstpitch;
   }
}

void CRotator::rotateExactMON(void *v, int iJob)
{
   CRotator *vp = (CRotator *) v;

   MTI_UINT16 *dst      = vp->dstpels;
   int dstpitch         = vp->frmWdth;
   int frmWdth          = vp->frmWdth;
   int frmHght          = vp->frmHght;

   MTI_UINT16 *srcPels  = vp->srcpels;
   int frameWidth       = vp->frmWdth;
   int srcpitch         = vp->frmWdth;
   int xlim             = (vp->frmWdth - 1)<<SHIFTR;
   int ylim             = (vp->frmHght - 1)<<SHIFTR;

   int rowsPerStripe = frmHght / vp->nStripes;

   int begRow = iJob*rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      stripeRows = frmHght - iJob*rowsPerStripe;

   int thecos = 32768*cos(vp->angle)/vp->stretch;
   int thesin = 32768*sin(vp->angle)/vp->stretch;

   // x' =  x cos - y sin
   // y' =  x sin + y cos

   int xlt = (-importWH/2)*thecos + ( importWH/2)*thesin;
   int ylt = (-importWH/2)*thesin + (-importWH/2)*thecos;

   int xrt = ( importWH/2)*thecos + ( importWH/2)*thesin;
   int yrt = ( importWH/2)*thesin + (-importWH/2)*thecos;

   int delx = (xrt - xlt) / importWH;
   int dely = (yrt - ylt) / importWH;

   xlt += (vp->frmWdth<<14);
   ylt += (vp->frmHght<<14);

   xrt += (vp->frmWdth<<14);
   yrt += (vp->frmHght<<14);

   xlt -= iJob*rowsPerStripe*dely;
   ylt += iJob*rowsPerStripe*delx;

   xrt -= iJob*rowsPerStripe*dely;
   yrt += iJob*rowsPerStripe*delx;

   dst += iJob*rowsPerStripe*dstpitch;

   for (int i=begRow;i<(begRow+stripeRows);i++) {

      int xrow = xlt;
      int yrow = ylt;

      int xend = xrt;
      int yend = yrt;

      int preBlacks, framePels, postBlacks;

      // derive the loop counts using a Cohn-Sutherland clipping algorithm
      rowLoopCounts(xrow, yrow, xend, yend,
                    xlim, ylim,
                    frmWdth,
                    &preBlacks, &framePels, &postBlacks);

      // the general output row has some initial black pels,
      // followed by some data pels, followed by black pels

      for (int j=0;j<preBlacks;j++) {

         *dst++       = 0;
      }

      xrow += preBlacks*delx;
      yrow += preBlacks*dely;

      for (int j=0;j<framePels;j++) {

         // here we do a bilinear interpolation
         int frx = (xrow>>7)&0xff;
         int fry = (yrow>>7)&0xff;
         int grx = 256 - frx;
         int gry = 256 - fry;

         MTI_UINT16 *src = srcPels + ((yrow>>15)*frameWidth + (xrow>>15))*3;

         *dst++ =((src[0         ]*grx + src[3         ]*frx)*gry +
                  (src[0+srcpitch]*grx + src[3+srcpitch]*frx)*fry)>>16;

         xrow += delx;
         yrow += dely;
      }

      for (int j=0;j<postBlacks;j++) {

         *dst++       = 0;
      }

      xlt -= dely;
      ylt += delx;

      xrt -= dely;
      yrt += delx;
   }
}
#endif
