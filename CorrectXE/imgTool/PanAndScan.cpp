#include <iostream>
#include <string.h>
#include "ColorSpaceConvert.h"
#include "ImageInfo.h"
#include "GraphicEngine.h"
#include "LineEngine.h"
#include "PanAndScan.h"
#include "MTIio.h"
#include "IniFile.h"    // for TRACE_N, believe it or not
#include "err_imgtool.h"

#define XFM_MULTI
#define RCT_MULTI
#define CVT_MULTI
// change definition of nStripes as well


//////////////////////////////////////////////////////////

static void      stripe8to8(CPanAndScan *vp, int iJob);
static void           rect8(CPanAndScan *vp, int iJob);
static void    stripe10to10(CPanAndScan *vp, int iJob);
static void          rect10(CPanAndScan *vp, int iJob);
static void  stripeDVS10to8(CPanAndScan *vp, int iJob);
static void  xfmDVS10toYUV8(CPanAndScan *vp, int iJob);
static void    stripe16to16(CPanAndScan *vp, int iJob);
static void          rect16(CPanAndScan *vp, int iJob);
static void         outline(CPanAndScan *vp);

//////////////////////////////////////////////////////////

CPanAndScan::
CPanAndScan(
                int swdth, // rc wdth
                int shght, // src hght
		int srcbp, // src bits per pixel (8 or 16)
               bool srcil, // src interlaced flag
                int dwdth, // dst wdth
                int dhght, // dst hght
                int dstbp, // dst bits per pixel (8 or 16)
               bool dstil, // dst interlaced flag
                int &iRet  // OUTPUT - error code
           )
{
   iRet = 0;

   // register src wdth
   wsrc = swdth;

   // register src hght
   hsrc = shght;

   // register src bits per pixel
   srcBitsPerPixel = srcbp;

   // register src interlaced flag
   srcInterlaced = srcil;

   // allocate and build the src row table
   srcTbl = new OFENTRY[hsrc];
   if (srcInterlaced) {
      for (int i=0;i<hsrc;i++) {
         srcTbl[i].rowfld = i&1;
         srcTbl[i].rowoff = (i>>1)*wsrc;
      }
   }
   else {
      for (int i=0;i<hsrc;i++) {
         srcTbl[i].rowfld = 0;
         srcTbl[i].rowoff = i*wsrc;
      }
   }

   // register dst wdth
   wdst = dwdth;

   // register dst hght
   hdst = dhght;

   // register dst bits per pixel
   dstBitsPerPixel = dstbp;

   // register dst interlaced flag
   dstInterlaced = dstil;

   // allocate and build the dst row table
   dstTbl = new OFENTRY[hdst];
   if (dstInterlaced) {
      for (int i=0;i<hdst;i++) {
         dstTbl[i].rowfld = i&1;
         dstTbl[i].rowoff = (i>>1)*wdst;
      }
   }
   else {
      for (int i=0;i<hdst;i++) {
         dstTbl[i].rowfld = 0;
         dstTbl[i].rowoff = i*wdst;
      }
   }

   // src window
   // the default is a floating point rectangle
   // with explicit coords across the whole src
   // the max X is reckoned in pixel-pairs

   fsrcWin[0].x = 0.0;
   fsrcWin[0].y = 0.0;

   fsrcWin[1].x = (double)wsrc;
   fsrcWin[1].y = 0.0;

   fsrcWin[2].x = (double)wsrc;
   fsrcWin[2].y = (double)hsrc;

   fsrcWin[3].x = 0.0;
   fsrcWin[3].y = (double)hsrc;

   fsrcWin[4].x = 0.0;
   fsrcWin[4].y = 0.0;

   // dst window
   // the default is a Manhattan rectangle
   // across the whole dst frame

   dstMinRow = 0;
   dstMinCol = 0;
   dstMaxRow = hdst-1;
   dstMaxCol = wdst-1;

   dstWinHght = (dstMaxRow - dstMinRow) + 1;
   dstWinWdth = (dstMaxCol - dstMinCol) + 1;

   // number of stripes subdividing the work
#if !defined(__linux)
   nStripes = sysmp(MP_NPROCS);
#else
#warning Setting nStripes = 1.  Should really calculate it.
   nStripes = 2;
#endif // !defined(__linux)

   nStripes = 2; // TTT

   // the src pitch in units of U
   srcPitch = wsrc<<1;

   // the dst pitch in units of U
   dstPitch = wdst<<1;

   // 8-bit is in a  UINT8 array 

   bgnd8[0]  = 0x80;
   bgnd8[1]  = 0x19;
   bgnd8[2]  = 0x80;
   bgnd8[3]  = 0x19;

   // 10-bit is in a  UINT8 array 

   bgnd10[0] = 0x80;
   bgnd10[1] = 0x06;
   bgnd10[2] = 0x48;
   bgnd10[3] = 0x00;
   bgnd10[4] = 0x64;

   // 16-bit is in a UINT16 array

   bgnd16[0] = 0x1900; // Y
   bgnd16[1] = 0x8000; // U
   bgnd16[2] = 0x8000; // V

   // assume there are no auxiliary buffers
   xfmBuf[0] = NULL;
   xfmBuf[1] = NULL;

   ////////////////////////////////////////////////

   // the conversion is done on several equal-sized
   // stripes in parallel, one stripe for each proc.

   cvtThread.PrimaryFunction = (void (*)(void *,int))NULL;
   cvtThread.SecondaryFunction = (void (*)(void *))NULL;
   if (srcBitsPerPixel==8) {
      if (dstBitsPerPixel==8) {

         linEng = new CLineEngYUV8();
         linEng->setFrameBufferDimensions(wdst,hdst,wdst*2,dstInterlaced);
         cvtThread.PrimaryFunction = (void (*)(void *, int))&stripe8to8;
         rctThread.PrimaryFunction = (void (*)(void *, int))&rect8;

         // one full line of the BGND color for the mat 
         linBuf = new unsigned char[(wdst/2)*4];
         unsigned char *dst = linBuf;
         for (int i=0;i<(wdst/2);i++) {
            *dst++ = bgnd8[0];
            *dst++ = bgnd8[1];
            *dst++ = bgnd8[2];
            *dst++ = bgnd8[3];
         }
      }
      else if (dstBitsPerPixel==10) {
      }
      else if (dstBitsPerPixel==16) {
      }
   }
   else if (srcBitsPerPixel==10) {
      if (dstBitsPerPixel==8) {

         //////////////////////////////////////////////////////////////////////
         //
         // this code sets up for generating 8-bit proxies from 10-bit DVS
         // input. The pathway is to generate a 1/3 width x 1/2 height 8-bit
         // intermediate frame, and then use the 8to8 stripe code to create
         // the proxy
         //
         // rebuild the src row table with 1/3 the pitch
         // this is needed for the 1/3 width intermediate
         srcTbl = new OFENTRY[hsrc];
         if (srcInterlaced) {
            for (int i=0;i<hsrc;i++) {
               srcTbl[i].rowfld = i&1;
               srcTbl[i].rowoff = (i>>1)*(wsrc/3);
            }
         }
         else {
            for (int i=0;i<hsrc;i++) {
               srcTbl[i].rowfld = 0;
               srcTbl[i].rowoff = i*(wsrc/3);
            }
         }

         // allocate auxiliary buffers
         int fldbyts = (wsrc/2)*4*hsrc;
         if (srcInterlaced) {
             fldbyts /= 2;
             xfmBuf[0] = new MTI_UINT8[fldbyts];
             xfmBuf[1] = new MTI_UINT8[fldbyts];
         }
         else {
             xfmBuf[0] = new MTI_UINT8[fldbyts];
         }

         linEng = new CLineEngYUV8();
         linEng->setFrameBufferDimensions(wdst,hdst,wdst*2,dstInterlaced);
         cvtThread.PrimaryFunction = (void (*)(void *, int))&stripe8to8;
         rctThread.PrimaryFunction = (void (*)(void *, int))&rect8;
         xfmThread.PrimaryFunction = (void (*)(void *, int))&xfmDVS10toYUV8;

         // one full line of the BGND color for the mat
         linBuf = new unsigned char[(wdst/2)*4];
         unsigned char *dst = linBuf;
         for (int i=0;i<(wdst/2);i++) {
            *dst++ = bgnd8[0];
            *dst++ = bgnd8[1];
            *dst++ = bgnd8[2];
            *dst++ = bgnd8[3];
         }

         //
         //////////////////////////////////////////////////////////////////////
      }
      else if (dstBitsPerPixel==10) {

         linEng = new CLineEngYUV10();
         linEng->setFrameBufferDimensions(wdst,hdst,(wdst/2)*5,dstInterlaced);
         cvtThread.PrimaryFunction = (void (*)(void *, int))&stripe10to10;
         rctThread.PrimaryFunction = (void (*)(void *, int))&rect10;

         // one full line of the BGND color for the mat
         linBuf = new unsigned char[(wdst/2)*5];
         unsigned char *dst = linBuf;
         for (int i=0;i<(wdst/2);i++) {
            *dst++ = bgnd10[0];
            *dst++ = bgnd10[1];
            *dst++ = bgnd10[2];
            *dst++ = bgnd10[3];
            *dst++ = bgnd10[4];
         }

      }
      else if (dstBitsPerPixel==16) {
      }
   }
   else if (srcBitsPerPixel==16) {
      if (dstBitsPerPixel==8) {
      }
      else if (dstBitsPerPixel==10) {
      }
      else if (dstBitsPerPixel==16) {
         linEng = new CLineEngRGB16();
         linEng->setFrameBufferDimensions(wdst,hdst,wdst*6,dstInterlaced);
         cvtThread.PrimaryFunction = (void (*)(void *, int))&stripe16to16;
         rctThread.PrimaryFunction = (void (*)(void *, int))&rect16;

         // one full line of the BGND color for the mat
         linBuf = new unsigned char[wdst*6];
         unsigned short *dst = (unsigned short *)linBuf;
         for (int i=0;i<wdst;i++) {
            *dst++ = bgnd16[0];
            *dst++ = bgnd16[1];
            *dst++ = bgnd16[2];
         }

      }
   }

  if (cvtThread.PrimaryFunction == NULL)
   {
    TRACE_0(errout << "PanAndScan: Unable to accommodate this bit combination");
    iRet = IMGTOOL_ERROR_PAN_SCAN_UNSUPPORTED_BIT_DEPTH_COMBO;
    return;
   }

   // graphic engine with default windows

   grEng  = new CGraphicEngine(linEng);
   grEng->setWorldWindow(0,0,wsrc,hsrc);
   grEng->setScreenWindow(0,0,wdst,hdst);

   // complete thread entries for the image conversion

   cvtThread.SecondaryFunction = (void (*)(void *))&outline;
   cvtThread.CallBackFunction  = NULL;
   cvtThread.vpApplicationData = this;
   cvtThread.iNThread          = nStripes;
   cvtThread.iNJob             = nStripes;

#ifdef CVT_MULTI
   iRet = MThreadAlloc(&cvtThread);
   if (iRet)
    {
     TRACE_0(errout << "PanAndScan: Unable to alloc cvt thread");
     return;
    }
#endif

    // ..and for the mat rectangle draw

    rctThread.SecondaryFunction = NULL;
    rctThread.CallBackFunction  = NULL;
    rctThread.vpApplicationData = this;
    rctThread.iNThread          = nStripes;
    rctThread.iNJob             = nStripes;

#ifdef RCT_MULTI
   iRet = MThreadAlloc(&rctThread);
   if (iRet)
    {
     TRACE_0(errout << "PanAndScan: Unable to alloc rct thread");
     return;
    }
#endif

    // .. and for the DVS 10-bit to YUV 8-bit xformer

    xfmThread.SecondaryFunction = NULL;
    xfmThread.CallBackFunction  = NULL;
    xfmThread.vpApplicationData = this;
    xfmThread.iNThread          = nStripes;
    xfmThread.iNJob             = nStripes;

#ifdef XFM_MULTI
    if (xfmBuf[0] != NULL) {

       iRet = MThreadAlloc(&xfmThread);
       if (iRet)
        {
         TRACE_0(errout << "PanAndScan: Unable to alloc xfm thread");
         return;
        }
    }
#endif

   /////////////////////////////////////////////////

}

CPanAndScan::
~CPanAndScan()
{
   int iRet;

   // deallocate the threads
#ifdef CVT_MULTI
   iRet = MThreadFree(&cvtThread);
   if (iRet)
    {
     //unexpected();
     TRACE_0(errout << "PanAndScan: Can't free CVT thread");
    }
#endif

#ifdef RCT_MULTI
   iRet = MThreadFree(&rctThread);
   if (iRet)
    {
     //unexpected();
     TRACE_0(errout << "PanAndScan: Can't free RCT thread");
    }
#endif

   if (xfmBuf[0] != NULL) { // DVS-10 to YUV-8 xformer enabled

#ifdef XFM_MULTI
      iRet = MThreadFree(&xfmThread);
      if (iRet)
       {
         //unexpected();
         TRACE_0(errout << "PanAndScan: Can't free XFM thread");
       }
#endif
       delete [] xfmBuf[0];
       delete [] xfmBuf[1];
   }

   // deallocate the one-line mat buffer
   delete linBuf;

   // deallocate the line-engine
   delete linEng;

   // and the tables
   delete [] dstTbl;
   delete [] srcTbl;
}

void CPanAndScan::
setSrcWindow(
              VERTEX *winSrc
            )
{
   for (int i=0;i<4;i++) {

      // doubles 

      fsrcWin[i].x = winSrc[i].x;
      fsrcWin[i].y = winSrc[i].y;

   }
   fsrcWin[4].x = winSrc[0].x;
   fsrcWin[4].y = winSrc[0].y;

}

void CPanAndScan::
setSrcWindow(
              double x0, double y0,
              double x1, double y1,
              double x2, double y2,
              double x3, double y3
            )
{
   // doubles

   fsrcWin[0].x = x0;
   fsrcWin[0].y = y0;

   fsrcWin[1].x = x1;
   fsrcWin[1].y = y1;

   fsrcWin[2].x = x2;
   fsrcWin[2].y = y2;

   fsrcWin[3].x = x3;
   fsrcWin[3].y = y3;

   fsrcWin[4].x = x0;
   fsrcWin[4].y = y0;

}

void CPanAndScan::
setDstWindow(
              int minRowDst,
              int minColDst,
              int maxRowDst,
              int maxColDst
            )
{
   // adjust the max's to conform to KM convention
   maxRowDst -= 1;
   maxColDst -= 1;

   if ((minRowDst<0)||(maxRowDst>hdst-1)||(minRowDst>maxRowDst)) return;
   if ((minColDst<0)||(maxColDst>wdst-1)||(minColDst>maxColDst)) return;

   dstMinRow = minRowDst;
   dstMaxRow = maxRowDst;

   // these coords are reckoned in pixels
   dstMinCol = minColDst;
   dstMaxCol = maxColDst;

   dstWinHght = dstMaxRow - dstMinRow + 1;
   dstWinWdth = dstMaxCol - dstMinCol + 1;

   // the window we map TO 
   grEng->setScreenWindow(minColDst,minRowDst,maxColDst+1,maxRowDst+1);
}

void CPanAndScan::
setWindows(
             VERTEX *winSrc,

             int minRowDst,
             int minColDst,
             int maxRowDst,
             int maxColDst
          )
{

   // the SRC window

   for (int i=0;i<4;i++) {

      // doubles

      fsrcWin[i].x = winSrc[i].x;
      fsrcWin[i].y = winSrc[i].y;

   }
   fsrcWin[4].x = winSrc[0].x;
   fsrcWin[4].y = winSrc[0].y;

   // the DST window

   // adjust the max's to conform to KM convention
   maxRowDst -= 1;
   maxColDst -= 1;

   if ((minRowDst<0)||(maxRowDst>hdst-1)||(minRowDst>maxRowDst)) return;
   if ((minColDst<0)||(maxColDst>wdst-1)||(minColDst>maxColDst)) return;

   dstMinRow = minRowDst;
   dstMaxRow = maxRowDst;

   // these coords are reckoned in pixels
   dstMinCol = minColDst;
   dstMaxCol = maxColDst;

   dstWinHght = dstMaxRow - dstMinRow + 1;
   dstWinWdth = dstMaxCol - dstMinCol + 1;

   // the window we map TO 
   grEng->setScreenWindow(minColDst,minRowDst,maxColDst+1,maxRowDst+1);
}

void CPanAndScan::
setBGNDColor(
               int Y, int U, int V 
// same for    int R, int G, int B
             )
{
   MTI_UINT8 *dstb = linBuf;
   MTI_UINT16 *dstw = (MTI_UINT16 *) linBuf;

   switch(dstBitsPerPixel) {

      case 8:
         bgnd8[0] = (MTI_UINT8) (U>>8);
         bgnd8[1] = (MTI_UINT8) (Y>>8);
         bgnd8[2] = (MTI_UINT8) (V>>8);
         bgnd8[3] = (MTI_UINT8) (Y>>8);

         // load the one-line buffer
         for (int i=0;i<(wdst/2);i++) {
            *dstb++ = bgnd8[0];
            *dstb++ = bgnd8[1];
            *dstb++ = bgnd8[2];
            *dstb++ = bgnd8[3];
         }

      break;

      case 10:
         MTI_UINT64 r10;
         r10 = (U>>6);
         r10 = (r10<<10) + (Y>>6);
         r10 = (r10<<10) + (V >>6);
         r10 = (r10<<10) + (Y>>6);
         bgnd10[0] = (MTI_UINT8)(r10>>32);
         bgnd10[1] = (MTI_UINT8)(r10>>24);
         bgnd10[2] = (MTI_UINT8)(r10>>16);
         bgnd10[3] = (MTI_UINT8)(r10>>8);
         bgnd10[4] = (MTI_UINT8)(r10);

         // load the one-line buffer
         for (int i=0;i<(wdst/2);i++) {
            *dstb++ = bgnd10[0];
            *dstb++ = bgnd10[1];
            *dstb++ = bgnd10[2];
            *dstb++ = bgnd10[3];
            *dstb++ = bgnd10[4];
         }

      break;

      case 16:
         bgnd16[0] = (MTI_UINT16) Y;
         bgnd16[1] = (MTI_UINT16) U;
         bgnd16[2] = (MTI_UINT16) V;

         // load the one-line buffer
         for (int i=0;i<wdst;i++) {
            *dstw++ = bgnd16[0];
            *dstw++ = bgnd16[1];
            *dstw++ = bgnd16[2];
         }

      break;

      default:
      break;

   }

} 

//////////////////////////////////////////////////////////////////////////////

void stripe8to8(CPanAndScan *vp, int iJob)
{
 MTI_INT32 vdelX = vp->vdelX;
 MTI_INT32 vdelY = vp->vdelY;
 MTI_INT32 hdelX = vp->hdelX;
 MTI_INT32 hdelY = vp->hdelY;

 MTI_UINT8 *srcbuf[2];
 srcbuf[0] = (MTI_UINT8 *)vp->srcbuf[0];
 srcbuf[1] = (MTI_UINT8 *)vp->srcbuf[1];
  OFENTRY *srcTbl = vp->srcTbl;

 MTI_UINT8 *dstbuf[2];
 dstbuf[0] = (MTI_UINT8 *)vp->dstbuf[0];
 dstbuf[1] = (MTI_UINT8 *)vp->dstbuf[1];
  OFENTRY *dstTbl = vp->dstTbl;
  int dstMinCol  = vp->dstMinCol;
  int dstWinWdth = vp->dstWinWdth;

 MTI_UINT8 bgnd0 = vp->bgnd8[0];
 MTI_UINT8 bgnd1 = vp->bgnd8[1];
 MTI_UINT8 bgnd2 = vp->bgnd8[2];
 MTI_UINT8 bgnd3 = vp->bgnd8[3];

  int wlim = vp->wsrc - 1;
  int hlim = vp->hsrc - 1;

 // calculate the stripe lines
 int lins = vp->dstWinHght / vp->nStripes;
 MTI_INT32 beg = iJob*lins;
 if (iJob==vp->nStripes-1) {
    lins = vp->dstWinHght - beg;
 }

 // the beg pt of the scan
 MTI_INT32 rowX = vp->begX + beg * vdelX;
 MTI_INT32 rowY = vp->begY + beg * vdelY;

 // the cur pt of the scan
 MTI_INT32 colX;
 MTI_INT32 colY;

 // the end pt of the scan
 MTI_INT32 endX = rowX + (dstWinWdth-1)* hdelX;
 MTI_INT32 endY = rowY + (dstWinWdth-1)* hdelY;

 // 9/25/01 - adj pos of first line of dst window 
 beg += vp->dstMinRow;

 if (vp->parLin) { // use closest line of correct parity

   for (int i=beg;i<beg+lins;i++) {

      // get parity of dst line
      int oddEven = i&1;

      int Xbeg = rowX >> 16;
      int Ybeg = rowY >> 16;
      int Xend = endX >> 16;
      int Yend = endY >> 16;

      if ((Xbeg>=0)&&(Xbeg<=wlim)&&(Xend>=0)&&(Xend<=wlim)&&
          (Ybeg>=0)&&(Ybeg<=hlim)&&(Yend>=0)&&(Yend<=hlim)) { // scan-line is inside

         colX = rowX;
         colY = rowY;

          OFENTRY *dstrow = dstTbl + i;

         // ysrc -> pixel of src
          MTI_UINT8 *colsrc;

         // coldst -> pixel of dst
          MTI_UINT8 *coldst = dstbuf[dstrow->rowfld] + ((dstrow->rowoff + dstMinCol)>>1)*4;

          MTI_UINT32 phase = (dstMinCol&1); // 0 for U, 1 for V

         // REWRITTEN TO ELIMINATE PHASE VARIABLE

         int j = 0;
         if (phase==0) {

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

                int x   = colX >> 16;
                int y   = colY >> 16;
               y = (y & ~1) + oddEven;

                OFENTRY *srcrow = srcTbl + y;

               colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)>>1)*4);

               *coldst++ = *colsrc;
               if (x&1)
                  *coldst++ = *(colsrc+3);
               else
                  *coldst++ = *(colsrc+1);

               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;
               y = (y & ~1) + oddEven;

               srcrow = srcTbl + y;

               colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)>>1)*4);

               *coldst++ = *(colsrc+2);
               if (x&1)
                  *coldst++ = *(colsrc+3);
               else
                  *coldst++ = *(colsrc+1);

               colX += hdelX;
               colY += hdelY;
            }

         }
         else {

            coldst += 2;

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

                int x   = colX >> 16;
                int y   = colY >> 16;
               y = (y & ~1) + oddEven;


                OFENTRY *srcrow = srcTbl + y;

               colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)>>1)*4);

               *coldst++ = *(colsrc+2);
               if (x&1)
                  *coldst++ = *(colsrc+3);
               else
                  *coldst++ = *(colsrc+1);

               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;
               y = (y & ~1) + oddEven;

               srcrow = srcTbl + y;

               colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)>>1)*4);

               *coldst++ = *colsrc;
               if (x&1)
                  *coldst++ = *(colsrc+3);
               else
                  *coldst++ = *(colsrc+1);

               colX += hdelX;
               colY += hdelY;

            }

         }

      }
      else {  // scan-line crosses in and out of source frame

         colX = rowX;
         colY = rowY;

          OFENTRY *dstrow = dstTbl + i;

         // ysrc -> pixel of src
          MTI_UINT8 *colsrc;

         // coldst -> pixel of dst
          MTI_UINT8 *coldst = dstbuf[dstrow->rowfld] + ((dstrow->rowoff + dstMinCol)>>1)*4;

          MTI_UINT32 phase = (dstMinCol&1); // 0 for U, 1 for V

         // REWRITTEN TO ELIMINATE PHASE VARIABLE

         int j = 0;
         if (phase==0) {

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

                int x   = colX >> 16;
                int y   = colY >> 16;
               y = (y & ~1) + oddEven;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

                   OFENTRY *srcrow = srcTbl + y;

                  colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)>>1)*4);

                  *coldst++ = *colsrc;
                  if (x&1)
                     *coldst++ = *(colsrc+3);
                  else
                     *coldst++ = *(colsrc+1);

               }
               else { // copy the bgnd UYVY

                  *coldst++ = bgnd0;
                  *coldst++ = bgnd1;

               }

               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;
               y = (y & ~1) + oddEven;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

                   OFENTRY *srcrow = srcTbl + y;

                  colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)>>1)*4);

                  *coldst++ = *(colsrc+2);
                  if (x&1)
                     *coldst++ = *(colsrc+3);
                  else
                     *coldst++ = *(colsrc+1);

               }
               else { // copy the bgnd UYVY

                  *coldst++ = bgnd2;
                  *coldst++ = bgnd3;

               }

               colX += hdelX;
               colY += hdelY;

            }

         }
         else {

            coldst += 2;

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

                int x   = colX >> 16;
                int y   = colY >> 16;
               y = (y & ~1) + oddEven;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

                   OFENTRY *srcrow = srcTbl + y;

                  colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)>>1)*4);

                  *coldst++ = *(colsrc+2);
                  if (x&1)
                     *coldst++ = *(colsrc+3);
                  else
                     *coldst++ = *(colsrc+1);

               }
               else { // copy the bgnd UYVY

                  *coldst++ = bgnd2;
                  *coldst++ = bgnd3;

               }

               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;
               y = (y & ~1) + oddEven;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

                   OFENTRY *srcrow = srcTbl + y;

                  colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)>>1)*4);

                  *coldst++ = *colsrc;
                  if (x&1)
                     *coldst++ = *(colsrc+3);
                  else
                     *coldst++ = *(colsrc+1);

               }
               else { // copy the bgnd UYVY

                  *coldst++ = bgnd0;
                  *coldst++ = bgnd1;

               }

               colX += hdelX;
               colY += hdelY;

            }

         }

      }

      // advance to the next row

      rowX += vdelX;
      rowY += vdelY;

      endX += vdelX;
      endY += vdelY;

   }

 }
 else { // dst line from closest line

   for (int i=beg;i<beg+lins;i++) {

      int Xbeg = rowX >> 16;
      int Ybeg = rowY >> 16;
      int Xend = endX >> 16;
      int Yend = endY >> 16;

      if ((Xbeg>=0)&&(Xbeg<=wlim)&&(Xend>=0)&&(Xend<=wlim)&&
          (Ybeg>=0)&&(Ybeg<=hlim)&&(Yend>=0)&&(Yend<=hlim)) { // scan-line is inside

         colX = rowX;
         colY = rowY;

          OFENTRY *dstrow = dstTbl + i;

         // ysrc -> pixel of src
          MTI_UINT8 *colsrc;

         // coldst -> pixel of dst
          MTI_UINT8 *coldst = dstbuf[dstrow->rowfld] + ((dstrow->rowoff + dstMinCol)>>1)*4;

          MTI_UINT32 phase = (dstMinCol&1); // 0 for U, 1 for V

         // REWRITTEN TO ELIMINATE PHASE VARIABLE

         int j = 0;
         if (phase==0) {

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

                int x   = colX >> 16;
                int y   = colY >> 16;

                OFENTRY *srcrow = srcTbl + y;

               colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)>>1)*4);

               *coldst++ = *colsrc;
               if (x&1)
                  *coldst++ = *(colsrc+3);
               else
                  *coldst++ = *(colsrc+1);

               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;

               srcrow = srcTbl + y;

               colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)>>1)*4);

               *coldst++ = *(colsrc+2);
               if (x&1)
                  *coldst++ = *(colsrc+3);
               else
                  *coldst++ = *(colsrc+1);

               colX += hdelX;
               colY += hdelY;
            }

         }
         else {

            coldst += 2;

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

                int x   = colX >> 16;
                int y   = colY >> 16;

                OFENTRY *srcrow = srcTbl + y;

               colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)>>1)*4);

               *coldst++ = *(colsrc+2);
               if (x&1)
                  *coldst++ = *(colsrc+3);
               else
                  *coldst++ = *(colsrc+1);

               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;

               srcrow = srcTbl + y;

               colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)>>1)*4);

               *coldst++ = *colsrc;
               if (x&1)
                  *coldst++ = *(colsrc+3);
               else
                  *coldst++ = *(colsrc+1);

               colX += hdelX;
               colY += hdelY;

            }

         }

      }
      else {  // scan-line crosses in and out of source frame

         colX = rowX;
         colY = rowY;

          OFENTRY *dstrow = dstTbl + i;

         // ysrc -> pixel of src
          MTI_UINT8 *colsrc;

         // coldst -> pixel of dst
          MTI_UINT8 *coldst = dstbuf[dstrow->rowfld] + ((dstrow->rowoff + dstMinCol)>>1)*4;

          MTI_UINT32 phase = (dstMinCol&1); // 0 for U, 1 for V

         // REWRITTEN TO ELIMINATE PHASE VARIABLE

         int j = 0;
         if (phase==0) {

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

                int x   = colX >> 16;
                int y   = colY >> 16;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

                   OFENTRY *srcrow = srcTbl + y;

                  colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)>>1)*4);

                  *coldst++ = *colsrc;
                  if (x&1)
                     *coldst++ = *(colsrc+3);
                  else
                     *coldst++ = *(colsrc+1);

               }
               else { // copy the bgnd UYVY

                  *coldst++ = bgnd0;
                  *coldst++ = bgnd1;

               }

               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

                   OFENTRY *srcrow = srcTbl + y;

                  colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)>>1)*4);

                  *coldst++ = *(colsrc+2);
                  if (x&1)
                     *coldst++ = *(colsrc+3);
                  else
                     *coldst++ = *(colsrc+1);

               }
               else { // copy the bgnd UYVY

                  *coldst++ = bgnd2;
                  *coldst++ = bgnd3;

               }

               colX += hdelX;
               colY += hdelY;

            }

         }
         else {

            coldst += 2;

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

                int x   = colX >> 16;
                int y   = colY >> 16;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

                   OFENTRY *srcrow = srcTbl + y;

                  colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)>>1)*4);

                  *coldst++ = *(colsrc+2);
                  if (x&1)
                     *coldst++ = *(colsrc+3);
                  else
                     *coldst++ = *(colsrc+1);

               }
               else { // copy the bgnd UYVY

                  *coldst++ = bgnd2;
                  *coldst++ = bgnd3;

               }

               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

                   OFENTRY *srcrow = srcTbl + y;

                  colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)>>1)*4);

                  *coldst++ = *colsrc;
                  if (x&1)
                     *coldst++ = *(colsrc+3);
                  else
                     *coldst++ = *(colsrc+1);

               }
               else { // copy the bgnd UYVY

                  *coldst++ = bgnd0;
                  *coldst++ = bgnd1;

               }

               colX += hdelX;
               colY += hdelY;

            }

         }

      }

      // advance to the next row

      rowX += vdelX;
      rowY += vdelY;

      endX += vdelX;
      endY += vdelY;

   }

 }

}

// segment to render 8-bit mat rectangles 
void rect8(CPanAndScan *vp, int iJob)
{
   MTI_UINT8 *dstbuf[2];
   dstbuf[0] = (MTI_UINT8 *)vp->dstbuf[0];
   dstbuf[1] = (MTI_UINT8 *)vp->dstbuf[1];
    OFENTRY *dstTbl = vp->dstTbl;
   MTI_UINT8 *linBuf = vp->linBuf;
    int rctMinCol  = vp->rctMinCol;
    int rctWinWdth = ((vp->rctWinWdth)>>1)*4;

   // calculate the stripe lines
   int lins = vp->rctWinHght / vp->nStripes;
   MTI_INT32 beg = iJob*lins;
   if (iJob==vp->nStripes-1) {
      lins = vp->rctWinHght - beg;
   }

   beg += vp->rctMinRow;
   for (int i=beg;i<beg+lins;i++) {

       OFENTRY *dstrow = dstTbl + i;

      // coldst -> pixel of dst
       MTI_UINT8 *coldst = dstbuf[dstrow->rowfld] + ((dstrow->rowoff + rctMinCol)>>1)*4;

      // copy from the one-line buffer
      memcpy(coldst,linBuf,rctWinWdth);

   }

}

////////////////////////////////////////////////////////////////////////////////

void stripe10to10(CPanAndScan *vp, int iJob)
{
 MTI_INT32 vdelX = vp->vdelX;
 MTI_INT32 vdelY = vp->vdelY;
 MTI_INT32 hdelX = vp->hdelX;
 MTI_INT32 hdelY = vp->hdelY;

 MTI_UINT8 *srcbuf[2];
 srcbuf[0] = (MTI_UINT8 *)vp->srcbuf[0];
 srcbuf[1] = (MTI_UINT8 *)vp->srcbuf[1];
  OFENTRY *srcTbl = vp->srcTbl;

 MTI_UINT8 *dstbuf[2];
 dstbuf[0] = (MTI_UINT8 *)vp->dstbuf[0];
 dstbuf[1] = (MTI_UINT8 *)vp->dstbuf[1];
  OFENTRY *dstTbl = vp->dstTbl;
  int dstMinCol  = vp->dstMinCol;
  int dstWinWdth = vp->dstWinWdth;

 MTI_UINT8 bgnd0 = vp->bgnd10[0];
 MTI_UINT8 bgnd1 = vp->bgnd10[1];
 MTI_UINT8 bgnd2 = vp->bgnd10[2];
 MTI_UINT8 bgnd3 = vp->bgnd10[3];
 MTI_UINT8 bgnd4 = vp->bgnd10[4];

  int wlim = vp->wsrc - 1;
  int hlim = vp->hsrc - 1;

 // calculate the stripe lines
 int lins = vp->dstWinHght / vp->nStripes;
 MTI_INT32 beg = iJob*lins;
 if (iJob==vp->nStripes-1) {
    lins = vp->dstWinHght - beg;
 }

 // the beginning pt of the scan
 MTI_INT32 rowX = vp->begX + beg * vdelX;
 MTI_INT32 rowY = vp->begY + beg * vdelY;

 MTI_INT32 endX = rowX + (dstWinWdth-1)* hdelX;
 MTI_INT32 endY = rowY + (dstWinWdth-1)* hdelY;

 // 9/25/01 - adj pos of first line of dst window
 beg += vp->dstMinRow;

 if (vp->parLin) { // use closest line of correct parity

   for (int i=beg;i<beg+lins;i++) {

      // get parity of dst line
      int oddEven = (i&1);

      int Xbeg = rowX >> 16;
      int Ybeg = rowY >> 16;
      int Xend = endX >> 16;
      int Yend = endY >> 16;

      if ((Xbeg>=0)&&(Xbeg<=wlim)&&(Xend>=0)&&(Xend<=wlim)&&
          (Ybeg>=0)&&(Ybeg<=hlim)&&(Yend>=0)&&(Yend<=hlim)) { // scan-line is inside

         MTI_INT32 colX = rowX;
         MTI_INT32 colY = rowY;

          OFENTRY *dstrow = dstTbl + i;

         // ysrc -> pixel of src
          MTI_UINT8 *colsrc;

         // coldst -> pixel of dst
          MTI_UINT8 *coldst = dstbuf[dstrow->rowfld] + ((dstrow->rowoff + dstMinCol)>>1)*5;

          MTI_UINT32 phase = (dstMinCol&1); // 0 for U, 1 for V

         // REWRITTEN TO ELIMINATE PHASE VARIABLE

         int j = 0;
         if (phase==0) {

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

                int x   = colX >> 16;
                int y   = colY >> 16;
               y = (y & ~1) + oddEven;

               // phase 0

                OFENTRY *srcrow = srcTbl + y;

               colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff + x)>>1)*5);

               *coldst++ = *colsrc;
               if ((x&1)==0) {
                  *coldst++ = *(colsrc+1);
                  *coldst   = ((*coldst)&0x0f) + ((*(colsrc+2))&0xf0);
               }
               else {
                  *coldst++ =  (*(colsrc+1)&0xc0)  +
                              ((*(colsrc+3)&0x03) << 4) +
                              ((*(colsrc+4)&0xf0) >> 4);
                  *coldst   = ((*(colsrc+4)&0x0f) << 4) + ((*coldst)&0x0f);
               }

               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;
               y = (y & ~1) + oddEven;

               // phase 1

               srcrow = srcTbl + y;

               colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff + x)>>1)*5);

               // autoincrement must be on separate line
               *coldst = ((*coldst)&0xf0) + ((*(colsrc+2))&0x0f);
               coldst++;
               if ((x&1)==1) {
                  *coldst++ = *(colsrc+3);
                  *coldst++ = *(colsrc+4);
               }
               else {
                  *coldst++ =  (*(colsrc+3)&0xfc)   +
                              ((*(colsrc+1)&0x30) >> 4);
                  *coldst++ = ((*(colsrc+1)&0x0f) << 4) +
                              ((*(colsrc+2)&0xf0) >> 4);
               }               

               colX += hdelX;
               colY += hdelY;
            }

         }
         else {

            coldst += 2;

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

                int x   = colX >> 16;
                int y   = colY >> 16;
               y = (y & ~1) + oddEven;

               // phase 1

                OFENTRY *srcrow = srcTbl + y;

               colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff + x)>>1)*5);

               // autoincrement must be on separate line
               *coldst = ((*coldst)&0xf0) + ((*(colsrc+2))&0x0f);
               coldst++;
               if ((x&1)==1) {
                  *coldst++ = *(colsrc+3);
                  *coldst++ = *(colsrc+4);
               }
               else {
                  *coldst++ =  (*(colsrc+3)&0xfc)   +
                              ((*(colsrc+1)&0x30) >> 4);
                  *coldst++ = ((*(colsrc+1)&0x0f) << 4) +
                              ((*(colsrc+2)&0xf0) >> 4);
               }               

               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;
               y = (y & ~1) + oddEven;

               // phase 0

               srcrow = srcTbl + y;

               colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff + x)>>1)*5);

               *coldst++ = *colsrc;
               if ((x&1)==0) {

                  *coldst++ = *(colsrc+1);
                  *coldst   = ((*coldst)&0x0f) + ((*(colsrc+2))&0xf0);

               }
               else {

                  *coldst++ =  (*(colsrc+1)&0xc0)  +
                              ((*(colsrc+3)&0x03) << 4) +
                              ((*(colsrc+4)&0xf0) >> 4);
                  *coldst   = ((*(colsrc+4)&0x0f) << 4) + ((*coldst)&0x0f);

               }

               colX += hdelX;
               colY += hdelY;

            }

         }

      }
      else {  // scan-line crosses in and out of source frame

         MTI_INT32 colX = rowX;
         MTI_INT32 colY = rowY;

          OFENTRY *dstrow = dstTbl + i;

         // ysrc -> pixel of src
          MTI_UINT8 *colsrc;

         // coldst -> pixel of dst
          MTI_UINT8 *coldst = dstbuf[dstrow->rowfld] + ((dstrow->rowoff + dstMinCol)>>1)*5;

          MTI_UINT32 phase = (dstMinCol&1); // 0 for U, 1 for V

         // REWRITTEN TO ELIMINATE PHASE VARIABLE

         int j = 0;
         if (phase==0) {

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

                int x   = colX >> 16;
                int y   = colY >> 16;
               y = (y & ~1) + oddEven;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

	          // phase 0

                   OFENTRY *srcrow = srcTbl + y;

                  colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff + x)>>1)*5);

                  *coldst++ = *colsrc;
                  if ((x&1)==0) {
                     *coldst++ = *(colsrc+1);
                     *coldst   = ((*coldst)&0x0f) + ((*(colsrc+2))&0xf0);
                  }
                  else {
                     *coldst++ =  (*(colsrc+1)&0xc0)   +
                                 ((*(colsrc+3)&0x03) << 4) +
                                 ((*(colsrc+4)&0xf0) >> 4);
                     *coldst   = ((*(colsrc+4)&0x0f) << 4) + ((*coldst)&0x0f);

                  }

               }
               else { // copy the bgnd UYVY

		  // phase 0

                  *coldst++ = bgnd0;
                  *coldst++ = bgnd1;
                  *coldst   = (*coldst&0x0f) + (bgnd2&0xf0);

               }

               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;
               y = (y & ~1) + oddEven;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

	          // phase 1

                   OFENTRY *srcrow = srcTbl + y;

                  colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff + x)>>1)*5);

                  // autoincrement must be on separate line
                  *coldst = ((*coldst)&0xf0) + ((*(colsrc+2))&0x0f);
                  coldst++;
                  if ((x&1)==1) {
                     *coldst++ = *(colsrc+3);
                     *coldst++ = *(colsrc+4);
                  }
                  else {
                     *coldst++ =  (*(colsrc+3)&0xfc)   +
                                 ((*(colsrc+1)&0x30) >> 4);
                     *coldst++ = ((*(colsrc+1)&0x0f) << 4) +
                                 ((*(colsrc+2)&0xf0) >> 4);
                  }               

               }
               else { // copy the bgnd UYVY

		  // phase 1

                  // autoincrement must be on separate line
                  *coldst = (*coldst&0xf0) + (bgnd2&0x0f);
                  coldst++;
                  *coldst++ = bgnd3;
                  *coldst++ = bgnd4;

               }

               colX += hdelX;
               colY += hdelY;

            }

         }
         else {

            coldst += 2;

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

                int x   = colX >> 16;
                int y   = colY >> 16;
               y = (y & ~1) + oddEven;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

	          // phase 1

                   OFENTRY *srcrow = srcTbl + y;

                  colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff + x)>>1)*5);

                  // autoincrement must be on separate line
                  *coldst = ((*coldst)&0xf0) + ((*(colsrc+2))&0x0f);
                  coldst++;
                  if ((x&1)==1) {
                     *coldst++ = *(colsrc+3);
                     *coldst++ = *(colsrc+4);
                  }
                  else {
                     *coldst++ =  (*(colsrc+3)&0xfc)   +
                                 ((*(colsrc+1)&0x30) >> 4);
                     *coldst++ = ((*(colsrc+1)&0x0f) << 4) +
                                 ((*(colsrc+2)&0xf0) >> 4);
                  }               

               }
               else { // copy the bgnd UYVY

		  // phase 1

                  *coldst   = (*coldst&0xf0) + (bgnd2&0x0f);
                  coldst++;
                  *coldst++ = bgnd3;
                  *coldst++ = bgnd4;

               }

               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;
               y = (y & ~1) + oddEven;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

	          // phase 0

                   OFENTRY *srcrow = srcTbl + y;

                  colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff + x)>>1)*5);

                  *coldst++ = *colsrc;
                  if ((x&1)==0) {
                     *coldst++ = *(colsrc+1);
                     *coldst   = (*coldst&0x0f) + ((*(colsrc+2))&0xf0);
                  }
                  else {
                     *coldst++ =  (*(colsrc+1)&0xc0)   +
                                 ((*(colsrc+3)&0x03) << 4) +
                                 ((*(colsrc+4)&0xf0) >> 4);
                     *coldst   = ((*(colsrc+4)&0x0f) << 4) + ((*coldst)&0xf0);

                  }

               }
               else { // copy the bgnd UYVY

		  // phase 0

                  *coldst++ = bgnd0;
                  *coldst++ = bgnd1;
                  *coldst   = (*coldst&0x0f) + (bgnd2&0xf0);

               }

               colX += hdelX;
               colY += hdelY;

            }

         }

      }

      // advance to the next row

      rowX += vdelX;
      rowY += vdelY;

      endX += vdelX;
      endY += vdelY;

   }

 }
 else { // dst line from closest line

   for (int i=beg;i<beg+lins;i++) {

      int Xbeg = rowX >> 16;
      int Ybeg = rowY >> 16;
      int Xend = endX >> 16;
      int Yend = endY >> 16;

      if ((Xbeg>=0)&&(Xbeg<=wlim)&&(Xend>=0)&&(Xend<=wlim)&&
          (Ybeg>=0)&&(Ybeg<=hlim)&&(Yend>=0)&&(Yend<=hlim)) { // scan-line is inside

         MTI_INT32 colX = rowX;
         MTI_INT32 colY = rowY;

          OFENTRY *dstrow = dstTbl + i;

         // ysrc -> pixel of src
          MTI_UINT8 *colsrc;

         // coldst -> pixel of dst
          MTI_UINT8 *coldst = dstbuf[dstrow->rowfld] + ((dstrow->rowoff + dstMinCol)>>1)*5;

          MTI_UINT32 phase = (dstMinCol&1); // 0 for U, 1 for V

         // REWRITTEN TO ELIMINATE PHASE VARIABLE

         int j = 0;
         if (phase==0) {

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

                int x   = colX >> 16;
                int y   = colY >> 16;

               // phase 0

                OFENTRY *srcrow = srcTbl + y;

               colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff + x)>>1)*5);

               *coldst++ = *colsrc;
               if ((x&1)==0) {
                  *coldst++ = *(colsrc+1);
                  *coldst   = ((*coldst)&0x0f) + ((*(colsrc+2))&0xf0);
               }
               else {
                  *coldst++ =  (*(colsrc+1)&0xc0)  +
                              ((*(colsrc+3)&0x03) << 4) +
                              ((*(colsrc+4)&0xf0) >> 4);
                  *coldst   = ((*(colsrc+4)&0x0f) << 4) + ((*coldst)&0x0f);
               }

               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;

               // phase 1

               srcrow = srcTbl + y;

               colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff + x)>>1)*5);

               // autoincrement must be on separate line
               *coldst = ((*coldst)&0xf0) + ((*(colsrc+2))&0x0f);
               coldst++;
               if ((x&1)==1) {
                  *coldst++ = *(colsrc+3);
                  *coldst++ = *(colsrc+4);
               }
               else {
                  *coldst++ =  (*(colsrc+3)&0xfc)   +
                              ((*(colsrc+1)&0x30) >> 4);
                  *coldst++ = ((*(colsrc+1)&0x0f) << 4) +
                              ((*(colsrc+2)&0xf0) >> 4);
               }               

               colX += hdelX;
               colY += hdelY;
            }

         }
         else {

            coldst += 2;

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

                int x   = colX >> 16;
                int y   = colY >> 16;

               // phase 1

                OFENTRY *srcrow = srcTbl + y;

               colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff + x)>>1)*5);

               // autoincrement must be on separate line
               *coldst = ((*coldst)&0xf0) + ((*(colsrc+2))&0x0f);
               coldst++;
               if ((x&1)==1) {
                  *coldst++ = *(colsrc+3);
                  *coldst++ = *(colsrc+4);
               }
               else {
                  *coldst++ =  (*(colsrc+3)&0xfc)   +
                              ((*(colsrc+1)&0x30) >> 4);
                  *coldst++ = ((*(colsrc+1)&0x0f) << 4) +
                              ((*(colsrc+2)&0xf0) >> 4);
               }               

               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;

               // phase 0

               srcrow = srcTbl + y;

               colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff + x)>>1)*5);

               *coldst++ = *colsrc;
               if ((x&1)==0) {

                  *coldst++ = *(colsrc+1);
                  *coldst   = ((*coldst)&0x0f) + ((*(colsrc+2))&0xf0);

               }
               else {

                  *coldst++ =  (*(colsrc+1)&0xc0)  +
                              ((*(colsrc+3)&0x03) << 4) +
                              ((*(colsrc+4)&0xf0) >> 4);
                  *coldst   = ((*(colsrc+4)&0x0f) << 4) + ((*coldst)&0x0f);

               }

               colX += hdelX;
               colY += hdelY;

            }

         }

      }
      else {  // scan-line crosses in and out of source frame

         MTI_INT32 colX = rowX;
         MTI_INT32 colY = rowY;

          OFENTRY *dstrow = dstTbl + i;

         // ysrc -> pixel of src
          MTI_UINT8 *colsrc;

         // coldst -> pixel of dst
          MTI_UINT8 *coldst = dstbuf[dstrow->rowfld] + ((dstrow->rowoff + dstMinCol)>>1)*5;

          MTI_UINT32 phase = (dstMinCol&1); // 0 for U, 1 for V

         // REWRITTEN TO ELIMINATE PHASE VARIABLE

         int j = 0;
         if (phase==0) {

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

                int x   = colX >> 16;
                int y   = colY >> 16;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

	          // phase 0

                   OFENTRY *srcrow = srcTbl + y;

                  colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff + x)>>1)*5);

                  *coldst++ = *colsrc;
                  if ((x&1)==0) {
                     *coldst++ = *(colsrc+1);
                     *coldst   = ((*coldst)&0x0f) + ((*(colsrc+2))&0xf0);
                  }
                  else {
                     *coldst++ =  (*(colsrc+1)&0xc0)   +
                                 ((*(colsrc+3)&0x03) << 4) +
                                 ((*(colsrc+4)&0xf0) >> 4);
                     *coldst   = ((*(colsrc+4)&0x0f) << 4) + ((*coldst)&0x0f);

                  }

               }
               else { // copy the bgnd UYVY

		  // phase 0

                  *coldst++ = bgnd0;
                  *coldst++ = bgnd1;
                  *coldst   = (*coldst&0x0f) + (bgnd2&0xf0);

               }

               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

	          // phase 1

                   OFENTRY *srcrow = srcTbl + y;

                  colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff + x)>>1)*5);

                  // autoincrement must be on separate line
                  *coldst = ((*coldst)&0xf0) + ((*(colsrc+2))&0x0f);
                  coldst++;
                  if ((x&1)==1) {
                     *coldst++ = *(colsrc+3);
                     *coldst++ = *(colsrc+4);
                  }
                  else {
                     *coldst++ =  (*(colsrc+3)&0xfc)   +
                                 ((*(colsrc+1)&0x30) >> 4);
                     *coldst++ = ((*(colsrc+1)&0x0f) << 4) +
                                 ((*(colsrc+2)&0xf0) >> 4);
                  }               

               }
               else { // copy the bgnd UYVY

		  // phase 1

                  // autoincrement must be on separate line
                  *coldst = (*coldst&0xf0) + (bgnd2&0x0f);
                  coldst++;
                  *coldst++ = bgnd3;
                  *coldst++ = bgnd4;

               }

               colX += hdelX;
               colY += hdelY;

            }

         }
         else {

            coldst += 2;

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

                int x   = colX >> 16;
                int y   = colY >> 16;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

	          // phase 1

                   OFENTRY *srcrow = srcTbl + y;

                  colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff + x)>>1)*5);

                  // autoincrement must be on separate line
                  *coldst = ((*coldst)&0xf0) + ((*(colsrc+2))&0x0f);
                  coldst++;
                  if ((x&1)==1) {
                     *coldst++ = *(colsrc+3);
                     *coldst++ = *(colsrc+4);
                  }
                  else {
                     *coldst++ =  (*(colsrc+3)&0xfc)   +
                                 ((*(colsrc+1)&0x30) >> 4);
                     *coldst++ = ((*(colsrc+1)&0x0f) << 4) +
                                 ((*(colsrc+2)&0xf0) >> 4);
                  }               

               }
               else { // copy the bgnd UYVY

		  // phase 1

                  *coldst   = (*coldst&0xf0) + (bgnd2&0x0f);
                  coldst++;
                  *coldst++ = bgnd3;
                  *coldst++ = bgnd4;

               }

               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

	          // phase 0

                   OFENTRY *srcrow = srcTbl + y;

                  colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff + x)>>1)*5);

                  *coldst++ = *colsrc;
                  if ((x&1)==0) {
                     *coldst++ = *(colsrc+1);
                     *coldst   = (*coldst&0x0f) + ((*(colsrc+2))&0xf0);
                  }
                  else {
                     *coldst++ =  (*(colsrc+1)&0xc0)   +
                                 ((*(colsrc+3)&0x03) << 4) +
                                 ((*(colsrc+4)&0xf0) >> 4);
                     *coldst   = ((*(colsrc+4)&0x0f) << 4) + ((*coldst)&0xf0);

                  }

               }
               else { // copy the bgnd UYVY

		  // phase 0

                  *coldst++ = bgnd0;
                  *coldst++ = bgnd1;
                  *coldst   = (*coldst&0x0f) + (bgnd2&0xf0);

               }

               colX += hdelX;
               colY += hdelY;

            }

         }

      }

      // advance to the next row

      rowX += vdelX;
      rowY += vdelY;

      endX += vdelX;
      endY += vdelY;

   }

 }

}

// segment to render 10-bit mat rectangles
void rect10(CPanAndScan *vp, int iJob)
{
   MTI_UINT8 *dstbuf[2];
   dstbuf[0] = (MTI_UINT8 *)vp->dstbuf[0];
   dstbuf[1] = (MTI_UINT8 *)vp->dstbuf[1];
    OFENTRY *dstTbl = vp->dstTbl;
   MTI_UINT8 *linBuf = vp->linBuf;
    int rctMinCol  = vp->rctMinCol;
    int rctWinWdth = ((vp->rctWinWdth)>>1)*5;

   // calculate the stripe lines
   int lins = vp->rctWinHght / vp->nStripes;
   MTI_INT32 beg = iJob*lins;
   if (iJob==vp->nStripes-1) {
      lins = vp->rctWinHght - beg;
   }

   beg += vp->rctMinRow;
   for (int i=beg;i<beg+lins;i++) {

       OFENTRY *dstrow = dstTbl + i;

      // coldst -> pixel of dst
       MTI_UINT8 *coldst = dstbuf[dstrow->rowfld] + ((dstrow->rowoff + rctMinCol)>>1)*5;

      // copy from the one-line buffer
      memcpy(coldst,linBuf,rctWinWdth);

   }

}

//////////////////////////////////////////////////////////////////////////////

#define GO
#ifdef _WINDOWS // WINDOWS code
void stripeDVS10to8(CPanAndScan *vp, int iJob)
{
 MTI_INT32 vdelX = vp->vdelX;
 MTI_INT32 vdelY = vp->vdelY;
 MTI_INT32 hdelX = vp->hdelX;
 MTI_INT32 hdelY = vp->hdelY;

 MTI_UINT8 *srcbuf[2];
 srcbuf[0] = (MTI_UINT8 *)vp->srcbuf[0];
 srcbuf[1] = (MTI_UINT8 *)vp->srcbuf[1];
 OFENTRY *srcTbl = vp->srcTbl;

 MTI_UINT8 *dstbuf[2];
 dstbuf[0] = (MTI_UINT8 *)vp->dstbuf[0];
 dstbuf[1] = (MTI_UINT8 *)vp->dstbuf[1];
 OFENTRY *dstTbl = vp->dstTbl;
 int dstMinCol  = vp->dstMinCol;
 int dstWinWdth = vp->dstWinWdth;

 MTI_UINT8 bgnd0 = vp->bgnd8[0];
 MTI_UINT8 bgnd1 = vp->bgnd8[1];
 MTI_UINT8 bgnd2 = vp->bgnd8[2];
 MTI_UINT8 bgnd3 = vp->bgnd8[3];

 int wlim = vp->wsrc - 1;
 int hlim = vp->hsrc - 1;

 // calculate the stripe lines
 int lins = vp->dstWinHght / vp->nStripes;
 MTI_INT32 beg = iJob*lins;
 if (iJob==vp->nStripes-1) {
    lins = vp->dstWinHght - beg;
 }

 // the beg pt of the scan
 MTI_INT32 rowX = vp->begX + beg * vdelX;
 MTI_INT32 rowY = vp->begY + beg * vdelY;

 // the cur pt of the scan
 MTI_INT32 colX;
 MTI_INT32 colY;

 // the end pt of the scan
 MTI_INT32 endX = rowX + (dstWinWdth-1)* hdelX;
 MTI_INT32 endY = rowY + (dstWinWdth-1)* hdelY;

 // 9/25/01 - adj pos of first line of dst window 
 beg += vp->dstMinRow;

 if (vp->parLin) { // use closest line of correct parity

   for (int i=beg;i<beg+lins;i++) {

      // get parity of dst line
      int oddEven = i&1;

      int Xbeg = rowX >> 16;
      int Ybeg = rowY >> 16;
      int Xend = endX >> 16;
      int Yend = endY >> 16;

      if ((Xbeg>=0)&&(Xbeg<=wlim)&&(Xend>=0)&&(Xend<=wlim)&&
          (Ybeg>=0)&&(Ybeg<=hlim)&&(Yend>=0)&&(Yend<=hlim)) { // scan-line is inside

         colX = rowX;
         colY = rowY;

         OFENTRY *dstrow = dstTbl + i;

         // ysrc -> pixel of src
          MTI_UINT32 *colsrc;

         // coldst -> pixel of dst
          MTI_UINT8 *coldst = dstbuf[dstrow->rowfld] + ((dstrow->rowoff + dstMinCol)>>1)*4;

         MTI_UINT32 phase = (dstMinCol&1); // 0 for U, 1 for V

          MTI_UINT32 unpack;

         // REWRITTEN TO ELIMINATE PHASE VARIABLE

         int j = 0;
         if (phase==0) {

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               int x   = colX >> 16;
               int y   = colY >> 16;
               y = (y & ~1) + oddEven;

               OFENTRY *srcrow = srcTbl + y;

               colsrc = (MTI_UINT32 *)(srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16));
#ifdef GO
               switch(x%6) {

	              case 0: // U and 1st Y

                     unpack = colsrc[0];
		             *coldst++ = (unpack>>2);
                     *coldst++ = (unpack>>12);

                     break;

	              case 1: // U and 2nd Y

                     unpack = colsrc[0];
		             *coldst++ = (unpack>>2);
                     unpack = colsrc[1];
                     *coldst++ = (unpack>>2);

                     break;

	              case 2: // U and 1st Y

                     unpack = colsrc[1];
                     *coldst++ = (unpack>>12);
                     *coldst++ = (unpack>>22);

                     break;

	             case 3: // U and 2nd Y

                     unpack = colsrc[1];
                     *coldst++ = (unpack>>12);
                     unpack = colsrc[2];
                     *coldst++ = (unpack>>12);

                     break;

	              case 4: // U and 1st Y

                     unpack = colsrc[2];
                     *coldst++ = (unpack>>22);
                     unpack = colsrc[3];
                     *coldst++ = (unpack>>2);

                     break;

	              case 5: // U and 2nd Y

                     unpack = colsrc[2];
                     *coldst++ = (unpack>>22);
                     unpack = colsrc[3];
                     *coldst++ = (unpack>>22);

                     break;
               }
#endif


               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;
               y = (y & ~1) + oddEven;

               srcrow = srcTbl + y;

               colsrc = (MTI_UINT32 *)(srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16));
#ifdef GO
               switch(x%6) {

	          case 0: // V and 1st Y

                     unpack = colsrc[0];
		     *coldst++ = (unpack>>22);
                     *coldst++ = (unpack>>12);

                     break;

	          case 1: // V and 2nd Y

                     unpack = colsrc[0];
		     *coldst++ = (unpack>>22);
                     unpack = colsrc[1];
                     *coldst++ = (unpack>>2);

                     break;

	          case 2: // V and 1st Y

                     unpack = colsrc[2];
                     *coldst++ = (unpack>>2);
                     unpack = colsrc[1];
                     *coldst++ = (unpack>>22);

                     break;

	          case 3: // V and 2nd Y

                     unpack = colsrc[2];
                     *coldst++ = (unpack>>2);
                     *coldst++ = (unpack>>12);

                     break;

	          case 4: // V and 1st Y

                     unpack = colsrc[3];
                     *coldst++ = (unpack>>12);
                     *coldst++ = (unpack>>2);

                     break;

	          case 5: // V and 2nd Y

                     unpack = colsrc[3];
                     *coldst++ = (unpack>>12);
                     *coldst++ = (unpack>>22);

                     break;
               }
#endif
               colX += hdelX;
               colY += hdelY;
            }

         }
         else {

            coldst += 2;

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               int x   = colX >> 16;
               int y   = colY >> 16;
               y = (y & ~1) + oddEven;

               OFENTRY *srcrow = srcTbl + y;

               colsrc = (MTI_UINT32 *)(srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16));
#ifdef GO
               switch(x%6) {

	          case 0: // V and 1st Y

                     unpack = colsrc[0];
		     *coldst++ = (unpack>>22);
                     *coldst++ = (unpack>>12);

                     break;

	          case 1: // V and 2nd Y

                     unpack = colsrc[0];
		     *coldst++ = (unpack>>22);
                     unpack = colsrc[1];
                     *coldst++ = (unpack>>2);

                     break;

	          case 2: // V and 1st Y

                     unpack = colsrc[2];
                     *coldst++ = (unpack>>2);
                     unpack = colsrc[1];
                     *coldst++ = (unpack>>22);

                     break;

	          case 3: // V and 2nd Y

                     unpack = colsrc[2];
                     *coldst++ = (unpack>>2);
                     *coldst++ = (unpack>>12);

                     break;

	          case 4: // V and 1st Y

                     unpack = colsrc[3];
                     *coldst++ = (unpack>>12);
                     *coldst++ = (unpack>>2);

                     break;

	          case 5: // V and 2nd Y

                     unpack = colsrc[3];
                     *coldst++ = (unpack>>12);
                     *coldst++ = (unpack>>22);

                     break;
               }
#endif
               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;
               y = (y & ~1) + oddEven;

               srcrow = srcTbl + y;

               colsrc = (MTI_UINT32 *)(srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16));
#ifdef GO
               switch(x%6) {

	          case 0: // U and 1st Y

                     unpack = colsrc[0];
		     *coldst++ = (unpack>>2);
                     *coldst++ = (unpack>>12);

                     break;

	          case 1: // U and 2nd Y

                     unpack = colsrc[0];
		     *coldst++ = (unpack>>2);
                     unpack = colsrc[1];
                     *coldst++ = (unpack>>2);

                     break;

	          case 2: // U and 1st Y

                     unpack = colsrc[1];
                     *coldst++ = (unpack>>12);
                     *coldst++ = (unpack>>22);

                     break;

	          case 3: // U and 2nd Y

                     unpack = colsrc[1];
                     *coldst++ = (unpack>>12);
                     unpack = colsrc[2];
                     *coldst++ = (unpack>>12);

                     break;

	          case 4: // U and 1st Y

                     unpack = colsrc[2];
                     *coldst++ = (unpack>>22);
                     unpack = colsrc[3];
                     *coldst++ = (unpack>>2);

                     break;

	          case 5: // U and 2nd Y

                     unpack = colsrc[2];
                     *coldst++ = (unpack>>22);
                     unpack = colsrc[3];
                     *coldst++ = (unpack>>22);

                     break;
               }
#endif
               colX += hdelX;
               colY += hdelY;

            }
         }
      }
      else {  // scan-line crosses in and out of source frame

         colX = rowX;
         colY = rowY;

         OFENTRY *dstrow = dstTbl + i;

         // ysrc -> pixel of src
          MTI_UINT32 *colsrc;

         // coldst -> pixel of dst
          MTI_UINT8 *coldst = dstbuf[dstrow->rowfld] + ((dstrow->rowoff + dstMinCol)>>1)*4;

         MTI_UINT32 phase = (dstMinCol&1); // 0 for U, 1 for V

          MTI_UINT32 unpack;

         // REWRITTEN TO ELIMINATE PHASE VARIABLE

         int j = 0;
         if (phase==0) {

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               int x   = colX >> 16;
               int y   = colY >> 16;
               y = (y & ~1) + oddEven;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

                  OFENTRY *srcrow = srcTbl + y;

                  colsrc = (MTI_UINT32 *)(srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16));

                  switch(x%6) {

	             case 0: // U and 1st Y

                        unpack = colsrc[0];
		        *coldst++ = (unpack>>2);
                        *coldst++ = (unpack>>12);

                        break;

	             case 1: // U and 2nd Y 

                        unpack = colsrc[0];
		        *coldst++ = (unpack>>2);
                        unpack = colsrc[1];
                        *coldst++ = (unpack>>2);  

                        break;

	             case 2: // U and 1st Y

                        unpack = colsrc[1];
                        *coldst++ = (unpack>>12);
                        *coldst++ = (unpack>>22);

                        break;

	             case 3: // U and 2nd Y

                        unpack = colsrc[1];
                        *coldst++ = (unpack>>12);
                        unpack = colsrc[2];
                        *coldst++ = (unpack>>12);

                        break;

	             case 4: // U and 1st Y

                        unpack = colsrc[2];
                        *coldst++ = (unpack>>22);
                        unpack = colsrc[3];
                        *coldst++ = (unpack>>2);

                        break;

	             case 5: // U and 2nd Y

                        unpack = colsrc[2];
                        *coldst++ = (unpack>>22);
                        unpack = colsrc[3];
                        *coldst++ = (unpack>>22);

                        break;
                  }
               }
               else { // copy the bgnd UYVY

                  *coldst++ = bgnd0;
                  *coldst++ = bgnd1;

               }

               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;
               y = (y & ~1) + oddEven;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

                  OFENTRY *srcrow = srcTbl + y;

                  colsrc = (MTI_UINT32 *)(srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16));

                  switch(x%6) {

	             case 0: // V and 1st Y

                        unpack = colsrc[0];
		        *coldst++ = (unpack>>22);
                        *coldst++ = (unpack>>12);

                        break;

	             case 1: // V and 2nd Y 

                        unpack = colsrc[0];
		        *coldst++ = (unpack>>22);
                        unpack = colsrc[1];
                        *coldst++ = (unpack>>2);  

                        break;

	             case 2: // V and 1st Y

                        unpack = colsrc[2];
                        *coldst++ = (unpack>>2);
                        unpack = colsrc[1];
                        *coldst++ = (unpack>>22);

                        break;

	             case 3: // V and 2nd Y

                        unpack = colsrc[2];
                        *coldst++ = (unpack>>2);
                        *coldst++ = (unpack>>12);

                        break;

	             case 4: // V and 1st Y

                        unpack = colsrc[3];
                        *coldst++ = (unpack>>12);
                        *coldst++ = (unpack>>2);

                        break;

	             case 5: // V and 2nd Y

                        unpack = colsrc[3];
                        *coldst++ = (unpack>>12);
                        *coldst++ = (unpack>>22);

                        break;
                  }
               }
               else { // copy the bgnd UYVY

                  *coldst++ = bgnd2;
                  *coldst++ = bgnd3;

               }

               colX += hdelX;
               colY += hdelY;

            }

         }
         else {

            coldst += 2;

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               int x   = colX >> 16;
               int y   = colY >> 16;
               y = (y & ~1) + oddEven;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

                  OFENTRY *srcrow = srcTbl + y;

                  colsrc = (MTI_UINT32 *)(srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16));

                  switch(x%6) {

	             case 0: // V and 1st Y

                        unpack = colsrc[0];
		        *coldst++ = (unpack>>22);
                        *coldst++ = (unpack>>12);

                        break;

	             case 1: // V and 2nd Y 

                        unpack = colsrc[0];
		        *coldst++ = (unpack>>22);
                        unpack = colsrc[1];
                        *coldst++ = (unpack>>2);  

                        break;

	             case 2: // V and 1st Y

                        unpack = colsrc[2];
                        *coldst++ = (unpack>>2);
                        unpack = colsrc[1];
                        *coldst++ = (unpack>>22);

                        break;

	             case 3: // V and 2nd Y

                        unpack = colsrc[2];
                        *coldst++ = (unpack>>2);
                        *coldst++ = (unpack>>12);

                        break;

	             case 4: // V and 1st Y

                        unpack = colsrc[3];
                        *coldst++ = (unpack>>12);
                        *coldst++ = (unpack>>2);

                        break;

	             case 5: // V and 2nd Y

                        unpack = colsrc[3];
                        *coldst++ = (unpack>>12);
                        *coldst++ = (unpack>>22);

                        break;
                  }
               }
               else { // copy the bgnd UYVY

                  *coldst++ = bgnd2;
                  *coldst++ = bgnd3;

               }

               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;
               y = (y & ~1) + oddEven;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

                  OFENTRY *srcrow = srcTbl + y;

                  colsrc = (MTI_UINT32 *)(srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16));

                  switch(x%6) {

	             case 0: // U and 1st Y

                        unpack = colsrc[0];
		        *coldst++ = (unpack>>2);
                        *coldst++ = (unpack>>12);

                        break;

	             case 1: // U and 2nd Y 

                        unpack = colsrc[0];
		        *coldst++ = (unpack>>2);
                        unpack = colsrc[1];
                        *coldst++ = (unpack>>2);  

                        break;

	             case 2: // U and 1st Y

                        unpack = colsrc[1];
                        *coldst++ = (unpack>>12);
                        *coldst++ = (unpack>>22);

                        break;

	             case 3: // U and 2nd Y

                        unpack = colsrc[1];
                        *coldst++ = (unpack>>12);
                        unpack = colsrc[2];
                        *coldst++ = (unpack>>12);

                        break;

	             case 4: // U and 1st Y

                        unpack = colsrc[2];
                        *coldst++ = (unpack>>22);
                        unpack = colsrc[3];
                        *coldst++ = (unpack>>2);

                        break;

	             case 5: // U and 2nd Y

                        unpack = colsrc[2];
                        *coldst++ = (unpack>>22);
                        unpack = colsrc[3];
                        *coldst++ = (unpack>>22);

                        break;
                  }
               }
               else { // copy the bgnd UYVY

                  *coldst++ = bgnd0;
                  *coldst++ = bgnd1;

               }

               colX += hdelX;
               colY += hdelY;

            }

         }

      }

      // advance to the next row

      rowX += vdelX;
      rowY += vdelY;

      endX += vdelX;
      endY += vdelY;

   }

 }
 else { // dst line from closest line

   for (int i=beg;i<beg+lins;i++) {

      int Xbeg = rowX >> 16;
      int Ybeg = rowY >> 16;
      int Xend = endX >> 16;
      int Yend = endY >> 16;

      if ((Xbeg>=0)&&(Xbeg<=wlim)&&(Xend>=0)&&(Xend<=wlim)&&
          (Ybeg>=0)&&(Ybeg<=hlim)&&(Yend>=0)&&(Yend<=hlim)) { // scan-line is inside

         colX = rowX;
         colY = rowY;

         OFENTRY *dstrow = dstTbl + i;

         // ysrc -> pixel of src

          MTI_UINT32 *colsrc;

         // coldst -> pixel of dst
          MTI_UINT8 *coldst = dstbuf[dstrow->rowfld] + ((dstrow->rowoff + dstMinCol)>>1)*4;

         MTI_UINT32 phase = (dstMinCol&1); // 0 for U, 1 for V

          MTI_UINT32 unpack;

         // REWRITTEN TO ELIMINATE PHASE VARIABLE

         int j = 0;
         if (phase==0) {

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               int x   = colX >> 16;
               int y   = colY >> 16;

               OFENTRY *srcrow = srcTbl + y;

               colsrc = (MTI_UINT32 *)(srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16));

#if MTI_ASM_X86_INTEL
               _asm {
                        mov     eax,x
                        xor     edx,edx
                        mov     ecx,6
                        div     ecx
                        mov     esi,colsrc
                        mov     edi,coldst

                        cmp     dl,3
                        jge     $U0GE3

                        cmp     dl,1
                        je      $U0EQ1
                        jg      $U0EQ2

               $U0EQ0:  mov     eax,[esi]
                        ror     eax,2
                        stosb
                        ror     eax,10
                        stosb
                        jmp     SHORT $U0ENDU

               $U0EQ1:  mov     eax,[esi]
                        ror     eax,2
                        stosb
                        mov     eax,[esi+4]
                        ror     eax,2
                        stosb
                        jmp     SHORT $U0ENDU

               $U0EQ2:  mov     eax,[esi+4]
                        ror     eax,12
                        stosb
                        ror     eax,10
                        stosb
                        jmp     SHORT $U0ENDU

               $U0GE3:  cmp     dl,4
                        je      $U0EQ4
                        jg      $U0EQ5

               $U0EQ3:  mov     eax,[esi+4]
                        ror     eax,12
                        stosb
                        mov     eax,[esi+8]
                        ror     eax,12
                        stosb
                        jmp     SHORT $U0ENDU

               $U0EQ4:  mov     eax,[esi+8]
                        ror     eax,22
                        stosb
                        mov     eax,[esi+12]
                        ror     eax,2
                        stosb
                        jmp     SHORT $U0ENDU

               $U0EQ5:  mov     eax,[esi+8]
                        ror     eax,22
                        stosb
                        mov     eax,[esi+12]
                        ror     eax,22
                        stosb

               $U0ENDU: mov     coldst,edi

               }

#else
               switch(x%6) {

	          case 0: // U and 1st Y

                     unpack = colsrc[0];
		     *coldst++ = (unpack>>2);
                     *coldst++ = (unpack>>12);

                     break;

	          case 1: // U and 2nd Y

                     unpack = colsrc[0];
		     *coldst++ = (unpack>>2);
                     unpack = colsrc[1];
                     *coldst++ = (unpack>>2);

                     break;

	          case 2: // U and 1st Y

                     unpack = colsrc[1];
                     *coldst++ = (unpack>>12);
                     *coldst++ = (unpack>>22);

                     break;

	          case 3: // U and 2nd Y

                     unpack = colsrc[1];
                     *coldst++ = (unpack>>12);
                     unpack = colsrc[2];
                     *coldst++ = (unpack>>12);

                     break;

	          case 4: // U and 1st Y

                     unpack = colsrc[2];
                     *coldst++ = (unpack>>22);
                     unpack = colsrc[3];
                     *coldst++ = (unpack>>2);

                     break;

	          case 5: // U and 2nd Y

                     unpack = colsrc[2];
                     *coldst++ = (unpack>>22);
                     unpack = colsrc[3];
                     *coldst++ = (unpack>>22);

                     break;
               }

#endif
               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;

               srcrow = srcTbl + y;

               colsrc = (MTI_UINT32 *)(srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16));

#if MTI_ASM_X86_INTEL
               _asm {
                        mov     eax,x
                        xor     edx,edx
                        mov     ecx,6
                        div     ecx
                        mov     esi,colsrc
                        mov     edi,coldst

                        cmp     dl,3
                        jge     $V0GE3

                        cmp     dl,1
                        je      $V0EQ1
                        jg      $V0EQ2

               $V0EQ0:  mov     eax,[esi]
                        ror     eax,22
                        stosb
                        rol     eax,10
                        stosb
                        jmp     SHORT $V0ENDU

               $V0EQ1:  mov     eax,[esi]
                        ror     eax,22
                        stosb
                        mov     eax,[esi+4]
                        ror     eax,2
                        stosb
                        jmp     SHORT $V0ENDU

               $V0EQ2:  mov     eax,[esi+8]
                        ror     eax,2
                        stosb
                        mov     eax,[esi+4]
                        ror     eax,22
                        stosb
                        jmp     SHORT $V0ENDU

               $V0GE3:  cmp     dl,4
                        je      $V0EQ4
                        jg      $V0EQ5

               $V0EQ3:  mov     eax,[esi+8]
                        ror     eax,2
                        stosb
                        ror     eax,10
                        stosb
                        jmp     SHORT $V0ENDU

               $V0EQ4:  mov     eax,[esi+12]
                        ror     eax,12
                        stosb
                        rol     eax,10
                        stosb
                        jmp     SHORT $V0ENDU

               $V0EQ5:  mov     eax,[esi+12]
                        ror     eax,12
                        stosb
                        ror     eax,10
                        stosb

               $V0ENDU: mov     coldst,edi

               }
#else
               switch(x%6) {

	          case 0: // V and 1st Y

                     unpack = colsrc[0];
		     *coldst++ = (unpack>>22);
                     *coldst++ = (unpack>>12);

                     break;

	          case 1: // V and 2nd Y

                     unpack = colsrc[0];
		     *coldst++ = (unpack>>22);
                     unpack = colsrc[1];
                     *coldst++ = (unpack>>2);

                     break;

	          case 2: // V and 1st Y

                     unpack = colsrc[2];
                     *coldst++ = (unpack>>2);
                     unpack = colsrc[1];
                     *coldst++ = (unpack>>22);

                     break;

	          case 3: // V and 2nd Y

                     unpack = colsrc[2];
                     *coldst++ = (unpack>>2);
                     *coldst++ = (unpack>>12);

                     break;

	          case 4: // V and 1st Y

                     unpack = colsrc[3];
                     *coldst++ = (unpack>>12);
                     *coldst++ = (unpack>>2);

                     break;

	          case 5: // V and 2nd Y

                     unpack = colsrc[3];
                     *coldst++ = (unpack>>12);
                     *coldst++ = (unpack>>22);

                     break;
               }
#endif
               colX += hdelX;
               colY += hdelY;
            }

         }
         else {

            coldst += 2;

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               int x   = colX >> 16;
               int y   = colY >> 16;

               OFENTRY *srcrow = srcTbl + y;

               colsrc = (MTI_UINT32 *)(srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16));
#if MTI_ASM_X86_INTEL
               _asm {
                        mov     eax,x
                        xor     edx,edx
                        mov     ecx,6
                        div     ecx
                        mov     esi,colsrc
                        mov     edi,coldst

                        cmp     dl,3
                        jge     $V1GE3

                        cmp     dl,1
                        je      $V1EQ1
                        jg      $V1EQ2

               $V1EQ0:  mov     eax,[esi]
                        ror     eax,22
                        stosb
                        rol     eax,10
                        stosb
                        jmp     SHORT $V1ENDU

               $V1EQ1:  mov     eax,[esi]
                        ror     eax,22
                        stosb
                        mov     eax,[esi+4]
                        ror     eax,2
                        stosb
                        jmp     SHORT $V1ENDU

               $V1EQ2:  mov     eax,[esi+8]
                        ror     eax,2
                        stosb
                        mov     eax,[esi+4]
                        ror     eax,22
                        stosb
                        jmp     SHORT $V1ENDU

               $V1GE3:  cmp     dl,4
                        je      $V1EQ4
                        jg      $V1EQ5

               $V1EQ3:  mov     eax,[esi+8]
                        ror     eax,2
                        stosb
                        ror     eax,10
                        stosb
                        jmp     SHORT $V1ENDU

               $V1EQ4:  mov     eax,[esi+12]
                        ror     eax,12
                        stosb
                        rol     eax,10
                        stosb
                        jmp     SHORT $V1ENDU

               $V1EQ5:  mov     eax,[esi+12]
                        ror     eax,12
                        stosb
                        ror     eax,10
                        stosb

               $V1ENDU: mov     coldst,edi

               }
#else
               switch(x%6) {

	          case 0: // V and 1st Y

                     unpack = colsrc[0];
		     *coldst++ = (unpack>>22);
                     *coldst++ = (unpack>>12);

                     break;

	          case 1: // V and 2nd Y

                     unpack = colsrc[0];
		     *coldst++ = (unpack>>22);
                     unpack = colsrc[1];
                     *coldst++ = (unpack>>2);

                     break;

	          case 2: // V and 1st Y

                     unpack = colsrc[2];
                     *coldst++ = (unpack>>2);
                     unpack = colsrc[1];
                     *coldst++ = (unpack>>22);

                     break;

	          case 3: // V and 2nd Y

                     unpack = colsrc[2];
                     *coldst++ = (unpack>>2);
                     *coldst++ = (unpack>>12);

                     break;

	          case 4: // V and 1st Y

                     unpack = colsrc[3];
                     *coldst++ = (unpack>>12);
                     *coldst++ = (unpack>>2);

                     break;

	          case 5: // V and 2nd Y

                     unpack = colsrc[3];
                     *coldst++ = (unpack>>12);
                     *coldst++ = (unpack>>22);

                     break;
               }
#endif
               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;

               srcrow = srcTbl + y;

               colsrc = (MTI_UINT32 *)(srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16));
#if MTI_ASM_X86_INTEL
               _asm {
                        mov     eax,x
                        xor     edx,edx
                        mov     ecx,6
                        div     ecx
                        mov     esi,colsrc
                        mov     edi,coldst

                        cmp     dl,3
                        jge     $U1GE3

                        cmp     dl,1
                        je      $U1EQ1
                        jg      $U1EQ2

               $U1EQ0:  mov     eax,[esi]
                        ror     eax,2
                        stosb
                        ror     eax,10
                        stosb
                        jmp     SHORT $U1ENDU

               $U1EQ1:  mov     eax,[esi]
                        ror     eax,2
                        stosb
                        mov     eax,[esi+4]
                        ror     eax,2
                        stosb
                        jmp     SHORT $U1ENDU

               $U1EQ2:  mov     eax,[esi+4]
                        ror     eax,12
                        stosb
                        ror     eax,10
                        stosb
                        jmp     SHORT $U1ENDU

               $U1GE3:  cmp     dl,4
                        je      $U1EQ4
                        jg      $U1EQ5

               $U1EQ3:  mov     eax,[esi+4]
                        ror     eax,12
                        stosb
                        mov     eax,[esi+8]
                        ror     eax,12
                        stosb
                        jmp     SHORT $U1ENDU

               $U1EQ4:  mov     eax,[esi+8]
                        ror     eax,22
                        stosb
                        mov     eax,[esi+12]
                        ror     eax,2
                        stosb
                        jmp     SHORT $U1ENDU

               $U1EQ5:  mov     eax,[esi+8]
                        ror     eax,22
                        stosb
                        mov     eax,[esi+12]
                        ror     eax,22
                        stosb

               $U1ENDU: mov     coldst,edi

               }
#else
               switch(x%6) {

	          case 0: // U and 1st Y

                     unpack = colsrc[0];
		     *coldst++ = (unpack>>2);
                     *coldst++ = (unpack>>12);

                     break;

	          case 1: // U and 2nd Y

                     unpack = colsrc[0];
		     *coldst++ = (unpack>>2);
                     unpack = colsrc[1];
                     *coldst++ = (unpack>>2);

                     break;

	          case 2: // U and 1st Y

                     unpack = colsrc[1];
                     *coldst++ = (unpack>>12);
                     *coldst++ = (unpack>>22);

                     break;

	          case 3: // U and 2nd Y

                     unpack = colsrc[1];
                     *coldst++ = (unpack>>12);
                     unpack = colsrc[2];
                     *coldst++ = (unpack>>12);

                     break;

	          case 4: // U and 1st Y

                     unpack = colsrc[2];
                     *coldst++ = (unpack>>22);
                     unpack = colsrc[3];
                     *coldst++ = (unpack>>2);

                     break;

	          case 5: // U and 2nd Y

                     unpack = colsrc[2];
                     *coldst++ = (unpack>>22);
                     unpack = colsrc[3];
                     *coldst++ = (unpack>>22);

                     break;
               }
#endif
               colX += hdelX;
               colY += hdelY;
            }

         }

      }
      else {  // scan-line crosses in and out of source frame

         colX = rowX;
         colY = rowY;

         OFENTRY *dstrow = dstTbl + i;

         // ysrc -> pixel of src
          MTI_UINT32 *colsrc;

         // coldst -> pixel of dst
          MTI_UINT8 *coldst = dstbuf[dstrow->rowfld] + ((dstrow->rowoff + dstMinCol)>>1)*4;

         MTI_UINT32 phase = (dstMinCol&1); // 0 for U, 1 for V

          MTI_UINT32 unpack;

         // REWRITTEN TO ELIMINATE PHASE VARIABLE

         int j = 0;
         if (phase==0) {

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               int x   = colX >> 16;
               int y   = colY >> 16;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

                  OFENTRY *srcrow = srcTbl + y;

                  colsrc = (MTI_UINT32 *)(srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16));

                  switch(x%6) {

	             case 0: // U and 1st Y

                        unpack = colsrc[0];
		        *coldst++ = (unpack>>2);
                        *coldst++ = (unpack>>12);

                        break;

	             case 1: // U and 2nd Y 

                        unpack = colsrc[0];
		        *coldst++ = (unpack>>2);
                        unpack = colsrc[1];
                        *coldst++ = (unpack>>2);  

                        break;

	             case 2: // U and 1st Y

                        unpack = colsrc[1];
                        *coldst++ = (unpack>>12);
                        *coldst++ = (unpack>>22);

                        break;

	             case 3: // U and 2nd Y

                        unpack = colsrc[1];
                        *coldst++ = (unpack>>12);
                        unpack = colsrc[2];
                        *coldst++ = (unpack>>12);

                        break;

	             case 4: // U and 1st Y

                        unpack = colsrc[2];
                        *coldst++ = (unpack>>22);
                        unpack = colsrc[3];
                        *coldst++ = (unpack>>2);

                        break;

	             case 5: // U and 2nd Y

                        unpack = colsrc[2];
                        *coldst++ = (unpack>>22);
                        unpack = colsrc[3];
                        *coldst++ = (unpack>>22);

                        break;
                  }
               }
               else { // copy the bgnd UYVY

                  *coldst++ = bgnd0;
                  *coldst++ = bgnd1;
               }

               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

                  OFENTRY *srcrow = srcTbl + y;

                  colsrc = (MTI_UINT32 *)(srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16));

                  switch(x%6) {

	             case 0: // V and 1st Y

                        unpack = colsrc[0];
		        *coldst++ = (unpack>>22);
                        *coldst++ = (unpack>>12);

                        break;

	             case 1: // V and 2nd Y 

                        unpack = colsrc[0];
		        *coldst++ = (unpack>>22);
                        unpack = colsrc[1];
                        *coldst++ = (unpack>>2);  

                        break;

	             case 2: // V and 1st Y

                        unpack = colsrc[2];
                        *coldst++ = (unpack>>2);
                        unpack = colsrc[1];
                        *coldst++ = (unpack>>22);

                        break;

	             case 3: // V and 2nd Y

                        unpack = colsrc[2];
                        *coldst++ = (unpack>>2);
                        *coldst++ = (unpack>>12);

                        break;

	             case 4: // V and 1st Y

                        unpack = colsrc[3];
                        *coldst++ = (unpack>>12);
                        *coldst++ = (unpack>>2);

                        break;

	             case 5: // V and 2nd Y

                        unpack = colsrc[3];
                        *coldst++ = (unpack>>12);
                        *coldst++ = (unpack>>22);

                        break;
                  }
               }
               else { // copy the bgnd UYVY

                  *coldst++ = bgnd2;
                  *coldst++ = bgnd3;
               }

               colX += hdelX;
               colY += hdelY;

            }

         }
         else {

            coldst += 2;

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               int x   = colX >> 16;
               int y   = colY >> 16;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

                  OFENTRY *srcrow = srcTbl + y;

                  colsrc = (MTI_UINT32 *)(srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16));

                  switch(x%6) {

	             case 0: // V and 1st Y

                        unpack = colsrc[0];
		        *coldst++ = (unpack>>22);
                        *coldst++ = (unpack>>12);

                        break;

	             case 1: // V and 2nd Y 

                        unpack = colsrc[0];
		        *coldst++ = (unpack>>22);
                        unpack = colsrc[1];
                        *coldst++ = (unpack>>2);  

                        break;

	             case 2: // V and 1st Y

                        unpack = colsrc[2];
                        *coldst++ = (unpack>>2);
                        unpack = colsrc[1];
                        *coldst++ = (unpack>>22);

                        break;

	             case 3: // V and 2nd Y

                        unpack = colsrc[2];
                        *coldst++ = (unpack>>2);
                        *coldst++ = (unpack>>12);

                        break;

	             case 4: // V and 1st Y

                        unpack = colsrc[3];
                        *coldst++ = (unpack>>12);
                        *coldst++ = (unpack>>2);

                        break;

	             case 5: // V and 2nd Y

                        unpack = colsrc[3];
                        *coldst++ = (unpack>>12);
                        *coldst++ = (unpack>>22);

                        break;
                  }
               }
               else { // copy the bgnd UYVY

                  *coldst++ = bgnd2;
                  *coldst++ = bgnd3;

               }

               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

                  OFENTRY *srcrow = srcTbl + y;

                  colsrc = (MTI_UINT32 *)(srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16));

                  switch(x%6) {

	             case 0: // U and 1st Y

                        unpack = colsrc[0];
		        *coldst++ = (unpack>>2);
                        *coldst++ = (unpack>>12);

                        break;

	             case 1: // U and 2nd Y 

                        unpack = colsrc[0];
		        *coldst++ = (unpack>>2);
                        unpack = colsrc[1];
                        *coldst++ = (unpack>>2);  

                        break;

	             case 2: // U and 1st Y

                        unpack = colsrc[1];
                        *coldst++ = (unpack>>12);
                        *coldst++ = (unpack>>22);

                        break;

	             case 3: // U and 2nd Y

                        unpack = colsrc[1];
                        *coldst++ = (unpack>>12);
                        unpack = colsrc[2];
                        *coldst++ = (unpack>>12);

                        break;

	             case 4: // U and 1st Y

                        unpack = colsrc[2];
                        *coldst++ = (unpack>>22);
                        unpack = colsrc[3];
                        *coldst++ = (unpack>>2);

                        break;

	             case 5: // U and 2nd Y

                        unpack = colsrc[2];
                        *coldst++ = (unpack>>22);
                        unpack = colsrc[3];
                        *coldst++ = (unpack>>22);

                        break;
                  }
               }
               else { // copy the bgnd UYVY

                  *coldst++ = bgnd0;
                  *coldst++ = bgnd1;
               }

               colX += hdelX;
               colY += hdelY;

            }

         }

      }

      // advance to the next row

      rowX += vdelX;
      rowY += vdelY;

      endX += vdelX;
      endY += vdelY;

   }

 }

}

#else // non-WINDOWS
void stripeDVS10to8(CPanAndScan *vp, int iJob)
{
 MTI_INT32 vdelX = vp->vdelX;
 MTI_INT32 vdelY = vp->vdelY;
 MTI_INT32 hdelX = vp->hdelX;
 MTI_INT32 hdelY = vp->hdelY;

 MTI_UINT8 *srcbuf[2];
 srcbuf[0] = (MTI_UINT8 *)vp->srcbuf[0];
 srcbuf[1] = (MTI_UINT8 *)vp->srcbuf[1];
  OFENTRY *srcTbl = vp->srcTbl;

 MTI_UINT8 *dstbuf[2];
 dstbuf[0] = (MTI_UINT8 *)vp->dstbuf[0];
 dstbuf[1] = (MTI_UINT8 *)vp->dstbuf[1];
  OFENTRY *dstTbl = vp->dstTbl;
  int dstMinCol  = vp->dstMinCol;
  int dstWinWdth = vp->dstWinWdth;

 MTI_UINT8 bgnd0 = vp->bgnd8[0];
 MTI_UINT8 bgnd1 = vp->bgnd8[1];
 MTI_UINT8 bgnd2 = vp->bgnd8[2];
 MTI_UINT8 bgnd3 = vp->bgnd8[3];

  int wlim = vp->wsrc - 1;
  int hlim = vp->hsrc - 1;

 // calculate the stripe lines
 int lins = vp->dstWinHght / vp->nStripes;
 MTI_INT32 beg = iJob*lins;
 if (iJob==vp->nStripes-1) {
    lins = vp->dstWinHght - beg;
 }

 // the beg pt of the scan
 MTI_INT32 rowX = vp->begX + beg * vdelX;
 MTI_INT32 rowY = vp->begY + beg * vdelY;

 // the cur pt of the scan
 MTI_INT32 colX;
 MTI_INT32 colY;

 // the end pt of the scan
 MTI_INT32 endX = rowX + (dstWinWdth-1)* hdelX;
 MTI_INT32 endY = rowY + (dstWinWdth-1)* hdelY;

 // 9/25/01 - adj pos of first line of dst window 
 beg += vp->dstMinRow;

 if (vp->parLin) { // use closest line of correct parity

   for (int i=beg;i<beg+lins;i++) {

      // get parity of dst line
      int oddEven = i&1;

      int Xbeg = rowX >> 16;
      int Ybeg = rowY >> 16;
      int Xend = endX >> 16;
      int Yend = endY >> 16;

      if ((Xbeg>=0)&&(Xbeg<=wlim)&&(Xend>=0)&&(Xend<=wlim)&&
          (Ybeg>=0)&&(Ybeg<=hlim)&&(Yend>=0)&&(Yend<=hlim)) { // scan-line is inside

         colX = rowX;
         colY = rowY;

          OFENTRY *dstrow = dstTbl + i;

         // ysrc -> pixel of src
          MTI_UINT8 *colsrc;

         // coldst -> pixel of dst
          MTI_UINT8 *coldst = dstbuf[dstrow->rowfld] + ((dstrow->rowoff + dstMinCol)>>1)*4;

          MTI_UINT32 phase = (dstMinCol&1); // 0 for U, 1 for V

          MTI_UINT32 unpack;

         // REWRITTEN TO ELIMINATE PHASE VARIABLE

         int j = 0;
         if (phase==0) {

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

                int x   = colX >> 16;
                int y   = colY >> 16;
               y = (y & ~1) + oddEven;

                OFENTRY *srcrow = srcTbl + y;

               colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16);

               switch(x%6) {

	          case 0: // U and 1st Y

                     unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		     *coldst++ = (unpack>>2);
                     *coldst++ = (unpack>>12);

                     break;

	          case 1: // U and 2nd Y 

                     unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		     *coldst++ = (unpack>>2);
                     unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                     *coldst++ = (unpack>>2);  

                     break;

	          case 2: // U and 1st Y

                     unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                     *coldst++ = (unpack>>12);
                     *coldst++ = (unpack>>22);

                     break;

	          case 3: // U and 2nd Y

                     unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                     *coldst++ = (unpack>>12);
                     unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                     *coldst++ = (unpack>>12);

                     break;

	          case 4: // U and 1st Y

                     unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                     *coldst++ = (unpack>>22);
                     unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12];
                     *coldst++ = (unpack>>2);

                     break;

	          case 5: // U and 2nd Y

                     unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                     *coldst++ = (unpack>>22);
                     unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12]; 
                     *coldst++ = (unpack>>22);

                     break;
               }


               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;
               y = (y & ~1) + oddEven;

               srcrow = srcTbl + y;

               colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16);

               switch(x%6) {

	          case 0: // V and 1st Y

                     unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		     *coldst++ = (unpack>>22);
                     *coldst++ = (unpack>>12);

                     break;

	          case 1: // V and 2nd Y 

                     unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		     *coldst++ = (unpack>>22);
                     unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                     *coldst++ = (unpack>>2);  

                     break;

	          case 2: // V and 1st Y

                     unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                     *coldst++ = (unpack>>2);
                     unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                     *coldst++ = (unpack>>22);

                     break;

	          case 3: // V and 2nd Y

                     unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                     *coldst++ = (unpack>>2);
                     *coldst++ = (unpack>>12);

                     break;

	          case 4: // V and 1st Y

                     unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12];
                     *coldst++ = (unpack>>12);
                     *coldst++ = (unpack>>2);

                     break;

	          case 5: // V and 2nd Y

                     unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12];
                     *coldst++ = (unpack>>12);
                     *coldst++ = (unpack>>22);

                     break;
               }

               colX += hdelX;
               colY += hdelY;
            }

         }
         else {

            coldst += 2;

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

                int x   = colX >> 16;
                int y   = colY >> 16;
               y = (y & ~1) + oddEven;

                OFENTRY *srcrow = srcTbl + y;

               colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16);

               switch(x%6) {

	          case 0: // V and 1st Y

                     unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		     *coldst++ = (unpack>>22);
                     *coldst++ = (unpack>>12);

                     break;

	          case 1: // V and 2nd Y

                     unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		     *coldst++ = (unpack>>22);
                     unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                     *coldst++ = (unpack>>2);  

                     break;

	          case 2: // V and 1st Y

                     unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                     *coldst++ = (unpack>>2);
                     unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                     *coldst++ = (unpack>>22);

                     break;

	          case 3: // V and 2nd Y

                     unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                     *coldst++ = (unpack>>2);
                     *coldst++ = (unpack>>12);

                     break;

	          case 4: // V and 1st Y

                     unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12];
                     *coldst++ = (unpack>>12);
                     *coldst++ = (unpack>>2);

                     break;

	          case 5: // V and 2nd Y

                     unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12];
                     *coldst++ = (unpack>>12);
                     *coldst++ = (unpack>>22);

                     break;
               }

               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;
               y = (y & ~1) + oddEven;

               srcrow = srcTbl + y;

               colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)>>1)*4);

               *coldst++ = *colsrc;
               if (x&1)
                  *coldst++ = *(colsrc+3);
               else
                  *coldst++ = *(colsrc+1);

               colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16);

               switch(x%6) {

	          case 0: // U and 1st Y

                     unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		     *coldst++ = (unpack>>2);
                     *coldst++ = (unpack>>12);

                     break;

	          case 1: // U and 2nd Y 

                     unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		     *coldst++ = (unpack>>2);
                     unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                     *coldst++ = (unpack>>2);  

                     break;

	          case 2: // U and 1st Y

                     unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                     *coldst++ = (unpack>>12);
                     *coldst++ = (unpack>>22);

                     break;

	          case 3: // U and 2nd Y

                     unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                     *coldst++ = (unpack>>12);
                     unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                     *coldst++ = (unpack>>12);

                     break;

	          case 4: // U and 1st Y

                     unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                     *coldst++ = (unpack>>22);
                     unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12];
                     *coldst++ = (unpack>>2);

                     break;

	          case 5: // U and 2nd Y

                     unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                     *coldst++ = (unpack>>22);
                     unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12]; 
                     *coldst++ = (unpack>>22);

                     break;
               }

               colX += hdelX;
               colY += hdelY;

            }

         }

      }
      else {  // scan-line crosses in and out of source frame

         colX = rowX;
         colY = rowY;

          OFENTRY *dstrow = dstTbl + i;

         // ysrc -> pixel of src
          MTI_UINT8 *colsrc;

         // coldst -> pixel of dst
          MTI_UINT8 *coldst = dstbuf[dstrow->rowfld] + ((dstrow->rowoff + dstMinCol)>>1)*4;

          MTI_UINT32 phase = (dstMinCol&1); // 0 for U, 1 for V

          MTI_UINT32 unpack;

         // REWRITTEN TO ELIMINATE PHASE VARIABLE

         int j = 0;
         if (phase==0) {

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

                int x   = colX >> 16;
                int y   = colY >> 16;
               y = (y & ~1) + oddEven;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

                   OFENTRY *srcrow = srcTbl + y;

                  colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16);

                  switch(x%6) {

	             case 0: // U and 1st Y

                        unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		        *coldst++ = (unpack>>2);
                        *coldst++ = (unpack>>12);

                        break;

	             case 1: // U and 2nd Y 

                        unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		        *coldst++ = (unpack>>2);
                        unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                        *coldst++ = (unpack>>2);  

                        break;

	             case 2: // U and 1st Y

                        unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                        *coldst++ = (unpack>>12);
                        *coldst++ = (unpack>>22);

                        break;

	             case 3: // U and 2nd Y

                        unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                        *coldst++ = (unpack>>12);
                        unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                        *coldst++ = (unpack>>12);

                        break;

	             case 4: // U and 1st Y

                        unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                        *coldst++ = (unpack>>22);
                        unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12];
                        *coldst++ = (unpack>>2);

                        break;

	             case 5: // U and 2nd Y

                        unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                        *coldst++ = (unpack>>22);
                        unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12];
                        *coldst++ = (unpack>>22);

                        break;
                  }
               }
               else { // copy the bgnd UYVY

                  *coldst++ = bgnd0;
                  *coldst++ = bgnd1;

               }

               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;
               y = (y & ~1) + oddEven;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

                   OFENTRY *srcrow = srcTbl + y;

                  colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16);

                  switch(x%6) {

	             case 0: // V and 1st Y

                        unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		        *coldst++ = (unpack>>22);
                        *coldst++ = (unpack>>12);

                        break;

	             case 1: // V and 2nd Y 

                        unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		        *coldst++ = (unpack>>22);
                        unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                        *coldst++ = (unpack>>2);  

                        break;

	             case 2: // V and 1st Y

                        unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                        *coldst++ = (unpack>>2);
                        unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                        *coldst++ = (unpack>>22);

                        break;

	             case 3: // V and 2nd Y

                        unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                        *coldst++ = (unpack>>2);
                        *coldst++ = (unpack>>12);

                        break;

	             case 4: // V and 1st Y

                        unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12];
                        *coldst++ = (unpack>>12);
                        *coldst++ = (unpack>>2);

                        break;

	             case 5: // V and 2nd Y

                        unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12];
                        *coldst++ = (unpack>>12);
                        *coldst++ = (unpack>>22);

                        break;
                  }
               }
               else { // copy the bgnd UYVY

                  *coldst++ = bgnd2;
                  *coldst++ = bgnd3;

               }

               colX += hdelX;
               colY += hdelY;

            }

         }
         else {

            coldst += 2;

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

                int x   = colX >> 16;
                int y   = colY >> 16;
               y = (y & ~1) + oddEven;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

                   OFENTRY *srcrow = srcTbl + y;

                  colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16);

                  switch(x%6) {

	             case 0: // V and 1st Y

                        unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		        *coldst++ = (unpack>>22);
                        *coldst++ = (unpack>>12);

                        break;

	             case 1: // V and 2nd Y 

                        unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		        *coldst++ = (unpack>>22);
                        unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                        *coldst++ = (unpack>>2);  

                        break;

	             case 2: // V and 1st Y

                        unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                        *coldst++ = (unpack>>2);
                        unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                        *coldst++ = (unpack>>22);

                        break;

	             case 3: // V and 2nd Y

                        unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                        *coldst++ = (unpack>>2);
                        *coldst++ = (unpack>>12);

                        break;

	             case 4: // V and 1st Y

                        unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12];
                        *coldst++ = (unpack>>12);
                        *coldst++ = (unpack>>2);

                        break;

	             case 5: // V and 2nd Y

                        unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12];
                        *coldst++ = (unpack>>12);
                        *coldst++ = (unpack>>22);

                        break;
                  }
               }
               else { // copy the bgnd UYVY

                  *coldst++ = bgnd2;
                  *coldst++ = bgnd3;

               }

               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;
               y = (y & ~1) + oddEven;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

                   OFENTRY *srcrow = srcTbl + y;

                  colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16);

                  switch(x%6) {

	             case 0: // U and 1st Y

                        unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		        *coldst++ = (unpack>>2);
                        *coldst++ = (unpack>>12);

                        break;

	             case 1: // U and 2nd Y 

                        unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		        *coldst++ = (unpack>>2);
                        unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                        *coldst++ = (unpack>>2);  

                        break;

	             case 2: // U and 1st Y

                        unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                        *coldst++ = (unpack>>12);
                        *coldst++ = (unpack>>22);

                        break;

	             case 3: // U and 2nd Y

                        unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                        *coldst++ = (unpack>>12);
                        unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                        *coldst++ = (unpack>>12);

                        break;

	             case 4: // U and 1st Y

                        unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                        *coldst++ = (unpack>>22);
                        unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12];
                        *coldst++ = (unpack>>2);

                        break;

	             case 5: // U and 2nd Y

                        unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                        *coldst++ = (unpack>>22);
                        unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12];
                        *coldst++ = (unpack>>22);

                        break;
                  }
               }
               else { // copy the bgnd UYVY

                  *coldst++ = bgnd0;
                  *coldst++ = bgnd1;

               }

               colX += hdelX;
               colY += hdelY;

            }

         }

      }

      // advance to the next row

      rowX += vdelX;
      rowY += vdelY;

      endX += vdelX;
      endY += vdelY;

   }

 }
 else { // dst line from closest line

   for (int i=beg;i<beg+lins;i++) {

      int Xbeg = rowX >> 16;
      int Ybeg = rowY >> 16;
      int Xend = endX >> 16;
      int Yend = endY >> 16;

      if ((Xbeg>=0)&&(Xbeg<=wlim)&&(Xend>=0)&&(Xend<=wlim)&&
          (Ybeg>=0)&&(Ybeg<=hlim)&&(Yend>=0)&&(Yend<=hlim)) { // scan-line is inside

         colX = rowX;
         colY = rowY;

          OFENTRY *dstrow = dstTbl + i;

         // ysrc -> pixel of src

          MTI_UINT8 *colsrc;

         // coldst -> pixel of dst
          MTI_UINT8 *coldst = dstbuf[dstrow->rowfld] + ((dstrow->rowoff + dstMinCol)>>1)*4;

          MTI_UINT32 phase = (dstMinCol&1); // 0 for U, 1 for V

          MTI_UINT32 unpack;

         // REWRITTEN TO ELIMINATE PHASE VARIABLE

         int j = 0;
         if (phase==0) {

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

                int x   = colX >> 16;
                int y   = colY >> 16;

                OFENTRY *srcrow = srcTbl + y;

               colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16);

               switch(x%6) {

	          case 0: // U and 1st Y

                     unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		     *coldst++ = (unpack>>2);
                     *coldst++ = (unpack>>12);

                     break;

	          case 1: // U and 2nd Y

                     unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		     *coldst++ = (unpack>>2);
                     unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                     *coldst++ = (unpack>>2);  

                     break;

	          case 2: // U and 1st Y

                     unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                     *coldst++ = (unpack>>12);
                     *coldst++ = (unpack>>22);

                     break;

	          case 3: // U and 2nd Y

                     unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                     *coldst++ = (unpack>>12);
                     unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                     *coldst++ = (unpack>>12);

                     break;

	          case 4: // U and 1st Y

                     unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                     *coldst++ = (unpack>>22);
                     unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12];
                     *coldst++ = (unpack>>2);

                     break;

	          case 5: // U and 2nd Y

                     unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                     *coldst++ = (unpack>>22);
                     unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12]; 
                     *coldst++ = (unpack>>22);

                     break;
               }

               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;

               srcrow = srcTbl + y;
               
               colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16);

               switch(x%6) {

	          case 0: // V and 1st Y

                     unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		     *coldst++ = (unpack>>22);
                     *coldst++ = (unpack>>12);

                     break;

	          case 1: // V and 2nd Y

                     unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		     *coldst++ = (unpack>>22);
                     unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                     *coldst++ = (unpack>>2);  

                     break;

	          case 2: // V and 1st Y

                     unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                     *coldst++ = (unpack>>2);
                     unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                     *coldst++ = (unpack>>22);

                     break;

	          case 3: // V and 2nd Y

                     unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                     *coldst++ = (unpack>>2);
                     *coldst++ = (unpack>>12);

                     break;

	          case 4: // V and 1st Y

                     unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12];
                     *coldst++ = (unpack>>12);
                     *coldst++ = (unpack>>2);

                     break;

	          case 5: // V and 2nd Y

                     unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12];
                     *coldst++ = (unpack>>12);
                     *coldst++ = (unpack>>22);

                     break;
               }

               colX += hdelX;
               colY += hdelY;
            }

         }
         else {

            coldst += 2;

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

                int x   = colX >> 16;
                int y   = colY >> 16;

                OFENTRY *srcrow = srcTbl + y;

               colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16);

               switch(x%6) {

	          case 0: // V and 1st Y

                     unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		     *coldst++ = (unpack>>22);
                     *coldst++ = (unpack>>12);

                     break;

	          case 1: // V and 2nd Y

                     unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		     *coldst++ = (unpack>>22);
                     unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                     *coldst++ = (unpack>>2);  

                     break;

	          case 2: // V and 1st Y

                     unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                     *coldst++ = (unpack>>2);
                     unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                     *coldst++ = (unpack>>22);

                     break;

	          case 3: // V and 2nd Y

                     unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                     *coldst++ = (unpack>>2);
                     *coldst++ = (unpack>>12);

                     break;

	          case 4: // V and 1st Y

                     unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12];
                     *coldst++ = (unpack>>12);
                     *coldst++ = (unpack>>2);

                     break;

	          case 5: // V and 2nd Y

                     unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12];
                     *coldst++ = (unpack>>12);
                     *coldst++ = (unpack>>22);

                     break;
               }

               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;

               srcrow = srcTbl + y;

               colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16);

               switch(x%6) {

	          case 0: // U and 1st Y

                     unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		     *coldst++ = (unpack>>2);
                     *coldst++ = (unpack>>12);

                     break;

	          case 1: // U and 2nd Y 

                     unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		     *coldst++ = (unpack>>2);
                     unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                     *coldst++ = (unpack>>2);  

                     break;

	          case 2: // U and 1st Y

                     unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                     *coldst++ = (unpack>>12);
                     *coldst++ = (unpack>>22);

                     break;

	          case 3: // U and 2nd Y

                     unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                     *coldst++ = (unpack>>12);
                     unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                     *coldst++ = (unpack>>12);

                     break;

	          case 4: // U and 1st Y

                     unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                     *coldst++ = (unpack>>22);
                     unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12];
                     *coldst++ = (unpack>>2);

                     break;

	          case 5: // U and 2nd Y

                     unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                     *coldst++ = (unpack>>22);
                     unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12];
                     *coldst++ = (unpack>>22);

                     break;
               }

               colX += hdelX;
               colY += hdelY;

            }

         }

      }
      else {  // scan-line crosses in and out of source frame

         colX = rowX;
         colY = rowY;

          OFENTRY *dstrow = dstTbl + i;

         // ysrc -> pixel of src
          MTI_UINT8 *colsrc;

         // coldst -> pixel of dst
          MTI_UINT8 *coldst = dstbuf[dstrow->rowfld] + ((dstrow->rowoff + dstMinCol)>>1)*4;

          MTI_UINT32 phase = (dstMinCol&1); // 0 for U, 1 for V

          MTI_UINT32 unpack;

         // REWRITTEN TO ELIMINATE PHASE VARIABLE

         int j = 0;
         if (phase==0) {

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

                int x   = colX >> 16;
                int y   = colY >> 16;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

                   OFENTRY *srcrow = srcTbl + y;

                  colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16);

                  switch(x%6) {

	             case 0: // U and 1st Y

                        unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		        *coldst++ = (unpack>>2);
                        *coldst++ = (unpack>>12);

                        break;

	             case 1: // U and 2nd Y 

                        unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		        *coldst++ = (unpack>>2);
                        unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                        *coldst++ = (unpack>>2);

                        break;

	             case 2: // U and 1st Y

                        unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                        *coldst++ = (unpack>>12);
                        *coldst++ = (unpack>>22);

                        break;

	             case 3: // U and 2nd Y

                        unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                        *coldst++ = (unpack>>12);
                        unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                        *coldst++ = (unpack>>12);

                        break;

	             case 4: // U and 1st Y

                        unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                        *coldst++ = (unpack>>22);
                        unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12];
                        *coldst++ = (unpack>>2);

                        break;

	             case 5: // U and 2nd Y

                        unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                        *coldst++ = (unpack>>22);
                        unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12];
                        *coldst++ = (unpack>>22);

                        break;
                  }
               }
               else { // copy the bgnd UYVY

                  *coldst++ = bgnd0;
                  *coldst++ = bgnd1;
               }

               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

                   OFENTRY *srcrow = srcTbl + y;

                  colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16);

                  switch(x%6) {

	             case 0: // V and 1st Y

                        unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		        *coldst++ = (unpack>>22);
                        *coldst++ = (unpack>>12);

                        break;

	             case 1: // V and 2nd Y 

                        unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		        *coldst++ = (unpack>>22);
                        unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                        *coldst++ = (unpack>>2);

                        break;

	             case 2: // V and 1st Y

                        unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                        *coldst++ = (unpack>>2);
                        unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                        *coldst++ = (unpack>>22);

                        break;

	             case 3: // V and 2nd Y

                        unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                        *coldst++ = (unpack>>2);
                        *coldst++ = (unpack>>12);

                        break;

	             case 4: // V and 1st Y

                        unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12];
                        *coldst++ = (unpack>>12);
                        *coldst++ = (unpack>>2);

                        break;

	             case 5: // V and 2nd Y

                        unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12];
                        *coldst++ = (unpack>>12);
                        *coldst++ = (unpack>>22);

                        break;
                  }
               }
               else { // copy the bgnd UYVY

                  *coldst++ = bgnd2;
                  *coldst++ = bgnd3;
               }

               colX += hdelX;
               colY += hdelY;

            }

         }
         else {

            coldst += 2;

            for (;;) {

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

                int x   = colX >> 16;
                int y   = colY >> 16;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

                   OFENTRY *srcrow = srcTbl + y;

                  colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16);

                  switch(x%6) {

	             case 0: // V and 1st Y

                        unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		        *coldst++ = (unpack>>22);
                        *coldst++ = (unpack>>12);

                        break;

	             case 1: // V and 2nd Y

                        unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		        *coldst++ = (unpack>>22);
                        unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                        *coldst++ = (unpack>>2);  

                        break;

	             case 2: // V and 1st Y

                        unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                        *coldst++ = (unpack>>2);
                        unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                        *coldst++ = (unpack>>22);

                        break;

	             case 3: // V and 2nd Y

                        unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                        *coldst++ = (unpack>>2);
                        *coldst++ = (unpack>>12);

                        break;

	             case 4: // V and 1st Y

                        unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12];
                        *coldst++ = (unpack>>12);
                        *coldst++ = (unpack>>2);

                        break;

	             case 5: // V and 2nd Y

                        unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12];
                        *coldst++ = (unpack>>12);
                        *coldst++ = (unpack>>22);

                        break;
                  }
               }
               else { // copy the bgnd UYVY

                  *coldst++ = bgnd2;
                  *coldst++ = bgnd3;

               }

               colX += hdelX;
               colY += hdelY;

               if (++j > dstWinWdth) break;

               // (colX, colY) is the cur scan pos

               x   = colX >> 16;
               y   = colY >> 16;

               if ((x>=0)&&(x<=wlim)&&
                   (y>=0)&&(y<=hlim)) {

                   OFENTRY *srcrow = srcTbl + y;

                  colsrc = srcbuf[srcrow->rowfld] + (((srcrow->rowoff+x)/6)*16);

                  switch(x%6) {

	             case 0: // U and 1st Y

                        unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		        *coldst++ = (unpack>>2);
                        *coldst++ = (unpack>>12);

                        break;

	             case 1: // U and 2nd Y 

                        unpack = (colsrc[3]<<24)+(colsrc[2]<<16)+(colsrc[1]<<8)+colsrc[0];
		        *coldst++ = (unpack>>2);
                        unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                        *coldst++ = (unpack>>2);

                        break;

	             case 2: // U and 1st Y

                        unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                        *coldst++ = (unpack>>12);
                        *coldst++ = (unpack>>22);

                        break;

	             case 3: // U and 2nd Y

                        unpack = (colsrc[7]<<24)+(colsrc[6]<<16)+(colsrc[5]<<8)+colsrc[4];
                        *coldst++ = (unpack>>12);
                        unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                        *coldst++ = (unpack>>12);

                        break;

	             case 4: // U and 1st Y

                        unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                        *coldst++ = (unpack>>22);
                        unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12];
                        *coldst++ = (unpack>>2);

                        break;

	             case 5: // U and 2nd Y

                        unpack = (colsrc[11]<<24)+(colsrc[10]<<16)+(colsrc[9]<<8)+colsrc[8];
                        *coldst++ = (unpack>>22);
                        unpack = (colsrc[15]<<24)+(colsrc[14]<<16)+(colsrc[13]<<8)+colsrc[12];
                        *coldst++ = (unpack>>22);

                        break;
                  }
               }
               else { // copy the bgnd UYVY

                  *coldst++ = bgnd0;
                  *coldst++ = bgnd1;
               }

               colX += hdelX;
               colY += hdelY;

            }

         }

      }

      // advance to the next row

      rowX += vdelX;
      rowY += vdelY;

      endX += vdelX;
      endY += vdelY;

   }

 }

}
#endif // non-WINDOWS

//////////////////////////////////////////////////////////////////////////////

void xfmDVS10toYUV8(CPanAndScan *vp, int iJob)
{
 MTI_UINT8 *srcbuf[2];
 srcbuf[0] = (MTI_UINT8 *)vp->srcbuf[0];
 srcbuf[1] = (MTI_UINT8 *)vp->srcbuf[1];

 MTI_UINT8 *dstbuf[2];
 dstbuf[0] = (MTI_UINT8 *)vp->xfmBuf[0];
 dstbuf[1] = (MTI_UINT8 *)vp->xfmBuf[1];

 int srcsixes = (vp->wsrc / 6);
 int srcpitch = (vp->wsrc / 6)*16;
 int dstpitch = (vp->wsrc / 6)*4;

 int oneint;

 int lins = vp->hsrc / vp->nStripes;
 MTI_INT32 beg = iJob*lins;
 if (iJob==vp->nStripes-1) {
    lins = vp->hsrc - beg;
 }

 MTI_UINT8 *src, *dst;

 if (vp->srcInterlaced) { // interlaced

    for (int i=beg, j=beg/2;i<beg+lins;i+=2,j++) {

       src = srcbuf[i&1]+srcpitch*(i>>1);
       dst = dstbuf[j&1]+dstpitch*(j>>1);

#ifdef _WINDOWS
#if MTI_ASM_X86_INTEL
       _asm {

            mov esi,src
            mov edi,dst
            mov ecx,srcsixes

       $X0: lodsd
            ror eax,2
            stosb
            ror eax,10
            stosb

            add esi,4

            lodsd
            ror eax,2
            stosb
            ror eax,10
            stosb

            add esi,4

            loop    $X0
       }
  #else

       MTI_UINT32 *srcw = (MTI_UINT32 *)src;
       MTI_UINT32 oneint;

       for (int k=0;k<srcsixes;k++) {

          oneint = *srcw++;
          *dst++ = oneint>>2;     // U
          *dst++ = oneint>>12;    // Y

		  srcw++;

          oneint = *srcw++;
          *dst++ = oneint>>2;     // V
          *dst++ = oneint>>12;    // Y

          srcw++;
       }

  #endif
#else  // SGI code
       MTI_UINT32 oneint;

       for (int k=0;k<srcsixes;k++) {

          oneint = (((MTI_UINT32)src[3])<<24)+(((MTI_UINT32)src[2])<<16)+(((MTI_UINT32)src[1])<<8)+(MTI_UINT32)src[0];
          *dst++ = oneint>>2;     // U
          *dst++ = oneint>>12;    // Y

          src += 8;

          oneint = (((MTI_UINT32)src[3])<<24)+(((MTI_UINT32)src[2])<<16)+(((MTI_UINT32)src[1])<<8)+(MTI_UINT32)src[0];
          *dst++ = oneint>>2;     // V
          *dst++ = oneint>>12;    // Y

          src += 8;
       }
#endif
    }
 }
 else { // non-interlaced

    for (int i=beg, j= beg/2;i<beg+lins;i+=2,j++) {

       src = srcbuf[0]+srcpitch*i;
       dst = dstbuf[0]+dstpitch*j;

#ifdef _WINDOWS
#if MTI_ASM_X86_INTEL
       _asm {

            mov esi,src
            mov edi,dst
            mov ecx,srcsixes

       $X1: lodsd
            ror eax,2
            stosb
            ror eax,10
            stosb

            add esi,4

            lodsd
            ror eax,2
            stosb
            ror eax,10
            stosb

            add esi,4

            loop    $X1
       }
   #else
       MTI_UINT32 *srcw = (MTI_UINT32 *)src;
       MTI_UINT32 oneint;

       for (int k=0;k<srcsixes;k++) {

          oneint = *srcw++;
          *dst++ = oneint>>2;     // U
          *dst++ = oneint>>12;    // Y

		  srcw++;

		  oneint = *srcw++;
		  *dst++ = oneint>>2;     // V
		  *dst++ = oneint>>12;    // Y

          srcw++;
       }
   #endif
#else
       MTI_UINT32 oneint;

       for (int k=0;k<srcsixes;k++) {

          oneint = (src[3]<<24)+(src[2]<<16)+(src[1]<<8)+src[0];
          *dst++ = oneint>>2;     // U
          *dst++ = oneint>>12;    // Y

          src += 8;

          oneint = (src[3]<<24)+(src[2]<<16)+(src[1]<<8)+src[0];
          *dst++ = oneint>>2;     // V
          *dst++ = oneint>>12;    // Y

          src += 8;
       }
#endif
    }
 }
}

//////////////////////////////////////////////////////////////////////////////

void stripe16to16(CPanAndScan *vp, int iJob)
{
 MTI_INT32 vdelX = vp->vdelX;
 MTI_INT32 vdelY = vp->vdelY;
 MTI_INT32 hdelX = vp->hdelX;
 MTI_INT32 hdelY = vp->hdelY;

 MTI_UINT16 *srcbuf[2];
 srcbuf[0] = (MTI_UINT16 *)vp->srcbuf[0];
 srcbuf[1] = (MTI_UINT16 *)vp->srcbuf[1];
  OFENTRY *srcTbl = vp->srcTbl;

 MTI_UINT16 *dstbuf[2];
 dstbuf[0] = (MTI_UINT16 *)vp->dstbuf[0];
 dstbuf[1] = (MTI_UINT16 *)vp->dstbuf[1];
  OFENTRY *dstTbl = vp->dstTbl;

  int dstMinCol  = vp->dstMinCol;
  int dstWinWdth = vp->dstWinWdth;

 MTI_UINT16 bgnd0 = vp->bgnd16[0];
 MTI_UINT16 bgnd1 = vp->bgnd16[1];
 MTI_UINT16 bgnd2 = vp->bgnd16[2];

  int wlim = vp->wsrc - 1;
  int hlim = vp->hsrc - 1;

 // calculate the stripe lines
 int lins = vp->dstWinHght / vp->nStripes;
 MTI_INT32 beg = iJob*lins;
 if (iJob==vp->nStripes-1) {
    lins = vp->dstWinHght - beg;
 }

 // the beg pt of the scan
 MTI_INT32 rowX = vp->begX + beg * vdelX;
 MTI_INT32 rowY = vp->begY + beg * vdelY;

 // the cur pt of the scan
 MTI_INT32 colX;
 MTI_INT32 colY;

 // the end pt of the scan
 MTI_INT32 endX = rowX + (dstWinWdth-1)* hdelX;
 MTI_INT32 endY = rowY + (dstWinWdth-1)* hdelY;

 beg += vp->dstMinRow;

 if (vp->parLin) { // use closest line of correct parity

   for (int i=beg;i<beg+lins;i++) {

      // get parity of dst line
      int oddEven = (i&1);

      int Xbeg = rowX >> 16;
      int Ybeg = rowY >> 16;
      int Xend = endX >> 16;
      int Yend = endY >> 16;

      if ((Xbeg>=0)&&(Xbeg<=wlim)&&(Xend>=0)&&(Xend<=wlim)&&
          (Ybeg>=0)&&(Ybeg<=hlim)&&(Yend>=0)&&(Yend<=hlim)) { // scan-line is inside

         colX = rowX;
         colY = rowY;

          OFENTRY *dstrow = dstTbl + i;

         // ysrc -> pixel of src
          MTI_UINT16 *colsrc;

         // coldst -> pixel of dst
          MTI_UINT16 *coldst = dstbuf[dstrow->rowfld] + (dstrow->rowoff + dstMinCol)*3;

         int j = 0;

         for (j=0;j<dstWinWdth;j++) {

            // (colX, colY) is the cur scan pos

             int x   = colX >> 16;
             int y   = colY >> 16;
            y = (y & ~1) + oddEven;

             OFENTRY *srcrow = srcTbl + y;

            colsrc = srcbuf[srcrow->rowfld] + (srcrow->rowoff + x)*3;

            *coldst++ = *colsrc++;
            *coldst++ = *colsrc++;
            *coldst++ = *colsrc++;

            colX += hdelX;
            colY += hdelY;

         }

      }
      else {  // scan-line crosses in and out of source frame

         colX = rowX;
         colY = rowY;

          OFENTRY *dstrow = dstTbl + i;

         // ysrc -> pixel of src
          MTI_UINT16 *colsrc;

         // coldst -> pixel of dst
          MTI_UINT16 *coldst = dstbuf[dstrow->rowfld] + (dstrow->rowoff + dstMinCol)*3;

         for (int j=0;j<dstWinWdth;j++) {

            // (colX, colY) is the cur scan pos

             int x   = colX >> 16;
             int y   = colY >> 16;
            y = (y & ~1) + oddEven;

            if ((x>=0)&&(x<=wlim)&&
                (y>=0)&&(y<=hlim)) {

                OFENTRY *srcrow = srcTbl + y;

               colsrc = srcbuf[srcrow->rowfld] + (srcrow->rowoff + x)*3;

               *coldst++ = *colsrc++;
               *coldst++ = *colsrc++;
               *coldst++ = *colsrc++;

            }
            else { // copy the bgnd UYVY

               *coldst++ = bgnd0;
               *coldst++ = bgnd1;
               *coldst++ = bgnd2;

            }

            colX += hdelX;
            colY += hdelY;

         }

      }

      // advance to the next row

      rowX += vdelX;
      rowY += vdelY;

      endX += vdelX;
      endY += vdelY;

   }

 }
 else { // dst line from closest line

   for (int i=beg;i<beg+lins;i++) {

      int Xbeg = rowX >> 16;
      int Ybeg = rowY >> 16;
      int Xend = endX >> 16;
      int Yend = endY >> 16;

      if ((Xbeg>=0)&&(Xbeg<=wlim)&&(Xend>=0)&&(Xend<=wlim)&&
          (Ybeg>=0)&&(Ybeg<=hlim)&&(Yend>=0)&&(Yend<=hlim)) { // scan-line is inside

         colX = rowX;
         colY = rowY;

          OFENTRY *dstrow = dstTbl + i;

         // ysrc -> pixel of src
          MTI_UINT16 *colsrc;

         // coldst -> pixel of dst
          MTI_UINT16 *coldst = dstbuf[dstrow->rowfld] + (dstrow->rowoff + dstMinCol)*3;

         int j = 0;

         for (j=0;j<dstWinWdth;j++) {

            // (colX, colY) is the cur scan pos

             int x   = colX >> 16;
             int y   = colY >> 16;

             OFENTRY *srcrow = srcTbl + y;

            colsrc = srcbuf[srcrow->rowfld] + (srcrow->rowoff + x)*3;

            *coldst++ = *colsrc++;
            *coldst++ = *colsrc++;
            *coldst++ = *colsrc++;

            colX += hdelX;
            colY += hdelY;

         }

      }
      else {  // scan-line crosses in and out of source frame

         colX = rowX;
         colY = rowY;

          OFENTRY *dstrow = dstTbl + i;

         // ysrc -> pixel of src
          MTI_UINT16 *colsrc;

         // coldst -> pixel of dst
          MTI_UINT16 *coldst = dstbuf[dstrow->rowfld] + (dstrow->rowoff + dstMinCol)*3;

         for (int j=0;j<dstWinWdth;j++) {

            // (colX, colY) is the cur scan pos

             int x   = colX >> 16;
             int y   = colY >> 16;

            if ((x>=0)&&(x<=wlim)&&
                (y>=0)&&(y<=hlim)) {

                OFENTRY *srcrow = srcTbl + y;

               colsrc = srcbuf[srcrow->rowfld] + (srcrow->rowoff + x)*3;

               *coldst++ = *colsrc++;
               *coldst++ = *colsrc++;
               *coldst++ = *colsrc++;

            }
            else { // copy the bgnd UYVY

               *coldst++ = bgnd0;
               *coldst++ = bgnd1;
               *coldst++ = bgnd2;

            }

            colX += hdelX;
            colY += hdelY;

         }

      }

      // advance to the next row

      rowX += vdelX;
      rowY += vdelY;

      endX += vdelX;
      endY += vdelY;

   }

 }

}

// segment to render 16-bit mat rectangles
void rect16(CPanAndScan *vp, int iJob)
{
   MTI_UINT8 *dstbuf[2];
   dstbuf[0] = (MTI_UINT8 *)vp->dstbuf[0];
   dstbuf[1] = (MTI_UINT8 *)vp->dstbuf[1];
    OFENTRY *dstTbl = vp->dstTbl;
   MTI_UINT8 *linBuf = vp->linBuf;
    int rctMinCol  = vp->rctMinCol;
    int rctWinWdth = vp->rctWinWdth*6;

   // calculate the stripe lines
   int lins = vp->rctWinHght / vp->nStripes;
   MTI_INT32 beg = iJob*lins;
   if (iJob==vp->nStripes-1) {
      lins = vp->rctWinHght - beg;
   }

   beg += vp->rctMinRow;
   for (int i=beg;i<beg+lins;i++) {

       OFENTRY *dstrow = dstTbl + i;

      // coldst -> pixel of dst
       MTI_UINT8 *coldst = dstbuf[dstrow->rowfld] + (dstrow->rowoff + rctMinCol)*6;

      // copy from the one-line buffer
      memcpy(coldst,linBuf,rctWinWdth);

   }
}


//////////////////////////////////////////////////////////////////////////////

void outline(CPanAndScan *vp)
{
   if (vp->srcOutl) {

      // use the graphic engine to draw the SRC quadrilateral

      (vp->linEng)->setFGColor(1);
      (vp->grEng)->clipAndScale(vp->fsrcWin,5);

   }

}

////////////////////////////////////////////////////////////////////////////

void CPanAndScan::
renderMat()
{
   int iRet;

   // the top mat rectangle is above the dst window
   rctMinRow = 0;
   rctMinCol = 0;
   rctMaxRow = dstMinRow - 1;
   rctMaxCol = wdst - 1;
   rctWinHght = rctMaxRow - rctMinRow + 1;
   rctWinWdth = rctMaxCol - rctMinCol + 1;
   if ((rctWinHght>0)&&(rctWinWdth>0)) {
#ifdef RCT_MULTI
      iRet = MThreadStart(&rctThread);
      if (iRet) {
        TRACE_0(errout << "PanAndScan: Unable to alloc cvt thread");
        return;
      }
#else
      rctThread.PrimaryFunction(this,0);
#endif
   }

   // the lft mat rectangle is to the left of the dst window
   rctMinRow = dstMinRow;
   rctMinCol = 0;
   rctMaxRow = dstMaxRow;
   rctMaxCol = ((dstMinCol - 1)&0xfffffffe)+1;
   rctWinHght = rctMaxRow - rctMinRow + 1;
   rctWinWdth = rctMaxCol - rctMinCol + 1;
   if ((rctWinHght>0)&&(rctWinWdth>0)) {
#ifdef RCT_MULTI
      iRet = MThreadStart(&rctThread);
      if (iRet) {
         TRACE_0(errout << "PanAndScan: Unable to start rct thread");
         return;
      }
#else
      rctThread.PrimaryFunction(this,0);
#endif
   }

   // the rgt mat rectangle is to the right of the dst window
   rctMinRow = dstMinRow;
   rctMinCol = (dstMaxCol+1)&0xfffffffe;
   rctMaxRow = dstMaxRow;
   rctMaxCol = wdst - 1;
   rctWinHght = rctMaxRow - rctMinRow + 1;
   rctWinWdth = rctMaxCol - rctMinCol + 1;
   if ((rctWinHght>0)&&(rctWinWdth>0)) {
#ifdef RCT_MULTI
      iRet = MThreadStart(&rctThread);
      if (iRet) {
         TRACE_0(errout << "PanAndScan: Unable to alloc cvt thread");
         return;
      }
#else
      rctThread.PrimaryFunction(this,0);
#endif
   }

   // the bot mat rectangle is below the dst window
   rctMinRow = dstMaxRow+1;
   rctMinCol = 0;
   rctMaxRow = hdst - 1;
   rctMaxCol = wdst - 1;
   rctWinHght = rctMaxRow - rctMinRow + 1;
   rctWinWdth = rctMaxCol - rctMinCol + 1;
   if ((rctWinHght>0)&&(rctWinWdth>0)) {
#ifdef RCT_MULTI
      iRet = MThreadStart(&rctThread);
      if (iRet) {
         TRACE_0(errout << "PanAndScan: Unable to alloc cvt thread");
         return;
      }
#else
      rctThread.PrimaryFunction(this,0);
#endif
   }
}

////////////////////////////////////////////////////////////////////////////

void CPanAndScan::
convert(MTI_UINT8 **srcbf,MTI_UINT8 **dstbf, bool parlin)
{
   int iRet;

   // drop args into object (to make them
   // part of vp passed to primary func)
   srcbuf = srcbf;
   dstbuf = dstbf;

   // if flag is set then maintain line
   // parity from source to destination
   parLin = parlin;

   // the SRC quadrilateral is rendered
   // into the DST window of the DST frame

   srcOutl = false; // flag clear

   // the vector for LFT-RGT scanning

   horzX = ((MTI_INT32)(fsrcWin[1].x*ONE)) - ((MTI_INT32)(fsrcWin[0].x*ONE));
   horzY = ((MTI_INT32)(fsrcWin[1].y*ONE)) - ((MTI_INT32)(fsrcWin[0].y*ONE));

   hdelX = horzX / (MTI_INT32)dstWinWdth;
   hdelY = horzY / (MTI_INT32)dstWinWdth;

   // the vector for UP-DN scanning

   vertX = ((MTI_INT32)(fsrcWin[3].x*ONE)) - ((MTI_INT32)(fsrcWin[0].x*ONE));
   vertY = ((MTI_INT32)(fsrcWin[3].y*ONE)) - ((MTI_INT32)(fsrcWin[0].y*ONE));

   vdelX = vertX / (MTI_INT32)dstWinHght;
   vdelY = vertY / (MTI_INT32)dstWinHght;

   // the STARTING POINT for the scan

   begX = ((MTI_INT32)(fsrcWin[0].x*ONE)) + (hdelX>>1) + (vdelX>>1);
   begY = ((MTI_INT32)(fsrcWin[0].y*ONE)) + (hdelY>>1) + (vdelY>>1);

   // for the case of DVS10 to YUV8 the stripe code will see an
   // intermediate which is 1/3 the width and 1/2 the height. We
   // adjust the scan parameters to account for this.
   if (xfmBuf[0] != NULL) { // DVS 10 to YUV 8

      hdelX /= 3;
      hdelY /= 3;

      vdelX /= 2;
      vdelY /= 2;

      begX /= 3;
      begY /= 3;
   }

   // render the bgnd mat
   renderMat();

   // if doing DVS10 to YUV8, first
   // create the YUV8 intermediate
   if (xfmBuf[0] != NULL) {

#ifdef XFM_MULTI
      iRet = MThreadStart(&xfmThread);
      if (iRet)
       {
         //unexpected();
         TRACE_0(errout << "PanAndScan: Can't start RCT thread");
         return;
       }
#else
      xfmDVS10toYUV8NoMulti(srcbuf,xfmBuf);
#endif

      // the source buf for the panandscan
      // is now the intermediate buffer
      srcbuf[0] = xfmBuf[0];
      srcbuf[1] = xfmBuf[1];
   }

#ifdef CVT_MULTI
   // multiprocess the stripes
   iRet = MThreadStart(&cvtThread);
   if (iRet)
    {
     //unexpected();
     TRACE_0(errout << "PanAndScan: Can't start CVT thread");
     return;
    }
#else
    cvtThread.PrimaryFunction(this,0);
#endif

}

/////////////////////////////////////////////////////////////////////////////

void CPanAndScan::
displaySource(MTI_UINT8 **srcbf,MTI_UINT8 **dstbf, bool parlin)
{
   int iRet;

   // drop args into object (to make them
   // part of vp passed to primary func)
   srcbuf = srcbf;
   dstbuf = dstbf;

   // if flag is set then maintain line
   // parity from source to destination
   parLin = parlin;

   // hook the dst buffer to line-draw engine

   linEng->setExternalFrameBuffer(dstbuf);

   // the entire SRC frame is rendered into
   // the DST window of the DST frame, and
   // the OUTLINE of the SRC quadrilateral
   // is superimposed on it to scale

   srcOutl = true;

   // the vector for LFT-RGT scanning

   horzX = (MTI_INT32)((wsrc)*ONE);
   horzY = (MTI_INT32) 0;

   hdelX = horzX / (MTI_INT32)dstWinWdth;
   hdelY = (MTI_INT32) 0;

   // the vector for UP-DN scanning

   vertX = (MTI_INT32) 0;
   vertY = (MTI_INT32)((hsrc)*ONE);

   vdelX = (MTI_INT32) 0;
   vdelY = vertY / (MTI_INT32)dstWinHght;

   // the STARTING POINT for the scan

   begX = (hdelX>>1);
   begY = (vdelY>>1);

   // render the bgnd mat
   renderMat();

   // if doing DVS10 to YUV8, first
   // create the YUV8 intermediate
   if (xfmBuf[0] != NULL) {

#ifdef XFM_MULTI
      iRet = MThreadStart(&xfmThread);
      if (iRet)
       {
         //unexpected();
         TRACE_0(errout << "PanAndScan: Can't start XFM thread");
         return;
       }
#else
      xfmDVS10toYUV8NoMulti(srcbuf,xfmBuf);
#endif
      // the source buf for the panandscan
      // is now the intermediate buffer
      srcbuf[0] = xfmBuf[0];
      srcbuf[1] = xfmBuf[1];
   }


#ifdef CVT_MULTI
   // multiprocess the stripes
   iRet = MThreadStart(&cvtThread);
   if (iRet)
    {
     //unexpected();
     TRACE_0(errout << "PanAndScan: Can't start CVT thread");
     return;
    }
#else
    cvtThread.PrimaryFunction(this,0);
#endif

}

/////////////////////////////////////////////////////////////////////////////
//
// transform a DVS 10-bit frame to a YUV 8-bit frame with 1/3 the width
// and 1/2 the height. This version uses no multiprocessing.
//
void CPanAndScan::
xfmDVS10toYUV8NoMulti(MTI_UINT8 **srcbuf, MTI_UINT8 **dstbuf)
{
   int srcsixes = (wsrc / 6);
   int srcpitch = (wsrc / 6)*16;
   int dstpitch = (wsrc / 6)*4;
   int lins = hsrc;

   MTI_UINT8 *src, *dst;

   if (srcInterlaced) { // interlaced

      for (int i=0, j=0;i<lins;i+=2,j++) {

         src = srcbuf[i&1]+srcpitch*(i>>1);
         dst = dstbuf[j&1]+dstpitch*(j>>1);

#ifdef _WINDOWS
#if MTI_ASM_X86_INTEL
         _asm {

            mov esi,src
            mov edi,dst
            mov ecx,srcsixes

       $X0: lodsd
            ror eax,2
            stosb       // U
            ror eax,10
            stosb       // Y

            add esi,4

            lodsd
            ror eax,2
            stosb       // V
            ror eax,10
            stosb       // Y

            add esi,4

            loop    $X0
         }
#else
         MTI_UINT32 *srcw = (MTI_UINT32 *)src;
         MTI_UINT32 oneint;

         for (int k=0;k<srcsixes;k++) {

            oneint = *srcw++;
            *dst++ = oneint>>2;     // U
            *dst++ = oneint>>12;    // Y

            srcw++;

            oneint = *srcw++;
            *dst++ = oneint>>2;     // V
            *dst++ = oneint>>12;    // Y

            srcw++;
         }
#endif
#endif
      }
   }
   else {

      for (int i=0, j=0;i<lins;i+=2,j++) {

         src = srcbuf[0]+srcpitch*(i);
         dst = dstbuf[0]+dstpitch*(j);

#ifdef _WINDOWS
#if MTI_ASM_X86_INTEL
         _asm {

            mov esi,src
            mov edi,dst
            mov ecx,srcsixes

       $X1: lodsd
            ror eax,2
            stosb       // U
            ror eax,10
            stosb       // Y

            add esi,4

            lodsd
            ror eax,2
            stosb       // V
            ror eax,10
            stosb       // Y

            add esi,4

            loop    $X1
         }
#else
         MTI_UINT32 *srcw = (MTI_UINT32 *)src;
         MTI_UINT32 oneint;

         for (int k=0;k<srcsixes;k++) {

            oneint = *srcw++;
            *dst++ = oneint>>2;
            *dst++ = oneint>>12;

			srcw++;

			oneint = *srcw++;
			*dst++ = oneint>>2;
			*dst++ = oneint>>12;

            srcw++;
         }
#endif
#endif
      }
   }
}







