/*
	File:	AspectRatioAM.h
	By:	Kevin Manbeck
	Date:	Oct 5, 2004

	AspectRatioAM is used to convert anamorphic images into
	their anamorphic.  The conversion must be very
	efficient so that it can be done in real time.

*/

#ifndef ASPECT_RATIO_AM
#define ASPECT_RATIO_AM

#include "imgToolDLL.h"
#include "machine.h"

class CImageFormat;

class MTI_IMGTOOLDLL_API CAspectRatioAM
{
public:
  CAspectRatioAM();
  ~CAspectRatioAM();

  void Free ();
  int Init (const CImageFormat *ifp);
  int Render (MTI_UINT8 *ucpSrc[2], MTI_UINT8 *ucpDst[2]);

  int getMatteCoordinates (int iJustify, float fMatteRatio,
		int &iRow0, int &iRow1, int &iCol0, int &iCol1);

private:
  int
    iNRow,
    iNCol,
    iPitch;

  int
    iBytePerLine;

  bool
    bInit;

  int
    iPixelPacking,
    iColorSpace,
    iNField;

  float
    fRowMatteFactor,
    fColMatteFactor;

  int Render_8_YUV_2 (MTI_UINT8 *ucpSrc[2], MTI_UINT8 *ucpDst[2]);
};

#endif


