// BufferPool.h
//
// include file for class CBufferPool
//
//////////////////////////////////////////////////////////////////////

#ifndef BufferPoolH
#define BufferPoolH

#include "machine.h"
#include "imgToolDLL.h"
#include "ThreadLocker.h"
#include <map>
using std::map;
#include <vector>
using std::vector;

//////////////////////////////////////////////////////////////////////

class MTI_IMGTOOLDLL_API CBufferPool
{

public:

   static CBufferPool *Create(size_t bufferSizeInBytes);
   static bool DestroyAllUnusedBuffers(CBufferPool *bufferPool);

   size_t getBufferSizeInBytes();  // Number of bytes in a buffer

   // increase the number of buffers in the pool (starts out at 0)
   int increasePoolSize(int newBufferCount);

   int allocateBuffer(void** bufferPtrPtr);
   void freeBuffer(void *bufferPtr);

   // tell us what's in the buffer and get a buffer with specific contents
   // (returns NULL if the requested contents are not in the cache)
   int identifyBufferContents(void* bufAddr, const string &clipGUID,
                              int frameIndex, int fieldIndex);

   // Find a free buffer that has the specified contents
   void *retrieveCachedBuffer(const string &clipGUID,
                              int frameIndex, int fieldIndex);

   // These allow the caller to tell us that contents of a buffer may no
   // longer be pertinent
   int invalidateBufferContents(void* bufAddr);
   int invalidateBufferContents(const string &clipGUID,
                                int frameIndex, int fieldIndex);


private:
   static void Destroy(CBufferPool *bufferPool);

   // Use factory/junkyard methods instead!
   CBufferPool(size_t bufferSizeInBytes);
   ~CBufferPool();

   bool poolWasDestroyed = false;
   size_t bytesPerBuffer = 0;         // Number of bytes in a single buffer;
   CSpinLock threadLock;

   // Changed to suppor cuda memory
   struct BufferSlot
   {
      void *bufAddr = nullptr;

      // new caching stuff -- info is valid if clipGUID is not empty
      string clipGUID;
      int frameIndex = -1;
      int fieldIndex = -1;

      bool inUseFlag = false;
      bool cudaMemory = false;
   };

   vector<BufferSlot> bufferSlots;
   typedef vector<BufferSlot>::iterator BufferSlotIterator;

   bool findSlotByBufAddr(void *argBufAddr, BufferSlotIterator &iter);
   bool findFreeSlot(BufferSlotIterator &iter);
   bool findSlotByCacheInfo(const string &clipGUID, int frameIndex, int fieldIndex,
                            BufferSlotIterator &iter);

   bool allocateMemoryForABuffer(BufferSlot &slot);
   void deallocateMemoryForABuffer(BufferSlot &slot);
	bool deallocateAllInactiveBuffers();

	friend std::ostream& operator<< (std::ostream& stream, const CBufferPool& bufferPool);
};

std::ostream& operator<< (std::ostream& stream, const CBufferPool& bufferPool);

//////////////////////////////////////////////////////////////////////

#endif // #ifndef BUFFER_POOL_H

