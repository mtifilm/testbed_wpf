/*
	File:	RenderH_YUV_Unrolled_C.cpp
	By:	Kevin Manbeck
	Date:	April 15, 2005

	Improve the efficiency of the code by unrolling the innermost loops
*/

#include "AspectRatio.h"

void CAspectRatio::RenderV_YUV_1 (MTI_UINT8 *ucpDst[2], int &iRow, int iStopDstLocal,
		int iJob)
/*
	Do VERTICAL rendering with 1 neighbor
*/
{
  MTI_UINT8 *ucpD;
  int iField, iR, *ipSP, *ipP, iPixel, iNei;
  int **ipLUT, iSubPixel;
  int iSum[VERTICAL_COMPONENTS], *ipWei;
  MTI_UINT8 *ucpUnpack[VERTICAL_COMPONENTS];
  int iComponent;

  // check for errors
  if ((iRow & 0x1) || (iStopDstLocal & 0x1))
    return;

  int iField0, iField1;

  if (iNFieldDst == 2)
   {
    iField0 = 0;
    iField1 = 1;
   }
  else
   {
    iField0 = 0;
    iField1 = 0;
   }


  ipSP = ipLUTSubPixel + iRow;
  ipP = ipLUTPixel + iRow;
  for (; iRow < iStopDstLocal; iRow+=2)
   {
    // do the "even" field
    iR = iRow / iNFieldDst;

    ucpD = ucpDst[iField0] + (iR * iPitchDst);

    iSubPixel = ipSP[0];
    iPixel = ipP[0];

    for (iComponent = 0; iComponent < iUnpackedComponent; iComponent += VERTICAL_COMPONENTS)
     {
      ipLUT = ipLUTWei[iSubPixel];
      ucpUnpack[0] = ucpUnpacked[iJob][iComponent] + iPixel;
      ucpUnpack[1] = ucpUnpacked[iJob][iComponent + 1] + iPixel;
      ucpUnpack[2] = ucpUnpacked[iJob][iComponent + 2] + iPixel;
      ucpUnpack[3] = ucpUnpacked[iJob][iComponent + 3] + iPixel;
      ucpUnpack[4] = ucpUnpacked[iJob][iComponent + 4] + iPixel;
      ucpUnpack[5] = ucpUnpacked[iJob][iComponent + 5] + iPixel;
      ucpUnpack[6] = ucpUnpacked[iJob][iComponent + 6] + iPixel;
      ucpUnpack[7] = ucpUnpacked[iJob][iComponent + 7] + iPixel;
      ucpUnpack[8] = ucpUnpacked[iJob][iComponent + 8] + iPixel;
      ucpUnpack[9] = ucpUnpacked[iJob][iComponent + 9] + iPixel;
      ucpUnpack[10] = ucpUnpacked[iJob][iComponent + 10] + iPixel;
      ucpUnpack[11] = ucpUnpacked[iJob][iComponent + 11] + iPixel;
      ucpUnpack[12] = ucpUnpacked[iJob][iComponent + 12] + iPixel;
      ucpUnpack[13] = ucpUnpacked[iJob][iComponent + 13] + iPixel;
      ucpUnpack[14] = ucpUnpacked[iJob][iComponent + 14] + iPixel;
      ucpUnpack[15] = ucpUnpacked[iJob][iComponent + 15] + iPixel;
      ucpUnpack[16] = ucpUnpacked[iJob][iComponent + 16] + iPixel;
      ucpUnpack[17] = ucpUnpacked[iJob][iComponent + 17] + iPixel;
      ucpUnpack[18] = ucpUnpacked[iJob][iComponent + 18] + iPixel;
      ucpUnpack[19] = ucpUnpacked[iJob][iComponent + 19] + iPixel;
      ucpUnpack[20] = ucpUnpacked[iJob][iComponent + 20] + iPixel;
      ucpUnpack[21] = ucpUnpacked[iJob][iComponent + 21] + iPixel;
      ucpUnpack[22] = ucpUnpacked[iJob][iComponent + 22] + iPixel;
      ucpUnpack[23] = ucpUnpacked[iJob][iComponent + 23] + iPixel;

      // pre-round the sums
      iSum[0] = iSum[1] = iSum[2] = iSum[3] =
      iSum[4] = iSum[5] = iSum[6] = iSum[7] =
      iSum[8] = iSum[9] = iSum[10] = iSum[11] =
      iSum[12] = iSum[13] = iSum[14] = iSum[15] =
      iSum[16] = iSum[17] = iSum[18] = iSum[19] =
      iSum[20] = iSum[21] = iSum[22] = iSum[23] =
                  iHalfPrecision;

      // do the 1 neighbor
      ipWei = ipLUT[0];
      iSum[0] += ipWei[ucpUnpack[0][0]];
      iSum[1] += ipWei[ucpUnpack[1][0]];
      iSum[2] += ipWei[ucpUnpack[2][0]];
      iSum[3] += ipWei[ucpUnpack[3][0]];
      iSum[4] += ipWei[ucpUnpack[4][0]];
      iSum[5] += ipWei[ucpUnpack[5][0]];
      iSum[6] += ipWei[ucpUnpack[6][0]];
      iSum[7] += ipWei[ucpUnpack[7][0]];
      iSum[8] += ipWei[ucpUnpack[8][0]];
      iSum[9] += ipWei[ucpUnpack[9][0]];
      iSum[10] += ipWei[ucpUnpack[10][0]];
      iSum[11] += ipWei[ucpUnpack[11][0]];
      iSum[12] += ipWei[ucpUnpack[12][0]];
      iSum[13] += ipWei[ucpUnpack[13][0]];
      iSum[14] += ipWei[ucpUnpack[14][0]];
      iSum[15] += ipWei[ucpUnpack[15][0]];
      iSum[16] += ipWei[ucpUnpack[16][0]];
      iSum[17] += ipWei[ucpUnpack[17][0]];
      iSum[18] += ipWei[ucpUnpack[18][0]];
      iSum[19] += ipWei[ucpUnpack[19][0]];
      iSum[20] += ipWei[ucpUnpack[20][0]];
      iSum[21] += ipWei[ucpUnpack[21][0]];
      iSum[22] += ipWei[ucpUnpack[22][0]];
      iSum[23] += ipWei[ucpUnpack[23][0]];

      // clamp and dump the results

      // no rounding - sums were pre-rounded
      // I commented out the "& 0xFF" because I assume the min/max
      // code will take care of making sure the sums are not negative;
      // if that assumption is not valid, they need to be uncommented
      long lTmp1, lTmp2;

      lTmp1 = iSum[0];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[1];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[2];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[3];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent] = lTmp1;

      lTmp1 = iSum[4];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[5];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[6];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[7];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 4] = lTmp1;

      lTmp1 = iSum[8];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[9];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[10];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[11];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 8] = lTmp1;

      lTmp1 = iSum[12];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[13];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[14];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[15];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 12] = lTmp1;

      lTmp1 = iSum[16];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[17];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[18];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[19];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 16] = lTmp1;

      lTmp1 = iSum[20];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[21];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[22];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[23];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 20] = lTmp1;
    }

    // do the "odd" field
    iR = (iRow+1) / iNFieldDst;

    ucpD = ucpDst[iField1] + (iR * iPitchDst);

    iSubPixel = ipSP[1];
    iPixel = ipP[1];

    for (iComponent = 0; iComponent < iUnpackedComponent; iComponent += VERTICAL_COMPONENTS)
     {
      ipLUT = ipLUTWei[iSubPixel];
      ucpUnpack[0] = ucpUnpacked[iJob][iComponent] + iPixel;
      ucpUnpack[1] = ucpUnpacked[iJob][iComponent + 1] + iPixel;
      ucpUnpack[2] = ucpUnpacked[iJob][iComponent + 2] + iPixel;
      ucpUnpack[3] = ucpUnpacked[iJob][iComponent + 3] + iPixel;
      ucpUnpack[4] = ucpUnpacked[iJob][iComponent + 4] + iPixel;
      ucpUnpack[5] = ucpUnpacked[iJob][iComponent + 5] + iPixel;
      ucpUnpack[6] = ucpUnpacked[iJob][iComponent + 6] + iPixel;
      ucpUnpack[7] = ucpUnpacked[iJob][iComponent + 7] + iPixel;
      ucpUnpack[8] = ucpUnpacked[iJob][iComponent + 8] + iPixel;
      ucpUnpack[9] = ucpUnpacked[iJob][iComponent + 9] + iPixel;
      ucpUnpack[10] = ucpUnpacked[iJob][iComponent + 10] + iPixel;
      ucpUnpack[11] = ucpUnpacked[iJob][iComponent + 11] + iPixel;
      ucpUnpack[12] = ucpUnpacked[iJob][iComponent + 12] + iPixel;
      ucpUnpack[13] = ucpUnpacked[iJob][iComponent + 13] + iPixel;
      ucpUnpack[14] = ucpUnpacked[iJob][iComponent + 14] + iPixel;
      ucpUnpack[15] = ucpUnpacked[iJob][iComponent + 15] + iPixel;
      ucpUnpack[16] = ucpUnpacked[iJob][iComponent + 16] + iPixel;
      ucpUnpack[17] = ucpUnpacked[iJob][iComponent + 17] + iPixel;
      ucpUnpack[18] = ucpUnpacked[iJob][iComponent + 18] + iPixel;
      ucpUnpack[19] = ucpUnpacked[iJob][iComponent + 19] + iPixel;
      ucpUnpack[20] = ucpUnpacked[iJob][iComponent + 20] + iPixel;
      ucpUnpack[21] = ucpUnpacked[iJob][iComponent + 21] + iPixel;
      ucpUnpack[22] = ucpUnpacked[iJob][iComponent + 22] + iPixel;
      ucpUnpack[23] = ucpUnpacked[iJob][iComponent + 23] + iPixel;

      // pre-round the sums
      iSum[0] = iSum[1] = iSum[2] = iSum[3] =
      iSum[4] = iSum[5] = iSum[6] = iSum[7] =
      iSum[8] = iSum[9] = iSum[10] = iSum[11] =
      iSum[12] = iSum[13] = iSum[14] = iSum[15] =
      iSum[16] = iSum[17] = iSum[18] = iSum[19] =
      iSum[20] = iSum[21] = iSum[22] = iSum[23] =
                  iHalfPrecision;

      // do the 1 neighbor
      ipWei = ipLUT[0];
      iSum[0] += ipWei[ucpUnpack[0][0]];
      iSum[1] += ipWei[ucpUnpack[1][0]];
      iSum[2] += ipWei[ucpUnpack[2][0]];
      iSum[3] += ipWei[ucpUnpack[3][0]];
      iSum[4] += ipWei[ucpUnpack[4][0]];
      iSum[5] += ipWei[ucpUnpack[5][0]];
      iSum[6] += ipWei[ucpUnpack[6][0]];
      iSum[7] += ipWei[ucpUnpack[7][0]];
      iSum[8] += ipWei[ucpUnpack[8][0]];
      iSum[9] += ipWei[ucpUnpack[9][0]];
      iSum[10] += ipWei[ucpUnpack[10][0]];
      iSum[11] += ipWei[ucpUnpack[11][0]];
      iSum[12] += ipWei[ucpUnpack[12][0]];
      iSum[13] += ipWei[ucpUnpack[13][0]];
      iSum[14] += ipWei[ucpUnpack[14][0]];
      iSum[15] += ipWei[ucpUnpack[15][0]];
      iSum[16] += ipWei[ucpUnpack[16][0]];
      iSum[17] += ipWei[ucpUnpack[17][0]];
      iSum[18] += ipWei[ucpUnpack[18][0]];
      iSum[19] += ipWei[ucpUnpack[19][0]];
      iSum[20] += ipWei[ucpUnpack[20][0]];
      iSum[21] += ipWei[ucpUnpack[21][0]];
      iSum[22] += ipWei[ucpUnpack[22][0]];
      iSum[23] += ipWei[ucpUnpack[23][0]];

      // clamp and dump the results

      // no rounding - sums were pre-rounded
      // I commented out the "& 0xFF" because I assume the min/max
      // code will take care of making sure the sums are not negative;
      // if that assumption is not valid, they need to be uncommented
      long lTmp1, lTmp2;

      lTmp1 = iSum[0];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[1];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[2];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[3];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent] = lTmp1;

      lTmp1 = iSum[4];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[5];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[6];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[7];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 4] = lTmp1;

      lTmp1 = iSum[8];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[9];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[10];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[11];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 8] = lTmp1;

      lTmp1 = iSum[12];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[13];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[14];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[15];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 12] = lTmp1;

      lTmp1 = iSum[16];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[17];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[18];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[19];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 16] = lTmp1;

      lTmp1 = iSum[20];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[21];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[22];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[23];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 20] = lTmp1;
     }
    ipSP += 2;
    ipP += 2;
   }
}

void CAspectRatio::RenderV_YUV_3 (MTI_UINT8 *ucpDst[2], int &iRow, int iStopDstLocal,
		int iJob)
/*
	Do VERTICAL rendering with 3 neighbors
*/
{
  MTI_UINT8 *ucpD;
  int iField, iR, *ipSP, *ipP, iPixel, iNei;
  int **ipLUT, iSubPixel;
  int iSum[VERTICAL_COMPONENTS], *ipWei;
  MTI_UINT8 *ucpUnpack[VERTICAL_COMPONENTS];
  int iComponent;

  // check for errors
  if ((iRow & 0x1) || (iStopDstLocal & 0x1))
    return;

  int iField0, iField1;

  if (iNFieldDst == 2)
   {
    iField0 = 0;
    iField1 = 1;
   }
  else
   {
    iField0 = 0;
    iField1 = 0;
   }


  ipSP = ipLUTSubPixel + iRow;
  ipP = ipLUTPixel + iRow;
  for (; iRow < iStopDstLocal; iRow+=2)
   {
    // do the "even" field
    iR = iRow / iNFieldDst;

    ucpD = ucpDst[iField0] + (iR * iPitchDst);

    iSubPixel = ipSP[0];
    iPixel = ipP[0];

    for (iComponent = 0; iComponent < iUnpackedComponent; iComponent += VERTICAL_COMPONENTS)
     {
      ipLUT = ipLUTWei[iSubPixel];
      ucpUnpack[0] = ucpUnpacked[iJob][iComponent] + iPixel;
      ucpUnpack[1] = ucpUnpacked[iJob][iComponent + 1] + iPixel;
      ucpUnpack[2] = ucpUnpacked[iJob][iComponent + 2] + iPixel;
      ucpUnpack[3] = ucpUnpacked[iJob][iComponent + 3] + iPixel;
      ucpUnpack[4] = ucpUnpacked[iJob][iComponent + 4] + iPixel;
      ucpUnpack[5] = ucpUnpacked[iJob][iComponent + 5] + iPixel;
      ucpUnpack[6] = ucpUnpacked[iJob][iComponent + 6] + iPixel;
      ucpUnpack[7] = ucpUnpacked[iJob][iComponent + 7] + iPixel;
      ucpUnpack[8] = ucpUnpacked[iJob][iComponent + 8] + iPixel;
      ucpUnpack[9] = ucpUnpacked[iJob][iComponent + 9] + iPixel;
      ucpUnpack[10] = ucpUnpacked[iJob][iComponent + 10] + iPixel;
      ucpUnpack[11] = ucpUnpacked[iJob][iComponent + 11] + iPixel;
      ucpUnpack[12] = ucpUnpacked[iJob][iComponent + 12] + iPixel;
      ucpUnpack[13] = ucpUnpacked[iJob][iComponent + 13] + iPixel;
      ucpUnpack[14] = ucpUnpacked[iJob][iComponent + 14] + iPixel;
      ucpUnpack[15] = ucpUnpacked[iJob][iComponent + 15] + iPixel;
      ucpUnpack[16] = ucpUnpacked[iJob][iComponent + 16] + iPixel;
      ucpUnpack[17] = ucpUnpacked[iJob][iComponent + 17] + iPixel;
      ucpUnpack[18] = ucpUnpacked[iJob][iComponent + 18] + iPixel;
      ucpUnpack[19] = ucpUnpacked[iJob][iComponent + 19] + iPixel;
      ucpUnpack[20] = ucpUnpacked[iJob][iComponent + 20] + iPixel;
      ucpUnpack[21] = ucpUnpacked[iJob][iComponent + 21] + iPixel;
      ucpUnpack[22] = ucpUnpacked[iJob][iComponent + 22] + iPixel;
      ucpUnpack[23] = ucpUnpacked[iJob][iComponent + 23] + iPixel;

      // pre-round the sums
      iSum[0] = iSum[1] = iSum[2] = iSum[3] =
      iSum[4] = iSum[5] = iSum[6] = iSum[7] =
      iSum[8] = iSum[9] = iSum[10] = iSum[11] =
      iSum[12] = iSum[13] = iSum[14] = iSum[15] =
      iSum[16] = iSum[17] = iSum[18] = iSum[19] =
      iSum[20] = iSum[21] = iSum[22] = iSum[23] =
                  iHalfPrecision;

      // do the 3 neighbors
      ipWei = ipLUT[0];
      iSum[0] += ipWei[ucpUnpack[0][0]];
      iSum[1] += ipWei[ucpUnpack[1][0]];
      iSum[2] += ipWei[ucpUnpack[2][0]];
      iSum[3] += ipWei[ucpUnpack[3][0]];
      iSum[4] += ipWei[ucpUnpack[4][0]];
      iSum[5] += ipWei[ucpUnpack[5][0]];
      iSum[6] += ipWei[ucpUnpack[6][0]];
      iSum[7] += ipWei[ucpUnpack[7][0]];
      iSum[8] += ipWei[ucpUnpack[8][0]];
      iSum[9] += ipWei[ucpUnpack[9][0]];
      iSum[10] += ipWei[ucpUnpack[10][0]];
      iSum[11] += ipWei[ucpUnpack[11][0]];
      iSum[12] += ipWei[ucpUnpack[12][0]];
      iSum[13] += ipWei[ucpUnpack[13][0]];
      iSum[14] += ipWei[ucpUnpack[14][0]];
      iSum[15] += ipWei[ucpUnpack[15][0]];
      iSum[16] += ipWei[ucpUnpack[16][0]];
      iSum[17] += ipWei[ucpUnpack[17][0]];
      iSum[18] += ipWei[ucpUnpack[18][0]];
      iSum[19] += ipWei[ucpUnpack[19][0]];
      iSum[20] += ipWei[ucpUnpack[20][0]];
      iSum[21] += ipWei[ucpUnpack[21][0]];
      iSum[22] += ipWei[ucpUnpack[22][0]];
      iSum[23] += ipWei[ucpUnpack[23][0]];

      ipWei = ipLUT[1];
      iSum[0] += ipWei[ucpUnpack[0][1]];
      iSum[1] += ipWei[ucpUnpack[1][1]];
      iSum[2] += ipWei[ucpUnpack[2][1]];
      iSum[3] += ipWei[ucpUnpack[3][1]];
      iSum[4] += ipWei[ucpUnpack[4][1]];
      iSum[5] += ipWei[ucpUnpack[5][1]];
      iSum[6] += ipWei[ucpUnpack[6][1]];
      iSum[7] += ipWei[ucpUnpack[7][1]];
      iSum[8] += ipWei[ucpUnpack[8][1]];
      iSum[9] += ipWei[ucpUnpack[9][1]];
      iSum[10] += ipWei[ucpUnpack[10][1]];
      iSum[11] += ipWei[ucpUnpack[11][1]];
      iSum[12] += ipWei[ucpUnpack[12][1]];
      iSum[13] += ipWei[ucpUnpack[13][1]];
      iSum[14] += ipWei[ucpUnpack[14][1]];
      iSum[15] += ipWei[ucpUnpack[15][1]];
      iSum[16] += ipWei[ucpUnpack[16][1]];
      iSum[17] += ipWei[ucpUnpack[17][1]];
      iSum[18] += ipWei[ucpUnpack[18][1]];
      iSum[19] += ipWei[ucpUnpack[19][1]];
      iSum[20] += ipWei[ucpUnpack[20][1]];
      iSum[21] += ipWei[ucpUnpack[21][1]];
      iSum[22] += ipWei[ucpUnpack[22][1]];
      iSum[23] += ipWei[ucpUnpack[23][1]];

      ipWei = ipLUT[2];
      iSum[0] += ipWei[ucpUnpack[0][2]];
      iSum[1] += ipWei[ucpUnpack[1][2]];
      iSum[2] += ipWei[ucpUnpack[2][2]];
      iSum[3] += ipWei[ucpUnpack[3][2]];
      iSum[4] += ipWei[ucpUnpack[4][2]];
      iSum[5] += ipWei[ucpUnpack[5][2]];
      iSum[6] += ipWei[ucpUnpack[6][2]];
      iSum[7] += ipWei[ucpUnpack[7][2]];
      iSum[8] += ipWei[ucpUnpack[8][2]];
      iSum[9] += ipWei[ucpUnpack[9][2]];
      iSum[10] += ipWei[ucpUnpack[10][2]];
      iSum[11] += ipWei[ucpUnpack[11][2]];
      iSum[12] += ipWei[ucpUnpack[12][2]];
      iSum[13] += ipWei[ucpUnpack[13][2]];
      iSum[14] += ipWei[ucpUnpack[14][2]];
      iSum[15] += ipWei[ucpUnpack[15][2]];
      iSum[16] += ipWei[ucpUnpack[16][2]];
      iSum[17] += ipWei[ucpUnpack[17][2]];
      iSum[18] += ipWei[ucpUnpack[18][2]];
      iSum[19] += ipWei[ucpUnpack[19][2]];
      iSum[20] += ipWei[ucpUnpack[20][2]];
      iSum[21] += ipWei[ucpUnpack[21][2]];
      iSum[22] += ipWei[ucpUnpack[22][2]];
      iSum[23] += ipWei[ucpUnpack[23][2]];

      // clamp and dump the results

      // no rounding - sums were pre-rounded
      // I commented out the "& 0xFF" because I assume the min/max
      // code will take care of making sure the sums are not negative;
      // if that assumption is not valid, they need to be uncommented
      long lTmp1, lTmp2;

      lTmp1 = iSum[0];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[1];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[2];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[3];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent] = lTmp1;

      lTmp1 = iSum[4];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[5];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[6];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[7];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 4] = lTmp1;

      lTmp1 = iSum[8];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[9];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[10];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[11];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 8] = lTmp1;

      lTmp1 = iSum[12];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[13];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[14];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[15];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 12] = lTmp1;

      lTmp1 = iSum[16];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[17];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[18];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[19];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 16] = lTmp1;

      lTmp1 = iSum[20];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[21];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[22];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[23];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 20] = lTmp1;
    }

    // do the "odd" field
    iR = (iRow+1) / iNFieldDst;

    ucpD = ucpDst[iField1] + (iR * iPitchDst);

    iSubPixel = ipSP[1];
    iPixel = ipP[1];

    for (iComponent = 0; iComponent < iUnpackedComponent; iComponent += VERTICAL_COMPONENTS)
     {
      ipLUT = ipLUTWei[iSubPixel];
      ucpUnpack[0] = ucpUnpacked[iJob][iComponent] + iPixel;
      ucpUnpack[1] = ucpUnpacked[iJob][iComponent + 1] + iPixel;
      ucpUnpack[2] = ucpUnpacked[iJob][iComponent + 2] + iPixel;
      ucpUnpack[3] = ucpUnpacked[iJob][iComponent + 3] + iPixel;
      ucpUnpack[4] = ucpUnpacked[iJob][iComponent + 4] + iPixel;
      ucpUnpack[5] = ucpUnpacked[iJob][iComponent + 5] + iPixel;
      ucpUnpack[6] = ucpUnpacked[iJob][iComponent + 6] + iPixel;
      ucpUnpack[7] = ucpUnpacked[iJob][iComponent + 7] + iPixel;
      ucpUnpack[8] = ucpUnpacked[iJob][iComponent + 8] + iPixel;
      ucpUnpack[9] = ucpUnpacked[iJob][iComponent + 9] + iPixel;
      ucpUnpack[10] = ucpUnpacked[iJob][iComponent + 10] + iPixel;
      ucpUnpack[11] = ucpUnpacked[iJob][iComponent + 11] + iPixel;
      ucpUnpack[12] = ucpUnpacked[iJob][iComponent + 12] + iPixel;
      ucpUnpack[13] = ucpUnpacked[iJob][iComponent + 13] + iPixel;
      ucpUnpack[14] = ucpUnpacked[iJob][iComponent + 14] + iPixel;
      ucpUnpack[15] = ucpUnpacked[iJob][iComponent + 15] + iPixel;
      ucpUnpack[16] = ucpUnpacked[iJob][iComponent + 16] + iPixel;
      ucpUnpack[17] = ucpUnpacked[iJob][iComponent + 17] + iPixel;
      ucpUnpack[18] = ucpUnpacked[iJob][iComponent + 18] + iPixel;
      ucpUnpack[19] = ucpUnpacked[iJob][iComponent + 19] + iPixel;
      ucpUnpack[20] = ucpUnpacked[iJob][iComponent + 20] + iPixel;
      ucpUnpack[21] = ucpUnpacked[iJob][iComponent + 21] + iPixel;
      ucpUnpack[22] = ucpUnpacked[iJob][iComponent + 22] + iPixel;
      ucpUnpack[23] = ucpUnpacked[iJob][iComponent + 23] + iPixel;

      // pre-round the sums
      iSum[0] = iSum[1] = iSum[2] = iSum[3] =
      iSum[4] = iSum[5] = iSum[6] = iSum[7] =
      iSum[8] = iSum[9] = iSum[10] = iSum[11] =
      iSum[12] = iSum[13] = iSum[14] = iSum[15] =
      iSum[16] = iSum[17] = iSum[18] = iSum[19] =
      iSum[20] = iSum[21] = iSum[22] = iSum[23] =
                  iHalfPrecision;

      // do the 3 neighbors
      ipWei = ipLUT[0];
      iSum[0] += ipWei[ucpUnpack[0][0]];
      iSum[1] += ipWei[ucpUnpack[1][0]];
      iSum[2] += ipWei[ucpUnpack[2][0]];
      iSum[3] += ipWei[ucpUnpack[3][0]];
      iSum[4] += ipWei[ucpUnpack[4][0]];
      iSum[5] += ipWei[ucpUnpack[5][0]];
      iSum[6] += ipWei[ucpUnpack[6][0]];
      iSum[7] += ipWei[ucpUnpack[7][0]];
      iSum[8] += ipWei[ucpUnpack[8][0]];
      iSum[9] += ipWei[ucpUnpack[9][0]];
      iSum[10] += ipWei[ucpUnpack[10][0]];
      iSum[11] += ipWei[ucpUnpack[11][0]];
      iSum[12] += ipWei[ucpUnpack[12][0]];
      iSum[13] += ipWei[ucpUnpack[13][0]];
      iSum[14] += ipWei[ucpUnpack[14][0]];
      iSum[15] += ipWei[ucpUnpack[15][0]];
      iSum[16] += ipWei[ucpUnpack[16][0]];
      iSum[17] += ipWei[ucpUnpack[17][0]];
      iSum[18] += ipWei[ucpUnpack[18][0]];
      iSum[19] += ipWei[ucpUnpack[19][0]];
      iSum[20] += ipWei[ucpUnpack[20][0]];
      iSum[21] += ipWei[ucpUnpack[21][0]];
      iSum[22] += ipWei[ucpUnpack[22][0]];
      iSum[23] += ipWei[ucpUnpack[23][0]];

      ipWei = ipLUT[1];
      iSum[0] += ipWei[ucpUnpack[0][1]];
      iSum[1] += ipWei[ucpUnpack[1][1]];
      iSum[2] += ipWei[ucpUnpack[2][1]];
      iSum[3] += ipWei[ucpUnpack[3][1]];
      iSum[4] += ipWei[ucpUnpack[4][1]];
      iSum[5] += ipWei[ucpUnpack[5][1]];
      iSum[6] += ipWei[ucpUnpack[6][1]];
      iSum[7] += ipWei[ucpUnpack[7][1]];
      iSum[8] += ipWei[ucpUnpack[8][1]];
      iSum[9] += ipWei[ucpUnpack[9][1]];
      iSum[10] += ipWei[ucpUnpack[10][1]];
      iSum[11] += ipWei[ucpUnpack[11][1]];
      iSum[12] += ipWei[ucpUnpack[12][1]];
      iSum[13] += ipWei[ucpUnpack[13][1]];
      iSum[14] += ipWei[ucpUnpack[14][1]];
      iSum[15] += ipWei[ucpUnpack[15][1]];
      iSum[16] += ipWei[ucpUnpack[16][1]];
      iSum[17] += ipWei[ucpUnpack[17][1]];
      iSum[18] += ipWei[ucpUnpack[18][1]];
      iSum[19] += ipWei[ucpUnpack[19][1]];
      iSum[20] += ipWei[ucpUnpack[20][1]];
      iSum[21] += ipWei[ucpUnpack[21][1]];
      iSum[22] += ipWei[ucpUnpack[22][1]];
      iSum[23] += ipWei[ucpUnpack[23][1]];

      ipWei = ipLUT[2];
      iSum[0] += ipWei[ucpUnpack[0][2]];
      iSum[1] += ipWei[ucpUnpack[1][2]];
      iSum[2] += ipWei[ucpUnpack[2][2]];
      iSum[3] += ipWei[ucpUnpack[3][2]];
      iSum[4] += ipWei[ucpUnpack[4][2]];
      iSum[5] += ipWei[ucpUnpack[5][2]];
      iSum[6] += ipWei[ucpUnpack[6][2]];
      iSum[7] += ipWei[ucpUnpack[7][2]];
      iSum[8] += ipWei[ucpUnpack[8][2]];
      iSum[9] += ipWei[ucpUnpack[9][2]];
      iSum[10] += ipWei[ucpUnpack[10][2]];
      iSum[11] += ipWei[ucpUnpack[11][2]];
      iSum[12] += ipWei[ucpUnpack[12][2]];
      iSum[13] += ipWei[ucpUnpack[13][2]];
      iSum[14] += ipWei[ucpUnpack[14][2]];
      iSum[15] += ipWei[ucpUnpack[15][2]];
      iSum[16] += ipWei[ucpUnpack[16][2]];
      iSum[17] += ipWei[ucpUnpack[17][2]];
      iSum[18] += ipWei[ucpUnpack[18][2]];
      iSum[19] += ipWei[ucpUnpack[19][2]];
      iSum[20] += ipWei[ucpUnpack[20][2]];
      iSum[21] += ipWei[ucpUnpack[21][2]];
      iSum[22] += ipWei[ucpUnpack[22][2]];
      iSum[23] += ipWei[ucpUnpack[23][2]];

      // clamp and dump the results

      // no rounding - sums were pre-rounded
      // I commented out the "& 0xFF" because I assume the min/max
      // code will take care of making sure the sums are not negative;
      // if that assumption is not valid, they need to be uncommented
      long lTmp1, lTmp2;

      lTmp1 = iSum[0];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[1];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[2];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[3];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent] = lTmp1;

      lTmp1 = iSum[4];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[5];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[6];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[7];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 4] = lTmp1;

      lTmp1 = iSum[8];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[9];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[10];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[11];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 8] = lTmp1;

      lTmp1 = iSum[12];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[13];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[14];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[15];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 12] = lTmp1;

      lTmp1 = iSum[16];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[17];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[18];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[19];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 16] = lTmp1;

      lTmp1 = iSum[20];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[21];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[22];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[23];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 20] = lTmp1;
     }
    ipSP += 2;
    ipP += 2;
   }
}

void CAspectRatio::RenderV_YUV_5 (MTI_UINT8 *ucpDst[2], int &iRow, int iStopDstLocal,
		int iJob)
/*
	Do VERTICAL rendering with 5 neighbors
*/
{
  MTI_UINT8 *ucpD;
  int iField, iR, *ipSP, *ipP, iPixel, iNei;
  int **ipLUT, iSubPixel;
  int iSum[VERTICAL_COMPONENTS], *ipWei;
  MTI_UINT8 *ucpUnpack[VERTICAL_COMPONENTS];
  int iComponent;

  // check for errors
  if ((iRow & 0x1) || (iStopDstLocal & 0x1))
    return;

  int iField0, iField1;

  if (iNFieldDst == 2)
   {
    iField0 = 0;
    iField1 = 1;
   }
  else
   {
    iField0 = 0;
    iField1 = 0;
   }


  ipSP = ipLUTSubPixel + iRow;
  ipP = ipLUTPixel + iRow;
  for (; iRow < iStopDstLocal; iRow+=2)
   {
    // do the "even" field
    iR = iRow / iNFieldDst;

    ucpD = ucpDst[iField0] + (iR * iPitchDst);

    iSubPixel = ipSP[0];
    iPixel = ipP[0];

    for (iComponent = 0; iComponent < iUnpackedComponent; iComponent += VERTICAL_COMPONENTS)
     {
      ipLUT = ipLUTWei[iSubPixel];
      ucpUnpack[0] = ucpUnpacked[iJob][iComponent] + iPixel;
      ucpUnpack[1] = ucpUnpacked[iJob][iComponent + 1] + iPixel;
      ucpUnpack[2] = ucpUnpacked[iJob][iComponent + 2] + iPixel;
      ucpUnpack[3] = ucpUnpacked[iJob][iComponent + 3] + iPixel;
      ucpUnpack[4] = ucpUnpacked[iJob][iComponent + 4] + iPixel;
      ucpUnpack[5] = ucpUnpacked[iJob][iComponent + 5] + iPixel;
      ucpUnpack[6] = ucpUnpacked[iJob][iComponent + 6] + iPixel;
      ucpUnpack[7] = ucpUnpacked[iJob][iComponent + 7] + iPixel;
      ucpUnpack[8] = ucpUnpacked[iJob][iComponent + 8] + iPixel;
      ucpUnpack[9] = ucpUnpacked[iJob][iComponent + 9] + iPixel;
      ucpUnpack[10] = ucpUnpacked[iJob][iComponent + 10] + iPixel;
      ucpUnpack[11] = ucpUnpacked[iJob][iComponent + 11] + iPixel;
      ucpUnpack[12] = ucpUnpacked[iJob][iComponent + 12] + iPixel;
      ucpUnpack[13] = ucpUnpacked[iJob][iComponent + 13] + iPixel;
      ucpUnpack[14] = ucpUnpacked[iJob][iComponent + 14] + iPixel;
      ucpUnpack[15] = ucpUnpacked[iJob][iComponent + 15] + iPixel;
      ucpUnpack[16] = ucpUnpacked[iJob][iComponent + 16] + iPixel;
      ucpUnpack[17] = ucpUnpacked[iJob][iComponent + 17] + iPixel;
      ucpUnpack[18] = ucpUnpacked[iJob][iComponent + 18] + iPixel;
      ucpUnpack[19] = ucpUnpacked[iJob][iComponent + 19] + iPixel;
      ucpUnpack[20] = ucpUnpacked[iJob][iComponent + 20] + iPixel;
      ucpUnpack[21] = ucpUnpacked[iJob][iComponent + 21] + iPixel;
      ucpUnpack[22] = ucpUnpacked[iJob][iComponent + 22] + iPixel;
      ucpUnpack[23] = ucpUnpacked[iJob][iComponent + 23] + iPixel;

      // pre-round the sums
      iSum[0] = iSum[1] = iSum[2] = iSum[3] =
      iSum[4] = iSum[5] = iSum[6] = iSum[7] =
      iSum[8] = iSum[9] = iSum[10] = iSum[11] =
      iSum[12] = iSum[13] = iSum[14] = iSum[15] =
      iSum[16] = iSum[17] = iSum[18] = iSum[19] =
      iSum[20] = iSum[21] = iSum[22] = iSum[23] =
                  iHalfPrecision;

      // do the 5 neighbors
      ipWei = ipLUT[0];
      iSum[0] += ipWei[ucpUnpack[0][0]];
      iSum[1] += ipWei[ucpUnpack[1][0]];
      iSum[2] += ipWei[ucpUnpack[2][0]];
      iSum[3] += ipWei[ucpUnpack[3][0]];
      iSum[4] += ipWei[ucpUnpack[4][0]];
      iSum[5] += ipWei[ucpUnpack[5][0]];
      iSum[6] += ipWei[ucpUnpack[6][0]];
      iSum[7] += ipWei[ucpUnpack[7][0]];
      iSum[8] += ipWei[ucpUnpack[8][0]];
      iSum[9] += ipWei[ucpUnpack[9][0]];
      iSum[10] += ipWei[ucpUnpack[10][0]];
      iSum[11] += ipWei[ucpUnpack[11][0]];
      iSum[12] += ipWei[ucpUnpack[12][0]];
      iSum[13] += ipWei[ucpUnpack[13][0]];
      iSum[14] += ipWei[ucpUnpack[14][0]];
      iSum[15] += ipWei[ucpUnpack[15][0]];
      iSum[16] += ipWei[ucpUnpack[16][0]];
      iSum[17] += ipWei[ucpUnpack[17][0]];
      iSum[18] += ipWei[ucpUnpack[18][0]];
      iSum[19] += ipWei[ucpUnpack[19][0]];
      iSum[20] += ipWei[ucpUnpack[20][0]];
      iSum[21] += ipWei[ucpUnpack[21][0]];
      iSum[22] += ipWei[ucpUnpack[22][0]];
      iSum[23] += ipWei[ucpUnpack[23][0]];

      ipWei = ipLUT[1];
      iSum[0] += ipWei[ucpUnpack[0][1]];
      iSum[1] += ipWei[ucpUnpack[1][1]];
      iSum[2] += ipWei[ucpUnpack[2][1]];
      iSum[3] += ipWei[ucpUnpack[3][1]];
      iSum[4] += ipWei[ucpUnpack[4][1]];
      iSum[5] += ipWei[ucpUnpack[5][1]];
      iSum[6] += ipWei[ucpUnpack[6][1]];
      iSum[7] += ipWei[ucpUnpack[7][1]];
      iSum[8] += ipWei[ucpUnpack[8][1]];
      iSum[9] += ipWei[ucpUnpack[9][1]];
      iSum[10] += ipWei[ucpUnpack[10][1]];
      iSum[11] += ipWei[ucpUnpack[11][1]];
      iSum[12] += ipWei[ucpUnpack[12][1]];
      iSum[13] += ipWei[ucpUnpack[13][1]];
      iSum[14] += ipWei[ucpUnpack[14][1]];
      iSum[15] += ipWei[ucpUnpack[15][1]];
      iSum[16] += ipWei[ucpUnpack[16][1]];
      iSum[17] += ipWei[ucpUnpack[17][1]];
      iSum[18] += ipWei[ucpUnpack[18][1]];
      iSum[19] += ipWei[ucpUnpack[19][1]];
      iSum[20] += ipWei[ucpUnpack[20][1]];
      iSum[21] += ipWei[ucpUnpack[21][1]];
      iSum[22] += ipWei[ucpUnpack[22][1]];
      iSum[23] += ipWei[ucpUnpack[23][1]];

      ipWei = ipLUT[2];
      iSum[0] += ipWei[ucpUnpack[0][2]];
      iSum[1] += ipWei[ucpUnpack[1][2]];
      iSum[2] += ipWei[ucpUnpack[2][2]];
      iSum[3] += ipWei[ucpUnpack[3][2]];
      iSum[4] += ipWei[ucpUnpack[4][2]];
      iSum[5] += ipWei[ucpUnpack[5][2]];
      iSum[6] += ipWei[ucpUnpack[6][2]];
      iSum[7] += ipWei[ucpUnpack[7][2]];
      iSum[8] += ipWei[ucpUnpack[8][2]];
      iSum[9] += ipWei[ucpUnpack[9][2]];
      iSum[10] += ipWei[ucpUnpack[10][2]];
      iSum[11] += ipWei[ucpUnpack[11][2]];
      iSum[12] += ipWei[ucpUnpack[12][2]];
      iSum[13] += ipWei[ucpUnpack[13][2]];
      iSum[14] += ipWei[ucpUnpack[14][2]];
      iSum[15] += ipWei[ucpUnpack[15][2]];
      iSum[16] += ipWei[ucpUnpack[16][2]];
      iSum[17] += ipWei[ucpUnpack[17][2]];
      iSum[18] += ipWei[ucpUnpack[18][2]];
      iSum[19] += ipWei[ucpUnpack[19][2]];
      iSum[20] += ipWei[ucpUnpack[20][2]];
      iSum[21] += ipWei[ucpUnpack[21][2]];
      iSum[22] += ipWei[ucpUnpack[22][2]];
      iSum[23] += ipWei[ucpUnpack[23][2]];

      ipWei = ipLUT[3];
      iSum[0] += ipWei[ucpUnpack[0][3]];
      iSum[1] += ipWei[ucpUnpack[1][3]];
      iSum[2] += ipWei[ucpUnpack[2][3]];
      iSum[3] += ipWei[ucpUnpack[3][3]];
      iSum[4] += ipWei[ucpUnpack[4][3]];
      iSum[5] += ipWei[ucpUnpack[5][3]];
      iSum[6] += ipWei[ucpUnpack[6][3]];
      iSum[7] += ipWei[ucpUnpack[7][3]];
      iSum[8] += ipWei[ucpUnpack[8][3]];
      iSum[9] += ipWei[ucpUnpack[9][3]];
      iSum[10] += ipWei[ucpUnpack[10][3]];
      iSum[11] += ipWei[ucpUnpack[11][3]];
      iSum[12] += ipWei[ucpUnpack[12][3]];
      iSum[13] += ipWei[ucpUnpack[13][3]];
      iSum[14] += ipWei[ucpUnpack[14][3]];
      iSum[15] += ipWei[ucpUnpack[15][3]];
      iSum[16] += ipWei[ucpUnpack[16][3]];
      iSum[17] += ipWei[ucpUnpack[17][3]];
      iSum[18] += ipWei[ucpUnpack[18][3]];
      iSum[19] += ipWei[ucpUnpack[19][3]];
      iSum[20] += ipWei[ucpUnpack[20][3]];
      iSum[21] += ipWei[ucpUnpack[21][3]];
      iSum[22] += ipWei[ucpUnpack[22][3]];
      iSum[23] += ipWei[ucpUnpack[23][3]];

      ipWei = ipLUT[4];
      iSum[0] += ipWei[ucpUnpack[0][4]];
      iSum[1] += ipWei[ucpUnpack[1][4]];
      iSum[2] += ipWei[ucpUnpack[2][4]];
      iSum[3] += ipWei[ucpUnpack[3][4]];
      iSum[4] += ipWei[ucpUnpack[4][4]];
      iSum[5] += ipWei[ucpUnpack[5][4]];
      iSum[6] += ipWei[ucpUnpack[6][4]];
      iSum[7] += ipWei[ucpUnpack[7][4]];
      iSum[8] += ipWei[ucpUnpack[8][4]];
      iSum[9] += ipWei[ucpUnpack[9][4]];
      iSum[10] += ipWei[ucpUnpack[10][4]];
      iSum[11] += ipWei[ucpUnpack[11][4]];
      iSum[12] += ipWei[ucpUnpack[12][4]];
      iSum[13] += ipWei[ucpUnpack[13][4]];
      iSum[14] += ipWei[ucpUnpack[14][4]];
      iSum[15] += ipWei[ucpUnpack[15][4]];
      iSum[16] += ipWei[ucpUnpack[16][4]];
      iSum[17] += ipWei[ucpUnpack[17][4]];
      iSum[18] += ipWei[ucpUnpack[18][4]];
      iSum[19] += ipWei[ucpUnpack[19][4]];
      iSum[20] += ipWei[ucpUnpack[20][4]];
      iSum[21] += ipWei[ucpUnpack[21][4]];
      iSum[22] += ipWei[ucpUnpack[22][4]];
      iSum[23] += ipWei[ucpUnpack[23][4]];

      // clamp and dump the results

      // no rounding - sums were pre-rounded
      // I commented out the "& 0xFF" because I assume the min/max
      // code will take care of making sure the sums are not negative;
      // if that assumption is not valid, they need to be uncommented
      long lTmp1, lTmp2;

      lTmp1 = iSum[0];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[1];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[2];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[3];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent] = lTmp1;

      lTmp1 = iSum[4];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[5];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[6];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[7];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 4] = lTmp1;

      lTmp1 = iSum[8];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[9];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[10];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[11];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 8] = lTmp1;

      lTmp1 = iSum[12];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[13];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[14];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[15];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 12] = lTmp1;

      lTmp1 = iSum[16];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[17];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[18];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[19];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 16] = lTmp1;

      lTmp1 = iSum[20];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[21];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[22];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[23];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 20] = lTmp1;
    }

    // do the "odd" field
    iR = (iRow+1) / iNFieldDst;

    ucpD = ucpDst[iField1] + (iR * iPitchDst);

    iSubPixel = ipSP[1];
    iPixel = ipP[1];

    for (iComponent = 0; iComponent < iUnpackedComponent; iComponent += VERTICAL_COMPONENTS)
     {
      ipLUT = ipLUTWei[iSubPixel];
      ucpUnpack[0] = ucpUnpacked[iJob][iComponent] + iPixel;
      ucpUnpack[1] = ucpUnpacked[iJob][iComponent + 1] + iPixel;
      ucpUnpack[2] = ucpUnpacked[iJob][iComponent + 2] + iPixel;
      ucpUnpack[3] = ucpUnpacked[iJob][iComponent + 3] + iPixel;
      ucpUnpack[4] = ucpUnpacked[iJob][iComponent + 4] + iPixel;
      ucpUnpack[5] = ucpUnpacked[iJob][iComponent + 5] + iPixel;
      ucpUnpack[6] = ucpUnpacked[iJob][iComponent + 6] + iPixel;
      ucpUnpack[7] = ucpUnpacked[iJob][iComponent + 7] + iPixel;
      ucpUnpack[8] = ucpUnpacked[iJob][iComponent + 8] + iPixel;
      ucpUnpack[9] = ucpUnpacked[iJob][iComponent + 9] + iPixel;
      ucpUnpack[10] = ucpUnpacked[iJob][iComponent + 10] + iPixel;
      ucpUnpack[11] = ucpUnpacked[iJob][iComponent + 11] + iPixel;
      ucpUnpack[12] = ucpUnpacked[iJob][iComponent + 12] + iPixel;
      ucpUnpack[13] = ucpUnpacked[iJob][iComponent + 13] + iPixel;
      ucpUnpack[14] = ucpUnpacked[iJob][iComponent + 14] + iPixel;
      ucpUnpack[15] = ucpUnpacked[iJob][iComponent + 15] + iPixel;
      ucpUnpack[16] = ucpUnpacked[iJob][iComponent + 16] + iPixel;
      ucpUnpack[17] = ucpUnpacked[iJob][iComponent + 17] + iPixel;
      ucpUnpack[18] = ucpUnpacked[iJob][iComponent + 18] + iPixel;
      ucpUnpack[19] = ucpUnpacked[iJob][iComponent + 19] + iPixel;
      ucpUnpack[20] = ucpUnpacked[iJob][iComponent + 20] + iPixel;
      ucpUnpack[21] = ucpUnpacked[iJob][iComponent + 21] + iPixel;
      ucpUnpack[22] = ucpUnpacked[iJob][iComponent + 22] + iPixel;
      ucpUnpack[23] = ucpUnpacked[iJob][iComponent + 23] + iPixel;

      // pre-round the sums
      iSum[0] = iSum[1] = iSum[2] = iSum[3] =
      iSum[4] = iSum[5] = iSum[6] = iSum[7] =
      iSum[8] = iSum[9] = iSum[10] = iSum[11] =
      iSum[12] = iSum[13] = iSum[14] = iSum[15] =
      iSum[16] = iSum[17] = iSum[18] = iSum[19] =
      iSum[20] = iSum[21] = iSum[22] = iSum[23] =
                  iHalfPrecision;

      // do the 5 neighbors
      ipWei = ipLUT[0];
      iSum[0] += ipWei[ucpUnpack[0][0]];
      iSum[1] += ipWei[ucpUnpack[1][0]];
      iSum[2] += ipWei[ucpUnpack[2][0]];
      iSum[3] += ipWei[ucpUnpack[3][0]];
      iSum[4] += ipWei[ucpUnpack[4][0]];
      iSum[5] += ipWei[ucpUnpack[5][0]];
      iSum[6] += ipWei[ucpUnpack[6][0]];
      iSum[7] += ipWei[ucpUnpack[7][0]];
      iSum[8] += ipWei[ucpUnpack[8][0]];
      iSum[9] += ipWei[ucpUnpack[9][0]];
      iSum[10] += ipWei[ucpUnpack[10][0]];
      iSum[11] += ipWei[ucpUnpack[11][0]];
      iSum[12] += ipWei[ucpUnpack[12][0]];
      iSum[13] += ipWei[ucpUnpack[13][0]];
      iSum[14] += ipWei[ucpUnpack[14][0]];
      iSum[15] += ipWei[ucpUnpack[15][0]];
      iSum[16] += ipWei[ucpUnpack[16][0]];
      iSum[17] += ipWei[ucpUnpack[17][0]];
      iSum[18] += ipWei[ucpUnpack[18][0]];
      iSum[19] += ipWei[ucpUnpack[19][0]];
      iSum[20] += ipWei[ucpUnpack[20][0]];
      iSum[21] += ipWei[ucpUnpack[21][0]];
      iSum[22] += ipWei[ucpUnpack[22][0]];
      iSum[23] += ipWei[ucpUnpack[23][0]];

      ipWei = ipLUT[1];
      iSum[0] += ipWei[ucpUnpack[0][1]];
      iSum[1] += ipWei[ucpUnpack[1][1]];
      iSum[2] += ipWei[ucpUnpack[2][1]];
      iSum[3] += ipWei[ucpUnpack[3][1]];
      iSum[4] += ipWei[ucpUnpack[4][1]];
      iSum[5] += ipWei[ucpUnpack[5][1]];
      iSum[6] += ipWei[ucpUnpack[6][1]];
      iSum[7] += ipWei[ucpUnpack[7][1]];
      iSum[8] += ipWei[ucpUnpack[8][1]];
      iSum[9] += ipWei[ucpUnpack[9][1]];
      iSum[10] += ipWei[ucpUnpack[10][1]];
      iSum[11] += ipWei[ucpUnpack[11][1]];
      iSum[12] += ipWei[ucpUnpack[12][1]];
      iSum[13] += ipWei[ucpUnpack[13][1]];
      iSum[14] += ipWei[ucpUnpack[14][1]];
      iSum[15] += ipWei[ucpUnpack[15][1]];
      iSum[16] += ipWei[ucpUnpack[16][1]];
      iSum[17] += ipWei[ucpUnpack[17][1]];
      iSum[18] += ipWei[ucpUnpack[18][1]];
      iSum[19] += ipWei[ucpUnpack[19][1]];
      iSum[20] += ipWei[ucpUnpack[20][1]];
      iSum[21] += ipWei[ucpUnpack[21][1]];
      iSum[22] += ipWei[ucpUnpack[22][1]];
      iSum[23] += ipWei[ucpUnpack[23][1]];

      ipWei = ipLUT[2];
      iSum[0] += ipWei[ucpUnpack[0][2]];
      iSum[1] += ipWei[ucpUnpack[1][2]];
      iSum[2] += ipWei[ucpUnpack[2][2]];
      iSum[3] += ipWei[ucpUnpack[3][2]];
      iSum[4] += ipWei[ucpUnpack[4][2]];
      iSum[5] += ipWei[ucpUnpack[5][2]];
      iSum[6] += ipWei[ucpUnpack[6][2]];
      iSum[7] += ipWei[ucpUnpack[7][2]];
      iSum[8] += ipWei[ucpUnpack[8][2]];
      iSum[9] += ipWei[ucpUnpack[9][2]];
      iSum[10] += ipWei[ucpUnpack[10][2]];
      iSum[11] += ipWei[ucpUnpack[11][2]];
      iSum[12] += ipWei[ucpUnpack[12][2]];
      iSum[13] += ipWei[ucpUnpack[13][2]];
      iSum[14] += ipWei[ucpUnpack[14][2]];
      iSum[15] += ipWei[ucpUnpack[15][2]];
      iSum[16] += ipWei[ucpUnpack[16][2]];
      iSum[17] += ipWei[ucpUnpack[17][2]];
      iSum[18] += ipWei[ucpUnpack[18][2]];
      iSum[19] += ipWei[ucpUnpack[19][2]];
      iSum[20] += ipWei[ucpUnpack[20][2]];
      iSum[21] += ipWei[ucpUnpack[21][2]];
      iSum[22] += ipWei[ucpUnpack[22][2]];
      iSum[23] += ipWei[ucpUnpack[23][2]];

      ipWei = ipLUT[3];
      iSum[0] += ipWei[ucpUnpack[0][3]];
      iSum[1] += ipWei[ucpUnpack[1][3]];
      iSum[2] += ipWei[ucpUnpack[2][3]];
      iSum[3] += ipWei[ucpUnpack[3][3]];
      iSum[4] += ipWei[ucpUnpack[4][3]];
      iSum[5] += ipWei[ucpUnpack[5][3]];
      iSum[6] += ipWei[ucpUnpack[6][3]];
      iSum[7] += ipWei[ucpUnpack[7][3]];
      iSum[8] += ipWei[ucpUnpack[8][3]];
      iSum[9] += ipWei[ucpUnpack[9][3]];
      iSum[10] += ipWei[ucpUnpack[10][3]];
      iSum[11] += ipWei[ucpUnpack[11][3]];
      iSum[12] += ipWei[ucpUnpack[12][3]];
      iSum[13] += ipWei[ucpUnpack[13][3]];
      iSum[14] += ipWei[ucpUnpack[14][3]];
      iSum[15] += ipWei[ucpUnpack[15][3]];
      iSum[16] += ipWei[ucpUnpack[16][3]];
      iSum[17] += ipWei[ucpUnpack[17][3]];
      iSum[18] += ipWei[ucpUnpack[18][3]];
      iSum[19] += ipWei[ucpUnpack[19][3]];
      iSum[20] += ipWei[ucpUnpack[20][3]];
      iSum[21] += ipWei[ucpUnpack[21][3]];
      iSum[22] += ipWei[ucpUnpack[22][3]];
      iSum[23] += ipWei[ucpUnpack[23][3]];

      ipWei = ipLUT[4];
      iSum[0] += ipWei[ucpUnpack[0][4]];
      iSum[1] += ipWei[ucpUnpack[1][4]];
      iSum[2] += ipWei[ucpUnpack[2][4]];
      iSum[3] += ipWei[ucpUnpack[3][4]];
      iSum[4] += ipWei[ucpUnpack[4][4]];
      iSum[5] += ipWei[ucpUnpack[5][4]];
      iSum[6] += ipWei[ucpUnpack[6][4]];
      iSum[7] += ipWei[ucpUnpack[7][4]];
      iSum[8] += ipWei[ucpUnpack[8][4]];
      iSum[9] += ipWei[ucpUnpack[9][4]];
      iSum[10] += ipWei[ucpUnpack[10][4]];
      iSum[11] += ipWei[ucpUnpack[11][4]];
      iSum[12] += ipWei[ucpUnpack[12][4]];
      iSum[13] += ipWei[ucpUnpack[13][4]];
      iSum[14] += ipWei[ucpUnpack[14][4]];
      iSum[15] += ipWei[ucpUnpack[15][4]];
      iSum[16] += ipWei[ucpUnpack[16][4]];
      iSum[17] += ipWei[ucpUnpack[17][4]];
      iSum[18] += ipWei[ucpUnpack[18][4]];
      iSum[19] += ipWei[ucpUnpack[19][4]];
      iSum[20] += ipWei[ucpUnpack[20][4]];
      iSum[21] += ipWei[ucpUnpack[21][4]];
      iSum[22] += ipWei[ucpUnpack[22][4]];
      iSum[23] += ipWei[ucpUnpack[23][4]];

      // clamp and dump the results

      // no rounding - sums were pre-rounded
      // I commented out the "& 0xFF" because I assume the min/max
      // code will take care of making sure the sums are not negative;
      // if that assumption is not valid, they need to be uncommented
      long lTmp1, lTmp2;

      lTmp1 = iSum[0];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[1];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[2];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[3];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent] = lTmp1;

      lTmp1 = iSum[4];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[5];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[6];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[7];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 4] = lTmp1;

      lTmp1 = iSum[8];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[9];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[10];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[11];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 8] = lTmp1;

      lTmp1 = iSum[12];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[13];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[14];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[15];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 12] = lTmp1;

      lTmp1 = iSum[16];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[17];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[18];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[19];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 16] = lTmp1;

      lTmp1 = iSum[20];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[21];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[22];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[23];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 20] = lTmp1;
     }
    ipSP += 2;
    ipP += 2;
   }
}

void CAspectRatio::RenderV_YUV_7 (MTI_UINT8 *ucpDst[2], int &iRow, int iStopDstLocal,
		int iJob)
/*
	Do VERTICAL rendering with 7 neighbors
*/
{
  MTI_UINT8 *ucpD;
  int iField, iR, *ipSP, *ipP, iPixel, iNei;
  int **ipLUT, iSubPixel;
  int iSum[VERTICAL_COMPONENTS], *ipWei;
  MTI_UINT8 *ucpUnpack[VERTICAL_COMPONENTS];
  int iComponent;

  // check for errors
  if ((iRow & 0x1) || (iStopDstLocal & 0x1))
    return;

  int iField0, iField1;

  if (iNFieldDst == 2)
   {
    iField0 = 0;
    iField1 = 1;
   }
  else
   {
    iField0 = 0;
    iField1 = 0;
   }


  ipSP = ipLUTSubPixel + iRow;
  ipP = ipLUTPixel + iRow;
  for (; iRow < iStopDstLocal; iRow+=2)
   {
    // do the "even" field
    iR = iRow / iNFieldDst;

    ucpD = ucpDst[iField0] + (iR * iPitchDst);

    iSubPixel = ipSP[0];
    iPixel = ipP[0];

    for (iComponent = 0; iComponent < iUnpackedComponent; iComponent += VERTICAL_COMPONENTS)
     {
      ipLUT = ipLUTWei[iSubPixel];
      ucpUnpack[0] = ucpUnpacked[iJob][iComponent] + iPixel;
      ucpUnpack[1] = ucpUnpacked[iJob][iComponent + 1] + iPixel;
      ucpUnpack[2] = ucpUnpacked[iJob][iComponent + 2] + iPixel;
      ucpUnpack[3] = ucpUnpacked[iJob][iComponent + 3] + iPixel;
      ucpUnpack[4] = ucpUnpacked[iJob][iComponent + 4] + iPixel;
      ucpUnpack[5] = ucpUnpacked[iJob][iComponent + 5] + iPixel;
      ucpUnpack[6] = ucpUnpacked[iJob][iComponent + 6] + iPixel;
      ucpUnpack[7] = ucpUnpacked[iJob][iComponent + 7] + iPixel;
      ucpUnpack[8] = ucpUnpacked[iJob][iComponent + 8] + iPixel;
      ucpUnpack[9] = ucpUnpacked[iJob][iComponent + 9] + iPixel;
      ucpUnpack[10] = ucpUnpacked[iJob][iComponent + 10] + iPixel;
      ucpUnpack[11] = ucpUnpacked[iJob][iComponent + 11] + iPixel;
      ucpUnpack[12] = ucpUnpacked[iJob][iComponent + 12] + iPixel;
      ucpUnpack[13] = ucpUnpacked[iJob][iComponent + 13] + iPixel;
      ucpUnpack[14] = ucpUnpacked[iJob][iComponent + 14] + iPixel;
      ucpUnpack[15] = ucpUnpacked[iJob][iComponent + 15] + iPixel;
      ucpUnpack[16] = ucpUnpacked[iJob][iComponent + 16] + iPixel;
      ucpUnpack[17] = ucpUnpacked[iJob][iComponent + 17] + iPixel;
      ucpUnpack[18] = ucpUnpacked[iJob][iComponent + 18] + iPixel;
      ucpUnpack[19] = ucpUnpacked[iJob][iComponent + 19] + iPixel;
      ucpUnpack[20] = ucpUnpacked[iJob][iComponent + 20] + iPixel;
      ucpUnpack[21] = ucpUnpacked[iJob][iComponent + 21] + iPixel;
      ucpUnpack[22] = ucpUnpacked[iJob][iComponent + 22] + iPixel;
      ucpUnpack[23] = ucpUnpacked[iJob][iComponent + 23] + iPixel;

      // pre-round the sums
      iSum[0] = iSum[1] = iSum[2] = iSum[3] =
      iSum[4] = iSum[5] = iSum[6] = iSum[7] =
      iSum[8] = iSum[9] = iSum[10] = iSum[11] =
      iSum[12] = iSum[13] = iSum[14] = iSum[15] =
      iSum[16] = iSum[17] = iSum[18] = iSum[19] =
      iSum[20] = iSum[21] = iSum[22] = iSum[23] =
                  iHalfPrecision;

      // do the 7 neighbors
      ipWei = ipLUT[0];
      iSum[0] += ipWei[ucpUnpack[0][0]];
      iSum[1] += ipWei[ucpUnpack[1][0]];
      iSum[2] += ipWei[ucpUnpack[2][0]];
      iSum[3] += ipWei[ucpUnpack[3][0]];
      iSum[4] += ipWei[ucpUnpack[4][0]];
      iSum[5] += ipWei[ucpUnpack[5][0]];
      iSum[6] += ipWei[ucpUnpack[6][0]];
      iSum[7] += ipWei[ucpUnpack[7][0]];
      iSum[8] += ipWei[ucpUnpack[8][0]];
      iSum[9] += ipWei[ucpUnpack[9][0]];
      iSum[10] += ipWei[ucpUnpack[10][0]];
      iSum[11] += ipWei[ucpUnpack[11][0]];
      iSum[12] += ipWei[ucpUnpack[12][0]];
      iSum[13] += ipWei[ucpUnpack[13][0]];
      iSum[14] += ipWei[ucpUnpack[14][0]];
      iSum[15] += ipWei[ucpUnpack[15][0]];
      iSum[16] += ipWei[ucpUnpack[16][0]];
      iSum[17] += ipWei[ucpUnpack[17][0]];
      iSum[18] += ipWei[ucpUnpack[18][0]];
      iSum[19] += ipWei[ucpUnpack[19][0]];
      iSum[20] += ipWei[ucpUnpack[20][0]];
      iSum[21] += ipWei[ucpUnpack[21][0]];
      iSum[22] += ipWei[ucpUnpack[22][0]];
      iSum[23] += ipWei[ucpUnpack[23][0]];

      ipWei = ipLUT[1];
      iSum[0] += ipWei[ucpUnpack[0][1]];
      iSum[1] += ipWei[ucpUnpack[1][1]];
      iSum[2] += ipWei[ucpUnpack[2][1]];
      iSum[3] += ipWei[ucpUnpack[3][1]];
      iSum[4] += ipWei[ucpUnpack[4][1]];
      iSum[5] += ipWei[ucpUnpack[5][1]];
      iSum[6] += ipWei[ucpUnpack[6][1]];
      iSum[7] += ipWei[ucpUnpack[7][1]];
      iSum[8] += ipWei[ucpUnpack[8][1]];
      iSum[9] += ipWei[ucpUnpack[9][1]];
      iSum[10] += ipWei[ucpUnpack[10][1]];
      iSum[11] += ipWei[ucpUnpack[11][1]];
      iSum[12] += ipWei[ucpUnpack[12][1]];
      iSum[13] += ipWei[ucpUnpack[13][1]];
      iSum[14] += ipWei[ucpUnpack[14][1]];
      iSum[15] += ipWei[ucpUnpack[15][1]];
      iSum[16] += ipWei[ucpUnpack[16][1]];
      iSum[17] += ipWei[ucpUnpack[17][1]];
      iSum[18] += ipWei[ucpUnpack[18][1]];
      iSum[19] += ipWei[ucpUnpack[19][1]];
      iSum[20] += ipWei[ucpUnpack[20][1]];
      iSum[21] += ipWei[ucpUnpack[21][1]];
      iSum[22] += ipWei[ucpUnpack[22][1]];
      iSum[23] += ipWei[ucpUnpack[23][1]];

      ipWei = ipLUT[2];
      iSum[0] += ipWei[ucpUnpack[0][2]];
      iSum[1] += ipWei[ucpUnpack[1][2]];
      iSum[2] += ipWei[ucpUnpack[2][2]];
      iSum[3] += ipWei[ucpUnpack[3][2]];
      iSum[4] += ipWei[ucpUnpack[4][2]];
      iSum[5] += ipWei[ucpUnpack[5][2]];
      iSum[6] += ipWei[ucpUnpack[6][2]];
      iSum[7] += ipWei[ucpUnpack[7][2]];
      iSum[8] += ipWei[ucpUnpack[8][2]];
      iSum[9] += ipWei[ucpUnpack[9][2]];
      iSum[10] += ipWei[ucpUnpack[10][2]];
      iSum[11] += ipWei[ucpUnpack[11][2]];
      iSum[12] += ipWei[ucpUnpack[12][2]];
      iSum[13] += ipWei[ucpUnpack[13][2]];
      iSum[14] += ipWei[ucpUnpack[14][2]];
      iSum[15] += ipWei[ucpUnpack[15][2]];
      iSum[16] += ipWei[ucpUnpack[16][2]];
      iSum[17] += ipWei[ucpUnpack[17][2]];
      iSum[18] += ipWei[ucpUnpack[18][2]];
      iSum[19] += ipWei[ucpUnpack[19][2]];
      iSum[20] += ipWei[ucpUnpack[20][2]];
      iSum[21] += ipWei[ucpUnpack[21][2]];
      iSum[22] += ipWei[ucpUnpack[22][2]];
      iSum[23] += ipWei[ucpUnpack[23][2]];

      ipWei = ipLUT[3];
      iSum[0] += ipWei[ucpUnpack[0][3]];
      iSum[1] += ipWei[ucpUnpack[1][3]];
      iSum[2] += ipWei[ucpUnpack[2][3]];
      iSum[3] += ipWei[ucpUnpack[3][3]];
      iSum[4] += ipWei[ucpUnpack[4][3]];
      iSum[5] += ipWei[ucpUnpack[5][3]];
      iSum[6] += ipWei[ucpUnpack[6][3]];
      iSum[7] += ipWei[ucpUnpack[7][3]];
      iSum[8] += ipWei[ucpUnpack[8][3]];
      iSum[9] += ipWei[ucpUnpack[9][3]];
      iSum[10] += ipWei[ucpUnpack[10][3]];
      iSum[11] += ipWei[ucpUnpack[11][3]];
      iSum[12] += ipWei[ucpUnpack[12][3]];
      iSum[13] += ipWei[ucpUnpack[13][3]];
      iSum[14] += ipWei[ucpUnpack[14][3]];
      iSum[15] += ipWei[ucpUnpack[15][3]];
      iSum[16] += ipWei[ucpUnpack[16][3]];
      iSum[17] += ipWei[ucpUnpack[17][3]];
      iSum[18] += ipWei[ucpUnpack[18][3]];
      iSum[19] += ipWei[ucpUnpack[19][3]];
      iSum[20] += ipWei[ucpUnpack[20][3]];
      iSum[21] += ipWei[ucpUnpack[21][3]];
      iSum[22] += ipWei[ucpUnpack[22][3]];
      iSum[23] += ipWei[ucpUnpack[23][3]];

      ipWei = ipLUT[4];
      iSum[0] += ipWei[ucpUnpack[0][4]];
      iSum[1] += ipWei[ucpUnpack[1][4]];
      iSum[2] += ipWei[ucpUnpack[2][4]];
      iSum[3] += ipWei[ucpUnpack[3][4]];
      iSum[4] += ipWei[ucpUnpack[4][4]];
      iSum[5] += ipWei[ucpUnpack[5][4]];
      iSum[6] += ipWei[ucpUnpack[6][4]];
      iSum[7] += ipWei[ucpUnpack[7][4]];
      iSum[8] += ipWei[ucpUnpack[8][4]];
      iSum[9] += ipWei[ucpUnpack[9][4]];
      iSum[10] += ipWei[ucpUnpack[10][4]];
      iSum[11] += ipWei[ucpUnpack[11][4]];
      iSum[12] += ipWei[ucpUnpack[12][4]];
      iSum[13] += ipWei[ucpUnpack[13][4]];
      iSum[14] += ipWei[ucpUnpack[14][4]];
      iSum[15] += ipWei[ucpUnpack[15][4]];
      iSum[16] += ipWei[ucpUnpack[16][4]];
      iSum[17] += ipWei[ucpUnpack[17][4]];
      iSum[18] += ipWei[ucpUnpack[18][4]];
      iSum[19] += ipWei[ucpUnpack[19][4]];
      iSum[20] += ipWei[ucpUnpack[20][4]];
      iSum[21] += ipWei[ucpUnpack[21][4]];
      iSum[22] += ipWei[ucpUnpack[22][4]];
      iSum[23] += ipWei[ucpUnpack[23][4]];

      ipWei = ipLUT[5];
      iSum[0] += ipWei[ucpUnpack[0][5]];
      iSum[1] += ipWei[ucpUnpack[1][5]];
      iSum[2] += ipWei[ucpUnpack[2][5]];
      iSum[3] += ipWei[ucpUnpack[3][5]];
      iSum[4] += ipWei[ucpUnpack[4][5]];
      iSum[5] += ipWei[ucpUnpack[5][5]];
      iSum[6] += ipWei[ucpUnpack[6][5]];
      iSum[7] += ipWei[ucpUnpack[7][5]];
      iSum[8] += ipWei[ucpUnpack[8][5]];
      iSum[9] += ipWei[ucpUnpack[9][5]];
      iSum[10] += ipWei[ucpUnpack[10][5]];
      iSum[11] += ipWei[ucpUnpack[11][5]];
      iSum[12] += ipWei[ucpUnpack[12][5]];
      iSum[13] += ipWei[ucpUnpack[13][5]];
      iSum[14] += ipWei[ucpUnpack[14][5]];
      iSum[15] += ipWei[ucpUnpack[15][5]];
      iSum[16] += ipWei[ucpUnpack[16][5]];
      iSum[17] += ipWei[ucpUnpack[17][5]];
      iSum[18] += ipWei[ucpUnpack[18][5]];
      iSum[19] += ipWei[ucpUnpack[19][5]];
      iSum[20] += ipWei[ucpUnpack[20][5]];
      iSum[21] += ipWei[ucpUnpack[21][5]];
      iSum[22] += ipWei[ucpUnpack[22][5]];
      iSum[23] += ipWei[ucpUnpack[23][5]];

      ipWei = ipLUT[6];
      iSum[0] += ipWei[ucpUnpack[0][6]];
      iSum[1] += ipWei[ucpUnpack[1][6]];
      iSum[2] += ipWei[ucpUnpack[2][6]];
      iSum[3] += ipWei[ucpUnpack[3][6]];
      iSum[4] += ipWei[ucpUnpack[4][6]];
      iSum[5] += ipWei[ucpUnpack[5][6]];
      iSum[6] += ipWei[ucpUnpack[6][6]];
      iSum[7] += ipWei[ucpUnpack[7][6]];
      iSum[8] += ipWei[ucpUnpack[8][6]];
      iSum[9] += ipWei[ucpUnpack[9][6]];
      iSum[10] += ipWei[ucpUnpack[10][6]];
      iSum[11] += ipWei[ucpUnpack[11][6]];
      iSum[12] += ipWei[ucpUnpack[12][6]];
      iSum[13] += ipWei[ucpUnpack[13][6]];
      iSum[14] += ipWei[ucpUnpack[14][6]];
      iSum[15] += ipWei[ucpUnpack[15][6]];
      iSum[16] += ipWei[ucpUnpack[16][6]];
      iSum[17] += ipWei[ucpUnpack[17][6]];
      iSum[18] += ipWei[ucpUnpack[18][6]];
      iSum[19] += ipWei[ucpUnpack[19][6]];
      iSum[20] += ipWei[ucpUnpack[20][6]];
      iSum[21] += ipWei[ucpUnpack[21][6]];
      iSum[22] += ipWei[ucpUnpack[22][6]];
      iSum[23] += ipWei[ucpUnpack[23][6]];

      // clamp and dump the results

      // no rounding - sums were pre-rounded
      // I commented out the "& 0xFF" because I assume the min/max
      // code will take care of making sure the sums are not negative;
      // if that assumption is not valid, they need to be uncommented
      long lTmp1, lTmp2;

      lTmp1 = iSum[0];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[1];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[2];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[3];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent] = lTmp1;

      lTmp1 = iSum[4];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[5];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[6];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[7];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 4] = lTmp1;

      lTmp1 = iSum[8];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[9];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[10];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[11];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 8] = lTmp1;

      lTmp1 = iSum[12];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[13];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[14];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[15];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 12] = lTmp1;

      lTmp1 = iSum[16];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[17];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[18];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[19];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 16] = lTmp1;

      lTmp1 = iSum[20];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[21];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[22];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[23];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 20] = lTmp1;
    }

    // do the "odd" field
    iR = (iRow+1) / iNFieldDst;

    ucpD = ucpDst[iField1] + (iR * iPitchDst);

    iSubPixel = ipSP[1];
    iPixel = ipP[1];

    for (iComponent = 0; iComponent < iUnpackedComponent; iComponent += VERTICAL_COMPONENTS)
     {
      ipLUT = ipLUTWei[iSubPixel];
      ucpUnpack[0] = ucpUnpacked[iJob][iComponent] + iPixel;
      ucpUnpack[1] = ucpUnpacked[iJob][iComponent + 1] + iPixel;
      ucpUnpack[2] = ucpUnpacked[iJob][iComponent + 2] + iPixel;
      ucpUnpack[3] = ucpUnpacked[iJob][iComponent + 3] + iPixel;
      ucpUnpack[4] = ucpUnpacked[iJob][iComponent + 4] + iPixel;
      ucpUnpack[5] = ucpUnpacked[iJob][iComponent + 5] + iPixel;
      ucpUnpack[6] = ucpUnpacked[iJob][iComponent + 6] + iPixel;
      ucpUnpack[7] = ucpUnpacked[iJob][iComponent + 7] + iPixel;
      ucpUnpack[8] = ucpUnpacked[iJob][iComponent + 8] + iPixel;
      ucpUnpack[9] = ucpUnpacked[iJob][iComponent + 9] + iPixel;
      ucpUnpack[10] = ucpUnpacked[iJob][iComponent + 10] + iPixel;
      ucpUnpack[11] = ucpUnpacked[iJob][iComponent + 11] + iPixel;
      ucpUnpack[12] = ucpUnpacked[iJob][iComponent + 12] + iPixel;
      ucpUnpack[13] = ucpUnpacked[iJob][iComponent + 13] + iPixel;
      ucpUnpack[14] = ucpUnpacked[iJob][iComponent + 14] + iPixel;
      ucpUnpack[15] = ucpUnpacked[iJob][iComponent + 15] + iPixel;
      ucpUnpack[16] = ucpUnpacked[iJob][iComponent + 16] + iPixel;
      ucpUnpack[17] = ucpUnpacked[iJob][iComponent + 17] + iPixel;
      ucpUnpack[18] = ucpUnpacked[iJob][iComponent + 18] + iPixel;
      ucpUnpack[19] = ucpUnpacked[iJob][iComponent + 19] + iPixel;
      ucpUnpack[20] = ucpUnpacked[iJob][iComponent + 20] + iPixel;
      ucpUnpack[21] = ucpUnpacked[iJob][iComponent + 21] + iPixel;
      ucpUnpack[22] = ucpUnpacked[iJob][iComponent + 22] + iPixel;
      ucpUnpack[23] = ucpUnpacked[iJob][iComponent + 23] + iPixel;

      // pre-round the sums
      iSum[0] = iSum[1] = iSum[2] = iSum[3] =
      iSum[4] = iSum[5] = iSum[6] = iSum[7] =
      iSum[8] = iSum[9] = iSum[10] = iSum[11] =
      iSum[12] = iSum[13] = iSum[14] = iSum[15] =
      iSum[16] = iSum[17] = iSum[18] = iSum[19] =
      iSum[20] = iSum[21] = iSum[22] = iSum[23] =
                  iHalfPrecision;

      // do the 7 neighbors
      ipWei = ipLUT[0];
      iSum[0] += ipWei[ucpUnpack[0][0]];
      iSum[1] += ipWei[ucpUnpack[1][0]];
      iSum[2] += ipWei[ucpUnpack[2][0]];
      iSum[3] += ipWei[ucpUnpack[3][0]];
      iSum[4] += ipWei[ucpUnpack[4][0]];
      iSum[5] += ipWei[ucpUnpack[5][0]];
      iSum[6] += ipWei[ucpUnpack[6][0]];
      iSum[7] += ipWei[ucpUnpack[7][0]];
      iSum[8] += ipWei[ucpUnpack[8][0]];
      iSum[9] += ipWei[ucpUnpack[9][0]];
      iSum[10] += ipWei[ucpUnpack[10][0]];
      iSum[11] += ipWei[ucpUnpack[11][0]];
      iSum[12] += ipWei[ucpUnpack[12][0]];
      iSum[13] += ipWei[ucpUnpack[13][0]];
      iSum[14] += ipWei[ucpUnpack[14][0]];
      iSum[15] += ipWei[ucpUnpack[15][0]];
      iSum[16] += ipWei[ucpUnpack[16][0]];
      iSum[17] += ipWei[ucpUnpack[17][0]];
      iSum[18] += ipWei[ucpUnpack[18][0]];
      iSum[19] += ipWei[ucpUnpack[19][0]];
      iSum[20] += ipWei[ucpUnpack[20][0]];
      iSum[21] += ipWei[ucpUnpack[21][0]];
      iSum[22] += ipWei[ucpUnpack[22][0]];
      iSum[23] += ipWei[ucpUnpack[23][0]];

      ipWei = ipLUT[1];
      iSum[0] += ipWei[ucpUnpack[0][1]];
      iSum[1] += ipWei[ucpUnpack[1][1]];
      iSum[2] += ipWei[ucpUnpack[2][1]];
      iSum[3] += ipWei[ucpUnpack[3][1]];
      iSum[4] += ipWei[ucpUnpack[4][1]];
      iSum[5] += ipWei[ucpUnpack[5][1]];
      iSum[6] += ipWei[ucpUnpack[6][1]];
      iSum[7] += ipWei[ucpUnpack[7][1]];
      iSum[8] += ipWei[ucpUnpack[8][1]];
      iSum[9] += ipWei[ucpUnpack[9][1]];
      iSum[10] += ipWei[ucpUnpack[10][1]];
      iSum[11] += ipWei[ucpUnpack[11][1]];
      iSum[12] += ipWei[ucpUnpack[12][1]];
      iSum[13] += ipWei[ucpUnpack[13][1]];
      iSum[14] += ipWei[ucpUnpack[14][1]];
      iSum[15] += ipWei[ucpUnpack[15][1]];
      iSum[16] += ipWei[ucpUnpack[16][1]];
      iSum[17] += ipWei[ucpUnpack[17][1]];
      iSum[18] += ipWei[ucpUnpack[18][1]];
      iSum[19] += ipWei[ucpUnpack[19][1]];
      iSum[20] += ipWei[ucpUnpack[20][1]];
      iSum[21] += ipWei[ucpUnpack[21][1]];
      iSum[22] += ipWei[ucpUnpack[22][1]];
      iSum[23] += ipWei[ucpUnpack[23][1]];

      ipWei = ipLUT[2];
      iSum[0] += ipWei[ucpUnpack[0][2]];
      iSum[1] += ipWei[ucpUnpack[1][2]];
      iSum[2] += ipWei[ucpUnpack[2][2]];
      iSum[3] += ipWei[ucpUnpack[3][2]];
      iSum[4] += ipWei[ucpUnpack[4][2]];
      iSum[5] += ipWei[ucpUnpack[5][2]];
      iSum[6] += ipWei[ucpUnpack[6][2]];
      iSum[7] += ipWei[ucpUnpack[7][2]];
      iSum[8] += ipWei[ucpUnpack[8][2]];
      iSum[9] += ipWei[ucpUnpack[9][2]];
      iSum[10] += ipWei[ucpUnpack[10][2]];
      iSum[11] += ipWei[ucpUnpack[11][2]];
      iSum[12] += ipWei[ucpUnpack[12][2]];
      iSum[13] += ipWei[ucpUnpack[13][2]];
      iSum[14] += ipWei[ucpUnpack[14][2]];
      iSum[15] += ipWei[ucpUnpack[15][2]];
      iSum[16] += ipWei[ucpUnpack[16][2]];
      iSum[17] += ipWei[ucpUnpack[17][2]];
      iSum[18] += ipWei[ucpUnpack[18][2]];
      iSum[19] += ipWei[ucpUnpack[19][2]];
      iSum[20] += ipWei[ucpUnpack[20][2]];
      iSum[21] += ipWei[ucpUnpack[21][2]];
      iSum[22] += ipWei[ucpUnpack[22][2]];
      iSum[23] += ipWei[ucpUnpack[23][2]];

      ipWei = ipLUT[3];
      iSum[0] += ipWei[ucpUnpack[0][3]];
      iSum[1] += ipWei[ucpUnpack[1][3]];
      iSum[2] += ipWei[ucpUnpack[2][3]];
      iSum[3] += ipWei[ucpUnpack[3][3]];
      iSum[4] += ipWei[ucpUnpack[4][3]];
      iSum[5] += ipWei[ucpUnpack[5][3]];
      iSum[6] += ipWei[ucpUnpack[6][3]];
      iSum[7] += ipWei[ucpUnpack[7][3]];
      iSum[8] += ipWei[ucpUnpack[8][3]];
      iSum[9] += ipWei[ucpUnpack[9][3]];
      iSum[10] += ipWei[ucpUnpack[10][3]];
      iSum[11] += ipWei[ucpUnpack[11][3]];
      iSum[12] += ipWei[ucpUnpack[12][3]];
      iSum[13] += ipWei[ucpUnpack[13][3]];
      iSum[14] += ipWei[ucpUnpack[14][3]];
      iSum[15] += ipWei[ucpUnpack[15][3]];
      iSum[16] += ipWei[ucpUnpack[16][3]];
      iSum[17] += ipWei[ucpUnpack[17][3]];
      iSum[18] += ipWei[ucpUnpack[18][3]];
      iSum[19] += ipWei[ucpUnpack[19][3]];
      iSum[20] += ipWei[ucpUnpack[20][3]];
      iSum[21] += ipWei[ucpUnpack[21][3]];
      iSum[22] += ipWei[ucpUnpack[22][3]];
      iSum[23] += ipWei[ucpUnpack[23][3]];

      ipWei = ipLUT[4];
      iSum[0] += ipWei[ucpUnpack[0][4]];
      iSum[1] += ipWei[ucpUnpack[1][4]];
      iSum[2] += ipWei[ucpUnpack[2][4]];
      iSum[3] += ipWei[ucpUnpack[3][4]];
      iSum[4] += ipWei[ucpUnpack[4][4]];
      iSum[5] += ipWei[ucpUnpack[5][4]];
      iSum[6] += ipWei[ucpUnpack[6][4]];
      iSum[7] += ipWei[ucpUnpack[7][4]];
      iSum[8] += ipWei[ucpUnpack[8][4]];
      iSum[9] += ipWei[ucpUnpack[9][4]];
      iSum[10] += ipWei[ucpUnpack[10][4]];
      iSum[11] += ipWei[ucpUnpack[11][4]];
      iSum[12] += ipWei[ucpUnpack[12][4]];
      iSum[13] += ipWei[ucpUnpack[13][4]];
      iSum[14] += ipWei[ucpUnpack[14][4]];
      iSum[15] += ipWei[ucpUnpack[15][4]];
      iSum[16] += ipWei[ucpUnpack[16][4]];
      iSum[17] += ipWei[ucpUnpack[17][4]];
      iSum[18] += ipWei[ucpUnpack[18][4]];
      iSum[19] += ipWei[ucpUnpack[19][4]];
      iSum[20] += ipWei[ucpUnpack[20][4]];
      iSum[21] += ipWei[ucpUnpack[21][4]];
      iSum[22] += ipWei[ucpUnpack[22][4]];
      iSum[23] += ipWei[ucpUnpack[23][4]];

      ipWei = ipLUT[5];
      iSum[0] += ipWei[ucpUnpack[0][5]];
      iSum[1] += ipWei[ucpUnpack[1][5]];
      iSum[2] += ipWei[ucpUnpack[2][5]];
      iSum[3] += ipWei[ucpUnpack[3][5]];
      iSum[4] += ipWei[ucpUnpack[4][5]];
      iSum[5] += ipWei[ucpUnpack[5][5]];
      iSum[6] += ipWei[ucpUnpack[6][5]];
      iSum[7] += ipWei[ucpUnpack[7][5]];
      iSum[8] += ipWei[ucpUnpack[8][5]];
      iSum[9] += ipWei[ucpUnpack[9][5]];
      iSum[10] += ipWei[ucpUnpack[10][5]];
      iSum[11] += ipWei[ucpUnpack[11][5]];
      iSum[12] += ipWei[ucpUnpack[12][5]];
      iSum[13] += ipWei[ucpUnpack[13][5]];
      iSum[14] += ipWei[ucpUnpack[14][5]];
      iSum[15] += ipWei[ucpUnpack[15][5]];
      iSum[16] += ipWei[ucpUnpack[16][5]];
      iSum[17] += ipWei[ucpUnpack[17][5]];
      iSum[18] += ipWei[ucpUnpack[18][5]];
      iSum[19] += ipWei[ucpUnpack[19][5]];
      iSum[20] += ipWei[ucpUnpack[20][5]];
      iSum[21] += ipWei[ucpUnpack[21][5]];
      iSum[22] += ipWei[ucpUnpack[22][5]];
      iSum[23] += ipWei[ucpUnpack[23][5]];

      ipWei = ipLUT[6];
      iSum[0] += ipWei[ucpUnpack[0][6]];
      iSum[1] += ipWei[ucpUnpack[1][6]];
      iSum[2] += ipWei[ucpUnpack[2][6]];
      iSum[3] += ipWei[ucpUnpack[3][6]];
      iSum[4] += ipWei[ucpUnpack[4][6]];
      iSum[5] += ipWei[ucpUnpack[5][6]];
      iSum[6] += ipWei[ucpUnpack[6][6]];
      iSum[7] += ipWei[ucpUnpack[7][6]];
      iSum[8] += ipWei[ucpUnpack[8][6]];
      iSum[9] += ipWei[ucpUnpack[9][6]];
      iSum[10] += ipWei[ucpUnpack[10][6]];
      iSum[11] += ipWei[ucpUnpack[11][6]];
      iSum[12] += ipWei[ucpUnpack[12][6]];
      iSum[13] += ipWei[ucpUnpack[13][6]];
      iSum[14] += ipWei[ucpUnpack[14][6]];
      iSum[15] += ipWei[ucpUnpack[15][6]];
      iSum[16] += ipWei[ucpUnpack[16][6]];
      iSum[17] += ipWei[ucpUnpack[17][6]];
      iSum[18] += ipWei[ucpUnpack[18][6]];
      iSum[19] += ipWei[ucpUnpack[19][6]];
      iSum[20] += ipWei[ucpUnpack[20][6]];
      iSum[21] += ipWei[ucpUnpack[21][6]];
      iSum[22] += ipWei[ucpUnpack[22][6]];
      iSum[23] += ipWei[ucpUnpack[23][6]];

      // clamp and dump the results

      // no rounding - sums were pre-rounded
      // I commented out the "& 0xFF" because I assume the min/max
      // code will take care of making sure the sums are not negative;
      // if that assumption is not valid, they need to be uncommented
      long lTmp1, lTmp2;

      lTmp1 = iSum[0];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[1];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[2];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[3];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent] = lTmp1;

      lTmp1 = iSum[4];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[5];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[6];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[7];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 4] = lTmp1;

      lTmp1 = iSum[8];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[9];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[10];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[11];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 8] = lTmp1;

      lTmp1 = iSum[12];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[13];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[14];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[15];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 12] = lTmp1;

      lTmp1 = iSum[16];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[17];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[18];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[19];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 16] = lTmp1;

      lTmp1 = iSum[20];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[21];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[22];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[23];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 20] = lTmp1;
     }
    ipSP += 2;
    ipP += 2;
   }
}

void CAspectRatio::RenderV_YUV_9 (MTI_UINT8 *ucpDst[2], int &iRow, int iStopDstLocal,
		int iJob)
/*
	Do VERTICAL rendering with 9 neighbors
*/
{
  MTI_UINT8 *ucpD;
  int iField, iR, *ipSP, *ipP, iPixel, iNei;
  int **ipLUT, iSubPixel;
  int iSum[VERTICAL_COMPONENTS], *ipWei;
  MTI_UINT8 *ucpUnpack[VERTICAL_COMPONENTS];
  int iComponent;

  // check for errors
  if ((iRow & 0x1) || (iStopDstLocal & 0x1))
    return;

  int iField0, iField1;

  if (iNFieldDst == 2)
   {
    iField0 = 0;
    iField1 = 1;
   }
  else
   {
    iField0 = 0;
    iField1 = 0;
   }


  ipSP = ipLUTSubPixel + iRow;
  ipP = ipLUTPixel + iRow;
  for (; iRow < iStopDstLocal; iRow+=2)
   {
    // do the "even" field
    iR = iRow / iNFieldDst;

    ucpD = ucpDst[iField0] + (iR * iPitchDst);

    iSubPixel = ipSP[0];
    iPixel = ipP[0];

    for (iComponent = 0; iComponent < iUnpackedComponent; iComponent += VERTICAL_COMPONENTS)
     {
      ipLUT = ipLUTWei[iSubPixel];
      ucpUnpack[0] = ucpUnpacked[iJob][iComponent] + iPixel;
      ucpUnpack[1] = ucpUnpacked[iJob][iComponent + 1] + iPixel;
      ucpUnpack[2] = ucpUnpacked[iJob][iComponent + 2] + iPixel;
      ucpUnpack[3] = ucpUnpacked[iJob][iComponent + 3] + iPixel;
      ucpUnpack[4] = ucpUnpacked[iJob][iComponent + 4] + iPixel;
      ucpUnpack[5] = ucpUnpacked[iJob][iComponent + 5] + iPixel;
      ucpUnpack[6] = ucpUnpacked[iJob][iComponent + 6] + iPixel;
      ucpUnpack[7] = ucpUnpacked[iJob][iComponent + 7] + iPixel;
      ucpUnpack[8] = ucpUnpacked[iJob][iComponent + 8] + iPixel;
      ucpUnpack[9] = ucpUnpacked[iJob][iComponent + 9] + iPixel;
      ucpUnpack[10] = ucpUnpacked[iJob][iComponent + 10] + iPixel;
      ucpUnpack[11] = ucpUnpacked[iJob][iComponent + 11] + iPixel;
      ucpUnpack[12] = ucpUnpacked[iJob][iComponent + 12] + iPixel;
      ucpUnpack[13] = ucpUnpacked[iJob][iComponent + 13] + iPixel;
      ucpUnpack[14] = ucpUnpacked[iJob][iComponent + 14] + iPixel;
      ucpUnpack[15] = ucpUnpacked[iJob][iComponent + 15] + iPixel;
      ucpUnpack[16] = ucpUnpacked[iJob][iComponent + 16] + iPixel;
      ucpUnpack[17] = ucpUnpacked[iJob][iComponent + 17] + iPixel;
      ucpUnpack[18] = ucpUnpacked[iJob][iComponent + 18] + iPixel;
      ucpUnpack[19] = ucpUnpacked[iJob][iComponent + 19] + iPixel;
      ucpUnpack[20] = ucpUnpacked[iJob][iComponent + 20] + iPixel;
      ucpUnpack[21] = ucpUnpacked[iJob][iComponent + 21] + iPixel;
      ucpUnpack[22] = ucpUnpacked[iJob][iComponent + 22] + iPixel;
      ucpUnpack[23] = ucpUnpacked[iJob][iComponent + 23] + iPixel;

      // pre-round the sums
      iSum[0] = iSum[1] = iSum[2] = iSum[3] =
      iSum[4] = iSum[5] = iSum[6] = iSum[7] =
      iSum[8] = iSum[9] = iSum[10] = iSum[11] =
      iSum[12] = iSum[13] = iSum[14] = iSum[15] =
      iSum[16] = iSum[17] = iSum[18] = iSum[19] =
      iSum[20] = iSum[21] = iSum[22] = iSum[23] =
                  iHalfPrecision;

      // do the 9 neighbors
      ipWei = ipLUT[0];
      iSum[0] += ipWei[ucpUnpack[0][0]];
      iSum[1] += ipWei[ucpUnpack[1][0]];
      iSum[2] += ipWei[ucpUnpack[2][0]];
      iSum[3] += ipWei[ucpUnpack[3][0]];
      iSum[4] += ipWei[ucpUnpack[4][0]];
      iSum[5] += ipWei[ucpUnpack[5][0]];
      iSum[6] += ipWei[ucpUnpack[6][0]];
      iSum[7] += ipWei[ucpUnpack[7][0]];
      iSum[8] += ipWei[ucpUnpack[8][0]];
      iSum[9] += ipWei[ucpUnpack[9][0]];
      iSum[10] += ipWei[ucpUnpack[10][0]];
      iSum[11] += ipWei[ucpUnpack[11][0]];
      iSum[12] += ipWei[ucpUnpack[12][0]];
      iSum[13] += ipWei[ucpUnpack[13][0]];
      iSum[14] += ipWei[ucpUnpack[14][0]];
      iSum[15] += ipWei[ucpUnpack[15][0]];
      iSum[16] += ipWei[ucpUnpack[16][0]];
      iSum[17] += ipWei[ucpUnpack[17][0]];
      iSum[18] += ipWei[ucpUnpack[18][0]];
      iSum[19] += ipWei[ucpUnpack[19][0]];
      iSum[20] += ipWei[ucpUnpack[20][0]];
      iSum[21] += ipWei[ucpUnpack[21][0]];
      iSum[22] += ipWei[ucpUnpack[22][0]];
      iSum[23] += ipWei[ucpUnpack[23][0]];

      ipWei = ipLUT[1];
      iSum[0] += ipWei[ucpUnpack[0][1]];
      iSum[1] += ipWei[ucpUnpack[1][1]];
      iSum[2] += ipWei[ucpUnpack[2][1]];
      iSum[3] += ipWei[ucpUnpack[3][1]];
      iSum[4] += ipWei[ucpUnpack[4][1]];
      iSum[5] += ipWei[ucpUnpack[5][1]];
      iSum[6] += ipWei[ucpUnpack[6][1]];
      iSum[7] += ipWei[ucpUnpack[7][1]];
      iSum[8] += ipWei[ucpUnpack[8][1]];
      iSum[9] += ipWei[ucpUnpack[9][1]];
      iSum[10] += ipWei[ucpUnpack[10][1]];
      iSum[11] += ipWei[ucpUnpack[11][1]];
      iSum[12] += ipWei[ucpUnpack[12][1]];
      iSum[13] += ipWei[ucpUnpack[13][1]];
      iSum[14] += ipWei[ucpUnpack[14][1]];
      iSum[15] += ipWei[ucpUnpack[15][1]];
      iSum[16] += ipWei[ucpUnpack[16][1]];
      iSum[17] += ipWei[ucpUnpack[17][1]];
      iSum[18] += ipWei[ucpUnpack[18][1]];
      iSum[19] += ipWei[ucpUnpack[19][1]];
      iSum[20] += ipWei[ucpUnpack[20][1]];
      iSum[21] += ipWei[ucpUnpack[21][1]];
      iSum[22] += ipWei[ucpUnpack[22][1]];
      iSum[23] += ipWei[ucpUnpack[23][1]];

      ipWei = ipLUT[2];
      iSum[0] += ipWei[ucpUnpack[0][2]];
      iSum[1] += ipWei[ucpUnpack[1][2]];
      iSum[2] += ipWei[ucpUnpack[2][2]];
      iSum[3] += ipWei[ucpUnpack[3][2]];
      iSum[4] += ipWei[ucpUnpack[4][2]];
      iSum[5] += ipWei[ucpUnpack[5][2]];
      iSum[6] += ipWei[ucpUnpack[6][2]];
      iSum[7] += ipWei[ucpUnpack[7][2]];
      iSum[8] += ipWei[ucpUnpack[8][2]];
      iSum[9] += ipWei[ucpUnpack[9][2]];
      iSum[10] += ipWei[ucpUnpack[10][2]];
      iSum[11] += ipWei[ucpUnpack[11][2]];
      iSum[12] += ipWei[ucpUnpack[12][2]];
      iSum[13] += ipWei[ucpUnpack[13][2]];
      iSum[14] += ipWei[ucpUnpack[14][2]];
      iSum[15] += ipWei[ucpUnpack[15][2]];
      iSum[16] += ipWei[ucpUnpack[16][2]];
      iSum[17] += ipWei[ucpUnpack[17][2]];
      iSum[18] += ipWei[ucpUnpack[18][2]];
      iSum[19] += ipWei[ucpUnpack[19][2]];
      iSum[20] += ipWei[ucpUnpack[20][2]];
      iSum[21] += ipWei[ucpUnpack[21][2]];
      iSum[22] += ipWei[ucpUnpack[22][2]];
      iSum[23] += ipWei[ucpUnpack[23][2]];

      ipWei = ipLUT[3];
      iSum[0] += ipWei[ucpUnpack[0][3]];
      iSum[1] += ipWei[ucpUnpack[1][3]];
      iSum[2] += ipWei[ucpUnpack[2][3]];
      iSum[3] += ipWei[ucpUnpack[3][3]];
      iSum[4] += ipWei[ucpUnpack[4][3]];
      iSum[5] += ipWei[ucpUnpack[5][3]];
      iSum[6] += ipWei[ucpUnpack[6][3]];
      iSum[7] += ipWei[ucpUnpack[7][3]];
      iSum[8] += ipWei[ucpUnpack[8][3]];
      iSum[9] += ipWei[ucpUnpack[9][3]];
      iSum[10] += ipWei[ucpUnpack[10][3]];
      iSum[11] += ipWei[ucpUnpack[11][3]];
      iSum[12] += ipWei[ucpUnpack[12][3]];
      iSum[13] += ipWei[ucpUnpack[13][3]];
      iSum[14] += ipWei[ucpUnpack[14][3]];
      iSum[15] += ipWei[ucpUnpack[15][3]];
      iSum[16] += ipWei[ucpUnpack[16][3]];
      iSum[17] += ipWei[ucpUnpack[17][3]];
      iSum[18] += ipWei[ucpUnpack[18][3]];
      iSum[19] += ipWei[ucpUnpack[19][3]];
      iSum[20] += ipWei[ucpUnpack[20][3]];
      iSum[21] += ipWei[ucpUnpack[21][3]];
      iSum[22] += ipWei[ucpUnpack[22][3]];
      iSum[23] += ipWei[ucpUnpack[23][3]];

      ipWei = ipLUT[4];
      iSum[0] += ipWei[ucpUnpack[0][4]];
      iSum[1] += ipWei[ucpUnpack[1][4]];
      iSum[2] += ipWei[ucpUnpack[2][4]];
      iSum[3] += ipWei[ucpUnpack[3][4]];
      iSum[4] += ipWei[ucpUnpack[4][4]];
      iSum[5] += ipWei[ucpUnpack[5][4]];
      iSum[6] += ipWei[ucpUnpack[6][4]];
      iSum[7] += ipWei[ucpUnpack[7][4]];
      iSum[8] += ipWei[ucpUnpack[8][4]];
      iSum[9] += ipWei[ucpUnpack[9][4]];
      iSum[10] += ipWei[ucpUnpack[10][4]];
      iSum[11] += ipWei[ucpUnpack[11][4]];
      iSum[12] += ipWei[ucpUnpack[12][4]];
      iSum[13] += ipWei[ucpUnpack[13][4]];
      iSum[14] += ipWei[ucpUnpack[14][4]];
      iSum[15] += ipWei[ucpUnpack[15][4]];
      iSum[16] += ipWei[ucpUnpack[16][4]];
      iSum[17] += ipWei[ucpUnpack[17][4]];
      iSum[18] += ipWei[ucpUnpack[18][4]];
      iSum[19] += ipWei[ucpUnpack[19][4]];
      iSum[20] += ipWei[ucpUnpack[20][4]];
      iSum[21] += ipWei[ucpUnpack[21][4]];
      iSum[22] += ipWei[ucpUnpack[22][4]];
      iSum[23] += ipWei[ucpUnpack[23][4]];

      ipWei = ipLUT[5];
      iSum[0] += ipWei[ucpUnpack[0][5]];
      iSum[1] += ipWei[ucpUnpack[1][5]];
      iSum[2] += ipWei[ucpUnpack[2][5]];
      iSum[3] += ipWei[ucpUnpack[3][5]];
      iSum[4] += ipWei[ucpUnpack[4][5]];
      iSum[5] += ipWei[ucpUnpack[5][5]];
      iSum[6] += ipWei[ucpUnpack[6][5]];
      iSum[7] += ipWei[ucpUnpack[7][5]];
      iSum[8] += ipWei[ucpUnpack[8][5]];
      iSum[9] += ipWei[ucpUnpack[9][5]];
      iSum[10] += ipWei[ucpUnpack[10][5]];
      iSum[11] += ipWei[ucpUnpack[11][5]];
      iSum[12] += ipWei[ucpUnpack[12][5]];
      iSum[13] += ipWei[ucpUnpack[13][5]];
      iSum[14] += ipWei[ucpUnpack[14][5]];
      iSum[15] += ipWei[ucpUnpack[15][5]];
      iSum[16] += ipWei[ucpUnpack[16][5]];
      iSum[17] += ipWei[ucpUnpack[17][5]];
      iSum[18] += ipWei[ucpUnpack[18][5]];
      iSum[19] += ipWei[ucpUnpack[19][5]];
      iSum[20] += ipWei[ucpUnpack[20][5]];
      iSum[21] += ipWei[ucpUnpack[21][5]];
      iSum[22] += ipWei[ucpUnpack[22][5]];
      iSum[23] += ipWei[ucpUnpack[23][5]];

      ipWei = ipLUT[6];
      iSum[0] += ipWei[ucpUnpack[0][6]];
      iSum[1] += ipWei[ucpUnpack[1][6]];
      iSum[2] += ipWei[ucpUnpack[2][6]];
      iSum[3] += ipWei[ucpUnpack[3][6]];
      iSum[4] += ipWei[ucpUnpack[4][6]];
      iSum[5] += ipWei[ucpUnpack[5][6]];
      iSum[6] += ipWei[ucpUnpack[6][6]];
      iSum[7] += ipWei[ucpUnpack[7][6]];
      iSum[8] += ipWei[ucpUnpack[8][6]];
      iSum[9] += ipWei[ucpUnpack[9][6]];
      iSum[10] += ipWei[ucpUnpack[10][6]];
      iSum[11] += ipWei[ucpUnpack[11][6]];
      iSum[12] += ipWei[ucpUnpack[12][6]];
      iSum[13] += ipWei[ucpUnpack[13][6]];
      iSum[14] += ipWei[ucpUnpack[14][6]];
      iSum[15] += ipWei[ucpUnpack[15][6]];
      iSum[16] += ipWei[ucpUnpack[16][6]];
      iSum[17] += ipWei[ucpUnpack[17][6]];
      iSum[18] += ipWei[ucpUnpack[18][6]];
      iSum[19] += ipWei[ucpUnpack[19][6]];
      iSum[20] += ipWei[ucpUnpack[20][6]];
      iSum[21] += ipWei[ucpUnpack[21][6]];
      iSum[22] += ipWei[ucpUnpack[22][6]];
      iSum[23] += ipWei[ucpUnpack[23][6]];

      ipWei = ipLUT[7];
      iSum[0] += ipWei[ucpUnpack[0][7]];
      iSum[1] += ipWei[ucpUnpack[1][7]];
      iSum[2] += ipWei[ucpUnpack[2][7]];
      iSum[3] += ipWei[ucpUnpack[3][7]];
      iSum[4] += ipWei[ucpUnpack[4][7]];
      iSum[5] += ipWei[ucpUnpack[5][7]];
      iSum[6] += ipWei[ucpUnpack[6][7]];
      iSum[7] += ipWei[ucpUnpack[7][7]];
      iSum[8] += ipWei[ucpUnpack[8][7]];
      iSum[9] += ipWei[ucpUnpack[9][7]];
      iSum[10] += ipWei[ucpUnpack[10][7]];
      iSum[11] += ipWei[ucpUnpack[11][7]];
      iSum[12] += ipWei[ucpUnpack[12][7]];
      iSum[13] += ipWei[ucpUnpack[13][7]];
      iSum[14] += ipWei[ucpUnpack[14][7]];
      iSum[15] += ipWei[ucpUnpack[15][7]];
      iSum[16] += ipWei[ucpUnpack[16][7]];
      iSum[17] += ipWei[ucpUnpack[17][7]];
      iSum[18] += ipWei[ucpUnpack[18][7]];
      iSum[19] += ipWei[ucpUnpack[19][7]];
      iSum[20] += ipWei[ucpUnpack[20][7]];
      iSum[21] += ipWei[ucpUnpack[21][7]];
      iSum[22] += ipWei[ucpUnpack[22][7]];
      iSum[23] += ipWei[ucpUnpack[23][7]];

      ipWei = ipLUT[8];
      iSum[0] += ipWei[ucpUnpack[0][8]];
      iSum[1] += ipWei[ucpUnpack[1][8]];
      iSum[2] += ipWei[ucpUnpack[2][8]];
      iSum[3] += ipWei[ucpUnpack[3][8]];
      iSum[4] += ipWei[ucpUnpack[4][8]];
      iSum[5] += ipWei[ucpUnpack[5][8]];
      iSum[6] += ipWei[ucpUnpack[6][8]];
      iSum[7] += ipWei[ucpUnpack[7][8]];
      iSum[8] += ipWei[ucpUnpack[8][8]];
      iSum[9] += ipWei[ucpUnpack[9][8]];
      iSum[10] += ipWei[ucpUnpack[10][8]];
      iSum[11] += ipWei[ucpUnpack[11][8]];
      iSum[12] += ipWei[ucpUnpack[12][8]];
      iSum[13] += ipWei[ucpUnpack[13][8]];
      iSum[14] += ipWei[ucpUnpack[14][8]];
      iSum[15] += ipWei[ucpUnpack[15][8]];
      iSum[16] += ipWei[ucpUnpack[16][8]];
      iSum[17] += ipWei[ucpUnpack[17][8]];
      iSum[18] += ipWei[ucpUnpack[18][8]];
      iSum[19] += ipWei[ucpUnpack[19][8]];
      iSum[20] += ipWei[ucpUnpack[20][8]];
      iSum[21] += ipWei[ucpUnpack[21][8]];
      iSum[22] += ipWei[ucpUnpack[22][8]];
      iSum[23] += ipWei[ucpUnpack[23][8]];

      // clamp and dump the results

      // no rounding - sums were pre-rounded
      // I commented out the "& 0xFF" because I assume the min/max
      // code will take care of making sure the sums are not negative;
      // if that assumption is not valid, they need to be uncommented
      long lTmp1, lTmp2;

      lTmp1 = iSum[0];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[1];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[2];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[3];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent] = lTmp1;

      lTmp1 = iSum[4];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[5];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[6];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[7];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 4] = lTmp1;

      lTmp1 = iSum[8];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[9];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[10];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[11];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 8] = lTmp1;

      lTmp1 = iSum[12];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[13];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[14];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[15];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 12] = lTmp1;

      lTmp1 = iSum[16];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[17];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[18];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[19];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 16] = lTmp1;

      lTmp1 = iSum[20];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[21];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[22];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[23];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 20] = lTmp1;
    }

    // do the "odd" field
    iR = (iRow+1) / iNFieldDst;

    ucpD = ucpDst[iField1] + (iR * iPitchDst);

    iSubPixel = ipSP[1];
    iPixel = ipP[1];

    for (iComponent = 0; iComponent < iUnpackedComponent; iComponent += VERTICAL_COMPONENTS)
     {
      ipLUT = ipLUTWei[iSubPixel];
      ucpUnpack[0] = ucpUnpacked[iJob][iComponent] + iPixel;
      ucpUnpack[1] = ucpUnpacked[iJob][iComponent + 1] + iPixel;
      ucpUnpack[2] = ucpUnpacked[iJob][iComponent + 2] + iPixel;
      ucpUnpack[3] = ucpUnpacked[iJob][iComponent + 3] + iPixel;
      ucpUnpack[4] = ucpUnpacked[iJob][iComponent + 4] + iPixel;
      ucpUnpack[5] = ucpUnpacked[iJob][iComponent + 5] + iPixel;
      ucpUnpack[6] = ucpUnpacked[iJob][iComponent + 6] + iPixel;
      ucpUnpack[7] = ucpUnpacked[iJob][iComponent + 7] + iPixel;
      ucpUnpack[8] = ucpUnpacked[iJob][iComponent + 8] + iPixel;
      ucpUnpack[9] = ucpUnpacked[iJob][iComponent + 9] + iPixel;
      ucpUnpack[10] = ucpUnpacked[iJob][iComponent + 10] + iPixel;
      ucpUnpack[11] = ucpUnpacked[iJob][iComponent + 11] + iPixel;
      ucpUnpack[12] = ucpUnpacked[iJob][iComponent + 12] + iPixel;
      ucpUnpack[13] = ucpUnpacked[iJob][iComponent + 13] + iPixel;
      ucpUnpack[14] = ucpUnpacked[iJob][iComponent + 14] + iPixel;
      ucpUnpack[15] = ucpUnpacked[iJob][iComponent + 15] + iPixel;
      ucpUnpack[16] = ucpUnpacked[iJob][iComponent + 16] + iPixel;
      ucpUnpack[17] = ucpUnpacked[iJob][iComponent + 17] + iPixel;
      ucpUnpack[18] = ucpUnpacked[iJob][iComponent + 18] + iPixel;
      ucpUnpack[19] = ucpUnpacked[iJob][iComponent + 19] + iPixel;
      ucpUnpack[20] = ucpUnpacked[iJob][iComponent + 20] + iPixel;
      ucpUnpack[21] = ucpUnpacked[iJob][iComponent + 21] + iPixel;
      ucpUnpack[22] = ucpUnpacked[iJob][iComponent + 22] + iPixel;
      ucpUnpack[23] = ucpUnpacked[iJob][iComponent + 23] + iPixel;

      // pre-round the sums
      iSum[0] = iSum[1] = iSum[2] = iSum[3] =
      iSum[4] = iSum[5] = iSum[6] = iSum[7] =
      iSum[8] = iSum[9] = iSum[10] = iSum[11] =
      iSum[12] = iSum[13] = iSum[14] = iSum[15] =
      iSum[16] = iSum[17] = iSum[18] = iSum[19] =
      iSum[20] = iSum[21] = iSum[22] = iSum[23] =
                  iHalfPrecision;

      // do the 9 neighbors
      ipWei = ipLUT[0];
      iSum[0] += ipWei[ucpUnpack[0][0]];
      iSum[1] += ipWei[ucpUnpack[1][0]];
      iSum[2] += ipWei[ucpUnpack[2][0]];
      iSum[3] += ipWei[ucpUnpack[3][0]];
      iSum[4] += ipWei[ucpUnpack[4][0]];
      iSum[5] += ipWei[ucpUnpack[5][0]];
      iSum[6] += ipWei[ucpUnpack[6][0]];
      iSum[7] += ipWei[ucpUnpack[7][0]];
      iSum[8] += ipWei[ucpUnpack[8][0]];
      iSum[9] += ipWei[ucpUnpack[9][0]];
      iSum[10] += ipWei[ucpUnpack[10][0]];
      iSum[11] += ipWei[ucpUnpack[11][0]];
      iSum[12] += ipWei[ucpUnpack[12][0]];
      iSum[13] += ipWei[ucpUnpack[13][0]];
      iSum[14] += ipWei[ucpUnpack[14][0]];
      iSum[15] += ipWei[ucpUnpack[15][0]];
      iSum[16] += ipWei[ucpUnpack[16][0]];
      iSum[17] += ipWei[ucpUnpack[17][0]];
      iSum[18] += ipWei[ucpUnpack[18][0]];
      iSum[19] += ipWei[ucpUnpack[19][0]];
      iSum[20] += ipWei[ucpUnpack[20][0]];
      iSum[21] += ipWei[ucpUnpack[21][0]];
      iSum[22] += ipWei[ucpUnpack[22][0]];
      iSum[23] += ipWei[ucpUnpack[23][0]];

      ipWei = ipLUT[1];
      iSum[0] += ipWei[ucpUnpack[0][1]];
      iSum[1] += ipWei[ucpUnpack[1][1]];
      iSum[2] += ipWei[ucpUnpack[2][1]];
      iSum[3] += ipWei[ucpUnpack[3][1]];
      iSum[4] += ipWei[ucpUnpack[4][1]];
      iSum[5] += ipWei[ucpUnpack[5][1]];
      iSum[6] += ipWei[ucpUnpack[6][1]];
      iSum[7] += ipWei[ucpUnpack[7][1]];
      iSum[8] += ipWei[ucpUnpack[8][1]];
      iSum[9] += ipWei[ucpUnpack[9][1]];
      iSum[10] += ipWei[ucpUnpack[10][1]];
      iSum[11] += ipWei[ucpUnpack[11][1]];
      iSum[12] += ipWei[ucpUnpack[12][1]];
      iSum[13] += ipWei[ucpUnpack[13][1]];
      iSum[14] += ipWei[ucpUnpack[14][1]];
      iSum[15] += ipWei[ucpUnpack[15][1]];
      iSum[16] += ipWei[ucpUnpack[16][1]];
      iSum[17] += ipWei[ucpUnpack[17][1]];
      iSum[18] += ipWei[ucpUnpack[18][1]];
      iSum[19] += ipWei[ucpUnpack[19][1]];
      iSum[20] += ipWei[ucpUnpack[20][1]];
      iSum[21] += ipWei[ucpUnpack[21][1]];
      iSum[22] += ipWei[ucpUnpack[22][1]];
      iSum[23] += ipWei[ucpUnpack[23][1]];

      ipWei = ipLUT[2];
      iSum[0] += ipWei[ucpUnpack[0][2]];
      iSum[1] += ipWei[ucpUnpack[1][2]];
      iSum[2] += ipWei[ucpUnpack[2][2]];
      iSum[3] += ipWei[ucpUnpack[3][2]];
      iSum[4] += ipWei[ucpUnpack[4][2]];
      iSum[5] += ipWei[ucpUnpack[5][2]];
      iSum[6] += ipWei[ucpUnpack[6][2]];
      iSum[7] += ipWei[ucpUnpack[7][2]];
      iSum[8] += ipWei[ucpUnpack[8][2]];
      iSum[9] += ipWei[ucpUnpack[9][2]];
      iSum[10] += ipWei[ucpUnpack[10][2]];
      iSum[11] += ipWei[ucpUnpack[11][2]];
      iSum[12] += ipWei[ucpUnpack[12][2]];
      iSum[13] += ipWei[ucpUnpack[13][2]];
      iSum[14] += ipWei[ucpUnpack[14][2]];
      iSum[15] += ipWei[ucpUnpack[15][2]];
      iSum[16] += ipWei[ucpUnpack[16][2]];
      iSum[17] += ipWei[ucpUnpack[17][2]];
      iSum[18] += ipWei[ucpUnpack[18][2]];
      iSum[19] += ipWei[ucpUnpack[19][2]];
      iSum[20] += ipWei[ucpUnpack[20][2]];
      iSum[21] += ipWei[ucpUnpack[21][2]];
      iSum[22] += ipWei[ucpUnpack[22][2]];
      iSum[23] += ipWei[ucpUnpack[23][2]];

      ipWei = ipLUT[3];
      iSum[0] += ipWei[ucpUnpack[0][3]];
      iSum[1] += ipWei[ucpUnpack[1][3]];
      iSum[2] += ipWei[ucpUnpack[2][3]];
      iSum[3] += ipWei[ucpUnpack[3][3]];
      iSum[4] += ipWei[ucpUnpack[4][3]];
      iSum[5] += ipWei[ucpUnpack[5][3]];
      iSum[6] += ipWei[ucpUnpack[6][3]];
      iSum[7] += ipWei[ucpUnpack[7][3]];
      iSum[8] += ipWei[ucpUnpack[8][3]];
      iSum[9] += ipWei[ucpUnpack[9][3]];
      iSum[10] += ipWei[ucpUnpack[10][3]];
      iSum[11] += ipWei[ucpUnpack[11][3]];
      iSum[12] += ipWei[ucpUnpack[12][3]];
      iSum[13] += ipWei[ucpUnpack[13][3]];
      iSum[14] += ipWei[ucpUnpack[14][3]];
      iSum[15] += ipWei[ucpUnpack[15][3]];
      iSum[16] += ipWei[ucpUnpack[16][3]];
      iSum[17] += ipWei[ucpUnpack[17][3]];
      iSum[18] += ipWei[ucpUnpack[18][3]];
      iSum[19] += ipWei[ucpUnpack[19][3]];
      iSum[20] += ipWei[ucpUnpack[20][3]];
      iSum[21] += ipWei[ucpUnpack[21][3]];
      iSum[22] += ipWei[ucpUnpack[22][3]];
      iSum[23] += ipWei[ucpUnpack[23][3]];

      ipWei = ipLUT[4];
      iSum[0] += ipWei[ucpUnpack[0][4]];
      iSum[1] += ipWei[ucpUnpack[1][4]];
      iSum[2] += ipWei[ucpUnpack[2][4]];
      iSum[3] += ipWei[ucpUnpack[3][4]];
      iSum[4] += ipWei[ucpUnpack[4][4]];
      iSum[5] += ipWei[ucpUnpack[5][4]];
      iSum[6] += ipWei[ucpUnpack[6][4]];
      iSum[7] += ipWei[ucpUnpack[7][4]];
      iSum[8] += ipWei[ucpUnpack[8][4]];
      iSum[9] += ipWei[ucpUnpack[9][4]];
      iSum[10] += ipWei[ucpUnpack[10][4]];
      iSum[11] += ipWei[ucpUnpack[11][4]];
      iSum[12] += ipWei[ucpUnpack[12][4]];
      iSum[13] += ipWei[ucpUnpack[13][4]];
      iSum[14] += ipWei[ucpUnpack[14][4]];
      iSum[15] += ipWei[ucpUnpack[15][4]];
      iSum[16] += ipWei[ucpUnpack[16][4]];
      iSum[17] += ipWei[ucpUnpack[17][4]];
      iSum[18] += ipWei[ucpUnpack[18][4]];
      iSum[19] += ipWei[ucpUnpack[19][4]];
      iSum[20] += ipWei[ucpUnpack[20][4]];
      iSum[21] += ipWei[ucpUnpack[21][4]];
      iSum[22] += ipWei[ucpUnpack[22][4]];
      iSum[23] += ipWei[ucpUnpack[23][4]];

      ipWei = ipLUT[5];
      iSum[0] += ipWei[ucpUnpack[0][5]];
      iSum[1] += ipWei[ucpUnpack[1][5]];
      iSum[2] += ipWei[ucpUnpack[2][5]];
      iSum[3] += ipWei[ucpUnpack[3][5]];
      iSum[4] += ipWei[ucpUnpack[4][5]];
      iSum[5] += ipWei[ucpUnpack[5][5]];
      iSum[6] += ipWei[ucpUnpack[6][5]];
      iSum[7] += ipWei[ucpUnpack[7][5]];
      iSum[8] += ipWei[ucpUnpack[8][5]];
      iSum[9] += ipWei[ucpUnpack[9][5]];
      iSum[10] += ipWei[ucpUnpack[10][5]];
      iSum[11] += ipWei[ucpUnpack[11][5]];
      iSum[12] += ipWei[ucpUnpack[12][5]];
      iSum[13] += ipWei[ucpUnpack[13][5]];
      iSum[14] += ipWei[ucpUnpack[14][5]];
      iSum[15] += ipWei[ucpUnpack[15][5]];
      iSum[16] += ipWei[ucpUnpack[16][5]];
      iSum[17] += ipWei[ucpUnpack[17][5]];
      iSum[18] += ipWei[ucpUnpack[18][5]];
      iSum[19] += ipWei[ucpUnpack[19][5]];
      iSum[20] += ipWei[ucpUnpack[20][5]];
      iSum[21] += ipWei[ucpUnpack[21][5]];
      iSum[22] += ipWei[ucpUnpack[22][5]];
      iSum[23] += ipWei[ucpUnpack[23][5]];

      ipWei = ipLUT[6];
      iSum[0] += ipWei[ucpUnpack[0][6]];
      iSum[1] += ipWei[ucpUnpack[1][6]];
      iSum[2] += ipWei[ucpUnpack[2][6]];
      iSum[3] += ipWei[ucpUnpack[3][6]];
      iSum[4] += ipWei[ucpUnpack[4][6]];
      iSum[5] += ipWei[ucpUnpack[5][6]];
      iSum[6] += ipWei[ucpUnpack[6][6]];
      iSum[7] += ipWei[ucpUnpack[7][6]];
      iSum[8] += ipWei[ucpUnpack[8][6]];
      iSum[9] += ipWei[ucpUnpack[9][6]];
      iSum[10] += ipWei[ucpUnpack[10][6]];
      iSum[11] += ipWei[ucpUnpack[11][6]];
      iSum[12] += ipWei[ucpUnpack[12][6]];
      iSum[13] += ipWei[ucpUnpack[13][6]];
      iSum[14] += ipWei[ucpUnpack[14][6]];
      iSum[15] += ipWei[ucpUnpack[15][6]];
      iSum[16] += ipWei[ucpUnpack[16][6]];
      iSum[17] += ipWei[ucpUnpack[17][6]];
      iSum[18] += ipWei[ucpUnpack[18][6]];
      iSum[19] += ipWei[ucpUnpack[19][6]];
      iSum[20] += ipWei[ucpUnpack[20][6]];
      iSum[21] += ipWei[ucpUnpack[21][6]];
      iSum[22] += ipWei[ucpUnpack[22][6]];
      iSum[23] += ipWei[ucpUnpack[23][6]];

      ipWei = ipLUT[7];
      iSum[0] += ipWei[ucpUnpack[0][7]];
      iSum[1] += ipWei[ucpUnpack[1][7]];
      iSum[2] += ipWei[ucpUnpack[2][7]];
      iSum[3] += ipWei[ucpUnpack[3][7]];
      iSum[4] += ipWei[ucpUnpack[4][7]];
      iSum[5] += ipWei[ucpUnpack[5][7]];
      iSum[6] += ipWei[ucpUnpack[6][7]];
      iSum[7] += ipWei[ucpUnpack[7][7]];
      iSum[8] += ipWei[ucpUnpack[8][7]];
      iSum[9] += ipWei[ucpUnpack[9][7]];
      iSum[10] += ipWei[ucpUnpack[10][7]];
      iSum[11] += ipWei[ucpUnpack[11][7]];
      iSum[12] += ipWei[ucpUnpack[12][7]];
      iSum[13] += ipWei[ucpUnpack[13][7]];
      iSum[14] += ipWei[ucpUnpack[14][7]];
      iSum[15] += ipWei[ucpUnpack[15][7]];
      iSum[16] += ipWei[ucpUnpack[16][7]];
      iSum[17] += ipWei[ucpUnpack[17][7]];
      iSum[18] += ipWei[ucpUnpack[18][7]];
      iSum[19] += ipWei[ucpUnpack[19][7]];
      iSum[20] += ipWei[ucpUnpack[20][7]];
      iSum[21] += ipWei[ucpUnpack[21][7]];
      iSum[22] += ipWei[ucpUnpack[22][7]];
      iSum[23] += ipWei[ucpUnpack[23][7]];

      ipWei = ipLUT[8];
      iSum[0] += ipWei[ucpUnpack[0][8]];
      iSum[1] += ipWei[ucpUnpack[1][8]];
      iSum[2] += ipWei[ucpUnpack[2][8]];
      iSum[3] += ipWei[ucpUnpack[3][8]];
      iSum[4] += ipWei[ucpUnpack[4][8]];
      iSum[5] += ipWei[ucpUnpack[5][8]];
      iSum[6] += ipWei[ucpUnpack[6][8]];
      iSum[7] += ipWei[ucpUnpack[7][8]];
      iSum[8] += ipWei[ucpUnpack[8][8]];
      iSum[9] += ipWei[ucpUnpack[9][8]];
      iSum[10] += ipWei[ucpUnpack[10][8]];
      iSum[11] += ipWei[ucpUnpack[11][8]];
      iSum[12] += ipWei[ucpUnpack[12][8]];
      iSum[13] += ipWei[ucpUnpack[13][8]];
      iSum[14] += ipWei[ucpUnpack[14][8]];
      iSum[15] += ipWei[ucpUnpack[15][8]];
      iSum[16] += ipWei[ucpUnpack[16][8]];
      iSum[17] += ipWei[ucpUnpack[17][8]];
      iSum[18] += ipWei[ucpUnpack[18][8]];
      iSum[19] += ipWei[ucpUnpack[19][8]];
      iSum[20] += ipWei[ucpUnpack[20][8]];
      iSum[21] += ipWei[ucpUnpack[21][8]];
      iSum[22] += ipWei[ucpUnpack[22][8]];
      iSum[23] += ipWei[ucpUnpack[23][8]];

      // clamp and dump the results

      // no rounding - sums were pre-rounded
      // I commented out the "& 0xFF" because I assume the min/max
      // code will take care of making sure the sums are not negative;
      // if that assumption is not valid, they need to be uncommented
      long lTmp1, lTmp2;

      lTmp1 = iSum[0];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[1];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[2];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[3];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent] = lTmp1;

      lTmp1 = iSum[4];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[5];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[6];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[7];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 4] = lTmp1;

      lTmp1 = iSum[8];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[9];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[10];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[11];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 8] = lTmp1;

      lTmp1 = iSum[12];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[13];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[14];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[15];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 12] = lTmp1;

      lTmp1 = iSum[16];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[17];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[18];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[19];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 16] = lTmp1;

      lTmp1 = iSum[20];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[21];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[22];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[23];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 20] = lTmp1;
     }
    ipSP += 2;
    ipP += 2;
   }
}

void CAspectRatio::RenderV_YUV_11 (MTI_UINT8 *ucpDst[2], int &iRow, int iStopDstLocal,
		int iJob)
/*
	Do VERTICAL rendering with 11 neighbors
*/
{
  MTI_UINT8 *ucpD;
  int iField, iR, *ipSP, *ipP, iPixel, iNei;
  int **ipLUT, iSubPixel;
  int iSum[VERTICAL_COMPONENTS], *ipWei;
  MTI_UINT8 *ucpUnpack[VERTICAL_COMPONENTS];
  int iComponent;

  // check for errors
  if ((iRow & 0x1) || (iStopDstLocal & 0x1))
    return;

  int iField0, iField1;

  if (iNFieldDst == 2)
   {
    iField0 = 0;
    iField1 = 1;
   }
  else
   {
    iField0 = 0;
    iField1 = 0;
   }


  ipSP = ipLUTSubPixel + iRow;
  ipP = ipLUTPixel + iRow;
  for (; iRow < iStopDstLocal; iRow+=2)
   {
    // do the "even" field
    iR = iRow / iNFieldDst;

    ucpD = ucpDst[iField0] + (iR * iPitchDst);

    iSubPixel = ipSP[0];
    iPixel = ipP[0];

    for (iComponent = 0; iComponent < iUnpackedComponent; iComponent += VERTICAL_COMPONENTS)
     {
      ipLUT = ipLUTWei[iSubPixel];
      ucpUnpack[0] = ucpUnpacked[iJob][iComponent] + iPixel;
      ucpUnpack[1] = ucpUnpacked[iJob][iComponent + 1] + iPixel;
      ucpUnpack[2] = ucpUnpacked[iJob][iComponent + 2] + iPixel;
      ucpUnpack[3] = ucpUnpacked[iJob][iComponent + 3] + iPixel;
      ucpUnpack[4] = ucpUnpacked[iJob][iComponent + 4] + iPixel;
      ucpUnpack[5] = ucpUnpacked[iJob][iComponent + 5] + iPixel;
      ucpUnpack[6] = ucpUnpacked[iJob][iComponent + 6] + iPixel;
      ucpUnpack[7] = ucpUnpacked[iJob][iComponent + 7] + iPixel;
      ucpUnpack[8] = ucpUnpacked[iJob][iComponent + 8] + iPixel;
      ucpUnpack[9] = ucpUnpacked[iJob][iComponent + 9] + iPixel;
      ucpUnpack[10] = ucpUnpacked[iJob][iComponent + 10] + iPixel;
      ucpUnpack[11] = ucpUnpacked[iJob][iComponent + 11] + iPixel;
      ucpUnpack[12] = ucpUnpacked[iJob][iComponent + 12] + iPixel;
      ucpUnpack[13] = ucpUnpacked[iJob][iComponent + 13] + iPixel;
      ucpUnpack[14] = ucpUnpacked[iJob][iComponent + 14] + iPixel;
      ucpUnpack[15] = ucpUnpacked[iJob][iComponent + 15] + iPixel;
      ucpUnpack[16] = ucpUnpacked[iJob][iComponent + 16] + iPixel;
      ucpUnpack[17] = ucpUnpacked[iJob][iComponent + 17] + iPixel;
      ucpUnpack[18] = ucpUnpacked[iJob][iComponent + 18] + iPixel;
      ucpUnpack[19] = ucpUnpacked[iJob][iComponent + 19] + iPixel;
      ucpUnpack[20] = ucpUnpacked[iJob][iComponent + 20] + iPixel;
      ucpUnpack[21] = ucpUnpacked[iJob][iComponent + 21] + iPixel;
      ucpUnpack[22] = ucpUnpacked[iJob][iComponent + 22] + iPixel;
      ucpUnpack[23] = ucpUnpacked[iJob][iComponent + 23] + iPixel;

      // pre-round the sums
      iSum[0] = iSum[1] = iSum[2] = iSum[3] =
      iSum[4] = iSum[5] = iSum[6] = iSum[7] =
      iSum[8] = iSum[9] = iSum[10] = iSum[11] =
      iSum[12] = iSum[13] = iSum[14] = iSum[15] =
      iSum[16] = iSum[17] = iSum[18] = iSum[19] =
      iSum[20] = iSum[21] = iSum[22] = iSum[23] =
                  iHalfPrecision;

      // do the 11 neighbors
      ipWei = ipLUT[0];
      iSum[0] += ipWei[ucpUnpack[0][0]];
      iSum[1] += ipWei[ucpUnpack[1][0]];
      iSum[2] += ipWei[ucpUnpack[2][0]];
      iSum[3] += ipWei[ucpUnpack[3][0]];
      iSum[4] += ipWei[ucpUnpack[4][0]];
      iSum[5] += ipWei[ucpUnpack[5][0]];
      iSum[6] += ipWei[ucpUnpack[6][0]];
      iSum[7] += ipWei[ucpUnpack[7][0]];
      iSum[8] += ipWei[ucpUnpack[8][0]];
      iSum[9] += ipWei[ucpUnpack[9][0]];
      iSum[10] += ipWei[ucpUnpack[10][0]];
      iSum[11] += ipWei[ucpUnpack[11][0]];
      iSum[12] += ipWei[ucpUnpack[12][0]];
      iSum[13] += ipWei[ucpUnpack[13][0]];
      iSum[14] += ipWei[ucpUnpack[14][0]];
      iSum[15] += ipWei[ucpUnpack[15][0]];
      iSum[16] += ipWei[ucpUnpack[16][0]];
      iSum[17] += ipWei[ucpUnpack[17][0]];
      iSum[18] += ipWei[ucpUnpack[18][0]];
      iSum[19] += ipWei[ucpUnpack[19][0]];
      iSum[20] += ipWei[ucpUnpack[20][0]];
      iSum[21] += ipWei[ucpUnpack[21][0]];
      iSum[22] += ipWei[ucpUnpack[22][0]];
      iSum[23] += ipWei[ucpUnpack[23][0]];

      ipWei = ipLUT[1];
      iSum[0] += ipWei[ucpUnpack[0][1]];
      iSum[1] += ipWei[ucpUnpack[1][1]];
      iSum[2] += ipWei[ucpUnpack[2][1]];
      iSum[3] += ipWei[ucpUnpack[3][1]];
      iSum[4] += ipWei[ucpUnpack[4][1]];
      iSum[5] += ipWei[ucpUnpack[5][1]];
      iSum[6] += ipWei[ucpUnpack[6][1]];
      iSum[7] += ipWei[ucpUnpack[7][1]];
      iSum[8] += ipWei[ucpUnpack[8][1]];
      iSum[9] += ipWei[ucpUnpack[9][1]];
      iSum[10] += ipWei[ucpUnpack[10][1]];
      iSum[11] += ipWei[ucpUnpack[11][1]];
      iSum[12] += ipWei[ucpUnpack[12][1]];
      iSum[13] += ipWei[ucpUnpack[13][1]];
      iSum[14] += ipWei[ucpUnpack[14][1]];
      iSum[15] += ipWei[ucpUnpack[15][1]];
      iSum[16] += ipWei[ucpUnpack[16][1]];
      iSum[17] += ipWei[ucpUnpack[17][1]];
      iSum[18] += ipWei[ucpUnpack[18][1]];
      iSum[19] += ipWei[ucpUnpack[19][1]];
      iSum[20] += ipWei[ucpUnpack[20][1]];
      iSum[21] += ipWei[ucpUnpack[21][1]];
      iSum[22] += ipWei[ucpUnpack[22][1]];
      iSum[23] += ipWei[ucpUnpack[23][1]];

      ipWei = ipLUT[2];
      iSum[0] += ipWei[ucpUnpack[0][2]];
      iSum[1] += ipWei[ucpUnpack[1][2]];
      iSum[2] += ipWei[ucpUnpack[2][2]];
      iSum[3] += ipWei[ucpUnpack[3][2]];
      iSum[4] += ipWei[ucpUnpack[4][2]];
      iSum[5] += ipWei[ucpUnpack[5][2]];
      iSum[6] += ipWei[ucpUnpack[6][2]];
      iSum[7] += ipWei[ucpUnpack[7][2]];
      iSum[8] += ipWei[ucpUnpack[8][2]];
      iSum[9] += ipWei[ucpUnpack[9][2]];
      iSum[10] += ipWei[ucpUnpack[10][2]];
      iSum[11] += ipWei[ucpUnpack[11][2]];
      iSum[12] += ipWei[ucpUnpack[12][2]];
      iSum[13] += ipWei[ucpUnpack[13][2]];
      iSum[14] += ipWei[ucpUnpack[14][2]];
      iSum[15] += ipWei[ucpUnpack[15][2]];
      iSum[16] += ipWei[ucpUnpack[16][2]];
      iSum[17] += ipWei[ucpUnpack[17][2]];
      iSum[18] += ipWei[ucpUnpack[18][2]];
      iSum[19] += ipWei[ucpUnpack[19][2]];
      iSum[20] += ipWei[ucpUnpack[20][2]];
      iSum[21] += ipWei[ucpUnpack[21][2]];
      iSum[22] += ipWei[ucpUnpack[22][2]];
      iSum[23] += ipWei[ucpUnpack[23][2]];

      ipWei = ipLUT[3];
      iSum[0] += ipWei[ucpUnpack[0][3]];
      iSum[1] += ipWei[ucpUnpack[1][3]];
      iSum[2] += ipWei[ucpUnpack[2][3]];
      iSum[3] += ipWei[ucpUnpack[3][3]];
      iSum[4] += ipWei[ucpUnpack[4][3]];
      iSum[5] += ipWei[ucpUnpack[5][3]];
      iSum[6] += ipWei[ucpUnpack[6][3]];
      iSum[7] += ipWei[ucpUnpack[7][3]];
      iSum[8] += ipWei[ucpUnpack[8][3]];
      iSum[9] += ipWei[ucpUnpack[9][3]];
      iSum[10] += ipWei[ucpUnpack[10][3]];
      iSum[11] += ipWei[ucpUnpack[11][3]];
      iSum[12] += ipWei[ucpUnpack[12][3]];
      iSum[13] += ipWei[ucpUnpack[13][3]];
      iSum[14] += ipWei[ucpUnpack[14][3]];
      iSum[15] += ipWei[ucpUnpack[15][3]];
      iSum[16] += ipWei[ucpUnpack[16][3]];
      iSum[17] += ipWei[ucpUnpack[17][3]];
      iSum[18] += ipWei[ucpUnpack[18][3]];
      iSum[19] += ipWei[ucpUnpack[19][3]];
      iSum[20] += ipWei[ucpUnpack[20][3]];
      iSum[21] += ipWei[ucpUnpack[21][3]];
      iSum[22] += ipWei[ucpUnpack[22][3]];
      iSum[23] += ipWei[ucpUnpack[23][3]];

      ipWei = ipLUT[4];
      iSum[0] += ipWei[ucpUnpack[0][4]];
      iSum[1] += ipWei[ucpUnpack[1][4]];
      iSum[2] += ipWei[ucpUnpack[2][4]];
      iSum[3] += ipWei[ucpUnpack[3][4]];
      iSum[4] += ipWei[ucpUnpack[4][4]];
      iSum[5] += ipWei[ucpUnpack[5][4]];
      iSum[6] += ipWei[ucpUnpack[6][4]];
      iSum[7] += ipWei[ucpUnpack[7][4]];
      iSum[8] += ipWei[ucpUnpack[8][4]];
      iSum[9] += ipWei[ucpUnpack[9][4]];
      iSum[10] += ipWei[ucpUnpack[10][4]];
      iSum[11] += ipWei[ucpUnpack[11][4]];
      iSum[12] += ipWei[ucpUnpack[12][4]];
      iSum[13] += ipWei[ucpUnpack[13][4]];
      iSum[14] += ipWei[ucpUnpack[14][4]];
      iSum[15] += ipWei[ucpUnpack[15][4]];
      iSum[16] += ipWei[ucpUnpack[16][4]];
      iSum[17] += ipWei[ucpUnpack[17][4]];
      iSum[18] += ipWei[ucpUnpack[18][4]];
      iSum[19] += ipWei[ucpUnpack[19][4]];
      iSum[20] += ipWei[ucpUnpack[20][4]];
      iSum[21] += ipWei[ucpUnpack[21][4]];
      iSum[22] += ipWei[ucpUnpack[22][4]];
      iSum[23] += ipWei[ucpUnpack[23][4]];

      ipWei = ipLUT[5];
      iSum[0] += ipWei[ucpUnpack[0][5]];
      iSum[1] += ipWei[ucpUnpack[1][5]];
      iSum[2] += ipWei[ucpUnpack[2][5]];
      iSum[3] += ipWei[ucpUnpack[3][5]];
      iSum[4] += ipWei[ucpUnpack[4][5]];
      iSum[5] += ipWei[ucpUnpack[5][5]];
      iSum[6] += ipWei[ucpUnpack[6][5]];
      iSum[7] += ipWei[ucpUnpack[7][5]];
      iSum[8] += ipWei[ucpUnpack[8][5]];
      iSum[9] += ipWei[ucpUnpack[9][5]];
      iSum[10] += ipWei[ucpUnpack[10][5]];
      iSum[11] += ipWei[ucpUnpack[11][5]];
      iSum[12] += ipWei[ucpUnpack[12][5]];
      iSum[13] += ipWei[ucpUnpack[13][5]];
      iSum[14] += ipWei[ucpUnpack[14][5]];
      iSum[15] += ipWei[ucpUnpack[15][5]];
      iSum[16] += ipWei[ucpUnpack[16][5]];
      iSum[17] += ipWei[ucpUnpack[17][5]];
      iSum[18] += ipWei[ucpUnpack[18][5]];
      iSum[19] += ipWei[ucpUnpack[19][5]];
      iSum[20] += ipWei[ucpUnpack[20][5]];
      iSum[21] += ipWei[ucpUnpack[21][5]];
      iSum[22] += ipWei[ucpUnpack[22][5]];
      iSum[23] += ipWei[ucpUnpack[23][5]];

      ipWei = ipLUT[6];
      iSum[0] += ipWei[ucpUnpack[0][6]];
      iSum[1] += ipWei[ucpUnpack[1][6]];
      iSum[2] += ipWei[ucpUnpack[2][6]];
      iSum[3] += ipWei[ucpUnpack[3][6]];
      iSum[4] += ipWei[ucpUnpack[4][6]];
      iSum[5] += ipWei[ucpUnpack[5][6]];
      iSum[6] += ipWei[ucpUnpack[6][6]];
      iSum[7] += ipWei[ucpUnpack[7][6]];
      iSum[8] += ipWei[ucpUnpack[8][6]];
      iSum[9] += ipWei[ucpUnpack[9][6]];
      iSum[10] += ipWei[ucpUnpack[10][6]];
      iSum[11] += ipWei[ucpUnpack[11][6]];
      iSum[12] += ipWei[ucpUnpack[12][6]];
      iSum[13] += ipWei[ucpUnpack[13][6]];
      iSum[14] += ipWei[ucpUnpack[14][6]];
      iSum[15] += ipWei[ucpUnpack[15][6]];
      iSum[16] += ipWei[ucpUnpack[16][6]];
      iSum[17] += ipWei[ucpUnpack[17][6]];
      iSum[18] += ipWei[ucpUnpack[18][6]];
      iSum[19] += ipWei[ucpUnpack[19][6]];
      iSum[20] += ipWei[ucpUnpack[20][6]];
      iSum[21] += ipWei[ucpUnpack[21][6]];
      iSum[22] += ipWei[ucpUnpack[22][6]];
      iSum[23] += ipWei[ucpUnpack[23][6]];

      ipWei = ipLUT[7];
      iSum[0] += ipWei[ucpUnpack[0][7]];
      iSum[1] += ipWei[ucpUnpack[1][7]];
      iSum[2] += ipWei[ucpUnpack[2][7]];
      iSum[3] += ipWei[ucpUnpack[3][7]];
      iSum[4] += ipWei[ucpUnpack[4][7]];
      iSum[5] += ipWei[ucpUnpack[5][7]];
      iSum[6] += ipWei[ucpUnpack[6][7]];
      iSum[7] += ipWei[ucpUnpack[7][7]];
      iSum[8] += ipWei[ucpUnpack[8][7]];
      iSum[9] += ipWei[ucpUnpack[9][7]];
      iSum[10] += ipWei[ucpUnpack[10][7]];
      iSum[11] += ipWei[ucpUnpack[11][7]];
      iSum[12] += ipWei[ucpUnpack[12][7]];
      iSum[13] += ipWei[ucpUnpack[13][7]];
      iSum[14] += ipWei[ucpUnpack[14][7]];
      iSum[15] += ipWei[ucpUnpack[15][7]];
      iSum[16] += ipWei[ucpUnpack[16][7]];
      iSum[17] += ipWei[ucpUnpack[17][7]];
      iSum[18] += ipWei[ucpUnpack[18][7]];
      iSum[19] += ipWei[ucpUnpack[19][7]];
      iSum[20] += ipWei[ucpUnpack[20][7]];
      iSum[21] += ipWei[ucpUnpack[21][7]];
      iSum[22] += ipWei[ucpUnpack[22][7]];
      iSum[23] += ipWei[ucpUnpack[23][7]];

      ipWei = ipLUT[8];
      iSum[0] += ipWei[ucpUnpack[0][8]];
      iSum[1] += ipWei[ucpUnpack[1][8]];
      iSum[2] += ipWei[ucpUnpack[2][8]];
      iSum[3] += ipWei[ucpUnpack[3][8]];
      iSum[4] += ipWei[ucpUnpack[4][8]];
      iSum[5] += ipWei[ucpUnpack[5][8]];
      iSum[6] += ipWei[ucpUnpack[6][8]];
      iSum[7] += ipWei[ucpUnpack[7][8]];
      iSum[8] += ipWei[ucpUnpack[8][8]];
      iSum[9] += ipWei[ucpUnpack[9][8]];
      iSum[10] += ipWei[ucpUnpack[10][8]];
      iSum[11] += ipWei[ucpUnpack[11][8]];
      iSum[12] += ipWei[ucpUnpack[12][8]];
      iSum[13] += ipWei[ucpUnpack[13][8]];
      iSum[14] += ipWei[ucpUnpack[14][8]];
      iSum[15] += ipWei[ucpUnpack[15][8]];
      iSum[16] += ipWei[ucpUnpack[16][8]];
      iSum[17] += ipWei[ucpUnpack[17][8]];
      iSum[18] += ipWei[ucpUnpack[18][8]];
      iSum[19] += ipWei[ucpUnpack[19][8]];
      iSum[20] += ipWei[ucpUnpack[20][8]];
      iSum[21] += ipWei[ucpUnpack[21][8]];
      iSum[22] += ipWei[ucpUnpack[22][8]];
      iSum[23] += ipWei[ucpUnpack[23][8]];

      ipWei = ipLUT[9];
      iSum[0] += ipWei[ucpUnpack[0][9]];
      iSum[1] += ipWei[ucpUnpack[1][9]];
      iSum[2] += ipWei[ucpUnpack[2][9]];
      iSum[3] += ipWei[ucpUnpack[3][9]];
      iSum[4] += ipWei[ucpUnpack[4][9]];
      iSum[5] += ipWei[ucpUnpack[5][9]];
      iSum[6] += ipWei[ucpUnpack[6][9]];
      iSum[7] += ipWei[ucpUnpack[7][9]];
      iSum[8] += ipWei[ucpUnpack[8][9]];
      iSum[9] += ipWei[ucpUnpack[9][9]];
      iSum[10] += ipWei[ucpUnpack[10][9]];
      iSum[11] += ipWei[ucpUnpack[11][9]];
      iSum[12] += ipWei[ucpUnpack[12][9]];
      iSum[13] += ipWei[ucpUnpack[13][9]];
      iSum[14] += ipWei[ucpUnpack[14][9]];
      iSum[15] += ipWei[ucpUnpack[15][9]];
      iSum[16] += ipWei[ucpUnpack[16][9]];
      iSum[17] += ipWei[ucpUnpack[17][9]];
      iSum[18] += ipWei[ucpUnpack[18][9]];
      iSum[19] += ipWei[ucpUnpack[19][9]];
      iSum[20] += ipWei[ucpUnpack[20][9]];
      iSum[21] += ipWei[ucpUnpack[21][9]];
      iSum[22] += ipWei[ucpUnpack[22][9]];
      iSum[23] += ipWei[ucpUnpack[23][9]];

      ipWei = ipLUT[10];
      iSum[0] += ipWei[ucpUnpack[0][10]];
      iSum[1] += ipWei[ucpUnpack[1][10]];
      iSum[2] += ipWei[ucpUnpack[2][10]];
      iSum[3] += ipWei[ucpUnpack[3][10]];
      iSum[4] += ipWei[ucpUnpack[4][10]];
      iSum[5] += ipWei[ucpUnpack[5][10]];
      iSum[6] += ipWei[ucpUnpack[6][10]];
      iSum[7] += ipWei[ucpUnpack[7][10]];
      iSum[8] += ipWei[ucpUnpack[8][10]];
      iSum[9] += ipWei[ucpUnpack[9][10]];
      iSum[10] += ipWei[ucpUnpack[10][10]];
      iSum[11] += ipWei[ucpUnpack[11][10]];
      iSum[12] += ipWei[ucpUnpack[12][10]];
      iSum[13] += ipWei[ucpUnpack[13][10]];
      iSum[14] += ipWei[ucpUnpack[14][10]];
      iSum[15] += ipWei[ucpUnpack[15][10]];
      iSum[16] += ipWei[ucpUnpack[16][10]];
      iSum[17] += ipWei[ucpUnpack[17][10]];
      iSum[18] += ipWei[ucpUnpack[18][10]];
      iSum[19] += ipWei[ucpUnpack[19][10]];
      iSum[20] += ipWei[ucpUnpack[20][10]];
      iSum[21] += ipWei[ucpUnpack[21][10]];
      iSum[22] += ipWei[ucpUnpack[22][10]];
      iSum[23] += ipWei[ucpUnpack[23][10]];

      // clamp and dump the results

      // no rounding - sums were pre-rounded
      // I commented out the "& 0xFF" because I assume the min/max
      // code will take care of making sure the sums are not negative;
      // if that assumption is not valid, they need to be uncommented
      long lTmp1, lTmp2;

      lTmp1 = iSum[0];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[1];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[2];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[3];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent] = lTmp1;

      lTmp1 = iSum[4];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[5];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[6];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[7];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 4] = lTmp1;

      lTmp1 = iSum[8];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[9];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[10];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[11];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 8] = lTmp1;

      lTmp1 = iSum[12];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[13];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[14];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[15];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 12] = lTmp1;

      lTmp1 = iSum[16];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[17];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[18];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[19];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 16] = lTmp1;

      lTmp1 = iSum[20];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[21];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[22];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[23];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 20] = lTmp1;
    }

    // do the "odd" field
    iR = (iRow+1) / iNFieldDst;

    ucpD = ucpDst[iField1] + (iR * iPitchDst);

    iSubPixel = ipSP[1];
    iPixel = ipP[1];

    for (iComponent = 0; iComponent < iUnpackedComponent; iComponent += VERTICAL_COMPONENTS)
     {
      ipLUT = ipLUTWei[iSubPixel];
      ucpUnpack[0] = ucpUnpacked[iJob][iComponent] + iPixel;
      ucpUnpack[1] = ucpUnpacked[iJob][iComponent + 1] + iPixel;
      ucpUnpack[2] = ucpUnpacked[iJob][iComponent + 2] + iPixel;
      ucpUnpack[3] = ucpUnpacked[iJob][iComponent + 3] + iPixel;
      ucpUnpack[4] = ucpUnpacked[iJob][iComponent + 4] + iPixel;
      ucpUnpack[5] = ucpUnpacked[iJob][iComponent + 5] + iPixel;
      ucpUnpack[6] = ucpUnpacked[iJob][iComponent + 6] + iPixel;
      ucpUnpack[7] = ucpUnpacked[iJob][iComponent + 7] + iPixel;
      ucpUnpack[8] = ucpUnpacked[iJob][iComponent + 8] + iPixel;
      ucpUnpack[9] = ucpUnpacked[iJob][iComponent + 9] + iPixel;
      ucpUnpack[10] = ucpUnpacked[iJob][iComponent + 10] + iPixel;
      ucpUnpack[11] = ucpUnpacked[iJob][iComponent + 11] + iPixel;
      ucpUnpack[12] = ucpUnpacked[iJob][iComponent + 12] + iPixel;
      ucpUnpack[13] = ucpUnpacked[iJob][iComponent + 13] + iPixel;
      ucpUnpack[14] = ucpUnpacked[iJob][iComponent + 14] + iPixel;
      ucpUnpack[15] = ucpUnpacked[iJob][iComponent + 15] + iPixel;
      ucpUnpack[16] = ucpUnpacked[iJob][iComponent + 16] + iPixel;
      ucpUnpack[17] = ucpUnpacked[iJob][iComponent + 17] + iPixel;
      ucpUnpack[18] = ucpUnpacked[iJob][iComponent + 18] + iPixel;
      ucpUnpack[19] = ucpUnpacked[iJob][iComponent + 19] + iPixel;
      ucpUnpack[20] = ucpUnpacked[iJob][iComponent + 20] + iPixel;
      ucpUnpack[21] = ucpUnpacked[iJob][iComponent + 21] + iPixel;
      ucpUnpack[22] = ucpUnpacked[iJob][iComponent + 22] + iPixel;
      ucpUnpack[23] = ucpUnpacked[iJob][iComponent + 23] + iPixel;

      // pre-round the sums
      iSum[0] = iSum[1] = iSum[2] = iSum[3] =
      iSum[4] = iSum[5] = iSum[6] = iSum[7] =
      iSum[8] = iSum[9] = iSum[10] = iSum[11] =
      iSum[12] = iSum[13] = iSum[14] = iSum[15] =
      iSum[16] = iSum[17] = iSum[18] = iSum[19] =
      iSum[20] = iSum[21] = iSum[22] = iSum[23] =
                  iHalfPrecision;

      // do the 11 neighbors
      ipWei = ipLUT[0];
      iSum[0] += ipWei[ucpUnpack[0][0]];
      iSum[1] += ipWei[ucpUnpack[1][0]];
      iSum[2] += ipWei[ucpUnpack[2][0]];
      iSum[3] += ipWei[ucpUnpack[3][0]];
      iSum[4] += ipWei[ucpUnpack[4][0]];
      iSum[5] += ipWei[ucpUnpack[5][0]];
      iSum[6] += ipWei[ucpUnpack[6][0]];
      iSum[7] += ipWei[ucpUnpack[7][0]];
      iSum[8] += ipWei[ucpUnpack[8][0]];
      iSum[9] += ipWei[ucpUnpack[9][0]];
      iSum[10] += ipWei[ucpUnpack[10][0]];
      iSum[11] += ipWei[ucpUnpack[11][0]];
      iSum[12] += ipWei[ucpUnpack[12][0]];
      iSum[13] += ipWei[ucpUnpack[13][0]];
      iSum[14] += ipWei[ucpUnpack[14][0]];
      iSum[15] += ipWei[ucpUnpack[15][0]];
      iSum[16] += ipWei[ucpUnpack[16][0]];
      iSum[17] += ipWei[ucpUnpack[17][0]];
      iSum[18] += ipWei[ucpUnpack[18][0]];
      iSum[19] += ipWei[ucpUnpack[19][0]];
      iSum[20] += ipWei[ucpUnpack[20][0]];
      iSum[21] += ipWei[ucpUnpack[21][0]];
      iSum[22] += ipWei[ucpUnpack[22][0]];
      iSum[23] += ipWei[ucpUnpack[23][0]];

      ipWei = ipLUT[1];
      iSum[0] += ipWei[ucpUnpack[0][1]];
      iSum[1] += ipWei[ucpUnpack[1][1]];
      iSum[2] += ipWei[ucpUnpack[2][1]];
      iSum[3] += ipWei[ucpUnpack[3][1]];
      iSum[4] += ipWei[ucpUnpack[4][1]];
      iSum[5] += ipWei[ucpUnpack[5][1]];
      iSum[6] += ipWei[ucpUnpack[6][1]];
      iSum[7] += ipWei[ucpUnpack[7][1]];
      iSum[8] += ipWei[ucpUnpack[8][1]];
      iSum[9] += ipWei[ucpUnpack[9][1]];
      iSum[10] += ipWei[ucpUnpack[10][1]];
      iSum[11] += ipWei[ucpUnpack[11][1]];
      iSum[12] += ipWei[ucpUnpack[12][1]];
      iSum[13] += ipWei[ucpUnpack[13][1]];
      iSum[14] += ipWei[ucpUnpack[14][1]];
      iSum[15] += ipWei[ucpUnpack[15][1]];
      iSum[16] += ipWei[ucpUnpack[16][1]];
      iSum[17] += ipWei[ucpUnpack[17][1]];
      iSum[18] += ipWei[ucpUnpack[18][1]];
      iSum[19] += ipWei[ucpUnpack[19][1]];
      iSum[20] += ipWei[ucpUnpack[20][1]];
      iSum[21] += ipWei[ucpUnpack[21][1]];
      iSum[22] += ipWei[ucpUnpack[22][1]];
      iSum[23] += ipWei[ucpUnpack[23][1]];

      ipWei = ipLUT[2];
      iSum[0] += ipWei[ucpUnpack[0][2]];
      iSum[1] += ipWei[ucpUnpack[1][2]];
      iSum[2] += ipWei[ucpUnpack[2][2]];
      iSum[3] += ipWei[ucpUnpack[3][2]];
      iSum[4] += ipWei[ucpUnpack[4][2]];
      iSum[5] += ipWei[ucpUnpack[5][2]];
      iSum[6] += ipWei[ucpUnpack[6][2]];
      iSum[7] += ipWei[ucpUnpack[7][2]];
      iSum[8] += ipWei[ucpUnpack[8][2]];
      iSum[9] += ipWei[ucpUnpack[9][2]];
      iSum[10] += ipWei[ucpUnpack[10][2]];
      iSum[11] += ipWei[ucpUnpack[11][2]];
      iSum[12] += ipWei[ucpUnpack[12][2]];
      iSum[13] += ipWei[ucpUnpack[13][2]];
      iSum[14] += ipWei[ucpUnpack[14][2]];
      iSum[15] += ipWei[ucpUnpack[15][2]];
      iSum[16] += ipWei[ucpUnpack[16][2]];
      iSum[17] += ipWei[ucpUnpack[17][2]];
      iSum[18] += ipWei[ucpUnpack[18][2]];
      iSum[19] += ipWei[ucpUnpack[19][2]];
      iSum[20] += ipWei[ucpUnpack[20][2]];
      iSum[21] += ipWei[ucpUnpack[21][2]];
      iSum[22] += ipWei[ucpUnpack[22][2]];
      iSum[23] += ipWei[ucpUnpack[23][2]];

      ipWei = ipLUT[3];
      iSum[0] += ipWei[ucpUnpack[0][3]];
      iSum[1] += ipWei[ucpUnpack[1][3]];
      iSum[2] += ipWei[ucpUnpack[2][3]];
      iSum[3] += ipWei[ucpUnpack[3][3]];
      iSum[4] += ipWei[ucpUnpack[4][3]];
      iSum[5] += ipWei[ucpUnpack[5][3]];
      iSum[6] += ipWei[ucpUnpack[6][3]];
      iSum[7] += ipWei[ucpUnpack[7][3]];
      iSum[8] += ipWei[ucpUnpack[8][3]];
      iSum[9] += ipWei[ucpUnpack[9][3]];
      iSum[10] += ipWei[ucpUnpack[10][3]];
      iSum[11] += ipWei[ucpUnpack[11][3]];
      iSum[12] += ipWei[ucpUnpack[12][3]];
      iSum[13] += ipWei[ucpUnpack[13][3]];
      iSum[14] += ipWei[ucpUnpack[14][3]];
      iSum[15] += ipWei[ucpUnpack[15][3]];
      iSum[16] += ipWei[ucpUnpack[16][3]];
      iSum[17] += ipWei[ucpUnpack[17][3]];
      iSum[18] += ipWei[ucpUnpack[18][3]];
      iSum[19] += ipWei[ucpUnpack[19][3]];
      iSum[20] += ipWei[ucpUnpack[20][3]];
      iSum[21] += ipWei[ucpUnpack[21][3]];
      iSum[22] += ipWei[ucpUnpack[22][3]];
      iSum[23] += ipWei[ucpUnpack[23][3]];

      ipWei = ipLUT[4];
      iSum[0] += ipWei[ucpUnpack[0][4]];
      iSum[1] += ipWei[ucpUnpack[1][4]];
      iSum[2] += ipWei[ucpUnpack[2][4]];
      iSum[3] += ipWei[ucpUnpack[3][4]];
      iSum[4] += ipWei[ucpUnpack[4][4]];
      iSum[5] += ipWei[ucpUnpack[5][4]];
      iSum[6] += ipWei[ucpUnpack[6][4]];
      iSum[7] += ipWei[ucpUnpack[7][4]];
      iSum[8] += ipWei[ucpUnpack[8][4]];
      iSum[9] += ipWei[ucpUnpack[9][4]];
      iSum[10] += ipWei[ucpUnpack[10][4]];
      iSum[11] += ipWei[ucpUnpack[11][4]];
      iSum[12] += ipWei[ucpUnpack[12][4]];
      iSum[13] += ipWei[ucpUnpack[13][4]];
      iSum[14] += ipWei[ucpUnpack[14][4]];
      iSum[15] += ipWei[ucpUnpack[15][4]];
      iSum[16] += ipWei[ucpUnpack[16][4]];
      iSum[17] += ipWei[ucpUnpack[17][4]];
      iSum[18] += ipWei[ucpUnpack[18][4]];
      iSum[19] += ipWei[ucpUnpack[19][4]];
      iSum[20] += ipWei[ucpUnpack[20][4]];
      iSum[21] += ipWei[ucpUnpack[21][4]];
      iSum[22] += ipWei[ucpUnpack[22][4]];
      iSum[23] += ipWei[ucpUnpack[23][4]];

      ipWei = ipLUT[5];
      iSum[0] += ipWei[ucpUnpack[0][5]];
      iSum[1] += ipWei[ucpUnpack[1][5]];
      iSum[2] += ipWei[ucpUnpack[2][5]];
      iSum[3] += ipWei[ucpUnpack[3][5]];
      iSum[4] += ipWei[ucpUnpack[4][5]];
      iSum[5] += ipWei[ucpUnpack[5][5]];
      iSum[6] += ipWei[ucpUnpack[6][5]];
      iSum[7] += ipWei[ucpUnpack[7][5]];
      iSum[8] += ipWei[ucpUnpack[8][5]];
      iSum[9] += ipWei[ucpUnpack[9][5]];
      iSum[10] += ipWei[ucpUnpack[10][5]];
      iSum[11] += ipWei[ucpUnpack[11][5]];
      iSum[12] += ipWei[ucpUnpack[12][5]];
      iSum[13] += ipWei[ucpUnpack[13][5]];
      iSum[14] += ipWei[ucpUnpack[14][5]];
      iSum[15] += ipWei[ucpUnpack[15][5]];
      iSum[16] += ipWei[ucpUnpack[16][5]];
      iSum[17] += ipWei[ucpUnpack[17][5]];
      iSum[18] += ipWei[ucpUnpack[18][5]];
      iSum[19] += ipWei[ucpUnpack[19][5]];
      iSum[20] += ipWei[ucpUnpack[20][5]];
      iSum[21] += ipWei[ucpUnpack[21][5]];
      iSum[22] += ipWei[ucpUnpack[22][5]];
      iSum[23] += ipWei[ucpUnpack[23][5]];

      ipWei = ipLUT[6];
      iSum[0] += ipWei[ucpUnpack[0][6]];
      iSum[1] += ipWei[ucpUnpack[1][6]];
      iSum[2] += ipWei[ucpUnpack[2][6]];
      iSum[3] += ipWei[ucpUnpack[3][6]];
      iSum[4] += ipWei[ucpUnpack[4][6]];
      iSum[5] += ipWei[ucpUnpack[5][6]];
      iSum[6] += ipWei[ucpUnpack[6][6]];
      iSum[7] += ipWei[ucpUnpack[7][6]];
      iSum[8] += ipWei[ucpUnpack[8][6]];
      iSum[9] += ipWei[ucpUnpack[9][6]];
      iSum[10] += ipWei[ucpUnpack[10][6]];
      iSum[11] += ipWei[ucpUnpack[11][6]];
      iSum[12] += ipWei[ucpUnpack[12][6]];
      iSum[13] += ipWei[ucpUnpack[13][6]];
      iSum[14] += ipWei[ucpUnpack[14][6]];
      iSum[15] += ipWei[ucpUnpack[15][6]];
      iSum[16] += ipWei[ucpUnpack[16][6]];
      iSum[17] += ipWei[ucpUnpack[17][6]];
      iSum[18] += ipWei[ucpUnpack[18][6]];
      iSum[19] += ipWei[ucpUnpack[19][6]];
      iSum[20] += ipWei[ucpUnpack[20][6]];
      iSum[21] += ipWei[ucpUnpack[21][6]];
      iSum[22] += ipWei[ucpUnpack[22][6]];
      iSum[23] += ipWei[ucpUnpack[23][6]];

      ipWei = ipLUT[7];
      iSum[0] += ipWei[ucpUnpack[0][7]];
      iSum[1] += ipWei[ucpUnpack[1][7]];
      iSum[2] += ipWei[ucpUnpack[2][7]];
      iSum[3] += ipWei[ucpUnpack[3][7]];
      iSum[4] += ipWei[ucpUnpack[4][7]];
      iSum[5] += ipWei[ucpUnpack[5][7]];
      iSum[6] += ipWei[ucpUnpack[6][7]];
      iSum[7] += ipWei[ucpUnpack[7][7]];
      iSum[8] += ipWei[ucpUnpack[8][7]];
      iSum[9] += ipWei[ucpUnpack[9][7]];
      iSum[10] += ipWei[ucpUnpack[10][7]];
      iSum[11] += ipWei[ucpUnpack[11][7]];
      iSum[12] += ipWei[ucpUnpack[12][7]];
      iSum[13] += ipWei[ucpUnpack[13][7]];
      iSum[14] += ipWei[ucpUnpack[14][7]];
      iSum[15] += ipWei[ucpUnpack[15][7]];
      iSum[16] += ipWei[ucpUnpack[16][7]];
      iSum[17] += ipWei[ucpUnpack[17][7]];
      iSum[18] += ipWei[ucpUnpack[18][7]];
      iSum[19] += ipWei[ucpUnpack[19][7]];
      iSum[20] += ipWei[ucpUnpack[20][7]];
      iSum[21] += ipWei[ucpUnpack[21][7]];
      iSum[22] += ipWei[ucpUnpack[22][7]];
      iSum[23] += ipWei[ucpUnpack[23][7]];

      ipWei = ipLUT[8];
      iSum[0] += ipWei[ucpUnpack[0][8]];
      iSum[1] += ipWei[ucpUnpack[1][8]];
      iSum[2] += ipWei[ucpUnpack[2][8]];
      iSum[3] += ipWei[ucpUnpack[3][8]];
      iSum[4] += ipWei[ucpUnpack[4][8]];
      iSum[5] += ipWei[ucpUnpack[5][8]];
      iSum[6] += ipWei[ucpUnpack[6][8]];
      iSum[7] += ipWei[ucpUnpack[7][8]];
      iSum[8] += ipWei[ucpUnpack[8][8]];
      iSum[9] += ipWei[ucpUnpack[9][8]];
      iSum[10] += ipWei[ucpUnpack[10][8]];
      iSum[11] += ipWei[ucpUnpack[11][8]];
      iSum[12] += ipWei[ucpUnpack[12][8]];
      iSum[13] += ipWei[ucpUnpack[13][8]];
      iSum[14] += ipWei[ucpUnpack[14][8]];
      iSum[15] += ipWei[ucpUnpack[15][8]];
      iSum[16] += ipWei[ucpUnpack[16][8]];
      iSum[17] += ipWei[ucpUnpack[17][8]];
      iSum[18] += ipWei[ucpUnpack[18][8]];
      iSum[19] += ipWei[ucpUnpack[19][8]];
      iSum[20] += ipWei[ucpUnpack[20][8]];
      iSum[21] += ipWei[ucpUnpack[21][8]];
      iSum[22] += ipWei[ucpUnpack[22][8]];
      iSum[23] += ipWei[ucpUnpack[23][8]];

      ipWei = ipLUT[9];
      iSum[0] += ipWei[ucpUnpack[0][9]];
      iSum[1] += ipWei[ucpUnpack[1][9]];
      iSum[2] += ipWei[ucpUnpack[2][9]];
      iSum[3] += ipWei[ucpUnpack[3][9]];
      iSum[4] += ipWei[ucpUnpack[4][9]];
      iSum[5] += ipWei[ucpUnpack[5][9]];
      iSum[6] += ipWei[ucpUnpack[6][9]];
      iSum[7] += ipWei[ucpUnpack[7][9]];
      iSum[8] += ipWei[ucpUnpack[8][9]];
      iSum[9] += ipWei[ucpUnpack[9][9]];
      iSum[10] += ipWei[ucpUnpack[10][9]];
      iSum[11] += ipWei[ucpUnpack[11][9]];
      iSum[12] += ipWei[ucpUnpack[12][9]];
      iSum[13] += ipWei[ucpUnpack[13][9]];
      iSum[14] += ipWei[ucpUnpack[14][9]];
      iSum[15] += ipWei[ucpUnpack[15][9]];
      iSum[16] += ipWei[ucpUnpack[16][9]];
      iSum[17] += ipWei[ucpUnpack[17][9]];
      iSum[18] += ipWei[ucpUnpack[18][9]];
      iSum[19] += ipWei[ucpUnpack[19][9]];
      iSum[20] += ipWei[ucpUnpack[20][9]];
      iSum[21] += ipWei[ucpUnpack[21][9]];
      iSum[22] += ipWei[ucpUnpack[22][9]];
      iSum[23] += ipWei[ucpUnpack[23][9]];

      ipWei = ipLUT[10];
      iSum[0] += ipWei[ucpUnpack[0][10]];
      iSum[1] += ipWei[ucpUnpack[1][10]];
      iSum[2] += ipWei[ucpUnpack[2][10]];
      iSum[3] += ipWei[ucpUnpack[3][10]];
      iSum[4] += ipWei[ucpUnpack[4][10]];
      iSum[5] += ipWei[ucpUnpack[5][10]];
      iSum[6] += ipWei[ucpUnpack[6][10]];
      iSum[7] += ipWei[ucpUnpack[7][10]];
      iSum[8] += ipWei[ucpUnpack[8][10]];
      iSum[9] += ipWei[ucpUnpack[9][10]];
      iSum[10] += ipWei[ucpUnpack[10][10]];
      iSum[11] += ipWei[ucpUnpack[11][10]];
      iSum[12] += ipWei[ucpUnpack[12][10]];
      iSum[13] += ipWei[ucpUnpack[13][10]];
      iSum[14] += ipWei[ucpUnpack[14][10]];
      iSum[15] += ipWei[ucpUnpack[15][10]];
      iSum[16] += ipWei[ucpUnpack[16][10]];
      iSum[17] += ipWei[ucpUnpack[17][10]];
      iSum[18] += ipWei[ucpUnpack[18][10]];
      iSum[19] += ipWei[ucpUnpack[19][10]];
      iSum[20] += ipWei[ucpUnpack[20][10]];
      iSum[21] += ipWei[ucpUnpack[21][10]];
      iSum[22] += ipWei[ucpUnpack[22][10]];
      iSum[23] += ipWei[ucpUnpack[23][10]];

      // clamp and dump the results

      // no rounding - sums were pre-rounded
      // I commented out the "& 0xFF" because I assume the min/max
      // code will take care of making sure the sums are not negative;
      // if that assumption is not valid, they need to be uncommented
      long lTmp1, lTmp2;

      lTmp1 = iSum[0];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[1];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[2];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[3];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent] = lTmp1;

      lTmp1 = iSum[4];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[5];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[6];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[7];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 4] = lTmp1;

      lTmp1 = iSum[8];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[9];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[10];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[11];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 8] = lTmp1;

      lTmp1 = iSum[12];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[13];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[14];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[15];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 12] = lTmp1;

      lTmp1 = iSum[16];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[17];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[18];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[19];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 16] = lTmp1;

      lTmp1 = iSum[20];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[21];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[22];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[23];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 20] = lTmp1;
     }
    ipSP += 2;
    ipP += 2;
   }
}

void CAspectRatio::RenderV_YUV_13 (MTI_UINT8 *ucpDst[2], int &iRow, int iStopDstLocal,
		int iJob)
/*
	Do VERTICAL rendering with 13 neighbors
*/
{
  MTI_UINT8 *ucpD;
  int iField, iR, *ipSP, *ipP, iPixel, iNei;
  int **ipLUT, iSubPixel;
  int iSum[VERTICAL_COMPONENTS], *ipWei;
  MTI_UINT8 *ucpUnpack[VERTICAL_COMPONENTS];
  int iComponent;

  // check for errors
  if ((iRow & 0x1) || (iStopDstLocal & 0x1))
    return;

  int iField0, iField1;

  if (iNFieldDst == 2)
   {
    iField0 = 0;
    iField1 = 1;
   }
  else
   {
    iField0 = 0;
    iField1 = 0;
   }


  ipSP = ipLUTSubPixel + iRow;
  ipP = ipLUTPixel + iRow;
  for (; iRow < iStopDstLocal; iRow+=2)
   {
    // do the "even" field
    iR = iRow / iNFieldDst;

    ucpD = ucpDst[iField0] + (iR * iPitchDst);

    iSubPixel = ipSP[0];
    iPixel = ipP[0];

    for (iComponent = 0; iComponent < iUnpackedComponent; iComponent += VERTICAL_COMPONENTS)
     {
      ipLUT = ipLUTWei[iSubPixel];
      ucpUnpack[0] = ucpUnpacked[iJob][iComponent] + iPixel;
      ucpUnpack[1] = ucpUnpacked[iJob][iComponent + 1] + iPixel;
      ucpUnpack[2] = ucpUnpacked[iJob][iComponent + 2] + iPixel;
      ucpUnpack[3] = ucpUnpacked[iJob][iComponent + 3] + iPixel;
      ucpUnpack[4] = ucpUnpacked[iJob][iComponent + 4] + iPixel;
      ucpUnpack[5] = ucpUnpacked[iJob][iComponent + 5] + iPixel;
      ucpUnpack[6] = ucpUnpacked[iJob][iComponent + 6] + iPixel;
      ucpUnpack[7] = ucpUnpacked[iJob][iComponent + 7] + iPixel;
      ucpUnpack[8] = ucpUnpacked[iJob][iComponent + 8] + iPixel;
      ucpUnpack[9] = ucpUnpacked[iJob][iComponent + 9] + iPixel;
      ucpUnpack[10] = ucpUnpacked[iJob][iComponent + 10] + iPixel;
      ucpUnpack[11] = ucpUnpacked[iJob][iComponent + 11] + iPixel;
      ucpUnpack[12] = ucpUnpacked[iJob][iComponent + 12] + iPixel;
      ucpUnpack[13] = ucpUnpacked[iJob][iComponent + 13] + iPixel;
      ucpUnpack[14] = ucpUnpacked[iJob][iComponent + 14] + iPixel;
      ucpUnpack[15] = ucpUnpacked[iJob][iComponent + 15] + iPixel;
      ucpUnpack[16] = ucpUnpacked[iJob][iComponent + 16] + iPixel;
      ucpUnpack[17] = ucpUnpacked[iJob][iComponent + 17] + iPixel;
      ucpUnpack[18] = ucpUnpacked[iJob][iComponent + 18] + iPixel;
      ucpUnpack[19] = ucpUnpacked[iJob][iComponent + 19] + iPixel;
      ucpUnpack[20] = ucpUnpacked[iJob][iComponent + 20] + iPixel;
      ucpUnpack[21] = ucpUnpacked[iJob][iComponent + 21] + iPixel;
      ucpUnpack[22] = ucpUnpacked[iJob][iComponent + 22] + iPixel;
      ucpUnpack[23] = ucpUnpacked[iJob][iComponent + 23] + iPixel;

      // pre-round the sums
      iSum[0] = iSum[1] = iSum[2] = iSum[3] =
      iSum[4] = iSum[5] = iSum[6] = iSum[7] =
      iSum[8] = iSum[9] = iSum[10] = iSum[11] =
      iSum[12] = iSum[13] = iSum[14] = iSum[15] =
      iSum[16] = iSum[17] = iSum[18] = iSum[19] =
      iSum[20] = iSum[21] = iSum[22] = iSum[23] =
                  iHalfPrecision;

      // do the 13 neighbors
      ipWei = ipLUT[0];
      iSum[0] += ipWei[ucpUnpack[0][0]];
      iSum[1] += ipWei[ucpUnpack[1][0]];
      iSum[2] += ipWei[ucpUnpack[2][0]];
      iSum[3] += ipWei[ucpUnpack[3][0]];
      iSum[4] += ipWei[ucpUnpack[4][0]];
      iSum[5] += ipWei[ucpUnpack[5][0]];
      iSum[6] += ipWei[ucpUnpack[6][0]];
      iSum[7] += ipWei[ucpUnpack[7][0]];
      iSum[8] += ipWei[ucpUnpack[8][0]];
      iSum[9] += ipWei[ucpUnpack[9][0]];
      iSum[10] += ipWei[ucpUnpack[10][0]];
      iSum[11] += ipWei[ucpUnpack[11][0]];
      iSum[12] += ipWei[ucpUnpack[12][0]];
      iSum[13] += ipWei[ucpUnpack[13][0]];
      iSum[14] += ipWei[ucpUnpack[14][0]];
      iSum[15] += ipWei[ucpUnpack[15][0]];
      iSum[16] += ipWei[ucpUnpack[16][0]];
      iSum[17] += ipWei[ucpUnpack[17][0]];
      iSum[18] += ipWei[ucpUnpack[18][0]];
      iSum[19] += ipWei[ucpUnpack[19][0]];
      iSum[20] += ipWei[ucpUnpack[20][0]];
      iSum[21] += ipWei[ucpUnpack[21][0]];
      iSum[22] += ipWei[ucpUnpack[22][0]];
      iSum[23] += ipWei[ucpUnpack[23][0]];

      ipWei = ipLUT[1];
      iSum[0] += ipWei[ucpUnpack[0][1]];
      iSum[1] += ipWei[ucpUnpack[1][1]];
      iSum[2] += ipWei[ucpUnpack[2][1]];
      iSum[3] += ipWei[ucpUnpack[3][1]];
      iSum[4] += ipWei[ucpUnpack[4][1]];
      iSum[5] += ipWei[ucpUnpack[5][1]];
      iSum[6] += ipWei[ucpUnpack[6][1]];
      iSum[7] += ipWei[ucpUnpack[7][1]];
      iSum[8] += ipWei[ucpUnpack[8][1]];
      iSum[9] += ipWei[ucpUnpack[9][1]];
      iSum[10] += ipWei[ucpUnpack[10][1]];
      iSum[11] += ipWei[ucpUnpack[11][1]];
      iSum[12] += ipWei[ucpUnpack[12][1]];
      iSum[13] += ipWei[ucpUnpack[13][1]];
      iSum[14] += ipWei[ucpUnpack[14][1]];
      iSum[15] += ipWei[ucpUnpack[15][1]];
      iSum[16] += ipWei[ucpUnpack[16][1]];
      iSum[17] += ipWei[ucpUnpack[17][1]];
      iSum[18] += ipWei[ucpUnpack[18][1]];
      iSum[19] += ipWei[ucpUnpack[19][1]];
      iSum[20] += ipWei[ucpUnpack[20][1]];
      iSum[21] += ipWei[ucpUnpack[21][1]];
      iSum[22] += ipWei[ucpUnpack[22][1]];
      iSum[23] += ipWei[ucpUnpack[23][1]];

      ipWei = ipLUT[2];
      iSum[0] += ipWei[ucpUnpack[0][2]];
      iSum[1] += ipWei[ucpUnpack[1][2]];
      iSum[2] += ipWei[ucpUnpack[2][2]];
      iSum[3] += ipWei[ucpUnpack[3][2]];
      iSum[4] += ipWei[ucpUnpack[4][2]];
      iSum[5] += ipWei[ucpUnpack[5][2]];
      iSum[6] += ipWei[ucpUnpack[6][2]];
      iSum[7] += ipWei[ucpUnpack[7][2]];
      iSum[8] += ipWei[ucpUnpack[8][2]];
      iSum[9] += ipWei[ucpUnpack[9][2]];
      iSum[10] += ipWei[ucpUnpack[10][2]];
      iSum[11] += ipWei[ucpUnpack[11][2]];
      iSum[12] += ipWei[ucpUnpack[12][2]];
      iSum[13] += ipWei[ucpUnpack[13][2]];
      iSum[14] += ipWei[ucpUnpack[14][2]];
      iSum[15] += ipWei[ucpUnpack[15][2]];
      iSum[16] += ipWei[ucpUnpack[16][2]];
      iSum[17] += ipWei[ucpUnpack[17][2]];
      iSum[18] += ipWei[ucpUnpack[18][2]];
      iSum[19] += ipWei[ucpUnpack[19][2]];
      iSum[20] += ipWei[ucpUnpack[20][2]];
      iSum[21] += ipWei[ucpUnpack[21][2]];
      iSum[22] += ipWei[ucpUnpack[22][2]];
      iSum[23] += ipWei[ucpUnpack[23][2]];

      ipWei = ipLUT[3];
      iSum[0] += ipWei[ucpUnpack[0][3]];
      iSum[1] += ipWei[ucpUnpack[1][3]];
      iSum[2] += ipWei[ucpUnpack[2][3]];
      iSum[3] += ipWei[ucpUnpack[3][3]];
      iSum[4] += ipWei[ucpUnpack[4][3]];
      iSum[5] += ipWei[ucpUnpack[5][3]];
      iSum[6] += ipWei[ucpUnpack[6][3]];
      iSum[7] += ipWei[ucpUnpack[7][3]];
      iSum[8] += ipWei[ucpUnpack[8][3]];
      iSum[9] += ipWei[ucpUnpack[9][3]];
      iSum[10] += ipWei[ucpUnpack[10][3]];
      iSum[11] += ipWei[ucpUnpack[11][3]];
      iSum[12] += ipWei[ucpUnpack[12][3]];
      iSum[13] += ipWei[ucpUnpack[13][3]];
      iSum[14] += ipWei[ucpUnpack[14][3]];
      iSum[15] += ipWei[ucpUnpack[15][3]];
      iSum[16] += ipWei[ucpUnpack[16][3]];
      iSum[17] += ipWei[ucpUnpack[17][3]];
      iSum[18] += ipWei[ucpUnpack[18][3]];
      iSum[19] += ipWei[ucpUnpack[19][3]];
      iSum[20] += ipWei[ucpUnpack[20][3]];
      iSum[21] += ipWei[ucpUnpack[21][3]];
      iSum[22] += ipWei[ucpUnpack[22][3]];
      iSum[23] += ipWei[ucpUnpack[23][3]];

      ipWei = ipLUT[4];
      iSum[0] += ipWei[ucpUnpack[0][4]];
      iSum[1] += ipWei[ucpUnpack[1][4]];
      iSum[2] += ipWei[ucpUnpack[2][4]];
      iSum[3] += ipWei[ucpUnpack[3][4]];
      iSum[4] += ipWei[ucpUnpack[4][4]];
      iSum[5] += ipWei[ucpUnpack[5][4]];
      iSum[6] += ipWei[ucpUnpack[6][4]];
      iSum[7] += ipWei[ucpUnpack[7][4]];
      iSum[8] += ipWei[ucpUnpack[8][4]];
      iSum[9] += ipWei[ucpUnpack[9][4]];
      iSum[10] += ipWei[ucpUnpack[10][4]];
      iSum[11] += ipWei[ucpUnpack[11][4]];
      iSum[12] += ipWei[ucpUnpack[12][4]];
      iSum[13] += ipWei[ucpUnpack[13][4]];
      iSum[14] += ipWei[ucpUnpack[14][4]];
      iSum[15] += ipWei[ucpUnpack[15][4]];
      iSum[16] += ipWei[ucpUnpack[16][4]];
      iSum[17] += ipWei[ucpUnpack[17][4]];
      iSum[18] += ipWei[ucpUnpack[18][4]];
      iSum[19] += ipWei[ucpUnpack[19][4]];
      iSum[20] += ipWei[ucpUnpack[20][4]];
      iSum[21] += ipWei[ucpUnpack[21][4]];
      iSum[22] += ipWei[ucpUnpack[22][4]];
      iSum[23] += ipWei[ucpUnpack[23][4]];

      ipWei = ipLUT[5];
      iSum[0] += ipWei[ucpUnpack[0][5]];
      iSum[1] += ipWei[ucpUnpack[1][5]];
      iSum[2] += ipWei[ucpUnpack[2][5]];
      iSum[3] += ipWei[ucpUnpack[3][5]];
      iSum[4] += ipWei[ucpUnpack[4][5]];
      iSum[5] += ipWei[ucpUnpack[5][5]];
      iSum[6] += ipWei[ucpUnpack[6][5]];
      iSum[7] += ipWei[ucpUnpack[7][5]];
      iSum[8] += ipWei[ucpUnpack[8][5]];
      iSum[9] += ipWei[ucpUnpack[9][5]];
      iSum[10] += ipWei[ucpUnpack[10][5]];
      iSum[11] += ipWei[ucpUnpack[11][5]];
      iSum[12] += ipWei[ucpUnpack[12][5]];
      iSum[13] += ipWei[ucpUnpack[13][5]];
      iSum[14] += ipWei[ucpUnpack[14][5]];
      iSum[15] += ipWei[ucpUnpack[15][5]];
      iSum[16] += ipWei[ucpUnpack[16][5]];
      iSum[17] += ipWei[ucpUnpack[17][5]];
      iSum[18] += ipWei[ucpUnpack[18][5]];
      iSum[19] += ipWei[ucpUnpack[19][5]];
      iSum[20] += ipWei[ucpUnpack[20][5]];
      iSum[21] += ipWei[ucpUnpack[21][5]];
      iSum[22] += ipWei[ucpUnpack[22][5]];
      iSum[23] += ipWei[ucpUnpack[23][5]];

      ipWei = ipLUT[6];
      iSum[0] += ipWei[ucpUnpack[0][6]];
      iSum[1] += ipWei[ucpUnpack[1][6]];
      iSum[2] += ipWei[ucpUnpack[2][6]];
      iSum[3] += ipWei[ucpUnpack[3][6]];
      iSum[4] += ipWei[ucpUnpack[4][6]];
      iSum[5] += ipWei[ucpUnpack[5][6]];
      iSum[6] += ipWei[ucpUnpack[6][6]];
      iSum[7] += ipWei[ucpUnpack[7][6]];
      iSum[8] += ipWei[ucpUnpack[8][6]];
      iSum[9] += ipWei[ucpUnpack[9][6]];
      iSum[10] += ipWei[ucpUnpack[10][6]];
      iSum[11] += ipWei[ucpUnpack[11][6]];
      iSum[12] += ipWei[ucpUnpack[12][6]];
      iSum[13] += ipWei[ucpUnpack[13][6]];
      iSum[14] += ipWei[ucpUnpack[14][6]];
      iSum[15] += ipWei[ucpUnpack[15][6]];
      iSum[16] += ipWei[ucpUnpack[16][6]];
      iSum[17] += ipWei[ucpUnpack[17][6]];
      iSum[18] += ipWei[ucpUnpack[18][6]];
      iSum[19] += ipWei[ucpUnpack[19][6]];
      iSum[20] += ipWei[ucpUnpack[20][6]];
      iSum[21] += ipWei[ucpUnpack[21][6]];
      iSum[22] += ipWei[ucpUnpack[22][6]];
      iSum[23] += ipWei[ucpUnpack[23][6]];

      ipWei = ipLUT[7];
      iSum[0] += ipWei[ucpUnpack[0][7]];
      iSum[1] += ipWei[ucpUnpack[1][7]];
      iSum[2] += ipWei[ucpUnpack[2][7]];
      iSum[3] += ipWei[ucpUnpack[3][7]];
      iSum[4] += ipWei[ucpUnpack[4][7]];
      iSum[5] += ipWei[ucpUnpack[5][7]];
      iSum[6] += ipWei[ucpUnpack[6][7]];
      iSum[7] += ipWei[ucpUnpack[7][7]];
      iSum[8] += ipWei[ucpUnpack[8][7]];
      iSum[9] += ipWei[ucpUnpack[9][7]];
      iSum[10] += ipWei[ucpUnpack[10][7]];
      iSum[11] += ipWei[ucpUnpack[11][7]];
      iSum[12] += ipWei[ucpUnpack[12][7]];
      iSum[13] += ipWei[ucpUnpack[13][7]];
      iSum[14] += ipWei[ucpUnpack[14][7]];
      iSum[15] += ipWei[ucpUnpack[15][7]];
      iSum[16] += ipWei[ucpUnpack[16][7]];
      iSum[17] += ipWei[ucpUnpack[17][7]];
      iSum[18] += ipWei[ucpUnpack[18][7]];
      iSum[19] += ipWei[ucpUnpack[19][7]];
      iSum[20] += ipWei[ucpUnpack[20][7]];
      iSum[21] += ipWei[ucpUnpack[21][7]];
      iSum[22] += ipWei[ucpUnpack[22][7]];
      iSum[23] += ipWei[ucpUnpack[23][7]];

      ipWei = ipLUT[8];
      iSum[0] += ipWei[ucpUnpack[0][8]];
      iSum[1] += ipWei[ucpUnpack[1][8]];
      iSum[2] += ipWei[ucpUnpack[2][8]];
      iSum[3] += ipWei[ucpUnpack[3][8]];
      iSum[4] += ipWei[ucpUnpack[4][8]];
      iSum[5] += ipWei[ucpUnpack[5][8]];
      iSum[6] += ipWei[ucpUnpack[6][8]];
      iSum[7] += ipWei[ucpUnpack[7][8]];
      iSum[8] += ipWei[ucpUnpack[8][8]];
      iSum[9] += ipWei[ucpUnpack[9][8]];
      iSum[10] += ipWei[ucpUnpack[10][8]];
      iSum[11] += ipWei[ucpUnpack[11][8]];
      iSum[12] += ipWei[ucpUnpack[12][8]];
      iSum[13] += ipWei[ucpUnpack[13][8]];
      iSum[14] += ipWei[ucpUnpack[14][8]];
      iSum[15] += ipWei[ucpUnpack[15][8]];
      iSum[16] += ipWei[ucpUnpack[16][8]];
      iSum[17] += ipWei[ucpUnpack[17][8]];
      iSum[18] += ipWei[ucpUnpack[18][8]];
      iSum[19] += ipWei[ucpUnpack[19][8]];
      iSum[20] += ipWei[ucpUnpack[20][8]];
      iSum[21] += ipWei[ucpUnpack[21][8]];
      iSum[22] += ipWei[ucpUnpack[22][8]];
      iSum[23] += ipWei[ucpUnpack[23][8]];

      ipWei = ipLUT[9];
      iSum[0] += ipWei[ucpUnpack[0][9]];
      iSum[1] += ipWei[ucpUnpack[1][9]];
      iSum[2] += ipWei[ucpUnpack[2][9]];
      iSum[3] += ipWei[ucpUnpack[3][9]];
      iSum[4] += ipWei[ucpUnpack[4][9]];
      iSum[5] += ipWei[ucpUnpack[5][9]];
      iSum[6] += ipWei[ucpUnpack[6][9]];
      iSum[7] += ipWei[ucpUnpack[7][9]];
      iSum[8] += ipWei[ucpUnpack[8][9]];
      iSum[9] += ipWei[ucpUnpack[9][9]];
      iSum[10] += ipWei[ucpUnpack[10][9]];
      iSum[11] += ipWei[ucpUnpack[11][9]];
      iSum[12] += ipWei[ucpUnpack[12][9]];
      iSum[13] += ipWei[ucpUnpack[13][9]];
      iSum[14] += ipWei[ucpUnpack[14][9]];
      iSum[15] += ipWei[ucpUnpack[15][9]];
      iSum[16] += ipWei[ucpUnpack[16][9]];
      iSum[17] += ipWei[ucpUnpack[17][9]];
      iSum[18] += ipWei[ucpUnpack[18][9]];
      iSum[19] += ipWei[ucpUnpack[19][9]];
      iSum[20] += ipWei[ucpUnpack[20][9]];
      iSum[21] += ipWei[ucpUnpack[21][9]];
      iSum[22] += ipWei[ucpUnpack[22][9]];
      iSum[23] += ipWei[ucpUnpack[23][9]];

      ipWei = ipLUT[10];
      iSum[0] += ipWei[ucpUnpack[0][10]];
      iSum[1] += ipWei[ucpUnpack[1][10]];
      iSum[2] += ipWei[ucpUnpack[2][10]];
      iSum[3] += ipWei[ucpUnpack[3][10]];
      iSum[4] += ipWei[ucpUnpack[4][10]];
      iSum[5] += ipWei[ucpUnpack[5][10]];
      iSum[6] += ipWei[ucpUnpack[6][10]];
      iSum[7] += ipWei[ucpUnpack[7][10]];
      iSum[8] += ipWei[ucpUnpack[8][10]];
      iSum[9] += ipWei[ucpUnpack[9][10]];
      iSum[10] += ipWei[ucpUnpack[10][10]];
      iSum[11] += ipWei[ucpUnpack[11][10]];
      iSum[12] += ipWei[ucpUnpack[12][10]];
      iSum[13] += ipWei[ucpUnpack[13][10]];
      iSum[14] += ipWei[ucpUnpack[14][10]];
      iSum[15] += ipWei[ucpUnpack[15][10]];
      iSum[16] += ipWei[ucpUnpack[16][10]];
      iSum[17] += ipWei[ucpUnpack[17][10]];
      iSum[18] += ipWei[ucpUnpack[18][10]];
      iSum[19] += ipWei[ucpUnpack[19][10]];
      iSum[20] += ipWei[ucpUnpack[20][10]];
      iSum[21] += ipWei[ucpUnpack[21][10]];
      iSum[22] += ipWei[ucpUnpack[22][10]];
      iSum[23] += ipWei[ucpUnpack[23][10]];

      ipWei = ipLUT[11];
      iSum[0] += ipWei[ucpUnpack[0][11]];
      iSum[1] += ipWei[ucpUnpack[1][11]];
      iSum[2] += ipWei[ucpUnpack[2][11]];
      iSum[3] += ipWei[ucpUnpack[3][11]];
      iSum[4] += ipWei[ucpUnpack[4][11]];
      iSum[5] += ipWei[ucpUnpack[5][11]];
      iSum[6] += ipWei[ucpUnpack[6][11]];
      iSum[7] += ipWei[ucpUnpack[7][11]];
      iSum[8] += ipWei[ucpUnpack[8][11]];
      iSum[9] += ipWei[ucpUnpack[9][11]];
      iSum[10] += ipWei[ucpUnpack[10][11]];
      iSum[11] += ipWei[ucpUnpack[11][11]];
      iSum[12] += ipWei[ucpUnpack[12][11]];
      iSum[13] += ipWei[ucpUnpack[13][11]];
      iSum[14] += ipWei[ucpUnpack[14][11]];
      iSum[15] += ipWei[ucpUnpack[15][11]];
      iSum[16] += ipWei[ucpUnpack[16][11]];
      iSum[17] += ipWei[ucpUnpack[17][11]];
      iSum[18] += ipWei[ucpUnpack[18][11]];
      iSum[19] += ipWei[ucpUnpack[19][11]];
      iSum[20] += ipWei[ucpUnpack[20][11]];
      iSum[21] += ipWei[ucpUnpack[21][11]];
      iSum[22] += ipWei[ucpUnpack[22][11]];
      iSum[23] += ipWei[ucpUnpack[23][11]];

      ipWei = ipLUT[12];
      iSum[0] += ipWei[ucpUnpack[0][12]];
      iSum[1] += ipWei[ucpUnpack[1][12]];
      iSum[2] += ipWei[ucpUnpack[2][12]];
      iSum[3] += ipWei[ucpUnpack[3][12]];
      iSum[4] += ipWei[ucpUnpack[4][12]];
      iSum[5] += ipWei[ucpUnpack[5][12]];
      iSum[6] += ipWei[ucpUnpack[6][12]];
      iSum[7] += ipWei[ucpUnpack[7][12]];
      iSum[8] += ipWei[ucpUnpack[8][12]];
      iSum[9] += ipWei[ucpUnpack[9][12]];
      iSum[10] += ipWei[ucpUnpack[10][12]];
      iSum[11] += ipWei[ucpUnpack[11][12]];
      iSum[12] += ipWei[ucpUnpack[12][12]];
      iSum[13] += ipWei[ucpUnpack[13][12]];
      iSum[14] += ipWei[ucpUnpack[14][12]];
      iSum[15] += ipWei[ucpUnpack[15][12]];
      iSum[16] += ipWei[ucpUnpack[16][12]];
      iSum[17] += ipWei[ucpUnpack[17][12]];
      iSum[18] += ipWei[ucpUnpack[18][12]];
      iSum[19] += ipWei[ucpUnpack[19][12]];
      iSum[20] += ipWei[ucpUnpack[20][12]];
      iSum[21] += ipWei[ucpUnpack[21][12]];
      iSum[22] += ipWei[ucpUnpack[22][12]];
      iSum[23] += ipWei[ucpUnpack[23][12]];

      // clamp and dump the results

      // no rounding - sums were pre-rounded
      // I commented out the "& 0xFF" because I assume the min/max
      // code will take care of making sure the sums are not negative;
      // if that assumption is not valid, they need to be uncommented
      long lTmp1, lTmp2;

      lTmp1 = iSum[0];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[1];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[2];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[3];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent] = lTmp1;

      lTmp1 = iSum[4];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[5];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[6];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[7];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 4] = lTmp1;

      lTmp1 = iSum[8];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[9];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[10];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[11];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 8] = lTmp1;

      lTmp1 = iSum[12];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[13];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[14];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[15];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 12] = lTmp1;

      lTmp1 = iSum[16];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[17];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[18];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[19];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 16] = lTmp1;

      lTmp1 = iSum[20];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[21];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[22];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[23];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 20] = lTmp1;
    }

    // do the "odd" field
    iR = (iRow+1) / iNFieldDst;

    ucpD = ucpDst[iField1] + (iR * iPitchDst);

    iSubPixel = ipSP[1];
    iPixel = ipP[1];

    for (iComponent = 0; iComponent < iUnpackedComponent; iComponent += VERTICAL_COMPONENTS)
     {
      ipLUT = ipLUTWei[iSubPixel];
      ucpUnpack[0] = ucpUnpacked[iJob][iComponent] + iPixel;
      ucpUnpack[1] = ucpUnpacked[iJob][iComponent + 1] + iPixel;
      ucpUnpack[2] = ucpUnpacked[iJob][iComponent + 2] + iPixel;
      ucpUnpack[3] = ucpUnpacked[iJob][iComponent + 3] + iPixel;
      ucpUnpack[4] = ucpUnpacked[iJob][iComponent + 4] + iPixel;
      ucpUnpack[5] = ucpUnpacked[iJob][iComponent + 5] + iPixel;
      ucpUnpack[6] = ucpUnpacked[iJob][iComponent + 6] + iPixel;
      ucpUnpack[7] = ucpUnpacked[iJob][iComponent + 7] + iPixel;
      ucpUnpack[8] = ucpUnpacked[iJob][iComponent + 8] + iPixel;
      ucpUnpack[9] = ucpUnpacked[iJob][iComponent + 9] + iPixel;
      ucpUnpack[10] = ucpUnpacked[iJob][iComponent + 10] + iPixel;
      ucpUnpack[11] = ucpUnpacked[iJob][iComponent + 11] + iPixel;
      ucpUnpack[12] = ucpUnpacked[iJob][iComponent + 12] + iPixel;
      ucpUnpack[13] = ucpUnpacked[iJob][iComponent + 13] + iPixel;
      ucpUnpack[14] = ucpUnpacked[iJob][iComponent + 14] + iPixel;
      ucpUnpack[15] = ucpUnpacked[iJob][iComponent + 15] + iPixel;
      ucpUnpack[16] = ucpUnpacked[iJob][iComponent + 16] + iPixel;
      ucpUnpack[17] = ucpUnpacked[iJob][iComponent + 17] + iPixel;
      ucpUnpack[18] = ucpUnpacked[iJob][iComponent + 18] + iPixel;
      ucpUnpack[19] = ucpUnpacked[iJob][iComponent + 19] + iPixel;
      ucpUnpack[20] = ucpUnpacked[iJob][iComponent + 20] + iPixel;
      ucpUnpack[21] = ucpUnpacked[iJob][iComponent + 21] + iPixel;
      ucpUnpack[22] = ucpUnpacked[iJob][iComponent + 22] + iPixel;
      ucpUnpack[23] = ucpUnpacked[iJob][iComponent + 23] + iPixel;

      // pre-round the sums
      iSum[0] = iSum[1] = iSum[2] = iSum[3] =
      iSum[4] = iSum[5] = iSum[6] = iSum[7] =
      iSum[8] = iSum[9] = iSum[10] = iSum[11] =
      iSum[12] = iSum[13] = iSum[14] = iSum[15] =
      iSum[16] = iSum[17] = iSum[18] = iSum[19] =
      iSum[20] = iSum[21] = iSum[22] = iSum[23] =
                  iHalfPrecision;

      // do the 13 neighbors
      ipWei = ipLUT[0];
      iSum[0] += ipWei[ucpUnpack[0][0]];
      iSum[1] += ipWei[ucpUnpack[1][0]];
      iSum[2] += ipWei[ucpUnpack[2][0]];
      iSum[3] += ipWei[ucpUnpack[3][0]];
      iSum[4] += ipWei[ucpUnpack[4][0]];
      iSum[5] += ipWei[ucpUnpack[5][0]];
      iSum[6] += ipWei[ucpUnpack[6][0]];
      iSum[7] += ipWei[ucpUnpack[7][0]];
      iSum[8] += ipWei[ucpUnpack[8][0]];
      iSum[9] += ipWei[ucpUnpack[9][0]];
      iSum[10] += ipWei[ucpUnpack[10][0]];
      iSum[11] += ipWei[ucpUnpack[11][0]];
      iSum[12] += ipWei[ucpUnpack[12][0]];
      iSum[13] += ipWei[ucpUnpack[13][0]];
      iSum[14] += ipWei[ucpUnpack[14][0]];
      iSum[15] += ipWei[ucpUnpack[15][0]];
      iSum[16] += ipWei[ucpUnpack[16][0]];
      iSum[17] += ipWei[ucpUnpack[17][0]];
      iSum[18] += ipWei[ucpUnpack[18][0]];
      iSum[19] += ipWei[ucpUnpack[19][0]];
      iSum[20] += ipWei[ucpUnpack[20][0]];
      iSum[21] += ipWei[ucpUnpack[21][0]];
      iSum[22] += ipWei[ucpUnpack[22][0]];
      iSum[23] += ipWei[ucpUnpack[23][0]];

      ipWei = ipLUT[1];
      iSum[0] += ipWei[ucpUnpack[0][1]];
      iSum[1] += ipWei[ucpUnpack[1][1]];
      iSum[2] += ipWei[ucpUnpack[2][1]];
      iSum[3] += ipWei[ucpUnpack[3][1]];
      iSum[4] += ipWei[ucpUnpack[4][1]];
      iSum[5] += ipWei[ucpUnpack[5][1]];
      iSum[6] += ipWei[ucpUnpack[6][1]];
      iSum[7] += ipWei[ucpUnpack[7][1]];
      iSum[8] += ipWei[ucpUnpack[8][1]];
      iSum[9] += ipWei[ucpUnpack[9][1]];
      iSum[10] += ipWei[ucpUnpack[10][1]];
      iSum[11] += ipWei[ucpUnpack[11][1]];
      iSum[12] += ipWei[ucpUnpack[12][1]];
      iSum[13] += ipWei[ucpUnpack[13][1]];
      iSum[14] += ipWei[ucpUnpack[14][1]];
      iSum[15] += ipWei[ucpUnpack[15][1]];
      iSum[16] += ipWei[ucpUnpack[16][1]];
      iSum[17] += ipWei[ucpUnpack[17][1]];
      iSum[18] += ipWei[ucpUnpack[18][1]];
      iSum[19] += ipWei[ucpUnpack[19][1]];
      iSum[20] += ipWei[ucpUnpack[20][1]];
      iSum[21] += ipWei[ucpUnpack[21][1]];
      iSum[22] += ipWei[ucpUnpack[22][1]];
      iSum[23] += ipWei[ucpUnpack[23][1]];

      ipWei = ipLUT[2];
      iSum[0] += ipWei[ucpUnpack[0][2]];
      iSum[1] += ipWei[ucpUnpack[1][2]];
      iSum[2] += ipWei[ucpUnpack[2][2]];
      iSum[3] += ipWei[ucpUnpack[3][2]];
      iSum[4] += ipWei[ucpUnpack[4][2]];
      iSum[5] += ipWei[ucpUnpack[5][2]];
      iSum[6] += ipWei[ucpUnpack[6][2]];
      iSum[7] += ipWei[ucpUnpack[7][2]];
      iSum[8] += ipWei[ucpUnpack[8][2]];
      iSum[9] += ipWei[ucpUnpack[9][2]];
      iSum[10] += ipWei[ucpUnpack[10][2]];
      iSum[11] += ipWei[ucpUnpack[11][2]];
      iSum[12] += ipWei[ucpUnpack[12][2]];
      iSum[13] += ipWei[ucpUnpack[13][2]];
      iSum[14] += ipWei[ucpUnpack[14][2]];
      iSum[15] += ipWei[ucpUnpack[15][2]];
      iSum[16] += ipWei[ucpUnpack[16][2]];
      iSum[17] += ipWei[ucpUnpack[17][2]];
      iSum[18] += ipWei[ucpUnpack[18][2]];
      iSum[19] += ipWei[ucpUnpack[19][2]];
      iSum[20] += ipWei[ucpUnpack[20][2]];
      iSum[21] += ipWei[ucpUnpack[21][2]];
      iSum[22] += ipWei[ucpUnpack[22][2]];
      iSum[23] += ipWei[ucpUnpack[23][2]];

      ipWei = ipLUT[3];
      iSum[0] += ipWei[ucpUnpack[0][3]];
      iSum[1] += ipWei[ucpUnpack[1][3]];
      iSum[2] += ipWei[ucpUnpack[2][3]];
      iSum[3] += ipWei[ucpUnpack[3][3]];
      iSum[4] += ipWei[ucpUnpack[4][3]];
      iSum[5] += ipWei[ucpUnpack[5][3]];
      iSum[6] += ipWei[ucpUnpack[6][3]];
      iSum[7] += ipWei[ucpUnpack[7][3]];
      iSum[8] += ipWei[ucpUnpack[8][3]];
      iSum[9] += ipWei[ucpUnpack[9][3]];
      iSum[10] += ipWei[ucpUnpack[10][3]];
      iSum[11] += ipWei[ucpUnpack[11][3]];
      iSum[12] += ipWei[ucpUnpack[12][3]];
      iSum[13] += ipWei[ucpUnpack[13][3]];
      iSum[14] += ipWei[ucpUnpack[14][3]];
      iSum[15] += ipWei[ucpUnpack[15][3]];
      iSum[16] += ipWei[ucpUnpack[16][3]];
      iSum[17] += ipWei[ucpUnpack[17][3]];
      iSum[18] += ipWei[ucpUnpack[18][3]];
      iSum[19] += ipWei[ucpUnpack[19][3]];
      iSum[20] += ipWei[ucpUnpack[20][3]];
      iSum[21] += ipWei[ucpUnpack[21][3]];
      iSum[22] += ipWei[ucpUnpack[22][3]];
      iSum[23] += ipWei[ucpUnpack[23][3]];

      ipWei = ipLUT[4];
      iSum[0] += ipWei[ucpUnpack[0][4]];
      iSum[1] += ipWei[ucpUnpack[1][4]];
      iSum[2] += ipWei[ucpUnpack[2][4]];
      iSum[3] += ipWei[ucpUnpack[3][4]];
      iSum[4] += ipWei[ucpUnpack[4][4]];
      iSum[5] += ipWei[ucpUnpack[5][4]];
      iSum[6] += ipWei[ucpUnpack[6][4]];
      iSum[7] += ipWei[ucpUnpack[7][4]];
      iSum[8] += ipWei[ucpUnpack[8][4]];
      iSum[9] += ipWei[ucpUnpack[9][4]];
      iSum[10] += ipWei[ucpUnpack[10][4]];
      iSum[11] += ipWei[ucpUnpack[11][4]];
      iSum[12] += ipWei[ucpUnpack[12][4]];
      iSum[13] += ipWei[ucpUnpack[13][4]];
      iSum[14] += ipWei[ucpUnpack[14][4]];
      iSum[15] += ipWei[ucpUnpack[15][4]];
      iSum[16] += ipWei[ucpUnpack[16][4]];
      iSum[17] += ipWei[ucpUnpack[17][4]];
      iSum[18] += ipWei[ucpUnpack[18][4]];
      iSum[19] += ipWei[ucpUnpack[19][4]];
      iSum[20] += ipWei[ucpUnpack[20][4]];
      iSum[21] += ipWei[ucpUnpack[21][4]];
      iSum[22] += ipWei[ucpUnpack[22][4]];
      iSum[23] += ipWei[ucpUnpack[23][4]];

      ipWei = ipLUT[5];
      iSum[0] += ipWei[ucpUnpack[0][5]];
      iSum[1] += ipWei[ucpUnpack[1][5]];
      iSum[2] += ipWei[ucpUnpack[2][5]];
      iSum[3] += ipWei[ucpUnpack[3][5]];
      iSum[4] += ipWei[ucpUnpack[4][5]];
      iSum[5] += ipWei[ucpUnpack[5][5]];
      iSum[6] += ipWei[ucpUnpack[6][5]];
      iSum[7] += ipWei[ucpUnpack[7][5]];
      iSum[8] += ipWei[ucpUnpack[8][5]];
      iSum[9] += ipWei[ucpUnpack[9][5]];
      iSum[10] += ipWei[ucpUnpack[10][5]];
      iSum[11] += ipWei[ucpUnpack[11][5]];
      iSum[12] += ipWei[ucpUnpack[12][5]];
      iSum[13] += ipWei[ucpUnpack[13][5]];
      iSum[14] += ipWei[ucpUnpack[14][5]];
      iSum[15] += ipWei[ucpUnpack[15][5]];
      iSum[16] += ipWei[ucpUnpack[16][5]];
      iSum[17] += ipWei[ucpUnpack[17][5]];
      iSum[18] += ipWei[ucpUnpack[18][5]];
      iSum[19] += ipWei[ucpUnpack[19][5]];
      iSum[20] += ipWei[ucpUnpack[20][5]];
      iSum[21] += ipWei[ucpUnpack[21][5]];
      iSum[22] += ipWei[ucpUnpack[22][5]];
      iSum[23] += ipWei[ucpUnpack[23][5]];

      ipWei = ipLUT[6];
      iSum[0] += ipWei[ucpUnpack[0][6]];
      iSum[1] += ipWei[ucpUnpack[1][6]];
      iSum[2] += ipWei[ucpUnpack[2][6]];
      iSum[3] += ipWei[ucpUnpack[3][6]];
      iSum[4] += ipWei[ucpUnpack[4][6]];
      iSum[5] += ipWei[ucpUnpack[5][6]];
      iSum[6] += ipWei[ucpUnpack[6][6]];
      iSum[7] += ipWei[ucpUnpack[7][6]];
      iSum[8] += ipWei[ucpUnpack[8][6]];
      iSum[9] += ipWei[ucpUnpack[9][6]];
      iSum[10] += ipWei[ucpUnpack[10][6]];
      iSum[11] += ipWei[ucpUnpack[11][6]];
      iSum[12] += ipWei[ucpUnpack[12][6]];
      iSum[13] += ipWei[ucpUnpack[13][6]];
      iSum[14] += ipWei[ucpUnpack[14][6]];
      iSum[15] += ipWei[ucpUnpack[15][6]];
      iSum[16] += ipWei[ucpUnpack[16][6]];
      iSum[17] += ipWei[ucpUnpack[17][6]];
      iSum[18] += ipWei[ucpUnpack[18][6]];
      iSum[19] += ipWei[ucpUnpack[19][6]];
      iSum[20] += ipWei[ucpUnpack[20][6]];
      iSum[21] += ipWei[ucpUnpack[21][6]];
      iSum[22] += ipWei[ucpUnpack[22][6]];
      iSum[23] += ipWei[ucpUnpack[23][6]];

      ipWei = ipLUT[7];
      iSum[0] += ipWei[ucpUnpack[0][7]];
      iSum[1] += ipWei[ucpUnpack[1][7]];
      iSum[2] += ipWei[ucpUnpack[2][7]];
      iSum[3] += ipWei[ucpUnpack[3][7]];
      iSum[4] += ipWei[ucpUnpack[4][7]];
      iSum[5] += ipWei[ucpUnpack[5][7]];
      iSum[6] += ipWei[ucpUnpack[6][7]];
      iSum[7] += ipWei[ucpUnpack[7][7]];
      iSum[8] += ipWei[ucpUnpack[8][7]];
      iSum[9] += ipWei[ucpUnpack[9][7]];
      iSum[10] += ipWei[ucpUnpack[10][7]];
      iSum[11] += ipWei[ucpUnpack[11][7]];
      iSum[12] += ipWei[ucpUnpack[12][7]];
      iSum[13] += ipWei[ucpUnpack[13][7]];
      iSum[14] += ipWei[ucpUnpack[14][7]];
      iSum[15] += ipWei[ucpUnpack[15][7]];
      iSum[16] += ipWei[ucpUnpack[16][7]];
      iSum[17] += ipWei[ucpUnpack[17][7]];
      iSum[18] += ipWei[ucpUnpack[18][7]];
      iSum[19] += ipWei[ucpUnpack[19][7]];
      iSum[20] += ipWei[ucpUnpack[20][7]];
      iSum[21] += ipWei[ucpUnpack[21][7]];
      iSum[22] += ipWei[ucpUnpack[22][7]];
      iSum[23] += ipWei[ucpUnpack[23][7]];

      ipWei = ipLUT[8];
      iSum[0] += ipWei[ucpUnpack[0][8]];
      iSum[1] += ipWei[ucpUnpack[1][8]];
      iSum[2] += ipWei[ucpUnpack[2][8]];
      iSum[3] += ipWei[ucpUnpack[3][8]];
      iSum[4] += ipWei[ucpUnpack[4][8]];
      iSum[5] += ipWei[ucpUnpack[5][8]];
      iSum[6] += ipWei[ucpUnpack[6][8]];
      iSum[7] += ipWei[ucpUnpack[7][8]];
      iSum[8] += ipWei[ucpUnpack[8][8]];
      iSum[9] += ipWei[ucpUnpack[9][8]];
      iSum[10] += ipWei[ucpUnpack[10][8]];
      iSum[11] += ipWei[ucpUnpack[11][8]];
      iSum[12] += ipWei[ucpUnpack[12][8]];
      iSum[13] += ipWei[ucpUnpack[13][8]];
      iSum[14] += ipWei[ucpUnpack[14][8]];
      iSum[15] += ipWei[ucpUnpack[15][8]];
      iSum[16] += ipWei[ucpUnpack[16][8]];
      iSum[17] += ipWei[ucpUnpack[17][8]];
      iSum[18] += ipWei[ucpUnpack[18][8]];
      iSum[19] += ipWei[ucpUnpack[19][8]];
      iSum[20] += ipWei[ucpUnpack[20][8]];
      iSum[21] += ipWei[ucpUnpack[21][8]];
      iSum[22] += ipWei[ucpUnpack[22][8]];
      iSum[23] += ipWei[ucpUnpack[23][8]];

      ipWei = ipLUT[9];
      iSum[0] += ipWei[ucpUnpack[0][9]];
      iSum[1] += ipWei[ucpUnpack[1][9]];
      iSum[2] += ipWei[ucpUnpack[2][9]];
      iSum[3] += ipWei[ucpUnpack[3][9]];
      iSum[4] += ipWei[ucpUnpack[4][9]];
      iSum[5] += ipWei[ucpUnpack[5][9]];
      iSum[6] += ipWei[ucpUnpack[6][9]];
      iSum[7] += ipWei[ucpUnpack[7][9]];
      iSum[8] += ipWei[ucpUnpack[8][9]];
      iSum[9] += ipWei[ucpUnpack[9][9]];
      iSum[10] += ipWei[ucpUnpack[10][9]];
      iSum[11] += ipWei[ucpUnpack[11][9]];
      iSum[12] += ipWei[ucpUnpack[12][9]];
      iSum[13] += ipWei[ucpUnpack[13][9]];
      iSum[14] += ipWei[ucpUnpack[14][9]];
      iSum[15] += ipWei[ucpUnpack[15][9]];
      iSum[16] += ipWei[ucpUnpack[16][9]];
      iSum[17] += ipWei[ucpUnpack[17][9]];
      iSum[18] += ipWei[ucpUnpack[18][9]];
      iSum[19] += ipWei[ucpUnpack[19][9]];
      iSum[20] += ipWei[ucpUnpack[20][9]];
      iSum[21] += ipWei[ucpUnpack[21][9]];
      iSum[22] += ipWei[ucpUnpack[22][9]];
      iSum[23] += ipWei[ucpUnpack[23][9]];

      ipWei = ipLUT[10];
      iSum[0] += ipWei[ucpUnpack[0][10]];
      iSum[1] += ipWei[ucpUnpack[1][10]];
      iSum[2] += ipWei[ucpUnpack[2][10]];
      iSum[3] += ipWei[ucpUnpack[3][10]];
      iSum[4] += ipWei[ucpUnpack[4][10]];
      iSum[5] += ipWei[ucpUnpack[5][10]];
      iSum[6] += ipWei[ucpUnpack[6][10]];
      iSum[7] += ipWei[ucpUnpack[7][10]];
      iSum[8] += ipWei[ucpUnpack[8][10]];
      iSum[9] += ipWei[ucpUnpack[9][10]];
      iSum[10] += ipWei[ucpUnpack[10][10]];
      iSum[11] += ipWei[ucpUnpack[11][10]];
      iSum[12] += ipWei[ucpUnpack[12][10]];
      iSum[13] += ipWei[ucpUnpack[13][10]];
      iSum[14] += ipWei[ucpUnpack[14][10]];
      iSum[15] += ipWei[ucpUnpack[15][10]];
      iSum[16] += ipWei[ucpUnpack[16][10]];
      iSum[17] += ipWei[ucpUnpack[17][10]];
      iSum[18] += ipWei[ucpUnpack[18][10]];
      iSum[19] += ipWei[ucpUnpack[19][10]];
      iSum[20] += ipWei[ucpUnpack[20][10]];
      iSum[21] += ipWei[ucpUnpack[21][10]];
      iSum[22] += ipWei[ucpUnpack[22][10]];
      iSum[23] += ipWei[ucpUnpack[23][10]];

      ipWei = ipLUT[11];
      iSum[0] += ipWei[ucpUnpack[0][11]];
      iSum[1] += ipWei[ucpUnpack[1][11]];
      iSum[2] += ipWei[ucpUnpack[2][11]];
      iSum[3] += ipWei[ucpUnpack[3][11]];
      iSum[4] += ipWei[ucpUnpack[4][11]];
      iSum[5] += ipWei[ucpUnpack[5][11]];
      iSum[6] += ipWei[ucpUnpack[6][11]];
      iSum[7] += ipWei[ucpUnpack[7][11]];
      iSum[8] += ipWei[ucpUnpack[8][11]];
      iSum[9] += ipWei[ucpUnpack[9][11]];
      iSum[10] += ipWei[ucpUnpack[10][11]];
      iSum[11] += ipWei[ucpUnpack[11][11]];
      iSum[12] += ipWei[ucpUnpack[12][11]];
      iSum[13] += ipWei[ucpUnpack[13][11]];
      iSum[14] += ipWei[ucpUnpack[14][11]];
      iSum[15] += ipWei[ucpUnpack[15][11]];
      iSum[16] += ipWei[ucpUnpack[16][11]];
      iSum[17] += ipWei[ucpUnpack[17][11]];
      iSum[18] += ipWei[ucpUnpack[18][11]];
      iSum[19] += ipWei[ucpUnpack[19][11]];
      iSum[20] += ipWei[ucpUnpack[20][11]];
      iSum[21] += ipWei[ucpUnpack[21][11]];
      iSum[22] += ipWei[ucpUnpack[22][11]];
      iSum[23] += ipWei[ucpUnpack[23][11]];

      ipWei = ipLUT[12];
      iSum[0] += ipWei[ucpUnpack[0][12]];
      iSum[1] += ipWei[ucpUnpack[1][12]];
      iSum[2] += ipWei[ucpUnpack[2][12]];
      iSum[3] += ipWei[ucpUnpack[3][12]];
      iSum[4] += ipWei[ucpUnpack[4][12]];
      iSum[5] += ipWei[ucpUnpack[5][12]];
      iSum[6] += ipWei[ucpUnpack[6][12]];
      iSum[7] += ipWei[ucpUnpack[7][12]];
      iSum[8] += ipWei[ucpUnpack[8][12]];
      iSum[9] += ipWei[ucpUnpack[9][12]];
      iSum[10] += ipWei[ucpUnpack[10][12]];
      iSum[11] += ipWei[ucpUnpack[11][12]];
      iSum[12] += ipWei[ucpUnpack[12][12]];
      iSum[13] += ipWei[ucpUnpack[13][12]];
      iSum[14] += ipWei[ucpUnpack[14][12]];
      iSum[15] += ipWei[ucpUnpack[15][12]];
      iSum[16] += ipWei[ucpUnpack[16][12]];
      iSum[17] += ipWei[ucpUnpack[17][12]];
      iSum[18] += ipWei[ucpUnpack[18][12]];
      iSum[19] += ipWei[ucpUnpack[19][12]];
      iSum[20] += ipWei[ucpUnpack[20][12]];
      iSum[21] += ipWei[ucpUnpack[21][12]];
      iSum[22] += ipWei[ucpUnpack[22][12]];
      iSum[23] += ipWei[ucpUnpack[23][12]];

      // clamp and dump the results

      // no rounding - sums were pre-rounded
      // I commented out the "& 0xFF" because I assume the min/max
      // code will take care of making sure the sums are not negative;
      // if that assumption is not valid, they need to be uncommented
      long lTmp1, lTmp2;

      lTmp1 = iSum[0];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[1];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[2];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[3];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent] = lTmp1;

      lTmp1 = iSum[4];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[5];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[6];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[7];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 4] = lTmp1;

      lTmp1 = iSum[8];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[9];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[10];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[11];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 8] = lTmp1;

      lTmp1 = iSum[12];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[13];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[14];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[15];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 12] = lTmp1;

      lTmp1 = iSum[16];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[17];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[18];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[19];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 16] = lTmp1;

      lTmp1 = iSum[20];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[21];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[22];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[23];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 20] = lTmp1;
     }
    ipSP += 2;
    ipP += 2;
   }
}

void CAspectRatio::RenderV_YUV_15 (MTI_UINT8 *ucpDst[2], int &iRow, int iStopDstLocal,
		int iJob)
/*
	Do VERTICAL rendering with 15 neighbors
*/
{
  MTI_UINT8 *ucpD;
  int iField, iR, *ipSP, *ipP, iPixel, iNei;
  int **ipLUT, iSubPixel;
  int iSum[VERTICAL_COMPONENTS], *ipWei;
  MTI_UINT8 *ucpUnpack[VERTICAL_COMPONENTS];
  int iComponent;

  // check for errors
  if ((iRow & 0x1) || (iStopDstLocal & 0x1))
    return;

  int iField0, iField1;

  if (iNFieldDst == 2)
   {
    iField0 = 0;
    iField1 = 1;
   }
  else
   {
    iField0 = 0;
    iField1 = 0;
   }


  ipSP = ipLUTSubPixel + iRow;
  ipP = ipLUTPixel + iRow;
  for (; iRow < iStopDstLocal; iRow+=2)
   {
    // do the "even" field
    iR = iRow / iNFieldDst;

    ucpD = ucpDst[iField0] + (iR * iPitchDst);

    iSubPixel = ipSP[0];
    iPixel = ipP[0];

    for (iComponent = 0; iComponent < iUnpackedComponent; iComponent += VERTICAL_COMPONENTS)
     {
      ipLUT = ipLUTWei[iSubPixel];
      ucpUnpack[0] = ucpUnpacked[iJob][iComponent] + iPixel;
      ucpUnpack[1] = ucpUnpacked[iJob][iComponent + 1] + iPixel;
      ucpUnpack[2] = ucpUnpacked[iJob][iComponent + 2] + iPixel;
      ucpUnpack[3] = ucpUnpacked[iJob][iComponent + 3] + iPixel;
      ucpUnpack[4] = ucpUnpacked[iJob][iComponent + 4] + iPixel;
      ucpUnpack[5] = ucpUnpacked[iJob][iComponent + 5] + iPixel;
      ucpUnpack[6] = ucpUnpacked[iJob][iComponent + 6] + iPixel;
      ucpUnpack[7] = ucpUnpacked[iJob][iComponent + 7] + iPixel;
      ucpUnpack[8] = ucpUnpacked[iJob][iComponent + 8] + iPixel;
      ucpUnpack[9] = ucpUnpacked[iJob][iComponent + 9] + iPixel;
      ucpUnpack[10] = ucpUnpacked[iJob][iComponent + 10] + iPixel;
      ucpUnpack[11] = ucpUnpacked[iJob][iComponent + 11] + iPixel;
      ucpUnpack[12] = ucpUnpacked[iJob][iComponent + 12] + iPixel;
      ucpUnpack[13] = ucpUnpacked[iJob][iComponent + 13] + iPixel;
      ucpUnpack[14] = ucpUnpacked[iJob][iComponent + 14] + iPixel;
      ucpUnpack[15] = ucpUnpacked[iJob][iComponent + 15] + iPixel;
      ucpUnpack[16] = ucpUnpacked[iJob][iComponent + 16] + iPixel;
      ucpUnpack[17] = ucpUnpacked[iJob][iComponent + 17] + iPixel;
      ucpUnpack[18] = ucpUnpacked[iJob][iComponent + 18] + iPixel;
      ucpUnpack[19] = ucpUnpacked[iJob][iComponent + 19] + iPixel;
      ucpUnpack[20] = ucpUnpacked[iJob][iComponent + 20] + iPixel;
      ucpUnpack[21] = ucpUnpacked[iJob][iComponent + 21] + iPixel;
      ucpUnpack[22] = ucpUnpacked[iJob][iComponent + 22] + iPixel;
      ucpUnpack[23] = ucpUnpacked[iJob][iComponent + 23] + iPixel;

      // pre-round the sums
      iSum[0] = iSum[1] = iSum[2] = iSum[3] =
      iSum[4] = iSum[5] = iSum[6] = iSum[7] =
      iSum[8] = iSum[9] = iSum[10] = iSum[11] =
      iSum[12] = iSum[13] = iSum[14] = iSum[15] =
      iSum[16] = iSum[17] = iSum[18] = iSum[19] =
      iSum[20] = iSum[21] = iSum[22] = iSum[23] =
                  iHalfPrecision;

      // do the 15 neighbors
      ipWei = ipLUT[0];
      iSum[0] += ipWei[ucpUnpack[0][0]];
      iSum[1] += ipWei[ucpUnpack[1][0]];
      iSum[2] += ipWei[ucpUnpack[2][0]];
      iSum[3] += ipWei[ucpUnpack[3][0]];
      iSum[4] += ipWei[ucpUnpack[4][0]];
      iSum[5] += ipWei[ucpUnpack[5][0]];
      iSum[6] += ipWei[ucpUnpack[6][0]];
      iSum[7] += ipWei[ucpUnpack[7][0]];
      iSum[8] += ipWei[ucpUnpack[8][0]];
      iSum[9] += ipWei[ucpUnpack[9][0]];
      iSum[10] += ipWei[ucpUnpack[10][0]];
      iSum[11] += ipWei[ucpUnpack[11][0]];
      iSum[12] += ipWei[ucpUnpack[12][0]];
      iSum[13] += ipWei[ucpUnpack[13][0]];
      iSum[14] += ipWei[ucpUnpack[14][0]];
      iSum[15] += ipWei[ucpUnpack[15][0]];
      iSum[16] += ipWei[ucpUnpack[16][0]];
      iSum[17] += ipWei[ucpUnpack[17][0]];
      iSum[18] += ipWei[ucpUnpack[18][0]];
      iSum[19] += ipWei[ucpUnpack[19][0]];
      iSum[20] += ipWei[ucpUnpack[20][0]];
      iSum[21] += ipWei[ucpUnpack[21][0]];
      iSum[22] += ipWei[ucpUnpack[22][0]];
      iSum[23] += ipWei[ucpUnpack[23][0]];

      ipWei = ipLUT[1];
      iSum[0] += ipWei[ucpUnpack[0][1]];
      iSum[1] += ipWei[ucpUnpack[1][1]];
      iSum[2] += ipWei[ucpUnpack[2][1]];
      iSum[3] += ipWei[ucpUnpack[3][1]];
      iSum[4] += ipWei[ucpUnpack[4][1]];
      iSum[5] += ipWei[ucpUnpack[5][1]];
      iSum[6] += ipWei[ucpUnpack[6][1]];
      iSum[7] += ipWei[ucpUnpack[7][1]];
      iSum[8] += ipWei[ucpUnpack[8][1]];
      iSum[9] += ipWei[ucpUnpack[9][1]];
      iSum[10] += ipWei[ucpUnpack[10][1]];
      iSum[11] += ipWei[ucpUnpack[11][1]];
      iSum[12] += ipWei[ucpUnpack[12][1]];
      iSum[13] += ipWei[ucpUnpack[13][1]];
      iSum[14] += ipWei[ucpUnpack[14][1]];
      iSum[15] += ipWei[ucpUnpack[15][1]];
      iSum[16] += ipWei[ucpUnpack[16][1]];
      iSum[17] += ipWei[ucpUnpack[17][1]];
      iSum[18] += ipWei[ucpUnpack[18][1]];
      iSum[19] += ipWei[ucpUnpack[19][1]];
      iSum[20] += ipWei[ucpUnpack[20][1]];
      iSum[21] += ipWei[ucpUnpack[21][1]];
      iSum[22] += ipWei[ucpUnpack[22][1]];
      iSum[23] += ipWei[ucpUnpack[23][1]];

      ipWei = ipLUT[2];
      iSum[0] += ipWei[ucpUnpack[0][2]];
      iSum[1] += ipWei[ucpUnpack[1][2]];
      iSum[2] += ipWei[ucpUnpack[2][2]];
      iSum[3] += ipWei[ucpUnpack[3][2]];
      iSum[4] += ipWei[ucpUnpack[4][2]];
      iSum[5] += ipWei[ucpUnpack[5][2]];
      iSum[6] += ipWei[ucpUnpack[6][2]];
      iSum[7] += ipWei[ucpUnpack[7][2]];
      iSum[8] += ipWei[ucpUnpack[8][2]];
      iSum[9] += ipWei[ucpUnpack[9][2]];
      iSum[10] += ipWei[ucpUnpack[10][2]];
      iSum[11] += ipWei[ucpUnpack[11][2]];
      iSum[12] += ipWei[ucpUnpack[12][2]];
      iSum[13] += ipWei[ucpUnpack[13][2]];
      iSum[14] += ipWei[ucpUnpack[14][2]];
      iSum[15] += ipWei[ucpUnpack[15][2]];
      iSum[16] += ipWei[ucpUnpack[16][2]];
      iSum[17] += ipWei[ucpUnpack[17][2]];
      iSum[18] += ipWei[ucpUnpack[18][2]];
      iSum[19] += ipWei[ucpUnpack[19][2]];
      iSum[20] += ipWei[ucpUnpack[20][2]];
      iSum[21] += ipWei[ucpUnpack[21][2]];
      iSum[22] += ipWei[ucpUnpack[22][2]];
      iSum[23] += ipWei[ucpUnpack[23][2]];

      ipWei = ipLUT[3];
      iSum[0] += ipWei[ucpUnpack[0][3]];
      iSum[1] += ipWei[ucpUnpack[1][3]];
      iSum[2] += ipWei[ucpUnpack[2][3]];
      iSum[3] += ipWei[ucpUnpack[3][3]];
      iSum[4] += ipWei[ucpUnpack[4][3]];
      iSum[5] += ipWei[ucpUnpack[5][3]];
      iSum[6] += ipWei[ucpUnpack[6][3]];
      iSum[7] += ipWei[ucpUnpack[7][3]];
      iSum[8] += ipWei[ucpUnpack[8][3]];
      iSum[9] += ipWei[ucpUnpack[9][3]];
      iSum[10] += ipWei[ucpUnpack[10][3]];
      iSum[11] += ipWei[ucpUnpack[11][3]];
      iSum[12] += ipWei[ucpUnpack[12][3]];
      iSum[13] += ipWei[ucpUnpack[13][3]];
      iSum[14] += ipWei[ucpUnpack[14][3]];
      iSum[15] += ipWei[ucpUnpack[15][3]];
      iSum[16] += ipWei[ucpUnpack[16][3]];
      iSum[17] += ipWei[ucpUnpack[17][3]];
      iSum[18] += ipWei[ucpUnpack[18][3]];
      iSum[19] += ipWei[ucpUnpack[19][3]];
      iSum[20] += ipWei[ucpUnpack[20][3]];
      iSum[21] += ipWei[ucpUnpack[21][3]];
      iSum[22] += ipWei[ucpUnpack[22][3]];
      iSum[23] += ipWei[ucpUnpack[23][3]];

      ipWei = ipLUT[4];
      iSum[0] += ipWei[ucpUnpack[0][4]];
      iSum[1] += ipWei[ucpUnpack[1][4]];
      iSum[2] += ipWei[ucpUnpack[2][4]];
      iSum[3] += ipWei[ucpUnpack[3][4]];
      iSum[4] += ipWei[ucpUnpack[4][4]];
      iSum[5] += ipWei[ucpUnpack[5][4]];
      iSum[6] += ipWei[ucpUnpack[6][4]];
      iSum[7] += ipWei[ucpUnpack[7][4]];
      iSum[8] += ipWei[ucpUnpack[8][4]];
      iSum[9] += ipWei[ucpUnpack[9][4]];
      iSum[10] += ipWei[ucpUnpack[10][4]];
      iSum[11] += ipWei[ucpUnpack[11][4]];
      iSum[12] += ipWei[ucpUnpack[12][4]];
      iSum[13] += ipWei[ucpUnpack[13][4]];
      iSum[14] += ipWei[ucpUnpack[14][4]];
      iSum[15] += ipWei[ucpUnpack[15][4]];
      iSum[16] += ipWei[ucpUnpack[16][4]];
      iSum[17] += ipWei[ucpUnpack[17][4]];
      iSum[18] += ipWei[ucpUnpack[18][4]];
      iSum[19] += ipWei[ucpUnpack[19][4]];
      iSum[20] += ipWei[ucpUnpack[20][4]];
      iSum[21] += ipWei[ucpUnpack[21][4]];
      iSum[22] += ipWei[ucpUnpack[22][4]];
      iSum[23] += ipWei[ucpUnpack[23][4]];

      ipWei = ipLUT[5];
      iSum[0] += ipWei[ucpUnpack[0][5]];
      iSum[1] += ipWei[ucpUnpack[1][5]];
      iSum[2] += ipWei[ucpUnpack[2][5]];
      iSum[3] += ipWei[ucpUnpack[3][5]];
      iSum[4] += ipWei[ucpUnpack[4][5]];
      iSum[5] += ipWei[ucpUnpack[5][5]];
      iSum[6] += ipWei[ucpUnpack[6][5]];
      iSum[7] += ipWei[ucpUnpack[7][5]];
      iSum[8] += ipWei[ucpUnpack[8][5]];
      iSum[9] += ipWei[ucpUnpack[9][5]];
      iSum[10] += ipWei[ucpUnpack[10][5]];
      iSum[11] += ipWei[ucpUnpack[11][5]];
      iSum[12] += ipWei[ucpUnpack[12][5]];
      iSum[13] += ipWei[ucpUnpack[13][5]];
      iSum[14] += ipWei[ucpUnpack[14][5]];
      iSum[15] += ipWei[ucpUnpack[15][5]];
      iSum[16] += ipWei[ucpUnpack[16][5]];
      iSum[17] += ipWei[ucpUnpack[17][5]];
      iSum[18] += ipWei[ucpUnpack[18][5]];
      iSum[19] += ipWei[ucpUnpack[19][5]];
      iSum[20] += ipWei[ucpUnpack[20][5]];
      iSum[21] += ipWei[ucpUnpack[21][5]];
      iSum[22] += ipWei[ucpUnpack[22][5]];
      iSum[23] += ipWei[ucpUnpack[23][5]];

      ipWei = ipLUT[6];
      iSum[0] += ipWei[ucpUnpack[0][6]];
      iSum[1] += ipWei[ucpUnpack[1][6]];
      iSum[2] += ipWei[ucpUnpack[2][6]];
      iSum[3] += ipWei[ucpUnpack[3][6]];
      iSum[4] += ipWei[ucpUnpack[4][6]];
      iSum[5] += ipWei[ucpUnpack[5][6]];
      iSum[6] += ipWei[ucpUnpack[6][6]];
      iSum[7] += ipWei[ucpUnpack[7][6]];
      iSum[8] += ipWei[ucpUnpack[8][6]];
      iSum[9] += ipWei[ucpUnpack[9][6]];
      iSum[10] += ipWei[ucpUnpack[10][6]];
      iSum[11] += ipWei[ucpUnpack[11][6]];
      iSum[12] += ipWei[ucpUnpack[12][6]];
      iSum[13] += ipWei[ucpUnpack[13][6]];
      iSum[14] += ipWei[ucpUnpack[14][6]];
      iSum[15] += ipWei[ucpUnpack[15][6]];
      iSum[16] += ipWei[ucpUnpack[16][6]];
      iSum[17] += ipWei[ucpUnpack[17][6]];
      iSum[18] += ipWei[ucpUnpack[18][6]];
      iSum[19] += ipWei[ucpUnpack[19][6]];
      iSum[20] += ipWei[ucpUnpack[20][6]];
      iSum[21] += ipWei[ucpUnpack[21][6]];
      iSum[22] += ipWei[ucpUnpack[22][6]];
      iSum[23] += ipWei[ucpUnpack[23][6]];

      ipWei = ipLUT[7];
      iSum[0] += ipWei[ucpUnpack[0][7]];
      iSum[1] += ipWei[ucpUnpack[1][7]];
      iSum[2] += ipWei[ucpUnpack[2][7]];
      iSum[3] += ipWei[ucpUnpack[3][7]];
      iSum[4] += ipWei[ucpUnpack[4][7]];
      iSum[5] += ipWei[ucpUnpack[5][7]];
      iSum[6] += ipWei[ucpUnpack[6][7]];
      iSum[7] += ipWei[ucpUnpack[7][7]];
      iSum[8] += ipWei[ucpUnpack[8][7]];
      iSum[9] += ipWei[ucpUnpack[9][7]];
      iSum[10] += ipWei[ucpUnpack[10][7]];
      iSum[11] += ipWei[ucpUnpack[11][7]];
      iSum[12] += ipWei[ucpUnpack[12][7]];
      iSum[13] += ipWei[ucpUnpack[13][7]];
      iSum[14] += ipWei[ucpUnpack[14][7]];
      iSum[15] += ipWei[ucpUnpack[15][7]];
      iSum[16] += ipWei[ucpUnpack[16][7]];
      iSum[17] += ipWei[ucpUnpack[17][7]];
      iSum[18] += ipWei[ucpUnpack[18][7]];
      iSum[19] += ipWei[ucpUnpack[19][7]];
      iSum[20] += ipWei[ucpUnpack[20][7]];
      iSum[21] += ipWei[ucpUnpack[21][7]];
      iSum[22] += ipWei[ucpUnpack[22][7]];
      iSum[23] += ipWei[ucpUnpack[23][7]];

      ipWei = ipLUT[8];
      iSum[0] += ipWei[ucpUnpack[0][8]];
      iSum[1] += ipWei[ucpUnpack[1][8]];
      iSum[2] += ipWei[ucpUnpack[2][8]];
      iSum[3] += ipWei[ucpUnpack[3][8]];
      iSum[4] += ipWei[ucpUnpack[4][8]];
      iSum[5] += ipWei[ucpUnpack[5][8]];
      iSum[6] += ipWei[ucpUnpack[6][8]];
      iSum[7] += ipWei[ucpUnpack[7][8]];
      iSum[8] += ipWei[ucpUnpack[8][8]];
      iSum[9] += ipWei[ucpUnpack[9][8]];
      iSum[10] += ipWei[ucpUnpack[10][8]];
      iSum[11] += ipWei[ucpUnpack[11][8]];
      iSum[12] += ipWei[ucpUnpack[12][8]];
      iSum[13] += ipWei[ucpUnpack[13][8]];
      iSum[14] += ipWei[ucpUnpack[14][8]];
      iSum[15] += ipWei[ucpUnpack[15][8]];
      iSum[16] += ipWei[ucpUnpack[16][8]];
      iSum[17] += ipWei[ucpUnpack[17][8]];
      iSum[18] += ipWei[ucpUnpack[18][8]];
      iSum[19] += ipWei[ucpUnpack[19][8]];
      iSum[20] += ipWei[ucpUnpack[20][8]];
      iSum[21] += ipWei[ucpUnpack[21][8]];
      iSum[22] += ipWei[ucpUnpack[22][8]];
      iSum[23] += ipWei[ucpUnpack[23][8]];

      ipWei = ipLUT[9];
      iSum[0] += ipWei[ucpUnpack[0][9]];
      iSum[1] += ipWei[ucpUnpack[1][9]];
      iSum[2] += ipWei[ucpUnpack[2][9]];
      iSum[3] += ipWei[ucpUnpack[3][9]];
      iSum[4] += ipWei[ucpUnpack[4][9]];
      iSum[5] += ipWei[ucpUnpack[5][9]];
      iSum[6] += ipWei[ucpUnpack[6][9]];
      iSum[7] += ipWei[ucpUnpack[7][9]];
      iSum[8] += ipWei[ucpUnpack[8][9]];
      iSum[9] += ipWei[ucpUnpack[9][9]];
      iSum[10] += ipWei[ucpUnpack[10][9]];
      iSum[11] += ipWei[ucpUnpack[11][9]];
      iSum[12] += ipWei[ucpUnpack[12][9]];
      iSum[13] += ipWei[ucpUnpack[13][9]];
      iSum[14] += ipWei[ucpUnpack[14][9]];
      iSum[15] += ipWei[ucpUnpack[15][9]];
      iSum[16] += ipWei[ucpUnpack[16][9]];
      iSum[17] += ipWei[ucpUnpack[17][9]];
      iSum[18] += ipWei[ucpUnpack[18][9]];
      iSum[19] += ipWei[ucpUnpack[19][9]];
      iSum[20] += ipWei[ucpUnpack[20][9]];
      iSum[21] += ipWei[ucpUnpack[21][9]];
      iSum[22] += ipWei[ucpUnpack[22][9]];
      iSum[23] += ipWei[ucpUnpack[23][9]];

      ipWei = ipLUT[10];
      iSum[0] += ipWei[ucpUnpack[0][10]];
      iSum[1] += ipWei[ucpUnpack[1][10]];
      iSum[2] += ipWei[ucpUnpack[2][10]];
      iSum[3] += ipWei[ucpUnpack[3][10]];
      iSum[4] += ipWei[ucpUnpack[4][10]];
      iSum[5] += ipWei[ucpUnpack[5][10]];
      iSum[6] += ipWei[ucpUnpack[6][10]];
      iSum[7] += ipWei[ucpUnpack[7][10]];
      iSum[8] += ipWei[ucpUnpack[8][10]];
      iSum[9] += ipWei[ucpUnpack[9][10]];
      iSum[10] += ipWei[ucpUnpack[10][10]];
      iSum[11] += ipWei[ucpUnpack[11][10]];
      iSum[12] += ipWei[ucpUnpack[12][10]];
      iSum[13] += ipWei[ucpUnpack[13][10]];
      iSum[14] += ipWei[ucpUnpack[14][10]];
      iSum[15] += ipWei[ucpUnpack[15][10]];
      iSum[16] += ipWei[ucpUnpack[16][10]];
      iSum[17] += ipWei[ucpUnpack[17][10]];
      iSum[18] += ipWei[ucpUnpack[18][10]];
      iSum[19] += ipWei[ucpUnpack[19][10]];
      iSum[20] += ipWei[ucpUnpack[20][10]];
      iSum[21] += ipWei[ucpUnpack[21][10]];
      iSum[22] += ipWei[ucpUnpack[22][10]];
      iSum[23] += ipWei[ucpUnpack[23][10]];

      ipWei = ipLUT[11];
      iSum[0] += ipWei[ucpUnpack[0][11]];
      iSum[1] += ipWei[ucpUnpack[1][11]];
      iSum[2] += ipWei[ucpUnpack[2][11]];
      iSum[3] += ipWei[ucpUnpack[3][11]];
      iSum[4] += ipWei[ucpUnpack[4][11]];
      iSum[5] += ipWei[ucpUnpack[5][11]];
      iSum[6] += ipWei[ucpUnpack[6][11]];
      iSum[7] += ipWei[ucpUnpack[7][11]];
      iSum[8] += ipWei[ucpUnpack[8][11]];
      iSum[9] += ipWei[ucpUnpack[9][11]];
      iSum[10] += ipWei[ucpUnpack[10][11]];
      iSum[11] += ipWei[ucpUnpack[11][11]];
      iSum[12] += ipWei[ucpUnpack[12][11]];
      iSum[13] += ipWei[ucpUnpack[13][11]];
      iSum[14] += ipWei[ucpUnpack[14][11]];
      iSum[15] += ipWei[ucpUnpack[15][11]];
      iSum[16] += ipWei[ucpUnpack[16][11]];
      iSum[17] += ipWei[ucpUnpack[17][11]];
      iSum[18] += ipWei[ucpUnpack[18][11]];
      iSum[19] += ipWei[ucpUnpack[19][11]];
      iSum[20] += ipWei[ucpUnpack[20][11]];
      iSum[21] += ipWei[ucpUnpack[21][11]];
      iSum[22] += ipWei[ucpUnpack[22][11]];
      iSum[23] += ipWei[ucpUnpack[23][11]];

      ipWei = ipLUT[12];
      iSum[0] += ipWei[ucpUnpack[0][12]];
      iSum[1] += ipWei[ucpUnpack[1][12]];
      iSum[2] += ipWei[ucpUnpack[2][12]];
      iSum[3] += ipWei[ucpUnpack[3][12]];
      iSum[4] += ipWei[ucpUnpack[4][12]];
      iSum[5] += ipWei[ucpUnpack[5][12]];
      iSum[6] += ipWei[ucpUnpack[6][12]];
      iSum[7] += ipWei[ucpUnpack[7][12]];
      iSum[8] += ipWei[ucpUnpack[8][12]];
      iSum[9] += ipWei[ucpUnpack[9][12]];
      iSum[10] += ipWei[ucpUnpack[10][12]];
      iSum[11] += ipWei[ucpUnpack[11][12]];
      iSum[12] += ipWei[ucpUnpack[12][12]];
      iSum[13] += ipWei[ucpUnpack[13][12]];
      iSum[14] += ipWei[ucpUnpack[14][12]];
      iSum[15] += ipWei[ucpUnpack[15][12]];
      iSum[16] += ipWei[ucpUnpack[16][12]];
      iSum[17] += ipWei[ucpUnpack[17][12]];
      iSum[18] += ipWei[ucpUnpack[18][12]];
      iSum[19] += ipWei[ucpUnpack[19][12]];
      iSum[20] += ipWei[ucpUnpack[20][12]];
      iSum[21] += ipWei[ucpUnpack[21][12]];
      iSum[22] += ipWei[ucpUnpack[22][12]];
      iSum[23] += ipWei[ucpUnpack[23][12]];

      ipWei = ipLUT[13];
      iSum[0] += ipWei[ucpUnpack[0][13]];
      iSum[1] += ipWei[ucpUnpack[1][13]];
      iSum[2] += ipWei[ucpUnpack[2][13]];
      iSum[3] += ipWei[ucpUnpack[3][13]];
      iSum[4] += ipWei[ucpUnpack[4][13]];
      iSum[5] += ipWei[ucpUnpack[5][13]];
      iSum[6] += ipWei[ucpUnpack[6][13]];
      iSum[7] += ipWei[ucpUnpack[7][13]];
      iSum[8] += ipWei[ucpUnpack[8][13]];
      iSum[9] += ipWei[ucpUnpack[9][13]];
      iSum[10] += ipWei[ucpUnpack[10][13]];
      iSum[11] += ipWei[ucpUnpack[11][13]];
      iSum[12] += ipWei[ucpUnpack[12][13]];
      iSum[13] += ipWei[ucpUnpack[13][13]];
      iSum[14] += ipWei[ucpUnpack[14][13]];
      iSum[15] += ipWei[ucpUnpack[15][13]];
      iSum[16] += ipWei[ucpUnpack[16][13]];
      iSum[17] += ipWei[ucpUnpack[17][13]];
      iSum[18] += ipWei[ucpUnpack[18][13]];
      iSum[19] += ipWei[ucpUnpack[19][13]];
      iSum[20] += ipWei[ucpUnpack[20][13]];
      iSum[21] += ipWei[ucpUnpack[21][13]];
      iSum[22] += ipWei[ucpUnpack[22][13]];
      iSum[23] += ipWei[ucpUnpack[23][13]];

      ipWei = ipLUT[14];
      iSum[0] += ipWei[ucpUnpack[0][14]];
      iSum[1] += ipWei[ucpUnpack[1][14]];
      iSum[2] += ipWei[ucpUnpack[2][14]];
      iSum[3] += ipWei[ucpUnpack[3][14]];
      iSum[4] += ipWei[ucpUnpack[4][14]];
      iSum[5] += ipWei[ucpUnpack[5][14]];
      iSum[6] += ipWei[ucpUnpack[6][14]];
      iSum[7] += ipWei[ucpUnpack[7][14]];
      iSum[8] += ipWei[ucpUnpack[8][14]];
      iSum[9] += ipWei[ucpUnpack[9][14]];
      iSum[10] += ipWei[ucpUnpack[10][14]];
      iSum[11] += ipWei[ucpUnpack[11][14]];
      iSum[12] += ipWei[ucpUnpack[12][14]];
      iSum[13] += ipWei[ucpUnpack[13][14]];
      iSum[14] += ipWei[ucpUnpack[14][14]];
      iSum[15] += ipWei[ucpUnpack[15][14]];
      iSum[16] += ipWei[ucpUnpack[16][14]];
      iSum[17] += ipWei[ucpUnpack[17][14]];
      iSum[18] += ipWei[ucpUnpack[18][14]];
      iSum[19] += ipWei[ucpUnpack[19][14]];
      iSum[20] += ipWei[ucpUnpack[20][14]];
      iSum[21] += ipWei[ucpUnpack[21][14]];
      iSum[22] += ipWei[ucpUnpack[22][14]];
      iSum[23] += ipWei[ucpUnpack[23][14]];

      // clamp and dump the results

      // no rounding - sums were pre-rounded
      // I commented out the "& 0xFF" because I assume the min/max
      // code will take care of making sure the sums are not negative;
      // if that assumption is not valid, they need to be uncommented
      long lTmp1, lTmp2;

      lTmp1 = iSum[0];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[1];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[2];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[3];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent] = lTmp1;

      lTmp1 = iSum[4];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[5];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[6];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[7];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 4] = lTmp1;

      lTmp1 = iSum[8];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[9];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[10];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[11];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 8] = lTmp1;

      lTmp1 = iSum[12];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[13];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[14];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[15];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 12] = lTmp1;

      lTmp1 = iSum[16];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[17];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[18];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[19];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 16] = lTmp1;

      lTmp1 = iSum[20];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[21];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[22];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[23];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 20] = lTmp1;
    }

    // do the "odd" field
    iR = (iRow+1) / iNFieldDst;

    ucpD = ucpDst[iField1] + (iR * iPitchDst);

    iSubPixel = ipSP[1];
    iPixel = ipP[1];

    for (iComponent = 0; iComponent < iUnpackedComponent; iComponent += VERTICAL_COMPONENTS)
     {
      ipLUT = ipLUTWei[iSubPixel];
      ucpUnpack[0] = ucpUnpacked[iJob][iComponent] + iPixel;
      ucpUnpack[1] = ucpUnpacked[iJob][iComponent + 1] + iPixel;
      ucpUnpack[2] = ucpUnpacked[iJob][iComponent + 2] + iPixel;
      ucpUnpack[3] = ucpUnpacked[iJob][iComponent + 3] + iPixel;
      ucpUnpack[4] = ucpUnpacked[iJob][iComponent + 4] + iPixel;
      ucpUnpack[5] = ucpUnpacked[iJob][iComponent + 5] + iPixel;
      ucpUnpack[6] = ucpUnpacked[iJob][iComponent + 6] + iPixel;
      ucpUnpack[7] = ucpUnpacked[iJob][iComponent + 7] + iPixel;
      ucpUnpack[8] = ucpUnpacked[iJob][iComponent + 8] + iPixel;
      ucpUnpack[9] = ucpUnpacked[iJob][iComponent + 9] + iPixel;
      ucpUnpack[10] = ucpUnpacked[iJob][iComponent + 10] + iPixel;
      ucpUnpack[11] = ucpUnpacked[iJob][iComponent + 11] + iPixel;
      ucpUnpack[12] = ucpUnpacked[iJob][iComponent + 12] + iPixel;
      ucpUnpack[13] = ucpUnpacked[iJob][iComponent + 13] + iPixel;
      ucpUnpack[14] = ucpUnpacked[iJob][iComponent + 14] + iPixel;
      ucpUnpack[15] = ucpUnpacked[iJob][iComponent + 15] + iPixel;
      ucpUnpack[16] = ucpUnpacked[iJob][iComponent + 16] + iPixel;
      ucpUnpack[17] = ucpUnpacked[iJob][iComponent + 17] + iPixel;
      ucpUnpack[18] = ucpUnpacked[iJob][iComponent + 18] + iPixel;
      ucpUnpack[19] = ucpUnpacked[iJob][iComponent + 19] + iPixel;
      ucpUnpack[20] = ucpUnpacked[iJob][iComponent + 20] + iPixel;
      ucpUnpack[21] = ucpUnpacked[iJob][iComponent + 21] + iPixel;
      ucpUnpack[22] = ucpUnpacked[iJob][iComponent + 22] + iPixel;
      ucpUnpack[23] = ucpUnpacked[iJob][iComponent + 23] + iPixel;

      // pre-round the sums
      iSum[0] = iSum[1] = iSum[2] = iSum[3] =
      iSum[4] = iSum[5] = iSum[6] = iSum[7] =
      iSum[8] = iSum[9] = iSum[10] = iSum[11] =
      iSum[12] = iSum[13] = iSum[14] = iSum[15] =
      iSum[16] = iSum[17] = iSum[18] = iSum[19] =
      iSum[20] = iSum[21] = iSum[22] = iSum[23] =
                  iHalfPrecision;

      // do the 15 neighbors
      ipWei = ipLUT[0];
      iSum[0] += ipWei[ucpUnpack[0][0]];
      iSum[1] += ipWei[ucpUnpack[1][0]];
      iSum[2] += ipWei[ucpUnpack[2][0]];
      iSum[3] += ipWei[ucpUnpack[3][0]];
      iSum[4] += ipWei[ucpUnpack[4][0]];
      iSum[5] += ipWei[ucpUnpack[5][0]];
      iSum[6] += ipWei[ucpUnpack[6][0]];
      iSum[7] += ipWei[ucpUnpack[7][0]];
      iSum[8] += ipWei[ucpUnpack[8][0]];
      iSum[9] += ipWei[ucpUnpack[9][0]];
      iSum[10] += ipWei[ucpUnpack[10][0]];
      iSum[11] += ipWei[ucpUnpack[11][0]];
      iSum[12] += ipWei[ucpUnpack[12][0]];
      iSum[13] += ipWei[ucpUnpack[13][0]];
      iSum[14] += ipWei[ucpUnpack[14][0]];
      iSum[15] += ipWei[ucpUnpack[15][0]];
      iSum[16] += ipWei[ucpUnpack[16][0]];
      iSum[17] += ipWei[ucpUnpack[17][0]];
      iSum[18] += ipWei[ucpUnpack[18][0]];
      iSum[19] += ipWei[ucpUnpack[19][0]];
      iSum[20] += ipWei[ucpUnpack[20][0]];
      iSum[21] += ipWei[ucpUnpack[21][0]];
      iSum[22] += ipWei[ucpUnpack[22][0]];
      iSum[23] += ipWei[ucpUnpack[23][0]];

      ipWei = ipLUT[1];
      iSum[0] += ipWei[ucpUnpack[0][1]];
      iSum[1] += ipWei[ucpUnpack[1][1]];
      iSum[2] += ipWei[ucpUnpack[2][1]];
      iSum[3] += ipWei[ucpUnpack[3][1]];
      iSum[4] += ipWei[ucpUnpack[4][1]];
      iSum[5] += ipWei[ucpUnpack[5][1]];
      iSum[6] += ipWei[ucpUnpack[6][1]];
      iSum[7] += ipWei[ucpUnpack[7][1]];
      iSum[8] += ipWei[ucpUnpack[8][1]];
      iSum[9] += ipWei[ucpUnpack[9][1]];
      iSum[10] += ipWei[ucpUnpack[10][1]];
      iSum[11] += ipWei[ucpUnpack[11][1]];
      iSum[12] += ipWei[ucpUnpack[12][1]];
      iSum[13] += ipWei[ucpUnpack[13][1]];
      iSum[14] += ipWei[ucpUnpack[14][1]];
      iSum[15] += ipWei[ucpUnpack[15][1]];
      iSum[16] += ipWei[ucpUnpack[16][1]];
      iSum[17] += ipWei[ucpUnpack[17][1]];
      iSum[18] += ipWei[ucpUnpack[18][1]];
      iSum[19] += ipWei[ucpUnpack[19][1]];
      iSum[20] += ipWei[ucpUnpack[20][1]];
      iSum[21] += ipWei[ucpUnpack[21][1]];
      iSum[22] += ipWei[ucpUnpack[22][1]];
      iSum[23] += ipWei[ucpUnpack[23][1]];

      ipWei = ipLUT[2];
      iSum[0] += ipWei[ucpUnpack[0][2]];
      iSum[1] += ipWei[ucpUnpack[1][2]];
      iSum[2] += ipWei[ucpUnpack[2][2]];
      iSum[3] += ipWei[ucpUnpack[3][2]];
      iSum[4] += ipWei[ucpUnpack[4][2]];
      iSum[5] += ipWei[ucpUnpack[5][2]];
      iSum[6] += ipWei[ucpUnpack[6][2]];
      iSum[7] += ipWei[ucpUnpack[7][2]];
      iSum[8] += ipWei[ucpUnpack[8][2]];
      iSum[9] += ipWei[ucpUnpack[9][2]];
      iSum[10] += ipWei[ucpUnpack[10][2]];
      iSum[11] += ipWei[ucpUnpack[11][2]];
      iSum[12] += ipWei[ucpUnpack[12][2]];
      iSum[13] += ipWei[ucpUnpack[13][2]];
      iSum[14] += ipWei[ucpUnpack[14][2]];
      iSum[15] += ipWei[ucpUnpack[15][2]];
      iSum[16] += ipWei[ucpUnpack[16][2]];
      iSum[17] += ipWei[ucpUnpack[17][2]];
      iSum[18] += ipWei[ucpUnpack[18][2]];
      iSum[19] += ipWei[ucpUnpack[19][2]];
      iSum[20] += ipWei[ucpUnpack[20][2]];
      iSum[21] += ipWei[ucpUnpack[21][2]];
      iSum[22] += ipWei[ucpUnpack[22][2]];
      iSum[23] += ipWei[ucpUnpack[23][2]];

      ipWei = ipLUT[3];
      iSum[0] += ipWei[ucpUnpack[0][3]];
      iSum[1] += ipWei[ucpUnpack[1][3]];
      iSum[2] += ipWei[ucpUnpack[2][3]];
      iSum[3] += ipWei[ucpUnpack[3][3]];
      iSum[4] += ipWei[ucpUnpack[4][3]];
      iSum[5] += ipWei[ucpUnpack[5][3]];
      iSum[6] += ipWei[ucpUnpack[6][3]];
      iSum[7] += ipWei[ucpUnpack[7][3]];
      iSum[8] += ipWei[ucpUnpack[8][3]];
      iSum[9] += ipWei[ucpUnpack[9][3]];
      iSum[10] += ipWei[ucpUnpack[10][3]];
      iSum[11] += ipWei[ucpUnpack[11][3]];
      iSum[12] += ipWei[ucpUnpack[12][3]];
      iSum[13] += ipWei[ucpUnpack[13][3]];
      iSum[14] += ipWei[ucpUnpack[14][3]];
      iSum[15] += ipWei[ucpUnpack[15][3]];
      iSum[16] += ipWei[ucpUnpack[16][3]];
      iSum[17] += ipWei[ucpUnpack[17][3]];
      iSum[18] += ipWei[ucpUnpack[18][3]];
      iSum[19] += ipWei[ucpUnpack[19][3]];
      iSum[20] += ipWei[ucpUnpack[20][3]];
      iSum[21] += ipWei[ucpUnpack[21][3]];
      iSum[22] += ipWei[ucpUnpack[22][3]];
      iSum[23] += ipWei[ucpUnpack[23][3]];

      ipWei = ipLUT[4];
      iSum[0] += ipWei[ucpUnpack[0][4]];
      iSum[1] += ipWei[ucpUnpack[1][4]];
      iSum[2] += ipWei[ucpUnpack[2][4]];
      iSum[3] += ipWei[ucpUnpack[3][4]];
      iSum[4] += ipWei[ucpUnpack[4][4]];
      iSum[5] += ipWei[ucpUnpack[5][4]];
      iSum[6] += ipWei[ucpUnpack[6][4]];
      iSum[7] += ipWei[ucpUnpack[7][4]];
      iSum[8] += ipWei[ucpUnpack[8][4]];
      iSum[9] += ipWei[ucpUnpack[9][4]];
      iSum[10] += ipWei[ucpUnpack[10][4]];
      iSum[11] += ipWei[ucpUnpack[11][4]];
      iSum[12] += ipWei[ucpUnpack[12][4]];
      iSum[13] += ipWei[ucpUnpack[13][4]];
      iSum[14] += ipWei[ucpUnpack[14][4]];
      iSum[15] += ipWei[ucpUnpack[15][4]];
      iSum[16] += ipWei[ucpUnpack[16][4]];
      iSum[17] += ipWei[ucpUnpack[17][4]];
      iSum[18] += ipWei[ucpUnpack[18][4]];
      iSum[19] += ipWei[ucpUnpack[19][4]];
      iSum[20] += ipWei[ucpUnpack[20][4]];
      iSum[21] += ipWei[ucpUnpack[21][4]];
      iSum[22] += ipWei[ucpUnpack[22][4]];
      iSum[23] += ipWei[ucpUnpack[23][4]];

      ipWei = ipLUT[5];
      iSum[0] += ipWei[ucpUnpack[0][5]];
      iSum[1] += ipWei[ucpUnpack[1][5]];
      iSum[2] += ipWei[ucpUnpack[2][5]];
      iSum[3] += ipWei[ucpUnpack[3][5]];
      iSum[4] += ipWei[ucpUnpack[4][5]];
      iSum[5] += ipWei[ucpUnpack[5][5]];
      iSum[6] += ipWei[ucpUnpack[6][5]];
      iSum[7] += ipWei[ucpUnpack[7][5]];
      iSum[8] += ipWei[ucpUnpack[8][5]];
      iSum[9] += ipWei[ucpUnpack[9][5]];
      iSum[10] += ipWei[ucpUnpack[10][5]];
      iSum[11] += ipWei[ucpUnpack[11][5]];
      iSum[12] += ipWei[ucpUnpack[12][5]];
      iSum[13] += ipWei[ucpUnpack[13][5]];
      iSum[14] += ipWei[ucpUnpack[14][5]];
      iSum[15] += ipWei[ucpUnpack[15][5]];
      iSum[16] += ipWei[ucpUnpack[16][5]];
      iSum[17] += ipWei[ucpUnpack[17][5]];
      iSum[18] += ipWei[ucpUnpack[18][5]];
      iSum[19] += ipWei[ucpUnpack[19][5]];
      iSum[20] += ipWei[ucpUnpack[20][5]];
      iSum[21] += ipWei[ucpUnpack[21][5]];
      iSum[22] += ipWei[ucpUnpack[22][5]];
      iSum[23] += ipWei[ucpUnpack[23][5]];

      ipWei = ipLUT[6];
      iSum[0] += ipWei[ucpUnpack[0][6]];
      iSum[1] += ipWei[ucpUnpack[1][6]];
      iSum[2] += ipWei[ucpUnpack[2][6]];
      iSum[3] += ipWei[ucpUnpack[3][6]];
      iSum[4] += ipWei[ucpUnpack[4][6]];
      iSum[5] += ipWei[ucpUnpack[5][6]];
      iSum[6] += ipWei[ucpUnpack[6][6]];
      iSum[7] += ipWei[ucpUnpack[7][6]];
      iSum[8] += ipWei[ucpUnpack[8][6]];
      iSum[9] += ipWei[ucpUnpack[9][6]];
      iSum[10] += ipWei[ucpUnpack[10][6]];
      iSum[11] += ipWei[ucpUnpack[11][6]];
      iSum[12] += ipWei[ucpUnpack[12][6]];
      iSum[13] += ipWei[ucpUnpack[13][6]];
      iSum[14] += ipWei[ucpUnpack[14][6]];
      iSum[15] += ipWei[ucpUnpack[15][6]];
      iSum[16] += ipWei[ucpUnpack[16][6]];
      iSum[17] += ipWei[ucpUnpack[17][6]];
      iSum[18] += ipWei[ucpUnpack[18][6]];
      iSum[19] += ipWei[ucpUnpack[19][6]];
      iSum[20] += ipWei[ucpUnpack[20][6]];
      iSum[21] += ipWei[ucpUnpack[21][6]];
      iSum[22] += ipWei[ucpUnpack[22][6]];
      iSum[23] += ipWei[ucpUnpack[23][6]];

      ipWei = ipLUT[7];
      iSum[0] += ipWei[ucpUnpack[0][7]];
      iSum[1] += ipWei[ucpUnpack[1][7]];
      iSum[2] += ipWei[ucpUnpack[2][7]];
      iSum[3] += ipWei[ucpUnpack[3][7]];
      iSum[4] += ipWei[ucpUnpack[4][7]];
      iSum[5] += ipWei[ucpUnpack[5][7]];
      iSum[6] += ipWei[ucpUnpack[6][7]];
      iSum[7] += ipWei[ucpUnpack[7][7]];
      iSum[8] += ipWei[ucpUnpack[8][7]];
      iSum[9] += ipWei[ucpUnpack[9][7]];
      iSum[10] += ipWei[ucpUnpack[10][7]];
      iSum[11] += ipWei[ucpUnpack[11][7]];
      iSum[12] += ipWei[ucpUnpack[12][7]];
      iSum[13] += ipWei[ucpUnpack[13][7]];
      iSum[14] += ipWei[ucpUnpack[14][7]];
      iSum[15] += ipWei[ucpUnpack[15][7]];
      iSum[16] += ipWei[ucpUnpack[16][7]];
      iSum[17] += ipWei[ucpUnpack[17][7]];
      iSum[18] += ipWei[ucpUnpack[18][7]];
      iSum[19] += ipWei[ucpUnpack[19][7]];
      iSum[20] += ipWei[ucpUnpack[20][7]];
      iSum[21] += ipWei[ucpUnpack[21][7]];
      iSum[22] += ipWei[ucpUnpack[22][7]];
      iSum[23] += ipWei[ucpUnpack[23][7]];

      ipWei = ipLUT[8];
      iSum[0] += ipWei[ucpUnpack[0][8]];
      iSum[1] += ipWei[ucpUnpack[1][8]];
      iSum[2] += ipWei[ucpUnpack[2][8]];
      iSum[3] += ipWei[ucpUnpack[3][8]];
      iSum[4] += ipWei[ucpUnpack[4][8]];
      iSum[5] += ipWei[ucpUnpack[5][8]];
      iSum[6] += ipWei[ucpUnpack[6][8]];
      iSum[7] += ipWei[ucpUnpack[7][8]];
      iSum[8] += ipWei[ucpUnpack[8][8]];
      iSum[9] += ipWei[ucpUnpack[9][8]];
      iSum[10] += ipWei[ucpUnpack[10][8]];
      iSum[11] += ipWei[ucpUnpack[11][8]];
      iSum[12] += ipWei[ucpUnpack[12][8]];
      iSum[13] += ipWei[ucpUnpack[13][8]];
      iSum[14] += ipWei[ucpUnpack[14][8]];
      iSum[15] += ipWei[ucpUnpack[15][8]];
      iSum[16] += ipWei[ucpUnpack[16][8]];
      iSum[17] += ipWei[ucpUnpack[17][8]];
      iSum[18] += ipWei[ucpUnpack[18][8]];
      iSum[19] += ipWei[ucpUnpack[19][8]];
      iSum[20] += ipWei[ucpUnpack[20][8]];
      iSum[21] += ipWei[ucpUnpack[21][8]];
      iSum[22] += ipWei[ucpUnpack[22][8]];
      iSum[23] += ipWei[ucpUnpack[23][8]];

      ipWei = ipLUT[9];
      iSum[0] += ipWei[ucpUnpack[0][9]];
      iSum[1] += ipWei[ucpUnpack[1][9]];
      iSum[2] += ipWei[ucpUnpack[2][9]];
      iSum[3] += ipWei[ucpUnpack[3][9]];
      iSum[4] += ipWei[ucpUnpack[4][9]];
      iSum[5] += ipWei[ucpUnpack[5][9]];
      iSum[6] += ipWei[ucpUnpack[6][9]];
      iSum[7] += ipWei[ucpUnpack[7][9]];
      iSum[8] += ipWei[ucpUnpack[8][9]];
      iSum[9] += ipWei[ucpUnpack[9][9]];
      iSum[10] += ipWei[ucpUnpack[10][9]];
      iSum[11] += ipWei[ucpUnpack[11][9]];
      iSum[12] += ipWei[ucpUnpack[12][9]];
      iSum[13] += ipWei[ucpUnpack[13][9]];
      iSum[14] += ipWei[ucpUnpack[14][9]];
      iSum[15] += ipWei[ucpUnpack[15][9]];
      iSum[16] += ipWei[ucpUnpack[16][9]];
      iSum[17] += ipWei[ucpUnpack[17][9]];
      iSum[18] += ipWei[ucpUnpack[18][9]];
      iSum[19] += ipWei[ucpUnpack[19][9]];
      iSum[20] += ipWei[ucpUnpack[20][9]];
      iSum[21] += ipWei[ucpUnpack[21][9]];
      iSum[22] += ipWei[ucpUnpack[22][9]];
      iSum[23] += ipWei[ucpUnpack[23][9]];

      ipWei = ipLUT[10];
      iSum[0] += ipWei[ucpUnpack[0][10]];
      iSum[1] += ipWei[ucpUnpack[1][10]];
      iSum[2] += ipWei[ucpUnpack[2][10]];
      iSum[3] += ipWei[ucpUnpack[3][10]];
      iSum[4] += ipWei[ucpUnpack[4][10]];
      iSum[5] += ipWei[ucpUnpack[5][10]];
      iSum[6] += ipWei[ucpUnpack[6][10]];
      iSum[7] += ipWei[ucpUnpack[7][10]];
      iSum[8] += ipWei[ucpUnpack[8][10]];
      iSum[9] += ipWei[ucpUnpack[9][10]];
      iSum[10] += ipWei[ucpUnpack[10][10]];
      iSum[11] += ipWei[ucpUnpack[11][10]];
      iSum[12] += ipWei[ucpUnpack[12][10]];
      iSum[13] += ipWei[ucpUnpack[13][10]];
      iSum[14] += ipWei[ucpUnpack[14][10]];
      iSum[15] += ipWei[ucpUnpack[15][10]];
      iSum[16] += ipWei[ucpUnpack[16][10]];
      iSum[17] += ipWei[ucpUnpack[17][10]];
      iSum[18] += ipWei[ucpUnpack[18][10]];
      iSum[19] += ipWei[ucpUnpack[19][10]];
      iSum[20] += ipWei[ucpUnpack[20][10]];
      iSum[21] += ipWei[ucpUnpack[21][10]];
      iSum[22] += ipWei[ucpUnpack[22][10]];
      iSum[23] += ipWei[ucpUnpack[23][10]];

      ipWei = ipLUT[11];
      iSum[0] += ipWei[ucpUnpack[0][11]];
      iSum[1] += ipWei[ucpUnpack[1][11]];
      iSum[2] += ipWei[ucpUnpack[2][11]];
      iSum[3] += ipWei[ucpUnpack[3][11]];
      iSum[4] += ipWei[ucpUnpack[4][11]];
      iSum[5] += ipWei[ucpUnpack[5][11]];
      iSum[6] += ipWei[ucpUnpack[6][11]];
      iSum[7] += ipWei[ucpUnpack[7][11]];
      iSum[8] += ipWei[ucpUnpack[8][11]];
      iSum[9] += ipWei[ucpUnpack[9][11]];
      iSum[10] += ipWei[ucpUnpack[10][11]];
      iSum[11] += ipWei[ucpUnpack[11][11]];
      iSum[12] += ipWei[ucpUnpack[12][11]];
      iSum[13] += ipWei[ucpUnpack[13][11]];
      iSum[14] += ipWei[ucpUnpack[14][11]];
      iSum[15] += ipWei[ucpUnpack[15][11]];
      iSum[16] += ipWei[ucpUnpack[16][11]];
      iSum[17] += ipWei[ucpUnpack[17][11]];
      iSum[18] += ipWei[ucpUnpack[18][11]];
      iSum[19] += ipWei[ucpUnpack[19][11]];
      iSum[20] += ipWei[ucpUnpack[20][11]];
      iSum[21] += ipWei[ucpUnpack[21][11]];
      iSum[22] += ipWei[ucpUnpack[22][11]];
      iSum[23] += ipWei[ucpUnpack[23][11]];

      ipWei = ipLUT[12];
      iSum[0] += ipWei[ucpUnpack[0][12]];
      iSum[1] += ipWei[ucpUnpack[1][12]];
      iSum[2] += ipWei[ucpUnpack[2][12]];
      iSum[3] += ipWei[ucpUnpack[3][12]];
      iSum[4] += ipWei[ucpUnpack[4][12]];
      iSum[5] += ipWei[ucpUnpack[5][12]];
      iSum[6] += ipWei[ucpUnpack[6][12]];
      iSum[7] += ipWei[ucpUnpack[7][12]];
      iSum[8] += ipWei[ucpUnpack[8][12]];
      iSum[9] += ipWei[ucpUnpack[9][12]];
      iSum[10] += ipWei[ucpUnpack[10][12]];
      iSum[11] += ipWei[ucpUnpack[11][12]];
      iSum[12] += ipWei[ucpUnpack[12][12]];
      iSum[13] += ipWei[ucpUnpack[13][12]];
      iSum[14] += ipWei[ucpUnpack[14][12]];
      iSum[15] += ipWei[ucpUnpack[15][12]];
      iSum[16] += ipWei[ucpUnpack[16][12]];
      iSum[17] += ipWei[ucpUnpack[17][12]];
      iSum[18] += ipWei[ucpUnpack[18][12]];
      iSum[19] += ipWei[ucpUnpack[19][12]];
      iSum[20] += ipWei[ucpUnpack[20][12]];
      iSum[21] += ipWei[ucpUnpack[21][12]];
      iSum[22] += ipWei[ucpUnpack[22][12]];
      iSum[23] += ipWei[ucpUnpack[23][12]];

      ipWei = ipLUT[13];
      iSum[0] += ipWei[ucpUnpack[0][13]];
      iSum[1] += ipWei[ucpUnpack[1][13]];
      iSum[2] += ipWei[ucpUnpack[2][13]];
      iSum[3] += ipWei[ucpUnpack[3][13]];
      iSum[4] += ipWei[ucpUnpack[4][13]];
      iSum[5] += ipWei[ucpUnpack[5][13]];
      iSum[6] += ipWei[ucpUnpack[6][13]];
      iSum[7] += ipWei[ucpUnpack[7][13]];
      iSum[8] += ipWei[ucpUnpack[8][13]];
      iSum[9] += ipWei[ucpUnpack[9][13]];
      iSum[10] += ipWei[ucpUnpack[10][13]];
      iSum[11] += ipWei[ucpUnpack[11][13]];
      iSum[12] += ipWei[ucpUnpack[12][13]];
      iSum[13] += ipWei[ucpUnpack[13][13]];
      iSum[14] += ipWei[ucpUnpack[14][13]];
      iSum[15] += ipWei[ucpUnpack[15][13]];
      iSum[16] += ipWei[ucpUnpack[16][13]];
      iSum[17] += ipWei[ucpUnpack[17][13]];
      iSum[18] += ipWei[ucpUnpack[18][13]];
      iSum[19] += ipWei[ucpUnpack[19][13]];
      iSum[20] += ipWei[ucpUnpack[20][13]];
      iSum[21] += ipWei[ucpUnpack[21][13]];
      iSum[22] += ipWei[ucpUnpack[22][13]];
      iSum[23] += ipWei[ucpUnpack[23][13]];

      ipWei = ipLUT[14];
      iSum[0] += ipWei[ucpUnpack[0][14]];
      iSum[1] += ipWei[ucpUnpack[1][14]];
      iSum[2] += ipWei[ucpUnpack[2][14]];
      iSum[3] += ipWei[ucpUnpack[3][14]];
      iSum[4] += ipWei[ucpUnpack[4][14]];
      iSum[5] += ipWei[ucpUnpack[5][14]];
      iSum[6] += ipWei[ucpUnpack[6][14]];
      iSum[7] += ipWei[ucpUnpack[7][14]];
      iSum[8] += ipWei[ucpUnpack[8][14]];
      iSum[9] += ipWei[ucpUnpack[9][14]];
      iSum[10] += ipWei[ucpUnpack[10][14]];
      iSum[11] += ipWei[ucpUnpack[11][14]];
      iSum[12] += ipWei[ucpUnpack[12][14]];
      iSum[13] += ipWei[ucpUnpack[13][14]];
      iSum[14] += ipWei[ucpUnpack[14][14]];
      iSum[15] += ipWei[ucpUnpack[15][14]];
      iSum[16] += ipWei[ucpUnpack[16][14]];
      iSum[17] += ipWei[ucpUnpack[17][14]];
      iSum[18] += ipWei[ucpUnpack[18][14]];
      iSum[19] += ipWei[ucpUnpack[19][14]];
      iSum[20] += ipWei[ucpUnpack[20][14]];
      iSum[21] += ipWei[ucpUnpack[21][14]];
      iSum[22] += ipWei[ucpUnpack[22][14]];
      iSum[23] += ipWei[ucpUnpack[23][14]];

      // clamp and dump the results

      // no rounding - sums were pre-rounded
      // I commented out the "& 0xFF" because I assume the min/max
      // code will take care of making sure the sums are not negative;
      // if that assumption is not valid, they need to be uncommented
      long lTmp1, lTmp2;

      lTmp1 = iSum[0];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[1];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[2];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[3];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent] = lTmp1;

      lTmp1 = iSum[4];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[5];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[6];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[7];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 4] = lTmp1;

      lTmp1 = iSum[8];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[9];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[10];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[11];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 8] = lTmp1;

      lTmp1 = iSum[12];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[13];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[14];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[15];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 12] = lTmp1;

      lTmp1 = iSum[16];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[17];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[18];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[19];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 16] = lTmp1;

      lTmp1 = iSum[20];
      if (lTmp1 > iClampMax)
        lTmp1 = iClampMax;
      else if (lTmp1 < iClampMin)
        lTmp1 = iClampMin;
      lTmp1 >>= PRECISION;
      // lTmp1 &= 0xFF;

      lTmp2 = iSum[21];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 8;

      lTmp2 = iSum[22];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 16;

      lTmp2 = iSum[23];
      if (lTmp2 > iClampMax)
        lTmp2 = iClampMax;
      else if (lTmp2 < iClampMin)
        lTmp2 = iClampMin;
      lTmp1 |= ((lTmp2 >> PRECISION) /* & 0xFF */) << 24;
      *(long *) &ucpD[iComponent + 20] = lTmp1;
     }
    ipSP += 2;
    ipP += 2;
   }
}

void CAspectRatio::RenderV_YUV (MTI_UINT8 *ucpDst[2], int &iRow, int iStopDstLocal,
		int iJob)
/*
	Do VERTICAL rendering with arbitrary neighbors
*/
{
  MTI_UINT8 *ucpD;
  int iField, iR, *ipSP, *ipP, iPixel, iNei;
  int **ipLUT, iSubPixel;
  int iSum, *ipWei;
  MTI_UINT8 *ucpUnpack;
  int iComponent;

  // check for errors
  if ((iRow & 0x1) || (iStopDstLocal & 0x1))
    return;

  int iField0, iField1;

  if (iNFieldDst == 2)
   {
    iField0 = 0;
    iField1 = 1;
   }
  else
   {
    iField0 = 0;
    iField1 = 0;
   }


  ipSP = ipLUTSubPixel + iRow;
  ipP = ipLUTPixel + iRow;
  for (; iRow < iStopDstLocal; iRow+=2)
   {
    // do the "even" field
    iR = iRow / iNFieldDst;

    ucpD = ucpDst[iField0] + (iR * iPitchDst);

    iSubPixel = *ipSP++;
    iPixel = *ipP++;

    for (iComponent = 0; iComponent < iUnpackedComponent; iComponent++)
     {
      ipLUT = ipLUTWei[iSubPixel];
      ucpUnpack = ucpUnpacked[iJob][iComponent];

      ucpUnpack += iPixel;

      iSum = 0;
      for (iNei = 0; iNei < iTotalNeighbor; iNei++)
       {
        ipWei = *ipLUT++;

        iSum += ipWei[*ucpUnpack++];
       }

      // clamp the result 
      if (iSum > iClampMax)
        iSum = iClampMax;
      else if (iSum < iClampMin)
        iSum = iClampMin;

      iSum = ((iSum + iHalfPrecision) >> PRECISION);

      *ucpD++ = iSum;
     }

    // do the "odd" field
    iR = (iRow+1) / iNFieldDst;

    ucpD = ucpDst[iField1] + (iR * iPitchDst);

    iSubPixel = *ipSP++;
    iPixel = *ipP++;

    for (iComponent = 0; iComponent < iUnpackedComponent; iComponent++)
     {
      ipLUT = ipLUTWei[iSubPixel];
      ucpUnpack = ucpUnpacked[iJob][iComponent];

      ucpUnpack += iPixel;

      iSum = 0;
      for (iNei = 0; iNei < iTotalNeighbor; iNei++)
       {
        ipWei = *ipLUT++;

        iSum += ipWei[*ucpUnpack++];
       }

      // clamp the result 
      if (iSum > iClampMax)
        iSum = iClampMax;
      else if (iSum < iClampMin)
        iSum = iClampMin;

      iSum = ((iSum + iHalfPrecision) >> PRECISION);

      *ucpD++ = iSum;
     }
   }
}




