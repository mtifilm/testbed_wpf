#ifndef PROXYBITMAP_H
#define PROXYBITMAP_H

#include "imgToolDLL.h"

#ifdef _WINDOWS
#include <windows.h>
#else
// roll our own for non-windows platforms...
#define HDC void*
#define HBITMAP void*
#endif


class CConvert;
class CImageFormat;
class CLUTManager;
class CDisplayFormat;
class CDisplayLUT;

// BITMAP class to create blittable BGRA frame-buffers

class MTI_IMGTOOLDLL_API ProxyBitmap
{
public:

   ProxyBitmap(CConvert *sharedConverter = NULL);

   ~ProxyBitmap();

   int initBitmap(HDC hndl, int wdth, int hght);
   void clearBitmap();

   HDC getDCHandle() const                    { return hBitmapDC;  };
   unsigned int *getBitmapFrameBuffer() const { return bitmapFB;   };
   int getWidth() const                       { return bitmapWdth; };
   int getHeight() const                      { return bitmapHght; };

   int renderToBitmap(MTI_UINT8 **frameData,
                      const string &imageFormatName,
                      const RECT *srcRect = NULL,     // full frame by default
                      const RECT *dstRect = NULL);    // full frame by default

   int renderToBitmap(MTI_UINT8 **frameData,
                      const CImageFormat *imageFormat,
                      const RECT *srcRect = NULL,     // full frame by default
                      const RECT *dstRect = NULL);    // full frame by default


private:

   // handle to memory device context for DIB
   HDC hBitmapDC;

   // bitmap dimensions
   int bitmapWdth, bitmapHght;

   // handle to bitmap
   HBITMAP hBitmap;

   // -> frame buffer
   unsigned int *bitmapFB;

   // LUT crap to deal with gamma because Convert doesn't
   // This is all copied from Displayer
   CLUTManager *lutmgr;
   CDisplayLUT *currentDisplayLUT;

   // Proxy converter
   CConvert *converter;
   bool converterIsShared;

   // image format stuff
   string oldImageFormatName;
   CImageFormat *myImageFormat;

};

#endif

