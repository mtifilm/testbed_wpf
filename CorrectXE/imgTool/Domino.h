/*
	File:	Domino.h
	By:	Kevin Manbeck
	Date:	April 8, 2001

	The Domino class is used to convert between 625 and the Domino
	formats.

	There are three functions defined by the API:

		Init ()
		Merge ()
		Split ()

	Init()
		must be called before either Merge() or Split().
	Init() sets up the parameters need to manipulate the Domino format

	Merge()
		Each call merges one more 625 frame into the domino frame.
	It is necessary to call this function 4 times to create 1 D4 image.
	It is necessary to call this function 16 times to create 1 D16 image.

	Split()
		Still needs to be developed
*/

#ifndef DOMINO_H
#define DOMINO_H

#include <string>
#include <iostream>
#include <inttypes.h>

#include "machine.h"

using std::string;
using std::ostream;

/////////////////////////////////////////////////////////////////////////
// Forward Declarations


/////////////////////////////////////////////////////////////////////////

class CDomino
{
public:
  CDomino();
  ~CDomino();

  int Init (int iN, int iField625, int iFieldDomino, int iNRow, int iNCol);
  int Merge (MTI_UINT8 **ucp625, MTI_UINT8 **ucpDomino, int iFrame);

private:

  bool
    bInitialized;

  int
    iNRow625,		// size of the 625 frame
    iNCol625,
    iRowStart625,	// location of Domino data in 625 image
    iRowStop625,
    iNRowDomino,	// size of Domino frame
    iNColDomino,
    iRowStartDomino,	// location of Domino data in Domino image
    iRowStopDomino,
    iColStartDomino,
    iColStopDomino,
    iFieldPerFrame625,	// fields per frame in 625
    iFieldPerFrameDomino,// fields per frame in Domino
    iStepDomino,	// number of pixels to step during interleave
    iNum625Frame,	// number of 625 frames in one Domino frame
    iaRowInter[16],// interleave position of 625 frame in Domino frame
    iaColInterY[16],
    iaColInterU[16],
    iaColInterV[16];
    

};
#endif
