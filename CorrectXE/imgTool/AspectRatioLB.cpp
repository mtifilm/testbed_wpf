/*
	File:	AspectRatioLB.h
	By:	Kevin Manbeck
	Date:	Apr 9, 2004

	AspectRatioLB is used to convert anamorphic images into
	their letterboxed version.  The conversion must be very
	efficient so that it can be done in real time.
*/

#include "AspectRatioLB.h"
#include "AspectRatio.h"
#include "ImageFormat3.h"
#include "AspectRatio.h"
#include "IniFile.h"      // for TRACE_
#include "err_imgtool.h"

CAspectRatioLB::CAspectRatioLB()
{
  arp = 0;
}

CAspectRatioLB::~CAspectRatioLB()
{
  Free();
}

void CAspectRatioLB::Free()
{
  delete arp;
  arp = 0;
}

int CAspectRatioLB::Init (const CImageFormat *ifp, int iQuality)
{
  int iRet;
  float fZoom;
  int iSize;
  bool bHoriz;

  // free storage from a previous run
  Free ();

  // a center extract keeps the horizontal dimension constant and zooms
  // the vertical by 3/4
  fZoom = 3./4.;
  iSize = fZoom * (float)ifp->getLinesPerFrame();
  bHoriz = false;

  // allocate a CAspectRatio
  arp = new CAspectRatio;

  // initialize the CAspectRatio
  iRet = arp->Init (ifp, ifp);
  if (iRet)
   {
    Free ();
    return iRet;
   }

  // make the filter
  iRet = arp->MakeFilter (fZoom, iQuality, 10);
  if (iRet)
   {
    Free ();
    return iRet;
   }

  // make the spatial LUT
  iRet = arp->MakeSpatial (iSize, bHoriz);
  if (iRet)
   {
    Free ();
    return iRet;
   }

  // the active picture starts here
  arp->iRowStart = (ifp->getLinesPerFrame() - iSize) / 2;
  arp->iColStart = 0;

  // the active picture stops here
  arp->iRowStop = arp->iRowStart + iSize;
  arp->iColStop = ifp->getPixelsPerLine();

  // the pixel aspect ratio is a = (4 * image_height) / (3 * image_width).
  // When doing a matte, the new height = (image_width*a) / (matte_ratio)
  // or new height = (4/3)*image_height / matte_ratio.  Define fMatteFactor
  // to be (4/3)*image_height.

  arp->fRowMatteFactor = (4. * (float)ifp->getLinesPerFrame()) / 3.;

  // when matte_ratio < 1.778, we use the formula (9/16)*image_width

  arp->fColMatteFactor = (9.* (float)ifp->getPixelsPerLine()) / 16.;

  return 0;
}


int CAspectRatioLB::Render (MTI_UINT8 *ucpSrc[2], MTI_UINT8 *ucpDst[2])
{
  int iRet = 0;

  if (arp == 0)
   {
    return ERR_ASPECT_RATIO_INIT;
   }

  iRet = arp->Render (ucpSrc, ucpDst);
  if (iRet)
    return iRet;
 
  return iRet;
}

int CAspectRatioLB::getMatteCoordinates (int iJustify, float fMatteRatio,
	int &iRow0, int &iRow1, int &iCol0, int &iCol1)
{

  iRow0 = 0;
  iRow1 = 0;
  iCol0 = 0;
  iCol1 = 0;

  if (arp == 0)
   {
    return ERR_ASPECT_RATIO_INIT;
   }

  if (fMatteRatio >= 1.78)
   {
    int iNewRow = arp->fRowMatteFactor / fMatteRatio + 0.5;

    if (iJustify == -1)
     {
      // top justify
      iRow0 = arp->iRowStart;
     }
    else if (iJustify == 0)
     {
      // center
      iRow0 = arp->iRowStart + (arp->iRowStop - arp->iRowStart - iNewRow)/2;
     }
    else
     {
      // bottom align
      iRow0 = arp->iRowStop - iNewRow;
     }
    iRow1 = iRow0 + iNewRow;

    // prevent round-off errors in case of 1.78 letterbox
    if (fMatteRatio == 1.78)
     {
      iRow0 = arp->iRowStart;
      iRow1 = arp->iRowStop;
     }
   }
  else if (fMatteRatio > 0.)
   {
    int iNewCol = arp->fColMatteFactor * fMatteRatio + .5;

    // always perform a center justification for side mattes
    iCol0 = arp->iColStart + (arp->iColStop - arp->iColStart - iNewCol)/2;
    iCol1 = iCol0 + iNewCol;
   }
  else
   {
    // no mattes when ratio is zero
   }


  return 0;
}
