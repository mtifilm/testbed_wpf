#ifndef FFMTCVT 
#define FFMTCVT

#include "imgToolDLL.h"             // Export/Import Defs
#include "machine.h"
#include "mthread.h"

struct RTRIPLE
{
   MTI_UINT32 coeff;
   int srcfld;
   int srcrow;
};

struct ROWENTRY
{
   MTI_UINT32 coeff0;  // coeff (0-0x100)
   int srcfld0; // src fld
   int srcrow0; // row beg 

   MTI_UINT32 coeff1;  // coeff (0-0x100)
   int srcfld1; // src fld
   int srcrow1; // row beg 

   MTI_UINT32 coeff2;  // coeff (0-0x100)
   int srcfld2; // src fld
   int srcrow2; // row beg 

   MTI_UINT32 coeff3;  // coeff (0-0x100)
   int srcfld3; // src fld
   int srcrow3; // row beg 

   int dstfld;  // dst fld
   int dstrow;  // row beg 
};

struct CDOUBLE
{
   MTI_UINT32 coeff;
   int srcpr;
};

struct COLENTRY
{
   MTI_UINT32 coeff0;  // coeff (0-0x100)
   int srcpr0; // src pixel pair 

   MTI_UINT32 coeff1;  // coeff (0-0x100)
   int srcpr1; // src pixel pair 

   MTI_UINT32 coeff2;  // coeff (0-0x100)
   int srcpr2; // src pixel pair 

   MTI_UINT32 coeff3;  // coeff (0-0x100)
   int srcpr3; // src pixel pair
};

///////////////////////////////////////////////////////////////////////


class MTI_IMGTOOLDLL_API CFastFormatConvert
{

public:

#define MAXWDTH 2048
#define MAXHGHT 1536

   // wdth of src bitmap
   int wsrc;

   // hght of src bitmap
   int hsrc;

   // src bits per pixel
   int srcBitsPerPixel;

   // src interlaced flag
   bool srcInterlaced;

   // wdth of dst bitmap
   int wdst;

   // hght of dst bitmap
   int hdst;

   // dst bits per pixel
   int dstBitsPerPixel;

   // dst interlaced flag
   bool dstInterlaced;

   // throttle
   double throttle;

   // src window
   int srcMinRow;
   int srcMinCol;
   int srcMaxRow;
   int srcMaxCol;

   int srcWinHght;
   int srcWinWdth;

   // dst window
   int dstMinRow;
   int dstMinCol;
   int dstMaxRow;
   int dstMaxCol;

   int dstWinHght;
   int dstWinWdth;

   // number of horiz stripes
   int nStripes;

   // pitch of src line
   int srcPitch;

   // one line averaged from src
   MTI_UINT16 *avgLin[16];

   // pitch of dst line
   int dstPitch;

   // row combining table
   ROWENTRY *rowTbl;

   // col combining table
   COLENTRY *colTbl;

   // input  buffer 
   MTI_UINT8 **srcbuf;

   // output buffer
   MTI_UINT8 **dstbuf;

   // control struct for thread
   MTHREAD_STRUCT tsThread;

   // constructor
   CFastFormatConvert(
                 int wsrc,  // src wdth
                 int hsrc,  // src hght
                 int srcbp, // bits per pixel
                bool srcil, // interlaced flag
                 int wdst,  // dst wdth
                 int hdst,  // dst hght
                 int dstbp, // bits per pixel
                bool dstil  // interlaced flag
              );

   // destructor
   ~CFastFormatConvert();

   // set the throttle: 0.0 (full avg) -> 1.0 (nearest-neighbor)
   void setThrottle(double throtval);

   // set background color value
   void setBGNDColor(int u, int y0, int v, int y1);

   // set the source window
   void setSrcWindow(int minRow,int minCol,int maxRow,int maxCol);

   // set the destination window
   void setDstWindow(int minRow,int minCol,int maxRow,int maxCol);

   // set source AND destination windows
   void setWindows(
			int minRowSrc,
			int minColSrc,
                        int maxRowSrc,
                        int maxColSrc,

			int minRowDst,
                        int minColDst,
                        int maxRowDst,
                        int maxColDst
                   );

   // execute the conversion from src to dst buffers
   void convert(MTI_UINT8 **srcbuf, MTI_UINT8 **dstbuf);

private:

   // build the row averaging table 
   void buildRowTable();

   // build the col averaging table
   void buildColTable();

};

// prototype of test code
void TestFastFormatConvert();

#endif
