/*
	File:	AspectRatio.h
	By:	Kevin Manbeck
	Date:	Mar 17, 2005

	AspectRatio is used to generate sinc function LUTs for
	converting aspect ratios.

*/

#ifndef ASPECT_RATIO
#define ASPECT_RATIO

#include "imgToolDLL.h"
#include "machine.h"
#include "mthread.h"

#define INIT_NONE 0x0
#define INIT_FORMAT 0x1
#define INIT_FILTER 0x2
#define INIT_SPATIAL 0x4

#define INIT_ALL 0x7

#define PRECISION 10
#define VERTICAL_COMPONENTS 24

#define MAX_AR_JOB 4  // multithread job count


class CImageFormat;

class MTI_IMGTOOLDLL_API CAspectRatio
{
public:
  CAspectRatio();
  ~CAspectRatio();

  int Init (const CImageFormat *ifpSrc, const CImageFormat *ifpDst);

  
  int MakeFilter (float fZoomFactor, int iNNeighborArg, int iNSubPixelArg);
  // args:  fZoomFactor < 1 means shrink.  > 1 means expand
  //        iNNeighborArg:  1 to 5.  larger values result in better quality
  //        iNSubPixelArg:  10 is pretty good.  Bigger values use more RAM

  int MakeSpatial (int iSizeDst, bool bHorizontalArg);
  // args:  iSizeDst:  the resulting image dimension, before padding
  //        bHorizontalArg:  indicates whether conversion is horiz or vertical

  int Render (MTI_UINT8 *ucpSrc[2], MTI_UINT8 *ucpDst[2]);
  int DoRender (MTI_UINT8 *ucpSrc[2], MTI_UINT8 *ucpDst[2], int iJob);

  int getMatteCoordinates (int iJustify, float fMatteRatio,
		int &iRow0, int &iRow1, int &iCol0, int &iCol1);

  float
    fRowMatteFactor,
    fColMatteFactor;

  int
    iRowStart,
    iRowStop,
    iColStart,
    iColStop;

private:
  int
    iNRowSrc,
    iNColSrc,
    iPitchSrc,
    iNRowDst,
    iNColDst,
    iPitchDst;

  MTHREAD_STRUCT
    tsThread;

  int 
    iRenderReturn;

  MTI_UINT8
    *ucpRenderSrc[2],
    *ucpRenderDst[2];

  int
    iInit;


  bool
    bHorizontal;

  int
    iPixelPackingSrc,
    iColorSpaceSrc,
    iNFieldSrc,
    iPixelPackingDst,
    iColorSpaceDst,
    iNFieldDst;

  float
    *fpSincAlloc,
    *fpSinc,
    *fpWindowAlloc,
    *fpWindow,
    *fpFilterAlloc,
    *fpFilter;

  int
    iNNeighbor,
    iTotalNeighbor,
    iNSubPixel,
    iFilterWidth,
    iStartSrc,
    iStopSrc,
    iStartDst,
    iStopDst;

  float
    fZoomFactor;

  int
   ***ipLUTWei;		// LUT for sinc values
			// dimension:  [SubPixel][Neighbor][GreyLevel]

  int
    *ipLUTSubPixel,	// LUT for where entry into fpLUTWei
    *ipLUTPixel;	// LUT for where to start using ucpExtractedData

  MTI_UINT8
    **ucpUnpackedAlloc[MAX_AR_JOB],
    **ucpUnpacked[MAX_AR_JOB];      // unpacked, extracted data
			// dimension:  [component][Pixel]

  int
    iUnpackedPixelStart,
    iUnpackedPixelStop,
    iUnpackedComponent;

  int
    *ipSrcAlloc[3],
    *ipSrc[3];

  int
    iClampMin,
    iClampMax,
    iHalfPrecision;

  int RenderH_8_YUV_2_TO_8_YUV_2 (MTI_UINT8 *ucpSrc[2], MTI_UINT8 *ucpDst[2],
		int iRow0, int iRow1, int iJob);
  int RenderH_8_YUV_1_TO_8_YUV_1 (MTI_UINT8 *ucpSrc[2], MTI_UINT8 *ucpDst[2],
		int iRow0, int iRow1, int iJob);
  int RenderV_8_YUV_2_TO_8_YUV_2 (MTI_UINT8 *ucpSrc[2], MTI_UINT8 *ucpDst[2],
		int iCol0, int iCol1, int iJob);
  int RenderV_8_YUV_1_TO_8_YUV_1 (MTI_UINT8 *ucpSrc[2], MTI_UINT8 *ucpDst[2],
		int iCol0, int iCol1, int iJob);
  int RenderH_10_YUV_2_TO_8_YUV_2 (MTI_UINT8 *ucpSrc[2], MTI_UINT8 *ucpDst[2],
		int iRow0, int iRow1, int iJob);
  int RenderH_10_YUV_1_TO_8_YUV_1 (MTI_UINT8 *ucpSrc[2], MTI_UINT8 *ucpDst[2],
		int iRow0, int iRow1, int iJob);
  int RenderH_10a_YUV_2_TO_8_YUV_2 (MTI_UINT8 *ucpSrc[2], MTI_UINT8 *ucpDst[2],
		int iRow0, int iRow1, int iJob);
  int RenderH_10a_YUV_1_TO_8_YUV_1 (MTI_UINT8 *ucpSrc[2], MTI_UINT8 *ucpDst[2],
		int iRow0, int iRow1, int iJob);
  int RenderV_10_YUV_2_TO_8_YUV_2 (MTI_UINT8 *ucpSrc[2], MTI_UINT8 *ucpDst[2],
		int iCol0, int iCol1, int iJob);
  int RenderV_10_YUV_1_TO_8_YUV_1 (MTI_UINT8 *ucpSrc[2], MTI_UINT8 *ucpDst[2],
		int iCol0, int iCol1, int iJob);
  int RenderV_10a_YUV_2_TO_8_YUV_2 (MTI_UINT8 *ucpSrc[2], MTI_UINT8 *ucpDst[2],
		int iCol0, int iCol1, int iJob);
  int RenderV_10a_YUV_1_TO_8_YUV_1 (MTI_UINT8 *ucpSrc[2], MTI_UINT8 *ucpDst[2],
		int iCol0, int iCol1, int iJob);

  void Free();
  int AllocWeights();
  void FreeWeights();
  int AllocSpatial();
  void FreeSpatial();
  int SetSize(int iSizeDst);

  void GenerateSinc ();
  void GenerateWindow ();
  void GenerateFilter ();
  void GenerateLUT ();

  int SetUnpacked ();
  void UnpackH_8_YUV (MTI_UINT8 *ucpSrc, int iJob);
  void UnpackH_10_YUV (MTI_UINT8 *ucpSrc, int iJob);
  void UnpackH_10a_YUV (MTI_UINT8 *ucpSrc, int iJob);
  void UnpackV_8_YUV_2 (MTI_UINT8 *ucpSrc[2], int iJob);
  void UnpackV_8_YUV_1 (MTI_UINT8 *ucpSrc[2], int iJob);
  void UnpackV_10_YUV_2 (MTI_UINT8 *ucpSrc[2], int iJob);
  void UnpackV_10_YUV_1 (MTI_UINT8 *ucpSrc[2], int iJob);
  void UnpackV_10a_YUV_2 (MTI_UINT8 *ucpSrc[2], int iJob);
  void UnpackV_10a_YUV_1 (MTI_UINT8 *ucpSrc[2], int iJob);
  void RenderH_YUV (MTI_UINT8 *ucpDst, int iJob);
  void RenderV_YUV (MTI_UINT8 *ucpDst[2], int iJob);

  // HORIZONTAL rendering where neighborhood loop are unrolled
  void RenderH_YUV_1 (MTI_UINT8 *ucpDst, int &iCol, int iDstStopLocal, int iJob);
  void RenderH_YUV_3 (MTI_UINT8 *ucpDst, int &iCol, int iDstStopLocal, int iJob);
  void RenderH_YUV_5 (MTI_UINT8 *ucpDst, int &iCol, int iDstStopLocal, int iJob);
  void RenderH_YUV_7 (MTI_UINT8 *ucpDst, int &iCol, int iDstStopLocal, int iJob);
  void RenderH_YUV_9 (MTI_UINT8 *ucpDst, int &iCol, int iDstStopLocal, int iJob);
  void RenderH_YUV_11 (MTI_UINT8 *ucpDst, int &iCol, int iDstStopLocal, int iJob);
  void RenderH_YUV_13 (MTI_UINT8 *ucpDst, int &iCol, int iDstStopLocal, int iJob);
  void RenderH_YUV_15 (MTI_UINT8 *ucpDst, int &iCol, int iDstStopLocal, int iJob);
  void RenderH_YUV (MTI_UINT8 *ucpDst, int &iCol, int iDstStopLocal, int iJob);

  // VERTICAL rendering where neighborhood loop are unrolled
  void RenderV_YUV_1 (MTI_UINT8 *ucpDst[2], int &iRow, int iDstStopLocal, int iJob);
  void RenderV_YUV_3 (MTI_UINT8 *ucpDst[2], int &iRow, int iDstStopLocal, int iJob);
  void RenderV_YUV_5 (MTI_UINT8 *ucpDst[2], int &iRow, int iDstStopLocal, int iJob);
  void RenderV_YUV_7 (MTI_UINT8 *ucpDst[2], int &iRow, int iDstStopLocal, int iJob);
  void RenderV_YUV_9 (MTI_UINT8 *ucpDst[2], int &iRow, int iDstStopLocal, int iJob);
  void RenderV_YUV_11 (MTI_UINT8 *ucpDst[2], int &iRow, int iDstStopLocal, int iJob);
  void RenderV_YUV_13 (MTI_UINT8 *ucpDst[2], int &iRow, int iDstStopLocal, int iJob);
  void RenderV_YUV_15 (MTI_UINT8 *ucpDst[2], int &iRow, int iDstStopLocal, int iJob);
  void RenderV_YUV (MTI_UINT8 *ucpDst[2], int &iRow, int iDstStopLocal, int iJob);

  friend void CallDoRender (void *vp, int iJob);

};

#endif


