
/*
	Here is a description of the public functions in CBrush:

	setRadius - sets the size of the brush.  Value is in pixels.

	setAspect - sets the eccentricity of the brush.  The value 1 is
		a circle

	setAngle - rotates the brush.  Angle is measured in degrees.

	setBlend - Each brush is a gaussian bell-shaped curve.  The blend
		value controls the shape of the curve.

	setOpacity - Controls the amount of reference image versus
		current image.  The range is 0. to 1.

	setType - see EBrushType for the allowed values

	getAspect, getAngle, getBlend, getOpacity, getType - are used to
		determine the parameter values of the brush

	getMinRow, getMinCol, getMaxRow, getMaxCol  - are used to determine
		the support of the brush.  Note:  (MinRow, MinCol) are
		inclusive.  (MaxRow, MaxCol) are exclusive.

	getWeight - is used to determine the weight of the reference frame
		at any pixel.
*/

#ifndef BrushAlgoH
#define BrushAlgoH

#include "machine.h"
#include "imgToolDLL.h"
#include "mthread.h"

enum EBrushType
{
  BT_SOLID,
  BT_SMOOTH,
  BT_SPRAY
};

class MTI_IMGTOOLDLL_API CBrush
{
public:
  CBrush (int nStripes = 0);
  CBrush(const CBrush& src);
  ~CBrush ();

  void setMaxRadius(int newMaxRadius);
  void setRadius  (int  newRadius);
  void setBlend   (int  newBlend);
  void setOpacity (int  newOpacity);
  void setAspect  (int  newAspect);
  void setAngle   (int  newAngle);
  void setShape   (bool isSquare);
  void setType (EBrushType newType);

  void setBrushParameters(int  newMaxRadius,
                          int  newRadius,
                          int  newBlend,
                          int  newOpacity,
                          int  newAspect,
                          int  newAngle,
                          bool newShape,
                          EBrushType newType=BT_SMOOTH);

  void setBrushParametersFromBrush(CBrush *srcbrsh);

  int  getMaxRadius();
  int  getRadius ();
  int  getBlend ();
  int  getOpacity ();
  int  getAspect ();
  int  getAngle ();
  bool getShape();
  EBrushType getType ();

  int getMinRow ();	// inclusive
  int getMinCol ();	// inclusive
  int getMaxRow ();	// exclusive
  int getMaxCol ();	// exclusive

  MTI_UINT16 getWeight (int iRow, int iCol);

  MTI_UINT16 getWeightFast (int iRow, int iCol);

private:

  void initGaussianTable();

  static void computeStripeWeight(void *v, int iJob);

  void xformCorner(float x, float y,
                   float fcosang, float fsinang,
                   float *xfmx, float *xfmy);

  void Allocate();

  void Reallocate();

  void ComputeWeight();

private:

  static int instanceCount;

#define EXPDIVS 8192
////#define EXPSHFT   13
#define UNITY  32768
////#define UNITSHFT  15
  static MTI_UINT16 *expTbl;

  bool reAllocate;
  bool reCompute;

  MTI_UINT16
    *ipWeightStorage,
    **ipWeight;

  int
    iFullMinRow,
    iFullMaxRow,
    iFullMinCol,
    iFullMaxCol;

  int
    iMinRow,
    iMaxRow,
    iMinCol,
    iMaxCol;

  bool
    bSquare;

  float
    fLrgThr;

  int iShift;

  int
    iMaxBrushSize;

  int
    iLastMaxRadius,
    iLastRadius,
    iLastAspect,
    iLastBlend,
    iLastAngle,
    iLastOpacity;

  float
    fRadius,
    fAspect,
    fBlend,
    fAngle,
    fOpacity;

  float
    fCosAngle,
    fSinAngle;

  EBrushType
    iType;

  ///////////////////////////////////////////////////////////////

  int nStripes; // number of stripes in frame (1,2,4,8, or 16)

  MTHREAD_STRUCT tsThread;

  ///////////////////////////////////////////////////////////////
};

#endif
