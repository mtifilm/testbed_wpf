/*
	Name:	AudioConvertAPI.h
	By:	Mike Braca
	Date:	Feb 11, 2002

	This program converts one audio file to another and changes
	the sample rate if necessary.
*/

#include "imgToolDLL.h"


class MTI_IMGTOOLDLL_API AudioConvertAPI
{
  string  strSrcName;
  string  strDstName;
  int     iFrameSrc;
  int     iFrameDst;
  int     iNFrameSrc;
  int     iNFrameDst;

  bool    bSrcPulldown;
  bool    bDstPulldown;

public:

  void    setSource(const string &strName,
                    int iInFrame=0, int iOutFrame=-1,
                    bool bHasPulldown=false)
  {
     strSrcName = strName;
     iFrameSrc = iInFrame;
     iNFrameSrc = iOutFrame;
     bSrcPulldown = bHasPulldown;
  };

  void    setDest(const string &strName,
                   int iInFrame=0, int iOutFrame=-1,
                    bool bHasPulldown=false)
  {
     strDstName = strName;
     iFrameDst = iInFrame;
     iNFrameDst = iOutFrame;
     bDstPulldown = bHasPulldown;
  };

  int     Convert();

};
