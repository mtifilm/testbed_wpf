#ifndef CVERTEX
#define CVERTEX

// a simple floating-point vertex

struct VERTEX
{
   double x;
   double y;
};

#endif

