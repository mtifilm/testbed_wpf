#ifndef RepairH
#define RepairH

/*
	File:	Repair.h
	By:	Kevin Manbeck
	Date:	June 29, 2001

	The Repair class is a base class used by various image processing and
	repair tools.

	The application programmer will not need to deal with the repair
	class directly.  Instead, the application programmer should 
	use RepairDRS, RepairVS, and RepairAF classes.

	The following functions are defined here:

	***********************************
		MANDITORY FUNCTIONS
	***********************************

	-----------------------------------------------------
	setImageDimension (long RegionULCRow, long RegionULCCol,
			long RegionLRCRow, long RegionLRCCol,
			long nImageRows, long Pitch);
	-----------------------------------------------------
			
	This function sets the size and pitch of the image that 
	will be processed.  Images are assumed to be composed of
	16 bit values.

	The point (RegionULCRow, RegionULCCol) represents the Upper
	Left Corner of the region of interest.  The point
	(RegionLRCRow, RegionLRCCol) is the Lower Right Corner
	of the region of interest.

	ULC in INCLUSIVE.  LRC is EXCLUSIVE.  That is the pixel along
	the right and bottom boundaries will not be processed.

        ------------------------------------------------------------
	setImageComponent (long NCom, long PrincipalCom,
		long *pMinValue, long *pMaxValue, long *pFillValue);
        ------------------------------------------------------------
		
	This function sets the number of components in the image.
	The principal component is the one used for motion estimation
	and any other analysis.  For YUV images, Y should be the 
	principal.  For RGB images, G should be the principal.

	Each component should have a minimum and maximum value.  The
	FillValue is usually set to the component values of BLACK.

        Note: this method is overloaded to handle either long or MTI_UINT16 data

	-----------------------
	setNFrame (long NFrame);
	-----------------------

	Several frames might be passed into the library for analysis.
	This function sets the number of frames used.

	---------------------------
	setRepairFrame (long Frame);
	---------------------------

	Only one frame will actually be processed.  Frame indicates
	which frame will be processed.

	----------------------------------------------------------------
	setDataPointer (long Frame, MTI_UINT16 *pData, long TemporalIndex);
	----------------------------------------------------------------

	This function must be called once for each of NFrame.  pData
	is a pointer to a frame of 16bit values.

	TemporalIndex indicates the relative temporal relationship between
	the NFrames of data.  For example if Frame=2 is 20 frames 
	ahead of Frame=1, their temporal indices should have a difference
	of 20.

	----------------------------------
	setResultPointer (MTI_UINT16 *pData);
	----------------------------------

	pData is a pointer to the region where the results are recorded.
	This pointer can be the same as one of the pointers supplied in
	setDataPointer(), or it can be different.  If it is the same, the
	repair will be saved in place.  If it is different, the repair will
	be saved in a different place.

	----------------------------------------------------------------
	setDataTypeIsReallyHalfFloat (bool flag);
	----------------------------------------------------------------

	If the argument is true, indicates the data really consists of halfs,
	not unsigned shorts.

	--------------------------------------------------------
	setPixelRegionList (CPixelRegionList *pPixelRegionList);
	--------------------------------------------------------

	pPixelRegionList is used to store the original pixel values for
	later use by the history.

	***************************************
		OPTIONAL FUNCTIONS
	***************************************

	------------------------------------
	setSuspendComponent (long Com)
	------------------------------------

	Sometimes we do not wish to process all components.  For example,
	in the case of alpha channel material, do not process the alpha
	channel.  In the case of flicker control, we may wish to suspend
	processing of one or more color components.

	Each time setImageComponent () is called, all suspensions are
	cancelled and all components are flagged for processing.

	-------------------------------
	setNProcessor (long NProcessor)
	-------------------------------

	The default is to use one processor.  This value can be increased
	to achieve multithreading.

	-------------------------------
	setDebugFlag (bool DebugFlag)
	-------------------------------

	This function is used to turn debug ON or OFF.

	-------------------------------
	setCommandFileFlag (bool CommandFileFlag)
	-------------------------------

	This function is used to turn command file flag ON or OFF.
	When the command file flag is true, the inputs to the library
	are saved to a file.  These files can be processed by a
	demo program.


	------------------------------
	setFieldRepeatFlag (bool bArg)
	------------------------------

	Calling this function with bArg=true will produce output images 
	with even and odd lines being identical.  This function was
	implemented to handle the 525 interlaced material at the Post Group.
	setFieldRepeatFlag(true) was needed since a single field is doubled
	to create a pair of fields.  The Repair algorithms need to know to
	perform the same fix on both fields.

*/

#include <string>
#include <iostream>

#include "machine.h"
#include "imgToolDLL.h"             // Export/Import Defs
#include "err_repair.h"
#include "PixelRegions.h"
#include "ImageInfo.h"
#include "RegionOfInterest.h"
#define LABEL_MODIFIED  LABEL_FIRST_UNUSED  // based on RegionOfInterest values

using std::string;
using std::ostream;

/////////////////////////////////////////////////////////////////////////
// Forward Declarations


/////////////////////////////////////////////////////////////////////////

#define MAX_COMPONENT 5
#define MAX_FRAME 5            // used by DRS and AF
#define MAX_INPUT_FRAME 100    // provide more flexibility for other tools

	// values of RepairType
#define RT_NONE 0x0

	// used by CRepairDRS
#define RT_ORDINARY           0x0100
#define RT_HARD_MOTION        0x0200
#define RT_ADJUST_DENSITY     0x0400
#define RT_HARD_MOTION_2      0x0800
#define RT_COLOR_BUMP         0x1000
#define RT_REPLACE            0x2000
#define RT_REPLACE_FULL_FRAME 0x4000

	// used by CRepairVS
#define RT_SCRATCH_FIND       0x010000
#define RT_SCRATCH_REPAIR     0x020000
#define RT_VERTICAL_SCRATCH   0x040000
#define RT_HORIZONTAL_SCRATCH 0x080000
#define RT_EMULSION_SCRATCH   0x100000
#define RT_OCCLUSION_SCRATCH  0x200000

class MTI_IMGTOOLDLL_API CRepair
{
public:
  CRepair();
  ~CRepair();

	// mandatory functions:  *MUST* be called by application
  int setImageDimension (long RegionULCRow, long RegionULCCol,
		long RegionLRCRow, long RegionLRCCol, long nImageRows, long Pitch);
  int setPixelComponents (EPixelComponents newPixelComponents);
  int setImageComponent (long NCom, long PrincipalCom, long *pMinValue,
                         long *pMaxValue, long *pFillValue);
  int setImageComponent (long NCom, long PrincipalCom, MTI_UINT16 *pMinValue,
                         MTI_UINT16 *pMaxValue, MTI_UINT16 *pFillValue);
  int setNFrame (long NFrame);
  int setRepairFrame (long Frame);
  int setDataPointer (long Frame, MTI_UINT16 *pData, long TemporalIndex);
  int setResultPointer (MTI_UINT16 *pData);
  void setDataTypeIsReallyHalfFloat (bool flag);
  void setPixelRegionList (CPixelRegionList *newPixelRegionList,
                           long *newGlobalSerial=NULL, long newLocalSerial=-1);

	// optional functions:  *MAY* be called by application
  int setSuspendComponent (long Com);
  int setNProcessor (long NProcessor);
  void setDebugFlag (bool DebugFlag);
  void setCommandFileFlag (bool CommandFileFlag);

  void setFieldRepeatFlag (bool bArg);

  int setSelectRegion (const RECT &newRect);
  int setSelectRegion (const POINT *newLasso);
  int setSelectRegion (const BEZIER_POINT *newBezier);
  void setBorderValues (int newBlendSize, EBlendType newBlendType,
		int newSurroundSize);

  void setRegionOfInterestPtr (const MTI_UINT8 *ucpBlend, MTI_UINT8 *ucpLabel);

protected:
  long
    lNProcessor,	// number of processors to use in multithreaded code
    lRegionULCRow,	// position of upper left corner
    lRegionULCCol,	// position of upper left corner
    lRegionLRCRow,	// position of lower right corner
    lRegionLRCCol,	// position of lower right corner
    lNRow,		// number of rows in (sub-)image
    lNCol,		// number of cols in (sub-)image
    lNImageRows, // number of rows in the full image
    lPitch,		// horizontal pitch of image
    lNCom,		// number of components
    lPrincipalCom,	// most important component (Y in YUV, G in RGB)
    lNFrame,		// number of frames
    laTemporalIndex[MAX_INPUT_FRAME],	// temporal index of each frame
    lRepairFrame,	// frame to repair
    laMinValue[MAX_COMPONENT],	// minimum allowed value in an image
    laMaxValue[MAX_COMPONENT],	// maximum allowed value in an image
    laFillValue[MAX_COMPONENT]; // value to use when initializing pixels

  bool
    bDataTypeIsReallyHalfFloat = false,
    bCommandFileFlag,		// save input to command file?
    bDebugFlag,			// run in debug mode?
    baProcessCom[MAX_COMPONENT],	// should this component be processed
    bFieldRepeatFlag;		// should we force fields to repeat?

  EPixelComponents
    iPixelComponents;

  MTI_UINT16
    *uspData[MAX_INPUT_FRAME],	// data is owned by the application
    *uspResult;			// data is owned by the application

  long
    lDefaultResultSize;

  MTI_UINT16
    *uspDefaultResult;		// internal copies of result image. 
				// Memory onwed by CRepair.

  CPixelRegionList
    *pPixelRegionList;

  long
    *lpGlobalPixelRegionListSerial,  // control access to pixelRegionList
    lLocalPixelRegionListSerial;     // these must match if we have access

  CRegionOfInterest
    roi;		// holds the user's selected region

  RECT
    rect;

  POINT
    *lasso;

  BEZIER_POINT
    *bezier;

  int
    iLassoAlloc,
    iBezierAlloc;

  bool
    bUseRECT,
    bUseLASSO,
    bUseBEZIER;

  int
    iBlendSize,
    iSurroundSize,
    iRenderBlendSize,
    iRenderSurroundSize;

  EBlendType
    iBlendType,
    iRenderBlendType;

  MTI_UINT8 
    *ucpROILabel;

  const MTI_UINT8
    *ucpROIBlend;

  static CPixelRegionList
    *NULL_pPixelRegionList;

  void PerformFieldRepeat();

  int AllocDefaultStorage ();

  int renderMask ();
};
#endif // REPAIR_H
