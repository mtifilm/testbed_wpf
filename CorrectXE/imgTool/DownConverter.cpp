//
// implementation file for class CDownConverter
//
// With 9 taps on a 2.66 gHz CPU, converts HD to SD in 31 mS or less
//
#include "DownConverter.h"
#include "ImageFormat3.h"
#include "ImageInfo.h"
#include "MTImalloc.h"
#include <iostream>
#include <stdio.h>
#include <math.h>

// define this for debugging without multithreading
//#define NO_MULTI

// define this to see that two threads makes no diff
//#define TWO_THREADS

////////////////////////////////////////////////////////////////////////////////
//
// BEATS can be minimized in the conversions by making the source and desti-
// nation dimensions have a small greatest-common-divisor. When this is done,
// the repeat cycle for the kernel-table is as long as possible, minimizing
// interaction between high-frequency spatial patterns. Thus, when doing an
// anamorphic conversion, use a source rectangle of (2, 1, 1917, 1078) instead
// of (0, 0, 1919, 1079).
//
// The arrangement of memory for the successive stages is as follows:
//
//
//              _________1920________
//             |UYVY etc             |
//             |UYVY etc             |
//             |                     |
//        1080 |---------------------|   SRCBUF (interlaced)
//             |                     |
//             |                     |
//             |_____________________|
//
//                        |
//                  UNPACK TO YUV8
//                        |
//                        V
//
//              _________1920________
//             |UYVY etc             |
//             |UYVY etc             |
//             |                     |
//        1080 |                     |   FRMBUF1 (non-interlaced)
//             |                     |
//             |                     |
//             |_____________________|
//
//                        |
//             HORZ CONVERT, TRANSPOSE
//                        |
//                        V
//
//                _______1080_______
//               |UUUUUU etc        |
//               |YYYYYY etc        |
//           720 |VVVVVV etc        |    FRMBUF2 (non-interlaced)
//               |YYYYYY etc        |
//               |__________________|
//
//                        |
//             VERT CONVERT, TRANSPOSE
//                        |
//                        V
//
//                   ____720_____
//                  |UYVY etc    |
//                  |UYVY etc    |       DSTBUF (interlaced)
//              486 |------------|
//                  |            |
//                  |____________|
//
//
//

static void null(void *v, int iJob);
static void unpackYUV_8_K1(void *v, int iJob);
static void unpackYUV_8_K2(void *v, int iJob);
static void unpackYUV_8_K3(void *v, int iJob);
static void unpackYUV_8_K4(void *v, int iJob);
static void unpackYUV_8_K5(void *v, int iJob);
static void unpackYUV_8_K6(void *v, int iJob);
static void unpackYUV_8_K7(void *v, int iJob);
static void unpackYUV_10_K1(void *v, int iJob);
static void unpackYUV_10_K2(void *v, int iJob);
static void unpackYUV_10_K3(void *v, int iJob);
static void unpackYUV_10_K4(void *v, int iJob);
static void unpackYUV_10_K5(void *v, int iJob);
static void unpackYUV_10_K6(void *v, int iJob);
static void unpackYUV_10_K7(void *v, int iJob);
static void unpackDVS_10_K1(void *v, int iJob);
static void unpackDVS_10_K2(void *v, int iJob);
static void unpackDVS_10_K3(void *v, int iJob);
static void unpackDVS_10_K4(void *v, int iJob);
static void unpackDVS_10_K5(void *v, int iJob);
static void unpackDVS_10_K6(void *v, int iJob);
static void unpackDVS_10_K7(void *v, int iJob);
static void horizontalDownconvert_K1(void *v, int iJob);
static void horizontalDownconvert_K2(void *v, int iJob);
static void horizontalDownconvert_K3(void *v, int iJob);
static void horizontalDownconvert_K4(void *v, int iJob);
static void horizontalDownconvert_K5(void *v, int iJob);
static void horizontalDownconvert_K6(void *v, int iJob);
static void horizontalDownconvert_K7(void *v, int iJob);
static void verticalDownconvert_K1(void *v, int iJob);
static void verticalDownconvert_K2(void *v, int iJob);
static void verticalDownconvert_K3(void *v, int iJob);
static void verticalDownconvert_K4(void *v, int iJob);
static void verticalDownconvert_K5(void *v, int iJob);
static void verticalDownconvert_K6(void *v, int iJob);
static void verticalDownconvert_K7(void *v, int iJob);

////////////////////////////////////////////////////////////////////////////////
//
//    C O N S T R U C T O R
//
CDownConverter::
CDownConverter(int numStripes)
: nStripes(numStripes)
{
#ifdef NO_MULTI
   nStripes = 2;
#else
   nStripes = numStripes;
#endif

   frmBuf1 = NULL;

   frmBuf2 = NULL;

   cntrVTbl = NULL;

   vrtTbl = NULL;

   vkern = NULL;

   cntry1Tbl = NULL;

   cntry0Tbl = NULL;

   cntruvTbl = NULL;

   hy1Tbl = NULL;

   hy0Tbl = NULL;

   huvTbl = NULL;

   hkern = NULL;

   dstbuf[0] = NULL;
   dstbuf[1] = NULL;

   // step 1 is UNPACK
   unpackThread.PrimaryFunction   = (void (*)(void *,int))&null;
   unpackThread.SecondaryFunction = NULL;
   unpackThread.CallBackFunction  = NULL;
   unpackThread.vpApplicationData = this;
   unpackThread.iNThread          = nStripes;
   unpackThread.iNJob             = nStripes;

   // step 2 is HORZ DOWNCONVERT
   horzThread.PrimaryFunction   = (void (*)(void *,int))&null;
   horzThread.SecondaryFunction = NULL;
   horzThread.CallBackFunction  = NULL;
   horzThread.vpApplicationData = this;
   horzThread.iNThread          = nStripes;
   horzThread.iNJob             = nStripes;

   // step 3 is VERT DOWNCONVERT
   vertThread.PrimaryFunction   = (void (*)(void *,int))&null;
   vertThread.SecondaryFunction = NULL;
   vertThread.CallBackFunction  = NULL;
   vertThread.vpApplicationData = this;
   vertThread.iNThread          = nStripes;
   vertThread.iNJob             = nStripes;

   // now allocate the mthreads
   if (nStripes > 1) {

#ifndef NO_MULTI
      int iRet;

      /*iRet = */ MThreadAlloc(&unpackThread);
      //if (iRet) {
      //   TRACE_0(errout << "Convert: MThreadAlloc failed, iRet=" << iRet);
      //}
#ifndef TWO_THREADS
      /* iRet = */ MThreadAlloc(&horzThread);
      //if (iRet) {
      //   TRACE_0(errout << "Convert: MThreadAlloc failed, iRet=" << iRet);
      //}
#endif
      /* iRet = */ MThreadAlloc(&vertThread);
      //if (iRet) {
      //   TRACE_0(errout << "Convert: MThreadAlloc failed, iRet=" << iRet);
      //}
#endif
   }
}

////////////////////////////////////////////////////////////////////////////////
//
//    D E S T R U C T O R
//
CDownConverter::
~CDownConverter()
{
#ifndef NO_MULTI
   MThreadFree(&vertThread);
#ifndef TWO_THREADS
   MThreadFree(&horzThread);
#endif
   MThreadFree(&unpackThread);
#endif

   delete [] cntrVTbl;

   delete [] vrtTbl;

   delete [] vkern;

   delete [] cntry1Tbl;

   delete [] cntry0Tbl;

   delete [] cntruvTbl;

   delete [] hy1Tbl;

   delete [] hy0Tbl;

   delete [] huvTbl;

   delete [] hkern;

   MTIfree(frmBuf2);

   MTIfree(frmBuf1);
}

////////////////////////////////////////////////////////////////////////////////
//
//                          i n i t i a l i z e
//
// Source and destination rectangles are INCLUSIVE.
// srcRect.left always EVEN, srcRect.right always ODD
// dstRect.left always EVEN, dstRect.right always ODD
//
int CDownConverter::
initialize(const CImageFormat *srcFmt, RECT *srcRect, RECT *dstRect,
                                        int horzkern, int vertkern)
{
   if ((horzkern < 1)||(horzkern > 7)||(vertkern < 1)||(vertkern > 7))
      return(DOWNCONVERT_ERROR_INVALID_KERNEL_SIZE);

   // extract the following from the src image-format
   frmwdth          = srcFmt->getPixelsPerLine();
   frmhght          = srcFmt->getLinesPerFrame();
   frmpitch         = srcFmt->getFramePitch();
   frmcolorspace    = srcFmt->getColorSpace();
   frmpelcomponents = srcFmt->getPixelComponents();
   frmpelpacking    = srcFmt->getPixelPacking();
   frmil            = srcFmt->getInterlaced();

   // select the unpack used
   switch(frmpelcomponents) {

      case IF_PIXEL_COMPONENTS_YUV422:

         switch(frmpelpacking) {

            case IF_PIXEL_PACKING_8Bits_IN_1Byte:

               xoffs = 2 * srcrect.left;
               phase = 0;
               switch(horzkern) {
                  case 1:
                     unpackThread.PrimaryFunction = (void (*)(void *,int))&unpackYUV_8_K1;
                  break;
                  case 2:
                     unpackThread.PrimaryFunction = (void (*)(void *,int))&unpackYUV_8_K2;
                  break;
                  case 3:
                     unpackThread.PrimaryFunction = (void (*)(void *,int))&unpackYUV_8_K3;
                  break;
                  case 4:
                     unpackThread.PrimaryFunction = (void (*)(void *,int))&unpackYUV_8_K4;
                  break;
                  case 5:
                     unpackThread.PrimaryFunction = (void (*)(void *,int))&unpackYUV_8_K5;
                  break;
                  case 6:
                     unpackThread.PrimaryFunction = (void (*)(void *,int))&unpackYUV_8_K6;
                  break;
                  case 7:
                     unpackThread.PrimaryFunction = (void (*)(void *,int))&unpackYUV_8_K7;
                  break;
               }
               break;

            case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:

               xoffs = 5 * (srcrect.left>>1);
               phase = 0;
               switch(horzkern) {
                  case 1:
                     unpackThread.PrimaryFunction = (void (*)(void *,int))&unpackYUV_10_K1;
                  break;
                  case 2:
                     unpackThread.PrimaryFunction = (void (*)(void *,int))&unpackYUV_10_K2;
                  break;
                  case 3:
                     unpackThread.PrimaryFunction = (void (*)(void *,int))&unpackYUV_10_K3;
                  break;
                  case 4:
                     unpackThread.PrimaryFunction = (void (*)(void *,int))&unpackYUV_10_K4;
                  break;
                  case 5:
                     unpackThread.PrimaryFunction = (void (*)(void *,int))&unpackYUV_10_K5;
                  break;
                  case 6:
                     unpackThread.PrimaryFunction = (void (*)(void *,int))&unpackYUV_10_K6;
                  break;
                  case 7:
                     unpackThread.PrimaryFunction = (void (*)(void *,int))&unpackYUV_10_K7;
                  break;
               }
               break;

            case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:

               xoffs = (srcrect.left/6)<<4;
               phase = (srcrect.left>>1)%3;
               switch(horzkern) {
                  case 1:
                     unpackThread.PrimaryFunction = (void (*)(void *,int))&unpackDVS_10_K1;
                  break;
                  case 2:
                     unpackThread.PrimaryFunction = (void (*)(void *,int))&unpackDVS_10_K2;
                  break;
                  case 3:
                     unpackThread.PrimaryFunction = (void (*)(void *,int))&unpackDVS_10_K3;
                  break;
                  case 4:
                     unpackThread.PrimaryFunction = (void (*)(void *,int))&unpackDVS_10_K4;
                  break;
                  case 5:
                     unpackThread.PrimaryFunction = (void (*)(void *,int))&unpackDVS_10_K5;
                  break;
                  case 6:
                     unpackThread.PrimaryFunction = (void (*)(void *,int))&unpackDVS_10_K6;
                  break;
                  case 7:
                     unpackThread.PrimaryFunction = (void (*)(void *,int))&unpackDVS_10_K7;
                  break;
               }
               break;

            default:
               return(DOWNCONVERT_ERROR_INVALID_FORMAT);
         }

      break;

      default:
         return(DOWNCONVERT_ERROR_INVALID_FORMAT);
   }

   // check the source rectangle
   if (!((0 <= srcRect->left)&&
        ((srcRect->left&1) == 0)&&
        (srcRect->left < srcRect->right)&&
        ((srcRect->right&1) == 1)&&
        (srcRect->right < frmwdth)&&
        (0 <= srcRect->top)&&
        (srcRect->top < srcRect->bottom)&&
        (srcRect->bottom < frmhght)))
      return(DOWNCONVERT_ERROR_INVALID_SRC_RECTANGLE);

   // source rectangle is OK
   srcrect          = *srcRect;
   srcwdth          = srcRect->right - srcRect->left + 1;
   srchght          = srcRect->bottom - srcRect->top + 1;

   // check the destination rectangle
   if (!((0 <= dstRect->left)&&
        ((dstRect->left&1) == 0)&&
        (dstRect->left < dstRect->right)&&
        ((dstRect->right&1) == 1)&&
        (dstRect->right < 720)&&
        (0 <= dstRect->top)&&
        (dstRect->top < dstRect->bottom)&&
        (dstRect->bottom < 486)))
      return(DOWNCONVERT_ERROR_INVALID_DST_RECTANGLE);

   // destination rectangle is OK
   dstrect          = *dstRect;
   dstwdth          = dstRect->right - dstRect->left + 1;
   dsthght          = dstRect->bottom - dstRect->top + 1;

   // the horizontal downconverter
   switch(horzkern) {
      case 1:
         horzThread.PrimaryFunction   = (void (*)(void *,int))&horizontalDownconvert_K1;
      break;
      case 2:
         horzThread.PrimaryFunction   = (void (*)(void *,int))&horizontalDownconvert_K2;
      break;
      case 3:
         horzThread.PrimaryFunction   = (void (*)(void *,int))&horizontalDownconvert_K3;
      break;
      case 4:
         horzThread.PrimaryFunction   = (void (*)(void *,int))&horizontalDownconvert_K4;
      break;
      case 5:
         horzThread.PrimaryFunction   = (void (*)(void *,int))&horizontalDownconvert_K5;
      break;
      case 6:
         horzThread.PrimaryFunction   = (void (*)(void *,int))&horizontalDownconvert_K6;
      break;
      case 7:
         horzThread.PrimaryFunction   = (void (*)(void *,int))&horizontalDownconvert_K7;
      break;
   }

   // the vertical downconverter
   switch(vertkern) {
      case 1:
         vertThread.PrimaryFunction   = (void (*)(void *,int))&verticalDownconvert_K1;
      break;
      case 2:
         vertThread.PrimaryFunction   = (void (*)(void *,int))&verticalDownconvert_K2;
      break;
      case 3:
         vertThread.PrimaryFunction   = (void (*)(void *,int))&verticalDownconvert_K3;
      break;
      case 4:
         vertThread.PrimaryFunction   = (void (*)(void *,int))&verticalDownconvert_K4;
      break;
      case 5:
         vertThread.PrimaryFunction   = (void (*)(void *,int))&verticalDownconvert_K5;
      break;
      case 6:
         vertThread.PrimaryFunction   = (void (*)(void *,int))&verticalDownconvert_K6;
      break;
      case 7:
         vertThread.PrimaryFunction   = (void (*)(void *,int))&verticalDownconvert_K7;
      break;
   }

   // allocate a frame buffer for the unpacked native frame
   // with room for margins at both ends of the rows
   pitch1  = 4*horzkern + srcwdth*2 + 4*horzkern;
   MTIfree(frmBuf1);
   frmBuf1 = (unsigned char *)MTImalloc(pitch1*srchght);
   if (frmBuf1 == 0)
      return(DOWNCONVERT_ERROR_INSUFFICIENT_MEMORY);

   // allocate a frame buffer to receive the horizontal conversion
   // this will be transposed, so that the data is UYVY columnwise
   pitch2 = vertkern + srchght + vertkern;
   MTIfree(frmBuf2);
   frmBuf2 = (unsigned char *)MTImalloc(pitch1*(dstwdth*2)); // TTT
   if (frmBuf2 == 0)
      return(DOWNCONVERT_ERROR_INSUFFICIENT_MEMORY);

   // the vertical conversion goes right into the destination frmbuf

   /////////////////////////////////////////////////////////////////////////////
   // the horizontal coefficients
   hmod = dstwdth/gcd(srcwdth, dstwdth);
   double cvtfactor = (double)srcwdth/(double)dstwdth;

   delete[] hkern;
   hkern = new KCoeff[hmod];
   double hcoeff[MAX_WEIGHTS];
   for (int i=0;i<hmod;i++) {

      // compute the weights
      double cntr0 = (i + 0.5)*cvtfactor;
      double cntr1 = (double)((int)cntr0);

      // arg is delta of pixel center from
      // scaled position of dst pixel center
      double arg = (cntr1 + 0.5) - cntr0;

      for (int j=0;j<=horzkern;j++) {
         hcoeff[MIDPT-j] = Kernel(arg-j, srcwdth, dstwdth, horzkern);
         hcoeff[MIDPT+j] = Kernel(arg+j, srcwdth, dstwdth, horzkern);
      }

      // normalize
      double negsum = 0.0;
      double possum = 0.0;
      for (int j=-horzkern;j<=horzkern;j++) {
         if (hcoeff[MIDPT+j] < 0.0)
            negsum -= hcoeff[MIDPT+j];
         else
            possum += hcoeff[MIDPT+j];
      }
      double sum = possum - negsum;
      for (int j=-horzkern;j<=horzkern;j++) {
         hcoeff[MIDPT+j] /= sum;
      }

      // convert to signed integer
      for (int j=-horzkern;j<=horzkern;j++) {
         hkern[i].wgt[MIDPT+j] = (int)(WGTONE* hcoeff[MIDPT+j]);
      }

      // sharpen up the normalization
      int tstsum = 0;
      for (int j=-horzkern;j<=horzkern;j++) {
         tstsum += hkern[i].wgt[MIDPT+j];
      }
      while ((tstsum < WGTONE-2)||(tstsum > WGTONE+2)) {

         double adj = (double)WGTONE/(double)tstsum;

         for (int j=-horzkern;j<=horzkern;j++) {
            hkern[i].wgt[MIDPT+j] *= adj;
         }

         tstsum = 0;
         for (int j=-horzkern;j<=horzkern;j++) {
            tstsum += hkern[i].wgt[MIDPT+j];
         }
      }

      // put any residue in the middle wgt
      hkern[i].wgt[MIDPT] += WGTONE - tstsum;
   }

   /////////////////////////////////////////////////////////////////////////////
   //
   // the horz wgt find tables (find tap weights for a given dst pel)
   delete[] huvTbl;
   huvTbl    = new KCoeff *[dstwdth];
   delete[] hy0Tbl;
   hy0Tbl    = new KCoeff *[dstwdth];
   delete[] hy1Tbl;
   hy1Tbl    = new KCoeff *[dstwdth];

   // the position tables (find a src pel from a dst pel)
   delete[] cntruvTbl;
   cntruvTbl = new int[dstwdth];
   delete[] cntry0Tbl;
   cntry0Tbl = new int[dstwdth];
   delete[] cntry1Tbl;
   cntry1Tbl = new int[dstwdth];

   for (int i=0;i<dstwdth;i+=2) {

      huvTbl[i] = hkern + ((i/2)%hmod);
      hy0Tbl[i] = hkern + (( i )%hmod);
      hy1Tbl[i] = hkern + ((i+1)%hmod);

      cntruvTbl[i] = (int)((i/2 + 0.5) * cvtfactor);
      cntry0Tbl[i] = (int)(( i  + 0.5) * cvtfactor);
      cntry1Tbl[i] = (int)(( i  + 1.5) * cvtfactor);
   }

   /////////////////////////////////////////////////////////////////////////////
   // the vertical   coefficients
   vmod = dsthght/gcd(srchght, dsthght);
   cvtfactor = (double)srchght/(double)dsthght;

   delete[] vkern;
   vkern = new KCoeff[vmod];
   double vcoeff[MAX_WEIGHTS];
   for (int i=0;i<vmod;i++) {

      // compute the weights
      double cntr0 = (i + 0.5)*cvtfactor;
      double cntr1 = (double)((int)cntr0);

      // arg is delta of pixel center from
      // scaled position of dst pixel center
      double arg = (cntr1 + 0.5) - cntr0;

      for (int j=0;j<=vertkern;j++) {
         vcoeff[MIDPT-j] = Kernel(arg-j, srcwdth, dstwdth, vertkern);
         vcoeff[MIDPT+j] = Kernel(arg+j, srcwdth, dstwdth, vertkern);
      }

      // normalize
      double negsum = 0.0;
      double possum = 0.0;
      for (int j=-vertkern;j<=vertkern;j++) {
         if (vcoeff[MIDPT+j] < 0.0)
            negsum -= vcoeff[MIDPT+j];
         else
            possum += vcoeff[MIDPT+j];
      }
      double sum = possum - negsum;
      for (int j=-vertkern;j<=vertkern;j++) {
         vcoeff[MIDPT+j] /= sum;
      }

      // convert to signed integer
      for (int j=-vertkern;j<=vertkern;j++) {
         vkern[i].wgt[MIDPT+j] = (int)(WGTONE * vcoeff[MIDPT+j]);
      }

      // sharpen up the normalization
      int tstsum = 0;
      for (int j=-vertkern;j<=vertkern;j++) {
         tstsum += vkern[i].wgt[MIDPT+j];
      }
      while ((tstsum < (WGTONE-2))||(tstsum > (WGTONE+2))) {

         double adj = (double)WGTONE/(double)tstsum;

         for (int j=-vertkern;j<=vertkern;j++) {
            vkern[i].wgt[MIDPT+j] *= adj;
         }

         tstsum = 0;
         for (int j=-vertkern;j<=vertkern;j++) {
            tstsum += vkern[i].wgt[MIDPT+j];
         }
      }

      // put any residue in the middle wgt
      vkern[i].wgt[MIDPT] += WGTONE - tstsum;
   }

   /////////////////////////////////////////////////////////////////////////////
   //
   // the vert coefficient find tables (find tap weights for a given dst pel)
   delete[] vrtTbl;
   vrtTbl    = new KCoeff *[dsthght];

   // the position tables (find a src pel from a dst pel)
   delete[] cntrVTbl;
   cntrVTbl = new int[dsthght];

   for (int i=0;i<dsthght;i+=1) {

      vrtTbl[i] = vkern + (( i )%vmod);

      cntrVTbl[i] = (int)((i + 0.5) * cvtfactor);
   }

   // successful init
   return(0);
}

////////////////////////////////////////////////////////////////////////////////
//
//                          c o n v e r t
//
void CDownConverter::
convert(MTI_UINT8 **srcBuf, MTI_UINT8 **dstBuf)
{
   srcbuf[0] = srcBuf[0];
   srcbuf[1] = srcBuf[1];

   dstbuf[0] = dstBuf[0];
   dstbuf[1] = dstBuf[1];

#ifdef NO_MULTI // testing with 2 stripes only
   unpackThread.PrimaryFunction(this,0);
   unpackThread.PrimaryFunction(this,1);
   horzThread.PrimaryFunction(this,0);
   horzThread.PrimaryFunction(this,1);
   vertThread.PrimaryFunction(this,0);
   vertThread.PrimaryFunction(this,1);
#else
   // fire up one thread for each stripe
   //int iRet;
   /* iRet = */ MThreadStart(&unpackThread);
   //if (iRet) {
   //   TRACE_0(errout << "Convert: MThreadStart failed, iRet=" << iRet);
   //}
#ifndef TWO_THREADS
   /* iRet = */ MThreadStart(&horzThread);
   //if (iRet) {
   //   TRACE_0(errout << "Convert: MThreadStart failed, iRet=" << iRet);
   //}
#endif
   /* iRet = */ MThreadStart(&vertThread);
   //if (iRet) {
   //   TRACE_0(errout << "Convert: MThreadStart failed, iRet=" << iRet);
   //}
#endif
}

int CDownConverter::
gcd(int num1, int num2)
{
   if (num1==0) return num2;
   if (num2==0) return num1;

   if (num1 >= num2)
      return(gcd(num2, (num1%num2)));
   else
      return(gcd(num1, (num2%num1)));
}

////////////////////////////////////////////////////////////////////////////////
//
//                 KERNELS - using the Blackman for now
//
#define PI 3.1415927

#if 1 // Blackman
double CDownConverter::Kernel(double arg, int srcpts, int dstpts, int kern)
{
   double retVal;

   if ((arg<=-kern)||(arg>=kern)) return(0.0);

   if (arg==0.0) return(1.0);

   retVal = ((double)dstpts/(double)srcpts)*PI*arg;

   retVal = sin(retVal)/retVal;

   retVal *= (0.42 + 0.5*cos(PI*arg/(double)kern) + .08*cos(PI*2.0*arg/(double)kern));

   return retVal;
}
#endif

#if 0 // Hamming
double CDownConverter::Kernel(double arg, int srcpts, int dstpts)
{
    double retVal;

   if (arg==0.0) return(1.0);

   retVal = ((double)dstpts/(double)srcpts)*PI*arg;

   retVal = sin(retVal)/retVal;

   retVal *= (0.54 + 0.46*cos(PI*arg/(double)KERNEL));

   return retVal;
}
#endif

#if 0 // Lanczos
double CDownConverter::Kernel(double arg, int srcpts, int dstpts)
{
   double retVal;

   if ((arg<=-KERNEL)||(arg>=KERNEL)) return(0.0);

   if (arg==0.0) return(1.0);

   retVal = sin(PI*arg/(double)KERNEL)/(PI*arg/(double)KERNEL);

   return retVal;
}
#endif

#if 0 // windowing function only
double CDownConverter::Kernel(double arg, int srcpts, int dstpts)
{
   double retVal;

   if ((arg<=-KERNEL)||(arg>=KERNEL)) return(0.0);

   if (arg==0.0) return(1.0);

   retVal = (0.42 + 0.5*cos(PI*arg/(double)KERNEL) + .08*cos(PI*2.0*arg/(double)KERNEL));

   return retVal;
}
#endif

////////////////////////////////////////////////////////////////////////////////

void null(void *v, int iJob)
{
}

////////////////////////////////////////////////////////////////////////////////
// These format-specific segments are called from the first thread. They may be
// compiled to call the horizontal downconvert directly when they are done.
//
// Note that the unpack is limited to the source rectangle, which may not be the
// entire HD frame. This may save a (very) little effort
//

#define KERNEL 1
void unpackYUV_8_K1(void *v, int iJob)
{
   CDownConverter *vp = (CDownConverter *)v;

   RECT srcrect = vp->srcrect;
   unsigned char *srcbuf[2];
   srcbuf[0] = vp->srcbuf[0];
   srcbuf[1] = vp->srcbuf[1];
   int frmpitch = vp->frmpitch;
   int xoffs = vp->xoffs;
   int pitch1 = vp->pitch1;
   unsigned char *frmBuf1 = vp->frmBuf1;

   unsigned int *src;
   unsigned char *dst, *dstend;
   unsigned int r0;

   /////////////////////////////////////////////////////////////////////////////
   //
   // HORZ Unpack
   //
   // This takes YUV 8-bit down to YUV 8-bit, with row margins added

   int rowsPerStripe = vp->srchght / vp->nStripes;
   int begRow = srcrect.top + iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = srcrect.bottom + 1;

   int rowadv = vp->srcwdth*2 + 4*KERNEL;

   dst = frmBuf1 + (begRow - srcrect.top)*pitch1;
   for (int i=begRow; i<endRow;i++) {

      src    = (unsigned int *)(srcbuf[i%2] + (i/2)*frmpitch + xoffs);
      dstend = dst + rowadv;

      // copy the first pixel pair KERNEL times
      for (int j=0;j<KERNEL;j++) {

         r0 = src[0];
         dst[0] = r0;
         dst[1] = r0>>8;
         dst[2] = r0>>16;
         dst[3] = r0>>8;
         dst += 4;
      }

      while (true) {

         r0 = src[0];
         dst[0]  = r0;
         dst[1]  = r0>>8;
         dst[2]  = r0>>16;
         dst[3]  = r0>>24;
         dst += 4;
         if (dst==dstend) break;
         src += 1;
      }

      // copy the last pixel pair KERNEL times
      dstend -= 4;
      for (int j=0;j<KERNEL;j++) {
         dst[0] = dstend[0];
         dst[1] = dstend[3];
         dst[2] = dstend[2];
         dst[3] = dstend[3];
         dst += 4;
      }
   }

#ifdef TWO_THREADS
   // by calling the horizontal downconvert from here,
   // the second thread can be eliminated.
   horizontalDownconvert_K1(v, iJob);
#endif
}

#undef KERNEL
#define KERNEL 2
void unpackYUV_8_K2(void *v, int iJob)
{
   CDownConverter *vp = (CDownConverter *)v;

   RECT srcrect = vp->srcrect;
   unsigned char *srcbuf[2];
   srcbuf[0] = vp->srcbuf[0];
   srcbuf[1] = vp->srcbuf[1];
   int frmpitch = vp->frmpitch;
   int xoffs = vp->xoffs;
   int pitch1 = vp->pitch1;
   unsigned char *frmBuf1 = vp->frmBuf1;

   unsigned int *src;
   unsigned char *dst, *dstend;
   unsigned int r0;

   /////////////////////////////////////////////////////////////////////////////
   //
   // HORZ Unpack
   //
   // This takes YUV 8-bit down to YUV 8-bit, with row margins added

   int rowsPerStripe = vp->srchght / vp->nStripes;
   int begRow = srcrect.top + iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = srcrect.bottom + 1;

   int rowadv = vp->srcwdth*2 + 4*KERNEL;

   dst = frmBuf1 + (begRow - srcrect.top)*pitch1;
   for (int i=begRow; i<endRow;i++) {

      src    = (unsigned int *)(srcbuf[i%2] + (i/2)*frmpitch + xoffs);
      dstend = dst + rowadv;

      // copy the first pixel pair KERNEL times
      for (int j=0;j<KERNEL;j++) {

         r0 = src[0];
         dst[0] = r0;
         dst[1] = r0>>8;
         dst[2] = r0>>16;
         dst[3] = r0>>8;
         dst += 4;
      }

      while (true) {

         r0 = src[0];
         dst[0]  = r0;
         dst[1]  = r0>>8;
         dst[2]  = r0>>16;
         dst[3]  = r0>>24;
         dst += 4;
         if (dst==dstend) break;
         src += 1;
      }

      // copy the last pixel pair KERNEL times
      dstend -= 4;
      for (int j=0;j<KERNEL;j++) {
         dst[0] = dstend[0];
         dst[1] = dstend[3];
         dst[2] = dstend[2];
         dst[3] = dstend[3];
         dst += 4;
      }
   }

#ifdef TWO_THREADS
   // by calling the horizontal downconvert from here,
   // the second thread can be eliminated.
   horizontalDownconvert_K2(v, iJob);
#endif
}

#undef KERNEL
#define KERNEL 3
void unpackYUV_8_K3(void *v, int iJob)
{
   CDownConverter *vp = (CDownConverter *)v;

   RECT srcrect = vp->srcrect;
   unsigned char *srcbuf[2];
   srcbuf[0] = vp->srcbuf[0];
   srcbuf[1] = vp->srcbuf[1];
   int frmpitch = vp->frmpitch;
   int xoffs = vp->xoffs;
   int pitch1 = vp->pitch1;
   unsigned char *frmBuf1 = vp->frmBuf1;

   unsigned int *src;
   unsigned char *dst, *dstend;
   unsigned int r0;

   /////////////////////////////////////////////////////////////////////////////
   //
   // HORZ Unpack
   //
   // This takes YUV 8-bit down to YUV 8-bit, with row margins added

   int rowsPerStripe = vp->srchght / vp->nStripes;
   int begRow = srcrect.top + iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = srcrect.bottom + 1;

   int rowadv = vp->srcwdth*2 + 4*KERNEL;

   dst = frmBuf1 + (begRow - srcrect.top)*pitch1;
   for (int i=begRow; i<endRow;i++) {

      src    = (unsigned int *)(srcbuf[i%2] + (i/2)*frmpitch + xoffs);
      dstend = dst + rowadv;

      // copy the first pixel pair KERNEL times
      for (int j=0;j<KERNEL;j++) {

         r0 = src[0];
         dst[0] = r0;
         dst[1] = r0>>8;
         dst[2] = r0>>16;
         dst[3] = r0>>8;
         dst += 4;
      }

      while (true) {

         r0 = src[0];
         dst[0]  = r0;
         dst[1]  = r0>>8;
         dst[2]  = r0>>16;
         dst[3]  = r0>>24;
         dst += 4;
         if (dst==dstend) break;
         src += 1;
      }

      // copy the last pixel pair KERNEL times
      dstend -= 4;
      for (int j=0;j<KERNEL;j++) {
         dst[0] = dstend[0];
         dst[1] = dstend[3];
         dst[2] = dstend[2];
         dst[3] = dstend[3];
         dst += 4;
      }
   }

#ifdef TWO_THREADS
   // by calling the horizontal downconvert from here,
   // the second thread can be eliminated.
   horizontalDownconvert_K3(v, iJob);
#endif
}

#undef KERNEL
#define KERNEL 4
void unpackYUV_8_K4(void *v, int iJob)
{
   CDownConverter *vp = (CDownConverter *)v;

   RECT srcrect = vp->srcrect;
   unsigned char *srcbuf[2];
   srcbuf[0] = vp->srcbuf[0];
   srcbuf[1] = vp->srcbuf[1];
   int frmpitch = vp->frmpitch;
   int xoffs = vp->xoffs;
   int pitch1 = vp->pitch1;
   unsigned char *frmBuf1 = vp->frmBuf1;

   unsigned int *src;
   unsigned char *dst, *dstend;
   unsigned int r0;

   /////////////////////////////////////////////////////////////////////////////
   //
   // HORZ Unpack
   //
   // This takes YUV 8-bit down to YUV 8-bit, with row margins added

   int rowsPerStripe = vp->srchght / vp->nStripes;
   int begRow = srcrect.top + iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = srcrect.bottom + 1;

   int rowadv = vp->srcwdth*2 + 4*KERNEL;

   dst = frmBuf1 + (begRow - srcrect.top)*pitch1;
   for (int i=begRow; i<endRow;i++) {

      src    = (unsigned int *)(srcbuf[i%2] + (i/2)*frmpitch + xoffs);
      dstend = dst + rowadv;

      // copy the first pixel pair KERNEL times
      for (int j=0;j<KERNEL;j++) {

         r0 = src[0];
         dst[0] = r0;
         dst[1] = r0>>8;
         dst[2] = r0>>16;
         dst[3] = r0>>8;
         dst += 4;
      }

      while (true) {

         r0 = src[0];
         dst[0]  = r0;
         dst[1]  = r0>>8;
         dst[2]  = r0>>16;
         dst[3]  = r0>>24;
         dst += 4;
         if (dst==dstend) break;
         src += 1;
      }

      // copy the last pixel pair KERNEL times
      dstend -= 4;
      for (int j=0;j<KERNEL;j++) {
         dst[0] = dstend[0];
         dst[1] = dstend[3];
         dst[2] = dstend[2];
         dst[3] = dstend[3];
         dst += 4;
      }
   }

#ifdef TWO_THREADS
   // by calling the horizontal downconvert from here,
   // the second thread can be eliminated.
   horizontalDownconvert_K4(v, iJob);
#endif
}

#undef KERNEL
#define KERNEL 5
void unpackYUV_8_K5(void *v, int iJob)
{
   CDownConverter *vp = (CDownConverter *)v;

   RECT srcrect = vp->srcrect;
   unsigned char *srcbuf[2];
   srcbuf[0] = vp->srcbuf[0];
   srcbuf[1] = vp->srcbuf[1];
   int frmpitch = vp->frmpitch;
   int xoffs = vp->xoffs;
   int pitch1 = vp->pitch1;
   unsigned char *frmBuf1 = vp->frmBuf1;

   unsigned int *src;
   unsigned char *dst, *dstend;
   unsigned int r0;

   /////////////////////////////////////////////////////////////////////////////
   //
   // HORZ Unpack
   //
   // This takes YUV 8-bit down to YUV 8-bit, with row margins added

   int rowsPerStripe = vp->srchght / vp->nStripes;
   int begRow = srcrect.top + iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = srcrect.bottom + 1;

   int rowadv = vp->srcwdth*2 + 4*KERNEL;

   dst = frmBuf1 + (begRow - srcrect.top)*pitch1;
   for (int i=begRow; i<endRow;i++) {

      src    = (unsigned int *)(srcbuf[i%2] + (i/2)*frmpitch + xoffs);
      dstend = dst + rowadv;

      // copy the first pixel pair KERNEL times
      for (int j=0;j<KERNEL;j++) {

         r0 = src[0];
         dst[0] = r0;
         dst[1] = r0>>8;
         dst[2] = r0>>16;
         dst[3] = r0>>8;
         dst += 4;
      }

      while (true) {

         r0 = src[0];
         dst[0]  = r0;
         dst[1]  = r0>>8;
         dst[2]  = r0>>16;
         dst[3]  = r0>>24;
         dst += 4;
         if (dst==dstend) break;
         src += 1;
      }

      // copy the last pixel pair KERNEL times
      dstend -= 4;
      for (int j=0;j<KERNEL;j++) {
         dst[0] = dstend[0];
         dst[1] = dstend[3];
         dst[2] = dstend[2];
         dst[3] = dstend[3];
         dst += 4;
      }
   }
#ifdef TWO_THREADS
   // by calling the horizontal downconvert from here,
   // the second thread can be eliminated.
   horizontalDownconvert_K5(v, iJob);
#endif
}

#undef KERNEL
#define KERNEL 6
void unpackYUV_8_K6(void *v, int iJob)
{
   CDownConverter *vp = (CDownConverter *)v;

   RECT srcrect = vp->srcrect;
   unsigned char *srcbuf[2];
   srcbuf[0] = vp->srcbuf[0];
   srcbuf[1] = vp->srcbuf[1];
   int frmpitch = vp->frmpitch;
   int xoffs = vp->xoffs;
   int pitch1 = vp->pitch1;
   unsigned char *frmBuf1 = vp->frmBuf1;

   unsigned int *src;
   unsigned char *dst, *dstend;
   unsigned int r0;

   /////////////////////////////////////////////////////////////////////////////
   //
   // HORZ Unpack
   //
   // This takes YUV 8-bit down to YUV 8-bit, with row margins added

   int rowsPerStripe = vp->srchght / vp->nStripes;
   int begRow = srcrect.top + iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = srcrect.bottom + 1;

   int rowadv = vp->srcwdth*2 + 4*KERNEL;

   dst = frmBuf1 + (begRow - srcrect.top)*pitch1;
   for (int i=begRow; i<endRow;i++) {

      src    = (unsigned int *)(srcbuf[i%2] + (i/2)*frmpitch + xoffs);
      dstend = dst + rowadv;

      // copy the first pixel pair KERNEL times
      for (int j=0;j<KERNEL;j++) {

         r0 = src[0];
         dst[0] = r0;
         dst[1] = r0>>8;
         dst[2] = r0>>16;
         dst[3] = r0>>8;
         dst += 4;
      }

      while (true) {

         r0 = src[0];
         dst[0]  = r0;
         dst[1]  = r0>>8;
         dst[2]  = r0>>16;
         dst[3]  = r0>>24;
         dst += 4;
         if (dst==dstend) break;
         src += 1;
      }

      // copy the last pixel pair KERNEL times
      dstend -= 4;
      for (int j=0;j<KERNEL;j++) {
         dst[0] = dstend[0];
         dst[1] = dstend[3];
         dst[2] = dstend[2];
         dst[3] = dstend[3];
         dst += 4;
      }
   }
#ifdef TWO_THREADS
   // by calling the horizontal downconvert from here,
   // the second thread can be eliminated.
   horizontalDownconvert_K6(v, iJob);
#endif
}

#undef KERNEL
#define KERNEL 7
void unpackYUV_8_K7(void *v, int iJob)
{
   CDownConverter *vp = (CDownConverter *)v;

   RECT srcrect = vp->srcrect;
   unsigned char *srcbuf[2];
   srcbuf[0] = vp->srcbuf[0];
   srcbuf[1] = vp->srcbuf[1];
   int frmpitch = vp->frmpitch;
   int xoffs = vp->xoffs;
   int pitch1 = vp->pitch1;
   unsigned char *frmBuf1 = vp->frmBuf1;

   unsigned int *src;
   unsigned char *dst, *dstend;
   unsigned int r0;

   /////////////////////////////////////////////////////////////////////////////
   //
   // HORZ Unpack
   //
   // This takes YUV 8-bit down to YUV 8-bit, with row margins added

   int rowsPerStripe = vp->srchght / vp->nStripes;
   int begRow = srcrect.top + iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = srcrect.bottom + 1;

   int rowadv = vp->srcwdth*2 + 4*KERNEL;

   dst = frmBuf1 + (begRow - srcrect.top)*pitch1;
   for (int i=begRow; i<endRow;i++) {

      src    = (unsigned int *)(srcbuf[i%2] + (i/2)*frmpitch + xoffs);
      dstend = dst + rowadv;

      // copy the first pixel pair KERNEL times
      for (int j=0;j<KERNEL;j++) {

         r0 = src[0];
         dst[0] = r0;
         dst[1] = r0>>8;
         dst[2] = r0>>16;
         dst[3] = r0>>8;
         dst += 4;
      }

      while (true) {

         r0 = src[0];
         dst[0]  = r0;
         dst[1]  = r0>>8;
         dst[2]  = r0>>16;
         dst[3]  = r0>>24;
         dst += 4;
         if (dst==dstend) break;
         src += 1;
      }

      // copy the last pixel pair KERNEL times
      dstend -= 4;
      for (int j=0;j<KERNEL;j++) {
         dst[0] = dstend[0];
         dst[1] = dstend[3];
         dst[2] = dstend[2];
         dst[3] = dstend[3];
         dst += 4;
      }
   }

#ifdef TWO_THREADS
   // by calling the horizontal downconvert from here,
   // the second thread can be eliminated.
   horizontalDownconvert_K7(v, iJob);
#endif
}

void unpackYUV_10_K1(void *v, int iJob)
{
   // can be implemented (but not yet needed in PC world)
}

void unpackYUV_10_K2(void *v, int iJob)
{
   // can be implemented (but not yet needed in PC world)
}

void unpackYUV_10_K3(void *v, int iJob)
{
   // can be implemented (but not yet needed in PC world)
}

void unpackYUV_10_K4(void *v, int iJob)
{
   // can be implemented (but not yet needed in PC world)
}

void unpackYUV_10_K5(void *v, int iJob)
{
   // can be implemented (but not yet needed in PC world)
}

void unpackYUV_10_K6(void *v, int iJob)
{
   // can be implemented (but not yet needed in PC world)
}

void unpackYUV_10_K7(void *v, int iJob)
{
   // can be implemented (but not yet needed in PC world)
}

#undef KERNEL
#define KERNEL 1
void unpackDVS_10_K1(void *v, int iJob)
{
   CDownConverter *vp = (CDownConverter *)v;

   RECT srcrect = vp->srcrect;
   int srchght = vp->srchght;
   unsigned char *srcbuf[2];
   srcbuf[0] = vp->srcbuf[0];
   srcbuf[1] = vp->srcbuf[1];
   int frmpitch = vp->frmpitch;
   int xoffs = vp->xoffs;
   int pitch1 = vp->pitch1;
   unsigned char *frmBuf1 = vp->frmBuf1;

   unsigned int *src;
   unsigned char *dst, *dstend;
   unsigned int r0;

   /////////////////////////////////////////////////////////////////////////////
   //
   // HORZ Unpack
   //
   // This takes DVS 10-bit down to YUV 8-bit, with row margins added

   int rowsPerStripe = srchght / vp->nStripes;
   int begRow = srcrect.top + iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = srcrect.bottom + 1;

   int rowadv = vp->srcwdth*2 + 4*KERNEL;
   int phase  = vp->phase;

   dst = frmBuf1 + (begRow - srcrect.top)*pitch1;
   for (int i=begRow; i<endRow;i++) {

      src    = (unsigned int *)(srcbuf[i%2] + (i/2)*frmpitch + xoffs);
      dstend = dst + rowadv;

      // copy the first pixel pair KERNEL times
      if (phase==0) {

         for (int j=0;j<KERNEL;j++) {

            r0 = src[0];
            dst[0] = r0>>2;
            dst[1] = r0>>12;
            dst[2] = r0>>22;
            dst[3] = r0>>12;
            dst += 4;
         }
      }
      else if (phase==1) {

         for (int j=0;j<KERNEL;j++) {

            r0 = src[1];
            dst[0] = r0>>12;
            dst[1] = r0>>22;
            dst[3] = r0>>22;
            r0 = src[2];
            dst[2] = r0>>2;
            dst += 4;
         }
      }
      else {

         for (int j=0;j<KERNEL;j++) {

            r0 = src[2];
            dst[0] = r0>>22;
            r0 = src[3];
            dst[1] = r0>>2;
            dst[2] = r0>>12;
            dst[3] = r0>>2;
            dst += 4;
         }
      }

      if (phase==0) goto p0;
      if (phase==1) goto p1;
      goto p2;

      while (true) {

         p0:;

            r0 = src[0];
            dst[0] = r0>>2;
            dst[1] = r0>>12;
            dst[2] = r0>>22;
            r0 = src[1];
            dst[3] = r0>>2;
            dst += 4;
            if (dst==dstend) break;

         p1:;

            r0 = src[1];
            dst[0] = r0>>12;
            dst[1] = r0>>22;
            r0 = src[2];
            dst[2] = r0>>2;
            dst[3] = r0>>12;
            dst += 4;
            if (dst==dstend) break;

         p2:;

            r0 = src[2];
            dst[0] = r0>>22;
            r0 = src[3];
            dst[1] = r0>>2;
            dst[2] = r0>>12;
            dst[3] = r0>>22;
            dst += 4;
            if (dst==dstend) break;
            src += 4;
      }

      // copy the last pixel pair KERNEL times
      dstend -= 4;
      for (int j=0;j<KERNEL;j++) {
         dst[0] = dstend[0];
         dst[1] = dstend[3];
         dst[2] = dstend[2];
         dst[3] = dstend[3];
         dst += 4;
      }
   }

#ifdef TWO_THREADS
   // by calling the horizontal downconvert from here,
   // the second thread can be eliminated.
   horizontalDownconvert_K1(v, iJob);
#endif
}

#undef KERNEL
#define KERNEL 2
void unpackDVS_10_K2(void *v, int iJob)
{
   CDownConverter *vp = (CDownConverter *)v;

   RECT srcrect = vp->srcrect;
   int srchght = vp->srchght;
   unsigned char *srcbuf[2];
   srcbuf[0] = vp->srcbuf[0];
   srcbuf[1] = vp->srcbuf[1];
   int frmpitch = vp->frmpitch;
   int xoffs = vp->xoffs;
   int pitch1 = vp->pitch1;
   unsigned char *frmBuf1 = vp->frmBuf1;

   unsigned int *src;
   unsigned char *dst, *dstend;
   unsigned int r0;

   /////////////////////////////////////////////////////////////////////////////
   //
   // HORZ Unpack
   //
   // This takes DVS 10-bit down to YUV 8-bit, with row margins added

   int rowsPerStripe = srchght / vp->nStripes;
   int begRow = srcrect.top + iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = srcrect.bottom + 1;

   int rowadv = vp->srcwdth*2 + 4*KERNEL;
   int phase  = vp->phase;

   dst = frmBuf1 + (begRow - srcrect.top)*pitch1;
   for (int i=begRow; i<endRow;i++) {

      src    = (unsigned int *)(srcbuf[i%2] + (i/2)*frmpitch + xoffs);
      dstend = dst + rowadv;

      // copy the first pixel pair KERNEL times
      if (phase==0) {

         for (int j=0;j<KERNEL;j++) {

            r0 = src[0];
            dst[0] = r0>>2;
            dst[1] = r0>>12;
            dst[2] = r0>>22;
            dst[3] = r0>>12;
            dst += 4;
         }
      }
      else if (phase==1) {

         for (int j=0;j<KERNEL;j++) {

            r0 = src[1];
            dst[0] = r0>>12;
            dst[1] = r0>>22;
            dst[3] = r0>>22;
            r0 = src[2];
            dst[2] = r0>>2;
            dst += 4;
         }
      }
      else {

         for (int j=0;j<KERNEL;j++) {

            r0 = src[2];
            dst[0] = r0>>22;
            r0 = src[3];
            dst[1] = r0>>2;
            dst[2] = r0>>12;
            dst[3] = r0>>2;
            dst += 4;
         }
      }

      if (phase==0) goto p0;
      if (phase==1) goto p1;
      goto p2;

      while (true) {

         p0:;

            r0 = src[0];
            dst[0] = r0>>2;
            dst[1] = r0>>12;
            dst[2] = r0>>22;
            r0 = src[1];
            dst[3] = r0>>2;
            dst += 4;
            if (dst==dstend) break;

         p1:;

            r0 = src[1];
            dst[0] = r0>>12;
            dst[1] = r0>>22;
            r0 = src[2];
            dst[2] = r0>>2;
            dst[3] = r0>>12;
            dst += 4;
            if (dst==dstend) break;

         p2:;

            r0 = src[2];
            dst[0] = r0>>22;
            r0 = src[3];
            dst[1] = r0>>2;
            dst[2] = r0>>12;
            dst[3] = r0>>22;
            dst += 4;
            if (dst==dstend) break;
            src += 4;
      }

      // copy the last pixel pair KERNEL times
      dstend -= 4;
      for (int j=0;j<KERNEL;j++) {
         dst[0] = dstend[0];
         dst[1] = dstend[3];
         dst[2] = dstend[2];
         dst[3] = dstend[3];
         dst += 4;
      }
   }

#ifdef TWO_THREADS
   // by calling the horizontal downconvert from here,
   // the second thread can be eliminated.
   horizontalDownconvert_K2(v, iJob);
#endif
}

#undef KERNEL
#define KERNEL 3
void unpackDVS_10_K3(void *v, int iJob)
{
   CDownConverter *vp = (CDownConverter *)v;

   RECT srcrect = vp->srcrect;
   int srchght = vp->srchght;
   unsigned char *srcbuf[2];
   srcbuf[0] = vp->srcbuf[0];
   srcbuf[1] = vp->srcbuf[1];
   int frmpitch = vp->frmpitch;
   int xoffs = vp->xoffs;
   int pitch1 = vp->pitch1;
   unsigned char *frmBuf1 = vp->frmBuf1;

   unsigned int *src;
   unsigned char *dst, *dstend;
   unsigned int r0;

   /////////////////////////////////////////////////////////////////////////////
   //
   // HORZ Unpack
   //
   // This takes DVS 10-bit down to YUV 8-bit, with row margins added

   int rowsPerStripe = srchght / vp->nStripes;
   int begRow = srcrect.top + iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = srcrect.bottom + 1;

   int rowadv = vp->srcwdth*2 + 4*KERNEL;
   int phase  = vp->phase;

   dst = frmBuf1 + (begRow - srcrect.top)*pitch1;
   for (int i=begRow; i<endRow;i++) {

      src    = (unsigned int *)(srcbuf[i%2] + (i/2)*frmpitch + xoffs);
      dstend = dst + rowadv;

      // copy the first pixel pair KERNEL times
      if (phase==0) {

         for (int j=0;j<KERNEL;j++) {

            r0 = src[0];
            dst[0] = r0>>2;
            dst[1] = r0>>12;
            dst[2] = r0>>22;
            dst[3] = r0>>12;
            dst += 4;
         }
      }
      else if (phase==1) {

         for (int j=0;j<KERNEL;j++) {

            r0 = src[1];
            dst[0] = r0>>12;
            dst[1] = r0>>22;
            dst[3] = r0>>22;
            r0 = src[2];
            dst[2] = r0>>2;
            dst += 4;
         }
      }
      else {

         for (int j=0;j<KERNEL;j++) {

            r0 = src[2];
            dst[0] = r0>>22;
            r0 = src[3];
            dst[1] = r0>>2;
            dst[2] = r0>>12;
            dst[3] = r0>>2;
            dst += 4;
         }
      }

      if (phase==0) goto p0;
      if (phase==1) goto p1;
      goto p2;

      while (true) {

         p0:;

            r0 = src[0];
            dst[0] = r0>>2;
            dst[1] = r0>>12;
            dst[2] = r0>>22;
            r0 = src[1];
            dst[3] = r0>>2;
            dst += 4;
            if (dst==dstend) break;

         p1:;

            r0 = src[1];
            dst[0] = r0>>12;
            dst[1] = r0>>22;
            r0 = src[2];
            dst[2] = r0>>2;
            dst[3] = r0>>12;
            dst += 4;
            if (dst==dstend) break;

         p2:;

            r0 = src[2];
            dst[0] = r0>>22;
            r0 = src[3];
            dst[1] = r0>>2;
            dst[2] = r0>>12;
            dst[3] = r0>>22;
            dst += 4;
            if (dst==dstend) break;
            src += 4;
      }

      // copy the last pixel pair KERNEL times
      dstend -= 4;
      for (int j=0;j<KERNEL;j++) {
         dst[0] = dstend[0];
         dst[1] = dstend[3];
         dst[2] = dstend[2];
         dst[3] = dstend[3];
         dst += 4;
      }
   }

#ifdef TWO_THREADS
   // by calling the horizontal downconvert from here,
   // the second thread can be eliminated.
   horizontalDownconvert_K3(v, iJob);
#endif
}

#undef KERNEL
#define KERNEL 4
void unpackDVS_10_K4(void *v, int iJob)
{
   CDownConverter *vp = (CDownConverter *)v;

   RECT srcrect = vp->srcrect;
   int srchght = vp->srchght;
   unsigned char *srcbuf[2];
   srcbuf[0] = vp->srcbuf[0];
   srcbuf[1] = vp->srcbuf[1];
   int frmpitch = vp->frmpitch;
   int xoffs = vp->xoffs;
   int pitch1 = vp->pitch1;
   unsigned char *frmBuf1 = vp->frmBuf1;

   unsigned int *src;
   unsigned char *dst, *dstend;
   unsigned int r0;

   /////////////////////////////////////////////////////////////////////////////
   //
   // HORZ Unpack
   //
   // This takes DVS 10-bit down to YUV 8-bit, with row margins added

   int rowsPerStripe = srchght / vp->nStripes;
   int begRow = srcrect.top + iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = srcrect.bottom + 1;

   int rowadv = vp->srcwdth*2 + 4*KERNEL;
   int phase  = vp->phase;

   dst = frmBuf1 + (begRow - srcrect.top)*pitch1;
   for (int i=begRow; i<endRow;i++) {

      src    = (unsigned int *)(srcbuf[i%2] + (i/2)*frmpitch + xoffs);
      dstend = dst + rowadv;

      // copy the first pixel pair KERNEL times
      if (phase==0) {

         for (int j=0;j<KERNEL;j++) {

            r0 = src[0];
            dst[0] = r0>>2;
            dst[1] = r0>>12;
            dst[2] = r0>>22;
            dst[3] = r0>>12;
            dst += 4;
         }
      }
      else if (phase==1) {

         for (int j=0;j<KERNEL;j++) {

            r0 = src[1];
            dst[0] = r0>>12;
            dst[1] = r0>>22;
            dst[3] = r0>>22;
            r0 = src[2];
            dst[2] = r0>>2;
            dst += 4;
         }
      }
      else {

         for (int j=0;j<KERNEL;j++) {

            r0 = src[2];
            dst[0] = r0>>22;
            r0 = src[3];
            dst[1] = r0>>2;
            dst[2] = r0>>12;
            dst[3] = r0>>2;
            dst += 4;
         }
      }

      if (phase==0) goto p0;
      if (phase==1) goto p1;
      goto p2;

      while (true) {

         p0:;

            r0 = src[0];
            dst[0] = r0>>2;
            dst[1] = r0>>12;
            dst[2] = r0>>22;
            r0 = src[1];
            dst[3] = r0>>2;
            dst += 4;
            if (dst==dstend) break;

         p1:;

            r0 = src[1];
            dst[0] = r0>>12;
            dst[1] = r0>>22;
            r0 = src[2];
            dst[2] = r0>>2;
            dst[3] = r0>>12;
            dst += 4;
            if (dst==dstend) break;

         p2:;

            r0 = src[2];
            dst[0] = r0>>22;
            r0 = src[3];
            dst[1] = r0>>2;
            dst[2] = r0>>12;
            dst[3] = r0>>22;
            dst += 4;
            if (dst==dstend) break;
            src += 4;
      }

      // copy the last pixel pair KERNEL times
      dstend -= 4;
      for (int j=0;j<KERNEL;j++) {
         dst[0] = dstend[0];
         dst[1] = dstend[3];
         dst[2] = dstend[2];
         dst[3] = dstend[3];
         dst += 4;
      }
   }

#ifdef TWO_THREADS
   // by calling the horizontal downconvert from here,
   // the second thread can be eliminated.
   horizontalDownconvert_K4(v, iJob);
#endif
}

#undef KERNEL
#define KERNEL 5
void unpackDVS_10_K5(void *v, int iJob)
{
   CDownConverter *vp = (CDownConverter *)v;

   RECT srcrect = vp->srcrect;
   int srchght = vp->srchght;
   unsigned char *srcbuf[2];
   srcbuf[0] = vp->srcbuf[0];
   srcbuf[1] = vp->srcbuf[1];
   int frmpitch = vp->frmpitch;
   int xoffs = vp->xoffs;
   int pitch1 = vp->pitch1;
   unsigned char *frmBuf1 = vp->frmBuf1;

   unsigned int *src;
   unsigned char *dst, *dstend;
   unsigned int r0;

   /////////////////////////////////////////////////////////////////////////////
   //
   // HORZ Unpack
   //
   // This takes DVS 10-bit down to YUV 8-bit, with row margins added

   int rowsPerStripe = srchght / vp->nStripes;
   int begRow = srcrect.top + iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = srcrect.bottom + 1;

   int rowadv = vp->srcwdth*2 + 4*KERNEL;
   int phase  = vp->phase;

   dst = frmBuf1 + (begRow - srcrect.top)*pitch1;
   for (int i=begRow; i<endRow;i++) {

      src    = (unsigned int *)(srcbuf[i%2] + (i/2)*frmpitch + xoffs);
      dstend = dst + rowadv;

      // copy the first pixel pair KERNEL times
      if (phase==0) {

         for (int j=0;j<KERNEL;j++) {

            r0 = src[0];
            dst[0] = r0>>2;
            dst[1] = r0>>12;
            dst[2] = r0>>22;
            dst[3] = r0>>12;
            dst += 4;
         }
      }
      else if (phase==1) {

         for (int j=0;j<KERNEL;j++) {

            r0 = src[1];
            dst[0] = r0>>12;
            dst[1] = r0>>22;
            dst[3] = r0>>22;
            r0 = src[2];
            dst[2] = r0>>2;
            dst += 4;
         }
      }
      else {

         for (int j=0;j<KERNEL;j++) {

            r0 = src[2];
            dst[0] = r0>>22;
            r0 = src[3];
            dst[1] = r0>>2;
            dst[2] = r0>>12;
            dst[3] = r0>>2;
            dst += 4;
         }
      }

      if (phase==0) goto p0;
      if (phase==1) goto p1;
      goto p2;

      while (true) {

         p0:;

            r0 = src[0];
            dst[0] = r0>>2;
            dst[1] = r0>>12;
            dst[2] = r0>>22;
            r0 = src[1];
            dst[3] = r0>>2;
            dst += 4;
            if (dst==dstend) break;

         p1:;

            r0 = src[1];
            dst[0] = r0>>12;
            dst[1] = r0>>22;
            r0 = src[2];
            dst[2] = r0>>2;
            dst[3] = r0>>12;
            dst += 4;
            if (dst==dstend) break;

         p2:;

            r0 = src[2];
            dst[0] = r0>>22;
            r0 = src[3];
            dst[1] = r0>>2;
            dst[2] = r0>>12;
            dst[3] = r0>>22;
            dst += 4;
            if (dst==dstend) break;
            src += 4;
      }

      // copy the last pixel pair KERNEL times
      dstend -= 4;
      for (int j=0;j<KERNEL;j++) {
         dst[0] = dstend[0];
         dst[1] = dstend[3];
         dst[2] = dstend[2];
         dst[3] = dstend[3];
         dst += 4;
      }
   }

#ifdef TWO_THREADS
   // by calling the horizontal downconvert from here,
   // the second thread can be eliminated.
   horizontalDownconvert_K5(v, iJob);
#endif
}

#undef KERNEL
#define KERNEL 6
void unpackDVS_10_K6(void *v, int iJob)
{
   CDownConverter *vp = (CDownConverter *)v;

   RECT srcrect = vp->srcrect;
   int srchght = vp->srchght;
   unsigned char *srcbuf[2];
   srcbuf[0] = vp->srcbuf[0];
   srcbuf[1] = vp->srcbuf[1];
   int frmpitch = vp->frmpitch;
   int xoffs = vp->xoffs;
   int pitch1 = vp->pitch1;
   unsigned char *frmBuf1 = vp->frmBuf1;

   unsigned int *src;
   unsigned char *dst, *dstend;
   unsigned int r0;

   /////////////////////////////////////////////////////////////////////////////
   //
   // HORZ Unpack
   //
   // This takes DVS 10-bit down to YUV 8-bit, with row margins added

   int rowsPerStripe = srchght / vp->nStripes;
   int begRow = srcrect.top + iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = srcrect.bottom + 1;

   int rowadv = vp->srcwdth*2 + 4*KERNEL;
   int phase  = vp->phase;

   dst = frmBuf1 + (begRow - srcrect.top)*pitch1;
   for (int i=begRow; i<endRow;i++) {

      src    = (unsigned int *)(srcbuf[i%2] + (i/2)*frmpitch + xoffs);
      dstend = dst + rowadv;

      // copy the first pixel pair KERNEL times
      if (phase==0) {

         for (int j=0;j<KERNEL;j++) {

            r0 = src[0];
            dst[0] = r0>>2;
            dst[1] = r0>>12;
            dst[2] = r0>>22;
            dst[3] = r0>>12;
            dst += 4;
         }
      }
      else if (phase==1) {

         for (int j=0;j<KERNEL;j++) {

            r0 = src[1];
            dst[0] = r0>>12;
            dst[1] = r0>>22;
            dst[3] = r0>>22;
            r0 = src[2];
            dst[2] = r0>>2;
            dst += 4;
         }
      }
      else {

         for (int j=0;j<KERNEL;j++) {

            r0 = src[2];
            dst[0] = r0>>22;
            r0 = src[3];
            dst[1] = r0>>2;
            dst[2] = r0>>12;
            dst[3] = r0>>2;
            dst += 4;
         }
      }

      if (phase==0) goto p0;
      if (phase==1) goto p1;
      goto p2;

      while (true) {

         p0:;

            r0 = src[0];
            dst[0] = r0>>2;
            dst[1] = r0>>12;
            dst[2] = r0>>22;
            r0 = src[1];
            dst[3] = r0>>2;
            dst += 4;
            if (dst==dstend) break;

         p1:;

            r0 = src[1];
            dst[0] = r0>>12;
            dst[1] = r0>>22;
            r0 = src[2];
            dst[2] = r0>>2;
            dst[3] = r0>>12;
            dst += 4;
            if (dst==dstend) break;

         p2:;

            r0 = src[2];
            dst[0] = r0>>22;
            r0 = src[3];
            dst[1] = r0>>2;
            dst[2] = r0>>12;
            dst[3] = r0>>22;
            dst += 4;
            if (dst==dstend) break;
            src += 4;
      }

      // copy the last pixel pair KERNEL times
      dstend -= 4;
      for (int j=0;j<KERNEL;j++) {
         dst[0] = dstend[0];
         dst[1] = dstend[3];
         dst[2] = dstend[2];
         dst[3] = dstend[3];
         dst += 4;
      }
   }

#ifdef TWO_THREADS
   // by calling the horizontal downconvert from here,
   // the second thread can be eliminated.
   horizontalDownconvert_K6(v, iJob);
#endif
}

#undef KERNEL
#define KERNEL 7
void unpackDVS_10_K7(void *v, int iJob)
{
   CDownConverter *vp = (CDownConverter *)v;

   RECT srcrect = vp->srcrect;
   int srchght = vp->srchght;
   unsigned char *srcbuf[2];
   srcbuf[0] = vp->srcbuf[0];
   srcbuf[1] = vp->srcbuf[1];
   int frmpitch = vp->frmpitch;
   int xoffs = vp->xoffs;
   int pitch1 = vp->pitch1;
   unsigned char *frmBuf1 = vp->frmBuf1;

   unsigned int *src;
   unsigned char *dst, *dstend;
   unsigned int r0;

   /////////////////////////////////////////////////////////////////////////////
   //
   // HORZ Unpack
   //
   // This takes DVS 10-bit down to YUV 8-bit, with row margins added

   int rowsPerStripe = srchght / vp->nStripes;
   int begRow = srcrect.top + iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = srcrect.bottom + 1;

   int rowadv = vp->srcwdth*2 + 4*KERNEL;
   int phase  = vp->phase;

   dst = frmBuf1 + (begRow - srcrect.top)*pitch1;
   for (int i=begRow; i<endRow;i++) {

      src    = (unsigned int *)(srcbuf[i%2] + (i/2)*frmpitch + xoffs);
      dstend = dst + rowadv;

      // copy the first pixel pair KERNEL times
      if (phase==0) {

         for (int j=0;j<KERNEL;j++) {

            r0 = src[0];
            dst[0] = r0>>2;
            dst[1] = r0>>12;
            dst[2] = r0>>22;
            dst[3] = r0>>12;
            dst += 4;
         }
      }
      else if (phase==1) {

         for (int j=0;j<KERNEL;j++) {

            r0 = src[1];
            dst[0] = r0>>12;
            dst[1] = r0>>22;
            dst[3] = r0>>22;
            r0 = src[2];
            dst[2] = r0>>2;
            dst += 4;
         }
      }
      else {

         for (int j=0;j<KERNEL;j++) {

            r0 = src[2];
            dst[0] = r0>>22;
            r0 = src[3];
            dst[1] = r0>>2;
            dst[2] = r0>>12;
            dst[3] = r0>>2;
            dst += 4;
         }
      }

      if (phase==0) goto p0;
      if (phase==1) goto p1;
      goto p2;

      while (true) {

         p0:;

            r0 = src[0];
            dst[0] = r0>>2;
            dst[1] = r0>>12;
            dst[2] = r0>>22;
            r0 = src[1];
            dst[3] = r0>>2;
            dst += 4;
            if (dst==dstend) break;

         p1:;

            r0 = src[1];
            dst[0] = r0>>12;
            dst[1] = r0>>22;
            r0 = src[2];
            dst[2] = r0>>2;
            dst[3] = r0>>12;
            dst += 4;
            if (dst==dstend) break;

         p2:;

            r0 = src[2];
            dst[0] = r0>>22;
            r0 = src[3];
            dst[1] = r0>>2;
            dst[2] = r0>>12;
            dst[3] = r0>>22;
            dst += 4;
            if (dst==dstend) break;
            src += 4;
      }

      // copy the last pixel pair KERNEL times
      dstend -= 4;
      for (int j=0;j<KERNEL;j++) {
         dst[0] = dstend[0];
         dst[1] = dstend[3];
         dst[2] = dstend[2];
         dst[3] = dstend[3];
         dst += 4;
      }
   }

#ifdef TWO_THREADS
   // by calling the horizontal downconvert from here,
   // the second thread can be eliminated.
   horizontalDownconvert_K7(v, iJob);
#endif
}

////////////////////////////////////////////////////////////////////////////////
// This is called from the second thread, but can also be called directly from
// inside the unpack code, when testing TWO threads vs THREE.
//

#undef KERNEL
#define KERNEL 1
void horizontalDownconvert_K1(void *v, int iJob)
{
   CDownConverter *vp = (CDownConverter *)v;

   int srchght = vp->srchght;
   int pitch1 = vp->pitch1;
   unsigned char *frmBuf1 = vp->frmBuf1;
   int pitch2 = vp->pitch2;
   unsigned char *frmBuf2 = vp->frmBuf2;
   int dstwdth = vp->dstwdth;

   /////////////////////////////////////////////////////////////////////////////
   //
   // HORZ Downconvert
   //
   // This generates a destination-resolution COLUMN from each row, setting the
   // stage for vertical downconvert. Row margins are added afterwards.

   unsigned char *unprow0, *unprow1, *unprow2, *unprow3, *unprow4, *unprow5;

   unsigned char *dst0;

   KCoeff **huvTbl = vp->huvTbl;
   KCoeff **hy0Tbl = vp->hy0Tbl;
   KCoeff **hy1Tbl = vp->hy1Tbl;

   int *cntruvTbl = vp->cntruvTbl;
   int *cntry0Tbl = vp->cntry0Tbl;
   int *cntry1Tbl = vp->cntry1Tbl;

   int uval0, y0val0, vval0, y1val0, uval1, y0val1, vval1, y1val1;
   int uval2, y0val2, vval2, y1val2, uval3, y0val3, vval3, y1val3;
   int uval4, y0val4, vval4, y1val4, uval5, y0val5, vval5, y1val5;

   int t0;

   // we've unpacked the srcrect, so adjust the begRow & endRow
   int rowsPerStripe = srchght / vp->nStripes;
   int begRow = iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = srchght;

   // downconvert the stripe 6 rows at a time
   int irow;
   for (irow=begRow;irow<endRow-6;irow+=6) {

      unprow0 = frmBuf1 + irow*pitch1 + KERNEL*4;
      unprow1 = unprow0 + pitch1;
      unprow2 = unprow1 + pitch1;
      unprow3 = unprow2 + pitch1;
      unprow4 = unprow3 + pitch1;
      unprow5 = unprow4 + pitch1;

      // dst0 points to the leftmost of 6 COLUMNS
      dst0 = frmBuf2 + KERNEL + irow;

      // generate the output pixel-pairs
      for (int j=0;j<dstwdth;j+=2) {

         KCoeff *iwuv = huvTbl[j];
         KCoeff *iwy0 = hy0Tbl[j];
         KCoeff *iwy1 = hy1Tbl[j];

         int cntruv = cntruvTbl[j];
         int cntry0 = cntry0Tbl[j];
         int cntry1 = cntry1Tbl[j];

         uval0  = 0;
         y0val0 = 0;
         vval0  = 0;
         y1val0 = 0;

         uval1  = 0;
         y0val1 = 0;
         vval1  = 0;
         y1val1 = 0;

         uval2  = 0;
         y0val2 = 0;
         vval2  = 0;
         y1val2 = 0;

         uval3  = 0;
         y0val3 = 0;
         vval3  = 0;
         y1val3 = 0;

         uval4  = 0;
         y0val4 = 0;
         vval4  = 0;
         y1val4 = 0;

         uval5  = 0;
         y0val5 = 0;
         vval5  = 0;
         y1val5 = 0;

         for (int k=-KERNEL;k<=KERNEL;k++) {

             int wgtuv = iwuv->wgt[MIDPT+k];
             int iu  = (cntruv+k)*4;
            uval0  += unprow0[iu  ]*wgtuv;
            uval1  += unprow1[iu  ]*wgtuv;
            uval2  += unprow2[iu  ]*wgtuv;
            uval3  += unprow3[iu  ]*wgtuv;
            uval4  += unprow4[iu  ]*wgtuv;
            uval5  += unprow5[iu  ]*wgtuv;

            vval0  += unprow0[iu+2]*wgtuv;
            vval1  += unprow1[iu+2]*wgtuv;
            vval2  += unprow2[iu+2]*wgtuv;
            vval3  += unprow3[iu+2]*wgtuv;
            vval4  += unprow4[iu+2]*wgtuv;
            vval5  += unprow5[iu+2]*wgtuv;

             int wgty0 = iwy0->wgt[MIDPT+k];
             int iy0 = (cntry0+k)*2 + 1;
            y0val0 += unprow0[iy0 ]*wgty0;
            y0val1 += unprow1[iy0 ]*wgty0;
            y0val2 += unprow2[iy0 ]*wgty0;
            y0val3 += unprow3[iy0 ]*wgty0;
            y0val4 += unprow4[iy0 ]*wgty0;
            y0val5 += unprow5[iy0 ]*wgty0;

             int wgty1 = iwy1->wgt[MIDPT+k];
             int iy1 = (cntry1+k)*2 + 1;

            y1val0 += unprow0[iy1 ]*wgty1;
            y1val1 += unprow1[iy1 ]*wgty1;
            y1val2 += unprow2[iy1 ]*wgty1;
            y1val3 += unprow3[iy1 ]*wgty1;
            y1val4 += unprow4[iy1 ]*wgty1;
            y1val5 += unprow5[iy1 ]*wgty1;
         }

         t0 = uval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         t0 = uval1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[1] = t0;
         t0 = uval2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[2] = t0;
         t0 = uval3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[3] = t0;
         t0 = uval4>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[4] = t0;
         t0 = uval5>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[5] = t0;
         dst0 += pitch2;

         t0 = y0val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         t0 = y0val1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[1] = t0;
         t0 = y0val2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[2] = t0;
         t0 = y0val3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[3] = t0;
         t0 = y0val4>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[4] = t0;
         t0 = y0val5>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[5] = t0;
         dst0 += pitch2;

         t0 = vval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         t0 = vval1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[1] = t0;
         t0 = vval2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[2] = t0;
         t0 = vval3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[3] = t0;
         t0 = vval4>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[4] = t0;
         t0 = vval5>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[5] = t0;
         dst0 += pitch2;

         t0 = y1val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         t0 = y1val1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[1] = t0;
         t0 = y1val2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[2] = t0;
         t0 = y1val3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[3] = t0;
         t0 = y1val4>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[4] = t0;
         t0 = y1val5>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[5] = t0;
         dst0 += pitch2;
      }
   }

   // downconvert the residual lines
   irow -=5;
   for (;irow<endRow;irow+=1) {

      unprow0 = frmBuf1 + irow*pitch1 + KERNEL*4;

      dst0 = frmBuf2 + KERNEL + irow;

      // generate the output pixel-pairs
      for (int j=0;j<dstwdth;j+=2) {

         KCoeff *iwuv = huvTbl[j];
         KCoeff *iwy0 = hy0Tbl[j];
         KCoeff *iwy1 = hy1Tbl[j];

         int cntruv = cntruvTbl[j];
         int cntry0 = cntry0Tbl[j];
         int cntry1 = cntry1Tbl[j];

         uval0  = 0;
         y0val0 = 0;
         vval0  = 0;
         y1val0 = 0;

         for (int k=-KERNEL;k<=KERNEL;k++) {

            int wgtuv = iwuv->wgt[MIDPT+k];
            int iuv = (cntruv+k)*4;
            uval0  += unprow0[iuv    ]*wgtuv;
            vval0  += unprow0[iuv + 2]*wgtuv;

            int wgty0 = iwy0->wgt[MIDPT+k];
            int iy0 = (cntry0+k)*2 + 1;
            y0val0 += unprow0[iy0    ]*wgty0;

            int wgty1 = iwy1->wgt[MIDPT+k];
            int iy1 = (cntry1+k)*2 + 1;
            y1val0 += unprow0[iy1    ]*wgty1;
         }

         // store the results COLUMNWISE
         t0 = uval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         dst0 += pitch2;
         t0 = y0val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         dst0 += pitch2;
         t0 = vval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         dst0 += pitch2;
         t0 = y1val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         dst0 += pitch2;
      }
   }

   // in frmBuf2 we now have, in order, a U-row, a Y-row, a V-row, a Y-row,
   // etc.., so that the original (horz-converted) rows form columns.
}

#undef KERNEL
#define KERNEL 2
void horizontalDownconvert_K2(void *v, int iJob)
{
   CDownConverter *vp = (CDownConverter *)v;

   int srchght = vp->srchght;
   int pitch1 = vp->pitch1;
   unsigned char *frmBuf1 = vp->frmBuf1;
   int pitch2 = vp->pitch2;
   unsigned char *frmBuf2 = vp->frmBuf2;
   int dstwdth = vp->dstwdth;

   /////////////////////////////////////////////////////////////////////////////
   //
   // HORZ Downconvert
   //
   // This generates a destination-resolution COLUMN from each row, setting the
   // stage for vertical downconvert. Row margins are added afterwards.

   unsigned char *unprow0, *unprow1, *unprow2, *unprow3, *unprow4, *unprow5;

   unsigned char *dst0;

   KCoeff **huvTbl = vp->huvTbl;
   KCoeff **hy0Tbl = vp->hy0Tbl;
   KCoeff **hy1Tbl = vp->hy1Tbl;

   int *cntruvTbl = vp->cntruvTbl;
   int *cntry0Tbl = vp->cntry0Tbl;
   int *cntry1Tbl = vp->cntry1Tbl;

   int uval0, y0val0, vval0, y1val0, uval1, y0val1, vval1, y1val1;
   int uval2, y0val2, vval2, y1val2, uval3, y0val3, vval3, y1val3;
   int uval4, y0val4, vval4, y1val4, uval5, y0val5, vval5, y1val5;

   int t0;

   // we've unpacked the srcrect, so adjust the begRow & endRow
   int rowsPerStripe = srchght / vp->nStripes;
   int begRow = iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = srchght;

   // downconvert the stripe 6 rows at a time
   int irow;
   for (irow=begRow;irow<endRow-6;irow+=6) {

      unprow0 = frmBuf1 + irow*pitch1 + KERNEL*4;
      unprow1 = unprow0 + pitch1;
      unprow2 = unprow1 + pitch1;
      unprow3 = unprow2 + pitch1;
      unprow4 = unprow3 + pitch1;
      unprow5 = unprow4 + pitch1;

      // dst0 points to the leftmost of 6 COLUMNS
      dst0 = frmBuf2 + KERNEL + irow;

      // generate the output pixel-pairs
      for (int j=0;j<dstwdth;j+=2) {

         KCoeff *iwuv = huvTbl[j];
         KCoeff *iwy0 = hy0Tbl[j];
         KCoeff *iwy1 = hy1Tbl[j];

         int cntruv = cntruvTbl[j];
         int cntry0 = cntry0Tbl[j];
         int cntry1 = cntry1Tbl[j];

         uval0  = 0;
         y0val0 = 0;
         vval0  = 0;
         y1val0 = 0;

         uval1  = 0;
         y0val1 = 0;
         vval1  = 0;
         y1val1 = 0;

         uval2  = 0;
         y0val2 = 0;
         vval2  = 0;
         y1val2 = 0;

         uval3  = 0;
         y0val3 = 0;
         vval3  = 0;
         y1val3 = 0;

         uval4  = 0;
         y0val4 = 0;
         vval4  = 0;
         y1val4 = 0;

         uval5  = 0;
         y0val5 = 0;
         vval5  = 0;
         y1val5 = 0;

         for (int k=-KERNEL;k<=KERNEL;k++) {

             int wgtuv = iwuv->wgt[MIDPT+k];
             int iu  = (cntruv+k)*4;
            uval0  += unprow0[iu  ]*wgtuv;
            uval1  += unprow1[iu  ]*wgtuv;
            uval2  += unprow2[iu  ]*wgtuv;
            uval3  += unprow3[iu  ]*wgtuv;
            uval4  += unprow4[iu  ]*wgtuv;
            uval5  += unprow5[iu  ]*wgtuv;

            vval0  += unprow0[iu+2]*wgtuv;
            vval1  += unprow1[iu+2]*wgtuv;
            vval2  += unprow2[iu+2]*wgtuv;
            vval3  += unprow3[iu+2]*wgtuv;
            vval4  += unprow4[iu+2]*wgtuv;
            vval5  += unprow5[iu+2]*wgtuv;

             int wgty0 = iwy0->wgt[MIDPT+k];
             int iy0 = (cntry0+k)*2 + 1;
            y0val0 += unprow0[iy0 ]*wgty0;
            y0val1 += unprow1[iy0 ]*wgty0;
            y0val2 += unprow2[iy0 ]*wgty0;
            y0val3 += unprow3[iy0 ]*wgty0;
            y0val4 += unprow4[iy0 ]*wgty0;
            y0val5 += unprow5[iy0 ]*wgty0;

             int wgty1 = iwy1->wgt[MIDPT+k];
             int iy1 = (cntry1+k)*2 + 1;

            y1val0 += unprow0[iy1 ]*wgty1;
            y1val1 += unprow1[iy1 ]*wgty1;
            y1val2 += unprow2[iy1 ]*wgty1;
            y1val3 += unprow3[iy1 ]*wgty1;
            y1val4 += unprow4[iy1 ]*wgty1;
            y1val5 += unprow5[iy1 ]*wgty1;
         }

         t0 = uval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         t0 = uval1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[1] = t0;
         t0 = uval2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[2] = t0;
         t0 = uval3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[3] = t0;
         t0 = uval4>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[4] = t0;
         t0 = uval5>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[5] = t0;
         dst0 += pitch2;

         t0 = y0val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         t0 = y0val1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[1] = t0;
         t0 = y0val2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[2] = t0;
         t0 = y0val3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[3] = t0;
         t0 = y0val4>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[4] = t0;
         t0 = y0val5>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[5] = t0;
         dst0 += pitch2;

         t0 = vval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         t0 = vval1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[1] = t0;
         t0 = vval2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[2] = t0;
         t0 = vval3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[3] = t0;
         t0 = vval4>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[4] = t0;
         t0 = vval5>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[5] = t0;
         dst0 += pitch2;

         t0 = y1val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         t0 = y1val1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[1] = t0;
         t0 = y1val2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[2] = t0;
         t0 = y1val3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[3] = t0;
         t0 = y1val4>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[4] = t0;
         t0 = y1val5>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[5] = t0;
         dst0 += pitch2;
      }
   }

   // downconvert the residual lines
   irow -=5;
   for (;irow<endRow;irow+=1) {

      unprow0 = frmBuf1 + irow*pitch1 + KERNEL*4;

      dst0 = frmBuf2 + KERNEL + irow;

      // generate the output pixel-pairs
      for (int j=0;j<dstwdth;j+=2) {

         KCoeff *iwuv = huvTbl[j];
         KCoeff *iwy0 = hy0Tbl[j];
         KCoeff *iwy1 = hy1Tbl[j];

         int cntruv = cntruvTbl[j];
         int cntry0 = cntry0Tbl[j];
         int cntry1 = cntry1Tbl[j];

         uval0  = 0;
         y0val0 = 0;
         vval0  = 0;
         y1val0 = 0;

         for (int k=-KERNEL;k<=KERNEL;k++) {

            int wgtuv = iwuv->wgt[MIDPT+k];
            int iuv = (cntruv+k)*4;
            uval0  += unprow0[iuv    ]*wgtuv;
            vval0  += unprow0[iuv + 2]*wgtuv;

            int wgty0 = iwy0->wgt[MIDPT+k];
            int iy0 = (cntry0+k)*2 + 1;
            y0val0 += unprow0[iy0    ]*wgty0;

            int wgty1 = iwy1->wgt[MIDPT+k];
            int iy1 = (cntry1+k)*2 + 1;
            y1val0 += unprow0[iy1    ]*wgty1;
         }

         // store the results COLUMNWISE
         t0 = uval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         dst0 += pitch2;
         t0 = y0val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         dst0 += pitch2;
         t0 = vval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         dst0 += pitch2;
         t0 = y1val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         dst0 += pitch2;
      }
   }

   // in frmBuf2 we now have, in order, a U-row, a Y-row, a V-row, a Y-row,
   // etc.., so that the original (horz-converted) rows form columns.
}

#undef KERNEL
#define KERNEL 3
void horizontalDownconvert_K3(void *v, int iJob)
{
   CDownConverter *vp = (CDownConverter *)v;

   int srchght = vp->srchght;
   int pitch1 = vp->pitch1;
   unsigned char *frmBuf1 = vp->frmBuf1;
   int pitch2 = vp->pitch2;
   unsigned char *frmBuf2 = vp->frmBuf2;
   int dstwdth = vp->dstwdth;

   /////////////////////////////////////////////////////////////////////////////
   //
   // HORZ Downconvert
   //
   // This generates a destination-resolution COLUMN from each row, setting the
   // stage for vertical downconvert. Row margins are added afterwards.

   unsigned char *unprow0, *unprow1, *unprow2, *unprow3, *unprow4, *unprow5;

   unsigned char *dst0;

   KCoeff **huvTbl = vp->huvTbl;
   KCoeff **hy0Tbl = vp->hy0Tbl;
   KCoeff **hy1Tbl = vp->hy1Tbl;

   int *cntruvTbl = vp->cntruvTbl;
   int *cntry0Tbl = vp->cntry0Tbl;
   int *cntry1Tbl = vp->cntry1Tbl;

   int uval0, y0val0, vval0, y1val0, uval1, y0val1, vval1, y1val1;
   int uval2, y0val2, vval2, y1val2, uval3, y0val3, vval3, y1val3;
   int uval4, y0val4, vval4, y1val4, uval5, y0val5, vval5, y1val5;

   int t0;

   // we've unpacked the srcrect, so adjust the begRow & endRow
   int rowsPerStripe = srchght / vp->nStripes;
   int begRow = iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = srchght;

   // downconvert the stripe 6 rows at a time
   int irow;
   for (irow=begRow;irow<endRow-6;irow+=6) {

      unprow0 = frmBuf1 + irow*pitch1 + KERNEL*4;
      unprow1 = unprow0 + pitch1;
      unprow2 = unprow1 + pitch1;
      unprow3 = unprow2 + pitch1;
      unprow4 = unprow3 + pitch1;
      unprow5 = unprow4 + pitch1;

      // dst0 points to the leftmost of 6 COLUMNS
      dst0 = frmBuf2 + KERNEL + irow;

      // generate the output pixel-pairs
      for (int j=0;j<dstwdth;j+=2) {

         KCoeff *iwuv = huvTbl[j];
         KCoeff *iwy0 = hy0Tbl[j];
         KCoeff *iwy1 = hy1Tbl[j];

         int cntruv = cntruvTbl[j];
         int cntry0 = cntry0Tbl[j];
         int cntry1 = cntry1Tbl[j];

         uval0  = 0;
         y0val0 = 0;
         vval0  = 0;
         y1val0 = 0;

         uval1  = 0;
         y0val1 = 0;
         vval1  = 0;
         y1val1 = 0;

         uval2  = 0;
         y0val2 = 0;
         vval2  = 0;
         y1val2 = 0;

         uval3  = 0;
         y0val3 = 0;
         vval3  = 0;
         y1val3 = 0;

         uval4  = 0;
         y0val4 = 0;
         vval4  = 0;
         y1val4 = 0;

         uval5  = 0;
         y0val5 = 0;
         vval5  = 0;
         y1val5 = 0;

         for (int k=-KERNEL;k<=KERNEL;k++) {

             int wgtuv = iwuv->wgt[MIDPT+k];
             int iu  = (cntruv+k)*4;
            uval0  += unprow0[iu  ]*wgtuv;
            uval1  += unprow1[iu  ]*wgtuv;
            uval2  += unprow2[iu  ]*wgtuv;
            uval3  += unprow3[iu  ]*wgtuv;
            uval4  += unprow4[iu  ]*wgtuv;
            uval5  += unprow5[iu  ]*wgtuv;

            vval0  += unprow0[iu+2]*wgtuv;
            vval1  += unprow1[iu+2]*wgtuv;
            vval2  += unprow2[iu+2]*wgtuv;
            vval3  += unprow3[iu+2]*wgtuv;
            vval4  += unprow4[iu+2]*wgtuv;
            vval5  += unprow5[iu+2]*wgtuv;

             int wgty0 = iwy0->wgt[MIDPT+k];
             int iy0 = (cntry0+k)*2 + 1;
            y0val0 += unprow0[iy0 ]*wgty0;
            y0val1 += unprow1[iy0 ]*wgty0;
            y0val2 += unprow2[iy0 ]*wgty0;
            y0val3 += unprow3[iy0 ]*wgty0;
            y0val4 += unprow4[iy0 ]*wgty0;
            y0val5 += unprow5[iy0 ]*wgty0;

             int wgty1 = iwy1->wgt[MIDPT+k];
             int iy1 = (cntry1+k)*2 + 1;

            y1val0 += unprow0[iy1 ]*wgty1;
            y1val1 += unprow1[iy1 ]*wgty1;
            y1val2 += unprow2[iy1 ]*wgty1;
            y1val3 += unprow3[iy1 ]*wgty1;
            y1val4 += unprow4[iy1 ]*wgty1;
            y1val5 += unprow5[iy1 ]*wgty1;
         }

         t0 = uval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         t0 = uval1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[1] = t0;
         t0 = uval2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[2] = t0;
         t0 = uval3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[3] = t0;
         t0 = uval4>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[4] = t0;
         t0 = uval5>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[5] = t0;
         dst0 += pitch2;

         t0 = y0val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         t0 = y0val1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[1] = t0;
         t0 = y0val2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[2] = t0;
         t0 = y0val3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[3] = t0;
         t0 = y0val4>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[4] = t0;
         t0 = y0val5>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[5] = t0;
         dst0 += pitch2;

         t0 = vval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         t0 = vval1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[1] = t0;
         t0 = vval2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[2] = t0;
         t0 = vval3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[3] = t0;
         t0 = vval4>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[4] = t0;
         t0 = vval5>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[5] = t0;
         dst0 += pitch2;

         t0 = y1val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         t0 = y1val1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[1] = t0;
         t0 = y1val2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[2] = t0;
         t0 = y1val3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[3] = t0;
         t0 = y1val4>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[4] = t0;
         t0 = y1val5>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[5] = t0;
         dst0 += pitch2;
      }
   }

   // downconvert the residual lines
   irow -=5;
   for (;irow<endRow;irow+=1) {

      unprow0 = frmBuf1 + irow*pitch1 + KERNEL*4;

      dst0 = frmBuf2 + KERNEL + irow;

      // generate the output pixel-pairs
      for (int j=0;j<dstwdth;j+=2) {

         KCoeff *iwuv = huvTbl[j];
         KCoeff *iwy0 = hy0Tbl[j];
         KCoeff *iwy1 = hy1Tbl[j];

         int cntruv = cntruvTbl[j];
         int cntry0 = cntry0Tbl[j];
         int cntry1 = cntry1Tbl[j];

         uval0  = 0;
         y0val0 = 0;
         vval0  = 0;
         y1val0 = 0;

         for (int k=-KERNEL;k<=KERNEL;k++) {

            int wgtuv = iwuv->wgt[MIDPT+k];
            int iuv = (cntruv+k)*4;
            uval0  += unprow0[iuv    ]*wgtuv;
            vval0  += unprow0[iuv + 2]*wgtuv;

            int wgty0 = iwy0->wgt[MIDPT+k];
            int iy0 = (cntry0+k)*2 + 1;
            y0val0 += unprow0[iy0    ]*wgty0;

            int wgty1 = iwy1->wgt[MIDPT+k];
            int iy1 = (cntry1+k)*2 + 1;
            y1val0 += unprow0[iy1    ]*wgty1;
         }

         // store the results COLUMNWISE
         t0 = uval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         dst0 += pitch2;
         t0 = y0val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         dst0 += pitch2;
         t0 = vval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         dst0 += pitch2;
         t0 = y1val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         dst0 += pitch2;
      }
   }

   // in frmBuf2 we now have, in order, a U-row, a Y-row, a V-row, a Y-row,
   // etc.., so that the original (horz-converted) rows form columns.
}

#undef KERNEL
#define KERNEL 4
void horizontalDownconvert_K4(void *v, int iJob)
{
   CDownConverter *vp = (CDownConverter *)v;

   int srchght = vp->srchght;
   int pitch1 = vp->pitch1;
   unsigned char *frmBuf1 = vp->frmBuf1;
   int pitch2 = vp->pitch2;
   unsigned char *frmBuf2 = vp->frmBuf2;
   int dstwdth = vp->dstwdth;

   /////////////////////////////////////////////////////////////////////////////
   //
   // HORZ Downconvert
   //
   // This generates a destination-resolution COLUMN from each row, setting the
   // stage for vertical downconvert. Row margins are added afterwards.

   unsigned char *unprow0, *unprow1, *unprow2, *unprow3, *unprow4, *unprow5;

   unsigned char *dst0;

   KCoeff **huvTbl = vp->huvTbl;
   KCoeff **hy0Tbl = vp->hy0Tbl;
   KCoeff **hy1Tbl = vp->hy1Tbl;

   int *cntruvTbl = vp->cntruvTbl;
   int *cntry0Tbl = vp->cntry0Tbl;
   int *cntry1Tbl = vp->cntry1Tbl;

   int uval0, y0val0, vval0, y1val0, uval1, y0val1, vval1, y1val1;
   int uval2, y0val2, vval2, y1val2, uval3, y0val3, vval3, y1val3;
   int uval4, y0val4, vval4, y1val4, uval5, y0val5, vval5, y1val5;

   int t0;

   // we've unpacked the srcrect, so adjust the begRow & endRow
   int rowsPerStripe = srchght / vp->nStripes;
   int begRow = iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = srchght;

   // downconvert the stripe 6 rows at a time
   int irow;
   for (irow=begRow;irow<endRow-6;irow+=6) {

      unprow0 = frmBuf1 + irow*pitch1 + KERNEL*4;
      unprow1 = unprow0 + pitch1;
      unprow2 = unprow1 + pitch1;
      unprow3 = unprow2 + pitch1;
      unprow4 = unprow3 + pitch1;
      unprow5 = unprow4 + pitch1;

      // dst0 points to the leftmost of 6 COLUMNS
      dst0 = frmBuf2 + KERNEL + irow;

      // generate the output pixel-pairs
      for (int j=0;j<dstwdth;j+=2) {

         KCoeff *iwuv = huvTbl[j];
         KCoeff *iwy0 = hy0Tbl[j];
         KCoeff *iwy1 = hy1Tbl[j];

         int cntruv = cntruvTbl[j];
         int cntry0 = cntry0Tbl[j];
         int cntry1 = cntry1Tbl[j];

         uval0  = 0;
         y0val0 = 0;
         vval0  = 0;
         y1val0 = 0;

         uval1  = 0;
         y0val1 = 0;
         vval1  = 0;
         y1val1 = 0;

         uval2  = 0;
         y0val2 = 0;
         vval2  = 0;
         y1val2 = 0;

         uval3  = 0;
         y0val3 = 0;
         vval3  = 0;
         y1val3 = 0;

         uval4  = 0;
         y0val4 = 0;
         vval4  = 0;
         y1val4 = 0;

         uval5  = 0;
         y0val5 = 0;
         vval5  = 0;
         y1val5 = 0;

         for (int k=-KERNEL;k<=KERNEL;k++) {

             int wgtuv = iwuv->wgt[MIDPT+k];
             int iu  = (cntruv+k)*4;
            uval0  += unprow0[iu  ]*wgtuv;
            uval1  += unprow1[iu  ]*wgtuv;
            uval2  += unprow2[iu  ]*wgtuv;
            uval3  += unprow3[iu  ]*wgtuv;
            uval4  += unprow4[iu  ]*wgtuv;
            uval5  += unprow5[iu  ]*wgtuv;

            vval0  += unprow0[iu+2]*wgtuv;
            vval1  += unprow1[iu+2]*wgtuv;
            vval2  += unprow2[iu+2]*wgtuv;
            vval3  += unprow3[iu+2]*wgtuv;
            vval4  += unprow4[iu+2]*wgtuv;
            vval5  += unprow5[iu+2]*wgtuv;

             int wgty0 = iwy0->wgt[MIDPT+k];
             int iy0 = (cntry0+k)*2 + 1;
            y0val0 += unprow0[iy0 ]*wgty0;
            y0val1 += unprow1[iy0 ]*wgty0;
            y0val2 += unprow2[iy0 ]*wgty0;
            y0val3 += unprow3[iy0 ]*wgty0;
            y0val4 += unprow4[iy0 ]*wgty0;
            y0val5 += unprow5[iy0 ]*wgty0;

             int wgty1 = iwy1->wgt[MIDPT+k];
             int iy1 = (cntry1+k)*2 + 1;

            y1val0 += unprow0[iy1 ]*wgty1;
            y1val1 += unprow1[iy1 ]*wgty1;
            y1val2 += unprow2[iy1 ]*wgty1;
            y1val3 += unprow3[iy1 ]*wgty1;
            y1val4 += unprow4[iy1 ]*wgty1;
            y1val5 += unprow5[iy1 ]*wgty1;
         }

         t0 = uval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         t0 = uval1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[1] = t0;
         t0 = uval2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[2] = t0;
         t0 = uval3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[3] = t0;
         t0 = uval4>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[4] = t0;
         t0 = uval5>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[5] = t0;
         dst0 += pitch2;

         t0 = y0val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         t0 = y0val1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[1] = t0;
         t0 = y0val2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[2] = t0;
         t0 = y0val3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[3] = t0;
         t0 = y0val4>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[4] = t0;
         t0 = y0val5>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[5] = t0;
         dst0 += pitch2;

         t0 = vval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         t0 = vval1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[1] = t0;
         t0 = vval2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[2] = t0;
         t0 = vval3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[3] = t0;
         t0 = vval4>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[4] = t0;
         t0 = vval5>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[5] = t0;
         dst0 += pitch2;

         t0 = y1val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         t0 = y1val1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[1] = t0;
         t0 = y1val2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[2] = t0;
         t0 = y1val3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[3] = t0;
         t0 = y1val4>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[4] = t0;
         t0 = y1val5>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[5] = t0;
         dst0 += pitch2;
      }
   }

   // downconvert the residual lines
   irow -=5;
   for (;irow<endRow;irow+=1) {

      unprow0 = frmBuf1 + irow*pitch1 + KERNEL*4;

      dst0 = frmBuf2 + KERNEL + irow;

      // generate the output pixel-pairs
      for (int j=0;j<dstwdth;j+=2) {

         KCoeff *iwuv = huvTbl[j];
         KCoeff *iwy0 = hy0Tbl[j];
         KCoeff *iwy1 = hy1Tbl[j];

         int cntruv = cntruvTbl[j];
         int cntry0 = cntry0Tbl[j];
         int cntry1 = cntry1Tbl[j];

         uval0  = 0;
         y0val0 = 0;
         vval0  = 0;
         y1val0 = 0;

         for (int k=-KERNEL;k<=KERNEL;k++) {

            int wgtuv = iwuv->wgt[MIDPT+k];
            int iuv = (cntruv+k)*4;
            uval0  += unprow0[iuv    ]*wgtuv;
            vval0  += unprow0[iuv + 2]*wgtuv;

            int wgty0 = iwy0->wgt[MIDPT+k];
            int iy0 = (cntry0+k)*2 + 1;
            y0val0 += unprow0[iy0    ]*wgty0;

            int wgty1 = iwy1->wgt[MIDPT+k];
            int iy1 = (cntry1+k)*2 + 1;
            y1val0 += unprow0[iy1    ]*wgty1;
         }

         // store the results COLUMNWISE
         t0 = uval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         dst0 += pitch2;
         t0 = y0val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         dst0 += pitch2;
         t0 = vval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         dst0 += pitch2;
         t0 = y1val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         dst0 += pitch2;
      }
   }

   // in frmBuf2 we now have, in order, a U-row, a Y-row, a V-row, a Y-row,
   // etc.., so that the original (horz-converted) rows form columns.
}

#undef KERNEL
#define KERNEL 5
void horizontalDownconvert_K5(void *v, int iJob)
{
   CDownConverter *vp = (CDownConverter *)v;

   int srchght = vp->srchght;
   int pitch1 = vp->pitch1;
   unsigned char *frmBuf1 = vp->frmBuf1;
   int pitch2 = vp->pitch2;
   unsigned char *frmBuf2 = vp->frmBuf2;
   int dstwdth = vp->dstwdth;

   /////////////////////////////////////////////////////////////////////////////
   //
   // HORZ Downconvert
   //
   // This generates a destination-resolution COLUMN from each row, setting the
   // stage for vertical downconvert. Row margins are added afterwards.

   unsigned char *unprow0, *unprow1, *unprow2, *unprow3, *unprow4, *unprow5;

   unsigned char *dst0;

   KCoeff **huvTbl = vp->huvTbl;
   KCoeff **hy0Tbl = vp->hy0Tbl;
   KCoeff **hy1Tbl = vp->hy1Tbl;

   int *cntruvTbl = vp->cntruvTbl;
   int *cntry0Tbl = vp->cntry0Tbl;
   int *cntry1Tbl = vp->cntry1Tbl;

   int uval0, y0val0, vval0, y1val0, uval1, y0val1, vval1, y1val1;
   int uval2, y0val2, vval2, y1val2, uval3, y0val3, vval3, y1val3;
   int uval4, y0val4, vval4, y1val4, uval5, y0val5, vval5, y1val5;

   int t0;

   // we've unpacked the srcrect, so adjust the begRow & endRow
   int rowsPerStripe = srchght / vp->nStripes;
   int begRow = iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = srchght;

   // downconvert the stripe 6 rows at a time
   int irow;
   for (irow=begRow;irow<endRow-6;irow+=6) {

      unprow0 = frmBuf1 + irow*pitch1 + KERNEL*4;
      unprow1 = unprow0 + pitch1;
      unprow2 = unprow1 + pitch1;
      unprow3 = unprow2 + pitch1;
      unprow4 = unprow3 + pitch1;
      unprow5 = unprow4 + pitch1;

      // dst0 points to the leftmost of 6 COLUMNS
      dst0 = frmBuf2 + KERNEL + irow;

      // generate the output pixel-pairs
      for (int j=0;j<dstwdth;j+=2) {

         KCoeff *iwuv = huvTbl[j];
         KCoeff *iwy0 = hy0Tbl[j];
         KCoeff *iwy1 = hy1Tbl[j];

         int cntruv = cntruvTbl[j];
         int cntry0 = cntry0Tbl[j];
         int cntry1 = cntry1Tbl[j];

         uval0  = 0;
         y0val0 = 0;
         vval0  = 0;
         y1val0 = 0;

         uval1  = 0;
         y0val1 = 0;
         vval1  = 0;
         y1val1 = 0;

         uval2  = 0;
         y0val2 = 0;
         vval2  = 0;
         y1val2 = 0;

         uval3  = 0;
         y0val3 = 0;
         vval3  = 0;
         y1val3 = 0;

         uval4  = 0;
         y0val4 = 0;
         vval4  = 0;
         y1val4 = 0;

         uval5  = 0;
         y0val5 = 0;
         vval5  = 0;
         y1val5 = 0;

         for (int k=-KERNEL;k<=KERNEL;k++) {

             int wgtuv = iwuv->wgt[MIDPT+k];
             int iu  = (cntruv+k)*4;
            uval0  += unprow0[iu  ]*wgtuv;
            uval1  += unprow1[iu  ]*wgtuv;
            uval2  += unprow2[iu  ]*wgtuv;
            uval3  += unprow3[iu  ]*wgtuv;
            uval4  += unprow4[iu  ]*wgtuv;
            uval5  += unprow5[iu  ]*wgtuv;

            vval0  += unprow0[iu+2]*wgtuv;
            vval1  += unprow1[iu+2]*wgtuv;
            vval2  += unprow2[iu+2]*wgtuv;
            vval3  += unprow3[iu+2]*wgtuv;
            vval4  += unprow4[iu+2]*wgtuv;
            vval5  += unprow5[iu+2]*wgtuv;

             int wgty0 = iwy0->wgt[MIDPT+k];
             int iy0 = (cntry0+k)*2 + 1;
            y0val0 += unprow0[iy0 ]*wgty0;
            y0val1 += unprow1[iy0 ]*wgty0;
            y0val2 += unprow2[iy0 ]*wgty0;
            y0val3 += unprow3[iy0 ]*wgty0;
            y0val4 += unprow4[iy0 ]*wgty0;
            y0val5 += unprow5[iy0 ]*wgty0;

             int wgty1 = iwy1->wgt[MIDPT+k];
             int iy1 = (cntry1+k)*2 + 1;

            y1val0 += unprow0[iy1 ]*wgty1;
            y1val1 += unprow1[iy1 ]*wgty1;
            y1val2 += unprow2[iy1 ]*wgty1;
            y1val3 += unprow3[iy1 ]*wgty1;
            y1val4 += unprow4[iy1 ]*wgty1;
            y1val5 += unprow5[iy1 ]*wgty1;
         }

         t0 = uval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         t0 = uval1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[1] = t0;
         t0 = uval2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[2] = t0;
         t0 = uval3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[3] = t0;
         t0 = uval4>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[4] = t0;
         t0 = uval5>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[5] = t0;
         dst0 += pitch2;

         t0 = y0val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         t0 = y0val1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[1] = t0;
         t0 = y0val2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[2] = t0;
         t0 = y0val3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[3] = t0;
         t0 = y0val4>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[4] = t0;
         t0 = y0val5>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[5] = t0;
         dst0 += pitch2;

         t0 = vval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         t0 = vval1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[1] = t0;
         t0 = vval2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[2] = t0;
         t0 = vval3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[3] = t0;
         t0 = vval4>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[4] = t0;
         t0 = vval5>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[5] = t0;
         dst0 += pitch2;

         t0 = y1val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         t0 = y1val1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[1] = t0;
         t0 = y1val2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[2] = t0;
         t0 = y1val3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[3] = t0;
         t0 = y1val4>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[4] = t0;
         t0 = y1val5>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[5] = t0;
         dst0 += pitch2;
      }
   }

   // downconvert the residual lines
   irow -=5;
   for (;irow<endRow;irow+=1) {

      unprow0 = frmBuf1 + irow*pitch1 + KERNEL*4;

      dst0 = frmBuf2 + KERNEL + irow;

      // generate the output pixel-pairs
      for (int j=0;j<dstwdth;j+=2) {

         KCoeff *iwuv = huvTbl[j];
         KCoeff *iwy0 = hy0Tbl[j];
         KCoeff *iwy1 = hy1Tbl[j];

         int cntruv = cntruvTbl[j];
         int cntry0 = cntry0Tbl[j];
         int cntry1 = cntry1Tbl[j];

         uval0  = 0;
         y0val0 = 0;
         vval0  = 0;
         y1val0 = 0;

         for (int k=-KERNEL;k<=KERNEL;k++) {

            int wgtuv = iwuv->wgt[MIDPT+k];
            int iuv = (cntruv+k)*4;
            uval0  += unprow0[iuv    ]*wgtuv;
            vval0  += unprow0[iuv + 2]*wgtuv;

            int wgty0 = iwy0->wgt[MIDPT+k];
            int iy0 = (cntry0+k)*2 + 1;
            y0val0 += unprow0[iy0    ]*wgty0;

            int wgty1 = iwy1->wgt[MIDPT+k];
            int iy1 = (cntry1+k)*2 + 1;
            y1val0 += unprow0[iy1    ]*wgty1;
         }

         // store the results COLUMNWISE
         t0 = uval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         dst0 += pitch2;
         t0 = y0val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         dst0 += pitch2;
         t0 = vval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         dst0 += pitch2;
         t0 = y1val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         dst0 += pitch2;
      }
   }

   // in frmBuf2 we now have, in order, a U-row, a Y-row, a V-row, a Y-row,
   // etc.., so that the original (horz-converted) rows form columns.
}

#undef KERNEL
#define KERNEL 6
void horizontalDownconvert_K6(void *v, int iJob)
{
   CDownConverter *vp = (CDownConverter *)v;

   int srchght = vp->srchght;
   int pitch1 = vp->pitch1;
   unsigned char *frmBuf1 = vp->frmBuf1;
   int pitch2 = vp->pitch2;
   unsigned char *frmBuf2 = vp->frmBuf2;
   int dstwdth = vp->dstwdth;

   /////////////////////////////////////////////////////////////////////////////
   //
   // HORZ Downconvert
   //
   // This generates a destination-resolution COLUMN from each row, setting the
   // stage for vertical downconvert. Row margins are added afterwards.

   unsigned char *unprow0, *unprow1, *unprow2, *unprow3, *unprow4, *unprow5;

   unsigned char *dst0;

   KCoeff **huvTbl = vp->huvTbl;
   KCoeff **hy0Tbl = vp->hy0Tbl;
   KCoeff **hy1Tbl = vp->hy1Tbl;

   int *cntruvTbl = vp->cntruvTbl;
   int *cntry0Tbl = vp->cntry0Tbl;
   int *cntry1Tbl = vp->cntry1Tbl;

   int uval0, y0val0, vval0, y1val0, uval1, y0val1, vval1, y1val1;
   int uval2, y0val2, vval2, y1val2, uval3, y0val3, vval3, y1val3;
   int uval4, y0val4, vval4, y1val4, uval5, y0val5, vval5, y1val5;

   int t0;

   // we've unpacked the srcrect, so adjust the begRow & endRow
   int rowsPerStripe = srchght / vp->nStripes;
   int begRow = iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = srchght;

   // downconvert the stripe 6 rows at a time
   int irow;
   for (irow=begRow;irow<endRow-6;irow+=6) {

      unprow0 = frmBuf1 + irow*pitch1 + KERNEL*4;
      unprow1 = unprow0 + pitch1;
      unprow2 = unprow1 + pitch1;
      unprow3 = unprow2 + pitch1;
      unprow4 = unprow3 + pitch1;
      unprow5 = unprow4 + pitch1;

      // dst0 points to the leftmost of 6 COLUMNS
      dst0 = frmBuf2 + KERNEL + irow;

      // generate the output pixel-pairs
      for (int j=0;j<dstwdth;j+=2) {

         KCoeff *iwuv = huvTbl[j];
         KCoeff *iwy0 = hy0Tbl[j];
         KCoeff *iwy1 = hy1Tbl[j];

         int cntruv = cntruvTbl[j];
         int cntry0 = cntry0Tbl[j];
         int cntry1 = cntry1Tbl[j];

         uval0  = 0;
         y0val0 = 0;
         vval0  = 0;
         y1val0 = 0;

         uval1  = 0;
         y0val1 = 0;
         vval1  = 0;
         y1val1 = 0;

         uval2  = 0;
         y0val2 = 0;
         vval2  = 0;
         y1val2 = 0;

         uval3  = 0;
         y0val3 = 0;
         vval3  = 0;
         y1val3 = 0;

         uval4  = 0;
         y0val4 = 0;
         vval4  = 0;
         y1val4 = 0;

         uval5  = 0;
         y0val5 = 0;
         vval5  = 0;
         y1val5 = 0;

         for (int k=-KERNEL;k<=KERNEL;k++) {

             int wgtuv = iwuv->wgt[MIDPT+k];
             int iu  = (cntruv+k)*4;
            uval0  += unprow0[iu  ]*wgtuv;
            uval1  += unprow1[iu  ]*wgtuv;
            uval2  += unprow2[iu  ]*wgtuv;
            uval3  += unprow3[iu  ]*wgtuv;
            uval4  += unprow4[iu  ]*wgtuv;
            uval5  += unprow5[iu  ]*wgtuv;

            vval0  += unprow0[iu+2]*wgtuv;
            vval1  += unprow1[iu+2]*wgtuv;
            vval2  += unprow2[iu+2]*wgtuv;
            vval3  += unprow3[iu+2]*wgtuv;
            vval4  += unprow4[iu+2]*wgtuv;
            vval5  += unprow5[iu+2]*wgtuv;

             int wgty0 = iwy0->wgt[MIDPT+k];
             int iy0 = (cntry0+k)*2 + 1;
            y0val0 += unprow0[iy0 ]*wgty0;
            y0val1 += unprow1[iy0 ]*wgty0;
            y0val2 += unprow2[iy0 ]*wgty0;
            y0val3 += unprow3[iy0 ]*wgty0;
            y0val4 += unprow4[iy0 ]*wgty0;
            y0val5 += unprow5[iy0 ]*wgty0;

             int wgty1 = iwy1->wgt[MIDPT+k];
             int iy1 = (cntry1+k)*2 + 1;

            y1val0 += unprow0[iy1 ]*wgty1;
            y1val1 += unprow1[iy1 ]*wgty1;
            y1val2 += unprow2[iy1 ]*wgty1;
            y1val3 += unprow3[iy1 ]*wgty1;
            y1val4 += unprow4[iy1 ]*wgty1;
            y1val5 += unprow5[iy1 ]*wgty1;
         }

         t0 = uval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         t0 = uval1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[1] = t0;
         t0 = uval2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[2] = t0;
         t0 = uval3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[3] = t0;
         t0 = uval4>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[4] = t0;
         t0 = uval5>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[5] = t0;
         dst0 += pitch2;

         t0 = y0val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         t0 = y0val1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[1] = t0;
         t0 = y0val2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[2] = t0;
         t0 = y0val3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[3] = t0;
         t0 = y0val4>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[4] = t0;
         t0 = y0val5>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[5] = t0;
         dst0 += pitch2;

         t0 = vval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         t0 = vval1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[1] = t0;
         t0 = vval2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[2] = t0;
         t0 = vval3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[3] = t0;
         t0 = vval4>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[4] = t0;
         t0 = vval5>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[5] = t0;
         dst0 += pitch2;

         t0 = y1val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         t0 = y1val1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[1] = t0;
         t0 = y1val2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[2] = t0;
         t0 = y1val3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[3] = t0;
         t0 = y1val4>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[4] = t0;
         t0 = y1val5>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[5] = t0;
         dst0 += pitch2;
      }
   }

   // downconvert the residual lines
   irow -=5;
   for (;irow<endRow;irow+=1) {

      unprow0 = frmBuf1 + irow*pitch1 + KERNEL*4;

      dst0 = frmBuf2 + KERNEL + irow;

      // generate the output pixel-pairs
      for (int j=0;j<dstwdth;j+=2) {

         KCoeff *iwuv = huvTbl[j];
         KCoeff *iwy0 = hy0Tbl[j];
         KCoeff *iwy1 = hy1Tbl[j];

         int cntruv = cntruvTbl[j];
         int cntry0 = cntry0Tbl[j];
         int cntry1 = cntry1Tbl[j];

         uval0  = 0;
         y0val0 = 0;
         vval0  = 0;
         y1val0 = 0;

         for (int k=-KERNEL;k<=KERNEL;k++) {

            int wgtuv = iwuv->wgt[MIDPT+k];
            int iuv = (cntruv+k)*4;
            uval0  += unprow0[iuv    ]*wgtuv;
            vval0  += unprow0[iuv + 2]*wgtuv;

            int wgty0 = iwy0->wgt[MIDPT+k];
            int iy0 = (cntry0+k)*2 + 1;
            y0val0 += unprow0[iy0    ]*wgty0;

            int wgty1 = iwy1->wgt[MIDPT+k];
            int iy1 = (cntry1+k)*2 + 1;
            y1val0 += unprow0[iy1    ]*wgty1;
         }

         // store the results COLUMNWISE
         t0 = uval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         dst0 += pitch2;
         t0 = y0val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         dst0 += pitch2;
         t0 = vval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         dst0 += pitch2;
         t0 = y1val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         dst0 += pitch2;
      }
   }

   // in frmBuf2 we now have, in order, a U-row, a Y-row, a V-row, a Y-row,
   // etc.., so that the original (horz-converted) rows form columns.
}

#undef KERNEL
#define KERNEL 7
void horizontalDownconvert_K7(void *v, int iJob)
{
   CDownConverter *vp = (CDownConverter *)v;

   int srchght = vp->srchght;
   int pitch1 = vp->pitch1;
   unsigned char *frmBuf1 = vp->frmBuf1;
   int pitch2 = vp->pitch2;
   unsigned char *frmBuf2 = vp->frmBuf2;
   int dstwdth = vp->dstwdth;

   /////////////////////////////////////////////////////////////////////////////
   //
   // HORZ Downconvert
   //
   // This generates a destination-resolution COLUMN from each row, setting the
   // stage for vertical downconvert. Row margins are added afterwards.

   unsigned char *unprow0, *unprow1, *unprow2, *unprow3, *unprow4, *unprow5;

   unsigned char *dst0;

   KCoeff **huvTbl = vp->huvTbl;
   KCoeff **hy0Tbl = vp->hy0Tbl;
   KCoeff **hy1Tbl = vp->hy1Tbl;

   int *cntruvTbl = vp->cntruvTbl;
   int *cntry0Tbl = vp->cntry0Tbl;
   int *cntry1Tbl = vp->cntry1Tbl;

   int uval0, y0val0, vval0, y1val0, uval1, y0val1, vval1, y1val1;
   int uval2, y0val2, vval2, y1val2, uval3, y0val3, vval3, y1val3;
   int uval4, y0val4, vval4, y1val4, uval5, y0val5, vval5, y1val5;

   int t0;

   // we've unpacked the srcrect, so adjust the begRow & endRow
   int rowsPerStripe = srchght / vp->nStripes;
   int begRow = iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = srchght;

   // downconvert the stripe 6 rows at a time
   int irow;
   for (irow=begRow;irow<endRow-6;irow+=6) {

      unprow0 = frmBuf1 + irow*pitch1 + KERNEL*4;
      unprow1 = unprow0 + pitch1;
      unprow2 = unprow1 + pitch1;
      unprow3 = unprow2 + pitch1;
      unprow4 = unprow3 + pitch1;
      unprow5 = unprow4 + pitch1;

      // dst0 points to the leftmost of 6 COLUMNS
      dst0 = frmBuf2 + KERNEL + irow;

      // generate the output pixel-pairs
      for (int j=0;j<dstwdth;j+=2) {

         KCoeff *iwuv = huvTbl[j];
         KCoeff *iwy0 = hy0Tbl[j];
         KCoeff *iwy1 = hy1Tbl[j];

         int cntruv = cntruvTbl[j];
         int cntry0 = cntry0Tbl[j];
         int cntry1 = cntry1Tbl[j];

         uval0  = 0;
         y0val0 = 0;
         vval0  = 0;
         y1val0 = 0;

         uval1  = 0;
         y0val1 = 0;
         vval1  = 0;
         y1val1 = 0;

         uval2  = 0;
         y0val2 = 0;
         vval2  = 0;
         y1val2 = 0;

         uval3  = 0;
         y0val3 = 0;
         vval3  = 0;
         y1val3 = 0;

         uval4  = 0;
         y0val4 = 0;
         vval4  = 0;
         y1val4 = 0;

         uval5  = 0;
         y0val5 = 0;
         vval5  = 0;
         y1val5 = 0;

         for (int k=-KERNEL;k<=KERNEL;k++) {

             int wgtuv = iwuv->wgt[MIDPT+k];
             int iu  = (cntruv+k)*4;
            uval0  += unprow0[iu  ]*wgtuv;
            uval1  += unprow1[iu  ]*wgtuv;
            uval2  += unprow2[iu  ]*wgtuv;
            uval3  += unprow3[iu  ]*wgtuv;
            uval4  += unprow4[iu  ]*wgtuv;
            uval5  += unprow5[iu  ]*wgtuv;

            vval0  += unprow0[iu+2]*wgtuv;
            vval1  += unprow1[iu+2]*wgtuv;
            vval2  += unprow2[iu+2]*wgtuv;
            vval3  += unprow3[iu+2]*wgtuv;
            vval4  += unprow4[iu+2]*wgtuv;
            vval5  += unprow5[iu+2]*wgtuv;

             int wgty0 = iwy0->wgt[MIDPT+k];
             int iy0 = (cntry0+k)*2 + 1;
            y0val0 += unprow0[iy0 ]*wgty0;
            y0val1 += unprow1[iy0 ]*wgty0;
            y0val2 += unprow2[iy0 ]*wgty0;
            y0val3 += unprow3[iy0 ]*wgty0;
            y0val4 += unprow4[iy0 ]*wgty0;
            y0val5 += unprow5[iy0 ]*wgty0;

             int wgty1 = iwy1->wgt[MIDPT+k];
             int iy1 = (cntry1+k)*2 + 1;

            y1val0 += unprow0[iy1 ]*wgty1;
            y1val1 += unprow1[iy1 ]*wgty1;
            y1val2 += unprow2[iy1 ]*wgty1;
            y1val3 += unprow3[iy1 ]*wgty1;
            y1val4 += unprow4[iy1 ]*wgty1;
            y1val5 += unprow5[iy1 ]*wgty1;
         }

         t0 = uval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         t0 = uval1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[1] = t0;
         t0 = uval2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[2] = t0;
         t0 = uval3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[3] = t0;
         t0 = uval4>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[4] = t0;
         t0 = uval5>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[5] = t0;
         dst0 += pitch2;

         t0 = y0val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         t0 = y0val1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[1] = t0;
         t0 = y0val2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[2] = t0;
         t0 = y0val3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[3] = t0;
         t0 = y0val4>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[4] = t0;
         t0 = y0val5>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[5] = t0;
         dst0 += pitch2;

         t0 = vval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         t0 = vval1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[1] = t0;
         t0 = vval2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[2] = t0;
         t0 = vval3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[3] = t0;
         t0 = vval4>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[4] = t0;
         t0 = vval5>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[5] = t0;
         dst0 += pitch2;

         t0 = y1val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         t0 = y1val1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[1] = t0;
         t0 = y1val2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[2] = t0;
         t0 = y1val3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[3] = t0;
         t0 = y1val4>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[4] = t0;
         t0 = y1val5>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[5] = t0;
         dst0 += pitch2;
      }
   }

   // downconvert the residual lines
   irow -=5;
   for (;irow<endRow;irow+=1) {

      unprow0 = frmBuf1 + irow*pitch1 + KERNEL*4;

      dst0 = frmBuf2 + KERNEL + irow;

      // generate the output pixel-pairs
      for (int j=0;j<dstwdth;j+=2) {

         KCoeff *iwuv = huvTbl[j];
         KCoeff *iwy0 = hy0Tbl[j];
         KCoeff *iwy1 = hy1Tbl[j];

         int cntruv = cntruvTbl[j];
         int cntry0 = cntry0Tbl[j];
         int cntry1 = cntry1Tbl[j];

         uval0  = 0;
         y0val0 = 0;
         vval0  = 0;
         y1val0 = 0;

         for (int k=-KERNEL;k<=KERNEL;k++) {

            int wgtuv = iwuv->wgt[MIDPT+k];
            int iuv = (cntruv+k)*4;
            uval0  += unprow0[iuv    ]*wgtuv;
            vval0  += unprow0[iuv + 2]*wgtuv;

            int wgty0 = iwy0->wgt[MIDPT+k];
            int iy0 = (cntry0+k)*2 + 1;
            y0val0 += unprow0[iy0    ]*wgty0;

            int wgty1 = iwy1->wgt[MIDPT+k];
            int iy1 = (cntry1+k)*2 + 1;
            y1val0 += unprow0[iy1    ]*wgty1;
         }

         // store the results COLUMNWISE
         t0 = uval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         dst0 += pitch2;
         t0 = y0val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         dst0 += pitch2;
         t0 = vval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         dst0 += pitch2;
         t0 = y1val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst0[0] = t0;
         dst0 += pitch2;
      }
   }

   // in frmBuf2 we now have, in order, a U-row, a Y-row, a V-row, a Y-row,
   // etc.., so that the original (horz-converted) rows form columns.
}

////////////////////////////////////////////////////////////////////////////////
//
// This is called from the third thread
//

#undef KERNEL
#define KERNEL 1
void verticalDownconvert_K1(void *v, int iJob)
{
   CDownConverter *vp = (CDownConverter *)v;

   int pitch2 = vp->pitch2;
   unsigned char *frmBuf2 = vp->frmBuf2;
   int dsthght = vp->dsthght;
   unsigned char *src, *dst;

   /////////////////////////////////////////////////////////////////////////////
   //
   // VERT Downconvert
   //
   // This generates a reduced-resolution column from each row, completing
   // the conversion.

   int rowsPerStripe = (vp->dstwdth/2) / vp->nStripes;
   int begRow = iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = (vp->dstwdth/2);

   unsigned char *urow0, *y0row0, *vrow0, *y1row0,
                 *urow1, *y0row1, *vrow1, *y1row1,
                 *urow2, *y0row2, *vrow2, *y1row2,
                 *urow3, *y0row3, *vrow3, *y1row3;

   // ptr to left of first lines of output
   unsigned char *pini[2];
   pini[0] = vp->dstbuf[vp->dstrect.top    %2] +
             ( vp->dstrect.top/2)    * SD_FRAME_PITCH + vp->dstrect.left*2;
   pini[1] = vp->dstbuf[(vp->dstrect.top+1)%2] +
             ((vp->dstrect.top+1)/2) * SD_FRAME_PITCH + vp->dstrect.left*2;
   unsigned char *p[2];

   KCoeff **vrtTbl = vp->vrtTbl;

   int *cntrVTbl = vp->cntrVTbl;

   int uval0, y0val0, vval0, y1val0, uval1, y0val1, vval1, y1val1;
   int uval2, y0val2, vval2, y1val2, uval3, y0val3, vval3, y1val3;

   int t0;

   // make sure the rows are terminated properly
   unsigned char *rowbeg = frmBuf2 + begRow*4*pitch2;
   unsigned char *rowend = rowbeg + pitch2 - KERNEL;
   for (int i=begRow*4;i<endRow*4;i++) {
      for (int j=0;j<KERNEL;j++) {
           rowbeg[j] = rowbeg[KERNEL];
           rowend[j] = rowend[-1];
      }
      rowbeg += pitch2;
      rowend += pitch2;
   }

   // each time through loop generates a column 4 pixel-pairs wide
   int irow;
   for (irow=begRow;irow<endRow-4;irow+=4) {

      urow0  = frmBuf2 + 4*irow*pitch2 + KERNEL;
      y0row0 = urow0  + pitch2;
      vrow0  = y0row0 + pitch2;
      y1row0 = vrow0  + pitch2;

      urow1  = y1row0 + pitch2;
      y0row1 = urow1  + pitch2;
      vrow1  = y0row1 + pitch2;
      y1row1 = vrow1  + pitch2;

      urow2  = y1row1 + pitch2;
      y0row2 = urow2  + pitch2;
      vrow2  = y0row2 + pitch2;
      y1row2 = vrow2  + pitch2;

      urow3  = y1row2 + pitch2;
      y0row3 = urow3  + pitch2;
      vrow3  = y0row3 + pitch2;
      y1row3 = vrow3  + pitch2;

      p[0] = pini[0] + 4*irow;
      p[1] = pini[1] + 4*irow;

      // generate the output
      for (int j=0;j<dsthght;j+=1) {

         KCoeff *iw = vrtTbl[j];

         int cntrv = cntrVTbl[j];

         uval0  = 0;
         y0val0 = 0;
         vval0  = 0;
         y1val0 = 0;

         uval1  = 0;
         y0val1 = 0;
         vval1  = 0;
         y1val1 = 0;

         uval2  = 0;
         y0val2 = 0;
         vval2  = 0;
         y1val2 = 0;

         uval3  = 0;
         y0val3 = 0;
         vval3  = 0;
         y1val3 = 0;

         for (int k=-KERNEL;k<=KERNEL;k++) {

            int wgtv = iw->wgt[MIDPT+k];

            int ivrt  = (cntrv+k);

            uval0   +=  urow0[ivrt]*wgtv;
            y0val0  += y0row0[ivrt]*wgtv;
            vval0   +=  vrow0[ivrt]*wgtv;
            y1val0  += y1row0[ivrt]*wgtv;

            uval1   +=  urow1[ivrt]*wgtv;
            y0val1  += y0row1[ivrt]*wgtv;
            vval1   +=  vrow1[ivrt]*wgtv;
            y1val1  += y1row1[ivrt]*wgtv;

            uval2   +=  urow2[ivrt]*wgtv;
            y0val2  += y0row2[ivrt]*wgtv;
            vval2   +=  vrow2[ivrt]*wgtv;
            y1val2  += y1row2[ivrt]*wgtv;

            uval3   +=  urow3[ivrt]*wgtv;
            y0val3  += y0row3[ivrt]*wgtv;
            vval3   +=  vrow3[ivrt]*wgtv;
            y1val3  += y1row3[ivrt]*wgtv;
         }

         dst = p[j&1];

         t0 = uval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[0] = t0;
         t0 = y0val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[1] = t0;
         t0 = vval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[2] = t0;
         t0 = y1val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[3] = t0;

         t0 = uval1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[4] = t0;
         t0 = y0val1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[5] = t0;
         t0 = vval1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[6] = t0;
         t0 = y1val1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[7] = t0;

         t0 = uval2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[8] = t0;
         t0 = y0val2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[9] = t0;
         t0 = vval2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[10] = t0;
         t0 = y1val2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[11] = t0;

         t0 = uval3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[12] = t0;
         t0 = y0val3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[13] = t0;
         t0 = vval3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[14] = t0;
         t0 = y1val3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[15] = t0;

         p[j&1] += SD_FRAME_PITCH;
      }
   }

   // downconvert the residual lines
   irow -= 3;
   for (;irow<endRow;irow+=1) {

      urow0  = frmBuf2 + 4*irow*pitch2 + KERNEL;
      y0row0 = urow0  + pitch2;
      vrow0  = y0row0 + pitch2;
      y1row0 = vrow0  + pitch2;

      p[0] = pini[0] + 4*irow;
      p[1] = pini[1] + 4*irow;

      // generate the output
      for (int j=0;j<dsthght;j+=1) {

         KCoeff *iw = vrtTbl[j];

         int cntrv = cntrVTbl[j];

         uval0  = 0;
         y0val0 = 0;
         vval0  = 0;
         y1val0 = 0;

         for (int k=-KERNEL;k<=KERNEL;k++) {

            int wgtv = iw->wgt[MIDPT+k];

            int ivrt  = (cntrv+k);

            uval0   +=  urow0[ivrt]*wgtv;
            y0val0  += y0row0[ivrt]*wgtv;
            vval0   +=  vrow0[ivrt]*wgtv;
            y1val0  += y1row0[ivrt]*wgtv;
         }

         dst = p[j&1];

         t0 = uval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[0] = t0;
         t0 = y0val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[1] = t0;
         t0 = vval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[2] = t0;
         t0 = y1val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[3] = t0;

         p[j&1] += SD_FRAME_PITCH;
      }
   }
}

#undef KERNEL
#define KERNEL 2
void verticalDownconvert_K2(void *v, int iJob)
{
   CDownConverter *vp = (CDownConverter *)v;

   int pitch2 = vp->pitch2;
   unsigned char *frmBuf2 = vp->frmBuf2;
   int dsthght = vp->dsthght;
   unsigned char *src, *dst;

   /////////////////////////////////////////////////////////////////////////////
   //
   // VERT Downconvert
   //
   // This generates a reduced-resolution column from each row, completing
   // the conversion.

   int rowsPerStripe = (vp->dstwdth/2) / vp->nStripes;
   int begRow = iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = (vp->dstwdth/2);

   unsigned char *urow0, *y0row0, *vrow0, *y1row0,
                 *urow1, *y0row1, *vrow1, *y1row1,
                 *urow2, *y0row2, *vrow2, *y1row2,
                 *urow3, *y0row3, *vrow3, *y1row3;

   // ptr to left of first lines of output
   unsigned char *pini[2];
   pini[0] = vp->dstbuf[vp->dstrect.top    %2] +
             ( vp->dstrect.top/2)    * SD_FRAME_PITCH + vp->dstrect.left*2;
   pini[1] = vp->dstbuf[(vp->dstrect.top+1)%2] +
             ((vp->dstrect.top+1)/2) * SD_FRAME_PITCH + vp->dstrect.left*2;
   unsigned char *p[2];

   KCoeff **vrtTbl = vp->vrtTbl;

   int *cntrVTbl = vp->cntrVTbl;

   int uval0, y0val0, vval0, y1val0, uval1, y0val1, vval1, y1val1;
   int uval2, y0val2, vval2, y1val2, uval3, y0val3, vval3, y1val3;

   int t0;

   // make sure the rows are terminated properly
   unsigned char *rowbeg = frmBuf2 + begRow*4*pitch2;
   unsigned char *rowend = rowbeg + pitch2 - KERNEL;
   for (int i=begRow*4;i<endRow*4;i++) {
      for (int j=0;j<KERNEL;j++) {
           rowbeg[j] = rowbeg[KERNEL];
           rowend[j] = rowend[-1];
      }
      rowbeg += pitch2;
      rowend += pitch2;
   }

   // each time through loop generates a column 4 pixel-pairs wide
   int irow;
   for (irow=begRow;irow<endRow-4;irow+=4) {

      urow0  = frmBuf2 + 4*irow*pitch2 + KERNEL;
      y0row0 = urow0  + pitch2;
      vrow0  = y0row0 + pitch2;
      y1row0 = vrow0  + pitch2;

      urow1  = y1row0 + pitch2;
      y0row1 = urow1  + pitch2;
      vrow1  = y0row1 + pitch2;
      y1row1 = vrow1  + pitch2;

      urow2  = y1row1 + pitch2;
      y0row2 = urow2  + pitch2;
      vrow2  = y0row2 + pitch2;
      y1row2 = vrow2  + pitch2;

      urow3  = y1row2 + pitch2;
      y0row3 = urow3  + pitch2;
      vrow3  = y0row3 + pitch2;
      y1row3 = vrow3  + pitch2;

      p[0] = pini[0] + 4*irow;
      p[1] = pini[1] + 4*irow;

      // generate the output
      for (int j=0;j<dsthght;j+=1) {

         KCoeff *iw = vrtTbl[j];

         int cntrv = cntrVTbl[j];

         uval0  = 0;
         y0val0 = 0;
         vval0  = 0;
         y1val0 = 0;

         uval1  = 0;
         y0val1 = 0;
         vval1  = 0;
         y1val1 = 0;

         uval2  = 0;
         y0val2 = 0;
         vval2  = 0;
         y1val2 = 0;

         uval3  = 0;
         y0val3 = 0;
         vval3  = 0;
         y1val3 = 0;

         for (int k=-KERNEL;k<=KERNEL;k++) {

            int wgtv = iw->wgt[MIDPT+k];

            int ivrt  = (cntrv+k);

            uval0   +=  urow0[ivrt]*wgtv;
            y0val0  += y0row0[ivrt]*wgtv;
            vval0   +=  vrow0[ivrt]*wgtv;
            y1val0  += y1row0[ivrt]*wgtv;

            uval1   +=  urow1[ivrt]*wgtv;
            y0val1  += y0row1[ivrt]*wgtv;
            vval1   +=  vrow1[ivrt]*wgtv;
            y1val1  += y1row1[ivrt]*wgtv;

            uval2   +=  urow2[ivrt]*wgtv;
            y0val2  += y0row2[ivrt]*wgtv;
            vval2   +=  vrow2[ivrt]*wgtv;
            y1val2  += y1row2[ivrt]*wgtv;

            uval3   +=  urow3[ivrt]*wgtv;
            y0val3  += y0row3[ivrt]*wgtv;
            vval3   +=  vrow3[ivrt]*wgtv;
            y1val3  += y1row3[ivrt]*wgtv;
         }

         dst = p[j&1];

         t0 = uval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[0] = t0;
         t0 = y0val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[1] = t0;
         t0 = vval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[2] = t0;
         t0 = y1val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[3] = t0;

         t0 = uval1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[4] = t0;
         t0 = y0val1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[5] = t0;
         t0 = vval1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[6] = t0;
         t0 = y1val1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[7] = t0;

         t0 = uval2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[8] = t0;
         t0 = y0val2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[9] = t0;
         t0 = vval2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[10] = t0;
         t0 = y1val2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[11] = t0;

         t0 = uval3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[12] = t0;
         t0 = y0val3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[13] = t0;
         t0 = vval3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[14] = t0;
         t0 = y1val3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[15] = t0;

         p[j&1] += SD_FRAME_PITCH;
      }
   }

   // downconvert the residual lines
   irow -= 3;
   for (;irow<endRow;irow+=1) {

      urow0  = frmBuf2 + 4*irow*pitch2 + KERNEL;
      y0row0 = urow0  + pitch2;
      vrow0  = y0row0 + pitch2;
      y1row0 = vrow0  + pitch2;

      p[0] = pini[0] + 4*irow;
      p[1] = pini[1] + 4*irow;

      // generate the output
      for (int j=0;j<dsthght;j+=1) {

         KCoeff *iw = vrtTbl[j];

         int cntrv = cntrVTbl[j];

         uval0  = 0;
         y0val0 = 0;
         vval0  = 0;
         y1val0 = 0;

         for (int k=-KERNEL;k<=KERNEL;k++) {

            int wgtv = iw->wgt[MIDPT+k];

            int ivrt  = (cntrv+k);

            uval0   +=  urow0[ivrt]*wgtv;
            y0val0  += y0row0[ivrt]*wgtv;
            vval0   +=  vrow0[ivrt]*wgtv;
            y1val0  += y1row0[ivrt]*wgtv;
         }

         dst = p[j&1];

         t0 = uval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[0] = t0;
         t0 = y0val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[1] = t0;
         t0 = vval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[2] = t0;
         t0 = y1val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[3] = t0;

         p[j&1] += SD_FRAME_PITCH;
      }
   }
}

#undef KERNEL
#define KERNEL 3
void verticalDownconvert_K3(void *v, int iJob)
{
   CDownConverter *vp = (CDownConverter *)v;

   int pitch2 = vp->pitch2;
   unsigned char *frmBuf2 = vp->frmBuf2;
   int dsthght = vp->dsthght;
   unsigned char *src, *dst;

   /////////////////////////////////////////////////////////////////////////////
   //
   // VERT Downconvert
   //
   // This generates a reduced-resolution column from each row, completing
   // the conversion.

   int rowsPerStripe = (vp->dstwdth/2) / vp->nStripes;
   int begRow = iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = (vp->dstwdth/2);

   unsigned char *urow0, *y0row0, *vrow0, *y1row0,
                 *urow1, *y0row1, *vrow1, *y1row1,
                 *urow2, *y0row2, *vrow2, *y1row2,
                 *urow3, *y0row3, *vrow3, *y1row3;

   // ptr to left of first lines of output
   unsigned char *pini[2];
   pini[0] = vp->dstbuf[vp->dstrect.top    %2] +
             ( vp->dstrect.top/2)    * SD_FRAME_PITCH + vp->dstrect.left*2;
   pini[1] = vp->dstbuf[(vp->dstrect.top+1)%2] +
             ((vp->dstrect.top+1)/2) * SD_FRAME_PITCH + vp->dstrect.left*2;
   unsigned char *p[2];

   KCoeff **vrtTbl = vp->vrtTbl;

   int *cntrVTbl = vp->cntrVTbl;

   int uval0, y0val0, vval0, y1val0, uval1, y0val1, vval1, y1val1;
   int uval2, y0val2, vval2, y1val2, uval3, y0val3, vval3, y1val3;

   int t0;

   // make sure the rows are terminated properly
   unsigned char *rowbeg = frmBuf2 + begRow*4*pitch2;
   unsigned char *rowend = rowbeg + pitch2 - KERNEL;
   for (int i=begRow*4;i<endRow*4;i++) {
      for (int j=0;j<KERNEL;j++) {
           rowbeg[j] = rowbeg[KERNEL];
           rowend[j] = rowend[-1];
      }
      rowbeg += pitch2;
      rowend += pitch2;
   }

   // each time through loop generates a column 4 pixel-pairs wide
   int irow;
   for (irow=begRow;irow<endRow-4;irow+=4) {

      urow0  = frmBuf2 + 4*irow*pitch2 + KERNEL;
      y0row0 = urow0  + pitch2;
      vrow0  = y0row0 + pitch2;
      y1row0 = vrow0  + pitch2;

      urow1  = y1row0 + pitch2;
      y0row1 = urow1  + pitch2;
      vrow1  = y0row1 + pitch2;
      y1row1 = vrow1  + pitch2;

      urow2  = y1row1 + pitch2;
      y0row2 = urow2  + pitch2;
      vrow2  = y0row2 + pitch2;
      y1row2 = vrow2  + pitch2;

      urow3  = y1row2 + pitch2;
      y0row3 = urow3  + pitch2;
      vrow3  = y0row3 + pitch2;
      y1row3 = vrow3  + pitch2;

      p[0] = pini[0] + 4*irow;
      p[1] = pini[1] + 4*irow;

      // generate the output
      for (int j=0;j<dsthght;j+=1) {

         KCoeff *iw = vrtTbl[j];

         int cntrv = cntrVTbl[j];

         uval0  = 0;
         y0val0 = 0;
         vval0  = 0;
         y1val0 = 0;

         uval1  = 0;
         y0val1 = 0;
         vval1  = 0;
         y1val1 = 0;

         uval2  = 0;
         y0val2 = 0;
         vval2  = 0;
         y1val2 = 0;

         uval3  = 0;
         y0val3 = 0;
         vval3  = 0;
         y1val3 = 0;

         for (int k=-KERNEL;k<=KERNEL;k++) {

            int wgtv = iw->wgt[MIDPT+k];

            int ivrt  = (cntrv+k);

            uval0   +=  urow0[ivrt]*wgtv;
            y0val0  += y0row0[ivrt]*wgtv;
            vval0   +=  vrow0[ivrt]*wgtv;
            y1val0  += y1row0[ivrt]*wgtv;

            uval1   +=  urow1[ivrt]*wgtv;
            y0val1  += y0row1[ivrt]*wgtv;
            vval1   +=  vrow1[ivrt]*wgtv;
            y1val1  += y1row1[ivrt]*wgtv;

            uval2   +=  urow2[ivrt]*wgtv;
            y0val2  += y0row2[ivrt]*wgtv;
            vval2   +=  vrow2[ivrt]*wgtv;
            y1val2  += y1row2[ivrt]*wgtv;

            uval3   +=  urow3[ivrt]*wgtv;
            y0val3  += y0row3[ivrt]*wgtv;
            vval3   +=  vrow3[ivrt]*wgtv;
            y1val3  += y1row3[ivrt]*wgtv;
         }

         dst = p[j&1];

         t0 = uval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[0] = t0;
         t0 = y0val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[1] = t0;
         t0 = vval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[2] = t0;
         t0 = y1val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[3] = t0;

         t0 = uval1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[4] = t0;
         t0 = y0val1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[5] = t0;
         t0 = vval1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[6] = t0;
         t0 = y1val1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[7] = t0;

         t0 = uval2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[8] = t0;
         t0 = y0val2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[9] = t0;
         t0 = vval2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[10] = t0;
         t0 = y1val2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[11] = t0;

         t0 = uval3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[12] = t0;
         t0 = y0val3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[13] = t0;
         t0 = vval3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[14] = t0;
         t0 = y1val3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[15] = t0;

         p[j&1] += SD_FRAME_PITCH;
      }
   }

   // downconvert the residual lines
   irow -= 3;
   for (;irow<endRow;irow+=1) {

      urow0  = frmBuf2 + 4*irow*pitch2 + KERNEL;
      y0row0 = urow0  + pitch2;
      vrow0  = y0row0 + pitch2;
      y1row0 = vrow0  + pitch2;

      p[0] = pini[0] + 4*irow;
      p[1] = pini[1] + 4*irow;

      // generate the output
      for (int j=0;j<dsthght;j+=1) {

         KCoeff *iw = vrtTbl[j];

         int cntrv = cntrVTbl[j];

         uval0  = 0;
         y0val0 = 0;
         vval0  = 0;
         y1val0 = 0;

         for (int k=-KERNEL;k<=KERNEL;k++) {

            int wgtv = iw->wgt[MIDPT+k];

            int ivrt  = (cntrv+k);

            uval0   +=  urow0[ivrt]*wgtv;
            y0val0  += y0row0[ivrt]*wgtv;
            vval0   +=  vrow0[ivrt]*wgtv;
            y1val0  += y1row0[ivrt]*wgtv;
         }

         dst = p[j&1];

         t0 = uval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[0] = t0;
         t0 = y0val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[1] = t0;
         t0 = vval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[2] = t0;
         t0 = y1val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[3] = t0;

         p[j&1] += SD_FRAME_PITCH;
      }
   }
}

#undef KERNEL
#define KERNEL 4
void verticalDownconvert_K4(void *v, int iJob)
{
   CDownConverter *vp = (CDownConverter *)v;

   int pitch2 = vp->pitch2;
   unsigned char *frmBuf2 = vp->frmBuf2;
   int dsthght = vp->dsthght;
   unsigned char *src, *dst;

   /////////////////////////////////////////////////////////////////////////////
   //
   // VERT Downconvert
   //
   // This generates a reduced-resolution column from each row, completing
   // the conversion.

   int rowsPerStripe = (vp->dstwdth/2) / vp->nStripes;
   int begRow = iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = (vp->dstwdth/2);

   unsigned char *urow0, *y0row0, *vrow0, *y1row0,
                 *urow1, *y0row1, *vrow1, *y1row1,
                 *urow2, *y0row2, *vrow2, *y1row2,
                 *urow3, *y0row3, *vrow3, *y1row3;

   // ptr to left of first lines of output
   unsigned char *pini[2];
   pini[0] = vp->dstbuf[vp->dstrect.top    %2] +
             ( vp->dstrect.top/2)    * SD_FRAME_PITCH + vp->dstrect.left*2;
   pini[1] = vp->dstbuf[(vp->dstrect.top+1)%2] +
             ((vp->dstrect.top+1)/2) * SD_FRAME_PITCH + vp->dstrect.left*2;
   unsigned char *p[2];

   KCoeff **vrtTbl = vp->vrtTbl;

   int *cntrVTbl = vp->cntrVTbl;

   int uval0, y0val0, vval0, y1val0, uval1, y0val1, vval1, y1val1;
   int uval2, y0val2, vval2, y1val2, uval3, y0val3, vval3, y1val3;

   int t0;

   // make sure the rows are terminated properly
   unsigned char *rowbeg = frmBuf2 + begRow*4*pitch2;
   unsigned char *rowend = rowbeg + pitch2 - KERNEL;
   for (int i=begRow*4;i<endRow*4;i++) {
      for (int j=0;j<KERNEL;j++) {
           rowbeg[j] = rowbeg[KERNEL];
           rowend[j] = rowend[-1];
      }
      rowbeg += pitch2;
      rowend += pitch2;
   }

   // each time through loop generates a column 4 pixel-pairs wide
   int irow;
   for (irow=begRow;irow<endRow-4;irow+=4) {

      urow0  = frmBuf2 + 4*irow*pitch2 + KERNEL;
      y0row0 = urow0  + pitch2;
      vrow0  = y0row0 + pitch2;
      y1row0 = vrow0  + pitch2;

      urow1  = y1row0 + pitch2;
      y0row1 = urow1  + pitch2;
      vrow1  = y0row1 + pitch2;
      y1row1 = vrow1  + pitch2;

      urow2  = y1row1 + pitch2;
      y0row2 = urow2  + pitch2;
      vrow2  = y0row2 + pitch2;
      y1row2 = vrow2  + pitch2;

      urow3  = y1row2 + pitch2;
      y0row3 = urow3  + pitch2;
      vrow3  = y0row3 + pitch2;
      y1row3 = vrow3  + pitch2;

      p[0] = pini[0] + 4*irow;
      p[1] = pini[1] + 4*irow;

      // generate the output
      for (int j=0;j<dsthght;j+=1) {

         KCoeff *iw = vrtTbl[j];

         int cntrv = cntrVTbl[j];

         uval0  = 0;
         y0val0 = 0;
         vval0  = 0;
         y1val0 = 0;

         uval1  = 0;
         y0val1 = 0;
         vval1  = 0;
         y1val1 = 0;

         uval2  = 0;
         y0val2 = 0;
         vval2  = 0;
         y1val2 = 0;

         uval3  = 0;
         y0val3 = 0;
         vval3  = 0;
         y1val3 = 0;

         for (int k=-KERNEL;k<=KERNEL;k++) {

            int wgtv = iw->wgt[MIDPT+k];

            int ivrt  = (cntrv+k);

            uval0   +=  urow0[ivrt]*wgtv;
            y0val0  += y0row0[ivrt]*wgtv;
            vval0   +=  vrow0[ivrt]*wgtv;
            y1val0  += y1row0[ivrt]*wgtv;

            uval1   +=  urow1[ivrt]*wgtv;
            y0val1  += y0row1[ivrt]*wgtv;
            vval1   +=  vrow1[ivrt]*wgtv;
            y1val1  += y1row1[ivrt]*wgtv;

            uval2   +=  urow2[ivrt]*wgtv;
            y0val2  += y0row2[ivrt]*wgtv;
            vval2   +=  vrow2[ivrt]*wgtv;
            y1val2  += y1row2[ivrt]*wgtv;

            uval3   +=  urow3[ivrt]*wgtv;
            y0val3  += y0row3[ivrt]*wgtv;
            vval3   +=  vrow3[ivrt]*wgtv;
            y1val3  += y1row3[ivrt]*wgtv;
         }

         dst = p[j&1];

         t0 = uval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[0] = t0;
         t0 = y0val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[1] = t0;
         t0 = vval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[2] = t0;
         t0 = y1val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[3] = t0;

         t0 = uval1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[4] = t0;
         t0 = y0val1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[5] = t0;
         t0 = vval1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[6] = t0;
         t0 = y1val1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[7] = t0;

         t0 = uval2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[8] = t0;
         t0 = y0val2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[9] = t0;
         t0 = vval2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[10] = t0;
         t0 = y1val2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[11] = t0;

         t0 = uval3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[12] = t0;
         t0 = y0val3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[13] = t0;
         t0 = vval3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[14] = t0;
         t0 = y1val3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[15] = t0;

         p[j&1] += SD_FRAME_PITCH;
      }
   }

   // downconvert the residual lines
   irow -= 3;
   for (;irow<endRow;irow+=1) {

      urow0  = frmBuf2 + 4*irow*pitch2 + KERNEL;
      y0row0 = urow0  + pitch2;
      vrow0  = y0row0 + pitch2;
      y1row0 = vrow0  + pitch2;

      p[0] = pini[0] + 4*irow;
      p[1] = pini[1] + 4*irow;

      // generate the output
      for (int j=0;j<dsthght;j+=1) {

         KCoeff *iw = vrtTbl[j];

         int cntrv = cntrVTbl[j];

         uval0  = 0;
         y0val0 = 0;
         vval0  = 0;
         y1val0 = 0;

         for (int k=-KERNEL;k<=KERNEL;k++) {

            int wgtv = iw->wgt[MIDPT+k];

             int ivrt  = (cntrv+k);

            uval0   +=  urow0[ivrt]*wgtv;
            y0val0  += y0row0[ivrt]*wgtv;
            vval0   +=  vrow0[ivrt]*wgtv;
            y1val0  += y1row0[ivrt]*wgtv;
         }

         dst = p[j&1];

         t0 = uval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[0] = t0;
         t0 = y0val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[1] = t0;
         t0 = vval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[2] = t0;
         t0 = y1val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[3] = t0;

         p[j&1] += SD_FRAME_PITCH;
      }
   }
}

#undef KERNEL
#define KERNEL 5
void verticalDownconvert_K5(void *v, int iJob)
{
   CDownConverter *vp = (CDownConverter *)v;

   int pitch2 = vp->pitch2;
   unsigned char *frmBuf2 = vp->frmBuf2;
   int dsthght = vp->dsthght;
   unsigned char *src, *dst;

   /////////////////////////////////////////////////////////////////////////////
   //
   // VERT Downconvert
   //
   // This generates a reduced-resolution column from each row, completing
   // the conversion.

   int rowsPerStripe = (vp->dstwdth/2) / vp->nStripes;
   int begRow = iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = (vp->dstwdth/2);

   unsigned char *urow0, *y0row0, *vrow0, *y1row0,
                 *urow1, *y0row1, *vrow1, *y1row1,
                 *urow2, *y0row2, *vrow2, *y1row2,
                 *urow3, *y0row3, *vrow3, *y1row3;

   // ptr to left of first lines of output
   unsigned char *pini[2];
   pini[0] = vp->dstbuf[vp->dstrect.top    %2] +
             ( vp->dstrect.top/2)    * SD_FRAME_PITCH + vp->dstrect.left*2;
   pini[1] = vp->dstbuf[(vp->dstrect.top+1)%2] +
             ((vp->dstrect.top+1)/2) * SD_FRAME_PITCH + vp->dstrect.left*2;
   unsigned char *p[2];

   KCoeff **vrtTbl = vp->vrtTbl;

   int *cntrVTbl = vp->cntrVTbl;

   int uval0, y0val0, vval0, y1val0, uval1, y0val1, vval1, y1val1;
   int uval2, y0val2, vval2, y1val2, uval3, y0val3, vval3, y1val3;

   int t0;

   // make sure the rows are terminated properly
   unsigned char *rowbeg = frmBuf2 + begRow*4*pitch2;
   unsigned char *rowend = rowbeg + pitch2 - KERNEL;
   for (int i=begRow*4;i<endRow*4;i++) {
      for (int j=0;j<KERNEL;j++) {
           rowbeg[j] = rowbeg[KERNEL];
           rowend[j] = rowend[-1];
      }
      rowbeg += pitch2;
      rowend += pitch2;
   }

   // each time through loop generates a column 4 pixel-pairs wide
   int irow;
   for (irow=begRow;irow<endRow-4;irow+=4) {

      urow0  = frmBuf2 + 4*irow*pitch2 + KERNEL;
      y0row0 = urow0  + pitch2;
      vrow0  = y0row0 + pitch2;
      y1row0 = vrow0  + pitch2;

      urow1  = y1row0 + pitch2;
      y0row1 = urow1  + pitch2;
      vrow1  = y0row1 + pitch2;
      y1row1 = vrow1  + pitch2;

      urow2  = y1row1 + pitch2;
      y0row2 = urow2  + pitch2;
      vrow2  = y0row2 + pitch2;
      y1row2 = vrow2  + pitch2;

      urow3  = y1row2 + pitch2;
      y0row3 = urow3  + pitch2;
      vrow3  = y0row3 + pitch2;
      y1row3 = vrow3  + pitch2;

      p[0] = pini[0] + 4*irow;
      p[1] = pini[1] + 4*irow;

      // generate the output
      for (int j=0;j<dsthght;j+=1) {

         KCoeff *iw = vrtTbl[j];

         int cntrv = cntrVTbl[j];

         uval0  = 0;
         y0val0 = 0;
         vval0  = 0;
         y1val0 = 0;

         uval1  = 0;
         y0val1 = 0;
         vval1  = 0;
         y1val1 = 0;

         uval2  = 0;
         y0val2 = 0;
         vval2  = 0;
         y1val2 = 0;

         uval3  = 0;
         y0val3 = 0;
         vval3  = 0;
         y1val3 = 0;

         for (int k=-KERNEL;k<=KERNEL;k++) {

            int wgtv = iw->wgt[MIDPT+k];

            int ivrt  = (cntrv+k);

            uval0   +=  urow0[ivrt]*wgtv;
            y0val0  += y0row0[ivrt]*wgtv;
            vval0   +=  vrow0[ivrt]*wgtv;
            y1val0  += y1row0[ivrt]*wgtv;

            uval1   +=  urow1[ivrt]*wgtv;
            y0val1  += y0row1[ivrt]*wgtv;
            vval1   +=  vrow1[ivrt]*wgtv;
            y1val1  += y1row1[ivrt]*wgtv;

            uval2   +=  urow2[ivrt]*wgtv;
            y0val2  += y0row2[ivrt]*wgtv;
            vval2   +=  vrow2[ivrt]*wgtv;
            y1val2  += y1row2[ivrt]*wgtv;

            uval3   +=  urow3[ivrt]*wgtv;
            y0val3  += y0row3[ivrt]*wgtv;
            vval3   +=  vrow3[ivrt]*wgtv;
            y1val3  += y1row3[ivrt]*wgtv;
         }

         dst = p[j&1];

         t0 = uval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[0] = t0;
         t0 = y0val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[1] = t0;
         t0 = vval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[2] = t0;
         t0 = y1val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[3] = t0;

         t0 = uval1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[4] = t0;
         t0 = y0val1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[5] = t0;
         t0 = vval1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[6] = t0;
         t0 = y1val1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[7] = t0;

         t0 = uval2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[8] = t0;
         t0 = y0val2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[9] = t0;
         t0 = vval2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[10] = t0;
         t0 = y1val2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[11] = t0;

         t0 = uval3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[12] = t0;
         t0 = y0val3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[13] = t0;
         t0 = vval3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[14] = t0;
         t0 = y1val3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[15] = t0;

         p[j&1] += SD_FRAME_PITCH;
      }
   }

   // downconvert the residual lines
   irow -= 3;
   for (;irow<endRow;irow+=1) {

      urow0  = frmBuf2 + 4*irow*pitch2 + KERNEL;
      y0row0 = urow0  + pitch2;
      vrow0  = y0row0 + pitch2;
      y1row0 = vrow0  + pitch2;

      p[0] = pini[0] + 4*irow;
      p[1] = pini[1] + 4*irow;

      // generate the output
      for (int j=0;j<dsthght;j+=1) {

         KCoeff *iw = vrtTbl[j];

         int cntrv = cntrVTbl[j];

         uval0  = 0;
         y0val0 = 0;
         vval0  = 0;
         y1val0 = 0;

         for (int k=-KERNEL;k<=KERNEL;k++) {

            int wgtv = iw->wgt[MIDPT+k];

            int ivrt  = (cntrv+k);

            uval0   +=  urow0[ivrt]*wgtv;
            y0val0  += y0row0[ivrt]*wgtv;
            vval0   +=  vrow0[ivrt]*wgtv;
            y1val0  += y1row0[ivrt]*wgtv;
         }

         dst = p[j&1];

         t0 = uval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[0] = t0;
         t0 = y0val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[1] = t0;
         t0 = vval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[2] = t0;
         t0 = y1val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[3] = t0;

         p[j&1] += SD_FRAME_PITCH;
      }
   }
}

#undef KERNEL
#define KERNEL 6
void verticalDownconvert_K6(void *v, int iJob)
{
   CDownConverter *vp = (CDownConverter *)v;

   int pitch2 = vp->pitch2;
   unsigned char *frmBuf2 = vp->frmBuf2;
   int dsthght = vp->dsthght;
   unsigned char *src, *dst;

   /////////////////////////////////////////////////////////////////////////////
   //
   // VERT Downconvert
   //
   // This generates a reduced-resolution column from each row, completing
   // the conversion.

   int rowsPerStripe = (vp->dstwdth/2) / vp->nStripes;
   int begRow = iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = (vp->dstwdth/2);

   unsigned char *urow0, *y0row0, *vrow0, *y1row0,
                 *urow1, *y0row1, *vrow1, *y1row1,
                 *urow2, *y0row2, *vrow2, *y1row2,
                 *urow3, *y0row3, *vrow3, *y1row3;

   // ptr to left of first lines of output
   unsigned char *pini[2];
   pini[0] = vp->dstbuf[vp->dstrect.top    %2] +
             ( vp->dstrect.top/2)    * SD_FRAME_PITCH + vp->dstrect.left*2;
   pini[1] = vp->dstbuf[(vp->dstrect.top+1)%2] +
             ((vp->dstrect.top+1)/2) * SD_FRAME_PITCH + vp->dstrect.left*2;
   unsigned char *p[2];

   KCoeff **vrtTbl = vp->vrtTbl;

   int *cntrVTbl = vp->cntrVTbl;

   int uval0, y0val0, vval0, y1val0, uval1, y0val1, vval1, y1val1;
   int uval2, y0val2, vval2, y1val2, uval3, y0val3, vval3, y1val3;

   int t0;

   // make sure the rows are terminated properly
   unsigned char *rowbeg = frmBuf2 + begRow*4*pitch2;
   unsigned char *rowend = rowbeg + pitch2 - KERNEL;
   for (int i=begRow*4;i<endRow*4;i++) {
      for (int j=0;j<KERNEL;j++) {
           rowbeg[j] = rowbeg[KERNEL];
           rowend[j] = rowend[-1];
      }
      rowbeg += pitch2;
      rowend += pitch2;
   }

   // each time through loop generates a column 4 pixel-pairs wide
   int irow;
   for (irow=begRow;irow<endRow-4;irow+=4) {

      urow0  = frmBuf2 + 4*irow*pitch2 + KERNEL;
      y0row0 = urow0  + pitch2;
      vrow0  = y0row0 + pitch2;
      y1row0 = vrow0  + pitch2;

      urow1  = y1row0 + pitch2;
      y0row1 = urow1  + pitch2;
      vrow1  = y0row1 + pitch2;
      y1row1 = vrow1  + pitch2;

      urow2  = y1row1 + pitch2;
      y0row2 = urow2  + pitch2;
      vrow2  = y0row2 + pitch2;
      y1row2 = vrow2  + pitch2;

      urow3  = y1row2 + pitch2;
      y0row3 = urow3  + pitch2;
      vrow3  = y0row3 + pitch2;
      y1row3 = vrow3  + pitch2;

      p[0] = pini[0] + 4*irow;
      p[1] = pini[1] + 4*irow;

      // generate the output
      for (int j=0;j<dsthght;j+=1) {

         KCoeff *iw = vrtTbl[j];

         int cntrv = cntrVTbl[j];

         uval0  = 0;
         y0val0 = 0;
         vval0  = 0;
         y1val0 = 0;

         uval1  = 0;
         y0val1 = 0;
         vval1  = 0;
         y1val1 = 0;

         uval2  = 0;
         y0val2 = 0;
         vval2  = 0;
         y1val2 = 0;

         uval3  = 0;
         y0val3 = 0;
         vval3  = 0;
         y1val3 = 0;

         for (int k=-KERNEL;k<=KERNEL;k++) {

            int wgtv = iw->wgt[MIDPT+k];

            int ivrt  = (cntrv+k);

            uval0   +=  urow0[ivrt]*wgtv;
            y0val0  += y0row0[ivrt]*wgtv;
            vval0   +=  vrow0[ivrt]*wgtv;
            y1val0  += y1row0[ivrt]*wgtv;

            uval1   +=  urow1[ivrt]*wgtv;
            y0val1  += y0row1[ivrt]*wgtv;
            vval1   +=  vrow1[ivrt]*wgtv;
            y1val1  += y1row1[ivrt]*wgtv;

            uval2   +=  urow2[ivrt]*wgtv;
            y0val2  += y0row2[ivrt]*wgtv;
            vval2   +=  vrow2[ivrt]*wgtv;
            y1val2  += y1row2[ivrt]*wgtv;

            uval3   +=  urow3[ivrt]*wgtv;
            y0val3  += y0row3[ivrt]*wgtv;
            vval3   +=  vrow3[ivrt]*wgtv;
            y1val3  += y1row3[ivrt]*wgtv;
         }

         dst = p[j&1];

         t0 = uval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[0] = t0;
         t0 = y0val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[1] = t0;
         t0 = vval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[2] = t0;
         t0 = y1val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[3] = t0;

         t0 = uval1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[4] = t0;
         t0 = y0val1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[5] = t0;
         t0 = vval1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[6] = t0;
         t0 = y1val1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[7] = t0;

         t0 = uval2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[8] = t0;
         t0 = y0val2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[9] = t0;
         t0 = vval2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[10] = t0;
         t0 = y1val2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[11] = t0;

         t0 = uval3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[12] = t0;
         t0 = y0val3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[13] = t0;
         t0 = vval3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[14] = t0;
         t0 = y1val3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[15] = t0;

         p[j&1] += SD_FRAME_PITCH;
      }
   }

   // downconvert the residual lines
   irow -= 3;
   for (;irow<endRow;irow+=1) {

      urow0  = frmBuf2 + 4*irow*pitch2 + KERNEL;
      y0row0 = urow0  + pitch2;
      vrow0  = y0row0 + pitch2;
      y1row0 = vrow0  + pitch2;

      p[0] = pini[0] + 4*irow;
      p[1] = pini[1] + 4*irow;

      // generate the output
      for (int j=0;j<dsthght;j+=1) {

         KCoeff *iw = vrtTbl[j];

         int cntrv = cntrVTbl[j];

         uval0  = 0;
         y0val0 = 0;
         vval0  = 0;
         y1val0 = 0;

         for (int k=-KERNEL;k<=KERNEL;k++) {

            int wgtv = iw->wgt[MIDPT+k];

            int ivrt  = (cntrv+k);

            uval0   +=  urow0[ivrt]*wgtv;
            y0val0  += y0row0[ivrt]*wgtv;
            vval0   +=  vrow0[ivrt]*wgtv;
            y1val0  += y1row0[ivrt]*wgtv;
         }

         dst = p[j&1];

         t0 = uval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[0] = t0;
         t0 = y0val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[1] = t0;
         t0 = vval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[2] = t0;
         t0 = y1val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[3] = t0;

         p[j&1] += SD_FRAME_PITCH;
      }
   }
}

#undef KERNEL
#define KERNEL 7
void verticalDownconvert_K7(void *v, int iJob)
{
   CDownConverter *vp = (CDownConverter *)v;

   int pitch2 = vp->pitch2;
   unsigned char *frmBuf2 = vp->frmBuf2;
   int dsthght = vp->dsthght;
   unsigned char *src, *dst;

   /////////////////////////////////////////////////////////////////////////////
   //
   // VERT Downconvert
   //
   // This generates a reduced-resolution column from each row, completing
   // the conversion.

   int rowsPerStripe = (vp->dstwdth/2) / vp->nStripes;
   int begRow = iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = (vp->dstwdth/2);

   unsigned char *urow0, *y0row0, *vrow0, *y1row0,
                 *urow1, *y0row1, *vrow1, *y1row1,
                 *urow2, *y0row2, *vrow2, *y1row2,
                 *urow3, *y0row3, *vrow3, *y1row3;

   // ptr to left of first lines of output
   unsigned char *pini[2];
   pini[0] = vp->dstbuf[vp->dstrect.top    %2] +
             ( vp->dstrect.top/2)    * SD_FRAME_PITCH + vp->dstrect.left*2;
   pini[1] = vp->dstbuf[(vp->dstrect.top+1)%2] +
             ((vp->dstrect.top+1)/2) * SD_FRAME_PITCH + vp->dstrect.left*2;
   unsigned char *p[2];

   KCoeff **vrtTbl = vp->vrtTbl;

   int *cntrVTbl = vp->cntrVTbl;

   int uval0, y0val0, vval0, y1val0, uval1, y0val1, vval1, y1val1;
   int uval2, y0val2, vval2, y1val2, uval3, y0val3, vval3, y1val3;

   int t0;

   // make sure the rows are terminated properly
   unsigned char *rowbeg = frmBuf2 + begRow*4*pitch2;
   unsigned char *rowend = rowbeg + pitch2 - KERNEL;
   for (int i=begRow*4;i<endRow*4;i++) {
      for (int j=0;j<KERNEL;j++) {
           rowbeg[j] = rowbeg[KERNEL];
           rowend[j] = rowend[-1];
      }
      rowbeg += pitch2;
      rowend += pitch2;
   }

   // each time through loop generates a column 4 pixel-pairs wide
   int irow;
   for (irow=begRow;irow<endRow-4;irow+=4) {

      urow0  = frmBuf2 + 4*irow*pitch2 + KERNEL;
      y0row0 = urow0  + pitch2;
      vrow0  = y0row0 + pitch2;
      y1row0 = vrow0  + pitch2;

      urow1  = y1row0 + pitch2;
      y0row1 = urow1  + pitch2;
      vrow1  = y0row1 + pitch2;
      y1row1 = vrow1  + pitch2;

      urow2  = y1row1 + pitch2;
      y0row2 = urow2  + pitch2;
      vrow2  = y0row2 + pitch2;
      y1row2 = vrow2  + pitch2;

      urow3  = y1row2 + pitch2;
      y0row3 = urow3  + pitch2;
      vrow3  = y0row3 + pitch2;
      y1row3 = vrow3  + pitch2;

      p[0] = pini[0] + 4*irow;
      p[1] = pini[1] + 4*irow;

      // generate the output
      for (int j=0;j<dsthght;j+=1) {

         KCoeff *iw = vrtTbl[j];

         int cntrv = cntrVTbl[j];

         uval0  = 0;
         y0val0 = 0;
         vval0  = 0;
         y1val0 = 0;

         uval1  = 0;
         y0val1 = 0;
         vval1  = 0;
         y1val1 = 0;

         uval2  = 0;
         y0val2 = 0;
         vval2  = 0;
         y1val2 = 0;

         uval3  = 0;
         y0val3 = 0;
         vval3  = 0;
         y1val3 = 0;

         for (int k=-KERNEL;k<=KERNEL;k++) {

            int wgtv = iw->wgt[MIDPT+k];

            int ivrt  = (cntrv+k);

            uval0   +=  urow0[ivrt]*wgtv;
            y0val0  += y0row0[ivrt]*wgtv;
            vval0   +=  vrow0[ivrt]*wgtv;
            y1val0  += y1row0[ivrt]*wgtv;

            uval1   +=  urow1[ivrt]*wgtv;
            y0val1  += y0row1[ivrt]*wgtv;
            vval1   +=  vrow1[ivrt]*wgtv;
            y1val1  += y1row1[ivrt]*wgtv;

            uval2   +=  urow2[ivrt]*wgtv;
            y0val2  += y0row2[ivrt]*wgtv;
            vval2   +=  vrow2[ivrt]*wgtv;
            y1val2  += y1row2[ivrt]*wgtv;

            uval3   +=  urow3[ivrt]*wgtv;
            y0val3  += y0row3[ivrt]*wgtv;
            vval3   +=  vrow3[ivrt]*wgtv;
            y1val3  += y1row3[ivrt]*wgtv;
         }

         dst = p[j&1];

         t0 = uval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[0] = t0;
         t0 = y0val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[1] = t0;
         t0 = vval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[2] = t0;
         t0 = y1val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[3] = t0;

         t0 = uval1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[4] = t0;
         t0 = y0val1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[5] = t0;
         t0 = vval1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[6] = t0;
         t0 = y1val1>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[7] = t0;

         t0 = uval2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[8] = t0;
         t0 = y0val2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[9] = t0;
         t0 = vval2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[10] = t0;
         t0 = y1val2>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[11] = t0;

         t0 = uval3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[12] = t0;
         t0 = y0val3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[13] = t0;
         t0 = vval3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[14] = t0;
         t0 = y1val3>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[15] = t0;

         p[j&1] += SD_FRAME_PITCH;
      }
   }

   // downconvert the residual lines
   irow -= 3;
   for (;irow<endRow;irow+=1) {

      urow0  = frmBuf2 + 4*irow*pitch2 + KERNEL;
      y0row0 = urow0  + pitch2;
      vrow0  = y0row0 + pitch2;
      y1row0 = vrow0  + pitch2;

      p[0] = pini[0] + 4*irow;
      p[1] = pini[1] + 4*irow;

      // generate the output
      for (int j=0;j<dsthght;j+=1) {

         KCoeff *iw = vrtTbl[j];

         int cntrv = cntrVTbl[j];

         uval0  = 0;
         y0val0 = 0;
         vval0  = 0;
         y1val0 = 0;

         for (int k=-KERNEL;k<=KERNEL;k++) {

            int wgtv = iw->wgt[MIDPT+k];

            int ivrt  = (cntrv+k);

            uval0   +=  urow0[ivrt]*wgtv;
            y0val0  += y0row0[ivrt]*wgtv;
            vval0   +=  vrow0[ivrt]*wgtv;
            y1val0  += y1row0[ivrt]*wgtv;
         }

         dst = p[j&1];

         t0 = uval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[0] = t0;
         t0 = y0val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[1] = t0;
         t0 = vval0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[2] = t0;
         t0 = y1val0>>WGTSHFT;
         if (t0 < 0) t0 = 0;
         if (t0 > 255) t0 = 255;
         dst[3] = t0;

         p[j&1] += SD_FRAME_PITCH;
      }
   }
}


