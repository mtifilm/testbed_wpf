// MaskToolRegions.cpp: implementation of the CMaskToolRegionList class.
//
/*
$Header: /usr/local/filmroot/imgTool/source/MaskToolRegions.cpp,v 1.6.2.4 2009/02/06 20:27:24 mbraca Exp $
*/
///////////////////////////////////////////////////////////////////////////////

#include "MaskToolRegions.h"
#include <typeinfo>


///////////////////////////////////////////////////////////////////////////////
// Constructor/Destructor
///////////////////////////////////////////////////////////////////////////////


CMaskToolRegion::CMaskToolRegion()
 : isSelected(false),
#ifdef USE_PER_REGION_INCLUSION_FLAGS
   inclusionFlag(true),
#endif
   hiddenFlag(false)
{
   // NB: Derived types must set the boundingBox
}

// Copy Constructor
CMaskToolRegion::CMaskToolRegion(const CMaskToolRegion &src)
 : isSelected(src.isSelected),
#ifdef USE_PER_REGION_INCLUSION_FLAGS
   inclusionFlag(src.inclusionFlag),
#endif
   hiddenFlag(false),
   _borderWidth(src.GetBorderWidth()),
   _blendSlew(src.GetBlendSlew())
{
   // NB: Derived types must set the boundingBox
}

CMaskToolRegion::~CMaskToolRegion()
{
}

// ---------------------------------------------------------------------------

CMaskToolRegion& CMaskToolRegion::operator=(const CMaskToolRegion &rhs)
{
   if (this == &rhs)
      return *this;  // avoid copying over self

   isSelected = rhs.isSelected;
#ifdef USE_PER_REGION_INCLUSION_FLAGS
   inclusionFlag = rhs.inclusionFlag;
#endif
   hiddenFlag = rhs.hiddenFlag;
   _borderWidth = rhs.GetBorderWidth();
   _blendSlew = rhs.GetBlendSlew();

   return *this;
}

int CMaskToolRegion::GetTCode(int x, int y)
{
   int retVal = 0;
   if (x < 0) retVal |= LEFT;
   if (y < 0) retVal |= ABOVE;
   return retVal;
}

int CMaskToolRegion::GetBorderWidth() const
{
   return _borderWidth;
}

RECT CMaskToolRegion::GetBoundingBox() const
{
   return boundingBox;
}

double CMaskToolRegion::GetBlendSlew() const
{
   return _blendSlew;
}

#ifdef USE_PER_REGION_INCLUSION_FLAGS
bool CMaskToolRegion::GetInclusionFlag() const
{
   return inclusionFlag;
}
#endif

bool CMaskToolRegion::GetHiddenFlag() const
{
   return hiddenFlag;
}

#ifdef USE_PER_REGION_INCLUSION_FLAGS
void CMaskToolRegion::SetInclusionFlag(bool newInclusionFlag)
{
   inclusionFlag = newInclusionFlag;
}
#endif

void CMaskToolRegion::SetHiddenFlag(bool newHiddenFlag)
{
   hiddenFlag = newHiddenFlag;
}

void CMaskToolRegion::SetBorderWidth(int newBorderWidth)
{
   _borderWidth = newBorderWidth;
}

void CMaskToolRegion::SetBlendSlew(double newBlendSlew)
{
   _blendSlew = newBlendSlew;
}

int CMaskToolRegion::InterpolateInt(int d1, int d2, double t)
{
   // Simple linear interpolation between two integer values based on
   // the interpolation parameter t.  The interpolation parameter is
   // must be between 0 and 1.

   return (int)(d1 * (1-t) + d2 * t + .5);
}

//////////////////////////////////////////////////////////////////////////////
//
// Bezier Curve Mask
//
//////////////////////////////////////////////////////////////////////////////

CBezierMaskToolRegion::CBezierMaskToolRegion(BEZIER_POINT *newBezier)
 : bezierPoints(0), pointCount(0)
{
   copyBezierPoints(newBezier);
}

CBezierMaskToolRegion::CBezierMaskToolRegion(const CBezierMaskToolRegion &src)
 : CMaskToolRegion(src),
   bezierPoints(0), pointCount(0)

{
   copyBezierPoints(src.bezierPoints);
}

CBezierMaskToolRegion::~CBezierMaskToolRegion()
{
   deleteBezierPoints();
}

// ---------------------------------------------------------------------------

CBezierMaskToolRegion& CBezierMaskToolRegion::operator=(
                                              const CBezierMaskToolRegion &rhs)
{
   if (this == &rhs)
      return *this;

   CMaskToolRegion::operator=(rhs);

   copyBezierPoints(rhs.bezierPoints);

   return *this;
}

// "Virtual" assignment operator
CMaskToolRegion& CBezierMaskToolRegion::assign(const CMaskToolRegion& src)
{
   if (this == &src)
      return *this;  // avoid self-assign

   // If the types do not match, throw an exception.  This should
   // never happen unless there is a coding or logic error
   if(GetRegionShape() != src.GetRegionShape())
      throw std::bad_cast();

   *this = static_cast<const CBezierMaskToolRegion&>(src);

   return *this;
}

// ---------------------------------------------------------------------------

void CBezierMaskToolRegion::copyBezierPoints(BEZIER_POINT *newBezier)
{
   // Get rid of existing bezier points
   deleteBezierPoints();

   if (newBezier == 0)
      {
      // create dummy bezier, used as placeholder only
      bezierPoints = 0;
      pointCount = 0;
      boundingBox.left = boundingBox.top = 0;
      boundingBox.right = boundingBox.bottom = 0;
      return; // all done
      }

   // Count the number of vertices in bezier.  Last vertex equals
   // the first vertex
   pointCount = 1;
   while (newBezier[pointCount].xctrlpre != newBezier[0].xctrlpre
          || newBezier[pointCount].yctrlpre != newBezier[0].yctrlpre
          || newBezier[pointCount].x != newBezier[0].x
          || newBezier[pointCount].y != newBezier[0].y
          || newBezier[pointCount].xctrlnxt != newBezier[0].xctrlnxt
          || newBezier[pointCount].yctrlnxt != newBezier[0].yctrlnxt)
      pointCount++;

   pointCount++; // add 1 for last point

   // allocate space for the captive spline
   bezierPoints = new BEZIER_POINT[pointCount];

   for (int i = 0; i < pointCount; ++i)
      {
      bezierPoints[i] = newBezier[i];
      }

   setBoundingBox();
}

void CBezierMaskToolRegion::setBoundingBox()
{
   // Determine bounding box
   boundingBox.left = boundingBox.top = 32767;
   boundingBox.right = boundingBox.bottom = -32768;

   for (int i = 0; i < pointCount; ++i)
      {
      if (bezierPoints[i].xctrlpre < boundingBox.left)
         boundingBox.left = bezierPoints[i].xctrlpre;
      if (bezierPoints[i].x < boundingBox.left)
         boundingBox.left = bezierPoints[i].x;
      if (bezierPoints[i].xctrlnxt < boundingBox.left)
         boundingBox.left = bezierPoints[i].xctrlnxt;

      if (bezierPoints[i].yctrlpre < boundingBox.top)
         boundingBox.top = bezierPoints[i].yctrlpre;
      if (bezierPoints[i].y < boundingBox.top)
         boundingBox.top = bezierPoints[i].y;
      if (bezierPoints[i].yctrlnxt < boundingBox.top)
         boundingBox.top = bezierPoints[i].yctrlnxt;

      if (bezierPoints[i].xctrlpre > boundingBox.right)
         boundingBox.right = bezierPoints[i].xctrlpre;
      if (bezierPoints[i].x > boundingBox.right)
         boundingBox.right = bezierPoints[i].x;
      if (bezierPoints[i].xctrlnxt > boundingBox.right)
         boundingBox.right = bezierPoints[i].xctrlnxt;

      if (bezierPoints[i].yctrlpre > boundingBox.bottom)
         boundingBox.bottom = bezierPoints[i].yctrlpre;
      if (bezierPoints[i].y > boundingBox.bottom)
         boundingBox.bottom = bezierPoints[i].y;
      if (bezierPoints[i].yctrlnxt > boundingBox.bottom)
         boundingBox.bottom = bezierPoints[i].yctrlnxt;
      }
}

void CBezierMaskToolRegion::deleteBezierPoints()
{
   delete [] bezierPoints;
   bezierPoints = 0;
   pointCount = 0;
}

int CBezierMaskToolRegion::tCode(double x, double y)
{
   int tcode = 0;

   if (x < 0) tcode |= LEFT;
   if (y < 0) tcode |= ABOV;

   return tcode;
}

int CBezierMaskToolRegion::jordanCrossing(double xi, double yi, double xf, double yf)
{
   int ti = tCode(xi, yi);
   int tf = tCode(xf, yf);

   if ((ti^tf)&LEFT) {

      if ((ti&tf)&ABOV) {

         return 1;
      }
      else if ((ti^tf)&ABOV) {

         if ((yi - xi*(yf - yi)/(xf - xi)) < 0) {

            return 1;
         }
      }
   }

   return 0;
}

bool CBezierMaskToolRegion::IsInside(int x, int y)
{
   int x0, y0, x1, y1, x2, y2, x3, y3, xl, yb, xr, yt;
   int delx, dely, length, nseg;
   double u0, u1, u2, u3, v0 , v1, v2, v3, xi, yi, xf, yf, delt;

   int jordan = 0; // Jordan counter

   x3 = bezierPoints[0].x; y3 = bezierPoints[0].y;
   for (int i=0;i<pointCount-1;i++) {

      x0 = x3;                         y0 = y3;
      x1 = bezierPoints[i  ].xctrlnxt; y1 = bezierPoints[i  ].yctrlnxt;
      x2 = bezierPoints[i+1].xctrlpre; y2 = bezierPoints[i+1].yctrlpre;
      x3 = bezierPoints[i+1].x;        y3 = bezierPoints[i+1].y;

      // calculate extent of spline segment & test it
      xl = x0; yt = y0; xr = x0; yb = y0;
      if (x1 < xl) xl = x1;
      if (y1 < yt) yt = y1;
      if (x1 > xr) xr = x1;
      if (y1 > yb) yb = y1;

      if (x2 < xl) xl = x2;
      if (y2 < yt) yt = y2;
      if (x2 > xr) xr = x2;
      if (y2 > yb) yb = y2;

      if (x3 < xl) xl = x3;
      if (y3 < yt) yt = y3;
      if (x3 > xr) xr = x3;
      if (y3 > yb) yb = y3;

      // with these conditions the spline segment can
      // make no contribution to the Jordan counter
      if ((yt >= y)||(xl > x)||(xr < x)) continue;

      // coefficients for X
      u0 = x0;
      u1 = 3*(x1 - x0);
      u2 = 3*(x2 - 2*x1 + x0);
      u3 = (x3 - 3*x2 + 3*x1 - x0);

      // coefficients for Y
      v0 = y0;
      v1 = 3*(y1 - y0);
      v2 = 3*(y2 - 2*y1 + y0);
      v3 = (y3 - 3*y2 + 3*y1 - y0);

      // "length" of spline
      length = 0;
      if ((delx = x1 - x0)<0) delx = -delx;
      length += delx;
      if ((dely = y1 - y0)<0) dely = -dely;
      length += dely;
      if ((delx = x2 - x1)<0) delx = -delx;
      length += delx;
      if ((dely = y2 - y1)<0) dely = -dely;
      length += dely;
      if ((delx = x3 - x2)<0) delx = -delx;
      length += delx;
      if ((dely = y3 - y2)<0) dely = -dely;
      length += dely;

      // number of segments with
      // average chordleng = 4
      nseg = length / 4;

      // must be at least one
      if (nseg == 0) nseg = 1;

      // value of delt
      delt = 1.0 / nseg;

      xf = x0; yf = y0;

      // now generate the intermediate points
      // and include them in the spline extent
      for (double t=delt;t<1.0;t+=delt) {

         xi = xf;  yi = yf;
         xf = (((u3*t+u2)*t+u1)*t+u0)+.5; yf = (((v3*t+v2)*t+v1)*t+v0)+.5;

         jordan += jordanCrossing(xi - x, yi - y, xf - x, yf - y);
      }

      xi = xf;  yi = yf;
      xf = x3;  yf = y3;

      jordan += jordanCrossing(xi - x, yi - y, xf - x, yf - y);
   }

   return (jordan&1);
}

// ---------------------------------------------------------------------------

BEZIER_POINT* CBezierMaskToolRegion::GetBezierPointer()
{
   return bezierPoints;
}

int CBezierMaskToolRegion::GetBezierPointCount()
{
   return pointCount;
}

// ---------------------------------------------------------------------------

void CBezierMaskToolRegion::SetBezierPoints(BEZIER_POINT *newBezier)
{
   copyBezierPoints(newBezier);
}

// ---------------------------------------------------------------------------

CMaskToolRegion* CBezierMaskToolRegion::Interpolate(const CMaskToolRegion &region2,
                                                    double intFactor)
{
   if (region2.GetRegionShape() != MASK_REGION_BEZIER)
      return 0;

   const CBezierMaskToolRegion &bezierRegion2 =
                              static_cast<const CBezierMaskToolRegion&>(region2);

   CBezierMaskToolRegion *newRegion = new CBezierMaskToolRegion(*this);

   for (int i = 0; i < pointCount-1; ++i)
      {
      newRegion->bezierPoints[i].xctrlpre = InterpolateInt(bezierPoints[i].xctrlpre,
                                         bezierRegion2.bezierPoints[i].xctrlpre,
                                                        intFactor);
      newRegion->bezierPoints[i].x = InterpolateInt(bezierPoints[i].x,
                                                 bezierRegion2.bezierPoints[i].x,
                                                 intFactor);
      newRegion->bezierPoints[i].xctrlnxt = InterpolateInt(bezierPoints[i].xctrlnxt,
                                                        bezierRegion2.bezierPoints[i].xctrlnxt,
                                                        intFactor);

      newRegion->bezierPoints[i].yctrlpre = InterpolateInt(bezierPoints[i].yctrlpre,
                                                        bezierRegion2.bezierPoints[i].yctrlpre,
                                                        intFactor);
      newRegion->bezierPoints[i].y = InterpolateInt(bezierPoints[i].y,
                                                 bezierRegion2.bezierPoints[i].y,
                                                 intFactor);
      newRegion->bezierPoints[i].yctrlnxt = InterpolateInt(bezierPoints[i].yctrlnxt,
                                                        bezierRegion2.bezierPoints[i].yctrlnxt,
                                                        intFactor);
      }

   // Avoid round-off issues by copying first point to last point
   newRegion->bezierPoints[pointCount-1] = newRegion->bezierPoints[0];

   // Set bounding box for interpolated bezier
   newRegion->setBoundingBox();

   return newRegion;
}

//////////////////////////////////////////////////////////////////////////////
//
// Lasso Mask
//
//////////////////////////////////////////////////////////////////////////////

CLassoMaskToolRegion::CLassoMaskToolRegion(POINT *newLasso)
 : lasso(0), pointCount(0)
{
   copyLassoPoints(newLasso);
}

CLassoMaskToolRegion::CLassoMaskToolRegion(const CLassoMaskToolRegion &src)
 : CMaskToolRegion(src),
   lasso(0), pointCount(0)
{
   copyLassoPoints(src.lasso);
}

CLassoMaskToolRegion::~CLassoMaskToolRegion()
{
   deleteLassoPoints();
}

// ---------------------------------------------------------------------------

CLassoMaskToolRegion& CLassoMaskToolRegion::operator=(
                                              const CLassoMaskToolRegion &rhs)
{
   if (this == &rhs)
      return *this;

   CMaskToolRegion::operator=(rhs);

   copyLassoPoints(rhs.lasso);

   return *this;
}

// "Virtual" assignment operator
CMaskToolRegion& CLassoMaskToolRegion::assign(const CMaskToolRegion& src)
{
   if (this == &src)
      return *this;  // avoid self-assign

   // If the types do not match, throw an exception.  This should
   // never happen unless there is a coding or logic error
   if(GetRegionShape() != src.GetRegionShape())
      throw std::bad_cast();

   *this = static_cast<const CLassoMaskToolRegion&>(src);

   return *this;
}

// ---------------------------------------------------------------------------

void CLassoMaskToolRegion::copyLassoPoints(POINT *newLasso)
{
   deleteLassoPoints();

   int firstX = newLasso[0].x;
   int firstY = newLasso[0].y;

   int count = 1;
   while (firstX != newLasso[count].x || firstY != newLasso[count].y)
      ++count;

   count++;   // include last point in the count

   // Allocate a new lasso
   lasso = new POINT[count];

   // Copy the points from the caller's lasso
   for (int i = 0; i < count; ++i)
      {
      lasso[i].x = newLasso[i].x;
      lasso[i].y = newLasso[i].y;
      }

   // Remember the number of points in the lasso
   pointCount = count;

   setBoundingBox();
}

void CLassoMaskToolRegion::deleteLassoPoints()
{
   delete [] lasso;
   lasso = 0;
   pointCount = 0;
}

void CLassoMaskToolRegion::setBoundingBox()
{
   boundingBox.left = boundingBox.top = 32767;
   boundingBox.right = boundingBox.bottom = -32768;

   // Find the bounding box
   for (int i = 0; i < pointCount-1; ++i)
      {
      int x = lasso[i].x;
      if (x < boundingBox.left)
         boundingBox.left = x;
      if (x > boundingBox.right)
         boundingBox.right = x;
      int y = lasso[i].y;
      if (y < boundingBox.top)
         boundingBox.top = y;
      if (y > boundingBox.bottom)
         boundingBox.bottom = y;
      }
}

bool CLassoMaskToolRegion::IsInside(int x, int y)
{
   int jordan = 0;

   for (int i=0; i < (pointCount-1); i++) {

      int xi = lasso[i].x;
      int yi = lasso[i].y;
      int xf = lasso[(i+1)].x;
      int yf = lasso[(i+1)].y;

      int ti = GetTCode(xi - x, yi - y);
      int tf = GetTCode(xf - x, yf - y);

      if ((ti^tf)&LEFT) {
         if ((ti&tf)&ABOVE) {
            jordan++;
         }
         else if ((ti^tf)&ABOVE) {
            double ycrs = (double)yi +
                          ((double)(x - xi))*((double)(yf - yi)) / ((double)(xf - xi));
            if (ycrs < y) jordan++;
         }
      }
   }

   return (jordan&1);
}

// ---------------------------------------------------------------------------

POINT* CLassoMaskToolRegion::GetLassoPointer()
{
   return lasso;
}

int CLassoMaskToolRegion::GetLassoVertexCount()
{
   return pointCount;
}

// ---------------------------------------------------------------------------

CMaskToolRegion* CLassoMaskToolRegion::Interpolate(const CMaskToolRegion &region2,
                                                   double intFactor)
{
   if (region2.GetRegionShape() != MASK_REGION_LASSO)
      return 0;

   const CLassoMaskToolRegion &lassoRegion2 =
                              static_cast<const CLassoMaskToolRegion&>(region2);

   CLassoMaskToolRegion *newRegion = new CLassoMaskToolRegion(*this);

   for (int i = 0; i < pointCount-1; ++i)
      {
      newRegion->lasso[i].x = InterpolateInt(lasso[i].x,
                                             lassoRegion2.lasso[i].x,
                                             intFactor);

      newRegion->lasso[i].y = InterpolateInt(lasso[i].y,
                                             lassoRegion2.lasso[i].y,
                                             intFactor);
      }

   // Avoid round-off issues by copying first point to last point
   newRegion->lasso[pointCount-1] = newRegion->lasso[0];

   // Set bounding box for interpolated bezier
   newRegion->setBoundingBox();

   return newRegion;
}

//////////////////////////////////////////////////////////////////////////////
//
// Rectangle Mask
//
//////////////////////////////////////////////////////////////////////////////

CRectMaskToolRegion::CRectMaskToolRegion(const RECT &newRect)
{
   copyRect(newRect);
}

CRectMaskToolRegion::CRectMaskToolRegion(const CRectMaskToolRegion &src)
 : CMaskToolRegion(src)
{
   copyRect(src.rect);
}

CRectMaskToolRegion::~CRectMaskToolRegion()
{
}

// ---------------------------------------------------------------------------

CRectMaskToolRegion& CRectMaskToolRegion::operator=(
                                                const CRectMaskToolRegion &rhs)
{
   if (this == &rhs)
      return *this;

   CMaskToolRegion::operator=(rhs);

   copyRect(rhs.rect);

   return *this;
}

// "Virtual" assignment operator
CMaskToolRegion& CRectMaskToolRegion::assign(const CMaskToolRegion& src)
{
   if (this == &src)
      return *this;  // avoid self-assign

   // If the types do not match, throw an exception.  This should
   // never happen unless there is a coding or logic error
   if(GetRegionShape() != src.GetRegionShape())
      throw std::bad_cast();

   *this = static_cast<const CRectMaskToolRegion&>(src);

   return *this;
}

// ---------------------------------------------------------------------------

void CRectMaskToolRegion::copyRect(const RECT &newRect)
{
   rect = newRect;
   boundingBox = rect;
}

bool CRectMaskToolRegion::IsInside(int x, int y)
{
   return((x >= rect.left)&&(x <= rect.right)&&(y >= rect.top)&&(y <= rect.bottom));
}

// ---------------------------------------------------------------------------

RECT CRectMaskToolRegion::GetRect()
{
   return rect;
}

// ---------------------------------------------------------------------------

CMaskToolRegion* CRectMaskToolRegion::Interpolate(const CMaskToolRegion &region2,
                                                  double intFactor)
{
   if (region2.GetRegionShape() != MASK_REGION_RECT)
      return 0;

   const CRectMaskToolRegion &rectRegion2 =
                              static_cast<const CRectMaskToolRegion&>(region2);

   CRectMaskToolRegion *newRegion = new CRectMaskToolRegion(*this);

   newRegion->rect.left = InterpolateInt(rect.left, rectRegion2.rect.left,
                                         intFactor);
   newRegion->rect.top = InterpolateInt(rect.top, rectRegion2.rect.top,
                                        intFactor);
   newRegion->rect.right = InterpolateInt(rect.right, rectRegion2.rect.right,
                                           intFactor);
   newRegion->rect.bottom = InterpolateInt(rect.bottom, rectRegion2.rect.bottom,
                                           intFactor);

   return newRegion;
}

//////////////////////////////////////////////////////////////////////////////
//
// Quad Mask
//
//////////////////////////////////////////////////////////////////////////////

CQuadMaskToolRegion::CQuadMaskToolRegion(const POINT *newQuad)
{
   copyQuadPoints(newQuad);
}

CQuadMaskToolRegion::CQuadMaskToolRegion(const RECT &newRect)
{
//            top
//      [0] -------- [1]
//       |            |
// left  |            | right
//       |            |
//      [3] -------- [2]
//           bottom

   quad[0].x = quad[3].x = newRect.left;
   quad[1].x = quad[2].x = newRect.right;
   quad[0].y = quad[1].y = newRect.top;
   quad[2].y = quad[3].y = newRect.bottom;

   // Add an extra point equal to first point.  This makes it easier
   // for other code to treat the quad like a closed lasso
   quad[4].x = quad[0].x;
   quad[4].y = quad[0].y;

   boundingBox = newRect;
}

CQuadMaskToolRegion::CQuadMaskToolRegion(const CQuadMaskToolRegion &src)
 : CMaskToolRegion(src)
{
   copyQuadPoints(src.quad);
}

CQuadMaskToolRegion::~CQuadMaskToolRegion()
{
}

// ---------------------------------------------------------------------------

CQuadMaskToolRegion& CQuadMaskToolRegion::operator=(
                                              const CQuadMaskToolRegion &rhs)
{
   if (this == &rhs)
      return *this;

   CMaskToolRegion::operator=(rhs);

   copyQuadPoints(rhs.quad);

   return *this;
}

// "Virtual" assignment operator
CMaskToolRegion& CQuadMaskToolRegion::assign(const CMaskToolRegion& src)
{
   if (this == &src)
      return *this;  // avoid self-assign

   // If the types do not match, throw an exception.  This should
   // never happen unless there is a coding or logic error
   if(GetRegionShape() != src.GetRegionShape())
      throw std::bad_cast();

   *this = static_cast<const CQuadMaskToolRegion&>(src);

   return *this;
}

// ---------------------------------------------------------------------------

void CQuadMaskToolRegion::copyQuadPoints(const POINT *newQuad)
{
   // newQuad is assumed to be non-NULL and have four points.  A possible fifth 
   // point is ignored.

   for (int i = 0; i < 4; ++i)
      {
      quad[i].x = newQuad[i].x;
      quad[i].y = newQuad[i].y;
      }

   // Add an extra point equal to first point.  This makes it easier
   // for other code to treat the quad like a closed lasso
   quad[4].x = quad[0].x;
   quad[4].y = quad[0].y;

   setBoundingBox();
}

void CQuadMaskToolRegion::setBoundingBox()
{
   boundingBox.left = boundingBox.top = 32767;
   boundingBox.right = boundingBox.bottom = -32768;

   // Find the bounding box for 4 points
   for (int i = 0; i < 4; ++i)
      {
      int x = quad[i].x;
      if (x < boundingBox.left)
         boundingBox.left = x;
      if (x > boundingBox.right)
         boundingBox.right = x;
      int y = quad[i].y;
      if (y < boundingBox.top)
         boundingBox.top = y;
      if (y > boundingBox.bottom)
         boundingBox.bottom = y;
      }
}

bool CQuadMaskToolRegion::IsInside(int x, int y)
{
   int jordan = 0;

   for (int i=0; i < 4; i++) {

       int xi = quad[i].x;
       int yi = quad[i].y;
       int xf = quad[(i+1)%4].x;
       int yf = quad[(i+1)%4].y;

       int ti = GetTCode(xi - x, yi - y);
       int tf = GetTCode(xf - x, yf - y);

       if ((ti^tf)&LEFT) {
          if ((ti&tf)&ABOVE) {
             jordan++;
          }
          else if ((ti^tf)&ABOVE) {
             double ycrs = (double)yi +
                           ((double)(x - xi))*((double)(yf - yi)) / ((double)(xf - xi));
             if (ycrs < y) jordan++;
          }
       }
   }

   return (jordan&1);
}

// ---------------------------------------------------------------------------

POINT* CQuadMaskToolRegion::GetQuadPointer()
{
   return quad;
}

// ---------------------------------------------------------------------------

CMaskToolRegion* CQuadMaskToolRegion::Interpolate(const CMaskToolRegion &region2,
                                                  double intFactor)
{
   if (region2.GetRegionShape() != MASK_REGION_QUAD)
      return 0;

   const CQuadMaskToolRegion &quadRegion2 =
                              static_cast<const CQuadMaskToolRegion&>(region2);

   CQuadMaskToolRegion *newRegion = new CQuadMaskToolRegion(*this);

   for (int i = 0; i < 4; ++i)
      {
      newRegion->quad[i].x = InterpolateInt(quad[i].x, quadRegion2.quad[i].x,
                                            intFactor);

      newRegion->quad[i].y = InterpolateInt(quad[i].y, quadRegion2.quad[i].y,
                                            intFactor);
      }

   // Add an extra point equal to first point.  This makes it easier
   // for other code to treat the quad like a closed lasso
   newRegion->quad[4].x = newRegion->quad[0].x;
   newRegion->quad[4].y = newRegion->quad[0].y;

   // Set bounding box for interpolated quad
   newRegion->setBoundingBox();

   return newRegion;
}

//////////////////////////////////////////////////////////////////////////////
//
// Mask Tool Region List
//
//////////////////////////////////////////////////////////////////////////////

CMaskToolRegionList::CMaskToolRegionList()
{
}

CMaskToolRegionList::~CMaskToolRegionList()
{
   ClearRegionList();
}

// ---------------------------------------------------------------------------

void CMaskToolRegionList::ClearRegionList()
{
   RegionList::iterator iter;
   for (iter = regionList.begin(); iter != regionList.end(); ++iter)
      {
      delete *iter;
      }

   regionList.clear();
}

// ---------------------------------------------------------------------------

int CMaskToolRegionList::GetMaskToolRegionCount() const
{
   return regionList.size();
}

CMaskToolRegion* CMaskToolRegionList::GetMaskToolRegion(int index) const
{
   if (index < 0 || index >= GetMaskToolRegionCount())
      return 0;

   return regionList[index];
}

void CMaskToolRegionList::AddMaskToolRegion(CMaskToolRegion *newMaskToolRegion)
{
   regionList.push_back(newMaskToolRegion);
}


