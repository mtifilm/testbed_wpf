#ifndef FLOAT_BRUSH_H
#define FLOAT_BRUSH_H

#include "machine.h"
#include "imgToolDLL.h"
#include "BrushAlgo.h"

MTI_IMGTOOLDLL_API struct BrushParameters
{
	int MaxRadius;
	int Radius;
	int Blend;
	int Opacity;
	int Aspect;
	int Angle;
	bool Shape;       // why the fuck bool
	EBrushType Type;

	bool operator == (const BrushParameters &b) const;
	bool operator != (const BrushParameters &b) const;
	BrushParameters& operator = (const BrushParameters & b);
};

// In order to avoid side effects by changing CBrush,
// This brush is created.  It basically adds the floating
// point brush from "kurt's" brush.
MTI_IMGTOOLDLL_API class CFloatBrush : public CBrush
{
public:
	CFloatBrush(int numStripes = 0);
	CFloatBrush(const BrushParameters &brushParameters, int numStripes = 0);
	~CFloatBrush();

	void setBrushParameters(int newMaxRadius, int newRadius, int newBlend, int newOpacity,
		int newAspect, int newAngle, bool newShape, EBrushType newType = BT_SMOOTH);

	void setBrushParametersFromBrush(CFloatBrush *brush);

	int getBrushWidth();
	int getBrushHeight();
	int getBrushSizeInFloats();

	MTI_REAL32 *getFloatWeights();

private:
	MTI_REAL32 *_floatBrush;
	int _floatBrushWidth;
	int _floatBrushHeight;

	void ComputeFloatBrushWeights();

};

#endif
