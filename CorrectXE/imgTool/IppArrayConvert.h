// ---------------------------------------------------------------------------

#ifndef IppArrayConvertH
#define IppArrayConvertH

#include "imgToolDLL.h"
#include "Ippheaders.h"
#include "IppArray.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <initializer_list>

#ifdef __BORLANDC__
#include "IniFile.h"
#endif

struct IppArrayConvertParams
{
    IppiRect roi;
    int pitchInPixels;
    Ipp16u maxValue;
    int numberOfJobs;
};

class MTI_IMGTOOLDLL_API IppArrayConvert
{
public:
    IppArrayConvert(IppiRect roi, int pitchInPixels, Ipp16u maxValue, int numberOfJobs = -1)
    {
        setRoi(roi);
        setPitchInPixels(pitchInPixels);
        setMaxValue(maxValue);
        setJobs(numberOfJobs);
    }

    IppArrayConvert(const IppArrayConvertParams &parms)
        : IppArrayConvert(parms.roi, parms.pitchInPixels, parms.maxValue, parms.numberOfJobs)
    {}

    void setRoi(const IppiRect &roi)
    {
        _roi = roi;
    }

    IppiRect getRoi() const
    {
        return _roi;
    }

    // This is really the width of the entire image.
    // Braca named this but should be masterWidth.
    void setPitchInPixels(int imagePitchInPixels)
    {
        _imagePitchInPixels = imagePitchInPixels;
    }

    int getPitchInPixels() const
    {
        return _imagePitchInPixels;
    }

    void setMaxValue(Ipp16u maxValue)
    {
        _maxValue = maxValue;
    }

    Ipp16u getMaxValue() const
    {
        return _maxValue;
    }

    void setJobs(int numberOfJobs)
    {
        _jobs = numberOfJobs;
    }

    int getJobs() const
    {
        return _jobs;
    }

    Ipp32fArray extractRoiAsFloat(Ipp16u* imageData);

    static Ipp32fArray ImportNormalizedYuvFromRgb(Ipp16u* imageData, int rows, int cols, int maxData, bool yOnly = false, int jobs=-1);

    static IppiRect convertToIppiRect(const RECT &rect)
    {
        IppiRect result =
        {.x = rect.left, .y = rect.top, .width = rect.right - rect.left, .height = rect.bottom - rect.top};
        return result;
    }

    static RECT convertToRect(const IppiRect &rect)
    {
        RECT result =
        {.left = rect.x, .top = rect.y, .right = rect.x + rect.width, .bottom = rect.y +rect.height};
        return result;
    }

private:
    MtiRect _roi;
    int _imagePitchInPixels;
    Ipp16u _maxValue;

    int _jobs = -1;

};

// ---------------------------------------------------------------------------
#endif
