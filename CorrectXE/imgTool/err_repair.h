#ifndef ERRREPAIR_H
#define ERRREPAIR_H

// allowed range for error values:  -4750 -- -4749

#define REPAIR_ERROR_DEBRIS_OUT_OF_BOUNDS	 -4750
#define REPAIR_ERROR_BAD_BORDER_DIMENSION	 -4751
#define REPAIR_ERROR_BAD_SURROUND_DIMENSION	 -4752
#define REPAIR_ERROR_UNKNOWN_REPAIR_TYPE	 -4753
#define REPAIR_ERROR_OPENING_COMMAND_FILE	 -4754
#define REPAIR_ERROR_OPENING_COMMAND_DATA_FILE	 -4755
#define REPAIR_ERROR_WRITING_COMMAND_DATA	 -4756
#define REPAIR_ERROR_NO_USABLE_PIXELS		 -4757
#define REPAIR_ERROR_BAD_NUMBER_FRAMES		 -4758
#define REPAIR_ERROR_BAD_PARAMETER		 -4759
#define REPAIR_MOVE_NOT_SUPPORTED		 -4760
#define REPAIR_ERROR_MALLOC			 -4761

/*
  Alpha order

REPAIR_ERROR_BAD_BORDER_DIMENSION
  The border dimension is illegal

REPAIR_ERROR_BAD_NUMBER_FRAMES
  The number of frames used to perform fix is illegal

REPAIR_ERROR_BAD_PARAMETER
  The parameter value being assigned is illegal 

REPAIR_ERROR_BAD_SURROUND_DIMENSION
  The surround dimension is illegal

REPAIR_ERROR_DEBRIS_OUT_OF_BOUNDS
  The location of the debris is out of bounds

REPAIR_ERROR_NO_USABLE_PIXELS	
  There are no usable pixels for the operation

REPAIR_ERROR_OPENING_COMMAND_FILE
  Unable to open the command file

REPAIR_ERROR_OPENING_COMMAND_DATA_FILE
  Unable to open a data file used when saving the command file

REPAIR_ERROR_UNKNOWN_REPAIR_TYPE
  The requested repair type is illegal

REPAIR_ERROR_WRITING_COMMAND_DATA
  Unable to write to the command data file

REPAIR_MOVE_NOT_SUPPORTED
  Unable to move this type of debris

*/
#endif
