#ifndef MTIBITMAP
#define MTIBITMAP

#include <windows.h>

#include "imgToolDLL.h"

// BITMAP class to create blittable BGRA frame-buffers

class MTI_IMGTOOLDLL_API CMTIBitmap
{
public:

   CMTIBitmap();
   ~CMTIBitmap();

   int initBitmap(HWND hndl, int wdth, int hght);

   int getWidth();
   int getHeight();
   HDC getDCHandle();
   unsigned int *getBitmapFrameBuffer();

   void Draw(HDC screenDC);

private:

   // handle to memory device context for DIB
   HDC hBitmapDC;

   // bitmap dimensions
   int bitmapWidth;
   int bitmapHeight;

   // handle to bitmap
   HBITMAP hBitmap;

   // -> frame buffer
   unsigned int *bitmapFB;
};

#endif

 
