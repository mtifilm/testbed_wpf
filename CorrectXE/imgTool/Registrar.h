//
// include file for class CRegistrar
//
#ifndef REGISTRARH
#define REGISTRARH

#include "machine.h"
#include "imgToolDLL.h"
#include "mthread.h"
#include "ImageInfo.h"

#define TLFT 0x8
#define TTOP 0x4
#define TRGT 0x2
#define TBOT 0x1

////////////////////////////////////////////////////////////

class CImageFormat;

struct AffineCoeff
{
   double AredX, BredX, CredX;
   double AredY, BredY, CredY;
   double AbluX, BbluX, CbluX;
   double AbluY, BbluY, CbluY;
};

class MTI_IMGTOOLDLL_API CRegistrar
{

public:

   CRegistrar(int nStripes = 0);

   virtual ~CRegistrar();

   void registerChannels(MTI_UINT16 *dst, MTI_UINT16 *src,
                         const CImageFormat *srcfmt,
                         RECT *matBox,
                         AffineCoeff *xfrm);

private:

   void makeTestPattern();

   static void rowLoopCounts(int xbeg, int ybeg,
                             int xend, int yend,
                             int xlim, int ylim,
                             int w,
                             int *z1, int *d, int *z2);
private:

   static void null(void *v, int iJob);
   static int regAffineRGB(void *v, int iJob, int iTotalJobs);

   // destination bufffer
   MTI_UINT16 *dstpels;

   // source buffer
   MTI_UINT16 *srcpels;

   // auxiliary buffer
   MTI_UINT16 *auxpels;

   int auxWds;

   // => scan source
   MTI_UINT16 *scnpels;

   // 2 copies of matBox
   RECT scnBox;

   RECT dstBox;

   // width of matBox
   int scnWdth;

   // height of matBox
   int scnHght;

   // width of frame in pels
   int frmWdth;

   // height of frame in pels
   int frmHght;

   // width of fattened frame
   int FrmWdth;

   // height of fattened frame
   int FrmHght;

   AffineCoeff *xform;

   // fractional-pixel offset
   double redxoffs,
          redyoffs;

   double grnxoffs,
          grnyoffs;

   double bluxoffs,
          bluyoffs;

   int rintx,
       rfrx,
       rgrx,
       rinty,
       rfry,
       rgry;

   int gintx,
       gfrx,
       ggrx,
       ginty,
       gfry,
       ggry;

   int bintx,
       bfrx,
       bgrx,
       binty,
       bfry,
       bgry;

   int rgbBytes;
   int alphaBytes;

   int xltRed;
   int yltRed;
   int xrtRed;
   int yrtRed;
   int xlbRed;
   int ylbRed;

   int xltBlu;
   int yltBlu;
   int xrtBlu;
   int yrtBlu;
   int xlbBlu;
   int ylbBlu;

   int rowdelxRed;
   int rowdelyRed;
   int coldelxRed;
   int coldelyRed;

   int rowdelxBlu;
   int rowdelyBlu;
   int coldelxBlu;
   int coldelyBlu;

   ///////////////////////////////////////////////////////////////

   int _nStripes; // number of stripes in frame (1,2,4,8, or 16)

   ///////////////////////////////////////////////////////////////

};

#endif

 