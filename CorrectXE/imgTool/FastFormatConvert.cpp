//#define ROWTBL
//#define COLTBL

#include "MTIio.h"                   // Windows <-> Unix definitions
#include "MTImalloc.h"
#include "FastFormatConvert.h"
#include <iostream>

// this is the 16-BIT VERSION of the HIDEF-to-NTSC CONVERTER
// we add a THROTTLE to go from FULL to NEAREST-NEIGHBOR
// we add NORMALIZING the SUM of the COEFFICIENTS
// we SIMPLIFY the generation of the COEFFICIENTS
// we add different versions of STRIPE to cover pixel depths
// we optimize the stripes again
// we extend the constructor and add the throttle method
// we add the src and dst window setting methods
// we special-case where src and dst wdths are equal


//////////////////////////////////////////////////////////

static void     stripe8to8(CFastFormatConvert *vp, int iJob);
static void   stripe8to8eq(CFastFormatConvert *vp, int iJob);
static void    stripe8to16(CFastFormatConvert *vp, int iJob);
static void  stripe8to16eq(CFastFormatConvert *vp, int iJob);
static void    stripe16to8(CFastFormatConvert *vp, int iJob);
static void  stripe16to8eq(CFastFormatConvert *vp, int iJob);
static void   stripe16to16(CFastFormatConvert *vp, int iJob);
static void stripe16to16eq(CFastFormatConvert *vp, int iJob);

//////////////////////////////////////////////////////////

CFastFormatConvert::
CFastFormatConvert(
                int swdth, // src wdth
                int shght, // src hght
		int srcbp, // src bits per pixel (8 or 16)
               bool srcil, // src interlaced flag
                int dwdth, // dst wdth
                int dhght, // dst hght
                int dstbp, // dst bits per pixel (8 or 16)
               bool dstil  // dst interlaced flag
           )
{
   // register src wdth
   wsrc = swdth;

   // register src hght
   hsrc = shght;

   // register src bits per pixel
   srcBitsPerPixel = srcbp;

   // register src interlaced flag
   srcInterlaced = srcil;

   // register dst wdth
   wdst = dwdth;

   // register dst hght
   hdst = dhght;

   // register dst bits per pixel
   dstBitsPerPixel = dstbp;

   // register dst interlaced flag
   dstInterlaced = dstil;

   // default value of throttle 
   throttle = 1.00;

   // src window

   srcMinRow = 0;
   srcMinCol = 0;
   srcMaxRow = hsrc-1;
   srcMaxCol = wsrc-1;

   srcWinHght = (srcMaxRow - srcMinRow) + 1;
   srcWinWdth = (srcMaxCol - srcMinCol) + 1;

   // dst window

   dstMinRow = 0;
   dstMinCol = 0;
   dstMaxRow = hdst-1;
   dstMaxCol = wdst-1;

   dstWinHght = (dstMaxRow - dstMinRow) + 1;
   dstWinWdth = (dstMaxCol - dstMinCol) + 1;

   // number of stripes subdividing the work
   nStripes = sysmp(MP_NPROCS);

   // the src pitch in units of U 
   srcPitch = wsrc<<1;

   // allocate space for one converted line per stripe
   for (int i=0;i<nStripes;i++) {
      avgLin[i] = new MTI_UINT16[MAXWDTH*2];
   }

   // the dst pitch in units of U
   dstPitch = wdst<<1;

   // allocate space for control tables
   rowTbl = new ROWENTRY[MAXHGHT];
   colTbl = new COLENTRY[MAXWDTH>>1];

   // table of coefficients for averaging lines
   buildRowTable();

   // table of coefficients for averaging pixels
   buildColTable();

   ////////////////////////////////////////////////

   // the conversion is done on several equal-sized
   // stripes in parallel, one stripe for each proc.

   tsThread.PrimaryFunction = (void (*)(void *,int))NULL;
   if (srcBitsPerPixel==8) {
      if (dstBitsPerPixel==8) {
         tsThread.PrimaryFunction = (void (*)(void *, int))&stripe8to8;
         if (wdst==wsrc) {
            tsThread.PrimaryFunction = (void (*)(void *, int))&stripe8to8eq;
         }
      }
      else if (dstBitsPerPixel==16) {
         tsThread.PrimaryFunction = (void (*)(void *, int))&stripe8to16;
      }
   }
   else if (srcBitsPerPixel==16) {
      if (dstBitsPerPixel==8) {
         tsThread.PrimaryFunction = (void (*)(void *, int))&stripe16to8;
      }
      else if (dstBitsPerPixel==16) {
         tsThread.PrimaryFunction = (void (*)(void *, int))&stripe16to16;
         if (wdst==wsrc) {
            tsThread.PrimaryFunction = (void (*)(void *, int))&stripe16to16eq;
         }
      }
   }
   tsThread.SecondaryFunction = NULL;
   tsThread.CallBackFunction = NULL;
   tsThread.vpApplicationData = this;
   tsThread.iNThread = nStripes;
   tsThread.iNJob = nStripes;

   int iRet = MThreadAlloc(&tsThread);
   if (iRet)
    {
      std::unexpected();
    }

   /////////////////////////////////////////////////

}

CFastFormatConvert::
~CFastFormatConvert()
{
   // deallocate the thread
   int iRet = MThreadFree(&tsThread);
   if (iRet)
    {
     std::unexpected();
    }

   // delete coeff tables
   delete [] rowTbl;
   delete [] colTbl;

   // delete averaging line
   for (int i=0;i<nStripes;i++) {
      delete [] avgLin[i];
   }
}
	
void CFastFormatConvert::
setThrottle(double throtval)
{
   if ((throtval<0.0)||(throtval>1.0)) return;
   throttle = throtval;

   if (throtval!= throttle) { // change

      throttle = throtval;

      // build new control tables

      buildRowTable();
      buildColTable();

   }
}

void CFastFormatConvert::
setSrcWindow(
              int minRowSrc,
              int minColSrc,
              int maxRowSrc,
              int maxColSrc
            )
{
   // adjust args sto conform to KM convention
   maxRowSrc -= 1;
   maxColSrc -= 1;

   if ((minRowSrc>maxRowSrc)||(minRowSrc<0)||(maxRowSrc>(hsrc-1))) return;
   if ((minColSrc>maxColSrc)||(minColSrc<0)||(maxColSrc>(wsrc-1))) return;

   minColSrc = minColSrc&0xfffe;
   maxColSrc = (maxColSrc&0xfffe)+1;

   if ((minRowSrc!=srcMinRow)||
       (maxRowSrc!=srcMaxRow)||
       (minColSrc!=srcMinCol)||
       (maxColSrc!=srcMaxCol)) { // change

      srcMinRow = minRowSrc;
      srcMaxRow = maxRowSrc;
      srcMinCol = minColSrc;
      srcMaxCol = maxColSrc;

      srcWinHght = srcMaxRow - srcMinRow + 1;
      srcWinWdth = srcMaxCol - srcMinCol + 1;

      // build new control tables

      buildRowTable();
      buildColTable();

   }

}

void CFastFormatConvert::
setDstWindow(
              int minRowDst,
              int minColDst,
              int maxRowDst,
              int maxColDst
            )
{
   // adjust args to conform to KM convention
   maxRowDst -= 1;
   maxColDst -= 1;

   if ((minRowDst>maxRowDst)||(minRowDst<0)||(maxRowDst>(hdst-1))) return;
   if ((minColDst>maxColDst)||(minColDst<0)||(maxColDst>(wdst-1))) return;

   minColDst = minColDst&0xfffe;
   maxColDst = (maxColDst&0xfffe)+1;

   if ((minRowDst!=dstMinRow)||
       (maxRowDst!=dstMaxRow)||
       (minColDst!=dstMinCol)||
       (maxColDst!=dstMaxCol)) { // change

      dstMinRow = minRowDst;
      dstMaxRow = maxRowDst;
      dstMinCol = minColDst;
      dstMaxCol = maxColDst;

      dstWinHght = dstMaxRow - dstMinRow + 1;
      dstWinWdth = dstMaxCol - dstMinCol + 1;

      // build new control tables

      buildRowTable();
      buildColTable();

   }

}



void CFastFormatConvert::
setWindows(
             int minRowSrc,
             int minColSrc,
             int maxRowSrc,
             int maxColSrc,

             int minRowDst,
             int minColDst,
             int maxRowDst,
             int maxColDst
          )
{
   // adjust args to conform to KM convention
   maxRowSrc -= 1;
   maxColSrc -= 1;
   maxRowDst -= 1;
   maxColDst -= 1;

   if ((minRowSrc>maxRowSrc)||(minRowSrc<0)||(maxRowSrc>(hsrc-1))) return;
   if ((minColSrc>maxColSrc)||(minColSrc<0)||(maxColSrc>(wsrc-1))) return;

   if ((minRowDst>maxRowDst)||(minRowDst<0)||(maxRowDst>(hdst-1))) return;
   if ((minColDst>maxColDst)||(minColDst<0)||(maxColDst>(wdst-1))) return;

   minColSrc = minColSrc&0xfffe;
   maxColSrc = (maxColSrc&0xfffe)+1;

   minColDst = minColDst&0xfffe;
   maxColDst = (maxColDst&0xfffe)+1;

   if ((minRowSrc!=srcMinRow)||
       (maxRowSrc!=srcMaxRow)||
       (minColSrc!=srcMinCol)||
       (maxColSrc!=srcMaxCol)||
       (minRowDst!=dstMinRow)||
       (maxRowDst!=dstMaxRow)||
       (minColDst!=dstMinCol)||
       (maxColDst!=dstMaxCol)) { // change
	
      srcMinRow = minRowSrc;
      srcMaxRow = maxRowSrc;
      srcMinCol = minColSrc;
      srcMaxCol = maxColSrc;

      srcWinHght = srcMaxRow - srcMinRow + 1;
      srcWinWdth = srcMaxCol - srcMinCol + 1;

      dstMinRow = minRowDst;
      dstMaxRow = maxRowDst;
      dstMinCol = minColDst;
      dstMaxCol = maxColDst;

      dstWinHght = dstMaxRow - dstMinRow + 1;
      dstWinWdth = dstMaxCol - dstMinCol + 1;

      // build new control tables

      buildRowTable();
      buildColTable();

   }

}

#ifdef _MSC_VER
#define ONE ((MTI_UINT64)0x0001000000000000)
#define HLF ((MTI_UINT64)0x0000800000000000)
#define INT ((MTI_UINT64)0xffff000000000000)
#define FRC ((MTI_UINT64)0x0000ffffffffffff)
#define THREE ((MTI_UINT64)0x0003000000000000)
#define MSHFT 8
#else
#define ONE ((MTI_UINT64)0x0001000000000000ULL)
#define HLF ((MTI_UINT64)0x0000800000000000ULL)
#define INT ((MTI_UINT64)0xffff000000000000ULL)
#define FRC ((MTI_UINT64)0x0000ffffffffffffULL)
#define THREE ((MTI_UINT64)0x0003000000000000ULL)
#define MSHFT 8
#endif

void CFastFormatConvert::
buildRowTable()
{
   MTI_UINT64 ymid, ylo, yhi, yint;
   int isrc;

   MTI_UINT64 hpixel = (((MTI_UINT64)srcWinHght)<<49)/((MTI_UINT64)dstWinHght);
   hpixel = (hpixel+1) >> 1;

   // HSPAN is the hght, in src pixels, over which averaging is done
   // this can't exceed three, so that the total lines is <= 4 
   MTI_UINT64 hspan = (((MTI_UINT64)(srcWinHght*(1.0-throttle)))<<48)/((MTI_UINT64)dstWinHght);
   if (hspan > THREE) hspan = THREE;

   for (int i=0; i<dstWinHght;i++) {

      // the center of the sample 
      ymid = (hpixel>>1) + i*hpixel;

      if (hspan==0) {

         // round to nearest value
         isrc = (ymid+HLF)>>48;

         rowTbl[i].coeff0  = (1<<MSHFT);
         rowTbl[i].srcfld0 = 0;
         rowTbl[i].srcrow0 = (srcMinRow+isrc)*srcPitch+srcMinCol*2;
         if (srcInterlaced) {
            rowTbl[i].srcfld0 = (srcMinRow+isrc)&1;
            rowTbl[i].srcrow0 = ((srcMinRow+isrc)>>1)*srcPitch+srcMinCol*2;
         }

         rowTbl[i].coeff1  = 0;
         rowTbl[i].srcfld1 = 0;
         rowTbl[i].srcrow1 = 0;

         rowTbl[i].coeff2  = 0;
         rowTbl[i].srcfld2 = 0;
         rowTbl[i].srcrow2 = 0;

         rowTbl[i].coeff3  = 0;
         rowTbl[i].srcfld3 = 0;
         rowTbl[i].srcrow3 = 0;

      }
      else {

         // the lo end of the sample
         ylo = ymid-(hspan>>1);
         // the hi end of the sample
         yhi = ylo + hspan;

         yint = (ylo&INT)+ONE;
         isrc = ylo>>48;

         int j = 0;
         RTRIPLE *rc = (RTRIPLE *)&(rowTbl[i].coeff0);
         MTI_UINT32  pen = 0;
         MTI_UINT32 cmax = 0;
         int jmax = 0;

         while (true) {

            if (yint < yhi) {

               (rc+j)->coeff  = (MTI_UINT32)(((ONE-(ylo&FRC))<<MSHFT)/hspan);
               (rc+j)->srcfld = 0;
               (rc+j)->srcrow = (srcMinRow+isrc)*srcPitch+srcMinCol*2; 
               if (srcInterlaced) {
                  (rc+j)->srcfld = (srcMinRow+isrc)&1;
                  (rc+j)->srcrow = ((srcMinRow+isrc)>>1)*srcPitch+srcMinCol*2;
               }

               ylo = yint;
               yint += ONE;
               isrc++;

               pen += (rc+j)->coeff; 
               if ((rc+j)->coeff > cmax) {
                  cmax = (rc+j)->coeff;
                  jmax = j;                  
               }               
	       j++;

            }
            else break;

         }

         (rc+j)->coeff  = (MTI_UINT32)((((MTI_UINT64)(yhi-ylo))<<MSHFT)/hspan);
         (rc+j)->srcfld = 0;
         (rc+j)->srcrow = (srcMinRow+isrc)*srcPitch+srcMinCol*2;
         if (srcInterlaced) {
            (rc+j)->srcfld = (srcMinRow+isrc)&1;
            (rc+j)->srcrow = ((srcMinRow+isrc)>>1)*srcPitch+srcMinCol*2;
         }

         pen += (rc+j)->coeff;
         if ((rc+j)->coeff > cmax) {
            jmax = j;
         }
         j++;

         while (j < 4) {

            (rc+j)->coeff  = 0;
            (rc+j)->srcfld = 0;
            (rc+j)->srcrow = 0;

            j++;

         }

         pen = ((MTI_UINT32)(1<<MSHFT)) - pen;
         (rc+jmax)->coeff += pen;

      }

      // now the destination field

      rowTbl[i].dstfld = 0;
      rowTbl[i].dstrow = (dstMinRow+i)*dstPitch+dstMinCol*2;
      if (dstInterlaced) { 
         rowTbl[i].dstfld = (dstMinRow+i)&1;
         rowTbl[i].dstrow = ((dstMinRow+i)>>1)*dstPitch+dstMinCol*2;
      }

#ifdef ROWTBL
      cout << "ROW " << i << endl;

      cout << rowTbl[i].coeff0  << endl;
      cout << rowTbl[i].srcfld0 << endl;
      cout << rowTbl[i].srcrow0 << endl << endl;

      cout << rowTbl[i].coeff1  << endl;
      cout << rowTbl[i].srcfld1 << endl;
      cout << rowTbl[i].srcrow1 << endl << endl;

      cout << rowTbl[i].coeff2  << endl;
      cout << rowTbl[i].srcfld2 << endl;
      cout << rowTbl[i].srcrow2 << endl << endl;

      cout << rowTbl[i].coeff3  << endl;
      cout << rowTbl[i].srcfld3 << endl;
      cout << rowTbl[i].srcrow3 << endl << endl;

      cout << rowTbl[i].dstfld << endl;
      cout << rowTbl[i].dstrow << endl << endl;
      cout << endl;
#endif

   }

}

void CFastFormatConvert::
buildColTable()
{
   MTI_UINT64 xmid, xlo, xhi, xint;
   int isrc;

   MTI_UINT64 wpixel = (((MTI_UINT64)srcWinWdth)<<49)/((MTI_UINT64)dstWinWdth);
   wpixel = (wpixel+1) >> 1;

   // WSPAN is the wdth, in src pixels, over which averaging is done
   // this can't exceed three, so that the total pixels is <= 4
   MTI_UINT64 wspan = (((MTI_UINT64)(srcWinWdth*(1.0-throttle)))<<48)/((MTI_UINT64)dstWinWdth);
   if (wspan > THREE) wspan = THREE;

   // we will operate on pixel-pairs, so we'll only need that many

   for (int i=0;i<(dstWinWdth>>1);i++) {

      // the center of the sample 
      xmid = (wpixel>>1) + i*wpixel;

      if (wspan==0) {

         // round to nearest value
         isrc = (xmid+HLF)>>48;

         colTbl[i].coeff0 = (1<<MSHFT);
         colTbl[i].srcpr0 = isrc * 4;

         colTbl[i].coeff1 = 0;
         colTbl[i].srcpr1 = 0;

         colTbl[i].coeff2 = 0;
         colTbl[i].srcpr2 = 0;

         colTbl[i].coeff3 = 0;
         colTbl[i].srcpr3 = 0;

      }
      else {

         // the lo end of the sample
         xlo = xmid-(wspan>>1);

         // the hi end of the sample
         xhi = xlo + wspan;

         xint = (xlo&INT)+ONE;
         isrc = xlo>>48;

         int j = 0;
         CDOUBLE *cc = (CDOUBLE *)&(colTbl[i].coeff0);
         MTI_UINT32  pen = 0;
         MTI_UINT32 cmax = 0;
         int jmax = 0;

         while (true) {

            if (xint < xhi) {

               (cc+j)->coeff = (MTI_UINT32)(((ONE-(xlo&FRC))<<MSHFT)/wspan);
               (cc+j)->srcpr = isrc * 4;

               xlo = xint;
               xint += ONE;
               isrc++;

               pen += (cc+j)->coeff;
               if ((cc+j)->coeff > cmax) {
                  cmax = (cc+j)->coeff;
                  jmax = j;
               }
               j++;

            }
            else break;

         }

         (cc+j)->coeff = (MTI_UINT32)((((MTI_UINT64)(xhi-xlo))<<MSHFT)/wspan);
         (cc+j)->srcpr = isrc * 4;

         pen += (cc+j)->coeff;
         if ((cc+j)->coeff > cmax) {
            jmax = j;
         }
         j++;

         while (j < 4) {

            (cc+j)->coeff = 0;
            (cc+j)->srcpr = 0;

            j++;

         }

         pen = ((MTI_UINT32)(1<<MSHFT)) - pen;
         (cc+jmax)->coeff += pen;

      }

#ifdef COLTBL
      cout << "COL " << i << endl;

      cout << colTbl[i].coeff0 << endl;
      cout << colTbl[i].srcpr0 << endl << endl;;

      cout << colTbl[i].coeff1 << endl;
      cout << colTbl[i].srcpr1 << endl << endl;

      cout << colTbl[i].coeff2 << endl;
      cout << colTbl[i].srcpr2 << endl << endl;

      cout << colTbl[i].coeff3 << endl;
      cout << colTbl[i].srcpr3 << endl << endl;

      cout << endl;
#endif

   }

}

void stripe8to8(CFastFormatConvert *vp, int iJob)
{
   MTI_UINT8 *r0,*r1,*r2,*r3,*rlin;
    MTI_UINT8 *rdst;
    ROWENTRY *rowent;
    COLENTRY *colent;
    MTI_UINT64 c0,c1,c2,c3;
    MTI_UINT64 t0,t1,t2,t3, avg, tmp;

   // calculate the stripe lines 
   int lins = vp->dstWinHght / vp->nStripes;
   int beg = iJob*lins;
   if (iJob==vp->nStripes-1) {
      lins = vp->dstWinHght - beg;
   }

   for (int i=beg;i<beg+lins;i++) {

      rowent = &(vp->rowTbl[i]);

      // we'll average up to four rows here
      rlin = (MTI_UINT8 *)vp->avgLin[iJob];

      r0 = ((MTI_UINT8 *)(vp->srcbuf[rowent->srcfld0])) + rowent->srcrow0;

      if ((c1=(MTI_UINT64)rowent->coeff1)==0) {

         // 1 line contributes

         for (int j=0; j< vp->srcWinWdth>>1; j++) { // convert UYVY
            *rlin++ = *r0++;
            *rlin++ = *r0++;
            *rlin++ = *r0++;
            *rlin++ = *r0++;
         }

      }
      else {

         c0 = (MTI_UINT64)rowent->coeff0;

         r1 = ((MTI_UINT8 *)(vp->srcbuf[rowent->srcfld1])) + rowent->srcrow1;

         if ((c2=(MTI_UINT64)rowent->coeff2)==0) {

            // 2 lines contribute

            for (int j=0; j< vp->srcWinWdth>>1; j++) { // convert UYVY

               t0 = (MTI_UINT64)(*r0++);
               t0 = (t0 << 16) + (*r0++);
               t0 = (t0 << 16) + (*r0++);
               t0 = (t0 << 16) + (*r0++);

               t1 = (MTI_UINT64)(*r1++);
               t1 = (t1 << 16) + (*r1++);
               t1 = (t1 << 16) + (*r1++);
               t1 = (t1 << 16) + (*r1++);

               avg = (t0*c0+t1*c1);
               tmp = avg;
               *rlin++ = (MTI_UINT8)(tmp>>(48+MSHFT));
               tmp = avg;
               *rlin++ = (MTI_UINT8)(tmp>>(32+MSHFT));
               tmp = avg;
               *rlin++ = (MTI_UINT8)(tmp>>(16+MSHFT));
               *rlin++ = (MTI_UINT8)(avg>>MSHFT);

            }

         }
         else {

            r2 = ((MTI_UINT8 *)(vp->srcbuf[rowent->srcfld2])) + rowent->srcrow2;

            if ((c3=(MTI_UINT64)rowent->coeff3)==0) {

               // 3 lines contribute

               for (int j=0; j< vp->srcWinWdth>>1; j++) { // convert UYVY

                  t0 = (MTI_UINT64)(*r0++);
                  t0 = (t0 << 16) + (*r0++);
                  t0 = (t0 << 16) + (*r0++);
                  t0 = (t0 << 16) + (*r0++);

                  t1 = (MTI_UINT64)(*r1++);
                  t1 = (t1 << 16) + (*r1++);
                  t1 = (t1 << 16) + (*r1++);
                  t1 = (t1 << 16) + (*r1++);

                  t2 = (MTI_UINT64)(*r2++);
                  t2 = (t2 << 16) + (*r2++);
                  t2 = (t2 << 16) + (*r2++);
                  t2 = (t2 << 16) + (*r2++);

                  avg = (t0*c0+t1*c1+t2*c2);
                  tmp = avg;
                  *rlin++ = (MTI_UINT8)(tmp>>(48+MSHFT));
                  tmp = avg;
                  *rlin++ = (MTI_UINT8)(tmp>>(32+MSHFT));
                  tmp = avg;
                  *rlin++ = (MTI_UINT8)(tmp>>(16+MSHFT));
                  *rlin++ = (MTI_UINT8)(avg>>MSHFT);

               }

            }
            else {

               r3 = ((MTI_UINT8 *)(vp->srcbuf[rowent->srcfld3])) + rowent->srcrow3;

               // 4 lines contribute

               for (int j=0; j< vp->srcWinWdth>>1; j++) { // convert UYVY

                  t0 = (MTI_UINT64)(*r0++);
                  t0 = (t0 << 16) + (*r0++);
                  t0 = (t0 << 16) + (*r0++);
                  t0 = (t0 << 16) + (*r0++);

                  t1 = (MTI_UINT64)(*r1++);
                  t1 = (t1 << 16) + (*r1++);
                  t1 = (t1 << 16) + (*r1++);
                  t1 = (t1 << 16) + (*r1++);

                  t2 = (MTI_UINT64)(*r2++);
                  t2 = (t2 << 16) + (*r2++);
                  t2 = (t2 << 16) + (*r2++);
                  t2 = (t2 << 16) + (*r2++);

                  t3 = (MTI_UINT64)(*r3++);
                  t3 = (t3 << 16) + (*r3++);
                  t3 = (t3 << 16) + (*r3++);
                  t3 = (t3 << 16) + (*r3++);

                  avg = (t0*c0+t1*c1+t2*c2+t3*c3);
                  tmp = avg;
                  *rlin++ = (MTI_UINT8)(tmp>>(48+MSHFT));
                  tmp = avg;
                  *rlin++ = (MTI_UINT8)(tmp>>(32+MSHFT));
                  tmp = avg;
                  *rlin++ = (MTI_UINT8)(tmp>>(16+MSHFT));
                  *rlin++ = (MTI_UINT8)(avg>>MSHFT);

               }

            }

         }

      }

      // AVGLIN now contains one line at source resolution, averaged
      // -> beg of dst line

      rdst = ((MTI_UINT8 *)(vp->dstbuf[rowent->dstfld])) + rowent->dstrow;

      for (int j=0; j<vp->dstWinWdth>>1; j++) {

         // a useful optimization
         colent = &(vp->colTbl[j]);

         r0 = ((MTI_UINT8 *)vp->avgLin[iJob]) + colent->srcpr0;

         if ((c1=(MTI_UINT64)colent->coeff1)==0) {

            // 1 pel contributes

            *rdst++ = *r0++;
            *rdst++ = *r0++;
            *rdst++ = *r0++;
            *rdst++ = *r0++;

         }
         else {

            c0 = (MTI_UINT64)colent->coeff0;

            r1 = ((MTI_UINT8 *)vp->avgLin[iJob]) + colent->srcpr1;

            if ((c2=(MTI_UINT64)colent->coeff2)==0) {

               // 2 pels contribute

               t0 = (MTI_UINT64)(*r0++);
               t0 = (t0 << 16) + (*r0++);
               t0 = (t0 << 16) + (*r0++);
               t0 = (t0 << 16) + (*r0++);

               t1 = (MTI_UINT64)(*r1++);
               t1 = (t1 << 16) + (*r1++);
               t1 = (t1 << 16) + (*r1++);
               t1 = (t1 << 16) + (*r1++);

               avg = (t0*c0+t1*c1);
               tmp = avg;
               *rdst++ = (MTI_UINT8)(tmp>>(48+MSHFT));
               tmp = avg;
               *rdst++ = (MTI_UINT8)(tmp>>(32+MSHFT));
               tmp = avg;
               *rdst++ = (MTI_UINT8)(tmp>>(16+MSHFT));
               *rdst++ = (MTI_UINT8)(avg>>MSHFT);

            }
            else {

               r2 = ((MTI_UINT8 *)vp->avgLin[iJob]) + colent->srcpr2;

               if ((c3=(MTI_UINT64)colent->coeff3)==0) {

                  // 3 pels contribute

                  t0 = (MTI_UINT64)(*r0++);
                  t0 = (t0 << 16) + (*r0++);
                  t0 = (t0 << 16) + (*r0++);
                  t0 = (t0 << 16) + (*r0++);

                  t1 = (MTI_UINT64)(*r1++);
                  t1 = (t1 << 16) + (*r1++);
                  t1 = (t1 << 16) + (*r1++);
                  t1 = (t1 << 16) + (*r1++);

                  t2 = (MTI_UINT64)(*r2++);
                  t2 = (t2 << 16) + (*r2++);
                  t2 = (t2 << 16) + (*r2++);
                  t2 = (t2 << 16) + (*r2++);

                  avg = (t0*c0+t1*c1+t2*c2);
                  tmp = avg;
                  *rdst++ = (MTI_UINT8)(tmp>>(48+MSHFT));
                  tmp = avg;
                  *rdst++ = (MTI_UINT8)(tmp>>(32+MSHFT));
                  tmp = avg;
                  *rdst++ = (MTI_UINT8)(tmp>>(16+MSHFT));
                  *rdst++ = (MTI_UINT8)(avg>>MSHFT);

               }
               else {

                  r3 = ((MTI_UINT8 *)vp->avgLin[iJob]) + colent->srcpr3;

                  // 4 pels contribute

                  t0 = (MTI_UINT64)(*r0++);
                  t0 = (t0 << 16) + (*r0++);
                  t0 = (t0 << 16) + (*r0++);
                  t0 = (t0 << 16) + (*r0++);

                  t1 = (MTI_UINT64)(*r1++);
                  t1 = (t1 << 16) + (*r1++);
                  t1 = (t1 << 16) + (*r1++);
                  t1 = (t1 << 16) + (*r1++);

                  t2 = (MTI_UINT64)(*r2++);
                  t2 = (t2 << 16) + (*r2++);
                  t2 = (t2 << 16) + (*r2++);
                  t2 = (t2 << 16) + (*r2++);

                  t3 = (MTI_UINT64)(*r3++);
                  t3 = (t3 << 16) + (*r3++);
                  t3 = (t3 << 16) + (*r3++);
                  t3 = (t3 << 16) + (*r3++);

                  avg = (t0*c0+t1*c1+t2*c2+t3*c3);
                  tmp = avg;
                  *rdst++ = (MTI_UINT8)(tmp>>(48+MSHFT));
                  tmp = avg;
                  *rdst++ = (MTI_UINT8)(tmp>>(32+MSHFT));
                  tmp = avg;
                  *rdst++ = (MTI_UINT8)(tmp>>(16+MSHFT));
                  *rdst++ = (MTI_UINT8)(avg>>MSHFT);

               }

            }

         }

      }

   }

}

void stripe8to8eq(CFastFormatConvert *vp, int iJob)
{
    MTI_UINT8 *r0,*r1,*r2,*r3,*rlin;
    ROWENTRY *rowent;
    MTI_UINT64 c0,c1,c2,c3;
    MTI_UINT64 t0,t1,t2,t3, avg, tmp;

   // calculate the stripe lines 
   int lins = vp->dstWinHght / vp->nStripes;
   int beg = iJob*lins;
   if (iJob==vp->nStripes-1) {
      lins = vp->dstWinHght - beg;
   }

   for (int i=beg;i<beg+lins;i++) {

      rowent = &(vp->rowTbl[i]);

      // we'll average up to four rows right to the dst
      rlin = ((MTI_UINT8 *)(vp->dstbuf[rowent->dstfld])) + rowent->dstrow;

      r0 = ((MTI_UINT8 *)(vp->srcbuf[rowent->srcfld0])) + rowent->srcrow0;

      if ((c1=(MTI_UINT64)rowent->coeff1)==0) {

         // 1 line contributes

         for (int j=0; j< vp->srcWinWdth>>1; j++) { // convert UYVY
            *rlin++ = *r0++;
            *rlin++ = *r0++;
            *rlin++ = *r0++;
            *rlin++ = *r0++;
         }

      }
      else {

         c0 = (MTI_UINT64)rowent->coeff0;

         r1 = ((MTI_UINT8 *)(vp->srcbuf[rowent->srcfld1])) + rowent->srcrow1;

         if ((c2=(MTI_UINT64)rowent->coeff2)==0) {

            // 2 lines contribute

            for (int j=0; j< vp->srcWinWdth>>1; j++) { // convert UYVY

               t0 = (MTI_UINT64)(*r0++);
               t0 = (t0 << 16) + (*r0++);
               t0 = (t0 << 16) + (*r0++);
               t0 = (t0 << 16) + (*r0++);

               t1 = (MTI_UINT64)(*r1++);
               t1 = (t1 << 16) + (*r1++);
               t1 = (t1 << 16) + (*r1++);
               t1 = (t1 << 16) + (*r1++);

               avg = (t0*c0+t1*c1);
               tmp = avg;
               *rlin++ = (MTI_UINT8)(tmp>>(48+MSHFT));
               tmp = avg;
               *rlin++ = (MTI_UINT8)(tmp>>(32+MSHFT));
               tmp = avg;
               *rlin++ = (MTI_UINT8)(tmp>>(16+MSHFT));
               *rlin++ = (MTI_UINT8)(avg>>MSHFT);

            }

         }
         else {

            r2 = ((MTI_UINT8 *)(vp->srcbuf[rowent->srcfld2])) + rowent->srcrow2;

            if ((c3=(MTI_UINT64)rowent->coeff3)==0) {

               // 3 lines contribute

               for (int j=0; j< vp->srcWinWdth>>1; j++) { // convert UYVY

                  t0 = (MTI_UINT64)(*r0++);
                  t0 = (t0 << 16) + (*r0++);
                  t0 = (t0 << 16) + (*r0++);
                  t0 = (t0 << 16) + (*r0++);

                  t1 = (MTI_UINT64)(*r1++);
                  t1 = (t1 << 16) + (*r1++);
                  t1 = (t1 << 16) + (*r1++);
                  t1 = (t1 << 16) + (*r1++);

                  t2 = (MTI_UINT64)(*r2++);
                  t2 = (t2 << 16) + (*r2++);
                  t2 = (t2 << 16) + (*r2++);
                  t2 = (t2 << 16) + (*r2++);

                  avg = (t0*c0+t1*c1+t2*c2);
                  tmp = avg;
                  *rlin++ = (MTI_UINT8)(tmp>>(48+MSHFT));
                  tmp = avg;
                  *rlin++ = (MTI_UINT8)(tmp>>(32+MSHFT));
                  tmp = avg;
                  *rlin++ = (MTI_UINT8)(tmp>>(16+MSHFT));
                  *rlin++ = (MTI_UINT8)(avg>>MSHFT);

               }

            }
            else {

               r3 = ((MTI_UINT8 *)(vp->srcbuf[rowent->srcfld3])) + rowent->srcrow3;

               // 4 lines contribute

               for (int j=0; j< vp->srcWinWdth>>1; j++) { // convert UYVY

                  t0 = (MTI_UINT64)(*r0++);
                  t0 = (t0 << 16) + (*r0++);
                  t0 = (t0 << 16) + (*r0++);
                  t0 = (t0 << 16) + (*r0++);

                  t1 = (MTI_UINT64)(*r1++);
                  t1 = (t1 << 16) + (*r1++);
                  t1 = (t1 << 16) + (*r1++);
                  t1 = (t1 << 16) + (*r1++);

                  t2 = (MTI_UINT64)(*r2++);
                  t2 = (t2 << 16) + (*r2++);
                  t2 = (t2 << 16) + (*r2++);
                  t2 = (t2 << 16) + (*r2++);

                  t3 = (MTI_UINT64)(*r3++);
                  t3 = (t3 << 16) + (*r3++);
                  t3 = (t3 << 16) + (*r3++);
                  t3 = (t3 << 16) + (*r3++);

                  avg = (t0*c0+t1*c1+t2*c2+t3*c3);
                  tmp = avg;
                  *rlin++ = (MTI_UINT8)(tmp>>(48+MSHFT));
                  tmp = avg;
                  *rlin++ = (MTI_UINT8)(tmp>>(32+MSHFT));
                  tmp = avg;
                  *rlin++ = (MTI_UINT8)(tmp>>(16+MSHFT));
                  *rlin++ = (MTI_UINT8)(avg>>MSHFT);

               }

            }

         }

      }

   }

}

void stripe8to16(CFastFormatConvert *vp, int iJob)
{
    MTI_UINT8 *r0,*r1,*r2,*r3,*rlin;
    MTI_UINT16 *rdst;
    ROWENTRY *rowent;
    COLENTRY *colent;
    MTI_UINT64 c0,c1,c2,c3;
    MTI_UINT64 t0,t1,t2,t3, avg, tmp;

   // calculate the stripe lines 
   int lins = vp->dstWinHght / vp->nStripes;
   int beg = iJob*lins;
   if (iJob==vp->nStripes-1) {
      lins = vp->dstWinHght - beg;
   }

   for (int i=beg;i<beg+lins;i++) {

      rowent = &(vp->rowTbl[i]);

      // we'll average up to four rows here
      rlin = (MTI_UINT8 *)vp->avgLin[iJob];

      r0 = ((MTI_UINT8 *)(vp->srcbuf[rowent->srcfld0])) + rowent->srcrow0;

      if ((c1=(MTI_UINT64)rowent->coeff1)==0) {

         // 1 line contributes

         for (int j=0; j< vp->srcWinWdth>>1; j++) { // convert UYVY
            *rlin++ = *r0++;
            *rlin++ = *r0++;
            *rlin++ = *r0++;
            *rlin++ = *r0++;
         }
      }
      else {

         c0 = (MTI_UINT64)rowent->coeff0;

         r1 = ((MTI_UINT8 *)(vp->srcbuf[rowent->srcfld1])) + rowent->srcrow1;

         if ((c2=(MTI_UINT64)rowent->coeff2)==0) {

            // 2 lines contribute

            for (int j=0; j< vp->srcWinWdth>>1; j++) { // convert UYVY

               t0 = (MTI_UINT64)(*r0++);
               t0 = (t0 << 16) + (*r0++);
               t0 = (t0 << 16) + (*r0++);
               t0 = (t0 << 16) + (*r0++);

               t1 = (MTI_UINT64)(*r1++);
               t1 = (t1 << 16) + (*r1++);
               t1 = (t1 << 16) + (*r1++);
               t1 = (t1 << 16) + (*r1++);

               avg = (t0*c0+t1*c1);
               tmp = avg;
               *rlin++ = (MTI_UINT8)(tmp>>(48+MSHFT));
               tmp = avg;
               *rlin++ = (MTI_UINT8)(tmp>>(32+MSHFT));
               tmp = avg;
               *rlin++ = (MTI_UINT8)(tmp>>(16+MSHFT));
               *rlin++ = (MTI_UINT8)(avg>>MSHFT);

            }

         }
         else {

            r2 = ((MTI_UINT8 *)(vp->srcbuf[rowent->srcfld2])) + rowent->srcrow2;

            if ((c3=(MTI_UINT64)rowent->coeff3)==0) {

               // 3 lines contribute

               for (int j=0; j< vp->srcWinWdth>>1; j++) { // convert UYVY

                  t0 = (MTI_UINT64)(*r0++);
                  t0 = (t0 << 16) + (*r0++);
                  t0 = (t0 << 16) + (*r0++);
                  t0 = (t0 << 16) + (*r0++);

                  t1 = (MTI_UINT64)(*r1++);
                  t1 = (t1 << 16) + (*r1++);
                  t1 = (t1 << 16) + (*r1++);
                  t1 = (t1 << 16) + (*r1++);

                  t2 = (MTI_UINT64)(*r2++);
                  t2 = (t2 << 16) + (*r2++);
                  t2 = (t2 << 16) + (*r2++);
                  t2 = (t2 << 16) + (*r2++);

                  avg = (t0*c0+t1*c1+t2*c2);
                  tmp = avg;
                  *rlin++ = (MTI_UINT8)(tmp>>(48+MSHFT));
                  tmp = avg;
                  *rlin++ = (MTI_UINT8)(tmp>>(32+MSHFT));
                  tmp = avg;
                  *rlin++ = (MTI_UINT8)(tmp>>(16+MSHFT));
                  *rlin++ = (MTI_UINT8)(avg>>MSHFT);

               }

            }
            else {

               r3 = ((MTI_UINT8 *)(vp->srcbuf[rowent->srcfld3])) + rowent->srcrow3;

               // 4 lines contribute

               for (int j=0; j< vp->srcWinWdth>>1; j++) { // convert UYVY

                  t0 = (MTI_UINT64)(*r0++);
                  t0 = (t0 << 16) + (*r0++);
                  t0 = (t0 << 16) + (*r0++);
                  t0 = (t0 << 16) + (*r0++);

                  t1 = (MTI_UINT64)(*r1++);
                  t1 = (t1 << 16) + (*r1++);
                  t1 = (t1 << 16) + (*r1++);
                  t1 = (t1 << 16) + (*r1++);

                  t2 = (MTI_UINT64)(*r2++);
                  t2 = (t2 << 16) + (*r2++);
                  t2 = (t2 << 16) + (*r2++);
                  t2 = (t2 << 16) + (*r2++);

                  t3 = (MTI_UINT64)(*r3++);
                  t3 = (t3 << 16) + (*r3++);
                  t3 = (t3 << 16) + (*r3++);
                  t3 = (t3 << 16) + (*r3++);

                  avg = (t0*c0+t1*c1+t2*c2+t3*c3);
                  tmp = avg;
                  *rlin++ = (MTI_UINT8)(tmp>>(48+MSHFT));
                  tmp = avg;
                  *rlin++ = (MTI_UINT8)(tmp>>(32+MSHFT));
                  tmp = avg;
                  *rlin++ = (MTI_UINT8)(tmp>>(16+MSHFT));
                  *rlin++ = (MTI_UINT8)(avg>>MSHFT);

               }

            }

         }

      }

      // AVGLIN now contains one line at source resolution, averaged
      // -> beg of dst line
      rdst = ((MTI_UINT16 *)(vp->dstbuf[rowent->dstfld])) + rowent->dstrow;

      for (int j=0; j<vp->dstWinWdth>>1; j++) {

         // a useful optimization
         colent = &(vp->colTbl[j]);

         r0 = ((MTI_UINT8 *)vp->avgLin[iJob]) + colent->srcpr0;

         if ((c1=(MTI_UINT64)colent->coeff1)==0) {

            // 1 pel contributes

	    *rdst++ = ((MTI_UINT16)*r0++)<<8;
            *rdst++ = ((MTI_UINT16)*r0++)<<8;
            *rdst++ = ((MTI_UINT16)*r0++)<<8;
            *rdst++ = ((MTI_UINT16)*r0++)<<8;

         }
         else {

            c0 = (MTI_UINT64)colent->coeff0;

            r1 = ((MTI_UINT8 *)vp->avgLin[iJob]) + colent->srcpr1;

            if ((c2=(MTI_UINT64)colent->coeff2)==0) {

               // 2 pels contribute

               t0 = (MTI_UINT64)(*r0++);
               t0 = (t0 << 16) + (*r0++);
               t0 = (t0 << 16) + (*r0++);
               t0 = (t0 << 16) + (*r0++);

               t1 = (MTI_UINT64)(*r1++);
               t1 = (t1 << 16) + (*r1++);
               t1 = (t1 << 16) + (*r1++);
               t1 = (t1 << 16) + (*r1++);

               avg = (t0*c0+t1*c1);
               tmp = avg;
               *rdst++ = (MTI_UINT16)(tmp>>(40+MSHFT));
               tmp = avg;
               *rdst++ = (MTI_UINT16)(tmp>>(24+MSHFT));
               tmp = avg;
               *rdst++ = (MTI_UINT16)(tmp>>(8+MSHFT));
               *rdst++ = (MTI_UINT16)(avg>>(MSHFT-8));

            }
            else {

               r2 = ((MTI_UINT8 *)vp->avgLin[iJob]) + colent->srcpr2;

               if ((c3=(MTI_UINT64)colent->coeff3)==0) {

                  // 3 pels contribute

                  t0 = (MTI_UINT64)(*r0++);
                  t0 = (t0 << 16) + (*r0++);
                  t0 = (t0 << 16) + (*r0++);
                  t0 = (t0 << 16) + (*r0++);

                  t1 = (MTI_UINT64)(*r1++);
                  t1 = (t1 << 16) + (*r1++);
                  t1 = (t1 << 16) + (*r1++);
                  t1 = (t1 << 16) + (*r1++);

                  t2 = (MTI_UINT64)(*r2++);
                  t2 = (t2 << 16) + (*r2++);
                  t2 = (t2 << 16) + (*r2++);
                  t2 = (t2 << 16) + (*r2++);

                  avg = (t0*c0+t1*c1+t2*c2);
                  tmp = avg;
                  *rdst++ = (MTI_UINT16)(tmp>>(40+MSHFT));
                  tmp = avg;
                  *rdst++ = (MTI_UINT16)(tmp>>(24+MSHFT));
                  tmp = avg;
                  *rdst++ = (MTI_UINT16)(tmp>>(8+MSHFT));
                  *rdst++ = (MTI_UINT16)(avg>>(MSHFT-8));

               }
               else {

                  r3 = ((MTI_UINT8 *)vp->avgLin[iJob]) + colent->srcpr3;

                  // 4 pels contribute

                  t0 = (MTI_UINT64)(*r0++);
                  t0 = (t0 << 16) + (*r0++);
                  t0 = (t0 << 16) + (*r0++);
                  t0 = (t0 << 16) + (*r0++);

                  t1 = (MTI_UINT64)(*r1++);
                  t1 = (t1 << 16) + (*r1++);
                  t1 = (t1 << 16) + (*r1++);
                  t1 = (t1 << 16) + (*r1++);

                  t2 = (MTI_UINT64)(*r2++);
                  t2 = (t2 << 16) + (*r2++);
                  t2 = (t2 << 16) + (*r2++);
                  t2 = (t2 << 16) + (*r2++);

                  t3 = (MTI_UINT64)(*r3++);
                  t3 = (t3 << 16) + (*r3++);
                  t3 = (t3 << 16) + (*r3++);
                  t3 = (t3 << 16) + (*r3++);

                  avg = (t0*c0+t1*c1+t2*c2+t3*c3);
                  tmp = avg;
                  *rdst++ = (MTI_UINT16)(tmp>>(40+MSHFT));
                  tmp = avg;
                  *rdst++ = (MTI_UINT16)(tmp>>(24+MSHFT));
                  tmp = avg;
                  *rdst++ = (MTI_UINT16)(tmp>>(8+MSHFT));
                  *rdst++ = (MTI_UINT16)(avg>>(MSHFT-8));

               }

            }

         }

      }

   }

}

void stripe16to8(CFastFormatConvert *vp, int iJob)
{
    MTI_UINT16 *r0,*r1,*r2,*r3,*rlin;
    MTI_UINT8 *rdst;
    ROWENTRY *rowent;
    COLENTRY *colent;
    MTI_UINT64 c0,c1,c2,c3;
    MTI_UINT64 t0,t1,t2,t3, avg, tmp;

   // calculate the stripe lines
   int lins = vp->dstWinHght / vp->nStripes;
   int beg = iJob*lins;
   if (iJob==vp->nStripes-1) {
      lins = vp->dstWinHght - beg;
   }

   for (int i=beg;i<beg+lins;i++) {

      rowent = &(vp->rowTbl[i]);

      // we'll average up to four rows here
      rlin = (MTI_UINT16 *)vp->avgLin[iJob];

      r0 = ((MTI_UINT16 *)(vp->srcbuf[rowent->srcfld0])) + rowent->srcrow0;

      if ((c1=(MTI_UINT64)rowent->coeff1)==0) {

         // 1 line contributes

         for (int j=0; j< vp->srcWinWdth>>1; j++) { // convert UYVY
            *rlin++ = *r0++;
            *rlin++ = *r0++;
            *rlin++ = *r0++;
            *rlin++ = *r0++;
         }

      }
      else {

         c0 = (MTI_UINT64)rowent->coeff0;

         r1 = ((MTI_UINT16 *)(vp->srcbuf[rowent->srcfld1])) + rowent->srcrow1;

         if ((c2=(MTI_UINT64)rowent->coeff2)==0) {

            // 2 lines contribute

            for (int j=0; j< vp->srcWinWdth>>1; j++) { // convert UYVY

               t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
               t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;
               avg = (t0*c0+t1*c1);
               tmp = avg;
               *rlin++ = (MTI_UINT16)(tmp>>(32+MSHFT));
               *rlin++ = (MTI_UINT16)(avg>>MSHFT);

               t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
               t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;
               avg = (t0*c0+t1*c1);
               tmp = avg;
               *rlin++ = (MTI_UINT16)(tmp>>(32+MSHFT));
               *rlin++ = (MTI_UINT16)(avg>>MSHFT);
 
            }

         }
         else {

            r2 = ((MTI_UINT16 *)(vp->srcbuf[rowent->srcfld2])) + rowent->srcrow2;

            if ((c3=(MTI_UINT64)rowent->coeff3)==0) {

               // 3 lines contribute

               for (int j=0; j< vp->srcWinWdth>>1; j++) { // convert UYVY

                  t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
                  t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;
                  t2 = ((MTI_UINT64)(*r2++)<<32); t2 += *r2++;
                  avg = (t0*c0+t1*c1+t2*c2);
                  tmp = avg;
                  *rlin++ = (MTI_UINT16)(tmp>>(32+MSHFT));
                  *rlin++ = (MTI_UINT16)(avg>>MSHFT);

                  t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
                  t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;
                  t2 = ((MTI_UINT64)(*r2++)<<32); t2 += *r2++;
                  avg = (t0*c0+t1*c1+t2*c2);
                  tmp = avg;
                  *rlin++ = (MTI_UINT16)(tmp>>(32+MSHFT));
                  *rlin++ = (MTI_UINT16)(avg>>MSHFT);
            
               }

            }
            else {

               r3 = ((MTI_UINT16 *)(vp->srcbuf[rowent->srcfld3])) + rowent->srcrow3;

               // 4 lines contribute

               for (int j=0; j< vp->srcWinWdth>>1; j++) { // convert UYVY

                  t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
                  t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;
                  t2 = ((MTI_UINT64)(*r2++)<<32); t2 += *r2++;
                  t3 = ((MTI_UINT64)(*r3++)<<32); t3 += *r3++;

                  avg = (t0*c0+t1*c1+t2*c2+t3*c3);
                  tmp = avg;
                  *rlin++ = (MTI_UINT16)(tmp>>(32+MSHFT));
                  *rlin++ = (MTI_UINT16)(avg>>MSHFT);

                  t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
                  t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;
                  t2 = ((MTI_UINT64)(*r2++)<<32); t2 += *r2++;
                  t3 = ((MTI_UINT64)(*r3++)<<32); t3 += *r3++;

                  avg = (t0*c0+t1*c1+t2*c2+t3*c3);
                  tmp = avg;
                  *rlin++ = (MTI_UINT16)(tmp>>(32+MSHFT));
                  *rlin++ = (MTI_UINT16)(avg>>MSHFT);

               }

            }

         }

      }

      // AVGLIN now contains one line at source resolution, averaged
      // -> beg of dst line
      rdst = ((MTI_UINT8 *)(vp->dstbuf[rowent->dstfld])) + rowent->dstrow;

      for (int j=0; j<vp->dstWinWdth>>1; j++) {

         colent = &(vp->colTbl[j]);

         r0 = ((MTI_UINT16 *)vp->avgLin[iJob]) + colent->srcpr0;

         if ((c1=(MTI_UINT64)colent->coeff1)==0) {

            // 1 pel contributes
	
	    *rdst++ = (MTI_UINT8)((*r0++)>>8);
	    *rdst++ = (MTI_UINT8)((*r0++)>>8);
            *rdst++ = (MTI_UINT8)((*r0++)>>8);
            *rdst++ = (MTI_UINT8)((*r0++)>>8);

         }
         else {

            c0 = (MTI_UINT64)colent->coeff0;

            r1 = ((MTI_UINT16 *)vp->avgLin[iJob]) + colent->srcpr1;

            if ((c2=(MTI_UINT64)colent->coeff2)==0) {

               // 2 pels contribute

               // U and Y

               t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
               t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;

               avg = (t0*c0+t1*c1);
               tmp = avg;
               *rdst++ = (MTI_UINT8)(tmp>>(40+MSHFT));
               *rdst++ = (MTI_UINT8)(avg>>(8+MSHFT));

               // V and Y

               t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
               t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;

               avg = (t0*c0+t1*c1);
               tmp = avg;
               *rdst++ = (MTI_UINT8)(tmp>>(40+MSHFT));
               *rdst++ = (MTI_UINT8)(avg>>(8+MSHFT));


            }
            else {

               r2 = ((MTI_UINT16 *)vp->avgLin[iJob]) + colent->srcpr2;

               if ((c3=(MTI_UINT64)colent->coeff3)==0) {

		  // 3 pels contribute

                  // U and Y

                  t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
                  t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;
                  t2 = ((MTI_UINT64)(*r2++)<<32); t2 += *r2++;

                  avg = (t0*c0+t1*c1+t2*c2);
                  tmp = avg;
                  *rdst++ = (MTI_UINT8)(tmp>>(40+MSHFT));
                  *rdst++ = (MTI_UINT8)(avg>>(8+MSHFT));

                  // V and Y

                  t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
                  t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;
                  t2 = ((MTI_UINT64)(*r2++)<<32); t2 += *r2++;

                  avg = (t0*c0+t1*c1+t2*c2);
                  tmp = avg;
                  *rdst++ = (MTI_UINT8)(tmp>>(40+MSHFT));
                  *rdst++ = (MTI_UINT8)(avg>>(8+MSHFT));

               }
               else {

                  r3 = ((MTI_UINT16 *)vp->avgLin[iJob]) + colent->srcpr3;

                  // 4 pels contribute

                  // U and Y

                  t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
                  t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;
                  t2 = ((MTI_UINT64)(*r2++)<<32); t2 += *r2++;
                  t3 = ((MTI_UINT64)(*r3++)<<32); t3 += *r3++;

                  avg = (t0*c0+t1*c1+t2*c2+t3*c3);
                  tmp = avg;
                  *rdst++ = (MTI_UINT8)(tmp>>(40+MSHFT));
                  *rdst++ = (MTI_UINT8)(avg>>(8+MSHFT));

                  // V and Y

                  t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
                  t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;
                  t2 = ((MTI_UINT64)(*r2++)<<32); t2 += *r2++;
                  t3 = ((MTI_UINT64)(*r3++)<<32); t3 += *r3++;

                  avg = (t0*c0+t1*c1+t2*c2+t3*c3);
                  tmp = avg;
                  *rdst++ = (MTI_UINT8)(tmp>>(40+MSHFT));
                  *rdst++ = (MTI_UINT8)(avg>>(8+MSHFT));

               }

            }

         }

      }

   }

}

void stripe16to16(CFastFormatConvert *vp, int iJob)
{
    MTI_UINT16 *r0,*r1,*r2,*r3,*rlin;
    MTI_UINT16 *rdst;
    ROWENTRY *rowent;
    COLENTRY *colent;
    MTI_UINT64 c0,c1,c2,c3;
    MTI_UINT64 t0,t1,t2,t3, avg, tmp;

   // calculate the stripe lines
   int lins = vp->dstWinHght / vp->nStripes;
   int beg = iJob*lins;
   if (iJob==vp->nStripes-1) {
      lins = vp->dstWinHght - beg;
   }

   for (int i=beg;i<beg+lins;i++) {

      rowent = &(vp->rowTbl[i]);

      // we'll average up to four rows here
      rlin = (MTI_UINT16 *)vp->avgLin[iJob];

      r0 = ((MTI_UINT16 *)(vp->srcbuf[rowent->srcfld0])) + rowent->srcrow0;

      if ((c1=(MTI_UINT64)rowent->coeff1)==0) {

         // 1 line contributes

         for (int j=0; j< vp->srcWinWdth>>1; j++) { // convert UYVY
            *rlin++ = *r0++;
            *rlin++ = *r0++;
            *rlin++ = *r0++;
            *rlin++ = *r0++;
         }

      }
      else {

         c0 = (MTI_UINT64)rowent->coeff0;

         r1 = ((MTI_UINT16 *)(vp->srcbuf[rowent->srcfld1])) + rowent->srcrow1;

         if ((c2=(MTI_UINT64)rowent->coeff2)==0) {

            // 2 lines contribute

            for (int j=0; j< vp->srcWinWdth>>1; j++) { // convert UYVY

               t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
               t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;
               avg = (t0*c0+t1*c1);
               tmp = avg;
               *rlin++ = (MTI_UINT16)(tmp>>(32+MSHFT));
               *rlin++ = (MTI_UINT16)(avg>>MSHFT);

               t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
               t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;
               avg = (t0*c0+t1*c1);
               tmp = avg;
               *rlin++ = (MTI_UINT16)(tmp>>(32+MSHFT));
               *rlin++ = (MTI_UINT16)(avg>>MSHFT);
 
            }

         }
         else {

            r2 = ((MTI_UINT16 *)(vp->srcbuf[rowent->srcfld2])) + rowent->srcrow2;

            if ((c3=(MTI_UINT64)rowent->coeff3)==0) {

               // 3 lines contribute

               for (int j=0; j< vp->srcWinWdth>>1; j++) { // convert UYVY

                  t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
                  t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;
                  t2 = ((MTI_UINT64)(*r2++)<<32); t2 += *r2++;
                  avg = (t0*c0+t1*c1+t2*c2);
                  tmp = avg;
                  *rlin++ = (MTI_UINT16)(tmp>>(32+MSHFT));
                  *rlin++ = (MTI_UINT16)(avg>>MSHFT);

                  t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
                  t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;
                  t2 = ((MTI_UINT64)(*r2++)<<32); t2 += *r2++;
                  avg = (t0*c0+t1*c1+t2*c2);
                  tmp = avg;
                  *rlin++ = (MTI_UINT16)(tmp>>(32+MSHFT));
                  *rlin++ = (MTI_UINT16)(avg>>MSHFT);
            
               }

            }
            else {

               r3 = ((MTI_UINT16 *)(vp->srcbuf[rowent->srcfld3])) + rowent->srcrow3;

               // 4 lines contribute

               for (int j=0; j< vp->srcWinWdth>>1; j++) { // convert UYVY

                  t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
                  t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;
                  t2 = ((MTI_UINT64)(*r2++)<<32); t2 += *r2++;
                  t3 = ((MTI_UINT64)(*r3++)<<32); t3 += *r3++;

                  avg = (t0*c0+t1*c1+t2*c2+t3*c3);
                  tmp = avg;
                  *rlin++ = (MTI_UINT16)(tmp>>(32+MSHFT));
                  *rlin++ = (MTI_UINT16)(avg>>MSHFT);

                  t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
                  t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;
                  t2 = ((MTI_UINT64)(*r2++)<<32); t2 += *r2++;
                  t3 = ((MTI_UINT64)(*r3++)<<32); t3 += *r3++;

                  avg = (t0*c0+t1*c1+t2*c2+t3*c3);
                  tmp = avg;
                  *rlin++ = (MTI_UINT16)(tmp>>(32+MSHFT));
                  *rlin++ = (MTI_UINT16)(avg>>MSHFT);

               }

            }

         }

      }

      // AVGLIN now contains one line at source resolution, averaged
      // -> beg of dst line
      rdst = ((MTI_UINT16 *)(vp->dstbuf[rowent->dstfld])) + rowent->dstrow;

      for (int j=0; j<vp->dstWinWdth>>1; j++) {

         colent = &(vp->colTbl[j]);

         r0 = ((MTI_UINT16 *)vp->avgLin[iJob]) + colent->srcpr0;

         if ((c1=(MTI_UINT64)colent->coeff1)==0) {

            // 1 pel contributes
	
            *rdst++ = *r0++;
            *rdst++ = *r0++;
            *rdst++ = *r0++;
            *rdst++ = *r0++;

         }
         else {

            c0 = (MTI_UINT64)colent->coeff0;

            r1 = ((MTI_UINT16 *)vp->avgLin[iJob]) + colent->srcpr1;

            if ((c2=(MTI_UINT64)colent->coeff2)==0) {

               // 2 pels contribute

               // U and Y

               t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
               t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;

               avg = (t0*c0+t1*c1);
               tmp = avg;
               *rdst++ = (MTI_UINT16)(tmp>>(32+MSHFT));
               *rdst++ = (MTI_UINT16)(avg>>MSHFT);

               // V and Y

               t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
               t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;

               avg = (t0*c0+t1*c1);
               tmp = avg;
               *rdst++ = (MTI_UINT16)(tmp>>(32+MSHFT));
               *rdst++ = (MTI_UINT16)(avg>>MSHFT);


            }
            else {

               r2 = ((MTI_UINT16 *)vp->avgLin[iJob]) + colent->srcpr2;

               if ((c3=(MTI_UINT64)colent->coeff3)==0) {

		  // 3 pels contribute

                  // U and Y

                  t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
                  t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;
                  t2 = ((MTI_UINT64)(*r2++)<<32); t2 += *r2++;

                  avg = (t0*c0+t1*c1+t2*c2);
                  tmp = avg;
                  *rdst++ = (MTI_UINT16)(tmp>>(32+MSHFT));
                  *rdst++ = (MTI_UINT16)(avg>>MSHFT);

                  // V and Y

                  t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
                  t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;
                  t2 = ((MTI_UINT64)(*r2++)<<32); t2 += *r2++;

                  avg = (t0*c0+t1*c1+t2*c2);
                  tmp = avg;
                  *rdst++ = (MTI_UINT16)(tmp>>(32+MSHFT));
                  *rdst++ = (MTI_UINT16)(avg>>MSHFT);

               }
               else {

                  r3 = ((MTI_UINT16 *)vp->avgLin[iJob]) + colent->srcpr3;

                  // 4 pels contribute

                  // U and Y

                  t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
                  t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;
                  t2 = ((MTI_UINT64)(*r2++)<<32); t2 += *r2++;
                  t3 = ((MTI_UINT64)(*r3++)<<32); t3 += *r3++;

                  avg = (t0*c0+t1*c1+t2*c2+t3*c3);
                  tmp = avg;
                  *rdst++ = (MTI_UINT16)(tmp>>(32+MSHFT));
                  *rdst++ = (MTI_UINT16)(avg>>MSHFT);

                  // V and Y

                  t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
                  t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;
                  t2 = ((MTI_UINT64)(*r2++)<<32); t2 += *r2++;
                  t3 = ((MTI_UINT64)(*r3++)<<32); t3 += *r3++;

                  avg = (t0*c0+t1*c1+t2*c2+t3*c3);
                  tmp = avg;
                  *rdst++ = (MTI_UINT16)(tmp>>(32+MSHFT));
                  *rdst++ = (MTI_UINT16)(avg>>MSHFT);

               }

            }

         }

      }

   }

}

void stripe16to16eq(CFastFormatConvert *vp, int iJob)
{
    MTI_UINT16 *r0,*r1,*r2,*r3,*rlin;;
    ROWENTRY *rowent;
    MTI_UINT64 c0,c1,c2,c3;
    MTI_UINT64 t0,t1,t2,t3, avg, tmp;

   // calculate the stripe lines
   int lins = vp->dstWinHght / vp->nStripes;
   int beg = iJob*lins;
   if (iJob==vp->nStripes-1) {
      lins = vp->dstWinHght - beg;
   }

   for (int i=beg;i<beg+lins;i++) {

      rowent = &(vp->rowTbl[i]);

      // we'll average up to four rows right to the dst
      rlin = ((MTI_UINT16 *)(vp->dstbuf[rowent->dstfld])) + rowent->dstrow;

      r0 = ((MTI_UINT16 *)(vp->srcbuf[rowent->srcfld0])) + rowent->srcrow0;

      if ((c1=(MTI_UINT64)rowent->coeff1)==0) {

         // 1 line contributes

         for (int j=0; j< vp->srcWinWdth>>1; j++) { // convert UYVY
            *rlin++ = *r0++;
            *rlin++ = *r0++;
            *rlin++ = *r0++;
            *rlin++ = *r0++;
         }

      }
      else {

         c0 = (MTI_UINT64)rowent->coeff0;

         r1 = ((MTI_UINT16 *)(vp->srcbuf[rowent->srcfld1])) + rowent->srcrow1;

         if ((c2=(MTI_UINT64)rowent->coeff2)==0) {

            // 2 lines contribute

            for (int j=0; j< vp->srcWinWdth>>1; j++) { // convert UYVY

               t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
               t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;
               avg = (t0*c0+t1*c1);
               tmp = avg;
               *rlin++ = (MTI_UINT16)(tmp>>(32+MSHFT));
               *rlin++ = (MTI_UINT16)(avg>>MSHFT);

               t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
               t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;
               avg = (t0*c0+t1*c1);
               tmp = avg;
               *rlin++ = (MTI_UINT16)(tmp>>(32+MSHFT));
               *rlin++ = (MTI_UINT16)(avg>>MSHFT);
 
            }

         }
         else {

            r2 = ((MTI_UINT16 *)(vp->srcbuf[rowent->srcfld2])) + rowent->srcrow2;

            if ((c3=(MTI_UINT64)rowent->coeff3)==0) {

               // 3 lines contribute

               for (int j=0; j< vp->srcWinWdth>>1; j++) { // convert UYVY

                  t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
                  t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;
                  t2 = ((MTI_UINT64)(*r2++)<<32); t2 += *r2++;
                  avg = (t0*c0+t1*c1+t2*c2);
                  tmp = avg;
                  *rlin++ = (MTI_UINT16)(tmp>>(32+MSHFT));
                  *rlin++ = (MTI_UINT16)(avg>>MSHFT);

                  t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
                  t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;
                  t2 = ((MTI_UINT64)(*r2++)<<32); t2 += *r2++;
                  avg = (t0*c0+t1*c1+t2*c2);
                  tmp = avg;
                  *rlin++ = (MTI_UINT16)(tmp>>(32+MSHFT));
                  *rlin++ = (MTI_UINT16)(avg>>MSHFT);
            
               }

            }
            else {

               r3 = ((MTI_UINT16 *)(vp->srcbuf[rowent->srcfld3])) + rowent->srcrow3;

               // 4 lines contribute

               for (int j=0; j< vp->srcWinWdth>>1; j++) { // convert UYVY

                  t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
                  t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;
                  t2 = ((MTI_UINT64)(*r2++)<<32); t2 += *r2++;
                  t3 = ((MTI_UINT64)(*r3++)<<32); t3 += *r3++;

                  avg = (t0*c0+t1*c1+t2*c2+t3*c3);
                  tmp = avg;
                  *rlin++ = (MTI_UINT16)(tmp>>(32+MSHFT));
                  *rlin++ = (MTI_UINT16)(avg>>MSHFT);

                  t0 = ((MTI_UINT64)(*r0++)<<32); t0 += *r0++;
                  t1 = ((MTI_UINT64)(*r1++)<<32); t1 += *r1++;
                  t2 = ((MTI_UINT64)(*r2++)<<32); t2 += *r2++;
                  t3 = ((MTI_UINT64)(*r3++)<<32); t3 += *r3++;

                  avg = (t0*c0+t1*c1+t2*c2+t3*c3);
                  tmp = avg;
                  *rlin++ = (MTI_UINT16)(tmp>>(32+MSHFT));
                  *rlin++ = (MTI_UINT16)(avg>>MSHFT);

               }

            }

         }

      }

   }

}

void CFastFormatConvert::
convert(MTI_UINT8 **srcbf,MTI_UINT8 **dstbf)
{
   // drop args into object (to make them
   // part of vp passed to primary func)
   srcbuf = srcbf;
   dstbuf = dstbf;

   // multiprocess the stripes
   int iRet = MThreadStart(&tsThread);
   if (iRet)
    {
     std::unexpected();
    }
}

/////////////////////////////////////////////////////////////////////////////
//
//                          T E S T  C O D E
//
/////////////////////////////////////////////////////////////////////////////

void TestFastFormatConvert()
{
   MTI_UINT8 *inbuf[2]; MTI_UINT8 *outbuf[2];

   inbuf[0] = (MTI_UINT8 *)MTImemalign(4,960*1080*8);
   inbuf[1] = (MTI_UINT8 *)MTImemalign(4,960*1080*8);

   MTI_UINT64 *ptr;

   ptr = (MTI_UINT64 *)inbuf[0];
   for (int i=0;i<960*1080;i++) *ptr++ = 
#ifdef _MSC_VER
      0x1122334411223344;
#else
      0x1122334411223344ULL;
#endif

   ptr = (MTI_UINT64 *)inbuf[1];
   for (int i=0;i<960*1080;i++) *ptr++ =
#ifdef _MSC_VER
      0x1122334411223344;
#else
      0x1122334411223344ULL;
#endif

   outbuf[0] = (MTI_UINT8 *)MTImemalign(4,960*1080*8);
   outbuf[1] = (MTI_UINT8 *)MTImemalign(4,960*1080*8);

   memset(outbuf[0],0,960*1080*8);

   memset(outbuf[1],0,960*1080*8);

   /////////////////////////////////////////////////////////////////////////

   int srcbitsPerPixel = -1;
   while ((srcbitsPerPixel!=8)&&(srcbitsPerPixel!=16)) {
      cout << "SRC BITS PER PIXEL?" << endl;
      cin  >> srcbitsPerPixel;
   }

   int srcintrl = -1;
   bool srcinterlaced;
   while ((srcintrl!=0)&&(srcintrl!=1)) {
      cout << "SRC INTERLACED? (1 or 0)" << endl;
      cin  >> srcintrl;
   }
   srcinterlaced = (srcintrl==1);

   int dstbitsPerPixel = -1;
   while ((dstbitsPerPixel!=8)&&(dstbitsPerPixel!=16)) {
      cout << "DST BITS PER PIXEL?" << endl;
      cin  >> dstbitsPerPixel;
   }

   int dstintrl = -1;
   bool dstinterlaced;
   while ((dstintrl!=0)&&(dstintrl!=1)) {
      cout << "DST INTERLACED? (1 or 0)" << endl;
      cin  >> dstintrl;
   }
   dstinterlaced = (dstintrl==1);

   // allocate a converter

   CFastFormatConvert myCnvt(
                       720,486,
                       srcbitsPerPixel,
                       srcinterlaced,
                       720,576,
                       dstbitsPerPixel,
                       dstinterlaced
                     );

   // adjust the throttle setting

   double throt = -1.0;
   while ((throt<0.0)||(throt>1.0)) {
      cout << "THROTTLE VALUE? (0.0 to 1.0)" << endl;
      cin >> throt;
   }

   myCnvt.setThrottle(throt);

   // set the SRC and DST windows

//   myCnvt.setWindows(540,960,1079,1919,
//                     243,360,485,719);

   //////////////////////////////////////////////////////////////////////////

   // execute the conversion 100 times for timing

   cout << "BEG" << endl;
   for (int i=0;i<100;i++) {
      myCnvt.convert(inbuf,outbuf);
   }
   cout << "END" << endl;

   // find the first non-zero INT in the outbuf (this'll be the
   // upper left corner of the DST window

   MTI_UINT32 *optr = (MTI_UINT32 *)outbuf[0];

   for (int i=0;i<960*1080*2;i++) {
      if (*(optr+i)!=0) {
         printf("%d %x\n",i,*(optr+i));
         break;
      }
   }
}

