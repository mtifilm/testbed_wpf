//
// implementation file for class CBitmap
//
#include "MTIBitmap.h"

//
//  C O N S T R U C T O R
//
CMTIBitmap::CMTIBitmap()
{
   // handle to memory device context for DIB
   hBitmapDC = NULL;
   
   // clear dimensions of bitmap
   bitmapWidth = 0;
   bitmapHeight = 0;

   // handle to bitmap
   hBitmap = NULL;

   // -> frame buffer
   bitmapFB = NULL;
}

//
//  D E S T R U C T O R
//
CMTIBitmap::~CMTIBitmap()
{
   // delete the DIB section
   if (hBitmap != NULL)
      DeleteObject(hBitmap);

   // delete memory device context
   if (hBitmapDC != NULL)
      DeleteDC(hBitmapDC);
}

//
// initialize the bitmap
//
int CMTIBitmap::initBitmap(HWND hndl, int wdth, int hght)
{
   // assume failure
   int retval = -1;

   // put instance into known state:
   //   delete bitmap handle
   //   zero bitmaps dims
   //   delete bitmap device context
   //
   DeleteObject(hBitmap);
   hBitmap = NULL;

   bitmapWidth = 0;
   bitmapHeight = 0;

   DeleteDC(hBitmapDC);
   hBitmapDC = NULL;

   // get parent device context
   HDC screenDC = GetDC(hndl);

   // create the memory device context
   hBitmapDC = CreateCompatibleDC(screenDC);

   if (hBitmapDC != NULL) { // valid memory DC

      // create the DIB bitmap
      unsigned char bminfo[sizeof(BITMAPINFOHEADER)+256*sizeof(RGBQUAD)];

      BITMAPINFO *bmi = (BITMAPINFO *) bminfo;
      bmi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
      bmi->bmiHeader.biWidth  =  wdth;
      bmi->bmiHeader.biHeight = -hght;
      bmi->bmiHeader.biPlanes = 1;
      bmi->bmiHeader.biBitCount = 32;
      bmi->bmiHeader.biCompression = 0;
      bmi->bmiHeader.biSizeImage = wdth*hght*4;
      bmi->bmiHeader.biXPelsPerMeter = 40*GetDeviceCaps(screenDC,LOGPIXELSX);
      bmi->bmiHeader.biYPelsPerMeter = 40*GetDeviceCaps(screenDC,LOGPIXELSY);
      bmi->bmiHeader.biClrUsed      = 256;
      bmi->bmiHeader.biClrImportant = 256;

      // -> bitmap frame buffer
      bitmapFB  = NULL;

      // create the in-memory bitmap & get its handle
      hBitmap = CreateDIBSection(screenDC,
                                 (const BITMAPINFO *)bminfo,
                                 DIB_RGB_COLORS,
			         (void **)&bitmapFB,
                                 NULL,
			          0);

      // if DIB creation failed return error code
      if (hBitmap == NULL) { // DIB allocation failure

         // unneeded now
         DeleteDC(hBitmapDC);

         // clear this
         hBitmapDC = NULL;

      }  // DIB allocation failure

      else { // have DIB

         // register dimensions of bitmap
         bitmapWidth = wdth;
         bitmapHeight = hght;

         // select the DIB into the memory device context
         SelectObject(hBitmapDC,hBitmap);

         // clear the new bitmap
         unsigned int *fill = bitmapFB;
         int pels = bitmapWidth*bitmapHeight;
         for (int i=0;i<pels;i++)
            *fill++ = 0;

         // success
         retval = 0;

      } // valid DIB

   } // valid memory DC

   // done with this
   ReleaseDC(hndl,screenDC);

   return(retval);
}

//
// returns width of bitmap in pixels
//
int CMTIBitmap::getWidth()
{
   return bitmapWidth;
}

//
// returns handle for blitting the bitmap
//
int CMTIBitmap::getHeight()
{
   return bitmapHeight;
}

//
// returns handle for blitting the bitmap
//
HDC CMTIBitmap::getDCHandle()
{
   return(hBitmapDC);
}

//
// returns ptr to start of bitmap's memory
//
unsigned int * CMTIBitmap::getBitmapFrameBuffer()
{
   return(bitmapFB);
}


void CMTIBitmap::Draw (HDC screenDC)
{
  // do a Windows bit blit

  BitBlt (
		screenDC,
		0,
		0,
		bitmapWidth,
		bitmapHeight,
		hBitmapDC,
		0,
		0,
		SRCCOPY
         );
}
