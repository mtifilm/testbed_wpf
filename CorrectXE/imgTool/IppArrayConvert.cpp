// ---------------------------------------------------------------------------

#pragma hdrstop

#include "SynchronousThreadRunner.h"
#include "IppArrayConvert.h"
#include "HRTimer.h"
#include "IniFile.h"
#include "SysInfo.h"
#define NUM_COMPONENTS 3

struct ConvertParams
{
    Ipp16u *src; // Pointer to first source element of interest
    int srcStep; // Source row pitch in bytes
    Ipp32f *dst; // Pointer to first destination element
    int dstStep; // Destination row pitch in bytes
    IppiSize size; // Width and height of pixels to be converted
    Ipp16u maxValue; // The maximum source value: must be (power of 2) - 1
    int numberOfJobs;
    // The total number of jobs for segmentation calculation

    Ipp32fArray dstArray;  // Replaces dst pointer
};

void Convert16uToNormalized32f(Ipp16u* src, int srcStep, Ipp32f *dst, int dstStep, IppiSize size, Ipp16u maxValue)
{
    // Convert to floats.
    auto status = ippiConvert_16u32f_C3R(src, srcStep, dst, dstStep, size);
    IppThrowOnError(status);

    // Normalize to range 0.0 < N <= 1.0 .
    auto scale = Ipp32f(maxValue);

    // presumably maxValue is (power of 2) - 1
    Ipp32f scales[] =  {scale, scale, scale};
    IppThrowOnError(ippiDivC_32f_C3R(dst, dstStep, scales, dst, dstStep, size));
}

int RunConversionJob(void *vp, int jobNumber, int totalJobs)
{
    if (vp == nullptr)
    {
		  return -1;
    }

    // Image segmented by blocks of rows.
    auto params = static_cast<ConvertParams*>(vp);
    IppiSize jobSize;
    jobSize.width = params->size.width;

    // Evenly distribute n extra rows over the first n jobs.
    auto minRowsPerJob = params->size.height / params->numberOfJobs;
    auto extraRows = params->size.height % params->numberOfJobs;
    auto jobY = (minRowsPerJob * jobNumber) + std::min<int>(extraRows, jobNumber);
    auto jobSrc = params->src + (jobY * (params->srcStep / sizeof(Ipp16u)));
    auto jobDst = params->dst + (jobY * (params->dstStep / sizeof(Ipp32f)));
    params->size.height = minRowsPerJob + ((jobNumber < extraRows) ? 1 : 0);
    jobSize.height = params->size.height;

    // Do this job!
    CHRTimer timer;
	 Convert16uToNormalized32f(jobSrc, params->srcStep, jobDst, params->dstStep, jobSize, params->maxValue);

	 return 0;
}

int RunRgbToYuvConversionJob(void *vp, int jobNumber, int totalJobs)
{
    if (vp == nullptr)
    {
		  return -1;
    }

    // Image segmented by blocks of rows.
    auto params = static_cast<ConvertParams*>(vp);
    IppiRect jobRoi;
    jobRoi.width = params->size.width;

    // Evenly distribute n extra rows over the first n jobs.
    auto minRowsPerJob = params->size.height / params->numberOfJobs;
    auto extraRows = params->size.height % params->numberOfJobs;
    jobRoi.y = (minRowsPerJob * jobNumber) + std::min<int>(extraRows, jobNumber);
    jobRoi.x = 0;

    auto jobSrc = params->src + (jobRoi.y * (params->srcStep / sizeof(Ipp16u)));
    jobRoi.height = minRowsPerJob + ((jobNumber < extraRows) ? 1 : 0);

    // Do this job!
    auto jobArray = params->dstArray(jobRoi);
	 jobArray.importNormalizedYuvFromRgb(jobSrc, jobRoi.height, jobRoi.width, params->maxValue);

	 return 0;
}

Ipp32fArray IppArrayConvert::extractRoiAsFloat(Ipp16u* imageData)
{
    ConvertParams params;
    params.maxValue = _maxValue;
    params.size.width = _roi.width;
    params.size.height = _roi.height;

    params.src = imageData + (((_roi.y * _imagePitchInPixels) + _roi.x) * NUM_COMPONENTS);
    params.srcStep = _imagePitchInPixels * NUM_COMPONENTS*sizeof(Ipp16u);

    Ipp32fArray result({_roi.getSize(), NUM_COMPONENTS});
    params.dst = result.data();
    params.dstStep = result.getRowPitchInBytes();

	 _jobs = params.numberOfJobs < 0 ? SysInfo::AvailableProcessorCount() : params.numberOfJobs;
	 if (_jobs == 1)
    {
        RunConversionJob(&params, 0, 1);
		  return result;
    }

	 SynchronousThreadRunner multithread(params.numberOfJobs, &params, RunConversionJob);
	 multithread.Run();

    return result;
}

// ---------------------------------------------------------------------------

Ipp32fArray IppArrayConvert::ImportNormalizedYuvFromRgb(Ipp16u* imageData, int rows, int cols, int maxValue, bool yOnly, int jobs)
{
    try
    {
        Ipp32fArray result({cols, rows, yOnly ? 1 : NUM_COMPONENTS});

        auto imagePitchInPixels = cols;

        ConvertParams params;
        params.maxValue = maxValue;
        params.size.width = cols;
        params.size.height = rows;

        params.src = imageData;
        params.srcStep = imagePitchInPixels * NUM_COMPONENTS*sizeof(Ipp16u);
        params.numberOfJobs = jobs < 0 ? SysInfo::AvailableProcessorCount() : jobs;
        params.dstArray = result;

        auto preSharedDataPtr = result.getSharedDataPtr();
        auto preDataWrapperPtr = preSharedDataPtr.get();
        auto preDataPtr = preSharedDataPtr->dataPtr;

        if (params.numberOfJobs == 1)
        {
            RunRgbToYuvConversionJob(&params, 0, 1);
            return result;
        }

        SynchronousThreadRunner multithread(params.numberOfJobs, &params, RunRgbToYuvConversionJob);
        multithread.Run();

        auto postSharedDataPtr = result.getSharedDataPtr();
        auto postDataWrapperPtr = postSharedDataPtr.get();

        if (postDataWrapperPtr != preDataWrapperPtr)
        {
            TRACE_0(errout << "dataWrapper: post:" << std::setbase(16) << std::setw(16) << std::setfill('0') << *(unsigned long long int *)
                  &postDataWrapperPtr << " != pre:" << std::setbase(16) << std::setw(16) << std::setfill('0') << *(unsigned long long int *)
                  &preDataWrapperPtr);

            throw std::runtime_error("sharedDataPtr changed during conversion");
        }

        try
        {
            if (postSharedDataPtr->dataPtr != preDataPtr)
            {
                TRACE_0(errout << "dataPtr: post:" << std::setbase(16) << std::setw(16) << std::setfill('0') << *(unsigned long long int *)
                      &postDataWrapperPtr << " != pre:" << std::setbase(16) << std::setw(16) << std::setfill('0') << *(unsigned long long int *)
                      &preDataWrapperPtr);
                throw std::runtime_error("dataPtr changed during conversion");
            }
        }
        catch (...)
        {
            TRACE_0(errout << "sharedDataPtr was trashed during conversion");
            throw std::runtime_error("sharedDataPtr was trashed during conversion");
        }

        return result;
    }
    catch (...)
    {
        DBTRACE(imageData);
        DBTRACE(rows);
        DBTRACE(cols);
        TRACE_0(errout << "CAUGHT EXCEPTION IN ImportNormalizedYuvFromRgb!");
        throw;
    }
}

// ---------------------------------------------------------------------------
#pragma package(smart_init)
