/*
	Name:	AudioConvertAPI.cpp
	By:	Kevin Manbeck
	Date:	April 4, 2002

	This program converts one audio file to another and changes
	the sample rate if necessary.
*/

#include <string>
#include <iostream>

#include <stdio.h>
#include <sys/types.h>
//#include <sys/prctl.h>
#include <signal.h>
//#include <unistd.h>
#include <errno.h>
#include <time.h>
//#include <strings.h>

#include "ClipAPI.h"
#include "AudioFormat3.h"
#include "AudioConvertAPI.h"

#define GENERATE_POPS 0



/* Main */
int AudioConvertAPI::Convert()
{
  string         stClipFilename;
  int iOpt, iRet, iNAlloc, iMaxSamplePerField;
  int iNFieldSrc, iNFieldDst, iNChannelSrc,
	iNChannelDst, iField;
  float fRatio, fSampleSrc, fWeight, fValueDst;
  MTI_INT16 *spSrc, *spDst;
  CBinManager    bmBinMgr;
  ClipSharedPtr clip;
  string sBinName, sClipName;
  CAudioFrameList    *aflpSrc;
  CAudioFrameList    *aflpDst;
  const CAudioFormat  *afpSrc;
  const CAudioFormat  *afpDst;
  float fSamplePerFrameSrc, fSamplePerFrameDst;
  MTI_UINT8 *ucpData[2], *ucpTmp;
  int iNSampleSrc, iNSampleDst, iSampleSrc, iSampleDst;
  int iChannelDst, iChannelSrc;
  int iNFrameSrcCuml, iNFrameDstCuml, iCuml, iFrameTmp;

  if (strSrcName == "" || strDstName == "")
    return 1;

  iNFrameSrcCuml=1;
  iNFrameDstCuml=1;

  if (bSrcPulldown && !bDstPulldown)
  {
    iNFrameSrcCuml = 5;
    iNFrameDstCuml = 4;
  }
  else if (bDstPulldown && !bSrcPulldown)
  {
    iNFrameSrcCuml = 4;
    iNFrameDstCuml = 5;
  }

  /*********************************************************************
   * Build the clips
   *********************************************************************/
		// open SOURCE first
  iRet = bmBinMgr.splitClipFileName (strSrcName, sBinName, sClipName);
  if (iRet)
   {
    TRACE_0(errout << "ERROR: Unable to read clip file " << strSrcName << endl
                   << "       Errno: " << errno << endl);
    return 1;
   }

  clip = bmBinMgr.openClip(sBinName, sClipName, &iRet);
  if (iRet)
   {
    TRACE_0(errout << "ERROR: Unable to read clip file " << strSrcName << endl
                   << "       Errno: " << errno << endl);
    return 1;
   }

  CAudioTrack *atpSrc = clip->getAudioTrack(0);
  if (atpSrc == NULL)
   {
    TRACE_0(errout << "ERROR: Unable to read clip file " << strSrcName << endl
                   << "       Errno: " << errno << endl);
    return 1;
   }

  aflpSrc = atpSrc->getAudioFrameList(0);
  
		// open DESTINATION next
  iRet = bmBinMgr.splitClipFileName (strDstName, sBinName, sClipName);
  if (iRet)
   {
    TRACE_0(errout << "ERROR: Unable to read clip file " << strDstName << endl
                   << "       Errno: " << errno << endl);
    return 1;
   }

  clip = bmBinMgr.openClip(sBinName, sClipName, &iRet);
  if (iRet)
   {
    TRACE_0(errout << "ERROR: Unable to read clip file " << strDstName << endl
                   << "       Errno: " << errno << endl);
    return 1;
   }


  CAudioTrack *atpDst = clip->getAudioTrack(0);
  if (atpDst == NULL)
   {
    TRACE_0(errout << "ERROR: Unable to read clip file " << strDstName << endl
                   << "       Errno: " << errno << endl);
    return 1;
   }

  aflpDst = atpDst->getAudioFrameList(0);
  

  /*********************************************************************
   * Get some info from the clip
   *********************************************************************/
  if (iNFrameSrc < 0)
    iNFrameSrc        = aflpSrc->getTotalFrameCount();
  if (iNFrameDst < 0)
    iNFrameDst        = aflpDst->getTotalFrameCount();

  afpSrc = aflpSrc->getAudioFormat();
  afpDst = aflpDst->getAudioFormat();

  iNFieldSrc        = afpSrc->getFieldCount();
  iNFieldDst        = afpDst->getFieldCount();

  iNChannelSrc      = afpSrc->getChannelCount();
  iNChannelDst      = afpDst->getChannelCount();

  fSamplePerFrameSrc = afpSrc->getSamplesPerField() * iNFieldSrc;
  fSamplePerFrameDst = afpDst->getSamplesPerField() * iNFieldDst;

  TRACE_2(errout << "Src convert samples:  " <<
		(iNFrameSrcCuml * fSamplePerFrameSrc) << endl);
  TRACE_2(errout << "Dst convert samples::  " <<
		(iNFrameDstCuml * fSamplePerFrameDst) << endl);

  if (
	(afpSrc->getSampleDataPacking() != AI_SAMPLE_DATA_PACKING_2_Bytes_L) ||
	(afpSrc->getSampleDataType() != AI_SAMPLE_DATA_TYPE_INT)
     )
   {
    TRACE_0(errout << "Error on source data packing" << endl);
    return 1;
   }

  if (
	(afpDst->getSampleDataPacking() != AI_SAMPLE_DATA_PACKING_2_Bytes_L) ||
	(afpDst->getSampleDataType() != AI_SAMPLE_DATA_TYPE_INT)
     )
   {
    TRACE_0(errout << "Error on destination data packing" << endl);
    return 1;
   }

  /*********************************************************************
   * Allocate memory
   *********************************************************************/
  iMaxSamplePerField = afpSrc->getSamplesPerField() + 5;
  iNAlloc = iMaxSamplePerField * iNFieldSrc * afpSrc->getBytesPerSample() *
		iNChannelSrc * iNFrameSrcCuml;
  spSrc = (short *) malloc (iNAlloc);
  if (spSrc == NULL)
   {
    TRACE_0(errout << "Unable to allocate storage" << endl);
    return 1;
   }

  iMaxSamplePerField = afpDst->getSamplesPerField() + 5;
  iNAlloc = iMaxSamplePerField * iNFieldDst * afpDst->getBytesPerSample() *
		iNChannelDst * iNFrameDstCuml;
  spDst = (short *) malloc (iNAlloc);

  /*********************************************************************
   * Process the data
   *********************************************************************/
  //printf("Converting frames %d -> %d to %d -> %d\n",
		//iFrameSrc, iNFrameSrc - 1, iFrameDst, iNFrameDst - 1);
  while (iFrameSrc < iNFrameSrc && iFrameDst < iNFrameDst)
   {
    // read in the source and store in the cumulative array
    iNSampleSrc = 0;
    for (iCuml = 0; iCuml < iNFrameSrcCuml; iCuml++)
     {
      // don't read past end of the audio clip
      if (iFrameSrc >= iNFrameSrc)
       {
        iFrameSrc = iNFrameSrc - 1;
       }

      if ((iFrameSrc % 1000 == 0) || (iFrameSrc == iNFrameSrc - 1))
       {
        //printf("\rConverting frame %d to %d...", iFrameSrc, iFrameDst);
	//fflush(stdout);
       }
        

      // Make spSrc hold entire frame of data.  Do this by making two
      // fields point to different locations in spSrc.
      ucpTmp = (MTI_UINT8 *)spSrc;
      for (iField = 0; iField < iNFieldSrc; iField++)
       {
        ucpData[iField] = ucpTmp +
                   afpSrc->getBytesPerSample() * iNChannelSrc * iNSampleSrc;
#if GENERATE_POPS
        memset(ucpData[iField], 0, afpSrc->getBytesPerSample() * iNChannelSrc
                         * aflpSrc->getSampleCountAtField (iFrameSrc, iField));
#endif
        iNSampleSrc += aflpSrc->getSampleCountAtField (iFrameSrc, iField);
       }

#if !GENERATE_POPS
      iRet = aflpSrc->readMediaAllFieldsOptimized(iFrameSrc, ucpData);
      if (iRet != 0)
       {
        TRACE_0(errout << "Error "  << iRet << " reading frame "
                       << iFrameSrc << endl);
       }
#endif

      // increment the source frame
      iFrameSrc++;
     }

/*
	Determine the number of samples in the destination
*/

    iNSampleDst = 0;
    for (iCuml = 0; iCuml < iNFrameDstCuml; iCuml++)
     {
      iFrameTmp = iFrameDst + iCuml;
      // don't write past end of the audio clip
      if (iFrameTmp >= iNFrameDst)
       {
        iFrameTmp = iNFrameDst - 1;
       }
      for (iField = 0; iField < iNFieldDst; iField++)
       {
        iNSampleDst += aflpDst->getSampleCountAtField (iFrameTmp, iField);
       }
     }

/*
	Copy from spSrc to spDst

	Force iNSampleSrc to fit into iNSampleDst.

	Force destination value at 0 to match source value at 0.

	Force destination value at (iNSampleDst-1) match source value
	at (iNSampleSrc-1)
*/

    fRatio = (float)(iNSampleSrc-1) / (float)(iNSampleDst-1);
    for (iSampleDst = 0; iSampleDst < iNSampleDst; iSampleDst++)
     {
      fSampleSrc = fRatio  * (float)iSampleDst;
      iSampleSrc = (int)fSampleSrc;
      fWeight = fSampleSrc - (float)iSampleSrc;

      for (iChannelDst = 0; iChannelDst < iNChannelDst; iChannelDst++)
       {
        iChannelSrc = iChannelDst % iNChannelSrc;

#ifndef TTT
        fValueDst = (float)spSrc[iSampleSrc * iNChannelSrc + iChannelSrc] *
		(1. - fWeight) +
		(float)spSrc[(iSampleSrc+1) * iNChannelSrc + iChannelSrc] *
		fWeight;

        spDst[iSampleDst*iNChannelDst + iChannelDst] = fValueDst + 0.5;
#else
spDst[iSampleDst*iNChannelDst + iChannelDst] = spSrc[iSampleSrc*iNChannelSrc + iChannelSrc];
#endif
       }
     }

    // Now write out the cumulative destination values
    iNSampleDst = 0;
    for (iCuml = 0; iCuml < iNFrameDstCuml; iCuml++)
     {
      // don't write past end of the audio clip
      if (iFrameDst < iNFrameDst)
       {
	// Make spDst hold entire frame of data.  Do this by making two
	// fields point to different locations in spDst.
        ucpTmp = (MTI_UINT8 *)spDst;
        for (iField = 0; iField < iNFieldDst; iField++)
         {
          ucpData[iField] = ucpTmp + afpDst->getBytesPerSample() *
		iNChannelDst * iNSampleDst;
          iNSampleDst += aflpDst->getSampleCountAtField (iFrameDst, iField);
#if GENERATE_POPS
          int pop = (iFrameDst * 10) + (iField + 1);
          switch (afpDst->getBytesPerSample())
           {
            case 4:
              if (pop > 0x10000000) pop = 0x10000000;
              for (int i = 0; i < iNChannelDst * 8; ++i)
               {
                *(((long*) ucpData[iField]) + i) = pop;
               }
              for (int i = i < iNChannelDst * 8; i < iNChannelDst * 16; ++i)
               {
                *(((long*) ucpData[iField]) + i) = -pop;
               }
              break;

            case 2:
            default:
              if (pop > 0x1000) pop = 0x1000;
              for (int i = 0; i < iNChannelDst * 8; ++i)
               {
                *(((short*) ucpData[iField]) + i) = pop;
               }
              for (int i = iNChannelDst * 8; i < iNChannelDst * 16; ++i)
               {
                *(((short*) ucpData[iField]) + i) = -pop;
               }
           }
#endif // GENERATE_POPS
         }

        iRet = aflpDst->writeMediaAllFieldsOptimized(iFrameDst, ucpData);
        if (iRet != 0)
         {
          TRACE_0( errout << "Error " << iRet << " writing frame"
                          << iFrameDst << endl);
         }
       }

      // increment the dest frame counter
      iFrameDst++;
     }
   }

  //printf("done\n");
      
  /*********************************************************************
   * Finish up
   *********************************************************************/

  free (spSrc);
  free (spDst);
  // GACK Never close clips - binmgr doesn't ref count!!!!!!!!
  //bmBinMgr.closeAllClips();

  return 0;
}
