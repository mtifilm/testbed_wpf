/*
	File:	AspectRatio.h
	By:	Kevin Manbeck
	Date:	Mar 17, 2005

	AspectRatio is used to convert images resolutions using sinc
	function LUTs.
*/

#include "AspectRatio.h"
#include "ImageFormat3.h"
#include "IniFile.h"      // for TRACE_
#include "err_imgtool.h"
#include <math.h>

#define AR_JOB 4    // must be <= MAX_AR_JOB

//#define DO_100_TIMES 1  // TTT for more accurate timing

#ifdef DO_100_TIMES
#include "HRTimer.h"
#endif

CAspectRatio::CAspectRatio()
{
  iInit = INIT_NONE;
  fpSincAlloc = 0;
  fpWindowAlloc = 0;
  fpFilterAlloc = 0;
  ipLUTWei = 0;
  ipLUTSubPixel = 0;
  ipLUTPixel = 0;
  for (int iJob = 0; iJob < AR_JOB; iJob++)
   {
    ucpUnpackedAlloc[iJob] = 0;
    ucpUnpacked[iJob] = 0;
   }
}

CAspectRatio::~CAspectRatio()
{
  Free ();
}

void CAspectRatio::Free ()
{
  FreeWeights ();
  FreeSpatial ();

  if (iInit & INIT_FORMAT)
   {
    int iRet = MThreadFree (&tsThread);
    if (iRet)
     {
      TRACE_0 (errout << "Unable to free mthreads in CAspectRatio");
     }
   }

  iInit = INIT_NONE;
}

int CAspectRatio::AllocWeights ()
{
  // allocate storage for sinc
  fpSincAlloc = (float *)malloc (iFilterWidth * sizeof(float));
  if (fpSincAlloc == 0)
   {
    return ERR_ASPECT_RATIO_MALLOC;
   }
  fpSinc = fpSincAlloc + (iNNeighbor * iNSubPixel);

  // allocate storate for window function
  fpWindowAlloc = (float *)malloc (iFilterWidth * sizeof(float));
  if (fpWindowAlloc == 0)
   {
    return ERR_ASPECT_RATIO_MALLOC;
   }
  fpWindow = fpWindowAlloc + (iNNeighbor * iNSubPixel);

  // allocate storate for filter values
  fpFilterAlloc = (float *)malloc (iFilterWidth * sizeof(float));
  if (fpFilterAlloc == 0)
   {
    return ERR_ASPECT_RATIO_MALLOC;
   }
  fpFilter = fpFilterAlloc + (iNNeighbor * iNSubPixel);

  // allocate storage for the LUT
  ipLUTWei = (int ***)malloc (iNSubPixel * sizeof(int **));
  if (ipLUTWei == 0) 
   {
    return ERR_ASPECT_RATIO_MALLOC;
   }

  for (int iSubPixel = 0; iSubPixel < iNSubPixel; iSubPixel++)
   {
    ipLUTWei[iSubPixel] = 0;
   }

  for (int iSubPixel = 0; iSubPixel < iNSubPixel; iSubPixel++)
   {
    ipLUTWei[iSubPixel] = (int **) malloc (iTotalNeighbor * sizeof(int *));
    if (ipLUTWei[iSubPixel] == 0)
     {
      return ERR_ASPECT_RATIO_MALLOC;
     }

    for (int iNeighbor = 0; iNeighbor < iTotalNeighbor; iNeighbor++)
     {
      ipLUTWei[iSubPixel][iNeighbor] = 0;
     }

    int iVal;
    if (iPixelPackingSrc == IF_PIXEL_PACKING_8Bits_IN_1Byte)
      iVal = 256;
    else if (
	iPixelPackingSrc == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS ||
	iPixelPackingSrc == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVSa
            )
      iVal = 1024;
    else
      iVal = 0;

    for (int iNeighbor = 0; iNeighbor < iTotalNeighbor; iNeighbor++)
     {
      ipLUTWei[iSubPixel][iNeighbor] = (int *)malloc (iVal * sizeof(int));
      if (ipLUTWei[iSubPixel][iNeighbor] == 0)
       {
        return ERR_ASPECT_RATIO_MALLOC;
       }
     }
   }

  return 0;
}

void CAspectRatio::FreeWeights()
{
  free (fpSincAlloc);
  fpSincAlloc = 0;
  free (fpWindowAlloc);
  fpWindowAlloc = 0;
  free (fpFilterAlloc);
  fpFilterAlloc = 0;
  if (ipLUTWei)
   {
    for (int iSubPixel = 0; iSubPixel < iNSubPixel; iSubPixel++)
     {
      if (ipLUTWei[iSubPixel])
       {
        for (int iNeighbor = 0; iNeighbor < iTotalNeighbor; iNeighbor++)
         {
          free (ipLUTWei[iSubPixel][iNeighbor]);
          ipLUTWei[iSubPixel][iNeighbor] = 0;
         }
       }

      free (ipLUTWei[iSubPixel]);
      ipLUTWei[iSubPixel] = 0;
     }

    free (ipLUTWei);
    ipLUTWei= 0;
   }
}

int CAspectRatio::AllocSpatial ()
{
  // each pixel from iStartDst to iStopDst needs a sub-pixel value
  ipLUTSubPixel = (int *) malloc (iStopDst * sizeof(int));
  if (ipLUTSubPixel == 0)
   {
    return ERR_ASPECT_RATIO_MALLOC;
   }

  // each pixel from iStartDst to iStopDst needs a pixel value
  ipLUTPixel = (int *) malloc (iStopDst * sizeof(int));
  if (ipLUTPixel == 0)
   {
    return ERR_ASPECT_RATIO_MALLOC;
   }

  // The unpacked pixels are stored in 2 D array
  for (int iJob = 0; iJob < AR_JOB; iJob++) 
   {
    ucpUnpackedAlloc[iJob] = (MTI_UINT8 **) malloc
		(iUnpackedComponent * sizeof (MTI_UINT8 *));
    if (ucpUnpackedAlloc[iJob] == 0)
     {
      return ERR_ASPECT_RATIO_MALLOC;
     }

    for (int iComponent = 0; iComponent < iUnpackedComponent; iComponent++)
     {
      ucpUnpackedAlloc[iJob][iComponent] = 0;
     }

    ucpUnpacked[iJob] = (MTI_UINT8 **) malloc
		(iUnpackedComponent * sizeof (MTI_UINT8 *));
    if (ucpUnpacked[iJob] == 0)
     {
      return ERR_ASPECT_RATIO_MALLOC;
     }

    for (int iComponent = 0; iComponent < iUnpackedComponent; iComponent++)
     {
      ucpUnpackedAlloc[iJob][iComponent] = (MTI_UINT8 *) malloc
		((iUnpackedPixelStop-iUnpackedPixelStart) * sizeof (MTI_UINT8));
      if (ucpUnpackedAlloc[iJob][iComponent] == 0)
       {
        return ERR_ASPECT_RATIO_MALLOC;
       }

      ucpUnpacked[iJob][iComponent] = ucpUnpackedAlloc[iJob][iComponent] -
		iUnpackedPixelStart;
     }
   }

  return 0;
}

void CAspectRatio::FreeSpatial ()
{
  free (ipLUTSubPixel);
  ipLUTSubPixel = 0;

  free (ipLUTPixel);
  ipLUTPixel = 0;

  for (int iJob = 0; iJob < AR_JOB; iJob++)
   {
    if (ucpUnpackedAlloc[iJob])
     {
      for (int iComponent = 0; iComponent < iUnpackedComponent; iComponent++)
       {
        free (ucpUnpackedAlloc[iJob][iComponent]);
        ucpUnpackedAlloc[iJob][iComponent] = 0;
       }

      free (ucpUnpackedAlloc[iJob]);
      ucpUnpackedAlloc[iJob] = 0;
     }

    free (ucpUnpacked[iJob]);
    ucpUnpacked[iJob] = 0;
   }

}

void CallDoRender (void *vp, int iJob)
{
  CAspectRatio *arp = (CAspectRatio *)vp;

  arp->iRenderReturn = arp->DoRender (arp->ucpRenderSrc, arp->ucpRenderDst, iJob);
}

int CAspectRatio::Init (const CImageFormat *ifpSrc, const CImageFormat *ifpDst)
{
  int iRet;

  // free from a previous run
  Free ();

  // make local copy of parameters
  iNRowSrc = ifpSrc->getLinesPerFrame();
  iNColSrc = ifpSrc->getPixelsPerLine();
  iPitchSrc = ifpSrc->getFramePitch();
  iPixelPackingSrc = ifpSrc->getPixelPacking();
  iColorSpaceSrc = ifpSrc->getColorSpace();
  iNFieldSrc = ifpSrc->getFieldCount();

  iNRowDst = ifpDst->getLinesPerFrame();
  iNColDst = ifpDst->getPixelsPerLine();
  iPitchDst = ifpDst->getFramePitch();
  iPixelPackingDst = ifpDst->getPixelPacking();
  iColorSpaceDst = ifpDst->getColorSpace();
  iNFieldDst = ifpDst->getFieldCount();

  // check for error conditions
  if (
	iPixelPackingSrc != IF_PIXEL_PACKING_8Bits_IN_1Byte &&
	iPixelPackingSrc != IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS &&
	iPixelPackingSrc != IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVSa
     )
   {
    TRACE_0 (errout << "This source pixel packing (" << iPixelPackingSrc <<
		") is not supported by CAspectRatio");
    return ERR_ASPECT_RATIO_PIXEL_PACKING;
   }

  if (
	iPixelPackingDst != IF_PIXEL_PACKING_8Bits_IN_1Byte
     )
   {
    TRACE_0 (errout << "This destination pixel packing (" << iPixelPackingDst <<
		") is not supported by CAspectRatio");
    return ERR_ASPECT_RATIO_PIXEL_PACKING;
   }

  if (
	iColorSpaceSrc != IF_COLOR_SPACE_CCIR_601_BG &&
	iColorSpaceSrc != IF_COLOR_SPACE_CCIR_601_M &&
	iColorSpaceSrc != IF_COLOR_SPACE_CCIR_709
     )
   {
    TRACE_0 (errout << "This source color space (" << iColorSpaceSrc <<
		") is not supported by CAspectRatio");
    return ERR_ASPECT_RATIO_COLOR_SPACE;
   }

  if (
	iColorSpaceDst != IF_COLOR_SPACE_CCIR_601_BG &&
	iColorSpaceDst != IF_COLOR_SPACE_CCIR_601_M &&
	iColorSpaceDst != IF_COLOR_SPACE_CCIR_709
     )
   {
    TRACE_0 (errout << "This destination color space (" << iColorSpaceDst <<
		") is not supported by CAspectRatio");
    return ERR_ASPECT_RATIO_COLOR_SPACE;
   }

  if (iNFieldSrc != iNFieldDst || iNFieldSrc < 0 || iNFieldSrc > 2)
   {
    TRACE_0 (errout << "This field count (" << iNFieldSrc <<
		", " << iNFieldDst << ") is not supported by CAspectRatio");
    return ERR_ASPECT_RATIO_NUM_FIELD;
   }

  // allocate the mthreads
  tsThread.iNThread = AR_JOB;
  tsThread.iNJob = AR_JOB;
  tsThread.PrimaryFunction = &CallDoRender;
  tsThread.SecondaryFunction = NULL;
  tsThread.CallBackFunction = NULL;
  tsThread.vpApplicationData = this;

  iRet = MThreadAlloc (&tsThread);
  if (iRet)
   {
    TRACE_0 (errout << "Unable to allocate mthreads in CAspectRatio");
    return iRet;
   }

  iInit |= INIT_FORMAT;
  return 0;
}

int CAspectRatio::MakeFilter (float fZoomFactorArg, int iNNeighborArg,
	int iNSubPixelArg)
{
  int iRet;

  if ((iInit & INIT_FORMAT) == 0)
   {
    return ERR_ASPECT_RATIO_INIT;
   }


  // make copy of parameters
  iNNeighbor = iNNeighborArg;
  iTotalNeighbor = 2*iNNeighbor + 1;
  iNSubPixel = iNSubPixelArg;

  if (iNNeighbor == 0)
    iNSubPixel = 1;

  iFilterWidth = 2*iNNeighbor*iNSubPixel + 1;
  fZoomFactor = fZoomFactorArg;

  // allocate storage
  iRet = AllocWeights ();
  if (iRet)
    return iRet;

  // fill in the sinc
  GenerateSinc ();

  // fill in the window
  GenerateWindow ();

  // fill in the filter
  GenerateFilter ();

  // fill in the LUT
  GenerateLUT ();

  iInit |= INIT_FILTER;
  return 0;
}

int CAspectRatio::MakeSpatial (int iSizeDst, bool bHorizontalArg)
{
  int iRet;

  if ((iInit & INIT_FORMAT) == 0)
   {
    return ERR_ASPECT_RATIO_INIT;
   }

  bHorizontal = bHorizontalArg;
 
  iRet = SetSize (iSizeDst);
  if (iRet)
    return iRet;

  iRet = SetUnpacked ();
  if (iRet)
    return iRet;

  iRet = AllocSpatial ();
  if (iRet)
    return iRet;

  // fill in the sub pixel lookup
  // iStartDst maps to iStartSrc.  iStopDst maps to iStopDst
  // Src = fSlope * Dst + fIntercept
  float fSlope, fIntercept;
  fSlope = (float)(iStopSrc - iStartSrc) / (float)(iStopDst - iStartDst);
  fIntercept = (float)iStartSrc - fSlope * (float)iStartDst;

  float fPixelSrc;
  int iPixelSrc, iSubPixel;
  float fSubPixel;
  for (int iDst = iStartDst; iDst < iStopDst; iDst++)
   {
    // where does this destination map in the source
    fPixelSrc = fSlope * (float)iDst + fIntercept;

    // which source pixel does this fall in
    iPixelSrc = (int)fPixelSrc;

    // what is the sub pixel offset
    fSubPixel = fPixelSrc - (float)iPixelSrc;
    iSubPixel = fSubPixel * (float)iNSubPixel;

    ipLUTSubPixel[iDst] = iSubPixel;

    ipLUTPixel[iDst] = iPixelSrc - iNNeighbor;
   }

  iInit |= INIT_SPATIAL;
  return 0;
}

int CAspectRatio::SetSize (int iSizeDst)
{
  int iSizeSrc;

  iSizeSrc = (float)iSizeDst / fZoomFactor + .5;

  if (bHorizontal)
   {
    if (iSizeDst > iNColDst)
      return ERR_ASPECT_RATIO_SIZE_TOO_BIG;

    if (iSizeSrc > iNColSrc)
      return ERR_ASPECT_RATIO_FACTOR_TOO_BIG;

    iStartDst = (iNColDst - iSizeDst) / 2;
    iStopDst = iStartDst + iSizeDst;

    iStartSrc = (iNColSrc - iSizeSrc) / 2;
    iStopSrc = iStartSrc + iSizeSrc;
   }
  else
   {
    if (iSizeDst > iNRowDst)
      return ERR_ASPECT_RATIO_SIZE_TOO_BIG;

    if (iSizeSrc > iNRowSrc)
      return ERR_ASPECT_RATIO_FACTOR_TOO_BIG;

    iStartDst = (iNRowDst - iSizeDst) / 2;
    iStopDst = iStartDst + iSizeDst;

    iStartSrc = (iNRowSrc - iSizeSrc) / 2;
    iStopSrc = iStartSrc + iSizeSrc;
   }

  return 0;
}

int CAspectRatio::SetUnpacked ()
{
  if (bHorizontal)
   {
    if (
	iPixelPackingSrc == IF_PIXEL_PACKING_8Bits_IN_1Byte &&
       (iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_BG ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_M ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_709)
       )
     {
      iUnpackedPixelStart = iStartSrc - iNNeighbor;
      iUnpackedPixelStop = iStopSrc + iNNeighbor;

      // make sure we start on a U boundary
      if (fmodl(iUnpackedPixelStart,2) != 0)
        iUnpackedPixelStart--;
      if (fmodl(iUnpackedPixelStop,2) != 0)
        iUnpackedPixelStop++;

      iUnpackedComponent = 3;
     }
    else if (
       (iPixelPackingSrc == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS ||
	iPixelPackingSrc == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVSa) &&
       (iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_BG ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_M ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_709)
       )
     {
      iUnpackedPixelStart = iStartSrc - iNNeighbor;
      iUnpackedPixelStop = iStopSrc + iNNeighbor;

      // 3 10bits in 4 has a data period of 6 pixels
      while (fmodl(iUnpackedPixelStart,6) != 0)
       {
        iUnpackedPixelStart--;
       }
      while (fmodl(iUnpackedPixelStop,6) != 0)
       {
        iUnpackedPixelStop++;
       }

      iUnpackedComponent = 3;
     }
    else
     {
      return ERR_ASPECT_RATIO_UNPACKED;
     }
   }
  else
   {
    if (
	iPixelPackingSrc == IF_PIXEL_PACKING_8Bits_IN_1Byte &&
       (iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_BG ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_M ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_709)
       )
     {
      iUnpackedPixelStart = iStartSrc - iNNeighbor;
      iUnpackedPixelStop = iStopSrc + iNNeighbor;

      // vertical extraction is faster by using runs of columns
      // for this packing, multiples of 2 (i.e. 1 pairs) are required
      iUnpackedComponent = VERTICAL_COMPONENTS; //12;  // UY, VY, UY, VY, UY, VY
     }
    else if (
       (iPixelPackingSrc == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS ||
	iPixelPackingSrc == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVSa) &&
       (iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_BG ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_M ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_709)
       )
     {
      // 3 10bits in 4 has a data period of 6 pixels
      iUnpackedPixelStart = iStartSrc - iNNeighbor;
      iUnpackedPixelStop = iStopSrc + iNNeighbor;

      // vertical extraction is faster by using runs of columns
      // for this packing, multiples of 12 (i.e. 6 pairs) are required
      iUnpackedComponent = VERTICAL_COMPONENTS; //12;  // UYV, YUY, VYU, YVY
     }
    else
     {
      return ERR_ASPECT_RATIO_UNPACKED;
     }
   }

  return 0;
}

int CAspectRatio::Render (MTI_UINT8 *ucpSrc[2], MTI_UINT8 *ucpDst[2])
{
  int iRet;

  ucpRenderSrc[0] = ucpSrc[0];
  ucpRenderSrc[1] = ucpSrc[1];
  ucpRenderDst[0] = ucpDst[0];
  ucpRenderDst[1] = ucpDst[1];

  iRet = MThreadStart (&tsThread);

  if (iRet == 0)
   {
    iRet = iRenderReturn;
   }

  return iRet;
}

int CAspectRatio::DoRender (MTI_UINT8 *ucpSrc[2], MTI_UINT8 *ucpDst[2], int iJob)
{
  int iRet;

  if (iInit != INIT_ALL)
   {
    return ERR_ASPECT_RATIO_INIT;
   }

  int iRow0 = (iJob) * iNRowSrc / AR_JOB;
  int iRow1 = (iJob+1) * iNRowSrc / AR_JOB;
  int iCol0 = (iJob) * iNColSrc / AR_JOB;
  int iCol1 = (iJob+1) * iNColSrc / AR_JOB;

  // make sure row start and stop are even
  iRow0 = (iRow0 / 2) * 2;
  iRow1 = (iRow1 / 2) * 2;

  // make sure col start and stop are multiples of VERTICAL_COMPONENTS
  iCol0 = (iCol0 / VERTICAL_COMPONENTS) * VERTICAL_COMPONENTS;
  iCol1 = (iCol1 / VERTICAL_COMPONENTS) * VERTICAL_COMPONENTS;

  if (bHorizontal)
   {
    if (
	iPixelPackingSrc == IF_PIXEL_PACKING_8Bits_IN_1Byte &&
       (iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_BG ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_M ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_709) &&
	iNFieldSrc == 2
       )
     {
      // this is 8 bit YUV data with 2 fields
      iRet = RenderH_8_YUV_2_TO_8_YUV_2 (ucpSrc, ucpDst, iRow0, iRow1,
		iJob);
     }
    else if (
	iPixelPackingSrc == IF_PIXEL_PACKING_8Bits_IN_1Byte &&
       (iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_BG ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_M ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_709) &&
	iNFieldSrc == 1
       )
     {
      // this is 8 bit YUV data with 1 field
      iRet = RenderH_8_YUV_1_TO_8_YUV_1 (ucpSrc, ucpDst, iRow0, iRow1,
		iJob);
     }
    else if (
	iPixelPackingSrc == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS &&
       (iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_BG ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_M ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_709) &&
	iNFieldSrc == 2
       )
     {
      // this is 10 bit  DVS YUV data with 2 fields
      iRet = RenderH_10_YUV_2_TO_8_YUV_2 (ucpSrc, ucpDst, iRow0, iRow1,
		iJob);
     }
    else if (
	iPixelPackingSrc == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS &&
       (iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_BG ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_M ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_709) &&
	iNFieldSrc == 1
       )
     {
      // this is 10 bit DVS YUV data with 1 field
      iRet = RenderH_10_YUV_1_TO_8_YUV_1 (ucpSrc, ucpDst, iRow0, iRow1,
		iJob);
     }
    else if (
	iPixelPackingSrc == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVSa &&
       (iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_BG ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_M ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_709) &&
	iNFieldSrc == 2
       )
     {
      // this is 10 bit DVSa YUV data with 2 fields
      iRet = RenderH_10a_YUV_2_TO_8_YUV_2 (ucpSrc, ucpDst, iRow0, iRow1,
		iJob);
     }
    else if (
	iPixelPackingSrc == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVSa &&
       (iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_BG ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_M ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_709) &&
	iNFieldSrc == 1
       )
     {
      // this is 10 bit DVSa YUV data with 2 fields
      iRet = RenderH_10a_YUV_1_TO_8_YUV_1 (ucpSrc, ucpDst, iRow0, iRow1,
		iJob);
     }
    else
     {
      iRet = ERR_ASPECT_RATIO_RENDER;
     }
   }
  else
   {
    if (
	iPixelPackingSrc == IF_PIXEL_PACKING_8Bits_IN_1Byte &&
       (iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_BG ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_M ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_709) &&
	iNFieldSrc == 2
       )
     {
      // this is 8 bit YUV data with 2 fields
      iRet = RenderV_8_YUV_2_TO_8_YUV_2 (ucpSrc, ucpDst, iCol0, iCol1,
		iJob);
     }
    else if (
	iPixelPackingSrc == IF_PIXEL_PACKING_8Bits_IN_1Byte &&
       (iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_BG ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_M ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_709) &&
	iNFieldSrc == 1
       )
     {
      // this is 8 bit YUV data with 2 fields
      iRet = RenderV_8_YUV_1_TO_8_YUV_1 (ucpSrc, ucpDst, iCol0, iCol1,
		iJob);
     }
    else if (
	iPixelPackingSrc == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS &&
       (iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_BG ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_M ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_709) &&
	iNFieldSrc == 2
       )
     {
      // this is 10 bit DVS YUV data with 2 fields
      iRet = RenderV_10_YUV_2_TO_8_YUV_2 (ucpSrc, ucpDst, iCol0, iCol1,
		iJob);
     }
    else if (
	iPixelPackingSrc == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS &&
       (iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_BG ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_M ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_709) &&
	iNFieldSrc == 1
       )
     {
      // this is 10 bit DVS YUV data with 2 fields
      iRet = RenderV_10_YUV_1_TO_8_YUV_1 (ucpSrc, ucpDst, iCol0, iCol1,
		iJob);
     }
    else if (
	iPixelPackingSrc == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVSa &&
       (iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_BG ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_M ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_709) &&
	iNFieldSrc == 2
       )
     {
      // this is 10 bit DVSa YUV data with 2 fields
      iRet = RenderV_10a_YUV_2_TO_8_YUV_2 (ucpSrc, ucpDst, iCol0, iCol1,
		iJob);
     }
    else if (
	iPixelPackingSrc == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVSa &&
       (iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_BG ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_601_M ||
	iColorSpaceSrc == IF_COLOR_SPACE_CCIR_709) &&
	iNFieldSrc == 1
       )
     {
      // this is 10 bit DVSa YUV data with 2 fields
      iRet = RenderV_10a_YUV_1_TO_8_YUV_1 (ucpSrc, ucpDst, iCol0, iCol1,
		iJob);
     }
    else
     {
      iRet = ERR_ASPECT_RATIO_RENDER;
     }
   }

  return iRet;
}

#ifdef DO_100_TIMES
CHRTimer hrt;
int iTTTUnpack, iTTTRender, iTTTTotal;
#endif

int CAspectRatio::RenderH_8_YUV_2_TO_8_YUV_2 (MTI_UINT8 *ucpSrc[2],
	MTI_UINT8 *ucpDst[2], int iRow0, int iRow1, int iJob)
{
  // this code does a HORIZONTAL render Y 8-bit YUV data with two fields

  int iRow, iField, iR;
  MTI_UINT8 *ucpS, *ucpD;


#ifdef DO_100_TIMES
  int iTTTTmp;
iTTTUnpack=0; iTTTRender=0; iTTTTotal=0;
  hrt.Start();


  iTTTTotal = hrt.Read();
  for (int i = 0; i < 100; ++i)
#endif
  for (iRow = iRow0; iRow < iRow1; iRow++)
   {
    iField = iRow % 2;
    iR = iRow / 2;

    ucpD = ucpDst[iField] + (iR * iPitchDst);
    ucpS = ucpSrc[iField] + (iR * iPitchSrc);

#ifdef DO_100_TIMES
    iTTTTmp = hrt.Read();
#endif
    UnpackH_8_YUV (ucpS, iJob);
#ifdef DO_100_TIMES
    iTTTUnpack += (hrt.Read() - iTTTTmp);

    iTTTTmp = hrt.Read();
#endif
    RenderH_YUV (ucpD, iJob);
#ifdef DO_100_TIMES
    iTTTRender += (hrt.Read() - iTTTTmp);
#endif
   }
#ifdef DO_100_TIMES
  iTTTTotal = hrt.Read() - iTTTTotal;

TRACE_0 (errout << "TTT HORIZONTAL Total: " << iTTTTotal/100 << "." << ((iTTTTotal/10)%10)
                            << ", unpack: " << iTTTUnpack/100 << "." << ((iTTTUnpack/10)%10)
                            << ", render: " << iTTTRender/100 << "." << ((iTTTRender/10)%10));
#endif

  return 0;
}

int CAspectRatio::RenderH_8_YUV_1_TO_8_YUV_1 (MTI_UINT8 *ucpSrc[2],
	MTI_UINT8 *ucpDst[2], int iRow0, int iRow1, int iJob)
{
  // this code does a HORIZONTAL render Y 8-bit YUV data with one fields

  int iRow;
  MTI_UINT8 *ucpS, *ucpD;

#ifdef DO_100_TIMES
  int iTTTTmp;
  iTTTUnpack=0; iTTTRender=0; iTTTTotal=0;
  hrt.Start();

  iTTTTotal = hrt.Read();
  for (int i = 0; i < 100; ++i)
#endif
  for (iRow = iRow0; iRow < iRow1; iRow++)
   {
    ucpD = ucpDst[0] + (iRow * iPitchDst);
    ucpS = ucpSrc[0] + (iRow * iPitchSrc);

#ifdef DO_100_TIMES
    iTTTTmp = hrt.Read();
#endif
    UnpackH_8_YUV (ucpS, iJob);
#ifdef DO_100_TIMES
    iTTTUnpack += (hrt.Read() - iTTTTmp);

    iTTTTmp = hrt.Read();
#endif
    RenderH_YUV (ucpD, iJob);
#ifdef DO_100_TIMES
    iTTTRender += (hrt.Read() - iTTTTmp);
#endif
   }
#ifdef DO_100_TIMES
  iTTTTotal = hrt.Read() - iTTTTotal;

TRACE_0 (errout << "TTT HORIZONTAL Total: " << iTTTTotal/100 << "." << ((iTTTTotal/10)%10)
                            << ", unpack: " << iTTTUnpack/100 << "." << ((iTTTUnpack/10)%10)
                            << ", render: " << iTTTRender/100 << "." << ((iTTTRender/10)%10));
#endif

  return 0;
}

int CAspectRatio::RenderH_10_YUV_2_TO_8_YUV_2 (MTI_UINT8 *ucpSrc[2],
	MTI_UINT8 *ucpDst[2], int iRow0, int iRow1, int iJob)
{
  // this code does a HORIZONTAL render Y 10-bit DVS YUV data with two fields

  int iRow, iField, iR;
  MTI_UINT8 *ucpS, *ucpD;

  for (iRow = iRow0; iRow < iRow1; iRow++)
   {
    iField = iRow % 2;
    iR = iRow / 2;

    ucpD = ucpDst[iField] + (iR * iPitchDst);
    ucpS = ucpSrc[iField] + (iR * iPitchSrc);

    UnpackH_10_YUV (ucpS, iJob);
    RenderH_YUV (ucpD, iJob);
   }

  return 0;
}

int CAspectRatio::RenderH_10_YUV_1_TO_8_YUV_1 (MTI_UINT8 *ucpSrc[2],
	MTI_UINT8 *ucpDst[2], int iRow0, int iRow1, int iJob)
{
  // this code does a HORIZONTAL render Y 10-bit DVS YUV data with one field

  int iRow;
  MTI_UINT8 *ucpS, *ucpD;

#ifdef DO_100_TIMES
  int iTTTTmp;
iTTTUnpack=0; iTTTRender=0; iTTTTotal=0;
  hrt.Start();


  iTTTTotal = hrt.Read();
  for (int i = 0; i < 100; ++i)
#endif
  for (iRow = iRow0; iRow < iRow1; iRow++)
   {
    ucpD = ucpDst[0] + (iRow * iPitchDst);
    ucpS = ucpSrc[0] + (iRow * iPitchSrc);

#ifdef DO_100_TIMES
    iTTTTmp = hrt.Read();
#endif
    UnpackH_10_YUV (ucpS, iJob);
#ifdef DO_100_TIMES
    iTTTUnpack += (hrt.Read() - iTTTTmp);

    iTTTTmp = hrt.Read();
#endif
    
    RenderH_YUV (ucpD, iJob);
#ifdef DO_100_TIMES
    iTTTRender += (hrt.Read() - iTTTTmp);
#endif
   }
#ifdef DO_100_TIMES
  iTTTTotal = hrt.Read() - iTTTTotal;

TRACE_0 (errout << "TTT HORIZONTAL Total: " << iTTTTotal/100 << "." << ((iTTTTotal/10)%10)
                            << ", unpack: " << iTTTUnpack/100 << "." << ((iTTTUnpack/10)%10)
                            << ", render: " << iTTTRender/100 << "." << ((iTTTRender/10)%10));
#endif

  return 0;
}

int CAspectRatio::RenderH_10a_YUV_2_TO_8_YUV_2 (MTI_UINT8 *ucpSrc[2],
	MTI_UINT8 *ucpDst[2], int iRow0, int iRow1, int iJob)
{
  // this code does a HORIZONTAL render Y 10-bit DVSa YUV data with two fields

  int iRow, iField, iR;
  MTI_UINT8 *ucpS, *ucpD;

  for (iRow = iRow0; iRow < iRow1; iRow++)
   {
    iField = iRow % 2;
    iR = iRow / 2;

    ucpD = ucpDst[iField] + (iR * iPitchDst);
    ucpS = ucpSrc[iField] + (iR * iPitchSrc);

    UnpackH_10a_YUV (ucpS, iJob);
    RenderH_YUV (ucpD, iJob);
   }

  return 0;
}

int CAspectRatio::RenderH_10a_YUV_1_TO_8_YUV_1 (MTI_UINT8 *ucpSrc[2],
	MTI_UINT8 *ucpDst[2], int iRow0, int iRow1, int iJob)
{
  // this code does a HORIZONTAL render Y 10-bit DVSa YUV data with one field

  int iRow;
  MTI_UINT8 *ucpS, *ucpD;

  for (iRow = iRow0; iRow < iRow1; iRow++)
   {
    ucpD = ucpDst[0] + (iRow * iPitchDst);
    ucpS = ucpSrc[0] + (iRow * iPitchSrc);

    UnpackH_10a_YUV (ucpS, iJob);
    
    RenderH_YUV (ucpD, iJob);
   }

  return 0;
}


void CAspectRatio::RenderH_YUV (MTI_UINT8 *ucpD, int iJob)
{
  int iCol, iSubPixel, iPixel;
  MTI_UINT8 *ucpUnpackY, *ucpUnpackUV;
  int iSumY, iSumUV, *ipWei;
  int iNei;

  // unroll the loop
  for (iCol = 0; iCol < iStartDst-4; iCol+=5)
   {
    ucpD[0] = 128;	// U or V
    ucpD[1] = 16;	// Y

    ucpD[2] = 128;	// U or V
    ucpD[3] = 16;	// Y

    ucpD[4] = 128;	// U or V
    ucpD[5] = 16;	// Y

    ucpD[6] = 128;	// U or V
    ucpD[7] = 16;	// Y

    ucpD[8] = 128;	// U or V
    ucpD[9] = 16;	// Y

    ucpD += 10;
   }

  for ( ; iCol < iStartDst; iCol++)
   {
    ucpD[0] = 128;	// U or V
    ucpD[1] = 16;	// Y

    ucpD += 2;
   }

  // make sure iCol is even
  if (iCol & 0x1)
   {
    iSubPixel = ipLUTSubPixel[iCol];
    iPixel = ipLUTPixel[iCol];
    ucpUnpackY = ucpUnpacked[iJob][0];   // Y values
    ucpUnpackUV = ucpUnpacked[iJob][2];  // V value

    ucpUnpackY += iPixel;
    ucpUnpackUV += iPixel;

    iSumY = 0;
    iSumUV = 0;
    for (iNei = 0; iNei < iTotalNeighbor; iNei++)
     {
      ipWei = ipLUTWei[iSubPixel][iNei];

      iSumY += ipWei[*ucpUnpackY++];
      iSumUV += ipWei[*ucpUnpackUV++];
     }

    // clamp the result 
    if (iSumY > iClampMax)
      iSumY = iClampMax;
    else if (iSumY < iClampMin)
      iSumY = iClampMin;

    if (iSumUV > iClampMax)
      iSumUV = iClampMax;
    else if (iSumUV < iClampMin)
      iSumUV = iClampMin;

    // round the result
    iSumY = ((iSumY + iHalfPrecision) >> PRECISION);
    iSumUV = ((iSumUV + iHalfPrecision) >> PRECISION);

    *ucpD++ = iSumUV;  // UV
    *ucpD++ = iSumY;  // Y

    iCol++;
   }

  int iStopDstLocal = iStopDst;
  if (iStopDstLocal & 0x1)
   {
    iStopDstLocal--;
   }


  if (iTotalNeighbor == 1)
   {
    RenderH_YUV_1 (ucpD, iCol, iStopDstLocal, iJob);
   }
  else if (iTotalNeighbor == 3)
   {
    RenderH_YUV_3 (ucpD, iCol, iStopDstLocal, iJob);
   }
  else if (iTotalNeighbor == 5)
   {
    RenderH_YUV_5 (ucpD, iCol, iStopDstLocal, iJob);
   }
  else if (iTotalNeighbor == 7)
   {
    RenderH_YUV_7 (ucpD, iCol, iStopDstLocal, iJob);
   }
  else if (iTotalNeighbor == 9)
   {
    RenderH_YUV_9 (ucpD, iCol, iStopDstLocal, iJob);
   }
  else if (iTotalNeighbor == 11)
   {
    RenderH_YUV_11 (ucpD, iCol, iStopDstLocal, iJob);
   }
  else if (iTotalNeighbor == 13)
   {
    RenderH_YUV_13 (ucpD, iCol, iStopDstLocal, iJob);
   }
  else if (iTotalNeighbor == 15)
   {
    RenderH_YUV_15 (ucpD, iCol, iStopDstLocal, iJob);
   }
  else
   {
    RenderH_YUV (ucpD, iCol, iStopDstLocal, iJob);
   }

  // take care of case where iStopDst is odd
  if (iStopDst & 0x1)
   {
    iSubPixel = ipLUTSubPixel[iCol];
    iPixel = ipLUTPixel[iCol];
    ucpUnpackY = ucpUnpacked[iJob][0];   // Y values
    ucpUnpackUV = ucpUnpacked[iJob][2];  // V value

    ucpUnpackY += iPixel;
    ucpUnpackUV += iPixel;

    iSumY = 0;
    iSumUV = 0;
    for (iNei = 0; iNei < iTotalNeighbor; iNei++)
     {
      ipWei = ipLUTWei[iSubPixel][iNei];

      iSumY += ipWei[*ucpUnpackY++];
      iSumUV += ipWei[*ucpUnpackUV++];
     }

    // clamp the result 
    if (iSumY > iClampMax)
      iSumY = iClampMax;
    else if (iSumY < iClampMin)
      iSumY = iClampMin;

    if (iSumUV > iClampMax)
      iSumUV = iClampMax;
    else if (iSumUV < iClampMin)
      iSumUV = iClampMin;

    // round the result
    iSumY = ((iSumY + iHalfPrecision) >> PRECISION);
    iSumUV = ((iSumUV + iHalfPrecision) >> PRECISION);

    *ucpD++ = iSumUV;  // UV
    *ucpD++ = iSumY;  // Y

    iCol++;
   }

  // unroll the loop
  for ( ; iCol < iNColDst-4; iCol+=5)
   {
    ucpD[0] = 128;	// U or V
    ucpD[1] = 16;	// Y

    ucpD[2] = 128;	// U or V
    ucpD[3] = 16;	// Y

    ucpD[4] = 128;	// U or V
    ucpD[5] = 16;	// Y

    ucpD[6] = 128;	// U or V
    ucpD[7] = 16;	// Y

    ucpD[8] = 128;	// U or V
    ucpD[9] = 16;	// Y

    ucpD += 10;
   }

  for ( ; iCol < iNColDst; iCol++)
   {
    ucpD[0] = 128;	// U or V
    ucpD[1] = 16;	// Y

    ucpD += 2;
   }
}


void CAspectRatio::UnpackH_8_YUV (MTI_UINT8 *ucpSrc, int iJob)
{
  MTI_UINT8 *ucpS;
  int iCol, iColStop;
  MTI_UINT8 *ucp[3], ucpTmp[3][8];

  ucp[0] = ucpUnpacked[iJob][0] + iUnpackedPixelStart;
  ucp[1] = ucpUnpacked[iJob][1] + iUnpackedPixelStart;
  ucp[2] = ucpUnpacked[iJob][2] + iUnpackedPixelStart;

  if (iUnpackedPixelStart < 0)
   {
    ucpS = ucpSrc;
   }
  else
   {
    // UnpackedPixelStart is chosen to have proper phase
    ucpS = ucpSrc + ( (iUnpackedPixelStart/2) * 4 );
   }

  for (iCol = iUnpackedPixelStart; iCol < 0; iCol++)
   {
    *ucp[0]++ = ucpS[1];	// Y
    *ucp[1]++ = ucpS[0];	// U
    *ucp[2]++ = ucpS[2];	// V
   }

  iColStop = iUnpackedPixelStop;
  if (iColStop > iNColSrc)
    iColStop = iNColSrc;

  for (; iCol < (iColStop - 7); iCol+=8)
   {
    ucpTmp[0][0] = ucpS[1];	// Y
    ucpTmp[1][0] = ucpS[0];	// U
    ucpTmp[2][0] = ucpS[2];	// V

    ucpTmp[0][1] = ucpS[3];	// Y
    ucpTmp[1][1] = ucpS[0];	// U
    ucpTmp[2][1] = ucpS[2];	// V

    ucpTmp[0][2] = ucpS[5];	// Y
    ucpTmp[1][2] = ucpS[4];	// U
    ucpTmp[2][2] = ucpS[6];	// V

    ucpTmp[0][3] = ucpS[7];	// Y
    ucpTmp[1][3] = ucpS[4];	// U
    ucpTmp[2][3] = ucpS[6];	// V

    ucpTmp[0][4] = ucpS[9];	// Y
    ucpTmp[1][4] = ucpS[8];	// U
    ucpTmp[2][4] = ucpS[10];	// V

    ucpTmp[0][5] = ucpS[11];	// Y
    ucpTmp[1][5] = ucpS[8];	// U
    ucpTmp[2][5] = ucpS[10];	// V

    ucpTmp[0][6] = ucpS[13];	// Y
    ucpTmp[1][6] = ucpS[12];	// U
    ucpTmp[2][6] = ucpS[14];	// V

    ucpTmp[0][7] = ucpS[15];	// Y
    ucpTmp[1][7] = ucpS[12];	// U
    ucpTmp[2][7] = ucpS[14];	// V

    *(long *) &ucp[0][0] = *( long *) &ucpTmp[0][0];
    *(long *) &ucp[0][4] = *( long *) &ucpTmp[0][4];
    *(long *) &ucp[1][0] = *( long *) &ucpTmp[1][0];
    *(long *) &ucp[1][4] = *( long *) &ucpTmp[1][4];
    *(long *) &ucp[2][0] = *( long *) &ucpTmp[2][0];
    *(long *) &ucp[2][4] = *( long *) &ucpTmp[2][4];

    ucpS += 16;
    ucp[0] += 8;
    ucp[1] += 8;
    ucp[2] += 8;
   }
  for (; iCol < (iColStop - 1); iCol+=2)
   {
    ucp[0][0] = ucpS[1];	// Y
    ucp[1][0] = ucpS[0];	// U
    ucp[2][0] = ucpS[2];	// V

    ucp[0][1] = ucpS[3];	// Y
    ucp[1][1] = ucpS[0];	// U
    ucp[2][1] = ucpS[2];	// V

    ucpS += 4;
    ucp[0] += 2;
    ucp[1] += 2;
    ucp[2] += 2;
   }
  for (; iCol < iUnpackedPixelStop; iCol++)
   {
    *ucp[0]++ = ucpS[3];	// Y
    *ucp[1]++ = ucpS[0];	// U
    *ucp[2]++ = ucpS[2];	// V
   }


}

void CAspectRatio::UnpackH_10_YUV (MTI_UINT8 *ucpSrc, int iJob)
{
  MTI_UINT8 *ucpS;
  int iCol, iColStop;
  MTI_UINT8 *ucp0, *ucp1, *ucp2;

  ucp0 = ucpUnpacked[iJob][0] + iUnpackedPixelStart;
  ucp1 = ucpUnpacked[iJob][1] + iUnpackedPixelStart;
  ucp2 = ucpUnpacked[iJob][2] + iUnpackedPixelStart;

  if (iUnpackedPixelStart < 0)
   {
    ucpS = ucpSrc;
   }
  else
   {
    // UnpackedPixelStart is chosen to have proper phase
    ucpS = ucpSrc + ( (iUnpackedPixelStart/6) * 16);
   }

  for (iCol = iUnpackedPixelStart; iCol < 0; iCol++)
   {
    // turn the 10 bit values into 8 bits
    *ucp0++ = ((ucpS[2] & 0x0F)<<4) + ((ucpS[1] & 0xF0)>>4);    // Y
    *ucp1++ = ((ucpS[1] & 0x03)<<6) + ((ucpS[0] & 0xFC)>>2);    // U
    *ucp2++ = ((ucpS[3] & 0x3F)<<2) + ((ucpS[2] & 0xC0)>>6);    // V
   }

  iColStop = iUnpackedPixelStop;
  if (iColStop > iNColSrc)
    iColStop = iNColSrc;

#if !MTI_ASM_X86_INTEL
  for (; iCol < iColStop; iCol+=6)
   {
#if 1
    // turn the 10 bit values into 8 bits
    *ucp0++ = ((ucpS[ 2] & 0x0F)<<4) + ((ucpS[ 1] & 0xF0)>>4);     // Y
    *ucp1++ = ((ucpS[ 1] & 0x03)<<6) + ((ucpS[ 0] & 0xFC)>>2);     // U
    *ucp2++ = ((ucpS[ 3] & 0x3F)<<2) + ((ucpS[ 2] & 0xC0)>>6);     // V

    *ucp0++ = ((ucpS[ 5] & 0x03)<<6) + ((ucpS[ 4] & 0xFC)>>2);     // Y
    *ucp1++ = ((ucpS[ 1] & 0x03)<<6) + ((ucpS[ 0] & 0xFC)>>2);     // U
    *ucp2++ = ((ucpS[ 3] & 0x3F)<<2) + ((ucpS[ 2] & 0xC0)>>6);     // V

    *ucp0++ = ((ucpS[ 7] & 0x3F)<<2) + ((ucpS[ 6] & 0xC0)>>6);     // Y
    *ucp1++ = ((ucpS[ 6] & 0x0F)<<4) + ((ucpS[ 5] & 0xF0)>>4);     // U
    *ucp2++ = ((ucpS[ 9] & 0x03)<<6) + ((ucpS[ 8] & 0xFC)>>2);     // V

    *ucp0++ = ((ucpS[10] & 0x0F)<<4) + ((ucpS[ 9] & 0xF0)>>4);     // Y
    *ucp1++ = ((ucpS[ 6] & 0x0F)<<4) + ((ucpS[ 5] & 0xF0)>>4);     // U
    *ucp2++ = ((ucpS[ 9] & 0x03)<<6) + ((ucpS[ 8] & 0xFC)>>2);     // V

    *ucp0++ = ((ucpS[13] & 0x03)<<6) + ((ucpS[12] & 0xFC)>>2);     // Y
    *ucp1++ = ((ucpS[11] & 0x3F)<<2) + ((ucpS[10] & 0xC0)>>6);     // U
    *ucp2++ = ((ucpS[14] & 0x0F)<<4) + ((ucpS[13] & 0xF0)>>4);     // V

    *ucp0++ = ((ucpS[15] & 0x3F)<<2) + ((ucpS[14] & 0xC0)>>6);     // Y
    *ucp1++ = ((ucpS[11] & 0x3F)<<2) + ((ucpS[10] & 0xC0)>>6);     // U
    *ucp2++ = ((ucpS[14] & 0x0F)<<4) + ((ucpS[13] & 0xF0)>>4);     // V
#else
    // turn the 10 bit values into 8 bits - optimized
    ucp0[0] = ((ucpS[2] & 0x0F)<<4) + ((ucpS[1] & 0xF0)>>4);              // Y
    ucp1[0] = ucp1[1] = ((ucpS[1] & 0x03)<<6) + ((ucpS[0] & 0xFC)>>2);    // U
    ucp2[0] = ucp2[1] = ((ucpS[3] & 0x3F)<<2) + ((ucpS[2] & 0xC0)>>6);    // V
    ucp0[1] = ((ucpS[ 5] & 0x03)<<6) + ((ucpS[4] & 0xFC)>>2);             // Y

    ucp0[2] = ((ucpS[ 7] & 0x3F)<<2) + ((ucpS[6] & 0xC0)>>6);             // Y
    ucp1[2] = ucp1[3] = ((ucpS[ 6] & 0x0F)<<4) + ((ucpS[5] & 0xF0)>>4);   // U
    ucp2[2] = ucp2[3] = ((ucpS[ 9] & 0x03)<<6) + ((ucpS[8] & 0xFC)>>2);   // V
    ucp0[3] = ((ucpS[10] & 0x0F)<<4) + ((ucpS[9] & 0xF0)>>4);             // Y

    ucp0[4] = ((ucpS[13] & 0x03)<<6) + ((ucpS[12] & 0xFC)>>2);            // Y
    ucp1[4] = ucp1[5] = ((ucpS[11] & 0x3F)<<2) + ((ucpS[10] & 0xC0)>>6);  // U
    ucp2[4] = ucp2[5] = ((ucpS[14] & 0x0F)<<4) + ((ucpS[13] & 0xF0)>>4);  // V
    ucp0[5] = ((ucpS[15] & 0x3F)<<2) + ((ucpS[14] & 0xC0)>>6);            // Y

    ucp0 += 6;
    ucp1 += 6;
    ucp2 += 6;
#endif

    ucpS += 16;
   }

#else  // OPTIMIZED IN ASSEMBLER
  _asm {
  mov    esi,ucpS
  mov    ebx,ucp0
  mov    ecx,ucp1
  mov    edx,ucp2

$L0:

  mov    eax,iCol
  cmp    eax,iColStop
  jge    $L1

  mov    eax,[esi]
  shr    eax,2
  mov    [ecx],al       // U
  mov    [ecx+1],al

  shr    eax,10
  mov    [ebx],al       // Y

  shr    eax,10
  mov    [edx],al       // V
  mov    [edx+1],al

  mov    eax,[esi+4]
  shr    eax,2
  mov    [ebx+1],al     // Y

  shr    eax,10
  mov    [ecx+2],al     // U
  mov    [ecx+3],al

  shr    eax,10
  mov    [ebx+2],al     // Y

  mov    eax,[esi+8]
  shr    eax,2
  mov    [edx+2],al     // V
  mov    [edx+3],al

  shr    eax,10
  mov    [ebx+3],al     // Y

  shr    eax,10
  mov    [ecx+4],al     // U
  mov    [ecx+5],al

  mov    eax,[esi+12]
  shr    eax,2
  mov    [ebx+4],al     // Y

  shr    eax,10
  mov    [edx+4],al     // V
  mov    [edx+5],al

  shr    eax,10
  mov    [ebx+5],al     // Y

  mov    eax,6
  add    ebx,eax
  add    ecx,eax
  add    edx,eax

  add    esi,16

  add    iCol,eax       // end FOR loop
  jmp    $L0

$L1:
  }
#endif

  ucpS -= 16;
  for (; iCol < iUnpackedPixelStop; iCol++)
   {
    // turn the 10 bit values into 8 bits
    *ucp0++ = ((ucpS[15] & 0x3F)<<2) + ((ucpS[14] & 0xC0)>>6);    // Y
    *ucp1++ = ((ucpS[11] & 0x3F)<<2) + ((ucpS[10] & 0xC0)>>6);    // U
    *ucp2++ = ((ucpS[14] & 0x0F)<<4) + ((ucpS[13] & 0xF0)>>4);    // V
   }

}

void CAspectRatio::UnpackH_10a_YUV (MTI_UINT8 *ucpSrc, int iJob)
{
  MTI_UINT8 *ucpS;
  int iCol, iColStop;
  MTI_UINT8 *ucp0, *ucp1, *ucp2;

  ucp0 = ucpUnpacked[iJob][0] + iUnpackedPixelStart;
  ucp1 = ucpUnpacked[iJob][1] + iUnpackedPixelStart;
  ucp2 = ucpUnpacked[iJob][2] + iUnpackedPixelStart;

  if (iUnpackedPixelStart < 0)
   {
    ucpS = ucpSrc;
   }
  else
   {
    // UnpackedPixelStart is chosen to have proper phase
    ucpS = ucpSrc + ( (iUnpackedPixelStart/6) * 16);
   }

  for (iCol = iUnpackedPixelStart; iCol < 0; iCol++)
   {
    // turn the 10 bit values into 8 bits
    *ucp0++ = ucpS[1];  // Y
    *ucp1++ = ucpS[0];  // U
    *ucp2++ = ucpS[2];  // V
   }

  iColStop = iUnpackedPixelStop;
  if (iColStop > iNColSrc)
    iColStop = iNColSrc;

#if !MTI_ASM_X86_INTEL
  for (; iCol < iColStop; iCol+=6)
   {
#if 0
    // turn the 10 bit values into 8 bits
    *ucp0++ = ucpS[ 1];  // Y
    *ucp1++ = ucpS[ 0];  // U
    *ucp2++ = ucpS[ 2];  // V

    *ucp0++ = ucpS[ 4];  // Y
    *ucp1++ = ucpS[ 0];  // U
    *ucp2++ = ucpS[ 2];  // V

    *ucp0++ = ucpS[ 6];  // Y
    *ucp1++ = ucpS[ 5];  // U
    *ucp2++ = ucpS[ 8];  // V

    *ucp0++ = ucpS[ 9];  // Y
    *ucp1++ = ucpS[ 5];  // U
    *ucp2++ = ucpS[ 8];  // V

    *ucp0++ = ucpS[12];  // Y
    *ucp1++ = ucpS[10];  // U
    *ucp2++ = ucpS[13];  // V

    *ucp0++ = ucpS[14];  // Y
    *ucp1++ = ucpS[10];  // U
    *ucp2++ = ucpS[13];  // V
#else
    // turn the 10 bit values into 8 bits - optimized
    ucp0[0] = ucpS[1];              // Y
    ucp1[0] = ucp1[1] = ucpS[0];    // U
    ucp2[0] = ucp2[1] = ucpS[2];    // V
    ucp0[1] = ucpS[4];              // Y

    ucp0[2] = ucpS[6];              // Y
    ucp1[2] = ucp1[3] = ucpS[5];    // U
    ucp2[2] = ucp2[3] = ucpS[8];    // V
    ucp0[3] = ucpS[9];              // Y

    ucp0[4] = ucpS[12];             // Y
    ucp1[4] = ucp1[5] = ucpS[10];   // U
    ucp2[4] = ucp2[5] = ucpS[13];   // V
    ucp0[5] = ucpS[14];             // Y

    ucp0 += 6;
    ucp1 += 6;
    ucp2 += 6;
#endif

    ucpS += 16;
   }

#else  // OPTIMIZED IN ASSEMBLER
  _asm {
  mov    esi,ucpS
  mov    ebx,ucp0
  mov    ecx,ucp1
  mov    edx,ucp2

$L0:

  mov    eax,iCol
  cmp    eax,iColStop
  jge    $L1

  mov    eax,[esi]
  mov    [ecx],al       // U
  mov    [ecx+1],al

  shr    eax,8
  mov    [ebx],al       // Y

  shr    eax,8
  mov    [edx],al       // V
  mov    [edx+1],al

  mov    eax,[esi+4]
  mov    [ebx+1],al     // Y

  shr    eax,8
  mov    [ecx+2],al     // U
  mov    [ecx+3],al

  shr    eax,8
  mov    [ebx+2],al     // Y

  mov    eax,[esi+8]
  mov    [edx+2],al     // V
  mov    [edx+3],al

  shr    eax,8
  mov    [ebx+3],al     // Y

  shr    eax,8
  mov    [ecx+4],al     // U
  mov    [ecx+5],al

  mov    eax,[esi+12]
  mov    [ebx+4],al     // Y

  shr    eax,8
  mov    [edx+4],al     // V
  mov    [edx+5],al

  shr    eax,8
  mov    [ebx+5],al     // Y

  mov    eax,6
  add    ebx,eax
  add    ecx,eax
  add    edx,eax

  add    esi,16

  add    iCol,eax       // end FOR loop
  jmp    $L0

$L1:
  }
#endif

  ucpS -= 16;
  for (; iCol < iUnpackedPixelStop; iCol++)
   {
    // turn the 10 bit values into 8 bits
    *ucp0++ = ucpS[14];    // Y
    *ucp1++ = ucpS[10];    // U
    *ucp2++ = ucpS[13];    // V
   }


}


int CAspectRatio::RenderV_8_YUV_2_TO_8_YUV_2 (MTI_UINT8 *ucpSrc[2],
	MTI_UINT8 *ucpDst[2], int iCol0, int iCol1, int iJob)
{
  // this code does a VERTICAL render Y 8-bit YUV data with two fields

  int iRow, iField, iR, iCol, iNeighbor, iStep;
  MTI_UINT8 *ucpS[2], *ucpD[2];

  // this code is written for VERTICAL_COMPONENTS //12 components (especially UnpackV_8_YUV_2
  // and RenderV_YUV)
  if (iUnpackedComponent != VERTICAL_COMPONENTS) //12)
   {
    return ERR_ASPECT_RATIO_UNPACKED_V;
   }

  iStep = iUnpackedComponent / 2;
#ifdef DO_100_TIMES
  int iTTTTmp;


iTTTUnpack=0; iTTTRender=0; iTTTTotal=0;
  hrt.Start();


  iTTTTotal = hrt.Read();

  for (int i = 0; i < 100; ++i)
#endif
  for (iCol = iCol0; iCol < iCol1; iCol+=iStep)
   {
    ucpD[0] = ucpDst[0] + (iCol * 2);
    ucpD[1] = ucpDst[1] + (iCol * 2);
    ucpS[0] = ucpSrc[0] + (iCol * 2);
    ucpS[1] = ucpSrc[1] + (iCol * 2);

#ifdef DO_100_TIMES
    iTTTTmp = hrt.Read();
#endif
    UnpackV_8_YUV_2 (ucpS, iJob);
#ifdef DO_100_TIMES
    iTTTUnpack += (hrt.Read() - iTTTTmp);

    iTTTTmp = hrt.Read();
#endif

    RenderV_YUV (ucpD, iJob);
#ifdef DO_100_TIMES
    iTTTRender += (hrt.Read() - iTTTTmp);
#endif
   }
#ifdef DO_100_TIMES
  iTTTTotal = hrt.Read() - iTTTTotal;

TRACE_0 (errout << "TTT VERTICAL Total: " << iTTTTotal/100 << "." << ((iTTTTotal/10)%10)
                          << ", unpack: " << iTTTUnpack/100 << "." << ((iTTTUnpack/10)%10)
                          << ", render: " << iTTTRender/100 << "." << ((iTTTRender/10)%10));
#endif

  return 0;
}

int CAspectRatio::RenderV_8_YUV_1_TO_8_YUV_1 (MTI_UINT8 *ucpSrc[2],
	MTI_UINT8 *ucpDst[2], int iCol0, int iCol1, int iJob)
{
  // this code does a VERTICAL render Y 8-bit YUV data with one field

  int iRow, iField, iR, iCol, iNeighbor, iStep;
  MTI_UINT8 *ucpS[2], *ucpD[2];

  // this code is written for VERTICAL_COMPONENTS //12 components (especially UnpackV_8_YUV_1
  // and RenderV_YUV)
  if (iUnpackedComponent != VERTICAL_COMPONENTS) //12)
   {
    return ERR_ASPECT_RATIO_UNPACKED_V;
   }

  iStep = iUnpackedComponent / 2;


#ifdef DO_100_TIMES
  int iTTTTmp;


iTTTUnpack=0; iTTTRender=0; iTTTTotal=0;
  hrt.Start();


  iTTTTotal = hrt.Read();

  for (int i = 0; i < 100; ++i)
#endif
  for (iCol = iCol0; iCol < iCol1; iCol+=iStep)
   {
    ucpD[0] = ucpDst[0] + (iCol * 2);
    ucpD[1] = ucpDst[1] + (iCol * 2);
    ucpS[0] = ucpSrc[0] + (iCol * 2);
    ucpS[1] = ucpSrc[1] + (iCol * 2);

#ifdef DO_100_TIMES
    iTTTTmp = hrt.Read();
#endif
    UnpackV_8_YUV_1 (ucpS, iJob);
#ifdef DO_100_TIMES
    iTTTUnpack += (hrt.Read() - iTTTTmp);

    iTTTTmp = hrt.Read();
#endif
    RenderV_YUV (ucpD, iJob);
#ifdef DO_100_TIMES
    iTTTRender += (hrt.Read() - iTTTTmp);
#endif
   }
#ifdef DO_100_TIMES
  iTTTTotal = hrt.Read() - iTTTTotal;

TRACE_0 (errout << "TTT VERTICAL Total: " << iTTTTotal/100 << "." << ((iTTTTotal/10)%10)
                          << ", unpack: " << iTTTUnpack/100 << "." << ((iTTTUnpack/10)%10)
                          << ", render: " << iTTTRender/100 << "." << ((iTTTRender/10)%10));
#endif

  return 0;
}


int CAspectRatio::RenderV_10_YUV_2_TO_8_YUV_2 (MTI_UINT8 *ucpSrc[2],
	MTI_UINT8 *ucpDst[2], int iCol0, int iCol1, int iJob)
{
  // this code does a VERTICAL render Y 10-bit YUV data with two fields

  int iRow, iField, iR, iCol, iNeighbor, iStep;
  MTI_UINT8 *ucpS[2], *ucpD[2];

  // this code is written for VERTICAL_COMPONENTS //12 components (especially UnpackV_10_YUV_2
  // and RenderV_YUV)
  if (iUnpackedComponent != VERTICAL_COMPONENTS) //12)
   {
    return ERR_ASPECT_RATIO_UNPACKED_V;
   }

  iStep = iUnpackedComponent / 2;
  for (iCol = iCol0; iCol < iCol1; iCol += iStep)
   {
    ucpD[0] = ucpDst[0] + (iCol * 8/3);
    ucpD[1] = ucpDst[1] + (iCol * 8/3);
    ucpS[0] = ucpSrc[0] + (iCol * 8/3);
    ucpS[1] = ucpSrc[1] + (iCol * 8/3);

    UnpackV_10_YUV_2 (ucpS, iJob);

    RenderV_YUV (ucpD, iJob);
   }

  return 0;
}

int CAspectRatio::RenderV_10_YUV_1_TO_8_YUV_1 (MTI_UINT8 *ucpSrc[2],
	MTI_UINT8 *ucpDst[2], int iCol0, int iCol1, int iJob)
{
  // this code does a VERTICAL render Y 10-bit YUV data with one fields

  int iRow, iField, iR, iCol, iNeighbor, iStep;
  MTI_UINT8 *ucpS[2], *ucpD[2];

  // this code is written for VERTICAL_COMPONENTS //12 components (especially UnpackV_10_YUV_1
  // and RenderV_YUV)
  if (iUnpackedComponent != VERTICAL_COMPONENTS) //12)
   {
    return ERR_ASPECT_RATIO_UNPACKED_V;
   }

  iStep = iUnpackedComponent / 2;
  for (iCol = iCol0; iCol < iCol1; iCol += iStep)
   {
    ucpD[0] = ucpDst[0] + (iCol * 8/3);
    ucpD[1] = ucpDst[1] + (iCol * 8/3);
    ucpS[0] = ucpSrc[0] + (iCol * 8/3);
    ucpS[1] = ucpSrc[1] + (iCol * 8/3);

    UnpackV_10_YUV_1 (ucpS, iJob);
    
    RenderV_YUV (ucpD, iJob);
   }

  return 0;
}

int CAspectRatio::RenderV_10a_YUV_2_TO_8_YUV_2 (MTI_UINT8 *ucpSrc[2],
	MTI_UINT8 *ucpDst[2], int iCol0, int iCol1, int iJob)
{
  // this code does a VERTICAL render Y 10-bit YUV data with two fields

  int iRow, iField, iR, iCol, iNeighbor, iStep;
  MTI_UINT8 *ucpS[2], *ucpD[2];

  // this code is written for VERTICAL_COMPONENTS (12 components)
  // (especially UnpackV_10_YUV_2 and RenderV_YUV)
  if (iUnpackedComponent != VERTICAL_COMPONENTS)
   {
    return ERR_ASPECT_RATIO_UNPACKED_V;
   }

  iStep = iUnpackedComponent / 2;
  for (iCol = iCol0; iCol < iCol1; iCol += iStep)
   {
    ucpD[0] = ucpDst[0] + (iCol * 8/3);
    ucpD[1] = ucpDst[1] + (iCol * 8/3);
    ucpS[0] = ucpSrc[0] + (iCol * 8/3);
    ucpS[1] = ucpSrc[1] + (iCol * 8/3);

    UnpackV_10a_YUV_2 (ucpS, iJob);
    
    RenderV_YUV (ucpD, iJob);
   }

  return 0;
}

int CAspectRatio::RenderV_10a_YUV_1_TO_8_YUV_1 (MTI_UINT8 *ucpSrc[2],
	MTI_UINT8 *ucpDst[2], int iCol0, int iCol1, int iJob)
{
  // this code does a VERTICAL render Y 10-bit DVSa YUV data with one fields

  int iRow, iField, iR, iCol, iNeighbor, iStep;
  MTI_UINT8 *ucpS[2], *ucpD[2];
  
  // this code is written for VERTICAL_COMPONENTS (12 components)
  // (especially UnpackV_10_YUV_1 and RenderV_YUV)
  if (iUnpackedComponent != VERTICAL_COMPONENTS)
   {
    return ERR_ASPECT_RATIO_UNPACKED_V;
   }

  iStep = iUnpackedComponent / 2;
  for (iCol = iCol0; iCol < iCol1; iCol += iStep)
   {
    ucpD[0] = ucpDst[0] + (iCol * 8/3);
    ucpD[1] = ucpDst[1] + (iCol * 8/3);
    ucpS[0] = ucpSrc[0] + (iCol * 8/3);
    ucpS[1] = ucpSrc[1] + (iCol * 8/3);

    UnpackV_10a_YUV_1 (ucpS, iJob);
    
    RenderV_YUV (ucpD, iJob);
   }

  return 0;
}


void CAspectRatio::RenderV_YUV (MTI_UINT8 *ucpDst[2], int iJob)
{
  MTI_UINT8 *ucpD;
  int iRow, iField, iR, iSubPixel, iPixel, iNei;
  int iSum, *ipWei;
  MTI_UINT8 *ucpUnpack;
  int iComponent;

  for (iRow = 0; iRow < iStartDst; iRow++)
   {
    iField = iRow % iNFieldDst;
    iR = iRow / iNFieldDst;

    ucpD = ucpDst[iField] + (iR * iPitchDst);

    // unroll the loop
    for (iComponent = 0; iComponent < iUnpackedComponent-9; iComponent+=10)
     {
      ucpD[0] = 128;	// U or V
      ucpD[1] = 16;	// Y

      ucpD[2] = 128;	// U or V
      ucpD[3] = 16;	// Y

      ucpD[4] = 128;	// U or V
      ucpD[5] = 16;	// Y

      ucpD[6] = 128;	// U or V
      ucpD[7] = 16;	// Y

      ucpD[8] = 128;	// U or V
      ucpD[9] = 16;	// Y

      // advance pointer
      ucpD += 10;
     }

    for ( ; iComponent < iUnpackedComponent; iComponent+=2)
     {
      ucpD[0] = 128;	// U or V
      ucpD[1] = 16;	// Y

      // advance pointer
      ucpD += 2;
     }
   }

  // make sure iRow is even
  if (iRow & 0x1)
   {
    iField = iRow % iNFieldDst;
    iR = iRow / iNFieldDst;

    ucpD = ucpDst[iField] + (iR * iPitchDst);

    iSubPixel = ipLUTSubPixel[iRow];
    iPixel = ipLUTPixel[iRow];

    for (iComponent = 0; iComponent < iUnpackedComponent; iComponent++)
     {
      ucpUnpack = ucpUnpacked[iJob][iComponent];

      ucpUnpack += iPixel;

      iSum = 0;
      for (iNei = 0; iNei < iTotalNeighbor; iNei++)
       {
        ipWei = ipLUTWei[iSubPixel][iNei];

        iSum += ipWei[*ucpUnpack++];
       }

      // clamp the result
      if (iSum > iClampMax)
        iSum = iClampMax;
      else if (iSum < iClampMin)
        iSum = iClampMin;

      // round the result
      iSum = ((iSum + iHalfPrecision) >> PRECISION);

      *ucpD++ = iSum;
     }

    iRow++;
   }

  int iStopDstLocal = iStopDst;
  if (iStopDstLocal & 0x1)
   {
    iStopDstLocal--;
   }


  if (iTotalNeighbor == 1)
   {
    RenderV_YUV_1 (ucpDst, iRow, iStopDstLocal, iJob);
   }
  else if (iTotalNeighbor == 3)
   {
    RenderV_YUV_3 (ucpDst, iRow, iStopDstLocal, iJob);
   }
  else if (iTotalNeighbor == 5)
   {
    RenderV_YUV_5 (ucpDst, iRow, iStopDstLocal, iJob);
   }
  else if (iTotalNeighbor == 7)
   {
    RenderV_YUV_7 (ucpDst, iRow, iStopDstLocal, iJob);
   }
  else if (iTotalNeighbor == 9)
   {
    RenderV_YUV_9 (ucpDst, iRow, iStopDstLocal, iJob);
   }
  else if (iTotalNeighbor == 11)
   {
    RenderV_YUV_11 (ucpDst, iRow, iStopDstLocal, iJob);
   }
  else if (iTotalNeighbor == 13)
   {
    RenderV_YUV_13 (ucpDst, iRow, iStopDstLocal, iJob);
   }
  else if (iTotalNeighbor == 15)
   {
    RenderV_YUV_15 (ucpDst, iRow, iStopDstLocal, iJob);
   }
  else
   {
    RenderV_YUV (ucpDst, iRow, iStopDstLocal, iJob);
   }

  if (iRow < iStopDst)
   {
    iField = iRow % iNFieldDst;
    iR = iRow / iNFieldDst;

    ucpD = ucpDst[iField] + (iR * iPitchDst);

    iSubPixel = ipLUTSubPixel[iRow];
    iPixel = ipLUTPixel[iRow];

    for (iComponent = 0; iComponent < iUnpackedComponent; iComponent++)
     {
      ucpUnpack = ucpUnpacked[iJob][iComponent];

      ucpUnpack += iPixel;

      iSum = 0;
      for (iNei = 0; iNei < iTotalNeighbor; iNei++)
       {
        ipWei = ipLUTWei[iSubPixel][iNei];

        iSum += ipWei[*ucpUnpack++];
       }

      // clamp the result
      if (iSum > iClampMax)
        iSum = iClampMax;
      else if (iSum < iClampMin)
        iSum = iClampMin;

      // round the result
      iSum = ((iSum + iHalfPrecision) >> PRECISION);

      *ucpD++ = iSum;
     }

    iRow++;
   }

  for (; iRow < iNRowDst; iRow++)
   {
    iField = iRow % iNFieldDst;
    iR = iRow / iNFieldDst;

    ucpD = ucpDst[iField] + (iR * iPitchDst);

    // unroll the loop
    for (iComponent = 0; iComponent < iUnpackedComponent-9; iComponent+=10)
     {
      ucpD[0] = 128;	// U or V
      ucpD[1] = 16;	// Y

      ucpD[2] = 128;	// U or V
      ucpD[3] = 16;	// Y

      ucpD[4] = 128;	// U or V
      ucpD[5] = 16;	// Y

      ucpD[6] = 128;	// U or V
      ucpD[7] = 16;	// Y

      ucpD[8] = 128;	// U or V
      ucpD[9] = 16;	// Y

      // advance pointer
      ucpD += 10;
     }

    for ( ; iComponent < iUnpackedComponent; iComponent+=2)
     {
      ucpD[0] = 128;	// U or V
      ucpD[1] = 16;	// Y

      // advance pointer
      ucpD += 2;
     }
   }

}

void CAspectRatio::UnpackV_8_YUV_2 (MTI_UINT8 *ucpSrc[2], int iJob)
{
  int iField, iR, iRow;
  MTI_UINT8 *ucpS[2];
  int iRowStop;
  MTI_UINT8 *ucpa[VERTICAL_COMPONENTS];
  MTI_UINT8 ucpTmp[VERTICAL_COMPONENTS][8];

  ucpa[0] = ucpUnpacked[iJob][0] + iUnpackedPixelStart;
  ucpa[1] = ucpUnpacked[iJob][1] + iUnpackedPixelStart;
  ucpa[2] = ucpUnpacked[iJob][2] + iUnpackedPixelStart;
  ucpa[3] = ucpUnpacked[iJob][3] + iUnpackedPixelStart;
  ucpa[4] = ucpUnpacked[iJob][4] + iUnpackedPixelStart;
  ucpa[5] = ucpUnpacked[iJob][5] + iUnpackedPixelStart;
  ucpa[6] = ucpUnpacked[iJob][6] + iUnpackedPixelStart;
  ucpa[7] = ucpUnpacked[iJob][7] + iUnpackedPixelStart;
  ucpa[8] = ucpUnpacked[iJob][8] + iUnpackedPixelStart;
  ucpa[9] = ucpUnpacked[iJob][9] + iUnpackedPixelStart;
  ucpa[10] = ucpUnpacked[iJob][10] + iUnpackedPixelStart;
  ucpa[11] = ucpUnpacked[iJob][11] + iUnpackedPixelStart;
  ucpa[12] = ucpUnpacked[iJob][12] + iUnpackedPixelStart;
  ucpa[13] = ucpUnpacked[iJob][13] + iUnpackedPixelStart;
  ucpa[14] = ucpUnpacked[iJob][14] + iUnpackedPixelStart;
  ucpa[15] = ucpUnpacked[iJob][15] + iUnpackedPixelStart;
  ucpa[16] = ucpUnpacked[iJob][16] + iUnpackedPixelStart;
  ucpa[17] = ucpUnpacked[iJob][17] + iUnpackedPixelStart;
  ucpa[18] = ucpUnpacked[iJob][18] + iUnpackedPixelStart;
  ucpa[19] = ucpUnpacked[iJob][19] + iUnpackedPixelStart;
  ucpa[20] = ucpUnpacked[iJob][20] + iUnpackedPixelStart;
  ucpa[21] = ucpUnpacked[iJob][21] + iUnpackedPixelStart;
  ucpa[22] = ucpUnpacked[iJob][22] + iUnpackedPixelStart;
  ucpa[23] = ucpUnpacked[iJob][23] + iUnpackedPixelStart;

  iField = 0;
  iR = 0;
  ucpS[0] = ucpSrc[iField] + (iR * iPitchSrc);
  for (iRow = iUnpackedPixelStart; iRow < 0; iRow++)
   {
    *ucpa[0]++ = ucpS[0][0];	// U
    *ucpa[1]++ = ucpS[0][1];	// Y
    *ucpa[2]++ = ucpS[0][2];	// V
    *ucpa[3]++ = ucpS[0][3];	// Y

    *ucpa[4]++ = ucpS[0][4];	// U
    *ucpa[5]++ = ucpS[0][5];	// Y
    *ucpa[6]++ = ucpS[0][6];	// V
    *ucpa[7]++ = ucpS[0][7];	// Y

    *ucpa[8]++ = ucpS[0][8];	// U
    *ucpa[9]++ = ucpS[0][9];	// Y
    *ucpa[10]++ = ucpS[0][10]; // V
    *ucpa[11]++ = ucpS[0][11]; // Y

    *ucpa[12]++ = ucpS[0][12];	// V
    *ucpa[13]++ = ucpS[0][13];	// Y
    *ucpa[14]++ = ucpS[0][14];	// U
    *ucpa[15]++ = ucpS[0][15];	// Y

    *ucpa[16]++ = ucpS[0][16];	// V
    *ucpa[17]++ = ucpS[0][17];	// Y
    *ucpa[18]++ = ucpS[0][18];	// U
    *ucpa[19]++ = ucpS[0][19];	// Y

    *ucpa[20]++ = ucpS[0][20];	// U
    *ucpa[21]++ = ucpS[0][21];	// Y
    *ucpa[22]++ = ucpS[0][22]; // V
    *ucpa[23]++ = ucpS[0][23]; // Y
   }

  iRowStop = iUnpackedPixelStop;
  if (iRowStop > iNRowSrc)
    iRowStop = iNRowSrc;

    iField = iRow % 2;
    iR = iRow / 2;
    ucpS[0] = ucpSrc[iField] + (iR * iPitchSrc);

    iField = (iRow + 1) % 2;
    iR = (iRow + 1) / 2;
    ucpS[1] = ucpSrc[iField] + (iR * iPitchSrc);

  for (; iRow < (iRowStop - 7); iRow += 8)
   {
    ucpTmp[0][0] = ucpS[0][0];	// U
    ucpTmp[1][0] = ucpS[0][1];	// Y
    ucpTmp[2][0] = ucpS[0][2];	// V
    ucpTmp[3][0] = ucpS[0][3];	// Y
    ucpTmp[4][0] = ucpS[0][4];	// U
    ucpTmp[5][0] = ucpS[0][5];	// Y
    ucpTmp[6][0] = ucpS[0][6];	// V
    ucpTmp[7][0] = ucpS[0][7];	// Y
    ucpTmp[8][0] = ucpS[0][8];	// U
    ucpTmp[9][0] = ucpS[0][9];	// Y
    ucpTmp[10][0] = ucpS[0][10];	// V
    ucpTmp[11][0] = ucpS[0][11];	// Y
    ucpTmp[12][0] = ucpS[0][12];	// U
    ucpTmp[13][0] = ucpS[0][13];	// Y
    ucpTmp[14][0] = ucpS[0][14];	// V
    ucpTmp[15][0] = ucpS[0][15];	// Y
    ucpTmp[16][0] = ucpS[0][16];	// U
    ucpTmp[17][0] = ucpS[0][17];	// Y
    ucpTmp[18][0] = ucpS[0][18];	// V
    ucpTmp[19][0] = ucpS[0][19];	// Y
    ucpTmp[20][0] = ucpS[0][20];	// U
    ucpTmp[21][0] = ucpS[0][21];	// Y
    ucpTmp[22][0] = ucpS[0][22];	// V
    ucpTmp[23][0] = ucpS[0][23];	// Y

    ucpS[0] += iPitchSrc;

    ucpTmp[0][1] = ucpS[1][0];	// U
    ucpTmp[1][1] = ucpS[1][1];	// Y
    ucpTmp[2][1] = ucpS[1][2];	// V
    ucpTmp[3][1] = ucpS[1][3];	// Y
    ucpTmp[4][1] = ucpS[1][4];	// U
    ucpTmp[5][1] = ucpS[1][5];	// Y
    ucpTmp[6][1] = ucpS[1][6];	// V
    ucpTmp[7][1] = ucpS[1][7];	// Y
    ucpTmp[8][1] = ucpS[1][8];	// U
    ucpTmp[9][1] = ucpS[1][9];	// Y
    ucpTmp[10][1] = ucpS[1][10];	// V
    ucpTmp[11][1] = ucpS[1][11];	// Y
    ucpTmp[12][1] = ucpS[1][12];	// U
    ucpTmp[13][1] = ucpS[1][13];	// Y
    ucpTmp[14][1] = ucpS[1][14];	// V
    ucpTmp[15][1] = ucpS[1][15];	// Y
    ucpTmp[16][1] = ucpS[1][16];	// U
    ucpTmp[17][1] = ucpS[1][17];	// Y
    ucpTmp[18][1] = ucpS[1][18];	// V
    ucpTmp[19][1] = ucpS[1][19];	// Y
    ucpTmp[20][1] = ucpS[1][20];	// U
    ucpTmp[21][1] = ucpS[1][21];	// Y
    ucpTmp[22][1] = ucpS[1][22];	// V
    ucpTmp[23][1] = ucpS[1][23];	// Y

    ucpS[1] += iPitchSrc;

    ucpTmp[0][2] = ucpS[0][0];	// U
    ucpTmp[1][2] = ucpS[0][1];	// Y
    ucpTmp[2][2] = ucpS[0][2];	// V
    ucpTmp[3][2] = ucpS[0][3];	// Y
    ucpTmp[4][2] = ucpS[0][4];	// U
    ucpTmp[5][2] = ucpS[0][5];	// Y
    ucpTmp[6][2] = ucpS[0][6];	// V
    ucpTmp[7][2] = ucpS[0][7];	// Y
    ucpTmp[8][2] = ucpS[0][8];	// U
    ucpTmp[9][2] = ucpS[0][9];	// Y
    ucpTmp[10][2] = ucpS[0][10];	// V
    ucpTmp[11][2] = ucpS[0][11];	// Y
    ucpTmp[12][2] = ucpS[0][12];	// U
    ucpTmp[13][2] = ucpS[0][13];	// Y
    ucpTmp[14][2] = ucpS[0][14];	// V
    ucpTmp[15][2] = ucpS[0][15];	// Y
    ucpTmp[16][2] = ucpS[0][16];	// U
    ucpTmp[17][2] = ucpS[0][17];	// Y
    ucpTmp[18][2] = ucpS[0][18];	// V
    ucpTmp[19][2] = ucpS[0][19];	// Y
    ucpTmp[20][2] = ucpS[0][20];	// U
    ucpTmp[21][2] = ucpS[0][21];	// Y
    ucpTmp[22][2] = ucpS[0][22];	// V
    ucpTmp[23][2] = ucpS[0][23];	// Y

    ucpS[0] += iPitchSrc;

    ucpTmp[0][3] = ucpS[1][0];	// U
    ucpTmp[1][3] = ucpS[1][1];	// Y
    ucpTmp[2][3] = ucpS[1][2];	// V
    ucpTmp[3][3] = ucpS[1][3];	// Y
    ucpTmp[4][3] = ucpS[1][4];	// U
    ucpTmp[5][3] = ucpS[1][5];	// Y
    ucpTmp[6][3] = ucpS[1][6];	// V
    ucpTmp[7][3] = ucpS[1][7];	// Y
    ucpTmp[8][3] = ucpS[1][8];	// U
    ucpTmp[9][3] = ucpS[1][9];	// Y
    ucpTmp[10][3] = ucpS[1][10];	// V
    ucpTmp[11][3] = ucpS[1][11];	// Y
    ucpTmp[12][3] = ucpS[1][12];	// U
    ucpTmp[13][3] = ucpS[1][13];	// Y
    ucpTmp[14][3] = ucpS[1][14];	// V
    ucpTmp[15][3] = ucpS[1][15];	// Y
    ucpTmp[16][3] = ucpS[1][16];	// U
    ucpTmp[17][3] = ucpS[1][17];	// Y
    ucpTmp[18][3] = ucpS[1][18];	// V
    ucpTmp[19][3] = ucpS[1][19];	// Y
    ucpTmp[20][3] = ucpS[1][20];	// U
    ucpTmp[21][3] = ucpS[1][21];	// Y
    ucpTmp[22][3] = ucpS[1][22];	// V
    ucpTmp[23][3] = ucpS[1][23];	// Y

    ucpS[1] += iPitchSrc;

    ucpTmp[0][4] = ucpS[0][0];	// U
    ucpTmp[1][4] = ucpS[0][1];	// Y
    ucpTmp[2][4] = ucpS[0][2];	// V
    ucpTmp[3][4] = ucpS[0][3];	// Y
    ucpTmp[4][4] = ucpS[0][4];	// U
    ucpTmp[5][4] = ucpS[0][5];	// Y
    ucpTmp[6][4] = ucpS[0][6];	// V
    ucpTmp[7][4] = ucpS[0][7];	// Y
    ucpTmp[8][4] = ucpS[0][8];	// U
    ucpTmp[9][4] = ucpS[0][9];	// Y
    ucpTmp[10][4] = ucpS[0][10];	// V
    ucpTmp[11][4] = ucpS[0][11];	// Y
    ucpTmp[12][4] = ucpS[0][12];	// U
    ucpTmp[13][4] = ucpS[0][13];	// Y
    ucpTmp[14][4] = ucpS[0][14];	// V
    ucpTmp[15][4] = ucpS[0][15];	// Y
    ucpTmp[16][4] = ucpS[0][16];	// U
    ucpTmp[17][4] = ucpS[0][17];	// Y
    ucpTmp[18][4] = ucpS[0][18];	// V
    ucpTmp[19][4] = ucpS[0][19];	// Y
    ucpTmp[20][4] = ucpS[0][20];	// U
    ucpTmp[21][4] = ucpS[0][21];	// Y
    ucpTmp[22][4] = ucpS[0][22];	// V
    ucpTmp[23][4] = ucpS[0][23];	// Y

    ucpS[0] += iPitchSrc;

    ucpTmp[0][5] = ucpS[1][0];	// U
    ucpTmp[1][5] = ucpS[1][1];	// Y
    ucpTmp[2][5] = ucpS[1][2];	// V
    ucpTmp[3][5] = ucpS[1][3];	// Y
    ucpTmp[4][5] = ucpS[1][4];	// U
    ucpTmp[5][5] = ucpS[1][5];	// Y
    ucpTmp[6][5] = ucpS[1][6];	// V
    ucpTmp[7][5] = ucpS[1][7];	// Y
    ucpTmp[8][5] = ucpS[1][8];	// U
    ucpTmp[9][5] = ucpS[1][9];	// Y
    ucpTmp[10][5] = ucpS[1][10];	// V
    ucpTmp[11][5] = ucpS[1][11];	// Y
    ucpTmp[12][5] = ucpS[1][12];	// U
    ucpTmp[13][5] = ucpS[1][13];	// Y
    ucpTmp[14][5] = ucpS[1][14];	// V
    ucpTmp[15][5] = ucpS[1][15];	// Y
    ucpTmp[16][5] = ucpS[1][16];	// U
    ucpTmp[17][5] = ucpS[1][17];	// Y
    ucpTmp[18][5] = ucpS[1][18];	// V
    ucpTmp[19][5] = ucpS[1][19];	// Y
    ucpTmp[20][5] = ucpS[1][20];	// U
    ucpTmp[21][5] = ucpS[1][21];	// Y
    ucpTmp[22][5] = ucpS[1][22];	// V
    ucpTmp[23][5] = ucpS[1][23];	// Y

    ucpS[1] += iPitchSrc;

    ucpTmp[0][6] = ucpS[0][0];	// U
    ucpTmp[1][6] = ucpS[0][1];	// Y
    ucpTmp[2][6] = ucpS[0][2];	// V
    ucpTmp[3][6] = ucpS[0][3];	// Y
    ucpTmp[4][6] = ucpS[0][4];	// U
    ucpTmp[5][6] = ucpS[0][5];	// Y
    ucpTmp[6][6] = ucpS[0][6];	// V
    ucpTmp[7][6] = ucpS[0][7];	// Y
    ucpTmp[8][6] = ucpS[0][8];	// U
    ucpTmp[9][6] = ucpS[0][9];	// Y
    ucpTmp[10][6] = ucpS[0][10];	// V
    ucpTmp[11][6] = ucpS[0][11];	// Y
    ucpTmp[12][6] = ucpS[0][12];	// U
    ucpTmp[13][6] = ucpS[0][13];	// Y
    ucpTmp[14][6] = ucpS[0][14];	// V
    ucpTmp[15][6] = ucpS[0][15];	// Y
    ucpTmp[16][6] = ucpS[0][16];	// U
    ucpTmp[17][6] = ucpS[0][17];	// Y
    ucpTmp[18][6] = ucpS[0][18];	// V
    ucpTmp[19][6] = ucpS[0][19];	// Y
    ucpTmp[20][6] = ucpS[0][20];	// U
    ucpTmp[21][6] = ucpS[0][21];	// Y
    ucpTmp[22][6] = ucpS[0][22];	// V
    ucpTmp[23][6] = ucpS[0][23];	// Y

    ucpS[0] += iPitchSrc;

    ucpTmp[0][7] = ucpS[1][0];	// U
    ucpTmp[1][7] = ucpS[1][1];	// Y
    ucpTmp[2][7] = ucpS[1][2];	// V
    ucpTmp[3][7] = ucpS[1][3];	// Y
    ucpTmp[4][7] = ucpS[1][4];	// U
    ucpTmp[5][7] = ucpS[1][5];	// Y
    ucpTmp[6][7] = ucpS[1][6];	// V
    ucpTmp[7][7] = ucpS[1][7];	// Y
    ucpTmp[8][7] = ucpS[1][8];	// U
    ucpTmp[9][7] = ucpS[1][9];	// Y
    ucpTmp[10][7] = ucpS[1][10];	// V
    ucpTmp[11][7] = ucpS[1][11];	// Y
    ucpTmp[12][7] = ucpS[1][12];	// U
    ucpTmp[13][7] = ucpS[1][13];	// Y
    ucpTmp[14][7] = ucpS[1][14];	// V
    ucpTmp[15][7] = ucpS[1][15];	// Y
    ucpTmp[16][7] = ucpS[1][16];	// U
    ucpTmp[17][7] = ucpS[1][17];	// Y
    ucpTmp[18][7] = ucpS[1][18];	// V
    ucpTmp[19][7] = ucpS[1][19];	// Y
    ucpTmp[20][7] = ucpS[1][20];	// U
    ucpTmp[21][7] = ucpS[1][21];	// Y
    ucpTmp[22][7] = ucpS[1][22];	// V
    ucpTmp[23][7] = ucpS[1][23];	// Y

    ucpS[1] += iPitchSrc;

    *(long *) &ucpa[0][0]  = *(long *) &ucpTmp[0][0];
    *(long *) &ucpa[0][4]  = *(long *) &ucpTmp[0][4];
    *(long *) &ucpa[1][0]  = *(long *) &ucpTmp[1][0];
    *(long *) &ucpa[1][4]  = *(long *) &ucpTmp[1][4];
    *(long *) &ucpa[2][0]  = *(long *) &ucpTmp[2][0];
    *(long *) &ucpa[2][4]  = *(long *) &ucpTmp[2][4];
    *(long *) &ucpa[3][0]  = *(long *) &ucpTmp[3][0];
    *(long *) &ucpa[3][4]  = *(long *) &ucpTmp[3][4];

    *(long *) &ucpa[4][0]  = *(long *) &ucpTmp[4][0];
    *(long *) &ucpa[4][4]  = *(long *) &ucpTmp[4][4];
    *(long *) &ucpa[5][0]  = *(long *) &ucpTmp[5][0];
    *(long *) &ucpa[5][4]  = *(long *) &ucpTmp[5][4];
    *(long *) &ucpa[6][0]  = *(long *) &ucpTmp[6][0];
    *(long *) &ucpa[6][4]  = *(long *) &ucpTmp[6][4];
    *(long *) &ucpa[7][0]  = *(long *) &ucpTmp[7][0];
    *(long *) &ucpa[7][4]  = *(long *) &ucpTmp[7][4];

    *(long *) &ucpa[8][0]  = *(long *) &ucpTmp[8][0];
    *(long *) &ucpa[8][4]  = *(long *) &ucpTmp[8][4];
    *(long *) &ucpa[9][0]  = *(long *) &ucpTmp[9][0];
    *(long *) &ucpa[9][4]  = *(long *) &ucpTmp[9][4];
    *(long *) &ucpa[10][0] = *(long *) &ucpTmp[10][0];
    *(long *) &ucpa[10][4] = *(long *) &ucpTmp[10][4];
    *(long *) &ucpa[11][0] = *(long *) &ucpTmp[11][0];
    *(long *) &ucpa[11][4] = *(long *) &ucpTmp[11][4];

    *(long *) &ucpa[12][0] = *(long *) &ucpTmp[12][0];
    *(long *) &ucpa[12][4] = *(long *) &ucpTmp[12][4];
    *(long *) &ucpa[13][0] = *(long *) &ucpTmp[13][0];
    *(long *) &ucpa[13][4] = *(long *) &ucpTmp[13][4];
    *(long *) &ucpa[14][0] = *(long *) &ucpTmp[14][0];
    *(long *) &ucpa[14][4] = *(long *) &ucpTmp[14][4];
    *(long *) &ucpa[15][0] = *(long *) &ucpTmp[15][0];
    *(long *) &ucpa[15][4] = *(long *) &ucpTmp[15][4];

    *(long *) &ucpa[16][0] = *(long *) &ucpTmp[16][0];
    *(long *) &ucpa[16][4] = *(long *) &ucpTmp[16][4];
    *(long *) &ucpa[17][0] = *(long *) &ucpTmp[17][0];
    *(long *) &ucpa[17][4] = *(long *) &ucpTmp[17][4];
    *(long *) &ucpa[18][0] = *(long *) &ucpTmp[18][0];
    *(long *) &ucpa[18][4] = *(long *) &ucpTmp[18][4];
    *(long *) &ucpa[19][0] = *(long *) &ucpTmp[19][0];
    *(long *) &ucpa[19][4] = *(long *) &ucpTmp[19][4];

    *(long *) &ucpa[20][0]  = *(long *) &ucpTmp[20][0];
    *(long *) &ucpa[20][4]  = *(long *) &ucpTmp[20][4];
    *(long *) &ucpa[21][0]  = *(long *) &ucpTmp[21][0];
    *(long *) &ucpa[21][4]  = *(long *) &ucpTmp[21][4];
    *(long *) &ucpa[22][0] = *(long *) &ucpTmp[22][0];
    *(long *) &ucpa[22][4] = *(long *) &ucpTmp[22][4];
    *(long *) &ucpa[23][0] = *(long *) &ucpTmp[23][0];
    *(long *) &ucpa[23][4] = *(long *) &ucpTmp[23][4];


    ucpa[0] += 8;
    ucpa[1] += 8;
    ucpa[2] += 8;
    ucpa[3] += 8;
    ucpa[4] += 8;
    ucpa[5] += 8;
    ucpa[6] += 8;
    ucpa[7] += 8;
    ucpa[8] += 8;
    ucpa[9] += 8;
    ucpa[10] += 8;
    ucpa[11] += 8;
    ucpa[12] += 8;
    ucpa[13] += 8;
    ucpa[14] += 8;
    ucpa[15] += 8;
    ucpa[16] += 8;
    ucpa[17] += 8;
    ucpa[18] += 8;
    ucpa[19] += 8;
    ucpa[20] += 8;
    ucpa[21] += 8;
    ucpa[22] += 8;
    ucpa[23] += 8;
   }
  for (; iRow < iRowStop; iRow++)
   {
    iField = iRow % 2;
    iR = iRow / 2;

    ucpS[0] = ucpSrc[iField] + (iR * iPitchSrc);

    *ucpa[0]++ = ucpS[0][0];	// U
    *ucpa[1]++ = ucpS[0][1];	// Y
    *ucpa[2]++ = ucpS[0][2];	// V
    *ucpa[3]++ = ucpS[0][3];	// Y

    *ucpa[4]++ = ucpS[0][4];	// U
    *ucpa[5]++ = ucpS[0][5];	// Y
    *ucpa[6]++ = ucpS[0][6];	// V
    *ucpa[7]++ = ucpS[0][7];	// Y

    *ucpa[8]++ = ucpS[0][8];	// U
    *ucpa[9]++ = ucpS[0][9];	// Y
    *ucpa[10]++ = ucpS[0][10]; // V
    *ucpa[11]++ = ucpS[0][11]; // Y

    *ucpa[12]++ = ucpS[0][12];	// V
    *ucpa[13]++ = ucpS[0][13];	// Y
    *ucpa[14]++ = ucpS[0][14];	// U
    *ucpa[15]++ = ucpS[0][15];	// Y

    *ucpa[16]++ = ucpS[0][16];	// V
    *ucpa[17]++ = ucpS[0][17];	// Y
    *ucpa[18]++ = ucpS[0][18];	// U
    *ucpa[19]++ = ucpS[0][19];	// Y

    *ucpa[20]++ = ucpS[0][20];	// U
    *ucpa[21]++ = ucpS[0][21];	// Y
    *ucpa[22]++ = ucpS[0][22]; // V
    *ucpa[23]++ = ucpS[0][23]; // Y
  }

  iField = (iNRowSrc-1)%2;
  iR = (iNRowSrc-1)/2;
  ucpS[0] = ucpSrc[iField] + (iR * iPitchSrc);
  for (; iRow < iUnpackedPixelStop; iRow++)
   {
    *ucpa[0]++ = ucpS[0][0];	// U
    *ucpa[1]++ = ucpS[0][1];	// Y
    *ucpa[2]++ = ucpS[0][2];	// V
    *ucpa[3]++ = ucpS[0][3];	// Y

    *ucpa[4]++ = ucpS[0][4];	// U
    *ucpa[5]++ = ucpS[0][5];	// Y
    *ucpa[6]++ = ucpS[0][6];	// V
    *ucpa[7]++ = ucpS[0][7];	// Y

    *ucpa[8]++ = ucpS[0][8];	// U
    *ucpa[9]++ = ucpS[0][9];	// Y
    *ucpa[10]++ = ucpS[0][10]; // V
    *ucpa[11]++ = ucpS[0][11]; // Y

    *ucpa[12]++ = ucpS[0][12];	// V
    *ucpa[13]++ = ucpS[0][13];	// Y
    *ucpa[14]++ = ucpS[0][14];	// U
    *ucpa[15]++ = ucpS[0][15];	// Y

    *ucpa[16]++ = ucpS[0][16];	// V
    *ucpa[17]++ = ucpS[0][17];	// Y
    *ucpa[18]++ = ucpS[0][18];	// U
    *ucpa[19]++ = ucpS[0][19];	// Y

    *ucpa[20]++ = ucpS[0][20];	// U
    *ucpa[21]++ = ucpS[0][21];	// Y
    *ucpa[22]++ = ucpS[0][22]; // V
    *ucpa[23]++ = ucpS[0][23]; // Y
   }


}

void CAspectRatio::UnpackV_8_YUV_1 (MTI_UINT8 *ucpSrc[2], int iJob)
{
  int iRow, iR;
  int iRowStop;
  MTI_UINT8 *ucpS;
  //MTI_UINT8 *ucp0, *ucp1, *ucp2, *ucp3, *ucp4, *ucp5, *ucp6, *ucp7,
		//*ucp8, *ucp9, *ucp10, *ucp11,
    //*ucp12, *ucp13, *ucp14, *ucp15, *ucp16, *ucp17, *ucp18, *ucp19;
  MTI_UINT8 *ucpa[VERTICAL_COMPONENTS];
  MTI_UINT8 ucpTmp[VERTICAL_COMPONENTS][8];

  ucpa[0] = ucpUnpacked[iJob][0] + iUnpackedPixelStart;
  ucpa[1] = ucpUnpacked[iJob][1] + iUnpackedPixelStart;
  ucpa[2] = ucpUnpacked[iJob][2] + iUnpackedPixelStart;
  ucpa[3] = ucpUnpacked[iJob][3] + iUnpackedPixelStart;
  ucpa[4] = ucpUnpacked[iJob][4] + iUnpackedPixelStart;
  ucpa[5] = ucpUnpacked[iJob][5] + iUnpackedPixelStart;
  ucpa[6] = ucpUnpacked[iJob][6] + iUnpackedPixelStart;
  ucpa[7] = ucpUnpacked[iJob][7] + iUnpackedPixelStart;
  ucpa[8] = ucpUnpacked[iJob][8] + iUnpackedPixelStart;
  ucpa[9] = ucpUnpacked[iJob][9] + iUnpackedPixelStart;
  ucpa[10] = ucpUnpacked[iJob][10] + iUnpackedPixelStart;
  ucpa[11] = ucpUnpacked[iJob][11] + iUnpackedPixelStart;
  ucpa[12] = ucpUnpacked[iJob][12] + iUnpackedPixelStart;
  ucpa[13] = ucpUnpacked[iJob][13] + iUnpackedPixelStart;
  ucpa[14] = ucpUnpacked[iJob][14] + iUnpackedPixelStart;
  ucpa[15] = ucpUnpacked[iJob][15] + iUnpackedPixelStart;
  ucpa[16] = ucpUnpacked[iJob][16] + iUnpackedPixelStart;
  ucpa[17] = ucpUnpacked[iJob][17] + iUnpackedPixelStart;
  ucpa[18] = ucpUnpacked[iJob][18] + iUnpackedPixelStart;
  ucpa[19] = ucpUnpacked[iJob][19] + iUnpackedPixelStart;
  ucpa[20] = ucpUnpacked[iJob][20] + iUnpackedPixelStart;
  ucpa[21] = ucpUnpacked[iJob][21] + iUnpackedPixelStart;
  ucpa[22] = ucpUnpacked[iJob][22] + iUnpackedPixelStart;
  ucpa[23] = ucpUnpacked[iJob][23] + iUnpackedPixelStart;

  iR = 0;
  ucpS = ucpSrc[0] + (iR * iPitchSrc);
  for (iRow = iUnpackedPixelStart; iRow < 0; iRow++)
   {
    *ucpa[0]++ = ucpS[0];	// U
    *ucpa[1]++ = ucpS[1];	// Y
    *ucpa[2]++ = ucpS[2];	// V
    *ucpa[3]++ = ucpS[3];	// Y

    *ucpa[4]++ = ucpS[4];	// U
    *ucpa[5]++ = ucpS[5];	// Y
    *ucpa[6]++ = ucpS[6];	// V
    *ucpa[7]++ = ucpS[7];	// Y

    *ucpa[8]++ = ucpS[8];	// U
    *ucpa[9]++ = ucpS[9];	// Y
    *ucpa[10]++ = ucpS[10]; // V
    *ucpa[11]++ = ucpS[11]; // Y

    *ucpa[12]++ = ucpS[12];	// V
    *ucpa[13]++ = ucpS[13];	// Y
    *ucpa[14]++ = ucpS[14];	// U
    *ucpa[15]++ = ucpS[15];	// Y

    *ucpa[16]++ = ucpS[16];	// V
    *ucpa[17]++ = ucpS[17];	// Y
    *ucpa[18]++ = ucpS[18];	// U
    *ucpa[19]++ = ucpS[19];	// Y

    *ucpa[20]++ = ucpS[20];	// U
    *ucpa[21]++ = ucpS[21];	// Y
    *ucpa[22]++ = ucpS[22]; // V
    *ucpa[23]++ = ucpS[23]; // Y
   }

  iRowStop = iUnpackedPixelStop;
  if (iRowStop > iNRowSrc)
    iRowStop = iNRowSrc;

  ucpS = ucpSrc[0] + (iRow * iPitchSrc);
  //ucpSStop = ucpSrc[0] + ((iRowStop - 7) * iPitchSrc);

  for (; iRow < (iRowStop - 7); iRow += 8)
   {
    ucpTmp[0][0] = ucpS[0];	// U
    ucpTmp[1][0] = ucpS[1];	// Y
    ucpTmp[2][0] = ucpS[2];	// V
    ucpTmp[3][0] = ucpS[3];	// Y
    ucpTmp[4][0] = ucpS[4];	// U
    ucpTmp[5][0] = ucpS[5];	// Y
    ucpTmp[6][0] = ucpS[6];	// V
    ucpTmp[7][0] = ucpS[7];	// Y
    ucpTmp[8][0] = ucpS[8];	// U
    ucpTmp[9][0] = ucpS[9];	// Y
    ucpTmp[10][0] = ucpS[10];	// V
    ucpTmp[11][0] = ucpS[11];	// Y
    ucpTmp[12][0] = ucpS[12];	// U
    ucpTmp[13][0] = ucpS[13];	// Y
    ucpTmp[14][0] = ucpS[14];	// V
    ucpTmp[15][0] = ucpS[15];	// Y
    ucpTmp[16][0] = ucpS[16];	// U
    ucpTmp[17][0] = ucpS[17];	// Y
    ucpTmp[18][0] = ucpS[18];	// V
    ucpTmp[19][0] = ucpS[19];	// Y
    ucpTmp[20][0] = ucpS[20];	// U
    ucpTmp[21][0] = ucpS[21];	// Y
    ucpTmp[22][0] = ucpS[22];	// V
    ucpTmp[23][0] = ucpS[23];	// Y

    ucpS += iPitchSrc;

    ucpTmp[0][1] = ucpS[0];	// U
    ucpTmp[1][1] = ucpS[1];	// Y
    ucpTmp[2][1] = ucpS[2];	// V
    ucpTmp[3][1] = ucpS[3];	// Y
    ucpTmp[4][1] = ucpS[4];	// U
    ucpTmp[5][1] = ucpS[5];	// Y
    ucpTmp[6][1] = ucpS[6];	// V
    ucpTmp[7][1] = ucpS[7];	// Y
    ucpTmp[8][1] = ucpS[8];	// U
    ucpTmp[9][1] = ucpS[9];	// Y
    ucpTmp[10][1] = ucpS[10];	// V
    ucpTmp[11][1] = ucpS[11];	// Y
    ucpTmp[12][1] = ucpS[12];	// U
    ucpTmp[13][1] = ucpS[13];	// Y
    ucpTmp[14][1] = ucpS[14];	// V
    ucpTmp[15][1] = ucpS[15];	// Y
    ucpTmp[16][1] = ucpS[16];	// U
    ucpTmp[17][1] = ucpS[17];	// Y
    ucpTmp[18][1] = ucpS[18];	// V
    ucpTmp[19][1] = ucpS[19];	// Y
    ucpTmp[20][1] = ucpS[20];	// U
    ucpTmp[21][1] = ucpS[21];	// Y
    ucpTmp[22][1] = ucpS[22];	// V
    ucpTmp[23][1] = ucpS[23];	// Y

    ucpS += iPitchSrc;

    ucpTmp[0][2] = ucpS[0];	// U
    ucpTmp[1][2] = ucpS[1];	// Y
    ucpTmp[2][2] = ucpS[2];	// V
    ucpTmp[3][2] = ucpS[3];	// Y
    ucpTmp[4][2] = ucpS[4];	// U
    ucpTmp[5][2] = ucpS[5];	// Y
    ucpTmp[6][2] = ucpS[6];	// V
    ucpTmp[7][2] = ucpS[7];	// Y
    ucpTmp[8][2] = ucpS[8];	// U
    ucpTmp[9][2] = ucpS[9];	// Y
    ucpTmp[10][2] = ucpS[10];	// V
    ucpTmp[11][2] = ucpS[11];	// Y
    ucpTmp[12][2] = ucpS[12];	// U
    ucpTmp[13][2] = ucpS[13];	// Y
    ucpTmp[14][2] = ucpS[14];	// V
    ucpTmp[15][2] = ucpS[15];	// Y
    ucpTmp[16][2] = ucpS[16];	// U
    ucpTmp[17][2] = ucpS[17];	// Y
    ucpTmp[18][2] = ucpS[18];	// V
    ucpTmp[19][2] = ucpS[19];	// Y
    ucpTmp[20][2] = ucpS[20];	// U
    ucpTmp[21][2] = ucpS[21];	// Y
    ucpTmp[22][2] = ucpS[22];	// V
    ucpTmp[23][2] = ucpS[23];	// Y

    ucpS += iPitchSrc;

    ucpTmp[0][3] = ucpS[0];	// U
    ucpTmp[1][3] = ucpS[1];	// Y
    ucpTmp[2][3] = ucpS[2];	// V
    ucpTmp[3][3] = ucpS[3];	// Y
    ucpTmp[4][3] = ucpS[4];	// U
    ucpTmp[5][3] = ucpS[5];	// Y
    ucpTmp[6][3] = ucpS[6];	// V
    ucpTmp[7][3] = ucpS[7];	// Y
    ucpTmp[8][3] = ucpS[8];	// U
    ucpTmp[9][3] = ucpS[9];	// Y
    ucpTmp[10][3] = ucpS[10];	// V
    ucpTmp[11][3] = ucpS[11];	// Y
    ucpTmp[12][3] = ucpS[12];	// U
    ucpTmp[13][3] = ucpS[13];	// Y
    ucpTmp[14][3] = ucpS[14];	// V
    ucpTmp[15][3] = ucpS[15];	// Y
    ucpTmp[16][3] = ucpS[16];	// U
    ucpTmp[17][3] = ucpS[17];	// Y
    ucpTmp[18][3] = ucpS[18];	// V
    ucpTmp[19][3] = ucpS[19];	// Y
    ucpTmp[20][3] = ucpS[20];	// U
    ucpTmp[21][3] = ucpS[21];	// Y
    ucpTmp[22][3] = ucpS[22];	// V
    ucpTmp[23][3] = ucpS[23];	// Y

    ucpS += iPitchSrc;

    ucpTmp[0][4] = ucpS[0];	// U
    ucpTmp[1][4] = ucpS[1];	// Y
    ucpTmp[2][4] = ucpS[2];	// V
    ucpTmp[3][4] = ucpS[3];	// Y
    ucpTmp[4][4] = ucpS[4];	// U
    ucpTmp[5][4] = ucpS[5];	// Y
    ucpTmp[6][4] = ucpS[6];	// V
    ucpTmp[7][4] = ucpS[7];	// Y
    ucpTmp[8][4] = ucpS[8];	// U
    ucpTmp[9][4] = ucpS[9];	// Y
    ucpTmp[10][4] = ucpS[10];	// V
    ucpTmp[11][4] = ucpS[11];	// Y
    ucpTmp[12][4] = ucpS[12];	// U
    ucpTmp[13][4] = ucpS[13];	// Y
    ucpTmp[14][4] = ucpS[14];	// V
    ucpTmp[15][4] = ucpS[15];	// Y
    ucpTmp[16][4] = ucpS[16];	// U
    ucpTmp[17][4] = ucpS[17];	// Y
    ucpTmp[18][4] = ucpS[18];	// V
    ucpTmp[19][4] = ucpS[19];	// Y
    ucpTmp[20][4] = ucpS[20];	// U
    ucpTmp[21][4] = ucpS[21];	// Y
    ucpTmp[22][4] = ucpS[22];	// V
    ucpTmp[23][4] = ucpS[23];	// Y

    ucpS += iPitchSrc;

    ucpTmp[0][5] = ucpS[0];	// U
    ucpTmp[1][5] = ucpS[1];	// Y
    ucpTmp[2][5] = ucpS[2];	// V
    ucpTmp[3][5] = ucpS[3];	// Y
    ucpTmp[4][5] = ucpS[4];	// U
    ucpTmp[5][5] = ucpS[5];	// Y
    ucpTmp[6][5] = ucpS[6];	// V
    ucpTmp[7][5] = ucpS[7];	// Y
    ucpTmp[8][5] = ucpS[8];	// U
    ucpTmp[9][5] = ucpS[9];	// Y
    ucpTmp[10][5] = ucpS[10];	// V
    ucpTmp[11][5] = ucpS[11];	// Y
    ucpTmp[12][5] = ucpS[12];	// U
    ucpTmp[13][5] = ucpS[13];	// Y
    ucpTmp[14][5] = ucpS[14];	// V
    ucpTmp[15][5] = ucpS[15];	// Y
    ucpTmp[16][5] = ucpS[16];	// U
    ucpTmp[17][5] = ucpS[17];	// Y
    ucpTmp[18][5] = ucpS[18];	// V
    ucpTmp[19][5] = ucpS[19];	// Y
    ucpTmp[20][5] = ucpS[20];	// U
    ucpTmp[21][5] = ucpS[21];	// Y
    ucpTmp[22][5] = ucpS[22];	// V
    ucpTmp[23][5] = ucpS[23];	// Y

    ucpS += iPitchSrc;

    ucpTmp[0][6] = ucpS[0];	// U
    ucpTmp[1][6] = ucpS[1];	// Y
    ucpTmp[2][6] = ucpS[2];	// V
    ucpTmp[3][6] = ucpS[3];	// Y
    ucpTmp[4][6] = ucpS[4];	// U
    ucpTmp[5][6] = ucpS[5];	// Y
    ucpTmp[6][6] = ucpS[6];	// V
    ucpTmp[7][6] = ucpS[7];	// Y
    ucpTmp[8][6] = ucpS[8];	// U
    ucpTmp[9][6] = ucpS[9];	// Y
    ucpTmp[10][6] = ucpS[10];	// V
    ucpTmp[11][6] = ucpS[11];	// Y
    ucpTmp[12][6] = ucpS[12];	// U
    ucpTmp[13][6] = ucpS[13];	// Y
    ucpTmp[14][6] = ucpS[14];	// V
    ucpTmp[15][6] = ucpS[15];	// Y
    ucpTmp[16][6] = ucpS[16];	// U
    ucpTmp[17][6] = ucpS[17];	// Y
    ucpTmp[18][6] = ucpS[18];	// V
    ucpTmp[19][6] = ucpS[19];	// Y
    ucpTmp[20][6] = ucpS[20];	// U
    ucpTmp[21][6] = ucpS[21];	// Y
    ucpTmp[22][6] = ucpS[22];	// V
    ucpTmp[23][6] = ucpS[23];	// Y

    ucpS += iPitchSrc;

    ucpTmp[0][7] = ucpS[0];	// U
    ucpTmp[1][7] = ucpS[1];	// Y
    ucpTmp[2][7] = ucpS[2];	// V
    ucpTmp[3][7] = ucpS[3];	// Y
    ucpTmp[4][7] = ucpS[4];	// U
    ucpTmp[5][7] = ucpS[5];	// Y
    ucpTmp[6][7] = ucpS[6];	// V
    ucpTmp[7][7] = ucpS[7];	// Y
    ucpTmp[8][7] = ucpS[8];	// U
    ucpTmp[9][7] = ucpS[9];	// Y
    ucpTmp[10][7] = ucpS[10];	// V
    ucpTmp[11][7] = ucpS[11];	// Y
    ucpTmp[12][7] = ucpS[12];	// U
    ucpTmp[13][7] = ucpS[13];	// Y
    ucpTmp[14][7] = ucpS[14];	// V
    ucpTmp[15][7] = ucpS[15];	// Y
    ucpTmp[16][7] = ucpS[16];	// U
    ucpTmp[17][7] = ucpS[17];	// Y
    ucpTmp[18][7] = ucpS[18];	// V
    ucpTmp[19][7] = ucpS[19];	// Y
    ucpTmp[20][7] = ucpS[20];	// U
    ucpTmp[21][7] = ucpS[21];	// Y
    ucpTmp[22][7] = ucpS[22];	// V
    ucpTmp[23][7] = ucpS[23];	// Y

    ucpS += iPitchSrc;

    *(long *) &ucpa[0][0]  = *(long *) &ucpTmp[0][0];
    *(long *) &ucpa[0][4]  = *(long *) &ucpTmp[0][4];
    *(long *) &ucpa[1][0]  = *(long *) &ucpTmp[1][0];
    *(long *) &ucpa[1][4]  = *(long *) &ucpTmp[1][4];
    *(long *) &ucpa[2][0]  = *(long *) &ucpTmp[2][0];
    *(long *) &ucpa[2][4]  = *(long *) &ucpTmp[2][4];
    *(long *) &ucpa[3][0]  = *(long *) &ucpTmp[3][0];
    *(long *) &ucpa[3][4]  = *(long *) &ucpTmp[3][4];

    *(long *) &ucpa[4][0]  = *(long *) &ucpTmp[4][0];
    *(long *) &ucpa[4][4]  = *(long *) &ucpTmp[4][4];
    *(long *) &ucpa[5][0]  = *(long *) &ucpTmp[5][0];
    *(long *) &ucpa[5][4]  = *(long *) &ucpTmp[5][4];
    *(long *) &ucpa[6][0]  = *(long *) &ucpTmp[6][0];
    *(long *) &ucpa[6][4]  = *(long *) &ucpTmp[6][4];
    *(long *) &ucpa[7][0]  = *(long *) &ucpTmp[7][0];
    *(long *) &ucpa[7][4]  = *(long *) &ucpTmp[7][4];

    *(long *) &ucpa[8][0]  = *(long *) &ucpTmp[8][0];
    *(long *) &ucpa[8][4]  = *(long *) &ucpTmp[8][4];
    *(long *) &ucpa[9][0]  = *(long *) &ucpTmp[9][0];
    *(long *) &ucpa[9][4]  = *(long *) &ucpTmp[9][4];
    *(long *) &ucpa[10][0] = *(long *) &ucpTmp[10][0];
    *(long *) &ucpa[10][4] = *(long *) &ucpTmp[10][4];
    *(long *) &ucpa[11][0] = *(long *) &ucpTmp[11][0];
    *(long *) &ucpa[11][4] = *(long *) &ucpTmp[11][4];

    *(long *) &ucpa[12][0] = *(long *) &ucpTmp[12][0];
    *(long *) &ucpa[12][4] = *(long *) &ucpTmp[12][4];
    *(long *) &ucpa[13][0] = *(long *) &ucpTmp[13][0];
    *(long *) &ucpa[13][4] = *(long *) &ucpTmp[13][4];
    *(long *) &ucpa[14][0] = *(long *) &ucpTmp[14][0];
    *(long *) &ucpa[14][4] = *(long *) &ucpTmp[14][4];
    *(long *) &ucpa[15][0] = *(long *) &ucpTmp[15][0];
    *(long *) &ucpa[15][4] = *(long *) &ucpTmp[15][4];

    *(long *) &ucpa[16][0] = *(long *) &ucpTmp[16][0];
    *(long *) &ucpa[16][4] = *(long *) &ucpTmp[16][4];
    *(long *) &ucpa[17][0] = *(long *) &ucpTmp[17][0];
    *(long *) &ucpa[17][4] = *(long *) &ucpTmp[17][4];
    *(long *) &ucpa[18][0] = *(long *) &ucpTmp[18][0];
    *(long *) &ucpa[18][4] = *(long *) &ucpTmp[18][4];
    *(long *) &ucpa[19][0] = *(long *) &ucpTmp[19][0];
    *(long *) &ucpa[19][4] = *(long *) &ucpTmp[19][4];

    *(long *) &ucpa[20][0]  = *(long *) &ucpTmp[20][0];
    *(long *) &ucpa[20][4]  = *(long *) &ucpTmp[20][4];
    *(long *) &ucpa[21][0]  = *(long *) &ucpTmp[21][0];
    *(long *) &ucpa[21][4]  = *(long *) &ucpTmp[21][4];
    *(long *) &ucpa[22][0] = *(long *) &ucpTmp[22][0];
    *(long *) &ucpa[22][4] = *(long *) &ucpTmp[22][4];
    *(long *) &ucpa[23][0] = *(long *) &ucpTmp[23][0];
    *(long *) &ucpa[23][4] = *(long *) &ucpTmp[23][4];


    ucpa[0] += 8;
    ucpa[1] += 8;
    ucpa[2] += 8;
    ucpa[3] += 8;
    ucpa[4] += 8;
    ucpa[5] += 8;
    ucpa[6] += 8;
    ucpa[7] += 8;
    ucpa[8] += 8;
    ucpa[9] += 8;
    ucpa[10] += 8;
    ucpa[11] += 8;
    ucpa[12] += 8;
    ucpa[13] += 8;
    ucpa[14] += 8;
    ucpa[15] += 8;
    ucpa[16] += 8;
    ucpa[17] += 8;
    ucpa[18] += 8;
    ucpa[19] += 8;
    ucpa[20] += 8;
    ucpa[21] += 8;
    ucpa[22] += 8;
    ucpa[23] += 8;
   }
  //while (ucpS < ucpSStop);

  for (; iRow < iRowStop; iRow++)
   {
    ucpS = ucpSrc[0] + (iRow * iPitchSrc);

    *ucpa[0]++ = ucpS[0];	// U
    *ucpa[1]++ = ucpS[1];	// Y
    *ucpa[2]++ = ucpS[2];	// V
    *ucpa[3]++ = ucpS[3];	// Y

    *ucpa[4]++ = ucpS[4];	// U
    *ucpa[5]++ = ucpS[5];	// Y
    *ucpa[6]++ = ucpS[6];	// V
    *ucpa[7]++ = ucpS[7];	// Y

    *ucpa[8]++ = ucpS[8];	// U
    *ucpa[9]++ = ucpS[9];	// Y
    *ucpa[10]++= ucpS[10]; // V
    *ucpa[11]++= ucpS[11]; // Y

    *ucpa[12]++ = ucpS[12];	// V
    *ucpa[13]++ = ucpS[13];	// Y
    *ucpa[14]++ = ucpS[14];	// U
    *ucpa[15]++ = ucpS[15];	// Y

    *ucpa[16]++ = ucpS[16];	// V
    *ucpa[17]++ = ucpS[17];	// Y
    *ucpa[18]++ = ucpS[18];	// U
    *ucpa[19]++ = ucpS[19];	// Y

    *ucpa[20]++ = ucpS[20];	// U
    *ucpa[21]++ = ucpS[21];	// Y
    *ucpa[22]++ = ucpS[22]; // V
    *ucpa[23]++ = ucpS[23]; // Y
   }

  iR = (iNRowSrc-1);
  ucpS = ucpSrc[0] + (iR * iPitchSrc);
   {
    *ucpa[0]++ = ucpS[0];	// U
    *ucpa[1]++ = ucpS[1];	// Y
    *ucpa[2]++ = ucpS[2];	// V
    *ucpa[3]++ = ucpS[3];	// Y


    *ucpa[4]++ = ucpS[4];	// U
    *ucpa[5]++ = ucpS[5];	// Y
    *ucpa[6]++ = ucpS[6];	// V
    *ucpa[7]++ = ucpS[7];	// Y

    *ucpa[8]++ = ucpS[8];	// U
    *ucpa[9]++ = ucpS[9];	// Y
    *ucpa[10]++= ucpS[10]; // V
    *ucpa[11]++= ucpS[11]; // Y


    *ucpa[12]++ = ucpS[12];	// V
    *ucpa[13]++ = ucpS[13];	// Y
    *ucpa[14]++ = ucpS[14];	// U
    *ucpa[15]++ = ucpS[15];	// Y

    *ucpa[16]++ = ucpS[16];	// V
    *ucpa[17]++ = ucpS[17];	// Y
    *ucpa[18]++ = ucpS[18];	// U
    *ucpa[19]++ = ucpS[19];	// Y

    *ucpa[20]++ = ucpS[20];	// U
    *ucpa[21]++ = ucpS[21];	// Y
    *ucpa[22]++ = ucpS[22]; // V
    *ucpa[23]++ = ucpS[23]; // Y
   }
}


void CAspectRatio::UnpackV_10_YUV_2 (MTI_UINT8 *ucpSrc[2], int iJob)
{
  int iField, iR, iRow;
  MTI_UINT8 *ucpS;
  int iRowStop;
  MTI_UINT8 *ucp0, *ucp1, *ucp2, *ucp3, *ucp4, *ucp5, *ucp6, *ucp7,
		*ucp8, *ucp9, *ucp10, *ucp11, *ucp12, *ucp13, *ucp14, *ucp15,
    *ucp16, *ucp17, *ucp18, *ucp19, *ucp20, *ucp21, *ucp22, *ucp23;

  ucp0 = ucpUnpacked[iJob][0] + iUnpackedPixelStart;
  ucp1 = ucpUnpacked[iJob][1] + iUnpackedPixelStart;
  ucp2 = ucpUnpacked[iJob][2] + iUnpackedPixelStart;
  ucp3 = ucpUnpacked[iJob][3] + iUnpackedPixelStart;
  ucp4 = ucpUnpacked[iJob][4] + iUnpackedPixelStart;
  ucp5 = ucpUnpacked[iJob][5] + iUnpackedPixelStart;
  ucp6 = ucpUnpacked[iJob][6] + iUnpackedPixelStart;
  ucp7 = ucpUnpacked[iJob][7] + iUnpackedPixelStart;
  ucp8 = ucpUnpacked[iJob][8] + iUnpackedPixelStart;
  ucp9 = ucpUnpacked[iJob][9] + iUnpackedPixelStart;
  ucp10 = ucpUnpacked[iJob][10] + iUnpackedPixelStart;
  ucp11 = ucpUnpacked[iJob][11] + iUnpackedPixelStart;
  ucp12 = ucpUnpacked[iJob][12] + iUnpackedPixelStart;
  ucp13 = ucpUnpacked[iJob][13] + iUnpackedPixelStart;
  ucp14 = ucpUnpacked[iJob][14] + iUnpackedPixelStart;
  ucp15 = ucpUnpacked[iJob][15] + iUnpackedPixelStart;
  ucp16 = ucpUnpacked[iJob][16] + iUnpackedPixelStart;
  ucp17 = ucpUnpacked[iJob][17] + iUnpackedPixelStart;
  ucp18 = ucpUnpacked[iJob][18] + iUnpackedPixelStart;
  ucp19 = ucpUnpacked[iJob][19] + iUnpackedPixelStart;
  ucp20 = ucpUnpacked[iJob][20] + iUnpackedPixelStart;
  ucp21 = ucpUnpacked[iJob][21] + iUnpackedPixelStart;
  ucp22 = ucpUnpacked[iJob][22] + iUnpackedPixelStart;
  ucp23 = ucpUnpacked[iJob][23] + iUnpackedPixelStart;

  iField = 0;
  iR = 0;
  ucpS = ucpSrc[iField] + (iR * iPitchSrc);
  for (iRow = iUnpackedPixelStart; iRow < 0; iRow++)
   {
#if !MTI_X86_ASM_INTEL
    // turn the 10 bit values into 8 bits
    *ucp0++ = ((ucpS[1] & 0x03)<<6) + ((ucpS[0] & 0xFF)>>2);    // U
    *ucp1++ = ((ucpS[2] & 0x0F)<<4) + ((ucpS[1] & 0xFC)>>4);    // Y
    *ucp2++ = ((ucpS[3] & 0x3F)<<4) + ((ucpS[2] & 0xF0)>>6);    // V

    *ucp3++ = ((ucpS[5] & 0x03)<<6) + ((ucpS[4] & 0xFF)>>2);    // Y
    *ucp4++ = ((ucpS[6] & 0x0F)<<4) + ((ucpS[5] & 0xFC)>>4);    // U
    *ucp5++ = ((ucpS[7] & 0x3F)<<4) + ((ucpS[6] & 0xF0)>>6);    // Y

    *ucp6++ = ((ucpS[9] & 0x03)<<6) + ((ucpS[8] & 0xFF)>>2);    // V
    *ucp7++ = ((ucpS[10] & 0x0F)<<4) + ((ucpS[9] & 0xFC)>>4);   // Y
    *ucp8++ = ((ucpS[11] & 0x3F)<<4) + ((ucpS[10] & 0xF0)>>6);  // U

    *ucp9++ = ((ucpS[13] & 0x03)<<6) + ((ucpS[12] & 0xFF)>>2);  // Y
    *ucp10++ = ((ucpS[14] & 0x0F)<<4) + ((ucpS[13] & 0xFC)>>4); // V
    *ucp11++ = ((ucpS[15] & 0x3F)<<4) + ((ucpS[14] & 0xF0)>>6); // Y

    *ucp12++ = ((ucpS[17] & 0x03)<<6) + ((ucpS[16] & 0xFF)>>2); // U
    *ucp13++ = ((ucpS[18] & 0x0F)<<4) + ((ucpS[17] & 0xFC)>>4); // Y
    *ucp14++ = ((ucpS[19] & 0x3F)<<4) + ((ucpS[18] & 0xF0)>>6); // V

    *ucp15++ = ((ucpS[21] & 0x03)<<6) + ((ucpS[20] & 0xFF)>>2); // Y
    *ucp16++ = ((ucpS[22] & 0x0F)<<4) + ((ucpS[21] & 0xFC)>>4); // U
    *ucp17++ = ((ucpS[23] & 0x3F)<<4) + ((ucpS[22] & 0xF0)>>6); // Y

    *ucp18++ = ((ucpS[25] & 0x03)<<6) + ((ucpS[24] & 0xFF)>>2); // V
    *ucp19++ = ((ucpS[26] & 0x0F)<<4) + ((ucpS[25] & 0xFC)>>4); // Y
    *ucp20++ = ((ucpS[27] & 0x3F)<<4) + ((ucpS[26] & 0xF0)>>6); // U

    *ucp21++ = ((ucpS[29] & 0x03)<<6) + ((ucpS[28] & 0xFF)>>2); // Y
    *ucp22++ = ((ucpS[30] & 0x0F)<<4) + ((ucpS[29] & 0xFC)>>4); // V
    *ucp23++ = ((ucpS[31] & 0x3F)<<4) + ((ucpS[30] & 0xF0)>>6); // Y
#else
    _asm {
    mov    esi,ucpS

    mov    eax,[esi]
    shr    eax,2
    mov    ebx,ucp0
    mov    [ebx],al
    inc    ucp0

    shr    eax,10
    mov    ebx,ucp1
    mov    [ebx],al
    inc    ucp1

    shr    eax,10
    mov    ebx,ucp2
    mov    [ebx],al
    inc    ucp2

    mov    eax,[esi+4]
    shr    eax,2
    mov    ebx,ucp3
    mov    [ebx],al
    inc    ucp3

    shr    eax,10
    mov    ebx,ucp4
    mov    [ebx],al
    inc    ucp4

    shr    eax,10
    mov    ebx,ucp5
    mov    [ebx],al
    inc    ucp5

    mov    eax,[esi+8]
    shr    eax,2
    mov    ebx,ucp6
    mov    [ebx],al
    inc    ucp6

    shr    eax,10
    mov    ebx,ucp7
    mov    [ebx],al
    inc    ucp7

    shr    eax,10
    mov    ebx,ucp8
    mov    [ebx],al
    inc    ucp8

    mov    eax,[esi+12]
    shr    eax,2
    mov    ebx,ucp9
    mov    [ebx],al
    inc    ucp9

    shr    eax,10
    mov    ebx,ucp10
    mov    [ebx],al
    inc    ucp10

    shr    eax,10
    mov    ebx,ucp11
    mov    [ebx],al
    inc    ucp11

    mov    eax,[esi+16]
    shr    eax,2
    mov    ebx,ucp12
    mov    [ebx],al
    inc    ucp12

    shr    eax,10
    mov    ebx,ucp13
    mov    [ebx],al
    inc    ucp13

    shr    eax,10
    mov    ebx,ucp14
    mov    [ebx],al
    inc    ucp14

    mov    eax,[esi+20]
    shr    eax,2
    mov    ebx,ucp15
    mov    [ebx],al
    inc    ucp15

    shr    eax,10
    mov    ebx,ucp16
    mov    [ebx],al
    inc    ucp16

    shr    eax,10
    mov    ebx,ucp17
    mov    [ebx],al
    inc    ucp17

    mov    eax,[esi+24]
    shr    eax,2
    mov    ebx,ucp18
    mov    [ebx],al
    inc    ucp18

    shr    eax,10
    mov    ebx,ucp19
    mov    [ebx],al
    inc    ucp19

    shr    eax,10
    mov    ebx,ucp20
    mov    [ebx],al
    inc    ucp20

    mov    eax,[esi+28]
    shr    eax,2
    mov    ebx,ucp21
    mov    [ebx],al
    inc    ucp21

    shr    eax,10
    mov    ebx,ucp22
    mov    [ebx],al
    inc    ucp22

    shr    eax,10
    mov    ebx,ucp23
    mov    [ebx],al
    inc    ucp23
    }
#endif
   }

  iRowStop = iUnpackedPixelStop;
  if (iRowStop > iNRowSrc)
    iRowStop = iNRowSrc;

  for (; iRow < iRowStop; iRow++)
   {
    iField = iRow % 2;
    iR = iRow / 2;

    ucpS = ucpSrc[iField] + (iR * iPitchSrc);

#if !MTI_ASM_X86_INTEL
    // turn the 10 bit values into 8 bits
    *ucp0++ = ((ucpS[1] & 0x03)<<6) + ((ucpS[0] & 0xFF)>>2);    // U
    *ucp1++ = ((ucpS[2] & 0x0F)<<4) + ((ucpS[1] & 0xFC)>>4);    // Y
    *ucp2++ = ((ucpS[3] & 0x3F)<<4) + ((ucpS[2] & 0xF0)>>6);    // V

    *ucp3++ = ((ucpS[5] & 0x03)<<6) + ((ucpS[4] & 0xFF)>>2);    // Y
    *ucp4++ = ((ucpS[6] & 0x0F)<<4) + ((ucpS[5] & 0xFC)>>4);    // U
    *ucp5++ = ((ucpS[7] & 0x3F)<<4) + ((ucpS[6] & 0xF0)>>6);    // Y

    *ucp6++ = ((ucpS[9] & 0x03)<<6) + ((ucpS[8] & 0xFF)>>2);    // V
    *ucp7++ = ((ucpS[10] & 0x0F)<<4) + ((ucpS[9] & 0xFC)>>4);   // Y
    *ucp8++ = ((ucpS[11] & 0x3F)<<4) + ((ucpS[10] & 0xF0)>>6);  // U

    *ucp9++ = ((ucpS[13] & 0x03)<<6) + ((ucpS[12] & 0xFF)>>2);  // Y
    *ucp10++= ((ucpS[14] & 0x0F)<<4) + ((ucpS[13] & 0xFC)>>4);  // V
    *ucp11++= ((ucpS[15] & 0x3F)<<4) + ((ucpS[14] & 0xF0)>>6);  // Y

    *ucp12++ = ((ucpS[17] & 0x03)<<6) + ((ucpS[16] & 0xFF)>>2); // U
    *ucp13++ = ((ucpS[18] & 0x0F)<<4) + ((ucpS[17] & 0xFC)>>4); // Y
    *ucp14++ = ((ucpS[19] & 0x3F)<<4) + ((ucpS[18] & 0xF0)>>6); // V

    *ucp15++ = ((ucpS[21] & 0x03)<<6) + ((ucpS[20] & 0xFF)>>2); // Y
    *ucp16++ = ((ucpS[22] & 0x0F)<<4) + ((ucpS[21] & 0xFC)>>4); // U
    *ucp17++ = ((ucpS[23] & 0x3F)<<4) + ((ucpS[22] & 0xF0)>>6); // Y

    *ucp18++ = ((ucpS[25] & 0x03)<<6) + ((ucpS[24] & 0xFF)>>2); // V
    *ucp19++ = ((ucpS[26] & 0x0F)<<4) + ((ucpS[25] & 0xFC)>>4); // Y
    *ucp20++ = ((ucpS[27] & 0x3F)<<4) + ((ucpS[26] & 0xF0)>>6); // U

    *ucp21++ = ((ucpS[29] & 0x03)<<6) + ((ucpS[28] & 0xFF)>>2); // Y
    *ucp22++ = ((ucpS[30] & 0x0F)<<4) + ((ucpS[29] & 0xFC)>>4); // V
    *ucp23++ = ((ucpS[31] & 0x3F)<<4) + ((ucpS[30] & 0xF0)>>6); // Y
#else
    _asm {
    mov    esi,ucpS

    mov    eax,[esi]
    shr    eax,2
    mov    ebx,ucp0
    mov    [ebx],al
    inc    ucp0

    shr    eax,10
    mov    ebx,ucp1
    mov    [ebx],al
    inc    ucp1

    shr    eax,10
    mov    ebx,ucp2
    mov    [ebx],al
    inc    ucp2

    mov    eax,[esi+4]
    shr    eax,2
    mov    ebx,ucp3
    mov    [ebx],al
    inc    ucp3

    shr    eax,10
    mov    ebx,ucp4
    mov    [ebx],al
    inc    ucp4

    shr    eax,10
    mov    ebx,ucp5
    mov    [ebx],al
    inc    ucp5

    mov    eax,[esi+8]
    shr    eax,2
    mov    ebx,ucp6
    mov    [ebx],al
    inc    ucp6

    shr    eax,10
    mov    ebx,ucp7
    mov    [ebx],al
    inc    ucp7

    shr    eax,10
    mov    ebx,ucp8
    mov    [ebx],al
    inc    ucp8

    mov    eax,[esi+12]
    shr    eax,2
    mov    ebx,ucp9
    mov    [ebx],al
    inc    ucp9

    shr    eax,10
    mov    ebx,ucp10
    mov    [ebx],al
    inc    ucp10

    shr    eax,10
    mov    ebx,ucp11
    mov    [ebx],al
    inc    ucp11

    mov    eax,[esi+16]
    shr    eax,2
    mov    ebx,ucp12
    mov    [ebx],al
    inc    ucp12

    shr    eax,10
    mov    ebx,ucp13
    mov    [ebx],al
    inc    ucp13

    shr    eax,10
    mov    ebx,ucp14
    mov    [ebx],al
    inc    ucp14

    mov    eax,[esi+20]
    shr    eax,2
    mov    ebx,ucp15
    mov    [ebx],al
    inc    ucp15

    shr    eax,10
    mov    ebx,ucp16
    mov    [ebx],al
    inc    ucp16

    shr    eax,10
    mov    ebx,ucp17
    mov    [ebx],al
    inc    ucp17

    mov    eax,[esi+24]
    shr    eax,2
    mov    ebx,ucp18
    mov    [ebx],al
    inc    ucp18

    shr    eax,10
    mov    ebx,ucp19
    mov    [ebx],al
    inc    ucp19

    shr    eax,10
    mov    ebx,ucp20
    mov    [ebx],al
    inc    ucp20

    mov    eax,[esi+28]
    shr    eax,2
    mov    ebx,ucp21
    mov    [ebx],al
    inc    ucp21

    shr    eax,10
    mov    ebx,ucp22
    mov    [ebx],al
    inc    ucp22

    shr    eax,10
    mov    ebx,ucp23
    mov    [ebx],al
    inc    ucp23
    }
#endif
   }

  iField = (iNRowSrc-1)%2;
  iR = (iNRowSrc-1)/2;
  ucpS = ucpSrc[iField] + (iR * iPitchSrc);
  for (; iRow < iUnpackedPixelStop; iRow++)
   {
#if !MTI_ASM_X86_INTEL
    // turn the 10 bit values into 8 bits
    *ucp0++ = ((ucpS[1] & 0x03)<<6) + ((ucpS[0] & 0xFF)>>2);    // U
    *ucp1++ = ((ucpS[2] & 0x0F)<<4) + ((ucpS[1] & 0xFC)>>4);    // Y
    *ucp2++ = ((ucpS[3] & 0x3F)<<4) + ((ucpS[2] & 0xF0)>>6);    // V

    *ucp3++ = ((ucpS[5] & 0x03)<<6) + ((ucpS[4] & 0xFF)>>2);    // Y
    *ucp4++ = ((ucpS[6] & 0x0F)<<4) + ((ucpS[5] & 0xFC)>>4);    // U
    *ucp5++ = ((ucpS[7] & 0x3F)<<4) + ((ucpS[6] & 0xF0)>>6);    // Y

    *ucp6++ = ((ucpS[9] & 0x03)<<6) + ((ucpS[8] & 0xFF)>>2);    // V
    *ucp7++ = ((ucpS[10] & 0x0F)<<4) + ((ucpS[9] & 0xFC)>>4);   // Y
    *ucp8++ = ((ucpS[11] & 0x3F)<<4) + ((ucpS[10] & 0xF0)>>6);  // U

    *ucp9++ = ((ucpS[13] & 0x03)<<6) + ((ucpS[12] & 0xFF)>>2);  // Y
    *ucp10++= ((ucpS[14] & 0x0F)<<4) + ((ucpS[13] & 0xFC)>>4);  // V
    *ucp11++= ((ucpS[15] & 0x3F)<<4) + ((ucpS[14] & 0xF0)>>6);  // Y

    *ucp12++ = ((ucpS[17] & 0x03)<<6) + ((ucpS[16] & 0xFF)>>2); // U
    *ucp13++ = ((ucpS[18] & 0x0F)<<4) + ((ucpS[17] & 0xFC)>>4); // Y
    *ucp14++ = ((ucpS[19] & 0x3F)<<4) + ((ucpS[18] & 0xF0)>>6); // V

    *ucp15++ = ((ucpS[21] & 0x03)<<6) + ((ucpS[20] & 0xFF)>>2); // Y
    *ucp16++ = ((ucpS[22] & 0x0F)<<4) + ((ucpS[21] & 0xFC)>>4); // U
    *ucp17++ = ((ucpS[23] & 0x3F)<<4) + ((ucpS[22] & 0xF0)>>6); // Y

    *ucp18++ = ((ucpS[25] & 0x03)<<6) + ((ucpS[24] & 0xFF)>>2); // V
    *ucp19++ = ((ucpS[26] & 0x0F)<<4) + ((ucpS[25] & 0xFC)>>4); // Y
    *ucp20++ = ((ucpS[27] & 0x3F)<<4) + ((ucpS[26] & 0xF0)>>6); // U

    *ucp21++ = ((ucpS[29] & 0x03)<<6) + ((ucpS[28] & 0xFF)>>2); // Y
    *ucp22++ = ((ucpS[30] & 0x0F)<<4) + ((ucpS[29] & 0xFC)>>4); // V
    *ucp23++ = ((ucpS[31] & 0x3F)<<4) + ((ucpS[30] & 0xF0)>>6); // Y
#else
    _asm {
    mov    esi,ucpS

    mov    eax,[esi]
    shr    eax,2
    mov    ebx,ucp0
    mov    [ebx],al
    inc    ucp0

    shr    eax,10
    mov    ebx,ucp1
    mov    [ebx],al
    inc    ucp1

    shr    eax,10
    mov    ebx,ucp2
    mov    [ebx],al
    inc    ucp2

    mov    eax,[esi+4]
    shr    eax,2
    mov    ebx,ucp3
    mov    [ebx],al
    inc    ucp3

    shr    eax,10
    mov    ebx,ucp4
    mov    [ebx],al
    inc    ucp4

    shr    eax,10
    mov    ebx,ucp5
    mov    [ebx],al
    inc    ucp5

    mov    eax,[esi+8]
    shr    eax,2
    mov    ebx,ucp6
    mov    [ebx],al
    inc    ucp6

    shr    eax,10
    mov    ebx,ucp7
    mov    [ebx],al
    inc    ucp7

    shr    eax,10
    mov    ebx,ucp8
    mov    [ebx],al
    inc    ucp8

    mov    eax,[esi+12]
    shr    eax,2
    mov    ebx,ucp9
    mov    [ebx],al
    inc    ucp9

    shr    eax,10
    mov    ebx,ucp10
    mov    [ebx],al
    inc    ucp10

    shr    eax,10
    mov    ebx,ucp11
    mov    [ebx],al
    inc    ucp11

    mov    eax,[esi+16]
    shr    eax,2
    mov    ebx,ucp12
    mov    [ebx],al
    inc    ucp12

    shr    eax,10
    mov    ebx,ucp13
    mov    [ebx],al
    inc    ucp13

    shr    eax,10
    mov    ebx,ucp14
    mov    [ebx],al
    inc    ucp14

    mov    eax,[esi+20]
    shr    eax,2
    mov    ebx,ucp15
    mov    [ebx],al
    inc    ucp15

    shr    eax,10
    mov    ebx,ucp16
    mov    [ebx],al
    inc    ucp16

    shr    eax,10
    mov    ebx,ucp17
    mov    [ebx],al
    inc    ucp17

    mov    eax,[esi+24]
    shr    eax,2
    mov    ebx,ucp18
    mov    [ebx],al
    inc    ucp18

    shr    eax,10
    mov    ebx,ucp19
    mov    [ebx],al
    inc    ucp19

    shr    eax,10
    mov    ebx,ucp20
    mov    [ebx],al
    inc    ucp20

    mov    eax,[esi+28]
    shr    eax,2
    mov    ebx,ucp21
    mov    [ebx],al
    inc    ucp21

    shr    eax,10
    mov    ebx,ucp22
    mov    [ebx],al
    inc    ucp22

    shr    eax,10
    mov    ebx,ucp23
    mov    [ebx],al
    inc    ucp23
    }
#endif
   }


}

void CAspectRatio::UnpackV_10_YUV_1 (MTI_UINT8 *ucpSrc[2], int iJob)
{
  int iRow, iR;
  MTI_UINT8 *ucpS;
  int iRowStop;
  MTI_UINT8 *ucp0, *ucp1, *ucp2, *ucp3, *ucp4, *ucp5, *ucp6, *ucp7,
		*ucp8, *ucp9, *ucp10, *ucp11, *ucp12, *ucp13, *ucp14, *ucp15,
    *ucp16, *ucp17, *ucp18, *ucp19, *ucp20, *ucp21, *ucp22, *ucp23;

  ucp0 = ucpUnpacked[iJob][0] + iUnpackedPixelStart;
  ucp1 = ucpUnpacked[iJob][1] + iUnpackedPixelStart;
  ucp2 = ucpUnpacked[iJob][2] + iUnpackedPixelStart;
  ucp3 = ucpUnpacked[iJob][3] + iUnpackedPixelStart;
  ucp4 = ucpUnpacked[iJob][4] + iUnpackedPixelStart;
  ucp5 = ucpUnpacked[iJob][5] + iUnpackedPixelStart;
  ucp6 = ucpUnpacked[iJob][6] + iUnpackedPixelStart;
  ucp7 = ucpUnpacked[iJob][7] + iUnpackedPixelStart;
  ucp8 = ucpUnpacked[iJob][8] + iUnpackedPixelStart;
  ucp9 = ucpUnpacked[iJob][9] + iUnpackedPixelStart;
  ucp10 = ucpUnpacked[iJob][10] + iUnpackedPixelStart;
  ucp11 = ucpUnpacked[iJob][11] + iUnpackedPixelStart;
  ucp12 = ucpUnpacked[iJob][12] + iUnpackedPixelStart;
  ucp13 = ucpUnpacked[iJob][13] + iUnpackedPixelStart;
  ucp14 = ucpUnpacked[iJob][14] + iUnpackedPixelStart;
  ucp15 = ucpUnpacked[iJob][15] + iUnpackedPixelStart;
  ucp16 = ucpUnpacked[iJob][16] + iUnpackedPixelStart;
  ucp17 = ucpUnpacked[iJob][17] + iUnpackedPixelStart;
  ucp18 = ucpUnpacked[iJob][18] + iUnpackedPixelStart;
  ucp19 = ucpUnpacked[iJob][19] + iUnpackedPixelStart;
  ucp20 = ucpUnpacked[iJob][20] + iUnpackedPixelStart;
  ucp21 = ucpUnpacked[iJob][21] + iUnpackedPixelStart;
  ucp22 = ucpUnpacked[iJob][22] + iUnpackedPixelStart;
  ucp23 = ucpUnpacked[iJob][23] + iUnpackedPixelStart;

  iR = 0;
  ucpS = ucpSrc[0] + (iR * iPitchSrc);
  for (iRow = iUnpackedPixelStart; iRow < 0; iRow++)
   {
#if !MTI_ASM_X86_INTEL
    // turn the 10 bit values into 8 bits
    *ucp0++ = ((ucpS[1] & 0x03)<<6) + ((ucpS[0] & 0xFF)>>2);    // U
    *ucp1++ = ((ucpS[2] & 0x0F)<<4) + ((ucpS[1] & 0xFC)>>4);    // Y
    *ucp2++ = ((ucpS[3] & 0x3F)<<4) + ((ucpS[2] & 0xF0)>>6);    // V

    *ucp3++ = ((ucpS[5] & 0x03)<<6) + ((ucpS[4] & 0xFF)>>2);    // Y
    *ucp4++ = ((ucpS[6] & 0x0F)<<4) + ((ucpS[5] & 0xFC)>>4);    // U
    *ucp5++ = ((ucpS[7] & 0x3F)<<4) + ((ucpS[6] & 0xF0)>>6);    // Y

    *ucp6++ = ((ucpS[9] & 0x03)<<6) + ((ucpS[8] & 0xFF)>>2);    // V
    *ucp7++ = ((ucpS[10] & 0x0F)<<4) + ((ucpS[9] & 0xFC)>>4);   // Y
    *ucp8++ = ((ucpS[11] & 0x3F)<<4) + ((ucpS[10] & 0xF0)>>6);  // U

    *ucp9++ = ((ucpS[13] & 0x03)<<6) + ((ucpS[12] & 0xFF)>>2);  // Y
    *ucp10++ = ((ucpS[14] & 0x0F)<<4) + ((ucpS[13] & 0xFC)>>4); // V
    *ucp11++ = ((ucpS[15] & 0x3F)<<4) + ((ucpS[14] & 0xF0)>>6); // Y

    *ucp12++ = ((ucpS[17] & 0x03)<<6) + ((ucpS[16] & 0xFF)>>2); // U
    *ucp13++ = ((ucpS[18] & 0x0F)<<4) + ((ucpS[17] & 0xFC)>>4); // Y
    *ucp14++ = ((ucpS[19] & 0x3F)<<4) + ((ucpS[18] & 0xF0)>>6); // V

    *ucp15++ = ((ucpS[21] & 0x03)<<6) + ((ucpS[20] & 0xFF)>>2); // Y
    *ucp16++ = ((ucpS[22] & 0x0F)<<4) + ((ucpS[21] & 0xFC)>>4); // U
    *ucp17++ = ((ucpS[23] & 0x3F)<<4) + ((ucpS[22] & 0xF0)>>6); // Y

    *ucp18++ = ((ucpS[25] & 0x03)<<6) + ((ucpS[24] & 0xFF)>>2); // V
    *ucp19++ = ((ucpS[26] & 0x0F)<<4) + ((ucpS[25] & 0xFC)>>4); // Y
    *ucp20++ = ((ucpS[27] & 0x3F)<<4) + ((ucpS[26] & 0xF0)>>6); // U

    *ucp21++ = ((ucpS[29] & 0x03)<<6) + ((ucpS[28] & 0xFF)>>2); // Y
    *ucp22++ = ((ucpS[30] & 0x0F)<<4) + ((ucpS[29] & 0xFC)>>4); // V
    *ucp23++ = ((ucpS[31] & 0x3F)<<4) + ((ucpS[30] & 0xF0)>>6); // Y
#else
    _asm {
    mov    esi,ucpS

    mov    eax,[esi]
    shr    eax,2
    mov    ebx,ucp0
    mov    [ebx],al
    inc    ucp0

    shr    eax,10
    mov    ebx,ucp1
    mov    [ebx],al
    inc    ucp1

    shr    eax,10
    mov    ebx,ucp2
    mov    [ebx],al
    inc    ucp2

    mov    eax,[esi+4]
    shr    eax,2
    mov    ebx,ucp3
    mov    [ebx],al
    inc    ucp3

    shr    eax,10
    mov    ebx,ucp4
    mov    [ebx],al
    inc    ucp4

    shr    eax,10
    mov    ebx,ucp5
    mov    [ebx],al
    inc    ucp5

    mov    eax,[esi+8]
    shr    eax,2
    mov    ebx,ucp6
    mov    [ebx],al
    inc    ucp6

    shr    eax,10
    mov    ebx,ucp7
    mov    [ebx],al
    inc    ucp7

    shr    eax,10
    mov    ebx,ucp8
    mov    [ebx],al
    inc    ucp8

    mov    eax,[esi+12]
    shr    eax,2
    mov    ebx,ucp9
    mov    [ebx],al
    inc    ucp9

    shr    eax,10
    mov    ebx,ucp10
    mov    [ebx],al
    inc    ucp10

    shr    eax,10
    mov    ebx,ucp11
    mov    [ebx],al
    inc    ucp11

    mov    eax,[esi+16]
    shr    eax,2
    mov    ebx,ucp12
    mov    [ebx],al
    inc    ucp12

    shr    eax,10
    mov    ebx,ucp13
    mov    [ebx],al
    inc    ucp13

    shr    eax,10
    mov    ebx,ucp14
    mov    [ebx],al
    inc    ucp14

    mov    eax,[esi+20]
    shr    eax,2
    mov    ebx,ucp15
    mov    [ebx],al
    inc    ucp15

    shr    eax,10
    mov    ebx,ucp16
    mov    [ebx],al
    inc    ucp16

    shr    eax,10
    mov    ebx,ucp17
    mov    [ebx],al
    inc    ucp17

    mov    eax,[esi+24]
    shr    eax,2
    mov    ebx,ucp18
    mov    [ebx],al
    inc    ucp18

    shr    eax,10
    mov    ebx,ucp19
    mov    [ebx],al
    inc    ucp19

    shr    eax,10
    mov    ebx,ucp20
    mov    [ebx],al
    inc    ucp20

    mov    eax,[esi+28]
    shr    eax,2
    mov    ebx,ucp21
    mov    [ebx],al
    inc    ucp21

    shr    eax,10
    mov    ebx,ucp22
    mov    [ebx],al
    inc    ucp22

    shr    eax,10
    mov    ebx,ucp23
    mov    [ebx],al
    inc    ucp23
    }
#endif
   }

  iRowStop = iUnpackedPixelStop;
  if (iRowStop > iNRowSrc)
    iRowStop = iNRowSrc;

  for (; iRow < iRowStop; iRow++)
   {
    ucpS = ucpSrc[0] + (iRow * iPitchSrc);

#if !MTI_ASM_X86_INTEL
    // turn the 10 bit values into 8 bits
    *ucp0++ = ((ucpS[1] & 0x03)<<6) + ((ucpS[0] & 0xFF)>>2);    // U
    *ucp1++ = ((ucpS[2] & 0x0F)<<4) + ((ucpS[1] & 0xFC)>>4);    // Y
    *ucp2++ = ((ucpS[3] & 0x3F)<<4) + ((ucpS[2] & 0xF0)>>6);    // V

    *ucp3++ = ((ucpS[5] & 0x03)<<6) + ((ucpS[4] & 0xFF)>>2);    // Y
    *ucp4++ = ((ucpS[6] & 0x0F)<<4) + ((ucpS[5] & 0xFC)>>4);    // U
    *ucp5++ = ((ucpS[7] & 0x3F)<<4) + ((ucpS[6] & 0xF0)>>6);    // Y

    *ucp6++ = ((ucpS[9] & 0x03)<<6) + ((ucpS[8] & 0xFF)>>2);    // V
    *ucp7++ = ((ucpS[10] & 0x0F)<<4) + ((ucpS[9] & 0xFC)>>4);   // Y
    *ucp8++ = ((ucpS[11] & 0x3F)<<4) + ((ucpS[10] & 0xF0)>>6);  // U

    *ucp9++ = ((ucpS[13] & 0x03)<<6) + ((ucpS[12] & 0xFF)>>2);  // Y
    *ucp10++= ((ucpS[14] & 0x0F)<<4) + ((ucpS[13] & 0xFC)>>4);  // V
    *ucp11++= ((ucpS[15] & 0x3F)<<4) + ((ucpS[14] & 0xF0)>>6);  // Y

    *ucp12++ = ((ucpS[17] & 0x03)<<6) + ((ucpS[16] & 0xFF)>>2); // U
    *ucp13++ = ((ucpS[18] & 0x0F)<<4) + ((ucpS[17] & 0xFC)>>4); // Y
    *ucp14++ = ((ucpS[19] & 0x3F)<<4) + ((ucpS[18] & 0xF0)>>6); // V

    *ucp15++ = ((ucpS[21] & 0x03)<<6) + ((ucpS[20] & 0xFF)>>2); // Y
    *ucp16++ = ((ucpS[22] & 0x0F)<<4) + ((ucpS[21] & 0xFC)>>4); // U
    *ucp17++ = ((ucpS[23] & 0x3F)<<4) + ((ucpS[22] & 0xF0)>>6); // Y

    *ucp18++ = ((ucpS[25] & 0x03)<<6) + ((ucpS[24] & 0xFF)>>2); // V
    *ucp19++ = ((ucpS[26] & 0x0F)<<4) + ((ucpS[25] & 0xFC)>>4); // Y
    *ucp20++ = ((ucpS[27] & 0x3F)<<4) + ((ucpS[26] & 0xF0)>>6); // U

    *ucp21++ = ((ucpS[29] & 0x03)<<6) + ((ucpS[28] & 0xFF)>>2); // Y
    *ucp22++ = ((ucpS[30] & 0x0F)<<4) + ((ucpS[29] & 0xFC)>>4); // V
    *ucp23++ = ((ucpS[31] & 0x3F)<<4) + ((ucpS[30] & 0xF0)>>6); // Y
#else
    _asm {
    mov    esi,ucpS

    mov    eax,[esi]
    shr    eax,2
    mov    ebx,ucp0
    mov    [ebx],al
    inc    ucp0

    shr    eax,10
    mov    ebx,ucp1
    mov    [ebx],al
    inc    ucp1

    shr    eax,10
    mov    ebx,ucp2
    mov    [ebx],al
    inc    ucp2

    mov    eax,[esi+4]
    shr    eax,2
    mov    ebx,ucp3
    mov    [ebx],al
    inc    ucp3

    shr    eax,10
    mov    ebx,ucp4
    mov    [ebx],al
    inc    ucp4

    shr    eax,10
    mov    ebx,ucp5
    mov    [ebx],al
    inc    ucp5

    mov    eax,[esi+8]
    shr    eax,2
    mov    ebx,ucp6
    mov    [ebx],al
    inc    ucp6

    shr    eax,10
    mov    ebx,ucp7
    mov    [ebx],al
    inc    ucp7

    shr    eax,10
    mov    ebx,ucp8
    mov    [ebx],al
    inc    ucp8

    mov    eax,[esi+12]
    shr    eax,2
    mov    ebx,ucp9
    mov    [ebx],al
    inc    ucp9

    shr    eax,10
    mov    ebx,ucp10
    mov    [ebx],al
    inc    ucp10

    shr    eax,10
    mov    ebx,ucp11
    mov    [ebx],al
    inc    ucp11

    mov    eax,[esi+16]
    shr    eax,2
    mov    ebx,ucp12
    mov    [ebx],al
    inc    ucp12

    shr    eax,10
    mov    ebx,ucp13
    mov    [ebx],al
    inc    ucp13

    shr    eax,10
    mov    ebx,ucp14
    mov    [ebx],al
    inc    ucp14

    mov    eax,[esi+20]
    shr    eax,2
    mov    ebx,ucp15
    mov    [ebx],al
    inc    ucp15

    shr    eax,10
    mov    ebx,ucp16
    mov    [ebx],al
    inc    ucp16

    shr    eax,10
    mov    ebx,ucp17
    mov    [ebx],al
    inc    ucp17

    mov    eax,[esi+24]
    shr    eax,2
    mov    ebx,ucp18
    mov    [ebx],al
    inc    ucp18

    shr    eax,10
    mov    ebx,ucp19
    mov    [ebx],al
    inc    ucp19

    shr    eax,10
    mov    ebx,ucp20
    mov    [ebx],al
    inc    ucp20

    mov    eax,[esi+28]
    shr    eax,2
    mov    ebx,ucp21
    mov    [ebx],al
    inc    ucp21

    shr    eax,10
    mov    ebx,ucp22
    mov    [ebx],al
    inc    ucp22

    shr    eax,10
    mov    ebx,ucp23
    mov    [ebx],al
    inc    ucp23
    }
#endif
   }

  iR = (iNRowSrc-1);
  ucpS = ucpSrc[0] + (iR * iPitchSrc);
  for (; iRow < iUnpackedPixelStop; iRow++)
   {
#if !MTI_ASM_X86_INTEL
    // turn the 10 bit values into 8 bits
    *ucp0++ = ((ucpS[1] & 0x03)<<6) + ((ucpS[0] & 0xFF)>>2);    // U
    *ucp1++ = ((ucpS[2] & 0x0F)<<4) + ((ucpS[1] & 0xFC)>>4);    // Y
    *ucp2++ = ((ucpS[3] & 0x3F)<<4) + ((ucpS[2] & 0xF0)>>6);    // V

    *ucp3++ = ((ucpS[5] & 0x03)<<6) + ((ucpS[4] & 0xFF)>>2);    // Y
    *ucp4++ = ((ucpS[6] & 0x0F)<<4) + ((ucpS[5] & 0xFC)>>4);    // U
    *ucp5++ = ((ucpS[7] & 0x3F)<<4) + ((ucpS[6] & 0xF0)>>6);    // Y

    *ucp6++ = ((ucpS[9] & 0x03)<<6) + ((ucpS[8] & 0xFF)>>2);    // V
    *ucp7++ = ((ucpS[10] & 0x0F)<<4) + ((ucpS[9] & 0xFC)>>4);   // Y
    *ucp8++ = ((ucpS[11] & 0x3F)<<4) + ((ucpS[10] & 0xF0)>>6);  // U

    *ucp9++ = ((ucpS[13] & 0x03)<<6) + ((ucpS[12] & 0xFF)>>2);  // Y
    *ucp10++= ((ucpS[14] & 0x0F)<<4) + ((ucpS[13] & 0xFC)>>4);  // V
    *ucp11++= ((ucpS[15] & 0x3F)<<4) + ((ucpS[14] & 0xF0)>>6);  // Y

    *ucp12++ = ((ucpS[17] & 0x03)<<6) + ((ucpS[16] & 0xFF)>>2); // U
    *ucp13++ = ((ucpS[18] & 0x0F)<<4) + ((ucpS[17] & 0xFC)>>4); // Y
    *ucp14++ = ((ucpS[19] & 0x3F)<<4) + ((ucpS[18] & 0xF0)>>6); // V

    *ucp15++ = ((ucpS[21] & 0x03)<<6) + ((ucpS[20] & 0xFF)>>2); // Y
    *ucp16++ = ((ucpS[22] & 0x0F)<<4) + ((ucpS[21] & 0xFC)>>4); // U
    *ucp17++ = ((ucpS[23] & 0x3F)<<4) + ((ucpS[22] & 0xF0)>>6); // Y

    *ucp18++ = ((ucpS[25] & 0x03)<<6) + ((ucpS[24] & 0xFF)>>2); // V
    *ucp19++ = ((ucpS[26] & 0x0F)<<4) + ((ucpS[25] & 0xFC)>>4); // Y
    *ucp20++ = ((ucpS[27] & 0x3F)<<4) + ((ucpS[26] & 0xF0)>>6); // U

    *ucp21++ = ((ucpS[29] & 0x03)<<6) + ((ucpS[28] & 0xFF)>>2); // Y
    *ucp22++ = ((ucpS[30] & 0x0F)<<4) + ((ucpS[29] & 0xFC)>>4); // V
    *ucp23++ = ((ucpS[31] & 0x3F)<<4) + ((ucpS[30] & 0xF0)>>6); // Y
#else
    _asm {
    mov    esi,ucpS

    mov    eax,[esi]
    shr    eax,2
    mov    ebx,ucp0
    mov    [ebx],al
    inc    ucp0

    shr    eax,10
    mov    ebx,ucp1
    mov    [ebx],al
    inc    ucp1

    shr    eax,10
    mov    ebx,ucp2
    mov    [ebx],al
    inc    ucp2

    mov    eax,[esi+4]
    shr    eax,2
    mov    ebx,ucp3
    mov    [ebx],al
    inc    ucp3

    shr    eax,10
    mov    ebx,ucp4
    mov    [ebx],al
    inc    ucp4

    shr    eax,10
    mov    ebx,ucp5
    mov    [ebx],al
    inc    ucp5

    mov    eax,[esi+8]
    shr    eax,2
    mov    ebx,ucp6
    mov    [ebx],al
    inc    ucp6

    shr    eax,10
    mov    ebx,ucp7
    mov    [ebx],al
    inc    ucp7

    shr    eax,10
    mov    ebx,ucp8
    mov    [ebx],al
    inc    ucp8

    mov    eax,[esi+12]
    shr    eax,2
    mov    ebx,ucp9
    mov    [ebx],al
    inc    ucp9

    shr    eax,10
    mov    ebx,ucp10
    mov    [ebx],al
    inc    ucp10

    shr    eax,10
    mov    ebx,ucp11
    mov    [ebx],al
    inc    ucp11

    mov    eax,[esi+16]
    shr    eax,2
    mov    ebx,ucp12
    mov    [ebx],al
    inc    ucp12

    shr    eax,10
    mov    ebx,ucp13
    mov    [ebx],al
    inc    ucp13

    shr    eax,10
    mov    ebx,ucp14
    mov    [ebx],al
    inc    ucp14

    mov    eax,[esi+20]
    shr    eax,2
    mov    ebx,ucp15
    mov    [ebx],al
    inc    ucp15

    shr    eax,10
    mov    ebx,ucp16
    mov    [ebx],al
    inc    ucp16

    shr    eax,10
    mov    ebx,ucp17
    mov    [ebx],al
    inc    ucp17

    mov    eax,[esi+24]
    shr    eax,2
    mov    ebx,ucp18
    mov    [ebx],al
    inc    ucp18

    shr    eax,10
    mov    ebx,ucp19
    mov    [ebx],al
    inc    ucp19

    shr    eax,10
    mov    ebx,ucp20
    mov    [ebx],al
    inc    ucp20

    mov    eax,[esi+28]
    shr    eax,2
    mov    ebx,ucp21
    mov    [ebx],al
    inc    ucp21

    shr    eax,10
    mov    ebx,ucp22
    mov    [ebx],al
    inc    ucp22

    shr    eax,10
    mov    ebx,ucp23
    mov    [ebx],al
    inc    ucp23
    }
#endif
   }

}


void CAspectRatio::UnpackV_10a_YUV_2 (MTI_UINT8 *ucpSrc[2], int iJob)
{
  int iField, iR, iRow;
  MTI_UINT8 *ucpS;
  int iRowStop;
  MTI_UINT8 *ucp0, *ucp1, *ucp2, *ucp3, *ucp4, *ucp5, *ucp6, *ucp7,
		*ucp8, *ucp9, *ucp10, *ucp11, *ucp12, *ucp13, *ucp14, *ucp15,
    *ucp16, *ucp17, *ucp18, *ucp19, *ucp20, *ucp21, *ucp22, *ucp23;

  ucp0 = ucpUnpacked[iJob][0] + iUnpackedPixelStart;
  ucp1 = ucpUnpacked[iJob][1] + iUnpackedPixelStart;
  ucp2 = ucpUnpacked[iJob][2] + iUnpackedPixelStart;
  ucp3 = ucpUnpacked[iJob][3] + iUnpackedPixelStart;
  ucp4 = ucpUnpacked[iJob][4] + iUnpackedPixelStart;
  ucp5 = ucpUnpacked[iJob][5] + iUnpackedPixelStart;
  ucp6 = ucpUnpacked[iJob][6] + iUnpackedPixelStart;
  ucp7 = ucpUnpacked[iJob][7] + iUnpackedPixelStart;
  ucp8 = ucpUnpacked[iJob][8] + iUnpackedPixelStart;
  ucp9 = ucpUnpacked[iJob][9] + iUnpackedPixelStart;
  ucp10 = ucpUnpacked[iJob][10] + iUnpackedPixelStart;
  ucp11 = ucpUnpacked[iJob][11] + iUnpackedPixelStart;
  ucp12 = ucpUnpacked[iJob][12] + iUnpackedPixelStart;
  ucp13 = ucpUnpacked[iJob][13] + iUnpackedPixelStart;
  ucp14 = ucpUnpacked[iJob][14] + iUnpackedPixelStart;
  ucp15 = ucpUnpacked[iJob][15] + iUnpackedPixelStart;
  ucp16 = ucpUnpacked[iJob][16] + iUnpackedPixelStart;
  ucp17 = ucpUnpacked[iJob][17] + iUnpackedPixelStart;
  ucp18 = ucpUnpacked[iJob][18] + iUnpackedPixelStart;
  ucp19 = ucpUnpacked[iJob][19] + iUnpackedPixelStart;
  ucp20 = ucpUnpacked[iJob][20] + iUnpackedPixelStart;
  ucp21 = ucpUnpacked[iJob][21] + iUnpackedPixelStart;
  ucp22 = ucpUnpacked[iJob][22] + iUnpackedPixelStart;
  ucp23 = ucpUnpacked[iJob][23] + iUnpackedPixelStart;

  iField = 0;
  iR = 0;
  ucpS = ucpSrc[iField] + (iR * iPitchSrc);
  for (iRow = iUnpackedPixelStart; iRow < 0; iRow++)
   {
    // turn the 10 bit values into 8 bits
    *ucp0++ = ucpS[ 0];    // U
    *ucp1++ = ucpS[ 1];    // Y
    *ucp2++ = ucpS[ 2];    // V

    *ucp3++ = ucpS[ 4];    // Y
    *ucp4++ = ucpS[ 5];    // U
    *ucp5++ = ucpS[ 6];    // Y

    *ucp6++ = ucpS[ 8];    // V
    *ucp7++ = ucpS[ 9];    // Y
    *ucp8++ = ucpS[10];    // U

    *ucp9++ = ucpS[12];    // Y
    *ucp10++ = ucpS[13];   // V
    *ucp11++ = ucpS[14];   // Y

    *ucp12++ = ucpS[16];   // U
    *ucp13++ = ucpS[17];   // Y
    *ucp14++ = ucpS[18];   // V

    *ucp15++ = ucpS[20];   // Y
    *ucp16++ = ucpS[21];   // U
    *ucp17++ = ucpS[22];   // Y

    *ucp18++ = ucpS[24];   // V
    *ucp19++ = ucpS[25];   // Y
    *ucp20++ = ucpS[26];   // U

    *ucp21++ = ucpS[28];   // Y
    *ucp22++ = ucpS[29];   // V
    *ucp23++ = ucpS[30];   // Y
   }

  iRowStop = iUnpackedPixelStop;
  if (iRowStop > iNRowSrc)
    iRowStop = iNRowSrc;

  for (; iRow < iRowStop; iRow++)
   {
    iField = iRow % 2;
    iR = iRow / 2;

    ucpS = ucpSrc[iField] + (iR * iPitchSrc);

    // turn the 10 bit values into 8 bits
    *ucp0++ = ucpS[ 0];    // U
    *ucp1++ = ucpS[ 1];    // Y
    *ucp2++ = ucpS[ 2];    // V

    *ucp3++ = ucpS[ 4];    // Y
    *ucp4++ = ucpS[ 5];    // U
    *ucp5++ = ucpS[ 6];    // Y

    *ucp6++ = ucpS[ 8];    // V
    *ucp7++ = ucpS[ 9];    // Y
    *ucp8++ = ucpS[10];    // U

    *ucp9++ = ucpS[12];    // Y
    *ucp10++ = ucpS[13];   // V
    *ucp11++ = ucpS[14];   // Y

    *ucp12++ = ucpS[16];   // U
    *ucp13++ = ucpS[17];   // Y
    *ucp14++ = ucpS[18];   // V

    *ucp15++ = ucpS[20];   // Y
    *ucp16++ = ucpS[21];   // U
    *ucp17++ = ucpS[22];   // Y

    *ucp18++ = ucpS[24];   // V
    *ucp19++ = ucpS[25];   // Y
    *ucp20++ = ucpS[26];   // U

    *ucp21++ = ucpS[28];   // Y
    *ucp22++ = ucpS[29];   // V
    *ucp23++ = ucpS[30];   // Y
   }

  iField = (iNRowSrc-1)%2;
  iR = (iNRowSrc-1)/2;
  ucpS = ucpSrc[iField] + (iR * iPitchSrc);
  for (; iRow < iUnpackedPixelStop; iRow++)
   {
    // turn the 10 bit values into 8 bits
    *ucp0++ = ucpS[ 0];    // U
    *ucp1++ = ucpS[ 1];    // Y
    *ucp2++ = ucpS[ 2];    // V

    *ucp3++ = ucpS[ 4];    // Y
    *ucp4++ = ucpS[ 5];    // U
    *ucp5++ = ucpS[ 6];    // Y

    *ucp6++ = ucpS[ 8];    // V
    *ucp7++ = ucpS[ 9];    // Y
    *ucp8++ = ucpS[10];    // U

    *ucp9++ = ucpS[12];    // Y
    *ucp10++ = ucpS[13];   // V
    *ucp11++ = ucpS[14];   // Y

    *ucp12++ = ucpS[16];   // U
    *ucp13++ = ucpS[17];   // Y
    *ucp14++ = ucpS[18];   // V

    *ucp15++ = ucpS[20];   // Y
    *ucp16++ = ucpS[21];   // U
    *ucp17++ = ucpS[22];   // Y

    *ucp18++ = ucpS[24];   // V
    *ucp19++ = ucpS[25];   // Y
    *ucp20++ = ucpS[26];   // U

    *ucp21++ = ucpS[28];   // Y
    *ucp22++ = ucpS[29];   // V
    *ucp23++ = ucpS[30];   // Y
   }


}

void CAspectRatio::UnpackV_10a_YUV_1 (MTI_UINT8 *ucpSrc[2], int iJob)
{
  int iRow, iR;
  MTI_UINT8 *ucpS;
  int iRowStop;
  MTI_UINT8 *ucp0, *ucp1, *ucp2, *ucp3, *ucp4, *ucp5, *ucp6, *ucp7,
		*ucp8, *ucp9, *ucp10, *ucp11, *ucp12, *ucp13, *ucp14, *ucp15,
    *ucp16, *ucp17, *ucp18, *ucp19, *ucp20, *ucp21, *ucp22, *ucp23;

  ucp0 = ucpUnpacked[iJob][0] + iUnpackedPixelStart;
  ucp1 = ucpUnpacked[iJob][1] + iUnpackedPixelStart;
  ucp2 = ucpUnpacked[iJob][2] + iUnpackedPixelStart;
  ucp3 = ucpUnpacked[iJob][3] + iUnpackedPixelStart;
  ucp4 = ucpUnpacked[iJob][4] + iUnpackedPixelStart;
  ucp5 = ucpUnpacked[iJob][5] + iUnpackedPixelStart;
  ucp6 = ucpUnpacked[iJob][6] + iUnpackedPixelStart;
  ucp7 = ucpUnpacked[iJob][7] + iUnpackedPixelStart;
  ucp8 = ucpUnpacked[iJob][8] + iUnpackedPixelStart;
  ucp9 = ucpUnpacked[iJob][9] + iUnpackedPixelStart;
  ucp10 = ucpUnpacked[iJob][10] + iUnpackedPixelStart;
  ucp11 = ucpUnpacked[iJob][11] + iUnpackedPixelStart;
  ucp12 = ucpUnpacked[iJob][12] + iUnpackedPixelStart;
  ucp13 = ucpUnpacked[iJob][13] + iUnpackedPixelStart;
  ucp14 = ucpUnpacked[iJob][14] + iUnpackedPixelStart;
  ucp15 = ucpUnpacked[iJob][15] + iUnpackedPixelStart;
  ucp16 = ucpUnpacked[iJob][16] + iUnpackedPixelStart;
  ucp17 = ucpUnpacked[iJob][17] + iUnpackedPixelStart;
  ucp18 = ucpUnpacked[iJob][18] + iUnpackedPixelStart;
  ucp19 = ucpUnpacked[iJob][19] + iUnpackedPixelStart;
  ucp20 = ucpUnpacked[iJob][20] + iUnpackedPixelStart;
  ucp21 = ucpUnpacked[iJob][21] + iUnpackedPixelStart;
  ucp22 = ucpUnpacked[iJob][22] + iUnpackedPixelStart;
  ucp23 = ucpUnpacked[iJob][23] + iUnpackedPixelStart;

  iR = 0;
  ucpS = ucpSrc[0] + (iR * iPitchSrc);
  for (iRow = iUnpackedPixelStart; iRow < 0; iRow++)
   {
    // turn the 10 bit values into 8 bits
    *ucp0++ = ucpS[ 0];    // U
    *ucp1++ = ucpS[ 1];    // Y
    *ucp2++ = ucpS[ 2];    // V

    *ucp3++ = ucpS[ 4];    // Y
    *ucp4++ = ucpS[ 5];    // U
    *ucp5++ = ucpS[ 6];    // Y

    *ucp6++ = ucpS[ 8];    // V
    *ucp7++ = ucpS[ 9];    // Y
    *ucp8++ = ucpS[10];    // U

    *ucp9++ = ucpS[12];    // Y
    *ucp10++ = ucpS[13];   // V
    *ucp11++ = ucpS[14];   // Y

    *ucp12++ = ucpS[16];   // U
    *ucp13++ = ucpS[17];   // Y
    *ucp14++ = ucpS[18];   // V

    *ucp15++ = ucpS[20];   // Y
    *ucp16++ = ucpS[21];   // U
    *ucp17++ = ucpS[22];   // Y

    *ucp18++ = ucpS[24];   // V
    *ucp19++ = ucpS[25];   // Y
    *ucp20++ = ucpS[26];   // U

    *ucp21++ = ucpS[28];   // Y
    *ucp22++ = ucpS[29];   // V
    *ucp23++ = ucpS[30];   // Y
   }

  iRowStop = iUnpackedPixelStop;
  if (iRowStop > iNRowSrc)
    iRowStop = iNRowSrc;

  for (; iRow < iRowStop; iRow++)
   {
    ucpS = ucpSrc[0] + (iRow * iPitchSrc);

    // turn the 10 bit values into 8 bits
    *ucp0++ = ucpS[ 0];    // U
    *ucp1++ = ucpS[ 1];    // Y
    *ucp2++ = ucpS[ 2];    // V

    *ucp3++ = ucpS[ 4];    // Y
    *ucp4++ = ucpS[ 5];    // U
    *ucp5++ = ucpS[ 6];    // Y

    *ucp6++ = ucpS[ 8];    // V
    *ucp7++ = ucpS[ 9];    // Y
    *ucp8++ = ucpS[10];    // U

    *ucp9++ = ucpS[12];    // Y
    *ucp10++ = ucpS[13];   // V
    *ucp11++ = ucpS[14];   // Y

    *ucp12++ = ucpS[16];   // U
    *ucp13++ = ucpS[17];   // Y
    *ucp14++ = ucpS[18];   // V

    *ucp15++ = ucpS[20];   // Y
    *ucp16++ = ucpS[21];   // U
    *ucp17++ = ucpS[22];   // Y

    *ucp18++ = ucpS[24];   // V
    *ucp19++ = ucpS[25];   // Y
    *ucp20++ = ucpS[26];   // U

    *ucp21++ = ucpS[28];   // Y
    *ucp22++ = ucpS[29];   // V
    *ucp23++ = ucpS[30];   // Y
   }

  iR = (iNRowSrc-1);
  ucpS = ucpSrc[0] + (iR * iPitchSrc);
  for (; iRow < iUnpackedPixelStop; iRow++)
   {
    // turn the 10 bit values into 8 bits
    *ucp0++ = ucpS[ 0];    // U
    *ucp1++ = ucpS[ 1];    // Y
    *ucp2++ = ucpS[ 2];    // V

    *ucp3++ = ucpS[ 4];    // Y
    *ucp4++ = ucpS[ 5];    // U
    *ucp5++ = ucpS[ 6];    // Y

    *ucp6++ = ucpS[ 8];    // V
    *ucp7++ = ucpS[ 9];    // Y
    *ucp8++ = ucpS[10];    // U

    *ucp9++ = ucpS[12];    // Y
    *ucp10++ = ucpS[13];   // V
    *ucp11++ = ucpS[14];   // Y

    *ucp12++ = ucpS[16];   // U
    *ucp13++ = ucpS[17];   // Y
    *ucp14++ = ucpS[18];   // V

    *ucp15++ = ucpS[20];   // Y
    *ucp16++ = ucpS[21];   // U
    *ucp17++ = ucpS[22];   // Y

    *ucp18++ = ucpS[24];   // V
    *ucp19++ = ucpS[25];   // Y
    *ucp20++ = ucpS[26];   // U

    *ucp21++ = ucpS[28];   // Y
    *ucp22++ = ucpS[29];   // V
    *ucp23++ = ucpS[30];   // Y
   }


}



void CAspectRatio::GenerateSinc ()
{
  float fFactor, fArg;
  fFactor = fZoomFactor;
  if (fZoomFactor > 1.)
   {
    // magnification requires sinc with paramter 1
    fFactor = 1.;
   }

  int iNFilter = iFilterWidth/2;
  for (int iFilter = -iNFilter; iFilter <= iNFilter; iFilter++)
   {
    fArg = 3.1415926 * (float)iFilter / (float)iNSubPixel;
    if (fArg == 0.)
     {
      fpSinc[iFilter] = fFactor;
     }
    else
     {
      fpSinc[iFilter] = sin (fFactor * fArg) / fArg;
     }
   }
}

void CAspectRatio::GenerateWindow ()
{
  float fArg;

  int iNFilter = iFilterWidth/2;
  if (iNFilter == 0)
   {
    fpWindow[0] = 1.;
   }
  else
   {
    for (int iFilter = -iNFilter; iFilter <= iNFilter; iFilter++)
     {
      // fArg ranges from 0 to pi to 0
      if (iFilter < 0)
       {
        fArg = 3.1415926 * (float)(iFilter+iNFilter) / (float)iNFilter;
       }
      else
       {
        fArg = 3.1415926 * (float)(iFilter-iNFilter) / (float)iNFilter;
       }

      fpWindow[iFilter] = .54 - .46 * cos (fArg);
     }
   }
}

void CAspectRatio::GenerateFilter ()
{
  float fArg;

  int iNFilter = iFilterWidth/2;
  for (int iFilter = -iNFilter; iFilter <= iNFilter; iFilter++)
   {
    fpFilter[iFilter] = fpSinc[iFilter] * fpWindow[iFilter];
   }
}

void CAspectRatio::GenerateLUT ()
{
  float fArg;
  int iVal, iFilter;
  float fScale;

  if (iPixelPackingSrc == IF_PIXEL_PACKING_8Bits_IN_1Byte)
   {
    iVal = 256;
    fScale = 1.;
   }
  else if (
	iPixelPackingSrc == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS ||
	iPixelPackingSrc == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVSa
          )
   {
    iVal = 1024;
    fScale = 1.;
   }
  else
   {
    iVal = 0;
    fScale = 0.;
   }

  // we will be using integer look up tables for the weights.
  // use PRECISION to control the accuracy of the integer values

  float fPrecision = (float) (1 << PRECISION);
  iClampMin = 0;
  iClampMax = (255 << PRECISION);
  iHalfPrecision = (1 << (PRECISION-1));


  // the subpixel designation means that the sinc function gets recentered
  // at the subpixel location.  The weights of the other pixels must be
  // adjusted.  A sub-pixel of 0.1 means that the weight at pixel X must be
  // what is ordinarily the weight at (X-1).9

  int iNFilter = iFilterWidth/2;
  for (int iSubPixel = 0; iSubPixel < iNSubPixel; iSubPixel++)
   {
    // find the sum of all the weights, so we can normalize
    float fTotal = 0.;
    for (int iNeighbor = -iNNeighbor; iNeighbor <= iNNeighbor; iNeighbor++)
     {
      iFilter = iNeighbor * iNSubPixel - iSubPixel;
      if (iFilter >= -iNFilter && iFilter <= iNFilter)
        fTotal += fpFilter[iFilter];
     }

    // assign normalized weights to the LUT
    float fLUTWei;
    int iLUTWei;
    for (int iNeighbor = -iNNeighbor; iNeighbor <= iNNeighbor; iNeighbor++)
     {
      int iN = iNeighbor + iNNeighbor;
      iFilter = iNeighbor * iNSubPixel - iSubPixel;
      if (iFilter >= -iNFilter && iFilter <= iNFilter)
       {
        for (int iV = 0; iV < iVal; iV++)
         {
          // multiply by PRESISION to increase the floating point resolution
          fLUTWei = (fpFilter[iFilter]/fTotal) * (float)iV * fScale *
		fPrecision;
          if (fLUTWei < 0)
            iLUTWei = fLUTWei - .5;
          else
            iLUTWei = fLUTWei + .5;

          ipLUTWei[iSubPixel][iN][iV] = iLUTWei;
         }
       }
      else
       {
        for (int iV = 0; iV < iVal; iV++)
         {
          ipLUTWei[iSubPixel][iN][iV] = 0;
         }
       }
     }
   }

}
