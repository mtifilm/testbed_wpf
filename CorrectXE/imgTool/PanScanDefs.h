
/*
	File:		panscandefs.h
*/

#ifndef PANSCANDEFS_H
#define PANSCANDEFS_H

#define PST_BOX_OVERLAY_ON 1
#define PST_BOX_OVERLAY_OFF 2
#define PST_SAFE_ACTION_ON 3
#define PST_SAFE_ACTION_OFF 4
#define PST_SAFE_TITLE_ON 5
#define PST_SAFE_TITLE_OFF 6
#define PST_NO_DYNAMICS_ON 7
#define PST_NO_DYNAMICS_OFF 8

#define PSD_NONE 0
#define PSD_CURVE_0 1	// this is a static move
#define PSD_CURVE_1 2	// this is an s-curve
#define PSD_CURVE_2 3	// this is a linear
#define PSD_CURVE_3 4
#define PSD_CURVE_4 5
#define PSD_TRIM_ON 20 // turn on Trim Mode
#define PSD_TRIM_OFF 21 // turn off Trim Mode
#define PSD_SET_PRESET_1 22
#define PSD_SET_PRESET_2 23
#define PSD_SET_PRESET_3 24
#define PSD_SET_PRESET_4 25
#define PSD_SET_PRESET_5 26
#define PSD_GET_PRESET_0 27	// used to turn trim mode on
#define PSD_GET_PRESET_1 28
#define PSD_GET_PRESET_2 29
#define PSD_GET_PRESET_3 30
#define PSD_GET_PRESET_4 31
#define PSD_GET_PRESET_5 32
#define PSD_LOCK_ZOOM_ON 40
#define PSD_LOCK_ZOOM_OFF 41
#define PSD_REMOVE 255

#endif

