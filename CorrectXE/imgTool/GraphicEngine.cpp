#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <iostream>
#include "LineEngine.h"
#include "GraphicEngine.h"

///////////////////////////////////////////////////////////////////////////////

CGraphicEngine::
CGraphicEngine(CLineEngine *linEng)
{
   // register the line-draw engine ptr
   l = linEng;

   // set default world window
   worldWinL = -MAXWORLDX;
   worldWinB = -MAXWORLDY;
   worldWinR =  MAXWORLDX;
   worldWinT =  MAXWORLDY;

   // set default screen window
   screenWinL = 0;
   screenWinB = 0;
   screenWinR = 512;
   screenWinT = 512;

   // set the scale factors
   setScaleFactors();
}

CGraphicEngine::
~CGraphicEngine()
{
}

///////////////////////////////////////////////////////////////////////////////

void CGraphicEngine::
setScaleFactors()
{
   xScaleFactor = (double)(screenWinR-screenWinL)/(double)(worldWinR-worldWinL);
   yScaleFactor = (double)(screenWinT-screenWinB)/(double)(worldWinT-worldWinB);}

int CGraphicEngine::
scaleX(double x)
{
   return(screenWinL + (int)((double)(x-worldWinL)*xScaleFactor));
}

int CGraphicEngine::
scaleY(double y)
{
   return(screenWinB + (int)((double)(y-worldWinB)*yScaleFactor));
}

///////////////////////////////////////////////////////////////////////////////

unsigned char CGraphicEngine::
tCode()
{
   unsigned char tcode = 0;

   if (VCurX<=worldWinL) tcode |= TLFT;
   if (VCurY<=worldWinB) tcode |= TBOT;
   if (VCurX>=worldWinR) tcode |= TRGT;
   if (VCurY>=worldWinT) tcode |= TTOP;

   return(tcode);
}

///////////////////////////////////////////////////////////////////////////////

void CGraphicEngine::
setWorldWindow(
                int l,
                int b,
                int r,
                int t
              )
{
   // register the coords
   worldWinL = l;
   worldWinB = b;
   worldWinR = r;
   worldWinT = t;

   // set the scale factors
   setScaleFactors();
}

void CGraphicEngine::
setScreenWindow(
                  int l,
                  int b,
                  int r,
                  int t
                )
{
   // register the coords
   screenWinL = l;
   screenWinB = b;
   screenWinR = r-1;
   screenWinT = t-1;

   // set the scale factors
   setScaleFactors();
}

///////////////////////////////////////////////////////////////////////////////

void CGraphicEngine::
clipAndScale(
               VERTEX *vtxarry,
               int vtxcnt
             )
{
   unsigned char tcur,tpre;

   double XCrs, YCrs;

   double delxi, delyi;
   double lftxi, rgtxi;
   double botyi, topyi;

   ///////////////////////////////////////

   --vtxcnt;
   VCurX = vtxarry->x;
   VCurY = vtxarry->y;
   vtxarry++;
   if((tcur=tCode())==0) {
      CpX = scaleX(VCurX);
      CpY = scaleY(VCurY);
      l->moveTo(CpX,CpY);
   }

   while (--vtxcnt>=0) {

      VPreX = VCurX;
      VPreY = VCurY;
      tpre  = tcur;

      VCurX = vtxarry->x;
      VCurY = vtxarry->y;
      tcur = tCode();

      if ((tcur&tpre)==0) {

         if (VCurY==VPreY) { // horz

            if (VCurX>VPreX) { // OR0

               if (tpre&TLFT) {
		  CpY = scaleY(VCurY);
                  l->moveTo(screenWinL,CpY);
               }
               if (tcur==0) { // VCur in middle
                  CpX = scaleX(VCurX);
                  l->lineTo(CpX,CpY);
               }
               else { // TRGT is set
                  l->lineTo(screenWinR,CpY);
                  l->drawDot();
               }

            }
            else if (VCurX<VPreX) { // OR8

               if (tpre&TRGT) {

                  CpY = scaleY(VCurY);
                  l->moveTo(screenWinR,CpY);
               }
               if (tcur==0) { // VCur in middle 
                  CpX = scaleX(VCurX);
                  l->lineTo(CpX,CpY);
               }
               else { // TLFT is set
                  l->lineTo(screenWinL,CpY);
                  l->drawDot();
               }

            }

         } // horz
         else if (VCurX==VPreX) { // vert

            if (VCurY>VPreY) { // OR12

               if (tpre&TBOT) {
                  CpX = scaleX(VCurX);
                  l->moveTo(CpX,screenWinB);

               }
               if (tcur==0) { // VCur in middle
                  CpY = scaleY(VCurY);
                  l->lineTo(CpX,CpY);

               }
               else { // TTOP is set
                  l->lineTo(CpX,screenWinT);
                  l->drawDot();
               }

            }
            else if (VCurY<VPreY) { // OR4

               if (tpre&TTOP) {
                  CpX = scaleX(VCurX);
                  l->moveTo(CpX,screenWinT);
               }
               if (tcur==0) { // VCur in middle
                  CpY = scaleY(VCurY);
                  l->lineTo(CpX,CpY);
               }
               else { // TBOT is set
                  l->lineTo(CpX,screenWinB);
                  l->drawDot();
               }

            }

         } // vert
         else { // oblique

            if ((tpre|tcur)!=0) {

               delxi = (double)(VCurX - VPreX);
               delyi = (double)(VCurY - VPreY);
               lftxi = (double)(worldWinL - VPreX);
               rgtxi = (double)(worldWinR - VPreX);
               botyi = (double)(worldWinB - VPreY);
               topyi = (double)(worldWinT - VPreY);

            }

            if (tpre!=0) { // pre is clipped

               if (tpre&TLFT) { // clip pre to lft edge
                  YCrs = VPreY + delyi*lftxi/delxi;
                  if (YCrs < (double)worldWinB) {
                     tpre |= TBOT;
                     if (tpre&tcur) goto doncur;
                  }
                  else if (YCrs > (double)worldWinT) {
                     tpre |= TTOP;
                     if (tpre&tcur) goto doncur;
                  }
                  else {
                     CpY = scaleY(YCrs);
                     l->moveTo(screenWinL,CpY);
                     goto donpre;
                  }
               }

               if (tpre&TBOT) { // clip pre to bot edge
                  XCrs = VPreX + delxi*botyi/delyi;
                  if (XCrs < (double)worldWinL) {
                     tpre |= TLFT;
                     if (tpre&tcur) goto doncur;
                  }
                  else if (XCrs > (double)worldWinR) {
                     tpre |= TRGT;
                     if (tpre&tcur) goto doncur;
                  }
                  else {
                     CpX = scaleX(XCrs);
                     l->moveTo(CpX,screenWinB);
                     goto donpre;
                  }
               }

               if (tpre&TRGT) { // clip pre to rgt edge
                  YCrs = VPreY + delyi*rgtxi/delxi;
                  if (YCrs < (double)worldWinB) {
                     tpre |= TBOT;
                     if (tpre&tcur) goto doncur;
                  }
                  else if (YCrs > (double)worldWinT) {
                     tpre |= TTOP;
                     if (tpre&tcur) goto doncur;
                  }
                  else {
                     CpY = scaleY(YCrs);
                     l->moveTo(screenWinR,CpY);
                     goto donpre;
                  }
               }

               if (tpre&TTOP) { // clip pre to top edge
                  XCrs = VPreX + delxi*topyi/delyi;
                  if (XCrs < (double)worldWinL) {
                     tpre |= TLFT;
                     if (tpre&tcur) goto doncur;
                  }
                  else if (XCrs > (double)worldWinR) {
                     tpre |= TRGT;
                     if (tpre&tcur) goto doncur;
                  }
                  else {
                     CpX = scaleX(XCrs);
                     l->moveTo(CpX,screenWinT);
                     goto donpre;
                  } 
               }
donpre:;
            }

            if (tcur==0) { // cur is not clipped
               CpX = scaleX(VCurX);
               CpY = scaleY(VCurY);
               l->lineTo(CpX,CpY);
            }
            else {

               if (tcur&TLFT) { // clip cur to lft edge
                  YCrs = VPreY + delyi*lftxi/delxi;
                  if ((YCrs>=worldWinB)&&(YCrs<=worldWinT)) {
                     CpY = scaleY(YCrs);
                     l->lineTo(screenWinL,CpY);
                     l->drawDot();
                     goto doncur;
                  }
               }

               if (tcur&TBOT) { // clip cur to bot edge
                  XCrs = VPreX + delxi*botyi/delyi;
                  if ((XCrs>=worldWinL)&&(XCrs<=worldWinR)) {
                     CpX = scaleX(XCrs);
                     l->lineTo(CpX,screenWinB);
                     l->drawDot();
                     goto doncur;
                  }
               }

               if (tcur&TRGT) { // clip cur to rgt edge
                  YCrs = VPreY + delyi*rgtxi/delxi;
                  if ((YCrs>=worldWinB)&&(YCrs<=worldWinT)) {
                     CpY = scaleY(YCrs);
                     l->lineTo(screenWinR,CpY);
                     l->drawDot();
                     goto doncur;
                  }
               }

               if (tcur&TTOP) { // clip cur to top edge
                  XCrs = VPreX + delxi*topyi/delyi;
                  if ((XCrs>=worldWinL)&&(XCrs<=worldWinR)) {
                     CpX = scaleX(XCrs);
                     l->lineTo(CpX,screenWinT);
                     l->drawDot();
                     goto doncur;
                  }
               }
doncur:;
            }

         } // oblique

      } // not rejected 

      vtxarry++;

   }

}

