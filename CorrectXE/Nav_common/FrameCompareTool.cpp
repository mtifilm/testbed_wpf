//---------------------------------------------------------------------------
// FrameCompareTool.cpp: implementation of the FrameCompareTool class.
//---------------------------------------------------------------------------

#include "FrameCompareTool.h"

#include "Displayer.h"
#include "MainWindowUnit.h"
#include "Player.h"
#include "ToolManager.h"

#pragma hdrstop
//---------------------------------------------------------------------------

#pragma package(smart_init)
//---------------------------------------------------------------------------

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

static MTI_UINT16 FrameCompareToolNumber = FRAME_COMPARE_TOOL_NUMBER;


// -------------------------------------------------------------------------
// FrameCompare Default Keyboard and Mouse Button Configuration

static CUserInputConfiguration::SConfigItem FrameCompareDefaultConfigItems[] =
{
   // FrameCompare Command                          Modifiers      Action
   //                                       + Key
   // ----------------------------------------------------------------

 { FCMP_CMD_START_MOVING_DIVIDER,            " LButton           ButtonDown " },
 { FCMP_CMD_STOP_MOVING_DIVIDER,             " LButton           ButtonUp   " },
 { FCMP_CMD_START_CHANGING_DIVIDER_ANGLE,    " Alt+LButton       ButtonDown " },
 { FCMP_CMD_STOP_CHANGING_DIVIDER_ANGLE,     " Alt+LButton       ButtonUp   " },
 { FCMP_CMD_TOGGLE_WIPE_MODE,                " Backspace         KeyDown " },
 { FCMP_CMD_TOGGLE_SIDE_BY_SIDE_MODE,        " Shift+Backspace   KeyDown " },
 { FCMP_CMD_EXIT_EITHER_MODE,                " Esc               KeyDown " },

// { FCMP_CMD_INCREASE_DIVIDER_ANGLE,          " Up            KeyDown " },
// { FCMP_CMD_INCREASE_DIVIDER_ANGLE,          " Left          KeyDown " },
// { FCMP_CMD_INCREASE_DIVIDER_ANGLE_SMALL,    " Ctrl+Up       KeyDown " },
// { FCMP_CMD_INCREASE_DIVIDER_ANGLE_SMALL,    " Ctrl+Left     KeyDown " },
// { FCMP_CMD_INCREASE_DIVIDER_ANGLE_LARGE,    " Shift+Up      KeyDown " },
// { FCMP_CMD_INCREASE_DIVIDER_ANGLE_LARGE,    " Shift+Left    KeyDown " },
//
// { FCMP_CMD_DECREASE_DIVIDER_ANGLE,          " Down          KeyDown " },
// { FCMP_CMD_DECREASE_DIVIDER_ANGLE,          " Right         KeyDown " },
// { FCMP_CMD_DECREASE_DIVIDER_ANGLE_SMALL,    " Ctrl+Down     KeyDown " },
// { FCMP_CMD_DECREASE_DIVIDER_ANGLE_SMALL,    " Ctrl+Right    KeyDown " },
// { FCMP_CMD_DECREASE_DIVIDER_ANGLE_LARGE,    " Shift+Down    KeyDown " },
// { FCMP_CMD_DECREASE_DIVIDER_ANGLE_LARGE,    " Shift+Right   KeyDown " },

 { FCMP_CMD_MOVE_DIVIDER_UP_LEFT,            " Ctrl+Shift+Up     KeyDown " },
 { FCMP_CMD_MOVE_DIVIDER_UP_LEFT,            " Ctrl+Shift+Left   KeyDown " },
// { FCMP_CMD_MOVE_DIVIDER_UP_LEFT_SMALL,      " Ctrl+Up           KeyDown " },
// { FCMP_CMD_MOVE_DIVIDER_UP_LEFT_SMALL,      " Ctrl+Left         KeyDown " },
// { FCMP_CMD_MOVE_DIVIDER_UP_LEFT_LARGE,      " Shift+Up          KeyDown " },
// { FCMP_CMD_MOVE_DIVIDER_UP_LEFT_LARGE,      " Shift+Left        KeyDown " },
 { FCMP_CMD_MOVE_DIVIDER_UP_LEFT_LARGE,      " PageUp            KeyDown " },
 { FCMP_CMD_MOVE_DIVIDER_UP_LEFT_LARGE,      " Ctrl+Shift+PageUp KeyDown " },

 { FCMP_CMD_MOVE_DIVIDER_DOWN_RIGHT,         " Ctrl+Shift+Down   KeyDown " },
 { FCMP_CMD_MOVE_DIVIDER_DOWN_RIGHT,         " Ctrl+Shift+Right  KeyDown " },
// { FCMP_CMD_MOVE_DIVIDER_DOWN_RIGHT_SMALL,   " Ctrl+Down           KeyDown " },
// { FCMP_CMD_MOVE_DIVIDER_DOWN_RIGHT_SMALL,   " Ctrl+Right          KeyDown " },
// { FCMP_CMD_MOVE_DIVIDER_DOWN_RIGHT_LARGE,   " Shift+Down          KeyDown " },
// { FCMP_CMD_MOVE_DIVIDER_DOWN_RIGHT_LARGE,   " Shift+Right         KeyDown " },
 { FCMP_CMD_MOVE_DIVIDER_DOWN_RIGHT_LARGE,   " PageDown            KeyDown " },
 { FCMP_CMD_MOVE_DIVIDER_DOWN_RIGHT_LARGE,   " Ctrl+Shift+PageDown KeyDown " },

 { FCMP_CMD_TOGGLE_DIVIDER_VERT_HORIZ,       " LButton		     DoubleClick "},
 { FCMP_CMD_TOGGLE_DIVIDER_VERT_HORIZ,       " Alt+LButton		  DoubleClick "},
 { FCMP_CMD_TOGGLE_DIVIDER_VERT_HORIZ,       " Ctrl+LButton		  DoubleClick "},
 { FCMP_CMD_TOGGLE_DIVIDER_VERT_HORIZ,       " Shift+LButton	  DoubleClick "},
 { FCMP_CMD_TOGGLE_DIVIDER_VERT_HORIZ,       " Ctrl+Alt+LButton  DoubleClick "},
 { FCMP_CMD_TOGGLE_DIVIDER_VERT_HORIZ,       " Shift+Alt+LButton DoubleClick "},

 { FCMP_CMD_TOGGLE_LABELS_ON_OFF,            " Ctrl+Backspace    KeyDown "},
};


static CUserInputConfiguration *FrameCompareDefaultUserInputConfiguration
= new CUserInputConfiguration(sizeof(FrameCompareDefaultConfigItems)
										/sizeof(CUserInputConfiguration::SConfigItem),
										 FrameCompareDefaultConfigItems);

// -------------------------------------------------------------------------
// FrameCompare Command Table

static CToolCommandTable::STableEntry FrameCompareCommandTableEntries[] =
{
   // FrameCompare Command                         FrameCompare Command String
   // -------------------------------------------------------------

 { FCMP_CMD_START_MOVING_DIVIDER,            "FCMP_CMD_START_MOVING_DIVIDER" },
 { FCMP_CMD_STOP_MOVING_DIVIDER,             "FCMP_CMD_STOP_MOVING_DIVIDER" },
 { FCMP_CMD_START_CHANGING_DIVIDER_ANGLE,    "FCMP_CMD_START_CHANGING_DIVIDER_ANGLE" },
 { FCMP_CMD_STOP_CHANGING_DIVIDER_ANGLE,     "FCMP_CMD_STOP_CHANGING_DIVIDER_ANGLE" },
 { FCMP_CMD_TOGGLE_WIPE_MODE,                "FCMP_CMD_TOGGLE_WIPE_MODE" },
 { FCMP_CMD_INCREASE_DIVIDER_ANGLE,          "FCMP_CMD_INCREASE_DIVIDER_ANGLE" },
 { FCMP_CMD_INCREASE_DIVIDER_ANGLE_SMALL,    "FCMP_CMD_INCREASE_DIVIDER_ANGLE_SMALL" },
 { FCMP_CMD_INCREASE_DIVIDER_ANGLE_LARGE,    "FCMP_CMD_INCREASE_DIVIDER_ANGLE_LARGE" },
 { FCMP_CMD_DECREASE_DIVIDER_ANGLE,          "FCMP_CMD_DECREASE_DIVIDER_ANGLE" },
 { FCMP_CMD_DECREASE_DIVIDER_ANGLE_SMALL,    "FCMP_CMD_DECREASE_DIVIDER_ANGLE_SMALL" },
 { FCMP_CMD_DECREASE_DIVIDER_ANGLE_LARGE,    "FCMP_CMD_DECREASE_DIVIDER_ANGLE_LARGE" },
 { FCMP_CMD_MOVE_DIVIDER_UP_LEFT,            "FCMP_CMD_MOVE_DIVIDER_UP_LEFT" },
 { FCMP_CMD_MOVE_DIVIDER_UP_LEFT_SMALL,      "FCMP_CMD_MOVE_DIVIDER_UP_LEFT_SMALL" },
 { FCMP_CMD_MOVE_DIVIDER_UP_LEFT_LARGE,      "FCMP_CMD_MOVE_DIVIDER_UP_LEFT_LARGE" },
 { FCMP_CMD_MOVE_DIVIDER_DOWN_RIGHT,         "FCMP_CMD_MOVE_DIVIDER_DOWN_RIGHT" },
 { FCMP_CMD_MOVE_DIVIDER_DOWN_RIGHT_SMALL,   "FCMP_CMD_MOVE_DIVIDER_DOWN_RIGHT_SMALL" },
 { FCMP_CMD_MOVE_DIVIDER_DOWN_RIGHT_LARGE,   "FCMP_CMD_MOVE_DIVIDER_DOWN_RIGHT_LARGE" },
 { FCMP_CMD_TOGGLE_SIDE_BY_SIDE_MODE,        "FCMP_CMD_TOGGLE_SIDE_BY_SIDE_MODE" },
 { FCMP_CMD_TOGGLE_LABELS_ON_OFF,            "FCMP_CMD_TOGGLE_LABELS_ON_OFF"},
 { FCMP_CMD_EXIT_EITHER_MODE,                "FCMP_CMD_EXIT_EITHER_MODE" },
};

static CToolCommandTable *FrameCompareCommandTable
	= new CToolCommandTable(sizeof(FrameCompareCommandTableEntries)
                           /sizeof(CToolCommandTable::STableEntry),
									FrameCompareCommandTableEntries);


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

FrameCompareTool::FrameCompareTool(const string &newToolName)
:  CToolObject(newToolName, NULL)
{
}

FrameCompareTool::~FrameCompareTool()
{
}

//////////////////////////////////////////////////////////////////////
// Built-in tools require a factory method
//////////////////////////////////////////////////////////////////////

CToolObject* FrameCompareTool::MakeTool(const string& newToolName)
{
	return new FrameCompareTool(newToolName);
}


// ===================================================================
//
// Function:    toolInitialize
//
// Description: Called when the ToolManager wants to initialize
//              the FrameCompare Tool
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================

int FrameCompareTool::toolInitialize(CToolSystemInterface *newSystemAPI)
{
   int retVal;

   // Initialize the FrameCompareTool's base class, i.e., CToolObject
   // This must be done before the FrameCompareTool initializes itself
   retVal = initBaseToolObject(FrameCompareToolNumber, newSystemAPI,
                               FrameCompareCommandTable,
                               FrameCompareDefaultUserInputConfiguration);
   if (retVal != 0)
   {
      // ERROR: initBaseToolObject failed
      TRACE_0(errout << "Error in FrameCompareTool, initBaseToolObject failed "
                     << retVal);
      return retVal;
   }

   return retVal;
}


// ===================================================================
//
// Function:    toolShutdown
//
// Description: Called when the Tool Manager wants to shut down the
//              FrameCompare Tool, typically when the main program is
//              shutting down.
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================

int FrameCompareTool::toolShutdown()
{
   int retVal;

   // Shutdown the FrameCompare Tool's base class, i.e., CToolObject.
   // This must be done after the FrameCompare Tool shuts down itself
   retVal = shutdownBaseToolObject();
   if (retVal != 0)
      {
      // ERROR: shutdownBaseToolObject failed
      return retVal;
      }

   return retVal;
}
//--------------------------------------------------------------------------

int FrameCompareTool::toolActivate()
{
   return 0;        // Built-in tool, always active
}
//--------------------------------------------------------------------------

int FrameCompareTool::toolDeactivate()
{
   return 0;        // Built-in tool, always active
}
//--------------------------------------------------------------------------

CToolProcessor* FrameCompareTool::makeToolProcessorInstance(const bool *newEmergencyStopFlagPtr)
{
	// This tool doesn't have a CToolProcessor
	return nullptr;
}
//--------------------------------------------------------------------------

int FrameCompareTool::onRedraw(int frameIndex)
{
	return TOOL_RETURN_CODE_NO_ACTION;   // Let other tools draw on the frame
}
//--------------------------------------------------------------------------

int FrameCompareTool::onChangingClip()
{
	return TOOL_RETURN_CODE_NO_ACTION;
}
//--------------------------------------------------------------------------

int FrameCompareTool::onChangingFraming()
{
	return TOOL_RETURN_CODE_NO_ACTION;
}
//--------------------------------------------------------------------------

int FrameCompareTool::onHeartbeat()
{
	// When in wipe/preview mode, we want to pop out of wipe when preview is complete.
	CToolManager toolMgr;
	if (!toolMgr.IsInViewOnlyMode() && mainWindow->GetPlayer()->getFrameCompareMode() != CPlayer::FrameCompareMode::Off)
	{
		CToolManager toolManager;
		bool previewing = getSystemAPI()->IsProvisionalPending()
									|| getSystemAPI()->arePaintToolChangesPending()
									|| toolManager.IsToolProcessing();
		if (!previewing && mainWindow->GetPlayer()->getFrameCompareMode() != CPlayer::FrameCompareMode::Off)
		{
			exitFromFrameCompareMode();
		}
	}

	return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onToolCommand
//
// Description: Frame Compare Tool's Command Handler
//
// Arguments:   CToolCommand &toolCommand
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================

int FrameCompareTool::onToolCommand(CToolCommand &toolCommand)
{
   int commandNumber = toolCommand.getCommandNumber();
	int result = TOOL_RETURN_CODE_NO_ACTION;

	const int LargeAngle = 90;
	const int NormalAngle = 15;
	const int SmallAngle = 5;
	const double LargeMove = 1.0;
	const double NormalMove = 1.0 / 10.0;  //1.0 / 6.0;
	const double SmallMove = 1.0 / 16.0;

	int smallNormalOrLarge = 0;  // Normal
	switch (commandNumber)
	{
		case FCMP_CMD_INCREASE_DIVIDER_ANGLE:
		case FCMP_CMD_DECREASE_DIVIDER_ANGLE:
		case FCMP_CMD_INCREASE_DIVIDER_ANGLE_SMALL:
		case FCMP_CMD_DECREASE_DIVIDER_ANGLE_SMALL:
		case FCMP_CMD_INCREASE_DIVIDER_ANGLE_LARGE:
		case FCMP_CMD_DECREASE_DIVIDER_ANGLE_LARGE:
			if (!_altKeyIsDown)
			{
				return TOOL_RETURN_CODE_NO_ACTION;
			}

         break;

		case FCMP_CMD_MOVE_DIVIDER_UP_LEFT_SMALL:
		case FCMP_CMD_MOVE_DIVIDER_DOWN_RIGHT_SMALL:
			smallNormalOrLarge = -1; // Small

			// Hack because Alt+ScrollWheel removes the Alt!!
			if (_altKeyIsDown)
			{
				commandNumber = (commandNumber == FCMP_CMD_MOVE_DIVIDER_UP_LEFT_SMALL)
										? FCMP_CMD_DECREASE_DIVIDER_ANGLE_SMALL
										: FCMP_CMD_INCREASE_DIVIDER_ANGLE_SMALL;
			}

			break;

		case FCMP_CMD_MOVE_DIVIDER_UP_LEFT_LARGE:
		case FCMP_CMD_MOVE_DIVIDER_DOWN_RIGHT_LARGE:
			smallNormalOrLarge = 1; // Large

			// Hack because Alt+ScrollWheel removes the Alt!!
			if (_altKeyIsDown)
			{
				commandNumber = (commandNumber == FCMP_CMD_MOVE_DIVIDER_UP_LEFT_LARGE)
										? FCMP_CMD_DECREASE_DIVIDER_ANGLE_LARGE
										: FCMP_CMD_INCREASE_DIVIDER_ANGLE_LARGE;
			}

			break;

		case FCMP_CMD_MOVE_DIVIDER_UP_LEFT:
		case FCMP_CMD_MOVE_DIVIDER_DOWN_RIGHT:

			// Hack because Alt+ScrollWheel removes the Alt!!
			if (_altKeyIsDown)
			{
				commandNumber = (commandNumber == FCMP_CMD_MOVE_DIVIDER_UP_LEFT)
										? FCMP_CMD_DECREASE_DIVIDER_ANGLE
										: FCMP_CMD_INCREASE_DIVIDER_ANGLE;
			}

			break;
	}

	switch (commandNumber)
	{
		case FCMP_CMD_START_MOVING_DIVIDER:
			result = startMovingDivider();
			break;

		case FCMP_CMD_STOP_MOVING_DIVIDER:
			result = stopMovingDivider();
			break;

		case FCMP_CMD_START_CHANGING_DIVIDER_ANGLE:
			break;

		case FCMP_CMD_STOP_CHANGING_DIVIDER_ANGLE:
			break;

		case FCMP_CMD_INCREASE_DIVIDER_ANGLE:
		case FCMP_CMD_INCREASE_DIVIDER_ANGLE_SMALL:
		case FCMP_CMD_INCREASE_DIVIDER_ANGLE_LARGE:
			result = changeDividerAngle((smallNormalOrLarge == -1)
													? SmallAngle
													: ((smallNormalOrLarge == 1)
														  ? LargeAngle
														  : NormalAngle));
			break;

		case FCMP_CMD_DECREASE_DIVIDER_ANGLE:
		case FCMP_CMD_DECREASE_DIVIDER_ANGLE_SMALL:
		case FCMP_CMD_DECREASE_DIVIDER_ANGLE_LARGE:
			result = changeDividerAngle((smallNormalOrLarge == -1)
													? -SmallAngle
													: ((smallNormalOrLarge == 1)
														  ? -LargeAngle
														  : -NormalAngle));
			break;

		case FCMP_CMD_MOVE_DIVIDER_UP_LEFT:
		case FCMP_CMD_MOVE_DIVIDER_UP_LEFT_SMALL:
		case FCMP_CMD_MOVE_DIVIDER_UP_LEFT_LARGE:
			result = moveDivider((smallNormalOrLarge == -1)
													? -SmallMove
													: ((smallNormalOrLarge == 1)
														  ? -LargeMove
														  : -NormalMove));
			break;

		case FCMP_CMD_MOVE_DIVIDER_DOWN_RIGHT:
		case FCMP_CMD_MOVE_DIVIDER_DOWN_RIGHT_SMALL:
		case FCMP_CMD_MOVE_DIVIDER_DOWN_RIGHT_LARGE:
			result = moveDivider((smallNormalOrLarge == -1)
													? SmallMove
													: ((smallNormalOrLarge == 1)
														  ? LargeMove
														  : NormalMove));
			break;

		case FCMP_CMD_TOGGLE_WIPE_MODE:
		{
			mainWindow->ToggleFrameWipeCompareMode();
			result = TOOL_RETURN_CODE_EVENT_CONSUMED;
			break;
		}

		case FCMP_CMD_TOGGLE_SIDE_BY_SIDE_MODE:
		{
			mainWindow->ToggleDualFrameCompareMode();
			result = TOOL_RETURN_CODE_EVENT_CONSUMED;
			break;
		}

		case FCMP_CMD_EXIT_EITHER_MODE:
			if (mainWindow->GetPlayer()->getFrameCompareMode() != CPlayer::FrameCompareMode::Off)
			{
				exitFromFrameCompareMode();
				result = TOOL_RETURN_CODE_EVENT_CONSUMED;
			}

			break;

		case FCMP_CMD_TOGGLE_DIVIDER_VERT_HORIZ:
			mainWindow->GetPlayer()->toggleFrameCompareDividerVertHoriz();
			break;

		case FCMP_CMD_TOGGLE_LABELS_ON_OFF:
         mainWindow->ToggleShowFrameCompareClipNames();
			break;
	}

	if ((result & TOOL_RETURN_CODE_EVENT_CONSUMED) == TOOL_RETURN_CODE_EVENT_CONSUMED)
	{
		const CToolCommandTable *commandTable = getCommandTable();
		const char* commandName = commandTable->findCommandName(commandNumber);
		if (commandName != NULL)
			TRACE_2(errout << "Command: " << commandName);
		else
			TRACE_2(errout << "FrameCompare swallowed command " << commandNumber);
	}

	return result;
}

// ===================================================================
//
// Function:    onUserInput
//
// Description: Navigator Tool's User Input Handler
//
// Arguments:   CUserInput &userInput
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================
int FrameCompareTool::onUserInput(CUserInput &userInput)
{
	// We need to remember the state of the ALT key!
	_altKeyIsDown = (userInput.shift & USER_INPUT_SHIFT_MASK_PHYS_ALT) != 0;

	return CToolObject::onUserInput(userInput);
}

// ===================================================================
//
// Function:    onMouseMove
//
// Description: Navigator Tool's Mouse Movement Handler
//
// Arguments:   CUserInput &userInput
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================
int FrameCompareTool::onMouseMove(CUserInput &userInput)
{
	int x = userInput.windowX;
	int y = userInput.windowY;
	if (x == _mouseXClient && y == _mouseYClient)
	{
		return TOOL_RETURN_CODE_NO_ACTION;
	}

	_mouseXClient = x;
	_mouseYClient = y;

	if (!_isWipingInProgress)
	{
		return TOOL_RETURN_CODE_NO_ACTION;
	}


	POINT refPoint = { x, y };
	mainWindow->GetPlayer()->setFrameCompareWiperOriginClient(refPoint);

	// Don't consume, so pan function keeps working!
	return TOOL_RETURN_CODE_NO_ACTION;
}
//---------------------------------------------------------------------------

int FrameCompareTool::startMovingDivider()
{
	if (mainWindow->GetPlayer()->getFrameCompareMode() != CPlayer::FrameCompareMode::Wipe)
	{
		return TOOL_RETURN_CODE_NO_ACTION;
	}

	POINT refPoint = { _mouseXClient, _mouseYClient };
	mainWindow->GetPlayer()->setFrameCompareWiperOriginClient(refPoint);

	_isWipingInProgress = true;

	return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//---------------------------------------------------------------------------

int FrameCompareTool::stopMovingDivider()
{
	if (!_isWipingInProgress)
	{
		return TOOL_RETURN_CODE_NO_ACTION;
	}

	_isWipingInProgress = false;

	if (mainWindow->GetPlayer()->getFrameCompareMode() != CPlayer::FrameCompareMode::Wipe)
	{
		return TOOL_RETURN_CODE_NO_ACTION;
	}

	POINT refPoint = { _mouseXClient, _mouseYClient };
	mainWindow->GetPlayer()->setFrameCompareWiperOriginClient(refPoint);

	return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//---------------------------------------------------------------------------

int FrameCompareTool::changeDividerAngle(int changeAmountInDegrees)
{
	if (mainWindow->GetPlayer()->getFrameCompareMode() != CPlayer::FrameCompareMode::Wipe)
	{
		return TOOL_RETURN_CODE_NO_ACTION;
	}

	mainWindow->GetPlayer()->changeFrameCompareWiperAngle(float(changeAmountInDegrees), true);

	return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//---------------------------------------------------------------------------

int FrameCompareTool::moveDivider(double fractionalOffset)
{
	if (mainWindow->GetPlayer()->getFrameCompareMode() != CPlayer::FrameCompareMode::Wipe)
	{
		return TOOL_RETURN_CODE_NO_ACTION;
	}

	mainWindow->GetPlayer()->changeFrameCompareWiperOriginRelative(float(fractionalOffset), true);

	return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//---------------------------------------------------------------------------

void FrameCompareTool::exitFromFrameCompareMode()
{
	if (mainWindow->GetPlayer()->getFrameCompareMode() == CPlayer::FrameCompareMode::SideBySide)
	{
		mainWindow->ToggleDualFrameCompareMode();
	}
	else
	{
		mainWindow->ToggleFrameWipeCompareMode();
	}
}
//---------------------------------------------------------------------------



