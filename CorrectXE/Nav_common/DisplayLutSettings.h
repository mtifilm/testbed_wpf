#ifndef __DISPLAY_LUT_SETTINGS_H__
#define __DISPLAY_LUT_SETTINGS_H__

#include "IniFile.h"
#include "ImageFormat3.h"

#define MIN_DISPLAY_GAMMA      0.1F
#define MAX_DISPLAY_GAMMA      3.0F
#define DEFAULT_DISPLAY_GAMMA  1.7F

enum ELutColorSpace
{
   LUTCS_INVALID    = -1,
   LUTCS_LINEAR     =  0,
   LUTCS_LOG        =  1,
   LUTCS_SMPTE_240  =  2,
   LUTCS_CCIR_709   =  3,
   LUTCS_CCIR_601M  =  4,
   LUTCS_CCIR_601BG =  5,
   LUTCS_EXR        =  6
};


class CDisplayLutSettings
{
public:

   CDisplayLutSettings();

   void InitToDefaults();
   void Conform();

   /* Mutators */

   void UseDefaultSourceColorSpace(bool flag);
   void UseDefaultSourceRange(bool flag);
   void UseDefaultSourceRangeExr(bool flag);
   void UseDefaultDisplayRange(bool flag);
   void UseDefaultDisplayGamma(bool flag);

   void SetSourceColorSpace(ELutColorSpace colorSpace);

   void SetSourceRange(const MTI_REAL32 *newMinValues,
                           const MTI_REAL32 *newMaxValues);
   void SetSourceRange(int bitsPerComponent,
                           const MTI_UINT16 *newMinValues,
                           const MTI_UINT16 *newMaxValues);

   void SetSourceRangeExr(const MTI_REAL32 *newMinValues,
                           const MTI_REAL32 *newMaxValues);
   void SetSourceRangeExr(const MTI_REAL32 newExposureAdjust,
                           const MTI_REAL32 newDynamicRange);

   void SetDisplayRange(const MTI_UINT16 *newMinValues,
                        const MTI_UINT16 *newMaxValues);

   void SetDisplayGamma(MTI_REAL32 newGamma);
   void SetDisplayGammaTimes10(int newGammaTimes10);

   /* Accessors */

   bool UsingDefaultSourceColorSpace() const;
   bool UsingDefaultSourceRange() const;
   bool UsingDefaultSourceRangeExr() const;
   bool UsingDefaultDisplayRange() const;
   bool UsingDefaultDisplayGamma() const;

   ELutColorSpace GetSourceColorSpace() const;

   const void GetSourceRange(MTI_REAL32 *minValues, MTI_REAL32 *maxValues);
   const void GetSourceRange(int bitsPerComponent, MTI_UINT16 *minValues,
                                             MTI_UINT16 *maxValues);

   const void GetSourceRangeExr(MTI_REAL32 *minValues, MTI_REAL32 *maxValues);
   const void GetSourceRangeExr(MTI_UINT16 minValues[], MTI_UINT16 maxValues[]);
   const void GetSourceRangeExr(MTI_REAL32 &exposureAdjust, MTI_REAL32 &dynamicRange);

   const void GetDisplayRange(MTI_UINT16 *minValues, MTI_UINT16 *maxValues);

   const MTI_REAL32 GetDisplayGamma();
   const MTI_UINT32 GetDisplayGammaTimes10();

   /* Reader / Writers */

   int ReadFromIniFile(const string &iniFileName,
                     const string &sectionNamePrefix);

   int SaveToIniFile(const string &iniFileName,
                     const string &sectionNamePrefix);

   int ReadFromIniFile(CIniFile *ini,
                     const string &sectionNamePrefix);

   int SaveToIniFile(CIniFile *ini,
                     const string &sectionNamePrefix);

   static void EraseIniSection(CIniFile *ini,
                     const string &sectionNamePrefix);

   void ImportSettingsFromWackyLutFormat(
                     const string &filePath,
                     const CImageFormat *currentClipImageFormat = NULL);

   void ExportSettingsInWackyLutFormat(
                     const string &filePath,
                     const CImageFormat *currentClipImageFormat);

   int ImportLegacyClipCustomLutSettings(
                     CIniFile *iniFile,
                     const string &sectionName,
                     const CImageFormat *fmt,
                     int defaultGammaTimes10);

   bool DoesDisplayLutSettingsSectionExist(
                     CIniFile *ini,
                     const string &sectionNamePrefix);

private:

   bool m_usingDefaultColorSpace;
   bool m_usingDefaultSourceRange;
   bool m_usingDefaultSourceRangeExr;
   bool m_usingDefaultDisplayRange;
   bool m_usingDefaultDisplayGamma;

   ELutColorSpace m_colorSpace;
   MTI_REAL32 m_sourceRangeMin[3];
   MTI_REAL32 m_sourceRangeMax[3];
   MTI_REAL32 m_sourceExrExposureAdjust;
   MTI_REAL32 m_sourceExrDynamicRange;
   MTI_UINT16 m_displayRangeMin[3];
   MTI_UINT16 m_displayRangeMax[3];
   MTI_REAL32 m_displayGamma;

   string translateColorSpaceToWackyFormat(
               EColorSpace defaultColorSpace);

   static string makeSectionName(const string &sectionNamePrefix);

   // Ini file keys
   static string displayLutSectionName;
   static string useDefaultColorSpaceKey;
   static string colorSpaceKey;
   static string useDefaultSourceRangeKey;
   static string sourceMinRYKey;
   static string sourceMaxRYKey;
   static string sourceMinGUKey;
   static string sourceMaxGUKey;
   static string sourceMinBVKey;
   static string sourceMaxBVKey;
   static string useDefaultSourceRangeExrKey;
   static string sourceExrExposureAdjustKey;
   static string sourceExrDynamicRangeKey;
   static string useDefaultDisplayRangeKey;
   static string displayMinRKey;
   static string displayMaxRKey;
   static string displayMinGKey;
   static string displayMaxGKey;
   static string displayMinBKey;
   static string displayMaxBKey;
   static string useDefaultDisplayGammaKey;
   static string displayGammaKey;

   static string legacyUseCustomLUTKey;
   static string legacyUseFormatDefaultLUTKey;
   static string legacyCustomLUTColorSpaceIndexKey;
   static string legacyCustomLUTSourceRangeKey;
   static string legacyCustomLUTRYBlkKey;
   static string legacyCustomLUTRYWhiKey;
   static string legacyCustomLUTGUBlkKey;
   static string legacyCustomLUTGUWhiKey;
   static string legacyCustomLUTBVBlkKey;
   static string legacyCustomLUTBVWhiKey;
   static string legacyCustomLUTRMinKey;
   static string legacyCustomLUTRMaxKey;
   static string legacyCustomLUTGMinKey;
   static string legacyCustomLUTGMaxKey;
   static string legacyCustomLUTBMinKey;
   static string legacyCustomLUTBMaxKey;
   static string legacyGammaKey;

};


#endif //  __DISPLAY_LUT_SETTINGS_H__
