// MaskTransformEditor.cpp
//
// (Based on TransformEditor.cpp)

#include <math.h>
#include "BezierEditor.h"
#include "MaskTransformEditor.h"
#include "Displayer.h"
#define PI 3.1415927

#define MINEDG 5.
#define USE_QUEUE

//------------------------------------------------------------------------------
// Bugzilla ref #2032 - KT-050112
// As currently implemented, the xform is refreshed by
// displayFrames' ToolManager call to onRedraw, which calls
// MaskTool's DrawMask() and gets the xform-in-progress from
// the regionList (which never contains more than 1 region).
// Because onRedraw is now called from inside displayFrame,
// the spline is drawn into the BACK GL buffer and made vis-
// ible by SwapBuffers. The process of drawing the spline
// is no longer perceptible. In addition, there appears to
// be a great speed advantage drawing to the BACK GL buffer,
// esp. in dual monitor systems.

CMaskTransformEditor::CMaskTransformEditor(CDisplayer * dsp)
{
   // save the address of the displayer
   displayer = dsp;

   // reset transform
   resetTransform();

   // calculate surrogate frame rectangle
   initTransformWidget();

   // initialize the modkeys
   modKeys = 0;

   majorState = MAJORSTATE_TRACK;
}

CMaskTransformEditor::~CMaskTransformEditor()
{
}

// The TransformEditor needs to know the dimensions of the
// image in the client area, in order to set up the Widget
//
void CMaskTransformEditor::setImageRectangle(RECT& rgbrct)
{
   rgbRect = rgbrct;
    
   calculateWidgetSize();

   calculateTransformWidget();
}

void CMaskTransformEditor::calculateWidgetSize()
{
   rgbWdth = (rgbRect.right - rgbRect.left + 1);
   rgbHght = (rgbRect.bottom - rgbRect.top + 1);
   rgbXctr = rgbRect.left + rgbWdth/2;
   rgbYctr = rgbRect.top  + rgbHght/2;

   wdgtSize = rgbWdth;
   if (rgbHght < rgbWdth)
      wdgtSize = rgbHght;
}

void CMaskTransformEditor::calculateTransformWidget()
{
   transformStretchedToRotated(rotWidget, strWidget, netRotationAngle);
}

void CMaskTransformEditor::resetTransform()
{
   DPOINT skwWidget[10];
   // the unit square centered on (0, 0)
   skwWidget[0].x =  -.5;
   skwWidget[0].y =  -.5;

   skwWidget[2].x =   .5;
   skwWidget[2].y =  -.5;

   skwWidget[4].x =   .5;
   skwWidget[4].y =   .5;

   skwWidget[6].x =  -.5;
   skwWidget[6].y =   .5;

   skwWidget[8].x =  -.5;
   skwWidget[8].y =  -.5;

   netRotationAngle = 0.0;
   netStretchX      = 1.0;
   netStretchY      = 1.0;
   netSkew          = 0.0;

   netOffsetX = 0;
   netOffsetY = 0;

   // now bring in the stretch factors
   transformSkewedToStretched(strWidget, skwWidget, netStretchX, netStretchY);

   // now bring in the size of the client area
   transformStretchedToRotated(rotWidget, strWidget, netRotationAngle);
}

void CMaskTransformEditor::initTransformWidget()
{
   calculateWidgetSize();

   calculateTransformWidget();
}

// ---------------------------------------------------------------------------
// Draw the list of spline vertices into RGBRECT, using
// the machinery in the DISPLAYER. This takes care of all
// the clipping and scaling.
//
void CMaskTransformEditor::drawTransform()
{
//   if (displayer->isOnTargetFrame())
      { // do nothing if off target frame

      // draw the WIDGET rectangle
      displayer->setGraphicsColor(SOLID_YELLOW);

      // draw the transform outline
      displayer->moveToFrame       (rotWidget[0].x, rotWidget[0].y);
      displayer->lineToFrameNoFlush(rotWidget[2].x, rotWidget[2].y);
      displayer->lineToFrameNoFlush(rotWidget[4].x, rotWidget[4].y);
      displayer->lineToFrameNoFlush(rotWidget[6].x, rotWidget[6].y);
      displayer->lineToFrameNoFlush(rotWidget[0].x, rotWidget[0].y);

      // draw the little grab boxes
      displayer->setGraphicsColor(SOLID_YELLOW);
      for (int i=0; i<8; i++)
         {
         displayer->drawUnfilledBoxFrameNoFlush(rotWidget[i].x, rotWidget[i].y);
         }

      // draw a box in the center
      displayer->drawUnfilledBoxFrameNoFlush(rgbXctr, rgbYctr);

      // flush the graphics
      displayer->flushGraphics();
   }
}

// refresh the transform-in-progress by drawing
// the image, then the regionList (containing
// the spline), into the back GL buffer. Then
// make everything visible by swapping buffers
// The chain of calls is displayFrame ->
// onRedraw -> DrawMask -> drawSplineFrame ->
// SwapBuffers
void CMaskTransformEditor::refreshTransform()
{
   displayer->displayFrameIfWeAreNotShowingDiffBlobs();
}
//------------------------------------------------------------------------------
//
// returns the quadrant number of a pt (x, y) relative to the origin.

int CMaskTransformEditor::getQuadrant(int x, int y)
{
   if (x < 0)
      {
      if (y < 0)
         return 1;
      else
         return 2;
      }
   else if (x == 0)
      {
      if (y < 0)
         return 1;
      else if (y == 0)
         return -1;
      else
         return 3;
      }
   else if (x > 0)
      {
      if (y <= 0)
         return 0;
      else
         return 3;
      }

   // Can't get here!
   return -1;
}

// note that negative y points upward, hence the formula here
//
int CMaskTransformEditor::crossProduct(int x0, int y0, int x1, int y1)
{
   return(x1*y0 - x0*y1);
}

// determines whether a pt (x, y) is inside or outide the
// (rotated) transform widget. Works by considering the widget
// polygon rel to (x, y), and calculating the quadrant-deltas
// for each edge. If this is non-zero, then (x, y) is inside
// the widget; if zero, then outside
//
bool CMaskTransformEditor::isOutsideTransformOutline(int x, int y)
{
   int totalQuadrantDeltas = 0;
   int qcur, qnxt, qdel, qcrs;

   // prime the pipeline
   qcur = getQuadrant(rotWidget[0].x - x, rotWidget[0].y - y);

   for (int i=2;  i<=8; i+=2)
      {
      // skipping midpoints (odd i)
      qnxt = getQuadrant(rotWidget[i].x - x, rotWidget[i].y - y);

      qdel = (qnxt - qcur);

      if (qdel == -3)
         qdel =  1;
      else if (qdel ==  3)
         qdel = -1;
      else if ((qdel==-2)||(qdel==2))
         {
         qcrs = crossProduct(rotWidget[i-2].x - x, rotWidget[i-2].y - y,
                             rotWidget[i].x   - x, rotWidget[i].y   - y);

         if (qcrs == 0)
            return false; // on the widget outline
         if (qcrs < 0)
            qdel = -2;
         else qdel = 2;
         }

      totalQuadrantDeltas += qdel;

      qcur = qnxt;
      }

   if (totalQuadrantDeltas != 0)
      return false;

   return true;
}

int CMaskTransformEditor::getWidgetBox(RECT& box)
{
   for (int i=0; i<8; i++)
      {
      if ((rotWidget[i].x >= box.left) && (rotWidget[i].x <= box.right ) &&
          (rotWidget[i].y >= box.top ) && (rotWidget[i].y <= box.bottom))
         return i;
      }

   return -1;
}

double CMaskTransformEditor::getAngle(double cosine, double sine)
{
   switch(getQuadrant(100000*cosine, 100000*sine))
      {
      case 0:
         sine = -sine;
         if (sine < cosine)
            return(asin(sine));
         else
            return(acos(cosine));

         break;

      case 1:
         sine = -sine;
         cosine = -cosine;
         if (sine < cosine)
            return(PI - asin(sine));
         else
            return(PI - acos(cosine));

         break;

      case 2:
          cosine = -cosine;
          if (sine < cosine)
             return(PI + asin(sine));
          else
             return(PI + acos(cosine));

         break;

      case 3:
          if (sine < cosine)
             return(2*PI - asin(sine));
          else
             return(2*PI - acos(cosine));

         break;
     }

   MTIassert(false);
   return 0;
}

double CMaskTransformEditor::getAngleFromClientCoords(int x, int y)
{
   double fx = x - rgbXctr;
   double fy = y - rgbYctr;

   double leng = sqrt( fx*fx + fy*fy );
   if (leng == 0)
      return -1.0;

   fx /= leng;
   fy /= leng;

   return getAngle( fx, fy );
}

void CMaskTransformEditor::calculateMidpoints(DPOINT *src)
{
   src[8]   = src[0];

   for (int i=1;i<8;i+=2)
      {
      src[i].x = (src[i-1].x + src[i+1].x)/2;
      src[i].y = (src[i-1].y + src[i+1].y)/2;
      }
}

bool CMaskTransformEditor::isTooSmall(DPOINT *src, int inipt, int finpt)
{
   double delx = src[inipt].x - src[finpt].x;
   if (delx < 0)
      delx = -delx;

   double dely = src[inipt].y - src[finpt].y;
   if (dely < 0)
      dely = -dely;

   return ((delx < MINEDG) && (dely < MINEDG));
}

bool CMaskTransformEditor::isTooSmall(DPOINT *src)
{
   double ux = src[2].x - src[0].x;
   double uy = src[2].y - src[0].y;

   double vx = src[6].x - src[0].x;
   double vy = src[6].y - src[0].y;

   double dotprod = ux*vy - uy*vx;

   return (fabs(dotprod) < MINEDG*MINEDG);
}

void CMaskTransformEditor::transformRotatedToStretched(DPOINT *dst, DPOINT *src,
                                                       double *angle)
{
   // get initial unit vector
   double cosine =  (src[2].x - src[0].x);
   double sine   =  (src[2].y - src[0].y);

   double leng   = sqrt(cosine*cosine + sine*sine);
   if (leng == 0)
      {
      cosine = 1.00;
      sine   = 0.00;
      }
   else
      {
      cosine /= leng;
      sine   /= leng;
      }

#if 0 // KKKKK formerly
   // corners
   for (int i=0;  i<10; i+=2)
      {
      dst[i].x = (cosine*(src[i].x - rgbXctr) +
                  sine  *(src[i].y - rgbYctr)) / wdgtSize ;

      dst[i].y = (-sine *(src[i].x - rgbXctr) +
                  cosine*(src[i].y - rgbYctr)) / wdgtSize ;
      }
#else
   // corners
   for (int i=0;  i<10; i+=2)
      {
      dst[i].x = (cosine*(src[i].x - rgbXctr) +
                  sine  *(src[i].y - rgbYctr)) / rgbWdth ;

      dst[i].y = (-sine *(src[i].x - rgbXctr) +
                  cosine*(src[i].y - rgbYctr)) / rgbHght ;
      }
#endif

   calculateMidpoints(dst);

   *angle = getAngle(cosine, sine);
}

void CMaskTransformEditor::transformStretchedToSkewed(DPOINT *dst, DPOINT *src,
                                                      double *xstr,
                                                      double *ystr,
                                                      double *skew)
{
   // here we pull out the stretch factors
   *xstr = src[2].x - src[0].x;
   *ystr = src[6].y - src[0].y;

   for (int i=0;i<=8;i+=2)
      {
      dst[i].x = src[i].x / (*xstr);
      dst[i].y = src[i].y / (*ystr);
      }

   calculateMidpoints(dst);

   // here we pull out the "skew"
   *skew = dst[6].x - dst[0].x;
}

void CMaskTransformEditor::acceptTentativeWidget()
{
   // copy the tentative widget to the rotated widget
   for (int i = 0; i < 8; i+=2)
      rotWidget[i] = tntWidget[i];

   // get the midpoints for the new widget edges
   calculateMidpoints(rotWidget);

   // take the widget back to skewed (initial edge horizontal)
   transformRotatedToStretched(strWidget, rotWidget, &netRotationAngle);

   // take the widget back to skewed
   DPOINT skwWidget[10];
   transformStretchedToSkewed(skwWidget, strWidget,
                              &netStretchX, &netStretchY,
                              &netSkew);
}

void CMaskTransformEditor::transformSkewedToStretched(DPOINT *dst, DPOINT *src,
                                                      double xstr, double ystr)
{
   for (int i=0;i<=8;i+=2)
      {
      dst[i].x = src[i].x * xstr;
      dst[i].y = src[i].y * ystr;
      }

   calculateMidpoints(dst);
}

void CMaskTransformEditor::transformStretchedToRotated(DPOINT *dst, DPOINT *src,
                                                       double angle)
{
   // get initial unit vector
   double cosine = cos(angle);
   double sine   = sin(angle);

#if 0 // KKKKK formerly
   // corners
   for (int i=0; i<10; i+=2)
      {
      dst[i].x = (cosine*src[i].x + sine  *src[i].y)*wdgtSize + rgbXctr;

      dst[i].y = (-sine *src[i].x + cosine*src[i].y)*wdgtSize + rgbYctr;
      }
#else
   // corners
   for (int i=0; i<10; i+=2)
      {
      dst[i].x = cosine*src[i].x*rgbWdth + sine*src[i].y*rgbHght   + rgbXctr;

      dst[i].y = -sine *src[i].x*rgbWdth + cosine*src[i].y*rgbHght + rgbYctr;
      }
#endif

   calculateMidpoints(dst);
}

//------------------------------------------------------------------------------

void CMaskTransformEditor::setShift(bool keydown)
{
   if (keydown)
      modKeys |= MOD_KEY_SHIFT;
   else
      modKeys &= ~MOD_KEY_SHIFT;
}

void CMaskTransformEditor::setAlt(bool keydown)
{
   if (keydown)
      modKeys |= MOD_KEY_ALT;
   else
      modKeys &= ~MOD_KEY_ALT;
}

void CMaskTransformEditor::setCtrl(bool keydown)
{
   if (keydown)
      modKeys |= MOD_KEY_CTRL;
   else
      modKeys &= ~MOD_KEY_CTRL;
}

////////////////////////////////////////////////////////////////////////////////
//
// Mouse handlers
//
////////////////////////////////////////////////////////////////////////////////

void CMaskTransformEditor::mouseDown()
{
   // The current mouse client (x, y) is the
   // last one recorded by mouseMove.
   RECT box;
   box.left   = displayer->scaleXClientToFrame(mouseXClient - BOX_SIZE);
   box.top    = displayer->scaleYClientToFrame(mouseYClient - BOX_SIZE);
   box.right  = displayer->scaleXClientToFrame(mouseXClient + BOX_SIZE);
   box.bottom = displayer->scaleYClientToFrame(mouseYClient + BOX_SIZE);

   switch(majorState)
      {
      case MAJORSTATE_TRACK:

         // if box is grabbed
         //    if no modkey
         //       majorState = KEEP_ANGLES
         //    elseif modkey = SHIFT
         //       majorState = SCALE_MASK
         //    elseif modkey = CTRL
         //       majorState = SKEW_MASK
         // elseif outside widget
         //    majorstate = ROTATE_MASK
         // elseif inside widget
         //    majorstate = OFFSET_MASK
                                         
         if ((widgetBox = getWidgetBox(box)) != -1)
            {
            if (modKeys == MOD_KEY_NONE)
               {
               majorState = MAJORSTATE_KEEP_ANGLES;
               }
            else if (modKeys == MOD_KEY_SHIFT)
               {
               majorState = MAJORSTATE_SCALE_MASK;
               }
            else if (modKeys == MOD_KEY_CTRL)
               {
               majorState = MAJORSTATE_SKEW_MASK;
               }
            }
         else if (isOutsideTransformOutline(mouseXFrame, mouseYFrame))
            {
            // Get the angle from client center to mouse.
            // You're outside the widget, so angle is OK
            curAng = getAngleFromClientCoords(mouseXFrame, mouseYFrame);
            newAng = curAng;
            majorState = MAJORSTATE_ROTATE_MASK;
            }
         else
            {
            majorState = MAJORSTATE_OFFSET_MASK;
            }
      break;
      }
} // mouseDown


// use this to segue directly into region drag mode
void CMaskTransformEditor::enterDragMode()
{
   majorState = MAJORSTATE_OFFSET_MASK;
}

// use this to set the majorstate from outside (arggh!)
void CMaskTransformEditor::setMajorState(int majorstate)
{
   majorState = majorstate;
}

// use this to expose the majorState
int CMaskTransformEditor::getMajorState()
{

   return(majorState);
}

// ----------------------------------------------------------------------------

void CMaskTransformEditor::mouseMove(int x, int y, bool doIdleRefresh)
{
   mouseXClient = x;
   mouseYClient = y;

   // these call dscale and then round the result (essential)
   int newMouseXFrame  = displayer->scaleXClientToFrame(x);
   int newMouseYFrame  = displayer->scaleYClientToFrame(y);

   deltaMouseXFrame = newMouseXFrame - mouseXFrame;
   deltaMouseYFrame = newMouseYFrame - mouseYFrame;

   mouseXFrame = newMouseXFrame;
   mouseYFrame = newMouseYFrame;

   int inipt, finpt,
       prept, nxtpt;

   double leng;
   double ux, uy;
   double vx, vy;
   double dotprod;
   double crsprod;
   double a, b;
   bool doRefresh = true;

   switch(majorState)
      {
      case MAJORSTATE_TRACK:
         // ?
         doRefresh = doIdleRefresh;
         break;

      case MAJORSTATE_KEEP_ANGLES:

         if (widgetBox & 1)
            {
            // grabbing an edge

            // we assume the widget edges are all non-zero
            inipt = (widgetBox - 1)%8;
            finpt = (widgetBox + 1)%8;
            nxtpt = (widgetBox + 3)%8;
            prept = (widgetBox + 5)%8;

            ux = rotWidget[nxtpt].x - rotWidget[finpt].x;
            uy = rotWidget[nxtpt].y - rotWidget[finpt].y;

            leng = sqrt(ux*ux+uy*uy);
            ux /= leng;
            uy /= leng;

            dotprod = ux*deltaMouseXFrame + uy*deltaMouseYFrame;

            ux *= dotprod;
            uy *= dotprod;

            // (ux, uv) is the displacement to be applied to inipt, finpt
            tntWidget[inipt].x = rotWidget[inipt].x + ux;
            tntWidget[inipt].y = rotWidget[inipt].y + uy;
            tntWidget[finpt].x = rotWidget[finpt].x + ux;
            tntWidget[finpt].y = rotWidget[finpt].y + uy;

            // the opposite displacement to the opposite pts
            tntWidget[prept].x = rotWidget[prept].x - ux;
            tntWidget[prept].y = rotWidget[prept].y - uy;
            tntWidget[nxtpt].x = rotWidget[nxtpt].x - ux;
            tntWidget[nxtpt].y = rotWidget[nxtpt].y - uy;
            }
         else
            {
            // grabbing a vertex

            // we assume the widget edges are all non-zero
            inipt = (widgetBox + 6)%8;
            finpt = (widgetBox + 0)%8;
            nxtpt = (widgetBox + 2)%8;
            prept = (widgetBox + 4)%8;

            // the unit vector from finpt to nxtpt

            ux = rotWidget[nxtpt].x - rotWidget[finpt].x;
            uy = rotWidget[nxtpt].y - rotWidget[finpt].y;

            leng = sqrt(ux*ux+uy*uy);
            ux /= leng;
            uy /= leng;

            // the unit vector from inipt to finpt

            vx = rotWidget[finpt].x - rotWidget[inipt].x;
            vy = rotWidget[finpt].y - rotWidget[inipt].y;

            leng = sqrt(vx*vx+vy*vy);
            vx /= leng;
            vy /= leng;

            // now we resolve the mouse displacement along
            // the two (non-orthogonal) unit vectors. This
            // means solving the linear equations
            //
            //   ux * a + vx * b = deltaMouseXFrame
            //
            //   uy * a + vy * b = deltaMouseYFrame
            //
            // which we do with Cramer's Rule

            crsprod = ux*vy - uy*vx;

            if (fabs(crsprod) < .01)
               return;

            a = (deltaMouseXFrame*vy - deltaMouseYFrame*vx) / crsprod;
            b = (deltaMouseYFrame*ux - deltaMouseXFrame*uy) / crsprod;

            ux *= a;
            uy *= a;

            vx *= b;
            vy *= b;

            // (ux, uy) is the displacement to be applied to inipt, finpt
            tntWidget[inipt].x = rotWidget[inipt].x + ux;
            tntWidget[inipt].y = rotWidget[inipt].y + uy;
            tntWidget[finpt].x = rotWidget[finpt].x + ux;
            tntWidget[finpt].y = rotWidget[finpt].y + uy;

            // the opposite displacement to the opposite pts
            tntWidget[prept].x = rotWidget[prept].x - ux;
            tntWidget[prept].y = rotWidget[prept].y - uy;
            tntWidget[nxtpt].x = rotWidget[nxtpt].x - ux;
            tntWidget[nxtpt].y = rotWidget[nxtpt].y - uy;

            // (vx, vy) is the displacement to be applied to finpt, nxtpt
            tntWidget[finpt].x += vx;
            tntWidget[finpt].y += vy;
            tntWidget[nxtpt].x += vx;
            tntWidget[nxtpt].y += vy;

            // the opposite displacement to the opposite pts
            tntWidget[inipt].x -= vx;
            tntWidget[inipt].y -= vy;
            tntWidget[prept].x -= vx;
            tntWidget[prept].y -= vy;
            }

         // if widget area not too small accept and display it
         if (!isTooSmall(tntWidget))
            acceptTentativeWidget();

         break;

      case MAJORSTATE_SCALE_MASK:

         inipt = widgetBox;
         finpt = (widgetBox + 2)%8;
         nxtpt = (widgetBox + 4)%8;

         // resolve the mouse displacement along the unit
         // vector from the image center to the inipt

         ux = rotWidget[inipt].x - rgbXctr;
         uy = rotWidget[inipt].y - rgbYctr;

         // leng can't be 0 because widget never has 0 area
         leng = sqrt(ux*ux+uy*uy);
         ux /= leng;
         uy /= leng;

         dotprod = ux*deltaMouseXFrame + uy*deltaMouseYFrame;

         ux *= dotprod;
         uy *= dotprod;

         // apply the resolved displacement to the inipt
         tntWidget[inipt].x = rotWidget[inipt].x + ux;
         tntWidget[inipt].y = rotWidget[inipt].y + uy;

         // now get the coefficient of scaling
         vx = tntWidget[inipt].x - rgbXctr;
         vy = tntWidget[inipt].y - rgbYctr;

         a = sqrt(vx*vx+vy*vy) / leng;

         // apply the scaling to all the vertices
         for (int i = 0; i < 8; i+=2)
            {
            tntWidget[i].x = (rotWidget[i].x - rgbXctr) * a + rgbXctr;
            tntWidget[i].y = (rotWidget[i].y - rgbYctr) * a + rgbYctr;
            }

         // if widget area not too small accept and display it
         if (!isTooSmall(tntWidget))
            acceptTentativeWidget();

         break;

      case MAJORSTATE_SKEW_MASK:

         if (widgetBox & 1)
            {
            // grabbing an edge

            inipt = (widgetBox - 1)%8;
            finpt = (widgetBox + 1)%8;
            nxtpt = (widgetBox + 3)%8;
            prept = (widgetBox + 5)%8;

            tntWidget[inipt].x = rotWidget[inipt].x + deltaMouseXFrame;
            tntWidget[inipt].y = rotWidget[inipt].y + deltaMouseYFrame;
            tntWidget[finpt].x = rotWidget[finpt].x + deltaMouseXFrame;
            tntWidget[finpt].y = rotWidget[finpt].y + deltaMouseYFrame;

            tntWidget[nxtpt].x = rotWidget[nxtpt].x - deltaMouseXFrame;
            tntWidget[nxtpt].y = rotWidget[nxtpt].y - deltaMouseYFrame;
            tntWidget[prept].x = rotWidget[prept].x - deltaMouseXFrame;
            tntWidget[prept].y = rotWidget[prept].y - deltaMouseYFrame;
            }
         else
            {
            // grabbing a vertex

            // we assume the widget edges are all non-zero
            inipt = (widgetBox + 6)%8;
            finpt = (widgetBox + 0)%8;
            nxtpt = (widgetBox + 2)%8;
            prept = (widgetBox + 4)%8;

            tntWidget[inipt]   = rotWidget[inipt];

            tntWidget[finpt].x = rotWidget[finpt].x + deltaMouseXFrame;
            tntWidget[finpt].y = rotWidget[finpt].y + deltaMouseYFrame;

            tntWidget[nxtpt]   = rotWidget[nxtpt];

            tntWidget[prept].x = rotWidget[prept].x - deltaMouseXFrame;
            tntWidget[prept].y = rotWidget[prept].y - deltaMouseYFrame;
            }

         // if widget area not too small accept and display it
         if (!isTooSmall(tntWidget))
            acceptTentativeWidget();

         break;

      case MAJORSTATE_ROTATE_MASK:

         if (deltaMouseXFrame != 0 || deltaMouseYFrame != 0)
            {
            // get the angle from client center to mouse
            newAng = getAngleFromClientCoords(mouseXFrame, mouseYFrame);

            if (newAng >= 0.)
               {
               // if angle is determinate do

               // change in net rotation angle
               netRotationAngle += (newAng - curAng) ;

               curAng = newAng;

               // rotate the widget
               transformStretchedToRotated(rotWidget, strWidget, netRotationAngle);
               }
            }
         break;

      case MAJORSTATE_OFFSET_MASK:

         if (deltaMouseXFrame != 0 || deltaMouseYFrame != 0) {

            // Move Mask Object
            // apply the offset to all the vertices
            for (int i = 0; i < 8; i+=2) {
               tntWidget[i].x = rotWidget[i].x + deltaMouseXFrame;
               tntWidget[i].y = rotWidget[i].y + deltaMouseYFrame;
            }

            rgbXctr += deltaMouseXFrame;
            rgbYctr += deltaMouseYFrame;

            netOffsetX += deltaMouseXFrame;
            netOffsetY += deltaMouseYFrame;

            // if widget area not too small accept and display it
            if (!isTooSmall(tntWidget))
               acceptTentativeWidget();
         }

         break;
      }

   if (doRefresh)
      refreshTransform();

}  // mouseMove

// ---------------------------------------------------------------------------

void CMaskTransformEditor::mouseUp()
{
   switch(majorState)
      {
      case MAJORSTATE_TRACK:
         break;

      case MAJORSTATE_KEEP_ANGLES:
      case MAJORSTATE_SCALE_MASK:
      case MAJORSTATE_SKEW_MASK:
      case MAJORSTATE_ROTATE_MASK:
      case MAJORSTATE_OFFSET_MASK:

         majorState = MAJORSTATE_TRACK;
         break;
      }

} // mouseUp

// ---------------------------------------------------------------------------

POINT* CMaskTransformEditor::TransformLasso(POINT *srcLasso, int count,
                                            POINT *dstLasso)
{
   double cosine = cos(-netRotationAngle);
   double sine = sin(-netRotationAngle);

   int xc = rgbXctr - netOffsetX;
   int yc = rgbYctr - netOffsetY;

   for (int i = 0; i < count; ++i)
      {
      double x = netStretchX * (srcLasso[i].x - xc);
      double y = netStretchY * (srcLasso[i].y - yc);
      dstLasso[i].x =  (int) floor(x*cosine - y*sine + .5) + rgbXctr;
      dstLasso[i].y = (int) floor(x*sine + y*cosine + .5) + rgbYctr;
      }

   return dstLasso;
}


void CMaskTransformEditor::TransformBezier(BEZIER_POINT *src)
{
   BEZIER_POINT *dst = src;
   double cosine = cos(-netRotationAngle);
   double sine = sin(-netRotationAngle);

   int xc = rgbXctr - netOffsetX;
   int yc = rgbYctr - netOffsetY;

   int count = 0;
   int i;
   for (i = 1; ; ++i)
      {
      if (src[i].xctrlpre == src[0].xctrlpre &&
          src[i].yctrlpre == src[0].yctrlpre &&
          src[i].x == src[0].x &&
          src[i].y == src[0].y &&
          src[i].xctrlnxt == src[0].xctrlnxt &&
          src[i].yctrlnxt == src[0].yctrlnxt)
         {
         // This point must be the last point because it matches the first point
         // We do not include this point in the Bezier Editor's vertices.
         count = i;
         break;
         }
      }
      
   for (i = 0; i < count; ++i)
      {
      double xctrlpre = netStretchX * (src[i].xctrlpre - xc);
      double yctrlpre = netStretchY * (src[i].yctrlpre - yc);
      dst[i].xctrlpre =  (int) floor(xctrlpre*cosine - yctrlpre*sine + .5) + rgbXctr;
      dst[i].yctrlpre = (int) floor(xctrlpre*sine + yctrlpre*cosine + .5) + rgbYctr;

      double x = netStretchX * (src[i].x - xc);
      double y = netStretchY * (src[i].y - yc);
      dst[i].x =  (int) floor(x*cosine - y*sine + .5) + rgbXctr;
      dst[i].y = (int) floor(x*sine + y*cosine + .5) + rgbYctr;

      double xctrlnxt = netStretchX * (src[i].xctrlnxt - xc);
      double yctrlnxt = netStretchY * (src[i].yctrlnxt - yc);
      dst[i].xctrlnxt =  (int) floor(xctrlnxt*cosine - yctrlnxt*sine + .5) + rgbXctr;
      dst[i].yctrlnxt = (int) floor(xctrlnxt*sine + yctrlnxt*cosine + .5) + rgbYctr;
      }

   dst[i].xctrlpre = dst[0].xctrlpre;
   dst[i].yctrlpre = dst[0].yctrlpre;
   dst[i].x = dst[0].x;
   dst[i].y = dst[0].y;
   dst[i].xctrlnxt = dst[0].xctrlnxt;
   dst[i].yctrlnxt = dst[0].yctrlnxt;

}

void CMaskTransformEditor::TransformBezier(Vertex *src)
{
   Vertex *dst = src;
   double cosine = cos(-netRotationAngle);
   double sine = sin(-netRotationAngle);

   int xc = rgbXctr - netOffsetX;
   int yc = rgbYctr - netOffsetY;

   // xform first point
   double xctrlpre = netStretchX * (dst->xctrlpre - xc);
   double yctrlpre = netStretchY * (dst->yctrlpre - yc);
   dst->xctrlpre = (int) floor(xctrlpre*cosine - yctrlpre*sine + .5) + rgbXctr;
   dst->yctrlpre = (int) floor(xctrlpre*sine + yctrlpre*cosine + .5) + rgbYctr;

   double x = netStretchX * (dst->x - xc);
   double y = netStretchY * (dst->y - yc);
   dst->x =  (int) floor(x*cosine - y*sine + .5) + rgbXctr;
   dst->y = (int) floor(x*sine + y*cosine + .5) + rgbYctr;

   double xctrlnxt = netStretchX * (dst->xctrlnxt - xc);
   double yctrlnxt = netStretchY * (dst->yctrlnxt - yc);
   dst->xctrlnxt = (int) floor(xctrlnxt*cosine - yctrlnxt*sine + .5) + rgbXctr;
   dst->yctrlnxt = (int) floor(xctrlnxt*sine + yctrlnxt*cosine + .5) + rgbYctr;

   // xform subsequent points
   while ((dst = dst->fwd) != NULL)
      {
      if (dst == src) break;

      double xctrlpre = netStretchX * (dst->xctrlpre - xc);
      double yctrlpre = netStretchY * (dst->yctrlpre - yc);
      dst->xctrlpre = (int) floor(xctrlpre*cosine - yctrlpre*sine + .5) + rgbXctr;
      dst->yctrlpre = (int) floor(xctrlpre*sine + yctrlpre*cosine + .5) + rgbYctr;

      double x = netStretchX * (dst->x - xc);
      double y = netStretchY * (dst->y - yc);
      dst->x = (int) floor(x*cosine - y*sine + .5) + rgbXctr;
      dst->y = (int) floor(x*sine + y*cosine + .5) + rgbYctr;

      double xctrlnxt = netStretchX * (dst->xctrlnxt - xc);
      double yctrlnxt = netStretchY * (dst->yctrlnxt - yc);
      dst->xctrlnxt = (int) floor(xctrlnxt*cosine - yctrlnxt*sine + .5) + rgbXctr;
      dst->yctrlnxt = (int) floor(xctrlnxt*sine + yctrlnxt*cosine + .5) + rgbYctr;
      }
}

void CMaskTransformEditor::InverseTransformBezier(Vertex *src)
{
   Vertex *dst = src;
   double cosine = cos(-netRotationAngle);
   double sine = sin(-netRotationAngle);

   int xc = rgbXctr - netOffsetX;
   int yc = rgbYctr - netOffsetY;

   double xprim, yprim;

   // invert first point
   xprim = dst->xctrlpre - rgbXctr;
   yprim = dst->yctrlpre - rgbYctr;
   dst->xctrlpre = (int) floor((( xprim*cosine + yprim*sine)/netStretchX)+ 0.5) + xc;
   dst->yctrlpre = (int) floor(((-xprim*sine + yprim*cosine)/netStretchY)+ 0.5) + yc;

   xprim = dst->x - rgbXctr;
   yprim = dst->y - rgbYctr;
   dst->x = (int) floor((( xprim*cosine + yprim*sine)/netStretchX)+ 0.5) + xc;
   dst->y = (int) floor(((-xprim*sine + yprim*cosine)/netStretchY)+ 0.5) + yc;

   xprim = dst->xctrlnxt - rgbXctr;
   yprim = dst->yctrlnxt - rgbYctr;
   dst->xctrlnxt = (int) floor((( xprim*cosine + yprim*sine)/netStretchX)+ 0.5) + xc;
   dst->yctrlnxt = (int) floor(((-xprim*sine + yprim*cosine)/netStretchY)+ 0.5) + yc;

   // invert subsequent points
   while ((dst = dst->fwd) != NULL)
      {
      if (dst == src) break;

      xprim = dst->xctrlpre - rgbXctr;
      yprim = dst->yctrlpre - rgbYctr;
      dst->xctrlpre = (int) floor((( xprim*cosine + yprim*sine)/netStretchX)+ 0.5) + xc;
      dst->yctrlpre = (int) floor(((-xprim*sine + yprim*cosine)/netStretchY)+ 0.5) + yc;

      xprim = dst->x - rgbXctr;
      yprim = dst->y - rgbYctr;
      dst->x = (int) floor((( xprim*cosine + yprim*sine)/netStretchX)+ 0.5) + xc;
      dst->y = (int) floor(((-xprim*sine + yprim*cosine)/netStretchY)+ 0.5) + yc;

      xprim = dst->xctrlnxt - rgbXctr;
      yprim = dst->yctrlnxt - rgbYctr;
      dst->xctrlnxt = (int) floor((( xprim*cosine + yprim*sine)/netStretchX)+ 0.5) + xc;
      dst->yctrlnxt = (int) floor(((-xprim*sine + yprim*cosine)/netStretchY)+ 0.5) + yc;
      }
}

