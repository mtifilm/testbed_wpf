#ifndef TrackingToolH
#define TrackingToolH

#include "bthread.h"
#include "HRTimer.h"
#include "IniFile.h"
#include "Ippheaders.h"
#include "MTIio.h"
#include "PixelRegions.h"
#include "RegionOfInterest.h"
#include "RepairShift.h"
#include "ToolObject.h"
#include "TrackingBox.h"


//////////////////////////////////////////////////////////////////////

// Tracking Tool Number (16-bit number that uniquely identifies this tool)
#define TRACKING_TOOL_NUMBER    223

// Tracking Tool Command Numbers
#define TRK_CMD_INVALID                       -1
#define TRK_CMD_NOOP                           0
#define TRK_CMD_ADD_SELECT_OR_DRAG_TRACKPOINT  1
#define TRK_CMD_STOP_DRAGGING_TRACKPOINT       2
#define TRK_CMD_SELECT_ANOTHER_TRACKPOINT      3

#define TRK_CMD_LOAD_TRACKPOINTS               5
#define TRK_CMD_LOAD_TRACKPOINTS_NOT_MARKS     6
#define TRK_CMD_SAVE_TRACKPOINTS               7
#define TRK_CMD_CLEAR_TRACKPOINTS              8
#define TRK_CMD_DELETE_SELECTED_TRACKPOINTS    9

#define TRK_CMD_BEGIN_PAN                     10
#define TRK_CMD_END_PAN                       11

#define TRK_CMD_TRACK                         12
#define TRK_CMD_STOP                          13
#define TRK_CMD_PAUSE_OR_CONTINUE             14

enum ETrackingMouseMode
{
   Tracking_MOUSE_MODE_NONE,
   Tracking_MOUSE_MODE_STRETCH,
};


//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CTrackingToolParameters;

//////////////////////////////////////////////////////////////////////


class CTrackingTool : public CToolObject
{
public:
   CTrackingTool(const string &newToolName);
   virtual ~CTrackingTool();

   // Static method to create a new instance of this tool
   static CToolObject* MakeTool(const string& newToolName);

   virtual int toolInitialize(CToolSystemInterface *newSystemAPI);
   virtual int toolShutdown();

   // Tool Event Handlers
   virtual int onChangingClip();
   virtual int onChangingFraming();
   virtual int onRedraw(int frameIndex);
   virtual int onUserInput(CUserInput &userInput);
   virtual int onToolCommand(CToolCommand &toolCommand);
   virtual int onToolProcessingStatus(const CAutotoolStatus& autotoolStatus);
   virtual int onMouseMove(CUserInput &userInput);
   virtual int onPreviewHackDraw(int frameIndex, MTI_UINT16 *frameBits, int frameBitsSizeInBytes);

   virtual int toolActivate();
   virtual int toolDeactivate();

   virtual CToolProcessor* makeToolProcessorInstance(
                                          const bool *newEmergencyStopFlagPtr);

   // These are not inherited from CToolObject
   bool LockTrackingTool();
   void ReleaseTrackingTool();
   void SetTrackingToolEditor(TrackingBoxManager *newEditor);
   void SetTrackingToolProgressMonitor(
                          IToolProgressMonitor *newToolProgressMonitor);
   void EnableTrackingTool();
   void DisableTrackingTool();
   int  StartTracking(bool force = false);
   void StopTracking();

private:

   // Instance Variables of Tracking Tool
   bool TrackingIsEnabled;
   ETrackingMouseMode mouseMode;
   bool TrackingInProgress;
   TrackingBoxManager *TrkPtsEditor;
   int trackingToolSetupHandle;
   bool advisoryLock;
#ifdef DEBUG_VISUALIZATION
   Ipp8uArray _debugVisualizationArray;
   CSpinLock _debugVisualizationLock;
#endif

   // Command Handlers

   // Tool processing
   virtual int RunFrameProcessing(int newResumeFrame);
   int GatherTrackingToolParameters(CTrackingToolParameters &toolParams);
   void DestroyAllToolSetups();

   // Helpers
   void RemoveAllTrackingBoxes();
   bool isAllTrackDataValid();
   Ipp8uArray FilterImage(MTI_UINT16 *imageData, int width, int height, int depth, int currentFrame);
};

//////////////////////////////////////////////////////////////////////

struct STrackingParameters
{
   TrackingBoxManager *TrkPtsEditor;
   CTrackingTool *TrackingToolObject;
   MTI_UINT8 lut8[256 * 3];
};

class CTrackingToolParameters : public CToolParameters
{
public:
   CTrackingToolParameters() : CToolParameters(1, 1) {};
   virtual ~CTrackingToolParameters() { };

   STrackingParameters trackingParameters;
};

//////////////////////////////////////////////////////////////////////

#endif
