#ifndef TRANSFORM_EDITOR
#define TRANSFORM_EDITOR

#include "machine.h"

class CDisplayer;
class CImageFormat;
class CPixelRegion;
class CToolPaintInterface;

class CTransformEditor
{
public:

   CTransformEditor(CDisplayer *);

   ~CTransformEditor();

   void setToolPaintInterface(CToolPaintInterface *);
   void setImageRectangle(RECT&);
   void calculateWidgetSize();
   void calculateTransformWidget();
   void resetTransform();
   void initTransformWidget();
   void moveImportFrame(int, int);
   void saveTransform();
   void restoreTransform();
   void setTransform(double, double, double, double, double, double);
   void setInverseTransform(double, double, double, double, double, double);
   void setRotation(double);
   void setSkew(double);
   void setStretchX(double);
   void setStretchY(double);
   void setOffsetX(double);
   void setOffsetY(double);
   void setOffsetXY(double, double);
   void getTransform(double *, double *, double *, double *, double *, double *);
   void setImportFrameOffset(int);
   void setImportFrameTimecode(int);
   void transmitProxy(unsigned int *);
   void transmitImportFrameMode(int);
   void transmitImportFrameOffset(int);
   void transmitImportFrameTimecode(int);
   void transmitTargetFrameTimecode(int);
   void setFrameLoadTimer();
   void transmitOnionSkinOffsets(int, int);
   void clearStrokeStack();
   void transmitStrokeEntry(int, int);
   void transmitPickedColor(float *);
   void onLoadNewTargetFrame();
   void transmitTransform(double, double, double, double, double, double);

   void setShift(bool);
   void setAlt(bool);
   void setCtrl(bool);
   void setKeyA(bool);
   void setKeyD(bool);

   void positionImportFrame();
   void mouseDown();
   void mouseMove(int x, int y);
   void mouseUp  ();

   void drawTransform();

   void loadTransformQueue(double, double, double, double, double, double);
   void timeTransform();
   void refreshTransform();

private:

   int getQuadrant(int x, int y);
   int crossProduct(int x0, int y0, int x1, int y1);
   bool isOutsideTransformOutline(int x, int y);

   int getWidgetBox(RECT&);

   double getAngle(double cosine, double sine);
   double getAngleFromClientCoords(int x, int y);

   void calculateMidpoints(DPOINT *src);
   bool isTooSmall(DPOINT *src, int inipt, int finpt);
   bool isTooSmall(DPOINT *src);

   void transformRotatedToStretched(DPOINT *dst, DPOINT *src, double *angle);
   void transformStretchedToSkewed(DPOINT *dst, DPOINT *src,
                                   double *xstr, double *ystr,
                                   double *skew);

   void acceptTentativeWidget();

   void transformSkewedToStretched(DPOINT *dst, DPOINT *src, double xstr, double ystr);
   void transformStretchedToRotated(DPOINT *dst, DPOINT *src, double angle);

private:

   CDisplayer *displayer;

   CToolPaintInterface *toolPaintInterface;

   RECT rgbRect;
   int rgbWdth, rgbHght;
   int rgbXctr, rgbYctr;

   int wdgtSize;

   DPOINT skwWidget[10];

   DPOINT strWidget[10];

   DPOINT rotWidget[10];

   DPOINT tntWidget[10];

   // mouse client coords
   int mouseXClient,
       mouseYClient;

   // mouse client deltas
   int deltaMouseXClient,
       deltaMouseYClient;

   // mouse frame coords
   int mouseXFrame,
       mouseYFrame;

   // mouse frame deltas
   int deltaMouseXFrame,
       deltaMouseYFrame;

   // for rotation dynamics
   double curCos, curSin, curAng;
   double newCos, newSin, newAng;

   // for translation dynamics
   double newOffsetX;
   double newOffsetY;

   // for skew dynamics - 0-7
   int widgetBox;

   double netRotationAngle;
   //double netStretchFactor;
   double netStretchX;
   double netStretchY;
   double netSkew;
   double netOffsetX;
   double netOffsetY;

   double savedRotationAngle;
   //double savedStretchFactor;
   double savedStretchX;
   double savedStretchY;
   double savedSkew;
   double savedOffsetX;
   double savedOffsetY;

   int rotEmpty;
   int rotFill;

struct TRANSFORM_ENTRY
{
   double tr_angle;
   double tr_stretchX;
   double tr_stretchY;
   double tr_skew;
   double tr_offsx;
   double tr_offsy;
};

#define MAX_TRANSFORMS 64

   TRANSFORM_ENTRY transformQueue[MAX_TRANSFORMS];

#define MOD_KEY_NONE  0
#define MOD_KEY_SHIFT 1
#define MOD_KEY_ALT   2
#define MOD_KEY_CTRL  4
   int modKeys;

#define MAJORSTATE_TRACK              0
#define MAJORSTATE_KEEP_ANGLES        1
#define MAJORSTATE_SCALE_IMPORT       2
#define MAJORSTATE_SKEW_IMPORT        3
#define MAJORSTATE_ROTATE_IMPORT      4
#define MAJORSTATE_OFFSET_IMPORT      5
   int majorState;

   int cntr;
};

#endif
