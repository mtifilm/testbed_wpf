// TrackingProc32f.cpp: implementation of the CTrackingProc16 class.
//
//////////////////////////////////////////////////////////////////////

#include "TrackingProc32f.h"

#include "bthread.h"
#include "Clip3.h"
#include "err_tool.h"
#include "HRTimer.h"
#include "ImageFormat3.h"
#include "IniFile.h"  // for TRACE_N()
#include "MathFunctions.h"
#include "MTIKeyDef.h"
#include "MTImalloc.h"
#include "MTIsleep.h"
#include "MTIstringstream.h"
#include "PixelRegions.h"
#include "ThreadLocker.h"
#include "ToolCommand.h"
#include "ToolCommandTable.h"
#include "ToolProgressMonitor.h"
#include "ToolSystemInterface.h"
#include "ToolUserInputMap.h"
#include "TrackingBoxManager.h"
#include "TrackingTool.h"

#define TRACKING_MIN_THREADS 1000  // effectively disable multithreading
static CSpinLock TrackingThreadLock;
#define MAX_FILTER_RADIUS 10
#define REFINED_MATCH_SCALE 5


///////////////////////////////////////////////////////////////////////////////
// Construction/Destruction
///////////////////////////////////////////////////////////////////////////////

CTrackingProc::CTrackingProc(int newToolNumber, const string &newToolName,
                                 CToolSystemInterface *newSystemAPI,
                                 const bool *newEmergencyStopFlagPtr,
                                 IToolProgressMonitor *newToolProgressMonitor)
 : CToolProcessor(newToolNumber, newToolName, 1, 1, newSystemAPI,
                  newEmergencyStopFlagPtr, newToolProgressMonitor)
{
   StartProcessThread(); // Required by Tool Infrastructure
}

CTrackingProc::~CTrackingProc()
{
   // Shouldn't the subimages array be cleared? QQQ
}


///////////////////////////////////////////////////////////////////////////////
// Setup
///////////////////////////////////////////////////////////////////////////////

int CTrackingProc::SetIOConfig(CToolIOConfig *toolIOConfig)
{
   // Tell Tool Infrastructure, via CToolProcessor supertype, about the
   // number of input and output frames

   SetInputMaxFrames(0, 2, 1);  // 'In' port index 0,
                                // Need two frames for each iteration,
                                // Need one new frame each iteration.

   SetOutputMaxFrames(0, 1);  // 'Out' port index 0,
                              // One frame output per iteration

   // Well, really the frame is not modified at all, but we need
   // to 'output' it to display it
   SetOutputModifyInPlace(0, 0, false); // 'Out' port index 0,
                                        // 'In' port index 0,
                                        // Do not clone the output frame

   return 0;
}

int CTrackingProc::SetFrameRange(CToolFrameRange *toolFrameRange)
{
   CToolFrameRange::SFrameRange &frameRange = toolFrameRange->inFrameRange[0];
   _inFrameIndex = frameRange.inFrameIndex;
   _outFrameIndex = frameRange.outFrameIndex;

   return 0;
}

int CTrackingProc::SetParameters(CToolParameters *toolParams)
{
   int retVal=0;

   CTrackingToolParameters *trackingToolParams
                        = static_cast<CTrackingToolParameters*>(toolParams);

   // Make a private copy of the parameters for later reference
   // Caller's toolParam is deleted after this function returns
   _trkPtsEditor = trackingToolParams->trackingParameters.TrkPtsEditor;
   _trackingArray = _trkPtsEditor->getTrackingArray();
//   _trackingArray->getExtents(_TrackBoxExtentX, _TrackBoxExtentY,
//                              _SearchAreaLeftExtent, _SearchAreaRightExtent,
//                              _SearchAreaTopExtent, _SearchAreaBottomExtent);
   int trackBoxExtent = _trackingArray->getTrackingBoxRadius();
   _TrackBoxExtentX = _TrackBoxExtentY = trackBoxExtent;
   _TrackBoxSize = MtiSize((2 * _TrackBoxExtentX) + 1, (2 * _TrackBoxExtentY) + 1);

   int searchyBoxExtent = _trackingArray->getSearchBoxRadius();
   _SearchAreaLeftExtent =
      _SearchAreaRightExtent =
         _SearchAreaTopExtent =
            _SearchAreaBottomExtent = trackBoxExtent + searchyBoxExtent;
   _SearchAreaSize = MtiSize(_SearchAreaLeftExtent + 1 + _SearchAreaRightExtent,
                             _SearchAreaTopExtent + 1 + _SearchAreaBottomExtent);

   // Grab the display LUT up front if we're going to need it.
   if (_trackingArray->getApplyLutFlag())
   {
      MTImemcpy(_currentDisplayLut8, trackingToolParams->trackingParameters.lut8, 256 * 3);
   }

   _trackingToolObject = trackingToolParams->trackingParameters.TrackingToolObject;

   // Find the iteration count
   _iterationCount = _outFrameIndex - _inFrameIndex;

   // Done
   return retVal;
}

#ifdef DEBUG_VISUALIZATION
void CTrackingProc::SetDebugParameters(Ipp8uArray &debugVisualizationArray, CSpinLock *debugVisualizationLock)
{
   _debugVisualizationArray = debugVisualizationArray;
   _debugVisualizationLock = debugVisualizationLock;
}
#endif



///////////////////////////////////////////////////////////////////////////////
//
// Processing functions
//
///////////////////////////////////////////////////////////////////////////////

int CTrackingProc::GetIterationCount(SToolProcessingData &procData)
{
   // This function is called by the Tool Infrastructure so it knows
   // how many processing iterations to do before stopping

   return _iterationCount;
}
///////////////////////////////////////////////////////////////////////////////

int CTrackingProc::BeginProcessing(SToolProcessingData &procData)
{
   CAutoErrorReporter autoErr("CTrackingProc::BeginProcessing",
                              AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   int retVal = 0;
   int nTmpX, nTmpY;

   // Tracking does not save to history, since it doesn't modify the frames, duh!
   SetSaveToHistory(false);

   // Just save some values for later
   const CImageFormat *imageFormat = GetSrcImageFormat(0);
   _nPicCol = imageFormat->getPixelsPerLine();
   _nPicRow = imageFormat->getLinesPerFrame();
   _RGB = imageFormat->getPixelComponents() == IF_PIXEL_COMPONENTS_RGB
        || imageFormat->getPixelComponents() == IF_PIXEL_COMPONENTS_RGBA
        || imageFormat->getPixelComponents() == IF_PIXEL_COMPONENTS_Y
        || imageFormat->getPixelComponents() == IF_PIXEL_COMPONENTS_YYY;

   const CImageFormat *realImageFormat = GetSystemAPI()->getVideoClipImageFormat();
   _sourceDepth = realImageFormat->getBitsPerComponent();

   if (_trackingArray->getApplyLutFlag())
   {
      // We are constructing a lut for each component that will have one entry
      // per component value. The lut will convert the component value to a
      // normalized float that reflects both the display lut values and the
      // weight assigned to the channel corresponding to the component for
      // the conversion to monochrome.

      int channel = _trackingArray->getChannel();
      vector<float> compWeights = { 0.32F, 0.64F, 0.04F };
      switch (channel)
      {
         case 0:
            compWeights = { 1.0F, 0.0F, 0.0F };
            break;
         case 1:
            compWeights = { 0.0F, 1.0F, 0.0F };
            break;
         case 2:
            compWeights = { 0.0F, 0.0F, 1.0F };
            break;
         default:
            break;
      }

      int valueThatMapsToOnePointOh = 1 << _sourceDepth;
      float *weightedLuts[3] = {_weightedLutR, _weightedLutG, _weightedLutB};
      for (int component = 0; component < 3; ++component)
      {
         float *componentLut = weightedLuts[component];

         // Shortcut if component is not being used for conversion to monochrome
         if (compWeights[component] == 0.0F)
         {
            for (int i = 0; i < valueThatMapsToOnePointOh; ++i)
            {
               componentLut[i] = 0.0F;
            }

            continue;
         }

         // Initialize component lut with neutral values.
         for (int i = 0; i < valueThatMapsToOnePointOh; ++i)
         {
            componentLut[i] = float(i);
         }

         // Translate 8-bit displayer lut to IPP levels and values.
         // Also applies the component weight.
         float lutLevels[257];
         float lutValues[257];
         MTI_UINT8 *displayLut = &_currentDisplayLut8[component * 256];

         // Transform the neutral lut to reflect the display lut using IPP.
         // The setup assumes that linear interpolation will be used, so finds
         // the breaks where the display lut slope changes between entries.
         lutLevels[0] = 0;
         lutValues[0] = displayLut[0] / 256.0F;
         int nLevels = 1;
         int prevSlope = int(displayLut[1]) - int(displayLut[0]);

         for (int i = 1; i < 256; ++i)
         {
            int slope = int(displayLut[i]) - int(displayLut[i - 1]);
            if (slope == prevSlope)
            {
               continue;
            }

            prevSlope = slope;
            lutLevels[nLevels] = valueThatMapsToOnePointOh * (i / 256.0F);
            lutValues[nLevels] = displayLut[i] * compWeights[component] / 256.0F;
            ++nLevels;
         }

//         lutLevels[nLevels] = valueThatMapsToOnePointOh * (255 / 256.0F);
//         lutValues[nLevels] = displayLut[255] * compWeights[component] / 256.0F;
//         ++nLevels;

         lutLevels[nLevels] = valueThatMapsToOnePointOh;
         lutValues[nLevels] = displayLut[255] * compWeights[component] / 256.0F;
         ++nLevels;

         int specSize = 0;
         const Ipp32f *pValues[1];
         pValues[0] = lutValues;
         const Ipp32f *pLevels[1];
         pLevels[0] = lutLevels;
         int pNLevels[1];
         pNLevels[0] = nLevels;

         MtiSize dataSize { valueThatMapsToOnePointOh, 1 };
         IppThrowOnError(  ippiLUT_GetSize(ippLinear, ipp32f, ippC1, dataSize, pNLevels, &specSize) );
                           IppiLUT_Spec* pSpec = (IppiLUT_Spec*) ippsMalloc_8u(specSize);
         IppThrowOnError(  ippiLUT_Init_32f(ippLinear, ippC1, dataSize, pValues, pLevels, pNLevels, pSpec) );
         IppThrowOnError(  ippiLUT_32f_C1IR(
                                 componentLut,
                                 valueThatMapsToOnePointOh * sizeof(Ipp32f),
                                 dataSize,
                                 pSpec) );
                           ippsFree(pSpec);
      }
   }

   // careful - (height, width)
   _BaseFilteredImage.clear();
   _NextFilteredImage.clear();

   // DEBUGGING!
#ifdef DEBUG_VISUALIZATION
   GetSystemAPI()->enterPreviewHackMode();
#endif

   // Sort the tracking points into the order they need to be processed
   _AnchorList = _trackingArray->getTrackingBoxTags(); // was CreateAnchorTree();

   // Allocate per-point data
   _TrackingBoxSubimagesArray.clear();

   // Boxes must be bigger because we need a point to perform a bilinear interpolation
   nTmpX = 2 * (_TrackBoxExtentX + 1) + 1;
   nTmpY = 2 * (_TrackBoxExtentY + 1) + 1;
   for (auto &trackingBoxTag : _AnchorList)
   {
      _TrackingBoxSubimagesArray[trackingBoxTag]._TrackBoxData = MonoArrayType({nTmpX, nTmpY});
   }

   // Boxes must be bigger because we need point to perform a bilinear interpolation
   nTmpX = (_SearchAreaLeftExtent + 1) + (_SearchAreaRightExtent + 1) + 1;
   nTmpY = (_SearchAreaTopExtent + 1) + (_SearchAreaBottomExtent + 1) + 1;
   for (auto &subimagesEntry : _TrackingBoxSubimagesArray)
   {
      subimagesEntry.second._SearchAreaData = MonoArrayType({nTmpX, nTmpY});
   }

   IToolProgressMonitor *progressMonitor = GetToolProgressMonitor();
   if (progressMonitor != NULL)
   {
     progressMonitor->StartProgress();
   }

   HRT.Start();   // Start the timer
   //_trackingToolObject->setStatusMessage("Tracking...");

   // Success, continue
   return retVal;
}
///////////////////////////////////////////////////////////////////////////////

int CTrackingProc::BeginIteration(SToolProcessingData &procData)
{
   int frameIndexList[2];
	bool isLastPass = (procData.iteration == (_iterationCount - 1));
   int numberOfInputFrames = (isLastPass? 1 : 2);

   // Track from current frame to next frame, then show CURRENT frame

   frameIndexList[0] = _inFrameIndex + procData.iteration;
   frameIndexList[1] = _inFrameIndex + procData.iteration + 1;
   SetInputFrames(0, numberOfInputFrames, frameIndexList);
   SetOutputFrames(0, 1, frameIndexList); // output first frame only
	// No release because modify-in-place is ON

   return 0;
}
///////////////////////////////////////////////////////////////////////////////

int CTrackingProc::DoProcess(SToolProcessingData &procData)
{
   CAutoErrorReporter autoErr("CTrackingProc::DoProcess",
      AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   try
	{
		bool isFirstPass = procData.iteration == 0;
		bool isLastPass = procData.iteration == (_iterationCount - 1);

		if (!isLastPass)
		{
			CToolFrameBuffer *inToolFrameBufferB = GetRequestedInputFrame(0, 0);
			CToolFrameBuffer *inToolFrameBufferN = GetRequestedInputFrame(0, 1);
         auto baseRgbImageData = inToolFrameBufferB->GetVisibleFrameBufferPtr();
         auto nextRgbImageData = inToolFrameBufferN->GetVisibleFrameBufferPtr();

			if (baseRgbImageData == nullptr || nextRgbImageData == nullptr)
			{
				autoErr.errorCode = TOOL_ERROR_FRAME_IS_MISSING;
				autoErr.msg << "Tracking aborted due to missing frame   ";
				return TOOL_ERROR_FRAME_IS_MISSING;
			}

         Ipp16uArray baseRgbImageArray({_nPicCol, _nPicRow, 3}, baseRgbImageData);
         Ipp16uArray nextRgbImageArray({_nPicCol, _nPicRow, 3}, nextRgbImageData);
			int retVal = TrackNextFrame(inToolFrameBufferB->GetFrameIndex(), baseRgbImageArray, nextRgbImageArray);
			if (retVal && isFirstPass)
			{
				TRACE_0(errout << "Warning: one or more tracking boxes were invalid and were ignored by TrackNextFrame()");
			}
		}
	}
   catch (const std::exception &ex)
   {
      TRACE_0(errout << "TrackNextFrame failed " << ex.what());
      return  (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_ERROR);
   }
   catch (...)
   {
		TRACE_0(errout << "Unknown error in TrackNextFrame");
      return  (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_ERROR);
   }

   return 0;
}
///////////////////////////////////////////////////////////////////////////////

int CTrackingProc::EndIteration(SToolProcessingData &procData)
{
   // Update the status
   IToolProgressMonitor *progressMonitor = GetToolProgressMonitor();
   if (progressMonitor != NULL)
   {
     progressMonitor->BumpProgress();
   }

   return 0;
}
///////////////////////////////////////////////////////////////////////////////

int CTrackingProc::EndProcessing(SToolProcessingData &procData)
{
   // DEBUGGING!
#ifdef DEBUG_VISUALIZATION
   GetSystemAPI()->exitPreviewHackMode();
#endif

   _BaseFilteredImage.clear();
   _NextFilteredImage.clear();

   _TrackingBoxSubimagesArray.clear();

   // Did tracking run to completion or was it aborted?
	if (procData.iteration == _iterationCount)
   {
		_trkPtsEditor->notifyTrackingComplete();
	}
	else
	{
		_trkPtsEditor->notifyTrackingStopped();
	}

   IToolProgressMonitor *progressMonitor = GetToolProgressMonitor();
   if (progressMonitor != NULL)
   {
//      progressMonitor->SetStatusMessage("Tracking complete");
		progressMonitor->StopProgress(procData.iteration == _iterationCount);
   }

   return 0;
}
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// Tracking routines, should be moved to own algorithm
//
//
FPOINT CTrackingProc::FindBestMatch(FPOINT fp) const
{
   FPOINT tpR;
   int tbex = _TrackBoxExtentX;
   int tbey = _TrackBoxExtentY;
   int sbel = _SearchAreaLeftExtent;
   int sber = _SearchAreaRightExtent;
   int sbet = _SearchAreaTopExtent;
   int sbeb = _SearchAreaBottomExtent;

   // Tracking box ROI
   int trackerWidth = (tbex * 2) + 1;
   int trackerHeight = (tbey * 2) + 1;
   MtiRect trackingBoxRoi(fp.x - tbex, fp.y - tbey, trackerWidth, trackerHeight);
   MtiRect imgRoi = _BaseFilteredImage.getRoi();
   trackingBoxRoi.x = std::max<float>(trackingBoxRoi.x, imgRoi.x);
   trackingBoxRoi.y = std::max<float>(trackingBoxRoi.y, imgRoi.y);
   if ((trackingBoxRoi.x + trackingBoxRoi.width) >= (imgRoi.x + imgRoi.width))
   {
      trackingBoxRoi.x = (imgRoi.x + imgRoi.width - trackingBoxRoi.width - 1);
   }
   if ((trackingBoxRoi.y + trackingBoxRoi.height) >= (imgRoi.y + imgRoi.height))
   {
      trackingBoxRoi.y = (imgRoi.y + imgRoi.height - trackingBoxRoi.height - 1);
   }

   auto trackBoxArray = _BaseFilteredImage(trackingBoxRoi);

   // The largest this can be is
   double hugeL1 = _TrackBoxSize.width * _TrackBoxSize.height * 65536.0;
   double minL1 = hugeL1;

   float originX = (sbel + (sber - trackerWidth) + 1) / 2.F;
   float originY = (sbet + (sbeb - trackerHeight) + 1) / 2.F;
   float bestMatchDistance = std::max<int>(sbet*sbet + sbel*sbel, sbeb*sbeb + sber*sber); // larger than max

   for (int searchOffsetY = -sbet; searchOffsetY <= sbeb - trackerHeight; ++searchOffsetY)
   {
      for (int searchOffsetX = -sbel; searchOffsetX <= sber - trackerWidth; ++searchOffsetX)
      {
         // searchX and searchY hold the precise tracking position (float)
         // which is NOT the same as the center of the best tracking box
         // position (integer)
         float searchX = fp.x + searchOffsetX;
         float searchY = fp.y + searchOffsetY;

         MtiRect searchBoxRoi(int(searchX), int(searchY), trackerWidth, trackerHeight);
         if ((searchBoxRoi & _NextFilteredImage.getRoi()) != searchBoxRoi)
         {
            continue;
         }

         auto searchBoxArray = _NextFilteredImage(searchBoxRoi);

         double L1;
         trackBoxArray.L1NormDiff(searchBoxArray, &L1);

         // No need to sqrt this, just need relative magnitude of distance from
         // the center of the trackib box position (0, 0)
         float searchBoxCenterOffsetX = searchOffsetX + tbex;
         float searchBoxCenterOffsetY = searchOffsetY + tbey;
         float distance = (searchBoxCenterOffsetX * searchBoxCenterOffsetX)
                        + (searchBoxCenterOffsetY * searchBoxCenterOffsetY);
         bool foundNewMin = (L1 < minL1) || (L1 == minL1 && distance < bestMatchDistance);

         if (foundNewMin)
         {
            minL1 = L1;
            bestMatchDistance = distance;

            // Record current best position.
            float x = searchX + tbex;
            float y = searchY + tbey;
            tpR = FPOINT(x, y);
         }
      }
   }

   if (minL1 == hugeL1)
   {
      // Shouldn't ever happen - all search boxes were clipped??
      return fp;
   }

   tpR = FindBestRefinedMatch(REFINED_MATCH_SCALE, fp, tpR, true);

   return tpR;
}

FPOINT CTrackingProc::FindBestRefinedMatch(int SCALE, FPOINT dpB, FPOINT dpN, bool doParabolicRefinement) const
{
   // stupid fucking debugger
   float xB = dpB.x;
   float yB = dpB.y;
   float xN = dpN.x;
   float yN = dpN.y;

   FPOINT tpR(dpB);
   MtiRect trackBoxRoi =
   {
      int(dpB.x) - _TrackBoxExtentX,
      int(dpB.y) - _TrackBoxExtentY,
      (_TrackBoxExtentX * 2) + 1,
      (_TrackBoxExtentY * 2) + 1
   };

   if ((trackBoxRoi & _BaseFilteredImage.getRoi()) != trackBoxRoi)
   {
      return dpN;
   }

   IppiSize scaledTrackBoxSize = { trackBoxRoi.width * SCALE, trackBoxRoi.height * SCALE };
   auto scaledTrackBoxArray = _BaseFilteredImage(trackBoxRoi).resize(scaledTrackBoxSize);

   // Search are includes one extra pixel on each side (We're looking for a subpixel offset here)
   MtiRect searchAreaRoi =
   {
      int(dpN.x) - _TrackBoxExtentX - 1,
      int(dpN.y) - _TrackBoxExtentY - 1,
      trackBoxRoi.width + 2,
      trackBoxRoi.height + 2
   };

   if ((searchAreaRoi & _NextFilteredImage.getRoi()) != searchAreaRoi)
   {
      return dpN;
   }

   IppiSize scaledSearchAreaSize = { searchAreaRoi.width * SCALE, searchAreaRoi.height * SCALE };
   auto scaledSearchAreaArray = _NextFilteredImage(searchAreaRoi).resize(scaledSearchAreaSize);

   int bestL1X = 0;
   int bestL1Y = 0;

   int searchAreaBoxExtentX = _TrackBoxExtentX + 1;
   int searchAreaBoxExtentY = _TrackBoxExtentY + 1;
   double DL1[(REFINED_MATCH_SCALE * 2 + 1) * (REFINED_MATCH_SCALE * 2 + 1)];

   // NOTE: This is not exactly the right way to search since we are searching around the
   // center pixel and not the min position.  However, let us assume that not much
   // differs
   double minL1 = 65536.0 * (SCALE * 2 * searchAreaBoxExtentX) * (SCALE * 2 * searchAreaBoxExtentY);
   for (int searchOffsetY = -SCALE; searchOffsetY <= SCALE; ++searchOffsetY)
   {
      for (int searchOffsetX = -SCALE; searchOffsetX <= SCALE; ++searchOffsetX)
      {
         MtiRect scaledSearchBoxRoi =
         {
            searchOffsetX + SCALE,
            searchOffsetY + SCALE,
            scaledTrackBoxSize.width,
            scaledTrackBoxSize.height
         };

         double L1;
         scaledTrackBoxArray.L1NormDiff(scaledSearchAreaArray(scaledSearchBoxRoi), &L1);
         if (L1 < minL1)
         {
            minL1 = L1;
            bestL1X = searchOffsetX;
            bestL1Y = searchOffsetY;
         }

         // Record all L1's for parabolic refinement.
         DL1[(searchOffsetY + SCALE) * (SCALE * 2 + 1) + searchOffsetX + SCALE] = L1;
      }
   }

   double refinedDeltaX = 0.0;
   double refinedDeltaY = 0.0;

   // If best x or y puts us at the edge of the search area, don't do
   // parabolic refinement because we'd go off the edge.
   if (bestL1X == -SCALE || bestL1X == SCALE || bestL1Y == -SCALE || bestL1Y == SCALE)
   {
      doParabolicRefinement = false;
   }

   if (doParabolicRefinement)
   {
      double D[9];
      for (int i = -1; i <= 1; i++)
      {
         for (int j = -1; j <= 1; j++)
         {
            D[(j + 1) * 3 + i + 1] = DL1[(bestL1Y + SCALE + j) * (SCALE * 2 + 1) + bestL1X + SCALE + i];
         }
      }

      EstimateMinFrom9Points(D, refinedDeltaX, refinedDeltaY);

      // QQQ I don't understand why this is clamped to the range -1.5 to 1.5
      // since I imagined it should be -1.0 to 1.0. i.e the minimum should
      // always be found inside the bounds of the 8 surrounding pixels.
      // This seems to imply that the coordinates correspond to the center of
      // the pixel and so the true minimum could be in the part of the surround
      // pixel that is not inside the bounds. Anyhow this probably doesn't
      // matter much, it's basically just a sanity check I guess. But maybe
      // weird shit happens when some of the L1's are equal?
//      MTIassert(refinedDeltaX > -1.0 && refinedDeltaX < 1.0);
//      MTIassert(refinedDeltaY > -1.0 && refinedDeltaY < 1.0);
      refinedDeltaX = std::min<double>(std::max<double>(-1.5, refinedDeltaX), 1.5);
      refinedDeltaY = std::min<double>(std::max<double>(-1.5, refinedDeltaY), 1.5);
   }

   // Apply the fine and finer adjustments to the coarse one.
   float x = dpN.x + ((bestL1X + refinedDeltaX) / (double) SCALE);
   float y = dpN.y + ((bestL1Y + refinedDeltaY) / (double) SCALE);
   tpR = FPOINT(x, y);

   return tpR;
}

namespace
{
#if 0
   Ipp32fArray MakeMonochromeImage(MTI_UINT16 *in, int nCol, int nRow, int nBits)
   {
      Ipp32fArray out(nRow, nCol);
      int i = 0;
      int shift = nBits - 8;
      float maxValue = float((1 << nBits) - 1);
      for (auto outIter = out.begin(); outIter != out.end(); ++outIter)
      {
         *outIter = ((0.32f * in[i + 0]) + (0.64f * in[i + 1]) + (0.04f * in[i + 2])) / maxValue;
         i += 3;
      }

      return out;
   }

   Ipp32fArray MakeMonochromeImageUsingLut(MTI_UINT16 *in, int nCol, int nRow, int nBits, float *weightedLuts[])
   {
      Ipp32fArray out(nRow, nCol);
      float *weightedLutR = weightedLuts[0];
      float *weightedLutG = weightedLuts[1];
      float *weightedLutB = weightedLuts[2];
      int i = 0;
      for (auto outIter = out.begin(); outIter != out.end(); ++outIter)
      {
         // Weights are baked in!
         Ipp32f y = weightedLutR[in[i + 0]] + weightedLutG[in[i + 1]] + weightedLutB[in[i + 2]]);
         *outIter = y;
         i += 3;
      }

      return out;
   }
#endif

   void MakeMonochromeImage(Ipp16uArray inArray, Ipp32fArray outArray, IppiRect roi, int nBits)
   {
      auto fuckingInArray = inArray;
      auto fuckingOutAray = outArray;

      MTIassert(inArray.getComponents() == 3);
      MTIassert(outArray.getComponents() == 1);
      MTIassert(inArray.getRows() == outArray.getRows() && inArray.getCols() == outArray.getCols());

      float normalizer = float(1 << nBits);

      Ipp16uArray inArrayWithRoi = inArray(roi);
      Ipp32fArray outArrayWithRoi = outArray(roi);

      auto inIter = inArrayWithRoi.begin();
      auto outIter = outArrayWithRoi.begin();
      while (outIter != outArrayWithRoi.end())
      {
         Ipp16u r = *(inIter++);
         Ipp16u g = *(inIter++);
         Ipp16u b = *(inIter++);
         Ipp32f y = ((0.32F * r) + (0.64F * g) + (0.04F * b)) / normalizer;
         *outIter++ = y;
      }
   }

   void MakeMonochromeImageUsingLut(Ipp16uArray inArray, Ipp32fArray outArray, IppiRect roi, int nBits, float *weightedLuts[])
   {
      auto fuckingInArray = inArray;
      auto fuckingOutAray = outArray;

      MTIassert(inArray.getComponents() == 3);
      MTIassert(outArray.getComponents() == 1);
      MTIassert(inArray.getRows() == outArray.getRows() && inArray.getCols() == outArray.getCols());

      Ipp16uArray inArrayWithRoi(inArray(roi));
      Ipp32fArray outArrayWithRoi(outArray(roi));

      float *weightedLutR = weightedLuts[0];
      float *weightedLutG = weightedLuts[1];
      float *weightedLutB = weightedLuts[2];

      auto inIter = inArrayWithRoi.begin();
      auto outIter = outArrayWithRoi.begin();
      int inIndex = 0;
      while (outIter != outArrayWithRoi.end())
      {
         Ipp16u rIn = inIter[inIndex];
         Ipp16u gIn = inIter[inIndex + 1];
         Ipp16u bIn = inIter[inIndex + 2];
         float  rOut = weightedLutR[rIn];
         float  gOut = weightedLutG[gIn];
         float  bOut = weightedLutB[bIn];
         float  yOut = rOut + gOut + bOut;
         *outIter++ = yOut;
      }
   }

#if 0  // replaced with IppiBilateralFilter
#define SURFACE_BLUR_RADIUS 3
#define SURFACE_BLUR_THRESHOLD 0.1
   void SurfaceBlur(MTI_UINT16 *image, int width, int height, int depth, MTI_UINT16 *out)
   {
      int threshold = max<int>(1, int((1 << depth) * SURFACE_BLUR_THRESHOLD));
      for (int y = SURFACE_BLUR_RADIUS; y < (height - SURFACE_BLUR_RADIUS); ++y)
      {
         for (int x = SURFACE_BLUR_RADIUS; x < (width - SURFACE_BLUR_RADIUS); ++x)
         {
            int xy = (y * width) + x;

            int radius = 1;
            int count = 9;
            int sum = 0;
            for (int yOffset = -(width * radius);
                  yOffset <= (width * radius);
                  yOffset += width)
            {
               for (int xOffset = -radius; xOffset <= radius; ++xOffset)
               {
                  sum += image[xy + yOffset + xOffset];
               }
            }

            // use regression?
            int centerValue = sum / count;

            // Surface blur - only use surround pixels that are different by less
            // than the threshold.
            sum = 0;
            count = 0;
            radius = SURFACE_BLUR_RADIUS;
            for (int yOffset = -(width * radius);
                  yOffset <= (width * radius);
                  yOffset += width)
            {
               for (int xOffset = -radius; xOffset <= radius; ++xOffset)
               {
                  int value = image[xy + yOffset + xOffset];
                  int diff = centerValue - value;
                  diff = (diff < 0) ? -diff : diff;
                  if (diff <= threshold)
                  {
                     sum += value;
                     ++count;
                  }
               }
            }

            out[xy] = (count > 0) ? (sum / count) : image[xy];
         }
      }
   }
#endif
}
//---------------------------------------------------------------------------

int CTrackingProc::FilterImage(int currentFrame, Ipp16uArray fullRgbImageArray, Ipp32fArray filteredImageArray)
{
   auto fuckingInArray = fullRgbImageArray;
   auto fuckingOutAray = filteredImageArray;

   float *weightedLuts[3] = {_weightedLutR, _weightedLutG, _weightedLutB};
#ifdef DEBUG_VISUALIZATION
   if (!_debugVisualizationArray.isEmpty())
   {
      filteredImageArray.set({128}); //.zero();
   }
#endif

   for (auto &trackingBoxTag : _trackingArray->getTrackingBoxTags())
   {
      MTIassert(_trackingArray->isValidTrackingBoxTag(trackingBoxTag));
      if (!_trackingArray->isValidTrackingBoxTag(trackingBoxTag))
      {
         return TOOL_ERROR_INVALID_TRACKING_BOX_TAG;
      }

      TrackingBox &trackingBox = _trackingArray->getTrackingBoxByTag(trackingBoxTag);

      if (!trackingBox.isValidAtFrame(currentFrame))
      {
         return TOOL_ERROR_CORRUPT_TRACKING_BOX;
      }

      auto currentFrameTrackingPosition = trackingBox.getTrackingPositionAtFrame(currentFrame);
      auto currentFramePoint = currentFrameTrackingPosition.out;

      MtiSize searchAreaPaddedShape =
      {
         _SearchAreaSize.width + 2 * MAX_FILTER_RADIUS,
         _SearchAreaSize.height + 2 * MAX_FILTER_RADIUS,
         1
      };

      MtiRect paddedSearchAreaRoi =
      {
         int(currentFramePoint.x) - _SearchAreaLeftExtent - MAX_FILTER_RADIUS,
         int(currentFramePoint.y) - _SearchAreaTopExtent - MAX_FILTER_RADIUS,
         searchAreaPaddedShape.width,
         searchAreaPaddedShape.height
      };

      // HANDLE CLIPPED SEARCH AREA PROPERLY QQQ TODO

      MtiRect fullRoi = filteredImageArray.getRoi();
      MtiRect clippedSearchAreaRoi = paddedSearchAreaRoi & fullRoi;

//      CHRTimer MAKE_MONOCHROME_TIMER;

      if (_trackingArray->getApplyLutFlag())
      {
         MakeMonochromeImageUsingLut(fullRgbImageArray, filteredImageArray, clippedSearchAreaRoi, _sourceDepth, weightedLuts);
      }
      else
      {
         MakeMonochromeImage(fullRgbImageArray, filteredImageArray, clippedSearchAreaRoi, _sourceDepth);
      }

//      DBTRACE(MAKE_MONOCHROME_TIMER);

      if (_trackingArray->getDegrainFlag() || _trackingArray->getAutoContrastFlag())
      {
//         CHRTimer BILATERAL_FILTER_TIMER;

         Ipp32fArray tempArray(clippedSearchAreaRoi.getSize());

         const int bilateralFilterRadius = 7;
         tempArray <<= _trackingArray->getDegrainFlag()
                        ? filteredImageArray(clippedSearchAreaRoi).applyBilateralFilter(bilateralFilterRadius)
                        : filteredImageArray(clippedSearchAreaRoi);

//         DBTRACE(BILATERAL_FILTER_TIMER);
//         CHRTimer EQUAL_HISTO_TIMER;

         filteredImageArray(clippedSearchAreaRoi)
                   <<= _trackingArray->getAutoContrastFlag()
                        ? tempArray.equalizeHistogram()
                        : tempArray;

//         DBTRACE(EQUAL_HISTO_TIMER);
      }
   }

   return 0;
}
//---------------------------------------------------------------------------

int CTrackingProc::TrackNextFrame(int currentFrame, Ipp16uArray baseRgbImageArray, Ipp16uArray nextRgbImageArray)
{
   auto localFuckingBaseArray = baseRgbImageArray;
   auto localFuckingNextArray = nextRgbImageArray;

   int retVal;
   int channel = _trackingArray->getChannel();
   // MTIassert(channel >= -1 && channel <= 2);    // WHY IS CHANNEL 3 AFTER LOAD QQQ
   if (channel < -1 || channel > 2)
   {
      channel = -1;
   }

   // If this is the first iteration, need to filter the first image.
   if (_NextFilteredImage.isEmpty())
   {
      MtiSize shape {_nPicCol, _nPicRow, 1};
      _BaseFilteredImage.createStorageIfEmpty(shape);
      _NextFilteredImage.createStorageIfEmpty(shape);
      retVal = FilterImage(currentFrame, baseRgbImageArray, _NextFilteredImage);
      MTIassert(retVal == 0);
   }

   // Old "next" image becomes the base. This works because all the tracking
   // boxes are contained in the search areas from the previous frame!
   // Ping pong is completed by using the old base as the next next.
   auto tempArray = _BaseFilteredImage;
   _BaseFilteredImage = _NextFilteredImage;
   _NextFilteredImage = tempArray;

   // Copy the filtered frame into the visualization array if requested.
#ifdef DEBUG_VISUALIZATION
   if (!_debugVisualizationArray.isEmpty())
   {
      CAutoSpinLocker lock(*_debugVisualizationLock);
      _debugVisualizationArray <<= (_BaseFilteredImage * 256.0F);
   }
#endif

//   CHRTimer FILTER_IMAGE_TIMER;

   // Filter the search areas for the next frame.
   retVal = FilterImage(currentFrame, nextRgbImageArray, _NextFilteredImage);
   MTIassert(retVal == 0);

//   DBTRACE(FILTER_IMAGE_TIMER);
//   CHRTimer TRACK_ALL_BOXES_TIMER;

   for (auto tag : _AnchorList)
   {
      int error = TrackOnePointInNextFrame(currentFrame, tag);
      if (error && !retVal)
      {
         retVal = error;
      }
   }

//   DBTRACE(TRACK_ALL_BOXES_TIMER);

   return retVal;
}
//---------------------------------------------------------------------------

int CTrackingProc::TrackOnePointInNextFrame(int currentFrame, int tag)
{
//   CAutoSpinLocker lock(TrackingThreadLock);

   auto nextFrame = currentFrame + 1;

   TRACE_3(errout << "BEFORE:" << endl << _trackingArray->toString());

	MTIassert(_trackingArray->isValidTrackingBoxTag(tag));
	if (!_trackingArray->isValidTrackingBoxTag(tag))
	{
		////throw std::runtime_error("Tried to track box with invalid tag.");
		return TOOL_ERROR_INVALID_TRACKING_BOX_TAG;
	}

	TrackingBox &trackingBox = _trackingArray->getTrackingBoxByTag(tag);

	if (!trackingBox.isValidAtFrame(currentFrame))
	{
		static int previousFailTag = -1;
		if (tag != previousFailTag)
		{
			previousFailTag = tag;
			TRACE_1(errout << "CORRUPT TRACKING BOX ignored: " << trackingBox);
		}

		////throw std::runtime_error("Tried to track from an invalid starting frame.");
		return TOOL_ERROR_CORRUPT_TRACKING_BOX;
	}

   if (trackingBox.isValidAtFrame(nextFrame))
   {
      // It was already tracked!
		return 0;
   }

   CTrackingProc *nonConstThis = const_cast<CTrackingProc *>(this);
   MonoArrayType &TrackBoxData = nonConstThis->_TrackingBoxSubimagesArray[tag]._TrackBoxData;
   MonoArrayType &SearchBoxData = nonConstThis->_TrackingBoxSubimagesArray[tag]._SearchAreaData;

   auto currentFrameTrackingPosition = trackingBox.getTrackingPositionAtFrame(currentFrame);
   auto currentFramePoint = currentFrameTrackingPosition.out;
   FPOINT nextFramePoint;

   {
//      CAutoSpinUnlocker unlock(TrackingThreadLock);
      nextFramePoint = FindBestMatch(currentFramePoint);
   }

   trackingBox.addTrackPoint(nextFramePoint);

	TRACE_3(errout << "AFTER:" << endl << _trackingArray->toString());
	return 0;
}
//---------------------------------------------------------------------------
