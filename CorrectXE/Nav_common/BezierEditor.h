#ifndef BezierEditorH
#define BezierEditorH

#include "machine.h"
#include "vector"

// spline vertex used to build up splines
//
class Vertex {

public:

   Vertex();
   ~Vertex();

   // vertex select states - defines come from DISPLAYER.H
//#define SELECT_NONE      0
//#define SELECT_VTX       1
//#define SELECT_PRE       2
//#define SELECT_NXT       4
//#define SELECT_OPP       8
//#define SELECT_EQU      16
   int select;

   // backward and forward links to other vertices
   Vertex *bwd, *fwd;

   // control point before this vertex
   double xctrlpre, yctrlpre;

   // vertex (attachment point)
   double x, y;

   // control point after  this vertex
   double xctrlnxt, yctrlnxt;

   bool operator!=(const Vertex &rhs) const
   {
	   return !(*this == rhs);
   }

   bool operator==(const Vertex &rhs) const
   {
	   return
//			  (bwd == rhs.bwd) &&
//			  (fwd == rhs.fwd) &&
			  (xctrlpre == rhs.xctrlpre) &&
			  (yctrlpre == rhs.yctrlpre) &&
			  (x == rhs.x) &&
			  (y == rhs.y) &&
			  (xctrlnxt == rhs.xctrlnxt) &&
			  (yctrlnxt == rhs.yctrlnxt);
   }
};

typedef vector<Vertex> VertexList;

class CDisplayer;

class CBezierEditor
{
public:

   CBezierEditor(CDisplayer *);

   CBezierEditor(const CBezierEditor&);

   ~CBezierEditor();

   void setShift(bool);
   void setAlt(bool);
   void setCtrl(bool);
   void setKeyA(bool);
   void setKeyD(bool);

   double mouseDown();
   void setMajorState(int);
   int  getMajorState();
   void mouseMove(int x, int y);
   void mouseUp  ();

   void newSpline();
   bool addPoint(POINT *);

   void drawSplineFrame(unsigned int dashpat = 0xffffffff, bool dontDrawVertices = false);

   void refreshSplineFrame();

   RECT getSimpleSplineExtent();
   bool getCompletedSplineExtent(RECT &);
   BEZIER_POINT *getCompletedSpline();
   int  getVertexCount();
   Vertex *getSplineVertices();
   bool isClosed();
   bool isEmpty();
   bool isFullySelected();
   bool wasSplineJustClosed();
   bool wasAdditionalVertexAdded();
   double getLatestTHit();

   VertexList copyVerticesToVertexList();

   void loadBezier(BEZIER_POINT *newPoints);
   int  selectVertices(RECT &);
   void unselectAllVertices();
   bool areAnyVerticesSelected();
   void deleteAllSelectedVertices();
   void loadBezier(Vertex *newVertices);
   void loadSelectState(Vertex *newVertices);
   bool interpolateVertices(CBezierEditor *bef, CBezierEditor *aft, double combin);

   Vertex *createAdditionalIntermediateVertex(double thit);
   bool calculateNewControlPoint(Vertex *, Vertex *, double *, double *);

   void neutralizeVertex(Vertex *, Vertex *, Vertex *, double thit);
   int neutralizeAllSelectedVertices();

   int eliminateAllSelectedVertices();
   int eliminateAllSelectedVertices(CBezierEditor *tmplt);

private:

   void freeAllVertices();
   Vertex* allocateVertex();
   void freeVertex(Vertex *vtx);
   double sqDistance(double xfm, double yfm, double xto, double yto);
   DRECT extentOfSplineSegment(Vertex *, Vertex *);

   bool boxStraddlesPoint(double x, double y, DRECT &box);
   Vertex * boxStraddlesAttachPoint(Vertex *vtx, DRECT &box);
   Vertex * boxStraddlesControlPoint(Vertex *vtx, DRECT &box);

#define LFTCOD 0x08
#define TOPCOD 0x04
#define RGTCOD 0x02
#define BOTCOD 0x01
   MTI_UINT8 ticTacToeCode(double x, double y, DRECT &box);

   double boxStraddlesEdge(double xi, double yi, double xf, double yf,
                           DRECT &box, MTI_UINT8 tcodsum);
   double boxStraddlesSplineSegment(Vertex *, Vertex *, DRECT &, int *, double *);
   double boxStraddlesSpline(Vertex *, DRECT &, int *, double *);
   void selectAllVertices(Vertex *);
   void unselectAllVertices(Vertex *);
   bool areAnyVerticesSelected(Vertex *);
   DRECT extentAllSelectedVertices(Vertex *);
   void translateAllSelectedVertices(Vertex *, double delx, double dely);
   void deleteAllSelectedVertices(Vertex *);
   Vertex *createIntermediateVertex(Vertex *vtx, double thit,
                                                 double xhit, double yhit);

   void drawSpline(Vertex *, unsigned int dashpat = 0xffffffff, bool dontDrawVertices = false);

private:

   CDisplayer *displayer;

   // mouse client coords
   int mouseXClient,
       mouseYClient;

   // mouse frame coords
   double mouseXFrame,
          mouseYFrame;

   // mouse deltas
   double deltaMouseXFrame,
          deltaMouseYFrame;

#define MOD_KEY_NONE  0
#define MOD_KEY_SHIFT 1
#define MOD_KEY_ALT   2
#define MOD_KEY_CTRL  4
   int modKeys;

/*
#define MAX_VERTICES 255
   Vertex *vertexArray;
   Vertex *vertexFree;
*/

   // vbles for the growing spline
   int vertexCount;
   Vertex *splineHead;
   Vertex *splineTail;
   Vertex *selectedVertex;
   bool splineIsClosed;

#define MAJORSTATE_TRACK              0
#define MAJORSTATE_FIND_CTRL          1
#define MAJORSTATE_DRAG_OPP_EQ        2
#define MAJORSTATE_DRAG_ATTACH_POINTS 3
#define MAJORSTATE_DRAG_CONTROL_POINT 4
   int majorState;

   int distSqToPrevious;
   double distToOpposingControlPoint;

   bool splineJustClosed;

   bool additionalVertexAdded;
   double latestTHit;
};

#endif
