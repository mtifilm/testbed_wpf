#ifndef TrackingProc16H
#define TrackingProc16H

#include "bthread.h"
#include "HRTimer.h"
#include "IniFile.h"
#include "MTIio.h"
#include "PixelRegions.h"
#include "RegionOfInterest.h"
#include "RepairShift.h"
#include "ToolObject.h"
#include "TrackingBox.h"


//////////////////////////////////////////////////////////////////////
// Forward Declarations
//
class CTrackingTool;
struct STrackingThreadParams;

typedef MTI_UINT16 Pixel;
typedef Pixel * Image;

//////////////////////////////////////////////////////////////////////
//
class CTrackingProc : public CToolProcessor
{
public:
   CTrackingProc(int newToolNumber, const string &newToolName,
                   CToolSystemInterface *newSystemAPI,
                   const bool *newEmergencyStopFlagPtr,
                   IToolProgressMonitor *newToolProgressMonitor);
   virtual ~CTrackingProc();

   CHRTimer HRT;
   int SetIOConfig(CToolIOConfig *toolIOConfig);
   int SetFrameRange(CToolFrameRange *toolFrameRange);

   void *_TrackingThreadCriticalSection;

private:
   // Tool's functions called from within context of the processing loop.
   int SetParameters(CToolParameters *toolParams);
   int BeginProcessing(SToolProcessingData &procData);
   int EndProcessing(SToolProcessingData &procData);
   int BeginIteration(SToolProcessingData &procData);
   int EndIteration(SToolProcessingData &procData);
   int DoProcess(SToolProcessingData &procData);
   int GetIterationCount(SToolProcessingData &procData);

private:
   int _inFrameIndex;
   int _outFrameIndex;
   int _iterationCount;

   // Need per-point allocations of subimages for multithreading
   struct TrackingBoxSubimages
    {
      Pixel *_TrackBoxData;
      Pixel *_SearchBoxData;

      TrackingBoxSubimages() : _TrackBoxData(NULL), _SearchBoxData(NULL) {};
      ~TrackingBoxSubimages() { delete(_TrackBoxData); delete(_SearchBoxData); };
    };

   map<int, TrackingBoxSubimages> _TrackingBoxSubimagesArray;
   map<int, STrackingThreadParams> _TrackingThreadParametersArray;
   CCountingWait _TrackingThreadsDoneSync;

   // Temporary storage for tracking etc
   int _TrackBoxExtentX;
   int _TrackBoxExtentY;
   int _TrackBoxSizeX;
   int _TrackBoxSizeY;
   int _TrackBoxPoints;

   int _SearchBoxLeftExtent;
   int _SearchBoxRightExtent;
   int _SearchBoxTopExtent;
   int _SearchBoxBottomExtent;
   int _SearchBoxSizeX;
   int _SearchBoxSizeY;
   int _SearchBoxPoints;

   vector<void *> _bthreadStructs;

   TagList _AnchorList;
   
   TrackingBoxManager *_trkPtsEditor;
   TrackingBoxSet *_trackingArray;    // private copy of params
   CTrackingTool *_trackingToolObject;  // pointer to CTrackingTool object [EVIL!!!!]

   // Tracking functions, to be moved later
   MTI_UINT16 *_BaseOrigImage;
   MTI_UINT16 *_BaseMonoImage;
   MTI_UINT16 *_NextOrigImage;
   MTI_UINT16 *_NextMonoImage;
   MTI_UINT16 *_MonoImages[2];
   MTI_UINT16 *_tempMonoImage;

   int _MonoImageIndex;

   int nPicCol;
   int nPicRow;
   int _RGB;
   int _sourceDepth = 16;
   MTI_UINT8 _currentDisplayLut8[256 * 3];
   MTI_UINT16 _currentDisplayLut16[65536 * 3];

   FPOINT FindBestMatch(FPOINT fp, Pixel *trackBoxData, Pixel *searchBoxData) const;
   FPOINT FindBestMatchX(FPOINT fp, Pixel *trackBoxData, Pixel *searchBoxData) const;
   FPOINT FindBestMatchY(FPOINT fp, Pixel *trackBoxData, Pixel *searchBoxData) const;

   typedef void (* CreateSubPixelMatrixT)(MTI_UINT16 *pIn, int nCol,
                                          double x, double y,
                                          int Nx, int Ny,
                                          MTI_UINT16 *pOut);
#define MAX_REFINED_MATCH_SCALE 10
   FPOINT FindBestRefinedMatchNx(int SCALE, CreateSubPixelMatrixT CreateSubPixelMatrixNx,
                                      FPOINT dpB, FPOINT dpN, bool doParabolicRefinement,
                                      Pixel *trackBoxData, Pixel *searchBoxData) const;

   FPOINT FindBestRefinedMatch2x(FPOINT dpB, FPOINT dpN, bool doParabolicRefinement, Pixel *trackBoxData, Pixel *searchBoxData) const;
   FPOINT FindBestRefinedMatch4x(FPOINT dpB, FPOINT dpN, bool doParabolicRefinement, Pixel *trackBoxData, Pixel *searchBoxData) const;
   FPOINT FindBestRefinedMatch5x(FPOINT dpB, FPOINT dpN, bool doParabolicRefinement, Pixel *trackBoxData, Pixel *searchBoxData) const;
   FPOINT FindBestRefinedMatch6x(FPOINT dpB, FPOINT dpN, bool doParabolicRefinement, Pixel *trackBoxData, Pixel *searchBoxData) const;
   FPOINT FindBestRefinedMatch7x(FPOINT dpB, FPOINT dpN, bool doParabolicRefinement, Pixel *trackBoxData, Pixel *searchBoxData) const;
   FPOINT FindBestRefinedMatch8x(FPOINT dpB, FPOINT dpN, bool doParabolicRefinement, Pixel *trackBoxData, Pixel *searchBoxData) const;
   FPOINT FindBestRefinedMatch10x(FPOINT dpB, FPOINT dpN, bool doParabolicRefinement, Pixel *trackBoxData, Pixel *searchBoxData) const;

   int TrackNextFrame(int CurrentFrame);
   static void TrackingThreadController(void *vpAppData, void *vpReserved);
   int TrackOnePointInNextFrame(int CurrentFrame, int iTag);

};

//////////////////////////////////////////////////////////////////////
// STrackingThreadParams:
//////////////////////////////////////////////////////////////////////

//  This structure is designed to pass information to the thread
struct STrackingThreadParams
  {
      STrackingThreadParams():
          doneFlag(false),
          doneSync(NULL),
          trackingProc(NULL),
          iTag(0),
          currentFrame(0)
      {};

      int  iTag;
      CTrackingProc *trackingProc;   // instance pointer
      int  currentFrame;
      CCountingWait startSync;
      CCountingWait *doneSync;    // Shared with other threads (gatherer)
      bool doneFlag;
  };

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////

#endif // !defined(TrackingTOOL_H)
