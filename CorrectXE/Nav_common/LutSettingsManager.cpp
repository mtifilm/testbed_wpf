//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//
//                    LUT SETTINGS MANAGER
//
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

#include "LutSettingsManager.h"

#include "BinManager.h"
#include "Displayer.h"
#include "MainWindowUnit.h" // UGLY! UGLY! UGLY!!!!
#include "MTIstringstream.h"
#include "NavMainCommon.h"

#include <ctype.h>
#include <algorithm>


#define LutListSectionName "LutList"
#define ShortcutsSectionName "Shortcuts"

//--------------------------------------------------------------------

CLutSettingsManager::CLutSettingsManager()
{
   selectedLut = LUTSEL_CLIP;

   // Load the User Lut Settings
   readUserLutListFromIniFile();
   if (userLutSettingsList.empty())
   {
      // none found - see if there are any legacy ones to glom onto
      readLegacyUserLutList();
   }
   LutSettingsChange.Notify();
}
//---------------------------------------------------------------------------

void CLutSettingsManager::LoadAndApplyAllNewClipSettings(ClipSharedPtr &newClip)
{
   string iniFileName;
   if (newClip != nullptr)
   {
      cachedClipPath = newClip->getClipPathName();

      // Load "Project" settings for this clip
      iniFileName = findProjectLutIniFile();
   }

   if (iniFileName.empty())
   {
      cachedClipPath = "";
      projectLutSettings.InitToDefaults();
      clipLutSettings.InitToDefaults();
   }
   else
   {
      projectLutSettings.ReadFromIniFile(iniFileName, ""); // no sect prefix

      // "Clip" is a little trickier because if the DisplayLutSettings
      // section doesn't exist in the desktop.ini file, we want to
      // look for "legacy" settings that are elsewhere in the same file
      iniFileName = findClipLutIniFile();

      CIniFile *ini = CreateIniFile(iniFileName);
      if (ini != nullptr)
      {
         if (clipLutSettings.DoesDisplayLutSettingsSectionExist(ini, ""))
         {
            clipLutSettings.ReadFromIniFile(ini, "");
         }
         else
         {
            CBinManager bm;
            clipLutSettings.ImportLegacyClipCustomLutSettings(
                        ini,
                        bm.getDesktopSectionUniqueName(),
                        newClip->getImageFormat(VIDEO_SELECT_NORMAL),
                        mainWindow->GetGammaPreference());
         }
         DeleteIniFile(ini);
      }
      else
      {
         clipLutSettings.InitToDefaults();
      }
   }

   applyAppropriateSettings();
}
//--------------------------------------------------------------------

void CLutSettingsManager::SelectLut(ELutSelection newSelection,
                          const string *userLutName)
{
   selectedLut = newSelection;
   MTIassert(newSelection != LUTSEL_USER || userLutName != nullptr);
   if (userLutName == nullptr)
      selectedUserLutName = "";
   else
      selectedUserLutName = *userLutName;

   applyAppropriateSettings();

   LutSettingsChange.Notify();
}
//--------------------------------------------------------------------

ELutSelection CLutSettingsManager::GetLutSelection(string &userLutName)
{
   userLutName = selectedUserLutName;
   
   return selectedLut;
}

//--------------------------------------------------------------------

void CLutSettingsManager::GetUserLutSettingsNames(StringList &listOfNames)
{
   // Returns a list of names that are sorted so that the luts that have
   // shortcuts come first (sorted 1-2-3-4-5-6-7-8-9-0) followed by
   // the non-shortcut names which are sorted case independently

   listOfNames.clear();
   TLutSettingsListIter iter;

   userLutSettingsList.sort();    // QQQ sort only when necessary!

   for (iter = userLutSettingsList.begin(); iter != userLutSettingsList.end(); ++iter)
   {
      listOfNames.push_back((*iter).name);
   }
}
//---------------------------------------------------------------------------

CDisplayLutSettings CLutSettingsManager::GetProjectLutSettings()
{
   return projectLutSettings;
}
//---------------------------------------------------------------------------

CDisplayLutSettings CLutSettingsManager::GetClipLutSettings()
{
   return clipLutSettings;
}
//---------------------------------------------------------------------------

CDisplayLutSettings CLutSettingsManager::GetUserLutSettings(const string &lutName)
{
   CDisplayLutSettings userLutSettings;

   TLutSettingsListIter iter = findUserLutSettingsListEntry(lutName);
   MTIassert(iter != userLutSettingsList.end());
   if (iter != userLutSettingsList.end())
   {
      userLutSettings = (*iter).settings;
   }

   return userLutSettings;
}
//---------------------------------------------------------------------------

int CLutSettingsManager::SetProjectLutSettings(
                                      const CDisplayLutSettings &newSettings)
{
   projectLutSettings = newSettings;
   projectLutSettings.SaveToIniFile(findProjectLutIniFile(), "");

   LutSettingsChange.Notify();

   return 0;
}
//---------------------------------------------------------------------------

int CLutSettingsManager::SetClipLutSettings(
                                      const CDisplayLutSettings &newSettings)
{
   clipLutSettings = newSettings;
   clipLutSettings.SaveToIniFile(findClipLutIniFile(), "");

   LutSettingsChange.Notify();

   return 0;
}
//---------------------------------------------------------------------------

int CLutSettingsManager::SetUserLutSettings(
                                        const string &lutName,
                                        const CDisplayLutSettings &newSettings)
{
   TLutSettingsListIter iter = findUserLutSettingsListEntry(lutName);

   if (iter != userLutSettingsList.end())
   {
      (*iter).settings = newSettings;
   }
   else
   {
      // not found - create new one
      SLutSettingsListEntry newEntry;
      newEntry.name = lutName;
      newEntry.settings = newSettings;
      userLutSettingsList.push_back(newEntry);
   }

   saveUserLutListToIniFile();
   
   LutSettingsChange.Notify();

   return 0;
}
//---------------------------------------------------------------------------

char CLutSettingsManager::GetUserLutShortcut(const string &lutName)
{
   char retVal = ' ';  // means "none"
   TLutSettingsListIter iter = findUserLutSettingsListEntry(lutName);
   MTIassert(iter != userLutSettingsList.end());

   if (iter != userLutSettingsList.end())
   {
      retVal = (*iter).shortcut;
   }
   
   return retVal;
}
//---------------------------------------------------------------------------

int CLutSettingsManager::SetUserLutShortcut(const string &lutName,
                                                   char newShortcut)
{
   TLutSettingsListIter iter;
   for (iter = userLutSettingsList.begin(); iter != userLutSettingsList.end(); ++iter)
   {
      if ((*iter).name == lutName)
      {
         (*iter).shortcut = newShortcut;
      }
      else if ((*iter).shortcut == newShortcut)
      {
         (*iter).shortcut = ' ';
      }
   }
   saveUserLutListToIniFile();      // beneficial side effect: sorts list

   LutSettingsChange.Notify();

   // Probably useless, but return non-zero if not found -- should assert
   if (iter == userLutSettingsList.end())
      return -1;

   return 0;
}
//---------------------------------------------------------------------------

int CLutSettingsManager::RemoveUserLutSettings(const string &lutName)
{
   TLutSettingsListIter iter = findUserLutSettingsListEntry(lutName);
   MTIassert(iter != userLutSettingsList.end());

   if (iter != userLutSettingsList.end())
   {
      userLutSettingsList.erase(iter);
      saveUserLutListToIniFile();
   }

   // If we deleted the selected LUT, select the [CLIP] LUT instead
   if (selectedLut == LUTSEL_USER && lutName == selectedUserLutName)
   {
      selectedLut = LUTSEL_CLIP;
      LutSettingsChange.Notify();
   }

   return 0;
}
//---------------------------------------------------------------------------

void CLutSettingsManager::ApplyProjectLutSettings()
{
   selectedLut = LUTSEL_PROJECT;

   CDisplayer *displayer = mainWindow->GetDisplayer();
   displayer->applyLUTSettings(projectLutSettings);

   LutSettingsChange.Notify();
}
//---------------------------------------------------------------------------

void CLutSettingsManager::ApplyClipLutSettings()
{
   selectedLut = LUTSEL_CLIP;

   CDisplayer *displayer = mainWindow->GetDisplayer();
   displayer->applyLUTSettings(clipLutSettings);

   LutSettingsChange.Notify();
}
//---------------------------------------------------------------------------

void CLutSettingsManager::ApplyUserLutSettings(const string &lutName)
{
   TLutSettingsListIter iter = findUserLutSettingsListEntry(lutName);
   MTIassert(iter != userLutSettingsList.end());

   if (iter == userLutSettingsList.end())
   {
      ApplyClipLutSettings();
   }
   else
   {
      selectedLut = LUTSEL_USER;
      selectedUserLutName = lutName;

      CDisplayer *displayer = mainWindow->GetDisplayer();
      displayer->applyLUTSettings((*iter).settings);

      LutSettingsChange.Notify();
   }
}
//---------------------------------------------------------------------------

void CLutSettingsManager::TakeSnapshot()
{
   Snapshot.projectLutSettings  = projectLutSettings;
   Snapshot.clipLutSettings     = clipLutSettings;
   Snapshot.userLutSettingsList = userLutSettingsList;
   Snapshot.selectedLut         = selectedLut;
   Snapshot.selectedUserLutName = selectedUserLutName;
}
//---------------------------------------------------------------------------

void CLutSettingsManager::RevertToSnapshot()
{
   projectLutSettings  = Snapshot.projectLutSettings;
   clipLutSettings     = Snapshot.clipLutSettings;
   userLutSettingsList = Snapshot.userLutSettingsList;
   SelectLut(Snapshot.selectedLut, &Snapshot.selectedUserLutName);
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

void CLutSettingsManager::applyAppropriateSettings()
{
   switch (selectedLut)
   {
      default:
         SelectLut(LUTSEL_CLIP);
         ApplyClipLutSettings();
         break;

      case LUTSEL_CLIP:
         ApplyClipLutSettings();
         break;

      case LUTSEL_PROJECT:
         ApplyProjectLutSettings();
         break;

      case LUTSEL_USER:
         ApplyUserLutSettings(selectedUserLutName);
         break;
   }
}
//---------------------------------------------------------------------------

TLutSettingsListIter CLutSettingsManager::findUserLutSettingsListEntry(
                                              const string &lutName)
{
   TLutSettingsListIter iter;

   for (iter = userLutSettingsList.begin();
        iter != userLutSettingsList.end() && (*iter).name != lutName;
        ++iter)
   {
      // Do nothing else, just looking for the name
   }
   return iter;
}
//---------------------------------------------------------------------------

string CLutSettingsManager::findProjectLutIniFile()
{
   // The "Project" is defined as the bin one level down from the root bin
   // that is a parent of the clip

   // Find the root bin directory
   StringList roots;
   CPMPGetBinRoots(&roots);
   string rootPath = RemoveDirSeparator(roots[0]);    // there is only one

   // Walk up the bin tree path until we hit the root, then the previous
   // bin was the "Project" bin
	CBinManager binManager;
	string projectPath = rootPath;  // clips in the root have root as "project"
	string nextBinPath = RemoveDirSeparator(binManager.getRealBinPath(cachedClipPath));
	while (nextBinPath != rootPath)
	{
		projectPath = nextBinPath;
		nextBinPath = RemoveDirSeparator(GetFilePath(nextBinPath));

		// This is a hack put in so that if root path never matches
      // we terminate this loop
		if (projectPath == nextBinPath)
		{
			return "";
		}
	}

   // For consistency with clips, we keep LUT settings in bin's desktop.ini
   string iniFilePath = binManager.makeClipDesktopIniFileName(projectPath);

   return iniFilePath;
}
//---------------------------------------------------------------------------

string CLutSettingsManager::findClipLutIniFile()
{
   // We put clip LUT settings in the clip's 'desktop.ini' file
   // (for legacy reasons... should we reconsider?)

   CBinManager binManager;
   string iniFilePath = binManager.makeClipDesktopIniFileName(cachedClipPath);

   return iniFilePath;
}
//---------------------------------------------------------------------------

string CLutSettingsManager::findUserLutIniFile()
{
   // We put User Lut settings in .cpmprc\UserLuts.ini

   string iniFilePath = "$(CPMP_USER_DIR)UserLuts.ini";

   ExpandEnviornmentString(iniFilePath);

   return iniFilePath;
}
//---------------------------------------------------------------------------
namespace {

   string makeLutKey(int lutNumber)
   {
      MTIostringstream os;
      os << "Lut" << setw(2) << setfill('0') << ++lutNumber;
      return os.str();
   }
}

//---------------------------------------------------------------------------

int CLutSettingsManager::readUserLutListFromIniFile()
{
   userLutSettingsList.clear();

   string iniFileName = findUserLutIniFile();

   CIniFile *ini = CreateIniFile(iniFileName, true);  // true=R/O
   if (ini == nullptr)
      return -1;

   int lutNumber = 0;
   bool keepGoing = true;
   do
   {
      string lutKey = makeLutKey(++lutNumber);
      string lutName = ini->ReadString(LutListSectionName, lutKey, "[]");
      if (lutName == "[]")
      {
         keepGoing = false;
      }
      else
      {
         SLutSettingsListEntry listEntry;

         listEntry.name = lutName;
         listEntry.settings.ReadFromIniFile(ini, lutKey);

         string shortcutString = ini->ReadString(ShortcutsSectionName, lutKey, " ");
         listEntry.shortcut = shortcutString[0];

         userLutSettingsList.push_back(listEntry);
      }
   } while (keepGoing);

   DeleteIniFile(ini);

   return 0;
}
//---------------------------------------------------------------------------

int CLutSettingsManager::saveUserLutListToIniFile()
{
   userLutSettingsList.sort();

   string iniFileName = findUserLutIniFile();

   CIniFile *ini = CreateIniFile(iniFileName, false);  // false=R/W
   if (ini == nullptr)
      return -1;

   int lutNumber = 0;
   bool keepGoing = true;
   do
   {
      ++lutNumber;
      string lutKey = makeLutKey(lutNumber);
      string lutName = ini->ReadString(LutListSectionName, lutKey, "[]");
      
      if (lutName != "[]")
         CDisplayLutSettings::EraseIniSection(ini, lutKey);
      else
         keepGoing = false;

   } while (keepGoing);

   ini->EraseSection(LutListSectionName);
   ini->EraseSection(ShortcutsSectionName);

   lutNumber = 0;
   TLutSettingsListIter iter;

   for (iter = userLutSettingsList.begin(); iter != userLutSettingsList.end(); ++iter)
   {
      ++lutNumber;
      string lutKey = makeLutKey(lutNumber);
      SLutSettingsListEntry &lutSettingsEntry = *iter;
      
      ini->WriteString(LutListSectionName, lutKey, lutSettingsEntry.name);

      if (lutSettingsEntry.shortcut != ' ')
      {
         char stupidCString[2];
         stupidCString[0] = lutSettingsEntry.shortcut;
         stupidCString[1] = '\0';
         ini->WriteString(ShortcutsSectionName, lutKey, stupidCString);
      }
      
      lutSettingsEntry.settings.SaveToIniFile(ini, lutKey);
   }

   DeleteIniFile(ini);
   return 0;
}
//---------------------------------------------------------------------------

int CLutSettingsManager::readLegacyUserLutList()
{
  userLutSettingsList.clear();

  // Read all the lines from the legacy lut list file
  CIniFile *ini = CreateIniFile("$(CPMP_USER_DIR)LUTList.ini");
  StringList strlLuts;
  ini->ReadSection("Luts", &strlLuts);

  // We assign the 10 shortcuts 1st come first serve, as in olden days -
  // note that the 10th lut gets '0' as ashortcut, which we will now
  // show first instead of last but "c'est la vie"....
  int shortcutOrdinal = 1;
  char shortcut = '1';

  for (StringList::iterator iter = strlLuts.begin();
        iter != strlLuts.end();
        ++iter)
  {
     string strTemp = ini->ReadString("Luts", *iter, "");

     // values read look like "[lut_name]lut_file_path"
     int endIndex = strTemp.find("]");

     // get name from between the []s
     string legacyLutName = strTemp.substr(1, endIndex - 1);
     // rest of the line after the ] is file path
     string legacyLutFile = strTemp.substr(endIndex+1);

     SLutSettingsListEntry listEntry;
     listEntry.name = legacyLutName;

     // Concoct a shortcut key!
     if (shortcutOrdinal == 10)
        shortcut = '0';
     else if (shortcutOrdinal > 10)
        shortcut = ' ';
     listEntry.shortcut = shortcut;
     ++shortcut;
     ++shortcutOrdinal;

     // Suck in the settings from the legacy file
     listEntry.settings.ImportSettingsFromWackyLutFormat(legacyLutFile);

     // stupid export format doesn't do defaults for display stuff
     MTI_UINT16 minValues[3];
     MTI_UINT16 maxValues[3];

     listEntry.settings.GetDisplayRange(minValues, maxValues);
     if (minValues[0] ==   0 && minValues[1] ==   0 && minValues[2] ==   0 &&
         maxValues[0] == 255 && maxValues[1] == 255 && maxValues[2] == 255 )
     {
        listEntry.settings.UseDefaultDisplayRange(true);
     }
     // Don't bother defaulting GAMMA


     // Add it to the "User LUT" list
     userLutSettingsList.push_back(listEntry);
  }
  
  DeleteIniFile(ini);

  return 0;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

SLutSettingsListEntry::SLutSettingsListEntry()
{
   shortcut = ' ';
}

int SLutSettingsListEntry::getOrdinal() const
{
   switch (shortcut)
   {
      case '0':
         return 10;
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
         return shortcut - '0';
      default:
         return 11;
   }
}

bool SLutSettingsListEntry::operator<(const SLutSettingsListEntry &rhs) const
{
   int myOrdinal = getOrdinal();
   int rhsOrdinal = rhs.getOrdinal();
   if (myOrdinal == rhsOrdinal)
   {
      string lhsName = name;
      string rhsName = rhs.name;
      transform(lhsName.begin(), lhsName.end(), lhsName.begin(), tolower);
      transform(rhsName.begin(), rhsName.end(), rhsName.begin(), tolower);


      return (lhsName < rhsName);
   }
   return (myOrdinal < rhsOrdinal);
}
//---------------------------------------------------------------------------


