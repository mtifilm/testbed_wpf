#ifndef TrackingProc8H
#define TrackingProc8H

#include "bthread.h"
#include "HRTimer.h"
#include "IniFile.h"
#include "Ippheaders.h"
#include "MTIio.h"
#include "PixelRegions.h"
#include "RegionOfInterest.h"
#include "RepairShift.h"
#include "ToolObject.h"
#include "TrackingBox.h"


//////////////////////////////////////////////////////////////////////
// Forward Declarations
//
class CTrackingTool;

typedef MTI_UINT8 MonoPixelType;
typedef Ipp8uArray MonoArrayType ;

//////////////////////////////////////////////////////////////////////
//
class CTrackingProc : public CToolProcessor
{
public:
   CTrackingProc(int newToolNumber, const string &newToolName,
                   CToolSystemInterface *newSystemAPI,
                   const bool *newEmergencyStopFlagPtr,
                   IToolProgressMonitor *newToolProgressMonitor);
   virtual ~CTrackingProc();

   CHRTimer HRT;
   int SetIOConfig(CToolIOConfig *toolIOConfig);
   int SetFrameRange(CToolFrameRange *toolFrameRange);
   void SetDebugParameters(Ipp8uArray &debugVisualizationArray, CSpinLock *debugVisualizationLock);

   void *_TrackingThreadCriticalSection;

private:
   // Tool's functions called from within context of the processing loop.
   int SetParameters(CToolParameters *toolParams);
   int BeginProcessing(SToolProcessingData &procData);
   int EndProcessing(SToolProcessingData &procData);
   int BeginIteration(SToolProcessingData &procData);
   int EndIteration(SToolProcessingData &procData);
   int DoProcess(SToolProcessingData &procData);
   int GetIterationCount(SToolProcessingData &procData);

private:
   int _inFrameIndex;
   int _outFrameIndex;
   int _iterationCount;

   // Need per-point allocations of subimages for multithreading
   struct TrackingBoxSubimages
    {
      MonoArrayType  _TrackBoxData;
      MonoArrayType  _SearchAreaData;
    };

   map<int, TrackingBoxSubimages> _TrackingBoxSubimagesArray;
   CCountingWait _TrackingThreadsDoneSync;

   // Temporary storage for tracking etc
   int _TrackBoxExtentX;
   int _TrackBoxExtentY;
   MtiSize _TrackBoxSize;

   int _SearchAreaLeftExtent;
   int _SearchAreaRightExtent;
   int _SearchAreaTopExtent;
   int _SearchAreaBottomExtent;
   MtiSize _SearchAreaSize;

   vector<void *> _bthreadStructs;

   TagList _AnchorList;
   
   TrackingBoxManager *_trkPtsEditor;
   TrackingBoxSet *_trackingArray;    // private copy of params
   CTrackingTool *_trackingToolObject;  // pointer to CTrackingTool object [EVIL!!!!]

   MonoArrayType  _BaseFilteredImage;
   MonoArrayType  _NextFilteredImage;

   int _nPicCol;
   int _nPicRow;
   int _RGB;
   int _sourceDepth = 16;
   MTI_UINT8 _currentDisplayLut8[256 * 3];
   MTI_UINT8 _weightedLut8[256 * 3];

   int FilterImage(int currentFrame, Ipp16uArray fullRgbImageArray, Ipp8uArray filteredImageArray);
   FPOINT FindBestMatch(FPOINT fp) const;
   FPOINT FindBestRefinedMatch(int SCALE, FPOINT dpB, FPOINT dpN, bool doParabolicRefinement) const;

   typedef void (* CreateSubPixelMatrixT)(MTI_UINT16 *pIn, int nCol,
                                          double x, double y,
                                          int Nx, int Ny,
                                          MTI_UINT16 *pOut);

   int TrackNextFrame(int CurrentFrame, Ipp16uArray baseRgbImageArray, Ipp16uArray nextRgbImageArray);
   int TrackOnePointInNextFrame(int CurrentFrame, int iTag);

   // DEBUGGING
   Ipp8uArray _debugVisualizationArray;
   CSpinLock *_debugVisualizationLock = nullptr;
};

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////

#endif // !defined(TrackingTOOL_H)
