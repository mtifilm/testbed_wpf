// NavigatorTool.cpp: implementation of the CNavigatorTool class.
//
// The CNavigatorTool class is derived from the CToolObject class.
// The CNavigatorTool's purposes is to 1) support keyboard mapping
// and 2) be the primary location for the Navigator's user-interface
// logic.  To the extent possible, CNavigatorTool is platform-independent.
//
//////////////////////////////////////////////////////////////////////

#include <stdio.h>   // TTT

#include "NavigatorTool.h"

#include "BaseToolUnit.h"
#include "BookmarkEventViewerUnit.h"
#include "Displayer.h"
#include "ImageFormat3.h"
#include "IniFile.h"
#include "MainWindowUnit.h"
#include "MaskTool.h"
#include "NavMainCommon.h"
#include "Player.h"
#include "ReticleTool.h"
#include "ToolCommand.h"
#include "ToolCommandTable.h"
#include "ToolManager.h"
#include "ToolSystemInterface.h"
#include "ToolUserInputMap.h"
#include "GOVTool.h"

//////////////////////////////////////////////////////////////////////
// Static Variables
//////////////////////////////////////////////////////////////////////

CMaskTool *CNavigatorTool::maskTool = 0;
CReticleTool *CNavigatorTool::reticleTool = 0;

// -------------------------------------------------------------------------

static MTI_UINT16 navigatorToolNumber = NAVIGATOR_TOOL_NUMBER;

// -------------------------------------------------------------------------
// Navigator Default Keyboard and Mouse Button Configuration

static CUserInputConfiguration::SConfigItem navDefaultConfigItems[] =
{
    // Navigator Command                    Modifiers      Action
    // + Key
    // ----------------------------------------------------------------
    // Basic Navigator Commands
	 {NAV_CMD_PLAY_REV, " X            KeyDown   "},
	 {NAV_CMD_STOP, " C            KeyDown   "},
	 {NAV_CMD_PLAY, " V            KeyDown   "},
	 {NAV_CMD_JOG_REV_1_FRAME, " S            KeyDown   "},
	 {NAV_CMD_JOG_FWD_1_FRAME, " F            KeyDown   "},
	 {NAV_CMD_PREV_EVENT, " Shift+S      KeyDown   "},
    {NAV_CMD_NEXT_EVENT, " Shift+F      KeyDown   "},
    // { NAV_CMD_PLAY_FWD_CONTINUOUS,         " Ctrl+Shift+F KeyDown   " },
    // { NAV_CMD_PLAY_FWD_STOP,               " Ctrl+Shift+F KeyUp     " },
    {NAV_CMD_PLAY_FWD_STOP, " F            KeyUp     "},
    // { NAV_CMD_PLAY_REV_CONTINUOUS,         " Ctrl+Shift+S KeyDown   " },
    // { NAV_CMD_PLAY_REV_STOP,               " Ctrl+Shift+S KeyUp     " },
    {NAV_CMD_PLAY_REV_STOP, " S            KeyUp     "},

    // Basic Zooming Display Commands
    // { NAV_CMD_ZOOM_ALL,                    " Z            KeyDown   " },
	 {NAV_CMD_ENTER_MOUSE_ZOOM_MODE, " Z            KeyDown   "},
	 {NAV_CMD_EXIT_MOUSE_ZOOM_MODE, " Z            KeyUp     "},
	 {NAV_CMD_ZOOM_IN, " Y            KeyDown   "},
	 {NAV_CMD_ZOOM_OUT, " Shift+Y      KeyDown   "},
	 {NAV_CMD_PANSTART, " U            KeyDown   "},
	 {NAV_CMD_PANSTOP, " U            KeyUp     "},
	 {NAV_CMD_JOG_PAN_MODE, " Shift+U      KeyDown   "},

    // "Go To" Commands
	 {NAV_CMD_GOTO_IN, " Home         KeyDown   "},
	 {NAV_CMD_GOTO_OUT, " End          KeyDown   "},
	 {NAV_CMD_GOTO_MARK_IN, " Shift+E      KeyDown   "},
	 {NAV_CMD_GOTO_MARK_OUT_MINUS_ONE, " Shift+R      KeyDown   "},

    // Loop & Play Commands
	 {NAV_CMD_LOOP_MARKS, " Shift+V      KeyDown   "},
	 {NAV_CMD_LOOP_AROUND_CURRENT_FRAME, " Shift+X      KeyDown   "},

    // Subclip Commands
	 {NAV_CMD_MARK_IN, " E            KeyDown   "},
	 {NAV_CMD_SET_MARK_IN, " Alt+[        KeyDown   "},
	 {NAV_CMD_REMOVE_MARK_IN, " {            KeyDown   "},
	 {NAV_CMD_REMOVE_MARK_IN, " Ctrl+E       KeyDown   "},
	 {NAV_CMD_MARK_OUT_INCLUSIVE, " Ctrl+]       KeyDown   "},
	 {NAV_CMD_MARK_OUT_EXCLUSIVE, " R            KeyDown   "},
	 {NAV_CMD_MARK_ALL, " Shift+H      KeyDown   "},
	 {NAV_CMD_SET_MARK_OUT, " Alt+]        KeyDown   "},
	 {NAV_CMD_REMOVE_MARK_OUT, " }            KeyDown   "},
	 {NAV_CMD_REMOVE_MARK_OUT, " Ctrl+R       KeyDown   "},
	 {NAV_CMD_EXPAND, " '            KeyDown   "},
	 {NAV_CMD_UNEXPAND, " \"           KeyDown   "},

    // Switching Clip Commands
	 {NAV_CMD_NEXT_CLIP, " Shift+PageDown KeyDown   "},
	 {NAV_CMD_PREVIOUS_CLIP, " Shift+PageUp   KeyDown   "},
    {NAV_CMD_TOGGLE_LAST_TWO_CLIPS, " PageUp         KeyDown   "},
    {NAV_CMD_TOGGLE_LAST_TWO_CLIPS, " PageDown       KeyDown   "},
    {NAV_CMD_NEXT_VERSION_CLIP, " Ctrl+PageDown  KeyDown   "},
    {NAV_CMD_PREVIOUS_VERSION_CLIP, " Ctrl+PageUp    KeyDown   "},

    // Switching Clip Commands
    {NAV_CMD_ALL_STOP, " Space        KeyDown   "},

    // Cut Commands, added by Kea. Aug 17, 2004
	 {NAV_CMD_CUT_ADD, " N            KeyDown    "},
	 {NAV_CMD_CUT_DEL, " Ctrl+N       KeyDown    "},
	 {NAV_CMD_CUT_NXT, " K            KeyDown    "},
	 {NAV_CMD_CUT_PRV, " J            KeyDown    "},
    {NAV_CMD_MARK_SHOT, " H            KeyDown    "},

    // Bookmark Commands
	 {NAV_CMD_ADD_BOOKMARK, " M            KeyDown    "},
	 {NAV_CMD_DROP_BOOKMARK, " Ctrl+M       KeyDown    "},
	{NAV_CMD_SET_BOOKMARK_IN, " Ctrl+Shift+M KeyDown    "},
	{NAV_CMD_SET_BOOKMARK_OUT, " Shift+M      KeyDown    "},
	{NAV_CMD_GO_TO_BOOKMARK_IN, " Ctrl+Shift+E KeyDown   "},
	{NAV_CMD_GO_TO_BOOKMARK_OUT, " Ctrl+Shift+R KeyDown   "},

    // Draw Mask Region
    {NAV_CMD_MASK_TOOL_ENBL_DSBL, " Shift+I      KeyDown    "},
	{NAV_CMD_MASK_TOGGLE_VISIBLE, " Ctrl+I      KeyDown    "},
	{NAV_CMD_MASK_TOGGLE_PROPERTIES, " Ctrl+Shift+I      KeyDown    "},

	 {NAV_CMD_MASK_SELECT_DN, " LButton      ButtonDown"},
	 {NAV_CMD_MASK_SELECT_UP, " LButton      ButtonUp  "},

    {NAV_CMD_MASK_SELECT_SHIFT_DN, " Shift+LButton ButtonDown"},
    {NAV_CMD_MASK_SELECT_SHIFT_UP, " Shift+LButton ButtonUp  "},

    {NAV_CMD_MASK_SELECT_CTRL_DN, " Ctrl+LButton ButtonDown"},
    {NAV_CMD_MASK_SELECT_CTRL_UP, " Ctrl+LButton ButtonUp  "},

	 {NAV_CMD_MASK_SELECT_ALT_DN, " Alt+LButton  ButtonDown"},
	 {NAV_CMD_MASK_SELECT_ALT_UP, " Alt+LButton  ButtonUp  "},

    {NAV_CMD_MASK_SELECT_CTRL_SHIFT_DN, " Ctrl+Shift+LButton ButtonDown "},
    {NAV_CMD_MASK_SELECT_CTRL_SHIFT_UP, " Ctrl+Shift+LButton ButtonUp "},

	 {NAV_CMD_MASK_HIDE_SELECTED, " A            KeyDown   "},
	 {NAV_CMD_MASK_DELETE_SELECTED, " Shift+A      KeyDown   "
	 },
	 {NAV_CMD_MASK_SELECT_ALL_REGIONS, " Ctrl+A       KeyDown   "},

	 {NAV_CMD_MASK_INVERT, " I            KeyDown   "},
	 {NAV_CMD_MASK_SET_KEYFRAME, " B            KeyDown   "},
	 {NAV_CMD_MASK_DROP_KEYFRAME, " Ctrl+B       KeyDown   "},
	 {NAV_CMD_MASK_CYCLE_SHAPES, " Q            KeyDown   "},
    {NAV_CMD_MASK_TOGGLE_MASK_VS_ROI, " Shift+Q      KeyDown   "},
    {NAV_CMD_MASK_TOGGLE_MASK_VS_ROI, " Shift+4      KeyDown   "},

    {NAV_CMD_SHIFT_RBUTTON_DOWN, " MButton 		       ButtonDown "},
    {NAV_CMD_SHIFT_RBUTTON_DOWN, " Shift+RButton          ButtonDown "},
    {NAV_CMD_SHIFT_RBUTTON_DOWN, " Ctrl+RButton     	   ButtonDown "},
    {NAV_CMD_SHIFT_RBUTTON_DOWN, " Alt+RButton      	   ButtonDown "},
    {NAV_CMD_SHIFT_RBUTTON_DOWN, " Ctrl+Alt+RButton       ButtonDown "},

    {NAV_CMD_CTRL_SHIFT_RBUTTON_DOWN, " Ctrl+Alt+Shift+RButton ButtonDown "},

    {NAV_CMD_SHIFT_RBUTTON_UP, " MButton 		       ButtonUp   "},
    {NAV_CMD_SHIFT_RBUTTON_UP, " Shift+RButton          ButtonUp   "},
    {NAV_CMD_SHIFT_RBUTTON_UP, " Ctrl+Shift+RButton     ButtonUp   "},
    {NAV_CMD_SHIFT_RBUTTON_UP, " Alt+RButton            ButtonUp   "},
    {NAV_CMD_SHIFT_RBUTTON_UP, " Ctrl+Alt+RButton       ButtonUp   "},

    {NAV_CMD_SHIFT_RBUTTON_UP, " RButton                ButtonUp   "},

    {NAV_CMD_SHIFT_RBUTTON_UP, " Alt+Shift+RButton      ButtonUp   "},

    {NAV_CMD_SHIFT_RBUTTON_UP, " Ctrl+Alt+Shift+RButton ButtonUp   "},
    {NAV_CMD_SHIFT_RBUTTON_UP, " Ctrl+RButton           ButtonUp   "},

    // Highlighting
    {NAV_CMD_TOGGLE_HIGHLIGHTS, " Shift+W      KeyDown   "},
    {NAV_CMD_TOGGLE_ORIGINAL_VALUES, " W            KeyDown   "},

    // Timeline stuff
	 {NAV_CMD_COPY_TIMECODE, " Ctrl+C       KeyDown   "},
	 {NAV_CMD_ENTER_TC_STYLE_CHANGE_MODE, " Alt+\\       KeyDown   "
	 },
	 {NAV_CMD_EXIT_TC_STYLE_CHANGE_MODE, " Alt+\\       KeyUp     "},

	 {NAV_CMD_EDIT_TIMECODE_0, " num0         KeyDown   "},
	 {NAV_CMD_EDIT_TIMECODE_1, " num1         KeyDown   "},
	 {NAV_CMD_EDIT_TIMECODE_2, " num2         KeyDown   "},
	 {NAV_CMD_EDIT_TIMECODE_3, " num3         KeyDown   "},
	 {NAV_CMD_EDIT_TIMECODE_4, " num4         KeyDown   "},
	 {NAV_CMD_EDIT_TIMECODE_5, " num5         KeyDown   "},
	 {NAV_CMD_EDIT_TIMECODE_6, " num6         KeyDown   "},
	 {NAV_CMD_EDIT_TIMECODE_7, " num7         KeyDown   "},
	 {NAV_CMD_EDIT_TIMECODE_8, " num8         KeyDown   "},
	 {NAV_CMD_EDIT_TIMECODE_9, " num9         KeyDown   "},
	 {NAV_CMD_EDIT_TIMECODE_PLUS, " num+         KeyDown   "},
	 {NAV_CMD_EDIT_TIMECODE_MINUS, " num-         KeyDown   "},
    {NAV_CMD_EDIT_TIMECODE_MULTIPLY, " num*         KeyDown   "},
    {NAV_CMD_EDIT_TIMECODE_DIVIDE, " num/         KeyDown   "},

	 {NAV_CMD_SHOW_CUTS_TIMELINE, " O            KeyDown   "},
	 {NAV_CMD_HIDE_CUTS_TIMELINE, " Shift+O      KeyDown   "},
    {NAV_CMD_SHOW_BOOKMARK_TIMELINE, " P            KeyDown   "},
    {NAV_CMD_HIDE_BOOKMARK_TIMELINE, " Shift+P      KeyDown   "},
    {NAV_CMD_SHOW_REFERENCE_TIMELINE, " [            KeyDown   "},
    {NAV_CMD_HIDE_REFERENCE_TIMELINE, " Shift+[      KeyDown   "},
	 {NAV_CMD_SHOW_MASK_TIMELINE, " ]            KeyDown   "},
	 {NAV_CMD_HIDE_MASK_TIMELINE, " Shift+]      KeyDown   "},

    // Help! I need somebody, help! Not just anybody....
    {NAV_CMD_TOGGLE_HELP_WINDOW, " F1           KeyDown   "},

    {NAV_CMD_TOGGLE_FIX, " T            KeyDown   "},

    // Tool selection
	 {NAV_CMD_SELECT_TOOL_1, " F2           KeyDown   "},
	 {NAV_CMD_SELECT_TOOL_2, " F3           KeyDown   "},
	 {NAV_CMD_SELECT_TOOL_3, " F4           KeyDown   "},
	 {NAV_CMD_SELECT_TOOL_4, " F5           KeyDown   "},
	 {NAV_CMD_SELECT_TOOL_5, " F6           KeyDown   "},
	 {NAV_CMD_SELECT_TOOL_6, " F7           KeyDown   "},
	 {NAV_CMD_SELECT_TOOL_7, " F8           KeyDown   "},
	 {NAV_CMD_SELECT_TOOL_8, " F9           KeyDown   "},
	 {NAV_CMD_SELECT_TOOL_9, " F10          KeyDown   "},
	 {NAV_CMD_SELECT_TOOL_10, " F11          KeyDown   "},
    {NAV_CMD_SELECT_TOOL_11, " F12          KeyDown   "},

    // Pop up the Bin Manager - This used to be done by the main menu
    {NAV_CMD_OPEN_BIN_MANAGER, " Ctrl+O       KeyDown   "},

    // Toggle Tool Palette window vsibility
    {NAV_CMD_TOGGLE_TOOL_PALETTE, " .            KeyDown   "},
	 {NAV_CMD_TOGGLE_BOOKMARK_VIEWER, " Shift+.      KeyDown   "},
	 {NAV_CMD_TOGGLE_PDL_VIEWER, " Ctrl+,       KeyDown   "
	 },
	 {NAV_CMD_TOGGLE_PRES_MODE_TC_OLAY, " L            KeyDown   "},
    {NAV_CMD_TOGGLE_TIMELINE_RIBBON_OLAY, " Shift+L      KeyDown   "},

    // Handle ALTS, else stupid things happen with menus
    {NAV_CMD_ALLOW_MENU_SHORTCUTS, " Alt+Alt      KeyDown   "},
    {NAV_CMD_DONT_ALLOW_MENU_SHORTCUTS, " Alt          KeyUp     "},

    // Loupe stuff
	 {NAV_CMD_TOGGLE_LOUPE_DOWN, " Shift+Z      KeyDown   "},
	 {NAV_CMD_TOGGLE_LOUPE_UP, " Shift+Z      KeyUp     "},
    // { NAV_CMD_TOGGLE_LOUPE_UP,             " Z            KeyUp     " },
    {NAV_CMD_LOUPE_4X_MAGNIFICATION, " 4            KeyDown   "},
    {NAV_CMD_LOUPE_5X_MAGNIFICATION, " 5            KeyDown   "},
    {NAV_CMD_LOUPE_6X_MAGNIFICATION, " 6            KeyDown   "},
    {NAV_CMD_LOUPE_7X_MAGNIFICATION, " 7            KeyDown   "},
    {NAV_CMD_LOUPE_8X_MAGNIFICATION, " 8            KeyDown   "},
    {NAV_CMD_LOUPE_MAGNIFICATION_UP, " UP           KeyDown   "},
    {NAV_CMD_LOUPE_MAGNIFICATION_DOWN, " DOWN         KeyDown   "},
    {NAV_CMD_LOUPE_TOGGLE_CROSSHAIRS, " 0            KeyDown   "},

	 {NAV_GOV_CMD_REVIEW_PREV, " Insert       KeyDown   "},
	 {NAV_GOV_CMD_REVIEW_NEXT, " Del          KeyDown   "}, };

static CUserInputConfiguration *navigatorDefaultUserInputConfiguration =
    new CUserInputConfiguration(sizeof(navDefaultConfigItems) / sizeof(CUserInputConfiguration::SConfigItem),
    navDefaultConfigItems);

// -------------------------------------------------------------------------
// Navigator Command Table

static CToolCommandTable::STableEntry navCommandTableEntries[] =
{
    // Navigator Command                    Navigator Command String
    // ----------------------------------------------------------------
    // Basic Navigator Commands
	 {NAV_CMD_PLAY_REV, "NAV_CMD_PLAY_REV"},
	 {NAV_CMD_STOP, "NAV_CMD_STOP"},
	 {NAV_CMD_PLAY, "NAV_CMD_PLAY"},
	 {NAV_CMD_JOG_REV_1_FRAME, "NAV_CMD_JOG_REV_1_FRAME"},
	 {NAV_CMD_JOG_FWD_1_FRAME, "NAV_CMD_JOG_FWD_1_FRAME"},
	 {NAV_CMD_PREV_EVENT, "NAV_CMD_PREV_EVENT"},
	 {NAV_CMD_NEXT_EVENT, "NAV_CMD_NEXT_EVENT"},
	 {NAV_CMD_PLAY_FWD_CONTINUOUS, "NAV_CMD_PLAY_FWD_CONTINUOUS"},
	 {NAV_CMD_PLAY_FWD_STOP, "NAV_CMD_PLAY_FWD_STOP"},
	 {NAV_CMD_PLAY_REV_CONTINUOUS, "NAV_CMD_PLAY_REV_CONTINUOUS"},
	 {NAV_CMD_PLAY_REV_STOP, "NAV_CMD_PLAY_REV_STOP"},

    // Basic Zooming Display Commands
	 {NAV_CMD_ZOOM_ALL, "NAV_CMD_ZOOM_ALL"},
	 {NAV_CMD_ZOOM_IN, "NAV_CMD_ZOOM_IN"},
	 {NAV_CMD_ZOOM_OUT, "NAV_CMD_ZOOM_OUT"
	 },
	 {NAV_CMD_PANSTART, "NAV_CMD_PANSTART"},
	 {NAV_CMD_PANSTOP, "NAV_CMD_PANSTOP"},
    {NAV_CMD_JOG_PAN_MODE, "NAV_CMD_JOG_PAN_MODE"},

    // "Go To" Commands
	 {NAV_CMD_GOTO_IN, "NAV_CMD_GOTO_IN"},
	 {NAV_CMD_GOTO_OUT, "NAV_CMD_GOTO_OUT"},
    {NAV_CMD_GOTO_MARK_IN, "NAV_CMD_GOTO_MARK_IN"},
    {NAV_CMD_GOTO_MARK_OUT_MINUS_ONE, "NAV_CMD_GOTO_MARK_OUT_MINUS_ONE"},

    // Loop & Play Commands
	 {NAV_CMD_LOOP_CLIP, "NAV_CMD_LOOP_CLIP"},
	 {NAV_CMD_PLAY_CLIP, "NAV_CMD_PLAY_CLIP"},
    {NAV_CMD_LOOP_MARKS, "NAV_CMD_LOOP_MARKS"},
    {NAV_CMD_LOOP_AROUND_CURRENT_FRAME, "NAV_CMD_LOOP_AROUND_CURRENT_FRAME"},

    // Subclip Commands
	 {NAV_CMD_MARK_IN, "NAV_CMD_MARK_IN"},
	 {NAV_CMD_SET_MARK_IN, "NAV_CMD_SET_MARK_IN"},
	 {NAV_CMD_REMOVE_MARK_IN, "NAV_CMD_REMOVE_MARK_IN"},
	 {NAV_CMD_REMOVE_MARK_IN, "NAV_CMD_REMOVE_MARK_IN"},
    {NAV_CMD_MARK_OUT_INCLUSIVE, "NAV_CMD_MARK_OUT_INCLUSIVE"},
	 {NAV_CMD_MARK_OUT_EXCLUSIVE, "NAV_CMD_MARK_OUT_EXCLUSIVE"},
	 {NAV_CMD_MARK_ALL, "NAV_CMD_MARK_ALL"},
	 {NAV_CMD_SET_MARK_OUT, "NAV_CMD_SET_MARK_OUT"},
	 {NAV_CMD_REMOVE_MARK_OUT, "NAV_CMD_REMOVE_MARK_OUT"},
	 {NAV_CMD_EXPAND, "NAV_CMD_EXPAND"},
	 {NAV_CMD_UNEXPAND, "NAV_CMD_UNEXPAND"},

    // Switching Clip Commands
	 {NAV_CMD_NEXT_CLIP, "NAV_CMD_NEXT_CLIP"},
	 {NAV_CMD_PREVIOUS_CLIP, "NAV_CMD_PREVIOUS_CLIP"},
    {NAV_CMD_TOGGLE_LAST_TWO_CLIPS, "NAV_CMD_TOGGLE_LAST_TWO_CLIPS"},
    {NAV_CMD_NEXT_VERSION_CLIP, "NAV_CMD_NEXT_VERSION_CLIP"},
    {NAV_CMD_PREVIOUS_VERSION_CLIP, "NAV_CMD_PREVIOUS_VERSION_CLIP"},

    // Switching Clip Commands
    {NAV_CMD_ALL_STOP, "NAV_CMD_ALL_STOP"},

    // Cut Commands, added by Kea. Aug 17, 2004
	 {NAV_CMD_CUT_ADD, "NAV_CMD_CUT_ADD"},
	 {NAV_CMD_CUT_DEL, "NAV_CMD_CUT_DEL"},
	 {NAV_CMD_CUT_NXT, "NAV_CMD_CUT_NXT"},
	 {NAV_CMD_CUT_PRV, "NAV_CMD_CUT_PRV"},
	 {NAV_CMD_MARK_SHOT, "NAV_CMD_MARK_SHOT"},

    // Bookmark Commands
	 {NAV_CMD_ADD_BOOKMARK, "NAV_CMD_ADD_BOOKMARK"},
	 {NAV_CMD_DROP_BOOKMARK, "NAV_CMD_DROP_BOOKMARK"},
	 {NAV_CMD_SET_BOOKMARK_IN, "NAV_CMD_SET_BOOKMARK_IN"},
	 {NAV_CMD_SET_BOOKMARK_OUT, "NAV_CMD_SET_BOOKMARK_OUT"},
    {NAV_CMD_GO_TO_BOOKMARK_IN, "NAV_CMD_GO_TO_BOOKMARK_IN"},
    {NAV_CMD_GO_TO_BOOKMARK_OUT, "NAV_CMD_GO_TO_BOOKMARK_OUT"},

    // Draw Mask Region
    {NAV_CMD_MASK_TOOL_ENBL_DSBL, "NAV_CMD_MASK_TOOL_ENBL_DSBL"},

	 {NAV_CMD_MASK_SELECT_DN, "NAV_CMD_MASK_SELECT_DN"},
	 {NAV_CMD_MASK_SELECT_UP, "NAV_CMD_MASK_SELECT_UP"},

    {NAV_CMD_MASK_SELECT_SHIFT_DN, "NAV_CMD_MASK_SELECT_SHIFT_DN"},
    {NAV_CMD_MASK_SELECT_SHIFT_UP, "NAV_CMD_MASK_SELECT_SHIFT_UP"},

    {NAV_CMD_MASK_SELECT_CTRL_DN, "NAV_CMD_MASK_SELECT_CTRL_DN"},
    {NAV_CMD_MASK_SELECT_CTRL_UP, "NAV_CMD_MASK_SELECT_CTRL_UP"},

    {NAV_CMD_MASK_SELECT_ALT_DN, "NAV_CMD_MASK_SELECT_ALT_DN"},
    {NAV_CMD_MASK_SELECT_ALT_UP, "NAV_CMD_MASK_SELECT_ALT_UP"},

    {NAV_CMD_MASK_SELECT_CTRL_SHIFT_DN, "NAV_CMD_MASK_SELECT_CTRL_SHIFT_DN"},
    {NAV_CMD_MASK_SELECT_CTRL_SHIFT_UP, "NAV_CMD_MASK_SELECT_CTRL_SHIFT_UP"},

    {NAV_CMD_MASK_HIDE_SELECTED, "NAV_CMD_MASK_HIDE_SELECTED"},
    {NAV_CMD_MASK_DELETE_SELECTED, "NAV_CMD_MASK_DELETE_SELECTED"},
    {NAV_CMD_MASK_SELECT_ALL_REGIONS, "NAV_CMD_MASK_SELECT_ALL_REGIONS"},

	 {NAV_CMD_MASK_INVERT, "NAV_CMD_MASK_INVERT"},
	 {NAV_CMD_MASK_SET_KEYFRAME, "NAV_CMD_MASK_SET_KEYFRAME"},
	 {NAV_CMD_MASK_DROP_KEYFRAME, "NAV_CMD_MASK_DROP_KEYFRAME"},
	 {NAV_CMD_MASK_CYCLE_SHAPES, "NAV_CMD_MASK_CYCLE_SHAPES"
	 },
	 {NAV_CMD_MASK_TOGGLE_MASK_VS_ROI, "NAV_CMD_MASK_TOGGLE_MASK_VS_ROI"},

    {NAV_CMD_SHIFT_RBUTTON_DOWN, "NAV_CMD_SHIFT_RBUTTON_DOWN"},
    {NAV_CMD_CTRL_SHIFT_RBUTTON_DOWN, "NAV_CMD_CTRL_SHIFT_RBUTTON_DOWN"},
    {NAV_CMD_SHIFT_RBUTTON_UP, "NAV_CMD_SHIFT_RBUTTON_UP"},

    // Highlighting
    {NAV_CMD_TOGGLE_HIGHLIGHTS, "NAV_CMD_TOGGLE_HIGHLIGHTS"},
    {NAV_CMD_TOGGLE_ORIGINAL_VALUES, "NAV_CMD_TOGGLE_ORIGINAL_VALUES"},

    // Timeline stuff
    {NAV_CMD_COPY_TIMECODE, "NAV_CMD_COPY_TIMECODE"},
    {NAV_CMD_ENTER_TC_STYLE_CHANGE_MODE, "NAV_CMD_ENTER_TC_STYLE_CHANGE_MODE"},
    {NAV_CMD_EXIT_TC_STYLE_CHANGE_MODE, "NAV_CMD_EXIT_TC_STYLE_CHANGE_MODE"},

	 {NAV_CMD_EDIT_TIMECODE_0, "NAV_CMD_EDIT_TIMECODE_0"},
	 {NAV_CMD_EDIT_TIMECODE_1, "NAV_CMD_EDIT_TIMECODE_1"},
	 {NAV_CMD_EDIT_TIMECODE_2, "NAV_CMD_EDIT_TIMECODE_2"},
	 {NAV_CMD_EDIT_TIMECODE_3, "NAV_CMD_EDIT_TIMECODE_3"},
	 {NAV_CMD_EDIT_TIMECODE_4, "NAV_CMD_EDIT_TIMECODE_4"},
	 {NAV_CMD_EDIT_TIMECODE_5, "NAV_CMD_EDIT_TIMECODE_5"},
	 {NAV_CMD_EDIT_TIMECODE_6, "NAV_CMD_EDIT_TIMECODE_6"},
	 {NAV_CMD_EDIT_TIMECODE_7, "NAV_CMD_EDIT_TIMECODE_7"},
	 {NAV_CMD_EDIT_TIMECODE_8, "NAV_CMD_EDIT_TIMECODE_8"},
	 {NAV_CMD_EDIT_TIMECODE_9, "NAV_CMD_EDIT_TIMECODE_9"},
    {NAV_CMD_EDIT_TIMECODE_PLUS, "NAV_CMD_EDIT_TIMECODE_PLUS"},
    {NAV_CMD_EDIT_TIMECODE_MINUS, "NAV_CMD_EDIT_TIMECODE_MINUS"},
    {NAV_CMD_EDIT_TIMECODE_MULTIPLY, "NAV_CMD_EDIT_TIMECODE_MULTIPLY"},
    {NAV_CMD_EDIT_TIMECODE_DIVIDE, "NAV_CMD_EDIT_TIMECODE_DIVIDE"},

    {NAV_CMD_SHOW_CUTS_TIMELINE, "NAV_CMD_SHOW_CUTS_TIMELINE"},
    {NAV_CMD_HIDE_CUTS_TIMELINE, "NAV_CMD_HIDE_CUTS_TIMELINE"},
    {NAV_CMD_SHOW_BOOKMARK_TIMELINE, "NAV_CMD_SHOW_BOOKMARK_TIMELINE"},
    {NAV_CMD_HIDE_BOOKMARK_TIMELINE, "NAV_CMD_HIDE_BOOKMARK_TIMELINE"},
    {NAV_CMD_SHOW_REFERENCE_TIMELINE, "NAV_CMD_SHOW_REFERENCE_TIMELINE"},
    {NAV_CMD_HIDE_REFERENCE_TIMELINE, "NAV_CMD_HIDE_REFERENCE_TIMELINE"},
    {NAV_CMD_SHOW_MASK_TIMELINE, "NAV_CMD_SHOW_MASK_TIMELINE"},
    {NAV_CMD_HIDE_MASK_TIMELINE, "NAV_CMD_HIDE_MASK_TIMELINE"},

    // Show help window
    {NAV_CMD_TOGGLE_HELP_WINDOW, "NAV_CMD_TOGGLE_HELP_WINDOW"},

    // Tool selection
	 {NAV_CMD_SELECT_TOOL_1, "NAV_CMD_SELECT_TOOL_1"},
	 {NAV_CMD_SELECT_TOOL_2, "NAV_CMD_SELECT_TOOL_2"},
	 {NAV_CMD_SELECT_TOOL_3, "NAV_CMD_SELECT_TOOL_3"},
	 {NAV_CMD_SELECT_TOOL_4, "NAV_CMD_SELECT_TOOL_4"},
	 {NAV_CMD_SELECT_TOOL_5, "NAV_CMD_SELECT_TOOL_5"},
	 {NAV_CMD_SELECT_TOOL_6, "NAV_CMD_SELECT_TOOL_6"},
	 {NAV_CMD_SELECT_TOOL_7, "NAV_CMD_SELECT_TOOL_7"},
	 {NAV_CMD_SELECT_TOOL_8, "NAV_CMD_SELECT_TOOL_8"},
	 {NAV_CMD_SELECT_TOOL_9, "NAV_CMD_SELECT_TOOL_9"},
	 {NAV_CMD_SELECT_TOOL_10, "NAV_CMD_SELECT_TOOL_10"},
    {NAV_CMD_SELECT_TOOL_11, "NAV_CMD_SELECT_TOOL_11"},

    // Pop up the Bin Manager - This used to be done by the main menu
    {NAV_CMD_OPEN_BIN_MANAGER, "NAV_CMD_OPEN_BIN_MANAGER"},

    // Toggle Tool Palette window vsibility
    {NAV_CMD_TOGGLE_TOOL_PALETTE, "NAV_CMD_TOGGLE_TOOL_PALETTE"},
    {NAV_CMD_TOGGLE_BOOKMARK_VIEWER, "NAV_CMD_TOGGLE_BOOKMARK_VIEWER"},
    {NAV_CMD_TOGGLE_PDL_VIEWER, "NAV_CMD_TOGGLE_PDL_VIEWER"},
    {NAV_CMD_TOGGLE_PRES_MODE_TC_OLAY, "NAV_CMD_TOGGLE_PRES_MODE_TC_OLAY"},
    {NAV_CMD_TOGGLE_TIMELINE_RIBBON_OLAY, "NAV_CMD_TOGGLE_TIMELINE_RIBBON_OLAY"},

    // Handle ALTS, else stupid things happen with menus
    {NAV_CMD_ALLOW_MENU_SHORTCUTS, "NAV_CMD_ALLOW_MENU_SHORTCUTS"},
    {NAV_CMD_DONT_ALLOW_MENU_SHORTCUTS, "NAV_CMD_DONT_ALLOW_MENU_SHORTCUTS"},

    // Magnifying Loupe
	 {NAV_CMD_TOGGLE_LOUPE_DOWN, "NAV_CMD_TOGGLE_LOUPE_DOWN"},

	 {NAV_CMD_TOGGLE_LOUPE_UP, "NAV_CMD_TOGGLE_LOUPE_UP"},
    {NAV_CMD_LOUPE_4X_MAGNIFICATION, "NAV_CMD_LOUPE_4X_MAGNIFICATION"},
    {NAV_CMD_LOUPE_5X_MAGNIFICATION, "NAV_CMD_LOUPE_5X_MAGNIFICATION"},
    {NAV_CMD_LOUPE_6X_MAGNIFICATION, "NAV_CMD_LOUPE_6X_MAGNIFICATION"},
    {NAV_CMD_LOUPE_7X_MAGNIFICATION, "NAV_CMD_LOUPE_7X_MAGNIFICATION"},
    {NAV_CMD_LOUPE_8X_MAGNIFICATION, "NAV_CMD_LOUPE_8X_MAGNIFICATION"},
    {NAV_CMD_LOUPE_MAGNIFICATION_UP, "NAV_CMD_LOUPE_MAGNIFICATION_UP"},
    {NAV_CMD_LOUPE_MAGNIFICATION_DOWN, "NAV_CMD_LOUPE_MAGNIFICATION_DOWN"},
    {NAV_CMD_LOUPE_TOGGLE_CROSSHAIRS, "NAV_CMD_LOUPE_TOGGLE_CROSSHAIRS"},

    {NAV_CMD_NO_OP, "NAV_CMD_NO_OP"}, };

static CToolCommandTable *navigatorCommandTable =
    new CToolCommandTable(sizeof(navCommandTableEntries) / sizeof(CToolCommandTable::STableEntry),
    navCommandTableEntries);

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CNavigatorTool::CNavigatorTool(const string &newToolName) : CToolObject(newToolName, NULL),
    navigatorState(NAV_STATE_READY_FOR_CMD)
{
    if (maskTool == 0)
        maskTool = new CMaskTool;

    if (reticleTool == 0)
        reticleTool = new CReticleTool;

    MTI_UINT32 **featureArray = mainWindow->GetFeatureArray();
#ifndef NO_LICENSING
    rsrcCtrl = new CRsrcCtrl;
    licenseIsOK = (rsrcCtrl->IsFeatureLicensed(featureArray[0]) || rsrcCtrl->IsFeatureLicensed(featureArray[1]));
#else
    licenseIsOK = true;
#endif

    weAreInDotMode = false;
    toolbarCorralWasVisibleBeforeDotMode = false;
    toolPaletteWasVisibleBeforeDotMode = false;

    hideLoupePendingMode = false;
    settingLoupeParameters = false;
}

CNavigatorTool::~CNavigatorTool()
{
    if (reticleTool != 0)
        delete reticleTool;

    if (maskTool != 0)
        delete maskTool;

#ifndef NO_LICENSING
    delete rsrcCtrl;
#endif
}

CToolObject* CNavigatorTool::MakeTool(const string& newToolName) {return new CNavigatorTool(newToolName);}

CToolProcessor* CNavigatorTool::makeToolProcessorInstance(const bool *newEmergencyStopFlagPtr)
{
    return 0; // This tool doesn't have a CToolProcessor
}

// ===================================================================
//
// Function:    toolInitialize
//
// Description: Called when the ToolManager wants to initialize
// the Navigator Tool
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================
int CNavigatorTool::toolInitialize(CToolSystemInterface *newSystemAPI)
{
    int retVal;

    // Initialize the CNavigatorTool's base class, i.e., CToolObject
    // This must be done before the CNavigatorTool initializes itself
    retVal = initBaseToolObject(navigatorToolNumber, newSystemAPI, navigatorCommandTable,
        navigatorDefaultUserInputConfiguration);
    if (retVal != 0)
    {
        // ERROR: initBaseToolObject failed
        return retVal;
    }

    // Initialize the Navigator Tool's command handling state
    navigatorState = NAV_STATE_READY_FOR_CMD;

    // TO DO: the CNavigatorTool's initialization code goes here

    // Tell the Mask and Reticle Tools about the System API
    maskTool->setSystemAPI(newSystemAPI);
    reticleTool->setSystemAPI(newSystemAPI);

    maskTool->toolRestoreSettings();
    mainWindow->UpdateMaskToolGUI(*maskTool);

    reticleTool->setReticleToolInternalEnabled(true);
    // TTT mainWindow->UpdateReticleToolGUI(*reticleTool);

    return retVal;
}

// ===================================================================
//
// Function:    toolShutdown
//
// Description: Called when the Tool Manager wants to shut down the
// Navigator Tool, typically when the main program is
// shutting down.
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================
int CNavigatorTool::toolShutdown()
{
    int retVal;

    // TO DO: the Navigator Tool's shutdown code goes here

    maskTool->toolSaveSettings();

    // Shutdown the Navigator Tool's base class, i.e., CToolObject.
    // This must be done after the Navigator Tool shuts down itself
    retVal = shutdownBaseToolObject();
    if (retVal != 0)
    {
        // ERROR: shutdownBaseToolObject failed
        return retVal;
    }

    return retVal;
}

// ===================================================================
//
// Function:    onToolCommand
//
// Description: Navigator Tool's Command Handler
//
// Arguments:   CToolCommand &toolCommand
//
// Returns:     Results of command handling as CToolObject's return codes,
// TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================
int CNavigatorTool::onToolCommand(CToolCommand &toolCommand)
{
    // Default return is that the Navigator command was processed ok
    int retVal = TOOL_RETURN_CODE_PROCESSED_OK | TOOL_RETURN_CODE_EVENT_CONSUMED;

    // Get the current clip handler
    CNavCurrentClip *clipHandler = mainWindow->GetCurrentClipHandler();

    // Get the Navigator command number
    int commandNumber = toolCommand.getCommandNumber();

    // Stoopid swap hack
    if (mainWindow->GetSwapWAndShiftWFlag())
    {
        switch (commandNumber)
        {
        case NAV_CMD_TOGGLE_HIGHLIGHTS:
            commandNumber = NAV_CMD_TOGGLE_ORIGINAL_VALUES;
            break;

        case NAV_CMD_TOGGLE_ORIGINAL_VALUES:
            commandNumber = NAV_CMD_TOGGLE_HIGHLIGHTS;
            break;

        default:
            break;
        }
    }

    // more security crap
#ifndef NO_LICENSING
    bool isLicenseReallyOK = licenseIsOK && (rsrcCtrl->FlexLMConfig() != NULL) && (!rsrcCtrl->IsExpired()) &&
        (rsrcCtrl->Version() != "Unknown");
#else
    bool isLicenseReallyOK = true;
    ////TODO: wtf
#endif
    // If there's no clip, don't execute any clip-dependent commands
    if (!(clipHandler->isClipAvailable() && isLicenseReallyOK))
    {
        switch (commandNumber)
        {
            // These are OK
        case NAV_CMD_SHOW_CUTS_TIMELINE:
        case NAV_CMD_HIDE_CUTS_TIMELINE:
        case NAV_CMD_SHOW_BOOKMARK_TIMELINE:
        case NAV_CMD_HIDE_BOOKMARK_TIMELINE:
        case NAV_CMD_SHOW_REFERENCE_TIMELINE:
        case NAV_CMD_HIDE_REFERENCE_TIMELINE:
        case NAV_CMD_SHOW_MASK_TIMELINE:
        case NAV_CMD_HIDE_MASK_TIMELINE:
        case NAV_CMD_SELECT_TOOL_1:
        case NAV_CMD_SELECT_TOOL_2:
        case NAV_CMD_SELECT_TOOL_3:
        case NAV_CMD_SELECT_TOOL_4:
        case NAV_CMD_SELECT_TOOL_5:
        case NAV_CMD_SELECT_TOOL_6:
        case NAV_CMD_SELECT_TOOL_7:
        case NAV_CMD_SELECT_TOOL_8:
        case NAV_CMD_SELECT_TOOL_9:
        case NAV_CMD_SELECT_TOOL_10:
        case NAV_CMD_SELECT_TOOL_11:
        case NAV_CMD_OPEN_BIN_MANAGER:
        case NAV_CMD_TOGGLE_TOOL_PALETTE:
        case NAV_CMD_TOGGLE_BOOKMARK_VIEWER:
        case NAV_CMD_TOGGLE_HELP_WINDOW:
        case NAV_CMD_FOCUS_MAIN_WINDOW:
        case NAV_CMD_ALLOW_MENU_SHORTCUTS:
        case NAV_CMD_DONT_ALLOW_MENU_SHORTCUTS:
        case NAV_CMD_TOGGLE_LOUPE_DOWN:
        case NAV_CMD_TOGGLE_LOUPE_UP:
        case NAV_CMD_LOUPE_4X_MAGNIFICATION:
        case NAV_CMD_LOUPE_5X_MAGNIFICATION:
        case NAV_CMD_LOUPE_6X_MAGNIFICATION:
        case NAV_CMD_LOUPE_7X_MAGNIFICATION:
        case NAV_CMD_LOUPE_8X_MAGNIFICATION:
        case NAV_CMD_LOUPE_MAGNIFICATION_UP:
        case NAV_CMD_LOUPE_MAGNIFICATION_DOWN:
        case NAV_CMD_LOUPE_TOGGLE_CROSSHAIRS:
        case NAV_CMD_NO_OP:
            // do nothing;
            break;
        default:
            return retVal; // squelch
        }
    }

    // Sigh - need to accept GOV if pending for the following commands
    switch (commandNumber)
    {
    case NAV_CMD_PLAY_REV:
    case NAV_CMD_STOP:
    case NAV_CMD_PLAY:
    case NAV_CMD_LOOP_CLIP:
    case NAV_CMD_PLAY_CLIP:
    case NAV_CMD_LOOP_MARKS:
    case NAV_CMD_LOOP_AROUND_CURRENT_FRAME:
    case NAV_CMD_NEXT_CLIP:
    case NAV_CMD_PREVIOUS_CLIP:
    case NAV_CMD_TOGGLE_LAST_TWO_CLIPS:
    case NAV_CMD_NEXT_VERSION_CLIP:
    case NAV_CMD_PREVIOUS_VERSION_CLIP:
    case NAV_CMD_ALL_STOP:
    case NAV_CMD_MASK_TOOL_ENBL_DSBL:
    case NAV_CMD_MASK_SELECT_DN:
    case NAV_CMD_MASK_SELECT_SHIFT_DN:
    case NAV_CMD_MASK_SELECT_CTRL_DN:
    case NAV_CMD_MASK_SELECT_ALT_DN:
    case NAV_CMD_MASK_SELECT_CTRL_SHIFT_DN:
    case NAV_CMD_MASK_HIDE_SELECTED:
    case NAV_CMD_MASK_DELETE_SELECTED:
    case NAV_CMD_MASK_SELECT_ALL_REGIONS:
    case NAV_CMD_MASK_INVERT:
    case NAV_CMD_MASK_SET_KEYFRAME:
    case NAV_CMD_MASK_DROP_KEYFRAME:
    case NAV_CMD_MASK_CYCLE_SHAPES:
    case NAV_CMD_MASK_TOGGLE_VISIBLE:
    case NAV_CMD_MASK_TOGGLE_MASK_VS_ROI:
    case NAV_CMD_SHIFT_RBUTTON_DOWN:
    case NAV_CMD_CTRL_SHIFT_RBUTTON_DOWN:
    case NAV_CMD_SHIFT_RBUTTON_UP:
    case NAV_CMD_SELECT_TOOL_1:
    case NAV_CMD_SELECT_TOOL_2:
    case NAV_CMD_SELECT_TOOL_3:
    case NAV_CMD_SELECT_TOOL_4:
    case NAV_CMD_SELECT_TOOL_5:
    case NAV_CMD_SELECT_TOOL_6:
    case NAV_CMD_SELECT_TOOL_7:
    case NAV_CMD_SELECT_TOOL_8:
    case NAV_CMD_SELECT_TOOL_9:
    case NAV_CMD_SELECT_TOOL_10:
    case NAV_CMD_SELECT_TOOL_11:

        getSystemAPI()->AcceptGOV();

        break;

        // These commands will NOT affect pending GOV
        //
        // Any commands that simply move to a different frame
    case NAV_CMD_JOG_REV_1_FRAME:
    case NAV_CMD_JOG_FWD_1_FRAME:
    case NAV_CMD_PREV_EVENT:
    case NAV_CMD_NEXT_EVENT:
    case NAV_CMD_PLAY_FWD_CONTINUOUS:
    case NAV_CMD_PLAY_REV_CONTINUOUS:
    case NAV_CMD_GOTO_IN:
    case NAV_CMD_GOTO_OUT:
    case NAV_CMD_GOTO_MARK_IN:
    case NAV_CMD_GOTO_MARK_OUT_MINUS_ONE:

        // Any zooming/panning-relatred commands
    case NAV_CMD_ZOOM_ALL:
    case NAV_CMD_ZOOM_IN:
    case NAV_CMD_ZOOM_OUT:
    case NAV_CMD_PANSTART:
    case NAV_CMD_PANSTOP:
    case NAV_CMD_JOG_PAN_MODE:
        //
        // Anything with key or button UP trigger
    case NAV_CMD_PLAY_FWD_STOP:
    case NAV_CMD_PLAY_REV_STOP:
    case NAV_CMD_MASK_SELECT_UP:
    case NAV_CMD_MASK_SELECT_SHIFT_UP:
    case NAV_CMD_MASK_SELECT_CTRL_UP:
    case NAV_CMD_MASK_SELECT_ALT_UP:
    case NAV_CMD_MASK_SELECT_CTRL_SHIFT_UP:
        //
        // Anything having to do with setting/removing marks
    case NAV_CMD_MARK_IN:
    case NAV_CMD_SET_MARK_IN:
    case NAV_CMD_REMOVE_MARK_IN:
    case NAV_CMD_MARK_OUT_INCLUSIVE:
    case NAV_CMD_MARK_OUT_EXCLUSIVE:
    case NAV_CMD_MARK_ALL:
    case NAV_CMD_SET_MARK_OUT:
    case NAV_CMD_REMOVE_MARK_OUT:
    case NAV_CMD_CLEAR_MARKS:
    case NAV_CMD_EXPAND:
    case NAV_CMD_UNEXPAND:
    case NAV_CMD_CUT_ADD:
    case NAV_CMD_CUT_DEL:
    case NAV_CMD_CUT_NXT:
    case NAV_CMD_CUT_PRV:
    case NAV_CMD_MARK_SHOT:
        //
        // Any highlighting-related commands
    case NAV_CMD_TOGGLE_HIGHLIGHTS:
    case NAV_CMD_TOGGLE_ORIGINAL_VALUES:
        //
        // Any timeline manipulation stuff
    case NAV_CMD_ADD_BOOKMARK:
    case NAV_CMD_DROP_BOOKMARK:
    case NAV_CMD_SET_BOOKMARK_IN:
    case NAV_CMD_SET_BOOKMARK_OUT:
    case NAV_CMD_GO_TO_BOOKMARK_IN:
    case NAV_CMD_GO_TO_BOOKMARK_OUT:
    case NAV_CMD_COPY_TIMECODE:
    case NAV_CMD_ENTER_TC_STYLE_CHANGE_MODE:
    case NAV_CMD_EXIT_TC_STYLE_CHANGE_MODE:
    case NAV_CMD_EDIT_TIMECODE_0:
    case NAV_CMD_EDIT_TIMECODE_1:
    case NAV_CMD_EDIT_TIMECODE_2:
    case NAV_CMD_EDIT_TIMECODE_3:
    case NAV_CMD_EDIT_TIMECODE_4:
    case NAV_CMD_EDIT_TIMECODE_5:
    case NAV_CMD_EDIT_TIMECODE_6:
    case NAV_CMD_EDIT_TIMECODE_7:
    case NAV_CMD_EDIT_TIMECODE_8:
    case NAV_CMD_EDIT_TIMECODE_9:
    case NAV_CMD_EDIT_TIMECODE_PLUS:
    case NAV_CMD_EDIT_TIMECODE_MINUS:
    case NAV_CMD_EDIT_TIMECODE_MULTIPLY:
    case NAV_CMD_EDIT_TIMECODE_DIVIDE:
    case NAV_CMD_SHOW_CUTS_TIMELINE:
    case NAV_CMD_HIDE_CUTS_TIMELINE:
    case NAV_CMD_SHOW_BOOKMARK_TIMELINE:
    case NAV_CMD_HIDE_BOOKMARK_TIMELINE:
    case NAV_CMD_SHOW_REFERENCE_TIMELINE:
    case NAV_CMD_HIDE_REFERENCE_TIMELINE:
    case NAV_CMD_SHOW_MASK_TIMELINE:
    case NAV_CMD_HIDE_MASK_TIMELINE:
        //
        // Anything not involving the main window
    case NAV_CMD_TOGGLE_HELP_WINDOW:
    case NAV_CMD_OPEN_BIN_MANAGER:
    case NAV_CMD_TOGGLE_TOOL_PALETTE:
    case NAV_CMD_TOGGLE_BOOKMARK_VIEWER:
    case NAV_CMD_FOCUS_MAIN_WINDOW:
    case NAV_CMD_ALLOW_MENU_SHORTCUTS:
    case NAV_CMD_DONT_ALLOW_MENU_SHORTCUTS:
    case NAV_CMD_NO_OP:
    case NAV_CMD_TOGGLE_LOUPE_DOWN:
    case NAV_CMD_TOGGLE_LOUPE_UP:
    case NAV_CMD_LOUPE_4X_MAGNIFICATION:
    case NAV_CMD_LOUPE_5X_MAGNIFICATION:
    case NAV_CMD_LOUPE_6X_MAGNIFICATION:
    case NAV_CMD_LOUPE_7X_MAGNIFICATION:
    case NAV_CMD_LOUPE_8X_MAGNIFICATION:
    case NAV_CMD_LOUPE_MAGNIFICATION_UP:
    case NAV_CMD_LOUPE_MAGNIFICATION_DOWN:
    case NAV_CMD_LOUPE_TOGGLE_CROSSHAIRS:

        break;
    }

    // TTT Test
    // TTT Print out the command name and number
    const CToolCommandTable *commandTable = getCommandTable();
    const char* commandName = commandTable->findCommandName(commandNumber);
    if (commandName == 0)
        commandName = "Unknown Command";
    TRACE_2(errout << " Command: Navigator: " << commandName << " (" << commandNumber << ")");

    // Get pointer to Player from global mainWindow;
    CPlayer *player = mainWindow->GetPlayer();
    int curfrm = player->getLastFrameIndex();

    // Get pointer to Displayer from global mainWindow
    CDisplayer *displayer = mainWindow->GetDisplayer();

    TTimelineFrame *timeline;

    timeline = mainWindow->GetTimeline();

    // more security crap: can't set marks

    // Interpret the new Navigator command given the Navigator Tool's
    // current state
    ENavigatorState nextState = navigatorState;
    // The IsResolutionLicensed is just random security crap
    if (navigatorState == NAV_STATE_READY_FOR_CMD)
    {
        switch (commandNumber)
        {
        case NAV_CMD_NO_OP:
            // Do nothing, duh
            break;
        case NAV_CMD_FOCUS_MAIN_WINDOW:
            mainWindow->FocusOnMainWindow();
            break;
        case NAV_CMD_ALLOW_MENU_SHORTCUTS:
            // Side effect - move focus to main window
            mainWindow->FocusOnMainWindow();
            mainWindow->SetMenuShortcutsAreOkNow(true);
            break;
        case NAV_CMD_DONT_ALLOW_MENU_SHORTCUTS:
            mainWindow->SetMenuShortcutsAreOkNow(false);
            break;
        case NAV_CMD_REWIND:
            player->playClipFastBwd(FAST_DELTA_FRAME);
            break;
        case NAV_CMD_PLAY_REV:
            player->playClipBwd();
            break;
        case NAV_CMD_STOP:
            player->stopAndSynchronize();
            break;
        case NAV_CMD_PLAY:
            player->playClipFwd();
            break;
        case NAV_CMD_FAST_FWD:
            player->playClipFastFwd(FAST_DELTA_FRAME);
            break;
        case NAV_CMD_JOG_REV_ONETHIRD:
            player->jogClipBwdOneThirdSecond(); // TTT
            nextState = NAV_STATE_JOG_REV_CONTINUOUS;
            break;
        case NAV_CMD_JOG_FWD_ONETHIRD:
            player->jogClipFwdOneThirdSecond(); // TTT
            nextState = NAV_STATE_JOG_FWD_CONTINUOUS;
            break;
        case NAV_CMD_JOG_REV_CONTINUOUS:
            player->jogClipSlowBwd(); // TTT
            nextState = NAV_STATE_JOG_REV_CONTINUOUS;
            break;
        case NAV_CMD_JOG_FWD_CONTINUOUS:
            player->jogClipSlowFwd(); // TTT
            nextState = NAV_STATE_JOG_FWD_CONTINUOUS;
            break;
        case NAV_CMD_JOG_REV_8_FRAMES:
            player->goToFrameSynchronous(curfrm - 8);
            break;
        case NAV_CMD_JOG_FWD_8_FRAMES:
            player->goToFrameSynchronous(curfrm + 8);
            break;
        case NAV_CMD_JOG_REV_1_FRAME:
            player->jogClipOnceBwd();
            break;
        case NAV_CMD_JOG_FWD_1_FRAME:
            player->jogClipOnceFwd();
            break;
        case NAV_CMD_PLAY_FWD_CONTINUOUS:
            player->jogClipFastFwd();
            nextState = NAV_STATE_PLAY_FWD_CONTINUOUS;
            break;
        case NAV_CMD_PLAY_REV_CONTINUOUS:
            player->jogClipFastBwd();
            nextState = NAV_STATE_PLAY_REV_CONTINUOUS;
            break;
        case NAV_CMD_ZOOM_ALL:
            if (displayer->zoomAll())
            {
                if (!player->isDisplaying())
                {
                    player->refreshFrameSynchronousCached();
                }
            }
            mainWindow->UpdateZoomLevelStatus();
            break;
		  case NAV_CMD_ZOOM_IN:
            if (displayer->zoomIn())
            {
                if (!player->isDisplaying())
                {
                    player->refreshFrameSynchronousCached();
                }
            }
            mainWindow->UpdateZoomLevelStatus();
            break;
        case NAV_CMD_ZOOM_OUT:
            if (displayer->zoomOut())
            {
                if (!player->isDisplaying())
                {
                    player->refreshFrameSynchronousCached();
                }
            }
            mainWindow->UpdateZoomLevelStatus();
            break;
        case NAV_CMD_PANSTART:
            mainWindow->captureMouse();
            displayer->panStart();
            break;
        case NAV_CMD_PANSTOP:
            displayer->panStop();
            mainWindow->releaseMouse();
            if (!player->isDisplaying() && player->getSubsampledPanning())
            {
                player->stopAndSynchronize();
            }
            break;
        case NAV_CMD_JOG_PAN_MODE:
            displayer->panModeJog();
            break;
        case NAV_CMD_ENTER_MOUSE_ZOOM_MODE:
            mainWindow->captureMouse();
            displayer->enterMouseZoomMode();
            break;
        case NAV_CMD_EXIT_MOUSE_ZOOM_MODE:
            displayer->exitMouseZoomModeOrResetZoom();
            mainWindow->releaseMouse();
            mainWindow->UpdateZoomLevelStatus();
            if (!player->isDisplaying())
            {
                player->refreshFrameSynchronousCached();
            }
            break;

        case NAV_CMD_GOTO_IN:
            player->goToIn();
            break;
        case NAV_CMD_GOTO_OUT:
            player->goToOut();
            break;
        case NAV_CMD_GOTO_MARK_IN:
            player->goToMarkIn();
            break;
        case NAV_CMD_GOTO_MARK_OUT_MINUS_ONE:
            player->goToMarkOutMinusOne();
            break;

        case NAV_CMD_GOTO_CUE:
            {
                CTimecode cueupTC = mainWindow->GetTimeline()->timelineCurrentTCVTimecodeEdit->tcP;
                CTimecode boundedTC = clipHandler->limitTimecode(cueupTC, false /* TTT true */);
                player->goToFrameSynchronous(boundedTC);
                mainWindow->GetTimeline()->timelineCurrentTCVTimecodeEdit->tcP = boundedTC;
            } break;

        case NAV_CMD_LOOP_CLIP:
            player->loopClipInOut();
            break;
        case NAV_CMD_PLAY_CLIP:
            player->playClipInOut();
            break;
        case NAV_CMD_LOOP_MARKS:
            // Hack : if marks are invald, simply loop full clip
            if (player->getMarkIn() >= 0 && player->getMarkOut() > player->getMarkIn())
                player->loopClipFwdBetweenMarks();
            else
                player->loopClipInOut();
            break;
        case NAV_CMD_LOOP_AROUND_CURRENT_FRAME:
            player->loopClipFwd2NPlus1Frames(8);
            break;
        case NAV_CMD_PLAY_MARKS:
            player->playClipFwdBetweenMarks(false);
            break;
            //
            // this cmd sets a flag to block all Player
            // cmds exc Stop before running
            //
        case NAV_CMD_PLAY_MARKS_TOOL:
            player->playClipFwdBetweenMarks(true);
            break;
        case NAV_CMD_MARK_IN:
            player->setMarkIn();
            timeline->UpdateMarks();
            break;

            // Cut Commands, Added by Kea, Aug 17 2004

        case NAV_CMD_CUT_ADD:
            timeline->performCutAction(1); // 1 is cut_add in timelineunit.
            break;
        case NAV_CMD_CUT_DEL:
            timeline->performCutAction(0); // 0 is cut_remove in timelineunit.
            break;
        case NAV_CMD_CUT_NXT:
            timeline->performCutAction(2); // 2 is cut_next in timelineunit.
            break;
        case NAV_CMD_CUT_PRV:
            timeline->performCutAction(3); // 3 is cut prev in timelineunit.
            break;
        case NAV_CMD_MARK_SHOT:
            timeline->performCutAction(4); // 4 is cut_markswitch in timelineunit.
            break;
            // CutCommands----------------------------

        case NAV_CMD_ADD_BOOKMARK:
            timeline->AddBookmark();
            break;
        case NAV_CMD_DROP_BOOKMARK:
            timeline->DropBookmark();
            break;
        case NAV_CMD_SET_BOOKMARK_IN:
            timeline->SetBookmarkIn();
            break;
        case NAV_CMD_SET_BOOKMARK_OUT:
            timeline->SetBookmarkOut();
            break;
        case NAV_CMD_GO_TO_BOOKMARK_IN:
            timeline->GoToBookmarkIn();
            break;
        case NAV_CMD_GO_TO_BOOKMARK_OUT:
            timeline->GoToBookmarkOut();
            break;

        case NAV_CMD_NEXT_EVENT:
            timeline->goToNextEventOnSelectedTimeline();
            break;
        case NAV_CMD_PREV_EVENT:
            timeline->goToPreviousEventOnSelectedTimeline();
            break;

        case NAV_CMD_SET_MARK_IN:
            player->removeMarkIn();
            timeline->UpdateMarks();
            break;
        case NAV_CMD_REMOVE_MARK_IN:
            player->removeMarkIn();
            timeline->UpdateMarks();
            break;
        case NAV_CMD_MARK_OUT_INCLUSIVE:
            player->setMarkOutInclusive();
            timeline->UpdateMarks();
            break;
        case NAV_CMD_MARK_OUT_EXCLUSIVE:
            player->setMarkOutExclusive();
            timeline->UpdateMarks();
            break;
        case NAV_CMD_MARK_ALL:
            player->setMarkAll();
            timeline->UpdateMarks();
            break;
        case NAV_CMD_SET_MARK_OUT:
            player->removeMarkOut();
            timeline->UpdateMarks();
            break;
        case NAV_CMD_REMOVE_MARK_OUT:
            player->removeMarkOut();
            timeline->UpdateMarks();
            break;
        case NAV_CMD_CLEAR_MARKS:
            player->removeMarkIn();
            player->removeMarkOut();
            timeline->UpdateMarks();
            break;
        case NAV_CMD_EXPAND:
            {
                bool rc = -1;
                if (player->isExpandedBetweenMarks())
                    rc = player->unexpandBetweenMarks();
                else
                    rc = player->expandBetweenMarks();

                if (rc == 0) {
                    // get timecode limits & display them
                    timeline->UpdateInTimecode();
                    timeline->UpdateOutTimecode();

                    // set trackbar scale
                    timeline->UpdateTimelineLimits();
                    if (player->isExpandedBetweenMarks())
                        player->setMarkAll();
                    timeline->UpdateMarks();
                }
                break;
            }
        case NAV_CMD_UNEXPAND:
            if (player->unexpandBetweenMarks() == 0) {

                // get timecode limits & display them
                timeline->UpdateInTimecode();
                timeline->UpdateOutTimecode();

                // set trackbar scale
                timeline->UpdateTimelineLimits();
            }
            break;
        case NAV_CMD_NEXT_CLIP:
            mainWindow->LoadNextClip();
            break;
        case NAV_CMD_PREVIOUS_CLIP:
            mainWindow->LoadPreviousClip();
            break;
        case NAV_CMD_TOGGLE_LAST_TWO_CLIPS:
            mainWindow->ToggleBetweenLastTwoClips();
            break;
        case NAV_CMD_NEXT_VERSION_CLIP:
            mainWindow->LoadNextVersionClip();
            break;
        case NAV_CMD_PREVIOUS_VERSION_CLIP:
            mainWindow->LoadPreviousVersionClip();
            break;
        case NAV_CMD_ALL_STOP:
            player->stopOrStart();
            break;

        case NAV_CMD_DRAW_RETICLE:
            if (reticleTool->isReticleToolInternalEnabled()) {
                maskTool->saveEnabledStateAndDisable();
                reticleTool->setReticleToolEnabled(true);
                mainWindow->UpdateMaskToolGUI(*maskTool);
                mainWindow->GetTimeline()->DRSNameLabel->Caption = "Stretch new reticle";
            }
            break;

            // Mask Tool commands
        case NAV_CMD_MASK_DRAW_BEZIER:
        case NAV_CMD_MASK_DRAW_LASSO:
        case NAV_CMD_MASK_DRAW_RECT:
        case NAV_CMD_MASK_EXCLUDE:
        case NAV_CMD_MASK_INCLUDE:
        case NAV_CMD_MASK_INVERT:
        case NAV_CMD_MASK_SELECT:
		case NAV_CMD_MASK_TOOL_ENBL_DSBL:
		case NAV_CMD_MASK_TOGGLE_PROPERTIES:
		case NAV_CMD_MASK_TOOL_ENABLE:
		case NAV_CMD_MASK_TOOL_DISABLE:
		case NAV_CMD_MASK_HIDE_SELECTED:
		case NAV_CMD_MASK_DELETE_SELECTED:
		case NAV_CMD_MASK_AUTOCLEAN_MODE:
		case NAV_CMD_MASK_SELECT_ALL_REGIONS:
		case NAV_CMD_MASK_SHOW_NOTHING:
		case NAV_CMD_MASK_SHOW_OUTLINE:
        case NAV_CMD_MASK_SHOW_INCLUSION:
        case NAV_CMD_MASK_SHOW_EXCLUSION:
        case NAV_CMD_MASK_CHANGE_BORDER:
        case NAV_CMD_MASK_SELECT_DN: // NAV_CMD_RETICLE_SELECT_DN
        case NAV_CMD_MASK_SELECT_UP: // NAV_CMD_RETICLE_SELECT_UP
        case NAV_CMD_MASK_SELECT_SHIFT_DN:
        case NAV_CMD_MASK_SELECT_SHIFT_UP:
        case NAV_CMD_MASK_SELECT_ALT_DN:
        case NAV_CMD_MASK_SELECT_ALT_UP:
        case NAV_CMD_MASK_SELECT_CTRL_DN:
        case NAV_CMD_MASK_SELECT_CTRL_UP:
        case NAV_CMD_MASK_SELECT_CTRL_SHIFT_DN:
        case NAV_CMD_MASK_SELECT_CTRL_SHIFT_UP:
        case NAV_CMD_MASK_SET_KEYFRAME:
        case NAV_CMD_MASK_DROP_KEYFRAME:
        case NAV_CMD_MASK_DROP_ALL_KEYFRAMES:
        case NAV_CMD_MASK_CYCLE_SHAPES:
        case NAV_CMD_MASK_TOGGLE_MASK_VS_ROI:
        case NAV_CMD_MASK_LOCK_VISIBILITY_OFF:
        case NAV_CMD_MASK_UNLOCK_VISIBILITY_OFF:
        case NAV_CMD_SHIFT_RBUTTON_DOWN:
        case NAV_CMD_CTRL_SHIFT_RBUTTON_DOWN:
        case NAV_CMD_SHIFT_RBUTTON_UP:
            retVal = ExecuteMaskToolCommand(commandNumber);
            break;

        case NAV_CMD_RETICLE_TOOL_ENABLE:
        case NAV_CMD_RETICLE_TOOL_DISABLE:
            retVal = ExecuteReticleToolCommand(commandNumber);
            break;

        case NAV_CMD_MASK_TOGGLE_VISIBLE:
            ExecuteMaskToolCommand(maskTool->IsMaskVisible()
                                    ? NAV_CMD_MASK_SHOW_NOTHING
                                    : NAV_CMD_MASK_SHOW_OUTLINE
                                  );
            break;

        case NAV_CMD_TOGGLE_HIGHLIGHTS:
            {
                int filter = player->getPlaybackFilter();

                // Turn off other modes.
                if ((filter&~PLAYBACK_FILTER_BOTH) != 0)
                {
                    filter = PLAYBACK_FILTER_NONE;
                }

                if ((filter & PLAYBACK_FILTER_HIGHLIGHT_FIXES) != 0)
                {
                    filter &= ~PLAYBACK_FILTER_HIGHLIGHT_FIXES;
                }
                else
                {
                    filter |= PLAYBACK_FILTER_HIGHLIGHT_FIXES;
                }

                player->setPlaybackFilter(filter);
                mainWindow->UpdatePlaybackFilterStatus();
                if (!player->isDisplaying())
                {
                    player->refreshFrameSynchronous();
                }
            } break;

        case NAV_CMD_TOGGLE_ORIGINAL_VALUES:
            {
                CToolManager toolManager;

                // HACK TO WORKAROUND A DISPLAYER BUG
                // This doesn't work if the provisional is pending;
                // note that the 'Check...' call will try to auto-accept
                // before checking if the provisional remains unresolved,
                // so this change will be mostly invisible!
                if (!toolManager.CheckProvisionalUnresolvedAndToolProcessing())
                {
                    int filter = player->getPlaybackFilter();

                    // Turn off other modes.
                    if ((filter&~PLAYBACK_FILTER_BOTH) != 0)
                    {
                        filter = PLAYBACK_FILTER_NONE;
                    }

                    if ((filter & PLAYBACK_FILTER_ORIGINAL_VALUES) != 0)
                    {
                        filter &= ~PLAYBACK_FILTER_ORIGINAL_VALUES;
                    }
                    else
                    {
                        filter |= PLAYBACK_FILTER_ORIGINAL_VALUES;
                    }

                    player->setPlaybackFilter(filter);
                    mainWindow->UpdatePlaybackFilterStatus();
                    if (!player->isDisplaying())
                    {
                        player->refreshFrameSynchronous();
                    }
                }
            } break;

        case NAV_CMD_TOGGLE_HIGHLIGHT_AND_ORIG:
            {
                int filter = player->getPlaybackFilter();
                if (filter == PLAYBACK_FILTER_BOTH)
                    filter = PLAYBACK_FILTER_NONE;
                else
                    filter = PLAYBACK_FILTER_BOTH;
                player->setPlaybackFilter(filter);
                mainWindow->UpdatePlaybackFilterStatus();
                if (!player->isDisplaying())
                    player->refreshFrameSynchronous();
            } break;

        case NAV_CMD_COPY_TIMECODE:
            {
                PTimecode ptc(player->getCurrentTimecode());
                mainWindow->SendToClipboard(ptc.getUserPreferredStyleString());
            } break;

        case NAV_CMD_ENTER_TC_STYLE_CHANGE_MODE:
            {
                if (_tcChangeStyleKeyIsDown)
                {
                    _tcChangeKeyWasHeldDown = true;
                    break;
                }

                _tcChangeStyleKeyIsDown = true;
                _tcChangeKeyWasHeldDown = false;

                PTimecode &tcP = player->CurrentTC;
                if (tcP.getCurrentUserPreferredStringStyle() != PTimecode::NormalStringStyle)
                {
                    tcP.setCurrentUserPreferredStringStyle(PTimecode::NormalStringStyle);
                }
                else
                {
                    tcP.setCurrentUserPreferredStringStyle((tcP.getFramesPerSecond() == 0) ?
                        PTimecode::ForceTimecodeStyle : PTimecode::ForceFrameNumberStyle);
                }

                break;
            }

        case NAV_CMD_EXIT_TC_STYLE_CHANGE_MODE:
            {
                if (!_tcChangeStyleKeyIsDown || !_tcChangeKeyWasHeldDown)
                {
                    _tcChangeStyleKeyIsDown = false;
                    _tcChangeKeyWasHeldDown = false;
                    break;
                }

                _tcChangeStyleKeyIsDown = false;
                _tcChangeKeyWasHeldDown = false;

                PTimecode &tcP = player->CurrentTC;
                if (tcP.getCurrentUserPreferredStringStyle() != PTimecode::NormalStringStyle)
                {
                    tcP.setCurrentUserPreferredStringStyle(PTimecode::NormalStringStyle);
                }
                else
                {
                    tcP.setCurrentUserPreferredStringStyle((tcP.getFramesPerSecond() == 0) ?
                        PTimecode::ForceTimecodeStyle : PTimecode::ForceFrameNumberStyle);
                }

                break;
            }

        case NAV_CMD_EDIT_TIMECODE_MULTIPLY:
            timeline->showTimecodeEditor((int)(unsigned)'*');
            break;
        case NAV_CMD_EDIT_TIMECODE_DIVIDE:
            timeline->showTimecodeEditor((int)(unsigned)'/');
            break;
        case NAV_CMD_EDIT_TIMECODE_PLUS:
            timeline->showTimecodeEditor((int)(unsigned)'+');
            break;
        case NAV_CMD_EDIT_TIMECODE_MINUS:
            timeline->showTimecodeEditor((int)(unsigned)'-');
            break;
        case NAV_CMD_EDIT_TIMECODE_0:
            timeline->showTimecodeEditor((int)(unsigned)'0');
            break;
        case NAV_CMD_EDIT_TIMECODE_1:
            timeline->showTimecodeEditor((int)(unsigned)'1');
            break;
        case NAV_CMD_EDIT_TIMECODE_2:
            timeline->showTimecodeEditor((int)(unsigned)'2');
            break;
        case NAV_CMD_EDIT_TIMECODE_3:
            timeline->showTimecodeEditor((int)(unsigned)'3');
            break;
        case NAV_CMD_EDIT_TIMECODE_4:
            timeline->showTimecodeEditor((int)(unsigned)'4');
            break;
        case NAV_CMD_EDIT_TIMECODE_5:
            timeline->showTimecodeEditor((int)(unsigned)'5');
            break;
        case NAV_CMD_EDIT_TIMECODE_6:
            timeline->showTimecodeEditor((int)(unsigned)'6');
            break;
        case NAV_CMD_EDIT_TIMECODE_7:
            timeline->showTimecodeEditor((int)(unsigned)'7');
            break;
        case NAV_CMD_EDIT_TIMECODE_8:
            timeline->showTimecodeEditor((int)(unsigned)'8');
            break;
        case NAV_CMD_EDIT_TIMECODE_9:
            timeline->showTimecodeEditor((int)(unsigned)'9');
            break;

        case NAV_CMD_SHOW_MASK_TIMELINE:
            timeline->ShowMaskAnimationTimeline(true);
            break;

        case NAV_CMD_HIDE_MASK_TIMELINE:
            timeline->ShowMaskAnimationTimeline(false);
            break;

        case NAV_CMD_SHOW_CUTS_TIMELINE:
            timeline->ShowCutsTimeline(true);
            break;

        case NAV_CMD_HIDE_CUTS_TIMELINE:
            timeline->ShowCutsTimeline(false);
            break;

        case NAV_CMD_SHOW_BOOKMARK_TIMELINE:
            timeline->ShowBookmarkTimeline(true);
            break;

        case NAV_CMD_HIDE_BOOKMARK_TIMELINE:
            timeline->ShowBookmarkTimeline(false);
            break;

        case NAV_CMD_SHOW_REFERENCE_TIMELINE:
            timeline->ShowReferenceTimeline(true);
            break;

        case NAV_CMD_HIDE_REFERENCE_TIMELINE:
            timeline->ShowReferenceTimeline(false);
            break;

		  case NAV_CMD_TOGGLE_HELP_WINDOW:
				mainWindow->ToolHelpMenuItemClick(nullptr);
				break;

        case NAV_CMD_SELECT_TOOL_1:
            HandleToolHotKey(0);
            break;

        case NAV_CMD_SELECT_TOOL_2:
            HandleToolHotKey(1);
            break;

        case NAV_CMD_SELECT_TOOL_3:
            HandleToolHotKey(2);
            break;

        case NAV_CMD_SELECT_TOOL_4:
            HandleToolHotKey(3);
            break;

        case NAV_CMD_SELECT_TOOL_5:
            HandleToolHotKey(4);
            break;

        case NAV_CMD_SELECT_TOOL_6:
            HandleToolHotKey(5);
            break;

        case NAV_CMD_SELECT_TOOL_7:
            HandleToolHotKey(6);
            break;

        case NAV_CMD_SELECT_TOOL_8:
            HandleToolHotKey(7);
            break;

        case NAV_CMD_SELECT_TOOL_9:
            HandleToolHotKey(8);
            break;

        case NAV_CMD_SELECT_TOOL_10:
            HandleToolHotKey(9);
            break;

        case NAV_CMD_SELECT_TOOL_11:
            HandleToolHotKey(10);
            break;

        case NAV_CMD_OPEN_BIN_MANAGER:
            mainWindow->ShowBinManager(true); // show
            break;

        case NAV_CMD_TOGGLE_TOOL_PALETTE:
            if (!BaseToolWindow->Visible)
            {
                CToolCommand showPalette = CToolCommand(this, NAV_CMD_SHOW_TOOL_PALETTE);
                this->onToolCommand(showPalette);
            }
            else
            {
                CToolCommand hidePalette = CToolCommand(this, NAV_CMD_HIDE_TOOL_PALETTE);
                this->onToolCommand(hidePalette);
            }
            break;

        case NAV_CMD_TOGGLE_BOOKMARK_VIEWER:
            BookmarkEventViewerForm->Visible = !BookmarkEventViewerForm->Visible;
            break;

        case NAV_CMD_HIDE_TOOL_PALETTE:
            BaseToolWindow->Hide();
            mainWindow->FocusOnMainWindow();
            break;

        case NAV_CMD_SHOW_TOOL_PALETTE:
            BaseToolWindow->Show();
            break;

        case NAV_CMD_TOGGLE_PDL_VIEWER:
            mainWindow->ShowPDLViewerMenuItemClick(NULL); // really toggles
            break;

        case NAV_CMD_TOGGLE_PRES_MODE_TC_OLAY:
            mainWindow->ToggleShowTimecodeOverlay();
            break;

        case NAV_CMD_TOGGLE_TIMELINE_RIBBON_OLAY:
            mainWindow->ToggleShowTimelineRibbonOverlay();
            break;

        case NAV_CMD_TOGGLE_LOUPE_DOWN:

            // This is a little tricky... the action is not always to
            // toggle, it can mean 'go into loupe setting mode'. So we
            // need to defer the 'toggle off' function to the 'key up'
            // event (if the loupe was visible and the user changed
            // a parameter while holdiung down Shift+Z, then the loupe
            // will remain visible)

            // SettingLoupeParameters acts helps distinguish between
            // the first instance of the Z and subsequent auto-repeated Z'a
            if (!settingLoupeParameters)
            {
                settingLoupeParameters = true;
                mainWindow->SetLoupeSettingMode(true);
                loupeVisibilityRememberer = mainWindow->IsLoupeVisible();
            }

            if (mainWindow->IsLoupeVisible())
            {
                // It gets even trickier! If the user holds down the
                // Shift+Z so the Z starts to auto-repeat, then this
                // will get triggered on the second and subsequent Z's
                // so when the user releases the Z, the loupe will
                // disappear. But if the user taps the Shift-Z so
                // auto-repeat doesn't kick in, then the loupe will
                // stay up until the Shift-Z is hit again!

                hideLoupePendingMode = true;
            }
            else
            {
                mainWindow->ShowLoupe();
                hideLoupePendingMode = false;
            }

            break;

        case NAV_CMD_TOGGLE_LOUPE_UP:

            if (settingLoupeParameters)
            {
                // The following must go BEFORE SetLoupeSettingMode()!
                bool aSettingChanged = mainWindow->didALoupeSettingChange();

                settingLoupeParameters = false;
                mainWindow->SetLoupeSettingMode(false);

                if (hideLoupePendingMode)
                {
                    hideLoupePendingMode = false;

                    // See the 'this is a little tricky' blurb, above!
                    if (aSettingChanged == false || loupeVisibilityRememberer == false)
                    {
                        mainWindow->HideLoupe();
                    }
                }
            }
            break;

        case NAV_CMD_LOUPE_4X_MAGNIFICATION:
            if (settingLoupeParameters)
            {
                mainWindow->SetLoupeMagnification(4);
                hideLoupePendingMode = false;
            }
            else
            {
                retVal = TOOL_RETURN_CODE_NO_ACTION;
            }
            break;

        case NAV_CMD_LOUPE_5X_MAGNIFICATION:
            if (settingLoupeParameters)
            {
                mainWindow->SetLoupeMagnification(5);
                hideLoupePendingMode = false;
            }
            else
            {
                retVal = TOOL_RETURN_CODE_NO_ACTION;
            }
            break;

        case NAV_CMD_LOUPE_6X_MAGNIFICATION:
            if (settingLoupeParameters)
            {
                mainWindow->SetLoupeMagnification(6);
                hideLoupePendingMode = false;
            }
            else
            {
                retVal = TOOL_RETURN_CODE_NO_ACTION;
            }
            break;

        case NAV_CMD_LOUPE_7X_MAGNIFICATION:
            if (settingLoupeParameters)
            {
                mainWindow->SetLoupeMagnification(7);
                hideLoupePendingMode = false;
            }
            else
            {
                retVal = TOOL_RETURN_CODE_NO_ACTION;
            }
            break;

        case NAV_CMD_LOUPE_8X_MAGNIFICATION:
            if (settingLoupeParameters)
            {
                mainWindow->SetLoupeMagnification(8);
                hideLoupePendingMode = false;
            }
            else
            {
                retVal = TOOL_RETURN_CODE_NO_ACTION;
            }
            break;

        case NAV_CMD_LOUPE_MAGNIFICATION_UP:
            if (settingLoupeParameters)
            {
                int mag = mainWindow->GetLoupeMagnification();
                mainWindow->SetLoupeMagnification(mag + 1);
                hideLoupePendingMode = false;
            }
            else
            {
                retVal = TOOL_RETURN_CODE_NO_ACTION;
            }
            break;

        case NAV_CMD_LOUPE_MAGNIFICATION_DOWN:
            if (settingLoupeParameters)
            {
                int mag = mainWindow->GetLoupeMagnification();
                mainWindow->SetLoupeMagnification(mag - 1);
                hideLoupePendingMode = false;
            }
            else
            {
                retVal = TOOL_RETURN_CODE_NO_ACTION;
            }
            break;

        case NAV_CMD_LOUPE_TOGGLE_CROSSHAIRS:
            if (settingLoupeParameters)
            {
                mainWindow->ToggleLoupeCrosshairs();
                hideLoupePendingMode = false;
            }
            else
            {
                retVal = TOOL_RETURN_CODE_NO_ACTION;
            }
            break;

        case NAV_GOV_CMD_REVIEW_PREV:
            ReviewToolSingleStep(false, false);
            retVal = TOOL_RETURN_CODE_EVENT_CONSUMED;
            break;

        case NAV_GOV_CMD_REVIEW_NEXT:
            ReviewToolSingleStep(true, false);
            retVal = TOOL_RETURN_CODE_EVENT_CONSUMED;
            break;

        case NAV_CMD_TOGGLE_FIX:
            if (getSystemAPI()->IsProvisionalPending())
            {
                // Provisional is pending, so this command is interpreted
                // as toggle
                getSystemAPI()->ToggleProvisional();
                retVal = TOOL_RETURN_CODE_PROCESSED_OK | TOOL_RETURN_CODE_EVENT_CONSUMED;
            }
            else
            {
                retVal = TOOL_RETURN_CODE_NO_ACTION;
            }
            break;

        default:
            // Return that Navigator ignored the command
            retVal = TOOL_RETURN_CODE_NO_ACTION;
            break;
        }
    }
    else if (navigatorState == NAV_STATE_JOG_FWD_CONTINUOUS || navigatorState == NAV_STATE_JOG_REV_CONTINUOUS)
    {
        // While the Navigator is Jogging  continuously,  all commands
        // but jog continuous stop are ignored. To ensure the computer
        // monitor is refreshed on stop,  we use a synchronous refresh
        if (navigatorState == NAV_STATE_JOG_FWD_CONTINUOUS && commandNumber == NAV_CMD_JOG_FWD_STOP)
        {
            // Stop Forward Continuous Jogging
            //
            player->stopJogAndSynchronize();
            nextState = NAV_STATE_READY_FOR_CMD;
        }
        else if (navigatorState == NAV_STATE_JOG_REV_CONTINUOUS && commandNumber == NAV_CMD_JOG_REV_STOP)
        {
            // Stop Reverse Continuous Jogging
            //
            player->stopJogAndSynchronize();
            nextState = NAV_STATE_READY_FOR_CMD;
        }
        else
        {
            // Return that Navigator ignored the command
            retVal = TOOL_RETURN_CODE_NO_ACTION;
        }
    }
    else if (navigatorState == NAV_STATE_PLAY_FWD_CONTINUOUS || navigatorState == NAV_STATE_PLAY_REV_CONTINUOUS)
    {
        // While the Navigator is Playing continuously, all commands
        // but stop are ignored
        if (navigatorState == NAV_STATE_PLAY_FWD_CONTINUOUS && commandNumber == NAV_CMD_PLAY_FWD_STOP)
        {
            // Stop Forward Continuous
            //
            player->stopJogAndSynchronize();
            nextState = NAV_STATE_READY_FOR_CMD;
        }
        else if (navigatorState == NAV_STATE_PLAY_REV_CONTINUOUS && commandNumber == NAV_CMD_PLAY_REV_STOP)
        {
            // Stop Reverse Continuous
            //
            player->stopJogAndSynchronize();
            nextState = NAV_STATE_READY_FOR_CMD;
        }
        else
        {
            // Return that Navigator ignored the command
            retVal = TOOL_RETURN_CODE_NO_ACTION;
        }
    }
    // Update the navigator's state variable
    navigatorState = nextState;

    return retVal;
}

// ===================================================================

void CNavigatorTool::HandleToolHotKey(int tabIndex)
{
   if (BaseToolWindow->GetCurrentTabIndex() == tabIndex)
   {
      mainWindow->SendSwitchSubtoolCmdToActiveTool();
   }
   else
   {
      BaseToolWindow->SwitchToTab(tabIndex);
   }
}

// ===================================================================

int CNavigatorTool::ExecuteMaskToolCommand(int commandNumber)
{
   // Default return is that the Navigator command was processed ok
   int retVal = TOOL_RETURN_CODE_PROCESSED_OK | TOOL_RETURN_CODE_EVENT_CONSUMED;

   // Get pointer to Displayer from global mainWindow

   // the select DN and select UP are the same as for the MASK tool,
   // but if the RETICLE tool is waiting for a rectangle to be
   // stretched, it gets the input.
   if (reticleTool->isReticleToolEnabled())
   {
      CDisplayer *displayer = mainWindow->GetDisplayer();

      switch (commandNumber)
      {
      case NAV_CMD_RETICLE_SELECT_DN:
         retVal = reticleTool->BeginUserSelect(displayer);
         break;

      case NAV_CMD_RETICLE_SELECT_UP:
         retVal = reticleTool->EndUserSelect(displayer);
         reticleTool->setReticleToolEnabled(false);
         maskTool->restoreSavedEnabledState();
         mainWindow->UpdateMaskToolGUI(*maskTool);
         mainWindow->GetTimeline()->DRSNameLabel->Caption = "";
         break;
      }

      return retVal;
   }

   // Don't do anything if the mask tool is completely disabled!
   if (!maskTool->IsMaskToolActivationAllowed())
   {
      return TOOL_RETURN_CODE_NO_ACTION;
   }

   if (maskTool->IsMaskRoiModeActive())
   {
      switch (commandNumber)
      {
      case NAV_CMD_MASK_TOGGLE_MASK_VS_ROI:
         maskTool->setMaskToolActivationAllowed(true, true, false);
         break;
      case NAV_CMD_MASK_TOOL_ENABLE:
      case NAV_CMD_MASK_TOOL_ENBL_DSBL:
         maskTool->ActivateNormalMode(true);
         break;
      case NAV_CMD_MASK_DRAW_RECT:
         maskTool->setRegionShape(MASK_REGION_RECT);
         break;
      case NAV_CMD_MASK_DRAW_LASSO:
         maskTool->setRegionShape(MASK_REGION_LASSO);
         break;
      case NAV_CMD_MASK_DRAW_BEZIER:
         maskTool->setRegionShape(MASK_REGION_BEZIER);
         break;
      case NAV_CMD_MASK_CYCLE_SHAPES:
         maskTool->cycleRegionShape();
         break;
      case NAV_CMD_MASK_SELECT_DN:
         retVal = maskTool->SelectDn();
         break;
      case NAV_CMD_MASK_SELECT_UP:
         retVal = maskTool->SelectUp();
         break;
      case NAV_CMD_MASK_LOCK_VISIBILITY_OFF:
         maskTool->setMaskVisibilityOffLock(true);
         break;
      case NAV_CMD_MASK_UNLOCK_VISIBILITY_OFF:
         maskTool->setMaskVisibilityOffLock(false);
         break;
      }
   }
   else if (maskTool->IsMaskToolActive())
   {
      switch (commandNumber)
      {
      case NAV_CMD_MASK_TOOL_DISABLE: // EXTERNAL DISABLE!
      case NAV_CMD_MASK_TOOL_ENBL_DSBL:
         maskTool->ActivateNormalMode(false);
         break;
      case NAV_CMD_MASK_TOGGLE_PROPERTIES:
         BaseToolWindow->ShowHidePropertiesWindow();
         break;
      case NAV_CMD_MASK_DRAW_RECT:
         maskTool->setRegionShape(MASK_REGION_RECT);
         break;
      case NAV_CMD_MASK_DRAW_LASSO:
         maskTool->setRegionShape(MASK_REGION_LASSO);
         break;
      case NAV_CMD_MASK_DRAW_BEZIER:
         maskTool->setRegionShape(MASK_REGION_BEZIER);
         break;
      case NAV_CMD_MASK_SELECT:
         maskTool->setRegionShape(MASK_REGION_INVALID);
         break;
      case NAV_CMD_MASK_CYCLE_SHAPES:
         maskTool->cycleRegionShape();
         break;
      case NAV_CMD_MASK_TOGGLE_MASK_VS_ROI:
         // Flip to ROI mode.
         maskTool->ToggleRoiModeActivation();
         break;
      case NAV_CMD_MASK_EXCLUDE:
         maskTool->setGlobalInclusionFlag(false);
         break;
      case NAV_CMD_MASK_INCLUDE:
         maskTool->setGlobalInclusionFlag(true);
         break;
      case NAV_CMD_MASK_INVERT:
         maskTool->setGlobalInclusionFlag(!maskTool->getGlobalInclusionFlag());
         break;
      case NAV_CMD_MASK_SELECT_ALL_REGIONS:
         maskTool->SelectAll();
         break;
      case NAV_CMD_MASK_HIDE_SELECTED:
         maskTool->HideSelected();
         break;
      case NAV_CMD_MASK_DELETE_SELECTED:
         maskTool->DeleteSelected();
         break;
      case NAV_CMD_MASK_SHOW_NOTHING:
         maskTool->setMaskVisible(false);
         break;
      case NAV_CMD_MASK_SHOW_OUTLINE:
         maskTool->setMaskVisible(true);
         break;
      case NAV_CMD_MASK_LOCK_VISIBILITY_OFF:
         maskTool->setMaskVisibilityOffLock(true);
         break;
      case NAV_CMD_MASK_UNLOCK_VISIBILITY_OFF:
         maskTool->setMaskVisibilityOffLock(false);
         break;
      case NAV_CMD_MASK_CHANGE_BORDER:
         maskTool->ChangeBorder();
         break;
      case NAV_CMD_MASK_SELECT_DN:
         retVal = maskTool->SelectDn();
         break;

      case NAV_CMD_MASK_SELECT_UP:
      case NAV_CMD_MASK_SELECT_SHIFT_UP:
      case NAV_CMD_MASK_SELECT_ALT_UP:
      case NAV_CMD_MASK_SELECT_CTRL_UP:
      case NAV_CMD_MASK_SELECT_CTRL_SHIFT_UP:
         retVal = maskTool->SelectUp();
         break;

      case NAV_CMD_MASK_SELECT_SHIFT_DN:
         retVal = maskTool->SelectShiftDn();
         break;
      case NAV_CMD_MASK_SELECT_ALT_DN:
         retVal = maskTool->SelectAltDn();
         break;
      case NAV_CMD_MASK_SELECT_CTRL_DN:
         retVal = maskTool->SelectCtrlDn();
         break;
      case NAV_CMD_MASK_SELECT_CTRL_SHIFT_DN:
         retVal = maskTool->SelectCtrlShiftDn();
         break;
      case NAV_CMD_MASK_SET_KEYFRAME:
         maskTool->SetKeyframe(mainWindow->GetPlayer()->getLastFrameIndex());
         break;
      case NAV_CMD_MASK_DROP_KEYFRAME:
         maskTool->DeleteKeyframe(mainWindow->GetPlayer()->getLastFrameIndex());
         break;
      case NAV_CMD_MASK_DROP_ALL_KEYFRAMES:
         maskTool->DeleteAllKeyframes(true);
         break;

      case NAV_CMD_SHIFT_RBUTTON_DOWN:
         maskTool->HandleShiftRightButtonDown(false);
         break;
      case NAV_CMD_CTRL_SHIFT_RBUTTON_DOWN:
         maskTool->HandleShiftRightButtonDown(true);
         break;
      case NAV_CMD_SHIFT_RBUTTON_UP:
         maskTool->HandleShiftRightButtonUp();
         break;
      }
   }
   else // Mask isn't active, so all we respond to is requests to activate it.
   {
      switch (commandNumber)
      {
      case NAV_CMD_MASK_TOOL_ENABLE:
      case NAV_CMD_MASK_TOOL_ENBL_DSBL:
         maskTool->ActivateNormalMode(true);
         break;
      case NAV_CMD_MASK_TOGGLE_MASK_VS_ROI:
         // Activate ROI mode..
         if (maskTool->IsMaskRoiModeActivationAllowed())
         {
            maskTool->setMaskToolActivationAllowed(true, true, true);
         }
         break;
      case NAV_CMD_MASK_SELECT_DN:
         // In case we're in auto-activate-ROI mode!
         retVal = maskTool->SelectDn();
         break;
      case NAV_CMD_MASK_SELECT_UP:
         // Because we do Down here!
         retVal = maskTool->SelectUp();
         break;
      case NAV_CMD_MASK_LOCK_VISIBILITY_OFF:
         maskTool->setMaskVisibilityOffLock(true);
         break;
      case NAV_CMD_MASK_UNLOCK_VISIBILITY_OFF:
         maskTool->setMaskVisibilityOffLock(false);
         break;
      }
   }

   return retVal;
}

int CNavigatorTool::ExecuteReticleToolCommand(int commandNumber)
{
    // Default return is that the Navigator command was processed ok
    int retVal = TOOL_RETURN_CODE_PROCESSED_OK | TOOL_RETURN_CODE_EVENT_CONSUMED;

    switch (commandNumber)
    {
    case NAV_CMD_RETICLE_TOOL_ENABLE:
        reticleTool->setReticleToolInternalEnabled(true);
        break;

    case NAV_CMD_RETICLE_TOOL_DISABLE:
        reticleTool->setReticleToolInternalEnabled(false);
        break;
    }

    // TTT mainWindow->UpdateReticleToolGUI(*reticleTool);

    return retVal;
}

// Larry's AutoClean 'A key' hack
bool CNavigatorTool::seeIfTheMaskToolWantsTheAKey() {return maskTool->HideSelected();}

void CNavigatorTool::tellTheMaskToolToUnselectAll() {maskTool->UnselectAll();}

bool CNavigatorTool::askTheMaskToolIfAnythingIsSelected() {return maskTool->IsAnythingSelected();}

void CNavigatorTool::setRectToMask(const RECT &rect)
{
   maskTool->setRectAsMask(rect);
}

// ===================================================================
//
// Function:    onMouseMove
//
// Description: Navigator Tool's Mouse Movement Handler
//
// Arguments:   CUserInput &userInput
//
// Returns:     Results of command handling as CToolObject's return codes,
// TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================
int CNavigatorTool::onMouseMove(CUserInput &userInput)
{
    // Default return is that the Navigator command was processed ok
    int retVal = TOOL_RETURN_CODE_PROCESSED_OK | TOOL_RETURN_CODE_EVENT_CONSUMED;

    // Get the current clip handler
    CNavCurrentClip *clipHandler = mainWindow->GetCurrentClipHandler();

    // If there's no clip, don't execute any command
    if (!clipHandler->isClipAvailable())
        return (retVal);

    // Call Clip Displayer's Mouse Move function to stretch a rectangle
    mainWindow->GetDisplayer()->mouseMv(userInput.windowX, userInput.windowY);
    if (mainWindow->GetDisplayer()->isPanning() && !mainWindow->GetPlayer()->isDisplaying()) { // need explicit refresh
        mainWindow->GetPlayer()->panFrameSynchronous();
    }

    if (!mainWindow->GetPlayer()->isDisplaying()) {
        maskTool->MouseMove(userInput.windowX, userInput.windowY);
    }

    // NavSystem's BezierEditor
    getSystemAPI()->BezierMouseMove(userInput.windowX, userInput.windowY);

    // Cannot tell if Clip Display actually did anything with the new
    // coordinates, but be optimistic
    return retVal;
}

// ===================================================================
//
// Function:    onRedraw
//
// Description:
//
// Arguments:   None
//
// Returns:
//
// ===================================================================
int CNavigatorTool::onRedraw(int frameIndex)
{
    maskTool->DrawMask(frameIndex);

    return (TOOL_RETURN_CODE_EVENT_CONSUMED);
}

int CNavigatorTool::onNewClip()
{
    // Default return is that the Navigator command was processed ok
    int retVal = TOOL_RETURN_CODE_PROCESSED_OK | TOOL_RETURN_CODE_EVENT_CONSUMED;
    const CImageFormat *imageFormat = getSystemAPI()->getVideoClipImageFormat();
    if (imageFormat == 0)
        return retVal;

    // Set the Mask Tool's active picture rect.  This has the side effect
    // of clearing the Mask if the new clip has an active Picture Rect
    // that is different from the previous clip.  If the active picture
    // Rects are the same for new and old clips, then we retain the
    // Mask
    maskTool->setClipActivePictureRect(imageFormat->getActivePictureRect());

    mainWindow->GetTimeline()->FrameResize(nullptr);

    return retVal;
}

CMaskTool* CNavigatorTool::GetMaskTool()
{
    if (maskTool == 0)
        maskTool = new CMaskTool;

    return maskTool;
}

CReticleTool* CNavigatorTool::GetReticleTool()
{
    if (reticleTool == 0)
        reticleTool = new CReticleTool;

    return reticleTool;
}

void CNavigatorTool::ReviewToolSingleStep(bool goNext, bool goZoom)
{
    CToolManager toolManager;
    int govToolIndex = toolManager.findTool("GOV"); // QQQ Manifest constant!!
    MTIassert(govToolIndex >= 0);
    if (govToolIndex < 0)
        return;

    CToolObject *toolObj = toolManager.GetTool(govToolIndex);
    CGOVTool *govTool = dynamic_cast<CGOVTool*>(toolObj);
    MTIassert(govTool != NULL);
    govTool->ReviewToolSingleStep(goNext, goZoom);
}
