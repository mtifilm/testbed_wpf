// MaskTool.h: interface for the CMaskTool class.
//
/*
$Header: /usr/local/filmroot/Nav_common/include/MaskTool.h,v 1.16.2.11 2009/02/06 20:27:23 mbraca Exp $
*/
///////////////////////////////////////////////////////////////////////////////

#ifndef MaskToolH
#define MaskToolH

#include "machine.h"
#include "MaskToolRegions.h"
#include "PixelRegions.h"
#include "RegionOfInterest.h"
#include "TimeLine.h"  // for CAnimateTimelineInterface (base class)
#include "ToolObject.h"
#include "ToolSystemInterface.h"
#include "NavigatorTool.h"
#include "IppArray.h"
#include "BezierEditor.h"
#include <vector>
#include <list>
using std::vector;
using std::list;

///////////////////////////////////////////////////////////////////////////////
// Forward Declarations

class CAnimateTimelineInterface;
class CBezierEditor;
class CDisplayer;
class CIniFile;
class MaskChangedAutoNotifier;
class CMaskTool;
class CMaskTransformEditor;
class CPDL;
class CPDLElement;
class CPDLEntry;
class CPDLEntry_Mask;

#ifdef __sgi
class MainWindow;
#endif // #ifdef __sgi

///////////////////////////////////////////////////////////////////////////////

enum EMaskToolMouseMode
{
   MASK_TOOL_MOUSE_MODE_NONE,
   MASK_TOOL_MOUSE_MODE_STRETCH,
   MASK_TOOL_MOUSE_MODE_SELECT_ADD,
   MASK_TOOL_MOUSE_MODE_SELECT_CTRL
};

enum EMaskToolTransformMode
{
   MASK_TOOL_SELECT,
   MASK_TOOL_TRANSFORM,
   MASK_TOOL_TRANSFORM_ACCEPT,
   MASK_TOOL_TRANSFORM_REJECT
};

///////////////////////////////////////////////////////////////////////////////

class CRegionNode
{
public:

   CRegionNode(CDisplayer * );
   CRegionNode(const CRegionNode& );
   virtual ~CRegionNode();

   CRegionNode *bwd, *fwd;

   bool isSelected;

   bool hiddenFlag;

#ifdef USE_PER_REGION_INCLUSION_FLAGS
   bool inclusionFlag;
#endif

   double blendSlew;

   int borderWidth;

   CBezierEditor *bezierEditor;

   CBezierEditor *xformBezierEditor;
};

class MaskParameters
{
public:

	vector<VertexList> AllVerticies;
	int getExclusionBorderWidth() const { return  _exclusionBorderWidth; }
	void setExclusionBorderWidth(int value) { _exclusionBorderWidth = value; }

	int getInclusionBorderWidth() const { return  _inclusionBorderWidth; }
	void setInclusionBorderWidth(int value) { _inclusionBorderWidth = value; }

	bool isMaskInclusive() const { return  _isMaskInclusive; }
	void setIsMaskInclusive(bool value) { _isMaskInclusive = value; }

	bool operator != (const MaskParameters &rhs) const {return !(*this == rhs);}
	bool operator == (const MaskParameters &rhs) const
	{
		if (getExclusionBorderWidth() != rhs.getExclusionBorderWidth())
		{
			return false;
		}
		if (getInclusionBorderWidth() != rhs.getInclusionBorderWidth())
		{
			return false;
		}

		if (isMaskInclusive() != rhs.isMaskInclusive())
		{
			return false;
		}

		auto rhsVertices = rhs.AllVerticies;

		if (rhsVertices.size() != AllVerticies.size())
		{
			return false;
		}

		for (auto i = 0; i < rhsVertices.size(); i++)
		{
			auto &v0 = rhsVertices[i];
			auto &v1 = AllVerticies[i];
			if (v0.size() != v1.size())
			{
				return false;
			}

			auto n = v0.size();
			if (std::equal(v0.begin(), v0.begin() + n, v1.begin()) == false)
			{
				return false;
			}
		}

		return true;
	}

private:
	int _exclusionBorderWidth = 0;
	int _inclusionBorderWidth = 0;
	bool _isMaskInclusive = true;
};

///////////////////////////////////////////////////////////////////////////////

class CMaskKeyframe
{
   friend CMaskTool;

public:  //    Public Functions
   CMaskKeyframe();
   CMaskKeyframe(int newFrameIndex, CRegionNode *rgnlst);
   ~CMaskKeyframe();

   void Clear();
   CRegionNode *GetRegionList();
   int GetFrameIndex() const;
   bool GetInclusionFlag() const;      // WTF? Always returns true!!

   void AddRegion(CRegionNode *);
   void DeleteRegion(int rgnindx);
   void AddVertex(int, double);
   void DeleteVertices(int, CRegionNode *);
   void SetRegion(CRegionNode *rgnlst);

   void InterpolateKeyframes(CMaskKeyframe *bef, CMaskKeyframe *aft, double combin);
   void CapturePDLEntry(CPDLElement *listElement, CMaskTool *maskTool);

   vector<VertexList> CopyVertexLists();

public:  //    Public Data
   static const string tag_Keyframe;
   static const string frameIndexAttr;

private: //    Private Functions
   void deleteMaskRegion();

private: //    Private Data

   int frameIndex;

   CRegionNode *regionListHead;
   CRegionNode *regionListTail;
   int regionCount;
   int regionSelectedCount;
};

class CMaskToolTimeline
{
public:
   CMaskToolTimeline();
   ~CMaskToolTimeline();

   CMaskKeyframe *GetKeyframe(int frameIndex);
   CMaskKeyframe *GetFirstKeyframe();
   CMaskKeyframe *GetNextKeyframe(int frameIndex);
   CMaskKeyframe *GetPreviousKeyframe(int frameIndex);

   void ComputeInterpolatedKeyframe(int frameIndex, CMaskKeyframe *theFrame);

   bool IsEmpty();
   int GetKeyframeCount();

   void SetKeyframe(int frameIndex, CRegionNode *rgnlst);
   void AddRegionToAllKeyframes(CRegionNode *rgn);
   void DeleteRegionFromAllKeyframes(int rgnindx);
   void AddVertexToAllKeyframes(int rgnindx, double locus);
   void DeleteVerticesFromAllKeyframes(int rgnindx, CRegionNode *tmplt);

   void DeleteKeyframe(int frameIndex);
   void DeleteEmptyKeyframes();
   void DeleteAllKeyframes();
   void ClearAll();

   void CapturePDLEntry(CPDLElement *listElement, CMaskTool *maskTool);
   int  ParsePDLKeyframe(CPDLElement *pdlKeyframe, CMaskTool *maskTool);

public:
   static const string tag_MaskTimeline;

private:

private:
   EMaskRegionShape regionShape;
   list<CMaskKeyframe*> keyframeList;
};


// ---------------------------------------------------------------------------

class CMaskToolAnimateTimelineIF : public CAnimateTimelineInterface
{
public:
   CMaskToolAnimateTimelineIF(CMaskTool *mtp) : maskTool(mtp) {};

   bool GetFirstKeyframe(int &keyframeIndex, bool &inclusion);
   bool GetNextKeyframe(int &keyframeIndex, bool &inclusion);
   bool GetPreviousKeyframe(int &keyframeIndex, bool &inclusion);
   bool IsKeyframeHere(int frameIndex);

   void DeleteKeyframe(int frameIndex);
   void DeleteAllKeyframes();

private:
   CMaskTool *maskTool;

};

///////////////////////////////////////////////////////////////////////////////

class CMaskTool
{
friend class CMaskToolAnimateTimelineIF;

public:
   CMaskTool();
   virtual ~CMaskTool();

   CAnimateTimelineInterface *GetAnimateTimelineInterface() {return _animateIFPtr;};

   int DrawMask(int frameIndex); // private


   void setMaskToolActivationAllowed(bool normalModeAllowed, bool roiModeAllowed = false, bool activateRoiMode = false);
   void ActivateNormalMode(bool newActive);
   bool ActivateRoiMode(bool newActive);
   bool ToggleRoiModeActivation();

   bool IsMaskToolActivationAllowed() const;
   bool IsMaskToolActive() const;
   bool IsMaskRoiModeActivationAllowed() const;
   bool IsMaskRoiModeActive() const;
   bool IsMaskInclusive() const;
   bool IsMaskAnimated() const;
   bool IsMaskAvailable() const;
   bool IsMaskVisible() const;
   bool IsMaskVisibilityLockedOff() const;

   bool IsMaskBeingDrawn() const { return _maskBeingDrawn; }
   void SetMaskBeingDrawn(bool value) { _maskBeingDrawn = value; }

   bool AreAnyMaskRegionsVisible(CRegionNode *rgnList) const;
   bool AreThereAnyMaskRegions(CRegionNode *rgnList) const;
   bool IsTransforming() const;

   int MouseMove(int x, int y);

   int SelectDn();
   int SelectUp();
   int SelectShiftDn();
   int SelectShiftUp();
   int SelectAltDn();
   int SelectAltUp();
   int SelectCtrlDn();
   int SelectCtrlUp();
   int SelectCtrlShiftDn();
   int SelectCtrlShiftUp();

//   void StartRoiExclusiveMode();
//   void EndRoiExclusiveMode();
   void setAutoActivateRoiModeOnDraw(bool flag);
   int HandleShiftRightButtonDown(bool altFlag);
   int HandleShiftRightButtonUp();

   void ClearAll();
   void DeleteSelected();
   bool HideSelected();
   void SelectAll();
   void UnselectAll();
   bool IsAnythingSelected();
   void ChangeBorder();

   void SetKeyframe(int frameIndex);
   void AddRegionToAllKeyframes(CRegionNode *);
   void AddVertexToAllKeyframes(int, double);
   void DeleteKeyframe(int frameIndex);
   void DeleteAllKeyframes(bool doRedraw);

   void CalculateRegionOfInterest(int);
   CRegionOfInterest* GetRegionOfInterest();
   int GetRegionOfInterest(int frameIndex, CRegionOfInterest &maskROI, double scale = 1);

   void CapturePDLEntry(CPDLEntry &pdlEntry);
   void CaptureRegions(CPDLElement *parentElement,
                       CRegionNode *rgnlst);
   void SetMask(CPDLEntry &pdlEntry, bool doNotActivateTheFuckingTool = false);
   CRegionNode* ParsePDLMaskRegion(CPDLElement *pdlMaskRegion);

   double getDefaultExclusionBlendSlew() const;
   int getDefaultExclusionBorderWidth() const;
   double getDefaultInclusionBlendSlew() const;
   int getDefaultInclusionBorderWidth() const;
   bool getGlobalInclusionFlag() const;
   bool getMaskRoiModeFlag() const;
//   int getMaskRoiButtonMode() const { return roiButtonTag; }
   void RestoreLastRoiMask();

   int getInclusiveTotalBorderWidth() const;
   void setInclusiveTotalBorderWidth(int width);

   double getInclusiveNormalizedBlend() const;
   void setInclusiveNormalizedBlend(double value);

   EMaskRegionShape getRegionShape() const;

   void setClipActivePictureRect(const RECT &newActiveRect);

   void setDefaultExclusionBlendSlew(double newBlendSlew);
   void setDefaultInclusionBlendSlew(double newBlendSlew);
   void setDefaultExclusionBorderWidth(int newBorderWidth);
   void setDefaultInclusionBorderWidth(int newBorderWidth);
   void setGlobalInclusionFlag(bool newInclusionFlag);
   void setMaskVisible(bool newVisible);
   void setMaskVisibilityOffLock(bool flag);
   void setRegionShape(EMaskRegionShape newRegionShape);
   void cycleRegionShape();
   EMaskRegionShape GetMaskRoiModeShape(RECT &rect,
                    POINT *lasso,
                    BEZIER_POINT *bezier);
   void SetMaskRoiModeShape(EMaskRegionShape shape,
                         const RECT &rect,
                         const POINT *lasso,
                         const BEZIER_POINT *bezier);
   void LockMaskRoiModeMask(bool lockFlag);
   void setRoiIsRectangularOnly(bool flag);
   bool getRoiIsRectangularOnly();

   int AutoSave();
   int SaveMaskToFile(const string &filename);
   int LoadMaskFromFile(const string &filename);
   int SaveMaskToMemory(CPDL* &memoryPDL);
   int LoadMaskFromMemory(CPDL* &memoryPDL, bool doNotActivateTheFuckingTool = false);
   void DestroySavedMemoryMask(CPDL* &memoryPDL);
   string GetMaskAsString();

   void setOneShotRoiMaskColor(int color);

   void saveEnabledStateAndDisable();
   void restoreSavedEnabledState();

   void setSystemAPI(CToolSystemInterface *newSystemAPI);

   int toolRestoreSettings();
   int toolSaveSettings();

   void setMaskTransformEditorIdleRefreshMode(bool onOffFlag);
   bool getMaskTransformEditorIdleRefreshMode();

   Ipp8uArray generateOverlayMask(Ipp8uArray mask, const IppiSize &targetSize, bool displayMaskInterior=false);

   bool isAnyMaskSelected();
   Ipp8uArray ComputeSmallMask(const IppiSize &sourceSize, int newWidth);

   vector<VertexList> CopyVertexLists();
   MaskParameters getUncomputedMaskParameters();

   static const string tag_MaskRegion;
   bool isRegionOfInterestAvailable() const { return _regionOfInterestAvailable; }
   void setRegionOfInterestAvailable(bool value) { _regionOfInterestAvailable = value; }

   void setRectAsMask(const RECT &rect);
   CMaskTransformEditor *getMaskTransfomEditor() { return _maskTransformEditor; }

private:
   friend class MaskChangedAutoNotifier;

   CRegionNode *SetKeyframeContext();

   void RefreshFrame();

   void CreateRegionList();
   void DeleteRegionList();
   CRegionNode *CreateRegionNode();
   CRegionNode *AppendRegionNode(CRegionNode *);
   CRegionNode *DeleteRegionNode(CRegionNode *);

   CToolSystemInterface* getSystemAPI();

   void ClearMask();

   void ClearRegionOfInterest();
   int RenderRegionOfInterest(bool forceRender=false);
   int RenderRegionOfInterest(CRegionOfInterest &maskROI,
                              CRegionNode *rgnlst, double scale = 1);

   bool WriteSettings(CIniFile *iniFile, const string &sectionName);
   bool ReadSettings(CIniFile *iniFile, const string &sectionName);

   CMaskToolTimeline* ParsePDLMaskTimeline(CPDLElement *timelineElement);
   string createDateAndTimeString();

   /////////

   bool _maskToolActiveFlag = false;
   bool _maskToolActivationAllowedFlag = false;
   bool _roiModeActiveFlag = false;
   bool _roiModeActivationAllowedFlag = false;
   bool _maskToolSaveState = false;

   EMaskRegionShape _maskRoiRegionShape = MASK_REGION_RECT;
   EMaskRegionShape _maskRegionShape = MASK_REGION_RECT;
   bool _globalInclusionFlag = true; // true for include, false for exclude
   bool _maskIsVisibleFlag = true;
   bool _maskVisibilityIsLockedOff = false;
   bool _previousMaskToolActivationState = false;
   bool _autoActivateRoiModeOnDrawFlag = false;
   bool _autoRoiModeActivationIsPending = false;

   double _defaultExclusionBlendSlew = 0; // inside, outside, centered
   int _defaultExclusionBorderWidth = 0;
   double _defaultInclusionBlendSlew = 0; // inside, outside, centered
   int _defaultInclusionBorderWidth = 0;

   CToolSystemInterface *_baseSystemAPI = nullptr;

   // This should be a stack, but I really don't care
   int _oneTimeMaskColor = INVALID_COLOR;
   bool _maskBeingDrawn = false;

   EMaskToolMouseMode mouseMode = MASK_TOOL_MOUSE_MODE_NONE;

   // client coords
   int _mouseX = -1;
   int _mouseY = -1;

   int _mouseXAtClick = -1;
   int _mouseYAtClick = -1;

   // frame coords
   int _mouseFrameX = -1;
   int _mouseFrameY = -1;

   // Mask Regions
   // NB: There are two mask region structures because it was easier
   //     to implement with three people (John Starr, Kevin Manbeck &
   //     Kurt Tolksdorf) working on different pieces.  We probably want
   //     to refactor these two classes, perhaps merging them or spliting
   //     them into more distinct classes.
   CMaskToolRegionList _regionList;
   CRegionOfInterest _regionOfInterest;

   bool _regionOfInterestAvailable = false; // true if the region of interest has
                                    // been calculated.  We do deferred
                                    // rendering of mask to save time while
                                    // the user is editing the mask
   bool _regionOfInterestAllocated = false;  // true if masks have been allocated for
                                    // the region of interest.  We try to
                                    // defer allocation until it is needed
//   bool _regionOfInterestFullFrame = false;  // true if the last region of interest
//                                    // calculated consisted of the full frame
   bool _roiIsRectangularOnly = false;

   RECT _clipActivePictureRect = {0x7fffffff, 0x7fffffff, 0x80000000, 0x80000000};

#define SELECT_NONE       0
#define SELECT_CTRL       1
#define SELECT_SHIFT      2
#define SELECT_CTRL_SHIFT 3
#define SELECT_ALT        4
   int  _selectModKeys = SELECT_NONE;

   bool _stretchingRectangle = false;
   int _currentFrame = -1;
   bool _isEdited = false;
   bool _isClosed = false;
   CMaskKeyframe *_globalKeyframe = nullptr;
   CRegionNode *_regionJustClosed = nullptr;
   CRegionNode *_regionJustEdited = nullptr;

   int _newVertexRegionIndex = -1;
   double _newVertexLocus = 0.0;

   bool _bezierExists = false;       // kludge to monitor existence of bezier

   CMaskTransformEditor *_maskTransformEditor = nullptr;
   bool _maskTransformEditorIdleRefreshMode = false;

   CMaskToolTimeline *_timeline = nullptr;
   list<CMaskToolTimeline*> _timelineList;
   CAnimateTimelineInterface *_animateIFPtr = nullptr;

   bool _deferredInclusionStupidHackLegacyFlag = false;
   bool _dontDrawVertices = false;

//   bool _inMaskRoiExclMode = false;

   // remember stuff at entry to roi-excl mode... to be restored at exit
//   bool _maskRoiExclOldMaskRoiMode = false;
//   EMaskRegionShape _maskRoiModeOldRegionShape = MASK_REGION_RECT;

   bool _roiModeOldInclusionFlag = false;
   bool _roiModeReverseRectAndLassoFlag = false;

   EMaskRegionShape _maskRoiModeShape = MASK_REGION_INVALID;
   RECT _maskRoiModeRect = {0, 0, 0, -0};
   POINT _maskRoiModeLasso[MAX_LASSO_PTS];
   BEZIER_POINT *_maskRoiModeBezier = nullptr;
   bool _maskRoiModeLockFlag = false;

   // Where to save a mask in memory
   CPDL *_maskMemoryPDL = nullptr;
   CPDL *_maskRoiMemoryPDL = nullptr;

private:
   // Private static variables
   static string maskToolSectionName;
   static string maskToolEnabledKey;
   static string exclusionBorderWidthKey;
   static string exclusionBlendSlewKey;
   static string inclusionBorderWidthKey;
   static string inclusionBlendSlewKey;

   static const string enabledAttr;
   static const string inclusionModeAttr;
   static const string blendSlewAttr;

//   static const string blendPositionCentered;
//   static const string blendPositionInside;
//   static const string blendPositionOutside;
   static const string borderWidthAttr;
   static const string inclusionFlagAttr;
   static const string hiddenFlagAttr;
   static const string pointsAttr;
   static const string shapeTypeAttr;
   static const string shapeTypeBezier;
   static const string shapeTypeLasso;
   static const string shapeTypeRect;
   static const string shapeTypeQuad;

};

///////////////////////////////////////////////////////////////////////////////

#endif // #if !defined(MASK_TOOL_H)
