#ifndef NavigatorToolH
#define NavigatorToolH

#include "ToolObject.h"

//////////////////////////////////////////////////////////////////////

// Navigator Tool Number (16-bit number that uniquely identifies this tool)
#define NAVIGATOR_TOOL_NUMBER    1

// Navigator Tool Command Numbers

// Navigation
#define NAV_CMD_NO_OP                     0
#define NAV_CMD_REWIND                    1
#define NAV_CMD_PLAY_REV                  2
#define NAV_CMD_STOP                      3
#define NAV_CMD_PLAY                      4
#define NAV_CMD_FAST_FWD                  5
#define NAV_CMD_JOG_REV_ONETHIRD          6
#define NAV_CMD_JOG_FWD_ONETHIRD          7
#define NAV_CMD_JOG_REV_CONTINUOUS        8
#define NAV_CMD_JOG_FWD_CONTINUOUS        9
#define NAV_CMD_JOG_REV_STOP              10
#define NAV_CMD_JOG_FWD_STOP              11
#define NAV_CMD_JOG_REV_8_FRAMES          12
#define NAV_CMD_JOG_FWD_8_FRAMES          13
#define NAV_CMD_JOG_REV_1_FRAME           14
#define NAV_CMD_JOG_FWD_1_FRAME           15
#define NAV_CMD_GOTO_IN                   16
#define NAV_CMD_GOTO_OUT                  17
#define NAV_CMD_GOTO_MARK_IN              18
#define NAV_CMD_GOTO_MARK_OUT_MINUS_ONE   19
#define NAV_CMD_LOOP_CLIP                 20
#define NAV_CMD_PLAY_CLIP                 21
#define NAV_CMD_LOOP_AROUND_CURRENT_FRAME 22
#define NAV_CMD_LOOP_MARKS                23
#define NAV_CMD_PLAY_MARKS                24
#define NAV_CMD_PLAY_MARKS_TOOL           25
#define NAV_CMD_ALL_STOP                  26
#define NAV_CMD_PLAY_FWD_CONTINUOUS       27
#define NAV_CMD_PLAY_FWD_STOP             28
#define NAV_CMD_PLAY_REV_CONTINUOUS       29
#define NAV_CMD_PLAY_REV_STOP             30

// Marks
#define NAV_CMD_MARK_IN                   31
#define NAV_CMD_SET_MARK_IN               32
#define NAV_CMD_REMOVE_MARK_IN            33
#define NAV_CMD_MARK_OUT_INCLUSIVE        34
#define NAV_CMD_MARK_OUT_EXCLUSIVE        35
#define NAV_CMD_MARK_ALL                  36
#define NAV_CMD_SET_MARK_OUT              37
#define NAV_CMD_REMOVE_MARK_OUT           38
#define NAV_CMD_CLEAR_MARKS               39
#define NAV_CMD_EXPAND                    40
#define NAV_CMD_UNEXPAND                  41
#define NAV_CMD_MARK_SHOT                 42

// Zoom and pan
#define NAV_CMD_ZOOM_ALL                  43
#define NAV_CMD_ZOOM_IN                   44
#define NAV_CMD_ZOOM_OUT                  45
#define NAV_CMD_PANSTART                  46
#define NAV_CMD_PANSTOP                   47
#define NAV_CMD_JOG_PAN_MODE              48

// Overlays
#define NAV_CMD_DRAW_RETICLE              49
#define NAV_CMD_TOGGLE_HIGHLIGHTS         50
#define NAV_CMD_TOGGLE_ORIGINAL_VALUES    51
#define NAV_CMD_TOGGLE_HIGHLIGHT_AND_ORIG 52

// Timeline timecode
#define NAV_CMD_COPY_TIMECODE             53
#define NAV_CMD_GOTO_CUE                  54

// Timeline visibility
#define NAV_CMD_SHOW_MASK_TIMELINE        55
#define NAV_CMD_HIDE_MASK_TIMELINE        56
#define NAV_CMD_SHOW_CUTS_TIMELINE        57
#define NAV_CMD_HIDE_CUTS_TIMELINE        58
#define NAV_CMD_SHOW_BOOKMARK_TIMELINE    59
#define NAV_CMD_HIDE_BOOKMARK_TIMELINE    60
#define NAV_CMD_SHOW_REFERENCE_TIMELINE   61
#define NAV_CMD_HIDE_REFERENCE_TIMELINE   62

// Timeline ops
#define NAV_CMD_NEXT_EVENT                63
#define NAV_CMD_PREV_EVENT                64
#define NAV_CMD_CUT_DEL                   65
#define NAV_CMD_CUT_ADD                   66
#define NAV_CMD_CUT_NXT                   67
#define NAV_CMD_CUT_PRV                   68
#define NAV_CMD_ADD_BOOKMARK              69
#define NAV_CMD_DROP_BOOKMARK             70

// Bin Manager
#define NAV_CMD_OPEN_BIN_MANAGER          71
#define NAV_CMD_TOGGLE_LAST_TWO_CLIPS     72
#define NAV_CMD_NEXT_CLIP                 73
#define NAV_CMD_PREVIOUS_CLIP             74
#define NAV_CMD_NEXT_VERSION_CLIP         75
#define NAV_CMD_PREVIOUS_VERSION_CLIP     76

// Tool selection
#define NAV_CMD_SELECT_TOOL_1             77
#define NAV_CMD_SELECT_TOOL_2             78
#define NAV_CMD_SELECT_TOOL_3             79
#define NAV_CMD_SELECT_TOOL_4             80
#define NAV_CMD_SELECT_TOOL_5             81
#define NAV_CMD_SELECT_TOOL_6             82
#define NAV_CMD_SELECT_TOOL_7             83
#define NAV_CMD_SELECT_TOOL_8             84
#define NAV_CMD_SELECT_TOOL_9             85
#define NAV_CMD_SELECT_TOOL_10            86
#define NAV_CMD_SELECT_TOOL_11            87

// Numpad
#define NAV_CMD_EDIT_TIMECODE_0           88
#define NAV_CMD_EDIT_TIMECODE_1           89
#define NAV_CMD_EDIT_TIMECODE_2           90
#define NAV_CMD_EDIT_TIMECODE_3           91
#define NAV_CMD_EDIT_TIMECODE_4           92
#define NAV_CMD_EDIT_TIMECODE_5           93
#define NAV_CMD_EDIT_TIMECODE_6           94
#define NAV_CMD_EDIT_TIMECODE_7           95
#define NAV_CMD_EDIT_TIMECODE_8           96
#define NAV_CMD_EDIT_TIMECODE_9           97
#define NAV_CMD_EDIT_TIMECODE_PLUS        98
#define NAV_CMD_EDIT_TIMECODE_MINUS       99
#define NAV_CMD_EDIT_TIMECODE_MULTIPLY    100
#define NAV_CMD_EDIT_TIMECODE_DIVIDE      101

// Window visibility
#define NAV_CMD_TOGGLE_TOOL_PALETTE       102
#define NAV_CMD_TOGGLE_PDL_VIEWER         103
#define NAV_CMD_FOCUS_MAIN_WINDOW         104
#define NAV_CMD_TOGGLE_HELP_WINDOW        105
#define NAV_CMD_HIDE_TOOL_PALETTE         106
#define NAV_CMD_SHOW_TOOL_PALETTE         107
#define NAV_CMD_TOGGLE_BOOKMARK_VIEWER    108

// Main menu shortcuts
#define NAV_CMD_ALLOW_MENU_SHORTCUTS      109
#define NAV_CMD_DONT_ALLOW_MENU_SHORTCUTS 110

// Popup loupe (magnifier)
#define NAV_CMD_LOUPE_TOGGLE_CROSSHAIRS   111
#define NAV_CMD_TOGGLE_LOUPE_DOWN         112
#define NAV_CMD_TOGGLE_LOUPE_UP           113
#define NAV_CMD_LOUPE_4X_MAGNIFICATION    114
#define NAV_CMD_LOUPE_5X_MAGNIFICATION    115
#define NAV_CMD_LOUPE_6X_MAGNIFICATION    116
#define NAV_CMD_LOUPE_7X_MAGNIFICATION    117
#define NAV_CMD_LOUPE_8X_MAGNIFICATION    118
#define NAV_CMD_LOUPE_MAGNIFICATION_UP    119
#define NAV_CMD_LOUPE_MAGNIFICATION_DOWN  120

//#define ---- AVAILABLE --------------   121

// Larry hacks
#define NAV_CMD_SET_BOOKMARK_IN           122
#define NAV_CMD_SET_BOOKMARK_OUT          123
#define NAV_CMD_GO_TO_BOOKMARK_IN         124
#define NAV_CMD_GO_TO_BOOKMARK_OUT        125
#define NAV_CMD_TOGGLE_PRES_MODE_TC_OLAY  126
#define NAV_CMD_TOGGLE_TIMELINE_RIBBON_OLAY 127

// Modifier keys
#define NAV_CMD_SHIFT_RBUTTON_DOWN        128
#define NAV_CMD_CTRL_SHIFT_RBUTTON_DOWN   129
#define NAV_CMD_SHIFT_RBUTTON_UP          130

// Added later, don't feel like renumbering!
#define NAV_CMD_TOGGLE_FIX				      131
#define NAV_CMD_ENTER_MOUSE_ZOOM_MODE	   132
#define NAV_CMD_EXIT_MOUSE_ZOOM_MODE	   133
#define NAV_CMD_ENTER_TC_STYLE_CHANGE_MODE  134
#define NAV_CMD_EXIT_TC_STYLE_CHANGE_MODE   135

 // SEE COMMENT BELOW!


////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// THIS VALUE MUST BE SET EQUAL OR GREATER TO HIGHEST NUMBERED COMMAND ABOVE  //
//                                                                            //
#define NAV_CMD_MAX_REAL_NAV_CMD          135                                 //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////


//-----------------------------------
#define NAV_CMD_MASK_TOOL                 (NAV_CMD_MAX_REAL_NAV_CMD+1)
#define NAV_CMD_MASK_SELECT_DN            (NAV_CMD_MASK_TOOL+0)
#define NAV_CMD_MASK_SELECT_UP            (NAV_CMD_MASK_TOOL+1)
#define NAV_CMD_MASK_SELECT_SHIFT_DN      (NAV_CMD_MASK_TOOL+2)
#define NAV_CMD_MASK_SELECT_SHIFT_UP      (NAV_CMD_MASK_TOOL+3)
#define NAV_CMD_MASK_SELECT_CTRL_DN       (NAV_CMD_MASK_TOOL+4)
#define NAV_CMD_MASK_SELECT_CTRL_UP       (NAV_CMD_MASK_TOOL+5)
#define NAV_CMD_MASK_SELECT_CTRL_SHIFT_DN (NAV_CMD_MASK_TOOL+6)
#define NAV_CMD_MASK_SELECT_CTRL_SHIFT_UP (NAV_CMD_MASK_TOOL+7)
#define NAV_CMD_MASK_DRAW_BEZIER          (NAV_CMD_MASK_TOOL+8)
#define NAV_CMD_MASK_DRAW_LASSO           (NAV_CMD_MASK_TOOL+9)
#define NAV_CMD_MASK_DRAW_RECT            (NAV_CMD_MASK_TOOL+10)
#define NAV_CMD_MASK_EXCLUDE              (NAV_CMD_MASK_TOOL+11)
#define NAV_CMD_MASK_INCLUDE              (NAV_CMD_MASK_TOOL+12)
#define NAV_CMD_MASK_INVERT               (NAV_CMD_MASK_TOOL+13)
#define NAV_CMD_MASK_SELECT               (NAV_CMD_MASK_TOOL+14)
#define NAV_CMD_MASK_TOOL_ENBL_DSBL       (NAV_CMD_MASK_TOOL+15)
#define NAV_CMD_MASK_TOOL_ENABLE          (NAV_CMD_MASK_TOOL+16)
#define NAV_CMD_MASK_TOOL_DISABLE         (NAV_CMD_MASK_TOOL+17)
#define NAV_CMD_MASK_SELECT_ALL_REGIONS   (NAV_CMD_MASK_TOOL+18)
#define NAV_CMD_MASK_HIDE_SELECTED        (NAV_CMD_MASK_TOOL+19)
#define NAV_CMD_MASK_DELETE_SELECTED      (NAV_CMD_MASK_TOOL+20)
#define NAV_CMD_MASK_TOGGLE_MASK_VS_ROI   (NAV_CMD_MASK_TOOL+21)
#define NAV_CMD_MASK_SHOW_NOTHING         (NAV_CMD_MASK_TOOL+22)
#define NAV_CMD_MASK_SHOW_OUTLINE         (NAV_CMD_MASK_TOOL+23)
#define NAV_CMD_MASK_SHOW_INCLUSION       (NAV_CMD_MASK_TOOL+24)
#define NAV_CMD_MASK_SHOW_EXCLUSION       (NAV_CMD_MASK_TOOL+25)
#define NAV_CMD_MASK_CHANGE_BORDER        (NAV_CMD_MASK_TOOL+26)
#define NAV_CMD_MASK_SET_KEYFRAME         (NAV_CMD_MASK_TOOL+27)
#define NAV_CMD_MASK_DROP_KEYFRAME        (NAV_CMD_MASK_TOOL+28)
#define NAV_CMD_MASK_DROP_ALL_KEYFRAMES   (NAV_CMD_MASK_TOOL+29)
#define NAV_CMD_MASK_SELECT_ALT_DN        (NAV_CMD_MASK_TOOL+30)
#define NAV_CMD_MASK_SELECT_ALT_UP        (NAV_CMD_MASK_TOOL+31)
#define NAV_CMD_MASK_CYCLE_SHAPES         (NAV_CMD_MASK_TOOL+32)
#define NAV_CMD_MASK_LOCK_VISIBILITY_OFF  (NAV_CMD_MASK_TOOL+33)
#define NAV_CMD_MASK_UNLOCK_VISIBILITY_OFF (NAV_CMD_MASK_TOOL+34)
#define NAV_CMD_MASK_AUTOCLEAN_MODE   	   (NAV_CMD_MASK_TOOL+35)
#define NAV_CMD_MASK_AUTOCLEAN_MASK   	   (NAV_CMD_MASK_TOOL+36)
#define NAV_CMD_MASK_TOGGLE_VISIBLE       (NAV_CMD_MASK_TOOL+37)
#define NAV_CMD_MASK_TOGGLE_PROPERTIES    (NAV_CMD_MASK_TOOL+38)

//-----------------------------------
#define NAV_CMD_RETICLE_TOOL_ENABLE       (NAV_CMD_MASK_TOOL+39)
#define NAV_CMD_RETICLE_TOOL_DISABLE      (NAV_CMD_RETICLE_TOOL_ENABLE+1)
#define NAV_CMD_RETICLE_SELECT_DN          NAV_CMD_MASK_SELECT_DN
#define NAV_CMD_RETICLE_SELECT_UP          NAV_CMD_MASK_SELECT_UP
//-----------------------------------

// These call back into the GOV tool
#define NAV_GOV_CMD_REVIEW_PREV           (NAV_CMD_RETICLE_TOOL_ENABLE+2)
#define NAV_GOV_CMD_REVIEW_NEXT           (NAV_GOV_CMD_REVIEW_PREV+1)

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CMaskTool;
class CReticleTool;
class CRsrcCtrl;

#ifdef __sgi
class MainWindow;
#endif // #ifdef __sgi

//////////////////////////////////////////////////////////////////////

class CNavigatorTool : public CToolObject
{
public:
   CNavigatorTool(const string &newToolName);
   virtual ~CNavigatorTool();

   virtual int toolInitialize(CToolSystemInterface *newSystemAPI);
   virtual int toolShutdown();

   virtual CToolProcessor* makeToolProcessorInstance(
                                          const bool *newEmergencyStopFlagPtr);

   // Tool Event Handlers
   virtual int onToolCommand(CToolCommand &toolCommand);
   virtual int onMouseMove(CUserInput &userInput);
   virtual int onNewClip();
   virtual int onRedraw(int frameIndex);

   // Static method to create a new instance of this tool
   static CToolObject* MakeTool(const string& newToolName);

#ifdef __sgi
   // Static function to set pointer to Main Window so Navigator Tool
   // can access it.
   static void setMainWindowPointer(MainWindow *newMainWindow);  
#endif // #ifdef __sgi

   static CMaskTool* GetMaskTool();
   static CReticleTool* GetReticleTool();
   static int ExecuteMaskToolCommand(int commandNumber);
   static int ExecuteReticleToolCommand(int commandNumber);

   // Larry's AutoClean 'A key' hack
   bool seeIfTheMaskToolWantsTheAKey();
   void tellTheMaskToolToUnselectAll();
   bool askTheMaskToolIfAnythingIsSelected();
   void setRectToMask(const RECT &rect);

private:
   enum ENavigatorState
   {
      NAV_STATE_READY_FOR_CMD,
      NAV_STATE_JOG_REV_CONTINUOUS,
      NAV_STATE_JOG_FWD_CONTINUOUS,
      NAV_STATE_PLAY_FWD_CONTINUOUS,
      NAV_STATE_PLAY_REV_CONTINUOUS
   };
   ENavigatorState navigatorState;

#ifdef __sgi
   static MainWindow *mainWindow;  // Navigator Tool's  pointer to Main Window, 
                                   // since it is not a global
#endif // #ifdef __sgi

	static CMaskTool *maskTool;

   static CReticleTool *reticleTool;

   bool saveMaskToolState;

   CRsrcCtrl *rsrcCtrl;
   bool licenseIsOK;

   bool weAreInDotMode;  // true after odd "." commands, else false
   bool toolbarCorralWasVisibleBeforeDotMode;
   bool toolPaletteWasVisibleBeforeDotMode;

   bool _tcChangeStyleKeyIsDown = false;
   bool _tcChangeKeyWasHeldDown = false;

   // loupe crap
   bool hideLoupePendingMode;
   bool settingLoupeParameters;
   bool loupeVisibilityRememberer;

   void HandleToolHotKey(int tabIndex);
   void ReviewToolSingleStep(bool goNext, bool goZoom);
};

//////////////////////////////////////////////////////////////////////

#endif // !defined(NAVIGATORTOOL_H)
