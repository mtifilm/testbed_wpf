#ifndef BaseToolH
#define BaseToolH

// BaseTool.h: interface for the CBaseTool class.
//
/*
$Header: /usr/local/filmroot/Nav_common/include/Attic/BaseTool.h,v 1.1.2.1 2008/01/23 12:12:04 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "ToolObject.h"

//////////////////////////////////////////////////////////////////////

// Base Tool Number (16-bit number that uniquely identifies this tool)
#define BASE_TOOL_NUMBER    12345

// Base Tool Command Numbers
#define BASE_CMD_NOOP                    1
#define BASE_CMD_ARROW_UP                        24
#define BASE_CMD_ARROW_DOWN                      25
#define BASE_CMD_ARROW_LEFT                      26
#define BASE_CMD_ARROW_RIGHT                     27
#define BASE_CMD_SHIFT_ARROW_UP                  28
#define BASE_CMD_SHIFT_ARROW_DOWN                29
#define BASE_CMD_SHIFT_ARROW_LEFT                30
#define BASE_CMD_SHIFT_ARROW_RIGHT               31
#define BASE_CMD_CTRL_ARROW_UP                   32
#define BASE_CMD_CTRL_ARROW_DOWN                 33
#define BASE_CMD_CTRL_ARROW_LEFT                 34
#define BASE_CMD_CTRL_ARROW_RIGHT                35

//////////////////////////////////////////////////////////////////////

class CBaseTool : public CToolObject
{
public:
   CBaseTool(const string &newToolName);
   virtual ~CBaseTool();

   virtual int toolInitialize(CToolSystemInterface *newSystemAPI);
   virtual int toolShutdown();

   virtual CToolProcessor* makeToolProcessorInstance(
                                          const bool *newEmergencyStopFlagPtr);

   // Tool Event Handlers
   virtual int onToolCommand(CToolCommand &toolCommand);
   virtual int onMouseMove(CUserInput &userInput);
   virtual int onNewClip();
   virtual int onRedraw(int frameIndex);

   // Static method to create a new instance of this tool
   static CToolObject* MakeTool(const string& newToolName);

private:
   bool SendArrowsKeysToTrackBar(int commandNumber);

};

//////////////////////////////////////////////////////////////////////

// Only one of these
extern CBaseTool *GBaseTool;

#endif // !defined(BASETOOL_H)
