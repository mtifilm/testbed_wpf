// Provisional.cpp implementation of the the CProvisional class
//
/*
 $Header: /usr/local/filmroot/Nav_common/source/Provisional.cpp,v 1.29.2.11 2009/03/09 07:59:00 mbraca Exp $
 */
//////////////////////////////////////////////////////////////////////

#include "Provisional.h"
#include "bthread.h"
#include "MTIsleep.h"
// #include "NavSystemInterface.h"
#include "PixelRegions.h"
#include "Player.h"
#include "Displayer.h"
#include "SaveRestore.h"
#include "ToolCommand.h"
#include "ToolCommandTable.h"
#include "ToolFrameBuffer.h"
#include "ToolManager.h"
#include "ToolNode.h"
#include "ToolUserInputMap.h"
#include "ThreadLocker.h"
#include "Clip3.h"

#include "MainWindowUnit.h"

#include <stddef.h>    // for _threadid macro

//////////////////////////////////////////////////////////////////////
// Static Member Variables

string CProvisional::processedImageStatusMsg = "Processed Image";
string CProvisional::originalImageStatusMsg  = "Original Image";

// ===========================================================================

static MTI_UINT16 provisionalToolNumber = PROVISIONAL_TOOL_NUMBER;

// -------------------------------------------------------------------------
// Provisional Keyboard and Mouse Button Configuration
// (The Provisional Tool does not have a GUI and does not accept user input,
// so these tables contain only a single dummy command to keep the
// tool initialization function happy)

static CUserInputConfiguration::SConfigItem provisionalDefaultConfigItems[] =
{
   // Provisional Command                Modifiers      Action
   // + Key
   // ----------------------------------------------------------------
   // Dummy Command
   {PROVISIONAL_CMD_DUMMY, " A            KeyDown   "}, };

static CUserInputConfiguration *provisionalUserInputConfiguration =
      new CUserInputConfiguration(sizeof(provisionalDefaultConfigItems) / sizeof(CUserInputConfiguration::SConfigItem),
   provisionalDefaultConfigItems);

// -------------------------------------------------------------------------
// Provisional Command Table

static CToolCommandTable::STableEntry provisionalCommandTableEntries[] =
{
   // Provisional Command              Provisional Command String
   // ----------------------------------------------------------------
   // Dummy Command
   {PROVISIONAL_CMD_DUMMY, "Dummy"}, };

static CToolCommandTable *provisionalCommandTable =
      new CToolCommandTable(sizeof(provisionalCommandTableEntries) / sizeof(CToolCommandTable::STableEntry),
   provisionalCommandTableEntries);

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CProvisional::CProvisional(CSaveRestore *newSaveRestoreEngine)
      : mode(TOOL_SETUP_TYPE_INVALID),
        provisionalFrame(0),
        provisionalFrameCopyForOutput(0),
        lastFrameIndex(-1),
        renderFlag(false),
        highlightFlag(false),
        pendingFlag(false),
        pendingVisible(false),
        reviewToolFlag(false),
        reprocessFlag(false),
        saveRestoreEngine(newSaveRestoreEngine),
        provisionalToolProc(0),
        historySaveCount(0),
        previousHistorySaveCount(0),
        validHistorySaveCount(0),
        lastValidFrameIndex(-1),
        reviewRegionFrameIndex(-1),
        reviewRegion(0),
        foregroundThreadId(GetCurrentThreadId())
{
}

CProvisional::~CProvisional()
{
   // Ummm caller should own this, or we should have allocated it!!!!
   // delete saveRestoreEngine;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CProvisional::setClip(ClipSharedPtr &newClip, int newVideoProxyIndex, int newVideoFramingIndex)
{
   int retVal;

   CVideoFrameList *videoFrameList = newClip->getVideoFrameList(newVideoProxyIndex, newVideoFramingIndex);
   if (videoFrameList == 0)
      return -1;

   retVal = saveRestoreEngine->setClip(newClip);
   if (retVal != 0)
      return retVal; // ERROR:

   retVal = saveRestoreEngine->verifyHeaderFile();
   if (retVal != 0)
      return retVal; // ERROR

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Toggle, Accept & Reject
//////////////////////////////////////////////////////////////////////

int CProvisional::Toggle(bool goZoom)
{
   CAutoThreadLocker atl(provisionalThreadLock); // Atomic access to pendingFlag
   // and provisionalFrame

   MTIassert(foregroundThreadId == (LONG)GetCurrentThreadId()); // CAN'T CALL FROM BACKGROUND

   if (mode == TOOL_SETUP_TYPE_INVALID)
      return 0;

   // If nothing pending, do nothing
   if (!pendingFlag)
      return 0;

   CPlayer *player = mainWindow->GetPlayer();

   // Need to stop the player in case it is playing when the user
   // toggles the pending change
   player->fullStop();

   // Determine direction of toggle and set new provisional state
   pendingVisible = !pendingVisible;

   if (pendingVisible)
   {
      if (highlightFlag)
      {
         // Display highlighting
         player->setHighlightColor(0, 65535, 0);
         player->loadProvisionalPixelRegionList(provisionalFrame->GetNewPixelsPixelRegionListPtr());
      }
      else
      {
         player->loadProvisionalPixelRegionList(0);
      }
      player->setProvisionalMode(DISPLAY_PROVISIONAL);
   }
   else
   {
      player->loadProvisionalPixelRegionList(0); // No highlighting

      // Show original
      player->loadProvisionalFrame(provisionalFrame->GetVisibleFrameBufferPtr());
      player->setProvisionalMode(DISPLAY_FROM_CLIP);
   }

   // Ask Player to display the relevant frame
   player->goToFrameSynchronousCached(provisionalFrame->GetFrameIndex());

   // Update status bar based on provisional state
   UpdateStatusBar();

   return 0;
}

int CProvisional::Accept(bool goToFrame)
{
   int retVal;

   if (mode != TOOL_SETUP_TYPE_SINGLE_FRAME)
      return 0;

   retVal = ResolveProvisional(!reviewToolFlag, goToFrame);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CProvisional::AcceptFromBackgroundThread()
{
   int retVal;

   if (mode != TOOL_SETUP_TYPE_SINGLE_FRAME)
      return 0;

   retVal = ResolveProvisionalFromBackgroundThread(!reviewToolFlag);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CProvisional::Reject(bool goToFrame)
{
   int retVal;

   if (mode == TOOL_SETUP_TYPE_INVALID)
      return 0;

   retVal = ResolveProvisional(reviewToolFlag, goToFrame);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CProvisional::RejectFromBackgroundThread()
{
   int retVal;

   if (mode == TOOL_SETUP_TYPE_INVALID)
      return 0;

   retVal = ResolveProvisionalFromBackgroundThread(reviewToolFlag);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CProvisional::ResolveProvisional(bool saveProvisional, bool goToFrame)
{
   CAutoThreadLocker atl(provisionalThreadLock); // Atomic access to pendingFlag
   // and provisionalFrame

   MTIassert(foregroundThreadId == (LONG)GetCurrentThreadId()); // CAN'T CALL FROM BACKGROUND

   if (!pendingFlag)
      return 0; // Nothing to do

   // Resolve the disposition of the history records either saying
   // they should be kept or flushed.
   if (!saveProvisional)
      DiscardHistory();

   // Remember the frame index
   int frameIndex = provisionalFrame->GetFrameIndex();

   CPlayer *player = mainWindow->GetPlayer();

   if (saveProvisional || goToFrame)
      player->fullStop();

   // Update the Provisional state flags.  Do this before any drawing
   // so that a tool object's onRedraw function sees the up-to-date
   // provisional state.
   pendingVisible = false;
   pendingFlag    = false;

   // Tell player no highlighting
   player->loadProvisionalPixelRegionList(0);

   if (saveProvisional)
   {
      // Tell the player to display the provisional one last time since
      // it needs to be redrawn, but it is not on the disk yet
      player->setProvisionalFrameIndex(frameIndex);
      player->loadProvisionalFrame(provisionalFrame->GetVisibleFrameBufferPtr());
      player->setProvisionalMode(DISPLAY_PROVISIONAL);

      // Need to write the provisional to the disk by passing it
      // to the Media Writer.  Don't return until frame is on disk
      // (or in a write cache)
      provisionalToolProc->PutProvisionalFrameSynchronous(provisionalFrame);

      if (player->getLastFrameIndex() != frameIndex)
      {
         // Jump to the frame we just wrote if the caller asked us to
         if (goToFrame)
            player->goToFrameSynchronous(frameIndex);

      }
   }
   else
   {
      // Don't need to save the provisional, so it must be deleted,
      // deallocating image buffers along the way
      delete provisionalFrame;
   }

   provisionalFrame = 0; // Provisional doesn't own the frame anymore

   // Tell player we're done with provisional
   player->clearProvisionalFrameIndex();
   player->setProvisionalMode(DISPLAY_FROM_CLIP);

   // Ask Player to display the relevant frame if it was not written to disk
   if (!saveProvisional && (goToFrame || (frameIndex == player->getLastFrameIndex())))
   {
      player->goToFrameSynchronous(frameIndex);
   }
   else if (saveProvisional && (frameIndex == player->getLastFrameIndex()) &&
      (player->getPlaybackFilter() == PLAYBACK_FILTER_HIGHLIGHT_FIXES))
   {
      // If we are showing the frame that was just written, we need to
      // force a redisplay now to get blue hilites to show up when the
      // filter is set to "show highlights"
      //
      player->refreshFrameSynchronous();
   }

   // Clear the status bar
   UpdateStatusBar();

   return 0;
}

int CProvisional::ResolveProvisionalFromBackgroundThread(bool saveProvisional)
{
   CAutoThreadLocker atl(provisionalThreadLock); // Atomic access to pendingFlag
   // and provisionalFrame

   if (!pendingFlag)
      return 0; // Nothing to do

   // Resolve the disposition of the history records either saying
   // they should be kept or flushed.
   if (!saveProvisional)
      DiscardHistory();

   // Update the Provisional state flags.
   pendingVisible = false;
   pendingFlag    = false;

   CPlayer *player = mainWindow->GetPlayer();
   int frameIndex  = provisionalFrame->GetFrameIndex();

   // Tell player no highlighting
   player->loadProvisionalPixelRegionList(0);

   if (saveProvisional)
   {
      // Need to write the provisional to the disk by passing it
      // to the Media Writer.  Don't return until frame is on disk
      // (or in a write cache)
      provisionalToolProc->PutProvisionalFrameSynchronous(provisionalFrame);
   }
   else
   {
      // Don't need to save the provisional, so it must be deleted,
      // deallocating image buffers along the way
      delete provisionalFrame;
   }

   provisionalFrame = 0; // Provisional doesn't own the frame anymore

   // Tell player we're done with provisional
   player->clearProvisionalFrameIndex();
   player->setProvisionalMode(DISPLAY_FROM_CLIP);

   // Crap. How are we going to reload ("goto") the original frame in
   // case the provisional was rejected?  QQQ

   // Crap. We need to update the status bar.... QQQ

   return 0;
}

int CProvisional::Clear(bool dolastframe)
{
   int retVal;

   MTIassert(foregroundThreadId == (LONG)GetCurrentThreadId()); // CAN'T CALL FROM BACKGROUND

   if (mode == TOOL_SETUP_TYPE_MULTI_FRAME && renderFlag)
   {
      // Just tell the Player we're done with provisional, since once a frame
      // goes to MediaWrite queue it is not managed as a provisional anymore
      CPlayer *player = mainWindow->GetPlayer();
      player->clearProvisionalFrameIndex();
      player->setProvisionalMode(DISPLAY_FROM_CLIP);

      // QQQ DON'T WE HAVE TO DELETE provisionalFrame HERE???

      if (dolastframe) // on request, redisplay the last frame
      {
         // Force redraw of current frame to get rid of provisional or
         // highlighting.  There is no way to know if either are present or if
         // we are on the right frame, so this redraw may be gratuitous.
         player->fullStop(); // force player to read frame from disk
         player->goToFrameSynchronous(player->getLastFrameIndex());
      }
   }
   else
   {
      // We're managing a provisional, so clear it out.
      retVal = ResolveProvisional(false, false);
      if (retVal != 0)
         return retVal; // ERROR
   }

   return 0; // Success
}

//////////////////////////////////////////////////////////////////////
// Setup Functions
//////////////////////////////////////////////////////////////////////

int CProvisional::SetMode(EToolSetupType newMode)
{
   if (mode == TOOL_SETUP_TYPE_SINGLE_FRAME && IsProvisionalPending())
      return -1; // ERROR

   if ((mode == TOOL_SETUP_TYPE_TRAINING || mode == TOOL_SETUP_TYPE_PRELOAD_ONLY) && IsProvisionalPending())
   {
      Clear(true); // Clear the current pending
   }

   mode = newMode;

   // Reset the highlight and render flags
   SetHighlight(false);
   SetRender(false);

   // Clear the reprocess flag
   SetReprocess(false);

   SetReview(false);

   return 0;
}

int CProvisional::SetRender(bool newRenderFlag)
{
   renderFlag = newRenderFlag;

   return 0;
}

int CProvisional::SetHighlight(bool newHighlightFlag)
{
   // MTIassert(foregroundThreadId == GetCurrentThreadId());  // CAN'T CALL FROM BACKGROUND

   if (highlightFlag != newHighlightFlag)
   {
      CAutoThreadLocker atl(provisionalThreadLock); // Atomic access to pendingFlag
      // and provisionalFrame

      highlightFlag = newHighlightFlag;
      if (IsProvisionalPending() && IsProcessedVisible())
      {
         // Redraw with new highlighting
         CPlayer *player = mainWindow->GetPlayer();
         player->fullStop();
         if (highlightFlag)
         {
            // Display highlighting
            player->setHighlightColor(0, 65535, 0);
            player->loadProvisionalPixelRegionList(provisionalFrame->GetNewPixelsPixelRegionListPtr());
         }
         else
         {
            player->loadProvisionalPixelRegionList(0);
         }

         int frameIndex = provisionalFrame->GetFrameIndex();
         player->setProvisionalFrameIndex(frameIndex);
         player->loadProvisionalFrame(provisionalFrame->GetVisibleFrameBufferPtr());
         player->setProvisionalMode(DISPLAY_PROVISIONAL);

         // Display the frame, but only if it is the current frame
         if (frameIndex == player->getLastFrameIndex())
            player->goToFrameSynchronous(frameIndex);
      }
   }

   return 0;
}

int CProvisional::SetReprocess(bool newReprocessFlag)
{
   if (!newReprocessFlag)
      reprocessFlag = false;
   else if (mode == TOOL_SETUP_TYPE_SINGLE_FRAME || mode == TOOL_SETUP_TYPE_TRAINING)
      reprocessFlag = true;

   return 0;
}

int CProvisional::SetReview(bool newReviewFlag)
{
   reviewToolFlag = newReviewFlag;

   return 0;
}

void CProvisional::StartProcessing()
{
   historySaveCount      = 0;
   validHistorySaveCount = 0;
   unvalidatedHistoryList.clear();
   lastValidFrameIndex = -1;
}

void CProvisional::StopProcessing()
{
   previousHistorySaveCount = historySaveCount;
}

//////////////////////////////////////////////////////////////////////
// Get Provisional Status Functions
//////////////////////////////////////////////////////////////////////

bool CProvisional::IsProvisionalPending()
{
   return pendingFlag;
}

bool CProvisional::IsProvisionalUnresolved()
{
   return (mode == TOOL_SETUP_TYPE_SINGLE_FRAME && IsProvisionalPending());
}

bool CProvisional::IsProcessedVisible()
{
   if (!pendingFlag)
      return false; // If provisional isn't pending, then the processed
   // presumably can't be visible

   return (!reviewToolFlag && pendingVisible) || (reviewToolFlag && !pendingVisible);
}

void CProvisional::UpdateStatusBar()
{
   MTIassert(foregroundThreadId == (LONG)GetCurrentThreadId()); // CAN'T CALL FROM BACKGROUND

   // tHERE IS NO STATUS BAR ANYMORE!

   // May not be needed here
   mainWindow->UpdateZoomLevelStatus();
}

int CProvisional::GetLastFrameIndex()
{
   return lastFrameIndex;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

void CProvisional::SetProvisionalToolProc(CProvisionalToolProc *newToolProc)
{
   provisionalToolProc = newToolProc;
}

//////////////////////////////////////////////////////////////////////
// PutFrame, GetFrame
//////////////////////////////////////////////////////////////////////

int CProvisional::PutFrame(CToolFrameBuffer *newProvisionalFrame)
{
   TRACE_3(errout << "        CProvisional::PutFrame ENTER");

   // TBD Need to know what tool this fix is from

   // TBD Keep track of reprocessed fix.  Must be on same frame and from the
   // same tool.  Now it just replaces whatever is pending.

   // TBD: Copy caller's prov frame or just use it as-is, since tool already
   // assumes the frame has been released.  If we hold the caller's
   // frame then we cannot delete the Tool Setup while a fix is pending
   // since deleting the Tool Setup deletes the CBufferPool

   CAutoThreadLocker atl(provisionalThreadLock); // Atomic access to pendingFlag
   // and provisionalFrame

   // Last frame index, used to note where processing has "Paused"
   if (newProvisionalFrame == 0)
   {
      // CProvisionalToolProc calls this function with a NULL argument
      // to indicate that processing has just started, so reset lastFrameIndex
      lastFrameIndex = -1;
      TRACE_3(errout << "        CProvisional::PutFrame RETURN");
      return 0;
   }
   else
      lastFrameIndex = newProvisionalFrame->GetFrameIndex();

   // Clear any review regions that may be displayed
   ClearReviewRegion(false);

   CPlayer *player = mainWindow->GetPlayer();

   if (IsProvisionalPending())
   {
      // A provisional frame is already pending, so I guess we need
      // to delete it
      player->loadProvisionalPixelRegionList(0);

      // Because the provisional frame was never accepted, we can't vouch for
      // its validity here (QQQ maybe we can?) SO we must regrettably destroy
      // the buffer rather than tossing it into the cache
      delete provisionalFrame;
      provisionalFrame = 0;
   }

   // Tell Player about new provisional frame
   CPixelRegionList *newPixels;
   if (highlightFlag)
   {
      // Display highlighting
      newPixels = newProvisionalFrame->GetNewPixelsPixelRegionListPtr();
      player->setHighlightColor(0, 65535, 0); // R, G, B
      if (!(mode == TOOL_SETUP_TYPE_MULTI_FRAME && renderFlag))
      {
         // If previewing in multi-frame mode, then have the player
         // remember the highlight pixels so the user can see the highlight
         // each time they land on this frame.
         player->loadProvisionalPixelRegionList(newPixels);
      }
   }
   else
      newPixels = 0;

   player->setProvisionalFrameIndex(newProvisionalFrame->GetFrameIndex());
   MTI_UINT16 *visibleFrameBuffer = newProvisionalFrame->GetVisibleFrameBufferPtr();
   MTIassert(visibleFrameBuffer != NULL);
   if (visibleFrameBuffer == NULL)
   {
      // I have no FUCKING IDEA why this is NULL or how to recover!!
      lastFrameIndex   = -1;
      pendingVisible   = false;
      pendingFlag      = false;
      provisionalFrame = 0;
      TRACE_3(errout << "        CProvisional::PutFrame BOGUS VFB!!!!!!");
      return 0;
   }

   player->refreshFromIntermediate(newProvisionalFrame->GetVisibleFrameBufferPtr(), newPixels);
   player->setProvisionalMode(DISPLAY_PROVISIONAL);

   if (mode == TOOL_SETUP_TYPE_MULTI_FRAME && renderFlag)
   {
      // Need to write the provisional to the disk by passing it
      // to the Media Writer
      provisionalToolProc->PutProvisionalFrame(newProvisionalFrame, false);

      // Update the Provisional state flags
      pendingVisible = false;
      pendingFlag    = false;

      provisionalFrame = 0;
   }
   else
   {
      // Update the Provisional state flags
      pendingVisible = true;
      pendingFlag    = true;

      // Set new provisional frame
      provisionalFrame = newProvisionalFrame;
   }

   // Added for scratch single-frame render mode QQQ
   // GAAAACK - HANGS DRS - WHY - QQQ
   // UpdateStatusBar();

   TRACE_3(errout << "        CProvisional::PutFrame EXIT");

   return 0;
}

int CProvisional::GetFrame(CToolFrameBuffer **provisionalFrameCopy)
{
   CAutoThreadLocker atl(provisionalThreadLock); // Atomic access to pendingFlag
   // and provisionalFrame

   MTIassert(foregroundThreadId == (LONG)GetCurrentThreadId()); // CAN'T CALL FROM BACKGROUND

   if (!pendingFlag)
      return -1; // ERROR: Provisional not pending

   // Create a new CToolFrameBuffer to return to the caller
   // cloned from the provisional frame
   *provisionalFrameCopy = new CToolFrameBuffer(*provisionalFrame);

   // Allocate image buffer space and copy the pixels, etc.
   // Returns true when done.  Returns false if memory could not be
   // allocated yet. Loop until memory is allocated.
   while (!((*provisionalFrameCopy)->CopyImage(*provisionalFrame, true, true)))
   {
      // Let's free up a cached tool frame just in case we're
      // really running out of buffers
      CToolManager toolManager;
      CToolFrameBuffer *tempFrameBuffer = toolManager.GetToolFrameCache()->GetOldestFrame();
      if (tempFrameBuffer != NULL)
      {
         delete tempFrameBuffer;
      }
      else
      {
         // Wait for buffers to become available
         MTImillisleep(1);
      }
   }

   return 0;
}

//////////////////////////////////////////////////////////////////////
// History Save & Restore
//////////////////////////////////////////////////////////////////////

int CProvisional::SaveToolFrameBuffer(CToolFrameBuffer *frameBuffer, int toolNumber, const string &toolString, bool saveToHistory)
{
   CAutoThreadLocker atl(provisionalThreadLock); // Atomic access to history
   // and provisionalFrame
   int retVal;

   if ((mode == TOOL_SETUP_TYPE_TRAINING) || (mode == TOOL_SETUP_TYPE_PRELOAD_ONLY) ||
      ((mode == TOOL_SETUP_TYPE_MULTI_FRAME) && !renderFlag))
      return 0; // Don't need to save this frame

   if (saveToHistory)
   {

      if (mode == TOOL_SETUP_TYPE_SINGLE_FRAME && reprocessFlag)
      {
         // Discard the previous saved history
         retVal = DiscardHistory();
         if (retVal != 0)
            return 0;
      }

      int saveCount = 0;
      retVal = frameBuffer->SaveToHistory(*saveRestoreEngine, toolNumber, toolString, &saveCount);
      if (retVal != 0)
         return retVal;

      // Remember how many saves we have made to the history
      historySaveCount += saveCount;

      // Unvalidated history cleanup hack for multiframe tools
      if (mode == TOOL_SETUP_TYPE_MULTI_FRAME && renderFlag)
      {
         // In case we need to unwind some history, remember the count after
         // we add the history for this frame
         unvalidatedHistoryList[frameBuffer->GetFrameIndex()] = historySaveCount;
      }
   }
   else
   { // full-frame tool, no history

      frameBuffer->CopyParentHistoryToVersion(*saveRestoreEngine);
   }

   return 0;
}

int CProvisional::DiscardHistory()
{
   // Internal method - caller must lock provisionalThreadLock
   // I'm not sure what the interaction is between this and the new
   // history validation hack
   int retVal;

   if (previousHistorySaveCount > 0)
   {
      // Discard the previous saved history
      retVal = saveRestoreEngine->discardNSaves(previousHistorySaveCount);
      if (retVal != 0)
         return retVal; // ERROR

      previousHistorySaveCount = 0;
   }

   return 0;
}

//////////////////////////////////////////////////////////////////////
// Unvalidated history cleanup hack for multiframe tools -
// Not used for single frame tools because those bypass the tool
// manager media writer and seem to have their own cleanup hack.
//////////////////////////////////////////////////////////////////////

// Every time we successfully write out a frame, we validate its history
void CProvisional::ValidateHistory(int frameIndex)
{
   if (mode == TOOL_SETUP_TYPE_MULTI_FRAME && renderFlag)
   {
      CAutoThreadLocker atl(provisionalThreadLock); // Atomic access to history
      // and provisionalFrame
      map<int, int>::iterator iter = unvalidatedHistoryList.find(frameIndex);
      lastValidFrameIndex = frameIndex;

      if (iter == unvalidatedHistoryList.end())
         return; // frame index not found in map - can't validate

      validHistorySaveCount = (*iter).second;
      unvalidatedHistoryList.erase(iter); // All done with this
   }

}

// In case of stop or error, discard history that was never validated
void CProvisional::DiscardUnvalidatedHistory()
{
   if (mode == TOOL_SETUP_TYPE_MULTI_FRAME && renderFlag)
   {
      CAutoThreadLocker atl(provisionalThreadLock); // Atomic access to history
      // and provisionalFrame
      int retVal = 0;

      if (historySaveCount > validHistorySaveCount)
      {
         int nSavesToDiscard = historySaveCount - validHistorySaveCount;
         historySaveCount    = validHistorySaveCount;

         // Discard the history that was never validated
         retVal = saveRestoreEngine->discardNSaves(nSavesToDiscard);
         if (retVal != 0)
         {
            TRACE_0(errout << "ERROR: CProvisional::DiscardUnvalidatedHistory: " << "Cannot discard history for unwritten frames");
         }
      }

      // All remaining history is valid
      unvalidatedHistoryList.clear();

      // If the history is being discarded for the current provisional frame,
      // clear out the provisional frame
      // CANNOT USE "provisionalFrame" BECAUSE IT MAY HAVE BEEN DELETED
      // int provisionalFrameIndex = provisionalFrame->GetFrameIndex();
      CPlayer *player           = mainWindow->GetPlayer();
      int provisionalFrameIndex = player->getProvisionalFrameIndex();
      if (provisionalFrameIndex > lastValidFrameIndex)
         Clear(true); // true = redisplay to get rid of red crap

   }
}

//////////////////////////////////////////////////////////////////////

int CProvisional::RestoreToolFrameBuffer(CToolFrameBuffer *frameBuffer, RECT *filterRect, POINT *filterLasso, BEZIER_POINT *filterBezier,
   CPixelRegionList *filterPixelRegion)
{
   int retVal;

   retVal = frameBuffer->RestoreFromHistory(*saveRestoreEngine, filterRect, filterLasso, filterBezier, filterPixelRegion);
   if (retVal != 0)
      return 0;

   return 0;
}

int CProvisional::DisplayReviewRegion(int newFrameIndex, CPixelRegionList *newReviewRegion, bool goZoom)
{
   // Check the current mode, pending, etc. to see if it is okay
   // to display the review region
   if (mode == TOOL_SETUP_TYPE_SINGLE_FRAME && IsProvisionalPending())
      return 0; // Can't display review region while something is pending

   // Get rid of current review region if there is one, but don't bother
   // to redraw
   ClearReviewRegion(false);

   CPlayer *player       = mainWindow->GetPlayer();
   CDisplayer *displayer = mainWindow->GetDisplayer();

   /*
    code removed 1/27/05 by KT. Obviously this is never executed
    since the call 'refreshFromIntermediate(0,0)' with a null
    ptr for the intermediate frame would've screwed up. The call
    to 'ClearReviewRegion' guarantees that reviewRegion = 0 at
    this point.

    if (reviewRegion != 0)
    {
    // Tell Player the current review region is going away
    player->refreshFromIntermediate(0, 0);

    // Get rid of the current review region
    delete reviewRegion;
    reviewRegion = 0;
    reviewRegionFrameIndex = -1;
    }
    */

   reviewRegionFrameIndex = newFrameIndex;
   reviewRegion           = newReviewRegion;

   // Do whatever to display the review region
   player->setProvisionalFrameIndex(reviewRegionFrameIndex);
   player->setHighlightColor(0, 0, 65535); // Blue
   player->loadProvisionalPixelRegionList(reviewRegion);
   player->setProvisionalMode(DISPLAY_FROM_CLIP);

   RECT boundingBox = reviewRegion->GetBoundingBox();
   if (!goZoom) // center the display without changing zoom level
            displayer->calculateReviewRectangle(&boundingBox);
   else // center the display while zooming maximally
            displayer->calculateZoomedReviewRectangle(&boundingBox);
   mainWindow->UpdateZoomLevelStatus();

   // Ask Player to display the relevant frame
   player->fullStop(); // force player to read frame from disk
   player->goToFrameSynchronous(reviewRegionFrameIndex);

   return 0;
}

int CProvisional::ClearReviewRegion(bool redraw)
{
   CPlayer *player = mainWindow->GetPlayer();

   if (reviewRegion != 0)
   {
      if (!(mode == TOOL_SETUP_TYPE_SINGLE_FRAME && IsProvisionalPending()))
      {
         // Tell Player the current review region is going away
         player->loadProvisionalPixelRegionList(0);

         if (redraw)
         {
            // Display the frame, but only if it is the current frame
            if (reviewRegionFrameIndex == player->getLastFrameIndex())
            {
               player->fullStop(); // force player to read frame from disk
               player->goToFrameSynchronous(reviewRegionFrameIndex);
            }
         }
      }

      // Get rid of the current review region
      delete reviewRegion;
      reviewRegion           = 0;
      reviewRegionFrameIndex = -1;
   }

   return 0;
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                              #################################
// ###     CProvisionalTool Class     ################################
// ####                              #################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CProvisionalTool::CProvisionalTool(const string &newToolName)
      : CToolObject(newToolName, NULL)
{
}

CProvisionalTool::~CProvisionalTool()
{
}

//////////////////////////////////////////////////////////////////////
// Initialization
//////////////////////////////////////////////////////////////////////

CToolObject* CProvisionalTool::MakeTool(const string& newToolName)
{
   return new CProvisionalTool(newToolName);
}

// ===================================================================
//
// Function:    toolInitialize
//
// Description: Called when the ToolManager wants to initialize
// the Provisional Tool
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================
int CProvisionalTool::toolInitialize(CToolSystemInterface *newSystemAPI)
{
   int retVal;

   // Initialize the CProvisionalTool's base class, i.e., CToolObject
   // This must be done before the CProvisionalTool initializes itself
   retVal = initBaseToolObject(provisionalToolNumber, newSystemAPI, provisionalCommandTable, provisionalUserInputConfiguration);
   if (retVal != 0)
   {
      // ERROR: initBaseToolObject failed
      TRACE_0(errout << "ERROR: CProvisionalTool::toolInitialize" << endl << "       CToolObject::initBaseToolObject failed" << endl <<
         "       with return = " << retVal);
      return retVal;
   }

   // Initialize the CProvisionalTool components

   return retVal;
} // end toolInitialize

// ===================================================================
//
// Function:    toolShutdown
//
// Description: Called when the Tool Manager wants to shut down the
// Provisional Tool, typically when the main program is
// shutting down.
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================

int CProvisionalTool::toolShutdown()
{
   int retVal;

   // Shut down Provisional Tool

   // Shutdown the Provisional Tool's base class, i.e., CToolObject.
   // This must be done after the Provisional Tool shuts down itself
   retVal = shutdownBaseToolObject();
   if (retVal != 0)
   {
      // ERROR: shutdownBaseToolObject failed
      return retVal;
   }

   return retVal;
} // end toolShutdown

CToolProcessor* CProvisionalTool::makeToolProcessorInstance(const bool *newEmergencyStopFlagPtr)
{
   return new CProvisionalToolProc(GetToolNumber(), GetToolName(), getSystemAPI(), newEmergencyStopFlagPtr);
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                                   ############################
// ###     CProvisionalToolProc Class      ###########################
// ####                                   ############################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CProvisionalToolProc::CProvisionalToolProc(int newToolNumber, const string &newToolName, CToolSystemInterface *newSystemAPI,
   const bool *newEmergencyStopFlagPtr)
      : CToolProcessor(newToolNumber, newToolName, 1, 1, newSystemAPI, newEmergencyStopFlagPtr),
        vpGetFrameThread(0)
{
   // Start the "Get Frame Loop" thread
   vpGetFrameThread = BThreadSpawn(GetFrameThread, this);
}

CProvisionalToolProc::~CProvisionalToolProc()
{
}

//////////////////////////////////////////////////////////////////////
// Set Parameters
//////////////////////////////////////////////////////////////////////

int CProvisionalToolProc::SetFrameRange(CToolFrameRange *toolFrameRange)
{
   return 0; // Success
}

int CProvisionalToolProc::SetIOConfig(CToolIOConfig *toolIOConfig)
{
   SetInputMaxFrames(0, 1, 0);
   SetOutputModifyInPlace(0, 0, false);
   SetOutputMaxFrames(0, 0);

   return 0; // Success
}

int CProvisionalToolProc::RegisterWithSystem()
{
   int retVal;

   retVal = GetSystemAPI()->RegisterProvisionalToolProc(this);
   if (retVal != 0)
      return retVal; // ERROR

   return 0;
}

// ===================================================================
//
// Function:    GetFrameThread
//
// Description:
// Run this function within a new thread by calling
// void *bThreadStruct = BThreadSpawn(GetFrameThread, this)
// from a CProvisionalToolProc member function
//
// Arguments:
//
// Returns:
//
// ===================================================================
void CProvisionalToolProc::GetFrameThread(void *vpProvisional, void *vpBThread)
{
   CProvisionalToolProc *tp = static_cast<CProvisionalToolProc*>(vpProvisional);
   tp->GetFrameLoop(vpBThread);
}

void CProvisionalToolProc::GetFrameLoop(void *vpBThread)
{
   bool threadActivity;
   bool processingActive = true;
   bool running          = true;
   bool endOfMaterial    = false;
   CAutotoolCommand toolCommand;
   EAutotoolCommand newToolCommand;
   CAutotoolStatus toolStatus;
   EToolControlState controlState, newControlState;
   CToolFrameBuffer *toolFrameBuffer;

   BThreadBegin(vpBThread);

   // Initial control state
   controlState = TOOL_CONTROL_STATE_STOPPED;

   while (processingActive)
   {
      threadActivity = false;

      // Get and process tool commands
      if (GetToolCommand(toolCommand) == 0)
      {
         newToolCommand = toolCommand.command;
      }
      else
         newToolCommand = AT_CMD_INVALID;

      // Tool Control State Machine
      newControlState = controlState;
      switch (controlState)
      {
      case TOOL_CONTROL_STATE_STOPPED:
         if (newToolCommand == AT_CMD_EXIT_THREAD)
         {
            newControlState = TOOL_CONTROL_STATE_EXITING_THREAD;
         }
         else if (newToolCommand == AT_CMD_RUN && !IsEmergencyStop())
         {
            endOfMaterial   = false;
            newControlState = TOOL_CONTROL_STATE_RUNNING;
         }
         break;
      case TOOL_CONTROL_STATE_RUNNING:
         if (newToolCommand == AT_CMD_EXIT_THREAD)
         {
            newControlState = TOOL_CONTROL_STATE_EXITING_THREAD;
         }
         else if (IsEmergencyStop())
         {
            FlushDstFrameQueues();
            newControlState = TOOL_CONTROL_STATE_STOPPED;
         }
         else if (endOfMaterial)
         {
            toolStatus.status = AT_STATUS_END_OF_MATERIAL;
            PutToolStatus(toolStatus);
            newControlState = TOOL_CONTROL_STATE_STOPPED;
         }
         else if (newToolCommand == AT_CMD_STOP)
         {
            FlushDstFrameQueues();
            newControlState = TOOL_CONTROL_STATE_STOPPED;
         }
         else if (newToolCommand == AT_CMD_PAUSE)
         {
            newControlState = TOOL_CONTROL_STATE_PAUSED;
         }
         break;
      case TOOL_CONTROL_STATE_PAUSED:
         if (newToolCommand == AT_CMD_EXIT_THREAD)
         {
            newControlState = TOOL_CONTROL_STATE_EXITING_THREAD;
         }
         else if (newToolCommand == AT_CMD_STOP || IsEmergencyStop())
         {
            newControlState = TOOL_CONTROL_STATE_STOPPED;
         }
         else if (newToolCommand == AT_CMD_RUN)
         {
            endOfMaterial   = false;
            newControlState = TOOL_CONTROL_STATE_RUNNING;
         }
         break;
      default:
         MTIassert(false);
         break;
      };

      if (controlState != newControlState)
      {
         // The control state has changed, so perform any actions
         // that are common to the entry of the new control state
         toolStatus.errorCode = 0;
         switch (newControlState)
         {
         case TOOL_CONTROL_STATE_STOPPED:
            running = false;
            toolStatus.status = AT_STATUS_STOPPED;
            PutToolStatus(toolStatus);
            break;
         case TOOL_CONTROL_STATE_RUNNING:
            GetSystemAPI()->PutFrameToProvisional(0);
            running           = true;
            toolStatus.status = AT_STATUS_RUNNING;
            PutToolStatus(toolStatus);
            break;
         case TOOL_CONTROL_STATE_PAUSED:
            running = false;
            PutEndOfMaterialFrames();
            toolStatus.status = AT_STATUS_PAUSED;
            PutToolStatus(toolStatus);
            break;
         case TOOL_CONTROL_STATE_EXITING_THREAD:
            running = false;
            processingActive = false;
            threadActivity   = true;
            break;
         default:
            MTIassert(false);
            break;
         };
      }
      controlState = newControlState;

      if (running)
      {
         // Get a tool frame buffer
         toolFrameBuffer = GetFrame(0);
         if (toolFrameBuffer != 0)
         {
            if (toolFrameBuffer->IsEndOfMaterial())
            {
               endOfMaterial = true;
               PutEndOfMaterialFrames();
               threadActivity = true;
            }
            else
            {
               // Pass the new tool frame buffer on to the Provisional thingy
               GetSystemAPI()->PutFrameToProvisional(toolFrameBuffer);
               threadActivity = true;
            }
         }
      }

      // If this thread isn't doing anything other than waiting for something
      // to do, then sleep for a bit so we don't hog the processor
      if (!threadActivity)
         MTImillisleep(1);
   }

   // Post thread-ending status
   toolStatus.status = AT_STATUS_PROCESS_THREAD_ENDING;
   PutToolStatus(toolStatus);
} // CProvisionalToolProc::GetFrameLoop

void CProvisionalToolProc::PutProvisionalFrame(CToolFrameBuffer *frameBuffer, bool writeOnce)
{
   if (IsEmergencyStop())
   {
      delete frameBuffer; // frame buffer goes into bit-bucket
      return;
   }

   // Put frame into the frame queue that connects to the Media Writer
   PutFrame(0, frameBuffer);

   if (writeOnce)
   {
      // Now put the bogus End-of-Material frame buffer so that the
      // Media Writer will stop after writing one frame
      PutEndOfMaterialFrames();

      // Trace our way to the Media Writer, which is assumed to be the
      // tool on the other side of the frame queue
      CToolNode *node            = GetToolNode();
      CToolNode *mediaWriterTool = node->GetDstNode(0);

      // Tell the Media Writer to Run.  This is ignored if the Media Writer
      // is already running.
      CAutotoolCommand toolCommand;
      toolCommand.command = AT_CMD_RUN;
      mediaWriterTool->PutToolCommand(toolCommand);
   }
}

int CProvisionalToolProc::PutProvisionalFrameSynchronous(CToolFrameBuffer *frameBuffer)
{
   int retVal;

   retVal = PutFrameSynchronous(0, frameBuffer);
   if (retVal != 0)
      return retVal;

   return 0;

}
