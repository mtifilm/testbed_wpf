#ifndef ProvisionalH
#define ProvisionalH

// Provisional.h: interface for the CProvisional class
//
//////////////////////////////////////////////////////////////////////

#include "IniFile.h"
#include "ToolObject.h"

#include <string>
using std::string;
#include <map>
using std::map;

//////////////////////////////////////////////////////////////////////
// Forward Declarations
//

class CClip;
class CPixelRegionList;
class CSaveRestore;
class CToolFrameBuffer;

//////////////////////////////////////////////////////////////////////
// Provisional Tool Numbers (16-bit number that uniquely
// identifies this tool)
#define PROVISIONAL_TOOL_NUMBER 5

// Dummy commands for the Provisional dummy command table
// (These tools do not have a GUI and do not accept user input)
#define PROVISIONAL_CMD_DUMMY   1

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////

class CProvisionalToolProc;

class CProvisional
{
public:
   CProvisional(CSaveRestore *newSaveRestoreEngine);
   ~CProvisional();

   int setClip(ClipSharedPtr &newClip, int newVideoProxyIndex,
               int newVideoFramingIndex);
               
   int Toggle(bool goZoom=false);
   int Accept(bool goToFrame=true);
   int AcceptFromBackgroundThread();  // gack
   int Reject(bool goToFrame=true);
   int RejectFromBackgroundThread();  // gack

   int Clear(bool goToFrame=false);
   int Refresh();

   bool IsProcessedVisible();
   bool IsProvisionalPending();
   bool IsProvisionalUnresolved();

   int SetMode(EToolSetupType newMode);
   int SetRender(bool newRenderFlag);
   int SetHighlight(bool newHighlightFlag);
   int SetReprocess(bool newReprocessFlag);
   int SetReview(bool newReviewFlag);

   int GetFrame(CToolFrameBuffer **provisionalFrameCopy);
   int PutFrame(CToolFrameBuffer *newProvisionalFrame);

   int GetLastFrameIndex();

   void SetProvisionalToolProc(CProvisionalToolProc *newToolProc);

   int RestoreToolFrameBuffer(CToolFrameBuffer *frameBuffer,
                              RECT *filterRect, POINT *filterLasso,
                              BEZIER_POINT *filterBezier,
                              CPixelRegionList *filterPixelRegion);
   int SaveToolFrameBuffer(CToolFrameBuffer *frameBuffer,
                           int toolNumber, const string &toolString,
                           bool saveToHistory);

   int DisplayReviewRegion(int newFrameIndex,
                           CPixelRegionList *newReviewRegion,
                           bool goZoom);
                           
   int ClearReviewRegion(bool redraw);

   void StartProcessing();   // Called by Tool Manager only
   void StopProcessing();
   void UpdateStatusBar();

   // History cleanup hack
   void ValidateHistory(int frameIndex);
   void DiscardUnvalidatedHistory();

private:
   int DiscardHistory();
   int ResolveProvisional(bool saveProvisional, bool goToFrame);
   int ResolveProvisionalFromBackgroundThread(bool saveProvisional);

private:
   EToolSetupType mode;      // Single-Frame, Multi-Frame or Training

   CToolFrameBuffer *provisionalFrame;
   CToolFrameBuffer *provisionalFrameCopyForOutput;

   int lastFrameIndex;

   bool renderFlag;
   bool highlightFlag;

   // State flags
   bool pendingFlag;    // Provisional change is present
   bool pendingVisible; // Provisional change is now displayed
   bool reviewToolFlag; // Provisional change is from a review tool
   bool reprocessFlag;  // Reprocess of a previous change

   CSaveRestore *saveRestoreEngine;

   CProvisionalToolProc *provisionalToolProc;

   int historySaveCount;
   int previousHistorySaveCount;
   int validHistorySaveCount;              // history cleanup hack
   map<int,int> unvalidatedHistoryList;
   int lastValidFrameIndex;

   int reviewRegionFrameIndex;
   CPixelRegionList *reviewRegion;

   CThreadLock provisionalThreadLock;  // Thread lock for making accesses
                                       // to pendingFlag and provisionalFrame
                                       // atomic.

   LONG foregroundThreadId;

private:
   static string processedImageStatusMsg;
   static string originalImageStatusMsg;
};

//////////////////////////////////////////////////////////////////////

class CProvisionalToolProc : public CToolProcessor
{
public:
   CProvisionalToolProc(int newToolNumber, const string& newToolName,
                        CToolSystemInterface *newSystemAPI,
                        const bool *newEmergencyStopFlagPtr);
   virtual ~CProvisionalToolProc();

   int RegisterWithSystem();

   virtual int SetIOConfig(CToolIOConfig *toolIOConfig);
   virtual int SetFrameRange(CToolFrameRange *toolFrameRange);

   void PutProvisionalFrame(CToolFrameBuffer *frameBuffer, bool writeOnce);
   int PutProvisionalFrameSynchronous(CToolFrameBuffer *frameBuffer);

   // Dummy functions needed to keep CToolProcessor happy
   int SetParameters(CToolParameters *toolParams) {return -1;};
   int BeginProcessing(SToolProcessingData &procData) {return -1;};
   int EndProcessing(SToolProcessingData &procData) {return -1;};
   int BeginIteration(SToolProcessingData &procData) {return -1;};
   int EndIteration(SToolProcessingData &procData) {return -1;};
   int DoProcess(SToolProcessingData &procData) {return -1;};
   int GetIterationCount(SToolProcessingData &procData) {return -1;};

private:
   static void GetFrameThread(void *vpProvisional, void *vpBThread);
   void GetFrameLoop(void *vpBThread);

private:
   CProvisional *provisional;

   void *vpGetFrameThread;
};

//////////////////////////////////////////////////////////////////////

class CProvisionalTool : public CToolObject
{
public:
   CProvisionalTool(const string &newToolName);
   virtual ~CProvisionalTool();

   int toolInitialize(CToolSystemInterface *newSystemAPI);
   int toolShutdown();

   // Static method to create a new instance of this tool
   static CToolObject* MakeTool(const string& newToolName);

   CToolProcessor* makeToolProcessorInstance(
                                          const bool *newEmergencyStopFlagPtr);

private:
};

//////////////////////////////////////////////////////////////////////

#endif // #if !defined(PROVISIONAL_H)


