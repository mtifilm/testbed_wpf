// NavMainCommon.cpp:  Platform-independent components of Navigator Main
//
/*
$Header: /usr/local/filmroot/Nav_common/source/NavMainCommon.cpp,v 1.57.2.6 2009/06/16 12:02:54 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "NavMainCommon.h"

#include "ClipAPI.h"
#include "CustomReticleUnit.h"
#include "Displayer.h"
#include "MainWindowUnit.h"
#include "MTIDialogs.h"
#include "NavSystemInterface.h"
#include "Player.h"
#include "PreferencesUnit.h"
#include "ToolManager.h"

//////////////////////////////////////////////////////////////////////
// Static Variables
//////////////////////////////////////////////////////////////////////

// Ini file strings for saved clip state
string CNavCurrentClip::clipStateSectionNamePrefix = "ClipState/";
string CNavCurrentClip::proxyIndexKey = "ProxyIndex";
string CNavCurrentClip::framingIndexKey = "FramingIndex";
string CNavCurrentClip::frameIndexKey = "FrameIndex";
string CNavCurrentClip::timeCodeIndexKey = "TimecodeIndex";
string CNavCurrentClip::markInKey = "MarkIn";
string CNavCurrentClip::markOutKey = "MarkOut";
string CNavCurrentClip::displayDevicesIndexKey = "DisplayDevicesIndex";
string CNavCurrentClip::displayModeValueKey    = "DisplayMode";
string CNavCurrentClip::zoomFactorValueKey      = "ZoomFactor";
string CNavCurrentClip::displayXCenterValueKey = "DisplayXCenter";
string CNavCurrentClip::displayYCenterValueKey = "DisplayYCenter";
string CNavCurrentClip::slowDisplaySpeedValueKey = "SlowDisplaySpeed";
string CNavCurrentClip::mediumDisplaySpeedValueKey = "MediumDisplaySpeed";
string CNavCurrentClip::fastDisplaySpeedValueKey = "FastDisplaySpeed";
string CNavCurrentClip::displaySpeedIndexKey = "DisplaySpeedIndex";
string CNavCurrentClip::customLUTKey = "UseCustomLUT";
string CNavCurrentClip::customLUTFormatDefaultKey = "UseFormatDefaultLUT";
string CNavCurrentClip::customLUTColorSpaceIndexKey = "CustomLUTColorSpaceIndex";
string CNavCurrentClip::customLUTSourceRangeKey = "UseCustomLUTSourceRange";
string CNavCurrentClip::customLUTRYBlkKey = "CustomLUTRYBlk";
string CNavCurrentClip::customLUTRYWhiKey = "CustomLUTRYWhi";
string CNavCurrentClip::customLUTGUBlkKey = "CustomLUTGUBlk";
string CNavCurrentClip::customLUTGUWhiKey = "CustomLUTGUWhi";
string CNavCurrentClip::customLUTBVBlkKey = "CustomLUTBVBlk";
string CNavCurrentClip::customLUTBVWhiKey = "CustomLUTBVWhi";
string CNavCurrentClip::customLUTRMinKey = "CustomLUTRMin";
string CNavCurrentClip::customLUTRMaxKey = "CustomLUTRMax";
string CNavCurrentClip::customLUTGMinKey = "CustomLUTGMin";
string CNavCurrentClip::customLUTGMaxKey = "CustomLUTGMax";
string CNavCurrentClip::customLUTBMinKey = "CustomLUTBMin";
string CNavCurrentClip::customLUTBMaxKey = "CustomLUTBMax";
string CNavCurrentClip::gammaValueKey = "Gamma";
string CNavCurrentClip::reticleModeKey = "ReticleMode";
string CNavCurrentClip::reticleTypeKey = "ReticleType";
string CNavCurrentClip::reticleFileKey = "ReticleFile";
string CNavCurrentClip::reticleAspectRatioKey = "ReticleAspectRatio";
string CNavCurrentClip::reticleApertureKey = "ReticleAperture";
string CNavCurrentClip::reticleXOffsetKey = "ReticleXOffset";
string CNavCurrentClip::reticleYOffsetKey = "ReticleYOffset";
string CNavCurrentClip::reticleHScaleKey = "ReticleHScale";
string CNavCurrentClip::reticleVScaleKey = "ReticleVScale";

#ifdef __sgi
// CNavCurrentClip's pointer to the Main Window
// (this is a global in Win version)
MainWindow *CNavCurrentClip::mainWindow = 0;
#endif // #ifdef __sgi

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CNavCurrentClip::CNavCurrentClip()
 : currentClip(nullptr)
 , currentVideoProxyIndex(-1)
 , currentVideoFramingIndex(-1)
 , currentDisplayDevicesIndex(-1)
 , currentDisplayWindowIndex(-1)
 , currentSlowDisplaySpeedValue(-1)
 , currentMediumDisplaySpeedValue(-1)
 , currentFastDisplaySpeedValue(-1)
 , currentDisplaySpeedIndex(-1)
{
}

CNavCurrentClip::~CNavCurrentClip()
{
   // Try to eliminate som CODEGUARD garbage
   if (getCurrentClip())
      unloadCurrentClip();

}

//////////////////////////////////////////////////////////////////////
// Class Initialization
//////////////////////////////////////////////////////////////////////

#ifdef __sgi
void CNavCurrentClip::setMainWindowPointer(MainWindow *newMainWindow)
{
   // Set local pointer to Main Window
   mainWindow = newMainWindow;
}
#endif // #ifdef __sgi


//////////////////////////////////////////////////////////////////////
// Clip Setup
//////////////////////////////////////////////////////////////////////

bool CNavCurrentClip::needToDeleteMask(ClipSharedPtr &previousClip, ClipSharedPtr &newClip)
{
	if (newClip == nullptr || previousClip == nullptr)
	{
		return true;
	}

	// Blow away mask if size changes
	auto imgFmt = previousClip->getImageFormat(VIDEO_SELECT_NORMAL);
	if (imgFmt == nullptr)
	{
		return true;
	}

	auto previousHeight = imgFmt->getLinesPerFrame();
	auto previousWidth = imgFmt->getPixelsPerLine();

	imgFmt = newClip->getImageFormat(VIDEO_SELECT_NORMAL);
	if (imgFmt == nullptr)
	{
		return true;
	}

	return (previousHeight != imgFmt->getLinesPerFrame()) || (previousWidth != imgFmt->getPixelsPerLine());
}

//------------------------------------------------------------------------
//
// Function:    openClipByName
//
// Description: Opens a clip given its Bin Path and Clip Name.
//              Sets "Current Clip" and initializes Player, Displayer,
//              etc., so Navigator is ready to use the clip.  By
//              default, the "Normal" Video Proxy and Video-Frames Video
//              Framing are selected.
//
// Arguments:   string& binPath           Bin Path
//              string& clipName          Clip's Name
//              int newVideoFramingIndex
//
// Returns:     0 if success, non-zero if a problem
//
//------------------------------------------------------------------------
int CNavCurrentClip::openClipByName(const string& binPath,
                                    const string& clipName)
{
   int retVal;

   // Save the state of the current clip, if there is one
   retVal = saveClipState();
   if (retVal != 0)
   {
      // wtf?? WHY ABORT IF WE CAN'T SAVE OLD CLIP STATE??? QQQ
      return retVal;
   }

   CBinManager binMgr;
   auto newClip = binMgr.openClip(binPath, clipName, &retVal);
   if (retVal != 0)
      return retVal; // ERROR: Could not open the clip

   // This is in the wrong place, needs to go before the old clip is closed,
   // which is before we get here in the case of reloading the open clip!!!
   // QQQ should really rewrite this spaghetti code...
   //CToolManager toolMgr;
   //toolMgr.onChangingClip(); // Notify tools that old clip is being closed

	// NEW: Do NOT reset the "previous clip" stuff if we are "switching to"
	// the current clip - that can happen when we discard or commit frames
	// in a version clip.
	auto previousClip = getCurrentClip();

	// Normally we want to use the previously selected clip for comparing,
	// but if that doesn't work, try to use the parent clip.
	bool tryToUseParentForCompare = previousClip == nullptr;
	string compareClipName = "No clip selected for comparison";
	ClipIdentifier newClipId(newClip->getClipPathName());

	// Don't change "previous clip" stuff if new clip is same as current clip!
	if (previousClip != nullptr && (previousClip->getBinPath() != binPath || previousClip->getClipName() != clipName))
	{
		// OK to reset these - we're actually switching to a different clip.
		previousBinPath = previousClip->getBinPath();
		previousClipName = previousClip->getClipName();

		if (previousClip->getImageFormat(VIDEO_SELECT_NORMAL)->getPixelsPerLine()
				== newClip->getImageFormat(VIDEO_SELECT_NORMAL)->getPixelsPerLine()
		&& previousClip->getImageFormat(VIDEO_SELECT_NORMAL)->getLinesPerFrame()
				== newClip->getImageFormat(VIDEO_SELECT_NORMAL)->getLinesPerFrame())
		{
			// Tell the Player about the new "alternate" clip.
			mainWindow->GetPlayer()->setFrameCompareClipName(previousClip->getClipPathName());
			ClipIdentifier clipId(previousClip->getClipPathName());
			compareClipName = clipId.GetFullDisplayString();
		}
		else
		{
			// Switched to an incompatible clip. Try to use the parent instead.
			tryToUseParentForCompare = true;
		}
	}

	if (tryToUseParentForCompare)
	{
		mainWindow->GetPlayer()->setFrameCompareClipName(newClipId.IsVersionClip()
																			? newClipId.GetParentPath():
																			"");
		ClipIdentifier prevClipId(newClipId.GetParentPath());
		compareClipName = prevClipId.GetFullDisplayString();
	}

	mainWindow->SetFrameCompareClipNames(compareClipName, newClipId.GetFullDisplayString());

	// we now include all the arguments derived from preferences
   retVal = setClip(newClip);
   if (retVal != 0)
		return retVal; // ERROR: Unable to initialize Player, Displayer, etc

	if (previousClip != nullptr && newClip != previousClip)
	{
	  if (needToDeleteMask(previousClip, newClip))
	  {
		 auto maskTool = CNavigatorTool::GetMaskTool();
		 if (maskTool != nullptr)
		 {
             maskTool->ClearAll();
         }
      }

	  // Close the previous current clip
	  CBinManager binMgr;
	  mainWindow->GetTimeline()->ReleaseClipInTimeline();
	  binMgr.closeClip(previousClip);
	}

   // Let's keep all the frickin' side effects together (see note above)
   //toolMgr.onNewClip();    // Notify tools that there is a new clip

   return 0;   // Success
}

int CNavCurrentClip::unloadCurrentClip()
{
   int retVal;

   if (getCurrentClip()== nullptr)
      return 0;      // No clip currently loaded, so just return


	// tell the Player that the clip is unloaded
	mainWindow->GetPlayer()->unloadVideoClip();

   // tell the Displayer that the clip is unloaded
   mainWindow->GetDisplayer()->clearImageFormat();
   mainWindow->GetTimeline()->ReleaseClipInTimeline();

	retVal = closeCurrentClip();
   if (retVal != 0)
      return retVal;
/*
   // Remember the "current" values
   currentClip = 0;
   currentVideoProxyIndex = -1;
   currentVideoFramingIndex = -1;

   currentBinPath = "";
   currentClipName = "";
*/
   return 0;
}

int CNavCurrentClip::closeCurrentClip()
{
   int retVal;

   if (getCurrentClip()== nullptr)
      return 0;      // No clip currently loaded, so just return


   CBinManager binMgr;
   retVal = binMgr.closeClip(currentClip);

   // Remember the "current" values
   currentClip = 0;
   currentVideoProxyIndex = -1;
   currentVideoFramingIndex = -1;

	previousBinPath = currentBinPath;
   previousClipName = currentClipName;
   currentBinPath = "";
   currentClipName = "";

   return retVal;
}

//------------------------------------------------------------------------
//
// Function:    reloadCurrentClip
//
// Description: Forces the current clip to be reopened.  State of
//              current clip is saved before it is closed and this
//              state is restored when the clip is opened
//
//              TTT: The state of the In and Out Marks is lost when
//                   the clip is reloaded.  The In and Out Marks are
//                   saved, but need a CPlayer function to set the In
//                   and Out Marks given a frame index
//
// Arguments:   string& binPath           Bin Path
//              string& clipName          Clip's Name
//
// Returns:     0 if success, non-zero if a problem
//
//------------------------------------------------------------------------
int CNavCurrentClip::reloadCurrentClip()
{
   int retVal;
   TRACE_2(errout << "INFO: entering CNavCurrentClip::reloadCurrentClip");

   if (!isClipAvailable())
      {
      TRACE_1(errout << "WARNING: CNavCurrentClip::reloadCurrentClip: no current clip to reload");
      return 0;      // No clip currently loaded, so just return
      }

   // switch the PLAYER to IDLE (important!)
   mainWindow->GetPlayer()->fullStop();

   // Save the current clip's state
   retVal = saveClipState();
   if (retVal != 0)
      {
      TRACE_0(errout << "ERROR: CNavCurrentClip::reloadCurrentClip: saveClipState failed, return: "
                     << retVal);
      return retVal;
      }

   // Notify tools that the old clip is about to close
   // This used to be inside openClipByName but we pulled it out because
   // it needs to be done BEFORE THE CLIP IS FRICKIN' CLOSED!
   CToolManager toolMgr;
	toolMgr.onChangingClip(); // Notify tools that old clip is being closed

   // Get the clip's bin path and clip name
   string binPath = currentClip->getBinPath();
	string clipName = currentClip->getClipName();

	// We want to preserve the existing "previous clip" so Page Up and Page Down
	// continue to work as expected
	string savedPreviousBinPath = previousBinPath;
	string savedPreviousClipName = previousClipName;

	// Tell the Player, Displayer, and Timeline that the clip is unloaded
	mainWindow->GetPlayer()->unloadVideoClip();
   mainWindow->GetDisplayer()->clearImageFormat();
   mainWindow->GetTimeline()->ReleaseClipInTimeline();

   // Update the registered media access object for the clip.
   auto videoProxy = currentClip->getVideoProxy(VIDEO_SELECT_NORMAL);
   auto mediaStorageList = videoProxy->getMediaStorageList();
   auto mediaStorage_0 = mediaStorageList->getMediaStorage(0);
   if (mediaStorage_0 != nullptr)
   {
      CMediaStorageList *nonConstMediaStorageList = const_cast<CMediaStorageList *>(mediaStorageList);
      EMediaType mediaType = mediaStorage_0->getMediaType();
      string mediaIdentifier = mediaStorage_0->getMediaIdentifier();
      string historyDirectory = mediaStorage_0->getHistoryDirectory();
      CImageFormat imageFormat = *(videoProxy->getImageFormat());
      nonConstMediaStorageList->replaceMediaStorage(0, mediaType, mediaIdentifier, historyDirectory, imageFormat);
   }

   // Close the current clip
   CBinManager binMgr;
	binMgr.closeClip(currentClip);
   currentClip = 0;
   currentVideoProxyIndex = -1;
   currentVideoFramingIndex = -1;
   currentBinPath = "";
   currentClipName = "";

   // Reopen the clip we just closed!
   retVal = openClipByName(binPath, clipName);
   if (retVal != 0)
      {
      TRACE_0(errout << "ERROR: CNavCurrentClip::reloadCurrentClip: openClipByName failed, return: "
                     << retVal);
      return retVal;
      }

   TRACE_2(errout << "INFO: exiting CNavCurrentClip::reloadCurrentClip:" << endl
                  << "      reopened bin: " << binPath << endl
						<< "              clip: " << clipName);

	// Restore the "previous clip" stuff.
	previousBinPath = savedPreviousBinPath;
	previousClipName = savedPreviousClipName;

   // Notify tools that there is a new clip
	toolMgr.onNewClip();

   // Notify anyone else interested that the clip has changed
   mainWindow->ClipHasChanged.Notify();

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    setClip
//
// Description: Sets "Current Clip" and initializes Player, Displayer,
//              etc., so Navigator is ready to use the clip. Saved state
//              of this clip is used to select Video Proxy, Framing
//              and Frame Index, and all other settings derived from
//              preferences
//
// Arguments:   ClipSharedPtr newClip     Pointer to new clip
//
// Returns:     0 if success, non-zero otherwise
//
//------------------------------------------------------------------------
int CNavCurrentClip::setClip(ClipSharedPtr &newClip)
{
   int retVal;

   // Get the video proxy, framing and frame indices from the saved state
   // of this clip or current settings

   CBinManager bm;
   string sectionName = bm.getDesktopSectionUniqueName();;

   CIniFile* clipDesktopIni = bm.openClipDesktop(newClip);
   CIniFile* binDesktopIni  = bm.openBinDesktop(newClip);
   //CIniFile* rootDesktopIni = bm.openRootDesktop();

   // Default values are all -1, which forces intelligent default in
   // call to setClipVideoProxyAndFraming

   // VIDEO PROXY INDEX comes from the CLIP INI
   int newVideoProxyIndex = VIDEO_SELECT_NORMAL;
   if (clipDesktopIni != NULL)
      newVideoProxyIndex = clipDesktopIni->ReadInteger(sectionName,
                                                       proxyIndexKey,
                                                       newVideoProxyIndex);

   // VIDEO FRAMING INDEX comes from the CLIP INI
   int newVideoFramingIndex = FRAMING_SELECT_FILM;
   if (clipDesktopIni != NULL)
      newVideoFramingIndex = clipDesktopIni->ReadInteger(sectionName,
                                                         framingIndexKey,
                                                         newVideoFramingIndex);

   // DISPLAY DEVICES comes from the CURRENT SETTING
   int newDisplayDevicesIndex = getCurrentDisplayDevicesIndex();

   // DISPLAY WINDOW INDEX wants to be 0 which means "use current settings"
   int newDisplayWindowIndex = 0;   // frickin' manifest constants

   // DISPLAY MODE STUFF comes from CURRENT SETTINGS - probably ignored
   // because of newDisplayWindowIndex = 0, but who the f*ck knows?
   DISPLAY_CONTEXT newDisplayContext =
                                mainWindow->GetDisplayer()->getDisplayContext();

   // DISPLAY SPEED VALUES come from the PREFERENCES WINDOW
   int newSlowDisplaySpeedValue   = mainWindow->GetSlowDisplaySpeedPreference();
   int newMediumDisplaySpeedValue = mainWindow->GetMediumDisplaySpeedPreference();
   int newFastDisplaySpeedValue   = mainWindow->GetFastDisplaySpeedPreference();

   // DISPLAY SPEED SETTING comes from CURRENT SETTING
   int newDisplaySpeedIndex = mainWindow->GetPlayer()->getDisplaySpeedIndex();

   // RETICLE STUFF comes from CURRENT SETTING
   int newReticleModeIndex = mainWindow->GetDisplayer()->getReticleMode();
   CReticle newReticle = mainWindow->GetDisplayer()->getReticle();

   // FRAME INDEX comes from CURRENT SETTING, with fallback to the clip's
   // last setting (which might be a frame index OR a timecode string!)
   // if the current setting isn't valid for the clip
   // XXX NOW ONLY MATCH TIMECODES, NOT FRAME INDICES!
   string newTCStr = mainWindow->GetPlayer()->CurrentTC.getUserPreferredStyleString();
   //int newFrameIndex = mainWindow->GetPlayer()->getLastFrameIndex();
   string fallbackTCStr;
   //int fallbackFrameIndex = -1;
   if (clipDesktopIni != NULL)
   {
      fallbackTCStr = clipDesktopIni->ReadString(sectionName,
                                                 timeCodeIndexKey,
                                                 fallbackTCStr);
      //fallbackFrameIndex = clipDesktopIni->ReadInteger(sectionName,
                                                       //frameIndexKey,
                                                       //fallbackFrameIndex);
   }

   // MARK IN and MARK OUT come from BIN SETTINGS if supposed to be shared
   // with other clips in the bin, else it comes from the clip's last setting
   // FOR NOW - BIN SHARING IS NOT IMPLEMENTED QQQ

   bool usingBinMarks = false;
   string markInTCStr;
   string markOutTCStr;
   if (binDesktopIni != NULL)
   {
      usingBinMarks = binDesktopIni->ReadBool("General",
                                              "ClipsShareInAndOutMarks",
                                              false);
      if (usingBinMarks)
      {
         markInTCStr = binDesktopIni->ReadString(sectionName, markInKey,
                                                 markInTCStr);
         markOutTCStr = binDesktopIni->ReadString(sectionName, markOutKey,
                                                  markOutTCStr);
      }
      if (markInTCStr.empty() && markOutTCStr.empty())
         usingBinMarks = false;
   }
   if ((!usingBinMarks) && (clipDesktopIni != NULL))
   {
      markInTCStr = clipDesktopIni->ReadString(sectionName,
                                               markInKey,
                                               markInTCStr);
      markOutTCStr = clipDesktopIni->ReadString(sectionName,
                                                markOutKey,
                                                markOutTCStr);
   }


   clipDesktopIni->FileName = "";  // Don't write back the ini file
   if (!DeleteIniFile(clipDesktopIni))
      return -531;
   binDesktopIni->FileName = "";  // Don't write back the ini file
   if (!DeleteIniFile(binDesktopIni))
      return -531;

#ifdef _OLD_LUT_CRAP_
   // This is the gamma that is used if the LUT choice is DEFAULT
   int newGammaValue = mainWindow->GetGammaPreference();
   mainWindow->GetDisplayer()->setDefaultGamma(newGammaValue);

   // Load a custom LUT appropriate to the clip
   loadCustomLUT(newClip, -1, newGammaValue);
#endif
   // BECUSE OF FRICKIN SPAGHETTI CODE I HAD TO BURY THIS INSIDE
   // Displayer::setImageFormat()!!!!!
   // mainWindow->LoadAndApplyLutSettings(newClip);

   retVal = setClipVideoProxyAndFraming(newClip, newVideoProxyIndex,
                                                 newVideoFramingIndex,
                                                 newDisplayDevicesIndex,
                                                 newDisplayWindowIndex,
                                                 newDisplayContext,
                                                 newReticleModeIndex,
                                                 newReticle,
                                                 //newFrameIndex,
                                                 //fallbackFrameIndex,
                                                 newTCStr,
                                                 fallbackTCStr,
                                                 markInTCStr,
                                                 markOutTCStr);
   if (retVal != 0)
      return retVal; // ERROR: Could not set clip

   // Make sure the display speed is correct and matches the status bar
   mainWindow->GetPlayer()->setDisplaySpeedIndex(newDisplaySpeedIndex);
   mainWindow->UpdateFPSViewStatus(mainWindow->GetPlayer()->getDisplaySpeedIndex());

   return 0;
}

//////////////////////////////////////////////////////////////////

int CNavCurrentClip::setClipVideoProxyAndFraming(ClipSharedPtr &newClip,
                                                 int newVideoProxyIndex,
                                                 int newVideoFramingIndex,
                                                 int newDisplayDevicesIndex,
                                                 int newDisplayWindowIndex,
                                                 DISPLAY_CONTEXT& newDisplayContext,
                                                 int newReticleModeIndex,
                                                 CReticle& newReticle,
                                                 //int newFrameIndex,
                                                 //int fallbackFrameIndex,
                                                 const string& newTCStr,
                                                 const string& fallbackTCStr,
                                                 const string& markInTCStr,
                                                 const string& markOutTCStr)
{
   if (newClip== nullptr)
      return -520; // ERROR: NULL clip pointer

   // switch the PLAYER to IDLE (important!)
   mainWindow->GetPlayer()->fullStop();

   // Filter new proxy index and new framing index
   if (newVideoProxyIndex < 0
       || newVideoProxyIndex >= newClip->getVideoProxyCount())
      newVideoProxyIndex = VIDEO_SELECT_NORMAL;

   CVideoProxy *videoProxy = newClip->getVideoProxy(newVideoProxyIndex);
   if (videoProxy== nullptr)
      return -521; // No video proxy, possibly an "empty clip" or a clip
                   // with audio tracks only

   if (newVideoFramingIndex < 0
       || newVideoFramingIndex >= videoProxy->getFramingCount())
      newVideoFramingIndex = FRAMING_SELECT_FILM;  // Default is film-frames

   // If this is a non-interlaced clip, don't attempt to use the
   // video-fields framing, even if caller has requested it.
   if (!videoProxy->getImageFormat()->getInterlaced()
       && (newVideoFramingIndex == FRAMING_SELECT_FIELDS_1_2
           || newVideoFramingIndex == FRAMING_SELECT_FIELD_1_ONLY
           || newVideoFramingIndex == FRAMING_SELECT_FIELD_2_ONLY))
      newVideoFramingIndex = FRAMING_SELECT_FILM;

   if (newDisplayDevicesIndex < 0)
      newDisplayDevicesIndex = DISPLAY_TO_WINDOW; // Default is local display

   // load the new clip into the (idling) PLAYER
	int retVal = mainWindow->GetPlayer()->loadVideoClip(newClip, newVideoProxyIndex,
																		 newVideoFramingIndex,
																		 newDisplayDevicesIndex);
	if (retVal != 0)
	{
		return retVal;
   }

   // Set clip in Navigator System Interface for save & restore
   CNavSystemInterface::setClip(newClip, newVideoProxyIndex,
                                newVideoFramingIndex);

   // moved this up from down below because I need to know it in setImageFormat
   currentClip = newClip;

   // load the new clip's dimensions into the DISPLAYER
   mainWindow->GetDisplayer()->setImageFormat(
                                newClip->getImageFormat(newVideoProxyIndex));

   // update in/out timecodes
   mainWindow->GetTimeline()->UpdateInTimecode();
   mainWindow->GetTimeline()->UpdateOutTimecode();

   // set trackbar scale
   mainWindow->GetTimeline()->UpdateTimelineLimits();

   // Remember the "current" values
   // currentClip = newClip;     moved up to be before setImageFormat()!!!
   currentVideoProxyIndex = newVideoProxyIndex;
   currentVideoFramingIndex = newVideoFramingIndex;
   //currentDisplayModeIndex = mainWindow->GetPlayer()->getDisplayMode();
   currentBinPath = currentClip->getBinPath();
   currentClipName = currentClip->getClipName();

   // Make sure the frame index is in range of clip
   CVideoFrameList *curVideoFrameList = getCurrentVideoFrameList();
   int newFrameIndex = -1;
   int inFrameIndex = mainWindow->GetPlayer()->getInFrameIndex();
   int outFrameIndex = mainWindow->GetPlayer()->getOutFrameIndex() + 1;  // Player is inclusive.

   if (curVideoFrameList != NULL)
   {
      CTimecode indexTC = curVideoFrameList->getInTimecode();

      indexTC.setTimeASCII(newTCStr.c_str());
      newFrameIndex = curVideoFrameList->getFrameIndex(indexTC);

      if (newFrameIndex < inFrameIndex || newFrameIndex >= outFrameIndex)
      {
         // Current timecode is invalid - use timecode fallback if possible
         indexTC.setTimeASCII(fallbackTCStr.c_str());
         newFrameIndex = curVideoFrameList->getFrameIndex(indexTC);
      }
   }

   // If still out of range, just go to the beginning of the clip
   if (newFrameIndex < inFrameIndex || newFrameIndex >= outFrameIndex)
      newFrameIndex = inFrameIndex;

   // Set the Mark In and Mark Out for both Player and Timeline
   // CAREFUL!! Player will ignore this request if it is doing anything,
   // like refreshing the current frame, so mbraca added locking
   mainWindow->GetPlayer()->lockReader(true);
   setMarks(curVideoFrameList, markInTCStr, markOutTCStr);
   mainWindow->GetPlayer()->lockReader(false);

   // Set the display context based on the preference specified by
   // newDisplayWindowIndex. The clip's last display context is passed in
   // as the second argument, when the first is -1. The current display
   // context has been saved by the Displayer call to "setImageFormat",
   // for use when the preference "Current Setting" is specified by
   // a value of 0 in the first argument.
   // COULD THIS GET ANY MORE FRICKIN' COMPLICATED??  Anyhow there are no
   // more such preferences, we want to set CURRENT SETTING always, so
   // I probably need to rip that crap out. QQQ
   mainWindow->GetDisplayer()->setDisplayContext(newDisplayWindowIndex,
                                                 newDisplayContext);
   // may have changed
   mainWindow->UpdateZoomLevelStatus();

   // Init the RETICLE to be saved when exiting from the clip
   mainWindow->GetDisplayer()->setExitReticle(newReticle);

   // Init the current reticle & insert its name into the toolbar
   mainWindow->GetDisplayer()->setReticle(newReticle);
   mainWindow->UpdateReticleToolbarComboBox();

   // Init the reticle display mode and update the toolbar buttons
   mainWindow->GetDisplayer()->setReticleMode(newReticleModeIndex);
   mainWindow->UpdateReticleToolbarModeButtons();

   // Go to the new frame
   mainWindow->GetPlayer()->goToFrameSynchronous(newFrameIndex);

   // At this point the REMOTE monitor, if called for, might
   // not yet be set up. Do an asynchronous refresh to do it
   // Bugzilla 2477
   mainWindow->GetPlayer()->refreshFrame();

  return 0;  // Success

} // setClipVideoProxyAndFraming

void CNavCurrentClip::setMarks(CVideoFrameList *videoFrameList,
                               const string& markInTCStr,
                               const string& markOutTCStr)
{
   int markIn, markOut;

   // Given the caller's mark timecode strings, get frame indices for
   // the mark in and out points.  An empty mark string indicates a
   // disabled mark for which the frame index is set to -1
   if (videoFrameList== nullptr)
      {
      markIn = -1;
      markOut = -1;
      }
   else
      {
      CTimecode markTimecode = videoFrameList->getInTimecode();
      if (!markInTCStr.empty())
         {
         markTimecode.setTimeASCII(markInTCStr.c_str());
         markIn = videoFrameList->getFrameIndex(markTimecode);
         }
      else
         markIn = -1;

      if (!markOutTCStr.empty())
         {
         markTimecode.setTimeASCII(markOutTCStr.c_str());
         markOut = videoFrameList->getFrameIndex(markTimecode);
         }
      else
         markOut = -1;
      }

   // Make sure the in and out mark frame indices are within range
   // of the frame list
   int inFrameIndex = mainWindow->GetPlayer()->getInFrameIndex();
   int outFrameIndex = mainWindow->GetPlayer()->getOutFrameIndex();

   if (markIn > -1)
      {
      if (markIn < inFrameIndex)
         markIn = inFrameIndex;
      else if (markIn > outFrameIndex)
         markIn = outFrameIndex;
      }

   if (markOut > -1)
      {
      if (markOut < inFrameIndex)
         markOut = inFrameIndex;
#ifdef OLD
      else if (markOut > outFrameIndex)
         markOut = outFrameIndex;
      }
#else
      else if (markOut > (outFrameIndex + 1))
         markOut = (outFrameIndex + 1);
      }
#endif

   // Tell the Player about the marks
   if (markIn > -1)
      mainWindow->GetPlayer()->setMarkIn(markIn);
   else
      mainWindow->GetPlayer()->removeMarkIn();
   if (markOut > -1)
      mainWindow->GetPlayer()->setMarkOut(markOut);
   else
      mainWindow->GetPlayer()->removeMarkOut();

   // Show the marks on the timeline (or hide them if they are not set)
   mainWindow->GetTimeline()->UpdateMarks();
}

//////////////////////////////////////////////////////////////////////
// Video Proxy & Video Framing Selection
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    selectVideoProxy
//
// Description: Selects a Video Proxy and initializes Player, Displayer,
//              etc., so Navigator is ready to use the clip.  The
//              Video Framing remains as previously selected
//
// Arguments:   int newVideoProxyIndex     Video Proxy Index
//                                           VIDEO_SELECT_NORMAL
//                                           VIDEO_SELECT_PROXY_1 thru
//                                           VIDEO_SELECT_PROXY_8
//                                         Video Proxies beyond 8 can be
//                                         selected by adding N-1 to
//                                         VIDEO_SELECT_PROXY_BASE.  For
//                                         example, VIDEO_SELECT_PROXY+9
//                                         will select the 10th proxy.
//
// Returns:      0 if success, non-zero otherwise.  If the caller's proxy
//               could not be selected (e.g., didn't exist), then the
//               previously selected proxy remains
//
//------------------------------------------------------------------------
int CNavCurrentClip::selectVideoProxy(int newVideoProxyIndex)
{
   int retVal;

   if (newVideoProxyIndex == currentVideoProxyIndex)
      return 0;      // Proxy already selected, nothing to do

   retVal = selectVideoProxyAndFraming(newVideoProxyIndex,
                                       currentVideoFramingIndex);
   if (retVal != 0)
      {
      // ERROR
      return retVal;
      }

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    selectVideoFraming
//
// Description:
//
// Arguments:   int newVideoFramingIndex
//
// Returns:
//
//------------------------------------------------------------------------
int CNavCurrentClip::selectVideoFraming(int newVideoFramingIndex)
{
   if (newVideoFramingIndex == currentVideoFramingIndex)
      return 0;      // Framing already selected, nothing to do

   int retVal;
   CToolManager toolMgr;

   toolMgr.onChangingFraming(); // Notify tools that framing is about to change

   if (toolMgr.CheckProvisionalUnresolvedAndToolProcessing())
      return -1;     // A change is pending or a tool is running so
                     // cannot change framing right now

   retVal = selectVideoProxyAndFraming(currentVideoProxyIndex,
                                       newVideoFramingIndex);
   if (retVal != 0)
      {
      // ERROR
      return retVal;
      }

   toolMgr.onNewFraming();    // Notify tools that framing has changed

   return 0;
}

int CNavCurrentClip::selectVideoProxyAndFraming(int newVideoProxyIndex,
                                                int newVideoFramingIndex)
{
   int retVal;

   if (currentClip == nullptr)
      return -510;  // ERROR: A Clip has not be set yet

   CVideoFrameList* newVideoFrameList;
   newVideoFrameList = currentClip->getVideoFrameList(newVideoProxyIndex,
                                                      newVideoFramingIndex);
   if (newVideoFrameList == nullptr)
      return -511; // ERROR: Caller's Video Proxy or Framing is not available

   // Get current display devices
   int curDisplayDevicesIndex = mainWindow->GetPlayer()->getDisplayMode();

   // Get index of last frame displayed
   int curFrameIndex = mainWindow->GetPlayer()->getLastFrameIndex();

   // Get the timecode for the last frame displayed
   string indexTCStr, fallbackTCStr;
   CVideoFrameList *curVideoFrameList = getCurrentVideoFrameList();
   CTimecode currentTimecode = curVideoFrameList->getInTimecode(); // Default
   currentTimecode.getTimeString(fallbackTCStr);
   CVideoFrame *currentFrame = curVideoFrameList->getFrame(curFrameIndex);
   if (currentFrame != 0)
      {
      currentTimecode = currentFrame->getTimecode();
      }

   // Find the frame index in the new track for the current timecode
   //int newFrameIndex, fallbackFrameIndex;        using TC now
   //newFrameIndex = newVideoFrameList->getFrameIndex(currentTimecode);
   //fallbackFrameIndex = newVideoFrameList->getInFrameIndex();  // Default

   // Turn the CurrentTimecode into a string
   currentTimecode.getTimeString(indexTCStr);


   // Get the timecodes for the Player's marks.  These will be used
   // to set the marks for the new frame list
   CTimecode markTC(0);
   string markInTCStr;
   if (mainWindow->GetPlayer()->getMarkIn() > -1)
      {
      markTC = mainWindow->GetPlayer()->getMarkInTimecode();
      markTC.getTimeString(markInTCStr);
      }
   string markOutTCStr;
   if (mainWindow->GetPlayer()->getMarkOut() > -1)
      {
      markTC = mainWindow->GetPlayer()->getMarkOutTimecode();
      markTC.getTimeString(markOutTCStr);
      }

   DISPLAY_CONTEXT dummyDisplayContext;

   CReticle dummyReticle;

   retVal = setClipVideoProxyAndFraming(currentClip,newVideoProxyIndex,
                                                    newVideoFramingIndex,
                                                    curDisplayDevicesIndex,
                                                    0, // use old display context
                                                    dummyDisplayContext,
                                                    0, // no display
                                                    dummyReticle,
                                                    //newFrameIndex,
                                                    //fallbackFrameIndex,
                                                    indexTCStr,
                                                    fallbackTCStr,
                                                    markInTCStr,
                                                    markOutTCStr);
   if (retVal != 0)
      return retVal; // ERROR: Could not set the new video proxy or track

   return 0;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CNavCurrentClip::saveClipState()
{
   if (currentClip == nullptr)
      return 0;  // Clip isn't available, so just return

   CBinManager bm;

   // Save state to clip, bin, and root desktop.ini files

   string sectionName = bm.getDesktopSectionUniqueName();

   CIniFile* iniFile = bm.openClipDesktop(currentClip);
   if (iniFile != NULL)
   {
      writeClipStateToIniFile(iniFile, sectionName);
      DeleteIniFile(iniFile);
   }

   iniFile = bm.openBinDesktop(currentClip);
   if (iniFile != NULL)
   {
      writeClipStateToIniFile(iniFile, sectionName);
      DeleteIniFile(iniFile);
   }

   iniFile = bm.openRootDesktop();
   if (iniFile != NULL)
   {
      writeClipStateToIniFile(iniFile, sectionName);
      DeleteIniFile(iniFile);
   }

#ifdef _OLD_LUT_CRAP_
   // Save the custom LUT values to the desktop.ini
   saveCustomLUT();
#endif

   return 0; // Success
}

int CNavCurrentClip::writeClipStateToIniFile(CIniFile *iniFile,
                                             const string& sectionName)
{
   if (iniFile == NULL)
      return -530;

   iniFile->WriteInteger(sectionName, proxyIndexKey, currentVideoProxyIndex);

   iniFile->WriteInteger(sectionName, framingIndexKey,
                         currentVideoFramingIndex);

   iniFile->WriteInteger(sectionName, displayDevicesIndexKey,
                         mainWindow->GetPlayer()->getDisplayMode());

   // write out elements of the current DISPLAY CONTEXT
   DISPLAY_CONTEXT currentDisplayContext = mainWindow->GetDisplayer()->
                                                        getDisplayContext();
   iniFile->WriteInteger(sectionName,displayModeValueKey,
                                     currentDisplayContext.displayMode);
   iniFile->WriteDouble(sectionName,zoomFactorValueKey,
                                     currentDisplayContext.zoomFactor);
   iniFile->WriteInteger(sectionName,displayXCenterValueKey,
                                     currentDisplayContext.dspXctr);
   iniFile->WriteInteger(sectionName,displayYCenterValueKey,
                                     currentDisplayContext.dspYctr);

   // write out the speed settings
   iniFile->WriteInteger(sectionName,slowDisplaySpeedValueKey,
                         mainWindow->GetPlayer()->getDisplaySpeed(DISPLAY_SPEED_SLOW));
   iniFile->WriteInteger(sectionName,mediumDisplaySpeedValueKey,
                         mainWindow->GetPlayer()->getDisplaySpeed(DISPLAY_SPEED_MEDIUM));
   iniFile->WriteInteger(sectionName,fastDisplaySpeedValueKey,
                         mainWindow->GetPlayer()->getDisplaySpeed(DISPLAY_SPEED_FAST));
   iniFile->WriteInteger(sectionName,displaySpeedIndexKey,
                         mainWindow->GetPlayer()->getDisplaySpeedIndex());

   iniFile->WriteInteger(sectionName,reticleModeKey,
                         mainWindow->GetDisplayer()->getReticleMode());

   // write out elements of the current RETICLE
   CReticle exitReticle = mainWindow->GetDisplayer()->getExitReticle();

   // if reticle type is CUSTOM do
   //    verify reticle
   //    if reticle is registered
   //       write reticle file
   //    else
   //       delete file key
   //    write reticle type
   //    write other parameters
   // else
   //    delete all keys but reticle type
   //    write reticle type

   if (exitReticle.getType() == RETICLE_TYPE_CUSTOM) {

      string filename = exitReticle.getFilename();

      if (filename == "[NEW]") {

         iniFile->DeleteKey(sectionName, reticleFileKey);
      }
      else {

         CReticle compareReticle;

         compareReticle.readFromFile(filename);

         if (compareReticle == exitReticle) {

            iniFile->WriteString(sectionName, reticleFileKey, filename);
         }
         else {

            iniFile->DeleteKey(sectionName, reticleFileKey);
         }
      }

      iniFile->WriteInteger(sectionName,reticleTypeKey, exitReticle.getType());

      iniFile->WriteDouble(sectionName, reticleAspectRatioKey,
                                        exitReticle.getAspectRatio());
      iniFile->WriteInteger(sectionName,reticleApertureKey,
                                        exitReticle.getAperture());
      iniFile->WriteInteger(sectionName,reticleXOffsetKey,
                                        exitReticle.getXOffset());
      iniFile->WriteInteger(sectionName,reticleYOffsetKey,
                                        exitReticle.getYOffset());
      iniFile->WriteInteger(sectionName,reticleHScaleKey,
                                        exitReticle.getHScale());
      iniFile->WriteInteger(sectionName,reticleVScaleKey,
                                        exitReticle.getVScale());
   }
   else { // default reticle or NONE

      iniFile->WriteInteger(sectionName,reticleTypeKey, exitReticle.getType());

      iniFile->DeleteKey(sectionName, reticleFileKey);
      iniFile->DeleteKey(sectionName, reticleAspectRatioKey);
      iniFile->DeleteKey(sectionName, reticleApertureKey);
      iniFile->DeleteKey(sectionName, reticleXOffsetKey);
      iniFile->DeleteKey(sectionName, reticleYOffsetKey);
      iniFile->DeleteKey(sectionName, reticleHScaleKey);
      iniFile->DeleteKey(sectionName, reticleVScaleKey);
   }

   // not writing frameIndexKey anymore, switched to timeCodeIndexKey
   //iniFile->WriteInteger(sectionName, frameIndexKey,
   //                      mainWindow->GetPlayer()->getLastFrameIndex());
   CTimecode timecode(0);
   string tcStr;
   if (mainWindow->GetPlayer()->getMarkIn() > -1)
      {
      timecode = mainWindow->GetPlayer()->getMarkInTimecode();
      timecode.getTimeString(tcStr);
      }
   else
      tcStr = "";  // In mark not set, write as empty string
   iniFile->WriteString(sectionName, markInKey, tcStr);
   if (mainWindow->GetPlayer()->getMarkOut() > -1)
      {
      timecode = mainWindow->GetPlayer()->getMarkOutTimecode();
      timecode.getTimeString(tcStr);
      }
   else
      tcStr = "";  // Out mark not set, write as empty string
   iniFile->WriteString(sectionName, markOutKey, tcStr);

   // getting the timeCodeIndexKey value and writing it out
   timecode = mainWindow->GetPlayer()->getCurrentTimecode();
   timecode.getTimeString(tcStr);
   iniFile->WriteString(sectionName, timeCodeIndexKey, tcStr);

   return 0; // Success
}

#ifdef _OLD_LUT_CRAP_
//------------------------------------------------------------------------
//
// Function:    reloadCurrentClipCustomLUT
//
// Description: Reloads the custom LUT for the current clip, in case
//              the user changed it or the customLUT for one opf the
//              clip's bins
//
// Arguments:   None
//
// Returns:     Nothing
//
//------------------------------------------------------------------------
void CNavCurrentClip::reloadCurrentClipCustomLUT()
{
    if (currentClip == nullptr)
        return;

    CDisplayer *Displayer = mainWindow->GetDisplayer();
    CPlayer *Player = mainWindow->GetPlayer();

    loadCustomLUT(currentClip, -1, Displayer->getDefaultGamma());

    Displayer->establishDisplayLUT();
    Player->refreshFrame();
}

//------------------------------------------------------------------------
//
// Function:    loadCustomLUT
//
// Description: Figures out custom display LUT values based on what's
//              stored in the clip's desktop.ini file and what values
//              are associated with the clip's bins. Passes the values
//              to the displayer.
//
// Arguments:   ClipSharedPtr &newClip     Pointer to new clip
//              int LUTChoice      Choice for display LUT (0=internal,
//                                 1=default, 2=custom); -1 means use the
//                                 value from the clip's desktop.ini (or
//                                 'default' if there isn't one)
//              int defaultGamma   The value to use for display gamma
//                                 when there isn't one in the clip's
//                                 desktop.ini
//
// Returns:     0 if success, non-zero otherwise
//
//------------------------------------------------------------------------
int CNavCurrentClip::loadCustomLUT(ClipSharedPtr &clip,
                                   int LUTChoice,
                                   int defaultGamma)
{
   CBinManager bm;
   string sectionName = bm.getDesktopSectionUniqueName();

   MTI_UINT16 sourceRangeBlk[3],
              sourceRangeWhi[3];
   MTI_UINT16 displayRangeMin[3],
              displayRangeMax[3];
   int newColorSpaceIndex = 0;          // default
   int newTransferFunctionIndex = 0;    // default
   bool newUseCustomRange = false;
   bool newUseFormatDefaultLUT = false;
   int newGammaValue = defaultGamma;

   // Open desktop.ini file.
   CIniFile* iniFile = bm.openClipDesktop(clip);
   if (iniFile == NULL) {
      TRACE_1(errout << "CNavCurrentClip::loadCustomLUT: cannot open desktop.ini");
      return -1;
   }

// the color space index of the LUT:
//
//    0 - default
//    1 - linear
//    2 - log
//    3 - SMPTE 240
//    4 - CCIR 709
//    5 - CCIR 601BG
//    6 - CCIR 601M
   newColorSpaceIndex = iniFile->ReadInteger(sectionName,
                                             customLUTColorSpaceIndexKey,
                                             newColorSpaceIndex);

   // No custom transfer function for clip YET.

   const CImageFormat *fmt = clip->getImageFormat(VIDEO_SELECT_NORMAL);

   newUseCustomRange = iniFile->ReadBool(sectionName,
                                         customLUTSourceRangeKey,
                                         newUseCustomRange);


   // load defaults in case desktop.ini has none
   fmt->getComponentValueMin(sourceRangeBlk);
   fmt->getComponentValueMax(sourceRangeWhi);

   // read the custom source range values from desktop.ini
   //  -- the defaults come from the clip's image format

   sourceRangeBlk[0] = iniFile->ReadInteger(sectionName,
                                            customLUTRYBlkKey,
                                            sourceRangeBlk[0]);

   sourceRangeWhi[0] = iniFile->ReadInteger(sectionName,
                                            customLUTRYWhiKey,
                                            sourceRangeWhi[0]);

   sourceRangeBlk[1] = iniFile->ReadInteger(sectionName,
                                            customLUTGUBlkKey,
                                            sourceRangeBlk[1]);

   sourceRangeWhi[1] = iniFile->ReadInteger(sectionName,
                                            customLUTGUWhiKey,
                                            sourceRangeWhi[1]);

   sourceRangeBlk[2] = iniFile->ReadInteger(sectionName,
                                            customLUTBVBlkKey,
                                            sourceRangeBlk[2]);

   sourceRangeWhi[2] = iniFile->ReadInteger(sectionName,
                                            customLUTBVWhiKey,
                                            sourceRangeWhi[2]);

   // read the custom display range values from desktop.ini
   // -- the defaults are just 0 and 255 for min & max

   displayRangeMin[0] = iniFile->ReadInteger(sectionName,
                                             customLUTRMinKey,
                                             0);

   displayRangeMax[0] = iniFile->ReadInteger(sectionName,
                                             customLUTRMaxKey,
                                             255);

   displayRangeMin[1] = iniFile->ReadInteger(sectionName,
                                             customLUTGMinKey,
                                             0);

   displayRangeMax[1] = iniFile->ReadInteger(sectionName,
                                             customLUTGMaxKey,
                                             255);

   displayRangeMin[2] = iniFile->ReadInteger(sectionName,
                                             customLUTBMinKey,
                                             0);

   displayRangeMax[2] = iniFile->ReadInteger(sectionName,
                                             customLUTBMaxKey,
                                             255);
   newGammaValue = iniFile->ReadInteger(sectionName,
                                        gammaValueKey,
                                        defaultGamma);

   if (LUTChoice < 0) { // select clip's last setting

      // Determine whether last use of clip had custom LUT enabled.
      // If desktop.ini contains no custom LUT data use default LUT
      LUTChoice = iniFile->ReadInteger(sectionName,
                                         customLUTKey,
                                         LUT_OPTION_DEFAULT);

      // Sometimes LUT_OPTION_DEFAULT means "use the bin custom LUT"
      // and sometimes it means "use the default for the clip format"
      newUseFormatDefaultLUT = iniFile->ReadInteger(sectionName,
                                         customLUTFormatDefaultKey,
                                         false);
   }

   // close the desktop.ini file - doesn't actually delete it!!
   iniFile->FileName = "";  // Don't write back the ini file
   if (!DeleteIniFile(iniFile)) {
      TRACE_1(errout << "CNavCurrentClip::loadCustomLUT: cannot close desktop.ini");
   }

   //////////////////////////////////////////////////////////////////

   CDisplayer *Displayer = mainWindow->GetDisplayer();

   // save all the custom LUT values
   Displayer->setCustomLUTOption(LUTChoice);
   Displayer->setCustomLUTColorSpaceIndex(newColorSpaceIndex);
   Displayer->setCustomLUTTransferFunctionIndex(newTransferFunctionIndex);
   Displayer->setCustomLUTSourceRange(newUseCustomRange);
   Displayer->setCustomLUTSourceRangeBlk(sourceRangeBlk);
   Displayer->setCustomLUTSourceRangeWhi(sourceRangeWhi);
   Displayer->setCustomLUTDisplayRangeMin(displayRangeMin);
   Displayer->setCustomLUTDisplayRangeMax(displayRangeMax);
   Displayer->setCustomLUTGamma(newGammaValue);

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    saveCustomLUT
//
// Description: Sucks the custom LUT values out of the Displayer and saves
//              them in the current clip's desktop.ini file.
//
// Arguments:   None
//
// Returns:     0 if success, non-zero otherwise
//
//------------------------------------------------------------------------
void CNavCurrentClip::saveCustomLUT()
{
   if (currentClip == nullptr)
      return;  // Clip isn't available, so just return

   CBinManager bm;
   string sectionName = bm.getDesktopSectionUniqueName();

   // Ugly hackery to try to figure out what the user diddled LUTwise
   // and make some sense of it.
   CBinDisplayLUT  binLUT;
   CDisplayer *Displayer = mainWindow->GetDisplayer();
   int newLUTIndex = Displayer->getCustomLUTOption();
   int newGammaValue = Displayer->getCustomLUTGamma();
   int newColorSpaceIndex = Displayer->getCustomLUTColorSpaceIndex();
   int newTransferFunctionIndex = Displayer->getCustomLUTTransferFunctionIndex();
   bool newUseCustomRange = Displayer->getCustomLUTSourceRangeEnabled();
   MTI_UINT16 sourceRefBlack[3], sourceRefWhite[3];
   MTI_UINT16 displayRangeMin[3], displayRangeMax[3];
   bool clipCustomValuesMatchBinCustomValues = false;
   bool newUseFormatDefaultLUT = false;

   Displayer->getCustomLUTSourceRangeBlk(sourceRefBlack);
   Displayer->getCustomLUTSourceRangeWhi(sourceRefWhite);
   Displayer->getCustomLUTDisplayRangeMin(displayRangeMin);
   Displayer->getCustomLUTDisplayRangeMax(displayRangeMax);

   // Open desktop.ini file. The argument 'sectionName' is passed by
   // reference and is filled in with the user-appropriate section
   // name within the destop.ini file (which maintains separate values
   // per user).
   CIniFile* iniFile = bm.openClipDesktop(currentClip);
   if (iniFile == NULL) {
      TRACE_1(errout << "CNavCurrentClip::saveCustomLUT: cannot open desktop.ini");
      return;
   }

   // dump the custom LUT settings
   iniFile->WriteInteger(sectionName, customLUTKey, newLUTIndex);
   iniFile->WriteInteger(sectionName, customLUTFormatDefaultKey,
                                      newUseFormatDefaultLUT);
   iniFile->WriteInteger(sectionName, customLUTColorSpaceIndexKey,
                                      newColorSpaceIndex);

   iniFile->WriteBool(sectionName, customLUTSourceRangeKey, newUseCustomRange);
   iniFile->WriteInteger(sectionName, customLUTRYBlkKey, sourceRefBlack[0]);
   iniFile->WriteInteger(sectionName, customLUTRYWhiKey, sourceRefWhite[0]);
   iniFile->WriteInteger(sectionName, customLUTGUBlkKey, sourceRefBlack[1]);
   iniFile->WriteInteger(sectionName, customLUTGUWhiKey, sourceRefWhite[1]);
   iniFile->WriteInteger(sectionName, customLUTBVBlkKey, sourceRefBlack[2]);
   iniFile->WriteInteger(sectionName, customLUTBVWhiKey, sourceRefWhite[2]);

   iniFile->WriteInteger(sectionName, customLUTRMinKey, displayRangeMin[0]);
   iniFile->WriteInteger(sectionName, customLUTRMaxKey, displayRangeMax[0]);
   iniFile->WriteInteger(sectionName, customLUTGMinKey, displayRangeMin[1]);
   iniFile->WriteInteger(sectionName, customLUTGMaxKey, displayRangeMax[1]);
   iniFile->WriteInteger(sectionName, customLUTBMinKey, displayRangeMin[2]);
   iniFile->WriteInteger(sectionName, customLUTBMaxKey, displayRangeMax[2]);

   iniFile->WriteInteger(sectionName,gammaValueKey, newGammaValue);

   // close the desktop.ini file - doesn't actually delete it!!
   if (!DeleteIniFile(iniFile)) {
      TRACE_1(errout << "CNavCurrentClip::saveCustomLUT: cannot open desktop.ini");
   }
}
#endif

//////////////////////////////////////////////////////////////////////
// Current Clip Information Accessors
//////////////////////////////////////////////////////////////////////

string CNavCurrentClip::getPreviousBinPath() const
{
   return previousBinPath;
}

string CNavCurrentClip::getPreviousClipName() const
{
   return previousClipName;
}

string CNavCurrentClip::getCurrentBinPath() const
{
   if (currentClip == nullptr)
      return("");    // Return empty string

   // get and return the current clip's REAL bin path (which is the
   // bin of the parent clip for version clips)
   CBinManager binManager;
   return binManager.getRealBinPath(currentClip->getClipPathName());
}

string CNavCurrentClip::getCurrentParentPath() const
{
   if (currentClip == nullptr)
      return("");    // Return empty string

   // get and return the current clip's "bin" path which in the
   // case of version clips is really the path of the parent clip!
   return currentClip->getBinPath();
}

string CNavCurrentClip::getCurrentClipPath() const
{
   if (currentClip == nullptr)
      return("");    // Return empty string

   // get and return the current clip's directory path (including the
   // Bin path).
   CBinManager binManager;
   return binManager.makeClipPath(currentBinPath, currentClipName);
}

ClipSharedPtr CNavCurrentClip::getCurrentClip() const
{
   return currentClip;
}

string CNavCurrentClip::getCurrentClipFileName() const
{
   if (currentClip == nullptr)
      return("");    // Return empty string

   // get and return the current clip's file name including the
   // Bin path.  Name does not include the file extension
   CBinManager binManager;
   return binManager.makeClipFileName(currentBinPath, currentClipName);
}

string CNavCurrentClip::getCurrentClipFileNameWithExt() const
{
   if (currentClip == nullptr)
      return("");    // No clip, return empty string

   // get and return the current clip's file name including the
   // Bin path and the extension .clp
   CBinManager binManager;
   return binManager.makeClipFileNameWithExt(currentBinPath, currentClipName);
}

string CNavCurrentClip::getCurrentClipName() const
{
   if (currentClip == nullptr)
      return("");    // Return empty string

   // get and return the current clip's  name
   return currentClip->getClipName();
}

CVideoFrameList* CNavCurrentClip::getCurrentVideoFrameList() const
{
   if (currentClip == nullptr)
      return 0;

   return currentClip->getVideoFrameList(currentVideoProxyIndex,
                                         currentVideoFramingIndex);
}

int CNavCurrentClip::getCurrentVideoProxyIndex() const
{
   return currentVideoProxyIndex;
}

int CNavCurrentClip::getCurrentVideoFramingIndex() const
{
   return currentVideoFramingIndex;
}

int CNavCurrentClip::getCurrentDisplayDevicesIndex() const
{
   return (mainWindow->GetPlayer()->getDisplayMode());
}

int CNavCurrentClip::getCurrentDisplayModeIndex() const
{
   return (mainWindow->GetDisplayer()->getDisplayMode());
}

int CNavCurrentClip::getCurrentSlowDisplaySpeedValue() const
{
   return (mainWindow->GetPlayer()->getDisplaySpeed(DISPLAY_SPEED_SLOW));
}

int CNavCurrentClip::getCurrentMediumDisplaySpeedValue() const
{
   return (mainWindow->GetPlayer()->getDisplaySpeed(DISPLAY_SPEED_MEDIUM));
}

int CNavCurrentClip::getCurrentFastDisplaySpeedValue() const
{
   return (mainWindow->GetPlayer()->getDisplaySpeed(DISPLAY_SPEED_FAST));
}

int CNavCurrentClip::getCurrentDisplaySpeedIndex() const
{
   return (mainWindow->GetPlayer()->getDisplaySpeedIndex());
}



bool CNavCurrentClip::isClipAvailable() const
{
   return (currentClip != nullptr);
}

bool CNavCurrentClip::isThisCurrentClip(const string &clipFileName)
{
   // return true if the caller's clip name is the current clip

   if (!isClipAvailable())
      return false;    // no clip available, so forget it

   CBinManager binMgr;

   // Force the clip file name into a standardized format
   string stdClipFileName = binMgr.makeClipFileNameWithExt(clipFileName);

   string currentClipFileName = getCurrentClipFileNameWithExt();

   if (currentClipFileName == stdClipFileName)
       return true;

   // if the file names don't match, check the GUIDs in case clip has
   // been moved or renamed

   int stat;
   auto clipArg = binMgr.openClip(clipFileName, &stat);
   if (stat != 0)
       return false;

   bool match = (currentClip->compareClipGUID(clipArg->getClipGUID()) == 0);

   binMgr.closeClip(clipArg);

   return match;
}

//////////////////////////////////////////////////////////////////////
// Utility Functions
//////////////////////////////////////////////////////////////////////

CTimecode CNavCurrentClip::limitTimecode(const CTimecode& newTC, bool warn)
{
   // Return a timecode that is bounded by the current frame list's
   // in and out timecodes

   CVideoFrameList *frameList = getCurrentVideoFrameList();
   if (frameList == nullptr)
      {
      if (warn)
         _MTIWarningDialog("Clip is not loaded");
      return CTimecode(0);    // Probably no clip loaded, shouldn't happen
      }

   int inFrame = frameList->getInFrameIndex();
   int outFrame = frameList->getOutFrameIndex();
   CTimecode inTC = frameList->getInTimecode();
   CTimecode outTC = frameList->getOutTimecode();

   // Get the frame index for the input timecode
   int TCFrameIndex = frameList->getFrameIndex(newTC);

   if ((TCFrameIndex == -1 && newTC < inTC)
       || (TCFrameIndex != -1 && TCFrameIndex < inFrame))
      {
      if (warn)
         _MTIWarningDialog("Timecode is before beginning of clip\nSetting to beginning of clip");

      TCFrameIndex = inFrame;
      }
   else if ((TCFrameIndex == -1 && newTC >= outTC)
            || (TCFrameIndex != -1 && TCFrameIndex >= outFrame))
      {
      if (warn)
         _MTIWarningDialog("Timecode is beyond end of clip\nSetting to end of clip");

      TCFrameIndex = outFrame - 1;
      }

   return frameList->getTimecodeForFrameIndex(TCFrameIndex);

}

//////////////////////////////////////////////////////////////////////
