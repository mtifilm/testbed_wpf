// MaskTransformEditor.h
//
#ifndef MaskTransformEditorH
#define MaskTransformEditorH

#include "machine.h"

// ---------------------------------------------------------------------------
// Forward Declarations

class CDisplayer;
class Vertex;

// ---------------------------------------------------------------------------

class CMaskTransformEditor
{
public:

   CMaskTransformEditor(CDisplayer *);
   ~CMaskTransformEditor();

   void setImageRectangle(RECT&);
   void calculateWidgetSize();
   void calculateTransformWidget();
   void resetTransform();
   void initTransformWidget();
   void restoreTransform();

   void setShift(bool);
   void setAlt(bool);
   void setCtrl(bool);

   void mouseDown();
   void enterDragMode();
   void setMajorState(int);
   int  getMajorState();
   void mouseMove(int x, int y, bool doIdleRefresh);
   void mouseUp  ();

   void drawTransform();

   void refreshTransform();

   POINT* TransformLasso(POINT *srcLasso, int count, POINT *dstLasso);
   void TransformBezier(BEZIER_POINT *src);

   void TransformBezier(Vertex *src);
   void InverseTransformBezier(Vertex *src);

private:

   int getQuadrant(int x, int y);
   int crossProduct(int x0, int y0, int x1, int y1);
   bool isOutsideTransformOutline(int x, int y);

   int getWidgetBox(RECT&);

   double getAngle(double cosine, double sine);
   double getAngleFromClientCoords(int x, int y);

   void calculateMidpoints(DPOINT *src);
   bool isTooSmall(DPOINT *src, int inipt, int finpt);
   bool isTooSmall(DPOINT *src);

   void transformRotatedToStretched(DPOINT *dst, DPOINT *src, double *angle);
   void transformStretchedToSkewed(DPOINT *dst, DPOINT *src,
                                   double *xstr, double *ystr,
                                   double *skew);

   void acceptTentativeWidget();

   void transformSkewedToStretched(DPOINT *dst, DPOINT *src, double xstr, double ystr);
   void transformStretchedToRotated(DPOINT *dst, DPOINT *src, double angle);

private:

   CDisplayer *displayer;

   RECT rgbRect;
   int rgbWdth, rgbHght;
   int rgbXctr, rgbYctr;

   int wdgtSize;

   DPOINT skwWidget[10];

   DPOINT strWidget[10];

   DPOINT rotWidget[10];

   DPOINT tntWidget[10];

   // mouse client coords
   int mouseXClient,
       mouseYClient;

   // mouse frame coords
   int mouseXFrame,
       mouseYFrame;

   // mouse frame deltas
   int deltaMouseXFrame,
       deltaMouseYFrame;

   // for rotation dynamics
   double curAng, newAng;

   // for skew dynamics - 0-7
   int widgetBox;

   double netRotationAngle;
   double netStretchX;
   double netStretchY;
   double netSkew;
   double netOffsetX;
   double netOffsetY;

#define MOD_KEY_NONE  0
#define MOD_KEY_SHIFT 1
#define MOD_KEY_ALT   2
#define MOD_KEY_CTRL  4
   int modKeys;

#define MAJORSTATE_TRACK            0
#define MAJORSTATE_KEEP_ANGLES      1
#define MAJORSTATE_SCALE_MASK       2
#define MAJORSTATE_SKEW_MASK        3
#define MAJORSTATE_ROTATE_MASK      4
#define MAJORSTATE_OFFSET_MASK      5
   int majorState;

   int cntr;
};

// ---------------------------------------------------------------------------

#endif // #ifndef MASK_TRANSFORM_EDITOR_H

