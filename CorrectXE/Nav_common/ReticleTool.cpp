// ReticleTool.cpp: implementation of the CReticleTool class.
//
///////////////////////////////////////////////////////////////////////////////

#include "ReticleTool.h"
#include "Displayer.h"
#include "IniFile.h"
#include "PDL.h"
#include "ToolObject.h"
#include "ToolSystemInterface.h"
#include "CustomReticleUnit.h"

#ifdef __BORLANDC__
#include "MainWindowUnit.h"
#endif // #ifdef __BORLANDC__

//////////////////////////////////////////////////////////////////////
// Static Variables
//////////////////////////////////////////////////////////////////////

string CReticleTool::reticleToolSectionName = "ReticleTool";

///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Constructor/Destructor
///////////////////////////////////////////////////////////////////////////////

CReticleTool::CReticleTool()
 : reticleToolEnabled(false),
   reticleToolInternalEnabled(true),
   baseSystemAPI(0), mouseMode(RETICLE_TOOL_MOUSE_MODE_NONE)
{
}

CReticleTool::~CReticleTool()
{
}

///////////////////////////////////////////////////////////////////////////////
// Accessors
///////////////////////////////////////////////////////////////////////////////

//const CReticle& CReticleTool::GetReticle()
//{
//   return reticle;
//}

CToolSystemInterface* CReticleTool::getSystemAPI()
{
   return baseSystemAPI;
}

bool CReticleTool::isReticleToolEnabled() const
{
   return reticleToolEnabled;
}

bool CReticleTool::isReticleToolInternalEnabled() const
{
   return reticleToolInternalEnabled;
}

///////////////////////////////////////////////////////////////////////////////
// Mutators
///////////////////////////////////////////////////////////////////////////////

// this method is used to set the tool to the mode
// in which it captures the stretched rectangle
void CReticleTool::setReticleToolEnabled(bool newEnabled)
{
   if (!reticleToolInternalEnabled) return;

   if (reticleToolEnabled != newEnabled)
      {
      reticleToolEnabled = newEnabled;
      }
}

void CReticleTool::setReticleToolInternalEnabled(bool newEnabled)
{
   if (reticleToolInternalEnabled != newEnabled)
      {
      reticleToolInternalEnabled = newEnabled;
      }
}

void CReticleTool::setSystemAPI(CToolSystemInterface *newSystemAPI)
{
   baseSystemAPI = newSystemAPI;
   CustomReticleDlg->setSystemAPI(newSystemAPI);
}

///////////////////////////////////////////////////////////////////////////////
// Tool Event Handlers
///////////////////////////////////////////////////////////////////////////////

int CReticleTool::BeginUserSelect(CDisplayer *displayer)
{
#ifdef OLD
   // Filter out auto-repeat on PC and multiple key + mouse button presses
   if (mouseMode != RETICLE_TOOL_MOUSE_MODE_NONE)
      return TOOL_RETURN_CODE_EVENT_CONSUMED;

   // clear any reticle showing
   displayer->setReticleShape(RETICLE_SHAPE_NONE);
   getSystemAPI()->refreshFrameCached();

   // set up to stretch a YELLOW rectangle
   getSystemAPI()->setGraphicsColor(SOLID_YELLOW);
   getSystemAPI()->setDRSLassoMode(false);

   // start the reticle RECT stretch
   getSystemAPI()->StartStretchRectangleOrLasso();

   mouseMode = RETICLE_TOOL_MOUSE_MODE_STRETCH;
#endif

   return(TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
}

int CReticleTool::EndUserSelect(CDisplayer *displayer)
{
#ifdef OLD
   // Filter out auto-repeat on PC and multiple key & mouse button presses
   if (mouseMode != RETICLE_TOOL_MOUSE_MODE_STRETCH)
      return TOOL_RETURN_CODE_EVENT_CONSUMED;

   // Ask the Base System to stop the stretching rectangle
   getSystemAPI()->stopStretchRectangleOrLasso();
   mouseMode = RETICLE_TOOL_MOUSE_MODE_NONE;

   // Ask the Base System for the coordinates of the rectangle
   // in image/frame coordinate space
   RECT imageRect;
   POINT *lassoPtr;
   if (getSystemAPI()->getStretchRectangleCoordinates(imageRect, lassoPtr)==0)
     {
      if (lassoPtr == 0)
        {
          displayer->setCustomReticle(&imageRect);
        }

      // Show the new custom reticle
      displayer->setReticleShape(RETICLE_SHAPE_CUSTOM);
      if (displayer->getReticleMode() == RETICLE_MODE_NONE)
         displayer->setReticleMode(RETICLE_MODE_ALL);
      getSystemAPI()->refreshFrameCached();
     }
#endif

   return TOOL_RETURN_CODE_EVENT_CONSUMED ;
}

#if 0
int CReticleTool::DrawReticle()
{
   getSystemAPI()->setGraphicsColor(SOLID_YELLOW);

   //getSystemAPI()->drawRectangleFrame(&reticleRect);

   return 0;
}
#endif

#if 0
void CReticleTool::ClearAll(CDisplayer *displayer)
{
   ClearReticle();

   getSystemAPI()->refreshFrameCached();
}
#endif

///////////////////////////////////////////////////////////////////////////////
// PDL Entry Interface
///////////////////////////////////////////////////////////////////////////////

#if 0
void CReticleTool::CapturePDLEntry(CPDLEntry &pdlEntry)
{
   //CPDLEntry_Reticle *pdlEntryReticle = pdlEntry.AddReticle();
}

void CReticleTool::SetReticle(CPDLEntry &pdlEntry, CDisplayer *displayer)
{
   // Set the reticle from a PDL Entry
   //ClearReticle();

   //reticle = pdlEntry.reticle;

   //getSystemAPI()->refreshFrameCached();

}
#endif

///////////////////////////////////////////////////////////////////////////////
// Settings
///////////////////////////////////////////////////////////////////////////////

#if 0
int CReticleTool::toolRestoreSettings()
{
   CIniFile *iniFile = CreateIniFile(CPMPIniFileName());
   if (iniFile == 0)
      return -1;

   if (!ReadSettings(iniFile, reticleToolSectionName))
      return -2;

   iniFile->FileName = "";  // don't write the ini file
   DeleteIniFile(iniFile);

   return 0;
}

int CReticleTool::toolSaveSettings()
{
   CIniFile *iniFile = CreateIniFile(CPMPIniFileName());
   if (iniFile == 0)
      return -1;

   if (!WriteSettings(iniFile, reticleToolSectionName))
      return -2;

   DeleteIniFile(iniFile);      // writes the ini file too

   return 0;
}

//---------------------------------------------------------------------------

bool CReticleTool::ReadSettings(CIniFile *iniFile, const string &sectionName)
{
   // Reads configuration.
   if (iniFile == NULL)
      return false;

   return true;
}

//---------------------------------------------------------------------------

bool CReticleTool::WriteSettings(CIniFile *iniFile, const string &sectionName)
{
   // WritFilees the configuration to the file
   if (iniFile == NULL)
      return false;

   return true;
}
#endif

//---------------------------------------------------------------------------

