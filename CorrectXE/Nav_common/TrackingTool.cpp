// TrackingTool.cpp: implementation of the CTrackingTool class.
//
/*
$Header: /usr/local/filmroot/Nav_common/source/Attic/TrackingTool.cpp,v 1.1.2.8 2009/07/30 00:51:26 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "TrackingTool.h"

#include "bthread.h"
#include "Clip3.h"
#include "err_tool.h"
#include "ImageFormat3.h"
#include "IniFile.h"  // for TRACE_N()
#include "MathFunctions.h"
#include "MTIKeyDef.h"
#include "MTImalloc.h"
#include "MTIsleep.h"
#include "MTIstringstream.h"
#include "MTIWinInterface.h"  // for CBusyCursor
#include "PixelRegions.h"
#include "ThreadLocker.h"
#include "ToolCommand.h"
#include "ToolCommandTable.h"
#include "ToolProgressMonitor.h"
#include "ToolSystemInterface.h"
#include "ToolUserInputMap.h"
#include "TrackingBoxManager.h"
#include "TrackingProc8.h"

#include <iomanip>

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

static MTI_UINT16 TrackingToolNumber = TRACKING_TOOL_NUMBER;


// -------------------------------------------------------------------------
// Tracking Default Keyboard and Mouse Button Configuration

static CUserInputConfiguration::SConfigItem TrackingDefaultConfigItems[] =
{
   // Tracking Command                          Modifiers      Action
   //                                       + Key
   // ----------------------------------------------------------------

 { TRK_CMD_ADD_SELECT_OR_DRAG_TRACKPOINT,   " LButton          ButtonDown " },
 { TRK_CMD_STOP_DRAGGING_TRACKPOINT,        " LButton          ButtonUp   " },
 { TRK_CMD_SELECT_ANOTHER_TRACKPOINT,       " Ctrl+LButton     ButtonDown " },
 { TRK_CMD_STOP_DRAGGING_TRACKPOINT,        " Ctrl+LButton     ButtonUp   " },

// These don't seem to be hooked up...
// { TRK_CMD_LOAD_TRACKPOINTS,                " Ctrl+L           KeyDown    " },
// { TRK_CMD_LOAD_TRACKPOINTS_NOT_MARKS,      " Shift+L          KeyDown    " },
// { TRK_CMD_SAVE_TRACKPOINTS,                " Ctrl+S           KeyDown    " },
 { TRK_CMD_CLEAR_TRACKPOINTS,               " Ctrl+Backspace   KeyDown    " },
 { TRK_CMD_CLEAR_TRACKPOINTS,               " Shift+A          KeyDown    " },
 { TRK_CMD_DELETE_SELECTED_TRACKPOINTS,     " Backspace        KeyDown    " },
 { TRK_CMD_DELETE_SELECTED_TRACKPOINTS,     " A                KeyDown    " },

 { TRK_CMD_BEGIN_PAN,                       " U                KeyDown    " },
 { TRK_CMD_END_PAN,                         " U                KeyUp      " },

 { TRK_CMD_TRACK,                           " T                KeyDown    " },
 { TRK_CMD_STOP,                            " Ctrl+Space       KeyDown    " },
};


static CUserInputConfiguration *TrackingDefaultUserInputConfiguration
= new CUserInputConfiguration(sizeof(TrackingDefaultConfigItems)
                              /sizeof(CUserInputConfiguration::SConfigItem),
                               TrackingDefaultConfigItems);

// -------------------------------------------------------------------------
// Tracking Command Table

static CToolCommandTable::STableEntry TrackingCommandTableEntries[] =
{
   // Tracking Command                         Tracking Command String
   // -------------------------------------------------------------

 { TRK_CMD_ADD_SELECT_OR_DRAG_TRACKPOINT,   "TRK_CMD_ADD_SELECT_OR_DRAG_TRACKPOINT" },
 { TRK_CMD_STOP_DRAGGING_TRACKPOINT,        "TRK_CMD_STOP_DRAGGING_TRACKPOINT" },
 { TRK_CMD_SELECT_ANOTHER_TRACKPOINT,       "TRK_CMD_SELECT_ANOTHER_TRACKPOINT" },
 { TRK_CMD_STOP_DRAGGING_TRACKPOINT,        "TRK_CMD_STOP_DRAGGING_TRACKPOINT" },

 { TRK_CMD_LOAD_TRACKPOINTS,                "TRK_CMD_LOAD_TRACKPOINTS" },
 { TRK_CMD_LOAD_TRACKPOINTS_NOT_MARKS,      "TRK_CMD_LOAD_TRACKPOINTS_NOT_MARKS" },
 { TRK_CMD_SAVE_TRACKPOINTS,                "TRK_CMD_SAVE_TRACKPOINTS" },
 { TRK_CMD_CLEAR_TRACKPOINTS,               "TRK_CMD_CLEAR_TRACKPOINTS" },
 { TRK_CMD_CLEAR_TRACKPOINTS,               "TRK_CMD_CLEAR_TRACKPOINTS" },
 { TRK_CMD_DELETE_SELECTED_TRACKPOINTS,     "TRK_CMD_DELETE_SELECTED_TRACKPOINTS" },

 { TRK_CMD_BEGIN_PAN,                       "TRK_CMD_BEGIN_PAN" },
 { TRK_CMD_END_PAN,                         "TRK_CMD_END_PAN" },

 { TRK_CMD_TRACK,                           "TRK_CMD_TRACK" },
 { TRK_CMD_STOP,                            "TRK_CMD_STOP" },
};

static CToolCommandTable *TrackingCommandTable
   = new CToolCommandTable(sizeof(TrackingCommandTableEntries)
                           /sizeof(CToolCommandTable::STableEntry),
                           TrackingCommandTableEntries);


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTrackingTool::CTrackingTool(const string &newToolName)
:  CToolObject(newToolName, NULL),
   trackingToolSetupHandle(-1),
   TrkPtsEditor(NULL),
   TrackingIsEnabled(false),
   TrackingInProgress(false),
   advisoryLock(false)
{
}

CTrackingTool::~CTrackingTool()
{
}

//////////////////////////////////////////////////////////////////////
// Built-in tools require a factory method
//////////////////////////////////////////////////////////////////////

CToolObject* CTrackingTool::MakeTool(const string& newToolName)
{
   return new CTrackingTool(newToolName);
}


// ===================================================================
//
// Function:    toolInitialize
//
// Description: Called when the ToolManager wants to initialize
//              the Tracking Tool
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================

int CTrackingTool::toolInitialize(CToolSystemInterface *newSystemAPI)
{
   int retVal;

   // Initialize the CTrackingTool's base class, i.e., CToolObject
   // This must be done before the CTrackingTool initializes itself
   retVal = initBaseToolObject(TrackingToolNumber, newSystemAPI,
                               TrackingCommandTable,
                               TrackingDefaultUserInputConfiguration);
   if (retVal != 0)
   {
      // ERROR: initBaseToolObject failed
      TRACE_0(errout << "Error in TrackingTool, initBaseToolObject failed "
                     << retVal);
      return retVal;
   }

   return retVal;
}


// ===================================================================
//
// Function:    toolShutdown
//
// Description: Called when the Tool Manager wants to shut down the
//              Tracking Tool, typically when the main program is
//              shutting down.
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================

int CTrackingTool::toolShutdown()
{
   int retVal;

   DestroyAllToolSetups();

   // Shutdown the Tracking Tool's base class, i.e., CToolObject.
   // This must be done after the Tracking Tool shuts down itself
   retVal = shutdownBaseToolObject();
   if (retVal != 0)
      {
      // ERROR: shutdownBaseToolObject failed
      return retVal;
      }

   return retVal;
} // end toolShutdown

// ===================================================================

CToolProcessor* CTrackingTool::makeToolProcessorInstance(
                                           const bool *newEmergencyStopFlagPtr)
{
   CTrackingProc *newToolProc;

   // Make a new tool processor, called by MakeSimpleToolSetup
   newToolProc =  new CTrackingProc(GetToolNumber(), GetToolName(),
                                    getSystemAPI(), newEmergencyStopFlagPtr,
                                    GetToolProgressMonitor());
#ifdef DEBUG_VISUALIZATION
#ifdef _DEBUG
   const CImageFormat *imageFormat = getSystemAPI()->getVideoClipImageFormat();
   int width = imageFormat->getPixelsPerLine();
   int height = imageFormat->getLinesPerFrame();
   _debugVisualizationArray = Ipp8uArray({width, height});
   newToolProc->SetDebugParameters(_debugVisualizationArray, &_debugVisualizationLock);
#endif
#endif

   return newToolProc;
}
//--------------------------------------------------------------------------

int CTrackingTool::toolActivate()
{
   return 0;        // Built-in tool, always active
}
//--------------------------------------------------------------------------

int CTrackingTool::toolDeactivate()
{
   return 0;        // Built-in tool, always active
}
//--------------------------------------------------------------------------

int CTrackingTool::onRedraw(int frameIndex)
{
   if (TrackingIsEnabled && (TrkPtsEditor != NULL))
   {
      TrkPtsEditor->drawTrackingPointsCurrentFrame();
   }

   return TOOL_RETURN_CODE_NO_ACTION;   // Let other tools draw on the frame
}
//--------------------------------------------------------------------------

int CTrackingTool::onChangingClip()
{
   DestroyAllToolSetups();
   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//--------------------------------------------------------------------------

int CTrackingTool::onChangingFraming()
{
   DestroyAllToolSetups();
   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//--------------------------------------------------------------------------

int CTrackingTool::onToolProcessingStatus(const CAutotoolStatus& autotoolStatus)
{
   int retVal = TOOL_RETURN_CODE_NO_ACTION;

   if (TrackingInProgress)
   {
      if (autotoolStatus.status == AT_STATUS_STOPPED)
      {
         TrackingInProgress = false;  // processing is done

         int errorCode = 0;
         getSystemAPI()->NotifyTrackingDone(errorCode);
      }

      // Call parent class' function
      CToolObject::onToolProcessingStatus(autotoolStatus);

      retVal = TOOL_RETURN_CODE_EVENT_CONSUMED;
   }

   return retVal;
}

// ===================================================================
//
// Function:    onToolCommand
//
// Description: Tracking Tool's Command Handler
//
// Arguments:   CToolCommand &toolCommand
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================

int CTrackingTool::onToolCommand(CToolCommand &toolCommand)
{
   int commandNumber = toolCommand.getCommandNumber();
   int result = TOOL_RETURN_CODE_NO_ACTION;

   if (TrackingIsEnabled == false || TrkPtsEditor == NULL)
       return result;

   switch (commandNumber)
   {
      case TRK_CMD_ADD_SELECT_OR_DRAG_TRACKPOINT:

         TrkPtsEditor->mouseDown(MOD_KEY_NONE);

         result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
         break;

      case TRK_CMD_SELECT_ANOTHER_TRACKPOINT:

         TrkPtsEditor->mouseDown(MOD_KEY_SHIFT);

         result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
         break;

      case TRK_CMD_STOP_DRAGGING_TRACKPOINT:

         TrkPtsEditor->mouseUp();

         result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
         break;

      case TRK_CMD_BEGIN_PAN:

         TrkPtsEditor->setPanningMode(true);

         // NOT Consumed! need to pass through to NavigatorTool
         break;

      case TRK_CMD_END_PAN:

         TrkPtsEditor->setPanningMode(false);

         // NOT Consumed! need to pass through to NavigatorTool
         break;

      case TRK_CMD_CLEAR_TRACKPOINTS:

         RemoveAllTrackingBoxes();

         result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);
         break;

      case TRK_CMD_DELETE_SELECTED_TRACKPOINTS:

         TrkPtsEditor->deleteSelectedTrackingPoints();

         result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                   TOOL_RETURN_CODE_PROCESSED_OK);

         break;

      case TRK_CMD_TRACK:

			if (!TrackingInProgress)
			{
				StartTracking(true);
			}

// wtf? QQQ
//         // Always consumed
//         result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
//                   TOOL_RETURN_CODE_PROCESSED_OK);

         break;

      case TRK_CMD_STOP:

         if (TrackingInProgress)
         {
            StopTracking();

            result = (TOOL_RETURN_CODE_EVENT_CONSUMED |
                      TOOL_RETURN_CODE_PROCESSED_OK);

            // NOT CONSUMED if not presently tracking
         }
         break;
   }

   if ((result & TOOL_RETURN_CODE_EVENT_CONSUMED) == TOOL_RETURN_CODE_EVENT_CONSUMED)
   {
      const CToolCommandTable *commandTable = getCommandTable();
      const char* commandName = commandTable->findCommandName(commandNumber);
      if (commandName != NULL)
         TRACE_2(errout << "Command: " << commandName);
      else
         TRACE_2(errout << "Tracking swallowed command " << commandNumber);
   }

   return result;
}

// ===================================================================
//
// Function:    onUserInput
//
// Description: Navigator Tool's User Input Handler
//
// Arguments:   CUserInput &userInput
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================
int CTrackingTool::onUserInput(CUserInput &userInput)
{
   // default: user input will be passed back to the tool manager
   int retVal = TOOL_RETURN_CODE_NO_ACTION;

   if (TrackingIsEnabled && (TrkPtsEditor != NULL))
   {
      retVal = CToolObject::onUserInput(userInput);

      // Gaack. We know that left mouse button will try to make or
      // manipulate tracking points, so make sure they're visible!!!
      // (Don't know why this is done here, but that's how it was in the
      // Stabilizer tool)
      if (userInput.action == USER_INPUT_ACTION_MOUSE_BUTTON_DOWN &&
          userInput.key == MTK_LBUTTON &&
          TrackingIsEnabled == true &&
          TrkPtsEditor != NULL)
      {
         TrkPtsEditor->displayTrackingPoints = true;
      }
   }

   return retVal;
}

// ===================================================================
//
// Function:    onMouseMove
//
// Description: Navigator Tool's Mouse Movement Handler
//
// Arguments:   CUserInput &userInput
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================
int CTrackingTool::onMouseMove(CUserInput &userInput)
{
   // default is mouseMove will be passed thru to Navigator tool
   int retVal = TOOL_RETURN_CODE_NO_ACTION;

   // If we're disabled or there's no clip, don't do anything
   if (TrackingIsEnabled && getSystemAPI()->isAClipLoaded()
       && (TrkPtsEditor != NULL))
   {
      TrkPtsEditor->mouseMove(userInput.windowX, userInput.windowY);
   }

   // Always pass through the mouse move so panning will continue to work
   return TOOL_RETURN_CODE_NO_ACTION;
}
//--------------------------------------------------------------------------

bool CTrackingTool::LockTrackingTool()
{
   MTIassert(!advisoryLock);
   if (advisoryLock)
      return false;

   advisoryLock = true;
   return true;
}
//--------------------------------------------------------------------------

void CTrackingTool::ReleaseTrackingTool()
{
   MTIassert(advisoryLock);
   advisoryLock = false;
}
//--------------------------------------------------------------------------

void CTrackingTool::SetTrackingToolEditor(TrackingBoxManager *newEditor)
{
   TrkPtsEditor = newEditor;
}
//--------------------------------------------------------------------------

void CTrackingTool::SetTrackingToolProgressMonitor(IToolProgressMonitor *newToolProgressMonitor)
{
   SetToolProgressMonitor(newToolProgressMonitor);
}
//--------------------------------------------------------------------------

void CTrackingTool::EnableTrackingTool()
{
	TrackingIsEnabled = true;
}
//--------------------------------------------------------------------------

void CTrackingTool::DisableTrackingTool()
{
   DestroyAllToolSetups();
   TrackingIsEnabled = false;
}
//--------------------------------------------------------------------------

int CTrackingTool::StartTracking(bool force)
{
	if (TrackingIsEnabled == false)
	{
		return TOOL_RETURN_CODE_NO_ACTION;
	}

	// Give the processing tool a chance to tell us to not track.
   // Two ways it can do that, disable us or respond to the Notify with 'false'.
	if (TrackingIsEnabled == false || !getSystemAPI()->NotifyTrackingStarting())
	{
		return TOOL_RETURN_CODE_NO_ACTION;
	}

	if (TrkPtsEditor == NULL)
	{
		return TOOL_RETURN_CODE_EVENT_CONSUMED;
	}

	CAutoErrorReporter autoErr("CTrackingTool::StartTracking",  AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

	// First check frame range
	int startFrameIndex = TrkPtsEditor->getRangeBegin();
   int stopFrameIndex = TrkPtsEditor->getRangeEnd();
   if (startFrameIndex < 0 || stopFrameIndex < 0 ||
       stopFrameIndex <= startFrameIndex)
   {
		// Range is bogus, so complain to user and refuse to do anything.
		autoErr.errorCode = -1;
		autoErr.msg << "Cannot track because the frame range is invalid";
	}
	else if (TrkPtsEditor->getNumberOfTrackingBoxes() < 1)
	{
		autoErr.errorCode = -1;
		autoErr.msg << "Cannot track because no tracking points have been added.";
	}
	else if (getSystemAPI()->isAClipLoaded() == false)
   {
      autoErr.errorCode = -1;
		autoErr.msg << "No clip is open";
	}

	if (TrkPtsEditor->wasTrackingDataInvalidatedByRendering())
	{
		TrkPtsEditor->invalidateAllTrackingData();
	}

	if (isAllTrackDataValid() && force)
	{
		TrkPtsEditor->invalidateAllTrackingData();
	}

	// Skip tracking phase if there was an error or if tracking data is valid.
	if (isAllTrackDataValid() || autoErr.errorCode != 0)
	{
		getSystemAPI()->NotifyTrackingDone(autoErr.errorCode);
		return TOOL_RETURN_CODE_EVENT_CONSUMED;
	}

	// Need to go through tool infrastructure so STOP command will work!
	StartToolProcessing(TOOL_PROCESSING_CMD_PREVIEW, false);

	return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//--------------------------------------------------------------------------

void CTrackingTool::StopTracking()
{
	if (TrackingInProgress)
	{
		StopToolProcessing();
	}
}
//--------------------------------------------------------------------------

void CTrackingTool::RemoveAllTrackingBoxes()
{
	if (!TrackingIsEnabled || TrkPtsEditor == NULL)
	{
		return;
	}

  TrkPtsEditor->removeAllTrackingBoxes();

  getSystemAPI()->NotifyTrackingCleared();
}
//--------------------------------------------------------------------------

bool CTrackingTool::isAllTrackDataValid()
{
   if (TrkPtsEditor == NULL
   || TrkPtsEditor->getTrackingArray()->getNumberOfTrackingBoxes() == 0)
   {
      return true;
   }

   return TrkPtsEditor->isTrackingDataValidOverEntireRange();
}
//--------------------------------------------------------------------------

void CTrackingTool::DestroyAllToolSetups()
{
   // Tracking has two tool setups: singleFrame and multiFrame
   // This function should not be called unless all of the tool setups
   // are stopped and there is no provisional pending.

   int activeToolSetup = getSystemAPI()->GetActiveToolSetupHandle();

   if (trackingToolSetupHandle >= 0)
   {
      if (trackingToolSetupHandle == activeToolSetup)
         getSystemAPI()->SetActiveToolSetup(-1);
      getSystemAPI()->DestroyToolSetup(trackingToolSetupHandle);
      trackingToolSetupHandle = -1;
   }
}
//--------------------------------------------------------------------------

int CTrackingTool::RunFrameProcessing(int newResumeFrame)
{
   int retVal;
   CAutoErrorReporter autoErr("CTrackingTool::RunFramePreprocessing",
                              AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
   string errorFunc;

   // Check if we have a tool setup yet
   if (trackingToolSetupHandle < 0)
   {
      // Create the tool setup
      CToolIOConfig ioConfig(1, 1);     // 1 in port, 1 out port
      trackingToolSetupHandle =
                             getSystemAPI()->MakeSimpleToolSetup(GetToolName(),
                                                   TOOL_SETUP_TYPE_MULTI_FRAME,
                                                   &ioConfig);
      if (trackingToolSetupHandle < 0)
      {
         autoErr.errorCode = -1;
         autoErr.msg << "Tracking internal error: "
                        "MakeSimpleToolSetup failed";
         return -1;
      }
   }

   // Set the active tool setup (this is different than the active tool)
   retVal = getSystemAPI()->SetActiveToolSetup(trackingToolSetupHandle);
   if (retVal != 0)
   {
      autoErr.errorCode = retVal;
      autoErr.msg << "Tracking internal error: SetActiveToolSetup("
                     << trackingToolSetupHandle
                     << ") failed with return code " << retVal;
      return retVal;
   }

   // Huh - why the heck wouldn't it be stopped??
   if (getSystemAPI()->GetActiveToolSetupStatus() == AT_STATUS_STOPPED)
   {
      // Set processing frame range from the Editor
      CToolFrameRange frameRange(1, 1);  // always looking at only one frame
      frameRange.inFrameRange[0].randomAccess = false;
      frameRange.inFrameRange[0].inFrameIndex = TrkPtsEditor->getTrackingArray()->getFirstUntrackedFrame() - 1;
      frameRange.inFrameRange[0].outFrameIndex = TrkPtsEditor->getRangeEnd();
      retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
      if (retVal != 0)
      {
         autoErr.errorCode = retVal;
         autoErr.msg << "Tracking internal error: SetToolFrameRange"
                     << " failed with return code " << retVal;
         return retVal;
      }
   }

   // Set render/preview flag for tool infrastructure.
   getSystemAPI()->SetProvisionalRender(false);  // No render during tracking

   // Set parameters
   CTrackingToolParameters *toolParams = new CTrackingToolParameters;
   GatherTrackingToolParameters(*toolParams);
   retVal = getSystemAPI()->SetToolParameters(toolParams);
   if (retVal != 0)
   {
      autoErr.errorCode = retVal;
      autoErr.msg << "Tracking internal error: SetToolParameters"
                  << " failed with return code " << retVal;
      return retVal;
   }

   // Start the image processing a-runnin'
	TrackingInProgress = true;
   getSystemAPI()->RunActiveToolSetup();

   return 0;

} // RunFrameProcessing
//--------------------------------------------------------------------------

int CTrackingTool::GatherTrackingToolParameters(CTrackingToolParameters &toolParams)
{
   STrackingParameters &trackingParameters = toolParams.trackingParameters;

   trackingParameters.TrkPtsEditor = TrkPtsEditor;
   trackingParameters.TrackingToolObject = this;
   getSystemAPI()->GetDisplayLutValues(trackingParameters.lut8);

   return 0;
}
//---------------------------------------------------------------------------

int CTrackingTool::onPreviewHackDraw(int frameIndex, MTI_UINT16 *frameBits, int frameBitsSizeInBytes)
{
//   //DEBUGGING
#ifdef DEBUG_VISUALIZATION
   const CImageFormat *imageFormat = getSystemAPI()->getVideoClipImageFormat();
   int width = imageFormat->getPixelsPerLine();
   int height = imageFormat->getLinesPerFrame();
   int depth = imageFormat->getBitsPerComponent();

   if (!_debugVisualizationArray.isEmpty()
   && _debugVisualizationArray.getCols() == width
   && _debugVisualizationArray.getRows() == height)
   {
      CAutoSpinLocker lock(_debugVisualizationLock);

      MTI_UINT16 *pOut = frameBits;
      int shift = depth - 8;
      for (auto iter = _debugVisualizationArray.begin(); iter != _debugVisualizationArray.end(); ++iter)
      {
         MTI_UINT16 monoVal = *iter;
//         if (monoVal != 128)
         {
            monoVal <<= shift;
            pOut[0] = pOut[1] = pOut[2] = monoVal;
         }

         pOut += 3;
      }
   }
#endif

   return TOOL_RETURN_CODE_NO_ACTION;
}
//---------------------------------------------------------------------------
