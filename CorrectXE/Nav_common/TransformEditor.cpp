
#include <math.h>
#include "TransformEditor.h"
#include "Displayer.h"
#include "PixelRegions.h"
#include "ToolPaintInterface.h"
#define PI 3.1415927

#define MINEDG 5.
#define USE_QUEUE

//------------------------------------------------------------------------------
// Bugzilla ref #2032 - KT-050112
// As currently implemented, the xform is refreshed by
// displayFrames' ToolManager call to onRedraw, which calls
// MaskTool's DrawMask() and gets the xform-in-progress from
// the regionList (which never contains more than 1 region).
// Because onRedraw is now called from inside displayFrame,
// the spline is drawn into the BACK GL buffer and made vis-
// ible by SwapBuffers. The process of drawing the spline
// is no longer perceptible. In addition, there appears to
// be a great speed advantage drawing to the BACK GL buffer,
// esp. in dual monitor systems.

CTransformEditor::CTransformEditor(CDisplayer * dsp)
{
   // save the address of the displayer
   displayer = dsp;

   // no tool paint interface yet
   toolPaintInterface = NULL;

   // give Displayer ptr to xform editor
   displayer->initTransformEditor(this, rgbRect);

   // reset transform
   resetTransform();

   // calculate surrogate frame rectangle
   initTransformWidget();

   // initialize the modkeys
   modKeys = 0;

   majorState = MAJORSTATE_TRACK;

   // initialize these in case we do a restore without having previously saved!
   savedRotationAngle = 0.;
   savedStretchX = 1.;
   savedStretchY = 1.;
   savedSkew = 0.;
   savedOffsetX = 0.;
   savedOffsetY = 0.;
}

CTransformEditor::~CTransformEditor()
{
}

// PaintTool.cpp gives the TransformEditor a little interface
// so the latter can do things like adjust the trackbars etc.
//
void CTransformEditor::setToolPaintInterface(CToolPaintInterface *ptintfc)
{
   toolPaintInterface = ptintfc;
}

// The TransformEditor needs to know the dimensions of the
// image in the client area, in order to set up the Widget
//
void CTransformEditor::setImageRectangle(RECT& rgbrct)
{
   rgbRect = rgbrct;

   calculateWidgetSize();

   calculateTransformWidget();
}

void CTransformEditor::calculateWidgetSize()
{
   rgbWdth = (rgbRect.right - rgbRect.left + 1);
   rgbHght = (rgbRect.bottom - rgbRect.top + 1);
   rgbXctr = rgbRect.left + rgbWdth/2;
   rgbYctr = rgbRect.top  + rgbHght/2;

   wdgtSize = rgbWdth/2;
   if (rgbHght < rgbWdth)
      wdgtSize = rgbHght/2;
}

void CTransformEditor::calculateTransformWidget()
{
   transformStretchedToRotated(rotWidget, strWidget, netRotationAngle);
}

void CTransformEditor::resetTransform()
{
   // the unit square centered on (0, 0)
   skwWidget[0].x =  -.5;
   skwWidget[0].y =  -.5;

   skwWidget[2].x =   .5;
   skwWidget[2].y =  -.5;

   skwWidget[4].x =   .5;
   skwWidget[4].y =   .5;

   skwWidget[6].x =  -.5;
   skwWidget[6].y =   .5;

   skwWidget[8].x =  -.5;
   skwWidget[8].y =  -.5;

   netRotationAngle = 0.0;
   netStretchX      = 1.0;
   netStretchY      = 1.0;
   netSkew          = 0.0;

   // now bring in the stretch factors
   transformSkewedToStretched(strWidget, skwWidget, netStretchX, netStretchY);

   // now bring in the size of the client area
   transformStretchedToRotated(rotWidget, strWidget, netRotationAngle);

   // clear the queue
   rotFill = rotEmpty = 0;

   // reset the transform variables in the Displayer
   displayer->clearTransform();

   // get these from the Displayer
   displayer->getImportOffsets(&netOffsetX, &netOffsetY);

   // draw the import frame, if any
   displayer->setImportRotationExact(netRotationAngle,
                                     netStretchX, netStretchY,
                                     netSkew,
                                     netOffsetX, netOffsetY);

   if (toolPaintInterface != NULL) {

      toolPaintInterface->setTransform(netRotationAngle,
                                       netStretchX, netStretchY,
                                       netSkew,
                                       netOffsetX, netOffsetY);
   }
}

void CTransformEditor::initTransformWidget()
{
   calculateWidgetSize();

   calculateTransformWidget();
}

void CTransformEditor::moveImportFrame(int delx, int dely)
{
   displayer->moveImportFrame(delx, dely);

   displayer->getImportOffsets(&netOffsetX, &netOffsetY);

   if (toolPaintInterface != NULL) {

      toolPaintInterface->setTransform(netRotationAngle,
                                       netStretchX, netStretchY,
                                       netSkew,
                                       netOffsetX, netOffsetY);
   }
}

void CTransformEditor::saveTransform()
{
   displayer->getTransform(&savedRotationAngle, &savedStretchX, &savedStretchY,
                           &savedSkew, &savedOffsetX, &savedOffsetY);
}

void CTransformEditor::restoreTransform()
{
   setTransform(savedRotationAngle, savedStretchX, savedStretchY,
                savedSkew, savedOffsetX, savedOffsetY);
}

void CTransformEditor::setTransform(double rot, double strx, double stry,
                                    double skew, double offx, double offy)
{
   // -PI > rot <= PI
   while (rot >=  2*PI) rot -= 2*PI;
   while (rot <= -2*PI) rot += 2*PI;
   if (rot > PI) rot -= 2*PI;
   netRotationAngle = rot;

   // limits on stretching
   if (strx < -2.0) strx = -2.0;
   if ((strx > -.0625)&&(strx < 0)) strx = -.0625;
   if ((strx <  .0625)&&(strx > 0)) strx =  .0625;
   if (strx >  2.0) strx =  2.0;
   if (stry < -2.0) stry = -2.0;
   if ((stry > -.0625)&&(stry < 0)) stry = -.0625;
   if ((stry <  .0625)&&(stry > 0)) stry =  .0625;
   if (stry >  2.0) stry =  2.0;
   netStretchX      = strx;
   netStretchY      = stry;

   // and on skew
   if (skew < -4.0) skew = -4.0;
   if (skew >  4.0) skew =  4.0;
   netSkew          = skew;

   // offsets must be rounded to the nearest 1/5 pel
   //
   // removed to allow reversible inversion (i.e.,
   // hitting invert button twice restores original)
   //
   //netOffsetX = (double)((int)(offx*5.))/5.;
   //netOffsetY = (double)((int)(offy*5.))/5.;

   netOffsetX = offx;
   netOffsetY = offy;

   // skewed unit square centered on (0, 0)
   skwWidget[0].x =  -.5 - .5*netSkew;
   skwWidget[0].y =  -.5;

   skwWidget[2].x =   .5 - .5*netSkew;
   skwWidget[2].y =  -.5;

   skwWidget[4].x =   .5 + .5*netSkew;
   skwWidget[4].y =   .5;

   skwWidget[6].x =  -.5 + .5*netSkew;
   skwWidget[6].y =   .5;

   skwWidget[8].x =  -.5 - .5*netSkew;
   skwWidget[8].y =  -.5;

   // now bring in the stretch factors
   transformSkewedToStretched(strWidget, skwWidget, netStretchX, netStretchY);

   // now bring in the size of the client area
   transformStretchedToRotated(rotWidget, strWidget, netRotationAngle);

   displayer->setTransform(netRotationAngle, netStretchX, netStretchY,
                           netSkew, netOffsetX, netOffsetY);

   // get these from the Displayer
   displayer->getImportOffsets(&netOffsetX, &netOffsetY);

   if (toolPaintInterface != NULL) {

      toolPaintInterface->setTransform(netRotationAngle,
                                       netStretchX, netStretchY,
                                       netSkew,
                                       netOffsetX, netOffsetY);
   }
}

void CTransformEditor::setRotation(double rot)
{
   netRotationAngle = rot;

   transformSkewedToStretched(strWidget, skwWidget, netStretchX, netStretchY);

   transformStretchedToRotated(rotWidget, strWidget, netRotationAngle);

   displayer->setImportRotationExact(netRotationAngle,
                                     netStretchX, netStretchY,
                                     netSkew,
                                     netOffsetX, netOffsetY);

   if (toolPaintInterface != NULL) {

      displayer->getImportOffsets(&netOffsetX, &netOffsetY);
      toolPaintInterface->setTransform(netRotationAngle,
                                       netStretchX, netStretchY,
                                       netSkew,
                                       netOffsetX, netOffsetY);
   }
}

void CTransformEditor::setSkew(double skew)
{
   // limits on skew
   if (skew < -4.0) skew = -4.0;
   if (skew >  4.0) skew =  4.0;
   netSkew          = skew;

   // skewed unit square centered on (0, 0)
   skwWidget[0].x =  -.5 - .5*netSkew;
   skwWidget[0].y =  -.5;

   skwWidget[2].x =   .5 - .5*netSkew;
   skwWidget[2].y =  -.5;

   skwWidget[4].x =   .5 + .5*netSkew;
   skwWidget[4].y =   .5;

   skwWidget[6].x =  -.5 + .5*netSkew;
   skwWidget[6].y =   .5;

   skwWidget[8].x =  -.5 - .5*netSkew;
   skwWidget[8].y =  -.5;

   // now bring in the stretch factors
   transformSkewedToStretched(strWidget, skwWidget, netStretchX, netStretchY);

   // now bring in the size of the client area
   transformStretchedToRotated(rotWidget, strWidget, netRotationAngle);

   displayer->setImportRotationExact(netRotationAngle,
                                     netStretchX, netStretchY,
                                     netSkew,
                                     netOffsetX, netOffsetY);

   if (toolPaintInterface != NULL) {

      displayer->getImportOffsets(&netOffsetX, &netOffsetY);
      toolPaintInterface->setTransform(netRotationAngle,
                                       netStretchX, netStretchY,
                                       netSkew,
                                       netOffsetX, netOffsetY);
   }
}

void CTransformEditor::setStretchX(double strx)
{
   // limits on stretching
   if (strx < -2.0) strx = -2.0;
   if ((strx > -.0625)&&(strx < 0)) strx = -.0625;
   if ((strx <  .0625)&&(strx > 0)) strx =  .0625;
   if (strx >  2.0) strx =  2.0;

   netStretchX = strx;

   // now bring in the stretch factors
   transformSkewedToStretched(strWidget, skwWidget, netStretchX, netStretchY);

   // now bring in the size of the client area
   transformStretchedToRotated(rotWidget, strWidget, netRotationAngle);

   displayer->setImportRotationExact(netRotationAngle,
                                     netStretchX, netStretchY,
                                     netSkew,
                                     netOffsetX, netOffsetY);

   if (toolPaintInterface != NULL) {

      displayer->getImportOffsets(&netOffsetX, &netOffsetY);
      toolPaintInterface->setTransform(netRotationAngle,
                                       netStretchX, netStretchY,
                                       netSkew,
                                       netOffsetX, netOffsetY);
   }
}

void CTransformEditor::setStretchY(double stry)
{
   if (stry < -2.0) stry = -2.0;
   if ((stry > -.0625)&&(stry < 0)) stry = -.0625;
   if ((stry <  .0625)&&(stry > 0)) stry =  .0625;
   if (stry >  2.0) stry =  2.0;

   netStretchY      = stry;

   // now bring in the stretch factors
   transformSkewedToStretched(strWidget, skwWidget, netStretchX, netStretchY);

   // now bring in the size of the client area
   transformStretchedToRotated(rotWidget, strWidget, netRotationAngle);

   displayer->setImportRotationExact(netRotationAngle,
                                     netStretchX, netStretchY,
                                     netSkew,
                                     netOffsetX, netOffsetY);

   if (toolPaintInterface != NULL) {

      displayer->getImportOffsets(&netOffsetX, &netOffsetY);
      toolPaintInterface->setTransform(netRotationAngle,
                                       netStretchX, netStretchY,
                                       netSkew,
                                       netOffsetX, netOffsetY);
   }
}

void CTransformEditor::setOffsetX(double offx)
{
   displayer->getImportOffsets(&netOffsetX, &netOffsetY);

   netOffsetX = offx;

   displayer->setImportOffsets(netOffsetX, netOffsetY);

   if (toolPaintInterface != NULL) {

      toolPaintInterface->setTransform(netRotationAngle,
                                       netStretchX, netStretchY,
                                       netSkew,
                                       netOffsetX, netOffsetY);
   }
}

void CTransformEditor::setOffsetY(double offy)
{
   displayer->getImportOffsets(&netOffsetX, &netOffsetY);

   netOffsetY = offy;

   displayer->setImportOffsets(netOffsetX, netOffsetY);

   if (toolPaintInterface != NULL) {

      toolPaintInterface->setTransform(netRotationAngle,
                                       netStretchX, netStretchY,
                                       netSkew,
                                       netOffsetX, netOffsetY);
   }
}

void CTransformEditor::setOffsetXY(double offx, double offy)
{
   displayer->getImportOffsets(&netOffsetX, &netOffsetY);

   netOffsetX = offx;
   netOffsetY = offy;

   displayer->setImportOffsets(netOffsetX, netOffsetY);

   if (toolPaintInterface != NULL) {

      toolPaintInterface->setTransform(netRotationAngle,
                                       netStretchX, netStretchY,
                                       netSkew,
                                       netOffsetX, netOffsetY);
   }
}

void CTransformEditor::getTransform(double *rotp, double *strxp, double *stryp,
                                    double *skewp, double *offxp, double *offyp)
{
   displayer->getTransform(rotp, strxp, stryp, skewp, offxp, offyp);
}

void CTransformEditor::setImportFrameOffset(int impoff)
{
   displayer->setImportFrameOffset(impoff);

   // In case we are in track align mode
   double rot, strx, stry, skew, offx, offy;
   getTransform(&rot, &strx, &stry, &skew, &offx, &offy);

   toolPaintInterface->setTransform(rot, strx, stry, skew, offx, offy);
}

void CTransformEditor::setImportFrameTimecode(int imptim)
{
   displayer->setImportFrameTimecode(imptim);

   // In case we are in track align mode
   double rot, strx, stry, skew, offx, offy;
   getTransform(&rot, &strx, &stry, &skew, &offx, &offy);

   toolPaintInterface->setTransform(rot, strx, stry, skew, offx, offy);
}

void CTransformEditor::transmitProxy(unsigned int *img)
{
   if (toolPaintInterface != NULL)
      toolPaintInterface->drawProxy(img);
}

void CTransformEditor::transmitImportFrameMode(int impmod)
{
   if (toolPaintInterface != NULL)
      toolPaintInterface->setImportFrameMode(impmod);
}

void CTransformEditor::transmitImportFrameOffset(int frmoff)
{
   if (toolPaintInterface != NULL)
      toolPaintInterface->setImportFrameOffset(frmoff);
}

void CTransformEditor::transmitImportFrameTimecode(int impfrm)
{
   if (toolPaintInterface != NULL)
      toolPaintInterface->setImportFrameTimecode(impfrm);
}

void CTransformEditor::transmitTargetFrameTimecode(int trgfrm)
{
   if (toolPaintInterface != NULL)
      toolPaintInterface->setTargetFrameTimecode(trgfrm);
}

void CTransformEditor::setFrameLoadTimer()
{
   if (toolPaintInterface != NULL)
      toolPaintInterface->setFrameLoadTimer();
}

void CTransformEditor::transmitOnionSkinOffsets(int offx, int offy)
{
   netOffsetX = offx;
   netOffsetY = offy;
   if (toolPaintInterface != NULL) {

#if 0 // I hate this business with multiple copies of things
      toolPaintInterface->setTransform(netRotationAngle,
                                       netStretchX, netStretchY,
                                       netSkew,
                                       netOffsetX, netOffsetY);
#else
      double rot, strx, stry, skew, junk;
      getTransform(&rot, &strx, &stry, &skew, &junk, &junk);

      toolPaintInterface->setTransform(rot, strx, stry, skew, offx, offy);
#endif
   }
}

void CTransformEditor::transmitPickedColor(float *colrcomp)
{
   if (toolPaintInterface != NULL)
      toolPaintInterface->setPickedColor(colrcomp);
}

void CTransformEditor::onLoadNewTargetFrame()
{
   if (toolPaintInterface != NULL)
      toolPaintInterface->onLoadNewTargetFrame();
}

void CTransformEditor::clearStrokeStack()
{
   if (toolPaintInterface != NULL)
      toolPaintInterface->clearStrokeStack();
}

void CTransformEditor::transmitStrokeEntry(int ordnum, int brmod)
{
   if (toolPaintInterface != NULL)
      toolPaintInterface->setStrokeEntry(ordnum, brmod);
}

void CTransformEditor::transmitTransform(double rot,  double strx, double stry,
                                         double skew, double offx, double offy)
{
   toolPaintInterface->setTransform(rot, strx, stry, skew, offx, offy);
}

// Draw the list of spline vertices into RGBRECT, using
// the machinery in the DISPLAYER. This takes care of all
// the clipping and scaling.
//
void CTransformEditor::drawTransform()
{
   if (displayer->isOnTargetFrame()) { // do nothing if off target frame

      // draw the WIDGET rectangle
      displayer->setGraphicsColor(SOLID_YELLOW);

      // draw the transform outline
      displayer->moveToClient       (rotWidget[0].x, rotWidget[0].y);
      displayer->lineToClientNoFlush(rotWidget[2].x, rotWidget[2].y);
      displayer->lineToClientNoFlush(rotWidget[4].x, rotWidget[4].y);
      displayer->lineToClientNoFlush(rotWidget[6].x, rotWidget[6].y);
      displayer->lineToClientNoFlush(rotWidget[0].x, rotWidget[0].y);

      // draw the little grab boxes
      displayer->setGraphicsColor(SOLID_YELLOW);
      for (int i=0;i<8;i++) {
         displayer->drawUnfilledBoxClientNoFlush(rotWidget[i].x, rotWidget[i].y);
      }

      // draw a box in the center
      displayer->drawUnfilledBoxClientNoFlush(rgbXctr, rgbYctr);

      // flush the graphics
      displayer->flushGraphics();
   }
}

void CTransformEditor::loadTransformQueue(double angle,
                                          double strx, double stry,
                                          double skew,
                                          double offx, double offy)
{
   if ((rotFill - rotEmpty) < MAX_TRANSFORMS) {

      transformQueue[rotFill%MAX_TRANSFORMS].tr_angle    = angle;
      transformQueue[rotFill%MAX_TRANSFORMS].tr_stretchX = strx;
      transformQueue[rotFill%MAX_TRANSFORMS].tr_stretchY = stry;
      transformQueue[rotFill%MAX_TRANSFORMS].tr_skew     = skew;
      transformQueue[rotFill%MAX_TRANSFORMS].tr_offsx    = offx;
      transformQueue[rotFill%MAX_TRANSFORMS].tr_offsy    = offy;

      rotFill++;
   }
}

   // receives timer from Paint Tool GUI, unloads transform queue
void CTransformEditor::timeTransform()
{
   if (rotEmpty < rotFill) {

      // empty one-fourth the remaining queue per cycle
      rotEmpty = rotEmpty + (rotFill - rotEmpty)/4;

      double rot  = transformQueue[rotEmpty%MAX_TRANSFORMS].tr_angle;
      double skew = transformQueue[rotEmpty%MAX_TRANSFORMS].tr_skew;
      double strx = transformQueue[rotEmpty%MAX_TRANSFORMS].tr_stretchX;
      double stry = transformQueue[rotEmpty%MAX_TRANSFORMS].tr_stretchY;
      double offx = transformQueue[rotEmpty%MAX_TRANSFORMS].tr_offsx;
      double offy = transformQueue[rotEmpty%MAX_TRANSFORMS].tr_offsy;

      if (rotEmpty == (rotFill - 1)) {

         displayer->setImportRotationExact(rot, strx, stry, skew, offx, offy);

      }
      else {

         displayer->setImportRotationFast (rot, strx, stry, skew, offx, offy);
      }

      // the Transform parameters can be updated after the fast
      // transform or alternatively only after the slow one

      if (toolPaintInterface != NULL) {

         displayer->getImportOffsets(&netOffsetX, &netOffsetY);
         toolPaintInterface->setTransform(rot,
                                          strx, stry,
                                          skew,
                                          netOffsetX, netOffsetY);
      }

      rotEmpty++;

      refreshTransform();
   }
}

// refresh the transform-in-progress by drawing
// the image, then the regionList (containing
// the spline), into the back GL buffer. Then
// make everything visible by swapping buffers
// The chain of calls is displayFrame ->
// onRedraw -> DrawMask -> drawSplineFrame ->
// SwapBuffers
void CTransformEditor::refreshTransform()
{
   displayer->displayFrame();
}
//------------------------------------------------------------------------------
//
// returns the quadrant number of a pt (x, y) relative to the origin.

int CTransformEditor::getQuadrant(int x, int y)
{
  if (x < 0) {
     if (y < 0) {
        return 1;
     }
     else {
        return 2;
     }
  }
  else if (x == 0) {
     if (y < 0) {
        return 1;
     }
     else if (y == 0) {
        return -1;
     }
     else {
        return 3;
     }
  }
  else if (x > 0) {
     if (y <= 0) {
        return 0;
     }
     else {
        return 3;
     }
  }

  // Can't get here!
  MTIassert(false);
  return -1;
}

// note that negative y points upward, hence the formula here
//
int CTransformEditor::crossProduct(int x0, int y0, int x1, int y1)
{
   return(x1*y0 - x0*y1);
}

// determines whether a pt (x, y) is inside or outide the
// (rotated) transform widget. Works by considering the widget
// polygon rel to (x, y), and calculating the quadrant-deltas
// for each edge. If this is non-zero, then (x, y) is inside
// the widget; if zero, then outside
//
bool CTransformEditor::isOutsideTransformOutline(int x, int y)
{
   int totalQuadrantDeltas = 0;

   int qcur, qnxt, qdel, qcrs;

   // prime the pipeline
   qcur = getQuadrant(rotWidget[0].x - x, rotWidget[0].y - y);

   for (int i=2;i<=8;i+=2) { // skipping midpoints (odd i)

      qnxt = getQuadrant(rotWidget[i].x - x, rotWidget[i].y - y);

      qdel = (qnxt - qcur);

      if      (qdel == -3)
         qdel =  1;
      else if (qdel ==  3)
         qdel = -1;
      else if ((qdel==-2)||(qdel==2)) {

         qcrs = crossProduct(rotWidget[i-2].x - x, rotWidget[i-2].y - y,
                             rotWidget[i].x   - x, rotWidget[i].y   - y);

         if (qcrs == 0) return false; // on the widget outline
         if (qcrs < 0) qdel = -2;
         else qdel = 2;
      }

      totalQuadrantDeltas += qdel;

      qcur = qnxt;
   }

   if (totalQuadrantDeltas != 0) return false;

   return true;
}

int CTransformEditor::getWidgetBox(RECT& box)
{

   for (int i=0;i<8;i++) {

      if ((rotWidget[i].x >= box.left)&&(rotWidget[i].x <= box.right )&&
          (rotWidget[i].y >= box.top )&&(rotWidget[i].y <= box.bottom))

         return i;
   }

   return -1;
}

double CTransformEditor::getAngle(double cosine, double sine)
{
   switch(getQuadrant(100000*cosine, 100000*sine)) {

	  case 0:

		  sine = -sine;
		  if (sine < cosine)
			 return(asin(sine));
		  else
			 return(acos(cosine));

	  break;

	  case 1:

		  sine = -sine;
		  cosine = -cosine;
		  if (sine < cosine)
			 return(PI - asin(sine));
		  else
			 return(PI - acos(cosine));

	  break;

	  case 2:

		  cosine = -cosine;
		  if (sine < cosine)
			 return(PI + asin(sine));
		  else
			 return(PI + acos(cosine));

	  break;

	  case 3:

		  if (sine < cosine)
			 return(2*PI - asin(sine));
		  else
			 return(2*PI - acos(cosine));

	  break;

	  default:
	    return 0;
   }

   return 0;
}

double CTransformEditor::getAngleFromClientCoords(int x, int y)
{
   double fx = x - rgbXctr;
   double fy = y - rgbYctr;

   double leng = sqrt( fx*fx + fy*fy );
   if (leng == 0)
      return -1.0;

   fx /= leng;
   fy /= leng;

   return getAngle( fx, fy );
}

void CTransformEditor::calculateMidpoints(DPOINT *src)
{
   src[8]   = src[0];

   for (int i=1;i<8;i+=2) {
      src[i].x = (src[i-1].x + src[i+1].x)/2;
      src[i].y = (src[i-1].y + src[i+1].y)/2;
   }
}

bool CTransformEditor::isTooSmall(DPOINT *src, int inipt, int finpt)
{
   double delx = src[inipt].x - src[finpt].x;
   if (delx < 0) delx = -delx;
   double dely = src[inipt].y - src[finpt].y;
   if (dely < 0) dely = -dely;

   return ((delx < MINEDG)&&(dely < MINEDG));
}

bool CTransformEditor::isTooSmall(DPOINT *src)
{
   double ux = src[2].x - src[0].x;
   double uy = src[2].y - src[0].y;

   double vx = src[6].x - src[0].x;
   double vy = src[6].y - src[0].y;

   double dotprod = ux*vy - uy*vx;

   return (fabs(dotprod) < MINEDG*MINEDG);
}

void CTransformEditor::transformRotatedToStretched(DPOINT *dst, DPOINT *src, double *angle)
{
   // get initial unit vector
   double cosine =  (src[2].x - src[0].x);
   double sine   =  (src[2].y - src[0].y);

   double leng   = sqrt(cosine*cosine + sine*sine);
   if (leng == 0) {

      cosine = 1.00;
      sine   = 0.00;
   }
   else {

      cosine /= leng;
      sine   /= leng;
   }

   // corners
   for (int i=0;i<10;i+=2) {

      dst[i].x = (cosine*(src[i].x - rgbXctr) +
                 sine  *(src[i].y - rgbYctr)) / wdgtSize ;

      dst[i].y = (-sine *(src[i].x - rgbXctr) +
                 cosine*(src[i].y - rgbYctr)) / wdgtSize ;
   }

   calculateMidpoints(dst);

   *angle = getAngle(cosine, sine);
}

void CTransformEditor::transformStretchedToSkewed(DPOINT *dst, DPOINT *src,
                                                  double *xstr, double *ystr,
                                                  double *skew)
{
   // here we pull out the stretch factors
   *xstr = src[2].x - src[0].x;
   *ystr = src[6].y - src[0].y;

   for (int i=0;i<=8;i+=2) {

      dst[i].x = src[i].x / (*xstr);
      dst[i].y = src[i].y / (*ystr);
   }

   calculateMidpoints(dst);

   // here we pull out the "skew"
   *skew = dst[6].x - dst[0].x;
}

void CTransformEditor::acceptTentativeWidget()
{
   // copy the tentative widget to the rotated widget
   for (int i = 0; i < 8; i+=2)
      rotWidget[i] = tntWidget[i];

   // get the midpoints for the new widget edges
   calculateMidpoints(rotWidget);

   // take the widget back to skewed (initial edge horizontal)
   transformRotatedToStretched(strWidget, rotWidget, &netRotationAngle);

   // take the widget back to skewed
   transformStretchedToSkewed(skwWidget, strWidget,
                              &netStretchX, &netStretchY,
                              &netSkew);

   // calculate the net stretch
   // seems to not be used anywhere
   //netStretchFactor = sqrt(abs(netStretchX*netStretchY));

   // apply the skew, scale, rotate operator to the import frame
#ifdef USE_QUEUE
   loadTransformQueue(netRotationAngle,
                     netStretchX, netStretchY,
                     netSkew,
                     netOffsetX, netOffsetY);
#else
   displayer->setImportRotationExact(netRotationAngle,
                                     netStretchX, netStretchY,
                                     netSkew);
   displayer->displayFrame();

   if (toolPaintInterface != NULL) {

      displayer->getImportOffsets(&netOffsetX, &netOffsetY);
      toolPaintInterface->setTransform(netRotationAngle,
                                       netStretchX, netStretchY,
                                       netSkew,
                                       netOffsetX, netOffsetY);
   }
#endif
}

void CTransformEditor::transformSkewedToStretched(DPOINT *dst, DPOINT *src,
                                                  double xstr, double ystr)
{
   for (int i=0;i<=8;i+=2) {

      dst[i].x = src[i].x * xstr;
      dst[i].y = src[i].y * ystr;
   }

   calculateMidpoints(dst);
}

void CTransformEditor::transformStretchedToRotated(DPOINT *dst, DPOINT *src, double angle)
{
   // get initial unit vector
   double cosine = cos(angle);
   double sine   = sin(angle);

   // corners
   for (int i=0;i<10;i+=2) {

      dst[i].x = (cosine*src[i].x + sine  *src[i].y)*wdgtSize + rgbXctr;

      dst[i].y = (-sine *src[i].x + cosine*src[i].y)*wdgtSize + rgbYctr;
   }

   calculateMidpoints(dst);
}

//------------------------------------------------------------------------------

void CTransformEditor::setShift(bool keydown)
{
   if (keydown)
      modKeys |= MOD_KEY_SHIFT;
   else
      modKeys &= ~MOD_KEY_SHIFT;
}

void CTransformEditor::setAlt(bool keydown)
{
   if (keydown)
      modKeys |= MOD_KEY_ALT;
   else
      modKeys &= ~MOD_KEY_ALT;
}

void CTransformEditor::setCtrl(bool keydown)
{
   if (keydown)
      modKeys |= MOD_KEY_CTRL;
   else
      modKeys &= ~MOD_KEY_CTRL;
}

void CTransformEditor::setKeyA(bool keydown)
{
   if (keydown&&(modKeys&MOD_KEY_CTRL)) {
//      selectAllVertices(splineHead);
      refreshTransform();
   }
}

void CTransformEditor::setKeyD(bool keydown)
{
   if (keydown&&(modKeys&MOD_KEY_CTRL)) {
//      deleteAllSelectedVertices(splineHead);
      refreshTransform();
   }
}

////////////////////////////////////////////////////////////////////////////////
//
// Mouse handlers
//
////////////////////////////////////////////////////////////////////////////////

void CTransformEditor::positionImportFrame()
{
   // do nothing if off target frame
   if (!displayer->isOnTargetFrame()) return;

   // init for dragging import frame

   newOffsetX = netOffsetX;
   newOffsetY = netOffsetY;

   majorState = MAJORSTATE_OFFSET_IMPORT;
}

void CTransformEditor::mouseDown()
{
   // do nothing if off target frame
   if (!displayer->isOnTargetFrame()) return;

   // The current mouse client (x, y) is the
   // last one recorded by mouseMove.
   RECT box;
   box.left   = mouseXClient - BOX_SIZE;
   box.top    = mouseYClient - BOX_SIZE;
   box.right  = mouseXClient + BOX_SIZE;
   box.bottom = mouseYClient + BOX_SIZE;

   switch(majorState) {

      case MAJORSTATE_TRACK:

         // if box is grabbed
         //    if no modkey
         //       majorState = KEEP_ANGLES
         //    elseif modkey = SHIFT
         //       majorState = SCALE_IMPORT
         //    elseif modkey = CTRL
         //       majorState = SKEW_IMPORT
         // elseif outside widget
         //    majorstate = ROTATE_IMPORT
         // elseif inside widget
         //    majorstate = OFFSET_IMPORT

         if ((widgetBox = getWidgetBox(box))!= -1) {

            if (modKeys == MOD_KEY_NONE) {

               majorState = MAJORSTATE_KEEP_ANGLES;
            }
            else if (modKeys == MOD_KEY_SHIFT) {

               majorState = MAJORSTATE_SCALE_IMPORT;
            }
            else if (modKeys == MOD_KEY_CTRL) {

               majorState = MAJORSTATE_SKEW_IMPORT;
            }
         }
         else if (isOutsideTransformOutline(mouseXClient, mouseYClient)) {

            // Get the angle from client center to mouse.
            // You're outside the widget, so angle is OK
            curAng = getAngleFromClientCoords(mouseXClient, mouseYClient);

            newAng = curAng;

            majorState = MAJORSTATE_ROTATE_IMPORT;
         }
         else {

            // init for dragging import frame

            newOffsetX = netOffsetX;
            newOffsetY = netOffsetY;

            majorState = MAJORSTATE_OFFSET_IMPORT;
         }

      break;
   }
}

void CTransformEditor::mouseMove(int x, int y)
{
   deltaMouseXClient = x - mouseXClient;
   deltaMouseYClient = y - mouseYClient;

   mouseXClient = x;
   mouseYClient = y;

   int xframe = displayer->scaleXClientToFrame(x);
   int yframe = displayer->scaleYClientToFrame(y);

   deltaMouseXFrame = xframe - mouseXFrame;
   deltaMouseYFrame = yframe - mouseYFrame;

   mouseXFrame = xframe;
   mouseYFrame = yframe;

   RECT box;
   box.left   = mouseXClient - BOX_SIZE;
   box.top    = mouseYClient - BOX_SIZE;
   box.right  = mouseXClient + BOX_SIZE;
   box.bottom = mouseYClient + BOX_SIZE;

   int inipt, finpt,
       prept, nxtpt;

   double leng;
   double ux, uy;
   double vx, vy;
   double dotprod;
   double crsprod;
   double a, b;

   switch(majorState) {

      case MAJORSTATE_TRACK:

         // use the machinery of Paint I, which integrates
         // panning and onionskin displacement
         displayer->mouseMv(x, y);

         if (displayer->isPanning())
            displayer->displayFrame();

      break;

      case MAJORSTATE_KEEP_ANGLES:

         if (widgetBox & 1) { // grabbing an edge

            // we assume the widget edges are all non-zero
            inipt = (widgetBox - 1)%8;
            finpt = (widgetBox + 1)%8;
            nxtpt = (widgetBox + 3)%8;
            prept = (widgetBox + 5)%8;

            ux = rotWidget[nxtpt].x - rotWidget[finpt].x;
            uy = rotWidget[nxtpt].y - rotWidget[finpt].y;

            leng = sqrt(ux*ux+uy*uy);
            ux /= leng;
            uy /= leng;

            dotprod = ux*deltaMouseXClient + uy*deltaMouseYClient;

            ux *= dotprod;
            uy *= dotprod;

            // (ux, uv) is the displacement to be applied to inipt, finpt
            tntWidget[inipt].x = rotWidget[inipt].x + ux;
            tntWidget[inipt].y = rotWidget[inipt].y + uy;
            tntWidget[finpt].x = rotWidget[finpt].x + ux;
            tntWidget[finpt].y = rotWidget[finpt].y + uy;

            // the opposite displacement to the opposite pts
            tntWidget[prept].x = rotWidget[prept].x - ux;
            tntWidget[prept].y = rotWidget[prept].y - uy;
            tntWidget[nxtpt].x = rotWidget[nxtpt].x - ux;
            tntWidget[nxtpt].y = rotWidget[nxtpt].y - uy;
         }
         else {               // grabbing a vertex

            // we assume the widget edges are all non-zero
            inipt = (widgetBox + 6)%8;
            finpt = (widgetBox + 0)%8;
            nxtpt = (widgetBox + 2)%8;
            prept = (widgetBox + 4)%8;

            // the unit vector from finpt to nxtpt

            ux = rotWidget[nxtpt].x - rotWidget[finpt].x;
            uy = rotWidget[nxtpt].y - rotWidget[finpt].y;

            leng = sqrt(ux*ux+uy*uy);
            ux /= leng;
            uy /= leng;

            // the unit vector from inipt to finpt

            vx = rotWidget[finpt].x - rotWidget[inipt].x;
            vy = rotWidget[finpt].y - rotWidget[inipt].y;

            leng = sqrt(vx*vx+vy*vy);
            vx /= leng;
            vy /= leng;

            // now we resolve the mouse displacement along
            // the two (non-orthogonal) unit vectors. This
            // means solving the linear equations
            //
            //   ux * a + vx * b = deltaMouseXClient
            //
            //   uy * a + vy * b = deltaMouseYClient
            //
            // which we do with Cramer's Rule

            crsprod = ux*vy - uy*vx;

            if (fabs(crsprod) < .01) return;

            a = (deltaMouseXClient*vy - deltaMouseYClient*vx) / crsprod;

            b = (deltaMouseYClient*ux - deltaMouseXClient*uy) / crsprod;

            ux *= a;
            uy *= a;

            vx *= b;
            vy *= b;

            // (ux, uy) is the displacement to be applied to inipt, finpt
            tntWidget[inipt].x = rotWidget[inipt].x + ux;
            tntWidget[inipt].y = rotWidget[inipt].y + uy;
            tntWidget[finpt].x = rotWidget[finpt].x + ux;
            tntWidget[finpt].y = rotWidget[finpt].y + uy;

            // the opposite displacement to the opposite pts
            tntWidget[prept].x = rotWidget[prept].x - ux;
            tntWidget[prept].y = rotWidget[prept].y - uy;
            tntWidget[nxtpt].x = rotWidget[nxtpt].x - ux;
            tntWidget[nxtpt].y = rotWidget[nxtpt].y - uy;

            // (vx, vy) is the displacement to be applied to finpt, nxtpt
            tntWidget[finpt].x += vx;
            tntWidget[finpt].y += vy;
            tntWidget[nxtpt].x += vx;
            tntWidget[nxtpt].y += vy;

            // the opposite displacement to the opposite pts
            tntWidget[inipt].x -= vx;
            tntWidget[inipt].y -= vy;
            tntWidget[prept].x -= vx;
            tntWidget[prept].y -= vy;
         }

         // if widget area not too small accept and display it
         if (!isTooSmall(tntWidget))
            acceptTentativeWidget();

      break;

      case MAJORSTATE_SCALE_IMPORT:

         inipt = widgetBox;
         finpt = (widgetBox + 2)%8;
         nxtpt = (widgetBox + 4)%8;

         // resolve the mouse displacement along the unit
         // vector from the image center to the inipt

         ux = rotWidget[inipt].x - rgbXctr;
         uy = rotWidget[inipt].y - rgbYctr;

         // leng can't be 0 because widget never has 0 area
         leng = sqrt(ux*ux+uy*uy);
         ux /= leng;
         uy /= leng;

         dotprod = ux*deltaMouseXClient + uy*deltaMouseYClient;

         ux *= dotprod;
         uy *= dotprod;

         // apply the resolved displacement to the inipt
         tntWidget[inipt].x = rotWidget[inipt].x + ux;
         tntWidget[inipt].y = rotWidget[inipt].y + uy;

         // now get the coefficient of scaling
         vx = tntWidget[inipt].x - rgbXctr;
         vy = tntWidget[inipt].y - rgbYctr;

         a = sqrt(vx*vx+vy*vy) / leng;

         // apply the scaling to all the vertices
         for (int i = 0; i < 8; i+=2) {
            tntWidget[i].x = (rotWidget[i].x - rgbXctr) * a + rgbXctr;
            tntWidget[i].y = (rotWidget[i].y - rgbYctr) * a + rgbYctr;
         }

         // if widget area not too small accept and display it
         if (!isTooSmall(tntWidget))
            acceptTentativeWidget();

      break;

      case MAJORSTATE_SKEW_IMPORT:

         if (widgetBox & 1) { // grabbing an edge

            inipt = (widgetBox - 1)%8;
            finpt = (widgetBox + 1)%8;
            nxtpt = (widgetBox + 3)%8;
            prept = (widgetBox + 5)%8;

            tntWidget[inipt].x = rotWidget[inipt].x + deltaMouseXClient;
            tntWidget[inipt].y = rotWidget[inipt].y + deltaMouseYClient;
            tntWidget[finpt].x = rotWidget[finpt].x + deltaMouseXClient;
            tntWidget[finpt].y = rotWidget[finpt].y + deltaMouseYClient;

            tntWidget[nxtpt].x = rotWidget[nxtpt].x - deltaMouseXClient;
            tntWidget[nxtpt].y = rotWidget[nxtpt].y - deltaMouseYClient;
            tntWidget[prept].x = rotWidget[prept].x - deltaMouseXClient;
            tntWidget[prept].y = rotWidget[prept].y - deltaMouseYClient;
         }
         else {               // grabbing a vertex

            // we assume the widget edges are all non-zero
            inipt = (widgetBox + 6)%8;
            finpt = (widgetBox + 0)%8;
            nxtpt = (widgetBox + 2)%8;
            prept = (widgetBox + 4)%8;

            tntWidget[inipt]   = rotWidget[inipt];

            tntWidget[finpt].x = rotWidget[finpt].x + deltaMouseXClient;
            tntWidget[finpt].y = rotWidget[finpt].y + deltaMouseYClient;

            tntWidget[nxtpt]   = rotWidget[nxtpt];

            tntWidget[prept].x = rotWidget[prept].x - deltaMouseXClient;
            tntWidget[prept].y = rotWidget[prept].y - deltaMouseYClient;
         }

         // if widget area not too small accept and display it
         if (!isTooSmall(tntWidget))
            acceptTentativeWidget();

      break;

      case MAJORSTATE_ROTATE_IMPORT:

         if ((deltaMouseXClient != 0)||(deltaMouseYClient != 0)) {

            // get the angle from client center to mouse
            newAng = getAngleFromClientCoords(mouseXClient, mouseYClient);

            if (newAng >= 0.) { // if angle is determinate do

               // change in net rotation angle
               netRotationAngle += (newAng - curAng) ;

               curAng = newAng;

               // rotate the widget
               transformStretchedToRotated(rotWidget, strWidget, netRotationAngle);

#ifdef USE_QUEUE
               loadTransformQueue(netRotationAngle,
                                  netStretchX, netStretchY,
                                  netSkew,
                                  netOffsetX, netOffsetY);
#else
               displayer->setImportRotationExact(netRotationAngle,
                                                 netStretchX, netStretchY,
                                                 netSkew);

               if (toolPaintInterface != NULL) {

                  displayer->getImportOffsets(&netOffsetX, &netOffsetY);
                  toolPaintInterface->setTransform(netRotationAngle,
                                                   netStretchX, netStretchY,
                                                   netSkew,
                                                   netOffsetX, netOffsetY);
               }

               displayer->displayFrame();
#endif
            }
         }

      break;

      case MAJORSTATE_OFFSET_IMPORT:

         if ((deltaMouseXClient != 0)||(deltaMouseYClient != 0)) {

            newOffsetX += deltaMouseXFrame;
            newOffsetY += deltaMouseYFrame;

#ifdef USE_QUEUE
            loadTransformQueue(netRotationAngle,
                               netStretchX, netStretchY,
                               netSkew,
                               newOffsetX,
                               newOffsetY);
#else
            displayer->mouseMv(x, y);

            if (toolPaintInterface != NULL) {

                  displayer->getImportOffsets(&netOffsetX, &netOffsetY);
                  toolPaintInterface->setTransform(netRotationAngle,
                                                   netStretchX, netStretchY,
                                                   netSkew,
                                                   netOffsetX, netOffsetY);
               }
#endif
         }

      break;
   }
}

void CTransformEditor::mouseUp()
{
   switch(majorState) {

      case MAJORSTATE_TRACK:

         displayer->mouseUp();

      break;

      case MAJORSTATE_KEEP_ANGLES:
      case MAJORSTATE_SCALE_IMPORT:
      case MAJORSTATE_SKEW_IMPORT:
      case MAJORSTATE_ROTATE_IMPORT:

          majorState = MAJORSTATE_TRACK;

      break;

      case MAJORSTATE_OFFSET_IMPORT:

         displayer->mouseUp();

         majorState = MAJORSTATE_TRACK;

      break;
   }
}





