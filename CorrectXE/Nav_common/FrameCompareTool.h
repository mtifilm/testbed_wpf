//---------------------------------------------------------------------------

#ifndef FrameCompareToolH
#define FrameCompareToolH
//---------------------------------------------------------------------------

#include "ToolCommand.h"
#include "ToolCommandTable.h"
#include "ToolObject.h"
#include "ToolProgressMonitor.h"
#include "ToolSystemInterface.h"
#include "ToolUserInputMap.h"

//////////////////////////////////////////////////////////////////////

// Frame Compare Tool Number (16-bit number that uniquely identifies this tool)
#define FRAME_COMPARE_TOOL_NUMBER    224

// Tracking Tool Command Numbers
#define FCMP_CMD_INVALID                        -1
#define FCMP_CMD_NOOP                            0
#define FCMP_CMD_START_MOVING_DIVIDER            1
#define FCMP_CMD_STOP_MOVING_DIVIDER             2
#define FCMP_CMD_TOGGLE_WIPE_MODE                3
#define FCMP_CMD_INCREASE_DIVIDER_ANGLE          4
#define FCMP_CMD_INCREASE_DIVIDER_ANGLE_SMALL    5
#define FCMP_CMD_INCREASE_DIVIDER_ANGLE_LARGE    6
#define FCMP_CMD_DECREASE_DIVIDER_ANGLE          7
#define FCMP_CMD_DECREASE_DIVIDER_ANGLE_SMALL    8
#define FCMP_CMD_DECREASE_DIVIDER_ANGLE_LARGE    9
#define FCMP_CMD_MOVE_DIVIDER_UP_LEFT           10
#define FCMP_CMD_MOVE_DIVIDER_UP_LEFT_SMALL     11
#define FCMP_CMD_MOVE_DIVIDER_UP_LEFT_LARGE     12
#define FCMP_CMD_MOVE_DIVIDER_DOWN_RIGHT        13
#define FCMP_CMD_MOVE_DIVIDER_DOWN_RIGHT_SMALL  14
#define FCMP_CMD_MOVE_DIVIDER_DOWN_RIGHT_LARGE  15
#define FCMP_CMD_TOGGLE_DIVIDER_VERT_HORIZ      16
#define FCMP_CMD_TOGGLE_SIDE_BY_SIDE_MODE       17
#define FCMP_CMD_TOGGLE_LABELS_ON_OFF           18
#define FCMP_CMD_START_CHANGING_DIVIDER_ANGLE   19
#define FCMP_CMD_STOP_CHANGING_DIVIDER_ANGLE    20
#define FCMP_CMD_EXIT_EITHER_MODE               21


//////////////////////////////////////////////////////////////////////


class FrameCompareTool : public CToolObject
{
public:
	FrameCompareTool(const string &newToolName);
   virtual ~FrameCompareTool();

   // Static method to create a new instance of this tool
   static CToolObject* MakeTool(const string& newToolName);

   virtual int toolInitialize(CToolSystemInterface *newSystemAPI);
   virtual int toolShutdown();

   // Tool Event Handlers
   virtual int onChangingClip();
   virtual int onChangingFraming();
   virtual int onRedraw(int frameIndex);
   virtual int onUserInput(CUserInput &userInput);
   virtual int onToolCommand(CToolCommand &toolCommand);
	virtual int onMouseMove(CUserInput &userInput);
	virtual int onHeartbeat();

   virtual int toolActivate();
	virtual int toolDeactivate();

	virtual CToolProcessor* makeToolProcessorInstance(const bool *newEmergencyStopFlagPtr);

private:
	int startMovingDivider();
	int stopMovingDivider();
	int startChangingAngle();
	int stopChangingAngle();
	int moveDivider(double fractionalOffset);
	int changeDividerAngle(int changeAmountInDegrees);
	void exitFromFrameCompareMode();

	int _mouseXClient = -1;
	int _mouseYClient = -1;
	bool _isWipingInProgress = false;
	bool _altKeyIsDown = false;
	bool _changingAngle = false;
};

//////////////////////////////////////////////////////////////////////

#endif
