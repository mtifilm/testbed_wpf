#ifndef PlayerH
#define PlayerH

#include <Classes.hpp>
#include "machine.h"
#include "bthread.h"
#include "ClipSharedPtr.h"
#include "Event.h"
#include "HRTimer.h"
#include "MediaFrameBuffer.h"
#include "pTimecode.h"
#include "SafeClasses.h"
#include "ThreadLocker.h"

class TmainWindow;

class CClip;
class CVideoFrameList;
class CImageFormat;
class CTimecode;
class CExtractor;
class CHardwareBuffer;
class CHRTimer;
class CPixelRegionList;
class CSaveRestore;
struct SSpaghettiContext;
class CLineEngine;

#define MARK_NOT_SET (-1)  // special value for marks indicating "not set"

// how many simultaneous reads?
#define MAX_DISK_READER_THREAD_COUNT 16
#define DEFAULT_DISK_READER_THREAD_COUNT 8
#define MIN_DISK_READER_THREAD_COUNT 1



////////////////////////////////////////////////////////////////////////////////
////////////// P L A Y E R /////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class CPlayer
{

struct DisplayRingSlot;

friend void DiskReaderCallBack(void *, void *);

public:

	CPlayer(int numberOfReadThreads = DEFAULT_DISK_READER_THREAD_COUNT);

	~CPlayer();

	void terminate();

	static void DiskReaderCallback(void *, void *);

	bool frameNeedsSynchronize(int);

   void DiskReader(void *);

   void switchToOff();

   void switchToLocked();

   void switchToUnlocked();

   void lockReader(bool);

   void switchToIdle();

   void switchToActive();

#define PLAYER_ERROR_ALLOC_VIDEO_FAILURE_LOCAL -1
#define PLAYER_ERROR_FIELD_BUFFER_ALLOCATION   -2
   int loadVideoClip(ClipSharedPtr &newClip, int newVideoProxyIndex,
                                     int newVideoFramingIndex,
                                     int newDisplayModeIndex);

   void unloadVideoClip();

   bool isAVideoClipLoaded();

   string getLoadedVideoClipName();

   const CImageFormat *getVideoClipImageFormat();

   int getEntireImageWidth();

   int getEntireImageHeight();

   int getVisibleImageTop();
   int getVisibleImageBottom();
   int getVisibleImageLeft();
   int getVisibleImageRight();

   double getVideoClipPixelAspectRatio();

   int getVideoClipBitsPerComponent();

   bool getVideoClipInterlaced();

   bool getSubsampledPlaybackMode();

   void setSubsampledPlaybackMode(bool enbl);

private:

   // old way to set fps
   void setPlaybackSpeed(int fps);

public:

   int getPlaybackSpeed();

   void changeNominalPlaybackSpeed(int fps);

   // these methods comprise the new way
   // of setting the display speed, which
   // uses a table of speeds and an index
#define DISPLAY_SPEED_NOMINAL   0
#define DISPLAY_SPEED_SLOW      1
#define DISPLAY_SPEED_MEDIUM    2
#define DISPLAY_SPEED_FAST      3
#define DISPLAY_SPEED_UNLIMITED 4
   void setDisplaySpeed(int indx, int fps);

   int getDisplaySpeed(int indx);

   void setDisplaySpeedIndex(int indx);

   int getDisplaySpeedIndex();

   float getActualDisplaySpeed();

   void setPlaybackFilter(int fltr);

   int getPlaybackFilter();

   void setInitialDisplayMode(int newmode);

   int getInitialDisplayMode();

#define DISPLAY_TO_NEITHER 0
#define DISPLAY_TO_WINDOW  1
#define DISPLAY_TO_MONITOR 2
#define DISPLAY_TO_BOTH    3
   int setDisplayMode(int newmode, bool force = false);

   int resetDisplayMode();

   int getDisplayMode();

#define DISPLAY_FROM_CLIP       0
#define DISPLAY_PROVISIONAL     1
   void setProvisionalMode(int newmode);

   int getProvisionalMode();

   void setProvisionalFrameIndex(int frmnum);

   void clearProvisionalFrameIndex();

   int getProvisionalFrameIndex();

   void setHighlightColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

   void loadProvisionalFrame(MTI_UINT16 *, CPixelRegionList *pxlrgnlst=NULL);

   void loadProvisionalPixelRegionList(CPixelRegionList *);

   // THIS IS UNSAFE so mbraca removed it so nobody would be tempted to use it
   //CPixelRegionList *getProvisionalPixelRegionList();

   void displayProvisionalFrame();

   void refreshFromIntermediate(MTI_UINT16 *, CPixelRegionList *pxlrgnlst=NULL);

	void displayFromRenderThread(MTI_UINT16 *internalFrameData, int frameIndex);

   bool windowDisplayIsEnabled();

   bool monitorDisplayIsEnabled();

   void outputVideo();

	int checkTimecodeLimits(int srcfrm, CVideoFrameList *videoFrameList = nullptr);

	int readMediaFrameBuffer(CVideoFrameList *videoFrameList, int frameIndex, MediaFrameBufferSharedPtr &frameBuffer);

	int getToolFrame(MTI_UINT16 *frm,
                    int srcfrm,
                    CVideoFrameList *videoFrameList = nullptr);

   int putToolFrame(MTI_UINT16 *frm,
                    MTI_UINT16 *aux,
                    CPixelRegionList&,
                    int dstfrm);

   int putToolFrame(MTI_UINT16 *inifrm,
                    MTI_UINT16 *finfrm,
                    MTI_UINT16 *aux,
                    int dstfrm);

	MediaFrameBufferSharedPtr getNextDisplayFrame();

   void setTrackBarUpdate(bool);

   bool trackBarUpdateEnabled();

   int getInFrameIndex();

   int getOutFrameIndex();

   int getLastFrameIndex();

   bool getLastFrameSync();

   void enterTrackBarPos(int);

////   char *getInTimecodeStr();
////   const string& getInTimecodeString();

////   char *getOutTimecodeStr();
////   const string& getOutTimecodeString();

////   char *getLastTimecodeStr();
////   const string& getLastTimecodeString();

   CTimecode getCurrentTimecode();
   CTimecode getInTimecode();
   CTimecode getOutTimecode();

   int getHalfFrame();
   bool getLastTimecodeFieldFlag();

   void displayFrameDone();

   bool isDisplaying();

   void fullStop();

   void goToFrame(int);

	bool overlayHistoryOnDisplayFrame(MediaFrameBufferSharedPtr &frameBuffer, int frameIndex);

	void oneFrameToWindow(MediaFrameBufferSharedPtr buf, int frameIndex, bool fast, bool recomputeImportDiffs);
//	void oneFrameToWindow(MTI_UINT8 **buf, int frameIndex, bool fast, bool recomputeImportDiffs);

// used to be 250, but this was not enough with very large DPX files
// REPLACED WITH TIMER
//#define MAX_LOOP_CYCLES 5000

   void goToFrameSynchronous(int frame, bool fast=false);
   void goToFrameSynchronousCached(int frame, bool fast=false);

   void goToFrame(const CTimecode&);
   void goToFrameSynchronous(const CTimecode&);

   void refreshFrame();
   void refreshFrameSynchronous(bool fast=false);
   void refreshFrameSynchronousCached(bool fast=false);

   void panFrameSynchronous();

   void stopAndSynchronize();

   void stopJogAndSynchronize();

   void goToIn();

   void goToOut();

   void stopOrStart();

   void playClipFwd();

   void playClipBwd();

   void playClipFastFwd(int newDeltaFrame);

   void playClipFastBwd(int newDeltaFrame);

   int getDeltaFrame();

   void jogClipOnceFwd();

   void jogClipOnceBwd();

   void jogClipFastFwd();

   void jogClipFastBwd();

   void jogClipSlowFwd();

   void jogClipSlowBwd();

   void jogClipFwdOneThirdSecond();

   void jogClipBwdOneThirdSecond();

   int setMarkIn();

   int setMarkIn(int);

   int setMarkInTimecode(const CTimecode&);

   int setMarkOutInclusive();

   int setMarkOutExclusive();

   int setMarkOut(int);

   int setMarkOutTimecode(const CTimecode&);

   int setMarkAll();

   int getMarkIn();

   CTimecode getMarkInTimecode();

   int getMarkOut();

   CTimecode getMarkOutTimecode();

   int removeMarkIn();

   int removeMarkOut();

   void goToMarkIn();

   void goToMarkOutMinusOne();

   void playClipInOut();

   void loopClipInOut();

   void playClipFwdBetweenMarks(bool=false);

	void toolPlayClipFrameRangeInclusive(int firstFrameIndex, int lastFrameIndex);

   void loopClipFwdBetweenMarks();

   void loopClipFwd2NPlus1Frames(int N);

   bool isExpandedBetweenMarks();
   
   int expandBetweenMarks();

   int unexpandBetweenMarks();

   void setSubsampledPlayback(bool);

   bool getSubsampledPlayback();

   void setSubsampledPanning(bool);

   bool getSubsampledPanning();

   PTimecode CurrentTC;

   CBHook MarksHaveChanged;

   void SetMainWindowPtr(TmainWindow *);

   // Added to allow timeline to wait before an update
   bool isIdle(void);

   // Added to allow DRS preloading to wait until the system is quiescent
   long getIdleTime();

   CSaveRestore *getReviewHistory();

	// main window refresh now controlled by multimedia timer
   void maybeInvokeMainWindowRefresh();
   void notifyMainWindowRefreshDone();
   void notifyPostMessageWasReceived();
   void expediteMainWindowRefresh();

   // Are we REALLY playing?
   bool isReallyPlaying();
	void setReallyPlaying(bool flag);

/////////////////////////////////////////////////////////////////////////////
// Frame comnpare stuff

public:
//	enum FrameCompareMode { Off, TwoClipWipe, PreviewWipe, TwoClipsSideBySide, PreviewSideBySide };
	enum FrameCompareMode { Off, Wipe, SideBySide };

	int setFrameCompareMode(FrameCompareMode newMode);
	FrameCompareMode getFrameCompareMode();
	int setFrameCompareClipName(const string &clipName);
	int setFrameCompareWiperOriginClient(POINT refPointClient);
	int changeFrameCompareWiperOriginRelative(double fractionalOffset, bool wrapAtExtremes);
	int setFrameCompareWiperAngle(float degrees);
	int changeFrameCompareWiperAngle(float degrees, bool wrapAtExtremes);
	int toggleFrameCompareDividerVertHoriz();

private:
	FrameCompareMode _frameCompareMode = FrameCompareMode::Off;
	string _frameCompareClipName;
	DPOINT _frameCompareWiperRelativeOrigin = {0.5, 0.5};
	float _frameCompareWiperAngleInDegrees = 60.F;  // 0 - 90
	ClipSharedPtr _frameCompareClip;
	CVideoFrameList *_frameCompareClipFrameList = nullptr;


////////////////////////////////////////////////////////////////////////
private:

   TmainWindow *mainWindow = nullptr;

	// extractor to xform intermediates into native for display
   CExtractor *extractor;

	// line engine to draw graphics into native form of provisional
	CLineEngine *lineEngine;

	//#define DISPLAY_TO_WINDOW  1
	//#define DISPLAY_TO_MONITOR 2
	//#define DISPLAY_TO_BOTH    3
   int displayMode;
   int initialDisplayMode;

	// hardware buffering subsystem
	CHardwareBuffer *hdwBuf;

#define NO_FRAME -1

	//
	// READER ring buffer
	//
	struct DisplayRingSlot
	{
		int iframeIndex = NO_FRAME;
		MediaFrameBufferSharedPtr frameBuffer = nullptr;
		bool syncUp = false;
	};

	class DisplayRing
	{
		DisplayRingSlot *_ring = nullptr;
		int _numberOfSlots = 0;
      int _maxNumberOfSlots = 0;

	public:
		DisplayRing();
		~DisplayRing();

      void setMaxSize(int newMaxSize);
      int getMaxSize();
		void setSize(int newSize);
		int size();
		DisplayRingSlot &operator[](int ptr);
	};

	DisplayRing displayRing;

//	int idisplayBuffSlots;
	int idisplayBuffLead;
//   int idisplayBuffCnt;

   int idisplayFillPtr;
	int idisplayEmptyPtr;
   int idisplayLagPtr;

//	DisplayRingSlot *displayBuf;


//////////////////////////////////////////////////////////////////////
//
// READAHEAD hack-o-rama

	int asyncReadThreadCount = DEFAULT_DISK_READER_THREAD_COUNT;

// How many buffers can we play with?
#define READ_AHEAD_TRACKER_COUNT ((MAX_DISK_READER_THREAD_COUNT * 2) + 4) // more than (MAX_DISK_READER_THREAD_COUNT * 2)
#define ASYNCH_READ_TRACKER_COUNT (READ_AHEAD_TRACKER_COUNT + 1)  // +1 for current frame

struct READ_AHEAD_THREAD_PARAMS
{
   // Set by Player before starting thread.
	CPlayer *player = nullptr;

	// Set by Thread at start.
	void *threadId = nullptr;

   // Number of threads for parallel reads.
   int numberOfReadThreads = 4;

   // Set by Player to get thread to die at app exit.
	bool die = false;
};

READ_AHEAD_THREAD_PARAMS _readAheadThreadParams;

struct ASYNC_WAIT_THREAD_PARAMS
{
   // Set by Player before starting thread.
	CPlayer *player = nullptr;
   int trackerNumber = -1;

	// Set by thread at start, cleared when done.
	void *threadId = nullptr;

   // Thread signals when wait and post-processing are complete.
	EventSharedPtr stateTransitionEvent = nullptr;

	~ASYNC_WAIT_THREAD_PARAMS() { stateTransitionEvent = nullptr; };
};

// Need a lock when accessing threadId if the parent is going to act
// based on a thread ID (want to make sure thethread doesn't end between
// reading the ID and tking the action!
static CSpinLock _AsyncWaitParamsThreadIdLock;

enum AsynchReadState
{
	ASYNC_READ_STATE_IDLE,
	ASYNC_READ_STATE_QUEUED,
	ASYNC_READ_STATE_READING,
	ASYNC_READ_STATE_POSTPROCESSING,
	ASYNC_READ_STATE_COMPLETED
};

struct ASYNCH_READ_TRACKER
{
	AsynchReadState state;
	void *threadId;
	int frameNumber;
   int displayBufIndex;
   void *readContext;
	Event completionEvent;  // main waits, thread signals
	int errorCode;
	bool cancel;
	CHRTimer readTimer;
	double readTimeInMsec;;
	CHRTimer totalTimer;

   ASYNCH_READ_TRACKER()
   {
      reset();
	};

   void reset()
	{
		state = ASYNC_READ_STATE_IDLE;
		frameNumber = -1;
      displayBufIndex = -1;
      readContext = NULL;
		errorCode = 0;
      cancel = false;
		readTimeInMsec = 0.0;
		completionEvent.reset();
	};

	bool isInUse()          { return state != ASYNC_READ_STATE_IDLE; };
	bool isReading()        { return state == ASYNC_READ_STATE_READING; };
	bool isPostprocessing() { return state == ASYNC_READ_STATE_POSTPROCESSING; };
	bool isBeingWorkedOn()  { return isReading() || isPostprocessing(); };
	bool isDone()           { return state == ASYNC_READ_STATE_COMPLETED; };

} asynchReadTracker[ASYNCH_READ_TRACKER_COUNT];

   CRingBuffer<int> readAheadWorkQueue;

   enum RA_DIRECTION {
      RA_FORWARD,
      RA_BACKWARD
   };

   int queueFrameReads(int frameCount=0,
                       int currentFrameNumber=-1,
							  int currentFrameBufferIndex = -1,
							  RA_DIRECTION readDirection = RA_FORWARD,
							  int wrapAtFrameNumber = -1,
                       int wrapToFrameNumber = -1);

   void cleanUpReadAheads();

   int readCurrentFrame(int currentFrameNumber, int currentFrameBuffIndex, double *frameReadTime);

#define PLAYER_ERROR_ALL_READ_AHEAD_THREADS_ARE_BUSY   -3

   int queueUpAnAsynchronousFrameRead(int trackerNumber,
                                    int frameNumber,
                                    int displayBufIndex);

   bool isAsynchronousFrameReadDone(int trackerNumber);

   int finishAnAsynchronousFrameRead(int trackerNumber);

   static void startReadAheadThread(void *vpAppData, void *vpReserved);
//	static void startOldStyleAsyncWaitThread(void *vpAppData, void *vpReserved);
//	static void startNewFangledAsyncWaitThread(void *vpAppData, void *vpReserved);

   void runReadAheadThread(READ_AHEAD_THREAD_PARAMS &params);
//	void startOldStyleAsyncRead(int trackerNumber);
//	void runOldStyleAsyncWaitThread(int trackerNumber);
//	void startNewFangledAsyncRead(int trackerNumber);
//	void runNewFangledAsyncWaitThread(int trackerNumber);

	static void startReadAheadWorkerThread(void *vpAppData, void *vpReserved);
	void runReadAheadWorkerThread(ASYNC_WAIT_THREAD_PARAMS &params);

	void recordFrameReadTime(double newReadTime);

	void *_readAheadWorkerThreadCompletionSemaphore2  = nullptr;


//////////////////////////////////////////////////////////////////////
//
// SIMPLE TOOL FRAME BUFFER

// THERE IS NO NEED TO HAVE THIS BE AN INSTANCE VARIABLE!!
////MediaFrameBufferSharedPtr getBuf;

//////////////////////////////////////////////////////////////////////
//
// INVISIBLE FIELDS array. The number of entries
// is srcMaxFieldCount-1-srcInterlaced

	MediaFrameBufferSharedPtr invBuf;

//////////////////////////////////////////////////////////////////////
//
// PROVISIONAL FIELDS array, frame index, and
// flag to indicate frame ready for display
//
	MediaFrameBufferSharedPtr provisionalFrameBuffer;
	int provisionalFrameIndex;

// for toggling orig/processed
//#define DISPLAY_FROM_CLIP     0
//#define DISPLAY_PROVISIONAL   1
   int provisionalDisplayMode;

// enables refresh of provisional frame alone
   bool isProvisionalReadyToDisplay;

// -> pixel region list to draw on top of frame
   CPixelRegionList *prvPixelRegionList;

// Thread Locker to keep pixel region list from deletion
   CThreadLock *prvPixelRegionListLock;

//////////////////////////////////////////////////////////////////////
//
// PLAYBACK REVIEW equipment
//
   CSaveRestore *reviewHistory;

   MTI_UINT16 *reviewBuf;
   int reviewBufSize;

   CPixelRegionList *reviewPixelRegionList;

#define REVIEW_MODE_HIGHLIGHT       1
#define REVIEW_MODE_ORIGINAL_VALUES 2
   int reviewMode;
//
//////////////////////////////////////////////////////////////////////

// READER virtual machine
//
// The operation of the READER virtual machine is extremely simple.
// In the execution of a typical read operation, the reader is forced
// into the IDLE state by issuance of an IDLE_READER command. Then
// the required parameter settings are made after checking that the
// IDLE state has been reached. Finally, the ACTIVATE_READER command
// is issued to cause the reader to read the sequence of frames as
// specified by the parameter settings.

	// major states for the READER virtual machine
	//
	enum ReaderMajorState
	{
		READER_IDLE,      // the reader is waiting for the next command
		READER_ACTIVE,		// the reader is reading a sequence of frames
								//    according to the parameters which have been set.
		READER_LOCKED,    // the reader thread is reading commands
		READER_ENDED      // the reader thread was successfully killed
	};

		ReaderMajorState majorState;
		void setMajorState(ReaderMajorState newState);

	// opcodes for the READER virtual machine
	//
	enum ReaderCommandOpCode
	{
		NOOP_READER,		// has no effect
		IDLE_READER,		// suspends reading by reader thread and limits
								// activity to checking for next command
		ACTIVATE_READER,  // causes reader to execute the read command for
								// which the parameters have been set.
		LOCK_READER,      // puts reader in loop to discard any commands except
		UNLOCK_READER,    // unlock causes reader to be placed in IDLE state
		KILL_READER       // kills the reader thread, shutting it down
	};

	void sendCommandToDiskReader(ReaderCommandOpCode cmd);
	ReaderCommandOpCode diskReaderCommand;
   void *kickDiskReader;        // Semaphore to wake up the disk reader
   void *diskReaderCommandAck;  // Semaphore to release calling thread
   CThreadLock diskReaderCommandLock;

   // State flag that indicates disk reader is being throttled back
   bool diskReaderIsThrottledBack;

// Thread locker to ensure proper order of reads by background threads
   CThreadLock diskReadThreadLock;


///////////////////////////////////////////////////////////////////////////////

// the SRC clip
   ClipSharedPtr srcClip;

// the SRC max field count
   int srcMaxFieldCount;

// the SRC visible field count
   int srcVisibleFieldCount;

// the SRC invisible field count
   int srcInvisibleFieldCount;

// the SRC video proxy index
   int srcVideoProxyIndex;

// the SRC video framing index
   int srcVideoFramingIndex;

// the SRC video frame list
   CVideoFrameList *srcVideoFrameList;

// the SRC video image format
   const CImageFormat *srcVideoImageFormat;

// the nominal frames-per-second
   int srcFramesPerSecond;

// max padded bytes per field
   int srcBytesPerField;

// the max and min frame limits
   int srcMinFrame;
   int srcMaxFrame;

// the SRC timecodes
   CTimecode *srcMinTimecode;
   CTimecode *srcMaxTimecode;

// the SRC timecode strings
////   char srcMinTimecodeStr[16];
////   char srcMaxTimecodeStr[16];

// REAL strings
////   string srcMinTimecodeString;
////   string srcMaxTimecodeString;

// the SRC image stats
   int    srcVisibleImageTop;
   int    srcVisibleImageBottom;
   int    srcVisibleImageLeft;
   int    srcVisibleImageRight;
#define FRAME4K 3999
   int    srcEntireImageHeight;
   int    srcEntireImageWidth;
   double srcPixelAspectRatio;
   int    srcPixelComponents;
   int    srcBitsPerComponent;
   bool   srcInterlaced;
   int    pxlComponentCount;

// the max and min marks
   int srcMinMark;
   int srcMaxMark;

// the limits save stack

struct LIMITS
{
   int minFrame;
   int maxFrame;
};

#define LIMITS_STACK_DEPTH 32

   int iLimStackSiz;

   int iLimStackPtr;

   LIMITS savedLimits[LIMITS_STACK_DEPTH];

///////////////////////////////////////////////////////////////////////////////

   bool blockCmdsExcStop;

#define PLAY_CLIP_FWD      1
#define PLAY_CLIP_BWD      2
#define LOOP_CLIP_FWD      3
#define FOLLOW_TRACKBAR    4
#define JOG_ONCE_FWD       5
#define JOG_ADDITIONAL_FWD 6
#define JOG_ONCE_BWD       7
#define JOG_ADDITIONAL_BWD 8

   int readMode;

   bool updateTrackBar; // needed to prevent feedback in sliding mode

   // used to cancel 'follow trackbar' mode after a half second of inactivity
   CHRTimer followTrackBarCancelTimer;

#define JOG_FRAME_QUEUE_SIZE 720

   int jogFrmQSize;

   int jogFrmQFillPtr;

   int jogFrmQEmptyPtr;

   int jogFrmQAdv;

   int *jogFrameQueue;

   int idwnFrame;
   int idelFrame;

   int ibegFrame;

   int iendFrame;

   int icurFrame;

   int deltaOneThird;

#define SLOW_DELTA_FRAME  1
#define MEDIUM_DELTA_FRAME  3
#define FAST_DELTA_FRAME 15
   int deltaFrame;

	MediaFrameBufferSharedPtr lastDisplayedFrameBuffer;
   int  lastDisplayedFrameIndex;
   bool isLastFrameOfSequence;

   // Flags for FAST feature. When this is enabled,
   // continuous playback and panning will employ
   // a subsampled display for higher speed.
   bool subsampledPlayback;
   bool subsampledPanning;

   // Flag for PLAYBACK REVIEW feature.
   // CAREFUL! THESE DEFINES ARE DUPLICATED IN ToolSystemInterface.h!!!!!!
#define PLAYBACK_FILTER_INVALID           -1
#define PLAYBACK_FILTER_NONE               0
#define PLAYBACK_FILTER_ORIGINAL_VALUES    1
#define PLAYBACK_FILTER_HIGHLIGHT_FIXES    2
#define PLAYBACK_FILTER_BOTH               (PLAYBACK_FILTER_ORIGINAL_VALUES + PLAYBACK_FILTER_HIGHLIGHT_FIXES)
#define PLAYBACK_FILTER_IMPORT_FRAME_DIFFS 4
   int playbackFilter;

   // timer for regulating playback speed
   CHRTimer *interframeDelayTimer;

   // the playback speed code
   int playbackSpeed;

   // interframe interval for nominal playback
   double nominalFrameInterval;

   // Desired inter-frame display delay
   double frameInterval;

   // inter-frame delay that overrides frameInterval (for jog ramps)
   int interFrameDelayIndex;

   // variables for the new way of setting
   // display (playback) speed selector
   int displaySpeedIndex; // 0-4

#define SPEED_UNLIMITED_FPS 100

   // speeds (selected by the above index)
   int displaySpeed[5]; // speeds in frms/sec

#define INTERVAL_RING_SIZE 15

   // This ring buffer records actual inter-frame intervals so we can
   // compute the actual FPS for display in the status bar
   float frameDeltaTime[INTERVAL_RING_SIZE];
   unsigned long frameDeltaTimeIndex;

   // This ring buffer records the observed read times for the most
   // recent frames & is used to compute an average that is used to
   // try to make playback less "herky-jerky"
   float frameReadTime[INTERVAL_RING_SIZE];
   unsigned long frameReadTimeIndex;

   // The computed "average" read time
   float averageReadTime;

   // Since the averageReadTime is computed incorrectly we now basically
   // ignore that and use the computed frame rate for playback smoothing!
   float observedFrameInterval;

   // stuff to control main window refresh
   float lastTimeFrameWasDisplayed;
   float nextFrameDisplayTime;
   int activeDutyCycle;
   HANDLE multimediaTimerHandle;
   bool inhibitMainWindowRefreshFlag;
   bool expediteMainWindowRefreshFlag;

   // synchronization between timer and main threads for main window refresh
   int messagePostedSerialNumber;
   int messageReceivedSerialNumber;
   int refreshInvokedSerialNumber;
   int refreshDoneSerialNumber;

   // actual display speed in frames per second
   float actualDisplaySpeed;

   CTimecode *lastTimecode;

////   char lastTimecodeStr[16];
////   string lastTimecodeString;

   int lastHalfFrame;
   bool lastTimecodeFieldFlag;

   // internal methods for actually setting the marks
   int setMinMarkInternal(int frame);
   int setMaxMarkInternal(int Frame);

   // internal method to make sure the display is stabilized before
   // setting marks to the "current frame".
   void stabilizeDisplay();

   // Video ring buffer
//   void *videoRingBufferAddr;
//   size_t videoRingBufferSize;

   // timer for tracking how long the reader has been idle
   CHRTimer *idleTimer;

   // A flag to distinguish between 'really playing' and 'just going to a frame'
   bool reallyPlayingFlag;

/***************************************************************************/

//   // This crap was stolen from VTREmulator/HardwareBuffer
//#define MAX_FIELD_PER_FRAME 10  // Haha HUGE!
//   struct CVideoBuffer
//	{
//     MTI_UINT8 *ucpField[MAX_FIELD_PER_FRAME];
//     long laBufIndex[MAX_FIELD_PER_FRAME];
//   };
//
//   int AllocVideo(int iNFrame, int iMaxPaddedFieldByteCount, int iFieldCount);
//   void FreeVideo();
//   MTI_UINT8 *getDataVideo(int iFrame, int iField);
//
//   CVideoBuffer *vbpRing;// class to hold video buffers, one per frame
//   int iNRingFrames;
//   MTI_UINT8 *ucpVideoPool;
///////////////////////////////////////////////////////////////////////////////

};

#endif
