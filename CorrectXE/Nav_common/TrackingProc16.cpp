// TrackingProc16.cpp: implementation of the CTrackingProc16 class.
//
//////////////////////////////////////////////////////////////////////

#include "TrackingProc16.h"

#include "bthread.h"
#include "Clip3.h"
#include "err_tool.h"
#include "ImageFormat3.h"
#include "IniFile.h"  // for TRACE_N()
#include "MathFunctions.h"
#include "MTIKeyDef.h"
#include "MTImalloc.h"
#include "MTIsleep.h"
#include "MTIstringstream.h"
#include "PixelRegions.h"
#include "ThreadLocker.h"
#include "ToolCommand.h"
#include "ToolCommandTable.h"
#include "ToolProgressMonitor.h"
#include "ToolSystemInterface.h"
#include "ToolUserInputMap.h"
#include "TrackingBoxManager.h"
#include "TrackingTool.h"

#define TRACKING_MIN_THREADS 1000  // effectively disable multithreading
static CSpinLock TrackingThreadLock;

///////////////////////////////////////////////////////////////////////////////
// Construction/Destruction
///////////////////////////////////////////////////////////////////////////////

CTrackingProc::CTrackingProc(int newToolNumber, const string &newToolName,
                                 CToolSystemInterface *newSystemAPI,
                                 const bool *newEmergencyStopFlagPtr,
                                 IToolProgressMonitor *newToolProgressMonitor)
 : CToolProcessor(newToolNumber, newToolName, 1, 1, newSystemAPI,
                  newEmergencyStopFlagPtr, newToolProgressMonitor)
 , _MonoImageIndex(-1)
{
   StartProcessThread(); // Required by Tool Infrastructure

   _MonoImages[0] = NULL;
   _MonoImages[1] = NULL;
}

CTrackingProc::~CTrackingProc()
{
   // Shouldn't the subimages array be cleared as well? QQQ
   _TrackingThreadParametersArray.clear();
}


///////////////////////////////////////////////////////////////////////////////
// Setup
///////////////////////////////////////////////////////////////////////////////

int CTrackingProc::SetIOConfig(CToolIOConfig *toolIOConfig)
{
   // Tell Tool Infrastructure, via CToolProcessor supertype, about the
   // number of input and output frames

   SetInputMaxFrames(0, 2, 1);  // 'In' port index 0,
                                // Need two frames for each iteration,
                                // Need one new frame each iteration.

   SetOutputMaxFrames(0, 1);  // 'Out' port index 0,
                              // One frame output per iteration

   // Well, really the frame is not modified at all, but we need
   // to 'output' it to display it
   SetOutputModifyInPlace(0, 0, false); // 'Out' port index 0,
                                        // 'In' port index 0,
                                        // Do not clone the output frame

   return 0;
}

int CTrackingProc::SetFrameRange(CToolFrameRange *toolFrameRange)
{
   CToolFrameRange::SFrameRange &frameRange = toolFrameRange->inFrameRange[0];
   _inFrameIndex = frameRange.inFrameIndex;
   _outFrameIndex = frameRange.outFrameIndex;
//
//   IToolProgressMonitor *progressMonitor = GetToolProgressMonitor();
//   if (progressMonitor != NULL)
//   {
//      progressMonitor->SetFrameCount(_outFrameIndex - _inFrameIndex);
//      progressMonitor->SetStatusMessage("Tracking");
//   }

   return 0;
}

int CTrackingProc::SetParameters(CToolParameters *toolParams)
{
   int retVal=0;

   CTrackingToolParameters *trackingToolParams
                        = static_cast<CTrackingToolParameters*>(toolParams);

   // Make a private copy of the parameters for later reference
   // Caller's toolParam is deleted after this function returns
   _trkPtsEditor = trackingToolParams->trackingParameters.TrkPtsEditor;
   _trackingArray = _trkPtsEditor->getTrackingArray();
//   _trackingArray->getExtents(_TrackBoxExtentX, _TrackBoxExtentY,
//                              _SearchBoxLeftExtent, _SearchBoxRightExtent,
//                              _SearchBoxTopExtent, _SearchBoxBottomExtent);
   _TrackBoxExtentX = _TrackBoxExtentY = _trackingArray->getTrackingBoxRadius();
   _SearchBoxLeftExtent = _SearchBoxRightExtent = _SearchBoxTopExtent = _SearchBoxBottomExtent = _trackingArray->getSearchBoxRadius();

   // Grab the display LUT up front if we're going to need it.
   if (_trackingArray->getApplyLutFlag())
   {
      MTImemcpy(_currentDisplayLut8, trackingToolParams->trackingParameters.lut8, 256 * 3);
   }

   _trackingToolObject = trackingToolParams->trackingParameters.TrackingToolObject;

   // Find the iteration count
   _iterationCount = _outFrameIndex - _inFrameIndex;

   // Done
   return retVal;
}


///////////////////////////////////////////////////////////////////////////////
//
// Processing functions
//
///////////////////////////////////////////////////////////////////////////////

int CTrackingProc::GetIterationCount(SToolProcessingData &procData)
{
   // This function is called by the Tool Infrastructure so it knows
   // how many processing iterations to do before stopping

   return _iterationCount;
}
///////////////////////////////////////////////////////////////////////////////

int CTrackingProc::BeginProcessing(SToolProcessingData &procData)
{
   CAutoErrorReporter autoErr("CTrackingProc::BeginProcessing",
                              AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   int retVal = 0;
   int nTmpX, nTmpY;

   // Tracking does not save to history, since it doesn't modify the frames, duh!
   SetSaveToHistory(false);

   // Just save some values for later

   const CImageFormat *imageFormat = GetSrcImageFormat(0);
   nPicCol = imageFormat->getPixelsPerLine();
   nPicRow = imageFormat->getLinesPerFrame();
   _RGB = imageFormat->getPixelComponents() == IF_PIXEL_COMPONENTS_RGB
        || imageFormat->getPixelComponents() == IF_PIXEL_COMPONENTS_RGBA
        || imageFormat->getPixelComponents() == IF_PIXEL_COMPONENTS_Y
        || imageFormat->getPixelComponents() == IF_PIXEL_COMPONENTS_YYY;

   const CImageFormat *realImageFormat = GetSystemAPI()->getVideoClipImageFormat();
   _sourceDepth = realImageFormat->getBitsPerComponent();

   if (_trackingArray->getApplyLutFlag())
   {
      int channel = _trackingArray->getChannel();
      vector<float> compWeights = { 0.32F, 0.64F, 0.04F };
      switch (channel)
      {
         case 0:
            compWeights = { 1.0F, 0.0F, 0.0F };
            break;
         case 1:
            compWeights = { 0.0F, 1.0F, 0.0F };
            break;
         case 2:
            compWeights = { 0.0F, 0.0F, 1.0F };
            break;
         default:
            break;
      }

      int shift = _sourceDepth - 8;
      int interpolatedSteps = 1 << shift;

      for (int i = 0; i < 3; ++i)
      {
         for (int j = 0; j < 256; ++j)
         {
            int index8 = (i * 256) + j;
            MTI_INT32 value = _currentDisplayLut8[index8] << shift;
            MTI_INT32 nextValue = (j == 255) ? value + interpolatedSteps : (_currentDisplayLut8[index8 + 1] << shift);
            MTI_INT32 stepWidth = (nextValue - value) >> shift;
            for (int k = 0; k < interpolatedSteps; ++k)
            {
               int index16 = (i * 65536) + (j * interpolatedSteps) + k;
               _currentDisplayLut16[index16] = MTI_UINT16(value * compWeights[i]);
               value += stepWidth;
            }
         }
      }
   }

   // DEBUGGING!
   //GetSystemAPI()->enterPreviewHackMode();

   // Sort the tracking points into the order they need to be processed
   _AnchorList = _trackingArray->getTrackingBoxTags(); // was CreateAnchorTree();

   // Kill old processing threads if still running
   if (!_TrackingThreadParametersArray.empty())
   {
      int runningCount = 0;
      for (auto &paramEntry : _TrackingThreadParametersArray)
      {
         auto &params = paramEntry.second;
         if (!params.doneFlag)
         {
            runningCount++;
         }
      }

      _TrackingThreadsDoneSync.SetCount(runningCount);

      for (auto &subimages : _TrackingBoxSubimagesArray)
      {
         auto tag = subimages.first;
         auto &params = _TrackingThreadParametersArray[tag];
         if (!params.doneFlag)
         {
            params.doneFlag = true;
            params.startSync.ReleaseOne();
         }
      }

      _TrackingThreadsDoneSync.Wait(_bthreadStructs, __func__, __LINE__);
      _TrackingThreadParametersArray.clear();
   }

   // Allocate per-point data
   _TrackingBoxSubimagesArray.clear();

   // Allocate some useful memory for tracking
   _TrackBoxSizeX = (2 * _TrackBoxExtentX) + 1;
   _TrackBoxSizeY = (2 * _TrackBoxExtentY) + 1;
   _TrackBoxPoints = _TrackBoxSizeX * _TrackBoxSizeY;

   // Boxes must be bigger because we need a point to perform a bilinear interpolation
   nTmpX = 2 * (_TrackBoxExtentX + 1) + 1;
   nTmpY = 2 * (_TrackBoxExtentY + 1) + 1;
   for (auto &trackingBoxTag : _AnchorList)
   {
      _TrackingBoxSubimagesArray[trackingBoxTag]._TrackBoxData = new Pixel[nTmpX * nTmpY];
   }

   // GAAAAHHH !!! CHANGING THE MEANING OF "EXTENT" MIDSTREAM!!!
   _SearchBoxLeftExtent   += _TrackBoxExtentX;
   _SearchBoxRightExtent  += _TrackBoxExtentX;
   _SearchBoxTopExtent    += _TrackBoxExtentY;
   _SearchBoxBottomExtent += _TrackBoxExtentY;
   _SearchBoxSizeX  = (_SearchBoxLeftExtent + _SearchBoxRightExtent) + 1;
   _SearchBoxSizeY  = (_SearchBoxTopExtent + _SearchBoxBottomExtent) + 1;
   _SearchBoxPoints = _SearchBoxSizeX * _SearchBoxSizeY;

   // Boxes must be bigger because we need point to perform a bilinear interpolation
   nTmpX = (_SearchBoxLeftExtent + 1) + (_SearchBoxRightExtent + 1) + 1;
   nTmpY = (_SearchBoxTopExtent + 1) + (_SearchBoxBottomExtent + 1) + 1;
   for (auto &subimagesEntry : _TrackingBoxSubimagesArray)
   {
      subimagesEntry.second._SearchBoxData = new Pixel[nTmpX * nTmpY];
   }

   // Allocate the tracking thread parameters and start the threads
   if (_TrackingBoxSubimagesArray.size() >= TRACKING_MIN_THREADS)
   {
      _TrackingThreadParametersArray.clear();
      for (auto &subimagesEntry : _TrackingBoxSubimagesArray)
      {
         auto tag = subimagesEntry.first;
         STrackingThreadParams &params = _TrackingThreadParametersArray[tag];
         params.iTag = -1;
         params.trackingProc = NULL;
         params.currentFrame = -1;
         params.doneFlag = true;
         params.startSync.SetCount(0);
         auto ts = BThreadSpawn(TrackingThreadController, &_TrackingThreadParametersArray[tag]);
         MTIassert(ts != nullptr);
         _bthreadStructs.push_back(ts);
      }
      // Yield the processor to allow the threads to actually start
      MTImillisleep(1);
    }

//   // Kill all tracking points but the start
//   _trackingArray->eraseAllBut(_inFrameIndex);

   // Initialize the robust point array
   //_trackingToolObject->RobustArray.clear();
   //_trackingToolObject->ComputeRobustTrackNoRotation(_inFrameIndex, _inFrameIndex);

   // Now say we must alloc the monochrome images
   _MonoImageIndex = -1;

   IToolProgressMonitor *progressMonitor = GetToolProgressMonitor();
   if (progressMonitor != NULL)
   {
     progressMonitor->StartProgress();
   }

   HRT.Start();   // Start the timer
   //_trackingToolObject->setStatusMessage("Tracking...");

   // Success, continue
   return retVal;
}
///////////////////////////////////////////////////////////////////////////////

int CTrackingProc::BeginIteration(SToolProcessingData &procData)
{
   int frameIndexList[2];
	bool isLastPass = (procData.iteration == (_iterationCount - 1));
   int numberOfInputFrames = (isLastPass? 1 : 2);

   // Track from current frame to next frame, then show CURRENT frame

   frameIndexList[0] = _inFrameIndex + procData.iteration;
   frameIndexList[1] = _inFrameIndex + procData.iteration + 1;
   SetInputFrames(0, numberOfInputFrames, frameIndexList);
   SetOutputFrames(0, 1, frameIndexList); // output first frame only
	// No release because modify-in-place is ON

   return 0;
}
///////////////////////////////////////////////////////////////////////////////

int CTrackingProc::DoProcess(SToolProcessingData &procData)
{
   CAutoErrorReporter autoErr("CTrackingProc::DoProcess",
      AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   try
	{
		bool isFirstPass = procData.iteration == 0;
		bool isLastPass = procData.iteration == (_iterationCount - 1);

		if (!isLastPass)
		{
			CToolFrameBuffer *inToolFrameBufferB = GetRequestedInputFrame(0, 0);
			CToolFrameBuffer *inToolFrameBufferN = GetRequestedInputFrame(0, 1);
			_BaseOrigImage                       = inToolFrameBufferB->GetVisibleFrameBufferPtr();
			_NextOrigImage                       = inToolFrameBufferN->GetVisibleFrameBufferPtr();

			if (_BaseOrigImage == NULL || _NextOrigImage == NULL)
			{
				autoErr.errorCode = TOOL_ERROR_FRAME_IS_MISSING;
				autoErr.msg << "Tracking aborted due to missing frame   ";
				return TOOL_ERROR_FRAME_IS_MISSING;
			}

			int retVal = TrackNextFrame(inToolFrameBufferB->GetFrameIndex());
			if (retVal && isFirstPass)
			{
				TRACE_0(errout << "Warning: one or more tracking boxes were invalid and were ignored by TrackNextFrame()");
			}
		}
	}
   catch (const std::exception &ex)
   {
      TRACE_0(errout << "TrackNextFrame failed " << ex.what());
      return  (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_ERROR);
   }
   catch (...)
   {
		TRACE_0(errout << "Unknown error in TrackNextFrame");
      return  (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_ERROR);
   }

   return 0;
}
///////////////////////////////////////////////////////////////////////////////

int CTrackingProc::EndIteration(SToolProcessingData &procData)
{
   // Update the status
   IToolProgressMonitor *progressMonitor = GetToolProgressMonitor();
   if (progressMonitor != NULL)
   {
     progressMonitor->BumpProgress();
   }

   return 0;
}
///////////////////////////////////////////////////////////////////////////////

int CTrackingProc::EndProcessing(SToolProcessingData &procData)
{
   // Stop the tracking threads
   if (!_TrackingThreadParametersArray.empty())
   {
      _TrackingThreadsDoneSync.SetCount(_TrackingBoxSubimagesArray.size());
      for (auto &subimagesEntry : _TrackingBoxSubimagesArray)
      {
         auto tag = subimagesEntry.first;
         _TrackingThreadParametersArray[tag].doneFlag = true;
         _TrackingThreadParametersArray[tag].startSync.ReleaseOne();
      }

      _TrackingThreadsDoneSync.Wait(_bthreadStructs, __func__, __LINE__);

      _TrackingThreadParametersArray.clear();
   }

   // DEBUGGING!
   //GetSystemAPI()->exitPreviewHackMode();

   delete [] _MonoImages[0];
   _MonoImages[0] = NULL;
   delete [] _MonoImages[1];
   _MonoImages[1] = NULL;

   _MonoImageIndex = -1;

   _TrackingBoxSubimagesArray.clear();

   // Did tracking run to completion or was it aborted?
	if (procData.iteration == _iterationCount)
   {
		_trkPtsEditor->notifyTrackingComplete();
	}
	else
	{
		_trkPtsEditor->notifyTrackingStopped();
	}

   IToolProgressMonitor *progressMonitor = GetToolProgressMonitor();
   if (progressMonitor != NULL)
   {
//      progressMonitor->SetStatusMessage("Tracking complete");
		progressMonitor->StopProgress(procData.iteration == _iterationCount);
   }

   return 0;
}
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// Tracking routines, should be moved to own algorithm
//
//
//// This function extracts an image with center at point tp,
// TrackingBoxPosition CTrackingProc::FindBestFirstMatch(const TrackingBoxPosition &tp,
//                                                 Pixel *trackBoxData,
//                                                 Pixel *searchBoxData)
//{
//   TrackingBoxPosition tpR(0,0);
//   int tbex = _TrackBoxExtentX;
//   int tbey = _TrackBoxExtentY;
//   int sbel = _SearchBoxLeftExtent;
//   int sber = _SearchBoxRightExtent;
//   int sbet = _SearchBoxTopExtent;
//   int sbeb = _SearchBoxBottomExtent;
//
//   CopySubImage(_BaseMonoImage, nPicCol, nPicRow, tp.x-tbex, tp.y-tbey, tbex*2, tbey*2, trackBoxData);
//   CopySubImage(_NextMonoImage, nPicCol, nPicRow, tp.x-sbel, tp.y-sbet, sbel+sber, sbet+sbeb, searchBoxData);
//
//   // The largest this can be is
//   double minL1 = _TrackBoxPoints * 65536.0;
//
//   // We first start tracking the upper left corner which is (-TrackBoxExtent, -TrackBoxExtent)
//   // from the actual track point
//   for (int i = -sbel; i <= sber - 2*tbex; i++)
//     for (int j = -sbet; j <= sbeb - 2*tbey; j++)
//       {
//         double L1 = ComputeL1BoxNorm(trackBoxData, _TrackBoxSizeX, _TrackBoxSizeY, searchBoxData, _SearchBoxSizeX, i+sbel, j+sbet);
//         if (L1 < minL1)
//           {
//              minL1 = L1;
//              // Restore the proper track box extent
//              tpR.x = tp.x + i + tbex;
//              tpR.y = tp.y + j + tbey;
//           }
//       }
//
//   return tpR;
//}

FPOINT CTrackingProc::FindBestMatch(FPOINT fp, Pixel *trackBoxData, Pixel *searchBoxData) const
{
   FPOINT tpR;
   int tbex = _TrackBoxExtentX;
   int tbey = _TrackBoxExtentY;
   int sbel = _SearchBoxLeftExtent;
   int sber = _SearchBoxRightExtent;
   int sbet = _SearchBoxTopExtent;
   int sbeb = _SearchBoxBottomExtent;

   CopySubImage(_BaseMonoImage, nPicCol, nPicRow, fp.x-tbex, fp.y-tbey, tbex*2, tbey*2, trackBoxData);
   CopySubImage(_NextMonoImage, nPicCol, nPicRow, fp.x-sbel, fp.y-sbet, sbel+sber, sbet+sbeb, searchBoxData);

   // The largest this can be is
	double minL1 = _TrackBoxPoints * 65536.0;
	int Nx = sbel+sber - 2*tbex + 1;
   int Ny = sbet+sbeb - 2*tbey + 1;
   int iX, iY;
#ifdef _PARABOLIC_SUBPIXEL_ESTIMATOR
   double *DL1 = new double[Nx*Ny];
#endif

   // We first start tracking the upper left corner which is (-TrackBoxExtent, -TrackBoxExtent)
   // from the actual track point
   for (int i = -sbel; i <= sber - 2*tbex; i++)
     for (int j = -sbet; j <= sbeb - 2*tbey; j++)
       {
         double L1 = ComputeL1BoxNorm(trackBoxData, _TrackBoxSizeX, _TrackBoxSizeY, searchBoxData, _SearchBoxSizeX, i+sbel, j+sbet);
#ifdef _PARABOLIC_SUBPIXEL_ESTIMATOR
		 DL1[(j+sbet)*Nx + i+sbel] = L1;
#endif
         if (L1 < minL1)
           {
              minL1 = L1;
              // Restore the proper track box extent
              iX = i + sbel;
              iY = j + sbet;
              float x = fp.x + i + tbex;
              float y = fp.y + j + tbey;
              tpR = FPOINT(x, y);
           }
       }

   // Note: if j is at the end, then there is a major problem with the tracking
   // region too small, so just punt
   if ((iX == 0) || (iX == (sbel+sber-2*tbex))) return tpR;
   if ((iY == 0) || (iY == (sbet+sbeb-2*tbey))) return tpR;

   // Find the subpixel resolution
#ifdef __TEST_CODE__
   FPOINT tpRa;
//#ifdef _PARABOLIC_SUBPIXEL_ESTIMATOR
   double D[9];
   double xdelta;
   double ydelta;

   for (int i=-1; i <= 1; i++)
     for (int j=-1; j <= 1; j++)
         D[(j+1)*3 + i+1] = DL1[(iY+j)*Nx + iX +i];

   EstimateMinFrom9Points(D, xdelta, ydelta);

   // xdelta and ydelta should not be larger than 1.5, if so, the interpolation is
   // wrong. So just use this value
   if (xdelta < -1.5) xdelta = -1.5;
   if (xdelta > 1.5) xdelta = 1.5;
   if (ydelta < -1.5) ydelta = -1.5;
   if (ydelta > 1.5) ydelta = 1.5;

   //tpR.x = tpR.x + xdelta;
   //tpR.y = tpR.y + ydelta;
   tpRa.x = tpR.x + xdelta;
   tpRa.y = tpR.y + ydelta;
//#else
   FPOINT tpRb;
   FPOINT tpRc;
   FPOINT tpRd;
   tpRb =  FindBestRefinedMatch8x(fp, tpR, true);
   tpRc =  FindBestRefinedMatch8x(fp, tpR, false);
   tpRd =  FindBestRefinedMatch10x(fp, tpR, true);
   tpR  =  FindBestRefinedMatch10x(fp, tpR, false);

   MTIostringstream osa, osb, osc, osd, ose, os;
   osa << "  PE: " << setfill(' ') << setw(8) << tpRa;
   osb << "  8P: " << setfill(' ') << setw(8) << tpRb;
   osc << "  8X: " << setfill(' ') << setw(8) << tpRc;
   osd << "  10P:" << setfill(' ') << setw(8) << tpRd;
   ose << "  10X:" << setfill(' ') << setw(8) << tpR;
   static foo = 0;

   os << (foo % 7) << setfill('0') << setw(3) << (foo/7) << setfill(' ')
      << osa.str() << osb.str() << osc.str() << osd.str() << ose.str();

   TRACE_0(errout << os.str());
   ++foo;

//#endif
#else
#if 0
      tpR =  FindBestRefinedMatch10x(fp, tpR);
#else

   int trackBoxSize = (tbex + tbey)/2;
   // TEST CODE trackBoxSize = ((sber - tbex) + (sbeb - tbey))/2;
   if (trackBoxSize <= 5)
      tpR =  FindBestRefinedMatch10x(fp, tpR, true, trackBoxData, searchBoxData);
   else if (trackBoxSize <= 10)
      tpR =  FindBestRefinedMatch8x(fp, tpR, true, trackBoxData, searchBoxData);
   else if (trackBoxSize <= 15)
      tpR =  FindBestRefinedMatch7x(fp, tpR, true, trackBoxData, searchBoxData);
   else if (trackBoxSize <= 20)
      tpR =  FindBestRefinedMatch6x(fp, tpR, true, trackBoxData, searchBoxData);
   else if (trackBoxSize <= 25)
      tpR =  FindBestRefinedMatch5x(fp, tpR, true, trackBoxData, searchBoxData);
   else
      tpR =  FindBestRefinedMatch4x(fp, tpR, true, trackBoxData, searchBoxData);
#endif

#endif // TEST_CODE

#ifdef _PARABOLIC_SUBPIXEL_ESTIMATOR
   delete DL1;
#endif
   return tpR;
}

FPOINT CTrackingProc::FindBestMatchX(FPOINT fp, Pixel *trackBoxData, Pixel *searchBoxData) const
{
   FPOINT tpR;
   int tbex = _TrackBoxExtentX;
   int tbey = _TrackBoxExtentY;
   int sbel = _SearchBoxLeftExtent;
   int sber = _SearchBoxRightExtent;
   int sbet = _SearchBoxTopExtent;
   int sbeb = _SearchBoxBottomExtent;

   CopySubImage(_BaseMonoImage, nPicCol, nPicRow, fp.x-tbex, fp.y-tbey, tbex*2, tbey*2, trackBoxData);
   CopySubImage(_NextMonoImage, nPicCol, nPicRow, fp.x-sbel, fp.y-sbet, sbel+sber, sbet+sbeb, searchBoxData);

   // The largest the L1 box norm can be is
	double minL1 = _TrackBoxPoints * 65536.0;

   // We move the tracking box around in the search range computing the L1 norm for each
   // point.  Thus the points start at 0 move Nx points (The +1 is because the center pixel of the
   // tracking point is not part of the extent.   Same is true of Ny.
   int Nx = sbel + sber - 2*tbex + 1;
   int Ny = sbet + sbeb - 2*tbey + 1;
   int iX, iY;
#ifdef _PARABOLIC_SUBPIXEL_ESTIMATOR
   double *DL1 = new double[Nx*Ny];
#error DL1 size is not correct and may result in access violation
#endif

   // We first start tracking at at the top row
   // from the actual track point
   for (int i = -sbel; i <= sber - 2*tbex; i++)
	   {
		 double L1 = ComputeL1BoxNorm(trackBoxData, _TrackBoxSizeX, _TrackBoxSizeY, searchBoxData, _SearchBoxSizeX, i+sbel, sbet);
#ifdef _PARABOLIC_SUBPIXEL_ESTIMATOR
		 DL1[sbet*Nx + i+sbel] = L1;
#endif
         if (L1 < minL1)
           {
              minL1 = L1;
              // Restore the proper track box extent
              iX = i + sbel;
#ifdef _PARABOLIC_SUBPIXEL_ESTIMATOR
              iY = sbet;
#endif
              float x = fp.x + i + tbex;
              float y = fp.y;
              tpR = FPOINT(x, y);
           }
       }

   // Note: if j is at the end, then there is a major problem with the tracking
   // region too small, so just punt
   if ((iX == 0) || (iX == (sbel+sber-2*tbex))) return tpR;

   // Find the subpixel resolution
#ifdef _PARABOLIC_SUBPIXEL_ESTIMATOR
   double D[9];
   double xdelta;
   double ydelta;

   for (int i=-1; i <= 1; i++)
     for (int j=-1; j <= 1; j++)
         D[(j+1)*3 + i+1] = DL1[iY*Nx + iX +i];

   EstimateMinFrom9Points(D, xdelta, ydelta);

   tpR.x = tpR.x + xdelta;
   tpR.y = fp.y;
#else

   tpR =  FindBestRefinedMatch4x(fp, tpR, false, trackBoxData, searchBoxData);

#endif

#ifdef _PARABOLIC_SUBPIXEL_ESTIMATOR
   delete[] DL1;
#endif
   return tpR;
}

FPOINT CTrackingProc::FindBestMatchY(FPOINT fp, Pixel *trackBoxData, Pixel *searchBoxData) const
{
   FPOINT tpR;
   int tbex = _TrackBoxExtentX;
   int tbey = _TrackBoxExtentY;
   int sbel = _SearchBoxLeftExtent;
   int sber = _SearchBoxRightExtent;
   int sbet = _SearchBoxTopExtent;
   int sbeb = _SearchBoxBottomExtent;

   CopySubImage(_BaseMonoImage, nPicCol, nPicRow, fp.x-tbex, fp.y-tbey, tbex*2, tbey*2, trackBoxData);
   CopySubImage(_NextMonoImage, nPicCol, nPicRow, fp.x-sbel, fp.y-sbet, sbel+sber, sbet+sbeb, searchBoxData);

   // The largest this can be is
	double minL1 = _TrackBoxPoints * 65536.0;
   int Nx = sbel + sber - 2*tbex + 1;
   int Ny = sbet + sbeb - 2*tbey + 1;
   int iX, iY;

#ifdef _PARABOLIC_SUBPIXEL_ESTIMATOR
   double *DL1 = new double[Nx*Ny];
#endif
   // We first start tracking the upper left corner which is (-TrackBoxExtent, -TrackBoxExtent)
   // from the actual track point
     for (int j = -sbel; j <= sber - 2*tbex; j++)
       {
         double L1 = ComputeL1BoxNorm(trackBoxData, _TrackBoxSizeX, _TrackBoxSizeY, searchBoxData, _SearchBoxSizeX, sbel, j+sbet);
#ifdef _PARABOLIC_SUBPIXEL_ESTIMATOR
		 DL1[(j+sbet)*Nx + sbel] = L1;
#endif
         if (L1 < minL1)
           {
              minL1 = L1;
              // Restore the proper track box extent
#ifdef _PARABOLIC_SUBPIXEL_ESTIMATOR
              iX = sbel;
#endif
              iY = j + sbet;
              float y = fp.y + j + tbey;
              float x = fp.x;
              tpR = FPOINT(x, y);
           }
       }

   // Note: if j is at the end, then there is a major problem with the tracking
   // region too small, so just punt
   if ((iY == 0) || (iY == (sbet+sbeb-2*tbey))) return tpR;

   // Find the subpixel resolution
#ifdef _PARABOLIC_SUBPIXEL_ESTIMATOR
   double D[9];
   double xdelta;
   double ydelta;

   for (int i=-1; i <= 1; i++)
     for (int j=-1; j <= 1; j++)
         D[(j+1)*3 + i+1] = DL1[(iY+j)*Nx + iX];

   EstimateMinFrom9Points(D, xdelta, ydelta);

   tpR.x = fp.x;
   tpR.y = tpR.y + ydelta;
#else

   tpR =  FindBestRefinedMatch4x(fp, tpR, false, trackBoxData, searchBoxData);

#endif

#ifdef _PARABOLIC_SUBPIXEL_ESTIMATOR
   delete[] DL1;
#endif
   return tpR;
}




FPOINT CTrackingProc::FindBestRefinedMatchNx(int SCALE,
                                                  CreateSubPixelMatrixT CreateSubPixelMatrixNx,
                                                  FPOINT dpB,
                                                  FPOINT dpN,
                                                  bool doParabolicRefinement,
                                                  Pixel *trackBoxData,
                                                  Pixel *searchBoxData) const
{
   FPOINT tpR(dpB);
   int tbex = _TrackBoxExtentX;
   int tbey = _TrackBoxExtentY;
   int sbel = _SearchBoxLeftExtent;
   int sber = _SearchBoxRightExtent;
   int sbet = _SearchBoxTopExtent;
   int sbeb = _SearchBoxBottomExtent;

   int BSearchBoxExtentX = tbex + 1;
   int BSearchBoxExtentY = tbey + 1;
   int nBaseX = SCALE*_TrackBoxSizeX;
   int nBaseY = SCALE*_TrackBoxSizeY;
   int nNextX = SCALE*(2*BSearchBoxExtentX+1);
   int nNextY = SCALE*(2*BSearchBoxExtentY+1);
   int iF = -SCALE;
   int jF = -SCALE;

   // Allocate some storage  -- should do this only once
   MTI_UINT16 *pBase = new MTI_UINT16[nBaseX*nBaseY];
   MTI_UINT16 *pNext = new MTI_UINT16[nNextX*nNextY];

   CopySubImage(_BaseMonoImage, nPicCol, nPicRow,
                                dpB.x-(tbex+1), dpB.y-(tbey+1),
                                2*tbex+2, 2*tbey+2, trackBoxData);
   CopySubImage(_NextMonoImage, nPicCol, nPicRow,
                                dpN.x-(sbel+1), dpN.y-(sbet+1),
                                sbel+sber+2, sbet+sbeb+2, searchBoxData);

   (* CreateSubPixelMatrixNx)(trackBoxData, 2*(tbex+1)+1, tbex+1, tbey+1,
                              tbex, tbey, pBase);
   (* CreateSubPixelMatrixNx)(searchBoxData, sbel+sber+3, sbel+1, sbet+1,
                              BSearchBoxExtentX, BSearchBoxExtentY, pNext);

   double DL1[(MAX_REFINED_MATCH_SCALE*2+1)*(MAX_REFINED_MATCH_SCALE*2+1)];

   // NOTE: This is not exactly the right way to search since we are searching around the
   // center pixel and not the min position.  However, let us assume that not much
   // differes
	double minL1 = 65536.0 * (SCALE*2) * (SCALE*2) * BSearchBoxExtentX * BSearchBoxExtentY;
   for (int i = -SCALE; i <= SCALE; i++)
     for (int j = -SCALE; j <= SCALE; j++)
       {
         double L1 = ComputeL1BoxNorm(pBase, nBaseX, nBaseY, pNext, nNextX, i+SCALE, j+SCALE);
         DL1[(j+SCALE)*(SCALE*2+1)+i+SCALE] = L1;
         if (L1 < minL1)
           {
              minL1 = L1;
              iF = i;
              jF = j;
           }
       }
   // Note: if j is at the end, then there is a major problem with the tracking
   // region too small, so just punt
//   if ((iF == -SCALE) || (iF == SCALE))
//      return tpR;
//   if ((jF == -SCALE) || (jF == SCALE))
//      return tpR;
   if ((iF == -SCALE || iF == SCALE)
   &&  (jF == -SCALE || jF == SCALE))
   {
      return tpR;
   }

   double xdelta = 0.0;
   double ydelta = 0.0;

   if (doParabolicRefinement)
     {
       double D[9];
       for (int i=-1; i <= 1; i++)
         for (int j=-1; j <= 1; j++)
             D[(j+1)*3 + i+1] = DL1[(jF+SCALE+j)*(SCALE*2+1) + iF+SCALE+i];

       EstimateMinFrom9Points(D, xdelta, ydelta);

       // xdelta and ydelta should not be larger than 1.5, if so, the interpolation is
       // wrong. So just use this value
       if (xdelta < -1.5)
           xdelta = -1.5;
       if (xdelta > 1.5)
           xdelta = 1.5;
       if (ydelta < -1.5)
           ydelta = -1.5;
       if (ydelta > 1.5)
           ydelta = 1.5;
     }

   //  NOTE: the iF/SCALE is relative movement of the center pixel, assign that
   // movement to the minimum pixel.
   float x = dpN.x + (iF+xdelta)/(float) SCALE;
   float y = dpN.y + (jF+ydelta)/(float) SCALE;
   tpR = FPOINT(x, y);

   delete[] pBase;
   delete[] pNext;

   return tpR;
}

//---------------------------------------------------------------------------

FPOINT CTrackingProc::FindBestRefinedMatch2x(FPOINT dpB,
                                                  FPOINT dpN,
                                                  bool doParabolicRefinement,
                                                  Pixel *trackBoxData,
                                                  Pixel *searchBoxData) const
{
   return FindBestRefinedMatchNx(2, CreateSubPixelMatrix2x, dpB, dpN,
                                 doParabolicRefinement,
                                 trackBoxData, searchBoxData);
}
//---------------------------------------------------------------------------

FPOINT CTrackingProc::FindBestRefinedMatch4x(FPOINT dpB,
                                             FPOINT dpN,
                                             bool doParabolicRefinement,
                                             Pixel *trackBoxData,
                                             Pixel *searchBoxData) const
{
   return FindBestRefinedMatchNx(4, CreateSubPixelMatrix4x, dpB, dpN,
                                 doParabolicRefinement,
                                 trackBoxData, searchBoxData);
}
//---------------------------------------------------------------------------

FPOINT CTrackingProc::FindBestRefinedMatch5x(FPOINT dpB,
                                             FPOINT dpN,
                                             bool doParabolicRefinement,
                                             Pixel *trackBoxData,
                                             Pixel *searchBoxData) const
{
   return FindBestRefinedMatchNx(5, CreateSubPixelMatrix5x, dpB, dpN,
                                 doParabolicRefinement,
                                 trackBoxData, searchBoxData);
}
//---------------------------------------------------------------------------

FPOINT CTrackingProc::FindBestRefinedMatch6x(FPOINT dpB,
                                             FPOINT dpN,
                                             bool doParabolicRefinement,
                                             Pixel *trackBoxData,
                                             Pixel *searchBoxData) const
{
   return FindBestRefinedMatchNx(6, CreateSubPixelMatrix6x, dpB, dpN,
                                 doParabolicRefinement,
                                 trackBoxData, searchBoxData);
}
//---------------------------------------------------------------------------

FPOINT CTrackingProc::FindBestRefinedMatch7x(FPOINT dpB,
                                             FPOINT dpN,
                                             bool doParabolicRefinement,
                                             Pixel *trackBoxData,
                                             Pixel *searchBoxData) const
{
   return FindBestRefinedMatchNx(7, CreateSubPixelMatrix7x, dpB, dpN,
                                 doParabolicRefinement,
                                 trackBoxData, searchBoxData);
}
//---------------------------------------------------------------------------

FPOINT CTrackingProc::FindBestRefinedMatch8x(FPOINT dpB,
                                             FPOINT dpN,
                                             bool doParabolicRefinement,
                                             Pixel *trackBoxData,
                                             Pixel *searchBoxData) const
{
   return FindBestRefinedMatchNx(8, CreateSubPixelMatrix8x, dpB, dpN,
                                 doParabolicRefinement,
                                 trackBoxData, searchBoxData);
}
//---------------------------------------------------------------------------

FPOINT CTrackingProc::FindBestRefinedMatch10x(FPOINT dpB,
                                              FPOINT dpN,
                                              bool doParabolicRefinement,
                                              Pixel *trackBoxData,
                                              Pixel *searchBoxData) const
{
   return FindBestRefinedMatchNx(10, CreateSubPixelMatrix10x, dpB, dpN,
                                 doParabolicRefinement,
                                 trackBoxData, searchBoxData);
}
//---------------------------------------------------------------------------


///////////////////////////////////////////////////////////////////////////////
// TRACKING THREADS - Heavy lifting done here
///////////////////////////////////////////////////////////////////////////////

/* static */
void CTrackingProc::TrackingThreadController(void *vpAppData, void *vpReserved)
{
   // Arg is pointer to param struct
   STrackingThreadParams *params = reinterpret_cast<STrackingThreadParams*>(vpAppData);

   vector<void *> bthreads;
   bthreads.push_back(vpReserved);
   BThreadBegin(vpReserved);
   while (true)
   {
      params->startSync.SetCount(1);
      params->startSync.Wait(bthreads, __func__, __LINE__);
      if (params->doneFlag)
      {
         params->doneSync->ReleaseOne();
         break; // from while(true)
      }
      try // Paranoia
      {
			params->trackingProc->TrackOnePointInNextFrame(params->currentFrame, params->iTag);
      }
      catch (...)
      {
			TRACE_0(errout << "TrackingThreadController: " << "Exception from TrackOnePointInNextFrame");
      }

      params->doneSync->ReleaseOne();
   }
}
//---------------------------------------------------------------------------

namespace
{
   void Make16BitMonochromeImageUsingLut(MTI_UINT16 *in, int nCol, int nRow, MTI_UINT16 lut[], MTI_UINT16 *out)
   {
      MTI_UINT16 *rLut = lut;
      MTI_UINT16 *gLut = lut + 65536;
      MTI_UINT16 *bLut = lut + (65536 * 2);
      for (int i = 0; i < (nRow * nCol * 3); i += 3)
      {
         // Weights are baked in!
         *out++ = rLut[in[i + 0]] + gLut[in[i + 1]] + bLut[in[i + 2]];
      }
   }

   void Make8BitMonochromeImageUsingLut(MTI_UINT16 *in, int nCol, int nRow, MTI_UINT16 lut[], MTI_UINT8 *out)
   {
      MTI_UINT16 *rLut = lut;
      MTI_UINT16 *gLut = lut + 65536;
      MTI_UINT16 *bLut = lut + (65536 * 2);
      for (int i = 0; i < (nRow * nCol * 3); i += 3)
      {
         // Weights are baked in!
         *out++ = (rLut[in[i + 0]] >> 8) + (gLut[in[i + 1]] >> 8) + (bLut[in[i + 2]] >> 8);
      }
   }

#define SURFACE_BLUR_RADIUS 3
#define SURFACE_BLUR_THRESHOLD 0.1
   void SurfaceBlur(MTI_UINT16 *image, int width, int height, int depth, MTI_UINT16 *out)
   {
      int threshold = max<int>(1, int((1 << depth) * SURFACE_BLUR_THRESHOLD));
      for (int y = SURFACE_BLUR_RADIUS; y < (height - SURFACE_BLUR_RADIUS); ++y)
      {
         for (int x = SURFACE_BLUR_RADIUS; x < (width - SURFACE_BLUR_RADIUS); ++x)
         {
            int xy = (y * width) + x;

            int radius = 1;
            int count = 9;
            int sum = 0;
            for (int yOffset = -(width * radius);
                  yOffset <= (width * radius);
                  yOffset += width)
            {
               for (int xOffset = -radius; xOffset <= radius; ++xOffset)
               {
                  sum += image[xy + yOffset + xOffset];
               }
            }

            // use regression?
            int centerValue = sum / count;

            // Surface blur - only use surround pixels that are different by less
            // than the threshold.
            sum = 0;
            count = 0;
            radius = SURFACE_BLUR_RADIUS;
            for (int yOffset = -(width * radius);
                  yOffset <= (width * radius);
                  yOffset += width)
            {
               for (int xOffset = -radius; xOffset <= radius; ++xOffset)
               {
                  int value = image[xy + yOffset + xOffset];
                  int diff = centerValue - value;
                  diff = (diff < 0) ? -diff : diff;
                  if (diff <= threshold)
                  {
                     sum += value;
                     ++count;
                  }
               }
            }

            out[xy] = (count > 0) ? (sum / count) : image[xy];
         }
      }
   }
//
//   void SurfaceBlur8Bit(MTI_UINT8 *image, int width, int height, MTI_UINT8 *out)
//   {
//      int channels = 1;
//	   Ipp8uArray srcImage(height, width, channels, image);
//
//      int radius = 7;  // Really 'diameter' methinks!
//      Ipp8uArray dstImage = srcImage.applyBilateralFilter(radius);
//
//      return dstImage;
//   }

}
//---------------------------------------------------------------------------

int CTrackingProc::TrackNextFrame(int CurrentFrame)
{
  int NextFrame = CurrentFrame + 1;
//  _trackingArray->at(NextFrame).clear();
  int channel = _trackingArray->getChannel();
  MTIassert(channel >= -1 && channel <= 2);
  if (channel < -1 || channel > 2)
  {
    channel = -1;
  }

  MTI_UINT16* lutPtr = _trackingArray->getApplyLutFlag() ? _currentDisplayLut16 : nullptr;

  // See if this is the first Track, if so allocate the monochrome images
  if (_MonoImageIndex < 0)
    {
      _MonoImages[0] = new MTI_UINT16[nPicCol*nPicRow];
      _MonoImages[1] = new MTI_UINT16[nPicCol*nPicRow];
      _MonoImageIndex = 0;

      if (_trackingArray->getDegrainFlag())
      {
         _tempMonoImage = new MTI_UINT16[nPicCol*nPicRow];
         if (lutPtr)
         {
            Make16BitMonochromeImageUsingLut(_BaseOrigImage, nPicCol, nPicRow, lutPtr, _tempMonoImage);
         }
         else
         {
            MakeMonoChromeImage(_BaseOrigImage, nPicCol, nPicRow, _sourceDepth, (_RGB ? channel : 0), nullptr, _tempMonoImage);
         }

         SurfaceBlur(_tempMonoImage, nPicCol, nPicRow, _sourceDepth, _MonoImages[1]);
      }
      else
      {
         if (lutPtr)
         {
            Make16BitMonochromeImageUsingLut(_BaseOrigImage, nPicCol, nPicRow, lutPtr, _MonoImages[1]);
         }
         else
         {
            MakeMonoChromeImage(_BaseOrigImage, nPicCol, nPicRow, _sourceDepth, (_RGB ? channel : 0), nullptr, _MonoImages[1]);
         }
      }
    }

  // Now swap the monochrome images
  _MonoImageIndex = (_MonoImageIndex + 1) % 2;
  _BaseMonoImage = _MonoImages[_MonoImageIndex];
  _NextMonoImage = _MonoImages[(_MonoImageIndex + 1) % 2];
  if (lutPtr)
  {
     Make16BitMonochromeImageUsingLut(_NextOrigImage, nPicCol, nPicRow, lutPtr, _NextMonoImage);
  }
  else
  {
     MakeMonoChromeImage(_NextOrigImage, nPicCol, nPicRow, _sourceDepth, (_RGB ? channel : 0), nullptr, _NextMonoImage);
  }

   if (_trackingArray->getDegrainFlag())
   {
     if (lutPtr)
     {
        Make16BitMonochromeImageUsingLut(_NextOrigImage, nPicCol, nPicRow, lutPtr, _tempMonoImage);
     }
     else
     {
        MakeMonoChromeImage(_NextOrigImage, nPicCol, nPicRow, _sourceDepth, (_RGB ? channel : 0), nullptr, _tempMonoImage);
     }

      SurfaceBlur(_tempMonoImage, nPicCol, nPicRow, _sourceDepth, _NextMonoImage);
   }
   else
   {
     if (lutPtr)
     {
        Make16BitMonochromeImageUsingLut(_NextOrigImage, nPicCol, nPicRow, lutPtr, _NextMonoImage);
     }
     else
     {
        MakeMonoChromeImage(_NextOrigImage, nPicCol, nPicRow, _sourceDepth, (_RGB ? channel : 0), nullptr, _NextMonoImage);
     }
   }

	int retVal = 0;
	for (auto tag : _AnchorList)
	{
		int error = TrackOnePointInNextFrame(CurrentFrame, tag);
		if (error && !retVal)
		{
			retVal = error;
		}
	}

	return retVal;
}
//---------------------------------------------------------------------------

int CTrackingProc::TrackOnePointInNextFrame(int currentFrame, int tag)
{
   CAutoSpinLocker lock(TrackingThreadLock);

   auto nextFrame = currentFrame + 1;

   TRACE_3(errout << "BEFORE:" << endl << _trackingArray->toString());

	MTIassert(_trackingArray->isValidTrackingBoxTag(tag));
	if (!_trackingArray->isValidTrackingBoxTag(tag))
	{
		////throw std::runtime_error("Tried to track box with invalid tag.");
		return TOOL_ERROR_INVALID_TRACKING_BOX_TAG;
	}

	TrackingBox &trackingBox = _trackingArray->getTrackingBoxByTag(tag);

	if (!trackingBox.isValidAtFrame(currentFrame))
	{
		static int previousFailTag = -1;
		if (tag != previousFailTag)
		{
			previousFailTag = tag;
			TRACE_1(errout << "CORRUPT TRACKING BOX ignored: " << trackingBox);
		}

		////throw std::runtime_error("Tried to track from an invalid starting frame.");
		return TOOL_ERROR_CORRUPT_TRACKING_BOX;
	}

   if (trackingBox.isValidAtFrame(nextFrame))
   {
      // It was already tracked!
		return 0;
   }

   CTrackingProc *nonConstThis = const_cast<CTrackingProc *>(this);
   Pixel *&TrackBoxData = nonConstThis->_TrackingBoxSubimagesArray[tag]._TrackBoxData;
   Pixel *&SearchBoxData = nonConstThis->_TrackingBoxSubimagesArray[tag]._SearchBoxData;

   auto currentFrameTrackingPosition = trackingBox.getTrackingPositionAtFrame(currentFrame);
   auto currentFramePoint = currentFrameTrackingPosition.out;
   FPOINT nextFramePoint;

   {
      CAutoSpinUnlocker unlock(TrackingThreadLock);
      nextFramePoint = FindBestMatch(currentFramePoint, TrackBoxData, SearchBoxData);
   }

   trackingBox.addTrackPoint(nextFramePoint);

	TRACE_3(errout << "AFTER:" << endl << _trackingArray->toString());
	return 0;
}
//--------------------------------------------------------------------------

int CTrackingTool::onPreviewHackDraw(int frameIndex, MTI_UINT16 *frameBits, int frameBitsSizeInBytes)
{
   //DEBUGGING
//   const CImageFormat *imageFormat = getSystemAPI()->getVideoClipImageFormat();
//   int width = imageFormat->getPixelsPerLine();
//   int height = imageFormat->getLinesPerFrame();
//   MTI_UINT16 *tempImage = (MTI_UINT16 *) MTImalloc((frameBitsSizeInBytes + 2) / 3);
//   MTI_UINT16 *monoImage = (MTI_UINT16 *) MTImalloc((frameBitsSizeInBytes + 2) / 3);
//
//   MTI_UINT16 *pIn = frameBits;
//   MTI_UINT16 *pInStop = frameBits + (width * height * 3);
//   MTI_UINT16 *pOut = tempImage;
//   while (pIn < pInStop)
//    {
////       *pOut++ = (0.32 * pIn[0]) + (0.64 * pIn[1]) + (0.4 * pIn[2]);
//       *pOut++ = pIn[2];
//       pIn += 3;
//    }
//
//   SurfaceBlur(tempImage, width, height, imageFormat->getBitsPerComponent(), monoImage);
//
//   pIn = monoImage;
//   pInStop = monoImage + (width * height);
//   pOut = frameBits;
//   while (pIn < pInStop)
//    {
//       pOut[0] = pOut[1] = pOut[2] = *pIn++;
//       pOut += 3;
//    }
//
//   MTIfree(monoImage);
//   MTIfree(tempImage);

   return TOOL_RETURN_CODE_NO_ACTION;
}
//---------------------------------------------------------------------------
