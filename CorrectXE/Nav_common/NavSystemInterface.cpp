// NavSystemInterface.cpp: implementation of the CNavSystemInterface class.
//
//  This class provides an interface by which a Tool can call functions
//  in the Navigator Base System.  The purpose of this class is to implement
//  the interface defined by the Abstract Base Class CToolSystemInterface.
//
//  This class hides the details of the base system from the Tools that
//  use it.  Unfortunately the implentation of the CNavSystemInterface
//  class will tend to get ugly since it must connect with many internal parts
//  of the Navigator and its supporting classes.
//
/*
$Header: /usr/local/filmroot/Nav_common/source/NavSystemInterface.cpp,v 1.163.2.85 2009/11/01 00:41:55 tolks Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "NavSystemInterface.h"

#include "BaseTool.h"
#include "BaseToolUnit.h"
#include "BezierEditor.h"
#include "ClipAPI.h"
#include "Displayer.h"
#include "FileSweeper.h"
#include "FrameCompareTool.h"
#include "GOVTool.h"
#include "Magnifier.h"
#include "MaskTool.h"
#include "MaskToolRegions.h"
#include "MTIDialogs.h"
#include "MainWindowUnit.h"
#include "NavigatorTool.h"
#include "NavMainCommon.h"
#include "PixelRegions.h"
#include "PDL.h"
#include "PDLViewerManager.h"
#include "PixelRegions.h"
#include "Player.h"
#include "Provisional.h"
#include "ReticleTool.h"
#include "SaveRestore.h"
#include "ToolCommand.h"
#include "ToolManager.h"
#include "ToolMediaIO.h"
#include "TrackingTool.h"
#include "TransformEditor.h"
#include "UserGuideViewer.h"

#include <string.h>
#include <memory>
using std::string;
using std::make_unique;


//////////////////////////////////////////////////////////////////////
// Local Class & Data Type Definitions
//////////////////////////////////////////////////////////////////////

class CHistoryReviewList
{
public:
   CHistoryReviewList(CSaveRestore *newSaveRestoreEngine);
   ~CHistoryReviewList();

   int getReviewRegionCount();
   int goToNextReview(int frameIndex, const CVideoFrameList *videoFrameList);
   int goToPreviousReview(int frameIndex,
                          const CVideoFrameList *videoFrameList);

   void setReviewListInvalid();

   int getReviewFrame();

   int getAllReviewRegions(int frameIndex, CPixelRegionList &reviewRegion);
   int getCurrentReviewRegion(CPixelRegionList &reviewRegion);

   int saveForMarkAllReviewRegions();
   int saveForMarkCurrentReviewRegion();
   int markReviewRegions();
   int reviseAllGOVRegions();
   int reviseGOVRegions(int frameNumber);
   int discardGOVRevisions();

private:
   int buildReviewList(int frameIndex);
   void deleteReviewList();

private:
   struct SHistoryEntry
   {
      int saveNumber;
      CPixelRegionList *pixelRegion;
   };

   typedef vector<SHistoryEntry> ReviewList;

private:

   ReviewList reviewList;     // List of review regions.  List is ordered
                              // newest to oldest
   vector<int> markList;      // List of global save numbers for history entries
                              // that were selected and may be marked later in
                              // history.
   int reviewFrame;           // Frame index for current review list
                              // -1 indicates that there are no fixes
                              // or that an error occurred
   int reviewListIndex;       // Index into review list
                              // -1 indicates index has not been set
   bool reviewListInvalid;    // Flag that indicates that review list has
                              // become invalid, typically because additional
                              // saves or restores were done for the current
                              // frame
   CSaveRestore *saveRestoreEngine;
};

//////////////////////////////////////////////////////////////////////
// Static Variables
//////////////////////////////////////////////////////////////////////

TForm * CNavSystemInterface::activeWindow = 0;
bool CNavSystemInterface::stretchRectangleAvailable = false;
RECT CNavSystemInterface::imageStretchRectangle= { 0, 0, 0, 0};
POINT * CNavSystemInterface::imageLassoPointer = 0;
MTIKeyEventHandler CNavSystemInterface::keyDownEventHandler = 0;
MTIKeyEventHandler CNavSystemInterface::keyUpEventHandler = 0;
MTIMouseButtonEventHandler CNavSystemInterface::mouseButtonDownEventHandler = 0;
MTIMouseButtonEventHandler CNavSystemInterface::mouseButtonUpEventHandler = 0;
MTIMouseMoveEventHandler CNavSystemInterface::mouseMoveEventHandler = 0;
MTIMouseWheelEventHandler CNavSystemInterface::mouseWheelEventHandler = 0;
CSaveRestore* CNavSystemInterface::saveRestoreEngine = new CSaveRestore;
CHistoryReviewList* CNavSystemInterface::historyReviewList = 0;
CProvisional* CNavSystemInterface::provisional = 0;
CNavSystemInterface::EHistoryReviewStatus CNavSystemInterface::historyReviewStatus
                                                  = HISTORY_REVIEW_STATUS_NONE;
DWORD CNavSystemInterface::mainThreadId = 0;  // 0 is an invalid thread ID


// Stoopid hack to delete static variables so I don't get a boatload of
// CODEGUARD errors!
int CNavSystemInterface::refCount = 0;

CBezierEditor* CNavSystemInterface::navBezierEditor = 0;

CTransformEditor *CNavSystemInterface::navTransformEditor = 0;

// List of internal tool names and respective factory methods
static CToolManager::SInternalToolTable internalToolTable[] =
{
//    Tool Name         Factory Method
// ----------------------------------------------------------------------------
   { "PluginMother",  CBaseTool::MakeTool        },   // The Mother of Plugins
   { "Navigator",     CNavigatorTool::MakeTool   },   // Navigator Controls
   { "MediaReader",   CMediaReaderTool::MakeTool },   // Non-GUI Media Reader
   { "MediaWriter",   CMediaWriterTool::MakeTool },   // Non-GUI Media Writer
   { "Provisional",   CProvisionalTool::MakeTool },   // Non-GUI Provisional
   { "GOV",           CGOVTool::MakeTool         },   // Non-GUI GOV
	{ "Tracking",      CTrackingTool::MakeTool    },   // Non-GUI Tracking
	{ "FrameCompare",  FrameCompareTool::MakeTool }    // Non-GUI Frame Compare (wipe)
};

#define INTERNAL_TOOL_COUNT (sizeof(internalToolTable)/sizeof(CToolManager::SInternalToolTable))

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CNavSystemInterface::CNavSystemInterface()
{
   // Initialize stretch rectangle handling
   stretchRectangleAvailable = false;

   if (saveRestoreEngine == 0)
      saveRestoreEngine = new CSaveRestore;

   if (historyReviewList == 0)
      historyReviewList = new CHistoryReviewList(saveRestoreEngine);

   if (provisional == 0)
      {
      provisional = new CProvisional(saveRestoreEngine);
      CToolManager toolMgr;
      toolMgr.SetProvisional(provisional);
      }

   if (navTransformEditor == 0)
      {
      navTransformEditor = new CTransformEditor(mainWindow->GetDisplayer());
      }

   if (mainThreadId == 0)
      {
      mainThreadId = GetCurrentThreadId();
      }

   ++refCount;
}

CNavSystemInterface::~CNavSystemInterface()
{
   // I delete these static objects to reduce CODEGUARD crap
   // But since they are shared among all tools, I have to ref count
   // because each tool for some fricken dumb reason has its own system
   // interface, even though there are NO private instance variables
   // (that I can find amidst all the fricken bloat) -
   // ALL the data associated with the interface seems to be static/shared!

   if (--refCount <= 0)
   {
      delete saveRestoreEngine;
      saveRestoreEngine = 0;
      delete historyReviewList;
      historyReviewList = 0;
      delete provisional;
      provisional = 0;
      delete navTransformEditor;
      navTransformEditor = 0;
      delete navBezierEditor;
      navBezierEditor = 0;
   }
}

//////////////////////////////////////////////////////////////////////
// Stretch Box API
//////////////////////////////////////////////////////////////////////

int  CNavSystemInterface::setGraphicsColor(int col)
{
   return(mainWindow->GetDisplayer()->setGraphicsColor(col));
}

void CNavSystemInterface::setStretchRectangleColor(int col)
{
   mainWindow->GetDisplayer()->setStretchRectangleColor(col);
}

void CNavSystemInterface::startStretchRectangleOrLasso()
{
   stretchRectangleAvailable = false;

#ifdef __BORLANDC__
   // Capture the mouse cursor
   SetCaptureControl(activeWindow);
#endif // #ifdef __BORLANDC__

   // Call Clip Displayer's function to setup stretching, including providing
   // the Clip Display with a callback function that gets called after
   // the mouseUp function has been called.  The Rectangle coordinates
   // to be returned are going to be in Frame coordinates.
   mainWindow->GetDisplayer()->stretchRectangleFrame(stretchRectangleCallback,
                                                      this);
   // Call Clip Displayer's function to start drawing the stretch box
   mainWindow->GetDisplayer()->mouseDn();
}

void CNavSystemInterface::stopStretchRectangleOrLasso(bool dspfrm)
{
   // Call Clip Displayer's function to stop drawing the stretch box
   mainWindow->GetDisplayer()->mouseUp(dspfrm);

#ifdef __BORLANDC__
   // Release the mouse cursor capture
   ReleaseCapture();
#endif // #ifdef __BORLANDC__
}

void CNavSystemInterface::cancelStretchRectangle()
{
   // Call Clip Displayer's function to cancel stretch box operation
   mainWindow->GetDisplayer()->cancelStretchRectangle();

#ifdef __BORLANDC__
   // Release the mouse cursor capture
   ReleaseCapture();
#endif // #ifdef __BORLANDC__
}

int CNavSystemInterface::getStretchRectangleCoordinates(RECT &imageRect, POINT* &lasso)
{
   // If the stretch rectangle has been successfully completed, this function
   // copies the current stretch rectangle in image coordinates into the
   // caller's RECT structure and returns 0
   // If the stretch rectangle is not available (either because it is not
   // yet complete or was cancelled by the user) then this function returns
   // non-zero

   if (stretchRectangleAvailable)
      {
      // Stretch Rectangle data is available, so copy it to caller's RECT
      imageRect = imageStretchRectangle;
      lasso = imageLassoPointer;
      return 0;
      }
   else
      {
      // Stretch rectangle is not available
      return 1;
      }
}

RECT CNavSystemInterface::getStretchRectangleScreenCoords()
{
   if (!stretchRectangleAvailable)
   {
      RECT rect = {-1, -1, -1, -1};
      return rect;
   }

   return mainWindow->GetStretchRectangleScreenCoords();
}

void CNavSystemInterface::stretchRectangleCallback(void *obj, void* data1, void *data2)
{
   // This is a static "callback" function that is called by the Clip Display
   // after its mouseUp function has been called.  Note that this function
   // does NOT get called if the stretch rectangle was "cancelled" because
   // the user changed the stretch direction

   // Get a pointer to the CNavSystemInterface instance originally passed
   // in by startStretchRectangleOrLasso
   CNavSystemInterface *navAPI = static_cast<CNavSystemInterface*>(obj);
   if (navAPI == 0)
      {
      // ERROR: CNavSystemInterface pointer is not good
      return;  // Cannot do anything
      }

   // Get a pointer to the RECT data provided by the Clip Displayer
   RECT *newRect = static_cast<RECT*>(data1);
   if (newRect == 0)
      {
      // ERROR: RECT pointer is not good
      return;  // Cannot do anything
      }

   // Copy new RECT data to CNavSystemInterface instance
   navAPI->imageStretchRectangle = *newRect;

   // Get a pointer to the POINT array represenbting the lasso,
   // also provided by the Clip Displayer. Note that this ptr
   // will be nullptr if we're not in LASSO mode
   POINT *newLasso = static_cast<POINT*>(data2);

   // copy the pointer to the POINT data to CNavSystemInterface
   navAPI->imageLassoPointer = newLasso;

   // Set flag to indicate that new stretch rectangle data is available
   navAPI->stretchRectangleAvailable = true;
}

void CNavSystemInterface::zoom(const RECT &showRect)
{
   CDisplayer *displayer = mainWindow->GetDisplayer();
   CPlayer *player = mainWindow->GetPlayer();
   RECT tmpRect = showRect;
   POINT  *lasso;

   displayer->calculateZoomedReviewRectangle(&tmpRect);
   mainWindow->UpdateZoomLevelStatus();
   player->fullStop();  // force player to read frame from disk
   player->goToFrameSynchronous(getLastFrameIndex());
}

void CNavSystemInterface::unzoom()
{
   CDisplayer *displayer = mainWindow->GetDisplayer();
   CPlayer *player = mainWindow->GetPlayer();

   displayer->zoomAll();
   mainWindow->UpdateZoomLevelStatus();
   player->fullStop();  // force player to read frame from disk
   player->goToFrameSynchronous(getLastFrameIndex());
}

void CNavSystemInterface::setRGBMask(int rgbmsk)
{
   CDisplayer *displayer = mainWindow->GetDisplayer();

   displayer->setRGBChannelsMask(rgbmsk);
   mainWindow->UpdateRGBChannelsToolbarButtons();
}

int  CNavSystemInterface::getRGBMask()
{
   CDisplayer *displayer = mainWindow->GetDisplayer();

   return displayer->getRGBChannelsMask();
}

int CNavSystemInterface::getLastFrameIndex()
{
   return(mainWindow->GetPlayer()->getLastFrameIndex());
}

void CNavSystemInterface::drawRectangleFrame(RECT * rectToDraw)
{
   mainWindow->GetDisplayer()->drawRectangleFrame(rectToDraw);
}

void CNavSystemInterface::drawLassoFrame(POINT *lasso)
{
   mainWindow->GetDisplayer()->drawLassoFrame(lasso);
}

void CNavSystemInterface::drawBezierFrame(BEZIER_POINT *bezier)
{
   mainWindow->GetDisplayer()->drawBezierFrame(bezier);
}

void CNavSystemInterface::lockReader(bool lock)
{
   mainWindow->GetPlayer()->lockReader(lock);
}

bool CNavSystemInterface::getMousePositionFrame(int * newx, int * newy)
{
   CToolManager toolMgr;
   // Figure out if the most recent mouse position is within the
   // (possibly zoomed, etc) image display
   int windowX, windowY;
   toolMgr.GetLastMousePositionWindow(windowX, windowY);
   POINT point;
   point.x = windowX;
   point.y = windowY;
   mainWindow->GetDisplayer()->clipPointClient(&point);
   if (point.x != windowX || point.y != windowY)
      return false;

   // Get the most recent mouse position in image coordinates
   toolMgr.GetLastMousePositionImage(*newx, *newy);

   return true;
}

void CNavSystemInterface::clipRectangleFrame(RECT * rect)
{
  mainWindow->GetDisplayer()->clipRectangleFrame(rect);
}

void CNavSystemInterface::clipLassoFrame(POINT * lasso)
{
  mainWindow->GetDisplayer()->clipLassoFrame(lasso);
}

//////////////////////////////////////////////////////////////////////
// Onion Skin API

int CNavSystemInterface::getDisplayMode()
{
   return mainWindow->GetDisplayer()->getDisplayMode();
}

void CNavSystemInterface::setDisplayMode(int newmode)
{
   mainWindow->GetDisplayer()->setDisplayMode(newmode);
   mainWindow->UpdateViewingModeStatus();
}

void CNavSystemInterface::resetDisplayMode()
{
   mainWindow->GetDisplayer()->resetDisplayMode();
}

void CNavSystemInterface::enterPaintMode()
{
   mainWindow->GetDisplayer()->enterPaintMode();
}

void CNavSystemInterface::initPaintHistory()
{
   auto clip = mainWindow->GetCurrentClipHandler()->getCurrentClip();
   mainWindow->GetDisplayer()->
      initPaintHistory(clip);
}

void CNavSystemInterface::enableAutoAccept(bool onoff)
{
   mainWindow->GetDisplayer()->enableAutoAccept(onoff);
}

void CNavSystemInterface::initImportProxy(int wdth, int hght)
{
   mainWindow->GetDisplayer()->initImportProxy(wdth, hght);
}

void CNavSystemInterface::exitPaintMode()
{
   mainWindow->GetDisplayer()->exitPaintMode();
}

void CNavSystemInterface::executeNavigatorToolCommand(int command)
{
   mainWindow->ExecuteNavigatorToolCommand(command);
}

void CNavSystemInterface::displayFrameAndBrush()
{
   mainWindow->GetDisplayer()->displayFrameAndBrush();
}

void CNavSystemInterface::setCaptureMode(int newmode)
{
   mainWindow->GetDisplayer()->setCaptureMode(newmode);
}

int  CNavSystemInterface::getCaptureMode()
{
   return(mainWindow->GetDisplayer()->getCaptureMode());
}

void CNavSystemInterface::setImportMode(int newmode)
{
   mainWindow->GetDisplayer()->setImportMode(newmode);
}

int  CNavSystemInterface::getImportMode()
{
   return(mainWindow->GetDisplayer()->getImportMode());
}

int CNavSystemInterface::setImportFrameClipName(const string &clipName)
{
   return mainWindow->GetDisplayer()->setImportFrameClipName(clipName);
}

string CNavSystemInterface::getImportFrameClipName()
{
   return mainWindow->GetDisplayer()->getImportFrameClipName();
}

ClipSharedPtr CNavSystemInterface::getImportClip()
{
   auto clip = mainWindow->GetDisplayer()->getImportClip();

   return clip ? clip : getClip();
}

CVideoFrameList* CNavSystemInterface::getImportClipVideoFrameList()
{
   CVideoFrameList* videoFrameList = mainWindow->GetDisplayer()->getImportClipVideoFrameList();

   return videoFrameList ? videoFrameList : getVideoFrameList();
}

int  CNavSystemInterface::computeDesiredImportFrame(int trg)
{
   return(mainWindow->GetDisplayer()->computeDesiredImportFrame(trg));
}

void CNavSystemInterface::setImportFrameOffset(int impoff)
{
   mainWindow->GetDisplayer()->setImportFrameOffset(impoff);
}

int CNavSystemInterface::getImportFrameOffset()
{
   return mainWindow->GetDisplayer()->getImportFrameOffset();
}

int CNavSystemInterface::syncImportFrameTimecode()
{
   return mainWindow->GetDisplayer()->syncImportFrameTimecode() ? 0 : -1;
}

void CNavSystemInterface::preloadImportFrame(int impfrm,
                                             int impmod,
                                             double rot,
                                             double strx, double stry,
                                             double skew,
                                             double offx,
                                             double offy)
{
   mainWindow->GetDisplayer()->preloadImportFrame(impfrm, impmod,
                                                  rot, strx, stry,
                                                  skew, offx, offy);
}

void CNavSystemInterface::forceTargetAndImportFrameLoaded()
{
   mainWindow->GetDisplayer()->forceTargetAndImportFrameLoaded();
}

void CNavSystemInterface::loadTargetAndImportFrames(int trgfrm)
{
   mainWindow->GetDisplayer()->loadTargetAndImportFrames(trgfrm);
}

void CNavSystemInterface::setImportFrameTimecode(int impfrm)
{
   mainWindow->GetDisplayer()->setImportFrameTimecode(impfrm);
}

int  CNavSystemInterface::getImportFrameTimecode()
{
   return(mainWindow->GetDisplayer()->getImportFrameTimecode());
}

void CNavSystemInterface::clearPaintFrames()
{
   mainWindow->GetDisplayer()->clearPaintFrames();
}

bool CNavSystemInterface::getPaintFrames(int *trg, int *imp)
{
   return (mainWindow->GetDisplayer()->getPaintFrames(trg, imp));
}

void CNavSystemInterface::unloadTargetAndImportFrames()
{
   mainWindow->GetDisplayer()->unloadTargetAndImportFrames();
}

bool CNavSystemInterface::targetFrameIsModified()
{
   return(mainWindow->GetDisplayer()->targetFrameIsModified());
}

void CNavSystemInterface::enableForceTargetLoad(bool enbl)
{
   mainWindow->GetDisplayer()->enableForceTargetLoad(enbl);
}

void CNavSystemInterface::enableForceImportLoad(bool enbl)
{
   mainWindow->GetDisplayer()->enableForceImportLoad(enbl);
}

void CNavSystemInterface::loadTargetFrame(int target)
{
   mainWindow->GetDisplayer()->loadTargetFrame(target);
}

void CNavSystemInterface::setTargetMark()
{
   mainWindow->GetPlayer()->setMarkIn();
   mainWindow->GetTimeline()->UpdateMarks();
}

void CNavSystemInterface::setMarkIn(int nFrame)
{
   mainWindow->GetPlayer()->setMarkIn(nFrame);
   mainWindow->GetTimeline()->UpdateMarks();
}

void CNavSystemInterface::setMarkOut(int nFrame)
{
   mainWindow->GetPlayer()->setMarkOut(nFrame);
   mainWindow->GetTimeline()->UpdateMarks();
}

void CNavSystemInterface::reloadModifiedTargetFrame()
{
   mainWindow->GetDisplayer()->reloadModifiedTargetFrame();
}

void CNavSystemInterface::redrawTargetFrame()
{
   mainWindow->GetDisplayer()->redrawTargetFrame();
}

void CNavSystemInterface::reloadModifiedImportFrame()
{
   mainWindow->GetDisplayer()->reloadModifiedImportFrame();
}

void CNavSystemInterface::reloadUnmodifiedTargetFrame()
{
   mainWindow->GetDisplayer()->reloadUnmodifiedTargetFrame();
}

MTI_UINT16 * CNavSystemInterface::getTargetIntermediate()
{
   return(mainWindow->GetDisplayer()->getTargetIntermediate());
}

MTI_UINT16 * CNavSystemInterface::getTargetOriginalIntermediate()
{
   return(mainWindow->GetDisplayer()->getTargetOriginalIntermediate());
}

MTI_UINT16 * CNavSystemInterface::getTargetPreStrokeIntermediate()
{
   return(mainWindow->GetDisplayer()->getTargetPreStrokeIntermediate());
}

void CNavSystemInterface::setOnionSkinMode(int onionmode)
{
   mainWindow->GetDisplayer()->setOnionSkinMode(onionmode);
}

void CNavSystemInterface::setOnionSkinTargetWgt(double wgt)
{
   mainWindow->GetDisplayer()->setOnionSkinTargetWgt(wgt);
}

void CNavSystemInterface::editMask()
{
   mainWindow->GetDisplayer()->setMouseMinorState(EDIT_MASK);
}

void CNavSystemInterface::setMouseMinorState(int newstate)
{
   mainWindow->GetDisplayer()->setMouseMinorState(newstate);
}

void CNavSystemInterface::setColorPickSampleSize(int siz)
{
   mainWindow->GetDisplayer()->setColorPickSampleSize(siz);
}

void CNavSystemInterface::getColorPickSample(int x, int y,
                           int &cmax, float &r, float &g, float &b)
{
   mainWindow->GetDisplayer()->getColorPickSample(x, y, cmax, r, g, b);
}

void CNavSystemInterface::clearFillSeed()
{
   mainWindow->GetDisplayer()->clearFillSeed();
}

void CNavSystemInterface::setFillTolerance(int tol)
{
   mainWindow->GetDisplayer()->setFillTolerance(tol);
}

void CNavSystemInterface::setDensityStrength(int denstrng)
{
   mainWindow->GetDisplayer()->setDensityStrength(denstrng);
}

void CNavSystemInterface::setGrainStrength(int grstrng)
{
   mainWindow->GetDisplayer()->setGrainStrength(grstrng);
}

void CNavSystemInterface::setGrainPresets(GrainPresets *grainPresets)
{
   mainWindow->GetDisplayer()->setGrainPresets(grainPresets);
}

void CNavSystemInterface::recycleFillSeed()
{
   mainWindow->GetDisplayer()->recycleFillSeed();
}

void CNavSystemInterface::moveImportFrame(int delx, int dely)
{
   CTransformEditor *transformEditor = getTransformEditor();

   transformEditor->moveImportFrame(delx, dely);
}

void CNavSystemInterface::loadImportFrame(int impframe)
{
  mainWindow->GetDisplayer()->setImportFrameTimecode(impframe);
}

void CNavSystemInterface::setImportMark()
{
   mainWindow->GetPlayer()->setMarkOutExclusive();
   mainWindow->GetTimeline()->UpdateMarks();
}

void CNavSystemInterface::setMainWindowFocus()
{
   // Should consider intent (focus on main window vs get focus off some other window) QQQ
   mainWindow->FocusOnMainWindow();
}

TForm *CNavSystemInterface::getBaseToolForm()
{
    return BaseToolWindow;
}

void CNavSystemInterface::setMainWindowAutofocus(bool onoff)
{
   // HACK for Paint which for some reason wants main window to take focus
   // whenever the mouse moves into it, but that screws up other tools
   mainWindow->SetAutofocus(onoff);
}

void CNavSystemInterface::enableBrushDisplay(bool onoff)
{
   mainWindow->GetDisplayer()->enableBrushDisplay(onoff);
}

void CNavSystemInterface::loadPaintBrush(CBrush *newbrush, int newmode,
                                         int darkerthrsh, int lighterthrsh,
                                         float *colorcomp)
{
   mainWindow->GetDisplayer()->loadPaintBrush(newbrush, newmode,
                                              darkerthrsh, lighterthrsh,
                                              colorcomp);
}

CBrush *CNavSystemInterface::getPaintBrush(int *drkrthr, int *lgtrthr, bool *usingcol, float *colors)
{
   return mainWindow->GetDisplayer()->getPaintBrush(drkrthr, lgtrthr, usingcol, colors);
}

void CNavSystemInterface::loadCloneBrush(CBrush *newbrush, bool isRel,
                                         int x, int y)
{
   mainWindow->GetDisplayer()->loadCloneBrush(newbrush, isRel, x, y);
}

CBrush *CNavSystemInterface::getCloneBrush(bool *isrel, int *x, int *y)
{
   return mainWindow->GetDisplayer()->getCloneBrush(isrel, x, y);
}

void CNavSystemInterface::unloadPaintBrush()
{
   mainWindow->GetDisplayer()->unloadPaintBrush();
}

void CNavSystemInterface::unloadCloneBrush()
{
   mainWindow->GetDisplayer()->unloadCloneBrush();
}

void CNavSystemInterface::setAutoAlignBoxSize(int hlfsize)
{
   mainWindow->GetDisplayer()->setAutoAlignBoxSize(hlfsize);
}

void CNavSystemInterface::setDisplayProcessed(bool proc)
{
   mainWindow->GetDisplayer()->setDisplayProcessed(proc);
}

void CNavSystemInterface::mouseDn()
{
   mainWindow->GetDisplayer()->mouseDn();
}

void CNavSystemInterface::mouseMv(int x, int y)
{
   mainWindow->GetDisplayer()->mouseMv(x,y);
}

void CNavSystemInterface::mouseUp()
{
   mainWindow->GetDisplayer()->mouseUp();
}

CPixelRegionList& CNavSystemInterface::getOriginalPixels()
{
   return(mainWindow->GetDisplayer()->getOriginalPixels());
}

int CNavSystemInterface::getToolFrame(MTI_UINT16 *frm, int srcfrm)
{
   return(mainWindow->GetPlayer()->getToolFrame(frm, srcfrm));
}

int CNavSystemInterface::putToolFrame(MTI_UINT16 *frm, MTI_UINT16 *aux,
                                      CPixelRegionList& orgpels,
                                      int dstfrm)
{
   return(mainWindow->GetPlayer()->putToolFrame(frm, aux, orgpels, dstfrm));
}

int CNavSystemInterface::putToolFrame(MTI_UINT16 *inifrm, MTI_UINT16 *finfrm,
                                      MTI_UINT16 *aux,
                                      int dstfrm)
{
   return(mainWindow->GetPlayer()->putToolFrame(inifrm, finfrm, aux, dstfrm));
}

void CNavSystemInterface::makeStrokeStack()
{
   mainWindow->GetDisplayer()->makeStrokeStack();
}

void CNavSystemInterface::clearStrokeStack()
{
   mainWindow->GetDisplayer()->clearStrokeStack();
}

int CNavSystemInterface::pushStrokeFromTarget(int macrofileleng)
{
   return(mainWindow->GetDisplayer()->pushStrokeFromTarget(macrofileleng));
}

int CNavSystemInterface::popStrokesToTarget(int popnum)
{
   return(mainWindow->GetDisplayer()->popStrokesToTarget(popnum));
}

void CNavSystemInterface::completeMacroStroke()
{
   return(mainWindow->GetDisplayer()->completeMacroStroke());
}

void CNavSystemInterface::paintOneBrushPositionFrame(int x, int y)
{
   mainWindow->GetDisplayer()->paintOneBrushPositionFrame((float)x + 0.5, (float)y + 0.5);
}

void CNavSystemInterface::paintOneStrokeSegmentFrame(int xbeg, int ybeg,
                                                     int xend, int yend)
{
   mainWindow->GetDisplayer()->paintOneStrokeSegmentFrame(xbeg, ybeg, xend, yend);
}

int CNavSystemInterface::initMaskForInpainting()
{
    return -1;

   //return(mainWindow->GetDisplayer()->initMaskForInpainting());
}

int CNavSystemInterface::iterateOneInpaintCycle()
{
   return -1;

   //return(mainWindow->GetDisplayer()->iterateOneInpaintCycle());
}

int CNavSystemInterface::getMaxComponentValue()
{
   return  mainWindow->GetDisplayer()->getMaxComponentValue();
}

void CNavSystemInterface::forceMouseToWindowCenter()
{
   mainWindow->forceMouseToWindowCenter();
}

void CNavSystemInterface::useTrackingAlignOffsets(bool flag)
{
   mainWindow->GetDisplayer()->setUseTrackingAlignOffsets(flag);
}

void CNavSystemInterface::clearAllTrackingAlignOffsets()
{
   mainWindow->GetDisplayer()->clearAllTrackingAlignOffsets();
}

void CNavSystemInterface::setTrackingAlignOffsets(int frame, double xoff,
                                                         double yoff)
{
   mainWindow->GetDisplayer()->setTrackingAlignOffsets(frame, xoff, yoff);
}

void CNavSystemInterface::getTrackingAlignOffsets(int frame, double &xoff,
                                                           double &yoff)
{
	mainWindow->GetDisplayer()->getTrackingAlignOffsets(frame, xoff, yoff);
}

bool CNavSystemInterface::doesFrameHaveAnyHistory(int frame)
{
   int retVal;
   CSaveRestore *reviewHistory = mainWindow->GetPlayer()->getReviewHistory();
   CAutoHistoryOpener opener(*reviewHistory, FLUSH_TYPE_RESTORE,
                             frame, ALL_FIELDS, 0, 0, 0, retVal);
   return retVal == 0 && reviewHistory->nextRestoreRecord() == 0;
}

void CNavSystemInterface::setAltClipDiffThreshold(float newThreshold)
{
	mainWindow->GetDisplayer()->setAltClipDiffThreshold(newThreshold);
	if (!isDisplaying())
	{
		refreshFrameCached();
	}
}

void CNavSystemInterface::setOrigValDiffThreshold(float newThreshold)
{
	mainWindow->GetDisplayer()->setOrigValDiffThreshold(newThreshold);
	if (!isDisplaying())
	{
		refreshFrameCached();
	}
}

void CNavSystemInterface::setPaintToolChangesPending(bool flag)
{
	mainWindow->GetDisplayer()->setPaintToolChangesPending(flag);
}

bool CNavSystemInterface::arePaintToolChangesPending()
{
	return mainWindow->GetDisplayer()->arePaintToolChangesPending();
}


//////////////////////////////////////////////////////////////////////
// Registration API

void CNavSystemInterface::unloadRegistrationFrame()
{
   mainWindow->GetDisplayer()->unloadRegistrationFrame();
}

void CNavSystemInterface::loadRegistrationFrame(int registr)
{
   mainWindow->GetDisplayer()->loadRegistrationFrame(registr);
}

void CNavSystemInterface::clearLoadedRegistrationFrame()
{
   mainWindow->GetDisplayer()->clearLoadedRegistrationFrame();
}

void CNavSystemInterface::clearRegisteredRegistrationFrame()
{
   mainWindow->GetDisplayer()->clearRegisteredRegistrationFrame();
}

MTI_UINT16 *CNavSystemInterface::getPreRegisteredIntermediate()
{
   return mainWindow->GetDisplayer()->getPreRegisteredIntermediate();
}

MTI_UINT16 *CNavSystemInterface::getRegisteredIntermediate()
{
   return mainWindow->GetDisplayer()->getRegisteredIntermediate();
}

void CNavSystemInterface::displayFromRenderThread(MTI_UINT16 *frm, int frmindx)
{
   mainWindow->GetPlayer()->displayFromRenderThread(frm, frmindx);
}

void CNavSystemInterface::enterRegistrationMode()
{
   mainWindow->GetDisplayer()->enterRegistrationMode();
}

void CNavSystemInterface::exitRegistrationMode()
{
   mainWindow->GetDisplayer()->exitRegistrationMode();
}

//////////////////////////////////////////////////////////////////////
// DeWarp API

void CNavSystemInterface::getLastFrameAsIntermediate(MTI_UINT16 *intbuf)
{
   mainWindow->GetDisplayer()->getLastFrameAsIntermediate(intbuf);
}

//////////////////////////////////////////////////////////////////////
// Magnifier API

CMagnifier* CNavSystemInterface::allocateMagnifier(int wdth, int hght,
                                                   RECT *magwin)
{
  return(mainWindow->GetDisplayer()->allocateMagnifier(wdth, hght, magwin));
}

void CNavSystemInterface::freeMagnifier(CMagnifier *mag)
{
  mainWindow->GetDisplayer()->freeMagnifier(mag);
}

//////////////////////////////////////////////////////////////////////
// Accessors for Functions Pointers to the Main Application's
// Keyboard and Mouse Input Event Handlers.  Used to call the
// event handlers from the other GUI components
//////////////////////////////////////////////////////////////////////
MTIKeyEventHandler CNavSystemInterface::getKeyDownEventHandler()
{
   return keyDownEventHandler;
}

MTIKeyEventHandler CNavSystemInterface::getKeyUpEventHandler()
{
   return keyUpEventHandler;
}

MTIMouseButtonEventHandler CNavSystemInterface::getMouseButtonDownEventHandler()
{
   return mouseButtonDownEventHandler;
}

MTIMouseButtonEventHandler CNavSystemInterface::getMouseButtonUpEventHandler()
{
   return mouseButtonUpEventHandler;
}

MTIMouseMoveEventHandler CNavSystemInterface::getMouseMoveEventHandler()
{
   return mouseMoveEventHandler;
}

MTIMouseWheelEventHandler CNavSystemInterface::getMouseWheelEventHandler()
{
   return mouseWheelEventHandler;
}

//////////////////////////////////////////////////////////////////////
// Static Mutators for Functions Pointers to the Main Application's
// Keyboard and Mouse Input Event Handlers.  Called from Main Application
// on startup to set the pointers
//////////////////////////////////////////////////////////////////////
void CNavSystemInterface::setActiveWindow(TForm *newWindow)
{
   activeWindow = mainWindow;
 ////  activeWindow = newWindow;
}

TForm *CNavSystemInterface::getActiveWindow(void)
{
   return activeWindow;
}

void CNavSystemInterface::setKeyDownEventHandler(MTIKeyEventHandler newEventHandler)
{
   keyDownEventHandler = newEventHandler;
}

void CNavSystemInterface::setKeyUpEventHandler(MTIKeyEventHandler newEventHandler)
{
   keyUpEventHandler = newEventHandler;
}

void CNavSystemInterface::setMouseButtonDownEventHandler(MTIMouseButtonEventHandler newEventHandler)
{
   mouseButtonDownEventHandler = newEventHandler;
}

void CNavSystemInterface::setMouseButtonUpEventHandler(MTIMouseButtonEventHandler newEventHandler)
{
   mouseButtonUpEventHandler = newEventHandler;
}

void CNavSystemInterface::setMouseMoveEventHandler(MTIMouseMoveEventHandler newEventHandler)
{
   mouseMoveEventHandler = newEventHandler;
}

void CNavSystemInterface::setMouseWheelEventHandler(MTIMouseWheelEventHandler newEventHandler)
{
   mouseWheelEventHandler = newEventHandler;
}

//---------------------------------------------------------------------------

CToolSystemInterface* CNavSystemInterface::createNavSystemInterface()
{
   // Static function to create instances of CNavSystemInterface.
   // Used by Tool Manager to provide System API to the Tools it creates

   auto systemAPI = new CNavSystemInterface;

   return systemAPI;
}

//---------------------------------------------------------------------------

bool CNavSystemInterface::DeactivateTool(const CToolObject *toolObj)
{
   // A Tool uses this function to deactivate itself, such as when the
   // user closes the tool's window

   // returns true if it is okay to close the tool, false
   // if it is not okay, e.g., a fix is pending

   CToolManager toolMgr;

   int toolIndex = toolMgr.findTool(toolObj);
   if (toolIndex == toolMgr.getActiveToolIndex())
      {
      if (toolMgr.CheckProvisionalUnresolvedAndToolProcessing())
         return false;
      toolMgr.setActiveTool(-1);
      mainWindow->ShowActiveToolName();
      }

   return true;
}

//---------------------------------------------------------------------------

bool CNavSystemInterface::isDisplaying()
{
   return mainWindow->GetPlayer()->isDisplaying();
}

// this call gets the time in milliseconds since the player entered idle state
long CNavSystemInterface::getIdleTime()
{
   return mainWindow->GetPlayer()->getIdleTime();
}

void CNavSystemInterface::fullStop()
{
   mainWindow->GetPlayer()->fullStop();
}

// this call is not synchronous, so it can be
// called from a bgnd thread
void CNavSystemInterface::goToFrame(int frmnum)
{
   mainWindow->GetPlayer()->goToFrame(frmnum);
   mainWindow->GetPlayer()->setTrackBarUpdate(true);
}

// this call always goes to the disk and gets a fresh
// copy of the native frame to refresh the display
//
void CNavSystemInterface::goToFrameSynchronous(int frmnum)
{
   mainWindow->GetPlayer()->goToFrameSynchronous(frmnum);
   mainWindow->GetPlayer()->setTrackBarUpdate(true);
}

// this call always goes to the disk and gets a fresh
// copy of the native frame to refresh the display
//
void CNavSystemInterface::refreshFrame()
{
   mainWindow->GetPlayer()->refreshFrameSynchronous();
   mainWindow->GetPlayer()->setTrackBarUpdate(true);
}

// if there's a copy of the native frame already in the
// ring buffer this call will use it. This eliminates
// the read of the native frame from disk when there are
// repetitive refreshes
//
void CNavSystemInterface::refreshFrameCached(bool fast)
{
   mainWindow->GetPlayer()->refreshFrameSynchronousCached(fast);
   mainWindow->GetPlayer()->setTrackBarUpdate(true);
}

void CNavSystemInterface::setPlaybackFilter(int newFilter, bool refresh)
{
   CPlayer *player = mainWindow->GetPlayer();

   player->setPlaybackFilter(newFilter);
   mainWindow->UpdatePlaybackFilterStatus();
   if (refresh && !player->isDisplaying())
      player->refreshFrameSynchronous();
}

int CNavSystemInterface::getPlaybackFilter()
{
   return mainWindow->GetPlayer()->getPlaybackFilter();
}

const CImageFormat * CNavSystemInterface::getVideoClipImageFormat()
{
  return(mainWindow->GetPlayer()->getVideoClipImageFormat());
}

bool CNavSystemInterface::isAClipLoaded()
{
   return mainWindow->GetPlayer()->isAVideoClipLoaded();
}

bool CNavSystemInterface::isMasterClipLoaded()
{
   if (!isAClipLoaded())
      return false;

   string clipPathName = mainWindow->GetPlayer()->getLoadedVideoClipName();
   CBinManager binManager;

   return (clipPathName == binManager.getMasterClipPath(clipPathName));
}

bool CNavSystemInterface::doesClipHaveVersions()
{
   if (!isAClipLoaded())
      return false;

   ClipIdentifier loadedClipId(mainWindow->GetPlayer()->getLoadedVideoClipName());
   CBinManager binManager;
   return binManager.doesClipHaveVersions(loadedClipId);
}

void CNavSystemInterface::toolPlayClipFrameRangeInclusive(int firstFrameIndex, int lastFrameIndex)
{
	mainWindow->GetPlayer()->toolPlayClipFrameRangeInclusive(firstFrameIndex, lastFrameIndex);
}

void CNavSystemInterface::fattenRectangleFrame(RECT * tempRect, int padding)
{
  mainWindow->GetDisplayer()->fattenRectangleFrame(tempRect, padding);
}

void CNavSystemInterface::UpdateStatusBar(const string &sStatus, int iPanel)
{
   switch (iPanel)
   {
      case 0:
         // Tool name
         mainWindow->GetTimeline()->DRSNameLabel->Caption = sStatus.c_str();
         break;

      case 1:
         // Tool message
         mainWindow->GetTimeline()->DRSMessageLabel->Caption = sStatus.c_str();
         break;
   }
}

RECT CNavSystemInterface::GetClientRectScreenCoords()
{
   return mainWindow->GetUsableClientRectInScreenCoords();
}

RECT CNavSystemInterface::getImageRectangle()
{
   return mainWindow->getImageRectangle();
}

void *CNavSystemInterface::GetMainWindowHandle()
{
   return (void *)mainWindow;
}

void CNavSystemInterface::setDRSLassoMode(bool state)
{
   // true for lasso, false for rectangle
   mainWindow->GetDisplayer()->setDRSLassoMode(state);
}

bool CNavSystemInterface::getDRSLassoMode()
{
   // returns true for lasso, false for rectangle
   return mainWindow->GetDisplayer()->getDRSLassoMode();
}

// ===================================================================
//
// Function:    GoToNextOrPreviousReviewFrame
//
// Description: Goes to the next or previous frame with fixes.
//
// Arguments:   goForward - forward/backward flag (true = forward)
//
// Returns:     0 if no error
//
// ===================================================================
int CNavSystemInterface::GoToNextOrPreviousReviewFrame(bool goNext, int frameIndex)
{
   int retVal;

   historyReviewStatus = HISTORY_REVIEW_STATUS_NONE;

   if (CheckProvisionalUnresolved())
      {
      return 0;
      }

   CVideoFrameList* videoFrameList
             = mainWindow->GetCurrentClipHandler()->getCurrentVideoFrameList();

   if (videoFrameList == nullptr)
      {
      // No clip loaded!
      return 0;
      }

   // Search for next/prev frame that has saves
   int reviewFrame;
   if (goNext)
      {
      reviewFrame = saveRestoreEngine->getNextFrameWithFixes(frameIndex);
      if (reviewFrame == -1)
         {
         // Time to loop around to the beginning frame
         // Subtract 1 from first frame to force search to include frame 0
         frameIndex = videoFrameList->getInFrameIndex() - 1;
         reviewFrame = saveRestoreEngine->getNextFrameWithFixes(frameIndex);
         }
      }
   else
      {
      reviewFrame = saveRestoreEngine->getPreviousFrameWithFixes(
                                                            frameIndex);
      if (reviewFrame == -1)
         {
         // Time to loop around to the end frame
         frameIndex = videoFrameList->getOutFrameIndex();
         reviewFrame = saveRestoreEngine->getPreviousFrameWithFixes(frameIndex);
         }
      }

   // Did we actually find something?
   if (reviewFrame == -1)
      {
      // No fixes!
      provisional->ClearReviewRegion(true);  // WTF IS THIS??? QQQ
      _MTIErrorDialog("There are no frames with fixes!    ");
      return 0;
      }

   CPlayer *player = mainWindow->GetPlayer();
   player->fullStop();  // force player to read frame from disk
   player->goToFrameSynchronous(reviewFrame);

   return 0;  // return success
}

// ===================================================================
//
// Function:    DisplaySingleReviewRegion
//
// Description: QQQ APPARENTLY THIS DOESN'T WORK!!!
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CNavSystemInterface::DisplaySingleReviewRegion(bool goNext,
                                                   bool goZoom,
                                                   int frameIndex)
{
   int retVal;

   historyReviewStatus = HISTORY_REVIEW_STATUS_NONE;

   if (CheckProvisionalUnresolved())
      {
      return 0;
      }

   CVideoFrameList* videoFrameList
             = mainWindow->GetCurrentClipHandler()->getCurrentVideoFrameList();

   if (videoFrameList == nullptr)
      {
      // Mo clip loaded!
      return 0;
      }

   // Tell historyReviewList to go to previous or next review region
   if (goNext)
      {
      retVal = historyReviewList->goToNextReview(frameIndex, videoFrameList);
      }
   else // go Next
      {
      retVal = historyReviewList->goToPreviousReview(frameIndex, videoFrameList);
      }

   if (retVal != 0)
      {
      provisional->ClearReviewRegion(true);
      return retVal;  // ERROR
      }

   int newReviewFrameIndex = historyReviewList->getReviewFrame();

   if (newReviewFrameIndex != -1)
      {
      CPixelRegionList *newReviewRegion = new CPixelRegionList;
      historyReviewList->getCurrentReviewRegion(*newReviewRegion);

      provisional->DisplayReviewRegion(newReviewFrameIndex, newReviewRegion, goZoom);

      historyReviewStatus = HISTORY_REVIEW_STATUS_SINGLE;
      }
   else
      {
      provisional->ClearReviewRegion(true);
      _MTIErrorDialog("There are no saved fixes to review");
      }

   mainWindow->UpdateZoomLevelStatus();

   return 0;  // return success
}

// ===================================================================
//
// Function:    DisplayAllReviewRegions
//
// Description:
//
// Arguments:
//
// Returns:
//
// ===================================================================
int CNavSystemInterface::DisplayAllReviewRegions(int frameIndex)
{
   int retVal;

   historyReviewStatus = HISTORY_REVIEW_STATUS_NONE;

   if (CheckProvisionalUnresolved())
      {
      return 0;
      }

   CPixelRegionList *newReviewRegion = new CPixelRegionList;
   retVal = historyReviewList->getAllReviewRegions(frameIndex, *newReviewRegion);
   if (retVal != 0)
      return retVal;

   int regionCount = historyReviewList->getReviewRegionCount();

   if (regionCount > 0)
      {
      provisional->DisplayReviewRegion(frameIndex, newReviewRegion, false);

      historyReviewStatus = HISTORY_REVIEW_STATUS_FRAME;
      }
   else
      {
      delete newReviewRegion;
      provisional->ClearReviewRegion(true);
      _MTIErrorDialog("There are no saved fixes to review");
      }

   return 0;
}

int CNavSystemInterface::ClearReviewRegions(bool doRedraw)
{
   historyReviewStatus = HISTORY_REVIEW_STATUS_NONE;

   historyReviewList->setReviewListInvalid();

   provisional->ClearReviewRegion(doRedraw);

   return 0;
}

int CNavSystemInterface::SelectReviewRegion(CPixelRegionList& reviewRegion)
{
   int retVal;

   if (CheckProvisionalUnresolved())
      {
      return -1;   // TBD - NEED BETTER ERROR CODE
      }

   int frameIndex = getLastFrameIndex();

   // Check that there review regions displayed on the currently displayed
   // frame
   if (historyReviewStatus == HISTORY_REVIEW_STATUS_NONE
       || frameIndex != historyReviewList->getReviewFrame())
      {
      // There are no review regions available or they are on another frame
      _MTIErrorDialog("There is no review box to select on this frame.\nUse Last, Next, or Frame to find the desired fix.");
      return -1; // TBD - NEED BETTER ERROR CODE
      }

   // Get the description of the current review region
   switch (historyReviewStatus)
      {
      case HISTORY_REVIEW_STATUS_SINGLE :
         {
         // A single review region is available
         retVal = historyReviewList->getCurrentReviewRegion(reviewRegion);
         if (retVal != 0)
            return retVal;

         historyReviewList->saveForMarkCurrentReviewRegion();
         }
         break;

      case HISTORY_REVIEW_STATUS_FRAME :
         {
         const CImageFormat *imageFormat = getVideoClipImageFormat();
         int frameWdth = imageFormat->getTotalFrameWidth();
         int frameHght = imageFormat->getTotalFrameHeight();
         int pxlComponentCount = imageFormat->getComponentCount();
         RECT activePictureRect = imageFormat->getActivePictureRect();

         // Select entire frame
         reviewRegion.Add(activePictureRect, 0,
                          frameWdth, frameHght,
                          pxlComponentCount);

         historyReviewList->saveForMarkAllReviewRegions();
         }
      break;

      case HISTORY_REVIEW_STATUS_NONE:
         // Do nothing!
         break;

      default:
         MTIassert(false);
         break;

      } // switch

   // Undraw, or whatever, the currently displayed review regions
   ClearReviewRegions(false);

   return 0;
}

int CNavSystemInterface::MarkReviewRegions()
{
   // Mark as restored those review regions that were previously selected

   int retVal;

   retVal = historyReviewList->markReviewRegions();
   if (retVal != 0)
      return retVal;    // ERROR

   return 0;
}

int CNavSystemInterface::ReviseAllGOVRegions()
{
   // Revise those review regions that were previously operated on

   int retVal;

   retVal = historyReviewList->reviseAllGOVRegions();
   if (retVal != 0)
      return retVal;    // ERROR

   return 0;
}

int CNavSystemInterface::ReviseGOVRegions(int frameNumber)
{
   // Revise frame's review regions that were previously operated on

   int retVal;

   retVal = historyReviewList->reviseGOVRegions(frameNumber);
   if (retVal != 0)
      return retVal;    // ERROR

   return 0;
}

int CNavSystemInterface::DiscardGOVRevisions()
{
   // Discard those review revisions that are waiting in the history

   int retVal;

   retVal = historyReviewList->discardGOVRevisions();
   if (retVal != 0)
      return retVal;    // ERROR

   return 0;
}

int CNavSystemInterface::RestoreToolFrameBuffer(CToolFrameBuffer *frameBuffer,
                                                RECT *filterRect,
                                                POINT *filterLasso,
                                                BEZIER_POINT *filterBezier,
                                            CPixelRegionList *filterPixelRegion)
{
   int retVal;

   retVal = provisional->RestoreToolFrameBuffer(frameBuffer, filterRect,
                                                filterLasso, filterBezier,
                                                filterPixelRegion);
   if (retVal != 0)
      return 0;

   return 0;
}

int CNavSystemInterface::SaveToolFrameBuffer(CToolFrameBuffer *frameBuffer,
                                             int toolNumber,
                                             const string& toolString,
                                             bool saveToHistory)
{
   int retVal;

   // Invalidate the Review List if on same frame
   if (frameBuffer->GetFrameIndex() == historyReviewList->getReviewFrame())
      {
      historyReviewList->setReviewListInvalid();
      }

   retVal = provisional->SaveToolFrameBuffer(frameBuffer,
                                             toolNumber, toolString,
                                             saveToHistory);
   if (retVal != 0)
      return retVal;

   return 0;
}

void CNavSystemInterface::ValidateHistory(int frameIndex)
{
   provisional->ValidateHistory(frameIndex);
}

void CNavSystemInterface::DiscardUnvalidatedHistory()
{
   provisional->DiscardUnvalidatedHistory();
}


//////////////////////////////////////////////////////////////////////
// Provisional Interface
//////////////////////////////////////////////////////////////////////

void CNavSystemInterface::ToggleProvisional(bool goZoom)
{
   provisional->Toggle(goZoom);
}

void CNavSystemInterface::AcceptProvisional(bool goToFrame)
{
   provisional->Accept(goToFrame);
}

void CNavSystemInterface::AcceptProvisionalInBackground()
{
   provisional->AcceptFromBackgroundThread();
}

void CNavSystemInterface::RejectProvisional(bool goToFrame)
{
   provisional->Reject(goToFrame);
}

void CNavSystemInterface::RejectProvisionalInBackground()
{
   provisional->RejectFromBackgroundThread();
}

void CNavSystemInterface::RefreshProvisional()
{
   CPlayer *player = mainWindow->GetPlayer();

   if (player->getProvisionalMode() == DISPLAY_PROVISIONAL &&
       player->getProvisionalFrameIndex() >= 0)
   {
      player->displayProvisionalFrame();
   }
}

void CNavSystemInterface::ClearProvisional(bool doLastFrame)
{
   provisional->Clear(doLastFrame);
}

bool CNavSystemInterface::IsProvisionalPending()
{
   return provisional->IsProvisionalPending();
}

bool CNavSystemInterface::IsProcessedVisible()
{
   return provisional->IsProcessedVisible();
}

bool CNavSystemInterface::CheckProvisionalUnresolved()
{
   CToolManager toolMgr;

   return toolMgr.CheckProvisionalUnresolved();
}

bool CNavSystemInterface::CheckToolProcessing()
{
   CToolManager toolMgr;

   return toolMgr.CheckToolProcessing();
}

bool CNavSystemInterface::CheckProvisionalUnresolvedAndToolProcessing()
{
   CToolManager toolMgr;

   return toolMgr.CheckProvisionalUnresolvedAndToolProcessing();
}

CToolFrameBuffer* CNavSystemInterface::GetProvisionalFrame()
{
   int retVal;

   CToolFrameBuffer *provisionalFrame;
   retVal = provisional->GetFrame(&provisionalFrame);

   if (retVal != 0)
      provisionalFrame = 0;   // ERROR

   return provisionalFrame;
}

int CNavSystemInterface::GetProvisionalFrameIndex()
{
   int retVal = -1;

   if (provisional->IsProvisionalPending())
      retVal = provisional->GetLastFrameIndex();

   return retVal;
}

int CNavSystemInterface::SetProvisionalHighlight(bool newHighlightFlag)
{
   return provisional->SetHighlight(newHighlightFlag);
}

int CNavSystemInterface::SetProvisionalRender(bool newRenderFlag)
{
   return provisional->SetRender(newRenderFlag);
}

int CNavSystemInterface::SetProvisionalReprocess(bool newReprocessFlag)
{
   return provisional->SetReprocess(newReprocessFlag);
}

int CNavSystemInterface::SetProvisionalReview(bool newReviewFlag)
{
   return provisional->SetReview(newReviewFlag);
}

void CNavSystemInterface::UpdateProvisionalStatusBarPanel()
{
   provisional->UpdateStatusBar();
}

int CNavSystemInterface::
RegisterProvisionalToolProc(CProvisionalToolProc *toolProc)
{
   // TTT - 1) DO WE HAVE TO CHECK THE STATUS OF THE PROVISIONAL BEFORE
   //          SETTING A NEW CProvisionalToolProcessor?
   //       2) DO WE HAVE TO SET SOMETHING IN THE PROVISIONAL?

   provisional->SetProvisionalToolProc(toolProc);

   return 0;
}

int CNavSystemInterface::PutFrameToProvisional(CToolFrameBuffer *toolFrame)
{
   int retVal;

   retVal = provisional->PutFrame(toolFrame);
   if (retVal != 0)
      return retVal;

   return 0;
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

int CNavSystemInterface::setClip(ClipSharedPtr &newClip, int newVideoProxyIndex,
                                 int newVideoFramingIndex)
{
   int retVal;

//   retVal = saveRestoreEngine->setClip(newClip, newVideoProxyIndex,
//                                       newVideoFramingIndex);
   retVal = provisional->setClip(newClip, newVideoProxyIndex,
                                 newVideoFramingIndex);
   if (retVal != 0)
      return retVal; // ERROR:

   return 0;
}

string CNavSystemInterface::getClipFilename()
{
   // Get the current clips complete file name, including Bin Path
   // and extension

   string filename;

   auto clip = mainWindow->GetCurrentClipHandler()->getCurrentClip();

   if (clip == nullptr)
      {
      // No current clip, return empty string
      return filename;
      }

   CBinManager binMgr;
   filename = binMgr.makeClipFileNameWithExt(clip->getBinPath(),
                                             clip->getClipName());
   return filename;
}

int CNavSystemInterface::getVideoFramingIndex()
{
   return mainWindow->GetCurrentClipHandler()->getCurrentVideoFramingIndex();
}

int CNavSystemInterface::getVideoProxyIndex()
{
   return mainWindow->GetCurrentClipHandler()->getCurrentVideoProxyIndex();
}

ClipSharedPtr CNavSystemInterface::getClip()
{
   return mainWindow->GetCurrentClipHandler()->getCurrentClip();
}

CVideoFrameList* CNavSystemInterface::getVideoFrameList()
{
   return mainWindow->GetCurrentClipHandler()->getCurrentVideoFrameList();
}

CRenderDestinationClip* CNavSystemInterface::getRenderDestinationClip()
{
   return mainWindow->GetRenderDestinationClip();
}

void CNavSystemInterface::initRenderDestinationClip(int srcornew)
{
   mainWindow->InitRenderDestinationClip(srcornew);
}

// Creates an ordered list of metadata directory paths from the current clip up
// through the master clip.
vector<string> CNavSystemInterface::getClipMetadataDirectoryChain(const string &metadataTypeName)
{
   vector<string> chain;
   auto clipPath = mainWindow->GetCurrentClipHandler()->getCurrentClipPath();

   CBinManager binManager;

   ClipIdentifier clipId(clipPath);
   while (true)
   {
      // The metadata directory for the version clip is the parent of
      // the history directory (i.e. just strip off the "\History"!)
      string historyDirectory;
      if (clipId.IsVersionClip())
      {
         historyDirectory = binManager.getDirtyHistoryFolderPath(clipPath);
      }
      else
      {
         int retVal;
         auto clip = binManager.openClip(clipPath, &retVal);
         if (clip)
         {
            auto videoProxy = clip->getVideoProxy(VIDEO_SELECT_NORMAL);
            if (videoProxy)
            {
               auto mediaStorageList = videoProxy->getMediaStorageList();
               if (mediaStorageList)
               {
                  auto mediaStorage_0 = mediaStorageList->getMediaStorage(0);
                  if (mediaStorage_0)
                  {
                     historyDirectory = RemoveDirSeparator(mediaStorage_0->getHistoryDirectory());
                  }
               }
            }

            binManager.closeClip(clip);
         }
      }

      auto metadataDirectory = RemoveDirSeparator(GetFileAllButLastDir(AddDirSeparator(historyDirectory)));
      if (!metadataDirectory.empty())
      {
         if (!metadataTypeName.empty())
         {
            metadataDirectory = AddDirSeparator(metadataDirectory) + metadataTypeName;
         }

         chain.push_back(metadataDirectory);
      }

      if (clipId.IsVersionClip() == false)
      {
         break;
      }

      clipId = clipId.GetParentClipId();
      clipPath = clipId.GetClipPath();
   }

   return chain;
}

MTI_INT32 CNavSystemInterface::getMarkIn()
{
   return mainWindow->GetPlayer()->getMarkIn();
}

MTI_INT32 CNavSystemInterface::getMarkOut()
{
   return mainWindow->GetPlayer()->getMarkOut();
}

CTimeLine *CNavSystemInterface::getClipTimeline()
{
	TTimelineFrame *timeline = mainWindow->GetTimeline();
	return timeline->MTL->TimeLine();
}

void CNavSystemInterface::getTimelineVisibleFrameIndexRange(int &inIndex, int &exclusiveOutIndex)
{
   TTimelineFrame *timeline = mainWindow->GetTimeline();
   timeline->MTL->GetVisibleFrameIndexRange(inIndex, exclusiveOutIndex);
}

// NOTE: cut list ALWAYS includes the clip in frame (0) and out frame (one past last frame)!
void CNavSystemInterface::getCutList(vector<int> &cutList)
{
   cutList.clear();

   auto clip = getClip();
   if (!clip)
   {
      // No clip loaded.
      return;
   }

   // Start off with the clip IN frame.
   cutList.push_back(0);

   // Fill in the actual cut frames.
   auto prevCutFrameIndex = 0;
   CTimeLine timeline(clip);
   while (true)
   {
      // We're done when the function returns the prevFrameIndex!
      auto cutFrameIndex = timeline.getNextEvent(prevCutFrameIndex, EVENT_CUT_MASK);
      if (cutFrameIndex == prevCutFrameIndex)
      {
         break;
      }

      cutList.push_back(cutFrameIndex);
      prevCutFrameIndex = cutFrameIndex;
   }

   // Finish up by adding the clip OUT frame.
   auto frameList = getVideoFrameList();
   if (frameList == nullptr)
   {
      return;
   }

   cutList.push_back(frameList->getTotalFrameCount());
}

int CNavSystemInterface::getShowAlphaState()
{
	TTimelineFrame *timeline = mainWindow->GetTimeline();
   return timeline ? timeline->getShowAlphaState() : -1;   // -1 = disabled
}

void CNavSystemInterface::setShowAlphaState(bool newState)
{
	TTimelineFrame *timeline = mainWindow->GetTimeline();
   if (timeline)
   {
      timeline->setShowAlphaState(newState);
   }
}


/////////////////////////////////////////////////////////////////////////////
// Tool Processing API
/////////////////////////////////////////////////////////////////////////////

int CNavSystemInterface::MakeSimpleToolSetup(const string& toolName,
                                             EToolSetupType newToolSetupType,
                                             CToolIOConfig *toolIOConfig)
{
   int newToolSetupHandle;
   CToolManager toolMgr;

   newToolSetupHandle = toolMgr.MakeSimpleToolSetup(toolName, newToolSetupType,
                                                    toolIOConfig);

   return newToolSetupHandle;
}

int CNavSystemInterface::DestroyToolSetup(int toolSetupHandle)
{
   int retVal;
   CToolManager toolMgr;

   if (toolSetupHandle < 0)
      return 0;

   if (toolSetupHandle == toolMgr.GetActiveToolSetupHandle())
      {
      // If caller's tool was the active tool setup, clear the current provisional,
      // just in case
      ClearProvisional(true);

      // Cannot clear the active tool setup
      SetActiveToolSetup(-1);
      }

   retVal = toolMgr.DestroyToolSetup(toolSetupHandle);
   if (retVal != 0)
      return retVal; // ERROR

   // Wait for the destruction to be complete
   toolMgr.WaitForToolSetupDestruction();

   return 0;
}

int CNavSystemInterface::SetActiveToolSetup(int newToolSetupHandle)
{
   int retVal;
   CToolManager toolMgr;


   retVal = toolMgr.SetActiveToolSetup(newToolSetupHandle);
   if (retVal != 0)
      return retVal; // ERROR
   return 0;
}

int CNavSystemInterface::GetActiveToolSetupHandle()
{
   CToolManager toolMgr;

   return toolMgr.GetActiveToolSetupHandle();
}

int CNavSystemInterface::SetToolFrameRange(CToolFrameRange *toolFrameRange)
{
   int retVal;
   CToolManager toolMgr;

   retVal = toolMgr.SetToolFrameRange(toolFrameRange);
   if (retVal != 0)
      return retVal; // ERROR
   return 0;
}

int CNavSystemInterface::SetToolParameters(CToolParameters *toolParameters)
{
   int retVal;
   CToolManager toolMgr;

   retVal = toolMgr.SetToolParameters(toolParameters);
   if (retVal != 0)
      return retVal; // ERROR
   return 0;
}

int CNavSystemInterface::RunActiveToolSetup()
{
   int retVal;
   CToolManager toolMgr;

   // Stop the player in case it is running
   fullStop();

   retVal = toolMgr.RunActiveToolSetup();
   if (retVal != 0)
      return retVal; // ERROR
   return 0;
}

int CNavSystemInterface::StopActiveToolSetup()
{
   int retVal;
   CToolManager toolMgr;

   retVal = toolMgr.StopActiveToolSetup();
   if (retVal != 0)
      return retVal; // ERROR
   return 0;
}

int CNavSystemInterface::EmergencyStopActiveToolSetup()
{
   int retVal;
   CToolManager toolMgr;

   retVal = toolMgr.EmergencyStopActiveToolSetup();
   if (retVal != 0)
      return retVal; // ERROR
   return 0;
}

int CNavSystemInterface::PauseActiveToolSetup()
{
   int retVal;
   CToolManager toolMgr;

   retVal = toolMgr.PauseActiveToolSetup();
   if (retVal != 0)
      return retVal; // ERROR
   return 0;
}

EAutotoolStatus CNavSystemInterface::GetActiveToolSetupStatus()
{
   CToolManager toolMgr;

   return toolMgr.GetActiveToolSetupStatus();
}

EAutotoolStatus CNavSystemInterface::GetToolSetupStatus(int toolSetupHandle)
{
   CToolManager toolMgr;

   return toolMgr.GetToolSetupStatus(toolSetupHandle);
}

void CNavSystemInterface::WaitForToolProcessing()
{
   // Wait for Tool Processing to complete

   CToolManager toolMgr;

   toolMgr.WaitForToolProcessing(mainThreadId == GetCurrentThreadId());
}

CBufferPool* CNavSystemInterface::GetBufferAllocator(int bytesPerBuffer,
                                                     int bufferCount,
                                                double invisibleFieldsPerFrame)
{
   CToolManager toolMgr;

   return toolMgr.GetBufferAllocator(bytesPerBuffer, bufferCount,
                                     invisibleFieldsPerFrame);
}

void CNavSystemInterface::ReleaseBufferAllocator(int bytesPerBuffer,
                                                 int bufferCount,
                                                double invisibleFieldsPerFrame)
{
   CToolManager toolMgr;

   toolMgr.ReleaseBufferAllocator(bytesPerBuffer, bufferCount,
                                  invisibleFieldsPerFrame);
}

///////////////////////////////////////////////////////////////////////////////
// PDL & Rendering
///////////////////////////////////////////////////////////////////////////////

int CNavSystemInterface::MakeOneNewPdlEntry(CPDL *pdl, int markin, int markout)
{
   // Get pointer to the current clip;
   auto clip = mainWindow->GetCurrentClipHandler()->getCurrentClip();
   if (clip == nullptr)
   {
      return -1;
   }

   // Collect information about the current clip
   string binPath = clip->getBinPath();
   string clipName = clip->getClipName();
   int proxyIndex = getVideoProxyIndex();
   int framingIndex = getVideoFramingIndex();

   if (markin < 0 || markout < 0 || markout <= markin)
   {
      return -1;
   }

   // Create, fill in, then add the single entry.
   CPDLEntry *pdlEntry = pdl->MakeNewEntry();

   // Add the source amd destination information to the new PDL Entry
   pdlEntry->MakeNewMediaSource(binPath, clipName, proxyIndex, framingIndex, markin, markout);
   pdlEntry->MakeNewMediaDest(binPath, clipName, proxyIndex, framingIndex, markin, markout);

   // Add the Mask Regions
   CNavigatorTool::GetMaskTool()->CapturePDLEntry(*pdlEntry);

   // Capture the tool's parameters
   CToolManager toolMgr;
   int retVal = toolMgr.onCapturePDLEntry(*pdlEntry);
   if (retVal != 0)
   {
      return -1;
   }

   return 0;
}

int CNavSystemInterface::MakeANewPdlEntryForEachShotInClip(CPDL *pdl)
{
   // One entry for each of the scenes in the entire clip.
   TTimelineFrame *timeline = mainWindow->GetTimeline();
   if (timeline == nullptr)
   {
      return -1;
   }

   int retVal = 0;
   auto saveMarkIn = mainWindow->GetPlayer()->getMarkIn();
   auto saveMarkOut = mainWindow->GetPlayer()->getMarkOut();

   vector<int> cutList;
   getCutList(cutList);
   for (auto i = 0; i < (cutList.size() - 1); ++i)
   {
      // Need to set real marks in case the tool cares about that!
      int entryMarkIn = cutList[i];
      int entryMarkOut = cutList[i + 1];
      mainWindow->GetPlayer()->setMarkIn(entryMarkIn);
      mainWindow->GetPlayer()->setMarkOut(entryMarkOut);

      retVal = MakeOneNewPdlEntry(pdl, entryMarkIn, entryMarkOut);
      Application->ProcessMessages();
      if (retVal != 0)
      {
         break;
      }
   }

   mainWindow->GetPlayer()->setMarkIn(saveMarkIn);
   mainWindow->GetPlayer()->setMarkOut(saveMarkOut);

   return retVal;
}

int CNavSystemInterface::AddEntriesToActivePdl(CPDL *pdl)
{
   CPDLViewerManager pdlViewerMgr;
   EPDLEntryAddReplaceAction action = pdlViewerMgr.IsRecapture();
   if (action == PDL_ENTRY_REPLACE)
   {
      pdlViewerMgr.ReplaceInActivePDL(*pdl);
   }
   else if (action == PDL_ENTRY_ADD)
   {
      pdlViewerMgr.AddToActivePDL(*pdl);
   }

   return 0;
}

int CNavSystemInterface::CapturePDLEntry(bool all)
{
   int retVal;
   CAutoErrorReporter autoErr("CNavSystemInterface::CapturePDLEntry", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
   CPDLViewerManager pdlViewerMgr;

   auto clip = mainWindow->GetCurrentClipHandler()->getCurrentClip();
   if (clip == nullptr)
   {
      // No clip loaded.
      return -1;
   }

   // All this is to pop up a warning if the active tool does not preserve
   // history and this is the first event we are adding to the active PDL.
   CToolManager toolMgr;
   int activeToolIndex = toolMgr.getActiveToolIndex();
   CToolObject *activeTool = toolMgr.GetTool(activeToolIndex);
   bool preservesHistory = true;
   if (activeTool != nullptr)
   {
      preservesHistory = activeTool->DoesToolPreserveHistory();
   }

   // if tool is destructive but the active PDL has no destructive entries yet
   if (!preservesHistory && !pdlViewerMgr.DoesActivePDLContainADestructiveEntry())
   {
      if (!WarnThereIsNoUndo())
      {
         // User declined to continue.
         return -1;
      }

      pdlViewerMgr.SetActivePDLContainsADestructiveEntry();
   }

   // NOTE: Using unique_ptr presumes that the "junkyard" method for CPDL only
   // deletes the CPDL object, does nothing else!
   auto pdl = make_unique<CPDL>();
   if (!pdl)
   {
      autoErr.errorCode = -1;
      autoErr.msg << "INTERNAL ERROR: Can't add event to PDL";
      return -1;
   }

   // Create (if need be) and show the active PDL. Ugly interface. Note this
   // will focus on the PDL window so when done, re-focus on the main window.
   pdlViewerMgr.ShowActivePDL();
   if (!all)
   {
      // Add one PDL entry for the current marked range.
      if (getMarkIn() < 0 || getMarkOut() < 0 || getMarkOut() <= getMarkIn())
      {
         autoErr.errorCode = -1;
         autoErr.msg << "ERROR: Can't add event to PDL because the marks are invalid!";
         return -1;
      }

      retVal = MakeOneNewPdlEntry(pdl.get(), getMarkIn(), getMarkOut());
   }
   else
   {
      pdlViewerMgr.BeginningPDLAddOperation();

      retVal = MakeANewPdlEntryForEachShotInClip(pdl.get());

      pdlViewerMgr.EndingPDLAddOperation();
   }

   retVal = AddEntriesToActivePdl(pdl.get());
   if (retVal)
   {
      autoErr.errorCode = -2;
      autoErr.msg << "INTERNAL ERROR: Can't add event to PDL";
      setMainWindowFocus();
      return -2;
   }

   // Save when we're done and we actually added or replaced a PDL entry.
   pdlViewerMgr.SaveActivePDLIfModified();

   setMainWindowFocus();

   return 0;
}

bool CNavSystemInterface::IsPDLRendering()
{
   CToolManager toolMgr;

   return toolMgr.IsPDLRendering();
}

bool CNavSystemInterface::IsPDLEntryLoading()
{
   // Returns true if a PDL entry is currently loading.  This is a hack
   // used by tools to distiguish human interaction from initialization
   // from a PDL entry.

   return mainWindow->IsPDLEntryLoading();
}

///////////////////////////////////////////////////////////////////////////////
// Mask Tool Interface
///////////////////////////////////////////////////////////////////////////////

bool CNavSystemInterface::IsMaskEnabled()
{
   return CNavigatorTool::GetMaskTool()->IsMaskToolActive();
}

bool CNavSystemInterface::IsMaskAvailable()
{
   return CNavigatorTool::GetMaskTool()->IsMaskAvailable();
}

bool CNavSystemInterface::IsMaskVisible()
{
   return CNavigatorTool::GetMaskTool()->IsMaskVisible();
}

bool CNavSystemInterface::IsMaskVisibilityLockedOff()
{
   return CNavigatorTool::GetMaskTool()->IsMaskVisibilityLockedOff();
}

void CNavSystemInterface::CalculateMaskROI(int frameIndex)
{
   CNavigatorTool::GetMaskTool()->CalculateRegionOfInterest(frameIndex);
}

CRegionOfInterest* CNavSystemInterface::GetMaskRoi()
{
   return CNavigatorTool::GetMaskTool()->GetRegionOfInterest();
}

int CNavSystemInterface::GetMaskRoi(int frameIndex, CRegionOfInterest &maskRoi)
{
   return CNavigatorTool::GetMaskTool()->GetRegionOfInterest(frameIndex,
                                                             maskRoi);
}

bool CNavSystemInterface::IsMaskAnimated()
{
   return CNavigatorTool::GetMaskTool()->IsMaskAnimated();
}

bool CNavSystemInterface::IsMaskExclusive()
{
   return !CNavigatorTool::GetMaskTool()->IsMaskInclusive();
}

bool CNavSystemInterface::IsMaskInRoiMode()
{
   return CNavigatorTool::GetMaskTool()->IsMaskRoiModeActive();
}

void CNavSystemInterface::SetMaskFromPdlEntry(CPDLEntry &pdlEntry)
{
	CNavigatorTool::GetMaskTool()->SetMask(pdlEntry);
}

string CNavSystemInterface::GetMaskAsString()
{
	return CNavigatorTool::GetMaskTool()->GetMaskAsString();
}

int  CNavSystemInterface::SaveMaskToFile(const string &filename)
{
	return CNavigatorTool::GetMaskTool()->SaveMaskToFile(filename);
}

int CNavSystemInterface::LoadMaskFromFile(const string &filename)
{
   if (!DoesFileExist(filename))
   {
      return -1;
   }

	return CNavigatorTool::GetMaskTool()->LoadMaskFromFile(filename);
}

// The enable/disable pile of steaming horse shit below has been replaced by this call:
void CNavSystemInterface::SetMaskAllowed(bool regularMode, bool roiMode, bool initialRoiModeState)
{
   CNavigatorTool::GetMaskTool()->setMaskToolActivationAllowed(regularMode, roiMode, initialRoiModeState);
}

//----------------------------------------------------------------------------
// The API to enable/disable the mask tool is highly confusing. There
// are two types of enabling - EXTERNAL and INTERNAL. EXTERNAL is controlled
// by the GUI, for example from a menu item - if it is enabled, then the
// msk tool will be active whenever INTERNAL is enabled. The protocol is that
// each tool that does NOT support the mask tool must call
// InternalDisableMaskTool() in the toolActivate() method, and MUST REMEMBER
// the return value, which was the state of INTERNAL enable at the time of the
// call. Then in the toolDeactivate() method, the tool must call
// InternalEnableMaskTool() and pass it the value of INTERNAL enable that
// it saved from the call to InternalDisableMaskTool(). I guess that is so
// we can nest calls to the INTERNAL enable/disable without screwing things
// up, but in fict this is a needless complication because the calls are
// never nested.
//
// Menu items and hot keys that globally enable/disable the mask tool must
// use the ExternalEnableMaskTool() call.

//bool CNavSystemInterface::InternalDisableMaskTool()
//{
//   bool wasEnabled = CNavigatorTool::GetMaskTool()->IsMaskToolInternalEnabled();
//
//   // NO I DON'T KNOW WHY THESE DON'T GET THEIR OWN MakTookCommnd's !!!
//   // So don't ask me.
//   CNavigatorTool::GetMaskTool()->setMaskToolInternalEnabled(false);
//
//   return wasEnabled;  // return prior state
//}

//void CNavSystemInterface::InternalEnableMaskTool(bool wasEnabled)
//{
//   // NO I DON'T KNOW WHY THESE DON'T GET THEIR OWN MakTookCommnd's !!!
//   // So don't ask me.
//   CNavigatorTool::GetMaskTool()->setMaskToolInternalEnabled(true);
//}

//bool CNavSystemInterface::ExternalEnableMaskTool(bool enable)
//{
//   bool wasEnabled = CNavigatorTool::GetMaskTool()->IsMaskToolEnabled();
//   int maskToolCmd = (enable) ? NAV_CMD_MASK_TOOL_ENABLE
//                              : NAV_CMD_MASK_TOOL_DISABLE;
//   CNavigatorTool::ExecuteMaskToolCommand(maskToolCmd);
//
//   return(wasEnabled); // return prior state
//}
//----------------------------------------------------------------------------

//void CNavSystemInterface::SetMaskToolRoiModeAllowed(bool allow, bool initialState)
//{
//   CNavigatorTool::GetMaskTool()->setMaskToolRoiModeAllowed(allow, initialState);
//}
//
//bool CNavSystemInterface::SetMaskToolRoiMode(bool value)
//{
//   return CNavigatorTool::GetMaskTool()->SetMaskRoiMode(value);
//}
//
//bool CNavSystemInterface::ToggleMaskToolRoiMode()
//{
//   CNavigatorTool::GetMaskTool()->ToggleMaskRoiMode();
//   return CNavigatorTool::GetMaskTool()->IsMaskInRoiMode();
//}

void CNavSystemInterface::SetMaskToolAutoActivateRoiModeOnDraw(bool flag)
{
   CNavigatorTool::GetMaskTool()->setAutoActivateRoiModeOnDraw(flag);
}

void CNavSystemInterface::EnableMaskOutlineDisplay(bool enable)
{
   int maskToolCmd = enable ? NAV_CMD_MASK_SHOW_OUTLINE
                            : NAV_CMD_MASK_SHOW_NOTHING;
   CNavigatorTool::ExecuteMaskToolCommand(maskToolCmd);
}

void CNavSystemInterface::setMaskVisibilityLockedOff(bool flag)
{
   int maskToolCmd = flag ? NAV_CMD_MASK_LOCK_VISIBILITY_OFF
                          : NAV_CMD_MASK_UNLOCK_VISIBILITY_OFF;
   CNavigatorTool::ExecuteMaskToolCommand(maskToolCmd);
}

void CNavSystemInterface::ExecuteMaskToolCommand(int mskcmd)
{
   CNavigatorTool::ExecuteMaskToolCommand(mskcmd);
}

//void CNavSystemInterface::ShowMaskBorderDialog()
//{
//   mainWindow->MaskBorderMenuItemClick(nullptr);
//}

// A call to be able to tell the Mask Transform Editor to not refresh
// the display every time the mouse moves, which is only needed for
// some obscure Paint mode -- interferes with profiling!
// Set to TRUE to always refresh, even when not needed for the mask
// (it will always refresh while drawing a mask, e.g.)
void CNavSystemInterface::SetMaskTransformEditorIdleRefreshMode(bool onOffFlag)
{
   CNavigatorTool::GetMaskTool()->setMaskTransformEditorIdleRefreshMode(onOffFlag);
}

void CNavSystemInterface::ClearMask()
{
   CNavigatorTool::GetMaskTool()->ClearAll();
}

ESelectedRegionShape CNavSystemInterface::GetROIShape(RECT &rect,
                                 POINT *lasso,
                                 BEZIER_POINT *bezier)
{
   EMaskRegionShape maskShape = CNavigatorTool::GetMaskTool()->
                                     GetMaskRoiModeShape(rect, lasso, bezier);
   ESelectedRegionShape roiShape =
            (maskShape == MASK_REGION_RECT)? SELECTED_REGION_SHAPE_RECT :
            ((maskShape == MASK_REGION_LASSO)? SELECTED_REGION_SHAPE_LASSO :
             ((maskShape == MASK_REGION_BEZIER)? SELECTED_REGION_SHAPE_BEZIER :
                                                   SELECTED_REGION_SHAPE_NONE));

   return roiShape;
}

void CNavSystemInterface::SetROIShape(ESelectedRegionShape shape,
                 const RECT &rect,
                 const POINT *lasso,
                 const BEZIER_POINT *bezier)
{
   EMaskRegionShape maskShape =
            (shape == SELECTED_REGION_SHAPE_RECT)? MASK_REGION_RECT :
            ((shape == SELECTED_REGION_SHAPE_LASSO)? MASK_REGION_LASSO :
             ((shape == SELECTED_REGION_SHAPE_BEZIER)? MASK_REGION_BEZIER :
                                                         MASK_REGION_INVALID));

   CNavigatorTool::GetMaskTool()->SetMaskRoiModeShape(maskShape, rect, lasso, bezier);
}

void CNavSystemInterface::RestorePreviousROIShape()
{
   CNavigatorTool::GetMaskTool()->RestoreLastRoiMask();
}

void CNavSystemInterface::LockROI(bool flag)
{
   CNavigatorTool::GetMaskTool()->LockMaskRoiModeMask(flag);
}

///////////////////////////////////////////////
// ROI MODE UBER-HACK
///////////////////////////////////////////////

// From Mask Tool TO active tool!
bool CNavSystemInterface::NotifyStartROIMode()
{
   CToolManager toolManager;
   return toolManager.NotifyStartROIMode();
}

// From Mask Tool TO active tool!
// Active tool returns TRUE if handled.
bool CNavSystemInterface::NotifyUserDrewROI()
{
   CToolManager toolManager;
   return toolManager.NotifyUserDrewROI();
}

// From Mask Tool TO active tool!
void CNavSystemInterface::NotifyEndROIMode()
{
	CToolManager toolManager;
	toolManager.NotifyEndROIMode();
}

///////////////////////////////////////////////
// ZONAL FLICKER UBER-HACK
///////////////////////////////////////////////

// From Mask Tool TO active tool!
void CNavSystemInterface::NotifyMaskChanged(const string &newMaskAsString)
{
	CToolManager toolManager;
	toolManager.NotifyMaskChanged(newMaskAsString);
}

// From Mask Tool TO active tool!
void CNavSystemInterface::NotifyMaskVisibilityChanged()
{
	CToolManager toolManager;
	toolManager.NotifyMaskVisibilityChanged();
}

////////////////////////////////////////////////////////////////////////////
// A stupid hack to make Larry happy. Normally the keys that affect the mask
// state are executed AFTER the active tool has passed the key through. But
// Larry wants the A key to go to the mask tool FIRST and then if the mask
// tool doesn't consume it, the active tool, in this case AutoClean can use
// it to do something, in this case to 'delete' debris fixes. Since this is
// backwards from how the system is designed (which, in my opinion was done
// wrong... I think the mask tool should get a crack at the inputs BEFORE the
// active tool)... anyhoo, I have no intention of rejiggering things now, so
// instead this hack will let the active tool tell the mask tool that an A
// was received and then can look at the return code which will be TRUE if
// the mask tool made use of the A key, or will be FALSE if the A key did
// not affect the mask state!!
////////////////////////////////////////////////////////////////////////////

namespace {
   CNavigatorTool *findNavigatorTool()
   {
      CToolManager toolManager;
      int navToolIndex = toolManager.findTool("Navigator");    // QQQ Manifest constant!!
      if (navToolIndex < 0)
         return nullptr;

      CToolObject *toolObj = toolManager.GetTool(navToolIndex);
      CNavigatorTool *navigatorTool = dynamic_cast<CNavigatorTool *>( toolObj );
      MTIassert(navigatorTool != nullptr);

      return navigatorTool;
   }
} // Anonymous namespace

//void CNavSystemInterface::TellTheMaskToolToUnselectAll()
//{
//   if (IsMaskEnabled())
//   {
//      CNavigatorTool *navigatorTool = findNavigatorTool();
//
//      if (navigatorTool != nullptr && navigatorTool->GetMaskTool() != nullptr)
//      {
//         navigatorTool->tellTheMaskToolToUnselectAll();
//      }
//   }
//}

//bool CNavSystemInterface::AskTheMaskToolIfAnythingIsSelected()
//{
//   bool retVal = false;
//
//   if (IsMaskEnabled())
//   {
//      CNavigatorTool *navigatorTool = findNavigatorTool();
//
//      if (navigatorTool != nullptr && navigatorTool->GetMaskTool() != nullptr)
//      {
//         retVal = navigatorTool->askTheMaskToolIfAnythingIsSelected();
//      }
//   }
//
//   return retVal;
//}

void CNavSystemInterface::setRectToMask(const RECT &rect)
{
   auto navigatorTool = findNavigatorTool();
   if (navigatorTool == nullptr)
   {
      return;
   }

   navigatorTool->setRectToMask(rect);
}

void CNavSystemInterface::setMaskRoiIsRectangularOnly(bool flag)
{
   CNavigatorTool *navigatorTool = findNavigatorTool();
   if (navigatorTool == nullptr)
   {
      return;
   }

   CMaskTool *maskTool = navigatorTool->GetMaskTool();
   if (maskTool == nullptr)
   {
      return;
   }

   maskTool->setRoiIsRectangularOnly(flag);
}

//void CNavSystemInterface::StartRoiExclusiveMode()
//{
//   CNavigatorTool *navigatorTool = findNavigatorTool();
//
//   if (navigatorTool != nullptr)
//   {
//      CMaskTool *maskTool = navigatorTool->GetMaskTool();
//
//      if (maskTool != nullptr)
//      {
//         maskTool->StartRoiExclusiveMode();
//      }
//   }
//}

//void CNavSystemInterface::EndRoiExclusiveMode()
//{
//   CNavigatorTool *navigatorTool = findNavigatorTool();
//
//   if (navigatorTool != nullptr)
//   {
//      CMaskTool *maskTool = navigatorTool->GetMaskTool();
//
//      if (maskTool != nullptr)
//      {
//         maskTool->EndRoiExclusiveMode();
//      }
//   }
//}

///// HACK //// HACK //// HACK //// HACK //// HACK //// HACK ////
//
// This is way different from RefreshFrame because it bypasses the
// Player and talks directly to the displayer which is critical for
// smooth interactivity for tools (like Grain) that need a shape (i.e
// a "brush") to be drawn on the frame.
//
/////////////   This is intended to be called ONLY from a tool's
// CAUTION //   onMouseMove handler, I imagine havoc would
/////////////   result from calling it from threads or whatnot!!!!!
//
void CNavSystemInterface::TellTheDisplayerToRedisplayTheCurrentFrame()
{
   if (!isDisplaying())
   {
      mainWindow->GetDisplayer()->displayFrame(); // back door!
   }
}

///// HACK //// HACK //// HACK //// HACK //// HACK //// HACK ////
//
// Tell the displayer to draw a shape outline (e.g. the Grain Tool brush)
// for us on the frame.
//
/////////////   This is intended to be called ONLY from a tool's
// CAUTION //   onRedraw handler, I imagine havoc would
/////////////   result from calling it from elsewhere!!!!!
//
void CNavSystemInterface::TellTheDisplayerToDrawAShape(
      double centerX, double centerY,
      int shapeType, int radius, int aspectRatio, int angle,
      MTI_UINT8 R, MTI_UINT8 G, MTI_UINT8 B)
{
   // back door!
   mainWindow->GetDisplayer()->drawShapeOnFrameNoFlush(
      centerX, centerY, shapeType, radius, aspectRatio, angle, R, G, B);
}

///////////////////////////////////////////////////////////////////////////////
// Reticle Tool Interface
///////////////////////////////////////////////////////////////////////////////

void CNavSystemInterface::InternalDisableReticleTool()
{
   CNavigatorTool::GetReticleTool()->setReticleToolInternalEnabled(false);
   CNavigatorTool::ExecuteReticleToolCommand(NAV_CMD_RETICLE_TOOL_DISABLE);
}

void CNavSystemInterface::InternalEnableReticleTool()
{
   CNavigatorTool::GetReticleTool()->setReticleToolInternalEnabled(true);
   CNavigatorTool::ExecuteReticleToolCommand(NAV_CMD_RETICLE_TOOL_ENABLE);
}

///////////////////////////////////////////////////////////////////////////////
// Bezier Editor Interface for Selection Regions
///////////////////////////////////////////////////////////////////////////////

// NB: This interface is merely sufficient to support dirt selection
//     in DRS.  At some future time we should bring selection drawing up into
//     the Navigator and integrate it with the Mask Tool and also Rectangle
//     and Lasso drawing.

void CNavSystemInterface::BezierMouseDown()
{
   CBezierEditor *bezierEditor = getBezierEditor();

   // pass event to bezier editor
   bezierEditor->setCtrl(false);
   bezierEditor->setShift(false);
   bezierEditor->mouseDown();
}

void CNavSystemInterface::BezierMouseMove(int x, int y)
{
   CBezierEditor *bezierEditor = getBezierEditor();

   bezierEditor->mouseMove(x, y);
}

void CNavSystemInterface::BezierMouseUp()
{
   CBezierEditor *bezierEditor = getBezierEditor();

   // pass event to bezier editor
   bezierEditor->setCtrl(false);
   bezierEditor->setShift(false);
   bezierEditor->mouseUp();
}

void CNavSystemInterface::BezierCtrlMouseDown()
{
   CBezierEditor *bezierEditor = getBezierEditor();

   // pass event to bezier editor
   bezierEditor->setShift(false);
   bezierEditor->setCtrl(true);
   bezierEditor->mouseDown();
}

void CNavSystemInterface::BezierShiftMouseDown()
{
   CBezierEditor *bezierEditor = getBezierEditor();

   // pass event to bezier editor
   bezierEditor->setCtrl(false);
   bezierEditor->setShift(true);
   bezierEditor->mouseDown();
}

void CNavSystemInterface::ClearBezier(bool doRedraw)
{
   CBezierEditor *bezierEditor = getBezierEditor();

   // DRS SPEEDUP
   // Don't do anything if there's no bezier; if we don't do this
   // we read the frame for no good reason to clear a non-existent
   // bezier, which slows down DRS
   if (bezierEditor->isEmpty())
      return;

   // clear out the editor
   bezierEditor->newSpline();

   // if requested, clear any bezier graphics from client area
   if (doRedraw)
      refreshFrameCached();
}

bool CNavSystemInterface::IsBezierClosed()
{
   CBezierEditor *bezierEditor = getBezierEditor();

   return bezierEditor->isClosed();
}

void CNavSystemInterface::FreeBezier(BEZIER_POINT *bezierPts)
{
   // Deallocates a Bezier returned by GetBezier() function.

   delete [] bezierPts;
}

bool CNavSystemInterface::GetBezierExtent(RECT& bezrct)
{
   // Gets the extent of the current bezier spline
   // Returns TRUE if the extent touches the frame
   // Returns FALSE if it's entirely outside
   CBezierEditor *bezierEditor = getBezierEditor();

   return(bezierEditor->getCompletedSplineExtent(bezrct));
}

BEZIER_POINT* CNavSystemInterface::GetBezier()
{
   // Returns a pointer to a bezier spline.
   // Returns nullptr if the bezier is not closed yet

   // The Bezier Points returned by this function are allocated on
   // the head and must be deallocated by the caller.  Example
   //     BEZIER_POINT *bezPts = getSystemAPI()->GetBezier();
   //     use the bezier somehow...
   //     getSystemAPI()->FreeBezier(bezPts);

   CBezierEditor *bezierEditor = getBezierEditor();

   return bezierEditor->getCompletedSpline();
}

void CNavSystemInterface::RedrawBezier()
{
   CBezierEditor *bezierEditor = getBezierEditor();

   bezierEditor->drawSplineFrame();
}

CBezierEditor* CNavSystemInterface::getBezierEditor()
{
   if (navBezierEditor == 0)
      {
      navBezierEditor = new CBezierEditor(mainWindow->GetDisplayer());
      }

   return navBezierEditor;
}

///////////////////////////////////////////////////////////////////////////////
// Transform Editor Interface for Paint II
///////////////////////////////////////////////////////////////////////////////

void CNavSystemInterface::SetToolPaintInterface(CToolPaintInterface *ptintfc)
{
   CTransformEditor *transformEditor = getTransformEditor();

   transformEditor->setToolPaintInterface(ptintfc);
}

void CNavSystemInterface::TransformPositionImportFrame()
{
   CTransformEditor *transformEditor = getTransformEditor();

   // pass event to transform editor
   transformEditor->positionImportFrame();
}

void CNavSystemInterface::TransformMouseDown()
{
   CTransformEditor *transformEditor = getTransformEditor();

   // pass event to bezier editor
   transformEditor->setCtrl(false);
   transformEditor->setShift(false);
   transformEditor->mouseDown();
}

void CNavSystemInterface::TransformMouseMove(int x, int y)
{
   CTransformEditor *transformEditor = getTransformEditor();

   transformEditor->mouseMove(x, y);
}

void CNavSystemInterface::TransformMouseUp()
{
   CTransformEditor *transformEditor = getTransformEditor();

   // pass event to bezier editor
   transformEditor->setCtrl(false);
   transformEditor->setShift(false);
   transformEditor->mouseUp();
}

void CNavSystemInterface::TransformCtrlMouseDown()
{
   CTransformEditor *transformEditor = getTransformEditor();

   // pass event to bezier editor
   transformEditor->setShift(false);
   transformEditor->setCtrl(true);
   transformEditor->mouseDown();
}

void CNavSystemInterface::TransformShiftMouseDown()
{
   CTransformEditor *transformEditor = getTransformEditor();

   // pass event to bezier editor
   transformEditor->setCtrl(false);
   transformEditor->setShift(true);
   transformEditor->mouseDown();
}

void CNavSystemInterface::ClearTransform(bool doRedraw)
{
   CTransformEditor *transformEditor = getTransformEditor();

   // clear out the editor
   transformEditor->resetTransform();

   // clear any transform graphics from client area
   if (doRedraw)
      refreshFrameCached();
}

void CNavSystemInterface::SaveTransform()
{
   CTransformEditor *transformEditor = getTransformEditor();

   // save the transform
   transformEditor->saveTransform();
}

void CNavSystemInterface::RestoreTransform()
{
   CTransformEditor *transformEditor = getTransformEditor();

   // restore the transform
   transformEditor->restoreTransform();
}

void CNavSystemInterface::SetRotation(double rot)
{
   CTransformEditor *transformEditor = getTransformEditor();

   // set the transform
   transformEditor->setRotation(rot);
}

void CNavSystemInterface::SetSkew(double skew)
{
   CTransformEditor *transformEditor = getTransformEditor();

   // set the transform
   transformEditor->setSkew(skew);
}

void CNavSystemInterface::SetStretchX(double strx)
{
   CTransformEditor *transformEditor = getTransformEditor();

   // set the transform
   transformEditor->setStretchX(strx);
}

void CNavSystemInterface::SetStretchY(double stry)
{
   CTransformEditor *transformEditor = getTransformEditor();

   // set the transform
   transformEditor->setStretchY(stry);
}

void CNavSystemInterface::SetOffsetX(double offx)
{
   CTransformEditor *transformEditor = getTransformEditor();

   // set the transform
   transformEditor->setOffsetX(offx);
}

void CNavSystemInterface::SetOffsetY(double offy)
{
   CTransformEditor *transformEditor = getTransformEditor();

   // set the transform
   transformEditor->setOffsetY(offy);
}

void CNavSystemInterface::SetOffsetXY(double offx, double offy)
{
   CTransformEditor *transformEditor = getTransformEditor();

   // set the transform
   transformEditor->setOffsetXY(offx, offy);
}

void CNavSystemInterface::SetTransform(double rot, double strx, double stry,
                                       double skew, double offx, double offy)
{
   CTransformEditor *transformEditor = getTransformEditor();

   // set the transform
   transformEditor->setTransform(rot, strx, stry, skew, offx, offy);
}

void CNavSystemInterface::GetTransform(double *rotp, double *strxp, double *stryp,
                                       double *skewp, double *offxp, double *offyp)
{
   CTransformEditor *transformEditor = getTransformEditor();

   // get the transform
   transformEditor->getTransform(rotp, strxp, stryp, skewp, offxp, offyp);
}

void CNavSystemInterface::SetMatBox(RECT *boxp)
{
   mainWindow->GetDisplayer()->setMatBox(boxp);
}

void CNavSystemInterface::GetMatBox(RECT *boxp)
{
   mainWindow->GetDisplayer()->getMatBox(boxp);
}

void CNavSystemInterface::SetRegistrationTransform(AffineCoeff *xfrm)
{
   mainWindow->GetDisplayer()->setRegistrationTransform(xfrm);
}

void CNavSystemInterface::GetRegistrationTransform(AffineCoeff *xfrm)
{
   mainWindow->GetDisplayer()->getRegistrationTransform(xfrm);
}

void CNavSystemInterface::RedrawTransform()
{
   CTransformEditor *transformEditor = getTransformEditor();

   transformEditor->drawTransform();
}

void CNavSystemInterface::TransformTimer()
{
   CTransformEditor *transformEditor = getTransformEditor();

   transformEditor->timeTransform();
}

CTransformEditor* CNavSystemInterface::getTransformEditor()
{
   if (navTransformEditor == 0)
      {
      navTransformEditor = new CTransformEditor(mainWindow->GetDisplayer());
      }

   return navTransformEditor;
}

////////////////////////////////////////////////////////////////////////////////

double CNavSystemInterface::dscaleXClientToFrame(int x)
{
   return(mainWindow->GetDisplayer()->dscaleXClientToFrame(x));
}

double CNavSystemInterface::dscaleYClientToFrame(int y)
{
   return(mainWindow->GetDisplayer()->dscaleYClientToFrame(y));
}

int CNavSystemInterface::scaleXFrameToClient(int x)
{
   return(mainWindow->GetDisplayer()->scaleXFrameToClient(x));
}

int CNavSystemInterface::scaleYFrameToClient(int y)
{
   return(mainWindow->GetDisplayer()->scaleYFrameToClient(y));
}

void CNavSystemInterface::drawUnfilledBoxFrameNoFlush(double x, double y)
{
   mainWindow->GetDisplayer()->drawUnfilledBoxFrameNoFlush(x, y);
}

void CNavSystemInterface::drawFilledBoxFrameNoFlush(double x, double y)
{
   mainWindow->GetDisplayer()->drawFilledBoxFrameNoFlush(x, y);
}

void CNavSystemInterface::drawUnfilledBoxScaledNoFlush(double x, double y, double rad)
{
   mainWindow->GetDisplayer()->drawUnfilledBoxScaledNoFlush(x, y, rad);
}

void CNavSystemInterface::drawFilledBoxScaledNoFlush(double x, double y, double rad)
{
   mainWindow->GetDisplayer()->drawFilledBoxScaledNoFlush(x, y, rad);
}

void CNavSystemInterface::drawArrowFrameNoFlush(double frx, double fry,
                                                double tox, double toy,
                                                double wdt, double len)
{
   mainWindow->GetDisplayer()->drawArrowFrameNoFlush(frx, fry,
                                                     tox, toy,
                                                     wdt, len);
}

void CNavSystemInterface::flushGraphics()
{
   mainWindow->GetDisplayer()->flushGraphics();
}

// New accurate pixel tracking boxes for stabilization

void CNavSystemInterface::drawTrackingBoxNoFlush(double centerX, double centerY,
                                                 double innerRadiusX, double innerRadiusY,
                                                 double outerRadiusX, double outerRadiusY,
                                                 bool showCrosshairs)
{
   mainWindow->GetDisplayer()->drawTrackingBoxNoFlush(centerX, centerY,
                                                      innerRadiusX, innerRadiusY,
                                                      outerRadiusX, outerRadiusY,
                                                      showCrosshairs);
}

// Methods to pass the Component (YUV) values from Navigator

int CNavSystemInterface::GetYcomp()
{
        return mainWindow->GetYcomp();
}

int CNavSystemInterface::GetUcomp()
{
        return mainWindow->GetUcomp();
}

int CNavSystemInterface::GetVcomp()
{
        return mainWindow->GetVcomp();
}

int CNavSystemInterface::getBitsPerComponent()
{
        return mainWindow->getBitsPerComponent();
}

int CNavSystemInterface::getPixelComponents()
{
        return mainWindow->getPixelComponents();
}

bool CNavSystemInterface::isMouseInFrame()
{
        return mainWindow->isMouseInFrame();
}


//////////////////////////////////////////////////////////////////////
// Cursor control interface
//////////////////////////////////////////////////////////////////////

void CNavSystemInterface::showCursor(bool showFlag)
{
   mainWindow->ShowCursor(showFlag);
}

void CNavSystemInterface::enterCursorOverrideMode(int CursorIndex)
{
   return mainWindow->GetDisplayer()->enterCursorOverrideMode(CursorIndex);
}

void CNavSystemInterface::exitCursorOverrideMode()
{
   mainWindow->GetDisplayer()->exitCursorOverrideMode();
}

void CNavSystemInterface::screenToClientCoords(int inX, int inY,
                                               int &outX, int &outY)
{
   mainWindow->GetDisplayer()->screenToClientCoords(inX, inY, outX, outY);
}

void CNavSystemInterface::informToolWindowStatus(bool windowActive)
{
   mainWindow->informToolWindowStatus(windowActive);
}

void CNavSystemInterface::setPreferredFrameCursor(TCursor cursor)
{
   mainWindow->GetDisplayer()->setPreferredFrameCursor(cursor);
}

//////////////////////////////////////////////////////////////////////
// Unified  Tool API
//////////////////////////////////////////////////////////////////////

int CNavSystemInterface::AdoptPluginToolGUI(const string &toolName,
                                            int tabIndex,
                                            int *topPanel, // (TPanel *)
                                  FosterParentEventHandlerT eventHandler)
{
   return BaseToolWindow->AdoptPluginToolGUI(toolName, tabIndex, topPanel,
                                             eventHandler);
}

void CNavSystemInterface::UnadoptPluginToolGUI(int tabIndex)
{
   BaseToolWindow->UnadoptPluginToolGUI(tabIndex);
}

void CNavSystemInterface::FocusAdoptedControl(int *control)
{
   if (BaseToolWindow->Visible)
      BaseToolWindow->FocusAdoptedControl(control);
}

int *CNavSystemInterface::GetFocusedAdoptedControl(void)
{
   return BaseToolWindow->GetFocusedAdoptedControl();
}

void CNavSystemInterface::DisableTool(EToolDisabledReason reason)
{
   BaseToolWindow->DisableTool(reason);
}

void CNavSystemInterface::EnableTool()
{
   BaseToolWindow->EnableTool();
}

// This returns true if the user responded OK to the warning, so processing
// should continue
bool CNavSystemInterface::WarnThereIsNoUndo()
{
   bool retVal = true;

   // No pop-ups in PDL rendering mode! Just pretend the user said OK!
   if (!IsPDLRendering())
   {
      retVal = BaseToolWindow->WarnThereIsNoUndo();
   }

   return retVal;
}

int *CNavSystemInterface::GetBaseToolFormHandle()
{
   TCustomForm *customForm = static_cast<TCustomForm *>(BaseToolWindow);
   return reinterpret_cast<int *>(customForm);
}

EAcceptOrRejectChangeResult CNavSystemInterface::ShowAcceptOrRejectDialog(
                                                 bool *turnOnAutoAccept)
{
   return BaseToolWindow->ShowAcceptOrRejectDialog(turnOnAutoAccept);
}

//////////////////////////////////////////////////////////////////////
// GOV Tool API
//////////////////////////////////////////////////////////////////////

bool CNavSystemInterface::NotifyGOVStarting(bool *cancelStretch)
{
   CToolManager toolManager;
   return toolManager.NotifyGOVStarting(cancelStretch);
}

void CNavSystemInterface::NotifyGOVDone()
{
   CToolManager toolManager;
   toolManager.NotifyGOVDone();
}

void CNavSystemInterface::NotifyGOVShapeChanged(ESelectedRegionShape shape,
                                                const RECT &rect,
                                                const POINT *lasso)
{
   CToolManager toolManager;
   toolManager.NotifyGOVShapeChanged(shape, rect, lasso);
}

void CNavSystemInterface::NotifyRightClicked()
{
   CToolManager toolManager;
   toolManager.NotifyRightClicked();
}

namespace {
   CGOVTool *findGOVTool()
   {
      CToolManager toolManager;
      int govToolIndex = toolManager.findTool("GOV");    // QQQ Manifest constant!!
      if (govToolIndex < 0)
         return nullptr;

      CToolObject *toolObj = toolManager.GetTool(govToolIndex);
      CGOVTool *govTool = dynamic_cast<CGOVTool *>( toolObj );
      MTIassert(govTool != nullptr);

      return govTool;
   }
} // Anonymous namespace

bool CNavSystemInterface::IsGOVPending()
{
   bool retVal = false;
   CGOVTool *govTool = findGOVTool();

   if (govTool != nullptr)
      retVal = govTool->IsGOVPending();

   return retVal;
}

bool CNavSystemInterface::IsGOVInProgress()
{
   bool retVal = false;
   CGOVTool *govTool = findGOVTool();

   if (govTool != nullptr)
      retVal = govTool->IsGOVInProgress();

   return retVal;
}

void CNavSystemInterface::AcceptGOV()
{
   CGOVTool *govTool = findGOVTool();
   if (govTool != nullptr)
      govTool->AcceptGOVIfPending();
}

void CNavSystemInterface::RejectGOV()
{
   CGOVTool *govTool = findGOVTool();
   if (govTool != nullptr)
      govTool->RejectGOVIfPending();
}

void CNavSystemInterface::SetTildeKeyHackMode(ETildeKeyHackMode newMode)
{
   CGOVTool *govTool = findGOVTool();
   if (govTool != nullptr)
      govTool->SetTildeKeyHackMode(newMode);
}

void CNavSystemInterface::SetGOVRepeatShape(ESelectedRegionShape shape,
                                            const RECT &rect,
                                            const POINT *lasso,
                                            const CPixelRegionList *regionList)
{
   CGOVTool *govTool = findGOVTool();
   if (govTool != nullptr)
      govTool->SetGOVRepeatShape(shape, rect, lasso, regionList);
}

void CNavSystemInterface::SetGOVToolProgressMonitor(
                          IToolProgressMonitor *newToolProgressMonitor)
{
   CGOVTool *govTool = findGOVTool();
   if (govTool != nullptr)
      govTool->SetGOVToolProgressMonitor(newToolProgressMonitor);
}

//////////////////////////////////////////////////////////////////////
// Tracking Pseudotool API                                          //
//////////////////////////////////////////////////////////////////////

// from Tracking to active tool:
bool CNavSystemInterface::NotifyTrackingStarting()
{
   CToolManager toolManager;
   return toolManager.NotifyTrackingStarting();
}

void CNavSystemInterface::NotifyTrackingDone(int errorCode)
{
   CToolManager toolManager;
   toolManager.NotifyTrackingDone(errorCode);
}

void CNavSystemInterface::NotifyTrackingCleared()
{
   CToolManager toolManager;
   toolManager.NotifyTrackingCleared();
}

namespace {
   CTrackingTool *findTrackingTool()
   {
      CToolManager toolManager;
      int trkToolIndex = toolManager.findTool("Tracking");  // QQQ Manifest constant!!
      if (trkToolIndex < 0)
         return nullptr;

      CToolObject *toolObj = toolManager.GetTool(trkToolIndex);
      CTrackingTool *trkTool = dynamic_cast<CTrackingTool *>( toolObj );
      MTIassert(trkTool != nullptr);

      return trkTool;
   }
} // Anonymous namespace

// from active tool to Tracking:
bool CNavSystemInterface::LockTrackingTool()
{
   CTrackingTool *trkTool = findTrackingTool();
   if (trkTool != nullptr)
   {
	  trkTool->LockTrackingTool();
   }

  return true;
}

void CNavSystemInterface::ReleaseTrackingTool()
{
   CTrackingTool *trkTool = findTrackingTool();
   if (trkTool != nullptr)
      trkTool->ReleaseTrackingTool();
}

void CNavSystemInterface::SetTrackingToolEditor(TrackingBoxManager *newEditor)
{
   CTrackingTool *trkTool = findTrackingTool();
   if (trkTool != nullptr)
      trkTool->SetTrackingToolEditor(newEditor);
}

void CNavSystemInterface::SetTrackingToolProgressMonitor(
                          IToolProgressMonitor *newToolProgressMonitor)
{
   CTrackingTool *trkTool = findTrackingTool();
   if (trkTool != nullptr)
      trkTool->SetTrackingToolProgressMonitor(newToolProgressMonitor);
}

void CNavSystemInterface::ActivateTrackingTool()
{
   CTrackingTool *trkTool = findTrackingTool();
   if (trkTool != nullptr)
      trkTool->EnableTrackingTool();
}

void CNavSystemInterface::DeactivateTrackingTool()
{
   CTrackingTool *trkTool = findTrackingTool();
   if (trkTool != nullptr)
      trkTool->DisableTrackingTool();
}

void CNavSystemInterface::StartTracking()
{
   CTrackingTool *trkTool = findTrackingTool();
   if (trkTool != nullptr)
      trkTool->StartTracking();
}

void CNavSystemInterface::StopTracking()
{
   CTrackingTool *trkTool = findTrackingTool();
   if (trkTool != nullptr)
      trkTool->StopTracking();
}



//////////////////////////////////////////////////////////////////////
// Tool Frame Cache API
//////////////////////////////////////////////////////////////////////

CToolFrameCache *CNavSystemInterface::GetToolFrameCache()
{
   CToolManager toolManager;
   return toolManager.GetToolFrameCache();
}


//////////////////////////////////////////////////////////////////////
// Background thread support
//////////////////////////////////////////////////////////////////////

void CNavSystemInterface::FireRefreshTimerFromBackground()
{
   // Tell the player to refresh the main window as soon as possible
   // (within a msec)
   CPlayer *player = mainWindow->GetPlayer();
   player->expediteMainWindowRefresh();
}

//////////////////////////////////////////////////////////////////////
// AutoClean API
//////////////////////////////////////////////////////////////////////

bool CNavSystemInterface::IsAutoCleanEnabled()
{
   // Yes! THis sucks! Duplicates declarations in Clip3.h AND I think
   // MediaStorage3.h
   const string historySectionName = "History";
   const string enableAutoCleanKey = "EnableAutoClean";

   bool enableAutoClean = false;
   CIniFile *localMachineIniFile = CPMPOpenLocalMachineIniFile();
   if (localMachineIniFile != nullptr)
   {
      localMachineIniFile->ReadOnly(true);

      ////////////////// in MTILocalMachine.ini
      //
      // [History]
      // HistoryType=FILE_PER_CLIP      (default, or FILE_PER_FRAME)
      // EnableAutoClean=false          (default)
      //////////////////

      enableAutoClean = localMachineIniFile->ReadBool(historySectionName,
                                                      enableAutoCleanKey,
                                                      enableAutoClean);
      DeleteIniFile(localMachineIniFile);
   }

   return enableAutoClean;
}

//////////////////////////////////////////////////////////////////////
// Debris File API
//////////////////////////////////////////////////////////////////////

//int CNavSystemInterface::GetAutoCleanDebrisVersion()
//{
//	// We are assuming that all debris have same versions
//	auto debrisFileName = GetAutoCleanDebrisFileName(0 );
//	if (debrisFileName.empty())
//	{
//		return ERR_AUTOCLEAN_NO_DEBRIS_DATA;
//	}
//
//	int retVal = 0;
//	auto debrisFile = new CAutoCleanDebrisFile(debrisFileName, retVal, 0);
//	if (retVal != 0)
//	{
//		return retVal;
//	}
//
//	auto version = debrisFile->GetFileVersion();
//	delete debrisFile;
//	return version;
//}
//CAutoCleanDebrisFile *CNavSystemInterface::GetAutoCleanDebrisFile(int frameIndex)
//{
//	string debrisFileName = GetAutoCleanDebrisFileName(frameIndex);
//	if (debrisFileName.empty())
//	{
//		return nullptr;
//	}
//
//	int result = 0;
//	CAutoCleanDebrisFile *debrisFile = new CAutoCleanDebrisFile(debrisFileName, result, frameIndex);
//
//	return debrisFile;
//}

//void CNavSystemInterface::ReleaseAutoCleanDebrisFile(CAutoCleanDebrisFile *debrisFile)
//{
//   delete debrisFile;
//}

//////////////////////////////////////////////////////////////////////
// AutoClean Display Hack-O-Rama
//////////////////////////////////////////////////////////////////////

void CNavSystemInterface::SetAutoCleanOverlayList(const OverlayDisplayList &newList)
{
   // trampoline!
   mainWindow->GetDisplayer()->SetAutoCleanOverlayList(newList);
}

void CNavSystemInterface::MakeSureThisRectIsVisible(const RECT &rect)
{
    mainWindow->GetDisplayer()->MakeSureThisRectIsVisible(rect);
}

// HAAAAACK - Sucks the actual LUT values that the Displayer uses to convert
// the clip native format to screen values - returns 768 valuse:
// 256 x R, 256 x G, 256 x B !!!
void CNavSystemInterface::GetDisplayLutValues(MTI_UINT8* outputValues)
{
   mainWindow->GetDisplayer()->GetDisplayLutValues(outputValues);
}

int CNavSystemInterface::GetListOfSmallHistoryRectangles(
         int frameIndex,
         int sizeLimit,       // max size of a side, in pixels
         vector<RECT> &rectList)    // output
{
   CSaveRestore *reviewHistory = mainWindow->GetPlayer()->getReviewHistory();
   rectList.clear();

   int retVal;
   CAutoHistoryOpener opener(*reviewHistory, FLUSH_TYPE_RESTORE,
                             frameIndex, ALL_FIELDS, 0, 0, 0, retVal);
   if (retVal == 0)
   {
      while (reviewHistory->nextRestoreRecord() == 0)
      {
         if (!reviewHistory->getCurrentRestoreRecordResFlag())
         {
            CPixelRegionList *pixelRegionList =
               reviewHistory->getCurrentRestoreRecordPixelRegionList();
            if (pixelRegionList != nullptr)
            {
               for (int i = 0; i < pixelRegionList->GetEntryCount(); ++i)
               {
                  CPixelRegion *rgn = pixelRegionList->GetEntry(i);
                  if (rgn->GetType() == PIXEL_REGION_TYPE_RECT)
                  {
                     RECT histRect = ((CRectPixelRegion *) rgn)->GetRect();

                     if ((histRect.bottom - histRect.top + 1) <= sizeLimit
                     && (histRect.right - histRect.left + 1) <= sizeLimit)
                     {
                        rectList.push_back(histRect);
                     }
                  }
               }

               delete pixelRegionList;
            }
         }
      }
   }

   return 0;
}

bool CNavSystemInterface::IsPlayerReallyPlaying()
{
    return mainWindow->GetPlayer()->isReallyPlaying();
}

void CNavSystemInterface::IDontNeedThisKeyDownEvent(WORD &Key, TShiftState Shift)
{
   CToolManager toolManager;
   toolManager.onKeyDown(Key, Shift);
}

void CNavSystemInterface::enterPreviewHackMode()
{
   mainWindow->GetDisplayer()->enterPreviewHackMode();
   if (!mainWindow->GetPlayer()->isDisplaying())
   {
      mainWindow->GetPlayer()->refreshFrameSynchronousCached();
   }
}

void CNavSystemInterface::exitPreviewHackMode()
{
   mainWindow->GetDisplayer()->exitPreviewHackMode();
   if (!mainWindow->GetPlayer()->isDisplaying())
   {
      mainWindow->GetPlayer()->refreshFrameSynchronousCached();
   }
}

void CNavSystemInterface::NotifyStartViewOnlyMode()
{
	CToolManager toolManager;
	toolManager.NotifyStartViewOnlyMode();
}

void CNavSystemInterface::NotifyEndViewOnlyMode()
{
	CToolManager toolManager;
	toolManager.NotifyEndViewOnlyMode();
}

///////////////////////////////////////////////////////////////////////////
// Main Window Top Panel API
void CNavSystemInterface::SetMainWindowTopPanelVisibility(bool onOff)
{
    mainWindow->SetTopPanelVisibility(onOff);
}

bool CNavSystemInterface::GetMainWindowTopPanelVisibility()
{
    return mainWindow->GetTopPanelVisibility();
}

CMTIBitmap *CNavSystemInterface::GetBitmapRefForTopPanelDrawing()
{
    return &mainWindow->GetBitmapRefForTopPanelDrawing();
}

void CNavSystemInterface::DrawBitmapOnMainWindowTopPanel()
{
    mainWindow->DrawBitmapOnTopPanel();
}

///////////////////////////////////////////////////////////////////////////
// Uer Guide Viewer API
void CNavSystemInterface::SetToolNameForUserGuide(const string &toolName)
{
	UserGuideViewer userGuideViewer;
	userGuideViewer.setActiveToolName(toolName);
}


////////////////////////////////////////////////////////////////////////////
// In the following:
// - Load up the outBuf with corresponding current pixels from the frame to
// avoid grief in the case some original pixels are missing
// - the right and bottom of filterRect are INCLUSIVE

int CNavSystemInterface::GetOriginalValues(int frameIndex,
                                           RECT filterRect,
                                           MTI_UINT16 *outBuf)
{
   int retVal = 0;
   CSaveRestore saveRestore;
   auto clip = mainWindow->GetCurrentClipHandler()->getCurrentClip();
   saveRestore.setClip(clip);

   ///////////////////////////////////////////////////////////////////////
   // Splendid hack.. it seems there is no way to just get the original
   // values directly.... instead we have to allocate a FULL-SIZED internal
   // format dummy frame buffer, initialize the patch that we are interested
   // in, restore the history values to that, then extract our patch!!
   ///////////////////////////////////////////////////////////////////////

   const CImageFormat *imageFormat = clip->getImageFormat(VIDEO_SELECT_NORMAL);
   int fullSizeWidth = imageFormat->getPixelsPerLine();
   int fullSizeHeight = imageFormat->getLinesPerFrame();
   int patchWidth = filterRect.right - filterRect.left + 1;
   int patchHeight = filterRect.bottom - filterRect.top + 1;

   // For sanity, let's refuse to do anything if it's not 3 components or
   // if it has "intervals" or is interlaced!!
   if (imageFormat->getComponentCountExcAlpha() != 3
   ||  imageFormat->getVerticalIntervalRows() != 0
   ||  imageFormat->getHorizontalIntervalPixels() != 0
   ||  imageFormat->getInterlaced() == true)
   {
      return 0;
   }

   // Grab memory for the dummy frame buffer
   int allocSize = fullSizeWidth * fullSizeHeight * 3 * sizeof(MTI_UINT16);
   MTI_UINT16 *dummyFrameBuffer = (MTI_UINT16 *) malloc(allocSize);

   // Initialize the patch we are interested in
   for (int row = 0; row < patchHeight; ++row)
   for (int col = 0; col < patchWidth; ++col)
   {
      int dummyIndex = 3 *(((row + filterRect.top) * fullSizeWidth)
                            + (col + filterRect.left));
      int patchIndex = 3 * ((row * patchWidth) + col);

      for (int comp = 0; comp < 3; ++comp)
      {
         dummyFrameBuffer[dummyIndex] = outBuf[patchIndex];
      }
   }

   // Restore original values to the dummy frame buffer

   int errorCode = 0;
   CAutoHistoryOpener opener(saveRestore, FLUSH_TYPE_RESTORE,
                             frameIndex, -1,
                             &filterRect, nullptr, nullptr,
                             errorCode);
   if (errorCode == 0)
   {
      // Gaaah... seems we need to try to restore ALL of the history records
      // for the given frame!!!
      while (saveRestore.nextRestoreRecord() == 0)
      {
         // We enforced non-interlaced above, so should only see field index 0
         MTIassert(saveRestore.getCurrentRestoreRecordFieldIndex() == 0);

         // Try to restore from this record
         retVal = saveRestore.restoreCurrentRestoreRecordPixels(dummyFrameBuffer,
                                                                true);
         if (retVal != 0)
               break;
      }

      // OK, copy the patch back out!
      for (int row = 0; row < patchHeight; ++row)
      for (int col = 0; col < patchWidth; ++col)
      {
         int dummyIndex = 3 *(((row + filterRect.top) * fullSizeWidth)
                               + (col + filterRect.left));
         int patchIndex = 3 * ((row * patchWidth) + col);

         for (int comp = 0; comp < 3; ++comp)
         {
            outBuf[patchIndex] = dummyFrameBuffer[dummyIndex];
         }
      }
   }

   // Done!
   delete[] dummyFrameBuffer;
   return retVal;
}


//////////////////////////////////////////////////////////////////////
// CHistoryReviewList Class Implementation
//////////////////////////////////////////////////////////////////////

CHistoryReviewList::CHistoryReviewList(CSaveRestore *newSaveRestoreEngine)
{
   saveRestoreEngine = newSaveRestoreEngine;
   reviewFrame = -1;
   reviewListIndex = -1;
   reviewListInvalid = true;
}

CHistoryReviewList::~CHistoryReviewList()
{
   // Nothing to do
}

int CHistoryReviewList::getCurrentReviewRegion(CPixelRegionList &reviewRegion)
{
   // Check for anything that would prevent access to review list
   if (reviewListInvalid                             // Obsolete review list
       || reviewFrame == -1                          // reviewFrame invalid
       || reviewListIndex < 0                        // index not set or invalid
       || reviewListIndex >= getReviewRegionCount()) // index beyond list
      {
      // Cannot return anything
      return -1;
      }

   // Copy current review region to caller's CPixelRegionList
   CPixelRegionList *currentReviewRegion
                                     = reviewList[reviewListIndex].pixelRegion;
   if (currentReviewRegion != 0)
      reviewRegion = *currentReviewRegion;

   return 0; // return success
}

int CHistoryReviewList::getReviewRegionCount()
{
   // Check for conditions that indicate that contents of review list
   // cannot be trusted
   if (reviewListInvalid         // Obsolete review list
       || reviewFrame == -1)     // Invalid reviewFrame
      {
      return 0;
      }
   else
      {
      // Return number of entries in reviewList
      return (int)reviewList.size();
      }
}

int CHistoryReviewList::saveForMarkAllReviewRegions()
{
   markList.clear();

   int reviewCount = (int)reviewList.size();
   if (reviewCount == 0)
      {
      // There is nothing in the review list to save
      return 0;
      }

   // Copy save numbers from review list to mark list
   for (int index = 0; index < reviewCount; ++index)
      {
      markList.push_back(reviewList[index].saveNumber);
      }

   return 0;
}

int CHistoryReviewList::saveForMarkCurrentReviewRegion()
{
   markList.clear();

   // Check for anything that would prevent access to review list
   if (reviewListInvalid                             // Obsolete review list
       || reviewFrame == -1                          // reviewFrame invalid
       || reviewListIndex < 0                        // index not set or invalid
       || reviewListIndex >= getReviewRegionCount()) // index beyond list
      {
      // Cannot return anything
      return -1;
      }

   // Put save number from current review region into mark list
   markList.push_back(reviewList[reviewListIndex].saveNumber);

   return 0;
}


int CHistoryReviewList::markReviewRegions()
{
   // Mark as restored all of the history entries that have global save
   // numbers that match those in the markList

   int retVal;
   {
      CAutoHistoryOpener opener(*saveRestoreEngine, FLUSH_TYPE_RESTORE,
                                reviewFrame, -1, 0, 0, 0, retVal);
      // old way: retVal = saveRestoreEngine->openForRestore(reviewFrame, -1, 0, 0, 0);
      if (retVal != 0)
         return retVal;    // ERROR

      // Get all of the history records for the given frame
      int saveNumber, i;
      int markListCount = (int)markList.size();
      while (saveRestoreEngine->nextRestoreRecord() == 0)
         {
         saveNumber = saveRestoreEngine->getCurrentRestoreRecordGlobalSaveNumber();
         for (i = 0; i < markListCount; ++i)
            {
            if (markList[i] == saveNumber)
               {
               saveRestoreEngine->setCurrentRestoreRecordResFlag(true);
               break;
               }
            }
         }

      // now done in auto opener destructor (FLUSH_TYPE_RESTORE flag)
      // retVal = saveRestoreEngine->closeRestore();
   }
   if (retVal != 0)
      return retVal;    // ERROR

   return 0;
}

int CHistoryReviewList::reviseAllGOVRegions()
{
   int retVal;

   // use the list that was built by calls
   // to restoreCurrentRestoreRecordPixels
   retVal = saveRestoreEngine->reviseAllGOVRegions();
   if (retVal != 0)
      return retVal;

   return 0;
}

int CHistoryReviewList::reviseGOVRegions(int frameNumber)
{
   int retVal;

   // use the list that was built by calls
   // to restoreCurrentRestoreRecordPixels
   retVal = saveRestoreEngine->reviseGOVRegions(frameNumber);
   if (retVal != 0)
      return retVal;

   return 0;
}

int CHistoryReviewList::discardGOVRevisions()
{
   int retVal;

   // discard the list that was built by calls
   // to restoreCurrentRestoreRecordPixels
   retVal = saveRestoreEngine->discardGOVRevisions();
   if (retVal != 0)
      return retVal;

   return 0;
}

int CHistoryReviewList::buildReviewList(int frameIndex)
{
   // Clear existing contents of review list
   deleteReviewList();

   // Initialize current review frame and list index
   reviewFrame = -1;
   reviewListIndex = -1;
   int retVal;

   // Open Save/Restore database to access restore records
   // Setting field = 0 results in one field per frame
   // Setting rect = 0 gets fixes for entire frame
   {
      CAutoHistoryOpener opener(*saveRestoreEngine, FLUSH_TYPE_RESTORE,
                                frameIndex, -1, 0, 0, 0, retVal);

      // old way: retVal = saveRestoreEngine->openForRestore(frameIndex, 0, 0, 0, 0);
      if (retVal != 0)
         return retVal;  // ERROR: Could not open for restore

      // Get restore records one at a time from Save/Restore Database
      // Add rectangles for this frame to review list
      // I think list is ordered newest to oldest
      while (saveRestoreEngine->nextRestoreRecord() == 0)
         {
         // Filter out save records that have already been restored or are invalid
         if(!saveRestoreEngine->getCurrentRestoreRecordResFlag()
            &&!saveRestoreEngine->getCurrentRestoreRecordInvalidFlag())
            {
            SHistoryEntry historyEntry;
            historyEntry.saveNumber
                    = saveRestoreEngine->getCurrentRestoreRecordGlobalSaveNumber();
            historyEntry.pixelRegion
                    = saveRestoreEngine->getCurrentRestoreRecordPixelRegionList();

            reviewList.push_back(historyEntry);
            }
         }

      // now done in auto opener destructor
      // Close Save/Restore database access
      //retVal = saveRestoreEngine->closeRestore();
   }
   if (retVal != 0)
      return retVal;  // ERROR: couldn't close restore

   reviewListInvalid = false;
   reviewFrame = frameIndex;

   return 0;   // Return success
}

void CHistoryReviewList::deleteReviewList()
{
   for (unsigned int i = 0; i < reviewList.size(); ++i)
      {
      delete reviewList[i].pixelRegion;
      }

   reviewList.clear();
}

int CHistoryReviewList::goToNextReview(int frameIndex,
                                       const CVideoFrameList *videoFrameList)
{
   int retVal;
   bool listIsEmpty = (reviewList.size() == 0);
   bool newFrameIndex = (frameIndex != reviewFrame);
    bool endOfList = (reviewListIndex == 0);

   if (   listIsEmpty         // List is empty
       || newFrameIndex       // Frame index has changed
       || reviewListInvalid   // List is obsolete
       || endOfList)          // Moving beyond end
      {
      // Review List must be rebuilt due to one of several reasons

      // Remember if list has to be rebuilt because it was obsolete
      bool listWasInvalid = reviewListInvalid && !listIsEmpty && !newFrameIndex
                            && !endOfList;
      // Set frame on which to start searching for next frame with saves
      int searchFrame;
      if (endOfList && !reviewListInvalid && !listIsEmpty && !newFrameIndex)
         searchFrame = frameIndex;
      else
         searchFrame = frameIndex - 1;

      // Search for next frame that has saves
      int newReviewFrameIndex = saveRestoreEngine->getNextFrameWithFixes(searchFrame);
      if (newReviewFrameIndex == -1)
         {
         // Time to loop around to the beginning frame
         // Subtract 1 from first frame to force search to include frame 0
         int firstFrame = videoFrameList->getInFrameIndex() - 1;
         newReviewFrameIndex = saveRestoreEngine->getNextFrameWithFixes(firstFrame);
         }
      if (newReviewFrameIndex == -1)
         {
         // Nothing more to restore
         reviewFrame = -1;
         return 0;
         }

      int savedReviewListIndex;
      if (listWasInvalid)
         {
         // Don't really know what happened to change the underlying
         // save database, so save the current review index and reuse it
         savedReviewListIndex = reviewListIndex;
         }
      // Build review list by querying save database
      retVal = buildReviewList(newReviewFrameIndex);
      if (retVal != 0)
         {
         // ERROR: Could not build the review list
         return retVal;
         }

      if (listWasInvalid)
         {
         // Reuse saved review list index
         reviewListIndex = savedReviewListIndex;
         if (reviewListIndex >= (int)reviewList.size())
            reviewListIndex = reviewList.size() - 1;
         }
      else
         {
         // Start at newest save in list
         reviewListIndex = reviewList.size() - 1;
         }
      }

   else  // Current review list is okay
      {
      if (reviewListIndex == -1)
         {
         // Start at newest save in list
         reviewListIndex = reviewList.size() - 1;
         }
      else
         {
         // Advance index to next
         --reviewListIndex;
         }
      }

   return 0;   // Return success
}

int CHistoryReviewList::goToPreviousReview(int frameIndex,
                                        const CVideoFrameList *videoFrameList)
{
   int retVal;
   bool listIsEmpty = (reviewList.size() == 0);
   bool newFrameIndex = (frameIndex != reviewFrame);
   bool endOfList = (reviewListIndex + 1 >= (int)reviewList.size());

   if (   listIsEmpty         // List is empty
       || newFrameIndex       // Frame index has changed
       || reviewListInvalid   // List is obsolete
       || endOfList)          // Moving beyond end
      {
      // Review List must be rebuilt due to one of several reasons

      // Remember if list has to be rebuilt because it was obsolete
      bool listWasInvalid = reviewListInvalid && !listIsEmpty && !newFrameIndex
                            && !endOfList;
      // Set frame on which to start searching for next frame with saves
      int searchFrame;
      if (endOfList && !reviewListInvalid && !listIsEmpty && !newFrameIndex)
         searchFrame = frameIndex;
      else
         searchFrame = frameIndex + 1;

      // Search for previous frame that has saves
      int newReviewFrameIndex =
                      saveRestoreEngine->getPreviousFrameWithFixes(searchFrame);
      if (newReviewFrameIndex == -1)
         {
         // Time to loop around to the last frame
         int outFrame = videoFrameList->getOutFrameIndex();
         newReviewFrameIndex =
                     saveRestoreEngine->getPreviousFrameWithFixes(outFrame);
         }
      if (newReviewFrameIndex == -1)
         {
         // Nothing more to restore
         reviewFrame = -1;
         return 0;
         }

      int savedReviewListIndex;
      if (listWasInvalid)
         {
         // Don't really know what happened to change the underlying
         // save database, so save the current review index and reuse it
         savedReviewListIndex = reviewListIndex;
         }
      // Build review list by querying save database
      retVal = buildReviewList(newReviewFrameIndex);
      if (retVal != 0)
         {
         // ERROR: Could not build the review list
         return retVal;
         }

      if (listWasInvalid)
         {
         // Reuse saved review list index
         reviewListIndex = savedReviewListIndex;
         }
      else
         {
         // Start at oldest save in list
         reviewListIndex = 0;
         }
      }

   else  // Current review list is okay
      {
      if (reviewListIndex == -1)
         {
         // Start at oldest save in list
         reviewListIndex = 0;
         }
      else
         {
         // Advance index to previous
         ++reviewListIndex;
         }
      }

   return 0;   // Return success
}

int CHistoryReviewList::getAllReviewRegions(int frameIndex,
                                            CPixelRegionList &reviewRegion)
{
   int retVal;
   bool listIsEmpty = (reviewList.size() == 0);
   bool newFrameIndex = (frameIndex != reviewFrame);

   if (   listIsEmpty         // List is empty
       || newFrameIndex       // Frame index has changed
       || reviewListInvalid)  // List is obsolete
      {
      // Review List must be rebuilt due to one of several reasons
      retVal = buildReviewList(frameIndex);
      if (retVal != 0)
         return retVal;   // ERROR: could not build the review list
      }

   int reviewCount = (int)reviewList.size();
   if (reviewCount == 0)
      {
      // There is nothing in the review list
      return 0;
      }

   // Copy contents of review list to caller's CPixelRegionList
   // This appends all of the review regions together
   reviewRegion = *(reviewList[0].pixelRegion);
   try
      {
      for (int index = 1; index < reviewCount; ++index)
         {
         reviewRegion.Add(*(reviewList[index].pixelRegion));
         }
      }
   catch (std::bad_alloc)
      {
      TRACE_0(errout << "ERROR: OUT OF MEMORY in sysapi::getAllReviewRegions");
      }

   return 0;
}

int CHistoryReviewList::getReviewFrame()
{
   return reviewFrame;
}

void CHistoryReviewList::setReviewListInvalid()
{
   reviewListInvalid = true;
}


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void InitToolManager()
{
   // Tell Tool Manager about Navigator's function used to create
   // instances of CNavSystemInterface, which is the concrete subtype
   // of CToolSystemInterface abstract base class.  The function is called
   // by the Tool Manager when the Tool Manager creates a new Tool object
   // CToolManager::setCreateToolSystemInterfaceFunc must be called with
   // the Navigator's function pointer prior to creating any internal tools
   // or loading plugins
   CToolManager toolManager;
   toolManager.setCreateToolSystemInterfaceFunc
                               (CNavSystemInterface::createNavSystemInterface);

   // Set up the tool manager to access the internal tools
	toolManager.loadInternalTools(internalToolTable, INTERNAL_TOOL_COUNT);

	// Adding this "first dibs" tool first because I want it to get very first
	// crack at the inputs.
	int toolIndex = toolManager.findTool("FrameCompare");
	toolManager.initTool(toolIndex);             // has no GUI, so init now
	toolManager.addFirstDibsTool(toolIndex);

   // For now the only "first dibs" (on user input) tool id the GOVtool
	toolIndex = toolManager.findTool("GOV");
	toolManager.initTool(toolIndex);             // has no GUI, so init now
	toolManager.addFirstDibsTool(toolIndex);

   // Oh - hey! we now have another "first dibs" tool - TrackingTool
   toolIndex = toolManager.findTool("Tracking");
   toolManager.initTool(toolIndex);             // has no GUI, so init now
   toolManager.addFirstDibsTool(toolIndex);

   // And now one more... not sure if the order really matters
   toolIndex = toolManager.findTool("PluginMother");  // ==BaseTool
   // (already inited)
   toolManager.addFirstDibsTool(toolIndex);

   // The Navigator Tool is always the default tool
   toolIndex = toolManager.findTool("Navigator");
   // (already inited)
   toolManager.setDefaultTool(toolIndex);

   // Initialize the pseudo-tools that do not have user-interfaces and so
   // will never be initialized in the usual way
   toolIndex = toolManager.findTool("MediaReader");
   toolManager.initTool(toolIndex);
   toolIndex = toolManager.findTool("MediaWriter");
   toolManager.initTool(toolIndex);
   toolIndex = toolManager.findTool("Provisional");
   toolManager.initTool(toolIndex);
}

///////////////////////////////////////////////////////////////////////////////
