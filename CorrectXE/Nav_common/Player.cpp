//
// implementation file for class CPlayer
//
#include "Player.h"

#include "bthread.h"
#include "ClipAPI.h"
#include "Displayer.h"
#include "Extractor.h"
#include "GOVTool.h"       // gag me
#include "LineEngine.h"
#include "MainWindowUnit.h"
#include "MathFunctions.h"
#include "MediaFrameBuffer.h"
#include "MediaFrameBufferReadCache.h"
#include "MTIDialogs.h"
#include "MTImalloc.h"
#include "MTIstringstream.h"
#include "MTIsleep.h"
#include "PixelRegions.h"
#include "SaveRestore.h"
#include "timecode.h"
#include "ThreadLocker.h"
#include "ToolManager.h"

#include <mmsystem.h>
#include <iomanip>
#include <stddef.h>    // for _threadid macro
#include <stdio.h>
// ------------------------------------------------------------------------------

#define ULTRAVERBOSE_READ_TRACE 0
#if ULTRAVERBOSE_READ_TRACE
#define UVTRACELOG_MSG(a,t,l)                                    \
   {                                                           \
	  if (IsProgramRunning())                                  \
	  {                                                        \
		 string errorMsg;                                      \
		 {                                                     \
			CAutoSystemThreadLocker lock(MTIstringstream::GetLock());  \
			std::ostringstream errout;                         \
				errout <<_threadid << ";\t";						   \
			a;                                                 \
			errorMsg = errout.str();                           \
		 }                                                     \
		 SendCPMPMessage(errorMsg, t, l);                      \
	  }                                                        \
   }
#define UVTRACE(a) do { \
								if (CPMPTraceLogLevel() >= 3) \
								  UVTRACELOG_MSG(a, CPMPTraceLevel() >= 3, CPMPLogLevel() >= 3); \
						 } while (0)
#else
#define UVTRACE(a)
#endif

//#define NO_PRIORITY_BOOST 1

//#define VIDEO_RING_BUFFER_SIZE (6 * (size_t) 216743936)

// ------------------------------------------------------------------------------

// I hope these are unique error code!
#define ERR_ASYNC_WAIT_TIMED_OUT           -61100
#define ERR_INTERNAL_ERROR_MISSING_BUFFER  -61101
#define ERR_OPERATION_CANCELED             -61102


// A value used to compute the numer of buffer slots in the displaybuf ring.
#define DISPLAYBUFCNT 24





/****************************************************************************

 Multimedia timer

 *************************************************************************** */

/*
 VOID CALLBACK WaitOrTimerCallback(
 __in  PVOID lpParameter,       - argument passed through - pointer to Player
 __in  BOOLEAN TimerOrWaitFired - we ignore this because it's always 'timer'
 );
 */
void CALLBACK MultimediaTimerCallback(PVOID lpParameter, BOOLEAN TimerOrWaitFired)
{
	CPlayer *player = (CPlayer*) lpParameter;

	player->maybeInvokeMainWindowRefresh();
}

#include <chrono>
#include <thread>
void TimingLoopThread(void *vpAppData, void *vpReserved)
{
	CPlayer *player = (CPlayer*) vpAppData;
	BThreadSetPriority(vpReserved, THREAD_PRIORITY_TIME_CRITICAL);
   BThreadBegin(vpReserved);
	while (true)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(2));
		player->maybeInvokeMainWindowRefresh();
	}
}

/****************************************************************************

 Jog delay ramps

 *************************************************************************** */

struct DELAY
{
   int delay; // msec
   int nxt;
};

static DELAY ramp[12] =
{

#define NO_DELAY_RAMP 0
	{0, NO_DELAY_RAMP}, // used for "go to frame"

#define NORMAL_PLAY_RAMP (NO_DELAY_RAMP+1)
	{-1, NORMAL_PLAY_RAMP}, // -1 = use nominal frame rate

#define FAST_JOG_RAMP (NORMAL_PLAY_RAMP+1)
   {120, FAST_JOG_RAMP + 1}, // "Big" delay so easy to jog 1 frame
   {60, FAST_JOG_RAMP + 2}, {-1, FAST_JOG_RAMP + 2}, // -1 = use nominal frame rate

#define SLOW_JOG_RAMP (FAST_JOG_RAMP+3)
   {80, SLOW_JOG_RAMP + 1}, // SLOW 'JOG' RAMP (used for ?? -
   {75, SLOW_JOG_RAMP + 2}, // once was used for stuff like
   {70, SLOW_JOG_RAMP + 3}, // "jog 1/3 sec" and "slow jog", but
   {65, SLOW_JOG_RAMP + 4}, // I don't see any keys mapped to
   {60, SLOW_JOG_RAMP + 5}, // those operations in NavigatorTool)
   {55, SLOW_JOG_RAMP + 5},

#define FOLLOW_TRACKBAR_RAMP (SLOW_JOG_RAMP+6)
   {10, FOLLOW_TRACKBAR_RAMP}

};

CSpinLock CPlayer::_AsyncWaitParamsThreadIdLock;

// ***************************************************************************

// C O N S T R U C T O R
//
CPlayer::CPlayer(int numberOfReadThreads)
		: //vbpRing(nullptr),
		  //iNRingFrames(0),
		  //ucpVideoPool(nullptr),
        asyncReadThreadCount(std::min<int>(std::max<int>(numberOfReadThreads, MIN_DISK_READER_THREAD_COUNT), MAX_DISK_READER_THREAD_COUNT))
{
   // initialize for MEDIA access
   // /* int stat = */ CMediaInterface::initialize();

   // allocate an instance of 'HRTimer' to allow
   // accurate interframe delays
	interframeDelayTimer = new CHRTimer;

   // allocate an instance of 'HRTimer' to allow
   // tracking of reader idle time
   idleTimer = new CHRTimer;

   // no invisible field buffers are allocated
   srcInvisibleFieldCount = 0;

   // allocate storage for READER ring buffer - when the clip is
   // interlaced, the frame storage is split between two fields

   srcMaxFieldCount = 0;
   srcVisibleFieldCount = 0;

	// Allocate the displayBuff RINGSLOTS
   //
	idisplayBuffLead = 5; // hdwBuf->getBoardFrames();
	displayRing.setMaxSize(DISPLAYBUFCNT + idisplayBuffLead);
	idisplayLagPtr = idisplayEmptyPtr = idisplayFillPtr = 0;

   // no provisional buffer or frame
   prvPixelRegionListLock = new CThreadLock;
   clearProvisionalFrameIndex();
	provisionalFrameBuffer = nullptr;
   provisionalDisplayMode = DISPLAY_FROM_CLIP;
   prvPixelRegionList = nullptr;

   // no review buffer exists yet
	reviewBuf = nullptr;
   reviewBufSize = 0;

   // no clip is loaded
   srcClip = nullptr;
   srcVideoFrameList = nullptr;
   srcVideoProxyIndex = -1;
   srcVideoFramingIndex = -1;

	// the display mode is WINDOW ONLY
   displayMode = DISPLAY_TO_WINDOW;
	initialDisplayMode = DISPLAY_TO_WINDOW;

   // init some reasonable values for the
   // display speeds, then choose nominal
   displaySpeed[DISPLAY_SPEED_NOMINAL] = 30;
   displaySpeed[DISPLAY_SPEED_SLOW] = 10;
   displaySpeed[DISPLAY_SPEED_MEDIUM] = 20;
   displaySpeed[DISPLAY_SPEED_FAST] = 30;
   displaySpeed[DISPLAY_SPEED_UNLIMITED] = 30;
   displaySpeedIndex = DISPLAY_SPEED_NOMINAL;
   actualDisplaySpeed = 0.;
   observedFrameInterval = 10.0F;

   for (int i = 0; i < INTERVAL_RING_SIZE; ++i)
   {
      frameDeltaTime[i] = 0.0F;
   }

   frameDeltaTimeIndex = 0;

   for (int i = 0; i < INTERVAL_RING_SIZE; ++i)
   {
      frameReadTime[i] = 0.0F;
   }

   frameReadTimeIndex = 0;

   lastTimeFrameWasDisplayed = 0;
   activeDutyCycle = 0;
   nextFrameDisplayTime = 0;
   messagePostedSerialNumber = 0;
	messageReceivedSerialNumber = 0;
	refreshInvokedSerialNumber = 0;
   refreshDoneSerialNumber = 0;
   interFrameDelayIndex = NORMAL_PLAY_RAMP;
   inhibitMainWindowRefreshFlag = false;
   expediteMainWindowRefreshFlag = false;
   reallyPlayingFlag = false;

   // Default subsampling modes. Note that
	// with these settings, the Player knows
   // the difference between a "fast" frame
   // (one that is part of panning or free-
   // running sequence) and a "slow frame"
   // (one that the Player stops on). This
   // will be used by Paint II, which can
   // load its buffers on "slow" frames with-
   // out interfering with Navigation
   subsampledPlayback = true;
   subsampledPanning = true;

   // see "refreshFrame"
   lastDisplayedFrameIndex = -1;
	lastDisplayedFrameBuffer = nullptr;

   // init the review playback flag
   playbackFilter = PLAYBACK_FILTER_NONE;

   // we'll need this
   srcVideoImageFormat = nullptr;

   // allocate storage for timecodes
   srcMinTimecode = new CTimecode(0);
	lastTimecode = new CTimecode(0);
   srcMaxTimecode = new CTimecode(0);

   // allocate an extractor
   extractor = new CExtractor;

   // allocate a history class
   reviewHistory = new CSaveRestore;

   // line engine depends on format
   lineEngine = nullptr;

   // allocate storage for queued frames (slider mode)
   jogFrameQueue = new int[JOG_FRAME_QUEUE_SIZE];
   jogFrmQSize = JOG_FRAME_QUEUE_SIZE;

   // Init the cross-thread synchroniuzed call mechanism
   diskReaderCommand = NOOP_READER;
	kickDiskReader = SemAlloc();
   diskReaderCommandAck = SemAlloc();

   // disk reader state flag
   diskReaderIsThrottledBack = false;

	// start in idle state
   majorState = READER_IDLE;

   // allow call Navigator CMDs
   blockCmdsExcStop = false;

	// launch disk reader thread
	BThreadSpawn(CPlayer::DiskReaderCallback, this);

//	// Preallocate the entire ring buffer of video frames.
//	// We try to get room for 6 8K frames, but if that fails
//	// we retry with 1/2 then with 1/4 of that size!
//	videoRingBufferSize = VIDEO_RING_BUFFER_SIZE;
//	videoRingBufferAddr = MTImemalign(MTI_MEMALIGN_DEFAULT, videoRingBufferSize);
//	if (videoRingBufferAddr == 0)
//   {
//      videoRingBufferSize /= 2;
//		videoRingBufferAddr = MTImemalign(MTI_MEMALIGN_DEFAULT, videoRingBufferSize);
//      if (videoRingBufferAddr == 0)
//		{
//			videoRingBufferSize /= 2;
//			videoRingBufferAddr = MTImemalign(MTI_MEMALIGN_DEFAULT, videoRingBufferSize);
//		}
//	}

   /*
    BOOL WINAPI CreateTimerQueueTimer(
    __out     PHANDLE phNewTimer,
    __in_opt  HANDLE TimerQueue,
    __in      WAITORTIMERCALLBACK Callback,
    __in_opt  PVOID Parameter,
    __in      DWORD DueTime,
    __in      DWORD Period,
    __in      ULONG Flags
    );
    WT_EXECUTEDEFAULT By default, the callback function is queued to a non-I/O
    worker thread.

    WT_EXECUTEINTIMERTHREAD The callback function is invoked by the timer
    thread itself. This flag should be used only for
    short tasks or it could affect other timer operations.
    The callback function is queued as an APC.
    It should not perform alertable wait operations.

    WT_EXECUTEINIOTHREAD    The callback function is queued to an I/O worker thread.
    This flag should be used if the function should be
    executed in a thread that waits in an alertable state.
    The callback function is queued as an APC.
    Be sure to address reentrancy issues if the function
    performs an alertable wait operation.

    WT_EXECUTEINPERSISTENTTHREAD The callback function is queued to a thread that
    never terminates. It does not guarantee that the same
    thread is used each time. This flag should be used
    only for short tasks or it could affect other timer
    operations. Note that currently no worker thread is
    truly persistent, although no worker thread will
    terminate if there are any pending I/O requests.

    WT_EXECUTELONGFUNCTION  The callback function can perform a long wait. This
    flag helps the system to decide if it should create a
    new thread.

    WT_EXECUTEONLYONCE      The timer will be set to the signaled state only once.
    If this flag is set, the Period parameter must be zero.

    WT_TRANSFER_IMPERSONATION Callback functions will use the current access token,
    whether it is a process or impersonation token. If this
    flag is not specified, callback functions execute only
    with the process token. Windows XP/2000:  This flag is
    not supported until Windows XP with SP2 and Windows
    Server 2003.
    */

   timeBeginPeriod(1);

//	BOOL boolRet = CreateTimerQueueTimer(&multimediaTimerHandle, nullptr, MultimediaTimerCallback, (PVOID) this, 2, 2, WT_EXECUTEINTIMERTHREAD);
	BThreadSpawn(TimingLoopThread, this);
}
//------------------------------------------------------------------------------

// D E S T R U C T O R
//
CPlayer::~CPlayer()
{
   /*
    BOOL WINAPI DeleteTimerQueueTimer(
    __in_opt  HANDLE TimerQueue,      - nullptr for default queue
    __in      HANDLE Timer,           - the timer to delete
    __in_opt  HANDLE CompletionEvent  - INVALID_HANDLE_VALUE for 'wait'
    );
    */
   DeleteTimerQueueTimer(nullptr, multimediaTimerHandle, INVALID_HANDLE_VALUE);

   // was terminate() called first?
   MTIassert(majorState == READER_ENDED);

   // return storage for queued frames
   delete[]jogFrameQueue;

   // return storage for history
   delete reviewHistory;

   // return storage for extractor
   delete extractor;

   // return storage for timecodes
   delete srcMinTimecode;
	delete lastTimecode;
   delete srcMaxTimecode;

//	// delete existing 'getBuf'
//	getBuf = nullptr;;

   // delete existing provisional buffer
   // and any remaining pixel region list
	provisionalFrameBuffer = nullptr;
	delete prvPixelRegionList;
   delete prvPixelRegionListLock;

	// delete the display ring buffer
	for (int i = 0; i < displayRing.getMaxSize(); i++)
	{
		displayRing[i].frameBuffer = nullptr;
	}

	// delete the hi-res timers
   delete interframeDelayTimer;
   delete idleTimer;

	delete[]reviewBuf;

   // destroy the lineEngine
   CLineEngineFactory::destroyPixelEng(lineEngine);
}
//------------------------------------------------------------------------------

void CPlayer::terminate()
{
   inhibitMainWindowRefreshFlag = true;

   // kills the reader thread
   switchToOff();
}
//------------------------------------------------------------------------------

void CPlayer::DiskReaderCallback(void *vp, void *vr)
{
	CPlayer *p = static_cast<CPlayer*>(vp);

#ifndef NO_PRIORITY_BOOST
	BThreadSetPriority(vr, THREAD_PRIORITY_ABOVE_NORMAL);
#endif

   p->DiskReader(vr);
}
//------------------------------------------------------------------------------

// CALL ONLY FROM DISK READER
void CPlayer::setMajorState(ReaderMajorState newState)
{
   if (majorState != newState)
   {
      // NO frickin' idea what this is for!
      mainWindow->GetTimeline()->MTL->SetAutoRefresh(newState != READER_ACTIVE);

      // if we are transitioning out of READER_ACTIVE, we must make sure
      // there are no read ahead operations in progress!
      if (majorState == READER_ACTIVE)
      {
         cleanUpReadAheads();
      }

      majorState = newState;

      if (newState == READER_IDLE)
      {
         idleTimer->Start();
      }
   }
}
//------------------------------------------------------------------------------

// how long the reader has been idle, in milliseconds. If the reader isn't
// idle, returns -1
long CPlayer::getIdleTime()
{
   long retVal = -1;

   if (majorState == READER_IDLE)
   {
      const long max_long = 0x7fffffff;
      double idleTime = idleTimer->Read();

      if (idleTime > max_long)
      {
         retVal = max_long;
      }
      else
      {
         retVal = (long) idleTime;
      }
   }

   return retVal;
}
//------------------------------------------------------------------------------

// signals frame display loop to synch computer monitor to remote display
// ?? HOW DO THEY GET OUT OF SYNCH??
bool CPlayer::frameNeedsSynchronize(int frame)
{
   switch (readMode)
   {
   case PLAY_CLIP_FWD:
      if (frame == iendFrame)
      {
         return (true);
      }
      break;

   case PLAY_CLIP_BWD:
   case LOOP_CLIP_FWD: // ?? Why does beg frame need sync when looping? QQQ
      if (frame == ibegFrame)
      {
         return (true);
      }
      break;

   case FOLLOW_TRACKBAR:
      // ?? WHY IS THIS '- 2' NOT '- 1'     // QQQ
      if (jogFrmQEmptyPtr >= (jogFrmQFillPtr - 2))
      {
         return (true);
      }
      break;

   case JOG_ONCE_FWD:
   case JOG_ONCE_BWD:
      if (jogFrmQEmptyPtr >= (jogFrmQFillPtr - 2))
      {
         return (true);
      }
      break;

   case JOG_ADDITIONAL_FWD:
   case JOG_ADDITIONAL_BWD: // ?? How does this make any frickin sense? QQQ
      if (frame == iendFrame)
      {
         return (true);
      }
      break;
   }

   return (false);
}
//------------------------------------------------------------------------------

void CPlayer::DiskReader(void *vr)
{
   BThreadBegin(vr);

   while (true)
	{
		bool gotKicked = false;

		if (majorState != READER_ACTIVE)
      {

         // Nothing going on - wait for a command from another thread
         SemSuspend(kickDiskReader);
         gotKicked = true;
      }
      else
		{
         // Just poll the semaphore, don't hang - If we're throttling back,
         // wait a bit before resuming
         bool waitABit = diskReaderIsThrottledBack || (readMode == FOLLOW_TRACKBAR || readMode == JOG_ONCE_FWD || readMode == JOG_ONCE_BWD);
         int timeout = (waitABit ? 1 : 0); // msec
         gotKicked = SemSuspendWithTimeout(kickDiskReader, timeout);
      }

      if (gotKicked)
      {
         // Grab the op code and process it
         int newOpCode = diskReaderCommand;
         diskReaderCommand = NOOP_READER;

         // We get killed at app close
         if (newOpCode == KILL_READER)
         {
            break;
         } // from while (true) loop

         // Caller can lock the reader to make sure it doesn't start up while
         // doing something like changing clips
         if (majorState == READER_LOCKED)
			{
            // When in "locked" mode, the only command we accept is "unlock"
            // (hopefully from the thread the did the initial locking and not
            // a rogue thread that wants to hijack the player!!
				if (newOpCode == UNLOCK_READER)
            {
               setMajorState(READER_IDLE);
            }

            // else do nothing, ignore the command

			}
         else
         {
				// Handle the rest of the commands
            switch (newOpCode)
            {
            case IDLE_READER:
               setMajorState(READER_IDLE);
               break;

            case LOCK_READER:
               setMajorState(READER_LOCKED);
               break;

            case ACTIVATE_READER:
               setMajorState(READER_ACTIVE);
               break;
            }
         }

         // Release the caller (after we've updated the state)
         SemResume(diskReaderCommandAck);
      }

      // We keep going at this point only if the reader is now active -
      // otherwise we just loop looking for commands

      if (majorState != READER_ACTIVE)
      {
         // go back to the top of the "while (true)" loop
         continue;
      }

      //////////////////////////////////////////
      //
      // READER IS ACTIVE
      //

      int currentFrameReadError = 0;

      // Throttling 'low water' mark -> 1/4 of total buffers free,
      // but not more than 4
      int totalBufferCount = displayRing.size() - idisplayBuffLead;
		int bufferFullCount = idisplayFillPtr - idisplayLagPtr;

		// I think the hysteresis is impeding performance, so eliminating it.
//		int lowWaterMark = (totalBufferCount * 3) / 4;
//		if (lowWaterMark < (totalBufferCount - 4))
//		{
//			lowWaterMark = totalBufferCount - 4;
//		}
		int lowWaterMark = totalBufferCount - 1;

      // Check if we need to stall because the ring buffer has wrapped around
      if (bufferFullCount >= totalBufferCount)
      {
         // Getting too far ahead - let's slow this puppy down
         if (!diskReaderIsThrottledBack)
         {
            diskReaderIsThrottledBack = true;
//				UVTRACE(errout << "*********** THROTTLE BACK ----------------");
			}

         continue;
      }

      // Check if we need to resume reading after throttling back
      // (when the number of buffers hits the 'low water' mark)
      if (diskReaderIsThrottledBack)
      {
         // Stop 'throttling back' if we hit the 'low water mark'
         // (1/4 of buffers available, but not more than 4)

         if (bufferFullCount > lowWaterMark)
         {
            // Not there yet, keep twiddling thumbs
            continue;
         }

         // We're at low water mark, so start actually reading ahead again
         diskReaderIsThrottledBack = false;
//         UVTRACE(errout << "        *** At low water mark *** " << "(" << bufferFullCount << " <= " << lowWaterMark << ")");
//         UVTRACE(errout << "*********** THROTTLE FORWARD +++++++++++++");
		}

      CHRTimer getBufferTimer;
      getBufferTimer.Start();
      double frameReadTime = 0.0;

      // For REMOTE DISPLAY --
      // wait for this buffer to move off the board
      // if (monitorDisplayIsEnabled())
      // {
      // hdwBuf->WaitForOutput(idisplayFillPtr%displayRing.size());
      // }

      // this variable allows us to bump the frames queue
      // EMPTY ptr only after the frame has been read

      jogFrmQAdv = 0;

      switch (readMode)
      {
		case FOLLOW_TRACKBAR:

			if (jogFrmQEmptyPtr < jogFrmQFillPtr)
         {

            // skip one-third of the queued frames
            // jogFrmQEmptyPtr = jogFrmQEmptyPtr +
            // ((jogFrmQFillPtr - jogFrmQEmptyPtr) * 3) / 4;
            jogFrmQEmptyPtr = jogFrmQFillPtr - 1;

            // also skip the next-to-the-last frame for quicker stops
            // if (jogFrmQEmptyPtr >= (jogFrmQFillPtr - 3))
            // jogFrmQEmptyPtr = jogFrmQFillPtr - 1;

            // pick up the next frame to read from the clip
            icurFrame = jogFrameQueue[(jogFrmQEmptyPtr) % jogFrmQSize];
            MTIassert(icurFrame >= 0);

            // advance the ptr AFTER reading the frame
            jogFrmQAdv = 1;

         }
         else
         {

            // There is nothing to show, so bail out
            continue; // jump to top of 'while(true)' loop
         }

         break;

      case JOG_ONCE_FWD:
      case JOG_ONCE_BWD:

         if (jogFrmQEmptyPtr < jogFrmQFillPtr)
         {

            // skip one-half of the queued frames
            jogFrmQEmptyPtr = jogFrmQEmptyPtr + (jogFrmQFillPtr - jogFrmQEmptyPtr) / 2;

            // also skip the next-to-the-last frame for quicker stops
            if (jogFrmQEmptyPtr >= (jogFrmQFillPtr - 3))
            {
               jogFrmQEmptyPtr = jogFrmQFillPtr - 1;
            }

            // pick up the next frame to read from the clip
            icurFrame = jogFrameQueue[(jogFrmQEmptyPtr) % jogFrmQSize];
            MTIassert(icurFrame >= 0);

            // advance the ptr AFTER reading the frame
            jogFrmQAdv = 1;

         }
         else
         {

            // There is nothing to show, so bail out
            continue; // jump to top of 'while(true)' loop
         }

         break;
      }

      // Detect ring buffer overruns
      MTIassert(((idisplayFillPtr % displayRing.size()) != (idisplayLagPtr % displayRing.size())) || (idisplayFillPtr == idisplayLagPtr));

      // debugging crap that I think is no longer needed
      MTIassert(icurFrame >= 0);
      if (icurFrame < 0)
      {
         static bool oneshot = false;
         if (!oneshot)
         {
            oneshot = true;
            _MTIErrorDialog("ERROR! icurFrame = -1 [READER_ACTIVE]");
         }
         // what now?
      }

      // mbraca: Don't know why, but sometimes the buffer is missing
//		MTIassert(displayRing[idisplayFillPtr].frameBuffer != nullptr);
//		if (displayRing[idisplayFillPtr].frameBuffer == nullptr)
//      {
//         static bool oneshot = false;
//         if (!oneshot)
//         {
//            oneshot = true;
//            _MTIErrorDialog("ERROR! displayRing[idisplayFillPtr].buf == nullptr [READER_ACTIVE]");
//         }
//			currentFrameReadError = ERR_INTERNAL_ERROR_MISSING_BUFFER;
//		}
//		else
		{

			bool gotCurrentFrame = false;

			// Initialize readahead parameters
			int wrapAtFrame = -1;
			int wrapToFrame = -1;
			RA_DIRECTION raDirection;

			switch (readMode)
			{

			default:
				raDirection = RA_FORWARD;
				break;

			case LOOP_CLIP_FWD:
				raDirection = RA_FORWARD;
				wrapAtFrame = iendFrame + 1; // iendFrame is INCLUSIVE
				wrapToFrame = ibegFrame;
				break;

			case PLAY_CLIP_BWD:
         case JOG_ONCE_BWD:
         case JOG_ADDITIONAL_BWD:
            raDirection = RA_BACKWARD;
            break;
         }

         // It is critical to the operation of this thread that we not
         // escape from this loop while I/O is pending for the "current"
         // frame!
         while (!gotCurrentFrame)
         {
            int raCount = 0;

            // Two-phase throttling mechanism to maximize I/O overlap
				bufferFullCount = idisplayFillPtr - idisplayLagPtr;

				if (diskReaderIsThrottledBack)
            {

               // Stop 'throttling back' if we hit the 'low water mark'
               // (1/4 of buffers available, but not more than 4)
               if (bufferFullCount <= lowWaterMark)
               {
                  // At low water mark, so start actually reading ahead again
                  diskReaderIsThrottledBack = false;
//						UVTRACE(errout << "        *** At low water mark *** " << "(" << bufferFullCount << " <= " << lowWaterMark << ")");
//						UVTRACE(errout << "*********** THROTTLE FORWARD +++++++++++++");
               }
               else
               {
						////UVTRACE(errout << "        *** Not at low water mark yet *** "
                  ////<< "(" << bufferFullCount << " > "
                  ////<< lowWaterMark << ")");

                  // It's safe to bail out of the loop because there are no
                  // I/O's in progress as long as we are throttled back
                  break; // need two hops to get back to top of 'while(true)'
               }
            }
            else if (bufferFullCount >= totalBufferCount)
            {
               // All the buffers are filled with real stuff -
               // so stop reading ahead for a bit
               if (!diskReaderIsThrottledBack)
               {

						diskReaderIsThrottledBack = true;
//						UVTRACE(errout << "*********** THROTTLE BACK ----------------");
               }

               // It's safe to bail out of the loop because no I/O can
               // be in progress if all the buffers are full
               break; // need two hops to get back to top of 'while(true)'
            }

            // Comput number of readaheads we want -
            // the check for frameInterval > 0 is a HACK to see if we are
            // really PLAYING (frameInterval > 0) or if we are simply going to
            // a frame (frameInterval will be 0 in that case)...
            // ALSO NOTE: We don't do readahead in the case of deltaFrame != 1
            // (fast forwardfjOG) because it makes things too messy and isn't
            // really worth it
            if ((frameInterval > 0) && (deltaFrame == 1) && (readMode == PLAY_CLIP_BWD || readMode == PLAY_CLIP_FWD ||
                  readMode == LOOP_CLIP_FWD))
            {
               // The number of readaheads we request is equal to the number
               // of buffers available to be read into
               // "-1" is because the current frame needs a buffer
               raCount = totalBufferCount - bufferFullCount - 1;

               // UNLESS we've hit the end of the frame sequence being played
               // CAREFUL - the "current" frame also counts as a "readahead"!
               // CAREFUL - iendFrame is INCLUSIVE!
               if (readMode == PLAY_CLIP_FWD && (icurFrame + raCount) > iendFrame)
               {

                  raCount = iendFrame - icurFrame;
               }

               if (readMode == PLAY_CLIP_BWD && (icurFrame - raCount) < ibegFrame)
               {

                  raCount = icurFrame - ibegFrame;
               }
            }

            // debugging
            static int oldRaCount = -1;
            static int oldFill = -1;
            static int oldEmpty = -1;
            static int oldLag = -1;

				if (oldRaCount != raCount || oldFill != idisplayFillPtr || oldEmpty != idisplayEmptyPtr || oldLag != idisplayLagPtr)
            {
               oldRaCount = raCount;
               oldFill = idisplayFillPtr;
               oldEmpty = idisplayEmptyPtr;
               oldLag = idisplayLagPtr;
					TRACE_3(errout << "READAHEAD: " << raCount << ", FILL=" << idisplayFillPtr << ", EMPTY=" << idisplayEmptyPtr << ", LAG=" <<
                     idisplayLagPtr << ", CNT=" << displayRing.size() << ", FULLCOUNT=" << bufferFullCount << ", TOTALCOUNT=" <<
                     totalBufferCount);
               ////<< ", DIR="
               ////<< ((raDirection == RA_BACKWARD)? "BACKWARD"
					////: "FORWARD"));
            }

				// The "current" frame always goes to the "fill" buffer
            int icurFrameBuffIndex = idisplayFillPtr % displayRing.size();

            // Queue up frame read requests to be processed in the background
            queueFrameReads(1 + raCount, icurFrame, icurFrameBuffIndex, raDirection, wrapAtFrame, wrapToFrame);

				// Try to complete the read of the current frame - if we
            // are doing asynchronous I/O we don't hang because we want
            // to keep the readaheads flowing (hence the timeout check)
            currentFrameReadError = readCurrentFrame(icurFrame, icurFrameBuffIndex, &frameReadTime);

            if (currentFrameReadError != ERR_ASYNC_WAIT_TIMED_OUT)
            {
               gotCurrentFrame = true;
					displayRing[icurFrameBuffIndex].iframeIndex = icurFrame;
					displayRing[icurFrameBuffIndex].syncUp = frameNeedsSynchronize(icurFrame);
            }
         } // while (!gotCurrentFrame)
		}

      if (diskReaderIsThrottledBack)
		{
			// second hop to get back to top of 'while(true)' loop
			continue;
		}

      if (currentFrameReadError != 0)
      {

			// READ OF CURRENT FRAME FAILED
         // if there was a media read error, the user's buffer
         // has received a frame with the error code as text

			// include this buffer in the FIFO, but not if we got the
         // weird "missing buffer" problem
         if (currentFrameReadError != ERR_INTERNAL_ERROR_MISSING_BUFFER)
         {
            idisplayFillPtr++;
//				UVTRACE(errout << "############### ERROR BUMP FILL -> " << idisplayFillPtr);
         }

         // and bring the reader to a stop
         setMajorState(READER_IDLE);

         continue; // back to the top of the 'while(true)' loop
		}

      ////////////////////////////////////
      //
      // got a good frame
      //

      // Overlay original values if we're in that mode

//    was:  overlayHistoryOnDisplayFrame(displayRing[idisplayFillPtr].offsetBuf, icurFrame);
		if ((playbackFilter & PLAYBACK_FILTER_ORIGINAL_VALUES) != 0)
		{
			CHRTimer debugTimer1;

			auto buggeredFrameBuffer = displayRing[idisplayFillPtr].frameBuffer->clone();
			if (buggeredFrameBuffer)
			{
				overlayHistoryOnDisplayFrame(buggeredFrameBuffer, icurFrame);
				displayRing[idisplayFillPtr].frameBuffer = buggeredFrameBuffer;
			}

			UVTRACE(errout << "overlayHistoryOnDisplayFrame took " << debugTimer1.Read() << " msec");
		}

      // Remember how long it took to get the frame
      UVTRACE(errout << "HHHHHHHHH Frame Read Times = " << frameReadTime << ", " << getBufferTimer.Read());
      recordFrameReadTime(frameReadTime > 0.0 ? frameReadTime : getBufferTimer.Read());

      // Signal that the current frame is now available for display
      // (expediting firing of the refresh timer callback)
		idisplayFillPtr++;
//		UVTRACE(errout << "############### GOOD BUMP FILL -> " << idisplayFillPtr);
		MTIassert(idisplayEmptyPtr <= idisplayFillPtr);

      // note how the value of "jogFrmQAdv" was defined
		// above. This keeps us from emptying the
      // queue too rapidly & makes multiple keying
      // work better by filling the queue
      jogFrmQEmptyPtr += jogFrmQAdv;

      // generate next frame index
      switch (readMode)
      {

      case PLAY_CLIP_FWD:

         if (icurFrame < iendFrame)
         {
            if ((icurFrame += deltaFrame) > iendFrame)
            {
               icurFrame = iendFrame;
            }
         }
         else
			{
            setMajorState(READER_IDLE);
				blockCmdsExcStop = false;
         }
         break;

      case PLAY_CLIP_BWD:

         if (icurFrame > ibegFrame)
         {
            if ((icurFrame -= deltaFrame) < ibegFrame)
            {
               icurFrame = ibegFrame;
            }
         }
         else
			{
            setMajorState(READER_IDLE);
            blockCmdsExcStop = false;
			}
         break;

      case LOOP_CLIP_FWD:

         if ((icurFrame += deltaFrame) > iendFrame)
         {
            icurFrame = ibegFrame;
         }
         break;

      case FOLLOW_TRACKBAR:

         // NO! we time this out with a timer now
         // if (jogFrmQEmptyPtr == jogFrmQFillPtr)
         // setMajorState(READER_IDLE);

         break;

      case JOG_ONCE_FWD:

         if (jogFrmQEmptyPtr == jogFrmQFillPtr)
         {
            if (icurFrame < iendFrame)
            {
               readMode = JOG_ADDITIONAL_FWD;
            }
            else
            {
               setMajorState(READER_IDLE);
            }
         }

         break;

      case JOG_ADDITIONAL_FWD:

         if (displayRing[(idisplayFillPtr - 1)].syncUp)
         {
            jogFrmQEmptyPtr = jogFrmQFillPtr;
            setMajorState(READER_IDLE);
         }
         else if (icurFrame < iendFrame)
         {
            if ((icurFrame += deltaFrame) > iendFrame)
            {
               icurFrame = iendFrame;
            }
         }

         break;

      case JOG_ONCE_BWD:

         if (jogFrmQEmptyPtr == jogFrmQFillPtr)
         {
            if (icurFrame > iendFrame)
            {
               readMode = JOG_ADDITIONAL_BWD;
            }
            else
            {
               setMajorState(READER_IDLE);
            }
         }

         break;

      case JOG_ADDITIONAL_BWD:

         if (displayRing[(idisplayFillPtr - 1)].syncUp)
         {
            jogFrmQEmptyPtr = jogFrmQFillPtr;
            setMajorState(READER_IDLE);
         }
         else if (icurFrame > iendFrame)
         {
            if ((icurFrame -= deltaFrame) < iendFrame)
            {
               icurFrame = iendFrame;
            }
         }

         break;
      }
   }

   // Release the caller (after we've updated the state)
   setMajorState(READER_ENDED);
   SemResume(diskReaderCommandAck);
}
//------------------------------------------------------------------------------

int CPlayer::queueFrameReads(int frameCount, int currentFrameNumber, int currentFrameBuffIndex, RA_DIRECTION readDirection,
      int wrapAtFrameNumber, int wrapToFrameNumber)
{
   MTIassert(frameCount <= ASYNCH_READ_TRACKER_COUNT);
   MTIassert(currentFrameBuffIndex != -1 || currentFrameNumber == -1);
   MTIassert(currentFrameNumber != -1 || frameCount == 0);
   int retVal = 0;

   // If deltaFrame = 0, then we are just going to a frame, so no frame queuing
   if (deltaFrame < 1)
   {
      frameCount = 0;
   }

   ////////////////////////////////////////////////////////////////////
   // Reading ahead with deltaFrame > 1 does not work. Probably not
   // worth fixing....
   if (deltaFrame > 1)
   {
      frameCount = 0;
   }
   ////////////////////////////////////////////////////////////////////

   // If frame count is 0, we aren't doing readahead, so we want to make
   // sure there's nothing going on...
   if (frameCount == 0)
   {
      cleanUpReadAheads();
      return 0;
   }

   // Delta frame is always > 0, so adjust now for directionality
   int signedDeltaFrame = ((readDirection == RA_FORWARD) ? deltaFrame : -deltaFrame);

   // Can't queue up more frames than we've got read-ahead trackers
   if (frameCount > ASYNCH_READ_TRACKER_COUNT)
   {
      frameCount = ASYNCH_READ_TRACKER_COUNT;
   }

   // Assign trackers to current frame and read-ahead frames (obviously don't
   // do anything if there already is a tracker for the frame)
   int needToQueueFrameNumber[ASYNCH_READ_TRACKER_COUNT];
   int needToQueueBuffIndex[ASYNCH_READ_TRACKER_COUNT];
   int needToQueueCount = 0;

   // Note that if there are any outstanding frame read requests that we
   // are no longer interested in, we need to try to cancel them and to
   // wait for them to complete before continuing. THIS IS TO AVOID
   // HAVING TWO THREADS READING INTO THE SAME BUFFER SIMULTANEOUSLY
   bool needToWaitForCompletion[ASYNCH_READ_TRACKER_COUNT];
   for (int artIndex = 0; artIndex < ASYNCH_READ_TRACKER_COUNT; ++artIndex)
   {
      needToWaitForCompletion[artIndex] = false;
	}

   // Scan the frames to be read and see(a) which need to have async
   // reads started, and (b) which have someone else trying to read
   // into the target buffer, so we need to cancel that other operation
   for (int qIndex = 0; qIndex < frameCount; ++qIndex)
   {
		int qBuffIndex = (currentFrameBuffIndex + qIndex) % displayRing.size();
      int qFrame = currentFrameNumber + (qIndex * signedDeltaFrame);

		// Handle looping (forward single frame mode only!)
		// Apparently, looping backwards is not supported!
		if (signedDeltaFrame == 1 && wrapAtFrameNumber >= 0 && qFrame >= wrapAtFrameNumber)
		{
			int wrapRange = wrapAtFrameNumber - wrapToFrameNumber;
			while (qFrame >= wrapAtFrameNumber)
			{
				qFrame -= wrapRange;
			}
		}

      bool foundIt = false;
      for (int artIndex = 0; artIndex < ASYNCH_READ_TRACKER_COUNT; ++artIndex)
      {
			ASYNCH_READ_TRACKER &tracker = asynchReadTracker[artIndex];

			if (tracker.frameNumber == qFrame && tracker.displayBufIndex == qBuffIndex)
         {
            foundIt = true;
            break;
         }
      }

      if (!foundIt)
      {
         needToQueueFrameNumber[needToQueueCount] = qFrame;
         needToQueueBuffIndex[needToQueueCount++] = qBuffIndex;

         for (int artIndex = 0; artIndex < ASYNCH_READ_TRACKER_COUNT; ++artIndex)
         {
            ASYNCH_READ_TRACKER &tracker = asynchReadTracker[artIndex];

            if (tracker.displayBufIndex == qBuffIndex)
            {
               UVTRACE(errout << "############### ABORT " << tracker.frameNumber);
               asynchReadTracker[artIndex].cancel = true;
               needToWaitForCompletion[artIndex] = true;
            }
         }
      }
   }

   // To properly dispose of unwanted trackers we need to wait until they've
   // been completed
   for (int artIndex = 0; artIndex < ASYNCH_READ_TRACKER_COUNT; ++artIndex)
   {
      if (needToWaitForCompletion[artIndex])
      {
         // This frame is not one we're interested in anymore -
         // to release the tracker, we need to wait for it to complete,
         // and for sanity, we do it synchronously here!
         int asyncReadStatus;
         do
         {
				asyncReadStatus = finishAnAsynchronousFrameRead(artIndex);
         }
         while (asyncReadStatus == ERR_ASYNC_WAIT_TIMED_OUT);

         // Make the tracker available for re-use
         ASYNCH_READ_TRACKER &tracker = asynchReadTracker[artIndex];
         UVTRACE(errout << "############### BAD TRACKER RESET " << tracker.frameNumber);
         tracker.reset();
      }
   }

   {
      ////CAutoThreadLocker lock(diskReadThreadLock);

      // Queue up frame reads if needed
      for (int qIndex = 0; qIndex < needToQueueCount; ++qIndex)
      {
         int qFrame = needToQueueFrameNumber[qIndex];
         int qBuffIndex = needToQueueBuffIndex[qIndex];

         // Find an unused tracker
         int artIndex;
         for (artIndex = 0; artIndex < ASYNCH_READ_TRACKER_COUNT; ++artIndex)
         {
				if (!asynchReadTracker[artIndex].isInUse())
            {
               break;
            }
         }

         MTIassert(artIndex < ASYNCH_READ_TRACKER_COUNT);
         if (artIndex < ASYNCH_READ_TRACKER_COUNT)
         {
				// Add this frame to the read ahead work queue
				queueUpAnAsynchronousFrameRead(artIndex, qFrame, qBuffIndex);
			}
         else
         {
            // No trackers left - bail out
            break;
         }
      }
   }

   // That's it!
   return retVal;
}
//------------------------------------------------------------------------------

void CPlayer::cleanUpReadAheads()
{
   int needToWaitForCompletion[ASYNCH_READ_TRACKER_COUNT];
   int howManyTrackersNeedWaiting = 0;

   // We do this in two phases so we can cancel them all first, then wait
   for (int artIndex = 0; artIndex < ASYNCH_READ_TRACKER_COUNT; ++artIndex)
   {
      ASYNCH_READ_TRACKER *tracker = &asynchReadTracker[artIndex];

		if (tracker->isInUse())
      {
//         UVTRACE(errout << "############### CLEAN UP " << tracker->frameNumber);
         tracker->cancel = true;
         needToWaitForCompletion[howManyTrackersNeedWaiting++] = artIndex;
      }
   }

   for (int waitIndex = 0; waitIndex < howManyTrackersNeedWaiting; ++waitIndex)
   {
      // To clean up the tracker, we need to wait for it to complete
      // This code stolen from queueFrameRead() and should be refactored
      int asyncReadStatus;
      int artIndex = needToWaitForCompletion[waitIndex];

      do
      {
         asyncReadStatus = finishAnAsynchronousFrameRead(artIndex);
      }
      while (asyncReadStatus == ERR_ASYNC_WAIT_TIMED_OUT);

      // Make the tracker available for re-use
      ASYNCH_READ_TRACKER &tracker = asynchReadTracker[artIndex];
//      UVTRACE(errout << "############### CLEAN TRACKER RESET " << tracker.frameNumber);
      tracker.reset();
   }

   // Done!
}
//------------------------------------------------------------------------------

int CPlayer::readCurrentFrame(int currentFrameNumber, int currentFrameBuffIndex, double *frameReadTime)
{
   MTIassert(currentFrameNumber >= 0);
	MTIassert(currentFrameBuffIndex >= 0 && currentFrameBuffIndex < displayRing.size());

   int retVal = 0;
   int currentFrameTrackerIndex = -1;
	DisplayRingSlot *currentRingSlot = &displayRing[currentFrameBuffIndex];

   // Find the tracker for the current frame
   for (int artIndex = 0; artIndex < ASYNCH_READ_TRACKER_COUNT; ++artIndex)
   {
      ASYNCH_READ_TRACKER &tracker = asynchReadTracker[artIndex];

		if (tracker.isInUse() && (currentFrameNumber == tracker.frameNumber) && (currentFrameBuffIndex == tracker.displayBufIndex))
      {
         // currentFrameTrackerIndex will be -1 if we never started a
         // read for the current frame, or will be >= 0 if the thread
         // corresponding to the index is presently working on reading it.
         currentFrameTrackerIndex = artIndex;
         break;
      }
   }

   // The read of the current frame is synchronous with this call;
   // if we didn't read the frame ahead, read it synchronously otherwise
   // wait for the worker thread to finish
   // UPDATE: Well not quite: if the read is asynchronous, we may abort
   // the wait for the worker thread to complete (via a 1-msec timeout)
   // so we can keep the readaheads flowing; we'll complete the read on
   // a future pass through here

   if (currentFrameTrackerIndex == -1)
   {
		CHRTimer readTimer;

		MediaFrameBufferSharedPtr localFrameBuffer;
		retVal = srcVideoFrameList->readMediaFrameBuffer(currentFrameNumber, localFrameBuffer);
		// UGGH QQQ can get null buffer here!
		//MTIassert(localFrameBuffer);
		if (!localFrameBuffer)
		{
			// QQQ make a utility in MediaFileIO for this!
			UVTRACE(errout << "____ERROR__________  GOT A nullptr FRAME BUFFER FOR" << currentFrameNumber
                        << " (" << retVal << ") _____ERROR_______ ");
			MediaFrameBuffer::ErrorInfo errorInfo { retVal, "", "Invalid media frame - clip may be corrupt", "" };
			localFrameBuffer = MediaFrameBuffer::CreateErrorBuffer(srcVideoImageFormat
                                                                  ? *srcVideoImageFormat
                                                                  : CImageFormat(),
                                                               errorInfo);
		}
		else if (localFrameBuffer->getErrorInfo().code != 0)
		{
			UVTRACE(errout << "____ERROR__________  GOT ERROR READING CURRENT FRAME " << currentFrameNumber
                        << " (" << localFrameBuffer->getErrorInfo().code << ") _____ERROR_______ ");
		}

		currentRingSlot->frameBuffer = localFrameBuffer;

		double elapsed = readTimer.Read();
		UVTRACE(errout << "*-*-*-*-*-SYNC READ FOR " << currentFrameNumber << " took " << elapsed);

		if (frameReadTime != nullptr)
		{
			*frameReadTime = elapsed;
		}
	}
   else
   {
      // We used to hang forever here, but now we let it time out
      // so we can keep servicing the read ahead threads (loop is
      // at higher level, retval will be ERR_ASYNC_WAIT_TIMED_OUT)

      CHRTimer debugTimer;
      retVal = finishAnAsynchronousFrameRead(currentFrameTrackerIndex);

      ASYNCH_READ_TRACKER &tracker = asynchReadTracker[currentFrameTrackerIndex];
      if (frameReadTime != nullptr)
      {
         *frameReadTime = tracker.readTimer.Read();
      }

      if (retVal != ERR_ASYNC_WAIT_TIMED_OUT)
      {
         UVTRACE(errout << "*-*-*-*-*-ASYNC WAIT FOR " << currentFrameNumber << ((retVal == 0) ? " SUCCEEDED" :
               " FAILED") << " after " << debugTimer.Read());

         // Make the tracker available for re-use
         UVTRACE(errout << "############### GOOD TRACKER RESET " << tracker.frameNumber);
         tracker.reset();
      }
   }

   // That's it!
   return retVal;
}
//------------------------------------------------------------------------------

// this used to put the command in a queue, but that made no sense because it
// always waited with spin loops for the command to be picked up, so I changed
// it to use synchronization stuff so it's more obvious what's going on.

void CPlayer::sendCommandToDiskReader(ReaderCommandOpCode cmd)
{
   if (majorState == READER_ENDED)
   {
      return;
   }

   CAutoThreadLocker lock(diskReaderCommandLock);
   diskReaderCommand = cmd;
   SemResume(kickDiskReader);
   SemSuspend(diskReaderCommandAck);
}
//------------------------------------------------------------------------------

// disables the disk reader thread
void CPlayer::switchToOff()
{
   fullStop();
   // this doesn't return until the thread is dead
   sendCommandToDiskReader(KILL_READER);
}
//------------------------------------------------------------------------------

void CPlayer::switchToLocked()
{
	MTIassert(majorState != READER_LOCKED);
	sendCommandToDiskReader(LOCK_READER);
}
//------------------------------------------------------------------------------

void CPlayer::switchToUnlocked()
{
   MTIassert(majorState == READER_LOCKED);
   sendCommandToDiskReader(UNLOCK_READER);
}
//------------------------------------------------------------------------------

// puts disk reader thread into IDLE state
void CPlayer::switchToIdle()
{
   // reset the frame interval timer - we use it to pace the refresh timing
   interframeDelayTimer->Start();

   if (majorState != READER_IDLE)
   {
      sendCommandToDiskReader(IDLE_READER);
   }
}
//------------------------------------------------------------------------------

void CPlayer::lockReader(bool lock)
{
   if (lock)
   {
      fullStop();
		switchToLocked();
		blockCmdsExcStop = true;
	}
   else
   {
      blockCmdsExcStop = false;
		switchToUnlocked();
	}
}
//------------------------------------------------------------------------------

// puts disk reader thread into ACTIVE state
void CPlayer::switchToActive()
{
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   // Reset the frame delta time FIFO ring - we set the index initially
   // to INTERVAL_RING_SIZE, the size of the ring, so that when we look at past
   // times in the first cycle, the index doesn't go negative!
   frameDeltaTimeIndex = INTERVAL_RING_SIZE;
   frameReadTimeIndex = INTERVAL_RING_SIZE;
   for (int i = 0; i < INTERVAL_RING_SIZE; ++i)
   {
      frameDeltaTime[i] = 0.0F;
      frameReadTime[i] = 0.0F;
   }

   lastTimeFrameWasDisplayed = -frameInterval; // used for FPS readout
   nextFrameDisplayTime = 0;

   // reset the frame interval timer
   interframeDelayTimer->Start();

   sendCommandToDiskReader(ACTIVATE_READER);
}
//------------------------------------------------------------------------------

// loads a new video clip into the PLAYER, based on:
// the new video proxy index
// the new video framing index
// the new display mode index
//
int CPlayer::loadVideoClip(ClipSharedPtr &newClip, int newVideoProxyIndex, int newVideoFramingIndex, int newDisplayModeIndex)
{
   // if null clip then abort
   if (newClip == nullptr)
	{
      return -1;
   }

   TRACE_3(errout << "LOAD " << newClip->getClipName());

   // make sure READER does not go active
	lockReader(true);

//	// delete existing 'getBuf'
//	getBuf = nullptr;

   // delete existing provisional buffer storage
	provisionalFrameBuffer = nullptr;

   // load the new clip and video track
   srcClip = newClip;

   // default is VIDEO_SELECT_NORMAL
   srcVideoProxyIndex = newVideoProxyIndex;

   // default is FRAMING_SELECT_VIDEO
   srcVideoFramingIndex = newVideoFramingIndex;

   // frame list equivalent to Rev2 clip
   srcVideoFrameList = srcClip->getVideoFrameList(srcVideoProxyIndex, srcVideoFramingIndex);
   // if null frame list then abort
   if (srcVideoFrameList == nullptr)
   {
      unloadVideoClip();
		lockReader(false);
		return (-1);
   }

   // get max padded bytes per field
   srcBytesPerField = srcVideoFrameList->getMaxPaddedFieldByteCount();
   if (srcBytesPerField < 0)
   {
      TRACE_0(errout << "ERROR: Clip " << newClip->getClipName() << " is corrupt (srcBytesPerField = " << srcBytesPerField << "), so cannot load it");
		lockReader(false);
      unloadVideoClip();
		return -1;
   }

   // Paint II needs to distinguish "fast"
   // and "slow" frames - this does it nice-
   // ly (see displayFrame.cpp "fast" arg)
   subsampledPlayback = true;
   subsampledPanning = true;

   // get min and max frame indices
   srcMinFrame = srcVideoFrameList->getInFrameIndex();
   srcMaxFrame = srcVideoFrameList->getOutFrameIndex() - 1;

   // get timecode limits for display on timeline
   CVideoFrame *framePtr;

   framePtr = srcVideoFrameList->getFrame(srcMinFrame);
   if (framePtr == nullptr)
   {
      TRACE_0(errout << "ERROR: Clip " << newClip->getClipName() << " is corrupt, so cannot load it");
		lockReader(false);
		unloadVideoClip();
		return -1;
   }

   framePtr->getTimecode(*srcMinTimecode);

   framePtr = srcVideoFrameList->getFrame(srcMaxFrame);
   if (framePtr == nullptr)
   {
      TRACE_0(errout << "ERROR: Clip " << newClip->getClipName() << " is corrupt, so cannot load it");
		lockReader(false);
		unloadVideoClip();
      return -1;
   }

   framePtr->getTimecode(*srcMaxTimecode);

   // get the image stats - changed 8/20/01 so that image format
   // is permanent
	srcVideoImageFormat = srcClip->getImageFormat(srcVideoProxyIndex);
   srcVisibleImageTop = srcVideoImageFormat->getVerticalIntervalRows();
   srcVisibleImageBottom = srcVisibleImageTop + srcVideoImageFormat->getLinesPerFrame() - 1;
   srcVisibleImageLeft = srcVideoImageFormat->getHorizontalIntervalPixels();
   srcVisibleImageRight = srcVisibleImageLeft + srcVideoImageFormat->getPixelsPerLine() - 1;
   srcEntireImageHeight = srcVisibleImageBottom + 1;
   srcEntireImageWidth = srcVisibleImageRight + 1;
   srcPixelAspectRatio = srcVideoImageFormat->getPixelAspectRatio();
   srcPixelComponents = srcVideoImageFormat->getPixelComponents();
   srcBitsPerComponent = srcVideoImageFormat->getBitsPerComponent();
   srcInterlaced = srcVideoImageFormat->getInterlaced();
   pxlComponentCount = srcVideoImageFormat->getComponentCountExcAlpha();

   // get max fields per frame
   srcMaxFieldCount = srcVideoFrameList->getMaxTotalFieldCount();

   // get visible fields per frame
   srcVisibleFieldCount = 1;
   if (srcInterlaced)
   {
      srcVisibleFieldCount++;
   }

   // get invisible fields per frame
   srcInvisibleFieldCount = srcMaxFieldCount - srcVisibleFieldCount;

   // Reset the current preferred time display style to the default.
   CurrentTC = *srcMinTimecode;
   auto defaultTcStyle = CurrentTC.getDefaultUserPreferredStringStyle();
   CurrentTC.setCurrentUserPreferredStringStyle(defaultTcStyle);

	// -------------------
   // If the clip isn't timecode-based, change the play speed to match the default FPS.
   // Otherwise set the FPS that was baked into the clip.
   int fps = CurrentTC.getFramesPerSecond();
   if (fps == 0)
   {
      bool df;
      CurrentTC.getDefaultFpsAndDropFrame(fps, df);
   }

	changeNominalPlaybackSpeed((fps == 0) ? 24 : fps);

	// -------------------
	// initialize the review history engine for this clip
	int retVal = reviewHistory->setClip(srcClip);
   if (retVal != 0)
   {
      // No TRACE message?
      unloadVideoClip();
		lockReader(false);
		return retVal; // ERROR:
   }

   // Clear the display ring and maybe trim the size of the display ring
   // if the frames are very large.
	for (int i = 0; i < displayRing.getMaxSize(); ++i)
   {
      displayRing[i].frameBuffer = nullptr;
   }

	MediaFrameBufferReadCache readCache;
   size_t cacheSize = readCache.getMediaFrameBufferCacheSizeInBytes();
   size_t maxDisplayRingMemory = cacheSize / 2;
   size_t frameSizeInBytes = srcVideoImageFormat->getBytesPerField();
   size_t desiredDisplayRingSize = std::min<size_t>((maxDisplayRingMemory / frameSizeInBytes), displayRing.getMaxSize());
   displayRing.setSize(desiredDisplayRingSize);

	// ------------------
	// [These comments make little sende to me.]
	// this is used to set the startup display device(s) after
	// one frame is displayed - see method "oneFrameToWindow33"
	// [left in SDI monitor stuff QQQ]
   initialDisplayMode = newDisplayModeIndex;

	// This function no longer sets the initial display devices, but instead
	// performs the allocation ASSUMING that the LOCAL display is specified
	// A subsequent call to "setDisplayDevices" further downstream in Player
	// method "oneFrameToWindow" then sets up the display devices just after
	// the first frame is displayed. This is done to reduce the delay between
	// setting the devices and displaying the first frame.

	displayMode = DISPLAY_TO_WINDOW;

// This is no longer variable so is done in the CPlayer constructor.
//	// ------------------
//	// Compute desired size of the display ring buffer.
//	// QQQ NOT SURE WHAT THE "LEAD" IS FOR (SDI?)
//	// QQQ MAY NEED TO RESTRICT NUMBER OF SLOTS IN THE RING BASED ON CURRENT CACHE SIZE
//	idisplayBuffLead = 5;
//	displayRing.size() = DISPLAYBUFCNT + idisplayBuffLead;

	// ------------------
	// Allocate new provisional buffer storage & reset the state.
	provisionalFrameBuffer = MediaFrameBuffer::CreatePrivateBuffer(*srcVideoImageFormat);
	provisionalDisplayMode = DISPLAY_FROM_CLIP;
	clearProvisionalFrameIndex();
	prvPixelRegionList = nullptr;

	// ------------------
	// Allocate the "review" buffer, whatever that is
	// Gag me. Alpha clips must have enough buf space for intermediates!
	// get the size in bytes of the internal format frame
	int totPels = srcEntireImageWidth * srcEntireImageHeight;
	int newReviewBufSize = totPels * 3 * 2;
	switch (srcVideoImageFormat->getAlphaMatteType())
	{
      case IF_ALPHA_MATTE_1_BIT:
         newReviewBufSize += (totPels + 7) / 8;
         break;

      case IF_ALPHA_MATTE_2_BIT_EMBEDDED:
         newReviewBufSize += (totPels + 3) / 4;
         break;

      case IF_ALPHA_MATTE_8_BIT:
      case IF_ALPHA_MATTE_8_BIT_INTERLEAVED:
         newReviewBufSize += totPels;
         break;

      case IF_ALPHA_MATTE_10_BIT_INTERLEAVED:
      case IF_ALPHA_MATTE_16_BIT:
      case IF_ALPHA_MATTE_16_BIT_INTERLEAVED:
         newReviewBufSize += totPels * 2;
         break;

      case IF_ALPHA_MATTE_NONE:
         // Do nothing
         break;

      case IF_ALPHA_MATTE_DEFAULT:
      case IF_ALPHA_MATTE_INVALID:
      default:
         MTIassert(false);
         break;
	}

   // allocate 16-bits per component
   newReviewBufSize = (newReviewBufSize + 1) / 2;
   if (newReviewBufSize > reviewBufSize)
   {
		delete[] reviewBuf;
		reviewBuf = new MTI_UINT16[newReviewBufSize];
      reviewBufSize = newReviewBufSize;
	}

	// ------------------
	// Get the correct line engine for this format
	// (used for drawing graphics into processed frame).
	lineEngine = CLineEngineFactory::makePixelEng(*srcVideoImageFormat);
   if (lineEngine == nullptr)
   {
      _MTIErrorDialog("Unable to open this clip");
		lockReader(false);
		return -1;
	}

	// ------------------
	// init the play marks and the limits-save stack
   srcMinMark = MARK_NOT_SET;
   srcMaxMark = MARK_NOT_SET;
   iLimStackSiz = LIMITS_STACK_DEPTH;
   iLimStackPtr = 0;

   // init the queued-frames FIFO
   jogFrmQFillPtr = jogFrmQEmptyPtr = 0;

   // init the last frame and last buffer
   lastDisplayedFrameIndex = -2;
	lastDisplayedFrameBuffer = nullptr;

   // init the review playback flag
   playbackFilter = PLAYBACK_FILTER_NONE;

   // unlock the command queue
	lockReader(false);

   // success
   return 0;
}
//------------------------------------------------------------------------------

void CPlayer::unloadVideoClip()
{
   if (srcClip == nullptr)
   {
      return;
   }

   TRACE_3(errout << "UNLOAD " << srcClip->getClipName());

   // this should have been here from the beginning;
	// the caller shouldn't have to worry about stop-
   // ping the PLAYER. See BUGZILLA bug 618. KT
   fullStop();

   // delete existing provisional buffer and clear out the display ring buffer.
	provisionalFrameBuffer = nullptr;
	for (int i = 0; i < displayRing.size(); ++i)
   {
      displayRing[i].frameBuffer = nullptr;
   }

   // now we can reset these pointers. Note how
   // SRCVIDEOFRAMELIST is tested in the code
   // to indicate when there's no clip loaded
   srcClip = nullptr;
   srcVideoProxyIndex = -1;
   srcVideoFramingIndex = -1;
   srcVideoFrameList = nullptr;

   mainWindow->HideFrameBufferErrorMessage();
}
//------------------------------------------------------------------------------

bool CPlayer::isAVideoClipLoaded()
{
   return (srcClip != nullptr);
}
//------------------------------------------------------------------------------

string CPlayer::getLoadedVideoClipName()
{
   if (srcClip == nullptr)
   {
      return string("");
   }

   return srcClip->getClipPathName();
}
//------------------------------------------------------------------------------

// ------------------------------------------------------------------------------
// these methods will leave the Player when NavSystemInterface is rationalized

const CImageFormat *CPlayer::getVideoClipImageFormat()
{
   return (srcVideoImageFormat);
}
//------------------------------------------------------------------------------

int CPlayer::getEntireImageWidth()
{
   return (srcEntireImageWidth);
}
//------------------------------------------------------------------------------

int CPlayer::getEntireImageHeight()
{
   return (srcEntireImageHeight);
}
//------------------------------------------------------------------------------

int CPlayer::getVisibleImageTop()
{
   return (srcVisibleImageTop);
}
//------------------------------------------------------------------------------

int CPlayer::getVisibleImageBottom()
{
   return (srcVisibleImageBottom);
}
//------------------------------------------------------------------------------

int CPlayer::getVisibleImageLeft()
{
   return (srcVisibleImageLeft);
}
//------------------------------------------------------------------------------

int CPlayer::getVisibleImageRight()
{
   return (srcVisibleImageRight);
}
//------------------------------------------------------------------------------

double CPlayer::getVideoClipPixelAspectRatio()
{
   return (srcPixelAspectRatio);
}
//------------------------------------------------------------------------------

int CPlayer::getVideoClipBitsPerComponent()
{
   return (srcBitsPerComponent);
}
//------------------------------------------------------------------------------

bool CPlayer::getVideoClipInterlaced()
{
   return (srcInterlaced);
}
//------------------------------------------------------------------------------

bool CPlayer::getSubsampledPlaybackMode()
{
   return (subsampledPlayback);
}
//------------------------------------------------------------------------------

void CPlayer::setSubsampledPlaybackMode(bool enbl)
{
   // KT Paint
   // Paint requires this so we can dis-
   // tinguish "slow" frames from "fast"
   subsampledPlayback = true; // enbl;
}
//------------------------------------------------------------------------------

//
// this allows us to set the playback speed
//
void CPlayer::setPlaybackSpeed(int fps)
{
   playbackSpeed = fps;

   // zero is a code for NOMINAL
   if (fps == 0)
   {
      fps = srcFramesPerSecond;
   }

   if (fps == 0)
   {
      // paranoia
      fps = 24;
   }

   // The frame interval, in milliseconds,
   // with a convenient fudge factor of 1.4%
   // NOTE: TIMING IS NOW ACCURATE so no need for fudge factor any more
   nominalFrameInterval = 1000. / ((double)fps /* * (1.014) */);

   // speed change takes effect immediately
   frameInterval = nominalFrameInterval;
}
//------------------------------------------------------------------------------

int CPlayer::getPlaybackSpeed()
{
   return (playbackSpeed);
}
//------------------------------------------------------------------------------

void CPlayer::changeNominalPlaybackSpeed(int fps)
{
   srcFramesPerSecond = fps;
   setDisplaySpeed(DISPLAY_SPEED_NOMINAL, 0);
   if (displaySpeedIndex == DISPLAY_SPEED_NOMINAL)
   {
      setDisplaySpeedIndex(DISPLAY_SPEED_NOMINAL);
   }

   deltaOneThird = srcFramesPerSecond / 3;
   setDisplaySpeed(DISPLAY_SPEED_UNLIMITED, SPEED_UNLIMITED_FPS);

   // the time interval for nominal playback
   nominalFrameInterval = 1000. / (double)srcFramesPerSecond;
}
//------------------------------------------------------------------------------

//
// this sets up a menu of display (playback) speeds
//
void CPlayer::setDisplaySpeed(int indx, int fps)
{
   if (indx == DISPLAY_SPEED_NOMINAL)
   {
      displaySpeed[indx] = srcFramesPerSecond;
   }
   else if (indx == DISPLAY_SPEED_UNLIMITED)
   {
      displaySpeed[indx] = SPEED_UNLIMITED_FPS;
   }
   else
   {
      displaySpeed[indx] = fps;
   }

   // if the table entry being reset is the
   // one currently selected, then the value
   // of playbackSpeed must be adjusted. The
   // call to setDisplaySpeedIndex does this
   if (indx == displaySpeedIndex)
   {
      setDisplaySpeedIndex(indx);
   }
}
//------------------------------------------------------------------------------

//
// this returns the value of a menu entry
//
int CPlayer::getDisplaySpeed(int indx)
{
   return (displaySpeed[indx]);
}
//------------------------------------------------------------------------------

//
// this sets the display speed from the menu
//
void CPlayer::setDisplaySpeedIndex(int indx)
{
   setPlaybackSpeed(displaySpeed[indx]);

   displaySpeedIndex = indx;
}
//------------------------------------------------------------------------------

int CPlayer::getDisplaySpeedIndex()
{
   return displaySpeedIndex;
}
//------------------------------------------------------------------------------

float CPlayer::getActualDisplaySpeed()
{
   return actualDisplaySpeed;
}
//------------------------------------------------------------------------------

//
// these set and get the playback filter (!)
//
void CPlayer::setPlaybackFilter(int fltr)
{
   if (playbackFilter == fltr)
   {
      return;
   }

   playbackFilter = fltr;

   // notify the tool so that it may adjust
   // ts processing when frames are drawn
   CToolManager toolManager;
   toolManager.onPlaybackFilterChange(fltr);

   // Redraw current frame with new filter.
   if (!isDisplaying())
   {
      refreshFrameSynchronousCached();
   }
}
//------------------------------------------------------------------------------

int CPlayer::getPlaybackFilter()
{
   return playbackFilter;
}
//------------------------------------------------------------------------------

void CPlayer::setInitialDisplayMode(int newmode)
{
   initialDisplayMode = newmode;
}
//------------------------------------------------------------------------------

int CPlayer::getInitialDisplayMode()
{
   return (initialDisplayMode);
}
//------------------------------------------------------------------------------

// set the display mode to use the computer monitor,
// the remote monitor, or both, using these arg vals
//
// DISPLAY_TO_WINDOW  1
// DISPLAY_TO_MONITOR 2
// DISPLAY_TO_BOTH    3
//
// Bugzilla 2477 - we add an argument 'force' to
// cause initialization irrespective of whether
// the monitor bit has changed
//
int CPlayer::setDisplayMode(int newmode, bool force)
{
   // must be inited
   int noLocal;
   int noRemote = false;

   // if no clip is loaded do nothing
   if (srcVideoFrameList == nullptr)
   {
      return 0;
   }

   // break out the 4K case, which can
   // only be displayed in LOCAL mode
   if (srcEntireImageWidth > FRAME4K)
   {
      newmode = DISPLAY_TO_WINDOW;
   }

   // Don't invoke main window refresh during this procedure
   inhibitMainWindowRefreshFlag = true;

   // lock the command queue so that all
   // commands except UNLOCK are NO-OPped
	lockReader(true);

// SAVE THIS CODE for SDI monitor
   // if we have a change in the display mode's MONITOR bit OR
   // if the user wants to force the setting of the mode
//   if (((displayMode & DISPLAY_TO_MONITOR) != (newmode & DISPLAY_TO_MONITOR)) || force)
//   {
//
//      // free up the buffers & turn off video
//      FreeVideo();
//
//      // allocate buffers & reset video
//
//      // the new mode determines whether we use
//      // hardware-specific buffers
//      EPixelComponents pixelComponentsRemoteDisplay = mainWindow->GetRemoteDisplayFormatPreference();
//      noRemote = AllocVideo(displayRing.size() + 1, srcBytesPerField, srcVisibleFieldCount);
//
//      if (noRemote)
//      { // allocation fails for REMOTE or BOTH
//
//         // revert to local
//         newmode = DISPLAY_TO_WINDOW;
//
//         int noLocal = AllocVideo(displayRing.size() + 1, srcBytesPerField, srcVisibleFieldCount);
//
//         if (noLocal)
//         { // allocation fails hard
//
//            inhibitMainWindowRefreshFlag = false;
//
//            // exit with the PLAYER locked
//            return PLAYER_ERROR_ALLOC_VIDEO_FAILURE_LOCAL;
//
//         }
//      }
//
//      // reallocate the visible fields for the ring buffer slots
//      for (int i = 0; i < displayRing.size(); ++i)
//      {
//         for (int j = 0; j < srcVisibleFieldCount; ++j)
//         {
//            displayRing[i].buf[j] = getDataVideo(i, j);
//            displayRing[i].offsetBuf[j] = displayRing[i].buf[j];
//         }
//      }
//
//      // reallocate the provisional frame's fields
//      // leave the invisible field slots alone
//      for (int j = 0; j < srcVisibleFieldCount; ++j)
//      {
//         provisionalFrameBuffer[j] = getDataVideo(displayRing.size(), j);
//      }
//
//      provisionalDisplayMode = DISPLAY_FROM_CLIP;
//      clearProvisionalFrameIndex();
//   }
// SDI MONITOR STUFF

   // set the new display mode
   displayMode = newmode;

   // resume refreshing main window
   inhibitMainWindowRefreshFlag = false;

   // unlock the command queue
	lockReader(false);

   // now we're ready for the dialog box from the original allocation
   if (noRemote)
   {

      const string AllocBuffErrMsg = "Clip cannot be played on the REMOTE monitor";
      _MTIErrorDialog(AllocBuffErrMsg);
   }
   else
   {
      // synchronize the displays
      // Note: changing this to SYNCHRONOUS cures Bugzilla ref #1391
      goToFrameSynchronous(lastDisplayedFrameIndex);
   }
	return 0;
}
// -------------------------------------------------------------------------

// This sets the Display mode to WINDOW, then back to what it was, which
// is useful for when the preference is changed in the Edit->Preferences
// dialog.
// 1/11/2005 - mpr
int CPlayer::resetDisplayMode()
{
	int displayModeSave = displayMode;
	int iRet = 0;

	if (displayMode == DISPLAY_TO_WINDOW)
	{ // If already DISPLAY_TO_WINDOW
		return 0;
	} // then just return
	if (displayMode == DISPLAY_TO_NEITHER)
	{ // If this, then not really init'ed
		return 0;
	} // so just return

	iRet = setDisplayMode(DISPLAY_TO_WINDOW);
	if (iRet != 0)
	{
		return (iRet);
	}
   iRet = setDisplayMode(displayModeSave);
   if (iRet != 0)
   {
      return (iRet);
   }

   return (0);
}
// -------------------------------------------------------------------------

// return the current display mode (for the menu)
//
int CPlayer::getDisplayMode()
{
   return (displayMode);
}
// ------------------------------------------------------

// allows the mode of display for the single provisional
// frame to be altered. When the mode has been set to
// 'DISPLAY_FROM_CLIP', the source data for the provisional
// frame number is the clip itself. When the mode has been
// set to 'DISPLAY_PROVISIONAL', the source data for the
// provisional frame number is the provisional frame buffer.
// Used for toggling between original and repaired versions
// of a given frame.
void CPlayer::setProvisionalMode(int newmode)
{
   provisionalDisplayMode = newmode;
}
//------------------------------------------------------------------------------

int CPlayer::getProvisionalMode()
{
	return provisionalDisplayMode;
}
//------------------------------------------------------------------------------

// sets the frame index for the provisional frame. This is
// needed
// i) to tell the Player when to pick up data from
// the provisional buffer instead of the clip
//
// ii) to update the timecode and slider when the
// provisional frame is displayed
void CPlayer::setProvisionalFrameIndex(int frmnum)
{
	// locked for the duration of the call
	CAutoThreadLocker prvLock(*prvPixelRegionListLock);

   provisionalFrameIndex = frmnum;
}
//------------------------------------------------------------------------------

// you can clear the provisional frame number so that the
// provisional data will not be displayed during playing
// of the clip. You can still refresh the display of the
// frame (but without update of timecode and slider);
void CPlayer::clearProvisionalFrameIndex()
{
   // locked for the duration of the call
	CAutoThreadLocker prvLock(*prvPixelRegionListLock);

   isProvisionalReadyToDisplay = false;
   provisionalFrameIndex = -1;
}
//------------------------------------------------------------------------------

int CPlayer::getProvisionalFrameIndex()
{
	return provisionalFrameIndex;
}
//------------------------------------------------------------------------------

void CPlayer::setHighlightColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   // set the color for highlighting into the native frame
   lineEngine->setFGColor(r, g, b);

   // set the color for highlighting into the graphics window
   int col;
   if ((r != 0) && (g == 0) && (b == 0))
   {
      col = SOLID_RED;
   }
   else if ((r == 0) && (g != 0) && (b == 0))
   {
      col = SOLID_GREEN;
   }
   else if ((r == 0) && (g == 0) && (b != 0))
   {
      col = SOLID_BLUE;
   }
   else if ((r != 0) && (g != 0) && (b == 0))
   {
      col = SOLID_YELLOW;
   }
   else if ((r != 0) && (g == 0) && (b != 0))
   {
      col = SOLID_MAGENTA;
   }
   else if ((r == 0) && (g != 0) && (b != 0))
   {
      col = SOLID_CYAN;
   }
   else if ((r == 0) && (g == 0) && (b == 0))
   {
      col = SOLID_DKGRAY;
   }
   else
   {
      col = SOLID_LTGRAY;
   }

   mainWindow->GetDisplayer()->setHighlightColor(col);
}
//------------------------------------------------------------------------------

// this takes a 16-bit "444" progressive intermediate and
// transforms it back into a native frame in the provisional
// buffer
void CPlayer::loadProvisionalFrame(MTI_UINT16 *frm, CPixelRegionList *pxlrgnlst)
{
	MTI_UINT8 *stupidBuf[2] = { provisionalFrameBuffer->getImageDataPtr(), nullptr };
	extractor->replaceActivePicture(stupidBuf, srcVideoImageFormat, frm);

   // This is where the GREEN highlights get drawn, for example after
   // autofilter preview
   if (pxlrgnlst != nullptr)
	{
//		lineEngine->setExternalFrameBuffer(stupidBuf);
//      pxlrgnlst->HighlightRunsInFrameBuffer(lineEngine);
   }
}
//------------------------------------------------------------------------------

// When a pixel region list is loaded here it will be drawn
// atop the provisional frame whether it is sourced from the provisionalFrameBuffer
// or from the clip.
//
// This is called from the AutoFilter processing thread, and will be
// blocked automatically if the prvPixelRegionListLock is set (as it
// is when the prvPixelRegionList is being displayed). Should fix
// Bugzilla ref #908.
//
void CPlayer::loadProvisionalPixelRegionList(CPixelRegionList *pxlrgnlst)
{
   // locked for the duration of the call
	CAutoThreadLocker prvLock(*prvPixelRegionListLock);

   if (prvPixelRegionList != nullptr)
   {
      delete prvPixelRegionList;
      prvPixelRegionList = nullptr;
   }
   try
   {
      if (pxlrgnlst != nullptr)
      {
         prvPixelRegionList = pxlrgnlst->MakeCopyWithNoRuns();
      }
   }
   catch (std::bad_alloc)
   {
      TRACE_0(errout << "ERROR: OUT OF MEMORY while copying provisional pixel region list");
   }
}
//------------------------------------------------------------------------------

// UNSAFE - removed by mbraca
// CPixelRegionList *CPlayer::getProvisionalPixelRegionList()
// {
// return(prvPixelRegionList);
// }
//------------------------------------------------------------------------------

// this takes the contents of the provisional buffer and
// displays them. If the provisional frame has been set
// then the timecode and slider are also updated
void CPlayer::displayProvisionalFrame()
{
   isProvisionalReadyToDisplay = true; // (see 'getNextDisplayFrame')
}
//------------------------------------------------------------------------------

// convert a 16-bit "444" progressive intermediate into
// native format, using the provisional buffer, and set
// the one-shot flag to cause display to take place in
// the display work proc. If the provisional frame index
// has been set to a value other than -1, the timecode
// and slider will be updated. If the prov frame index
// is set to -1, the timecode and slider will not be
// updated. In general,  you want to set the provisional
// frame (using 'setProvisionalFrame') when you know
// which frame is to be the target of the fix. Then
// 'refreshFromIntermediate' can be used to update the
// display with different versions of the fixed frame
// as generated by DRS and the arrow keys.
//
// The argument pxlrgnlst is used to superimpose graphics
// upon the frame to be refreshed. See "PixelRegions.cpp"
void CPlayer::refreshFromIntermediate(MTI_UINT16 *frm, CPixelRegionList *pxlrgnlst)
{
	// Make sure the player is not doing anything else
	fullStop();

   // intermediate->native in provisional buf
   loadProvisionalFrame(frm, pxlrgnlst);

   // set flag to display frm provisional
   displayProvisionalFrame();

   // advise that we want a main window refresh sooner rather than later
   expediteMainWindowRefresh();
}
//------------------------------------------------------------------------------

void CPlayer::displayFromRenderThread(MTI_UINT16 *internalFrameData, int frameIndex)
{
	// Old debugging code - should probably get rid of it! QQQ
	MTIassert(frameIndex >= 0);
	if (frameIndex < 0)
	{
		static bool oneshot = false;
		if (!oneshot)
		{
			oneshot = true;
			_MTIErrorDialog("INTERNAL ERROR! frameIndex = -1 [displayFromRenderThread]");
		}
	}

	// No idea why this is here! QQQ
	setTrackBarUpdate(true);

	// Construct a new media frame buffer and convert the incoming data from
	// internal to native format into it.
	auto nativeFrameBuffer = MediaFrameBuffer::CreatePrivateBuffer(*srcVideoImageFormat);
	MTI_UINT8 *stupidBuf[2] = { nativeFrameBuffer->getImageDataPtr(), nullptr };
	extractor->replaceActivePicture(stupidBuf, srcVideoImageFormat, internalFrameData);

	// Wait for a slot to open up in the display ring.
	while ((idisplayFillPtr - idisplayLagPtr) >= displayRing.size() - idisplayBuffLead)
   {
      MTImillisleep(1);
   }

	// Stick the native-format buffer in the display ring.
	displayRing[idisplayFillPtr].frameBuffer = nativeFrameBuffer;
	displayRing[idisplayFillPtr].iframeIndex = frameIndex;
	displayRing[idisplayFillPtr].syncUp = true;
	idisplayFillPtr++;

//	UVTRACE(errout << "############### MEH BUMP FILL -> " << idisplayFillPtr);
   MTIassert(idisplayEmptyPtr <= idisplayFillPtr);

	// We want this to be synchronous, so wait here until the frame is displayed.
	while (idisplayEmptyPtr < idisplayFillPtr)
   {
      MTImillisleep(1);
   }
}
//------------------------------------------------------------------------------

bool CPlayer::windowDisplayIsEnabled()
{
	if ((displayMode & DISPLAY_TO_WINDOW) == 0)
   {
      return (false);
   }
   else
   {
      return (true);
   }
}
//------------------------------------------------------------------------------

bool CPlayer::monitorDisplayIsEnabled()
{
   if ((displayMode & DISPLAY_TO_MONITOR) == 0)
   {
      return (false);
   }
   else
   {
      return (true);
   }
}
//------------------------------------------------------------------------------

// this method outputs the last frame (obtained
// by GETNEXTDISPLAYFRAME) to the video monitor
void CPlayer::outputVideo()
{
	// // the last buffer index is set in GETNEXTDISPLAYFRAME
	// hdwBuf->OutputVideo(ilastBuffer);
}
//------------------------------------------------------------------------------

int CPlayer::checkTimecodeLimits(int srcfrm, CVideoFrameList *videoFrameList)
{
   int minFrame = srcMinFrame;
   int maxFrame = srcMaxFrame;

   if (videoFrameList)
   {
      minFrame = videoFrameList->getInFrameIndex();
      maxFrame = videoFrameList->getOutFrameIndex() - 1;
   }

   int retVal = srcfrm;

   // Can't fail - restricted to frame range.
   if (retVal < minFrame)
   {
      retVal = minFrame;
   }
   else if (retVal > maxFrame)
   {
      retVal = maxFrame;
   }

   return retVal;
}
//------------------------------------------------------------------------------

int CPlayer::readMediaFrameBuffer(CVideoFrameList *videoFrameList, int frameIndex, MediaFrameBufferSharedPtr &mediaFrameBuffer)
{
	MediaFrameBufferSharedPtr localFrameBuffer;

	if (videoFrameList == nullptr)
	{
		videoFrameList = srcVideoFrameList;
	}

   TRACE_3(errout << "READ FROM " << srcClip->getClipName());

	int retVal = videoFrameList->readMediaFrameBuffer(frameIndex, localFrameBuffer);

	if (retVal || !localFrameBuffer)
	{
		MTIassert(localFrameBuffer);
		if (!localFrameBuffer)
		{
			UVTRACE(errout << "ERROR: GOT A nullptr FRAME BUFFER FOR" << frameIndex << " (" << retVal << ")");
			MediaFrameBuffer::ErrorInfo errorInfo { retVal, "", "Media file is missing!", "" };
			localFrameBuffer = MediaFrameBuffer::CreateErrorBuffer(*srcVideoImageFormat, errorInfo);
		}
		else
		{
			UVTRACE(errout << "ERROR: GOT ERROR READING CURRENT FRAME " << frameIndex
                        << " (" << localFrameBuffer->getErrorInfo().code << ")");
		}
	}
	else if (localFrameBuffer->getSwapStatus() != MediaFrameBuffer::NotSwapped)
	{
		UVTRACE(errout << "___________________  GOT A SWAPPED FRAME BUFFER FOR  " << frameIndex << " _________________ ");

		MediaFrameBufferSharedPtr newLocalFrameBuffer = MediaFrameBuffer::CreatePrivateBuffer(*srcVideoImageFormat);
		unsigned char *copyFrom = (unsigned char *) localFrameBuffer->getImageDataPtr();
		unsigned char *copyTo = (unsigned char *) newLocalFrameBuffer->getImageDataPtr();
		size_t copySize = localFrameBuffer->getImageDataSize();

		switch (localFrameBuffer->getSwapStatus())
		{
		default:
			MTImemcpy(copyTo, copyFrom, copySize);
			break;

		case MediaFrameBuffer::Swapped16:
			{
				MTI_UINT16 *swapFrom = (MTI_UINT16*)copyFrom;
				MTI_UINT16 *swapTo = (MTI_UINT16*)copyTo;
				size_t swapCount = copySize / sizeof(MTI_UINT16);
				SwapINT16(swapFrom, swapTo, swapCount);
				break;
			}

		case MediaFrameBuffer::Swapped32:
			{
				MTI_UINT32 *swapFrom = (MTI_UINT32*)copyFrom;
				MTI_UINT32 *swapTo = (MTI_UINT32*)copyTo;
				size_t swapCount = copySize / sizeof(MTI_UINT16);
				SwapINT32(swapFrom, swapTo, swapCount);
				break;
			}
		}

		// TODO: COPY/SWAP ALPHA DATA! QQQ

		localFrameBuffer = newLocalFrameBuffer;
	}
	else
	{
		UVTRACE(errout << "___________________  GOT A GOOD FRAME BUFFER FOR  " << frameIndex << " _________________ ");
	}

   mediaFrameBuffer = localFrameBuffer;

	return retVal;
}
//------------------------------------------------------------------------------

//
// this is a simple method for a tool to obtain
// an arbitrary frame in intermediate form. Uses
// getBuf so it can be called when Player running
// NOTE: GETBUF is no longer needed - we just use a temp MediaFrameBuffer.
int CPlayer::getToolFrame(MTI_UINT16 *userBuffer, int frameIndex, CVideoFrameList *videoFrameList)
{
	int retVal;

	int minFrame = videoFrameList ? videoFrameList->getInFrameIndex() : srcMinFrame;
	int maxFrame = videoFrameList ? (videoFrameList->getOutFrameIndex() - 1) : srcMaxFrame;

	// barring other problems, always get a frame
	if (frameIndex < minFrame)
	{
		frameIndex = minFrame;
	}
	else if (frameIndex > maxFrame)
	{
		frameIndex = maxFrame;
	}

	// Get the native formatr frame buffer for the desired frame.
	MediaFrameBufferSharedPtr nativeFrameBuffer;
	retVal = readMediaFrameBuffer(videoFrameList, frameIndex, nativeFrameBuffer);
	if (retVal != 0)
	{
		// prob other than frame index
		return retVal;
	}

	if (nativeFrameBuffer->getErrorInfo().code != 0)
	{
		return nativeFrameBuffer->getErrorInfo().code;
	}

	// read succeeded - extract the frame
	MTI_UINT8 *stupidBuf[2] = { nativeFrameBuffer->getImageDataPtr(), nullptr };
   CImageFormat frameImageFormat = nativeFrameBuffer->getImageFormat();
	extractor->extractActivePicture(userBuffer, stupidBuf, srcVideoImageFormat, &frameImageFormat);

	// return the index of the frame we actualy fetched!
   return frameIndex;
}

// ------------------------------------------------------------------------------
//
// This method accepts intermediate data from a tool, converts it to native
// format, and places it into the display ring for display and writeback. If
// interlaced, is assumed to be in the first intermediate buffer in the array.
// If the format is interlaced, the subsequent buffers in the array (representing
// the invisible fields) contain either even or odd lines only. The argument
// aux is an auxiliary buffer of the same size to hold intermediates of the
// invisible fields
//

int CPlayer::putToolFrame(MTI_UINT16 *intermediateBuffer, MTI_UINT16 *aux, CPixelRegionList& orgpels, int frameIndex)
{
	CAutoErrorReporter autoErr("CPlayer::putToolFrame", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   // Can't write the frame if the frame list is locked!
   if (srcVideoFrameList->isLocked())
   {
		autoErr.errorCode = CLIP_ERROR_FRAME_LIST_IS_LOCKED;
		autoErr.msg << "Cannot modify BOTH the video AND the film frames!";
      return CLIP_ERROR_FRAME_LIST_IS_LOCKED;
   }

   int retVal;

   // when using the provisional buffer to write back
   // frames from a tool, the provisional frame index
   // must always be cleared to ensure that the con-
   // tents of the provisional buffer are never passed
   // by the Player in lieu of a clip frame
   clearProvisionalFrameIndex();

   CSaveRestore history;
   auto historyIsFucked = history.setClip(srcClip);
   if (historyIsFucked != 0)
   {
		autoErr.errorCode = CLIP_ERROR_FRAME_LIST_IS_LOCKED;
		autoErr.msg << "Internal error: tried to modify BOTH the video AND the film frames!";
      return CLIP_ERROR_FRAME_LIST_IS_LOCKED;
   }

   // we'll need info about the frame and its fields
   CVideoFrame *frame = srcVideoFrameList->getFrame(frameIndex);
   if (frame == nullptr)
   {
		autoErr.errorCode = -1;
		autoErr.msg << "Cannot write modified frame because the clip is corrupt";
      return -1;
   }

   int fieldCount = frame->getTotalFieldCount();
   CVideoField *field;

   if (!historyIsFucked)
   {
      // perform the history save operation atomically
      CAutoHistoryOpener historyOpener(history, FLUSH_TYPE_SAVE, historyIsFucked);

      // Hmmm do we really want to abort if there's a problem with history? QQQ
      if (historyIsFucked)
      {
         autoErr.errorCode = historyIsFucked;
         autoErr.msg << "Cannot open the history file for frame " << frameIndex;
         historyOpener.dontFlush(); // ugghh
      }
      else
      {
         //
         // record the original pixel values for the
         // target frame's visible fields in history
         //

         history.newGlobalSaveNumber();

         // save the original values for the visible fields
         historyIsFucked = history.saveRegionList(frameIndex, // frame index
               0, // field index
               EVEN_AND_ODD, // field num -1 both
               TOOL_PAINT, // tool code
               nullptr, // ->cmd string (PDL style)
               orgpels); // original values

         if (historyIsFucked)
         {
            autoErr.errorCode = historyIsFucked;
            autoErr.msg << "Cannot write to the history file for frame " << frameIndex << ". Original pixels may be unrecoverable.";
            historyOpener.dontFlush(); // ugghh
            return retVal;
         }
      }

	} // End of history opener scope

   // Tell GOV tool what pixels we are replacing
	CToolManager toolManager;
   int govToolIndex = toolManager.findTool("GOV"); // QQQ Manifest constant!!
   if (govToolIndex < 0)
   {
      return 0;
   }

   CToolObject *toolObj = toolManager.GetTool(govToolIndex);
   CGOVTool *govTool = dynamic_cast<CGOVTool*>(toolObj);
   MTIassert(govTool != nullptr);
   govTool->SetGOVRepeatShape(SELECTED_REGION_SHAPE_REGION, orgpels.GetBoundingBox(), nullptr, &orgpels);

   // having saved the original values, go on
   // and write back the new version of the
	// target frame to the clip media

	auto rawFrameBuffer = std::make_shared<RawMediaFrameBuffer>(srcVideoImageFormat->getBytesPerField());
	auto nativeMediaFrameBuffer = MediaFrameBuffer::CreateSharedBuffer(rawFrameBuffer, *srcVideoImageFormat, /* isDecoded */ true);
	MTI_UINT8 *stupidBuf[2] = { nativeMediaFrameBuffer->getImageDataPtr(), nullptr };
	extractor->replaceActivePicture(stupidBuf, srcVideoImageFormat, intermediateBuffer);

	// Write the buffer out.
	retVal = srcVideoFrameList->writeMediaFrameBuffer(frameIndex, nativeMediaFrameBuffer);
	if (retVal != 0)
	{
		MTIostringstream os;
		os << "Write failed for frame " << frameIndex << "! (" << retVal << "). Changes are lost.";
		_MTIErrorDialog(os.str().c_str());

		return retVal;
	}

	// Update field and frame list files
   // TBD only do this if we allocated new dirty media, and only update the
	// one frame!
	srcClip->updateAllTrackFiles();
   srcClip->updateAllVideoFrameListFiles();

	// record the fact that the clip media has been modified
	retVal = srcClip->updateMediaModifiedStatus(TRACK_TYPE_VIDEO, srcVideoProxyIndex);
	if (retVal != 0)
	{
		return retVal;
	}

	return 0;
}
//------------------------------------------------------------------------------

int CPlayer::putToolFrame(
		MTI_UINT16 *inifrm, // initial version of frame
		MTI_UINT16 *finfrm, // final version of frame
		MTI_UINT16 *aux,    // aux buffer for invis frame
		int frameIndex)         // frame number
{
   // Can't write the frame if the frame list is locked!
   if (srcVideoFrameList->isLocked())
	{
      _MTIErrorDialog("Cannot modify BOTH the video AND the film frames!    ");
		return CLIP_ERROR_FRAME_LIST_IS_LOCKED;
   }

   int retVal;

   // when using the provisional buffer to write back
   // frames from a tool, the provisional frame index
   // must always be cleared to ensure that the con-
   // tents of the provisional buffer are never passed
   // by the Player in lieu of a clip frame
   clearProvisionalFrameIndex();

	CSaveRestore history;
	retVal = history.setClip(srcClip);
   if (retVal != 0)
   {
      return retVal;
   }

   // we'll need info about the frame and its fields
   CVideoFrame *frame = srcVideoFrameList->getFrame(frameIndex);
   if (frame == nullptr)
   {
      TRACE_0(errout << "ERROR: CPlayer::putToolFrame: clip is corrupt");
      return -1;
	}

   int fieldCount = frame->getTotalFieldCount();
   CVideoField *field;

   // bounding box of original pels
   RECT boundingBox;

   {
      // perform the history save operation atomically
      CAutoHistoryOpener historyOpener(history, FLUSH_TYPE_SAVE, retVal);

      // Hmmm do we really want to abort if there's a problem with history? QQQ
      if (retVal != 0)
      {
         return retVal;
      }

      //
      // record the original pixel values for the
      // target frame's visible fields in history
      //

      history.newGlobalSaveNumber();

		retVal = history.saveDifferenceRuns(
				frameIndex, // frame index
            0, // field index
            EVEN_AND_ODD, // field num -1 both
            TOOL_PAINT, // tool code
            nullptr, // ->cmd string (PDL style)
            inifrm, // unprocessed
            finfrm, // processed
            inifrm, // src for org pels
            &boundingBox); // -> box
      if (retVal != 0)
      {
         historyOpener.dontFlush(); // ugghh
         return retVal;
      }

   } // End of history opener scope

   // Tell GOV tool what pixels we are replacing
   CToolManager toolManager;
   int govToolIndex = toolManager.findTool("GOV"); // QQQ Manifest constant!!
   if (govToolIndex < 0)
   {
      return 0;
   }

   CToolObject *toolObj = toolManager.GetTool(govToolIndex);
   CGOVTool *govTool = dynamic_cast<CGOVTool*>(toolObj);
   MTIassert(govTool != nullptr);
   // govTool->SetGOVRepeatShape(GOV_SELECT_SHAPE_REGION,
   // boundingBox,
   // nullptr,
   // &orgpels);

   // having saved the original values, go on
   // and write back the new version of the
   // target frame to the clip media

//	MTI_UINT8 *stupidBuf[2] = {getBuf->getImageDataPtr(), nullptr};
//	extractor->replaceActivePicture(stupidBuf, srcVideoImageFormat, finfrm // intermediate
//			);

   //
   // record the original pixel values for the
   // target frame's invisible fields in history.
   // This will take a little work...
	//
#ifdef xxxmfioxxx
//	for (int fieldIndex = 0; fieldIndex < fieldCount; ++fieldIndex)
//   {
         //
//		field = frame->getField(fieldIndex);
//      if (!field->isFieldInvisible())
//      {
//         continue;
//      } // skip over visible fields
//
//      // Read the invisible field's media
//      // NOTE: This must be done BEFORE the visible fields of the frame
//      // are written to, else in a version clip we will read
//		// when a frame is first written to
//		field->readMedia(getBuf[fieldIndex]);
//
//      // Extract the invisible field to an intermediate buffer.  You will need
//      // to know whether the field is odd, even or progressive. Find the index
//      // of the visible sibling, then take that modulo the number of fields
//      unsigned char *fieldArray[2];
//      bool oddField;
//      int evnodd;
//      if ((field->getVisibleSiblingFieldIndex() % srcVisibleFieldCount) == 0)
//      {
//         // Even field or progressive frame
//			fieldArray[0] = getBuf[fieldIndex];
//         fieldArray[1] = 0;
//         oddField = false;
//         evnodd = EVEN;
//      }
//      else
//      {
//         // Odd field
//         fieldArray[0] = 0;
//			fieldArray[1] = getBuf[fieldIndex];
//         oddField = true;
//         evnodd = ODD;
//      }
//
//      extractor->extractActivePicture(aux, fieldArray, srcVideoImageFormat);
//
//      // Copy the pixel values from the invisible field (in intermediate
//      // buffer) to the pixel region list orgpels, which is clobbered;
//      // i.e. call scratchRegionList.ReplacePixelsFromFrame
//      // See the function CToolFrameBuffer::SaveToHistory for an example
//      // of this call.
//
//      {
//         // Perform the history save operation atomically
//         //
//         // Theoretically we should only have to open/close the history
//         // files only once for all visible and invisible fields, but this
//         // works around an apparent bug in SaveRestore that overwrites
//         // visible frame original pixels with invisible frame ones!
//
//         CAutoHistoryOpener historyOpener(history, FLUSH_TYPE_SAVE, retVal);
//
//         // Hmmm do we really want to abort if there's a problem with history? QQQ
//         if (retVal != 0)
//         {
//            return retVal;
//         }
//
//         // Each invisible field save is uniquely tagged
//         history.newGlobalSaveNumber();
//
//         retVal = history.saveDifferenceRuns(frameIndex, // frame index
//               0, // field index
//               evnodd, // see above
//               TOOL_PAINT, // tool code
//               nullptr, // ->cmd string (PDL style)
//               inifrm, // unprocessed
//               finfrm, // processed
//               aux, // src for org pels
//               nullptr); // -> box
//         if (retVal != 0)
//         {
//            historyOpener.dontFlush(); // ugghh
//            return retVal;
//         }
//
//      } // End of history opener scope
//
//      // Update the invisible field (in intermediate buffer) with the
//      // changed pixel values from the visible frame buffer
//      // i.e. scratchRegionList.CopyPixelsFrameToFrame
//      // See the function CToolFrameBuffer::UpdateInvisibleFields for example
//      // orgpels.CopyPixelsFrameToFrame(frm,
//      // frameWdth, frameHght,
//      // aux,
//      // copyEven, copyOdd);
//
//      int frmWdth = srcEntireImageWidth;
//      int frmHght = srcEntireImageHeight;
//
//      MTI_UINT16 *iniPtr = inifrm;
//      MTI_UINT16 *finPtr = finfrm;
//      MTI_UINT16 *auxPtr = aux;
//      for (int i = 0; i < frmHght; i++)
//      {
//         bool oddlin = i & 1;
//         if (!((oddlin && (evnodd == ODD)) || (!oddlin && (evnodd == EVEN))))
//         {
//            iniPtr += frmWdth * pxlComponentCount;
//            finPtr += frmWdth * pxlComponentCount;
//            auxPtr += frmWdth * pxlComponentCount;
//            continue;
//         }
//         for (int j = 0; j < frmWdth; j++)
//         {
//            for (int k = 0; k < pxlComponentCount; k++)
//            {
//               if (finPtr[k] != iniPtr[k])
//               {
//                  for (int l = 0; l < pxlComponentCount; l++)
//                  {
//                     auxPtr[l] = finPtr[l];
//                  }
//                  break;
//               }
//            }
//            iniPtr += pxlComponentCount;
//            finPtr += pxlComponentCount;
//            auxPtr += pxlComponentCount;
//         }
//      }
//
//      // Replace the invisible field (intermediate buffer) back into
//      // the native field buffer
//      extractor->replaceActivePicture(fieldArray, srcVideoImageFormat, aux // intermediate
//            );
//
//	} // for each invisible field
//
#endif

//	// We now write visible and invisible fields in a single write operation
//
//	retVal = srcVideoFrameList->writeMediaAllFieldsOptimized(frameIndex, stupidBuf);
//	if (retVal != 0)
//	{
//
//		MTIostringstream os;
//
//		os << "Write failed! Error code = " << retVal;
//		_MTIErrorDialog(os.str().c_str());
//
//		return retVal;
//	}

	auto rawFrameBuffer = std::make_shared<RawMediaFrameBuffer>(srcVideoImageFormat->getBytesPerField());
	auto nativeMediaFrameBuffer = MediaFrameBuffer::CreateSharedBuffer(rawFrameBuffer, *srcVideoImageFormat, /* isDecoded */ true);
	MTI_UINT8 *stupidBuf[2] = { nativeMediaFrameBuffer->getImageDataPtr(), nullptr };
	extractor->replaceActivePicture(stupidBuf, srcVideoImageFormat, finfrm);

	// Write the buffer out.
	retVal = srcVideoFrameList->writeMediaFrameBuffer(frameIndex, nativeMediaFrameBuffer);
	if (retVal != 0)
	{
		MTIostringstream os;
		os << "Write failed! Error code = " << retVal;
		_MTIErrorDialog(os.str().c_str());

		return retVal;
	}

	// Update field and frame list files
	// TBD only do this if we allocated new dirty media, and only update the
   // one frame!
   srcClip->updateAllTrackFiles();
   srcClip->updateAllVideoFrameListFiles();

   // record the fact that the clip media has been modified
   retVal = srcClip->updateMediaModifiedStatus(TRACK_TYPE_VIDEO, srcVideoProxyIndex);
   if (retVal != 0)
   {
      return retVal;
   }

   return 0;
}

// ------------------------------------------------------------------------------
// supplies frames to the DISPLAYER from the display ring
MediaFrameBufferSharedPtr CPlayer::getNextDisplayFrame()
{
   // Short circuit if the Disk Reader thread was stopped
   if (majorState == READER_ENDED)
	{
      return nullptr;
   }

   // if there's a provisional frame waiting, pass it along
   if (isProvisionalReadyToDisplay)
   {
		isProvisionalReadyToDisplay = false;

		// last of sequence
		isLastFrameOfSequence = true;      // ??? QQQ

		lastDisplayedFrameBuffer = provisionalFrameBuffer;

      // the frame number (can be -1)
      lastDisplayedFrameIndex = provisionalFrameIndex;
		if (lastDisplayedFrameIndex < 0)
      {
         // This used to return the provisionalFrameBuffer, but I think that's wrong,
         // I think we should say there's no provisional available and
         // just return nullptr which should work fine. It would have been
         // nice if the above comment explained HOW the provisionalFrameIndex "can
			// be -1" at this point!
			return nullptr;
      }

		CVideoFrame *lastFramePtr = srcVideoFrameList->getFrame(lastDisplayedFrameIndex);

		// if the frame was out of range, punt -- should "never" happen
		if (lastFramePtr == nullptr)
		{
			TRACE_1(errout << "WARNING: Player::getNextDisplayFrame(1) misplaced a buffer");
			return nullptr;
		}

		// A hack to inform the frame write cache what frame to cache.
		// I don't remember why this is needed, but we aren't going to
		// be writing while playing, so don't call it if playing (can
		// be slow bwcause of the stupid ImageFileIO lock...)
		if (!isReallyPlaying())
		{
			lastFramePtr->DoFrameCacheHack();
		}

		lastFramePtr->getTimecode(*lastTimecode);
		lastHalfFrame = lastTimecode->isHalfFrame() ? 1 : 0;
		lastTimecodeFieldFlag = !lastTimecode->is720P() && lastTimecode->getField();

		// Notify all of changes
		CurrentTC = *lastTimecode;

		return provisionalFrameBuffer;
	}

	//UVTRACE(errout << "GETNEXTFRAME: Empty=" << idisplayEmptyPtr << ", FILL=" << idisplayFillPtr);

	// Check if there is a frame in the ring buffer that's ready to be displayed.
	MTIassert(idisplayEmptyPtr <= idisplayFillPtr);
	if (idisplayEmptyPtr >= idisplayFillPtr)
	{
		return nullptr;
	}

	// the index of the frame within the clip
	int inextFrame = displayRing[idisplayEmptyPtr].iframeIndex;

	// the sync flag of the frame
	isLastFrameOfSequence = displayRing[idisplayEmptyPtr].syncUp;

	CVideoFrame *nextFramePtr = srcVideoFrameList->getFrame(inextFrame);

	// if the frame was not accessible, punt -- should "never" happen
	if (nextFramePtr == nullptr)
	{
		MTIostringstream os;
		os << "Avoided jumping to first frame of clip!" << endl << "inextFrame=" << inextFrame << "  " << "isLastFrameOfSequence=" << isLastFrameOfSequence <<
				endl << "idisplayEmptyPtr=" << idisplayEmptyPtr << "  " << "idisplayFillPtr=" << idisplayFillPtr << "    " << endl <<
				"idisplayLagPtr=" << idisplayLagPtr << "  ";
		TRACE_1(errout << "ERROR: " << os.str());

		// Attempt full recovery:
		// short out ring buffer
		idisplayLagPtr = idisplayEmptyPtr = idisplayFillPtr;
		// free up all slots and pump last
		// frame through the ring buffer
		for (int i = 0; i < displayRing.size(); i++)
		{
			displayRing[i].iframeIndex = -3; // NO_FRAME;          QQQ
			displayRing[i].frameBuffer = nullptr;
		}

		// refresh whatever was the last frame
		goToFrame(lastDisplayedFrameIndex);
		return nullptr;
	}

	// We're clear of trouble, safe to replace 'iLastFrame' now!
	lastDisplayedFrameIndex = inextFrame;

	nextFramePtr->getTimecode(*lastTimecode);
	lastHalfFrame = lastTimecode->isHalfFrame() ? 1 : 0;
	lastTimecodeFieldFlag = !lastTimecode->is720P() && lastTimecode->getField();

	// A hack to inform the frame write cache what frame to cache.
	// I don't remember why this is needed, but we aren't going to
	// be writing while playing, so don't call it if playing (can
	// be slow bwcause of the stupid ImageFileIO lock...)
	bool temp = isReallyPlaying();
	if (temp == false)
	{
		nextFramePtr->DoFrameCacheHack();
	}

	// Notify all of changes
	CurrentTC = *lastTimecode;

	// delay for frame timing goes here
	float averageInterval = 0.0F;
	float weightedAverageInterval = 0.0F;

	// Compute the average frame rate for display in the status bar
	float timeNow = interframeDelayTimer->Read();
	float timeSinceLastDisplay = timeNow - lastTimeFrameWasDisplayed;
	lastTimeFrameWasDisplayed = timeNow;

	// Initialization hack - avoids having the index go negative
	// PARANOIA - this was already done at initialization in SetActive()
	if (frameDeltaTimeIndex < INTERVAL_RING_SIZE)
	{
		frameDeltaTimeIndex = INTERVAL_RING_SIZE;
		for (int i = 0; i < INTERVAL_RING_SIZE; ++i)
		{
			frameDeltaTime[i] = 0.0F;
		}
	}

	// save the interval in a 15-fold FIFO
	frameDeltaTime[(frameDeltaTimeIndex++) % INTERVAL_RING_SIZE] = timeSinceLastDisplay;

	if (isLastFrameOfSequence)
	{
		// last frame of sequence
		// WTF - for some reason I can't figure out, we set this sync hack
		// flag when looping hits the last frame, so it messes up the
		// speed readout by resetting to 0 every cycle! QQQ
		if (readMode != LOOP_CLIP_FWD)
		{

			actualDisplaySpeed = 0.;
			observedFrameInterval = 10.0F;
			setReallyPlaying(false);
		}
	}
	else
	{
		// get speed by computing the average of the FIFO contents
		int firstIndex = frameDeltaTimeIndex - INTERVAL_RING_SIZE;
		int lastIndex = frameDeltaTimeIndex - 1;

		float weight = 1.0F;
		const float weightGainMultiplier = 1.05F; // 1.0 for no weighting
		float weightedAccumulation = 0.0F;
		float rawAccumulation = 0.0F;
		float normalizer = 0.0F;
		float averageDelay = 0.0F;
		int countOfIncludedDelays = 0;
		for (int i = firstIndex; i <= lastIndex; ++i)
		{

			float rawDelay = frameDeltaTime[i % INTERVAL_RING_SIZE];
			float weightedDelay = rawDelay * weight;

			// Don't factor in the zeroes at start-up, and ignore delays
			// longer than one second because that probably means I was
			// paused in the debugger
			if (rawDelay > 0.F && rawDelay <= 1000.0F)
			{

				weightedAccumulation += weightedDelay;
				normalizer += weight;
				rawAccumulation += rawDelay;
				++countOfIncludedDelays;
			}

			weight *= weightGainMultiplier;
		}

		if (weightedAccumulation > 0 && normalizer > 0)
		{

			weightedAverageInterval = weightedAccumulation / normalizer;
			averageInterval = rawAccumulation / countOfIncludedDelays;

			// Don't want to show speed faster than requested!
			if (frameInterval > weightedAverageInterval)
			{
				weightedAverageInterval = frameInterval;
			}

			actualDisplaySpeed = 1000.0F / weightedAverageInterval;
			observedFrameInterval = weightedAverageInterval;
		}
		else
		{

			actualDisplaySpeed = 0.0F;
			observedFrameInterval = 10.0F;
		}

		TRACE_3(errout << "-{-{-{-{-{-{ FPS=" << actualDisplaySpeed << " FI=" << frameInterval << " Raw=" << averageInterval <<
				" Weighted=" << weightedAverageInterval << " }-}-}-}-}-}");
	}

	if (lastDisplayedFrameIndex == provisionalFrameIndex
	&& provisionalDisplayMode == DISPLAY_PROVISIONAL)
	{
		// QQQ why does this need to be locked?
		CAutoThreadLocker prvLock(*prvPixelRegionListLock);
		lastDisplayedFrameBuffer = provisionalFrameBuffer;
	}
	else
	{
		lastDisplayedFrameBuffer = displayRing[idisplayEmptyPtr].frameBuffer;
	}

	return lastDisplayedFrameBuffer;
}
//------------------------------------------------------------------------------

void CPlayer::setTrackBarUpdate(bool update)
{
	updateTrackBar = update;
}
//------------------------------------------------------------------------------

bool CPlayer::trackBarUpdateEnabled()
{
	return updateTrackBar;
}
//------------------------------------------------------------------------------

int CPlayer::getInFrameIndex()
{
	return srcMinFrame;
}
//------------------------------------------------------------------------------

int CPlayer::getOutFrameIndex()
{
	return srcMaxFrame;
}
//------------------------------------------------------------------------------

int CPlayer::getLastFrameIndex()
{
	return lastDisplayedFrameIndex;
}
//------------------------------------------------------------------------------

bool CPlayer::getLastFrameSync()
{
	return isLastFrameOfSequence;
}
//------------------------------------------------------------------------------

void CPlayer::enterTrackBarPos(int frmnum)
{
	if (srcVideoFrameList == nullptr)
	{
		return;
   }

   if (frmnum != lastDisplayedFrameIndex)
   {

      if (majorState == READER_ACTIVE && readMode != FOLLOW_TRACKBAR)
      {
         fullStop();
      }

      // increment fill pointer AFTER number has been stored
      jogFrameQueue[jogFrmQFillPtr % jogFrmQSize] = frmnum;
      ++jogFrmQFillPtr;

      // We will pop out of 'follow track bar' mode after some period
      // of inactivity
      followTrackBarCancelTimer.Start();

      // Kick the reader
      if (majorState != READER_ACTIVE)
      {

         readMode = FOLLOW_TRACKBAR;
         updateTrackBar = false;
         frameInterval = 0;
         averageReadTime = 0;
         interFrameDelayIndex = FOLLOW_TRACKBAR_RAMP;

         switchToActive();
      }
   }
}
//------------------------------------------------------------------------------

CTimecode CPlayer::getCurrentTimecode()
{
   if (lastTimecode == nullptr)
   {
      return CTimecode(0);
   }

   return *lastTimecode;
}
//------------------------------------------------------------------------------

CTimecode CPlayer::getInTimecode()
{
   if (srcMinTimecode == nullptr)
   {
      return CTimecode(0);
   }

   return *srcMinTimecode;
}
//------------------------------------------------------------------------------

CTimecode CPlayer::getOutTimecode()
{
   if (srcMaxTimecode == nullptr)
   {
      return CTimecode(0);
   }

   return *srcMaxTimecode;
}
//------------------------------------------------------------------------------

int CPlayer::getHalfFrame()
{
   return (lastHalfFrame);
}
//------------------------------------------------------------------------------

bool CPlayer::getLastTimecodeFieldFlag()
{
   return lastTimecodeFieldFlag;
}
//------------------------------------------------------------------------------

void CPlayer::displayFrameDone()
{
   // The main thing we do here is bump the "empty" pointer to make the
   // buffer corresponding to the old empty pointer available to the reader.
   // The lag is to protect the frame that we last displayed from being
   // overwritten to effectively create a one-frame cache.
   // If we are actively playing, there is no need to maintain the lag
   // pointer because there is no reason to cache the frame. In that case
   // keep it equal to the "empty" pointer to show it is not pointing at
   // anything useful

   if (majorState == READER_ACTIVE && (readMode == PLAY_CLIP_BWD || readMode == PLAY_CLIP_FWD || readMode == LOOP_CLIP_FWD))
   {

		idisplayLagPtr = ++idisplayEmptyPtr; // lag == empty
	}
   else
   {
      // Not playing - ok to lag
		idisplayLagPtr = idisplayEmptyPtr++; // lag == (empty - 1)
	}

	if (idisplayFillPtr < idisplayEmptyPtr)
	{
		idisplayFillPtr = idisplayEmptyPtr;
	}

	TRACE_3(errout << "nnnnnnnnnn Display Frame Done" << ", fill=" << idisplayFillPtr << ", empty=" << idisplayEmptyPtr << ", lag=" <<
         idisplayLagPtr << " nnnnnnnnnn");
}
//------------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
//
// button functions
//
// indicates whether subsequent calls to "getNextDisplayFrame"
// will find anything in the queue to be displayed. This is used
// by the mouse handler to avoid redrawing the frame itself.
//
bool CPlayer::isDisplaying()
{
	MTIassert(idisplayEmptyPtr <= idisplayFillPtr);
	bool temp = ((majorState == READER_ACTIVE) || (idisplayEmptyPtr < idisplayFillPtr));
	return temp;
}
//------------------------------------------------------------------------------

// this stops the player in its tracks
void CPlayer::fullStop()
{
   // tell the disk reader to stop reading and lock out other threads
	MTIassert(majorState != READER_LOCKED);
	switchToLocked();

   // purge the frames queue
   jogFrmQEmptyPtr = jogFrmQFillPtr;

   // stop the display ring ptrs so that no
   // overwrite of the last video buffer occurs
// QQQ	idisplayLagPtr = idisplayEmptyPtr = idisplayFillPtr = ilastBuffer + 1;
	idisplayLagPtr = idisplayEmptyPtr = idisplayFillPtr = idisplayFillPtr + 1;
	MTIassert(idisplayEmptyPtr <= idisplayFillPtr);
//	UVTRACE(errout << "############### 1 MOD FILL -> " << idisplayFillPtr);

   // free up all slots for use by DRS
   for (int i = 0; i < displayRing.size(); i++)
   {
		displayRing[i].iframeIndex = -4; // NO_FRAME;
		displayRing[i].frameBuffer = nullptr;
	}

   // ok - we're done dicking with the display buffers
	sendCommandToDiskReader(UNLOCK_READER);

   // reset the frame interval timer - we use it to pace the refresh timing
   interframeDelayTimer->Start();

   // the actual display speed drops to 0.
   actualDisplaySpeed = 0.;

   // This is the "unlimited" setting.
   observedFrameInterval = 10.0F;

   // not playing anymore
   setReallyPlaying(false);
}
//------------------------------------------------------------------------------

// go to an arbitrary frame and read from clip
void CPlayer::goToFrame(int frame)
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   if (blockCmdsExcStop)
   {
      return;
   }

   fullStop();

   if (frame < srcMinFrame)
   {
      frame = srcMinFrame;
   }
   else if (frame > srcMaxFrame)
   {
      frame = srcMaxFrame;
   }

   /*
    // Added a check in goToFrame to avoid seg faults JAM
    if (frame < srcMinFrame)
    return;
    if (frame > srcMaxFrame)
    return;
    */
   icurFrame = iendFrame = frame;
   MTIassert(icurFrame >= 0);
   if (icurFrame < 0)
   {
      static bool oneshot = false;
      if (!oneshot)
      {
         oneshot = true;
			_MTIErrorDialog("ERROR! icurframe = -1 [goToFrame] ");
      }
   }

   deltaFrame = 0;
   interFrameDelayIndex = NO_DELAY_RAMP;
   readMode = PLAY_CLIP_FWD;
   updateTrackBar = true;
   frameInterval = 0;
   averageReadTime = 0;

   switchToActive();
}
//------------------------------------------------------------------------------

bool CPlayer::overlayHistoryOnDisplayFrame(MediaFrameBufferSharedPtr &frameBuffer, int frameIndex)
{
   if (!frameBuffer)
   {
      return false;
   }

   MTI_UINT8 *stupidBuf[2] = { (MTI_UINT8 *)frameBuffer->getImageDataPtr(), nullptr };
	if (stupidBuf[0] == nullptr)
   {
      return false;
   }

   CImageFormat frameImageFormat = frameBuffer->getImageFormat();
   bool fixes = false;
	if ((playbackFilter & PLAYBACK_FILTER_ORIGINAL_VALUES) != 0)
	{
		// replace all original values before displaying the frame (!)
      int retVal;
      CAutoHistoryOpener opener(*reviewHistory, FLUSH_TYPE_RESTORE, frameIndex, ALL_FIELDS, 0, 0, 0, retVal);

		if (retVal == 0)
		{
         while (reviewHistory->nextRestoreRecord() == 0)
			{
            if (!reviewHistory->getCurrentRestoreRecordResFlag())
				{
					if (!fixes)
					{
						extractor->extractActivePicture(reviewBuf, stupidBuf, srcVideoImageFormat, &frameImageFormat);
                  fixes = true;
               }

               int retVal = reviewHistory->restoreCurrentRestoreRecordPixels(reviewBuf, false);
               if (retVal != 0)
               {
                  break;
               }
            }
         }
      }

      if (fixes)
      {
			extractor->replaceActivePicture(stupidBuf, (const CImageFormat *)srcVideoImageFormat, reviewBuf);
      }
   }
   //
   // ..fun's over!
   // -----------------------------------------------------------------------

   // We used to overlay the blue stuff here as well, but that got moved
   // to DisplayFrame

   return fixes;
}
#if 0
//------------------------------------------------------------------------------
bool CPlayer::hackQueryFrameCompareModeForDisplayFrameInPaint(bool &doSideBySide, MediaFrameBufferSharedPtr &wipeFrameBuffer)
{
	// This needs to do the same stuff that "oneFrameToWindow" does.
	// Called only when Paint tool is active.
	string leftClipName;
	string rightClipName;
	frameBuffer = nullptr;

	// Note: Paint doesn't use provisional.
	if (frameIndex == getProvisionalFrameIndex())
	{
		CAutoThreadLocker prvLock(*prvPixelRegionListLock);
		if (_frameCompareMode != FrameCompareMode::Off && srcVideoFrameList != nullptr)
		{
			 // Provisional frame gets compared to the CURRENT SOURCE clip!
			 srcVideoFrameList->readMediaFrameBuffer(frameIndex, wipeFrameBuffer);
		}

		if (wipeFrameBuffer)
		{
			if (_frameCompareMode == FrameCompareMode::SideBySide)
			{
				mainWindow->GetDisplayer()->displayFramesSideBySide(
													frameBuffer,
													wipeFrameBuffer,
													frameIndex,
													fast,
													prvPixelRegionList,
													recomputeImportDiffs);
			}
			else
			{
				mainWindow->GetDisplayer()->displayFrameWithWiper(
													frameBuffer,
													wipeFrameBuffer,
													frameIndex,
													fast,
													prvPixelRegionList,
													recomputeImportDiffs,
													_frameCompareWiperRelativeOrigin,
													_frameCompareWiperAngleInDegrees);
			}

			ClipIdentifier srcClipId(srcClip->getClipPathName());
			leftClipName = srcClipId.GetFullDisplayString();
			rightClipName = "PREVIEW";
		}
		else
		{
			mainWindow->GetDisplayer()->displayFrame(frameBuffer, frameIndex, fast, prvPixelRegionList, recomputeImportDiffs);
		}
	}
	else
	{
		CAutoThreadLocker prvLock(*prvPixelRegionListLock);
		MediaFrameBufferSharedPtr wipeFrameBuffer;
		if (_frameCompareMode != FrameCompareMode::Off && _frameCompareClipFrameList != nullptr)
		{
			 // Source frame gets compared to frame from PREVIOUS CLIP!
			 _frameCompareClipFrameList->readMediaFrameBuffer(frameIndex, wipeFrameBuffer);
		}

		if (wipeFrameBuffer)
		{
			if (_frameCompareMode == FrameCompareMode::SideBySide)
			{
				mainWindow->GetDisplayer()->displayFramesSideBySide(
													frameBuffer,
													wipeFrameBuffer,
													frameIndex,
													fast,
													prvPixelRegionList,
													recomputeImportDiffs);
			}
			else
			{
				mainWindow->GetDisplayer()->displayFrameWithWiper(
													frameBuffer,
													wipeFrameBuffer,
													frameIndex,
													fast,
													prvPixelRegionList,
													recomputeImportDiffs,
													_frameCompareWiperRelativeOrigin,
													_frameCompareWiperAngleInDegrees);
			}

			ClipIdentifier compareClipId(_frameCompareClip->getClipPathName());
			leftClipName = compareClipId.GetFullDisplayString();
			ClipIdentifier srcClipId(srcClip->getClipPathName());
			rightClipName = srcClipId.GetFullDisplayString();
		}
		else
		{
			mainWindow->GetDisplayer()->displayFrame(frameBuffer, frameIndex, fast, nullptr, recomputeImportDiffs);
      }

		if (frameBuffer && frameBuffer->getErrorInfo().code != 0)
		{
			mainWindow->ShowFrameBufferErrorMessage(frameBuffer->getErrorInfo());
		}
		else if (wipeFrameBuffer && wipeFrameBuffer->getErrorInfo().code != 0)
		{
			mainWindow->ShowFrameBufferErrorMessage(wipeFrameBuffer->getErrorInfo());
		}
		else
		{
			mainWindow->HideFrameBufferErrorMessage();
		}
	}

	mainWindow->SetFrameCompareClipNames(leftClipName, rightClipName);
}
#endif
//------------------------------------------------------------------------------

void CPlayer::oneFrameToWindow(MediaFrameBufferSharedPtr frameBuffer, int frameIndex, bool fast, bool recomputeImportDiffs)
{
	string leftClipName;
	string rightClipName;

	MTIassert(frameIndex >= 0);
	if (frameIndex == getProvisionalFrameIndex()
	|| (frameIndex == mainWindow->GetDisplayer()->getTargetFrameIndex()
				&& mainWindow->GetDisplayer()->arePaintToolChangesPending()))
	{
		CAutoThreadLocker prvLock(*prvPixelRegionListLock);
		MediaFrameBufferSharedPtr wipeFrameBuffer;
		if (_frameCompareMode != FrameCompareMode::Off && srcVideoFrameList != nullptr)
		{
			 // Provisional frame gets compared to the CURRENT SOURCE clip!
			 srcVideoFrameList->readMediaFrameBuffer(frameIndex, wipeFrameBuffer);
		}

		if (wipeFrameBuffer)
		{
			if (_frameCompareMode == FrameCompareMode::SideBySide)
			{
				mainWindow->GetDisplayer()->displayFramesSideBySide(
													frameBuffer,
													wipeFrameBuffer,
													frameIndex,
													fast,
													prvPixelRegionList,
													recomputeImportDiffs);
			}
			else
			{
				mainWindow->GetDisplayer()->displayFrameWithWiper(
													frameBuffer,
													wipeFrameBuffer,
													frameIndex,
													fast,
													prvPixelRegionList,
													recomputeImportDiffs,
													_frameCompareWiperRelativeOrigin,
													_frameCompareWiperAngleInDegrees);
			}

			ClipIdentifier srcClipId(srcClip->getClipPathName());
			leftClipName = srcClipId.GetFullDisplayString();
			rightClipName = "PREVIEW";
		}
		else
		{
			mainWindow->GetDisplayer()->displayFrameCalledFromPlayer(frameBuffer, frameIndex, fast, prvPixelRegionList, recomputeImportDiffs);
		}
	}
	else
	{
		CAutoThreadLocker prvLock(*prvPixelRegionListLock);
		MediaFrameBufferSharedPtr wipeFrameBuffer;
		if (_frameCompareMode != FrameCompareMode::Off && _frameCompareClipFrameList != nullptr)
		{
			 // Source frame gets compared to frame from PREVIOUS CLIP!
			 _frameCompareClipFrameList->readMediaFrameBuffer(frameIndex, wipeFrameBuffer);
		}

		if (wipeFrameBuffer)
		{
			if (_frameCompareMode == FrameCompareMode::SideBySide)
			{
				mainWindow->GetDisplayer()->displayFramesSideBySide(
													frameBuffer,
													wipeFrameBuffer,
													frameIndex,
													fast,
													prvPixelRegionList,
													recomputeImportDiffs);
			}
			else
			{
				mainWindow->GetDisplayer()->displayFrameWithWiper(
													frameBuffer,
													wipeFrameBuffer,
													frameIndex,
													fast,
													prvPixelRegionList,
													recomputeImportDiffs,
													_frameCompareWiperRelativeOrigin,
													_frameCompareWiperAngleInDegrees);
			}

			ClipIdentifier compareClipId(_frameCompareClip->getClipPathName());
			leftClipName = compareClipId.GetFullDisplayString();
			ClipIdentifier srcClipId(srcClip->getClipPathName());
			rightClipName = srcClipId.GetFullDisplayString();
		}
		else
		{
			mainWindow->GetDisplayer()->displayFrameCalledFromPlayer(frameBuffer, frameIndex, fast, nullptr, recomputeImportDiffs);
      }

		if (frameBuffer && frameBuffer->getErrorInfo().code != 0)
		{
			mainWindow->ShowFrameBufferErrorMessage(frameBuffer->getErrorInfo());
		}
		else if (wipeFrameBuffer && wipeFrameBuffer->getErrorInfo().code != 0)
		{
			mainWindow->ShowFrameBufferErrorMessage(wipeFrameBuffer->getErrorInfo());
		}
		else
		{
			mainWindow->HideFrameBufferErrorMessage();
		}
	}

	mainWindow->SetFrameCompareClipNames(leftClipName, rightClipName);
}
//------------------------------------------------------------------------------

// Go to an arbitrary frame, read from clip, and display.
// An argument for fast, subsampled display is available
void CPlayer::goToFrameSynchronous(int frame, bool fast)
{
	// CAREFUL!! If we are going to the current frame DO NOT SHORT CIRCUIT
	// because some calling code depends on the fact that this function
   // ALWAYS READS A FRESH COPY of the frame from the disk, even if it's
   // the current frame!!!!

   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   if (blockCmdsExcStop)
   {
      return;
   }

   // read a fresh copy of the desired frame from disk
   goToFrame(frame);

   // spin until the data pops thru the ring buffer
   // or a counter is exhausted. The latter should
   // never happen when there's a valid source clip
	CHRTimer loopTimer; // A much better way to time stuff
	loopTimer.Start();

	MediaFrameBufferSharedPtr frameBuffer = getNextDisplayFrame();
	while (!frameBuffer)
	{
      // It isn't going to work if the Disk Reader thread has ended
      if (majorState == READER_ENDED)
      {
         return;
      }

		// 20 SECONDS?? WTF ?? QQQ
      loopTimer.DelayNS(1); // Non-spinning 1 msec delay
		if (loopTimer.Read() > 10000)
		{
         // No frame showed up within 10 seconds
         TRACE_0(errout << "I/O ERROR: goToFrameSync: DISPLAY FRAME TIMED OUT");
         return;
		}

		frameBuffer = getNextDisplayFrame();
   }

	// frameBuffer is a real frame now
   // if the MONITOR is enabled, output the frame to it

   if (monitorDisplayIsEnabled())
	{
      // buffer index is ILASTBUFFER
      outputVideo();
   }

   // update the slider and post the timecode for the frame
   mainWindow->UpdateSliderAndTimecode();

   // if bgnd flag is cocked, display bgnd
   if (mainWindow->GetDisplayer()->getBgndFlag())
   {
      mainWindow->GetDisplayer()->setBgndFlag(false);
      mainWindow->GetDisplayer()->displayBackground();
   }

   // new way has "displayFrame draw the pixel region list into the back
   // buffer, so it doesn't appear until SwapBuffers is called. Flicker gone.
	oneFrameToWindow(frameBuffer, lastDisplayedFrameIndex, fast, true);

   // advance the ring buffer
	displayFrameDone();
}
//------------------------------------------------------------------------------

// this method tries to go to the frame without reading
// a fresh copy from the disk
void CPlayer::goToFrameSynchronousCached(int frame, bool fast)
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   if (blockCmdsExcStop)
   {
      return;
   }

   // sync with reader
	switchToLocked();

   // try to find the desired frame in the ring buffer
   bool foundDesiredFrame = false;
   int ptr;
	for (ptr = idisplayLagPtr; ptr < idisplayFillPtr; ++ptr)
   {
      if (displayRing[ptr].iframeIndex == frame)
      {
			foundDesiredFrame = true;
         break;
      }
   }

   if (foundDesiredFrame)
	{
		// the desired frame is just the last one
      // back up the empty pointer without fear
		// preserve a cache frame if there's a valid frame at *(ptr-1)
		idisplayEmptyPtr = ptr;
		idisplayFillPtr = idisplayEmptyPtr + 1;
//      UVTRACE(errout << "############### 2 MOD FILL -> " << idisplayFillPtr);
      // QQQ why not allow lag ptr to lag by more than 1??
      if (idisplayEmptyPtr > idisplayLagPtr)
		{
         idisplayLagPtr = idisplayEmptyPtr - 1;
      }

		MTIassert(idisplayEmptyPtr <= idisplayFillPtr);

		sendCommandToDiskReader(UNLOCK_READER);

//		unsigned char **buf;
//		buf = getNextDisplayFrame();
		MediaFrameBufferSharedPtr frameBuffer = getNextDisplayFrame();

      /////////// START WORKAROUND FOR A BUG ////////////////////////////
		// Must be a timing bug here... sometimes getNextDisplayFrame()
      // returns nullptr pointer!!!!
		MTIassert(frameBuffer);
		if (frameBuffer == nullptr)
      {
			// It isn't going to work if the Disk Reader thread has ended
         if (majorState == READER_ENDED)
         {
            return;
         }

         TRACE_0(errout << "INTERNAL ERROR: cached buffer disappeared!");
         // I hope this makes sense.... copied from below....  anyhow
         // it's better than JUST FRICKIN' CRASHING!!
         //
         // read a fresh copy of the frame from disk and display it
         goToFrameSynchronous(frame, fast);
         return;
      }
      /////////// END WORKAROUND FOR A BUG //////////////////////////////

      // if the MONITOR is enabled, output the frame to it
      if (monitorDisplayIsEnabled())
      {

         // buffer index is ILASTBUFFER
         outputVideo();
      }

      int frameIndex = getLastFrameIndex();

      // update the slider and update the timecode to this frame
      mainWindow->UpdateSliderAndTimecode();

      // if bgnd flag is cocked, display bgnd
      if (mainWindow->GetDisplayer()->getBgndFlag())
      {
         mainWindow->GetDisplayer()->setBgndFlag(false);
         mainWindow->GetDisplayer()->displayBackground();
      }

      // new way has "displayFrame draw the pixel region list into the back
      // buffer, so it doesn't appear until SwapBuffers is called. Flicker gone.
		oneFrameToWindow(frameBuffer, lastDisplayedFrameIndex, fast, true);

      // advance the ring buffer
		displayFrameDone();

   }
   else
   { // the desired frame is not in the ring buffer

      // release the reader
		sendCommandToDiskReader(UNLOCK_READER);

      // read a fresh copy of the frame from disk and display it
      goToFrameSynchronous(frame, fast);
   }
}
//------------------------------------------------------------------------------

// go to an arbitrary timecode and read from clip
void CPlayer::goToFrame(const CTimecode& timcod)
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   if (blockCmdsExcStop)
   {
      return;
   }

   fullStop();

	int dstfrm = srcVideoFrameList->getFrameIndex(timcod);
   if (dstfrm != -1)
   {
      goToFrame(dstfrm);
   }
}
//------------------------------------------------------------------------------

// go to an arbitrary timecode and read from clip
void CPlayer::goToFrameSynchronous(const CTimecode& timcod)
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   fullStop();

	int dstfrm = srcVideoFrameList->getFrameIndex(timcod);
   if (dstfrm != -1)
   {
      goToFrameSynchronous(dstfrm);
   }
}
//------------------------------------------------------------------------------

// refresh the last frame. Modified 11/04/01
// to induce correct state for use of buffers
// for fix
// TTT review for poss simplification
void CPlayer::refreshFrame()
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   // if called too early, abort
   if (lastDisplayedFrameIndex == -1)
   {
      return;
   }

   // the last frame may only be represented
   // once in the ring buffer after this call
   if (majorState == READER_IDLE)
   {
		/* this code used the lag ptr to avoid going to the disk -- but
       but we have to go to the disk on some display mode changes */

      // if the lag ptr -> desired frame
      if (displayRing[idisplayLagPtr].iframeIndex == lastDisplayedFrameIndex)
      {
         // if desired frame was just displayed
			if (idisplayEmptyPtr == (idisplayLagPtr + 1))
			{
            // the desired frame is just the last one
            // back up the empty pointer without fear
            //
            // WTF how come goToFrameSynchronousCached moves the fill ptr
            // but we don't move it here?? QQQ
            //
            // UPDATE!!! I'm going to try resetting the fill ptr because
            // I see a bug when refreshing the frame while diddlng the
            // the LUT settings because IsDisplaying() returns TRUE and
            // the reader is just sitting there IDLE but the empty ptr
            // is one less than the fill ptr. Let's hope for the best!!!!!!
            idisplayFillPtr = idisplayEmptyPtr;
//            UVTRACE(errout << "############### 3 MOD FILL -> " << idisplayFillPtr);
            //////
				idisplayEmptyPtr = idisplayLagPtr;
				MTIassert(idisplayEmptyPtr <= idisplayFillPtr);
			}
		}
		else
		{
         // short out ring buffer
			idisplayLagPtr = idisplayEmptyPtr = idisplayFillPtr;

         // free up all slots and pump last
         // frame through the ring buffer
			for (int i = 0; i < displayRing.size(); i++)
         {
				displayRing[i].iframeIndex = -5; // NO_FRAME;
				displayRing[i].frameBuffer = nullptr;
			}

         goToFrame(lastDisplayedFrameIndex);
      }
   }
}
//------------------------------------------------------------------------------

// synchronous refresh of fresh copy of last frame
void CPlayer::refreshFrameSynchronous(bool fast)
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   // if called too early, abort
   if (lastDisplayedFrameIndex == -1)
   {
      return;
   }

   goToFrameSynchronous(lastDisplayedFrameIndex, fast);
}
//------------------------------------------------------------------------------

// synchronous refresh of last frame, cached if available
void CPlayer::refreshFrameSynchronousCached(bool fast)
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   // if called too early, abort
   if (lastDisplayedFrameIndex == -1)
   {
      return;
   }

   goToFrameSynchronousCached(lastDisplayedFrameIndex, fast);
}
//------------------------------------------------------------------------------

// refresh for PANning, from ring buffer when available
void CPlayer::panFrameSynchronous()
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   goToFrameSynchronousCached(lastDisplayedFrameIndex, subsampledPanning);
}
//------------------------------------------------------------------------------

// stop and synchronize display
void CPlayer::stopAndSynchronize()
{
   MTIassert(majorState != READER_LOCKED);
   if (majorState == READER_LOCKED)
   {

      return;
   }

   // the flag is cleared
   blockCmdsExcStop = false;

   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   // when display is to remote video only,
   // or if the subsampled display is enabled,
   // synchronize the computer display with
   // a full-sampled display
   if ((displayMode == DISPLAY_TO_MONITOR) || (subsampledPlayback || subsampledPanning))
   {
      // always goes to computer display
      // mbraca changed this to use the cached frame if available
      refreshFrameSynchronousCached();
   }
   else
   {

      fullStop();
   }

   setReallyPlaying(false);

   // notify the tool so any bgnd
   // threads there can be stopped
   CToolManager toolManager;
   toolManager.onPlayerStop();
}
//------------------------------------------------------------------------------

void CPlayer::stopJogAndSynchronize()
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   // If the jog is getting canceled before it got a chance to do anything
   // (i.e. during the initial 'ramp-up' period) just make it jog
   // exactly once and we're done
   if (lastDisplayedFrameIndex == idwnFrame)
   { // no frames

      // mbraca changed this to try to use a cached frame because we've
      // probably already kicked off a read for this frame
      goToFrameSynchronousCached(lastDisplayedFrameIndex + idelFrame);
   }
   else
   {

      // mbraca changed this to match stopAndSynchronize() because we were
      // always getting a double read for no good reason of the last frame
      // jogged to (makes back-and-forth jogging less responsive at 4K)
      if ((displayMode == DISPLAY_TO_MONITOR) || (subsampledPlayback || subsampledPanning))
      {
         // mbraca changed this to use the cached frame if available
         refreshFrameSynchronousCached();
      }
      else
      {
         fullStop();
      }
   }
}
//------------------------------------------------------------------------------

// go to the IN frame & read from clip
void CPlayer::goToIn()
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   if (blockCmdsExcStop)
   {
      return;
   }

   goToFrame(srcMinFrame);
}
//------------------------------------------------------------------------------

// go to the OUT frame & read from clip
void CPlayer::goToOut()
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   if (blockCmdsExcStop)
   {
      return;
   }

   goToFrame(srcMaxFrame);
}
//------------------------------------------------------------------------------

void CPlayer::stopOrStart()
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   if (blockCmdsExcStop)
   {
      return;
   }

   // if already playing - go fast forward
   if (majorState != READER_ACTIVE)
   {
      playClipFwd();
   }
   else
   {
      stopAndSynchronize();
   }
}
//------------------------------------------------------------------------------

// play clip fwd from cur frame
void CPlayer::playClipFwd()
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   if (blockCmdsExcStop)
   {
      return;
   }

   // if already playing - go fast forward
   if (majorState == READER_ACTIVE && readMode == PLAY_CLIP_FWD)
   {
      if (deltaFrame < MEDIUM_DELTA_FRAME)
      {
         playClipFastFwd(MEDIUM_DELTA_FRAME);
      }
      else if (deltaFrame < FAST_DELTA_FRAME)
      {
         playClipFastFwd(FAST_DELTA_FRAME);
      }
   }
   else if (readMode == PLAY_CLIP_BWD && deltaFrame > SLOW_DELTA_FRAME && lastDisplayedFrameIndex > srcMinFrame)
   {

      if (deltaFrame >= FAST_DELTA_FRAME)
      {
         playClipFastBwd(MEDIUM_DELTA_FRAME);
      }
      else
      {
         playClipFastBwd(SLOW_DELTA_FRAME); // !!
      }
   }
   else if (lastDisplayedFrameIndex < srcMaxFrame)
   {

      // goToFrameSynchronous(lastDisplayedFrameIndex+1);

      if (lastDisplayedFrameIndex < srcMaxFrame)
      {

			if ((majorState != READER_ACTIVE) || (iendFrame != srcMaxFrame) || (deltaFrame != SLOW_DELTA_FRAME) ||
               (interFrameDelayIndex != NORMAL_PLAY_RAMP) || (readMode != PLAY_CLIP_FWD) || (updateTrackBar != true) ||
               (frameInterval != nominalFrameInterval))
         {

            fullStop();

            icurFrame = lastDisplayedFrameIndex + 1;
            iendFrame = srcMaxFrame;
            deltaFrame = SLOW_DELTA_FRAME;
            interFrameDelayIndex = NORMAL_PLAY_RAMP;
            readMode = PLAY_CLIP_FWD;
            updateTrackBar = true;
            frameInterval = nominalFrameInterval;
            averageReadTime = 0.0F;

            setReallyPlaying(true);

            switchToActive();
         }
      }
   }
}
//------------------------------------------------------------------------------

// play clip bwd from cur frame
void CPlayer::playClipBwd()
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   if (blockCmdsExcStop)
   {
      return;
   }

   // if already playing - go fast forward
   if (readMode == PLAY_CLIP_BWD)
   {

      if (deltaFrame < MEDIUM_DELTA_FRAME)
      {
         playClipFastBwd(MEDIUM_DELTA_FRAME);
      }
      else if (deltaFrame < FAST_DELTA_FRAME)
      {
         playClipFastBwd(FAST_DELTA_FRAME);
      }
   }
   else if (readMode == PLAY_CLIP_FWD && deltaFrame > SLOW_DELTA_FRAME && lastDisplayedFrameIndex < srcMaxFrame)
   {

      if (deltaFrame >= FAST_DELTA_FRAME)
      {
         playClipFastFwd(MEDIUM_DELTA_FRAME);
      }
      else
      {
         playClipFastFwd(SLOW_DELTA_FRAME); // !!
      }
   }
   else if (lastDisplayedFrameIndex > srcMinFrame)
   {

      // goToFrameSynchronous(lastDisplayedFrameIndex-1);

      if (lastDisplayedFrameIndex > srcMinFrame)
      {

         if ((majorState != READER_ACTIVE) || (ibegFrame != srcMinFrame) || (deltaFrame != SLOW_DELTA_FRAME) ||
               (interFrameDelayIndex != NORMAL_PLAY_RAMP) || (readMode != PLAY_CLIP_BWD) || (updateTrackBar != true) ||
               (frameInterval != nominalFrameInterval))
         {

            fullStop();

            icurFrame = lastDisplayedFrameIndex - 1;
            ibegFrame = srcMinFrame;
            deltaFrame = SLOW_DELTA_FRAME;
            interFrameDelayIndex = NORMAL_PLAY_RAMP;
            readMode = PLAY_CLIP_BWD;
            updateTrackBar = true;
            frameInterval = nominalFrameInterval;
            averageReadTime = 0.0F;

            setReallyPlaying(true);

            switchToActive();
         }
      }
   }
}
//------------------------------------------------------------------------------

// play clip fast fwd from cur frame
void CPlayer::playClipFastFwd(int newDeltaFrame)
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   if (blockCmdsExcStop)
   {
      return;
   }

   if (lastDisplayedFrameIndex < srcMaxFrame)
   {

      int desiredFrameInterval = (newDeltaFrame >= FAST_DELTA_FRAME) ? 0 : nominalFrameInterval;

		if ((majorState != READER_ACTIVE) || (iendFrame != srcMaxFrame) || (deltaFrame != newDeltaFrame) ||
            (interFrameDelayIndex != NORMAL_PLAY_RAMP) || (readMode != PLAY_CLIP_FWD) || (updateTrackBar != true) ||
            (frameInterval != desiredFrameInterval))
      {

         fullStop();

         icurFrame = lastDisplayedFrameIndex + 1;
         iendFrame = srcMaxFrame;
         deltaFrame = newDeltaFrame;
         interFrameDelayIndex = NORMAL_PLAY_RAMP;
         readMode = PLAY_CLIP_FWD;
         updateTrackBar = true;
         frameInterval = desiredFrameInterval;
         averageReadTime = 0.0F;

         setReallyPlaying(true);

         switchToActive();
      }
   }
}
//------------------------------------------------------------------------------

// play clip fast bwd from cur frame
void CPlayer::playClipFastBwd(int newDeltaFrame)
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   if (blockCmdsExcStop)
   {
      return;
   }

   if (lastDisplayedFrameIndex > srcMinFrame)
   {

      if ((majorState != READER_ACTIVE) || (ibegFrame != srcMinFrame) || (deltaFrame != newDeltaFrame) ||
            (interFrameDelayIndex != NORMAL_PLAY_RAMP) || (readMode != PLAY_CLIP_BWD) || (updateTrackBar != true) || (frameInterval != 0))
      {

         fullStop();

         icurFrame = lastDisplayedFrameIndex - 1;
         ibegFrame = srcMinFrame;
         deltaFrame = newDeltaFrame;
         interFrameDelayIndex = NORMAL_PLAY_RAMP;
         readMode = PLAY_CLIP_BWD;
         updateTrackBar = true;
         frameInterval = 0;
         averageReadTime = 0.0F;

         setReallyPlaying(true);

         switchToActive();
      }
   }
}
//------------------------------------------------------------------------------

// access deltaFrame setting
int CPlayer::getDeltaFrame()
{
   int retVal = deltaFrame;

   switch (readMode)
   {
   case PLAY_CLIP_BWD:
   case JOG_ONCE_BWD:
   case JOG_ADDITIONAL_BWD:
      retVal = -retVal;
      break;
   }

   return retVal;
}
//------------------------------------------------------------------------------

// jog clip one frame fwd
void CPlayer::jogClipOnceFwd()
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   if (blockCmdsExcStop)
   {
      return;
   }

   int frmnum = lastDisplayedFrameIndex + 1;
   if (frmnum <= srcMaxFrame)
   {
      goToFrameSynchronous(frmnum);
   }
}
//------------------------------------------------------------------------------

// jog clip one frame bwd
void CPlayer::jogClipOnceBwd()
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   if (blockCmdsExcStop)
   {
      return;
   }
   int frmnum = lastDisplayedFrameIndex - 1;
   if (frmnum >= srcMinFrame)
   {
      goToFrameSynchronous(frmnum);
   }
}
//------------------------------------------------------------------------------

// jog clip fwd with nice timing rampup
void CPlayer::jogClipFastFwd()
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   if (blockCmdsExcStop)
   {
      return;
   }

   if ((majorState == READER_ACTIVE) && (readMode == JOG_ADDITIONAL_FWD))
   {
      return;
   }

   switchToIdle();
   idisplayLagPtr = idisplayEmptyPtr = idisplayFillPtr;

   idwnFrame = lastDisplayedFrameIndex; // KKKKK
   idelFrame = SLOW_DELTA_FRAME;

   int frmnum = lastDisplayedFrameIndex + 1;
   if (jogFrmQEmptyPtr < jogFrmQFillPtr)
   {
      frmnum = jogFrameQueue[(jogFrmQFillPtr - 1) % jogFrmQSize] + 1;
   }

   if (frmnum <= srcMaxFrame)
   {

      jogFrameQueue[jogFrmQFillPtr++ % jogFrmQSize] = frmnum;
      readMode = JOG_ONCE_FWD;
      updateTrackBar = true;
      // frameInterval = 0; changed 11/24/04
      frameInterval = nominalFrameInterval;
      averageReadTime = 0.0F;

		iendFrame = srcMaxFrame;
      deltaFrame = SLOW_DELTA_FRAME;
      interFrameDelayIndex = FAST_JOG_RAMP;

      switchToActive();
   }
}
//------------------------------------------------------------------------------

void CPlayer::jogClipFastBwd()
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   if (blockCmdsExcStop)
   {
      return;
   }

   if ((majorState == READER_ACTIVE) && (readMode == JOG_ADDITIONAL_BWD))
   {
      return;
   }

   switchToIdle();
	idisplayLagPtr = idisplayEmptyPtr = idisplayFillPtr;

   idwnFrame = lastDisplayedFrameIndex; // KKKKK
   idelFrame = -SLOW_DELTA_FRAME;

   int frmnum = lastDisplayedFrameIndex - 1;
   if (jogFrmQEmptyPtr < jogFrmQFillPtr)
   {
      frmnum = jogFrameQueue[(jogFrmQFillPtr - 1) % jogFrmQSize] - 1;
   }

   if (frmnum >= srcMinFrame)
   {

      jogFrameQueue[jogFrmQFillPtr++ % jogFrmQSize] = frmnum;
      readMode = JOG_ONCE_BWD;
      updateTrackBar = true;
      // frameInterval = 0; changed 11/24/04
      frameInterval = nominalFrameInterval;
      averageReadTime = 0.0F;

		iendFrame = srcMinFrame;
      deltaFrame = SLOW_DELTA_FRAME;
      interFrameDelayIndex = FAST_JOG_RAMP;

      switchToActive();
   }
}
//------------------------------------------------------------------------------

// jog clip continuously fwd, with OUT
// as a hard stop
void CPlayer::jogClipSlowFwd()
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   if (blockCmdsExcStop)
   {
      return;
   }

   if ((majorState == READER_ACTIVE) && (readMode == JOG_ADDITIONAL_FWD))
   {
      return;
   }

   switchToIdle();
	idisplayLagPtr = idisplayEmptyPtr = idisplayFillPtr;

   idwnFrame = lastDisplayedFrameIndex; // KKKKK
   idelFrame = SLOW_DELTA_FRAME;

   int frmnum = lastDisplayedFrameIndex + 1;
   if (jogFrmQEmptyPtr < jogFrmQFillPtr)
   {
      frmnum = jogFrameQueue[(jogFrmQFillPtr - 1) % jogFrmQSize] + 1;
   }

   if (frmnum <= srcMaxFrame)
   {

      jogFrameQueue[jogFrmQFillPtr % jogFrmQSize] = frmnum;
      jogFrmQFillPtr++;
      readMode = JOG_ONCE_FWD;
      updateTrackBar = true;
      frameInterval = 0;
      averageReadTime = 0.0F;

		iendFrame = srcMaxFrame;
      deltaFrame = SLOW_DELTA_FRAME;
      interFrameDelayIndex = SLOW_JOG_RAMP;

      switchToActive();
   }
}
//------------------------------------------------------------------------------

// jog clip continuously bwd, with IN
// as a hard stop
void CPlayer::jogClipSlowBwd()
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   if (blockCmdsExcStop)
   {
      return;
   }

   if ((majorState == READER_ACTIVE) && (readMode == JOG_ADDITIONAL_BWD))
   {
      return;
   }

   switchToIdle();
   idisplayLagPtr = idisplayEmptyPtr = idisplayFillPtr;

   idwnFrame = lastDisplayedFrameIndex; // KKKKK
   idelFrame = -SLOW_DELTA_FRAME;

   int frmnum = lastDisplayedFrameIndex - 1;
   if (jogFrmQEmptyPtr < jogFrmQFillPtr)
   {
      frmnum = jogFrameQueue[(jogFrmQFillPtr - 1) % jogFrmQSize] - 1;
   }

   if (frmnum >= srcMinFrame)
   {

      jogFrameQueue[jogFrmQFillPtr++ % jogFrmQSize] = frmnum;
      readMode = JOG_ONCE_BWD;
      updateTrackBar = true;
      frameInterval = 0;
      averageReadTime = 0.0F;

		iendFrame = srcMinFrame;
      deltaFrame = SLOW_DELTA_FRAME;
      interFrameDelayIndex = SLOW_JOG_RAMP;

      switchToActive();
   }
}
//------------------------------------------------------------------------------

// jog clip continuously fwd, with OUT
// as a hard stop
void CPlayer::jogClipFwdOneThirdSecond()
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   if (blockCmdsExcStop)
   {
      return;
   }

   if ((majorState == READER_ACTIVE) && (readMode == JOG_ADDITIONAL_FWD))
   {
      return;
   }

   switchToIdle();
   idisplayLagPtr = idisplayEmptyPtr = idisplayFillPtr;

   int frmnum = lastDisplayedFrameIndex + deltaOneThird;
   if (jogFrmQEmptyPtr < jogFrmQFillPtr)
   {
      frmnum = jogFrameQueue[(jogFrmQFillPtr - 1) % jogFrmQSize] + deltaOneThird;
   }
   if (frmnum > srcMaxFrame)
   {
      frmnum = srcMaxFrame;
   }

   if (frmnum <= srcMaxFrame)
   {

      jogFrameQueue[jogFrmQFillPtr++ % jogFrmQSize] = frmnum;
      readMode = JOG_ONCE_FWD;
      updateTrackBar = true;
      frameInterval = 0;
      averageReadTime = 0.0F;

		iendFrame = srcMaxFrame;
      deltaFrame = deltaOneThird;
      interFrameDelayIndex = SLOW_JOG_RAMP;

      switchToActive();
   }
}
//------------------------------------------------------------------------------

// jog clip continuously bwd, with IN
// as a hard stop
void CPlayer::jogClipBwdOneThirdSecond()
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   if (blockCmdsExcStop)
   {
      return;
   }

   if ((majorState == READER_ACTIVE) && (readMode == JOG_ADDITIONAL_BWD))
   {
      return;
   }

   switchToIdle();
	idisplayLagPtr = idisplayEmptyPtr = idisplayFillPtr;

   int frmnum = lastDisplayedFrameIndex - deltaOneThird;
   if (jogFrmQEmptyPtr < jogFrmQFillPtr)
   {
      frmnum = jogFrameQueue[(jogFrmQFillPtr - 1) % jogFrmQSize] - deltaOneThird;
   }
   if (frmnum < srcMinFrame)
   {
      frmnum = srcMinFrame;
   }

   if (frmnum >= srcMinFrame)
   {

      jogFrameQueue[jogFrmQFillPtr++ % jogFrmQSize] = frmnum;
      readMode = JOG_ONCE_BWD;
      updateTrackBar = true;
      frameInterval = 0;
      averageReadTime = 0.0F;

		iendFrame = srcMinFrame;
      deltaFrame = deltaOneThird;
      interFrameDelayIndex = SLOW_JOG_RAMP;

      switchToActive();
   }
}
//------------------------------------------------------------------------------

// play entire clip fwd from IN to OUT
void CPlayer::playClipInOut()
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   if (blockCmdsExcStop)
   {
      return;
   }

   fullStop();

   icurFrame = srcMinFrame;
   ibegFrame = srcMinFrame;
	iendFrame = srcMaxFrame;
   deltaFrame = SLOW_DELTA_FRAME;
   interFrameDelayIndex = NORMAL_PLAY_RAMP;
   readMode = PLAY_CLIP_FWD;
   updateTrackBar = true;
   frameInterval = nominalFrameInterval;
   averageReadTime = 0.0F;

   setReallyPlaying(true);

   switchToActive();
}
//------------------------------------------------------------------------------

// loop clip fwd continuously betw IN and OUT
void CPlayer::loopClipInOut()
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   fullStop();

   icurFrame = srcMinFrame;
   ibegFrame = srcMinFrame;
	iendFrame = srcMaxFrame;
   deltaFrame = SLOW_DELTA_FRAME;
   interFrameDelayIndex = NORMAL_PLAY_RAMP;
   readMode = LOOP_CLIP_FWD;
   updateTrackBar = true;
   frameInterval = nominalFrameInterval;
   averageReadTime = 0.0F;

   setReallyPlaying(true);

   switchToActive();
}
//------------------------------------------------------------------------------

// set the Min Mark, subject to clip in/out limits (internal method)
int CPlayer::setMinMarkInternal(int frame)
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return (-1);
   }

   // special case.... "-1" is OK, means "mark not set"
   if (frame != MARK_NOT_SET)
   {

      // silently limit the mark to the clip boundaries
      if (frame < srcMinFrame)
      {
         frame = srcMinFrame;
      }
      if (frame > srcMaxFrame)
      { // max is "out" frame - 1
         frame = srcMaxFrame;
      }

      // if we're at or past the max mark, move the max mark
      // GAACK - can't do this because marks are overloaded to
      // mean "source" and "target" for paint!!! THAT SUCKS!
      // if (frame >= srcMaxMark)
      // srcMaxMark = frame + 1;
   }

   // set the mark and tell the world
   if (srcMinMark != frame)
   {
      srcMinMark = frame;
      MarksHaveChanged.Notify();

      // CToolManager toolMgr;
      // toolMgr.onNewMarks();
   }

   return 0;
}
//------------------------------------------------------------------------------

// set the Max Mark, subject to clip in/out limits (internal method)
int CPlayer::setMaxMarkInternal(int frame)
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return (-1);
   }

   // special case.... "-1" is OK, means "mark not set"
   if (frame != MARK_NOT_SET)
   {

      // silently limit the mark to the clip boundaries
      if (frame < srcMinFrame /* + 1 */)
      { // can't do + 1, see GAACK below
         frame = srcMinFrame /* + 1 */ ;
      }
      if (frame > srcMaxFrame + 1)
      { // max is "out" frame - 1
         frame = srcMaxFrame + 1;
      }

      // if we're at or before the min mark, move the min mark
      // GAACK - can't do this because marks are overloaded to
      // mean "source" and "target" for paint!!! THAT SUCKS!
      // if (frame <= srcMinMark)
      // srcMinMark = frame - 1;
   }

   // set the mark and tell the world
   if (srcMaxMark != frame)
   {
      srcMaxMark = frame;
      MarksHaveChanged.Notify();

      // CToolManager toolMgr;
      // toolMgr.onNewMarks();
   }

   return 0;
}
//------------------------------------------------------------------------------

// when setting mark to "current" frame, make sure we're at it!
void CPlayer::stabilizeDisplay()
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   // if there are queued frames then the position
   // of the slider and the vble ILASTFRAME won't
   // necessarily agree. For this reason we leapfrog
   // the entries in the queue and synchronize the
   // display before setting the mark.
   if (jogFrmQEmptyPtr < jogFrmQFillPtr)
   {

      // get the last queued frame number...
      int frmnum = jogFrameQueue[(jogFrmQFillPtr - 1) % jogFrmQSize];

      // and go to it
      goToFrameSynchronous(frmnum);

   }
}
//------------------------------------------------------------------------------

// position the '[' mark at the current frame
int CPlayer::setMarkIn()
{
   stabilizeDisplay();

   return (setMinMarkInternal(lastDisplayedFrameIndex));
}
//------------------------------------------------------------------------------

// position the '[' mark at an arbitrary frame
// -- fails if frame index out of range
int CPlayer::setMarkIn(int frame)
{
   // we don't let you set the mark to an arbitrary position during playback
   if (majorState == READER_ACTIVE)
   {
      // This is stupid - just stop the player!
      //return (0);
      fullStop();
   }

   return (setMinMarkInternal(frame));
}
//------------------------------------------------------------------------------

// position the '[' mark at an arbitrary timecode
// -- fails if timecode out of range
int CPlayer::setMarkInTimecode(const CTimecode& timcod)
{
   // we don't let you set the mark to an arbitrary position during playback
   if (majorState == READER_ACTIVE)
   {
      // This is stupid - just stop the player!
      //return (0);
      fullStop();
   }

	return (setMinMarkInternal(srcVideoFrameList->getFrameIndex(timcod)));
}
//------------------------------------------------------------------------------

// position the ']' mark to include current frame
int CPlayer::setMarkOutInclusive()
{
   stabilizeDisplay();

   return (setMaxMarkInternal(lastDisplayedFrameIndex + 1));
}
//------------------------------------------------------------------------------

// position the ']' mark to exclude current frame
int CPlayer::setMarkOutExclusive()
{
   stabilizeDisplay();

   return (setMaxMarkInternal(lastDisplayedFrameIndex));
}
//------------------------------------------------------------------------------

// position the ']' mark at an arbitrary frame
// -- fails if frame index out of range
int CPlayer::setMarkOut(int frame)
{
   // we don't let you set the mark to an arbitrary position during playback
   if (majorState == READER_ACTIVE)
   {
      // This is stupid - just stop the player!
      //return (0);
      fullStop();
   }

   return (setMaxMarkInternal(frame));
}
//------------------------------------------------------------------------------

// position the ']' mark at an arbitrary timecode
// -- fails if timecode out of range
int CPlayer::setMarkOutTimecode(const CTimecode& timcod)
{
   // we don't let you set the mark to an arbitrary position during playback
   if (majorState == READER_ACTIVE)
   {
      // This is stupid - just stop the player!
      //return (0);
      fullStop();
   }

   return (setMaxMarkInternal(srcVideoFrameList->getFrameIndex(timcod)));
}
//------------------------------------------------------------------------------

int CPlayer::setMarkAll()
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return (-1);
   }

   // we don't let you set the marks to specific positions during playback
   if (majorState == READER_ACTIVE)
   {
      // This is stupid - just stop the player!
      //return (0);
      fullStop();
   }

   // set marks to include the entire clip
   srcMinMark = srcMinFrame;
   srcMaxMark = srcMaxFrame + 1;

   MarksHaveChanged.Notify();

   // CToolManager toolMgr;
   // toolMgr.onNewMarks();

   return (0);
}
//------------------------------------------------------------------------------

// accessors for '[' and ']'
int CPlayer::getMarkIn()
{
   return (srcMinMark);
}
//------------------------------------------------------------------------------

CTimecode CPlayer::getMarkInTimecode()
{
   CTimecode theTimecode(CTimecode::NOT_SET);

   if (srcMinMark != MARK_NOT_SET && srcVideoFrameList != nullptr)
   {
      CVideoFrame *theFramePtr = srcVideoFrameList->getFrame(srcMinMark);
      if (theFramePtr != nullptr)
      {
         theFramePtr->getTimecode(theTimecode);
      }
   }

   return (theTimecode);
}
//------------------------------------------------------------------------------

int CPlayer::getMarkOut()
{
   return (srcMaxMark);
}
//------------------------------------------------------------------------------

// remove the '[' mark
int CPlayer::removeMarkIn()
{
   // we don't let you remove the mark during playback
   if (majorState == READER_ACTIVE)
   {
      // This is stupid - just stop the player!
      //return (0);
      fullStop();
   }

   return (setMinMarkInternal(MARK_NOT_SET)); // -1
}
//------------------------------------------------------------------------------

CTimecode CPlayer::getMarkOutTimecode()
{
   CTimecode theTimecode(CTimecode::NOT_SET);

   if (srcMaxMark != MARK_NOT_SET && srcMaxMark != 0 && srcVideoFrameList != nullptr)
   {

      // the value of srcMaxMark may now exceed that of srcMaxFrame (by 1);
      // also, it can no longer be < (srcMinFrame+1). So need to get the
      // timecode for the previous frame and then bump it
      CVideoFrame *theFramePtr = srcVideoFrameList->getFrame(srcMaxMark - 1);
      if (theFramePtr != nullptr)
      {
         theFramePtr->getTimecode(theTimecode);
         theTimecode++; // one more than one less is the one we want
      }
      else
      { // try the frame without the back and forward business
         theFramePtr = srcVideoFrameList->getFrame(srcMaxMark);
         if (theFramePtr != nullptr)
         {
            theFramePtr->getTimecode(theTimecode);
         }
      }
   }
   return (theTimecode);
}
//------------------------------------------------------------------------------

// remove the ']' mark
int CPlayer::removeMarkOut()
{
   // we don't let you remove the mark during playback
   if (majorState == READER_ACTIVE)
   {
      // This is stupid - just stop the player!
      //return (0);
      fullStop();
   }

   return (setMaxMarkInternal(MARK_NOT_SET)); // -1
}
//------------------------------------------------------------------------------

// go to the '[' mark
void CPlayer::goToMarkIn()
{
   if (srcMinMark != MARK_NOT_SET)
   {
      goToFrameSynchronous(srcMinMark);
   }
}
//------------------------------------------------------------------------------

// go to the ']' mark
void CPlayer::goToMarkOutMinusOne()
{
   if (srcMaxMark != MARK_NOT_SET)
   {
      goToFrameSynchronous(srcMaxMark - 1);
   }
}
//------------------------------------------------------------------------------

// play clip fwd from '[' mark to ']' mark
void CPlayer::playClipFwdBetweenMarks(bool block)
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   if (blockCmdsExcStop)
   {
      return;
   }

   // both marks must be set & in order
   if ((srcMinMark == MARK_NOT_SET) || (srcMaxMark == MARK_NOT_SET) || (srcMinMark >= srcMaxMark))
   {
      return;
   }

   // make sure you can run before
   // blocking or Player will hang!
   if (block)
   {
      blockCmdsExcStop = true;
   }

   fullStop();

   icurFrame = srcMinMark;
   ibegFrame = srcMinMark;
	iendFrame = srcMaxMark - 1;
   deltaFrame = SLOW_DELTA_FRAME;
   interFrameDelayIndex = NORMAL_PLAY_RAMP;
   readMode = PLAY_CLIP_FWD;
   updateTrackBar = true;
   frameInterval = nominalFrameInterval;
   averageReadTime = 0.0F;

   setReallyPlaying(true);

   switchToActive();
}
//------------------------------------------------------------------------------

void CPlayer::toolPlayClipFrameRangeInclusive(int firstFrameIndex, int lastFrameIndex)
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   if (blockCmdsExcStop)
   {
      return;
   }

   // both marks must be set & in order
   if (firstFrameIndex == MARK_NOT_SET || lastFrameIndex == MARK_NOT_SET)
   {
      return;
   }

   bool reverse = lastFrameIndex < firstFrameIndex;

   // make sure you can run before
   // blocking or Player will hang!
   blockCmdsExcStop = true;

   // important to keep "getNextFrame"
   // from returning provisional buf!
   clearProvisionalFrameIndex();

   fullStop();

   icurFrame = firstFrameIndex;
   ibegFrame = lastFrameIndex + (reverse ? 0 : 1);
	iendFrame = lastFrameIndex;
   deltaFrame = SLOW_DELTA_FRAME;
   interFrameDelayIndex = NORMAL_PLAY_RAMP;
   readMode = reverse ? PLAY_CLIP_BWD : PLAY_CLIP_FWD;
   updateTrackBar = true;
   frameInterval = nominalFrameInterval;
   averageReadTime = 0.0F;

   setReallyPlaying(true);

   switchToActive();
}
//------------------------------------------------------------------------------

// loop clip fwd continuously betw '[' and ']'
void CPlayer::loopClipFwdBetweenMarks()
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   if (blockCmdsExcStop)
   {
      return;
   }

   // both marks must be set & in order
   if ((srcMinMark == MARK_NOT_SET) || (srcMaxMark == MARK_NOT_SET) || (srcMinMark >= srcMaxMark))
   {
      return;
   }

   fullStop();

   icurFrame = srcMinMark;
   ibegFrame = srcMinMark;
	iendFrame = srcMaxMark - 1;
   deltaFrame = SLOW_DELTA_FRAME;
   interFrameDelayIndex = NORMAL_PLAY_RAMP;
   readMode = LOOP_CLIP_FWD;
   updateTrackBar = true;
   frameInterval = nominalFrameInterval;
   averageReadTime = 0.0F;

   setReallyPlaying(true);

   switchToActive();
}
//------------------------------------------------------------------------------

// loop N frames back to N frames forward
void CPlayer::loopClipFwd2NPlus1Frames(int N)
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return;
   }

   if (blockCmdsExcStop)
   {
      return;
   }

   ibegFrame = lastDisplayedFrameIndex - N;
   if (ibegFrame < srcMinFrame)
   {
      N += srcMinMark - ibegFrame;
      ibegFrame = srcMinFrame;
   }
	iendFrame = lastDisplayedFrameIndex + N;
   if (iendFrame > srcMaxFrame)
   {
      ibegFrame -= (iendFrame - srcMaxFrame);
      if (ibegFrame < srcMinFrame)
      {
         ibegFrame = srcMinFrame;
      }
		iendFrame = srcMaxFrame;
   }
   icurFrame = ibegFrame;

   fullStop();

   deltaFrame = SLOW_DELTA_FRAME;
   interFrameDelayIndex = NORMAL_PLAY_RAMP;
   readMode = LOOP_CLIP_FWD;
   updateTrackBar = true;
   frameInterval = nominalFrameInterval;
   averageReadTime = 0.0F;

   setReallyPlaying(true);

   switchToActive();
}
//------------------------------------------------------------------------------

bool CPlayer::isExpandedBetweenMarks()
{
   return (iLimStackPtr > 0);
}
//------------------------------------------------------------------------------

// '[' becomes IN, ']' becomes OUT
int CPlayer::expandBetweenMarks()
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return (-1);
   }

   if (blockCmdsExcStop)
   {
      return (-1);
   }

   // both marks must be set & in order
   // also, the limits stack must have room
   if ((srcMinMark == MARK_NOT_SET) || (srcMaxMark == MARK_NOT_SET) || (srcMinMark >= srcMaxMark) || (iLimStackPtr == iLimStackSiz))
   {
      return (-1);
   }

   // the old IN and OUT are pushed onto a stack
   savedLimits[iLimStackPtr].minFrame = srcMinFrame;
   savedLimits[iLimStackPtr].maxFrame = srcMaxFrame;
   srcMinFrame = srcMinMark;
   srcMaxFrame = srcMaxMark - 1;
   // srcMinMark  = MARK_NOT_SET;
   // srcMaxMark  = MARK_NOT_SET;
   iLimStackPtr++;

   // get timecode limits for display on timeline
   CVideoFrame *framePtr;

   framePtr = srcVideoFrameList->getFrame(srcMinFrame);
   if (framePtr == nullptr)
   {
      srcMinTimecode->setAbsoluteTime(0);
   }
   else
   {
      framePtr->getTimecode(*srcMinTimecode);
   }

   framePtr = srcVideoFrameList->getFrame(srcMaxFrame + 1);
   if (framePtr == nullptr)
   {
      srcMaxTimecode->setAbsoluteTime(0);
   }
   else
   {
      framePtr->getTimecode(*srcMaxTimecode);
   }

   // altered to leave current frame unchanged
   // whenever it is inside the marks. Sets it
   // to bot if cur frame below the mark; to top
   // if cur frame above the mark
   int targetFrame = lastDisplayedFrameIndex;
   if (icurFrame < srcMinMark)
   {
      targetFrame = srcMinMark;
   }
   if (icurFrame >= srcMaxMark)
   {
      targetFrame = srcMaxMark - 1;
   }
   goToFrame(targetFrame);

   return (0);
}
//------------------------------------------------------------------------------

int CPlayer::unexpandBetweenMarks()
{
   // if no clip is loaded, abort
   if (srcVideoFrameList == nullptr)
   {
      return (-1);
   }

   if (blockCmdsExcStop)
   {
      return (-1);
   }

   if (iLimStackPtr == 0)
   {
      return (-1);
   }

   iLimStackPtr--;
   srcMinMark = srcMinFrame;
   srcMaxMark = srcMaxFrame + 1;
   srcMinFrame = savedLimits[iLimStackPtr].minFrame;
   srcMaxFrame = savedLimits[iLimStackPtr].maxFrame;

   // get timecode limits for display on timeline
   CVideoFrame *framePtr;

   framePtr = srcVideoFrameList->getFrame(srcMinFrame);
   if (framePtr == nullptr)
   {
      srcMinTimecode->setAbsoluteTime(0);
   }
   else
   {
      framePtr->getTimecode(*srcMinTimecode);
   }

   framePtr = srcVideoFrameList->getFrame(srcMaxFrame + 1);
   if (framePtr == nullptr)
   {
      srcMaxTimecode->setAbsoluteTime(0);
   }
   else
   {
      framePtr->getTimecode(*srcMaxTimecode);
   }

   // altered to leave current frame unchanged
   goToFrame(lastDisplayedFrameIndex);

   return (0);
}
//------------------------------------------------------------------------------

void CPlayer::setSubsampledPlayback(bool fast)
{
   subsampledPlayback = fast;
}
//------------------------------------------------------------------------------

bool CPlayer::getSubsampledPlayback()
{
   return (subsampledPlayback);
}
//------------------------------------------------------------------------------

void CPlayer::setSubsampledPanning(bool fast)
{
   subsampledPanning = fast;
}
//------------------------------------------------------------------------------

bool CPlayer::getSubsampledPanning()
{
   return (subsampledPanning);
}
//------------------------------------------------------------------------------

void CPlayer::SetMainWindowPtr(TmainWindow *mainWin)
{
   mainWindow = mainWin;
}
//------------------------------------------------------------------------------

bool CPlayer::isIdle(void)
{
   return majorState == READER_IDLE;
}
//------------------------------------------------------------------------------

CSaveRestore *CPlayer::getReviewHistory()
{
   return reviewHistory;
}

// -------------------------------------------------------------------------
// This is run at extremely high priority... so keep it short!!!
// Called about once a millisecond
//
// DEBUG
static CHRTimer RefreshPostMessageTimer;
static double RefreshPostedMessageAtTime = 0.F;
static double RefreshReceivedMessageAtTime = 0.F;
static int NumberOfRefreshMessagesQueued = 0;
static int MainThreadStallCounter = 0;
static CHRTimer watchDogTimer;
struct foo { int interval; int proctime; int stallcount; bool posted; };
static foo footable[500] = {0, 0};
static int fooindex = 0;
static int foolastproctime = 0;
static bool fooposted = false;

//
void CPlayer::maybeInvokeMainWindowRefresh()
{
	if (mainWindow == nullptr)
	{
		return;
	}

	if (messagePostedSerialNumber > 1 && fooindex < 500 && majorState == READER_ACTIVE)
	{
		double interval = watchDogTimer.Read();
		watchDogTimer.Start();
		footable[fooindex++] = { (int)(interval + 0.5), foolastproctime, MainThreadStallCounter, fooposted };
		foolastproctime = 0;
		fooposted = false;
	}

	// Don't do anything if main window refresh is inhibited (for example
	// while we are switching display devices)
	if (inhibitMainWindowRefreshFlag)
	{
		activeDutyCycle = 0;
		return;
	}

	// 'active duty cycle' is a hack to enforce a duty cycle consisting
	// of at least one skipped iteration for every 10 calls so we let the
	// GUI in fairly often... the numbers are completely arbitrary...
	if (++activeDutyCycle >= 10)
	{
		activeDutyCycle = 0;
		return;
	}

	// Also skip a cycle if the main thread message handler callback has not
	// yet completed because we want to avoid stacking up "posted" messages
	if (messageReceivedSerialNumber != messagePostedSerialNumber)
	{
		if (++MainThreadStallCounter > 20)
		{
			foolastproctime = 0;
		}

		activeDutyCycle = 0;
		return;
	}

	MainThreadStallCounter = 0;
   bool doPostMessage = false;

	// timer units are msecs
	float timeNow = interframeDelayTimer->Read();
	if (nextFrameDisplayTime == 0.0F)
	{
		// First time through - want to show a frame immediately and sync.
		nextFrameDisplayTime = timeNow - 0.01F;
	}

	switch (majorState)
	{

	case READER_ACTIVE:

		// NOTE: previously we reset the interframeDelayTimer timer
		// every frame, but now for better accuracy we let it run
		// and compute the 'next display time' based on it

		// First make sure that the last message we posted actually
		// resulted in a frame refresh, otherwise we'll just try again now
		if (refreshDoneSerialNumber < refreshInvokedSerialNumber)
		{
			// if at first you don't succeed...
			doPostMessage = true;
		}

		// QQQ Why isn't there an "else" here?
		if (nextFrameDisplayTime <= timeNow)
		{
			// Time to display a new frame - advance the serial number
			refreshInvokedSerialNumber = refreshDoneSerialNumber + 1;
			doPostMessage = true;

			// In case we are jogging rather than playing, pick up the
			// frame delay from a table (when we are playing this will
			// yield 0, and we use the 'frame interval' instead)
			float jogDelay = (float) ramp[interFrameDelayIndex].delay;

			// Compute the next display refresh time
			// NOTE - frameInterval can be 0 when "playing" a single frame,
			// but in that case we really want a delay of 0
			float nextInterval;
			if (jogDelay < 0)
			{
				if (frameInterval == 0)
				{
					nextInterval = 0;
				}
				else
				{
					// averageReadTime was computed as a function of the
					// previous 15 observed frame read times.. we try to
					// match our display rate fairly closely to what is
					// actually being achieved because that will result
					// in noticeably smoother playback if we're not able
					// to run at the desired speed... however if the speed
					// is set to 'unlimited' (defined as 100 FPS), then we
					// do not fool around with any kind of smoothing.
					//
					// Also do not throttle back if the number of available
					// buffered frames is at or above one frame below
					// the low water mark.
					//
					// Also do not throttle back during the first second
					// of playing
					//
					// UPDATE: Because average read time is computed
					// incorrectly, we now use the observed frame rate as
					// the basis for smoothing.

					// Ugggh, these were stolen from DiskReader - QQQ
					int totalBufferCount = displayRing.size() - idisplayBuffLead;
					int bufferFullCount = idisplayFillPtr - idisplayLagPtr;
//                  int lowWaterMark = (totalBufferCount * 3) / 4;
					int lowWaterMark = totalBufferCount - 1;


					// Here 10 msecs (100 fps) represents the "unlimited" setting
					if (frameInterval <= 10.0F || bufferFullCount >= (lowWaterMark - 1) || timeNow < 1000.0)
					{
						nextInterval = frameInterval;
					}
					else
					{
						////nextInterval = averageReadTime;
						nextInterval = observedFrameInterval * 0.90F;

						// Sanity checks - don't go faster than the requested
						// speed, and don't try to go slower than 4 FPS
						if (frameInterval > nextInterval)
						{
							nextInterval = frameInterval;
						}
						else if (nextInterval > 250) // QQQ manifest constant
						{
							nextInterval = 250; // QQQ manifest constant
						}
					}
				}
			}
			else
			{
				nextInterval = jogDelay;
			}

//         TRACE_3(errout << "KKKKKKKK NFDT=" << nextFrameDisplayTime << ", TN=" << timeNow << ", NI=" << nextInterval);

			nextFrameDisplayTime += nextInterval;

			// Make sure the next display time is in the future;
			// we may be falling behind reading the frames;
			// Use + 2 instead of + 1 to allow other stuff to run
			if (nextFrameDisplayTime <= timeNow)
			{
				nextFrameDisplayTime = timeNow + 2;
			} // msec

			// Move to next value on the ramp - don't worry, these loop!
			interFrameDelayIndex = ramp[interFrameDelayIndex].nxt;
		}
		break;

	case READER_IDLE:

		// Reader is idle - need to crank the main window
		// refresh timer periodically anyhow - I think every
		// 100 msec should be sufficient!

		if (expediteMainWindowRefreshFlag || (timeNow > 100))
		{
			doPostMessage = true;
			expediteMainWindowRefreshFlag = false;

			// We have hijacked the inter-frame timer to do idle timing
			interframeDelayTimer->Start();
		}
		break;

	case READER_LOCKED:

		// Reader is LOCKED - which could mean that a tool has
		// hijacked the display frame queue! If that's the case we just
		// invoke the main window refresh timer at a faster pace than
		// if the reader is idle - every 20 msec

		if (timeNow > 20)
		{
			doPostMessage = true;

			// We have hijacked the inter-frame timer to do idle timing
			interframeDelayTimer->Start();
		}
		break;

	case READER_ENDED:
	default:
		// Uh, do nothing!
		break;
	}

	if (!doPostMessage)
	{
		foolastproctime = int(watchDogTimer.Read() + 0.5);
		return;
	}

	// We need to synchronize with the main window refresh timer so we
	// don't stack up posted messages
	++messagePostedSerialNumber;

	double refreshMessageTimeNow = RefreshPostMessageTimer.Read();
	int messagePostingInterval = refreshMessageTimeNow - RefreshPostedMessageAtTime;
	int queued = ++NumberOfRefreshMessagesQueued;
	RefreshPostedMessageAtTime = refreshMessageTimeNow;
//		UVTRACE(errout << "^v^v^v^v^v^v^v^v PM: POSTING MSG #" << messagePostedSerialNumber
//							<< ", queued=" << queued << ", interval=" << messagePostingInterval
//							<< " msec ^v^v^v^v^v^v^v^v");

	if (mainWindow != nullptr)
   {
	   PostMessage(mainWindow->Handle, CB_FIRE_REFRESH_TIMER, 0, 0);
	}

	foolastproctime = int(watchDogTimer.Read() + 0.5);
   fooposted = true;
}
// -------------------------------------------------------------------------

// This is how the main window refresh timer tells us it completed the
// processing triuggered by the last post message
void CPlayer::notifyPostMessageWasReceived()
{
	++messageReceivedSerialNumber;

	RefreshReceivedMessageAtTime = RefreshPostMessageTimer.Read();
//	UVTRACE(errout << "^v^v^v^v^v^v^v^v PM: RECEIVE MSG #" << messageReceivedSerialNumber
//						<< ", LAG=" << (RefreshReceivedMessageAtTime - RefreshPostedMessageAtTime)
//						<< " msec ^v^v^v^v^v^v^v^v");

   // This is a convenient place to check for expiration of the
   // 'follow trackbar cancel' timer ... sorry!
   if (majorState == READER_ACTIVE && readMode == FOLLOW_TRACKBAR && followTrackBarCancelTimer.Read() >= 500)
   {
		switchToIdle();
	}

//	if (fooindex == 500)
//	{
//		MTIostringstream os;
//		for (int i = 0; i < 500; ++i)
//		{
//			os << "FOO[" << i << "] = { " << footable[i].interval << ", " << footable[i].proctime
//							 << ", " << footable[i].stallcount << ", " << (footable[i].posted?"POSTED":"") << " }" << endl;
//		}
//
//      TRACE_3(errout << os.str());
//
//      fooindex = 0;
//	}
}
// -------------------------------------------------------------------------

// This is how the main window refresh timer tells us it that it actually
// performed a requested refresh
void CPlayer::notifyMainWindowRefreshDone()
{
	++refreshDoneSerialNumber;

//	double timeNow = RefreshPostMessageTimer.Read();
//	UVTRACE(errout << "^v^v^v^v^v^v^v^v MAIN WINDOW COMPLETED REFRESH #" << refreshDoneSerialNumber
//						<< ", interval = " << (timeNow - RefreshReceivedMessageAtTime)
//						<< " msec ^v^v^v^v^v^v^v^v");
}
// -------------------------------------------------------------------------

// This is how a tool tells us it wants the main window refresh timer to
// fire pronto - NOTE: only works when the reader is IDLE
void CPlayer::expediteMainWindowRefresh()
{
   expediteMainWindowRefreshFlag = true;
}
// -------------------------------------------------------------------------

bool CPlayer::isReallyPlaying()
{
   return reallyPlayingFlag;
}
// -------------------------------------------------------------------------

void CPlayer::setReallyPlaying(bool flag)
{
   if (flag)
   {
      CToolManager toolManager;
      toolManager.onPlayerStart();
   }

   reallyPlayingFlag = flag;
}
// -------------------------------------------------------------------------

int CPlayer::queueUpAnAsynchronousFrameRead(int trackerNumber, int frameNumber, int displayBufIndex)
{
   ASYNCH_READ_TRACKER &tracker = asynchReadTracker[trackerNumber];

   tracker.reset();
	tracker.state = ASYNC_READ_STATE_QUEUED;
	tracker.frameNumber = frameNumber;
	tracker.displayBufIndex = displayBufIndex;
	tracker.totalTimer.Start();

	UVTRACE(errout << ",AAAA " << setw(4*(tracker.frameNumber % 10)) << tracker.frameNumber << ": READER QUEUES FRAME ");

   readAheadWorkQueue.Add(trackerNumber);

   // Make sure the processing thread is running!
	if (_readAheadThreadParams.threadId == nullptr)
   {
		_readAheadThreadParams.player = this;
		_readAheadThreadParams.numberOfReadThreads = asyncReadThreadCount;
		_readAheadThreadParams.die = false;
		BThreadSpawn(startReadAheadThread, reinterpret_cast<void *>(&_readAheadThreadParams));
	}

   return 0;
}
// -------------------------------------------------------------------------

bool CPlayer::isAsynchronousFrameReadDone(int trackerNumber)
{
   bool retVal;
   ASYNCH_READ_TRACKER &tracker = asynchReadTracker[trackerNumber];

	retVal = tracker.isDone();

   return retVal;
}
// -------------------------------------------------------------------------

int CPlayer::finishAnAsynchronousFrameRead(int trackerNumber)
{
   ASYNCH_READ_TRACKER &tracker = asynchReadTracker[trackerNumber];
	static int oldFrameNumber = -1;    // DEBUG

	// DEBUG
	if (oldFrameNumber != tracker.frameNumber)
	{
		oldFrameNumber = tracker.frameNumber;
		UVTRACE(errout << "DISK READER WAITS FOR COMPLETION OF READ FOR " << tracker.frameNumber);
	}

   // Wait for up to one millisec for the reader thread to be done reading;
   // we don't want to hang here because we want to be able to deal with
	// the other readahead threads
	if (!tracker.isDone())
	{
		if (!tracker.completionEvent.wait(1))
		{

			////UVTRACE(errout << ",WWW2 " << setw(4*(tracker.frameNumber%10))
			////<< tracker.frameNumber << ": READER COMPLETION WAIT TIMED OUT");
			return ERR_ASYNC_WAIT_TIMED_OUT;
		}
	}

   // Read is complete! Clean up
	TRACE_3(errout << "$$$$$$$$$ Reading frame " << tracker.frameNumber << " took " << tracker.readTimeInMsec << " msec, total=" <<
         tracker.totalTimer.Read() << " msec $$$$$$$$$$");
	UVTRACE(errout << "DISK READER CONTINUES");

   return 0;
}
// -------------------------------------------------------------------------

/* static */
void CPlayer::startReadAheadThread(void *vpAppData, void *vpReserved)
{
	/* volatile */ READ_AHEAD_THREAD_PARAMS* params = reinterpret_cast< /* volatile */ READ_AHEAD_THREAD_PARAMS*>(vpAppData);
	CPlayer *_this = (CPlayer*)(params->player);

	// "reserved" parameter is the thread ID.
	params->threadId = vpReserved;

#ifndef NO_PRIORITY_BOOST
	BThreadSetPriority(params->threadId, THREAD_PRIORITY_ABOVE_NORMAL);
#endif

   int retVal = BThreadBegin(vpReserved);
	MTIassert(retVal == 0);

   _this->runReadAheadThread(*params);

}
// -------------------------------------------------------------------------

void CPlayer::runReadAheadThread(READ_AHEAD_THREAD_PARAMS &params)
{
	vector<ASYNC_WAIT_THREAD_PARAMS> readAheadWorkerThreadParams;
	EventSharedPtr readAheadWorkerStateTransitionEvent = std::make_shared<Event>();

	// QQQ TODO check if READ_AHEAD_TRACKER_COUNT is the right thing to use here
	for (int i = 0; i < READ_AHEAD_TRACKER_COUNT; ++i)
   {
		readAheadWorkerThreadParams.emplace_back();
		readAheadWorkerThreadParams[i].player = this;
		readAheadWorkerThreadParams[i].stateTransitionEvent = readAheadWorkerStateTransitionEvent;
	}

   while (!params.die)
   {
		// If the Q is empty, sleep until something is put in it.
      readAheadWorkQueue.Wait();

      int trackerNumber;
		try
      {
			trackerNumber = readAheadWorkQueue.Take();
      }
		catch (...)
      {
         trackerNumber = -1;
      }

      if (trackerNumber == -1)
      {
			continue;
      }

      int waitParamIndex = -1;
      do
		{
			// Reset the state transition event - we aren't interested in the past.
         // CAREFUL: need to do this BEFORE counting threads!
			readAheadWorkerStateTransitionEvent->reset();

			// Count the number of threads that are now reading. That's what
			// the numberOfReadThreads parameter refers to.
			// QQQ TODO check if READ_AHEAD_TRACKER_COUNT is the right thing to use here
			int readingCount = 0;
			int postProcessingCount = 0;
			for (int i = 0; i < READ_AHEAD_TRACKER_COUNT; ++i)
			{
				ASYNCH_READ_TRACKER &tracker = asynchReadTracker[i];
				if (tracker.isInUse())
				{
					readingCount += (tracker.isReading()) ? 1 : 0;
					postProcessingCount += (tracker.isPostprocessing()) ? 1 : 0;
				}
			}

			{
				CAutoSpinLocker lock(_AsyncWaitParamsThreadIdLock);

				void *threadWithEarliestDeadline = nullptr;
				int earliestDeadline = 0x7FFFFFFF;
				int earliestDeadlineFrame = -1;
				void *threadWithNextEarliestDeadline = nullptr;
				int nextEarliestDeadline = 0x7FFFFFFF;
				int nextEarliestDeadlineFrame = -1;


				for( const auto &workerParams : readAheadWorkerThreadParams )
				{
					if (workerParams.threadId == nullptr)
					{
						continue;
					}

					// We use the display buffer index as the deadline.
					ASYNCH_READ_TRACKER &tracker = asynchReadTracker[workerParams.trackerNumber];
					int deadline = tracker.displayBufIndex;
					if (deadline < earliestDeadline && tracker.isBeingWorkedOn())
					{
						threadWithEarliestDeadline = workerParams.threadId;
						earliestDeadline = deadline;
						earliestDeadlineFrame = tracker.frameNumber;
					}
					else if (deadline < nextEarliestDeadline && tracker.isBeingWorkedOn())
					{
						threadWithNextEarliestDeadline = workerParams.threadId;
						nextEarliestDeadline = deadline;
						nextEarliestDeadlineFrame = tracker.frameNumber;
					}
				}

				// Boost priorities for the two threads with the earliest deadlines.
				if (threadWithNextEarliestDeadline)
				{
#ifndef NO_PRIORITY_BOOST
					::BThreadSetPriority(threadWithNextEarliestDeadline, THREAD_PRIORITY_ABOVE_NORMAL);
					UVTRACE(errout << "^^^^^ RAT: SET ABOVE NORMAL PRIORITY FOR READ OF " << nextEarliestDeadlineFrame << " TO BUFINDEX " << nextEarliestDeadline << " ^^^^^");
#endif
				}

				if (threadWithEarliestDeadline)
				{
#ifndef NO_PRIORITY_BOOST
					::BThreadSetPriority(threadWithEarliestDeadline, THREAD_PRIORITY_HIGHEST);
					UVTRACE(errout << "^^^^^ RAT: SET HIGHEST PRIORITY FOR READ OF " << earliestDeadlineFrame << " TO BUFINDEX " << earliestDeadline << " ^^^^^");
#endif
				}
			}

			// DEBUG
			static int oldReadingCount = -1;
			static int oldPostProcessingCount = -1;
			if (readingCount != oldReadingCount || postProcessingCount != oldPostProcessingCount)
			{
				UVTRACE(errout << "oooooooo RAT: reading=" << readingCount << ", postProcessing=" << postProcessingCount << " oooooooo");
				oldReadingCount = readingCount;
				oldPostProcessingCount = postProcessingCount;
			}

			// If we are allowed to start a new read, find an unused parameter
			// struct, which is how we control the total number of threads
			// (reading + postprocessing)
			if (readingCount < params.numberOfReadThreads && postProcessingCount < (params.numberOfReadThreads * 2))
			{
				for (int i = 0; i < READ_AHEAD_TRACKER_COUNT; ++i)
				{
					if (readAheadWorkerThreadParams[i].threadId == nullptr)
					{
						waitParamIndex = i;
						break;
					}
				}
			}

			// QQQ FIX ME - we want to get notified each time a READING thread
			// transitions to POSTPROCESSING, not just when the thread completes.
			if (waitParamIndex == -1)
			{
				UVTRACE(errout << "oooooooo RAT: ALL " << (params.numberOfReadThreads * 3) << " RA trackers are in use!! oooooooo");
				bool signalled = readAheadWorkerStateTransitionEvent->wait(10000);
				UVTRACE(errout << "oooooooo RAT: Wait ended by " << (signalled ? "SIGNAL" : "TIMEOUT") << " oooooooo");
			}
		}
		while (waitParamIndex == -1);

		readAheadWorkerThreadParams[waitParamIndex].trackerNumber = trackerNumber;

		UVTRACE(errout << "oooooooo RAT: waitParamIndex=" << waitParamIndex << " gets trackerNumber=" << trackerNumber << " oooooooo");

		ASYNCH_READ_TRACKER *selectedTracker = &asynchReadTracker[trackerNumber];
		selectedTracker->errorCode = 0;
		selectedTracker->state = ASYNC_READ_STATE_READING;

		// Is this right? why not just leave it alone? QQQ
		////displayRing[selectedTracker->displayBufIndex].frameBuffer = nullptr;

		BThreadSpawn(startReadAheadWorkerThread, &readAheadWorkerThreadParams[waitParamIndex]);

		// Give the thread a running start.
		MTImillisleep(2);
	}

   // Signal main thread that we did indeed die
	params.die = false;
}
// -------------------------------------------------------------------------

/* static */
void CPlayer::startReadAheadWorkerThread(void *vpAppData, void *vpReserved)
{
	ASYNC_WAIT_THREAD_PARAMS* params = reinterpret_cast<ASYNC_WAIT_THREAD_PARAMS*>(vpAppData);
	CPlayer *_this = (CPlayer*)(params->player);

	// I don't need to lock threadIdLock here because the parent can't run.
	params->threadId = vpReserved;

	int retVal = BThreadBegin(vpReserved);
	MTIassert(retVal == 0);

	_this->runReadAheadWorkerThread(*params);

	{
		CAutoSpinLocker lock(_AsyncWaitParamsThreadIdLock);
		params->threadId = nullptr;
	}

	params->stateTransitionEvent->signal();
}
// -------------------------------------------------------------------------

// QQQ Deal with ERROR FRAMES
void CPlayer::runReadAheadWorkerThread(ASYNC_WAIT_THREAD_PARAMS &params)
{
	// Start up async read
	UVTRACE(errout << "((((___________________RA WORKER ENTERED_________________");

	int retVal;
	ASYNCH_READ_TRACKER *tracker = &asynchReadTracker[params.trackerNumber];
	tracker->readTimer.Start();
	CHRTimer localTimer;

	// Fetch the frame file synchronously from the disk.
	if (!tracker->cancel)
	{
		UVTRACE(errout << "___________________ RAW: START PREFETCH of " << tracker->frameNumber << " _________________");

		retVal = srcVideoFrameList->fetchMediaFrameFile(tracker->frameNumber);

		// NOTE: WE IGNORE the result of the prefetch because we want to only
		// really fail on the buffer read.
		if (retVal)
		{
			TRACE_0(errout << "ERROR: Prefetch of frame " << tracker->frameNumber << " FAILED (" << retVal << ") _________________");
			retVal = 0;
		}
	}

	double prefetchTime = localTimer.Read();
	UVTRACE(errout << " RAW: PREFETCH took " << prefetchTime << " msec");

	tracker->state = ASYNC_READ_STATE_POSTPROCESSING;   // BEFORE the signal!
	UVTRACE(errout << "___________________ RAW: Signal prefetch complete of " << tracker->frameNumber << " _________________");
	params.stateTransitionEvent->signal();

	if (!tracker->cancel)
	{
		UVTRACE(errout << "___________________ RAW: GET FRAME BUFFER FOR " << tracker->frameNumber << " _________________");

		MediaFrameBufferSharedPtr mediaFrameBuffer;

		// This is guaranteed to return a pointer to a buffer full of data
		// (synthesizes a gray frame if there's a read issue).
		retVal = readMediaFrameBuffer(nullptr, tracker->frameNumber, mediaFrameBuffer);
		MTIassert(mediaFrameBuffer);

		if (retVal)
		{
			TRACE_0(errout << "ERROR: Attempt to read frame " << tracker->frameNumber << " FAILED (" << retVal << ") _________________");
		}

		UVTRACE(errout << " RAW: GET FRAME BUFFER took " << (localTimer.Read() - prefetchTime) << " msec");

		if (!tracker->cancel)
		{
			displayRing[tracker->displayBufIndex].frameBuffer = mediaFrameBuffer;
		}
	}

	if (tracker->cancel)
	{
		UVTRACE(errout << "___________________ RAW: READ OPERATION WAS CANCELLED! _________________ ");
		retVal = ERR_OPERATION_CANCELED;
	}

	// Always return 0 here, it's not handled properly and causes crashing
	tracker->errorCode = 0; // retVal;
	tracker->readTimeInMsec = tracker->readTimer.Read();
	tracker->state = ASYNC_READ_STATE_COMPLETED;

	// Tell the Reader thread we're done with this frame
	UVTRACE(errout << "___________________ RAW: Signal completion of  " << tracker->frameNumber << " _________________");
	tracker->completionEvent.signal();
	UVTRACE(errout << "___________________ RAW: END POST-PROCESSING FOR  " << tracker->frameNumber << " _________________");
}
// -------------------------------------------------------------------------

void CPlayer::recordFrameReadTime(double newReadTime)
{
	// Use robust regression algorithm with the most recent 15 reads to
	// get an "average" read time

   // Add new read time to the ring buffer
   frameReadTime[(frameReadTimeIndex++) % INTERVAL_RING_SIZE] = float(newReadTime);

   // Copy the ring buffer to local storage so we don't have to lock
   float localReadTime[INTERVAL_RING_SIZE];

   // find shortest read time and count valid entries
   float shortestReadTime = 1000.0F; // one second
   int numberOfObservations = 0;
   for (int i = 0; i < INTERVAL_RING_SIZE; ++i)
   {
      float observedInterval = frameReadTime[i];

      // 0.0 means "hasn't been filled in yet"
      if (observedInterval > 0.0F)
      {
         localReadTime[numberOfObservations++] = observedInterval;
         if (observedInterval < shortestReadTime)
         {
            shortestReadTime = observedInterval;
         }
      }
   }

   // Fill in unused entries with the shortest time, so we go a litle
   // faster at start-up
   for (int i = numberOfObservations; i < INTERVAL_RING_SIZE; ++i)
   {
      localReadTime[i] = shortestReadTime;
   }

   if (numberOfObservations == 0)
   {
      averageReadTime = 0.0F;
   }
   else if (numberOfObservations < 3)
   {
      // NOTE! robust() has a bug where if you pass it a vector full of
      // 0's it crashes, so we short-circuit here and declare the shortest
      // time to be the "average"
      averageReadTime = shortestReadTime;
   }
   else
   {
      // Do robust regression
      mvector<double>vec_b(1);
      vec_b.zero();
      mvector<double>vec_y(INTERVAL_RING_SIZE);
      matrix<double>matrix_X(INTERVAL_RING_SIZE, 1);
      int j = 0;
      for (int i = 0; i < INTERVAL_RING_SIZE; ++i)
      {
         if (localReadTime[i] > 0.0F)
         {
            vec_y[j] = localReadTime[i] - shortestReadTime;
            matrix_X[j][0] = 1;
            ++j;
         }
      }
      robust(vec_y, matrix_X, 0.01, vec_b);
      averageReadTime = shortestReadTime +float(vec_b[0]);
   }

	UVTRACE(errout << "////////////////// waited " << newReadTime << " for current frame [avg=" << averageReadTime << ", short=" <<
         shortestReadTime << "]");
}
//------------------------------------------------------------------------------

int CPlayer::setFrameCompareMode(FrameCompareMode newMode)
{
	_frameCompareMode = newMode;
	mainWindow->GetDisplayer()->calculateImageRectangle();

	CToolManager toolMgr;
	if (!isDisplaying() && !toolMgr.IsToolProcessing())
	{
		refreshFrameSynchronousCached();
	}

	return 0;
}
//------------------------------------------------------------------------------

CPlayer::FrameCompareMode CPlayer::getFrameCompareMode()
{
	return _frameCompareMode;
}
//------------------------------------------------------------------------------

int CPlayer::setFrameCompareClipName(const string &clipName)
{
	CAutoErrorReporter autoErr("CPlayer::setImportFrameClipName", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

	if (_frameCompareClip != nullptr)
	{
		if (_frameCompareClipName == clipName)
		{
			// Pop out of wipe mode
			if (_frameCompareMode != FrameCompareMode::Off)
			{
				setFrameCompareMode(FrameCompareMode::Off);
			}

			return 0;
		}

		CBinManager binManager;
		binManager.closeClip(_frameCompareClip);
		_frameCompareClip = nullptr;
		_frameCompareClipFrameList = nullptr;
	}

	_frameCompareClipName = clipName;
	if (_frameCompareClipName.empty())
	{
		return 0;
	}

	int retVal = 0;
	CBinManager binManager;

	_frameCompareClip = binManager.openClip(_frameCompareClipName, &retVal);
	if (retVal != 0)
	{
		autoErr.errorCode = retVal;
		autoErr.msg << "Can't open clip " << _frameCompareClipName;
		return retVal;
	}

	// Look up the main video framelist from the clip just once
	_frameCompareClipFrameList = _frameCompareClip->getVideoFrameList(VIDEO_SELECT_NORMAL, FRAMING_SELECT_VIDEO);
	if (_frameCompareClipFrameList == nullptr)
	{
		autoErr.errorCode = retVal;
		autoErr.msg << "Unable to get framelist from clip " << _frameCompareClipName;
		return retVal;
	}

	CToolManager toolMgr;
	if (_frameCompareMode != FrameCompareMode::Off && !isDisplaying() && !toolMgr.IsToolProcessing())
	{
		refreshFrameSynchronousCached();
	}

	return 0;
}
//------------------------------------------------------------------------------

int CPlayer::setFrameCompareWiperOriginClient(POINT refPointClient)
{
	_frameCompareWiperRelativeOrigin.x = mainWindow->GetDisplayer()->scaleClientXToRgbRectNormalized(refPointClient.x);
	_frameCompareWiperRelativeOrigin.y = mainWindow->GetDisplayer()->scaleClientYToRgbRectNormalized(refPointClient.y);

	CToolManager toolMgr;
	if (_frameCompareMode == FrameCompareMode::Wipe && !isDisplaying() && !toolMgr.IsToolProcessing())
	{
		refreshFrameSynchronousCached();
	}

	return 0;
}
//------------------------------------------------------------------------------

int CPlayer::changeFrameCompareWiperOriginRelative(double fractionalOffset, bool wrapAtExtremes)
{
	const double MaxPosition = 0.98;
	const double MinPosition = 0.02;

	// Special case: if relative offset is 1.0 or -1.0, then toggle between the
	// max and min positions so the muose wheel always works when shift is down!
	if (wrapAtExtremes
	&& (fractionalOffset == 1.0 || fractionalOffset == -1.0)
	&& (_frameCompareWiperRelativeOrigin.x == MaxPosition && _frameCompareWiperRelativeOrigin.y == MaxPosition))
	{
		_frameCompareWiperRelativeOrigin.x = MinPosition;
		_frameCompareWiperRelativeOrigin.y = MinPosition;
	}
	else if (wrapAtExtremes
	&& (fractionalOffset == 1.0 || fractionalOffset == -1.0)
	&& (_frameCompareWiperRelativeOrigin.x == MinPosition && _frameCompareWiperRelativeOrigin.y == MinPosition))
	{
		_frameCompareWiperRelativeOrigin.x = MaxPosition;
		_frameCompareWiperRelativeOrigin.y = MaxPosition;
	}
	else
	{
		double x = _frameCompareWiperRelativeOrigin.x + fractionalOffset;
		x = std::min<double>(MaxPosition, std::max<double>(MinPosition, x));
		_frameCompareWiperRelativeOrigin.x = x;
		double y = _frameCompareWiperRelativeOrigin.y + fractionalOffset;
		y = std::min<double>(MaxPosition, std::max<double>(MinPosition, y));
		_frameCompareWiperRelativeOrigin.y = y;
	}

	CToolManager toolMgr;
	if (_frameCompareMode == FrameCompareMode::Wipe && !isDisplaying() && !toolMgr.IsToolProcessing())
	{
		refreshFrameSynchronousCached();
	}

	return 0;
}
//------------------------------------------------------------------------------

int CPlayer::setFrameCompareWiperAngle(float degrees)
{
	degrees = std::min<float>(90.F, std::max<float>(0.F, degrees));
	_frameCompareWiperAngleInDegrees = degrees;

	if (_frameCompareMode != FrameCompareMode::Wipe || isDisplaying())
	{
		return 0;
	}

	CToolManager toolMgr;
	if (_frameCompareMode == FrameCompareMode::Wipe && !isDisplaying() && !toolMgr.IsToolProcessing())
	{
		refreshFrameSynchronousCached();
	}

	return 0;
}
//------------------------------------------------------------------------------

int CPlayer::changeFrameCompareWiperAngle(float degrees, bool wrapAtExtremes)
{
	// Special case: if change angle is 90 or -90, then toggle between
	// those extremes, so the muose wheel always works when shift is down!
	if (wrapAtExtremes
	&& (degrees == 90.0 || degrees == -90.F)
	&& (_frameCompareWiperAngleInDegrees == 90.F || _frameCompareWiperAngleInDegrees == 0.F))
	{
		_frameCompareWiperAngleInDegrees = (_frameCompareWiperAngleInDegrees == 0.F) ? 90.F : 0.F;
	}
	else
	{
		_frameCompareWiperAngleInDegrees += degrees;
		_frameCompareWiperAngleInDegrees = std::min<float>(90.F, std::max<float>(0.F, _frameCompareWiperAngleInDegrees));
	}

	if (_frameCompareMode != FrameCompareMode::Wipe || isDisplaying())
	{
		return 0;
	}

	CToolManager toolMgr;
	if (_frameCompareMode == FrameCompareMode::Wipe && !isDisplaying() && !toolMgr.IsToolProcessing())
	{
		refreshFrameSynchronousCached();
	}

	return 0;
}
//------------------------------------------------------------------------------

int CPlayer::toggleFrameCompareDividerVertHoriz()
{
	_frameCompareWiperAngleInDegrees = (_frameCompareWiperAngleInDegrees > 45.F)
														? 0.F
														: 90.F;

	if (_frameCompareMode != FrameCompareMode::Wipe || isDisplaying())
	{
		return 0;
	}

	CToolManager toolMgr;
	if (_frameCompareMode == FrameCompareMode::Wipe && !isDisplaying() && !toolMgr.IsToolProcessing())
	{
		refreshFrameSynchronousCached();
	}

	return 0;
}

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------

// class DisplayRing

CPlayer::DisplayRing::DisplayRing()
{
	_ring = nullptr;
	_numberOfSlots = 0;
}

CPlayer::DisplayRing::~DisplayRing()
{
	delete[] _ring;
}

void CPlayer::DisplayRing::setMaxSize(int newMaxSize)
{
	delete[] _ring;
	_ring = new DisplayRingSlot[newMaxSize];
   _maxNumberOfSlots = newMaxSize;
	_numberOfSlots = newMaxSize;
}

int CPlayer::DisplayRing::getMaxSize()
{
	return _maxNumberOfSlots;
}

void CPlayer::DisplayRing::setSize(int newSize)
{
	_numberOfSlots = std::min<int>(newSize, _maxNumberOfSlots);
}

int CPlayer::DisplayRing::size()
{
	return _numberOfSlots;
}

CPlayer::DisplayRingSlot &CPlayer::DisplayRing::operator[](int ptr)
{
	if (_numberOfSlots < 1)
	{
		static DisplayRingSlot dummySlot;
		return dummySlot;
	}

	return _ring[ptr % _numberOfSlots];
}
// -------------------------------------------------------------------------




