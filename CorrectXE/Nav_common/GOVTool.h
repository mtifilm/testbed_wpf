// GOVTool.h: interface for the CGOVTool class.
//
/*
$Header: /usr/local/filmroot/Nav_common/include/Attic/GOVTool.h,v 1.1.2.13 2009/06/16 12:03:09 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////

#if !defined(GOVTOOL_H)
#define GOVTOOL_H

#include "ToolObject.h"
#include "PixelRegions.h"
#include "RegionOfInterest.h"
//#include "RepairDRS.h"
//#include "RepairShift.h"
#include "MTIio.h"
#include "IniFile.h"



//////////////////////////////////////////////////////////////////////

// GOV Tool Number (16-bit number that uniquely identifies this tool)
#define GOV_TOOL_NUMBER    222

// Values that are currently hardcoded, but need to be made
// more general eventually.
#define ORIGINAL_RECT_BORDER 3 // size of larger rectangle around original fix

// GOV Tool Command Numbers
#define GOV_CMD_INVALID                     -1
#define GOV_CMD_START_SELECT_RECT            1
#define GOV_CMD_START_SELECT_RECT_ZOOM       2
#define GOV_CMD_START_SELECT_LASSO           3
#define GOV_CMD_START_SELECT_LASSO_ZOOM      4
#define GOV_CMD_END_SELECT                   5
#define GOV_CMD_SELECT_CANCEL                6
#define GOV_CMD_ACCEPT                       7
#define GOV_CMD_ACCEPT_IN_PLACE              8
#define GOV_CMD_ACCEPT_AND_APPLY_ALL         9
#define GOV_CMD_REJECT                      10
#define GOV_CMD_STOP_ALL_PROCESSING         11
#define GOV_CMD_REPEAT_LAST_GOV_OR_DRS      12
#define GOV_CMD_REPEAT_TOGGLE_GOV_OR_DRS    13
#define GOV_CMD_TOGGLE_GOV                  14
#define GOV_CMD_TOGGLE_GOV_ZOOM             15
#define GOV_CMD_UNZOOM                      16
#define GOV_CMD_REVIEW_PREV                 17
#define GOV_CMD_REVIEW_PREV_ZOOM            18
#define GOV_CMD_REVIEW_NEXT                 19
#define GOV_CMD_REVIEW_NEXT_ZOOM            20

enum EGOVMouseMode
{
   GOV_MOUSE_MODE_NONE,
   GOV_MOUSE_MODE_STRETCH,
};


//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CGOVToolParameters;

//////////////////////////////////////////////////////////////////////


class CGOVTool : public CToolObject
{
public:
   CGOVTool(const string &newToolName);
   virtual ~CGOVTool();

   // Static method to create a new instance of this tool
   static CToolObject* MakeTool(const string& newToolName);

   virtual int toolInitialize(CToolSystemInterface *newSystemAPI);
   virtual int toolShutdown();

   // Tool Event Handlers
   virtual int onChangingClip();
   virtual int onNewClip();
   virtual int onChangingFraming();
   virtual int onRedraw(int frameIndex);
   virtual int onToolCommand(CToolCommand &toolCommand);
   virtual int onToolProcessingStatus(const CAutotoolStatus& autotoolStatus);
   virtual int onMouseMove(CUserInput &userInput);

   virtual bool toolHideQuery();
   virtual int toolActivate();
   virtual int toolDeactivate();

   virtual CToolProcessor* makeToolProcessorInstance(
                                          const bool *newEmergencyStopFlagPtr);

   virtual bool IsInConsumeAllInputMode();
   virtual int TryToResolveProvisional();

   // Not part of the CToolObject interface
   bool IsGOVInProgress();
   bool IsGOVPending();
   void AcceptGOVIfPending();
   void RejectGOVIfPending();
   void SetTildeKeyHackMode(ETildeKeyHackMode newMode);
   void SetGOVRepeatShape(ESelectedRegionShape shape,
                          const RECT &rect,
                          const POINT *lasso,
                          const CPixelRegionList *regionList);
   void SetGOVToolProgressMonitor(
                          IToolProgressMonitor *newToolProgressMonitor);

   // Kludge, should be correctly moved to interface
   void ReviewToolSingleStep(bool goLast, bool goZoom);

private:

// Instance Variables of GOV Tool

   ESelectedRegionShape selectionShape;
   RECT selectionRect;
   POINT *selectionLassoPtr;
   POINT *lassoCopy;
   BEZIER_POINT *selectionBezierPtr;
   BEZIER_POINT *bezierCopy;
   CPixelRegionList *selectionRegionList;
   bool selectionCameFromUser;
   bool delayedZoom;
   EGOVMouseMode mouseMode;
   bool govInProgress;
   bool multiframeGovInProgress;
   bool govPending;
   int pendingGOVFrameIndex;
   bool lastActionWasGOV;
   ETildeKeyHackMode tildeKeyHackMode;
   bool govIsActive;
   bool bSendStartNotifyOnNextMouseMove;
   int oldMouseX;
   int oldMouseY;

   int singleFrameToolSetupHandle;
   int multiFrameToolSetupHandle;

   // General Crap
   void InitState();
   bool NotifyGOVStarting(bool *cancelStretch = nullptr);
   void NotifyGOVDone();

   // Command Handlers
   int BeginGovSelect(ESelectedRegionShape shape, bool goZoom);
   int EndGovSelect();
   int TogglePendingGOV(bool goZoom);
   int AcceptPendingGOV(bool goToFrame);
   int ApplyPendingGOVToMarkedRange();
   int RejectPendingGOV(bool goToFrame);
   int HandleTildeKeyHack(bool shifted);

   int DoSingleFrameGOV();
   int DoMultiFrameGOV();

   // Tool processing
   virtual int RenderSingleFrame();
   virtual int RunFrameProcessing(int newResumeFrame);
   int GatherGOVToolParameters(CGOVToolParameters &toolParams);
   void DestroyAllToolSetups();

   // Helpers
   POINT* CopyLasso(const POINT *srcLasso);

   // GOV Review Tool Operations
   int ReviewToolCommand(int toolCommand);
   void ReviewToolFrame();
   void ReviewToolSelect();

};

//////////////////////////////////////////////////////////////////////

struct SGOVParameters
{
   ESelectedRegionShape selectionShape;
   RECT selectionRect;    // Bounding rectangle
   POINT *selectionLasso; // Must be non-NULL if selection shape is LASSO
   CPixelRegionList *selectionRegionList; // Non-NULL if shape is 'region'
};

class CGOVToolParameters : public CToolParameters
{
public:
   CGOVToolParameters() : CToolParameters(1, 1) {};
   virtual ~CGOVToolParameters() { };

   SGOVParameters govParams;

};

//////////////////////////////////////////////////////////////////////


class CGOVToolProc : public CToolProcessor
{
public:
   CGOVToolProc(int newToolNumber, const string &newToolName,
                   CToolSystemInterface *newSystemAPI,
                   const bool *newEmergencyStopFlagPtr,
                   IToolProgressMonitor *newToolProgressMonitor);
   virtual ~CGOVToolProc();

   int SetIOConfig(CToolIOConfig *toolIOConfig);
   int SetFrameRange(CToolFrameRange *toolFrameRange);

private:
   // Tool's functions called from within context of the processing loop.
   int SetParameters(CToolParameters *toolParams);
   int BeginProcessing(SToolProcessingData &procData);
   int EndProcessing(SToolProcessingData &procData);
   int BeginIteration(SToolProcessingData &procData);
   int EndIteration(SToolProcessingData &procData);
   int DoProcess(SToolProcessingData &procData);
   int GetIterationCount(SToolProcessingData &procData);

private:

   int inFrameIndex;
   int outFrameIndex;

   SGOVParameters govParams;
};

extern MTI_UINT32 *GOVFeatureTable[];

//////////////////////////////////////////////////////////////////////

#endif // !defined(GOVTOOL_H)
