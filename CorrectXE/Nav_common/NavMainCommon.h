// NavMainCommon.h:  Platform-independent components of Navigator Main
//////////////////////////////////////////////////////////////////////

#ifndef NavMainCommonH
#define NavMainCommonH

#include "ClipSharedPtr.h"
#include "timecode.h"
#include <string>
using std::string;

//////////////////////////////////////////////////////////////////////
// Forward Declarations
class CClip;
class CVideoFrameList;
class CReticle;
struct DISPLAY_CONTEXT;
class CIniFile;

//////////////////////////////////////////////////////////////////////

class CNavCurrentClip
{
public:
   CNavCurrentClip();
   ~CNavCurrentClip();

   bool needToDeleteMask(ClipSharedPtr &previousClip, ClipSharedPtr &newClip);
   int openClipByName(const string& binPath, const string& clipName);

   int reloadCurrentClip();
   int unloadCurrentClip();
   int closeCurrentClip();

   int saveClipState();
   int writeClipStateToIniFile(CIniFile *ini, const string &sectionName);

   int setClip(ClipSharedPtr &newClip);

   int selectVideoProxy(int newVideoProxyIndex);
   int selectVideoFraming(int newVideoFramingIndex);

   string getPreviousBinPath() const;
   string getPreviousClipName() const;

   string getCurrentBinPath() const;
   string getCurrentParentPath() const;
   string getCurrentClipPath() const;
   ClipSharedPtr getCurrentClip() const;
   string getCurrentClipFileName() const;
   string getCurrentClipFileNameWithExt() const;
   string getCurrentClipName() const;
   CVideoFrameList* getCurrentVideoFrameList() const;
   int getCurrentVideoProxyIndex() const;
   int getCurrentVideoFramingIndex() const;
   int getCurrentDisplayDevicesIndex() const;
   int getCurrentDisplayModeIndex() const;
   int getCurrentSlowDisplaySpeedValue() const;
   int getCurrentMediumDisplaySpeedValue() const;
   int getCurrentFastDisplaySpeedValue() const;
   int getCurrentDisplaySpeedIndex() const;

   bool isClipAvailable() const;   // true if current clip is non-NULL
   bool isThisCurrentClip(const string &clipFileName);

   CTimecode limitTimecode(const CTimecode &inTC, bool warn);

private:
   ClipSharedPtr currentClip;
   int currentVideoProxyIndex;
   int currentVideoFramingIndex;
   int currentDisplayDevicesIndex;
   int currentDisplayWindowIndex;
   int currentSlowDisplaySpeedValue;
   int currentMediumDisplaySpeedValue;
   int currentFastDisplaySpeedValue;
   int currentDisplaySpeedIndex;

   string currentBinPath;
   string currentClipName;
   string previousBinPath;
   string previousClipName;

private:
   int setClipVideoProxyAndFraming(ClipSharedPtr &newClip, int newVideoProxyIndex,
                                                   int newVideoFramingIndex,
                                                   int newDisplayDevicesIndex,
                                                   int newDisplayWindowIndex,
                                                   DISPLAY_CONTEXT& newDisplayContext,
                                                   int newReticleModeIndex,
                                                   CReticle& newReticle,
                                                   const string& newTCStr,
                                                   const string& fallbackTCStr,
                                                   const string& markInTCStr,
                                                   const string& markOutTCStr);

   int selectVideoProxyAndFraming(int newVideoProxyIndex,
                                  int newVideoFramingIndex);
   void setMarks(CVideoFrameList *videoFrameList, const string& markInTCStr,
                 const string& markOutTCStr);

private:
   // Ini file strings for saved clip state
   static string clipStateSectionNamePrefix;
   static string proxyIndexKey;
   static string framingIndexKey;
   static string frameIndexKey;
   static string timeCodeIndexKey;
   static string markInKey;
   static string markOutKey;
   static string displayDevicesIndexKey;
   static string displayModeValueKey;
   static string zoomFactorValueKey;
   static string displayXCenterValueKey;
   static string displayYCenterValueKey;
   static string slowDisplaySpeedValueKey;
   static string mediumDisplaySpeedValueKey;
   static string fastDisplaySpeedValueKey;
   static string displaySpeedIndexKey;
   static string customLUTKey;
   static string customLUTFormatDefaultKey;
   static string customLUTColorSpaceIndexKey;
   static string customLUTSourceRangeKey;
   static string customLUTRYBlkKey;
   static string customLUTRYWhiKey;
   static string customLUTGUBlkKey;
   static string customLUTGUWhiKey;
   static string customLUTBVBlkKey;
   static string customLUTBVWhiKey;
   static string customLUTRMinKey;
   static string customLUTRMaxKey;
   static string customLUTGMinKey;
   static string customLUTGMaxKey;
   static string customLUTBMinKey;
   static string customLUTBMaxKey;
   static string gammaValueKey;
   static string reticleModeKey;
   static string reticleTypeKey;
   static string reticleFileKey;
   static string reticleAspectRatioKey;
   static string reticleApertureKey;
   static string reticleXOffsetKey;
   static string reticleYOffsetKey;
   static string reticleHScaleKey;
   static string reticleVScaleKey;
};

//////////////////////////////////////////////////////////////////////

#endif // #if !defined(NAV_MAIN_COMMON_H)


