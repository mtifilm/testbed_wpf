// BaseTool.cpp: implementation of the CBaseTool class.
//
// The CBaseTool class is derived from the CToolObject class.
// The CBaseTool's purposes is to 1) support keyboard mapping
// and 2) be the primary location for the Base's user-interface
// logic.  To the extent possible, CBaseTool is platform-independent.
//
//////////////////////////////////////////////////////////////////////

#include <stdio.h>   // TTT

#include "BaseTool.h"
#include "NavMainCommon.h"
#include "ImageFormat3.h"
#include "IniFile.h"

#include "ToolCommand.h"
#include "ToolCommandTable.h"
#include "ToolSystemInterface.h"
#include "ToolUserInputMap.h"

#include "CompactTrackEditFrameUnit.h"
#include "MainWindowUnit.h"
#include "MTIKeyDef.h"
#include "BaseToolUnit.h"
#include "TrackEditFrameUnit.h"

#include "Displayer.h"
#include "Player.h"


// -------------------------------------------------------------------------

static MTI_UINT16 baseToolNumber = BASE_TOOL_NUMBER;
CBaseTool *GBaseTool = NULL;

// -------------------------------------------------------------------------
// Base Tool Default Keyboard and Mouse Button Configuration

static CUserInputConfiguration::SConfigItem baseDefaultConfigItems[] =
{
   // Base Tool Command                    Modifiers      Action
   //                                       + Key
   // ----------------------------------------------------------------
   // Basic Base Tool Commands
   { BASE_CMD_NOOP,                       " Esc          KeyDown   " },

 { BASE_CMD_ARROW_UP,                         " Up               KeyDown   " },
 { BASE_CMD_ARROW_DOWN,                       " Down             KeyDown   " },
 { BASE_CMD_ARROW_LEFT,                       " Left             KeyDown   " },
 { BASE_CMD_ARROW_RIGHT,                      " Right            KeyDown   " },
 { BASE_CMD_SHIFT_ARROW_UP,                   " Shift+Up         KeyDown   " },
 { BASE_CMD_SHIFT_ARROW_DOWN,                 " Shift+Down       KeyDown   " },
 { BASE_CMD_SHIFT_ARROW_LEFT,                 " Shift+Left       KeyDown   " },
 { BASE_CMD_SHIFT_ARROW_RIGHT,                " Shift+Right      KeyDown   " },
 { BASE_CMD_CTRL_ARROW_UP,                    " Ctrl+Up          KeyDown   " },
 { BASE_CMD_CTRL_ARROW_DOWN,                  " Ctrl+Down        KeyDown   " },
 { BASE_CMD_CTRL_ARROW_LEFT,                  " Ctrl+Left        KeyDown   " },
 { BASE_CMD_CTRL_ARROW_RIGHT,                 " Ctrl+Right       KeyDown   " },

};

static CUserInputConfiguration *baseToolDefaultUserInputConfiguration
 = new CUserInputConfiguration(sizeof(baseDefaultConfigItems)
                               /sizeof(CUserInputConfiguration::SConfigItem),
                               baseDefaultConfigItems);

// -------------------------------------------------------------------------
// Base Tool Command Table

static CToolCommandTable::STableEntry baseCommandTableEntries[] =
{
   // Navigator Command                    Base Tool Command String
   // ----------------------------------------------------------------
   // Basic Navigator Commands
   { BASE_CMD_NOOP,                       "No-op"                },

 { BASE_CMD_ARROW_UP,                         "BASE_CMD_ARROW_UP" },
 { BASE_CMD_ARROW_DOWN,                       "BASE_CMD_ARROW_DOWN" },
 { BASE_CMD_ARROW_LEFT,                       "BASE_CMD_ARROW_LEFT" },
 { BASE_CMD_ARROW_RIGHT,                      "BASE_CMD_ARROW_RIGHT" },
 { BASE_CMD_SHIFT_ARROW_UP,                   "BASE_CMD_SHIFT_ARROW_UP" },
 { BASE_CMD_SHIFT_ARROW_DOWN,                 "BASE_CMD_SHIFT_ARROW_DOWN" },
 { BASE_CMD_SHIFT_ARROW_LEFT,                 "BASE_CMD_SHIFT_ARROW_LEFT" },
 { BASE_CMD_SHIFT_ARROW_RIGHT,                "BASE_CMD_SHIFT_ARROW_RIGHT" },
 { BASE_CMD_CTRL_ARROW_UP,                    "BASE_CMD_CTRL_ARROW_UP" },
 { BASE_CMD_CTRL_ARROW_DOWN,                  "BASE_CMD_CTRL_ARROW_DOWN" },
 { BASE_CMD_CTRL_ARROW_LEFT,                  "BASE_CMD_CTRL_ARROW_LEFT" },
 { BASE_CMD_CTRL_ARROW_RIGHT,                 "BASE_CMD_CTRL_ARROW_RIGHT" },

};

static CToolCommandTable *baseToolCommandTable
= new CToolCommandTable(sizeof(baseCommandTableEntries)
                        /sizeof(CToolCommandTable::STableEntry),
                           baseCommandTableEntries);

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBaseTool::CBaseTool(const string &newToolName)
: CToolObject(newToolName, NULL)
{
   // Nothing to do
}

CBaseTool::~CBaseTool()
{
   // Nothing to do
}

CToolObject* CBaseTool::MakeTool(const string& newToolName)
{
   GBaseTool = new CBaseTool(newToolName);
   return GBaseTool;
}

CToolProcessor* CBaseTool::makeToolProcessorInstance(
                                          const bool *newEmergencyStopFlagPtr)
{
   return 0;   // This tool doesn't have a CToolProcessor
}

// ===================================================================
//
// Function:    toolInitialize
//
// Description: Called when the ToolManager wants to initialize
//              the Base Tool
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================
int CBaseTool::toolInitialize(CToolSystemInterface *newSystemAPI)
{
   int retVal;

   // Whups!!! Bad naming for this tool - should change it!!   QQQ

   // Initialize the CBaseTool's base class, i.e., CToolObject
   // This must be done before the CBaseTool initializes itself
   retVal = initBaseToolObject(baseToolNumber, newSystemAPI,
                               baseToolCommandTable,
                               baseToolDefaultUserInputConfiguration);
   if (retVal != 0)
      {
      // ERROR: initBaseToolObject failed
      return retVal;
      }

   // TO DO: the CBaseTool's initialization code goes here

   return retVal;
}

// ===================================================================
//
// Function:    toolShutdown
//
// Description: Called when the Tool Manager wants to shut down the
//              Base Tool, typically when the main program is
//              shutting down.
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================
int CBaseTool::toolShutdown()
{
   int retVal;

   // TO DO: the Base Tool's shutdown code goes here

   GBaseTool = NULL;  // Don't want GUI to call us any more

   // Shutdown the Navigator Tool's base class, i.e., CToolObject.
   // This must be done after the Navigator Tool shuts down itself
   retVal = shutdownBaseToolObject();
   if (retVal != 0)
      {
      // ERROR: shutdownBaseToolObject failed
      return retVal;
      }

   return retVal;
}

// ===================================================================
//
// Function:    onToolCommand
//
// Description: Navigator Tool's Command Handler
//
// Arguments:   CToolCommand &toolCommand
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================
int CBaseTool::onToolCommand(CToolCommand &toolCommand)
{
   // Default return is that the Navigator command did nothing
   int retVal = TOOL_RETURN_CODE_NO_ACTION;

   int commandNumber = toolCommand.getCommandNumber();

   switch (commandNumber)
   {
      case BASE_CMD_ARROW_UP:
      case BASE_CMD_ARROW_DOWN:
      case BASE_CMD_ARROW_LEFT:
      case BASE_CMD_ARROW_RIGHT:
      case BASE_CMD_SHIFT_ARROW_UP:
      case BASE_CMD_SHIFT_ARROW_DOWN:
      case BASE_CMD_SHIFT_ARROW_LEFT:
      case BASE_CMD_SHIFT_ARROW_RIGHT:
      case BASE_CMD_CTRL_ARROW_UP:
      case BASE_CMD_CTRL_ARROW_DOWN:
      case BASE_CMD_CTRL_ARROW_LEFT:
      case BASE_CMD_CTRL_ARROW_RIGHT:
         if (SendArrowsKeysToTrackBar(commandNumber))
         {
            retVal = (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_PROCESSED_OK);
         }
         break;

      default:
         break;
   }

   return retVal;
}

// --------------------------------------------------------------------------
bool CBaseTool::SendArrowsKeysToTrackBar(int commandNumber)
{
   WORD key = 0;
   TShiftState shiftState;
   switch (commandNumber)
   {
      case BASE_CMD_ARROW_UP:
      case BASE_CMD_SHIFT_ARROW_UP:
      case BASE_CMD_CTRL_ARROW_UP:
         key = MTK_UP;
         break;
      case BASE_CMD_ARROW_DOWN:
      case BASE_CMD_SHIFT_ARROW_DOWN:
      case BASE_CMD_CTRL_ARROW_DOWN:
         key = MTK_DOWN;
         break;
      case BASE_CMD_ARROW_LEFT:
      case BASE_CMD_SHIFT_ARROW_LEFT:
      case BASE_CMD_CTRL_ARROW_LEFT:
         key = MTK_LEFT;
         break;
      case BASE_CMD_ARROW_RIGHT:
      case BASE_CMD_SHIFT_ARROW_RIGHT:
      case BASE_CMD_CTRL_ARROW_RIGHT:
         key = MTK_RIGHT;
         break;
      default:
         break;
   }

   switch (commandNumber)
   {
      case BASE_CMD_ARROW_UP:
      case BASE_CMD_ARROW_DOWN:
      case BASE_CMD_ARROW_LEFT:
      case BASE_CMD_ARROW_RIGHT:
         break;
      case BASE_CMD_SHIFT_ARROW_UP:
      case BASE_CMD_SHIFT_ARROW_DOWN:
      case BASE_CMD_SHIFT_ARROW_LEFT:
      case BASE_CMD_SHIFT_ARROW_RIGHT:
         shiftState << ssShift;
         break;
      case BASE_CMD_CTRL_ARROW_UP:
      case BASE_CMD_CTRL_ARROW_DOWN:
      case BASE_CMD_CTRL_ARROW_LEFT:
      case BASE_CMD_CTRL_ARROW_RIGHT:
         shiftState << ssCtrl;
         break;
      default:
         break;
   }
   int retVal = TTrackEditFrame::ProcessKeyDown(key, shiftState);
   if (!retVal)
   {
      retVal = TCompactTrackEditFrame::ProcessKeyDown(key, shiftState);
   }

   return retVal;
}
// ===================================================================

// ===================================================================
//
// Function:    onMouseMove
//
// Description: Base Tool's Mouse Movement Handler
//
// Arguments:   CUserInput &userInput
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================
int CBaseTool::onMouseMove(CUserInput &userInput)
{
   int retVal = TOOL_RETURN_CODE_NO_ACTION;


   return retVal;
}

// ===================================================================
//
// Function:    onRedraw
//
// Description:
//
// Arguments:   None
//
// Returns:
//
// ===================================================================
int CBaseTool::onRedraw(int frameIndex)
{

  return(TOOL_RETURN_CODE_EVENT_CONSUMED);
}

int CBaseTool::onNewClip()
{
   int retVal = TOOL_RETURN_CODE_NO_ACTION;


   return retVal;
}
