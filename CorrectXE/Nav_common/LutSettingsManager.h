#ifndef __LUT_SETTINGS_MANAGER_H__
#define __LUT_SETTINGS_MANAGER_H__

////////////////////////////////////////////////////////////////////////

#include "IniFile.h"

#include "ClipAPI.h"
#include "DisplayLutSettings.h"
#include "ImageFormat3.h"
#include "PropX.h"            // For CBHook

////////////////////////////////////////////////////////////////////////

enum ELutSelection
{
   LUTSEL_NONE,
   LUTSEL_PROJECT,
   LUTSEL_CLIP,
   LUTSEL_USER
};

struct SLutSettingsListEntry
{
    string name;
    CDisplayLutSettings settings;
    char shortcut;

    SLutSettingsListEntry();
    int getOrdinal() const ;
    bool operator<(const SLutSettingsListEntry &rhs) const;
};

typedef list<SLutSettingsListEntry> TLutSettingsList;
typedef list<SLutSettingsListEntry>::iterator TLutSettingsListIter;


////////////////////////////////////////////////////////////////////////


class CLutSettingsManager
{
public:
   CLutSettingsManager();

   void LoadAndApplyAllNewClipSettings(ClipSharedPtr &clip);

   void SelectLut(ELutSelection newSelection, const string *userLutName=nullptr);
   ELutSelection GetLutSelection(string &userLutName);

   void GetUserLutSettingsNames(StringList &listOfNames);

   CDisplayLutSettings GetProjectLutSettings();
   CDisplayLutSettings GetClipLutSettings();
   CDisplayLutSettings GetUserLutSettings(const string &lutName);

   int SetProjectLutSettings(const CDisplayLutSettings &newSettings);
   int SetClipLutSettings(const CDisplayLutSettings &newSettings);
   int SetUserLutSettings(const string &lutName,
                          const CDisplayLutSettings &newSettings);

   char GetUserLutShortcut(const string &lutName);
   int SetUserLutShortcut(const string &lutName, char newShortcut);
   int RemoveUserLutSettings(const string &lutName);

   void ApplyProjectLutSettings();
   void ApplyClipLutSettings();
   void ApplyUserLutSettings(const string &lutName);

   void TakeSnapshot();
   void RevertToSnapshot();

   CBHook LutSettingsChange;

private:

   // Apply settings based on what's selected
   void applyAppropriateSettings();

   // Finds a user lut settings entry by lut name
   TLutSettingsListIter findUserLutSettingsListEntry(const string &lutName);

   // Get the paths to ini files containing the Project/Clip/User LUT settings
   string findProjectLutIniFile();
   string findClipLutIniFile();
   string findUserLutIniFile();

   // We directly manage the user lut list on disk
   int readUserLutListFromIniFile();
   int saveUserLutListToIniFile();

   // we convert old-time lut list the first time we run
   int readLegacyUserLutList();

private:

   string cachedClipPath;
   CDisplayLutSettings projectLutSettings;
   CDisplayLutSettings clipLutSettings;
   StringList userLutNames;
   TLutSettingsList userLutSettingsList;

   ELutSelection selectedLut;
   string selectedUserLutName;

   struct SSnapshot
   {
      CDisplayLutSettings projectLutSettings;
      CDisplayLutSettings clipLutSettings;
      StringList userLutNames;
      TLutSettingsList userLutSettingsList;
      ELutSelection selectedLut;
      string selectedUserLutName;
   } Snapshot;

};

#endif //  __LUT_SETTINGS_MANAGER_H__
