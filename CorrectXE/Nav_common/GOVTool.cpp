// GOVTool.cpp: implementation of the CGOVTool class.
//
/*
$Header: /usr/local/filmroot/Nav_common/source/Attic/GOVTool.cpp,v 1.1.2.37 2009/10/20 12:44:28 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "GOVTool.h"

#include "IniFile.h"  // for TRACE_N()
#include "err_clip.h"
#include "MTIDialogs.h"
#include "MTIKeyDef.h"
#include "MTIsleep.h"
#include "MTIstringstream.h"
#include "MTIWinInterface.h"  // for CBusyCursor
#include "PixelRegions.h"
#include "RepairDRS.h"
#include "RepairShift.h"
#include "ToolCommand.h"
#include "ToolCommandTable.h"
#include "ToolProgressMonitor.h"
#include "ToolSystemInterface.h"
#include "ToolUserInputMap.h"
#include "ImageFormat3.h"
#include "Clip3.h"
#include "cpmp_err.h"  // for ERR_ALLOC

#define GENERIC_ERROR -1;

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

static MTI_UINT16 govToolNumber = GOV_TOOL_NUMBER;

// -------------------------------------------------------------------------
// GOV Default Keyboard and Mouse Button Configuration

static CUserInputConfiguration::SConfigItem govDefaultConfigItems[] =
{
   // GOV Command                          Modifiers      Action
   //                                       + Key
   // ----------------------------------------------------------------

// Start selection - rect or lasso
   { GOV_CMD_START_SELECT_RECT,           " RButton            ButtonDown" },
   { GOV_CMD_START_SELECT_LASSO,          " Ctrl+RButton       ButtonDown" },

// End selection
   { GOV_CMD_END_SELECT,                  " RButton            ButtonUp  " },
   { GOV_CMD_END_SELECT,                  " Ctrl+RButton       ButtonUp  " },

// Per Dave and Larry, eliminate the GOV-and-zoom commands
// { GOV_CMD_START_SELECT_RECT_ZOOM,      " Shift+RButton      ButtonDown" },
// { GOV_CMD_START_SELECT_LASSO_ZOOM,     " Ctrl+Shift+RButton ButtonDown" },
// { GOV_CMD_END_SELECT,                  " Shift+RButton      ButtonUp  " },
// { GOV_CMD_END_SELECT,                  " Ctrl+Shift+RButton ButtonUp  " },

// Cancel selection in progress
   { GOV_CMD_SELECT_CANCEL,               " Esc                KeyDown   " },

// Accept/Reject GOV
   { GOV_CMD_ACCEPT,                      " G                  KeyDown   " },
   { GOV_CMD_ACCEPT_IN_PLACE,             " Ctrl+G             KeyDown   " },
   { GOV_CMD_REJECT,                      " A                  KeyDown   " },

// Accept GOV and apply to all frames in marked range
   { GOV_CMD_ACCEPT_AND_APPLY_ALL,        " Shift+G            KeyDown   " },

// Cancel multiframe GOV in progress
   { GOV_CMD_STOP_ALL_PROCESSING,         " Ctrl+Space         KeyDown   " },

// Repeat GOV
   { GOV_CMD_REPEAT_LAST_GOV_OR_DRS,      " `                  KeyDown   " },
   { GOV_CMD_REPEAT_TOGGLE_GOV_OR_DRS,    " Shift+`            KeyDown   " },

// Toggle Pending GOV
   { GOV_CMD_TOGGLE_GOV,                  " T                  KeyDown   " },
   { GOV_CMD_TOGGLE_GOV_ZOOM,             " Shift+T            KeyDown   " },
////   { GOV_CMD_UNZOOM,                      " Z                  KeyDown   " },

// Commands for the Review Tool Buttons
   { GOV_CMD_REVIEW_PREV,                 " Insert             KeyDown   " },
   { GOV_CMD_REVIEW_PREV_ZOOM,            " Shift+Insert       KeyDown   " },
   { GOV_CMD_REVIEW_NEXT,                 " Del                KeyDown   " },
   { GOV_CMD_REVIEW_NEXT_ZOOM,            " Shift+Del          KeyDown   " },

};


static CUserInputConfiguration *govDefaultUserInputConfiguration
= new CUserInputConfiguration(sizeof(govDefaultConfigItems)
                              /sizeof(CUserInputConfiguration::SConfigItem),
                               govDefaultConfigItems);

// -------------------------------------------------------------------------
// GOV Command Table

static CToolCommandTable::STableEntry govCommandTableEntries[] =
{
   // GOV Command                         GOV Command String
   // -------------------------------------------------------------

   { GOV_CMD_START_SELECT_RECT,           "GOV_CMD_START_SELECT_RECT"       },
   { GOV_CMD_START_SELECT_RECT_ZOOM,      "GOV_CMD_START_SELECT_RECT_ZOOM"  },
   { GOV_CMD_START_SELECT_LASSO,          "GOV_CMD_START_SELECT_LASSO"      },
   { GOV_CMD_START_SELECT_LASSO_ZOOM,     "GOV_CMD_START_SELECT_LASSO_ZOOM" },
   { GOV_CMD_END_SELECT,                  "GOV_CMD_END_SELECT"              },
   { GOV_CMD_SELECT_CANCEL,               "GOV_CMD_SELECT_CANCEL"           },
   { GOV_CMD_ACCEPT,                      "GOV_CMD_ACCEPT"                  },
   { GOV_CMD_ACCEPT_IN_PLACE,             "GOV_CMD_ACCEPT_IN_PLACE"         },
   { GOV_CMD_ACCEPT_AND_APPLY_ALL,        "GOV_CMD_ACCEPT_AND_APPLY_ALL"    },
   { GOV_CMD_REJECT,                      "GOV_CMD_REJECT"                  },
   { GOV_CMD_STOP_ALL_PROCESSING,         "GOV_CMD_STOP_ALL_PROCESSING"     },
   { GOV_CMD_REPEAT_LAST_GOV_OR_DRS,      "GOV_CMD_REPEAT_LAST_GOV_OR_DRS"  },
   { GOV_CMD_REPEAT_TOGGLE_GOV_OR_DRS,    "GOV_CMD_REPEAT_TOGGLE_GOV_OR_DRS"},
   { GOV_CMD_TOGGLE_GOV,                  "GOV_CMD_TOGGLE_GOV"              },
   { GOV_CMD_TOGGLE_GOV_ZOOM,             "GOV_CMD_TOGGLE_GOV_ZOOM"         },
   { GOV_CMD_UNZOOM,                      "GOV_CMD_UNZOOM"                  },
   { GOV_CMD_REVIEW_PREV,                 "GOV_CMD_REVIEW_PREV"             },
   { GOV_CMD_REVIEW_PREV_ZOOM,            "GOV_CMD_REVIEW_PREV_ZOOM"        },
   { GOV_CMD_REVIEW_NEXT,                 "GOV_CMD_REVIEW_NEXT"             },
   { GOV_CMD_REVIEW_NEXT_ZOOM,            "GOV_CMD_REVIEW_NEXT_ZOOM"        },
};

static CToolCommandTable *govCommandTable
   = new CToolCommandTable(sizeof(govCommandTableEntries)
                           /sizeof(CToolCommandTable::STableEntry),
                           govCommandTableEntries);


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGOVTool::CGOVTool(const string &newToolName)
: CToolObject(newToolName, NULL)
{
   lassoCopy = new POINT[MAX_LASSO_PTS];    // Gag me

   // Tool Setup handles
   singleFrameToolSetupHandle = -1;
   multiFrameToolSetupHandle = -1;
   selectionRegionList = NULL;

   InitState();

   // Active tool must have exclusive control of this:
   tildeKeyHackMode = TILDE_KEY_HACK_OTHER_MODE;
}

CGOVTool::~CGOVTool()
{
   delete [] lassoCopy;
   delete selectionRegionList;
}

//////////////////////////////////////////////////////////////////////
// Built-in tools require a factory method
//////////////////////////////////////////////////////////////////////

CToolObject* CGOVTool::MakeTool(const string& newToolName)
{
   return new CGOVTool(newToolName);
}


// ===================================================================
//
// Function:    toolInitialize
//
// Description: Called when the ToolManager wants to initialize
//              the GOV Tool
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================

int CGOVTool::toolInitialize(CToolSystemInterface *newSystemAPI)
{
   int retVal;

   // Initialize the CGOVTool's base class, i.e., CToolObject
   // This must be done before the CGOVTool initializes itself
   retVal = initBaseToolObject(govToolNumber, newSystemAPI, govCommandTable,
                               govDefaultUserInputConfiguration);
   if (retVal != 0)
   {
      // ERROR: initBaseToolObject failed
      TRACE_0(errout << "Error in GOVTOOL, initBaseToolObject failed "
                     << retVal);
      return retVal;
   }

   InitState();

   return retVal;
}


// ===================================================================
//
// Function:    toolShutdown
//
// Description: Called when the Tool Manager wants to shut down the
//              GOV Tool, typically when the main program is
//              shutting down.
//
// Arguments:   None
//
// Returns:     If succeeded, returns 0.  Non-zero if an error
//
// ===================================================================

int CGOVTool::toolShutdown()
{
   int retVal;

   AcceptPendingGOV(false);
   DestroyAllToolSetups();

   // Shutdown the GOV Tool's base class, i.e., CToolObject.
   // This must be done after the GOV Tool shuts down itself
   retVal = shutdownBaseToolObject();
   if (retVal != 0)
      {
      // ERROR: shutdownBaseToolObject failed
      return retVal;
      }

   return retVal;
} // end toolShutdown

// ===================================================================

CToolProcessor* CGOVTool::makeToolProcessorInstance(
                                           const bool *newEmergencyStopFlagPtr)
{
   CToolProcessor *newToolProc;

   // Make a new tool processor, called by MakeSimpleToolSetup
   newToolProc =  new CGOVToolProc(GetToolNumber(), GetToolName(),
                                   getSystemAPI(), newEmergencyStopFlagPtr,
                                   GetToolProgressMonitor());

   return newToolProc;
}
//--------------------------------------------------------------------------

bool CGOVTool::toolHideQuery()
{
   // If a GOV is pending, accept it
   AcceptGOVIfPending();

   return true;
}
//--------------------------------------------------------------------------

int CGOVTool::toolActivate()
{
   return 0;        // Built-in tool, always active
}
//--------------------------------------------------------------------------

int CGOVTool::toolDeactivate()
{
   return 0;        // Built-in tool, always active
}
//--------------------------------------------------------------------------

int CGOVTool::onRedraw(int frameIndex)
{
  // Show yellow rectangle if GOV is in progress or is pending and we are
  // looking at the frame where the GOV is being applied.
  if ((govInProgress || govPending)
      && frameIndex == pendingGOVFrameIndex)
  {
     // Original portion of fix being shown, so draw the rectangle
     RECT tempRect = selectionRect;
     getSystemAPI()->setGraphicsColor(SOLID_WHITE);
     getSystemAPI()->drawRectangleFrame(&tempRect);
  }

  return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//--------------------------------------------------------------------------

int CGOVTool::onChangingClip()
{
   // This function is called just before a clip is unloaded.

   AcceptPendingGOV(false);
   DestroyAllToolSetups();

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//--------------------------------------------------------------------------

int CGOVTool::onNewClip()
{
   // This function is called after a new clip is loaded.

   InitState();

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//--------------------------------------------------------------------------

int CGOVTool::onChangingFraming()
{
   AcceptPendingGOV(false);
   DestroyAllToolSetups();

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//--------------------------------------------------------------------------

int CGOVTool::onToolProcessingStatus(const CAutotoolStatus& autotoolStatus)
{
   int retVal = TOOL_RETURN_CODE_NO_ACTION;

   if (multiframeGovInProgress)
   {
      if (autotoolStatus.status == AT_STATUS_STOPPED)
      {
         getSystemAPI()->DiscardGOVRevisions();  // In case user cancelled!
         govInProgress = multiframeGovInProgress = false;  // processing is done
         NotifyGOVDone();
      }

      // Call parent class' function
      CToolObject::onToolProcessingStatus(autotoolStatus);

      retVal = TOOL_RETURN_CODE_EVENT_CONSUMED;
   }
   else if (govInProgress)
   {
      // Make sure active & default tools don't get called!
      retVal = TOOL_RETURN_CODE_EVENT_CONSUMED;
   }

   return retVal;
}
//--------------------------------------------------------------------------

int CGOVTool::onMouseMove(CUserInput &userInput)
{
   int mouseX = userInput.windowX;
   int mouseY = userInput.windowY;

   if ((oldMouseX != mouseX || oldMouseY != mouseY) &&
       bSendStartNotifyOnNextMouseMove)
   {
      bool cancelStretch=true;
      if (!NotifyGOVStarting(&cancelStretch))
      {
         // Abort - active tool is locking us out
         mouseMode = GOV_MOUSE_MODE_NONE;
         if (cancelStretch == true)
         {
            getSystemAPI()->cancelStretchRectangle();
         }
      }

      bSendStartNotifyOnNextMouseMove = false;
   }
   oldMouseX = mouseX;
   oldMouseY = mouseY;

   // Always pass through the mouse move so panning will continue to work
   return TOOL_RETURN_CODE_NO_ACTION;
}

// ===================================================================
//
// Function:    onToolCommand
//
// Description: GOV Tool's Command Handler
//
// Arguments:   CToolCommand &toolCommand
//
// Returns:     Results of command handling as CToolObject's return codes,
//              TOOL_RETURN_CODE_*.  See ToolObject.h
//
// ===================================================================

int CGOVTool::onToolCommand(CToolCommand &toolCommand)
{
   int commandNumber = toolCommand.getCommandNumber();
   int result = TOOL_RETURN_CODE_NO_ACTION;
   int retVal = 0;

   // Dispatcher for GOV commands
   // There are five modes that the tool can be in:
   // 1) IDLE - looking for a right mouse down event
   // 2) STRETCHING - looking for a right mouse up event or cancel
   // 3) SINGLE FRAME PROCESSING - running GOV on a single frame
   // 4) GOV PENDING - looking to accept or reject the GOV
   // 5) MULTIFRAME PROCESSING - running GOV on marked range of frames

   // First handle the case where a multiframe GOV isd in progress -
   // the ONLY input recognizwd in that case is the Stop Processing command
   // (Ctrl+Space) !

   if (multiframeGovInProgress)
   {
      switch (commandNumber)
      {
         case GOV_CMD_STOP_ALL_PROCESSING:
            StopToolProcessing();
            break;
      }

      // Lock out other tools!!
      result = TOOL_RETURN_CODE_EVENT_CONSUMED;
   }

   // Second handle single frame processing in progress (lock out other tools)

   else if (govInProgress)
   {
      // Lock out other tools!!
      result = TOOL_RETURN_CODE_EVENT_CONSUMED;
   }

   // Third,  handle the mode where a single-frame GOV fix is pending

   else if (govPending)
   {
      switch (commandNumber)
      {
         case GOV_CMD_TOGGLE_GOV:
            retVal = TogglePendingGOV(false);
            result = TOOL_RETURN_CODE_EVENT_CONSUMED;
            break;

         case GOV_CMD_TOGGLE_GOV_ZOOM:
            getSystemAPI()->zoom(selectionRect);
            retVal = TogglePendingGOV(false);
            result = TOOL_RETURN_CODE_EVENT_CONSUMED;
            break;

         case GOV_CMD_UNZOOM:
            getSystemAPI()->unzoom();
            result = TOOL_RETURN_CODE_EVENT_CONSUMED;
            break;

         case GOV_CMD_ACCEPT:
            retVal = AcceptPendingGOV(true);   // True = Go to accepted frame
            result = TOOL_RETURN_CODE_EVENT_CONSUMED;
            break;

         case GOV_CMD_ACCEPT_IN_PLACE:
            retVal = AcceptPendingGOV(false); // False = Stay on current frame
            result = TOOL_RETURN_CODE_EVENT_CONSUMED;
            break;

         case GOV_CMD_ACCEPT_AND_APPLY_ALL:
            retVal = ApplyPendingGOVToMarkedRange();
            result = TOOL_RETURN_CODE_EVENT_CONSUMED;
            break;

         case GOV_CMD_REJECT:
            retVal = RejectPendingGOV(true);
            result = TOOL_RETURN_CODE_EVENT_CONSUMED;
            break;

         case GOV_CMD_START_SELECT_RECT:
         case GOV_CMD_START_SELECT_RECT_ZOOM:
         case GOV_CMD_START_SELECT_LASSO:
         case GOV_CMD_START_SELECT_LASSO_ZOOM:
         case GOV_CMD_REPEAT_LAST_GOV_OR_DRS:
         case GOV_CMD_REPEAT_TOGGLE_GOV_OR_DRS:
         case GOV_CMD_REVIEW_PREV:           // is this right?
         case GOV_CMD_REVIEW_PREV_ZOOM:      // is this right?
         case GOV_CMD_REVIEW_NEXT:           // is this right?
         case GOV_CMD_REVIEW_NEXT_ZOOM:      // is this right?
            retVal = AcceptPendingGOV(false);
            // No result because not consumed yet!
            break;

         default:
            // Not one of our commands -- no action at all -
            // it is up to the active tool to tell us to auto-accept
            // a pending fix
            break;
      }
   }
   else
   {
   }

   // If nothing was pending, or if something was pending and has been
   // auto-accepted, see if we need to start or stop stretching

   if (result != TOOL_RETURN_CODE_EVENT_CONSUMED)
   {
      if (mouseMode == GOV_MOUSE_MODE_STRETCH)
      {
         retVal = 0;
         switch (commandNumber)
         {
            case GOV_CMD_END_SELECT:
               retVal = EndGovSelect();
               break;

            case GOV_CMD_SELECT_CANCEL:
               mouseMode = GOV_MOUSE_MODE_NONE;
               getSystemAPI()->stopStretchRectangleOrLasso();
               NotifyGOVDone();
               break;

            default:
               retVal = -1;
               break;
         }

         // DO NOT consume the event - let other tools have a crack at it
         //result = TOOL_RETURN_CODE_EVENT_CONSUMED;
         // WTF - this messes up operation of GOV when DRS is active,
         // so put it back. Sucks. I don't like that buttonDown's are passed
         // through but not buttonUp's!!! QQQ
         // Aha - we'll pass the event through only if the EndGovSelect failed!!
         if (retVal == 0)
         {
            result = TOOL_RETURN_CODE_EVENT_CONSUMED;
         }
      }
      else // Nothing going on - see if we want to start something
      {
         // When we get a command to start stretching, we first ask the
         // active tool if it's OK to do GOV now, and if not, we don't
         // start it.
         switch (commandNumber)
         {
            case GOV_CMD_START_SELECT_RECT:
               retVal = BeginGovSelect(SELECTED_REGION_SHAPE_RECT, false);
               bSendStartNotifyOnNextMouseMove = true;
               // DO NOT consume the event - let other tools have a
               // crack at it (for example for Paint to implement
               // "quick erase" mode
               //result = TOOL_RETURN_CODE_EVENT_CONSUMED;
               break;

            case GOV_CMD_START_SELECT_RECT_ZOOM:
               retVal = BeginGovSelect(SELECTED_REGION_SHAPE_RECT, true);
               bSendStartNotifyOnNextMouseMove = true;
               // DO NOT consume the event - let other tools have a
               // crack at it
               //result = TOOL_RETURN_CODE_EVENT_CONSUMED;
               break;

            case GOV_CMD_START_SELECT_LASSO:
               retVal = BeginGovSelect(SELECTED_REGION_SHAPE_LASSO, false);
               bSendStartNotifyOnNextMouseMove = true;
               // DO NOT consume the event - let other tools have a
               // crack at it
               //result = TOOL_RETURN_CODE_EVENT_CONSUMED;
               break;

            case GOV_CMD_START_SELECT_LASSO_ZOOM:
               retVal = BeginGovSelect(SELECTED_REGION_SHAPE_LASSO, true);
               bSendStartNotifyOnNextMouseMove = true;
               // DO NOT consume the event - let other tools have a
               // crack at it
               //result = TOOL_RETURN_CODE_EVENT_CONSUMED;
               break;

            case GOV_CMD_REPEAT_LAST_GOV_OR_DRS:       // ~ key
               result = HandleTildeKeyHack(false); // unshifted
               break;

            case GOV_CMD_REPEAT_TOGGLE_GOV_OR_DRS:     // SHIFT-~
               result = HandleTildeKeyHack(true); // shifted
               break;

            case GOV_CMD_REVIEW_PREV:
               // Moved to Navigator tool
//               ReviewToolSingleStep(false, false);
//               result = TOOL_RETURN_CODE_EVENT_CONSUMED;
               break;

            case GOV_CMD_REVIEW_PREV_ZOOM:
               ReviewToolSingleStep(false, true);
               result = TOOL_RETURN_CODE_EVENT_CONSUMED;
               break;

            case GOV_CMD_REVIEW_NEXT:
               // Moved to Navigator tool
//               ReviewToolSingleStep(true, false);
//               result = TOOL_RETURN_CODE_EVENT_CONSUMED;
               break;

            case GOV_CMD_REVIEW_NEXT_ZOOM:
               ReviewToolSingleStep(true, true);
               result = TOOL_RETURN_CODE_EVENT_CONSUMED;
               break;

            default:
               break;
         }
      }
   }

   if (result == TOOL_RETURN_CODE_EVENT_CONSUMED)
   {
      if (retVal == 0)
         result |= TOOL_RETURN_CODE_PROCESSED_OK;
      else
         result |= TOOL_RETURN_CODE_ERROR;

      const CToolCommandTable *commandTable = getCommandTable();
      const char* commandName = commandTable->findCommandName(commandNumber);
      if (commandName != NULL)
         TRACE_2(errout << "Command: " << commandName << " (" << commandNumber << ")");
      else
         TRACE_2(errout << "GOV swallowed command " << commandNumber);
   }

   return result;
}
//--------------------------------------------------------------------------

// ===================================================================
//
// Function:    BeginGovSelect
//
// Description: Called when the user first clicks and holds the
//              right mouse button for drawing a gov rectangle
//              or lasso
//
// Arguments:   Selected shape (rect or lasso) and "auto-zoom" flag
//
// Returns:     We always claim to have successfully consumed the event
//
// ===================================================================

int CGOVTool::BeginGovSelect(ESelectedRegionShape shape, bool goZoom)
{
   // The mouseMode flag filters out auto-repeat on PC and
   // multiple key + mouse button presses
   // do nothing if invalid modes
   if (mouseMode != GOV_MOUSE_MODE_NONE)
      return 0;

   // Some global state
   selectionShape = shape;
   delayedZoom = goZoom;
   mouseMode = GOV_MOUSE_MODE_STRETCH;

   // Clear some crap  - not sure if these are needed
   getSystemAPI()->ClearReviewRegions(true);
   getSystemAPI()->ClearBezier(false);

   // Begin the RECT stretch or LASSO loop
   getSystemAPI()->setDRSLassoMode(selectionShape == SELECTED_REGION_SHAPE_LASSO);
   getSystemAPI()->setStretchRectangleColor(SOLID_WHITE);
   getSystemAPI()->startStretchRectangleOrLasso();

   return 0;
}

// ===================================================================
//
// Function:    EndGovSelect
//
// Description: Called to end the selection of a GOV rectangle or lasso.
//              When the user completes the rectangle or lasso, the
//              coordinates are collected and the GOV processed on the
//              current frame
//
// Arguments:   None
//
// Returns:     Always claims to have successfully cosumed the event
//
// ===================================================================
int CGOVTool::EndGovSelect()
{
   int result = 0;
   RECT imageRect;
   POINT *lassoPtr = NULL;

   // Filter out auto-repeat on PC and multiple key & mouse button presses
   if (mouseMode != GOV_MOUSE_MODE_STRETCH)
      return -1;

   // Sanitize the state
   mouseMode = GOV_MOUSE_MODE_NONE;
   bSendStartNotifyOnNextMouseMove = false;

   // Ask the Base System to stop the stretching rectangle,
   // then get the selection rectangle and maybe the lasso
   getSystemAPI()->stopStretchRectangleOrLasso();

   bool selectRegionAvailable = (0 == getSystemAPI()->
                                             getStretchRectangleCoordinates(
                                                        imageRect, lassoPtr));

   // If the selection wasn't canceled, process it
   if (selectRegionAvailable)
   {
      // Maybe zoom first
      if (delayedZoom)
      {
         getSystemAPI()->zoom(imageRect);
         delayedZoom = false;
      }

      // Copy the lasso locally if there is one (if not the lasso ptr
      // will be NULL and selectionLassoPtr will be set to NULL)
      selectionRect = imageRect;
      selectionLassoPtr = CopyLasso(lassoPtr);
      delete selectionRegionList;
      selectionRegionList = NULL;
      selectionShape = (selectionLassoPtr == NULL)?
                           SELECTED_REGION_SHAPE_RECT : SELECTED_REGION_SHAPE_LASSO;
      selectionCameFromUser = true;

      DoSingleFrameGOV();
   }
   else
   {
      // If I don't do this, the state gets royally fucked up
      getSystemAPI()->cancelStretchRectangle();

      if (govIsActive)
      {
         // Mouse had moved, so we had started a GOV but then canceled it
         NotifyGOVDone();
      }
      else
      {
         // Otherwise that means the mouse didn't move, so we want to
         // relay to the active tool that a right mouse click took place
         getSystemAPI()->NotifyRightClicked();
      }

      result = -1;
   }

   return result;
}
//--------------------------------------------------------------------------

bool CGOVTool::IsInConsumeAllInputMode()
{
   return ((mouseMode == GOV_MOUSE_MODE_STRETCH) || govInProgress);

}
//--------------------------------------------------------------------------

POINT* CGOVTool::CopyLasso(const POINT *srcLasso)
{
   // Make a copy of the  lasso

   if(srcLasso == NULL)
      return NULL;    // oops, no lasso, so just return NULL pointer

   for (int i = 0; ; ++i)
      {
      lassoCopy[i] = srcLasso[i];
      if (i > 2
          && srcLasso[i].x == srcLasso[0].x
          && srcLasso[i].y == srcLasso[0].y)
         break;
      }

   return lassoCopy;  // return ptr to preallocated lasso
}
//--------------------------------------------------------------------------

int CGOVTool::TogglePendingGOV(bool goZoom)
{
   getSystemAPI()->ToggleProvisional(goZoom);

   return 0;
}
//--------------------------------------------------------------------------

int CGOVTool::ApplyPendingGOVToMarkedRange()
{
   int retVal = 0;

   // Short circuit if nothing to rerun!
   if (!govPending)
      return retVal;

   // Proceed with rendering for Get Original Values, All Frames
   retVal = DoMultiFrameGOV();

   getSystemAPI()->setMainWindowFocus();

   return retVal;
}
//--------------------------------------------------------------------------

bool CGOVTool::IsGOVInProgress()
{
   return (govIsActive && !govPending); // was govInProgress; -- active
                                        // includes stretch but not pending
}
//--------------------------------------------------------------------------

bool CGOVTool::IsGOVPending()
{
   return govPending;
}
//--------------------------------------------------------------------------

// This one is PUBLIC:
void CGOVTool::AcceptGOVIfPending()
{
   AcceptPendingGOV(false);     // Don't go to GOV frame
}

// This one is PRIVATE
int CGOVTool::AcceptPendingGOV(bool goToFrame)
{
   int retVal = 0;

   // Short circuit if nothing to accept!
   if (!govPending)
      return retVal;

   // Haha this is nucking futs... ACCEPT GOV by REJECTING PROVISIONAL!
   getSystemAPI()->RejectProvisional(goToFrame);
   getSystemAPI()->ReviseAllGOVRegions(); // take restored bits out of history
   getSystemAPI()->refreshFrame();

   // For ~ key hack, we need to convery the new selection region to the
   // active tool (well, only DRS really cares...)
   lastActionWasGOV = true;
   if (selectionCameFromUser)
   {
      getSystemAPI()->NotifyGOVShapeChanged(selectionShape, selectionRect,
                                            selectionLassoPtr);
   }
   else
   {
      getSystemAPI()->NotifyGOVShapeChanged(SELECTED_REGION_SHAPE_NONE,
                                            selectionRect, NULL);
   }

   IToolProgressMonitor *progressMonitor = GetToolProgressMonitor();
   if (progressMonitor != NULL)
      progressMonitor->SetStatusMessage("GOV accepted");

   // OK, clear status and yellow box, and notify active tool that we're done
   govPending = false;
   getSystemAPI()->refreshFrameCached();
   NotifyGOVDone();

   return retVal;
}
//--------------------------------------------------------------------------
// This one is PUBLIC:
void CGOVTool::RejectGOVIfPending()
{
   RejectPendingGOV(false);     // Don't go to GOV frame
}

int CGOVTool::RejectPendingGOV(bool goToFrame)
{
   int retVal = 0;

   // Short circuit if nothing to reject!
   if (!govPending)
      return retVal;

   // Really stoopid - need to ACCEPT provisional to REJECT the GOV!!!
   getSystemAPI()->AcceptProvisional(goToFrame);
   getSystemAPI()->DiscardGOVRevisions();
   getSystemAPI()->refreshFrame();

   IToolProgressMonitor *progressMonitor = GetToolProgressMonitor();
   if (progressMonitor != NULL)
      progressMonitor->SetStatusMessage("GOV rejected");

   // OK, clear status and yellow box, and notify active tool that we're done
   govPending = false;
   getSystemAPI()->refreshFrameCached();
   NotifyGOVDone();

   return retVal;
}
//--------------------------------------------------------------------------

void CGOVTool::SetTildeKeyHackMode(ETildeKeyHackMode newMode)
{
// ****************** SIDE EFFECT ALERT *************************
   // When switching tools, we only want to remember the last
   // shape if the shape originated from a GOV operation!
   // If not in DRS, then whatever shape we've got must have
   // originated from a GOV operation - this could change if
   // we ever fix PAINT to pass along its change regions to us!
   // If in DRS mode, we check to see who went last.
   // UYPDATE: Paint now sends us its fix region!
   if ((tildeKeyHackMode != TILDE_KEY_HACK_OTHER_MODE) &&
       (!lastActionWasGOV))
   {
      selectionShape = SELECTED_REGION_SHAPE_NONE;
   }
// **************************************************************

   tildeKeyHackMode = newMode;
}
//--------------------------------------------------------------------------

void CGOVTool::SetGOVRepeatShape(ESelectedRegionShape shape,
                                 const RECT &rect,
                                 const POINT *lasso,
                                 const CPixelRegionList *regionList)
{
   MTIassert((shape == SELECTED_REGION_SHAPE_REGION)? (regionList != NULL) : true);
   MTIassert((shape == SELECTED_REGION_SHAPE_LASSO)? (lasso != NULL) : true);
   if ((shape == SELECTED_REGION_SHAPE_REGION && regionList == NULL) ||
       (shape == SELECTED_REGION_SHAPE_LASSO && lasso == NULL))
   {
      shape = SELECTED_REGION_SHAPE_NONE;
   }

   selectionShape = shape;
   selectionRect = rect;
   selectionLassoPtr = CopyLasso(lasso);
   if (shape != SELECTED_REGION_SHAPE_REGION)
   {
      delete selectionRegionList;
      selectionRegionList = NULL;
   }
   else
   {
      if (selectionRegionList == NULL)
      {
         selectionRegionList = new CPixelRegionList(*regionList);
      }
      else
      {
         // Optimizated if the region list structures match
         *selectionRegionList = *regionList;
      }

      // Well, this really sucks - the bounding box for some reason
      // is WAY BIGGER than it should be, so recompute it
      //selectionRect = selectionRegionList->GetBoundingBox();
      selectionRect.left = selectionRect.top = 0x7FFFFFFF;
      selectionRect.right = selectionRect.bottom = 0;
      for (int i=0; i<selectionRegionList->GetEntryCount();i++)
      {
         CPixelRegion *rgn = selectionRegionList->GetEntry(i);
         switch(rgn->GetType())
         {
            case PIXEL_REGION_TYPE_POINT:
            {
               CPointPixelRegion *pntrgn = static_cast<CPointPixelRegion*>(rgn);
               POINT pnt = pntrgn->GetPoint();
               if (pnt.x < selectionRect.left)   selectionRect.left   = pnt.x;
               if (pnt.x > selectionRect.right)  selectionRect.right  = pnt.x;
               if (pnt.y < selectionRect.top)    selectionRect.top    = pnt.y;
               if (pnt.y > selectionRect.bottom) selectionRect.bottom = pnt.y;
            }
            break;

            case PIXEL_REGION_TYPE_RECT:
            {
               CRectPixelRegion  *rctrgn = static_cast<CRectPixelRegion*>(rgn);
               RECT rct = rctrgn->GetRect();
               if (rct.left   < selectionRect.left)   selectionRect.left   = rct.left;
               if (rct.right  > selectionRect.right)  selectionRect.right  = rct.right;
               if (rct.top    < selectionRect.top)    selectionRect.top    = rct.top;
               if (rct.bottom > selectionRect.bottom) selectionRect.bottom = rct.bottom;
            }
            break;

            case PIXEL_REGION_TYPE_MASK:
            {
               CMaskPixelRegion *mskrgn;
               MTI_UINT16 *run;

               mskrgn = static_cast<CMaskPixelRegion*>(rgn);
               mskrgn->OpenRuns();
               while ((run=mskrgn->GetNextRun())!=NULL)
               {
                  MTI_UINT16 xLeft = run[0];
                  MTI_UINT16 xRight = xLeft + run[2];
                  MTI_UINT16 y = run[1];

                  selectionRect.left   = std::min<int>(selectionRect.left, xLeft);
                  selectionRect.right  = std::max<int>(selectionRect.right, xRight);
                  selectionRect.top    = std::min<int>(selectionRect.top, y);
                  selectionRect.bottom = std::max<int>(selectionRect.bottom, y);
               }

               // Regression test!
               const CImageFormat *clipImageFormat = getSystemAPI()->getVideoClipImageFormat();
               int frameWidth = clipImageFormat->getPixelsPerLine();
               int frameHeight = clipImageFormat->getLinesPerFrame();
               MTIassert(selectionRect.right < frameWidth);
               MTIassert(selectionRect.left <=selectionRect.right);
               MTIassert(selectionRect.bottom < frameHeight);
               MTIassert(selectionRect.top <=selectionRect.bottom);
            }
            break;

            case PIXEL_REGION_TYPE_LASSO:
            {
               CLassoPixelRegion *lasrgn = static_cast<CLassoPixelRegion*>(rgn);
               RECT  rct = lasrgn->GetRect();
               if (rct.left   < selectionRect.left)   selectionRect.left   = rct.left;
               if (rct.right  > selectionRect.right)  selectionRect.right  = rct.right;
               if (rct.top    < selectionRect.top)    selectionRect.top    = rct.top;
               if (rct.bottom > selectionRect.bottom) selectionRect.bottom = rct.bottom;
            }
            break;

            case PIXEL_REGION_TYPE_BEZIER:
            {
               CBezierPixelRegion *bezrgn = static_cast<CBezierPixelRegion*>(rgn);
               RECT  rct = bezrgn->GetRect();
               if (rct.left   < selectionRect.left)   selectionRect.left   = rct.left;
               if (rct.right  > selectionRect.right)  selectionRect.right  = rct.right;
               if (rct.top    < selectionRect.top)    selectionRect.top    = rct.top;
               if (rct.bottom > selectionRect.bottom) selectionRect.bottom = rct.bottom;
            }
            break;
         }

      } // end loop thru list elements
   }
   selectionCameFromUser = false;

// ***************** SIDE EFFECT ALERT !!! ***********************
   lastActionWasGOV = false;
// ***************************************************************
}

int CGOVTool::HandleTildeKeyHack(bool shifted)
{
   int result = TOOL_RETURN_CODE_NO_ACTION;

   // ALWAYS let paint handle naked tilde!
   if (shifted == false && tildeKeyHackMode == TILDE_KEY_HACK_PAINT_MODE)
   {
      return result;
   }

   // CAREFUL! Need to do this first in case the first DRS
   // fix is pending - then it will accept and tell us what
   // the shape of the fix is!

   // CAREFUL! If PAINT is active, don't "start GOV" unless we're sure we'll
   // want to go through with it because of nasty side effects!!!

   if (tildeKeyHackMode != TILDE_KEY_HACK_PAINT_MODE &&
       NotifyGOVStarting() == false)
   {
      // Active tool told us to not proceed
      return result;
   }

   if (selectionShape == SELECTED_REGION_SHAPE_NONE)
   {
      // There is nothing to repeat!
      NotifyGOVDone();
      return result;
   }

   bool isCorrectKey =
      // SHIFT tilde repeats GOV in Paint mode (note we screened out
      // the unshifted case above)
      (tildeKeyHackMode == TILDE_KEY_HACK_PAINT_MODE)
      ||
      // If not in DRS or PAINT, tilde key repeats GOV whether
      // or not it is shifted
      (tildeKeyHackMode == TILDE_KEY_HACK_OTHER_MODE)
      ||
      // UNSHIFTED tilde key repeats GOV if in DRS mode and GOV was done last
      ((!shifted) && lastActionWasGOV)
      ||
      // SHIFTED tilde key repeats GOV if in DRS mode and DRS was done last
      (shifted && (!lastActionWasGOV));

   if (tildeKeyHackMode != TILDE_KEY_HACK_PAINT_MODE &&
       isCorrectKey == false)
   {
      // Not for us... abort
      NotifyGOVDone();
   }
   else
   {
      // OK, we got the right command, do GOV with the last
      // GOV or DRS region; it's now SAFE to tell Paint that GOV is starting!
      if (tildeKeyHackMode != TILDE_KEY_HACK_PAINT_MODE ||
          NotifyGOVStarting() == true)
      {
         getSystemAPI()->ClearReviewRegions(true);     // ??
         result = DoSingleFrameGOV();
         result = TOOL_RETURN_CODE_EVENT_CONSUMED;
      }
   }

   return result;
}
//--------------------------------------------------------------------------

void CGOVTool::SetGOVToolProgressMonitor(
                          IToolProgressMonitor *newToolProgressMonitor)
{
   SetToolProgressMonitor(newToolProgressMonitor);
}
//--------------------------------------------------------------------------

void CGOVTool::InitState()
{
  // Reset the GOVState
   selectionShape = SELECTED_REGION_SHAPE_NONE;
   selectionRect.top = 0;
   selectionRect.bottom = 0;
   selectionRect.left = 0;
   selectionRect.right = 0;
   selectionLassoPtr = NULL;
   selectionBezierPtr = NULL;
   delete selectionRegionList;
   selectionRegionList = NULL;
   selectionCameFromUser = false;
   delayedZoom = false;
   mouseMode = GOV_MOUSE_MODE_NONE;
   govInProgress = false;
   multiframeGovInProgress = false;
   govPending = false;
   pendingGOVFrameIndex = -1;
   lastActionWasGOV = false;
   // NO! Active tool Tool must control this!!
   //tildeKeyHackMode = TILDE_KEY_HACK_OTHER_MODE;
   govIsActive = false;
   bSendStartNotifyOnNextMouseMove = false;
   oldMouseX = -1;
   oldMouseY = -1;
}
//--------------------------------------------------------------------------

bool CGOVTool::NotifyGOVStarting(bool *cancelStretchRect)
{
   bool retVal = true;

   // rigmarole because Paint gets confused if we double-start
   if (!govIsActive)
   {
      MTIassert(!govInProgress);
      retVal = getSystemAPI()->NotifyGOVStarting(cancelStretchRect);
      if (retVal)
      {
         govIsActive = true;
      }
   }

   return retVal;
}
//--------------------------------------------------------------------------

void CGOVTool::NotifyGOVDone()
{
   if (govIsActive)
   {
      MTIassert(!govInProgress);
      getSystemAPI()->NotifyGOVDone();
      govIsActive = false;
   }
}

//--------------------------------------------------------------------------

void CGOVTool::DestroyAllToolSetups()
{
   // GOV has two tool setups: singleFrame and multiFrame
   // This function should not be called unless all of the tool setups
   // are stopped and there is no provisional pending.

   int activeToolSetup = getSystemAPI()->GetActiveToolSetupHandle();

   if (singleFrameToolSetupHandle >= 0)
   {
      if (singleFrameToolSetupHandle == activeToolSetup)
         getSystemAPI()->SetActiveToolSetup(-1);
      getSystemAPI()->DestroyToolSetup(singleFrameToolSetupHandle);
      singleFrameToolSetupHandle = -1;
   }

   if (multiFrameToolSetupHandle >= 0)
   {
      if (multiFrameToolSetupHandle == activeToolSetup)
         getSystemAPI()->SetActiveToolSetup(-1);
      getSystemAPI()->DestroyToolSetup(multiFrameToolSetupHandle);
      multiFrameToolSetupHandle = -1;
   }
}
//--------------------------------------------------------------------------

int CGOVTool::RenderSingleFrame()
{
   int retVal = 0;
   string errorFunc;

   CBusyCursor busyCursor(true);   // Busy cursor until busyCursor goes
                                   // out-of-scope, e.g., this function returns
   govInProgress = true;

   // We'll need to remember which frame exacrly contains pending GOV,
   // in case we move off it before accepting it -- wtf, I'm going to change
   // it so if you move away from the frame it accepts then
   pendingGOVFrameIndex = getSystemAPI()->getLastFrameIndex();

   // Check if we have a tool setup yet
   if (singleFrameToolSetupHandle < 0)
   {
      // Create the tool setup
      CToolIOConfig ioConfig(1, 1);
      singleFrameToolSetupHandle =
             getSystemAPI()->MakeSimpleToolSetup(GetToolName(),
                                                 TOOL_SETUP_TYPE_SINGLE_FRAME,
                                                 &ioConfig);
      if (singleFrameToolSetupHandle < 0)
      {
         retVal = -1;
         govInProgress = false;
         return retVal;
      }
   }

   // Set the active tool setup (this is different than the active tool)
   retVal = getSystemAPI()->SetActiveToolSetup(singleFrameToolSetupHandle);
   if (retVal != 0)
   {
      govInProgress = false;
      return retVal;
   }

   // Set processing frame range
   CToolFrameRange frameRange(1, 1);
   frameRange.inFrameRange[0].randomAccess = true;
   frameRange.inFrameRange[0].inFrameIndex = pendingGOVFrameIndex;
   frameRange.inFrameRange[0].outFrameIndex = pendingGOVFrameIndex + 1;
   retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
   if (retVal != 0)
   {
      govInProgress = false;
      return retVal;
   }

   CGOVToolParameters *govToolParams = new CGOVToolParameters;
   retVal = GatherGOVToolParameters(*govToolParams);
   if (retVal != 0)
   {
      govInProgress = false;
      return retVal;
   }

   // Note: system will delete the CToolParameter instance
   //       when it is done with it.
   retVal = getSystemAPI()->SetToolParameters(govToolParams);
   if (retVal != 0)
   {
      govInProgress = false;
      return retVal;
   }

   getSystemAPI()->goToFrameSynchronous(pendingGOVFrameIndex);

   // Some day I will figure out what these do
   getSystemAPI()->SetProvisionalReview(true);
   getSystemAPI()->SetProvisionalReprocess(false);

   // Start the image processing a-runnin'
   getSystemAPI()->RunActiveToolSetup();

   // Wait for processing to complete
   getSystemAPI()->WaitForToolProcessing();

   // Update tool state
   govPending = true;
   govInProgress = false;

   return retVal;
}
//--------------------------------------------------------------------------

int CGOVTool::RunFrameProcessing(int newResumeFrame)
{
   int retVal;
   string errorFunc;

   govInProgress = true;

   // Run multi-frame processing to render Get Original Values, All Frames
   // Check if we have a tool setup yet
   if (multiFrameToolSetupHandle < 0)
   {
      // Create the tool setup
      CToolIOConfig ioConfig(1, 1);
      multiFrameToolSetupHandle =
                             getSystemAPI()->MakeSimpleToolSetup(GetToolName(),
                                                   TOOL_SETUP_TYPE_MULTI_FRAME,
                                                   &ioConfig);
      if (multiFrameToolSetupHandle < 0)
      {
         retVal = -1;
         govInProgress = false;
         return retVal;
      }
   }

   // Set the active tool setup (this is different than the active tool)
   retVal = getSystemAPI()->SetActiveToolSetup(multiFrameToolSetupHandle);
   if (retVal != 0)
   {
      govInProgress = false;
      return retVal;
   }

   // Huh - why the heck wouldn't it be stopped??
   if (getSystemAPI()->GetActiveToolSetupStatus() == AT_STATUS_STOPPED)
   {
      // Set processing frame range to in and out marks
      CToolFrameRange frameRange(1, 1);
      frameRange.inFrameRange[0].randomAccess = false;
      frameRange.inFrameRange[0].inFrameIndex = getSystemAPI()->getMarkIn();
      frameRange.inFrameRange[0].outFrameIndex = getSystemAPI()->getMarkOut();
      retVal = getSystemAPI()->SetToolFrameRange(&frameRange);
      if (retVal != 0)
      {
         govInProgress = false;
         return retVal;
      }
   }

   // Set to render
   getSystemAPI()->SetProvisionalRender(true);

   // Set parameters
   CGOVToolParameters *toolParams = new CGOVToolParameters;
   retVal = GatherGOVToolParameters(*toolParams);
   if (retVal != 0)
   {
      govInProgress = false;
      return retVal;
   }
   retVal = getSystemAPI()->SetToolParameters(toolParams);
   if (retVal != 0)
   {
      govInProgress = false;
      return retVal;
   }

   // Start the image processing a-runnin'
   multiframeGovInProgress = true;
   getSystemAPI()->RunActiveToolSetup();

   return 0;

} // RunFrameProcessing
//--------------------------------------------------------------------------

int CGOVTool::GatherGOVToolParameters(CGOVToolParameters &toolParams)
{
   SGOVParameters &govParams = toolParams.govParams;

   govParams.selectionShape = (selectionRegionList != NULL)?
                                          SELECTED_REGION_SHAPE_REGION :
                                 ((selectionLassoPtr != NULL)?
                                             SELECTED_REGION_SHAPE_LASSO :
                                                SELECTED_REGION_SHAPE_RECT);
   govParams.selectionRect = selectionRect;
   govParams.selectionLasso = selectionLassoPtr;
   govParams.selectionRegionList = selectionRegionList;

   return 0;
}
//--------------------------------------------------------------------------

int CGOVTool::DoSingleFrameGOV()
{
   int retVal;

   // Redraw the rectangle - not sure if this is needed
   getSystemAPI()->setGraphicsColor(DASHED_YELLOW);
   getSystemAPI()->drawRectangleFrame(&selectionRect);

   retVal = RenderSingleFrame();

   return retVal;
}
//--------------------------------------------------------------------------

int CGOVTool::DoMultiFrameGOV()
{
   int retVal;

   // First check Mark In and Mark Out
   int markIn = getSystemAPI()->getMarkIn();
   int markOut = getSystemAPI()->getMarkOut();
   if (markIn < 0 || markOut < 0 || markOut <= markIn)
   {
      // Marks are bogus, so complain to user and refuse to do anything
      _MTIErrorDialog("Cannot get original values for marked range    \n"
                      "because the marks are not valid");
      return GENERIC_ERROR;
   }

   // Next have user confirm intentions
   MTIostringstream os;
   int nFrames = markOut - markIn;
   os << "You are about to restore original values for "
      << nFrames << " frame"  << ((nFrames == 1)? "" : "s") << ".   \n"
      << "You cannot undo this operation -- do you wish to proceed?    ";
   retVal = _MTIWarningDialog(os.str());
   if (retVal != MTI_DLG_OK)
      return 0;

   // THE FOLLOWING LINE IS NOT RIGHT IF THE PROVISIONAL FRAME INDEX LIES
   // OUTSIDE THE RANGE markIn to markOut !!! QQQ

   // REJECT the frame we just did (by ACCEPTING the Provisional!)
   // because we will re-do this frame as part of the marked range
   // sequence
   getSystemAPI()->AcceptProvisional(true);  // true = go to frame - WHY?
   govPending = false;

   // Want these cleared to start
   getSystemAPI()->DiscardGOVRevisions();

   // Redraw the rectangle - not sure if this is needed
   getSystemAPI()->setGraphicsColor(DASHED_YELLOW);
   getSystemAPI()->drawRectangleFrame(&selectionRect);

   // Do it!
   //RunFrameProcessing(-1);
   // NO - need to go through tool infrastructure so STOP command will work!
   StartToolProcessing(TOOL_PROCESSING_CMD_RENDER, false);

   return retVal;
}

// ===================================================================
//
// Function:    TryToResolveProvisional
//
// Description: We are given a chance to try to auto accept before
//              the system puts up an obnoxious dialog
//
// Arguments:   None
//
// Returns:     TOOL_RETURN_CODE_NO_ACTION if we didn't have a fix pending,
//              else TOOL_RETURN_CODE_EVENT_CONSUMED
//
// ===================================================================
int CGOVTool::TryToResolveProvisional()
{
   if (govPending)
   {
      AcceptPendingGOV(false);
      return TOOL_RETURN_CODE_EVENT_CONSUMED;
   }

   return TOOL_RETURN_CODE_NO_ACTION;
}

//////////////////////////////////////////////////////////////////////
//  GOV Review Tool Functions
//////////////////////////////////////////////////////////////////////

void CGOVTool::ReviewToolSingleStep(bool goNext, bool goZoom)
{
   int retVal;

   if (mouseMode == GOV_MOUSE_MODE_STRETCH)
      return;

   // Check that nothing pending and processing is not running
   if (getSystemAPI()->CheckProvisionalUnresolvedAndToolProcessing())
      return;

   int frameIndex = getSystemAPI()->getLastFrameIndex();

   // Display the next or previous saved region
   //retVal = getSystemAPI()->DisplaySingleReviewRegion(goNext, goZoom, frameIndex);
   // Per Dave:
   // Just go to the next (or previous) frame with a fix
   retVal = getSystemAPI()->GoToNextOrPreviousReviewFrame(goNext, frameIndex);
   if (retVal != 0)
   {
      // ERROR
      // TTT Tell user about error
      return;
   }
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// TOOL PROCESSOR
///////////////////////////////////////////////////////////////////////////////

CGOVToolProc::CGOVToolProc(int newToolNumber, const string &newToolName,
                                 CToolSystemInterface *newSystemAPI,
                                 const bool *newEmergencyStopFlagPtr,
                                 IToolProgressMonitor *newToolProgressMonitor)
 : CToolProcessor(newToolNumber, newToolName, 1, 1, newSystemAPI,
                  newEmergencyStopFlagPtr, newToolProgressMonitor)
{
   StartProcessThread();
}

CGOVToolProc::~CGOVToolProc()
{
}

///////////////////////////////////////////////////////////////////////////////
//
///////////////////////////////////////////////////////////////////////////////

int CGOVToolProc::SetIOConfig(CToolIOConfig *toolIOConfig)
{
   SetInputMaxFrames(0, 1, 1);   // Input Port 0 = 1 frame
   SetOutputModifyInPlace(0, 0, false);
   SetOutputMaxFrames(0, 1);

   return 0;
}

int CGOVToolProc::SetFrameRange(CToolFrameRange *toolFrameRange)
{
   CToolFrameRange::SFrameRange &frameRange = toolFrameRange->inFrameRange[0];
   inFrameIndex = frameRange.inFrameIndex;
   outFrameIndex = frameRange.outFrameIndex;
   int frameCount = outFrameIndex - inFrameIndex;

   IToolProgressMonitor *progressMonitor = GetToolProgressMonitor();
   if (progressMonitor != NULL)
   {
      progressMonitor->SetStatusMessage("Restoring original values for all marked frames");
      if (frameCount > 1)
         progressMonitor->SetFrameCount(outFrameIndex - inFrameIndex);
   }

   return 0;
}

int CGOVToolProc::SetParameters(CToolParameters *toolParams)
{
   CGOVToolParameters *govToolParams
                             = static_cast<CGOVToolParameters*>(toolParams);

   // Remember the GOV parameters
   govParams = govToolParams->govParams;

   return 0;
}


int CGOVToolProc::BeginProcessing(SToolProcessingData &procData)
{
   // Review tool does not save to history, since it is restoring
   // a frame from the history
   SetSaveToHistory(false);

   IToolProgressMonitor *progressMonitor = GetToolProgressMonitor();
   if (progressMonitor != NULL)
   {
     progressMonitor->StartProgress();
   }

   return 0;
}

int CGOVToolProc::BeginIteration(SToolProcessingData &procData)
{
   int inputFrameIndexList[1];
   inputFrameIndexList[0] = inFrameIndex + procData.iteration;
   // inputFrameIndexList now contains the frame indices we want as input
   SetInputFrames(0, 1, inputFrameIndexList);

   // These are the frames that will be the output
   int outputFrameIndexList[1];
   outputFrameIndexList[0] = inFrameIndex + procData.iteration;
   SetOutputFrames(0, 1, outputFrameIndexList);

   return 0;
}

int CGOVToolProc::GetIterationCount(SToolProcessingData &procData)
{
   return outFrameIndex - inFrameIndex;
}

int CGOVToolProc::DoProcess(SToolProcessingData &procData)
{
   int retVal = 0;

   CToolFrameBuffer *inToolFrameBuffer = GetRequestedInputFrame(0, 0);

   // Force the buffer to be written to the disk, even though it looks
   // like to the tool media module that no pixels were changed!
   if (inToolFrameBuffer != NULL)
      inToolFrameBuffer->SetForceWrite(true);  // I guess IN is OUT as well?

   retVal = GetSystemAPI()->RestoreToolFrameBuffer(inToolFrameBuffer,
                                                   &govParams.selectionRect,
                                                   govParams.selectionLasso,
                                                   NULL,  // never bezier
                                                   govParams.selectionRegionList);
   if (retVal != 0)
      {
      CAutoErrorReporter autoErr("CGOVToolProc::DoProcess",
                              AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
      autoErr.errorCode = retVal;
      autoErr.msg << "RestoreToolFrameBuffer in DoProcess failed, Error: "
                  << retVal;
      }

   return retVal;
}

int CGOVToolProc::EndIteration(SToolProcessingData &procData)
{
   // Update the status
   IToolProgressMonitor *progressMonitor = GetToolProgressMonitor();
   if (progressMonitor != NULL)
   {
     progressMonitor->BumpProgress();
   }

   return 0;
}

int CGOVToolProc::EndProcessing(SToolProcessingData &procData)
{
   // Update the status
   IToolProgressMonitor *progressMonitor = GetToolProgressMonitor();
   if (progressMonitor != NULL)
   {
      int iterCount = GetIterationCount(procData);
      progressMonitor->SetStatusMessage((iterCount > 1)? "GOV complete"
                                                       : "GOV pending");
      progressMonitor->StopProgress(procData.iteration == iterCount);
   }

   return 0;
}
///////////////////////////////////////////////////////////////////////////////
