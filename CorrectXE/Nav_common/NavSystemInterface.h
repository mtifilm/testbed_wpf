
//---------------------------------------------------------------------------

#ifndef NavSystemInterfaceH
#define NavSystemInterfaceH

// NavSystemInterface.h:  Interface to Navigator base system from Tool
//                        objects
//
//  This class provides an interface by which a Tool can call functions
//  in the Navigator Base System.  The purpose of this class is to implement
//  the interface defined by the Abstract Base Class CToolSystemInterface.
/*
$Header: /usr/local/filmroot/Nav_common/include/NavSystemInterface.h,v 1.119.2.71 2009/11/01 00:41:31 tolks Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "ToolSystemInterface.h"

// Ugly recursive header but we need the #defines
#include "NavigatorTool.h"
//////////////////////////////////////////////////////////////////////
// Forward Declarations

#ifdef __sgi
class MainWindow;
#endif // #ifdef __sgi

class CProvisional;
class CHistoryReviewList;
class CMagnifier;
class CSaveRestore;
class CClip;
class CBezierEditor;
class CTransformEditor;
class CPaintTool;
class CPDL;
class CPDLEntry;
class CPixelRegionList;
class TMTIForm;
class TMainWindow;
class CTimeLine;
class GrainPresets;

//////////////////////////////////////////////////////////////////////

class CNavSystemInterface : public CToolSystemInterface
{
public:
   CNavSystemInterface();
   ~CNavSystemInterface();

   // Stretch Box API
   virtual int  setGraphicsColor(int);
   virtual void setStretchRectangleColor(int);
   virtual void startStretchRectangleOrLasso();
   virtual void stopStretchRectangleOrLasso(bool dspfrm=true);
   virtual void cancelStretchRectangle();
   virtual int getStretchRectangleCoordinates(RECT &imageRect, POINT* &lasso);
   virtual RECT getStretchRectangleScreenCoords();
   virtual int getLastFrameIndex();
   virtual void drawRectangleFrame(RECT *);
   virtual void drawLassoFrame(POINT *);
   virtual void drawBezierFrame(BEZIER_POINT *);
   virtual void lockReader(bool);
   virtual bool getMousePositionFrame(int *newx, int *newy);
   virtual void clipRectangleFrame(RECT *);
   virtual void clipLassoFrame(POINT *);

   // Zoom API
   virtual void zoom(const RECT &showRect);
   virtual void unzoom();

   // Color Mask API
   virtual void setRGBMask(int);
   virtual int  getRGBMask();

   // Paint API
   virtual int  getDisplayMode();
   virtual void setDisplayMode(int);
   virtual void resetDisplayMode();
   virtual void enterPaintMode();
   virtual void initPaintHistory();
   virtual void enableAutoAccept(bool);
   virtual void initImportProxy(int, int);
   virtual void exitPaintMode();
   virtual void executeNavigatorToolCommand(int);
   virtual void displayFrameAndBrush();
   virtual void setCaptureMode(int);
   virtual int  getCaptureMode();
   virtual void setImportMode(int);
   virtual  int getImportMode();
   virtual  int setImportFrameClipName(const string &clipName);
   virtual string getImportFrameClipName();
   virtual ClipSharedPtr getImportClip();
   virtual CVideoFrameList* getImportClipVideoFrameList();
   virtual  int computeDesiredImportFrame(int);
   virtual void setImportFrameOffset(int);
   virtual  int getImportFrameOffset();
   virtual int syncImportFrameTimecode();
   virtual void preloadImportFrame(int, int,
                                   double, double, double,
                                   double, double, double);
   virtual void forceTargetAndImportFrameLoaded();
   virtual void loadTargetAndImportFrames(int);
   virtual void setImportFrameTimecode(int);
   virtual  int getImportFrameTimecode();
   virtual void clearPaintFrames();
   virtual bool getPaintFrames(int *trg, int *imp);
   virtual void unloadTargetAndImportFrames();
   virtual void enableForceTargetLoad(bool);
   virtual void enableForceImportLoad(bool);
   virtual void loadTargetFrame(int);
   virtual void setTargetMark();
   virtual void reloadModifiedTargetFrame();
   virtual void redrawTargetFrame();
   virtual void reloadModifiedImportFrame();
   virtual void reloadUnmodifiedTargetFrame();
   virtual MTI_UINT16 *getTargetIntermediate();
   virtual MTI_UINT16 *getTargetOriginalIntermediate();
   virtual MTI_UINT16 *getTargetPreStrokeIntermediate();
   virtual bool targetFrameIsModified();
   virtual void loadImportFrame(int);
   virtual void setImportMark();
   virtual void setOnionSkinMode(int);
   virtual void setOnionSkinTargetWgt(double);
   virtual void editMask();
   virtual void setMouseMinorState(int);
   virtual void setColorPickSampleSize(int);
   virtual void getColorPickSample(int, int, int&, float&, float&, float&);
   virtual void clearFillSeed();
   virtual void setFillTolerance(int);
   virtual void recycleFillSeed();
   virtual void setDensityStrength(int);
   virtual void setGrainStrength(int);
   virtual void setGrainPresets(GrainPresets *grainPresets);
   virtual void moveImportFrame(int, int);
   virtual void setMainWindowFocus();
   virtual TForm *getBaseToolForm();
   virtual void setMainWindowAutofocus(bool onoff);
   virtual void enableBrushDisplay(bool);
   virtual void loadPaintBrush(CBrush *, int, int, int, float *);
   virtual CBrush *getPaintBrush(int *, int *, bool *, float *);
   virtual void loadCloneBrush(CBrush *, bool, int, int);
   virtual CBrush *getCloneBrush(bool *, int *, int *);
   virtual void unloadPaintBrush();
   virtual void unloadCloneBrush();
   virtual void setAutoAlignBoxSize(int);
   virtual void setDisplayProcessed(bool);
   virtual void mouseDn();
   virtual void mouseMv(int, int);
   virtual void mouseUp();
   virtual CPixelRegionList& getOriginalPixels();
   virtual int  getToolFrame(MTI_UINT16 *, int);
   virtual int  putToolFrame(MTI_UINT16 *, MTI_UINT16 *, CPixelRegionList&, int);
   virtual int  putToolFrame(MTI_UINT16 *, MTI_UINT16 *, MTI_UINT16 *, int);
   virtual void makeStrokeStack();
   virtual void clearStrokeStack();
   virtual int pushStrokeFromTarget(int);
   virtual int popStrokesToTarget(int);
   virtual void completeMacroStroke();
   virtual void paintOneBrushPositionFrame(int, int);
   virtual void paintOneStrokeSegmentFrame(int, int, int, int);
   virtual int initMaskForInpainting();
   virtual int iterateOneInpaintCycle();
   virtual int getMaxComponentValue();
   virtual void forceMouseToWindowCenter();
   virtual void useTrackingAlignOffsets(bool);
   virtual void clearAllTrackingAlignOffsets();
   virtual void setTrackingAlignOffsets(int frame, double xoff, double yoff);
   virtual void getTrackingAlignOffsets(int frame, double &xoff, double &yoff);
   virtual bool doesFrameHaveAnyHistory(int frame);
	virtual void setAltClipDiffThreshold(float newThreshold);
	virtual void setOrigValDiffThreshold(float newThreshold);
	virtual void setPaintToolChangesPending(bool flag);
	virtual bool arePaintToolChangesPending();

   // Registration API
   virtual void unloadRegistrationFrame();
   virtual void loadRegistrationFrame(int);
   virtual void clearLoadedRegistrationFrame();
   virtual void clearRegisteredRegistrationFrame();
   virtual MTI_UINT16 *getPreRegisteredIntermediate();
   virtual MTI_UINT16 *getRegisteredIntermediate();
   virtual void displayFromRenderThread(MTI_UINT16 *, int);
   virtual void enterRegistrationMode();
   virtual void exitRegistrationMode();
   virtual void SetMatBox(RECT *);
   virtual void GetMatBox(RECT *);
   virtual void SetRegistrationTransform(AffineCoeff *);
   virtual void GetRegistrationTransform(AffineCoeff *);

   // DeWarp API
   virtual void getLastFrameAsIntermediate(MTI_UINT16 *);

   // Magnifier API
   virtual CMagnifier * allocateMagnifier(int wdth, int hght, RECT *magwin);
   virtual void freeMagnifier(CMagnifier *mag);

   // Timeline API
   virtual CTimeLine *getClipTimeline();
   virtual void getTimelineVisibleFrameIndexRange(int &inIndex, int &exclusiveOutIndex);
	virtual void getCutList(vector<int> &cutList);
   virtual int getShowAlphaState(); // Tri-state (-1 = disabled, 0 = off, 1 = on)
   virtual void setShowAlphaState(bool newState);

   // Accessors and Mutators for Keyboard and Mouse Event Handler
   // Function Pointers
#ifndef NO_GUI_HACK
   virtual MTIKeyEventHandler getKeyDownEventHandler();
   virtual MTIKeyEventHandler getKeyUpEventHandler();
   virtual MTIMouseButtonEventHandler getMouseButtonDownEventHandler();
   virtual MTIMouseButtonEventHandler getMouseButtonUpEventHandler();
   virtual MTIMouseMoveEventHandler getMouseMoveEventHandler();
   virtual MTIMouseWheelEventHandler getMouseWheelEventHandler();
#endif

   static void setActiveWindow(TForm *newWindow);
   static TForm *getActiveWindow(void);

#ifndef NO_GUI_HACK
   static void setKeyDownEventHandler(MTIKeyEventHandler newEventHandler);
   static void setKeyUpEventHandler(MTIKeyEventHandler newEventHandler);
   static void setMouseButtonDownEventHandler(
                                  MTIMouseButtonEventHandler newEventHandler);
   static void setMouseButtonUpEventHandler(
                                  MTIMouseButtonEventHandler newEventHandler);
   static void setMouseMoveEventHandler(
                                    MTIMouseMoveEventHandler newEventHandler);
   static void setMouseWheelEventHandler(
                                    MTIMouseWheelEventHandler newEventHandler);
#endif

   // Static function used to create a new instance of
   // CNavSystemInterface.  A pointer to this function
   // is passed to the Tool Manager at startup so that
   // the Tool Manager can make instances of CNavSystemInterface
   // for each tool it creates.
   static CToolSystemInterface* createNavSystemInterface();

   virtual bool DeactivateTool(const CToolObject *toolObj);

   //  These get data from the player
   virtual bool isDisplaying();
   virtual long getIdleTime();
   virtual void fullStop();
   virtual void goToFrame(int);
   virtual void goToFrameSynchronous(int);
   virtual void refreshFrame();
   virtual void refreshFrameCached(bool fast = false);
   virtual void setPlaybackFilter(int newFilter, bool refresh=false);
   virtual int  getPlaybackFilter();
   virtual const CImageFormat * getVideoClipImageFormat();
   virtual void fattenRectangleFrame(RECT *, int);
   virtual void setDRSLassoMode(bool);
   virtual bool getDRSLassoMode();
   virtual MTI_INT32 getMarkIn();
   virtual MTI_INT32 getMarkOut();
   virtual void setMarkIn(MTI_INT32 nFrame);
   virtual void setMarkOut(MTI_INT32 nFrame);
   virtual bool isAClipLoaded();
   virtual bool isMasterClipLoaded();
   virtual bool doesClipHaveVersions();
	virtual void toolPlayClipFrameRangeInclusive(int firstFrameIndex, int lastFrameIndex);

   // interact with the Main Window
   virtual void UpdateStatusBar(const string &sStatus, int iPanel);
   virtual RECT GetClientRectScreenCoords();
   virtual RECT getImageRectangle();
   virtual void *GetMainWindowHandle();
   virtual int scaleXFrameToClient(int);
   virtual int scaleYFrameToClient(int);

   // Save & Restore Interface
   virtual int DisplayAllReviewRegions(int frameIndex);
   virtual int DisplaySingleReviewRegion(bool goNext, bool goZoom, int frameIndex);
   virtual int GoToNextOrPreviousReviewFrame(bool goNext, int frameIndex);
   virtual int SelectReviewRegion(CPixelRegionList& reviewRegion);
   virtual int ClearReviewRegions(bool doRedraw);
   virtual int MarkReviewRegions();
   virtual int ReviseAllGOVRegions();
   virtual int ReviseGOVRegions(int frameNumber);
   virtual int DiscardGOVRevisions();

   virtual int RestoreToolFrameBuffer(CToolFrameBuffer *frameBuffer,
                                      RECT *filterRect, POINT *filterLasso,
                                      BEZIER_POINT *filterBezier,
                                      CPixelRegionList *filterPixelRegion);
   virtual int SaveToolFrameBuffer(CToolFrameBuffer *frameBuffer,
                                   int toolNumber, const string& toolString,
                                   bool saveToHistory);
   // History cleanup hack
   virtual void ValidateHistory(int frameIndex);
   virtual void DiscardUnvalidatedHistory();

   // Set clip pointer for Save/Restore, called when user opens a clip
   static int setClip(ClipSharedPtr &newClip, int newVideoProxyIndex,
                      int newVideoFramingIndex);

   // Clip API
   virtual string getClipFilename();
   virtual int getVideoProxyIndex();
   virtual int getVideoFramingIndex();
   virtual ClipSharedPtr getClip();
   virtual CVideoFrameList* getVideoFrameList();
#if 1 // not yet: OLD_RENDER_DEST_HACK
   virtual CRenderDestinationClip* getRenderDestinationClip();
   virtual void initRenderDestinationClip(int);
#endif // OLD_RENDER_DEST_HACK
   virtual vector<string> getClipMetadataDirectoryChain(const string &metadataTypeName = string(""));

   // Provisional Interface
   virtual void ToggleProvisional(bool goZoom=false);
   virtual void AcceptProvisional(bool goToFrame);
   virtual void AcceptProvisionalInBackground();
   virtual void RejectProvisional(bool goToFrame);
   virtual void RejectProvisionalInBackground();
   virtual void RefreshProvisional();
   virtual void ClearProvisional(bool doLastFrame);
   virtual bool IsProvisionalPending();
   virtual bool IsProcessedVisible();
   virtual CToolFrameBuffer* GetProvisionalFrame();
   virtual int GetProvisionalFrameIndex();
   virtual int SetProvisionalHighlight(bool newHighlightFlag);
   virtual int SetProvisionalRender(bool newRenderFlag);
   virtual int SetProvisionalReprocess(bool newReprocessFlag);
   virtual int SetProvisionalReview(bool newReviewFlag);

   // Provisional Interface - extensions for interface to Provisional
   // pseudo-tool.  Not to be used by regular tools
   virtual int RegisterProvisionalToolProc(CProvisionalToolProc *toolProc);
   virtual int PutFrameToProvisional(CToolFrameBuffer *toolFrame);

   // Tool Manager API
   virtual int MakeSimpleToolSetup(const string& toolName,
                                   EToolSetupType newToolSetupType,
                                   CToolIOConfig *toolIOConfig);
   virtual int DestroyToolSetup(int toolSetupHandle);
   virtual int SetActiveToolSetup(int newToolSetupHandle);
   virtual int GetActiveToolSetupHandle();
   virtual int SetToolFrameRange(CToolFrameRange *toolFrameRange);
   virtual int SetToolParameters(CToolParameters *toolParameters);
   virtual int RunActiveToolSetup();
   virtual int StopActiveToolSetup();
   virtual int EmergencyStopActiveToolSetup();
   virtual int PauseActiveToolSetup();
   virtual EAutotoolStatus GetActiveToolSetupStatus();
   virtual EAutotoolStatus GetToolSetupStatus(int toolSetupHandle);
   virtual void WaitForToolProcessing();

   virtual bool CheckProvisionalUnresolved();
   virtual bool CheckToolProcessing();
   virtual bool CheckProvisionalUnresolvedAndToolProcessing();
   virtual void UpdateProvisionalStatusBarPanel();

   // PDL & Rendering
   int MakeOneNewPdlEntry(CPDL *pdl, int markin, int markout);
   int MakeANewPdlEntryForEachShotInClip(CPDL *pdl);
   int AddEntriesToActivePdl(CPDL *pdl);
   virtual int CapturePDLEntry(bool all = false);
   virtual bool IsPDLRendering();
   virtual bool IsPDLEntryLoading();

   // Mask Tool Interface
   virtual void SetMaskAllowed(bool regularMode, bool roiMode = false, bool initialRoiModeState = false);
   virtual bool IsMaskEnabled();
   virtual bool IsMaskAvailable();
   virtual bool IsMaskVisible();
   virtual bool IsMaskVisibilityLockedOff();
   virtual bool IsMaskAnimated();
   virtual bool IsMaskExclusive();
   virtual bool IsMaskInRoiMode();
   virtual CRegionOfInterest* GetMaskRoi();
   virtual int GetMaskRoi(int frameIndex, CRegionOfInterest &maskRoi);
   virtual void ClearMask();
   virtual void SetMaskToolAutoActivateRoiModeOnDraw(bool flag);
   virtual void SetMaskFromPdlEntry(CPDLEntry &pdlEntry);
   virtual int SaveMaskToFile(const string &filename);
   virtual int LoadMaskFromFile(const string &filename);
   virtual string GetMaskAsString();

   virtual void CalculateMaskROI(int);
   virtual void SetMaskTransformEditorIdleRefreshMode(bool onOffFlag);
   virtual void EnableMaskOutlineDisplay(bool);
   virtual void setMaskVisibilityLockedOff(bool flag);
   virtual void ExecuteMaskToolCommand(int mskcmd);
   virtual void setRectToMask(const RECT &rect);
   virtual void setMaskRoiIsRectangularOnly(bool flag);

   virtual ESelectedRegionShape GetROIShape(RECT &rect,
                                            POINT *lasso,
                                            BEZIER_POINT *bezier);
   virtual void SetROIShape(ESelectedRegionShape shape,
                            const RECT &rect,
                            const POINT *lasso,
                            const BEZIER_POINT *bezier);
   virtual void RestorePreviousROIShape();
   virtual void LockROI(bool flag);

   // from Mask Tool to active tool:
   virtual bool NotifyStartROIMode(); // Active tool returns TRUE if ROI mode OK
   virtual bool NotifyUserDrewROI();  // Active tool returns TRUE if handled
   virtual void NotifyEndROIMode();
   virtual void NotifyMaskChanged(const string &newMaskAsString);
   virtual void NotifyMaskVisibilityChanged();

   // Reticle Tool stuff
   virtual void InternalDisableReticleTool();
   virtual void InternalEnableReticleTool();

   // Bezier Editor for selection regions
   virtual void BezierMouseDown();
   virtual void BezierMouseMove(int x, int y);
   virtual void BezierMouseUp();
   virtual void BezierCtrlMouseDown();
   virtual void BezierShiftMouseDown();
   virtual void ClearBezier(bool doRedraw);
   virtual void FreeBezier(BEZIER_POINT* bezierPts);
   virtual bool GetBezierExtent(RECT &);
   virtual BEZIER_POINT* GetBezier();
   virtual bool IsBezierClosed();
   virtual void RedrawBezier();

   // Transform Editor for Paint II
   virtual void SetToolPaintInterface(CToolPaintInterface *);
   virtual void TransformPositionImportFrame();
   virtual void TransformMouseDown();
   virtual void TransformMouseMove(int x, int y);
   virtual void TransformMouseUp();
   virtual void TransformCtrlMouseDown();
   virtual void TransformShiftMouseDown();
   virtual void ClearTransform(bool doRedraw);
   virtual void SaveTransform();
   virtual void RestoreTransform();
   virtual void SetRotation(double);
   virtual void SetSkew(double);
   virtual void SetStretchX(double);
   virtual void SetStretchY(double);
   virtual void SetOffsetX(double);
   virtual void SetOffsetY(double);
   virtual void SetOffsetXY(double, double);
   virtual void SetTransform(double, double, double,
                             double, double, double);
   virtual void GetTransform(double *, double *, double *,
                             double *, double *, double *);
   virtual void RedrawTransform();
   virtual void TransformTimer();

   // for Tracking Points Editor in DeWarp
   virtual double dscaleXClientToFrame(int);
   virtual double dscaleYClientToFrame(int);
   virtual void drawUnfilledBoxFrameNoFlush(double x, double y);
   virtual void drawFilledBoxFrameNoFlush(double x, double y);
   virtual void drawUnfilledBoxScaledNoFlush(double x, double y, double rad);
   virtual void drawFilledBoxScaledNoFlush(double x, double y, double rad);
   virtual void drawArrowFrameNoFlush(double frx, double fry,
                                      double tox, double toy,
                                      double wdt, double len);

   // New accurate pixel tracking boxes for stabilization:
   // Nested boxes with optional crosshairs - use 0 for outer radius if
   // you don't want an outer box.
   virtual void drawTrackingBoxNoFlush(double centerX, double centerY,
                                       double innerRadiusX, double innerRadiusY,
                                       double outerRadiusX, double outerRadiusY,
                                       bool showCrosshairs);
   virtual void flushGraphics();

   // Methods to pass the Component (YUV) values from Navigator

   virtual int GetYcomp();
   virtual int GetUcomp();
   virtual int GetVcomp();
   virtual int getBitsPerComponent();
   virtual int getPixelComponents();
   virtual bool isMouseInFrame();

   // Cursor control API

   virtual void showCursor(bool showFlag);
   virtual void enterCursorOverrideMode(int CursorIndex);
   virtual void exitCursorOverrideMode();
   virtual void screenToClientCoords(int inX, int inY, int &outX, int &outY);
   virtual void informToolWindowStatus(bool windowActive);
   virtual void setPreferredFrameCursor(TCursor cursor);

   // Base tool ("Plugin Adopter") API

   // 'topPanel' arg must be a TPanel* that is the ancestor of all
   // of the adopted tool GUI's controls
   virtual int AdoptPluginToolGUI(
             const string &toolName, int tabIndex, int *topPanel,
             FosterParentEventHandlerT eventHandler);
   virtual void UnadoptPluginToolGUI(int tabIndex);
   virtual void FocusAdoptedControl(int *control);
   // The following really returns a TWinControl*
   int *GetFocusedAdoptedControl(void);
   void DisableTool(EToolDisabledReason reason);
   void EnableTool();
   virtual bool WarnThereIsNoUndo();
   // The following really returns a TComponent*
   virtual int *GetBaseToolFormHandle();  // for dialog positioning
   virtual EAcceptOrRejectChangeResult ShowAcceptOrRejectDialog(
                             bool *turnOnAutoAccept=NULL);

   // GOV Pseudotool API

   // from GOV to active tool:
   virtual bool NotifyGOVStarting(bool *cancelStretch=nullptr);  //  Active Tool replies true if it's OK
   virtual void NotifyGOVDone();
   virtual void NotifyGOVShapeChanged(ESelectedRegionShape shape,
                                      const RECT &rect,
                                      const POINT *lasso);
   virtual void NotifyRightClicked();

   // from active tool to GOV
   virtual bool IsGOVInProgress();
   virtual bool IsGOVPending();
   virtual void AcceptGOV();
   virtual void RejectGOV();
   virtual void SetTildeKeyHackMode(ETildeKeyHackMode newMode);
   virtual void SetGOVRepeatShape(ESelectedRegionShape shape,
                                  const RECT &rect,
                                  const POINT *lasso,
                                  const CPixelRegionList *regionList);
   virtual void SetGOVToolProgressMonitor(
                          IToolProgressMonitor *newToolProgressMonitor);


   // Tracking Pseudotool API

   // from Tracking to active tool:
   virtual bool NotifyTrackingStarting();
   virtual void NotifyTrackingDone(int errorCode);
   virtual void NotifyTrackingCleared();

   // from active tool to Tracking:
   virtual bool LockTrackingTool();
   virtual void ReleaseTrackingTool();
   virtual void SetTrackingToolEditor(TrackingBoxManager *newEditor);
   virtual void SetTrackingToolProgressMonitor(
                          IToolProgressMonitor *newToolProgressMonitor);
   virtual void ActivateTrackingTool();
   virtual void DeactivateTrackingTool();
   virtual void StartTracking();
   virtual void StopTracking();

   // Tool Frame Cache API
   virtual CToolFrameCache *GetToolFrameCache();

   // Background thread support
   virtual void FireRefreshTimerFromBackground(); // force heartbeat

   // AutoClean API
   virtual bool IsAutoCleanEnabled();

   // Debris file API
//   virtual int GetAutoCleanDebrisVersion();
//   virtual string GetAutoCleanDebrisFileName(int frameIndex);
//   virtual CAutoCleanDebrisFile *GetAutoCleanDebrisFile(int frameIndex);
//   virtual void ReleaseAutoCleanDebrisFile(CAutoCleanDebrisFile *debrisFile);

   // AutoClean display HACK-O-RAMA
   virtual void SetAutoCleanOverlayList(const OverlayDisplayList &newList);
   virtual void MakeSureThisRectIsVisible(const RECT &rect);
   virtual void GetDisplayLutValues(MTI_UINT8 *outputValues);
   virtual int GetListOfSmallHistoryRectangles(
                  int frameIndex,
                  int sizeLimit,       // max size of a side, in pixels
                  vector<RECT> &rectList);   // output
   virtual int GetOriginalValues(
                  int frameIndex,
                  RECT filterRect,      // INCLUSIVE right & bottom
                  MTI_UINT16 *outBuf);  // filterRect size, prefill with current values
   virtual bool IsPlayerReallyPlaying();

   // Shameless Hacks
   virtual void TellTheDisplayerToRedisplayTheCurrentFrame();
   virtual void TellTheDisplayerToDrawAShape(
      double centerX, double centerY,
      int shapeType, int radius, int aspectRatio, int angle,
      MTI_UINT8 R, MTI_UINT8 G, MTI_UINT8 B);
   virtual void IDontNeedThisKeyDownEvent(WORD &Key, TShiftState Shift);

   // Preview hack for color breathing tool
   virtual void enterPreviewHackMode();
	virtual void exitPreviewHackMode();

	// View-only mode
	virtual void NotifyStartViewOnlyMode();
	virtual void NotifyEndViewOnlyMode();

	// Main Window Top Panel API
   virtual void        SetMainWindowTopPanelVisibility(bool onOff);
   virtual bool        GetMainWindowTopPanelVisibility();
   virtual CMTIBitmap *GetBitmapRefForTopPanelDrawing();
	virtual void        DrawBitmapOnMainWindowTopPanel();

	// User guide viewer API
	virtual void SetToolNameForUserGuide(const string &toolName);


private:

   enum EHistoryReviewStatus
      {
      HISTORY_REVIEW_STATUS_NONE,   // No debris regions are displayed
      HISTORY_REVIEW_STATUS_SINGLE, // Single debris region is displayed
      HISTORY_REVIEW_STATUS_FRAME   // All debris regions for a frame are displayed
      };

private:

   // added third argument 4/01/02 for ptr to lasso array
   static void stretchRectangleCallback(void *obj, void* data1, void *data2);


private:

#ifdef __BORLANDC__
   // mainWindow or fullWindow
   static TForm *activeWindow;
#endif

   // Stretch Rectangle Variables
   static bool stretchRectangleAvailable;
   static RECT imageStretchRectangle;
   static POINT *imageLassoPointer;

#ifndef NO_GUI_HACK
   // Pointers to Event Handling Functions supplied by application
   static MTIKeyEventHandler keyDownEventHandler;
   static MTIKeyEventHandler keyUpEventHandler;
   static MTIMouseButtonEventHandler mouseButtonDownEventHandler;
   static MTIMouseButtonEventHandler mouseButtonUpEventHandler;
   static MTIMouseMoveEventHandler mouseMoveEventHandler;
   static MTIMouseWheelEventHandler mouseWheelEventHandler;
#endif

   virtual CBufferPool* GetBufferAllocator(int bytesPerBuffer, int bufferCount,
                                           double invisibleFieldsPerFrame);
   virtual void ReleaseBufferAllocator(int bytesPerBuffer, int bufferCount,
                                       double invisibleFieldsPerFrame);

   virtual CBezierEditor *getBezierEditor();

   virtual CTransformEditor *getTransformEditor();


#ifdef __sgi
   static MainWindow *mainWindow;  // System API's  pointer to Main Window,
                                   // since it is not a global
#endif // #ifdef __sgi

   static CSaveRestore *saveRestoreEngine;
   static CHistoryReviewList *historyReviewList;
   static string currentClipFilename;

   static CProvisional *provisional;  // Pointer to CProvisional instance
                                      // owned by the Provisional pseudo-Tool
   static EHistoryReviewStatus historyReviewStatus;

   static CBezierEditor *navBezierEditor;

   static CTransformEditor *navTransformEditor;

   static DWORD mainThreadId;

   static int refCount;


};

//////////////////////////////////////////////////////////////////////
// Global Functions

void InitToolManager();

//////////////////////////////////////////////////////////////////////

#endif // #if !defined(NAVSYSTEMINTERFACE_H)




