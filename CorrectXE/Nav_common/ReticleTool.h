// ReticleTool.h: interface for the CReticleTool class.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef RETICLETOOLH
#define RETICLETOOLH

#include "machine.h"

///////////////////////////////////////////////////////////////////////////////
// Forward Declarations

class CDisplayer;
class CIniFile;
class CPDLEntry;
class CToolSystemInterface;

///////////////////////////////////////////////////////////////////////////////

enum EReticleToolMouseMode
{
   RETICLE_TOOL_MOUSE_MODE_NONE,
   RETICLE_TOOL_MOUSE_MODE_STRETCH
};

///////////////////////////////////////////////////////////////////////////////

class CReticleTool
{
public:
   CReticleTool();
   virtual ~CReticleTool();

   int BeginUserSelect(CDisplayer *displayer);
   int EndUserSelect(CDisplayer *displayer);

   bool isReticleToolEnabled() const;
   bool isReticleToolInternalEnabled() const;

   void setReticleToolEnabled(bool newEnabled);
   void setReticleToolInternalEnabled(bool newEnabled);

   void setSystemAPI(CToolSystemInterface *newSystemAPI);

private:

   CToolSystemInterface* getSystemAPI();

private:

   bool reticleToolInternalEnabled;

   bool reticleToolEnabled;

   CToolSystemInterface *baseSystemAPI;  // Ptr to System Interface object

   EReticleToolMouseMode mouseMode;

private:
   // Private static variables
   static string reticleToolSectionName;
   static string reticleToolEnabledKey;
};
///////////////////////////////////////////////////////////////////////////////

#endif

