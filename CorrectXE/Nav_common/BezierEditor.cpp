
#include <math.h>
#include "BezierEditor.h"
#include "Displayer.h"
#include "PixelRegions.h"

//----- Vertex class -----------------------------------------------------------

Vertex::Vertex()
{
   select = SELECT_NONE;
   bwd = NULL;
   fwd = NULL;
}

Vertex::~Vertex()
{
}

//------------------------------------------------------------------------------
// Bugzilla ref #2032 - KT-050112
// we have modified the way the spline dynamics are done,
// in order to eliminate flicker. Previously, the image
// was refreshed (without the spline), and then the spline
// was drawn into the FRONT GL buffer. This process was
// perceptible to the user, esp when the spline was large.
// As currently implemented, the spline is refreshed by
// displayFrames' ToolManager call to onRedraw, which calls
// MaskTool's DrawMask() and gets the spline-in-progress from
// the regionList (which never contains more than 1 region).
// Because onRedraw is now called from inside displayFrame,
// the spline is drawn into the BACK GL buffer and made vis-
// ible by SwapBuffers. The process of drawing the spline
// is no longer perceptible. In addition, there appears to
// be a great speed advantage drawing to the BACK GL buffer,
// esp. in dual monitor systems.

CBezierEditor::CBezierEditor(CDisplayer * dsp)
{
   // save the address of the displayer
   displayer = dsp;

   // initialize the modkeys
   modKeys = 0;

   // no spline allocated
   splineHead = 0;

   additionalVertexAdded = false;

   // initialize for a new spline
   newSpline();
}

CBezierEditor::CBezierEditor(const CBezierEditor& bzed)
{
   // get the address of the displayer
   displayer = bzed.displayer;

   // initialize the modkeys
   modKeys = 0;

   // no spline allocated
   splineHead = 0;

   additionalVertexAdded = false;

   // copy the vertices from the source
   loadBezier(bzed.splineHead);
}

CBezierEditor::~CBezierEditor()
{
   freeAllVertices();
}

// manage your vertices
//
void CBezierEditor::freeAllVertices()
{
   // free any vertices in a spline
   Vertex *vi = splineHead;
   Vertex *vf;
   while (vi!=NULL) {
      vf = vi->fwd;
      delete vi;
      vi = vf;

       if (vi == splineHead) break;
   }

   splineHead = NULL;
}

Vertex* CBezierEditor::allocateVertex()
{
   if (vertexCount == MAX_BEZIER_PTS)
      return NULL;
   return new Vertex;
}

void CBezierEditor::freeVertex(Vertex *vtx)
{
   delete vtx;
}

// initialize for a new spline
//
void CBezierEditor::newSpline()
{
   freeAllVertices();

   // no spline, no closed
   vertexCount = 0;
   splineHead = NULL;
   splineTail = NULL;
   splineIsClosed = false;

   // home the majorstate
   majorState = MAJORSTATE_TRACK;
}

bool CBezierEditor::addPoint(POINT *pt)
{
   if (pt == NULL) return false;

   if ((vertexCount > 1)&&(pt->x == splineHead->x)&&(pt->y == splineHead->y)) {

      splineTail->fwd = splineHead;
      splineHead->bwd = splineTail;
      splineIsClosed = true;
   }
   else {

   Vertex *dst = allocateVertex();

      dst->select = SELECT_NONE;
      dst->bwd = splineTail;
      dst->fwd = NULL;
      dst->xctrlpre = pt->x;
      dst->yctrlpre = pt->y;
      dst->x        = pt->x;
      dst->y        = pt->y;
      dst->xctrlnxt = pt->x;
      dst->yctrlnxt = pt->y;

      if (vertexCount == 0) {

         splineHead = splineTail = dst;
      }
      else {

         splineTail->fwd = dst;
         splineTail = dst;
      }

      vertexCount++;
   }

   return splineIsClosed;
}

// Draw the list of spline vertices into IMGRECT, using
// the machinery in the DISPLAYER. This takes care of all
// the clipping and scaling.
//
void CBezierEditor::drawSpline(Vertex *vtx, unsigned int dashpat, bool dontDrawVertices)
{
   if (vtx==NULL) return;

   // first the spline itself
   Vertex *vi = vtx;
   Vertex *vf;
   displayer->moveToFrame(vi->x, vi->y);
   while ((vf=vi->fwd) != NULL) {

      displayer->splineToFrameNoFlush(vi->xctrlnxt, vi->yctrlnxt,
                                      vf->xctrlpre, vf->yctrlpre,
                                      vf->x, vf->y, dashpat);
      if (vf == vtx) break;
      vi = vf;
   }

   if (!dontDrawVertices)
   {
      // ...then the spline vertices - when selected these
      // have the control points shown as little dumbbells
      vi = vtx;
      while (vi != NULL) {

         displayer->drawVertexFrameNoFlush(vi->xctrlpre, vi->yctrlpre,
                                           vi->x       , vi->y       ,
                                           vi->xctrlnxt, vi->yctrlnxt,
                                           vi->select);

         vi = vi->fwd;
         if (vi == vtx) break;
      }
   }

   // do ONE flush
   displayer->flushGraphics();
}


// the spline is drawn in the color set by the
// last call to Displayer->setGraphicsColor
//
void CBezierEditor::drawSplineFrame(unsigned int dashpat, bool dontDrawVertices)
{
   drawSpline(splineHead, dashpat, dontDrawVertices);
}

// refresh the spline-in-progress by drawing
// the image, then the regionList (containing
// the spline), into the back GL buffer. Then
// make everything visible by swapping buffers
// The chain of calls is displayFrame ->
// onRedraw -> DrawMask -> drawSplineFrame ->
// SwapBuffers
void CBezierEditor::refreshSplineFrame()
{
   displayer->displayFrame();
}

RECT CBezierEditor::getSimpleSplineExtent()
{
   RECT retVal;
   retVal.left   = 32767;
   retVal.top    = 32767;
   retVal.right  = -32768;
   retVal.bottom = -32768;

   Vertex *src = splineHead;
   while (src != NULL) {

      if (src->xctrlpre < retVal.left  ) retVal.left   = src->xctrlpre;
      if (src->xctrlpre > retVal.right ) retVal.right  = src->xctrlpre;
      if (src->yctrlpre < retVal.top   ) retVal.top    = src->yctrlpre;
      if (src->yctrlpre > retVal.bottom) retVal.bottom = src->yctrlpre;

      if (src->x < retVal.left  ) retVal.left   = src->x;
      if (src->x > retVal.right ) retVal.right  = src->x;
      if (src->y < retVal.top   ) retVal.top    = src->y;
      if (src->y > retVal.bottom) retVal.bottom = src->y;

      if (src->xctrlnxt < retVal.left  ) retVal.left   = src->xctrlnxt;
      if (src->xctrlnxt > retVal.right ) retVal.right  = src->xctrlnxt;
      if (src->yctrlnxt < retVal.top   ) retVal.top    = src->yctrlnxt;
      if (src->yctrlnxt > retVal.bottom) retVal.bottom = src->yctrlnxt;

      if ((src = src->fwd)==splineHead) break;
   }

	// wtf is this 3* shit? QQQ
   retVal.left   -= 3*BOX_SIZE;
   retVal.top    -= 3*BOX_SIZE;
   retVal.right  += 3*BOX_SIZE;
   retVal.bottom += 3*BOX_SIZE;

   return retVal;
}

// new, smart version generates intermediate points
// returns the extent of the completed spline in the
// argument RECT. Returns TRUE if the extent overlaps
// the frame, FALSE if it doesn't
bool CBezierEditor::getCompletedSplineExtent(RECT& bezrct)
{
   RECT extent;
   int  x,  y, x0, y0, x1, y1, x2, y2, x3, y3;
   int delx, dely, length, nseg;
   double u0, u1, u2, u3, v0 , v1, v2, v3, delt;

   extent.left   = MAXFRAMEX;
   extent.top    = MAXFRAMEY;
   extent.right  = MINFRAMEX;
   extent.bottom = MINFRAMEY;

   Vertex *vi = splineHead;

   // include the first vertex in the extent
   x3 = vi->x; y3 = vi->y;
   if (x3 < extent.left)
      extent.left = x3;
   if (y3 < extent.top )
      extent.top  = y3;
   if (x3 > extent.right)
      extent.right = x3;
   if (y3 > extent.bottom)
      extent.bottom = y3;

   for (int i=0;i<vertexCount;i++) {

      x0 = x3; y0 = y3;
      x1 = vi->xctrlnxt; y1 = vi->yctrlnxt;

      vi = vi->fwd;

      x2 = vi->xctrlpre; y2 = vi->yctrlpre;
      x3 = vi->x; y3 = vi->y;

      // coefficients for X
      u0 = x0;
      u1 = 3*(x1 - x0);
      u2 = 3*(x2 - 2*x1 + x0);
      u3 = (x3 - 3*x2 + 3*x1 - x0);

      // coefficients for Y
      v0 = y0;
      v1 = 3*(y1 - y0);
      v2 = 3*(y2 - 2*y1 + y0);
      v3 = (y3 - 3*y2 + 3*y1 - y0);

      // "length" of spline
      length = 0;
      if ((delx = x1 - x0)<0) delx = -delx;
      length += delx;
      if ((dely = y1 - y0)<0) dely = -dely;
      length += dely;
      if ((delx = x2 - x1)<0) delx = -delx;
      length += delx;
      if ((dely = y2 - y1)<0) dely = -dely;
      length += dely;
      if ((delx = x3 - x2)<0) delx = -delx;
      length += delx;
      if ((dely = y3 - y2)<0) dely = -dely;
      length += dely;

      // number of segments with
      // average chordleng = 4
      nseg = length / 4;

      // must be at least one
      if (nseg == 0) nseg = 1;

      // value of delt
      delt = 1.0 / nseg;

      // now generate the intermediate points
      // and include them in the spline extent
      for (double t=delt;t<1.0;t+=delt) {

         x = (((u3*t+u2)*t+u1)*t+u0)+.5; y = (((v3*t+v2)*t+v1)*t+v0)+.5;

         if (x < extent.left)
            extent.left = x;
         if (y < extent.top )
            extent.top  = y;
         if (x > extent.right)
            extent.right = x;
         if (y > extent.bottom)
            extent.bottom = y;
      }

      if (x3 < extent.left)
         extent.left = x3;
      if (y3 < extent.top )
         extent.top  = y3;
      if (x3 > extent.right)
         extent.right = x3;
      if (y3 > extent.bottom)
         extent.bottom = y3;
   }

   // and the extent to the frame limits
   displayer->clipRectangleFrame(&extent);

   // return the clipped extent
   bezrct = extent;

   return(displayer->rectangleIsValid(&extent));
}

// returns a completed spline in the form of an array of
// BEZIER_POINTS, with explicit closure.
//
BEZIER_POINT *CBezierEditor::getCompletedSpline()
{
   if ((!splineIsClosed)||(vertexCount==0)) return NULL;

   BEZIER_POINT *retval = new BEZIER_POINT[vertexCount+1];

   Vertex *vi = splineHead;
   for (int i=0;i<vertexCount;i++) {

      retval[i].xctrlpre = vi->xctrlpre;
      retval[i].yctrlpre = vi->yctrlpre;

      retval[i].x        = vi->x;
      retval[i].y        = vi->y;

      retval[i].xctrlnxt = vi->xctrlnxt;
      retval[i].yctrlnxt = vi->yctrlnxt;

      vi = vi->fwd;
   }

   // last vertex same as first closes the spline
   retval[vertexCount].xctrlpre = retval[0].xctrlpre;
   retval[vertexCount].yctrlpre = retval[0].yctrlpre;

   retval[vertexCount].x        = retval[0].x;
   retval[vertexCount].y        = retval[0].y;

   retval[vertexCount].xctrlnxt = retval[0].xctrlnxt;
   retval[vertexCount].yctrlnxt = retval[0].yctrlnxt;

   return retval;
}

int CBezierEditor::getVertexCount()
{
   return vertexCount;
}

Vertex *CBezierEditor::getSplineVertices()
{
   return splineHead;
}

VertexList CBezierEditor::copyVerticesToVertexList()
{
	vector<Vertex> result;
	auto sh = splineHead;
	if (sh == nullptr)
	{
		return result;
	}

	do
	{
		result.push_back(*sh);
		sh = sh->fwd;
	}
    while ((sh != splineHead) && (sh != nullptr));

	return result;
}

bool CBezierEditor::isClosed()
{
   // Returns true if a closed spline is available
   return (vertexCount > 0 && splineIsClosed);
}

bool CBezierEditor::isEmpty()
{
   // Returns true if there are not any points in the bezier
   return (vertexCount < 1);
}

bool CBezierEditor::isFullySelected()
{
   Vertex *vi = splineHead;
   while (vi != NULL) {

      if (vi->select == SELECT_NONE)
         return false;
      vi = vi->fwd;
      if ((vi == NULL)||(vi == splineHead))
         return true;
   }

   return false;
}

// return the extent box of the spline segment vi->vf
//
DRECT CBezierEditor::extentOfSplineSegment(Vertex *vi, Vertex *vf)
{
   DRECT extent;
   extent.left   = vi->x;
   extent.top    = vi->y;
   extent.right  = vi->x;
   extent.bottom = vi->y;

   if (vi->xctrlnxt < extent.left)
      extent.left = vi->xctrlnxt;
   if (vi->yctrlnxt < extent.top)
      extent.top  = vi->yctrlnxt;
   if (vi->xctrlnxt > extent.right)
      extent.right = vi->xctrlnxt;
   if (vi->yctrlnxt > extent.bottom)
      extent.bottom = vi->yctrlnxt;

   if (vf->xctrlpre < extent.left)
      extent.left = vf->xctrlpre;
   if (vf->yctrlpre < extent.top)
      extent.top  = vf->yctrlpre;
   if (vf->xctrlpre > extent.right)
      extent.right = vf->xctrlpre;
   if (vf->yctrlpre > extent.bottom)
      extent.bottom = vf->yctrlpre;

   if (vf->x < extent.left)
      extent.left = vf->x;
   if (vf->y < extent.top)
      extent.top  = vf->y;
   if (vf->x > extent.right)
      extent.right = vf->x;
   if (vf->y > extent.bottom)
      extent.bottom = vf->y;

   return extent;
}

bool CBezierEditor::wasSplineJustClosed()
{
   return splineJustClosed;
}

bool CBezierEditor::wasAdditionalVertexAdded()
{
   return additionalVertexAdded;
}

double CBezierEditor::getLatestTHit()
{
   return latestTHit;
}

// test for point inside box
//
bool CBezierEditor::boxStraddlesPoint(double x, double y, DRECT &box)
{
  if ((x >= box.left)&&
      (y >= box.top) &&
      (x <= box.right) &&
      (y <= box.bottom))
     return true;

  return false;
}

// return the squared distance between two points
double CBezierEditor::sqDistance(double xfm, double yfm, double xto, double yto)
{
   int delx = (xto-xfm); int dely = (yto-yfm);
   return (delx*delx + dely*dely);
}

// Test to see if an attach point of the spline is inside box.
// Return the vertex
Vertex * CBezierEditor::boxStraddlesAttachPoint(Vertex *vtx, DRECT &box)
{
   Vertex *vi = vtx;
   while (vi!=NULL) {

      // does the box straddle this attach point?
      if (boxStraddlesPoint(vi->x, vi->y, box)) return vi;

      vi = vi->fwd;

      // if spline is closed you're done
      if (vi==vtx) break;
   }

   return NULL;
}

// Test to see if a control point of the spline is inside box.
// Return the vertex
//
Vertex * CBezierEditor::boxStraddlesControlPoint(Vertex *vtx, DRECT &box)
{
   Vertex *vi = vtx;
   while (vi!=NULL) {

      if (vi->select&SELECT_VTX) {

         // does the box straddle either control point?
         if ((boxStraddlesPoint(vi->xctrlpre, vi->yctrlpre, box))||
             (boxStraddlesPoint(vi->xctrlnxt, vi->yctrlnxt, box)))
            return vi;
      }

      vi = vi->fwd;

      // if spline is closed you're done
      if (vi==vtx) break;
   }

   return NULL;
}

// Cohn-Sutherland codes
//
MTI_UINT8 CBezierEditor::ticTacToeCode(double x, double y, DRECT &box)
{
   MTI_UINT8 tcode = 0;

   if (x < box.left)   tcode |= LFTCOD;
   if (y < box.top )   tcode |= TOPCOD;
   if (x > box.right)  tcode |= RGTCOD;
   if (y > box.bottom) tcode |= BOTCOD;

   return tcode;
}

// Analyze the crossings of a spline chord segment across the test box.
// Assumes both endpoints of the edge are outside the box. Argument
// tcodsum is the logical OR of the Cohn-Sutherland codes for the end-
// points relative to the box. If there's a crossing, return the avg
// of the t-parameters for the crossings along the edge.
//
double CBezierEditor::boxStraddlesEdge(double xi, double yi,
                                       double xf ,double yf,
                                       DRECT &box, MTI_UINT8 tcodsum)
{
   double delx = xf - xi;
   double dely = yf - yi;

   double thit = 0.;
   int crossings = 0;
   double xcrs, ycrs;

   if (tcodsum&LFTCOD) { // crosses lft edge of box

      ycrs = yi + (box.left - xi)*dely/delx;
      if ((ycrs<=box.bottom)&&(ycrs>box.top)) {
         thit += (double)(box.left - xi)/delx;
         ++crossings;
      }
   }
   if (tcodsum&TOPCOD) { // crosses top edge of box

      xcrs = xi + (box.top - yi)*delx/dely;
      if ((xcrs>=box.left)&&(xcrs<box.right)) {
         thit += (double)(box.top - yi)/dely;
         if (++crossings==2) return (thit/2.);
      }
   }
   if (tcodsum&RGTCOD) { // crosses rgt edge of box

      ycrs = yi + (box.right - xi)*dely/delx;
      if ((ycrs>=box.top)&&(ycrs<box.bottom)) {
         thit += (double)(box.right - xi)/delx;
         if (++crossings==2) return (thit/2.);
      }
   }
   if (tcodsum&BOTCOD) { // crosses bot edge of box

      xcrs = xi + (box.bottom - yi)*delx/dely;
      if ((xcrs<=box.right)&&(xcrs>box.left)) {
         thit += (double)(box.bottom - yi)/dely;
         if (++crossings==2) return (thit/2.);
      }
   }

   // box does not straddle edge
   return -1.0;
}

// Analyze the crossing of a spline segment relative to a box. If the
// box straddles the spline segment, return the best estimate of the
// t-parameter ([0,1] along the spline segment) for the crossing.
//
double CBezierEditor::boxStraddlesSplineSegment(Vertex *vi, Vertex *vf, DRECT &box,
                                                int *jordan, double *edgdst)
{
   // get the spline segment extent
   DRECT extent = extentOfSplineSegment(vi, vf);

   // if box does not overlap spline
   // segment extent then return -1.0
   //if ((box.left   > extent.right )||
   //    (box.top    > extent.bottom)||
   //    (box.right  < extent.left  )||
   //    (box.bottom < extent.top   ))
   //   return -1.0;

   // The box overlaps the spline extent.
   // Expand the spline into tiny chords
   // and test them against the box.  We
   // use Cohn-Sutherland ("tic-tac-toe")
   // codes to do this

   // initial end point
   double x0 = vi->x;
   double y0 = vi->y;

   // 1st control point
   double x1 = vi->xctrlnxt;
   double y1 = vi->yctrlnxt;

   // 2nd control point
   double x2 = vf->xctrlpre;
   double y2 = vf->yctrlpre;

   // final end point
   double x3 = vf->x;
   double y3 = vf->y;

   // coefficients for X
   double u0 = x0;
   double u1 = 3*(x1 - x0);
   double u2 = 3*(x2 - 2*x1 + x0);
   double u3 = (x3 - 3*x2 + 3*x1 - x0);

   // coefficients for Y
   double v0 = y0;
   double v1 = 3*(y1 - y0);
   double v2 = 3*(y2 - 2*y1 + y0);
   double v3 = (y3 - 3*y2 + 3*y1 - y0);

   // "length" of spline
   double delx, dely;
   double length = 0;
   if ((delx = x1 - x0)<0) delx = -delx;
   length += delx;
   if ((dely = y1 - y0)<0) dely = -dely;
   length += dely;
   if ((delx = x2 - x1)<0) delx = -delx;
   length += delx;
   if ((dely = y2 - y1)<0) dely = -dely;
   length += dely;
   if ((delx = x3 - x2)<0) delx = -delx;
   length += delx;
   if ((dely = y3 - y2)<0) dely = -dely;
   length += dely;

   // number of segments - this needs to
   // be fairly large to get an accurate
   // estimate of the [0,1] parameter of
   // the hit point
   int nseg = (length/ 2)*32;

   // must have at least one
   if (nseg == 0) nseg = 1;

   // value of delt
   double delt = 1.0 / nseg;

   // now generate the spline
   double tpre, tcur;
   double xpre, ypre, xcur, ycur;
   MTI_UINT8 tcodpre, tcodcur;

   // 1st end point
   tcur = 0.;

   xcur = x0; ycur = y0;
   double boxdst = (xcur - box.left)*(xcur - box.left) + (ycur - box.top)*(ycur - box.top);
   if (boxdst < *edgdst)
      *edgdst = boxdst;

   tcodcur = ticTacToeCode(xcur, ycur, box);
   if (tcodcur == 0) return tcur; // (x0, y0) is in the box

   double thit;
   for (int i=0;i<(nseg-1);i++) {

      // advance to next chord endpoint
      tpre = tcur;
      xpre = xcur; ypre = ycur;

      boxdst = (xcur - box.left)*(xcur - box.left) + (ycur - box.top)*(ycur - box.top);
      if (boxdst < *edgdst)
         *edgdst = boxdst;

      tcodpre = tcodcur;

      tcur += delt;
      xcur = (((u3*tcur+u2)*tcur+u1)*tcur+u0);
      ycur = (((v3*tcur+v2)*tcur+v1)*tcur+v0);
      tcodcur = ticTacToeCode(xcur, ycur, box);

      if (tcodcur == 0)
         return tcur; // (xcur, ycur) is in the box

      // determine edge contribution to Jordan sum
      if (((tcodpre|tcodcur)&TOPCOD)&&
          ((tcodpre^tcodcur)&LFTCOD)) {

         double ycrs = ypre + (box.left - xpre)*(ycur - ypre)/(xcur - xpre);
         if (ycrs < box.top)
            *jordan += 1;
      }

      if ((tcodpre&tcodcur) == 0) { // no trivial reject

         MTI_UINT8 tcodsum = tcodpre|tcodcur;

         if ((thit = boxStraddlesEdge(xpre,ypre,xcur,ycur, box, tcodsum)) > 0.) {

            // The segment (xpre,ypre)->(xcur,ycur) crosses the box.
            // The locus of the crossing on the segment is thit
            return (tpre + thit*delt);
         }
      }
   }

   // 2nd endpoint terminates final chord
   tpre = tcur;
   xpre = xcur; ypre = ycur;
   tcodpre = tcodcur;

   tcur = 1.0;
   xcur = x3;
   ycur = y3;

   boxdst = (xcur - box.left)*(xcur - box.left) + (ycur - box.top)*(ycur - box.top);
   if (boxdst < *edgdst)
      *edgdst = boxdst;

   tcodcur = ticTacToeCode(xcur, ycur, box);

   if (tcodcur == 0) return tcur; // (xcur, ycur) is in the box

   // determine edge contribution to Jordan sum
   if (((tcodpre|tcodcur)&TOPCOD)&&
       ((tcodpre^tcodcur)&LFTCOD)) {

      double ycrs = ypre + (box.left - xpre)*(ycur - ypre)/(xcur - xpre);
      if (ycrs < box.top)
         *jordan += 1;
   }

   if ((tcodpre&tcodcur) == 0) { // no trivial reject

      MTI_UINT8 tcodsum = tcodpre|tcodcur;

      if ((thit = boxStraddlesEdge(xpre,ypre,xcur,ycur, box, tcodsum)) > 0.) {

         // The segment (xpre,ypre)->(xcur,ycur) crosses the box.
         // The locus of the crossing on the segment is thit
         return (tpre + thit*delt);
      }
   }

   // the spline segment does not cross the box
   return -1.0;
}

// Analyze the crossing of a spline relative to a box. If the
// box straddles the spline , return the best estimate of the
// t-parameter ([0,n] along the spline) for the crossing.
//
double CBezierEditor::boxStraddlesSpline(Vertex *vtx, DRECT &box,
                                         int *jordan, double *edgdst)
{
   *jordan = 0;

   if (vtx!=NULL) {

      Vertex *vi = vtx;
      Vertex *vf = vtx->fwd;

      // if spline contains one vertex...test it
      if (vf == NULL) {
         if (boxStraddlesPoint(vi->x, vi->y, box)) return 0.;
         else return -1.;
      }

      double segnum = 0.;
      double seghit;
      while (vf != NULL) {

         // does the box straddle this spline segment?
         if ((seghit = boxStraddlesSplineSegment(vi, vf, box, jordan, edgdst)) >= 0.)
            return (segnum + seghit);

         if (vf == vtx) break; // spline is closed

         vi = vf;
         vf = vi->fwd;
         segnum += 1.;
      }
   }

   // the box did not straddle the spline
   return -1.0;
}

void CBezierEditor::selectAllVertices(Vertex *vtx)
{
   Vertex *vi = vtx;
   while (vi != NULL) {
      vi->select = SELECT_VTX;
      vi = vi->fwd;
      if (vi==vtx) break;
   }
}

void CBezierEditor::unselectAllVertices(Vertex *vtx)
{
   Vertex *vi = vtx;
   while (vi != NULL) {
      vi->select = SELECT_NONE;
      vi = vi->fwd;
      if (vi==vtx) break;
   }
}

bool CBezierEditor::areAnyVerticesSelected(Vertex *vtx)
{
   bool retVal = false;

   Vertex *vi = vtx;
   while (vi != NULL) {

      if (vi->select==SELECT_VTX) {

         retVal = true;
         break;
      }

      vi = vi->fwd;
      if (vi==vtx) break;
   }

   return retVal;
}

DRECT CBezierEditor::extentAllSelectedVertices(Vertex *vtx)
{
   DRECT retval;
   retval.left   = MAXFRAMEX;
   retval.top    = MAXFRAMEY;
   retval.right  = MINFRAMEX;
   retval.bottom = MINFRAMEY;

   Vertex *vi = vtx;
   while (vi != NULL) {

      if (vi->select & SELECT_VTX) { // vertex is selected

         // include coords in extent
         if (vi->x < retval.left  ) retval.left   = vi->x;
         if (vi->y < retval.top   ) retval.top    = vi->y;
         if (vi->x > retval.right ) retval.right  = vi->x;
         if (vi->y > retval.bottom) retval.bottom = vi->y;
      }

      vi = vi->fwd;
      if (vi==vtx) break;
   }

   return retval;
}

// Translate the selected vertices by an amount [delx, dely],
// They may wander outside the clip's active rectangle
//
void CBezierEditor::translateAllSelectedVertices(Vertex *vtx,
                                                 double delx, double dely)
{
   Vertex *vi = vtx;
   while (vi != NULL) {

      if (vi->select & SELECT_VTX) { // vertex is selected

         // ctrl pt may go off frame
         vi->xctrlpre += delx;
         vi->yctrlpre += dely;

         vi->x        += delx;
         vi->y        += dely;

         // ctrl pt may go off frame
         vi->xctrlnxt += delx;
         vi->yctrlnxt += dely;
      }

      vi = vi->fwd;
      if (vi==vtx) break;
   }
}

// Go through the spline and delete the vertices which are
// selected. Set splineHead, splineTail, and vertexCount in
// order to continue editing.
//
void CBezierEditor::deleteAllSelectedVertices(Vertex *vtx)
{
   Vertex *newSplineHead = NULL;
   Vertex *newSplineTail = NULL;
   int newVertexCount = 0;
   int lastVertexSelect = 0;

   // break the closure before processing
   if (splineIsClosed) {
      splineTail->fwd = NULL;
      splineHead->bwd = NULL;
   }
   else
      lastVertexSelect = splineTail->select;

   Vertex *vi = vtx;
   Vertex *vf;
   while (vi != NULL) {

      vf = vi->fwd;

      if (vi->select & SELECT_VTX) { // vertex is selected

         // free the vertex
         freeVertex(vi);

      }
      else {                         // vertex is unselected

         // add the vertex to the new chain
         if (newSplineHead==NULL) {
            newSplineHead = vi;
            vi->bwd = NULL;
         }
         if (newSplineTail != NULL) {
            newSplineTail->fwd = vi;
            vi->bwd = newSplineTail;
         }
         newSplineTail = vi;
         vi->fwd = NULL;

         newVertexCount++;
      }

      if (vf==vtx) break;
      vi = vf;
   }

   if (splineIsClosed&&(newVertexCount>=2)) {
      newSplineTail->fwd = newSplineHead;
      newSplineHead->bwd = newSplineTail;
   }
   else
      splineIsClosed = false;

   splineHead = newSplineHead;
   splineTail = newSplineTail;
   vertexCount = newVertexCount;

   // select state of spline tail
   if (splineTail != NULL) {
      splineTail->select = lastVertexSelect;
   }

   // must work if splineTail = NULL
   selectedVertex = NULL;
   if (lastVertexSelect & SELECT_VTX)
      selectedVertex = splineTail;
}

// Creates an intermediate vertex at the locus specified by thit.
// The original mouse frame cordinates are also supplied as argu-
// ments, for the sake of exactness.
Vertex *CBezierEditor::createIntermediateVertex(Vertex *vtx,
                                                double thit,
                                                double xhit, double yhit)
{
   // thit specifies the locus of the hit from its
   // integer part (which specifies which segment
   // of the spline) and its fractional part, which
   // specifies how far along the segment.
   int    ihit = thit;                // integer    part
   double fhit = thit - (double)ihit; // fractional part
   double ghit = 1.0 - fhit;          // fractional part backwards

   double u0, u1, u2;
   double v0, v1, v2;

   Vertex *vi = vtx;
   Vertex *vf;
   int i = 0;
   while (vi != NULL) {

      if (((vf=vi->fwd)!=NULL)&&(i==ihit)) {

         // vi & vf are the endpoints of the affected segment
         double x0 = vi->x;
         double y0 = vi->y;

         double x1 = vi->xctrlnxt;
         double y1 = vi->yctrlnxt;

         double x2 = vf->xctrlpre;
         double y2 = vf->yctrlpre;

         double x3 = vf->x;
         double y3 = vf->y;

         // allocate a new vertex
         Vertex *newVertex = allocateVertex();

         // if we ran out of vertices, return NULL
         if (newVertex == NULL) return NULL;

         // connect it between vi and vf
         vi->fwd = newVertex;
         newVertex->bwd = vi;
         newVertex->fwd = vf;
         vf->bwd = newVertex;
         if (vi == splineTail)
            splineTail = newVertex;

         // think forward with the fraction fhit
         u0 = x0;
         u1 = (x1 - x0);
         u2 = (x2 - 2*x1 + x0);
         double u3 = (x3 - 3*x2 + 3*x1 - x0);

         v0 = y0;
         v1 = (y1 - y0);
         v2 = (y2 - 2*y1 + y0);
         double v3 = (y3 - 3*y2 + 3*y1 - y0);

         // here is the new attach point
         //newVertex->x = (((u3*fhit+3*u2)*fhit+3*u1)*fhit+u0);
         //newVertex->y = (((v3*fhit+3*v2)*fhit+3*v1)*fhit+v0);

         // the attach point of the new vertex is derived
         // from mouse position (xhit, yhit), rather than
         // from thit (which may be slightly off, but is
         // OK for computing the control points
         newVertex->x = xhit;
         newVertex->y = yhit;

         if ((x1==x0)&&(y1==y0)&&
             (x2==x3)&&(y2==y3)) { // vi->vf is a straight line

            // the new control points are collapsed
            newVertex->xctrlpre = newVertex->x;
            newVertex->yctrlpre = newVertex->y;

            newVertex->xctrlnxt = newVertex->x;
            newVertex->yctrlnxt = newVertex->y;
         }
         else { // vi->vf is not a straight line

            // think forward with the fraction fhit
            u0 = x0;
            u1 = (x1 - x0);
            u2 = (x2 - 2*x1 + x0);

            v0 = y0;
            v1 = (y1 - y0);
            v2 = (y2 - 2*y1 + y0);

            // two new control points
            vi->xctrlnxt = u1*fhit + u0;
            vi->yctrlnxt = v1*fhit + v0;

            newVertex->xctrlpre = (u2*fhit + 2*u1)*fhit + u0;
            newVertex->yctrlpre = (v2*fhit + 2*v1)*fhit + v0;

            // think reverse with the fraction ghit
            u0 = x3;
            u1 = (x2 - x3);
            u2 = (x1 - 2*x2 + x3);

            v0 = y3;
            v1 = (y2 - y3);
            v2 = (y1 - 2*y2 + y3);

            // two more new control points
            newVertex->xctrlnxt = (u2*ghit + 2*u1)*ghit + u0;
            newVertex->yctrlnxt = (v2*ghit + 2*v1)*ghit + v0;

            vf->xctrlpre = u1*ghit + u0;
            vf->yctrlpre = v1*ghit + v0;
         }

         // count the new vertex
         vertexCount++;

         // new vertex and new control points
         // which don't alter the spline shape
         return newVertex;
      }


      if (vf==vtx) return NULL;
      vi = vf;
      i++;
   }

   return NULL;
}

// Creates an intermediate vertex at the locus specified by thit.
// Algorithm:
//
// if { (x0,y0), (x1,y1), (x2,y2), (x3,y3) } are the four points
// of the spline segment, and if a new vertex (x', y') is to be
// inserted at thit [ 0 <= thit <= 1 ] from (x0, y0), then the
// curve between (x0, y0) and (x', y') has the form
//
//    x = u3' t**3 + u2' t**2 + u1' t + u0'
//
//    y = v3' t**3 + v2' t**2 + v1' t + v0'
//
// Because this curve is idential to the original between (x0, y0)
// and (x3, y3), we also have between (x0, y0) and (x;, y')
//
//   x = u3 (t*thit)**3 + u2 (t*thit)**2 + u1 (t*thit) + u0
//
//   y = v3 (t*thit)**3 + v2 (t*thit)**2 + v1 (t*thit) + v0
//
//  This leads to
//
//     u3' = u3 * (thit**3)
//     u2' = u2 * (thit**2)         and similar for the v's
//     u1' = u1 * (thit)
//     u0' = u0
//
//  But, according to the original scheme
//
//     u0' = x0';                           v0' = y0';
//     u1' = (x1' - x0')                    v1' = (y1' - y0');
//     u2' = (x2' - 2*x1' + x0')            v2' = (y2' - 2*y1' + y0')
//     u3' = (x3' - 3*x2' + 3*x1' - x0')    v3' = (y3' - 3*y2' + 3*y1' - y0')
//
// which allows us to work backwards and get the new control points:
//
//     x1' = u1' + u0'               y1' = v1' + v0'
//
//     x2' = u2' + 2*u1' + u0'       y2' = v2' + 2*v1' + v0'
//
//
Vertex *CBezierEditor::createAdditionalIntermediateVertex(double thit)
{
   // thit specifies the locus of the hit from its
   // integer part (which specifies which segment
   // of the spline) and its fractional part, which
   // specifies how far along the segment.
   int    ihit = thit;                // integer    part
   double fhit = thit - (double)ihit; // fractional part
   double ghit = 1.0 - fhit;          // fractional part backwards

   double u0, u1, u2, u3;
   double v0, v1, v2, v3;

   Vertex *vtx = splineHead;
   Vertex *vi = vtx;
   Vertex *vf;
   int i = 0;
   while (vi != NULL) {

      if (((vf=vi->fwd)!=NULL)&&(i==ihit)) {

         // vi & vf are the endpoints of the affected segment
         double x0 = vi->x;
         double y0 = vi->y;

         double x1 = vi->xctrlnxt;
         double y1 = vi->yctrlnxt;

         double x2 = vf->xctrlpre;
         double y2 = vf->yctrlpre;

         double x3 = vf->x;
         double y3 = vf->y;

         // allocate a new vertex
         Vertex *newVertex = allocateVertex();

         // if we ran out of vertices, return NULL
         if (newVertex == NULL) return NULL;

         // connect it between vi and vf
         vi->fwd = newVertex;
         newVertex->bwd = vi;
         newVertex->fwd = vf;
         vf->bwd = newVertex;

         // think forward with the fraction fhit
         u0 = x0;
         u1 = (x1 - x0);
         u2 = (x2 - 2*x1 + x0);
         u3 = (x3 - 3*x2 + 3*x1 - x0);

         v0 = y0;
         v1 = (y1 - y0);
         v2 = (y2 - 2*y1 + y0);
         v3 = (y3 - 3*y2 + 3*y1 - y0);

         // here is the new attach point
         newVertex->x = (((u3*fhit+3*u2)*fhit+3*u1)*fhit+u0);
         newVertex->y = (((v3*fhit+3*v2)*fhit+3*v1)*fhit+v0);

         if ((x1==x0)&&(y1==y0)&&
             (x2==x3)&&(y2==y3)) { // vi->vf is a straight line

            // the new control points are collapsed
            newVertex->xctrlpre = newVertex->x;
            newVertex->yctrlpre = newVertex->y;

            newVertex->xctrlnxt = newVertex->x;
            newVertex->yctrlnxt = newVertex->y;
         }
         else { // vi->vf is not a straight line

            // two new control points - see doc above
            vi->xctrlnxt = u1*fhit + u0;
            vi->yctrlnxt = v1*fhit + v0;

            newVertex->xctrlpre = (u2*fhit + 2*u1)*fhit + u0;
            newVertex->yctrlpre = (v2*fhit + 2*v1)*fhit + v0;

            // think reverse with the fraction ghit
            u0 = x3;
            u1 = (x2 - x3);
            u2 = (x1 - 2*x2 + x3);

            v0 = y3;
            v1 = (y2 - y3);
            v2 = (y1 - 2*y2 + y3);

            // two more new control points - see doc above
            newVertex->xctrlnxt = (u2*ghit + 2*u1)*ghit + u0;
            newVertex->yctrlnxt = (v2*ghit + 2*v1)*ghit + v0;

            vf->xctrlpre = u1*ghit + u0;
            vf->yctrlpre = v1*ghit + v0;
         }

         // count the new vertex
         vertexCount++;

         // new vertex and new control points
         // which don't alter the spline shape
         return newVertex;
      }


      if (vf==vtx) return NULL;
      vi = vf;
      i++;
   }

   return NULL;
}

bool CBezierEditor::calculateNewControlPoint(Vertex *vi, Vertex *vf, double *x, double *y)
{
   double xfwd = vi->xctrlnxt - vi->x;
   double yfwd = vi->yctrlnxt - vi->y;
   double xbwd = vf->xctrlpre - vf->x;
   double ybwd = vf->yctrlpre - vf->y;

   double denm = (xbwd*yfwd - ybwd*xfwd);
   double alfa = (vf->y - vi->y)*xbwd - (vf->x - vi->x)*ybwd;
   double beta = (vf->y - vi->y)*xfwd - (vf->x - vi->x)*yfwd;

   if (denm != 0) {

      alfa = alfa / denm;
      beta = beta / denm;

      double x0 = vi->x + alfa*xfwd;
      double y0 = vi->y + alfa*yfwd;

      double x1 = vf->x + beta*xbwd;
      double y1 = vf->y + beta*ybwd;

      *x = (x0 + x1) / 2.;
      *y = (y0 + y1) / 2.;

      return true;
   }

   return false;
}

void CBezierEditor::neutralizeVertex(Vertex *vi, Vertex *newVertex, Vertex *vf, double fhit)
{
   double ghit = 1.0 - fhit;          // fractional part backwards

   double u0, u1, u2, u3;
   double v0, v1, v2, v3;

   // vi & vf are the endpoints of the affected segment
   double x0 = vi->x;
   double y0 = vi->y;

   double x1 = vi->xctrlnxt;
   double y1 = vi->yctrlnxt;

   double x2 = vf->xctrlpre;
   double y2 = vf->yctrlpre;

   double x3 = vf->x;
   double y3 = vf->y;

   if (newVertex == NULL) return;

   // think forward with the fraction fhit
   u0 = x0;
   u1 = (x1 - x0);
   u2 = (x2 - 2*x1 + x0);
   u3 = (x3 - 3*x2 + 3*x1 - x0);

   v0 = y0;
   v1 = (y1 - y0);
   v2 = (y2 - 2*y1 + y0);
   v3 = (y3 - 3*y2 + 3*y1 - y0);

   // here is the new attach point
   newVertex->x = (((u3*fhit+3*u2)*fhit+3*u1)*fhit+u0);
   newVertex->y = (((v3*fhit+3*v2)*fhit+3*v1)*fhit+v0);

   if ((x1==x0)&&(y1==y0)&&
       (x2==x3)&&(y2==y3)) { // vi->vf is a straight line

      // the new control points are collapsed
      newVertex->xctrlpre = newVertex->x;
      newVertex->yctrlpre = newVertex->y;
      newVertex->xctrlnxt = newVertex->x;
      newVertex->yctrlnxt = newVertex->y;
   }
   else { // vi->vf is not a straight line

      // two new control points
      vi->xctrlnxt = u1*fhit + u0;
      vi->yctrlnxt = v1*fhit + v0;

      newVertex->xctrlpre = (u2*fhit + 2*u1)*fhit + u0;
      newVertex->yctrlpre = (v2*fhit + 2*v1)*fhit + v0;

      // think reverse with the fraction ghit
      u0 = x3;
      u1 = (x2 - x3);
      u2 = (x1 - 2*x2 + x3);
      v0 = y3;
      v1 = (y2 - y3);
      v2 = (y1 - 2*y2 + y3);

      // two more new control points
      newVertex->xctrlnxt = (u2*ghit + 2*u1)*ghit + u0;
      newVertex->yctrlnxt = (v2*ghit + 2*v1)*ghit + v0;

      vf->xctrlpre = u1*ghit + u0;
      vf->yctrlpre = v1*ghit + v0;
   }
}

int CBezierEditor::neutralizeAllSelectedVertices()
{
   if (vertexCount < 2) return 0;

   int totneut = 0;

   Vertex *first;
   Vertex *V0, *V1;

   V0 = splineHead;
   if (V0->bwd != NULL) {
      while (V0->select != SELECT_NONE) {
         V0 = V0->fwd;
         if ((V0 == NULL)||(V0 == splineHead)) { // no unsel verts
            return 0;
         }
      }
   }

   first = V0;

   while (true) {

      int selcnt = 0;
      V1 = V0->fwd;
      while (V1->select != SELECT_NONE) {
         selcnt++;
         if (V1->fwd == NULL) break;
         V1 = V1->fwd;
      }

      if (selcnt > 0) {

         totneut += selcnt;

         double factor = (double)(selcnt + 1);

         V0->xctrlnxt = (V0->x) + factor*(V0->xctrlnxt - V0->x);
         V0->yctrlnxt = (V0->y) + factor*(V0->yctrlnxt - V0->y);

         V1->xctrlpre = (V1->x) + factor*(V1->xctrlpre - V1->x);
         V1->yctrlpre = (V1->y) + factor*(V1->yctrlpre - V1->y);

         Vertex *run = V0;
         for (int i=0; i<selcnt; i++) {

            neutralizeVertex(run, run->fwd, V1, 1./(double)(selcnt+1 - i));
            run = run->fwd;
            run->select = SELECT_NONE;
         }
      }

      if ((V1->fwd == NULL)||(V1 == first)) break;

      V0 = V1;
   }

   return totneut;
}

int CBezierEditor::eliminateAllSelectedVertices()
{
   int totelim = 0;

   // advance past leading selected vertices
   // and determine if all vertices selected

   Vertex *src = splineHead;
   while ((src->select != SELECT_NONE)) {
      src = src->fwd;
      totelim++;
      if ((src == NULL)||(src == splineHead)) { // all selected
         newSpline();
         return 1;
      }
   }

   // first pass adjusts the control pts
   // to minimize the shape change when
   // the vertices are deleted

   Vertex *V0, *V1, *first, *last;

   if (!splineIsClosed) {

      first = src;
      first->xctrlpre = first->x;
      first->yctrlpre = first->y;

      // retract trailing selected vertices
      src = splineTail;
      while ((src->select != SELECT_NONE)) {
         src = src->bwd;
         totelim++;
      }

      last = src;
      last->xctrlnxt = last->x;
      last->yctrlnxt = last->y;

      V0 = first;
      while (V0 != last) {

         src = V0->fwd;
         int selcnt = 0;
         while (src->select != SELECT_NONE) {

            src = src->fwd;
            selcnt++;
         }

         V1 = src;

         if (selcnt > 0) {

            totelim += selcnt;

            double factor = (double)(selcnt + 1);

            V0->xctrlnxt = (V0->x) + factor*(V0->xctrlnxt - V0->x);
            V0->yctrlnxt = (V0->y) + factor*(V0->yctrlnxt - V0->y);

            V1->xctrlpre = (V1->x) + factor*(V1->xctrlpre - V1->x);
            V1->yctrlpre = (V1->y) + factor*(V1->yctrlpre - V1->y);
         }

         V0 = V1;
      }
   }
   else {

      first = src;

      V0 = first;
      while (true) {

         src = V0->fwd;
         int selcnt = 0;
         while (src->select != SELECT_NONE) {

            src = src->fwd;
            selcnt++;
         }

         V1 = src;

         if (selcnt > 0) {

            totelim += selcnt;

            double factor = (double)(selcnt + 1);

            V0->xctrlnxt = (V0->x) + factor*(V0->xctrlnxt - V0->x);
            V0->yctrlnxt = (V0->y) + factor*(V0->yctrlnxt - V0->y);

            V1->xctrlpre = (V1->x) + factor*(V1->xctrlpre - V1->x);
            V1->yctrlpre = (V1->y) + factor*(V1->yctrlpre - V1->y);
         }

         if (V1 == first) break;

         V0 = V1;
      }
   }

   // now go ahead do the delete
   deleteAllSelectedVertices();

   return totelim;
}

int CBezierEditor::eliminateAllSelectedVertices(CBezierEditor *tmplt)
{
   int totelim = 0;

   // must have same vertex count & closure cond
   if (vertexCount != tmplt->vertexCount) return false;

   bool baseClosed = (splineTail->fwd == splineHead)&&
                     (splineHead->bwd == splineTail);
   bool tmplClosed = (tmplt->splineTail->fwd == tmplt->splineHead)&&
                     (tmplt->splineHead->bwd == tmplt->splineTail);

   if (baseClosed != tmplClosed) return false;

   // advance past leading selected vertices
   Vertex *V0, *V1, *first;
   Vertex *src = tmplt->splineHead;
   Vertex *dst = splineHead;
   while (src->select != SELECT_NONE) {
      src = src->fwd;
      dst = dst->fwd;
      if ((src == NULL)||(src == tmplt->splineHead)) {
         newSpline();
         return false;
      }
   }

   // mark first unselected vertex
   first = dst;

   V0 = dst;

   while (true) {

      src = src->fwd;
      dst = dst->fwd;

      int selcnt = 0;

      // get count of selected vertices following V0
      while ((src != NULL)&&(src->select != SELECT_NONE)) {
         selcnt++;
         src = src->fwd;
         dst = dst->fwd;
      }

      if (src == NULL) { // spline not closed

         // free leading and trailing selecteds, adjusting ctrl pts
         // so that when & if the spline is closed, they go smoothly

         int selcnt = 0;

         // remove leading  selected ( < first)
         Vertex *run = splineHead;
         while (run != first) {
            Vertex *nxt = run->fwd;
            freeVertex(run);
            vertexCount--;
            selcnt++;
            run = nxt;
         }

         // remove trailing selected ( > V0)
         run = V0->fwd;
         while (run != NULL) {
            Vertex *nxt = run->fwd;
            freeVertex(run);
            vertexCount--;
            selcnt++;
            run = nxt;
         }

         double factor = (double)(selcnt + 1);

         first->xctrlpre = first->x + factor*(first->xctrlpre - first->x);
         first->yctrlpre = first->x + factor*(first->yctrlpre - first->y);

         V0->xctrlnxt = (V0->x) + factor*(V0->xctrlnxt - V0->x);
         V0->yctrlnxt = (V0->y) + factor*(V0->yctrlnxt - V0->y);

         totelim += selcnt;

         return totelim;
      }

      V1 = dst;

      if (selcnt > 0) {

         totelim += selcnt;

         double factor = (double)(selcnt + 1);

         V0->xctrlnxt = (V0->x) + factor*(V0->xctrlnxt - V0->x);
         V0->yctrlnxt = (V0->y) + factor*(V0->yctrlnxt - V0->y);

         V1->xctrlpre = (V1->x) + factor*(V1->xctrlpre - V1->x);
         V1->yctrlpre = (V1->y) + factor*(V1->yctrlpre - V1->y);

         Vertex *run = V0->fwd;
         for (int i=0; i<selcnt; i++) {
            Vertex *nxt = run->fwd;
            freeVertex(run);
            vertexCount--;
            run = nxt;
         }

         if (V0 == V1) {
            V0->fwd = NULL;
            V0->bwd = NULL;
            V0->xctrlnxt = (V0->x);
            V0->yctrlnxt = (V0->y);
            V0->xctrlpre = (V0->x);
            V0->yctrlpre = (V0->y);
         }
         else {
            V0->fwd = V1;
            V1->bwd = V0;
         }
      }

      if (V1 == first) {
         splineHead = first;
         splineTail = V0;
         break;
      }

      V0 = V1;
   }

   splineIsClosed = (splineTail->fwd == splineHead)&&(splineHead->bwd == splineTail);
   return totelim;
}

//------------------------------------------------------------------------------

void CBezierEditor::setShift(bool keydown)
{
   if (keydown)
      modKeys |= MOD_KEY_SHIFT;
   else
      modKeys &= ~MOD_KEY_SHIFT;
}

void CBezierEditor::setAlt(bool keydown)
{
   if (keydown)
      modKeys |= MOD_KEY_ALT;
   else
      modKeys &= ~MOD_KEY_ALT;
}

void CBezierEditor::setCtrl(bool keydown)
{
   if (keydown)
      modKeys |= MOD_KEY_CTRL;
   else
      modKeys &= ~MOD_KEY_CTRL;
}

void CBezierEditor::setKeyA(bool keydown)
{
   if (keydown&&(modKeys&MOD_KEY_CTRL)) {
      selectAllVertices(splineHead);
      refreshSplineFrame();
   }
}

void CBezierEditor::setKeyD(bool keydown)
{
   if (keydown&&(modKeys&MOD_KEY_CTRL)) {
      deleteAllSelectedVertices(splineHead);
      refreshSplineFrame();
   }
}

// we'll make this call return a boolean to indicate
// when the mouse-click is inside the (closed) spline
//
double CBezierEditor::mouseDown()
{
   int jordan = 0;
	double edgdst = 2000000000.;
	const int BOX_HIT_FUDGED_SIZE = BOX_SIZE + 2;

   // The current mouse client (x, y) is the
   // last one recorded by mouseMove. Ditto
   // for the mouse frame (x,y)
	RECT box;
	box.left   = mouseXClient - BOX_HIT_FUDGED_SIZE;
	box.top    = mouseYClient - BOX_HIT_FUDGED_SIZE;
	box.right  = mouseXClient + BOX_HIT_FUDGED_SIZE;
	box.bottom = mouseYClient + BOX_HIT_FUDGED_SIZE;

   DRECT dbox;
   dbox.left   = displayer->dscaleXClientToFrame(box.left);
   dbox.top    = displayer->dscaleYClientToFrame(box.top);
   dbox.right  = displayer->dscaleXClientToFrame(box.right);
   dbox.bottom = displayer->dscaleYClientToFrame(box.bottom);

   Vertex *attachPointHit;
   Vertex *controlPointHit;
   double splineHit;

   splineJustClosed = false;
   additionalVertexAdded = false;

   switch(majorState) {

      case MAJORSTATE_TRACK:

         if ((attachPointHit=boxStraddlesAttachPoint(splineHead, dbox)) != NULL) {

            // undrawing the spline in its present state
            // would be here but is no longer necessary

            // if spline is not closed
            // if captured vertex is splineHead
            //    close the spline
            if (!splineIsClosed&&
                (vertexCount >= 2)&&
                (attachPointHit == splineHead)) {
               splineTail->fwd = splineHead;
               splineHead->bwd = splineTail;
               splineTail->select &= ~SELECT_VTX;
               splineIsClosed = true;
               splineJustClosed = true;
            }

            switch(modKeys) {

               case MOD_KEY_NONE:

                  // if vertex is not selected
                  //    unselect all vertices
                  //    select captured vertex
                  // goto drag selected vertices
                  //
                  if (attachPointHit->select == SELECT_NONE) {
                     unselectAllVertices(splineHead);
                     selectedVertex = attachPointHit;
                     selectedVertex->select = SELECT_VTX;
                  }
                  majorState = MAJORSTATE_DRAG_ATTACH_POINTS;

               break;

               case MOD_KEY_SHIFT:

                  // if shift key dn & ctrl key up
                  //   if captured vertex is selected
                  //      unselect captured vertex
                  //   else if captured vertex is not selected
                  //      select captured vertex
                  //      goto drag selected vertices
                  //
                  if (attachPointHit->select != SELECT_NONE) {
                     attachPointHit->select = SELECT_NONE;
                  }
                  else {
                     selectedVertex = attachPointHit;
                     selectedVertex->select = SELECT_VTX;
                     majorState = MAJORSTATE_DRAG_ATTACH_POINTS;
                  }

               break;

               case MOD_KEY_CTRL:

                  // if shift key up & ctrl key dn
                  //    unselect all vertices
                  //    select captured vertex
                  //    collapse ctrl points
                  //    select opp-eq
                  //    goto find ctrl point
                  unselectAllVertices(splineHead);
                  selectedVertex = attachPointHit;
                  selectedVertex->select = SELECT_VTX;
                  selectedVertex->xctrlpre = selectedVertex->x;
                  selectedVertex->yctrlpre = selectedVertex->y;
                  selectedVertex->xctrlnxt = selectedVertex->x;
                  selectedVertex->yctrlnxt = selectedVertex->y;
                  selectedVertex->select |= SELECT_OPP|SELECT_EQU;
                  majorState = MAJORSTATE_FIND_CTRL;

               break;

               default:

                  // other combinations of shift, ctrl, & alt do nothing

               break;

            }

            // refresh the modified spline
            refreshSplineFrame();

            return edgdst;
         }
         else if ((controlPointHit=boxStraddlesControlPoint(splineHead, dbox)) != NULL) {

            // undrawing the spline in its present state
            // would be here but is no longer necessary

            // select the vertex and the desired control point
            selectedVertex = controlPointHit;
            selectedVertex->select = SELECT_VTX;
            if     (boxStraddlesPoint(selectedVertex->xctrlpre,selectedVertex->yctrlpre, dbox)) {
               selectedVertex->select |= SELECT_PRE;
               double nxtx = selectedVertex->xctrlnxt - selectedVertex->x;
               double nxty = selectedVertex->yctrlnxt - selectedVertex->y;
               distToOpposingControlPoint = sqrt(nxtx*nxtx+nxty*nxty);
            }
            else if (boxStraddlesPoint(selectedVertex->xctrlnxt,selectedVertex->yctrlnxt, dbox)) {
               selectedVertex->select |= SELECT_NXT;
               double prex = selectedVertex->xctrlpre - selectedVertex->x;
               double prey = selectedVertex->yctrlpre - selectedVertex->y;
               distToOpposingControlPoint = sqrt(prex*prex+prey*prey);
            }

            // CTRL key up -> vertices opposing
            // CTRL key dn -> vertices independent
            //
            if (modKeys == MOD_KEY_CTRL)
               selectedVertex->select |= SELECT_OPP;

            majorState = MAJORSTATE_DRAG_CONTROL_POINT;

            // refresh the modified spline
            refreshSplineFrame();

            return edgdst;
         }
         else if ((splineHit=boxStraddlesSpline(splineHead, dbox, &jordan, &edgdst )) >= 0.) {

            // undrawing the spline in its present state
            // would be here but is no longer necessary

            // if SHIFT key NOT down unselect all vertices
            if (!(modKeys&MOD_KEY_SHIFT))
               unselectAllVertices(splineHead);

            // create an intermediate vertex and select it
            selectedVertex = createIntermediateVertex(splineHead,
                                                      splineHit,
                                                      mouseXFrame,
                                                      mouseYFrame);
            selectedVertex->select = SELECT_VTX;
            majorState = MAJORSTATE_DRAG_ATTACH_POINTS;

            additionalVertexAdded = true;
            latestTHit = splineHit;

            // refresh the modified spline
            refreshSplineFrame();

            return edgdst;
         }

         else if (!splineIsClosed) { // (x,y) off the spline

            // undrawing the spline in its present state
            // would be here but is no longer necessary

            unselectAllVertices(splineHead);

            Vertex *tail = allocateVertex();
            if (tail!=NULL) {

               // update the links
               // splineHead->1st vertex allocated to spline
               // splineTail->most recent vertex allocated
               if (splineHead==NULL)
                  splineHead = tail;
               tail->bwd = splineTail;
               tail->fwd = NULL;
               if (splineTail!=NULL)
                  splineTail->fwd = tail;
               splineTail = tail;

               // the new vertex
               tail->xctrlpre = mouseXFrame;
               tail->yctrlpre = mouseYFrame;
               tail->x        = mouseXFrame;
               tail->y        = mouseYFrame;
               tail->xctrlnxt = mouseXFrame;
               tail->yctrlnxt = mouseYFrame;
               tail->select = SELECT_VTX|SELECT_OPP|SELECT_EQU;

               // this is the selected vertex
               selectedVertex = tail;

               // set up for finding which ctrl pt is selected
               if (tail->bwd != NULL) {

                  distSqToPrevious = sqDistance(tail->x, tail->y,
                                                tail->bwd->x, tail->bwd->y);
               }

               // count the vertex
               vertexCount++;
            }

            // refresh the modified spline
            refreshSplineFrame();

            // now go find the correct ctrl point to drag
            majorState = MAJORSTATE_FIND_CTRL;

            return edgdst;
         }
         else {

            // may be inside or outside
            if (jordan&1)
               edgdst = -edgdst;
            return edgdst;
         }

      break;

   }

   return edgdst;
}

void CBezierEditor::setMajorState(int majorstate)
{
   majorState = majorstate;
}

int CBezierEditor::getMajorState()
{
   return majorState;
}

void CBezierEditor::mouseMove(int x, int y)
{
   mouseXClient = x;
   mouseYClient = y;

   RECT box;
   box.left   = mouseXClient - BOX_SIZE;
   box.top    = mouseYClient - BOX_SIZE;
   box.right  = mouseXClient + BOX_SIZE;
   box.bottom = mouseYClient + BOX_SIZE;

   double newMouseXFrame  = displayer->dscaleXClientToFrame(x);
   double newMouseYFrame  = displayer->dscaleYClientToFrame(y);

   deltaMouseXFrame = newMouseXFrame - mouseXFrame;
   deltaMouseYFrame = newMouseYFrame - mouseYFrame;

   mouseXFrame = newMouseXFrame;
   mouseYFrame = newMouseYFrame;

   DRECT dbox;
   dbox.left   = displayer->dscaleXClientToFrame(box.left);
   dbox.top    = displayer->dscaleYClientToFrame(box.top);
   dbox.right  = displayer->dscaleXClientToFrame(box.right);
   dbox.bottom = displayer->dscaleYClientToFrame(box.bottom);

   switch(majorState) {

      case MAJORSTATE_TRACK:
      break;

      case MAJORSTATE_FIND_CTRL:

            // undrawing the spline in its present state
            // would be here but is no longer necessary

            if (!boxStraddlesPoint(selectedVertex->x, selectedVertex->y, dbox)) {

               // mouse moved off the selected vertex

               if (selectedVertex->bwd == NULL) { // selected was 1st vertex

                  // select the NXT ctrl point
                  selectedVertex->select |= SELECT_NXT;
               }

               else { // there's a previous vertex

                  if (sqDistance(mouseXFrame, mouseYFrame,
                                 selectedVertex->bwd->x, selectedVertex->bwd->y) >=

                           distSqToPrevious) {

                     // select the NXT ctrl point
                     selectedVertex->select |= SELECT_NXT;
                  }
                  else {

                     // select the PRE ctrl point
                     selectedVertex->select |= SELECT_PRE;
                  }
               }

               majorState = MAJORSTATE_DRAG_OPP_EQ;
            }

         // refresh the modified spline
         refreshSplineFrame();

      break;

      case MAJORSTATE_DRAG_OPP_EQ:

         // undrawing the spline in its present state
         // would be here but is no longer necessary

         // put the mouse coordinates in the selected ctrl point
         // make the other ctrl point opposing and equidistant
         if (selectedVertex->select & SELECT_PRE) {

            selectedVertex->xctrlpre = mouseXFrame;
            selectedVertex->yctrlpre = mouseYFrame;
            selectedVertex->xctrlnxt = 2*selectedVertex->x - mouseXFrame;
            selectedVertex->yctrlnxt = 2*selectedVertex->y - mouseYFrame;
         }
         if (selectedVertex->select & SELECT_NXT) {

            selectedVertex->xctrlpre = 2*selectedVertex->x - mouseXFrame;
            selectedVertex->yctrlpre = 2*selectedVertex->y - mouseYFrame;
            selectedVertex->xctrlnxt = mouseXFrame;
            selectedVertex->yctrlnxt = mouseYFrame;
         }

         // refresh the modified spline
         refreshSplineFrame();

      break;

      case MAJORSTATE_DRAG_ATTACH_POINTS:

         // undrawing the spline in its present state
         // would be here but is no longer necessary

         // translate all the selected vertices
         translateAllSelectedVertices(splineHead,
                                      deltaMouseXFrame, deltaMouseYFrame);

         // refresh the modified spline
         refreshSplineFrame();

      break;

      case MAJORSTATE_DRAG_CONTROL_POINT:

         // undrawing the spline in its present state
         // would be here but is no longer necessary

         // Translate the selected control point. Adjust the sibling
         // control point if SELECT_OPP is enabled.

         if (selectedVertex->select&SELECT_PRE) { // pre control point

            selectedVertex->xctrlpre += deltaMouseXFrame;
            selectedVertex->yctrlpre += deltaMouseYFrame;

            if (selectedVertex->select&SELECT_OPP) { // nxt opposes it

               double prex = selectedVertex->xctrlpre - selectedVertex->x;
               double prey = selectedVertex->yctrlpre - selectedVertex->y;
               double preleng = sqrt(prex*prex+prey*prey);
               double nxtx, nxty;

               if (preleng > 0.) {

                  prex /= preleng;
                  prey /= preleng;

                  nxtx = prex*distToOpposingControlPoint;
                  nxty = prey*distToOpposingControlPoint;

                  // adjust the nxt control point
                  selectedVertex->xctrlnxt = selectedVertex->x - nxtx;
                  selectedVertex->yctrlnxt = selectedVertex->y - nxty;
               }
            }
         }

         else if (selectedVertex->select&SELECT_NXT) { // nxt control point

            selectedVertex->xctrlnxt += deltaMouseXFrame;
            selectedVertex->yctrlnxt += deltaMouseYFrame;

            if (selectedVertex->select&SELECT_OPP) { // pre opposes it

               double nxtx = selectedVertex->xctrlnxt - selectedVertex->x;
               double nxty = selectedVertex->yctrlnxt - selectedVertex->y;
               double nxtleng = sqrt(nxtx*nxtx+nxty*nxty);
               double prex, prey;

               if (nxtleng > 0.) {

                  nxtx /= nxtleng;
                  nxty /= nxtleng;

                  prex = nxtx*distToOpposingControlPoint;
                  prey = nxty*distToOpposingControlPoint;

                  // adjust the pre control point
                  selectedVertex->xctrlpre = selectedVertex->x - prex;
                  selectedVertex->yctrlpre = selectedVertex->y - prey;
               }
            }
         }

         // refresh the modified spline
         refreshSplineFrame();

      break;
   }
}

void CBezierEditor::mouseUp()
{
   switch(majorState) {

      case MAJORSTATE_TRACK:

      break;

      case MAJORSTATE_FIND_CTRL:

         majorState = MAJORSTATE_TRACK;

      break;

      case MAJORSTATE_DRAG_OPP_EQ:

         // undrawing the spline in its present state
         // would be here but is no longer necessary

         // unselect the control point that was dragged
         selectedVertex->select &=
                       ~(SELECT_PRE|SELECT_NXT|SELECT_OPP|SELECT_EQU);

         // refresh the modified spline
         refreshSplineFrame();

         majorState = MAJORSTATE_TRACK;

      break;

      case MAJORSTATE_DRAG_ATTACH_POINTS:

         majorState = MAJORSTATE_TRACK;

      break;

      case MAJORSTATE_DRAG_CONTROL_POINT:

         // undrawing the spline in its present state
         // would be here but is no longer necessary

         // unselect the control point that was dragged
         selectedVertex->select &= ~(SELECT_PRE|SELECT_NXT);

         // refresh the modified spline
         refreshSplineFrame();

         majorState = MAJORSTATE_TRACK;

      break;
   }
}

void CBezierEditor::loadBezier(BEZIER_POINT *newBezier)
{
   // Load an array of bezier points into the Bezier Editor

   // Clear out the existing bezier.  Assuming splineHead and splineTail
   // pointers are set to NULL and vertexCount set to zero.
   newSpline();
   selectedVertex = 0;

   for (int i = 0; ; ++i)
      {
      if (i > 0 &&
          newBezier[i].xctrlpre == newBezier[0].xctrlpre &&
          newBezier[i].yctrlpre == newBezier[0].yctrlpre &&
          newBezier[i].x == newBezier[0].x &&
          newBezier[i].y == newBezier[0].y &&
          newBezier[i].xctrlnxt == newBezier[0].xctrlnxt &&
          newBezier[i].yctrlnxt == newBezier[0].yctrlnxt)
         {
         // This point must be the last point because it matches the first point
         // We do not include this point in the Bezier Editor's vertices.
         // Link the head and tail of the bezier vertices
         splineTail->fwd = splineHead;
         splineHead->bwd = splineTail;
         splineIsClosed = true;
         break;
         }

      // Make a new editor vertex
      Vertex *newVertex = allocateVertex();
      if (newVertex == 0)
         break;   // we've run out of vertices (should never happen)

      if (splineHead == 0)
         splineHead = newVertex;     // remember the first vertex

      if (splineTail != 0)
         splineTail->fwd = newVertex;
      newVertex->bwd = splineTail;
      splineTail = newVertex;

      newVertex->fwd = 0;

      // Copy the caller's bezier point
      newVertex->xctrlpre = newBezier[i].xctrlpre;
      newVertex->yctrlpre = newBezier[i].yctrlpre;
      newVertex->x = newBezier[i].x;
      newVertex->y = newBezier[i].y;
      newVertex->xctrlnxt = newBezier[i].xctrlnxt;
      newVertex->yctrlnxt = newBezier[i].yctrlnxt;

      newVertex->select = SELECT_NONE;

      vertexCount++;
      }
}

int CBezierEditor::selectVertices(RECT& selrct)
{
   int totsel = 0;

   Vertex *vi = splineHead;
   while (vi != NULL) {

      if ((vi->x >= selrct.left)&&(vi->x <= selrct.right)&&
          (vi->y >= selrct.top)&&(vi->y <= selrct.bottom)) {

         if (vi->select == SELECT_NONE) {
            vi->select = SELECT_VTX;
         }
         else {
            vi->select = SELECT_NONE;
         }

         totsel++;
      }
      vi = vi->fwd;
      if (vi==splineHead) break;
   }

   return totsel;
}

void CBezierEditor::unselectAllVertices()
{
   unselectAllVertices(splineHead);
}

bool CBezierEditor::areAnyVerticesSelected()
{
   return areAnyVerticesSelected(splineHead);
}

void CBezierEditor::deleteAllSelectedVertices()
{
   deleteAllSelectedVertices(splineHead);
}

void CBezierEditor::loadBezier(Vertex *newVertices)
{
   // Load an array of bezier vertices into the Bezier Editor

   // Clear out the existing bezier. Assuming splineHead and splineTail
   // pointers are set to NULL and vertexCount set to zero.
   newSpline();
   selectedVertex = 0;

   Vertex *srcVertex = newVertices;
   if (srcVertex == NULL) return;
   
   Vertex *dstVertex;
   dstVertex = allocateVertex();
   *dstVertex = *srcVertex;
   dstVertex->select = SELECT_NONE;
   splineHead = splineTail = dstVertex;
   splineHead->bwd = NULL;
   splineHead->fwd = NULL;

   vertexCount++;

   while (srcVertex->fwd) {

	  splineIsClosed = ((srcVertex->fwd == newVertices)&&
							(newVertices->bwd == srcVertex));
	  if (splineIsClosed) break;

      srcVertex = srcVertex->fwd;
      dstVertex = allocateVertex();
      *dstVertex = *srcVertex;
      dstVertex->select = SELECT_NONE;
      splineTail->fwd = dstVertex;
      dstVertex->bwd = splineTail;
      splineTail = dstVertex;
      splineTail->fwd = NULL;

      vertexCount++;
   }

   if (splineIsClosed) {
      splineHead->bwd = splineTail;
      splineTail->fwd = splineHead;
   }
}

void CBezierEditor::loadSelectState(Vertex *newVertices)
{
   // Load the select state of one Bezier into another

   Vertex *src = newVertices;
   Vertex *dst = splineHead;

   if ((src == NULL)||(dst == NULL)) return;

   dst->select = src->select;
   src = src->fwd; dst = dst->fwd;
   while ((src != NULL)&&(src != newVertices)&&(dst != NULL)&&(dst != splineHead)) {
      dst->select = src->select;
      src = src->fwd; dst = dst->fwd;
   }
}

bool CBezierEditor::interpolateVertices(CBezierEditor *bef,
                                        CBezierEditor *aft,
                                        double combin)
{
   if ((vertexCount != bef->vertexCount)||(vertexCount != aft->vertexCount))
      return false;

   double fr1 = combin;
   double fr2 = 1.0 - combin;

   Vertex *dst  =      splineHead;
   Vertex *src1 = bef->splineHead;
   Vertex *src2 = aft->splineHead;

   while (dst != NULL) {

      dst->xctrlpre = src1->xctrlpre*fr1 + src2->xctrlpre*fr2;
      dst->yctrlpre = src1->yctrlpre*fr1 + src2->yctrlpre*fr2;

      dst->x = src1->x*fr1 + src2->x*fr2;
      dst->y = src1->y*fr1 + src2->y*fr2;

      dst->xctrlnxt = src1->xctrlnxt*fr1 + src2->xctrlnxt*fr2;
      dst->yctrlnxt = src1->yctrlnxt*fr1 + src2->yctrlnxt*fr2;

      if (dst == splineTail) break;

      dst  = dst->fwd;
      src1 = src1->fwd;
      src2 = src2->fwd;
   }

   return true;
}


