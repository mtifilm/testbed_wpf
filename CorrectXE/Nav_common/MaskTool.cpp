///////////////////////////////////////////////////////////////////////////////
//
// MaskTool.cpp: implementation of the CMaskTool class.
//
///////////////////////////////////////////////////////////////////////////////

#include "MaskTool.h"

#include "BezierEditor.h"
#include "Displayer.h"
#include "IniFile.h"
#include "MaskTransformEditor.h"
#include "MTIDialogs.h"
#include "MTIstringstream.h"
#include "PDL.h"
#include "ToolSystemInterface.h"
#include <time.h>

#include <fstream>

#ifdef __BORLANDC__
#include "MainWindowUnit.h"
#endif // #ifdef __BORLANDC__

#ifdef __sgi
#include "MainWindow.h"                                         a
#endif // #ifdef __sgi

//////////////////////////////////////////////////////////////////////
// Static Variables
//////////////////////////////////////////////////////////////////////

#ifdef __sgi
// Mask Tool's pointer to the Main Window
MainWindow *CMaskTool::mainWindow = 0;
#endif // #ifdef __sgi

string CMaskTool::maskToolSectionName = "MaskTool";
string CMaskTool::maskToolEnabledKey = "MaskToolEnabled";
string CMaskTool::exclusionBorderWidthKey = "ExclusionBorderWidth";
string CMaskTool::exclusionBlendSlewKey = "ExclusionBlendSlew";
string CMaskTool::inclusionBorderWidthKey = "InclusionBorderWidth";
string CMaskTool::inclusionBlendSlewKey = "InclusionBlendSlew";

const string CMaskTool::tag_MaskRegion = "MaskRegion";
const string CMaskTool::enabledAttr = "Enabled";
const string CMaskTool::inclusionModeAttr = "Mode";
const string CMaskTool::blendSlewAttr = "BlendSlew";
// const string CMaskTool::blendPositionCentered = "Centered";
// const string CMaskTool::blendPositionInside =   "Inside";
// const string CMaskTool::blendPositionOutside =  "Outside";
const string CMaskTool::borderWidthAttr = "BorderWidth";
const string CMaskTool::inclusionFlagAttr = "InclusionFlag";
const string CMaskTool::hiddenFlagAttr = "HiddenFlag";
const string CMaskTool::pointsAttr = "Points";
const string CMaskTool::shapeTypeAttr = "ShapeType";
const string CMaskTool::shapeTypeBezier = "Bezier";
const string CMaskTool::shapeTypeLasso = "Lasso";
const string CMaskTool::shapeTypeRect = "Rect";
const string CMaskTool::shapeTypeQuad = "Quad";

const string CMaskKeyframe::tag_Keyframe = "Keyframe";
const string CMaskKeyframe::frameIndexAttr = "FrameIndex";

const string CMaskToolTimeline::tag_MaskTimeline = "MaskTimeline";

struct SBlendPositionNameTableEntry
{
   double blendSlew;
   const char *name;
};

// static SBlendPositionNameTableEntry blendPositionNameTable[] =
// {
// {MASK_BLEND_INSIDE,           "Inside"  },
// {MASK_BLEND_OUTSIDE,          "Outside" },
// {MASK_BLEND_CENTERED,         "Centered"},
// {MASK_BLEND_POSITION_INVALID, NULL      }  // table terminator
// };

class MaskChangedAutoNotifier
{
   CMaskTool *_maskTool = nullptr;
   static string _oldMaskString;
   static bool _oldMaskVisibility;
   static int nestCount;

public:
   MaskChangedAutoNotifier(CMaskTool *maskTool) : _maskTool(maskTool)
   {
      ++nestCount;
   }

   ~MaskChangedAutoNotifier()
   {
      if (--nestCount > 0)
      {
         return;
      }

      if (_maskTool->IsMaskRoiModeActive())
      {
         // Can't get mask while in ROI mode!
         return;
      }

      if (_maskTool->IsMaskVisible() != _oldMaskVisibility)
      {
         _oldMaskVisibility = _maskTool->IsMaskVisible();
         _maskTool->getSystemAPI()->NotifyMaskVisibilityChanged();
      }

      auto newMaskString = _maskTool->GetMaskAsString();
      if (newMaskString == _oldMaskString)
      {
         // Didn't actually change externally.
         return;
      }

      _oldMaskString = newMaskString;
      _maskTool->getSystemAPI()->NotifyMaskChanged(newMaskString);
   }

   MaskChangedAutoNotifier(const MaskChangedAutoNotifier &rhs) = delete;
   MaskChangedAutoNotifier &operator==(const MaskChangedAutoNotifier &rhs) = delete;
};

string MaskChangedAutoNotifier::_oldMaskString;
bool MaskChangedAutoNotifier::_oldMaskVisibility = false;
int MaskChangedAutoNotifier::nestCount = 0;

///////////////////////////////////////////////////////////////////////////////

CRegionNode::CRegionNode(CDisplayer *dsp)
{
   bwd = fwd = NULL;

   isSelected = false;

   hiddenFlag = false;

   blendSlew = 0.0;

#ifdef USE_PER_REGION_INCLUSION_FLAGS
   inclusionFlag = true;
#endif

   borderWidth = 0;

   bezierEditor = new CBezierEditor(dsp);

   xformBezierEditor = new CBezierEditor(dsp);
}

CRegionNode::CRegionNode(const CRegionNode& nod)
{
   bwd = fwd = NULL;

   isSelected = nod.isSelected;

   hiddenFlag = nod.hiddenFlag;

   blendSlew = nod.blendSlew;

#ifdef USE_PER_REGION_INCLUSION_FLAGS
   inclusionFlag = nod.inclusionFlag;
#endif

   borderWidth = nod.borderWidth;

   bezierEditor = new CBezierEditor(*(nod.bezierEditor));

   xformBezierEditor = new CBezierEditor(*(nod.xformBezierEditor));
}

CRegionNode::~CRegionNode()
{
   delete xformBezierEditor;

   delete bezierEditor;
}

///////////////////////////////////////////////////////////////////////////////

namespace
{

// This HACK version checks to make sure the lasso is closed, but to do
// that it needs to assume that lassos are always allocated as an array
// of MAX_LASSO_PTS points!!
bool CopyLasso(const POINT *srcLasso, POINT *dstLasso)
{
   bool retVal = false;

   if (srcLasso == NULL || dstLasso == NULL)
   {
      return false;
   } // oops, no lasso, so just return "not closed"

   memcpy(dstLasso, srcLasso, MAX_LASSO_PTS * sizeof(POINT));

   for (int i = 3; retVal == false && i < MAX_LASSO_PTS; ++i)
   {
      if (srcLasso[i].x == srcLasso[0].x && srcLasso[i].y == srcLasso[0].y)
      {
         retVal = true;
      }
   }

   return retVal; // true if closed
}

} // end anonymous namespace

///////////////////////////////////////////////////////////////////////////////
// Constructor/Destructor
///////////////////////////////////////////////////////////////////////////////

CMaskTool::CMaskTool()
{
   // keyframe not bound to a frame used
   // to hold the interpolated regions
   _globalKeyframe = new CMaskKeyframe;

   // Mask Transform editor
   _maskTransformEditor = new CMaskTransformEditor(mainWindow->GetDisplayer());

   // Mask Timeline
   _timeline = new CMaskToolTimeline;

   _animateIFPtr = new CMaskToolAnimateTimelineIF(this);
}

CMaskTool::~CMaskTool()
{
   delete _timeline;

   delete _maskTransformEditor;

   // delete _globalKeyframe;
   delete _globalKeyframe;

   // Codeguard reported this leaking 20090101
   delete _animateIFPtr;
}

///////////////////////////////////////////////////////////////////////////////
// Accessors
///////////////////////////////////////////////////////////////////////////////

bool CMaskTool::getGlobalInclusionFlag() const
{
   return _globalInclusionFlag;
}
//---------------------------------------------------------------------------

bool CMaskTool::getMaskRoiModeFlag() const
{
   return _roiModeActiveFlag;
}
//---------------------------------------------------------------------------

double CMaskTool::getDefaultExclusionBlendSlew() const
{
   return _defaultExclusionBlendSlew;
}
//---------------------------------------------------------------------------

double CMaskTool::getDefaultInclusionBlendSlew() const
{
   return _defaultInclusionBlendSlew;
}
//---------------------------------------------------------------------------

int CMaskTool::getDefaultExclusionBorderWidth() const
{
   return _defaultExclusionBorderWidth;
}
//---------------------------------------------------------------------------

int CMaskTool::getDefaultInclusionBorderWidth() const
{
   return _defaultInclusionBorderWidth;
}
//---------------------------------------------------------------------------

EMaskRegionShape CMaskTool::getRegionShape() const
{
   if (_roiModeActiveFlag)
   {
      if (_roiModeReverseRectAndLassoFlag && (_maskRoiRegionShape == MASK_REGION_RECT))
      {
         return MASK_REGION_LASSO;
      }
      else if (_roiModeReverseRectAndLassoFlag && (_maskRoiRegionShape == MASK_REGION_LASSO))
      {
         return MASK_REGION_RECT;
      }

      return _maskRoiRegionShape;
   }

   return _maskRegionShape;
}
//---------------------------------------------------------------------------

// called by "loadTargetFrame" method to render the
// mask before executing the macro on the loaded frame
//
void CMaskTool::CalculateRegionOfInterest(int frameIndex)
{
   _timeline->ComputeInterpolatedKeyframe(frameIndex, _globalKeyframe);

   RenderRegionOfInterest(true);

   setRegionOfInterestAvailable(true);

   _currentFrame = frameIndex;
   _globalKeyframe->frameIndex = _currentFrame;
}
//---------------------------------------------------------------------------

static CSpinLock RoiLock;

// returns ptr to tool's Region of Interest
// with the mask rendered into it
//
CRegionOfInterest* CMaskTool::GetRegionOfInterest()
{
   CAutoSpinLocker lock(RoiLock);
   RenderRegionOfInterest(false);

   return &_regionOfInterest;

}
//---------------------------------------------------------------------------

int CMaskTool::GetRegionOfInterest(int frameIndex, CRegionOfInterest &maskRoi, double scale)
{
   // renders the mask into the caller's
   // Region of Interest

   int retVal;
   if (IsMaskAnimated())
   {

      // It wasn't obvious to me but it seems we pass in the _globalKeyframe's
      // region list because it is essentially used as a template for the
      // mask's regions/vertices, and the interpolation will merely modify
      // the positions of those vertices; apparently we are guaranteed here
      // that the _globalKeyframe has been initialized, which I guess happens
      // when any frame in the clip is displayed.

      CMaskKeyframe *localKeyframe = new CMaskKeyframe(frameIndex, _globalKeyframe->regionListHead);

      _timeline->ComputeInterpolatedKeyframe(frameIndex, localKeyframe);
      retVal = RenderRegionOfInterest(maskRoi, localKeyframe->regionListHead, scale);
      delete localKeyframe;
   }
   else
   {
      retVal = RenderRegionOfInterest(maskRoi, _globalKeyframe->regionListHead, scale);
   }

   return retVal;
}

///////////////////////////////////////////////////////////////////////////////
// Mutators
///////////////////////////////////////////////////////////////////////////////

void CMaskTool::setOneShotRoiMaskColor(int color)
{
   _oneTimeMaskColor = color;
}
//---------------------------------------------------------------------------

void CMaskTool::saveEnabledStateAndDisable()
{
   _maskToolSaveState = _maskToolActivationAllowedFlag;
   setMaskToolActivationAllowed(false);
}
//---------------------------------------------------------------------------

void CMaskTool::restoreSavedEnabledState()
{
   setMaskToolActivationAllowed(_maskToolSaveState);
}
//---------------------------------------------------------------------------

void CMaskTool::setClipActivePictureRect(const RECT &newActivePictureRect)
{
   MaskChangedAutoNotifier notifier(this);

   if (_clipActivePictureRect.left == newActivePictureRect.left
   && _clipActivePictureRect.top == newActivePictureRect.top
   && _clipActivePictureRect.right == newActivePictureRect.right
   && _clipActivePictureRect.bottom == newActivePictureRect.bottom)
   {
      // Didn't change!
      return;
   }

   // If the active picture Rect changes, clear the current
   // region list and animated mask keyframes
   DeleteAllKeyframes(false);
   ClearMask();
   _clipActivePictureRect = newActivePictureRect;
   _regionOfInterestAllocated = false;
}
//---------------------------------------------------------------------------

void CMaskTool::setGlobalInclusionFlag(bool newInclusionFlag)
{
   MaskChangedAutoNotifier notifier(this);

   if (newInclusionFlag != _globalInclusionFlag)
   {
      // invert the inclusion flag
      _globalInclusionFlag = !_globalInclusionFlag;

      // Synchronize the state of the 'global key frame' if a mask is defined
      if (IsMaskAvailable())
      {
#ifdef USE_PER_REGION_INCLUSION_FLAGS
         CRegionNode *rgnlst = _globalKeyframe->regionListHead;

         for (CRegionNode * rgn = rgnlst; rgn != NULL; rgn = rgn->fwd)
         {
            rgn->inclusionFlag = _globalInclusionFlag;
         }
#endif

         // Make a new keyframe if needed, otherwise replace the
         // region list of the existing keyframe with the global frame's
         SetKeyframe(_currentFrame);
      }

      // A hack to tell us to re-render the region of interest from the
      // individual mask regions
      setRegionOfInterestAvailable(false);

      mainWindow->UpdateMaskToolGUI(*this);
      RefreshFrame();
   }
}
//---------------------------------------------------------------------------

void CMaskTool::setDefaultExclusionBlendSlew(double newBlendSlew)
{
   MaskChangedAutoNotifier notifier(this);

   _defaultExclusionBlendSlew = newBlendSlew;
   setRegionOfInterestAvailable(false);
}
//---------------------------------------------------------------------------

void CMaskTool::setDefaultInclusionBlendSlew(double newBlendSlew)
{
   MaskChangedAutoNotifier notifier(this);

   _defaultInclusionBlendSlew = newBlendSlew;
   setRegionOfInterestAvailable(false);
}
//---------------------------------------------------------------------------

void CMaskTool::setDefaultExclusionBorderWidth(int newBorderWidth)
{
   MaskChangedAutoNotifier notifier(this);

   _defaultExclusionBorderWidth = newBorderWidth;
   setRegionOfInterestAvailable(false);
}
//---------------------------------------------------------------------------

void CMaskTool::setDefaultInclusionBorderWidth(int newBorderWidth)
{
   MaskChangedAutoNotifier notifier(this);

   _defaultInclusionBorderWidth = newBorderWidth;
   setRegionOfInterestAvailable(false);
}
//---------------------------------------------------------------------------

void CMaskTool::setMaskVisible(bool newVisible)
{
   MaskChangedAutoNotifier notifier(this);

   if (newVisible != _maskIsVisibleFlag)
   {
      _maskIsVisibleFlag = newVisible;
      mainWindow->UpdateMaskToolGUI(*this);
      RefreshFrame();
   }
}
//---------------------------------------------------------------------------

void CMaskTool::setMaskVisibilityOffLock(bool flag)
{
   if (_maskVisibilityIsLockedOff == flag)
   {
      return;
   }

   _maskVisibilityIsLockedOff = flag;
   setMaskVisible(!_maskVisibilityIsLockedOff);
}
//---------------------------------------------------------------------------

bool CMaskTool::IsMaskVisibilityLockedOff() const
{
   return _maskVisibilityIsLockedOff;
}
//---------------------------------------------------------------------------

void CMaskTool::setMaskToolActivationAllowed(bool maskToolAllowed, bool roiModeAllowed, bool activateRoiMode)
{
   MaskChangedAutoNotifier notifier(this);

   // One thing to keep in mind is NORMAL and ROI modes are mutually exclusive.
   // Operationally, the mask tool can be in one of four states as defined by
   // these four flags:
   //
   // State       MaskToolAllowed  MaskToolActive  RoiModeAllowed  RoiModeActive
   // ----------  ---------------  --------------    --------------  -------------
   // DISABLED:       false           false            false          false
   // INACTIVE:       true            false         true or false     false
   // NORMAL MODE:    true            true          true or false     false
   // ROI MODE:       true            true             true           true
   //
   // ALL OTHER COMBINATIONS ARE ILLEGAL!

   // Sanity
   MTIassert(maskToolAllowed || !roiModeAllowed);
   MTIassert(roiModeAllowed || !activateRoiMode);
   roiModeAllowed = maskToolAllowed ? roiModeAllowed : false;
   activateRoiMode = roiModeAllowed ? activateRoiMode : false;

   // First see if we want to end up with the mask tool enabled.
   auto activateMaskTool = !_maskToolActivationAllowedFlag
                           && maskToolAllowed
                           && !activateRoiMode
                           && _previousMaskToolActivationState;


   // If we need to turn off ROI mode, do it first.
   if (!activateRoiMode && _roiModeActiveFlag)
   {
      ToggleRoiModeActivation();
   }
   else if (_maskToolActivationAllowedFlag && !maskToolAllowed && !_roiModeActiveFlag)
   {
      // If we're disabling the mask, remember the current state
      // so we can restore it when it's re-enabled. DO NOT CHANGE the "previous
      // state" if the current tool is in ROI mode!!
      _previousMaskToolActivationState = _maskToolActiveFlag;
   }


   _maskToolActivationAllowedFlag = maskToolAllowed;
   _roiModeActivationAllowedFlag = roiModeAllowed;

   // Deactivate the tool if we're disabling it.
   if (!_maskToolActivationAllowedFlag && _maskToolActiveFlag)
   {
      _maskToolActiveFlag = false;
   }

   // Activate the mask tool if it was active when we last disabled it.
   if (activateMaskTool)
   {
      ActivateNormalMode(true);
   }

   // Now safe to turn on ROI mode if desired.
   if (activateRoiMode && !_roiModeActiveFlag)
   {
      ToggleRoiModeActivation();
   }

   // Update GUI and mask state.
   mainWindow->UpdateMaskToolGUI(*this);
   RefreshFrame();
}
//---------------------------------------------------------------------------

void CMaskTool::ActivateNormalMode(bool newActive)
{
   MaskChangedAutoNotifier notifier(this);

   // DISABLED state?
   if (!IsMaskToolActivationAllowed())
   {
      MTIassert(!_roiModeActiveFlag);
      _maskToolActiveFlag = false;
      _roiModeActivationAllowedFlag = false;
      _roiModeActiveFlag = false;
      return;
   }

   // Sanity checks for states which should be impossible to get into.
   MTIassert(_maskToolActiveFlag || !_roiModeActiveFlag);
   MTIassert(_roiModeActivationAllowedFlag || !_roiModeActiveFlag);

   // If we're in ROI mode, we need to turn that off first.
   if (_roiModeActiveFlag && newActive)
   {
      ToggleRoiModeActivation();
   }

   // No-op?
   if (newActive == _maskToolActiveFlag)
   {
      // Already in the correct state.
      return;
   }

   _maskToolActiveFlag = newActive;

   // Need to re-render mask!
   setRegionOfInterestAvailable(false);

   mainWindow->UpdateMaskToolGUI(*this);
   RefreshFrame();
}
//---------------------------------------------------------------------------

bool CMaskTool::ActivateRoiMode(bool newActive)
{
   // This just tries to set the mask ROI mode and returns true if mode matches
   // Basically toggle if not in this mode
   if (_roiModeActiveFlag == newActive)
   {
      return true;
   }

   return ToggleRoiModeActivation();
}
//---------------------------------------------------------------------------

bool CMaskTool::ToggleRoiModeActivation()
{
   MaskChangedAutoNotifier notifier(this);

   bool retVal = false;
   MTIassert(_roiModeActivationAllowedFlag || !_roiModeActiveFlag);

   if (_roiModeActivationAllowedFlag && (!_roiModeActiveFlag))
   {
      // Ask the tool if it's OK to go into ROI mode now.
      if (getSystemAPI()->NotifyStartROIMode())
      {
         // It's OK!
         // Ugh, DO THIS FIRST so SaveMaskToMemory() works!
         _maskToolActiveFlag = true;

         // Save the mask state so we can restore it when we exit 'mask ROI' mode
         SaveMaskToMemory(_maskMemoryPDL);

         // OK to turn this on now.
         _roiModeActiveFlag = true;

         // Always inclusive when we get to mask ROI mode this way
         _roiModeOldInclusionFlag = _globalInclusionFlag;
         _globalInclusionFlag = true;

         // ROI mode always starts from scratch.
         ClearAll();

         retVal = true;
      }
      else
      {
         //// Beep();
      }
   }
   else if (_roiModeActiveFlag)
   {
      // Tell the active tool that we're popping out of 'ROI' mode
      getSystemAPI()->NotifyEndROIMode();

      // Turn off ROI mode and deactivate the mask tool so it doesn't
      // activate normal mode. Do this BEFORE restoring the old mask!!
      _roiModeActiveFlag = false;
      _maskToolActiveFlag = false;

      // Restore old mask state
      LoadMaskFromMemory(_maskMemoryPDL, true);
      DestroySavedMemoryMask(_maskMemoryPDL);
      DestroySavedMemoryMask(_maskRoiMemoryPDL);  // ??? QQQ

      // Restore global inclusion flag
      _globalInclusionFlag = _roiModeOldInclusionFlag;

      retVal = true;
   }

   if (retVal)
   {
      // Sync the toolbar, menu and _timeline and redisplay the frame to
      // draw or erase the mask from the previous state.
      mainWindow->UpdateMaskToolGUI(*this);
      mainWindow->GetTimeline()->RedrawMTL();
      RefreshFrame();
   }

   return retVal;
}
//---------------------------------------------------------------------------

void CMaskTool::setRegionShape(EMaskRegionShape newRegionShape)
{
   MaskChangedAutoNotifier notifier(this);

   if (_roiModeActiveFlag)
   {
      _maskRoiRegionShape = newRegionShape;
   }
   else
   {
      _maskRegionShape = newRegionShape;
   }

   mainWindow->UpdateMaskToolGUI(*this);
}
//---------------------------------------------------------------------------

void CMaskTool::cycleRegionShape()
{
   MaskChangedAutoNotifier notifier(this);
   EMaskRegionShape newRegionShape;

   switch (getRegionShape())
   {

   case MASK_REGION_RECT:

      newRegionShape = MASK_REGION_LASSO;

      break;

   case MASK_REGION_LASSO:

      newRegionShape = MASK_REGION_BEZIER;

      break;

   case MASK_REGION_BEZIER:
   default:

      newRegionShape = MASK_REGION_RECT;

      break;
   }

   setRegionShape(newRegionShape);
}
//---------------------------------------------------------------------------

void CMaskTool::setSystemAPI(CToolSystemInterface * newSystemAPI)
{
   _baseSystemAPI = newSystemAPI;
}
//---------------------------------------------------------------------------

CToolSystemInterface* CMaskTool::getSystemAPI()
{
   return _baseSystemAPI;
}
//---------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
// Tool Event Handlers
///////////////////////////////////////////////////////////////////////////////

// Haha this makes NO frickin sense. Why would you blindly set the
// global keyframe's frame index to match the current frame, then not
// recompute the regions, but return whatever crap happened to be previously
// conputed???
CRegionNode * CMaskTool::SetKeyframeContext()
{
   MaskChangedAutoNotifier notifier(this);

   _currentFrame = getSystemAPI()->getLastFrameIndex();
   _globalKeyframe->frameIndex = _currentFrame;

   return (_globalKeyframe->regionListHead);
}

#ifdef _DEBUG
bool saw_button_down = false;
#endif

const int AutoRoiActivationSaneMoveDistance = 100; // pixels

int CMaskTool::MouseMove(int x, int y)
{
   if (_autoRoiModeActivationIsPending)
   {
      _autoRoiModeActivationIsPending = false;

      auto xMove = _mouseXAtClick - x;
      auto yMove = _mouseYAtClick - y;
      auto distanceMovedSinceClick = sqrt((xMove * xMove) + (yMove * yMove));
      if (!_roiModeActiveFlag
      && distanceMovedSinceClick < AutoRoiActivationSaneMoveDistance)
      {
         ActivateRoiMode(true);
         if (!_roiModeActiveFlag)
         {
            // Tool disallowed ROI mode.
            return TOOL_RETURN_CODE_EVENT_CONSUMED;
         }
      }
      else
      {
         TRACE_0(errout << "((((((((( THWARTED BOGUS ROI ACTIVATION )))))))))))");

         // tell the base system to stop stretching
         getSystemAPI()->stopStretchRectangleOrLasso();
         mouseMode = MASK_TOOL_MOUSE_MODE_NONE;
         return TOOL_RETURN_CODE_EVENT_CONSUMED;
      }
   }

   for (CRegionNode * rgn = _globalKeyframe->regionListHead; rgn != NULL; rgn = rgn->fwd)
   {
      rgn->bezierEditor->mouseMove(x, y);
      rgn->xformBezierEditor->mouseMove(x, y);
   }

   _maskTransformEditor->mouseMove(x, y, _maskTransformEditorIdleRefreshMode);

   // save current mouse client coords
   _mouseX = x;
   _mouseY = y;

   // and frame coords
   _mouseFrameX = getSystemAPI()->dscaleXClientToFrame(x) + 0.5;
   _mouseFrameY = getSystemAPI()->dscaleYClientToFrame(y) + 0.5;

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//---------------------------------------------------------------------------

int CMaskTool::SelectDn()
{
#ifdef _DEBUG
   if (saw_button_down)
   {
      TRACE_0(errout << "((((((((( EXTRA BUTTON DOWN )))))))))))");
   }
   saw_button_down = true;
#endif

   _mouseXAtClick = _mouseX;
   _mouseYAtClick = _mouseY;

   if (_roiModeActiveFlag && _maskRoiModeLockFlag)
   {
      // Locked out!
      return TOOL_RETURN_CODE_NO_ACTION;
   }

   if (!_maskToolActiveFlag
   && !_maskRoiModeLockFlag
   && _autoActivateRoiModeOnDrawFlag)
   {
      _autoRoiModeActivationIsPending = true;
      if (_maskRoiModeShape != MASK_REGION_LASSO)
      {
         _maskRoiModeShape = MASK_REGION_RECT;
      }

   }
   else if (_maskToolActiveFlag)
   {
      _autoRoiModeActivationIsPending = false;
   }
   else
   {
      // Mask is inactive and auto-ROI-activation is off.
      return TOOL_RETURN_CODE_NO_ACTION;
   }

   SetMaskBeingDrawn(true);

   CRegionNode *regionListHead = SetKeyframeContext();
   if (_roiModeActiveFlag && (regionListHead != NULL) && (regionListHead->bezierEditor->isClosed()))
   {
      // Can't edit a mask ROI region once it's closed, so start a new region!
   }
   else
   {
      _selectModKeys = SELECT_NONE;

      CRegionNode *regionListHead = SetKeyframeContext();

      int majorState;
      _regionJustClosed = NULL;
      _regionJustEdited = NULL;
      int regionIndex = 0;
      _newVertexRegionIndex = -1;
      for (CRegionNode * rgn = regionListHead; rgn != NULL; rgn = rgn->fwd)
      {
         if (!rgn->isSelected)
         {
            rgn->bezierEditor->setShift(false);
            rgn->bezierEditor->setAlt(false);
            rgn->bezierEditor->setCtrl(false);
            rgn->bezierEditor->mouseDown();
            if (rgn->bezierEditor->wasSplineJustClosed())
            {
               _regionJustClosed = rgn;

               if (_roiModeActiveFlag)
               {
                  // Save the ROI shape for ~ key hack
                  _maskRoiModeShape = MASK_REGION_BEZIER;
                  rgn->bezierEditor->getCompletedSplineExtent(_maskRoiModeRect);
                  delete[]_maskRoiModeBezier;
                  _maskRoiModeBezier = rgn->bezierEditor->getCompletedSpline();
               }
            }

            if (rgn->bezierEditor->wasAdditionalVertexAdded())
            {
               _newVertexRegionIndex = regionIndex;
               _newVertexLocus = rgn->bezierEditor->getLatestTHit();
            }

            majorState = rgn->bezierEditor->getMajorState();
         }
         else
         {
            rgn->xformBezierEditor->setShift(false);
            rgn->xformBezierEditor->setAlt(false);
            rgn->xformBezierEditor->setCtrl(false);
            rgn->xformBezierEditor->mouseDown();
            if (rgn->xformBezierEditor->wasSplineJustClosed())
            {
               _regionJustClosed = rgn;

               if (_roiModeActiveFlag)
               {
                  // Save the ROI shape for ~ key hack
                  _maskRoiModeShape = MASK_REGION_BEZIER;
                  rgn->xformBezierEditor->getCompletedSplineExtent(_maskRoiModeRect);
                  delete[]_maskRoiModeBezier;
                  _maskRoiModeBezier = rgn->bezierEditor->getCompletedSpline();
               }
            }

            if (rgn->xformBezierEditor->wasAdditionalVertexAdded())
            {
               _newVertexRegionIndex = regionIndex;
               _newVertexLocus = rgn->bezierEditor->getLatestTHit();
            }

            majorState = rgn->xformBezierEditor->getMajorState();
         }

         if (majorState != MAJORSTATE_TRACK)
         {
            _regionJustEdited = rgn;

            for (CRegionNode * rgna = regionListHead; rgna != NULL; rgna = rgna->fwd)
            {
               if (!rgna->isSelected)
               {
                  if (rgna != rgn)
                  {
                     rgna->bezierEditor->unselectAllVertices();
                  }
               }
               else
               {
                  // move spline from xformBezierEditor to bezierEditor
                  Vertex *xformBezierVertices = rgna->xformBezierEditor->getSplineVertices();
                  rgna->bezierEditor->loadBezier(xformBezierVertices);

                  rgna->isSelected = false;
               }
            }

            _globalKeyframe->regionSelectedCount = 0;

            // feed the event back in
            // rgn->bezierEditor->setShift(false);
            // rgn->bezierEditor->setAlt(false);
            // rgn->bezierEditor->setCtrl(false);
            // rgn->bezierEditor->mouseDown();
            rgn->bezierEditor->setMajorState(majorState);

            RefreshFrame();

            return TOOL_RETURN_CODE_EVENT_CONSUMED;
         }

         regionIndex++;
      }

      if (_globalKeyframe->regionSelectedCount > 0)
      {

         _maskTransformEditor->setShift(false);
         _maskTransformEditor->setAlt(false);
         _maskTransformEditor->setCtrl(false);
         _maskTransformEditor->mouseDown();
         if (_maskTransformEditor->getMajorState() != MAJORSTATE_TRACK)
         {
            for (CRegionNode * rgn = regionListHead; rgn != NULL; rgn = rgn->fwd)
            {
               if (rgn->isSelected)
               {
                  // move spline from xform bezier editor to bezier editor
                  Vertex *xformBezierVertices = rgn->xformBezierEditor->getSplineVertices();
                  rgn->bezierEditor->loadBezier(xformBezierVertices);

                  // apply the inverse mask transform
                  Vertex *bezierVertices = rgn->bezierEditor->getSplineVertices();
                  _maskTransformEditor->InverseTransformBezier(bezierVertices);
               }
            }

            return TOOL_RETURN_CODE_EVENT_CONSUMED;
         }
      }
   }

   // if in mask ROI mode, delete all regions and all keyframes
   if (_roiModeActiveFlag)
   {
      // delete all regions in _globalKeyframe, then delete all keyframes
      ClearAll();
   }

   _stretchingRectangle = false;
   EMaskRegionShape regionShape = getRegionShape();
   if ((regionShape == MASK_REGION_RECT) || (regionShape == MASK_REGION_LASSO))
   {
      auto drawngRoi = _roiModeActiveFlag || _autoRoiModeActivationIsPending;
      auto maskColor = IsMaskInclusive() ? (drawngRoi ? SOLID_GREEN : DASHED_GREEN) : (drawngRoi ? SOLID_RED : DASHED_RED);

      // Hack to set ROI color to anything we want, for one time only
      if (_oneTimeMaskColor != INVALID_COLOR)
      {
         maskColor = _oneTimeMaskColor;
      }

      getSystemAPI()->setStretchRectangleColor(maskColor);
      getSystemAPI()->setDRSLassoMode(regionShape == MASK_REGION_LASSO);

      // tell base system to start stretching
      getSystemAPI()->startStretchRectangleOrLasso();
      _stretchingRectangle = true;
   }
   else
   {
      CRegionNode *newNode = CreateRegionNode();

      for (CRegionNode * rgna = regionListHead; rgna != NULL; rgna = rgna->fwd)
      {

         rgna->bezierEditor->unselectAllVertices();
      }

      // pass mousedown event to new editor
      newNode->bezierEditor->setShift(false);
      newNode->bezierEditor->setAlt(false);
      newNode->bezierEditor->setCtrl(false);
      newNode->bezierEditor->mouseMove(_mouseX, _mouseY);
      newNode->bezierEditor->mouseDown();
      if (newNode->bezierEditor->getMajorState() != MAJORSTATE_TRACK)
      {

         _regionJustEdited = newNode;
      }
   }

   mouseMode = MASK_TOOL_MOUSE_MODE_STRETCH;

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}

int CMaskTool::SelectUp()
{
#ifdef _DEBUG
   if (!saw_button_down)
   {
      TRACE_0(errout << "((((((((( MISSED BUTTON DOWN )))))))))))");
   }
   saw_button_down = false;
#endif

   MaskChangedAutoNotifier notifier(this);

   if (_autoRoiModeActivationIsPending)
   {
      _autoRoiModeActivationIsPending = false;
      _stretchingRectangle = false;
      getSystemAPI()->stopStretchRectangleOrLasso();
      mouseMode = MASK_TOOL_MOUSE_MODE_NONE;
      return TOOL_RETURN_CODE_NO_ACTION;
   }

   CRegionNode *regionListHead = SetKeyframeContext();

   if (_roiModeActiveFlag)
   {
      if (_maskRoiModeLockFlag)
      {
         // Locked out!
         _stretchingRectangle = false;
         getSystemAPI()->stopStretchRectangleOrLasso();
         mouseMode = MASK_TOOL_MOUSE_MODE_NONE;
         return TOOL_RETURN_CODE_NO_ACTION;
      }
      else if (_stretchingRectangle == false)
      {
         // Not stretching... working on a bezier?
         if (regionListHead != NULL && regionListHead->bezierEditor->isClosed() == true)
         {
            // Nope! Nothing happening to finish up
            return TOOL_RETURN_CODE_NO_ACTION;
         }
      }
   }

   /////////////////////////////////////////////////////////////////////////////
   //
   // this sequence redirects the BUTTON UP event to the
   // routine corresponding to the previous BUTTON DOWN.
   // This way the  meaning  of the event  or  rectangle
   // stretch is determined by the state of the mod keys
   // when the BUTTON DOWN event occurred (not BUTTON UP)

   auto retVal = 0;
   switch (_selectModKeys)
   {
   case SELECT_NONE:
      break;

   case SELECT_CTRL:
      return SelectCtrlUp();

   case SELECT_SHIFT:
      return SelectShiftUp();

   case SELECT_CTRL_SHIFT:
      return SelectCtrlShiftUp();

   case SELECT_ALT:
      return SelectAltUp();

   default:
      return -1;
   }

   /////////////////////////////////////////////////////////////////////////////

   _maskTransformEditor->setShift(true);
   _maskTransformEditor->setAlt(false);
   _maskTransformEditor->setCtrl(false);
   _maskTransformEditor->mouseUp();

   for (CRegionNode * rgn = regionListHead; rgn != NULL; rgn = rgn->fwd)
   {
      rgn->bezierEditor->setShift(true);
      rgn->bezierEditor->setAlt(false);
      rgn->bezierEditor->setCtrl(false);
      rgn->bezierEditor->mouseUp();

      rgn->xformBezierEditor->setShift(true);
      rgn->xformBezierEditor->setAlt(false);
      rgn->xformBezierEditor->setCtrl(false);
      rgn->xformBezierEditor->mouseUp();
   }

   if (_stretchingRectangle)
   {
      // Filter out auto-repeat on PC and multiple key & mouse button presses
      if (mouseMode != MASK_TOOL_MOUSE_MODE_STRETCH)
      {
         return TOOL_RETURN_CODE_EVENT_CONSUMED;
      }

      // tell the base system to stop stretching
      getSystemAPI()->stopStretchRectangleOrLasso();
      mouseMode = MASK_TOOL_MOUSE_MODE_NONE;

      // If good coords, load the rectangle or lasso into the bezierEditor
      RECT imageRect;
      POINT *lassoPtr;
      POINT lasso[MAX_LASSO_PTS];
      if (getSystemAPI()->getStretchRectangleCoordinates(imageRect, lassoPtr) == 0)
      {
         CRegionNode *newNode = CreateRegionNode();

         // Grab the lasso and make sure it's closed
         if (CopyLasso(lassoPtr, lasso))
         { // from, to
            lassoPtr = lasso;
         }
         else
         {
            lassoPtr = NULL;
         }

         _regionJustClosed = newNode;

         for (CRegionNode * rgna = regionListHead; rgna != NULL; rgna = rgna->fwd)
         {
            rgna->bezierEditor->unselectAllVertices();
         }

         newNode->bezierEditor->newSpline(); // ???
         if (lassoPtr == NULL)
         {
            POINT pt;
            pt.x = imageRect.left;
            pt.y = imageRect.top;
            newNode->bezierEditor->addPoint(&pt);
            pt.x = imageRect.right;
            pt.y = imageRect.top;
            newNode->bezierEditor->addPoint(&pt);
            pt.x = imageRect.right;
            pt.y = imageRect.bottom;
            newNode->bezierEditor->addPoint(&pt);
            pt.x = imageRect.left;
            pt.y = imageRect.bottom;
            newNode->bezierEditor->addPoint(&pt);
            pt.x = imageRect.left;
            pt.y = imageRect.top;
            newNode->bezierEditor->addPoint(&pt);
         }
         else
         {
            while (!newNode->bezierEditor->addPoint(lassoPtr++))
            {;
            }
         }

         if (_roiModeActiveFlag)
         {
            // Save the ROI shape for ~ key hack
            _maskRoiModeShape = ((lassoPtr == NULL) ? MASK_REGION_RECT : MASK_REGION_LASSO);
            _maskRoiModeRect = imageRect;
            CopyLasso(lasso, _maskRoiModeLasso); // from, to
            delete[]_maskRoiModeBezier;
            _maskRoiModeBezier = NULL;
         }
      }
      else if (_roiModeActiveFlag)
      {
         // Still need to notify even if drawing is done and there's no region
         getSystemAPI()->NotifyUserDrewROI();
      }

      _stretchingRectangle = false;
   }

   // HACK to suppress the squares at each vertex in mask ROI mode if rect or lasso
   _dontDrawVertices = (_roiModeActiveFlag && (_maskRoiModeShape != MASK_REGION_BEZIER));

   if (_regionJustClosed != NULL)
   {

      // add the new region to all keyframes
      AddRegionToAllKeyframes(_regionJustClosed);

      // makes a new keyframe if needed, otherwise replaces the
      // region list of the existing keyframe with dummyFrame's
      SetKeyframe(_currentFrame);

      // Tell the active tool that the user finished drawing the ROI...
      // if the tool doesn't handle it (returns false) we treat the
      // ROI as an ordinary mask ROI mask
      bool roiHandled = false;

      if (_roiModeActiveFlag)
      {
         // HACK: Don't draw all the stupid bezier vertex points in mask ROI mode -
         // not allowed to edit the shape after it's closed anyhow!!
         // Do this before the Notify in case the user refreshes the frame
         _dontDrawVertices = true;

         roiHandled = getSystemAPI()->NotifyUserDrewROI();
         SaveMaskToMemory(_maskRoiMemoryPDL); // to support ~ key operation

         _dontDrawVertices = roiHandled;
      }
   }
   else if ((_newVertexRegionIndex != -1) && (_regionJustEdited->bezierEditor->isClosed()))
   {

      // add vertex to all keyframes
      AddVertexToAllKeyframes(_newVertexRegionIndex, _newVertexLocus);

      // makes a new keyframe if needed, otherwise replaces the
      // region list of the existing keyframe with dummyFrame's
      SetKeyframe(_currentFrame);
   }

   else if ((_regionJustEdited != NULL) && (_regionJustEdited->bezierEditor->isClosed()))
   {

      // makes a new keyframe if needed, otherwise replaces the
      // region list of the existing keyframe with dummyFrame's
      SetKeyframe(_currentFrame);
   }

   SetMaskBeingDrawn(false);

   // refresh the screen
   RefreshFrame();

   // mbraca: protect against CRASH when we get a Button UP event
   // and we never saw the button DOWN event;
   _regionJustClosed = NULL;
   _regionJustEdited = NULL;
   _newVertexRegionIndex = -1;

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//---------------------------------------------------------------------------

int CMaskTool::SelectShiftDn()
{
   if (_roiModeActiveFlag && _maskRoiModeLockFlag)
   {
      // Locked out!
      return TOOL_RETURN_CODE_NO_ACTION;
   }

   CRegionNode *regionListHead = SetKeyframeContext();
   if (_roiModeActiveFlag && (regionListHead != NULL) && (regionListHead->bezierEditor->isClosed()))
   {
      // Can't edit a mask ROI region once it's closed
      return TOOL_RETURN_CODE_NO_ACTION;
   }

   _selectModKeys = SELECT_SHIFT;
   if (_globalKeyframe->regionSelectedCount > 0)
   {
      _maskTransformEditor->setShift(true);
      _maskTransformEditor->setAlt(false);
      _maskTransformEditor->setCtrl(false);
      _maskTransformEditor->mouseDown();
      if (_maskTransformEditor->getMajorState() == MAJORSTATE_SCALE_MASK)
      {
         for (CRegionNode * rgn = regionListHead; rgn != NULL; rgn = rgn->fwd)
         {
            if (rgn->isSelected)
            {
               // move spline from xform bezier editor to bezier editor
               Vertex *xformBezierVertices = rgn->xformBezierEditor->getSplineVertices();
               rgn->bezierEditor->loadBezier(xformBezierVertices);

               // apply the inverse mask transform
               Vertex *bezierVertices = rgn->bezierEditor->getSplineVertices();
               _maskTransformEditor->InverseTransformBezier(bezierVertices);
            }
         }

         return TOOL_RETURN_CODE_EVENT_CONSUMED;
      }
   }

   _maskTransformEditor->setMajorState(MAJORSTATE_TRACK);

   double clickInside;
   int majorState;
   double mindst = -2000000000.;
   CRegionNode *regionSelected = NULL;
   _regionJustEdited = NULL;
   for (CRegionNode * rgn = regionListHead; rgn != NULL; rgn = rgn->fwd)
   {
      if (!rgn->isSelected)
      {
         rgn->bezierEditor->setShift(true);
         rgn->bezierEditor->setAlt(false);
         rgn->bezierEditor->setCtrl(false);
         clickInside = rgn->bezierEditor->mouseDown();
         majorState = rgn->bezierEditor->getMajorState();
         rgn->bezierEditor->setMajorState(MAJORSTATE_TRACK);
      }
      else
      {
         rgn->xformBezierEditor->setShift(true);
         rgn->xformBezierEditor->setAlt(false);
         rgn->xformBezierEditor->setCtrl(false);
         clickInside = rgn->xformBezierEditor->mouseDown();
         majorState = rgn->xformBezierEditor->getMajorState();
         rgn->xformBezierEditor->setMajorState(MAJORSTATE_TRACK);
      }
      if ((clickInside <= 0.0) && (clickInside > mindst))
      {
         mindst = clickInside;
         regionSelected = rgn;
      }
      if (majorState != MAJORSTATE_TRACK)
      {
         _regionJustEdited = rgn;
         break;
      }
   }

   if (_regionJustEdited != NULL)
   {
      // unselect all selected regions
      for (CRegionNode * rgna = regionListHead; rgna != NULL; rgna = rgna->fwd)
      {
         if (rgna->isSelected)
         {
            // move spline from xformBezierEditor to bezierEditor
            Vertex *xformBezierVertices = rgna->xformBezierEditor->getSplineVertices();
            rgna->bezierEditor->loadBezier(xformBezierVertices);
            rgna->isSelected = false;
         }

         if (rgna != _regionJustEdited)
         {
            rgna->bezierEditor->unselectAllVertices();
         }
      }

      _globalKeyframe->regionSelectedCount = 0;

      // feed the event back in
      _regionJustEdited->bezierEditor->setMajorState(majorState);

      RefreshFrame();

      return TOOL_RETURN_CODE_EVENT_CONSUMED;
   }

   if (regionSelected != NULL)
   {
      if (!regionSelected->isSelected)
      {
         // move spline from bezierEditor to xformBezierEditor
         Vertex *bezierVertices = regionSelected->bezierEditor->getSplineVertices();
         regionSelected->xformBezierEditor->loadBezier(bezierVertices);

         // unselect the vertices in the unselected regions
         for (CRegionNode * rgna = regionListHead; rgna != NULL; rgna = rgna->fwd)
         {
            if (!rgna->isSelected)
            {
               rgna->bezierEditor->unselectAllVertices();
            }
         }

         regionSelected->isSelected = true;
         _globalKeyframe->regionSelectedCount++;
      }
      else
      {
         // move spline from xformBezierEditor to bezierEditor
         Vertex *xformBezierVertices = regionSelected->xformBezierEditor->getSplineVertices();
         regionSelected->bezierEditor->loadBezier(xformBezierVertices);

         // unselect the vertices in the unselected regions
         for (CRegionNode * rgna = regionListHead; rgna != NULL; rgna = rgna->fwd)
         {
            if (!rgna->isSelected)
            {
               rgna->bezierEditor->unselectAllVertices();
            }
         }

         // unselecting => update keyframe
         _regionJustEdited = regionSelected;

         regionSelected->isSelected = false;
         _globalKeyframe->regionSelectedCount--;
      }

      // extent / selected regions
      RECT selRect;
      selRect.left = 0x7fffffff;
      selRect.top = 0x7fffffff;
      selRect.right = 0x80000000;
      selRect.bottom = 0x80000000;

      for (CRegionNode * rgn = regionListHead; rgn != NULL; rgn = rgn->fwd)
      {
         if (rgn->isSelected)
         {
            // move spline from xformBezierEditor to bezierEditor
            Vertex *xformBezierVertices = rgn->xformBezierEditor->getSplineVertices();
            rgn->bezierEditor->loadBezier(xformBezierVertices);

            // include region extent
            RECT rgnRect = rgn->bezierEditor->getSimpleSplineExtent();
            if (rgnRect.left < selRect.left)
            {
               selRect.left = rgnRect.left;
            }
            if (rgnRect.top < selRect.top)
            {
               selRect.top = rgnRect.top;
            }
            if (rgnRect.right > selRect.right)
            {
               selRect.right = rgnRect.right;
            }
            if (rgnRect.bottom > selRect.bottom)
            {
               selRect.bottom = rgnRect.bottom;
            }
         }
      }

      if (_globalKeyframe->regionSelectedCount > 0)
      {
         _maskTransformEditor->resetTransform();
         _maskTransformEditor->setImageRectangle(selRect);
         _maskTransformEditor->setMajorState(MAJORSTATE_OFFSET_MASK);
      }

      RefreshFrame();

      return TOOL_RETURN_CODE_EVENT_CONSUMED;
   }

   // outside all regions
   getSystemAPI()->setStretchRectangleColor(DASHED_WHITE);
   getSystemAPI()->setDRSLassoMode(false);

   // tell base system to start stretching
   mouseMode = MASK_TOOL_MOUSE_MODE_STRETCH;
   getSystemAPI()->startStretchRectangleOrLasso();
   _stretchingRectangle = true;

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//---------------------------------------------------------------------------

int CMaskTool::SelectShiftUp()
{
   CRegionNode *regionListHead = SetKeyframeContext();

   for (CRegionNode * rgn = regionListHead; rgn != NULL; rgn = rgn->fwd)
   {
      rgn->bezierEditor->setShift(true);
      rgn->bezierEditor->setAlt(false);
      rgn->bezierEditor->setCtrl(false);
      rgn->bezierEditor->mouseUp();

      rgn->xformBezierEditor->setShift(true);
      rgn->xformBezierEditor->setAlt(false);
      rgn->xformBezierEditor->setCtrl(false);
      rgn->xformBezierEditor->mouseUp();
   }

   _maskTransformEditor->setShift(true);
   _maskTransformEditor->setAlt(false);
   _maskTransformEditor->setCtrl(false);
   _maskTransformEditor->mouseUp();

   if (_stretchingRectangle)
   {
      // if any regions are selected, then there are no
      // individual vertices selected to worry about
      if (_globalKeyframe->regionSelectedCount > 0)
      {
         for (CRegionNode * rgna = _globalKeyframe->regionListHead; rgna != NULL; rgna = rgna->fwd)
         {
            if (rgna->isSelected)
            {
               // move spline from xformBezierEditor to bezierEditor
               Vertex *xformBezierVertices = rgna->xformBezierEditor->getSplineVertices();
               rgna->bezierEditor->loadBezier(xformBezierVertices);

               rgna->isSelected = false;

               _regionJustEdited = rgna;
            }
         }

         // make sure this is cleared
         _globalKeyframe->regionSelectedCount = 0;
      }

      // Filter out auto-repeat on PC and multiple key & mouse button presses
      if (mouseMode != MASK_TOOL_MOUSE_MODE_STRETCH)
      {
         return TOOL_RETURN_CODE_EVENT_CONSUMED;
      }

      // tell the base system to stop stretching
      getSystemAPI()->stopStretchRectangleOrLasso();
      _stretchingRectangle = false;
      mouseMode = MASK_TOOL_MOUSE_MODE_NONE;

      // If good coords, load the rectangle or lasso into the bezierEditor
      RECT imageRect;
      POINT *lassoPtr;
      CRegionNode *regionSelected = NULL;
      if (getSystemAPI()->getStretchRectangleCoordinates(imageRect, lassoPtr) == 0)
      {
         for (CRegionNode * rgna = _globalKeyframe->regionListHead; rgna != NULL; rgna = rgna->fwd)
         {
            if (rgna->bezierEditor->selectVertices(imageRect) > 0)
            {
               regionSelected = rgna;
               break;
            }
         }

         if (regionSelected != NULL)
         {
            for (CRegionNode * rgna = _globalKeyframe->regionListHead; rgna != NULL; rgna = rgna->fwd)
            {
               if (rgna != regionSelected)
               {
                  rgna->bezierEditor->unselectAllVertices();
               }
            }
         }

         if ((regionSelected != NULL) && regionSelected->bezierEditor->isFullySelected())
         {
            regionSelected->bezierEditor->unselectAllVertices();

            // move spline from bezierEditor to xformBezierEditor
            Vertex *bezierVertices = regionSelected->bezierEditor->getSplineVertices();
            regionSelected->xformBezierEditor->loadBezier(bezierVertices);

            regionSelected->isSelected = true;

            _globalKeyframe->regionSelectedCount = 1;

            // transform reset
            _maskTransformEditor->resetTransform();
            RECT r = regionSelected->bezierEditor->getSimpleSplineExtent();
            _maskTransformEditor->setImageRectangle(r);
            _maskTransformEditor->setMajorState(MAJORSTATE_TRACK);
         }
      }
   }

   if ((_regionJustEdited != NULL) && (_regionJustEdited->bezierEditor->isClosed()))
   {
      // makes a new keyframe if needed, otherwise replaces the
      // region list of the existing keyframe with dummyFrame's
      SetKeyframe(_currentFrame);
   }

   // refresh the display
   RefreshFrame();

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//---------------------------------------------------------------------------

int CMaskTool::SelectAltDn()
{
   _selectModKeys = SELECT_ALT;

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//---------------------------------------------------------------------------

int CMaskTool::SelectAltUp()
{
   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//---------------------------------------------------------------------------

int CMaskTool::SelectCtrlDn()
{
   if (_roiModeActiveFlag)
   {
      _roiModeReverseRectAndLassoFlag = true;
      int retVal = SelectDn();
      _selectModKeys = SELECT_CTRL;

      return retVal;
   }

   _selectModKeys = SELECT_CTRL;
   CRegionNode *regionListHead = SetKeyframeContext();

   if (_globalKeyframe->regionSelectedCount > 0)
   {
      _maskTransformEditor->setShift(false);
      _maskTransformEditor->setAlt(false);
      _maskTransformEditor->setCtrl(true);
      _maskTransformEditor->mouseDown();
      if (_maskTransformEditor->getMajorState() == MAJORSTATE_SKEW_MASK)
      {
         for (CRegionNode * rgn = regionListHead; rgn != NULL; rgn = rgn->fwd)
         {
            if (rgn->isSelected)
            {
               // move spline from xform bezier editor to bezier editor
               Vertex *xformBezierVertices = rgn->xformBezierEditor->getSplineVertices();
               rgn->bezierEditor->loadBezier(xformBezierVertices);

               // apply the inverse mask transform
               Vertex *bezierVertices = rgn->bezierEditor->getSplineVertices();
               _maskTransformEditor->InverseTransformBezier(bezierVertices);
            }
         }

         return TOOL_RETURN_CODE_EVENT_CONSUMED;
      }
   }

   _maskTransformEditor->setMajorState(MAJORSTATE_TRACK);

   double clickInside;
   int majorState;
   double mindst = -2000000000.;
   CRegionNode *regionSelected = NULL;
   _regionJustEdited = NULL;
   for (CRegionNode * rgn = regionListHead; rgn != NULL; rgn = rgn->fwd)
   {
      if (!rgn->isSelected)
      {
         rgn->bezierEditor->setShift(false);
         rgn->bezierEditor->setAlt(false);
         rgn->bezierEditor->setCtrl(true);
         clickInside = rgn->bezierEditor->mouseDown();
         majorState = rgn->bezierEditor->getMajorState();
         rgn->bezierEditor->setMajorState(MAJORSTATE_TRACK);
      }
      else
      {
         rgn->xformBezierEditor->setShift(false);
         rgn->xformBezierEditor->setAlt(false);
         rgn->xformBezierEditor->setCtrl(true);
         clickInside = rgn->xformBezierEditor->mouseDown();
         majorState = rgn->xformBezierEditor->getMajorState();
         rgn->xformBezierEditor->setMajorState(MAJORSTATE_TRACK);
      }
      if ((clickInside <= 0.0) && (clickInside > mindst))
      {
         mindst = clickInside;
         regionSelected = rgn;
      }
      if (majorState != MAJORSTATE_TRACK)
      {
         _regionJustEdited = rgn;
         break;
      }
   }

   if ((regionSelected != NULL) || (_regionJustEdited != NULL))
   {
      // unselect all selected regions
      for (CRegionNode * rgna = regionListHead; rgna != NULL; rgna = rgna->fwd)
      {
         if (rgna->isSelected)
         {
            // move spline from xformBezierEditor to bezierEditor
            Vertex *xformBezierVertices = rgna->xformBezierEditor->getSplineVertices();
            rgna->bezierEditor->loadBezier(xformBezierVertices);

            rgna->isSelected = false;
         }

         if (rgna != _regionJustEdited)
         {
            rgna->bezierEditor->unselectAllVertices();
         }
      }

      _globalKeyframe->regionSelectedCount = 0;

      if (_regionJustEdited != NULL)
      {
         // feed the event back in
         _regionJustEdited->bezierEditor->setMajorState(majorState);
      }
      else if (regionSelected != NULL)
      {
         // move spline from bezierEditor to xformBezierEditor
         Vertex *bezierVertices = regionSelected->bezierEditor->getSplineVertices();
         regionSelected->xformBezierEditor->loadBezier(bezierVertices);

         // one region selected
         regionSelected->isSelected = true;
         _globalKeyframe->regionSelectedCount = 1;

         // transform reset
         _maskTransformEditor->resetTransform();
         RECT r = regionSelected->bezierEditor->getSimpleSplineExtent();
         _maskTransformEditor->setImageRectangle(r);
         _maskTransformEditor->setMajorState(MAJORSTATE_OFFSET_MASK);
      }

      RefreshFrame();

      return TOOL_RETURN_CODE_EVENT_CONSUMED;
   }

   // outside all regions

   // unselect all selected regions
   for (CRegionNode * rgna = regionListHead; rgna != NULL; rgna = rgna->fwd)
   {
      if (rgna->isSelected)
      {
         // move spline from xformBezierEditor to bezierEditor
         Vertex *xformBezierVertices = rgna->xformBezierEditor->getSplineVertices();
         rgna->bezierEditor->loadBezier(xformBezierVertices);

         rgna->isSelected = false;

         _regionJustEdited = rgna;
      }

      rgna->bezierEditor->unselectAllVertices();
   }

   _globalKeyframe->regionSelectedCount = 0;

   RefreshFrame();

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//---------------------------------------------------------------------------

int CMaskTool::SelectCtrlUp()
{
   if (_roiModeActiveFlag)
   {
      _roiModeReverseRectAndLassoFlag = false;
      mainWindow->UpdateMaskToolGUI(*this);
      _selectModKeys = SELECT_NONE;
      return SelectUp();
   }

   CRegionNode *regionListHead = SetKeyframeContext();

   for (CRegionNode * rgn = regionListHead; rgn != NULL; rgn = rgn->fwd)
   {
      rgn->bezierEditor->setShift(false);
      rgn->bezierEditor->setAlt(false);
      rgn->bezierEditor->setCtrl(true);
      rgn->bezierEditor->mouseUp();

      rgn->xformBezierEditor->setShift(false);
      rgn->xformBezierEditor->setAlt(false);
      rgn->xformBezierEditor->setCtrl(true);
      rgn->xformBezierEditor->mouseUp();
   }

   _maskTransformEditor->setShift(false);
   _maskTransformEditor->setAlt(false);
   _maskTransformEditor->setCtrl(true);
   _maskTransformEditor->mouseUp();

   if ((_regionJustEdited != NULL) && (_regionJustEdited->bezierEditor->isClosed()))
   {
      // makes a new keyframe if needed, otherwise replaces the
      // region list of the existing keyframe with dummyFrame's
      SetKeyframe(_currentFrame);
   }

   RefreshFrame();

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//---------------------------------------------------------------------------

int CMaskTool::SelectCtrlShiftDn()
{
   if (_roiModeActiveFlag)
   {
      _roiModeReverseRectAndLassoFlag = true;
      mainWindow->UpdateMaskToolGUI(*this);
      int retVal = SelectDn();
      _selectModKeys = SELECT_CTRL_SHIFT;

      return retVal;
   }

   _selectModKeys = SELECT_CTRL_SHIFT;

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
//---------------------------------------------------------------------------

int CMaskTool::SelectCtrlShiftUp()
{
   if (_roiModeActiveFlag)
   {
      _roiModeReverseRectAndLassoFlag = false;
      _selectModKeys = SELECT_NONE;
      return SelectUp();
   }

   return TOOL_RETURN_CODE_EVENT_CONSUMED;
}
////---------------------------------------------------------------------------
//
//void CMaskTool::StartRoiExclusiveMode()
//{
//   // Forbid entry to mask ROI mode if not presently allowed, and prevent re-entry
//   if (_roiModeActivationAllowedFlag && !_inMaskRoiExclMode)
//   {
//      // Sanity
//      _inMaskRoiExclMode = true;
//
//      // The first thing to do is flip us into mask ROI mode if we're not there
//      if (_roiModeActiveFlag)
//      {
//         _maskRoiExclOldMaskRoiMode = true;
//      }
//      else
//      {
//         _maskRoiExclOldMaskRoiMode = false;
//         bool retVal = ToggleRoiModeActivation();
//         MTIassert(retVal); // Caller shouldn't reject change!!
//      }
//
//      // Remember ROI mode's shape in case we change it
//      _maskRoiModeOldRegionShape = _maskRoiRegionShape;
//
//      // We use EXCLUSION mode to differentiate from real ROI mode
//      _globalInclusionFlag = false;
//
//      // Clear existing mask
//      ClearAll();
//
//      // Sync up toolbar, menu and _timeline
//      mainWindow->UpdateMaskToolGUI(*this);
//      mainWindow->GetTimeline()->RedrawMTL();
//   }
//}
////---------------------------------------------------------------------------
//
//void CMaskTool::EndRoiExclusiveMode()
//{
//   if (_inMaskRoiExclMode)
//   {
//      _inMaskRoiExclMode = false;
//
//      // Clear existing mask ROI-mode mask
//      ClearAll();
//
//      // Restore old state
//      _globalInclusionFlag = true;
//      _maskRoiRegionShape = _maskRoiModeOldRegionShape;
//
//      // Flip back to 'mask' mode if needed
//      if (!_maskRoiExclOldMaskRoiMode)
//      {
//         bool retVal = ToggleRoiModeActivation();
//         MTIassert(retVal); // actually can't fail
//      }
//      else
//      {
//         // Staying in mask ROI mode - just sync up the toolbar, menu and _timeline
//         mainWindow->UpdateMaskToolGUI(*this);
//         mainWindow->GetTimeline()->RedrawMTL();
//      }
//   }
//}
//---------------------------------------------------------------------------

void CMaskTool::setAutoActivateRoiModeOnDraw(bool flag)
{
   _autoActivateRoiModeOnDrawFlag = flag;
}
//---------------------------------------------------------------------------

int CMaskTool::HandleShiftRightButtonDown(bool doLasso)
{
//   // NOTE!!! Caller must have already called StartRoiExclusiveMode() !!!
//   if (_inMaskRoiExclMode)
//   {
//      // For sanity, bounce mask ROI mode out of BEZIER
//      if (_maskRoiRegionShape != MASK_REGION_BEZIER)
//      {
//         _maskRoiRegionShape = MASK_REGION_RECT;
//      }
//
//      if ((doLasso && (_maskRoiRegionShape != MASK_REGION_LASSO)) || ((!doLasso) && (_maskRoiRegionShape != MASK_REGION_RECT)))
//      {
//         _roiModeReverseRectAndLassoFlag = true;
//         mainWindow->UpdateMaskToolGUI(*this);
//      }
//
//      return SelectDn();
//   }
//
   return TOOL_RETURN_CODE_NO_ACTION;
}
//---------------------------------------------------------------------------

int CMaskTool::HandleShiftRightButtonUp()
{
//   // NOTE!!! Caller must call EndRoiExclusiveMode() after this !!!
//   if (_inMaskRoiExclMode)
//   {
//      _roiModeReverseRectAndLassoFlag = false;
//      mainWindow->UpdateMaskToolGUI(*this);
//      return SelectUp();
//   }
//
   return TOOL_RETURN_CODE_NO_ACTION;
}
//---------------------------------------------------------------------------

//////////////////////////////////////////////////////////////////////////////

void CMaskTool::RefreshFrame()
{
   mainWindow->GetDisplayer()->displayFrame();
}
//---------------------------------------------------------------------------

int CMaskTool::DrawMask(int frameIndex)
{
   if (!IsMaskToolActive() || !IsMaskVisible())
   {
      return 0;
   }

   auto currentColor = IsMaskInclusive() ? SOLID_GREEN : SOLID_RED;

   if (_oneTimeMaskColor != INVALID_COLOR)
   {
      currentColor = _oneTimeMaskColor;
   }

   // reset context when changing frame
   if (frameIndex != _currentFrame)
   {

      _timeline->ComputeInterpolatedKeyframe(frameIndex, _globalKeyframe);

      setRegionOfInterestAvailable(false);

      // subsequent draws don't reset context
      _currentFrame = frameIndex;
   }

   if (_globalKeyframe->regionSelectedCount > 0)
   {
      _maskTransformEditor->drawTransform();
   }

   if (_roiModeActiveFlag && _maskRoiModeLockFlag)
   {
      getSystemAPI()->setGraphicsColor(IsMaskInclusive() ? SOLID_CYAN : SOLID_MAGENTA);
   }
   else
   {
      getSystemAPI()->setGraphicsColor(currentColor);
   }

   for (CRegionNode * rgn = _globalKeyframe->regionListHead; rgn != NULL; rgn = rgn->fwd)
   {
      if (!rgn->isSelected)
      {
         if (!(_roiModeActiveFlag && _maskRoiModeLockFlag))
         {
           if (!rgn->hiddenFlag)
            {
#ifdef USE_PER_REGION_INCLUSION_FLAGS
               getSystemAPI()->setGraphicsColor(rgn->inclusionFlag ? SOLID_GREEN : SOLID_RED);
#else
               getSystemAPI()->setGraphicsColor(currentColor);
#endif
            }
            else
            {
               getSystemAPI()->setGraphicsColor(SOLID_BLUE);
            }
         }

         unsigned int dashpat = (_roiModeActiveFlag == false && getRegionShape() == MASK_REGION_BEZIER && rgn->bezierEditor->isClosed()
               == false) ? 0x0F0F0F0F : 0xFFFFFFFF;

         rgn->bezierEditor->drawSplineFrame(dashpat, _dontDrawVertices);
      }
      else
      {
         // selected
         if (_maskTransformEditor->getMajorState() != MAJORSTATE_TRACK)
         {

            // move spline from bezierEditor to xformBezierEditor
            Vertex *bezierVertices = rgn->bezierEditor->getSplineVertices();
            rgn->xformBezierEditor->loadBezier(bezierVertices);

            // apply transform to spline in xformBezierEditor
            Vertex *xformBezierVertices = rgn->xformBezierEditor->getSplineVertices();
            _maskTransformEditor->TransformBezier(xformBezierVertices);
         }

         if (!rgn->hiddenFlag)
         {

#ifdef USE_PER_REGION_INCLUSION_FLAGS
            getSystemAPI()->setGraphicsColor(rgn->inclusionFlag ? SOLID_GREEN : SOLID_RED);
#else
            getSystemAPI()->setGraphicsColor(currentColor);
#endif

            rgn->xformBezierEditor->drawSplineFrame(0x0f0f0f0f);
         }
         else
         {

            getSystemAPI()->setGraphicsColor(SOLID_BLUE);

            rgn->xformBezierEditor->drawSplineFrame(0x0f000f00);

#ifdef USE_PER_REGION_INCLUSION_FLAGS
            getSystemAPI()->setGraphicsColor(rgn->inclusionFlag ? SOLID_GREEN : SOLID_RED);
#else
            getSystemAPI()->setGraphicsColor(currentColor);
#endif

            rgn->xformBezierEditor->drawSplineFrame(0x000f000f);

         }
      }
   }

   return 0;
}
//---------------------------------------------------------------------------

// mbraca redefined this to return true only if the mask exists, is enabled,
// and has more than one key frame - previously it was equivalent to
// IsMaskAvailable()
bool CMaskTool::IsMaskAnimated() const
{
   return (IsMaskAvailable() && (_timeline->GetKeyframeCount() > 1));
}
//---------------------------------------------------------------------------

bool CMaskTool::IsMaskToolActive() const
{
   return _maskToolActiveFlag;
}
//---------------------------------------------------------------------------

bool CMaskTool::IsMaskToolActivationAllowed() const
{
   return _maskToolActivationAllowedFlag;
}
//---------------------------------------------------------------------------

bool CMaskTool::IsMaskInclusive() const
{
   // Per Larry, allow exclusive mask ROI mode again
   // QQQ Maybe make this settable, e.g. don't want exclusive for Autofilter
   // if (_roiModeActiveFlag)
   // return true;

   return _globalInclusionFlag;
}
//---------------------------------------------------------------------------

bool CMaskTool::IsMaskRoiModeActivationAllowed() const
{
   return _roiModeActivationAllowedFlag;
}
//---------------------------------------------------------------------------

bool CMaskTool::IsMaskRoiModeActive() const
{
   return _roiModeActiveFlag;
}
//---------------------------------------------------------------------------

// This checks a keyframe's region list to see if there are any visible
// regions in the mask
bool CMaskTool::AreAnyMaskRegionsVisible(CRegionNode * rgnList) const
{
   bool retVal = false;

   if (_maskToolActiveFlag)
   {
      for (CRegionNode * rgn = rgnList; rgn != NULL; rgn = rgn->fwd)
      {
         if (!rgn->hiddenFlag && rgn->bezierEditor->isClosed())
         {
            retVal = true;
            break;
         }
      }
   }

   return retVal;
}
//---------------------------------------------------------------------------

// This checks a keyframe's region list to see if there are any regions
// at all in the mask (hidden or visible)
bool CMaskTool::AreThereAnyMaskRegions(CRegionNode * rgnList) const
{
   bool retVal = false;

   if (_maskToolActiveFlag)
   {
      for (CRegionNode * rgn = rgnList; rgn != NULL; rgn = rgn->fwd)
      {
         if (rgn->bezierEditor->isClosed())
         {
            retVal = true;
            break;
         }
      }
   }

   return retVal;
}
//---------------------------------------------------------------------------

// mbraca redefined this to return true only if the mask is enabled,
// and contains at least one region, regardless of whther any regions
// are visible or not (previously it returned false if all the regions
// were marked 'hidden')
bool CMaskTool::IsMaskAvailable() const
{
   bool retVal = false;

   if (_maskToolActiveFlag && (!_timeline->IsEmpty()))
   {

      // Apparently we are guaranteed here that the global key frame contains
      // something meaningful if a mask is indeed available! Probably because
      // it gets set when a frame from the clip is displayed?

      retVal = AreThereAnyMaskRegions(_globalKeyframe->regionListHead);
   }

   return retVal;
}
//---------------------------------------------------------------------------

bool CMaskTool::IsMaskVisible() const
{
   return _maskIsVisibleFlag;
}
//---------------------------------------------------------------------------

bool CMaskTool::IsTransforming() const
{
   return (_globalKeyframe->regionSelectedCount > 0);
}
//---------------------------------------------------------------------------

void CMaskTool::CreateRegionList()
{
   _globalKeyframe->Clear();
}
//---------------------------------------------------------------------------

void CMaskTool::DeleteRegionList()
{
   CreateRegionList();
}
//---------------------------------------------------------------------------

CRegionNode *CMaskTool::CreateRegionNode()
{
   CRegionNode *newNode = new CRegionNode(mainWindow->GetDisplayer());

   // TEMPORARY
#ifdef USE_PER_REGION_INCLUSION_FLAGS
   newNode->inclusionFlag = _globalInclusionFlag;
#endif
   newNode->blendSlew = _defaultExclusionBlendSlew;
   newNode->borderWidth = _defaultExclusionBorderWidth;
   if (IsMaskInclusive())
   {
      newNode->blendSlew = _defaultInclusionBlendSlew;
      newNode->borderWidth = _defaultInclusionBorderWidth;
   }

   // Per Larry: in ROI (a.k.a. mask ROI) mode there is never a border!!
   if (_roiModeActiveFlag)
   {
      // Position doesn't matter!!
      newNode->borderWidth = 0;
   }

   return AppendRegionNode(newNode);
}
//---------------------------------------------------------------------------

CRegionNode *CMaskTool::AppendRegionNode(CRegionNode * nod)
{
   nod->fwd = NULL;
   if ((nod->bwd = _globalKeyframe->regionListTail) == NULL)
   {
      _globalKeyframe->regionListHead = nod;
   }
   else
   {
      _globalKeyframe->regionListTail->fwd = nod;
   }
   _globalKeyframe->regionListTail = nod;

   _globalKeyframe->regionCount++;

   return _globalKeyframe->regionListTail;
}
//---------------------------------------------------------------------------

CRegionNode *CMaskTool::DeleteRegionNode(CRegionNode * nod)
{
   _globalKeyframe->regionCount--;
   if (nod->isSelected)
   {
      _globalKeyframe->regionSelectedCount--;
   }

   CRegionNode *bwd, *fwd;
   bwd = nod->bwd;
   fwd = nod->fwd;
   if (bwd == 0)
   {
      _globalKeyframe->regionListHead = fwd;
   }
   else
   {
      bwd->fwd = fwd;
   }
   if (fwd == 0)
   {
      _globalKeyframe->regionListTail = bwd;
   }
   else
   {
      fwd->bwd = bwd;
   }

   delete nod;

   return fwd;
}
//---------------------------------------------------------------------------

void CMaskTool::SelectAll()
{
   // extent / selected regions
   RECT selRect;
   selRect.left = 0x7fffffff;
   selRect.top = 0x7fffffff;
   selRect.right = 0x80000000;
   selRect.bottom = 0x80000000;

   _globalKeyframe->regionSelectedCount = 0;
   for (CRegionNode * rgn = _globalKeyframe->regionListHead; rgn != NULL; rgn = rgn->fwd)
   {

      // move spline from bezierEditor to xformBezierEditor
      Vertex *bezierVertices = rgn->bezierEditor->getSplineVertices();
      rgn->xformBezierEditor->loadBezier(bezierVertices);

      // include region extent
      RECT rgnRect = rgn->bezierEditor->getSimpleSplineExtent();
      if (rgnRect.left < selRect.left)
      {
         selRect.left = rgnRect.left;
      }
      if (rgnRect.top < selRect.top)
      {
         selRect.top = rgnRect.top;
      }
      if (rgnRect.right > selRect.right)
      {
         selRect.right = rgnRect.right;
      }
      if (rgnRect.bottom > selRect.bottom)
      {
         selRect.bottom = rgnRect.bottom;
      }

      rgn->isSelected = true;
      _globalKeyframe->regionSelectedCount++;
   }

   _maskTransformEditor->resetTransform();
   _maskTransformEditor->setImageRectangle(selRect);

   // show all objects selected
   RefreshFrame();
}
//---------------------------------------------------------------------------

/////////////////////////////////////////////////////////////////////////////
// Added by mbraca so I can call it from AutoClean - code stolen from one of
// *many* places where all the selections are cleared
/////////////////////////////////////////////////////////////////////////////
void CMaskTool::UnselectAll()
{
   // unselect all selected regions
   for (CRegionNode * rgn = _globalKeyframe->regionListHead; rgn != NULL; rgn = rgn->fwd)
   {

      if (rgn->isSelected)
      {

         // move spline from xformBezierEditor to bezierEditor
         Vertex *xformBezierVertices = rgn->xformBezierEditor->getSplineVertices();
         rgn->bezierEditor->loadBezier(xformBezierVertices);

         rgn->isSelected = false;

         _regionJustEdited = rgn; // no frickin' clue what this is
      }

      rgn->bezierEditor->unselectAllVertices();
   }

   _globalKeyframe->regionSelectedCount = 0;

   RefreshFrame();
}
//---------------------------------------------------------------------------

// Another hack for AutoClean
bool CMaskTool::IsAnythingSelected()
{
   bool retVal = false;

   // unselect all selected regions
   for (CRegionNode * rgn = _globalKeyframe->regionListHead; retVal == false && rgn != NULL; rgn = rgn->fwd)
   {

      retVal = rgn->isSelected || rgn->bezierEditor->areAnyVerticesSelected();
   }

   return retVal;
}
//---------------------------------------------------------------------------

void CMaskTool::DeleteSelected()
{
   MaskChangedAutoNotifier notifier(this);

   if (_globalKeyframe->regionSelectedCount == 0)
   {

      // deleting selected VERTICES
      int totdel = 0;

      int rgnindx = 0;

      // mbraca 20090101 prevent using deleted memory when you delete rgn
      // below (happens when you delete each point of an unclosed bezier)
      CRegionNode *rgnfwd = NULL; // this replaces rgn->fwd in the if()

      for (CRegionNode * rgn = _globalKeyframe->regionListHead; rgn != NULL; rgn = rgnfwd)
      {

         rgnfwd = rgn->fwd;

         if (rgn->bezierEditor->isClosed())
         {

            _timeline->DeleteVerticesFromAllKeyframes(rgnindx, rgn);

            totdel += rgn->bezierEditor->eliminateAllSelectedVertices();

            if (rgn->bezierEditor->isEmpty())
            {

               _timeline->DeleteRegionFromAllKeyframes(rgnindx);

               DeleteRegionNode(rgn);

               rgnindx--;
            }
         }
         else
         { // not closed => not in keyframes

            totdel += rgn->bezierEditor->eliminateAllSelectedVertices();

            if (rgn->bezierEditor->isEmpty())
            {

               DeleteRegionNode(rgn);

               rgnindx--;
            }
         }

         rgnindx++;
      }

      // makes a new keyframe if needed, otherwise replaces the
      // region list of the existing keyframe with dummyFrame's
      if (totdel > 0)
      {

         SetKeyframe(_currentFrame);

         RefreshFrame();

         // KKKKK FIXING BUG 1/15/09
         setRegionOfInterestAvailable(false);
      }
   }
   else
   {

      // deleting selected REGIONS

      int rgnindx = 0;

      for (CRegionNode * rgn = _globalKeyframe->regionListHead; rgn != NULL; rgn = rgn->fwd)
      {

         if (rgn->isSelected)
         {

            _timeline->DeleteRegionFromAllKeyframes(rgnindx);

            DeleteRegionNode(rgn);

            rgnindx--;
         }

         rgnindx++;
      }

      if ((_globalKeyframe->regionCount == 0) ||
            ((_globalKeyframe->regionCount == 1) && (!_globalKeyframe->regionListHead->bezierEditor->isClosed())))
      {

         _timeline->DeleteEmptyKeyframes();
      }

      // makes a new keyframe if needed, otherwise replaces the
      // region list of the existing keyframe with dummyFrame's
      SetKeyframe(_currentFrame);

      RefreshFrame();

      // KKKKK FIXING BUG 1/15/09
      setRegionOfInterestAvailable(false);
   }
}
//---------------------------------------------------------------------------

// For Larry's AutoClean 'A key' hack, we changes this from returning 'void'
// to 'bool' and return true if we did anything, else false!
bool CMaskTool::HideSelected()
{
   MaskChangedAutoNotifier notifier(this);

   bool retVal = false;

   if (_globalKeyframe->regionSelectedCount == 0)
   {

      int totneut = 0;

      for (CRegionNode * rgn = _globalKeyframe->regionListHead; rgn != NULL; rgn = rgn->fwd)
      {
         totneut += rgn->bezierEditor->neutralizeAllSelectedVertices();

         if (rgn->bezierEditor->isEmpty())
         {
            DeleteRegionNode(rgn);
         }
      }

      if (totneut > 0)
      {
         // makes a new keyframe if needed, otherwise replaces the
         // region list of the existing keyframe with dummyFrame's
         SetKeyframe(_currentFrame);

         RefreshFrame();

         retVal = true;
      }
   }
   else
   {
      for (CRegionNode * rgn = _globalKeyframe->regionListHead; rgn != NULL; rgn = rgn->fwd)
      {
         if (rgn->isSelected)
         {
            rgn->hiddenFlag = !rgn->hiddenFlag;
         }
      }

      // makes a new keyframe if needed, otherwise replaces the
      // region list of the existing keyframe with dummyFrame's
      SetKeyframe(_currentFrame);

      RefreshFrame();

      retVal = true;
   }

   return retVal;
}
//---------------------------------------------------------------------------

void CMaskTool::RestoreLastRoiMask()
{
   LoadMaskFromMemory(_maskRoiMemoryPDL, true);
   mainWindow->GetTimeline()->RedrawMTL();

   //MaskChanged.Notify();
}
//---------------------------------------------------------------------------

void CMaskTool::ChangeBorder()
{
   MaskChangedAutoNotifier notifier(this);

   // Get the latest settings from the user interface
   mainWindow->GatherMaskToolSettings(*this);

   // KKKKK
   setRegionOfInterestAvailable(false);

   // update the GUI
   RefreshFrame();
}
//---------------------------------------------------------------------------

void CMaskTool::SetKeyframe(int frameIndex)
{
   MaskChangedAutoNotifier notifier(this);

   if (frameIndex < 0)
   {
      return;
   }

   if (IsMaskToolActive() && (_globalKeyframe->regionCount >= 1))
   {

      if (!IsMaskToolActive() || _globalKeyframe->regionCount < 1)
      {
         return;
      }

      setRegionOfInterestAvailable(false);

      _timeline->SetKeyframe(frameIndex, _globalKeyframe->regionListHead);
   }

   mainWindow->GetTimeline()->RedrawMTL();
}
//---------------------------------------------------------------------------

void CMaskTool::AddRegionToAllKeyframes(CRegionNode * rgn)
{
   MaskChangedAutoNotifier notifier(this);

   _timeline->AddRegionToAllKeyframes(rgn);
}
//---------------------------------------------------------------------------

void CMaskTool::AddVertexToAllKeyframes(int rgnindx, double locus)
{
   MaskChangedAutoNotifier notifier(this);

   _timeline->AddVertexToAllKeyframes(rgnindx, locus);
}
//---------------------------------------------------------------------------

void CMaskTool::DeleteKeyframe(int frameIndex)
{
   MaskChangedAutoNotifier notifier(this);

   _timeline->DeleteKeyframe(frameIndex);

   // refresh the screen
   RefreshFrame();

   mainWindow->GetTimeline()->RedrawMTL();
}
//---------------------------------------------------------------------------

void CMaskTool::DeleteAllKeyframes(bool doRedraw)
{
   MaskChangedAutoNotifier notifier(this);

   _timeline->DeleteAllKeyframes();

   if (doRedraw)
   {

      // refresh the screen
      RefreshFrame();
   }

   mainWindow->GetTimeline()->RedrawMTL();
}
//---------------------------------------------------------------------------

void CMaskTool::ClearAll()
{
   while (_globalKeyframe->regionListHead != 0)
   {
      DeleteRegionNode(_globalKeyframe->regionListHead);
   }

   DeleteAllKeyframes(true);
}
//---------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
// PDL Entry Interface
///////////////////////////////////////////////////////////////////////////////

void CMaskTool::CapturePDLEntry(CPDLEntry & pdlEntry)
{
   CPDLEntry_Mask *pdlEntryMask = pdlEntry.AddMask();

   pdlEntryMask->SetMaskEnabled(IsMaskToolActive());
   pdlEntryMask->SetGlobalInclusionMode(IsMaskInclusive() ? ALL_INCLUSIVE : ALL_EXCLUSIVE);

   if (IsMaskAvailable())
   {
      CPDLElement *pdlMaskTimelineList = pdlEntryMask->NewAnimatedMaskTimelineList();

      // NB: Don't actually have a list of timelines, so just add one
      _timeline->CapturePDLEntry(pdlMaskTimelineList, this);
   }
}
//---------------------------------------------------------------------------

void CMaskTool::CaptureRegions(CPDLElement * parentElement, CRegionNode * rgnlst)
{
   for (CRegionNode * rgn = rgnlst; rgn != NULL; rgn = rgn->fwd)
   {

      // shape ALWAYS a BEZIER
      const string *shapeTypeStr = &shapeTypeBezier;

      // blend position same for all regions in list (for now)
      // make the PDL element for the region
      CPDLElement *newRegion = parentElement->MakeNewChild(tag_MaskRegion);

      newRegion->SetAttribString(shapeTypeAttr, *shapeTypeStr);
#ifdef USE_PER_REGION_INCLUSION_FLAGS
      newRegion->SetAttribBool(inclusionFlagAttr, rgn->inclusionFlag);
#endif
      newRegion->SetAttribBool(hiddenFlagAttr, rgn->hiddenFlag);
      newRegion->SetAttribInteger(borderWidthAttr, rgn->borderWidth);
      newRegion->SetAttribInteger(blendSlewAttr, rgn->blendSlew);

      // now get the BEZIER coordinates
      BEZIER_POINT *bezierPts = rgn->bezierEditor->getCompletedSpline();

      // Create a comma-separated string of all of the bezier points by
      // looping over all of the points in the bezier curve, stopping after
      // the last point matches the first point
      MTIostringstream ostrm;
      for (int i = 0; ; ++i)
      {
         if (i > 0)
         {
            // comma between bezier points
            ostrm << ", ";
         }

         ostrm << bezierPts[i].xctrlpre << "," << bezierPts[i].yctrlpre << "," << bezierPts[i].x << "," << bezierPts[i]
               .y << "," << bezierPts[i].xctrlnxt << "," << bezierPts[i].yctrlnxt;

         if (i > 0 && bezierPts[i].xctrlpre == bezierPts[0].xctrlpre && bezierPts[i].yctrlpre == bezierPts[0].yctrlpre && bezierPts[i]
               .x == bezierPts[0].x && bezierPts[i].y == bezierPts[0].y && bezierPts[i].xctrlnxt == bezierPts[0].xctrlnxt && bezierPts[i]
               .yctrlnxt == bezierPts[0].yctrlnxt)
         {
            break;
         }

      }

      newRegion->SetAttribString(pointsAttr, ostrm.str());

      delete[]bezierPts;
   }
}

void CMaskTool::SetMask(CPDLEntry & pdlEntry, bool doNotActivateTheFuckingTool)
{
   MaskChangedAutoNotifier notifier(this);

   // Set the mask from a PDL Entry

   // Clear any existing regions in the mask
   // ClearMask();

   // mbraca 20090713
   // WTF??!? I don't know why ClearMask() is commented out above.
   // I think we need to do a really heavy-duty reset here!!!
   // In any case we need to clear the global key frame because if
   // this PDL entry has the mask enabled but doesn't have any mask
   // regions defined, we don't want the ghost of PDL entries past
   // to bite us in the ass!
   _globalKeyframe->Clear(); // let's see if this is all we need...
   //

   // May need to pop out of ROI mode.
   ActivateRoiMode(false);

   CPDLEntry_Mask *pdlEntryMask = pdlEntry.mask;

   if (pdlEntryMask == nullptr || pdlEntryMask->GetMaskEnabled() == false)
   {
      if (IsMaskToolActivationAllowed() && !doNotActivateTheFuckingTool)
      {
         ActivateNormalMode(false);
      }
   }
   else if (pdlEntryMask->animatedMaskTimelineList == 0)
   {
      if (IsMaskToolActivationAllowed() && !doNotActivateTheFuckingTool)
      {
         ActivateNormalMode(pdlEntryMask->GetMaskEnabled());
         setMaskVisible(pdlEntryMask->GetMaskEnabled());
      }
   }
   else
   {
      if (!doNotActivateTheFuckingTool)
      {
         ActivateNormalMode(true);
         setMaskVisible(true);
      }

      EGlobalInclusionMode inclMode = pdlEntryMask->GetGlobalInclusionMode();
      if (inclMode == USE_FLAGS)
      {
         _deferredInclusionStupidHackLegacyFlag = true;
      }
      else
      {
         _deferredInclusionStupidHackLegacyFlag = false;
         setGlobalInclusionFlag(inclMode == ALL_INCLUSIVE);
      }

      CPDLElement *animatedMaskTimelineList = pdlEntryMask->animatedMaskTimelineList;

      for (unsigned int i = 0; i < animatedMaskTimelineList->GetChildElementCount(); ++i)
      {
         CPDLElement *pdlTimeline = animatedMaskTimelineList->GetChildElement(i);
         if (pdlTimeline->GetElementName() != CMaskToolTimeline::tag_MaskTimeline)
         {
            continue;
         }

         CMaskToolTimeline *newTimeline = ParsePDLMaskTimeline(pdlTimeline);

         if (_timeline != 0)
         {
            delete _timeline;
         }

         _timeline = newTimeline;
      }
   }

   mainWindow->UpdateMaskToolGUI(*this);

   // force interpolation
   _currentFrame = -1;

   // refresh the screen
   RefreshFrame();
}
//---------------------------------------------------------------------------

CMaskToolTimeline* CMaskTool::ParsePDLMaskTimeline(CPDLElement * timelineElement)
{
   MaskChangedAutoNotifier notifier(this);
   CMaskToolTimeline *newTimeline = new CMaskToolTimeline;

   int lastFrame = -1;
   for (unsigned int i = 0; i < timelineElement->GetChildElementCount(); ++i)
   {

      CPDLElement *pdlKeyframe = timelineElement->GetChildElement(i);
      if (pdlKeyframe->GetElementName() != CMaskKeyframe::tag_Keyframe)
      {
         continue;
      }

      lastFrame = newTimeline->ParsePDLKeyframe(pdlKeyframe, this);
   }

   // if any keyframes were present, copy the last one parsed into "_globalKeyframe"
   if (lastFrame != -1)
   {
      delete _globalKeyframe;
      _globalKeyframe = new CMaskKeyframe(-1, newTimeline->GetKeyframe(lastFrame)->GetRegionList());
   }

   return newTimeline;
}
//---------------------------------------------------------------------------

CRegionNode* CMaskTool::ParsePDLMaskRegion(CPDLElement * pdlMaskRegion)
{
   CRegionNode *nod = new CRegionNode(mainWindow->GetDisplayer());

#ifdef USE_PER_REGION_INCLUSION_FLAGS
   nod->inclusionFlag = pdlMaskRegion->GetAttribBool(CPDLEntry_Mask::inclusionFlagAttr, _globalInclusionFlag);
#else
   if (_deferredInclusionStupidHackLegacyFlag)
   {
      string value = pdlMaskRegion->GetAttribString(CPDLEntry_Mask::inclusionFlagAttr, "wtf");
      if (value != "wtf")
      {
         _deferredInclusionStupidHackLegacyFlag = false;
         setGlobalInclusionFlag(pdlMaskRegion->GetAttribBool(CPDLEntry_Mask::inclusionFlagAttr, _globalInclusionFlag));
      }
   }
#endif

   nod->hiddenFlag = pdlMaskRegion->GetAttribBool(CPDLEntry_Mask::hiddenFlagAttr, false);

   nod->borderWidth = pdlMaskRegion->GetAttribInteger(CPDLEntry_Mask::borderWidthAttr, 0);

   nod->blendSlew = pdlMaskRegion->GetAttribDouble(CPDLEntry_Mask::blendSlewAttr, 0.0);

   string shapeType = pdlMaskRegion->GetAttribString(CPDLEntry_Mask::shapeTypeAttr, CPDLEntry_Mask::shapeTypeBezier);
   vector<int>pointList;
   pdlMaskRegion->GetAttribIntegerList(CPDLEntry_Mask::pointsAttr, pointList);

   if (shapeType == CPDLEntry_Mask::shapeTypeBezier)
   {

      const int intsPerPoint = 6;
      int pointCount = pointList.size() / intsPerPoint;
      BEZIER_POINT *points = new BEZIER_POINT[pointCount];
      for (int i = 0, j = 0; i < pointCount; ++i, j += intsPerPoint)
      {

         points[i].xctrlpre = pointList[j];
         points[i].yctrlpre = pointList[j + 1];
         points[i].x = pointList[j + 2];
         points[i].y = pointList[j + 3];
         points[i].xctrlnxt = pointList[j + 4];
         points[i].yctrlnxt = pointList[j + 5];
      }

      nod->bezierEditor->loadBezier(points);

      nod->xformBezierEditor->loadBezier(points);

      delete[]points;
   }

   return nod;
}
//---------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
// Region of Interest
///////////////////////////////////////////////////////////////////////////////
void ScaleBezier(BEZIER_POINT * bp, double scale)
{
   bp->xctrlpre = bp->xctrlpre * scale;
   bp->yctrlpre = bp->yctrlpre * scale;
   bp->x = bp->x * scale;
   bp->y = bp->y * scale;
   bp->xctrlnxt = bp->xctrlnxt * scale;
   bp->yctrlnxt = bp->yctrlnxt * scale;
}
//---------------------------------------------------------------------------

int CMaskTool::RenderRegionOfInterest(bool forceRender)
{
   // Render mask to local Region of Interest
   int retVal;

   if (isRegionOfInterestAvailable() && !forceRender)
   {
      return 0;
   } // already done

   retVal = RenderRegionOfInterest(_regionOfInterest, _globalKeyframe->regionListHead);
   setRegionOfInterestAvailable(retVal == 0);
   if (retVal != 0)
   {
      return retVal;
   }

   return 0;

}
//---------------------------------------------------------------------------

int CMaskTool::RenderRegionOfInterest(CRegionOfInterest & maskRoi, CRegionNode * rgnlst, double scale)
{
   MTIassert((scale > 0) && (scale <= 1));

   // Render mask to caller's Region of Interest
   int retVal;

   // Allocate masks for ROI if necessary.  This call is made every time,
   // so it is assumed that allocateBytemaps only allocates memory if
   // it doesn't already have sufficient memory allocated
   auto scaledPicture = _clipActivePictureRect;
   scaledPicture.left *= scale;
   scaledPicture.right *= scale;
   scaledPicture.top *= scale;
   scaledPicture.bottom *= scale;

   retVal = maskRoi.allocateBytemaps(_clipActivePictureRect);
   if (retVal != 0)
   {
      return retVal;
   }

   // Clear the region of interest if there is anything in it
   maskRoi.clearRegionList();

   // Set up the blending parameters
   maskRoi.setBlendType(BLEND_TYPE_LINEAR);
   if (AreAnyMaskRegionsVisible(rgnlst))
   {

      double blendSlew = getDefaultExclusionBlendSlew();
      int borderWidth = getDefaultExclusionBorderWidth();
      bool inclusionFlag = IsMaskInclusive();
      if (inclusionFlag)
      {
         blendSlew = getDefaultInclusionBlendSlew();
         borderWidth = getDefaultInclusionBorderWidth();
      }

      // Per Larry: in ROI (a.k.a. mask ROI) mode there is never a border
      if (_roiModeActiveFlag)
      {
         // Position doesn't matter
         borderWidth = 0;
      }

      int interiorRadius, exteriorRadius;
      interiorRadius = getDefaultInclusionBorderWidth(); // int(borderWidth*(1.0 - blendSlew)/2);
      exteriorRadius = getDefaultExclusionBorderWidth(); // int(borderWidth*(1.0 + blendSlew)/2);

      // The CRegionOfInterest always considers interior/exterior
      // relative to the total inclusion area, not the individual
      // shapes

      if (inclusionFlag)
      {
         maskRoi.setBlendRadiusInterior(interiorRadius * scale);
         maskRoi.setBlendRadiusExterior(exteriorRadius * scale);
      }
      else
      {
         maskRoi.setBlendRadiusInterior(exteriorRadius * scale);
         maskRoi.setBlendRadiusExterior(interiorRadius * scale);
      }

      // now pull the Bezier pt information out of the several regions,
      // creating from them a sequence of closed regions in one array!
      int vertices = 0;
      for (CRegionNode * rgn = rgnlst; rgn != NULL; rgn = rgn->fwd)
      {
         if (!rgn->hiddenFlag && rgn->bezierEditor->isClosed())
         {
            vertices += (rgn->bezierEditor->getVertexCount() + 1);
         }
      }
      BEZIER_POINT *bezierPts = new BEZIER_POINT[vertices];

      BEZIER_POINT *dst = bezierPts;
      for (CRegionNode * rgn = rgnlst; rgn != NULL; rgn = rgn->fwd)
      {
         if (!rgn->hiddenFlag && rgn->bezierEditor->isClosed())
         {
            BEZIER_POINT *src = rgn->bezierEditor->getCompletedSpline();
            int vcnt = rgn->bezierEditor->getVertexCount() + 1;
            for (int i = 0; i < vcnt; i++)
            {
               *dst = *src++;
               ScaleBezier(dst, scale);
               dst++;
            }
         }
      }

      // now we add a BezierListRegion (new!)
      maskRoi.addRegion(bezierPts, vertices, inclusionFlag);
      delete[]bezierPts;

      retVal = maskRoi.renderBlendAndLabelBytemaps();
      if (retVal != 0)
      {
         return retVal;
      }
   }
   else
   {

      // A mask is not available, either because the user has not defined
      // any regions or the Mask Tool is disabled.  In either case, de-
      // fine an inclusion region which is the size of the active picture
      // WAS:
      // maskRoi.setBlendRadiusInterior(0);
      // maskRoi.setBlendRadiusExterior(0);
      // maskRoi.addRegion(_clipActivePictureRect, true);
      maskRoi.renderFullFrameBlendAndLabelBytemaps();
      if (retVal != 0)
      {
         return retVal;
      }
   }

   return 0;
}
//---------------------------------------------------------------------------

void CMaskTool::ClearRegionOfInterest()
{
   MaskChangedAutoNotifier notifier(this);

   // Clear out any shapes already in the ROI
   _regionOfInterest.clearRegionList();

   setRegionOfInterestAvailable(false);
}
//---------------------------------------------------------------------------

void CMaskTool::ClearMask()
{
   // Clear Mask Tool's list of regions

   // Clear Region of Interest's entries
   ClearRegionOfInterest();
}
//---------------------------------------------------------------------------

int CMaskTool::toolRestoreSettings()
{
   CIniFile *iniFile = CreateIniFile(CPMPIniFileName());
   if (iniFile == 0)
   {
      return -1;
   }

   if (!ReadSettings(iniFile, maskToolSectionName))
   {
      return -2;
   }

   iniFile->FileName = ""; // don't write the ini file
   DeleteIniFile(iniFile);

   return 0;
}
//---------------------------------------------------------------------------

int CMaskTool::toolSaveSettings()
{
   CIniFile *iniFile = CreateIniFile(CPMPIniFileName());
   if (iniFile == 0)
   {
      return -1;
   }

   if (!WriteSettings(iniFile, maskToolSectionName))
   {
      return -2;
   }

   DeleteIniFile(iniFile); // writes the ini file too

   return 0;
}
// ---------------------------------------------------------------------------

void CMaskTool::setMaskTransformEditorIdleRefreshMode(bool onOffFlag)
{
   _maskTransformEditorIdleRefreshMode = onOffFlag;
}
// ---------------------------------------------------------------------------

bool CMaskTool::getMaskTransformEditorIdleRefreshMode()
{
   return _maskTransformEditorIdleRefreshMode;
}
// ---------------------------------------------------------------------------

bool CMaskTool::ReadSettings(CIniFile * iniFile, const string & sectionName)
{
   // Reads configuration from the file
   if (iniFile == NULL)
   {
      return false;
   }

   string valueStr, defaultStr;

   _previousMaskToolActivationState = iniFile->ReadBool(sectionName, maskToolEnabledKey, false);

   // Read exclusion border width, default to current value
   _defaultExclusionBorderWidth = iniFile->ReadInteger(sectionName, exclusionBorderWidthKey, _defaultExclusionBorderWidth);

   _defaultExclusionBlendSlew = iniFile->ReadDouble(sectionName, exclusionBlendSlewKey, _defaultExclusionBlendSlew);

   // Read inclusion border width, default to current value
   _defaultInclusionBorderWidth = iniFile->ReadInteger(sectionName, inclusionBorderWidthKey, _defaultInclusionBorderWidth);

   _defaultInclusionBlendSlew = iniFile->ReadDouble(sectionName, inclusionBlendSlewKey, _defaultInclusionBlendSlew);

   return true;
}
// ---------------------------------------------------------------------------

bool CMaskTool::WriteSettings(CIniFile * iniFile, const string & sectionName)
{
   // Writes configuration to the file
   if (iniFile == NULL)
   {
      return false;
   }

   iniFile->WriteBool(sectionName, maskToolEnabledKey, _maskToolActiveFlag);
   iniFile->WriteInteger(sectionName, exclusionBorderWidthKey, _defaultExclusionBorderWidth);
   iniFile->WriteDouble(sectionName, exclusionBlendSlewKey, _defaultExclusionBlendSlew);
   iniFile->WriteInteger(sectionName, inclusionBorderWidthKey, _defaultInclusionBorderWidth);
   iniFile->WriteDouble(sectionName, inclusionBlendSlewKey, _defaultInclusionBlendSlew);

   return true;
}
// ---------------------------------------------------------------------------

int CMaskTool::SaveMaskToFile(const string & filename)
{
   int retVal;

   // Create temporary PDL to hold the new entry
   CPDL *tmpPDL = CPDL::MakeNewPDL();

   // Create a new PDL Entry
   CPDLEntry *pdlEntry = tmpPDL->MakeNewEntry();

   // Add the Mask Regions
   CapturePDLEntry(*pdlEntry);

   ofstream pdlFileStream(filename.c_str());
   if (!pdlFileStream.is_open())
   {
      MTIostringstream msg;
      msg << "Could not open " << filename;
      _MTIErrorDialog(msg.str());
      return -1; // NB: need real error code
   }

   // write the PDL as an xml file.
   // Note: need error return
   tmpPDL->writeXML(pdlFileStream);

   pdlFileStream.close();

   // Delete the temporary PDL
   CPDL::DestroyPDL(tmpPDL);

   return 0;
}
//---------------------------------------------------------------------------

int CMaskTool::AutoSave()
{
   int retVal;

   // Generate file name
   // Clip Directory
   // "Mask_"
   // Frame Range
   // Date & Time
   //
   auto clip = getSystemAPI()->getClip();
   string autoSaveDir;
   if (clip != nullptr)
   {
      autoSaveDir = AddDirSeparator(AddDirSeparator(clip->getBinPath()) + clip->getClipName());
   }

   string filename = autoSaveDir + createDateAndTimeString() + ".msk";

   // Test if file already exists.  If it does, append a number and try again

   // Save the mask
   retVal = SaveMaskToFile(filename);
   if (retVal != 0)
   {
      return retVal;
   }

   return 0;
}
//---------------------------------------------------------------------------

string CMaskTool::createDateAndTimeString()
{
   // Get the current date and time
   time_t ltime;
   time(&ltime); // Y2K problem?, i.e. run out of seconds in 2038?
   struct tm *today = localtime(&ltime);

   // Format date/time string to YYYYMMDD_HHMMSS
   char todayStr[101];
   strftime(todayStr, 100, "%y%m%d_%H%M%S", today);

   // Return as string
   return string(todayStr);
}
//---------------------------------------------------------------------------

int CMaskTool::LoadMaskFromFile(const string & filename)
{
   MaskChangedAutoNotifier notifier(this);
   int retVal;

   ClearAll();

   if (_roiModeActiveFlag || !IsMaskToolActive())
   {
      // Switch to normal mode so we can see the mask!
      ActivateNormalMode(true);
   }

   // Parse PDL file
   CPDL *newPDL = ParsePDL(filename);

   if (newPDL == 0)
   {
      // Parsing failed
      _MTIErrorDialog(theError.getMessage());
      return theError.getError();
   }

   // Hack for ROI mode
   _dontDrawVertices = false;

   newPDL->InitPDLEntryIterator();
   CPDLEntry *pdlEntry = newPDL->GetNextPDLEntry();
   if (pdlEntry == 0)
   {
      return -1;
   }

   SetMask(*pdlEntry);

   if (!IsMaskVisible())
   {
      setMaskVisible(true);
   }

   // Refresh the frame to show the new state of the mask
   RefreshFrame();

   // Update _timeline
   mainWindow->GetTimeline()->RedrawMTL();

   return 0;
}
//---------------------------------------------------------------------------

int CMaskTool::SaveMaskToMemory(CPDL * &memoryPDL)
{
   int retVal;

   // Avoid memory leaks
   DestroySavedMemoryMask(memoryPDL);

   // Create PDL to hold the current mask
   memoryPDL = CPDL::MakeNewPDL();

   // Create a new PDL Entry
   CPDLEntry *pdlEntry = memoryPDL->MakeNewEntry();

   // Add the Mask Regions
   CapturePDLEntry(*pdlEntry);

   // All done!
   return 0;
}
//---------------------------------------------------------------------------

int CMaskTool::LoadMaskFromMemory(CPDL * &memoryPDL, bool doNotActivateTheFuckingTool)
{
   // Start from scratch
   ClearAll();

   // Handle no saved mask case
   if (memoryPDL == NULL)
   {
      return 0;
   }

   // Hack for ROI mode
   _dontDrawVertices = _roiModeActiveFlag;

   // Find what should be the one and only PDL entry
   memoryPDL->InitPDLEntryIterator();
   CPDLEntry *pdlEntry = memoryPDL->GetNextPDLEntry();
   MTIassert(pdlEntry != NULL)
   if (pdlEntry == NULL)
   {
      // Didn't find any entries - can't happen
      return -1;
   }

   // Restore the saved mask
   SetMask(*pdlEntry, doNotActivateTheFuckingTool);

   // Sync up toolbar, menu and _timeline
   mainWindow->UpdateMaskToolGUI(*this);
   mainWindow->GetTimeline()->RedrawMTL();

   return 0;
}
//---------------------------------------------------------------------------

void CMaskTool::DestroySavedMemoryMask(CPDL * &memoryPDL)
{
   if (memoryPDL != NULL)
   {
      CPDL::DestroyPDL(memoryPDL);
      memoryPDL = NULL;
   }
}
//---------------------------------------------------------------------------

string CMaskTool::GetMaskAsString()
{
   // Create temporary PDL to hold the new entry
   CPDL *tmpPDL = CPDL::MakeNewPDL();

   // Create a new PDL Entry
   CPDLEntry *pdlEntry = tmpPDL->MakeNewEntry();

   // Add the Mask Regions
   CapturePDLEntry(*pdlEntry);

   // Write the PDL as xml into a string stream.
   std::ostringstream stringStream;
   tmpPDL->writeXML(stringStream);

   // Delete the temporary PDL
   CPDL::DestroyPDL(tmpPDL);

   return stringStream.str();
}
//---------------------------------------------------------------------------

/////////////////////////////////////////////////////////////////////////////
//
// MaskRoi mode shape stuff
//
/////////////////////////////////////////////////////////////////////////////

EMaskRegionShape CMaskTool::GetMaskRoiModeShape(RECT & rect, POINT * lasso, BEZIER_POINT * bezier)
{
   rect = _maskRoiModeRect;
   if (lasso != NULL && _maskRoiModeShape == MASK_REGION_LASSO)
   {
      memcpy(lasso, _maskRoiModeLasso, MAX_LASSO_PTS*sizeof(POINT));
   }

   if (bezier != NULL && _maskRoiModeShape == MASK_REGION_BEZIER)
   {
      memcpy(bezier, _maskRoiModeBezier, MAX_BEZIER_PTS*sizeof(BEZIER_POINT));
   }

   return _maskRoiModeShape;
}
//---------------------------------------------------------------------------

void CMaskTool::SetMaskRoiModeShape(EMaskRegionShape shape, const RECT & rect, const POINT * lasso, const BEZIER_POINT * bezier)
{
   if (!_roiModeActiveFlag)
   {
      return;
   }

   // The strategy here is that we need to build an ROI mode mask and
   // then save and clear it, so if the tool asks to reuse the previous
   // ROI mask we'll get this one

   _maskRoiModeShape = shape;
   delete[]_maskRoiModeBezier;
   _maskRoiModeBezier = NULL;
   _maskRoiModeRect = rect;
   POINT *lassoPtr = NULL;

   if (shape == MASK_REGION_LASSO)
   {
      MTIassert(lasso != NULL);
      if ((lasso == NULL) || !CopyLasso(lasso, _maskRoiModeLasso))
      {
         _maskRoiModeShape = MASK_REGION_INVALID;
         return;
      }

      lassoPtr = _maskRoiModeLasso;
   }

   if (shape == MASK_REGION_BEZIER)
   {
      MTIassert(bezier != NULL);
      if (bezier == NULL)
      {
         _maskRoiModeShape = MASK_REGION_INVALID;
      }
      else
      {
         memcpy(_maskRoiModeBezier, bezier, MAX_BEZIER_PTS*sizeof(BEZIER_POINT));
      }
   }

   ClearAll();

   // Most of this was stolen from SelectUp()
   CRegionNode *newNode = CreateRegionNode();
   _regionJustClosed = newNode;
   newNode->bezierEditor->newSpline();

   if (_maskRoiModeShape == MASK_REGION_BEZIER)
   {
      // punt for now... I don't feel like figuring it out
      // anyhow I don't think anyone will try to pass in a bezier !!!!
      _maskRoiModeShape = MASK_REGION_INVALID;
      return;
   }
   else if (_maskRoiModeShape == MASK_REGION_LASSO)
   {
      while (!newNode->bezierEditor->addPoint(lassoPtr++))
      {;
      }
   }
   else // _maskRoiModeShape == MASK_REGION_RECT
   {
      POINT pt;
      pt.x = _maskRoiModeRect.left;
      pt.y = _maskRoiModeRect.top;
      newNode->bezierEditor->addPoint(&pt);
      pt.x = _maskRoiModeRect.right;
      pt.y = _maskRoiModeRect.top;
      newNode->bezierEditor->addPoint(&pt);
      pt.x = _maskRoiModeRect.right;
      pt.y = _maskRoiModeRect.bottom;
      newNode->bezierEditor->addPoint(&pt);
      pt.x = _maskRoiModeRect.left;
      pt.y = _maskRoiModeRect.bottom;
      newNode->bezierEditor->addPoint(&pt);
      pt.x = _maskRoiModeRect.left;
      pt.y = _maskRoiModeRect.top;
      newNode->bezierEditor->addPoint(&pt);
   }

   // add the new region to all keyframes
   AddRegionToAllKeyframes(_regionJustClosed);

   // makes a new keyframe if needed, otherwise replaces the
   // region list of the existing keyframe with dummyFrame's
   SetKeyframe(_currentFrame);
   SaveMaskToMemory(_maskRoiMemoryPDL); // to support ~ key operation

   // OK we don't want to see the mask yet, so clear it!
   ClearAll();

   // Clean up
   _regionJustClosed = NULL;
   _regionJustEdited = NULL;
   _newVertexRegionIndex = -1;
}
//---------------------------------------------------------------------------

void CMaskTool::LockMaskRoiModeMask(bool lockFlag)
{
   // Only allowed to lock mask if it's showing
 //  MTIassert(IsMaskAvailable());
   if (lockFlag)
   {
//      if (IsMaskAvailable())
      {
         _maskRoiModeLockFlag = true;
         RefreshFrame(); // Mask changes to cyan or magenta when locked
      }
   }
   else
   {
      _maskRoiModeLockFlag = false;
      RefreshFrame(); // restore green/red
   }
}
//---------------------------------------------------------------------------

void CMaskTool::setRoiIsRectangularOnly(bool flag)
{
   _roiIsRectangularOnly = flag;
   mainWindow->UpdateMaskToolGUI(*this);
}
//---------------------------------------------------------------------------

bool CMaskTool::getRoiIsRectangularOnly()
{
   return _roiIsRectangularOnly;
}
//---------------------------------------------------------------------------

/////////////////////////////////////////////////////////////////////////////
//
// CMaskTimeline
//
/////////////////////////////////////////////////////////////////////////////

CMaskToolTimeline::CMaskToolTimeline() : regionShape(MASK_REGION_INVALID)
{
}
//---------------------------------------------------------------------------

CMaskToolTimeline::~CMaskToolTimeline()
{
   DeleteAllKeyframes();
}
// ----------------------------------------------------------------------------

CMaskKeyframe* CMaskToolTimeline::GetKeyframe(int frameIndex)
{
   CMaskKeyframe *keyframe = 0;

   // Search for the keyframe with the matching frame index
   for (list<CMaskKeyframe*>::iterator iter = keyframeList.begin(); iter != keyframeList.end(); ++iter)
   {
      if ((*iter)->GetFrameIndex() == frameIndex)
      {
         keyframe = *iter;
         break;
      }
   }

   return keyframe;
}
//---------------------------------------------------------------------------

CMaskKeyframe* CMaskToolTimeline::GetFirstKeyframe()
{
   CMaskKeyframe *keyframe = 0;

   // Get first keyframe in the keyframe list
   if (!keyframeList.empty())
   {
      keyframe = keyframeList.front();
   }

   return keyframe;
}
//---------------------------------------------------------------------------

CMaskKeyframe* CMaskToolTimeline::GetNextKeyframe(int frameIndex)
{
   CMaskKeyframe *keyframe = 0;

   // Iterate through the list of keyframes to find the first keyframe
   // with a frame index that is greater than the caller's frame index
   for (list<CMaskKeyframe*>::iterator iter = keyframeList.begin(); iter != keyframeList.end(); ++iter)
   {
      if ((*iter)->GetFrameIndex() > frameIndex)
      {
         keyframe = *iter;
         break;
      }
   }

   return keyframe;
}
//---------------------------------------------------------------------------

CMaskKeyframe* CMaskToolTimeline::GetPreviousKeyframe(int frameIndex)
{
   CMaskKeyframe *keyframe = 0;

   // Iterate backwards through the list of keyframes to find the first
   // keyframe with a frame index that is less than the caller's frame index.
   for (list<CMaskKeyframe*>::reverse_iterator iter = keyframeList.rbegin(); iter != keyframeList.rend(); ++iter)
   {
      if ((*iter)->GetFrameIndex() < frameIndex)
      {
         keyframe = *iter;
         break;
      }
   }

   return keyframe;
}
//---------------------------------------------------------------------------

void CMaskToolTimeline::ComputeInterpolatedKeyframe(int frameIndex, CMaskKeyframe * theFrame)
{
   CMaskKeyframe *begKeyframe = 0;
   CMaskKeyframe *endKeyframe = 0;

   if (!keyframeList.empty())
   {
      if (frameIndex <= keyframeList.front()->GetFrameIndex())
      {
         endKeyframe = keyframeList.front();
      } // Before first keyframe
      else if (frameIndex >= keyframeList.back()->GetFrameIndex())
      {
         begKeyframe = keyframeList.back();
      } // After last keyframe
      else
      {
         // Search the keyframes, looking for a pair that enclose the caller's
         // frame index
         for (list<CMaskKeyframe*>::iterator iter = keyframeList.begin();
         iter != keyframeList.end() && (begKeyframe == 0 || endKeyframe == 0); ++iter)
         {
            CMaskKeyframe *keyframe = *iter;
            if (frameIndex >= keyframe->GetFrameIndex())
            {
               begKeyframe = keyframe;
            }

            if (frameIndex <= keyframe->GetFrameIndex())
            {
               endKeyframe = keyframe;
            }
         }
      }
   }

   double t;

   if (begKeyframe == 0 && endKeyframe == 0)
   {
      return;
   }
   else if (begKeyframe == 0 && endKeyframe != 0)
   {

      begKeyframe = endKeyframe;
      t = 0.0;
   }
   else if (begKeyframe != 0 && endKeyframe == 0)
   {

      endKeyframe = begKeyframe;
      t = 1.0;
   }
   else if (begKeyframe != 0 && endKeyframe != 0)
   {

      int begIndex = begKeyframe->GetFrameIndex();
      int endIndex = endKeyframe->GetFrameIndex();

      if (begIndex == endIndex)
      {

         endKeyframe = begKeyframe;
         t = 1.0;
      }
      else
      {

         t = (double)(endIndex - frameIndex) / ((double)(endIndex - begIndex));
      }
   }

   theFrame->InterpolateKeyframes(begKeyframe, endKeyframe, t);
}
//---------------------------------------------------------------------------

bool CMaskToolTimeline::IsEmpty()
{
   return keyframeList.empty();
}
//---------------------------------------------------------------------------

int CMaskToolTimeline::GetKeyframeCount()
{
   return (int) keyframeList.size();
}
//---------------------------------------------------------------------------

// ---------------------------------------------------------------------------
static bool cmpMaskKeyframe(CMaskKeyframe * e1, CMaskKeyframe * e2)
{
   return (e1->GetFrameIndex() < e2->GetFrameIndex());
}
//---------------------------------------------------------------------------

void CMaskToolTimeline::SetKeyframe(int frameIndex, CRegionNode * rgnlst)
{
   CMaskKeyframe *keyframe = GetKeyframe(frameIndex);
   if (keyframe == 0)
   {
      // Create new keyframe with new region list
      keyframe = new CMaskKeyframe(frameIndex, rgnlst);
      keyframeList.push_back(keyframe);
      keyframeList.sort(cmpMaskKeyframe);
      // sort keyframe list by frame indices
   }
   else
   {
      // Copy new region list and its parameters to existing keyframe
      keyframe->SetRegion(rgnlst);
   }
}
//---------------------------------------------------------------------------

void CMaskToolTimeline::AddRegionToAllKeyframes(CRegionNode * rgn)
{
   for (list<CMaskKeyframe*>::iterator iter = keyframeList.begin(); iter != keyframeList.end(); ++iter)
   {
      (*iter)->AddRegion(rgn);
   }
}
//---------------------------------------------------------------------------

void CMaskToolTimeline::DeleteRegionFromAllKeyframes(int rgnindx)
{
   for (list<CMaskKeyframe*>::iterator iter = keyframeList.begin(); iter != keyframeList.end(); ++iter)
   {
      (*iter)->DeleteRegion(rgnindx);
   }
}
//---------------------------------------------------------------------------

void CMaskToolTimeline::AddVertexToAllKeyframes(int rgnindx, double locus)
{
   for (list<CMaskKeyframe*>::iterator iter = keyframeList.begin(); iter != keyframeList.end(); ++iter)
   {
      (*iter)->AddVertex(rgnindx, locus);
   }
}
//---------------------------------------------------------------------------

void CMaskToolTimeline::DeleteVerticesFromAllKeyframes(int rgnindx, CRegionNode * tmplt)
{
   for (list<CMaskKeyframe*>::iterator iter = keyframeList.begin(); iter != keyframeList.end(); ++iter)
   {
      (*iter)->DeleteVertices(rgnindx, tmplt);
   }
}
// --------------------------------------------------------------------------

void CMaskToolTimeline::DeleteKeyframe(int frameIndex)
{
   CMaskKeyframe *keyframe = 0;

   // Search for the keyframe with the matching frame index
   for (list<CMaskKeyframe*>::iterator iter = keyframeList.begin(); iter != keyframeList.end(); ++iter)
   {
      if ((*iter)->GetFrameIndex() == frameIndex)
      {
         keyframe = *iter;
         keyframeList.erase(iter);
         delete keyframe;
         break;
      }
   }
}
//---------------------------------------------------------------------------

void CMaskToolTimeline::DeleteEmptyKeyframes()
{
   CMaskKeyframe *keyframe = 0;

   // Make sure all keyframes are empty of regions
   bool abort = false;
   for (list<CMaskKeyframe*>::iterator iter = keyframeList.begin(); iter != keyframeList.end(); ++iter)
   {
      if ((*iter)->GetRegionList() != NULL)
      {
         abort = true;
         break;
      }
   }

   if (!abort)
   {

      DeleteAllKeyframes();
   }
}
//---------------------------------------------------------------------------

void CMaskToolTimeline::DeleteAllKeyframes()
{
   // Delete all of the keyframes in the _timeline
   for (list<CMaskKeyframe*>::iterator iter = keyframeList.begin(); iter != keyframeList.end(); ++iter)
   {
      delete *iter;
   }

   keyframeList.clear();
}
// ---------------------------------------------------------------------------

void CMaskToolTimeline::CapturePDLEntry(CPDLElement * listElement, CMaskTool * maskTool)
{
   CPDLElement *element = listElement->MakeNewChild(tag_MaskTimeline);

   for (list<CMaskKeyframe*>::iterator iter = keyframeList.begin(); iter != keyframeList.end(); ++iter)
   {
      CMaskKeyframe *keyframe = *iter;
      keyframe->CapturePDLEntry(element, maskTool);
   }
}
//---------------------------------------------------------------------------

int CMaskToolTimeline::ParsePDLKeyframe(CPDLElement * pdlKeyframe, CMaskTool * maskTool)
{
   int frameIndex = pdlKeyframe->GetAttribInteger(CMaskKeyframe::frameIndexAttr, -1);
   CRegionNode *rgnlst = NULL;
   CRegionNode *tail = NULL;
   for (unsigned i = 0; i < pdlKeyframe->GetChildElementCount(); i++)
   {
      CPDLElement *pdlMaskRegion = pdlKeyframe->GetChildElement(i);
      if (pdlMaskRegion == 0 || pdlMaskRegion->GetElementName() != CMaskTool::tag_MaskRegion)
      {
         break;
      }

      CRegionNode *nod = maskTool->ParsePDLMaskRegion(pdlMaskRegion);
      if (tail == NULL)
      {
         rgnlst = nod;
      }
      else
      {
         tail->fwd = nod;
      }

      tail = nod;
      tail->fwd = NULL;
   }

   SetKeyframe(frameIndex, rgnlst);

   for (CRegionNode * rgn = rgnlst; rgn != NULL;)
   {
      CRegionNode *nxt = rgn->fwd;
      delete rgn;
      rgn = nxt;
   }

   return frameIndex;
}
//---------------------------------------------------------------------------

/////////////////////////////////////////////////////////////////////////////
//
// CMaskKeyframe
//
/////////////////////////////////////////////////////////////////////////////
CMaskKeyframe::CMaskKeyframe()
{
   frameIndex = -1;

   regionListHead = NULL;
   regionListTail = NULL;
   regionCount = 0;
   regionSelectedCount = 0;
}
//---------------------------------------------------------------------------

CMaskKeyframe::CMaskKeyframe(int newFrameIndex, CRegionNode * rgnlst) : frameIndex(newFrameIndex)
{
   frameIndex = newFrameIndex;

   regionListHead = NULL;
   regionListTail = NULL;
   regionCount = 0;
   regionSelectedCount = 0;

   SetRegion(rgnlst);
}
//---------------------------------------------------------------------------

CMaskKeyframe::~CMaskKeyframe()
{
   Clear();
}
//---------------------------------------------------------------------------

void CMaskKeyframe::Clear()
{
   for (CRegionNode * nod = regionListTail; nod != NULL;)
   {
      CRegionNode *onebwd = nod->bwd;
      delete nod;
      nod = onebwd;
   }

   frameIndex = -1;

   regionListHead = NULL;
   regionListTail = NULL;
   regionCount = 0;
   regionSelectedCount = 0;
}
// ---------------------------------------------------------------------------

void CMaskKeyframe::deleteMaskRegion()
{
   for (CRegionNode * src = regionListHead; src != NULL;)
   {

      CRegionNode *nxt = src->fwd;

      delete src;

      src = nxt;
   }

   regionListHead = NULL;
   regionListTail = NULL;
   regionCount = 0;
   regionSelectedCount = 0;
}
// ---------------------------------------------------------------------------

int CMaskKeyframe::GetFrameIndex() const
{
   return frameIndex;
}
//---------------------------------------------------------------------------

bool CMaskKeyframe::GetInclusionFlag() const
{
   return true; // haha wtf??
}
//---------------------------------------------------------------------------

CRegionNode* CMaskKeyframe::GetRegionList()
{
   return regionListHead;
}
//---------------------------------------------------------------------------

void CMaskKeyframe::AddRegion(CRegionNode * rgn)
{
   // make a nice copy
   CRegionNode *rgncpy = new CRegionNode(*rgn);

   rgncpy->fwd = NULL;
   if ((rgncpy->bwd = regionListTail) == NULL)
   {
      regionListHead = rgncpy;
   }
   else
   {
      regionListTail->fwd = rgncpy;
   }
   regionListTail = rgncpy;

   regionCount++;
}
//---------------------------------------------------------------------------

void CMaskKeyframe::DeleteRegion(int rgnindx)
{
   int indx = 0;
   CRegionNode *rgn = regionListHead;
   for (; rgn != NULL; rgn = rgn->fwd)
   {

      if (indx == rgnindx)
      {
         break;
      }
      indx++;
   }

   if (rgn != NULL)
   {
      // only closed regions are in keyframes
      if (rgn->bwd == NULL)
      {
         regionListHead = rgn->fwd;
      }
      else
      {
         rgn->bwd->fwd = rgn->fwd;
      }

      if (rgn->fwd == NULL)
      {
         regionListTail = rgn->bwd;
      }
      else
      {
         rgn->fwd->bwd = rgn->bwd;
      }
   }
}
//---------------------------------------------------------------------------

void CMaskKeyframe::AddVertex(int rgnindx, double locus)
{
   int indx = 0;
   CRegionNode *rgn = regionListHead;
   for (; rgn != NULL; rgn = rgn->fwd)
   {
      if (indx == rgnindx)
      {
         break;
      }
      indx++;
   }

   if (rgn != NULL)
   {
      // only closed regions are in keyframes
      rgn->bezierEditor->createAdditionalIntermediateVertex(locus);
   }
}
//---------------------------------------------------------------------------

void CMaskKeyframe::DeleteVertices(int rgnindx, CRegionNode * tmplt)
{
   int indx = 0;
   CRegionNode *rgn = regionListHead;
   for (; rgn != NULL; rgn = rgn->fwd)
   {
      if (indx == rgnindx)
      {
         break;
      }
      indx++;
   }

   if (rgn != NULL)
   {
      // only closed regions are in keyframes
      rgn->bezierEditor->eliminateAllSelectedVertices(tmplt->bezierEditor);
   }
}
//---------------------------------------------------------------------------

void CMaskKeyframe::SetRegion(CRegionNode * rgnlst)
{
   deleteMaskRegion();

   for (CRegionNode * src = rgnlst; src != NULL; src = src->fwd)
   {
      // only closed regions included
      if (src->bezierEditor->isClosed())
      {
         AddRegion(src);
      }
   }
}
//---------------------------------------------------------------------------

void CMaskKeyframe::InterpolateKeyframes(CMaskKeyframe * before, CMaskKeyframe * after, double combin)
{
   CRegionNode *cur = regionListHead;
   CRegionNode *bef = before->regionListHead;
   CRegionNode *aft = after->regionListHead;

   while ((cur != NULL) && (bef != NULL) && (aft != NULL))
   {

      // the interpolated region is hidden iff the
      // greatest lower keyframe has it hidden
      cur->hiddenFlag = bef->hiddenFlag; // && aft->hiddenFlag ?;

      if (!cur->isSelected)
      { // only unselected regions are interpolated
         cur->bezierEditor->interpolateVertices(bef->bezierEditor, aft->bezierEditor, combin);
      }

      bef = bef->fwd;
      aft = aft->fwd;
      cur = cur->fwd;
   }
}
//---------------------------------------------------------------------------

void CMaskKeyframe::CapturePDLEntry(CPDLElement * listElement, CMaskTool * maskTool)
{
   CPDLElement *element = listElement->MakeNewChild(tag_Keyframe);

   element->SetAttribInteger(frameIndexAttr, frameIndex);

   maskTool->CaptureRegions(element, regionListHead);
}

///////////////////////////////////////////////////////////////////////////////

bool CMaskToolAnimateTimelineIF::GetFirstKeyframe(int &keyframeIndex, bool &inclusion)
{
   CMaskKeyframe *keyframe;

   keyframe = maskTool->_timeline->GetFirstKeyframe();
   if (keyframe == 0)
   {
      return false;
   }

   keyframeIndex = keyframe->GetFrameIndex();
   inclusion = keyframe->GetInclusionFlag(); // always returns true!!

   return true;
}
//---------------------------------------------------------------------------

bool CMaskToolAnimateTimelineIF::GetNextKeyframe(int &keyframeIndex, bool &inclusion)
{
   CMaskKeyframe *keyframe;

   keyframe = maskTool->_timeline->GetNextKeyframe(keyframeIndex);
   if (keyframe == 0)
   {
      return false;
   }

   keyframeIndex = keyframe->GetFrameIndex();
   inclusion = keyframe->GetInclusionFlag(); // always returns true!!

   return true;
}
//---------------------------------------------------------------------------

bool CMaskToolAnimateTimelineIF::GetPreviousKeyframe(int &keyframeIndex, bool &inclusion)
{
   CMaskKeyframe *keyframe;

   keyframe = maskTool->_timeline->GetPreviousKeyframe(keyframeIndex);
   if (keyframe == 0)
   {
      return false;
   }

   keyframeIndex = keyframe->GetFrameIndex();
   inclusion = keyframe->GetInclusionFlag(); // always returns true!!

   return true;
}
//---------------------------------------------------------------------------

bool CMaskToolAnimateTimelineIF::IsKeyframeHere(int frameIndex)
{
   CMaskKeyframe *keyframe;

   keyframe = maskTool->_timeline->GetKeyframe(frameIndex);

   return (keyframe != 0);
}
//---------------------------------------------------------------------------

void CMaskToolAnimateTimelineIF::DeleteKeyframe(int frameIndex)
{
   maskTool->DeleteKeyframe(frameIndex);
}
//---------------------------------------------------------------------------

void CMaskToolAnimateTimelineIF::DeleteAllKeyframes()
{
   maskTool->DeleteAllKeyframes(true);
}
//---------------------------------------------------------------------------

Ipp8uArray blur(Ipp8uArray & mask)
{
   IppStatus status = ippStsNoErr;
   IppiSize maskSize =
   {3, 3};
   IppiSize srcSize = mask.getSize();
   Ipp8u* pBuffer = NULL;
   Ipp8u* pSrc = mask.data();
   Ipp8u* pDst = (Ipp8u*)malloc(srcSize.width * srcSize.height);
   int srcStep = mask.getRowPitchInBytes();
   int dstStep = srcStep;
   int bufSize = 0;

   /* Get work buffer size */ status = ippiFilterBoxBorderGetBufferSize(srcSize, maskSize, ipp8u, 1, &bufSize);
   pBuffer = (Ipp8u*)malloc(bufSize);

   /* Filter the image */
   if (status >= ippStsNoErr)
   {
      status = ippiFilterBoxBorder_8u_C1R(pSrc, srcStep, pDst, dstStep, srcSize, maskSize, ippBorderRepl, NULL, pBuffer);
   }

   if (pBuffer)
   {
      free(pBuffer);
   }

   Ipp8uArray result(mask.getSize(), pDst);
   return result;
}
//---------------------------------------------------------------------------

Ipp8uArray CMaskTool::generateOverlayMask(Ipp8uArray mask, const IppiSize & targetSize, bool displayMaskInterior)
{
   auto inclusiveMask = IsMaskInclusive();
   Ipp8u rcolor = inclusiveMask ? 0x00 : 0xFF;
   Ipp8u bcolor = inclusiveMask ? 0x00 : 0x00;
   Ipp8u gcolor = inclusiveMask ? 0xFF : 0x00;
   int color = IsMaskInclusive() ? 0x0000FF00 : 0x000000FF;

   auto frameHeight = mask.getRows();
   auto frameWidth = mask.getCols();

   auto smallMask = mask.resize(targetSize);
   smallMask = blur(smallMask);

   Ipp8uArray smallRgbaMask({targetSize, 4});

   vector<bool>previousMaskRow(smallMask.getCols());
   auto row0 = smallMask.getRowPointer(0);
   for (auto c = 0; c < smallMask.getCols(); c++)
   {
      previousMaskRow[c] = *row0++ == 0 || *row0++ == 0xFF;
   }

   for (auto r = 0; r < smallMask.getRows(); r++)
   {
      auto pMaskRoiMask = smallMask.getRowPointer(r);

      // we start here
      bool previousPixelMask = *pMaskRoiMask == 0 || *pMaskRoiMask == 0xFF;
      auto pRgbMaskRow = smallRgbaMask.getRowPointer(r);

      auto cols = smallMask.getCols();
      for (auto c = 0; c < cols; c++)
      {
         auto currentAlpha = *pMaskRoiMask;

         auto currentPixelMask = currentAlpha == 0 || currentAlpha == 0xFF;

         Ipp8u rcolor = inclusiveMask ? 0x00 : 0xFF;
         Ipp8u bcolor = inclusiveMask ? 0x00 : 0x00;
         Ipp8u gcolor = inclusiveMask ? 0xFF : 0x00;

         // auto interior = currentAlpha == 0xFF;
         // if (c < cols-1)
         // {
         // if (pMaskRoiMask[1] >= 0x80)
         // {
         // interior = true;
         // }
         // }

         if (currentAlpha == 0xFF)
         {
            currentAlpha = displayMaskInterior ? 0x8F : 0;
            rcolor >>= 1;
            gcolor >>= 1;
         }

         currentAlpha = displayMaskInterior ? currentAlpha >>= 1 : 0;
         if (currentPixelMask != previousMaskRow[c])
         {
            previousMaskRow[c] = currentPixelMask;

            // Transistion has happened, just ignore a second one
            previousPixelMask = currentPixelMask;
            if (r > 0 && inclusiveMask)
            {
               auto lp = smallRgbaMask.getElementPointer({c, r - 1});
               *lp++ = inclusiveMask ? 0x00 : 0xFF;
               *lp++ = inclusiveMask ? 0xFF : 0x00;
               *lp++ = bcolor;
               *lp++ = 0xFF;
            }
            else
            {
               currentAlpha = 0xFF;
            }
         }

         // This is NOT correct but makes the image look correct
         // The line for mask is one pixes to the left of where it starts
         if (currentPixelMask != previousPixelMask)
         {
            previousPixelMask = currentPixelMask;
            if (c > 0 && inclusiveMask)
            {
               auto lp = smallRgbaMask.getElementPointer({c - 1, r});
               *lp++ = rcolor;
               *lp++ = inclusiveMask ? 0xFF : 0x00;
               *lp++ = bcolor;
               *lp++ = 0xFF;
            }
            else
            {
               currentAlpha = 0xFF;
            }
         }

         *pRgbMaskRow++ = rcolor;
         *pRgbMaskRow++ = gcolor;
         *pRgbMaskRow++ = bcolor;
         *pRgbMaskRow++ = currentAlpha;

         pMaskRoiMask++;
      }
   }

   // This +1 is because I can't figure out the exact top row so when
   // magnified a lot, the row is off one.  This avoids access violation
   // Since its not visible, we don't care
   return smallRgbaMask.resize({frameWidth, frameHeight + 1});
}
//---------------------------------------------------------------------------

// This is a hack.  Masks are cleared and set to the rect
void CMaskTool::setRectAsMask(const RECT & rect)
{
   if (!IsMaskToolActivationAllowed())
   {
      return;
   }

   if (_roiModeActiveFlag || !IsMaskToolActive())
   {
      // Switch to normal mode so we can see the mask!
      ActivateNormalMode(true);
   }

   ClearAll();

   CRegionNode *newNode = CreateRegionNode();
   newNode->bezierEditor->newSpline();

   POINT pt;
   pt.x = rect.left;
   pt.y = rect.top;
   newNode->bezierEditor->addPoint(&pt);
   pt.x = rect.right;
   pt.y = rect.top;
   newNode->bezierEditor->addPoint(&pt);
   pt.x = rect.right;
   pt.y = rect.bottom;
   newNode->bezierEditor->addPoint(&pt);
   pt.x = rect.left;
   pt.y = rect.bottom;
   newNode->bezierEditor->addPoint(&pt);
   pt.x = rect.left;
   pt.y = rect.top;

   newNode->bezierEditor->addPoint(&pt);
   _dontDrawVertices = false;

   AddRegionToAllKeyframes(newNode);
   SetKeyframe(_currentFrame);

   // Refresh the frame to show the new state of the mask
   if (!IsMaskVisible())
   {
      setMaskVisible(true);
   }

   RefreshFrame();
   mainWindow->UpdateMaskToolGUI(*this);
}
//---------------------------------------------------------------------------

bool CMaskTool::isAnyMaskSelected()
{
   CRegionNode *regionListHead = SetKeyframeContext();

   for (CRegionNode * rgn = regionListHead; rgn != NULL; rgn = rgn->fwd)
   {
      if (rgn->isSelected)
      {
         return true;
      }
   }

   return false;
}
//---------------------------------------------------------------------------

Ipp8uArray CMaskTool::ComputeSmallMask(const IppiSize & sourceSize, int newWidth)
{
   _currentFrame = getSystemAPI()->getLastFrameIndex();
   CRegionOfInterest maskRoi;

   // Changes some internal values, save them
   auto blendMax = std::max<int>(getDefaultExclusionBorderWidth(), getDefaultInclusionBorderWidth());

   // These reduce the ringing on lines
   auto scale = 0.25;
   if (blendMax < 20)
   {
      scale = 1;
   }
   else if (scale < 30)
   {
      scale = 0.50;
   }
   else if (scale < 40)
   {
      scale = 0.25;
   }

   auto roiAvailable = isRegionOfInterestAvailable();
   auto retVal = GetRegionOfInterest(_currentFrame, maskRoi, scale);
   Ipp8u* mask = (Ipp8u*)maskRoi.getBlendPtr();

   auto scaledPicture = _clipActivePictureRect;
   scaledPicture.left *= scale;
   scaledPicture.right *= scale;
   scaledPicture.top *= scale;
   scaledPicture.bottom *= scale;

   auto h = scaledPicture.bottom - scaledPicture.top;
   auto w = scaledPicture.right - scaledPicture.left;

   auto m = (Ipp8u*)malloc(sourceSize.height * sourceSize.width);
   memcpy(m, mask, sourceSize.height * sourceSize.width);
   Ipp8uArray smallMask(sourceSize, m);
   auto result = smallMask({0, 0, w, h}).resize(sourceSize);

   // Hack to reduce the effect of the resize;
   for (auto r = 0; r < smallMask.getRows(); r++)
   {
      auto p = smallMask.getRowPointer(r);
      for (auto c = 0; c < smallMask.getCols(); c++)
      {
         if (*p > 50)
         {
            *p = 255;
         }

         p++;
      }
   }

   return result;
}
//---------------------------------------------------------------------------

vector<VertexList>CMaskTool::CopyVertexLists()
{
   vector<VertexList>result;
   CRegionNode *rgnlst = _globalKeyframe->regionListHead;

   for (CRegionNode * rgn = rgnlst; rgn != NULL; rgn = rgn->fwd)
   {
      result.push_back(rgn->bezierEditor->copyVerticesToVertexList());
   }

   return result;
}
//---------------------------------------------------------------------------

MaskParameters CMaskTool::getUncomputedMaskParameters()
{
   MaskParameters result;
   result.AllVerticies = CopyVertexLists();
   result.setExclusionBorderWidth(getDefaultExclusionBorderWidth());
   result.setInclusionBorderWidth(getDefaultInclusionBorderWidth());
   result.setIsMaskInclusive(IsMaskInclusive());

   return result;
}
//---------------------------------------------------------------------------

