

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//
// CDisplayLutSettings
//
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

#include "DisplayLutSettings.h"

#include "LUTParser.h" // for wacky format import/export

//////////////////////////////////////////////////////////////////////
// Ini file keys
//////////////////////////////////////////////////////////////////////

// Ini file section name
string CDisplayLutSettings::displayLutSectionName = "DisplayLutSettings";

// Source Color Space settings
string CDisplayLutSettings::useDefaultColorSpaceKey = "UseDefaultColorSpace";
string CDisplayLutSettings::colorSpaceKey = "CustomSrcColorSpace";

// Source Range Settings
string CDisplayLutSettings::useDefaultSourceRangeKey = "UseDefaultSourceRange";
string CDisplayLutSettings::sourceMinRYKey = "CustomSrcMinRY";
string CDisplayLutSettings::sourceMaxRYKey = "CustomSrcMaxRY";
string CDisplayLutSettings::sourceMinGUKey = "CustomSrcMinGU";
string CDisplayLutSettings::sourceMaxGUKey = "CustomSrcMaxGU";
string CDisplayLutSettings::sourceMinBVKey = "CustomSrcMinBV";
string CDisplayLutSettings::sourceMaxBVKey = "CustomSrcMaxBV";

string CDisplayLutSettings::useDefaultSourceRangeExrKey = "UseDefaultSourceRangeExr";
string CDisplayLutSettings::sourceExrExposureAdjustKey = "CustomSrcExrExposureAdjust";
string CDisplayLutSettings::sourceExrDynamicRangeKey = "CustomSrcExrDynamicRange";

// Display Range settings
string CDisplayLutSettings::useDefaultDisplayRangeKey = "UseDefaultDisplayRange";
string CDisplayLutSettings::displayMinRKey = "CustomDispMinR";
string CDisplayLutSettings::displayMaxRKey = "CustomDispMaxR";
string CDisplayLutSettings::displayMinGKey = "CustomDispMinG";
string CDisplayLutSettings::displayMaxGKey = "CustomDispMaxG";
string CDisplayLutSettings::displayMinBKey = "CustomDispMinB";
string CDisplayLutSettings::displayMaxBKey = "CustomDispMaxB";

// Display Gamma settings
string CDisplayLutSettings::useDefaultDisplayGammaKey = "UseDefaultDisplayGamma";
string CDisplayLutSettings::displayGammaKey = "CustomDispGamma";

// Legacy Clip settings
string CDisplayLutSettings::legacyUseCustomLUTKey = "UseCustomLUT";
string CDisplayLutSettings::legacyUseFormatDefaultLUTKey = "UseFormatDefaultLUT";
string CDisplayLutSettings::legacyCustomLUTColorSpaceIndexKey = "CustomLUTColorSpaceIndex";
string CDisplayLutSettings::legacyCustomLUTSourceRangeKey = "UseCustomLUTSourceRange";
string CDisplayLutSettings::legacyCustomLUTRYBlkKey = "CustomLUTRYBlk";
string CDisplayLutSettings::legacyCustomLUTRYWhiKey = "CustomLUTRYWhi";
string CDisplayLutSettings::legacyCustomLUTGUBlkKey = "CustomLUTGUBlk";
string CDisplayLutSettings::legacyCustomLUTGUWhiKey = "CustomLUTGUWhi";
string CDisplayLutSettings::legacyCustomLUTBVBlkKey = "CustomLUTBVBlk";
string CDisplayLutSettings::legacyCustomLUTBVWhiKey = "CustomLUTBVWhi";
string CDisplayLutSettings::legacyCustomLUTRMinKey = "CustomLUTRMin";
string CDisplayLutSettings::legacyCustomLUTRMaxKey = "CustomLUTRMax";
string CDisplayLutSettings::legacyCustomLUTGMinKey = "CustomLUTGMin";
string CDisplayLutSettings::legacyCustomLUTGMaxKey = "CustomLUTGMax";
string CDisplayLutSettings::legacyCustomLUTBMinKey = "CustomLUTBMin";
string CDisplayLutSettings::legacyCustomLUTBMaxKey = "CustomLUTBMax";
string CDisplayLutSettings::legacyGammaKey = "GammaKey";

//--------------------------------------------------------------------
// Construction/Destruction
//--------------------------------------------------------------------

CDisplayLutSettings::CDisplayLutSettings()
{
   InitToDefaults();
}
//--------------------------------------------------------------------


//--------------------------------------------------------------------
// Accessors
//--------------------------------------------------------------------

bool CDisplayLutSettings::UsingDefaultSourceColorSpace() const
{
   return m_usingDefaultColorSpace;
}
//--------------------------------------------------------------------

ELutColorSpace CDisplayLutSettings::GetSourceColorSpace() const
{
   return m_colorSpace;
}
//--------------------------------------------------------------------

bool CDisplayLutSettings::UsingDefaultSourceRange() const
{
   return m_usingDefaultSourceRange;
}
//--------------------------------------------------------------------

bool CDisplayLutSettings::UsingDefaultSourceRangeExr() const
{
   return m_usingDefaultSourceRangeExr;
}
//--------------------------------------------------------------------

const void CDisplayLutSettings::GetSourceRange(MTI_REAL32 minValues[],
                                               MTI_REAL32 maxValues[])
{
   for (int i = 0; i < 3; ++i)
   {
      minValues[i] =  m_sourceRangeMin[i];
      maxValues[i] =  m_sourceRangeMax[i];
   }
}
//--------------------------------------------------------------------

const void CDisplayLutSettings::GetSourceRange(int sourceDepth,
                                     MTI_UINT16 minValues[],
                                     MTI_UINT16 maxValues[])
{
   int maxValue = (1 << sourceDepth) - 1;

   for (int i = 0; i < 3; ++i)
   {
      minValues[i] =  MTI_UINT16(0.5F + maxValue * m_sourceRangeMin[i]);
      maxValues[i] =  MTI_UINT16(0.5F + maxValue * m_sourceRangeMax[i]);
   }
}
//--------------------------------------------------------------------

const void CDisplayLutSettings::GetSourceRangeExr(MTI_UINT16 minValues[],
                                                  MTI_UINT16 maxValues[])
{
   int maxValue = MTI_UINT16(0.5F + (1024.0F * (15.0F - m_sourceExrExposureAdjust))) + 32768;
   int minValue = maxValue - MTI_UINT16(0.5F + (1024.0F * m_sourceExrDynamicRange));
   minValues[0] = minValues[1] = minValues[2] = minValue;
   maxValues[0] = maxValues[1] = maxValues[2] = maxValue;
}
//--------------------------------------------------------------------

const void CDisplayLutSettings::GetSourceRangeExr(MTI_REAL32 &exposureAdjust,
                                                  MTI_REAL32 &dynamicRange)
{
   exposureAdjust = m_sourceExrExposureAdjust;
   dynamicRange = m_sourceExrDynamicRange;
}
//--------------------------------------------------------------------

bool CDisplayLutSettings::UsingDefaultDisplayRange() const
{
   return m_usingDefaultDisplayRange;
}
//--------------------------------------------------------------------

void const CDisplayLutSettings::GetDisplayRange(MTI_UINT16 displayRangeMin[],
                                          MTI_UINT16 displayRangeMax[])
{
   for (int i = 0; i < 3; ++i)
   {
      displayRangeMin[i] = m_displayRangeMin[i];
      displayRangeMax[i] = m_displayRangeMax[i];
   }
}
//--------------------------------------------------------------------

bool CDisplayLutSettings::UsingDefaultDisplayGamma() const
{
   return m_usingDefaultDisplayGamma;
}
//--------------------------------------------------------------------

const MTI_REAL32 CDisplayLutSettings::GetDisplayGamma()
{
   return m_displayGamma;
}
//--------------------------------------------------------------------

const MTI_UINT32 CDisplayLutSettings::GetDisplayGammaTimes10()
{
   return MTI_UINT32(m_displayGamma * 10.F + 0.5F);
}
//--------------------------------------------------------------------


//--------------------------------------------------------------------
// Mutators
//--------------------------------------------------------------------

void CDisplayLutSettings::UseDefaultSourceColorSpace(bool flag)
{
   m_usingDefaultColorSpace = flag;
   Conform();
}
//--------------------------------------------------------------------

void CDisplayLutSettings::SetSourceColorSpace(ELutColorSpace newColorSpace)
{
   m_colorSpace = newColorSpace;
   Conform();
}
//--------------------------------------------------------------------

void CDisplayLutSettings::UseDefaultSourceRange(bool flag)
{
   m_usingDefaultSourceRange = flag;
   Conform();
}
//--------------------------------------------------------------------

void CDisplayLutSettings::UseDefaultSourceRangeExr(bool flag)
{
   m_usingDefaultSourceRangeExr = flag;
   Conform();
}
//--------------------------------------------------------------------

void CDisplayLutSettings::SetSourceRange(const MTI_REAL32 *newMinValues,
                                         const MTI_REAL32 *newMaxValues)
{
   for (int i = 0; i < 3; ++i)
   {
      m_sourceRangeMin[i] = newMinValues[i];
      m_sourceRangeMax[i] = newMaxValues[i];
   }

   Conform();
}
//--------------------------------------------------------------------

void CDisplayLutSettings::SetSourceRange(int bitsPerComponent,
                                         const MTI_UINT16 *newMinValues,
                                         const MTI_UINT16 *newMaxValues)
{
   if (m_colorSpace == LUTCS_EXR)
   {
      MTI_REAL32 minValue = (newMinValues[0] - 32768) / (15.0F * 1024.0F);
      MTI_REAL32 maxValue = (newMaxValues[1] - 32768) / (15.0F * 1024.0F);

      m_sourceRangeMin[0] = m_sourceRangeMin[1] = m_sourceRangeMin[2] = minValue;
      m_sourceRangeMin[0] = m_sourceRangeMin[1] = m_sourceRangeMin[2] = maxValue;
   }
   else
   {
      MTI_REAL32 maxValue = MTI_REAL32((1 << bitsPerComponent) - 1);

      for (int i = 0; i < 3; ++i)
      {
         MTIassert(newMinValues[i] >= 0 && newMinValues[i] <= maxValue);
         MTIassert(newMaxValues[i] >= 0 && newMaxValues[i] <= maxValue);

         m_sourceRangeMin[i] =  newMinValues[i] / maxValue;
         m_sourceRangeMax[i] =  newMaxValues[i] / maxValue;
      }
   }

   Conform();
}
//--------------------------------------------------------------------

void CDisplayLutSettings::SetSourceRangeExr(const MTI_REAL32 newExposureAdjust,
                                            const MTI_REAL32 newDynamicRange)
{
   m_sourceExrExposureAdjust = newExposureAdjust;
   m_sourceExrDynamicRange = newDynamicRange;

   Conform();
}
//--------------------------------------------------------------------

void CDisplayLutSettings::UseDefaultDisplayRange(bool flag)
{
   m_usingDefaultDisplayRange = flag;
   Conform();
}
//--------------------------------------------------------------------

void CDisplayLutSettings::SetDisplayRange(const MTI_UINT16 *newMinValues,
                                          const MTI_UINT16 *newMaxValues)
{
   for (int i = 0; i < 3; ++i)
   {
      m_displayRangeMin[i] = newMinValues[i];
      m_displayRangeMax[i] = newMaxValues[i];
   }
   Conform();
}
//--------------------------------------------------------------------

void CDisplayLutSettings::UseDefaultDisplayGamma(bool flag)
{
   m_usingDefaultDisplayGamma = flag;
   Conform();
}
//--------------------------------------------------------------------

void CDisplayLutSettings::SetDisplayGamma(MTI_REAL32 newGamma)
{
   m_displayGamma = newGamma;
   Conform();
}
//--------------------------------------------------------------------

void CDisplayLutSettings::SetDisplayGammaTimes10(int newGammaTimes10)
{
   SetDisplayGamma(MTI_REAL32(newGammaTimes10 / 10.F));
}
//--------------------------------------------------------------------


//--------------------------------------------------------------------
// Initializors
//--------------------------------------------------------------------

void CDisplayLutSettings::InitToDefaults()
{
   m_usingDefaultColorSpace = true;
   m_usingDefaultSourceRange = true;
   m_usingDefaultSourceRangeExr = true;
   m_usingDefaultDisplayRange = true;
   m_usingDefaultDisplayGamma = true;

   m_colorSpace = LUTCS_INVALID;

   for (int i = 0; i < 3; ++i)
   {
      m_sourceRangeMin[i] = (m_colorSpace == LUTCS_EXR) ? 0.5F : 0.F;
      m_sourceRangeMax[i] = (m_colorSpace == LUTCS_EXR) ? 0.734375F : 1.F;
      m_displayRangeMin[i] = 0;
      m_displayRangeMax[i] = 255;
   }

   m_sourceExrExposureAdjust = 0.0F;
   m_sourceExrDynamicRange = 15.0F;

   // TODO: Need to get this from PREFERENCES WINDOW!  QQQ
   m_displayGamma = DEFAULT_DISPLAY_GAMMA;
}
//--------------------------------------------------------------------

//--------------------------------------------------------------------
// Reader / writers
//--------------------------------------------------------------------

int CDisplayLutSettings::ReadFromIniFile(const string &iniFileName,
                                    const string &sectionNamePrefix)
{
   CIniFile *ini = ::CreateIniFile(iniFileName, true); // true -> R/O
   if (ini == NULL)
   {
      TRACE_0(errout << "ERROR: Cannot read LUT settings from ini file "
                     << iniFileName);
      return -1;
   }

   ReadFromIniFile(ini, sectionNamePrefix);

   // Done - Note this does not actually DELETE the ini file, just closes it!!!!
   DeleteIniFile(ini);

   Conform();

   return 0;
}
//--------------------------------------------------------------------

int CDisplayLutSettings::SaveToIniFile(const string &iniFileName,
                                       const string &sectionNamePrefix)
{
   CIniFile *ini = ::CreateIniFile(iniFileName, false); // false -> R/W
   if (ini == NULL)
   {
      TRACE_0(errout << "ERROR: Cannot write LUT settings to ini file "
                     << iniFileName);
      return -1;
   }

   SaveToIniFile(ini, sectionNamePrefix);

   // Done - Note this does not actually DELETE the ini file, just closes it!!!!
   DeleteIniFile(ini);

   return 0;
}
//--------------------------------------------------------------------

int CDisplayLutSettings::ReadFromIniFile(CIniFile *ini,
                                    const string &sectionNamePrefix)
{
   MTIassert(ini != NULL);

   string sectionName = makeSectionName(sectionNamePrefix);

   InitToDefaults();

   m_usingDefaultColorSpace = ini->ReadBool(sectionName,
                                               useDefaultColorSpaceKey,
                                               true);
   m_colorSpace = ELutColorSpace(ini->ReadInteger(sectionName,
                                               colorSpaceKey,
                                               LUTCS_INVALID));

   m_usingDefaultSourceRange = ini->ReadBool(sectionName,
                                               useDefaultSourceRangeKey,
                                               true);
   m_sourceRangeMin[0] = float(ini->ReadDouble(sectionName,
                                               sourceMinRYKey,
                                               0.0));
   m_sourceRangeMin[1] = float(ini->ReadDouble(sectionName,
                                               sourceMinGUKey,
                                               0.0));
   m_sourceRangeMin[2] = float(ini->ReadDouble(sectionName,
                                               sourceMinBVKey,
                                               0.0));
   m_sourceRangeMax[0] = float(ini->ReadDouble(sectionName,
                                               sourceMaxRYKey,
                                               1.0));
   m_sourceRangeMax[1] = float(ini->ReadDouble(sectionName,
                                               sourceMaxGUKey,
                                               1.0));
   m_sourceRangeMax[2] = float(ini->ReadDouble(sectionName,
                                               sourceMaxBVKey,
                                               1.0));

   m_usingDefaultSourceRangeExr = ini->ReadBool(sectionName,
                                               useDefaultSourceRangeExrKey,
                                               true);
   m_sourceExrExposureAdjust = float(ini->ReadDouble(sectionName,
                                               sourceExrExposureAdjustKey,
                                               0.0));
   m_sourceExrDynamicRange = float(ini->ReadDouble(sectionName,
                                               sourceExrDynamicRangeKey,
                                               15.0));

   m_usingDefaultDisplayRange = ini->ReadBool(sectionName,
                                               useDefaultDisplayRangeKey,
                                               true);
   m_displayRangeMin[0] = MTI_UINT16(ini->ReadInteger(sectionName,
                                               displayMinRKey,
                                               0));
   m_displayRangeMin[1] = MTI_UINT16(ini->ReadInteger(sectionName,
                                               displayMinGKey,
                                               0));
   m_displayRangeMin[2] = MTI_UINT16(ini->ReadInteger(sectionName,
                                               displayMinBKey,
                                               0));
   m_displayRangeMax[0] = MTI_UINT16(ini->ReadInteger(sectionName,
                                               displayMaxRKey,
                                               255));
   m_displayRangeMax[1] = MTI_UINT16(ini->ReadInteger(sectionName,
                                               displayMaxGKey,
                                               255));
   m_displayRangeMax[2] = MTI_UINT16(ini->ReadInteger(sectionName,
                                               displayMaxBKey,
                                               255));

   m_usingDefaultDisplayGamma = ini->ReadBool(sectionName,
                                               useDefaultDisplayGammaKey,
                                               true);
   m_displayGamma = float(ini->ReadDouble(sectionName,
                                               displayGammaKey,
                                               DEFAULT_DISPLAY_GAMMA));

   Conform();

   return 0;
}
//--------------------------------------------------------------------

int CDisplayLutSettings::SaveToIniFile(CIniFile *ini,
                                       const string &sectionNamePrefix)
{
   MTIassert(ini != NULL);

   string sectionName = makeSectionName(sectionNamePrefix);

   Conform();

   ini->EraseSection(sectionName);

   ini->WriteBool(sectionName, useDefaultColorSpaceKey,
                                   m_usingDefaultColorSpace);
   if (!m_usingDefaultColorSpace)
   {
      ini->WriteInteger(sectionName, colorSpaceKey,
                                      int(m_colorSpace));
   }

   ini->WriteBool(sectionName, useDefaultSourceRangeKey,m_usingDefaultSourceRange);
   if (!m_usingDefaultSourceRange)
   {
      ini->WriteDouble(sectionName, sourceMinRYKey, m_sourceRangeMin[0]);
      ini->WriteDouble(sectionName, sourceMinGUKey, m_sourceRangeMin[1]);
      ini->WriteDouble(sectionName, sourceMinBVKey, m_sourceRangeMin[2]);
      ini->WriteDouble(sectionName, sourceMaxRYKey, m_sourceRangeMax[0]);
      ini->WriteDouble(sectionName, sourceMaxGUKey, m_sourceRangeMax[1]);
      ini->WriteDouble(sectionName, sourceMaxBVKey, m_sourceRangeMax[2]);
   }

   ini->WriteBool(sectionName, useDefaultSourceRangeExrKey, m_usingDefaultSourceRangeExr);
   if (!m_usingDefaultSourceRangeExr)
   {
      ini->WriteDouble(sectionName, sourceExrExposureAdjustKey, m_sourceExrExposureAdjust);
      ini->WriteDouble(sectionName, sourceExrDynamicRangeKey,m_sourceExrDynamicRange);
   }

   ini->WriteBool(sectionName, useDefaultDisplayRangeKey, m_usingDefaultDisplayRange);
   if (!m_usingDefaultDisplayRange)
   {
      ini->WriteInteger(sectionName, displayMinRKey, m_displayRangeMin[0]);
      ini->WriteInteger(sectionName, displayMinGKey, m_displayRangeMin[1]);
      ini->WriteInteger(sectionName, displayMinBKey, m_displayRangeMin[2]);
      ini->WriteInteger(sectionName, displayMaxRKey, m_displayRangeMax[0]);
      ini->WriteInteger(sectionName, displayMaxGKey, m_displayRangeMax[1]);
      ini->WriteInteger(sectionName, displayMaxBKey, m_displayRangeMax[2]);
   }

   ini->WriteBool(sectionName, useDefaultDisplayGammaKey, m_usingDefaultDisplayGamma);
   if (!m_usingDefaultDisplayGamma)
   {
      ini->WriteDouble(sectionName, displayGammaKey, m_displayGamma);
   }

   return 0;
}
//---------------------------------------------------------------------------

void CDisplayLutSettings::EraseIniSection(CIniFile *ini,
                     const string &sectionNamePrefix)
{
   string sectionName = makeSectionName(sectionNamePrefix);

   ini->EraseSection(sectionName);
}
//---------------------------------------------------------------------------

string CDisplayLutSettings::translateColorSpaceToWackyFormat(EColorSpace defaultColorSpace)
{
   string retVal = LUTDESC_OTHER;

   if (m_usingDefaultColorSpace)
   {
      switch (defaultColorSpace)
      {
         case IF_COLOR_SPACE_LINEAR: // Typically RGB
            retVal = LUTDESC_LINEARLINEAR;
            break;
         case IF_COLOR_SPACE_LOG: // Typically RGB
            retVal = LUTDESC_LOGLINEAR;
            break;
         case IF_COLOR_SPACE_EXR:
            retVal = LUTDESC_EXRLINEAR;
            break;
         case IF_COLOR_SPACE_SMPTE_240:
            retVal = LUTDESC_SMPTELINEAR;
            break;
         case IF_COLOR_SPACE_CCIR_709:
            retVal = LUTDESC_CCIR709LINEAR;
            break;
         case IF_COLOR_SPACE_CCIR_601_BG:
            retVal = LUTDESC_CCIR601BGLINEAR;
            break;
         case IF_COLOR_SPACE_CCIR_601_M:
            retVal = LUTDESC_CCIR601MLINEAR;
            break;

         default:
//   #ifdef _DEBUG
//            throw "Unhandled defaultColorSpace in translateColorSpaceToWackyFormat";
//   #endif
            retVal = LUTDESC_LINEARLINEAR;
            break;
      }
   }
   else
   {
      switch (m_colorSpace)
      {
         case LUTCS_LINEAR:
            retVal = LUTDESC_LINEARLINEAR;
            break;
         case LUTCS_LOG:
            retVal = LUTDESC_LOGLINEAR;
            break;
         case LUTCS_EXR:
            retVal = LUTDESC_EXRLINEAR;
            break;
         case LUTCS_SMPTE_240:
            retVal = LUTDESC_SMPTELINEAR;
            break;
         case LUTCS_CCIR_709:
            retVal = LUTDESC_CCIR709LINEAR;
            break;
         case LUTCS_CCIR_601M:
            retVal = LUTDESC_CCIR601MLINEAR;
            break;
         case LUTCS_CCIR_601BG:
            retVal = LUTDESC_CCIR601BGLINEAR;
            break;

         default:
//   #ifdef _DEBUG
//            throw "Unhandled m_colorSpace in translateColorSpaceToWackyFormat";
//   #endif
            retVal = LUTDESC_LINEARLINEAR;
            break;
      }
   }

   return retVal;
}
//---------------------------------------------------------------------------

void CDisplayLutSettings::ImportSettingsFromWackyLutFormat(
                            const string &filePath,
                            const CImageFormat *currentClipImageFormat)
{
   CLUTParser lutParser;

   lutParser.setFilename(filePath.c_str());
   if(!lutParser.load())
   {
     MessageBox(NULL, lutParser.getLastErrorMsg(),
             "LUT Import", MB_OK | MB_TASKMODAL | MB_ICONERROR);
     return;
   }

   // Wacky formt does not encode 'default'
   m_usingDefaultColorSpace = false;
   m_usingDefaultSourceRange = false;
   m_usingDefaultSourceRangeExr = false;
   m_usingDefaultDisplayRange = false;
   m_usingDefaultDisplayGamma = false;

   // Parse color space
   if(strstr(lutParser.getDescription(), LUTDESC_LOGLINEAR) != NULL)
      m_colorSpace = LUTCS_LOG;
   else if(strstr(lutParser.getDescription(), LUTDESC_LINEARLINEAR) != NULL)
      m_colorSpace = LUTCS_LINEAR;
   else if(strstr(lutParser.getDescription(), LUTDESC_EXRLINEAR) != NULL)
      m_colorSpace = LUTCS_EXR;
   else if(strstr(lutParser.getDescription(), LUTDESC_SMPTELINEAR) != NULL)
      m_colorSpace = LUTCS_SMPTE_240;
   else if(strstr(lutParser.getDescription(), LUTDESC_CCIR709LINEAR) != NULL)
      m_colorSpace = LUTCS_CCIR_709;
   else if(strstr(lutParser.getDescription(), LUTDESC_CCIR601BGLINEAR) != NULL)
      m_colorSpace = LUTCS_CCIR_601BG;
   else if(strstr(lutParser.getDescription(), LUTDESC_CCIR601MLINEAR) != NULL)
      m_colorSpace = LUTCS_CCIR_601M;
   else
      m_usingDefaultColorSpace = true;

   // Parse source range - note wacky format does not encode bit depth,
   // so we use whatever the current clip has!
   int bitDepth;
   if (currentClipImageFormat != NULL)
   {
      bitDepth = currentClipImageFormat->getBitsPerComponent();
   }
   else
   {
      // Don't have the clip image format.... guess the bit depth by
      // peeking at the MAX RED level!!!! Bwaaa-haaa-haaaaaa!!

      int refWhite0 = lutParser.getRefWhite(0);

      if (refWhite0 < 256)
         bitDepth = 8;
      else if (refWhite0 < 1024)
         bitDepth = 10;
      else if (refWhite0 < 4096)
         bitDepth = 12;
      else if (refWhite0 < 16384)
         bitDepth = 14;
      else
         bitDepth = 16;
   }
   float maxValue = float((1 << bitDepth) - 1);

   m_sourceRangeMin[0] = lutParser.getRefBlack(0) / maxValue;
   m_sourceRangeMin[1] = lutParser.getRefBlack(1) / maxValue;
   m_sourceRangeMin[2] = lutParser.getRefBlack(2) / maxValue;
   m_sourceRangeMax[0] = lutParser.getRefWhite(0) / maxValue;
   m_sourceRangeMax[1] = lutParser.getRefWhite(1) / maxValue;
   m_sourceRangeMax[2] = lutParser.getRefWhite(2) / maxValue;

   // QQQ need to derive these from min/maxes
   m_sourceExrExposureAdjust = 1.0F;
   m_sourceExrDynamicRange = 15.0F;

   // Parse display range
   m_displayRangeMin[0] = lutParser.getShadow(0);
   m_displayRangeMin[1] = lutParser.getShadow(1);
   m_displayRangeMin[2] = lutParser.getShadow(2);
   m_displayRangeMax[0] = lutParser.getHighlight(0);
   m_displayRangeMax[1] = lutParser.getHighlight(1);
   m_displayRangeMax[2] = lutParser.getHighlight(2);

   // Parse display gamma... for some reason we get it from what's labeled
   // the 'film gamma' instead of from what's labeled the 'display gamma' --
   // no idea why - we also only use the gamma from the first component
   // because we can't deal with gamma-per-component! That makes use of this
   // format even stupider
   m_displayGamma = lutParser.getGamma(0);  // take component 0's (R/Y)

   // filter out complete wackiness
   Conform();
}
//---------------------------------------------------------------------------

void CDisplayLutSettings::ExportSettingsInWackyLutFormat(
                            const string &filePath,
                            const CImageFormat *currentClipImageFormat)
{
   MTIassert(currentClipImageFormat != NULL);

   CLUTParser lutParser;
   EColorSpace defaultColorSpace = currentClipImageFormat->getColorSpace();
   int bitDepth = currentClipImageFormat->getBitsPerComponent();

   string description = translateColorSpaceToWackyFormat(defaultColorSpace);
   lutParser.setDescription(description.c_str());

   lutParser.setFilename(filePath.c_str());

   // For source, convert float fractions to MTI_UINT16

   MTI_UINT16 maxSource = (1 << bitDepth) - 1;
   MTI_UINT16 sourceRangeMin[3];
   sourceRangeMin[0] = MTI_INT16(maxSource * m_sourceRangeMin[0] + 0.5);
   sourceRangeMin[1] = MTI_INT16(maxSource * m_sourceRangeMin[1] + 0.5);
   sourceRangeMin[2] = MTI_INT16(maxSource * m_sourceRangeMin[2] + 0.5);
   lutParser.setRefBlack(sourceRangeMin);

   MTI_UINT16 sourceRangeMax[3];
   sourceRangeMax[0] = MTI_INT16(maxSource * m_sourceRangeMax[0] + 0.5);
   sourceRangeMax[1] = MTI_INT16(maxSource * m_sourceRangeMax[1] + 0.5);
   sourceRangeMax[2] = MTI_INT16(maxSource * m_sourceRangeMax[2] + 0.5);
   lutParser.setRefWhite(sourceRangeMax);

   // Display settings are already MTI_UINT16's

   lutParser.setShadow(m_displayRangeMin);
   lutParser.setHighlight(m_displayRangeMax);

   // We don't do 'soft clipping'
   MTI_UINT16 customDisplaySoftClip[3];
   customDisplaySoftClip[0] = 0;
   customDisplaySoftClip[1] = 0;
   customDisplaySoftClip[2] = 0;
   lutParser.setSoftclip(customDisplaySoftClip);

   // Don't know why we set this gamma (film gamma) instead of the
   // other one (display gamma)
   float customDisplayGamma[3];
   customDisplayGamma[0] = m_displayGamma;
   customDisplayGamma[1] = m_displayGamma;
   customDisplayGamma[2] = m_displayGamma;
   lutParser.setGamma(customDisplayGamma);

   // Don't know why we set the other gamma (film gamma) instead of this
   // one (display gamma)
   float customDisplayGammaCorrect[3];
   customDisplayGammaCorrect[0] = 1.0;
   customDisplayGammaCorrect[1] = 1.0;
   customDisplayGammaCorrect[2] = 1.0;
   lutParser.setGammaCorrect(customDisplayGammaCorrect);  // WTF??!?

   // Write out the stupid file
   if(!lutParser.save())
   {
      MessageBox(NULL, lutParser.getLastErrorMsg(), "Custom LUT Saving",
                 MB_OK | MB_ICONERROR | MB_TASKMODAL);
   }
}
//------------------------------------------------------------------------

int CDisplayLutSettings::ImportLegacyClipCustomLutSettings(
                            CIniFile *iniFile,
                            const string &sectionName,
                            const CImageFormat *fmt,
                            int defaultGammaTimes10)
{
   bool useLegacyCustomLut = iniFile->ReadBool(sectionName,
                                            legacyUseCustomLUTKey,
                                            false);

   // We no longer care about this option....
   //bool newUseFormatDefaultLut = iniFile->ReadBool(sectionName,
                                            //legacyUseFormatDefaultLUTKey,
                                            //true);

   // If we weren't using the legacy custom LUT, set everything to
   // "default" and we're done!
   if (!useLegacyCustomLut)
   {
      m_usingDefaultColorSpace = true;
      m_usingDefaultSourceRange = true;
      m_usingDefaultSourceRangeExr = true;
      m_usingDefaultDisplayRange = true;
      m_usingDefaultDisplayGamma = true;

      return 0;
   }

   // the color space index of the LUT:
   //
   //    0 - default
   //    1 - linear
   //    2 - log
   //    3 - SMPTE 240
   //    4 - CCIR 709
   //    5 - CCIR 601BG
   //    6 - CCIR 601M
   int newColorSpaceIndex = int(iniFile->ReadInteger(sectionName,
                                             legacyCustomLUTColorSpaceIndexKey,
                                             0));
   if (newColorSpaceIndex == 0)
   {
      m_usingDefaultColorSpace = true;
   }
   else
   {
      m_usingDefaultColorSpace = false;

      switch (newColorSpaceIndex)
      {
         case 1:
            m_colorSpace = LUTCS_LINEAR;
            break;
         case 2:
            m_colorSpace = LUTCS_LOG;
            break;
         case 3:
            m_colorSpace = LUTCS_SMPTE_240;
            break;
         case 4:
            m_colorSpace = LUTCS_CCIR_709;
            break;
         case 5:
            m_colorSpace = LUTCS_CCIR_601BG;
            break;
         case 6:
            m_colorSpace = LUTCS_CCIR_601M;
            break;
      }
   }

   bool newUseCustomRange = iniFile->ReadBool(sectionName,
                                              legacyCustomLUTSourceRangeKey,
                                              false);
   m_usingDefaultSourceRange = !newUseCustomRange;

   if (!m_usingDefaultSourceRange)
   {
      MTI_UINT16 sourceRangeMin[3];
      MTI_UINT16 sourceRangeMax[3];

      // load defaults in case desktop.ini has none
      fmt->getComponentValueMin(sourceRangeMin);
      fmt->getComponentValueMax(sourceRangeMax);

      // read the custom source range values from desktop.ini
      //  -- the defaults come from the clip's image format

      sourceRangeMin[0] = MTI_UINT16(iniFile->ReadInteger(sectionName,
                                               legacyCustomLUTRYBlkKey,
                                               sourceRangeMin[0]));

      sourceRangeMax[0] = MTI_UINT16(iniFile->ReadInteger(sectionName,
                                               legacyCustomLUTRYWhiKey,
                                               sourceRangeMax[0]));

      sourceRangeMin[1] = MTI_UINT16(iniFile->ReadInteger(sectionName,
                                               legacyCustomLUTGUBlkKey,
                                               sourceRangeMin[1]));

      sourceRangeMax[1] = MTI_UINT16(iniFile->ReadInteger(sectionName,
                                               legacyCustomLUTGUWhiKey,
                                               sourceRangeMax[1]));

      sourceRangeMin[2] = MTI_UINT16(iniFile->ReadInteger(sectionName,
                                               legacyCustomLUTBVBlkKey,
                                               sourceRangeMin[2]));

      sourceRangeMax[2] = MTI_UINT16(iniFile->ReadInteger(sectionName,
                                               legacyCustomLUTBVWhiKey,
                                               sourceRangeMax[2]));

      SetSourceRange(fmt->getBitsPerComponent(), sourceRangeMin, sourceRangeMax);
   }

   // QQQ need to derive these from min/maxes
   m_sourceExrExposureAdjust = 1.0F;
   m_sourceExrDynamicRange = 15.0F;

   // read the custom display range values from desktop.ini
   // -- the defaults are just 0 and 255 for min & max

   m_displayRangeMin[0] = MTI_UINT16(iniFile->ReadInteger(sectionName,
                                             legacyCustomLUTRMinKey,
                                             0));

   m_displayRangeMax[0] = MTI_UINT16(iniFile->ReadInteger(sectionName,
                                             legacyCustomLUTRMaxKey,
                                             255));

   m_displayRangeMin[1] = MTI_UINT16(iniFile->ReadInteger(sectionName,
                                             legacyCustomLUTGMinKey,
                                             0));

   m_displayRangeMax[1] = MTI_UINT16(iniFile->ReadInteger(sectionName,
                                             legacyCustomLUTGMaxKey,
                                             255));

   m_displayRangeMin[2] = MTI_UINT16(iniFile->ReadInteger(sectionName,
                                             legacyCustomLUTBMinKey,
                                             0));

   m_displayRangeMax[2] = MTI_UINT16(iniFile->ReadInteger(sectionName,
                                             legacyCustomLUTBMaxKey,
                                             255));

   // Lagacy doesn't have explicit "User default display Range", so fake it
   if (m_displayRangeMin[0] == 0 && m_displayRangeMin[1] == 0 &&
       m_displayRangeMin[2] == 0 && m_displayRangeMax[0] == 255 &&
       m_displayRangeMax[1] == 255 && m_displayRangeMax[2] == 255)
   {
      m_usingDefaultDisplayRange = true;
   }
   else
   {
      m_usingDefaultDisplayRange = false;
   }

   // read the gamma
   int newGammaValue = int(iniFile->ReadInteger(sectionName,
                                                legacyGammaKey,
                                                defaultGammaTimes10));
   SetDisplayGammaTimes10(newGammaValue);
   m_usingDefaultDisplayGamma = (newGammaValue == defaultGammaTimes10);


   return 0;
}
//--------------------------------------------------------------------

bool CDisplayLutSettings::DoesDisplayLutSettingsSectionExist(
                     CIniFile *ini,
                     const string &sectionNamePrefix)
{
   string sectionName = makeSectionName(sectionNamePrefix);

   return ini->SectionExists(sectionName);
}
//--------------------------------------------------------------------

//--------------------------------------------------------------------
// Local crap
//--------------------------------------------------------------------

string CDisplayLutSettings::makeSectionName(const string &sectionNamePrefix)
{
   string sectionName = sectionNamePrefix;
   if (sectionName.empty())
   {
      sectionName = displayLutSectionName;
   }
   else
   {
      if (sectionName[sectionName.length()-1] != '/')
         sectionName += "/";
      sectionName += displayLutSectionName;
   }

   return sectionName;
}
//--------------------------------------------------------------------

void CDisplayLutSettings::Conform()
{
   for (int i = 0; i < 3; ++i)
   {
      if (m_sourceRangeMin[i] < 0.F || m_sourceRangeMin[i] > 1.F)
          m_sourceRangeMin[i] = 0.F;
      if (m_sourceRangeMax[i] < m_sourceRangeMin[i])
          m_sourceRangeMax[i] = m_sourceRangeMin[i];
      if (m_sourceRangeMax[i] > 1.F)
          m_sourceRangeMax[i] = 1.F;
   }

   for (int i = 0; i < 3; ++i)
   {
      if (m_displayRangeMin[i] < 0 || m_displayRangeMin[i] > 255)
          m_displayRangeMin[i] = 0;
      if (m_displayRangeMax[i] < m_displayRangeMin[i])
          m_displayRangeMax[i] = m_displayRangeMin[i];
      if (m_displayRangeMax[i] > 255)
          m_displayRangeMax[i] = 255;
   }
   if (m_displayGamma < MIN_DISPLAY_GAMMA || m_displayGamma > MAX_DISPLAY_GAMMA)
      m_displayGamma = DEFAULT_DISPLAY_GAMMA;
}
//--------------------------------------------------------------------
