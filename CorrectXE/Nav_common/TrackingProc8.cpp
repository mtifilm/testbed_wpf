// TrackingProc8.cpp: implementation of the CTrackingProc16 class.
//
//////////////////////////////////////////////////////////////////////

#include "TrackingProc8.h"

#include "bthread.h"
#include "Clip3.h"
#include "err_tool.h"
#include "ImageFormat3.h"
#include "IniFile.h"  // for TRACE_N()
#include "MathFunctions.h"
#include "MTIKeyDef.h"
#include "MTImalloc.h"
#include "MTIsleep.h"
#include "MTIstringstream.h"
#include "PixelRegions.h"
#include "ThreadLocker.h"
#include "ToolCommand.h"
#include "ToolCommandTable.h"
#include "ToolProgressMonitor.h"
#include "ToolSystemInterface.h"
#include "ToolUserInputMap.h"
#include "TrackingBoxManager.h"
#include "TrackingTool.h"

#define TRACKING_MIN_THREADS 1000  // effectively disable multithreading
static CSpinLock TrackingThreadLock;
#define MAX_FILTER_RADIUS 10
#define REFINED_MATCH_SCALE 5


///////////////////////////////////////////////////////////////////////////////
// Construction/Destruction
///////////////////////////////////////////////////////////////////////////////

CTrackingProc::CTrackingProc(int newToolNumber, const string &newToolName,
                                 CToolSystemInterface *newSystemAPI,
                                 const bool *newEmergencyStopFlagPtr,
                                 IToolProgressMonitor *newToolProgressMonitor)
 : CToolProcessor(newToolNumber, newToolName, 1, 1, newSystemAPI,
                  newEmergencyStopFlagPtr, newToolProgressMonitor)
{
   StartProcessThread(); // Required by Tool Infrastructure
}

CTrackingProc::~CTrackingProc()
{
   // Shouldn't the subimages array be cleared? QQQ
}


///////////////////////////////////////////////////////////////////////////////
// Setup
///////////////////////////////////////////////////////////////////////////////

int CTrackingProc::SetIOConfig(CToolIOConfig *toolIOConfig)
{
   // Tell Tool Infrastructure, via CToolProcessor supertype, about the
   // number of input and output frames

   SetInputMaxFrames(0, 2, 1);  // 'In' port index 0,
                                // Need two frames for each iteration,
                                // Need one new frame each iteration.

   SetOutputMaxFrames(0, 1);  // 'Out' port index 0,
                              // One frame output per iteration

   // Well, really the frame is not modified at all, but we need
   // to 'output' it to display it
   SetOutputModifyInPlace(0, 0, false); // 'Out' port index 0,
                                        // 'In' port index 0,
                                        // Do not clone the output frame

   return 0;
}

int CTrackingProc::SetFrameRange(CToolFrameRange *toolFrameRange)
{
   CToolFrameRange::SFrameRange &frameRange = toolFrameRange->inFrameRange[0];
   _inFrameIndex = frameRange.inFrameIndex;
   _outFrameIndex = frameRange.outFrameIndex;

   return 0;
}

int CTrackingProc::SetParameters(CToolParameters *toolParams)
{
   int retVal=0;

   CTrackingToolParameters *trackingToolParams
                        = static_cast<CTrackingToolParameters*>(toolParams);

   // Make a private copy of the parameters for later reference
   // Caller's toolParam is deleted after this function returns
   _trkPtsEditor = trackingToolParams->trackingParameters.TrkPtsEditor;
   _trackingArray = _trkPtsEditor->getTrackingArray();
//   _trackingArray->getExtents(_TrackBoxExtentX, _TrackBoxExtentY,
//                              _SearchAreaLeftExtent, _SearchAreaRightExtent,
//                              _SearchAreaTopExtent, _SearchAreaBottomExtent);
   int trackBoxExtent = _trackingArray->getTrackingBoxRadius();
   _TrackBoxExtentX = _TrackBoxExtentY = trackBoxExtent;
   _TrackBoxSize = MtiSize((2 * _TrackBoxExtentX) + 1, (2 * _TrackBoxExtentY) + 1);

   int searchyBoxExtent = _trackingArray->getSearchBoxRadius();
   _SearchAreaLeftExtent =
      _SearchAreaRightExtent =
         _SearchAreaTopExtent =
            _SearchAreaBottomExtent = trackBoxExtent + searchyBoxExtent;
   _SearchAreaSize = MtiSize(_SearchAreaLeftExtent + 1 + _SearchAreaRightExtent,
                             _SearchAreaTopExtent + 1 + _SearchAreaBottomExtent);

   // Grab the display LUT up front if we're going to need it.
   if (_trackingArray->getApplyLutFlag())
   {
      MTImemcpy(_currentDisplayLut8, trackingToolParams->trackingParameters.lut8, 256 * 3);
   }

   _trackingToolObject = trackingToolParams->trackingParameters.TrackingToolObject;

   // Find the iteration count
   _iterationCount = _outFrameIndex - _inFrameIndex;

   // Done
   return retVal;
}

void CTrackingProc::SetDebugParameters(Ipp8uArray &debugVisualizationArray, CSpinLock *debugVisualizationLock)
{
   _debugVisualizationArray = debugVisualizationArray;
   _debugVisualizationLock = debugVisualizationLock;
}



///////////////////////////////////////////////////////////////////////////////
//
// Processing functions
//
///////////////////////////////////////////////////////////////////////////////

int CTrackingProc::GetIterationCount(SToolProcessingData &procData)
{
   // This function is called by the Tool Infrastructure so it knows
   // how many processing iterations to do before stopping

   return _iterationCount;
}
///////////////////////////////////////////////////////////////////////////////

int CTrackingProc::BeginProcessing(SToolProcessingData &procData)
{
   CAutoErrorReporter autoErr("CTrackingProc::BeginProcessing",
                              AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   int retVal = 0;
   int nTmpX, nTmpY;

   // Tracking does not save to history, since it doesn't modify the frames, duh!
   SetSaveToHistory(false);

   // Just save some values for later
   const CImageFormat *imageFormat = GetSrcImageFormat(0);
   _nPicCol = imageFormat->getPixelsPerLine();
   _nPicRow = imageFormat->getLinesPerFrame();
   _RGB = imageFormat->getPixelComponents() == IF_PIXEL_COMPONENTS_RGB
        || imageFormat->getPixelComponents() == IF_PIXEL_COMPONENTS_RGBA
        || imageFormat->getPixelComponents() == IF_PIXEL_COMPONENTS_Y
        || imageFormat->getPixelComponents() == IF_PIXEL_COMPONENTS_YYY;

   const CImageFormat *realImageFormat = GetSystemAPI()->getVideoClipImageFormat();
   _sourceDepth = realImageFormat->getBitsPerComponent();

   if (_trackingArray->getApplyLutFlag())
   {
      int channel = _trackingArray->getChannel();
      vector<float> compWeights = { 0.32F, 0.64F, 0.04F };
      switch (channel)
      {
         case 0:
            compWeights = { 1.0F, 0.0F, 0.0F };
            break;
         case 1:
            compWeights = { 0.0F, 1.0F, 0.0F };
            break;
         case 2:
            compWeights = { 0.0F, 0.0F, 1.0F };
            break;
         default:
            break;
      }

      for (int i = 0; i < (256 * 3); ++i)
      {
         _weightedLut8[i] = _currentDisplayLut8[i] * compWeights[i / 256];
      }
   }

   // careful - (height, width)
   _BaseFilteredImage.nullify();
   _NextFilteredImage.nullify();

   // DEBUGGING!
//   GetSystemAPI()->enterPreviewHackMode();

   // Sort the tracking points into the order they need to be processed
   _AnchorList = _trackingArray->getTrackingBoxTags(); // was CreateAnchorTree();

   // Allocate per-point data
   _TrackingBoxSubimagesArray.clear();

   // Boxes must be bigger because we need a point to perform a bilinear interpolation
   nTmpX = 2 * (_TrackBoxExtentX + 1) + 1;
   nTmpY = 2 * (_TrackBoxExtentY + 1) + 1;
   for (auto &trackingBoxTag : _AnchorList)
   {
      _TrackingBoxSubimagesArray[trackingBoxTag]._TrackBoxData = MonoArrayType(nTmpY, nTmpX);
   }

   // Boxes must be bigger because we need point to perform a bilinear interpolation
   nTmpX = (_SearchAreaLeftExtent + 1) + (_SearchAreaRightExtent + 1) + 1;
   nTmpY = (_SearchAreaTopExtent + 1) + (_SearchAreaBottomExtent + 1) + 1;
   for (auto &subimagesEntry : _TrackingBoxSubimagesArray)
   {
      subimagesEntry.second._SearchAreaData = MonoArrayType(nTmpY, nTmpX);
   }

   IToolProgressMonitor *progressMonitor = GetToolProgressMonitor();
   if (progressMonitor != NULL)
   {
     progressMonitor->StartProgress();
   }

   HRT.Start();   // Start the timer
   //_trackingToolObject->setStatusMessage("Tracking...");

   // Success, continue
   return retVal;
}
///////////////////////////////////////////////////////////////////////////////

int CTrackingProc::BeginIteration(SToolProcessingData &procData)
{
   int frameIndexList[2];
	bool isLastPass = (procData.iteration == (_iterationCount - 1));
   int numberOfInputFrames = (isLastPass? 1 : 2);

   // Track from current frame to next frame, then show CURRENT frame

   frameIndexList[0] = _inFrameIndex + procData.iteration;
   frameIndexList[1] = _inFrameIndex + procData.iteration + 1;
   SetInputFrames(0, numberOfInputFrames, frameIndexList);
   SetOutputFrames(0, 1, frameIndexList); // output first frame only
	// No release because modify-in-place is ON

   return 0;
}
///////////////////////////////////////////////////////////////////////////////

int CTrackingProc::DoProcess(SToolProcessingData &procData)
{
   CAutoErrorReporter autoErr("CTrackingProc::DoProcess",
      AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   try
	{
		bool isFirstPass = procData.iteration == 0;
		bool isLastPass = procData.iteration == (_iterationCount - 1);

		if (!isLastPass)
		{
			CToolFrameBuffer *inToolFrameBufferB = GetRequestedInputFrame(0, 0);
			CToolFrameBuffer *inToolFrameBufferN = GetRequestedInputFrame(0, 1);
         auto baseRgbImageData = inToolFrameBufferB->GetVisibleFrameBufferPtr();
         auto nextRgbImageData = inToolFrameBufferN->GetVisibleFrameBufferPtr();

			if (baseRgbImageData == nullptr || nextRgbImageData == nullptr)
			{
				autoErr.errorCode = TOOL_ERROR_FRAME_IS_MISSING;
				autoErr.msg << "Tracking aborted due to missing frame   ";
				return TOOL_ERROR_FRAME_IS_MISSING;
			}

         Ipp16uArray baseRgbImageArray(_nPicRow, _nPicCol, 3, baseRgbImageData);
         Ipp16uArray nextRgbImageArray(_nPicRow, _nPicCol, 3, nextRgbImageData);
			int retVal = TrackNextFrame(inToolFrameBufferB->GetFrameIndex(), baseRgbImageArray, nextRgbImageArray);
			if (retVal && isFirstPass)
			{
				TRACE_0(errout << "Warning: one or more tracking boxes were invalid and were ignored by TrackNextFrame()");
			}
		}
	}
   catch (const std::exception &ex)
   {
      TRACE_0(errout << "TrackNextFrame failed " << ex.what());
      return  (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_ERROR);
   }
   catch (...)
   {
		TRACE_0(errout << "Unknown error in TrackNextFrame");
      return  (TOOL_RETURN_CODE_EVENT_CONSUMED | TOOL_RETURN_CODE_ERROR);
   }

   return 0;
}
///////////////////////////////////////////////////////////////////////////////

int CTrackingProc::EndIteration(SToolProcessingData &procData)
{
   // Update the status
   IToolProgressMonitor *progressMonitor = GetToolProgressMonitor();
   if (progressMonitor != NULL)
   {
     progressMonitor->BumpProgress();
   }

   return 0;
}
///////////////////////////////////////////////////////////////////////////////

int CTrackingProc::EndProcessing(SToolProcessingData &procData)
{
   // DEBUGGING!
//   GetSystemAPI()->exitPreviewHackMode();

   _BaseFilteredImage.nullify();
   _NextFilteredImage.nullify();

   _TrackingBoxSubimagesArray.clear();

   // Did tracking run to completion or was it aborted?
	if (procData.iteration == _iterationCount)
   {
		_trkPtsEditor->notifyTrackingComplete();
	}
	else
	{
		_trkPtsEditor->notifyTrackingStopped();
	}

   IToolProgressMonitor *progressMonitor = GetToolProgressMonitor();
   if (progressMonitor != NULL)
   {
//      progressMonitor->SetStatusMessage("Tracking complete");
		progressMonitor->StopProgress(procData.iteration == _iterationCount);
   }

   return 0;
}
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// Tracking routines, should be moved to own algorithm
//
//
FPOINT CTrackingProc::FindBestMatch(FPOINT fp) const
{
   FPOINT tpR;
   int tbex = _TrackBoxExtentX;
   int tbey = _TrackBoxExtentY;
   int sbel = _SearchAreaLeftExtent;
   int sber = _SearchAreaRightExtent;
   int sbet = _SearchAreaTopExtent;
   int sbeb = _SearchAreaBottomExtent;

   // Tracking box ROI
   int trackerWidth = (tbex * 2) + 1;
   int trackerHeight = (tbey * 2) + 1;
   MtiRect trackingBoxRoi(fp.x - tbex, fp.y - tbey, trackerWidth, trackerHeight);
   auto trackBoxArray = _BaseFilteredImage(trackingBoxRoi);

   // The largest this can be is
   double minL1 = _TrackBoxSize.width * _TrackBoxSize.height * 65536.0;

   float originX = (sbel + (sber - trackerWidth) + 1) / 2.F;
   float originY = (sbet + (sbeb - trackerHeight) + 1) / 2.F;
   float bestMatchDistance = max<int>(sbet*sbet + sbel*sbel, sbeb*sbeb + sber*sber); // larger than max

   for (int searchOffsetY = -sbet; searchOffsetY <= sbeb - trackerHeight; ++searchOffsetY)
   {
      for (int searchOffsetX = -sbel; searchOffsetX <= sber - trackerWidth; ++searchOffsetX)
      {
         // searchX and searchY hold the precise tracking position (float)
         // which is NOT the same as the center of the best tracking box
         // position (integer)
         float searchX = fp.x + searchOffsetX;
         float searchY = fp.y + searchOffsetY;
         MtiRect searchBoxRoi(int(searchX), int(searchY), trackerWidth, trackerHeight);
         auto searchBoxArray = _NextFilteredImage(searchBoxRoi);

         double L1;
         trackBoxArray.L1NormDiff(searchBoxArray, &L1);

         // No need to sqrt this, just need relative magnitude of distance from
         // the center of the trackib box position (0, 0)
         float searchBoxCenterOffsetX = searchOffsetX + tbex;
         float searchBoxCenterOffsetY = searchOffsetY + tbey;
         float distance = (searchBoxCenterOffsetX * searchBoxCenterOffsetX)
                        + (searchBoxCenterOffsetY * searchBoxCenterOffsetY);
         bool foundNewMin = (L1 < minL1) || (L1 == minL1 && distance < bestMatchDistance);

         if (foundNewMin)
         {
            minL1 = L1;
            bestMatchDistance = distance;

            // Record current best position.
            float x = searchX + tbex;
            float y = searchY + tbey;
            tpR = FPOINT(x, y);
         }
      }
   }

   tpR = FindBestRefinedMatch(REFINED_MATCH_SCALE, fp, tpR, true);

   return tpR;
}

FPOINT CTrackingProc::FindBestRefinedMatch(int SCALE, FPOINT dpB, FPOINT dpN, bool doParabolicRefinement) const
{
   FPOINT tpR(dpB);
   MtiRect trackBoxRoi =
   {
      int(dpB.x) - _TrackBoxExtentX,
      int(dpB.y) - _TrackBoxExtentY,
      (_TrackBoxExtentX * 2) + 1,
      (_TrackBoxExtentY * 2) + 1
   };

   IppiSize scaledTrackBoxSize = { trackBoxRoi.width * SCALE, trackBoxRoi.height * SCALE };
   auto scaledTrackBoxArray = _BaseFilteredImage(trackBoxRoi).resize(scaledTrackBoxSize);

   // Search are includes one extra pixel on each side (We're looking for a subpixel offset here)
   MtiRect searchAreaRoi =
   {
      int(dpN.x) - _TrackBoxExtentX - 1,
      int(dpN.y) - _TrackBoxExtentY - 1,
      trackBoxRoi.width + 2,
      trackBoxRoi.height + 2
   };

   IppiSize scaledSearchAreaSize = { searchAreaRoi.width * SCALE, searchAreaRoi.height * SCALE };
   auto scaledSearchAreaArray = _NextFilteredImage(searchAreaRoi).resize(scaledSearchAreaSize);

   int bestL1X = 0;
   int bestL1Y = 0;

   int searchAreaBoxExtentX = _TrackBoxExtentX + 1;
   int searchAreaBoxExtentY = _TrackBoxExtentY + 1;
   double DL1[(REFINED_MATCH_SCALE * 2 + 1) * (REFINED_MATCH_SCALE * 2 + 1)];

   // NOTE: This is not exactly the right way to search since we are searching around the
   // center pixel and not the min position.  However, let us assume that not much
   // differs
   double minL1 = 65536.0 * (SCALE * 2 * searchAreaBoxExtentX) * (SCALE * 2 * searchAreaBoxExtentY);
   for (int searchOffsetY = -SCALE; searchOffsetY <= SCALE; ++searchOffsetY)
   {
      for (int searchOffsetX = -SCALE; searchOffsetX <= SCALE; ++searchOffsetX)
      {
         MtiRect scaledSearchBoxRoi =
         {
            searchOffsetX + SCALE,
            searchOffsetY + SCALE,
            scaledTrackBoxSize.width,
            scaledTrackBoxSize.height
         };

         double L1;
         scaledTrackBoxArray.L1NormDiff(scaledSearchAreaArray(scaledSearchBoxRoi), &L1);
         if (L1 < minL1)
         {
            minL1 = L1;
            bestL1X = searchOffsetX;
            bestL1Y = searchOffsetY;
         }

         // Record all L1's for parabolic refinement.
         DL1[(searchOffsetY + SCALE) * (SCALE * 2 + 1) + searchOffsetX + SCALE] = L1;
      }
   }

   double refinedDeltaX = 0.0;
   double refinedDeltaY = 0.0;

   // If best x or y puts us at the edge of the search area, don't do
   // parabolic refinement because we'd go off the edge.
   if (bestL1X == -SCALE || bestL1X == SCALE || bestL1Y == -SCALE || bestL1Y == SCALE)
   {
      doParabolicRefinement = false;
   }

   if (doParabolicRefinement)
   {
      double D[9];
      for (int i = -1; i <= 1; i++)
      {
         for (int j = -1; j <= 1; j++)
         {
            D[(j + 1) * 3 + i + 1] = DL1[(bestL1Y + SCALE + j) * (SCALE * 2 + 1) + bestL1X + SCALE + i];
         }
      }

      EstimateMinFrom9Points(D, refinedDeltaX, refinedDeltaY);

      // QQQ I don't understand why this is clamped to the range -1.5 to 1.5
      // since I imagined it should be -1.0 to 1.0. i.e the minimum should
      // always be found inside the bounds of the 8 surrounding pixels.
      // This seems to imply that the coordinates correspond to the center of
      // the pixel and so the true minimum could be in the part of the surround
      // pixel that is not inside the bounds. Anyhow this probably doesn't
      // matter much, it's basically just a sanity check I guess. But maybe
      // weird shit happens when some of the L1's are equal?
      MTIassert(refinedDeltaX > -1.0 && refinedDeltaX < 1.0);
      MTIassert(refinedDeltaY > -1.0 && refinedDeltaY < 1.0);
      refinedDeltaX = std::min<double>(std::max<double>(-1.5, refinedDeltaX), 1.5);
      refinedDeltaY = std::min<double>(std::max<double>(-1.5, refinedDeltaY), 1.5);
   }

   // Apply the fine and finer adjustments to the coarse one.
   float x = dpN.x + ((bestL1X + refinedDeltaX) / (double) SCALE);
   float y = dpN.y + ((bestL1Y + refinedDeltaY) / (double) SCALE);
   tpR = FPOINT(x, y);

   return tpR;
}

namespace
{
   Ipp8uArray Make8BitMonochromeImage(MTI_UINT16 *in, int nCol, int nRow, int nBits)
   {
      Ipp8uArray out(nRow, nCol);
      int i = 0;
      int shift = nBits - 8;
      for (auto outIter = out.begin(); outIter != out.end(); ++outIter)
      {
         *outIter = (Ipp8u)((unsigned short)((0.32 * in[i + 0]) + (0.64 * in[i + 1]) + (0.04 * in[i + 2])) >> shift);
         i += 3;
      }

      return out;
   }

   Ipp8uArray Make8BitMonochromeImageUsingLut(MTI_UINT16 *in, int nCol, int nRow, int nBits, MTI_UINT8 lut[])
   {
      Ipp8uArray out(nRow, nCol);
      MTI_UINT8 *rLut = lut;
      MTI_UINT8 *gLut = lut + 256;
      MTI_UINT8 *bLut = lut + (256 * 2);
      int i = 0;
      int shift = nBits - 8;
      for (auto outIter = out.begin(); outIter != out.end(); ++outIter)
      {
         // Weights are baked in!
         Ipp8u y = Ipp8u(rLut[in[i + 0] >> shift] + gLut[in[i + 1] >> shift] + bLut[in[i + 2] >> shift]);
         *outIter = y;
         i += 3;
      }

      return out;
   }

   void Make8BitMonochrome(Ipp16uArray inArray, Ipp8uArray outArray, IppiRect roi, int nBits)
   {
      auto fuckingInArray = inArray;
      auto fuckingOutAray = outArray;

      MTIassert(inArray.getComponents() == 3);
      MTIassert(outArray.getComponents() == 1);
      MTIassert(inArray.getRows() == outArray.getRows() && inArray.getCols() == outArray.getCols());

      Ipp16uArray inArrayWithRoi = inArray(roi);
      Ipp8uArray outArrayWithRoi = outArray(roi);

      int i = 0;
      int shift = nBits - 8;
      auto inIter = inArrayWithRoi.begin();
      auto outIter = outArrayWithRoi.begin();
      while (outIter != outArrayWithRoi.end())
      {
         Ipp16u r = *(inIter++);
         Ipp16u g = *(inIter++);
         Ipp16u b = *(inIter++);
         Ipp8u y = Ipp8u(Ipp16u((0.32 * r) + (0.64 * g) + (0.04 * b)) >> shift);
         *outIter++ = y;
      }
   }

   void Make8BitMonochromeUsingLut(Ipp16uArray inArray, Ipp8uArray outArray, IppiRect roi, int nBits, MTI_UINT8 lut[])
   {
      auto fuckingInArray = inArray;
      auto fuckingOutAray = outArray;

      MTIassert(inArray.getComponents() == 3);
      MTIassert(outArray.getComponents() == 1);
      MTIassert(inArray.getRows() == outArray.getRows() && inArray.getCols() == outArray.getCols());

      Ipp16uArray inArrayWithRoi(inArray(roi));
      Ipp8uArray outArrayWithRoi(outArray(roi));
      MTI_UINT8 *rLut = lut;
      MTI_UINT8 *gLut = lut + 256;
      MTI_UINT8 *bLut = lut + (256 * 2);
      int i = 0;
      int shift = nBits - 8;
      auto inIter = inArrayWithRoi.begin();
      auto outIter = outArrayWithRoi.begin();
      while (outIter != outArrayWithRoi.end())
      {
         Ipp16u r = *inIter++;
         Ipp16u g = *inIter++;
         Ipp16u b = *inIter++;
         Ipp8u y = Ipp8u(rLut[r >> shift] + gLut[g >> shift] + bLut[b >> shift]);
         *outIter++ = y;
      }
   }

#if 0  // replaced with IppiBilateralFilter
#define SURFACE_BLUR_RADIUS 3
#define SURFACE_BLUR_THRESHOLD 0.1
   void SurfaceBlur(MTI_UINT16 *image, int width, int height, int depth, MTI_UINT16 *out)
   {
      int threshold = max<int>(1, int((1 << depth) * SURFACE_BLUR_THRESHOLD));
      for (int y = SURFACE_BLUR_RADIUS; y < (height - SURFACE_BLUR_RADIUS); ++y)
      {
         for (int x = SURFACE_BLUR_RADIUS; x < (width - SURFACE_BLUR_RADIUS); ++x)
         {
            int xy = (y * width) + x;

            int radius = 1;
            int count = 9;
            int sum = 0;
            for (int yOffset = -(width * radius);
                  yOffset <= (width * radius);
                  yOffset += width)
            {
               for (int xOffset = -radius; xOffset <= radius; ++xOffset)
               {
                  sum += image[xy + yOffset + xOffset];
               }
            }

            // use regression?
            int centerValue = sum / count;

            // Surface blur - only use surround pixels that are different by less
            // than the threshold.
            sum = 0;
            count = 0;
            radius = SURFACE_BLUR_RADIUS;
            for (int yOffset = -(width * radius);
                  yOffset <= (width * radius);
                  yOffset += width)
            {
               for (int xOffset = -radius; xOffset <= radius; ++xOffset)
               {
                  int value = image[xy + yOffset + xOffset];
                  int diff = centerValue - value;
                  diff = (diff < 0) ? -diff : diff;
                  if (diff <= threshold)
                  {
                     sum += value;
                     ++count;
                  }
               }
            }

            out[xy] = (count > 0) ? (sum / count) : image[xy];
         }
      }
   }
#endif
}
//---------------------------------------------------------------------------

int CTrackingProc::FilterImage(int currentFrame, Ipp16uArray fullRgbImageArray, Ipp8uArray filteredImageArray)
{
   auto fuckingInArray = fullRgbImageArray;
   auto fuckingOutAray = filteredImageArray;

   MTI_UINT8* lutPtr = _trackingArray->getApplyLutFlag() ? _weightedLut8 : nullptr;
   if (!_debugVisualizationArray.isNull())
   {
      filteredImageArray.set({128}); //.zero();
   }

   for (auto &trackingBoxTag : _trackingArray->getTrackingBoxTags())
   {
      MTIassert(_trackingArray->isValidTrackingBoxTag(trackingBoxTag));
      if (!_trackingArray->isValidTrackingBoxTag(trackingBoxTag))
      {
         return TOOL_ERROR_INVALID_TRACKING_BOX_TAG;
      }

      TrackingBox &trackingBox = _trackingArray->getTrackingBoxByTag(trackingBoxTag);

      if (!trackingBox.isValidAtFrame(currentFrame))
      {
         return TOOL_ERROR_CORRUPT_TRACKING_BOX;
      }

      auto currentFrameTrackingPosition = trackingBox.getTrackingPositionAtFrame(currentFrame);
      auto currentFramePoint = currentFrameTrackingPosition.out;

      MtiShape searchAreaPaddedShape =
      {
         _SearchAreaSize.width + 2 * MAX_FILTER_RADIUS,
         _SearchAreaSize.height + 2 * MAX_FILTER_RADIUS,
         1
      };

      MtiRect paddedSearchAreaRoi =
      {
         int(currentFramePoint.x) - _SearchAreaLeftExtent - MAX_FILTER_RADIUS,
         int(currentFramePoint.y) - _SearchAreaTopExtent - MAX_FILTER_RADIUS,
         searchAreaPaddedShape.width,
         searchAreaPaddedShape.height
      };

      // HANDLE CLIPPED SEARCH AREA PROPERLY QQQ TODO

      MtiRect fullRoi = filteredImageArray.getRoi();
      MtiRect clippedSearchAreaRoi = // paddedSearchAreaRoi && _NextMonoImage.getRoi();
      {
         max<int>(fullRoi.x, paddedSearchAreaRoi.x),
         max<int>(fullRoi.y, paddedSearchAreaRoi.y),
         min<int>(fullRoi.width, paddedSearchAreaRoi.width),
         min<int>(fullRoi.height, paddedSearchAreaRoi.height),
      };

      // DEBUGGING
      //clippedSearchAreaRoi = filteredImageArray.getRoi();
      // DEBUGGING

      if (lutPtr)
      {
         Make8BitMonochromeUsingLut(fullRgbImageArray, filteredImageArray, clippedSearchAreaRoi, _sourceDepth, lutPtr);
      }
      else
      {
         Make8BitMonochrome(fullRgbImageArray, filteredImageArray, clippedSearchAreaRoi, _sourceDepth);
      }

      // DEBUGGING
      //clippedSearchAreaRoi.width /= 2;
      // DEBUGGING

      if (_trackingArray->getDegrainFlag() || _trackingArray->getAutoContrastFlag())
      {

         Ipp8uArray tempArray(paddedSearchAreaRoi.height, paddedSearchAreaRoi.width);

         tempArray <<= _trackingArray->getDegrainFlag()
                        ? filteredImageArray(clippedSearchAreaRoi).applyBilateralFilter(5)
                        : filteredImageArray(clippedSearchAreaRoi);

         filteredImageArray(clippedSearchAreaRoi)
                   <<= _trackingArray->getAutoContrastFlag()
                        ? tempArray.equalizeHistogram()
                        : tempArray;
      }
   }

   return 0;
}
//---------------------------------------------------------------------------

int CTrackingProc::TrackNextFrame(int currentFrame, Ipp16uArray baseRgbImageArray, Ipp16uArray nextRgbImageArray)
{
   auto localFuckingBaseArray = baseRgbImageArray;
   auto localFuckingNextArray = nextRgbImageArray;

   int retVal;
   int channel = _trackingArray->getChannel();
   // MTIassert(channel >= -1 && channel <= 2);    // WHY IS CHANNEL 3 AFTER LOAD QQQ
   if (channel < -1 || channel > 2)
   {
      channel = -1;
   }

   MTI_UINT8* lutPtr = _trackingArray->getApplyLutFlag() ? _weightedLut8 : nullptr;

   // If this is the first iteration, need to filter the first image.
   if (_NextFilteredImage.isNull())
   {
      MtiShape shape {_nPicCol, _nPicRow, 1};
      _BaseFilteredImage.createStorageIfNull(shape);
      _NextFilteredImage.createStorageIfNull(shape);
      retVal = FilterImage(currentFrame, baseRgbImageArray, _NextFilteredImage);
      MTIassert(retVal == 0);
   }

   // Old "next" image becomes the base. This works because all the tracking
   // boxes are contained in the search areas from the previous frame!
   // Ping pong is completed by using the old base as the next next.
   auto tempArray = _BaseFilteredImage;
   _BaseFilteredImage = _NextFilteredImage;
   _NextFilteredImage = tempArray;

   // Copy the filtered frame into the visualization array if requested.
   if (!_debugVisualizationArray.isNull())
   {
      CAutoSpinLocker lock(*_debugVisualizationLock);
      _debugVisualizationArray <<= _BaseFilteredImage;
   }

   // Filter the search areas for the next frame.
   retVal = FilterImage(currentFrame, nextRgbImageArray, _NextFilteredImage);
   MTIassert(retVal == 0);

   for (auto tag : _AnchorList)
   {
      int error = TrackOnePointInNextFrame(currentFrame, tag);
      if (error && !retVal)
      {
         retVal = error;
      }
   }

   return retVal;
}
//---------------------------------------------------------------------------

int CTrackingProc::TrackOnePointInNextFrame(int currentFrame, int tag)
{
//   CAutoSpinLocker lock(TrackingThreadLock);

   auto nextFrame = currentFrame + 1;

   TRACE_3(errout << "BEFORE:" << endl << _trackingArray->toString());

	MTIassert(_trackingArray->isValidTrackingBoxTag(tag));
	if (!_trackingArray->isValidTrackingBoxTag(tag))
	{
		////throw std::runtime_error("Tried to track box with invalid tag.");
		return TOOL_ERROR_INVALID_TRACKING_BOX_TAG;
	}

	TrackingBox &trackingBox = _trackingArray->getTrackingBoxByTag(tag);

	if (!trackingBox.isValidAtFrame(currentFrame))
	{
		static int previousFailTag = -1;
		if (tag != previousFailTag)
		{
			previousFailTag = tag;
			TRACE_1(errout << "CORRUPT TRACKING BOX ignored: " << trackingBox);
		}

		////throw std::runtime_error("Tried to track from an invalid starting frame.");
		return TOOL_ERROR_CORRUPT_TRACKING_BOX;
	}

   if (trackingBox.isValidAtFrame(nextFrame))
   {
      // It was already tracked!
		return 0;
   }

   CTrackingProc *nonConstThis = const_cast<CTrackingProc *>(this);
   MonoArrayType &TrackBoxData = nonConstThis->_TrackingBoxSubimagesArray[tag]._TrackBoxData;
   MonoArrayType &SearchBoxData = nonConstThis->_TrackingBoxSubimagesArray[tag]._SearchAreaData;

   auto currentFrameTrackingPosition = trackingBox.getTrackingPositionAtFrame(currentFrame);
   auto currentFramePoint = currentFrameTrackingPosition.out;
   FPOINT nextFramePoint;

   {
//      CAutoSpinUnlocker unlock(TrackingThreadLock);
      nextFramePoint = FindBestMatch(currentFramePoint);
   }

   trackingBox.addTrackPoint(nextFramePoint);

	TRACE_3(errout << "AFTER:" << endl << _trackingArray->toString());
	return 0;
}
//---------------------------------------------------------------------------
