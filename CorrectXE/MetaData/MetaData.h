/*
	File:	MetaData.h
	By:	Kevin Manbeck
	Date:	Sept 14, 2001

	This header file describes the Meta Data class.

	Meta Data Files are binary files with fixed length records.  One
	Meta Data File may hold several different types of meta data.

	The MetaData file is organized as follows:

		MD_HEADER
		MD_SECTION_HEADER #0
		MD_SECTION_SUB_HEADER (if required)
			ENTRY #0
			ENTRY #1
			 ...
		MD_SECTION_HEADER #1
		MD_SECTION_SUB_HEADER (if required)
			ENTRY #0
			ENTRY #1
			 ...
		etc.

	There must be one MD_HEADER and an arbitrary number of sections.
	The number of entries in each section is specified in the Section
	Header.

	CAUTION:  since one meta data file can contain several different
	types of data, the application programmer must be certain one file
	is not opened more than once.  If this happens, adding new sections
	to the file will likely lead to file inconsistencies.

	For this reason, it is recommended that 1 file contain only 
	closely related meta data information.
*/

#ifndef METADATAH
#define METADATAH

#include "MetaDataDLL.h"

#include <string>
#include <iostream>

#ifdef __sgi
#include <inttypes.h>
#endif // #ifdef __sgi

#include "machine.h"

using std::string;
using std::ostream;

#define VERSION_1 1

#define MAX_SECTION 500

#define TYPE_NONE 0
#define TYPE_DIFF 1
#define TYPE_PAN_SCAN_INSTRUCTION 2
#define TYPE_AUDIO_PROXY 3
#define NUMBER_TYPE 4

#define NULL_SECTION (-1)

#ifndef MTIX
  #define MTIX 1297369432   // 'M'<<24 + 'T'<<16 + 'I'<<8 + 'X'
#endif

typedef struct
 {
  MTI_UINT32
    ulMagicNumber;	// MTIX

  char
    caDescription[32];

  MTI_INT32
    lVersion,		// see VERSION_
    lNSection,
    laSectionType[MAX_SECTION];		// each section is of a different type

  MTI_INT64
    llaSectionPosition[MAX_SECTION+1];	// position in file of each section
 } MD_HEADER;

typedef struct
 {
  MTI_INT32
    lVersion,		// version of this section
    lType,		// type of this section
    lSubHeaderSize,	// size of the sub-header, if it exists
    lRecordSize,	// size of each entry in this section
    lNRecord;		// number of records in this section
 } MD_SECTION_HEADER;


/////////////////////////////////////////////////////////////////////////
// Specific types of meta data.  Add new structures here.

//  When adding a new structure, be sure to add new code in AddSection()


/////////////////////////////////////////////////////////////////////////

	// values of DiffFlag
#define DF_NONE 0x0
#define DF_AUTO_CUT 0x1	// automatically detected cut
#define DF_USER_CUT 0x2 // user-specified cut
#define DF_USER_NON_CUT 0x4  // user removed cut
#define DF_SCENE_CUT (DF_AUTO_CUT | DF_USER_CUT)
#define DF_NOT_CALC 0x8 // difference has not be calculated for this frame

#define FRAME_DIFF_NUM_ROW_BLOCK 8	// should be a multiple of 2
#define FRAME_DIFF_NUM_COL_BLOCK 8

/*
	The FrameLabel contains several types on information about a
	frame.

	1.  3:2 pull down label for 525I and 1080I.  3:2 pulldown means
	the film sequence A, B, C, D, gets changed to the video sequence
	Aa, Bb, Bc, Cd, Dd.  That is 4 frames are changed into 10 fields.

	The labels for 525I are FL_aA, FL_bB, FL_cB, FL_dC, FL_dD.
	The labels for 1080I are FL_Aa, FL_Bb, FL_Bc, FL_Cd, FL_Dd.

	2.  3:2 pull down label for 720P.  This means the film sequence
	A, B gets changed to the progressive video sequence
	A, A, B, B, B.  That is 2 film frames get changed into 5 progressive
	video frames.

	The labels for this are FL_A0, FL_A1, FL_B0, FL_B1, FL_B2

	3.  Scene cuts.  There are two types of scene cuts:  cuts specified
	by an operator.  cuts that are detected automatically.

	The labels for this are FL_USER_CUT and FL_AUTO_CUT.


*/
#define FL_NONE		0x0
#define FL_Aa		0x1	// this frame comes from 1 film frame
#define FL_Bb		0x2	// this frame comes from 1 film frame
#define FL_Bc		0x4	// this frame comes from 2 film frames
#define FL_Cd		0x8	// this frame comes from 2 film frames
#define FL_Dd		0x10	// this frame comes from 1 film frame

#define FL_A0		0x20	// 720p pulldown
#define FL_A1		0x40	// 720p pulldown
#define FL_B0		0x80	// 720p pulldown
#define FL_B1		0x100	// 720p pulldown
#define FL_B2		0x200	// 720p pulldown

#define FL_aA		0x400	// this frame comes from 1 film frame
#define FL_bB		0x800	// this frame comes from 1 film frame
#define FL_cB		0x1000	// this frame comes from 2 film frames
#define FL_dC		0x2000	// this frame comes from 2 film frames
#define FL_dD		0x4000	// this frame comes from 1 film frame


#define FL_FRAME (FL_Aa|FL_Bb|FL_Bc|FL_Cd|FL_Dd|FL_A0|FL_A1|FL_B0|FL_B1|FL_B2|FL_aA|FL_bB|FL_cB|FL_dC|FL_dD)

#define FL_32_BREAK	0x8000	// a break in the 3:2 sequence
#define FL_USER_OVERRIDE 0x10000 // user says do not fix cadence
#define FL_CUT          0x20000 // cut threshold exceeded


typedef struct
 {
/*
	faBlockAve is the average intensity in each block.
		- faBlockAve is useful for a shot detector

	faBlockDiff is the average absolute pixel-to-pixel difference
		- faBlockDiff is useful for detecting 3:2 cadence
*/

  float
   faBlockAveFrame[FRAME_DIFF_NUM_ROW_BLOCK][FRAME_DIFF_NUM_COL_BLOCK],
   faBlockDiffFrame[FRAME_DIFF_NUM_ROW_BLOCK][FRAME_DIFF_NUM_COL_BLOCK],
   faBlockAveField[2][FRAME_DIFF_NUM_ROW_BLOCK][FRAME_DIFF_NUM_COL_BLOCK],
   faBlockDiffField[2][FRAME_DIFF_NUM_ROW_BLOCK][FRAME_DIFF_NUM_COL_BLOCK];

   MTI_INT32
    lDiffFlag;	// see DF_ for values
 } FRAME_DIFF;

#define PF_NONE 0x0
#define PF_IN 0x1         // this frame had an IN point defined
#define PF_OUT 0x2	  // this frame had an OUT point defined
#define PF_DYNAMIC 0x8
#define PF_STATIC 0x10
#define PF_RENDER_COMPLETE 0x20  // this frame as already been rendered
#define PF_RENDER_IN_PROCESS 0x40  // this frame is currently being rendered
#define PF_VAS 0x80	// pan instruction from VAS protocol
#define PF_RECT_0 0x1000	// static move
#define PF_RECT_1 0x2000	// S-curve move
#define PF_RECT_2 0x4000	// linear move
#define PF_RECT_3 0x8000	// S-curve move
#define PF_RECT_4 0x10000	// S-curve move

#define PF_RECT (PF_RECT_0|PF_RECT_1|PF_RECT_2|PF_RECT_3|PF_RECT_4)

typedef struct
{
  float
    fShiftH,
    fShiftV,
    fZoomH,
    fZoomV,
    fRotate,
    fFrameAspectRatioSrc,
    fFrameAspectRatioDst;

  bool
    bFullFrameFlagSrc,
    bFullFrameFlagDst;

  MTI_INT32
    lPanFlag;  // see PF_ for values
} PAN_INSTRUCTION;

#define MAX_AUDIO_PROXY_CHANNEL 8
typedef struct
{
  MTI_INT16
    saMinimum[MAX_AUDIO_PROXY_CHANNEL],	// minimum value in frame
    saMaximum[MAX_AUDIO_PROXY_CHANNEL];	// maximum value in frame
} AUDIO_PROXY;

#define OVERLAY_AUDIO_LTC 0
#define OVERLAY_CAMERA_ROLL 1
#define OVERLAY_FEET_FRAMES 2
#define OVERLAY_KEYKODE 3
#define OVERLAY_LAYOFF_TIMECODE 4
#define OVERLAY_SCENE 5
#define OVERLAY_SCENE_TAKE 6
#define OVERLAY_SCENE_TAKE_CAMERA_ROLL 7
#define OVERLAY_TAKE 8

#define OVERLAY_NUMBER 16

typedef struct
{
  MTI_REAL32
    faPosX[OVERLAY_NUMBER],
    faPosY[OVERLAY_NUMBER],
    faSize[OVERLAY_NUMBER];

  MTI_INT32
    iaFG[OVERLAY_NUMBER],
    iaBG[OVERLAY_NUMBER];

  bool
    baVisible[OVERLAY_NUMBER];
} OVERLAY_STRUCT;

/////////////////////////////////////////////////////////////////////////
// Forward Declarations


/////////////////////////////////////////////////////////////////////////

class MTI_METADATADLL_API  CMetaData
{
public:
  CMetaData(const string &strFileNameArg);
  CMetaData(const string &strFileNameArg, bool bReadOnlyArg);
  ~CMetaData();

  int AssignFrameLabels (int iFirstFrame, int iNFrame, int *ipFrameLabel,
		int iPulldown);
  int AssignCadenceRepairLabels (int iFirstFrame, int iNFrame,
	int *ipFrameLabel, int iPulldown, float *fpCutStatistic,
		float fCutThresh);
		// ipFrameLabel[iFirstFrame] is the label for iFirstFrame

  int Open ();	// open existing file for I/O
  int Create (); // create new file (destroy old one)
  int Close ();
  int Delete ();

  int Initialize (long lType, long lNRecord, void **vpSubHeader,
		void **vpRecord);
		// open existing or create if necessary.
		// read in section or create a new section
		// return pointer to requested section

  int SelectSection (long lType);
  int AddSection (long lType, long lNRecord);

		// perform IO on records in a section
  int WriteOneRecord (long lRecord);	     // write 1 entry
  int ReadOneRecord (long lRecord);	     // read 1 entry
  int WriteRecordSet (long lStart, long lStop); // write (Stop-Start) entries
  int ReadRecordSet (long lStart, long lStop);  // read (Stop-Start) entries
  int WriteWholeSection ();  // write all records incl section header
  int ReadWholeSection ();   // read all records incl section header

  void *GetRecordPtr();
  void *GetSubHeaderPtr();

  long GetNRecord();

private:
  int WriteHeader ();
  int ReadHeader ();
  int WriteSectionHeader ();
  int ReadSectionHeader ();
  int WriteSectionSubHeader (void *vpSubHeader, long lSize);
  int ReadSectionSubHeader (void *vpSubHeader, long lSize);

  void ClearHeader();
  int Free();

  int Find32 (int iFirstFrame, int iNFrame, int *ipFrameLabel,
		int iPulldown);
  int NextLabel (int iLabelArg);
  int PreviousLabel (int iLabelArg);

  int FindCadenceLabels (int iFirstFrame, int iNFrame, int *ipFrameLabel,
		int iPulldown, float *fpCutStatistic, float fCutThresh);
  int FindCuts (int *ipFrameLabel, float *fpFieldDiff,
		int iStartFrame, int iNFrame, int iNField,
		float fThreshold, int iMedianSearchRadius, float fMinFieldDiff,
		float *fpStatRatio);
  float FindStatRatio (int iField, float *fpFieldDiff, int iNTotField,
	int iMedianSearchRadius);
  int DetermineCadenceLabels (int *ipFrameLabel, float *fpDifference,
	int iStartFrame, int iNFrame, int iNField);
  int FindConsistentRun (int *ipFrameLabel, float *fpDifference,
		int iCut0, int iCut1, int iNField);
  void AssignLabel (int *ipFrameLabel, int iFFVVF0, int iFFVVF1);

  bool
    bReadOnly,
    bMustSwap;

  MD_HEADER
    mh;

  MD_SECTION_HEADER
    msha[MAX_SECTION];

  void
    *vpSubHeader[MAX_SECTION],
    *vpSectionRecord[MAX_SECTION];

  int
    iFD;	// file descriptor

  long
    lCurrentSection;

  string
    strFileName;
};
#endif
