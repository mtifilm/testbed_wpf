//---------------------------------------------------------------------------

#ifndef AutoCleanRenderHistoryH
#define AutoCleanRenderHistoryH

#include "MetaDataDLL.h"
#include "DebrisMetadata.h"
#include "IniFile.h"
#include <set>

#define ERR_AUTOCLEAN_CANT_OPEN_DEBRIS_FILE     (-31000)
#define ERR_AUTOCLEAN_CORRUPT_DEBRIS_FILE       (-31001)
#define ERR_AUTOCLEAN_WRONG_VERSION_DEBRIS_FILE (-31002)
#define ERR_AUTOCLEAN_DEBRIS_WASNT_FIXED        (-31003)
#define ERR_AUTOCLEAN_INTERNAL_ERROR            (-31004)
#define ERR_AUTOCLEAN_CANT_WRITE_TO_DEBRIS_FILE (-31005)
#define ERR_AUTOCLEAN_NO_DEBRIS_DATA		    (-31006)
#define ERR_AUTOCLEAN_ALREADY_IN_CACHE		    (-31007)
#define ERR_AUTOCLEAN_BAD_UUID_DEBRIS_FILE    (-30008)
#define ERR_AUTOCLEAN_NOT_A_DEBRIS_FILE       (-30009)

class EDebrisFileException: public exception
{
public:
  EDebrisFileException(const string &message)
  {
  	 _message = message;
  }

  virtual const char* what() const throw()
  {
    return _message.c_str();
  }

private:
	string _message;
};

class MTI_METADATADLL_API CAutoCleanRenderHistory
{
public:
	CAutoCleanRenderHistory();
	CAutoCleanRenderHistory(const string &fixHistoryFilePath, int frameIndex);
	virtual ~CAutoCleanRenderHistory();

    void Load(const string &fixHistoryFilePath, int frameIndex);
    void Save(const string &fixHistoryFilePath = "");

	void Merge(const string &fixHistoryFilePath = "");
    void Merge(const vector<int> &fixes);

    bool IsDirty() { return _dirty; }

    void SetFrameIndex(int frameIndex) { _frameIndex = frameIndex; }
    int GetFrameIndex() { return _frameIndex; }

    const vector<int> &GetRenderedIds() const { return _debrisIds; }

    int TotalSize();

    void Clear();

private:
    vector<int> _debrisIds;
    string _fixHistoryFilePath;
    int _frameIndex;
    bool _historyFileExists = false;
    bool _dirty;
};

//---------------------------------------------------------------------------
#endif
