#ifndef JOBINFOH
#define JOBINFOH

#include "MetaDataDLL.h"

#include <string>
using std::string;

struct MTI_METADATADLL_API JobInfo
{
   string Name;
   string Nickname;
   int    Number;
   int    ReelCount;
   bool   IsASlashB;
   bool   IsCombined;
   bool   IsLastReelAOnly;
   bool   HasDewarpFolder;
   bool   HasMasksFolder;
   bool   HasPdlsFolder;
   bool   HasReportsFolder;
   bool   HasCutListsFolder;
   bool   HasStabilizeFolder;
   string MediaLocation;
   bool   HasDrsMediaFolder;
   bool   HasUserMediaFolder;
   string UserMediaFolderName;
   string JobFolder;

   JobInfo();
   JobInfo(const JobInfo &orig);
   JobInfo& operator=(const JobInfo &rhs);

   int LoadFrom(const string &newJobFolder);
   int SaveTo(const string &newJobFolder) const;

   int ValidateName() const;
   int ValidateNickname() const;
   int ValidateMediaLocation() const;
   bool ValidateAll() const;

   string GetJobRootFolderName() const;

   static bool IsAJobFolder(const string &folder);

private:

   int Save() const;
};

#define JOBINFO_ERR_NO_FOLDER_SPECIFIED     -19501
#define JOBINFO_ERR_INI_FILE_OPEN_FAIL      -19502
#define JOBINFO_ERR_INVALID_NAME            -19503
#define JOBINFO_ERR_INVALID_NICKNAME        -19504
#define JOBINFO_ERR_INVALID_MEDIA_LOCATION  -19505
#define JOBINFO_ERR_INVALID_JOB_INFO        -19506
#define JOBINFO_ERR_INI_FILE_DOES_NOT_EXIST -19507

#define JOB_INI_FILE_NAME "Job.ini"


#endif
