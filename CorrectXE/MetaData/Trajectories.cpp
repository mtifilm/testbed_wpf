
#include "Trajectories.h"
#include "IniFile.h"   // for TRACE_N()
#include "MTIstringstream.h"
#include <iomanip>
////#include <strstream>
#include <errno.h>


//#define USE_WEIGHTS


CTrajectories::CTrajectories(const string &trajFilePath,
                             int width, int height,
                             int blockScaleFactor,
                             int temporalRange)
: m_width(width)
, m_height(height)
, m_blockScaleFactor(blockScaleFactor)
, m_temporalRange(temporalRange)
, m_frameIndex(-1)
, m_trajX(NULL)
, m_trajY(NULL)
, m_weights(NULL)
, m_trajectoriesLoadedOK(false)
, m_pathIsATemplate(false)
{
   size_t firstHashPos = trajFilePath.find_first_of('#');
   if (firstHashPos == string::npos)
   {
      // No hashes - presume it's an actual file path
      m_trajFilePath = trajFilePath;
   }
   else
   {
      m_trajPathLeft = trajFilePath.substr(0, firstHashPos);
      size_t lastHashPos = trajFilePath.find_last_of('#');
      m_trajPathRight = trajFilePath.substr(lastHashPos + 1);
      m_trajPathNumDigits = lastHashPos + 1 - firstHashPos;
      m_pathIsATemplate = true;
   }
}

CTrajectories::~CTrajectories()
{
   cleanUp();
}

int CTrajectories::SetFrameIndex(int newFrameIndex)
{
   MTIassert(m_pathIsATemplate);
   
   int retVal = 0;

   if (m_frameIndex != newFrameIndex)
   {
      MTIostringstream os;
      m_frameIndex = newFrameIndex;

      os << m_trajPathLeft
         << std::setw(m_trajPathNumDigits) << std::setfill('0') << m_frameIndex
         << m_trajPathRight;

      m_trajFilePath = os.str();

      retVal = LoadTrajectories();
   }

   return retVal;
}

int CTrajectories::GetTrajectory(int temporalOffset,
                                 int x, int y,
                                 int &dx, int &dy)
{
   MTIassert(temporalOffset >= -m_temporalRange);
   MTIassert(temporalOffset <   m_temporalRange);

   if (!m_trajectoriesLoadedOK)
   {
      return TRAJ_ERR_TRAJECTORIES_NOT_LOADED;
   }

   long lTrajRow = y / m_blockScaleFactor;
   long lTrajCol = x / m_blockScaleFactor;
   long lTrajRowCol = (lTrajRow * (m_width / 2)) + lTrajCol;

   // Get a temporal offset in the range [0, temporalRange * 2 - 1]
   // Example: if temporal range is 5:
   // legal inputs are      -5,-4,-3,-2,-1, 1, 2, 3, 4, 5
   // we want (respectively) 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
   long lFrameSelectOffset = temporalOffset + m_temporalRange -
                             ((temporalOffset > 0)? 1 : 0);
   long lTrajIndex = lTrajRowCol * (m_temporalRange * 2) + lFrameSelectOffset;

   dx = m_trajX[lTrajIndex];
   dy = m_trajY[lTrajIndex];

   return 0;
}

int CTrajectories::GetAllTrajectoriesLinearized(int temporalOffset,
                                                long *trajOut,
                                                long &mostPositiveMotion,
                                                long &mostNegativeMotion)
{
   MTIassert(temporalOffset >= -m_temporalRange);
   MTIassert(temporalOffset <   m_temporalRange);

   mostPositiveMotion = 0;
   mostNegativeMotion = 0;

   if (!m_trajectoriesLoadedOK)
   {
      return TRAJ_ERR_TRAJECTORIES_NOT_LOADED;
   }

   int rawTrajOffset = temporalOffset + m_temporalRange -
                             ((temporalOffset > 0)? 1 : 0);;
   int rawTrajIncrement = m_temporalRange * 2;
   int cookedTrajOffset = 0;
   const int cookedTrajIncrement = 1;
   int cookedTrajStop = (m_width / m_blockScaleFactor) *
                        (m_height / m_blockScaleFactor);

   while (cookedTrajOffset < cookedTrajStop)
   {
      long dX = m_trajX[rawTrajOffset];
      long dY = m_trajY[rawTrajOffset];
      long dXY = (dY * m_width) + dX;

      trajOut[cookedTrajOffset] = dXY;

      if (dXY > mostPositiveMotion)
         mostPositiveMotion = dXY;
      if (dXY < mostNegativeMotion)
         mostNegativeMotion = dXY;

      rawTrajOffset += rawTrajIncrement;
      cookedTrajOffset += cookedTrajIncrement;
   }

   return 0;
}


int CTrajectories::LoadTrajectories()
{
   int retVal = 0;

   MTIassert(m_trajFilePath.empty() == false);
   if (m_trajFilePath.empty())
   {
      return TRAJ_ERROR_CANT_OPEN_TRAJ_FILE;
   }
   
   FILE *fp = fopen(m_trajFilePath.c_str(), "rb");
   if (fp == NULL)
   {
      return TRAJ_ERROR_CANT_OPEN_TRAJ_FILE;
   }

   cleanUp();
   m_trajectoriesLoadedOK = false;

   const size_t trajSize = (m_width / m_blockScaleFactor) *
                           (m_height / m_blockScaleFactor) *
                           (m_temporalRange * 2);

   m_trajX   = new short[trajSize];
   m_trajY   = new short[trajSize];
#ifdef USE_WEIGHTS
   m_weights = new float[trajSize];
#endif

   int readResult = fread(m_trajX, trajSize, sizeof(short), fp);

   if (readResult > 0)
   {
      readResult = fread(m_trajY, trajSize, sizeof(short), fp);

#ifdef USE_WEIGHTS
      if (readResult > 0)
      {
         readResult = fread(m_weights, trajSize, sizeof(float), fp);
      }
#endif
   }
   if (readResult > 0)
   {
      m_trajectoriesLoadedOK = true;
   }
   else
   {
      retVal = TRAJ_ERROR_READ_FAILED;
   }

   fclose(fp);

   if (retVal == 0)
   {
      TRACE_0(errout << ")_)_)_)_)_)_ LOADED TRAJECTORY FILE " << m_trajFilePath);
   }
   else
   {
      TRACE_0(errout << ")_)_)_)_)_)_ TRAJECTORY FILE " << m_trajFilePath
                     << " LOAD FAILED!" << endl
                     << "             Reason: " << strerror(errno));
   }

   return retVal;
}

void CTrajectories::cleanUp()
{
   delete [] m_trajX;
   m_trajX = NULL;
   delete [] m_trajY;
   m_trajY = NULL;
#ifdef USE_WEIGHTS
   delete [] m_weights;
   m_weights = NULL;
#endif
}
