/*
	Name:	MetaData.cpp
	By:	Kevin Manbeck
	Date:	Sept 14, 2001

	The Meta Data File holds an arbitrary collection of fixed
	length records.  See MetaData.h for more details.
*/

#include "MetaData.h"

#include "MTIio.h"
#include <stdio.h>
#include <errno.h>
#include <time.h>      // time_t is broken in sys/types.h in RadStudio XE4
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <string.h>

#include "ImageInfo.h"
#include "IniFile.h"  // for TRACE
#include "MTImalloc.h"
#include "err_metadata.h"

// This is a guess at something close to the max length to read or write
// to a file at once...
#define MAX_IO_LENGTH (30*1024*1024)


CMetaData::CMetaData(const string &strFileNameArg)
{
  long lSection;

  bReadOnly = false;
  bMustSwap = false;
  iFD = -1;

  ClearHeader();

  for (lSection = 0; lSection < MAX_SECTION; lSection++)
   {
    vpSubHeader[lSection] = NULL;
    vpSectionRecord[lSection] = NULL;
   }

  strFileName = strFileNameArg;
}  /* CMetaData */

CMetaData::CMetaData(const string &strFileNameArg, bool bReadOnlyArg)
{
  long lSection;
  iFD = -1;
  bReadOnly = bReadOnlyArg;

  ClearHeader();

  for (lSection = 0; lSection < MAX_SECTION; lSection++)
   {
    vpSubHeader[lSection] = NULL;
    vpSectionRecord[lSection] = NULL;
   }

  strFileName = strFileNameArg;
}  /* CMetaData */

CMetaData::~CMetaData()
{
  Free();
}  /* ~CMetaData */

int CMetaData::AssignFrameLabels (int iStartFrame, int iNFrame,
		int *ipFrameLabel, int iPulldown)
/*
	This function runs through the frames and assigns frame labels

	Use EPulldown values:

		IF_PULLDOWN_FRAME_3_2 for 720P
		IF_PULLDOWN_FIELD_3_2 for 1080I
		IF_PULLDOWN_SWAPPED_FIELD_3_2 for 525I
*/
{
  int iRet, iFrame;

	// initialize all the frame records
  for (iFrame = iStartFrame; iFrame < iStartFrame + iNFrame; iFrame++)
   {
    ipFrameLabel[iFrame] = FL_NONE;
   }

  MTIassert(iStartFrame >= 0);
  if (iStartFrame < 0)
    return ERR_METADATA_BAD_PARAM;

  iRet = Open ();
  if (iRet)
    return iRet;

  iRet = SelectSection (TYPE_DIFF);
  if (iRet)
   {
    Close ();
    return iRet;
   }

  iRet = ReadWholeSection();
  if (iRet)
   {
    Close ();
    return iRet;
   }

  iRet = Find32 (iStartFrame, iNFrame, ipFrameLabel, iPulldown);
  if (iRet)
   {
    Close ();
    return iRet;
   }

  return 0;
}  /* AssignFrameLabel */

#define NUM_MAX 10
int CMetaData::Find32 (int iStartFrame, int iNFrame, int *ipFrameLabel,
		int iPulldown)
{
  int iNField, iRow, iCol, iF, iDiffFrame, iLabel, iStopFrame, iFramePrev,
		iFrameNext, iFieldStop, iBestSmall, iSmall, iStart,
		iBestStart, iFieldStart, iField, iFrame, iMax;
  float *fpDifference, *fpDiff, fAveDiff, fMaxAveDiff, fTmp;
  float faMax[NUM_MAX];
  FRAME_DIFF *fdp;
  bool *bpMinFlag, *bpMF;
  float fSum;

/*
	Clear the labels
*/

  for (iFrame = 0; iFrame < iNFrame; iFrame++)
   {
    ipFrameLabel[iFrame + iStartFrame] = FL_NONE;
   }

/*
	In order to find the 3:2 sequence, we need to keep track of the
	largest field or frame difference at each location.
*/

  if (iPulldown == IF_PULLDOWN_FRAME_3_2)
   {
    iNField = 1;
   }
  else
   {
    iNField = 2;
   }

  fpDifference = (float *) malloc (iNFrame * iNField * sizeof(float));
  if (fpDifference == NULL)
    return ERR_METADATA_OUT_OF_MEMORY;

  bpMinFlag = (bool *) malloc (iNFrame * iNField * sizeof(bool));
  if (bpMinFlag == NULL)
   {
    free (fpDifference);
    return ERR_METADATA_OUT_OF_MEMORY;
   }

  fdp = (FRAME_DIFF *)GetRecordPtr() + iStartFrame;
  fpDiff = fpDifference;
  for (iFrame = iStartFrame; iFrame < iStartFrame + iNFrame &&
		iFrame < GetNRecord(); iFrame++)
   {
/*
	We classify the 3:2 sequence by examining smallest difference
	values.  In perfect 3:2 material the difference is zero between
	some of the fields.

	Static material can give zero differences in some of the regions,
	so we keep track of the maximum difference observed.

	Basing decisions on a single region can be a little noisy,
	so we take the average of the largest two differences.
*/

    if (iPulldown == IF_PULLDOWN_FRAME_3_2)
     {
      for (iMax = 0; iMax < NUM_MAX; iMax++)
       {
        faMax[iMax] = -1.;
       }
      iField = 0;
      for (iRow = 0; iRow < FRAME_DIFF_NUM_ROW_BLOCK; iRow++)
      for (iCol = 0; iCol < FRAME_DIFF_NUM_COL_BLOCK; iCol++)
       {
        for (iMax = 0; iMax < NUM_MAX; iMax++)
         {
          if (fdp->faBlockDiffField[iField][iRow][iCol] > faMax[iMax])
           {
            for (int i = NUM_MAX-1; i > iMax; i--)
             {
              faMax[i] = faMax[i-1];
             }

            faMax[iMax] = fdp->faBlockDiffField[iField][iRow][iCol];

            break;	// out of for loop
           }
         }
       }
		// faMax[0-2] is often due to time code overlay, so do not use
      fSum = 0;
      for (int i = 3; i < NUM_MAX; i++)
       {
        fSum += faMax[i];
       }
      fpDiff[iField] = fSum / (float)(NUM_MAX-1);
     }
    else
     {
      for (iField = 0; iField < iNField; iField++)
       {
        for (iMax = 0; iMax < NUM_MAX; iMax++)
         {
          faMax[iMax] = -1.;
         }
        for (iRow = 0; iRow < FRAME_DIFF_NUM_ROW_BLOCK; iRow++)
        for (iCol = 0; iCol < FRAME_DIFF_NUM_COL_BLOCK; iCol++)
         {
          for (iMax = 0; iMax < NUM_MAX; iMax++)
           {
            if (fdp->faBlockDiffField[iField][iRow][iCol] > faMax[iMax])
             {
              for (int i = NUM_MAX-1; i > iMax; i--)
               {
                faMax[i] = faMax[i-1];
               }

              faMax[iMax] = fdp->faBlockDiffField[iField][iRow][iCol];

              break;	// out of for loop
             }
           }
         }
		// faMax[0] and faMax[1] are often due to time code overlay,
		// so do not use
        fSum = 0;
        for (int i = 2; i < NUM_MAX; i++)
         {
          fSum += faMax[i];
         }
        fpDiff[iField] = fSum / (float)(NUM_MAX-1);
       }
     }

    fpDiff += iNField;		// move to next frame
    fdp++;
   }

/*
	In the 720P case, the 3:2 pulldown is of the form:

			AABBBAABBBAABBB

	so the difference values are

			lslsslslsslslss

	where l=large difference and s=small difference

	In the 1080I case, the 3:2 pulldown is of the form:

			AaBbBcCdDdAaBbBcCdDd

	so the difference values are

			llllsllllsllllslllls

	In the 525I case, the 3:2 pulldown is of the form:

			aAbBcBdCdDaAbBcBdCdD

	so the difference values are
			lllllsllsllllllslls

	**NOTE:  the reason 525I does not follow the standard 3:2 pattern
	is that our code rearranges the 525I material so field 1 appears
	first.  This has the advantage of making display and processing
	simpler at the expense of 3:2 detection and removal.

	To make this computation simpler, we temporarily swap the
	values of 525I material to make the pattern llllsllllslllls

	Look for either lsllslsllslslls  OR llllsllllsllllslllls
*/

  fpDiff = fpDifference;
  bpMF = bpMinFlag;

  if (iPulldown == IF_PULLDOWN_SWAPPED_FIELD_3_2)
   {
		// swap values of fpDifference to make computation simpler
    for (iFrame = 0; iFrame < iNFrame; iFrame++)
     {
      fTmp = fpDiff[2*iFrame];
      fpDiff[2*iFrame] = fpDiff[2*iFrame+1];
      fpDiff[2*iFrame+1] = fTmp;
     }
   }

  switch (iPulldown)
   {
    case IF_PULLDOWN_FRAME_3_2:
      for (iFrame = 0; iFrame < iNFrame; iFrame++)
       {
        *bpMF = true;
        if (iFrame-4 >= 0)
         {
          if (
		(fpDiff[0] > fpDiff[-4]) ||
		(fpDiff[-1] > fpDiff[-4]) ||
		(fpDiff[-3] > fpDiff[-4]) ||
		(fpDiff[0] > fpDiff[-2]) ||
		(fpDiff[-1] > fpDiff[-2]) ||
		(fpDiff[-3] > fpDiff[-2])
              )
           {
            *bpMF = false;
           }
         }

        if (iFrame+4 < iNFrame)
         {
          if (
		(fpDiff[0] > fpDiff[1]) ||
		(fpDiff[2] > fpDiff[1]) ||
		(fpDiff[4] > fpDiff[1]) ||
		(fpDiff[0] > fpDiff[3]) ||
		(fpDiff[2] > fpDiff[3]) ||
		(fpDiff[4] > fpDiff[3])
             )
           {
            *bpMF = false;
           }
         }
        bpMF++;
        fpDiff++;
       }
    break;
    case IF_PULLDOWN_FIELD_3_2:		// case of 1080I
    case IF_PULLDOWN_SWAPPED_FIELD_3_2:		// case of 525I
      for (iField = 0; iField < iNField * iNFrame; iField++)
       {
        *bpMF = true;
        if (iField-4 >= 0)
         {
          if (
		fpDiff[0] > fpDiff[-4] ||
		fpDiff[0] > fpDiff[-3] ||
		fpDiff[0] > fpDiff[-2] ||
		fpDiff[0] > fpDiff[-1]
             )
           {
            *bpMF = false;
           }
         }
        if (iField+4 < iNField * iNFrame)
         {
          if (
		fpDiff[0] > fpDiff[1] ||
		fpDiff[0] > fpDiff[2] ||
		fpDiff[0] > fpDiff[3] ||
		fpDiff[0] > fpDiff[4]
             )
           {
            *bpMF = false;
           }
         }
        bpMF++;
        fpDiff++;
       }
    break;
    default:
      free (fpDifference);
      free (bpMinFlag);
      return ERR_METADATA_INTERNAL_ERROR;
   }

/*
	The pattern in bpMinFlag should be
		FFFFTFFFFTFFFFTFFFFT.

	In black areas, we will see TTTTTTTTTTTTT.

	So find the longest pattern.
*/

  iField = 0;
  while (iField < iNFrame * iNField)
   {
    iFieldStart = iField;
    while ((iFieldStart < iNFrame * iNField) &&
		(bpMinFlag[iFieldStart] == false))
     {
      iFieldStart++;
     }

    iBestSmall = 0;
    iBestStart = 0;
    for (iStart = 0; iStart < 5; iStart++)
     {
      iF = iFieldStart + iStart;
      iSmall = 0;
      while ((iF < iNFrame * iNField) && (bpMinFlag[iF] == true))
       {
        iF += 5;
        iSmall++;
       }

      if (iSmall > iBestSmall)
       {
        iBestSmall = iSmall;
        iBestStart = iStart;
       }
     }

    iFieldStart += iBestStart;
    iFieldStop = iFieldStart + 5 * (iBestSmall-1) + 1;

    if (iBestSmall == 0)
      iFieldStop = iFieldStart;

/*
	Now that we have found the best run, assign frame labels.
*/

    switch (iPulldown)
     {
      case IF_PULLDOWN_FRAME_3_2:	// 720P case
       {
        iLabel = FL_B2;

        iStopFrame = iFieldStop + iStartFrame;
        if (iStopFrame > GetNRecord())
          iStopFrame = GetNRecord();

        iFrame = iFieldStart + iStartFrame;
        for (; iFrame < iStopFrame; iFrame++)
         {
          ipFrameLabel[iFrame] = iLabel;
          iLabel = NextLabel (iLabel);
         }
       }
      break;
      case IF_PULLDOWN_FIELD_3_2:	// 1080I case
        if (iFieldStart & 0x1)
          iLabel = FL_Dd;
        else
          iLabel = FL_Bc;

        iStopFrame = iFieldStop/2;
        if (iStopFrame*2 < iFieldStop)
          iStopFrame++;

        iStopFrame += iStartFrame;
        if (iStopFrame > GetNRecord())
          iStopFrame = GetNRecord();

        iFrame = iFieldStart/2 + iStartFrame;
        for (; iFrame < iStopFrame; iFrame++)
         {
          ipFrameLabel[iFrame] = iLabel;
          iLabel = NextLabel (iLabel);
         }
      break;
      case IF_PULLDOWN_SWAPPED_FIELD_3_2:	// 525I case
        if (iFieldStart & 0x1)
          iLabel = FL_dD;
        else
          iLabel = FL_cB;

        iStopFrame = iFieldStop/2;
        if (iStopFrame*2 < iFieldStop)
          iStopFrame++;

        iStopFrame += iStartFrame;
        if (iStopFrame > GetNRecord())
          iStopFrame = GetNRecord();

        iFrame = iFieldStart/2 + iStartFrame;
        for (; iFrame < iStopFrame; iFrame++)
         {
          ipFrameLabel[iFrame] = iLabel;
          iLabel = NextLabel (iLabel);
         }
      break;
      default:
        free (fpDifference);
        free (bpMinFlag);
        return ERR_METADATA_INTERNAL_ERROR;
     }
    iField = (iStopFrame-iStartFrame) * iNField;
   }

/*
	At this stage, we have labelled all the unambiguous fields.
	Now run through the remaining fields and fill in the FL_NONE
	areas.
*/

  iFrame = 0;
  while (iFrame < iNFrame)
   {
	// find the first FL_NONE in a run
    while (iFrame < iNFrame && ipFrameLabel[iFrame+iStartFrame] != FL_NONE)
     {
      iFrame++;
     }
    iFramePrev = iFrame;

	// find the last FL_NONE in the run
    while (iFrame < iNFrame && ipFrameLabel[iFrame+iStartFrame] == FL_NONE)
     {
      iFrame++;
     }
    iFrameNext = iFrame;

    if (iFramePrev == 0 && iFrameNext == iNFrame)
     {
		// no pattern was found.  supply one arbitrarily
      switch (iPulldown)
       {
        case IF_PULLDOWN_FRAME_3_2:		// 720P
          iLabel = FL_A0;
        break;
        case IF_PULLDOWN_FIELD_3_2:		// 1080I
          iLabel = FL_Aa;
        break;
        case IF_PULLDOWN_SWAPPED_FIELD_3_2:		// 525I
          iLabel = FL_aA;
        break;
        default:
          free (fpDifference);
          free (bpMinFlag);
          return ERR_METADATA_INTERNAL_ERROR;
       }

      for (iF = iFramePrev; iF < iFrameNext; iF++)
       {
        ipFrameLabel[iF + iStartFrame] = iLabel;
        iLabel = NextLabel (iLabel);
       }
     }
    else if (iFramePrev == 0)
     {
		// extend the pattern
      for (iF = iFrameNext-1; iF >= iFramePrev; iF--)
       {
        ipFrameLabel[iF+iStartFrame] = PreviousLabel
		(ipFrameLabel[iF+iStartFrame+1]);
       }
     }
    else if (iFrameNext == iNFrame)
     {
		// extend the pattern
      for (iF = iFramePrev; iF < iFrameNext; iF++)
       {
        ipFrameLabel[iF+iStartFrame] = NextLabel
		(ipFrameLabel[iF+iStartFrame-1]);
       }
     }
    else
     {
		// break occurs between iFramePrev and iFrameNext
		// look at the differences to figure out exactly
		// where break occurs

		// break occurs when average BlockDiffFrame is largest

      fMaxAveDiff = -1000000.;
      iDiffFrame = iFramePrev;
      fdp = (FRAME_DIFF *) GetRecordPtr() + iFramePrev;

      for (iF = iFramePrev+1; iF <= iFrameNext; iF++)
       {
        fAveDiff = 0.;
        for (iRow = 0; iRow < FRAME_DIFF_NUM_ROW_BLOCK; iRow++)
        for (iCol = 0; iCol < FRAME_DIFF_NUM_COL_BLOCK; iCol++)
         {
          fAveDiff += fdp->faBlockDiffFrame[iRow][iCol];
         }

        if (fAveDiff > fMaxAveDiff)
         {
          fMaxAveDiff = fAveDiff;
          iDiffFrame = iF;
         }

        ++fdp;
       }

	// extend the pattern from iFramePrev to iDiffFrame
      for (iF = iFramePrev; iF < iDiffFrame; iF++)
       {
        ipFrameLabel[iF+iStartFrame] = NextLabel
		(ipFrameLabel[iF+iStartFrame-1]);
       }

	// extend the pattern from iFrameNext back to iDiffFrame
      for (iF = iFrameNext - 1; iF >= iDiffFrame; iF--)
       {
        ipFrameLabel[iF+iStartFrame] = PreviousLabel
		(ipFrameLabel[iF+iStartFrame+1]);
       }

	// if the pattern is consistent at iDiffFrame, there is
	// not really a break

      if (
	(PreviousLabel(ipFrameLabel[iDiffFrame]) != ipFrameLabel[iDiffFrame-1])
	|| (NextLabel(ipFrameLabel[iDiffFrame]) != ipFrameLabel[iDiffFrame+1])
         )
       {
        ipFrameLabel[iDiffFrame] |= FL_32_BREAK;
       }
     }
   }

  free (fpDifference);
  free (bpMinFlag);
  return 0;
}  /* Find32 */

int CMetaData::NextLabel (int iLabelArg)
{
  int iLabel;

  switch (iLabelArg & FL_FRAME)
   {
    case FL_Aa:
      iLabel = FL_Bb;
    break;
    case FL_Bb:
      iLabel = FL_Bc;
    break;
    case FL_Bc:
      iLabel = FL_Cd;
    break;
    case FL_Cd:
      iLabel = FL_Dd;
    break;
    case FL_Dd:
      iLabel = FL_Aa;
    break;
    case FL_A0:
      iLabel = FL_A1;
    break;
    case FL_A1:
      iLabel = FL_B0;
    break;
    case FL_B0:
      iLabel = FL_B1;
    break;
    case FL_B1:
      iLabel = FL_B2;
    break;
    case FL_B2:
      iLabel = FL_A0;
    break;
    case FL_aA:
      iLabel = FL_bB;
    break;
    case FL_bB:
      iLabel = FL_cB;
    break;
    case FL_cB:
      iLabel = FL_dC;
    break;
    case FL_dC:
      iLabel = FL_dD;
    break;
    case FL_dD:
      iLabel = FL_aA;
    break;
    default:
      iLabel = FL_NONE;
   }

  return iLabel;
}  /* NextLabel */

int CMetaData::PreviousLabel (int iLabelArg)
{
  int iLabel;

  switch (iLabelArg & FL_FRAME)
   {
    case FL_Aa:
      iLabel = FL_Dd;
    break;
    case FL_Bb:
      iLabel = FL_Aa;
    break;
    case FL_Bc:
      iLabel = FL_Bb;
    break;
    case FL_Cd:
      iLabel = FL_Bc;
    break;
    case FL_Dd:
      iLabel = FL_Cd;
    break;
    case FL_aA:
      iLabel = FL_dD;
    break;
    case FL_bB:
      iLabel = FL_aA;
    break;
    case FL_cB:
      iLabel = FL_bB;
    break;
    case FL_dC:
      iLabel = FL_cB;
    break;
    case FL_dD:
      iLabel = FL_dC;
    break;
    case FL_A0:
      iLabel = FL_B2;
    break;
    case FL_A1:
      iLabel = FL_A0;
    break;
    case FL_B0:
      iLabel = FL_A1;
    break;
    case FL_B1:
      iLabel = FL_B0;
    break;
    case FL_B2:
      iLabel = FL_B1;
    break;
    default:
      iLabel = FL_NONE;
   }

  return iLabel;
}  /* PreviousLabel */

int CMetaData::AssignCadenceRepairLabels (int iStartFrame, int iNFrame,
		int *ipFrameLabel, int iPulldown, float *fpCutStatistic,
		float fCutThresh)
/*
	This function runs through the frames and assigns frame labels

	Use EPulldown values:

		IF_PULLDOWN_FRAME_3_2 for 720P
		IF_PULLDOWN_FIELD_3_2 for 1080I
		IF_PULLDOWN_SWAPPED_FIELD_3_2 for 525I
*/
{
  int iRet, iFrame;

	// initialize all the frame records
  for (iFrame = iStartFrame; iFrame < iStartFrame + iNFrame; iFrame++)
   {
    ipFrameLabel[iFrame] = FL_NONE;
   }

  MTIassert(iStartFrame >= 0);
  if (iStartFrame < 0)
    return ERR_METADATA_BAD_PARAM;

  iRet = Open ();
  if (iRet)
    return iRet;

  iRet = SelectSection (TYPE_DIFF);
  if (iRet)
   {
    Close ();
    return iRet;
   }

  iRet = ReadWholeSection();
  if (iRet)
   {
    Close ();
    return iRet;
   }

  iRet = FindCadenceLabels (iStartFrame, iNFrame, ipFrameLabel, iPulldown,
                fpCutStatistic, fCutThresh);
  if (iRet)
   {
    Close ();
    return iRet;
   }

  return 0;
}  /* AssignCadenceRepairLabels */

int CMetaData::FindCadenceLabels (int iStartFrame, int iNFrame, int *ipFrameLabel,
		int iPulldown, float *fpCutStatistic, float fCutThresh)
{
  int iNField, iRow, iCol, iF, iDiffFrame, iLabel, iStopFrame, iFramePrev,
		iFrameNext, iFieldStop, iBestSmall, iSmall, iStart,
		iBestStart, iFieldStart, iField, iFrame, iMax;
  float *fpDifference, *fpDiff, fAveDiff, fMaxAveDiff, fTmp;
  float faMax[NUM_MAX];
  FRAME_DIFF *fdp;
  float fSum;
  int iSum;
  float *fpCutDiff;

/*
	Clear the labels
*/

  for (iFrame = 0; iFrame < iNFrame; iFrame++)
   {
    ipFrameLabel[iFrame + iStartFrame] = FL_NONE;
   }

/*
	In order to find the 3:2 sequence, we need to keep track of the
	largest field or frame difference at each location.
*/

  if (iPulldown == IF_PULLDOWN_FRAME_3_2)
   {
    iNField = 1;
   }
  else
   {
    iNField = 2;
   }

  // Summarize the difference values
  fpDifference = (float *) malloc (iNFrame * iNField * sizeof(float));
  if (fpDifference == NULL)
    return ERR_METADATA_OUT_OF_MEMORY;


  fdp = (FRAME_DIFF *)GetRecordPtr() + iStartFrame;
  fpDiff = fpDifference;
  for (iFrame = iStartFrame; iFrame < iStartFrame + iNFrame &&
		iFrame < GetNRecord(); iFrame++)
   {
/*
	We classify the 3:2 sequence by examining smallest difference
	values.  In perfect 3:2 material the difference is zero between
	some of the fields.

	Static material can give zero differences in some of the regions,
	so we keep track of the maximum difference observed.

	Basing decisions on a single region can be a little noisy,
	so we take the average of the largest two differences.
*/

    if (iPulldown == IF_PULLDOWN_FRAME_3_2)
     {
      for (iMax = 0; iMax < NUM_MAX; iMax++)
       {
        faMax[iMax] = -1.;
       }
      iField = 0;
      for (iRow = 0; iRow < FRAME_DIFF_NUM_ROW_BLOCK; iRow++)
      for (iCol = 0; iCol < FRAME_DIFF_NUM_COL_BLOCK; iCol++)
       {
        for (iMax = 0; iMax < NUM_MAX; iMax++)
         {
          if (fdp->faBlockDiffField[iField][iRow][iCol] > faMax[iMax])
           {
            for (int i = NUM_MAX-1; i > iMax; i--)
             {
              faMax[i] = faMax[i-1];
             }

            faMax[iMax] = fdp->faBlockDiffField[iField][iRow][iCol];

            break;	// out of for loop
           }
         }
       }
		// faMax[0-2] is often due to time code overlay, so do not use
      fSum = 0;
      for (int i = 3; i < NUM_MAX; i++)
       {
        fSum += faMax[i];
       }
      fpDiff[iField] = fSum / (float)(NUM_MAX-1);
     }
    else
     {
      for (iField = 0; iField < iNField; iField++)
       {
        for (iMax = 0; iMax < NUM_MAX; iMax++)
         {
          faMax[iMax] = -1.;
         }
        for (iRow = 0; iRow < FRAME_DIFF_NUM_ROW_BLOCK; iRow++)
        for (iCol = 0; iCol < FRAME_DIFF_NUM_COL_BLOCK; iCol++)
         {
          for (iMax = 0; iMax < NUM_MAX; iMax++)
           {
            if (fdp->faBlockDiffField[iField][iRow][iCol] > faMax[iMax])
             {
              for (int i = NUM_MAX-1; i > iMax; i--)
               {
                faMax[i] = faMax[i-1];
               }

              faMax[iMax] = fdp->faBlockDiffField[iField][iRow][iCol];

              break;	// out of for loop
             }
           }
         }
		// faMax[0] and faMax[1] are often due to time code overlay,
		// so do not use
        fSum = 0;
        for (int i = 2; i < NUM_MAX; i++)
         {
          fSum += faMax[i];
         }
        fpDiff[iField] = fSum / (float)(NUM_MAX-1);
       }
     }

    fpDiff += iNField;		// move to next frame
    fdp++;
   }

  fpCutDiff = (float *) malloc (iNFrame * iNField * sizeof(float));
  if (fpCutDiff == NULL)
   {
    free (fpDifference);
    return ERR_METADATA_OUT_OF_MEMORY;
   }

  fdp = (FRAME_DIFF *)GetRecordPtr() + iStartFrame;
  fpDiff = fpCutDiff;
  for (iFrame = iStartFrame; iFrame < iStartFrame + iNFrame &&
		iFrame < GetNRecord(); iFrame++)
   {
/*
	To detect a cut, keep the average field difference over the
	entire frame.  Ignore areas at the top and bottom because of
	letterbox.  Ignore areas at left and right because of framing.
*/

    if (iPulldown == IF_PULLDOWN_FRAME_3_2)
     {
      iField = 0;
      fSum = 0.;
      iSum = 0;
      for (iRow = 2; iRow < FRAME_DIFF_NUM_ROW_BLOCK-2; iRow++)
      for (iCol = 1; iCol < FRAME_DIFF_NUM_COL_BLOCK-1; iCol++)
       {
        fSum += fdp->faBlockDiffField[iField][iRow][iCol];
        iSum++;
       }

      fpDiff[iField] = fSum / (float)(iSum);
     }
    else
     {
      for (iField = 0; iField < iNField; iField++)
       {
        fSum = 0.;
        iSum = 0;
        for (iRow = 2; iRow < FRAME_DIFF_NUM_ROW_BLOCK-2; iRow++)
        for (iCol = 1; iCol < FRAME_DIFF_NUM_COL_BLOCK-1; iCol++)
         {
          fSum += fdp->faBlockDiffField[iField][iRow][iCol];
          iSum++;
         }

        fpDiff[iField] = fSum / (float)(iSum);
       }
     }

    fpDiff += iNField;		// move to next frame
    fdp++;
   }

/*
	In the 720P case, the 3:2 pulldown is of the form:

			AABBBAABBBAABBB

	so the difference values are

			lslsslslsslslss

	where l=large difference and s=small difference

	In the 1080I case, the 3:2 pulldown is of the form:

			AaBbBcCdDdAaBbBcCdDd

	so the difference values are

			llllsllllsllllslllls

	In the 525I case, the 3:2 pulldown is of the form:

			aAbBcBdCdDaAbBcBdCdD

	so the difference values are
			lllllsllsllllllslls

	**NOTE:  the reason 525I does not follow the standard 3:2 pattern
	is that our code rearranges the 525I material so field 1 appears
	first.  This has the advantage of making display and processing
	simpler at the expense of 3:2 detection and removal.

	To make this computation simpler, we temporarily swap the
	values of 525I material to make the pattern llllsllllslllls

	Look for either lsllslsllslslls  OR llllsllllsllllslllls
*/

  if (iPulldown == IF_PULLDOWN_SWAPPED_FIELD_3_2)
   {
		// swap values of fpDifference to make computation simpler
    for (iFrame = 0; iFrame < iNFrame; iFrame++)
     {
      fTmp = fpDifference[2*iFrame];
      fpDifference[2*iFrame] = fpDifference[2*iFrame+1];
      fpDifference[2*iFrame+1] = fTmp;

      fTmp = fpCutDiff[2*iFrame];
      fpCutDiff[2*iFrame] = fpCutDiff[2*iFrame+1];
      fpCutDiff[2*iFrame+1] = fTmp;
     }
   }

#define SEARCH_RADIUS 5
#define MIN_FIELD_DIFF 0.001

  // Find the cuts
  int iRet = FindCuts (ipFrameLabel, fpCutDiff,
		iStartFrame, iNFrame, iNField,
		fCutThresh, SEARCH_RADIUS, MIN_FIELD_DIFF,
		fpCutStatistic);

  if (iRet)
   {
    free (fpDifference);
    free (fpCutDiff);
    return iRet;
   }

  // look for the labels
  iRet = DetermineCadenceLabels (ipFrameLabel, fpDifference, iStartFrame,
		iNFrame, iNField);
  if (iRet)
   {
    free (fpDifference);
    free (fpCutDiff);
    return iRet;
   }


  free (fpDifference);
  free (fpCutDiff);
  return 0;
}  /* FindCadenceLabels */

int CMetaData::Open ()
{
  int iArg;

  Free();

  if (bReadOnly)
   {
    iArg = O_RDONLY | O_BINARY;
   }
  else
   {
    iArg = O_RDWR | O_BINARY;
   }

  iFD = open (strFileName.c_str(), iArg, 0644);
  if (iFD == -1)
   {
    TRACE_1(errout << "INFO: can't open metadata file " << strFileName
                   << " because:" << endl << strerror(errno));
    return ERR_METADATA_OPEN_FAIL;
   }

  if (ReadHeader ())
   {
    TRACE_0(errout << "ERROR: Unable to read the header from metadata file "
                   << strFileName);
    return ERR_METADATA_FILE_IS_CORRUPT;
   }

  return 0;
}  /* Open */

int CMetaData::Create ()
{
  int iRet;

  MTIassert(!bReadOnly);
  if (bReadOnly)
   {
    return ERR_METADATA_INTERNAL_ERROR;
   }

  Free();
  ClearHeader();

  iFD = open (strFileName.c_str(), O_RDWR | O_CREAT | O_TRUNC | O_BINARY, 0644);

  if (iFD == -1)
   {
    TRACE_0(errout << "ERROR: can't create metadata file " << strFileName
                   << " because:" << endl << strerror(errno));
    return ERR_METADATA_CREATE_FAIL;
   }

  iRet = WriteHeader();

  return iRet;
}  /* Create */

int CMetaData::Delete ()
{
  int iRet;

  MTIassert(!bReadOnly);
  if (bReadOnly)
   {
    return ERR_METADATA_INTERNAL_ERROR;
   }

  Free();
  ClearHeader();

  chmod (strFileName.c_str(), S_IREAD | S_IWRITE);
  iRet = _unlink (strFileName.c_str());

  if (iRet == -1)
   {
    TRACE_1(errout << "ERROR: can't unlink metadata file " << strFileName
                   << " because:" << endl << strerror(errno));
    iRet = ERR_METADATA_DELETE_FAIL;
   }

  return iRet;
}  /* Create */

int CMetaData::Initialize (long lType, long lNRecord,
		void **vpSubHeader, void **vpRecord)
{
  int iRet;

  if (vpSubHeader != NULL)
    *vpSubHeader = NULL;
  if (vpRecord != NULL)
    *vpRecord = NULL;


  iRet = Open();
  if (iRet)
   {
        // try to delete the file first in case it is there but corrupt
    Delete();
	// file does not exist.  Create it
    iRet = Create();
    if (iRet)
     {
      return iRet;
     }
   }


  if (lType != TYPE_NONE)
   {
	// select the desired section
    iRet = SelectSection (lType);
    if (iRet)
     {
      if (iRet == ERR_METADATA_NO_SUCH_SECTION)
       {
          // section does not exist, create it
        iRet = AddSection (lType, lNRecord);
       }
      if (iRet)
        return iRet;
     }

	// read in the entire section
    iRet = ReadWholeSection ();
    if (iRet)
      return iRet;
   }

	// return the pointer
  if (vpSubHeader != NULL)
    *vpSubHeader = GetSubHeaderPtr();
  if (vpRecord != NULL)
    *vpRecord = GetRecordPtr();

  return 0;
}  /* Initialize */


int CMetaData::Close ()
{
  if (iFD != -1)
   {
    int iRet = close (iFD);
    MTIassert (iRet == 0);
    if (iRet != 0)
     {
      TRACE_0(errout << "ERROR: Can't close metadata file because:" << endl
                     << strerror(errno));
      // F*ck it, just keep going - can only be because iFD is bogus
      //return ERR_METADATA_INTERNAL_ERROR;
     }

    iFD = -1;
   }

  return 0;
}  /* Close */

int CMetaData::WriteHeader()
{
  MTIassert (!bMustSwap);
  if (bMustSwap)
    return ERR_METADATA_INTERNAL_ERROR;

  if (lseek64 (iFD, 0, SEEK_SET) < 0)
   {
    TRACE_0(errout << "ERROR: Can't position to beginnning of metadata file");
    return ERR_METADATA_SEEK_FAIL;
   }

  if (write (iFD, &mh, sizeof(MD_HEADER)) < 0)
   {
    TRACE_0(errout << "ERROR: Can't write header to metadata file "
                   << strFileName << " because :" << endl << strerror(errno));
    return ERR_METADATA_HEADER_WRITE_FAIL;
   }

  return 0;
}  /* WriteHeader */

int CMetaData::WriteRecordSet(long lStart, long lStop)
/*
	Write all entries >= Start and < Stop
*/
{
  MTI_INT64 llFilePosition, llRecordByteOffset;

  MTIassert(!bReadOnly);
  if (bReadOnly)
   {
    TRACE_0(errout << "INTERNAL ERROR: Can't write metadata record: READONLY");
    return ERR_METADATA_WRITE_RECORD_FAIL;
   }

  if (lCurrentSection == NULL_SECTION)
   {
    TRACE_0(errout << "INTERNAL ERROR: Can't write metadata record: NULLSECT");
    return ERR_METADATA_WRITE_RECORD_FAIL;
   }

  if (lStart < 0)
   {
    TRACE_0(errout << "INTERNAL ERROR: Can't write metadata record: LSTART("
                   << lStart << ")" );
    return ERR_METADATA_WRITE_RECORD_FAIL;
   }

  if (lStop > msha[lCurrentSection].lNRecord)
   {
    TRACE_0(errout << "INTERNAL ERROR: Can't write metadata record: LSTOP("
                   << lStart << "+" << (lStop-lStart) << " > "
                   << msha[lCurrentSection].lNRecord << ")" );
    return ERR_METADATA_WRITE_RECORD_FAIL;
   }

  if (lStart >= lStop)
   {
    TRACE_0(errout << "INTERNAL ERROR: Can't write metadata record: PARAMS("
                   << lStart << " > " << lStop << ")" );
    return ERR_METADATA_WRITE_RECORD_FAIL;
   }

  llRecordByteOffset = lStart * msha[lCurrentSection].lRecordSize;

  llFilePosition = mh.llaSectionPosition[lCurrentSection] +
                     sizeof (MD_SECTION_HEADER) +
                     msha[lCurrentSection].lSubHeaderSize + llRecordByteOffset;

	// Advance file descriptor to correct position
  if (lseek64 (iFD, llFilePosition, SEEK_SET) < 0)
   {
    TRACE_0(errout << "INTERNAL ERROR: Can't write metadata record: SEEK("
                   << llFilePosition << ")" );
    return ERR_METADATA_SEEK_FAIL;
   }

  long lMaxWriteRecords = MAX_IO_LENGTH / msha[lCurrentSection].lRecordSize;
  long lRecordsLeftToWrite = (lStop - lStart);
  long lWriteLength;

  while (lRecordsLeftToWrite > 0)
   {
    long lNWriteRecords = lRecordsLeftToWrite;
    if (lNWriteRecords > lMaxWriteRecords)
     {
      lNWriteRecords = lMaxWriteRecords;
     }
    lRecordsLeftToWrite -= lNWriteRecords;
    lWriteLength = lNWriteRecords * msha[lCurrentSection].lRecordSize;

    if (write (iFD, (MTI_UINT8 *)(vpSectionRecord[lCurrentSection]) +
                  (long)llRecordByteOffset, lWriteLength) < 0)
     {
      TRACE_0(errout << "INTERNAL ERROR: Can't write metadata record: WRITE("
                     << lWriteLength << "@" << llRecordByteOffset
                     << ") because " << strerror(errno));
      return ERR_METADATA_WRITE_RECORD_FAIL;
     }

    llRecordByteOffset += lWriteLength;
   }

  TRACE_3(errout << "MetaData to disk: " << lStart << " --> " << (lStop-1));
  return 0;
}  /* WriteRecord */

int CMetaData::WriteOneRecord(long lRecord)
/*
	Write out a single entry
*/
{
  return WriteRecordSet (lRecord, lRecord+1);
}  /* WriteRecord */

int CMetaData::WriteWholeSection()
/*
	Write out all entries, including headers
*/
{
  int iRet;

  MTIassert(lCurrentSection != NULL_SECTION);
  if (lCurrentSection == NULL_SECTION)
    return ERR_METADATA_INTERNAL_ERROR;

  iRet = WriteSectionHeader ();
  if (iRet)
    return iRet;

  iRet = WriteRecordSet (0, msha[lCurrentSection].lNRecord);
  if (iRet)
    return iRet;

  return 0;
}  /* WriteRecord */

int CMetaData::ReadHeader()
{
  if (lseek64 (iFD, 0, SEEK_SET) < 0)
    return ERR_METADATA_SEEK_FAIL;

  if (read (iFD, &mh, sizeof(MD_HEADER)) < 0)
   {
    TRACE_0(errout << "ERROR: Can't read header from metadata file "
                   << strFileName << " because:" << endl << strerror(errno));
    return ERR_METADATA_READ_HEADER_FAIL;
   }

  if (mh.ulMagicNumber == MTIX)
   {
    bMustSwap = false;
   }
  else
   {
	 unsigned int ulMagicLocal = mh.ulMagicNumber;
	 byteSwap(ulMagicLocal);

    if (ulMagicLocal == MTIX)
     {
      bMustSwap = true;
     }
    else
     {
      return ERR_METADATA_FILE_IS_CORRUPT;
     }
   }

	// swap the bytes
	if (bMustSwap)
	{
		byteSwap(mh.ulMagicNumber);
		byteSwap(mh.lVersion);
		byteSwap(mh.lNSection);
		for (long lSection = 0; lSection < MAX_SECTION; lSection++)
		{
			byteSwap(mh.laSectionType[lSection]);
		}

		for (long lSection = 0; lSection <= MAX_SECTION; lSection++)
		{
			byteSwap(mh.llaSectionPosition[lSection]);
		}

	}

  return 0;
}  /* ReadHeader */

int CMetaData::ReadRecordSet(long lStart, long lStop)
/*
	Read all entries >= Start and < Stop
*/
{
  MTI_INT64 llFilePosition, llRecordByteOffset;

  MTIassert(lCurrentSection != NULL_SECTION);
  if (lCurrentSection == NULL_SECTION)
    return ERR_METADATA_INTERNAL_ERROR;

  MTIassert(lStart >= 0);
  MTIassert(lStop <= msha[lCurrentSection].lNRecord);
  MTIassert(lStart < lStop);
  if (lStart < 0 || lStop > msha[lCurrentSection].lNRecord || lStart >= lStop)
    return ERR_METADATA_INTERNAL_ERROR;

  llRecordByteOffset = lStart * msha[lCurrentSection].lRecordSize;

  llFilePosition = mh.llaSectionPosition[lCurrentSection] +
		sizeof (MD_SECTION_HEADER) +
			msha[lCurrentSection].lSubHeaderSize + llRecordByteOffset;

	// Advance file descriptor to correct position
  if (lseek64 (iFD, llFilePosition, SEEK_SET) < 0)
   {
    TRACE_0(errout << "ERROR: Can't seek to " << llFilePosition
                   << " in metadata file " << strFileName << " because:"
                   << endl << strerror(errno));
    return ERR_METADATA_SEEK_FAIL;
   }

  long lMaxReadRecords = MAX_IO_LENGTH / msha[lCurrentSection].lRecordSize;
  long lRecordsLeftToRead = (lStop - lStart);
  long lReadLength;

  while (lRecordsLeftToRead > 0)
   {
    long lNReadRecords = lRecordsLeftToRead;
    if (lNReadRecords > lMaxReadRecords)
     {
      lNReadRecords = lMaxReadRecords;
     }
    lRecordsLeftToRead -= lNReadRecords;
    lReadLength = lNReadRecords * msha[lCurrentSection].lRecordSize;

    if (read (iFD, (MTI_UINT8 *)vpSectionRecord[lCurrentSection] +
                  (long)llRecordByteOffset, lReadLength) < 0)
     {
      TRACE_0(errout << "ERROR: Can't read record set from metadata file "
                     << strFileName << " because:" << endl << strerror(errno));
      return ERR_METADATA_READ_RECORD_FAIL;
     }

    llRecordByteOffset += lReadLength;
   }
  llRecordByteOffset = lStart * msha[lCurrentSection].lRecordSize;

  // swap the entries in the record
	if (bMustSwap)
	{
		for (long lRecord = lStart; lRecord < lStop; lRecord++)
		{
			switch (msha[lCurrentSection].lType)
			{
				case TYPE_DIFF:
				{
					FRAME_DIFF *fpd = (FRAME_DIFF*)((MTI_UINT8*)vpSectionRecord[lCurrentSection] + (long)llRecordByteOffset);
					for (int iRow = 0; iRow < FRAME_DIFF_NUM_ROW_BLOCK; iRow++)
					{
						for (int iCol = 0; iCol < FRAME_DIFF_NUM_COL_BLOCK; iCol++)
						{
							byteSwap(fpd->faBlockAveFrame[iRow][iCol]);
							byteSwap(fpd->faBlockDiffFrame[iRow][iCol]);
							byteSwap(fpd->faBlockAveField[0][iRow][iCol]);
							byteSwap(fpd->faBlockAveField[1][iRow][iCol]);
							byteSwap(fpd->faBlockDiffField[0][iRow][iCol]);
							byteSwap(fpd->faBlockDiffField[1][iRow][iCol]);
						}
					}
					byteSwap(fpd->lDiffFlag);
				} break;

				case TYPE_PAN_SCAN_INSTRUCTION:
				{
					PAN_INSTRUCTION *pip = (PAN_INSTRUCTION*)((MTI_UINT8*)vpSectionRecord[lCurrentSection] + (long)llRecordByteOffset);

					byteSwap(pip->fShiftH);
					byteSwap(pip->fShiftV);
					byteSwap(pip->fZoomH);
					byteSwap(pip->fZoomV);
					byteSwap(pip->fRotate);
					byteSwap(pip->fFrameAspectRatioSrc);
					byteSwap(pip->fFrameAspectRatioDst);
					byteSwap(pip->lPanFlag);
				} break;
				default:
				return ERR_METADATA_INTERNAL_ERROR;
			}

			llRecordByteOffset += msha[lCurrentSection].lRecordSize;
		}
	}

  return 0;
}  /* ReadRecord */

int CMetaData::ReadOneRecord(long lRecord)
/*
	Read in a single entry
*/
{
  return ReadRecordSet (lRecord, lRecord+1);
}  /* ReadRecord */

int CMetaData::ReadWholeSection()
/*
	Read in all entries, including section headers
*/
{
  int iRet;

  MTIassert(lCurrentSection != NULL_SECTION);
  if (lCurrentSection == NULL_SECTION)
    return ERR_METADATA_INTERNAL_ERROR;

  iRet = ReadSectionHeader ();
  if (iRet)
    return iRet;

/*
	Allocate storage for the records
*/

  free (vpSectionRecord[lCurrentSection]);
  vpSectionRecord[lCurrentSection] = NULL;

  vpSectionRecord[lCurrentSection] = (void *) malloc
		(msha[lCurrentSection].lRecordSize *
			msha[lCurrentSection].lNRecord);
  if (vpSectionRecord[lCurrentSection] == NULL)
    return ERR_METADATA_OUT_OF_MEMORY;

  iRet = ReadRecordSet (0,  msha[lCurrentSection].lNRecord);
  if (iRet)
    return iRet;

  return 0;
}  /* ReadRecord */

int CMetaData::ReadSectionHeader()
{
  MTIassert(lCurrentSection != NULL_SECTION);
  if (lCurrentSection == NULL_SECTION)
    return ERR_METADATA_INTERNAL_ERROR;

  if (lseek64 (iFD, mh.llaSectionPosition[lCurrentSection], SEEK_SET) < 0)
   {
    TRACE_0(errout << "ERROR: Can't seek to " << mh.llaSectionPosition[lCurrentSection]
                   << " in metadata file " << strFileName << " because: "
                   << endl << strerror(errno));
    return ERR_METADATA_SEEK_FAIL;
   }

  if (read (iFD, &msha[lCurrentSection], sizeof(MD_SECTION_HEADER)) < 0)
   {
    TRACE_0(errout << "ERROR: Can't read section header from metadata file "
                   << strFileName << " because: " << endl << strerror(errno));
    return ERR_METADATA_READ_HEADER_FAIL;
   }

	if (bMustSwap)
	{
		byteSwap(msha[lCurrentSection].lVersion);
		byteSwap(msha[lCurrentSection].lType);
		byteSwap(msha[lCurrentSection].lSubHeaderSize);
		byteSwap(msha[lCurrentSection].lRecordSize);
		byteSwap(msha[lCurrentSection].lNRecord);
	}

  free (vpSubHeader[lCurrentSection]);
  vpSubHeader[lCurrentSection] = NULL;

  free (vpSectionRecord[lCurrentSection]);
  vpSectionRecord[lCurrentSection] = NULL;

/*
	If necessary, read in the sub-header
*/

  if (msha[lCurrentSection].lSubHeaderSize > 0)
   {
    vpSubHeader[lCurrentSection] = (void *) malloc
		(msha[lCurrentSection].lSubHeaderSize);
    if (vpSubHeader[lCurrentSection] == NULL)
      return ERR_METADATA_OUT_OF_MEMORY;

    if (read (iFD, vpSubHeader[lCurrentSection],
		msha[lCurrentSection].lSubHeaderSize) < 0)
     {
      TRACE_0(errout << "ERROR: Can't read section subheader from metadata file "
                     << strFileName << " because: " << endl << strerror(errno));
      return ERR_METADATA_READ_HEADER_FAIL;
     }
   }

  return 0;
}  /* ReadSectionHeader */

int CMetaData::WriteSectionHeader()
{
  MTIassert(!bReadOnly);
  if (bReadOnly)
    return ERR_METADATA_INTERNAL_ERROR;

  MTIassert(!bMustSwap);
  if (bMustSwap)
    return ERR_METADATA_INTERNAL_ERROR;

  MTIassert(lCurrentSection != NULL_SECTION);
  if (lCurrentSection == NULL_SECTION)
    return ERR_METADATA_INTERNAL_ERROR;

  if (lseek64 (iFD, mh.llaSectionPosition[lCurrentSection], SEEK_SET) < 0)
   {
    TRACE_0(errout << "ERROR: Can't seek to " << mh.llaSectionPosition[lCurrentSection]
                   << " in metadata file " << strFileName << " because: "
                   << endl << strerror(errno));
    return ERR_METADATA_SEEK_FAIL;
   }

  if (write (iFD, &msha[lCurrentSection], sizeof(MD_SECTION_HEADER)) < 0)
   {
    TRACE_0(errout << "ERROR: Can't write section header to metadata file "
                   << strFileName << " because: " << endl << strerror(errno));
    return ERR_METADATA_HEADER_WRITE_FAIL;
   }

/*
	If necessary, write out the sub-header
*/

  if (msha[lCurrentSection].lSubHeaderSize > 0)
   {
    if (write (iFD, vpSubHeader[lCurrentSection],
		msha[lCurrentSection].lSubHeaderSize) < 0)
     {
      TRACE_0(errout << "ERROR: Can't write section subheader to metadata file "
                     << strFileName << " because: " << endl << strerror(errno));
      return ERR_METADATA_HEADER_WRITE_FAIL;
     }
   }

  return 0;
}  /* WriteSectionHeader */

void CMetaData::ClearHeader()
{
  mh.ulMagicNumber = ('M' << 24) + ('T' << 16) + ('I' << 8) + 'X';
  sprintf (mh.caDescription, "\nBinary Meta Data File\n");
  mh.lVersion = VERSION_1;

  mh.lNSection = 0;
  mh.llaSectionPosition[mh.lNSection] = sizeof(MD_HEADER);

  lCurrentSection = NULL_SECTION;
}  /* ClearHeader */

int CMetaData::Free()
{
  long lSection;
  int iRet = 0;

  /* iRet = */ Close();

  for (lSection = 0; lSection < MAX_SECTION; lSection++)
   {
    free (vpSubHeader[lSection]);
    vpSubHeader[lSection] = NULL;

    free (vpSectionRecord[lSection]);
    vpSectionRecord[lSection] = NULL;
   }

  return iRet;
}  /* Free */

int CMetaData::SelectSection (long lType)
{
  long lSection;

	// return if no file is open
  MTIassert(iFD != -1);
  if (iFD == -1)
    return ERR_METADATA_INTERNAL_ERROR;

  lCurrentSection = NULL_SECTION;

  for (lSection = 0; lSection < mh.lNSection && lCurrentSection == NULL_SECTION;
	lSection++)
   {
    if (mh.laSectionType[lSection] == lType)
     {
      lCurrentSection = lSection;
     }
   }

  if (lCurrentSection == NULL_SECTION)
    return ERR_METADATA_NO_SUCH_SECTION;

  return 0;
}  /* SelectSection */

int CMetaData::AddSection (long lType, long lNRecord)
{
  int iRet, iCnt;
  long lSection, lRecord, lRow, lCol;
  FRAME_DIFF fdEmpty;
  PAN_INSTRUCTION piEmpty;
  AUDIO_PROXY apEmpty;
  void *vpEmptyRecord, *vpSubHeaderLocal;

	// return if no file is open
  MTIassert(iFD != -1);
  if (iFD == -1)
    return ERR_METADATA_INTERNAL_ERROR;

/*
	Does this section already exist?
*/

  for (lSection = 0; lSection < mh.lNSection; lSection++)
   {
    MTIassert(mh.laSectionType[lSection] != lType);
    if (mh.laSectionType[lSection] == lType)
     {
      return ERR_METADATA_INTERNAL_ERROR;
     }
   }

/*
	Section does not exist, so create a new section
*/

  lCurrentSection = mh.lNSection;
  MTIassert(lCurrentSection < MAX_SECTION);
  if (lCurrentSection >= MAX_SECTION)
    return ERR_METADATA_INTERNAL_ERROR;

  msha[lCurrentSection].lType = lType;
  msha[lCurrentSection].lNRecord = lNRecord;

//  Add new structures here.
//    There are three steps:
//      1.  set version and size of SubHeader and Record
//      2.  set the empty record value
//      3.  set the values of the sub header

  switch (lType)
   {
    case TYPE_DIFF:
		// set version and size of SubHeader and Record
      msha[lCurrentSection].lVersion = VERSION_1;
      msha[lCurrentSection].lSubHeaderSize = 0;
      msha[lCurrentSection].lRecordSize = sizeof(FRAME_DIFF);

		// set the empty record
      for (lRow = 0; lRow < FRAME_DIFF_NUM_ROW_BLOCK; lRow++)
      for (lCol = 0; lCol < FRAME_DIFF_NUM_COL_BLOCK; lCol++)
       {
        fdEmpty.faBlockAveFrame[lRow][lCol] = 0.;
        fdEmpty.faBlockDiffFrame[lRow][lCol] = 0.;
        fdEmpty.faBlockAveField[0][lRow][lCol] = 0.;
        fdEmpty.faBlockDiffField[0][lRow][lCol] = 0.;
        fdEmpty.faBlockAveField[1][lRow][lCol] = 0.;
        fdEmpty.faBlockDiffField[1][lRow][lCol] = 0.;
       }
      fdEmpty.lDiffFlag = DF_NOT_CALC;
      vpEmptyRecord = &fdEmpty;

		// set the sub header
      vpSubHeaderLocal = NULL;		// no sub-header for FRAME_DIFF
    break;
    case TYPE_PAN_SCAN_INSTRUCTION:
		// set version and size of SubHeader and Record
      msha[lCurrentSection].lVersion = VERSION_1;
      msha[lCurrentSection].lSubHeaderSize = 0;
      msha[lCurrentSection].lRecordSize = sizeof(PAN_INSTRUCTION);

		// set the empty record
      piEmpty.fShiftH = 0.;
      piEmpty.fShiftV = 0.;
      piEmpty.fZoomH = 0.;
      piEmpty.fZoomV = 0.;
      piEmpty.fRotate = 0.;
      piEmpty.fFrameAspectRatioSrc = 0.;
      piEmpty.fFrameAspectRatioDst = 0.;
      piEmpty.bFullFrameFlagSrc = false;
      piEmpty.bFullFrameFlagDst = false;
      piEmpty.lPanFlag = PF_NONE;
      vpEmptyRecord = &piEmpty;

		// set the sub header
      vpSubHeaderLocal = NULL;		// no sub-header for PAN_INSTRUCTION
    break;
    case TYPE_AUDIO_PROXY:
		// set version and size of SubHeader and Record
      msha[lCurrentSection].lVersion = VERSION_1;
      msha[lCurrentSection].lSubHeaderSize = 0;
      msha[lCurrentSection].lRecordSize = sizeof(AUDIO_PROXY);

		// set the empty record
      for (iCnt = 0; iCnt < MAX_AUDIO_PROXY_CHANNEL; iCnt++)
       {
        apEmpty.saMinimum[iCnt] = 0;
        apEmpty.saMaximum[iCnt] = 0;
       }
      vpEmptyRecord = &piEmpty;

		// set the sub header
      vpSubHeaderLocal = NULL;		// no sub-header for AUDIO_PROXY
    break;
    default:
      return ERR_METADATA_INTERNAL_ERROR;
   }

  free (vpSubHeader[lCurrentSection]);
  vpSubHeader[lCurrentSection] = NULL;

  free (vpSectionRecord[lCurrentSection]);
  vpSectionRecord[lCurrentSection] = NULL;

  if (msha[lCurrentSection].lSubHeaderSize)
   {
		// Initialize a new sub-header
    vpSubHeader[lCurrentSection] = (void *) malloc (
		msha[lCurrentSection].lSubHeaderSize);
    if (vpSubHeader[lCurrentSection] == NULL)
     {
      return ERR_METADATA_OUT_OF_MEMORY;
     }

    memcpy (vpSubHeader[lCurrentSection], vpSubHeaderLocal,
		msha[lCurrentSection].lSubHeaderSize);
   }

		// Initialize the storage for the records
  vpSectionRecord[lCurrentSection] = (void *) malloc (lNRecord *
		msha[lCurrentSection].lRecordSize);
  if (vpSectionRecord[lCurrentSection] == NULL)
   {
    return ERR_METADATA_OUT_OF_MEMORY;
   }

  for (lRecord = 0; lRecord < lNRecord; lRecord++)
   {
    memcpy ((MTI_UINT8 *)vpSectionRecord[lCurrentSection] +
		(lRecord * msha[lCurrentSection].lRecordSize), vpEmptyRecord,
		msha[lCurrentSection].lRecordSize);
   }

/*
	Write out this new section
*/

  iRet = WriteWholeSection ();
  if (iRet)
    return iRet;

/*
	Write new header
*/

  mh.laSectionType[lCurrentSection] = lType;
  mh.llaSectionPosition[lCurrentSection+1] =
	mh.llaSectionPosition[lCurrentSection] +
	msha[lCurrentSection].lNRecord * msha[lCurrentSection].lRecordSize;
  mh.lNSection++;

  iRet = WriteHeader();
  if (iRet)
    return iRet;

  return 0;
}  /* AddSection */

void * CMetaData::GetRecordPtr ()
{
  if (lCurrentSection == NULL_SECTION)
    return NULL;

  return vpSectionRecord[lCurrentSection];
}  /* GetRecordPtr */

void * CMetaData::GetSubHeaderPtr ()
{
  if (lCurrentSection == NULL_SECTION)
    return NULL;

  return vpSubHeader[lCurrentSection];
}  /* GetSubHeaderPtr */

long CMetaData::GetNRecord ()
{
  if (lCurrentSection == NULL_SECTION)
    return 0;

  return msha[lCurrentSection].lNRecord;
}  /* GetNRecord */

int CMetaData::FindCuts (int *ipFrameLabel, float *fpFieldDiff,
		int iStartFrame, int iNFrame, int iNField,
		float fThreshold, int iMedianSearchRadius, float fMinFieldDiff,
		float *fpStatRatio)
/*
	If fpStatRatio is not NULL, it will be filled in
*/
{
  int iField, iEventIndex;
  float fStatRatio, fStatRatioB, fFieldDiff;
  int iFrame;

  for (iFrame = 0; iFrame < iNFrame; iFrame++)
   {
    iField = (iStartFrame + iFrame) * iNField;

    fStatRatio = FindStatRatio (iField, fpFieldDiff, iNFrame*iNField,
		iMedianSearchRadius);
    fFieldDiff = fpFieldDiff[iField];

    if (iNField == 2)
     {
      fStatRatioB = FindStatRatio (iField+1, fpFieldDiff, iNFrame*iNField,
		iMedianSearchRadius);

      // keep track of the minimum stat ratio
      if (fStatRatioB < fStatRatio)
        fStatRatio = fStatRatioB;

      if (fpFieldDiff[iField+1] < fFieldDiff)
        fFieldDiff = fpFieldDiff[iField+1];
     }

    // keep track of the minimum of

    if (fpStatRatio)
     {
      fpStatRatio[iFrame] = fStatRatio;
     }


/*
	A cut must have all fields above the threshold
*/

    if (fStatRatio > fThreshold)
     {
/*
	If the FieldDiff statistic is too small, we will not
	consider placing a cut here.
*/

      if (fFieldDiff > fMinFieldDiff)
       {
        ipFrameLabel[iFrame] |= FL_CUT;
       }
     }
   }

  // force a cut at the first frame
  ipFrameLabel[0] |= FL_CUT;

  return 0;
}  /* FindCuts */

float CMetaData::FindStatRatio (int iField, float *fpFieldDiff, int iNTotField,
	int iMedianSearchRadius)
{
  int iFieldStart, iFieldStop, iMinRange, iRange, iF, iCnt, iNAbove,
	iNBelow;
  float fCandidate, fMedian, fStatRatio, fStat, *fpFD;

  iFieldStart = iField - iMedianSearchRadius;
  if (iFieldStart < 0)
    iFieldStart = 0;

  iFieldStop = iFieldStart + 2*iMedianSearchRadius + 1;
  if (iFieldStop > iNTotField)
    iFieldStop = iNTotField;

  iMinRange = iFieldStop - iFieldStart;

  for (iF = iFieldStart; iF < iFieldStop; iF++)
   {
    iNAbove = 0;
    iNBelow = 0;

    fCandidate = fpFieldDiff[iF];

    fpFD = fpFieldDiff + iFieldStart;
    for (iCnt = iFieldStart; iCnt < iFieldStop; iCnt++)
     {
      if (*fpFD <= fCandidate)
        iNBelow++;

      if (*fpFD >= fCandidate)
        iNAbove++;

      fpFD++;
     }

    if (iNAbove > iNBelow)
      iRange = iNAbove - iNBelow;
    else
      iRange = iNBelow - iNAbove;

    if (iRange < iMinRange)
     {
      iMinRange = iRange;
      fMedian = fCandidate;
     }
   }

  fStat = fpFieldDiff[iField];
/*
	Prevent divide by zero errors.
*/
  if (
	(fMedian > 0.) || (fStat > 0.)
     )
   {
    fStatRatio = fStat / (fMedian + fStat);
   }
  else
   {
    fStatRatio = 0.;
   }

  return fStatRatio;
}  /* FindStatRatio */

int CMetaData::DetermineCadenceLabels (int *ipFrameLabel, float *fpDifference,
	int iStartFrame, int iNFrame, int iNField)
{

  int iCut0, iCut1, iFrame, iRet;

/*
	Look for 3/2 runs between subsequent cuts.
*/

  iCut0 = iStartFrame;
  iCut1 = iStartFrame;
  iFrame = iStartFrame+1;
  while (iCut1 < iNFrame + iStartFrame)
   {
    // find the next cut
    iCut1 = -1;
    while (iFrame < iNFrame + iStartFrame && iCut1 == -1)
     {
      if (ipFrameLabel[iFrame] & FL_CUT)
       {
        iCut1 = iFrame;
       }

      iFrame++;
     }

    if (iFrame == iNFrame + iStartFrame)
     {
      iCut1 = iFrame;
     }


    iRet = FindConsistentRun (ipFrameLabel, fpDifference, iCut0, iCut1,
		iNField);
    if (iRet)
      return iRet;

    iCut0 = iCut1;
   }

  return 0;
}

int CMetaData::FindConsistentRun (int *ipFrameLabel, float *fpDifference,
		int iCut0, int iCut1, int iNField)
{
  int iFFVVF, *ipMinFlag, iFieldStart, iFieldStop, iCnt, iFStart,
		iFStop, iF, iField, iBestMistake,
		iaMistake[5], iNMin, iLength, iLengthBad, iLongestBad,
		iaLongestBad[5], iFlag;
  int iFFVVF0, iFFVVF1;
  float fMinDiffLeft, fMinDiffRight;
  int *ipMinFlagAlloc;

  // allocate storage to hold the min flags
  ipMinFlagAlloc = (int *) malloc ( (iCut1 - iCut0) * iNField * sizeof(int) );
  if (ipMinFlagAlloc == 0)
    return ERR_METADATA_OUT_OF_MEMORY;

  ipMinFlag = ipMinFlagAlloc - (iCut0 * iNField);

  iFieldStart = iCut0 * iNField;
  iFieldStop = iCut1 * iNField;


/*
	Assign the values to MinFlag.
	MF_HARD_MINIMUM means that the value is significantly less than the
	surround.
	MF_SOFT_MINIMUM means the value is somewhat less than the surround.
	MF_ISOLATED_MINIMUM means that the MF_HARD_MINIMUM or
	MF_SOFT_MINIMUM is surrounded by four MF_NONE fields.
*/

#define MF_NONE 0x0
#define MF_HARD_MINIMUM 0x1
#define MF_SOFT_MINIMUM 0x2
#define MF_ISOLATED_MINIMUM 0x4


  for (iField = iFieldStart; iField < iFieldStop; iField++)
   {
    iFStart = iField - 4;
    iFStop = iField + 5;
    if (iFStart < iFieldStart)
      iFStart = iFieldStart;
    if (iFStop > iFieldStop)
      iFStop = iFieldStop;

    fMinDiffLeft = fpDifference[iField];
    fMinDiffRight = fpDifference[iField];

    for (iF = iFStart; iF <= iField; iF++)
     {
      if (fpDifference[iF] < fMinDiffLeft)
       {
        fMinDiffLeft = fpDifference[iF];
       }
     }
    for (iF = iField; iF < iFStop; iF++)
     {
      if (fpDifference[iF] < fMinDiffRight)
       {
        fMinDiffRight = fpDifference[iF];
       }
     }

    ipMinFlag[iField] = MF_NONE;
    if (fpDifference[iField] == fMinDiffLeft ||
		fpDifference[iField] == fMinDiffRight)
     {
      ipMinFlag[iField] = MF_SOFT_MINIMUM;
     }

/*
	Since a cut that occurs at a DD needs a min flag on the cut
	field.
*/

    if (iField == iFieldStart + 1)
     {
      ipMinFlag[iField] = MF_SOFT_MINIMUM;
     }
   }

/*
	Look for MinFlags that are isolated.  That is, a MF_HARD_MINIMUM or
	MF_SOFT_MINIMUM that is surrounded by MF_NONE.
*/

  for (iField = iFieldStart; iField < iFieldStop; iField++)
   {
    iFStart = iField - 4;
    iFStop = iField + 5;

/*
	Near the cut point, we expect to see minimum flags.  Allow the
	ISOLATED flag to be set.
*/
    if (iFStart < iFieldStart+5)
      iFStart = iFieldStart+5;
    if (iFStop > iFieldStop-5)
      iFStop = iFieldStop-5;

    if (ipMinFlag[iField] & (MF_SOFT_MINIMUM | MF_HARD_MINIMUM))
     {
      iFlag = MF_ISOLATED_MINIMUM;
      for (iF = iFStart; iF < iFStop; iF++)
      if (iF != iField)
       {
        if (ipMinFlag[iF] != MF_NONE)
          iFlag &= ~MF_ISOLATED_MINIMUM;
       }
      ipMinFlag[iField] |= iFlag;
     }
   }


/*
	For each of five starting values, look for longest run of minimum
	values.
*/

  iBestMistake = (iFieldStop - iFieldStart);
  for (iCnt = 0; iCnt < 5; iCnt++)
   {
    iNMin = 0;
    iLength = 0;
    iLongestBad = 0;
    for (iField = iFieldStart + iCnt; iField < iFieldStop; iField += 5)
     {
      if (ipMinFlag[iField] & MF_SOFT_MINIMUM)
       {
        iNMin++;
        iLengthBad = 0;
       }
      else
       {
        iLengthBad++;
        if (iLengthBad > iLongestBad)
          iLongestBad = iLengthBad;
       }
      iLength++;
     }

    iaMistake[iCnt] = iLength - iNMin;
    iaLongestBad[iCnt] = iLongestBad;
    if (iaMistake[iCnt] < iBestMistake)
     {
      iBestMistake = iaMistake[iCnt];
     }
   }

/*
	Any starting position that has the fewest number of mistakes
	is a run.
*/

#define TOLERANCE_32 2

  for (iCnt = 0; iCnt < 5; iCnt++)
   {
    if (
	(iaMistake[iCnt] == 0) ||
	(iaLongestBad[iCnt] < TOLERANCE_32 &&
		iaMistake[iCnt] == iBestMistake)
       )
     {
/*
	Note:  iFFVVF =  0, 1, 2, 3, 4, 5, 6, 7, 8, 9
	corresponding to A, A, B, B, B, C, C, D, D, D

	The matching values of iCnt are:
			 4, 3, 2, 1, 0, 4, 3, 2, 1, 0

	So we need even or odd designation of iFieldStart
	to disambiguate the iFFVVF:
*/

      if ((iFieldStart + iCnt) & 0x1)
        iFFVVF = 9 - iCnt;
      else
        iFFVVF = 4 - iCnt;

/*
	Assign the labels to the fields
*/

      for (iField = iFieldStart; iField < iFieldStop; iField+=iNField)
       {
        iFFVVF0 = iFFVVF;

        iFFVVF++;
        if (iFFVVF > 9)
          iFFVVF = 0;

        if (iNField == 2)
         {
          iFFVVF1 = iFFVVF;

          iFFVVF++;
          if (iFFVVF > 9)
            iFFVVF = 0;
         }
        else
         {
          iFFVVF1 = -1;
         }

        AssignLabel (ipFrameLabel + (iField / iNField), iFFVVF0, iFFVVF1);
       }
     }
   }

  free (ipMinFlagAlloc);

  return 0;
}  /* FindConsistentRun */

void CMetaData::AssignLabel (int *ipFrameLabel, int iFFVVF0, int iFFVVF1)
{

  if (iFFVVF0 == 0 && iFFVVF1 == 1)
   {
    *ipFrameLabel |= FL_aA;
   }
  else if (iFFVVF0 == 2 && iFFVVF1 == 3)
   {
    *ipFrameLabel |= FL_bB;
   }
  else if (iFFVVF0 == 4 && iFFVVF1 == 5)
   {
    *ipFrameLabel |= FL_cB;
   }
  else if (iFFVVF0 == 6 && iFFVVF1 == 7)
   {
    *ipFrameLabel |= FL_dC;
   }
  else if (iFFVVF0 == 8 && iFFVVF1 == 9)
   {
    *ipFrameLabel |= FL_dD;
   }

  return;
}  /* AssignLabel */
