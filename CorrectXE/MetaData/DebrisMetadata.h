#ifndef DEBRIS_METADATA_H
#define DEBRIS_METADATA_H

// Need to include a Microsoft SDK; e.g., C:\Program Files (x86)\Microsoft SDKs\Windows\v7.1A\Include
#include <Rpc.h>  // for UUID
#ifdef __BORLANDC__
// Won't work for 32 bit
#pragma comment(lib, "Rpcrt4.a")
#else
#pragma comment(lib, "Rpcrt4.lib")
#endif

#include <type_traits>

#define DEBRIS_CURRENT_VERSION 9

#define DEBRIS_FILE_HEADER_SIZE 1024

// constants to describe the channel and contrast of the debris
#define DEBRIS_PROCESS_TYPE_NONE     0x0
#define DEBRIS_PROCESS_TYPE_POS      0x1
#define DEBRIS_PROCESS_TYPE_NEG      0x2
#define DEBRIS_PROCESS_TYPE_R        0x4
#define DEBRIS_PROCESS_TYPE_G        0x8
#define DEBRIS_PROCESS_TYPE_B        0x10
#define DEBRIS_PROCESS_TYPE_Y        0x20
#define DEBRIS_PROCESS_TYPE_SPATIAL  0x40

#define DEBRIS_PROCESS_CHANNEL_MASK  0x3C
#define DEBRIS_PROCESS_CONTRAST_MASK 0x03

// constants for user data
#define DEBRIS_USER_FLAG_NONE            0x0
#define DEBRIS_USER_FLAG_FORCE_NO        0x1
#define DEBRIS_USER_FLAG_FORCE_YES       0x2
#define DEBRIS_USER_FLAG_RENDERED        0x4


#define DEBRIS_FILE_MAX_FEATURE 32

struct SDebrisSummary // metadata structure with summary information
{
   unsigned short usRowStart;
   unsigned short usColStart;
   unsigned short usRowCount;
   unsigned short usColCount;
   unsigned short usProcessSize;
   unsigned short usProcessType;
   unsigned short usUserFlag;
   unsigned short usFinalScore;
   unsigned short usContrastScore;

   // offset to the repair values
   int iFileOffset;
};

struct SDebrisAncillary // metadata structure with ancillary information
{
   // ID generated during processing
   int iProcessID;

   // Motion information of the best match
   short sMatchTimeA;
   short sMatchRowOffA;
   short sMatchColOffA;
   short sMatchTimeB;
   short sMatchRowOffB;
   short sMatchColOffB;

   // Motion information of the best repair
   short sRepairTimeA;
   short sRepairRowOffA;
   short sRepairColOffA;
   short sRepairTimeB;
   short sRepairRowOffB;
   short sRepairColOffB;

   // the features
   short sNFeature;
   short saFeature[DEBRIS_FILE_MAX_FEATURE];
};

#define AC_PROCESS_CONTRAST_POS  0    // bright dirt
#define AC_PROCESS_CONTRAST_NEG  1    // dark dirt
#define AC_PROCESS_CONTRAST_ERR  2    // error
#define DEBRIS_IS_DARK   AC_PROCESS_CONTRAST_NEG
#define DEBRIS_IS_BRIGHT AC_PROCESS_CONTRAST_POS

enum ERepairDisposition
{
   REJECT_UNKNOWN = 0,
   REJECT_DIRT_SIZE = 1,
   REJECT_CONTRAST = 2,
   REJECT_MATCHED_REJECT = 4
};

enum EPrepressStatus
{
   UNKNOWN = 0,
   KNOWN_ACCEPT,    // Green
   KNOWN_REJECT,
   WILL_ACCEPT,     // Cyan
   WILL_REJECT,     // Orange  or Red
   FORCED_ACCEPT,   // Magenta
   FORCED_REJECT,   // Yellow
   RENDERED,        // Blue
   NO_FIX,          // Black
   NO_FILTER,       // Black
   NO_ACTIVE_FILTER,   // Gray or White
};

struct SMetaDebris
{
   // This is just a wrapper around a guid with an index back into
   // the list and an frame number.
   SDebrisSummary *Summary = nullptr;
   SDebrisAncillary *Ancillary = nullptr;
   UUID Uuid = {0L, 0, 0, {0, 0, 0, 0, 0, 0, 0, 0}};
   int Index = -1;
   int Frame = -1;

   inline bool operator == (const SMetaDebris &metaDebris) const
   {
      return ((metaDebris.Uuid == Uuid) && (metaDebris.Index == Index));
   }

   inline bool isEqual(const SMetaDebris *metaDebris)
   {
      return (*this) == (*metaDebris);
   }

   inline int getId() const
   {
      return Index + 1;
   }

   int getContrast() const
   {
      auto contrastBit = Summary->usProcessType & DEBRIS_PROCESS_CONTRAST_MASK;
      switch (contrastBit)
      {
      case DEBRIS_PROCESS_TYPE_POS:
         return AC_PROCESS_CONTRAST_POS;

      case DEBRIS_PROCESS_TYPE_NEG:
         return AC_PROCESS_CONTRAST_NEG;

      default:
         return AC_PROCESS_CONTRAST_ERR;
      }
   }

   const UUID &getUuid() const {return Uuid; }

   bool isDirtBright() const  { return (getContrast() == AC_PROCESS_CONTRAST_POS); };
   bool isDirtDark() const    { return (getContrast() == AC_PROCESS_CONTRAST_NEG); };

   int getProcessSize() const    { return Summary->usProcessSize; }
   int getRowCount() const       { return Summary->usRowCount; }
   int getColCount() const       { return Summary->usColCount; }
   int getRowStart() const       { return Summary->usRowStart; }
   int getColStart() const       { return Summary->usColStart; }
   int getFinalScore() const     { return Summary->usFinalScore; }
   bool isSpatial() const        { return Summary->usProcessType & DEBRIS_PROCESS_TYPE_SPATIAL; }
   bool isRendered() const       { return Summary->usUserFlag & DEBRIS_USER_FLAG_RENDERED; }
   bool isForcedYes() const      { return Summary->usUserFlag & DEBRIS_USER_FLAG_FORCE_YES; }
   bool isForcedNo() const       { return Summary->usUserFlag & DEBRIS_USER_FLAG_FORCE_NO; }

   void setRenderedFlag()        { Summary->usUserFlag |= DEBRIS_USER_FLAG_RENDERED; }
   void clearRenderedFlag()      { Summary->usUserFlag &=  (~DEBRIS_USER_FLAG_RENDERED); }
   int getContrastScore() const  { return Summary->usContrastScore; }


   POINT getCenter() const
   {
      POINT center;
      center.x = getColStart() + getColCount() / 2;
      center.y = getRowStart() + getRowCount() / 2;
      return center;
   }
};

static_assert(sizeof(SMetaDebris) == 24 + 2*sizeof(nullptr), "Size of SMetaDebris is incorrect");
static_assert(std::is_trivially_copyable<SMetaDebris>::value, "SMetaDebris must be trivially_copyable");

#endif
