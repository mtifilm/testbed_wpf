#include "JobInfo.h"
#include "IniFile.h"

#include <algorithm>
using std::transform;

namespace
{
   const string JobIniFileName = JOB_INI_FILE_NAME;
   const string JobInfoSectionName = "JobInfo";
   const string JobNameKey = "Name";
   const string JobNicknameKey = "Nickname";
   const string JobNumberKey = "Number";
   const string JobReelCountKey = "ReelCount";
   const string JobIsASlashBKey = "ASlashB";
   const string JobIsCombinedKey = "Combined";
   const string JobIsLastReelAOnlyKey = "LastReelAOnly";
   const string JobHasDewarpFolderKey = "HasDewarpFolder";
   const string JobHasMasksFolderKey = "HasMasksFolder";
   const string JobHasPdlsFolderKey = "HasPdlsFolder";
   const string JobHasReportsFolderKey = "HasReportsFolder";
   const string JobHasCutListsFolderKey = "HasCutListsFolder";
   const string JobHasStabilizeFolderKey = "HasStabilizeFolder";
   const string JobMediaLocationKey = "MediaLocation";
   const string JobHasDrsMediaFolderKey = "HasDrsMediaFolder";
   const string JobHasUserMediaFolderKey = "HasUserMediaFolder";
   const string JobUserMediaFolderNameKey = "UserMediaFolderName";
};
//------------------------------------------------------------------------

JobInfo::JobInfo()
: Name("")
, Nickname("")
, Number(-1)
, ReelCount(0)
, IsASlashB(false)
, IsCombined(false)
, IsLastReelAOnly(false)
, HasDewarpFolder(true)
, HasMasksFolder(true)
, HasPdlsFolder(true)
, HasReportsFolder(true)
, HasCutListsFolder(true)
, HasStabilizeFolder(true)
, HasDrsMediaFolder(false)
, HasUserMediaFolder(false)
, MediaLocation("")
, UserMediaFolderName("")
{
}
//------------------------------------------------------------------------

// Copy constructor
JobInfo::JobInfo(const JobInfo &orig)
 : Name(orig.Name)
 , Nickname(orig.Nickname)
 , Number(orig.Number)
 , ReelCount(orig.ReelCount)
 , IsASlashB(orig.IsASlashB)
 , IsCombined(orig.IsCombined)
 , IsLastReelAOnly(orig.IsLastReelAOnly)
 , HasDewarpFolder(orig.HasDewarpFolder)
 , HasMasksFolder(orig.HasMasksFolder)
 , HasPdlsFolder(orig.HasPdlsFolder)
 , HasReportsFolder(orig.HasReportsFolder)
 , HasCutListsFolder(orig.HasCutListsFolder)
 , HasStabilizeFolder(orig.HasStabilizeFolder)
 , MediaLocation(orig.MediaLocation)
 , HasDrsMediaFolder(orig.HasDrsMediaFolder)
 , HasUserMediaFolder(orig.HasUserMediaFolder)
 , UserMediaFolderName(orig.UserMediaFolderName)
{
}
//------------------------------------------------------------------------

// = operator
JobInfo& JobInfo::operator=(const JobInfo &rhs)
{
   if (this == &rhs)
   {
      return *this;
   }

   Name = rhs.Name;
   Nickname = rhs.Nickname;
   Number = rhs.Number;
   ReelCount = rhs.ReelCount;
   IsASlashB = rhs.IsASlashB;
   IsCombined = rhs.IsCombined;
   IsLastReelAOnly = rhs.IsLastReelAOnly;
   HasDewarpFolder = rhs.HasDewarpFolder;
   HasMasksFolder = rhs.HasMasksFolder;
   HasPdlsFolder = rhs.HasPdlsFolder;
   HasReportsFolder = rhs.HasReportsFolder;
   HasCutListsFolder = rhs.HasCutListsFolder;
   HasStabilizeFolder = rhs.HasStabilizeFolder;
   MediaLocation = rhs.MediaLocation;
   HasDrsMediaFolder = rhs.HasDrsMediaFolder;
   HasUserMediaFolder = rhs.HasUserMediaFolder;
   UserMediaFolderName = rhs.UserMediaFolderName;
   return *this;
}
//------------------------------------------------------------------------

int JobInfo::LoadFrom(const string &newJobFolder)
{
   if (newJobFolder.empty())
   {
      return JOBINFO_ERR_NO_FOLDER_SPECIFIED;
   }

   JobFolder = newJobFolder;
   string jobIniFilePath = AddDirSeparator(JobFolder) + JobIniFileName;
   if (!DoesFileExist(jobIniFilePath))
   {
      return JOBINFO_ERR_INI_FILE_DOES_NOT_EXIST;
   }

   CIniFile *jobIniFile = CreateIniFile(jobIniFilePath, /* ReadOnlyFlag= */ true);
   if (jobIniFile == NULL)
   {
      return JOBINFO_ERR_INI_FILE_OPEN_FAIL;
   }

   Name                = jobIniFile->ReadString(JobInfoSectionName, JobNameKey, "");
   Nickname            = jobIniFile->ReadString(JobInfoSectionName, JobNicknameKey, "");
   Number              = jobIniFile->ReadInteger(JobInfoSectionName, JobNumberKey, -1);
   ReelCount           = jobIniFile->ReadInteger(JobInfoSectionName, JobReelCountKey, 0);
   IsASlashB           = jobIniFile->ReadBool(JobInfoSectionName, JobIsASlashBKey, false);
   IsCombined          = jobIniFile->ReadBool(JobInfoSectionName, JobIsCombinedKey, false);
   IsLastReelAOnly     = jobIniFile->ReadBool(JobInfoSectionName, JobIsLastReelAOnlyKey, false);
   HasDewarpFolder     = jobIniFile->ReadBool(JobInfoSectionName, JobHasDewarpFolderKey, false);
   HasMasksFolder      = jobIniFile->ReadBool(JobInfoSectionName, JobHasMasksFolderKey, false);
   HasPdlsFolder       = jobIniFile->ReadBool(JobInfoSectionName, JobHasPdlsFolderKey, false);
   HasReportsFolder    = jobIniFile->ReadBool(JobInfoSectionName, JobHasReportsFolderKey, false);
   HasCutListsFolder   = jobIniFile->ReadBool(JobInfoSectionName, JobHasCutListsFolderKey, false);
   HasStabilizeFolder  = jobIniFile->ReadBool(JobInfoSectionName, JobHasStabilizeFolderKey, false);
   HasDrsMediaFolder   = jobIniFile->ReadBool(JobInfoSectionName, JobHasDrsMediaFolderKey, false);
   HasUserMediaFolder  = jobIniFile->ReadBool(JobInfoSectionName, JobHasUserMediaFolderKey, false);
   MediaLocation       = jobIniFile->ReadString(JobInfoSectionName, JobMediaLocationKey, "");
   UserMediaFolderName = jobIniFile->ReadString(JobInfoSectionName, JobUserMediaFolderNameKey, "");

   DeleteIniFile(jobIniFile);

   return 0;
}
//------------------------------------------------------------------------

int JobInfo::Save() const
{
   if (JobFolder.empty())
   {
      return JOBINFO_ERR_NO_FOLDER_SPECIFIED;
   }

   if (!ValidateAll())
   {
      return JOBINFO_ERR_INVALID_JOB_INFO;
   }

   string jobIniFilePath = AddDirSeparator(JobFolder) + JobIniFileName;

   CIniFile *jobIniFile = CreateIniFile(jobIniFilePath);
   if (jobIniFile == NULL)
   {
      return JOBINFO_ERR_INI_FILE_OPEN_FAIL;
   }

   jobIniFile->WriteString(JobInfoSectionName, JobNameKey, Name);
   jobIniFile->WriteString(JobInfoSectionName, JobNicknameKey, Nickname);
   jobIniFile->WriteInteger(JobInfoSectionName, JobNumberKey, Number);
   jobIniFile->WriteInteger(JobInfoSectionName, JobReelCountKey, ReelCount);
   jobIniFile->WriteBool(JobInfoSectionName, JobIsASlashBKey, IsASlashB);
   jobIniFile->WriteBool(JobInfoSectionName, JobIsCombinedKey, IsCombined);
   jobIniFile->WriteBool(JobInfoSectionName, JobIsLastReelAOnlyKey, IsLastReelAOnly);
   jobIniFile->WriteBool(JobInfoSectionName, JobHasDewarpFolderKey, HasDewarpFolder);
   jobIniFile->WriteBool(JobInfoSectionName, JobHasMasksFolderKey, HasMasksFolder);
   jobIniFile->WriteBool(JobInfoSectionName, JobHasPdlsFolderKey, HasPdlsFolder);
   jobIniFile->WriteBool(JobInfoSectionName, JobHasReportsFolderKey, HasReportsFolder);
   jobIniFile->WriteBool(JobInfoSectionName, JobHasCutListsFolderKey, HasCutListsFolder);
   jobIniFile->WriteBool(JobInfoSectionName, JobHasStabilizeFolderKey, HasStabilizeFolder);
   jobIniFile->WriteBool(JobInfoSectionName, JobHasDrsMediaFolderKey, HasDrsMediaFolder);
   jobIniFile->WriteBool(JobInfoSectionName, JobHasUserMediaFolderKey, HasUserMediaFolder);
   jobIniFile->WriteString(JobInfoSectionName, JobMediaLocationKey, MediaLocation);
   jobIniFile->WriteString(JobInfoSectionName, JobUserMediaFolderNameKey, UserMediaFolderName);

   DeleteIniFile(jobIniFile);

   return 0;
}
//------------------------------------------------------------------------

int JobInfo::SaveTo(const string &newJobFolder) const
{
   JobInfo *_this = const_cast<JobInfo *>(this);
   _this->JobFolder = newJobFolder;
   return Save();
}
//------------------------------------------------------------------------

int JobInfo::ValidateName() const
{
   if (Name.empty())
   {
      return JOBINFO_ERR_INVALID_NAME;
   }

   // Anything else?
   return 0;
}
//------------------------------------------------------------------------

int JobInfo::ValidateNickname() const
{
   // TODO: Check for illegal characters... other constraints?
   if (Nickname.empty())
   {
      return JOBINFO_ERR_INVALID_NICKNAME;
   }

   // Anything else?
   return 0;
}
//------------------------------------------------------------------------

int JobInfo::ValidateMediaLocation() const
{
   // TODO: Check that folder exists
   if (MediaLocation.empty())
   {
      return JOBINFO_ERR_INVALID_MEDIA_LOCATION;
   }

   // Anything else?
   return 0;
}
//------------------------------------------------------------------------

bool JobInfo::ValidateAll() const
{
   // Larry sea all fields are optional except the Name!
   return ValidateName() == 0;
}
//------------------------------------------------------------------------

namespace
{
   // Avoiding standard stuff like 'toLower' because I'm afraid they use locale
   char toLarryFolderChar(char c)
   {
      if (c <= 'Z' && c >= 'A')
      {
         return c - ('Z' - 'z');
      }

      if ((c <= 'z' && c >= 'a') || (c <= '9' && c >= '0'))
      {
         return c;
      }

      return '_';
   }
}
//------------------------------------------------------------------------

string JobInfo::GetJobRootFolderName() const
{
   string sanitizedName = Name;
   transform(sanitizedName.begin(), sanitizedName.end(), sanitizedName.begin(), toLarryFolderChar);

   if (Number > 0)
   {
      char buffer[50];
      sprintf(buffer, "%s_%d", sanitizedName.substr(0, 32).c_str(), Number);
      sanitizedName = string(buffer);
   }

   return sanitizedName;
}
//------------------------------------------------------------------------

/* static */
bool JobInfo::IsAJobFolder(const string &folder)
{
   JobInfo jobInfo;
   if (jobInfo.LoadFrom(folder) != 0)
   {
      return false;
   }

   return jobInfo.ValidateAll();
}
//------------------------------------------------------------------------


