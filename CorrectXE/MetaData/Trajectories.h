#ifndef __CTRAJECTORIES_H__
#define __CTRAJECTORIES_H__

#include "MetaDataDLL.h"             // Export/Import Defs
#include <string>
using std::string;

#define TRAJ_ERR_TRAJECTORIES_NOT_LOADED -26000
#define TRAJ_ERROR_CANT_OPEN_TRAJ_FILE   -26001
#define TRAJ_ERROR_READ_FAILED           -26002

#define DEFAULT_TRAJ_BLOCK_SCALE_FACTOR 2  /* one traj for each 2x2 block */
#define DEFAULT_TRAJ_TEPORAL_RANGE 5       /* 5 past, 5 future = 10 total */

class MTI_METADATADLL_API CTrajectories
{
public:

   CTrajectories(const string &trajFilePath,
                 int width, int height,
                 int blockScaleFactor,
                 int temporalRange);
   ~CTrajectories();

   int SetFrameIndex(int newFrameIndex);
   int LoadTrajectories();
   int GetTrajectory(int temporalOffset,
                     int x, int y,
                     int &dx, int &dy);
   int GetAllTrajectoriesLinearized(int temporalOffset,
                                    long *trajOut,
                                    long &mostPositiveMotion,
                                    long &mostNegativeMotion);

private:

   string m_trajFilePath;
   string m_trajPathLeft, m_trajPathRight;
   size_t m_trajPathNumDigits;
   int m_width;
   int m_height;
   int m_blockScaleFactor;
   int m_temporalRange;

   int m_frameIndex;
   short *m_trajX, *m_trajY;
   float *m_weights;
   bool m_trajectoriesLoadedOK;
   bool m_pathIsATemplate;

   void cleanUp();
};

#endif // __CTRAJECTORIES_H__
