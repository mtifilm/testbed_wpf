/*
   File Name:  MetaDataDLL.h

   This just defines the export/import macros
   if MTI_METADATADLL is defined, exports are defined
   else imports are defined
*/

#ifndef METADATADLL_H
#define METADATADLL_H

//////////////////////////////////////////////////////////////////////
#include "machine.h"

#ifdef _WIN32                  // Window compilers need export/import

// Define Import/Export for windows
#ifdef MTI_METADATADLL
#define MTI_METADATADLL_API  __declspec(dllexport)
#else
#define MTI_METADATADLL_API  __declspec(dllimport)
#endif

//--------------------UNIX---------------------------
#else

// Needs nothing for SGI, UNIX, LINUX
#define MTI_METADATADLL_API
#endif   // End of UNIX

#endif   // Header file


