/*
	file:	FrameDiff.h
	by:	Kevin Manbeck
	date:	Jan 15, 2001

	This file contains functions used to calculate the
	difference between two images.

	The following types of differences are computed here
		1.  sub-sampled field differences.  This can be
		used to determine 3:2 cadence.
		2.  sub-sampled frame difference.  This can be
		used to determine location of cuts.
		3.  full frame difference based on a threshold.
		This can be used for dub-checking.

	Here is a list of functions in this class:

	---------------------------------------
	CFrameDiff (const CImageFormat *ifpArg);
	---------------------------------------
	The constructor.
		ifpArg is the image format of the clip

	--------------
  	~CFrameDiff ();
	--------------
	The destructor.

	-----------------------------------
	setCalculationPerSecond (int iArg);
	-----------------------------------
	[OPTIONAL] to control behavior of SubSampleDiff()
	Sets the number of calculations per second.
		iArg is the desired number of calculations per second.

	Returns 0 on success.

	--------------------------------
	setInnerRectPercent (float fArg);
	--------------------------------
	[OPTIONAL] to control behavior of SubSampleDiff()
	Noise at the edge of the picture can affect difference calculations.
	Use this function to control how much of the picture is examined.
		fArg is the desired amount.  Range:  0. -- 1.

	Returns 0 on success.

	--------------------------
	setDiffThresh (float fArg);
	--------------------------
	[OPTIONAL] to control behavior of FullFrameDiff()
	Sets the threshold at which a difference is judged to be consequential.
	Some differences are so small that they should not be detected.
		fArg is the desired threshold.  Range:  0. -- 1.

	Returns 0 on success.

	-----------------------------------------------------------
	SubSampleDiff (MTI_UINT8 **ucpCurr, MTI_UINT8 **ucpPrev,
		CMetaData *mdpDiff, int iFrame);
	-----------------------------------------------------------
	This function calculates two types of difference:  field difference
	values (used for 3:2 detection) and frame difference values (used
	for cut detection).  It is done via sub-sampling the block data.
		ucpCurr - pointers to current frame data
		ucpPrev - pointers to previous frame data
		mdpDiff	- a meta data structure for difference statistics
		iFrame	- index of current frame

		If ucpPrev is NULL, difference values will be set to
		NULL state.

	Returns 0 on success.

	Also see:
		setCalculationPerSecond ()
		setInnerRectPercent ()

	---------------------------------------------------
	int FullFrameDiff (MTI_UINT8 **ucpA, MTI_UINT8 **ucpB, long *lpDiffLoc) 
	---------------------------------------------------
	This function calculates the difference between two frames.  It
	is suitable for use by the dub check software.  Any difference 
	detected by this function is reported back to the caller via the
	DifferenceLocation value.  Return value takes values from DL_*.
		ucpA - pointers to one frame of data
		ucpB - pointers to another frame of data

	Returns 0 when there are no errors

	Also see:
		setDiffThresh ()


	-------------------------------------------------------
	void CopyFields (const CFrameDiff &fdRef0, int iField0, 
		const CFrameDiff &fdRef1, int iField1)
	-------------------------------------------------------
	This function is used to manipulate 3:2 pulldown in the difference
	statistics.

	It is used to copy difference values from two different frames
	and merge them together into a single frame.
	

	--------------------------------------------------------------
	void ZeroFields (const CFrameDiff &fdRef0, int iField0, 
		const CFrameDiff &fdRef1, int iField1, int iFieldZero)
	--------------------------------------------------------------
	This function is used to manipulate 3:2 pulldown in the difference
	statistics.

	It is used to copy difference values from two different frames
	and merge them together into a single frame.

	Furthermore, it will set one of the field difference values to
	0 as happens with repeated fields in 3:2 pulldown.
	
*/

#ifndef FRAMEDIFFH
#define FRAMEDIFFH

#include "MetaDataDLL.h"

#include "machine.h"
#include "mthread.h"

	// how much of the image to use for subsampling.
#define DEFAULT_INNER_RECT_PERCENT .9

	// how many sub-sampled pixels to use
#define DEFAULT_CALCULATION_PER_SECOND 1000000

	// indication of location of difference
#define DL_NONE 0x0
#define DL_ULC 0x1
#define DL_URC 0x2
#define DL_LLC 0x4
#define DL_LRC 0x8

//  Forward Declarations
class CImageFormat;
class CMetaData;

class MTI_METADATADLL_API CFrameDiff
 {
public:
  CFrameDiff (const CImageFormat *ifpArg);
  ~CFrameDiff ();

  int setCalculationPerSecond (int iArg);
  int setCalculationPerSecond (float fArg);
  int setInnerRectPercent (float fArg);
  int setDiffThresh (float fArg);

  int SubSampleDiff (MTI_UINT8 **ucpCurr, MTI_UINT8 **ucpPrev,
		FRAME_DIFF *fdp);
  int SubSampleDiff (MTI_UINT8 **ucpCurr, MTI_UINT8 **ucpPrev,
		CMetaData *mdpDiff, int iFrame);
  int FullFrameDiff (MTI_UINT8 **ucpA, MTI_UINT8 **ucpB, long *lpDiffLoc);
  void CopyFields (FRAME_DIFF &fdResult, const FRAME_DIFF &fdRef0, int iField0,
		const FRAME_DIFF &fdRef1, int iField1);
  void ZeroFields (FRAME_DIFF &rdResult, const FRAME_DIFF &fdRef0, int iField0,
		const FRAME_DIFF &fdRef1, int iField1, int iFieldZero);

private:
/*
	General purpose values
*/
  float
    fFrameRate;		// number of frames per second

  long
    lPixelComponent,	// pixel components, see EPixelComponent
    lPixelPacking,	// pixel packing of data, see EPixelPacking
    lNField,		// number of fields per frame
    lNRow,		// number rows per image
    lNCol;		// number columns per image

  long
    lDiffThresh;

  MTHREAD_STRUCT
    tsSubSample,	// multi-thread used for sub-sample code
    tsFullFrame;	// multi-thread used for full frame

  MTI_UINT8
    *ucpCurr[2],
    *ucpPrev[2];

  void
    *vpMetaDataRecord;	// pointer to meta data record

/*
	Values specific to block sub-sampling
*/

		// block values used for sub-sample difference
  long
    laBlockStart[2][FRAME_DIFF_NUM_ROW_BLOCK][FRAME_DIFF_NUM_COL_BLOCK],
    lNPixelPerBlock,
    *lpBlockPixel;

  long
    lCalculationPerSecond;	// number of calculations per second

  float
    fInnerRectPercent;	// percent of image to use in order to avoid
			// edge effects.

  long
    lDifferenceLocation;

  MTI_UINT8 
    *ucpDataA[2],
    *ucpDataB[2];

		// functions
  int SubSampleLookup();
  int DataOffset (long lFieldRow, long lFieldCol, long &lOffset);
  void CalcSubSampleDiff (int iJob);
  void CalcFullFrameDiff (int iJob);

  friend void CallCalcSubSampleDiff (void *vp, int iJob);
  friend void CallCalcFullFrameDiff (void *vp, int iJob);
 };

#endif // #ifndef FRAMEDIFF_H
