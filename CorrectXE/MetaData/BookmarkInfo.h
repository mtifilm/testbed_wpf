#ifndef BOOKMARKINFOH
#define BOOKMARKINFOH

#include "IniFile.h"      // Ugggh... for StringList!!
#include "MetaDataDLL.h"
#include "timecode.h"

#include <string>
using std::string;
#include <set>
using std::set;

struct MTI_METADATADLL_API BookmarkInfo
{
   string    User;
   string    Date;
   string    ReelNumber;
   CTimecode InTimecode;
   CTimecode OutTimecode;
   string    DefectType;
   string    DefectLocation;
   string    Artist;
   string    Completed;
   string    QcBy;
   string    QcDate;
   string    Note;

   BookmarkInfo();
   BookmarkInfo(CTimecode newInTimecode);
   BookmarkInfo(const BookmarkInfo &orig);
   ~BookmarkInfo();
   BookmarkInfo& operator=(const BookmarkInfo &rhs);

   int LoadFrom(const string &bookmarkIniFilePath, int frameIndex, const CTimecode &tcProto);
   int SaveTo(const string &bookmarkIniFilePath, int frameIndex) const;
   void Clear();

   static void GetCsvHeaderFields(StringList &headerFields);
   void GetCsvRowFields(StringList &rowFields);

   static int Delete(const string &bookmarkIniFilePath, int frameIndex);
   static set<int> GetSetOfBookmarkFrameIndices(const string &bookmarkIniFilePath);
   static BookmarkInfo *Create(const StringList &fieldNames, const StringList &fieldValues, const CTimecode &tcProto);

private:

   static set<int> BookmarkLocationCache;
   static string BookmarkLocationCachedFile;
};

#define BOOKMARKINFO_ERR_INVALID_USER            -19601
#define BOOKMARKINFO_ERR_NO_INI_FILE_SPECIFIED   -19602
#define BOOKMARKINFO_ERR_INI_FILE_DOES_NOT_EXIST -19603
#define BOOKMARKINFO_ERR_INI_FILE_OPEN_FAIL      -19604
#define BOOKMARKINFO_ERR_IN_TIMECODE_NOT_SET     -19605
#define BOOKMARKINFO_INDEX_OUT_OF_RANGE          -19606
#define BOOKMARKINFO_NO_BOOKMARK_AT_THIS_FRAME   -19607


#endif
