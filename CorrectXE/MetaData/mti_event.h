
/*
	Flags to signify differ frame-based events
*/

#define EVENT_NONE            0x0000000000000000  // no event

// NOTE: AUTO and ADD are now SYNONYMOUS!! They were originally supposed
//       to be colored differently on the timeline, but now they're both green!
#define EVENT_CUT_AUTO        0x0000000000000001  // automatically detected cut
#define EVENT_CUT_ADD         0x0000000000000002  // operator specified cut
#define EVENT_CUT_MASK        (EVENT_CUT_AUTO | EVENT_CUT_ADD)

#define EVENT_CADENCE_BREAK   0x0000000000000008  // break in 3:2 cadence
#define EVENT_CADENCE_AA      0x0000000000000010  // AA frame
#define EVENT_CADENCE_BB      0x0000000000000020  // BB frame
#define EVENT_CADENCE_BC      0x0000000000000040  // BC frame
#define EVENT_CADENCE_CD      0x0000000000000080  // CD frame
#define EVENT_CADENCE_DD      0x0000000000000100  // DD frame
#define EVENT_CADENCE_VIDEO   0x0000000000000200  // video cadence
#define EVENT_CADENCE_ORPHAN  0x0000000000000400  // orphaned field
#define EVENT_CADENCE_USER    0x0000000000000800  // user specified cadence
#define EVENT_CADENCE_MASK    (EVENT_CADENCE_BREAK | EVENT_CADENCE_AA | EVENT_CADENCE_BB \
                              | EVENT_CADENCE_BC | EVENT_CADENCE_CD | EVENT_CADENCE_DD \
                              | EVENT_CADENCE_VIDEO | EVENT_CADENCE_ORPHAN | EVENT_CADENCE_USER)

#define EVENT_BOOKMARK        0x0000000000001000  // user specified mark
#define EVENT_MARK_IN         0x0000000000002000  // Mark In
#define EVENT_MARK_OUT_EXCL   0x0000000000004000  // Mark Out - EXCLUSIVE
#define EVENT_MARK_OUT_INCL   0x0000000000008000  // Mark Out - INCLUSIVE
#define EVENT_MARK_MASK       (EVENT_BOOKMARK | EVENT_MARK_IN | EVENT_MARK_OUT_EXCL | EVENT_MARK_OUT_INCL)

#define EVENT_PANDS_STATIC    0x0000000000010000  // Static PANDS event
#define EVENT_PANDS_DYNAMIC   0x0000000000020000  // Dynamic PANDS event
#define EVENT_PANDS_MASK      (EVENT_PANDS_STATIC | EVENT_PANDS_DYNAMIC)

#define EVENT_KEYFRAME        0x0000000000040000  // Keyframe location
#define EVENT_KEYFRAME_MASK   (EVENT_KEYFRAME)

#define EVENT_AUD_BOUNDARY    0x0000000000100000  // Audio shot boundary
#define EVENT_AUD_CLAP        0x0000000000200000  // Audio clap point
#define EVENT_AUD_TRIM_IN     0x0000000000400000  // Audio Trim in (INCLUSIVE)
#define EVENT_AUD_TRIM_OUT    0x0000000000800000  // Audio Trim out (EXCLUSIVE)
#define EVENT_AUD_CLAP_OTHER  0x0000000001000000  // clap for another camera
#define EVENT_AUD_LTC_BREAK   0x0000000002000000  // break in the LTC value
#define EVENT_AUD_MASK        (EVENT_AUD_BOUNDARY | EVENT_AUD_CLAP | EVENT_AUD_TRIM_IN \
                              | EVENT_AUD_TRIM_OUT | EVENT_AUD_CLAP_OTHER | EVENT_AUD_LTC_BREAK)

#define EVENT_PIC_BOUNDARY    0x0000000010000000  // Picture shot boundary
#define EVENT_PIC_CLAP        0x0000000020000000  // Picture clap point
#define EVENT_PIC_TRIM_IN     0x0000000040000000  // Picture Trim in (INCLUSIVE)
#define EVENT_PIC_TRIM_OUT    0x0000000080000000  // Picture Trim out(EXCLUSIVE)
#define EVENT_PIC_MASK        (EVENT_PIC_BOUNDARY | EVENT_PIC_CLAP | EVENT_PIC_TRIM_IN | EVENT_PIC_TRIM_OUT)

#define EVENT_REF_DIRTY_FRAME 0x0000000100000000  // Frame is dirty
#define EVENT_REF_MASK        (EVENT_REF_DIRTY_FRAME)

#define EVENT_CB_BREAK        0x0000001000000000  // Color breathing break
#define EVENT_CB_REFERENCE    0x0000002000000000  // Color breathing reference frame
#define EVENT_CB_MASK         (EVENT_CB_BREAK | EVENT_CB_REFERENCE)  // Color breathing mask

// events that can occur on an audio or a video time line
#define EVENT_COMMON_MASK (EVENT_CUT_MASK | EVENT_MARK_MASK | EVENT_CB_MASK)

// events that can occur on an audio time line
#define EVENT_AUDIO_MASK (EVENT_COMMON_MASK | EVENT_AUD_MASK)

// events that can occur on a video time line
#define EVENT_VIDEO_MASK (EVENT_COMMON_MASK | EVENT_CADENCE_MASK |  EVENT_PANDS_MASK | EVENT_PIC_MASK)

// events that can occur on a reference timeline
#define EVENT_REFERENCE_MASK (EVENT_REF_MASK)

/*
	Multi frame events
*/
#define MF_EVENT_NONE         0x0000000000000000  // no event

#define MF_EVENT_LOG_AST      0x0000000000000001  // Audio scene/take 
#define MF_EVENT_LOG_PST      0x0000000000000002  // Picture scene/take 
#define MF_EVENT_LOG_MASK     0x000000000000000F  // mask for logger events

#define MF_EVENT_PROC_AF      0x0000000000000010  // processed by auto filter
#define MF_EVENT_PROC_GR      0x0000000000000020  // processed by grain
#define MF_EVENT_PROC_FL      0x0000000000000040  // processed by flicker
#define MF_EVENT_PROC_AC      0x0000000000000080  // processed by aper. corr.
#define MF_EVENT_PROC_STAB    0x0000000000000100  // processed by stab.
#define MF_EVENT_PROC_MASK    0x0000000000000FF0  // mask for processing

#define MF_EVENT_PANDS_STATIC  0x0000000000001000  // static PANDS range
#define MF_EVENT_PANDS_DYNAMIC 0x0000000000002000  // dynamic PANDS range
#define MF_EVENT_PANDS_MASK    0x0000000000003000  // mask for PANDS

#define MF_EVENT_MARKED_RANGE  0x0000000000004000  // marked range
#define MF_EVENT_MARKED_MASK   0x0000000000004000  // mask for marked range

#define MF_EVENT_DIRTY_FRAME   0x0000000000008000  // mask for dirty frames

