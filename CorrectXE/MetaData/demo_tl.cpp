
#include <stdlib.h>
#include <stdio.h>
#include "ClipAPI.h"
#include "TimeLine.h"
#include "getopt.h"
#include "IniFile.h"
#include "PanAndScan.h"
#include "SMPTETimecode.h"

#include <stdio.h>

void Usage (char *cpArg)
{
  printf ("Usage:  %s <-A clip name> [-c]\n", cpArg);
  printf ("-c is used to create synthetic track files\n");
}

int main (int iArgC, char **cpArgV)
{
  int iRet, iOpt;
  CTimeLine tl;
  string strClipName;

  CreateApplicationName(cpArgV[0]);

  if (iArgC < 2)
   {
    Usage (cpArgV[0]);
    return -1;
   }

  // Initialize access to read/write media
  if (CMediaInterface::initialize())
   {
    TRACE_0 ("Unable to initialize media access");
    return -1;
   }

  bool bCreateNew = false;
  while ( (iOpt = getopt (iArgC, cpArgV, "A:c")) != EOF)
   {
    switch (iOpt)
     {
      case 'A':
        strClipName = optarg;
      break;
      case 'c':
        bCreateNew = true;
      break;
      default:
        Usage (cpArgV[0]);
        return -1;
     }
   }

  iRet = tl.setClipName (strClipName);
  if (iRet)
   {
    char caMessage[128];
    sprintf (caMessage, "Unable to set clip name: %s  reason: %d\n",
            strClipName.c_str(), iRet);
    TRACE_0 (errout << caMessage);
    return -1;
   }

  if (bCreateNew)
   {
    // this is hard-coded to treat 10 shots, each with 275 frames.

    MTI_UINT8 *ucpHD[2], *ucpSD[2];
    CBinManager bmBinMgr;
    int iRet;
    ClipSharedPtr clip;
    CVideoFrameList *vflpHD, *vflpSD;
    CPanAndScan *pasp;

    clip = bmBinMgr.openClip (strClipName, &iRet);
    if (iRet)
     {
      fprintf (stderr, "FATAL ERROR at line %d\n", __LINE__);
      exit (1);
     }

    vflpHD = clip->getVideoFrameList (0, FRAMING_SELECT_VIDEO);
    vflpSD = clip->getVideoFrameList (1, FRAMING_SELECT_VIDEO);

    pasp = new CPanAndScan (
	vflpHD->getImageFormat()->getPixelsPerLine(),
	vflpHD->getImageFormat()->getLinesPerFrame(),
	vflpHD->getImageFormat()->getBitsPerComponent(),
	vflpHD->getImageFormat()->getInterlaced(),
	vflpSD->getImageFormat()->getPixelsPerLine(),
	vflpSD->getImageFormat()->getLinesPerFrame(),
	vflpSD->getImageFormat()->getBitsPerComponent(),
	vflpSD->getImageFormat()->getInterlaced(),
  iRet);
    if (iRet)
     {
      fprintf (stderr, "FATAL ERROR at line %d\n", __LINE__);
      exit (1);
     }

    pasp->setSrcWindow (240, 0, 1680, 0, 1680, 1080, 240, 1080);
    pasp->setDstWindow (0,0,720,486);

    if (vflpHD == 0 || vflpSD == 0)
     {
      fprintf (stderr, "FATAL ERROR at line %d\n", __LINE__);
      exit (1);
     }

    for (int iField = 0; iField < 2; iField++)
     {
      ucpHD[iField] = (MTI_UINT8 *) malloc (vflpHD->
		getMaxPaddedFieldByteCount());
      ucpSD[iField] = (MTI_UINT8 *) malloc (vflpSD->
		getMaxPaddedFieldByteCount());

      if (ucpHD[iField] == NULL || ucpSD[iField] == NULL)
       {
        fprintf (stderr, "FATAL ERROR at line %d\n", __LINE__);
        exit (1);
       }
     }

    // clear all LTC breaks
    for (int iFrame = 0; iFrame < 2750; iFrame++)
     {
      tl.deleteEvent (iFrame, EVENT_LTC_BREAK);
     }

    tl.addEvent (16, EVENT_LTC_BREAK);
    tl.addEvent (291, EVENT_LTC_BREAK);
    tl.addEvent (567, EVENT_LTC_BREAK);
    tl.addEvent (840, EVENT_LTC_BREAK);
    tl.addEvent (1116, EVENT_LTC_BREAK);
    tl.addEvent (1391, EVENT_LTC_BREAK);
    tl.addEvent (6*275+16, EVENT_LTC_BREAK);
    tl.addEvent (7*275+16, EVENT_LTC_BREAK);
    tl.addEvent (8*275+16, EVENT_LTC_BREAK);
    tl.addEvent (9*275+16, EVENT_LTC_BREAK);

    // put different key kode values on every frame
    CTimeTrack *ttpKeykode = clip->getTimeTrack (0);
    int iFrame=16;
    for (int iShot = 0; iShot < 6; iShot++)
     {
      for (int iCnt = 0; iCnt < 275; iCnt++)
       {
        CKeykodeFrame kf;
        int iPerf;
        char caMessage[64];
        if (iCnt == 0)
         {
          if (iShot == 0)
            iPerf = 1200 * 16 * 4 + 5 * 4 + 2;
          else if (iShot == 1)
            iPerf = 1805 * 16 * 4 + 15 * 4 + 1;
          else if (iShot == 2)
            iPerf = 2540 * 16 * 4 + 2 * 4 + 3;
          else if (iShot == 3)
            iPerf = 4970 * 16 * 4 + 8 * 4 + 0;
          else if (iShot == 4)
            iPerf = 8224 * 16 * 4 + 10 * 4 + 2;
         }
        else
         {
          iPerf += 4;
         }

        sprintf (caMessage, "0200329095%.4d%.2d", iPerf/64, iPerf%64);
        ttpKeykode->getKeykodeFrame(iFrame)->setKeykode(caMessage);
        iFrame++;
       }
     }

    // put different LTC values on every frame
    CTimeTrack *ttpAudioLTC = clip->getTimeTrack (1);
    iFrame=16;
    CSMPTETimecodeFrame *tfp = ttpAudioLTC->getSMPTETimecodeFrame(0);
    CTimecode tc = tfp->getTimecode(30);
    for (int iShot = 0; iShot < 10; iShot++)
     {
      for (int iCnt = 0; iCnt < 275; iCnt++)
       {
        CSMPTETimecodeFrame *tfp = ttpAudioLTC->getSMPTETimecodeFrame(iFrame);
        if (iCnt == 0)
         {
          if (iShot == 0)
            tc.setTimeASCII("01:00:32:05");
          else if (iShot == 1)
            tc.setTimeASCII("01:01:02:07");
          else if (iShot == 2)
            tc.setTimeASCII("01:02:42:05");
          else if (iShot == 3)
            tc.setTimeASCII("01:03:11:15");
          else if (iShot == 4)
            tc.setTimeASCII("01:04:15:13");
          else if (iShot == 5)
            tc.setTimeASCII("01:05:22:11");
          else if (iShot == 6)
            tc.setTimeASCII("01:06:01:07");
          else if (iShot == 7)
            tc.setTimeASCII("01:07:45:01");
          else if (iShot == 8)
            tc.setTimeASCII("01:08:32:12");
          else if (iShot == 9)
            tc.setTimeASCII("01:09:05:14");
         }
        else
         {
          tc.setAbsoluteTime (tc.getAbsoluteTime() + 1);
         }

        CSMPTETimecode stc;
        stc.setTimecode (tc);
        
        float fPartialFrame;
        switch (iFrame%4)
         {
          case 0:
            fPartialFrame = 0.0;
          break;
          case 1:
            fPartialFrame = 0.25;
          break;
          case 2:
            fPartialFrame = -.50;
          break;
          case 3:
            fPartialFrame = -.25;
          break;
         }
        ttpAudioLTC->getSMPTETimecodeFrame(iFrame)->setSMPTETimecode (stc);
        ttpAudioLTC->getSMPTETimecodeFrame(iFrame)->setPartialFrame
		(fPartialFrame);
        iFrame++;
       }
     }

#ifdef TTT
    // create the proxy images
    for (int iShot = 0; iShot < 10; iShot++)
     {
      for (int iCnt = 0; iCnt < 275; iCnt++)
       {
        int iFrame = iShot * 275 + iCnt + 16;
        if (iFrame%20 == 0)
          printf ("TTT doing frame %d\n", iFrame);

        if (iShot < 6)
         {
          // read in the HD frame
          iRet = vflpHD->readMediaVisibleFields (iFrame, ucpHD);
          if (iRet)
           {
            fprintf (stderr, "FATAL ERROR at line %d\n", __LINE__);
            exit (1);
           }
         }
        else
         {
          // set HD frame to BLACK
          for (int iField = 0; iField < 2; iField++)
          for (MTI_UINT32 iByte = 0; iByte < vflpHD->getMaxPaddedFieldByteCount();
                iByte++)
           {
            ucpHD[iField][iByte] = 128;
           }
         }

        // convert to SD
        pasp->convert (ucpHD, ucpSD);

        // write out SD
        iRet = vflpSD->writeMediaVisibleFields (iFrame, ucpSD);
        if (iRet)
         {
          fprintf (stderr, "FATAL ERROR at line %d\n", __LINE__);
          exit (1);
         }
       }
     }
#endif

    tl.updateEventFile ();
    clip->updateAllTrackFiles();
   }
  else
   {
    // print out information contained in the tracks

    for (int iFrame = 0; iFrame < tl.getTotalFrameCount(); iFrame++)
     {
      bool bCutFlag;
      int iEvent = tl.getEventFlag(iFrame);
      if (iEvent & EVENT_CUT_MASK)
        bCutFlag = true;
      else
        bCutFlag = false;
      printf ("f: %5d kk: %s LTC: %s event: 0x%x cut: %d\n", iFrame,
        tl.getTelecineKeykode(iFrame).c_str(),
        tl.getAudioLTC(iFrame).c_str(),
        iEvent, bCutFlag);

      if (bCutFlag)
       {
        printf ("\n* * * * * * * * * * * * *\n\n");
       }
     }
   }

  return 0;
}  /* main */
