//---------------------------------------------------------------------------

#pragma hdrstop

#include "AutoCleanRenderHistory.h"

#include <mmsystem.h>
#include <iostream>
#include <fstream>

#define FIX_HISTORY_MAGIC_WORD mmioStringToFOURCC("ACFH", 0)

CAutoCleanRenderHistory::CAutoCleanRenderHistory()
{
}

CAutoCleanRenderHistory::~CAutoCleanRenderHistory()
{
	if (_dirty)
    {
    	try
    	{
           Save();
        }
        catch (const exception &ex)
        {
            TRACE_0(errout << __func__ << " Closed failed because " << ex.what());
        }
        catch (...)
        {
        	TRACE_0(errout << __func__ << " Close failed");
        }
    }
}

CAutoCleanRenderHistory::CAutoCleanRenderHistory(const string &fixHistoryFilePath, int frameIndex)
{
  	Load(fixHistoryFilePath, frameIndex);
}

void CAutoCleanRenderHistory::Load(const string &fixHistoryFilePath, int frameIndex)
{
	_debrisIds.clear();
	_frameIndex = frameIndex;
	Merge(fixHistoryFilePath);
    _fixHistoryFilePath = fixHistoryFilePath;
    _historyFileExists = true;
    _dirty = false;
}

void CAutoCleanRenderHistory::Merge(const string &fixHistoryFilePath)
{
	return;
	auto fixFileName = fixHistoryFilePath;
	if (fixHistoryFilePath.empty())
    {
    	fixFileName = _fixHistoryFilePath;
    }

	ifstream fixStream(fixFileName, ios::in | ios::binary);
    if (fixStream.is_open() == false)
    {
    	// no file is not an error, just no fixes
    	return;
    }

    // We are interested in minimizing io speed, so blast it in
    FOURCC magicNumber;
    fixStream.read((char *)&magicNumber, sizeof(FOURCC));

    if (magicNumber != FIX_HISTORY_MAGIC_WORD)
    {
        throw EDebrisFileException("Invalid history fix file, bad magic number\n"  + fixFileName);
    }

	unsigned int size;
    fixStream.read((char *)&size, sizeof(size));

	vector<int> fixes(size);
    auto dataSize = size*sizeof(int);
    fixStream.read((char *)fixes.data(), dataSize);
    if (dataSize != fixStream.gcount())
    {
       throw EDebrisFileException("Invalid history fix file, file is too small\n" + fixFileName);
    }

    // Don't have to do this but it keeps us from reading the file
    fixStream.close();

    // Merge it in
    Merge(fixes);
}

void CAutoCleanRenderHistory::Save(const string &fixHistoryFilePath)
{
	return;
	auto fixFileName = fixHistoryFilePath;
	if (fixHistoryFilePath.empty())
    {
    	fixFileName = _fixHistoryFilePath;
    }

    // Do nothing if empty
    if (_fixHistoryFilePath.empty())
    {
    	return;
    }

 	ofstream fixStream(fixFileName, ios::out | ios::binary);
    if (fixStream.is_open() == false)
    {
       throw EDebrisFileException("Unable to open history fix file for write '" + fixFileName + "'");
    }

 	fixStream.exceptions(std::ofstream::failbit | std::ofstream::badbit );

    // In theory there cannot be duplicates, however just to be safe
	auto uniqueIter = std::unique(_debrisIds.begin(), _debrisIds.end());
    MTIassert(std::distance(_debrisIds.begin(), uniqueIter) == std::distance(_debrisIds.begin(), _debrisIds.end()));
	_debrisIds.resize(std::distance(_debrisIds.begin(), uniqueIter));

    FOURCC magicNumber = FIX_HISTORY_MAGIC_WORD;
    fixStream.write((char *)&magicNumber, sizeof(FOURCC));

    int size = _debrisIds.size();
    fixStream.write((char *)&size, sizeof(size));
    fixStream.write((char *)_debrisIds.data(), size*sizeof(int));

    _dirty = false;
}

void CAutoCleanRenderHistory::Merge(const vector<int> &fixes)
{
	for (auto id : fixes)
	{
    	if (find(_debrisIds.begin(), _debrisIds.end(), id) == _debrisIds.end())
		{
           _dirty = true;
           _debrisIds.push_back(id);
        }
	}

    sort(_debrisIds.begin(), _debrisIds.end());
}

void CAutoCleanRenderHistory::Clear()
{
	_debrisIds.clear();
    return;
    if (_fixHistoryFilePath.empty())
    {
    	return;
    }

    if (!DeleteFile(_fixHistoryFilePath.c_str()))
    {
    	TRACE_0(errout << "Failed to delete fix history file " << _fixHistoryFilePath);
    	MTIassert(false);
    }
}

int CAutoCleanRenderHistory::TotalSize()
{
	return sizeof(_debrisIds);
}
//---------------------------------------------------------------------------
#pragma package(smart_init)
