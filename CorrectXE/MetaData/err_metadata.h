#ifndef ERRMETADATA_H
#define ERRMETADATA_H

/*

Alpha Order:

ERR_SQL_ALREADY_EXISTS	    - the requested scene/take already exists
ERR_SQL_CAPTURE_IN_PROGRESS - video is already being captured
ERR_SQL_CLIP_NOT_IN_DATABASE- the requested clip is not in database
ERR_SQL_EVENT_MISMATCH      - fatal scene/take mismatch
ERR_SQL_EVENT_REPEAT        - fatal scene/take duplication
ERR_SQL_FAILURE_CONNECT     - mySQL returned a failure on call to connect
ERR_SQL_FAILURE_QUERY	    - mySQL returned a failure on call to query
ERR_SQL_FOUND_MORE_ENTRIES  - found more entries than were expected
ERR_SQL_FOUND_NULL_ENTRY    - found a NULL entry when non-NULL was expected
ERR_SQL_INVALID_GUID	    - the GUID carried by the clip is not valid
ERR_SQL_MULTIPLE_GUID	    - the sql database has more than one entry with
				same GUID value
ERR_SQL_NEED_ADD            - an sql entry needs to be added back
ERR_SQL_NEED_MERGE          - an sql entry needs to be merged
ERR_SQL_NEED_OVERWRITE      - need to overwrite an existing entry
ERR_SQL_NO_DATABASE         - no database entry found in MTILocalMachine.ini
ERR_SQL_NO_HOST             - no host entry found in MTILocalMachine.ini
ERR_SQL_NO_RESULT	    - a send command produced no results
ERR_SQL_NULL_CONNECTION     - connection to mySQL is NULL
ERR_SQL_OPEN_INI_FILE       - not able to open the MTILocalMachine.ini file
ERR_SQL_OUT_OF_BOUNDS_COL   - col request is out of bounds
ERR_SQL_OUT_OF_BOUNDS_ROW   - row request is out of bounds
ERR_SQL_RECV_BOOL           - unable to receive boolean
ERR_SQL_RECV_INT            - unable to receive integer
ERR_SQL_REEL_WILL_HAVE_GAP  - a gap will exist on the resulting reel
ERR_SQL_TIME_STAMP          - the mySQL time stamp mechanism appears stuck
ERR_SQL_UNKN_MODE           - the logger mode is not supported

ERR_TIMELINE_MALLOC	    - unable to allocate sufficient storage
ERR_TIMELINE_CLIPNAME	    - A problem with the clip name
ERR_TIMELINE_OPENCLIP	    - Unable to open the clip

*/

// allowed range for error values:  -5250 -- -5349

#define ERR_SQL_OPEN_INI_FILE			-5250
#define ERR_SQL_NO_DATABASE			-5251
#define ERR_SQL_NULL_CONNECTION			-5252
#define ERR_SQL_FAILURE_CONNECT			-5253
#define ERR_SQL_FAILURE_QUERY			-5254
#define ERR_SQL_NO_HOST				-5255
#define ERR_SQL_NO_RESULT			-5256
#define ERR_SQL_OUT_OF_BOUNDS_ROW		-5257
#define ERR_SQL_OUT_OF_BOUNDS_COL		-5258
#define ERR_SQL_RECV_INT			-5259
#define ERR_SQL_FOUND_NULL_ENTRY		-5260
#define ERR_SQL_RECV_BOOL			-5261
#define ERR_SQL_EVENT_MISMATCH			-5262
#define ERR_SQL_CLIP_NOT_IN_DATABASE		-5263
#define ERR_SQL_NEED_MERGE			-5264
#define ERR_SQL_EVENT_REPEAT			-5265
#define ERR_SQL_ALREADY_EXISTS			-5266
#define ERR_SQL_NEED_OVERWRITE			-5267
#define ERR_SQL_NEED_ADD			-5268
#define ERR_SQL_TIME_STAMP			-5269
#define ERR_SQL_CAPTURE_IN_PROGRESS		-5270
#define ERR_SQL_UNKN_MODE			-5271
#define ERR_SQL_INVALID_GUID			-5272
#define ERR_SQL_MULTIPLE_GUID			-5273
#define ERR_SQL_DELETE_EVENT			-5274
#define ERR_SQL_ADD_FAILED			-5275
#define ERR_SQL_NO_EVENT			-5276
#define ERR_SQL_BAD_COUNT			-5277
#define ERR_SQL_SYSTEM_LOCK			-5278
#define ERR_SQL_NO_CLIENT			-5279
#define ERR_SQL_MULTIPLE_CLIENT			-5280
#define ERR_SQL_FOUND_MORE_ENTRIES		-5281
#define ERR_SQL_ADDING_PICKUP_TO_ITSELF		-5282
#define ERR_SQL_REEL_WILL_HAVE_GAP              -5283
#define ERR_SQL_COULD_NOT_FIND_PARENT_SYNC_EVENT  -5284

////////////////////////////////////////////////////////////////////////////////


#define ERR_TIMELINE_MALLOC			-5300
#define ERR_TIMELINE_CLIPNAME			-5301
#define ERR_TIMELINE_OPENCLIP			-5302
#define ERR_TIMELINE_REALLOC			-5303

#define ERR_SAVE_RESTORE_MALLOC                 -5310

#define ERR_METADATA_CREATE_FAIL                -5330
#define ERR_METADATA_OPEN_FAIL                  -5331
#define ERR_METADATA_SEEK_FAIL                  -5332
#define ERR_METADATA_DELETE_FAIL                -5333
#define ERR_METADATA_HEADER_WRITE_FAIL          -5334
#define ERR_METADATA_READ_HEADER_FAIL           -5335
#define ERR_METADATA_BAD_PARAM                  -5336
#define ERR_METADATA_OUT_OF_MEMORY              -5337
#define ERR_METADATA_INTERNAL_ERROR             -5338
#define ERR_METADATA_FILE_IS_CORRUPT            -5339
#define ERR_METADATA_WRITE_RECORD_FAIL          -5340
#define ERR_METADATA_READ_RECORD_FAIL           -5341
#define ERR_METADATA_NO_SUCH_SECTION            -5342
#define ERR_METADATA_DEBRISPATH_NOT_WRITABLE	-5343
#define ERR_METADATA_MEDIA_ID_WRITE_FAILED	    -5344
#define ERR_METADATA_MEDIA_ID_READ_FAILED	    -5345

// HISTORY ERRORS

#define HISTORY_ERROR_INTERNAL                  -9001

#define HISTORY_ERROR_OPENING_HDR_FILE          -9002
#define HISTORY_ERROR_OPENING_RES_FILE          -9003
#define HISTORY_ERROR_OPENING_PXL_FILE          -9004
#define HISTORY_ERROR_OPENING_HST_FILE          -9005

#define HISTORY_ERROR_SEEKING_IN_HDR_FILE       -9006
#define HISTORY_ERROR_SEEKING_IN_RES_FILE       -9007
#define HISTORY_ERROR_SEEKING_IN_PXL_FILE       -9008
#define HISTORY_ERROR_SEEKING_IN_HST_FILE       -9009

#define HISTORY_ERROR_READING_HDR_FILE	        -9010
#define HISTORY_ERROR_READING_RES_FILE          -9011
#define HISTORY_ERROR_READING_PXL_FILE          -9012
#define HISTORY_ERROR_READING_HST_FILE          -9013

#define HISTORY_ERROR_WRITING_HDR_FILE	        -9014
#define HISTORY_ERROR_WRITING_RES_FILE          -9015
#define HISTORY_ERROR_WRITING_PXL_FILE          -9016
#define HISTORY_ERROR_WRITING_HST_FILE          -9017

#define HISTORY_ERROR_CLOSING_HDR_FILE	        -9018
#define HISTORY_ERROR_CLOSING_RES_FILE          -9019
#define HISTORY_ERROR_CLOSING_PXL_FILE          -9020
#define HISTORY_ERROR_CLOSING_HST_FILE          -9021

#define HISTORY_ERROR_TRUNCATING_HDR_FILE       -9022
#define HISTORY_ERROR_TRUNCATING_RES_FILE       -9023
#define HISTORY_ERROR_TRUNCATING_PXL_FILE       -9024
#define HISTORY_ERROR_TRUNCATING_HST_FILE       -9025

#define HISTORY_ERROR_RESTORING_PIXELS          -9026
#define HISTORY_WARNING_NO_HISTORY_FILE         -9027
#define HISTORY_ERROR_UNKNOWN_ERROR             -9028

#endif

