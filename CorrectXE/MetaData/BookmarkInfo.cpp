//------------------------------------------------------------------------
#include "BookmarkInfo.h"

#include "FileSweeper.h" // Ugggh... for StringTrim!!
#include "pTimecode.h"
//------------------------------------------------------------------------

namespace
{
   const string BookmarkUserKey = "User";
   const string BookmarkDateKey = "Date";
   const string BookmarkReelNumberKey = "ReelNumber";
   const string BookmarkInTimecodeKey = "InTimecode";
   const string BookmarkOutTimecodeKey = "OutTimecode";
   const string BookmarkDefectTypeKey = "DefectType";
   const string BookmarkDefectLocationKey = "DefectLocation";
   const string BookmarkArtistKey = "Artist";
   const string BookmarkCompletedKey = "Completed";
   const string BookmarkQcByKey = "QcBy";
   const string BookmarkQcDateKey = "QcDate";
   const string BookmarkNoteKey = "Note";
};

set<int> BookmarkInfo::BookmarkLocationCache;
string BookmarkInfo::BookmarkLocationCachedFile;
//------------------------------------------------------------------------

BookmarkInfo::BookmarkInfo()
: InTimecode(CTimecode::NOT_SET), OutTimecode(CTimecode::NOT_SET)
{
}
//------------------------------------------------------------------------

BookmarkInfo::BookmarkInfo(CTimecode newInTimecode)
: InTimecode(newInTimecode), OutTimecode(newInTimecode)
{
}
//------------------------------------------------------------------------

// Copy constructor
BookmarkInfo::BookmarkInfo(const BookmarkInfo &orig)
   : User(orig.User)
   , Date(orig.Date)
   , ReelNumber(orig.ReelNumber)
   , InTimecode(orig.InTimecode)
   , OutTimecode(orig.OutTimecode)
   , DefectType(orig.DefectType)
   , DefectLocation(orig.DefectLocation)
   , Artist(orig.Artist)
   , Completed(orig.Completed)
   , QcBy(orig.QcBy)
   , QcDate(orig.QcDate)
   , Note(orig.Note)
{
}
//------------------------------------------------------------------------

BookmarkInfo::~BookmarkInfo()
{
   // Don't know why I need this, but linking BookmarkManager said it was missing
}
//------------------------------------------------------------------------

// = operator
BookmarkInfo& BookmarkInfo::operator=(const BookmarkInfo &rhs)
{
   if (this == &rhs)
   {
      return *this;
   }

   User = rhs.User;
   Date = rhs.Date;
   ReelNumber = rhs.ReelNumber;
   InTimecode = rhs.InTimecode;
   OutTimecode = rhs.OutTimecode;
   DefectType = rhs.DefectType;
   DefectLocation = rhs.DefectLocation;
   Artist = rhs.Artist;
   Completed = rhs.Completed;
   QcBy = rhs.QcBy;
   QcDate = rhs.QcDate;
   Note = rhs.Note;

   return *this;
}
//------------------------------------------------------------------------

int BookmarkInfo::LoadFrom(const string &bookmarkIniFilePath, int frameIndex, const CTimecode &tcProto)
{
   if (frameIndex < 0)
   {
      return BOOKMARKINFO_INDEX_OUT_OF_RANGE;
   }

   if (bookmarkIniFilePath.empty())
   {
      return BOOKMARKINFO_ERR_NO_INI_FILE_SPECIFIED;
   }

   if (!DoesFileExist(bookmarkIniFilePath))
   {
      return BOOKMARKINFO_ERR_INI_FILE_DOES_NOT_EXIST;
   }

   CIniFile *bookmarkIniFile = CreateIniFile(bookmarkIniFilePath, /* ReadOnlyFlag= */ true);
   if (bookmarkIniFile == NULL)
   {
      return BOOKMARKINFO_ERR_INI_FILE_OPEN_FAIL;
   }

   MTIostringstream os;
   os << frameIndex;
   string iniFileSection = os.str();

   int retVal;
   if (bookmarkIniFile->SectionExists(iniFileSection))
   {
      User       = bookmarkIniFile->ReadString(iniFileSection, BookmarkUserKey, "");
      Date       = bookmarkIniFile->ReadString(iniFileSection, BookmarkDateKey, "");
      ReelNumber = bookmarkIniFile->ReadString(iniFileSection, BookmarkReelNumberKey, "");

      string temp = bookmarkIniFile->ReadString(iniFileSection, BookmarkInTimecodeKey, "");
      InTimecode  = temp.empty() ? CTimecode::NOT_SET : PTimecode(temp, tcProto.getFlags(), tcProto.getFramesPerSecond());

      temp        = bookmarkIniFile->ReadString(iniFileSection, BookmarkOutTimecodeKey, "");
      OutTimecode = temp.empty() ? CTimecode::NOT_SET : PTimecode(temp, tcProto.getFlags(), tcProto.getFramesPerSecond());

      DefectType     = bookmarkIniFile->ReadString(iniFileSection, BookmarkDefectTypeKey, "");
      DefectLocation = bookmarkIniFile->ReadString(iniFileSection, BookmarkDefectLocationKey, "");
      Artist         = bookmarkIniFile->ReadString(iniFileSection, BookmarkArtistKey, "");
      Completed      = bookmarkIniFile->ReadString(iniFileSection, BookmarkCompletedKey, "");
      QcBy           = bookmarkIniFile->ReadString(iniFileSection, BookmarkQcByKey, "");
      QcDate         = bookmarkIniFile->ReadString(iniFileSection, BookmarkQcDateKey, "");
      Note           = bookmarkIniFile->ReadString(iniFileSection, BookmarkNoteKey, "");

      retVal = 0;
   }
   else
   {
      Clear();
      retVal = BOOKMARKINFO_NO_BOOKMARK_AT_THIS_FRAME;
   }

   DeleteIniFile(bookmarkIniFile);

   return retVal;
}
//------------------------------------------------------------------------
namespace
{
void WriteIniStringIfNotEmpty(CIniFile *ini, const string &section, const string &key, const string &value)
{
   if (value.empty())
   {
      ini->DeleteKey(section, key);
   }
   else
   {
      ini->WriteString(section, key, value);
   }
}
};

//------------------------------------------------------------------------

int BookmarkInfo::SaveTo(const string &bookmarkIniFilePath, int frameIndex) const
{
   if (frameIndex < 0)
   {
      return BOOKMARKINFO_INDEX_OUT_OF_RANGE;
   }

   if (bookmarkIniFilePath.empty())
   {
      return BOOKMARKINFO_ERR_NO_INI_FILE_SPECIFIED;
   }

   if (InTimecode == CTimecode::NOT_SET)
   {
      return BOOKMARKINFO_ERR_IN_TIMECODE_NOT_SET;
   }

   CIniFile *bookmarkIniFile = CreateIniFile(bookmarkIniFilePath);
   if (bookmarkIniFile == NULL)
   {
      return BOOKMARKINFO_ERR_INI_FILE_OPEN_FAIL;
   }

   MTIostringstream os;
   os << frameIndex;
   string iniFileSection = os.str();

   string localInTimecode = StringTrim((string)PTimecode(InTimecode));
   string localOutTimecode = (OutTimecode == CTimecode::NOT_SET)
                              ? localInTimecode
                              : StringTrim((string)PTimecode(OutTimecode));

   WriteIniStringIfNotEmpty(bookmarkIniFile, iniFileSection, BookmarkUserKey, User);
   WriteIniStringIfNotEmpty(bookmarkIniFile, iniFileSection, BookmarkDateKey, Date);
   WriteIniStringIfNotEmpty(bookmarkIniFile, iniFileSection, BookmarkReelNumberKey, ReelNumber);
   WriteIniStringIfNotEmpty(bookmarkIniFile, iniFileSection, BookmarkInTimecodeKey, localInTimecode);
   WriteIniStringIfNotEmpty(bookmarkIniFile, iniFileSection, BookmarkOutTimecodeKey, localOutTimecode);
   WriteIniStringIfNotEmpty(bookmarkIniFile, iniFileSection, BookmarkDefectTypeKey, DefectType);
   WriteIniStringIfNotEmpty(bookmarkIniFile, iniFileSection, BookmarkDefectLocationKey, DefectLocation);
   WriteIniStringIfNotEmpty(bookmarkIniFile, iniFileSection, BookmarkArtistKey, Artist);
   WriteIniStringIfNotEmpty(bookmarkIniFile, iniFileSection, BookmarkCompletedKey, Completed);
   WriteIniStringIfNotEmpty(bookmarkIniFile, iniFileSection, BookmarkQcByKey, QcBy);
   WriteIniStringIfNotEmpty(bookmarkIniFile, iniFileSection, BookmarkQcDateKey, QcDate);
   WriteIniStringIfNotEmpty(bookmarkIniFile, iniFileSection, BookmarkNoteKey, Note);

   DeleteIniFile(bookmarkIniFile);

   if (bookmarkIniFilePath == BookmarkLocationCachedFile)
   {
      BookmarkLocationCache.insert(frameIndex);
   }

   return 0;
}
//------------------------------------------------------------------------

void BookmarkInfo::GetCsvHeaderFields(StringList &headerFields)
{
   headerFields.clear();
   headerFields.push_back("Reel");
   headerFields.push_back("In");
   headerFields.push_back("Out");
   headerFields.push_back("Dur");
   headerFields.push_back("Defect");
   headerFields.push_back("Location");
   headerFields.push_back("Date");
   headerFields.push_back("User");
   headerFields.push_back("Artist");
   headerFields.push_back("Completed");
   headerFields.push_back("QC by");
   headerFields.push_back("QC date");
   headerFields.push_back("Note");
}
//------------------------------------------------------------------------

void BookmarkInfo::GetCsvRowFields(StringList &rowFields)
{
   MTIostringstream os;
   string inValue         = (InTimecode == CTimecode::NOT_SET)
                            ? string("")
                            : StringTrim((string)PTimecode(InTimecode));

   string outValue        = (OutTimecode == CTimecode::NOT_SET)
                            ? inValue
                            : StringTrim((string)PTimecode(OutTimecode));

   string durationValue = "0";
   if (InTimecode != CTimecode::NOT_SET && OutTimecode != CTimecode::NOT_SET)
   {
      int duration = OutTimecode - InTimecode;
      MTIostringstream os;
      os << duration;
      durationValue = os.str();
   }

   string userValue           = StringTrim(User);
   string dateValue           = StringTrim(Date);
   string reelNumberValue     = StringTrim(ReelNumber);
   string defectTypeValue     = StringTrim(DefectType);
   string defectLocationValue = StringTrim(DefectLocation);
   string artistValue         = StringTrim(Artist);
   string completedValue      = StringTrim(Completed);
   string qcByValue           = StringTrim(QcBy);
   string qcDateValue         = StringTrim(QcDate);
   string noteValue           = StringTrim(Note);

   rowFields.clear();
   rowFields.push_back(reelNumberValue);
   rowFields.push_back(inValue);
   rowFields.push_back(outValue);
   rowFields.push_back(durationValue);
   rowFields.push_back(defectTypeValue);
   rowFields.push_back(defectLocationValue);
   rowFields.push_back(dateValue);
   rowFields.push_back(userValue);
   rowFields.push_back(artistValue);
   rowFields.push_back(completedValue);
   rowFields.push_back(qcByValue);
   rowFields.push_back(qcDateValue);
   rowFields.push_back(noteValue);
}

//------------------------------------------------------------------------

void BookmarkInfo::Clear()
{
   User = "";
   Date = "";
   ReelNumber = "";
   InTimecode = CTimecode::NOT_SET;
   OutTimecode = CTimecode::NOT_SET;
   DefectType = "";
   DefectLocation = "";
   Artist = "";
   Completed = "";
   QcBy = "";
   QcDate = "";
   Note = "";
}
//------------------------------------------------------------------------

int BookmarkInfo::Delete(const string &bookmarkIniFilePath, int frameIndex)
{
   if (bookmarkIniFilePath.empty())
   {
      return BOOKMARKINFO_ERR_NO_INI_FILE_SPECIFIED;
   }

   CIniFile *bookmarkIniFile = CreateIniFile(bookmarkIniFilePath);
   if (bookmarkIniFile == NULL)
   {
      return BOOKMARKINFO_ERR_INI_FILE_OPEN_FAIL;
   }

   MTIostringstream os;
   os << frameIndex;
   string iniFileSection = os.str();

   bookmarkIniFile->EraseSection(iniFileSection);

   DeleteIniFile(bookmarkIniFile);

   if (bookmarkIniFilePath == BookmarkLocationCachedFile)
   {
      BookmarkLocationCache.erase(frameIndex);
   }

   return 0;
}
//------------------------------------------------------------------------
namespace
{
   bool isAllDigits(const string &s)
   {
	  for (unsigned int i = 0; i < s.length(); ++i)
      {
         if (s[i] < '0' || s[i] > '9')
         {
            return false;
         }
      }

      return s.length() > 0;
   }
};

// Gets ordered set of indices of frames that have bookmarks.
set<int> BookmarkInfo::GetSetOfBookmarkFrameIndices(const string &bookmarkIniFilePath)
{
   if (bookmarkIniFilePath == BookmarkLocationCachedFile)
   {
      return BookmarkLocationCache;
   }

   BookmarkLocationCache.clear();
   BookmarkLocationCachedFile = bookmarkIniFilePath;

   CIniFile *bookmarkIniFile = CreateIniFile(bookmarkIniFilePath);
   if (bookmarkIniFile == NULL)
   {
      return BookmarkLocationCache;
   }

   // The section names are the frame indices
   StringList sectionList;
   bookmarkIniFile->ReadSections(&sectionList);
   for (unsigned int i = 0; i < sectionList.size(); ++i)
   {
      if (!isAllDigits(sectionList[i]))
      {
         // Not a frame index
         continue;
      }

      int frameIndex = atoi(sectionList[i].c_str());
      BookmarkLocationCache.insert(frameIndex);
   }

   return BookmarkLocationCache;
}
//------------------------------------------------------------------------

BookmarkInfo *BookmarkInfo::Create(
   const StringList &fieldNames,
   const StringList &fieldValues,
   const CTimecode &tcProto)
{
   BookmarkInfo *bookmarkInfo = new BookmarkInfo;
   int fieldCount = std::min<int>((int)fieldNames.size(), (int)fieldValues.size());
   for (int i = 0; i < fieldCount; ++i)
   {
      if (fieldNames[i] == "User")
      {
         bookmarkInfo->User = fieldValues[i];
      }
      else if (fieldNames[i] == "Reel")
      {
         bookmarkInfo->ReelNumber = fieldValues[i];
      }
      else if (fieldNames[i] == "In")
      {
         bookmarkInfo->InTimecode = fieldValues[i].empty()
                                    ? CTimecode::NOT_SET
                                    : PTimecode(fieldValues[i], tcProto.getFlags(), tcProto.getFramesPerSecond());
      }
      else if (fieldNames[i] == "Out")
      {
         bookmarkInfo->OutTimecode = fieldValues[i].empty()
                                     ? CTimecode::NOT_SET
                                     : PTimecode(fieldValues[i], tcProto.getFlags(), tcProto.getFramesPerSecond());
      }
      else if (fieldNames[i] == "Dur")
      {
         // There is no duration field!
      }
      else if (fieldNames[i] == "Defect")
      {
         bookmarkInfo->DefectType = fieldValues[i];
      }
      else if (fieldNames[i] == "Location")
      {
         bookmarkInfo->DefectLocation = fieldValues[i];
      }
      else if (fieldNames[i] == "Date")
      {
         bookmarkInfo->Date = fieldValues[i];
      }
      else if (fieldNames[i] == "User")
      {
         bookmarkInfo->User = fieldValues[i];
      }
      else if (fieldNames[i] == "Artist")
      {
         bookmarkInfo->Artist = fieldValues[i];
      }
      else if (fieldNames[i] == "Completed")
      {
         bookmarkInfo->Completed = fieldValues[i];
      }
      else if (fieldNames[i] == "QC by")
      {
         bookmarkInfo->QcBy = fieldValues[i];
      }
      else if (fieldNames[i] == "QC date")
      {
         bookmarkInfo->QcDate = fieldValues[i];
      }
      else if (fieldNames[i] == "Note")
      {
         bookmarkInfo->Note = fieldValues[i];
      }
   }

   return bookmarkInfo;
}
//------------------------------------------------------------------------


