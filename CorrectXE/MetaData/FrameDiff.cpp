/*
	file:	CFrameDiff.cpp
	by:	Kevin Manbeck
	date:	Mar 18, 2002

	This file contains functions used to calculate the
	difference between two images.

	Full documentation is in CFrameDiff.h
*/
#include <iostream>
#include <assert.h>
#include <math.h>
using std::ostream;

#include "ImageFormat3.h"
#include "MetaData.h"
#include "FrameDiff.h"
#include "IniFile.h"  // for MTIassert, d'oh!


void CallCalcSubSampleDiff (void *vp, int iJob)
{
  ((CFrameDiff *)vp)->CalcSubSampleDiff(iJob);
}  /* CallCalcSubSampleDiff */

void CallCalcFullFrameDiff (void *vp, int iJob)
{
  ((CFrameDiff *)vp)->CalcFullFrameDiff(iJob);
}  /* CallCalcFullFrameDiff */

CFrameDiff::CFrameDiff(const CImageFormat *ifpArg)
{
  int iRet;

	// set pointers to NULL
  lpBlockPixel = NULL;

	// copy relevant values out of the image format
  fFrameRate = CImageInfo::queryNominalFrameRate(ifpArg->getImageFormatType());
  lNRow = ifpArg->getLinesPerFrame();
  lNCol = ifpArg->getPixelsPerLine();
  lNField = ifpArg->getFieldCount();
  lPixelComponent = ifpArg->getPixelComponents();
  lPixelPacking = ifpArg->getPixelPacking();

	// set the default control values
  lCalculationPerSecond = DEFAULT_CALCULATION_PER_SECOND;
  fInnerRectPercent = DEFAULT_INNER_RECT_PERCENT;

	// build default lookup tables
  iRet = SubSampleLookup ();
  if (iRet)
   {
	//assert (false);
   }

	// set up the MTHREAD_STRUCTS
  tsSubSample.iNThread = 4;
  tsSubSample.iNJob = 4;
  tsSubSample.PrimaryFunction = &CallCalcSubSampleDiff;
  tsSubSample.SecondaryFunction = NULL;
  tsSubSample.CallBackFunction = NULL;
  tsSubSample.vpApplicationData = this;

  iRet = MThreadAlloc (&tsSubSample);
  if (iRet)
   {
    //assert (false);
   }

  tsFullFrame.iNThread = 4;
  tsFullFrame.iNJob = 4;
  tsFullFrame.PrimaryFunction = &CallCalcFullFrameDiff;
  tsFullFrame.SecondaryFunction = NULL;
  tsFullFrame.CallBackFunction = NULL;
  tsFullFrame.vpApplicationData = this;

  iRet = MThreadAlloc (&tsFullFrame);
  if (iRet)
   {
    //assert (false);
   }

  iRet = setDiffThresh (0.);
  if (iRet)
   {
    //assert (false);
   }

}  /* CFrameDiff */

CFrameDiff::~CFrameDiff()
{
  free (lpBlockPixel);
  lpBlockPixel = NULL;

  MThreadFree (&tsSubSample);
  MThreadFree (&tsFullFrame);
}  /* ~CFrameDiff */

int CFrameDiff::setCalculationPerSecond (int iArg)
{
  int iRet;

  lCalculationPerSecond = iArg;
  iRet = SubSampleLookup ();
  return iRet;
}  /* setCalculationPerSecond */

int CFrameDiff::setCalculationPerSecond (float fPercent)
{
  int iRet;

  lCalculationPerSecond = (float)DEFAULT_CALCULATION_PER_SECOND * fPercent/100.;
  iRet = setCalculationPerSecond ((int)lCalculationPerSecond);
  return iRet;
}  /* setCalculationPerSecond */

int CFrameDiff::setInnerRectPercent (float fArg)
{
  int iRet;

  fInnerRectPercent = fArg;
  if (fInnerRectPercent > 1.)
    fInnerRectPercent = 1.;
  if (fInnerRectPercent < 0.)
    fInnerRectPercent = 0.;

  iRet = SubSampleLookup ();
  return iRet;
}  /* setInnerRectPercent */

int CFrameDiff::SubSampleDiff (MTI_UINT8 **ucpCurrArg, MTI_UINT8 **ucpPrevArg,
		FRAME_DIFF *fdp)
{
  long lRow, lCol;
  int iRet;

  vpMetaDataRecord = (void *)fdp;
  if (ucpPrevArg == NULL || ucpPrevArg[0] == NULL)
   {
	// nothing to compute, just set to defaults
    fdp->lDiffFlag = DF_NOT_CALC;
    for (lRow = 0; lRow < FRAME_DIFF_NUM_ROW_BLOCK; lRow++)
    for (lCol = 0; lCol < FRAME_DIFF_NUM_COL_BLOCK; lCol++)
     {
      fdp->faBlockAveFrame[lRow][lCol] = -1;
      fdp->faBlockDiffFrame[lRow][lCol] = -1;

      fdp->faBlockAveField[0][lRow][lCol] = -1;
      fdp->faBlockAveField[1][lRow][lCol] = -1;
      fdp->faBlockDiffField[0][lRow][lCol] = -1;
      fdp->faBlockDiffField[1][lRow][lCol] = -1;
     }

    return 0;
   }

  ucpCurr[0] = ucpCurrArg[0];
  ucpCurr[1] = ucpCurrArg[1];
  ucpPrev[0] = ucpPrevArg[0];
  ucpPrev[1] = ucpPrevArg[1];

	// start up the thread
  iRet = MThreadStart (&tsSubSample);
  if (iRet)
   {
    return iRet;
   }

  return 0;
}  /* SubSampleDiff */

int CFrameDiff::SubSampleDiff (MTI_UINT8 **ucpCurrArg, MTI_UINT8 **ucpPrevArg,
		CMetaData *mdpDiff, int iFrame)
{
  long lRow, lCol;
  int iRet;
  FRAME_DIFF *fdp;

  fdp = (FRAME_DIFF *) mdpDiff->GetRecordPtr() + iFrame;

  return SubSampleDiff (ucpCurrArg, ucpPrevArg, fdp);
}  /* SubSampleDiff */

int CFrameDiff::SubSampleLookup()
/*
	This function creates the look up tables used by sub-sample
	difference calculations.
*/
{
  long lFieldRow, lFieldCol, lRowInBlock, lColInBlock, lPixel;
  int iRet;
  long lField, lBlockRow, lBlockCol, lRow, lCol;
  long lRowBlockSize, lColBlockSize, lRowStart, lRowStop, lColStart,
		lColStop;
  float fNPixelPerFrame, fNPixelPerBlock, fPercentageOfBlock, fPercentageOfSide;
  long lNRowPixelPerBlock, lNColPixelPerBlock, lRowPixel, lColPixel;

		// free previous tables
  free (lpBlockPixel);
  lpBlockPixel = NULL;

/*
	Decide where each block begins.

	We impose the following constraints:

		ignore the outermost boundaries.  This is done
		so we do not get fooled by matting and edge distortions.
*/

  lRowStart = (float)lNRow * (1.-fInnerRectPercent)/2. + 0.5;
  lRowStop = lNRow - lRowStart;
  lColStart = (float)lNCol * (1.-fInnerRectPercent)/2. + 0.5;
  lColStop = lNCol - lColStart;

	// size of each block
  lRowBlockSize = (lRowStop - lRowStart) / FRAME_DIFF_NUM_ROW_BLOCK;
  lColBlockSize = (lColStop - lColStart) / FRAME_DIFF_NUM_COL_BLOCK;

  for (lBlockRow = 0; lBlockRow < FRAME_DIFF_NUM_ROW_BLOCK; lBlockRow++)
  for (lBlockCol = 0; lBlockCol < FRAME_DIFF_NUM_COL_BLOCK; lBlockCol++)
   {
    lRow = lBlockRow * lRowBlockSize + lRowStart;
    lCol = lBlockCol * lColBlockSize + lColStart;

    for (lField = 0; lField < lNField; lField++)
     {
		// convert to field coordinates
      lFieldRow = lRow / lNField;
      lFieldCol = lCol;

		// calculate position in field
      iRet = DataOffset (lFieldRow, lFieldCol,
		laBlockStart[lField][lBlockRow][lBlockCol]);
      if (iRet)
        return iRet;
     }
   }

/*
	Decide the number of pixels in each block.
*/

	// How many pixels should we visit in each frame
  fNPixelPerFrame = (float)lCalculationPerSecond / fFrameRate;

	// How many pixels should we place in each block
  fNPixelPerBlock = fNPixelPerFrame / (float)(FRAME_DIFF_NUM_ROW_BLOCK
		* FRAME_DIFF_NUM_COL_BLOCK);

  fPercentageOfBlock = fNPixelPerBlock / (float)(lRowBlockSize * lColBlockSize);
  fPercentageOfSide = sqrt (fPercentageOfBlock);
  lNRowPixelPerBlock = ((float)lRowBlockSize * fPercentageOfSide)/
		(float)lNField + 0.5;
  lNColPixelPerBlock = (float)lColBlockSize * fPercentageOfSide + 0.5;
  if (lNRowPixelPerBlock == 0)
    lNRowPixelPerBlock = 1;
  if (lNColPixelPerBlock == 0)
    lNColPixelPerBlock = 1;

  // How many pixels should we place in each block
  lNPixelPerBlock = lNRowPixelPerBlock * lNColPixelPerBlock;

	// set the number of pixels in each block
  lpBlockPixel = (long *) malloc (sizeof(long) * lNPixelPerBlock);
  if (lpBlockPixel == NULL)
    return -1;

/*
	Now determine the positions of each pixel, relative to the
	starting position of the block.
*/

  lPixel = 0;
  for (lRowPixel = 0; lRowPixel < lNRowPixelPerBlock; lRowPixel++)
  for (lColPixel = 0; lColPixel < lNColPixelPerBlock; lColPixel++)
   {
    lRowInBlock = (((float)lRowPixel + 0.5) / (float)lNRowPixelPerBlock)
		* (float)lRowBlockSize;
    lColInBlock = (((float)lColPixel + 0.5) / (float)lNColPixelPerBlock)
		* (float)lColBlockSize;

    // change the location in the block into field coordinates
    lFieldRow = lRowInBlock / lNField;
    lFieldCol = lColInBlock;

    // change this field location into an offset from beginning of the block
    iRet = DataOffset (lFieldRow, lFieldCol, lpBlockPixel[lPixel]);
    if (iRet)
      return iRet;
    lPixel++;
   }

  return 0;
}  /* SubSampleLookup */


int CFrameDiff::DataOffset (long lFieldRow, long lFieldCol, long &lOffset)
/*
	This function translates the (lFieldRow, lFieldCol) coordinates
	into a linear address.
*/
{
    long lLinePitch;

/*
	Take into account the format, bits, and packing to determine
	address of this pixel in the full image

	Note:  The 8-bit data will be treated as unsigned char.
	       The 16-bit data will be treated as unsigned short.
	       The 10-bit packed data will be treated as unsigned char.
	       The 12-bit packed data will be treated as unsigned char.
	       The 12-bit unpacked data will be treated as unsigned short.
*/

  switch (lPixelComponent)
   {
//    case IF_PIXEL_COMPONENTS_LUMINANCE:
    case IF_PIXEL_COMPONENTS_Y:
      switch (lPixelPacking)
       {
        case IF_PIXEL_PACKING_8Bits_IN_1Byte:
        case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:    // xyzzy1
        case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_INT:
          lOffset = lFieldRow * lNCol + lFieldCol;
          break;

        case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
          lOffset = ((lFieldRow * lNCol + lFieldCol)*4) / 3;
          break;

        case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:
		// 4 pixels in 5 bytes - force iCol to be multiple of 4
        // monochrome not line-padded in dpx
          lOffset = ((lFieldRow * lNCol + lFieldCol)/4) * 5;
          break;

        default:
          return -1;
       }
    break;
    case IF_PIXEL_COMPONENTS_RGB:
    case IF_PIXEL_COMPONENTS_YUV444:
//    case IF_PIXEL_COMPONENTS_Y:
    case IF_PIXEL_COMPONENTS_YYY:
         switch (lPixelPacking)
       {
        case IF_PIXEL_PACKING_8Bits_IN_1Byte:
        case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L:
        case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R:
        case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes:
        case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_LE:
        case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_BE:
        case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
        case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:
        case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_INT:
          lOffset = (lFieldRow * lNCol + lFieldCol)*3;
          break;

        case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
          lOffset = (lFieldRow * lNCol + lFieldCol)*4;
          break;

        case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes:
        case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes_LR:
          // Each pixel is 4.5 bytes, so force to even boundary
          // Pad each line to 32 bits
          //lLinePitch = ((((lNCol*3)+2)/3)*3)*4;  // 3 comp, 3 comp/32 bits [WRONGO], 4 bytes/32 bits
          //lOffset = (lFieldRow * lLinePitch) + (lFieldCol/2) * 9;
          lLinePitch = ((lNCol*36+31)/32)*4; // 36b per pel rnd up to mult of 32, 4 bytes/32 bits
          lOffset = (lFieldRow * lLinePitch) + (lFieldCol/2) * 9;
          break;

        default:
          return -1;
       }
    break;
    case IF_PIXEL_COMPONENTS_RGBA:
      switch (lPixelPacking)
       {
        case IF_PIXEL_PACKING_8Bits_IN_1Byte:
        case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L:
        case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R:
        case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes:
        case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_LE:
        case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_BE:
        case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
        case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:
        case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_INT:
          lOffset = (lFieldRow * lNCol + lFieldCol)*4;
        break;

        case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
          // Repeats every 3 pixels (16 bytes), so force it to three-pixel
          // boundary. But we presume lines are padded out to 32 bits, so
          // compute pitch separately.
          lLinePitch = (((lNCol*4)+2)/3)*3*4;  // 4 comp, 3 comp/32bits, 4 bytes/32 bits
          lOffset = (lFieldRow * lLinePitch) + (lFieldCol/3)*16;
        break;

        case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes:
        case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes_LR:
          lOffset = (lFieldRow * lNCol + lFieldCol)*6;
        break;

        default:
          return -1;
       }
    break;
    case IF_PIXEL_COMPONENTS_YUV422:
      switch (lPixelPacking)
       {
        case IF_PIXEL_PACKING_8Bits_IN_1Byte:		// CbYCrY in 4 bytes
        case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L:
        case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R:
        case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
        case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:
        case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_INT:	// CbYCrY in 4 shorts
		// force iCol to be even
          lOffset = (lFieldRow * lNCol + (lFieldCol & ~0x1))*2;
        break;
        case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:
		// force iCol to be even
        // I don't think this is right - I think each line should be padded
        // to 32 bit boundary, so pitch should be computed separately
        // but msaybe this format isn't padded, so I need to check it
          lOffset = (lFieldRow * lNCol + (lFieldCol & ~0x1))/2 * 5;
        break;
        case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:
        case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
        // 3 components in 8 bytes -- force iCol to be a multiple of 3
        // I don't think this is right - I think each line should be padded
        // to 32 bit boundary, so pitch should be computed separately
        // but it doesn't really matter since 422 only occurs in SD or HD
          lOffset = (lFieldRow * lNCol + ((lFieldCol/3)*3))/3 * 8;
        break;
        default:
          return -1;
       }
    break;
    default:
      return -1;
   }

  return 0;
} /* DataOffset */

void CFrameDiff::CalcSubSampleDiff (int iJob)
{
  long lRow, lCol, lSumAve, lSumDiff, lField, *lpP, lPixel, lCurr, lPrev,
	lDiff, lBlock, lBlockStart, lBlockStop;
  unsigned char *ucpP, *ucpC;
  unsigned short *uspP, *uspC;
  float fAve, fDiff;
  FRAME_DIFF *fdp;

  fdp = (FRAME_DIFF *)vpMetaDataRecord;

  lBlockStart = (float)(FRAME_DIFF_NUM_ROW_BLOCK * FRAME_DIFF_NUM_COL_BLOCK)
		* (float)iJob / (float)tsSubSample.iNJob;
  lBlockStop = (float)(FRAME_DIFF_NUM_ROW_BLOCK * FRAME_DIFF_NUM_COL_BLOCK)
		* (float)(iJob+1) / (float)tsSubSample.iNJob;
  for (lBlock = lBlockStart; lBlock < lBlockStop; lBlock++)
   {
    lRow = lBlock / FRAME_DIFF_NUM_COL_BLOCK;
    lCol = lBlock % FRAME_DIFF_NUM_COL_BLOCK;

    for (lField = 0; lField < lNField; lField++)
     {
		// previous frame - only one or the other of these is right !!! gack!
      ucpP = ucpPrev[lField] + laBlockStart[lField][lRow][lCol];
      uspP = ((unsigned short *) ucpPrev[lField]) + laBlockStart[lField][lRow][lCol];

		// current frame - only one or the other of these is right !!! gack!
      ucpC = ucpCurr[lField] + laBlockStart[lField][lRow][lCol];
      uspC = ((unsigned short *) ucpCurr[lField]) + laBlockStart[lField][lRow][lCol];

      lpP = lpBlockPixel;

      lSumDiff = 0;
      lSumAve = 0;
      if (lPixelPacking == IF_PIXEL_PACKING_8Bits_IN_1Byte)
       {
        for (lPixel = 0; lPixel < lNPixelPerBlock; lPixel++)
         {
          switch (lPixelComponent)
           {
//            case IF_PIXEL_COMPONENTS_LUMINANCE:
            case IF_PIXEL_COMPONENTS_Y:
              lCurr = (long)ucpC[*lpP];
              lPrev = (long)ucpP[*lpP];
            break;
            case IF_PIXEL_COMPONENTS_RGB:
            case IF_PIXEL_COMPONENTS_RGBA:
//            case IF_PIXEL_COMPONENTS_Y:
            case IF_PIXEL_COMPONENTS_YYY:
              lCurr = (long)ucpC[*lpP+1];   // G
              lPrev = (long)ucpP[*lpP+1];
            break;
            case IF_PIXEL_COMPONENTS_YUV444:
              lCurr = (long)ucpC[*lpP+1];  // Y (UYV)
              lPrev = (long)ucpP[*lpP+1];
            break;
            case IF_PIXEL_COMPONENTS_YUV422:
              lCurr = (long)ucpC[*lpP+3];  // second Y in UYVY
              lPrev = (long)ucpP[*lpP+3];
            break;
            default:
               MTIassert(false);
            break;
          }

          lpP++;

          lDiff = lCurr - lPrev;
          if (lDiff < 0)
            lSumDiff -= lDiff;
          else
            lSumDiff += lDiff;

          lSumAve += lCurr;
         }

        fAve = (float)lSumAve / (float)lNPixelPerBlock;
        fDiff = (float)lSumDiff / (float)lNPixelPerBlock;
        fAve /= 255.;
        fDiff /= 255.;
       }
      else if (lPixelPacking == IF_PIXEL_PACKING_4_10Bits_IN_5Bytes)
       {
        for (lPixel = 0; lPixel < lNPixelPerBlock; lPixel++)
         {
          switch (lPixelComponent)
           {
           ///////// WTF?? WHERE ARE THE RGBs??? QQQ

//            case IF_PIXEL_COMPONENTS_LUMINANCE:
            case IF_PIXEL_COMPONENTS_Y:
              lCurr = (((long) ucpC[*lpP]) << 2) + (long) ((ucpC[*lpP+1] >> 6) & 0x03);
              lPrev = (((long) ucpP[*lpP]) << 2) + (long) ((ucpP[*lpP+1] >> 6) & 0x03);
            break;
            case IF_PIXEL_COMPONENTS_YUV444:
	        // The pattern is UYV. We will look at Y.
              lCurr = ((((long) ucpC[*lpP+1]) & 0x3F) << 4) + (long) ((ucpC[*lpP+2] >> 4) & 0x0F);
              lPrev = ((((long) ucpP[*lpP+1]) & 0x3F) << 4) + (long) ((ucpP[*lpP+2] >> 4) & 0x0F);
            break;
            case IF_PIXEL_COMPONENTS_YUV422:
	        // The pattern is UYVY, with each set of four consuming 5 bytes.
            // We will look at the last Y in this pattern.
              lCurr = ((((long) ucpC[*lpP+3]) & 0x3) << 8) + (long)(ucpC[*lpP+4]);
              lPrev = ((((long) ucpP[*lpP+3]) & 0x3) << 8) + (long)(ucpP[*lpP+4]);
            break;
            default:
               MTIassert(false);
            break;
           }
          lpP++;

          lDiff = lCurr - lPrev;
          if (lDiff < 0)
            lSumDiff -= lDiff;
          else
            lSumDiff += lDiff;

          lSumAve += lCurr;
         }

        fAve = (float)lSumAve / (float)lNPixelPerBlock;
        fDiff = (float)lSumDiff / (float)lNPixelPerBlock;
        fAve /= 1023.;
        fDiff /= 1023.;
       }
      else if (lPixelPacking == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS)
       {
        for (lPixel = 0; lPixel < lNPixelPerBlock; lPixel++)
         {
/*
	The pattern is UYV, YUY, VYU, or YVY with each set of three consuming
	4 bytes.  We have chosen the offset values to fall on the YUY and
	YVY sets.

	We will look at the first Y in these patterns.
*/

          lCurr = ((long)(ucpC[*lpP+5] & 0x3) << 8) + (long)(ucpC[*lpP+4]);
          lPrev = ((long)(ucpP[*lpP+5] & 0x3) << 8) + (long)(ucpP[*lpP+4]);
          lpP++;

          lDiff = lCurr - lPrev;
          if (lDiff < 0)
            lSumDiff -= lDiff;
          else
            lSumDiff += lDiff;

          lSumAve += lCurr;
         }

        fAve = (float)lSumAve / (float)lNPixelPerBlock;
        fDiff = (float)lSumDiff / (float)lNPixelPerBlock;
        fAve /= 1023.;
        fDiff /= 1023.;
       }
      else if (lPixelPacking == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L)
       {
        // This is like IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS,
        // but big-endian
        for (lPixel = 0; lPixel < lNPixelPerBlock; lPixel++)
         {
          switch (lPixelComponent)
           {
//            case IF_PIXEL_COMPONENTS_LUMINANCE:
            case IF_PIXEL_COMPONENTS_Y:
            case IF_PIXEL_COMPONENTS_YYY:
              lCurr = ((long) (ucpC[*lpP]) << 2) + (long)((ucpC[*lpP+1] >> 6) & 0x03);
              lPrev = ((long) (ucpP[*lpP]) << 2) + (long)((ucpP[*lpP+1] >> 6) & 0x03);
            break;
            case IF_PIXEL_COMPONENTS_RGB:
            case IF_PIXEL_COMPONENTS_RGBA:
            case IF_PIXEL_COMPONENTS_YUV444:
//            case IF_PIXEL_COMPONENTS_Y:
//            case IF_PIXEL_COMPONENTS_YYY:
	        // The pattern is UYV or RGB or RGBA. We will look at Y or G (2nd component).
              lCurr = ((((long) ucpC[*lpP+1]) & 0x3F) << 4) + (long) ((ucpC[*lpP+2] >> 4) & 0x0F);
              lPrev = ((((long) ucpP[*lpP+1]) & 0x3F) << 4) + (long) ((ucpP[*lpP+2] >> 4) & 0x0F);
            break;
            case IF_PIXEL_COMPONENTS_YUV422:
	        // The pattern is UYV, YUY, VYU, or YVY with each set of three
            // consuming 4 bytes.  We have chosen the offset values to fall
            // on the YUY and YVY sets.
            // We will look at the first Y in these patterns.
              lCurr = ((long) (ucpC[*lpP+4]) << 2) + (long) ((ucpC[*lpP+5] >> 6) & 0x03);
              lPrev = ((long) (ucpP[*lpP+4]) << 2) + (long) ((ucpP[*lpP+5] >> 6) & 0x03);
            break;
            default:
               MTIassert(false);
            break;
          }
          lpP++;

          lDiff = lCurr - lPrev;
          if (lDiff < 0)
            lSumDiff -= lDiff;
          else
            lSumDiff += lDiff;

          lSumAve += lCurr;
         }

        fAve = (float)lSumAve / (float)lNPixelPerBlock;
        fDiff = (float)lSumDiff / (float)lNPixelPerBlock;
        fAve /= 1023.;
        fDiff /= 1023.;
       }
      else if ((lPixelPacking == IF_PIXEL_PACKING_2_12Bits_IN_3Bytes)||
               (lPixelPacking == IF_PIXEL_PACKING_2_12Bits_IN_3Bytes_LR))
       {
        for (lPixel = 0; lPixel < lNPixelPerBlock; lPixel++)
         {
          switch (lPixelComponent)
           {
//            case IF_PIXEL_COMPONENTS_LUMINANCE:
            case IF_PIXEL_COMPONENTS_Y:
            case IF_PIXEL_COMPONENTS_YYY:
              lCurr = (((long) ucpC[*lpP]) << 4) + (long) ((ucpC[*lpP+1] >> 4) & 0x0F);
              lPrev = (((long) ucpP[*lpP]) << 4) + (long) ((ucpP[*lpP+1] >> 4) & 0x0F);
            break;
            case IF_PIXEL_COMPONENTS_RGB:
            case IF_PIXEL_COMPONENTS_RGBA:
//            case IF_PIXEL_COMPONENTS_Y:
//            case IF_PIXEL_COMPONENTS_YYY:
	        // The pattern is RGB or RGBA. We will look at G (2nd component).
              lCurr = ((((long) ucpC[*lpP+1]) & 0x0F) << 8) + (long) ucpC[*lpP+2];
              lPrev = ((((long) ucpP[*lpP+1]) & 0x0F) << 8) + (long) ucpP[*lpP+2];
            break;
            default:
               MTIassert(false);
            break;
          }
          lpP++;

          lDiff = lCurr - lPrev;
          if (lDiff < 0)
            lSumDiff -= lDiff;
          else
            lSumDiff += lDiff;

          lSumAve += lCurr;
         }

        fAve = (float)lSumAve / (float)lNPixelPerBlock;
        fDiff = (float)lSumDiff / (float)lNPixelPerBlock;
        fAve /= 4095.;
        fDiff /= 4095.;
       }
      else if (lPixelPacking == IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_LE)
       {
        for (lPixel = 0; lPixel < lNPixelPerBlock; lPixel++)
         {
          switch (lPixelComponent)
           {
//            case IF_PIXEL_COMPONENTS_LUMINANCE:
            case IF_PIXEL_COMPONENTS_Y:
            case IF_PIXEL_COMPONENTS_YYY:
              lCurr = (long) (uspC[*lpP] >> 4);
              lPrev = (long) (uspP[*lpP] >> 4);
            break;
            case IF_PIXEL_COMPONENTS_RGB:
            case IF_PIXEL_COMPONENTS_RGBA:
//            case IF_PIXEL_COMPONENTS_Y:
//            case IF_PIXEL_COMPONENTS_YYY:

            default:
	        // The pattern is RGB or RGBA. We will look at G (2nd component).
              lCurr = (long) (uspC[*lpP+1] >> 4);
              lPrev = (long) (uspP[*lpP+1] >> 4);
            break;
          }
          lpP++;

          lDiff = lCurr - lPrev;
          if (lDiff < 0)
            lSumDiff -= lDiff;
          else
            lSumDiff += lDiff;

          lSumAve += lCurr;
         }

        fAve = (float)lSumAve / (float)lNPixelPerBlock;
        fDiff = (float)lSumDiff / (float)lNPixelPerBlock;
        fAve /= 4095.;
        fDiff /= 4095.;
       }
      else if (lPixelPacking == IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_BE)
       {
        for (lPixel = 0; lPixel < lNPixelPerBlock; lPixel++)
         {
          switch (lPixelComponent)
           {
//            case IF_PIXEL_COMPONENTS_LUMINANCE:
            case IF_PIXEL_COMPONENTS_Y:
            case IF_PIXEL_COMPONENTS_YYY:
              lCurr = (long) (((uspC[*lpP] >> 12) & 0xF) | (uspC[*lpP] << 4));
              lPrev = (long) (((uspP[*lpP] >> 12) & 0xF) | (uspP[*lpP] << 4));
            break;
            case IF_PIXEL_COMPONENTS_RGB:
            case IF_PIXEL_COMPONENTS_RGBA:
//            case IF_PIXEL_COMPONENTS_Y:
//            case IF_PIXEL_COMPONENTS_YYY:
            default:
	        // The pattern is RGB or RGBA. We will look at G (2nd component).
              lCurr = (long) (((uspC[*lpP+1] >> 12) & 0xF) | (uspC[*lpP+1] << 4));
              lPrev = (long) (((uspP[*lpP+1] >> 12) & 0xF) | (uspP[*lpP+1] << 4));
            break;
//            default:
//               MTIassert(false);
//            break;
          }
          lpP++;

          lDiff = lCurr - lPrev;
          if (lDiff < 0)
            lSumDiff -= lDiff;
          else
            lSumDiff += lDiff;

          lSumAve += lCurr;
         }

        fAve = (float)lSumAve / (float)lNPixelPerBlock;
        fDiff = (float)lSumDiff / (float)lNPixelPerBlock;
        fAve /= 4095.;
        fDiff /= 4095.;
       }
      else if (lPixelPacking == IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_INT    // QQQ not possible?
      || lPixelPacking == IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE
		|| lPixelPacking == IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE
		|| lPixelPacking == IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L
		|| lPixelPacking == IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R)
       {
        for (lPixel = 0; lPixel < lNPixelPerBlock; lPixel++)
         {
          switch (lPixelComponent)
           {
//            case IF_PIXEL_COMPONENTS_LUMINANCE:
            case IF_PIXEL_COMPONENTS_Y:
            case IF_PIXEL_COMPONENTS_YYY:
              lCurr = (long) uspC[*lpP];
              lPrev = (long) uspP[*lpP];
            break;
            case IF_PIXEL_COMPONENTS_RGB:
            case IF_PIXEL_COMPONENTS_RGBA:
//            case IF_PIXEL_COMPONENTS_Y:
//            case IF_PIXEL_COMPONENTS_YYY:
            default:
	        // The pattern is RGB or RGBA. We will look at G (2nd component).
              lCurr = (long) uspC[*lpP+1];
              lPrev = (long) uspP[*lpP+1];
            break;
//            default:
//               MTIassert(false);
//            break;
          }
          lpP++;

          lDiff = lCurr - lPrev;
          if (lDiff < 0)
            lSumDiff -= lDiff;
          else
            lSumDiff += lDiff;

          lSumAve += lCurr;
         }

        fAve = (float)lSumAve / (float)lNPixelPerBlock;
        fDiff = (float)lSumDiff / (float)lNPixelPerBlock;
        if (lPixelPacking == IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L
            || lPixelPacking == IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R)
        {
            fAve /= 1023.;
            fDiff /= 1023.;
        }
        else
        {
            fAve /= 65535.;
            fDiff /= 65535.;
        }
       }
/*
   		else if (lPixelPacking == IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE)
   		{
   			/// QQQ TODO: IMPLEMENT THIS!
   			MTIassert(lPixelPacking != IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE);
			}

*/

      if (lNField == 1)
       {
        fdp->faBlockAveFrame[lRow][lCol] = fAve;
        fdp->faBlockDiffFrame[lRow][lCol] = fDiff;

        fdp->faBlockAveField[0][lRow][lCol] = -1;
        fdp->faBlockAveField[1][lRow][lCol] = -1;
        fdp->faBlockDiffField[0][lRow][lCol] = -1;
        fdp->faBlockDiffField[1][lRow][lCol] = -1;
       }
      else
       {
        fdp->faBlockAveField[lField][lRow][lCol] = fAve;
        fdp->faBlockDiffField[lField][lRow][lCol] = fDiff;
       }
     }

    if (lNField == 2)
     {
      fdp->faBlockAveFrame[lRow][lCol] =
		(fdp->faBlockAveField[0][lRow][lCol] +
			fdp->faBlockAveField[1][lRow][lCol]) / 2.;
      fdp->faBlockDiffFrame[lRow][lCol] =
		(fdp->faBlockDiffField[0][lRow][lCol] +
			fdp->faBlockDiffField[1][lRow][lCol]) / 2.;
     }
   }

  return;
}  /* CalcSubSampleDiff */

void CFrameDiff::CalcFullFrameDiff (int iJob)
// ************************************************************
// *** THIS IS NOT USED IN CORRECT SO CAN BE SAFELY IGNORED ***
// ************************************************************
{
  long lField;
  long lA0, lA1, lA2, lA3, lA4, lA5;
  long lB0, lB1, lB2, lB3, lB4, lB5;
  long lVA0, lVA1, lVA2, lVA3;
  long lVB0, lVB1, lVB2, lVB3;
  MTI_UINT8 *ucpA, *ucpB;
  long lPixel, lNPixel, lStartPixel, lStopPixel;

  lField = iJob % lNField;


  lNPixel = lNRow * lNCol / lNField;

  lStartPixel = iJob * lNPixel / tsSubSample.iNJob;
  lStopPixel = (iJob+1) * lNPixel / tsSubSample.iNJob;

  switch (lPixelPacking)
   {
    case IF_PIXEL_PACKING_8Bits_IN_1Byte:
      lStartPixel &= ~0x1; // start and stop pixels should be even
      lStopPixel &= ~0x1;
      ucpA = ucpDataA[lField] + lStartPixel * 2;
      ucpB = ucpDataB[lField] + lStartPixel * 2;
      for (lPixel = lStartPixel; lPixel < lStopPixel; lPixel+=2)
       {
        lA0 = (long)ucpA[0];
        lA1 = (long)ucpA[1];
        lA2 = (long)ucpA[2];
        lA3 = (long)ucpA[3];
        lB0 = (long)ucpB[0];
        lB1 = (long)ucpB[1];
        lB2 = (long)ucpB[2];
        lB3 = (long)ucpB[3];

        ucpA+= 4;
        ucpB+= 4;
    
        if (
            (lA0 - lB0) < -lDiffThresh ||
            (lA0 - lB0) > lDiffThresh ||
            (lA1 - lB1) < -lDiffThresh ||
            (lA1 - lB1) > lDiffThresh ||
            (lA2 - lB2) < -lDiffThresh ||
            (lA2 - lB2) > lDiffThresh ||
            (lA3 - lB3) < -lDiffThresh ||
            (lA3 - lB3) > lDiffThresh
           )
         {
          lDifferenceLocation++;
         }
       }
    break;
    case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:
      lStartPixel &= ~0x1; // start and stop pixels should be even
      lStopPixel &= ~0x1;
      ucpA = ucpDataA[lField] + (long)(lStartPixel/2 * 5);
      ucpB = ucpDataB[lField] + (long)(lStartPixel/2 * 5);
      for (lPixel = lStartPixel; lPixel < lStopPixel; lPixel+=2)
       {
        lA0 = (((long)ucpA[0])<<2) + (((long)ucpA[1])>>6);
        lA1 = (((long)(ucpA[1]&0x3F))<<4) + (((long)ucpA[2])>>4);
        lA2 = (((long)(ucpA[2]&0xF))<<6) + (((long)ucpA[3])>>2);
        lA3 = (((long)(ucpA[3]&0x3))<<8) + ((long)ucpA[4]);
        lB0 = (((long)ucpB[0])<<2) + (((long)ucpB[1])>>6);
        lB1 = (((long)(ucpB[1]&0x3F))<<4) + (((long)ucpB[2])>>4);
        lB2 = (((long)(ucpB[2]&0xF))<<6) + (((long)ucpB[3])>>2);
        lB3 = (((long)(ucpB[3]&0x3))<<8) + ((long)ucpB[4]);

        ucpA+= 5;
        ucpB+= 5;
    
        if (
            (lA0 - lB0) < -lDiffThresh ||
            (lA0 - lB0) > lDiffThresh ||
            (lA1 - lB1) < -lDiffThresh ||
            (lA1 - lB1) > lDiffThresh ||
            (lA2 - lB2) < -lDiffThresh ||
            (lA2 - lB2) > lDiffThresh ||
            (lA3 - lB3) < -lDiffThresh ||
            (lA3 - lB3) > lDiffThresh
           )
         {
          lDifferenceLocation++;
         }
       }
    break;
    case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:
	// start and stop pixels should be a multiple of 6
      lStartPixel = (lStartPixel/6) * 6;
      lStopPixel = (lStopPixel/6) * 6;
      ucpA = ucpDataA[lField] + (long)(lStartPixel/3 * 8);
      ucpB = ucpDataB[lField] + (long)(lStartPixel/3 * 8);
      for (lPixel = lStartPixel; lPixel < lStopPixel; lPixel+=6)
       {
        lVA0 = (ucpA[3]<<24)+(ucpA[2]<<16)+(ucpA[1]<<8)+(ucpA[0]);
        lVA1 = (ucpA[7]<<24)+(ucpA[6]<<16)+(ucpA[5]<<8)+(ucpA[4]);
        lVA2 = (ucpA[11]<<24)+(ucpA[10]<<16)+(ucpA[9]<<8)+(ucpA[8]);
        lVA3 = (ucpA[15]<<24)+(ucpA[14]<<16)+(ucpA[13]<<8)+(ucpA[12]);
        lVB0 = (ucpB[3]<<24)+(ucpB[2]<<16)+(ucpB[1]<<8)+(ucpB[0]);
        lVB1 = (ucpB[7]<<24)+(ucpB[6]<<16)+(ucpB[5]<<8)+(ucpB[4]);
        lVB2 = (ucpB[11]<<24)+(ucpB[10]<<16)+(ucpB[9]<<8)+(ucpB[8]);
        lVB3 = (ucpB[15]<<24)+(ucpB[14]<<16)+(ucpB[13]<<8)+(ucpB[12]);
        lA0 = (lVA0>>10)&0x3ff;	// Y0
        lA1 = (lVA1    )&0x3ff;	// Y1
        lA2 = (lVA1>>20)&0x3ff;	// Y2
        lA3 = (lVA2>>10)&0x3ff;	// Y3
        lA4 = (lVA3    )&0x3ff;	// Y4
        lA5 = (lVA3>>20)&0x3ff;	// Y5
        lB0 = (lVB0>>10)&0x3ff;	// Y0
        lB1 = (lVB1    )&0x3ff;	// Y1
        lB2 = (lVB1>>20)&0x3ff;	// Y2
        lB3 = (lVB2>>10)&0x3ff;	// Y3
        lB4 = (lVB3    )&0x3ff;	// Y4
        lB5 = (lVB3>>20)&0x3ff;	// Y5

        ucpA+= 16;
        ucpB+= 16;
    
        if (
            (lA0 - lB0) < -lDiffThresh ||
            (lA0 - lB0) > lDiffThresh ||
            (lA1 - lB1) < -lDiffThresh ||
            (lA1 - lB1) > lDiffThresh ||
            (lA2 - lB2) < -lDiffThresh ||
            (lA2 - lB2) > lDiffThresh ||
            (lA3 - lB3) < -lDiffThresh ||
            (lA3 - lB3) > lDiffThresh ||
            (lA4 - lB4) < -lDiffThresh ||
            (lA4 - lB4) > lDiffThresh ||
            (lA5 - lB5) < -lDiffThresh ||
            (lA5 - lB5) > lDiffThresh
           )
         {
          lDifferenceLocation++;
         }
       }
    break;
    default:
      lDifferenceLocation++;
   }

}  /* CalcFullFrameDiff */

int CFrameDiff::setDiffThresh (float fArg)
{

  if (fArg < 0. || fArg > 1.)
    return -1;

  switch (lPixelPacking)
   {
    case IF_PIXEL_PACKING_8Bits_IN_1Byte:
      lDiffThresh = fArg * 256.;
    break;
    case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
    case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:
    case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_INT:  // QQQ not possible?
      lDiffThresh = fArg * 65536.;
    break;
    case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:
    case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:
    case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
    case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R:
    case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L:
    case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R:
      lDiffThresh = fArg * 1024.;
    break;
    case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes:
    case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes_LR:
    case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_LE:
    case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_BE:
      lDiffThresh = fArg * 4096.;
    break;
    default:
      return -1;
   }

  return 0;
}  /* setDiffThresh */

int CFrameDiff::FullFrameDiff (MTI_UINT8 **ucpA, MTI_UINT8 **ucpB,
		long *lpDiffLoc)
{
  int iRet;

  lDifferenceLocation = DL_NONE;

  ucpDataA[0] = ucpA[0];
  ucpDataA[1] = ucpA[1];
  ucpDataB[0] = ucpB[0];
  ucpDataB[1] = ucpB[1];

	// start up the thread
  iRet = MThreadStart (&tsFullFrame);
  if (iRet)
   {
    return iRet;
   }

  *lpDiffLoc = lDifferenceLocation;
  
  return 0;
}  /* FullFrameDiff */

void CFrameDiff::CopyFields (FRAME_DIFF &fdResult, const FRAME_DIFF &fdRef0,
        int iField0, const FRAME_DIFF &fdRef1, int iField1)
/*
	This function copies iField0 from Ref0 and iField1 from fdRef1
	into "rdResult"
*/
{
  long lRow, lCol;

  for (lRow = 0; lRow < FRAME_DIFF_NUM_ROW_BLOCK; lRow++)
  for (lCol = 0; lCol < FRAME_DIFF_NUM_COL_BLOCK; lCol++)
   {
    fdResult.faBlockAveField[0][lRow][lCol] =
		fdRef0.faBlockAveField[iField0][lRow][lCol];
    fdResult.faBlockAveField[1][lRow][lCol] =
		fdRef1.faBlockAveField[iField1][lRow][lCol];
    fdResult.faBlockDiffField[0][lRow][lCol] =
		fdRef0.faBlockDiffField[iField0][lRow][lCol];
    fdResult.faBlockDiffField[1][lRow][lCol] =
		fdRef1.faBlockDiffField[iField1][lRow][lCol];

    fdResult.faBlockAveFrame[lRow][lCol] =
        (fdResult.faBlockAveField[0][lRow][lCol] +
		    fdResult.faBlockAveField[1][lRow][lCol]) / 2.;
    fdResult.faBlockDiffFrame[lRow][lCol] =
        (fdResult.faBlockDiffField[0][lRow][lCol] +
		    fdResult.faBlockDiffField[1][lRow][lCol]) / 2.;

   }
}  /* CopyFields */

void CFrameDiff::ZeroFields (FRAME_DIFF &fdResult, const FRAME_DIFF &fdRef0,
        int iField0, const FRAME_DIFF &fdRef1, int iField1, int iFieldZero)
/*
	This function copies iField0 from Ref0 and iField1 from fdRef1
	into fdResult.  It sets the difference values in iFieldZero to zero.
*/
{
  long lRow, lCol;

  for (lRow = 0; lRow < FRAME_DIFF_NUM_ROW_BLOCK; lRow++)
  for (lCol = 0; lCol < FRAME_DIFF_NUM_COL_BLOCK; lCol++)
   {
    fdResult.faBlockAveField[0][lRow][lCol] =
		fdRef0.faBlockAveField[iField0][lRow][lCol];
    fdResult.faBlockAveField[1][lRow][lCol] =
		fdRef1.faBlockAveField[iField1][lRow][lCol];
    fdResult.faBlockDiffField[0][lRow][lCol] =
		fdRef0.faBlockDiffField[iField0][lRow][lCol];
    fdResult.faBlockDiffField[1][lRow][lCol] =
		fdRef1.faBlockDiffField[iField1][lRow][lCol];
    fdResult.faBlockDiffField[iFieldZero][lRow][lCol] = 0.;

    fdResult.faBlockAveFrame[lRow][lCol] =
        (fdResult.faBlockAveField[0][lRow][lCol] +
		    fdResult.faBlockAveField[1][lRow][lCol]) / 2.;
    fdResult.faBlockDiffFrame[lRow][lCol] =
        (fdResult.faBlockDiffField[0][lRow][lCol] +
		    fdResult.faBlockDiffField[1][lRow][lCol]) / 2.;
   }
}  /* CopyFields */

