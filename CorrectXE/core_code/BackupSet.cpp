/** @file BackupSet.cpp
 *
 * Implementation of CBackupSet.
 */

#include "BackupSet.hpp"

CBackupSet::CBackupSet()
    : mBackupSetString()
{
    // nothing else to do
}


CBackupSet::CBackupSet(std::string const &aBackupSetString)
    : mBackupSetString(aBackupSetString)
{
    // nothing else to do
}


std::string CBackupSet::GetBackupSetString() const
{
    return mBackupSetString;
}
