/*
	File:	vtr_tc.h
	By:	Kevin Manbeck
	Date:	May 18, 2000

	This header file is used for VTR time code manipulation functions
*/
#ifndef VTR_TC_H
#define VTR_TC_H

typedef struct
{
  int
    iH,		/* the hour */
    iM,		/* the minute */
    iS,		/* the second */
    iF,		/* the frame */
    iField,	/* the field */
    iDF,	/* drop frame flag */
    iCF;	/* color frame flag */
} TIME_CODE;

#endif
