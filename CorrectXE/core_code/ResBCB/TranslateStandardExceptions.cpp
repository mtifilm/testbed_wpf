#include "TranslateStandardExceptions.h"
/*
TranslateStandardExceptions.cpp/.h
----------------------------------
A Library for Converting C++ Style Exceptions Into VCL Style Exceptions
Copyright (c) 2003 Early Ehlinger

-- Overview --
Get rid of "External Exception EEFFACE" errors, once and for all!

-- License & Details --
Gnu Lesser General Public License, v2.1
See TranslateStandardExceptions.h for License and Library Details

*/

typedef Exception* (__fastcall* ExceptObjProcType)( Sysutils::PExceptionRecord P );
ExceptObjProcType pOldExceptObjProc = NULL;

class not_standard_exception
  {
  public:
    virtual void foo() = 0;
  };

std::exception const* get_std_exception( not_standard_exception const* p_exception )
  {
  std::exception const* p_std_exception = 0;
  __try
    {
    p_std_exception = dynamic_cast< std::exception const* >( p_exception );
    }
  __except( EXCEPTION_EXECUTE_HANDLER )
    { }
  return p_std_exception;
  }


Exception* __fastcall GetCppExceptionObject(Sysutils::PExceptionRecord P)
  {
  static const DWORD cCppException = 0xEEFFACE;
  static const DWORD cDelphiException = 0xEEDFADE;
    /*
      I've only ever seen an unhandled Delphi exception *once*, and don't know
      exactly how to filter it.  If it becomes a problem, I'll debug it a bit
      more.  Right now, this constant is here just for documentary purposes.
    */

  const int cMagic = 0x52;
  switch( P->ExceptionCode )
    {
    case cCppException:
      {
      // Extract relevant information from the exception record:
      char* type_name = (char*)P->ExceptionInformation[0];
      not_standard_exception const* p_exception
        = reinterpret_cast< not_standard_exception* >( P->ExceptionInformation[2] + cMagic );

      if ( std::exception const* p_std_exception = get_std_exception( p_exception ) )
        {
        if ( std::logic_error const* p_logic_error = dynamic_cast< std::logic_error const* >( p_std_exception ) )
          return new res_bcb::CppStdLogicError( type_name , p_logic_error );
        else if ( std::domain_error const* p_domain_error = dynamic_cast< std::domain_error const* >( p_std_exception ) )
          return new res_bcb::CppStdDomainError( type_name , p_domain_error );
        else if ( std::invalid_argument const* p_invalid_argument = dynamic_cast< std::invalid_argument const* >( p_std_exception ) )
          return new res_bcb::CppStdInvalidArgument( type_name , p_invalid_argument );
        else if ( std::length_error const* p_length_error = dynamic_cast< std::length_error const* >( p_std_exception ) )
          return new res_bcb::CppStdLengthError( type_name , p_length_error );
        else if ( std::out_of_range const* p_out_of_range = dynamic_cast< std::out_of_range const* >( p_std_exception ) )
          return new res_bcb::CppStdOutOfRange( type_name , p_out_of_range );
        else if ( std::runtime_error const* p_runtime_error = dynamic_cast< std::runtime_error const* >( p_std_exception ) )
          return new res_bcb::CppStdRuntimeError( type_name , p_runtime_error );
        else if ( std::range_error const* p_range_error = dynamic_cast< std::range_error const* >( p_std_exception ) )
          return new res_bcb::CppStdRangeError( type_name , p_range_error );
        else if ( std::overflow_error const* p_overflow_error = dynamic_cast< std::overflow_error const* >( p_std_exception ) )
          return new res_bcb::CppStdOverflowError( type_name , p_overflow_error );
        else
          return new res_bcb::CppStdException( type_name , p_std_exception );
        }
      return new res_bcb::CppException( type_name , p_exception );
      }
    default:
      if ( pOldExceptObjProc )
        return pOldExceptObjProc(P);
      else
        return NULL;
    }
  }

void install_exception_object_handler( )
  {
  pOldExceptObjProc = reinterpret_cast< ExceptObjProcType >( ExceptObjProc );
  System::ExceptObjProc = GetCppExceptionObject;
  }

#pragma startup install_exception_object_handler

void uninstall_exception_object_handler( )
  {
  if ( pOldExceptObjProc )
    System::ExceptObjProc = pOldExceptObjProc; // System.pas
  }
#pragma exit uninstall_exception_object_handler


namespace res_bcb
  {
  CppException::CppException( char const* ObjectTypeName , void const* ObjectAddress )
    : Exception( "" )
    , m_object_type_name( ObjectTypeName )
    , m_object_address( ObjectAddress )
    { }

  void __fastcall CppException::AfterConstruction( )
    {
    Message = GetExceptionMessage( );
    inherited::AfterConstruction( );
    }

  AnsiString CppException::GetExceptionMessage( )
    {
    return "C++ Exception (Type " + ObjectTypeName + ")";
    }

  CppStdException::CppStdException
    ( char const* ObjectTypeName
    , std::exception const* ObjectAddress )
    : CppException( ObjectTypeName , ObjectAddress )
    , m_alias( "C++ Exception" )
    , m_what( ObjectAddress->what() )
    { }

  CppStdException::CppStdException
    ( char const* ObjectTypeName
    , std::exception const* ObjectAddress
    , char const* Alias )
    : CppException( ObjectTypeName , ObjectAddress )
    , m_alias( Alias )
    , m_what( ObjectAddress->what() )
    { }
    
  AnsiString CppStdException::GetExceptionMessage( )
    {
    return What + "  [" + Alias + "/" + ObjectTypeName + "]";
    }

  CppStdLogicError::CppStdLogicError
    ( char const* ObjectTypeName
    , std::logic_error const* ObjectAddress )
    : inherited( ObjectTypeName , ObjectAddress , "Logic Error" )
    { }

  CppStdDomainError::CppStdDomainError
    ( char const* ObjectTypeName
    , std::domain_error const* ObjectAddress )
    : inherited( ObjectTypeName , ObjectAddress , "Domain Error" )
    { }

  CppStdInvalidArgument::CppStdInvalidArgument
    ( char const* ObjectTypeName
    , std::invalid_argument const* ObjectAddress )
    : inherited( ObjectTypeName , ObjectAddress , "Invalid Argument" )
    { }

  CppStdLengthError::CppStdLengthError
    ( char const* ObjectTypeName
    , std::length_error const* ObjectAddress )
    : inherited( ObjectTypeName , ObjectAddress , "Length Error" )
    { }

  CppStdOutOfRange::CppStdOutOfRange
    ( char const* ObjectTypeName
    , std::out_of_range const* ObjectAddress )
    : inherited( ObjectTypeName , ObjectAddress , "Out of Range" )
    { }

  CppStdRuntimeError::CppStdRuntimeError
    ( char const* ObjectTypeName
    , std::runtime_error const* ObjectAddress )
    : inherited( ObjectTypeName , ObjectAddress , "Runtime Error" )
    { }

  CppStdRangeError::CppStdRangeError
    ( char const* ObjectTypeName
    , std::range_error const* ObjectAddress )
    : inherited( ObjectTypeName , ObjectAddress , "Range Error" )
    { }

  CppStdOverflowError::CppStdOverflowError
    ( char const* ObjectTypeName
    , std::overflow_error const* ObjectAddress )
    : inherited( ObjectTypeName , ObjectAddress , "Overflow" )
    { }
    
  }
