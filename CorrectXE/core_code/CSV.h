#ifndef CSVH
#define CSVH

#include "corecodelibint.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using std::string;
typedef std::vector<string> stringlist;

// Return values
#define CSV_SUCCESS      0
#define CSV_FILE_EOF     1
#define CSV_FILE_IO_FAIL 2


class MTI_CORECODELIB_API CsvReader
{
public:
   CsvReader(const string &csvFilePath);
   ~CsvReader();
   int GetRow(stringlist &row);
   void static parseCsvRow(const string& line, stringlist &row);

private:
   std::ifstream *fileStream;
};

class MTI_CORECODELIB_API CsvWriter
{
public:
   CsvWriter(const string &csvFilePath);
   ~CsvWriter();
   int PutRow(const stringlist &row);

private:
   std::ofstream *fileStream;
};

#endif // _CSV_H_
