/*
   File Name:  FileSweeper.h
   Created: July 3, 2001 by John Mertus
   Version 1.00

   This quick and dirty routine creates a method for calling a method
   for EACH file name in a wild card specifier.
     E.g, if the specifier is /u/mertus/V*.c
          then a method will be called for each V*.c file in /u/mertus

   Note, this routine behaves the exact same way in windows and unix.

   The simplest way to use is
    class Test
{
    public:
    // The following macro defines
    //     bool LoadFonts(string Mask);
    DEFINE_FILE_SWEEPER(LoadFonts, LoadFont, false, Test);

    private:
    bool LoadFont(string In);
}


   Sometimes you wish to define the wild card method yourself,
   for example, you want a function LoadFonts() without wildcards.

     class Test
      {
         public:
           void LoadAllFonts(void);
          .
          .
          .

         private:
           DEFINE_FILE_SWEEP_CB(LoadFontCB, Test);
      }

     bool Test::LoadFontCB(string Name)
     {
        // Load the font from the file name Name
        return(true);
     }

     void Test::LoadAllFonts(void)
     {
          FILE_SWEEP(LoadFontCB, "*.ttf");
     }

     Of course this could also be done via
    class Test
{
    public:
    void LoadAllFonts(void) {LoadFonts("*.ttf"); };

    private:
    // The following macro defines
    //     bool LoadFonts(string Mask);
    DEFINE_FILE_SWEEPER(LoadFonts, LoadFont, false, Test);
    bool LoadFont(string In);
}

*/
#ifndef FileSweeperH
#define FileSweeperH

#include "corecodelibint.h"
#include <string>
#include "machine.h"
#include "TheError.h"
#include "err_core_code.h"

using std::string;


//
// These break the files back into their parts
// /dirpath/name.ext
//  GetFilePath of the above return /dirpath/  (Include following  /)
//  GetFileName of the above return name
//  GetFileExt of the above return .ext    (Includes the .)
//  In the case of name.ext.xxx the return is .xxx
//
//  AddFileExt adds a file extension to a name if there is no one
//  ReplaceFileExt replaces a file extensions to a name if there is no one
//
MTI_CORECODELIB_API string GetFilePath(const string &Name);
MTI_CORECODELIB_API string GetPathRoot(const string &Name);
MTI_CORECODELIB_API string GetFileLastDir(const string &Name);
MTI_CORECODELIB_API string GetFileAllButLastDir(const string &Name);
MTI_CORECODELIB_API string GetFileName(const string &Name);
MTI_CORECODELIB_API string GetFileExt(const string &Name);
MTI_CORECODELIB_API string GetFileNameWithExt(const string &Name);
MTI_CORECODELIB_API string AddFileExt(const string &Name, const string &Ext);
MTI_CORECODELIB_API string ReplaceFileExt(const string &Name, const string &Ext);
MTI_CORECODELIB_API bool DoesFileExist(const string &Name);
MTI_CORECODELIB_API MTI_UINT64 GetFileLength(const string &Name);
MTI_CORECODELIB_API bool GetBytesPerSector(char *caFilename, unsigned long *ulpBytesPerSector);
MTI_CORECODELIB_API bool DoesDirectoryExist(const string &Name);
MTI_CORECODELIB_API bool DoesWritableDirectoryExist(const string &Name);
MTI_CORECODELIB_API bool DoesWritableFileExist(const string &Name);
MTI_CORECODELIB_API bool MakeDirectory(const string &Name);
MTI_CORECODELIB_API bool MakeDirectoryBackwards(const string &Name);
MTI_CORECODELIB_API bool MakeHiddenDirectory(const string &Name);

MTI_CORECODELIB_API bool MTICopyFile(const string &existingFilePath,
				     const string &newFilePath,
				     bool failIfNewFileExists);
MTI_CORECODELIB_API int CopyAllRegularFiles(const string &srcDir,
                                            const string &dstDir,
                                            bool skipExistingFiles,
                                            bool skipIniBackupFiles,
                                            const StringList &excludeList,
                                            string& reason);

MTI_CORECODELIB_API string AddDirSeparator(const string &Name);
MTI_CORECODELIB_API string RemoveDirSeparator(const string &Name);
MTI_CORECODELIB_API string CombinePathAndName(const string &path, const string &name);
MTI_CORECODELIB_API string CombinePathNames(const vector<string> &names, const string &ext);

MTI_CORECODELIB_API string RemoveWhiteSpace(const string &Name);
MTI_CORECODELIB_API string AddSubfolder(const string &Name, const string &Subfolder);
MTI_CORECODELIB_API string RemoveSubfolder(const string &Name, const string &Subfolder);
MTI_CORECODELIB_API string ExpandRelativePaths(const string &FileName, const string &AbsPath);

MTI_CORECODELIB_API string GetSystemErrorMessage(int errorCode);
MTI_CORECODELIB_API string GetLastSystemErrorMessage(void);

// Some string processing routines, here because this is very low level
//
MTI_CORECODELIB_API bool StringAllDigits(const string &s);
MTI_CORECODELIB_API string StringTrimLeft(const string &s);
MTI_CORECODELIB_API string StringTrimRight(const string &s);
MTI_CORECODELIB_API string StringTrim(const string &s);

MTI_CORECODELIB_API void FindRightmostDigitRun(const string& inStr,
                                 /* OUTPUT */  int& digitIndex,
                                 /* OUTPUT */  int& digitCount);

MTI_CORECODELIB_API MTI_UINT64 GetLastModifiedTime(const string &filename);

// The Sweep procedure method
//   User writes this method
//   Return true to continue processing, false to abort
//
typedef bool (FileSweepProc)(void *, string);
MTI_CORECODELIB_API bool FileSweeper(void *, FileSweepProc, const string &, bool Abort = true);
MTI_CORECODELIB_API bool SweepDown(void *, FileSweepProc, const string &, bool bAbort=true);

//---------------DEFINE_FILE_SWEEP_CB-------------------John Mertus---Jul 2001--
//
//  This is used by an external class to set the call back to that class
//   CBName is the callback function which
//             is a method of the form void CBName(string name)

#define DEFINE_FILE_SWEEP_CB(CBName, CL) \
  bool CBName(string name); \
  static bool _Static##CBName(void *t, string name) {return reinterpret_cast<CL *>(t)->CBName(name); }

//---------------FILE_SWEEP-----------------------------John Mertus---Jul 2001--
//
//  This is used to invoke the sweeping from within the same class
//   CBName is the callback function defined above
//   Mask is the file specifier
//
#define FILE_SWEEP(CBName, Mask) \
  FileSweeper(this, _Static##CBName, Mask)

//-----------FILE_SWEEP_EXT-----------------------------John Mertus---Jul 2001--
//
//  This is used to invoke the sweeping from an external class
//   CBName is the callback function defined above
//   CL is the class of the callback (usually this)
//   Mask is the file specifier
//
#define FILE_SWEEP_EXT(CBName, Mask, CL) \
  FileSweeper(CL, _Static##CBName, Mask)

//---------------DEFINE_FILE_SWEEPER-----------------------------John Mertus---Jul 2001--
//
//  This is used to invoke the sweeping from within the same class
//   CBName is the callback function defined above
//   Mask is the file specifier
//
#define DEFINE_FILE_SWEEPER(SweepName, TargetName, Abort, CL) \
  static bool _Static##SweepName(void *t, string name) {return reinterpret_cast<CL *>(t)->SweepName(name); } \
  static bool _Static##TargetName(void *t, string name) {return reinterpret_cast<CL *>(t)->TargetName(name); } \
  bool SweepName(string Mask) {return(FileSweeper(this, _Static##TargetName, Mask, Abort));}


#define FILE_SWEEPER_CALL(Name) _Static##Name
#ifdef _WINDOWS
#define DIR_SEPARATOR '\\'
#define sDIR_SEPARATOR "\\"
#define DIR_DOUBLESEPARATOR "\\\\"
#else
#define DIR_SEPARATOR '/'
#define sDIR_SEPARATOR "/"
#define DIR_DOUBLESEPARATOR "//"
#endif

//---------------LONG CLIP DEBUG HACK-----------------------------
//
MTI_CORECODELIB_API string GetLongClipDebugHackFileName(const string &Name);


#endif // _FileSweeper

