#include "machine.h"
#include "SharedMemClass.h"
#include <process.h>

// This class simplifies the process of creating a region of shared memory.
//  In Win32, this is accomplished by using the CreateFileMapping and
//  MapViewOfFile functions.

//-------------------TSharedMem-------------------John Mertus----Aug 2003-----

TSharedMem::TSharedMem(const string &Name, int Size)

// This contructs (or maps) the memory section of name Name with size Size
//  Note: Created needs to be checked after the call to see if this succeeded
//
//****************************************************************************
{
  try{
     _Name = Name;
     _Size = Size;

      // CreateFileMapping, when called with $FFFFFFFF for the handle value,
      // creates a region of shared memory. If an object with same name already
      // exists, then a Handle to that object will be returned.
      _Handle = CreateFileMapping((HANDLE)-1,
                                   NULL,
                                   PAGE_READWRITE,
                                   0,
                                   Size,
                                   Name.c_str());

	  unsigned int err = GetLastError();
	  _Created = (err == 0);
	  if (_Handle == 0) throw this;

      // We still need to map a pointer to the handle of the shared memory region
      _Buffer = MapViewOfFile(_Handle, FILE_MAP_WRITE, 0, 0, Size);
      if (_Buffer == NULL) throw this;
  }

  catch(...)
   {
   }
}

//------------------~TSharedMem-------------------John Mertus----Aug 2003-----

   TSharedMem::~TSharedMem()

//   Usual destructor, clean up all mappings
//
//****************************************************************************
{
	if (_Buffer != NULL) UnmapViewOfFile(_Buffer);
	if (_Handle != 0) CloseHandle(_Handle);
}

//----------------------Name----------------------John Mertus----Aug 2003-----

   const string &TSharedMem::Name(void)

//  Return the name of the mapped file
//
//****************************************************************************
{
	return _Name;
}

//----------------------Size----------------------John Mertus----Aug 2003-----

int TSharedMem::Size(void)

//  Return the size of the mapped segement
//
//****************************************************************************
{
	return _Size;
}
//---------------------Buffer---------------------John Mertus----Aug 2003-----

   void *TSharedMem::Buffer(void)

//  This returns the buffere pointer of the mapped file
//
//****************************************************************************
{
	return _Buffer;
}
//--------------------Created---------------------John Mertus----Aug 2003-----

   bool TSharedMem::Created(void)

//   Return true if the section was created, false otherwise
//
//****************************************************************************
{
	return _Created;
}


