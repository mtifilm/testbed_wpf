/*
   File Name:  DllSupport.cpp
   Create:  July 11, 2001 by John Mertus

   This is a catchall library which defines certain functions necessary
   to support windows and DLL.
*/

//#include <Windows.h>
#ifdef _WINDOWS
#define _WIN32_WINNT 0x0400 // require at least NT 4.0 for CreateWaitableTimer
#include <windows.h>
#endif
#include <shlobj.h>
#include "machine.h"
#include "HRTimer.h"
#include "IniFile.h"
#include "TheError.h"
#include "DllSupport.h"
#include "MTIstringstream.h"
#include <process.h>
#include <psapi.h>
#include <strsafe.h>
#include <regex>


/*-----------GetSystemErrorMessage------------------------JAM--Mar 2000----*/

  string GetSystemMessage(int ErrorCode)

/* This function returns a string that contains a human readable     */
/* version of the error message in ErrorCode                         */
/* Ususal usage is ErrorMessage = GetSystemMessage(GetLastError());  */
/*                                                                   */
/*********************************************************************/
{
  char cErrorMessage[255];

  string Result = "";

  // Do nothing if not an error
  if (ErrorCode == 0)
    {
       return(Result);
    }

  FormatMessage
          (
             FORMAT_MESSAGE_FROM_SYSTEM,
             NULL,
             ErrorCode,
             MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
             (LPTSTR) &cErrorMessage,
             255,
             NULL
          );

  Result = cErrorMessage;
  return(Result);
};


//--------------GetDLLInfo---------------------John Mertus Jan 2001---

       string GetDLLInfo(string name, string Info)

//  Return information about a file. the following information is available
//
//   'CompanyName', 'FileDescription', 'FileVersion', 'InternalName',
//   'LegalCopyright', 'LegalTradeMarks', 'OriginalFilename',
//   'ProductName', 'ProductVersion', 'Comments'
//
//*********************************************************************
{
  DWORD dwLen;
  void *Value;
  unsigned int Len;

  int n = GetFileVersionInfoSize(name.c_str(), &dwLen);
  if (n == 0) return("");
  void *Data = malloc(n);
  if (!GetFileVersionInfo(name.c_str(),0,n,Data))
    {
       free(Data);
       return("");
    }

  // We should be looking up the page
  string codePages[] = {"StringFileInfo\\040904B0\\", "StringFileInfo\\040904E4\\"};

  for (auto codePage : codePages)
  {
    auto s = codePage + Info;
    if (VerQueryValue(Data,__TEXT((char *)s.c_str()),&Value, &Len))
    {
       string Result = (char *)Value;
       free(Data);
       return Result;
    }
  }

  free(Data);
  return("");


}

//--------------GetDLLVersion------------------John Mertus Jan 2001---

     string GetDLLVersion(string Name)

//  This returns the current dll version as a string.
//
//*********************************************************************
{
  string s = GetDLLInfo(Name, "FileVersion");
  if (s == "") return("Unknown");
  return(s);
/*
  int n = s.find('.');
  string V = s.substr(n+1, s.length());
  int m = V.find('.') + n +1;
  V = s.substr(0, m);
  V = V + ", Release " + s.substr(m+1, s.length());

  return(V);
*/
}

std::pair<int, int> GetDLLMajorAndMinorVersionNumbers(string Name)
{
   string s = GetDLLInfo(Name, "FileVersion");

   std::regex rx(R"(^(\d+)\.(\d+)\.)");
   std::smatch m;

   std::pair<int, int> majorAndMinor = {0, 0};
   std::regex_search(s, m, rx);
   MTIassert(!m.empty());
   if (m.empty())
   {
      TRACE_0(errout << "DLL version has unexpected format: " << s);
      return majorAndMinor;
   }

   std::istringstream is1(m[1]);
   std::istringstream is2(m[2]);
   is1 >> majorAndMinor.first;
   is2 >> majorAndMinor.second;

   return majorAndMinor;
}

//--------------GetDLLCopyright------------------John Mertus Jan 2001---

     string GetDLLCopyright(string Name)

//  This returns the current dll copyright as a string.
//
//*********************************************************************
{
  return(GetDLLInfo(Name, "LegalCopyright"));
}

//--------------GetFileDateString---------------John Mertus Jan 2001---

   string GetFileDateString(string Name)

//  This returns the last write date of a file.
//
//*********************************************************************
{

  const string MonthsStr[] =
    {"January", "February", "March", "April",
     "May", "June", "July", "August",
     "September", "October","November", "December"};

  FILETIME CreationTime;
  SYSTEMTIME SystemTime;
  // Get the time of the file creation
  HANDLE h = CreateFile(Name.c_str(), GENERIC_READ,
             FILE_SHARE_READ | FILE_SHARE_WRITE, NULL,
             OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL, NULL);

  if (h == INVALID_HANDLE_VALUE) return("");

  // Now find the time
  if (!GetFileTime(h, NULL, NULL, &CreationTime))
    {
       CloseHandle(h);
       return("");
    }

  CloseHandle(h);  // No need for it anymore

  if (!FileTimeToSystemTime(&CreationTime, &SystemTime)) return("");

  // Create our date
  MTIostringstream str;
  str << MonthsStr[SystemTime.wMonth-1] << " " << SystemTime.wDay << ", " << SystemTime.wYear;

  return(str.str());
}


// Used to communicate between the GetUserFolder and the BrowseCallback
// TBD replace this with proper non-static
static char *StaticBrowseFolder;

//--------------BrowseCallbackProc-------------------John Mertus Nov 2002---

  int CALLBACK BrowseCallbackProc(HWND  hwnd, UINT  uMsg, LPARAM  lParam, LPARAM  lpData)

//  This is used by the GetUserFolder to set the title of the dialog box.
//
//***************************************************************************
{

  switch ( uMsg )
  {
    case BFFM_INITIALIZED:
      SendMessage ( hwnd, BFFM_SETSELECTION, TRUE, (LPARAM)StaticBrowseFolder );
      break;

    default:
      break;

  } return 0;
}

//--------------GetUserFolder-------------------John Mertus Nov 2002---

  char *GetUserFolder (void *hPar, const char *StartFolder, const char *title )

//  This uses the system object to request a new file folder
//  Returns NULL if "Cancel" button clicked from SHBrowseForFolder()
//
//*********************************************************************

{
  BROWSEINFO binf;
  ITEMIDLIST *itemId;
  LPMALLOC g_pMalloc;
  static char BrowseDir[256];
  size_t i = 0;

  // setup browse info
  StaticBrowseFolder = BrowseDir;
  binf.hwndOwner = (HWND)hPar;
  binf.pidlRoot = NULL;
  binf.pszDisplayName = BrowseDir;
  binf.lpszTitle = title;
  binf.ulFlags = BIF_RETURNONLYFSDIRS;
  binf.lpfn = BrowseCallbackProc;
  binf.iImage = 0;

  // get shell malloc handle
  SHGetMalloc(&g_pMalloc);

  strcpy(BrowseDir, StartFolder);
  i = strlen (BrowseDir );
  // spec is not 'c:\'
  if ( i > 3 )
  {
    if ( BrowseDir[i-1] == '\\' )
      BrowseDir[i-1] = 0;
  }
  // invoke browse dlg
  itemId = SHBrowseForFolder(&binf);

  if (itemId)
   {
     SHGetPathFromIDList (itemId, BrowseDir);
   }
  else
   BrowseDir[0] = 0;

  // free itemid list, this is from a win example, ye gods...
  g_pMalloc->Free(itemId);
  g_pMalloc->Release();

  i = strlen ( BrowseDir );

  if (i > 0)  // Append \ only if not null
    if ( BrowseDir[i-1] != '\\' )
      strcat ( BrowseDir, "\\" );

  return (char *)BrowseDir;
}


#define ACCESS_READ  1
#define ACCESS_WRITE 2

bool IsAdmin(void)
{
 HANDLE hToken;
 DWORD  dwStatus;
 DWORD  dwAccessMask;
 DWORD  dwAccessDesired;
 DWORD  dwACLSize;
 DWORD  dwStructureSize = sizeof(PRIVILEGE_SET);
 PACL   pACL            = NULL;
 PSID   psidAdmin       = NULL;
 BOOL   bReturn         = FALSE;
 PRIVILEGE_SET   ps;
 GENERIC_MAPPING GenericMapping;
 PSECURITY_DESCRIPTOR     psdAdmin           = NULL;
 SID_IDENTIFIER_AUTHORITY SystemSidAuthority = SECURITY_NT_AUTHORITY;

 if(!ImpersonateSelf(SecurityImpersonation))
  goto LeaveIsAdmin;

 if (!OpenThreadToken(GetCurrentThread(), TOKEN_QUERY, FALSE, &hToken))
 {
  if (GetLastError() != ERROR_NO_TOKEN)
   goto LeaveIsAdmin;

  if (!OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY, &hToken))
   goto LeaveIsAdmin;

  if (!OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY, &hToken))
   goto LeaveIsAdmin;
 }

 if (!AllocateAndInitializeSid(&SystemSidAuthority, 2,
  SECURITY_BUILTIN_DOMAIN_RID, DOMAIN_ALIAS_RID_ADMINS,
  0, 0, 0, 0, 0, 0, &psidAdmin))
  goto LeaveIsAdmin;

 psdAdmin =
   (PSECURITY_DESCRIPTOR)LocalAlloc(LPTR, SECURITY_DESCRIPTOR_MIN_LENGTH);
 if (psdAdmin == NULL)
  goto LeaveIsAdmin;

 if (!InitializeSecurityDescriptor(psdAdmin,
  SECURITY_DESCRIPTOR_REVISION))
  goto LeaveIsAdmin;

 dwACLSize = sizeof(ACL) + sizeof(ACCESS_ALLOWED_ACE) +
     GetLengthSid(psidAdmin) - sizeof(DWORD);

 pACL = (PACL)LocalAlloc(LPTR, dwACLSize);
 if (pACL == NULL)
  goto LeaveIsAdmin;

 if (!InitializeAcl(pACL, dwACLSize, ACL_REVISION2))
  goto LeaveIsAdmin;

 dwAccessMask= ACCESS_READ | ACCESS_WRITE;

 if (!AddAccessAllowedAce(pACL, ACL_REVISION2, dwAccessMask, psidAdmin))
  goto LeaveIsAdmin;

 if (!SetSecurityDescriptorDacl(psdAdmin, TRUE, pACL, FALSE))
  goto LeaveIsAdmin;

 if(!SetSecurityDescriptorGroup(psdAdmin, psidAdmin, FALSE))
  goto LeaveIsAdmin;
 if(!SetSecurityDescriptorOwner(psdAdmin, psidAdmin, FALSE))
  goto LeaveIsAdmin;

 if (!IsValidSecurityDescriptor(psdAdmin))
  goto LeaveIsAdmin;

 dwAccessDesired = ACCESS_READ;

 GenericMapping.GenericRead    = ACCESS_READ;
 GenericMapping.GenericWrite   = ACCESS_WRITE;
 GenericMapping.GenericExecute = 0;
 GenericMapping.GenericAll     = ACCESS_READ | ACCESS_WRITE;

 if (!AccessCheck(psdAdmin, hToken, dwAccessDesired,
  &GenericMapping, &ps, &dwStructureSize, &dwStatus, &bReturn))
  goto LeaveIsAdmin;

 if(!RevertToSelf())
  bReturn = FALSE;

LeaveIsAdmin:

 if (pACL) LocalFree(pACL);
 if (psdAdmin) LocalFree(psdAdmin);
 if (psidAdmin) FreeSid(psidAdmin);

 return bReturn;
}

//-----------KillProgramByPID------------------------JAM--Mar 2002---

  bool KillProgramByPID(DWORD ProcessID)

//  This kills the window given a window handle h
//  Return is true if the program no longer exists, even if it was never there
//  False if it could not be killed.
//
//*********************************************************************
{
    HANDLE ProcHandle = OpenProcess(PROCESS_ALL_ACCESS, false, ProcessID);
    if (!TerminateProcess(ProcHandle, 0))
      {
         CloseHandle(ProcHandle);
         theError.set(GetSystemMessage(GetLastError()), GetLastError());
         return false;
      }

    CloseHandle(ProcHandle);
    return true;
}

//-----------KillProgramByWinHandle------------------------JAM--Mar 2002---

  bool KillProgramByWinHandle(const HWND h)

//  This kills the window given a window handle h
//  Return is true if the program no longer exists, even if it was never there
//  False if it could not be killed.
//
//*********************************************************************
{
    if (h == NULL) return true;

    DWORD Ret=0;
	PDWORD_PTR pRet = (PDWORD_PTR) &Ret;
    SendMessageTimeout(h, WM_CLOSE, 0, 0, SMTO_ABORTIFHUNG	| SMTO_BLOCK, 250, pRet);

    // Wait up to 1 second for termination, if not, kill it
    CHRTimer HRT;
    HRT.Start();
    DWORD ProcessID = 0;
    while (HRT.Read() < 1000)
      {
        Sleep(10);
        ProcessID = 0;
        GetWindowThreadProcessId(h, &ProcessID);
        if (ProcessID == 0) return true;
      }

    // WM_Close did not work, kill the process
    KillProgramByPID(ProcessID);
    return true;
}


//-------------KillNamedWindow------------------------JAM--Mar 2002----

  bool KillNamedWindow(const string &WinName)

//  This find the window with the caption WinName and kills it
//  Return is true if the program no longer exists, even if it was never there
//  False if it could not be killed.
//
//*********************************************************************
{
    return KillProgramByWinHandle(FindWindow(NULL, WinName.c_str()));
}

//-------------EqualNoCase-------------John Mertus--Aug 2001---------

  bool EqualNoCase(const string &S1, const string &S2)

// This checks if the strings S1 and S2 are the same, case insensitive.
//
//***********************************************************************
{
   if (S1.length() != S2.length()) return(false);
   for (unsigned i=0; i < S1.length(); i++)
        if (toupper(S1[i]) !=  toupper(S2[i])) return(false);
   return(true);
}

string GetBaseNameFromPID(DWORD pid)
{
  HANDLE ProcHandle = OpenProcess(
                  PROCESS_QUERY_INFORMATION | PROCESS_VM_READ,
                  false, pid);
  if (ProcHandle == NULL) return("");

  HMODULE Module;
  DWORD NumModules;
  char FileName[MAX_PATH];

  // Get the first module
  if (!EnumProcessModules(ProcHandle, &Module, sizeof(HMODULE), &NumModules))
  {
	  CloseHandle(ProcHandle);
	  return "";
  }

  // Get the base name
  if (!GetModuleBaseName(ProcHandle, Module, FileName, sizeof(FileName)))
  {
	  CloseHandle(ProcHandle);
	  return "";
  }

  CloseHandle(ProcHandle);
  string strResult = FileName;
  return strResult;
}

//-------------KillAWindow-------------John Mertus--Feb 2003---------

  BOOL CALLBACK KillAWindow(HWND hwnd, LPARAM lParam)

//  This is used by EnumWindows to kill any window that matches
//  the lParam but is not ones own self.
//
//  lParam is the case insensitive base file name of the process
//  e.g., EditControllerWin.exe
//
//  Return is always true.
//
//***********************************************************************
{
  string str = (char *)lParam;

  DWORD ProcessID = 0;
  GetWindowThreadProcessId(hwnd, &ProcessID);
  if (ProcessID == 0) return true;
  if (ProcessID == GetCurrentProcessId()) return true;

  // Now get the GetFileName(Application->ExeName.c_str())
  HANDLE ProcHandle = OpenProcess(PROCESS_ALL_ACCESS, false, ProcessID);
  HMODULE Module;
  DWORD NumModules;
  char FileName[MAX_PATH];

  // Get the first module
  if (!EnumProcessModules(ProcHandle, &Module, sizeof(HMODULE), &NumModules))
  {
	  CloseHandle(ProcHandle);
	  return true;
  }

  // Get the base name
  if (!GetModuleBaseName(ProcHandle, Module, FileName, sizeof(FileName)))
  {
	  CloseHandle(ProcHandle);
	  return true;
  }

  // If base name matches kill it
  if (EqualNoCase(str, FileName))
    {
       KillProgramByWinHandle(hwnd);
    }

  CloseHandle(ProcHandle);
  return true;
}

//-------------KillAllWindowsButMe-------------John Mertus--Feb 2003---------

    void KillAllWindowsButMe(const string &WinName)

//  This sweeps through all top windows of the system and kills
//  all programs whose file name is WinName
//
//  WinName is the case insensitive base file name of the process
//  e.g., "EditControllerWin.exe"
//
//
//***********************************************************************
{
   EnumWindows((WNDENUMPROC)KillAWindow, (long)WinName.c_str());
}

//-------------UpdateTheFuckingSysTray-------------John Mertus--Feb 2003---------

   void UpdateTheSysTray(void)

//  This truely ugly code does a fake mouse move in the systray
//  This seems to be the only way to get the systray to update itself
//  and remove dead icons.
//
//***********************************************************************
{
    HWND hwndTaskBar, hwndTrayWnd, hwndTrayToolBar;
    hwndTaskBar  = FindWindowEx (NULL, NULL, "Shell_TrayWnd", NULL);
    hwndTrayWnd = FindWindowEx (hwndTaskBar , NULL, "TrayNotifyWnd", NULL);
    hwndTrayToolBar = FindWindowEx(hwndTrayWnd, NULL, "ToolbarWindow32", NULL);
    RECT rTrayToolBar;
    GetClientRect(hwndTrayToolBar, &rTrayToolBar);
    for (WORD x = 0; x < rTrayToolBar.right ; x += 8)
     for (WORD y = 0; y < rTrayToolBar.bottom ; y +=8)
       SendMessage(hwndTrayToolBar , WM_MOUSEMOVE, 0, MAKELPARAM(x,y) );
}

#define  ListSize 65535
#define  ItemSize sizeof(DWORD)

//-------------KillAllProgramsButMe-----------John Mertus--Feb 2003---------

    void KillAllProgramsButMe(const string &WinName)

//  This sweeps through all PID's of the system and kills
//  all programs whose file name is WinName
//
//  WinName is the case insensitive base file name of the process
//  e.g., "EditControllerWin.exe"
//
//  To gracefully try to terminate the programs, a KillAllWindowsButMe
//  is first executed.
//
//***********************************************************************
{
   KillAllWindowsButMe(WinName);
   int Count;
   DWORD cbNeeded;
   DWORD *PIDList;
   DWORD pid;

   PIDList = new DWORD[ListSize];
   if (!EnumProcesses(PIDList, ListSize, &cbNeeded)) return;
   Count = cbNeeded/ItemSize;
   for (int i = 0; i <= Count; i++)
     {
        pid = PIDList[i];
        // Kill All but self
        if (pid != GetCurrentProcessId())
          if (EqualNoCase(GetBaseNameFromPID(pid), WinName))
            {
               KillProgramByPID(pid);
               // Remove any dead systray icons
               Sleep(10);  // Wait a little while
               UpdateTheSysTray();
            }
     }

   delete[] PIDList;
   UpdateTheSysTray();

}
