#include "SharedMap.h"

map<string, void *> SharedMap::TheMap;

void SharedMap::add(const string &key, void *value)
{
   TheMap[key] = value;
}

void SharedMap::remove(const string &key)
{
   TheMap[key] = NULL;
}

void *SharedMap::retrieve(const string &key)
{
   return TheMap[key];
}

void *SharedMap::getMap()
{
   return &TheMap;
}



