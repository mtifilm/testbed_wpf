/*
	A POSIX compliant sleep routines.

	Based on nanosleep ().

	Functions:
	MTIsleep (double dArg), where dArg is the number of seconds
	MTInanosleep (int iArg), where iArg is the number of nanoseconds
	MTImicrosleep (int iArg), where iArg is the number of microseconds
	MTImillisleep (int iArg), where iArg is the number of milliseconds
*/
#include "corecodelibint.h"

#ifndef MTIsleepH
#define MTIsleepH
MTI_CORECODELIB_API void MTIsleep (double dArg);
MTI_CORECODELIB_API void MTInanosleep (int iArg);
MTI_CORECODELIB_API void MTImicrosleep (int iArg);
MTI_CORECODELIB_API void MTImillisleep (int iArg);

#endif

