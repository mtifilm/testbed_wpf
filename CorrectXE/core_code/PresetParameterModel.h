// ---------------------------------------------------------------------------

#ifndef PresetParameterModelH
#define PresetParameterModelH

#include "IPresetParameters.h"
#include <map>
using std::map;

// ---------------------------------------------------------------------------

class IPresets
{
public:
	virtual int GetSelectedIndex() const = 0;
	virtual void SetSelectedIndexAndCopyParametersToCurrent(int presetIndex) = 0;
	virtual void ReadPresetsFromIni(const string &iniFileName) = 0;
	virtual void WritePresetsToIni(const string &iniFileName) const = 0;
	virtual bool AreParametersTheSameAsCurrent(const IPresetParameters *presetParameters) const = 0;
	virtual void CopyCurrentParametersToSelectedPreset() = 0;
	virtual void SetCurrent(const IPresetParameters *presetParameters) = 0;
	virtual bool DoesCurrentMatchSelected() = 0;
	virtual int GetNumberOfPresets() const = 0;
	virtual IPresetParameters *GetSelectedPreset() = 0;

	CBHook SelectedIndexChanged;
	CBHook CurrentParametersChanged;
};

// ---------------------------------------------------------------------------
//
// This maintains presets, which are subclasses of the IPresetParameters interface
// whose inisections are named [<name>.Preset<n>]
// PresetModel("GrainParameters", 2)
// creates two presets with section names GrainParameters.Preset0, GrainParameters.Preset1
//
// Current is the current preset displayed, this is mapped to the gui
// As the user changes the gui, current changes.  When the save button is clicked
// the current is copied to the selected.
//
// There is a selected preset defined by a selected index into the preset list (presets)
//
// Basically, as the user clicks a preset button, the number of that button, 0, 1, ...
// the selected index is changed and the selecte preset are copied to the current preset.
// This initiates a CurrentPresetChange.
//
// Hook the SelectedIndexChanged to process any preset index change
//
// ---------------------------------------------------------------------------
template<typename T>
class PresetParameterModel : public map<int, T>, public IPresets
{
public:
	PresetParameterModel(const string &prefix, int numberOfPresets)
   {
		_prefix = prefix;
		_selectedIndex = -1;
		_numberOfPresets = numberOfPresets;

		// This is just to avoid list being empty before ReadParameters is called
		// Note that we initialize the Tag to be the preset index, but we no
      // longer rely on that when writing out the presets to the ini file.
      for (int i = 0; i < numberOfPresets; i++)
      {
			T t;
			t.Tag = i;
			map<int, T>::operator[](i) = t;
      }
	}

   virtual ~PresetParameterModel()
   {
   }

	int GetSelectedIndex() const
   {
      return _selectedIndex;
   }

	void SetSelectedIndexAndCopyParametersToCurrent(int presetIndex)
	{
		MTIassert(presetIndex < _numberOfPresets);
		if (presetIndex < 0 || presetIndex >= _numberOfPresets)
		{
         _selectedIndex = -1;
			return;
		}

      if (!DoesCurrentMatchSelected() || (_selectedIndex != presetIndex))
      {
         _selectedIndex = presetIndex;
			_currentPreset = map<int, T>::at(_selectedIndex);
			SelectedIndexChanged.Notify();
			CurrentParametersChanged.Notify();
		}
   }

   void ReadPresetsFromIni(const string &iniFileName)
   {
      CIniFile *ini = CreateIniFile(iniFileName);

////      map<int, T>::clear();

//      for (int i = 0; i < _numberOfPresets; i++)
//      {
//         T t;
//         MTIostringstream os;
//			os << _prefix << ".Preset" << i;
//			t.ReadParameters(ini, os.str());
//         t.Tag = i;
//			map<int, T>::operator[](i) = t;
//		}

      for (int i = 0; i < _numberOfPresets; i++)
      {
         MTIostringstream os;
			os << _prefix << ".Preset" << i;
			(*this)[i].ReadParameters(ini, os.str());
		}

      DeleteIniFile(ini);
   }

   void WritePresetsToIni(const string &iniFileName) const
   {
      int n = map<int, T>::size();

      CIniFile *ini = CreateIniFile(iniFileName);

		for (auto presetIndex = 0; presetIndex < _numberOfPresets; ++presetIndex)
		{
			MTIostringstream os;
			os << _prefix << ".Preset" << presetIndex;

			map<int, T>::at(presetIndex).WriteParameters(ini, os.str());
      }

      DeleteIniFile(ini);
   }

   bool AreParametersTheSameAsCurrent(const T &presetParameters) const
   {
      return _currentPreset.AreParametersTheSame(presetParameters);
   }

	bool AreParametersTheSameAsCurrent(const IPresetParameters *parameters) const
	{
		const T *presetParameters = dynamic_cast<const T *>(parameters);
		MTIassert(presetParameters != nullptr);
		if (presetParameters == nullptr)
		{
			return false;
		}

		return _currentPreset.AreParametersTheSame(*presetParameters);
   }

	void CopyCurrentParametersToSelectedPreset()
	{
		if (_selectedIndex < 0 || _selectedIndex >= _numberOfPresets)
		{
         // No preset has been selected yet.
			return;
		}

		map<int, T>::at(_selectedIndex) = _currentPreset;
   }

	void SetCurrent(const T &presetParameters)
	{
		_currentPreset = presetParameters;
		CurrentParametersChanged.Notify();
   }

	void SetCurrent(const IPresetParameters *parameters)
	{
		const T *presetParameters = dynamic_cast<const T *>(parameters);
		MTIassert(presetParameters != nullptr);
		if (presetParameters == nullptr)
		{
			return;
		}

		SetCurrent(*presetParameters);
	}

   bool DoesCurrentMatchSelected()
   {
		if (_selectedIndex < 0 || _selectedIndex >= _numberOfPresets)
		{
         // No preset has been selected yet.
         return false;
      }

		return map<int, T>::at(_selectedIndex).AreParametersTheSame(_currentPreset);
	}

   int GetNumberOfPresets() const
   {
      return _numberOfPresets;
   }

   T &GetCurrentPreset()
   {
      return _currentPreset;
   }

	IPresetParameters *GetSelectedPreset()
   {
		// Index may be out of range so let that generate an error
		MTIassert(_selectedIndex < _numberOfPresets);
		if (_selectedIndex < 0 || _selectedIndex >= _numberOfPresets)
		{
			return nullptr;
		}

		return &(map<int, T>::at(_selectedIndex));
   }

   // Used only for debugging
   T &GetPreset(int i)
   {
      return map<int, T>::at(i);
   }

private:
   void clearList()
   {
		for (auto preset : *this)
		{
         delete preset;
      }

		map<int, T>::clear();
   }

private:
   int _selectedIndex;
	int _numberOfPresets;
	string _prefix;
   T _currentPreset;

};

// ---------------------------------------------------------------------------
#endif
