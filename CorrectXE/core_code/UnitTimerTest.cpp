//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UnitTimerTest.h"
#include "MTI_UST.h"
#include <mmsystem.h>
#include <winsock.h>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)
{
  MTI_INT64 llTmp;
  int iNum;
  sscanf (Edit1->Text.c_str(), "%d", &iNum);

  Screen->Cursor = crHourGlass;

  int iRet = 0;
  hrt.Start();
  for (int iCnt = 0; iCnt < iNum; iCnt++)
   {
    iRet |= mtiGetUST (&llTmp);
   }
  float fMilliSeconds = hrt.Read();


  if (iRet)
   {
    ShowMessage ("mtiGetUST returned an error");
   }

  float fRate = fMilliSeconds / (float)(iNum);

  char caMessage[64];
  sprintf (caMessage, "mSec per call:  %f", fRate);
  Label1->Caption = caMessage;

  Screen->Cursor = crArrow;                   
}

void __stdcall CallbackFcn (UINT uTimerID, UINT uMsg, DWORD dwUser, DWORD dw1,
        DWORD dw2)
{
  Form1->Button2Finish ();
}

void TForm1::Button2Finish ()
{
  float fMilliSeconds = hrt.Read();

  char caMessage[64];

  sprintf (caMessage, "Actual mSec: %f", fMilliSeconds);
  Label2->Caption = caMessage;

  Screen->Cursor = crArrow;
}

//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
  int iNum;
  sscanf (Edit2->Text.c_str(), "%d", &iNum);

  Screen->Cursor = crHourGlass;

  hrt.Start();

  if (timeSetEvent (iNum, 0, CallbackFcn, 0, TIME_ONESHOT) == NULL)
   {
    ShowMessage ("Unable to timeSetEvent()");
   }

   Screen->Cursor = crArrow;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
  int iNum;
  sscanf (Edit3->Text.c_str(), "%d", &iNum);

  Screen->Cursor = crHourGlass;

  hrt.Start();

  Sleep (iNum);

  float fMilliSeconds = hrt.Read();

  char caMessage[64];

  sprintf (caMessage, "Actual mSec: %f", fMilliSeconds);
  Label3->Caption = caMessage;

  Screen->Cursor = crArrow;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button4Click(TObject *Sender)
{
  float fNum;
  sscanf (Edit4->Text.c_str(), "%f", &fNum);

  Screen->Cursor = crHourGlass;

  hrt.Start();

  hrt.Delay (fNum);

  float fMilliSeconds = hrt.Read();

  char caMessage[64];
  sprintf (caMessage, "Actual mSec: %f", fMilliSeconds);

  Label4->Caption = caMessage;

  Screen->Cursor = crArrow;
}

void SleepSelect (int iMilliSec)
{
  struct timeval  tv;
  tv.tv_sec = iMilliSec / 1000;
  tv.tv_usec = (iMilliSec % 1000) * 1000;

  select (0, 0, 0, 0, &tv);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button5Click(TObject *Sender)
{
  int iNum;
  sscanf (Edit5->Text.c_str(), "%d", &iNum);
  LARGE_INTEGER liDueTime;

  liDueTime.QuadPart=-iNum*10000;    // convert to 100 nanosec.  negative means
                                // relative wait

  Screen->Cursor = crHourGlass;

  hrt.Start();

  if (!SetWaitableTimer(hTimer, &liDueTime, 0, NULL, NULL, 0))
   {
    ShowMessage ("Unable to wait for timer");
   }

  if (WaitForSingleObject (hTimer, INFINITE) != WAIT_OBJECT_0)
   {
    ShowMessage ("Unable to wait for single object");
   }


  float fMilliSeconds = hrt.Read();

  char caMessage[64];
  sprintf (caMessage, "Actual mSec: %f", fMilliSeconds);

  Label5->Caption = caMessage;

  Screen->Cursor = crArrow;

}
//---------------------------------------------------------------------------


void __fastcall TForm1::FormShow(TObject *Sender)
{
  #define TARGET_RESOLUTION 1     // attempt to use 1 milli sec resolution

  TIMECAPS tc;
  UINT wTimerRes;

  if (timeGetDevCaps (&tc, sizeof(TIMECAPS)) != TIMERR_NOERROR)
   {
    ShowMessage ("Unable to get time caps");
   }
  else
   {
    char caMessage[64];
    sprintf (caMessage, "Resolution Range:  %d -- %d", tc.wPeriodMin,
                tc.wPeriodMax);
    ShowMessage (caMessage);

    wTimerRes = min (max(tc.wPeriodMin, TARGET_RESOLUTION),
                tc.wPeriodMax);
    if (timeBeginPeriod (wTimerRes) != TIMERR_NOERROR)
      ShowMessage ("Unable to set the timer period");
   }

  // allocate a waitable timer
  hTimer = CreateWaitableTimer (NULL, NULL, "WaitableTimer");
  if (!hTimer)
   {
    ShowMessage ("Cannot create waitable timer");
   }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormDestroy(TObject *Sender)
{
  timeEndPeriod (1);        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button6Click(TObject *Sender)
{
  float fNum;
  sscanf (Edit6->Text.c_str(), "%f", &fNum);

  Screen->Cursor = crHourGlass;

  hrt.Start();

  hrt.DelayNS (fNum);

  float fMilliSeconds = hrt.Read();

  char caMessage[64];
  sprintf (caMessage, "Actual mSec: %f", fMilliSeconds);

  Label6->Caption = caMessage;

  Screen->Cursor = crArrow;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button7Click(TObject *Sender)
{
  int iNum;
  sscanf (Edit7->Text.c_str(), "%d", &iNum);

  Screen->Cursor = crHourGlass;

  hrt.Start();

  hrt.Sleep (iNum);

  float fMilliSeconds = hrt.Read();

  char caMessage[64];
  sprintf (caMessage, "Actual mSec: %f", fMilliSeconds);

  Label7->Caption = caMessage;

  Screen->Cursor = crArrow;
}
//---------------------------------------------------------------------------

