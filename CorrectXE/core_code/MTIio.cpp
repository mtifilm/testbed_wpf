/*
    File: MTIio.cpp
    Created Aug 21, 2001 by John Mertus

    The purpose of this is just to make the
    for open/close/lseek consistent between UNIX and WINDOW
*/
#include "machine.h"
#include "MTIio.h"
#include "BackupSet.hpp"
#include "FileSweeper.h"
#include "HRTimer.h"
#include "MTIstringstream.h"
#include <assert.h>
#include <stdio.h>
#include <time.h>
#include <locale.h>
#include <iomanip>

#ifndef MTI_CORECODELIB_NO_DLL
#include "ipps.h"
#endif

// Trace Destination flags
static int g_TraceDestinationFlag = TD_PROGRAM;

#if defined(__CONSOLE_DIALOGS)
#include "MTIioConsole.cc"
#elif defined(_WINDOWS)
#include "MTIioWin.cc"
#else
#include "MTIioX.cc"
#endif

//**************************COMMON CODE**********************************

void ChangeTraceDestination(const trace_destination td)
{
  g_TraceDestinationFlag = td;
}

#define PROGRAM_IS_RUNNING_STATUS 1
#define PROGRAM_IS_EXITING_STATUS -1
static int programStatus = PROGRAM_IS_RUNNING_STATUS;

bool IsProgramRunning()
{
	return programStatus == PROGRAM_IS_RUNNING_STATUS;
}

bool IsProgramExiting()
{
	return programStatus == PROGRAM_IS_EXITING_STATUS;
}

void NotifyProgramRunning(bool flag)
{
	programStatus = flag? PROGRAM_IS_RUNNING_STATUS : 0;
}

void NotifyProgramExiting()
{
   programStatus = PROGRAM_IS_EXITING_STATUS;
}

//**************************Endian Conversion*****************************

#ifdef MTI_CORECODELIB_NO_DLL
// IPP NOT ALLOWED IF WE'RE NOT BUILDING CORE_CODE.DLL!
namespace
{
	inline void swap2(void *in, void *out)
	{
		((char *)out)[1] = ((char *)in)[0];
		((char *)out)[0] = ((char *)in)[1];
	}

	inline void swap4(void *in, void *out)
	{
		((char *)out)[0] = ((char *)in)[3];
		((char *)out)[1] = ((char *)in)[2];
		((char *)out)[2] = ((char *)in)[1];
		((char *)out)[3] = ((char *)in)[0];
	}
}

void SwapINT16(MTI_UINT16 *p, size_t n)
{
	for (int i = 0; i < n; ++i)
	{
		byteSwap(p[i]);
	}
}

void SwapINT16(MTI_INT16 *p, size_t n)
{
	for (int i = 0; i < n; ++i)
	{
		byteSwap(p[i]);
	}
}

void SwapINT32(MTI_UINT32 *p, size_t n)
{
	for (int i = 0; i < n; ++i)
	{
		byteSwap(p[i]);
	}
}

void SwapINT32(MTI_INT32 *p, size_t n)
{
	for (int i = 0; i < n; ++i)
	{
		byteSwap(p[i]);
	}
}

void SwapREAL32(MTI_REAL32 *p, size_t n)
{
	for (int i = 0; i < n; ++i)
	{
		byteSwap(p[i]);
	}
}

void SwapINT16(MTI_UINT16 *src, MTI_UINT16 *dst, size_t n)
{
	for (int i = 0; i < n; ++i)
	{
		swap2(&src[i], &dst[i]);
	}
}

void SwapINT16(MTI_INT16 *src, MTI_INT16 *dst, size_t n)
{
	for (int i = 0; i < n; ++i)
	{
		swap2(&src[i], &dst[i]);
	}
}

void SwapINT32(MTI_UINT32 *src, MTI_UINT32 *dst, size_t n)
{
	for (int i = 0; i < n; ++i)
	{
		swap4(&src[i], &dst[i]);
	}
}

void SwapINT32(MTI_INT32 *src, MTI_INT32 *dst, size_t n)
{
	for (int i = 0; i < n; ++i)
	{
		swap4(&src[i], &dst[i]);
	}
}

void SwapREAL32(MTI_REAL32 *src, MTI_REAL32 *dst, size_t n)
{
	for (int i = 0; i < n; ++i)
	{
		swap4(&src[i], &dst[i]);
	}
}

#else // IPP IS ALLOWED!

void SwapINT16(MTI_UINT16 *usp, size_t n)
{
	ippsSwapBytes_16u_I((Ipp16u*)usp, n);
}

void SwapINT16(MTI_INT16 *sp, size_t n)
{
	ippsSwapBytes_16u_I((Ipp16u*)sp, n);
}

void SwapINT32(MTI_UINT32 *ulp, size_t n)
{
	ippsSwapBytes_32u_I((Ipp32u*)ulp, n);
}

void SwapINT32(MTI_INT32 *lp, size_t n)
{
	ippsSwapBytes_32u_I((Ipp32u*)lp, n);
}

void SwapREAL32(MTI_REAL32 *fp, size_t n)
{
	ippsSwapBytes_32u_I((Ipp32u*)fp, n);
}

void SwapINT16(MTI_UINT16 *src, MTI_UINT16 *dst, size_t n)
{
	ippsSwapBytes_16u((Ipp16u*)src, (Ipp16u*)dst, n);
}

void SwapINT16(MTI_INT16 *src, MTI_INT16 *dst, size_t n)
{
	ippsSwapBytes_16u((Ipp16u*)src, (Ipp16u*)dst, n);
}

void SwapINT32(MTI_UINT32 *src, MTI_UINT32 *dst, size_t n)
{
	ippsSwapBytes_32u((Ipp32u*)src, (Ipp32u*)dst, n);
}

void SwapINT32(MTI_INT32 *src, MTI_INT32 *dst, size_t n)
{
	ippsSwapBytes_32u((Ipp32u*)src, (Ipp32u*)dst, n);
}

void SwapREAL32(MTI_REAL32 *src, MTI_REAL32 *dst, size_t n)
{
	ippsSwapBytes_32u((Ipp32u*)src, (Ipp32u*)dst, n);
}

#endif


//---------CharLessThanIgnoreCase---------------mbraca--Aug 2006---------

  bool CharLessThanIgnoreCase(char x, char y)

// This checks if the character x precedes character y, case insensitive.
//
//***********************************************************************
{
  return toupper(x) < toupper(y);
}

//---------CharEqualIgnoreCase-------------John Mertus--Aug 2001---------

  bool CharEqualIgnoreCase(char x, char y)

// This checks if the character x and y are the same, case insensitive.
//
//***********************************************************************
{
  return toupper(x) == toupper(y);
}

//-------------LessThanIgnoreCase---------------mbraca--Aug 2006---------

  bool LessThanIgnoreCase(const string &S1, const string &S2)

// This checks if the string S1 precedes string S2, case insensitive.
// It qualifies as a "Weak Strict Ordering" for STL sorting purposes.
//
//***********************************************************************
{
   const size_t S1Length = S1.length();
   const size_t S2Length = S2.length();
   const size_t checkLength = std::min<size_t>(S1Length, S2Length);

   for (unsigned i = 0; i < checkLength; ++i)
      if (!CharEqualIgnoreCase(S1[i], S2[i]))
         return CharLessThanIgnoreCase(S1[i], S2[i]);

   return (S1Length < S2Length);
}

namespace {   // Local function

//----Helper function for SmartLessThan---------mbraca--Mar 2007---------

  StringList breakOutRuns(const string &S)

//***********************************************************************
{
   StringList runs;
   unsigned int i = 0;

   while (i < S.length())
   {
      int runStart = i;

      if (isdigit(S[i]))
      {
         for (++i; (i < S.length()) && isdigit(S[i]); ++i)
            /* Do nothing else, just looking for the end of the run */;
      }
      else if (isalpha(S[i]))
      {
         for (++i; (i < S.length()) && isalpha(S[i]); ++i)
            /* Do nothing else, just looking for the end of the run */;
      }
      else {
         for (++i; (i < S.length()) && !isdigit(S[i]) && !isalpha(S[i]); ++i)
            /* Do nothing else, just looking for the end of the run */;
      }
      runs.push_back(S.substr(runStart, i - runStart));
   }
   return runs;
}
} // end anonymous namespace


//-------------SmartLessThan---------------mbraca--Mar 2007---------

  bool SmartLessThan(const string &S1, const string &S2)

// This checks if the string S1 precedes string S2, case insensitive.
// It qualifies as a "Weak Strict Ordering" for STL sorting purposes.
// It's "smart" because it breaks the string up into runs of all alpha,
// all numeric and all-non-alphnumeric and compares the substrings.
// For example: in straight string comparison a10 < a9 but with "smart"
// compare, a9 < a10 (like XP explorer does).
//
//***********************************************************************
{
   StringList R1 = breakOutRuns(S1);
   StringList R2 = breakOutRuns(S2);
   const size_t R1Size = R1.size();
   const size_t R2Size = R2.size();
   const size_t checkCount = std::min<size_t>(R1Size, R2Size);

   for (unsigned i = 0; i < checkCount; ++i)
   {
      if (!EqualIgnoreCase(R1[i], R2[i]))
      {
         if (isdigit(R1[i][0]) && isdigit(R2[i][0]))
         {
            int N1, N2;
            MTIistringstream stream1, stream2;
            stream1.str(R1[i]);
            stream2.str(R2[i]);
            stream1 >> N1;
            stream2 >> N2;
            return N1 < N2;
         }
         else
         {
            return LessThanIgnoreCase(R1[i], R2[i]);
         }
      }
   }

   return (R1Size < R2Size);
}

//-------------EqualIgnoreCase-------------John Mertus--Aug 2001---------

  bool EqualIgnoreCase(const string &S1, const string &S2)

// This checks if the strings S1 and S2 are the same, case insensitive.
//
//***********************************************************************
{
   if (S1.length() != S2.length()) return(false);
   for (unsigned i=0; i < S1.length(); i++)
        if (!CharEqualIgnoreCase(S1[i], S2[i])) return(false);
   return(true);
}

//-------------SearchAndReplace-------------------John Mertus-----Jul 2001---

   bool SearchAndReplace(string &Whole, const string ToBeReplaced, const string Replacement)

// This searches the string Whole for all occurences of ToBeReplace
// and replaces them with string Replacement.
//
// Return is true iff at least one replacement took place.
//
// Note: Infinite recursion cannot take place but in this case some
// replacements appear not to be done.
//
//
//***************************************************************************
{
   // Sweep through the string, replacing ToBeReplaced by Replacement
   size_t found, pos=0 ;
   bool Result = false;
   while ((found=Whole.find(ToBeReplaced,pos)) != string::npos)
     {
       pos = found+Replacement.length();
       Whole = Whole.substr(0,found)+Replacement+Whole.substr(found+ToBeReplaced.length());
       Result = true;
     };
   return(Result);
}

//----------------ReturnEnv-----------------------John Mertus-----Jul 2001---

 string ReturnEnv(const char *Var)

// Windows does not properly return the getenv, this fixes it
//
//***************************************************************************
{
  string Result;
#ifdef _WINDOWS_
   // Environments cannot be longer than 256
   char Buffer[256];
   if(GetEnvironmentVariable(Var, Buffer, sizeof(Buffer)) == 0) return "";
   Result = Buffer;
#else
   char *p = getenv(Var);
   if (p == NULL)
     Result = "";
   else
     Result = p;

#endif
   return Result;
}

//----------------GetMTIEnv-----------------------John Mertus-----Jul 2001---

  string GetMTIEnv(const string Var)

// This behaves similar to getenv except that special environment variables
// are translated into their special meanings.  See the code for details
//
//***************************************************************************
{
	string Str;

   // No translation, see if any are special variables
// Windows translations************************************************
	// Special cases
	if ((Var == "USERDIR") || (Var == "CPMP_USER_DIR"))
	{
		// Try a translation on variable, if it works, return it
		Str = ReturnEnv("CPMP_USER_DIR");
		if (Str != "")
			return AddDirSeparator(Str);

		Str = ReturnEnv("USERPROFILE");
		if (Str == "")
			return "C:\\.cpmprc\\";
		return Str + "\\.cpmprc\\";
	}

	if (Var == "CPMP_SHARED_DIR")
	{
		// Try a translation on variable, if it works, return it
		Str = ReturnEnv("CPMP_SHARED_DIR");
		if (Str == "")
			Str = (string)DEFAULT_SHARED_DIR;
		Str = AddDirSeparator(Str);

		// Check if the directory doesn't exist, if so create it
		if (!DoesWritableDirectoryExist(Str))
		{
			// Str must end in a backslash
			string binDirectory = Str + "MTIBins\\";
			if (!MakeDirectory(binDirectory))
			{
				{
					CAutoErrorReporter autoErr("Core::GetMTIEnv", AUTO_ERROR_NOTIFY_WAIT,
						AUTO_ERROR_TRACE_0);
					autoErr.errorCode = FILESWEEPER_ERROR_MKDIR_FAILED;
					autoErr.msg << "****FAILURE**** cannot create or write to \n" + binDirectory;
				}

				exit(3333);
			}
		}

		return Str;
	}

	// Change CPMP_LOCAL_DIR to DRS_NOVA_LOCAL_DIR
	if ((Var == "DRS_NOVA_LOCAL_DIR") || (Var == "CPMP_LOCAL_DIR"))
	{
		// Try a translation on variable, if it works, return it
		Str = ReturnEnv("DRS_NOVA_LOCAL_DIR");
		if (Str != "")
			return AddDirSeparator(Str);

		return CPMPGetDefaultLocalDir();
	}

   // No special translations, try normal
   return ReturnEnv(Var.c_str());
}

//--------------ExpandEnviornmentString-----------John Mertus-----Jul 2001---

  bool ExpandEnviornmentString(string &str)

// This takes str and replaces substrings of the form $(VAR) with the
// environment variable VAR.
//
//  Note: Infinite recursion can take place but the first level
//  is prohibited.
//
//***************************************************************************
{
//
//  This is a recursive replace, the string str is searched for
//  variables of the form $(VAR)
//
    size_t f1 = str.find("$(");
    if (f1 == string::npos) return false;    // Exit if not found
    size_t f2 = str.find(")",f1+2);
    if (f2 == string::npos) return false;    // Exit if not found, ERROR

    // We have a $() lets see if there is a () inside it
    auto leftP = 1;
    auto f3 = 2;
    for (auto c : str.substr(f1+2))
    {
       if (c == '(') leftP++;
       if (c == ')') leftP--;
       if (leftP == 0)
       {
         f2 = f3;
         break;
       }

       f3++;
    }

    // Avoid infinite recursion
    string Repl = GetMTIEnv(str.substr(f1+2,f2-f1-2));
    string ToBe = str.substr(f1,f2-f1+1);

    if (Repl.find(ToBe) != string::npos) return(false);  // Infinite recursion

    // Finally go do it
    SearchAndReplace(str, ToBe, Repl);
    // Replace double directory separators
    SearchAndReplace(str, DIR_DOUBLESEPARATOR, sDIR_SEPARATOR);
    ExpandEnviornmentString(str);         // Do it again until done right
    return true;
}

//--------------ConformFileName------------------John Mertus-----Sep 2001---

  void ConformFileName(string &Whole, bool bForMySQL)

// This conforms a file name, str, from one file system to the other.
// E.g., Windows <--> Unix
//
//  Note: This is very crude as drives are not properly handled
//
//  MySQL does not handle the '\' character, even under windows.  Set
//  bForMySQL flag true to force '/'
//
//***************************************************************************
{
#ifdef _WINDOWS_
char DefaultReplacement = '\\';
char DefaultToBeReplaced = '/';
#else
char DefaultReplacement = '/';
char DefaultToBeReplaced = '\\';
#endif

   char Replacement, ToBeReplaced;

   if (bForMySQL)
    {
     // change '\' into '/'
     Replacement = '/';
     ToBeReplaced = '\\';
    }
   else
    {
     Replacement = DefaultReplacement;
     ToBeReplaced = DefaultToBeReplaced;
    }

   size_t found, pos=0 ;
   while ((found=Whole.find(ToBeReplaced,pos)) != string::npos)
     {
       pos = found+1;
	   Whole = Whole.substr(0,found)+Replacement+Whole.substr(found+1);
     };
   return;

}

static string ApplicationName;       // Application name
static string LogFile;               // Where the log of the program is kept


//---------------GetApplicationName---------------John Mertus----Sep 2001-----

  string GetApplicationName(void)
//  This dummy function is linked in only when non-viewkit programs
// are created.  In that case CreateApplicationName must be called
//
//****************************************************************************
{
#ifdef __BORLANDC__
  if (ApplicationName == "")
  {
	 return ".\\DRS_Nova.exe";
//// CANNOT CALL TRACE HERE
////	 TRACE_0(errout << "GetApplicationName should not be called before name is set, set Application name in startup");
  }
#endif

  return ApplicationName;
}

//
// We need only one of these
CLogFile ProgramLogFile;

//*************************Code for CLogFile***********************************

   std::string NumberConvertedBackToString(int aNumber)
{
#ifdef WIN64
	return std::to_string((long long)aNumber);
#else
	char intStr[25];
	itoa(aNumber, intStr, 10);
	return string(intStr);
#endif
}

//--------------------Close----------------------John Mertus----Oct 2001-----

    void CLogFile::Close(void)

//  If the log file is opened, it is closed and now the next
//  write will try to reopen the current file name
//
//****************************************************************************
{
   if (_Stream != NULL)       // Close if Opened
     {
        fclose(_Stream);
        _Stream = NULL;
        _bFirst = true;
     }
}
//--------------------Write----------------------John Mertus----Oct 2001-----

    bool CLogFile::Write(const string &str)

//  This writes the string str to the log file.
//  Return is true if write succeeded, false otherwise
//
//  The first write will open the file on demand.
//  Lastly an new line is appended to the log file if there is not one.
//
//****************************************************************************
{
     bool Result = true;

#ifndef _MTIPACKAGE
// Stupid error in lib/vs DLL result in this code
     if (_Stream == NULL)
       {
         if (!Open()) return(false);
       }

     unsigned n = fprintf(_Stream,"%s", str.c_str());
     // Add a newline if one does not exist
     if (str.size() == 0)
       fprintf(_Stream,"\n");
     else if (str[str.size()-1] != '\n')
       fprintf(_Stream,"\n");

     fflush(_Stream);
     Result = (n == str.size());
#endif
     return(Result);
}

//----------------CLogFile------------------------John Mertus----Oct 2001-----

   CLogFile::CLogFile(CBackupSet *backupSetPtr)
      : _backupSetPtr(backupSetPtr)
//  This constructs the log file.  Usual garbage
//
//****************************************************************************
{
	_Stream = NULL;
	_sFileName = "";
	_iKeep = 5;     // Keep five copies
	_bFirst = true;
}

//----------------CLogFile------------------------John Mertus----Oct 2001-----

     CLogFile::~CLogFile(void)

//  This destroys the log file.  Remove all the excess files
//
//****************************************************************************
{
    RemoveExcessFiles();
}



//----------------SetFileName---------------------John Mertus----Oct 2001-----

     bool CLogFile::SetFileName(const string &str)

//  This sets the internal file name to str, it does not open the file
//  Return is true if write succeeded, false otherwise
//
//  The first write will open the file on demand.
//
//****************************************************************************
{
   Close();                    // Close any opened file
   _sFileName = str;
   _bFirst = true;             // Try on failure again
   return(true);
}

//--------------------Open-----------------------John Mertus----Sep 2001-----

       bool CLogFile::Open()

//  This looks at the fsFileName and if not blank opens it.
//  If a failure occured on the same name, it does not retry.
//
//  Return is true if file is opened, false otherwise
//
//****************************************************************************
{
   if (_sFileName == "") return(false);  // Nothing to open
   if (!_bFirst) return(false);          // Failed once, don't retry

   Close();
   _bFirst = false;                      // No matter what happens, try only once
                                         // Close and SetFileName resets this flag
   _sActualFileName = NextFileName();
   _Stream = fopen(_sActualFileName.c_str(), "wt");
   if (_Stream == NULL)
      {
         // Must not log here, so use TRACE
         TRACELOG_MSG(errout << "Log Opened failed for " << _sFileName, true, false);
         return(false);
      }

   RemoveExcessFiles();
   return(true);
}

//-------------------FindExcessFile-------------John Mertus----Sep 2001-----

     bool CLogFile::FindExcessFile(string Name)

//  This very specialized routine looks at the name desides the
//  if the file revision number is too small adds it to _DeleteList
//  a list if files to be deleted.
//
//****************************************************************************
{
    int n = GetFN(Name);

    if (_iKeep < 0) return(false);    // Stop doing anything
    if (n < 0) return(true);
    if (n > (_iMaxFN - _iKeep)) return(true);

    // Now add to the delete list the file
    _DeleteList.push_back(Name);
    return(true);
}

//--------------------SingleFindMaxFN-------------John Mertus----Sep 2001-----

     bool CLogFile::SingleFindMaxFN(string Name)

//  This very specialized routine looks at the name and finds the file number.
//  The maximum file number is updated.
//
//****************************************************************************
{
    _iMaxFN = std::max(_iMaxFN, GetFN(Name));
    return(true);
}

//--------------------GetFN------------------------John Mertus----Sep 2001-----

     int CLogFile::GetFN(const string &Name)

//  This very specialized routine looks at the name and finds the file number.
//
//****************************************************************************
{
     int iFileNumber;
     string sFileNumber;
     if (_backupSetPtr != NULL)
     {
        size_t lastDotPos = Name.rfind('.');
        if (lastDotPos == string::npos) return(-1);
        size_t penultimateDotPos = Name.rfind('.', lastDotPos-1);
        if (penultimateDotPos == string::npos) return(-1);
        sFileNumber = Name.substr(penultimateDotPos+1,
                                  lastDotPos - (penultimateDotPos+1));
     }
     else
     {
        size_t lastDotPos = Name.rfind('.');
        if (lastDotPos == string::npos) return(-1);
        sFileNumber = Name.substr(lastDotPos+1);
     }
     MTIistringstream istr(sFileNumber);
     istr >> iFileNumber;
     if (istr.fail())
        iFileNumber = -1;

     if (NumberConvertedBackToString(iFileNumber) != sFileNumber)
     {
        iFileNumber = -1;
     }

     return(iFileNumber);
}

//--------------------NextFileName-----------------John Mertus----Sep 2001-----

     string CLogFile::NextFileName()

//  This returns with the next file name in log command.  Right now,
//  this is just BaseFileName.n, if the CLogFile is not configured
//  with a CBackupSet where n is the number.  This is
//  BaseFileName.n.backupSetString if the CLogFile is configured with
//  a CBackupSet.  n runs from 0 to infinity, the lower number ones
//  are deleted.
//
//  Return is the file to open.
//
//****************************************************************************
{
   if (_sFileName == "") return("");  // Nothing to open xxxx
   _iMaxFN = 0;                          // Nothing found
   FindMaxFN(_sFileName + ".*");
   MTIostringstream os;
   _iMaxFN++;
   os << _sFileName << "." << _iMaxFN;
   if (_backupSetPtr != NULL)
   {
      os << "." << _backupSetPtr->GetBackupSetString();
   }
   return (os.str());
}

//--------------------LastExistingFileName---------John Mertus----Sep 2001-----

     string CLogFile::LastExistingFileName()

//  This returns with the last existing file name in log command.
//  This is just BaseFileName.n where n is the number if the CLogFile
//  is not configured with a CBackupSet.  This is
//  BaseFileName.n.backupSetString if the CLogFile is configured with
//  a CBackupSet.  n runs from 0 to infinity
//
//  Return is the file to open.
//
//****************************************************************************
{
   if (_sFileName == "") return("");  // Nothing to open xxxx
   _iMaxFN = 0;                          // Nothing found

   if (_backupSetPtr != NULL)
   {
     FindMaxFN(_sFileName + ".*" + "." + _backupSetPtr->GetBackupSetString());
   }
   else
   {
     FindMaxFN(_sFileName + ".*");
   }

   MTIostringstream os;
   os << _sFileName << "." << _iMaxFN;
   if (_backupSetPtr != NULL)
   {
      os << "." << _backupSetPtr->GetBackupSetString();
   }

   if (DoesFileExist(os.str()) == true)
     return(os.str());
   else
     return("");
}

//---------------RemoveExcessFiles-----------------John Mertus----Sep 2001-----

     void CLogFile::RemoveExcessFiles(void)

// Removes any excess files
// A list is build and then destroyed, this is because an opendir
// locks the directory under some operating systems.
//
//****************************************************************************
{
   _DeleteList.clear();
   // Collect the information
   FindExcessFiles(_sFileName + ".*");

   // Remove them
   for (unsigned i = 0; i < _DeleteList.size(); i++)
     if (remove(_DeleteList[i].c_str()) != 0)
       TRACE_0(errout << "Could not delete log file " << _DeleteList[i] << endl;
               errout << "Because: " << strerror(errno));
}

//*****************Start of support code*************************************

//------------CPMPSetLogFileName------------------John Mertus----Sep 2001-----

  bool CPMPSetLogFileName(const string &name)

//  This sets the logging file to name, any previous loging file is
//  closed.
//
//****************************************************************************
{
    return(ProgramLogFile.SetFileName(name));
}

//--------------SendLogMessage--------------------John Mertus-----Sep 2001---

   bool SendLogMessage(const string &str)

// This sends str to a log file, if such a file exists.
//
//***************************************************************************
{
   bool Result = ProgramLogFile.Write(str);
   return(Result);
}


// Need a forward declaration.
bool SendTraceMessage(const string &str);

static CHRTimer HRTimer;

//--------------SendCPMPMessage-----------------John Mertus-----Sep 2001---

   bool SendCPMPMessage(const string &str, bool trace, bool log)

// This sends the message str
//     if trace, it sends a trace message
//     if log, it logs to a file, if a file exists.
//
//***************************************************************************
{
   bool Result;
   static bool firstTimeFlag = true;
   if (firstTimeFlag)
     {
       firstTimeFlag = false;
       HRTimer.Start();

       time_t ltime;
       time(&ltime);
       string timeString(ctime(&ltime));

       // erase newline at end of time string
       size_t timeLen = timeString.length();
       if (timeLen > 0 && timeString[timeLen-1] == '\n')
           timeString.erase(timeLen-1);

       SendTraceMessage(string("\n\n============== Trace started at ")
                        + timeString + " ==============\n");
       SendLogMessage(string("============== Logging started at ")
                        + timeString + " ==============\n");
     }

   int millisec = (int) HRTimer.Read();
   MTIostringstream os;

   char buffer[1024];
   sprintf(buffer, "%0d:%02d:%02d.%03d  ",
                   millisec / 3600000,
                   (millisec % 3600000) / 60000,
					    (millisec % 60000) / 1000,
                   millisec % 1000);
   os << buffer << str;

   if (trace) Result = SendTraceMessage(os.str());
   if (log) Result = SendLogMessage(os.str());
   return(Result);
}

//--------------SendCPMPMessage-----------------John Mertus-----Sep 2001---

   bool SendCPMPMessage(const char *msg, bool trace, bool log)

// This sends the message str
//     if trace, it sends a trace message
//     if log, it logs to a file, if a file exists.
//
//***************************************************************************
{
   double timeNow = HRTimer.Read()/1000.0;     // convert to seconds
   int hrs = (int) (timeNow / 3600);
   int mins = ((int) (timeNow / 60)) % 60;
   int secs = ((int) timeNow) % 60;
   int msecs = ((int) (timeNow * 1000)) % 1000;

   char buf[10001];
   sprintf(buf, "%d:%02d:%02d.%03d %s\n", hrs, mins, secs, msecs, msg);

   if (trace) SendTraceMessage(buf);
   if (log) SendLogMessage(buf);
   return(true);
}

//-------------CreateApplicationName--------------John Mertus----Sep 2001-----

    void  CreateApplicationName(const string argv0)

//
//  This function sets the application name full name for argv.
//  Note: The only way to call this function is
//    CreateApplicationName(argv[0]);
//
//****************************************************************************
{
  ApplicationName = ReturnCompleteName(argv0);
}

//--------------ConformEnvironmentName-----------John Mertus-----Jul 2001---

  string ConformEnvironmentName(const string &str)

// This takes str and replaces substrings of the form $(VAR) with the
// environment variable VAR.  Returns the expanded string
//
//  Note: Infinite recursion can take place but the first level
//  is prohibited.
//
//***************************************************************************
{
  string Result = str;
  ExpandEnviornmentString(Result);
  ConformFileName(Result);
  return Result;
}

// like strptime(sD.c_str(), "%d-%b-%Y", &tblock);
int parse_flex_date(string const &sDate, struct tm &tblock)
{
   string MonthName[] = {"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};
   char Month[4];
   Month[3] = 0;
   int Day;
   int Year;

   // See if the date string is 0 or never expires
   if (sDate.length() < 11) return -1;

   if (sscanf(sDate.c_str(), "%d", &Day) != 1) return -1;
   memcpy(Month, sDate.c_str() + 3, 3);
   if (sscanf(sDate.c_str()+7, "%d", &Year) != 1) return -1;

   memset(&tblock, 0, sizeof(tblock));
   tblock.tm_mday = Day;

   for (tblock.tm_mon = 0; tblock.tm_mon < 12; tblock.tm_mon++)
     if (strcasecmp(MonthName[tblock.tm_mon].c_str(), Month) == 0) break;

   tblock.tm_year = Year - 1900;
   tblock.tm_isdst = -1;

   // Check to see if decoding went wrong
   if (tblock.tm_mon == 12)
     {
        TRACE_0(errout << "WARNING Could not decode license date " << sDate);
        return -1;
     }

   return 0;
}

// like strptime(sDate.c_str(), "%b %d %Y", &tblock);
int parse_cpp_date(string const &sDate, struct tm &tblock)
{
   string MonthName[] = {"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};
   char Month[4];
   Month[3] = 0;
   int Day;
   int Year;
   int Mon;

   if (sscanf(sDate.c_str()+4, "%d", &Day) != 1) return -1;
   memcpy(Month, sDate.c_str(), 3);
   if (sscanf(sDate.c_str()+7, "%d", &Year) != 1) return -1;

   for (Mon = 0; Mon < 12; Mon++)
     if (strcasecmp(MonthName[Mon].c_str(), Month) == 0) break;

   // Check to see if decoding went wrong
   if (Mon == 12)
     {
        TRACE_0(errout << "WARNING Could not decode _DATE_ date " << sDate);
        return -1;
     }

   memset(&tblock, 0, sizeof(tblock));
   tblock.tm_mday = Day;

   for (tblock.tm_mon = 0; tblock.tm_mon < 12; tblock.tm_mon++)
     if (strcasecmp(MonthName[tblock.tm_mon].c_str(), Month) == 0) break;

   tblock.tm_year = Year - 1900;
   tblock.tm_isdst = -1;

   // Check to see if decoding went wrong
   if (tblock.tm_mon == 12)
     {
        TRACE_0(errout << "WARNING Could not decode license date " << sDate);
        return -1;
     }

   return 0;
}

//-------------DateStringToMTITime----------------John Mertus----Jan 2005-----

    MTI_TIME DateStringToMTITime(const string &sDate, bool UTC)

//   This just changes a FLEXnet date into the MTI_TIME
//   if UTC is true then the date is in UTC time
//   otherwise in local time.
//
//****************************************************************************
{
   struct tm tblock;
   string sD = sDate;

   // I'm not really sure we need to set the locale but it doesn't hurt
   // TTT string OldLocale = setlocale(LC_TIME, NULL);
   // TTT setlocale(LC_TIME, "English" );
   if (parse_flex_date(sD, tblock) != 0)
     {
        return MTI_TIME_NEVER;
     }

   // Return it to the old value
   //TTT setlocale(LC_TIME, OldLocale.c_str());

   // Correct for GMT
   if (UTC)
     {
       time_t t = time(0);
       tm UTC = *gmtime(&t);
       tm Local = *localtime(&t);
       return MTI_TIME(mktime(&tblock) + difftime(mktime(&Local), mktime(&UTC)));
     }

   return MTI_TIME(mktime(&tblock));
}

//------------CurrentMTITime----------------------John Mertus----Jan 2005-----

    MTI_TIME CurrentMTITime(void)

//  This returns the current time in MTI_TIME
//  MTI_TIME is alway UTC time.
//
//****************************************************************************
{
    return MTI_TIME(time(NULL));
}

//------------MTITimeToDateString------------------John Mertus----Jul 2004-----

    string MTITimeToDateString(MTI_TIME Secs, bool UTC)

//  This translates the MTI concept of seconds as defined by MTICurrentTime
//  into a human readable string.  For now we use the UNIX convention
//
//****************************************************************************
{
    // See if we the seconds are valid
    if (Secs <= 0) return "Illegal Date";

    string s = MTITimeToDateTimeString(Secs, UTC);
    size_t pos = s.find(' ');
    return s.substr(0,pos+1) +  s.substr(pos+10, s.length());
}

//------------MTITimeToDateTimeString--------------John Mertus----Jan 2005-----

    string MTITimeToDateTimeString(MTI_TIME Secs, bool UTC)

//  This translates the MTI concept of seconds as defined by MTICurrentTime
//  into a human readable string.  For now we use the UNIX convention
//
//****************************************************************************
{
    // See if we the seconds are valid
    if (Secs <= 0) return "Illegal Date";

    // I'm not really sure we need to set the locale but it doesn't hurt
//    string OldLocale = setlocale(LC_TIME, NULL);
//    setlocale(LC_TIME, "English" );

    char str[50];
    if (UTC)
      {
         tm tUTC = *gmtime((const time_t *)&Secs);
         strftime(str, sizeof(str), "%d-%b-%Y %T (UTC)", &tUTC);
      }
    else
      {
         tm tLocal = *localtime((const time_t *)&Secs);
         strftime(str, sizeof(str), "%d-%b-%Y %T (Local)", &tLocal);
      }

//    setlocale(LC_TIME, OldLocale.c_str());

    return (string)str;

}

//-----------MTIBuildMinorVersionFromDate--------------John Mertus----Dec 2004-----

   string MTIBuildMinorVersionFromDate(const string &sDate)

//  This returns the date the build was created.
//   The return is VERSION.YYMMDD to be the from needed by FLEXlm
//   if __DATE__ return an invalid date, 0.0 is returned as an error.
//   This can happen if the build language is NOT english.
//
//*********************************************************************************
{
   struct tm tblock;

   // I'm not really sure we need to set the locale but it doesn't hurt
//TTT   string OldLocale = setlocale(LC_TIME, NULL);
//TTT   setlocale(LC_TIME, "English" );
   if (parse_cpp_date(sDate, tblock) != 0)
     {
	   TRACE_0(errout << "WARNING Could not decode _DATE_ date " << __DATE__);

	   return "000000";
	 }

   // Return it to the old value
//TTT   setlocale(LC_TIME, OldLocale.c_str());

   char cVersion[11];
   sprintf(cVersion, "%2.2d%02.2d%02.2d",tblock.tm_year+1900 - 2000, tblock.tm_mon+1, tblock.tm_mday);
   string sVersion = cVersion;

//  REMOVE THE NEXT LINE FOR VERSION CONTROL -JAM
#ifndef ENABLE_VERSIONING
 //  sVersion = "000000";
#endif

   return sVersion;
}

// There is one universal build date for the code
static string _BuildDate;

//-----------------MTIBuildDate-----------------------John Mertus----Dec 2004-----

   string MTIBuildDate(void)

//  This returns the date the build was created.
//   The return is VERSION.YYMMDD to be the from needed by FLEXlm
//   if __DATE__ return an invalid date, 0.0 is returned as an error.
//   This can happen if the build language is NOT english.
//
//*********************************************************************************
{
   if (_BuildDate != "") return _BuildDate;

   _BuildDate = "1." + MTIBuildMinorVersionFromDate(__DATE__);
   return _BuildDate;
}

string getArchiveBaseDir(void)
{
  string strArchiveBaseDir = (string)"$(CPMP_SHARED_DIR)CDArchive/";

  strArchiveBaseDir = ConformEnvironmentName(strArchiveBaseDir);

  return strArchiveBaseDir;
}

void getArchiveDirAndIniName(string const &binPath,
                             string const &clipName,
                             string const &guid,
                             string &strArchiveBaseDir,
                             string &strArchiveDir,
                             string &strArchiveIni)
{
  strArchiveBaseDir = getArchiveBaseDir();

  string strRelativePath = CPMPGetPathRelativeToBinRoot(binPath);
  SearchAndReplaceAllChar(strRelativePath, '\\', '_');
  strArchiveDir = strArchiveBaseDir + strRelativePath + "_" + clipName
                + "_" + guid;

  strArchiveIni = strArchiveDir + ".ini";

  strArchiveDir     = ConformEnvironmentName(strArchiveDir);
  strArchiveIni     = ConformEnvironmentName(strArchiveIni);

  return;
}

/////////////////////////////////////////////////////////////////////////////
//
// This routine replaces all occurrences of 'old' character
//   with 'cNew' character in the passed string.  It returns
//   true if any changes were made or false otherwise.
//
//   Created: 7/18/2006 - Michael Russell
//
//   Typical Usage:
//      b = stringReplaceAll(s, 'a', 'b');
//   Returns:
//      bool - true if any replacements made.
//   Changes:
//      s - string with replacements
//
//  Note: This may already be in some string library, but I couldn't
//    find it.
//
/////////////////////////////////////////////////////////////////////////////
bool SearchAndReplaceAllChar(string &s, const char old, const char cNew)
{
  string::iterator sit;
  bool             result = false;

  for (sit = s.begin(); sit < s.end(); sit++) {
    if (*sit == old)
    {
      *sit = cNew;
      result = true;
    }
  }

  return result;
}

//-----------------StringSerialize--------------------John Mertus----Mar 2007-----

  bool StringSerialize(const string &str, ostream &osStream)

//  This streams out a string
//   str is the string to stream
//   osStream is an existing stream
//   returns true if write was successful, false otherwise
//
//*********************************************************************************
{
  // Write the string
  short l = short(str.length());
  osStream.write((char *)&l, sizeof(l));
  if (osStream.rdstate() != ios::goodbit) return false;

  // Write the string + the null character if this is a non-null string
  osStream.write(str.c_str(), str.length()+1);
  return (osStream.rdstate() == ios::goodbit);
}

//-----------------StringUnSerialize-------------------John Mertus----Mar 2007-----

 bool StringUnSerialize(string &str, istream &inStream)

//  This reads a stream from a string,
//   This is not a true serializer as the return should be a string and
//   a throw on error, but we don't do this type of programming here.
//   str is the string to read
//   inStream is an existing input stream
//   returns true if sream was successful, false otherwise
//   In a failure, str is set to ""
//
//********************************************************************************
{
    // Read the length of the string
   short length = 0;
   inStream.read((char *)&length, sizeof(length));
   if (inStream.rdstate() != ios::goodbit) return false;

   // See if we are the null string
   if (length == 0)
   {
      str = "";
      return true;
   }

   // Read the string data itself
   char *buffer = new char [length+1];
   inStream.read((char *)&buffer, length+1);
   if (inStream.rdstate() != ios::goodbit)
   {
     str = "";
     delete [] buffer;
     return false;
   }

   // Now assign the string the buffer
   str = buffer;
   delete[] buffer;

   // we have succeded
   return true;
}

//-----------------WCharToStdString-------------------John Mertus----Mar 2013-----

 string WCharToStdString(const wchar_t* str)

//  This converts a wchar_t (an UnicodeString.c_str()) to a std::string
//
//********************************************************************************
{
  char s0[1024];
  WideCharToMultiByte(CP_UTF8, 0, str, -1, s0, 255, NULL, NULL);
  MTIostringstream ss;
  ss << s0;
  return ss.str();
}

////-----------------StringToStdString-------------------John Mertus----Mar 2013-----
//
// string StringToStdString(const String &str)
//
////  This converts a AnsiString to a std::string
////
////********************************************************************************
//{
//   return WCharToStdString(str.c_str());
//}
void SetApplicationPointer(void *ap)
{
  ApplicationName = string((char *)ap);
}
