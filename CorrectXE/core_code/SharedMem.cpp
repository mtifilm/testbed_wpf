#include "machine.h"
#include "TheError.h"
#include "SharedMem.h"
#include <process.h>

// This does the VTR shared memory

//-----------DoesVTRSharedMemoryExist-------------John Mertus----Oct 2003-----

   bool DoesVTRSharedMemoryExist(const string &Name)

//  Returns true if the name Name exists as shared memory
//
//****************************************************************************
{
    // Blank names cannot be mapped
    if (Name == "") return false;

    HANDLE htmp = OpenFileMapping(FILE_MAP_ALL_ACCESS, false, Name.c_str());
    if (htmp == NULL) return false;
    CloseHandle(htmp);
    return true;
}

//-------------VTRSharedMemory_Init---------------John Mertus----Oct 2003-----

  void VTRSharedMemory_Init(VTRSharedMemory *VSM, const string &ID)

//  Just initialize the shared memory
//  ID is the internal ID
//
//****************************************************************************
{
   VSM->PID = GetCurrentProcessId();
   memset(VSM->Name, 0, sizeof(VSM->Name));
   memset(VSM->ID, 0, sizeof(VSM->ID));
   strncpy(VSM->ID, ID.c_str(), sizeof(VSM->ID)-1);
   VSM->Action = vsaNone;
   VSM->AResult = vsrNone;
   VSM->AcceptCue = true;
   VSM->HWnd = NULL;
   VSM->Type = vstUnknown;
}

//-----------VTRSharedMemory_Cue------------------John Mertus----Oct 2003-----

  bool VTRSharedMemory_Cue(VTRSharedMemory *VSM, const PTimecode &ptc)

//  Sends a request to queue up timecode to the Server vtr
//
//****************************************************************************
{
   if (!VSM->AcceptCue) return false;
   VSM->tcCue = ptc;
   bcdTC BCD;
   ptc.TimecodeToBCD(&BCD);
   VTRSharedMemory_SetAction(VSM, vsaCue, BCD);
   return true;
}

//------------------------------------VTRSharedMemory_SetAction------------------------------------John Mertus----Oct 2003-----

  void VTRSharedMemory_SetAction(VTRSharedMemory *VSM, VTR_SHARED_ACTION Action, int Data)

//  This just sets the action for a vtr
//   Action is the action to set
//
//****************************************************************************
{
   VSM->Action = Action;
   VSM->AResult = vsrNone;
   if (VSM->HWnd != NULL)
      PostMessage((HWND)(VSM->HWnd), MTI_MESSAGE_VTR_COMMAND, Action, Data);
}

//--------------CVTRSharedMemoryList--------------John Mertus----Oct 2003-----

     CVTRSharedMemoryList::CVTRSharedMemoryList(void)

//  Usual constructor
//    Map the Name Index List, if it does not exist, clean it out
//
//****************************************************************************
{
   // Create new shared memory section
   mVSML = new TSharedMem("MTIVTRSharedMemList", sizeof(VTRSharedMemoryList));
   if (mVSML == NULL) return;

   // See if this is the first copy, if so, clean it out
   if (mVSML->Created())
     {
         NameIndexList()->Max = MAX_VTR_SECTIONS;
         memset(NameIndexList()->Name, 0, sizeof(NameIndexList()->Name));
     }

   // No section is yet mapped
   _VTR = (TSharedMem **)malloc(sizeof(TSharedMem *)*NameIndexList()->Max);
   for (int i=0; i < NameIndexList()->Max; i++)
      _VTR[i] = NULL;

   _ServerIndex = -1;
   _DefaultIndex = -1;
}

//-------------~CVTRSharedMemoryList--------------John Mertus----Oct 2003-----

       CVTRSharedMemoryList::~CVTRSharedMemoryList(void)

//  Usual Destructor
//
//****************************************************************************
{
   // Delete out of shared list
   if (ServerVTR() != NULL)
     {
        int i = FindName(IDName);
        if (i >= 0) memset(NameIndexList()->Name[i], 0, MAX_VTR_SECTION_NAME);
     }

   //  Delete all mapped sections
   if (NameIndexList() != NULL)
     {
        for (int i=0; i < NameIndexList()->Max; i++)
           if (_VTR[i] != NULL)
              {
                 delete _VTR[i];
                 _VTR[i] = NULL;
              }
        free(_VTR);
        delete mVSML;
        mVSML = NULL;
     }
}

//-----------------NameIndexList------------------John Mertus----Oct 2003-----

     VTRSharedMemoryList *CVTRSharedMemoryList::NameIndexList(void)

//  This just returns a pointer to the List of Index Names
//  Null if there is no such pointer
//
//****************************************************************************
{
   if (mVSML == NULL) return NULL;
   return (VTRSharedMemoryList *)mVSML->Buffer();
}

//-------------------ServerVTR--------------------John Mertus----Oct 2003-----

    VTRSharedMemory *CVTRSharedMemoryList::ServerVTR(void)

//  Just return the Server vtr
//
//****************************************************************************
{
   if (_ServerIndex < 0) return NULL;
   return (VTRSharedMemory *)_VTR[_ServerIndex]->Buffer();
}

//-----------RemoveVTRSharedMemory----------------John Mertus----Oct 2003-----

     bool CVTRSharedMemoryList::RemoveVTRSharedMemory(int idx)

//  Remove VTR from from memory and the pointer in the index list
//  if there are no more references to to
//
//  return is true if it removed.
//  false if the pointer still exists, that is something is mapped
//
//****************************************************************************
{
    // See if idx exists
    if (NameIndexList() == NULL) return false;
    if ((idx < 0) || (idx >= NameIndexList()->Max)) return true;
    delete _VTR[idx];
    _VTR[idx] = NULL;

    if (!DoesVTRSharedMemoryExist(NameIndexList()->Name[idx]))
      {
        memset(NameIndexList()->Name[idx], 0, MAX_VTR_SECTION_NAME);
        return true;
      }

    return false;
}

//------------------DeleteServer------------------John Mertus----Oct 2003-----

     bool  CVTRSharedMemoryList::DeleteServer(void)

//  Just remove the Server VTR from memory and the pointer in the index list
//  return is true if it removed.
//  false if the pointer still exists, that is something is mapped
//
//****************************************************************************
{
   bool Result = RemoveVTRSharedMemory(_ServerIndex);
   _ServerIndex = -1;
   return Result;
}

//---------------------PIDName---------------------John Mertus----Oct 2003-----

     string  CVTRSharedMemoryList::PIDName(void)

//  Internal routine to create a PID name
//    return is a string of the form PIDXXXXXXXX where XXXXXXX is the hex
// for the PID
//
//****************************************************************************
{
   char str[20];
   sprintf(str, "PID%08lX", GetCurrentProcessId());
   string Result = str;
   return Result;
}

//------------------CreateServer------------------John Mertus----Oct 2003-----

       bool  CVTRSharedMemoryList::CreateServer(void)

//   This creates a Server VTR section.
//  but can be the same over other applications.
//   return is False if another Server exist, the old must be deleted first
//  via DeleteServer
//
//****************************************************************************
{
   char str[20];
   sprintf(str,"%08X",(int)this);
   return CreateServer(str);
}

//------------------CreateServer------------------John Mertus----Oct 2003-----

       bool  CVTRSharedMemoryList::CreateServer(const string &ID)

//   This creates a Server VTR section.
//  but can be the same over other applications.
//   return is False if another Server exist, the old must be deleted first
//  via DeleteServer
//
//****************************************************************************
{
   if (_ServerIndex >= 0) return false;

   string tmp = PIDName() + ID;
   if (tmp.size() >= MAX_VTR_SECTION_NAME)
     {
         TRACE_0(errout << "String ID too big in VTRSharedMemoryList_Create");
         return false;
     }

   // Now define a VTR section that has that name
   int idx = FindName(tmp);
   if (idx < 0)
     // Find the name, if it does not exist add it to first
     for (int i = 0; i < NameIndexList()->Max; i++)
       {
         if (NameIndexList()->Name[i][0] == 0)
           {
             // Now create a new section
             TSharedMem *_VSM = new TSharedMem(tmp.c_str(), sizeof(VTRSharedMemory));
             if (_VSM == NULL) return false;


             // Everything worked, define it
             _ServerIndex = i;
             _VTR[i]= _VSM;
             // And initialize it
             if (_VSM->Created()) VTRSharedMemory_Init(ServerVTR(), tmp);
             strncpy(NameIndexList()->Name[i], tmp.c_str(), MAX_VTR_SECTION_NAME);
             break;
           }
       }
     else
       _ServerIndex = idx;

   IDName = tmp;
   return true;
}

//------------------NumberOfVTRs------------------John Mertus----Oct 2003-----

       int  CVTRSharedMemoryList::NumberOfVTRs(void)

//  Returns the nubmer of VTRS.  If a VTR name exists but is not mapped
// then it is deleted from the index list
//
//****************************************************************************
{
   // Check to see if the shared memory exists
   // This should ALWAYS be the case
   if (NameIndexList() == NULL) return 0;

   int Result = 0;
   for (int i = 0; i < NameIndexList()->Max; i++)
     if (NameIndexList()->Name[i][0] != 0)
        {
           // See if shared memory exists, if not, VTR has died so
           // delete the section if it exist
           if (!DoesVTRSharedMemoryExist( NameIndexList()->Name[i]))
              RemoveVTRSharedMemory(i);
           else
               Result++;
        }


   return Result;
}


//--------------------FindName--------------------John Mertus----Oct 2003-----

       int  CVTRSharedMemoryList::FindName(const string &Name)

//  This matches the start of NAME with the IndexList and then
//  returns the first VTR that matches.
//
//****************************************************************************
{
   for (int i = 0; i < NameIndexList()->Max; i++)
     {
        string tmp = NameIndexList()->Name[i];
        if (Name == tmp.substr(0,Name.size())) return i;
     }

   return -1;
}

//-------------------ReleaseVTR-------------------John Mertus----Oct 2003-----

       bool CVTRSharedMemoryList::ReleaseVTR(int idx)

//  This removes VTR memory section.
//   Note:  if idx is the Server vtr, nothing is done
//   return is true if the VTR was released
//   false if not.
//  Use RemoveVTSharedMemory if the Server is to be deleted.
//
//****************************************************************************
{
   if (NameIndexList() == NULL) return false;

   // See if this is the default VTR
   if (idx == -1) idx = _DefaultIndex;

   // Map a know section
   if ((idx >= 0) && (idx < NameIndexList()->Max))
     {
       //  Never release the Server VTR
       if (_ServerIndex == idx) return false;
       if (_VTR[idx] != NULL)
         {
             delete _VTR[idx];
            _VTR[idx] = NULL;
         }
       return true;
     }

   return true;
}

//---------------DefaultIndex--------------------John Mertus----Oct 2003-----

  int CVTRSharedMemoryList::DefaultIndex(int idx)

//  Just set the default index
//
//***************************************************************************
{
  _DefaultIndex = idx;
  return _DefaultIndex;
}


//---------------DefaultIndex--------------------John Mertus----Oct 2003-----

  int CVTRSharedMemoryList::DefaultIndex(void)

  //  Just set the default index
//
//***************************************************************************

{
  return _DefaultIndex;
}

//-------------------SelectVTR--------------------John Mertus----Oct 2003-----

       VTRSharedMemory *CVTRSharedMemoryList::SelectVTR(int idx)

//  This returns a VTR
//    if idx is -1 (VTR_DEFAULT) then
//        If there is a VTR that matches this name, use it
//        If there is a VTR that matches the PID use it
//        First VTR in list
//    If none, return NULL;
//
//***************************************************************************
{
    if (NameIndexList() == NULL) return NULL;

    // Map a know section
    if ((idx >= 0) && (idx < NameIndexList()->Max))
      {
          // See if there is a name attached
          if (NameIndexList()->Name[idx][0] == 0) return NULL;

          // Yes, map the section and return it
          if (_VTR[idx] == NULL)
            {
               _VTR[idx] = new TSharedMem(NameIndexList()->Name[idx], sizeof (VTRSharedMemory));
               // Selecting a VTR requires the VTR to exist, if it does not mark it
               // as non existant, this happens when a VTR dies
               if (_VTR[idx]->Created())
                 {
                    RemoveVTRSharedMemory(idx);
                 }
            }
          if (_VTR[idx] != NULL) return (VTRSharedMemory *)_VTR[idx]->Buffer();
          return NULL;
      }

    // Return the default if one is specified
    if (_DefaultIndex >= 0)
      if (DoesVTRSharedMemoryExist(NameIndexList()->Name[_DefaultIndex]))
        {
           if (_VTR[_DefaultIndex] == NULL)
               _VTR[_DefaultIndex] = new TSharedMem(NameIndexList()->Name[_DefaultIndex], sizeof (VTRSharedMemory));

           if (_VTR[_DefaultIndex] != NULL)
              return (VTRSharedMemory *)_VTR[_DefaultIndex]->Buffer();
        }

    // Looking for a new one
    _DefaultIndex = -1;

    // User wants the default VTR, give it to them
    // If the lists owns a section, return it
    if (ServerVTR() != NULL)
      {
         _DefaultIndex = _ServerIndex;
         return ServerVTR();
      }

    // Try to match the ID
    string tmp = PIDName();
    int i = FindName(tmp);
    if (i > 0) return SelectVTR(i);

    // No PID matches, see if there is ANY and return first
    for (int i=0; i < NameIndexList()->Max; i++)
      if (NameIndexList()->Name[i][0] != 0)
	  {
	   if (_VTR[i] != NULL)
		 {
		   _DefaultIndex = i;
			return (VTRSharedMemory *)_VTR[_DefaultIndex]->Buffer();
		 }
	   else
		 {
		   _VTR[i] = new TSharedMem(NameIndexList()->Name[i], sizeof (VTRSharedMemory));
		   // Selecting a VTR requires the VTR to exist, if it does not mark it
		   // as non existant, this happens when a VTR dies
		   if (_VTR[i]->Created())
			 {
				RemoveVTRSharedMemory(i);
			 }
		   else
			 {
			   _DefaultIndex = i;
			   return (VTRSharedMemory *)_VTR[_DefaultIndex]->Buffer();
			 }
		 }
	   }
	// Nothing to map
    return NULL;
}

//---------------SetServerTimecode----------------John Mertus----Oct 2003-----

       bool CVTRSharedMemoryList::SetServerTimecode(const CTimecode &tc)

//    Just copy a timecode down the shared memory section
//   should be very fast
//    tc is the timecode to copy
//    Return is false if no Server vtr
//              true otherwise
//
//   Note:  No attempt to notify other side.
//
//****************************************************************************
{
   if (ServerVTR() == NULL) return false;
   ServerVTR()->tcVTR = tc;
   return true;
}

//--------------SetServerParameters---------------John Mertus----Oct 2003-----

   void CVTRSharedMemoryList::SetServerParameters(const string &Name, HANDLE h, VTR_SHARED_TYPE type)

//  This sets the user defined parameters of a VTR memory section
//   Name is the device name, e.g Sony 2000
//   Handle h a window handle to send messages back to
//   Type is the type of the VTR Usually vstEXTERNAL for an true vtr
//   and vstEMULATOR for the emulator
//
//****************************************************************************
{
   if (ServerVTR() == NULL) return;
   memset(ServerVTR()->Name, 0, sizeof(ServerVTR()->Name));
   strncpy(ServerVTR()->Name, Name.c_str(), sizeof(ServerVTR()->Name)-1);

   ServerVTR()->HWnd = h;
   ServerVTR()->Type = type;
}

//---------------------ndexByID------------------John Mertus----Oct 2003-----

   int  CVTRSharedMemoryList::IndexByID(const string &ID)

//  Return the VTR index given the ID
//  If no ID, return -1
//
//****************************************************************************
{
    if (NameIndexList() == NULL) return -1;

    for (int i=0; i < NameIndexList()->Max; i++)
       if (_VTR[i] != NULL)
         if (_VTR[i]->Name() == ID) return i;

    return -1;
}

//---------------IndexByAttachToID---------------John Mertus----Oct 2003-----

   int  CVTRSharedMemoryList::IndexByAttachToID(const string &ID)

//  An attach ID must have the same PID and trailing ID, that is in the
// same process.
//  If id does not match or is blank return -1
//
//****************************************************************************
{
    if (NameIndexList() == NULL) return -1;
    if (ID == "") return -1;
    return IndexByID(PIDName() + ID);
}

// Initialize the VTR messages
unsigned int MTI_MESSAGE_VTR_COMMAND = 0;

// This simple class just creates the messages for one and all
class CCreateMTIWindowMessages
{
  public:
  CCreateMTIWindowMessages(void)
    {
       MTI_MESSAGE_VTR_COMMAND = RegisterWindowMessage("MTI_MESSAGE_VTR_COMMAND");
    }
};

// Now create them
static CCreateMTIWindowMessages CreateMTIWindowMessages;

static bool _bLicenseSD = true;
static bool _bLicenseHD = true;
static bool _bLicense4K = true;
static bool _bLicenseDI = false;

void SetCoreResolution(bool bLicenseSD, bool bLicenseHD, bool bLicense4K, bool bLicenseDI)
{
  _bLicenseSD = bLicenseSD;
  _bLicenseHD = bLicenseHD;
  _bLicense4K = bLicense4K;
  _bLicenseDI = bLicenseDI;
}

void GetCoreResolution(bool &bLicenseSD, bool &bLicenseHD, bool &bLicense4K, bool &bLicenseDI)
{
  bLicenseSD = _bLicenseSD;
  bLicenseHD = _bLicenseHD;
  bLicense4K = _bLicense4K;
  bLicenseDI = _bLicenseDI;
}

