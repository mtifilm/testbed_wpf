// SMPTETimecode.h:  interface for the CSMPTETimecode class, which
//                   holds a representation of SMPTE 12M standard timecodes
//
// Created by: John Starr, March 21, 2003
//
/* CVS Info:
$Header: /usr/local/filmroot/core_code/include/SMPTETimecode.h,v 1.5 2006/03/27 02:53:33 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef SMPTE_TIMECODE_H
#define SMPTE_TIMECODE_H

#include "corecodelibint.h"
#include "timecode.h"

//////////////////////////////////////////////////////////////////////

// The DVS Video board extracts the SMPTE timecode in an 80 (LTC) or 90 (VITC)
// bit codeword into two 32-bit words.  The format of the LTC and VITC
// codewords can be found in SMPTE Standard 12M.

/*
   The SMPTE 12M standard specifies the timecode format for 30, 25 and 24
   frame-per-second (fps) systems.

   The timecode consists of the hour, minute, second and frame number plus
   six flag bits.  Each decimal digit of the timecode is stored as a
   separate BCD (binary coded decimal) value within the timecode word.

   Six flag bits are packed into the high-order bits of the tens position of
   the frames, seconds, minutes and hours.  These flag bits have to be masked
   off before the BCD values can be extractee

   In typical fashion, the meaning of a particular flag bit is dependent on
   the frame rate and source of the time code (LTC or VITC).

   Bit       Description
   -----------------------------------------------------------
   0 - 3     Units of Frames
   4 - 5     Tens of Frames
   6         Drop Frame (30 fps, should be 0 for 24 and 25 fps)
   7         Color Frame (25 or 30 fps, should be 0 for 24 fps)

   8 - 11    Units of Seconds
   12 - 14   Tens of Seconds
   15        Polarity Correction (LTC and 24 or 30 fps)
             Binary Group Flag 0 (LTC and 25 fps)
             Field Flag (VITC and 30 fps)

   16 - 19   Units of Minutes
   20 - 22   Tens of Minutes
   23        Binary Group Flag 0 (24 or 30 fps)
             Binary Group Flag 2 (25 fps)
             
   24 - 27   Units of Hours
   28 - 29   Tens of Hours
   30        Binary Group Flag 1
   31        Binary Group Flag 2 (LTC and 24 or 30 fps)
             Polarity Correction (LTC and 25 fps)
             Field Flag (VITC and 25 fps)

   The timecode can be initialized to a "dummy" value of OxFFFFFFFF

*/

// Flag bit masks
#define TC_DROP_FRAME_MASK    0x00000040      // Bit 6
#define TC_COLOR_FRAME_MASK   0x00000080      // Bit 7
#define TC_BIT_15_MASK        0x00008000
#define TC_BIT_23_MASK        0x00800000
#define TC_BGF1_MASK          0x40000000      // Bit 30
#define TC_BIT_31_MASK        0x80000000

// Timecode BCD digit masks
#define TC_FRAMES_UNITS_MASK  0x0000000F  // Bits 0-3
#define TC_FRAMES_TENS_MASK   0x00000030  // Bits 4-5, mask flags in bits 6 & 7
#define TC_FRAMES_MASK        (TC_FRAMES_UNITS_MASK|TC_FRAMES_TENS_MASK)
#define TC_SECONDS_UNITS_MASK 0x00000F00  // Bits 8-11
#define TC_SECONDS_TENS_MASK  0x00007000  // Bits 12-14, mask flag in bit 15
#define TC_SECONDS_MASK       (TC_SECONDS_UNITS_MASK|TC_SECONDS_TENS_MASK)
#define TC_MINUTES_UNITS_MASK 0x000F0000  // Bits 16-19
#define TC_MINUTES_TENS_MASK  0x00700000  // Bits 20-22, mask flag in bit 23
#define TC_MINUTES_MASK       (TC_MINUTES_UNITS_MASK|TC_MINUTES_TENS_MASK)
#define TC_HOURS_UNITS_MASK   0x0F000000  // Bits 24-27
#define TC_HOURS_TENS_MASK    0x30000000  // Bits 28-29, mask flags in bits 30
                                          // & 31
#define TC_HOURS_MASK         (TC_HOURS_UNITS_MASK|TC_HOURS_TENS_MASK)
#define TC_TIME_MASK          (TC_HOURS_MASK|TC_MINUTES_MASK|TC_SECONDS_MASK|TC_FRAMES_MASK)

#define TC_DUMMY_VALUE        0xFFFFFFFF

// ---------------------------------------------------------------------------

class MTI_CORECODELIB_API CSMPTETimecode
{
public:
   CSMPTETimecode();
   virtual ~CSMPTETimecode();

//   int getBinaryGroupFlags() const;
   MTI_UINT32 getRawTimecode() const;
   MTI_UINT32 getRawUserData() const;
   CTimecode getTimecode(int frameRate) const;

   bool isDummyValue() const;
   bool isDropFrame() const;

   void setToDummyValue();
   void setRawTimecode(MTI_UINT32 newTimecode);
   void setRawUserData(MTI_UINT32 newUserData);
   void setTimecode(const CTimecode &newTimecode);

   // Increment & Decrement operators
   void increment(int frameRate);       // ++TC
   void decrement(int frameRate);       // --TC

   void addFrameOffset(int frameOffset, int frameRate);   // TC += frameOffset

   // Comparison operator
   bool isEqualTo(const CSMPTETimecode &rhs, bool compareUserData) const;

protected:
   bool isKnownFrameRate(int frameRate) const;

private:
   MTI_UINT32 m_timecode;
   MTI_UINT32 m_userData;
};

//////////////////////////////////////////////////////////////////////

#endif // #ifndef SMPTE_TIMECODE_H



