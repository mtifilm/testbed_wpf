/*
   File Name:  CPlugin.cpp
   Created: June 3, 2001 by John Mertus
   Version 1.00

   This is the plugin class template and functions
      CPlugin     -  Base Plugin Class
      CPluginList -  Helper class that list plugins.

*/

#include "CPlugin.h"
#include "IniFile.h"
#include "DllSupport.h"
#include <errno.h>
/*
    The following definitions are used to distinquish between unix or windows
    We always have an option on how to do this, but since the methods are really
    only minor name changes, do it this way
*/
#ifdef _WINDOWS
#include <windows.h>                 // DLL interface headers

#define LOAD_SHARED_LIBRARY(N) LoadLibrary(N)
FARPROC LOAD_FUNCTION(void * h, const char *p)
{
#ifdef _WIN64
  return GetProcAddress((HMODULE)h, p);
#else
  string Result = (string)"_" + p;
  return(GetProcAddress((HMODULE)h, Result.c_str()));
#endif
}

#define FREE_SHARED_LIBRARY(H) FreeLibrary((HMODULE)H)

#else

//
// Unix definitons, these work for any system supporting dynamic linked so
// including sun, sig and Linux.
#include <dlfcn.h>                   // DSO header
#define LOAD_SHARED_LIBRARY(N) dlopen(N, RTLD_NOW)
#define LOAD_FUNCTION(H, N) dlsym(H,N)
#define FREE_SHARED_LIBRARY(H) dlclose(H)
#endif

//----------------Constructor--------------John Mertus--March 2000------

	 CPlugin::CPlugin(string FileName)

//  This opens the plugin file with Name FileName
//  does some initilaization and then binds the procedures
//
//***********************************************************************
{
  auto c = FileName.c_str();
  qHandle = LoadLibrary(FileName.c_str());
  if (qHandle == nullptr)
	{
		ErrorNo = GetLastError();           // Doesn't matter the number
		ErrorMessage = (string)"Unable to load plugin " + FileName +
				"\n  Because Shared Library Load Failed\n" + GetSystemMessage(ErrorNo);
		TRACE_0(errout << "Error " << ErrorNo << ", " << ErrorMessage);
        return;
    }

  //
  // Store the file name for later use
  qPluginFileName = FileName;

  //
  // Check that this file is a valid MTI plugin
  //
  MTI_PluginVersion = (bool(*)(int *))LOAD_FUNCTION(qHandle,"MTI_PluginVersion");
  if (MTI_PluginVersion == NULL)
    {
        ErrorNo = 2;           // Doesn't matter the number
        ErrorMessage = (string)"Not a valid plugin " + FileName;
    	TRACE_0(errout << "CPlugin: " << ErrorMessage);
        return;
	}

  int version[5];
  if (!MTI_PluginVersion(version))
  {
	 ErrorNo = 3;
	 return;
  }

  if (!BindAddresses())
    {
      // Errors get set above
      TRACE_0(errout << "CPlugin: Failed to bind addresses "
	             << endl << ErrorMessage);
      return;
	}

  //
  ErrorNo = 0;             // Return success
  ErrorMessage = "Success";
}

//----------------Destructor----------------John Mertus--March 2000------

     CPlugin::~CPlugin(void)

//  This release all the resources of the shared object.
//
//***********************************************************************
{
	if (qHandle == NULL) return;
	FREE_SHARED_LIBRARY(qHandle);
    qHandle = NULL;
}

//------------------PluginName-------------John Mertus--March 2000------

       string CPlugin::PluginName(void)

//  Convience function to get the plugin resource
//
//***********************************************************************
{
   string Result = (char *)Resource(PIN_RES_PLUGIN_NAME);
   return Result;
}

//---------------BindAddresses-------------John Mertus--March 2000------

       bool CPlugin::BindAddresses(void)

//  This loads the common routines from the plugin
//  An exception is thrown if any method does not exist in the plugin
//
//***********************************************************************
{
  Create = (void *(*)(string))LOAD_FUNCTION(qHandle,"Create");
  if (Create == NULL)
    {
        ErrorNo = 3;           // Doesn't matter the number
        ErrorMessage = (string)"Create fcn not found in " + qPluginFileName;
        return(false);
    }

  Resource = (void * (*)(int))LOAD_FUNCTION(qHandle,"Resource");
  if (Resource == NULL)
    {
        ErrorNo = 3;           // Doesn't matter the number
        ErrorMessage = (string)"Resource fcn not found in " + qPluginFileName;
        return(false);
    }

  // These do NOT have to exist in the plugin
  SendTheMessage = (int (*)(int, void *))LOAD_FUNCTION(qHandle,"SendTheMessage");
  if (SendTheMessage == NULL)
    {
        SendTheMessage = &NullMessage;
    }

  Properties = (bool (*)(void))LOAD_FUNCTION(qHandle,"Properties");
  if (Properties == NULL)
  {
	  Properties = &NullProperties;

  }

  CurrentState = (PluginStateStruct (*)(void))LOAD_FUNCTION(qHandle,"CurrentState");
  if (CurrentState == NULL)
  {
	  CurrentState = &NullCurrentState;
  }

  // Read/write properties
  ReadProperties = (bool (*)(string, string))LOAD_FUNCTION(qHandle, "ReadProperties");
  if (ReadProperties == NULL)
    {
        ReadProperties = &NullRWProperties;
    }

  WriteProperties = (bool (*)(string, string))LOAD_FUNCTION(qHandle, "WriteProperties");
  if (WriteProperties == NULL)
    {
        WriteProperties = &NullRWProperties;
    }

  return(true);
}

  PluginStateStruct CPlugin::NullCurrentState(void)
 {
	PluginStateStruct cps;
    return cps;
 }

//***************************CPluginStateWrapper*****************************

//-----------------CPluginStateWrapper-----------John Mertus----Apr 2004-----

	 CPluginStateWrapper::CPluginStateWrapper(void)

//  Usual constructor
//
//****************************************************************************
{
  _pluginStateStruct.m_State = PLUGIN_STATE_UNKNOWN;
  _pluginStateStruct.m_Expires = MTI_TIME_EXPIRED;
}

//-----------------CPluginStateWrapper--------------------------Jan 2020-----

	 CPluginStateWrapper::CPluginStateWrapper(PluginStateStruct pluginStateStruct)

//  Usual constructor
//
//****************************************************************************
{
  _pluginStateStruct = pluginStateStruct;
}

//-----------------CPluginStateWrapper------------John Mertus----Apr 2004-----

	 CPluginStateWrapper::~CPluginStateWrapper(void)

//   Destructor, doesn't do anything
//
//****************************************************************************
{
}

//---------------------State----------------------John Mertus----Apr 2004-----

	EPluginState CPluginStateWrapper::State(void) const

//  Return the current plugin state
//
//****************************************************************************
{
  return (EPluginState)_pluginStateStruct.m_State;
}
//---------------------State----------------------John Mertus----Apr 2004-----

	void CPluginStateWrapper::State(EPluginState psValue)

//  Sets the current plugin state, see header for allowable states
//
//****************************************************************************
{
  _pluginStateStruct.m_State = psValue;
}

//-------------------Expires----------------------John Mertus----Feb 2005-----

	MTI_TIME CPluginStateWrapper::Expires(void) const

// Returns the expiratio ntime
//
//****************************************************************************
{
  return _pluginStateStruct.m_Expires;
}
//-------------------Expires----------------------John Mertus----Feb 2005-----

	void CPluginStateWrapper::Expires(MTI_TIME et)

//  Sets the Expiration time
//
//****************************************************************************
{
  _pluginStateStruct.m_Expires = et;
}

//-------------------strReason--------------------John Mertus----Apr 2004-----

	string CPluginStateWrapper::strReason(void) const

//  Returns an ascii reason for the state, this is valid only after a call
// to State.
//
//****************************************************************************
{
  return _pluginStateStruct.m_strReason;
}

//-------------------strReason--------------------John Mertus----Apr 2004-----

	void CPluginStateWrapper::strReason(const string &strValue)

//  Set the status to the string value
//
//****************************************************************************
{
  strncpy(_pluginStateStruct.m_strReason, strValue.c_str(), 1000);
  _pluginStateStruct.m_strReason[1000] = '\0';
}

//-------------------getWrappedPluginState-----------------------Jan 2020-----

PluginStateStruct &CPluginStateWrapper::getWrappedPluginState()

//  Set the status to the string value
//
//****************************************************************************
{
  return _pluginStateStruct;
}


//***********************************CPLUGINLIST*******************************

//----------------Constructor--------------John Mertus--March 2000------

     CPluginList::CPluginList(string Mask)

//  This creates a file and loads the plugins as defined by the mask
//
//***********************************************************************
{
  LoadPlugins(Mask);
}

//----------------Destructor----------------John Mertus--March 2000------

     CPluginList::~CPluginList(void)

//  This release all the resources of the shared object.
//
//***********************************************************************
{
    for (iterator i = begin(); i != end(); i++)
       delete *i;
}


//----------------LoadPlugin------------------------John Mertus-----Jan 2001---

  bool CPluginList::LoadPlugin(string FName)

// This loads one plugin from a file name
//   FName is any file name
//   Return is true if the plugin is loaded, false otherwise.
//
//*****************************************************************************
{
    //
    //  See if it is of our type
    //
    CPlugin *pi = new CPlugin(FName);
    if (pi->ErrorNo != 0)
	  {
    	TRACE_1(errout << "CPlugin::LoginPlugin failed " << FName);
        return(false);              // not loaded
	  }

	push_back(pi);
    return(true);
}


/*
//----------------GetPluginByName------------------John Mertus-----Jan 2001---

  CPlugin *TConfigureForm::GetPluginByName(string Name)

//
//  The action is simple, for each configured device, this header is
//  written, then the configured device is called to fill in the data
//
//  This procedure looks at all the loaded plugins and finds the first (and should be
//  only one) that matches the file name
//
// ****************************************************************************
{
  CPlugin *Result;

  for (int i=0; i < AvailableListBox->Items->Count; i++)
    {
      Result = (CPlugin *)AvailableListBox->GetData(i);
      if (Result != NULL)
        {
           if (Result->GetPluginName() == Name) return(Result);
        }
    }

  // Failure, no match
  return(NULL);
}
*/


