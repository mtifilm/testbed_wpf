/*
   File Name:  TheError.cpp
   Created: May 3, 2002 by John Mertus
   Version 1.00
*/

#include <string>
#include <vector>
#include <queue>
#include "machine.h"

typedef vector<string> StringList;
typedef vector<int> IntegerList;
typedef vector<bool> BoolList;
typedef MTI_UINT32 ErrorTID;
typedef vector<ErrorTID> TIDList;

#include "TheError.h"
#include "IniFile.h"
#include "MTIstringstream.h"
#include "ThreadLocker.h"

/*********************************CMTIErrReport****************************/


//----------------------Find----------------------John Mertus----Apr 2002-----

      int CMTIErrReport::Find(ErrorTID tid)

// This takes a TID and returns the position in the list of that thread id.
//
//****************************************************************************
{
  for (unsigned i=0; i < TIDs.size(); i++)
    if (tid == TIDs[i]) return(i);

  return(-1);
}

//-----------------getCurrentTID-----------------John Mertus----Apr 2002-----

    ErrorTID CMTIErrReport::getCurrentTID(void)

//  This returns the current thread ID
//  Needed because Unix and Windows have different approaches to thread ID's
//
//****************************************************************************
{
  return GetCurrentThreadId();
}

//---------------------SetMessage------------------John Mertus----Apr 2002---

     void CMTIErrReport::setMessage(const string &ErrMsg)

// This sets the current error message of teh current thread
//   ErrMsg is a human readable error string.
//
//***************************************************************************
{
  CAutoThreadLocker lock(*pLocker);
  MTI_UINT32 tid = getCurrentTID();
  //
  // Insert the information into the thread ID
  int iPos = Find(tid);
  if (iPos == -1)
    {
      TIDs.push_back(tid);
      ErrorMsgs.push_back(ErrMsg);
      ErrorNums.push_back(-1);
      return;
    }

  // Found, replace the error Message
  ErrorMsgs[iPos] = ErrMsg;
}

//---------------------setError------------------John Mertus----Apr 2002---

     void CMTIErrReport::setError(const int Err)

// This sets the current error number of the current thread
//   Err is the error number.
//
//***************************************************************************
{
  CAutoThreadLocker lock(*pLocker);
  MTI_UINT32 tid = getCurrentTID();
  //
  // Insert the information into the thread ID
  int iPos = Find(tid);
  if (iPos == -1)
    {
      TIDs.push_back(tid);
      ErrorMsgs.push_back("");
      ErrorNums.push_back(Err);
      return;
    }

  // Found, replace the error Message
  ErrorNums[iPos] = Err;
}

//---------------------Set-------------------------John Mertus----Apr 2002---

     void CMTIErrReport::set(const int Err, const string &ErrMsg)

// This set the error message and error number in the current thread.
//   Err is the error number
//   ErrMsg is a human readable error string.
//
//***************************************************************************
{
  CAutoThreadLocker lock(*pLocker);
  MTI_UINT32 tid = getCurrentTID();
  //
  // Insert the information into the thread ID
  int iPos = Find(tid);
  if (iPos == -1)
    {
      TIDs.push_back(tid);
      ErrorMsgs.push_back(ErrMsg);
      ErrorNums.push_back(Err);
      return;
    }

  // Found, replace the error
  ErrorMsgs[iPos] = ErrMsg;
  ErrorNums[iPos] = Err;
}

//------------------getFmtError-----------------John Mertus----Jun 2002-----

      string CMTIErrReport::getFmtError(MTI_UINT32 tid)

//   This returns a Formated Error Message of the form
//  Error <error>: Message
//  Where error is the error number and Message is the current message
//  If (tid != 0) that thread is used.
//
//****************************************************************************
{
  MTIostringstream os;
  os << "Error " << getError(tid) << ": " << getMessage(tid);
  return(os.str());
}

//-------------------getError---------------------John Mertus----Apr 2002-----

       int CMTIErrReport::getError(MTI_UINT32 tid)

//  This returns the error number associated with thread id tid
//  If tid is not specified, the current thread is used
//
//****************************************************************************
{
   // Lock other people out
   CAutoThreadLocker lock(*pLocker);

   // If no thread ID, get the current
   if (tid == 0) tid = getCurrentTID();
   int iPos = Find(tid);

   // Not found return nothing
   if (iPos == -1)
     {
        return(0);
     }

   // Return the error number
   int iResult = ErrorNums[iPos];
   return(iResult);
}

//-------------------Notify----------------------John Mertus----Apr 2002-----

   int CMTIErrReport::Notify(void)

//  This seralizes the display and sends a message to the top form
//  Return is after message is processed.
//
//****************************************************************************
{
   if (vpHandle == NULL) return(0);
   // Push a copy of the data into the notify queue
   // This will be taken off when the data is received.
   NotifyData *nd = new NotifyData;
   nd->vpClass = vpHandle;
   nd->Error = getError();
   nd->Message = getMessage();
   nd->tid = getCurrentTID();
   nd->bWait = true;
   NotifyQueue.push(nd);
   SendMessage((HWND)vpHandle, CM_ERRORHANDLER, nd->tid, 0);
   int iRet = nd->iRet;
   delete nd;
   return(iRet);
}

//-------------------NotifyNoWait----------------John Mertus----Apr 2002-----

   void CMTIErrReport::NotifyNoWait(void)

//  This seralizes the display and sends a message to the top form
//  Return is at once/
//
//****************************************************************************
{
   if (vpHandle == NULL) return;
   // Push a copy of the data into the notify queue
   // This will be taken off when the data is received.
   NotifyData *nd = new NotifyData;
   nd->vpClass = vpHandle;
   nd->Error = getError();
   nd->Message = getMessage();
   nd->tid = getCurrentTID();
   nd->bWait = false;
   NotifyQueue.push(nd);
   PostMessage((HWND)vpHandle, CM_ERRORHANDLER, theError.getCurrentTID(), 0);
}

//-------------------getMessage-------------------John Mertus----Apr 2002-----

   string CMTIErrReport::getMessage(int &iErr, ErrorTID tid)

       //  This returns the error message associated with thread id tid
//  If tid is not specified, the current thread is used
//
//****************************************************************************
{
   // Lock other people out
   CAutoThreadLocker lock(*pLocker);

   // If no thread ID, get the current
   if (tid == 0) tid = getCurrentTID();
   int iPos = Find(tid);

   // Not found return nothing
   if (iPos == -1)
     {
        iErr = 0;
        return("");
     }

   // Return the error number
   string sResult = ErrorMsgs[iPos];
   iErr = ErrorNums[iPos];
   return(sResult);
}


//-------------------getMessage-------------------John Mertus----Apr 2002-----

       string CMTIErrReport::getMessage(ErrorTID tid)

//  This returns the error message associated with thread id tid
//  If tid is not specified, the current thread is used
//
//****************************************************************************
{
   int iErr;
   return getMessage(iErr,tid);
}

//-----------------getAllMessages-----------------John Mertus----Apr 2002-----

   StringList CMTIErrReport::getAllMessages(IntegerList *ilErr, TIDList *ilTID)

// This returns all the Messages for all the threads.
//  If ilErr is not null, return the errors
//  if ilTID is not null, return the thread ids of the error
//
//****************************************************************************
{
   CAutoThreadLocker lock(*pLocker);
   StringList Result = ErrorMsgs;
   if (ilErr != NULL) *ilErr = ErrorNums;
   if (ilTID != NULL) *ilTID = TIDs;
   return(Result);
}

//---------------------Remove---------------------John Mertus----Apr 2002-----

     void CMTIErrReport::Remove(MTI_UINT32 tid)

//  This deletes all the error information for the thread tid.
//
//****************************************************************************
{
   CAutoThreadLocker lock(*pLocker);
   int iPos = Find(tid);
   if (iPos > 0)
     {
        ErrorMsgs.erase(ErrorMsgs.begin() + iPos);
        ErrorNums.erase(ErrorNums.begin() + iPos);
        TIDs.erase(TIDs.begin() + iPos);
     }
}

//------------------getAllErrors------------------John Mertus----Apr 2002-----

   IntegerList CMTIErrReport::getAllErrors(void)

//  This returns all the errors for all threads
//
//****************************************************************************
{
   CAutoThreadLocker lock(*pLocker);
   IntegerList Result = ErrorNums;
   return(Result);
}


//------------------Constructor------------------John Mertus----Apr 2002-----

   CMTIErrReport::CMTIErrReport(void)

//  Initialize the properties
//
//****************************************************************************
{
  vpHandle = NULL;
  pLocker = new CThreadLock;
}

//------------------Destructor------------------John Mertus----Apr 2002-----

   CMTIErrReport::~CMTIErrReport(void)

//  Just free up the vectors
//
//****************************************************************************
{
  TIDs.clear();
  ErrorNums.clear();
  ErrorMsgs.clear();
  delete pLocker;
}

//     void CMTIErrReport::set(const int Err, const MTIostringstream &osErr) {set(Err, osErr.str()); }
//     void CMTIErrReport::set(const string &ErrMsg, const int Err) {set(Err, ErrMsg); }
//     void CMTIErrReport::setMessage(const MTIostringstream &osErr) {setMessage(osErr.str()); }

// This is the error singleton
CMTIErrReport theError;

//----------------------------------------------------------------------------
// CAutoErrorReporter
//

CAutoErrorReporter::CAutoErrorReporter(EAutoErrorNotify notify,
                                       EAutoErrorTrace trace)
: errorCode(0),
  notifyAction(notify), traceAction(trace)
{
}

CAutoErrorReporter::CAutoErrorReporter(string newTracePrefix,
                                       EAutoErrorNotify notify,
                                       EAutoErrorTrace trace)
: errorCode(0),
  notifyAction(notify), traceAction(trace), tracePrefix(newTracePrefix)
{
}

CAutoErrorReporter::~CAutoErrorReporter()
{
   // Error code is zero, so do nothing
   if (errorCode == 0)
      return;

   // Set the error code and message in theError
   theError.set(errorCode, msg);

   // Construct TRACE message
   MTIostringstream traceMsg;
   string traceLabel = tracePrefix;
   if (!tracePrefix.empty())
      traceLabel += ": ";
   traceMsg << "ERROR: " << traceLabel << msg.str() << " Error = " << errorCode;

   // TRACE at requested level
   switch(traceAction)
      {
      case AUTO_ERROR_NO_TRACE:
      default:
         // No trace
         break;
      case AUTO_ERROR_TRACE_0:
         TRACE_0(errout << traceMsg.str());
         break;
      case AUTO_ERROR_TRACE_1:
         TRACE_1(errout << traceMsg.str());
         break;
      case AUTO_ERROR_TRACE_2:
         TRACE_2(errout << traceMsg.str());
         break;
      }

   // Do requested Notify action
   switch(notifyAction)
      {
      case AUTO_ERROR_NO_NOTIFY:
      default:
         // No notify
         break;
      case AUTO_ERROR_NOTIFY_WAIT:
         theError.Notify();
         break;
      case AUTO_ERROR_NOTIFY_NO_WAIT:
         theError.NotifyNoWait();
         break;
      }
}

void CAutoErrorReporter::RepeatTheError()
{
   // Pass up the error code and message already in theError
   errorCode = theError.getError();
   msg << theError.getMessage();
}



