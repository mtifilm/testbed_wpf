/*
   File Name:  TheError.h
   Created: May 3, 2002 by John Mertus
   Version 1.00
   Version 2.00 Completely revised Aug 2001
           2.01 Changed case insensitive checking.
           2.02 Added Read Lists
   This is a rework of the DOS ini Library into C++
   with an interface almost exactly as Borland's interface

*/
#ifndef TheErrorH
#define TheErrorH

#include <string>
#include <vector>
#include <queue>
#include <stdio.h>
#include "machine.h"
#include "err_defs.h"
#include "corecodelibint.h"
#include "MTIstringstream.h"

using std::vector;
using std::string;
class CThreadLock;

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)  // Too hard to fix, should be OK
#endif


//
// Some simple type defs
//

typedef vector<string> StringList;
typedef vector<int> IntegerList;
typedef vector<bool> BoolList;
typedef MTI_UINT32 ErrorTID;
typedef vector<ErrorTID> TIDList;

typedef struct
{
   int  Error;              // The Error Number
   string Message;          // An error Message
   ErrorTID tid;            // The thread ID that called it.
   void *vpClass;           // The receiving class
   bool bWait;              // Used to wait for notification
   int  iRet;               // Return value
} NotifyData;


#ifdef _WINDOWS
#include <windows.h>
//
//--------------------------Serialization code----------------------------
//
//  The following macros define a way of a thread reporting an error back
//  to the main gui loop. The main gui loop defines an error handler, and
//  when a thread encounters an error, it posts a message that causes the
//  error handler to be run.
//
//  This is necessary because threads cannot update GUIs.  For windows
//  a message is sent, for X, a more complicated posix signal is used.
//
//  Step 1: In a gui class.h, say the MainWindow, under protected,
//  define the macro
//  protected:
//  BEGIN_MESSAGE_MAP
//    DEFINE_ERROR_MESSAGE_HANDLER
//  END_MESSAGE_MAP(TMTIForm)
//
//  Note: If BEGIN_MESSAGE_MAP is already defined, stick the DEFINE_ERROR_MESSAGE_HANDLER
//    in that map.
//
//    DEFINE_ERROR_HANDLER(MainWindow);
//
//  Step 2:  In the constructor, enter the macro
//    INIT_ERROR_HANDLER;
//
//  Step 3: In the .cpp file for the gui class, define your handler by
//  int MainWindow::ErrorHandler(const ErrorTID etid, const int Error, const string &Message)
//   {
//     Error and Message the the error message and number
//     return int;  // used as a return to Notify 
//   }
//
//  For each thread, to report an error do
//   theError.Notify();
//  The Notify send a message off and returns
//  To wait for the error handler to complete before the return do
//   theError.NotifyNoWait();
//  
//  Note: There is one and only one error handler, multiple
//    DEFINE_ERROR_HANDLERS will not work properly!
//
//******************************Window Macros*****************************
#ifndef _MSC_VER // wtf??
#ifndef ETIMEDOUT
#define ETIMEDOUT  WAIT_TIMEOUT // Unix - windows equivalent
#endif
#endif

#define CM_ERRORHANDLER (WM_APP + 1315)
#define DEFINE_ERROR_HANDLER(A) \
void __fastcall _ErrorHandler(TMessage &Message) { \
    if (theError.NotifyQueue.empty()) return; \
    NotifyData *nd = theError.NotifyQueue.front(); \
    nd->iRet = ErrorHandler(nd->tid, nd->Error, nd->Message);  \
    if (!nd->bWait) delete nd; \
    theError.NotifyQueue.pop(); \
    return; \
    }\
int ErrorHandler(const ErrorTID etid, const int nd, const string &Messsage);

#define DEFINE_ERROR_MESSAGE_HANDLER MESSAGE_HANDLER(CM_ERRORHANDLER, TMessage, _ErrorHandler)
#define DEFINE_ERROR_HANDLER_COMPLETE(A) \
DEFINE_ERROR_HANDLER(A) \
BEGIN_MESSAGE_MAP  \
  MESSAGE_HANDLER(CM_ERRORHANDLER, TMessage, _ErrorHandler) \
END_MESSAGE_MAP(TForm)

#define INIT_ERROR_HANDLER theError.vpHandle = Handle;
#else
#include <sys/errno.h>
#include <pthread.h>
#include <signal.h>
#include <semaphore.h>
typedef void (*StaticErrorProc)(void);
#define DEFINE_ERROR_HANDLER(A)\
static void _ErrorHandler(void) { \
    if (theError.NotifyQueue.empty()) return; \
    NotifyData *nd = theError.NotifyQueue.front(); \
    nd->iRet = ((A *)(nd->vpClass))->ErrorHandler(nd->tid, nd->Error, nd->Message);  \
    bool bW = nd->bWait; \
    if (!nd->bWait) delete nd; \
    theError.NotifyQueue.pop(); \
    if (bW) theError.NotifyCompleted(); \
    return; \
    }\
int ErrorHandler(const ErrorTID etid, const int nd, const string &Messsage) \

#define INIT_ERROR_HANDLER \
InitTheErrorHandler(this, _ErrorHandler);

void InitTheErrorHandler(void *vp, StaticErrorProc proc);
#endif


//  The error class
//   Note this class is realized only once and accessed through theError
//  Never use this class directly
//
//  Note:  This depends upon the fact that thread idenitifers can be mapped to
//  unsigned integers.  Although this may not be true in all Unix system, it is true
//  on all our systems.
//
class MTI_CORECODELIB_API CMTIErrReport
{
   public:
     CMTIErrReport();
     ~CMTIErrReport();
     //
     //  Actual error setters
     void set(const int Err) {set(Err, ""); }
     void set(const int Err, const string &ErrMsg);
     void set(const int Err, const MTIostringstream &osErr) {set(Err, osErr.str()); }
     void set(const string &ErrMsg, const int Err=-1) {set(Err, ErrMsg); }

     void setMessage(const string &ErrMsg);
     void setMessage(const MTIostringstream &osErr) {setMessage(osErr.str()); }

     void setError(const int Err);

     int getError(ErrorTID tid = 0);
     string getMessage(ErrorTID tid = 0);
     string getMessage(int &iErr, ErrorTID tid=0);
     string getFmtError(ErrorTID tid = 0);

     IntegerList getAllErrors(void);
     StringList getAllMessages(IntegerList *ilErr=NULL, TIDList *ilTID=NULL);

     ErrorTID getCurrentTID(void);
     void Remove(ErrorTID tid);
     int Notify(void);
     void NotifyNoWait(void);
     //
     // The notification varaiables differe between the two systems
     // in Window vpHandle is a handle of the form to receive the message
     // in X vpHandle represents the class to receive the message.
     void *vpHandle;
     std::queue<NotifyData *> NotifyQueue;

   private:
     CThreadLock *pLocker;
     int Find(ErrorTID TID);
     // These translate the setters and getters into property ones
     void SetError1(int Err) {setError(Err); }
     int GetError1(void) { return getError(); }
     string GetFmtError1(void) { return getFmtError(); }
     void SetMessage1(string Msg) {setMessage(Msg); };
     string GetMessage1(void) {return getMessage(); };
     TIDList             TIDs;
     StringList          ErrorMsgs;
     IntegerList         ErrorNums;
};


extern MTI_CORECODELIB_API CMTIErrReport theError;

//----------------------------------------------------------------------------
// CAutoErrorReporter
//
//   CAutoErrorReporter is a convenience wrapper around theError that makes 
//   it a little bit easier to set theError and do a TRACE with less clutter.
//
//   CAutoErrorReporter is used by creating a local instance within a
//   function.  The user's function sets the CAutoErrorReporter member
//   variables errorCode and msg.  When the function returns, the
//   CAutoErrorReporter's destructor is called.  If errorCode
//   was set, then the destructor sets theError, and optionally calls TRACE and 
//   theError.Notify.
//
// Example Usage:
//
//  int CMyClass::myFunction(int arg)
//     {
//     int retVal;
//
//     // Create local instance of CAutoErrorReporter
//     CAutoErrorReporter autoErr("CMyClass::myFunction", AUTO_ERROR_NOTIFY, AUTO_ERROR_TRACE_0);
//
//     // body of function ......
//
//     if (arg != 7)
//        {
//        // Set the error code and message string
//        autoErr.errorCode = SOME_ERROR_CODE;
//        autoErr.msg << "Some error message with arg = " << arg;
//        return SOME_ERROR_CODE;   // when autoErr's destructor is caller
//                                  // theError.set, TRACE_0 and
//                                  // theError.Notify are called
//        }
//
//     return 0;    // autoErr is destroyed here, but does nothing because errorCode is zero.


// Notification Action
enum EAutoErrorNotify
{
   AUTO_ERROR_NO_NOTIFY,         // Set theError, but do not call Notify
   AUTO_ERROR_NOTIFY_WAIT,       // Notify and wait for return
   AUTO_ERROR_NOTIFY_NO_WAIT     // Notify and return immediately
};

// Trace Action
enum EAutoErrorTrace
{
   AUTO_ERROR_NO_TRACE,          // No TRACE
   AUTO_ERROR_TRACE_0,           // TRACE level 0
   AUTO_ERROR_TRACE_1,           // TRACE level 1
   AUTO_ERROR_TRACE_2            // TRACE level 2
};


class MTI_CORECODELIB_API CAutoErrorReporter
{
public:
   CAutoErrorReporter(EAutoErrorNotify notify = AUTO_ERROR_NO_NOTIFY,
                      EAutoErrorTrace trace = AUTO_ERROR_TRACE_0);
   CAutoErrorReporter(string newTracePrefix,
                      EAutoErrorNotify notify = AUTO_ERROR_NO_NOTIFY,
                      EAutoErrorTrace trace = AUTO_ERROR_TRACE_0);
   ~CAutoErrorReporter();  // real work done here

   void RepeatTheError();

public:
   MTIostringstream msg;   // supports formatting
   int errorCode;       // initialized to 0, indicating no error
   EAutoErrorNotify notifyAction;
   EAutoErrorTrace traceAction;
   string tracePrefix;
};

#ifdef _MSC_VER
#pragma warning(pop)
#endif


//----------------------------------------------------------------------------

#endif
