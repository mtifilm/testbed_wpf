/*
   File Name:  BCDLIB.h
   Create:  July 30, 2000 by John Mertus
   Version 1.00   Initial Release

   Just some BCD functions

*/

#ifndef _BCDLIBH
#define _BCDLIBH

#include "corecodelibint.h"
#include "machine.h"
typedef MTI_UINT32 bcdTC;                        // BCD timecode packed into an int
//
// BCD routines
//
  MTI_CORECODELIB_API unsigned char IntToBCD(int);                  // Integer to one BCD character
  MTI_CORECODELIB_API int BCDToInt(unsigned char);                  // one BCD character to integer
#endif
