/*
   File Name:  ThreadLocker.cpp
   Created: Aug 1, 2004 by John Mertus
   Version 1.00
*/

#include "ThreadLocker.h"
#include "IniFile.h"
#include "windows.h"

void CoreCodePopupErrorMessage(const string &message)
{
   TRACE_0(errout << message);
   string fullMessage = message + string("\nPLEASE REPORT THIS TO DEVELOPERS!");

   MessageBox(0, message.c_str(), 0, MB_ICONSTOP | MB_OK | MB_TOPMOST);
}

//----------------Constructor-----------------------John Mertus----Jul 2000---

    CThreadLock::CThreadLock(void)

//  Creates an underlying Critical Section
//
/****************************************************************************/
{
   InitializeCriticalSection(&m_CriticalSection);
}


//-----------------Destructor-----------------------John Mertus----Jul 2000---

    CThreadLock::~CThreadLock(void)

//  Destroys the underlying Critical Section
//
/****************************************************************************/
{
   try
   {
      DeleteCriticalSection(&m_CriticalSection);
   }
   catch (...)
   {
      CoreCodePopupErrorMessage("EXCEPTION: from DeleteCriticalSection() in ~CThreadLock!!");
   }
}

/*---------------Lock-----------------------------John Mertus----Jul 2000---*/

    bool CThreadLock::Lock(const char *msg)

/*   This waits while the lock is already locked and then locks it.         */
/*  'msg' is left over from UNIX days, should be removed!                   */
/*                                                                          */
/****************************************************************************/
{
  EnterCriticalSection(&m_CriticalSection);
  return true;
}

/*---------------TryToLock---------------------------------------Jan 2010---*/

	bool CThreadLock::TryToLock()

/*   Tries to lock and returns true if successful, else false (no waiting)  */
/*                                                                          */
/****************************************************************************/
{
  return (TryEnterCriticalSection(&m_CriticalSection) != 0);
}

/*---------------TryToLock---------------------------------------Jan 2010---*/

	int CThreadLock::LetsTryToLock()

/*   Tries to lock and returns true if successful, else false (no waiting)  */
/*                                                                          */
/****************************************************************************/
{
  return this->TryToLock();
}

/*---------------UnLock---------------------------John Mertus----Jul 2000---*/

      bool CThreadLock::UnLock(const char *msg)

/*   This tells the thread that the data can now be safely changed.         */
/*  'msg' is left over from UNIX days, should be removed!                   */
/*                                                                          */
/****************************************************************************/
{
   try
   {
      LeaveCriticalSection(&m_CriticalSection);
   }
   catch (...)
   {
      CoreCodePopupErrorMessage("EXCEPTION: from LeaveCriticalSection() in CThreadLock!!");
   }

	return true;
}

//*****************CSystemThreadLock*****************************************

//----------------Constructor-----------------------John Mertus----Dec 2005---

    CSystemThreadLock::CSystemThreadLock(const string &str)

//  Creates a mutex with name str
//
/****************************************************************************/
{
   m_sMutexName = str;
   m_Mutex = CreateMutex(NULL, false, m_sMutexName.c_str());
   if (m_Mutex == NULL)
     {
       throw(this);
     }
}

//-----------------Destructor-----------------------John Mertus----Dec 2005---

    CSystemThreadLock::~CSystemThreadLock(void)

//  Releases the mutex/
//
/****************************************************************************/
{
   CloseHandle(m_Mutex);
}

/*---------------Lock-----------------------------John Mertus----Dec 2005---*/

	bool CSystemThreadLock::Lock()

/*   This waits for the thread to become available and then locks it.       */
/*  msg is displayed if the lock fails in one second.                       */
/*                                                                          */
/****************************************************************************/
{
  int iErr = WaitForSingleObject(m_Mutex, INFINITE);
  if ((iErr != 0) && (iErr != (int)WAIT_FAILED))
    {
      return(false);
    }
  return(true);
}

/*---------------UnLock---------------------------John Mertus----Dec 2005---*/

      bool CSystemThreadLock::UnLock()

/*   This tells the thread that the data can now be safely changed.         */
/*                                                                          */
/****************************************************************************/
{
   return(ReleaseMutex(m_Mutex));
}

//------------------------------------------------------------------
//
// SPIN LOCK

#if ROLL_OUR_OWN_SPIN_LOCK

CSpinLock::CSpinLock()
: mLock(0),
  mNesting(0)
{
};

#else

CSpinLock::CSpinLock(void)
{
  // I read somewhere that the Windows heap allocator uses 4000
  // as a spin count, so I just jacked it up a bit
  InitializeCriticalSectionAndSpinCount(&m_CriticalSection, 10000);
}

CSpinLock::~CSpinLock(void)
{
    try
    {
		DeleteCriticalSection(&m_CriticalSection);
    }
    catch (...)
    {
        CoreCodePopupErrorMessage("EXCEPTION: from DeleteCriticalSection() in ~CSpinLock!!");
    }
}

#endif
//------------------------------------------------------------------
