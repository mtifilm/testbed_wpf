
#include "timecode.h"

const CTimecode CTimecode::NOT_SET(-1);


//
// implementation file for class CTimecode
//
//////////////////////////////////////////////////////////////
//
//		C O N S T R U C T O R S 
//
//////////////////////////////////////////////////////////////
CTimecode::CTimecode(
                        int frms
                    )
{
   // set the value and force the other vbles to zero
   frames = frms;
   flags  = 0;
   framesPerSecond = 0;
}   

CTimecode::CTimecode(
			int flgs, // flags 
			int fps   // frames-per-second
			)
{
   // set the frames to zero and
   frames = 0;
   flags = 0;
   framesPerSecond = fps;
   if (framesPerSecond!=0) {
      flags = flgs;
   }
}

CTimecode::CTimecode(
			int hrs,
			int mins,
			int secs,
			int frms,
			int flgs,
			int fps
		   )
{
   // assume it's absolute mode
   frames = frms;
   flags  = 0;

   framesPerSecond = fps;
   if (framesPerSecond!=0) { // not absolute mode
      flags = flgs;
      frames = ((hrs*60+mins)*60+secs)*fps+frms;
   }
   framesPerSecond = fps;
   validateTimecode();
}

CTimecode::CTimecode()
{
   // set the value to zero and force the other vbles to zero
   frames = 0;
   flags  = 0;
   framesPerSecond = 0;
}

CTimecode::~CTimecode()
{
}

//////////////////////////////////////////////////////////////
//
//		M U T A T O R S
//
//////////////////////////////////////////////////////////////

// Function to set the time in simple frames
void CTimecode::setAbsoluteTime(
                        int frms
                       )
{
   frames = frms;
}

// Function to set the timevalue
void CTimecode::setTime(
			int hrs,	// hours
			int mins,	// minutes
			int secs,	// seconds
			int frms	// frames
			)
{
   // assume absolute mode
   frames = frms;

   if (framesPerSecond!=0) { // not absolute mode
      frames = ((hrs*60+mins)*60+secs)*framesPerSecond+frms;
   }
   validateTimecode();
}

void CTimecode::setFrameCount(int iFrameCount)
{
  // set time to frms
  this->setAbsoluteTime(0);   // 00:00:00:00
  (*this) = (*this) + iFrameCount;
}

bool CTimecode::setTimeASCII(
			const char *timestr
			)
{
   int hrs, mins, secs, frms;

   frames = 0;

   if (framesPerSecond==0) { // ___FFFFFFFF

      // 3/19/02 - assuming the field is exactly
      // 11 chars there should be at least 3 leading
      // spaces. But we don't want to break anything
      // else so we don't enforce this 

      while (*timestr==' ') {
         timestr++;
      }
      int i=0;
      while ((isdigit(*timestr))&&(*timestr!=0)) {
         frames = frames*10 + (*timestr++ - '0');
         i++;
      }
      if ((*timestr!=0)||(i>8)) return(false);

   }
   else { // HH:MM:SS:FF

      if (!isdigit(*timestr)) return(false);
      hrs = *timestr++ - '0';
      if (!isdigit(*timestr)) return(false);
      hrs = hrs*10 + *timestr++ - '0';
      if (*timestr++!=':') return(false);

      if (!isdigit(*timestr)) return(false);
      mins = *timestr++ - '0';
      if (!isdigit(*timestr)) return(false);
      mins = mins*10 + *timestr++ - '0';
      if (*timestr++!=':') return(false);

      if (!isdigit(*timestr)) return(false);
      secs = *timestr++ - '0';
      if (!isdigit(*timestr)) return(false);
      secs = secs*10 + *timestr++ - '0';
      if ((*timestr!=':')&&(*timestr!='.')) return(false);
      timestr++;
      if (!isdigit(*timestr)) return(false);
      frms = *timestr++ - '0';
      if (!isdigit(*timestr)) return(false);
      frms = frms*10 + *timestr++ - '0';

      if ((flags&FRAME720P)!=0) {
         frms *= 2;
         if (*timestr=='.') {
            frms++;
            timestr++;
         }
         else if (*timestr==' ') {
            timestr++;
         }
      }

      if (*timestr!=0) return(false);

      frames = ((hrs*60+mins)*60+secs)*framesPerSecond + frms;

   }

   validateTimecode();
   return(true);
}

void CTimecode::setHours(
			int hrs
			)
{
   if (framesPerSecond!=0) {

      int mins, secs, frms;

      frms   = frames%framesPerSecond;
      frames = frames/framesPerSecond;
      secs   = frames%60;
      frames = frames/60;
      mins   = frames%60;

      frames = ((hrs*60+mins)*60+secs)*framesPerSecond + frms;
      validateTimecode();
   }
}

void CTimecode::setMinutes(
			int mins
			)
{
   if (framesPerSecond!=0) {

      int hrs, secs, frms;

      frms   = frames%framesPerSecond;
      frames = frames/framesPerSecond;
      secs   = frames%60;
      frames = frames/60;
      hrs    = frames/60;

      frames = ((hrs*60+mins)*60+secs)*framesPerSecond + frms;
      validateTimecode();
   }
}

void CTimecode::setSeconds(
			int secs
			)
{
   if (framesPerSecond!=0) {

      int frms;

      frms   = frames%framesPerSecond;
      frames = frames/framesPerSecond;
      frames = frames/60;

      frames = (frames*60+secs)*framesPerSecond + frms;
      validateTimecode();
   }
}

void CTimecode::setFrames(
			int frms
			)
{
   if (framesPerSecond==0) {
      frames = frms;
   }
   else {
      frames = frames/framesPerSecond;

      frames = frames*framesPerSecond + frms;
      validateTimecode();
   }
}

void CTimecode::setFlags(
                         int flgs
			)
{
   flags = 0;
   if (framesPerSecond!=0) {
      flags = flgs;
      validateTimecode();
   }
}

void CTimecode::setDropFrame(bool value)
{
   if (value)
      setFlags(flags | DROPFRAME);    // Set flag bit
   else
      setFlags(flags & ~DROPFRAME);   // Clear flag bit
}

void CTimecode::setHalfFrame(bool value)
{
   if (value)
      setFlags(flags | HALFFRAME);    // Set flag bit
   else
      setFlags(flags & ~HALFFRAME);   // Clear flag bit
}

void CTimecode::set720P(bool value)
{
   if (value)
      setFlags(flags | FRAME720P);    // Set flag bit
   else
      setFlags(flags & ~FRAME720P);   // Clear flag bit
}

void CTimecode::setField(bool newField)
{
   // set to false for field 1 or don't care
   // set to true for field 2

   if (newField)
      setFlags(flags | TC_FIELDFLAG);    // Set flag bit
   else
      setFlags(flags & ~TC_FIELDFLAG);   // Clear flag bit
}

void CTimecode::setFramesPerSecond(
		        int fps
			)
{
   framesPerSecond = fps;
   if (framesPerSecond==0) flags=0;
   validateTimecode();
}

//////////////////////////////////////////////////////////////
//
//		A C C E S S O R S
//
//////////////////////////////////////////////////////////////

int CTimecode::getAbsoluteTime() const
{
   return(frames);
}

int CTimecode::getFrameCount() const
{
  CTimecode *tcLocal;
  int iFrameCount;

  tcLocal = new CTimecode (*this);
  tcLocal->setAbsoluteTime (0);  // 00:00:00:00
  iFrameCount = *this - *tcLocal;
  delete tcLocal;
  return iFrameCount;
}


void CTimecode::getTimeASCII(char *timestr) const
{
   if (framesPerSecond==0) { // FFFFFFFFFFF

      // here we simply convert the frames
      // to an integer, with enough leading
      // zeroes to make 11 places. This way
      // the size is the same as HH:MM:SS:FF.

      int tmp = frames;
      char dig[12];

      int i = 0;
      dig[i++] = tmp%10 + '0';
      tmp = tmp/10;
      while (tmp!=0) {
         dig[i++] = tmp%10 + '0';
         tmp = tmp/10;
      }
      while (i<11) dig[i++] = ' ';
      while (i>0) *timestr++ = dig[--i];

   }
   else { // HH:MM:SS:FF

      int hrs, mins, secs, frms;

      hrs  = frames;
      frms = hrs%framesPerSecond;
      hrs  = hrs/framesPerSecond;
      secs = hrs%60;
      hrs  = hrs/60;
      mins = hrs%60;
      hrs  = hrs/60;

      if (hrs>99) hrs = 99;

      *timestr++ = hrs/10 + '0';
      *timestr++ = hrs%10 + '0';
      *timestr++ = ':';
      *timestr++ = mins/10 + '0';
      *timestr++ = mins%10 + '0';
      *timestr++ = ':';
      *timestr++ = secs/10 + '0';
      *timestr++ = secs%10 + '0';

      *timestr++ = ':';
      if ((flags&DROPFRAME)!=0) *(timestr-1) = '.';

      if ((flags&FRAME720P)!=0) {

         // bug repaired 9/25/01 KT

         if (frms&1) {
            frms /= 2;
            *timestr++ = frms/10 + '0';
            *timestr++ = frms%10 + '0';
            *timestr++ = '.';
         }
         else {
            frms /= 2;
            *timestr++ = frms/10 + '0';
            *timestr++ = frms%10 + '0';
            *timestr++ = ' '; // bug fixed 1/29/02
         }

      }
      else {
         *timestr++ = frms/10 + '0';
         *timestr++ = frms%10 + '0';
      }

   }

   *timestr = 0;
}

int CTimecode::ASCII2FrameCount(const char *timestr) const
/*
	This function translates an ASCII string into a frame count
*/
{
  CTimecode *tcLocal;
  int iFrameCount;

  if (timestr[0] == '\0')
    return -1;

  tcLocal = new CTimecode (*this);
  tcLocal->setTimeASCII (timestr);
  iFrameCount = tcLocal->getFrameCount();

  delete tcLocal;

  return iFrameCount;
}

void CTimecode::FrameCount2ASCII(char *timestr, int iFrameCount) const
/*
	This function translates a frame count into an ASCII string.
*/
{
  CTimecode *tcLocal;

  if (iFrameCount < 0)
   {
    timestr[0] = '\0';
    return;
   }

  tcLocal = new CTimecode (*this);
  tcLocal->setFrameCount (iFrameCount);
  tcLocal->getTimeASCII (timestr);

  delete tcLocal;
}

void CTimecode::getTimeString(string &timestr) const
{
   char timechr[16];
   getTimeASCII(timechr);
   timestr = timechr;
}

int CTimecode::getHours() const
{
   if (framesPerSecond==0) return(0);
   return(frames/(3600*framesPerSecond));
}

int CTimecode::getMinutes() const
{
   if (framesPerSecond==0) return(0);
   return((frames/(60*framesPerSecond))%60);
}

int CTimecode::getSeconds() const
{
   if (framesPerSecond==0) return(0);
   return((frames/framesPerSecond)%60);
}

int CTimecode::getFrames() const
{
   if (framesPerSecond==0) return(frames);
   return(frames%framesPerSecond);
}

int CTimecode::getFlags() const
{
   return(flags);
}

bool CTimecode::isDropFrame() const
{
   return (flags & DROPFRAME);
}

bool CTimecode::isHalfFrame() const
{
   return (flags & HALFFRAME);
}

bool CTimecode::is720P() const
{
   return (flags & FRAME720P);
}

bool CTimecode::getField() const
{
   // Returns false for field 1 or don't care
   // Returns true for field 2
   
   return (flags & TC_FIELDFLAG);
}

int CTimecode::getFramesPerSecond() const
{
   return(framesPerSecond);
}

ostream& operator<<(ostream& os, const CTimecode& tc)
{
   char ostr[14];

   tc.getTimeASCII(ostr);
   os << ostr;
   return os;
}
#ifdef _WIN64
MTIostringstream& operator<<(MTIostringstream& os, const CTimecode& tc)
{
   char ostr[14];

   tc.getTimeASCII(ostr);
   os << ostr;
   return os;
}
#endif

//////////////////////////////////////////////////////////////
// convertTimecode
//     creates an equivalent CTimecode with another frame rate.
//     Hours, Minutes and Seconds remain the same, but frame digits
//     are converted for new frame rate.
//     Useful for converting between 24, 25 and 30 frames per second timecodes
//
/////////////////////////////////////////////////////////////
CTimecode CTimecode::convertTimecode(int newFramesPerSecond, bool newDropFrame)
{
   if (newFramesPerSecond == 0)
      {
      // New timecode is a frame counter
      return CTimecode(getAbsoluteTime());
      }
   else if (framesPerSecond == 0)
      {
      // This timecode is a frame counter
      CTimecode newTimecode(0, newFramesPerSecond);
      newTimecode.setDropFrame(newDropFrame);
      newTimecode.setAbsoluteTime(getAbsoluteTime());
      return newTimecode;
      }
   else
      {
      // Both this and new timecode are regular HH:MM:SS:FF timecodes,
      // so adjust frames digits by the ratio of the this timecode's
      // frame rate and caller's requested frame rate.  Hours, minutes
      // and seconds remain the same
      double frameRateRatio = (double)newFramesPerSecond/(double)framesPerSecond;
      int newFrames = (int)(getFrames() * frameRateRatio + .5);
      int newFlags = (newDropFrame) ? DROPFRAME : 0;
      return CTimecode(getHours(), getMinutes(), getSeconds(), newFrames,
                       newFlags, newFramesPerSecond);
      }
}

//////////////////////////////////////////////////////////////
//
//		v a l i d a t e T i m e c o d e
//
//////////////////////////////////////////////////////////////
// Validate the time code. This means adjusting it
// upward if drop-frame is enabled and the values
// are not valid drop-frame timecodes.
void CTimecode::validateTimecode()
{
   if (framesPerSecond==0) { // FFFFFFFFFFF

      // enforce maximum

      if (frames > MAXFRAMES) {
         frames = MAXFRAMES;
      }
   }
   else { // HH:MM:SS:FF

      unsigned maxfrms = 100*3600*framesPerSecond - 1;
      if (frames > maxfrms) frames = maxfrms;
   
      if ((flags&DROPFRAME)!=0)
      {
         // n:0:0 and n:0:1 are verboten, except where n%10 = 0
         if ((frames%(60*framesPerSecond)==0)&&(frames%(600*framesPerSecond)!=0)) frames+=2;
         if ((frames%(60*framesPerSecond)==1)&&(frames%(600*framesPerSecond)!=1)) frames+=1;
      }
   }
}

//////////////////////////////////////////////////////////////
//
//	d i f f e r e n c e O f T i m e C o d e s
//
//////////////////////////////////////////////////////////////
// Calculates the difference, in frames, of two timecodes
int operator-(const CTimecode& dstTimecode, const CTimecode& srcTimecode)
{
   // timecodes must be commensurable
   if ((srcTimecode.framesPerSecond!=dstTimecode.framesPerSecond)||
       ((srcTimecode.flags & TC_FLAG_MASK)!=(dstTimecode.flags & TC_FLAG_MASK)))
      return(0);

   int src = srcTimecode.frames;
   int dst = dstTimecode.frames;
   int fdelt;
   if ((fdelt=(dst-src))==0) return(0);

   if (((srcTimecode.flags)&DROPFRAME)!=0) {

      int mM = 60*srcTimecode.framesPerSecond;
      int tM = 10*mM;

      int k;
      if (fdelt>0)
      { 
         fdelt += dst/tM - dst/mM;
         fdelt -= src/tM - src/mM;
         if ((k=(dst-1))>=0) fdelt += k/tM - k/mM;
         if ((k=(src-1))>=0) fdelt -= k/tM - k/mM;
      }
      else
      {
         if ((k=(src-1))>=0) fdelt -= k/tM - k/mM;
         if ((k=(dst-1))>=0) fdelt += k/tM - k/mM;
         if ((k=(src-2))>=0) fdelt -= k/tM - k/mM;
         if ((k=(dst-2))>=0) fdelt += k/tM - k/mM; 
      }
   }
   return(fdelt);
}

//////////////////////////////////////////////////////////////
//
//		i n c r e m e n t T i m e C o d e
//
//////////////////////////////////////////////////////////////
// Increments a time code by one.
void CTimecode::incrementTimecode()
{
   frames++;
   validateTimecode();
}

// prefix autoincrement
const CTimecode& operator++(CTimecode& tc)
{
   tc.incrementTimecode();
   return(tc);
}
// postfix autoincrement
const CTimecode operator++(CTimecode& tc, int)
{
   CTimecode before = tc;
   tc.incrementTimecode(); 
   return(before);
}
//////////////////////////////////////////////////////////////
//
//		d e c r e m e n t T i m e C o d e
//
//////////////////////////////////////////////////////////////
// Decrements a time code by one.
void CTimecode::decrementTimecode()
{
   frames--;
   validateTimecode();
}

// prefix autodecrement
const CTimecode& operator--(CTimecode& tc)
{
   tc.decrementTimecode();
   return(tc);
}
// postfix autodecrement
const CTimecode operator--(CTimecode& tc, int)
{
   CTimecode before = tc;
   tc.decrementTimecode(); 
   return(before);
}

//////////////////////////////////////////////////////////////
//
//	t i m e C o d e P l u s N F r a m e s
//
// Returns the time code which is N frames from the given
// timecode. The argument may be positive or negative.
//
// Algorithm:
//   In the drop-frame case, when we apply a displacement
//   to a base time code, we must stretch that displacement
//   to account for the timecodes which are not allowed.
//   The stretch factor is calculated on the basis of
//   frames-per-second, so that timecodes of the form
//   n:0:0 and n:0:1 are excluded (except where n is a
//   multiple of 10). The stretch factor gives us an "analog"
//   estimate of the desired result timecode, which is then
//   adjusted iteratively to be exact. The number of iterations
//   needed is never more than two, which allows the routine
//   to execute in 4 us. The iteration calculation mimics that
//   for the difference of two timecodes (as above).
//
//////////////////////////////////////////////////////////////
CTimecode operator+(const CTimecode& srcTimecode, int framesAway)
{
   int fdst;

   if (framesAway==0) return(srcTimecode);

   int mS = srcTimecode.framesPerSecond;
   int mM = 60*mS;
   int tM = 10*mM;

   // the course timecode expressed in frames
   int src = srcTimecode.frames;

   // the tentative destination timecode expressed in frames
   int dst = src + framesAway;

   if (((srcTimecode.flags)&DROPFRAME)!=0)
   { // make adjustments

      // calculate stretch factor   
      double strfact = 1.0/(1.0-2.0*(1.0/((double)mM)-1.0/((double)tM))) - 1.0;

      int fdelt = 0;
      int k;
      if (framesAway>0)
      {
         // adjust for stretch factor
         dst += (int)(strfact*(double)framesAway);
         // make final adjustment iteratively
         for (;;)
         {
            fdelt = dst - src;
            fdelt+= dst/tM - dst/mM;
            fdelt-= src/tM - src/mM;
            if ((k=(dst-1))>=0) fdelt += k/tM - k/mM;
            if ((k=(src-1))>=0) fdelt -= k/tM - k/mM;
            if (fdelt==framesAway) {
	       fdst = dst%(600*mS);
               if ((fdst==0)||(fdst==1)) goto done;
               fdst = dst%(60*mS);
               if ((fdst==0)||(fdst==1)) dst--;
               else goto done;
            }  
            else fdelt>framesAway?dst--:dst++;   
         }
      }
      else
      {
         // adjust for stretch factor
         dst += (int)(strfact*(double)framesAway);
         // make final adjustment iteratively
         for (;;)
         {
            fdelt = dst - src;
            if ((k=(src-1))>=0) fdelt -= k/tM - k/mM;
            if ((k=(dst-1))>=0) fdelt += k/tM - k/mM;
            if ((k=(src-2))>=0) fdelt -= k/tM - k/mM;
            if ((k=(dst-2))>=0) fdelt += k/tM - k/mM;
            if (fdelt==framesAway) {
               fdst = dst%(600*mS);
               if ((fdst==0)||(fdst==1)) goto done;
               fdst = dst%(60*mS);
               if ((fdst==0)||(fdst==1)) dst++;
               else goto done;
            }
            else fdelt<framesAway?dst++:dst--;
         }
      }
   }
 
done:;

   CTimecode dstTime(srcTimecode.flags,srcTimecode.framesPerSecond);
   dstTime.frames = dst;

   return(dstTime);
}
//////////////////////////////////////////////////////////////
//
//		e q u i v a l e n c e
//
//////////////////////////////////////////////////////////////
bool operator==(const CTimecode& tl, const CTimecode& tr)
{
   if ((tl.flags==tr.flags)&&
       (tl.framesPerSecond==tr.framesPerSecond)&&
       (tl.frames==tr.frames))
      return(true);

   return(false);
}

//////////////////////////////////////////////////////////////
//
//		n o n  - e q u i v a l e n c e
//
//////////////////////////////////////////////////////////////
bool operator!=(const CTimecode& tl, const CTimecode& tr)
{
   return (!(tl==tr));
}

//////////////////////////////////////////////////////////////
//
//		greater-than operator
//    greater-than or equal-to operator
//
//////////////////////////////////////////////////////////////
bool CTimecode::greaterThan(const CTimecode &rhs) const
{
   // Ignore Half-Frame and Field flags
   if ((flags & TC_FLAG_MASK) == (rhs.flags & TC_FLAG_MASK)
       && framesPerSecond == rhs.framesPerSecond
       && frames > rhs.frames) {
      return(true);
   }

   return(false);
}

bool CTimecode::equalTo(const CTimecode &rhs) const
{
   // Ignore Half-Frame and Field flags
   if ((flags & TC_FLAG_MASK) == (rhs.flags & TC_FLAG_MASK)
       && framesPerSecond == rhs.framesPerSecond
       && frames == rhs.frames) {
      return(true);
   }

   return(false);
}

bool operator>(const CTimecode& tl, const CTimecode& tr)
{
   // Ignore Half-Frame flag
   return (tl.greaterThan(tr));
}

bool operator>=(const CTimecode& tl, const CTimecode& tr)
{
   // Ignore Half-Frame flag
   return (tl.greaterThan(tr) || tl.equalTo(tr));
}

//////////////////////////////////////////////////////////////
//
//		less-than operator
//    less-than or equal-to operator
//
//////////////////////////////////////////////////////////////
bool CTimecode::lessThan(const CTimecode &rhs) const
{
   // Ignore Half-Frame and Field flags
   if ((flags & TC_FLAG_MASK) == (rhs.flags & TC_FLAG_MASK)
       && framesPerSecond == rhs.framesPerSecond
       && frames < rhs.frames) {
      return(true);
   }

   return(false);
}

bool operator<(const CTimecode& tl, const CTimecode& tr)
{
   // Ignore Half-Frame flag
   return (tl.lessThan(tr));
}

bool operator<=(const CTimecode& tl, const CTimecode& tr)
{
   // Ignore Half-Frame flag
   return (tl.lessThan(tr) || tl.equalTo(tr));
}

//////////////////////////////////////////////////////////////
//
//		T E S T   S U I T E
//
//////////////////////////////////////////////////////////////
void TestTimecode()
{
#ifdef GEEK
   cout << "ENTER DROP FRAME (0 for N, 1 for Y)" << endl;
   int df;
   do {
      cin >> df;
   } while ((df!=0)&&(df!=1));

   cout << "ENTER FRAMES PER SECOND (0,24,25,30,50,60)" << endl;
   int fps;
   do {
      cin >> fps;
   } while ((fps!=0)&&(fps!=24)&&(fps!=25)&&(fps!=30)&&(fps!=50)&&(fps!=60));

   CTimecode T0(df,fps);
   cout << "ENTER BEG TIMEVAL HH:MM:SS:FF" << endl;
   char bstr[32];
   do {  
      cin >> bstr;
   }
   while (!T0.setTimeASCII(bstr));

   cout << "  T0 = " << T0 << endl;
   ++T0;
   cout << "++T0 = " << T0 << endl;
   T0++;
   cout << "T0++ = " << T0 << endl;
   --T0;
   cout << "--T0 = " << T0 << endl;
   T0--;
   cout << "T0-- = " << T0 << endl;

   CTimecode T1(df,fps);

   cout << "ENTER END TIMEVAL HH:MM:SS:FF" << endl;
   char estr[32];
   do {
      cin >> estr;
   }
   while (!T1.setTimeASCII(estr));

   int del = T1 - T0;

   cout << "DELTA FRAMES = " << del << endl;

   cout << "ENTER LOOP COUNT" << endl;

   int loop;
   cin >> loop;

   CTimecode T2(df,fps);

   for (int i=0; i< loop; i++) {
   T2 = T0 + del;
   }

   cout << "BEG TIMEVAL + DELTA FRAMES = " << T2 << endl;

   if (T1==T2) cout << "VALUE IS CORRECT" << endl;
   else cout << "VALUE IS INCORRECT" << endl;
#endif

#ifdef GEEK
   ///////////////////////////////////////////////////////////////

   cout << "ENTER DROP FRAME (0 for N, 1 for Y)" << endl;
   int df;
   do {
      cin >> df;
   } while ((df!=0)&&(df!=1));

   cout << "ENTER FRAMES PER SECOND (24,25,30,50,60)" << endl;
   int fps;
   do {
      cin >> fps;
   } while ((fps!=24)&&(fps!=25)&&(fps!=30)&&(fps!=50)&&(fps!=60));

   char bstr[32];
   CTimecode T0(df,fps);
   char estr[32];
   CTimecode T1(df,fps);

   // a DURATION is a timecode with NO DROP-FRAME
   char dstr[32];
   CTimecode DUR(false,fps);

   int del;

   ///////////////////////////////////////////////////////////////
   // specify an IN and an OUT and get a DURATION

   cout << "ENTER BEG TIMEVAL HH:MM:SS:FF" << endl;
   do {
      cin >> bstr;
   }
   while (!T0.setTimeASCII(bstr));

   cout << "ENTER END TIMEVAL HH:MM:SS:FF" << endl;
   do {
      cin >> estr;
   }
   while (!T1.setTimeASCII(estr));

   // get absolute times in frames
   int t0 = T0.getTime();
   int t1 = T1.getTime();

   // the duration in frames
   if ((del = t1 - t0) < 0) {
     del = 0;
     T1 = T0; // force equal
   }

   // convert del to a DURATION timecode
   DUR.setTime(del);

   // output the three values

   cout << "T0 " << T0 << endl;

   cout << "T1 " << T1 << endl;

   cout << "DUR " << DUR << endl;

   /////////////////////////////////////////////////////////////////
   // specify an IN and a DURATION and get an OUT
  
   cout << "ENTER BEG TIMEVAL HH:MM:SS:FF" << endl;
   do {
      cin >> bstr;
   }
   while (!T0.setTimeASCII(bstr));

   cout << "ENTER DURATION HH:MM:SS:FF" << endl;
   do {
      cin >> dstr;
   }
   while (!DUR.setTimeASCII(dstr));

   // derive the end timecode

   t0 = T0.getTime();

   del = DUR.getTime();

   t1 = t0 + del;

   T1.setTime(t1);

   // because this may get adjusted for drop-frame
   // legality, re-calculate the duration

   t1 = T1.getTime();

   del = t1 - t0;

   DUR.setTime(del);

   // output the three values

   cout << "T0 " << T0 << endl;

   cout << "T1 " << T1 << endl;

   cout << "DUR " << DUR << endl;

   /////////////////////////////////////////////////////////////////
   // specify an OUT and a DURATION and get an IN

   cout << "ENTER END TIMEVAL HH:MM:SS:FF" << endl;
   do {
      cin >> estr;
   }
   while (!T1.setTimeASCII(estr));

   cout << "ENTER DURATION HH:MM:SS:FF" << endl;
   do {
      cin >> dstr;
   }
   while (!DUR.setTimeASCII(dstr));

   // derive the beg timecode

   t1 = T1.getTime();

   del = DUR.getTime();

   t0 = t1 - del;
   if (t0 < 0) {
      t0 = 0;
      del = t1 - t0;
   }

   T0.setTime(t0);

   // because this may get adjusted for drop-frame
   // legality, re-calculate the duration

   t0 = T0.getTime();

   del = t1 - t0;

   DUR.setTime(del);

   // output the three values

   cout << "T0 " << T0 << endl;

   cout << "T1 " << T1 << endl;

   cout << "DUR " << DUR << endl;
#endif

#ifdef GEEK
   cout << "ENTER DROP FRAME (0 for N, 1 for Y)" << endl;
   int df;
   do {
      cin >> df;
   } while ((df!=0)&&(df!=1));

   cout << "ENTER FRAMES PER SECOND (0,24,25,30,50,60)" << endl;
   int fps;
   do {
      cin >> fps;
   } while ((fps!=0)&&(fps!=24)&&(fps!=25)&&(fps!=30)&&(fps!=50)&&(fps!=60));

   CTimecode T0(df,fps);
   if (fps==0) {
      cout << "ENTER BEG TIMEVAL nnnnnnnn" << endl;
   }
   else {
      cout << "ENTER BEG TIMEVAL HH:MM:SS:FF" << endl;
   }
   char bstr[32];
   do {
      cin >> bstr;
   }
   while (!T0.setTimeASCII(bstr));

   cout << T0 << endl;
   cout << "TIME " << T0.getAbsoluteTime()    << endl;
   cout << "HRS  " << T0.getHours()   << endl;
   cout << "MINS " << T0.getMinutes() << endl;
   cout << "SECS " << T0.getSeconds() << endl;
   cout << "FRMS " << T0.getFrames()  << endl;

   // this juggles it to a different type

   T0.setFramesPerSecond(0);

   cout << T0 << endl;
   cout << "TIME " << T0.getAbsoluteTime()    << endl;
   cout << "HRS  " << T0.getHours()   << endl;
   cout << "MINS " << T0.getMinutes() << endl;
   cout << "SECS " << T0.getSeconds() << endl;
   cout << "FRMS " << T0.getFrames()  << endl;

   T0.setFramesPerSecond(fps);

   T0.setHours(99);
   cout << T0 << endl;

   T0.setMinutes(59);
   cout << T0 << endl;

   T0.setSeconds(59);
   cout << T0 << endl;

   T0.setFrames(fps-1);
   cout << T0 << endl;
#endif


   cout << "ENTER DROP FRAME (0 for N, 1 for Y)" << endl;
   int df;
   do {
	  std::cin >> df;
   } while ((df!=0)&&(df!=1));

   cout << "ENTER FRAMES PER SECOND (0,24,25,30,50,60)" << endl;
   int fps;
   do {
	  std::cin >> fps;
   } while ((fps!=0)&&(fps!=24)&&(fps!=25)&&(fps!=30)&&(fps!=50)&&(fps!=60));

   CTimecode T0(df|FRAME720P,fps);

   if (fps==0) {
      cout << "ENTER BEG TIMEVAL nnnnnnnn" << endl;
   }
   else {
      cout << "ENTER BEG TIMEVAL HH:MM:SS:FF" << endl;
   }
   char bstr[32];
   do {
      std::cin >> bstr;
   }
   while (!T0.setTimeASCII(bstr));

   T0++;
   cout << T0 << endl;
   T0++;
   cout << T0 << endl;
   T0--;
   cout << T0 << endl;
   T0--;
   cout << T0 << endl;

   T0 = T0 + 40;
   cout << T0 << endl;
	
}

