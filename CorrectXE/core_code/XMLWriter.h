//////////////////////////////////////////////////////////////////////
//
// XMLWriter.h: interface & implementation for the XMLStream class.
// 
//////////////////////////////////////////////////////////////////////

#ifndef XMLWRITERH
#define XMLWRITERH

#include "corecodelibint.h"
#include "MTIstringstream.h"

#ifdef _MSC_VER
#include "MTIio.h"
#include "machine.h"
#endif

#include <stack>
#include <string>
//#include <sstream>
#include <assert.h>

using std::string;

////////////////////////////////////////////////////////////////////////////

class MTI_CORECODELIB_API CXMLStream
{
public:

   // Internal helper class
   struct SController
   {
      enum what_type
        {
        whatProlog, whatTag, whatTagEnd, whatAttribute,
        whatCharData, whatEscape
        };

      what_type what;
      std::string str;

      inline SController(const SController& c) : what(c.what), str(c.str) {}
      inline SController(const what_type _what) : what(_what){}

      // use template constructor because string field <str> may be initialized
      // from different sources: char*, std::string etc
      template<class T> inline SController(const what_type _what, const T& _str)
         : what(_what), str(_str) {}
   };

   // XMLStream refers std::ostream object to perform actual output operations
   CXMLStream(std::ostream& _s);

   // Before destroying check whether all the open tags are closed
   ~CXMLStream();

   // default behavior - delegate object output to std::ostream
   template<class T> CXMLStream& operator<<(const T& value)
   {
      if (stateTagName == state)
         tagName << value;
      s << value;
      return *this;
   };

   CXMLStream& operator<<(const SController& controller);

private:
   // state of the stream
   enum state_type
      {
      stateNone, stateTag, stateAttribute, stateTagName
      };

   // tag name stack
   typedef std::stack<std::string> tag_stack_type;

   tag_stack_type tags;
   state_type state;
   std::ostream &s;
   bool prologWritten;
   MTIostringstream tagName;

   // I don't know any way easier (legal) to clear std::stringstream...
   inline void clearTagName()
   {
      const std::string empty_str;
      tagName.str(empty_str);
   }

   // Close current tag
   void closeTagStart(bool self_closed = false);

   // Close tag (may be with closing all of its children)
   void endTag(const std::string& tag);

#ifdef _MSC_VER
   // Holy crap this is ugly!  See
   // http://www.codeproject.com/cpp/MemberTemplateDLL.asp for
   // justification. -mlm

   // This method exists only to force the compiler to instantiate
   // versions of our member template.  It should never be called.
   void doNotCall()
      {
      //  We were serious about that "doNotCall" thing.
      assert(0);

      int i = 0;
      operator<<(i);

      unsigned int ui = 0;
      operator<<(ui);

      MTI_INT64 i64 = 0;
      operator<<(i64);

      MTI_UINT64 ui64 = 0;
      operator<<(ui64);

      string s = "";
      operator<<(s);

      float f = 0.0;
      operator<<(f);

      double d = 0.0;
      operator<<(d);

      bool b = true;
      operator<<(b);
      }
#endif
}; // class CXMLStream

////////////////////////////////////////////////////////////////////////////

// Helper functions, they may be simply overwritten

inline const CXMLStream::SController prolog()
{
   return CXMLStream::SController(CXMLStream::SController::whatProlog);
}

inline const CXMLStream::SController tag()
{
   return CXMLStream::SController(CXMLStream::SController::whatTag);
}

inline const CXMLStream::SController tag(const string& tag_name)
{
   return CXMLStream::SController(CXMLStream::SController::whatTag, tag_name);
}

inline const CXMLStream::SController endTag()
{
   return CXMLStream::SController(CXMLStream::SController::whatTagEnd);
}

inline const CXMLStream::SController endTag(const string& tag_name)
{
   return CXMLStream::SController(CXMLStream::SController::whatTagEnd, tag_name);
}

inline const CXMLStream::SController attr(const string& attr_name)
{
   return CXMLStream::SController(CXMLStream::SController::whatAttribute, attr_name);
}

inline const CXMLStream::SController chardata(const string& chardata_string)
{
   return CXMLStream::SController(CXMLStream::SController::whatCharData, chardata_string);
}

inline const CXMLStream::SController escape(const string& value)
{
   return CXMLStream::SController(CXMLStream::SController::whatEscape, value);
}

////////////////////////////////////////////////////////////////////////////

#endif  //  XMLWRITER_H
