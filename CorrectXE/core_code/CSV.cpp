#include "CSV.h"

// -------------------------------------------------------------------------

namespace
{
    string makeCsvTokenFromString(const string &rawString)
    {
	string csvToken;
	bool hasCommas = rawString.find(',') != string::npos;
	bool hasDoubleQuotes = rawString.find('"') != string::npos;

	if (hasCommas || hasDoubleQuotes)
	{
	    // Quote the whole token.
	    csvToken += '"';
	}

	if (hasDoubleQuotes)
	{
	    // For simplicity when there is an embedded quote we look at
	    // each character.
	    for (unsigned int i = 0; i < rawString.length(); ++i)
	    {
		if (rawString[i] == '"')
		{
		    // Double the double quotes.
		    csvToken += '"';
		}

		csvToken += rawString[i];
	    }
	}
	else
	{
	    // No embedded double quotes - blast out the whole string.
	    csvToken += rawString;
	}

	if (hasCommas || hasDoubleQuotes)
	{
	    // Close the quotation.
	    csvToken += '"';
	}

	return csvToken;
    }

};

// Takes the CSV line and parses it into the individual fields.
// NOTE!! THis does NOT handle newlines embedded in fields!!
void CsvReader::parseCsvRow(const string& line, /* OUTPUT */ stringlist &row)
{
    row.clear();
    bool inQuote = false;
    string field;
    string::const_iterator lineIter = line.begin();
    for (lineIter = line.begin(); lineIter != line.end(); ++lineIter)
    {
	switch (*lineIter)
	{
	case '"':
	    // Peek at next char to see if we have double double quotes.
	    if (lineIter < (line.end() - 1) && *(lineIter + 1) == '"')
	    {
		// Yes - output one double quote; don't change quote state.
		field += '"';
		++lineIter;
	    }
	    else
	    {
		inQuote = !inQuote;
	    }
	    break;

	case ',':
	    if (inQuote)
	    {
		field += ',';
	    }
	    else
	    {
		row.push_back(field);
		field.clear();
	    }
	    break;

	default:
	    field += *lineIter;
	    break;
	}
    }

    row.push_back(field);
}

// -------------------------------------------------------------------------

CsvReader::CsvReader(const string &csvFilePath)
{
    this->fileStream = new std::ifstream(csvFilePath, std::ifstream::in);
}
// -------------------------------------------------------------------------

CsvReader::~CsvReader()
{
    delete this->fileStream;
}
// -------------------------------------------------------------------------

int CsvReader::GetRow(stringlist &row)
{
    if (this->fileStream == NULL)
    {
	return CSV_FILE_IO_FAIL;
    }

    if (this->fileStream->peek() == std::char_traits<char>::eof())
    {
	return CSV_FILE_EOF;
    }

    std::string fileLine;
    std::getline((*this->fileStream), fileLine);
    parseCsvRow(fileLine, row);

    return CSV_SUCCESS;
}
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------

CsvWriter::CsvWriter(const string &csvFilePath)
{
    this->fileStream = new std::ofstream(csvFilePath, std::ofstream::out);
}
// -------------------------------------------------------------------------

CsvWriter::~CsvWriter()
{
    delete this->fileStream;
}
// -------------------------------------------------------------------------

int CsvWriter::PutRow(const stringlist &row)
{
    if (this->fileStream == NULL)
    {
	return CSV_FILE_IO_FAIL;
    }

    bool firstToken = true;
    for (int i = 0; i < row.size(); ++i)
    {
	if (firstToken)
	{
	    firstToken = false;
	}
	else
	{
	    (*this->fileStream) << ',';
	}

	(*this->fileStream) << makeCsvTokenFromString(row[i]);
    }

    (*this->fileStream) << std::endl;

    return CSV_SUCCESS;
}
// -------------------------------------------------------------------------
