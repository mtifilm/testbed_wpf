/*
    File Name:  HRTimer.cpp
    Create:  March 2, 2000
    Release 1.00

    This provides a High Resolution Timer for windows.  The accuracy
    depends upon the machine but for windows is better than 100
    microseconds and for sgi and linux on intel the same.

                                        -John Mertus@Brown.EDU
*/

//---------------------------------------------------------------------------
#include "HRTimer.h" 
#include <time.h>

#ifndef _MSC_VER
#include <algorithm>
using std::min;
using std::max;
#endif

#ifdef _WINDOWS
#include <windows.h>
#include <dos.h>
#include "SharedMem.h"
#else
#include <sys/time.h>
#include <unistd.h>
#endif

//#include "MTIstringstream.h"

//---------------------------------------------------------------------------


 /*-----------------------Constructor----------------JAM--Mar 2000----*/

  CHRTimer::CHRTimer()

 /* This just finds if the hardware supports the timer and finds the  */
 /* clock freq                                                        */
 /*                                                                   */
 /*********************************************************************/
{
#ifdef _WINDOWS
   LARGE_INTEGER l;
   QueryPerformanceFrequency(&l);
   m_dClockFreq = l.QuadPart;

   wTimerRes = -1;   // initialize millisecond resolution
   hWaitableTimer = NULL;  // handle to waitable timer

   hWaitableTimer = CreateWaitableTimer (NULL, TRUE, "WaitableHRTimer");
   if (hWaitableTimer != NULL)
    {
     // get the capabilities of this computer
     TIMECAPS tc;
     if (timeGetDevCaps (&tc, sizeof(TIMECAPS)) == TIMERR_NOERROR)
      {
       #define TARGET_RESOLUTION 1   // attempt to use a 1 millisec timer
       UINT wTmp;
       wTmp = min (max (tc.wPeriodMin, (UINT)TARGET_RESOLUTION), tc.wPeriodMax);

       // set the resolution of the timer
       if (timeBeginPeriod (wTmp) == TIMERR_NOERROR)
        {
         // successfully set the multi media timer
         wTimerRes = wTmp;
        }
      }
    }
   
#else
   m_dClockFreq = 1000000;    // Noninal clock
#endif

   Start();
}

CHRTimer::~CHRTimer ()
{
#ifdef _WINDOWS
  // destroy the Waitable timer
  CloseHandle (hWaitableTimer);
  hWaitableTimer = NULL;
#endif
}

 /*-----------------------Start-----------------------JAM--Mar 2000---*/

     void CHRTimer::Start()

 /* Call this to zero the timer.  This just gets the count value      */
 /* and stores it for later.                                          */
 /*                                                                   */
 /*********************************************************************/
{
#ifdef _WINDOWS
   LARGE_INTEGER l;
   QueryPerformanceCounter(&l);
   m_dStartCount = l.QuadPart;
   m_intervalStart = Read();
#else
   struct timeval tv;
   struct timezone tz;
   gettimeofday(&tv, &tz);
   m_dStartCount = (double)tv.tv_sec*1000000.0 + tv.tv_usec;
#endif
}

/*-------------------------Read--------------------JAM--Mar 2000----*/

   double CHRTimer::Read(bool reset)

/* This returns the current count in milliseconds.  StartTimer must  */
/* be called first                                                   */
/*                                                                   */
/*********************************************************************/
{
#ifdef _WINDOWS
  LARGE_INTEGER llCurrentCount;
  QueryPerformanceCounter(&llCurrentCount);
  double dCurrentCount = double(llCurrentCount.QuadPart) - m_dStartCount;
  if (reset)
     m_dStartCount = llCurrentCount.QuadPart;
#else
  struct timeval tv;
  struct timezone tz;
  gettimeofday(&tv, &tz);
  double dCurrentCount = (double)tv.tv_sec*1000000.0 + tv.tv_usec - m_dStartCount;
  if (reset)
     m_dStartCount = (double)tv.tv_sec*1000000.0 + tv.tv_usec;
#endif
  return(1000.0 * dCurrentCount / m_dClockFreq);
}



/*-----------------------ReadAsString----------------JAM--Mar 2000----*/

  string CHRTimer::ReadAsString()

/* This returns the current count in milliseconds.  StartTimer must  */
/* be called first                                                   */
/*                                                                   */
/*********************************************************************/
{
  MTIostringstream os;
  os << Read();
  return(os.str());
}

/*-----------------------Delay----------------JAM--Mar 2000----*/

  void CHRTimer::Delay(float fDelay)

//  Delays at least fDelay milliseconds.
//  Delay can be fractional, up to the accuracy of the time
//
//********************************************************************
{
  double st = Read();
  while ((Read() - st) < fDelay);
}

/*-----------------------Sleep----------------JAM--Mar 2000----*/

  void CHRTimer::Sleep(int iDelay)

//
//  This function uses the Multi Media timer to get close to the
//  desired delay.  
//
//********************************************************************
{
  int iRet = -1;

#ifdef _WINDOWS
  if (hWaitableTimer)
   {
    // the MultiMedia timer has resolution of wTimeRes.  Set the timer
    // to expire in iDelay milliseconds
    if ((int)wTimerRes != -1)
     {
      LARGE_INTEGER liDueTime;
      liDueTime.QuadPart = -iDelay * 10000;   // convert to 100 nanosec.
		// negative means relative wait.

      if (SetWaitableTimer (hWaitableTimer, &liDueTime, 0, NULL, NULL, 0))
       {
        // success, wait for event
        if (WaitForSingleObject (hWaitableTimer, INFINITE) == WAIT_OBJECT_0)
         {
          // success
          iRet = 0;
         }
       }
     }
   }
#endif

  if (iRet)
   {
    // something went wrong, no choice but to use SleepMS()
    SleepMS (iDelay);
   }
}

/*-----------------------DelayNS--------------JAM--Mar 2000----*/

  void CHRTimer::DelayNS(float fDelay)

//  Delays at least fDelay milliseconds.  -- NON-SPINNING version
//  Delay can be fractional, up to the accuracy of the time
//
//  This function uses the Multi Media timer to get close to the
//  desired delay.  Then is spins while waiting for the final few
//  micro seconds.
//
//********************************************************************
{
  double st = Read();


#ifdef _WINDOWS
  if (hWaitableTimer)
   {
    // the MultiMedia timer has resolution of wTimeRes.  Set the timer
    // to expire in (fDelay - wTimerRes) milliseconds
    int iNonSpinningMS = (int)fDelay - wTimerRes;
    if ((int)wTimerRes != -1 && iNonSpinningMS > 0)
     {
      LARGE_INTEGER liDueTime;
      liDueTime.QuadPart = -iNonSpinningMS * 10000;   // convert to 100 nanosec.
		// negative means relative wait.

      if (SetWaitableTimer (hWaitableTimer, &liDueTime, 0, NULL, NULL, 0))
       {
        // success, wait for event
        WaitForSingleObject (hWaitableTimer, INFINITE);
       }
     }
   }
#else
  // in unix, use SleepMS() which does not spin
  int iNonSpinningMS = (int)fDelay - 1;
  SleepMS (iNonSpinningMS);
#endif

  // spin the CPU waiting for remaining time
  while ((Read() - st) < fDelay);
}

//---------------------SleepMS--------------------John Mertus----Feb 2002---*/

  void SleepMS(int ms)

//   This rouitine sleeps for ms Millisecond.  Unlike delay, which does
//  not release the cpu, this does.
//
//****************************************************************************
{
#ifdef _WINDOWS
  Sleep(ms);
#else
  struct timeval tv;
  tv.tv_sec = ms /1000;
  tv.tv_usec = (ms % 1000)*1000;
  select(0, NULL, NULL, NULL, &tv);
#endif
}

#ifdef _WINDOWS
static MTI_INT64 *Fudge = NULL;
static TSharedMem MTITimerGlobal("MTITimerSharedMemGlobal", sizeof(MTI_INT64));
#endif

//------------WallClockTimeStamp------------John Mertus----Feb 2003---

  string WallClockStamp(enHRT Resolution)

//  Returns a string that represents wallclock time (time of day)
//  accurate to milliseconds
//
//   On a 1 ghz machine, about 45 requests could be done in a millisecond
// DO NO USE TRACE IN THIS MESSAGE
//
//********************************************************************
{
   MTIostringstream os;

#ifdef _WINDOWS
#define INVTIME 1000000
   LARGE_INTEGER l;
   MTI_INT64 Freq;
   QueryPerformanceFrequency((LARGE_INTEGER *)&Freq);

   QueryPerformanceCounter(&l);
   //  First time through the routine
   //  Set a pointer to the global local time,
   //  If first time, set the global time
   if (Fudge == NULL)
    {
       // Our fudge factor is in the global
       Fudge = (MTI_INT64 *)MTITimerGlobal.Buffer();
       // This is true if this is the first time.
//       if (MTITimerGlobal.Created())
//      if (*Fudge == 0)
         {
            LARGE_INTEGER timeQ;
            SYSTEMTIME st;
            GetLocalTime(&st);
            SystemTimeToFileTime(&st,(FILETIME *)&timeQ);
            // timeQ.QuadPart/10 = 1 microsecond)
            *Fudge = timeQ.QuadPart/(10000000/INVTIME) -(l.QuadPart*INVTIME)/Freq;
         }
    }
   //  Now get highfreq time, adjust by local time
   //  Note (current time is 100 microsecond intervals + fudge) = (start time + duration in
   //  100 microsecond intervals
   MTI_INT64 time = (l.QuadPart*INVTIME)/Freq + *Fudge;
   MTI_INT64 tmp = INVTIME*60.*60*24;
   MTI_INT64 tmp2 = tmp/24;

   // Convert to ascii string
   int hour = (time % tmp)/tmp2;
   tmp = tmp2;
   tmp2 = tmp2/60;
   int min = (time % tmp)/tmp2;
   tmp = tmp2;
   tmp2 = tmp2/60;
   int sec = (time % tmp)/tmp2;
   tmp = tmp2;
   tmp2 = tmp2/1000;
   int msec = (time % tmp)/tmp2;
   tmp = tmp2;
   tmp2 = 1;
   int usec = (time % tmp)/tmp2;

   os << std::setw(2) << std::setfill('0') << hour << ":"
      << std::setw(2) << std::setfill('0') << min << ":"
      << std::setw(2) << std::setfill('0') << sec;

   if ((Resolution == enMILLISECONDS) || (Resolution == enMICROSECONDS))
      os << ":" << std::setw(3) << std::setfill('0') << msec;

   if (Resolution == enMICROSECONDS)
      os << "." << std::setw(3) << std::setfill('0') << usec;

#else
   struct timeval tv;
   time_t t;

   time(&t);
   string s = ctime(&t);
   gettimeofday(&tv, NULL);
   int msec  = tv.tv_usec/1000;
   os << s.substr(11,8) << ":"
	  << std::setw(3) << std::setfill('0') << msec;
#endif
   return os.str();

}

double CHRTimer::intervalMilliseconds()
{
	auto end = Read(false);
	auto start = m_intervalStart;
	m_intervalStart = end;

	return end - start;
}

double CHRTimer::elapsedMilliseconds()
{
	return Read(false);
}

std::ostream & operator<<(std::ostream & os, CHRTimer & hrt)
{
	os << std::fixed << std::setprecision(3) << hrt.intervalMilliseconds();
	return os;
}

