/*
 File Name:  IniFile.h
 Created: Augest 3, 2000 by John Mertus
 Version 1.00
 Version 2.00 Completely revised Aug 2001
 2.01 Changed case insensitive checking.
 2.02 Added Read Lists
 This is a rework of the DOS ini Library into C++
 with an interface almost exactly as Borland's interface

 */
#ifndef IniFileH
#define IniFileH
#include <string>
#include <vector>
#include <stdio.h>
#include <assert.h>
#include <iostream>
#include "machine.h"
#include "HRTimer.h"
#include "MTIio.h"
#include "MTIstringstream.h"
#include "TheError.h"
#include "ThreadLocker.h"
#include "MTIsleep.h"

#include "corecodelibint.h"
typedef vector<string>StringList;
typedef vector<int>IntegerList;
typedef vector<bool>BoolList;
typedef MTI_UINT32 ErrorTID;
typedef vector<ErrorTID>TIDList;
typedef vector<StringList>ListStringList;


MTI_CORECODELIB_API string CPMPGetDefaultLocalDir();

//
// Master default defines
#ifdef _WINDOWS
#define DEFAULT_LOCAL_DIR "DEFAULT_LOCAL_DIR obsolete; use CPMPGetDefaultLocalDir()"
#define DEFAULT_SHARED_DIR "C:\\MTIShare\\"
#else
#define DEFAULT_LOCAL_DIR "/mtiimages/MTISystem/"
#define DEFAULT_SHARED_DIR "/mtiimages/MTISystem/MTIShare/"
#endif

// These define where the files are found
//
#define DEFAULT_TIMING_FILE "$(CPMP_LOCAL_DIR)timing.cfg"
#define DEFAULT_DONGLE_FILE "$(CPMP_LOCAL_DIR)dongle.cfg"
#define DEFAULT_EMULATOR_FILE "$(CPMP_LOCAL_DIR)emulator.cfg"
#define DEFAULT_RAWDISK_FILE "$(CPMP_SHARED_DIR)rawdisk.cfg"
#define DEFAULT_LOCAL_MACHINE_FILE "$(CPMP_LOCAL_DIR)MTILocalMachine.ini"

//
// These are the default paths
#define DEFAULT_BIN_PATH "$(CPMP_SHARED_DIR)MTIBins"
#define DEFAULT_LOG_PATH "$(CPMP_USER_DIR)"
#define DEFAULT_CLIP_SCHEME_PATH "$(CPMP_LOCAL_DIR)ClipSchemes"
#define DEFAULT_CLIP_SCHEME_INI_FILE "ClipSchemes.ini"  // prepend CLIP_SCHEME_PATH
#define DEFAULT_USER_MANUAL_NAME "DRS�Nova User Guide.pdf"
#define DEFAULT_RELEASE_NOTES_NAME "DRS�Nova Release Notes.txt"
#ifdef _WINDOWS
#define DEFAULT_TEMP_PATH "$(TEMP)\\"
#else
#define DEFAULT_TEMP_PATH "/tmp/"
#endif
//
// Define the file headers in LOCAL MACHINE and MTIcpmp
#define VTR_SECTION "RemoteEdit"
#define GENERAL_SECTION "General"
#define CONFIG_FILE_SECTION "ConfigFiles"

// Define different styles of comment
#define COMMENT_CHARS_DEFAULT ";#%"
#define COMMENT_CHARS_NO_PERCENT ";#"

// Association.  These are used to associate a number in an ini file with both a
// #define or enum and symbolic name. Associations are really just arrays of strings whose index
// is the value of the #define or enums and not integers  E.g.
//
// enum EVideoProgressiveType
// {
// VI_VIDEO_PROGRESSIVE_TYPE_INVALID = 0,
// VI_VIDEO_PROGRESSIVE_TYPE_P = 1,
// VI_VIDEO_PROGRESSIVE_TYPE_SF = 2
// };
//
// static const SIniAssociation VideoProgressiveAssociationsData[] =
// {
// {"VI_VIDEO_PROGRESSIVE_TYPE_INVALID", VI_VIDEO_PROGRESSIVE_TYPE_INVALID},
// {"VI_VIDEO_PROGRESSIVE_TYPE_P", VI_VIDEO_PROGRESSIVE_TYPE_P},
// {"VI_VIDEO_PROGRESSIVE_TYPE_SF", VI_VIDEO_PROGRESSIVE_TYPE_SF},
// {"", -1}
// };
//
// CIniAssociations VideoProgressiveAssociationsData(VideoProgressiveAssociationsData, "VI_VIDEO_PROGRESSIVE_");
//
// This will associate
// VI_VIDEO_PROGRESSIVE_TYPE_INVALID(0) with "VI_VIDEO_PROGRESSIVE_TYPE_INVALID" and "TYPE INVALID"
// VI_VIDEO_PROGRESSIVE_TYPE_P(1) with "VI_VIDEO_PROGRESSIVE_TYPE_P" and "TYPE_P"
// VI_VIDEO_PROGRESSIVE_TYPE_SF(1) with "VI_VIDEO_PROGRESSIVE_TYPE_SF" and "TYPE_SF"
//
// The strings can be anything and thus can be used for error messages.
//
struct SIniAssociation
{
    const char* name;
    int type;
};

class MTI_CORECODELIB_API CIniAssociations
{
public:
    CIniAssociations(const SIniAssociation sa[], const string &sPrefix, int nSize = -1);
    CIniAssociations(const char *sl[], const string &sPrefix, int nSize = -1);

    string DisplayName(int Value) const ;
    string SymbolicName(int Value) const ;

    string DisplayNameByIndex(int Value) const ;
    string SymbolicNameByIndex(int Value) const ;

    bool ValueByDisplayName(const string &Name, int &Value) const ;
    bool ValueBySymbolicName(const string &Name, int &Value) const ;

    int Size(void) const ;

    // Setters and getters
    string Prefix(void) const ;
    void Prefix(const string &sPrefix);
    int Index(int Value) const ;

private:
    StringList _Names;
    IntegerList _Types;
    string _Prefix;

};

// Hide the internal helper classes from programmers
// A simple Cheshire smile.
class IniSectionList;

class MTI_CORECODELIB_API CIniFile
{
public:
    string FileName;

    CIniFile(const string &);
    CIniFile(istream &inStream);
    ~CIniFile(void);

    void DeleteKey(const string &Section, const string &Ident);
    void EraseSection(const string &Section);
    void ReadSection(const string &Section, StringList *Strings);
    void ReadSections(StringList *Strings);
    void ReadSectionValues(const string &Section, StringList *Strings);
    bool SectionExists(const string &Section);
    bool KeyExists(const string &Section, const string &Ident);
    bool SaveIfNecessary(void);
    bool IsModified();

    bool WriteStream(ostream &ostream);
    //
    // Read individual value methods
    virtual bool ReadBool(const string &Section, const string &Ident, bool Default);
    virtual double ReadDouble(const string &Section, const string &Name, double Default);
    virtual MTI_INT64 ReadInteger(const string &Section, const string &Name, MTI_INT64 Default);
    virtual MTI_UINT64 ReadUnsignedInteger(const string &Section, const string &Name, MTI_UINT64 Default);
    virtual string ReadString(const string &Section, const string &Ident, const string &Default);
    virtual string ReadString(const string &Section, const string &Ident, const string &Default,
	const string &CommentChars);
    virtual string ReadFileName(const string &Section, const string &Ident, const string &Default);
    virtual void ReadStringList(const string &Section, const string &Ident, StringList *Strings,
	StringList *Default = NULL);
    virtual IntegerList ReadIntegerList(const string &Section, const string &Ident);
    virtual BoolList ReadBoolList(const string &Section, const string &Ident);
    virtual int ReadAssociation(const CIniAssociations &cia, const string &Section, const string &Ident, int Default);

    //
    // Write individual value methods
    virtual void WriteBool(const string &Section, const string &Ident, bool Value);
    virtual void WriteDouble(const string &Section, const string &Name, double Value);
#ifndef _MSC_VER
    virtual void WriteInteger(const string &Section, const string &Name, MTI_INT64 Value);
    virtual void WriteUnsignedInteger(const string &Section, const string &Ident, MTI_UINT64 ullVal);
#else
    virtual void WriteInteger(const string &Section, const string &Name, MTI_INT32 Value);
    virtual void WriteUnsignedInteger(const string &Section, const string &Ident, MTI_UINT32 ullVal);
#endif

    virtual void WriteString(const string &Section, const string &Ident, const string &Value);
    virtual void WriteStringList(const string &Section, const string &Ident, StringList *Strings);
    virtual void WriteIntegerList(const string &Section, const string &Ident, const IntegerList &il);
    virtual void WriteBoolList(const string &Section, const string &Ident, const BoolList &il);
    virtual void WriteAssociation(const CIniAssociations &cia, const string &Section, const string &Ident, int Value);
    //
    // Methods that read and if does not exist, creates it
    bool ReadBoolCreate(const string &Section, const string &Ident, bool Default);
    double ReadDoubleCreate(const string &Section, const string &Name, double Default);
    MTI_INT64 ReadIntegerCreate(const string &Section, const string &Name, MTI_INT64 Default);
    MTI_UINT64 ReadUnsignedIntegerCreate(const string &Section, const string &Name, MTI_UINT64 Default);
    string ReadStringCreate(const string &Section, const string &Ident, const string &Default);
    string ReadStringCreate(const string &Section, const string &Ident, const string &Default,
	const string &CommentChars);
    string ReadFileNameCreate(const string &Section, const string &Ident, const string &Default);
    void ReadStringListCreate(const string &Section, const string &Ident, StringList *Strings,
	StringList *Default = NULL);
    bool DiscardChanges(void);
    bool DiscardChanges(bool DiscardChangesFlag);
    bool ReadOnly(void);
    bool ReadOnly(bool ReadOnlyFlag);

protected:
    virtual bool fReadFile(void); // All the hard work of reading the file
    virtual bool fWriteFile(void); // Writes out the ini file

    bool fReadStream(istream &inStream); // read from STL input stream

private:
    bool bModified; // True if the ini file has been modified
    bool bDiscardChanges; // True if we want to discard changes
    bool bReadOnly; // True if we are readonly
    IniSectionList *Sections; // Where the data is stored
};

// Support Functions
MTI_CORECODELIB_API CIniFile *CreateIniFile(const string &str, bool ReadOnlyFlag = false);
MTI_CORECODELIB_API bool DestroyIniFile(const string &str, bool KeepBackupFiles = false);
MTI_CORECODELIB_API bool DeleteIniFile(CIniFile *&ini, bool DiscardChangesFlag = false);
MTI_CORECODELIB_API bool DeleteIniFileDiscardChanges(CIniFile *&ini);
MTI_CORECODELIB_API string CPMPIniFileName(const string &str, const string &KeyValue);
MTI_CORECODELIB_API string CPMPIniFileName(void);
MTI_CORECODELIB_API string CPMPIniFileName(const string &str);
MTI_CORECODELIB_API string CPMPMasterIniFileName(void);
MTI_CORECODELIB_API CIniFile *CPMPOpenSiteIniFile(void);
MTI_CORECODELIB_API CIniFile *CPMPOpenMasterIniFile(void);
MTI_CORECODELIB_API CIniFile *CPMPOpenLocalMachineIniFile(void);
MTI_CORECODELIB_API void CPMPSetDefaultKeyName(const string &str);
MTI_CORECODELIB_API string CPMPGetDefaultKeyName(void);
MTI_CORECODELIB_API int CPMPTraceLevel(void);
MTI_CORECODELIB_API int CPMPLogLevel(void);
//
// This should not be here, but will be moved later
MTI_CORECODELIB_API void CPMPGetBinRoots(StringList *sl);
// returns argument fullPath if fullPath is not relative to bin root
MTI_CORECODELIB_API string CPMPGetPathRelativeToBinRoot(string const &fullPath);

MTI_CORECODELIB_API string GetProgramDataDir(void);
MTI_CORECODELIB_API string GetSystemDir(void);

//
// Trace Macros
#define CPMPTraceLogLevel() std::max<int>(CPMPTraceLevel(), CPMPLogLevel())

// These macros may appear a bit bizarre, but since they contain an
// 'if', they are surrounded by a "do/while" pair to avoid problems
// within a bounding 'if'.  For a bit more info, one can look at
// http://www.eskimo.com/~scs/C-faq/q10.4.html
// -mpr

#define TRACE_0(a) do { \
                        if (CPMPTraceLogLevel() >= 0) \
                          TRACELOG_MSG(a, CPMPTraceLevel() >= 0, CPMPLogLevel() >= 0);  \
                   } while (0)
#define TRACE_1(a) do { \
                        if (CPMPTraceLogLevel() >= 1) \
                          TRACELOG_MSG(a, CPMPTraceLevel() >= 1, CPMPLogLevel() >= 1); \
                    } while (0)
#define TRACE_2(a) do { \
                        if (CPMPTraceLogLevel() >= 2) \
                          TRACELOG_MSG(a, CPMPTraceLevel() >= 2, CPMPLogLevel() >= 2); \
                   } while (0)
#define TRACE_3(a) do { \
                        if (CPMPTraceLogLevel() >= 3) \
                          TRACELOG_MSG(a, CPMPTraceLevel() >= 3, CPMPLogLevel() >= 3); \
                   } while (0)

#define DBLINE TRACE_0(errout << __LINE__);
#define DBTRACE(A) TRACE_0(errout << "Line: " << __LINE__ << ", " << #A << "= " << A);
#define DBTRACE2(A, B) TRACE_0(errout << "Line: " << __LINE__ << ", " << #A << "= " << A << ", " << #B << "= " << B);
#define DBCOMMENT(A) TRACE_0(errout << (A));
#define DBTIMER(name) DBTimer name##_TIMER(#name)

class DBTimer
{
   CHRTimer _timer;
   string _name;

public:
   DBTimer(const string &name) : _name(name) {}
   ~DBTimer()
   {
      TRACE_0(errout << _name << " took " << _timer << " msec");
   }
};

// ---------------------------------------------------------------------------
// This class is used for Most Recently Used List
// It is a fifo string list with a finite length (usually 10) and a
// very limited interface.
// push to add a string, any matching string is removed
// pop to remove it
// front to return top item
// erase to remove a string
// [] or at to retreive an itemm, [0] is last item pushed
// Some other vector methods are exposed.
//
// NaxSize is the maximum size of the list
// CaseSensitive if strings are compared using case.
//
class MTI_CORECODELIB_API CMRU : private StringList
{
public:
    CMRU(void);
    int MaxSize(int n);
    int MaxSize(void) const ;
    bool CaseSensitive(bool b);
    bool CaseSensitive(void) const ;

    void push(const string &str);
    string pop(void);
    int erase(const string &str);

    // Make public part of the private interface
    using StringList::front;
    using StringList::empty;
    using StringList::size;
    using StringList::clear;
    // using StringList::erase is ugly because its argument is so close to
    // CMRU::erase.  If the former is needed, call StringList::erase
    // explicitly.
    // using StringList::erase;
    string operator[](size_type n);
    CMRU & operator = (const CMRU & out);

    // Define missing at in string list
#ifdef __linux

    string at(int i) const
    {
	return *(begin() + i);
    }
#else
    using StringList::at;
#endif

    // I/O operators
    bool ReadSettings(CIniFile *ini, const string &section, const string &name);
    bool WriteSettings(CIniFile *ini, const string &section, const string &name);

private:
    int _MaxSize;
    bool _CaseSensitive;
};

#endif
