//---------------------------------------------------------------------------

#ifndef UnitTimerTestH
#define UnitTimerTestH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include "machine.h"
#include "HRTimer.h"

//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TButton *Button1;
        TButton *Button2;
        TEdit *Edit1;
        TLabel *Label1;
        TLabel *Label2;
        TEdit *Edit2;
        TEdit *Edit3;
        TButton *Button3;
        TLabel *Label3;
        TEdit *Edit4;
        TButton *Button4;
        TLabel *Label4;
        TEdit *Edit5;
        TButton *Button5;
        TLabel *Label5;
        TEdit *Edit6;
        TButton *Button6;
        TLabel *Label6;
        TEdit *Edit7;
        TButton *Button7;
        TLabel *Label7;
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall Button3Click(TObject *Sender);
        void __fastcall Button4Click(TObject *Sender);
        void __fastcall Button5Click(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall Button6Click(TObject *Sender);
        void __fastcall Button7Click(TObject *Sender);
private:	// User declarations
  CHRTimer hrt;
  HANDLE hTimer;
public:		// User declarations
  void Button2Finish();
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
