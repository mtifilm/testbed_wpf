/*
    File: MTIioX.cc
    Created Sept 21, 2001 by John Mertus

    This file defines the MTIio routines that are compiled under
    Unix and X.  Only unix
*/
//*******************************WINDOW CODE*******************************

#include <sys/dir.h>
#include <iostream>
#include <errno.h>

#include "IniFile.h"  // Just for the trace macros
#include <Xm/Xm.h>
#include <Vk/VkApp.h>
#include <Vk/VkSimpleWindow.h>
#include <Vk/VkErrorDialog.h>
#include <Vk/VkInfoDialog.h>
#include <Vk/VkQuestionDialog.h>
#include <Vk/VkWarningDialog.h>

//*************************UNIX START**************************************

//--------------SendTraceMessage-----------------John Mertus-----Sep 2001---

   bool SendTraceMessage(const string &str)

// This sends the message str to the Tracer program
//
//***************************************************************************
{
  // Strip off any eol
  if (str.size() <= 0) return(true);
  if (str[str.size()-1] == '\n')
    printf("Trace: %s",str.c_str());
  else
    printf("Trace: %s\n",str.c_str());
   fflush(stdout);
   return(true);
}


//----------------CollapseFilePath----------------John Mertus----Sep 2001-----

     string CollapseFilePath(string s)

//
//  This takes a file path/name and produes a simpler path from it.
//  Thus ./a.out becomes /u/mertus/a.out if the current dir was /u/mertus
//  Also /this/that/../what/the reduces to /this/what/the
//
//****************************************************************************
{
   //
   // Collapse /../ if possible
   int n;
   while ((n = s.find("/../")) != string::npos)
     {
       if (n > 0)
         {
           int m = s.rfind("/",n-1);
           s.erase(m,(n-m)+3);
         }
       else
         s.erase(0,3);
     }

   // Make the string pretty
   SearchAndReplace(s,"/./","/");
   while (s.find("//") != string::npos) SearchAndReplace(s,"//","/");

   return(s);
}

//---------------ReturnCompleteName---------------John Mertus----Sep 2001-----

  string ReturnCompleteName(const string &name)

//  This highly specialized routine takes arg[0] and expands
//  the path.
//
//****************************************************************************
{
  if (name == "") return(name);
  // See if a / starts in the name
  // if so it is an absolute path, just return it
  if (name[0] == '/') return(CollapseFilePath(name));

  // See if a / occures in the string
  // if so, its a relative path
  if ( name.find('/') != string::npos)
    {
       char *p = getcwd(NULL,1024);
       if (p == NULL) return(name);
       string s = p;
       free(p);
       s = CollapseFilePath(s+ '/' + name);
       return(s);
    }

  // Nothing found, assum a path, go parse it
  char *p = strdup(getenv("PATH"));
  char *l;
  l = strtok(p, ":");

  // No path found, return name
  if (l == NULL)
     {
       free(p);
       return(name);
     }

  // Continue pushing
  do
    {
       string s = l;
       // Expand . to Current directory
       if (s == ".")
          {
             char *p = getcwd(NULL, 1024);
	     s = p;
	     free(p);
          }


       // Add a / if necessary
       if (s[s.length()-1] != '/') s = s + '/';

       // Now form the name
       s = s + name;
       if (DoesFileExist(s))
	 {
           free(p);
	   return(s);
         }
       l = strtok(NULL, ":");
       // Nothing found
       if (l == NULL)
	 {
	   free(p);
	   return("");
	 }
    } while (true);
}


/*------------_MTISevereErrorDialogHL--------John Mertus----Oct 2001---*/

   int _MTISevereErrorDialogHL(const string &msg, const string &file, int line)

/*  This displays an error and returns the button a user pressed.           */
/*  msg is the message to display                                           */
/*  file is assumed to be the file name (from the __FILE__ macro)           */
/*  line is the line number (from the __LINE__ macro)                       */
/*                                                                          */
/****************************************************************************/
{
  printf("Severe Error %s in %s at %s\n", msg, file, line);
}

