/*
   File Name:  pTimecode.cpp
   Created: Oct 3, 2000 by John Mertus
   Version 1.00

   This is just a version of CTimecode that adds properties
      DropFrame,
      FrameRate,
      Hrs,
      Mins,
      Secs,
      Frames

   Also adds a field and ColorFrame flags with a method for reading the BCD
   information.
*/
#include "pTimecode.h"
#include "BCDLib.h"
#include "MTIstringstream.h"


#ifdef IN_COMPONENT_LIBRARY
#include "SharedMapForComponents.h"
#define SharedMap SharedMapForComponents
#else
#include "SharedMap.h"
#endif


// Constructor, just intialize the properties
PTimecode::PTimecode(const PTimecode &tc)
      : CTimecode(tc)
{
   Assign(tc);
   InitTC();
};

PTimecode::PTimecode(const CTimecode &tc)
      : CTimecode(tc)
{
   Assign(tc);
   InitTC();
};

PTimecode::PTimecode(int frms)
      : CTimecode(frms)
{
   InitTC();
   validateTimecode();
}

PTimecode::PTimecode(int df, int fs)
      : CTimecode(df, fs)
{
   InitTC();
};

PTimecode::PTimecode(int hrs, int mins, int secs, int frms, int flgs, int fps)
      : CTimecode(hrs, mins, secs, frms, flgs, fps)
{
   InitTC();
};

PTimecode::PTimecode(const string &s, int flgs, int fps)
      : CTimecode(0, 0, 0, 0, flgs, fps)
{
   if (!ConformTimeASCII(s))
   {
      Assign(CTimecode::NOT_SET);
   }

   InitTC();
};

PTimecode::~PTimecode()
{
   REMOVE_CBHOOK(UserPreferredTimecodeStringStyleChanged, (*getTimecodeStringStyleCBHook()));
};

//---------------ConformTimeASCII--------------------John Mertus---Feb 2001---

bool PTimecode::ConformTimeASCII(const string &sIn)

//  This takes any character string sIn and produces a valid time code
//  from it.  The time code is right justified, so 3 2 is 00:00:03:02
//  Any : space or . can be used a separators between didgits.
//
//  If sIn contains exactly 8 didgets, sIn is assumed to be two digits of
//  hr, min, sec, frame.  E.g., 01100211 become 01:00:02:11 while
//   1100211 (7 digets) is assumed to be a frame count.
//
//  Trailing and leading spaces are ignored.
//
//  Lastly, if the input begins with a + or -, the following timecode is
// added or subtracted.  +/- inside the code produces invalid results.
//
//  Result is TRUE if the string is wellformed
//         is FALSE if not and theError is set.
//
//  Note:  Multiple delimitators are combined into one, so
//        1  :  4  3 2  is a vaild time code, but so is
//        1::3:2  which parses to 0:1:3:1 not 1:0:3:2
//
//---------------------------------------------------------------------------
{
   // Hack to be able to peek at VTimecodeEdit user input
   m_preConformedString = sIn;

   // fcn is if the timecode is absolute, add or subtract
   enum FunctionType
   {
      ABS, ADD, SUB
   };

   FunctionType fcn = ABS;

   int val[8] =  {0, 0, 0, 0, 0, 0, 0, 0}; // Stack where the time values are saved
   int idx = 4;                            // Stack index.
   char *p;                                // Parsing pointer;
//   int OddField = 0;

   // Determine FPS of the display string (timecode vs frame number).
   int fps = getFramesPerSecond();
   int displayFps = fps;
   int defaultFps;
   bool defaultIsDropFrame;
   getDefaultFpsAndDropFrame(defaultFps, defaultIsDropFrame);
   StringStyle displayStyle = getCurrentUserPreferredStringStyle();
   if (fps == 0 && displayStyle == ForceTimecodeStyle)
   {
      displayFps = defaultFps;
   }
   else if (fps > 0 && displayStyle == ForceFrameNumberStyle)
   {
      displayFps = 0;
   }

   // Trim off all garbage, if null just return
   string ts = StringTrim(sIn);

   //   // Kludge, if we are 720P, see if an odd frame is there
//   if (ts[ts.size() - 1] == '.')
//   {
//      OddField = 1;
//      ts = ts.substr(0, ts.size() - 1);
//
//      if (ts.size() == 0)
//      {
//         InhibitCallbacks();
//         if (is720P())
//         {
//            setTime(0, 0, 0, 1);
//            setField(false);
//         }
//         else
//         {
//            setTime(0, 0, 0, 0);
//            setField(true);
//         }
//         UnInhibitCallbacks();
//         return true;
//      }
//   }

   // Check for leading '+'.
   int inputSign = 1;
   if (ts.size() > 0 && ts[0] == '+')
   {
      // Delete the + character.
      ts = StringTrimLeft(ts.erase(0, 1));
      fcn = ADD;
   }

   // Check for leading '-'. No 'else' here because +- is valid input.
   if (ts.size() > 0 && ts[0] == '-')
   {
       // Delete the - character
      ts = StringTrimLeft(ts.erase(0, 1));

      // If we allow negative number, a - is an absolute value.
      // Otherwise make it indicates subtraction.
      if (m_bAllowNegative)
      {
         inputSign = -1;
      }
      else
      {
         fcn = SUB;
      }
   }

   // If the string is now empty don't do anything!
   if (ts.size() == 0)
   {
      // Do NOT return false here, so it's a true no-op.
      return true;
   }

   if (StringAllDigits(ts) && displayFps == 0)
   {
      // We are showing a frame number, so all digits is always a frame number.
      if (sscanf(ts.c_str(), "%d", &val[3]) != 1)
      {
         theError.set(TIMECODE_ERROR_UNKNOWN_INTERNAL, (string)"Unexpected parse of " + ts + " from " + sIn);
         return false;
      }
   }
   else if (ts.size() == 8 && StringAllDigits(ts))
   {
      // We are showing a timecode, so 8 digits is interpreted as a time code.
      if (sscanf(ts.c_str(), "%2d%2d%2d%2d", &val[0], &val[1], &val[2], &val[3]) != 4)
      {
         theError.set(TIMECODE_ERROR_UNKNOWN_INTERNAL, (string)"Unexpected parse of " + ts + " from " + sIn);
         return false;
      }
   }
   else if (ts.size() == 7 && StringAllDigits(ts))
   {
      // We are showing a timecode, so 7 digits is interpreted as a time code.
      if (sscanf(ts.c_str(), "%1d%2d%2d%2d", &val[0], &val[1], &val[2], &val[3]) != 4)
      {
         theError.set(TIMECODE_ERROR_UNKNOWN_INTERNAL, (string)"Unexpected parse of " + ts + " from " + sIn);
         return false;
      }
   }
   else if (StringAllDigits(ts))
   {
      // We are all digits, less than 7 characters, make a frame
      if (sscanf(ts.c_str(), "%d", &val[3]) != 1)
      {
         theError.set(TIMECODE_ERROR_UNKNOWN_INTERNAL, (string)"Unexpected parse of " + ts + " from " + sIn);
         return false;
      }
   }
   else
   {
      // It's a timecode like "01:02:03:04" or "01.02.03.04" or "01 02 03 04"
      char cTmp[100]; // Error if longer
      memset(cTmp, 0, sizeof(cTmp));
      strncpy(cTmp, ts.c_str(), sizeof(cTmp) - 1);

      p = strtok(cTmp, " :."); // Parse the string
      while ((p != NULL) && (idx < 8))
      {
         // Check that the string is all digits
         if (!StringAllDigits(p))
         {
            theError.set(TIMECODE_ERROR_ILLEGAL_CHARACTERS, (string)"Illegal character in timecode " + ts);
            return false;
         }

         // Now we have a problem, *p can have multiple lengths and still be
         // valid, e.g., 0133 means 01:33
         // Take off most 2 characters
         //
         int l = strlen(p);
         while (l > 0)
         {
            // Scan at most 2 characters
            if (sscanf(p, "%2d", val + idx++) != 1)
            {
               theError.set(TIMECODE_ERROR_ILLEGAL_CHARACTERS, (string)"Illegal character in timecode " + ts);
               return false;
            }

            p = p + std::min<int>(2, l);
            l = strlen(p);
            // Check to see if too much has been entered
            if ((idx >= 8) && (l > 0))
            {
               theError.set(TIMECODE_ERROR_TOO_LONG, (string)"Too many fields in timecode " + ts);
               return false;
            }

         }
         p = strtok(NULL, " :.");
      }

      // See if no more than 4 fields have been entered
      if (p != NULL)
      {
         theError.set(TIMECODE_ERROR_TOO_LONG, (string)"Too many fields in timecode " + ts);
         return false;
      }
   }

   // Now set the timecodes
   PTimecode tcNew(*this);
   int newFps = (fps == 0) ? defaultFps : fps;
   bool newIsDropFrame = (fps == 0) ? defaultIsDropFrame : isDropFrame();
   tcNew.setFpsAndDropFrame(newFps, newIsDropFrame, /* keep= */ false);
   tcNew.setAllowNegative(getAllowNegative());
   tcNew.setSign(inputSign);
   tcNew.setTime(val[idx - 4], val[idx - 3], val[idx - 2], val[idx - 1]);

   // Now decide what to do with this timecode
   // If change, will force notify event
   switch (fcn)
   {
      case ABS:
         setSign(inputSign);
         //Assign(tcNew);
         setAbsoluteTime(tcNew.getAbsoluteTime());
         break;

      case ADD:
         //Frame += tcNew.Frame;
         setAbsoluteTime(getAbsoluteTime() + tcNew.getAbsoluteTime());
         break;

      case SUB:
         //Frame -= tcNew.Frame;
         setAbsoluteTime(getAbsoluteTime() - tcNew.getAbsoluteTime());
         break;
   }

   return true;
}

//---------------Assign-----------------------------John Mertus---Feb 2001---

   void PTimecode::Assign(const CTimecode &tc)

//
//  This assigns a time from one timecode to another
//  If a change, then Notify is called.
//
//***************************************************************************
{
   // Check for a change
   if (tc == *this)
     {
       if (GetInvalidState()) Notify();  // Notify if we are invalidated
       return;
     }

   InhibitCallbacks();                  // To avoid multiple callbacks
   setFramesPerSecond(tc.getFramesPerSecond());
   setFlags(tc.getFlags());
   setTime(tc);
   UnInhibitCallbacks();                // Allow callbacks
}

//---------------TimecodeToBCD----------------------John Mertus---Feb 2001---

   void PTimecode::TimecodeToBCD(bcdTC *bcd) const

//
//  This takes the current time code and returns the BCD equivalent of it
//  in bcdp array.    bcdp must have size 4 bytes.
//  Byte 3 is hrs, 2 is mins, 1 is secs, 0 is frames
//
//***************************************************************************
{
    unsigned char *bcdp = (unsigned char *)bcd;

    if (is720P())
      bcdp[0] = IntToBCD(getFrames()/2);
    else
      bcdp[0] = IntToBCD(getFrames());

    bcdp[1] = IntToBCD(getSeconds());
    bcdp[2] = IntToBCD(getMinutes());
    bcdp[3] = IntToBCD(getHours());

    if (isColorFrame()) bcdp[0] |= 0x80;
    if (isDropFrame()) bcdp[0] |= 0x40;
    if (is720P())
      {
         if ((getFrames() & 1) != 0) bcdp[0] |= 0x80;
      }
    else
      if (getField()) bcdp[1] |= 0x80;
}

//---------------TimecodeToBCDF--------------------Kevin Manbeck--June 2003--

   void PTimecode::TimecodeToBCDF(bcdTC *bcd) const

//
//  This takes the current time code and returns the BCD equivalent of it
//  in bcdp array.    bcdp must have size 4 bytes.
//  This uses the FORWARD direction
//  Byte 0 is hrs, 1 is mins, 2 is secs, 3 is frames
//
//***************************************************************************
{
    unsigned char *bcdp = (unsigned char *)bcd;

    if (is720P())
      bcdp[3] = IntToBCD(getFrames()/2);
    else
      bcdp[3] = IntToBCD(getFrames());

    bcdp[2] = IntToBCD(getSeconds());
    bcdp[1] = IntToBCD(getMinutes());
    bcdp[0] = IntToBCD(getHours());

    if (isColorFrame()) bcdp[3] |= 0x80;
    if (isDropFrame()) bcdp[3] |= 0x40;

    if (is720P())
      {
         if ((getFrames()&1) != 0) bcdp[2] |= 0x80;
      }
    else
      {
         if (getField()) bcdp[2] |= 0x80;
      }

}

//---------------BCDToTimecode----------------------John Mertus---Feb 2001---

   void PTimecode::BCDToTimecode(bcdTC *bcd)

//
//  This takes a char array of BCD time codes and converts them to TC
//  bcdp is the BCD input as from a VTR, 4 characters
//  Byte 3 is hrs, 2 is mins, 1 is secs, 0 is frames
//
//  If a change timecode happens, then Notify is called.
//
//  An undocumented feature of the CURRENT TIME SENSE function is that
//  the most significant bit of the frame data is set when the time
//  code is not drop frame.  The second most significant bit
//  of the frame data is set when the time code is drop frame.
//
//  For some unknown (and undocumented) reason, the most significant
//  bits of the seconds data are often set.  Get rid of this.
//
//  The most significant bit of the minutes is sometimes set.
//
//  In PAL the most significant
//  bits of the hour data are often set.  Get rid of this.
//
//***************************************************************************
{
    int iField, iF, iS, iM, iH;
    bool bDF, bCF;

    unsigned char *bcdp = (unsigned char *)bcd;
    iF = BCDToInt(bcdp[0] & 0x3F);
    iS = BCDToInt(bcdp[1] & 0x7F);
    iM = BCDToInt(bcdp[2] & 0x7F);
    iH = BCDToInt(bcdp[3] & 0x7F);

    /* Set the Color Flags and the Drop Frame */
    bCF = ((bcdp[0] & 0x80) != 0);
    bDF = ((bcdp[0] & 0x40) != 0);

    /* Find which field we are on */
    if (bcdp[1] & 0x80)
      iField = 1;
    else
      iField = 0;

    InhibitCallbacks();                  // To avoid multiple callbacks

    // Fudge the 720P input
    if (is720P())
      iF = 2*iF + iField;
    else
      setField(iField == 1);

    setTime(iH, iM, iS, iF);
    setDropFrame(bDF);
    setColorFrame(bCF);
    UnInhibitCallbacks();                // Allow callbacks
}

//---------------isColorFrame----------------------John Mertus---Feb 2001---

    bool PTimecode::isColorFrame(void) const

//  Just return the value of the color frame
//
//***************************************************************************
{
  return(m_bColorFrame);
}

//---------------setDropFrame----------------------John Mertus---Feb 2001---

   void PTimecode::setDropFrame(bool Value, bool keep)

//  Just set the drop frame, if a change, call a notify
//
//***************************************************************************
{
   // Check for a change
   if (isDropFrame() == Value)
     {
       if (GetInvalidState()) Notify();  // Notify if we are invalidated
       return;
     }

   InhibitCallbacks();
   string OldTC = keep ? String() : string("");
   CTimecode::setDropFrame(Value);
   if (keep) *this = OldTC;
   Notify();
   UnInhibitCallbacks();
}

//---------------setColorFrame----------------------John Mertus---Feb 2001---

   void PTimecode::setColorFrame(bool Value)

//  Just set the color frame, send a notify if a change
//
//***************************************************************************
{
   // Check for a change
   if (isColorFrame() == Value)
     {
       if (GetInvalidState()) Notify();  // Notify if we are invalidated
       return;
     }

   m_bColorFrame = Value;
   Notify();
}

//---------------setField--------------------------John Mertus---Feb 2001---

   void PTimecode::setField(bool Value)

//  Sets the current field, either 1 or 2
//
//***************************************************************************
{
   // Check for a change
   if (getField() == Value)
     {
       if (GetInvalidState()) Notify();  // Notify if we are invalidated
       return;
     }
   CTimecode::setField(Value);
   Notify();
}


//---------------set720P-----------------------------John Mertus---Feb 2001---

    void PTimecode::set720P(bool Value, bool keep)

//  Changes the 720P flag, Value is true if TC is a 720P or false
//
//***************************************************************************
{
   // Check for a change
   if (is720P() == Value)
     {
       if (GetInvalidState()) Notify();  // Notify if we are invalidated
       return;
     }

    InhibitCallbacks();
    CTimecode::set720P(Value);
    setField(false);
    Notify();
    UnInhibitCallbacks();
}

//---------------setTime-----------------------------John Mertus---Feb 2001---

    void PTimecode::setTime(const CTimecode &tc)

//  Sets the timecode to time give by tc.  Notice this is NOT absolute time.
//  TC is a timecode.
//
//***************************************************************************
{
   // Check for a change
   if (tc == *this)
     {
       if (GetInvalidState()) Notify();  // Notify if we are invalidated
       return;
     }
    InhibitCallbacks();
    CTimecode::setTime(tc.getHours(), tc.getMinutes(), tc.getSeconds(), tc.getFrames());
    Notify();
    UnInhibitCallbacks();
}

//--------------setFramesPerSecond------------------John Mertus---Feb 2001---

void PTimecode::setFramesPerSecond(int fps, bool keep)

//  Sets the FPS. Keep is true if we should also change the frame count so
//  the string version of the timecode stays the same.
//
//***************************************************************************
{
    // Check for a change
    if (fps == getFramesPerSecond())
    {
       // Notify if we are invalidated
       if (GetInvalidState())
       {
          Notify();
       }

       return;
    }

    string OldTC = keep ? String() : string("");

    InhibitCallbacks();

    CTimecode::setFramesPerSecond(fps);

    if (keep)
    {
       *this = OldTC;
    }

    Notify();
    UnInhibitCallbacks();
}

//--------------setFpsAndDropFrame------------------------------------------

void PTimecode::setFpsAndDropFrame(int fps, bool dropFrame, bool keep)

//  Sets the FPS and the drop frame. Keep is true if we should also change
//  the frame count so the string version of the timecode stays the same.
//
//***************************************************************************
{
   // Check for a change
   if (fps == getFramesPerSecond() && dropFrame == isDropFrame())
   {
      // Notify if we are invalidated
      if (GetInvalidState())
      {
         Notify();
      }

      return;
   }

   string OldTC = keep ? String() : string("");

   InhibitCallbacks();

   CTimecode::setFramesPerSecond(fps);
   CTimecode::setDropFrame(dropFrame);

   if (keep)
   {
      *this = OldTC;
   }

   Notify();
   UnInhibitCallbacks();
}

//---------------setAttributes----------------------John Mertus---Feb 2001---

    void PTimecode::setAttributes(const CTimecode &tc, bool keep)

//  Sets the attributes (fps, df) of the time code
//  If keep, then original time will be kept
//
//***************************************************************************
{
    // Check for a change
    if (areAttributesSame(tc))
      {
        if (GetInvalidState()) Notify();  // Notify if we are invalidated
        return;
      }

    string OldTC = keep ? String() : string("");

    InhibitCallbacks();

    CTimecode::setDropFrame(tc.isDropFrame());
    CTimecode::setFramesPerSecond(tc.getFramesPerSecond());
    CTimecode::set720P(tc.is720P());

    if (keep)
    {
       *this = OldTC;
    }

    Notify();
    UnInhibitCallbacks();
}

//---------------setAllAttributes----------------------John Mertus---Feb 2001---

    void PTimecode::setAllAttributes(const PTimecode &tc, bool keep)

//  Sets the attributes of the PTimecode this includes the timecode atributes
//  as well as the color flag and group.
//  if Keep, original time will be kept
//
//***************************************************************************
{
   // Check for a change
   if (tc == *this)
     {
       if (GetInvalidState()) Notify();  // Notify if we are invalidated
       return;
     }

    InhibitCallbacks();
    setAttributes(tc, keep);
    setGroup(tc.getGroup());
    UnInhibitCallbacks();
}
//---------------setTime-----------------------------John Mertus---Feb 2001---

    void PTimecode::setTime(int hrs, int mins, int secs, int frms)

//  Sets the timecode to time give by h, min, secs, frams.
//  Notice this is NOT absolute time.  TC is a timecode.
//
//***************************************************************************
{
    // Check to see if a change
    // Check for a change
    if ((getFrames() == frms) && (getSeconds() == secs) && (getMinutes() == mins) && (getHours() == hrs))
     {
       if (GetInvalidState()) Notify();  // Notify if we are invalidated
       return;
     }

    InhibitCallbacks();
    CTimecode::setTime(hrs, mins, secs, frms);
    Notify();
    UnInhibitCallbacks();
};

//---------------setAbsoluteTime---------------------John Mertus---Feb 2001---

    void PTimecode::setAbsoluteTime(int Value)

//  Sets the time in frames.
//
//***************************************************************************
{
   // Because CTimecode can have an invalid tc. make it valid
   // Note the possible problem that we overnotify if Value is not valid
   // This could lead to an infinte loop if the call back resets to an
   // invalid time code.
   validateTimecode();
   // Check for a change
   if (Value == getAbsoluteTime())
     {
       if (GetInvalidState()) Notify();  // Notify if we are invalidated
       return;
     }

   InhibitCallbacks();
   CTimecode::setAbsoluteTime(Value);
   Notify();
   UnInhibitCallbacks();
}

//---------------Index------------------------------John Mertus---May 2001---

    int PTimecode::Index(void)

//  The index is the sequential count from 0 of a time code.  Differes from
// absolute time because it does not skip on a drop frame.  On non-drop frames
// the index and absolutetime are the same
//
//***************************************************************************
{
    int Result = getAbsoluteTime();
    if (!isDropFrame()) return(Result);

    //
    //  Adjust the absolute time to an sequential index
    //
    int im = getMinutes();
    Result = Result - 108*getHours() - 2*(im - (int)im/10);
    return(Result);
}

/*----------------------Dump----------------------John Mertus----Feb 2002---*/

      void PTimecode::Dump(void) const

//     This is a debug routine, it dumps the contents of the TC
//  out to the trace.
//
/****************************************************************************/
{
   TRACE_0(errout << DumpString());
}

//--------------------DumpString------------------John Mertus----Feb 2002---

      string PTimecode::DumpString(void) const

//     This is a debug routine, it dumps the contents of the TC
//  out to the trace.
//
/****************************************************************************/
{
   string EvenOdd[] = {"First", "Second"};
   MTIostringstream os;

    string df;
    isDropFrame() ? (df = "Yes"):(df = "No");
    os <<
    "  Time String:    " << HalfFrameString() << "       " << endl <<
    "  Rate:              " << getFramesPerSecond() << endl <<
    "  Frames:          " << Frames() << endl <<
    "  Drop Frame:   " << df << endl;
    isColorFrame() ? (df = "Yes"):(df = "No");
    os <<
    "  Color Frame:  " << df << endl <<
    "  Field:              " << EvenOdd[getField()];
    if (is720P()) os <<  endl <<
    "  Format:          720P";
//    if (isHalfFrame()) os << ", Film Half Frame";

  return os.str();

}

//---------------setAbsoluteFrame--------------------John Mertus---Feb 2001---

    void PTimecode::setAbsoluteFrame(int Value)

//  Gets the absolute frames
//
//***************************************************************************
{
   // Check for a change
   if (Value == getAbsoluteFrame())
     {
       if (GetInvalidState()) Notify();  // Notify if we are invalidated
       return;
     }

   PTimecode Zero(*this);
   Zero.setTime(0,0,0,0);

   setSign(Value);               // Set the sign, sign is ONLY 1 or -1
   if (m_bAllowNegative && (Value < 0)) Value = -Value;       // Make positive if negative

   InhibitCallbacks();
   *this = Zero + Value;
   Notify();
   UnInhibitCallbacks();
}

//---------------getAbsoluteFrame--------------------John Mertus---Feb 2001---

    int PTimecode::getAbsoluteFrame(void) const

//  Gets the absolute frames
//
//***************************************************************************
{
   PTimecode Zero(*this);
   Zero.setTime(0,0,0,0);
   int Result = *this - Zero;
   return(getSign()*Result);
}

//---------------getGroup---------------------------John Mertus---Feb 2001---

    int PTimecode::getGroup(void) const

//  This returns the group index of the timecode
//
//***************************************************************************
{
   return(m_Group);
}

//---------------setGroup---------------------------John Mertus---Feb 2001---

    void PTimecode::setGroup(int nGroup)

//  This sets the group index, if the group index is changed,
//  a notify event is called
//
//***************************************************************************
{
   // Check for a change
   if (m_Group == nGroup)
     {
       if (GetInvalidState()) Notify();  // Notify if we are invalidated
       return;
     }

   m_Group = nGroup;
}

//---------------setSign---------------------------John Mertus---Aug 2002---

   void PTimecode::setSign(int Value)

//  This sets the sign of the timecode.
//  If Value is < 0, the sign is assumed negative(-1)
//  if Value is >= 0, the sign is assumed positive(+1)
//
//***************************************************************************
{
   if (Value < 0) Value = -1; else Value = 1;
   // Check for a change
   if (Value == m_iSign)
     {
       if (GetInvalidState()) Notify();  // Notify if we are invalidated
       return;
     }

   m_iSign = Value;
   Notify();

}

//---------------getSign---------------------------John Mertus---Aug 2002---

   int PTimecode::getSign(void) const

//  Returns the sign,
//   if AllowNegative is false, then the return is alwasy 1
//   else return is the value of the sign variable (m_iSign)
//
//***************************************************************************
{
   if (!m_bAllowNegative) return(1);
   return(m_iSign);
}


//---------------setAllowNegative------------------John Mertus---Aug 2002---

   void PTimecode::setAllowNegative(bool Value)

//
//***************************************************************************
{
   if (Value == m_bAllowNegative) return;
   m_bAllowNegative = Value;
}

//----------------------Frames----------------------John Mertus---Aug 2002---

   int PTimecode::Frames(void) const

//  Just another name for getAbsoluteFrame
//
//***************************************************************************
{
   return getAbsoluteFrame();
}

//-------------------String------------------------John Mertus---Aug 2002---

   string PTimecode::String(void) const

// Return the string associated with the timecode
//
//***************************************************************************
{
  char Result[20];
  string s;
  getTimeASCII(Result);
  s = Result;
  if (m_bAllowNegative && (m_iSign == -1)) s = "-" + s;
  if (getField()) s = s + '.';
  return s;
}


//---------------HalfFrameString------------------John Mertus---Aug 2002---

   string PTimecode::HalfFrameString(void) const

// Return the string together with the half frame
//
//***************************************************************************
{
   if (!isHalfFrame())
     return String() + " ";
   else
     return String() + "�";
}

//------------------------InitTC---------------------John Mertus---Aug 2002---

   void PTimecode::InitTC(void)

// Common initialization code
//
//***************************************************************************
{
   PROPERTY_INIT(Frame, getAbsoluteFrame, setAbsoluteFrame);
   m_Group = 0;
   m_iSign = 1;
   m_bColorFrame = false;
   m_bAllowNegative = false;

   SET_CBHOOK(UserPreferredTimecodeStringStyleChanged, (*getTimecodeStringStyleCBHook()));
}


//---------------areAttributesSame-------------------John Mertus---Aug 2002---

    bool PTimecode::areAttributesSame(const CTimecode &tc)

// Returns true if the attributes of tc and this the same
//
//***************************************************************************
{
    return ((isDropFrame() == tc.isDropFrame()) &&
           (is720P() == tc.is720P()) &&
           (getFramesPerSecond() == tc.getFramesPerSecond()));

}


/////////////////////////////////////////////////////////////////
//
//		U S E R   P R E F E R R E D   S T R I N G   S T Y L E
//
/////////////////////////////////////////////////////////////////

static string CBHookKey = "TimecodeStringStyleChangedCBHook";
static string StringStyleKey = "TimecodeStringStyle";
static string DefaultStringStyleKey = "TimecodeDefaultStringStyle";
static string CurrentStringStyleKey = "TimecodeCurrentStringStyle";
static string DefaultFpsKey = "TimecodeDefaultFPS";

static string NormalStringStyleValue = "NormalStringStyle";
static string TimecodeStringStyleValue = "ForceTimecodeStyle";
static string FrameNumberStringStyleValue = "ForceFrameNumberStyle";
static int Fps24Value = 24;
static int Fps25Value = 25;
static int Fps30DfValue = -30;
static int Fps30Value = 30;
static int Fps50Value = 50;
static int Fps60Value = 60;
static int Fps60DfValue = -60;

CBHook *PTimecode::getTimecodeStringStyleCBHook()
{
   void *temp = SharedMap::retrieve(CBHookKey);
   CBHook *timecodeStringStyleChanged = reinterpret_cast<CBHook *>(temp);
   if (timecodeStringStyleChanged == NULL)
   {
      timecodeStringStyleChanged = new CBHook;
      SharedMap::add(CBHookKey, timecodeStringStyleChanged);
   }

   return timecodeStringStyleChanged;
}

string PTimecode::getUserPreferredStyleString()
{
   PTimecode localTimecode(*this);

   switch (getCurrentUserPreferredStringStyle())
   {
      case NormalStringStyle:
         break;

      case ForceFrameNumberStyle:
         localTimecode.setFpsAndDropFrame(0, false, /* keep= */ false);
         break;

      case ForceTimecodeStyle:
         if (localTimecode.getFramesPerSecond() == 0)
         {
            int defaultFps;
            bool defaultIsDropFrame;
            getDefaultFpsAndDropFrame(defaultFps, defaultIsDropFrame);
            localTimecode.setFpsAndDropFrame(defaultFps, defaultIsDropFrame, /* keep= */ false);
         }

         break;
   }

   string retVal;
   localTimecode.getTimeString(retVal);

   return retVal;
}

PTimecode::StringStyle PTimecode::getDefaultUserPreferredStringStyle()
{
   void * temp = SharedMap::retrieve(DefaultStringStyleKey);
   string * stringStyleAsStringPtr = reinterpret_cast<string *>(temp);
   if (stringStyleAsStringPtr == NULL)
   {
      stringStyleAsStringPtr = &NormalStringStyleValue;
      SharedMap::add(DefaultStringStyleKey, stringStyleAsStringPtr);
   }

   return (*stringStyleAsStringPtr == TimecodeStringStyleValue)
            ? ForceTimecodeStyle
            : ((*stringStyleAsStringPtr == FrameNumberStringStyleValue)
               ? ForceFrameNumberStyle
               : NormalStringStyle);
}

void PTimecode::setDefaultUserPreferredStringStyle(PTimecode::StringStyle newStringStyle)
{
   PTimecode::StringStyle oldStringStyle = getDefaultUserPreferredStringStyle();
   if (oldStringStyle == newStringStyle)
   {
      return;
   }

   string * stringStyleAsStringPtr = (newStringStyle == ForceFrameNumberStyle)
                                 ? &FrameNumberStringStyleValue
                                 : ((newStringStyle == ForceTimecodeStyle)
                                    ? &TimecodeStringStyleValue
                                    : &NormalStringStyleValue);
   SharedMap::add(DefaultStringStyleKey, stringStyleAsStringPtr);

   setCurrentUserPreferredStringStyle(newStringStyle);
}

PTimecode::StringStyle PTimecode::getCurrentUserPreferredStringStyle()
{
   void * temp = SharedMap::retrieve(CurrentStringStyleKey);
   string * stringStyleAsStringPtr = reinterpret_cast<string *>(temp);
   if (stringStyleAsStringPtr == 0)
   {
      stringStyleAsStringPtr = &NormalStringStyleValue;
      SharedMap::add(CurrentStringStyleKey, stringStyleAsStringPtr);
   }

   return (*stringStyleAsStringPtr == TimecodeStringStyleValue)
            ? ForceTimecodeStyle
            : ((*stringStyleAsStringPtr == FrameNumberStringStyleValue)
               ? ForceFrameNumberStyle
               : NormalStringStyle);
}

void PTimecode::setCurrentUserPreferredStringStyle(PTimecode::StringStyle newStringStyle)
{
   PTimecode::StringStyle oldStringStyle = getCurrentUserPreferredStringStyle();
   if (oldStringStyle == newStringStyle)
   {
      return;
   }

   string * stringStyleAsStringPtr = (newStringStyle == ForceFrameNumberStyle)
                                 ? &FrameNumberStringStyleValue
                                 : ((newStringStyle == ForceTimecodeStyle)
                                    ? &TimecodeStringStyleValue
                                    : &NormalStringStyleValue);
   SharedMap::add(CurrentStringStyleKey, stringStyleAsStringPtr);

   getTimecodeStringStyleCBHook()->Notify();
}

void PTimecode::getDefaultFpsAndDropFrame(int &defaultFps, bool &defaultIsDropFrame)
{
   void * temp = SharedMap::retrieve(DefaultFpsKey);
   int * defaultFpsAsIntPtr = reinterpret_cast<int *>(temp);
   if (defaultFpsAsIntPtr == 0)
   {
      defaultFpsAsIntPtr = &Fps24Value;
      SharedMap::add(DefaultFpsKey, defaultFpsAsIntPtr);
   }

   defaultIsDropFrame = *defaultFpsAsIntPtr < 0;
   defaultFps = defaultIsDropFrame ? -*defaultFpsAsIntPtr : *defaultFpsAsIntPtr;
}

void PTimecode::setDefaultFpsAndDropFrame(int newDefaultFps, bool newDefaultIsDropFrame)
{
   int oldDefaultFps;
   bool oldDefaultIsDropFrame;
   getDefaultFpsAndDropFrame(oldDefaultFps, oldDefaultIsDropFrame);
   if (oldDefaultFps == newDefaultFps && oldDefaultIsDropFrame == newDefaultIsDropFrame)
   {
      return;
   }

   int * defaultFpsAsIntPtr = (newDefaultFps == 25)
                              ? &Fps25Value
                              : ((newDefaultFps == 30 && newDefaultIsDropFrame == true)
                                 ? &Fps30DfValue
                                 : ((newDefaultFps == 30 && newDefaultIsDropFrame == false)
                                    ? &Fps30Value
                                    : ((newDefaultFps == 50)
                                       ? &Fps50Value
                                       : ((newDefaultFps == 60 && newDefaultIsDropFrame == true)
                                          ? &Fps60DfValue
                                          : ((newDefaultFps == 60 && newDefaultIsDropFrame == false)
                                             ? &Fps60Value
                                             : &Fps24Value)))));

   SharedMap::add(DefaultFpsKey, defaultFpsAsIntPtr);

   getTimecodeStringStyleCBHook()->Notify();
}

//***************************************************************************
// When the timecode string style changes, pretend the timecode changed.
void PTimecode::UserPreferredTimecodeStringStyleChanged(void *Sender)
{
   Notify();
}

//***************************************************************************
