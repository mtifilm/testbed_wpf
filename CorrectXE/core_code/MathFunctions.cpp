//---------------------------------------------------------------------------

#pragma hdrstop

#include "MathFunctions.h"
#include "TheError.h"
#include <math.h>
#include <vector>
#include <map>

#include "ippdefs.h"
#include "ippcore.h"
#include "ippi.h"

using std::vector;
using std::map;
#include "IniFile.h"  // for TRACE_N()

//TODO move this to a better place
#include <exception>

class IppException: public std::exception
{
public:
  IppException()
  {
     this->ippStatus = ippStsNoErr;
  }

  IppException(IppStatus status)
  {
	 this->ippStatus = status;
  }

  void SetException(IppStatus status)
  {
	 this->ippStatus = status;
  }

  virtual const char* what() const throw()
  {
	return ippGetStatusString(ippStatus);
  }

private:
  IppStatus ippStatus;
};


//---------------------------------------------------------------------------

#pragma package(smart_init)

bool Smooth3(std::valarray<double> &MC, int nSFixed, int nEFixed, double Alpha,
             int *percentDone, bool *abortFlag)
//
//  nSFixed and nEFixed can only be 0, 1, or 2
//
//
{
   if (percentDone != NULL)
       *percentDone = 0;

   // Don't do anything if alpha is negative
   // This means no smoothing
   if (Alpha < 0 || MC.size() == 0)
     {
       if (percentDone != NULL)
           *percentDone = 100;
       return true;
     }

   // Constant if Alpha is 0
   if (Alpha == 0)
     {
       double constValue = MC[0];  // Original value at start frame
       if (nSFixed < 1)
         {
           // No anchor at start
           if (nEFixed > 0)
             {
               // Anchor at end - use that value
               constValue = MC[MC.size() - 1];
             }
           else
             {
               // No anchors - use the median of all the values
               double dMin = 10000.0;
               double dMax = -10000.0;
               for (unsigned i = 0; i < MC.size(); i++)
                 {
                   if (MC[i] < dMin) dMin = MC[i];
                   if (MC[i] > dMax) dMax = MC[i];
                 }
               constValue = (dMin + dMax) / 2;
             }
         }
       for (unsigned i=0; i < MC.size(); i++) MC[i] = constValue;
       if (percentDone != NULL)
           *percentDone = 100;
       return true;
     }

   // If either nSFixed or nEFixed are zero, smoothing required at least 4 points
   int T = MC.size();
   if (((nSFixed == 0) || (nEFixed == 0)) && (T < 4))
     {
       theError.set(MATH_ERROR_SMOOTHING_TOO_FEW_FRAMES, "Operation failed: To render this shot, the marked frame range must be greater than four frames)");
       return false;
     }

   // Both are anchored
   if (T <= (nSFixed+nEFixed))
     {
       theError.set(MATH_ERROR_SMOOTHING_TOO_FEW_FRAMES, "Operation failed: To render this shot, the marked frame range must be greater than the total number of anchor frames.");
       return false;
     }

   //  If T = 3, then the anchor size are 1 and 1, so we just do a fake smoothing by
   //  duplicating the anchor points.  The reason for this is I don't have
   //  the time to solve this mathematically
   if ((T == 3) && (nSFixed == 1) && (nEFixed == 1))
     {
       matrix<long double> L(5, 5);
       L.zero();

       Alpha = 4*Alpha;
       L[0][0] = Alpha;
       L[1][1] = Alpha;

       L[2][0] = 1;
       L[2][1] = -4;
       L[2][2] = 6 + Alpha;
       L[2][3] = -4;
       L[2][4] = 1;

       L[3][3] = Alpha;
       L[4][4] = Alpha;

       mvector<int> mvIndex(5);
       matrix<long double> LU = L;
       double d;

      // Decompose the matrix
      if (!ludcmp<long double>(LU, mvIndex, d, percentDone, abortFlag))
      {
         theError.set(MATH_ERROR_SMOOTHING_MATRIX_SINGULAR, "Internal Error: Cannot invert matrix");
         return false;
      }

      mvector<long double> Wx(5);
      for (unsigned i=0; i < 3; i++) Wx[i+1] = Alpha*MC[i];
      Wx[0] = Wx[1];
      Wx[4] = Wx[3];

      lubksb<long double>(LU, mvIndex, Wx);
      MC[1] = Wx[2];

      if (percentDone != NULL)
          *percentDone = 100;
      return true;
     }

   // T >= 4 and this is the correct solution.
   matrix<long double> L(T, T);
   L.zero();

   // Do the first two rows
   if (nSFixed > 0)
     {
       L[0][0] = Alpha;
     }
   else
     {
       L[0][0] = 1 + Alpha;
       L[0][1] = -2;
       L[0][2] = 1;
     }

   if (nSFixed > 1)
     {
        L[1][1] = Alpha;
     }
   else
     {
       L[1][0] = -2;
       L[1][1] = 5 + Alpha;
       L[1][2] = -4;
       L[1][3] = 1;
     }

   // Now do the middle of the matrix
   for (int i = 2; i < T-2; i++)
     {
       L[i][i-2] = 1;
       L[i][i-1] = -4;
       L[i][i]   = 6 + Alpha;
       L[i][i+1] = -4;
       L[i][i+2] = 1;
     }


   // Do the last two rows
   if (nEFixed > 1)
     {
       L[T-2][T-2] = Alpha;
     }
   else
     {
       L[T-2][T-4] = 1;
       L[T-2][T-3] = -4;
       L[T-2][T-2] = 5 + Alpha;
       L[T-2][T-1] = -2;
     }

   if (nEFixed > 0)
     {
       L[T-1][T-1] = Alpha;
     }
   else
     {
       L[T-1][T-3] = 1;
       L[T-1][T-2] = -2;
       L[T-1][T-1] = 1 + Alpha;
     }

   mvector<int> mvIndex(T);
   matrix<long double> LU = L;
   double d;

   // Decompose the matrix
    if (!ludcmp<long double>(LU, mvIndex, d, percentDone,  abortFlag))
      {
         theError.set(MATH_ERROR_SMOOTHING_MATRIX_SINGULAR, "Internal Error: Cannot invert matrix");
         return false;
      }

    mvector<long double> Wx(T);
    for (unsigned i=0; i < MC.size(); i++) Wx[i] = Alpha*MC[i];

    lubksb<long double>(LU, mvIndex, Wx);

    for (unsigned i=0; i < MC.size(); i++) MC[i] = Wx[i];

    if (percentDone != NULL)
        *percentDone = 100;
    return true;
}

typedef struct
  {
     double dMin;
     int    nLoc;
  }  SFcnValue;

typedef map<int, SFcnValue> CFcnValues;
typedef map<int, CFcnValues> AFcnValues;
typedef map<int, AFcnValues> CDPArray;


void Smooth2(std::valarray<double> &MC, int nSFixed, int nEFixed, double Alpha,
             int *percentDone, bool *abortFlag)
{
  // This needs work: it seems to always do nSFixed=2, nEFixed=2.

   CDPArray DPA;

   double dMin = MC.min();

   std::valarray<double> Z(4.0*(MC - dMin));
   int L = Z.max();
   int T = Z.size();

   // Compute all the F and G functions
   double c0, c1, cm, mn;
   SFcnValue FV;
   // Create the first array
   for (int a=0; a < L; a++)
     for (int b=0; b < L; b++)
       {
          // Compute F(0,a,b)
          mn = (-2*a + b);
          mn = mn*mn + Alpha*Z[0]*Z[0];
          FV.dMin = mn;
          FV.nLoc = 0;
          DPA[0][a][b] = FV;

          for (int xh=1; xh <= L; xh++)
            {
              c0 = xh - 2*a + b;
              c0 *= c0;
              c1 = (Z[0] - xh);
              c1 *= c1;
              cm = c0 + Alpha*c1;
              if (cm < mn)
                {
                  mn = cm;
                  FV.dMin = mn;
                  FV.nLoc = xh;
                  DPA[0][a][b] = FV;
                }
             }
       }
   // Create the remaining values
   for (int k = 1; k < T; k++)
{
   for (int a=0; a <= L; a++)
     for (int b=0; b <= L; b++)
       {
          // Compute F(k,a,b)
          mn = 32*L*L*L*L;
          for (int xh=0; xh <= L; xh++)
            {
              c0 = xh - 2*a + b;
              c0 *= c0;
              c1 = (Z[k] - xh);
              c1 *= c1;
              cm = c0 + c1 + DPA[k-1][xh][a].dMin;
              if (cm < mn)
                {
                  mn = cm;
                  FV.dMin = mn;
                  FV.nLoc = xh;
                  DPA[k][a][b] = FV;
                }
             }
       }
}
   // Now trace bacwards
   std::valarray<int> ZH(T);
   for (int k=0; k < T; k++)
     ZH[k] = Z[k];

   for (int k=T-3; k >= 2; k--)
     {
       ZH[k] = DPA[k][ZH[k+1]][ZH[k+2]].nLoc;
     }

   for (int k=0; k < T; k++)
     MC[k] = ZH[k]/4.0 + dMin;
}


//float UEuclidean(float X0, float Y0, float X1, float Y1)
//// Computes the Euclidean distance
//{
//   float r0 = (X0 - X1);
//   float r1 = (Y0 - Y1);
//   return sqrt((float)(r0*r0 + r1*r1));
//}
//
//float UThinPlate(float X0, float Y0, float X1, float Y1)
//// Computes the thin spline Distance
//// This should be refactored for speed
//{
//   float r0 = (X0 - X1);
//   float r1 = (Y0 - Y1);
//   float r = sqrt(r0*r0 + r1*r1);
//   if (r == 0) return 0;
//   if (r <= 10E-100) r=10E-100;
//   return r*r*log(r);
//}

float ULogrithmic(float X0, float Y0, float X1, float Y1)
{
   float r0 = (X0 - X1);
   float r1 = (Y0 - Y1);
   float r = sqrt(r0*r0 + r1*r1);
   if (r == 0) return 0;
   if (r <= 10E-100) r=10E-100;
   return r*log(r);
}

//float UIdenity(float X0, float Y0, float X1, float Y1)
//{
//   return 0;
//}

int L1Norm(MTI_UINT16 *p1, MTI_UINT16 *p2, int N)
{
  int Result = 0;
  for (int i=0; i < N; i++)
      Result += abs(p1[i] - p2[i]);

  return Result;
}

double ComputeL1BoxNormCCode(MTI_UINT16 *pBase, int nB, MTI_UINT16 *pNext, int nN, int x, int y)
{
   double dRet = 0;
   MTI_UINT16 *pB = pBase;
   MTI_UINT16 *pN = pNext + y*nN + x;
   for (int i=0; i < nB; i++)
	 {
		dRet += L1Norm(pB, pN, nB);
		pB += nB;
		pN += nN;
	 }

   return dRet;
}

double ComputeL1BoxNormIppCode(MTI_UINT16 *pBase, int nB, MTI_UINT16 *pNext, int nN, int x, int y)
{
   double dRet = 0;

   // Assume we are square
   IppiSize roi = {nB, nB};
   unsigned short *target = pNext + nN * y + x;

   // The width is given in bytex not pixels, while the roi is in pixels
   IppStatus err = ippiNormDiff_L1_16u_C1R(pBase, sizeof(MTI_UINT16) * nB, target, sizeof(MTI_UINT16) * nN, roi, &dRet);
   if (err != ippStsNoErr)
   {
	   IppException ippException;
	   ippException.SetException(err);
	   throw ippException;
   }

   return dRet;
}

double ComputeL1BoxNorm(MTI_UINT16 *pBase, int nB, MTI_UINT16 *pNext, int nN, int x, int y)
{
	return ComputeL1BoxNormIppCode(pBase, nB, pNext, nN, x, y);
}

// Compute the L1 norm between the tracking box (pBase) and the search area pNext
// i.e., the L1 norm of a small box (tracking) in a bigger box (search area).
// pBase is the tracking of size (nBx, nBy).
// pNext is the search box (must be bigger than tracking boxl and has columns nN
// (x,y) is the point the upper left of the tracking box starts at.
//   as a result x must be between 0 and (horz size of search area - horz size of tracking box) or nN - nBx.
//   and y between 0 and (vert size of search area - vert size of tracking box)
double ComputeL1BoxNormIppCode(MTI_UINT16 *pBase, int nBx, int nBy, MTI_UINT16 *pNext, int nN, int x, int y)
{
    double dRet = 0;

   // Assume we are square
   IppiSize roi = {nBx, nBy};
   unsigned short * target = pNext + nN * y + x;

   // The width is given in bytex not pixels, while the roi is in pixels
   IppStatus err = ippiNormDiff_L1_16u_C1R(pBase, sizeof(MTI_UINT16) * nBx, target, sizeof(MTI_UINT16) * nN, roi, &dRet);
   if (err != ippStsNoErr)
   {
	   IppException ippException;
	   ippException.SetException(err);
	   throw ippException;
   }

   return dRet;
}

double ComputeL1BoxNormCCode(MTI_UINT16 *pBase, int nBx, int nBy, MTI_UINT16 *pNext, int nN, int x, int y)
{
   double dRet = 0;
   MTI_UINT16 *pB = pBase;
   MTI_UINT16 *pN = pNext + y*nN + x;
   for (int i=0; i < nBy; i++)
     {
        dRet += L1Norm(pB, pN, nBx);
        pB += nBx;
        pN += nN;
     }

   return dRet;
}

double ComputeL1BoxNorm(MTI_UINT16 *pBase, int nBx, int nBy, MTI_UINT16 *pNext, int nN, int x, int y)
{
	  return   ComputeL1BoxNormIppCode(pBase, nBx, nBy, pNext, nN, x, y);
}

 void CopySubImage(MTI_UINT16 *imgBase, int nCol, int nRow, double x, double y, int N, MTI_UINT16 *imgOut)
{
//   int ix = tp.x + 0.5;
//   int iy = tp.y + 0.5;
   int ix = x;
   int iy = y;

   // Copy over the data, points outside of the image are defined as 0
   int iSC = ix - N;
   if (iSC >= nCol) return;
   int iEC = ix + N;
   if (iEC < 0) return;

   int iSR = iy - N;
   if (iSR >= nRow) return;
   int iER = iy + N;
   if (iER < 0) return;

   // Set the extent of the copy
   if (iSC < 0) iSC = 0;
   if (iEC >= nCol) iEC = nCol-1;
   if (iSR < 0) iSR = 0;
   if (iER >= nRow) iER = nRow-1;

   int mPPerR = 2*N+1;

   MTI_UINT16 *pB = imgBase + iSR*nCol + iSC;
   MTI_UINT16 *pN = imgOut + (iSR - (iy-N))*mPPerR + (iSC - (ix - N));

   for (int iR = iSR; iR <= iER; iR++)
     {
        memmove(pN, pB, (iEC - iSC + 1)*sizeof(MTI_UINT16));
        pB += nCol;
        pN += mPPerR;
     }

   // Now fill in any missing points
   // We don't have to be fast here as it doesn't happen often

   // Fill them at the bottom
   pB = imgOut + (iER-iSR)*mPPerR;
   pN = imgOut + (iER-iSR+1)*mPPerR;
   for (int iR = iER+1; iR <= iy+N; iR++)
     {
        memmove(pN, pB, mPPerR*sizeof(MTI_UINT16));
        pN += mPPerR;
     }

   // Fill them in at the top
   pB = imgOut + (iSR - (iy-N))*mPPerR;
   pN = imgOut;
   for (int iR = iy-N; iR < iSR ; iR++)
     {
        memmove(pN, pB, mPPerR*sizeof(MTI_UINT16));
        pN += mPPerR;
     }

   // Fill them out at the left side
   if (iSC == 0)
     {
       pN = imgOut;
       for (int iR = -N; iR <= N ; iR++)
         {
           for (int iC=0; iC < iSC - (ix-N); iC++)
             pN[iC] = pN[iSC - (ix-N)];

           pN += mPPerR;
         }
     }

   // Fill them out at the left side
   if (iEC == (nCol-1))
     {
       pN = imgOut;
       for (int iR = -N; iR <= N ; iR++)
         {
           for (int iC=nCol; iC < ix+N; iC++)
             pN[iC-iSC] = pN[nCol-iSC-1];

           pN += mPPerR;
         }
     }

}

void CopySubImage(MTI_UINT16 *imgBase, int nCol, int nRow,
                  double xul, double yul, int Nx, int Ny,
                  MTI_UINT16 *imgOut)
{
//   int ix = tp.x + 0.5;
//   int iy = tp.y + 0.5;
   int ix = (int) xul;
   int iy = (int) yul;

   // Copy over the data, points outside of the image are defined as 0
   int iSC = ix;
   if (iSC >= nCol) return;
   int iEC = ix + Nx;
   if (iEC < 0) return;

   int iSR = iy;
   if (iSR >= nRow) return;
   int iER = iy + Ny;
   if (iER < 0) return;

   // Set the extent of the copy
   if (iSC < 0) iSC = 0;
   if (iEC >= nCol) iEC = nCol-1;
   if (iSR < 0) iSR = 0;
   if (iER >= nRow) iER = nRow-1;

   int mPPerR = Nx+1;

   MTI_UINT16 *pB = imgBase + iSR*nCol + iSC;
   MTI_UINT16 *pN = imgOut + (iSR - iy)*mPPerR + (iSC - ix);

   for (int iR = iSR; iR <= iER; iR++)
     {
        memmove(pN, pB, (iEC - iSC + 1)*sizeof(MTI_UINT16));
        pB += nCol;
        pN += mPPerR;
     }

   // Now fill in any missing points
   // We don't have to be fast here as it doesn't happen often

   // Fill them at the bottom
   pB = imgOut + (iER-iSR)*mPPerR;
   pN = imgOut + (iER-iSR+1)*mPPerR;
   for (int iR = iER+1; iR <= iy+Ny; iR++)
     {
        memmove(pN, pB, mPPerR*sizeof(MTI_UINT16));
        pN += mPPerR;
     }

   // Fill them in at the top
   pB = imgOut + (iSR - iy)*mPPerR;
   pN = imgOut;
   for (int iR = iy; iR < iSR ; iR++)
     {
        memmove(pN, pB, mPPerR*sizeof(MTI_UINT16));
        pN += mPPerR;
     }

   // Fill them out at the left side
   if (iSC == 0)
     {
       pN = imgOut;
       for (int iR = 0; iR <= Ny ; iR++)
         {
           for (int iC=0; iC < iSC - ix; iC++)
             pN[iC] = pN[iSC - ix];

           pN += mPPerR;
         }
     }

   // Fill them out at the left side
   if (iEC == (nCol-1))
     {
       pN = imgOut;
       for (int iR = 0; iR <= Ny ; iR++)
         {
           for (int iC=nCol; iC < ix+Nx; iC++)
             pN[iC-iSC] = pN[nCol-iSC-1];

           pN += mPPerR;
         }
     }

}

void CreateSubPixelMatrix2x(MTI_UINT16 *pIn, int nCol, double x, double y, int Nx, int Ny, MTI_UINT16 *pOut)

// Computes the fixed (2 levels) subpixel matrix pOut from the position x, y
//
//  Where pIn is apointer to the image
//  N is the extent of the source image (x-N, x+N) x (y-N, y+N)
//  pOut is 2 times bigger
//  pOut MUST exist
//
// Bilinear interpolation is done by
//  f(x,y) = ax + by + cxy + d
//  where 0 <= x,y <= 1 and the coefficients are given by
//  a = -C00 + C10
//  b = -C00 + C01
//  c =  C00 - C10 - C01 + C11
//  d =  C00
//
//******************************************************************************
{
   // Set up the pointer
   MTI_UINT16 *pR = pIn + ((int)y - Ny)*nCol + (int)x - Nx;
   MTI_UINT16 *pO = pOut;
   int IntraColSize = 2*(2*Nx + 1);

   for (int iR = 0; iR < (2*Ny+1); iR++)
     {
       MTI_UINT16 *p0 = pO;
       MTI_UINT16 *p1 = p0 + IntraColSize;

       MTI_UINT16 *pRI = pR;
       for (int iC = 0; iC < (2*Nx+1); iC++)
         {
            double C00 = *pRI;
            double C10 = *(pRI+1);
            double C01 = *(pRI + nCol);
            double C11 = *(pRI + nCol + 1);
            double a = -C00 + C10;
            double b = -C00 + C01;
            double c =  C00 - C10 - C01 + C11;
            double d =  C00;

            #define zero (0.0)
            #define half (1.0/2)

            // y = 0/2
            p0[0] = zero*a + zero*b + (zero*zero)*c + d;
            p0[1] = half*a + zero*b + (half*zero)*c + d;
            p0 += 2;

            // y = 1/2
            p1[0] = zero*a + half*b + (zero*half)*c + d;
            p1[1] = half*a + half*b + (half*half)*c + d;
            p1 += 2;

            pRI += 1;
         }

       // Done with input row, continue to next input row
         pR += nCol;
         pO += 2*IntraColSize;
     }
}


void CreateSubPixelMatrix2(MTI_UINT16 *pIn, int nCol, double x, double y, int N, MTI_UINT16 *pOut)

// Computes the fixed (4 levels) subpixel matrix pOut from the position x, y
//
// like the following but (2N+1)x(2N+1) instead of (2Nx+1)x(2Ny+1)
//
//  Where pIn is apointer to the image
//  N is the extent of the source image (x-N, x+N) x (y-N, y+N)
//  pOut is 4 times bigger
//  pOut MUST exist
//
// Bilinear interpolation is done by
//  f(x,y) = ax + by + cxy + d
//  where 0 <= x,y <= 1 and the coefficients are given by
//  a = -C00 + C10
//  b = -C00 + C01
//  c =  C00 - C10 - C01 + C11
//  d =  C00
//
//******************************************************************************
{
   // Set up the pointer
   MTI_UINT16 *pR = pIn + ((int)y - N)*nCol + (int)x - N;
   MTI_UINT16 *pO = pOut;
   int IntraColSize = 4*(2*N + 1);

   for (int iR = 0; iR < (2*N+1); iR++)
     {
       MTI_UINT16 *p0 = pO;
       MTI_UINT16 *p1 = p0 + IntraColSize;
       MTI_UINT16 *p2 = p1 + IntraColSize;
       MTI_UINT16 *p3 = p2 + IntraColSize;

       MTI_UINT16 *pRI = pR;
       for (int iC = 0; iC < (2*N+1); iC++)
         {
            double C00 = *pRI;
            double C10 = *(pRI+1);
            double C01 = *(pRI + nCol);
            double C11 = *(pRI + nCol + 1);
            double a = -C00 + C10;
            double b = -C00 + C01;
            double c =  C00 - C10 - C01 + C11;
            double d =  C00;

            // y = 0
            *p0++ = d;
            *p0++ = 0.25*a + d;
            *p0++ = 0.50*a + d;
            *p0++ = 0.75*a + d;

            // y = .25
            *p1++ = 0.25*b + d;
            *p1++ = 0.25*a + 0.25*b + (0.25*0.25)*c + d;
            *p1++ = 0.50*a + 0.25*b + (0.50*0.25)*c + d;
            *p1++ = 0.75*a + 0.25*b + (0.75*0.25)*c + d;

            // y = .50
            *p2++ = 0.50*b + d;
            *p2++ = 0.25*a + 0.50*b + (0.25*0.50)*c + d;
            *p2++ = 0.50*a + 0.50*b + (0.50*0.50)*c + d;
            *p2++ = 0.75*a + 0.50*b + (0.75*0.50)*c + d;

            // y = .75
            *p3++ = 0.75*b + d;
            *p3++ = 0.25*a + 0.75*b + (0.25*0.75)*c + d;
            *p3++ = 0.50*a + 0.75*b + (0.50*0.75)*c + d;
            *p3++ = 0.75*a + 0.75*b + (0.75*0.75)*c + d;

             pRI += 1;
         }

       // Done with input row, continue to next input row
         pR += nCol;
         pO += 4*IntraColSize;
     }
}

void CreateSubPixelMatrix4x(MTI_UINT16 *pIn, int nCol, double x, double y, int Nx, int Ny, MTI_UINT16 *pOut)

// Computes the fixed (4 levels) subpixel matrix pOut from the position x, y
//
//  Where pIn is apointer to the image
//  N is the extent of the source image (x-N, x+N) x (y-N, y+N)
//  pOut is 4 times bigger
//  pOut MUST exist
//
// Bilinear interpolation is done by
//  f(x,y) = ax + by + cxy + d
//  where 0 <= x,y <= 1 and the coefficients are given by
//  a = -C00 + C10
//  b = -C00 + C01
//  c =  C00 - C10 - C01 + C11
//  d =  C00
//
//******************************************************************************
{
   // Set up the pointer
   MTI_UINT16 *pR = pIn + ((int)y - Ny)*nCol + (int)x - Nx;
   MTI_UINT16 *pO = pOut;
   int IntraColSize = 4*(2*Nx + 1);

   for (int iR = 0; iR < (2*Ny+1); iR++)
     {
       MTI_UINT16 *p0 = pO;
       MTI_UINT16 *p1 = p0 + IntraColSize;
       MTI_UINT16 *p2 = p1 + IntraColSize;
       MTI_UINT16 *p3 = p2 + IntraColSize;

       MTI_UINT16 *pRI = pR;
       for (int iC = 0; iC < (2*Nx+1); iC++)
         {
            double C00 = *pRI;
            double C10 = *(pRI+1);
            double C01 = *(pRI + nCol);
            double C11 = *(pRI + nCol + 1);
            double a = -C00 + C10;
            double b = -C00 + C01;
            double c =  C00 - C10 - C01 + C11;
            double d =  C00;

#define zero4ths (0.0)
#define one4th (1.0/4)
#define two4ths (2.0/4)
#define three4ths (3.0/4)

            // y = 0/4
            p0[0] = zero4ths *a + zero4ths *b + (zero4ths *zero4ths )*c + d;
            p0[1] = one4th   *a + zero4ths *b + (one4th   *zero4ths )*c + d;
            p0[2] = two4ths  *a + zero4ths *b + (two4ths  *zero4ths )*c + d;
            p0[3] = three4ths*a + zero4ths *b + (three4ths*zero4ths )*c + d;
            p0 += 4;

            // y = 1/4
            p1[0] = zero4ths *a + one4th   *b + (zero4ths *one4th   )*c + d;
            p1[1] = one4th   *a + one4th   *b + (one4th   *one4th   )*c + d;
            p1[2] = two4ths  *a + one4th   *b + (two4ths  *one4th   )*c + d;
            p1[3] = three4ths*a + one4th   *b + (three4ths*one4th   )*c + d;
            p1 += 4;

            // y = 2/4
            p2[0] = zero4ths *a + two4ths  *b + (zero4ths *two4ths  )*c + d;
            p2[1] = one4th   *a + two4ths  *b + (one4th   *two4ths  )*c + d;
            p2[2] = two4ths  *a + two4ths  *b + (two4ths  *two4ths  )*c + d;
            p2[3] = three4ths*a + two4ths  *b + (three4ths*two4ths  )*c + d;
            p2 += 4;

            // y = 3/4
            p3[0] = zero4ths *a + three4ths*b + (zero4ths *three4ths)*c + d;
            p3[1] = one4th   *a + three4ths*b + (one4th   *three4ths)*c + d;
            p3[2] = two4ths  *a + three4ths*b + (two4ths  *three4ths)*c + d;
            p3[3] = three4ths*a + three4ths*b + (three4ths*three4ths)*c + d;
            p3 += 4;

            pRI += 1;
         }

       // Done with input row, continue to next input row
         pR += nCol;
         pO += 4*IntraColSize;
     }
}

void CreateSubPixelMatrix5x(MTI_UINT16 *pIn, int nCol, double x, double y, int Nx, int Ny, MTI_UINT16 *pOut)

// Computes the fixed (5 levels) subpixel matrix pOut from the position x, y
//
//  Where pIn is apointer to the image
//  N is the extent of the source image (x-N, x+N) x (y-N, y+N)
//  pOut is 5 times bigger
//  pOut MUST exist
//
// Bilinear interpolation is done by
//  f(x,y) = ax + by + cxy + d
//  where 0 <= x,y <= 1 and the coefficients are given by
//  a = -C00 + C10
//  b = -C00 + C01
//  c =  C00 - C10 - C01 + C11
//  d =  C00
//
//******************************************************************************
{
   // Set up the pointer
   MTI_UINT16 *pR = pIn + ((int)y - Ny)*nCol + (int)x - Nx;
   MTI_UINT16 *pO = pOut;
   int IntraColSize = 5*(2*Nx + 1);

   for (int iR = 0; iR < (2*Ny+1); iR++)
     {
       MTI_UINT16 *p0 = pO;
       MTI_UINT16 *p1 = p0 + IntraColSize;
       MTI_UINT16 *p2 = p1 + IntraColSize;
       MTI_UINT16 *p3 = p2 + IntraColSize;
       MTI_UINT16 *p4 = p3 + IntraColSize;

       MTI_UINT16 *pRI = pR;
       for (int iC = 0; iC < (2*Nx+1); iC++)
         {
            double C00 = *pRI;
            double C10 = *(pRI+1);
            double C01 = *(pRI + nCol);
            double C11 = *(pRI + nCol + 1);
            double a = -C00 + C10;
            double b = -C00 + C01;
            double c =  C00 - C10 - C01 + C11;
            double d =  C00;

            #define zero5ths (0.0)
            #define one5th (1.0/5)
            #define two5ths (2.0/5)
            #define three5ths (3.0/5)
            #define four5ths (4.0/5)

            // y = 0/5
            p0[0] = zero5ths *a + zero5ths *b + (zero5ths *zero5ths )*c + d;
            p0[1] = one5th   *a + zero5ths *b + (one5th   *zero5ths )*c + d;
            p0[2] = two5ths  *a + zero5ths *b + (two5ths  *zero5ths )*c + d;
            p0[3] = three5ths*a + zero5ths *b + (three5ths*zero5ths )*c + d;
            p0[4] = four5ths *a + zero5ths *b + (four5ths *zero5ths )*c + d;
            p0 += 5;

            // y = 1/5
            p1[0] = zero5ths *a + one5th   *b + (zero5ths *one5th   )*c + d;
            p1[1] = one5th   *a + one5th   *b + (one5th   *one5th   )*c + d;
            p1[2] = two5ths  *a + one5th   *b + (two5ths  *one5th   )*c + d;
            p1[3] = three5ths*a + one5th   *b + (three5ths*one5th   )*c + d;
            p1[4] = four5ths *a + one5th   *b + (four5ths *one5th   )*c + d;
            p1 += 5;

            // y = 2/5
            p2[0] = zero5ths *a + two5ths  *b + (zero5ths *two5ths  )*c + d;
            p2[1] = one5th   *a + two5ths  *b + (one5th   *two5ths  )*c + d;
            p2[2] = two5ths  *a + two5ths  *b + (two5ths  *two5ths  )*c + d;
            p2[3] = three5ths*a + two5ths  *b + (three5ths*two5ths  )*c + d;
            p2[4] = four5ths *a + two5ths  *b + (four5ths *two5ths  )*c + d;
            p2 += 5;

            // y = 3/5
            p3[0] = zero5ths *a + three5ths*b + (zero5ths *three5ths)*c + d;
            p3[1] = one5th   *a + three5ths*b + (one5th   *three5ths)*c + d;
            p3[2] = two5ths  *a + three5ths*b + (two5ths  *three5ths)*c + d;
            p3[3] = three5ths*a + three5ths*b + (three5ths*three5ths)*c + d;
            p3[4] = four5ths *a + three5ths*b + (four5ths *three5ths)*c + d;
            p3 += 5;

            // y = 4/5
            p4[0] = zero5ths *a + four5ths *b + (zero5ths *four5ths )*c + d;
            p4[1] = one5th   *a + four5ths *b + (one5th   *four5ths )*c + d;
            p4[2] = two5ths  *a + four5ths *b + (two5ths  *four5ths )*c + d;
            p4[3] = three5ths*a + four5ths *b + (three5ths*four5ths )*c + d;
            p4[4] = four5ths *a + four5ths *b + (four5ths *four5ths )*c + d;
            p4 += 5;

            pRI += 1;
         }

       // Done with input row, continue to next input row
         pR += nCol;
         pO += 5*IntraColSize;
     }
}

void CreateSubPixelMatrix6x(MTI_UINT16 *pIn, int nCol, double x, double y, int Nx, int Ny, MTI_UINT16 *pOut)

// Computes the fixed (6 levels) subpixel matrix pOut from the position x, y
//
//  Where pIn is apointer to the image
//  N is the extent of the source image (x-N, x+N) x (y-N, y+N)
//  pOut is 6 times bigger
//  pOut MUST exist
//
// Bilinear interpolation is done by
//  f(x,y) = ax + by + cxy + d
//  where 0 <= x,y <= 1 and the coefficients are given by
//  a = -C00 + C10
//  b = -C00 + C01
//  c =  C00 - C10 - C01 + C11
//  d =  C00
//
//******************************************************************************
{
   // Set up the pointer
   MTI_UINT16 *pR = pIn + ((int)y - Ny)*nCol + (int)x - Nx;
   MTI_UINT16 *pO = pOut;
   int IntraColSize = 6*(2*Nx + 1);

   for (int iR = 0; iR < (2*Ny+1); iR++)
     {
       MTI_UINT16 *p0 = pO;
       MTI_UINT16 *p1 = p0 + IntraColSize;
       MTI_UINT16 *p2 = p1 + IntraColSize;
       MTI_UINT16 *p3 = p2 + IntraColSize;
       MTI_UINT16 *p4 = p3 + IntraColSize;
       MTI_UINT16 *p5 = p4 + IntraColSize;

       MTI_UINT16 *pRI = pR;
       for (int iC = 0; iC < (2*Nx+1); iC++)
         {
            double C00 = *pRI;
            double C10 = *(pRI+1);
            double C01 = *(pRI + nCol);
            double C11 = *(pRI + nCol + 1);
            double a = -C00 + C10;
            double b = -C00 + C01;
            double c =  C00 - C10 - C01 + C11;
            double d =  C00;

            #define zero6ths (0.0)
            #define one6th (1.0/6)
            #define two6ths (2.0/6)
            #define three6ths (3.0/6)
            #define four6ths (4.0/6)
            #define five6ths (5.0/6)

            // y = 0/6
            p0[0] = zero6ths *a + zero6ths *b + (zero6ths *zero6ths )*c + d;
            p0[1] = one6th   *a + zero6ths *b + (one6th   *zero6ths )*c + d;
            p0[2] = two6ths  *a + zero6ths *b + (two6ths  *zero6ths )*c + d;
            p0[3] = three6ths*a + zero6ths *b + (three6ths*zero6ths )*c + d;
            p0[4] = four6ths *a + zero6ths *b + (four6ths *zero6ths )*c + d;
            p0[5] = five6ths *a + zero6ths *b + (five6ths *zero6ths )*c + d;
            p0 += 6;

            // y = 1/6
            p1[0] = zero6ths *a + one6th   *b + (zero6ths *one6th   )*c + d;
            p1[1] = one6th   *a + one6th   *b + (one6th   *one6th   )*c + d;
            p1[2] = two6ths  *a + one6th   *b + (two6ths  *one6th   )*c + d;
            p1[3] = three6ths*a + one6th   *b + (three6ths*one6th   )*c + d;
            p1[4] = four6ths *a + one6th   *b + (four6ths *one6th   )*c + d;
            p1[5] = five6ths *a + one6th   *b + (five6ths *one6th   )*c + d;
            p1 += 6;

            // y = 2/6
            p2[0] = zero6ths *a + two6ths  *b + (zero6ths *two6ths  )*c + d;
            p2[1] = one6th   *a + two6ths  *b + (one6th   *two6ths  )*c + d;
            p2[2] = two6ths  *a + two6ths  *b + (two6ths  *two6ths  )*c + d;
            p2[3] = three6ths*a + two6ths  *b + (three6ths*two6ths  )*c + d;
            p2[4] = four6ths *a + two6ths  *b + (four6ths *two6ths  )*c + d;
            p2[5] = five6ths *a + two6ths  *b + (five6ths *two6ths  )*c + d;
            p2 += 6;

            // y = 3/6
            p3[0] = zero6ths *a + three6ths*b + (zero6ths *three6ths)*c + d;
            p3[1] = one6th   *a + three6ths*b + (one6th   *three6ths)*c + d;
            p3[2] = two6ths  *a + three6ths*b + (two6ths  *three6ths)*c + d;
            p3[3] = three6ths*a + three6ths*b + (three6ths*three6ths)*c + d;
            p3[4] = four6ths *a + three6ths*b + (four6ths *three6ths)*c + d;
            p3[5] = five6ths *a + three6ths*b + (five6ths *three6ths)*c + d;
            p3 += 6;

            // y = 4/6
            p4[0] = zero6ths *a + four6ths *b + (zero6ths *four6ths )*c + d;
            p4[1] = one6th   *a + four6ths *b + (one6th   *four6ths )*c + d;
            p4[2] = two6ths  *a + four6ths *b + (two6ths  *four6ths )*c + d;
            p4[3] = three6ths*a + four6ths *b + (three6ths*four6ths )*c + d;
            p4[4] = four6ths *a + four6ths *b + (four6ths *four6ths )*c + d;
            p4[5] = five6ths *a + four6ths *b + (five6ths *four6ths )*c + d;
            p4 += 6;

            // y = 5/6
            p5[0] = zero6ths *a + five6ths *b + (zero6ths *five6ths )*c + d;
            p5[1] = one6th   *a + five6ths *b + (one6th   *five6ths )*c + d;
            p5[2] = two6ths  *a + five6ths *b + (two6ths  *five6ths )*c + d;
            p5[3] = three6ths*a + five6ths *b + (three6ths*five6ths )*c + d;
            p5[4] = four6ths *a + five6ths *b + (four6ths *five6ths )*c + d;
            p5[5] = five6ths *a + five6ths *b + (five6ths *five6ths )*c + d;
            p5 += 6;

            pRI += 1;
         }

       // Done with input row, continue to next input row
         pR += nCol;
         pO += 6*IntraColSize;
     }
}

void CreateSubPixelMatrix7x(MTI_UINT16 *pIn, int nCol, double x, double y, int Nx, int Ny, MTI_UINT16 *pOut)

// Computes the fixed (7 levels) subpixel matrix pOut from the position x, y
//
//  Where pIn is apointer to the image
//  N is the extent of the source image (x-N, x+N) x (y-N, y+N)
//  pOut is 7 times bigger
//  pOut MUST exist
//
// Bilinear interpolation is done by
//  f(x,y) = ax + by + cxy + d
//  where 0 <= x,y <= 1 and the coefficients are given by
//  a = -C00 + C10
//  b = -C00 + C01
//  c =  C00 - C10 - C01 + C11
//  d =  C00
//
//******************************************************************************
{
   // Set up the pointer
   MTI_UINT16 *pR = pIn + ((int)y - Ny)*nCol + (int)x - Nx;
   MTI_UINT16 *pO = pOut;
   int IntraColSize = 7*(2*Nx + 1);

   for (int iR = 0; iR < (2*Ny+1); iR++)
     {
       MTI_UINT16 *p0 = pO;
       MTI_UINT16 *p1 = p0 + IntraColSize;
       MTI_UINT16 *p2 = p1 + IntraColSize;
       MTI_UINT16 *p3 = p2 + IntraColSize;
       MTI_UINT16 *p4 = p3 + IntraColSize;
       MTI_UINT16 *p5 = p4 + IntraColSize;
       MTI_UINT16 *p6 = p5 + IntraColSize;

       MTI_UINT16 *pRI = pR;
       for (int iC = 0; iC < (2*Nx+1); iC++)
         {
            double C00 = *pRI;
            double C10 = *(pRI+1);
            double C01 = *(pRI + nCol);
            double C11 = *(pRI + nCol + 1);
            double a = -C00 + C10;
            double b = -C00 + C01;
            double c =  C00 - C10 - C01 + C11;
            double d =  C00;

            #define zero7ths (0.0)
            #define one7th (1.0/7)
            #define two7ths (2.0/7)
            #define three7ths (3.0/7)
            #define four7ths (4.0/7)
            #define five7ths (5.0/7)
            #define six7ths (6.0/7)

            // y = 0/7
            p0[0] = zero7ths *a + zero7ths *b + (zero7ths *zero7ths )*c + d;
            p0[1] = one7th   *a + zero7ths *b + (one7th   *zero7ths )*c + d;
            p0[2] = two7ths  *a + zero7ths *b + (two7ths  *zero7ths )*c + d;
            p0[3] = three7ths*a + zero7ths *b + (three7ths*zero7ths )*c + d;
            p0[4] = four7ths *a + zero7ths *b + (four7ths *zero7ths )*c + d;
            p0[5] = five7ths *a + zero7ths *b + (five7ths *zero7ths )*c + d;
            p0[6] = six7ths  *a + zero7ths *b + (six7ths  *zero7ths )*c + d;
            p0 += 7;

            // y = 1/7
            p1[0] = zero7ths *a + one7th   *b + (zero7ths *one7th   )*c + d;
            p1[1] = one7th   *a + one7th   *b + (one7th   *one7th   )*c + d;
            p1[2] = two7ths  *a + one7th   *b + (two7ths  *one7th   )*c + d;
            p1[3] = three7ths*a + one7th   *b + (three7ths*one7th   )*c + d;
            p1[4] = four7ths *a + one7th   *b + (four7ths *one7th   )*c + d;
            p1[5] = five7ths *a + one7th   *b + (five7ths *one7th   )*c + d;
            p1[6] = six7ths  *a + one7th   *b + (six7ths  *one7th   )*c + d;
            p1 += 7;

            // y = 2/7
            p2[0] = zero7ths *a + two7ths  *b + (zero7ths *two7ths  )*c + d;
            p2[1] = one7th   *a + two7ths  *b + (one7th   *two7ths  )*c + d;
            p2[2] = two7ths  *a + two7ths  *b + (two7ths  *two7ths  )*c + d;
            p2[3] = three7ths*a + two7ths  *b + (three7ths*two7ths  )*c + d;
            p2[4] = four7ths *a + two7ths  *b + (four7ths *two7ths  )*c + d;
            p2[5] = five7ths *a + two7ths  *b + (five7ths *two7ths  )*c + d;
            p2[6] = six7ths  *a + two7ths  *b + (six7ths  *two7ths  )*c + d;
            p2 += 7;

            // y = 3/7
            p3[0] = zero7ths *a + three7ths*b + (zero7ths *three7ths)*c + d;
            p3[1] = one7th   *a + three7ths*b + (one7th   *three7ths)*c + d;
            p3[2] = two7ths  *a + three7ths*b + (two7ths  *three7ths)*c + d;
            p3[3] = three7ths*a + three7ths*b + (three7ths*three7ths)*c + d;
            p3[4] = four7ths *a + three7ths*b + (four7ths *three7ths)*c + d;
            p3[5] = five7ths *a + three7ths*b + (five7ths *three7ths)*c + d;
            p3[6] = six7ths  *a + three7ths*b + (six7ths  *three7ths)*c + d;
            p3 += 7;

            // y = 4/7
            p4[0] = zero7ths *a + four7ths *b + (zero7ths *four7ths )*c + d;
            p4[1] = one7th   *a + four7ths *b + (one7th   *four7ths )*c + d;
            p4[2] = two7ths  *a + four7ths *b + (two7ths  *four7ths )*c + d;
            p4[3] = three7ths*a + four7ths *b + (three7ths*four7ths )*c + d;
            p4[4] = four7ths *a + four7ths *b + (four7ths *four7ths )*c + d;
            p4[5] = five7ths *a + four7ths *b + (five7ths *four7ths )*c + d;
            p4[6] = six7ths  *a + four7ths *b + (six7ths  *four7ths )*c + d;
            p4 += 7;

            // y = 5/7
            p5[0] = zero7ths *a + five7ths *b + (zero7ths *five7ths )*c + d;
            p5[1] = one7th   *a + five7ths *b + (one7th   *five7ths )*c + d;
            p5[2] = two7ths  *a + five7ths *b + (two7ths  *five7ths )*c + d;
            p5[3] = three7ths*a + five7ths *b + (three7ths*five7ths )*c + d;
            p5[4] = four7ths *a + five7ths *b + (four7ths *five7ths )*c + d;
            p5[5] = five7ths *a + five7ths *b + (five7ths *five7ths )*c + d;
            p5[6] = six7ths  *a + five7ths *b + (six7ths  *five7ths )*c + d;
            p5 += 7;

            // y = 6/7
            p6[0] = zero7ths *a + six7ths  *b + (zero7ths *six7ths  )*c + d;
            p6[1] = one7th   *a + six7ths  *b + (one7th   *six7ths  )*c + d;
            p6[2] = two7ths  *a + six7ths  *b + (two7ths  *six7ths  )*c + d;
            p6[3] = three7ths*a + six7ths  *b + (three7ths*six7ths  )*c + d;
            p6[4] = four7ths *a + six7ths  *b + (four7ths *six7ths  )*c + d;
            p6[5] = five7ths *a + six7ths  *b + (five7ths *six7ths  )*c + d;
            p6[6] = six7ths  *a + six7ths  *b + (six7ths  *six7ths  )*c + d;
            p6 += 7;

            pRI += 1;
         }

       // Done with input row, continue to next input row
         pR += nCol;
         pO += 7*IntraColSize;
     }
}

void CreateSubPixelMatrix8x(MTI_UINT16 *pIn, int nCol, double x, double y, int Nx, int Ny, MTI_UINT16 *pOut)

// Computes the fixed (8 levels) subpixel matrix pOut from the position x, y
//
//  Where pIn is apointer to the image
//  N is the extent of the source image (x-N, x+N) x (y-N, y+N)
//  pOut is 8 times bigger
//  pOut MUST exist
//
// Bilinear interpolation is done by
//  f(x,y) = ax + by + cxy + d
//  where 0 <= x,y <= 1 and the coefficients are given by
//  a = -C00 + C10
//  b = -C00 + C01
//  c =  C00 - C10 - C01 + C11
//  d =  C00
//
//******************************************************************************
{
   // Set up the pointer
   MTI_UINT16 *pR = pIn + ((int)y - Ny)*nCol + (int)x - Nx;
   MTI_UINT16 *pO = pOut;
   int IntraColSize = 8*(2*Nx + 1);

   for (int iR = 0; iR < (2*Ny+1); iR++)
     {
       MTI_UINT16 *p0 = pO;
       MTI_UINT16 *p1 = p0 + IntraColSize;
       MTI_UINT16 *p2 = p1 + IntraColSize;
       MTI_UINT16 *p3 = p2 + IntraColSize;
       MTI_UINT16 *p4 = p3 + IntraColSize;
       MTI_UINT16 *p5 = p4 + IntraColSize;
       MTI_UINT16 *p6 = p5 + IntraColSize;
       MTI_UINT16 *p7 = p6 + IntraColSize;

       MTI_UINT16 *pRI = pR;
       for (int iC = 0; iC < (2*Nx+1); iC++)
         {
            double C00 = *pRI;
            double C10 = *(pRI+1);
            double C01 = *(pRI + nCol);
            double C11 = *(pRI + nCol + 1);
            double a = -C00 + C10;
            double b = -C00 + C01;
            double c =  C00 - C10 - C01 + C11;
            double d =  C00;

            #define zero8ths  (0.0)
            #define one8th    (1.0/8)
            #define two8ths   (2.0/8)
            #define three8ths (3.0/8)
            #define four8ths  (4.0/8)
            #define five8ths  (5.0/8)
            #define six8ths   (6.0/8)
            #define seven8ths (7.0/8)

            // y = 0/8
            p0[0] = zero8ths *a + zero8ths *b + (zero8ths *zero8ths )*c + d;
            p0[1] = one8th   *a + zero8ths *b + (one8th   *zero8ths )*c + d;
            p0[2] = two8ths  *a + zero8ths *b + (two8ths  *zero8ths )*c + d;
            p0[3] = three8ths*a + zero8ths *b + (three8ths*zero8ths )*c + d;
            p0[4] = four8ths *a + zero8ths *b + (four8ths *zero8ths )*c + d;
            p0[5] = five8ths *a + zero8ths *b + (five8ths *zero8ths )*c + d;
            p0[6] = six8ths  *a + zero8ths *b + (six8ths  *zero8ths )*c + d;
            p0[7] = seven8ths*a + zero8ths *b + (seven8ths*zero8ths )*c + d;
            p0 += 8;

            // y = 1/8
            p1[0] = zero8ths *a + one8th   *b + (zero8ths *one8th   )*c + d;
            p1[1] = one8th   *a + one8th   *b + (one8th   *one8th   )*c + d;
            p1[2] = two8ths  *a + one8th   *b + (two8ths  *one8th   )*c + d;
            p1[3] = three8ths*a + one8th   *b + (three8ths*one8th   )*c + d;
            p1[4] = four8ths *a + one8th   *b + (four8ths *one8th   )*c + d;
            p1[5] = five8ths *a + one8th   *b + (five8ths *one8th   )*c + d;
            p1[6] = six8ths  *a + one8th   *b + (six8ths  *one8th   )*c + d;
            p1[7] = seven8ths*a + one8th   *b + (seven8ths*one8th   )*c + d;
            p1 += 8;

            // y = 2/8
            p2[0] = zero8ths *a + two8ths  *b + (zero8ths *two8ths  )*c + d;
            p2[1] = one8th   *a + two8ths  *b + (one8th   *two8ths  )*c + d;
            p2[2] = two8ths  *a + two8ths  *b + (two8ths  *two8ths  )*c + d;
            p2[3] = three8ths*a + two8ths  *b + (three8ths*two8ths  )*c + d;
            p2[4] = four8ths *a + two8ths  *b + (four8ths *two8ths  )*c + d;
            p2[5] = five8ths *a + two8ths  *b + (five8ths *two8ths  )*c + d;
            p2[6] = six8ths  *a + two8ths  *b + (six8ths  *two8ths  )*c + d;
            p2[7] = seven8ths*a + two8ths  *b + (seven8ths*two8ths  )*c + d;
            p2 += 8;

            // y = 3/8
            p3[0] = zero8ths *a + three8ths*b + (zero8ths *three8ths)*c + d;
            p3[1] = one8th   *a + three8ths*b + (one8th   *three8ths)*c + d;
            p3[2] = two8ths  *a + three8ths*b + (two8ths  *three8ths)*c + d;
            p3[3] = three8ths*a + three8ths*b + (three8ths*three8ths)*c + d;
            p3[4] = four8ths *a + three8ths*b + (four8ths *three8ths)*c + d;
            p3[5] = five8ths *a + three8ths*b + (five8ths *three8ths)*c + d;
            p3[6] = six8ths  *a + three8ths*b + (six8ths  *three8ths)*c + d;
            p3[7] = seven8ths*a + three8ths*b + (seven8ths*three8ths)*c + d;
            p3 += 8;

            // y = 4/8
            p4[0] = zero8ths *a + four8ths *b + (zero8ths *four8ths )*c + d;
            p4[1] = one8th   *a + four8ths *b + (one8th   *four8ths )*c + d;
            p4[2] = two8ths  *a + four8ths *b + (two8ths  *four8ths )*c + d;
            p4[3] = three8ths*a + four8ths *b + (three8ths*four8ths )*c + d;
            p4[4] = four8ths *a + four8ths *b + (four8ths *four8ths )*c + d;
            p4[5] = five8ths *a + four8ths *b + (five8ths *four8ths )*c + d;
            p4[6] = six8ths  *a + four8ths *b + (six8ths  *four8ths )*c + d;
            p4[7] = seven8ths*a + four8ths *b + (seven8ths*four8ths )*c + d;
            p4 += 8;

            // y = 5/8
            p5[0] = zero8ths *a + five8ths *b + (zero8ths *five8ths )*c + d;
            p5[1] = one8th   *a + five8ths *b + (one8th   *five8ths )*c + d;
            p5[2] = two8ths  *a + five8ths *b + (two8ths  *five8ths )*c + d;
            p5[3] = three8ths*a + five8ths *b + (three8ths*five8ths )*c + d;
            p5[4] = four8ths *a + five8ths *b + (four8ths *five8ths )*c + d;
            p5[5] = five8ths *a + five8ths *b + (five8ths *five8ths )*c + d;
            p5[6] = six8ths  *a + five8ths *b + (six8ths  *five8ths )*c + d;
            p5[7] = seven8ths*a + five8ths *b + (seven8ths*five8ths )*c + d;
            p5 += 8;

            // y = 6/8
            p6[0] = zero8ths *a + six8ths  *b + (zero8ths *six8ths  )*c + d;
            p6[1] = one8th   *a + six8ths  *b + (one8th   *six8ths  )*c + d;
            p6[2] = two8ths  *a + six8ths  *b + (two8ths  *six8ths  )*c + d;
            p6[3] = three8ths*a + six8ths  *b + (three8ths*six8ths  )*c + d;
            p6[4] = four8ths *a + six8ths  *b + (four8ths *six8ths  )*c + d;
            p6[5] = five8ths *a + six8ths  *b + (five8ths *six8ths  )*c + d;
            p6[6] = six8ths  *a + six8ths  *b + (six8ths  *six8ths  )*c + d;
            p6[7] = seven8ths*a + six8ths  *b + (seven8ths*six8ths  )*c + d;
            p6 += 8;

            // y = 7/8
            p7[0] = zero8ths *a + seven8ths*b + (zero8ths *seven8ths)*c + d;
            p7[1] = one8th   *a + seven8ths*b + (one8th   *seven8ths)*c + d;
            p7[2] = two8ths  *a + seven8ths*b + (two8ths  *seven8ths)*c + d;
            p7[3] = three8ths*a + seven8ths*b + (three8ths*seven8ths)*c + d;
            p7[4] = four8ths *a + seven8ths*b + (four8ths *seven8ths)*c + d;
            p7[5] = five8ths *a + seven8ths*b + (five8ths *seven8ths)*c + d;
            p7[6] = six8ths  *a + seven8ths*b + (six8ths  *seven8ths)*c + d;
            p7[7] = seven8ths*a + seven8ths*b + (seven8ths*seven8ths)*c + d;
            p7 += 8;

            pRI += 1;
         }

       // Done with input row, continue to next input row
         pR += nCol;
         pO += 8*IntraColSize;
     }
}

#if __OLD_CRAP__
void CreateSubPixelMatrix8x(MTI_UINT16 *pIn, int nCol, double x, double y, int Nx, int Ny, MTI_UINT16 *pOut)

// Computes the fixed (8 levels) subpixel matrix pOut from the position x, y
//
//  Where pIn is apointer to the image
//  N is the extent of the source image (x-N, x+N) x (y-N, y+N)
//  pOut is 8 times bigger
//  pOut MUST exist
//
// Bilinear interpolation is done by
//  f(x,y) = ax + by + cxy + d
//  where 0 <= x,y <= 1 and the coefficients are given by
//  a = -C00 + C10
//  b = -C00 + C01
//  c =  C00 - C10 - C01 + C11
//  d =  C00
//
//******************************************************************************
{
   // Set up the pointer
   MTI_UINT16 *pR = pIn + ((int)y - Ny)*nCol + (int)x - Nx;
   MTI_UINT16 *pO = pOut;
   int IntraColSize = 8*(2*Nx + 1);

   for (int iR = 0; iR < (2*Ny+1); iR++)
     {
       MTI_UINT16 *p0 = pO;
       MTI_UINT16 *p1 = p0 + IntraColSize;
       MTI_UINT16 *p2 = p1 + IntraColSize;
       MTI_UINT16 *p3 = p2 + IntraColSize;
       MTI_UINT16 *p4 = p3 + IntraColSize;
       MTI_UINT16 *p5 = p4 + IntraColSize;
       MTI_UINT16 *p6 = p5 + IntraColSize;
       MTI_UINT16 *p7 = p6 + IntraColSize;

       MTI_UINT16 *pRI = pR;
       for (int iC = 0; iC < (2*Nx+1); iC++)
         {
            double C00 = *pRI;
            double C10 = *(pRI+1);
            double C01 = *(pRI + nCol);
            double C11 = *(pRI + nCol + 1);
            double a = -C00 + C10;
            double b = -C00 + C01;
            double c =  C00 - C10 - C01 + C11;
            double d =  C00;

            // y = .000
            p0[0] = 0.000*a + 0.000*b + (0.000*0.000)*c + d;
            p0[1] = 0.125*a + 0.000*b + (0.125*0.000)*c + d;
            p0[2] = 0.250*a + 0.000*b + (0.250*0.000)*c + d;
            p0[3] = 0.375*a + 0.000*b + (0.375*0.000)*c + d;
            p0[4] = 0.500*a + 0.000*b + (0.500*0.000)*c + d;
            p0[5] = 0.625*a + 0.000*b + (0.625*0.000)*c + d;
            p0[6] = 0.750*a + 0.000*b + (0.750*0.000)*c + d;
            p0[6] = 0.875*a + 0.000*b + (0.875*0.000)*c + d;
            p0 += 8;

            // y = .125
            p1[0] = 0.000*a + 0.125*b + (0.000*0.125)*c + d;
            p1[1] = 0.125*a + 0.125*b + (0.125*0.125)*c + d;
            p1[2] = 0.250*a + 0.125*b + (0.250*0.125)*c + d;
            p1[3] = 0.375*a + 0.125*b + (0.375*0.125)*c + d;
            p1[4] = 0.500*a + 0.125*b + (0.500*0.125)*c + d;
            p1[5] = 0.625*a + 0.125*b + (0.625*0.125)*c + d;
            p1[6] = 0.750*a + 0.125*b + (0.750*0.125)*c + d;
            p1[7] = 0.875*a + 0.125*b + (0.875*0.125)*c + d;
            p1 += 8;

            // y = .250
            p2[0] = 0.000*a + 0.250*b + (0.000*0.250)*c + d;
            p2[1] = 0.125*a + 0.250*b + (0.125*0.250)*c + d;
            p2[2] = 0.250*a + 0.250*b + (0.250*0.250)*c + d;
            p2[3] = 0.375*a + 0.250*b + (0.375*0.250)*c + d;
            p2[4] = 0.500*a + 0.250*b + (0.500*0.250)*c + d;
            p2[5] = 0.625*a + 0.250*b + (0.625*0.250)*c + d;
            p2[6] = 0.750*a + 0.250*b + (0.750*0.250)*c + d;
            p2[7] = 0.875*a + 0.250*b + (0.875*0.250)*c + d;
            p2 += 8;

            // y = .375
            p3[0] = 0.000*a + 0.375*b + (0.000*0.375)*c + d;
            p3[1] = 0.125*a + 0.375*b + (0.125*0.375)*c + d;
            p3[2] = 0.250*a + 0.375*b + (0.250*0.375)*c + d;
            p3[3] = 0.375*a + 0.375*b + (0.375*0.375)*c + d;
            p3[4] = 0.500*a + 0.375*b + (0.500*0.375)*c + d;
            p3[5] = 0.625*a + 0.375*b + (0.625*0.375)*c + d;
            p3[6] = 0.750*a + 0.375*b + (0.750*0.375)*c + d;
            p3[7] = 0.875*a + 0.375*b + (0.875*0.375)*c + d;
            p3 += 8;

            // y = .500
            p4[0] = 0.000*a + 0.500*b + (0.000*0.500)*c + d;
            p4[1] = 0.125*a + 0.500*b + (0.125*0.500)*c + d;
            p4[2] = 0.250*a + 0.500*b + (0.250*0.500)*c + d;
            p4[3] = 0.375*a + 0.500*b + (0.375*0.500)*c + d;
            p4[4] = 0.500*a + 0.500*b + (0.500*0.500)*c + d;
            p4[5] = 0.625*a + 0.500*b + (0.625*0.500)*c + d;
            p4[6] = 0.750*a + 0.500*b + (0.750*0.500)*c + d;
            p4[7] = 0.875*a + 0.500*b + (0.875*0.500)*c + d;
            p4 += 8;

            // y = .625
            p5[0] = 0.000*a + 0.625*b + (0.000*0.625)*c + d;
            p5[1] = 0.125*a + 0.625*b + (0.125*0.625)*c + d;
            p5[2] = 0.250*a + 0.625*b + (0.250*0.625)*c + d;
            p5[3] = 0.375*a + 0.625*b + (0.375*0.625)*c + d;
            p5[4] = 0.500*a + 0.625*b + (0.500*0.625)*c + d;
            p5[5] = 0.625*a + 0.625*b + (0.625*0.625)*c + d;
            p5[6] = 0.750*a + 0.625*b + (0.750*0.625)*c + d;
            p5[7] = 0.875*a + 0.625*b + (0.875*0.625)*c + d;
            p5 += 8;

            // y = .750
            p6[0] = 0.000*a + 0.750*b + (0.000*0.750)*c + d;
            p6[1] = 0.125*a + 0.750*b + (0.125*0.750)*c + d;
            p6[2] = 0.250*a + 0.750*b + (0.250*0.750)*c + d;
            p6[3] = 0.375*a + 0.750*b + (0.375*0.750)*c + d;
            p6[4] = 0.500*a + 0.750*b + (0.500*0.750)*c + d;
            p6[5] = 0.625*a + 0.750*b + (0.625*0.750)*c + d;
            p6[6] = 0.750*a + 0.750*b + (0.750*0.750)*c + d;
            p6[7] = 0.875*a + 0.750*b + (0.875*0.750)*c + d;
            p6 += 8;

            // y = .875
            p7[0] = 0.000*a + 0.875*b + (0.000*0.875)*c + d;
            p7[1] = 0.125*a + 0.875*b + (0.125*0.875)*c + d;
            p7[2] = 0.250*a + 0.875*b + (0.250*0.875)*c + d;
            p7[3] = 0.375*a + 0.875*b + (0.375*0.875)*c + d;
            p7[4] = 0.500*a + 0.875*b + (0.500*0.875)*c + d;
            p7[5] = 0.625*a + 0.875*b + (0.625*0.875)*c + d;
            p7[6] = 0.750*a + 0.875*b + (0.750*0.875)*c + d;
            p7[7] = 0.875*a + 0.875*b + (0.875*0.875)*c + d;
            p7 += 8;

            pRI += 1;
         }

       // Done with input row, continue to next input row
         pR += nCol;
         pO += 8*IntraColSize;
     }
}
#endif // _OLD_CRAP_

void CreateSubPixelMatrix10x(MTI_UINT16 *pIn, int nCol, double x, double y, int Nx, int Ny, MTI_UINT16 *pOut)

// Computes the fixed (10 levels) subpixel matrix pOut from the position x, y
//
//  Where pIn is apointer to the image
//  N is the extent of the source image (x-N, x+N) x (y-N, y+N)
//  pOut is 10 times bigger
//  pOut MUST exist
//
// Bilinear interpolation is done by
//  f(x,y) = ax + by + cxy + d
//  where 0 <= x,y <= 1 and the coefficients are given by
//  a = -C00 + C10
//  b = -C00 + C01
//  c =  C00 - C10 - C01 + C11
//  d =  C00
//
//******************************************************************************
{
   // Set up the pointer
   MTI_UINT16 *pR = pIn + ((int)y - Ny)*nCol + (int)x - Nx;
   MTI_UINT16 *pO = pOut;
   int IntraColSize = 10*(2*Nx + 1);

   for (int iR = 0; iR < (2*Ny+1); iR++)
     {
       MTI_UINT16 *p0 = pO;
       MTI_UINT16 *p1 = p0 + IntraColSize;
       MTI_UINT16 *p2 = p1 + IntraColSize;
       MTI_UINT16 *p3 = p2 + IntraColSize;
       MTI_UINT16 *p4 = p3 + IntraColSize;
       MTI_UINT16 *p5 = p4 + IntraColSize;
       MTI_UINT16 *p6 = p5 + IntraColSize;
       MTI_UINT16 *p7 = p6 + IntraColSize;
       MTI_UINT16 *p8 = p7 + IntraColSize;
       MTI_UINT16 *p9 = p8 + IntraColSize;

       MTI_UINT16 *pRI = pR;
       for (int iC = 0; iC < (2*Nx+1); iC++)
         {
            double C00 = *pRI;
            double C10 = *(pRI+1);
            double C01 = *(pRI + nCol);
            double C11 = *(pRI + nCol + 1);
            double a = -C00 + C10;
            double b = -C00 + C01;
            double c =  C00 - C10 - C01 + C11;
            double d =  C00;

#if 1 //def __OLD_WAY__
            // y = .0
            p0[0] = 0.0*a + 0.0*b + (0.0*0.0)*c + d;
            p0[1] = 0.1*a + 0.0*b + (0.1*0.0)*c + d;
            p0[2] = 0.2*a + 0.0*b + (0.2*0.0)*c + d;
            p0[3] = 0.3*a + 0.0*b + (0.3*0.0)*c + d;
            p0[4] = 0.4*a + 0.0*b + (0.4*0.0)*c + d;
            p0[5] = 0.5*a + 0.0*b + (0.5*0.0)*c + d;
            p0[6] = 0.6*a + 0.0*b + (0.6*0.0)*c + d;
            p0[7] = 0.7*a + 0.0*b + (0.7*0.0)*c + d;
            p0[8] = 0.8*a + 0.0*b + (0.8*0.0)*c + d;
            p0[9] = 0.9*a + 0.0*b + (0.9*0.0)*c + d;
            p0 += 10;

            // y = .1
            p1[0] = 0.0*a + 0.1*b + (0.0*0.1)*c + d;
            p1[1] = 0.1*a + 0.1*b + (0.1*0.1)*c + d;
            p1[2] = 0.2*a + 0.1*b + (0.2*0.1)*c + d;
            p1[3] = 0.3*a + 0.1*b + (0.3*0.1)*c + d;
            p1[4] = 0.4*a + 0.1*b + (0.4*0.1)*c + d;
            p1[5] = 0.5*a + 0.1*b + (0.5*0.1)*c + d;
            p1[6] = 0.6*a + 0.1*b + (0.6*0.1)*c + d;
            p1[7] = 0.7*a + 0.1*b + (0.7*0.1)*c + d;
            p1[8] = 0.8*a + 0.1*b + (0.8*0.1)*c + d;
            p1[9] = 0.9*a + 0.1*b + (0.9*0.1)*c + d;
            p1 += 10;

            // y = .2
            p2[0] = 0.0*a + 0.2*b + (0.0*0.2)*c + d;
            p2[1] = 0.1*a + 0.2*b + (0.1*0.2)*c + d;
            p2[2] = 0.2*a + 0.2*b + (0.2*0.2)*c + d;
            p2[3] = 0.3*a + 0.2*b + (0.3*0.2)*c + d;
            p2[4] = 0.4*a + 0.2*b + (0.4*0.2)*c + d;
            p2[5] = 0.5*a + 0.2*b + (0.5*0.2)*c + d;
            p2[6] = 0.6*a + 0.2*b + (0.6*0.2)*c + d;
            p2[7] = 0.7*a + 0.2*b + (0.7*0.2)*c + d;
            p2[8] = 0.8*a + 0.2*b + (0.8*0.2)*c + d;
            p2[9] = 0.9*a + 0.2*b + (0.9*0.2)*c + d;
            p2 += 10;

            // y = .3
            p3[0] = 0.0*a + 0.3*b + (0.0*0.3)*c + d;
            p3[1] = 0.1*a + 0.3*b + (0.1*0.3)*c + d;
            p3[2] = 0.2*a + 0.3*b + (0.2*0.3)*c + d;
            p3[3] = 0.3*a + 0.3*b + (0.3*0.3)*c + d;
            p3[4] = 0.4*a + 0.3*b + (0.4*0.3)*c + d;
            p3[5] = 0.5*a + 0.3*b + (0.5*0.3)*c + d;
            p3[6] = 0.6*a + 0.3*b + (0.6*0.3)*c + d;
            p3[7] = 0.7*a + 0.3*b + (0.7*0.3)*c + d;
            p3[8] = 0.8*a + 0.3*b + (0.8*0.3)*c + d;
            p3[9] = 0.9*a + 0.3*b + (0.9*0.3)*c + d;
            p3 += 10;

            // y = .4
            p4[0] = 0.0*a + 0.4*b + (0.0*0.4)*c + d;
            p4[1] = 0.1*a + 0.4*b + (0.1*0.4)*c + d;
            p4[2] = 0.2*a + 0.4*b + (0.2*0.4)*c + d;
            p4[3] = 0.3*a + 0.4*b + (0.3*0.4)*c + d;
            p4[4] = 0.4*a + 0.4*b + (0.4*0.4)*c + d;
            p4[5] = 0.5*a + 0.4*b + (0.5*0.4)*c + d;
            p4[6] = 0.6*a + 0.4*b + (0.6*0.4)*c + d;
            p4[7] = 0.7*a + 0.4*b + (0.7*0.4)*c + d;
            p4[8] = 0.8*a + 0.4*b + (0.8*0.4)*c + d;
            p4[9] = 0.9*a + 0.4*b + (0.9*0.4)*c + d;
            p4 += 10;

            // y = .5
            p5[0] = 0.0*a + 0.5*b + (0.0*0.5)*c + d;
            p5[1] = 0.1*a + 0.5*b + (0.1*0.5)*c + d;
            p5[2] = 0.2*a + 0.5*b + (0.2*0.5)*c + d;
            p5[3] = 0.3*a + 0.5*b + (0.3*0.5)*c + d;
            p5[4] = 0.4*a + 0.5*b + (0.4*0.5)*c + d;
            p5[5] = 0.5*a + 0.5*b + (0.5*0.5)*c + d;
            p5[6] = 0.6*a + 0.5*b + (0.6*0.5)*c + d;
            p5[7] = 0.7*a + 0.5*b + (0.7*0.5)*c + d;
            p5[8] = 0.8*a + 0.5*b + (0.8*0.5)*c + d;
            p5[9] = 0.9*a + 0.5*b + (0.9*0.5)*c + d;
            p5 += 10;

            // y = .6
            p6[0] = 0.0*a + 0.6*b + (0.0*0.6)*c + d;
            p6[1] = 0.1*a + 0.6*b + (0.1*0.6)*c + d;
            p6[2] = 0.2*a + 0.6*b + (0.2*0.6)*c + d;
            p6[3] = 0.3*a + 0.6*b + (0.3*0.6)*c + d;
            p6[4] = 0.4*a + 0.6*b + (0.4*0.6)*c + d;
            p6[5] = 0.5*a + 0.6*b + (0.5*0.6)*c + d;
            p6[6] = 0.6*a + 0.6*b + (0.6*0.6)*c + d;
            p6[7] = 0.7*a + 0.6*b + (0.7*0.6)*c + d;
            p6[8] = 0.8*a + 0.6*b + (0.8*0.6)*c + d;
            p6[9] = 0.9*a + 0.6*b + (0.9*0.6)*c + d;
            p6 += 10;

            // y = .7
            p7[0] = 0.0*a + 0.7*b + (0.0*0.7)*c + d;
            p7[1] = 0.1*a + 0.7*b + (0.1*0.7)*c + d;
            p7[2] = 0.2*a + 0.7*b + (0.2*0.7)*c + d;
            p7[3] = 0.3*a + 0.7*b + (0.3*0.7)*c + d;
            p7[4] = 0.4*a + 0.7*b + (0.4*0.7)*c + d;
            p7[5] = 0.5*a + 0.7*b + (0.5*0.7)*c + d;
            p7[6] = 0.6*a + 0.7*b + (0.6*0.7)*c + d;
            p7[7] = 0.7*a + 0.7*b + (0.7*0.7)*c + d;
            p7[8] = 0.8*a + 0.7*b + (0.8*0.7)*c + d;
            p7[9] = 0.9*a + 0.7*b + (0.9*0.7)*c + d;
            p7 += 10;

            // y = .8
            p8[0] = 0.0*a + 0.8*b + (0.0*0.8)*c + d;
            p8[1] = 0.1*a + 0.8*b + (0.1*0.8)*c + d;
            p8[2] = 0.2*a + 0.8*b + (0.2*0.8)*c + d;
            p8[3] = 0.3*a + 0.8*b + (0.3*0.8)*c + d;
            p8[4] = 0.4*a + 0.8*b + (0.4*0.8)*c + d;
            p8[5] = 0.5*a + 0.8*b + (0.5*0.8)*c + d;
            p8[6] = 0.6*a + 0.8*b + (0.6*0.8)*c + d;
            p8[7] = 0.7*a + 0.8*b + (0.7*0.8)*c + d;
            p8[8] = 0.8*a + 0.8*b + (0.8*0.8)*c + d;
            p8[9] = 0.9*a + 0.8*b + (0.9*0.8)*c + d;
            p8 += 10;

            // y = .9
            p9[0] = 0.0*a + 0.9*b + (0.0*0.9)*c + d;
            p9[1] = 0.1*a + 0.9*b + (0.1*0.9)*c + d;
            p9[2] = 0.2*a + 0.9*b + (0.2*0.9)*c + d;
            p9[3] = 0.3*a + 0.9*b + (0.3*0.9)*c + d;
            p9[4] = 0.4*a + 0.9*b + (0.4*0.9)*c + d;
            p9[5] = 0.5*a + 0.9*b + (0.5*0.9)*c + d;
            p9[6] = 0.6*a + 0.9*b + (0.6*0.9)*c + d;
            p9[7] = 0.7*a + 0.9*b + (0.7*0.9)*c + d;
            p9[8] = 0.8*a + 0.9*b + (0.8*0.9)*c + d;
            p9[9] = 0.9*a + 0.9*b + (0.9*0.9)*c + d;
            p9 += 10;

#else // new way
            double val, incr;
            int i;

            // y = .0
            val = 0.0*a + 0.0*b + (0.0*0.0)*c + d;
            incr = 0.1*a + (0.1*0.0)*c;
            p0[0] = val;
            for (i = 1; i < 10; ++i)
               p0[i] = (val += incr);
            p0 += 10;

            // y = .1
            val = 0.0*a + 0.1*b + (0.0*0.1)*c + d;
            incr = 0.1*a + (0.1*0.1)*c;
            p1[0] = val;
            for (i = 1; i < 10; ++i)
               p1[i] = (val += incr);
            p1 += 10;

            // y = .2
            val = 0.0*a + 0.2*b + (0.0*0.2)*c + d;
            incr = 0.1*a + (0.1*0.2)*c;
            p2[0] = val;
            for (i = 1; i < 10; ++i)
               p2[i] = (val += incr);
            p2 += 10;

            // y = .3
            val = 0.0*a + 0.3*b + (0.0*0.3)*c + d;
            incr = 0.1*a + (0.1*0.3)*c;
            p3[0] = val;
            for (i = 1; i < 10; ++i)
               p3[i] = (val += incr);
            p2 += 10;

            // y = .4
            val = 0.0*a + 0.4*b + (0.0*0.4)*c + d;
            incr = 0.1*a + (0.1*0.4)*c;
            p4[0] = val;
            for (i = 1; i < 10; ++i)
               p4[i] = (val += incr);
            p4 += 10;

            // y = .5
            val = 0.0*a + 0.5*b + (0.0*0.5)*c + d;
            incr = 0.1*a + (0.1*0.5)*c;
            p5[0] = val;
            for (i = 1; i < 10; ++i)
               p5[i] = (val += incr);
            p5 += 10;

            // y = .6
            val = 0.0*a + 0.6*b + (0.0*0.6)*c + d;
            incr = 0.1*a + (0.1*0.6)*c;
            p6[0] = val;
            for (i = 1; i < 10; ++i)
               p6[i] = (val += incr);
            p6 += 10;

            // y = .7
            val = 0.0*a + 0.7*b + (0.0*0.7)*c + d;
            incr = 0.1*a + (0.1*0.7)*c;
            p7[0] = val;
            for (i = 1; i < 10; ++i)
               p7[i] = (val += incr);
            p7 += 10;

            // y = .8
            val = 0.0*a + 0.8*b + (0.0*0.8)*c + d;
            incr = 0.1*a + (0.1*0.8)*c;
            p8[0] = val;
            for (i = 1; i < 10; ++i)
               p8[i] = (val += incr);
            p8 += 10;

            // y = .9
            val = 0.0*a + 0.9*b + (0.0*0.9)*c + d;
            incr = 0.1*a + (0.1*0.9)*c;
            p9[0] = val;
            for (i = 1; i < 10; ++i)
               p9[i] = (val += incr);
            p9 += 10;

#endif

            pRI += 1;
         }

       // Done with input row, continue to next input row
         pR += nCol;
         pO += 10*IntraColSize;
     }
}

namespace {

   // CAREFUL: endCol is EXCLUSIVE
   bool testRGBRowSegment(MTI_UINT16 *pIn, int iR, int nCol,
                          int startCol, int endCol,
                          int lumLow, int lumHigh)
   {
      MTI_UINT16 *p = pIn + (iR*nCol + startCol)*3;
      int r = 0, g = 0, b = 0;
      for (int iC=startCol; iC < endCol; iC++)
      {
         r += p[0];
         g += p[1];
         b += p[2];
         p += 3;
      }
      int lum = int(r*0.29 + g*0.60 + b*0.11) / (endCol - startCol);

      return (lum > lumLow && lum < lumHigh);
   };

   // CAREFUL: endRow is EXCLUSIVE
   bool testRGBColSegment(MTI_UINT16 *pIn, int iC, int nCol,
                          int startRow, int endRow,
                          int lumLow, int lumHigh)
   {
      int r = 0, g = 0, b = 0;
      for (int iR=startRow; iR < endRow; iR++)
      {
         MTI_UINT16 *p = pIn + (iR*nCol + iC)*3;
         r += p[0];
         g += p[1];
         b += p[2];
      }
      int lum = int(r*0.29 + g*0.60 + b*0.11) / (endRow - startRow);

      return (lum > lumLow && lum < lumHigh);
   };

#define MAX_TEST_SEGMENTS 10
#define MIN_SEGMENT_SIZE  72

   bool testRGBRow(MTI_UINT16 *pIn, int iR, int nCol, int firstCol, int lastCol,
                   int lumLow, int lumHigh)
   {
      int nSegments = MAX_TEST_SEGMENTS;
      int segSize = (lastCol - firstCol + 1) / nSegments;
      int iSeg;

      while (nSegments > 1 && segSize < MIN_SEGMENT_SIZE)
      {
         --nSegments;
         segSize = (lastCol - firstCol + 1) / nSegments;
      }
      for (iSeg = 0; iSeg < nSegments; ++ iSeg)
      {
         int segStart = firstCol + iSeg * segSize;
         int segStop = (iSeg == (nSegments - 1))? lastCol : segStart + segSize;

         if (!testRGBRowSegment(pIn, iR, nCol, segStart, segStop,
                                lumLow, lumHigh))
         {
             break;
         }
      }

      return (iSeg == nSegments);
   };

   bool testRGBCol(MTI_UINT16 *pIn, int iC, int nCol, int firstRow, int lastRow,
                   int lumLow, int lumHigh)
   {
      int nSegments = MAX_TEST_SEGMENTS;
      int segSize = (lastRow - firstRow + 1) / nSegments;
      int iSeg;

      while (nSegments > 1 && segSize < MIN_SEGMENT_SIZE)
      {
         --nSegments;
         segSize = (lastRow - firstRow + 1) / nSegments;
      }
      for (iSeg = 0; iSeg < nSegments; ++ iSeg)
      {
         int segStart = firstRow + iSeg * segSize;
         int segStop = (iSeg == (nSegments - 1))? lastRow : segStart + segSize;

         if (!testRGBColSegment(pIn, iC, nCol, segStart, segStop,
                                lumLow, lumHigh))
         {
             break;
         }
      }

      return (iSeg == nSegments);
   };

}

#define FIND_MATTE_BASED_ON_LUMINANCE_ONLY 1

RECT FindMatBox(MTI_UINT16 *pIn, int nCol, int nRow, int Threshold,
                RECT &inRect, bool RGB, int R, int G, int B,
                int matX, int matY)

// This finds the best MAT box for a monochrome image
//   pIn is the image of size nCol, nRow
//   Return is the Mat Box
//
//*****************************************************************************
{

   RECT dRect = inRect;
   int Ylow = int(R*0.29 + G*0.60 + B*0.11) - Threshold/2;
   int Yhigh = Ylow + Threshold;
   int oneTenthCols = nCol / 10;  // ignore a tenth at each end of row because
   int oneTenthRows;              // of rounded corners
   int left = oneTenthCols;
   int right = nCol - oneTenthCols;
   int top, bottom;               // computed later

   MTIassert(inRect.right < nCol);   // inclusive
   MTIassert(inRect.bottom < nRow);   // inclusive

   if (RGB)
   {
#ifdef FIND_MATTE_BASED_ON_LUMINANCE_ONLY

      // Now test the top row
      for (int iR=inRect.top; iR < nRow/2; iR++)
      {
         if (!testRGBRow(pIn, iR, nCol, left,  right, Ylow, Yhigh))
         {
            dRect.top = iR;
            break;
         }
      }

      // Now test the bottom row
      for (int iR=inRect.bottom; iR > nRow/2; iR--)
      {
         if (!testRGBRow(pIn, iR, nCol, left,  right, Ylow, Yhigh))
         {
            dRect.bottom = iR;
            break;
         }
      }

      // Compute top/bottom for col test;
      // ignore a tenth at each end to deal with rounded corners
      oneTenthRows = (dRect.bottom - dRect.top + 1) / 10;
      top = dRect.top + oneTenthRows;
      bottom = dRect.bottom - oneTenthRows;
      if (bottom <= top)
      {
         // punt - gonna suck anyhow
         top = dRect.top;
         bottom = dRect.bottom;
      }

      // Now test the left side
      for (int iC = inRect.left; iC < nCol/2; iC++)
      {
         if (!testRGBCol(pIn, iC, nCol, top, bottom, Ylow, Yhigh))
         {
            dRect.left = iC;
            break;
         }
      }

      // Now test the right side
      for (int iC = inRect.right; iC > nCol/2; iC--)
      {
         if (!testRGBCol(pIn, iC, nCol, top, bottom, Ylow, Yhigh))
         {
            dRect.right = iC;
            break;
         }
      }

      if ((matY > dRect.top && matY < dRect.bottom) &&
          (matX > dRect.left && matX < dRect.right))
      {
         // Ooops - known matte point was not in the matte we found
         // try inside out instead

         // Now test the top row
         for (int iR = nRow/2 - 1; iR >= inRect.top; --iR)
         {
            if (testRGBRow(pIn, iR, nCol, left,  right, Ylow, Yhigh))
            {
               if (iR < (nRow/2 - 1))    // sanity
                  dRect.top = iR + 1;
               break;
            }
         }

         // Now test the bottom row
         for (int iR = nRow/2 + 1; iR <= inRect.bottom; ++iR)
         {
            if (testRGBRow(pIn, iR, nCol, left, right, Ylow, Yhigh))
            {
               if (iR > (nRow/2 + 1))    // sanity
                  dRect.bottom = iR - 1;
               break;
            }
         }

         // Compute top/bottom for col test;
         // ignore a tenth at each end to deal with rounded corners
         oneTenthRows = (dRect.bottom - dRect.top) / 10;
         top = dRect.top + oneTenthRows;
         bottom = dRect.bottom - oneTenthRows;
         if (bottom <= top)
         {
            // punt - gonna suck anyhow
            top = dRect.top;
            bottom = dRect.bottom;
         }

         // Now test the left side
         for (int iC = nCol/2 - 1; iC >= inRect.left; --iC)
         {
            if (testRGBCol(pIn, iC, nCol, top, bottom, Ylow, Yhigh))
            {
               if (iC < (nCol/2 - 1))    // sanity
                  dRect.left = iC + 1;
               break;
            }
         }

         // Now test the right side
         for (int iC = nCol/2 + 1; iC <= inRect.right; ++iC)
         {
            if (testRGBCol(pIn, iC, nCol, top, bottom, Ylow, Yhigh))
            {
               if (iC > (nCol/2 + 1))    // sanity
                  dRect.right = iC - 1;
               break;
            }
         }
      }

#else // find matte based on actual color

      int Rlow = R - Threshold/2;
      int Rhigh = Rlow + Threshold;
      int Glow = G - Threshold/2;
      int Ghigh = Glow + Threshold;
      int Blow = B - Threshold/2;
      int Bhigh = Blow + Threshold;

      // Now test the top row
      for (int iR=dRect.top; iR < nRow/2; iR++)
      {
         MTI_UINT16 *p = pIn + iR*nCol*3;
         int r = 0, g = 0, b = 0;
         for (int iC=dRect.left; iC < dRect.right; iC++)
         {
            r += p[0];
            g += p[1];
            b += p[2];
            p += 3;
         }
         r /= (dRect.right - dRect.left + 1);
         g /= (dRect.right - dRect.left + 1);
         b /= (dRect.right - dRect.left + 1);
         if (r < Rlow || r > Rhigh ||
             g < Glow || g > Ghigh ||
             b < Blow || b > Bhigh)
         {
            dRect.top = iR;
            break;
         }
      }

      // Now test the bottom row
      for (int iR=dRect.bottom; iR > nRow/2; iR--)
      {
         MTI_UINT16 *p = pIn + iR*nCol*3;
         int r = 0, g = 0, b = 0;
         for (int iC=dRect.left; iC < dRect.right; iC++)
         {
            r += p[0];
            g += p[1];
            b += p[2];
            p += 3;
         }
         r /= (dRect.right - dRect.left + 1);
         g /= (dRect.right - dRect.left + 1);
         b /= (dRect.right - dRect.left + 1);
         if (r < Rlow || r > Rhigh ||
             g < Glow || g > Ghigh ||
             b < Blow || b > Bhigh)
         {
            dRect.bottom = iR;
            break;
         }
      }

      // Now test the left side
      for (int iC=dRect.left; iC < nCol/2; iC++)
      {
         int r = 0, g = 0, b = 0;
         for (int iR=dRect.top; iR < dRect.bottom; iR++)
         {
            MTI_UINT16 *p = pIn + (iR*nCol + iC)*3;
            r += p[0];
            g += p[1];
            b += p[2];
         }
         r /= (dRect.bottom - dRect.top + 1);
         g /= (dRect.bottom - dRect.top + 1);
         b /= (dRect.bottom - dRect.top + 1);
         if (r < Rlow || r > Rhigh ||
             g < Glow || g > Ghigh ||
             b < Blow || b > Bhigh)
         {
            dRect.left = iC;
            break;
         }
      }

      // Now test the right side
      for (int iC=dRect.right; iC > nCol/2; iC--)
      {
         int r = 0, g = 0, b = 0;
         for (int iR=dRect.top; iR < dRect.bottom; iR++)
         {
            MTI_UINT16 *p = pIn + (iR*nCol + iC)*3;
            r += p[0];
            g += p[1];
            b += p[2];
         }
         r /= (dRect.bottom - dRect.top + 1);
         g /= (dRect.bottom - dRect.top + 1);
         b /= (dRect.bottom - dRect.top + 1);
         if (r < Rlow || r > Rhigh ||
             g < Glow || g > Ghigh ||
             b < Blow || b > Bhigh)
         {
            dRect.right = iC;
            break;
         }
      }
#endif
   }
   else // !RGB
   {
      // Now test the top row
      for (int iR=dRect.top; iR < nRow/2; iR++)
      {
         MTI_UINT16 *p = pIn + iR*nCol*3;
         int y = 0;
         for (int iC=dRect.left; iC < dRect.right; iC++)
         {
            y += p[0];
            p += 3;
         }
         int yAvg = y / (dRect.right - dRect.left + 1);
         if (yAvg < Ylow || yAvg > Yhigh)
         {
            dRect.top = iR;
            break;
         }
      }

      // Now test the bottom row
      for (int iR=dRect.bottom; iR > nRow/2; iR--)
      {
         MTI_UINT16 *p = pIn + iR*nCol*3;
         int y = 0;
         for (int iC=dRect.left; iC < dRect.right; iC++)
         {
            y += p[0];
            p += 3;
         }
         int yAvg = y / (dRect.right - dRect.left + 1);
         if (yAvg < Ylow || yAvg > Yhigh)
         {
            dRect.bottom = iR;
            break;
         }
      }

      // Now test the left side
      for (int iC=dRect.left; iC < nCol/2; iC++)
      {
         int y = 0;
         for (int iR=dRect.top; iR < dRect.bottom; iR++)
         {
            MTI_UINT16 *p = pIn + (iR*nCol + iC)*3;
            y += p[0];
         }
         int yAvg = y / (dRect.bottom - dRect.top + 1);
         if (yAvg < Ylow || yAvg > Yhigh)
         {
            dRect.left = iC;
            break;
         }
      }

      // Now test the right side
      for (int iC=dRect.right; iC > nCol/2; iC--)
      {
         int y = 0;
         for (int iR=dRect.top; iR < dRect.bottom; iR++)
         {
            MTI_UINT16 *p = pIn + (iR*nCol + iC)*3;
            y += p[0];
         }
         int yAvg = y / (dRect.bottom - dRect.top + 1);
         if (yAvg < Ylow || yAvg > Yhigh)
         {
            dRect.right = iC;
            break;
         }
      }
   }

   return dRect;

}

RECT FindMatBox(MTI_UINT16 *pIn, int nCol, int nRow, int Threshold, RECT &inRect)

// This finds the best MAT box for a monochrome image
//   pIn is the image of size nCol, nRow
//   Return is the Mat Box
//
//*****************************************************************************
{

   RECT dRect = inRect;
   Threshold = 3*Threshold;

   // Now test the top row
   for (int iR=dRect.top; iR < nRow/2; iR++)
    {
      MTI_UINT16 *p = pIn + iR*nCol*3;
      int m = 0;
      for (int iC=dRect.left; iC < dRect.right; iC++)
	  {
		 m += *p++;
		 m += *p++;
		 m += *p++;
      }

      m = m/(dRect.right - dRect.left + 1);
      if (m > Threshold)
            {
              dRect.top = iR;
              break;
            }

    }

   // Now test the bottom row
   for (int iR=dRect.bottom; iR > nRow/2; iR--)
    {
      MTI_UINT16 *p = pIn + iR*nCol*3;
      int m = 0;
      for (int iC=dRect.left; iC < dRect.right; iC++)
	  {
		 m += *p++;
		 m += *p++;
		 m += *p++;
	  }

      m = m/(dRect.right - dRect.left + 1);
      if (m > Threshold)
        {
          dRect.bottom = iR;
          break;
        }
    }

    // Now test the left
    for (int iC=dRect.left; iC < nCol/2; iC++)
    {
	  int m = 0;
		for (int iR = dRect.top; iR < dRect.bottom; iR++)
		{
			MTI_UINT16 *p = pIn + (iR * nCol + iC) * 3;

			m += *p++;
			m += *p++;
			m += *p++;

		}

      m = m/(dRect.bottom - dRect.top + 1);
      if (m > Threshold)
         {
           dRect.left = iC;
           break;
         }

    }

    // Now test the right
    for (int iC=dRect.right; iC > nCol/2; iC--)
    {
      int m = 0;
      for (int iR=dRect.top; iR < dRect.bottom; iR++)
        {
		  MTI_UINT16 *p = pIn + (iR*nCol + iC)*3;
		 m += *p++;
		 m += *p++;
		 m += *p++;
         }

      m = m/(dRect.bottom - dRect.top + 1);
      if (m > Threshold)
         {
           dRect.right = iC;
           break;
         }
    }

   return dRect;

}

double BiLinear( double x, double y, double C00, double C10, double C01, double C11)
{
      double a = -C00 + C10;
      double b = -C00 + C01;
      double c =  C00 - C10 - C01 + C11;
      double d =  C00;

      x = x - (int)x;
      y = y - (int)y;
      return a*x + b*y + x*y*c + d;
}

void MakeMonoChromeImageRGB(MTI_UINT16 *pIn, int nCol, int nRow, MTI_UINT16 *pOut)
{
   MTI_UINT16 *pInStop = pIn + (nRow * nCol * 3);
   while (pIn < pInStop)
    {
       *pOut++ = (0.32 * pIn[0]) + (0.64 * pIn[1]) + (0.4 * pIn[2]);
       pIn += 3;
    }
}

void MakeMonoChromeImageFromOneChannel(MTI_UINT16 *pIn, int nCol, int nRow, int channel, MTI_UINT16 *pOut)
{
   MTIassert(channel >= 0 && channel <= 2);
   pIn += channel;
   MTI_UINT16 *pInStop = pIn + (nRow * nCol * 3);
   while (pIn < pInStop)
   {
      *pOut++ = *pIn;
      pIn += 3;
   }
}

#define LUT_LOOK_UP(lut, val, shift) (lut[val >> shift] << shift)

void MakeMonoChromeImageRGB(MTI_UINT16 *pIn, int nCol, int nRow, int nBits, MTI_UINT8 *lutPtr, MTI_UINT16 *pOut)
{
   MTI_UINT16 *pInStop = pIn + (nRow * nCol * 3);
   MTI_UINT8 *rLut = &lutPtr[000];
   MTI_UINT8 *gLut = &lutPtr[256];
   MTI_UINT8 *bLut = &lutPtr[512];
   MTIassert(nBits >= 8 && nBits <=16);
   int shift = nBits - 8;
   while (pIn < pInStop)
    {
       *pOut++ = (0.32 * LUT_LOOK_UP(rLut, pIn[0], shift))
               + (0.64 * LUT_LOOK_UP(gLut, pIn[1], shift))
               + (0.04 * LUT_LOOK_UP(bLut, pIn[2], shift));
       pIn += 3;
    }
}

void MakeMonoChromeImageFromOneChannel(MTI_UINT16 *pIn, int nCol, int nRow, int nBits, int channel, MTI_UINT8 *lutPtr, MTI_UINT16 *pOut)
{
   MTIassert(channel >= 0 && channel <= 2);
   pIn += channel;
   MTI_UINT16 *pInStop = pIn + (nRow * nCol * 3);
   MTI_UINT8 *lut = &lutPtr[channel * 256];
   int shift = nBits - 8;
   while (pIn < pInStop)
   {
      *pOut++ = LUT_LOOK_UP(lut, *pIn, shift);
      pIn += 3;
   }
}

void MakeMonoChromeImage(MTI_UINT16 *pIn, int nCol, int nRow, int nBits, int channel, MTI_UINT8 *lutPtr, MTI_UINT16 *pOut)
{
   MTIassert(channel >= -1 && channel <= 2);

   // Channel == -1 means combine RGB
   if (channel == -1)
   {
      if (lutPtr)
      {
         MakeMonoChromeImageRGB(pIn, nCol, nRow, nBits, lutPtr, pOut);
      }
      else
      {
         MakeMonoChromeImageRGB(pIn, nCol, nRow, pOut);
      }
   }
   else
   {
      if (lutPtr)
      {
         MakeMonoChromeImageFromOneChannel(pIn, nCol, nRow, nBits, channel, lutPtr, pOut);
      }
      else
      {
         MakeMonoChromeImageFromOneChannel(pIn, nCol, nRow, channel, pOut);
      }
   }
}

#define TINY 1.0e-30

void EstimateMinFrom9Points(double D[9], double &x, double &y)
{
  double a = D[0] - 2*D[1] + D[2] + D[3] - 2*D[4] + D[5] + D[6] - 2*D[7] + D[8];
  double b = D[0] + D[1] + D[2] - 2*D[3] - 2*D[4] - 2*D[5] + D[6] + D[7] + D[8];
  double c = 1.5*(D[0] - D[2] - D[6] + D[8]);
  double d = -D[0] + D[2] - D[3] + D[5] - D[6] + D[8];
  double e = -D[0] - D[1] - D[2] + D[6] + D[7] + D[8];

  double g = 4*a*b - c*c;

  // No 2D quad solution if g is small
  if (abs(g) <= TINY)
    {
       // See if the 1D solution is ok
       if (abs(a) <= TINY)
         x = 0;  // No return the center
       else
         x = d/(2*a);

       // See if the 1D solution is ok
       if (abs(b) <= TINY)
         y = 0;  // No return the center
       else
         y = e/(2*b);
       return;
    }

  x = (e*c - 2*b*d)/g;
  y = (d*c - 2*a*e)/g;
}

/* gammln as implemented in the
 * first edition of Numerical Recipes in C */
double gammln(double xx)
{
    double x,tmp,ser;
    static double cof[6]={76.18009172947146,    -86.50532032941677,
                          24.01409824083091,    -1.231739572450155,
                          0.1208650973866179e-2,-0.5395239384953e-5};
    int j;

    x=xx-1.0;
    tmp=x+5.5;
    tmp -= (x+0.5)*log(tmp);
    ser=1.000000000190015;
    for (j=0;j<=5;j++) {
        x += 1.0;
        ser += cof[j]/x;
    }
    return -tmp+log(2.5066282746310005*ser);
}

#define MAXIT 100
#define EPS 3.0e-7
#define FPMIN 1.0e-30

double betacf(double a, double b, double x)
{
//	void nrerror();
	int m,m2;
	double aa,c,d,del,h,qab,qam,qap;

	qab=a+b;
	qap=a+1.0;
	qam=a-1.0;
	c=1.0;
	d=1.0-qab*x/qap;
	if (fabs(d) < FPMIN) d=FPMIN;
	d=1.0/d;
	h=d;
	for (m=1;m<=MAXIT;m++) {
		m2=2*m;
		aa=m*(b-m)*x/((qam+m2)*(a+m2));
		d=1.0+aa*d;
		if (fabs(d) < FPMIN) d=FPMIN;
		c=1.0+aa/c;
		if (fabs(c) < FPMIN) c=FPMIN;
		d=1.0/d;
		h *= d*c;
		aa = -(a+m)*(qab+m)*x/((a+m2)*(qap+m2));
		d=1.0+aa*d;
		if (fabs(d) < FPMIN) d=FPMIN;
		c=1.0+aa/c;
		if (fabs(c) < FPMIN) c=FPMIN;
		d=1.0/d;
		del=d*c;
		h *= del;
		if (fabs(del-1.0) < EPS) break;
	}
	if (m > MAXIT) throw("a or b too big, or MAXIT too small in betacf");
	return h;
}
#undef MAXIT
#undef EPS
#undef FPMIN

double betai(double a, double b, double x)
{
	double betacf(double a, double b, double x),gammln(double x);
//	void nrerror();
	double bt;

	if (x < 0.0 || x > 1.0) throw("Bad x in routine betai");
	if (x == 0.0 || x == 1.0) bt=0.0;
	else
		bt=exp(gammln(a+b)-gammln(a)-gammln(b)+a*log(x)+b*log(1.0-x));
	if (x < (a+1.0)/(a+b+2.0))
		return bt*betacf(a,b,x)/a;
	else
		return 1.0-bt*betacf(b,a,1.0-x)/b;
}



void pearsn(double x[], double y[], unsigned long n, double &r, double &prob, double &z)
{
	double betai(double a, double b, double x);
	unsigned long j;
	double yt,xt,t,df;
	double syy=0.0,sxy=0.0,sxx=0.0,ay=0.0,ax=0.0;

	for (j=1;j<=n;j++) {
		ax += x[j];
		ay += y[j];
	}
	ax /= n;
	ay /= n;
	for (j=1;j<=n;j++) {
		xt=x[j]-ax;
		yt=y[j]-ay;
		sxx += xt*xt;
		syy += yt*yt;
		sxy += xt*yt;
	}
	r=sxy/(sqrt(sxx*syy)+TINY);
	z=0.5*log((1.0+(r)+TINY)/(1.0-(r)+TINY));
//	df=n-2;
//	t=(r)*sqrt(df/((1.0-(r)+TINY)*(1.0+(r)+TINY)));
        prob = 0;
//	prob=betai(0.5*df,0.5,df/(df+t*t));
}
#undef TINY


/* The ziggurat method for RNOR and REXP
Combine the code below with the main program in which you want
normal or exponential variates.   Then use of RNOR in any expression
will provide a standard normal variate with mean zero, variance 1,
while use of REXP in any expression will provide an exponential variate
with density exp(-x),x>0.
Before using RNOR or REXP in your main, insert a command such as
zigset(86947731 );
with your own choice of seed value>0, rather than 86947731.
(If you do not invoke zigset(...) you will get all zeros for RNOR and REXP.)
For details of the method, see Marsaglia and Tsang, "The ziggurat method
for generating random variables", Journ. Statistical Software.
*/

static unsigned long jz,jsr=123456789;

#define SHR3 (jz=jsr, jsr^=(jsr<<13), jsr^=(jsr>>17), jsr^=(jsr<<5),jz+jsr)
#define UNI (.5 + (signed) SHR3*.2328306e-9)
#define IUNI SHR3

static long hz;
static unsigned long iz, kn[128], ke[256];
static float wn[128],fn[128], we[256],fe[256];

#define RNOR (hz=SHR3, iz=hz&127, ((unsigned long)labs(hz)<kn[iz])? hz*wn[iz] : nfix())
#define REXP (jz=SHR3, iz=jz&255, (    jz <ke[iz])? jz*we[iz] : efix())

/* nfix() generates variates from the residue when rejection in RNOR occurs. */

static float nfix(void)
{
const float r = 3.442620f;     /* The start of the right tail */
static float x, y;
 for(;;)
  {  x=hz*wn[iz];      /* iz==0, handles the base strip */
     if(iz==0)
       { do{ x=-log(UNI)*0.2904764; y=-log(UNI);}	/* .2904764 is 1/r */
        while(y+y<x*x);
        return (hz>0)? r+x : -r-x;
       }
                         /* iz>0, handle the wedges of other strips */
      if( fn[iz]+UNI*(fn[iz-1]-fn[iz]) < exp(-.5*x*x) ) return x;

     /* initiate, try to exit for(;;) for loop*/
      hz=SHR3;
      iz=hz&127;
      if((unsigned long)labs(hz)<kn[iz]) return (hz*wn[iz]);
  }

}

/* efix() generates variates from the residue when rejection in REXP occurs. */
static float efix(void)
{ float x;
 for(;;)
  {  if(iz==0) return (7.69711-log(UNI));          /* iz==0 */
     x=jz*we[iz]; if( fe[iz]+UNI*(fe[iz-1]-fe[iz]) < exp(-x) ) return (x);

      /* initiate, try to exit for(;;) loop */
   jz=SHR3;
   iz=(jz&255);
   if(jz<ke[iz]) return (jz*we[iz]);
  }
}
/*--------This procedure sets the seed and creates the tables------*/

static void zigset(unsigned long jsrseed)
{  const double m1 = 2147483648.0, m2 = 4294967296.;
   double dn=3.442619855899,tn=dn,vn=9.91256303526217e-3, q;
   double de=7.697117470131487, te=de, ve=3.949659822581572e-3;
   int i;
   jsr^=jsrseed;

/* Set up tables for RNOR */
   q=vn/exp(-.5*dn*dn);
   kn[0]=(dn/q)*m1;
   kn[1]=0;

   wn[0]=q/m1;
   wn[127]=dn/m1;

   fn[0]=1.;
   fn[127]=exp(-.5*dn*dn);

    for(i=126;i>=1;i--)
    {dn=sqrt(-2.*log(vn/dn+exp(-.5*dn*dn)));
     kn[i+1]=(dn/tn)*m1;
     tn=dn;
     fn[i]=exp(-.5*dn*dn);
     wn[i]=dn/m1;
    }

/* Set up tables for REXP */
    q = ve/exp(-de);
    ke[0]=(de/q)*m2;
    ke[1]=0;

    we[0]=q/m2;
    we[255]=de/m2;

    fe[0]=1.;
    fe[255]=exp(-de);

   for(i=254;i>=1;i--)
  {de=-log(ve/de+exp(-de));
   ke[i+1]= (de/te)*m2;
   te=de;
   fe[i]=exp(-de);
   we[i]=de/m2;
  }
}

static bool seeded = false;

void setRandSeed(unsigned long seed)
{
   zigset(seed);
   seeded = true;
}

double getRandNormal()
{
   if (!seeded)
    {
      setRandSeed(86947731);
    }
   return RNOR;
}

/**
template <class T>
struct mymatrix
{
    mymatrix(int ncols=1)
    : m_ncols(1)
    {
       add_col(ncols);
    };

    ~mymatrix()
    {
        for (int i = 0; i < m_nCols; ++i)
            mColVec.pop_back();
        m_nCols = 0;
    };

    add_col(int nCols=1)
    {
        vector<T> newVec;
        for (int i = 0; i < nCols; ++i)
            m_colVec.push_back(newVec);
        m_nCols += nCols;
    };

    vector<T> get_col(int colNum)
    {
                return;
    };
}
**/

//typedef vector<double> dvector;
//typedef vector<dvector> dmatrix;  // each dvector is a column


double madsigma(const mvector<double> &r, int p);
void wfit(const mvector<double> &y, const matrix<double> &X, const mvector<double> &w,
          /* OUTPUT */ mvector<double> &b);
void bisquare(const mvector<double> &r,
          /* OUTPUT */ mvector<double> &w);

// y vector of observations of the 'dependent' variable (nx1)
// X matrix values of the independent variables - every row
//     is a separate observation of every independent
//     variable (nxp)
// T threshold for stopping the iterated least squares - stop
//     if all of the output components change by less than T
// b estimated regression coefficients (px1), so that y is
// approximated by Xb (return value)
//
// Returns true if successful (got convergence)

bool robust(const mvector<double> &y, const matrix<double> &X,
            double T,  /* OUTPUT */ mvector<double> &b)
{
    //b.clear();
    mvector<double> h;
    int i;

    // Get n (number of observations) and p (number of independent variables)
    //  [n,p]=size(X);
    int n = X.rows;
    if (n < 1)
        return false;
    int p = X.cols;

    // Compute the 'leverage' of each of the observations (a measure of
    // how much effect a given y will have on the estimated b)
    //   h = diag(X*inv(X'*X)*X'); // h is a vector with n components - namely
                                   // the diagonal of the nxn matrix argument
    //compute_leverage(X, h);
    matrix<double> mTrans, mTmp1, mTmp2, mTmp3;
    mTrans = X.transpose();
    mTmp1 = mTrans * X;
    mTmp2 = invertMatrix(mTmp1);
    mTmp3 = (X * mTmp2) * mTrans;
    h = diag(mTmp3);

    // Can't let the components of h be 1 - or get trouble:
    //    h=min(.9999,h); // This is component-wise minimization
    for (i = 0; i < n; ++i)
        h[i] = std::min<double>(h[i], 0.9999);

    // Now make a vector of n adjustments, one for each observation
    //    adj=1./sqrt(1-h); // The i'th component of adj is 1/sqrt(1-h(i))
    mvector<double> adj(n);
    for (i = 0; i < n; ++i)
        adj[i] = 1.0 / sqrt(1.0 - h[i]);

    // If the residuals ever get really small, then one of the residual
    // adjustments needs to be adjusted! 'Really small is in terms of the
    // standard deviation of the observables.

    //   meany = sum(y)/n; // mean
    double meany = 0.0;
    for (i = 0; i < n; ++i)
        meany += y[i];
    meany /= n;

    //   sdy = sqrt( dot((y-meany),(y-meany)) )/(n-1); // stand. dev.
    double sdy = 0.0;
    for (i = 0; i < n; ++i)
        sdy += (y[i] - meany) * (y[i] - meany);
    sdy = ((sdy > 0.0)? (sqrt(sdy) / (n - 1)) : 0.0);

    //   tiny_s = .000001 * sdy;
    double tiny_s = 0.000001 * sdy;

    // Initialize the iterations with an ordinary least squares estimate of b
    // First initialize the weight vector to all ones (hence ordinary least
    // squares)
    // w=ones(n,1);
    mvector<double> w(n);
    for (i = 0; i < n; ++i)
        w[i] = 1.0;

    //    b_old=wfit(y,X,w); // b_old is the estimate of b;
    mvector<double> b_old(p);
    wfit(y, X, w, b_old);

    //    r_old=y-X*b_old; // vector of associated residuals
    mvector<double> r_old(n);
    mvector<double> vtemp(n);
    // matrix_multiply(X, b_old, vtemp);
    vtemp = X * b_old;
    // vector_subtract(y, vtemp, r_old);
    r_old = y - vtemp;

    // Now do iterated, re-weighted, least squares:
    //   for iter=1:50
    bool victory = true;
    mvector<double> b_new(p);
    int iter;
    for (iter = 0; iter < 50; ++iter)
    {
#if 0
        TRACE_3(errout << "+++++++ ITER " << iter << " +++++++");
#endif

        // Hoping not to make it to 50 - otherwise return a warning and return the
        // latest estimate of b
        // if iter==50
        // 'did not converge'
        // b=b_old;
        // return
        // end
        // [ after end of loop ]

        // Otherwise, compute re-weighted residuals and then the new weight vector
        // First, the re-weighted residuals, which depend on a robust estimate
        // of the standard error of the current residuals
        //   ser = madsigma(r_old,p);
        double ser = madsigma(r_old, p);

        // Now the adjusted residuals
        //   radj = (r_old.*adj)/(4.685*max(ser,tiny_s));
        double tmp1 = 4.685 * std::max<double>(ser, tiny_s);
        mvector<double> radj(n);
        for (i = 0; i < n; ++i)
        {
           radj[i] = (r_old[i] * adj[i]) / tmp1;
        }

        // and the associated weight vector
        //   w=bisquare(radj);
        bisquare(radj, w);

        // Now the new (weighted) least-squares estimate and new residuals
        //   b_new=wfit(y,X,w);
        wfit(y, X, w, b_new);

        //   r_new=y-X*b_new;
        mvector<double> r_new(n);
        mvector<double> vtmp(n);
        vtmp = X * b_new;
        r_new = y - vtmp;

        // Finally, if there is negligable change in all components of b, declare
        // victory and go home
        //   if max(abs(b_old-b_new)) < T
        victory = true;
        for (i = 0; (i < p) && victory; ++i)
        {
            victory = (fabs(b_old[i] - b_new[i]) < T);
        }
#if 0
        for (i = 0; i < p; ++i)
        {
            TRACE_3(errout << i << ": OLD=" << b_old[i] << "  NEW=" << b_new[i]
                           << "  FABS=" << fabs(b_old[i] - b_new[i])
                           << ((fabs(b_old[i] - b_new[i]) < T)? "  <<<<" : ""));

        }
#endif
        if (victory)
        {
            //   b=b_new;
            //   return
            // [ after loop ]
            break;
        }
        //   else // otherwise, set up for next iteration
        //   b_old=b_new;
        //   r_old=r_new;
        //   end

        b_old = b_new;
        r_old = r_new;

    //    end // ends loop over iterations
    }
    // return
    b = b_new;
    return victory;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Now the three auxillary functions called by robust
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// First a robust estimate of the s.e. of a set of residuals
// function s=madsigma(r,p)
double madsigma(const mvector<double> &r, int p)
{
    vector<double> rs;  // NOTE: std::vector NOT mvector!

    // r vector of residuals
    // p number of residuals
    // Returns a robust estimate of the standard error of the residuals.
    // This is the median of the largest size(r)-p absolute residuals, divided
    // by a constant to make this close to the actual standard error for the
    // case of iid mean zero Gaussian residuals.
    //   rs = sort(abs(r));

    for (int i = 0; i < r.length(); ++i)
        rs.push_back(fabs(r[i]));
    std::sort(rs.begin(), rs.end());

    //    s = median(rs(p+1:end))/.6745;
    int medianIndex = p + (((int) rs.size()) - p) / 2;
    return rs[medianIndex] / 0.6745;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Next a weighted least-squares regression
void wfit(const mvector<double> &y, const matrix<double> &X,
                const mvector<double> &w, /* OUTPUT */ mvector<double> &b)
{
    int n = X.rows;
    int p = X.cols;
    mvector<double> sw(n);
    mvector<double> yw(n);
    int i, j;

    // The weights multiply the squared errors: w_i(y_i - (Xb)_i)^2. Distribute
    // the i'th weight by multiplying its square root times y_i and times the
    // i'th row of X.
    //   sw=sqrt(w); // vector of square roots
    //   yw=y.*sw; // component-wise mulitiplication
    for (i = 0; i < n; ++i)
    {
       sw[i] = ((w[i] > 0.0)? sqrt(w[i]) : 0.0);
       yw[i] = y[i] * sw[i];
    }

    //   p=size(X,2); // number of columns in X
    //   Xw=X.*sw(:,ones(1,p)); // every element of the i'th row gets multiplied
                                // by the i'th component of sw
    matrix<double> Xw(n, p);
    for (i = 0; i < n; ++i)
       for (j = 0; j < p; ++j)
          (Xw[i])[j] = (X[i])[j] * sw[i];

    //   b = inv(Xw'*Xw)*Xw'*yw; // weighted least-squares fit

    matrix<double> XwT, mTmp1, mTmp2, mTmp3;
    XwT = Xw.transpose();
    mTmp1 = XwT * Xw;
    mTmp2 = invertMatrix(mTmp1);
    mTmp3 = mTmp2 * XwT;
    b = mTmp3 * yw;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Finally, the weighting function
void bisquare(const mvector<double> &r, /* OUTPUT */ mvector<double> &w)
{
    int i;

    // w.zero();     WTF? gets error : 'Member identifier expected'
        for (i = 0; i < w.length(); ++i) w[i] = 0.0;

    // The i'th component of w is zero if r_i>1, and (1-r_i*r_i)^2 otherwise
    for (int i = 0; i < r.length(); ++i)
    {
        double r_i = r[i];
        if (fabs(r_i) >= 1.0)
        {
           w[i] = 0.0;
        }
        else
        {
           double dTmp = 1.0 - r_i * r_i;
           w[i] = dTmp * dTmp;
        }
    }
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

RECT FindMatBox(MTI_UINT16 *pIn, int nCol, int nRow, int Threshold, RECT &inRect, bool RGB)

// This finds the best MAT box for a monochrome image
//   pIn is the image of size nCol, nRow
//   Return is the Mat Box
//
//*****************************************************************************
{

   RECT dRect = inRect;
   if (RGB) Threshold = 3*Threshold;

   // Now test the top row
   for (int iR=dRect.top; iR < nRow/2; iR++)
    {
      MTI_UINT16 *p = pIn + iR*nCol*3;
      int m = 0;
      for (int iC=dRect.left; iC < dRect.right; iC++)
        if (RGB)
		 {
		   m += *p++;
		   m += *p++;
		   m += *p++;
		 }
		else
         { m += *p; p += 3;}

      m = m/(dRect.right - dRect.left + 1);
      if (m > Threshold)
            {
              dRect.top = iR;
              break;
            }

    }

   // Now test the bottom row
   for (int iR=dRect.bottom; iR > nRow/2; iR--)
	{
	  MTI_UINT16 *p = pIn + iR*nCol*3;
	  int m = 0;
	  for (int iC=dRect.left; iC < dRect.right; iC++)
		if (RGB)
	  {
		 m += *p++;
		 m += *p++;
		 m += *p++;
	  }
		else
		 { m += *p; p += 3;}

	  m = m/(dRect.right - dRect.left + 1);
	  if (m > Threshold)
		{
		  dRect.bottom = iR;
		  break;
		}
	}

    // Now test the left
    for (int iC=dRect.left; iC < nCol/2; iC++)
    {
      int m = 0;
      for (int iR=dRect.top; iR < dRect.bottom; iR++)
        {
          MTI_UINT16 *p = pIn + (iR*nCol + iC)*3;
          if (RGB)
		 {
		   m += *p++;
		   m += *p++;
		   m += *p++;
		 }
          else
            m += *p;
         }

      m = m/(dRect.bottom - dRect.top + 1);
      if (m > Threshold)
         {
           dRect.left = iC;
           break;
         }

    }

    // Now test the right
    for (int iC=dRect.right; iC > nCol/2; iC--)
    {
      int m = 0;
      for (int iR=dRect.top; iR < dRect.bottom; iR++)
        {
          MTI_UINT16 *p = pIn + (iR*nCol + iC)*3;
          if (RGB)
		 {
		   m += *p++;
		   m += *p++;
		   m += *p++;
		 }
		  else
			m += *p;
		 }

      m = m/(dRect.bottom - dRect.top + 1);
      if (m > Threshold)
         {
           dRect.right = iC;
           break;
         }
    }

   return dRect;

}

float interpolateMonoCubically(const vector<float> &xsArg, const vector<float> ysArg, float x)
{
   vector<float> xs = xsArg;
   vector<float> ys = ysArg;
   size_t length = std::min<size_t>(xs.size(), ys.size());
   if (length == 0)
   {
      return  0;
   }

	if (length == 1)
   {
		return ys[0];
	}

	// Rearrange xs and ys so that xs is sorted
	vector<int> indexes;
	for (auto i = 0; i < length; ++i)
   {
      indexes.push_back(i);
   }

   std::sort(indexes.begin(), indexes.end(), [&](int a, int b) { return xs[a] < xs[b]; });
	auto oldXs = xs;
   auto oldYs = ys;
	xs.clear();
   ys.clear();

   for (auto i = 0; i < length; i++)
   {
      xs.push_back(oldXs[indexes[i]]);
      ys.push_back(oldYs[indexes[i]]);
   }

	// Get consecutive differences and slopes
	vector<float> dys;
   vector<float> dxs;
   vector<float> ms;
	for (auto i = 0; i < length - 1; i++)
   {
		float dx = xs[i + 1] - xs[i];
      float dy = ys[i + 1] - ys[i];
		dxs.push_back(dx);
      dys.push_back(dy);
      ms.push_back(dy / dx);
	}

	// Get degree-1 coefficients
	vector<float> c1s;
   c1s.push_back(ms[0]);
	for (auto i = 0; i < dxs.size() - 1; i++)
   {
		float m = ms[i];
      float mNext = ms[i + 1];
		if ((m * mNext) <= 0.F)
      {
			c1s.push_back(0);
		}
      else
      {
			float dx_ = dxs[i];
         float dxNext = dxs[i + 1];
         float common = dx_ + dxNext;
			c1s.push_back(3 * common / ((common + dxNext) / m + (common + dx_) / mNext));
		}
	}

	c1s.push_back(ms[ms.size() - 1]);

	// Get degree-2 and degree-3 coefficients
	vector<float> c2s;
   vector<float> c3s;
	for (auto i = 0; i < c1s.size() - 1; ++i)
   {
		float c1 = c1s[i];
      float m_ = ms[i];
      float invDx = 1/dxs[i];
      float common_ = c1 + c1s[i + 1] - m_ - m_;
		c2s.push_back((m_ - c1 - common_)*invDx);
      c3s.push_back(common_*invDx*invDx);
	}

   // The rightmost point in the dataset should give an exact result
   size_t i = xs.size() - 1;
   if (x == xs[i])
   {
      return ys[i];
   }

   // Search for the interval x is in, returning the corresponding y if x is one of the original xs
   int low = 0;
   int mid;
   int high = int(c3s.size() - 1);
   while (low <= high)
   {
      mid = int(std::floorf(0.5F*(low + high)));
      float xHere = xs[mid];
      if (xHere < x)
      {
         low = mid + 1;
      }
      else if (xHere > x)
      {
         high = mid - 1;
      }
      else
      {
         return ys[mid];
      }
   }

   i = std::max<int>(0, high);

   // Interpolate
   float diff = x - xs[i];
   float diffSq = diff*diff;

   return ys[i] + c1s[i]*diff + c2s[i]*diffSq + c3s[i]*diff*diffSq;
};
