//---------------------------------------------------------------------------

#ifndef SqliteBlobH
#define SqliteBlobH
//---------------------------------------------------------------------------

#include "SqliteDB.h"

class MTI_CORECODELIB_API CSqliteBlob final
{
public:
   CSqliteBlob(CSqliteDB *sqliteDB);
   ~CSqliteBlob();

   void open(const string &tableName, const string &columnName, int rowId, bool readOnly=false);
   void write(const void *data, int size, int offset=0);
   void read(void *data, int size, int offset=0);
   void reopen(int newRowId);
   void close();

   int getSize() const;
   bool isOpen() const;

private:
   CSqliteDB *_sqliteDB = nullptr;
   sqlite3_blob *_blob = nullptr;
};
#endif
