#ifndef SharedMapH
#define SharedMapH

#include "corecodelibint.h"
#include <map>
#include <string>
using std::map;
using std::string;

class MTI_CORECODELIB_API SharedMap
{
   static map<string, void *> TheMap;

public:

   static void add(const string &key, void *value);
   static void remove(const string &key);
   static void *retrieve(const string &key);

   static void *getMap();

private:

   SharedMap() {};
};

#endif // _SHARED_LIST_H_
