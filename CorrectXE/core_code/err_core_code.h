/*
   File Name:  err_core_code.h
   Created: June 3, 2002 by John Mertus

   This contains all the error codes for all core_code functions
*/

#ifndef ERR_CORE_CODEH
#define ERR_CORE_CODEH

//-------------------------------------------------------------------------
//
//  CIniFile Erros  INIFILE_ERROR__XXX
//
#define INIFILE_ERROR_CREATE_FAILED -1500
//  This happens when a the INI file cannot be created.  Most likely a protection
// problem. A ini file gives this error if it cannot be read by user

#define INIFILE_ERROR_READ_FAILED -1501
//  The read of the ini file failed because of some reason.  Most likely a protection
// problem but also if the file is corrupted very badly.

#define INIFILE_ERROR_WRITE_FAILED -1502
//  The write of the ini file failed because of some reason.  Most likely because
// the disk is full.

#define FILESWEEPER_ERROR_MKDIR_FAILED -1503
//  A mkfile (Make Directory) system call failed.  Most likey protection
//  problem or the directory name contains illegal characters.

#define FILESWEEPER_ERROR_SET_FILE_ATTRIBUTES -1504
//  Some attribute of the file could not be set.  Either a protection problem
//  or the file does not exist altogether.

#define TIMECODE_ERROR_BLANK -1505
//  A blank, e.g, blank or only whitespace, string has been entered as a timecode

#define TIMECODE_ERROR_UNKNOWN_INTERNAL -1506
//  An internal unknown error has happened.  Most likely some code space has
//  been corrupted.

#define TIMECODE_ERROR_ILLEGAL_CHARACTERS -1507
//  The timecode string contains is not a digit or :, space, or .

#define TIMECODE_ERROR_TOO_LONG -1508
//  The timecode string conains more than 4 fields

#define INIFILE_ERROR_ACCESS_LOCALMACHINEFILE -1509
// An error occurred when accessing local machine file. 
//   $(CPMP_LOCAL_DIR)MTILocalMachine.ini
// This usually happens
// if the user does not have the priv. to write to the directory or file and
// some key must be written.  Most likey an improperly set up system

#define INIFILE_ERROR_ACCESS_MASTERFILE -1510
// An error occurred when accessing master file. 
//  <execution directory>MTIcpmp.ini
// This usually happens
// if the user does not have the priv. to write to the directory or file and
// some key must be written.  Most likey an improperly set up system

#define INIFILE_ERROR_NO_TIMING_FILE -1511
// The timing file "$(CPMP_LOCAL_DIR)timing.cfg" does not exist
// Default is /mtiimage/MTISystem/timing.cfg.
// This represents a major problem in the installation of the system.
// Check also to see if the Local Machine File exists and this entry
// points to the correct location

#define INIFILE_ERROR_NO_COMPUTERTYPE -1512
// In the local machine file (usually MTILocalMachine.INI), the entry
// [General]
// ComputerType=??
// Does not exist.  This is a result of a bad install.  Define this
// type by looking at timing.cfg.

#define FILESWEEPER_ERROR_COPY_FAILED -1513
//  A CopyFile system call failed.


#endif
