// MTImalloc.cpp
//
//     Memory allocation functions.
//
//     In 64-bit world, we will just call the Windows functions.
//
///////////////////////////////////////////////////////////////////////////////

#define __IN_MTI_MALLOC__
#include "machine.h"
#include "MTImalloc.h"
#undef __IN_MTI_MALLOC__

#include <exception> // for std::bad_alloc

#include "IniFile.h"    // for TRACE_MEM_N() !!!
#include "TheError.h"
#include "ThreadLocker.h"
#include <stddef.h>    // for _threadid macro
#include <map>
#include <ippcore.h>
#include <ipps.h>

// There's no memalign, so need to simulate it
static std::map<void *, void *> memalignMap;
static CSpinLock memalignLock;

//#ifdef _DEBUG
#define USE_FENCEPOSTS
//#endif

#ifdef USE_FENCEPOSTS
static std::map<void *, unsigned int> fencepostMap;
static CSpinLock fencepostLock;
#endif

#define LOG_MALLOCS
#ifdef LOG_MALLOCS
const string logFileName = "C:\\tmp\\__MALLOC__.log";
static FILE *logFile = nullptr;
static size_t mallocedBytes = 0;
static size_t memalignedBytes = 0;
#endif

///////////////////////////////////////////////////////////////////////////////

void *MTImalloc(size_t n)
{
#ifdef LOG_MALLOCS
   if (logFile == nullptr)
   {
      logFile = fopen (logFileName.c_str(), "w");
   }

   if (logFile != nullptr)
   {
      mallocedBytes += n;
      fprintf(logFile, "M %zu %zu\n", n, mallocedBytes);
   }
#endif
#ifdef USE_FENCEPOSTS
	unsigned char *bytes =  (unsigned char *) malloc(n + 2);
	if (bytes == nullptr)
	{
		return nullptr;
	}

   bytes[n] = 0x92;
   bytes[n + 1] = 0x65;
   {
      CAutoSpinLocker lock(fencepostLock);
      fencepostMap[bytes] = (unsigned int) n;
   }
   return bytes;
#else
   return malloc(n);
#endif
}
//-----------------------------------------------------------------------

void *MTIcalloc(size_t n_elements, size_t element_size)
{
#ifdef USE_FENCEPOSTS
   size_t n = n_elements * element_size;
   unsigned char *bytes =  (unsigned char *) MTImalloc(n);
   for (size_t i = 0; i < n; ++i)
   {
      bytes[i] = 0;
   }
   return bytes;
#else
  return calloc(n_elements, element_size);
#endif
}
//-----------------------------------------------------------------------

void MTIfree(void *p)
{
   // HACK memory allocator is deleted too soon so just screw deallocation
   // on exit
   if (IsProgramExiting())
   {
      return;
   }

   if (p == 0)
   {
      return;
   }

#ifdef MALLOC_LOG
   size_t nBytesFreed = 0;
#endif

#ifdef USE_FENCEPOSTS

   // Beware of nearly identical code in MTIcheckFencepostOverrun()
   {
      CAutoSpinLocker lock(fencepostLock);

      // Careful! map[foo] creates an entry for foo if it didn't exist
      // so that's why we do a find first
      std::map<void *, unsigned int>::iterator iter = fencepostMap.find(p);
      MTIassert(iter != fencepostMap.end());
      if (iter != fencepostMap.end())
      {
         unsigned int n = (*iter).second;
         unsigned char *cp = (unsigned char *) p;
         MTIassert(cp[n] == 0x92 && cp[n + 1] == 0x65);
         fencepostMap.erase(iter);
#ifdef MALLOC_LOG
         nBytesFreed = n;
#endif
      }
   }
#endif

   {
      CAutoSpinLocker lock(memalignLock);

      // Careful! map[foo] creates an entry for foo if it didn't exist
      // so that's why we do a find first
      std::map<void *, void *>::iterator iter = memalignMap.find(p);
      if (iter != memalignMap.end())
      {
         p = memalignMap[p];
         memalignMap.erase(iter);
         VirtualFree(p, 0, MEM_RELEASE);
#ifdef MALLOC_LOG
         memalignedBytes -= nBytesFreed;
#endif
         return;
      }
   }

   free(p);
#ifdef MALLOC_LOG
   mallocedBytes -= nBytesFreed;
#endif
}
//-----------------------------------------------------------------------

void *MTIrealloc(void *p, size_t n)
{
#ifdef USE_FENCEPOSTS
	if (p == 0)
   {
      return 0;
   }

   void *newBytes = MTImalloc(n);

   size_t oldN = 0;
   {
      CAutoSpinLocker lock(fencepostLock);

      // Careful! map[foo] creates an entry for foo if it didn't exist
      // so that's why we do a find instead
      std::map<void *, unsigned int>::iterator iter;
      iter = fencepostMap.find(p);
      MTIassert(iter != fencepostMap.end());
      if (iter != fencepostMap.end())
      {
         oldN = (*iter).second;
      }
   }

   if (oldN > 0 && newBytes != 0)
   {
      MTImemcpy(newBytes, p, oldN);
   }

   MTIfree(p);

   return newBytes;

#else
   return realloc(p, n);
#endif
}
//-----------------------------------------------------------------------

void *MTImemalign(size_t alignment, size_t n)
{
#ifdef LOG_MALLOCS
   if (logFile == nullptr)
   {
      logFile = fopen (logFileName.c_str(), "w");
   }

   if (logFile != nullptr)
   {
      memalignedBytes += n;
      fprintf(logFile, "A %zu %zu\n", n, memalignedBytes);
   }
#endif

   auto allocN = n;

#ifdef USE_FENCEPOSTS
   allocN += 2;
#endif

   // VirtualAlloc aligns to page boundary, so no need for padding,
   if (alignment != 4096 && alignment != 0)
   {
      allocN += 2 * alignment;     // QQQ why "2 *" ?
   }

   unsigned char *unaligned_vp = (unsigned char *)::VirtualAlloc(nullptr, allocN, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
   MTI_UINT64 unaligned_vp_as_uint64 = reinterpret_cast<MTI_UINT64>(unaligned_vp);
   unsigned char *vp = reinterpret_cast<unsigned char *>
      (alignment * ((unaligned_vp_as_uint64 + alignment - 1) / alignment));

   MTIassert((alignment != 4096 && alignment != 0) || vp == unaligned_vp);

   {
      CAutoSpinLocker lock(memalignLock);
      memalignMap[vp] = unaligned_vp;
   }

#ifdef USE_FENCEPOSTS
   vp[n] = 0x92;
   vp[n + 1] = 0x65;
   {
      CAutoSpinLocker lock(fencepostLock);
      MTI_UINT64 vp_as_uint64 = reinterpret_cast<MTI_UINT64>(vp);
      fencepostMap[unaligned_vp] = (vp_as_uint64 - unaligned_vp_as_uint64) +  n;
   }
#endif

   return vp;
}
//-----------------------------------------------------------------------

// Override global operator new
void* MTInew(size_t size)
{
   void *p = MTImalloc(size);
   if (p == 0)
   {
      throw std::bad_alloc(); // ANSI/ISO compliant behavior
   }

   return p;
}
//-----------------------------------------------------------------------

// Override global operator new[]
void* MTInewArray(size_t size)
{
   void *p = MTImalloc(size);
   if (p == 0)
   {
      throw std::bad_alloc(); // ANSI/ISO compliant behavior
   }

   return p;
}
//-----------------------------------------------------------------------

// Override global operator delete
void MTIdelete (void *p)
{
   MTIfree(p);
}
//-----------------------------------------------------------------------

// Override global operator delete[]
void MTIdeleteArray (void *p)
{
   MTIfree(p);
}
//-----------------------------------------------------------------------

char* MTIstrdup(const char *cp)
{
   size_t size = 0;
   if (cp != NULL)
      size = strlen(cp);
   char * dp = (char *) MTImalloc (size + 1);
   memcpy(dp, cp, size + 1);   // includes null terminator

   return dp;
}
//-----------------------------------------------------------------------

void MTImemcpy(void *pDst, const void *pSrc, size_t n)
{
	if (n < 1)
	{
		return;
	}

#ifdef USE_FENCEPOSTS
   if (n < 1)
   {
      return;
   }

	// ippsCopy messes up the stack if it causes an exception, so test ability
	// to read/write each page.
	for (int i = 0; i < n; i += 4096)
	{
		((char *)pDst)[i] = ((char *)pSrc)[i];
	}

	// Special case last byte.
	char c = *(((char *)pSrc) + n - 1);
	*(((char *)pDst) + n - 1) = c;
#endif

	ippsCopy_8u((const Ipp8u*)pSrc, (Ipp8u*)pDst, n);
}
// -----------------------------------------------------------------------

void MTImemset(void *pDst, unsigned char fillChar, size_t n)
{
	if (n < 1)
	{
		return;
	}

#ifdef USE_FENCEPOSTS
   if (n < 1)
   {
      return;
   }

	// ippsSet messes up the stack if it causes an exception, so test ability
	// to write each page.
	for (int i = 0; i < n; i += 4096)
	{
		((char *)pDst)[i] = fillChar;
	}

	// Special case last byte.
	*(((char *)pDst) + n - 1) = fillChar;
#endif

	ippsSet_8u((Ipp8u)fillChar, (Ipp8u*)pDst, n);
}

//-----------------------------------------------------------------------

// NOTE This is the same as the code in MTIfree, except it doesn't delete
// the pointer's entry in the fence post map.
bool  MTIcheckFencepostOverrun(void *p)
{
#ifdef USE_FENCEPOSTS
	CAutoSpinLocker lock(fencepostLock);

	// Careful! map[foo] creates an entry for foo if it didn't exist
	// so that's why we do a find first
	std::map<void *, unsigned int>::iterator iter = fencepostMap.find(p);
//	MTIassert(iter != fencepostMap.end());
	if (iter != fencepostMap.end())
	{
		unsigned int n = (*iter).second;
		unsigned char *cp = (unsigned char *) p;
		MTIassert(cp[n] == 0x92 && cp[n + 1] == 0x65);
      if (cp[n] != 0x92 || cp[n + 1] != 0x65)
      {
         return true;
      }
	}
#endif

   return false;
}
//-----------------------------------------------------------------------
