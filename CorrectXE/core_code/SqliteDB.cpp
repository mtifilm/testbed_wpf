// ---------------------------------------------------------------------------

#pragma hdrstop
// ---------------------------------------------------------------------------
#pragma package(smart_init)

#include "SqliteDB.h"

CSqliteDB::CSqliteDB()
{
    _isConnected = false;
    _connectionObject = new SQLITECONNECTIONOBJECT();
    Sync = new SyncDB();
}

CSqliteDB::~CSqliteDB()
{
    closeConnection();
    delete _connectionObject;
    delete Sync;
}

void CSqliteDB::closeConnection()
{
    if (_connectionObject->connection)
    {
        sqlite3_close(_connectionObject->connection);
        _connectionObject->connection = nullptr;
    }

    _isConnected = false;
}

string CSqliteDB::getLastError() const {return _lastError;}

int CSqliteDB::getLastErrorCode() const {return _lastErrorCode;}

bool CSqliteDB::isConnected() const {return _isConnected;}

bool CSqliteDB::openConnection(const string &databaseName, const string &databaseDir)
{
    // Add a \\ if necessary
    _connectionObject->SQLiteDatabaseName = databaseName;
    _connectionObject->SQLiteDBPath = databaseDir.back() == '\\' ? databaseDir : databaseDir + '\\';

    _isConnected = true;

    string db = _connectionObject->SQLiteDatabaseName;
    string dir = _connectionObject->SQLiteDBPath;
    string path = dir.append(db);

    int rc = sqlite3_open(path.c_str(), &(_connectionObject->connection));
    _lastError = (string)sqlite3_errmsg(_connectionObject->connection);
    _lastErrorCode = sqlite3_errcode(_connectionObject->connection);

    if (!rc)
    {
        if (_lastError.find("not an error") == string::npos)
            _isConnected = false;
    }

    return _isConnected;
}

void CSqliteDB::beginTransaction() {
    sqlite3_exec(_connectionObject->connection, "BEGIN TRANSACTION", nullptr, nullptr, nullptr);}

void CSqliteDB::commitTransaction() {
    sqlite3_exec(_connectionObject->connection, "COMMIT TRANSACTION", nullptr, nullptr, nullptr);}

void CSqliteDB::rollbackTransaction() {
    sqlite3_exec(_connectionObject->connection, "ROLLBACK TRANSACTION", nullptr, nullptr, nullptr);}

IResult* CSqliteDB::executeSelect(const char *Query)
{
    if (!isConnected())
    {
        return nullptr;
    }

    Sync->LockDB();

    if (sqlite3_prepare_v2(_connectionObject->connection, Query, -1, &_connectionObject->statement,
        nullptr) != SQLITE_OK)
    {
        _lastError = sqlite3_errmsg(_connectionObject->connection);
        _lastErrorCode = sqlite3_errcode(_connectionObject->connection);
        sqlite3_finalize(_connectionObject->statement);
        Sync->UnLockDB();
        ThrowOnSqlError(_lastErrorCode);
    }

    // NOTE: unlockDB is done on release
    _columnCount = sqlite3_column_count(_connectionObject->statement);
    IResult *ires = this;
    return ires;
}

UINT CSqliteDB::execute(const char *query)
{
    if (!isConnected())
    {
        throw std::runtime_error("CSqlite connection not open for execute");
    }

    _lastError = "";
    _lastErrorCode = SQLITE_OK;
    char* err = nullptr;

    if (sqlite3_exec(_connectionObject->connection, query, nullptr, 0, &err) != SQLITE_OK)
    {
        _lastErrorCode = sqlite3_errcode(_connectionObject->connection);
        _lastError = err;
        sqlite3_free(err);
        throw std::runtime_error(string("CSqlite execute failed\n") + _lastError);
    }

    return sqlite3_total_changes(_connectionObject->connection);
}

/* Result Set Definations */
int CSqliteDB::getColumnCount() {return _columnCount;}

const char* CSqliteDB::nextColumnName(int count)
{
    if (count > _columnCount)
    {
        return "";
    }

    return sqlite3_column_name(_connectionObject->statement, count);
}

bool CSqliteDB::next() {return (sqlite3_step(_connectionObject->statement) == SQLITE_ROW) ? true : false;}

const char* CSqliteDB::columnData(int index)
{
    if (index > _columnCount)
    {
        return "";
    }

    return ((const char*)sqlite3_column_text(_connectionObject->statement, index));
}

string CSqliteDB::columnDataAsString(int index)
{
    if (index > _columnCount)
    {
        throw std::runtime_error("Column index out of range");
    }

    return string(columnData(index));
}

int CSqliteDB::columnDataAsInteger(int index)
{
    if (index > _columnCount)
    {
        throw std::runtime_error("Column index out of range");
    }

    return sqlite3_column_int(_connectionObject->statement, index);
}

double CSqliteDB::columnDataAsDouble(int index)
{
    if (index > _columnCount)
    {
        throw std::runtime_error("Column index out of range");
    }

    return sqlite3_column_double(_connectionObject->statement, index);
}

void CSqliteDB::release()
{
    sqlite3_finalize(_connectionObject->statement);
    _columnCount = 0;
    _lastError = "";
    _lastErrorCode = SQLITE_OK;
    Sync->UnLockDB();
}

void CSqliteDB::ThrowOnSqlError(int returnCode)
{
    if (returnCode == SQLITE_OK)
    {
        return;
    }

    throw std::runtime_error(string(sqlite3_errstr(returnCode)));
}

int64_t CSqliteDB::getLastInsertRowID() const {return sqlite3_last_insert_rowid(getConnection());}
