/** @file BackupSet.hpp
 *
 * Abstract interface for CBackupSet.
 */

#ifndef BACKUPSET_HPP
#define BACKUPSET_HPP

#include "corecodelibint.h"
#include <string>

/** @brief CBackupSet encapsulates the identifier of a backup set.
 *
 * The backup set identifier must be representable as a string.
 */
class MTI_CORECODELIB_API CBackupSet
{
public:
    /** Accessor to retrieve the string representation of the CBackupSet. */
    std::string GetBackupSetString() const;

protected:
    /** Construct a CBackupSet with a new unique backup set identifier. */
    CBackupSet();

    /** Construct a CBackupSet using an existing identifier. */
    CBackupSet(std::string const &aBackupSetString);

    std::string mBackupSetString;
};

#endif
