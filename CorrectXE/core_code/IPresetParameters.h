#ifndef IPresetParametersH
#define IPresetParametersH

#include "IniFile.h"
// ---------------------------------------------------------------------------
//
// Abstract interface that reads/writes preset values to an inifile
// Basically the PresetParametersModel keeps a list and calls these
// functions when necessary.
//
// SectionName is section in inifile and has the default value Preset0, Preset1, ...
//
// ---------------------------------------------------------------------------
class IPresetParameters
{
public:
    virtual void ReadParameters(CIniFile *ini, const string &iniSectionName) = 0;
    virtual void WriteParameters(CIniFile *ini, const string &iniSectionName) const = 0;
    virtual bool AreParametersTheSame(const IPresetParameters &presetParameter) const = 0;

	 int Tag = -1;
	 CBHook ParametersChanged;
};

#endif

