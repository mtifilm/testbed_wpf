#include "MTIstringstream.h"
#ifdef _WIN64  // Rad Studio bug in 64-bits only (string strams not thread-safe)

CSystemThreadLock *MTIstringstream::Lock = NULL;

MTIstringstream::MTIstringstream()
{
   if (Lock == NULL)
   {
      Lock = new CSystemThreadLock("mtistringstreamlock");
   }
}

MTIstringstream::~MTIstringstream()
{
}

MTIostringstream::MTIostringstream()
{
   CAutoSystemThreadLocker lock(GetLock());
   wrappedStream = new std::ostringstream;
}

MTIostringstream::MTIostringstream(const string &s)
{
   CAutoSystemThreadLocker lock(GetLock());
   wrappedStream = new std::ostringstream(s);
}

MTIostringstream::~MTIostringstream()
{
   CAutoSystemThreadLocker lock(GetLock());
   delete wrappedStream;
}

MTIistringstream::MTIistringstream()
{
   CAutoSystemThreadLocker lock(GetLock());
   wrappedStream = new std::istringstream;
}

MTIistringstream::MTIistringstream(const string &s)
{
   CAutoSystemThreadLocker lock(GetLock());
   wrappedStream = new std::istringstream(s);
}

MTIistringstream::~MTIistringstream()
{
   CAutoSystemThreadLocker lock(GetLock());
   delete wrappedStream;
}
#endif
