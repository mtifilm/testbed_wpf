/*
   File Name  BCD.cpp
   Create:  July 30, 2000 by John Mertus

   This contains the BCD functions.
*/

#include "BCDLib.h"

//-----------IntToBCD--------------------------------JAM--Mar 2000----

  unsigned char IntToBCD(int n)

// Integer to one BCD character
// Input is a integer 0 to 99
// Output is a BCD character
//
//*********************************************************************
{
   return( ((n/10)<<4) + (n%10));
}

//-----------BCDToInt--------------------------------JAM--Mar 2000----

  int BCDToInt(unsigned char uc)

// Integer to one BCD character
// Input is BCD character xy with x and y between 0 and 9
// Output is an integer 0 to 99
//
//*********************************************************************
{
    int iTens = (uc >>4);
    int iOnes = (uc & 0xF);
    return(iTens*10 + iOnes);
}
