//---------------------------------------------------------------------------

#ifndef MathFunctionsH
#define MathFunctionsH

#include "corecodelibint.h"

#include <valarray>
#include "machine.h"
#include "tarray.h"

//---------------------------------------------------------------------------
// General mathematical functions

MTI_CORECODELIB_API bool Smooth3(std::valarray<double> &MC, int nSFixed, int nEFixed, double Alpha,
                                 int *percentDone=NULL, bool *abortFlag=NULL);
MTI_CORECODELIB_API void Smooth2(std::valarray<double> &MC, int nSFixed, int nEFixed, double Alpha,
                                 int *percentDone, bool *abortFlag);

// U Functions
//MTI_CORECODELIB_API float UEuclidean(float X0, float Y0, float X1, float Y1);
//MTI_CORECODELIB_API float UThinPlate(float X0, float Y0, float X1, float Y1);
//MTI_CORECODELIB_API float ULogrithmic(float X0, float Y0, float X1, float Y1);
//MTI_CORECODELIB_API float UIdenity(float X0, float Y0, float X1, float Y1);

MTI_CORECODELIB_API int L1Norm(MTI_UINT16 *p1, MTI_UINT16 *p2, int N);
MTI_CORECODELIB_API double ComputeL1BoxNorm(MTI_UINT16 *pBase, int nB, MTI_UINT16 *pNext, int nN, int x, int y);
MTI_CORECODELIB_API double ComputeL1BoxNorm(MTI_UINT16 *pBase, int nBx, int nBy, MTI_UINT16 *pNext, int nN, int x, int y);
MTI_CORECODELIB_API void CopySubImage(MTI_UINT16 *imgBase, int nCol, int nRow, double x, double y, int N, MTI_UINT16 *imgOut);
MTI_CORECODELIB_API void CopySubImage(MTI_UINT16 *imgBase, int nCol, int nRow, double xul, double yul, int Nx, int Ny, MTI_UINT16 *imgOut);
MTI_CORECODELIB_API void CreateSubPixelMatrix2(MTI_UINT16 *pIn, int nCol, double x, double y, int N, MTI_UINT16 *pOut);
MTI_CORECODELIB_API void CreateSubPixelMatrix2x(MTI_UINT16 *pIn, int nCol, double x, double y, int Nx, int Ny, MTI_UINT16 *pOut);
MTI_CORECODELIB_API void CreateSubPixelMatrix4x(MTI_UINT16 *pIn, int nCol, double x, double y, int Nx, int Ny, MTI_UINT16 *pOut);
MTI_CORECODELIB_API void CreateSubPixelMatrix5x(MTI_UINT16 *pIn, int nCol, double x, double y, int Nx, int Ny, MTI_UINT16 *pOut);
MTI_CORECODELIB_API void CreateSubPixelMatrix6x(MTI_UINT16 *pIn, int nCol, double x, double y, int Nx, int Ny, MTI_UINT16 *pOut);
MTI_CORECODELIB_API void CreateSubPixelMatrix7x(MTI_UINT16 *pIn, int nCol, double x, double y, int Nx, int Ny, MTI_UINT16 *pOut);
MTI_CORECODELIB_API void CreateSubPixelMatrix8x(MTI_UINT16 *pIn, int nCol, double x, double y, int Nx, int Ny, MTI_UINT16 *pOut);
MTI_CORECODELIB_API void CreateSubPixelMatrix10x(MTI_UINT16 *pIn, int nCol, double x, double y, int Nx, int Ny, MTI_UINT16 *pOut);
MTI_CORECODELIB_API double BiLinear( double x, double y, double C00, double C10, double C01, double C11);
MTI_CORECODELIB_API void MakeMonoChromeImage(MTI_UINT16 *pIn, int nCol, int nRow, int nBits, int channel, MTI_UINT8 *lutPtr, MTI_UINT16 *pOut);
MTI_CORECODELIB_API void pearsn(double x[], double y[], unsigned long n, double &r, double &prob, double &z);
MTI_CORECODELIB_API RECT FindMatBox(MTI_UINT16 *pIn, int nCol, int nRow, int Threshold, RECT &inRect);
MTI_CORECODELIB_API RECT FindMatBox(MTI_UINT16 *pIn, int nCol, int nRow, int Threshold, RECT &inRect, bool RGB, int R, int G, int B, int matX=0, int matY=0);
MTI_CORECODELIB_API void EstimateMinFrom9Points(double D[9], double &x, double &y);
MTI_CORECODELIB_API bool robust(const mvector<double> &y, const matrix<double> &X,
                                double T,  /* OUTPUT */ mvector<double> &b);

MTI_CORECODELIB_API void setRandSeed(unsigned long seed);
MTI_CORECODELIB_API double getRandNormal();
MTI_CORECODELIB_API RECT FindMatBox(MTI_UINT16 *pIn, int nCol, int nRow, int Threshold, RECT &inRect, bool RGB);

MTI_CORECODELIB_API float interpolateMonoCubically(const vector<float> &xsArg, const vector<float> ysArg, float x);

#define MATH_ERROR_SMOOTHING_TOO_FEW_FRAMES  -32769
#define MATH_ERROR_SMOOTHING_MATRIX_SINGULAR -32770

template <typename It>
class RangeView {
public:
  typedef It iterator;

  RangeView(): _begin(), _end() {}
  RangeView(iterator begin, iterator end): _begin(begin), _end(end) {}

  iterator begin() const { return _begin; }
  iterator end() const { return _end; }

private:
  iterator _begin;
  iterator _end;
};

template <typename C>
RangeView<typename C::iterator> rangeView(C& c, size_t begin, size_t end) {
  return RangeView<typename C::iterator>(
           std::next(c.begin(), begin),
           std::next(c.begin(), end)
         );
}

template <typename C>
RangeView<typename C::const_iterator> rangeView(C const& c, size_t begin, size_t end) {
  return RangeView<typename C::const_iterator>(
           std::next(c.begin(), begin),
           std::next(c.begin(), end)
         );
}

#endif


