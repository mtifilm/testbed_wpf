/*
	File:	ComputerType.h
	By:	Kevin Manbeck
	Date:	Feb 16, 2002

	The ComputerType class keep track of which hardware configuration
	is currently in use.

	The following public functions are available in the ComputerType class:

 	queryComputerType (string strComputerType)
		This function converts a string to the value of the enum.

	queryComputerTypeName (EComputerType ctComputerType)
		This function converts a value of the enum to a string.

*/
#ifndef COMPUTERTYPE_H
#define COMPUTERTYPE_H


#include <string>
using std::string;

/*
	The EComputerType enumerator is use to describe various hardware
	configurations.
*/

enum EComputerType
{
  CT_INVALID = 0,		// illegal value
  CT_ONYX2_DIVO_XTHD = 1,	// onyx2 with DIVO and XTHD
  CT_OCTANE1_IMPACT = 2,	// octane 1 with IMPACT
  CT_O2_PROFESSIONAL = 3,	// O2 with professional video
  CT_O2_PERSONAL = 4,		// O2 with personal video
  CT_OCTANE2_DM2 = 5,		// octane 2 with DM2 video
  CT_ORIGIN2_DIVO_XTHD = 6,	// origin 200 or 2000 with DIVO and XTHD
  CT_ORIGIN2_DM3 = 7,		// origin 200 or 2000 with DM3 video
  CT_ORIGIN3_DM3 = 8		// origin 300 or 3000 with DM3 video
};

class CComputerType
{
public:
  static EComputerType queryComputerType (const string &strComputerType);
  static string queryComputerTypeName (EComputerType ctComputerType);
};

#endif // #ifndef COMPUTERTYPE_H
