// ---------------------------------------------------------------------------

#ifndef SqliteDBH
#define SqliteDBH
// ---------------------------------------------------------------------------

/******************************************************************************************
 Based on a class by Saurabh Bhushan, hacked by John Mertus

 This Class provide the simple wrapper for SQLite DB.
 ***************************************************************************************** */

// OS Headers
#include "machine.h"
#include "err_defs.h"
#include "corecodelibint.h"
#include "FileSweeper.h"
#include "MTIsleep.h"
#include <Windows.h>
#include <string>
#include <stddef.h>

// SQLite Header and library
#include "sqlite3.h"

// Struct for Sync database for Multithreading
typedef struct Sync
{
    CRITICAL_SECTION _cs;

    Sync() {::InitializeCriticalSection(&_cs);}

    void LockDB() {::EnterCriticalSection(&_cs);}

    void UnLockDB() {::LeaveCriticalSection(&_cs);}

    ~Sync() {::DeleteCriticalSection(&_cs);}
} SyncDB;

/* Interface class for Result data of query */
class MTI_CORECODELIB_API IResult
{
public:
    /* This function return of count of column
     present in result set of last excueted query */
    virtual int getColumnCount() = 0;

    /* Get the next coloumn name */
    virtual const char* nextColumnName(int count) = 0;

    /* This function returns TRUE if still rows are
     der in result set of last excueted query FALSE
     if no row present */
    virtual bool next() = 0;

    /* Get the next coloumn data */
    virtual const char* columnData(int clmNum) = 0;
    virtual int columnDataAsInteger(int index) = 0;
    virtual double columnDataAsDouble(int index) = 0;
    virtual string columnDataAsString(int index) = 0;

    /* RELEASE all result set as well as RESET all data */
    virtual void release() = 0;
};

// SQLite Wrapper Class
class MTI_CORECODELIB_API CSqliteDB : public IResult
{

public:
    CSqliteDB();
    virtual ~CSqliteDB();

    /* Open Connection */
    bool openConnection(const string &databaseName, const string &databaseDir);

    /* Close Connection */
    void closeConnection();

    /* Query Wrapper */
    /* For large insert operation Memory Insert option for SQLLITE dbJournal */
    void beginTransaction();
    void commitTransaction();
    void rollbackTransaction();

    /* This Method called when SELECT Query to be excuted.
     Return RESULTSET class pointer on success else NULL of failed */
    IResult* executeSelect(const char *query);

    IResult* executeSelect(const string &query) const
    {
        auto constThis = const_cast<CSqliteDB*>(this);
        return constThis->executeSelect(query.c_str());
    }

    /* This Method called when INSERT/DELETE/UPDATE Query to be excuted.
     Return UINT count of effected data on success */
    UINT execute(const char *query);

    UINT execute(const string &query)
    {
        auto constThis = const_cast<CSqliteDB*>(this);
        return constThis->execute(query.c_str());
    }

    /* Get Last Error of excution */
    string getLastError() const;
    int getLastErrorCode() const;
    int64_t getLastInsertRowID() const;

    /* Return TRUE if databse is connected else FALSE */
    bool isConnected() const;

    sqlite3 *getConnection() const {return _connectionObject->connection;}

    string getDatabaseName() const {return _connectionObject ? _connectionObject->SQLiteDatabaseName : "";}

    string getDatabaseDir() const {return _connectionObject ? _connectionObject->SQLiteDBPath : "";}

    string getDatabaseFullName() const {return AddDirSeparator(getDatabaseDir()) + getDatabaseName();}

    static void ThrowOnSqlError(int returnCode);

protected:
    /* SQLite Connection Object */
    typedef struct SQLLITEConnection
    {
        string SQLiteDatabaseName; // Database Name
        string SQLiteDBPath; // Databse File Dir

        sqlite3 *connection = nullptr; // SQLite Connection Object
        sqlite3_stmt *statement = nullptr; // SQLite statement object
    } SQLITECONNECTIONOBJECT;

    // SQLite Connection Details
    SQLITECONNECTIONOBJECT *_connectionObject = nullptr;

    /* Sync Database in Case of Multiple Threads using class object */
    SyncDB *Sync;

    bool _isConnected; /* Is Connected To DB */
    string _lastError; /* Last Error String */

    int _lastErrorCode = SQLITE_OK;

    int _columnCount; /* No.Of Column in Result */

private:
    /* This function return of count of column
     present in result set of last excueted query */
    int getColumnCount();

    /* Get the next coloumn name */
    const char* nextColumnName(int count);

    /* This function returns TRUE if still rows are
     der in result set of last excueted query FALSE
     if no row present */
    bool next();

    /* Get the next coloumn data */
    const char* columnData(int index);
    int columnDataAsInteger(int index);
    double columnDataAsDouble(int index);
    string columnDataAsString(int index);

    /* RELEASE all result set as well as RESET all data */
    void release();

};

#endif
