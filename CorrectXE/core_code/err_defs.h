/*
   File Name:  err_defs.h
   Created: May 20, 2002 by John Mertus
   Version 1.00

   This defines the range of error numbers for each module and is used to
   reserve the error numbers

   This file is included in the MTI error processing header TheError.h and
   need not be directly included.

   Rules for allocating error numbers
      The list of error number are sorted by values, lowest to highest.
      -1 and 0 cannot be uniquely assigned but may be used for generic indications
      of failure and success.

      1)  Check err_defs.h out from CVS under the core_code module

      2) Copy an existing section after (or before) the numbers you wish to
      allocate.   Change the appropriate comments, owner and ranges.

      3) Check err_defs.h file back into cvs.

      Meta rules for modules errors
      Build a header with the name
         err_xxxx.h     where xxxx is your module name; e.g.,
         err_clip.h

      In the module assign symbolic names to numbers, e.g.,

      #define CLIP_ERROR_RAW_OGLV_DISK_WRITE_FAILED        -2119

      In the code, use only this symbolic error name, not the number.

      Note:  One error module may be share across many classes, but should not
      be shared accross different CVS modules.

      Second, major programs may have ranges that logically break apart
      the different modules.  
*/

#ifndef ERR_DEFS_H
#define ERR_DEFS_H

//
//-----------------------------------------------------------------------
//  Thread errors
//
#define  THRD_ERROR_LOWEST    -10099
#define  THRD_ERROR_HIGHEST   -10000

//
//-----------------------------------------------------------------------
//  Color brething error range  -  John Mertus
//
#define  CB_ERROR_LOWEST    -9999
#define  CB_ERROR_HIGHEST   -9900

//
//-----------------------------------------------------------------------
//  Dewarp error range  -  John Mertus
//
#define  DEWARP_ERROR_LOWEST    -8099
#define  DEWARP_ERROR_HIGHEST   -8000

//
//-----------------------------------------------------------------------
//  EDL error range  - Kevin Manbeck
//
#define  EDL_ERROR_LOWEST    -7999
#define  EDL_ERROR_HIGHEST   -7900

//
//-----------------------------------------------------------------------
//  Hippi library error range  - Kevin Manbeck
//
#define  HIPPI_ERROR_LOWEST    -7899
#define  HIPPI_ERROR_HIGHEST   -7800

//
//-----------------------------------------------------------------------
//  PDL library error range  - John Starr
//
#define  PDL_ERROR_LOWEST    -7799
#define  PDL_ERROR_HIGHEST   -7700

//
//-----------------------------------------------------------------------
//  Licensing Errors - John Mertus
//
#define  FLEX_LIC_ERROR_LOWEST    -7649
#define  FLEX_LIC_ERROR_HIGHEST   -7600

//
//-----------------------------------------------------------------------
//  BinManager - Kevin Manbeck
//
#define  BIN_MANAGER_ERROR_LOWEST    -7599
#define  BIN_MANAGER_ERROR_HIGHEST   -7550

//
//-----------------------------------------------------------------------
//  Logger - Kevin Manbeck
//
#define  LOGGER_ERROR_LOWEST    -7549
#define  LOGGER_ERROR_HIGHEST   -7500

//
//-----------------------------------------------------------------------
//  PlayList - Kevin Manbeck
//
#define  PLAYLIST_ERROR_LOWEST    -7499
#define  PLAYLIST_ERROR_HIGHEST   -7150

//
//-----------------------------------------------------------------------
//  Grain Reduction - Kevin Kochanek
//
#define  GRAIN_ERROR_LOWEST    -7149
#define  GRAIN_ERROR_HIGHEST   -7100

//
//-----------------------------------------------------------------------
//  PDL Lib error range  - Kevin Manbeck
//
#define  PDL_LIB_ERROR_LOWEST    -7099
#define  PDL_LIB_ERROR_HIGHEST   -7050

//
//-----------------------------------------------------------------------
//  Persistent scratch error range  - Kevin Manbeck
//
#define  PERSISTENTSCRATCH_ERROR_LOWEST    -7049
#define  PERSISTENTSCRATCH_ERROR_HIGHEST   -7000

//
//-----------------------------------------------------------------------
//  File error range
//
#define  FILE_ERROR_LOWEST    -6199
#define  FILE_ERROR_HIGHEST   -6000

//
//-----------------------------------------------------------------------
//  MetaData library error range  - Kevin Manbeck
//
#define  METADATA_ERROR_LOWEST    -5349
#define  METADATA_ERROR_HIGHEST   -5250

//
//-----------------------------------------------------------------------
//  format error range  - Matt McClure
//
#define  FORMAT_ERROR_LOWEST    -5200
#define  FORMAT_ERROR_HIGHEST   -5050

//
//-----------------------------------------------------------------------
//  autofilter plugin error range  - Kevin Manbeck
//
#define  AUTOFILTER_ERROR_LOWEST    -5049
#define  AUTOFILTER_ERROR_HIGHEST   -5000

//
//-----------------------------------------------------------------------
//  imgTool error range  - Kevin Manbeck
//
#define  IMGTOOL_ERROR_LOWEST    -4999
#define  IMGTOOL_ERROR_HIGHEST   -4850
#define  REPAIR_ERROR_LOWEST    -4849
#define  REPAIR_ERROR_HIGHEST   -4750

//
//-----------------------------------------------------------------------
//  Utility error range (libutil) - Kevin Manbeck
//
#define  UTIL_ERROR_LOWEST    -4749
#define  UTIL_ERROR_HIGHEST   -4500


//-----------------------------------------------------------------------
//  Remote edit controller (CRemoteEditCtrl) - John Mertus

//
#define  REMOTE_EC_ERROR_LOWEST    -4499
#define  REMOTE_EC_ERROR_HIGHEST   -4250


//-----------------------------------------------------------------------
//  Edit controller class (CEditCtrl, ECSocketComm, EditController) - John Mertus
//
#define  EDITCTRL_ERROR_LOWEST    -4249
#define  EDITCTRL_ERROR_HIGHEST   -4000

//
//-----------------------------------------------------------------------
//  Tool Library error range - John Starr
//
#define TOOL_ERROR_LOWEST     -3999
#define TOOL_ERROR_HIGHEST    -3000

//
//-----------------------------------------------------------------------
//  Clip error range - John Starr
//
#define  CLIP_ERROR_LOWEST    -2999
#define  CLIP_ERROR_HIGHEST   -2000

//
//-----------------------------------------------------------------------
//  core code errors, (CIniFile, MTIio)  - John Mertus
//
#define  CORE_CODE_ERROR_LOWEST    -1999
#define  CORE_CODE_ERROR_HIGHEST   -1500

//
//-----------------------------------------------------------------------
//  dongle code errors (in systools) - Kevin Manbeck / Mike Russell
//
#define  DONGLE_ERROR_LOWEST    -799
#define  DONGLE_ERROR_HIGHEST   -700

//
//-----------------------------------------------------------------------
//  Sercom error range (sercom) - Kevin Manbeck
//
#define  SERCOM_ERROR_LOWEST    -20
#define  SERCOM_ERROR_HIGHEST   -2




#define MTI_SUCCESS               0            // Generic Success
#define MTI_ERROR                -1            // Generic Failure

// End of err_defs.h
// Put nothing after the next line
#endif

