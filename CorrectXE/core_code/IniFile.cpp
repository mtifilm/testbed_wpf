/*
 File Name:  IniFile.cpp
 Created: Augest 3, 2000 by John Mertus
 Version 1.00

 This is a rework of the DOS ini Library into C++
 with names exactly like TIniFile from Borland


 */
#include "IniFile.h"
#include "FileSweeper.h"
#include "MTIio.h"
#include "MTIstringstream.h"
#include <errno.h>
#include <time.h>
#include <algorithm>
#ifdef _WINDOWS
#include <shlobj.h>
#endif

// static string LogBaseFile="";
// static string DefaultKey="";
// static int iGlobalTraceLevel=0;
// static int iProgramTraceLevel=-2;
// static int iGlobalLogLevel=-2;
// static int iProgramLogLevel=-2;
// static int iGlobalIniKeep=2;

class GlobalIniData
{
public:
	GlobalIniData()
	{
		iGlobalTraceLevel = 0;
		iProgramTraceLevel = -2;
		iGlobalLogLevel = -2;
		iProgramLogLevel = -2;
		iGlobalIniKeep = 2;
	}

	~GlobalIniData()
	{
		iGlobalTraceLevel = 0;
	}

	string MasterIniFileName;
	string LogBaseFile;
	string DefaultKey;

	int iGlobalTraceLevel;
	int iProgramTraceLevel;
	int iGlobalLogLevel;
	int iProgramLogLevel;
	int iGlobalIniKeep;
};

static GlobalIniData *globalIniData = new GlobalIniData();

// An inisection is just a list of the header string and lines after the
// header along with a method to read/write key and values.
//
class IniSection : public StringList
{
public:
	string Name; // Name of section, e.g., xxx if line is [xxx]

	string Key(unsigned int); // Returns the key associated with index i
	bool KeyExists(const string &Ident); // Returns true if Ident is a key
	string Value(const string &Ident); // Returns the value associated with key Ident
	string Value(unsigned idx); // Returns the value associated with index idx
	int Index(const string &Ident); // Return the index of key Ident
	void Delete(unsigned idx); // Removes index from list
	bool WriteFile(FILE *); // Writes out the ini file
	bool WriteStream(ostream &stream); // Writes out the ini file
};

// A InisectionList is a list of pointers to the IniSections
// Note that the IniSectionList OWNS the objects and will delete them
// Thus don't pop off, but Delete
class IniSectionList : public vector<IniSection*>
{
public:
	IniSectionList(void);
	~IniSectionList();
	void Delete(unsigned int i);
	int IniSectionIndex(const string &Name); // Returns index of IniSection
	IniSection *GetIniSection(const string &Name); // Returns pointer to IniSection
	bool WriteFile(FILE *); // Writes out the ini file
	bool WriteStream(ostream &stream); // Writes out the ini file
};

// ----------------CPMPGetDefaultLocalDir------mlm---Jan 2006------
MTI_CORECODELIB_API string CPMPGetDefaultLocalDir()

	// returns the path to the "local" dir
{
	string result = AddDirSeparator(GetProgramDataDir()) + "DRS Nova\\";
	return result;
}

// -------------------GetSystemDir-------------jam---Jun 2014------

MTI_CORECODELIB_API string GetSystemDir(void)

	// returns the path to the Windows/System directory
{
	PWSTR localDir;
	bool success = SUCCEEDED(SHGetKnownFolderPath(FOLDERID_System, 0, NULL, &localDir));
	assert(success);
	string result = WCharToStdString(localDir);
	CoTaskMemFree(localDir);
	return result;
}

// -------------------GetProgramDataDir-------------jam---Jun 2014------

MTI_CORECODELIB_API string GetProgramDataDir(void)

	// returns the path to the %ProgramData%\MTI Film
{
	PWSTR localDir;
	bool success = SUCCEEDED(SHGetKnownFolderPath(FOLDERID_ProgramData, 0, NULL, &localDir));
	assert(success);
	string result = WCharToStdString(localDir) + "\\MTI Film";
	CoTaskMemFree(localDir);
	return result;
}

// ----------------CheckForTruth--------------John Mertus---Aug 2000------

bool CheckForTruth(const string &str)

	// Checks if a string means true, three reason
	// 1,  true, or t
	//
	// ***********************************************************************
{
	return (str == "1") || EqualIgnoreCase(str, "true") || EqualIgnoreCase(str, "t");
}

// ----------------CreateIniFile--------------John Mertus---Aug 2000------

CIniFile *CreateIniFile(const string &cFileName, bool ReadOnlyFlag)

	// This tries to open/create an IniFile
	// If success result is CIniFile, if fail, result is NULL
	//
	// ***********************************************************************
{
	if (cFileName == "")
		return (NULL); // Blank returns NULL;
	CIniFile *Result;
	try
	{
		Result = new CIniFile(cFileName);
		Result->ReadOnly(ReadOnlyFlag);
	}
	catch (...)
	{
		// Create failed sets the global error
		Result = NULL;
	}

	return (Result);
}

// ----------------DestroyIniFile------------Kevin Manbeck---May 2006------

bool DestroyIniFile(const string &cFileName, bool KeepBackupFiles)

	// This tries to remove the ini file and any backup ini files
	// If KeepBackupFiles is true, all the backup ini files will be preserved
	// If success result is true, if fail, result is false
	//
	// ***********************************************************************
{
	if (cFileName == "")
		return false;

	if (KeepBackupFiles == false)
	{
		// files will be deleted with IniLogFile goes out of scope
		CLogFile IniLogFile;
		IniLogFile.SetFileName(cFileName);
		IniLogFile.SetKeep(0);
	}

	// remove the ini file
	if (remove(cFileName.c_str()) != 0)
	{
		TRACE_0(errout << "ERROR: Could not delete file: " << cFileName << endl << "Because: " << strerror
			(errno));

		return false;
	}

	// success
	return true;
}

// ----------------Constructor--------------John Mertus---Aug 2000------

CIniFile::CIniFile(const string &cFileName)

	// This opens up an .ini file with name FileName for reading
	// If the file does not exist, the file is created
	//
	// ***********************************************************************
{
	bModified = false;
	bDiscardChanges = false;
	bReadOnly = false;
	FileName = ConformEnvironmentName(cFileName); // Save the file name
	Sections = new IniSectionList;

	if (!fReadFile())
	{
		TRACE_0(errout << "ERROR: Failed to create ini file: " << cFileName << endl;
		errout << "Because: " << theError.getMessage();
		theError.set(INIFILE_ERROR_CREATE_FAILED, errout.str()));
		throw(this);
	}
}

// ----------------Constructor--------------John Starr ---Aug 2005------

CIniFile::CIniFile(istream &inStream)

	// Creates the CIniFile from a STL input stream
	//
	// ***********************************************************************
{
	bModified = false;
	Sections = new IniSectionList;
	if (!fReadStream(inStream))
	{
		TRACE_0(errout << "ERROR: Failed to create ini file from input stream" << endl;
		theError.set(INIFILE_ERROR_CREATE_FAILED, errout.str()));
		throw(this);
	}
}

// ----------------Destructor----------------John Mertus---Aug 2000------

CIniFile::~CIniFile(void)

	// This writes out all the information and closes the file
	//
	// ***********************************************************************
{
	SaveIfNecessary();
	delete Sections;
}
// ---------------DeleteIniFile---------------John Mertus---Aug 2000------

bool DeleteIniFile(CIniFile *&ini, bool Discard)

	// This deletes ini file, returns true if file data is written out
	// correctly and false if there is an error
	//
	// ***********************************************************************
{
	// Don't save if the ini file is null or Discard is true
	if (ini == NULL)
		return true;
	// mbraca 20090101: GAAAA! LEAKS ini IF Discard==true!!!
	// Just set it, SaveIfNecessary does the right thing
	// if (ini->DiscardChanges(Discard)) return true;
	ini->DiscardChanges(Discard);

	bool Result = true;
	// Try to save the file
	// // mbraca 20090101: added (!ini->DiscardChanges(Discard)) from above
	if (!ini->SaveIfNecessary())
	{
		TRACE_0(errout << "ERROR: Could not write config file " + ini->FileName);
		ini->FileName = ""; // Turn off saving
		Result = false;
	}

	delete ini;
	ini = NULL;
	return (Result);
}

// ---------------DeleteIniFileDiscardChanges--John Mertus---Oct 2005------

bool DeleteIniFileDiscardChanges(CIniFile *&ini)

	// This deletes the inifile and disregards the changes
	//
	// ***********************************************************************
{
	return DeleteIniFile(ini, true);
}

// ----------------DiscardChanges-------------John Mertus---Oct 2005------

bool CIniFile::DiscardChanges(bool Discard)

	// This set the state and returns the state of the Dischard changes
	// If true nothing will be written out
	// false, changes will be written out
	//
	// ***********************************************************************
{
	bDiscardChanges = Discard;
	return bDiscardChanges;
}

// ----------------DiscardChanges-------------John Mertus---Oct 2005------

bool CIniFile::DiscardChanges(void)

	// This returns the state of the Dischard changes
	// If true nothing will be written out
	// false, changes will be written out
	//
	// ***********************************************************************
{
	return bDiscardChanges;
}

// ------------------ReadOnly-----------------John Mertus---Oct 2005------

bool CIniFile::ReadOnly(bool ReadOnly)

	// This set the state and returns the state of the Dischard changes
	// If true nothing will be written out
	// false, changes will be written out
	//
	// ***********************************************************************
{
	bReadOnly = ReadOnly;
	return bReadOnly;
}

// ------------------ReadOnly-----------------John Mertus---Oct 2005------

bool CIniFile::ReadOnly(void)

	// This returns the state of the Dischard changes
	// If true nothing will be written out
	// false, changes will be written out
	//
	// ***********************************************************************
{
	return bReadOnly;
}

// ----------------SaveIfNecessary-----------John Mertus---Aug 2000------

bool CIniFile::SaveIfNecessary(void)

	// This writes out all the information if file has been modified
	//
	// ***********************************************************************
{
	if ((!bModified) || (DiscardChanges()) || ReadOnly())
		return true; // Nothing to do
	if (FileName == "")
		return true; // Nothing to do
	return (fWriteFile());
}

bool CIniFile::IsModified()
{
   return bModified && !DiscardChanges() && !ReadOnly();
}

// Specialized convenience functions
const char *trim(const char *p)
{
	int n = strlen(p);
	for (int i = 0; i < n; i++)
		if ((*p != ' ') && (*p != char(9)))
			return (p);
		else
			p++;
	return (NULL);
}

bool IsHeader(const char *p)
{
	char *t = (char *)trim(p);
	if (t == NULL)
		return (false);
	return (t[0] == '[');
}

//
// A key is simple, the LHS of first = if the = is not
// preceeded by a comment char
//
string GetKey(const string &str)
{
	int k = 0;
	string s;
	for (unsigned j = 0; j < str.size(); j++)
	{
		switch (str[j])
		{
		case '=':
			// Trim off beginning
			k = str.size() - strlen(trim(str.c_str()));
			s = str.substr(k, j - k);
			for (k = s.size() - 1; k >= 0; k--)
				if ((s[k] != ' ') && (s[k] != char(9)))
					break;
			return (s.substr(0, k + 1));

		case ';':
		case '#':
			return ("");
		}
	}

	return ("");
}

bool CommentLine(const string &str)
{
	for (unsigned j = 0; j < str.size(); j++)
	{
		switch (str[j])
		{
		case ' ':
		case char(9):
			break;

		case ';':
		case '#':
			return (true);

		default:
			return (false);
		}
	}
	return (true);
}

// ----------------ReadSections--------------John Mertus---Aug 2000------

void CIniFile::ReadSections(StringList *Strings)

	// This reads all the sections into the string list
	//
	// ***********************************************************************
{
	Strings->clear();
	for (unsigned int i = 0; i < Sections->size(); i++)
		Strings->push_back((*Sections)[i]->Name);
}

// ----------------DeleteKey--------------------John Mertus---Aug 2000------

void CIniFile::DeleteKey(const string &Section, const string &Ident)

	// Section identifies the section in the file that contains the key
	// Ident is the name of the key from to delete
	// No error is returned if Section, Ident do not exist
	//
	// ***********************************************************************
{
	IniSection *is = Sections->GetIniSection(Section);
	if (is == NULL)
		return; // No section, nothing to do
	int idx = is->Index(Ident); // Get the index
	if (idx < 0)
		return;
	is->Delete(idx);
	bModified = true; // We have changed the ini file
}

// -------------------KeyExists--------------John Mertus---Aug 2000------

bool CIniFile::KeyExists(const string &Section, const string &Ident)

	// Return is true if the Section, Ident exist in the ini file
	// False otherwise
	//
	// ***********************************************************************
{
	IniSection *is = Sections->GetIniSection(Section);
	if (is == NULL)
		return (false);
	return (is->KeyExists(Ident));
}

// ----------------SectionExists--------------John Mertus---Aug 2000------

bool CIniFile::SectionExists(const string &Section)

	// Return is true if the Section Section exist in the ini file
	// false otherwise
	//
	// ***********************************************************************
{
	IniSection *is = Sections->GetIniSection(Section);
	return (is != NULL);
}

// ----------------WriteString----------------John Mertus---Aug 2000------

void CIniFile::WriteString(const string &Section, const string &Ident, const string &Value)

	// This creates a line of the form Iden=Value in the section Section
	// If the section, ident does not exist, it creates it
	// Otherwise it repleces the value
	//
	// ***********************************************************************
{
	IniSection *is = Sections->GetIniSection(Section);
	if (is == NULL)
	{
		// No section, go create a new one
		is = new IniSection;
		is->Name = Section;
		is->push_back('[' + Section + ']');
		Sections->push_back(is);
	}

	// See if ident exists
	int idx = is->Index(Ident);
	if (idx < 0)
	{
		// No, create a new one
		is->push_back(Ident + '=' + Value);
		bModified = true;
	}
	else
	{
		// Exists, just replace string if different
		if ((*is)[idx] != Ident + '=' + Value)
		{
			if (ReadOnly())
				throw(this);
			bModified = true;
			(*is)[idx] = Ident + '=' + Value;
		}
	}

}

// ----------------WriteAssociation-----------John Mertus---Jan 2004------

void CIniFile::WriteAssociation(const CIniAssociations &cia, const string &Section,
	const string &Ident, int Value)

	// This creates a line of the form Iden=String in the section Section
	// The string is the value associated
	// Otherwise it repleces the value
	//
	// ***********************************************************************
{
	WriteString(Section, Ident, cia.SymbolicName(Value));
}

// ----------------ReadAssociation-----------John Mertus---Jan 2004------

int CIniFile::ReadAssociation(const CIniAssociations &cia, const string &Section,
	const string &Ident, int Value)

	// This reads the string associated with the ident and then
	// matches the string with the association value, if no match
	// the default is returned
	//
	// ***********************************************************************
{
	string sValue = ReadString(Section, Ident, "");
	if (sValue == "")
		return Value; // Return default if nothing found
	int n;
	if (cia.ValueBySymbolicName(sValue, n))
		return n;
	return Value;
}

// ----------------EraseSection---------------John Mertus---Aug 2000------

void CIniFile::EraseSection(const string &Section)

	// This delects an entire section refered to as Section
	//
	// ***********************************************************************
{
	int idx = Sections->IniSectionIndex(Section); // Find the section
	if (idx < 0)
		return; // Nothing to do
	Sections->Delete(idx);
	bModified = true;
}

// ----------------ReadString-----------------John Mertus---Aug 2000------

string CIniFile::ReadString(const string &Section, const string &Ident, const string &Default)
{
	// Calls ReadString with the default comments
	return ReadString(Section, Ident, Default, COMMENT_CHARS_DEFAULT);
}

string CIniFile::ReadString(const string &Section, const string &Ident, const string &Default,
	const string &CommentChars)

	// Section identifies the section in the file that contains the desired key.
	// Ident is the name of the key from which to retrieve the value.
	// Default is the string value to return if the:
	// Section does not exist.
	// Key does not exist.
	// Data value for the key is not assigned.
	//
	// ***********************************************************************
{
	IniSection *is = Sections->GetIniSection(Section);
	if (is == NULL)
		return (Default); // No section, return default
	string Result = is->Value(Ident); // Get the value
	if (Result == "")
		return (Default); // No key or data not assigned

	// find first comment
	string::size_type idxComment = Result.find_first_of(CommentChars);
	if (idxComment != string::npos)
	{
		// strip off the comment
		Result = Result.substr(0, idxComment);
	}
	else
		if (idxComment == 0)
		{
			// only found a comment
			return (Default);
		}

	// find first non-BLANK
	string::size_type idxFirst = Result.find_first_not_of(" ");
	if (idxFirst == string::npos)
	{
		// only found BLANKS
		return (Default);
	}
	// find final non-BLANK
	string::size_type idxLast = Result.find_last_not_of(" ");
	if (idxLast == string::npos)
	{
		// only found BLANKS
		return (Default);
	}

	// create the substring
	Result = Result.substr(idxFirst, (idxLast - idxFirst + 1));

	return (Result);
}

// ----------------ReadStringCreate-----------John Mertus---Aug 2001------

string CIniFile::ReadStringCreate(const string &Section, const string &Ident, const string &Default)
{
	// calls ReadStringCreate with the default comments
	return ReadStringCreate(Section, Ident, Default, COMMENT_CHARS_DEFAULT);
}

string CIniFile::ReadStringCreate(const string &Section, const string &Ident, const string &Default,
	const string &CommentChars)

	// Section identifies the section in the file that contains the desired key.
	// Ident is the name of the key from which to retrieve the value.
	// Default is the string value to set in file
	//
	// This is like ReadString but if the key does not exist, creates it.
	// Used when one wants to create readonly keys.
	//
	// ***********************************************************************
{
	string Result;
	if (!KeyExists(Section, Ident))
	{
		Result = Default;
		WriteString(Section, Ident, Default);
	}
	else
	{ // Read the name
		Result = ReadString(Section, Ident, Default, CommentChars);
	}

	return (Result);
}
// ----------------ReadFileName----------------John Mertus---Sep 2001------

string CIniFile::ReadFileName(const string &Section, const string &Ident, const string &Default)

	// This reads a string and then translates the string into a file name
	// The Translation is two fold,
	// All $(var) substrings are replaced by the environment variable var
	// The proper directory seperators for the file system are subsituted
	// via the ConformFileName function.
	//
	// ***********************************************************************
{
	string Result = ReadString(Section, Ident, Default);
	ExpandEnviornmentString(Result);
	ConformFileName(Result);
	return (Result);
}
// ----------------ReadFileNameCreate-----------John Mertus---Aug 2001------

string CIniFile::ReadFileNameCreate(const string &Section, const string &Ident,
	const string &Default)

	// Section identifies the section in the file that contains the desired key.
	// Ident is the name of the key from which to retrieve the value.
	// Default is the string value to set in file
	//
	// This is like ReadFileName but if the key does not exist, creates it.
	// Used when one wants to create readonly keys.
	//
	// ***********************************************************************
{
	string Result;
	if (!KeyExists(Section, Ident))
	{
		WriteString(Section, Ident, Default);
	}

	// Must reread the file name to do any translations
	Result = ReadFileName(Section, Ident, Default);

	return (Result);
}

// ----------------ReadSection---------------John Mertus---Aug 2000------

void CIniFile::ReadSection(const string &Section, StringList *Strings)

	// This reads all the key names of section Section into Strings
	//
	// ***********************************************************************
{
	Strings->clear();
	IniSection *is = Sections->GetIniSection(Section);
	if (is == NULL)
		return; // Nothing to do
	for (unsigned int i = 1; i < is->size(); i++)
	{
		string key = is->Key(i);
		if (key != "")
			Strings->push_back(key);
	}
}

// ----------------ReadSectionValues----------John Mertus---Aug 2000------

void CIniFile::ReadSectionValues(const string &Section, StringList *Strings)

	// This reads all the value names of section Section into Strings
	//
	// ***********************************************************************
{
	Strings->clear();
	IniSection *is = Sections->GetIniSection(Section);
	if (is == NULL)
		return; // Nothing to do
	for (unsigned int i = 1; i < is->size(); i++)
	{
		string key = is->Key(i);
		if (key != "")
			Strings->push_back(is->Value(i));
	}
}

// ----------------ReadStringList-------------John Mertus---Aug 2000------

void CIniFile::ReadStringList(const string &Section, const string &Ident, StringList *Strings,
	StringList *Default)

	// This reads a list of strings from a section Section.  A list of strings
	// are defined as Ident_n, when n starts at 0 and works up.
	//
	// ***********************************************************************
{
	if (Strings == NULL)
		return;

	unsigned int oldsize = Strings->size();
	IniSection *is = Sections->GetIniSection(Section);
	if (is != NULL)
	{
		// There cannot be more than is->size() values to the list.
		for (unsigned int i = 0; i < is->size(); i++)
		{
			MTIostringstream os;
			os << Ident << "[" << i << "]";
			string v = is->Value(os.str());
			if (v != "")
				Strings->push_back(v);
		}
	}
	// If nothing read, copy over the default
	if (Default == NULL)
		return;
	if (Strings->size() == oldsize)
		for (unsigned int i = 0; i < Default->size(); i++)
			Strings->push_back((*Default)[i]);
}

// ------------ReadStringListCreate-----------John Mertus---Aug 2001------

void CIniFile::ReadStringListCreate(const string &Section, const string &Ident, StringList *Strings,
	StringList *Default)

	// Section identifies the section in the file that contains the desired list of keys.
	// Ident is the name of the key from which to retrieve the values.
	// Default is the string list to set in file
	//
	// This is like ReadStringList but if the key does not exist, creates it.
	// Used when one wants to create readonly keys.
	//
	// ***********************************************************************
{
	MTIostringstream os;
	os << Ident << "[0]";
	if (!KeyExists(Section, os.str()))
	{
		WriteStringList(Section, Ident, Default);
	}

	ReadStringList(Section, Ident, Strings, Default);

	return;
}

// ----------------WriteStringList-------------John Mertus---Aug 2000------

void CIniFile::WriteStringList(const string &Section, const string &Ident, StringList *Strings)

	// This writes a list of strings from a section Section.  A list of strings
	// are defined as Ident_n, when n starts at 0 and works up.
	//
	// ***********************************************************************
{
	if (Strings == NULL)
		return;

	IniSection *is = Sections->GetIniSection(Section);
	if (is != NULL)
	{

		// There cannot be more than is->size() values to the list
		// Blow all these away
		unsigned int n = is->size();
		for (unsigned int i = 0; i < n; i++)
		{
			MTIostringstream os;
			os << Ident << "[" << i << "]";
			is->Delete(is->Index(os.str()));
		}
	}

	// Now write them back out
	for (unsigned int i = 0; i < Strings->size(); i++)
	{
		MTIostringstream os;
		os << Ident << "[" << i << "]";
		WriteString(Section, os.str(), (*Strings)[i]);
	}

	bModified = true;
}

// ----------------ReadIntegerList-------------John Mertus---Nov 2002------

IntegerList CIniFile::ReadIntegerList(const string &Section, const string &Ident)

	// This reads a list of integer from a section Section.  A list of strings
	// are defined as Ident[n], when n starts at 0 and works up.
	//
	// ***********************************************************************
{
	StringList sl;
	IntegerList il;
	ReadStringList(Section, Ident, &sl);
	for (unsigned i = 0; i < sl.size(); i++)
	{
		int Result;
		string s = sl[i];
		MTIistringstream istr(s);
		if ((s[0] == '0') && (toupper(s[1]) == 'X'))
		{
			char *p;
			Result = strtol(s.c_str(), &p, 16);
			if (p == s.c_str())
				Result = 0;
		}
		else
      {
         istr >> Result;
         if (istr.fail())
				Result = 0;
      }

		il.push_back(Result);
	}

	return il;
}

// ----------------WriteIntegerList---------------John Mertus---Nov 2002------

void CIniFile::WriteIntegerList(const string &Section, const string &Ident, const IntegerList &il)

	// This writes a list of integer from a section Section.  A list of strings
	// are defined as Ident[n], when n starts at 0 and works up.
	//
	// ****************************************************************************
{
	StringList sl;
	for (unsigned i = 0; i < il.size(); i++)
	{
		MTIostringstream os;
		os << il[i];
		sl.push_back(os.str());
	}

	WriteStringList(Section, Ident, &sl);
}

// ----------------ReadBoolList-------------John Mertus---Nov 2002------

BoolList CIniFile::ReadBoolList(const string &Section, const string &Ident)

	// This reads a list of bools from a section Section.  A list of strings
	// are defined as Ident[n], when n starts at 0 and works up.
	//
	// ***********************************************************************
{
	StringList sl;
	BoolList il;
	ReadStringList(Section, Ident, &sl);
	for (unsigned i = 0; i < sl.size(); i++)
		il.push_back(CheckForTruth(sl[i]));

	return il;
}

// ----------------WriteBoolList---------------John Mertus---Nov 2002------

void CIniFile::WriteBoolList(const string &Section, const string &Ident, const BoolList &il)

	// This writes a list of bools from a section Section.  A list of strings
	// are defined as Ident[n], when n starts at 0 and works up.
	//
	// ****************************************************************************
{
	StringList sl;
	for (unsigned i = 0; i < il.size(); i++)
	{
		MTIostringstream os;
		os << il[i];
		sl.push_back(os.str());
	}

	WriteStringList(Section, Ident, &sl);
}

// ----------------fReadFile------------------John Mertus---Aug 2000------

bool CIniFile::fReadFile(void)

	// This reads the ini file and creates the Sections->
	// All the hard work is done here.
	//
	// ***********************************************************************
{
	FILE *stream; // File Handle
	IniSection *Section; // Section to build

	// Try to open the file
	stream = fopen(FileName.c_str(), "rt");
	if (stream == NULL)
	{
		// could not open the file.  Print a message
		TRACE_3(errout << "INFO: Could not open file: " << FileName << endl << "      Because: " <<
			strerror(errno));
		return (true);
	}
	//
	// Succeded, go process it
	Section = new IniSection;
	Sections->push_back(Section); // A no-name section always exists

	//
	// Note: the following is not a mistake, ini files can contain lines at
	// the beginning that are not under a header file
	// TBD: Hard limit of 1024 characters, should really make it soft
	char buffer[1024];
	while (true)
	{
		char rc;
		char *r = &rc;
		r = fgets(buffer, sizeof(buffer), stream);
		// Now check for an non end of file error
		if (feof(stream) && (r == NULL))
			break; // Done
		if (r == NULL)
		{
			TRACE_0(errout << "ERROR: Can't read ini File " << FileName;
			errout << endl << "  Because: " << strerror(errno);
			theError.set(INIFILE_ERROR_READ_FAILED, errout.str()););
			fclose(stream);
			return (false);
		}

		if (buffer[strlen(buffer) - 1] == '\n')
			buffer[strlen(buffer) - 1] = 0; // Zap newline character

		if (IsHeader(buffer))
		{
			// Create the section name
			char *p = (char *)trim(buffer);
			char Result[sizeof(buffer)];
			for (unsigned int i = 1; i < strlen(p); i++)
			{
				Result[i - 1] = 0;
				if (p[i] == ']')
					break;
				Result[i - 1] = p[i];
			}

			// Check to see if the section already exists, if so we
			// append it back on
			Section = Sections->GetIniSection(Result);
			if (Section == NULL)
			{
				// No section, just create it
				Section = new IniSection;
				Section->Name = Result;
				Section->push_back(buffer); // Add header to buffer
				Sections->push_back(Section); // Save old section
			}
			else
			{
				bModified = true; // File has changed
			}
		}
		else
		{
			// Check to see if a comment line
			if (CommentLine(buffer))
				Section->push_back(buffer);
			// Not a Header, check if key exists
			else
				if (!Section->KeyExists(GetKey(buffer)))
					Section->push_back(buffer); // No Add line to buffer
				else
					bModified = true; // File has changed
		}
	}

	// Success, go home
	fclose(stream);
	return true;
}

// ----------------fReadStream------------------John Starr ---Aug 2005------

bool CIniFile::fReadStream(istream &inStream)

	// This reads a STL input stream containing the equivalent of an
	// ini file and creates the Sections->
	// All the hard work is done here.
	//
	// ***********************************************************************
{
	IniSection *Section; // Section to build

	//
	// Succeded, go process it
	Section = new IniSection;
	Sections->push_back(Section); // A no-name section always exists

	//
	// Note: the following is not a mistake, ini files can contain lines at
	// the beginning that are not under a header file
	// TBD: Hard limit of 1024 characters, should really make it soft
	char buffer[1024];
	while (true)
	{
		// Read up to sizeof(buffer)-1 character or to end-of-line character
		// inStream.get(buffer, sizeof(buffer));
		inStream.getline(buffer, sizeof(buffer));
		if (inStream.eof())
			break; // Done
		// Now check for an non end-of-file error
		if (inStream.fail())
		{
			TRACE_0(errout << "ERROR: Can't read input stream " << endl;
			theError.set(INIFILE_ERROR_READ_FAILED, errout.str()););
			return (false);
		}

		if (buffer[strlen(buffer) - 1] == '\n')
			buffer[strlen(buffer) - 1] = 0; // Zap newline character

		if (IsHeader(buffer))
		{
			// Create the section name
			char *p = (char *)trim(buffer);
			char Result[sizeof(buffer)];
			for (unsigned int i = 1; i < strlen(p); i++)
			{
				Result[i - 1] = 0;
				if (p[i] == ']')
					break;
				Result[i - 1] = p[i];
			}

			// Check to see if the section already exists, if so we
			// append it back on
			Section = Sections->GetIniSection(Result);
			if (Section == NULL)
			{
				// No section, just create it
				Section = new IniSection;
				Section->Name = Result;
				Section->push_back(buffer); // Add header to buffer
				Sections->push_back(Section); // Save old section
			}
			else
			{
				bModified = true; // File has changed
			}
		}
		else
		{
			// Check to see if a comment line
			if (CommentLine(buffer))
				Section->push_back(buffer);
			// Not a Header, check if key exists
			else
				if (!Section->KeyExists(GetKey(buffer)))
					Section->push_back(buffer); // No Add line to buffer
				else
					bModified = true; // File has changed
		}
	}

	// Success, go home
	return (true);

} // fReadStream

string MTITempFileName(const string &Template)

	//
	// Call this function with the COMPLETE path and file name in template.
	// Return will the template + GUID if template+GUID is a possible file name
	// "" if the new file name would be too long
	//
	// ***************************************************************************
{
	// Create a UID
	string Result = Template + "XXXXXX";
	char *p = mktemp((char *)Result.c_str());
	if (p == NULL)
		return ""; // Error creating the name
	Result = p;

	// Check for file too big
#ifdef _WINDOWS
	if (Result.size() >= (MAX_PATH-1))
		return "";
#endif

#ifdef __unix
	if (Result.size() >= pathconf(Template.c_str(), _PC_PATH_MAX))
		return "";
#endif
	return Result;
}

// This simple routine renames a file back and reports any errors
//
bool InternalIniRename(const string &fromName, const string &toName)
{
	if (fromName == "")
		return true;

	if (rename(fromName.c_str(), toName.c_str()) == 0)
		return true;
	TRACE_0(errout << " ERROR: Cannot rename temp ini file to ini " << endl <<
		"        because: " << strerror(errno)
		<< endl << "        ini " << toName << " is now called " << fromName;
	theError.set(INIFILE_ERROR_CREATE_FAILED, errout.str()));

	return false;
}

// ----------------fWriteFile-----------------John Mertus---Aug 2000------

bool CIniFile::fWriteFile(void)

	// This writes the ini file from the sections
	//
	// ***********************************************************************
{
	string strNewFileName;
	FILE *stream; // File Handle

	CLogFile IniLogFile;
	IniLogFile.SetFileName(FileName);
	IniLogFile.SetKeep(globalIniData->iGlobalIniKeep);

	// Take two paths, if no file exists, just try to write file
	if (DoesFileExist(FileName))
	{
		// If file exists, try to rename it
		if (!DoesWritableFileExist(FileName))
		{
			// Failure because file exists but is not writable
			TRACE_0(errout << " ERROR: " << FileName <<
				" is not writable, could be open or readonly");
			return false;
		}
		else
		{
			strNewFileName = IniLogFile.NextFileName();
			if (strNewFileName == "")
			{
				TRACE_0(errout << " ERROR: Cannot create a unique file name from " << FileName);
				return false;
			}
		}

		// Now rename inifile
		if (rename(FileName.c_str(), strNewFileName.c_str()) != 0)
		{
			TRACE_0(errout << " ERROR: Cannot rename inifile to temp name " << endl <<
				"        ini file: " << FileName << ", New name: " << strNewFileName << endl <<
				"        because: " << endl << strerror(errno);

			theError.set(INIFILE_ERROR_CREATE_FAILED, errout.str());

			);
			return false;
		}

	}

	// Try to open the file
	stream = fopen(FileName.c_str(), "wt");
	if (stream == NULL)
	{
		theError.set(INIFILE_ERROR_CREATE_FAILED, (string)"Could not open " + FileName +
			"\nBecause: " + strerror(errno));

		// Now try to rename the file back
		// An error will report the error in rename
		InternalIniRename(strNewFileName, FileName);
		return false;
	}

	bool Result = Sections->WriteFile(stream);
	if (!Result)
	{
		theError.setMessage((string)"Write failed on " + FileName + "\nBecause: " +
			theError.getMessage());
		InternalIniRename(strNewFileName, FileName);
	}

	if (fclose(stream) < 0)
	{
		theError.setMessage((string)"Write failed on " + FileName + "\nBecause: " +
			theError.getMessage());
		InternalIniRename(strNewFileName, FileName);
		Result = false;
	}

	// If everything went OK and keep is 0, then delete it
	// if (strNewFileName != "")
	// if (DoesFileExist(strNewFileName))
	// if (remove(strNewFileName.c_str()) < 0)
	// TRACE_0(errout << "Inifile OK, but backup could not be deleted" << endl
	// << "Because: " << strerror(errno));

	bModified = false;
	return Result;
}

// ----------------WriteStream-----------------John Starr---Aug 2005------

bool CIniFile::WriteStream(ostream &stream)

	// This writes the ini file from the sections
	//
	// ***********************************************************************
{
	bool Result = Sections->WriteStream(stream);

	if (!Result)
		theError.setMessage((string)"Write to stream failed");

	bModified = Result;
	return (Result);
}

// ----------------ReadBool-------------------John Mertus---Aug 2000------

bool CIniFile::ReadBool(const string &Section, const string &Ident, bool Default)

	// Section identifies the section in the file that contains the desired key.
	// Ident is the name of the key from which to retrieve the value.
	// Default is the bool value to return if the:
	// Section does not exist.
	// Key does not exist.
	// Data value for the key is not assigned.
	//
	// ***********************************************************************
{
	string s = ReadString(Section, Ident, "");
	if (s == "")
		return (Default);
	return CheckForTruth(s);
}

// ----------------WriteBool------------------John Mertus---Aug 2000------

void CIniFile::WriteBool(const string &Section, const string &Ident, bool Value)

	// This creates a line of the form Iden=Value in the section Section
	// If the section, ident does not exist, it creates it
	// Otherwise it replaces the value
	//
	// ***********************************************************************
{
	string s;
	if (Value)
		s = "true";
	else
		s = "false";
	WriteString(Section, Ident, s);
}

// ----------------ReadBoolCreate--------------John Mertus---Aug 2001------

bool CIniFile::ReadBoolCreate(const string &Section, const string &Ident, bool Default)

	// Section identifies the section in the file that contains the desired key.
	// Ident is the name of the key from which to retrieve the value.
	// Default is the boolean value to set in file
	//
	// This is like ReadBool but if the key does not exist, creates it.
	// Used when one wants to create readonly keys.
	//
	// ***********************************************************************
{
	bool Result;
	if (!KeyExists(Section, Ident))
	{
		Result = Default;
		WriteBool(Section, Ident, Default);
	}
	else
	{ // Read the name
		Result = ReadBool(Section, Ident, Default);
	}

	return (Result);
}

// ----------------ReadDouble------------------John Mertus---Aug 2000------

double CIniFile::ReadDouble(const string &Section, const string &Ident, double Default)

	// Section identifies the section in the file that contains the desired key.
	// Ident is the name of the key from which to retrieve the value.
	// Default is the bool value to return if the:
	// Section does not exist.
	// Key does not exist.
	// Data value for the key is not assigned.
	//
	// ***********************************************************************
{
	string s = ReadString(Section, Ident, "");
	if (s == "")
		return (Default);

	// Convert the stream
	MTIistringstream istr(s);
	double Result;
	istr >> Result;
	if (istr.fail())
		Result = Default;
	return (Result);
}

// ----------------WriteDouble------------------John Mertus---Aug 2000------

void CIniFile::WriteDouble(const string &Section, const string &Ident, double Value)

	// This creates a line of the form Iden=Value in the section Section
	// If the section, ident does not exist, it creates it
	// Otherwise it replaces the value
	//
	// ***********************************************************************
{
	MTIostringstream ostr;
	ostr << Value;
	WriteString(Section, Ident, ostr.str());
}

// ----------------ReadDoubleCreate-----------John Mertus---Aug 2001------

double CIniFile::ReadDoubleCreate(const string &Section, const string &Ident, double Default)

	// Section identifies the section in the file that contains the desired key.
	// Ident is the name of the key from which to retrieve the value.
	// Default is the double value to set in file
	//
	// This is like ReadDouble but if the key does not exist, creates it.
	// Used when one wants to create readonly keys.
	//
	// ***********************************************************************
{
	double Result;
	if (!KeyExists(Section, Ident))
	{
		Result = Default;
		WriteDouble(Section, Ident, Default);
	}
	else
	{ // Read the name
		Result = ReadDouble(Section, Ident, Default);
	}

	return (Result);
}

// ----------------ReadInteger------------------John Mertus---Aug 2000------

MTI_INT64 CIniFile::ReadInteger(const string &Section, const string &Ident, MTI_INT64 Default)

	// Section identifies the section in the file that contains the desired key.
	// Ident is the name of the key from which to retrieve the value.
	// Default is the 64 bit integer value to return if the:
	// Section does not exist.
	// Key does not exist.
	// Data value for the key is not assigned.
	//
	// ***********************************************************************
{
	string s = StringTrimLeft(ReadString(Section, Ident, ""));
	if (s == "")
		return (Default);

	// Convert the stream
	// #ifndef _MSC_VER // Just for compilation... should be changed
	// MTI_INT64 Result;
	// #else
	// MTI_INT32 Result;
	// #endif
	// // Check Hex result
	// if (s.size() > 2)
	// {
	// // Borland's hex seems to be broken, use this one
	// if ((s[0] == '0') && (toupper(s[1]) == 'X'))
	// {
	// size_t p;
	// Result = stol(s, &p, 16);
	// if (p != s.length()) Result = Default;
	// return Result;
	// }
	// }

	// Parse as a decimal
	try
	{
		return std::atol(s.c_str());
	}
	catch (...)
	{
		return Default;
	}
}

// ----------------WriteInteger---------------John Mertus---Aug 2000------

#ifndef _MSC_VER // Visual C complains about 64 bit ints in this method:
// error C2593: 'operator <<' is ambiguous.  This should be fixed.
void CIniFile::WriteInteger(const string & Section, const string & Ident, MTI_INT64 Value)
#else

void CIniFile::WriteInteger(const string &Section, const string &Ident, MTI_INT32 Value)
#endif

	// This creates a line of the form Iden=Value in the section Section
	// If the section, ident does not exist, it creates it
	// Otherwise it replaces the value
	//
	// ***********************************************************************
{
	MTIostringstream ostr;
	ostr << Value;
	WriteString(Section, Ident, ostr.str());
}

// ----------------ReadIntegerCreate-----------John Mertus---Aug 2001------

MTI_INT64 CIniFile::ReadIntegerCreate(const string &Section, const string &Ident, MTI_INT64 Default)

	// Section identifies the section in the file that contains the desired key.
	// Ident is the name of the key from which to retrieve the value.
	// Default is the 64 bit integer value to set in file
	//
	// This is like ReadInteger but if the key does not exist, creates it.
	// Used when one wants to create readonly keys.
	//
	// ***********************************************************************
{
	MTI_INT64 Result;
	if (!KeyExists(Section, Ident))
	{
		Result = Default;
		WriteInteger(Section, Ident, Default);
	}
	else
	{ // Read the name
		Result = ReadInteger(Section, Ident, Default);
	}

	return (Result);
}

// ----------------ReadUnsignedInteger----------John Mertus---Aug 2000------

MTI_UINT64 CIniFile::ReadUnsignedInteger(const string &Section, const string &Ident,
	MTI_UINT64 Default)

	// Section identifies the section in the file that contains the desired key.
	// Ident is the name of the key from which to retrieve the value.
	// Default is the 64 bit integer value to return if the:
	// Section does not exist.
	// Key does not exist.
	// Data value for the key is not assigned.
	//
	// ***********************************************************************
{
	string s = ReadString(Section, Ident, "");
	if (s == "")
		return (Default);

	// Convert the stream
	MTIistringstream istr(s);
#ifndef _MSC_VER // Just for compilation... should be changed
	MTI_UINT64 Result;
#else
	MTI_UINT32 Result;
#endif
	istr >> Result;
	if (istr.fail())
		Result = Default;
	return (Result);
}

// ----------------WriteUnsignedInteger-------John Mertus---Aug 2000------

#ifndef _MSC_VER // Visual C complains about 64 bit ints in this method:
// error C2593: 'operator <<' is ambiguous.  This should be fixed.
void CIniFile::WriteUnsignedInteger(const string & Section, const string & Ident, MTI_UINT64 Value)
#else

void CIniFile::WriteUnsignedInteger(const string &Section, const string &Ident, MTI_UINT32 Value)
#endif

	// This creates a line of the form Iden=Value in the section Section
	// If the section, ident does not exist, it creates it
	// Otherwise it replaces the value
	//
	// ***********************************************************************
{
	MTIostringstream ostr;
	ostr << Value;
	WriteString(Section, Ident, ostr.str());
}

// ----------------ReadUnsignedIntegerCreate---John Mertus---Aug 2001------

MTI_UINT64 CIniFile::ReadUnsignedIntegerCreate(const string &Section, const string &Ident,
	MTI_UINT64 Default)

	// Section identifies the section in the file that contains the desired key.
	// Ident is the name of the key from which to retrieve the value.
	// Default is the 64 bit integer value to set in file
	//
	// This is like ReadInteger but if the key does not exist, creates it.
	// Used when one wants to create readonly keys.
	//
	// ***********************************************************************
{
	MTI_UINT64 Result;
	if (!KeyExists(Section, Ident))
	{
		Result = Default;
		WriteUnsignedInteger(Section, Ident, Default);
	}
	else
	{ // Read the name
		Result = ReadUnsignedInteger(Section, Ident, Default);
	}

	return (Result);
}

// ***************************IniSection**********************************

// ----------------Key------------------------John Mertus---Aug 2000------

string IniSection::Key(unsigned int idx)

	// Returns the key of index idx.  If key does not exist returns ""
	//
	// ***********************************************************************
{
	if (idx <= 0)
		return (""); // First line is header
	if (idx >= size())
		return ("");
	return (GetKey((*this)[idx]));
}

// ------------KeyExists----------------------John Mertus---Aug 2000------

bool IniSection::KeyExists(const string &Ident)

	// Returns true if Ident occures as a key in the section
	// Section is case insensitive
	//
	// ***********************************************************************
{
	return (Index(Ident) >= 0);
}

// ----------------Delete--------------------John Mertus---Aug 2000------

void IniSection::Delete(unsigned int idx)

	// Remove an element at index idx
	//
	// ***********************************************************************
{
	if (idx >= size())
		return;
	for (iterator r = begin(); r != end(); r++)
		if ((*this)[idx] == *r)
		{
			erase(r);
			return;
		}
}

// ----------------Value----------------------John Mertus---Aug 2000------

string IniSection::Value(const string &Ident)

	// Returns the Value of a key associated with string k
	// if Key does not exist, a blank value is returned
	// Note: Value includes any comments
	//
	// ***********************************************************************
{
	int idx = Index(Ident);
	if (idx < 0)
		return ("");
	return (Value(idx));
}

// ----------------Value----------------------John Mertus---Aug 2000------

string IniSection::Value(unsigned idx)

	// Returns the Value of a key associated with index idx
	// if there is no key, return is NULL
	// Note: Value includes any comments
	//
	// ***********************************************************************
{
	string sk = Key(idx);
	if (sk == "")
		return (""); // No key at that index
	sk = (*this)[idx]; // Get Line
	int ns = sk.find("="); // Always will find a =
	return (sk.substr(ns + 1, sk.size()));
}

// ----------------Index----------------------John Mertus---Aug 2000------

int IniSection::Index(const string &Ident)

	// Return the index of key Ident
	// If Ident does not exist, a -1 is returned
	//
	// ***********************************************************************
{
	if (Ident.size() == 0)
		return (-1);
	for (unsigned i = 0; i < size(); i++)
	{
		string sk = Key(i);
		if (EqualIgnoreCase(sk, Ident))
			return (i);
	}
	return (-1); // Just a line with = in it.
}

// ----------------WriteFile-----------------John Mertus---Aug 2000------

bool IniSection::WriteFile(FILE *stream)

	// This writes the ini file from the section
	//
	// ***********************************************************************
{
	// Special case, lines at top
	if ((Name == "") && (size() == 0))
		return (true);
	for (unsigned i = 0; i < size(); i++)
	{
		// Do not write blank lines
		/* TTT: vector::at() doesn't compile on LINUX */
		if (StringTrim((*this)[i]) != "")
			if (fprintf(stream, "%s\n", (*this)[i].c_str()) < 0)
			{
				theError.set(INIFILE_ERROR_WRITE_FAILED, strerror(errno));
			}
	}

	// Put a blank line
	fprintf(stream, "\n");

	return (true);
}

// ----------------WriteStream-----------------John Starr---Aug 2005------

bool IniSection::WriteStream(ostream &stream)

	// This writes the ini file from the section
	//
	// ***********************************************************************
{
	// Special case, lines at top
	if ((Name == "") && (size() == 0))
		return (true);
	for (unsigned i = 0; i < size(); i++)
	{
		// Do not write blank lines
		/* TTT: vector::at() doesn't compile on LINUX */
		if (StringTrim((*this)[i]) != "")
		{
			stream << (*this)[i] << endl;
		}
	}

	// Put a blank line
	stream << endl;

	return (true);

} // WriteStream

// ***************************IniSectionList******************************

// ----------------Constructor--------------John Mertus---Aug 2000------

IniSectionList::IniSectionList(void)

	// Usual constructor
	//
	// ***********************************************************************
{
}

// ----------------Destructor----------------John Mertus---Aug 2000------

IniSectionList::~IniSectionList()

	// Destroy the list and all the elements in it
	// The vector will clean up the rest
	//
	// ***********************************************************************
{
	for (unsigned int i = 0; i < size(); i++)
		delete(*this)[i];
}

// ----------------Delete--------------------John Mertus---Aug 2000------

void IniSectionList::Delete(unsigned int i)

	// Remove an element from the list
	//
	// ***********************************************************************
{
	if (i >= size())
		return;
	for (iterator r = begin(); r != end(); r++)
		if ((*this)[i] == *r)
		{
			delete *r;
			erase(r);
			return;
		}
}

// ----------------IniSectionIndex------------John Mertus---Aug 2000------

int IniSectionList::IniSectionIndex(const string &Name)

	// Get the index of an section from the section name Name.
	// Name is case insensitive.
	//
	// Return is the index or -1 if the name does not occure.
	//
	// ***********************************************************************
{
	for (unsigned int i = 0; i < size(); i++)
	{
		IniSection *is = this->at(i); // Get Section
		if (EqualIgnoreCase(Name, is->Name))
			return i;
	}

	return -1;
}

// ----------------GetIniSection--------------John Mertus---Aug 2000------

IniSection *IniSectionList::GetIniSection(const string &Name)

	// Gets a pointer to the IniSection with header Name
	// Name is case insensitive.
	//
	// Return is the pointer or NULL if the name does not occure.
	//
	// ***********************************************************************
{
	int i = IniSectionIndex(Name);
	if (i < 0)
		return (NULL);
	return ((*this)[i]);
}

// ----------------WriteFile-----------------John Mertus---Aug 2000------

bool IniSectionList::WriteFile(FILE *stream)

	// This writes the ini file from the sections
	//
	// ***********************************************************************
{
	for (unsigned i = 0; i < size(); i++)
		if (!(*this)[i]->WriteFile(stream))
			return (false);

	return (true);
}

// ----------------WriteSteam-----------------John Starr---Aug 2005------

bool IniSectionList::WriteStream(ostream &stream)

	// This writes the ini file to a STL output stream from the sections
	//
	// ***********************************************************************
{
	for (unsigned i = 0; i < size(); i++)
		if (!(*this)[i]->WriteStream(stream))
			return (false);

	return (true);
}

// -------------CPMPSetDefaultKey-------------John Mertus---Sep 2001------

void CPMPSetDefaultKey(const string &str)

	// This changes the default key to str
	//
	// ***********************************************************************
{
	globalIniData->DefaultKey = str;
	if (globalIniData->DefaultKey == "")
		globalIniData->DefaultKey = GetFileName(GetApplicationName());
}

// -------------CPMPGetDefaultKey-------------John Mertus---Sep 2001------

string CPMPGetDefaultKey(void)

	// This returns the default key
	//
	// ***********************************************************************
{
	return globalIniData->DefaultKey;
}

// ----------------CPMPInitMasterIniFile-----------John Mertus-----Jul 2001---

void CPMPInitMasterIniFile(void)

	// This specialized routine is called internally to create or load the
	// master ini file creation
	//
	// ***************************************************************************
{
	if (globalIniData->MasterIniFileName.size() == 0)
		CPMPMasterIniFileName();
}

// ----------------CPMPTraceLevel----------------John Mertus-----Jul 2001---

int CPMPTraceLevel(void)

	// This returns the maximum of the global and program trace levels
	//
	// ***************************************************************************
{
	CPMPInitMasterIniFile();
	return (std::max<int>(globalIniData->iProgramTraceLevel, globalIniData->iGlobalTraceLevel));
}

// ----------------CPMPLogLevel----------------John Mertus-----Jul 2001---

int CPMPLogLevel(void)

	// This returns the maximum of the global and program log levels
	//
	// ***************************************************************************
{
	CPMPInitMasterIniFile();
	return (std::max<int>(globalIniData->iProgramLogLevel, globalIniData->iGlobalLogLevel));
}

// ----------------CheckForDirectory----------------John Mertus-----Jul 2001---

bool CheckForDirectory(const string &Name)

	// This is a helper function checks to see the directory specified by the
	// file name exists, if not, it asks the user to create it.
	//
	// Return is true if directory exists at the end of the call
	// false if does not exist.
	//
	// ***************************************************************************
{
	string path = GetFilePath(Name);
	if (!DoesDirectoryExist(path))
	{
		TRACE_1(errout << "Directory " << path << " does not exist ");
		if (!MakeDirectory(path))
		{
			// MessageBox(NULL, theError.getFmtError().c_str() , "Severe Error", MB_ICONSTOP | MB_OK);
			TRACE_0(errout << theError.getFmtError());
			throw theError.getFmtError();
		}
		else
			TRACE_1(errout << "Directory " << path << " created");
	}
	return (true);
}

// -------------CPMPMasterIniFileName--------------John Mertus-----Nov 2001---

string CPMPMasterIniFileName(void)

	// This creates the master inifile if it does not exist
	//
	// ***************************************************************************
{
	if (globalIniData->MasterIniFileName != "")
	{
		return globalIniData->MasterIniFileName; // Its been defined
	}

	// Look up the environment variable CPMP_MASTER_INI
	string ifn = ReturnEnv("CPMP_INI_FILE");
	if (ifn == "")
	{
		// CPMP_INI_FILE not found
		globalIniData->MasterIniFileName = GetFilePath(GetApplicationName()) + "MTIcpmp.ini";
	}
	else
	{
		// CPMP_INI_FILE found
		globalIniData->MasterIniFileName = ifn;
	}

	// Get the default file name
	if (globalIniData->DefaultKey == "")
		globalIniData->DefaultKey = GetFileName(GetApplicationName());

	// Get the trace flag
	CheckForDirectory(globalIniData->MasterIniFileName);
	CIniFile *ini = CreateIniFile(globalIniData->MasterIniFileName);
	if (ini != NULL)
	{
		// Read the trace flag
		// See if there is a key of that name, if not, create it

		globalIniData->iGlobalTraceLevel = ini->ReadIntegerCreate("TraceOptions", "TraceLevel", 0);
		globalIniData->iGlobalLogLevel = ini->ReadIntegerCreate("TraceOptions", "LogLevel", 0);
		//
		// Get the logging file
		// Note: this is ALWAYS the default key for the program
		globalIniData->LogBaseFile = ini->ReadFileNameCreate("TraceOptions", "LogPath",
			"$(CPMP_USER_DIR)") + globalIniData->DefaultKey + ".log";
		globalIniData->LogBaseFile = ExpandRelativePaths(globalIniData->LogBaseFile,
			GetFilePath(globalIniData->MasterIniFileName));
		ProgramLogFile.SetKeep(ini->ReadIntegerCreate("TraceOptions", "Versions", 5));
		globalIniData->iGlobalIniKeep = ini->ReadIntegerCreate("IniOptions", "Versions",
			globalIniData->iGlobalIniKeep);

		// Create the log file
		// This checks that the directory of the file exists
		CPMPSetLogFileName(globalIniData->LogBaseFile);
		CheckForDirectory(globalIniData->LogBaseFile);

		// Try to save the file
		DeleteIniFile(ini);
	}
	else
	{
		MTIostringstream os;
		os << "Could not create Master ini file" << endl << globalIniData->MasterIniFileName;

		TRACE_0(errout << os.str());
		// MessageBox(NULL, os.str().c_str() , "Severe Error", MB_ICONSTOP | MB_OK);
		throw os.str();
	}

	//
	// Trace flag is now valid
//   TRACE_0(errout << "Master ini file: " << globalIniData->MasterIniFileName << endl;
//           errout << "             TraceLevel: " << globalIniData->iGlobalTraceLevel;
//           errout << ", LogLevel: " << globalIniData->iGlobalLogLevel << endl; errout << endl << endl;);
//   TRACE_1(errout << "Program: " << GetApplicationName() << " at "; time_t t; time(&t);
//           errout << ctime(&t); errout << "   Default key: " << globalIniData->DefaultKey;);
//	// The above generates the log file name, don't combine the two lines
//   TRACE_1(
//	if (globalIniData->iGlobalLogLevel >= 0)
//		errout << "   Log File: " << ProgramLogFile.GetActualFileName() << endl;);

	return globalIniData->MasterIniFileName;
}

// --------------CPMPOpenMasterIniFile------------John Mertus-----Nov 2001---

CIniFile *CPMPOpenMasterIniFile(void)

	// This is a helper function which when called returns the proper
	// master ini file.
	//
	// **************************************************************************
{
	return CreateIniFile(CPMPMasterIniFileName());
}

// ----------------CPMPOpenSiteIniFile------------John Mertus-----Nov 2001---

CIniFile *CPMPOpenSiteIniFile(void)

	// This is a helper function which when called returns the proper
	// site ini file is an NSF mounted file for all machines.
	//
	// Use:
	// CIniFile *ini = CPMPOpenSiteIniFile();
	// if (ini == NULL) report error
	// else
	// Read what is needed
	// DeleteIniFile(ini);
	//
	// Note: This function creates a entry in the MasterIniFileName if it does
	// not exits
	//
	// ***************************************************************************
{
	string SiteName;

	// Make sure the master file name is known
	// Note this creates it
	string sMIF = CPMPMasterIniFileName();
	CIniFile *ini = CreateIniFile(sMIF);
	if (ini != NULL)
	{
		SiteName = ini->ReadFileNameCreate(CONFIG_FILE_SECTION, "SiteFile",
			"$(CPMP_SHARED_DIR)MTISite.ini");
		SiteName = ExpandRelativePaths(SiteName, GetFilePath(sMIF));
		TRACE_0(errout << "Site ini file: " << SiteName << endl);

		// Close the Master inifile, report any error
		if (!DeleteIniFile(ini))
			TRACE_0(errout << "Could not write Site configuration file name to Master file" << endl;
		errout << "  Because: " << strerror(errno));

		// Open and return the file
		CheckForDirectory(SiteName);
		return (CreateIniFile(SiteName));
	}
	return (NULL);
}

// -----------CPMPOpenLocalMachineIniFile------------John Mertus-----Nov 2001---

CIniFile *CPMPOpenLocalMachineIniFile(void)

	// This is a helper function which when called returns the proper
	// site ini file is an NSF mounted file for all machines.
	//
	// Use:
	// CIniFile *ini = CPMPOpenSiteIniFile();
	// if (ini == NULL) report error
	// else
	// Read what is needed
	// DeleteIniFile(ini);
	//
	// Note: This function creates a entry in the MasterIniFileName if it does
	// not exist
	//
	// ***************************************************************************
{
	string LocalName;

	// Make sure the master file name is known
	// Note this creates it
	string sMIF = CPMPMasterIniFileName();
	CIniFile *ini = CreateIniFile(sMIF);
	if (ini == NULL)
	{
		TRACE_0(errout << "ERROR: Could not open master ini file";
		theError.set(INIFILE_ERROR_ACCESS_MASTERFILE, errout.str()));
		return (NULL);
	}

	LocalName = ini->ReadFileNameCreate(CONFIG_FILE_SECTION, "LocalFile",
		DEFAULT_LOCAL_MACHINE_FILE);
	LocalName = ExpandRelativePaths(LocalName, GetFilePath(sMIF));

	// Close the Master inifile, report any error
	if (!DeleteIniFile(ini))
	{
		TRACE_0(errout << "ERROR: Could not write local machine ini file name to master ini file" <<
			endl; string sTmp = errout.str(); errout << "  Because: " << theError.getMessage();
		theError.set(INIFILE_ERROR_ACCESS_MASTERFILE, sTmp););
		return (NULL);
	}

	// Open and return the file
	CheckForDirectory(LocalName);
	return (CreateIniFile(LocalName));
}

// ----------------CPMPIniFileName----------------John Mertus-----Jul 2001---

string CPMPIniFileName(const string &str, const string &KeyValue)

	// This is a helper function which when called returns the proper
	// inifile for the system.
	//
	// With an arguement str, this function returns the file associated
	// to that name in the master MTIcpmp.ini file.  Without the arguement
	// the function is equivalent to calling with the main program's name.
	//
	// Str is the key to lookup
	// If the key Str does not exist, and KeyValue is not blank, then
	// str=KeyValue  is added to the master ini file
	//
	// Note: This function creates the default MasterIniFileName on demand
	//
	// ***************************************************************************
{
	string Result;

	// *********On first execution, create the MasterIniFileName***************
	//
	string MIF = CPMPMasterIniFileName();

	// ***********************Find the ini file to use**************************
	// Name defined, find the key section

	string Key;
	if (str != "")
		Key = str;
	else
		Key = globalIniData->DefaultKey;

	TRACE_1(errout << "   Opening key: " << Key << endl);

	// The name by default is $(USERDIR)Key.ini
	// Any environmental variables need to be translated
	// and any relative paths made absolute.
	// e.g.  ./plugins means masterfile path + /plugins

	string DefaultName;
	if (KeyValue != "")
		DefaultName = KeyValue;
	else
		DefaultName = "$(USERDIR)" + Key + ".ini";

	string DefaultTranslatedName = DefaultName;
	ExpandEnviornmentString(DefaultTranslatedName);
	DefaultTranslatedName = ExpandRelativePaths(DefaultTranslatedName,
		GetFilePath(globalIniData->MasterIniFileName));

	// Get the section
//	CIniFile *ini = CreateIniFile(MIF);
//	if (ini == NULL)
//	{
//		TRACE_1(errout << "     No master file " << MIF << endl);
//		TRACE_1(errout << "     Translation: " + DefaultName);
//		return (DefaultTranslatedName); // Default for nothing
//	}
//
//	// See if there is a key of that name, if not, create it
//	if (!ini->KeyExists(CONFIG_FILE_SECTION, Key))
//	{
//		TRACE_1(errout << "     Created Key: " << Key << "=" << DefaultName);
//	}

//	// Note: because of translation, this must be reread.
//	Result = ini->ReadFileNameCreate(CONFIG_FILE_SECTION, Key, DefaultName);
//
//	// If this is a relative name, add the file path from master ini file
//	Result = ExpandRelativePaths(Result, GetFilePath(MIF));
//	TRACE_1(errout << "     Translation: " + Result);
//
//	//
//	// Close the master ini file
//	DeleteIniFile(ini);

   Result = DefaultTranslatedName;
	// *********************Read Program Specific Data**********************
	//
	// See if directory exists
	//
	CheckForDirectory(Result);

	auto ini = CreateIniFile(Result);
	if (ini != NULL)
	{
		auto programTraceLevel = ini->ReadIntegerCreate("TraceOptions", "TraceLevel", -1);
		if (programTraceLevel >= 0) globalIniData->iProgramTraceLevel = programTraceLevel;

		auto programLogLevel = ini->ReadIntegerCreate("TraceOptions", "LogLevel", -1);
		if (programLogLevel >= 0) globalIniData->iProgramLogLevel = programLogLevel;
		// Try to save the file
		DeleteIniFile(ini);
	}

	return (Result);
}

string MTI_CORECODELIB_API CPMPIniFileName(void)
{
	return CPMPIniFileName("", "");
}

string MTI_CORECODELIB_API CPMPIniFileName(const string &str)
{
	return CPMPIniFileName(str, "");
}

// --------------CPMPGetBinRoots-----------------John Mertus-----Nov 2001---

void CPMPGetBinRoots(StringList *sl)

	// This returns a list of bin the bin roots in sl.
	//
	// ***************************************************************************
{
	StringList Default;
	// Default is the the Local Directory plus MTIBins
	Default.push_back(AddDirSeparator((string)"$(CPMP_SHARED_DIR)" + "MTIBins"));
	CIniFile *ini = CPMPOpenLocalMachineIniFile();
	if (ini != NULL)
	{
		ini->ReadStringListCreate("BinDirectories", "Root", sl, &Default);
		// Make the file names into useful file names
		for (unsigned i = 0; i < sl->size(); i++)
		{
			string s = (*sl)[i];
			ExpandEnviornmentString(s);
			ConformFileName(s);
			(*sl)[i] = AddDirSeparator(s);
		}
		if (!DeleteIniFile(ini))
			TRACE_0(errout << "CPMPGetBinRoots Failed");
	}

}

// --------------CPMPGetPathRelativeToBinRoot------ mlm -----Oct 2005---

MTI_CORECODELIB_API string CPMPGetPathRelativeToBinRoot(string const &fullPath)
{
	string relPath;
	string rootPath;
	StringList binList;

	// Make bin path relative to root bin path
	::CPMPGetBinRoots(&binList);
	if (binList.size() < 1)
	{
		TRACE_0(errout << "ERROR: CPMPGetPathRelativeToBinRoot: NO BINS?? ! ?");
		return fullPath;
	}
	if (binList.size() > 1)
	{
		TRACE_0(errout << "ALERT: CPMPGetPathRelativeToBinRoot: " <<
			"DID NOT EXPECT MORE THAN ONE ROOT BIN!  (" << binList.size() << ")");
		// continue, use first one only - too lazy to make this a loop!
	}
	rootPath = binList[0];

	// Add trailing slash if necessary
	if (rootPath[rootPath.length() - 1] != DIR_SEPARATOR)
	{
		rootPath += DIR_SEPARATOR;
	}

	// Snip it off the front of the path, if it's there
	if (fullPath.substr(0, rootPath.length()) == rootPath)
	{
		relPath = fullPath.substr(rootPath.length());
	}
	else
	{
		relPath = fullPath;
	}

	return relPath;
}

// *************************  Associations *********************************

// -------------------Constructor------------------John Mertus----Jan 2003-----

CIniAssociations::CIniAssociations(const SIniAssociation sa[], const string &sPrefix, int iSize)

	// This constructor takes a list of associations Strings <-> Numbers
	// and a optional prefix to strip off and produces the class.
	//
	// If size is specified, it is the size of the associations, else
	// this assumes the association ends in a "" string.
	//
	// ****************************************************************************
{
			_Prefix = sPrefix;
	_Names.clear();
	_Types.clear();
	// Store all the values into the vectors
	int i = 0;

	if (iSize < 0)
		while (sa[i].name[0] != 0)
		{
			_Names.push_back(sa[i].name);
			_Types.push_back(sa[i].type);
			i++;
		}
	else
		for (i = 0; i < iSize; i++)
		{
			_Names.push_back(sa[i].name);
			_Types.push_back(sa[i].type);
		}
}

// -------------------Constructor------------------John Mertus----Jan 2003-----

CIniAssociations::CIniAssociations(const char *sl[], const string &sPrefix, int iSize)

	// This constructor takes a list of  Strings
	// and a optional prefix to strip off and produces the class.
	// The associated number starts at 0 and goes to the number of strings.
	//
	// If size is specified, it is the size of the associations, else
	// this assumes the association ends in a "" string.
	//
	// ****************************************************************************
{
	_Prefix = sPrefix;
	_Names.clear();
	_Types.clear();
	// Store all the values into the vectors
	int i = 0;

	if (iSize < 0)
		while (strlen(sl[i]) != 0)
		{
			_Names.push_back(sl[i]);
			_Types.push_back(i);
			i++;
		}
	else
		for (i = 0; i < iSize; i++)
		{
			_Names.push_back(sl[i]);
			_Types.push_back(i);
		}
}

// ---------------------Prefix---------------------John Mertus----Jan 2003-----

void CIniAssociations::Prefix(const string &sPrefix)

	// Just return the actual prefix, not really needed
	//
	// ****************************************************************************
{
	_Prefix = sPrefix;
}

// --------------SymbolicNameByIndex---------------John Mertus----Jan 2003-----

string CIniAssociations::SymbolicNameByIndex(int Value) const

	// An index is just the number in the list, return it
	//
	// ****************************************************************************
{
	if (Value < 0)
	{
		TRACE_0(errout << "SymbolicNameByIndex called with bad index");
		return "Invalid";
	}

	return _Names[Value];
}

// ------------------SymbolicName------------------John Mertus----Jan 2003-----

string CIniAssociations::SymbolicName(int Value) const

	// Find the map from the symbolic value and find the name associated with it
	//
	// ****************************************************************************
{
	return SymbolicNameByIndex(Index(Value));
}

// ---------------DisplayNameByIndex---------------John Mertus----Jan 2003-----

string CIniAssociations::DisplayNameByIndex(int Value) const

	// Compute the display name from the index, the number in the list
	//
	// ****************************************************************************

{
	if (Value < 0)
		Value = 0; // Invalid indexes always return the first item
	if (Size() <= 0)
		return "";

	// Check to see if prefix match, if so strip it
	//
	string strResult = _Names[Value];
	if (_Prefix.size() > 0)
	{
		if (_Prefix == strResult.substr(0, _Prefix.size()))
			strResult = strResult.substr(_Prefix.size());
	}
	replace(strResult.begin(), strResult.end(), '_', ' ');
	return strResult;
}

// ------------------DisplayName-------------------John Mertus----Jan 2003-----

string CIniAssociations::DisplayName(int Value) const

	// This takes an integer and returns the display name
	// for the association.
	// e.g.  if
	// {"VI_VIDEO_FIELD_RATE_60", VI_VIDEO_FIELD_RATE_60}
	// And Prefix is set to VI_VIDEO_
	//
	// Convert(VI_VIDEO_FIELD_RATE_60) returns FIELD RATE 60
	//
	// If Value does not correspond to a type, then the return
	// is the first value which should be always Invalid or None
	//
	// ***************************************************************************
{
	return DisplayNameByIndex(Index(Value));
}

// ----------------------Size----------------------John Mertus----Jan 2003-----

int CIniAssociations::Size(void) const

	// Return the number of associations
	//
	// ****************************************************************************
{
	return _Names.size();
}

// ---------------------Index----------------------John Mertus----Jan 2003-----

int CIniAssociations::Index(int Value) const

	// Map the symbolic value to the index of the list element
	//
	// Return is -1 if no such value
	//
	// ****************************************************************************
{
	if (Size() <= 0)
		return -1;

	for (unsigned i = 0; i < _Types.size(); i++)
	{
		if (_Types[i] == Value)
			return i;
	}

	return -1;
}

// ---------------ValueByDisplayName----------------John Mertus----Jan 2004-----

bool CIniAssociations::ValueByDisplayName(const string &Name, int &Value) const

	// Map display name into the value of the enum associated with that name
	//
	// Return is -1 if no such value
	//
	// ****************************************************************************
{
	for (int i = 0; i < Size(); i++)
		if (EqualIgnoreCase(Name, DisplayNameByIndex(i)))
		{
			Value = _Types[i];
			return true;
		}

	return false;
}

// ---------------ValueBySymbolicName----------------John Mertus----Jan 2004-----

bool CIniAssociations::ValueBySymbolicName(const string &Name, int &Value) const

	// Map display name into the value of the enum associated with that name
	//
	// Return is -1 if no such value
	//
	// ****************************************************************************
{
	for (int i = 0; i < Size(); i++)
		if (EqualIgnoreCase(Name, SymbolicNameByIndex(i)))
		{
			Value = _Types[i];
			return true;
		}

	return false;
}

// -----------------------CMRU-----------------------John Mertus----Jun 2003---

CMRU::CMRU(void)

	// Normal constructor, just initialize all the variables
	// The CaseSensitive flag depends upon the operating system
	// Windows is set false
	// Linux, Unix is set true
	//
	// ****************************************************************************
{
	_MaxSize = 10;
#ifdef _WINDOWS
	_CaseSensitive = false;
#else
	_CaseSensitive = true;
#endif
}

// ---------------------operator---------------------John Mertus----Jun 2003---

CMRU & CMRU:: operator = (const CMRU & out)

	// Copy the variables and string list
	//
	// ****************************************************************************
{
	clear();
	_CaseSensitive = out.CaseSensitive();
	_MaxSize = out.MaxSize();
	for (int i = (int)out.size() - 1; i >= 0; i--)
		push(out.at(i));

	return *this;
}

// ---------------------operator---------------------John Mertus----Jun 2003---

string CMRU:: operator[](size_type n)

	// Index operator, return string associated with the index
	//
	// ****************************************************************************
{
	return *(begin() + n);
}

// ----------------------erase-----------------------John Mertus----Jun 2003---

int CMRU::erase(const string &str)

	// This removes the string str if it occures in the list.
	// return is -1 if the string str does not contain str
	// otherwise it returns the indes where the string occures.
	//
	// ****************************************************************************
{
	// These two blocks are faster than other syntax
	if (CaseSensitive())
	{
		for (unsigned i = 0; i < size(); i++)
			if (at(i) == str)
			{
				StringList::erase(begin() + i);
				return i;
			}
	}
	else
	{
		for (unsigned i = 0; i < size(); i++)
			if (EqualIgnoreCase(at(i), str))
			{
				StringList::erase(begin() + i);
				return i;
			}
	}

	return -1;
}

// -----------------------push-----------------------John Mertus----Jun 2003---

void CMRU::push(const string &str)

	// This pushes the string str onto the top of the fifo buffer.
	// If str was allready in the array, that occurences is deleted.
	//
	// ****************************************************************************
{
	// See if it occures in the list, if so remove it
	while (erase(str) >= 0);

	// If we have reached the maximum, delete oldest
	while ((int)size() >= MaxSize())
		StringList::erase(end() - 1);
	insert(begin(), str);
}

// -----------------------pop------------------------John Mertus----Jun 2003---

string CMRU::pop(void)

	// Pop the top value off the fifo buffer
	// Return is the top of the string
	// If the list is empty the return is blank
	//
	// ****************************************************************************
{
	if (empty())
		return "";
	string Result = front();
	StringList::erase(begin());
	return Result;
}

// ---------------------MaxSize----------------------John Mertus----Jun 2003---

int CMRU::MaxSize(int n)

	// Sets the maximum size of the list.  If the list exceeds this size,
	// the oldest values are deleted.
	// The minimum size is 1, if set less than 1, MaxSize is set to 1.
	// n is the deisred size
	// return is the actual size,
	//
	// ****************************************************************************
{
	// List must be at least one element long
	if (n < 1)
		n = 1;
	if (n == _MaxSize)
		return MaxSize();
	_MaxSize = n;

	// Now shrink the list if necessary
	while ((int)size() > MaxSize())
		StringList::erase(end() - 1);

	return MaxSize();
}

// ---------------------MaxSize----------------------John Mertus----Jun 2003---

int CMRU::MaxSize(void) const

	// Returns the maximum size, this value will always be 1 or larger.
	//
	// ****************************************************************************
{
	return _MaxSize;
}

// ------------------CaseSensitive-------------------John Mertus----Jun 2003---

bool CMRU::CaseSensitive(bool b)

	// This sets if the strings in the list are considered equal depending upon
	// case.  That is if CaseSensitive, strings are consider equal only if they
	// match exactly.
	// b is the desired value
	// return is current value of the CaseSensitive flag
	// After call to case insensitive, any now duplicate strings are removed
	//
	// ****************************************************************************
{
	if (b == _CaseSensitive)
		return CaseSensitive();
	_CaseSensitive = b;

	// If we were case sensitive and now are not, we
	// must recompute the list
	if (!CaseSensitive())
	{
		CMRU mru;
		mru = *this;
		*this = mru;
	}

	return CaseSensitive();
}

// ------------------CaseSensitive-------------------John Mertus----Jun 2003---

bool CMRU::CaseSensitive(void) const

	// Just return the current value.
	//
	// ****************************************************************************
{
	return _CaseSensitive;
}

// ------------------ReadSettings--------------------John Mertus----Jun 2003---

bool CMRU::ReadSettings(CIniFile *ini, const string &section, const string &name)

	// This reads the mru list to the ini file
	// section -  Section name
	// name  -  what name to use on the list
	//
	// ****************************************************************************
{
	StringList sl;
	ini->ReadStringList(section, name, &sl);
	clear();

	MaxSize(ini->ReadInteger(section, name + ".MaxSize", 10));
	CaseSensitive(ini->ReadBool(section, name + ".CaseSensitive", false));
	for (int i = (int)sl.size() - 1; i >= 0; i--)
		push(sl[i]);

	return true;
}

// -----------------WriteSettings--------------------John Mertus----Jun 2003---

bool CMRU::WriteSettings(CIniFile *ini, const string &section, const string &name)

	// This writes the mru list to the ini file
	// section -  Section name
	// name  -  what name to use on the list
	//
	// ****************************************************************************
{
	ini->WriteInteger(section, name + ".MaxSize", MaxSize());
	ini->WriteBool(section, name + ".CaseSensitive", CaseSensitive());
	ini->WriteStringList(section, name, this);
	return true;
}
