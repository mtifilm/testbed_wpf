/*
   File Name:  SharedMemClass.h
   Created: Augest 01, 2004 by John Mertus

   This class simplifies the process of creating a region of shared memory.
   In Win32, this is accomplished by using the CreateFileMapping and
   MapViewOfFile functions.

*/
#ifndef _SHAREDMEMCLASS_H
#define _SHAREDMEMCLASS_H
#include "machine.h"
#include "corecodelibint.h"

//TSharedMem
class MTI_CORECODELIB_API TSharedMem
{
  public:
    TSharedMem(const string &Name, int Size);
    virtual ~TSharedMem(void);
    const string &Name(void);
    int Size(void);
    void *Buffer(void);
    bool Created(void);

  private:
    string _Name;               //  Section name
    int _Size;                  //  Section size
    bool _Created;              //  True if created
    void *_Buffer;              //  Pointer to file
    void *_Handle;              //  Handle to file

};
#endif
