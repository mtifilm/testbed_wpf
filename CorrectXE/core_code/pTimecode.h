/*
   File Name:  pTimecode.h
   Created: Oct 3, 2000 by John Mertus
   Version 1.00

   This is just a version of CTimecode that adds a notify propery,
   also
     ConformTimeASCII(string s)  This takes any string and converts it into
     a time code under the rules that the hrs, mins, secs, frms are seperated
     by a ":" or <space> or ".".  If not all are specified, the result is right
     justified, and if more are specified, only the first 4 are used.

     Assign(CTimecode)
*/
//---------------------------------------------------------------------------
#ifndef pTimecodeH
#define pTimecodeH
#include "timecode.h"
#include "PropX.h"
#include "corecodelibint.h"
#include "BCDLib.h"
#include "IniFile.h"

class MTI_CORECODELIB_API PTimecode : public CTimecode, public CBHook
{
	public:

   // Constructor, just intialize the properties
      PTimecode(const PTimecode &tc);
      PTimecode(const CTimecode &tc);
      PTimecode(int frms);
      PTimecode(int df=0, int fs=24);
      PTimecode(int hrs, int mins, int secs, int frms, int flgs, int fps);
      PTimecode(const string &s, int flgs, int fps);

      virtual ~PTimecode();

   //
   // String/Assignment methods
   //
		bool ConformTimeASCII(const string &sIn);
		void Assign(const CTimecode &s);
		void BCDToTimecode(bcdTC *bcd);    // Stores BCD as timecode
		void TimecodeToBCD(bcdTC *bcd) const;  // Stores timecode as BCD
		void TimecodeToBCDF(bcdTC *bcd) const; // timecode as BCD FORWARD direction
		virtual void setAbsoluteTime(int Value);

		void Dump(void) const;
		string DumpString(void) const;
   //
   // Override equality
		PTimecode &operator=(const CTimecode &tc) {Assign(tc); return(*this);};
		PTimecode &operator=(const PTimecode &tc) {Assign(tc); return(*this);};
		PTimecode &operator=(int iF) {setAbsoluteFrame(iF); return(*this);};
		void operator=(string s) { if (!ConformTimeASCII(s)) Assign(CTimecode::NOT_SET); }

   // How to convert a PTimecode to a string
		operator string() const {return String(); }
		string String(void) const;
		string HalfFrameString(void) const;

   //
   // Get and set the attribute groupgroup
		int getGroup(void) const;
		void setGroup(int n);

		void setTime(int hrs, int mins, int secs, int frms);

		PROPERTY(int, Frame, getAbsoluteFrame, setAbsoluteFrame, PTimecode);
		int Frames(void) const;
   
      bool areAttributesSame(const CTimecode &tc);

   // Getters, setters
		bool isColorFrame(void) const;
		void setDropFrame(bool Value, bool keep = true);
		void setColorFrame(bool Value);

		void set720P(bool Value, bool keep = true);
		void setTime(const CTimecode &tc);
		void setAttributes(const CTimecode &tc, bool keep = true);
		void setAllAttributes(const PTimecode &tc, bool keep = true);

      void setFramesPerSecond(int fps, bool keep = true);
      void setFpsAndDropFrame(int fps, bool dropFrame, bool keep = true);
      void setField(bool newField);
		int  Index(void);                    // Obsolute same as frame

		void setSign(int Value);
		int  getSign(void) const;

		void setAllowNegative(bool Value);
		bool getAllowNegative(void) const { return m_bAllowNegative; }

   // Hack to peek at user input of VTimecodeEdit
      string getPreConformedString() { return m_preConformedString; }


   // User preferred string style stuff.
      string getUserPreferredStyleString();

      enum StringStyle { NormalStringStyle, ForceFrameNumberStyle, ForceTimecodeStyle };

      StringStyle getDefaultUserPreferredStringStyle();
      void setDefaultUserPreferredStringStyle(StringStyle newStringStyle);
      StringStyle getCurrentUserPreferredStringStyle();
      void setCurrentUserPreferredStringStyle(StringStyle newStringStyle);

      // 24, 25, 30, 50 or 60. Also -30 and -60 indicate drop frame.
      void getDefaultFpsAndDropFrame(int &fps, bool &isDropFrame);
      void setDefaultFpsAndDropFrame(int fps, bool isDropFrame);

      DEFINE_CBHOOK(UserPreferredTimecodeStringStyleChanged, PTimecode);
      CBHook *getTimecodeStringStyleCBHook();
   //

	private:
		void InitTC(void);
		void setAbsoluteFrame(int Value);
		int  getAbsoluteFrame(void) const;
		bool m_bColorFrame;                   // Current color frame
		int  m_Group;                         // Group index
		bool m_bAllowNegative;                // Allow negative timecodes
		int  m_iSign;                         // Sign of the timecode, 1 or -1
        string m_preConformedString;          // Hack to peek at user input
                                              //    for VTimecodeEdit
};
#endif
