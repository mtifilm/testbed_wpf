/*
    File: MTIio.h
    Created Aug 21, 2001 by John Mertus

    The purpose of this is just to make the header files consistent
    for open/close/lseek between UNIX and WINDOW
*/
#ifndef MTIioH
#define MTIioH
#include <vector>
#include "machine.h"
#include "corecodelibint.h"
#include "FileSweeper.h"
#include "MTIstringstream.h"
#include "ThreadLocker.h"
#include <sstream>
#include <stdio.h>

//----------------------Start of windows code-------------------------
#ifdef _WINDOWS
#include <sys\stat.h>
#include <fcntl.h>
#include <io.h>
#define strcasecmp stricmp
#define strncasecmp strnicmp

#ifdef _MSC_VER
//  See also clip/include/Int64.h which solves a similar problem.  We
//  didn't notice that code when we implemented this code.  Someday,
//  we should pick one and stick with it!
extern MTI_CORECODELIB_API std::ostream& operator<<(std::ostream& os, __int64 i );
extern MTI_CORECODELIB_API std::istream& operator>>(std::istream& is, __int64 &i);
extern MTI_CORECODELIB_API std::ostream& operator<<(std::ostream& os, unsigned __int64 i );
extern MTI_CORECODELIB_API std::istream& operator>>(std::istream& is, unsigned __int64 &i);
#endif

#if defined(_MSC_VER) || defined(__BORLANDC__)
extern MTI_CORECODELIB_API double mti_rint(double x);
extern MTI_CORECODELIB_API MTI_INT64 lseek64(int, MTI_INT64, int);
#endif

// Define codes to sysmp
#define MP_NPROCS 1
extern MTI_CORECODELIB_API int sysmp(int Cmd);
//extern MTI_CORECODELIB_API void * memalign(int align, unsigned long size);
extern MTI_CORECODELIB_API int truncate64(const char *path, MTI_UINT64 offset);

//End of Windows definitions

//--------------------Start of SGI definitions-------------------------
#elif defined(__sgi) || defined(__linux)
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>

#ifdef __sgi
#include <sys/sysmp.h>                  // Multiproc definitions
#endif

#ifdef __linux
// Define codes to sysmp
#define MP_NPROCS 1
extern MTI_CORECODELIB_API int sysmp(int Cmd);
#endif

// File 'open' flags defined on PC but not present in Unix
#ifndef O_BINARY
#define O_BINARY 0
#endif
#ifndef O_NOINHERIT
#define O_NOINHERIT 0
#endif
#ifndef O_TEXT
#define O_TEXT 0
#endif

// End of SGI/Linux defintions
#else

#error "No compiler defined"
#endif  // End of Unix definitions
//
//****************Common to both systems********************************
//
extern MTI_CORECODELIB_API bool LessThanIgnoreCase(const string &S1, const string &S2);
extern MTI_CORECODELIB_API bool CharLessThanIgnoreCase(char x, char y);
extern MTI_CORECODELIB_API bool SmartLessThan(const string &S1, const string &S2);
extern MTI_CORECODELIB_API bool EqualIgnoreCase(const string &S1, const string &S2);
extern MTI_CORECODELIB_API bool CharEqualIgnoreCase(char x, char y);
extern MTI_CORECODELIB_API bool ExpandEnviornmentString(string &str);
extern MTI_CORECODELIB_API string ConformEnvironmentName(const string &str);
extern MTI_CORECODELIB_API void ConformFileName(string &str,
		bool bForMySQL=false);
extern MTI_CORECODELIB_API string GetMTIEnv(const string Var);
extern MTI_CORECODELIB_API bool SearchAndReplace(string &Whole, const string ToBeReplaced, const string Replacement);
extern MTI_CORECODELIB_API string GetApplicationName(void);
extern MTI_CORECODELIB_API string ReturnCompleteName(const string &name);
extern MTI_CORECODELIB_API void CreateApplicationName(const string argv0);
extern MTI_CORECODELIB_API bool SendCPMPMessage(const string &str, bool trace, bool log);
extern MTI_CORECODELIB_API bool SendCPMPMessage(const char *msg, bool trace, bool log);
extern MTI_CORECODELIB_API bool CPMPSetLogFileName(const string &name);
extern MTI_CORECODELIB_API string ReturnEnv(const char *Var);
extern MTI_CORECODELIB_API string getArchiveBaseDir(void);
extern MTI_CORECODELIB_API void getArchiveDirAndIniName(string const &binPath,
                                                        string const &clipName,
                                                        string const &guid,
                                                        string &strArchiveBaseDir,
                                                        string &strArchiveDir,
                                                        string &strArchiveIni);
extern MTI_CORECODELIB_API bool SearchAndReplaceAllChar(string &s,
                                                        const char old,
                                                        const char cNew)
;

//
// Edian transfers
//
// Not Used
extern MTI_CORECODELIB_API void SetApplicationPointer(void *ap);

//********************BYTE SWAP functions**************************************

inline void Swap2BytesInPlace(void *datum)
{
   MTI_UINT8 *p = (MTI_UINT8 *)datum;
   MTI_UINT8 tmp = p[0];
   p[0] = p[1];
   p[1] = tmp;
}

inline void Swap4BytesInPlace(void *datum)
{
   MTI_UINT8 *p = (MTI_UINT8 *)datum;
   MTI_UINT8 tmp = p[0];
   p[0] = p[3];
   p[3] = tmp;
   tmp = p[1];
   p[1] = p[2];
   p[2] = tmp;
}

inline void Swap8BytesInPlace(void *datum)
{
   MTI_UINT8 *p = (MTI_UINT8 *)datum;
   for (int i = 0; i < 4; ++i)
   {
      MTI_UINT8 tmp = p[i];
      p[i] = p[7 - i];
      p[7 - i] = tmp;
   }
}

inline void byteSwap(MTI_UINT16 &datum)
{
	Swap2BytesInPlace(&datum);
}

inline void byteSwap(MTI_INT16 &datum)
{
	Swap2BytesInPlace(&datum);
}

inline void byteSwap(MTI_UINT32 &datum)
{
	Swap4BytesInPlace(&datum);
}

inline void byteSwap(MTI_INT32 &datum)
{
	Swap4BytesInPlace(&datum);
}

inline void byteSwap(MTI_UINT64 &datum)
{
	Swap8BytesInPlace(&datum);
}

inline void byteSwap(MTI_INT64 &datum)
{
	Swap8BytesInPlace(&datum);
}

inline void byteSwap(MTI_REAL32 &datum)
{
	Swap4BytesInPlace(&datum);
}

inline void byteSwap(MTI_REAL64 &datum)
{
	Swap8BytesInPlace(&datum);
}

extern MTI_CORECODELIB_API void SwapINT16(MTI_UINT16 *sp, size_t n);
extern MTI_CORECODELIB_API void SwapINT16(MTI_INT16 *sp, size_t n);
extern MTI_CORECODELIB_API void SwapINT32(MTI_INT32 *lp, size_t n);
extern MTI_CORECODELIB_API void SwapINT32(MTI_UINT32 *ulp, size_t n);
extern MTI_CORECODELIB_API void SwapREAL32(MTI_REAL32 *fp, size_t n);
extern MTI_CORECODELIB_API void SwapINT16(MTI_UINT16 *src, MTI_UINT16 *dst, size_t n=1);
extern MTI_CORECODELIB_API void SwapINT16(MTI_INT16 *src, MTI_INT16 *dst, size_t n=1);
extern MTI_CORECODELIB_API void SwapINT32(MTI_UINT32 *src, MTI_UINT32 *dst, size_t n=1);
extern MTI_CORECODELIB_API void SwapINT32(MTI_INT32 *src, MTI_INT32 *dst, size_t n=1);
extern MTI_CORECODELIB_API void SwapREAL32(MTI_REAL32 *src, MTI_REAL32 *dst, size_t n=1);

//************************String serialization******************************
extern MTI_CORECODELIB_API bool StringUnSerialize(string &str, istream &inStream);
extern MTI_CORECODELIB_API bool StringSerialize(const string &str, ostream &osStream);
extern MTI_CORECODELIB_API string WCharToStdString(const wchar_t* str);
////extern MTI_CORECODELIB_API string StringToStdString(const String &str);

//
//******************Debug Trace Macro**************************************
//
//  The more useful macros lie in IniFile.h, these are the base macros
//
extern MTI_CORECODELIB_API bool IsProgramRunning();
extern MTI_CORECODELIB_API bool IsProgramExiting();
extern MTI_CORECODELIB_API void NotifyProgramExiting();
extern MTI_CORECODELIB_API void NotifyProgramRunning(bool flag=true);
#ifdef _WIN64  // Rad Studio bug in 64-bits only (string strams not thread-safe)
#define TRACELOG_MSG(a,t,l)                                    \
   {                                                           \
	  if (IsProgramRunning())                                  \
	  {                                                        \
		 string _error_msg_;                                      \
		 {                                                     \
			CAutoSystemThreadLocker lock(MTIstringstream::GetLock());  \
			std::ostringstream errout;                         \
            errout << __func__ << ";\t";						   \
			a;                                                 \
			_error_msg_ = errout.str();                           \
		 }                                                     \
		 SendCPMPMessage(_error_msg_, t, l);                      \
	  }                                                        \
   }
#else
#define TRACELOG_MSG(a,t,l)                                  \
   {                                                           \
		 string _error_msg_;                                      \
		 {                                                     \
			std::ostringstream errout;                         \
			a;                                                 \
			_error_msg_ = errout.str();                           \
		 }                                                     \
		 SendCPMPMessage(_error_msg_, t, l);                      \
   }
#endif
#define TRACE_MSG(a) TRACELOG_MSG(a,true,true)
#define LOG_MSG(a) TRACELOG_MSG(a,false,true)

#ifdef _DEBUG
#define TRACE_DEBUG(a) TRACELOG_MSG(a, true, true)
#define LOG_DEBUG(a) TRACELOG_MSG(a, false, true)
#else
#define TRACE_DEBUG(a)
#define LOG_DEBUG(a)
#endif


class CBackupSet;

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)  // Too hard to fix, should be OK
#endif


//
//*****************************CLogFile************************************
// Log file class.  Not really needed, a few statics would do
// but this was an effort to use classes, no need to rewrite.
//  Log files have the form <name>.log.n  where n is the log number
//
// Note:  FN means File Number
//
class MTI_CORECODELIB_API CLogFile
{
public:
     CLogFile(CBackupSet *backupSetPtr = NULL);
     ~CLogFile(void);

     bool SetFileName(const string &str);
     void Close(void);
     bool Open();
     virtual bool Write(const string &str);
     string GetActualFileName(void) {return(_sActualFileName);}
     void SetKeep(int n) {_iKeep = n;};
     int GetKeep(void) {return(_iKeep);};
     string NextFileName(void);
     string LastExistingFileName(void);

private:
     FILE *_Stream;           // File Handle
     bool _bFirst;            // Controls the opening
     string _sFileName;       // Log file name
     string _sActualFileName; // Actual file name
     int _iKeep;              // How many copies of log file to keep
     int _iMaxFN;             // Maximum file number
     CBackupSet *_backupSetPtr; // optional backup set

//
// Locate the highest file
//
     DEFINE_FILE_SWEEPER(FindMaxFN, SingleFindMaxFN, false, CLogFile);
     bool SingleFindMaxFN(string In);
     int GetFN(const string &In);

     DEFINE_FILE_SWEEPER(FindExcessFiles, FindExcessFile, false, CLogFile);
     bool FindExcessFile(string In);
     vector<string> _DeleteList;
     void RemoveExcessFiles(void);
};

#ifdef _MSC_VER
#pragma warning(pop)
#endif

extern CLogFile ProgramLogFile;
//
//  More trace options
typedef enum {TD_NONE, TD_TERMINAL, TD_PROGRAM, TD_FILE} trace_destination;
MTI_CORECODELIB_API void ChangeTraceDestination(const trace_destination);
//
// This is so existing code does not break
////#include "MTIDialogs.h"

// Move these date routine to core_code where they rightly belong
#define MTI_LOCAL_TIME false
#define MTI_UTC_TIME true
extern MTI_CORECODELIB_API string MTIBuildDate(void);
extern MTI_CORECODELIB_API MTI_TIME CurrentMTITime(void);
extern MTI_CORECODELIB_API MTI_TIME DateStringToMTITime(const string &sDate, bool UTC = MTI_UTC_TIME);
extern MTI_CORECODELIB_API string MTITimeToDateString(MTI_TIME time, bool UTC = MTI_UTC_TIME);
extern MTI_CORECODELIB_API string MTITimeToDateTimeString(MTI_TIME time, bool UTC = MTI_UTC_TIME);
extern MTI_CORECODELIB_API string MTIBuildMinorVersionFromDate(const string &sDate);

#define StringToStdString(a) WCharToStdString((a).c_str())
#define AnsiStringToStdString(a) string(((a).c_str()))
#endif
