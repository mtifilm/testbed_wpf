//---------------------------------------------------------------------------

#pragma hdrstop

#include "SqliteBlob.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)


   CSqliteBlob::CSqliteBlob(CSqliteDB *sqliteDB) : _sqliteDB(sqliteDB)
   {
   }

   CSqliteBlob::~CSqliteBlob()
   {
      close();
   }

   void CSqliteBlob::open(const string &tableName, const string &columnName, int rowId, bool readOnly)
   {
      auto rc = sqlite3_blob_open(_sqliteDB->getConnection(), "main", tableName.c_str(), columnName.c_str(), rowId, readOnly ? 0 : 1, &_blob);
      if (rc != SQLITE_OK)
      {
         close();
         CSqliteDB::ThrowOnSqlError(rc);
      }
   }

   int CSqliteBlob::getSize() const
   {
      if (isOpen() == false)
      {
         throw std::runtime_error("Blob is not open");
      }

      return sqlite3_blob_bytes(_blob);
   }

   void CSqliteBlob::write(const void *data, int size, int offset)
   {
      CSqliteDB::ThrowOnSqlError(sqlite3_blob_write(_blob, data, size, offset));
   }

   void CSqliteBlob::read(void *data, int size, int offset)
   {
      CSqliteDB::ThrowOnSqlError(sqlite3_blob_read(_blob, data, size, offset));
   }

   void CSqliteBlob::reopen(int newRowId)
   {
      CSqliteDB::ThrowOnSqlError(sqlite3_blob_reopen(_blob, newRowId));
   }
   void CSqliteBlob::close()
   {
         sqlite3_blob_close(_blob);
         _blob = nullptr;
   }

   bool CSqliteBlob::isOpen() const
   {
      return _blob == nullptr;
   }
