/*
 File: MTIioWin.cc
 Created Sept 21, 2001 by John Mertus

 This file defines the MTIio routines that are compiled under
 windows.  Only windows code is contained here.
 */
// *******************************WINDOW CODE*******************************

#include "IniFile.h"  // Just for the trace macros
#include <cmath>

static unsigned CF_MTIText = 0;
static unsigned guMTITextMessage = 0;
//// static HANDLE   _tracerPipe = nullptr;

// ----------------lseek64--------------John Mertus----Aug 2001--------

#if defined(_MSC_VER) || defined(__BORLANDC__)

MTI_INT64 lseek64(int handle, MTI_INT64 offset, int fromwhere)

	 // This emulates the lseek function but for 64 bit integers
	 // handle is the handle from open
	 // offset is the 64 bit offset
	 // fromwhere is the lseek fromwhere; e.g.,
	 // SEEK_CUR	Current file pointer position
	 // SEEK_END	End-of-file
	 // SEEK_SET	File beginning
	 //
	 // ***********************************************************************
{
	// Convert the open handle to the CreateFile handle
	HANDLE osfHandle = (HANDLE)(uintptr_t)(_get_osfhandle(handle));
	DWORD dwMoveMethod;

	switch (fromwhere)
	{
	case SEEK_CUR:
		dwMoveMethod = FILE_CURRENT;
		break;

	case SEEK_END:
		dwMoveMethod = FILE_END;
		break;

	case SEEK_SET:
		dwMoveMethod = FILE_BEGIN;
		break;
	}

	// Set the pointer
	// Note:  The person who designed SetFilePointer is without question the
	// dumbest programmer that ever existed on the face of this earth and most
	// likely on any other inhabited planet in the Milky Way, in this
	// Universe and in any and all other alternate universes. (Or he is Bill Gates)
	// The function overlodes itself, modifies half an arguement, and does not properly
	// set GetLastError() on success.
	// Don't change the code unless you really
	// understand how bad this function really is.
	DWORD Err = SetFilePointer(osfHandle, LOWLONGLONG(offset), ((LONG*)&offset) + 1, dwMoveMethod);
	if (Err == 0xFFFFFFFF)
		if (GetLastError() != NO_ERROR)
			return (-1);

	// Now get the current pointer position
	MTI_INT64 Result = 0;
	*(DWORD*)&Result = SetFilePointer(osfHandle, Result, ((LONG*)&Result) + 1, FILE_CURRENT);
	if (LOWLONGLONG(Result) == 0xFFFFFFFF)
		if (GetLastError() != NO_ERROR)
			return (-1);
	return (Result);
}

#endif

// ----------------sysmp--------------John Mertus--Aug 2001---------

int sysmp(int Cmd)

	 // This is a trivial implimation of the SGI multiproc infor function
	 // CMD takes a constant MP_xxx as defined in sgi man pages
	 // Output is as defined in the sgi man pages
	 // Currently ONLY MP_NPROCS are used
	 //
	 // ***********************************************************************
{
	SYSTEM_INFO si;

	switch (Cmd)
	{
	case MP_NPROCS:
		GetSystemInfo(&si);
		return (si.dwNumberOfProcessors);

		// Note, this MUST never happen
	default:
		assert(false);
		break;
	}

	return (-1);
}

// ----------------truncate64--------------John Mertus--Aug 2001------

int truncate64(const char *path, MTI_UINT64 offset)

	 // This truncates or extends a file to 64 bit offset
	 // If the file is extended, the contents of the file between the old
	 // EOF position and the new position are not defined unlike UNIX
	 // where is it defined as 0;
	 //
	 // if return was success, then 0 was returned, else -1
	 //
	 // ***********************************************************************
{
	// TBD: if offset is less than length the file must be padded with
	// zeros
	HANDLE h = CreateFile(path, GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	if (h == NULL)
		return (-1);

	DWORD Err = SetFilePointer(h, LOWLONGLONG(offset), ((LONG*)&offset) + 1, FILE_BEGIN);
	if (Err == 0xFFFFFFFF)
		if (GetLastError() != NO_ERROR)
		{
			CloseHandle(h);
			return (-1);
		}

	if (SetEndOfFile(h))
	{
		CloseHandle(h);
		return (0);
	}

	// Failure
	CloseHandle(h);
	return (-1);
}

#if 0
// ----------------memalign--------------John Mertus--Aug 2001---------

void *memalign(int align, unsigned long size)

	 // This just does a memory align on size
	 //
	 // ***********************************************************************
{
	int n = ((size + align) / align);
	byte *p = (byte *)malloc(n * align);
	p += (align - ((int)p % align));
	return ((void *)p);
}
#endif // 0

bool writeString(HANDLE pipe, const string &data)
{
	const int MAX_SIZE = 1024;
	char buffer[MAX_SIZE + 3]; // avoid allocation

	DWORD cbWritten = 0;

	auto len = std::min<int>(data.size(), MAX_SIZE);
   auto messageSize = len + 1;
	buffer[0] = messageSize / 256;
	buffer[1] = messageSize % 256;
	memcpy_s(buffer + 2, MAX_SIZE, data.c_str(), len);
   buffer[len + 2] = '\n';
	auto fSuccess = WriteFile(pipe, // pipe handle
		 buffer, // message
		 messageSize + 2, // message length
		 &cbWritten,      // bytes written
		 nullptr);        // not overlapped

	return fSuccess;
}

// NOTE: this is 0 NOT INVALID_HANDLE_VALUE
static HANDLE hPipe = 0;

// --------------SendTraceMessage-----------------John Mertus-----Sep 2001---

bool SendTraceMessage(const string &str)

// This sends a tracer message.
//
// ***************************************************************************
{
	// This will direct all the messages to the terminal
	if (g_TraceDestinationFlag == TD_TERMINAL)
	{
		if (str.size() <= 0)
			return (true);
		if (str[str.size() - 1] == '\n')
			printf("%s: %s", WallClockStamp().c_str(), str.c_str());
		else
			printf("%s: %s\n", WallClockStamp().c_str(), str.c_str());
		fflush(stdout);
		return (true);
	}

	// We get one and only one chance to open pipe
	if (hPipe == 0)
	{
		const char * lpszPipename = "\\\\.\\pipe\\TracerPipe831";
		hPipe = CreateFile(lpszPipename, // pipe name
			 GENERIC_WRITE, 0, // no sharing
			 NULL, // default security attributes
			 OPEN_EXISTING, // opens existing pipe
			 0, // default attributes
			 NULL); // no template file
	}

	if (hPipe == INVALID_HANDLE_VALUE)
	{
		return false;
	}

	if (writeString(hPipe, str))
	{
		return true;
	}

	CloseHandle(hPipe);
	hPipe = INVALID_HANDLE_VALUE;
	return false;
}

//// --------------SendTraceMessage-----------------John Mertus-----Sep 2001---
//
//bool SendTraceMessage(const string &str)
//
//	 // This sends a tracer message.
//	 //
//	 // ***************************************************************************
//{
//	// This will direct all the messages to the terminal
//	if (g_TraceDestinationFlag == TD_TERMINAL)
//	{
//		if (str.size() <= 0)
//			return (true);
//		if (str[str.size() - 1] == '\n')
//			printf("%s: %s", WallClockStamp().c_str(), str.c_str());
//		else
//			printf("%s: %s\n", WallClockStamp().c_str(), str.c_str());
//		fflush(stdout);
//		return (true);
//	}
//
//	// // Null will change to INVALID_HANDLE or a non-zero handle
//	// if (_tracerPipe == nullptr)
//	// {
//	// auto exeName = GetApplicationName();
//	// _tracerPipe = CreateNamedPipe(TEXT("\\\\.\\pipe\\TracerPipe"),
//	// PIPE_ACCESS_OUTBOUND,
//	// PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT,
//	// 255,
//	// 1024 * 16,
//	// 0,
//	// NMPWAIT_USE_DEFAULT_WAIT,
//	// NULL);
//	// }
//
//	//
//	// Find the window, if not found, do nothing
//	//
//	HWND TraceHwd = FindWindow(0, "Tracer");
//	if (TraceHwd == 0)
//		return (false);
//
//	// Register a clipboard format
//	if (CF_MTIText == 0)
//		CF_MTIText = RegisterClipboardFormat("MTITraceFormat");
//	if (CF_MTIText == 0)
//		return (false);
//
//	//
//	// See if there is data to send
//	int Size = str.length();
//	if (Size == 0)
//	{
//		return (true);
//	}
//
//	// if (_tracerPipe != INVALID_HANDLE_VALUE)
//	// {
//	// DWORD bytesWritten = 0;
//	// auto result = WriteFile(_tracerPipe, str.c_str(), Size+1, &bytesWritten, nullptr);
//	// auto n = GetLastSystemErrorMessage();
//	// assert(result);
//	// if (!result)
//	// {
//	// return false;
//	// }
//	// }
//
//	// Can't open clipboard
//	if (!OpenClipboard(NULL))
//		return (false);
//
//	// Now copy the data
//	HGLOBAL Data = GlobalAlloc(GMEM_MOVEABLE + GMEM_DDESHARE, Size + 1);
//	char * DataPtr = (char *)GlobalLock(Data);
//	memmove(DataPtr, str.c_str(), Size + 1);
//	if (SetClipboardData(CF_MTIText, Data) == NULL)
//	{
//		GlobalFree(Data);
//		return (false);
//	}
//
//	// Success, so home
//	GlobalUnlock(Data);
//	CloseClipboard();
//
//	//
//	// Now send a message that we are done
//	if (guMTITextMessage == 0)
//		guMTITextMessage = RegisterWindowMessage("MTITraceMessage");
//	if (guMTITextMessage == 0)
//		return (false);
//
//	// Note, this is a synchronous send
//#ifndef _MSC_VER
//	SendMessage(TraceHwd, guMTITextMessage, 0, 0);
//#endif
//	return (true);
//}
//
//// --------------SendTraceMessage-----------------Mike Braca-----Jan 2009---
//
//bool SendTraceMessage(const char *msg)
//
//	 // This sends a tracer message without using strings
//	 //
//	 // ***************************************************************************
//{
//	int Size = strlen(msg);
//	if (Size == 0)
//		return (true);
//
//	// This will direct all the messages to the terminal
//	if (g_TraceDestinationFlag == TD_TERMINAL)
//	{
//		if (msg[Size - 1] == '\n')
//			printf("%s", msg);
//		else
//			printf("%s\n", msg);
//		fflush(stdout);
//		return (true);
//	}
//	//
//	// Find the window, if not found, do nothing
//	//
//	HWND TraceHwd = FindWindow(0, "Tracer");
//	if (TraceHwd == 0)
//		return (false);
//
//	// Register a clipboard format
//	if (CF_MTIText == 0)
//		CF_MTIText = RegisterClipboardFormat("MTITraceFormat");
//	if (CF_MTIText == 0)
//		return (false);
//
//	// Can't open clipboard
//	if (!OpenClipboard(NULL))
//		return (false);
//
//	// Now copy the data
//	HGLOBAL Data = GlobalAlloc(GMEM_MOVEABLE + GMEM_DDESHARE, Size + 1);
//	char * DataPtr = (char *)GlobalLock(Data);
//	memmove(DataPtr, msg, Size + 1);
//	if (SetClipboardData(CF_MTIText, Data) == NULL)
//	{
//		GlobalFree(Data);
//		return (false);
//	}
//
//	// Success, so home
//	GlobalUnlock(Data);
//	CloseClipboard();
//
//	//
//	// Now send a message that we are done
//	if (guMTITextMessage == 0)
//		guMTITextMessage = RegisterWindowMessage("MTITraceMessage");
//	if (guMTITextMessage == 0)
//		return (false);
//
//	// Note, this is a synchronous send
//#ifndef _MSC_VER
//	SendMessage(TraceHwd, guMTITextMessage, 0, 0);
//#endif
//	return (true);
//}

// ----------MTICreateHiddenDirectory-------------John Mertus-----Sep 2001---

bool MTICreateHiddenDirectory(string str)

	 // This creates a hidden directory of the name str
	 //
	 // ***************************************************************************
{return (true);}

/* ---------------ReturnCompleteName---------------John Mertus----Feb 2002--- */

string ReturnCompleteName(const string & str)

	 // In console mode (DOS) the arg[0] contains full path name
	 //
	 /****************************************************************************/
	  {
	  return(str);
	  }


	  //----------------------rint----------------------John Mertus----Dec 2003-----

	  #if defined(_MSC_VER) || defined(__BORLANDC__)
	  double mti_rint(double x)

	  //  This round up or down  to the integer
	  //  Similar to the X/Open portability specification (XPG 4.2) of the same name
	  //
	  //****************************************************************************
	  {
	  double a = std::floor(x);
	  if(x - a < .5) return a;
	  if(x - a > .5) return a + 1.0;
	  return(std::floor(a/2) == a/2)?a:a+1.0;
	  }

	  #endif

	  //---------64 bit iostream functionality-----------Aaron Geman--Nov 2004------

	  //  These fuctions provide 64 bit int compatability with i/o input
	  //  and output operators in Visual C++
	  //
	  //  See also clip/include/Int64.h which solves a similar problem.  We
	  //  didn't notice that code when we implemented this code.  Someday,
	  //  we should pick one and stick with it!
	  //****************************************************************************
	  #ifdef _MSC_VER
	  #include <iostream>
	  #include <string>

	  std::ostream& operator<<(std::ostream& os, __int64 i )
	  {
	  char buf[20];
	  sprintf(buf,"%I64d", i );
	  os << buf;
	  return os;
	  }

	  std::istream& operator>>(std::istream& is, __int64 &i)
	  {
	  string str;
	  is >> str;
	  i = _atoi64(str.c_str());
	  return is;
	  }

	  std::ostream& operator<<(std::ostream& os, unsigned __int64 i )
	  {
	  char buf[20];
	  sprintf(buf,"%I64d", i );
	  os << buf;
	  return os;
	  }

	  std::istream& operator>>(std::istream& is, unsigned __int64 &i)
	  {
	  string str;
	  is >> str;
	  i = _atoi64(str.c_str());
	  return is;
	  }
	  #endif
