// MTIKeyDef.h: 
//
// Description: This include file contains platform-independent aliases
//              for MS Windows and X-Windows keyboard key symbol definitions. 
/*
$Header: /usr/local/filmroot/core_code/include/MTIKeyDef.h,v 1.5 2007/01/25 15:04:12 tolks Exp $
*/
//////////////////////////////////////////////////////////////////////

#if !defined(MTIKEYDEF_H)
#define MTIKEYDEF_H

//////////////////////////////////////////////////////////////////////
// Key Symbol Definitions for X-Windows

// Note:  X-Windows has key symbols for both unshifted and shifted
//        keyboard characters

#ifdef __sgi

#include <X11/X.h>
#include <X11/keysym.h>

#define MTK_INVALID         0               // Invalid or Unknown key 

// Mouse Buttons
#define MTK_LBUTTON         Button1         // Left Mouse Button
#define MTK_MBUTTON         Button2         // Middle Mouse Button
#define MTK_RBUTTON         Button3         // Right Mouse Button

// Keyboard Keys
#define MTK_BACK            XK_BackSpace    // Backspace
#define MTK_TAB             XK_Tab          // Tab
#define MTK_RETURN          XK_Return       // Return
//#define MTK_SHIFT           XK_Shift_L      // Shift
//#define ?                   XK_Shift_R      // Shift
//#define MTK_CONTROL         XK_Control_L    // Ctrl
//#define ?                   XK_Control_R    // Ctrl
//#define MTK_ALT            XK_Alt_L        // Alt
//#define ?                   XK_Alt_R        // Alt
#define MTK_PAUSE           XK_Pause        // Pause
#define MTK_CAPSLOCK        XK_Caps_Lock    // CapsLock
#define MTK_ESCAPE          XK_Escape       // Esc
#define MTK_SPACE           XK_space        // Space
#define MTK_PAGEUP          XK_Page_Up      // PageUp
#define MTK_PAGEDOWN        XK_Page_Down    // PageDown
#define MTK_END             XK_End          // End
#define MTK_HOME            XK_Home         // Home
#define MTK_LEFT            XK_Left         // Left
#define MTK_UP              XK_Up           // Up
#define MTK_RIGHT           XK_Right        // Right
#define MTK_DOWN            XK_Down         // Down
#define MTK_PRINTSCREEN     XK_Print        // PrintScreen
#define MTK_INSERT          XK_Insert       // Insert
#define MTK_DELETE          XK_Delete       // Del

// Number pad keys
#define MTK_NUMPAD0         XK_KP_0         // Num0
#define MTK_NUMPAD1         XK_KP_1         // Num1
#define MTK_NUMPAD2         XK_KP_2         // Num2
#define MTK_NUMPAD3         XK_KP_3         // Num3
#define MTK_NUMPAD4         XK_KP_4         // Num4
#define MTK_NUMPAD5         XK_KP_5         // Num5
#define MTK_NUMPAD6         XK_KP_6         // Num6
#define MTK_NUMPAD7         XK_KP_7         // Num7
#define MTK_NUMPAD8         XK_KP_8         // Num8
#define MTK_NUMPAD9         XK_KP_9         // Num9
#define MTK_MULTIPLY        XK_KP_Multiply  // Num*
#define MTK_ADD             XK_KP_Add       // Num+
#define MTK_SUBTRACT        XK_KP_Subtract  // Num-
#define MTK_DECIMAL         XK_KP_Decimal   // Num.
#define MTK_DIVIDE          XK_KP_Divide    // Num/

#define MTK_F1              XK_F1           // F1
#define MTK_F2              XK_F2           // F2
#define MTK_F3              XK_F3           // F3
#define MTK_F4              XK_F4           // F4
#define MTK_F5              XK_F5           // F5
#define MTK_F6              XK_F6           // F6
#define MTK_F7              XK_F7           // F7
#define MTK_F8              XK_F8           // F8
#define MTK_F9              XK_F9           // F9
#define MTK_F10             XK_F10          // F10
#define MTK_F11             XK_F11          // F11
#define MTK_F12             XK_F12          // F12

#define MTK_NUMLOCK         XK_Num_Lock     // NumLock
#define MTK_SCROLL          XK_Scroll_Lock  // ScrollLock
#define MTK_SEMICOLON       XK_semicolon    // ;
#define MTK_COMMA           XK_comma        // ,
#define MTK_MINUS           XK_minus        // -
#define MTK_PERIOD          XK_period       // .
#define MTK_SLASH           XK_slash        // /
#define MTK_GRAVE           XK_grave        // `
#define MTK_BRACKETLEFT     XK_bracketleft  // [
#define MTK_BACKSLASH       XK_backslash    // \  
#define MTK_BRACKETRIGHT    XK_bracketright // ]
#define MTK_APOSTROPHE      XK_apostrophe   // '

// Upper-Case Punctuation
#define MTK_ASCIITILDE      XK_asciitilde    // ~  (Shift+`)
#define MTK_EXCLAMATION     XK_exclam        // !  (Shift+1)
#define MTK_AT              XK_at            // @  (Shift+2)
#define MTK_NUMBERSIGN      XK_numbersign    // #  (Shift+3)
#define MTK_DOLLAR          XK_dollar        // $  (Shift+4)
#define MTK_PERCENT         XK_percent       // %  (Shift+5)
#define MTK_ASCIICIRCUM     XK_asciicircum   // ^  (Shift+6)
#define MTK_AMPERSAND       XK_ampersand     // &  (Shift+7)
#define MTK_ASTERISK        XK_asterisk      // *  (Shift+8)
#define MTK_PARENLEFT       XK_parenleft     // (  (Shift+9)
#define MTK_PARENRIGHT      XK_parenright    // )  (Shift+0)
#define MTK_UNDERSCORE      XK_underscore    // _  (Shift+-)
#define MTK_PLUS            XK_plus          // +  (Shift++)
#define MTK_BRACELEFT       XK_braceleft     // {  (Shift+[)
#define MTK_BRACERIGHT      XK_braceright    // }  (Shift+])
#define MTK_BAR             XK_bar           // |  (Shift+\)
#define MTK_COLON           XK_colon         // :  (Shift+;)
#define MTK_QUOTEDBL        XK_quotedbl      // "  (Shift+')
#define MTK_LESS            XK_less          // <  (Shift+,)
#define MTK_GREATER         XK_greater       // >  (Shift+.)
#define MTK_QUESTION        XK_question      // ?  (Shift+/)

#define MTK_0               XK_0            // 0
#define MTK_1               XK_1            // 1
#define MTK_2               XK_2            // 2
#define MTK_3               XK_3            // 3
#define MTK_4               XK_4            // 4
#define MTK_5               XK_5            // 5
#define MTK_6               XK_6            // 6
#define MTK_7               XK_7            // 7
#define MTK_8               XK_8            // 8
#define MTK_9               XK_9            // 9

// Upper-case alpha characters
#define MTK_A               XK_A            // A
#define MTK_B               XK_B            // B
#define MTK_C               XK_C            // C
#define MTK_D               XK_D            // D
#define MTK_E               XK_E            // E
#define MTK_F               XK_F            // F
#define MTK_G               XK_G            // G
#define MTK_H               XK_H            // H
#define MTK_I               XK_I            // I
#define MTK_J               XK_J            // J
#define MTK_K               XK_K            // K
#define MTK_L               XK_L            // L
#define MTK_M               XK_M            // M
#define MTK_N               XK_N            // N
#define MTK_O               XK_O            // O
#define MTK_P               XK_P            // P
#define MTK_Q               XK_Q            // Q
#define MTK_R               XK_R            // R
#define MTK_S               XK_S            // S
#define MTK_T               XK_T            // T
#define MTK_U               XK_U            // U
#define MTK_V               XK_V            // V
#define MTK_W               XK_W            // W
#define MTK_X               XK_X            // X
#define MTK_Y               XK_Y            // Y
#define MTK_Z               XK_Z            // Z

// Lower-case alpha characters
#define MTK_a               XK_a            // a
#define MTK_b               XK_b            // b
#define MTK_c               XK_c            // c
#define MTK_d               XK_d            // d
#define MTK_e               XK_e            // e
#define MTK_f               XK_f            // f
#define MTK_g               XK_g            // g
#define MTK_h               XK_h            // h
#define MTK_i               XK_i            // I
#define MTK_j               XK_j            // j
#define MTK_k               XK_k            // k
#define MTK_l               XK_l            // l
#define MTK_m               XK_m            // m
#define MTK_n               XK_n            // n
#define MTK_o               XK_o            // o
#define MTK_p               XK_p            // p
#define MTK_q               XK_q            // q
#define MTK_r               XK_r            // r
#define MTK_s               XK_s            // s
#define MTK_t               XK_t            // t
#define MTK_u               XK_u            // u
#define MTK_v               XK_v            // v
#define MTK_w               XK_w            // w
#define MTK_x               XK_x            // x
#define MTK_y               XK_y            // y
#define MTK_z               XK_z            // z

#define MTK_EQUAL           XK_equal        // =

#endif // #ifdef __sgi

// End of X-Window Key Symbols
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Key Symbol Definitions for MS Windows (WIN32)

// Note: MS Windows has key symbols for unshifted keyboard characters
//       only.

#ifdef _WIN32

#include <windows.h>

#define MTK_INVALID         0               // Invalid or Unknown key
 

// Mouse Buttons
#define MTK_LBUTTON         VK_LBUTTON      // Left Mouse Button
#define MTK_MBUTTON         VK_MBUTTON      // Middle Mouse Button
#define MTK_RBUTTON         VK_RBUTTON      // Right Mouse Button

// Keyboard Keys
#define MTK_BACK            VK_BACK         // Backspace
#define MTK_TAB             VK_TAB          // Tab
#define MTK_RETURN          VK_RETURN       // Return
#define MTK_SHIFT           VK_SHIFT        // Shift
#define MTK_CONTROL         VK_CONTROL      // Ctrl
#define MTK_ALT             VK_MENU         // Alt
#define MTK_PAUSE           VK_PAUSE        // Pause
#define MTK_CAPSLOCK        VK_CAPITAL      // CapsLock
#define MTK_ESCAPE          VK_ESCAPE       // Esc
#define MTK_SPACE           VK_SPACE        // Space
#define MTK_PAGEUP          VK_PRIOR        // PageUp
#define MTK_PAGEDOWN        VK_NEXT         // PageDown
#define MTK_END             VK_END          // End
#define MTK_HOME            VK_HOME         // Home
#define MTK_LEFT            VK_LEFT         // Left
#define MTK_UP              VK_UP           // Up
#define MTK_RIGHT           VK_RIGHT        // Right
#define MTK_DOWN            VK_DOWN         // Down
#define MTK_PRINTSCREEN     VK_SNAPSHOT     // PrintScreen
#define MTK_INSERT          VK_INSERT       // Insert
#define MTK_DELETE          VK_DELETE       // Del

// Number pad keys
#define MTK_NUMPAD0         VK_NUMPAD0      // Num0
#define MTK_NUMPAD1         VK_NUMPAD1      // Num1
#define MTK_NUMPAD2         VK_NUMPAD2      // Num2
#define MTK_NUMPAD3         VK_NUMPAD3      // Num3
#define MTK_NUMPAD4         VK_NUMPAD4      // Num4
#define MTK_NUMPAD5         VK_NUMPAD5      // Num5
#define MTK_NUMPAD6         VK_NUMPAD6      // Num6
#define MTK_NUMPAD7         VK_NUMPAD7      // Num7
#define MTK_NUMPAD8         VK_NUMPAD8      // Num8
#define MTK_NUMPAD9         VK_NUMPAD9      // Num9
#define MTK_MULTIPLY        VK_MULTIPLY     // Num*
#define MTK_ADD             VK_ADD          // Num+
#define MTK_SUBTRACT        VK_SUBTRACT     // Num-
#define MTK_DECIMAL         VK_DECIMAL      // Num.
#define MTK_DIVIDE          VK_DIVIDE       // Num/

// Function Keys
#define MTK_F1              VK_F1           // F1
#define MTK_F2              VK_F2           // F2
#define MTK_F3              VK_F3           // F3
#define MTK_F4              VK_F4           // F4
#define MTK_F5              VK_F5           // F5
#define MTK_F6              VK_F6           // F6
#define MTK_F7              VK_F7           // F7
#define MTK_F8              VK_F8           // F8
#define MTK_F9              VK_F9           // F9
#define MTK_F10             VK_F10          // F10
#define MTK_F11             VK_F11          // F11
#define MTK_F12             VK_F12          // F12

#define MTK_NUMLOCK         VK_NUMLOCK      // NumLock
#define MTK_SCROLL          VK_SCROLL       // ScrollLock

#ifdef __BORLANDC__
#define MTK_SEMICOLON       VK_OEM_1        // ;
#define MTK_COMMA           VK_OEM_COMMA    // ,
#define MTK_MINUS           VK_OEM_MINUS    // -
#define MTK_PERIOD          VK_OEM_PERIOD   // .
#define MTK_SLASH           VK_OEM_2        // /
#define MTK_GRAVE           VK_OEM_3        // `
#define MTK_BRACKETLEFT     VK_OEM_4        // [
#define MTK_BACKSLASH       VK_OEM_5        // \ backslash
#define MTK_BRACKETRIGHT    VK_OEM_6        // ]
#define MTK_APOSTROPHE      VK_OEM_7        // '
#else // Compiler not Borland
#define MTK_SEMICOLON       0xba            // ;
#define MTK_COMMA           0x2c            // ,
#define MTK_MINUS           0x2d            // -
#define MTK_PERIOD          0x2e            // .
#define MTK_SLASH           0x2f            // /
#define MTK_GRAVE           0x60            // `
#define MTK_BRACKETLEFT     0x5b            // [
#define MTK_BACKSLASH       0x5c            /* \ */
#define MTK_BRACKETRIGHT    0x5d            // ]
#define MTK_APOSTROPHE      0x27            // '
#endif // #ifdef __BORLANDC__

#define MTK_0               0x30            // 0
#define MTK_1               0x31            // 1
#define MTK_2               0x32            // 2
#define MTK_3               0x33            // 3
#define MTK_4               0x34            // 4
#define MTK_5               0x35            // 5
#define MTK_6               0x36            // 6
#define MTK_7               0x37            // 7
#define MTK_8               0x38            // 8
#define MTK_9               0x39            // 9

#define MTK_A               0x41            // a
#define MTK_B               0x42            // b
#define MTK_C               0x43            // c
#define MTK_D               0x44            // d
#define MTK_E               0x45            // e
#define MTK_F               0x46            // f
#define MTK_G               0x47            // g
#define MTK_H               0x48            // h
#define MTK_I               0x49            // I
#define MTK_J               0x4a            // j
#define MTK_K               0x4b            // k
#define MTK_L               0x4c            // l
#define MTK_M               0x4d            // m
#define MTK_N               0x4e            // n
#define MTK_O               0x4f            // o
#define MTK_P               0x50            // p
#define MTK_Q               0x51            // q
#define MTK_R               0x52            // r
#define MTK_S               0x53            // s
#define MTK_T               0x54            // t
#define MTK_U               0x55            // u
#define MTK_V               0x56            // v
#define MTK_W               0x57            // w
#define MTK_X               0x58            // x
#define MTK_Y               0x59            // y
#define MTK_Z               0x5a            // z

#define MTK_EQUAL           0xbb            // =

#endif // #ifdef _WIN32

//////////////////////////////////////////////////////////////////////

#endif // #if !defined(MTIKEYDEF_H)
