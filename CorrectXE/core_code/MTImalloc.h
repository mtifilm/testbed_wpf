// MTImalloc.h
//
//     Memory allocation functions.
//
//     In 64-bit world, we will just call the Windows functions.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef MTImallocH
#define MTImallocH

#ifndef _MACHINE_H
#include "machine.h"
#else // _MACHINE_H

#include "corecodelibint.h"

///////////////////////////////////////////////////////////////////////////////

#define MTI_MEMALIGN_DEFAULT 4096

MTI_CORECODELIB_API void* MTImalloc(size_t n);
MTI_CORECODELIB_API void* MTIcalloc(size_t n_elements, size_t element_size);
MTI_CORECODELIB_API void  MTIfree(void *p);
MTI_CORECODELIB_API void* MTIrealloc(void *p, size_t n);
MTI_CORECODELIB_API void* MTImemalign(size_t alignment, size_t n);
MTI_CORECODELIB_API void* MTInew(size_t n);      // call from overridden new
MTI_CORECODELIB_API void* MTInewArray(size_t n);   // new[]
MTI_CORECODELIB_API void  MTIdelete(void *p);    // call from overridden delete
MTI_CORECODELIB_API void  MTIdeleteArray(void *p); // delete[]
MTI_CORECODELIB_API char* MTIstrdup(const char *cp);  // free with MTIfree()
MTI_CORECODELIB_API void  MTImemcpy(void *pDst, const void *pSrc, size_t n);
MTI_CORECODELIB_API void  MTImemset(void *pDst, unsigned char fillChar, size_t n);
MTI_CORECODELIB_API bool  MTIcheckFencepostOverrun(void *p);

#define CheckFencepostOverrun(p) \
   if (MTIcheckFencepostOverrun(p)) { DBCOMMENT("FENCEPOST OVERRUN"); DBTRACE(p); } else


///////////////////////////////////////////////////////////////////////////////

class MTImemoryStream : public std::streambuf
{
public:
   MTImemoryStream(char *p, size_t size)
   {
      // Set output (put) stream
      this->setp(p, p + size);

      // Set input (get) stream
      this->setg(p, p, p + size);
   }

   // Example of how to read characters
   size_t read(char *b, size_t count)
   {
      return this->sgetn(b, count);
   }

   // Get current input pointer position
   char* get_ptr() const { return this->gptr(); }
};

///////////////////////////////////////////////////////////////////////////////

#ifndef __IN_MTI_MALLOC__
#if USE_MTI_MALLOC_INSTEAD_OF_REGULAR_MALLOC

#define malloc(n)     (MTImalloc((size_t)n))
#define calloc(n,s)   (MTIcalloc((size_t)n,(size_t)s))
#define free(p)       (MTIfree((void*)p))
#define realloc(p,n)  (MTIrealloc((void*)p,(size_t)n))
#define memalign(a,n) (MTImemalign((size_t)a,(size_t)n))

#else

#include "malloc.h"

#endif // USE_MTI_MALLOC_INSTEAD_OF_REGULAR_MALLOC

#endif // __IN_MTI_MALLOC__
#endif // MACHINE_H

///////////////////////////////////////////////////////////////////////////////

#endif // #ifndef MTI_MALLOC_H









