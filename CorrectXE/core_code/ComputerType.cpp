/*
	File:	ComputerType.cpp
	By:	Kevin Manbeck
	Date:	Feb 16, 2002

	The ComputerType class keep track of which hardware configuration
	is currently in use.
*/

#include "ComputerType.h"

struct ComputerTypeMap
{
  char *cp;
  EComputerType ct;
};

static const ComputerTypeMap ctmComputerTypeMap[] =
{
  {"CT_INVALID",		 CT_INVALID},
  {"CT_ONYX2_DIVO_XTHD",	 CT_ONYX2_DIVO_XTHD},
  {"CT_OCTANE1_IMPACT",		 CT_OCTANE1_IMPACT},
  {"CT_O2_PROFESSIONAL",	 CT_O2_PROFESSIONAL},
  {"CT_O2_PERSONAL",		 CT_O2_PERSONAL},
  {"CT_OCTANE2_DM2",		 CT_OCTANE2_DM2},
  {"CT_ORIGIN2_DIVO_XTHD",	 CT_ORIGIN2_DIVO_XTHD},
  {"CT_ORIGIN2_DM3",		 CT_ORIGIN2_DM3},
  {"CT_ORIGIN3_DM3",		 CT_ORIGIN3_DM3},
};

EComputerType CComputerType::queryComputerType (const string &strArg)
{
  for (int i = 0; i < sizeof(ctmComputerTypeMap) / sizeof(ComputerTypeMap); i++)
   {
    if (strcmp (strArg.c_str(), ctmComputerTypeMap[i].cp) == 0)
     {
      return ctmComputerTypeMap[i].ct;
     }
   }

  return CT_INVALID;
}  /* CComputerType */

string CComputerType::queryComputerTypeName (const EComputerType ctArg)
{
  for (int i = 0; i < sizeof(ctmComputerTypeMap) / sizeof(ComputerTypeMap); i++)
   {
    if (ctArg == ctmComputerTypeMap[i].ct)
     {
      return string (ctmComputerTypeMap[i].cp);
     }
   }

  return string("CT_INVALID");
}  /* CComputerType */
