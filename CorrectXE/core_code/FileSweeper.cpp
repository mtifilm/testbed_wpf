/*
   File Name:  FileSweeper.cpp
   Created: July 3, 2001 by John Mertus
   Version 1.00

   This quick and dirty routine creates a method for calling a method
   for EACH file name in a wild card specifier.
     E.g, if the specifier is /u/mertus/V*.c
          then a method will be called for each V*.c file in /u/mertus

   Note, this routine behaves the exact same way in windows and unix.
*/
#include "FileSweeper.h"
#include "IniFile.h"    // for TRACE_N()  .... gaggggg!
#include <sys/stat.h>
#include <errno.h>
#include <MTImalloc.h>

#include <fstream>
#include <iostream>

// Different headers
#ifdef _Windows
#define _WINDOWS
#endif

#ifdef _WINDOWS
#include <stdlib.h>
#include <fileapi.h>
#include <shlwapi.h>

// Borland
#ifdef __BORLANDC__
#include <dir.h>
#include <dirent.h>
#endif

// Visual C++
#ifdef _MSC_VER
#include <direct.h>
#endif

#ifdef __GNUG__
#include <dir.h>
#endif

#include <windows.h>                // DLL interface headers
#include <time.h>      // time_t is broken in sys/types.h in RadStudio XE4
#include <sys/types.h>
//#include <sysutils.hpp>

#else
// Unix needs
#include <sys/types.h>
#include <sys/dir.h>
#include <fnmatch.h>
#include <libgen.h>
#include <algorithm>
#endif

#ifdef _DEBUG
#define ENABLE_LONG_CLIP_DEBUG_HACK 1        // set to 0 to turn off
#else
#define ENABLE_LONG_CLIP_DEBUG_HACK 0
#endif

//----------------ExpandRelativePaths---------------John Mertus-----Jul 2001---

    string ExpandRelativePaths(const string &FileName, const string &AbsPath)

//  This really weird subroutine checks if the filename is relative, if not
//  it just returns the FileName.  If not, the AbsPath is prepended to the
//  file name.
//
//*****************************************************************************
#ifdef _WINDOWS
{
  // Go back to the old dos codes
  char drive[_MAX_DRIVE];
  char dir[_MAX_DIR];
  char file[_MAX_FNAME];
  char ext[_MAX_EXT];

  // split the string to separate elems
  _splitpath(FileName.c_str(),drive,dir,file,ext);

  //  Check relativeness
  if (strlen(dir) != 0)
    {
      if (dir[0] == '\\') return(FileName);    // Absolute path
    }
  //
  // See if AbsPath ends in a '\'
  if (AbsPath.length() != 0)
    {
      if (AbsPath[AbsPath.length()-1] != '\\')
        return(AbsPath + '\\' + dir + file + ext);
     else
        return(AbsPath + dir + file + ext);
     }
  return(FileName);

}
#else
// Unix/Linux Version
{
  if (FileName.length() == 0) return(FileName);
  if (FileName[0] == '/') return(FileName);

  if (AbsPath.length() != 0)
    {
      if (AbsPath[AbsPath.length()-1] != '/')
        return(AbsPath + '/' + FileName);
     else
        return(AbsPath + FileName);
     }
  return(FileName);
}

#endif

string CombinePathAndName(const string &path, const string &name)
{
   return AddDirSeparator(path) + name;
}

//----------------GetFilePath-----------------------John Mertus-----Jul 2001---

    string GetFilePath(const string &Name)

//  Given a file name, extract the path out of it.
//  Return is the string name
//
//*****************************************************************************
#ifdef _WINDOWS
{
  // Go back to the old dos codes
  char drive[_MAX_DRIVE];
  char dir[_MAX_DIR];
  char file[_MAX_FNAME];
  char ext[_MAX_EXT];

  // split the string to separate elems
  _splitpath(Name.c_str(),drive,dir,file,ext);

  // merge everything into one string
  string Result = (string)drive + dir;
  return(Result);

}
#else
// Unix/Linux Version
{
  // basename returns the last name, not last file name, so check if this
  // is just a path
  if (Name == "") return("");
  if (Name[Name.length()-1] == '/') return(Name);

  // Not special case, go parse it
  char *TmpPath = strdup(Name.c_str());             // Create a temporary path
  string Result = (string)dirname(TmpPath) + '/';

  free(TmpPath);                                     // Clean up
  return(Result);
}

#endif

//----------------GetPathRoot-----------------------John Mertus-----Jul 2014---

	string GetPathRoot(const string &Name)

//  Given a file name, extract the root (drive)
//  Return is the drive name with \ (backslash)
//
//*****************************************************************************
{
  // Go back to the old dos codes
  char drive[_MAX_DRIVE];
  char dir[_MAX_DIR];
  char file[_MAX_FNAME];
  char ext[_MAX_EXT];

  // split the string to separate elems
  _splitpath(Name.c_str(),drive,dir,file,ext);

  // merge everything into one string
  string Result = (string)drive + "\\";
  return Result;
}

//----------------GetFileLastDir--------------------Mike Russell----Oct 2003---

    string GetFileLastDir(const string &Name)

//  Given a path, extract the last part of the path out of it.
//  Return is the string which is the last part of the path w/o any '\'
//  Presumes passed in value will contain trailing '\'
//
//*****************************************************************************
#ifdef _WINDOWS
{
  char caName[1024];
  char *cpPtr;
  int  iLen, i;

  strcpy(caName, Name.c_str());
  iLen = strlen(caName);

  if (iLen <= 0)                // Make sure not just NULL string
    return "";

  if (caName[iLen-1] != '\\')   // Make sure ends in backslash
    return "";

  caName[iLen-1] = '\0';        // Chop off final backslash

  i = iLen - 2;
  if (i < 0)
    return "";

  while ((caName[i] != '\\') && (i >= 0))
  {
    i--;
  }

  // Don't return the frickin C: as a folder !!!!
  if (i < 0 && caName[strlen(caName) - 1] == ':')
  {
      return "";
  }

  cpPtr = &(caName[i+1]);

  // merge everything into one string
  string Result = (string)cpPtr;
  return(Result);

}
#else
// Unix/Linux Version
{
  // basename returns the last name, not last file name, so check if this
  // is just a path
  if (Name == "") return("");
  if (Name[Name.length()-1] == '/') return(Name);

  // Not special case, go parse it
  char *TmpPath = strdup(Name.c_str());             // Create a temporary path
  string Result = (string)basename(TmpPath);

  free(TmpPath);                                     // Clean up
  return(Result);
}

#endif

//----------------GetFileAllButLastDir--------------Mike Russell----Oct 2003---

    string GetFileAllButLastDir(const string &Name)

//  Given a path, extract all but the last directory.
//  Return is the string which is this last directory w/ trailing backslash
//  Presumes passed in value will contain trailing '\'
//  Note: In the case of being sent e.g. G:\, the null string is returned.
//        One might argue that just the drive letter be returned, but I
//        believe this method makes more sense.
//
//*****************************************************************************
#ifdef _WINDOWS
{
  // Go back to the old dos codes
  char drive[_MAX_DRIVE];
  char dir[_MAX_DIR];
  char file[_MAX_FNAME];
  char ext[_MAX_EXT];
  char caName[1024];
  int  iLen, i;

  strcpy(caName, Name.c_str());
  iLen = strlen(caName);

  if (iLen <= 0)                // Make sure not just NULL string
    return "";

  if (caName[iLen-1] != '\\')   // Make sure ends in backslash
    return "";

  i = iLen - 2;
  if (i < 0)
    return "";

  while ((caName[i] != '\\') && (i >= 0))
  {
    i--;
  }

  caName[i+1] = '\0';     // End string here

  // merge everything into one string
  string Result = (string)caName;
  return(Result);

}
#else
// Unix/Linux Version
{
  // dirname returns up to the last name, not last file name, so check if this
  // is just a path
  if (Name == "") return("");
  if (Name[Name.length()-1] == '/') return(Name);

  // Not special case, go parse it
  char *TmpPath = strdup(Name.c_str());             // Create a temporary path
  string Result = (string)dirname(TmpPath);

  free(TmpPath);                                     // Clean up
  return(Result);
}

#endif

//----------------GetFileName-----------------------John Mertus-----Jul 2001---

    string GetFileName(const string &Name)

//  Given a file name, extract the base name, without extension out of it.
//  Return is the file name
//
//*****************************************************************************
#ifdef _WINDOWS
{
  char drive[_MAX_DRIVE];
  char dir[_MAX_DIR];
  char file[_MAX_FNAME];
  char ext[_MAX_EXT];

  // split the string to separate elems
  _splitpath(Name.c_str(),drive,dir,file,ext);

  // Just return the string
  string Result = file;
  return(Result);

}
#else
// Unix/Linux Version
{
  // is just a path
  if (Name == "") return("");
  if (Name[Name.length()-1] == '/') return("");

  // Not special case, go parse it
  char *TmpName = basename((char *)Name.c_str());          // Create a temporary path

  // Find if a . has occured in the name
  char *CResult = strrchr(TmpName, '.');
  if (CResult == NULL) return(TmpName);

  string Result;
  Result = "";
  for (char *cp = TmpName; cp != CResult; cp++)
    Result = Result + *cp;

  return(Result);
}

#endif

//----------------GetFileExt-----------------------John Mertus-----Jul 2001---

    string GetFileExt(const string &Name)

//  Given a complete file name extract the extension.
//  In the case of multiple .'s; e.g., name.ext.xxx the last . is used
//  That is, .xxx NOT .exe.xxx
//
//*****************************************************************************
#ifdef _WINDOWS
{
  char drive[_MAX_DRIVE];
  char dir[_MAX_DIR];
  char file[_MAX_FNAME];
  char ext[_MAX_EXT];

  // split the string to separate elems
  _splitpath(Name.c_str(),drive,dir,file,ext);

  // Just return the string
  string Result = ext;
  return(Result);

}
#else
// Unix/Linux Version
{
  char *TmpName = basename((char *)Name.c_str());          // Create a temporary path
  string Result;
  Result = "";

  // Find if a . has occured in the name
  char *CResult = strrchr(TmpName, '.');
  if (CResult == NULL) return("");

  Result = CResult;
  return(Result);
}

#endif

//----------------GetFileNameWithExt---------------Mike Braca-----Dec 2006---

    string GetFileNameWithExt(const string &Name)

//  Given a file name, extract the base name, with extension out of it.
//  Return is the file name
//
//*****************************************************************************
#ifdef _WINDOWS
{
  char drive[_MAX_DRIVE];
  char dir[_MAX_DIR];
  char file[_MAX_FNAME];
  char ext[_MAX_EXT];

  // split the string to separate elems
  _splitpath(Name.c_str(),drive,dir,file,ext);

  // Just return the string
  string Result = string(file) + ext;
  return(Result);

}
#else
// Unix/Linux Version
{
  // WTF? Can't basename() handle these special cases???
  // empty?
  if (Name == "")
     return("");
  // no filename?
  if (Name[Name.length()-1] == '/')
     return("");

  // Not special case, go parse it
  return basename((char *)Name.c_str());
}

#endif

//----------------FileSweeper-----------------------John Mertus-----Jul 2001---

    bool FileSweeper(void *cl, FileSweepProc FSCB, const string &Mask, bool Abort)

//
//  This specialized routine is the core of sweeping through a list of filename
//    cl is the class instance
//    FSCB is the static call back routine
//    Mask is the mask to process
//    Abort true means stop if FSCB return false
//    Abort false means process forever
//
//  Use the macros
//      DEFINE_FILE_SWEEP_CB(CallBack, Class);  to define bool CallBack(string name);
//  and
//      FILE_SWEEP(this, CallBack, "*.DLL");  to call Callback for each file name
//
//*****************************************************************************
#ifdef _WINDOWS
{
   if (Mask == "") return(true);

   WIN32_FIND_DATA w32findData;
   HANDLE h = FindFirstFile(Mask.c_str(), &w32findData);
   if (h == INVALID_HANDLE_VALUE)
   {
      if (GetLastError() == ERROR_FILE_NOT_FOUND)
         return true;
      else
         return false;
   }

   string Path = GetFilePath(Mask);

   bool Result = false;
   bool filesRemaining = true;
   while (filesRemaining)
   {
      // Get a file name
      string FName = Path + w32findData.cFileName;

      // call the callback argument function
      Result = FSCB(cl, FName);

      // if it fails and abort is enabled, end the loop
      if ((!Result) && Abort) break;

      // non-zero value means success - opposite of MTI convention
      filesRemaining = FindNextFile(h, &w32findData);
   }

   FindClose(h);
   return(Result);
}
#else
//
//  Unix/Linux version of code
{
    DIR *dirp;
    struct direct *dp;
    bool Result = true;
    if (Mask == "") return(Result);

    string Path = GetFilePath(Mask);

    dirp = opendir(Path.c_str());                             // Open directory
    if (dirp == NULL) return(false);                  // No such directory

    // Main loop
    while ((dp = readdir(dirp)) != NULL)
      {
        if (!fnmatch(basename((char *)Mask.c_str()),dp->d_name, FNM_PATHNAME | FNM_PERIOD))
        if (strstr(dp->d_name, "") != NULL)
          {
            string FName = Path + dp->d_name;
            Result = FSCB(cl, FName);
            if ((!Result) && Abort) break;            // Stop processing if return is false
          }
      }

    //
    // Clean up
    closedir(dirp);
    return(Result);
}

#endif

//----------------SweepDown-----------------------John Mertus-----Jul 2002---

   bool SweepDown(void *cl, FileSweepProc FSCB, const std::string &Mask, bool bAbort)

//
//  This specialized routine is the core of sweeping through a list of filename
//  down the directory tree
//    cl is the class instance
//    FSCB is the static call back routine
//    Mask is the mask to process and where to start
//    Abort true means stop if FSCB return false
//    Abort false means process forever
//
//  This routine starts at the path and calls the FileSweepProc for all
// files that match in the directory and all directories below.
//
//*****************************************************************************
#ifdef _WINDOWS
{
   WIN32_FIND_DATA w32findData;
   bool Result = true;
   bool filesRemaining = true;

   // Process all the clips in ths directory
   FSCB(cl, Mask);

   // Recursive down all directories except for . and ..
   string sProperPath = AddDirSeparator(GetFilePath(Mask));
   HANDLE h = FindFirstFileEx((sProperPath +"*.*").c_str(),
	   FindExInfoStandard,
	   &w32findData,
	   FindExSearchLimitToDirectories,
	   NULL,
	   0);
   if (h == INVALID_HANDLE_VALUE) return(false);

   while (filesRemaining)
   {
      // Get an name
  	   if ((strcmp(w32findData.cFileName, ".") != 0) && (strcmp(w32findData.cFileName, "..") != 0))
      {
		   string FName = AddDirSeparator(sProperPath + w32findData.cFileName) + GetFileName(Mask);
         Result = SweepDown(cl, FSCB, FName, bAbort);
         if ((!Result) && bAbort) break;                   // Stop processing if return is false
      }
	   filesRemaining = FindNextFile(h, &w32findData);
   }

   FindClose(h);
   return true;
}
#else
{
// TBD: Write UNIX version
  return false;
}
#endif

//-----------------DoesFileExist------------------John Mertus----Sep 2001-----

     bool DoesFileExist(const string &Name)

//
//  This returns true of the file specified by Name exists.
//  It returns false if this file is a directory, other special file
//  or does not exist.
//
//****************************************************************************
{
#if 0
   struct stat statbuf;
   if (stat(Name.c_str(), &statbuf) != 0) return(false);
   return((statbuf.st_mode) & S_IFREG);
#endif

   WIN32_FIND_DATA wfdFindFileData;

#if ENABLE_LONG_CLIP_DEBUG_HACK
   string strFileName = GetLongClipDebugHackFileName(Name);
   LPCTSTR lpFileName = strFileName.c_str();
#else
   LPCTSTR lpFileName = Name.c_str();
#endif

   HANDLE hFind = ::FindFirstFile(lpFileName, &wfdFindFileData);
   if (hFind == INVALID_HANDLE_VALUE)
   {
      string errorMessage = GetLastSystemErrorMessage();
      int lastError = ::GetLastError();
      switch (lastError)
      {
         case ERROR_FILE_NOT_FOUND:
            TRACE_3(errout << "$$$$$$$$$$$ File not found: " << lpFileName << " $$$$$$$$$$$");
            break;

         case ERROR_PATH_NOT_FOUND:
            TRACE_3(errout << "$$$$$$$$$$$ Path not found: " << lpFileName << " $$$$$$$$$$$");
            break;

         case ERROR_DIRECTORY:
            TRACE_3(errout << "$$$$$$$$$$$ Bad directory: " << lpFileName << " $$$$$$$$$$$");
            break;

         default:
            TRACE_0(errout << "ERROR: Find FAIL: " << lpFileName
                           << ": [" << lastError << "] " << errorMessage);
            break;
      }

      return false;
   }

   FindClose(hFind);

   return ((wfdFindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0
          && (wfdFindFileData.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) == 0);

}

//-----------------GetFileLength--------------Michael Russell----Aug 2003-----

     MTI_UINT64 GetFileLength(const string &Name)

//
//  This returns the MTI_UINT64 length of a file.
//
//****************************************************************************
{
  MTI_UINT64                u64Size;
#ifdef _WINDOWS
  bool                      bResult = false;
  WIN32_FILE_ATTRIBUTE_DATA fAttribData;

#if ENABLE_LONG_CLIP_DEBUG_HACK
   bResult = GetFileAttributesEx(GetLongClipDebugHackFileName(Name).c_str(),
                GetFileExInfoStandard, &fAttribData);
#else
  bResult = GetFileAttributesEx(Name.c_str(), GetFileExInfoStandard,
              &fAttribData);
#endif

  if (bResult)      // Success
    u64Size = (((MTI_UINT64) fAttribData.nFileSizeHigh) << 32)
                | (MTI_UINT64) fAttribData.nFileSizeLow;
  else
    u64Size = 0;

#else   // Unix
  // Note that Linux should get a #define somewhere so the 64-bit routines
  //   are used - this is done with:
  //     #define _FILE_OFFSET_BITS 64
  //   before early #include's.
  struct stat statbuf;
  int    iResult;

  iResult = stat(Name.c_str(), &statbuf);
  if (iResult == 0)
    u64Size = statbuf.st_size;
  else
  {
    perror("GetFileLength");        // TTT - probably a better way
    u64Size = 0;
  }

#endif
  return(u64Size);
}

//--------------BytesPerSector Cache----------Mike Braca----Mar 2005v-----

struct BPSCache
{
    char caLastDiskName[512];
    unsigned long ulLastBytesPerSector;

    BPSCache()
    : ulLastBytesPerSector(0)
    {
        caLastDiskName[0] = '\0';
    }
};

static BPSCache bpsCache;

//-----------------GetBytesPerSector----------Michael Russell----Oct 2003-----

     bool GetBytesPerSector(char *caFilename, unsigned long *ulpBytesPerSector)

//
//  This returns the Bytes per Sector for the disk that a given file
//    lives on.  This is useful only from within Windows.  For Unix
//    we return false.
//
//****************************************************************************
{
#ifdef _WINDOWS
  char *cpPosBackSlash;
  int   iPosBackSlash;
  char  caDiskname[512];
  char *cpDiskname = caDiskname;

  unsigned long ulSectorsPerCluster, ulNumberOfFreeClusters;
  unsigned long ulTotalNumberOfClusters;


  /*
   * Get the Number of Bytes Per Sector for current disk
   */
  if (caFilename[1] != ':')
  {
    // UNC name
    cpDiskname = NULL;

    // "5" == strlen("\\a\b") here.
    if (strlen(caFilename) >= 5 && caFilename[0] == '\\' && caFilename[1] == '\\')
    {
      // Find the 4th back slash ( \\host\dir\ )
      char *cpPos = caFilename + 3;
      int iBSCount = 2;
      do
      {
        if (*cpPos == '\\')
            iBSCount++;
      }
      while (*(++cpPos) != '\0' && iBSCount < 4);

      if (iBSCount == 3 || iBSCount == 4)
      {
        strcpy(caDiskname, caFilename);
        if (iBSCount == 3)
            caDiskname[cpPos++ - caFilename] = '\\';
        caDiskname[cpPos - caFilename] = '\0';
        cpDiskname = caDiskname;
      }
    }
  }
  else
  {
    // Drive letter name
    cpPosBackSlash = strchr(caFilename, '\\');    // Find the backslash
    if (cpPosBackSlash == NULL)
      iPosBackSlash = 0;
    else                 // Since strchr returns a pointer to the \, we subtract
      iPosBackSlash = cpPosBackSlash - caFilename;  // pointers to get the position

    if (iPosBackSlash != 0)
    {                                     // Create string e.g. "F:\"
      strcpy(caDiskname, caFilename);
      caDiskname[iPosBackSlash+1] = '\0';
    }
    else                  // When NULL, the root of the current directory
      cpDiskname = NULL;  //   is used.  This may not be what is desired.
  }

  if ((cpDiskname != NULL) && (0 == strcmp(cpDiskname, bpsCache.caLastDiskName)))
  {
     *ulpBytesPerSector = bpsCache.ulLastBytesPerSector;
  }
  else
  {
    if (GetDiskFreeSpace(cpDiskname, &ulSectorsPerCluster, ulpBytesPerSector,
      &ulNumberOfFreeClusters, &ulTotalNumberOfClusters) == 0)
    {

      // TTT - probably should do some error handling
      *ulpBytesPerSector = 0;
      return (false);  // Problem
    }
    else
    {
      strcpy(bpsCache.caLastDiskName, cpDiskname);
      bpsCache.ulLastBytesPerSector = *ulpBytesPerSector;
    }
  }

  return(true); // OK
#else       // Unix
  *ulpBytesPerSector = 0;
  return(false);
#endif
}
//---------DoesWritableFileExist------------------John Mertus----Sep 2001-----

     bool DoesWritableFileExist(const string &Name)

//
//  This returns true of the file specified by Name exists and can be written
//  False otherwise
//
//****************************************************************************
{
   struct stat statbuf;

#if ENABLE_LONG_CLIP_DEBUG_HACK
   if (stat(GetLongClipDebugHackFileName(Name).c_str(), &statbuf) != 0)
      return(false);
#else
   if (stat(Name.c_str(), &statbuf) != 0) return(false);
#endif

   return(((statbuf.st_mode) & S_IFREG) && (S_IWRITE & statbuf.st_mode));
}

//-----------------DoesDirectoryExist-------------John Mertus----Sep 2001-----

     bool DoesDirectoryExist(const string &Name)

//
//  This returns true of the file specified by Name is a directory
//  It returns false otherwise and errno is set
//
//****************************************************************************
{
   struct stat statbuf;
   // Windows and unix differ in directory specifications, a directory
   // separtator at the end doesn't change Unix but does windows
   // So elimate it
   string TmpName = Name;
   if (TmpName == "") return(false);     // Blank names are not directories

   // Windows is weird, the stat can not end in a DIR_SEPARATOR except when
   // its the system root directory. Thus first try it as sent, then try it
   // without separator

   // This is a little fudge, if file does not exist, stat does nothing
   // so by setting the mode to zero, it all works,
   statbuf.st_mode = 0;
   stat(TmpName.c_str(), &statbuf);
   if ((statbuf.st_mode) & S_IFDIR) return(true);

   //  Try it without
   if (Name[Name.length()-1] != DIR_SEPARATOR) return(false);

   TmpName = Name.substr(0, Name.length()-1);
   if (stat(TmpName.c_str(), &statbuf) != 0) return(false);
   return((statbuf.st_mode) & S_IFDIR);
}

//-----------DoesWritableDirectoryExist-------------John Mertus----Sep 2001-----

	  bool DoesWritableDirectoryExist(const string &name)

//
//  This returns true of the file specified by Name is a directory
//  It returns false otherwise.
//
//****************************************************************************
{
	// Windows and unix differ in directory specifications, a directory
	// separtator at the end doesn't change Unix but does windows
	// So eliminate it!
	string localPath = RemoveDirSeparator(name);
	if (localPath == "")
	{
		return false;
	}

#ifdef USE_STAT_TO_GET_FILE_ATTRIBUTES
	struct stat statbuf;
	// This is a little fudge, if file does not exist, stat does nothing
	// so by setting the mode to zero, it all works,
	statbuf.st_mode = 0;
	stat(localPath.c_str(), &statbuf);
	TRACE_0(errout << "DoesWritableDirectoryExist(" << localPath << ") =>  S_IFDIR=" << (statbuf.st_mode & S_IFDIR) << " S_IWRITE=" << (statbuf.st_mode & S_IWRITE));

	return (statbuf.st_mode & S_IFDIR) && (statbuf.st_mode & S_IWRITE);
#else
	auto fileAttributes = GetFileAttributes(localPath.c_str());
	if (fileAttributes == INVALID_FILE_ATTRIBUTES)
	{
		TRACE_0(errout << "DoesWritableDirectoryExist(" << localPath << ") =>  INVALID_FILE_ATTRIBUTES");
		return false;
	}

	if ((fileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0)
	{
		TRACE_0(errout << "DoesWritableDirectoryExist(" << localPath << ") =>  FILE_ATTRIBUTE_DIRECTORY == 0");
		return false;
	}

	if ((fileAttributes & FILE_ATTRIBUTE_READONLY) != 0)
	{
		TRACE_0(errout << "DoesWritableDirectoryExist(" << localPath << ") =>  FILE_ATTRIBUTE_READONLY != 0");
		return false;
	}

   return true;
#endif
}

string removeFileSpecs(const string &filePath)
{
	vector<char> temp(filePath.size());
	memcpy(temp.data(), filePath.data(), filePath.size());
	auto status = PathRemoveFileSpecA(temp.data());
   if (status)
   {
     return string(temp.data());
   }

	return filePath;
}

string getFileName(const string &filePath)
{
	vector<char> temp(filePath.size());
   memcpy(temp.data(), filePath.data(), filePath.size());
	PathStripPathA(temp.data());
   return string(temp.data());
}

//---------------MakeDirectory--------------------John Mertus----Mar 2019-----

bool MakeDirectoryBackwards(const string &str)

// Because of the issue with parsing a network "devices" we
// need to look up from the target not down from top.  Its 18 years
// later and I'm still writing this shit.
//
//****************************************************************************
{
	if (str.size() == 0)
   {
   	return false;
   }

   if (DoesDirectoryExist(str))
	{
      return true;
   }

   // strip off last backslash
   auto tmpStr = RemoveDirSeparator(str);

	auto upOneDirectory = removeFileSpecs(tmpStr);
   auto directoryName = getFileName(tmpStr);
//   DBTRACE(str);
//   DBTRACE(upOneDirectory);
//   DBTRACE(directoryName);

   if ((upOneDirectory.length() == 0) || (directoryName.length() == 0))
   {
   	return false;
   }

   if (upOneDirectory == directoryName)
   {
      // Avoid infinite recursion!!
      return false;
   }

   if (MakeDirectoryBackwards(upOneDirectory) == true)
   {
      if (CreateDirectoryA(str.c_str(), nullptr))
      {
      	return true;
      }

      auto err = GetLastError();
      if (err == ERROR_PATH_NOT_FOUND)
      {
      	return false;
      }

      return true;
   }

   return false;
}

//---------------MakeDirectory--------------------John Mertus----Sep 2001-----

   bool MakeDirectory(const string &sName)

//
//  This creates a directory with name Name
//  Name must end with the directory separator.
//  True if the directory already exists or is created
//  False otherwise
//
//****************************************************************************
{
	return MakeDirectoryBackwards(sName);

//   string Name = AddDirSeparator(sName);               // Force a directory
//   if (DoesDirectoryExist(Name)) return(true);        // Nothing to do
//   // Loop through the name, finding the next directory separator
//   // and seeing if that exists.  If not try to create it.
//   int loc = 0;      // Start at the beginning
//   while (true)
//    {
//        int n = Name.find(DIR_SEPARATOR,loc)+1;
//        if (n == 0) return(true);
//        string TempDir = Name.substr(0,n);
//
//// Windows and unix differ in their creation
//#ifdef _WINDOWS
//        if (!DoesDirectoryExist(TempDir))
//           if (mkdir(TempDir.c_str()) == -1)
//                {
//                  theError.set(FILESWEEPER_ERROR_MKDIR_FAILED,
//                        (string)"Could not create directory " + TempDir + "\nBecause: " + strerror(errno));
//                  return(false);
//                }
//        // Windows does not have .xxx meaning hidden, add that
//        // First check it is of this form
//        if (TempDir.size() >= 2)
//          {
//            if (TempDir[loc] == '.')
//              if (isalnum(TempDir[loc+1])!=0)
//                SetFileAttributes(TempDir.c_str(), FILE_ATTRIBUTE_HIDDEN);
//          }
//#else
//        if (!DoesDirectoryExist(TempDir))
//	  {
//             if (mkdir(TempDir.c_str(), 0755) == -1)
//               {
//                  theError.set(FILESWEEPER_ERROR_MKDIR_FAILED,
//                        (string)"Could not create directory " + TempDir + "\nBecause: " + strerror(errno));
//                  return(false);
//               }
//	  }
//#endif
//        loc = n;
//    }
}

//----------------MakeHiddenDirectory-------------John Mertus----Sep 2001-----

   bool MakeHiddenDirectory(const string &Name)

//
//  This creates a directory of name Name
//  Under windows this will hide the directory.
//  Unix uses . to hide directories and that is up to the programmer.
//
//****************************************************************************
{
   if (!MakeDirectory(Name)) return(false);        // could not make directory
//
//  Windows code to change attributes
//
#ifdef _WINDOWS
   if (!SetFileAttributes(Name.c_str(), FILE_ATTRIBUTE_HIDDEN))
     {
        theError.set(FILESWEEPER_ERROR_SET_FILE_ATTRIBUTES, (string)"Could not make directory hidden\n" +
                GetLastSystemErrorMessage());
        return(false);
     }
   return(true);
#else
   return(true);
#endif
}

//--------------MTICopyFile----------------Matt McClure----Nov 2005-----

MTI_CORECODELIB_API bool MTICopyFile(const string &existingFilePath,
				     const string &newFilePath,
				     bool failIfNewFileExists)
{
#if 0 // FORMERLY
   // behavior closely modeled after Windows API CopyFile();
   // http://msdn.microsoft.com/library/default.asp?url=/library/en-us/fileio/fs/copyfile.asp
   // GetLastError() after failure is not supported.
   //
   std::ifstream existingFile(existingFilePath.c_str(), std::ios::binary);
   if (existingFile == 0 || existingFile.fail())
      return false;

   if (failIfNewFileExists == true && DoesFileExist(newFilePath) == true)
      return false;

   std::ofstream newFile(newFilePath.c_str(), std::ios::binary);
   if (newFile == 0 || newFile.fail())
      return false;

   newFile << existingFile.rdbuf();

   if (newFile.fail() || existingFile.fail())
      return false;

   return true;
#else // NOW using WINDOWS API
   //
   if (!::CopyFile(existingFilePath.c_str(),
                   newFilePath.c_str(),
                   failIfNewFileExists))
   {
      theError.set(FILESWEEPER_ERROR_COPY_FAILED,
                   string("Could not copy "
                   + existingFilePath + string(" to ") + newFilePath + string("\n")
                   + GetLastSystemErrorMessage()));
      return false;
   }

   return true;
#endif
}

//-----------------ReplaceFileExt-----------------John Mertus----Apr 2002-----

      string ReplaceFileExt(const string &Name, const string &Ext)

//  This returns the file name Name with any extensio replaced by Ext.
//  If no extension in Name, the return adds ext to the name
//
//   Name is a file Name
//   Ext is the extension including the ., e.g., .ini
//
//****************************************************************************
{
  string sFE = GetFileExt(Name);
  if (sFE == "") return(Name + Ext);   // No extension, just return it
  return(GetFilePath(Name) + GetFileName(Name) + Ext);
}

//-------------------AddFileExt-------------------John Mertus----Apr 2002-----

    string AddFileExt(const string &Name, const string &Ext)

//  if Name is contains an extension, this returns Name
//  if Name does not have an extension, this returns Name + Ext
//
//  Name is a file name
//  Ext is the extension including the .; e.g., .ini
//
//****************************************************************************
{
  if (GetFileExt(Name) == "") return(Name + Ext);
  return(Name);
}

//----------------AddDirSeparator----------------John Mertus----Nov 2001-----

    string AddDirSeparator(const string &Name)

//
//  This takes name, checks to see if there is a directory separator at the
//  end, if there is, it returns the name.  If no separator, it adds one and
//  returns that name.
//
//****************************************************************************
{
   if (Name.size() == 0) return(Name);
   if (Name[Name.length()-1] == DIR_SEPARATOR) return(Name);
   return(Name + DIR_SEPARATOR);
}

//--------------RemoveDirSeparator---------------John Mertus----Nov 2001-----

    string RemoveDirSeparator(const string &Name)

//
//  This takes name, checks to see if there is a directory separator at the
//  end, if there is, removes it  If no separator, it returns the name.
//
//****************************************************************************
{
   if (Name.size() == 0) return(Name);
   if (Name[Name.length()-1] == DIR_SEPARATOR) return(Name.substr(0, Name.length()-1));
   return(Name);
}

//----------------RemoveWhiteSpace---------------Kurt Tolksdorf----Oct 2010

    string RemoveWhiteSpace(const string &Name)

{
    string retVal;

    int length = Name.size();

    for (int i=0; i<length; i++) {

       if (Name[i] != ' ')
          retVal.push_back(Name[i]);
    }

    return retVal;
}


//----------------AddSubfolder-------------------Kurt Tolksdorf----Oct 2010

    string AddSubfolder(const string &Name, const string &Subfolder)

//
// This takes name, checks to see if the subfolder is the last part of the path
// and adds it if it is not

{
   string name = RemoveDirSeparator(Name);
   string subfolder = RemoveDirSeparator(Subfolder);

   int length = name.size();

   int i;
   for (i=length-1; i>= 0; i--) {

      if (name[i] == '\\')
         break;
   }

   if (name.substr(i+1, length) != subfolder)
      name = name + "\\" + subfolder;

   return name;
}

//----------------RemoveSubfolder-------------------Kurt Tolksdorf----Oct 2010

    string RemoveSubfolder(const string &Name, const string &Subfolder)

//
// This takes name, checks to see if the subfolder is the last part of the path
// and removes it if it is

{
   string name = RemoveDirSeparator(Name);
   string subfolder = RemoveDirSeparator(Subfolder);

   int length = name.size();

   int i;
   for (i=length-1; i>= 0; i--) {

      if (name[i] == '\\')
         break;
   }

   if (name.substr(i+1, length) == subfolder)
      name = name.substr(0, i);

   return name;
}



//-------------GetSystemErrorMessage-------------John Mertus----Nov 2001-----

  string MTI_CORECODELIB_API GetSystemErrorMessage(int errorCode)

// This converts a system error code to a human-readable string.
//
//****************************************************************************
{
   const DWORD bufferSize = 256;
   TCHAR errorMessageBuffer[bufferSize];

   // This is the windows call to get the proper error message
   FormatMessage(
      FORMAT_MESSAGE_FROM_SYSTEM,
      NULL,
      errorCode,
      MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US),
      errorMessageBuffer,
      bufferSize,
      NULL);

   return string((char *) errorMessageBuffer);
}

//-------------GetLastSystemErrorMessage---------John Mertus----Nov 2001-----

  string MTI_CORECODELIB_API GetLastSystemErrorMessage(void)

// This returns the last system error as a human-readable string.
//
//****************************************************************************
{
   return GetSystemErrorMessage(GetLastError());
}

bool IsNotADigit (char c) {
  return !::isdigit(c);
}

//----------------StringAllDigits-----------------John Mertus----Jun 2002-----

  bool StringAllDigits(const string &s)

//  This just return true if the string contains all digits
//
//****************************************************************************
{
  return (s.end() == find_if(s.begin(),s.end(), IsNotADigit));
}

bool IsNotASpace (char c) {
  return !::isspace(c);
}


//-----------------StringTrimLeft-----------------John Mertus----Jun 2002-----

  string StringTrimLeft(const string &s)

//  This returns the string with the left white spaces trimmed off
//
//****************************************************************************
{
  string sR = s;
  sR.erase(sR.begin(), find_if(sR.begin(), sR.end(), IsNotASpace));
  return(sR);
}

//----------------StringTrimRight-----------------John Mertus----Jun 2002-----

  string StringTrimRight(const string &s)

//  This returns the string with the right white spaces trimmed off
//
//****************************************************************************
{
  string sR = s;
  sR.erase(find_if(sR.rbegin(), sR.rend(), IsNotASpace).base(), sR.end());

  return(sR);
}

//-------------------StringTrim-------------------John Mertus----Jun 2002-----

  string StringTrim(const string &s)

//  This trims the leading and trailing white spaces
//
//****************************************************************************
{
  return(StringTrimLeft(StringTrimRight(s)));
}

//////////////////////////////////////////////////////////////////////
//
// This will copy all files in the specified srcDir to the specified
//   dstDir.  Files which are directories (FILE_ATTRIBUTE_DIRECTORY)
//   are NOT copied.  No recursion is done.
//
//   Created: 7/24/2006 - Michael Russell
//
//   Typical Usage:
//      i = CopyAllRegularFiles(srcDir, dstDir, reason);
//   Inputs:
//      srcDir = string (e.g. "C:\\MTIShare\\MTIBins\\Proj\\Clip001")
//      dstDir = string (e.g. "C:\\MTIShare\\CDArchive\\Proj_Clip001")
//      skipExistingFiles = bool, TRUE to skip existing files
//      skipIniBackupFiles = bool, TRUE to skip ini backups (end in .ini.NNN)
//      excludeList = StringList, a list of filenames to exclude from the copy
//   Returns:
//      int              = number of files copied
//                         -1 = error
//      reason           = string - error reason
//
//////////////////////////////////////////////////////////////////////
int CopyAllRegularFiles(const string &srcDir,
   const string &dstDir,
   const bool skipExistingFiles,
   bool skipIniBackupFiles,
   const StringList &excludeList,
   string& reason)
{
   int numFilesCopied = 0;
   HANDLE hFind;
   WIN32_FIND_DATA lpFindFileData;
   string shortFN      = "";
   string srcDirSearch = srcDir + DIR_SEPARATOR + "*.*";

   reason = "";

   hFind = FindFirstFile(srcDirSearch.c_str(), &lpFindFileData);
   if (hFind == INVALID_HANDLE_VALUE)
   {
      if (GetLastError() == ERROR_FILE_NOT_FOUND)
      {
         // This is OK - no matches
         return 0;
      }
      else
      {
         MTIostringstream os;
         os << "CopyAllRegularFiles: could not FindFirstFile(" << srcDirSearch.c_str()
               << ") because:" << endl << GetLastSystemErrorMessage();
         TRACE_0(errout << "ERROR: " << os.str());
         reason = os.str();
         return -1;
      }
   }

   do
   {
      shortFN = lpFindFileData.cFileName;
      string srcFN;
      string dstFN;

      // Copy file if not a directory
      if (!(lpFindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
      {
         srcFN = srcDir + DIR_SEPARATOR + shortFN;
         dstFN = dstDir + DIR_SEPARATOR + shortFN;

         if (skipIniBackupFiles)
         {
            string numbers("0123456789");
            size_t found = srcFN.find_last_not_of(numbers);

            if (found != string::npos                    // not all digits?
            && found < (srcFN.size() - 1)                // ends with digits?
            && found > 4                                 // has room for "x.ini.N*"
            && (srcFN.substr(found - 4, 5) == ".ini."
               || srcFN.substr(found - 4, 4) == ".INI."))
            {
               // It's an INI backup file - skip it!
               continue;
            }
         }

            bool skipIt = false;
            for (int i = 0; i < (int)excludeList.size(); ++i)
            {
               if (0 == strcasecmp(GetFileNameWithExt(srcFN).c_str(),
                                   excludeList[i].c_str()))
               {
                  skipIt = true;
                  break;
               }
            }

            if (skipIt)
            {
               continue;
            }

         // Do copy if not skipping existing files or (if skipping
         // existing files, but file does not exist)
         if (!skipExistingFiles || (skipExistingFiles && !DoesFileExist(dstFN)))
         {
            // in the following, "false" means "overwrite existing file" as
            // opposed to failing
            bool bRet = CopyFile(srcFN.c_str(), dstFN.c_str(), false);
            if (!bRet)
            {
               MTIostringstream os;
               os << "CopyAllRegularFiles: could not CopyFile(" << srcFN.c_str() << " --> " << dstFN.c_str()
                     << ") because:" << endl << GetLastSystemErrorMessage();
               TRACE_0(errout << "ERROR: " << os.str());
               reason = os.str();
               FindClose(hFind);
               return -1;
            }

            numFilesCopied++;
         }
      }

   }
   while (FindNextFile(hFind, &lpFindFileData));

   FindClose(hFind);

   return numFilesCopied;
}

//////////////////////////////////////////////////////////////////////
//
// This will find the rightmost run of digits in a string
//
void FindRightmostDigitRun(const string& inStr, int& digitIndex, int& digitCount)
{
   digitIndex = 0;
   digitCount = 0;

   string::const_iterator strIterator = inStr.begin();
   strIterator += GetFilePath(inStr).size();  // skip past path

   string::const_iterator strEnd = inStr.end();

   int newDigitCount = 0;
   string::const_iterator newDigitStart;

   // Search for the RIGHTMOST run of digits
   while (strIterator != strEnd)
      {
      // Skip over non-digits
      while (strIterator != strEnd && !isdigit(*strIterator))
         ++strIterator;

      if (strIterator != strEnd)
         {
         // Count digits
         newDigitStart = strIterator;
         while (strIterator != strEnd && isdigit(*strIterator))
            ++strIterator;
         newDigitCount = strIterator - newDigitStart;
         }
      }

   digitCount = newDigitCount;
   if (newDigitCount > 0)
      digitIndex = newDigitStart - inStr.begin();

}

//////////////////////////////////////////////////////////////////////

MTI_UINT64 GetLastModifiedTime(const string &filename)
{
   union
   {
      MTI_UINT64 lastModifiedTimeAsInt64;
      FILETIME lastModifiedTImeAsFILETIME;
   } retVal;

   retVal.lastModifiedTimeAsInt64 = 0;

   // Open the file to get the last write time
   LPCTSTR lpFileName = (LPCTSTR) filename.c_str();
   DWORD dwDesiredAccess = 0; //GENERIC_READ;
   DWORD dwShareMode = FILE_SHARE_READ | FILE_SHARE_WRITE;
   LPSECURITY_ATTRIBUTES lpSecurityAttributes = nullptr;
   DWORD dwCreationDisposition = OPEN_EXISTING;
   DWORD dwFlagsAndAttributes = 0;
   HANDLE hTemplateFile = INVALID_HANDLE_VALUE;
   HANDLE hFile = CreateFile(lpFileName, dwDesiredAccess, dwShareMode,
                             lpSecurityAttributes, dwCreationDisposition,
                             dwFlagsAndAttributes, hTemplateFile);

   if (hFile == INVALID_HANDLE_VALUE)
   {
      auto errorCode = GetLastError();
      TRACE_0(errout << "ERROR: Can't get last write time for file "
                     << lpFileName << endl
                     << "        Reason: " << GetSystemErrorMessage(errorCode) << "(" << errorCode << ")");
   }
   else
   {
      // Successfully opened - get the last write time
      bool success = GetFileTime(hFile, NULL, NULL, &retVal.lastModifiedTImeAsFILETIME);
      CloseHandle(hFile);
   }

   return retVal.lastModifiedTimeAsInt64;
};

//////////////////////////////////////////////////////////////////////
//
// This is a hack where we can do debugging of very long clips, but
// only need 10 actual frames of data. Here we find the rightmost run
// of digits in a file name, and replace all but the rightmost of those
// with _'s
//
string GetLongClipDebugHackFileName(const string &Name)
{
   string localName = Name;

#if ENABLE_LONG_CLIP_DEBUG_HACK

   string nameWithoutPath = GetFileName(Name);
   if (nameWithoutPath.substr(0, 6) == "xyzzy-")
   {
      // Hack ON! '
      int digitIndex, digitCount;
      FindRightmostDigitRun(localName, digitIndex, digitCount);
      // Replace all but the final digit with _'s
      if (digitCount > 1)
      {
          localName.replace(digitIndex, digitCount-1, digitCount-1, '_');
	  }
   }

#endif

   return localName;
}

//////////////////////////////////////////////////////////////////////

string CombinePathNames(const vector<string> &names, const string &ext)
{

  string result = "";
  for (unsigned int i=0; i < names.size(); i++)
  {
     string name = names[i];
     result = AddDirSeparator(result) + name;
  }

  return result + ext;
}

//////////////////////////////////////////////////////////////////////
