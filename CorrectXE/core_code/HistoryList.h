/*
   File Name:  MenuHistory.h
   Created: Sept 11, 2001 by John Mertus
   Version 1.00

   This is a class that lies below the GUI's that handles all
   some common file class handling.

   To use
     Set IniFileName to the configuration file to use
     Set IniSection to the section (Default is file history)

     ReadProperties() to read all the names
     Add("c:\test\data.dat");    Adds name to top of list.
     On destroy, the properties are automatically written

   CHistory list also will notify if any changes.  See HistoryMenu or
   and PropX.h for how this is done.
*/

#ifndef _HISTORYLIST
#define _HISTORYLIST

#include "corecodelibint.h"
#include "machine.h"
#include "PropX.h"
#include <vector>
class MTI_CORECODELIB_API CHistoryList : public vector<string>, public CBHook
{
  public:
    string  IniFileName;            // Storage for the inifile
    string  IniSection;

    CHistoryList(int n=10);

    virtual bool WriteProperties(void);  // Writes the configuration to the file
    virtual bool ReadProperties(void);   // Reads configuration.
    void Add(string);                    // Pushes on top
    bool Delete(int);                    // Deletes index from list
    int  Index(string);                  // Index of the string
    bool Remove(string);                 // Deletes string from list
    void Clear(void);                    // Clears out the queue

    // Not used
    int     Selected;               // Selected file
    CBHook  PCBOpen;                // Hook when an open occures
    //
    // Usual getters, setters
    // Note: These should never be used except for rare cases
    void SetMaxSize(int Value);
    int  GetMaxSize(void);
    void SetCaseSensitivity(bool Value);
    bool GetCaseSensitivity(void);

    // Deal with # not allowed in ini files;
    // if the substitute is not '\0' then all #'s will be converted
    // to the substitute before writing to the ini file, and all
    // substitute will be converted to #'s when read from the ini file
    void SetPoundSubstitute(char c);
    char GetPoundSubstitute(void);

#if defined(__GNUG__)  && (__GNUG__ < 3)
    vector<string>::reference at(size_type n);
    vector<string>::const_reference at(size_type n) const;
#endif

  private:
    int fiMaxSize;                     //  Maximum names on list
    int fbCaseSensitive;               //  List is case sensitive
    char cPoundSubstitute;             // char to substitute for # in ini file
    bool Compare(string, string);      //  True if the strings are the same
};

#endif
