/*
   File Name:  CPlugin.h
   Created: June 3, 2001 by John Mertus
   Version 1.00

   This is the plugin class template and functions
      CPlugin     -  Base Plugin Class
      CPluginList -  Helper class that list plugins.
*/
//---------------------------------------------------------------------------

#ifndef CPluginH
#define CPluginH

#include "machine.h"
#include "FileSweeper.h"
#include "corecodelibint.h"
#include <string>
#include <vector>

//
// Export, import definitions
//
class MTI_CORECODELIB_API CPlugin;
class MTI_CORECODELIB_API CPluginList;
class MTI_CORECODELIB_API CPluginStateWrapper;

//
//  This defines the PlugIN RESource numbers
//
#define PIN_RES_TOOL_NAMES   1                // List of Names of tools char *[n]
#define PIN_RES_TOOL_NUMBER  2                // Size of the list (also ends in blank name)
#define PIN_RES_PLUGIN_NAME  3                // Name of plugin char *
#define PIN_RES_COPYRIGHT    4                // Copyright
#define PIN_RES_TRADEMARK    5                // Trademark
#define PIN_RES_COMPANY      6                // Company
#define PIN_RES_ICON         7                // Bitmap of icon.
#define PIN_RES_FEATURE      8                // Encrypted feature name table


//
// Messages
#define PIN_MSG_SET_PARENT   1                // Parent window pointer

enum EPluginState {
	 PLUGIN_STATE_INVALID=0,
	 PLUGIN_STATE_UNKNOWN,
	 PLUGIN_STATE_ENABLED,
	 PLUGIN_STATE_DISABLED,
	 PLUGIN_STATE_DEMO};

//
//**************************CPluginState***************************************
//
//  After a plugin is created, the CPluginStatus class contains information
//  about the current status of the plugin.

struct PluginStateStruct
{
	int m_State;
	char m_strReason[1001];
	MTI_TIME m_Expires;
};


class CPluginStateWrapper
{
  public:
	CPluginStateWrapper();
	CPluginStateWrapper(PluginStateStruct pluginStateStruct);
	~CPluginStateWrapper(void);

    EPluginState State(void) const;
    void State(EPluginState psValue);

    string strReason(void) const;
    void strReason(const string &strValue);

    MTI_TIME Expires(void) const;
	void Expires(MTI_TIME et);

	PluginStateStruct &getWrappedPluginState();

  private:
//	EPluginState m_State;
//	string m_strReason;
//	MTI_TIME m_Expires;
	struct PluginStateStruct _pluginStateStruct;
};

//
//**************************CPlugin********************************************
//
//  A plugin is a DLL or Shared Library that exports named classes
//
//  This defines an incomplete plugin class.
//  To create a real plugin class, you muse define a method that
//  returns a new plugin
//    e.g.,  if CPluginTool is the plugin tool
//
//
//              CPluginTool *(*CreateTool(string Name))
//
//

class CPlugin
  {
    public:
      // Error reporting
      int ErrorNo;                      // Error Number, 0 is no error
      string ErrorMessage;              // Ascii Error Message

      // User Properties
      int Index;                        // Just an index into a plugin list
      void *Data;                       // User data, not owned or defined by
                                        // the plugin

      CPlugin(string Name);             // Create the plugin
      ~CPlugin(void);                   // Destroy the plugin

      string FileName(void) {return(qPluginFileName);};
      string PluginName(void);

	  // Exported functions
	  bool (*MTI_PluginVersion)(int V[4]);
      void *(*Create)(string FileName); // Returns a pointer to the plugin name
      void *(*Resource)(int n);         // Pointer to resource, e.g, icon etc.

      // These do not have to exist
      int  (*SendTheMessage)(int, void *);           // Message passing info
	  bool (*Properties)(void);
      bool (*ReadProperties)(string, string );    // For reading/writting configured properties
      bool (*WriteProperties)(string, string);

//      CPluginState CurrentState(void);           // Get the current state
	  PluginStateStruct (*CurrentState)(void);       // Get the current state


    private:
      void *qHandle;                     // Plugin Handle
      string qPluginFileName;
      virtual bool BindAddresses(void);   // Links the addresses

      // If not defined, just use null
      static bool NullProperties(void) {return(true);};
      static bool NullRWProperties(string, string) {return(true);};
      static int NullMessage(int, void *) {return(0);};
	  static PluginStateStruct NullCurrentState(void);
  };

//
// CPlugin list
//  This is just a list of all plugins, with methods for creating and adding
// plugins to the list.
//

class CPluginList : public vector<CPlugin *>
  {
    public:
     CPluginList(string Mask);             // Create the plugin
     ~CPluginList(void);                   // Destroy the plugin

// The following macro defines
//     bool LoadPlugins(string Mask);
     DEFINE_FILE_SWEEPER(LoadPlugins, LoadPlugin, false, CPluginList);

    private:
     bool LoadPlugin(string Name);
  };

#endif
