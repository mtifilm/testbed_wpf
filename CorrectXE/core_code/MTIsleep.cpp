/*
	A POSIX compliant sleep routines.

	Based on nanosleep ().

	Functions:
	MTIsleep (double dArg), where dArg is the number of seconds
	MTInanosleep (int iArg), where iArg is the number of nanoseconds
	MTImicrosleep (int iArg), where iArg is the number of microseconds
	MTImillisleep (int iArg), where iArg is the number of milliseconds
*/
#include "machine.h"
#include "MTIsleep.h"

//-----SGI Versions-------------------------------------------------------------
#if defined( __sgi)|| defined(__linux)
#include <time.h>

void MTIsleep (double dArg)
{
  long lSeconds, lNanoSeconds;
  struct timespec ts;

  lSeconds = (int)dArg;
  lNanoSeconds = (long)((dArg - (double)lSeconds) * 1000000000.);

  ts.tv_sec = lSeconds;
  ts.tv_nsec = lNanoSeconds;

  nanosleep (&ts, NULL);
}

void MTInanosleep (int iArg)
{
  struct timespec ts;

  ts.tv_sec = 0;
  ts.tv_nsec = iArg;

  nanosleep (&ts, NULL);
}

void MTImicrosleep (int iArg)
{
  struct timespec ts;

  ts.tv_sec = 0;
  ts.tv_nsec = iArg * 1000;

  nanosleep (&ts, NULL);
}

void MTImillisleep (int iArg)
{
  struct timespec ts;

  ts.tv_sec = 0;
  ts.tv_nsec = iArg * 1000000;

  nanosleep (&ts, NULL);
}

//-----WINDOWS Versions---------------------------------------------------------
#elif defined(_WIN32)
#include <windows.h>

void MTIsleep (double dArg)
{
  int millisecs = (int)(dArg * 1000.);
  Sleep(millisecs);
}

void MTInanosleep (int iArg)
{
  int millisecs = iArg / 1000000;
  Sleep(millisecs);
}

void MTImicrosleep (int iArg)
{
  int millisecs = iArg / 1000;
  Sleep(millisecs);
}

void MTImillisleep (int iArg)
{
   Sleep(iArg);
}
#endif

