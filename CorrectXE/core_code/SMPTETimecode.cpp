// SMPTETimecode.cpp:  implementation of the CSMPTETimecode class, which
//                     holds a representation of SMPTE 12M standard timecodes
//
// Created by: John Starr, March 21, 2003
//
/* CVS Info:
$Header: /usr/local/filmroot/core_code/source/SMPTETimecode.cpp,v 1.8 2006/03/27 02:53:50 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "SMPTETimecode.h"
#include "BCDLib.h"

//////////////////////////////////////////////////////////////////////
//  Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSMPTETimecode::CSMPTETimecode()
 : m_timecode(0), m_userData(0)
{

}

CSMPTETimecode::~CSMPTETimecode()
{

}

//////////////////////////////////////////////////////////////////////
//  Accessors
//////////////////////////////////////////////////////////////////////

MTI_UINT32 CSMPTETimecode::getRawTimecode() const
{
   return m_timecode;
}

MTI_UINT32 CSMPTETimecode::getRawUserData() const
{
   return m_userData;
}

CTimecode CSMPTETimecode::getTimecode(int frameRate) const
{
   int hours, minutes, seconds, frames;

   if (!isKnownFrameRate(frameRate))
      return CTimecode(0);     // bad frame rate argument, return bogus TC

   if (isDummyValue())
      return CTimecode(0);

   // Mask off SMPTE timecode flag bits
   MTI_UINT32 tmpBCDTC = m_timecode & TC_TIME_MASK;

   // C pointer trickery to get at hours, minutes, seconds and frames
   // in individual bytes in endian-independent way
   MTI_UINT8 *tcBCDDigits = (MTI_UINT8*)(&tmpBCDTC);

   // Convert BCD digits to integers for hours, minutes, seconds and frames
   frames = BCDToInt(tcBCDDigits[0]);
   seconds = BCDToInt(tcBCDDigits[1]);
   minutes = BCDToInt(tcBCDDigits[2]);
   hours = BCDToInt(tcBCDDigits[3]);

   // Flags for CTimecode
   int tcFlags = (isDropFrame()) ? DROPFRAME : 0;

   return CTimecode(hours, minutes, seconds, frames, tcFlags, frameRate);
}

bool CSMPTETimecode::isDummyValue() const
{
   return (m_timecode == TC_DUMMY_VALUE);
}

bool CSMPTETimecode::isDropFrame() const
{
   return (m_timecode & TC_DROP_FRAME_MASK);
}

bool CSMPTETimecode::isKnownFrameRate(int frameRate) const
{
	return (frameRate == 30 || frameRate == 25 || frameRate == 24 || frameRate == 50 || frameRate == 60);
}


//////////////////////////////////////////////////////////////////////
//  Mutators
//////////////////////////////////////////////////////////////////////

void CSMPTETimecode::setToDummyValue()
{
   m_timecode = TC_DUMMY_VALUE;
   m_userData = TC_DUMMY_VALUE;
}

void CSMPTETimecode::setRawTimecode(MTI_UINT32 newTimecode)
{
   m_timecode = newTimecode;
}

void CSMPTETimecode::setRawUserData(MTI_UINT32 newUserData)
{
   m_userData = newUserData;
}

void CSMPTETimecode::setTimecode(const CTimecode &newTimecode)
{
   // Set this CSMPTETimecode's m_timecode from a CTimecode
   // Only the Drop Frame flag is set in the raw timecode, the
   // other flags are not set
   // Does not modify the m_userData

   // Temporary BCD timecode with flags
   MTI_UINT32 tmpBCDTC = 0;

   // C pointer trickery to get at hours, minutes, seconds and frames
   // in individual bytes in endian-independent way
   MTI_UINT8 *tcBCDDigits = (MTI_UINT8*)(&tmpBCDTC);

   // Convert CTimecode's hours, minutes, seconds & frames to BCD digits
   tcBCDDigits[0] = IntToBCD(newTimecode.getFrames());
   tcBCDDigits[1] = IntToBCD(newTimecode.getSeconds());
   tcBCDDigits[2] = IntToBCD(newTimecode.getMinutes());
   tcBCDDigits[3] = IntToBCD(newTimecode.getHours());

   // Set flag bits
   tmpBCDTC &= TC_TIME_MASK;            // Mask off flag bits
	if (newTimecode.isDropFrame())
      tmpBCDTC |= TC_DROP_FRAME_MASK;   // Set drop frame flag

   setRawTimecode(tmpBCDTC);
}

//////////////////////////////////////////////////////////////////////
//  Increment and Decrement Operators
//////////////////////////////////////////////////////////////////////

void CSMPTETimecode::increment(int frameRate)
{
   // Increment frames by one, carry to seconds, minutes and hours as necessary,
   // adjust for Drop-Frame as necessary

   if (isDummyValue())
      return;

   CTimecode tc = getTimecode(frameRate);
   ++tc;
   setTimecode(tc);
}

void CSMPTETimecode::decrement(int frameRate)
{
   // Decrement frames by one, carry to seconds, minutes and hours as necessary,
   // adjust for Drop-Frame as necessary

   if (isDummyValue())
      return;

   CTimecode tc = getTimecode(frameRate);
   --tc;
   setTimecode(tc);
}

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

void CSMPTETimecode::addFrameOffset(int frameOffset, int frameRate)
{
   // Add frameOffset to frames, carry to seconds, minutes and hours as
   // necessary, adjust for Drop-Frame as necessary.  The argument
   // frameOffset may be positive or negative

   if (isDummyValue())
      return;

   setTimecode(getTimecode(frameRate) + frameOffset);
}

//////////////////////////////////////////////////////////////////////
//  Comparison Operators
//////////////////////////////////////////////////////////////////////

bool CSMPTETimecode::isEqualTo(const CSMPTETimecode &rhs, bool compareUserData) const
{
   if ( (m_timecode&TC_TIME_MASK) != (rhs.m_timecode&TC_TIME_MASK))
      return false;

   if (compareUserData && m_userData != rhs.m_userData)
      return false;

   return true;
}

