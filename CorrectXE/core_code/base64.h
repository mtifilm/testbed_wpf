#ifndef Base64H
#define Base64H
#include <string>
#include "machine.h"
#include "corecodelibint.h"

MTI_CORECODELIB_API std::string base64_encode(unsigned char const* , unsigned int len);
MTI_CORECODELIB_API std::string base64_decode(std::string const& s);

#endif