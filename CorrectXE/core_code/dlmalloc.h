//
//  This is a temporary header file to use alternative malloc
//  To macros are defined
//      dlmalloc  and dlfree
//  These just are malloc or free for non windows
//  otherwise they call the dlmalloc function in dlmalloc
//
//
#ifndef DLMALLOC_H
#define DLMALLOC_H

#define __IN_MTI_MALLOC__
#include "machine.h"
#undef __IN_MTI_MALLOC__

//
//  On windows, just define the mallocs
#ifdef _WINDOWS
extern "C" {
void *dlmalloc(size_t n);
void *dlcalloc(size_t n_elements, size_t element_size);
void dlfree(void *);
void *dlrealloc(void *p, size_t n);
void *dlmemalign(size_t alignment, size_t n);
void *dlvalloc(size_t n);
}
#else

// Just make them the same
#define dlmalloc malloc
#define dlcalloc calloc
#define dlfree free
#define dlrealloc realloc
#define dlmemalign memalign
#define dlvalloc valloc
#endif

#endif
