// File: XMLWriter.cpp
//
//  Implementation of CXMLStream class
//
// ---------------------------------------------------------------------------

#include "XMLWriter.h"
#include "FileSweeper.h"

//////////////////////////////////////////////////////////////////////////////

CXMLStream::CXMLStream(std::ostream& _s)
 : s(_s), state(stateNone), prologWritten(false)
{
   // XMLStream refers std::ostream object to perform actual output operations
}

CXMLStream::~CXMLStream()
{
   // Before destroying check whether all the open tags are closed
   if (stateTagName == state)
      {
      s << "/>";
      state = stateNone;
      }

   while (tags.size())
      endTag(tags.top());
}

//////////////////////////////////////////////////////////////////////////////

CXMLStream& CXMLStream::operator<<(const SController& controller)
{
   switch (controller.what)
      {
      case SController::whatProlog:
         if (!prologWritten && stateNone == state)
            {
            s << "<?xml version=\"1.0\"?>" << std::endl;
            prologWritten = true;
            }
         break;   //   Controller::whatProlog

      case SController::whatTag:
         closeTagStart();
         s << std::endl << '<';
         if (controller.str.empty())
            {
            clearTagName();
            state = stateTagName;
            }
         else
            {
            s << controller.str;
            tags.push(controller.str);
            state = stateTag;
            }
         break;   //   Controller::whatTag

      case SController::whatTagEnd:
         endTag(controller.str);
         break;   // Controller::whatTagEnd

      case SController::whatAttribute:
         switch (state)
            {
            case stateTagName:
               tags.push(tagName.str());
               break;

			case stateAttribute:
			   s << '\"';
			   break;

			default:
				break;
            }

         if (stateNone != state)
            {
            s << ' ' << controller.str << "=\"";
            state = stateAttribute;
            }
         // else throw some error - unexpected attribute (out of any tag)

         break;   //   Controller::whatAttribute

      case SController::whatCharData:
         closeTagStart();
         state = stateNone;
         if (isspace(controller.str[0]) || isspace(controller.str[controller.str.length() - 1]))
         {
            auto trimmedCdata = StringTrim(controller.str);
            if (trimmedCdata.empty() == false)
            {
               s << "<![CDATA[" << trimmedCdata << "]]>";
            }
         }
         else
         {
            s << "<![CDATA[" << controller.str << "]]>";
         }

         break;   //   Controller::whatCharData

      case SController::whatEscape:
         // Escape the entity references & < > ' "
         for (std::string::const_iterator iter = controller.str.begin();
              iter != controller.str.end(); ++iter)
            {
            switch (*iter)
               {
               case '&':
                  s << "&amp;";
                  break;
               case '<':
                  s << "&lt;";
                  break;
               case '>':
                  s << "&gt;";
                  break;
               case '\'':
                  s << "&apos;";
                  break;
               case '\"':
                  s << "&quot";
                  break;
               default:
                  s << *iter;
                  break;
               }
            }
      }

   return *this;
}

//////////////////////////////////////////////////////////////////////////////

void CXMLStream::closeTagStart(bool self_closed)
{
   if (stateTagName == state)
      tags.push(tagName.str());

   // note: absence of 'break's is not an error
   switch (state)
      {
      case stateAttribute:
         s << '\"';

      case stateTagName:
      case stateTag:
         if (self_closed)
            s << '/';
		 s << '>';
		 break;

	  default:
	  	break;
      }
}

void CXMLStream::endTag(const std::string& tag)
{
   bool brk = false;

   while (tags.size() > 0 && !brk)
      {
      if (stateNone == state)
         s << std::endl << "</" << tags.top() << '>';
      else
         {
         closeTagStart(true);
         state = stateNone;
         }
      brk = tag.empty() || tag == tags.top();
      tags.pop();
      }
}

