#ifndef __MTI_STRING_STREAM_H__
#define __MTI_STRING_STREAM_H__

#ifdef _WIN64  // Rad Studio bug in 64-bits only (string strams not thread-safe)
#include <iostream>
#include <string>
#include <sstream>
#include "corecodelibint.h"
#include "ThreadLocker.h"


class MTI_CORECODELIB_API MTIstringstream
{
   static CSystemThreadLock *Lock;

protected:

   MTIstringstream();
   virtual ~MTIstringstream();

public:

   inline static CSystemThreadLock &GetLock() { return *Lock; }
};

class MTI_CORECODELIB_API MTIostringstream : MTIstringstream
{

   std::ostringstream *wrappedStream;

public:

   MTIostringstream();
   MTIostringstream(const string &s);
   ~MTIostringstream();

   inline string str() const
   {
      CAutoSystemThreadLocker lock(GetLock());
      return wrappedStream->str();
   }

   inline void str(const string &s)
   {
      CAutoSystemThreadLocker lock(GetLock());
      wrappedStream->str(s);
   }

   inline void clear(std::ios::iostate state = std::ios::goodbit)
   {
      CAutoSystemThreadLocker lock(GetLock());
      wrappedStream->clear(state);
   }

   inline bool fail()
   {
      CAutoSystemThreadLocker lock(GetLock());
      return wrappedStream->fail();
   }

   inline bool eof()
   {
      CAutoSystemThreadLocker lock(GetLock());
      return wrappedStream->eof();
   }

   inline std::ios::fmtflags flags() const
   {
      CAutoSystemThreadLocker lock(GetLock());
      return wrappedStream->flags();
   }

   inline std::ios::fmtflags flags (std::ios::fmtflags fmtfl)
   {
      CAutoSystemThreadLocker lock(GetLock());
      return wrappedStream->flags(fmtfl);
   }

   inline std::ios::fmtflags setf(std::ios::fmtflags fmtfl)
   {
      CAutoSystemThreadLocker lock(GetLock());
      return wrappedStream->setf(fmtfl);
   }

   inline std::ios::fmtflags setf(std::ios::fmtflags fmtfl, std::ios::fmtflags mask)
   {
      CAutoSystemThreadLocker lock(GetLock());
      return wrappedStream->setf(fmtfl, mask);
   }

   inline void unsetf(std::ios::fmtflags mask)
   {
      CAutoSystemThreadLocker lock(GetLock());
      wrappedStream->unsetf(mask);
   }

   // CALLER MUST LOCK MTIstringstream::Lock while accessing naked ostringstream!
   // This is a C++11 conversion operator. Not sure how to do it in older C++!
   //// DOESN'T COMPILE:
   ////[bcc64 Error] MTIstringstream.h(66): call to implicitly-deleted copy constructor of 'ostringstream'
   ////              (aka 'basic_ostringstream<char, char_traits<char>, allocator<char> >')
   ////              sstream(549): copy constructor is implicitly deleted because
   ////              'basic_ostringstream<char, std::char_traits<char>, std::allocator<char> >'
   ////               has a user-declared move constructor
//   operator ostringstream() const
//   {
//      return *wrappedStream;
//   }

#define OPERATOR_LTLT(rhsType)                    \
   MTIostringstream& operator<<(rhsType rhs)      \
   {                                              \
      CAutoSystemThreadLocker lock(GetLock());            \
      *wrappedStream << rhs;                      \
      return *this;                               \
   }

   OPERATOR_LTLT(const char *);
   OPERATOR_LTLT(const char &);
   OPERATOR_LTLT(const signed char *);
   OPERATOR_LTLT(const signed char &);
   OPERATOR_LTLT(const unsigned char *);
   OPERATOR_LTLT(const unsigned char &);
   OPERATOR_LTLT(const wchar_t *);
   OPERATOR_LTLT(const wchar_t &);
   OPERATOR_LTLT(const string);
   OPERATOR_LTLT(int);
   OPERATOR_LTLT(const int *);
   ////OPERATOR_LTLT(const int &);
   OPERATOR_LTLT(const unsigned int &);
   OPERATOR_LTLT(const unsigned int *);
   OPERATOR_LTLT(const double &);
   OPERATOR_LTLT(const double *);
   OPERATOR_LTLT(const short &);
   OPERATOR_LTLT(const short *);
   OPERATOR_LTLT(const unsigned short &);
   OPERATOR_LTLT(const unsigned short *);
   OPERATOR_LTLT(const long &);
   OPERATOR_LTLT(const long *);
   OPERATOR_LTLT(const unsigned long &);
   OPERATOR_LTLT(const unsigned long *);
   OPERATOR_LTLT(const long long &);
   OPERATOR_LTLT(const long long *);
   OPERATOR_LTLT(const unsigned long long &);
   OPERATOR_LTLT(const unsigned long long *);
   OPERATOR_LTLT(std::_Smanip<std::streamsize>);                    // e.g. setw()
   OPERATOR_LTLT(std::_Smanip<int>);                           // e.g. setbase(16)
   OPERATOR_LTLT(std::_Fillobj<char>);                         // e.g. setfill()

   typedef std::basic_ostream<char, std::char_traits<char>>& (*StandardEndLine)(std::basic_ostream<char, std::char_traits<char>>&);
   OPERATOR_LTLT(StandardEndLine);                        // endl

   typedef std::ios_base &(*StandardHex)(std::ios_base &);
   OPERATOR_LTLT(StandardHex);                            // hex
};

class MTI_CORECODELIB_API MTIistringstream : MTIstringstream
{

   std::istringstream *wrappedStream;

public:

   MTIistringstream();
   MTIistringstream(const string &s);
   ~MTIistringstream();

   inline string str() const
   {
      CAutoSystemThreadLocker lock(GetLock());
      return wrappedStream->str();
   }

   inline void str(const string &s)
   {
      CAutoSystemThreadLocker lock(GetLock());
      wrappedStream->str(s);
   }

   inline int peek()
   {
      CAutoSystemThreadLocker lock(GetLock());
      return wrappedStream->peek();
   }

   inline void clear(std::ios::iostate state = std::ios::goodbit)
   {
      CAutoSystemThreadLocker lock(GetLock());
      wrappedStream->clear(state);
   }

   inline std::ios::fmtflags flags() const
   {
      CAutoSystemThreadLocker lock(GetLock());
      return wrappedStream->flags();
   }

   inline bool fail()
   {
      CAutoSystemThreadLocker lock(GetLock());
      return wrappedStream->fail();
   }

   inline bool eof()
   {
      CAutoSystemThreadLocker lock(GetLock());
      return wrappedStream->eof();
   }

   inline std::ios::fmtflags flags(std::ios::fmtflags fmtfl)
   {
      CAutoSystemThreadLocker lock(GetLock());
      return wrappedStream->flags(fmtfl);
   }

   inline std::ios::fmtflags setf(std::ios::fmtflags fmtfl)
   {
      CAutoSystemThreadLocker lock(GetLock());
      return wrappedStream->setf(fmtfl);
   }

   inline std::ios::fmtflags setf(std::ios::fmtflags fmtfl, std::ios::fmtflags mask)
   {
      CAutoSystemThreadLocker lock(GetLock());
      return wrappedStream->setf(fmtfl, mask);
   }

   inline void unsetf(std::ios::fmtflags mask)
   {
      CAutoSystemThreadLocker lock(GetLock());
      wrappedStream->unsetf(mask);
   }

   inline MTIistringstream& ignore(std::streamsize n = 1, int delim = EOF)
   {
      CAutoSystemThreadLocker lock(GetLock());
      wrappedStream->ignore(n, delim);
      return *this;
   }

   // This is a C++11 conversion operator. Not sure how to do it in older C++!
   explicit operator bool() const
   {
      return !wrappedStream->fail();
   }

#define OPERATOR_GTGT(rhsType)                    \
   MTIistringstream& operator>>(rhsType rhs)      \
   {                                              \
	  CAutoSystemThreadLocker lock(GetLock());            \
      *wrappedStream >> rhs;                      \
      return *this;                               \
   }

   OPERATOR_GTGT(char *);
   OPERATOR_GTGT(char &);
   OPERATOR_GTGT(signed char *);
   OPERATOR_GTGT(signed char &);
   OPERATOR_GTGT(unsigned char *);
   OPERATOR_GTGT(unsigned char &);
   OPERATOR_GTGT(string);
   OPERATOR_GTGT(int &);
   OPERATOR_GTGT(unsigned int &);
   OPERATOR_GTGT(double &);
   OPERATOR_GTGT(short &);
   OPERATOR_GTGT(unsigned short &);
   OPERATOR_GTGT(long &);
   OPERATOR_GTGT(unsigned long &);
   OPERATOR_GTGT(long long &);
   OPERATOR_GTGT(unsigned long long &);
};

#else
#include <iostream>
#include <string>
#include <sstream>
using std::stringstream;
using std::ostringstream;
using std::istringstream;
#define MTIstringstream  stringstream;
#define MTIostringstream ostringstream
#define MTIistringstream istringstream
#endif

#endif // __MTI_STRING_STREAM__

