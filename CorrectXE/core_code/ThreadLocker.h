/*
   File Name:  ThreadLocker.h
   Created: Aug 1, 2003 by John Mertus
   Version 1.00

   This is the locking class


*/
#ifndef THEADLOCKERH
#define THEADLOCKERH

#include "corecodelibint.h"
#include <string>
#include <vector>
#include <queue>
#include <stdio.h>
#include "machine.h"
#include "err_defs.h"
#include "PropX.h"

#ifndef ROLL_OUR_OWN_SPIN_LOCK
#define ROLL_OUR_OWN_SPIN_LOCK 0
#endif

////xxxx MTI_CORECODELIB_API void CoreCodePopupErrorMessage(const string &message);

//////////////////////////////////////////////////////////////////////
// ThreadLock is a quick and dirty way of using mutexes to make access
// to multiple thread safe for a class.
class MTI_CORECODELIB_API CThreadLock
{
  public:

  CThreadLock(void);
  ~CThreadLock(void);

  bool Lock(const char *msg);  // 'msg' is ignored - left over from UNIX days
  bool TryToLock();
  bool UnLock(const char *msg=NULL);  // 'msg' is gnored
  int LetsTryToLock();

  private:

  CRITICAL_SECTION m_CriticalSection;
};

// AutoThreadLocker locks the ThreadLock for a given scope when you
// create it on the stack. E.g.:
//
//	static CThreadLock fooLock;
//
//	if (I_need_to_do_something_safely) {
//		CAutoThreadLocker fooLocker(fooLock);
//		...
//	}
// fooLock is automatically unlocked when you fall out of scope.

class CAutoThreadLocker {
public:

  CAutoThreadLocker(CThreadLock& lock, const char* msg = 0)
    : mLock	(&lock), mMsg(msg)
    {
      mLock->Lock(mMsg);
    }
  ~CAutoThreadLocker()
    {
      mLock->UnLock(mMsg);
    }
private:
  CAutoThreadLocker(const CAutoThreadLocker&) = delete;
  CAutoThreadLocker& operator = (const CAutoThreadLocker&) = delete;
  CThreadLock*	mLock;
  const char*	mMsg;
};


// AutoThreadUnlocker unlocks the ThreadLock for a given scope when you
// create it on the stack. E.g.:
//
//	static CThreadLock fooLock;
//
//	if (I_need_to_release_the_lock_for_a_short_time) {
//		CAutoThreadUnlocker fooUnlocker(fooLock);
//		...
//	}
// fooLock is automatically re-locked when you fall out of scope.

class CAutoThreadUnlocker {
public:

  CAutoThreadUnlocker(CThreadLock& lock, const char* msg = 0)
    : mLock	(&lock), mMsg(msg)
    {
      mLock->UnLock(mMsg);
    }
  ~CAutoThreadUnlocker()
    {
      mLock->Lock(mMsg);
    }
private:
  CThreadLock*	mLock;
  const char*	mMsg;
};

//////////////////////////////////////////////////////////////////////
// SystemThreadLock is is like a regular thread lock, but wraps a global
// system mutex so it can be shared by name among modules
class MTI_CORECODELIB_API CSystemThreadLock
{
  public:
	bool Lock();
	bool UnLock();

    CSystemThreadLock(const string &str);
    ~CSystemThreadLock(void);

    void *GetMutex(void) { return(&m_Mutex); };

  private:
    string m_sMutexName;
//
// Normally this would be handled with a cheshire class
   HANDLE m_Mutex;              // Window objects are handles
};

// AutoSystemThreadLocker locks the SystemThreadLock for a given scope when you
// create it on the stack. E.g.:
//
//	static CSystemThreadLock fooLock;
//
//	if (I_need_to_do_something_safely) {
//		CAutoSystemThreadLocker fooLocker(fooLock);
//		...
//	}
// fooLock is automatically unlocked when you fall out of scope.

class CAutoSystemThreadLocker {
public:

  CAutoSystemThreadLocker(CSystemThreadLock& lock)
	: mLock	(&lock)
    {
	  mLock->Lock();
    }
  ~CAutoSystemThreadLocker()
    {
	  mLock->UnLock();
    }
private:
  CAutoSystemThreadLocker(const CAutoSystemThreadLocker&) = delete;
  CAutoSystemThreadLocker& operator = (const CAutoSystemThreadLocker&) = delete;
  CSystemThreadLock*	mLock;
};


// AutoSystemThreadUnlocker unlocks the SystemThreadLock for a given scope when you
// create it on the stack. E.g.:
//
//	static CSystemThreadLock fooLock;
//
//	if (I_need_to_release_the_lock_for_a_short_time) {
//		CAutoSystemThreadUnlocker fooUnlocker(fooLock);
//		...
//	}
// fooLock is automatically re-locked when you fall out of scope.

class CAutoSystemThreadUnlocker {
public:

  CAutoSystemThreadUnlocker(CSystemThreadLock& lock)
	: mLock	(&lock)
    {
	  mLock->UnLock();
    }
  ~CAutoSystemThreadUnlocker()
    {
	  mLock->Lock();
    }
private:
  CAutoSystemThreadUnlocker(const CAutoSystemThreadUnlocker&) = delete;
  CAutoSystemThreadUnlocker& operator = (const CAutoSystemThreadUnlocker&) = delete;
  CSystemThreadLock*	mLock;
};


///////////////////////////////////////////////////////////////////////////////
// Static thread locker, using interlocked instructions with spin waits
// instead of Critical Sections
//
// CAutoSpinLocker locks the InterLock for a given scope when you
// create it on the stack. CAutoSpinUnlocker unlocks the Interlock for
// a given scope when you create it on the stack. E.g.:
//
//	static int fooLock = 0;
//
//	if (I_need_to_do_something_safely) {
//	    CAutoSpinLocker fooLocker(fooLock);
//	    ...
//          if (I_want_to_unlock_for_a_short_time) {
//              CAutoSpinUnlocker(fooLock);
//              ...
//          }
//          // foolock is automatically re-locked here
//	}
//      // fooLock is automatically unlocked when you fall out of scope.

class MTI_CORECODELIB_API CSpinLock
{

#if ROLL_OUR_OWN_SPIN_LOCK

   volatile LONG mLock;
   volatile LONG mNesting;

public:

   CSpinLock();

   inline void lock()
   {
      // Assuming the thread ID can't be 0!
      DWORD threadID = _threadid;

      // We own the lock if it contains our thread ID
      while (mLock != threadID)
      {
         // If mLock is 0, atomically set it to threadID;
         // returns old value of mLock. If the exchange worked,
         // then we just acquired the lock
         InterlockedCompareExchange(&mLock, threadID, 0);
         if (mLock != threadID)
         {
            Sleep(0);  // end the time slice early to yield the CPU
         }
      }

      // Keep track of nested calls - we require an unlock per lock
      ++mNesting;
   }

   inline void unlock()
   {
      DWORD threadID = _threadid;
      if (mLock != threadID)
      {
         return;
      }

      if (--mNesting < 0)
      {
         mNesting = 0;
      }

      if (mNesting == 0)
      {
         mLock = 0;
      }
   }

#else  // Use spinning critical section

private:

  CRITICAL_SECTION m_CriticalSection;

public:

  CSpinLock(void);
  ~CSpinLock(void);

  inline void lock()
  {
	  EnterCriticalSection(&m_CriticalSection);
  }

  inline void unlock()
  {
    try
    {
		LeaveCriticalSection(&m_CriticalSection);
    }
    catch (...)
    {
       // CoreCodePopupErrorMessage("EXCEPTION: from LeaveCriticalSection() in CSpinLock!!");
    }
  }

#endif
};

class CAutoSpinLocker
{
  CSpinLock *mSpinLock;
  CAutoSpinLocker(const CAutoSpinLocker&) = delete;
  CAutoSpinLocker& operator = (const CAutoSpinLocker&) = delete;

public:

  CAutoSpinLocker(CSpinLock& spinLock)
    : mSpinLock (&spinLock)
    {
       mSpinLock->lock();
    }

  ~CAutoSpinLocker()
    {
      mSpinLock->unlock();
    }

};

class CAutoSpinUnlocker
{
  CSpinLock *mSpinLock;
  CAutoSpinUnlocker(const CAutoSpinUnlocker&) = delete;
  CAutoSpinUnlocker& operator = (const CAutoSpinUnlocker&) = delete;

public:

  CAutoSpinUnlocker(CSpinLock& spinLock)
    : mSpinLock (&spinLock)
    {
       mSpinLock->unlock();
    }

  ~CAutoSpinUnlocker()
    {
      mSpinLock->lock();
    }

};

#endif

