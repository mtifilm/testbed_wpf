/*
   File Name:  HistoryList.cpp
   Created: Sept 11, 2001 by John Mertus
   Version 1.00

   This is a small class that contains the names of the history files

*/
#include "HistoryList.h"
#include "IniFile.h"
#include "MTIstringstream.h"

#if defined(__GNUG__)  && (__GNUG__ < 3)
#include <stdexcept>
#endif

//-------------------Constructor--------------John Mertus-----Sept--01--

   CHistoryList::CHistoryList(int n)

//  Creates a history list, this is a list without repetions that
//  pushes the newest on top and pops the oldest off the bottom.
//  N is the maximum names to keep.
//
//*********************************************************************
{
    fiMaxSize = n;                   // Total names to keep
#ifdef _WINDOWS_
    fbCaseSensitive = false;         // Defaults differ for OS
#else
    fbCaseSensitive = true;
#endif
    cPoundSubstitute = '\0';         // No default substitution
    IniFileName = CPMPIniFileName();
    IniSection = "FileHistory";
}
//----------------------Clear------------------John Mertus-----Sept--01--

   void CHistoryList::Clear(void)

//  This just deletes all entries in the history list
//
//***********************************************************************
{
    clear();
    Notify();
}

//----------------------Add--------------------John Mertus-----Sept--01--

   void CHistoryList::Add(string str)

//  This adds an entry to the history file at the top.
//  Any duplicate name is deleted and the str pushed on the top
//
//***********************************************************************
{
   // Remove spaces from beginning
   InhibitCallbacks();
   str.erase(0,str.find_first_not_of(' '));
   Remove(str);
   insert(begin(),str);
   if ((int)size() > fiMaxSize) pop_back();
   Notify();
   UnInhibitCallbacks();
}

//----------------------Index------------------John Mertus-----Sept--01--

   int CHistoryList::Index(string str)

//  This returns the index of where str occures in the string
//  Return is the index number if the str occures, -1 otherwise
//
//***********************************************************************
{
    for (unsigned i = 0; i < size(); i++)
      if (Compare(str, at(i)))
         {
            return(i);
         }

    return(-1);
}

//----------------------Remove------------------John Mertus-----Sept--01--

   bool CHistoryList::Remove(string str)

//  This removes a string str from the list.
//  Return true if the string existed, false if it did not.
//
//***********************************************************************
{
    int indx = Index(str);
    if (indx < 0) return(false);

    Delete(indx);
    return(true);
}

//----------------------Delete------------------John Mertus-----Sept--01--

   bool CHistoryList::Delete(int n)

//  This removes the index i from the list
//  Return true if i is in range, false if it is not
//
//***********************************************************************
{
    if ((n < 0) || (n >= (int)size())) return(false);
    erase(begin() + n);
    Notify();
    return(true);
}

//---------------WriteProperties---------------John Mertus-----Sept--01--

   bool CHistoryList::WriteProperties(void)

//  This writes out the history list in the file specified by
//  IniFileName and Section IniSection
//
//  Return is true if the history is written out correctly
//
//***********************************************************************
{
   CIniFile *ini = CreateIniFile(IniFileName.c_str());
   if (ini == NULL) return(false);

   // Delete the section
   ini->EraseSection(IniSection);
   for (unsigned i = 0; i < size(); i++)
     {
       MTIostringstream os;
       string str = at(i);

       if (cPoundSubstitute != '\0')
         {
           for (unsigned j = 0; j < str.size(); j++)
             {
               if (str[j] == '#') str[j] = cPoundSubstitute;
             }
         }
       os << i;
       ini->WriteString(IniSection, os.str() , str);
     }

   return(DeleteIniFile(ini));
}

//---------------ReadProperties----------------John Mertus-----Sept--01--

   bool CHistoryList::ReadProperties(void)

//  This read out the history list in the file specified by
//  IniFileName and Section IniSection
//
//  Return is true if the history list is read in correctly
//
//***********************************************************************
{
   CIniFile *ini = CreateIniFile(IniFileName.c_str());
   if (ini == NULL) return(false);

   //
   // Stop all callbacks until completion
   InhibitCallbacks();
   Clear();     // Clear out any list

   // Read all the values
   StringList list;
   ini->ReadSection(IniSection, &list);

   //
   // Now read in reverse so they come out right
   for (int i = fiMaxSize-1; i >= 0; i--)
     {
        MTIostringstream os;
        os << i;
        string str = ini->ReadString(IniSection, os.str(), "");

        if (cPoundSubstitute != '\0')
          {
            for (unsigned j = 0; j < str.size(); j++)
              {
                if (str[j] == cPoundSubstitute) str[j] = '#';
              }
          }
        if (str != "") Add(str);
     }
   UnInhibitCallbacks();
   return(DeleteIniFile(ini));
}

//--------------------Compare------------------John Mertus-----Sept--01--

   bool CHistoryList::Compare(string S1, string S2)

//  This compares string S1 with S2.
//  if (fbCaseSensitive) then the compare is done with regard to case
//  Otherwise the compare is done ignoring case
//  return is true if equal, false if unequal
//
//***********************************************************************
{
   if (fbCaseSensitive) return(S1==S2);
   // Comparison function used to compare strings while ignoring case.
   return(EqualIgnoreCase(S1, S2));
}                                    

//------------------SetMaxSize----------------John Mertus-----Sept--01--

   void CHistoryList::SetMaxSize(int Value)

//  Just set the value for maximum to keep
//
//***********************************************************************
{
   if (Value < 0) return;
   fiMaxSize = Value;
}

//------------------GetMaxSize----------------John Mertus-----Sept--01--

   int CHistoryList::GetMaxSize(void)

//  Just return the value for maximum to keep
//
//***********************************************************************
{
   return(fiMaxSize);
}

//---------------SetCaseSensitivity------------John Mertus-----Sept--01--

   void CHistoryList::SetCaseSensitivity(bool Value)

//  Just set the value for the case sensitivity
//
//***********************************************************************
{
   fbCaseSensitive = Value;
}

//--------------GetCaseSensitivity-------------John Mertus-----Sept--01--

   bool CHistoryList::GetCaseSensitivity(void)

//  Return the current case sensitivity
//
//***********************************************************************
{
   return(fbCaseSensitive);
}

//--------------SetPoundSubstitute-------------Mike Braca-----July--03--

   void CHistoryList::SetPoundSubstitute(char c)

//  Set the  character to substituted for #'s in ini files
//  NOTE: the zero character ('\0') means don't substitute
//
//***********************************************************************
{
   cPoundSubstitute = c;
}

//--------------GetPoundSubstitute-------------Mike Braca-----July--03--

   char CHistoryList::GetPoundSubstitute(void)

//  Return the current character to substituted for #'s in ini files
//
//***********************************************************************
{
   return(cPoundSubstitute);
}


//--------------at-----------------------------Matt McClure-----Feb--02--
#if defined(__GNUG__)  && (__GNUG__ < 3)
// For some reason the GNU C++ Standard Library doesn't have
// vector::at() until GCC 3, which isn't standard on any Linux
// distribution yet.
vector<string>::reference CHistoryList::at(size_type n)
// Bounds-checked version of operator[]
{
    if (n >= this->size())
        throw(out_of_range("vector"));
    return (*this)[n];
}

vector<string>::const_reference CHistoryList::at(size_type n) const
// Bounds-checked version of operator[]
{
    if (n >= this->size())
        throw(out_of_range("vector"));
    return (*this)[n];
}
#endif
