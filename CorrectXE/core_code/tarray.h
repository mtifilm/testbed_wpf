#ifndef __TARRAY_H__
#define __TARRAY_H__

#include "MTIstringstream.h"
#include "IniFile.h"
#include <iostream>
#include <stdlib.h>
#include <math.h>
//#define NDEBUG // uncomment to remove checking of my_assert()
//#include <my_assert.h>
namespace {
  void my_assert(bool condition)
  {
#ifndef NDEBUG
////    if (!condition)
////      asm { int 3h }
#endif
  }
}
#define DEFAULT_ALLOC 2

#ifndef _TArrayH
#define _TArrayH


template <class ElType> class matrix;

template <class ElType>
class mvector
{
	friend class matrix<ElType>;
	 ElType * data;
	 int len;
public:
	 int length()const;
	 mvector();
	 mvector(int n);
	 mvector(int n, const ElType *values);
	 ~mvector(){ delete [] data;}
	 //Copy operator
	 mvector(const mvector<ElType> &v);
	 //assignment operator
	mvector<ElType>& operator =(const mvector<ElType> &original);
	ElType& operator[](int i)const  ;
	mvector<ElType> operator+(const mvector<ElType>& v) const;
	mvector<ElType> operator-(const mvector<ElType>&v) const;
	void  rprint()const;  //print entries on a single line
	void resize(int n);
   void zero(void);
   void fill(ElType value);
	int operator==(const mvector<ElType>& v) const;
	friend   mvector<ElType> operator*(ElType c,mvector<ElType>& v );
	friend   mvector<ElType> operator*(mvector<ElType>& v,ElType c );
   //friend ostream& operator<< <ElType> (ostream& s,mvector<ElType>& v);
};
template <class ElType>
void mvector<ElType>::zero()
{
	for(int i=0;i<len;i++) data[i]=(ElType)0;
}
template <class ElType>
void mvector<ElType>::fill(ElType value)
{
	for(int i=0;i<len;i++) data[i]=value;
}
template <class ElType>
int mvector<ElType>::length()const
{
	return len;
}
template <class ElType>
ElType& mvector<ElType>::operator[](int i)const
{
	my_assert(i>=0 && i < len);
	return data[i];
}

template <class ElType>
mvector<ElType>::mvector()
{
	data=new ElType[ DEFAULT_ALLOC];
	my_assert(data!=0);
	len=  DEFAULT_ALLOC;
}
template <class ElType>
mvector<ElType>::mvector(int n)
{
	data = new ElType[len=n];
	my_assert(data!=0);
}
template <class ElType>
mvector<ElType>::mvector(int n, const ElType *values)
{
	data = new ElType[len=n];
	my_assert(data!=0);
	for(int i=0;i<len;i++) data[i]=values[i];
}
template <class ElType>
mvector<ElType>::mvector(const mvector<ElType>& v)
{
	data=new ElType[len=v.len];
	my_assert(data!=0);
	for(int i=0;i<len;i++) data[i]=v.data[i];
}
template <class ElType>
mvector<ElType>& mvector<ElType>::operator =(const mvector<ElType> &original)
{
		if(this != &original)
		{
			delete [] data;
			data= new ElType[len=original.len];
			my_assert(data!=0);
			for(int i=0;i<len;i++) data[i]=original.data[i];
		}
		return *this;
}
template <class ElType>
mvector<ElType> mvector<ElType>::operator+(const mvector<ElType>& v) const
{
	mvector<ElType> sum(len);
	for(int i=0;i<len;i++) sum[i] = data[i]+v.data[i];
	return sum;
}
template <class ElType>
mvector<ElType> mvector<ElType>::operator-(const mvector<ElType>& v) const
{
		mvector<ElType> sum(len);
		for(int i=0;i<len;i++) sum[i] = data[i]-v.data[i];
		return sum;
}
template <class ElType>
mvector<ElType> vabs(const mvector<ElType> &v)
{
		mvector<ElType> absvec(v.length());
		for(int i = 0; i < v.length(); i++)
      {
         absvec[i] = (v[i] < ElType(0)) ? -v[i] : v[i];
      }

		return absvec;
}
template <class ElType>
void  mvector<ElType>::rprint()const  //print entries on a single line
{
		int i;
		cout << "mvector: ";
		cout << "(";
		for(i=0;i<len-1;i++) cout << data[i] << ",";
		cout << data[len-1] << ")" << endl;
		return;
}
template <class ElType>
void mvector<ElType>::resize(int n)
{
		delete[]data;
		data = new ElType[len=n];
		my_assert(data !=0);
}
template <class ElType>
int mvector<ElType>::operator==(const mvector<ElType>& v) const
{
		if(len != v.len) return 0;
		for(int i=0;i<len;i++) if(data[i]!=v.data[i]) return 0;
		return 1;
}
template <class ElType>
mvector<ElType> operator*(ElType c,mvector<ElType>& v )
{
		mvector<ElType> ans(v.len);
		for(int i=0;i<v.len;i++) ans[i]=c*v[i];
		return ans;
}
template <class ElType>
mvector<ElType> operator*(mvector<ElType>& v,ElType c )
	{
		mvector<ElType> ans(v.len);
		for(int i=0;i<v.len;i++) ans[i]=c*v[i];
		return ans;
	}
template <class ElType>
ostream& operator<<(ostream& s, const mvector<ElType>& v)
{
   s << "(";
	for(int i = 0; i < v.length() - 1; i++)
      s << v[i] << ", ";
	s << v[v.length() - 1] << ")" << endl;
	return s;
}
template <class ElType>
MTIostringstream& operator<< (MTIostringstream& s, mvector<ElType>& v)
{
	  s << "(";
	for(int i=0;i<v.length()-1;i++) s << v[i] << ", ";
	s << v[v.length()-1]<<")"<<endl;
	return s;
}

template <class ElType>
class matrix
{
	mvector<ElType> *m;

	public:
	int rows,cols;
	matrix();
	matrix(int r, int c);
	matrix(const mvector<ElType> &v);
	matrix(const matrix<ElType> &s);
	matrix(const matrix<ElType> &s, int r);
	~matrix();
	matrix& operator =(const matrix<ElType>& s);
	mvector<ElType>& operator[](const int i) const;
	mvector<ElType> operator*(const mvector<ElType>&v) const;
	friend matrix<ElType> operator*(ElType c, const matrix<ElType>&a);
	friend matrix<ElType> operator*(const matrix<ElType>&a, ElType c);
	matrix<ElType> operator*(const matrix<ElType>& a) const;
	matrix<ElType> operator+(const matrix<ElType>& a) const;
	matrix<ElType> operator-(const matrix<ElType>& a) const;
	matrix<ElType> transpose() const;
   matrix<ElType> mldivide(const matrix<ElType> &rhs) const;
	//matrix<ElType> invert() const;
   void zero();
   void fill(ElType value);
   void setDiag(mvector<ElType> v);
	//friend ostream& operator<<(ostream& s,matrix<ElType>& m);
	friend void ludcmp(matrix<ElType>& a,mvector<int>& indx,double &d);
	friend void lubksb(matrix<ElType>&a,mvector<int>& indx,mvector<ElType>&b);
};
template <class ElType>
matrix<ElType>::matrix()
{
	m = new mvector<ElType>[DEFAULT_ALLOC];
	my_assert(m !=0);
	rows=cols=DEFAULT_ALLOC;
	for(int i=0;i<rows;i++)
	{
		mvector<ElType> v;
		m[i]= v;
	}
}

template <class ElType>
matrix<ElType>::matrix(int r, int c)
{
	m= new mvector<ElType>[r];
	my_assert(m != 0);
	rows=r;
	cols=c;
	for(int i=0;i<r;i++)
	{
		mvector<ElType> v(cols);
		m[i]=v;
	}
}
template <class ElType>
matrix<ElType>::matrix(const mvector<ElType> &v)
{
	int i;
	rows=1;
	cols = v.length();
	m = new mvector<ElType>[1];
	my_assert(m!=0);
   m[0]=v;
}
template <class ElType>
matrix<ElType>::matrix(const matrix<ElType> &s)
{
	int i;
	rows=s.rows;
	m = new mvector<ElType>[rows];
	my_assert(m!=0);
	cols =s.cols;
	for(i=0;i<rows;i++)
	{
	  m[i]=s.m[i];
	}
}
template <class ElType>
matrix<ElType>::matrix(const matrix<ElType> &s, int r)
{
	int i;
	rows = r;
	m = new mvector<ElType>[rows];
	my_assert(m!=0);
	cols = s.cols;
   int rcopy = rows < s.rows ? rows : s.rows;
	for(i = 0; i < rcopy; i++)
	{
	  m[i] = s.m[i];
	}
	for(; i < rows; i++)
	{
	  m[i].zero();
	}
}
template <class ElType>
matrix<ElType>::~matrix()
{
	delete [] m;
}

template <class ElType>
matrix<ElType>& matrix<ElType>::operator =(const matrix<ElType> &s)
{
	if(this != &s)
	{
		delete []m;
		rows= s.rows;
		cols=s.cols;
		m = new mvector<ElType>[rows];
		my_assert(m !=0);
		for(int i=0;i<rows;i++) m[i]=s.m[i];
	}
	return *this;
}
template <class ElType>
mvector<ElType>& matrix<ElType>::operator[](const int i) const
{
	my_assert(i>=0 && i < rows);
	return m[i];
}
template <class ElType>
mvector<ElType> matrix<ElType>::operator*(const mvector<ElType>& v) const
{
	int i,j;
	my_assert(cols == v.len);
	mvector<ElType> ans(rows);
	for(i=0;i<rows;i++)
	{
		ans.data[i]=0.0;
		for(j=0;j<cols;j++) ans.data[i] += m[i][j]*v.data[j];
	}
	return ans;
}
template <class ElType>
matrix<ElType> operator*(ElType c,const matrix<ElType>& s)
{
	matrix<ElType> ans(s.rows,s.cols);
	for(int i=0;i<ans.rows;i++)
	  {
		ans.m[i]= c*s.m[i];
	  }
	return ans;
}
template <class ElType>
matrix<ElType> matrix<ElType>::transpose() const
{
   matrix<ElType> ans(cols,rows);
   for(int i=0;i<rows;i++)
	{
	  for(int j=0;j<cols;j++) ans[j][i]=m[i][j];
   }
   return ans;
}

template <class ElType>
matrix<ElType> mabs(const matrix<ElType> &a)
{
   matrix<ElType> ans(a.rows, a.cols);
   for(int i = 0; i < a.rows; i++)
	{
	   ans[i] = vabs(a[i]);
   }

   return ans;
}

template <class ElType>
matrix<ElType> operator*(const matrix<ElType>& s,ElType c)
{
	matrix<ElType> ans(s.rows,s.cols);
	for(int i=0;i<ans.rows;i++)
	  {
		ans.m[i]= c*s.m[i];
	  }
	return ans;
}
template <class ElType>
matrix<ElType>  matrix<ElType> ::operator*(const matrix<ElType>&  a) const
{

	my_assert(cols == a.rows);

	matrix<ElType>  ans(rows,a.cols);
	for(int i=0;i<rows;i++)
	{
		for(int j=0;j<a.cols;j++)
		{
			ans.m[i][j]=0.0;
			for(int k=0;k<cols;k++)
			{
				ans.m[i][j] += m[i][k]*a.m[k][j];
			}
		}
	}
	return ans;
}
template <class ElType>
matrix<ElType>  matrix<ElType> ::operator+(const matrix<ElType> & a) const
{
	int i,j;

	my_assert(rows== a.rows);
	my_assert(cols== a.cols);

	matrix<ElType>  ans(a.rows,a.cols);
	for(i=0;i<a.rows;i++)
	{
		for(j=0;j<a.cols;j++)
		  {
			ans.m[i][j] = m[i][j] + a.m[i][j];  //faster than assigning mvectors?
		}
	}
	return ans;
}
template <class ElType>
matrix<ElType> matrix<ElType>::operator-(const matrix<ElType>& a) const
{
	int i,j;
	my_assert(rows == a.rows);
	my_assert(cols == a.cols);
	matrix ans(rows,cols);
	for(i=0;i<rows;i++)
	{
		for(j=0;j<cols;j++)
		ans.m[i][j] = m[i][j] - a.m[i][j];
	}
	return ans;
}

template <class ElType>
ostream& operator<<(ostream& s, const matrix<ElType>& m)
{
	for(int i = 0; i < m.rows; i++)
      s << m[i];
	return s;
}

template <class ElType>
void matrix<ElType>::zero()
{
   for (int i=0; i < rows; i++)
     (*this)[i].zero();
}

template <class ElType>
void matrix<ElType>::fill(ElType value)
{
   for (int i=0; i < rows; i++)
     (*this)[i].fill(value);
}

template <class ElType>
void matrix<ElType>::setDiag(mvector<ElType> v)
{
   for (int i=0; i < rows; i++)
     (*this)[i][i] = v[i];
}

template <class ElType>
mvector<ElType> diag(const matrix<ElType> &s)
{
   int len = (s.rows<s.cols)? s.rows : s.cols;
   mvector<ElType> ans(len);
   for(int i=0;i<len;i++) ans[i]=s[i][i];
   return ans;
}
#define TINY 1.0e-20;
//we assume fabs(ElType) is defined
//assignment of doubles to ElType is defined
template <class ElType>
bool ludcmp(matrix<ElType>& a, mvector<int>& indx,double& d,
            int *percentDone=NULL, bool *abortFlag=NULL)
{
    //////////////////////////////////////////////////////////////////////
    // Given a matrix a[1..n][1..n], this routine replaces it by the LU
    // decomposition of a rowwise permutation of itself. a and n are input.
    // a is output, arranged as in equation (2.3.14) above; indx[1..n] is
    // an output vector that records the row permutation eected by the
    // partial pivoting; d is output as 1 depending on whether the number
    // of row interchanges was even or odd, respectively. This routine is
    // used in combination with lubksb to solve linear equations or invert
    //a matrix.
    //////////////////////////////////////////////////////////////////////

        if (percentDone != NULL)
            *percentDone = 0;
	int i,imax,j,k;
	ElType  big,dum,sum,temp;
	int n=a.rows;
	mvector<ElType> vv(n);
	my_assert(a.rows == a.cols);
	d=1.0;
	for (i=0;i<n;i++)
	{
		big=0.0;
		for (j=0;j<n;j++) if ((temp=fabs(a[i][j])) > big) big=temp;
		if (big == 0.0)
                   return false;
		vv[i]=1.0/big;
	}
	for (j=0;j<n;j++)
	{
                if ((abortFlag) != NULL && *abortFlag)
                {
                        *abortFlag = false;   // Ack
                        return false;
                }

		for (i=0;i<j;i++)
		{
			sum=a[i][j];
			for (k=0;k<i;k++) sum -= a[i][k]*a[k][j];
			a[i][j]=sum;
		}
		big=0.0;
		for (i=j;i<n;i++)
		{
			sum=a[i][j];
			for (k=0;k<j;k++) sum -= a[i][k]*a[k][j];
			a[i][j]=sum;
			if ( (dum=vv[i]*fabs(sum)) >= big)
			{
				big=dum;
				imax=i;
			}
		}
		if (j != imax)
		{
			for (k=0;k<n;k++)
			{
				dum=a[imax][k];
				a[imax][k]=a[j][k];
				a[j][k]=dum;
			}
			d = -(d);
			vv[imax]=vv[j];
		}
		indx[j]=imax;
		if (a[j][j] == 0.0) a[j][j]=TINY;
		if (j != n-1) {
			dum=1.0/(a[j][j]);
			for (i=j+1;i<n;i++) a[i][j] *= dum;
		}
                if (percentDone != NULL)
                    *percentDone = (j * 100)/n;
	}
  if (percentDone != NULL)
      *percentDone = 100;

  // Done
  return true;
}
#undef TINY
template <class ElType>
void lubksb(const matrix<ElType>& a,mvector<int>& indx,mvector<ElType>& b)
{
    ///////////////////////////////////////////////////////////////////////
    // Solves the set of n linear equations AX = B. Here a[1..n][1..n] is
    // input, not as the matrix A but rather as its LU decomposition,
    // determined by the routine ludcmp. indx[1..n] is input as the
    // permutation vector returned by ludcmp. b[1..n] is input as the
    // right-hand side vector B, and returns with the solution vector
    // X. a, n, and indx are not modifed by this routine and can be left
    // in place for successive calls with different right-hand sides b.
    ////////////////////////////////////////////////////////////////////////'

	int i,ip,j;
	ElType sum;
	int n=a.rows;
	for (i=0;i<n;i++)
	{
		ip=indx[i];
		sum=b[ip];
		b[ip]=b[i];
		for (j=0;j<=i-1;j++)
            sum -= a[i][j]*b[j];
		b[i]=sum;
	}
	for (i=n-1;i>=0;i--)
	{
		sum=b[i];
		for (j=i+1;j<n;j++)
            sum -= a[i][j]*b[j];
		b[i]=sum/a[i][i];
	}
}

// This sort of implements matlab's mldivide operator, but only for an
// nxn matrix mldivided by a nx1 matrix!
template <class ElType>
matrix<ElType> matrix<ElType>::mldivide(const matrix<ElType> &rhs) const
{
   if (rows != cols || rows != rhs.rows)
   {
      throw std::exception("Can only mldivide a square matrix by one with same number of rows and one column");
   }

   // Decompose the matrix
   mvector<int> mvIndex(rows);
   matrix<ElType> LU = *this;
   double d;
   if (!ludcmp<ElType>(LU, mvIndex, d))
   {
      return matrix<ElType>(0, 0);;
   }


   // Back substitution.
   mvector<ElType> outVec(rows);
   outVec = rhs.transpose()[0];
   lubksb<ElType>(LU, mvIndex, outVec);

   // We want to return a nx1 matrix, not an n-element vector.
   matrix<ElType> outMat(1, outVec.length());
   outMat[0] = outVec;
   return outMat.transpose();;
}

// Invert an LU matrix
template <class ElType>
matrix<ElType> invertLUmatrix(const matrix<ElType>& LU,
                                   mvector<int>& indx)
{
   int i, n = LU.rows;

   matrix<ElType> r(n, n);
   r.zero();
   if (n != LU.cols)
   {
      my_assert(false);
      return r;           // should assert
   }

   for (int i = 0; i < n; ++i)
   {
       r[i][i] = 1.0;
       lubksb(LU, indx, r[i]);
   }
   return r;   // or possibly return r.transpose();
}

// Invert a matrix
template <class ElType>
matrix<ElType> invertMatrix(const matrix<ElType>& a)
{
   int n = a.rows;
   matrix<ElType> r(a.rows,a.cols);
   r.zero();
   if (n != a.cols)
   {
      my_assert(false);
      return r;
   }

   mvector<int> mvIndex(n);
   matrix<ElType> LU = a;
   double d;

   // Decompose the matrix
   if (ludcmp<ElType>(LU, mvIndex, d))
   {
       r = invertLUmatrix(LU, mvIndex);
   }

   return r;
}

#if 0
// Invert a matrix guaranteed to be triangular using Gaussian elimination
template <class ElType>
matrix<ElType>  void invertTriangularMatrix(const matrix<ElType>& a,
                                            mvector<int>& indx,
                                            bool lower)
{
   int i, j;
   int n = a.rows;

   // We will
   matrix<double> m(a);

   // Allocate an identity matrix that will be become the result
   matrix<double> r(n, n);

   r,zero();
   if (a.rows != a.cols)
      return r;  // error - must be square

   if (lower)
   {
       // Now unitize the result matrix diagonal
       for (i = 0; i < m.rows; ++i)
       {
           r[i][i] = 1.0;
       }

       // OK, let's eliminate everything BELOW the diagonal in the source matrix
       for (i = 0; i < (n-1); ++i)
       {
           int ip = indx[i];  // row-permuted

           // For each row i except the last, eliminate the only remaining
           // value from all the rows starting at row i+1

           for (j = i+1; j < n; ++j)
           {
               // for the value vs at position (j,i) in the SOURCE matrix
               // and the value vc at position (i,i) in the RESULT matrix
               // and the value vr at position (j,j) in the RESULT matrix
               // value to subtract at result position (j,i) is (vs*vc*vr)
               r[j][i] -= m[j][i]*r[i][i]*r[j][j];
           }
       }
   }
   else // upper
       {
       // Unitize the result matrix diagonal, setting the result diagonal
       // at the same time
       for (i = 0; i < m.rows; ++i)
       {
           r[i][i] = 1.0/m[i][i];
       }

       // OK, let's eliminate everything ABOVE the diagonal in the source matrix
       for (i = n-1; i > 0; --i)
       {
           // Back substitution
           // For each row i except the first, eliminate the only remaining
           // value from all the rows starting at row i-1

           for (j = i-1; j >= 0; --j)
           {
               // for the value vs at position (j,i) in the SOURCE matrix
               // and the value vc at position (i,i) in the RESULT matrix
               // and the value vr at position (j,j) in the RESULT matrix
               // value to subtract at result position (j,i) is (vs*vc*vr)
               r[j][i] -= m[j][i]*r[i][i]*r[j][j];
           }
       }
   }
   return r;
}
#endif // 0



#endif

#endif // __TARRAY_H__

