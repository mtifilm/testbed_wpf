/*
   File Name:  SharedMem.h
   Created: Augest 22, 2003 by John Mertus


   This class contains the VTR shared memory functions
   The SharedMemClass file contains the real shared memory
*/
#ifndef _SHAREDMEM_H
#define _SHAREDMEM_H
#include "machine.h"
#include "SharedMemClass.h"
#include "TheError.h"
#include "MTIio.h"
#include "pTimecode.h"

#include "corecodelibint.h"

//
//  Dynamic messages
extern MTI_CORECODELIB_API unsigned MTI_MESSAGE_VTR_COMMAND;
//
//  The following structure defines the shared memory that a VTR uses to communicate
//  between programs, we cannot use classes, (CTimecode kinda works here by accident}

enum VTR_SHARED_ACTION {vsaNone, vsaCue};
enum VTR_SHARED_RESULT {vsrNone, vsrSuccess, vsrCannotDo};
enum VTR_SHARED_TYPE {vstUnknown, vsrExternal, vsrEmulator};

typedef struct VTRSharedMemoryStruct
{
   MTI_UINT32  PID;           // The PID of the VTR
   char ID[32];               // Shared ID name
   char Name[32];             // Device Name of VTR
   MTI_UINT32  Action;        // Action
   MTI_UINT32  AResult;       // Result of action
   CTimecode tcVTR;           // Current timecode of the VTR
   CTimecode tcCue;           // Cueup Timecode
   bool  AcceptCue;           // True if the tc accepts queues
   VTR_SHARED_TYPE Type;      // Type of VTR
   HANDLE HWnd;               // Window handle
}  VTRSharedMemory;

// Since memory has to be shared, just make a list
#define MAX_VTR_SECTIONS 10
#define MAX_VTR_SECTION_NAME 32
typedef struct VTRSharedMemoryListStruct
{
   int   Max;
   char  Name[MAX_VTR_SECTIONS][MAX_VTR_SECTION_NAME];
} VTRSharedMemoryList;

MTI_CORECODELIB_API void VTRSharedMemory_Init(VTRSharedMemory *VSM, const string &ID);
MTI_CORECODELIB_API void VTRSharedMemory_SetAction(VTRSharedMemory *VSM, VTR_SHARED_ACTION Action, int Data=0);
MTI_CORECODELIB_API bool VTRSharedMemory_Cue(VTRSharedMemory *VSM, const PTimecode &ptc);

//  This shared memory list is BOTH the Server and Clien
//  So the programmer must remember which or both they are using.
class MTI_CORECODELIB_API CVTRSharedMemoryList
{
  public:
     CVTRSharedMemoryList(void);
     ~CVTRSharedMemoryList(void);

     bool  CreateServer(void);
     bool  CreateServer(const string &ID);
     bool  DeleteServer(void);
     VTRSharedMemoryList *NameIndexList(void);
     VTRSharedMemory *ServerVTR(void);
     int  NumberOfVTRs(void);
     int  FindName(const string &Name);
     int  IndexByID(const string &ID);
     int  IndexByAttachToID(const string &ID);
     VTRSharedMemory *SelectVTR(int idx=-1);
     bool ReleaseVTR(int idx=-1);
     bool SetServerTimecode(const CTimecode &tc);
     void SetServerParameters(const string &Name, HANDLE h, VTR_SHARED_TYPE type);
     int DefaultIndex(int idx);
     int DefaultIndex(void);

  private:
     bool RemoveVTRSharedMemory(int idx);
     string PIDName(void);
     TSharedMem *mVSML;
     TSharedMem **_VTR;
     int _DefaultIndex;
     int _ServerIndex;
     string IDName;
};

bool MTI_CORECODELIB_API DoesVTRSharedMemoryExist(const string &Name);
void MTI_CORECODELIB_API SetCoreResolution(bool bLicenseSD, bool bLicenseHD, bool bLicense4K, bool bLicenseDI);
void MTI_CORECODELIB_API GetCoreResolution(bool &bLicenseSD, bool &bLicenseHD, bool &bLicense4K, bool &bLicenseDI);

#endif