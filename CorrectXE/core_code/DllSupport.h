/*
   File Name:  DllSupport.h
   Create:  July 11, 2001 by John Mertus
   Version 1.00   Initial Release

   This is a catchall library which defines certain functions necessary
   to support windows and DLL.
*/
#ifndef DllSupportH
#define DllSupportH

#include "machine.h"
#include "corecodelibint.h"
#include <string>

//
// Information about EXE and files
//
  extern MTI_CORECODELIB_API string GetDLLVersion(string Name);
  extern MTI_CORECODELIB_API std::pair<int, int> GetDLLMajorAndMinorVersionNumbers(string Name);
  extern MTI_CORECODELIB_API string GetFileDateString(string Name);
  extern MTI_CORECODELIB_API string GetDLLInfo(string Name, string Info);
  extern MTI_CORECODELIB_API string GetDLLCopyright(string Name);
  extern MTI_CORECODELIB_API char *GetUserFolder (void * hPar, const char *StartFolder, const char *title);
  extern MTI_CORECODELIB_API bool IsAdmin(void);
  extern MTI_CORECODELIB_API bool KillNamedWindow(const string &WinName);
  extern MTI_CORECODELIB_API bool KillProgramByWinHandle(const HWND h);
  extern MTI_CORECODELIB_API void KillAllWindowsButMe(const string &WinName);
  extern MTI_CORECODELIB_API void KillAllProgramsButMe(const string &WinName);

// GetSystemMessage returns a string that contains a human readable
// version of the error message in ErrorCode
// Ususal usage is ErrorMessage = GetSystemMessage(GetLastError());
  string MTI_CORECODELIB_API GetSystemMessage(int ErrorCode);

#endif
