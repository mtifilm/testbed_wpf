/*
     File Name:  HRTimer.h
     Create:  March 2, 2000
     Release 2.00  Support windows and unix
*/
//---------------------------------------------------------------------------
#ifndef HRTimerH
#define HRTimerH
#include "machine.h"
#include "corecodelibint.h"

/*
  This defines a high resolution timer.  There are two important
  member functions
      Start    Which start counting
      Read     Which returns the current count in milliseconds;

*/
class MTI_CORECODELIB_API CHRTimer
 {
   public:
     CHRTimer(void);        // Construct it, does a start
    ~CHRTimer(void);
     double Read(bool reset=false);
     void Start(void);
     string ReadAsString(void);
     void Delay(float fDelay);     // Delays for fDelay milliseconds
                                   // can do sub milliseconds 
     void DelayNS(float fDelay);   // Non-spinning version of Delay
                                   // can do sub milliseconds 
     void Sleep (int iDelay);    // Sleeps for the specified number
				   // of milliseconds

	  double intervalMilliseconds();
 	  double elapsedMilliseconds();

   private:
	 double m_dClockFreq;     // The HR clock frequency
	 double m_dStartCount;    // The start fills this in
	 double m_intervalStart;  // For the interval timer

#ifdef _WINDOWS
     UINT wTimerRes;     // resolution of the Windows Multi-media timer
     HANDLE hWaitableTimer; // handle to waitable timer
#endif
 };
//---------------------------------------------------------------------------

MTI_CORECODELIB_API void SleepMS(int ms);

typedef enum  {enSECONDS, enMILLISECONDS, enMICROSECONDS } enHRT;
MTI_CORECODELIB_API string WallClockStamp(enHRT Resolution = enMILLISECONDS);

MTI_CORECODELIB_API std::ostream& operator<<(std::ostream& os, CHRTimer &hrt);
#endif
