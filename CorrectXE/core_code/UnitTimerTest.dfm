object Form1: TForm1
  Left = 192
  Top = 108
  Width = 952
  Height = 656
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 96
    Top = 128
    Width = 32
    Height = 13
    Caption = 'Label1'
  end
  object Label2: TLabel
    Left = 320
    Top = 128
    Width = 32
    Height = 13
    Caption = 'Label2'
  end
  object Label3: TLabel
    Left = 512
    Top = 128
    Width = 32
    Height = 13
    Caption = 'Label2'
  end
  object Label4: TLabel
    Left = 696
    Top = 128
    Width = 32
    Height = 13
    Caption = 'Label2'
  end
  object Label5: TLabel
    Left = 88
    Top = 280
    Width = 32
    Height = 13
    Caption = 'Label2'
  end
  object Label6: TLabel
    Left = 264
    Top = 280
    Width = 32
    Height = 13
    Caption = 'Label2'
  end
  object Label7: TLabel
    Left = 440
    Top = 280
    Width = 32
    Height = 13
    Caption = 'Label2'
  end
  object Button1: TButton
    Left = 96
    Top = 80
    Width = 105
    Height = 25
    Caption = 'mtiGetUST'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 320
    Top = 80
    Width = 113
    Height = 25
    Caption = 'timeSetEvent'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Edit1: TEdit
    Left = 96
    Top = 48
    Width = 121
    Height = 21
    TabOrder = 2
    Text = '50'
  end
  object Edit2: TEdit
    Left = 320
    Top = 48
    Width = 121
    Height = 21
    TabOrder = 3
    Text = '50'
  end
  object Edit3: TEdit
    Left = 512
    Top = 48
    Width = 121
    Height = 21
    TabOrder = 4
    Text = '50'
  end
  object Button3: TButton
    Left = 512
    Top = 80
    Width = 113
    Height = 25
    Caption = 'Sleep'
    TabOrder = 5
    OnClick = Button3Click
  end
  object Edit4: TEdit
    Left = 696
    Top = 48
    Width = 121
    Height = 21
    TabOrder = 6
    Text = '50'
  end
  object Button4: TButton
    Left = 696
    Top = 80
    Width = 113
    Height = 25
    Caption = 'HRT delay'
    TabOrder = 7
    OnClick = Button4Click
  end
  object Edit5: TEdit
    Left = 88
    Top = 200
    Width = 121
    Height = 21
    TabOrder = 8
    Text = '50'
  end
  object Button5: TButton
    Left = 88
    Top = 232
    Width = 113
    Height = 25
    Caption = 'Waitable timer'
    TabOrder = 9
    OnClick = Button5Click
  end
  object Edit6: TEdit
    Left = 264
    Top = 200
    Width = 121
    Height = 21
    TabOrder = 10
    Text = '50'
  end
  object Button6: TButton
    Left = 264
    Top = 232
    Width = 113
    Height = 25
    Caption = 'HRT delayNB'
    TabOrder = 11
    OnClick = Button6Click
  end
  object Edit7: TEdit
    Left = 440
    Top = 200
    Width = 121
    Height = 21
    TabOrder = 12
    Text = '50'
  end
  object Button7: TButton
    Left = 440
    Top = 232
    Width = 113
    Height = 25
    Caption = 'HRT Sleep'
    TabOrder = 13
    OnClick = Button7Click
  end
end
