/*  File: machine.h
    Created June 19, 2000 by John Mertus
    Keeper: John Mertus

    This file contains the typedefs for the MTI code that translates the
    dependent hardware onto independent software.

    This is the ONLY place that common machine dependent typedefs
    can be defined which create machine independent code.

    Note however that specific machine dependent typedefs and functions can
    be defined elsewere but used to create machine dependent code.

    This supports some conditionals

      __sgi  means we are using the sgi cc or CC compiler
      __unix means we are on a unix system, including linux, is defined for cc and gcc
      __linux means we are on a linux system
      _WINDOWS means we are on a windows machine

      __GUNC__ means we are using the gcc compiler
      __BORLANDC__ means we are using the BorlandC compiler (Note: This does not imply windows)

    THIS CODE IS MACHINE DEPENDENT!
*/

#ifndef _MACHINE_H
//------------------------------COMMON SECTION--------------------------------

//
// Deal with the endian issues, ENDIAN will be defined to mean
// which endian we are compiling under.
//
#define MTI_BIG_ENDIAN 1                  // The symbolic names
#define MTI_LITTLE_ENDIAN 2


//----------------PRE WINDOWS SECTION, VC 6 or BC 4------------------------------------
//
// By default, Borland defines _Windows and VC defines _WIN32, define _WINDOWS also
//
#if defined (_Windows) || defined (_WIN32)
#ifndef _WINDOWS
#define _WINDOWS
#endif
#endif

//----------------WINDOWS SECTION, VC 6 or BC 4--------------------------------
#ifdef _WINDOWS

#ifdef _MSC_VER // Visual C++ specific
#define _WIN32_WINNT 0x0400 // target system is at least NT4; allows
                            // MSVC++ to pick up functions from
                            // windows.h that would otherwise be
                            // excluded
#endif

#include <windows.h>
#include <algorithm>

#define ENDIAN MTI_LITTLE_ENDIAN             // The actual edian of the machine

// Now make explicit 64, 32, 16 and 8 bit data types.
typedef __int64 MTI_INT64;                  // Signed 64 bits
typedef unsigned __int64 MTI_UINT64;        // Unsigned 64 bits

#if defined(_MSC_VER)
#define INT64_LITERAL(x) x ## i64
#define UINT64_LITERAL(x) x ## ui64
#else
#define INT64_LITERAL(x) x ## LL
#define UINT64_LITERAL(x) x ## ULL
#endif

typedef int MTI_INT32;                      // Signed 32 bits
typedef unsigned int MTI_UINT32;            // Unsigned 32 bits

typedef short MTI_INT16;                    // Signed 16 bits
typedef unsigned short MTI_UINT16;          // Unsigned 16 bits

typedef signed char MTI_INT8;               // Signed 8 bits.
typedef unsigned char MTI_UINT8;            // Unsigned 8 bits

typedef float MTI_REAL32;                      
typedef double MTI_REAL64;

#define strcasecmp stricmp                  // Compare case insenstive
#define strncasecmp strnicmp

#ifdef __BORLANDC__
////#define __func__ __FUNC__  // C++Builder 6 doesn't define __func__
#endif

#ifdef _MSC_VER // Visual C++ specific

#ifndef INVALID_SET_FILE_POINTER
#define INVALID_SET_FILE_POINTER ((DWORD)-1)  //For SetFilePointer()
#endif

#define for if(false) {} else for

#define S_IRUSR S_IREAD
#define S_IWUSR S_IWRITE
#define S_IXUSR S_IEXEC
#define S_IRWXU (S_IREAD|S_IWRITE|S_IEXEC)

#endif // end Visual C++ specific

// Make sure we are defined and only once
#ifdef _MACHINE_H
#error Multiple definitions of machine types
#endif

#define _MACHINE_H
#endif


//----------------MSDOS SECTION, VC 6 or BC 4--------------------------------

#ifdef _CONSOLE
#define __CONSOLE_DIALOGS
#include <basetsd.h>
#define ENDIAN MTI_LITTLE_ENDIAN           // The actual edian of the machine

// Now make explicit 64, 32, 16 and 8 bit data types.
typedef __int64 MTI_INT64;                  // Signed 64 bits
typedef unsigned __int64 MTI_UINT64;        // Unsigned 64 bits

#if defined(_MSC_VER)
#define INT64_LITERAL(x) x ## i64
#define UINT64_LITERAL(x) x ## ui64
#else
#define INT64_LITERAL(x) x ## LL
#define UINT64_LITERAL(x) x ## ULL
#endif

typedef int MTI_INT32;                      // Signed 32 bits
typedef unsigned int MTI_UINT32;            // Unsigned 32 bits

typedef short MTI_INT16;                    // Signed 16 bits
typedef unsigned short MTI_UINT16;          // Unsigned 16 bits

typedef signed char MTI_INT8;               // Signed 8 bits.
typedef unsigned char MTI_UINT8;            // Unsigned 8 bits

typedef float MTI_REAL32;                   // 
typedef double MTI_REAL64;

//
// Make sure we are defined and only once
#ifdef _MACHINE_H
#error Multiple definitions of machine types
#endif

#define _MACHINE_H
#endif



//-----------------SGI SECTION, cc or gcc compilers----------------------------
#ifdef __sgi
#include <sgidefs.h>

#define ENDIAN MTI_BIG_ENDIAN                // The actual edian of the machine
// Now make explicit 64, 32, 16 and 8 bit data types.

typedef __int64_t MTI_INT64 ;               // Signed 64 bits
typedef __uint64_t MTI_UINT64;              // Unsigned 64 bits

#define INT64_LITERAL(x) x ## LL
#define UINT64_LITERAL(x) x ## ULL

typedef __int32_t MTI_INT32;                // Signed 32 bits
typedef __uint32_t MTI_UINT32;              // Unsigned 32 bits

typedef short MTI_INT16;                    // Signed 16 bits
typedef unsigned short MTI_UINT16;          // Unsigned 16 bits
typedef signed char MTI_INT8;               // Signed 8 bits.
typedef unsigned char MTI_UINT8;            // Unsigned 8 bits

typedef float MTI_REAL32;                   // 
typedef double MTI_REAL64;

//-- inserted by KT 3/26/02
// defined for SGI same as WINDOWS native

struct POINT
{
   MTI_INT32 x;
   MTI_INT32 y;
};

//-- inserted by KT 7/17/01
// defined for SGI same as WINDOWS native

struct RECT
{
   MTI_INT32 left;	// inclusive
   MTI_INT32 top;	// inclusive
   MTI_INT32 right;	// inclusive
   MTI_INT32 bottom;	// inclusive
};

//
// Make sure we are defined and only once
#ifdef _MACHINE_H
#error Multiple definitions of machine types
#endif
#define _MACHINE_H

#endif

//-----------------Linux SECTION, cc or gcc compilers--------------------------
#ifdef __linux

#define __CONSOLE_DIALOGS

#ifdef __i386 // endianness is really machine- not OS-dependent

#define ENDIAN MTI_LITTLE_ENDIAN            // The actual endian of the machine

#endif // __i386

#include <sys/types.h>

typedef int64_t MTI_INT64;                 // Signed 64 bits
typedef u_int64_t MTI_UINT64;              // Unsigned 64 bits

#define INT64_LITERAL(x) x ## LL
#define UINT64_LITERAL(x) x ## ULL

typedef int32_t MTI_INT32;                 // Signed 32 bits
typedef u_int32_t MTI_UINT32;              // Unsigned 32 bits

typedef int16_t MTI_INT16;                 // Signed 16 bits
typedef u_int16_t MTI_UINT16;              // Unsigned 16 bits

typedef int8_t MTI_INT8;                   // Signed 8 bits.
typedef u_int8_t MTI_UINT8;                // Unsigned 8 bits

typedef float MTI_REAL32;
typedef double MTI_REAL64;

// defined same as WINDOWS native
struct POINT
{
   MTI_INT32 x;
   MTI_INT32 y;
};

// defined same as WINDOWS native
struct RECT
{
   MTI_INT32 left;	// inclusive
   MTI_INT32 top;	// inclusive
   MTI_INT32 right;	// inclusive
   MTI_INT32 bottom;	// inclusive
};

//
// Make sure we are defined and only once
#ifdef _MACHINE_H
#error Multiple definitions of machine types
#endif
#define _MACHINE_H

#endif // __linux

/* Make sure we are defined at least once */
#ifndef _MACHINE_H
#error No machine has been defined
#endif

//------------------------------COMMON SECTION---------------------------------

// color definitions for the rectangles
#define INVALID_COLOR  -1
#define SOLID_RED       0
#define DASHED_RED      1
#define SOLID_GREEN     2
#define DASHED_GREEN    3
#define SOLID_BLUE      4
#define DASHED_BLUE     5
#define SOLID_WHITE     6
#define DASHED_WHITE    7
#define SOLID_YELLOW    8
#define DASHED_YELLOW   9
#define SOLID_CYAN     10
#define DASHED_CYAN    11
#define SOLID_MAGENTA  12
#define DASHED_MAGENTA 13
#define SOLID_BLACK    14
#define DASHED_BLACK   15
#define SOLID_LTGRAY   16
#define DASHED_LTGRAY  17
#define SOLID_DKGRAY   18
#define DASHED_DKGRAY  19
#define SOLID_ORANGE   20
#define DASHED_ORANGE  21
#define NUMBER_RECT_COLORS 22

#define LOWPART_LONGLONG		((LONGLONG) 0x00000000FFFFFFFF)
#define HIGHPART_LONGLONG		((LONGLONG) 0xFFFFFFFF00000000)

#define LOWLONGLONG(x)			 ( (MTI_UINT32) ( (x) & LOWPART_LONGLONG ) )
#define HILONGLONG(x)			 ( (MTI_INT32) ( ( (x) & HIGHPART_LONGLONG ) >> 32 ) )

#define MINFRAMEX -0x7fffffff
#define MAXFRAMEX  0x7fffffff
#define MINFRAMEY -0x7fffffff
#define MAXFRAMEY  0x7fffffff

// used by Bezier curves
struct DRECT
{
   double left;
   double top;
   double right;
   double bottom;
};

struct DPOINT
{
   double x;
   double y;
};

struct BEZIER_POINT
{
   MTI_INT32 xctrlpre;
   MTI_INT32 yctrlpre;
   MTI_INT32 x;
   MTI_INT32 y;
   MTI_INT32 xctrlnxt;
   MTI_INT32 yctrlnxt;
};

// Time word
#define MTI_TIME MTI_INT32
#define MTI_TIME_NEVER 0
#define MTI_TIME_EXPIRED (-1)

// Define some common includes
#ifndef AVID_SDK_CONFLICT
#ifdef __cplusplus
#include <sstream>

#if defined(__GNUG__) && (__GNUG__ < 3) // This wouldn't be necessary
					// if libstdc++ had the right
					// headers
#include <iostream>
#else
#include <ostream>
#endif

#include <string>
#include <iostream>
#include <iomanip>
#include <vector>

using std::string;
using std::endl;
using std::cout;
using std::cin;
using std::vector;
using std::string;
using std::setw;
using std::setfill;
using std::setbase;
using std::left;
using std::right;

#endif /* __cplusplus */
#endif // AVID_SDK_CONFLICT

// End of machine.h

#ifndef MTI_ASM_X86_ATT
#define MTI_ASM_X86_ATT 0
#endif

#ifndef MTI_ASM_X86_INTEL
#define MTI_ASM_X86_INTEL 0
#endif

//======================================================================

#define OVERRIDE_NEW_AND_DELETE 0

#if OVERRIDE_NEW_AND_DELETE

#include <new>
#include "MTImalloc.h"

// Override global operator new
void* operator new (size_t size)
{
   return MTInew(size);
}

// Override global operator new[]
void* operator new[] (size_t size)
{
   return MTInewArray(size);
}

// Override global operator delete
void operator delete (void *p)
{
   MTIdelete(p);
}

// Override global operator delete[]
void operator delete[] (void *p)
{
   MTIdeleteArray(p);
}
#endif // OVERRIDE_NEW_AND_DELETE

//========================================================================
// MTI assert - hardcoded breakpoint (from which you can continue, unlike
//              the official assert(). NOTE! DON'T ALLOW ASSERT TO BE
//              TRIGGERED FOR STATICALLY ALLOCATED OBJECTS!
//              Also: whatever uses this must also include IniFile.h to get
//              TRACE_0 definition
//
#ifdef _WINDOWS

  #ifdef  _DEBUG

    #define MTIassert(x) \
      if (!(x)) { \
        TRACE_0(errout << __FILE__ << ":" << __LINE__  << ": " \
                       << "ASSERT (" << #x << ") FAILED!"); \
       DebugBreak(); \
      } \
      else;

  #else  // If not in debug mode, lose the breakpoint and just TRACE

    #define MTIassert(x) \
      if (!(x)) { \
        TRACE_0(errout << __FILE__ << ":" << __LINE__  << ": " \
                       << "ASSERT (" << #x << ") FAILED!"); \
      } \
      else;

  #endif

#else   // Linux uses assert, which we can use safely, since it doesn't bring up a dialog within the program.

  #include <assert.h>
  #define MTIassert assert

#endif
//========================================================================

#ifdef _WIN64
struct ShortROI
{
   short Left = 0;
   short Right = 0;
   short Top = 0;
   short Bottom = 0;

   ShortROI()
   {
      Left = -1;
      Right = -1;
      Top = -1;
      Bottom = -1;
   }

   ShortROI(int left, int right, int top, int bottom)
   {
      Left = left;
      Right = right;
      Top = top;
      Bottom = bottom;
   }

   bool IsSet()
   {
      return (Left != Right) || (Top != Bottom);
   }

   bool Equals(const ShortROI &shortROI) const
   {
      return (Left == shortROI.Left) && (Right == shortROI.Right) && (Top == shortROI.Top) &&
         (Bottom == shortROI.Bottom);
   }

   bool Equals(const RECT &rect) const
   {
      return (Left == rect.left) && (Right == rect.right) && (Top == rect.top) && (Bottom == rect.bottom);
   }

   void Set(const RECT &rect)
   {
      Left = rect.left;
      Right = rect.right;
      Top = rect.top;
      Bottom = rect.bottom;
   }

   RECT GetROIasRect() const
   {
      RECT rect;
      rect.left = Left;
      rect.right = Right;
      rect.top = Top;
      rect.bottom = Bottom;
      return rect;
   }
};

static_assert(sizeof(ShortROI) == 4*sizeof(short), "ShortROI incorrect size");
static_assert(std::is_trivially_copyable<ShortROI>::value, "ShortROI must be able to be copied via mem copy");

// This is a simple iterator for integer and loops
// simple replacement for for loops
// In release mode it is slightly slower than for loops so be careful
#include <iterator>     // std::iterator, std::input_iterator_tag
class loopIterator : public std::iterator<std::input_iterator_tag, int>
{
public:
	loopIterator(int x) :_q(x) {}
	loopIterator(const loopIterator& mit) : _q(mit._q) {}
	loopIterator& operator++() { ++_q; return *this; }
	loopIterator operator++(int) { loopIterator tmp(*this); operator++(); return tmp; }
	bool operator==(const loopIterator& rhs) const { return _q == rhs._q; }
	bool operator!=(const loopIterator& rhs) const { return _q != rhs._q; }
	int& operator*() { return _q; }
	void setQ(int n) { _q = n; }

private:
	int _q;
};

// range(low, high) goes from low to high,
// including low but excluding high.
// for (i = low; i < high; i++)
// usage
//   	for (auto i : range(low, high))
class range
{
public:
	range(int high) : range(0, high) {}
	range(size_t high) : range(0, int(high)) {}

	range(size_t low, size_t high) : range(int(low), int(high)) {}
	range(int low, int high)
	{
 		// if high < low, we would never stop
		// so, if so, just set to no loop
		high = std::max<int>(low, high);
		_begin.setQ(low);
		_end.setQ(high);
	}

	loopIterator begin() { return _begin; }
	loopIterator end() { return _end; }

private:
	loopIterator _begin{ 0 };
	loopIterator _end{ 0 };
};

// loop(start, reps) does reps repeditions starting at start
// for (i=start; i < start+reps; i++)
// usage
//     for (auto i : loop(start, reps))
// or
//     for (auto i : loop(reps))  // Same as range(reps);
class loop : public range
{
	loop(size_t start, size_t reps) : range(int(start), int(start + reps)) {}
	loop(int start, int reps) : range(start, start + reps) {	}
	loop(int reps) : range(reps) {	}
};

// Does this belong here>
inline std::ostream& operator << (std::ostream& os, const RECT &rhs)
{
	os << "(left=" << rhs.left << ", top=" << rhs.top << ", ";
	os << "(right=" << rhs.right << ", bottom=" << rhs.bottom << ", ";
	os << "w=" << rhs.right - rhs.left << ", h=" << rhs.bottom - rhs.top << ")";
	return os;
}

inline std::ostream& operator << (std::ostream& os, const ShortROI &rhs)
{
	os << "(left=" << rhs.Left << ", top=" << rhs.Top << ", ";
	os << "(right=" << rhs.Right << ", bottom=" << rhs.Bottom << ", ";
	os << "w=" << rhs.Right - rhs.Left << ", h=" << rhs.Bottom - rhs.Top << ")";
	return os;
}

// How low should our typedefs be?
// Probably not here
typedef std::pair<int, int> SceneCut;

// HACK to fix error in lround
// WTF! lround is defined in std, but not implemented (get "unresolved external"
inline long my_lround(float f)
{
   return (long) (f + 0.500001F);
}

#endif
#endif //_MACHINE_H



