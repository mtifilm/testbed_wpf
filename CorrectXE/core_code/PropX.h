//
// File Name: PropX.h
// Created July 20, 2000 by John Mertus
//
// Version 1.00  Initial Release
//         1.01  Added Notify() method and callbacks to Read only properties
//         1.02  Added CBProperty, changed function callbacks
//         1.03  Removed Property from DEFINE_CBHOOK, it wasn't used
//
// This header allows property of an object to appear as simple varaibles.
// It also provides call back hooks from the properties into visual components.
//
//-------------Hooks-----------------------------------------------
//
//  The lowest level of a property is the CBHook.  This gives a class the
// ability to easily call back a method of another procedure when necessary.
// This version support multiple callback and notify inhibit.
//
//  You may derive a class from CBHook or use a multiple class declaration.
//  For example
//    Class PTimecode : public CTimecode, public CBHook
//
//  Within PTimecode, you decide when the notify should occure
//  to force a notify use Notify(), e.g,
//    void SetTC(CTimecode &Value)
//    {
//       if (Value != PrivateValue)
//          {
//             PrivateValue = Value;
//             Notify();
//          }
//       else
//          {
//              if (GetInvalidState()) Notify();
//          }
//    }
//
//  How to declare callbacks.  Years after PTimecode is created, another class
//  is needed that uses timecode as a property.
//
//  class Today
//    public:
//      PTimecode tcP;
//    private:
//       DEFINE_CBHOOK(tcpChanged, Today);
//
//  ! Besides other things, this defines a method for you
//       void tcpChanged(void *Data);
//
//  };
//
//  In the constructor you MUST do
//    Today::Today(void)
//      {
//         SET_CBHOOK(tcpChanged, tcP);
//      }
//
//  Finally, you create the callback function
//
//    void Today::tcpChanged(void *Sender)
//     {
//         PTimecode *ptcP = (PTimecode *)Sender;
//         ...
//     }
//
//
//  More general:
//  SET_CBHOOK(CBFcn, V1, CL);
//  Where
//    CBFcn is the name of the callback function you wish to define
//    V1 is the property name
//    CL is the class in which this declaration occures.
//
//  If you wish, you can look at DEFINE_CBHOOK and do it by hand to
//  make it any name you wish.
//
//  In the initialization code
//  SET_CBHOOK(CBName,V1) must be called OR
//  SET_CBHOOK_DATA(CBName, V1, Pointer)  OR
//  SET_CBHOOK_POINTER(CBName,V1 *) must be called
//  SET_CBHOOK_POINTER_DATA(CBName, V1 *, Pointer);
//
//   Note the CBName and property MUST match the one define by DEFINE_CBHOOK
//
//  Once defined, there are certain macro you can call
//  WARNING
//    These macros must be called within the same class that set the hook
//
//       void REMOVE_CBHOOK(CBNAME, V1)
//    Removes CBNAME that is used by property V1
//
//       void REMOVE_ALL_CBHOOKS(V1)
//    Removes all the callbacks associated with class V1
//
//  if you need to remove these callback outside the setting class,
//  look the macros and at RemoveCallback and RemoveAllCallback and
//  replace this with the class
//
//  Inhibiting callbacks
//       InhibitCallbacks(void)
//    When called, no notify event, even specified by Notify() will be invoked
//
//       UnInhibitCallbacks(void)
//    Restores the notify callbacks, if there was a Notify pending, it is invoked
//
//    NOTE: InhibitCallbacks/UnInhibitCallbacks MUST be called in pairs before
//    the callback is restored or inhibited.
//
//    Invalidate concept.
//
//    For example, suppose you wish to both set a value and then Notify, even if
//    the value is not changed, this is often the case for user input
//
//    InvalidateData();
//    SetTC(new value);
//
//    NOTE:  When using the properties described below, the Setters are in
//    effect callbacks/Notify without inhibit, the invalid concept is built
//    in and does not have to be designed as in SetTC.
//
//    E.g., if we declare
//       PROPERTY(CTimecode, tc, GetTC, SetTC, Today);
//   and
//       PROPERTY_INIT(tc, GetTC, SetTC)
//
//   The Getters, Setters are
//      void SetTC(CTimecode Value)
//   {
//      PrivateTC = Value;
//   }
//
//   Finally SET_CBHOOK_DATA can be used to change the data passed to the
//   callback from the sender to arbitray data pointer.  It is not recommend that
//   this form is used.
//
//------------------------------Arithemetic Properties---------------------------
//
//
// This version defines properties that have arithmetic operators
// defined such as +, ++, +=
//
// For example:
//  To define a property Width that allows a minimum and maximum
//  width of an image:
//
//  First define two function, a setter and a getter in the
//  private area.
//
//     class test
//       ...
//     private:
//       int fWidth;
//       void SetWidth(int Value)
//         {
//             if (Value < 640) Value = 640;
//             if (Value > 1600) Value = 1600;
//             if (fWidth == Value) return;
//             fWidth = Value;
//             UpdateDisplay();
//         };
//       int GetWidth(void) {return(fWidth);};
//
//   Now, two lines must be added,
//     First: in the public area declare the interface to the property Width.
//      public:
//        PROPETY(int, Width, GetWidth, SetWidth, test);
//     this says, delcare a property Width
//     use function GetWidth as the setter, and SetWidth as the getter,
//
//  The PROPERTY Macro is of the form
//     PROPERTY(type, property Name,  Getter function, Setter function, class);
//  And this MUST go in the public part of the interface! These is exactly 0
//  reasons for defining a PROPERTY as protected or private.  It will fail in
//  unexpected ways if you do.
//
//    The last arguement is always the class name.
//
//  C++ doesn't allow compile time initalization for classes, so
//  in the constructor, this property must be inited, e.g., the actual
//  Getter and Setter functions attached to the property.
//
//     test()
//       ...
//       PROPERTY_INIT(Width, GetWidth, SetWidth);
//
//  Now outside the class, you address Width almost as a common integer,
//  E.g.,  test t;
//     t.Width = 34;
//     next = t.Width + 128;
//     t.Width++;
//
//  However, it does NOT have an address, so you cannot pass to a function,
//  e.g.  function(&t.Width) is not valid.
//
//  Do not think of PROPERTIES as an integer, but as a property.  The
//  same distinction is made between Classes and Structures.
//
//  Note: Because the Getter is so often just returns the value of the
//  private variable associated with the property, there is a shortcut.
//  SPROPERTY for Short PPROPERTY
//
//       SPROPERTY(Type, Property Name, Private Variable, Setter Function, Class);
//  E.g.,
//       SPROPERTY(int, Width, fWidth, SetWidth, test);
//  note the third arguement is not a setter but a variable.
//  the init is SPROPERTY_INIT(Width, fWidth, SetWidth);
//
//  Lastly, a property can be readonly, buy using the RO_PROPERTY macro
//       RO_PROPERTY(int, Width, GetWidth, test);
//  and  RO_PROPERTY_INIT(Width, GetWidth);
//
//  and the short versions
//       RO_SPROPERTY(int, Width, fWidth, test);
//       RO_SPROPERTY_INIT(Width, fWidth);
//
//  Final Note:  The define and init macros are paired, never use one
//  type with another type, you will get a compiler error of an undefined
//  function.
//
// NOTE: Never call this class directly, use the PROPERTY macros
//
//  Specifications:
//    roValueProperty                    Class with a getter, no setter
//    lValueProperty : roValueProperty   Derived, adding a setter and =
//    aValueProperty : lValueProperty    Derived, add arithmetic ++, += etc.
//
#ifndef PROPX_H
#define PROPX_H
#include <list>

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)  // Too hard to fix, should be OK
#endif


//
//-----------------------CBHook---------------------------------------
//
// This is a callback property, used to allow callbacks to classes
//
class CBHook
  {
      private:
        // The SET_CALLBACK_HOOK macro uses this to communicate back
        // to another class.  pa is the address of the calling class
        // d is another class, will be "this" class
        typedef void (*CallBackFcn)(void *pa, void *d);
        struct CallBackData
          {
            void *Class;        // A pointer to the class
            void *Data;         // A pointer to data
            CallBackFcn Fcn;    // What to call
          };


      private:
        bool jInvalidState;     // Invalidate flag
        int  jInhibit;          // Used to inhibit notification
        bool jNotify;           // Should we notify if we reset.
        std::list<CallBackData> *jCBList;  // This contains the callback data
                                      // Each element represents a callback
                                      // to one class

      public:

      // Init just tells us that there are no property callbacks
      // The actual construction comes when the first callback
      // is installed
      CBHook()
        {
           jCBList = NULL;     // Real constructor will happen when this is created
        };

      ~CBHook()
        {
           delete jCBList;     // Clean up
           jCBList = NULL;
        };

      //
      //  This is the notify function, call it to force a list of callbacks
      //
      void Notify(void)
        {
          if (jCBList != NULL)
            {
               // Next run down the list and notify it
               if (jInhibit > 0)
                 {
                    jNotify = true;   // Mark that a notify was desired but inhibited
                    return;
                 }
               for (std::list<CallBackData>::iterator i = jCBList->begin(); i != jCBList->end(); i++)
                 if (i->Fcn != NULL) i->Fcn(i->Class, i->Data);

               jNotify = false;        // No need to notify
            }
          jInvalidState = false;     // Data has been notified.
        };

        // The call back function setter
        // A list is made for multiple callbacks.
        //
        //
        void SetCallBack(void *pa, CallBackFcn cfp, void *Data)
          {
            // Ignore null classes
            if (pa == NULL) return;

            // First check if we need to create the list
            if (jCBList == NULL)
              {
                jCBList = new std::list<CallBackData>;
                jNotify = false;
                jInvalidState = true;
                jInhibit = 0;
              }

            // Next see if the class already exists in the list
            for (std::list<CallBackData>::iterator i = jCBList->begin(); i != jCBList->end(); i++)
              if (i->Class == pa)
                  // If class and pointer already exist, just change the data
                  if (cfp == i->Fcn)
                    {
                       i->Data = Data;
                       return;
                    }

            // If function pointer is null, just exit.  E.g., allow deletes
            // when class is already deleted.
            if (cfp == NULL) return;

            // Does not exist, add it
            CallBackData CBD;
            CBD.Class = pa;
            CBD.Fcn = cfp;
            CBD.Data = Data;
            jCBList->push_back(CBD);
          };

        // Delete a callback
        // Call this to remove a callback function from the list
        //
        void RemoveCallBack(void *pa, CallBackFcn cfp)
          {
            // First check if there is anything to delete
            if (jCBList == NULL) return;

            // Next see if the class already exists in the list
            std::list<CallBackData>::iterator i = jCBList->begin();
            while (i != jCBList->end())
              {
                if ((i->Class == pa) && (cfp == i->Fcn))
                  {
                    jCBList->erase(i);
                    i = jCBList->begin();
                  }
                else
                  i++;
              }
          };

        // Delete all callbacks
        // Call this to remove all callbacks for a given class
        // If NULL passed, all classes are deleted
        //
        void RemoveAllCallBacks(void *pa)
          {
            // First check if there is anything to delete
            if (jCBList == NULL) return;

            // Next see if the class already exists in the list
            if (pa != NULL)
              {
                std::list<CallBackData>::iterator i = jCBList->begin();
                while (i != jCBList->end())
                  {
                    if (i->Class == pa)
                      {
                        jCBList->erase(i);
                        i = jCBList->begin();
                      }
                    else
                      i++;
                   }
             }
            else
              {
                jCBList->clear();
              }
          };

        // The sets all the data for the callbacks
        //
        void SetAllData(void *Data)
          {
            // First check if there is anything to delete
            if (jCBList == NULL) return;
            for (std::list<CallBackData>::iterator i = jCBList->begin(); i != jCBList->end(); i++)
               i->Data = Data;
          };

        // Inhibit callbacks
        // When inhibited, no notify at all is send
        //
        void InhibitCallbacks(void)
          {
             jInhibit++;             // Pop our inhibit flag
          }

        // This allows for callback changes
        // If changed, then force a Notify
        void UnInhibitCallbacks(void)
          {
             jInhibit--;
             if (jNotify) Notify();
          }

        // The set to invalid and
        // get invalid value
        void InvalidateData(void) {jInvalidState = true; };
        bool GetInvalidState(void) {return(jInvalidState); };

   };
//
//--------------------roValueProperty-------------------------------------
//
// roValueProperty is short for Read Only Property Variable  xxxxx
//
template <class Type>
  class roValueProperty : public CBHook
   {
      private:

      typedef Type (*GetType)(void *p);

      protected:
        void *jClassAddr;     // Address of the class to pass back up
        //
        // The next two define the getters, if jGetType != Null, then that routine
        // is called to get the value, if jGetType == Null, the pointer pV is used to
        // get the value.  This latter is the short form of the property.
        Type *pV;
        GetType jGetType;
        Type GetIt(void) const
          {
             if (jGetType==NULL) return(*pV);
             return(jGetType(jClassAddr));
          };

      public:
      // Init just sets all the variable to zero
      roValueProperty()
          {
             pV = NULL;
             jGetType = NULL;
             jClassAddr = NULL;
          };

      // This function must be called to setup the callback to accessors
      //  CA  is the class address, e.g, this
      //  SetType is the function that sets the variable
      //  GetType is the function that gets the value of the variable
      void Xetters(void *CA, GetType gfp=NULL)
         {
           jClassAddr = CA;
           jGetType = gfp;
         };

      void SXetters(void *CA, Type *V=NULL)
          {
             jClassAddr = CA;
             pV = V;
          };

      // The only operator is a conversion operator
      // Note: Even though ROPropVar is a public extension of PropVar,
      // operators do not extend.
		operator const Type() const {if (jGetType == NULL) return (*pV) ; else return(jGetType(jClassAddr));};
        Type operator + (const Type &v1) {return (Type)GetIt()+v1; };

        friend std::ostream& operator<< (std::ostream &o, const roValueProperty<Type> &avp)
        { return o << static_cast<Type>(avp); };

   };


//--------------------lValueProperty-------------------------------------
//
//  This is the template class that defines a non-arithmetic interface
// variables; e.g. only conversion and assigenment.  lValueProperty is
// Read Only Property Variable
// NOTE: Never call this class directly, use the L_PROPERTY macros

template <class Type>
  class lValueProperty : public roValueProperty<Type>
   {
      // Local typedefs, note there will not be a name conflict
      // Second, GetType must agree with the previous GetType.
      typedef Type (*GetType)(void *p);
      typedef void (*SetType)(void *p, Type Value);

      protected:
        SetType jSetType;     // If <> NULL, then sets the variable

        // This is used because stupid C++ cannot inherit assignment (=)
        Type Assign(Type &Value)
          {
            // http://gcc.gnu.org/ml/gcc-help/2005-07/msg00146.html
            // for why it's this->GetIt() instead of GetIt()
            Type v = this->GetIt();
            jSetType(this->jClassAddr, Value);

            // If the value has changed, call the callbacks
            if ( (v != Value) || this->GetInvalidState() || bAlwaysNotify)
                this->Notify();

            return(this->GetIt());
          }


      public:
        // Init just sets all the variable to zero
        lValueProperty()
          {
             jSetType = NULL;    //
             bAlwaysNotify = false;
          };

        ~lValueProperty()
          {
          };

        // This function must be called to setup the callback to accessors
        //  CA  is the class address, e.g, this
        //  SetType is the function that sets the variable
        //  GetType is the function that gets the value of the variable
        //  Xetters is the long version
        void Xetters(void *CA, GetType gfp=NULL, SetType sfp=NULL)
          {
            roValueProperty<Type>::Xetters(CA, gfp);  // Set it below
            jSetType = sfp;
          };

        // SXetters is the short version.
        void SXetters(void *CA, Type *V=NULL, SetType sfp=NULL)
          {
            roValueProperty<Type>::SXetters(CA, V);
            jSetType = sfp;
          };


        // This defines all the operators
        Type operator=(Type Value)
          {
             Assign(Value);
             return *this;
          }
        operator const Type() const {return(this->GetIt());};
        bool bAlwaysNotify;
   };

//  This is the template class that defines an arithmentic interface via
// variables; e.g. where +, ++, += are defined for the type.  aValueProperty
// is short for Arithemetic Property

// NOTE: Never call this class directly, use the PROPERTY macros
template <class Type>
  class aValueProperty  : public lValueProperty<Type>
   {
      // Local typedefs, note there will not be a name conflict
      typedef void (*SetType)(void *p, Type Value);
      typedef Type (*GetType)(void *p);


      public:
        // This defines all the operators
        Type operator=(Type Value)
          {
			 this->Assign(Value);
			 #ifdef _WIN64
			 return *this;
			 #else
			 return (Type)*this;
			 #endif
          }

        operator const Type() const {return(this->GetIt());}
        Type operator ++(int) {Type iR = this->GetIt(); jSetType(this->jClassAddr, iR+1); return(iR);}
		Type operator ++() {this->jSetType(this->jClassAddr, this->GetIt()+1); return(this->GetIt());}
		Type operator --(int) {Type iR = this->GetIt(); jSetType(this->jClassAddr, iR-1); return(iR);}
		Type operator --() {this->jSetType(this->jClassAddr, this->GetIt()-1); return(this->GetIt());}

		Type operator +=(Type Value) {this->jSetType(this->jClassAddr, this->GetIt()+Value); return(this->GetIt());}
		Type operator -=(Type Value) {this->jSetType(this->jClassAddr, this->GetIt()-Value); return(this->GetIt());}
        Type operator + (const Type &v1) {return (Type)this->GetIt()+v1; }

        friend std::ostream& operator << (std::ostream &o, const aValueProperty<Type> &avp)
        { return o << static_cast<Type>(avp); }

   };

//********************//

//------------------LPROPERTY----------------------------John Mertus---Jul 2000--
//
//  Define a lValue property
//   Type is a typedef that defines +, +=, ++ etc. such as int, real, complex
//   V1 is the property name, e.g., Width
//   G is the getter function, of the form Type Get???(void);
//   S is the setter function, of the form void Set???(Type Value);
//   CL is always the class name, e.g, test in the above.
//
//  Use with LPROPERTY_INIT in the constructor
//  This is usually used with pointers to classes.
//
#define LPROPERTY(Type, V1, G, S, CL) \
private: \
  static void _StaticL##S(void *p, Type n) {reinterpret_cast<CL *>(p)->S(n);};  \
  static Type _StaticL##G(void *p) {return(reinterpret_cast<CL *>(p)->G());}  \
public: \
   lValueProperty<Type> V1

//---------------LPROPERTY_INIT--------------------------John Mertus---Jul 2000--
//
//  Run time initialization of the property. Must be placed in the contructor
//   V1 is same property name in PROPERTY
//   G is the same Getter as in PROPERTY
//   S is the same Setter as in PROPERTY
//
#define LPROPERTY_INIT(V1, G, S) \
V1.Xetters(this, &_StaticL##G, &_Static##S)

//********************//

//------------------PROPERTY----------------------------John Mertus---Jul 2000--
//
//  Define an arithmetic property
//   Type is a typedef that defines +, +=, ++ etc. such as int, real, complex
//   V1 is the property name, e.g., Width
//   G is the getter function, of the form Type Get???(void);
//   S is the setter function, of the form void Set???(Type Value);
//   CL is always the class name, e.g, test in the above.
//
//  Use with PROPERTY_INIT in the constructor
//
#define PROPERTY(Type, V1, G, S, CL) \
private: \
  typedef void (*Set##Type##CallBack) (Type *p); \
  static void _Static##S(void *p, Type n) {reinterpret_cast<CL *>(p)->S(n);};  \
  static Type _Static##G(void *p) {return(reinterpret_cast<CL *>(p)->G());}  \
public: \
   aValueProperty<Type> V1

//---------------PROPERTY_INIT---------------------------John Mertus---Jul 2000--
//
//  Run time initialization of the property. Must be placed in the contructor
//   V1 is same property name in PROPERTY
//   G is the same Getter as in PROPERTY
//   S is the same Setter as in PROPERTY
//
#define PROPERTY_INIT(V1, G, S) \
V1.Xetters(this, &_Static##G, &_Static##S)

//******************//

//-----------------SPROPERTY----------------------------John Mertus---Jul 2000--
//
//  Define an arithmetic property, short form
//   Type is a typedef that defines +, +=, ++ etc. such as int, real, complex
//   V1 is the property name, e.g., Width
//   FV is a private variable of type Type;
//   S is the setter function, of the form void Set???(Type Value);
//   CL is always the class name, e.g, test in the above.
//
//  Use with SPROPERTY_INIT in the constructor
//
#define SPROPERTY(Type, V1, FV, S, CL) \
private: \
  static void _SStatic##S(void *p, Type n) {reinterpret_cast<CL *>(p)->S(n);};  \
public: \
  aValueProperty<Type> V1

//---------------SPROPERTY_INIT--------------------------John Mertus---Jul 2000--
//
//  Run time initialization of the property. Must be placed in the contructor
//   V1 is same property name in PROPERTY
//   FV is the same Type Variable as in PROPERTY
//   S is the same Setter as in PROPERTY
//
#define SPROPERTY_INIT(V1, FV, S) \
V1.SXetters(this, &FV, &_SStatic##S)

//******************//

//---------------RO_PROPERTY----------------------------John Mertus---Jul 2000--
//
//  Define a Read Only arithmetic property
//   Type is a typedef that defines +, +=, ++ etc. such as int, real, complex
//   V1 is the property name, e.g., Width
//   G is the getter function, of the form Type Get???(void);
//   CL is always the class name, e.g, test in the above.
//
//  Use with RO_PROPERTY_INIT in the constructor
//
#define RO_PROPERTY(Type, V1, G, CL) \
private: \
  static Type _ROStatic##G(void *p) {return(reinterpret_cast<CL *>(p)->G());}  \
public: \
   roValueProperty<Type> V1

//---------------RO_PROPERTY_INIT------------------------John Mertus---Jul 2000--
//
//  Run time initialization of the property. Must be placed in the contructor
//   V1 is same property name in PROPERTY
//   G is the same Getter as in PROPERTY
//
#define  RO_PROPERTY_INIT(V1, G) \
V1.Xetters(this, &_ROStatic##G);


//******************//

//---------------RO_SPROPERTY----------------------------John Mertus---Jul 2000--
//
//  Define a Read Only arithmetic property, short form
//   Type is a typedef that defines +, +=, ++ etc. such as int, real, complex
//   V1 is the property name, e.g., Width
//   FV is a private variable of type Type;
//   CL is always the class name, e.g, test in the above.
//
//  Use with RO_SPROPERTY_INIT in the constructor
//
#define RO_SPROPERTY(Type, V1, G, CL) \
public: \
   roValueProperty<Type> V1

//---------------RO_SPROPERTY_INIT-----------------------John Mertus---Jul 2000--
//
//  Run time initialization of the property. Must be placed in the contructor
//   V1 is same property name as in PROPERTY
//   FV is the same Type Variable as in PROPERTY
//
#define RO_SPROPERTY_INIT(V1, FV) \
  V1.SXetters(this, &FV)

//---------------DEFINE_GENERIC_CBHOOK----------------------John Mertus---Jul 2000--
//
//  This is used by an external class to set the call back to that class
//   V1 is same property name as in PROPERTY
//   CB is a method of the form void CB(Type *Value)
#define DEFINE_GENERIC_CBHOOK(V1, CL)\
  void V1##ChangeCB(void *p); \
  static void _Static##V1##ChangeCB(void *t, void *p) {reinterpret_cast<CL *>(t)->V1##ChangeCB(p); }

//---------------DEFINE_CBHOOK-----------------------John Mertus---Jul 2000--
//
//  This is used by an external class to set the call back to that class
//   V1 is same property name as in PROPERTY
//   CB is a method of the form void CB(Type *Value)
#define DEFINE_CBHOOK(CBName, CL) \
  void CBName(void *p); \
  static void _Static##CBName(void *t, void *p) {reinterpret_cast<CL *>(t)->CBName(p); }

//---------------SET_GENERIC_CBHOOK-----------------------John Mertus---Jul 2000--
//
//  This is used by an external class to set the call back to that class
//   V1 is same property name as in PROPERTY
//
#define SET_GENERIC_CBHOOK(V1) \
  V1.SetCallBack(this, _Static##V1##ChangeCB, &V1);

//---------------SET_CBHOOK-------------------------John Mertus---Jul 2000--
//
//  This is used by an external class to set the call back to that class
//   V1 is same property name as in PROPERTY
//
#define SET_CBHOOK(CBName, V1) \
  V1.SetCallBack(this, _Static##CBName, &V1)

//---------------REPLACE_CBHOOK---------------------John Mertus---Jul 2000--
//
//  This replaces a
//   V1 is same property name as in PROPERTY
//
#define REPLACE_CBHOOK(CBName, V1) \
  V1->RemoveCallBack(this, _Static##CBName); \
  V1.SetCallBack(this, _Static##CBName, &V1)

//---------------SET_CBHOOK_DATA-------------------John Mertus---Jul 2000--
//
//  This is used by an external class to set the call back to the class
//  along with a different data pointer.
//   CBName is the callback name, V1 the property and D a pointer to
//   the desired Data in the callback.
//
#define SET_CBHOOK_DATA(CBName, V1, D) \
  V1.SetCallBack(this, _Static##CBName, D)

//---------------SET_CBHOOK_POINTER--------------John Mertus---Jul 2000--
//
//  Same as SET_CBHOOK except
//   V1 is same property name as in SET_CBHOOK execept V1 is a pointer
//
#define SET_CBHOOK_POINTER(CBName, V1) \
  V1->SetCallBack(this, _Static##CBName, V1)

#define SET_CBHOOK_POINTER_DATA(CBName, V1, D) \
  V1->SetCallBack(this, _Static##CBName, D)

//---------------SET_GENERIC_CBHOOK_POINTER------------John Mertus---Jul 2000--
//
//  Same as SET_GENERIC_CBHOOK except
//   V1 is same property name as in SET_PROPERTY_CB execept V1 is a pointer
//
#define SET_GENERIC_CBHOOK_POINTER(V1) \
  V1->SetCallBack(this, _Static##V1##ChangeCB, V1)

//---------------REMOVE_CBHOOK-----------------------John Mertus---Jul 2000--
//
//  This is used by an external class to set the call back to that class
//   V1 is same property name as in PROPERTY
//
#define REMOVE_CBHOOK(CBName, V1) \
  V1.RemoveCallBack(this, _Static##CBName);

#define REMOVE_CBHOOK_POINTER(CBName, V1) \
  V1->RemoveCallBack(this, _Static##CBName);

//---------------REMOVE_ALL_CBHOOKS------------------John Mertus---Jul 2000--
//
//  This is used by an external class to set the call back to that class
//   V1 is same property name as in PROPERTY
//
#define REMOVE_ALL_CBHOOKS(V1) \
  V1.RemoveAllCallBacks(this)

//--------------------ACBHook--------------------------John Mertus---Jul 2003--
//
//  This uses the above to produce a callback for an arithmetic operator
//  that is ones that understand +, -, 0 and 1
//
template <class Type> class ACBHook : public CBHook
{
    public:
       ACBHook(void) {_Value = 0;}
       ACBHook(const ACBHook &ip) {Assign(ip);}
       ACBHook &operator=(const ACBHook &ip) {return Assign(ip);}

       ACBHook(const Type &i) {Assign(i);}
       ACBHook &operator=(const Type &i) {return Assign(i);}
       ACBHook operator ++() {return Assign(_Value + 1);}
       ACBHook operator ++(int) {Type iR = _Value; Assign(_Value + 1); return iR;}
       ACBHook operator --() {return Assign(_Value - 1);}
       ACBHook operator --(int) {Type iR = _Value; Assign(_Value - 1); return iR;}
       ACBHook operator +=(ACBHook Value) {return Assign(_Value + Value);}
       ACBHook operator -=(ACBHook Value) {return Assign(_Value - Value);}
       ACBHook operator + (const Type &v1) {return _Value+v1; };

       operator Type() const {return _Value;}

       friend std::ostream& operator << (std::ostream &o,  const ACBHook &avp)
                       { return o << (Type)avp; }

    private:
       // This forces the actual callback
       ACBHook &Assign(const Type &in)
        {
           if (in == _Value) return *this;
           _Value = in;
           Notify();
           return *this;
        }
       Type _Value;
};

// Do the type defs
typedef ACBHook<int> intP;
typedef ACBHook<double> doubleP;
typedef ACBHook<float> floatP;
typedef ACBHook<unsigned> unsignedP;
typedef ACBHook<unsigned char> byteP;


#ifdef _MSC_VER
#pragma warning(pop)
#endif

#endif

