﻿// ---------------------------------------------------------------------------

#pragma hdrstop

#include "MtiMkl.h"
#include "mkl.h"
#include <stdint.h>
#include "IniFile.h"

// ---------------------------------------------------------------------------
#pragma package(smart_init)
// extern "C"
// {
// void mkl_lapack_sgesv(int64_t* n, int64_t* nrhs, float* a, int64_t* lda, int64_t* ipiv, float* b, int64_t* ldb,
// int64_t* info);
// }

const char noTranspose = 'N';

namespace MtiMkl
{
	int lubksb(float *a, int n, int *ipiv, float *b)
	{
		lapack_int n_mkl = n;
		lapack_int nrhs_mkl = 1;
		lapack_int lda_mkl = n;
		lapack_int ldb_mkl = n;
		lapack_int info_mlk = -1000;
		lapack_int ipiv_mkl[n];

		// strictly this is not necessary unless we are an IPL64 interface
		for (auto i = 0; i < n; i++)
		{
			ipiv_mkl[i] = ipiv[i];
		}

		// Solve for B, replacing B. One set of sulutions for each column of B.
		return LAPACKE_sgetrs(LAPACK_COL_MAJOR, noTranspose, n_mkl, nrhs_mkl, a, lda_mkl, ipiv_mkl, b, ldb_mkl);
	}

	int lubksb(double *a, int n, int *ipiv, double *b)
	{
		lapack_int n_mkl = n;
		lapack_int nrhs_mkl = 1;
		lapack_int lda_mkl = n;
		lapack_int ldb_mkl = n;
		lapack_int info_mlk = -1000;
		lapack_int ipiv_mkl[n];

		// strictly this is not necessary unless we are an IPL64 interface
		for (auto i = 0; i < n; i++)
		{
			ipiv_mkl[i] = ipiv[i];
		}

		// Solve for B, replacing B. One set of sulutions for each column of B.
		return LAPACKE_dgetrs(LAPACK_COL_MAJOR, noTranspose, n_mkl, nrhs_mkl, a, lda_mkl, ipiv_mkl, b, ldb_mkl);
	}

	int ludcmp(int n, float* a, int lda, int *ipiv)
	{
		// Create the ILP64 interface
		lapack_int n_mkl = n;
		lapack_int ipiv_mkl[n];
		lapack_int lda_mkl = lda;

		auto info_mlk = LAPACKE_sgetrf(LAPACK_COL_MAJOR, n_mkl, n_mkl, a, lda_mkl, ipiv_mkl);

		// strictly this is not necessary unless we are an IPL64 interface
		for (auto i = 0; i < n; i++)
		{
			ipiv[i] = ipiv_mkl[i];
		}

		return info_mlk;
	}

	int ludcmp(int n, double* a, int lda, int *ipiv)
	{
		// Create the ILP64 interface
		lapack_int n_mkl = n;
		lapack_int ipiv_mkl[n];
		lapack_int lda_mkl = lda;

		auto info_mlk = LAPACKE_dgetrf(LAPACK_COL_MAJOR, n_mkl, n_mkl, a, lda_mkl, ipiv_mkl);

		// strictly this is not necessary unless we are an IPL64 interface
		for (auto i = 0; i < n; i++)
		{
			ipiv[i] = ipiv_mkl[i];
		}

		return info_mlk;
	}

	float ULogrithmic(float X0, float Y0, float X1, float Y1)
	{
		float r0 = (X0 - X1);
		float r1 = (Y0 - Y1);
		float r = std::sqrt(r0 * r0 + r1 * r1);
		if (r == 0)
			return 0;

		if (r <= 10E-100)
			r = 10E-100;
		return r*std::log(r);
	}

	bool SolveForWeights(const vector<IppiPoint_32f> &tpS, const vector<IppiPoint_32f> &tpO, vector<double> &Wx,
		 vector<double> &Wy)
	{
		if (tpO.size() != tpS.size())
		{
			TRACE_0(errout << "Internal Error: Size of points do not match");
			return false;
		}

		// Building the matrices for solving the L*(w|a)=(v|0) problem with L={[K|P];[P'|0]}
		// Building K and P (Need to build L)
		auto N0 = (int)tpO.size();
		auto N3 = N0 + 3;
		vector<double>LU(N3*N3);

		auto sub2ind = [N3](int i, int j)
		{
			return i*N3 + j;
		};

		for (auto i = 0; i < N0; i++)
		{
			for (int j = 0; j < N0; j++)
			{
				if (i == j)
				{
					LU[sub2ind(i, j)] = 0;
				}
				else
				{
					auto d = ULogrithmic(tpS[i].x, tpS[i].y, tpS[j].x, tpS[j].y);
					LU[sub2ind(i, j)] = (float)d;
				}
			}

			LU[sub2ind(i, N0 + 0)] = 1;
			LU[sub2ind(i, N0 + 1)] = tpS[i].x;
			LU[sub2ind(i, N0 + 2)] = tpS[i].y;

			LU[sub2ind(N0 + 0, i)] = LU[sub2ind(i, N0 + 0)];
			LU[sub2ind(N0 + 1, i)] = LU[sub2ind(i, N0 + 1)];
			LU[sub2ind(N0 + 2, i)] = LU[sub2ind(i, N0 + 2)];
		}

		for (auto i = 0; i < 3; i++)
		{
			LU[sub2ind(N0 + 0, N0 + i)] = 0;
			LU[sub2ind(N0 + 1, N0 + i)] = 0;
			LU[sub2ind(N0 + 2, N0 + i)] = 0;
		}

		// Find the results
		Wx.resize(N3);
		Wy.resize(N3);

		for (int i = 0; i < N0; i++)
		{
			Wx[i] = tpO[i].x;
			Wy[i] = tpO[i].y;
		}

		for (int i = N0; i < N3; i++)
		{
			Wx[i] = 0;
			Wy[i] = 0;
		}

		// Now solve for the weights
		vector<int>mvIndex(N3);
		double d;

		// Decompose the matrix
		if (MtiMkl::ludcmp(N3, LU.data(), N3, mvIndex.data()) != 0)
		{
			TRACE_0(errout << "Matrix inversion failed");
			return false;
		}

		MtiMkl::lubksb(LU.data(), N3, mvIndex.data(), Wx.data());
		MtiMkl::lubksb(LU.data(), N3, mvIndex.data(), Wy.data());
		return true;
	}
}
