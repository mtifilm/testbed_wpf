// ---------------------------------------------------------------------------

#ifndef MtiMklH
#define MtiMklH
// ---------------------------------------------------------------------------
#include "corecodelibint.h"
#include "ippcore.h"
#include <vector>

namespace MtiMkl
{
   float MTI_CORECODELIB_API ULogrithmic(float X0, float Y0, float X1, float Y1);
	int MTI_CORECODELIB_API lubksb(float *a, int n, int *indx, float *b);
	int MTI_CORECODELIB_API ludcmp(int n, float* a, int lda, int *ipiv);

	int MTI_CORECODELIB_API lubksb(double *a, int n, int *indx, double *b);
	int MTI_CORECODELIB_API ludcmp(int n, double* a, int lda, int *ipiv);

   // NOTE: this uses the r*log(r) distance kernel
	bool MTI_CORECODELIB_API SolveForWeights
   (
   	const std::vector<IppiPoint_32f>&tpS,
      const std::vector<IppiPoint_32f>&tpO,
   	std::vector<double>&Wx,
		std::vector<double>&Wy
   );
}

#endif
