/* Declarations for getopt. */
#ifndef GETOPT_H
#define GETOPT_H

#include "corecodelibint.h"
#ifdef __cplusplus
extern "C" {
#endif
extern MTI_CORECODELIB_API	int   opterr,		/* if error message should be printed */
			optind,		/* index into parent argv vector */
			optopt,		/* character checked for validity */
			optreset;	/* reset getopt */
extern MTI_CORECODELIB_API	const char   *optarg;	/* argument associated with option */

#define	BADCH	(int)'?'
#define	BADARG	(int)':'
#define	EMSG	""
#include "corecodelibint.h"
MTI_CORECODELIB_API int getopt(int nargc, char * const *nargv, const char *ostr);
#ifdef __cplusplus
};
#endif
#endif
