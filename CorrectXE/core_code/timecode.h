#ifndef TimecodeH
#define TimecodeH

//
// interface file for class CTimecode
//
// this is a NEW TIMECODE begun by KT on 2/14/01

#include "corecodelibint.h"
#include <iostream>
#include "machine.h"
#include "MTIstringstream.h"

#define MAXFRAMES (100*3600*60-1)

class MTI_CORECODELIB_API CTimecode
{

private:

   // Time is always stored here as ABSOLUTE
   // FRAMES. When the frames-per-second is
   // non-zero, the ASCII representation is
   // of the four-element type. When the
   // frames-per-second is zero, the ASCII
   // is an 8-digit integer.

   MTI_UINT32 frames;

   // always clear whenever frames-per-second
   // is 0

#define DROPFRAME 0x01
#define FRAME720P 0x02
#define HALFFRAME 0x04
#define TC_FIELDFLAG 0x08   // Set for field 2, cleared for field 1

#define TC_FLAG_MASK (~(HALFFRAME | TC_FIELDFLAG))// Masks flags we want to
                                                  // ignore for most operations

   MTI_UINT8  flags;

   // When frames-per-second is 0, the timecode
   // is only interpreted in ABSOLUTE MODE. In
   // that case dropFrame is forced to be zero.

   MTI_UINT8  framesPerSecond;

   // pad out the record to 64-bits
   MTI_UINT8 dum[2];

public:
/////////////////////////////////////////////////////////////////
//
//		C O N S T R U C T O R S
//
/////////////////////////////////////////////////////////////////

   CTimecode(int frames);	// to create a film version time code
				// usually initialize with frames=0

   CTimecode(int flgs, int framesPerSecond);	// to create a video time code
				// non-zero framesPerSecond and
				// see DROPFRAME, FRAME720P, HALFFRAME for flgs

   CTimecode(int hrs, int mins, int secs, int frms, int flgs, int framesPerSecond);
		//  to create a particular instance of a time code.

   CTimecode ();	// will set frames to zero.

   virtual ~CTimecode();


/////////////////////////////////////////////////////////////////
//
//		M U T A T O R S
//
/////////////////////////////////////////////////////////////////

   void setAbsoluteTime(int frms);
	/*  NOTE:  Absolute time does not respect Drop Frame flag
	To set a particular frame count use setFrameCount(int frms) */

   void setFrameCount(int iFrameCount);
        /* this sets the time be iFrameCount frames after 00:00:00:00
	It respects Drop Frame */

   void setTime(int hrs,int mins,int secs,int frms);

   bool setTimeASCII(const char *timestr);

   void setHours(int hrs);

   void setMinutes(int mins);

   void setSeconds(int secs);

   void setFrames(int frms);

   void setFlags(int flgs);
   void setDropFrame(bool value);
   void setHalfFrame(bool value);
   void set720P(bool value);
   void setField(bool newField);

   void setFramesPerSecond(int framesPerSecond);

/////////////////////////////////////////////////////////////////
//
//		A C C E S S O R S
//
/////////////////////////////////////////////////////////////////

   int getAbsoluteTime() const;
	/*  NOTE:  Absolute time does not respect Drop Frame flag
	To set a particular frame count use setFrameCount(int frms) */

   int getFrameCount () const;
       /* this returns the frame count from 00:00:00:00.  It respects
       the drop frame status */ 

   void getTimeASCII(char *timechr) const;

   void getTimeString(string &timestr) const;

   int getHours() const;

   int getMinutes() const;

   int getSeconds() const;

   int getFrames() const;

   int getFlags() const;
   bool isDropFrame() const;
   bool isHalfFrame() const;
   bool is720P() const;
   bool getField() const;     // false for field 1, true for field 2

   int getFramesPerSecond() const;

   int absoluteTime() const;

  /* 
   *  ASCII2FrameCount and FrameCount2ASCII are use to translate
   *  between ASCII and FrameCount.  The FrameCount value respects
   *  the drop frame status.
   */

   int ASCII2FrameCount(const char *timechr) const;
   void FrameCount2ASCII(char *timechr, int iFrameCount) const;

   // convertTimecode creates an equivalent CTimecode with another
   // frame per second.  Hours, Minutes and Seconds remain the same,
   // but Frames is converted
   CTimecode convertTimecode(int newFramesPerSecond, bool newDropFrame);

/////////////////////////////////////////////////////////////////
//
//		O P E R A T O R S
//
/////////////////////////////////////////////////////////////////

public:
   // override the "<<" operator to print CTIMECODEs
   friend MTI_CORECODELIB_API ostream&
	  operator<<(ostream& os, const CTimecode& tc);
#ifdef _WIN64
   friend MTI_CORECODELIB_API MTIostringstream&
      operator<<(MTIostringstream& os, const CTimecode& tc);
#endif

   // override the "++" operator
   friend MTI_CORECODELIB_API const CTimecode&
      operator++(CTimecode&);     // prefix
   friend MTI_CORECODELIB_API const CTimecode
      operator++(CTimecode&, int); // postfix

   // override the "--" operator
   friend MTI_CORECODELIB_API const CTimecode&
      operator--(CTimecode&);     // prefix
   friend MTI_CORECODELIB_API const CTimecode
      operator--(CTimecode&, int); // postfix

   // override the "-" operator to get the
   // difference in frames of two timecodes
   friend MTI_CORECODELIB_API int
      operator-(const CTimecode&,const CTimecode&); 

   // override the "+" operator to get a timecode
   // from a timecode + a framecount 
   friend MTI_CORECODELIB_API CTimecode
      operator+(const CTimecode&, int);

   // override the "==" operator to compare timecodes
   friend MTI_CORECODELIB_API bool
      operator==(const CTimecode&, const CTimecode&);

   // overide the "!=" operator to compare timecodes
   friend MTI_CORECODELIB_API bool
      operator!=(const CTimecode&, const CTimecode&);

   // overide the "<" operator to compare timecodes (ignoring half-frame flag)
   friend MTI_CORECODELIB_API bool
      operator<(const CTimecode&, const CTimecode&);

   // overide the "<=" operator to compare timecodes (ignoring half-frame flag)
   friend MTI_CORECODELIB_API bool
      operator<=(const CTimecode&, const CTimecode&);

   // overide the ">" operator to compare timecodes (ignoring half-frame flag)
   friend MTI_CORECODELIB_API bool
      operator>(const CTimecode&, const CTimecode&);

   // overide the ">=" operator to compare timecodes (ignoring half-frame flag)
   friend MTI_CORECODELIB_API bool
      operator>=(const CTimecode&, const CTimecode&);

/////////////////////////////////////////////////////////////////
//
// A handy constant to initialize CTimecodes to something that
// says it's uninitialized. Usage example:
//
// Constructor of object of type CFoo containing timecode m_StartTime:
//
//    CFoo::CFoo() : m_StartTime(CTimecode::NOT_SET) {};
//
// then in some method...
//
//    if (m_StartTime == CTimecode::NOT_SET) {
//          ...whatever...
//    }
//
/////////////////////////////////////////////////////////////////
public:
   static const CTimecode NOT_SET;

protected:
	void validateTimecode();

private:
/////////////////////////////////////////////////////////////////
//
//		I N T E R N A L   F U N C T I O N S
//		
/////////////////////////////////////////////////////////////////


	void incrementTimecode();

	void decrementTimecode();

   // Comparison helper functions
   bool equalTo(const CTimecode &rhs) const;
   bool lessThan(const CTimecode &rhs) const;
   bool greaterThan(const CTimecode &rhs) const;

};

void TestTimecode();

#endif


