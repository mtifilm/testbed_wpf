#include <stdio.h>
#include "mcs_support.h"
#include "pipe_support.h"
#include "mcs.h"

// Forward Declarations
void _ProcessBoxDataCallback(XtPointer  clientData,
                            int       *ipFDIn,
                            XtInputId *pXtInputID);
void MCSInitCallbacks(void);

// Local Global
static CMCS *theMCSClass = NULL;

//
// These are outside of the class
//
int MCSConnect(void)
{
  if (theMCSClass != NULL)
    return(1);
  else
    theMCSClass = new CMCS;

  if (theMCSClass->bInitialized)
  {
    MCSInitCallbacks();
    return(1);
  }
  else
  {
    delete theMCSClass;
    theMCSClass = NULL;
    return(0);
  }
}

int MCSAppAddInput(XtAppContext xtAC)
{
  if (theMCSClass == NULL)
    return(-1);
  if (!theMCSClass->bInitialized)
    return(-2);

  theMCSClass->xtInputID = XtAppAddInput(xtAC,
                                 theMCSClass->Get_iFDPipeFromDaemon(),
                                 (XtPointer) XtInputReadMask,
                                 (XtInputCallbackProc) _ProcessBoxDataCallback,
                                 NULL);
  return(theMCSClass->xtInputID);
}

int MCSSetJogWheelPosition(int iNewWheelPosition)
{
  if (theMCSClass == NULL)
    return(-1);
  if (!theMCSClass->bInitialized)
    return(-2);

  theMCSClass->SetJogWheelPosition(iNewWheelPosition);
  return(0);
}

int MCSGetJogWheelPosition(void)
{
  // TTT - need to do something with -1 & -2 since these are potentially
  //       valid return values

  if (theMCSClass == NULL)
    return(-1);
  if (!theMCSClass->bInitialized)
    return(-2);

  return(theMCSClass->GetJogWheelPosition());
}

void _ProcessBoxDataCallback(XtPointer  clientData,
                            int       *ipFDIn,
                            XtInputId *pXtInputID)
{
  theMCSClass->ProcessBoxDataCallback(clientData, ipFDIn, pXtInputID);
}

void MCSInitCallbacks(void)
{
  int iButton;

  // Zero the Button Callbacks
  for (iButton = 0; iButton <= MAX_BUTTON_NUM; iButton++)
  {
    theMCSClass->AddCallback(MCS_BUTTON, iButton, MCS_PRESS,   NULL, NULL);
    theMCSClass->AddCallback(MCS_BUTTON, iButton, MCS_RELEASE, NULL, NULL);
  }

  // Zero the Jog Wheel Callback
  theMCSClass->AddCallback(MCS_JOG_WHEEL, 0, 0, NULL, NULL);

  // Zero the Shuttle Ring Callback
  theMCSClass->AddCallback(MCS_SHUTTLE_RING, 0, 0, NULL, NULL);
}

int MCSAddCallback(int iType,
                   int iButtonWheel,
                   int iPressRelease,
                   XtCallbackProc xtCallback,
                   XtPointer      xtData)
{
  // Check that valid iType - currently MCS_BUTTON, MCS_JOG_WHEEL
  //   or MCS_SHUTTLE_RING
  if ((iType != MCS_BUTTON) && (iType != MCS_JOG_WHEEL)
      && (iType != MCS_SHUTTLE_RING))
  {
    fprintf(stderr,
         "Illegal Callback Type Number: iType = %d, must be = %d, %d or %d\n",
         iType, MCS_BUTTON, MCS_JOG_WHEEL, MCS_SHUTTLE_RING);
    return(-1);
  }

  // If button, then make sure valid button
  if ((iType == MCS_BUTTON)
      && ((iButtonWheel < MIN_BUTTON_NUM) || (iButtonWheel > MAX_BUTTON_NUM)))
  {
    fprintf(stderr,
            "Illegal Callback Setting: Button = %d, MAX = %d, MIN = %d\n",
            iButtonWheel, MIN_BUTTON_NUM, MAX_BUTTON_NUM);
    return(-1);
  }

  // If button, and valid button number, then make sure Press or Release
  if ((iType == MCS_BUTTON)
      && ((iButtonWheel >= MIN_BUTTON_NUM) && (iButtonWheel <= MAX_BUTTON_NUM))
      &&  ((iPressRelease != MCS_PRESS) && (iPressRelease != MCS_RELEASE)))
  {
    fprintf(stderr,
        "Illegal Callback Setting: Button Press/Release %d, must be %d or %d\n",
            iPressRelease, MCS_PRESS, MCS_RELEASE);
    return(-1);
  }

  if ((iType == MCS_JOG_WHEEL)
      && ((iButtonWheel != 0) || (iPressRelease != 0)))
  {
    fprintf(stderr,
            "Illegal Callback Setting: Jog Wheel = %d %d, must be = 0 0\n",
            iButtonWheel, iPressRelease);
    return(-1);
  }

  if ((iType == MCS_SHUTTLE_RING)
      && ((iButtonWheel != 0) || (iPressRelease != 0)))
  {
    fprintf(stderr,
            "Illegal Callback Setting: Shuttle Ring = %d %d, must be = 0 0\n",
            iButtonWheel, iPressRelease);
    return(-1);
  }

//printf("TTT adding callback %d %d %d\n", iType, iButtonWheel, iPressRelease);
  theMCSClass->AddCallback(iType, iButtonWheel, iPressRelease,
                           xtCallback, xtData);
  return(1);
}

//
// These are inside of the class
//
class CMCSDestroy
{
  public:
    ~CMCSDestroy()
    {
      delete theMCSClass;
      theMCSClass = NULL;
    }
};

static CMCSDestroy MCSDestroy;

//
// Class Constructor.
//
CMCS::CMCS(void)
{
    bInitialized = false;

    stMyData.i = 22;
    stMyData.j = 43;

//    _default_callback_data.obj =         (void *) this;
//    _default_callback_data.client_data = (void *) &stMyData;

    if ((iFDPipeToDaemon = OpenPipeToDaemon()) == -1)
    {
      fprintf(stderr, "*** mcsd apparently not running\n");
      return;
    }

    SendToDaemon("HELO");

    if ((iFDPipeFromDaemon = OpenPipeFromDaemon()) == -1)
    {
      fprintf(stderr, "*** mcsd didn't open pipe back to us\n");
      return;
    }

    iJogWheelPosition = 0;

    bInitialized = true;
}

//
// Minimal Destructor.
//
CMCS::~CMCS() 
{
  // Nothing for now
  // TTT - Do closes eventually
}

void CMCS::ProcessBoxDataCallback(XtPointer  clientData,
                                  int       *ipFDIn,
                                  XtInputId *pXtInputID)
{
//  int *ip;

  //printf("\nTTT ******************************\n");
  //printf("TTT Entered ProcessBoxDataCallback\n");
//    VkCallbackStruct *data = (VkCallbackStruct *)clientData;
//printf("TTT data         0x%x\n", data);
//printf("TTT obj          0x%x\n", data->obj);
//printf("TTT client_data  0x%x\n", data->client_data);
//ip = (int *) data->client_data;
//printf("TTT pre  client_data  %d\n", *ip);
//printf("TTT FD           %d\n", *ipFDIn);
//printf("TTT InID         %d\n", *pXtInputID);
//    JLCooperWindow *obj = (JLCooperWindow *)data->obj;

//    obj->ProcessBoxData((XtPointer)data->client_data, ipFDIn, pXtInputID);
  if (theMCSClass == NULL)
  {
    fprintf(stderr, "MCS class not allocated\n");
    return;
  }

  if (!theMCSClass->bInitialized)
  {
    fprintf(stderr, "MCS class not initialized\n");
    return;
  }

  theMCSClass->ProcessBoxData(NULL, ipFDIn, pXtInputID);

//printf("TTT post client_data  %d\n", *ip);
  //printf("TTT done ProcessBoxDataCallback\n");
  //printf("TTT ******************************\n");
}

void CMCS::AddCallback(int iType,
                       int iButtonWheel,
                       int iPressRelease,
                       XtCallbackProc xtCallback,
                       XtPointer      xtData)
{
//printf("xtCallback %x\n", xtCallback);
//printf("xtData     %x\n", xtData);
  scbCallBack[iType][iButtonWheel][iPressRelease].funcCallBack = xtCallback;
  scbCallBack[iType][iButtonWheel][iPressRelease].funcData     = xtData;
}

int CMCS::Get_iFDPipeFromDaemon()
{
  return(iFDPipeFromDaemon);
}

int CMCS::GetJogWheelPosition()
{
  return(iJogWheelPosition);
}

int CMCS::SetJogWheelPosition(int iNewWheelPosition)
{
  iJogWheelPosition = iNewWheelPosition;
  //printf("Set wheel position to %d\n", iJogWheelPosition);
  return(0);	// TTT - for now always succeed
}

void CMCS::ProcessBoxData(XtPointer client_data,
                    int       *iFDIn,
                    XtInputId *pXtInputID)
{
  char        caLine[MAX_LINE_LEN];
  char        caLine2[MAX_LINE_LEN];
  bool        bKeepReadingF;			// Not used here
//  int        *iCD = (int *) client_data;
//  int         i;
  int         iType, iButtonWheel, iPressRelease;
  int         iType2;
  int         iChange;
  int         iRet;
  signed char scChange;
  bool        bDone;
  int         iCommandsProcessed;

  iRet = ProcessData(&iFDPipeFromDaemon, &bKeepReadingF, caLine);
  iCommandsProcessed = 1;

  if (iFDPipeFromDaemon == -1)		/* mcsd died */
  {
    fprintf(stderr, "*** mcsd daemon dead.\n");
    bInitialized = false;
    XtRemoveInput(theMCSClass->xtInputID);
    return;
  }

  //printf("TTT got\n");
//for (i=0; i<7; i++)
  //printf(" %d", caLine[i]);
  //printf("\nTTT got %s\n", caLine);
  if (caLine[0] != SENDER_MCSD)
  {
    fprintf(stderr, "Reply not from MCSD\n");
    return;
  }

  iType         = caLine[1];
  iButtonWheel  = caLine[2];
  iPressRelease = caLine[3];

  if (iType == MCS_DAEMON_CMD)
  {
    printf("Got Daemon Reply: %s", caLine + 2);
    return;
  }

  if (iType == MCS_JOG_WHEEL)
  {
    scChange = (signed char) caLine[5];		// Two steps to get sign right
    iChange = (int) scChange;
    //printf("TTT Change = %d\n", iChange);
    iJogWheelPosition += iChange;
    //printf("TTT iJogWheelPosition = %d\n", iJogWheelPosition);

    bDone = FALSE;

    while (!bDone)
    {
      iRet = ProcessData(&iFDPipeFromDaemon, &bKeepReadingF, caLine2);

      if (iRet == -1)		// Nothing more to read
      {
        bDone = TRUE;
      }
      else
      {
        if (iFDPipeFromDaemon == -1)		/* mcsd died */
        {
          fprintf(stderr, "*** mcsd daemon dead.\n");
          bInitialized = false;
          XtRemoveInput(theMCSClass->xtInputID);
          return;
        }

        if (caLine2[0] != SENDER_MCSD)
        {
          fprintf(stderr, "Reply not from MCSD\n");
          return;
        }

        iType2 = caLine2[1];

        if (iType2 != MCS_JOG_WHEEL)
        {
          printf("TTT - got unexpected non-jog wheel event\n");
          bDone = TRUE;
        }
        else
        {
          iCommandsProcessed++;
          scChange = (signed char) caLine2[5];	// Two steps to get sign right
          iChange = (int) scChange;
          iJogWheelPosition += iChange;
        }
      }
    }
  }

  if (scbCallBack[iType][iButtonWheel][iPressRelease].funcCallBack != NULL)
  {
    //printf("TTT calling %d %d %d callback\n", iType, iButtonWheel, iPressRelease);
    scbCallBack[iType][iButtonWheel][iPressRelease].funcCallBack(NULL,
             scbCallBack[iType][iButtonWheel][iPressRelease].funcData,
             NULL);
  }
  //else
   // printf("No callback for: %d %d %d\n", iType, iButtonWheel, iPressRelease);

}
