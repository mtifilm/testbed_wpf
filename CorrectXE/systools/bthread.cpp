/*
 File:  bthread.c
 By:    Kevin Manbeck
 Date:  April 1, 2001

 The bthread code based on the self-blocking properties of the IRIX
 sproc() function.

 See bthread.h for full documentation.
 */

#include "bthread.h"

#include "HRTimer.h"
#include "IniFile.h" // for TRACE_N... uggh
#include "SysInfo.h"

#include <process.h>
#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
#include "Ippheaders.h"

struct BTHREAD_STRUCT;
struct BTHREADPOOL;
struct BTHREADPOOL_THREAD_LIST;

static BTHREADPOOL *TheThreadPool;

static BTHREADPOOL *BThreadPoolCreate(int lowWater, int highWater);
static BTHREAD_STRUCT *BThreadPoolAddThread(BTHREADPOOL *threadPool);
static void BThreadPoolRemoveThread(BTHREADPOOL *threadPool, BTHREAD_STRUCT *thread);
static BTHREAD_STRUCT *BThreadPoolGetIdleThread(BTHREADPOOL *threadPool);
static BTHREADPOOL_THREAD_LIST *BThreadPoolCreateThreadList();
static void BThreadPoolSetThreadIdle(BTHREADPOOL *threadPool, BTHREAD_STRUCT *thread);

// A structure that contains the information needed to call
// the user's task function.
struct BTHREAD_TASK
{
   // Pointer to the callback function.
   void(*AppFcn)(void *vpAppData, void *vpReserved) = nullptr;

   // User-defined data associated with the thread.
   void *vpAppData = nullptr;

   // Thread ID data.
   void *vpReserved = nullptr;

   // "Begin" semaphore.
	void *beginSemaphore = nullptr;

	// Caller finename and line number (for debugging)
	char callerFilePathForDebugging[100];
	int callerLineNumberForDebugging;

   BTHREAD_TASK()
   {
      beginSemaphore = SemAlloc();
      callerFilePathForDebugging[0] = 0;
      callerLineNumberForDebugging = 0;
   }

   ~BTHREAD_TASK()
   {
      SemFree(beginSemaphore);
   }
};

typedef std::shared_ptr<BTHREAD_TASK> BTHREAD_TASK_SHARED_PTR;

//////////////////////////////////////
// BTHREADPOOL STRUCTURES
//////////////////////////////////////

// The per-thread tracking structure.
struct BTHREAD_STRUCT
{
   // Windows thread handle.
   HANDLE hChildThread = INVALID_HANDLE_VALUE;

   // Use this to suspend and resume the thread.
   void *suspendSemaphore = nullptr;

   // If non-NULL: the task this thread isperforming.
   BTHREAD_TASK_SHARED_PTR task;

   // If non-NULL: the thread pool this thread belongs to.
   BTHREADPOOL *threadPool = nullptr;

   // The thread waits on this semaphore while idle.
   void *idleSemaphore = nullptr;

   // The "busy" flag.
   bool isBusy = false;

   BTHREAD_STRUCT()
   {
      suspendSemaphore = SemAlloc();
      idleSemaphore = SemAlloc();
   }

   ~BTHREAD_STRUCT()
   {
      // Close the Windows thread.
      if (hChildThread != INVALID_HANDLE_VALUE)
      {
         // TerminateThread will sometimes cause a crash.  Don't know why.
         // iRet |= TerminateThread (bsp->hChildThread, 0);
         CloseHandle(hChildThread);
      }

      // Deallocate the smaphores.
      SemFree(suspendSemaphore);
      SemFree(idleSemaphore);

      // Release the task shared ptr.
      task = nullptr;
   }
};

// Task list entry
struct BTHREADPOOL_THREAD_LIST_ITEM
{
   // The thread struct for this thread.
   BTHREAD_STRUCT *threadStruct;

   // List NEXT pointer.
   struct BTHREADPOOL_THREAD_LIST_ITEM* next;
};

// The thread list.
struct BTHREADPOOL_THREAD_LIST
{
   // Pointer to a single-linked list of threads.
   BTHREADPOOL_THREAD_LIST_ITEM *head;

   // Count of all threads associated with this pool.
   int totalCount;

   // Count of listed threads that are presently idle.
   int idleCount;

   // A mutex for multithread safety.
   void *mutex;
};

// The threadpool.
struct BTHREADPOOL
{
   // The list of threads in the pool.
   BTHREADPOOL_THREAD_LIST *threadList;

   // Low water and high water counts for idle threads.
   // If the number of idle threads dips below the low water mark we will
   // spawn new ones until we reach the high water count. Conversely if the
   // number of idle threads exceeds the high water mark we will kill some.
   int lowWater;
   int highWater;

   // This gets set to false when we are trying to destroy the thread pool
   bool keepAlive;

   // To tell the background processor to make a pass
   void *backgroundSemaphore;
};

//////////////////////////////////////
// BTHREAD PRIVATE FUNCTIONS
//////////////////////////////////////

/*
 * The thread pool thread loop.
 */
static unsigned int __stdcall WINAPI BThreadPoolThreadLoop(void *param)
{
   BTHREAD_STRUCT *thread = (BTHREAD_STRUCT *)param;
   if (thread == NULL)
   {
      return 0;
   }

   BTHREADPOOL *threadPool = thread->threadPool;
   while (threadPool != NULL && threadPool->keepAlive)
   {
      // Wait for someone to give us a task to do.
      SemSuspend(thread->idleSemaphore);
      if (!thread->task)
      {
         // NULL task means "exit".
         break;
		}

      BTHREAD_TASK_SHARED_PTR &task = thread->task;

		//install_exception_object_handler( );
		CAutoErrorReporter autoErr("BThreadPoolThreadLoop", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

      // Run the task.
      int errorCode = 0;
      bool sehError = false;
		auto tryFilterException = [&errorCode](int code)	{return EXCEPTION_EXECUTE_HANDLER; };

		__try
		{
			try
			{
				task->AppFcn(task->vpAppData, thread);
			}
			catch (const std::exception &ex)
			{
				TRACE_0(errout << "***ERROR**** a BThread has caught std::exception: " << ex.what());
				TRACE_0(errout << "             BThread task created @ " << task->callerFilePathForDebugging << '[' <<
					 task->callerLineNumberForDebugging << ']');

				autoErr.errorCode = -10000;
				autoErr.msg << "THREAD CRASHED (std::exception) - reason: " << endl << ex.what()
					 << endl << "The thread was created at " << task->callerFilePathForDebugging << '[' <<
					 task->callerLineNumberForDebugging << ']';
			}
		}
		__except (tryFilterException(GetExceptionCode()))
		{
	      errorCode = GetExceptionCode();
			sehError = true;
		}

      // Any I/O in the __except could crash system
      if (sehError)
		{
			TRACE_0(errout << "***ERROR**** a BThread has thrown an non-standard error: " << std::hex << errorCode);
			TRACE_0(errout << "             BThread task created @ "
								<< task->callerFilePathForDebugging
								<< ":" << task->callerLineNumberForDebugging);

			autoErr.errorCode = -10001;
			autoErr.msg << "THREAD CRASHED - reason: NON_STANDARD EXCEPTION" << endl
				<< "The thread was created at " << task->callerFilePathForDebugging
				<< '[' << task->callerLineNumberForDebugging << ']';
		}

      // Release the task shared ptr.
		thread->task = nullptr;

      // Do this LAST!
      BThreadPoolSetThreadIdle(threadPool, thread);
   }

   delete thread;

   return 0;
}

//////////////////////////////////////
// BTHREAD PUBLIC FUNCTIONS
//////////////////////////////////////

/*
 * This function spawns a thread which runs AppFcn.  BThreadSpawn()
 * then blocks until it is released by a call to BThreadBegin() from
 * the AppFcn().
 *
 * The memory allocated here persists until it is no longer needed.
 * A call to BThreadEnd() or BThreadKill() will free the memory allocted
 * here.
 *
 * Upon success, BThreadSpawn returns a void pointer.
 * Upon failure, BThreadSpawn returns a NULL pointer.
 *
 * NOTE: now with thread pools, gets an idle thread from the pool instead of
 * actually spawning a new thread!
 */

static CSpinLock poolCreateLock;

void *BThreadSpawnEx(
	void(*AppFcn)(void *vpAppData, void *vpReserved),
	void *vpAppData,
	const char *callerFilePath,
	int callerLineNumber)
{
   if (AppFcn == NULL)
	{
      return NULL;
   }

   if (TheThreadPool == NULL)
   {
      CAutoSpinLocker lock(poolCreateLock);

      if (TheThreadPool == NULL)
      {
         TheThreadPool = BThreadPoolCreate(-1, -1);
      }
   }

   auto task = std::make_shared<BTHREAD_TASK>();
   if (!task)
   {
      return NULL;
   }

   task->AppFcn = AppFcn;
	task->vpAppData = vpAppData;

   // In debug traces, show only the directory and filename of the caller.
	string filename = AddDirSeparator(GetFileLastDir(callerFilePath)) + GetFileNameWithExt(callerFilePath);
	strncpy(task->callerFilePathForDebugging, filename.c_str(), 99);
	task->callerFilePathForDebugging[99] = 0;
	task->callerLineNumberForDebugging = callerLineNumber;

   // Find an idle thread to run the task.
   BTHREAD_STRUCT *thread = NULL;
   while (thread == NULL)
   {
      // If there are no idle threads, create one synchronously (don't wait for
      // the background thread to do it)
      if (TheThreadPool->threadList->idleCount == 0)
      {
         BThreadPoolAddThread(TheThreadPool);
      }

      thread = BThreadPoolGetIdleThread(TheThreadPool);
      if (thread == NULL)
      {
         // Someone stole our thread!
         SemResume(TheThreadPool->backgroundSemaphore);
         Sleep(0);
      }
   }

   task->vpReserved = thread;

   thread->task = task;
   SemResume(thread->idleSemaphore);

   // Wait until the task gets run and calls BThreadBegin.
   SemSuspend(task->beginSemaphore);

   // OK to release the task shared ptr now.
   task = nullptr;

   return (void *)thread;
}

/*
 When the application function is finished making local copies of
 volatile variables, this function will release the parent thread.
 */
int BThreadBegin(void * vpReserved)
{
   BTHREAD_STRUCT * thread = (BTHREAD_STRUCT *) vpReserved;

   if (thread == NULL || !thread->task)
   {
      return -1;
   }

   if (SemResume(thread->task->beginSemaphore) != 0)
   {
      return -1;
   }

   return 0;
}

/////*
//// When the application function is complete, this function will
//// destroy the thread.
//// NOTE: Because of thread pooling, this function is no longer supported!
//// */
////int BThreadEnd(void *vpReserved)
////{
////   BTHREAD_STRUCT *bsp = (BTHREAD_STRUCT*)vpReserved;
////
////   BThreadStructFree(bsp);
////   _endthreadex(0);
////
////   // should never reach this point
////   return 0;
////}


/*
 This function can be used to kill a thread.  It is not recommended,
 since the destruction will not do a proper clean up of the thread.

 If a thread attempts to kill itself, the BTHREAD_STRUCT will not
 be freed.  This will cause a slow memory leak.
 */
int BThreadKill(void *vpReserved)
{
   BTHREAD_STRUCT *bsp = (BTHREAD_STRUCT*)vpReserved;
   delete bsp;
   return 0;
}

int BThreadSuspend(void *vp)
{
   BTHREAD_STRUCT *bsp = (BTHREAD_STRUCT*)vp;
   return SemSuspend(bsp->suspendSemaphore);
}

int BThreadResume(void *vp)
{
   BTHREAD_STRUCT *bsp = (BTHREAD_STRUCT*)vp;
   return SemResume(bsp->suspendSemaphore);
}

int BThreadSetPriority(void *vp, int iPriority)
{
   BTHREAD_STRUCT *bsp = (BTHREAD_STRUCT*)vp;
   return SetThreadPriority(bsp->hChildThread, iPriority);
}

bool BThreadIsRunning(void *vp)
{
   BTHREAD_STRUCT *bsp = (BTHREAD_STRUCT*)vp;
   auto threadHandle = bsp->hChildThread;
   auto id = GetThreadId(threadHandle);
   if (threadHandle == INVALID_HANDLE_VALUE)
   {
      return false;
   }

   DWORD lpExitCode = -1;
   ::GetExitCodeThread(threadHandle, &lpExitCode);

   auto x = STILL_ACTIVE;
   return STILL_ACTIVE == lpExitCode;
}

//////////////////////////////////////
// THREAD POOL
//////////////////////////////////////

static bool BThreadPoolKeepAlive = true;
const int BThreadLowWaterDefaultPerProcessor = 2;   // 1
const int BThreadHighWaterDefaultPerProcessor = 5;  // 20
const float BThreadStartupDefaultThreadLevel = 0.40; // .75

// Background processing thread for thread pools. Keeps the number of idle
// threads between the low and high water marks.
static unsigned int __stdcall WINAPI BThreadPoolBackgroundProcessor(void *param)
{
   BTHREADPOOL *threadPool = (BTHREADPOOL *)param;
   if (threadPool == NULL || threadPool->threadList == NULL)
   {
      return 0;
   }

   if(!SetThreadPriority(GetCurrentThread(), THREAD_MODE_BACKGROUND_BEGIN))
   {
      DWORD dwError = GetLastError();
      TRACE_0(errout << "ThreadPool background processor failed to enter background mode (" << dwError << ")");
   }

   BTHREADPOOL_THREAD_LIST *list = threadPool->threadList;
   int startUp = (int)(threadPool->highWater * BThreadStartupDefaultThreadLevel);

	// Initially fire up some threads.
   while (list->totalCount < startUp)
   {
      BTHREAD_STRUCT *thread = BThreadPoolAddThread(threadPool);
      if (thread == NULL)
      {
         break;
      }

		Sleep((list->totalCount < threadPool->lowWater) ? 0 : 20);
   }

   while (threadPool->keepAlive)
   {
      // Try to keep total thread count at the high water mark but make sure
      // the nomber of idle threads is above the low water mark.
//      if (list->totalCount > threadPool->highWater && list->idleCount > threadPool->lowWater)
		if (list->idleCount > threadPool->highWater)
		{
//         TRACE_3(errout << "THREADPOOL BACKGROUND: HIGH BEFORE = " << list->idleCount << " / " << list->totalCount);
//			while (list->totalCount > threadPool->highWater && list->idleCount > threadPool->lowWater)
//			{
            BTHREAD_STRUCT *thread = BThreadPoolGetIdleThread(threadPool);
				BThreadPoolRemoveThread(threadPool, thread);
				TRACE_3(errout << "Thread REMOVED FROM buffer pool");
//			}
		}
//		else if (list->totalCount < threadPool->highWater && list->idleCount < threadPool->lowWater)
		else if (list->idleCount < threadPool->lowWater)
		{
//         TRACE_3(errout << "THREADPOOL BACKGROUND: IDLE BEFORE = " << list->idleCount << " / " << list->totalCount);
//			while (list->totalCount < threadPool->highWater && list->idleCount < threadPool->lowWater)
//			{
				BTHREAD_STRUCT *thread = BThreadPoolAddThread(threadPool);
				TRACE_3(errout << "Thread ADDED TO buffer pool");
				if (thread == NULL)
				{
					break;
				}
//			}
      }

//		SemSuspendWithTimeout(threadPool->backgroundSemaphore, 1000);
		SemSuspendWithTimeout(threadPool->backgroundSemaphore, 250);
   }

   return 0;
}

// Create bthread pool.
static BTHREADPOOL *BThreadPoolCreate(int lowWater, int highWater)
{
	BTHREADPOOL *threadPool = (BTHREADPOOL *)malloc(sizeof(BTHREADPOOL));
	if (threadPool == NULL)
   {
      return NULL;
   }

   // Use defaults for low & high water if less than 0 or 1 respectively.
   if (lowWater < 0)
   {
      lowWater = BThreadLowWaterDefaultPerProcessor * SysInfo::AvailableProcessorCount();
   }

   if (highWater < 1)
   {
      highWater = BThreadHighWaterDefaultPerProcessor * SysInfo::AvailableProcessorCount();
   }

   threadPool->lowWater = lowWater;
   threadPool->highWater = highWater;
   threadPool->keepAlive = true;
   threadPool->backgroundSemaphore = SemAlloc();;

   threadPool->threadList = BThreadPoolCreateThreadList();
   if (threadPool->threadList == NULL)
   {
      return NULL;
   }

   // TODO: Spin up threads from a background thread!
   HANDLE backgroundThread = (HANDLE*)(uintptr_t)_beginthreadex(NULL, 0, BThreadPoolBackgroundProcessor, (void *)threadPool, CREATE_SUSPENDED, NULL);
   if (backgroundThread == INVALID_HANDLE_VALUE)
   {
      return NULL;
   }

   ResumeThread(backgroundThread);
   SemResume(threadPool->backgroundSemaphore);

   return threadPool;
}

/*
 * Create a new thread and add it to the pool.
 */
static BTHREAD_STRUCT *BThreadPoolAddThread(BTHREADPOOL *threadPool)
{
   if (threadPool == NULL || threadPool->threadList == NULL)
   {
      return NULL;
   }

   BTHREAD_STRUCT *thread = new BTHREAD_STRUCT;
   if (thread == NULL)
   {
      return NULL;
   }

   thread->threadPool = threadPool;
   thread->hChildThread = (HANDLE*)(uintptr_t)_beginthreadex(NULL, 0, BThreadPoolThreadLoop, (void *)thread, CREATE_SUSPENDED, NULL);
   if (thread->hChildThread == INVALID_HANDLE_VALUE)
   {
      delete thread;
      return NULL;
   }

   BTHREADPOOL_THREAD_LIST *list = threadPool->threadList;
   BTHREADPOOL_THREAD_LIST_ITEM *listItem =
      (BTHREADPOOL_THREAD_LIST_ITEM *)malloc(sizeof(BTHREADPOOL_THREAD_LIST_ITEM));
   if (listItem == NULL)
   {
      delete thread;
      return NULL;
   }

   listItem->threadStruct = thread;

   CriticalSectionEnter(list->mutex);

   listItem->next = list->head;
   list->head = listItem;
   list->totalCount++;
   list->idleCount++;
//   TRACE_3(errout << "THREADPOOL: ADD THREAD = " << list->idleCount << " / " << list->totalCount);

   CriticalSectionExit(list->mutex);

   // Tell the background thread that the number of idle threads changed.
   SemResume(threadPool->backgroundSemaphore);

   // Thread was created in suspended state
   ResumeThread(thread->hChildThread);

   return thread;
}

/*
 * Thread list "constructor"
 */
static BTHREADPOOL_THREAD_LIST *BThreadPoolCreateThreadList()
{
   BTHREADPOOL_THREAD_LIST *list = (BTHREADPOOL_THREAD_LIST*) malloc(sizeof(BTHREADPOOL_THREAD_LIST));
   if (list == NULL)
   {
      return NULL;
   }

   list->head = NULL;
   list->totalCount = 0;
   list->idleCount = 0;
   list->mutex = CriticalSectionAlloc( );

   return list;
}

/*
 * Thread list "destructor"
 */
static void BThreadPoolDestroyThreadList(BTHREADPOOL *threadPool)
{
   if (threadPool == NULL || threadPool->threadList == NULL)
   {
      return;
   }

   BTHREADPOOL_THREAD_LIST *list = threadPool->threadList;

   // Remove each thread from the pool list, and null out the link to thread
   // pool from each thread, signaling the task semaphore which will cause the
   // thread to exit.
   CriticalSectionEnter(list->mutex);

   BTHREADPOOL_THREAD_LIST_ITEM *listItem = list->head;
   while (listItem != NULL)
   {
      BTHREAD_STRUCT *thread = listItem->threadStruct;
      if (thread != NULL)
      {
         thread->threadPool = NULL;
         SemResume(thread->idleSemaphore);
      }

      listItem = listItem->next;
   }

   list->head = NULL;
   list->totalCount = 0;
   list->idleCount = 0;

   CriticalSectionExit(list->mutex);
}

/*
 * Mark a thread as idle after it finishes a task.
 */
static void BThreadPoolSetThreadIdle(BTHREADPOOL *threadPool, BTHREAD_STRUCT *thread)
{
   if (threadPool == NULL || thread == NULL || (!thread->isBusy))
   {
      return;
   }

	thread->isBusy = false;
	::SetThreadPriority(thread->hChildThread, THREAD_PRIORITY_NORMAL);

	BTHREADPOOL_THREAD_LIST *list = threadPool->threadList;
	CriticalSectionEnter(list->mutex);
   threadPool->threadList->idleCount++;
//   TRACE_3(errout << "THREADPOOL: SET THREAD IDLE = " << list->idleCount << " / " << list->totalCount);
   CriticalSectionExit(list->mutex);

   // Tell the background thread that the number of idle threads changed.
   SemResume(threadPool->backgroundSemaphore);
}

/*
 * Remove the specified thread from the pool.
 */
static void BThreadPoolRemoveThread(BTHREADPOOL *threadPool, BTHREAD_STRUCT *thread)
{
   if (threadPool == NULL || thread == NULL || threadPool->threadList == NULL)
   {
      return;
   }

   BTHREADPOOL_THREAD_LIST *list = threadPool->threadList;

   CriticalSectionEnter(list->mutex);

   BTHREADPOOL_THREAD_LIST_ITEM *listItem = list->head;
   BTHREADPOOL_THREAD_LIST_ITEM *previousListEntry = NULL;
   while (listItem != NULL && listItem->threadStruct != thread)
   {
      previousListEntry = listItem;
      listItem = listItem->next;
   }

   if (listItem != NULL)
   {
      if (previousListEntry == NULL)
      {
         list->head = listItem->next;
      }
      else
      {
         previousListEntry->next = listItem->next;
      }

      list->totalCount--;
      if (!thread->isBusy)
      {
         list->idleCount--;
      }

//      TRACE_3(errout << "THREADPOOL: REMOVE THREAD = " << list->idleCount << " / " << list->totalCount);
   }

   CriticalSectionExit(list->mutex);

   // This will tell the thread to exit.
   thread->threadPool = NULL;
   SemResume(thread->idleSemaphore);

   // Tell the background thread that the number of idle threads changed.
   SemResume(threadPool->backgroundSemaphore);
}

/*
 * Get an idle thread from the pool and mark it as "busy".
 */
static BTHREAD_STRUCT *BThreadPoolGetIdleThread(BTHREADPOOL *threadPool)
{
   if (threadPool == NULL || threadPool->threadList == NULL)
   {
      return NULL;
   }

   BTHREADPOOL_THREAD_LIST *list = threadPool->threadList;

   CriticalSectionEnter(list->mutex);

   BTHREADPOOL_THREAD_LIST_ITEM *listItem = list->head;
   while (listItem != NULL)
   {
      if (!listItem->threadStruct->isBusy)
      {
         break;
      }

      listItem = listItem->next;
   }

   if (listItem != NULL)
   {
      listItem->threadStruct->isBusy = true;
      list->idleCount--;
//      TRACE_3(errout << "THREADPOOL: GET IDLE THREAD = " << list->idleCount << " / " << list->totalCount);
   }

   CriticalSectionExit(list->mutex);

   // Tell the background thread that the number of idle threads changed.
   SemResume(threadPool->backgroundSemaphore);

   return (listItem == NULL) ? NULL : listItem->threadStruct;
}

//////////////////////////////////////
// CRITICAL SECTION
//////////////////////////////////////

void * CriticalSectionAlloc()
{
   CRITICAL_SECTION *csp = (CRITICAL_SECTION*) malloc(sizeof(CRITICAL_SECTION));
   if (csp == NULL)
   {
      return NULL;
   }

   InitializeCriticalSection(csp);
   return (void *)csp;
}

int CriticalSectionEnter(void *vp)
{
   EnterCriticalSection((CRITICAL_SECTION*)vp);
   return 0;
}

int CriticalSectionExit(void *vp)
{
	try
	{
		LeaveCriticalSection((CRITICAL_SECTION*)vp);
	}
	catch (...)
	{
		TRACE_0(errout << "EXCEPTION: from LeaveCriticalSection() in CriticalSectionExit!!");
		throw;
	}

	return 0;
}

int CriticalSectionFree(void *vp)
{
   if (vp == NULL)
   {
      return 0;
   }

   try
   {
       DeleteCriticalSection((CRITICAL_SECTION*)vp);
   }
   catch (...)
   {
      TRACE_0(errout << "EXCEPTION: from DeleteCriticalSection() in CriticalSectionFree!!");
      throw;
   }

   free(vp);

   return 0;
}

//////////////////////////////////////
// COUNTING SEMAPHORE
//////////////////////////////////////

void *SemAlloc()
{
   SECURITY_ATTRIBUTES sa;

   sa.nLength = sizeof(SECURITY_ATTRIBUTES);
   sa.lpSecurityDescriptor = NULL;
   sa.bInheritHandle = true;

   // NOTE: Hard-coded max count of 1000.
   return (void *)CreateSemaphore(&sa, 0, 1000, NULL);
}

int SemSuspend(void *vp)
{
   WaitForSingleObject((HANDLE)vp, INFINITE);
   return 0;
}

bool SemSuspendWithTimeout(void *vp, int timeoutMsecs)
{
   if (WaitForSingleObject((HANDLE)vp, timeoutMsecs) == WAIT_TIMEOUT)
   {
      return false;
   }

   return true;
}

int SemResume(void *vp)
{
   ReleaseSemaphore((HANDLE)vp, 1, NULL);
   return 0;
}

int SemFree(void *vp)
{
   if (vp == NULL)
   {
      return 0;
   }

   if (!CloseHandle((HANDLE)vp))
   {
      return -1;
   }

   return 0;
}

//------------------------------------------------------------------
//
// Counting Wait;

CCountingWait::CCountingWait(int count)
  {
     Create(count);
  }

CCountingWait::CCountingWait(void)
  {
     Create(0);
  }

 void CCountingWait::Create(int n)
{
    _event = CreateEvent(NULL, false, false, 0);
   SetCount(n);
}
  CCountingWait::~CCountingWait(void)
    {
      CloseHandle(_event);
    }


  void CCountingWait::SetCount(int count)
    {
      Locker.Lock("CountingWait::SetCount");
      _count = count;
      ResetEvent(_event);
      Locker.UnLock();
    }

  void CCountingWait::ReleaseOne(void)
    {
      // Count should never be < 0
      Locker.Lock("CountingWait::ReleaseOne");
      _count = _count - 1;
      if (_count <= 0)
         SetEvent(_event);
      Locker.UnLock();
    }

  bool CCountingWait::Poll(void)
    {
       return (_count < 1);
    }

  bool CCountingWait::Wait(const vector<void *> &threadStruct, const char *function, int line)
  {
     while (WaitForSingleObject(_event, 2000) != WAIT_OBJECT_0)
     {
        TRACE_0(errout << "2000 ms timeout " << function << ", line " << line);
        for (auto vp : threadStruct)
        {
           if (!BThreadIsRunning(vp))
           {
                TRACE_0(errout << "Thread crashed in " << function << ", line " << line);
           }
        }
     }

     return true;
  }
