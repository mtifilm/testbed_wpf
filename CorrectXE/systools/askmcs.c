#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/termio.h>
#include <sys/types.h>
#include <bstring.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <errno.h>
#include "mcs_support.h"
#include "pipe_support.h"

main(int argc, char *argv[])
{
  bool           bKeepReadingF;
  char           caLine[MAX_LINE_LEN];
  int            iRet;
  int            iSelectCount;
  fd_set         fdsRead;
  struct timeval tv;
  int            iFDPipeFromDaemon = -1;
  int            iFDPipeToDaemon   = -1;

  if ((iFDPipeToDaemon = OpenPipeToDaemon()) == -1)
  {
    fprintf(stderr, "%s: mcsd apparently not running - exiting...\n",
            argv[0]);
    exit(-1);
  }

  SendToDaemon("HELO");

  if ((iFDPipeFromDaemon = OpenPipeFromDaemon()) == -1)
  {
    fprintf(stderr, "%s: mcsd didn't open pipe back to us - exiting...\n",
            argv[0]);
    exit(-1);
  }

  bKeepReadingF = TRUE;

  while (bKeepReadingF)
  {
    FD_ZERO(&fdsRead);
    FD_SET(iFDPipeFromDaemon, &fdsRead);
    iSelectCount = iFDPipeFromDaemon + 1;

    tv.tv_sec  = 5;
    tv.tv_usec = 0;
  
//printf("TTT preselect\n");
    iRet = select(iSelectCount, &fdsRead, NULL, NULL, &tv);
//printf("TTT select returned %d\n", iRet);

    if (iRet > 0)
    {
printf("TTT %d descriptors to process\n", iRet);
      if (FD_ISSET(iFDPipeFromDaemon, &fdsRead))
        ProcessData(&iFDPipeFromDaemon, &bKeepReadingF, caLine);
    }
    else if (iRet == 0)
    {
      printf("TTT select timeout\n");
    }
    else
    {
      perror("select:");
      exit(-1);
    }
  } /* while (bKeepReadingF) */

  if (!strncmp(caLine, "BUSY", 4))
  {
    printf("Sorry - busy\n");
    close(iFDPipeToDaemon);
    close(iFDPipeFromDaemon);
    exit(-1);
  }

  SendToDaemon("STAT");

//  bKeepReadingF = TRUE;

//  while (bKeepReadingF)
  while (1)
  {
    FD_ZERO(&fdsRead);
    FD_SET(iFDPipeFromDaemon, &fdsRead);
    iSelectCount = iFDPipeFromDaemon + 1;

    tv.tv_sec  = 5;
    tv.tv_usec = 0;
  
//printf("TTT preselect\n");
    iRet = select(iSelectCount, &fdsRead, NULL, NULL, &tv);
//printf("TTT select returned %d\n", iRet);

    if (iRet > 0)
    {
printf("TTT %d descriptors to process\n", iRet);
      if (FD_ISSET(iFDPipeFromDaemon, &fdsRead))
        ProcessData(&iFDPipeFromDaemon, &bKeepReadingF, caLine);
    }
    else if (iRet == 0)
    {
      printf("TTT select timeout\n");
    }
    else
    {
      perror("select:");
      exit(-1);
    }
  } /* while (bKeepReadingF) */
}
