//---------------------------------------------------------------------------

#ifndef UUIDSupportH
#define UUIDSupportH

#include <Rpc.h>
#include <string>
#include <sstream>
#include "guid_mti.h"
#include "systoolsDLL.h"

#ifdef __BORLANDC__
#pragma comment(lib, "Rpcrt4.a")
#else
#pragma comment(lib, "Rpcrt4.lib")
#endif

namespace UuidSupport
{
   MTI_SYSTOOLSDLL_API std::string UuidToStdString(const UUID &uuid);
   MTI_SYSTOOLSDLL_API UUID StdStringToUuid(const string &uuidString);

   MTI_SYSTOOLSDLL_API UUID GuidToUuid(const guid_t &guid);
   const UUID NilUuid = {0L, 0, 0, {0,0,0,0,0,0,0,0}};
   const string NullGuidString = "00000000-0000-0000-0000-000000000000";
   MTI_SYSTOOLSDLL_API UUID UuidBySeed(const UUID originalUuid, int seed);

   MTI_SYSTOOLSDLL_API UUID CreateUuid();
   MTI_SYSTOOLSDLL_API string CreateUuidString();
}


//---------------------------------------------------------------------------
#endif
