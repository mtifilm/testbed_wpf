/* Defines */
#define MAX_LINE_LEN 512
#define BYTES_PER_LINE 7

/* Forward Declarations */
char *GetPipeName(char *caFileName, const long lPID);
int OpenPipe(const char *caPipeName, int iFlags);
int CreateInPipe (const char *caName);
int CreateOutPipe(const char *caName);
int SendToPipe(const int iFD, const char *caLine);

/*
        Function declarations for external c functions
*/
#ifdef __cplusplus
extern "C"
{
#endif
int SendToDaemon(const char *caLine);
int OpenPipeToDaemon(void);
int OpenPipeFromDaemon(void);
int ProcessData(int *ipFDPipeFromDaemon, bool *bpKeepReadingF, char *caLine);
#ifdef __cplusplus
}
#endif
