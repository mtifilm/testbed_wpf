/*
   File Name:  SafeClasses.h
   Created: December 3, 2000 by John Mertus

*/
#ifndef _SAFECLASSES_H
#define _SAFECLASSES_H

#include "systoolsDLL.h"
#include "machine.h"
#include "TheError.h"
#include "ThreadLocker.h"
#include <assert.h>

#ifdef _WINDOWS
#include <windows.h>
#if !defined(ETIMEDOUT)
#define ETIMEDOUT  WAIT_TIMEOUT // Unix - windows equivalent
#endif
#else
#include <sys/errno.h>
#include <pthread.h>
#endif

//------------------------------RingBuffer--------------------------------------
//
//  RingBuffer is a template class that contains types in a ring buffer
//  A ring buffer is a circular list.
//
//     RingBuffer(N)  creates a ringbuffer of size N
//       Add(Type) adds a type to the buffer
//       Take() returns a type from the ring buffer
//       Empty() returns true if empty, false otherwise
//       Front() returns the topmost element in the buffer
//       Back() returns the last in element in the buffer
//       Size() is the size of the buffer;
//
//
#include <queue>

template <class Type> class CRingBuffer
{
  public:
  int Overflows;               // Items missed
  int    ErrorNum;             // Error number of message, 
  string ErrorMessage;         // None blank on error

  // Define the Queue like interface
  Type Take(void);             // Gets an element from the ring buffer.
  void Add(Type elem);         // Adds an element to the ring buffer
  bool Empty(void);            // Is the ring buffer empty?
  void ClearInput(void);       // Cleans out the ring buffer
  Type Front(void);            // Oldest element in buffer
  Type Back(void);             // Newest element in buffer
  int  Size(void);             // Size of buffer
  void SetRingSize(int Value); // Sets the size of buffer
  int  GetRingSize(void);      // Returns the size of buffer
  void SignalError(void);      // Says an error has happened
  // This waits ms milliseconds for something to come into the buffer
  // if not specified, it waits forever
  int Wait(int ms=-1);

  // Constructor;
  CRingBuffer(int n=2048);
  ~CRingBuffer(void);

  // Ring buffer uses the queue to hold data.
  // Locker is used to make code thread safe
  private:
    std::queue<Type> Q;
    unsigned fRingSize;          // The size of the ring buffer
    CThreadLock Locker;          // To make this thread safe
    void Signal(void);           // Signals something is ready
    void ResetSignal(void);      // Resets the signal;
//
// Normally this would be handled with a cheshire class
#ifdef _WINDOWS
   HANDLE m_Event;              // Window objects are handles
#else
   pthread_cond_t m_Event;      // pthread handles
#endif
};

// The standard says separate compilation of templates is legal,
// no known compiler supports this, so the complete template MUST
// be available to the program
#include "SafeTemplates.hcc"

// Add some safe file routines
// The following creates a unique FileName. Although, in theory, another
// name could be created, the odds are near impossible.  They are the same
// that a GUID are the same.
MTI_SYSTOOLSDLL_API string MTIUniqueFileName(const string &Template);
#endif
