//---------------------------------------------------------------------------

#ifndef ran266H
#define ran266H
//---------------------------------------------------------------------------

#include "systoolsDLL.h"

// This has to be in its own cpp module because of asm code.
MTI_SYSTOOLSDLL_API float ran266(long *lseed);

#endif
