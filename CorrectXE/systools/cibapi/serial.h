/*
serial.h
*/

int ser_gethandle(char *);
int ser_sethandleparms(int);
int ser_close(int);
int ser_dtron(int);
int ser_dtroff(int);
long ser_getspeed(int);
int ser_setspeed(int,unsigned long);
int ser_read(int,unsigned char *,int,unsigned long);
int ser_read_data(int,char *,int,unsigned long);
int ser_write(int,unsigned char *,int,unsigned long);
int ser_write_data(int,char *,int);
