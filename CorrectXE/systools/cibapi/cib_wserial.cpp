/*
   File Name:  wserial.cpp

   This class does serial I/O to a COM port.
   See the H file for how to use this class.


*/
#pragma hdrstop
#include "machine.h"
#include "cib_wserial.h"
 /*-----------------------Constructor----------------JAM--Mar 2000----*/

  CIB_WSerial::CIB_WSerial()

 /* This does nothing but set a few variables to defaults             */
 /*                                                                   */
 /*********************************************************************/
{
   fhComm = NULL;
#ifdef DO_TIMESTAMPS
   hRxThread = NULL;
#endif /* DO_TIMESTAMPS */
   fPortName = "";

   // Set the device control block
   memset(&fdcb, 0, sizeof(DCB));
   fdcb.DCBlength = sizeof(DCB);
   BuildCommDCB("38400,N,8,1", &fdcb);

   //  Setup the properties
   PROPERTY_INIT(Parity, GetParity, SetParity);
   PROPERTY_INIT(Baud, GetBaud, SetBaud);
   PROPERTY_INIT(Bits, GetBits, SetBits);
   PROPERTY_INIT(StopBits, GetStopBits, SetStopBits);
//   SPROPERTY_INIT(PortName, fPortName, SetPortName);
   PROPERTY_INIT(PortName, GetPortName, SetPortName);
   RO_PROPERTY_INIT(Opened, GetOpened);

#ifdef DO_TIMESTAMPS
   fQueuePackets = 16;
   cRing = NULL;
#endif /* DO_TIMESTAMPS */
}
 /*-----------------------Destructor-----------------JAM--Mar 2000----*/

  CIB_WSerial::~CIB_WSerial()

 /* Usual destructor.                                                 */
 /*                                                                   */
 /*********************************************************************/
{
  Close();
}

/*----------------------Close-----------------------JAM--Mar 2000----*/

  bool CIB_WSerial::Close(void)

/* This closes the serial if it is open.                             */
/* Result is TRUE if port was open and it closed or if port was not  */
/* opened.   False if the port failed to close.                      */
/*                                                                   */
/*********************************************************************/
{
  if (!Opened) return(true);

#ifdef DO_TIMESTAMPS
  // Cancel any pending IO, this is done by setting the terminate flag
  // which cancels the I/O and terminates the thread.
  AbortThread = true;
#endif /* DO_TIMESTAMPS */

  // Set the port name to nil
  fPortName = "";
//
// Restore to initial state;
//
  SetCommTimeouts(fhComm, &ctmoOld);
  SetCommState(fhComm, &fdcbOld);
  CloseHandle(fhComm);             // Close it
  fhComm = NULL;

#ifdef DO_TIMESTAMPS
// Delete the ring buffer;
  delete cRing;
#endif /* DO_TIMESTAMPS */


// Success, report it
  return(true);
}

/*-----------------------Open-----------------------JAM--Mar 2000----*/

  bool CIB_WSerial::Open(string Name)

/* This does nothing but set a few variables to defaults             */
/*                                                                   */
/*********************************************************************/
{

  // Close it if open
 ////if (!Close()) return(false);
 // mbraca doesn't like that. If it's already opened, then no-op.
 if (Opened)
    return true;

  // Now try to open the file
  fhComm = CreateFile(Name.c_str(),
                      GENERIC_READ | GENERIC_WRITE | SYNCHRONIZE,
                      0,
                      0,
                      OPEN_EXISTING,
 #ifdef DO_TIMESTAMPS
                      FILE_FLAG_OVERLAPPED,
 #else
                      0,
 #endif
                      0);

  if(fhComm == INVALID_HANDLE_VALUE)
    {
      GetLastErrorMessage(true);
      return(false);
    }

  // Get the current timeout values
  GetCommTimeouts(fhComm,&ctmoOld);
  GetCommState(fhComm, &fdcbOld);

  // Now set all the timeouts to zero
  COMMTIMEOUTS ctmoNew;
  ctmoNew.ReadIntervalTimeout = MAXDWORD; // 0;
  ctmoNew.ReadTotalTimeoutConstant = 1000; // one sec // 100;
  ctmoNew.ReadTotalTimeoutMultiplier = MAXDWORD; // 0;
  ctmoNew.WriteTotalTimeoutMultiplier = 100;
  ctmoNew.WriteTotalTimeoutConstant = 0;
  SetCommTimeouts(fhComm, &ctmoNew);

  SetCommState(fhComm, &fdcb);

#ifdef DO_TIME_STAMPS
// Create the thread
  cRing = new CRingBuffer<KeyStamp>(256);

  fRxTotal = 0;
  fTxTotal = 0;

  AbortThread = false;

  hRxThread = CreateThread((LPSECURITY_ATTRIBUTES )NULL,	// No Security
                           (DWORD )0,							// Same Stack Size
			   (LPTHREAD_START_ROUTINE )StaticReceiveProc,	// Thread Procedure
			   (PVOID )this,				// Parameter Pointer
			   (DWORD )0,							// Start Immediately
			   &dwRxThreadId						// Thread ID

			   );

  // Report Errors
  if(!hRxThread)
    {
      GetLastErrorMessage(TRUE);
      // QQQ MTISevereErrorDialog(ErrorMessage);
    }

  SetThreadPriority(hRxThread, THREAD_PRIORITY_ABOVE_NORMAL);
#endif /* DO_TIME_STAMPS */

// Success, report it
  return(true);
}

/*-----------------------GetBaud--------------------JAM--Mar 2000----*/

  unsigned CIB_WSerial::GetBaud(void)

/* This returns the ACTUAL baud rate of the communications port.     */
/*                                                                   */
/*********************************************************************/
{
   GetParameters();
   return(fdcb.BaudRate);
}

/*-----------------------SetBaud--------------------JAM--Mar 2000----*/

  void CIB_WSerial::SetBaud(unsigned Value)

/* This trys to set the baud rate of the terminal to Value.          */
/*  Note, the Baud propery may not equal the value set               */
/*                                                                   */
/*********************************************************************/
{
   if (fdcb.BaudRate == Value) return;
   fdcb.BaudRate = Value;
   SetParameters();
}

/*-----------------------GetDTRControl--------------JAM--Mar 2000----*/

  unsigned CIB_WSerial::GetDTRControl(void)

/* This returns the ACTUAL DTR mode of the communications port.      */
/*                                                                   */
/*********************************************************************/
{
   GetParameters();
   return(fdcb.fDtrControl);
}

/*-----------------------SetDTRControl--------------JAM--Mar 2000----*/

  void CIB_WSerial::SetDTRControl(unsigned Value)

/* This trys to set the DTR mode of the terminal to Value.           */
/*                                                                   */
/*********************************************************************/
{
   if (fdcb.fDtrControl == Value) return;
   fdcb.fDtrControl = Value;
   SetParameters();
}

/*-----------------------GetRTSControl--------------JAM--Mar 2000----*/

  unsigned CIB_WSerial::GetRTSControl(void)

/* This returns the ACTUAL RTS mode of the communications port.      */
/*                                                                   */
/*********************************************************************/
{
   GetParameters();
   return(fdcb.fRtsControl);
}

/*-----------------------SetRTSControl--------------JAM--Mar 2000----*/

  void CIB_WSerial::SetRTSControl(unsigned Value)

/* This trys to set the RTS mode of the terminal to Value.           */
/*                                                                   */
/*********************************************************************/
{
   if (fdcb.fRtsControl == Value) return;
   fdcb.fRtsControl = Value;
   SetParameters();
}

/*-----------------------GetParity-----------------JAM--Mar 2000----*/

  BYTE CIB_WSerial::GetParity(void)

/* This returns the ACTUAL parity setting of the communications port */
/*                                                                   */
/*********************************************************************/
{
   GetParameters();
   return(fdcb.Parity);
}

/*-----------------------SetParity-----------------JAM--Mar 2000----*/

  void CIB_WSerial::SetParity(BYTE Value)

/* This trys to set the parity of the terminal to Value.             */
/* Values are 0-4 = no, odd, even, mark, space                       */
/* Defined in windows.h as NOPARITY, ODDPARITY, etc.                 */
/*  Note, the parity propery may not equal the value set             */
/*                                                                   */
/*********************************************************************/
{
   if (fdcb.Parity == Value) return;
   if (Value == NOPARITY) fdcb.fParity = 0; else fdcb.fParity = 1;
   fdcb.Parity = Value;
   SetParameters();
}


/*-----------------------SetBits-------------------JAM--Mar 2000----*/

  void CIB_WSerial::SetBits(BYTE Value)

/* This trys to set the bits of the terminal to Value.               */
/* Possible values are 4 to 8.                                       */
/*  Note, the bits property may not equal the value set              */
/*                                                                   */
/*********************************************************************/
{
   if (fdcb.ByteSize == Value) return;
   fdcb.ByteSize = Value;
   SetParameters();
}
/*-----------------------GetBits-------------------JAM--Mar 2000----*/

  BYTE CIB_WSerial::GetBits(void)

/* This returns the ACTUAL parity setting of the communications port */
/*                                                                   */
/*********************************************************************/
{
   GetParameters();
   return(fdcb.ByteSize);
}

/*-----------------------SetStopBits---------------JAM--Mar 2000----*/

  void CIB_WSerial::SetStopBits(BYTE Value)

/* This trys to set the parity of the terminal to Value.             */
/*  Note, the parity propery may not be set to this value            */
/*                                                                   */
/*********************************************************************/
{
   if (fdcb.StopBits == Value) return;
   fdcb.StopBits = Value;
   SetParameters();
}

/*-----------------------GetStopBits---------------JAM--Mar 2000----*/

  BYTE CIB_WSerial::GetStopBits(void)

/* This returns the ACTUAL parity setting of the communications port */
/*                                                                   */
/*********************************************************************/
{
   GetParameters();
   return(fdcb.StopBits);
}

/*-----------GetLastErrorMessage--------------------JAM--Mar 2000----*/

  void CIB_WSerial::GetLastErrorMessage(bool bResult)

/* This function returns a string that contains a human readable     */
/* version of the error message in the string ErrorMessage.          */
/*   bResult is the error flag (not error code!) if true an error    */
/* message is formated, if false, the result is "Success"            */
/*                                                                   */
/*********************************************************************/
{
  char cErrorMessage[255];

  // Do nothing if not an error
  if (!bResult)
    {
       ErrorMessage = "Success";
       return;
    }

  DWORD ErrorCode = GetLastError();

  FormatMessage
          (
             FORMAT_MESSAGE_FROM_SYSTEM,
             NULL,
             ErrorCode,
             MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
             (LPTSTR) &cErrorMessage,
             255,
             NULL
          );

  ErrorMessage = cErrorMessage;
};

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#ifdef DO_TIMESTAMPS
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

/*-----------------------GetKey---------------------JAM--Mar 2000----*/

   KeyStamp CIB_WSerial::GetKey(void)

/* This returns the next key-stamp combination in the ring buffer.   */
/* If the ring buffer is empty, a 0,0 is returned.                   */
/*                                                                   */
/*********************************************************************/
{
   KeyStamp ks = {char(0), true, 0};
   if (cRing->Empty()) return(ks);
   return(cRing->Take());
}

/*-----------------------GetKeyW--------------------JAM--Mar 2000----*/

   KeyStamp CIB_WSerial::GetKeyW(int ms)

/* This waits int ms for the next key in the buffer.                 */
/* If the ring buffer is empty, a 0,0 is returned.                   */
/*                                                                   */
/*********************************************************************/
{
   KeyStamp ks = {char(0), true, 0};

   cRing->Wait(ms);
   if (cRing->Empty()) return(ks);
   return(cRing->Take());
}


/*-----------------------Empty----------------------JAM--Mar 2000----*/

   bool CIB_WSerial::Empty(void)

/* Returns true if no keys are in the ring buffer, false otherwise   */
/*                                                                   */
/*********************************************************************/
{
   return(cRing->Empty());
}

/*-----------------------RxProc---------------------JAM--Mar 2000----*/

  DWORD CIB_WSerial::RxProc(void)

/* This is the virtual receive thread.  Its responsibility is to     */
/* queue the I/O and then process it.                                */
/*                                                                   */
/*********************************************************************/
{
  OVERLAPPED OverlapR;
  KeyStamp ks;

  memset(&OverlapR, 0, sizeof(OVERLAPPED));
  OverlapR.hEvent = CreateEvent(NULL, true, false, NULL);

  while(!AbortThread)
    {
     unsigned long dwBytesRead = 0;
     int bResult = ReadFile(fhComm, &ks.Key, 1, &dwBytesRead, &OverlapR);
  //
  // See if there is an error on the read
  //
     if (!bResult)
       {
          if (GetLastError() != ERROR_IO_PENDING)
            {
             GetLastErrorMessage(true);
             Sleep(100);  // Multiple errors can cause thread to lock
            }
          else
           {
             // This should never timeout as the serial port
             // will complete in 100 ms
             WaitForSingleObject(OverlapR.hEvent, 1000);
             GetOverlappedResult(fhComm,&OverlapR,&dwBytesRead, TRUE);
           }
       }
     else
       {
         // This should never timeout as the serial port
         // will complete in 100 ms
         WaitForSingleObject(OverlapR.hEvent, 1000);
         GetOverlappedResult(fhComm,&OverlapR,&dwBytesRead, TRUE);
       };

     // See if data is coming
     if(dwBytesRead)
      {
        int iRet = mtiGetUST(&ks.UST);
        ks.Error = (iRet != 0);
        fRxTotal++;
        cRing->Add(ks);
      }

    }

CancelIo(fhComm);
ExitThread(0);
return(0);
}

/*-----------------------IOComplete-----------------JAM--Mar 2000----*/

   void CIB_WSerial::IOComplete(DWORD dwError, DWORD dwNumberOfBytes, LPOVERLAPPED lpoBuff)

/* This is the I/O completion routine, it stores the character in    */
/* the ring buffer and then queues another I/O.                      */
/* An event flag is signled to show a character is available.        */
/*                                                                   */
/*********************************************************************/
{
  WSerialCompleationBuffer *p = (WSerialCompleationBuffer *)lpoBuff->hEvent;

  // on abort error, just release the memory
  if (dwError == ERROR_OPERATION_ABORTED)
    {
      free(p);
      return;
    }

  // Requeue the event.
  int bResult = ReadFileEx(fhComm, &p->Key, 1, &p->oBuff, &StaticIOComplete);
  GetLastErrorMessage(!bResult);
  if (!bResult) throw(ErrorMessage);

}
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#endif /* DO_TIMESTAMPS */
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

//-----------------------SendChar-------------------JAM--Mar 2000----*/

  void CIB_WSerial::SendChar(char Key)

// This sends one character out the port.  Key is the character to
// send.
//
//*********************************************************************/
{
// NOTE: this is a dumb send
// TODO: create a more efficient way of transmitting data
//
    OVERLAPPED OverLap;
    unsigned long c;
    BOOL success;

    if (!Opened) return;

    memset(&OverLap, 0, sizeof(OverLap));
    success = WriteFile(fhComm, &Key, 1, &c, &OverLap);
    if ((!success) || c == 0) {
        int err = GetLastError();
        printf("SendChar(1) FAILED, sent %d chars, error %d\n", c, err);
    }
}

//-----------------------SendBuffer-----------------JAM--Aug 2000----*/

  int CIB_WSerial::SendBuffer(const char *cp, int N)

// This sends a buffer of data to the serial port.  cp is the data
// N is the number to send.
//
//*********************************************************************/
{
    static OVERLAPPED OverLap;
    unsigned long c;
    BOOL success;

    int Result = 0;
    if (!Opened)
      {
         ErrorMessage = "Port is not open";
         return(Result);
      }

    memset(&OverLap, 0, sizeof(OverLap));
    success = WriteFile(fhComm, cp, N, &c, &OverLap);
    if ((!success) || c < (unsigned long) N) {
        int err = GetLastError();
        printf("SendBuffer(%d) FAILED, sent %d chars, error %d\n", N, c, err);
    }
    Result = c;
    return(Result);
}

//-----------------------ReadBuffer-----------------JAM--Aug 2000----*/

  int CIB_WSerial::ReadBuffer(unsigned char *cp, int N, MTI_INT64 *sp)

// This reads a buffer of data from the serical port.
// cp is the data
// N is the number of bytes to read
// Return is number of bytes
//
//*********************************************************************/
{
  return(ReadBuffer(cp, N, -1, sp));
}

//-----------------------ReadBuffer-----------------JAM--Aug 2000----*/

  int CIB_WSerial::ReadBuffer(unsigned char *cp, int N, int ms, MTI_INT64 *sp)

// This reads a buffer of data from the serical port.
// cp is the data
// N is the number of bytes to read
// ms is the maximum number of milliseconds to wait for each character.
// Return is number of bytes
//
//*********************************************************************/
{
    int Result = 0;              // how many character read
    if (!Opened) return(Result);

    for (int i=0; i < N; i++)
      {
#ifdef DO_TIMESTAMPS

        KeyStamp ks = GetKeyW(ms);
        cp[i] = ks.Key;
        if (sp != NULL) sp[i] = ks.UST;
        // If an error, e.g, a null is returned
        if (ks.Error)
         {
           ErrorMessage = "Error in ReadBuffer\n" + cRing->ErrorMessage;
           return(Result);
         }

#else /* don't DO_TIMESTAMPS */

        OVERLAPPED OverLap;
        unsigned long c;
        COMMTIMEOUTS ctmoNew;
        BOOL success;

        ctmoNew.ReadIntervalTimeout = MAXDWORD; // 0;
        ctmoNew.ReadTotalTimeoutConstant = ms > 0? ms : MAXDWORD; // 0;
        ctmoNew.ReadTotalTimeoutMultiplier = MAXDWORD; // 0;
        ctmoNew.WriteTotalTimeoutMultiplier = 100;
        ctmoNew.WriteTotalTimeoutConstant = 0;
        SetCommTimeouts(fhComm, &ctmoNew);

        memset(&OverLap, 0, sizeof(OverLap));
        success = ReadFile(fhComm, &cp[i], 1, &c, &OverLap);
        if ((!success) || c == 0) {
          int err = GetLastError();
          if (err)
                printf("ReadBuffer(%d) FAILED, got %d chars, error %d\n",
                        N, i, err);
          break;
        }
        if (sp != NULL)
            sp[i] = 0;

#endif /* DO_TIMESTANPS */

        // Bump the number of characters read
        Result++;
      }

    return(Result);
}

