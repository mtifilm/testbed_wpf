/*--------------------------------------------------------------------------
 * Copyright (C) 1992,1993,1994,1995,1996 Dallas Semiconductor Corporation. 
 * All rights Reserved. Printed in U.S.A.
 * This software is protected by copyright laws of
 * the United States and of foreign countries.
 * This material may also be protected by patent laws of the United States 
 * and of foreign countries.
 * This software is furnished under a license agreement and/or a
 * nondisclosure agreement and may only be used or copied in accordance
 * with the terms of those agreements.
 * The mere transfer of this software does not imply any licenses
 * of trade secrets, proprietary technology, copyrights, patents,
 * trademarks, maskwork rights, or any other form of intellectual
 * property whatsoever. Dallas Semiconductor retains all ownership rights.
 *--------------------------------------------------------------------------
 *
 *  COMMON.C
 *  Description here.           
 *
 *
 *  Author: 
 *  Project: DS2480
 *  Created: 8/9/96
 *  Distribution: In House
 *  Compiler: GCC 2.6.3 (tested under Linux)
 *  Externals: common.h ddp.h
 *  Version: 0.99.0
 *
 *  Version History:
 *  0.99.0 - Initial Release
 */

/* 
common.c
*/
#include "machine.h"
#ifndef WIN32

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/time.h>
#include <ctype.h>
#include "common.h"

/*
Returns 1 if current time is greater than end time.
*/
int timepassed(struct timeval current,struct timeval end)
{
  if (current.tv_sec > end.tv_sec)
  {
    return(1);
  }
  if (current.tv_sec == end.tv_sec)
  {
    if (current.tv_usec >= end.tv_usec)
    {
      return(1);
    }
  }
  return(0);
}

#endif /* not WIN32 */
