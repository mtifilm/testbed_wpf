/*--------------------------------------------------------------------------
 * Copyright (C) 1992,1993,1994,1995,1996 Dallas Semiconductor Corporation. 
 * All rights Reserved. Printed in U.S.A.
 * This software is protected by copyright laws of
 * the United States and of foreign countries.
 * This material may also be protected by patent laws of the United States 
 * and of foreign countries.
 * This software is furnished under a license agreement and/or a
 * nondisclosure agreement and may only be used or copied in accordance
 * with the terms of those agreements.
 * The mere transfer of this software does not imply any licenses
 * of trade secrets, proprietary technology, copyrights, patents,
 * trademarks, maskwork rights, or any other form of intellectual
 * property whatsoever. Dallas Semiconductor retains all ownership rights.
 *--------------------------------------------------------------------------
 *
 *  DS2480COM.C
 *  Description here.           
 *
 *
 *  Author: 
 *  Project: DS2480
 *  Created: 8/9/96
 *  Distribution: In House
 *  Compiler: GCC 2.6.3 (tested under Linux)
 *  Externals: serial.h ds2480.h ds2480com.h
 *  Version: 0.99.9
 *
 *  Version History:
 *  0.99.9 - Added DS2480Pad(), DS2480Stream(), DS2480StreamRS(), and modified
 *           DS2480Access() to speed up Crypto/JiB stuff.  Currently
 *           DS2480StreamRS() is broken, and not used. 2/3/98
 *  0.99.8 - Changed DS2480Block() to allow streaming of bytes
 *           at standard one wire speed and 19200 baudrate
 *           if a DS2480A2 is detected.  The DS2480A1 can only
 *           stream at standard one wire speed and 9600 baudrate.
 *           10/8/97
 *  0.99.7 - Fixed DS2480Byte for DS2480 Version A1 0xE3/0xF3.
 *           Added DS2480 Version1/Version2 checking, and adaptation
 *           for 0xE3/0xF3 padding.
 *           8/12/97
 *  0.99.6 - Fixed DS2480Search to not look for echoes on Search
 *           accelerator command bytes.  Also, separated One
 *           wire reset out of command.  6/2/97
 *  0.99.5 - Fixed DS2480Search to get rid of overrun problems.
 *           12/18/96
 *  0.99.4 - Implemented and finished testing DS2480Search().
 *           What a pain!  12/17/96
 *  0.99.3 - Fixed DS2480Block() problem with E3 command screwup.
 *           12/10/96
 *  0.99.2 - Fixed DS2480SetSpeed() so we don't accidentally
 *           catch a byte we don't want.  11/25/96
 *  0.99.1 - Added DS2480Block()  11/18/96
 *  0.99.0 - Initial Release
 */

/*
ds2480com.c
*/
#include "machine.h"

#ifdef WIN32
#include "HRTimer.h"

// Need my own sleep because MTImillisleep sleeps a minimum of 15 msecs
// Also beware: the SleepMS() function in HRTimer just calls Sleep()!!!

static void MikesMilliSleep(long millis)
{
   MTI_INT64 start_time, time_now, target_time;
   CHRTimer timer;

   start_time = (MTI_INT64) timer.Read();
   target_time = start_time + (MTI_INT64) millis;
   do {
      Sleep(0);
      time_now = (MTI_INT64) timer.Read();
   } while (time_now < target_time);
}
#define tcdrain(x) MikesMilliSleep(10)
#define tcflush(h,x) do {unsigned char buf[1024]; ser_read(h, buf, 1024, 10);} while(0)
#endif /* WIN32 */

#include <stdio.h>
#include <fcntl.h>
#ifndef WIN32
#include <termios.h>
#include <unistd.h>
#endif /* not WIN32 */
#include <stdlib.h>
#include <sys/types.h>
#ifndef WIN32
#include <sys/times.h>
#endif /* not WIN32 */
#include "MTIsleep.h"

#include "serial.h"
#include "ds2480.h"
#include "ds2480com.h"

#define TRUE	1
#define FALSE	0

extern unsigned char AuthFlag;
static int timeout = 1500;  /* 1.5ms per character should be enough */
static int BitPrime = 0;

/* Start out in command mode */
static unsigned char DS2480Mode = MODSEL_COMMAND;
/* Start out in standard speed */
static unsigned char DS2480Speed = SPEEDSEL_STD;
/* Start out with unknown DS2480 version */
static unsigned char DS2480Version = 0;

/* Pads arr if necessary and writes it to newarr which is
   2 times the size of arr to allow for padding.  Returns
   the number of bytes in newarr.
*/
int DS2480Pad(unsigned char *arr,int start,int length,unsigned char *newarr)
{
  int i,j;
  int newlength = 0;
  
  for (i = start;i < (start+length);i++)
  {
    if ((arr[i] == 0xE3) ||
       ((arr[i] == 0xF3) && (DS2480Version == DS2480VERSION1)))
    {
      newlength++;
    }
    
    newlength++;
  }
  
  j = 0;
  for (i = start;i < (start+length);i++)
  {
    if ((arr[i] == 0xE3) ||
       ((arr[i] == 0xF3) && (DS2480Version == DS2480VERSION1)))
    {
      newarr[j] = arr[i];
      j++;
    }
    
    newarr[j] = arr[i];
    j++;
  }
  
  return (newlength);
}

unsigned char DS2480Stream(int handle,unsigned char *romid,unsigned char *arr,
        int arrlength)
{
  int i;
  unsigned char *newarr;

/*  int offset;
  unsigned char tempromid[16];
  unsigned char *temparr;
  int temparrlen;
  int tempromidlen;

  if ( (DS2480Speed == SPEEDSEL_OD)
       && (ser_getspeed(handle) <= 57600) )
  {
    ToggleCommandMode(handle);
    
    // Malloc space for incoming Data Array
    temparr = (unsigned char *)malloc(arrlength*2);
    if (temparr == NULL)
      return FALSE;

    // Pad the ROM ID and the data array.
    tempromidlen = DS2480Pad(romid,0,8,tempromid);
    temparrlen = DS2480Pad(arr,0,arrlength,temparr);

    // Malloc space for combined byte array.
    newarr = (unsigned char *)malloc(temparrlen+tempromidlen+3);
    if (newarr == FALSE)
      return FALSE;
    
    offset = 0;
    newarr[offset++] = CMD_COMM | FUNCTSEL_RESET | DS2480Speed | 0x01;
    newarr[offset++] = 0xE1; // Change to Data Mode
    newarr[offset++] = 0x55; // Search ROM Command
      
    // Copy the ROM ID in.
    for (i = 0;i < tempromidlen;i++)
      newarr[offset++] = tempromid[i];
      
    // Copy the Data Array in.
    for (i = 0;i < temparrlen;i++)
      newarr[offset++] = temparr[i];
    
    DS2480RawBlock(handle,newarr,offset,2+8+arrlength);
    ser_write(handle,newarr,offset,timeout*offset);
    tcdrain(handle);
    ser_read(handle,newarr,(2+8+arrlength),timeout*(arrlength));

    // Copy the returned data back.
    for (i = 10;i < (10+arrlength);i++)
      arr[i-10] = newarr[i];

    free (newarr);
    free (temparr);
    
    DS2480Mode = MODSEL_DATA;
    
    if ((newarr[0] & 0x03) != 0x01)
      return FALSE;
  }
  else*/
  {
    if (DS2480Reset(handle) != 1)
      return FALSE;

    newarr = (unsigned char *)malloc(9+arrlength);
    if (newarr == NULL)
      return FALSE;

    newarr[0] = 0x55;  
    for (i = 0;i < 8;i++)
      newarr[1+i] = romid[i];
    for (i = 0;i < arrlength;i++)
      newarr[9+i] = arr[i];

    DS2480Block(handle,newarr,arrlength+9);

    for (i = 0;i < arrlength;i++)
      arr[i] = newarr[9+i];
      
    free(newarr);
  }

  return TRUE;
}

unsigned char DS2480StreamRS(int handle,unsigned char *romid,unsigned char *arr,int arrlength)
{
  int i;
  unsigned char *newarr;

/*  unsigned char *temparr;
  unsigned char tempromid[16];
  int temparrlen;
  int tempromidlen;

  if ( (DS2480Speed == SPEEDSEL_OD)
    && (ser_getspeed(handle) <= 57600) )
  {
    ToggleCommandMode(handle);
    
    // Malloc space for incoming Data Array
    temparr = (unsigned char *)malloc(arrlength*2);
    if (temparr == NULL)
      return FALSE;

    // Pad the ROM ID and the data array.
    tempromidlen = DS2480Pad(romid,0,8,tempromid);
    temparrlen = DS2480Pad(arr,0,arrlength,temparr);

    // Malloc space for combined byte array.
    newarr = (unsigned char *)malloc(temparrlen+tempromidlen+3);
    if (newarr == FALSE)
      return FALSE;
    
    offset = 0;
    newarr[offset++] = CMD_COMM | FUNCTSEL_RESET | DS2480Speed | 0x01;
    newarr[offset++] = 0xE1; // Change to Data Mode
    newarr[offset++] = 0x55; // Search ROM Command
      
    // Copy the ROM ID in.
    for (i = 0;i < tempromidlen;i++)
      newarr[offset++] = tempromid[i];
      
    // Copy the Data Array in.
    for (i = 0;i < temparrlen;i++)
      newarr[offset++] = temparr[i];
      
    newarr[offset++] = 0xE3; // Change to Command Mode
    // Set up Infinite pull up.
    newarr[offset++] = CMD_CONFIG | PARMSEL_5VPULSE | PARMSET_infinite; 
    // Do the bit, and turn on the strong pull up.
    newarr[offset++] = CMD_COMM | FUNCTSEL_BIT | DS2480Speed | BITPOL_ONE | PRIME5V_TRUE;
    
    for (i = 0;i < offset;i++)
      printf("%02X ",newarr[i]);
    printf("\n");

    ser_write(handle,newarr,offset,timeout*offset);
    tcdrain(handle);
    ser_read(handle,newarr,(2+8+arrlength+2),timeout*(arrlength));

    for (i = 0;i < (2+8+arrlength+2);i++)
      printf("%02X ",newarr[i]);
    printf("\n");

    // Copy the returned data back.
    for (i = 10;i < (10+arrlength);i++)
      arr[i-10] = newarr[i];

    free (newarr);
    free (temparr);
    
    DS2480Mode = MODSEL_COMMAND;
    
    if ((newarr[0] & 0x03) != 0x01)
      return FALSE;
  }
  else*/
  {
    if (DS2480Reset(handle) != 1)
      return FALSE;

    newarr = (unsigned char *)malloc(9+arrlength);
    if (newarr == NULL)
      return FALSE;

    newarr[0] = 0x55;  
    for (i = 0;i < 8;i++)
      newarr[1+i] = romid[i];
    for (i = 0;i < arrlength;i++)
      newarr[9+i] = arr[i];

    printf("Send %d\n",arrlength+9);
    for (i = 0;i < (arrlength+9);i++)
      printf("%02X ",newarr[i]);
    printf("\n");

    DS2480Block(handle,newarr,arrlength+9);

    printf("Recv\n");
    for (i = 0;i < (arrlength+9);i++)
      printf("%02X ",newarr[i]);
    printf("\n\n");

    for (i = 0;i < arrlength;i++)
      arr[i] = newarr[9+i];
    free(newarr);

    DS2480SetParm(handle,PARMSEL_5VPULSE,PARMSET_infinite);
    DS2480BitPrime();
    return (DS2480Bit(handle,1)?(unsigned char)0:(unsigned char)1);
  }
  
/*
  return (TRUE);
*/
}

int DS2480Access(int handle,unsigned char *romid)
{
  int i;
  unsigned char arr[3+8*2];
  unsigned char tempromid[8*2];
  int tempromidlen;

  if ( (DS2480Speed == SPEEDSEL_OD)
       && (ser_getspeed(handle) <= 57600) )
  {
    ToggleCommandMode(handle);
    arr[0] = (unsigned char)
        (CMD_COMM | FUNCTSEL_RESET | DS2480Speed | 0x01); /* Reset */
    arr[1] = 0xE1; /* Change to data mode */
    arr[2] = 0x55; /* Match ROM */
    
    tempromidlen = DS2480Pad(romid,0,8,tempromid);
    
    /* Copy the ROM ID */
    for (i = 0;i < tempromidlen;i++)
      arr[3+i] = tempromid[i];
      
    ser_write(handle,arr,11,timeout*11);
    tcdrain(handle);
    ser_read(handle,arr,10,timeout*10-(8-tempromidlen));
    
    /* We leave the part in data mode */
    DS2480Mode = MODSEL_DATA;
    
    if ((arr[0] & 0x03) != 0x01)
      return FALSE;
  }
  else
  {
    if (DS2480Reset(handle) != 1)
      return FALSE;

    arr[0] = 0x55;  /* Match ROM */
    for (i = 0;i < 8;i++)
      arr[1+i] = romid[i];

    DS2480Block(handle,arr,9);
  }

  return TRUE;
}

int DS2480Reset(int handle)
{
  unsigned char command = 0x01;

  if (AuthFlag)
  {
    if (DS2480Mode != MODSEL_COMMAND)
      ToggleCommandMode(handle);

    command |= (unsigned char)(CMD_COMM | FUNCTSEL_RESET | DS2480Speed);

    if (ser_write(handle,&command,1,timeout) < 0) return(-1);
    tcdrain(handle);
    if (ser_read(handle,&command,1,timeout*2) < 0) return(-1);

    /* Get DS2480 version from Reset return byte */
    DS2480Version = (unsigned char)(command & RB_CHIPID_MASK);
    switch (command & RB_RESET_MASK)
    {
      case RB_1WIRESHORT:
        return (-1);
      case RB_PRESENCE:
        return(1);
      case RB_ALARMPRESENCE:
        return(2);
      case RB_NOPRESENCE:
        return(0);
      default:
        break;
    }
  }
  
  return(-1);
}

int DS2480Bit(int handle,unsigned char onebit)
{
  unsigned char command = 0x01;

  if (AuthFlag)
  {
    if (DS2480Mode != MODSEL_COMMAND)
      ToggleCommandMode(handle);

    command |= (unsigned char) (CMD_COMM | FUNCTSEL_BIT | DS2480Speed);
    command |= (unsigned char) ((onebit)?BITPOL_ONE:BITPOL_ZERO);

    if (BitPrime == 1)
    {
      command |= PRIME5V_TRUE;
      BitPrime = 0;
    }

    if (ser_write(handle,&command,1,timeout) < 0) return(-1);
    tcdrain(handle);
    if (ser_read(handle,&command,1,timeout) < 0) return(-1);

    if ((command & RB_BIT_MASK) == RB_BIT_ONE)
      return(1);
    else
      return(0);
  }
  
  return(-1);
}

int DS2480SearchTmp(int handle,unsigned char *buf)
{
  int i;
  unsigned char command_stream[24] = { 0 };

  if (AuthFlag)
  {
    if (DS2480Mode != MODSEL_COMMAND)
      ToggleCommandMode(handle);


    /* Send the Reset command separately because this could
       take as long as 5ms with alarming conditions.  Therefore,
       we cannot stream this command. 6/2/97 */
    if (DS2480Reset(handle) <= 0)
      return(-1);

    /* Change to data mode. */
    command_stream[0] = 0xE1;
    
    /* Send a Search ROM Command */
    command_stream[1] = 0xF0;
    
    /* Change to Command Mode */
    command_stream[2] = 0xE3;

    /* Turn DS2480 Search Mode On */
    command_stream[3] = 0x01;
    command_stream[3] |= (unsigned char)
        (CMD_COMM | FUNCTSEL_SEARCHON | DS2480Speed);

    /* Toggle into Data Mode */
    command_stream[4] = 0xE1;
    DS2480Mode = MODSEL_DATA;

    /* Copy in 16 bytes of data. */
    for (i = 0;i < 16;i++)
      command_stream[i+5] = buf[i];

    /* Toggle into Command Mode */
    command_stream[21] = 0xE3;
    
    /* Turn DS2480 Search Mode Off */
    command_stream[22] = 0x01;
    command_stream[22] |= (unsigned char)
        (CMD_COMM | FUNCTSEL_SEARCHOFF | DS2480Speed);

    /* Return to Data Mode */
    command_stream[23] = 0xE1;

    /* Send data */
    if ( ((DS2480Speed == SPEEDSEL_OD) && (ser_getspeed(handle) <= 57600))  \
        || (ser_getspeed(handle) == 9600) )
    {
      if (ser_write(handle,command_stream,24,timeout) < 0) return(-1);
      tcdrain(handle);
      if (ser_read(handle,command_stream,17,timeout*18) < 0) return(-1);
      /* 5 characters will not come back because they are not echoed. */
      for (i = 0;i < 16;i++)
        buf[i] = command_stream[1+i];
    }
    else
    {      
      ser_write(handle,command_stream,1,timeout);

      ser_write(handle,command_stream+1,1,timeout);
      ser_read(handle,command_stream,1,timeout);

      for (i = 2;i < 5;i++)
        ser_write(handle,command_stream+i,1,timeout);
       
      for (i = 5;i < 21;i++)
      {
        ser_write(handle,command_stream+i,1,timeout);
        tcdrain(handle);
        ser_read(handle,buf-5+i,1,timeout);
      }
      
      for (i = 21;i < 24;i++)
        ser_write(handle,command_stream+i,1,timeout);
    }

    return(1);
  }
  
  return(-1);
}

/* Gets 16 byte buffer, adds accoutrements, and does
   a ROM Search
*/
int DS2480Search(int handle,unsigned char *buf)
{
  int i;
  unsigned char command_stream[24] = { 0 };

  if (AuthFlag)
  {
    if (DS2480Mode != MODSEL_COMMAND)
      ToggleCommandMode(handle);

    /* Send the Reset command separately because this could
       take as long as 5ms with alarming conditions.  Therefore,
       we cannot stream this command. 6/2/97 */
    if (DS2480Reset(handle) <= 0)
      return(-1);

    /* Change to data mode. */
    command_stream[0] = 0xE1;
    
    /* Send a Search ROM Command */
    command_stream[1] = 0xF0;
    
    /* Change to Command Mode */
    command_stream[2] = 0xE3;

    /* Turn DS2480 Search Mode On */
    command_stream[3] = 0x01;
    command_stream[3] |= (unsigned char)
        (CMD_COMM | FUNCTSEL_SEARCHON | DS2480Speed);

    /* Toggle into Data Mode */
    command_stream[4] = 0xE1;
    DS2480Mode = MODSEL_DATA;

    /* Copy in 16 bytes of data. */
    for (i = 0;i < 16;i++)
      command_stream[i+5] = buf[i];

    /* Toggle into Command Mode */
    command_stream[21] = 0xE3;
    
    /* Turn DS2480 Search Mode Off */
    command_stream[22] = 0x01;
    command_stream[22] |= (unsigned char)
        (CMD_COMM | FUNCTSEL_SEARCHOFF | DS2480Speed);

    /* Return to Data Mode */
    command_stream[23] = 0xE1;

    /* Send data */
    if ( ((DS2480Speed == SPEEDSEL_OD) && (ser_getspeed(handle) <= 57600))  \
        || (ser_getspeed(handle) == 9600) )
    {
      if (ser_write(handle,command_stream,24,timeout) < 0) return(-1);
      tcdrain(handle);
      if (ser_read(handle,command_stream,17,timeout*18) < 0) return(-1);
      /* 5 characters will not come back because they are not echoed. */
      for (i = 0;i < 16;i++)
        buf[i] = command_stream[1+i];
    }
    else
    {      
      ser_write(handle,command_stream,1,timeout);

      ser_write(handle,command_stream+1,1,timeout);
      ser_read(handle,command_stream,1,timeout);

      for (i = 2;i < 5;i++)
        ser_write(handle,command_stream+i,1,timeout);
       
      for (i = 5;i < 21;i++)
      {
        ser_write(handle,command_stream+i,1,timeout);
        tcdrain(handle);
        ser_read(handle,buf-5+i,1,timeout);
      }
      
      for (i = 21;i < 24;i++)
        ser_write(handle,command_stream+i,1,timeout);
    }

    return(1);
  }
  
  return(-1);
}

unsigned char DS2480Byte(int handle,unsigned char inbyte)
{
  if (AuthFlag)
  {
    if (DS2480Mode != MODSEL_DATA)
      ToggleDataMode(handle);

    /* Double any 0xE3s so we don't go into command mode.
       Also double any 0xF3s with DS2480 Version 1. */
    if (  (inbyte == 0xE3) ||
         ((inbyte == 0xF3) && (DS2480Version == DS2480VERSION1)) )
    {
      ser_write(handle,&inbyte,1,timeout);
      tcdrain(handle);
    }
    if (ser_write(handle,&inbyte,1,timeout) == 1)
    {
#ifdef WIN32
      /*
       * Note: I tried to just jack up the timeout
       * on the read instead of looping, but there
       * appears to be something wrong with it because
       * it seems to return way too soon.
       */
      for (int i = 0; i < 20; ++i) {
        MikesMilliSleep(1);
        if (ser_read(handle,&inbyte,1,timeout) == 1) {
          return(inbyte);
        }
      }
      return(-1);
#else
      tcdrain(handle);
      if (ser_read(handle,&inbyte,1,timeout) == 1)
        return(inbyte);
      else
        return(-1);
#endif /* WIN32 */
    }
    else
      return(-1);
  }
  
  return(-1);
}

int DS2480Block(int handle,unsigned char *bytearr,int length)
{
  int i;
  unsigned char newarr[SERIAL_DS2480_BLKSIZE*2];
  int newarrlength;
  int offset;
  unsigned char bogusbyte;
  
  if (AuthFlag)
  {
    if (DS2480Mode != MODSEL_DATA)
      ToggleDataMode(handle);

    /* We are allowed to stream if one of 3 conditions hold.
       1. We are at Overdrive one wire speed.
       2. We are at a serial baudrate of 9600.
       3. We are using a DS2480A2 and are at a baudrate less than
          or equal to 19200.  The DS2480A2 has a version
          identifier of DS2480VERSION2.
    */
    if ( (DS2480Speed == SPEEDSEL_OD) \
       || (ser_getspeed(handle) == 9600) \
       || ((DS2480Version == DS2480VERSION2) && (ser_getspeed(handle) <= 19200)))
    {

      for (offset = 0;offset < length;offset += SERIAL_DS2480_BLKSIZE)
      {
        if ((length-offset) < SERIAL_DS2480_BLKSIZE)
        {
          newarrlength = DS2480Pad(bytearr,offset,length-offset,newarr);
          while (ser_read(handle,&bogusbyte,1,0) != -1);

          if (ser_write(handle,newarr,newarrlength,timeout*newarrlength) < 0) return(-1);
          tcdrain(handle);
          if (ser_read(handle,bytearr+offset,length-offset,timeout*newarrlength) < 0) return(-1);
        }
        else
        {
          newarrlength = DS2480Pad(bytearr,offset,SERIAL_DS2480_BLKSIZE,newarr);
          if (ser_write(handle,newarr,newarrlength,timeout*newarrlength) < 0) return(-1);
          tcdrain(handle);
          if (ser_read(handle,bytearr+offset,SERIAL_DS2480_BLKSIZE,timeout*SERIAL_DS2480_BLKSIZE) < 0) return(-1);
        }
      }
    }
    else
    {
      for (i = 0;i < length;i++)
        bytearr[i] = DS2480Byte(handle,bytearr[i]);
    }

    return(0);
  }
  return(-1);
}

int ToggleCommandMode(int handle)
{
  unsigned char command = 0xE3;
  if (AuthFlag)
  {
    if (DS2480Mode != MODSEL_COMMAND)
    {
      /* Send this command once to knock the 2480 out of data mode */
      if (ser_write(handle,&command,1,timeout) < 0) return(-1);

      DS2480Mode = MODSEL_COMMAND;
    }
    return(0);
  }
  
  return(-1);
}

int ToggleDataMode(int handle)
{
  unsigned char command = 0xE1;

  if (AuthFlag)
  {
    if (DS2480Mode != MODSEL_DATA)
    {
      if(ser_write(handle,&command,1,timeout) == 1)
      {
        DS2480Mode = MODSEL_DATA;
        return(0);
      }
      else
        return(-1);
    }
    else
      return(0);
  }
  
  return(-1);
}

unsigned char DS2480ToggleOverdrive()
{
  if (AuthFlag)
  {
    if (DS2480Speed == SPEEDSEL_OD)
    {
      DS2480OverdriveOff();
      return(0);
    }
    else
    {
      DS2480OverdriveOn();
      return(1);
    }
  }
  
  return(-1);
}

unsigned char DS2480OverdriveOn()
{
  if (AuthFlag)
  {
    DS2480Speed = SPEEDSEL_OD;
    return(1);
  }
  
  return(-1);
}

void DS2480OverdriveOff()
{
  if (AuthFlag)
    DS2480Speed = SPEEDSEL_STD;
}

int DS2480BitPrime()
{
  BitPrime = 1;
  
  return(1);
}

int DS2480StrongPUOn(int handle)
{
  unsigned char command = 0x01;

  if (AuthFlag)
  {
    if (DS2480Mode != MODSEL_COMMAND)
      ToggleCommandMode(handle);

    command |= CMD_COMM | BITPOL_5V | SPEEDSEL_PULSE;

    if (ser_write(handle,&command,1,timeout) < 0) return(-1);
    tcdrain(handle);

    return(0);
  }
  
  return(-1);
}

int DS2480StrongPUOff(int handle)
{
  unsigned char command = 0xF1;

  if (AuthFlag)
  {
    /* We must toggle into command mode before the F1 cancel
       will take effect!
    */
    if (DS2480Mode != MODSEL_COMMAND)
      ToggleCommandMode(handle);

    if (ser_write(handle,&command,1,timeout) < 0) return(-1);
    tcdrain(handle);
    /* We may or may not get a byte because the pull up may
       have terminated already.  So, return 1 no matter what.
    */
    if (ser_read(handle,&command,1,timeout) < 0) return(-1);

    return(1);
  }
  
  return(-1);
}

int DS2480Present(int handle)
{
  unsigned char command = 0x01;

  if (AuthFlag)
  {
    if (DS2480Mode != MODSEL_COMMAND)
      ToggleCommandMode(handle);

    command |= (unsigned char)(CMD_COMM | FUNCTSEL_RESET | DS2480Speed);

    if (ser_write(handle,&command,1,timeout) < 0) return(-1);
    tcdrain(handle);
    if (ser_read(handle,&command,1,timeout) < 0) return(-1);

    /* Check for the revision number to make sure we're
       talking to a DS2480 */
    if ( ((command & RB_CHIPID_MASK) == DS2480VERSION1) ||
         ((command & RB_CHIPID_MASK) == DS2480VERSION2) )
      return(1);
    else
      return(-1);
  }
  
  return(-1);
}

int DS2480SetSpeed(int handle,long speed)
{
  unsigned char command = 0x01;
  unsigned char command2 = 0x01;
  int fallback = 0;
  long oldspeed;

  if (AuthFlag)
  {
    /* Get the current baud rate. */
    oldspeed = ser_getspeed(handle);
    
    if (DS2480Mode != MODSEL_COMMAND)
      ToggleCommandMode(handle);

    command |= CMD_CONFIG | PARMSEL_BAUDRATE;
  
    switch(speed)
    {
      case 9600:
        command |= PARMSET_9600;
        break;
      case 19200:
        command |= PARMSET_19200;
        break;
      case 57600:
        command |= PARMSET_57600;
        break;
      case 115200:
        command |= PARMSET_115200;
        break;
      default:
        command = 0;
        break;
    }
  
    if (command != 0)
    {
      /* Tell DS2480 to change baudrate */
      if (ser_write(handle,&command,1,timeout) < 0) return(-1);
      /* Make sure buffer is flushed */
      MTIsleep (.01);
      tcdrain(handle);
      /* Then we change */
      ser_setspeed(handle,speed);
      /* Then we wait for things to settle */
      MTImillisleep(10);
      /* Flush any input we got */
      tcflush(handle,TCIFLUSH);
      
      command2 |= CMD_CONFIG | PARMSEL_PARMREAD | (PARMSEL_BAUDRATE >> 3);
      /* Verify that we can still talk */
      if (ser_write(handle,&command2,1,timeout) < 0)
        fallback = 1;
      /* Make sure buffer is drained. */
      tcdrain(handle);
      if (!fallback)
        if (ser_read(handle,&command2,1,timeout) < 0)
          fallback = 1;
      if (!fallback)
      {
        if ((command2 & 0x0E) == (command & 0x0E))
        {
          /* OK, speed select worked */
          return(1);
        }
        else
          fallback = 1;
      }

      /* Maybe the part is in data mode?  First, we try to kick 
         the part out of data mode.
      */
      if (fallback)
      {
        fallback = 0;
        /* Set to original speed */
        tcflush(handle,TCIOFLUSH);
        ser_setspeed(handle,oldspeed);
        /* Send an E3 */
        command = 0xE3;
        ser_write(handle,&command,1,timeout);
        tcdrain(handle);
        /* Then, send a reset */
        command = 0xC1;
        command2 = 0x00;
        ser_write(handle,&command,1,timeout);
        tcdrain(handle);
        if (ser_read(handle,&command2,1,timeout) < 0)
          fallback = 1;
        else
        {
          if ((command2 & 0xC0) != (command & 0xC0))
            fallback = 1;
        }
      }

      /* OK, now we try to resync at 9600 */
      if (fallback)
      {
        /* Error, fallback to 9600, resync */

#ifdef EMULATOR
        /* Put the DS5000 in monitor load mode. */
        ser_dtron(handle);
#else
        /* Drop power to the DS2480. */
        ser_dtroff(handle);
#endif        
        MTImillisleep(30);
#ifdef EMULATOR        
        /* Put the DS5000 in run mode. */
        ser_dtroff(handle);
#else
        /* Restore power to the DS2480. */
        ser_dtron(handle);
#endif        
	MTImillisleep(30);
        tcflush(handle,TCIOFLUSH);
        ser_setspeed(handle,9600);
        tcflush(handle,TCIOFLUSH);
        DS2480Reset(handle);
        return(0);
      }
    }
    else
    {
      return(-1);
    }
  }
  else
  {
    return(-1);
  }
  return(-1);
}

int DS2480SetParm(int handle,unsigned char setting,unsigned char value)
{
  unsigned char command = 0x01;
  unsigned char response = 0x01;

  if (AuthFlag)
  {
    if (DS2480Mode != MODSEL_COMMAND)
      ToggleCommandMode(handle);

    command |= (unsigned char)(CMD_CONFIG | setting | value);

    /* Change parameter */
    if (ser_write(handle,&command,1,timeout) < 0) return(-1);
    /* Make sure buffer is flushed */
    tcdrain(handle);
    /* Read back parameter return. */    
    if (ser_read(handle,&response,1,timeout) < 0) return(-1);

    if ((command & 0x7E) == (response & 0x7E))
      return(1);
    else
      return(-1);
  }
  
  return(-1);
}

int DS2480GetParm(int handle,unsigned char setting)
{
  unsigned char command = 0x01;

  if (AuthFlag)
  {
    if (DS2480Mode != MODSEL_COMMAND)
      ToggleCommandMode(handle);

    /* Read back parameter for checking */
    command |= (unsigned char)(CMD_CONFIG | PARMSEL_PARMREAD | (setting >> 3));
    if (ser_write(handle,&command,1,timeout) < 0) return(-1);
    tcdrain(handle);
    if (ser_read(handle,&command,1,timeout) < 0) return(-1);

    return(command);
  }
  
  return(-1);
}
