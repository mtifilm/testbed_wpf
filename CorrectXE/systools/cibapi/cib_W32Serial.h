/*
   File Name:  W32Serial.h
   Create:  July 30, 2000 by John Mertus

   This class does serial with time stamping.


*/
#ifndef _W32SERIAL_H
#define _W32SERIAL_H

#include "PropX.h"
#include "SafeClasses.h"
#include "systoolsDLL.h"

#ifdef DO_TIMESTAMPS
#include "MTI_UST.h"

/* This structure is used to pass information to the I/O compleations routine
   via the hEvent structure on overlapped I/O */
typedef struct _WSerialCompleationBuffer
  {
     void  *Serial;      // The Actual Class
     OVERLAPPED oBuff;   // Overlapp buffer
     char   Key;         // The address of the buffer that received the data
  } WSerialCompleationBuffer;


/* Each keystroke returns this  */
typedef struct _KeyStamp
  {
     char  Key;         // Key returnted
     bool  Error;       // True if an error
     MTI_INT64 UST;       // Universal Time Code
  } KeyStamp;
#endif /* DO_TIMESTAMPS */

/* This is the major serial input/output class   */
class CIB_WSerial
{
   public:

   string ErrorMessage;         // This is set to the ascii of an error

   // Properties of the communications port

   // from winbase.h:
   // BYTE Parity 
   //  #define NOPARITY            0
   //  #define ODDPARITY           1
   //  #define EVENPARITY          2
   //  #define MARKPARITY          3
   //  #define SPACEPARITY         4

   // BYTE StopBits
   //  #define ONESTOPBIT          0
   //  #define ONE5STOPBITS        1  (=1.5)
   //  #define TWOSTOPBITS         2

   // DTR Control Flow Values.
   //  #define DTR_CONTROL_DISABLE    0x00
   //  #define DTR_CONTROL_ENABLE     0x01
   //  #define DTR_CONTROL_HANDSHAKE  0x02

   // RTS Control Flow Values
   //  #define RTS_CONTROL_DISABLE    0x00
   //  #define RTS_CONTROL_ENABLE     0x01
   //  #define RTS_CONTROL_HANDSHAKE  0x02
   //  #define RTS_CONTROL_TOGGLE     0x03

   // DWORD BaudRate;       /* Baudrate at which running       */
   // BYTE ByteSize;        /* Number of bits/byte, 4-8        */
 
   PROPERTY(BYTE, Parity, GetParity, SetParity, CIB_WSerial);
   PROPERTY(unsigned, Baud, GetBaud, SetBaud, CIB_WSerial);
   PROPERTY(BYTE, Bits, GetBits, SetBits, CIB_WSerial);
   PROPERTY(BYTE, StopBits, GetStopBits, SetStopBits, CIB_WSerial);

   // Portname is almost open
//   SPROPERTY(string, PortName, fPortName, SetPortName, CIB_WSerial);
   PROPERTY(string, PortName, GetPortName, SetPortName, CIB_WSerial);
   RO_PROPERTY(bool, Opened, GetOpened, CIB_WSerial);

   // This open/closes up the interface
   bool Open(string Name);
   bool Close(void);

   void SendChar(char Key);
   int SendBuffer(const char *cp, int N);    // Sends N character in buffer ucp, return number sent

   // Usual constructor/Destructor
   CIB_WSerial(void);
   ~CIB_WSerial(void);

   /* Get character routines */
#ifdef DO_TIMESTAMPS
   bool Empty(void);       // True if no characters available
   bool Wait(int ms);      // Waits for next character
   KeyStamp GetKey(void);  // Returns the next keystamp
   KeyStamp GetKeyW(int ms); // Waits ms milliseconds for next character and returns it
#endif /* DO_TIMESTAMPS */
   int ReadBuffer(unsigned char *cp, int N, MTI_INT64 *sp=NULL);          // Get a buffer
   int ReadBuffer(unsigned char *cp, int N, int ms, MTI_INT64 *sp=NULL);  // Get a buffer, wait at most ms milliseconds

   /* Hide about everything */
   private:
   DCB  fdcb;            // Contains all the information about the port
   string fPortName;     // Name of the communications port
   HANDLE fhComm;        // Internal Handle for comm port
#ifdef DO_TIMESTAMPS
   HANDLE hRxThread;     // Receive thread handle
   bool AbortThread;     // Set to terminate the thread
   DWORD dwRxThreadId;   // Thread ID.
   int fQueuePackets;    // I/O to queue
#endif /* DO_TIMESTAMPS */

   int fRxTotal;         // Total receive characters
   int fTxTotal;         // Total characters transmitted

#ifdef DO_TIMESTAMPS
   CRingBuffer<KeyStamp> *cRing;
#endif /* DO_TIMESTAMPS */

   COMMTIMEOUTS ctmoOld; // Area to save timeouts on open for restore on close
   DCB  fdcbOld;         // Area to save the setting on open for restore on close


#ifdef DO_TIMESTAMPS
   // This is the virtual code that deals with the I/O compleation.
   void IOComplete(
                DWORD dwErrorCode,	                // completion code
                DWORD dwNumberOfBytesTransfered,	// number of bytes transferred
                LPOVERLAPPED lpOverlapped 	        // pointer to structure with I/O information
                );

   // This static procedure does nothing but dispatch the virtual function
   // IOComplete.
   static void WINAPI StaticIOComplete(
                DWORD dwErrorCode,	                // completion code
                DWORD dwNumberOfBytesTransfered,	// number of bytes transferred
                LPOVERLAPPED lpOverlapped 	        // pointer to structure with I/O information
                )
   {
     WSerialCompleationBuffer *b = (WSerialCompleationBuffer *)lpOverlapped->hEvent;
     reinterpret_cast<CIB_WSerial *>(b->Serial)->IOComplete(dwErrorCode, dwNumberOfBytesTransfered, lpOverlapped);
   };

   DWORD RxProc(void);

   static DWORD WINAPI StaticReceiveProc(CIB_WSerial *Serial)
   {
     return(Serial->RxProc());
   }
#endif /* DO_TIMESTAMPS */

   /*------------------Property Get/Setters-----------------------------*/
   //
   // Open the port
   //
   void SetPortName(string Value)
     {
        // On failure just return and do nothing
        // mbraca doesn't like this: if (!Open(Value)) throw this;
        // On success, set the port name
        fPortName = Value;
     };
   string GetPortName(void) {return(fPortName);};
   //
   // This just get the information about the port
   //
   void GetParameters(void)
     {
        if (!Opened) return;
        GetCommState(fhComm, &fdcb);
     };

   //
   // This just sets the information about the port
   //
   void SetParameters(void)
     {
        if (!Opened) return;
        SetCommState(fhComm, &fdcb);
     };

   // Used to get an error message
   void GetLastErrorMessage(bool bResult);

public:
   //
   // These are the internal representations
   // of the usual rates

   // Parity
   BYTE GetParity(void);
   void SetParity(BYTE Value);

   // Baud
   unsigned GetBaud(void);
   void SetBaud(unsigned Value);

   // DTR
   unsigned GetDTRControl(void);
   void SetDTRControl(unsigned Value);

   // RTS
   unsigned GetRTSControl(void);
   void SetRTSControl(unsigned Value);

   // Bits
   BYTE GetBits(void);
   void SetBits(BYTE Value);

   // Stop Bits
   BYTE GetStopBits(void);
   void SetStopBits(BYTE Value);

   // Simple inline functions to see if we are opened
   bool GetOpened(void) { return(fhComm != NULL); };
};

#endif
