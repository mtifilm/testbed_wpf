/*****************************************************************************
 *
 * sauthunx.h        
 *
 *   Main include file for sauthunx.c (iButton access system)
 *
 ****************************************************************************/

#define TRUE  1
#define FALSE 0

#define SERIAL_DS2480   0
#define PARALLEL_DS1481 1
#define MAXTYPE	        1
#define MAXFILENAMELEN  40

#define DO_RESET         0
#define DO_BIT           1 
#define DO_BYTE          2 
#define TOGGLE_OVERDRIVE 3 
#define TOGGLE_PASSTHRU  4 
#define CHECK_BRICK      5 

#define DS1410_KEY    0x135506C4; 
#define OUR_SEM_PERMS 0x1B6;      /* Any process is allowed sem access */

typedef struct _DOWComm
{
   unsigned char Command;
   unsigned char Xfer;
}
DOWComm, *PDOWComm;

/*
 *  DS1481 mode control functions
 */
unsigned char EnterPassthru(void);
unsigned char ExitPassthru(void);
void  TogglePassthru(void);
unsigned char iBOverdriveOn(void);
void  iBOverdriveOff(void);

/*
 *   These functions communicate directly with DOW PDD or TTY driver.
 */
unsigned char DOWReset(void);
unsigned char DOWBit(unsigned char);
int DOWByte(unsigned char);
unsigned char DOWBlock(unsigned char *,int);
unsigned char ToggleOverdrive(void);
int Send4(void);
int DS1481Present(void);

/*
 *   Implements DOW ROM search algorithm.
 */
unsigned char RomSearch(void);

/*
 *  Access system functions
 */
unsigned char iBDOWCheck(void);                 
unsigned char iBKeyOpen(void);
unsigned char iBKeyClose(void);
unsigned char iBSetup(void);
unsigned char iBNext(void);
unsigned char iBFirst(void);
unsigned char iBAccess(void);
unsigned char iBFastAccess(void);
unsigned char gndtest(void);
unsigned char iBDataByte(unsigned char, unsigned char *);
unsigned char iBDataBlock(unsigned char *, int);
unsigned char *iBROMData(void);
unsigned char SetAdapterSpeed(unsigned long);
unsigned char SetAdapterType(unsigned char, char *);
