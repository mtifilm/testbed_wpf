/*
ds2480.h
*/

/* Version strings */
#define DS2480VERSION1			0x04
#define DS2480VERSION2			0x08

/* Return byte value */
#define RB_CHIPID_MASK			0x1C

#define RB_RESET_MASK			0x03
#define RB_1WIRESHORT			0x00
#define RB_PRESENCE			0x01
#define RB_ALARMPRESENCE		0x02 
#define RB_NOPRESENCE			0x03

#define RB_BIT_MASK			0x03
#define RB_BIT_ONE			0x03
#define RB_BIT_ZERO			0x00

/* Masks for all bit ranges */
#define CMD_MASK			0x80
#define FUNCTSEL_MASK			0x60
#define BITPOL_MASK			0x10
#define SPEEDSEL_MASK			0x0C
#define MODSEL_MASK			0x02
#define PARMSEL_MASK			0x70
#define PARMSET_MASK			0x0E

/* Command or config bit */
#define CMD_COMM			0x80
#define CMD_CONFIG			0x00

/* Function select bits */
#define FUNCTSEL_BIT			0x00
#define FUNCTSEL_SEARCHON		0x30
#define FUNCTSEL_SEARCHOFF		0x20
#define FUNCTSEL_RESET			0x40
#define FUNCTSEL_CHMOD			0x60

/* Bit polarity/Pulse voltage bits */
#define BITPOL_ONE			0x10
#define BITPOL_ZERO			0x00
#define BITPOL_5V			0x00
#define BITPOL_12V			0x10

/* One Wire speed bits */
#define SPEEDSEL_STD			0x00
#define SPEEDSEL_FLEX			0x04
#define SPEEDSEL_OD			0x08
#define SPEEDSEL_PULSE			0x0C

/* Data/Command mode select bits */
#define MODSEL_DATA			0x00
#define MODSEL_COMMAND			0x02

/* 5V Follow Pulse select bits (If 5V pulse
   will be following the next byte or bit.) */
#define PRIME5V_TRUE			0x02
#define PRIME5V_FALSE			0x00

/* Parameter select bits */
#define PARMSEL_PARMREAD		0x00
#define PARMSEL_SLEW			0x10
#define PARMSEL_12VPULSE		0x20
#define PARMSEL_5VPULSE			0x30
#define PARMSEL_WRITE1LOW		0x40
#define PARMSEL_SAMPLEOFFSET		0x50
#define PARMSEL_ACTIVEPULLUPTIME	0x60
#define PARMSEL_BAUDRATE		0x70

/* Pull down slew rate. */
#define PARMSET_Slew0pF			0x00
#define PARMSET_Slew90pF		0x02
#define PARMSET_Slew180pF		0x04
#define PARMSET_Slew270pF		0x06
#define PARMSET_Slew360pF		0x08
#define PARMSET_Slew450pF		0x0A
#define PARMSET_Slew540pF		0x0C
#define PARMSET_Slew630pF		0x0E

/* 12V programming pulse time table */
#define PARMSET_32us			0x00
#define PARMSET_64us			0x02
#define PARMSET_128us			0x04
#define PARMSET_256us			0x06
#define PARMSET_512us			0x08
#define PARMSET_1024us			0x0A
#define PARMSET_2048us			0x0C
#define PARMSET_infinite		0x0E

/* 5V strong pull up pulse time table */
#define PARMSET_16p4ms			0x00
#define PARMSET_65p5ms			0x02
#define PARMSET_131ms			0x04
#define PARMSET_262ms			0x06
#define PARMSET_524ms			0x08
#define PARMSET_1p05s			0x0A
#define PARMSET_2p10s			0x0C
#define PARMSET_infinite		0x0E

/* Write 1 low time */
#define PARMSET_Write8us		0x00
#define PARMSET_Write9us		0x02
#define PARMSET_Write10us		0x04
#define PARMSET_Write11us		0x06
#define PARMSET_Write12us		0x08
#define PARMSET_Write13us		0x0A
#define PARMSET_Write14us		0x0C
#define PARMSET_Write15us		0x0E

/* Data sample offset and Write 0 recovery time */
#define PARMSET_SampOff4us		0x00
#define PARMSET_SampOff5us		0x02
#define PARMSET_SampOff6us		0x04
#define PARMSET_SampOff7us		0x06
#define PARMSET_SampOff8us		0x08
#define PARMSET_SampOff9us		0x0A
#define PARMSET_SampOff10us		0x0C
#define PARMSET_SampOff11us		0x0E

/* Active pull up on time */
#define PARMSET_PullUp0p0us		0x00
#define PARMSET_PullUp0p5us		0x02
#define PARMSET_PullUp1p0us		0x04
#define PARMSET_PullUp1p5us		0x06
#define PARMSET_PullUp2p0us		0x08
#define PARMSET_PullUp2p5us		0x0A
#define PARMSET_PullUp3p0us		0x0C
#define PARMSET_PullUp3p5us		0x0E
 
/* Baud rate bits */
/*
Old Baud rate bits
#define PARMSET_9600			0x00
#define PARMSET_38400			0x02
#define PARMSET_57600			0x04
#define PARMSET_115200			0x06
*/

#define PARMSET_9600			0x00
#define PARMSET_19200			0x02
#define PARMSET_57600			0x04
#define PARMSET_115200			0x06
