/*
   File Name:  WSerial.h
   Created:  July 30, 2000 by John Mertus

   This is a fake header, the real header, which depends upon the operating
   system is correctly loaded.

   Currently, there is only one header for windows
*/

#ifndef _WSERIAL_H
#define _WSERIAL_H

#include "machine.h"

#ifdef _WINDOWS
#include "cib_W32Serial.h"
#else
#error No header for this operating system
#endif

#endif
 