/*****************************************************************************
 *
 *    Access system layer the the CiB API.          { Ver 0.01  10/24/96 }
 *         
 *   Copyright (C) 1994, 1996 Dallas Semiconductor Corporation. 
 *   All rights Reserved. Printed in U.S.A.
 *   This software is protected by copyright laws of
 *   the United States and of foreign countries.
 *   This material may also be protected by patent laws of the United States 
 *   and of foreign countries.
 *   This software is furnished under a license agreement and/or a
 *   nondisclosure agreement and may only be used or copied in accordance
 *   with the terms of those agreements.
 *   The mere transfer of this software does not imply any licenses
 *   of trade secrets, proprietary technology, copyrights, patents,
 *   trademarks, maskwork rights, or any other form of intellectual
 *   property whatsoever. Dallas Semiconductor retains all ownership rights.
 *
 *****************************************************************************/
#include "machine.h"
#ifdef WIN32
typedef unsigned short ushort;
typedef unsigned long ulong;
#endif /* WIN32 */

#include <fcntl.h>  
#include <signal.h>
#ifndef WIN32
#include <unistd.h>
#endif /* not WIN32 */
#include <sys/types.h>
#ifndef WIN32
#include <sys/ipc.h>
#include <sys/sem.h>
#endif /* not WIN32 */
#include <sys/stat.h>
#include <errno.h>
#include <stdio.h>

#include "ds2480.h"
#include "ds2480com.h"
#include "serial.h"
#include "sauthunx.h"
#include "MTIsleep.h"

/* 
 * State and mode flags 
 */
unsigned char SetupOk = FALSE; 
unsigned char FailNext;
unsigned char AccessGood;
unsigned char Passthru = TRUE;
unsigned char AuthFlag = FALSE;

#ifndef WIN32
/* Semaphore stuff */
static unsigned char sem_init = 0;
static int   sem_id;
static struct sembuf sops;
/* End of Semaphore stuff */
#endif /* not WIN32 */

/* 
 * ROM search depth specifier 
 */
unsigned char l0 = 0;

/* 
 * ROM data buffer 
 */
unsigned char RomDta[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };
int DS2480SearchTmp(int handle,unsigned char *buf);

/* 
 * Xmit structure used for communication with parallel port driver 
 */
DOWComm DS1481Comm;

/* 
 * Port/Adapter configuration vars 
 */
int   FileDes;
unsigned char Type = SERIAL_DS2480;
unsigned char PortNum = 0;
char  FileName[MAXFILENAMELEN]; 
int   returnval;

unsigned char iBKeyOpen()
{
#ifndef WIN32
   int semflag;
   key_t DS1410_key;

#ifdef HAS_UNION_SEMUN
   union semun semarg;
#else
   union semun {
     int val;
     struct semid_ds *buf;
     ushort *array;
   } semarg;
#endif

   if (!sem_init)
   {
     /* Try to create semaphore */
     DS1410_key = DS1410_KEY;
     semflag = IPC_CREAT | IPC_EXCL | OUR_SEM_PERMS;
     sem_id = semget(DS1410_key, 1, semflag);
                  
     if (sem_id == -1)
     {
       /* Already created */
       semflag = IPC_EXCL | OUR_SEM_PERMS;
       sem_id = semget(DS1410_key, 1, semflag);
     }
     else
     {
       /* Initialize semval */
       semarg.val = 1;
       semctl(sem_id, 0, SETVAL, semarg);
     }

     sem_init = TRUE;
   }
#endif /* not WIN32 */

   if (!AuthFlag)
   {
#ifndef WIN32
      /* Set up struct sops to change sem value, with UNDO */
      sops.sem_num = 0;
      sops.sem_op  = -1;
      sops.sem_flg = SEM_UNDO;

      /* Block here until semaphore is released. */
      semop(sem_id, &sops, 1);
#endif /* not WIN32 */

      /* Open the file */
      switch (Type)
      {
        case SERIAL_DS2480:
          FileDes = ser_gethandle(FileName);
          break;
#ifndef WIN32
        case PARALLEL_DS1481:
          /* If we've used iBSetup, PortNum will not be zero. 
             iBSetup denotes use of old driver call to /dev/dow0.
          */
          if (PortNum == 0)
            FileDes = open(FileName,O_RDWR | O_NONBLOCK);
          else
            FileDes = open("/dev/dow0",O_RDWR | O_NONBLOCK);
          break;
#endif /* not WIN32 */
        default:
          FileDes = -1;
          break;
      }

      if (FileDes < 0)
      {
#ifndef WIN32
        /* Release and set semval back to 1 */
        semarg.val = 1;
        semctl(sem_id, 0, SETVAL, semarg);
#endif /* not WIN32 */
        return(AuthFlag);
      }

      AuthFlag = TRUE;

      /* Get the adapter ready to work */
      switch (Type)
      {
        case SERIAL_DS2480:
          /* We need to set the speed to 9600 and send a reset first
             just in case the part is in calibration mode. 6/2/97 */
          ser_setspeed(FileDes,9600);
          /* This reset is necessary for the part to calibrate.
             New as of 6/2/97 */
          DS2480Reset(FileDes);
          SetAdapterSpeed(9600);
          break;
#ifndef WIN32
        case PARALLEL_DS1481:
          ExitPassthru();
          break;
#endif /* not WIN32 */
      }
   } 

   return AuthFlag;
}

unsigned char iBKeyClose()
{
#ifndef WIN32

#ifdef HAS_UNION_SEMUN
   union semun semarg;
#else
   union semun {
     int val;
     struct semid_ds *buf;
     ushort *array;
   } semarg;
#endif
#endif /* not WIN32 */

   if (AuthFlag)
   {
      switch(Type)
      {
        case SERIAL_DS2480:
          ToggleCommandMode(FileDes);
          DOWReset();
          SetAdapterSpeed(9600);
          break;
#ifndef WIN32
        case PARALLEL_DS1481:
          EnterPassthru();
          break;
#endif /* not WIN32 */
      }

      ser_close(FileDes);

#ifndef WIN32
      /* Set semval back to 1 */
      semarg.val = 1;
      semctl(sem_id, 0, SETVAL, semarg);
#endif /* not WIN32 */

      MTImillisleep(10);
      AuthFlag = FALSE;
   } 

   return TRUE;
}

/* 
 *   See if device driver has been installed 
 */
unsigned char iBDOWCheck()                 
{
  struct stat buf;

  switch(Type)
  {
    case SERIAL_DS2480:
    case PARALLEL_DS1481:
      /* Check to see if file exists */
      if (stat(FileName, &buf) != -1)
        return TRUE;
      break;

    default:
      break;
  }
 
  return FALSE;
}      

unsigned char EnterPassthru()
{
  TogglePassthru();
  Passthru = TRUE;

  return TRUE;
}

unsigned char ExitPassthru()
{
  int i = 0;

  Passthru = TRUE;

  while (Passthru && (i++ < 3))
  {
    if (Passthru)
      TogglePassthru();

    Passthru = !DS1481Present();
  }

  iBOverdriveOff(); 

  return !Passthru; 
}

void TogglePassthru()
{
  Send4();

  /* Wait for mode to change */
  MTImillisleep(1600);  
}

void FastSleep(ushort MSec)
{
   MTImillisleep(MSec * 100);
}

unsigned char iBOverdriveOn()
{
  if (!DOWReset())
    return FALSE;
 
  /* Put all devices (that support overdrive) in overdrive mode */
  DOWByte(0x3C);

  return ToggleOverdrive() ? (unsigned char)TRUE : ToggleOverdrive();
}

void iBOverdriveOff()
{
  if (ToggleOverdrive())
    ToggleOverdrive();

  return;
}

unsigned char SetAdapterType(unsigned char type,char *port)
{
  unsigned char RetCode;
  int   i;
  
  RetCode = TRUE;

  switch (type)
  {
    case PARALLEL_DS1481:
      /* Set Parallel port number to zero. */
      PortNum = 0;
    case SERIAL_DS2480:
      i = 0;
      while ((port[i] != 0x00) && (i < (MAXFILENAMELEN-1)))
      {
        FileName[i] = port[i];
        i++;
      }
      if (port[i] == 0x00)
      {
        FileName[i] = 0x00;
        Type        = type;
      }
      else
        RetCode = FALSE; 
      break;

    default:
      RetCode = FALSE;
      break;
  }

  return RetCode;
}

unsigned char SetAdapterSpeed(ulong speed)
{
  switch (Type)
  {
    case SERIAL_DS2480:
      if (DS2480SetSpeed(FileDes,speed) >= 0)
      {
        return TRUE;
      }
      else
      {
        return FALSE;
      }

    default:
      break;
  }
  
  return TRUE;
}

unsigned char SetAdapter5VTime(unsigned char time)
{
  switch (Type)
  {
    case SERIAL_DS2480:
      if (DS2480SetParm(FileDes,PARMSEL_5VPULSE,time) >= 0)
      {
        return TRUE;
      }
      else
      {
        return FALSE;
      }

    default:
      break;
  }
  
  return TRUE;
}

unsigned char Adapter5VPrime()
{
  DS2480BitPrime();
  
  return TRUE;
}

unsigned char Adapter5VCancel()
{
  switch (Type)
  {
    case SERIAL_DS2480:
      if (DS2480StrongPUOff(FileDes) >= 0)
      {
        return TRUE;
      }
      else
      {
        return FALSE;
      }

    default:
      break;
  }
  
  return FALSE;
}

unsigned char Adapter5VActivate(ulong ustimeout)
{
  switch (Type)
  {
    case SERIAL_DS2480:
      if (DS2480StrongPUOn(FileDes) >= 0)
      {
        MTImillisleep(ustimeout/10);
        /* Turn off just to make sure */
        DS2480StrongPUOff(FileDes);
        return TRUE;
      }
      else
      {
        return FALSE;
      }

    default:
      break;
  }
  
  return TRUE;
}

unsigned char iBSetup()
{
  struct stat buf;

  SetupOk = FALSE;

  switch(Type)
  {
    case SERIAL_DS2480:
      /* Check to see if file exists */
      if (stat(FileName, &buf) != -1)
        SetupOk = FailNext = AccessGood = TRUE;
      break;
    default:
      break;
  }
 
  return SetupOk;                       
}

/*
 *  Find next part on DOW bus 
 */
unsigned char iBNext()                                          
{
   unsigned char tr;

   if (SetupOk)
   {
      /* See if last search found last button */
      if (FailNext)                 
      {
         FailNext = FALSE; 
         /* Reset next function  */
         l0 = 0;                                    
      } 
      else if ((tr = RomSearch()) != 0)
      {
         /* See if we should force failure */
         if (tr == 2) 
            FailNext = TRUE;                 
        
         return TRUE;
      }
   }

   return FALSE;                                
}

/* 
 *  Find first DOW part on specified port  
 */
unsigned char iBFirst()                     
{
   /* Don't force a failure here  */
   FailNext = FALSE;              
   /* Point Rom Search algorithm to the top */
   l0 = 0;                         

   /* Go look for the first DOW part on the bus */
   return iBNext();             
}

unsigned char iBRefresh()                     
{
  int i;
  unsigned char SearchBuf[16];

      for (i = 0;i < 16;i++) SearchBuf[i] = 0x00;
printf ("FileDes is %d\n", FileDes);
      if (DS2480SearchTmp(FileDes,SearchBuf) < 0)
       {
printf ("TTT unintended error \n");
       }

return TRUE;


#ifdef TESTING
   /* Don't force a failure here  */
   FailNext = FALSE;              
   /* Point Rom Search algorithm to the top */
   l0 = 0;                         

   /* Go look for the first DOW part on the bus */

   if (SetupOk)
   {
      /* See if last search found last button */
      if (FailNext)                 
      {
         FailNext = FALSE; 
         /* Reset next function  */
         l0 = 0;                                    
      } 
      else 
      {

{
  unsigned char i = 0, x = 0, ld = l0;
  unsigned char RomBit, Mask;
  unsigned char SearchBuf[16];

      for (i = 0;i < 16;i++) SearchBuf[i] = 0x00;

      /* Returns false if Presence Pulse not detected. */
      if (DS2480SearchTmp(FileDes,SearchBuf) < 0)
       {
printf ("TTT unintended error \n");
        tr = 0;
       }

return TRUE;

#ifdef TESTING
      /* Check for invalid search */
      if ((SearchBuf[15] & 0xC0) == 0xC0)
        x = 3;

      /* Copy into the RomDta buffer */
      for (i = 0;i < 8;i++) RomDta[i] = 0x00;
      i = 0;
      while (i++ < 64)
      {
        RomBit = (SearchBuf[(i - 1) / 4] >> (((i - 1) % 4) * 2 + 1)) & 0x01;
        RomDta[(i - 1) / 8] |= (RomBit << (i - 1)%8);
      }
      /* Find the last discrepancy */
      i = 64;
      while (i > 0)
      {
        if ( SearchBuf[(i - 1) / 4] & (0x01 << (((i-1)%4) * 2)) &&
             !(SearchBuf[(i - 1) / 4] & (0x01 << (((i-1)%4) * 2 + 1))) )
        {
          l0 = i;
          i = 1;
        }
        i--;
      }
 
  if (x == 3) 
   tr = 0 ;
  else
   tr =  1 + (ld == l0);    
#endif
}
printf ("TTT tr:  %d\n", tr);
       if (tr != 0)
        {
         /* See if we should force failure */
         if (tr == 2) 
            FailNext = TRUE;                 
        
         return TRUE;
        }
      }
   }

   return FALSE;                                
#endif
}


/*
 *  "Strong" access
 */
unsigned char iBStrongAccess()               
{
   unsigned char i, j;             
   unsigned char mask = 0x80;
   unsigned char byte;
   unsigned char bit;
   unsigned char bytesend[24]; 

   /* Assume failure  */
   AccessGood = FALSE;                                   

   /* Send reset pulse */
   if (DOWReset())                                      
   {
      /* ROM search command byte */
      DOWByte(0xF0);                             

      switch (Type)
      {
        case SERIAL_DS2480:
          /* Setup the 24 byte search block */
          for (i = 0;i < 24;i++)
            bytesend[i] = 0xFF;
          byte = 0;
          bit = 2;
          for (i = 0;i < 8;i++)
          {
            for (j = 0;j < 8;j++)
            {
              if (((RomDta[i] >> j) & 1) != 1)
                bytesend[byte] &= (unsigned char)(~(mask >> (7 - bit)));
              bit += (unsigned char)3;
              if (bit >= 8)
                byte++;
              bit = (unsigned char)(bit % 8);
            }
          }
          /* Send the block. */
          DOWBlock(bytesend,24);
          /* Check the block, make sure we talked to a part. */
          byte = 0;
          bit = 2;
          for (i = 0;i < 8;i++)
          {
            for (j = 0;j < 8;j++)
            {
              if ( ((bytesend[byte] >> bit) & 1) != \
                   ((RomDta[i] >> j) & 1) )
              {
                return(FALSE);
              }
              bit += (unsigned char)3;
              if (bit >= 8)
                byte++;
              bit = (unsigned char)(bit % 8);
            }
          }
          break;
#ifndef WIN32
        case PARALLEL_DS1481:
          /* Byte loop */
          for (i = 0; i < 8; i++)          
          {
            /* Bit loop */
            for (j = 0; j < 8; j++)      
            {
              /* Send two read time slots */
              if (((DOWBit(TRUE) << 1) | DOWBit(TRUE)) == 3)
                return FALSE;         

              /* Send write time slot */
              DOWBit((unsigned char) ((RomDta[i] >> j) & 1));
            }      
          }
          break;
#endif /* not WIN32 */
        default:
          break;
      }
      
      /* Success if we made it through all bits */
      AccessGood = TRUE;      
   }

   return AccessGood;  
}

/*
 *  Reset buttons on port.
 */
unsigned char iBReset()
{
  return(DOWReset());
}

/* 
 *  Xmit data to buttons on the port 
 */
unsigned char iBDataByte(unsigned char br, unsigned char *ucRet)
/*
	KM:  Change the behavior of this function to support
	meaningful error reporting.
*/
{
  int iRet;

  if (SetupOk && AccessGood)
   {
    iRet = DOWByte(br);
    if (iRet != -1)
     {
      *ucRet = (unsigned char) iRet;
      return TRUE;
     }
    else
     {
      return FALSE;
     }
   }
  else
   {
    return FALSE;
   }
}

unsigned char iBDataBit(unsigned char br)
{
  return (SetupOk && AccessGood) ? DOWBit(br) : br;
}

unsigned char iBDataBlock(unsigned char *barr, int length)
{
  return (SetupOk && AccessGood) ? DOWBlock(barr,length) : barr[0];
}

unsigned char iBStreamRS(unsigned char *arr,int length,int run)
{
  int i;
  unsigned char AccessGood = FALSE;
  unsigned char accessarray[9];
  
  switch (Type)
  {
    case SERIAL_DS2480:
      AccessGood = DS2480StreamRS(FileDes,RomDta,arr,length);
      break;
      
    default:
      if (iBReset())
      {
        AccessGood = TRUE;
  
        /* Set up block for transmit */
        accessarray[0] = 0x55;
        for (i = 1;i < 9;i++)
          accessarray[i] = RomDta[i-1];
        /* Transmit block. */    
        iBDataBlock(accessarray,9);
        iBDataBlock(arr,length);
        if (run == 1)
        {
          SetAdapter5VTime(PARMSET_infinite);
          Adapter5VPrime();
          AccessGood = (iBDataBit(1)?(unsigned char)0:(unsigned char)1);
        }
      }
      break;
  }

  return AccessGood;
}

unsigned char iBStream(unsigned char *arr,int length)
{
  int i;
  unsigned char AccessGood = FALSE;
  unsigned char accessarray[9];
  
  switch (Type)
  {
    case SERIAL_DS2480:
      AccessGood = DS2480Stream(FileDes,RomDta,arr,length);
      break;
      
    default:
      if (iBReset())
      {
        AccessGood = TRUE;
  
        /* Set up block for transmit */
        accessarray[0] = 0x55;
        for (i = 1;i < 9;i++)
          accessarray[i] = RomDta[i-1];
        /* Transmit block. */    
        iBDataBlock(accessarray,9);
        iBDataBlock(arr,length);
      }
      break;
  }

  return AccessGood;
}

unsigned char iBAccess()
{
  int i;
  unsigned char accessarray[9];
  
  AccessGood = FALSE;

  switch (Type)
  {
    case SERIAL_DS2480:
      AccessGood = (unsigned char)DS2480Access(FileDes,RomDta);
      break;
      
    default:
      if (iBReset())
      {
        AccessGood = TRUE;
  
        /* Set up block for transmit */
        accessarray[0] = 0x55;
        for (i = 1;i < 9;i++)
          accessarray[i] = RomDta[i-1];
        /* Transmit block. */    
        iBDataBlock(accessarray,9);
      }
      break;
  }
  
  return AccessGood;
}

unsigned char iBFastAccess()
{
   return iBAccess();
}

unsigned char gndtest()
{
   if (SetupOk)
   {
      /* Put parts on bus in known state */
      DOWReset();                        
      /* Return result of write 1 time slot */
      return DOWBit(TRUE);
   }

   return FALSE;
}

unsigned char *iBROMData()
{
   /* Return pointer to ROM Data buffer  */
   return RomDta;                     
}

/*****************************************************************************
 *
 *    This function performs a ROM search and finds one part on the DOW bus
 * per call.
 *
 *     Return values : 0 => No parts on bus or bus error
 *                     1 => A part was found and more are out there
 *                     2 => A part was found and it was last one on the bus
 *
 */
unsigned char RomSearch()
{
  unsigned char i = 0, x = 0, ld = l0;
  unsigned char RomBit;
  unsigned char SearchBuf[16];
#ifndef WIN32
  unsigned char Mask;
#endif

  switch (Type)
  {
    case SERIAL_DS2480:
      /* Set up 16 bytes of ROMID data */
      for (i = 0;i < 16;i++) SearchBuf[i] = 0x00;
      /* If we're not doing a first. */
      if (l0 != 0)
      {
        i = 0;
        while (i++ < 64)
        {
          if (i >= ld)
          {
            if (i == ld)
              RomBit = 1;
            else
              RomBit = 0;
          }
          else
            RomBit = (unsigned char)((RomDta[(i - 1) / 8] >>
                ((i - 1) % 8)) & 0x01);
          SearchBuf[(i - 1) / 4] |= (unsigned char)(RomBit << (((i-1)%4)*2+1));
        }
      }

      /* Returns false if Presence Pulse not detected. */
      if (DS2480Search(FileDes,SearchBuf) < 0)
        return(FALSE);

      /* Check for invalid search */
      if ((SearchBuf[15] & 0xC0) == 0xC0)
        x = 3;

      /* Copy into the RomDta buffer */
      for (i = 0;i < 8;i++) RomDta[i] = 0x00;
      i = 0;
      while (i++ < 64)
      {
        RomBit = (unsigned char)((SearchBuf[(i - 1) / 4] >>
                 (((i - 1) % 4) * 2 + 1)) & 0x01);
        RomDta[(i - 1) / 8] |= (unsigned char)(RomBit << (i - 1)%8);
      }
      /* Find the last discrepancy */
      i = 64;
      while (i > 0)
      {
        if ( SearchBuf[(i - 1) / 4] & (0x01 << (((i-1)%4) * 2)) &&
             !(SearchBuf[(i - 1) / 4] & (0x01 << (((i-1)%4) * 2 + 1))) )
        {
          l0 = i;
          i = 1;
        }
        i--;
      }
      break;
#ifndef WIN32
    case PARALLEL_DS1481:
      /* Reset DOW bus */
      if (DOWReset())     
      {
        /* Send search command */
        DOWByte(0xF0);   
      }
      else
        return FALSE;

      /* While not done and bus error */
      while ((i++ < 64) && (x < 3))            
      {
        /* Get bit mask */
        Mask = 1 << ((i - 1) % 8);           

        RomBit = RomDta[(i - 1) >> 3] & Mask ? TRUE : FALSE; 

        /* Send first read time slot */
        x = DOWBit(TRUE) << 1; 
        /* Send second read time slot */
        x |= DOWBit(TRUE);      
  
        /* Is there a disagreement in this bit position */
        if (!x)               
        {
          /* Stay on old path or pick a new one ?  */
          if (i >= ld)              
            RomBit = (i == ld);   /* Send write 1 if at position of ld  */

          /* Save this value as temp last disagreement  */
          if (!RomBit)         
            l0 = i;
        } 
        else 
          RomBit = (x & 1) ^ 1;    /* Get lsb of x and flip it  */

        /* Send write time slot */
        DOWBit(RomBit);                               

        if (RomBit)              
        {
          /* Set bit in Rom Data byte */
          RomDta[(i - 1) >> 3] |= Mask;          
        }
        else
        {
          /* Clear bit in ROM data byte */
          RomDta[(i - 1) >> 3] &= (Mask ^ 0xFF); 
        }
      }
      break;
#endif /* not WIN32 */
    default:
      break;
  }
 
  return (x == 3) ? (unsigned char)0 : (unsigned char)(1 + (ld == l0));    
}

unsigned char DOWReset()
{
  if (AuthFlag)
  {
    switch(Type)
    {
      case SERIAL_DS2480:
        if (DS2480Reset(FileDes) == 1)
          return(1);
        else
          return(0);
#ifndef WIN32
      case PARALLEL_DS1481:
        /* Assign command byte */
        DS1481Comm.Command = DO_RESET;                 
        /* Set port number */
        DS1481Comm.Xfer = PortNum;

        /* Send info to DOW driver */
        write(FileDes, &DS1481Comm.Command, 2);      
        /* Get result */
        read(FileDes, &DS1481Comm.Xfer, 1);         

        /* Return result of reset operation */
        return DS1481Comm.Xfer;
#endif /* not WIN32 */
      default:
        return FALSE;
    }
  }
  /* Return FALSE if process did not have control of bus */
  return FALSE;     
}

/*
 *  Send bit to 1-wire bus
 */
unsigned char DOWBit(unsigned char BitVal)
{
  if (AuthFlag)
  {
    switch(Type)
    {
      case SERIAL_DS2480:
        if (DS2480Bit(FileDes,BitVal) == 1)
          return(1);
        else
          return(0);
#ifndef WIN32
      case PARALLEL_DS1481:
        /* Assign command byte */
        DS1481Comm.Command = DO_BIT;               
        /* Byte to send to 1-wire bus */ 
        DS1481Comm.Xfer = BitVal;               
        /* Send packet to driver */
        write(FileDes, &DS1481Comm.Command, 2);
        /* Get result of write */
        read(FileDes, &DS1481Comm.Xfer, 1);            

        return DS1481Comm.Xfer; 
#endif /* not WIN32 */
      default:
        return(FALSE);
    }  
  }
  return BitVal;
}

/*
 *  Send byte to 1-wire bus 
 */   
int DOWByte(unsigned char ByteVal)                      
{
  if (AuthFlag)
  {
    switch(Type)
    {
      case SERIAL_DS2480:
        return(DS2480Byte(FileDes,ByteVal));

#ifndef WIN32
      case PARALLEL_DS1481:
        /* Assign command byte */
        DS1481Comm.Command = DO_BYTE;             
        /* Byte to send to 1-wire bus */
        DS1481Comm.Xfer = ByteVal;              
        /* Send info to DOW driver */
        write(FileDes, &DS1481Comm.Command, 2);
        /* Get result of write */
        read(FileDes, &DS1481Comm.Xfer, 1);

        return DS1481Comm.Xfer;  
#endif /* not WIN32 */

      default:
        return FALSE;
    }
  } 

  return ByteVal;
}

/* Send a stream of bytes to the port. */
unsigned char DOWBlock(unsigned char *barr, int length)
{
  int i;
  
  if (AuthFlag)
  {
    switch(Type)
    {
      case SERIAL_DS2480:
        for (i = 0;i < length;i += SERIAL_DS2480_BLKSIZE)
        {
          if ((length - i) < SERIAL_DS2480_BLKSIZE)
            DS2480Block(FileDes,(barr + i),length - i);
          else
            DS2480Block(FileDes,(barr + i),SERIAL_DS2480_BLKSIZE);
        }
        return TRUE;
        
#ifndef WIN32
      case PARALLEL_DS1481:
        for (i = 0;i < length;i++)
          barr[i] = DOWByte(barr[i]);
        return TRUE;
#endif /* not WIN32*/

      default:
        return FALSE;
    }
  }  
  
  /* return(barr[0]); ????????????? */
  return TRUE;
}

unsigned char ToggleOverdrive()
{
  if (AuthFlag)
  {
    switch(Type)
    {
      case SERIAL_DS2480:
        return(DS2480ToggleOverdrive());
#ifndef WIN32
      case PARALLEL_DS1481:
        /* Assign command byte */
        DS1481Comm.Command = TOGGLE_OVERDRIVE;                 
        DS1481Comm.Xfer    = 0;

        /* Send info to DOW driver */
        write(FileDes, &DS1481Comm.Command, 2);
        /* Get result */
        read(FileDes, &DS1481Comm.Xfer, 1);         

        /* Return result of reset operation */
        return DS1481Comm.Xfer;           
#endif /* not WIN32 */

      default:
        return FALSE;
    }
  }

  return FALSE;     
}

int Send4()
{
  if (AuthFlag)
  {
    switch(Type)
    {
      case SERIAL_DS2480:
        return TRUE;

#ifndef WIN32
      case PARALLEL_DS1481:
        /* Assign command byte */
        DS1481Comm.Command = TOGGLE_PASSTHRU;                 
        DS1481Comm.Xfer    = 0;

        /* Send info to DOW driver */
        write(FileDes, &DS1481Comm.Command, 2);
        /* Get result */
        read(FileDes, &DS1481Comm.Xfer, 1);         

        /* Return result of reset operation */
        return DS1481Comm.Xfer;           
#endif /* not WIN32 */

      default:
        return FALSE;
    }
  }

  return FALSE;     
}

int DS1481Present()
{
  if (AuthFlag)
  {
    switch(Type)
    {
      case SERIAL_DS2480:
        return(DS2480Present(FileDes));

#ifndef WIN32
      case PARALLEL_DS1481:
        /* Assign command byte */
        DS1481Comm.Command = CHECK_BRICK;                 
        DS1481Comm.Xfer    = 0;

        /* Send info to DOW driver */
        write(FileDes, &DS1481Comm.Command, 2);      
        /* Get result */
        read(FileDes, &DS1481Comm.Xfer, 1);

        /* Return result of reset operation */
        return DS1481Comm.Xfer;           
#endif /* not WIN32 */

      default:
        return FALSE;
    }
  }

  return FALSE;     
}
