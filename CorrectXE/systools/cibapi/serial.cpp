/*--------------------------------------------------------------------------
 * Copyright (C) 1992,1993,1994,1995,1996 Dallas Semiconductor Corporation. 
 * All rights Reserved. Printed in U.S.A.
 * This software is protected by copyright laws of
 * the United States and of foreign countries.
 * This material may also be protected by patent laws of the United States 
 * and of foreign countries.
 * This software is furnished under a license agreement and/or a
 * nondisclosure agreement and may only be used or copied in accordance
 * with the terms of those agreements.
 * The mere transfer of this software does not imply any licenses
 * of trade secrets, proprietary technology, copyrights, patents,
 * trademarks, maskwork rights, or any other form of intellectual
 * property whatsoever. Dallas Semiconductor retains all ownership rights.
 *--------------------------------------------------------------------------
 *
 *  SERIAL.C
 *  Description here.           
 *
 *
 *  Author: 
 *  Project: DS2480
 *  Created: 8/9/96
 *  Distribution: In House
 *  Compiler: GCC 2.6.3 (tested under Linux)
 *  Externals: serial.h common.h
 *  Version: 0.99.5
 *
 *  Version History:

   May 13, 1998, Kevin Manbeck.  The original code did not disable the 
	SGI serial communication optimization.  This code makes a 
	call to ioctl() with parameter SIOC_ITIMER in ser_dtron().
	functions are called too close together, the button does not work.
	Add an MTImillisleep() in DS2480Reset to contain problem.


 *  0.99.5 - Added ser_sethandleparms() to allow setting of serial
 *           parameters with an already open file handle.  6/26/97
 *  0.99.4 - Took out 2 stop bits for 19200 12/13/96
 *  0.99.3 - Modified to use 2 stop bits to pace 19200 characters.
 *           12/11/96
 *  0.99.2 - Modified to use with DS2480.  9/25/96
 *  0.99.1 - Fixed ECHO problems back to the net box.  8/15/96
 *  0.99.0 - Initial Release
 */

/*
serial.c
*/
#include "machine.h"

#ifndef CRTSCTS
#define CRTSCTS CNEW_RTSCTS
#endif

#define PPP_IDLE	0x7E
#define PPP_ESCAPE	0x7D

#include <stdio.h>
#include <errno.h>
#include <string.h>
#ifndef WIN32
#include <unistd.h>
#include <termios.h>
#endif /* not WIN32 */
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#ifndef WIN32
#include <sys/time.h>
#include <sys/times.h>
#include <sys/ioctl.h>
#if !defined(__linux)
#include <sys/z8530.h>		/* KM, May 13, 1998 */
#endif /* !defined(__linux) */
#endif /* not WIN32 */
#include "MTIsleep.h"

#include "serial.h"
#include "common.h"

#ifdef WIN32
#include "cib_wserial.h"
#else /* not WIN32 */
struct termios origterm;
#endif /* WIN32 */

int ser_gethandle(char *filename)
{
  int            serhandle;

#ifdef WIN32

  CIB_WSerial* wserial = new CIB_WSerial();
  if (wserial == NULL || !wserial->Open(filename))
  {
      return -1;
  }
  serhandle = (int) wserial;

#else /* not WIN32 */

  struct termios myterm;
  
  if ((serhandle = open(filename,O_RDWR|O_NOCTTY)) < 0)
  {
    return(-1);
  }
  
  /* Get terminal parameters. */
  tcgetattr(serhandle,&myterm);
  /* Save original settings. */
  origterm = myterm;

  /* Set to non-canonical mode, and no RTS/CTS handshaking */
  myterm.c_iflag &= ~(BRKINT|ICRNL|IGNCR|INLCR|INPCK|ISTRIP|IXON|IXOFF|PARMRK);
  myterm.c_iflag |= IGNBRK|IGNPAR;
  myterm.c_oflag &= ~(OPOST);
  myterm.c_cflag &= ~(CRTSCTS|CSIZE|HUPCL|PARENB);
  myterm.c_cflag |= (CLOCAL|CS8|CREAD);
  myterm.c_lflag &= ~(ECHO|ECHOE|ECHOK|ECHONL|ICANON|IEXTEN|ISIG);
  myterm.c_cc[VMIN] = 0;
  myterm.c_cc[VTIME] = 3;
  tcsetattr(serhandle,TCSANOW,&myterm);
  tcflush(serhandle,TCIOFLUSH);

#endif /* WIN32 */

#ifdef EMULATOR
  /* Pull DTR low so we don't put the 5000FP in monitor load mode. */
  ser_dtroff(serhandle);
#else
  /* Pull DTR high so we don't drop power to the DS2480. */
  ser_dtron(serhandle);
#endif

  MTImillisleep(10);
  
  return(serhandle);
}

int ser_sethandleparms(int serhandle)
{
#ifndef WIN32

  struct termios myterm;
  
  /* Get terminal parameters. */
  tcgetattr(serhandle,&myterm);
  /* Save original settings. */
  origterm = myterm;

  /* Set to non-canonical mode, and no RTS/CTS handshaking */
  myterm.c_iflag &= ~(BRKINT|ICRNL|IGNCR|INLCR|INPCK|ISTRIP|IXON|IXOFF|PARMRK);
  myterm.c_iflag |= IGNBRK|IGNPAR;
  myterm.c_oflag &= ~(OPOST);
  myterm.c_cflag &= ~(CRTSCTS|CSIZE|HUPCL|PARENB);
  myterm.c_cflag |= (CLOCAL|CS8|CREAD);
  myterm.c_lflag &= ~(ECHO|ECHOE|ECHOK|ECHONL|ICANON|IEXTEN|ISIG);
  myterm.c_cc[VMIN] = 0;
  myterm.c_cc[VTIME] = 3;
  tcsetattr(serhandle,TCSANOW,&myterm);
  tcflush(serhandle,TCIOFLUSH);
#endif /* not WIN32 */

#ifdef EMULATOR
  /* Pull DTR low so we don't put the 5000FP in monitor load mode. */
  ser_dtroff(serhandle);
#else
  /* Pull DTR high so we don't drop power to the DS2480. */
  ser_dtron(serhandle);
#endif
  MTImillisleep(10);
  
  return(serhandle);
}

int ser_close(int serhandle)
{
#ifdef WIN32

  CIB_WSerial* wserial = (CIB_WSerial*) serhandle;
  delete wserial;

#else /* not WIN32 */

  tcsetattr(serhandle,TCSANOW,&origterm);
  close(serhandle);
  
#endif /* WIN32 */
  
  return(1);
}

/* Sets the DTR line high (+10V) which corresponds to
   0V on the other side of the DS232A on the emulator.
*/
int ser_dtron(int serhandle)
{
#ifdef WIN32

  CIB_WSerial* wserial = (CIB_WSerial*) serhandle;
  wserial->SetDTRControl(DTR_CONTROL_ENABLE);
  wserial->SetRTSControl(RTS_CONTROL_ENABLE);

#else /* not WIN32 */

  u_int modemlines;
  
  ioctl(serhandle,TIOCMGET,&modemlines);
  modemlines |= TIOCM_DTR;
  modemlines |= TIOCM_RTS;
  ioctl(serhandle,TIOCMSET,&modemlines);

#if defined(__sgi)
  ioctl (serhandle, SIOC_ITIMER, 0);		/* KM, May 13, 1998 */
#endif
#endif /* WIN32 */
  return(0);
}

/* Sets the DTR line low (-10V) which corresponds to
   5V on the other side of the DS232A on the emulator.
*/
int ser_dtroff(int serhandle)
{
#ifdef WIN32

  CIB_WSerial* wserial = (CIB_WSerial*) serhandle;
  wserial->SetDTRControl(DTR_CONTROL_DISABLE);
  wserial->SetRTSControl(RTS_CONTROL_DISABLE);

#else /* not WIN32 */

  u_int modemlines;
  
  ioctl(serhandle,TIOCMGET,&modemlines);
  modemlines &= ~(TIOCM_DTR);
  modemlines &= ~(TIOCM_RTS);
  ioctl(serhandle,TIOCMSET,&modemlines);
  
#endif /* WIN32 */

  return(0);
}

/*
Return current speed of serial port or -1 if speed not supported.
*/
long ser_getspeed(int serhandle)
{
  long           returnspeed = -1;

#ifdef WIN32

  CIB_WSerial* wserial = (CIB_WSerial*) serhandle;
  returnspeed = wserial->GetBaud();

#else /* not WIN32 */

  struct termios myterm;
  speed_t        speed;

  tcgetattr(serhandle,&myterm);
  speed = cfgetospeed(&myterm);
  switch (speed)
  {
       case B600: returnspeed = 600; break;
      case B1200: returnspeed = 1200; break;
      case B2400: returnspeed = 2400; break;
      case B4800: returnspeed = 4800; break;
      case B9600: returnspeed = 9600; break;
     case B19200: returnspeed = 19200; break;
     case B38400: returnspeed = 38400; break;
#ifdef B57600
     case B57600: returnspeed = 57600; break;
#endif
#ifdef B115200
    case B115200: returnspeed = 115200; break;
#endif
#ifdef B230400
    case B230400: returnspeed = 230400; break;
#endif
         default: return(-1);
  }
#endif /* WIN32 */

  return(returnspeed);
}

/*
Set speed of serial port.
*/
int ser_setspeed(int serhandle,u_long speed)
{
  int            result = 0;

#ifdef WIN32

  CIB_WSerial* wserial = (CIB_WSerial*) serhandle;
  wserial->SetBaud(speed);

#else /* not WIN32 */

  struct termios myterm;
  speed_t        newspeed;

  tcgetattr(serhandle,&myterm);
  switch (speed)
  {
       case 600: newspeed = B600; break;
      case 1200: newspeed = B1200; break;
      case 2400: newspeed = B2400; break;
      case 4800: newspeed = B4800; break;
      case 9600: newspeed = B9600; break;
     case 19200: newspeed = B19200; break;
     case 38400: newspeed = B38400; break;
#ifdef B57600
     case 57600: newspeed = B57600; break;
#endif
#ifdef B115200
    case 115200: newspeed = B115200; break;
#endif
#ifdef B230400
    case 230400: newspeed = B230400; break;
#endif
        default: return(-1);
  }
  tcflush(serhandle,TCIOFLUSH);

  /* Should I Set an extra stop bit?? */
  myterm.c_cflag |= CSTOPB;

  /* I think I only have to set the output speed to
     set both input and output speed, but I'm not sure,
     so I'll do both.
  */
  cfsetispeed(&myterm,newspeed);
  cfsetospeed(&myterm,newspeed);  

  /* Changing the baud rate seems to cause the DTR line
     to toggle.  I think this is to allow the DCE device
     on the other end of the line to resync at a new baud
     rate.  Hmm...A question to ponder.
  */
  result = tcsetattr(serhandle,TCSAFLUSH,&myterm);

#endif /* WIN32 */

  /* Make sure dtr stays where we want it.  Low for the
     emulator so it will run, and high for the DS2480 brick
     so it can get power.
  */
#ifdef EMULATOR
  ser_dtroff(serhandle);
#else
  ser_dtron(serhandle);
#endif
  
  if (result == -1)
  {
#ifdef DEBUG
    printf("ser_setspeed(): Could not tcsetattr()\n");
#endif
    return(-1);
  }
  
  return(0);
}

/*
Reads in a sequence of bytes from the serial to
bytearr of size length.

Returns number of bytes read or
       -1 for timeout error. 
*/
int ser_read(int serhandle,unsigned char *bytearr,int length,u_long timeout)
{
  int result;

  /* Read data from the serial port. */
  result = ser_read_data(serhandle,(char*)bytearr,length,timeout);
  
  if (result != -1)
  {
    /* Return number of bytes read */
    return(result);
  }
  else
  {
    /* We timed out. */
    return(-1);
  }
}

/*
Given a handle, a buffer of a certain maximum size, an address
location and a timeout.  Put the next incoming packet in the
buffer.

Returns number of bytes read, or -1 for timeout error.
*/
int ser_read_data(int serhandle,char *buf,int size,u_long timeout)
{
  int btotal = 0;

#ifdef WIN32

  /* Convert micro timeout to milli (-1 for no timeout) */
  int millitimeout = (int) ((timeout > 0)? (timeout + 999)/1000 : -1);

  CIB_WSerial* wserial = (CIB_WSerial*) serhandle;
  if (wserial == NULL)
  {
      return -1;
  }
  btotal = wserial->ReadBuffer((unsigned char*)buf, size, millitimeout);
  if (btotal <= 0)
  {
      return -1;
  }

#else /* not WIN32 */

  int bread = 0;
  struct timeval curtime,endtime;

  /* Init the wait timer */
  gettimeofday(&curtime,NULL);
  endtime.tv_usec = (curtime.tv_usec + timeout) % 1000000;
  endtime.tv_sec = curtime.tv_sec + (curtime.tv_usec + timeout) / 1000000;

  /* Loop until we fill the buffer, time out, or get a single packet */
  while ((btotal < size) && (!timepassed(curtime,endtime)))
  {
    if ((bread = read(serhandle,buf,size-btotal)) > 0)
    {
      btotal += bread;
      buf += bread;
    }
    if (bread < 0)
    {
      if (errno != EWOULDBLOCK)
      {
#ifdef DEBUG
        printf("ser_read_data(): errno: %d\n",errno);
#endif
        return(-1);
      }
    }
    gettimeofday(&curtime,NULL);
  }

  if (timepassed(curtime,endtime) && (btotal <= 0))
    return(-1);

#endif /* WIN32 */

  return(btotal);
}

/*
Send data to a specific host.

Returns 0 if no error.
        -1 for timeout error. 
*/
int ser_write(int serhandle,unsigned char *bytearr,int length,u_long)
{
  int result;
  
#ifdef DEBUG
  printf("%02X, ",bytearr[0]);
#endif
  /* Send the data. */
  result = ser_write_data(serhandle,(char*)bytearr,length);
  
  /* Return number of bytes written */
  return(result);
}

/*
Returns number of bytes written or -1 for timeout.
*/
int ser_write_data(int serhandle,char *buf,int buflength)
{
  int btotal = 0;

#ifdef WIN32

  CIB_WSerial* wserial = (CIB_WSerial*) serhandle;
  if (wserial == NULL)
  {
      return -1;
  }
  btotal = wserial->SendBuffer(buf, buflength);
  if (btotal <= 0)
  {
      return -1;
  }

#else /* not WIN32 */

  int bwritten = 0;
  
  while ((btotal < buflength))
  {
    if ((bwritten = write(serhandle,buf,buflength-btotal)) > 0)
    {
      btotal += bwritten;
      buf += bwritten;
    }
    if (bwritten < 0)
    {
#ifdef DEBUG
      printf("ser_write_data(): error: %d\n",errno);
#endif
      return(-1);
    }
  }

#endif /* WIN32 */

  return(btotal);
}

