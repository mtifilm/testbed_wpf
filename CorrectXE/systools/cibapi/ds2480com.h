/*
ds2480com.h
*/

int    DS2480Access(int,unsigned char *);
unsigned char    DS2480Stream(int,unsigned char *,unsigned char *,int);
unsigned char    DS2480StreamRS(int,unsigned char *,unsigned char *,int);
int    DS2480Reset(int);
int    DS2480Bit(int,unsigned char);
int    DS2480Search(int,unsigned char *);
unsigned char    DS2480Byte(int,unsigned char);
int    DS2480Block(int,unsigned char *,int);
int    ToggleCommandMode(int);
int    ToggleDataMode(int);
unsigned char    DS2480ToggleOverdrive(void);
unsigned char DS2480OverdriveOn(void);
void   DS2480OverdriveOff(void);
int    DS2480BitPrime(void);
int    DS2480StrongPUOn(int);
int    DS2480StrongPUOff(int);
int    DS2480Present(int);
int    DS2480SetSpeed(int,long);
int    DS2480SetParm(int,unsigned char,unsigned char);
int    DS2480GetParm(int,unsigned char);

/* This value (256 bytes) has been tested to work on:
   SunOS 4.1.4, Linux 2.0.x
   SunOS will NOT work with 1024 byte send/receive buffers.
*/
#define SERIAL_DS2480_BLKSIZE	256

