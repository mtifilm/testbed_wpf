/*
	File:   math.c
	By:	Kevin Manbeck
	Date:	March 8, 2001

	This file contains many math functions
*/
#include <math.h>
#include "mt_math.h"
#include "MTImalloc.h"

#include "cpmp_err.h"

int InvertMatrix (float **fpSrc, float **fpDst, int iDim)
{
  double **dpTmp, dFactor, dEpsilon=0.0000001;
  int iRow, iCol, iRR, iFoundOne, iError, iCnt;

  dpTmp = (double **) malloc (iDim * sizeof(double *));
  if (dpTmp == NULL)
    return RET_FAILURE;

  for (iRow = 0; iRow < iDim; iRow++)
   {
    dpTmp[iRow] = (double *) malloc (2*iDim * sizeof(double));
    if (dpTmp[iRow] == NULL)
      return RET_FAILURE;
   }

/*
	Set up the temporary array
*/

  for (iRow = 0; iRow < iDim; iRow++)
   {
    for (iCol = 0; iCol < iDim; iCol++)
     {
      dpTmp[iRow][iCol] = (double)fpSrc[iRow][iCol];
      if (iRow == iCol)
        dpTmp[iRow][iCol+iDim] = 1.;
      else
        dpTmp[iRow][iCol+iDim] = 0.;
     }
   }

/*
	Make the left half of dpTmp upper diagonal with 1.'s along diagonal
*/

  iError = FALSE;
  for (iRow = 0; iRow < iDim && iError == FALSE; iRow++)
   {
    if (fabs(dpTmp[iRow][iRow]) < dEpsilon)
     {
/*
	The diagonal element is zero.  Add in a non-zero row.
*/
      iFoundOne = FALSE;
      for (iCnt = iRow + 1; iCnt < iDim && iFoundOne == FALSE; iCnt++)
       {
        if (fabs(dpTmp[iCnt][iRow]) > dEpsilon)
         {
          iFoundOne = TRUE;
          for (iCol = iRow; iCol < 2*iDim; iCol++)
           {
            dpTmp[iRow][iCol] += dpTmp[iCnt][iCol];
           }
         }
       }

      if (iFoundOne == FALSE)
       {
        iError = TRUE;
       }
     }

    if (iError == FALSE)
     {
/*
	Set the diagonal element to 1.
*/

      dFactor = 1. / dpTmp[iRow][iRow];
      for (iCol = iRow; iCol < 2*iDim; iCol++)
       {
        dpTmp[iRow][iCol] *= dFactor;
       }

/*
	Subtract this row from lower rows
*/

      for (iRR = iRow + 1; iRR < iDim; iRR++)
       {
        dFactor = dpTmp[iRR][iRow];
        for (iCol = iRow; iCol < 2*iDim; iCol++)
         {
          dpTmp[iRR][iCol] -= dFactor * dpTmp[iRow][iCol];
         }
       }
     }
   }

/*
	Get rid on entries in the upper right portion.  Keep 1.'s along
	diagonal.
*/

  for (iRow = iDim-1; iRow > 0 && iError == FALSE; iRow--)
   {
/*
	Subtract this row from upper rows
*/

    for (iRR = 0; iRR < iRow; iRR++)
     {
      dFactor = dpTmp[iRR][iRow];
      for (iCol = iRow; iCol < 2*iDim; iCol++)
       {
        dpTmp[iRR][iCol] -= dFactor * dpTmp[iRow][iCol];
       }
     }
   }

/*
	Copy over the inverse values.
*/

  if (iError == FALSE)
   {
    for (iRow = 0; iRow < iDim; iRow++)
    for (iCol = 0; iCol < iDim; iCol++)
     {
      fpDst[iRow][iCol] = (float)dpTmp[iRow][iCol+iDim];
     }
   }

  for (iRow = 0; iRow < iDim; iRow++)
   {
    free (dpTmp[iRow]);
   }

  free (dpTmp);

  if (iError)
    return RET_FAILURE;
  else
    return RET_SUCCESS;
}  /* InvertMatrix */


////////////////////////////////////////////////////////////////////////////////


static float IntegerDegreeToSineTable[] =
{
	/* 0 */
	0.00000F, 0.01745F, 0.03490F, 0.05234F, 0.06976F, 0.08716F, 0.10453F, 0.12187F, 0.13917F, 0.15643F, 0.17365F, 0.19081F, 0.20791F, 0.22495F, 0.24192F,

	/* 15 */
	0.25882F, 0.27564F, 0.29237F, 0.30902F, 0.32557F, 0.34202F, 0.35837F, 0.37461F, 0.39073F, 0.40674F, 0.42262F, 0.43837F, 0.45399F, 0.46947F, 0.48481F,

	/* 30 */
	0.50000F, 0.51504F, 0.52992F, 0.54464F, 0.55919F, 0.57358F, 0.58779F, 0.60182F, 0.61566F, 0.62932F, 0.64279F, 0.65606F, 0.66913F, 0.68200F, 0.69466F,

	/* 45 */
	0.70711F, 0.71934F, 0.73135F, 0.74314F, 0.75471F, 0.76604F, 0.77715F, 0.78801F, 0.79864F, 0.80902F, 0.81915F, 0.82904F, 0.83867F, 0.84805F, 0.85717F,

	/* 60 */
	0.86603F, 0.87462F, 0.88295F, 0.89101F, 0.89879F, 0.90631F, 0.91355F, 0.92050F, 0.92718F, 0.93358F, 0.93969F, 0.94552F, 0.95106F, 0.95630F, 0.96126F,

	/* 75 */
	0.96593F, 0.97030F, 0.97437F, 0.97815F, 0.98163F, 0.98481F, 0.98769F, 0.99027F, 0.99255F, 0.99452F, 0.99619F, 0.99756F, 0.99863F, 0.99939F, 0.99985F,

	/* 90 */
	1.00000F
};

// NOTE: Input is clamped to range: 0 <= input <= 90
float Degrees2Sine(float degrees)
{
	if (degrees <= 0.F)
	{
		return 0.F;
	}

	if (degrees >= 90.F)
	{
		return 1.F;
	}

	// Linearly interpolate between table values,
	int degreeFloor = (int) floorf(degrees);

	float low = IntegerDegreeToSineTable[degreeFloor];
	float high = IntegerDegreeToSineTable[degreeFloor + 1];

	float retVal = low + ((high - low) * (degrees - degreeFloor));

	return retVal;
}

// NOTE: Input is clamped to range: 0 <= input <= 90
float Degrees2Cosine(float degrees)
{
	if (degrees <= 0.F)
	{
		return 1.F;
	}

	if (degrees >= 90.F)
	{
		return 0.F;
	}

	float retVal = Degrees2Sine(90.F - degrees);

	return retVal;
}

////////////////////////////////////////////////////////////////////////////////

