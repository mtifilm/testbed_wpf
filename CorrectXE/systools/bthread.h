#ifndef BThreadH
#define BThreadH

/*
	File:  bthread.h
	By:    Kevin Manbeck
	Date:  April 1, 2001

	The bthread code based on the self-blocking properties of the IRIX
	sproc() function.  But instead of sproc, it uses threads.

	The BThread functions run under both IRIX and WIN32.  They have
	the same behavior under both operating systems.  

	There are six inter-related functions that deal with bthread's

	1.  void * BThreadSpawn (void (*AppFcn) (void *, void *), void *)
	2.  int BThreadBegin (void *)
	3.  int BThreadEnd (void *)
        4.  int BThreadKill (void *)
        5.  int BThreadSuspend (void *)
	6.  int BThreadResume (void *)

	BThreadSpawn() creates a new thread that is a clone of the calling
	thread.  The memory between the threads is shared.  Care must
	be exercised to make code thread safe.

	The first argument to BThreadSpawn() is a function pointer.  The
	function prototype must be

		void AppFcn (void *vpAppData, void *vpReserved);

	The second argument to BThreadSpawn() is the pointer to the application
	data that will be passed to AppFcn().

	When a child thread is created by BThreadSpawn(), the parent thread
	is blocked until AppFcn() makes a call to BThreadBegin().  This creates
	a safe interval during which the child thread can make local copies
	of volatile global data.  The parent will remain blocked until the
	BThreadBegin() function is called by AppFcn().  The argument to 
	BThreadBegin is vpReserved, which is the second argument to AppFcn().

	When the AppFcn() is finished, a call to BThreadEnd() will release
	the thread.  The argument to BThreadEnd is vpReserved, which is the
	second argument to AppFcn().  Note:  If desired, the AppFcn()
	may simply return when finished.  The bthread functions will do
	the clean-up.

	BThreadKill() will kill a thread from the outside.  It is recommended
	that a thread terminate normally with BThreadEnd().  However, sometimes
	is is useful to kill a thread from the outside.  The argument to 
	BThreadKill is the pointer returned by BThreadSpawn().

	If a thread kills itself, some memory will not be freed.  This
	situation should be avoided.

	Both BThreadEnd() and BThreadKill() destroy the thread.  Any return
	code must be set by the application in shared memory.

	Every thread has a suspend count.  A call to BThreadSuspend()
	will decrement the count and suspend the thread until the suspend
	count is greater than or equal to zero.  BThreadResume() will
	increment the count.  A return of zero means success.

	As is the case with all semaphores, the BThreadSuspend() call must be
	made from the thread to be suspended.  It is not possible to suspend
	a thread from another thread.  The call to BThreadResume() may be made
	from any thread.

	----------

	There are four additional functions that are independent of the BThread
	functions.  However, they can be very useful in conjunction with
	BThread, so they are defined here:

	1.  void * CriticalSectionAlloc()
	2.  int CriticalSectionEnter(void *vp)
	3.  int CriticalSectionExit(void *vp)
	4.  int CriticalSectionFree(void *vp)

	CriticalSecionAlloc() returns a handle to the critical section
	structure.  It returns NULL on failure.

	CriticalSectionEnter(), CriticalSectionExit(), and 
	CriticalSectionFree() all take a critical section handle as an
	arguement.  Return 0 means success.

	When code enters a critical section, other code cannot enter it
	until CriticalSectionExit() is called.

	-----------

	There are four semaphore functions that are independent, but can
	be very useful.

	1.  void * SemAlloc ();
	2.  int SemSuspend (void *vp);
	3.  int SemResume (void *vp);
	4.  int SemFree (void *vp);

	SemAlloc() returns a pointer to a semaphore.  Returns NULL on failure.

	SemSuspend() and SemResume() are analogous to BThreadSuspend()
	and BThreadResume(), but without the need to create a BThread.
	Return is 0 on success.

	SemFree() destroys the semaphore.  Return is 0 on success.

*/

#include "systoolsDLL.h"
#include "ThreadLocker.h"
#include <process.h>

#define BTHREAD_PRIORITY_ABOVE_NORMAL THREAD_PRIORITY_ABOVE_NORMAL
        // Indicates 1 point above normal priority for the priority class.
#define BTHREAD_PRIORITY_BELOW_NORMAL THREAD_PRIORITY_BELOW_NORMAL
        // Indicates 1 point below normal priority for the priority class.
#define BTHREAD_PRIORITY_HIGHEST THREAD_PRIORITY_HIGHEST
        //Indicates 2 points above normal priority for the priority class.
#define BTHREAD_PRIORITY_IDLE THREAD_PRIORITY_IDLE
        // Indicates a base priority level of 1 for IDLE_PRIORITY_CLASS,
        // NORMAL_PRIORITY_CLASS, or HIGH_PRIORITY_CLASS processes, and a
        // base priority level of 16 for REALTIME_PRIORITY_CLASS processes.
#define BTHREAD_PRIORITY_LOWEST THREAD_PRIORITY_LOWEST
        // Indicates 2 points below normal priority for the priority class.
#define BTHREAD_PRIORITY_NORMAL THREAD_PRIORITY_NORMAL
        // Indicates normal priority for the priority class.
#define BTHREAD_PRIORITY_TIME_CRITICAL THREAD_PRIORITY_TIME_CRITICAL
        // Indicates a base priority level of 15 for IDLE_PRIORITY_CLASS,
        // NORMAL_PRIORITY_CLASS, or HIGH_PRIORITY_CLASS processes, and a
		  // base priority level of 31 for REALTIME_PRIORITY_CLASS processes.

#define BThreadSpawn(function, data) BThreadSpawnEx(function, data, __FILE__, __LINE__)

MTI_SYSTOOLSDLL_API void * BThreadSpawnEx (
										void (*AppFcn) (void *vpAppData, void *vpReserved),
										void *vpAppData,
										const char* callerFilePath,
										int callerLineNumber);
MTI_SYSTOOLSDLL_API int BThreadBegin (void *vpReserved);
////MTI_SYSTOOLSDLL_API int BThreadEnd (void *vpReserved); - REMOVED - just return from the thread proc!!
MTI_SYSTOOLSDLL_API int BThreadKill (void *vpReserved);
MTI_SYSTOOLSDLL_API int BThreadSuspend (void *vpReserved);
MTI_SYSTOOLSDLL_API int BThreadResume (void *vpReserved);
MTI_SYSTOOLSDLL_API int BThreadSetPriority (void *vpReserved, int iPriority);
MTI_SYSTOOLSDLL_API bool BThreadIsRunning(void *vp);

MTI_SYSTOOLSDLL_API void * CriticalSectionAlloc();
MTI_SYSTOOLSDLL_API int CriticalSectionEnter(void *vp);
MTI_SYSTOOLSDLL_API int CriticalSectionExit(void *vp);
MTI_SYSTOOLSDLL_API int CriticalSectionFree(void *vp);

MTI_SYSTOOLSDLL_API void * SemAlloc ();
MTI_SYSTOOLSDLL_API int SemSuspend (void *vp);
// following returns true if semaphore signalled or false if timed out
MTI_SYSTOOLSDLL_API bool SemSuspendWithTimeout (void *vp, int timeoutMsecs);
MTI_SYSTOOLSDLL_API int SemResume (void *vp);
MTI_SYSTOOLSDLL_API int SemFree (void *vp);

//////////////////////////////////////////////////////////////////////
// This class waits for a resource to become free when count
// threads are working on it
class MTI_SYSTOOLSDLL_API CCountingWait
  {
    public:
      CCountingWait(void);
      CCountingWait(int count);
      ~CCountingWait(void);

      void SetCount(int count);
      void ReleaseOne(void);
      bool Poll(void);
      bool Wait(const vector<void *> &threadStruct, const char *function, int line);

    private:
      void Create(int n);

      HANDLE _event;
      int    _count;
      CThreadLock Locker;
   };

#endif
