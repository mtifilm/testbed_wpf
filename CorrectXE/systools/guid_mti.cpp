#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "md5_mti.h"
#include "guid_mti.h"

#include "machine.h"

#ifndef _WINDOWS
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/sysinfo.h>
#include <sys/time.h>
#endif

// set the following to the number of 100ns ticks of the actual
//resolution of your system's clock
//
#define GUIDS_PER_TICK 1

// Set the following to a call to acquire a system wide global lock
// Not used for now
#define LOCK
#define UNLOCK

// Some internal
typedef MTI_UINT64 guid_time_t;
typedef struct {
 char nodeID[6];
} guid_node_t;


void get_ieee_node_identifier(guid_node_t *node);
void get_system_time(guid_time_t *uuid_time);
void get_random_info(char seed[16]);

/* various forward declarations */
static void format_guid_v1(guid_t * guid, MTI_UINT16 clockseq,
guid_time_t timestamp, guid_node_t node);
static void format_guid_v3(guid_t * guid, unsigned char hash[16]);
static void get_current_time(guid_time_t * timestamp);
static MTI_UINT16 true_random(void);

static guid_node_t IEEENode;
static bool NodeValid = false;

/* guid_create -- generator a guid */
//------------------guid_create-------------------John Mertus----Jul 2003-----

  guid_t guid_create(void)

//  Generate a GUID.  
//
//****************************************************************************
{
    guid_t guid;
    guid_time_t timestamp;
    MTI_UINT16 clockseq;
    guid_node_t node;

    /* acquire system wide lock so we're alone */
    LOCK;

    /* get current time */
    get_current_time(&timestamp);

    /* get node ID */
    if (!NodeValid)
      {
         get_ieee_node_identifier(&IEEENode);
         NodeValid = true;
      }

    node = IEEENode;

    // clockseq should be from nv storage but this is good enough
    clockseq = true_random();

    /* stuff fields into the guid */
    format_guid_v1(&guid, clockseq, timestamp, node);

    UNLOCK;
    return guid;
};

//-----------------format_guid_v1----------------John Mertus----Jul 2003-----

  void format_guid_v1(guid_t * guid, MTI_UINT16 clock_seq, guid_time_t
                      timestamp, guid_node_t node) 

// Make a guid from the timestamp, clockseq, and node ID 
//
//****************************************************************************
{
   /* Construct a version 1 guid with the information we've gathered
    * plus a few constants. */
   guid->time_low = (unsigned long)(timestamp & 0xFFFFFFFF);
   guid->time_mid = (unsigned short)((timestamp >> 32) & 0xFFFF);
   guid->time_hi_and_version = (unsigned short)((timestamp >> 48) &
      0x0FFF);
   guid->time_hi_and_version |= (1 << 12);
   guid->clock_seq_low = clock_seq & 0xFF;
   guid->clock_seq_hi_and_reserved = (clock_seq & 0x3F00) >> 8;
   guid->clock_seq_hi_and_reserved |= 0x80;
   memcpy(&guid->node, &node, sizeof guid->node);
}

// data type for guid generator persistent state
typedef struct {
 guid_time_t ts;       /* saved timestamp */
 guid_node_t node;     /* saved node ID */
 MTI_UINT16 cs;        /* saved clock sequence */
 } guid_state;

//----------------------get_current_time-----------John Mertus----Jul 2003-----

  void get_current_time(guid_time_t * timestamp)

// get-current_time -- get time as 60 bit 100ns ticks since whenever.
// Compensate for the fact that real clock resolution may be
// less than 100ns.
//
//****************************************************************************
{
   guid_time_t         time_now;
   static guid_time_t  time_last;
   static MTI_UINT16   guids_this_tick;
   static int          inited = 0;

   if (!inited) {
     get_system_time(&time_now);
     guids_this_tick = GUIDS_PER_TICK;
     inited = 1;
   }

   while (1) {
       get_system_time(&time_now);

     /* if clock reading changed since last guid generated... */
       if (time_last != time_now) {
          /* reset count of guids gen'd with this clock reading */
           guids_this_tick = 0;
          break;
     };
       if (guids_this_tick < GUIDS_PER_TICK) {
          guids_this_tick++;
          break;
     };
     /* going too fast for our clock; spin */
   }

 /* add the count of guids to low order bits of the clock reading */
 *timestamp = time_now + guids_this_tick;
}

// true_random -- generate a crypto-quality random number.
//  This sample doesn't do that.
static MTI_UINT16 true_random(void)
{
 static int inited = 0;
 MTI_INT64 time_now;

 if (!inited)
   {
     char seed[16];
     get_random_info(seed);
     memcpy(&time_now,seed, sizeof(time_now));
     srand((unsigned int)(((time_now >> 32) ^ time_now)&0xffffffff));
     inited = 1;
   }

   return (rand());
}

//-----------------guid_create_from_name----------John Mertus----Jul 2003-----

  guid_t guid_create_from_name(const guid_t &nsid, const string name)

//  Create a guid using a "name" from a "name space
//  guid to serve as context, so identical names from different name 
//  spaces generate different guids
//
//****************************************************************************
{
 guid_t guid;
 md5_state_t c;
 md5_byte_t digest[16];

 guid_t net_nsid;      /* context guid in network byte order */

 /* put name space ID in network byte order so it hashes the same
     no matter what endian machine we're on */
 net_nsid = nsid;
 net_nsid.time_low = htonl(net_nsid.time_low);
 net_nsid.time_mid = htons(net_nsid.time_mid);
 net_nsid.time_hi_and_version = htons(net_nsid.time_hi_and_version);

 md5_init_mti(&c);
 md5_append_mti(&c, (unsigned char *)&net_nsid, sizeof(guid_t));
 md5_append_mti(&c, (const md5_byte_t *)name.c_str(), name.length());
 md5_finish_mti( &c, digest);

 /* the hash is in network byte order at this point */
 format_guid_v3(&guid, digest);
 return guid;
};

/* format_guid_v3 -- make a guid from a (pseudo)random 128 bit number
*/
void format_guid_v3(guid_t * guid, unsigned char hash[16]) {
   /* Construct a version 3 guid with the (pseudo-)random number
    * plus a few constants. */

   memcpy(guid, hash, sizeof(guid_t));

 /* convert guid to local byte order */
 guid->time_low = ntohl(guid->time_low);
 guid->time_mid = ntohs(guid->time_mid);
 guid->time_hi_and_version = ntohs(guid->time_hi_and_version);

 /* put in the variant and version bits */
   guid->time_hi_and_version &= 0x0FFF;
   guid->time_hi_and_version |= (3 << 12);
   guid->clock_seq_hi_and_reserved &= 0x3F;
   guid->clock_seq_hi_and_reserved |= 0x80;
};

//--------guid_compare----------------------------John Mertus----Jul 2003-----

  int guid_compare(const guid_t &u1, const guid_t u2)

// guid_compare --  Compare two guid's "lexically" and return
//      -1   u1 is lexically before u2
//       0   u1 is equal to u2
//       1   u1 is lexically after u2
//   Note:   lexical ordering is not temporal ordering!
//
//****************************************************************************
{
 int i;

#define CHECK(f1, f2) if (f1 != f2) return f1 < f2 ? -1 : 1;
 CHECK(u1.time_low, u2.time_low);
 CHECK(u1.time_mid, u2.time_mid);
 CHECK(u1.time_hi_and_version, u2.time_hi_and_version);
 CHECK(u1.clock_seq_hi_and_reserved, u2.clock_seq_hi_and_reserved);
 CHECK(u1.clock_seq_low, u2.clock_seq_low)
 for (i = 0; i < 6; i++) {
     if (u1.node[i] < u2.node[i])
          return -1;
     if (u1.node[i] > u2.node[i])
     return 1;
   }
 return 0;
};

//------------ guid_to_string---------------------John Mertus----Jul 2003-----

  string guid_to_string(const guid_t &u)

//  Takes a guid and makes a 36 character string from it.
//
//****************************************************************************
{
  char Result[100];
  sprintf(Result, "%8.8x-%4.4x-%4.4x-%2.2x%2.2x-%2.2x%2.2x%2.2x%2.2x%2.2x%2.2x", 
           u.time_low, u.time_mid,
           u.time_hi_and_version, u.clock_seq_hi_and_reserved,
           u.clock_seq_low, u.node[0], u.node[1], u.node[2],
                            u.node[3], u.node[4], u.node[5]);

  string sResult = Result;
  return sResult;
}

//------------guid_from_string--------------------John Mertus----Jul 2003-----

  bool guid_from_string(guid_t &u, const string sguid)

//  Converts a 36 byte strong to a guid
//   u is the guid to create
//   squid is the string
//
//  return is true if the string parses correctly
//
//****************************************************************************
{
  if (sguid.length() != 36) return false;
  int x;

  if (sscanf(sguid.substr(0,8).c_str(),"%x",&x) != 1) return false;
  u.time_low = x;
  if (sguid[8] != '-') return false;

  if (sscanf(sguid.substr(9,4).c_str(),"%x",&x) != 1) return false;
  u.time_mid = x;
  if (sguid[13] != '-') return false;

  if (sscanf(sguid.substr(14,4).c_str(),"%x",&x) != 1) return false;
  u.time_hi_and_version = x;
  if (sguid[18] != '-') return false;

  if (sscanf(sguid.substr(19,2).c_str(),"%x",&x) != 1) return false;
  u.clock_seq_hi_and_reserved = x;
  if (sscanf(sguid.substr(21,2).c_str(),"%x",&x) != 1) return false;
  u.clock_seq_low = x;

  if (sguid[23] != '-') return false;
  // now parse the node, note this is not endian specific.
  for (int i=0; i < 6; i++)
   {
      if (sscanf(sguid.substr(24+i*2, 2).c_str(),"%x",&x) != 1) return false;
      u.node[i] = x;
   }

  return true;
}

//*************************MACHINE DEPENDENT SECTION***********************
#ifdef _WIN32
#include "IPTypes.h"
// Stolen from <ifcons.h>:
#define MIB_IF_TYPE_OTHER               1
#define MIB_IF_TYPE_ETHERNET            6
#define MIB_IF_TYPE_TOKENRING           9
#define MIB_IF_TYPE_FDDI                15
#define MIB_IF_TYPE_PPP                 23
#define MIB_IF_TYPE_LOOPBACK            24
#define MIB_IF_TYPE_SLIP                28

// Stolen from iphlpapi.h
extern "C"
DWORD WINAPI GetAdaptersInfo(
		PIP_ADAPTER_INFO pAdapterInfo, PULONG pOutBufLen
		);

#define MAX_MAC_ADDRS 6
#define MAC_ADDR_SIZE 18  // strlen("XX-XX-XX-XX-XX-XX"+1)
#else
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <errno.h>
#endif /* WIN32 */


#if defined(__sgi)

//***********************************SGI SECTION*******************************

extern "C" unsigned sysid (unsigned char id[16]);
     /* system dependent call to get IEEE node ID.
        This sample implementation generates a random node ID
        */

     void get_ieee_node_identifier(guid_node_t *node)
{
   MTI_INT32 sid = sysid(NULL);
   node->nodeID[0] = 0x08;
   node->nodeID[1] = 0;
   memcpy(node->nodeID + 2, &sid, sizeof(sid));
   return;
}

     void get_random_info(char seed[16])
{
       md5_state_t c;

       typedef struct {
           MTI_INT64 s;
           struct timeval t;
           char hostname[257];
       } randomness;
       randomness r;

       md5_init_mti(&c);

       r.s = (MTI_UINT64) sysid(NULL);
       gettimeofday(&r.t, (struct timezone *)0);
       gethostname(r.hostname, 256);
       md5_append_mti(&c, (unsigned char*)&r, sizeof(randomness));
       md5_finish_mti(&c, (md5_byte_t *)seed);
     };

//*********************************LINUX SECTION*******************************

#elif defined(__linux)

  bool get_node_from_dev(guid_node_t *node, char *dev)
{
  struct ifreq ifr;
  int sk;

  // Debug purposes
  for (int i=0; i < 14; i++) ifr.ifr_addr.sa_data[i] = -1;

  // create a temporary socket: ioctl needs one
  if ((sk = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP)) < 0)
   {
     return false;
   }

  // copy the device name into the ifreq structure
  if (strlen(dev) >= IFNAMSIZ) return false;
  strcpy(ifr.ifr_name, dev);

  // make the request
  ifr.ifr_addr.sa_family = AF_UNSPEC;

  if (!ioctl(sk, SIOCGIFHWADDR, &ifr))
   {
    // if successful, copy the result into the supplied buffer */
    memcpy(node, (struct sockaddr_in *)(&ifr.ifr_addr.sa_data),
     sizeof(guid_node_t));
   }
  else
   {
     close(sk);
     return false;
   }

  // and close the temporary socket */
  close(sk);
  return true;

}

    void get_ieee_node_identifier(guid_node_t *node)
{
   if (get_node_from_dev(node, "eth0")) return;
   if (get_node_from_dev(node, "nb0")) return;
   throw;
}

     void get_random_info(char seed[16])
{
       md5_state_t c;

       typedef struct {
           struct sysinfo s;
           struct timeval t;
           char hostname[257];
       } randomness;
       randomness r;

       md5_init_mti(&c);

       sysinfo(&r.s);
       gettimeofday(&r.t, (struct timezone *)0);
       gethostname(r.hostname, 256);
       md5_append_mti(&c, (unsigned char*)&r, sizeof(randomness));
       md5_finish_mti(&c, (md5_byte_t *)seed);
     }

//******************************WINDOWS SECTION*******************************

#elif defined(_WINDOWS)

     /* system dependent call to get IEEE node ID.
        This sample implementation generates a random node ID
        */

     void get_ieee_node_identifier(guid_node_t *node)
{
    PIP_ADAPTER_INFO pAdaptersInfo = NULL, pAdapter = NULL;
    DWORD err = 0, adaptersInfoSize = 0;
    bool Found = false;

    err = GetAdaptersInfo(NULL, &adaptersInfoSize);
    if (err != ERROR_BUFFER_OVERFLOW)
      {
        char seed[16];
        get_random_info(seed);
        memcpy(node, seed, sizeof(*node));
        return;
      }
    pAdaptersInfo = (PIP_ADAPTER_INFO) GlobalAlloc(GPTR, adaptersInfoSize);
    if (pAdaptersInfo == NULL)
      {
        get_random_info((char *)node);
        return;
      }

    err = GetAdaptersInfo(pAdaptersInfo, &adaptersInfoSize);
    if (!err) {
	for (pAdapter = pAdaptersInfo; pAdapter; pAdapter = pAdapter->Next) {
	    /* Try to make sure it's a hardware adapter */
	    switch (pAdapter->Type) {
	        case MIB_IF_TYPE_ETHERNET:
	        case MIB_IF_TYPE_FDDI:
		    break;
                default:
                    continue;
	    }
	    /* Screen out oddball lengths and AOL addresses */
	    if (pAdapter->AddressLength != 6 ||
                (pAdapter->Address[0] == 0x00 &&
                 pAdapter->Address[1] == 0x03 &&
		 pAdapter->Address[2] == 0x8A)) {
	        continue;
            }
           memcpy(node, pAdapter->Address, sizeof(guid_node_t));
           Found = true;
           break;
        }
    }
    GlobalFree(pAdaptersInfo);
    if (!Found) throw;
}

static MTI_INT64 Fudge = -1;
static MTI_INT64 Freq = -1;
static LARGE_INTEGER lOld;

     void get_system_time(guid_time_t *uuid_time)
{
   LARGE_INTEGER l;
   QueryPerformanceCounter(&l);

   //  First time through the routine
   //  get the frequence and the local time
   if (Fudge == -1)
    {
       LARGE_INTEGER f;
       ULARGE_INTEGER timeQ;
       lOld = l;
       GetSystemTimeAsFileTime((FILETIME *)&timeQ);
       // NT keeps time in FILETIME format which is 100ns ticks since
       // Jan 1, 1601.  UUIDs use time in 100ns ticks since Oct 15, 1582.
       // The difference is 17 Days in Oct + 30 (Nov) + 31 (Dec)
       // + 18 years and 5 leap days.
       timeQ.QuadPart +=
               (unsigned __int64) (1000*1000*10)       // seconds
             * (unsigned __int64) (60 * 60 * 24)       // days
             * (unsigned __int64) (17+30+31+365*18+5); // # of days
       QueryPerformanceFrequency(&f);
       Freq = f.QuadPart;
       Fudge = timeQ.QuadPart;
    }

   //  Now get highfreq time, adjust by local time
   //  Unless we are running years, there should not be overflows
   *uuid_time = ((l.QuadPart - lOld.QuadPart)*10000000)/Freq + Fudge;
}

     void get_random_info(char seed[16])
{
       md5_state_t c;

       typedef struct {
           MEMORYSTATUS m;
           SYSTEM_INFO s;
           FILETIME t;
           LARGE_INTEGER pc;
           DWORD tc;
           DWORD l;
           DWORD pid;
           char hostname[MAX_COMPUTERNAME_LENGTH + 1];
       } randomness;
       randomness r;

       md5_init_mti(&c);
       /* memory usage stats */
       GlobalMemoryStatus(&r.m);
       /* random system stats */
       GetSystemInfo(&r.s);
       /* 100ns resolution (nominally) time of day */
       GetSystemTimeAsFileTime(&r.t);
       /* high resolution performance counter */
       QueryPerformanceCounter(&r.pc);

       r.pid = GetCurrentThreadId();
       /* milliseconds since last boot */
       r.tc = GetTickCount();
       r.l = MAX_COMPUTERNAME_LENGTH + 1;

       GetComputerName(r.hostname, &r.l );
       md5_append_mti(&c, (unsigned char*)&r, sizeof(randomness));
       md5_finish_mti(&c, (unsigned char*)seed);
}

#else
#error No supported system
#endif

#ifndef _WINDOWS
     void get_system_time(guid_time_t *uuid_time)
{
         struct timeval tp;

         gettimeofday(&tp, (struct timezone *)0);

         /* Offset between UUID formatted times and Unix formatted times.
            UUID UTC base time is October 15, 1582.
            Unix base time is January 1, 1970.
         */
         *uuid_time = (tp.tv_sec * 10000000) + (tp.tv_usec * 10) +
           MTI_INT64(0x01B21DD213814000ULL);
};
#endif



