#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <signal.h>
/*#include <sys/termio.h>*/
#include <sys/types.h>
#include <string.h>
#include <strings.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <errno.h>
#include "mcs_support.h"
#include "pipe_support.h"

/* Local Globals */
int            LOC_iFDPipeToDaemon;
int            LOC_iFDPipeFromDaemon = -1;
long           LOC_lMyPID;

char *GetPipeName(char *caFileName, const long lPID)
{
  sprintf(caFileName, "/tmp/.mcsd-%ld.pipe", lPID);
  return(caFileName);
}

int OpenPipe(const char *caPipeName, int iFlags)
{
  bool bDoneF = FALSE;
  int  iFD = -1;
  int  iNumWaits = 0;

  while (!bDoneF)
  {
    iFD = open(caPipeName, iFlags);

    if (iFD == -1)
    {
      // Usually the pipe is not ready immediately for the first open
      //   so, we only print the perror if it's after the first time or
      //   in the case the error is not ENOENT (No such file or directory)
      if ((iNumWaits > 0) || (errno != ENOENT))
        perror("Pipe not ready:");

      iNumWaits++;
#if defined(__sgi)
      sginap(2);
#endif /* defined(__sgi) */
    }
    else
    {
      printf("TTT Pipe '%s' from daemon opened as FD %d\n", caPipeName, iFD);
      bDoneF = TRUE;
    }

    if (iNumWaits > 10)
      bDoneF = TRUE;
  }

  return(iFD);
}

int CreateInPipe(const char *caName)
{
  return(mkfifo (caName, (mode_t) 0666));
}

int CreateOutPipe(const char *caName)
{
  return(mkfifo (caName, (mode_t) 0644));
}

int SendToPipe(const int iFD, const char *caLine)
{
  ssize_t sstRet;
  int     iLenLine;
  char    caLineCR[MAX_LINE_LEN];

  memset(caLineCR, 0, MAX_LINE_LEN);		// zap it

  sprintf(caLineCR, "%c%c%4s\n", SENDER_MCSD, MCS_DAEMON_CMD, caLine);
  iLenLine = strlen(caLineCR);

printf("TTT sending '%4s'\n", caLine);
  sstRet = write(iFD, caLineCR, iLenLine);

  if (sstRet == -1)
  {
    perror("SendToPipe write:");
    return(-1);
  }
  if (sstRet != iLenLine)
  {
    fprintf(stderr, "Short write: wanted %d, only wrote %ld\n", iLenLine,
             (long) sstRet);
    return(-1);
  }

  return(1);
}

int SendToDaemon(const char *caLine)
{
  ssize_t sstRet; 
  int     iLenLine;
  char    caLineCR[MAX_LINE_LEN];

  sprintf(caLineCR, "%4s %6ld\n", caLine, LOC_lMyPID);
  iLenLine = strlen(caLineCR);

printf("TTT sending '%4s %6ld'\n", caLine, LOC_lMyPID);
  sstRet = write(LOC_iFDPipeToDaemon, caLineCR, iLenLine);

  if (sstRet == -1)
  {
    perror("write to Daemon:");
    return(-1);
  }
  if (sstRet != iLenLine)
  {
    fprintf(stderr, "Short write: wanted %d, only wrote %ld\n", iLenLine,
             (long) sstRet);
    return(-1);
  }

  return(1);
}

int OpenPipeToDaemon (void)
{
//printf("TTT open\n");
  LOC_iFDPipeToDaemon = open("/tmp/.mcsd-in.pipe", O_WRONLY | O_NONBLOCK);

  if (LOC_iFDPipeToDaemon < 0)
  {
    perror("Open pipe to mcsd:");
    if (errno == ENXIO)
      fprintf(stderr, "mcsd probably not running\n");
    return(-1);
  }
printf("TTT Pipe to daemon opened as FD %d\n", LOC_iFDPipeToDaemon);

  LOC_lMyPID = (long) getpid();

  return(LOC_iFDPipeToDaemon);
}

int OpenPipeFromDaemon(void)
{
  char           caFileNamePipeFromDaemon[128];

  GetPipeName(caFileNamePipeFromDaemon, LOC_lMyPID);

  LOC_iFDPipeFromDaemon = OpenPipe(caFileNamePipeFromDaemon,
                                     O_RDONLY | O_NONBLOCK);

  if (LOC_iFDPipeFromDaemon == -1)
  {
    fprintf(stderr,
             "Gave up waiting for daemon to create named pipe '%s' for us\n",
             caFileNamePipeFromDaemon);
    return(-1);
  }

  return(LOC_iFDPipeFromDaemon);
}

int ProcessData(int *ipFDPipeFromDaemon, bool *bpKeepReadingF, char *caLine)
{
  int i;
  int iRet;

//printf("TTT ProcessData\n");
  i = ReadBytes(*ipFDPipeFromDaemon, (UCHAR *) caLine, BYTES_PER_LINE);
//printf("TTT After RB\n");

  if (i > 0)
  {
//int j;
//printf("TTT %i: ", i);
//for (j = 0; j < i; j++)
//  printf("%d (%c) ", caLine[j], caLine[j]);
//printf("\n");

   if (i == BYTES_PER_LINE)
     *bpKeepReadingF = FALSE;
  }
  else if (i == 0)
  {
    fprintf(stderr, "EOF on PIPE - 'mcsd' daemon probably died\n");
    iRet = close(*ipFDPipeFromDaemon);
    *ipFDPipeFromDaemon = -1;

    if (iRet == -1)
      perror("close:");

    *bpKeepReadingF = FALSE;
  }
  else if ((i == -1) && (errno == EAGAIN))
  {
    ;	// Do nothing - just means nothing to read
  }
  else
  {
    perror("ReadBytes:");
  }

  return(i);
}
