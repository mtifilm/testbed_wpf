#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <signal.h>
/*#include <sys/termio.h>*/
#include <sys/types.h>
#include <string.h>
#include <strings.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <errno.h>
#include "mcs_support.h"
#include "pipe_support.h"

/* Defines */
#define MAX_LINE_LEN    512
#define MAX_PIPES         8
#define COMMAND_LEN       4	/* e.g. HELO, STAT */
#define FULL_COMMAND_LEN 12	/* e.g. HELO 123456 */
#define PIPE_IN          "/tmp/.mcsd-in.pipe"

/* Local Globals */
bool LOC_bMCS_AvailableF = TRUE;

/* Forward Declarations */
int ProcessPipeInData(int *ipFDPipeIn, long *lpActivePID,
                      bool *bpDaemonAvailableF, char *caActiveFileName,
                      int *ipFDActive);
int SendReceivedDataToActivePipe(const int iFDActive, const UCHAR ucType,
                                 const UCHAR ucButton,
                                 const UCHAR ucPressRelease,
                                 const SCHAR cShuttlePos, const SCHAR JogPos);

main(int iArgC, char *cpArgV[])
{
  int            iRet;
  int            iPort = 1;		/* Default ttyd1 */
  int            iOpt;
  fd_set         fdsRead;
  struct timeval tv;
  int            iFDPipeIn;
  int            iSelectCount;
  unsigned char  ucaMCS[2];
  char           caActiveFileName[128] = "";
  int            iFD_MCS;
  int            iBytesRead;
  long           lActivePID = -1;
  int            iFDActive = -1;
  struct stat    stsStat;
//  long           laPipeOutPID[MAX_PIPES];
  bool           bDaemonAvailableF = TRUE;
  UCHAR          ucType         = 0;
  UCHAR          ucButton       = 0;
  UCHAR          ucPressRelease = 0;
  SCHAR          cShuttlePos    = 0;
  SCHAR          cJogPos        = 0;
  bool           bDieIfNoMCS = TRUE;
  bool           bMCSFound   = FALSE;
  bool           bFakeData   = FALSE;	// To have fake plays sent at time of
					//   select timeout

// Parse Args
  while ((iOpt = getopt (iArgC, cpArgV, "fnp:")) != EOF)
  {
    switch (iOpt)
    {
      case 'n':			// n - "no die" - run even if MCS not found
        bDieIfNoMCS = FALSE;	//   useful for debugging daemon when MCS box
      break;			//   not available.
      case 'f':			// f - "fake plays"
        bFakeData = TRUE;
      break;
      case 'p':			// p - Port
        iPort = atoi(optarg);
      break;
    }
  }
printf("TTT trying port /dev/ttyd%d\n", iPort);

// Grab PIPE signal
  signal(SIGPIPE, SIG_IGN);	/* ignore broken pipe */

// Open pipes
  iRet = stat(PIPE_IN, &stsStat);
printf("TTT %d\n", iRet);
  if ((iRet == -1) && (errno == ENOENT))  /* No such file or directory */
    CreateInPipe(PIPE_IN);                  /*   so create */

  iFDPipeIn = open(PIPE_IN, O_RDONLY | O_NONBLOCK);
printf("TTT Open of iFDPipeIn returned %d\n", iFDPipeIn);

  if (iFDPipeIn < 0)
  {
    perror("open Inbound pipe:");
    exit(-1);
  }

  /* Set these to be unused */
  /* TTT - to be implemented */
//  for (i = 0; i < MAX_PIPES; i++)
//    laPipeOutPID[i] = -1;

// Open MCS
  if (!(iFD_MCS = InitMCS(iPort)))
  {
    fprintf(stderr, "InitMCS() failed.\n");
    bMCSFound = FALSE;

    if (bDieIfNoMCS)
      exit(-1);
/* TTT - need to set retry instead of 'exit'ing probably */
  }
  else
    bMCSFound = TRUE;

printf("TTT iFD_MCS = %d\n", iFD_MCS);

// Go
  while (1)
  {
    FD_ZERO(&fdsRead);
    FD_SET(iFDPipeIn, &fdsRead);
    iSelectCount = iFDPipeIn + 1;

    if (bMCSFound)
    {
      FD_SET(iFD_MCS, &fdsRead);
      iSelectCount = iFD_MCS + 1;

      if (iFDPipeIn > iFD_MCS)
        iSelectCount = iFDPipeIn + 1;
    }

    tv.tv_sec  = 5;
    tv.tv_usec = 0;

//printf("TTT preselect\n");
    iRet = select(iSelectCount, &fdsRead, NULL, NULL, &tv);
//printf("TTT select returned %d\n", iRet);

    if (iRet < 0) 
    {
      perror("select() failed:");
      exit(-1);
    }
    else if (iRet > 0) 
    {
printf("TTT %d descriptors to process\n", iRet);
      if (FD_ISSET(iFDPipeIn, &fdsRead))
      {
        iRet = ProcessPipeInData(&iFDPipeIn, &lActivePID,
                                 &bDaemonAvailableF, caActiveFileName,
                                 &iFDActive);
      }

      if (FD_ISSET(iFD_MCS, &fdsRead))
      {
//printf("TTT iFDPipeIn %d, lActivePID %ld, bDaemonAvailableF %d\n", iFDPipeIn, lActivePID, bDaemonAvailableF);
//printf("TTT caActiveFileName %s, iFDActive %d\n", caActiveFileName, iFDActive);
        iBytesRead = ReadBytes(iFD_MCS, ucaMCS, 2);
printf("%d: %x %x\n", iBytesRead, ucaMCS[0], ucaMCS[1]);
        if (lActivePID != -1)
        {
          iRet = ParseMCSCommand(ucaMCS, &ucType, &ucButton,
                                 &ucPressRelease, &cShuttlePos, &cJogPos);
          if (iRet)
            SendReceivedDataToActivePipe(iFDActive, ucType, ucButton,
                                         ucPressRelease, cShuttlePos,
                                         cJogPos);
          /* TTT - else, send some error b/c parsing of command failed */
        }
      }
    }
    else
    {
      printf("TTT select timeout\n");
      if ((lActivePID != -1) && bFakeData)
        SendReceivedDataToActivePipe(iFDActive, MCS_BUTTON, MCS_PLAY,
                                     MCS_RELEASE, 0, 0);
    }
  }

  printf("\nBye!\n");
}

int ProcessPipeInData(int *ipFDPipeIn, long *lpActivePID,
                      bool *bpDaemonAvailableF, char *caActiveFileName,
                      int *ipFDActive)
{
  int  iBytesRead;
  int  iRet;
  int  iFD;
  int  iFDtmp;
  long lPID;
  char caCommand[512];
  char caFileNamePipeOut[128];

  iBytesRead = ReadBytes(*ipFDPipeIn, (UCHAR *) caCommand, FULL_COMMAND_LEN);

  if (iBytesRead == FULL_COMMAND_LEN)
  {
printf("TTT '%s'\n", caCommand);
    if (!strncmp(caCommand, "HELO", COMMAND_LEN))
    {
      sscanf(caCommand + COMMAND_LEN + 1, "%ld", &lPID);
printf("TTT pid='%d'\n", lPID);
      GetPipeName(caFileNamePipeOut, lPID);

      iRet = CreateOutPipe(caFileNamePipeOut);

      if (iRet == 0)
      {
        printf("TTT pipe created '%s'\n", caFileNamePipeOut);
//        laPipeOutPID[0] = lPID;	/* TTT - handle index */

        if (*bpDaemonAvailableF)
        {
          iFD = OpenPipe(caFileNamePipeOut, O_WRONLY | O_NONBLOCK);

          if (iFD < 0)
            perror("Error opening PipeOut:");
          else
          {
            if (LOC_bMCS_AvailableF)
            {
              SendToPipe(iFD, "YOUR");
              *bpDaemonAvailableF = FALSE;
              *lpActivePID = lPID;
              strcpy(caActiveFileName, caFileNamePipeOut);
              *ipFDActive = iFD;
            }
            else
              SendToPipe(iFD, "NOTA");
          }
        }
        else	/* Daemon Busy */
        {
          iFD = OpenPipe(caFileNamePipeOut, O_WRONLY | O_NONBLOCK);

          if (iFD < 0)
            perror("Error opening PipeOut:");
          else
            SendToPipe(iFD, "BUSY");
        }
      }
      else
        perror("mkfifo:");
    }
    else if (!strncmp(caCommand, "STAT", COMMAND_LEN))
    {
      sscanf(caCommand + COMMAND_LEN + 1, "%ld", &lPID);
      if (*lpActivePID == lPID)
      {
        if (LOC_bMCS_AvailableF)
          SendToPipe(*ipFDActive, "AVAL");
        else
          SendToPipe(*ipFDActive, "NOTA");
      }
    }
    else
    {
      printf("Unsupported command '%s'\n", caCommand);
    }
  }
  else if (iBytesRead == 0)
  {
    printf("TTT - EOF on fifo - reopening...\n");
    close(*ipFDPipeIn);

    *ipFDPipeIn = open("/tmp/.mcsd-in.pipe", O_RDONLY | O_NONBLOCK);
printf("TTT - Open returned %d\n", *ipFDPipeIn);

    if (*ipFDPipeIn < 0)
    {
      perror("open Inbound pipe:");
      exit(-1);
    }

    *bpDaemonAvailableF = TRUE;

/* TTT - Figure out which guy caused this so we can clean up
         For now, just check the Active one */
    iFDtmp = *ipFDActive;
    if (write(iFDtmp, "TEST", 3) == -1)
    {
      perror("post write:");

      if (close(iFDtmp) == -1)
        perror("post close:");

      if (caActiveFileName[0] != '\0')
      {
printf("TTT deleting '%s'\n", caActiveFileName);
        if (unlink(caActiveFileName) != 0)
          perror("unlink:");
      }

      strcpy(caActiveFileName, "");
      *ipFDActive  = -1;
      *lpActivePID = -1;
printf("TTT After write\n");
    }
  }
  else
  {
    perror("Read PipeIn Command:");
  }
printf("here\n");

  return(TRUE);		/* TTT - ? */
}

int SendReceivedDataToActivePipe(const int iFDActive, const UCHAR ucType,
                                 const UCHAR ucButton,
                                 const UCHAR ucPressRelease,
                                 const SCHAR cShuttlePos, const SCHAR cJogPos)
{
  char caOutputLine[32];

  caOutputLine[0] = SENDER_MCSD;
  caOutputLine[1] = ucType;
  caOutputLine[2] = ucButton;
  caOutputLine[3] = ucPressRelease;
  caOutputLine[4] = cShuttlePos;
  caOutputLine[5] = cJogPos;
  caOutputLine[6] = '\n';

  if (write(iFDActive, caOutputLine, BYTES_PER_LINE) < BYTES_PER_LINE)
  {
    perror("write:");
    return(FALSE);
  }

  return(TRUE);
}
