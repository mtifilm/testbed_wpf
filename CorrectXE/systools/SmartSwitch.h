#ifndef SMART_SWITCH_H
#define SMART_SWITCH_H

#ifdef __sgi
//
// For SmartSwitch on SGI
//

int SetSmartSwitch(const char *caSerialPort, const char cSwitchPort);
int SetSmartSwitchNumeric(const char *caSerialPort, const int iSwitchPort);
#endif

#endif /* SMART_SWITCH_H */
