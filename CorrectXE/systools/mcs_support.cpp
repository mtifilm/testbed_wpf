/*****************************************************************************

        File:           mcs_support.cpp
	Written by:	Michael Russell
	Date:		December 21, 2001

	This file contains code for interacting with the JLCooper MCS box

*****************************************************************************/
#include <machine.h>

#ifdef _WINDOWS
#include "timestamp.h"
#include "W32Serial.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <fcntl.h>
#include <errno.h>
#include <limits.h>

#ifndef _WINDOWS
#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <stropts.h>
#if !defined(__linux)
#include <sys/z8530.h>
#endif /* !defined(__linux) */
#endif /* ifndef _WINDOWS */

#include <string.h>

#ifdef _WINDOWS
#include <io.h>
#endif

#include "MTIsleep.h"
#include "mcs_support.h"

/* Local Globals */
int  LOC_iDEBUG_F =  1;
char LOC_caCommand[MAX_TYPE_NUM   + 1][16];
char LOC_caButtons[MAX_BUTTON_NUM + 1][32];
#ifdef _WINDOWS
WSerial *LOC_ws_port = NULL;
#else
int  LOC_MCS_iFD  = -1;
#endif

/* Forward declarations */\
#ifdef _WINDOWS
WSerial *ConnectToMCS (int iNCommPort);
#else
int  ConnectToMCS (int iNCommPort);
#endif
int  HaveMCS();

/**********************************************************************/
/*                                                                    */
/*  InitMCS() - this sets up some strings related to the buttons      */
/*    and wheels and then attempts to connect to the MCS.             */
/*      It is called with the port number which the box is connected  */
/*    to.  For example InitMCS(2), would mean "/dev/ttyd2".           */
/*    Returns the FD of the opened port upon success, -1 for failure. */
/*                                                                    */
/*    Creation Date: 12/24/2001                                       */
/*    Creator:       Michael Russell                                  */
/*    Modification History:                                           */
/*                                                                    */
/**********************************************************************/
#ifdef _WINDOWS
WSerial *InitMCS(int iNCommPort)
#else
int  InitMCS(int iNCommPort)
#endif
{
#ifdef _WINDOWS
  WSerial *ws_port = NULL;
#else
  int iFD = -1;
#endif

  strcpy(LOC_caCommand[0],                "");
  strcpy(LOC_caCommand[MCS_BUTTON],       "Button");
  strcpy(LOC_caCommand[MCS_JOG_WHEEL],    "Jog Wheel");
  strcpy(LOC_caCommand[MCS_SHUTTLE_RING], "Shuttle");

  strcpy(LOC_caButtons[MCS_RECORD],       "Record");
  strcpy(LOC_caButtons[MCS_F6],           "F6");
  strcpy(LOC_caButtons[MCS_F4],           "F4");
  strcpy(LOC_caButtons[MCS_W7],           "W7");
  strcpy(LOC_caButtons[MCS_PLAY],         "Play");
  strcpy(LOC_caButtons[MCS_STOP],         "Stop");
  strcpy(LOC_caButtons[MCS_FAST_FORWARD], "Fast Forward");
  strcpy(LOC_caButtons[MCS_REWIND],       "Rewind");
  strcpy(LOC_caButtons[MCS_VSTICK_DOWN],  "V-Stick Down");
  strcpy(LOC_caButtons[MCS_VSTICK_UP],    "V-Stick Up");
  strcpy(LOC_caButtons[MCS_VSTICK_LEFT],  "V-Stick Left");
  strcpy(LOC_caButtons[MCS_VSTICK_RIGHT], "V-Stick Right");
  strcpy(LOC_caButtons[MCS_F3],           "F3");
  strcpy(LOC_caButtons[MCS_F2],           "F2");
  strcpy(LOC_caButtons[MCS_F1],           "F1");
  strcpy(LOC_caButtons[MCS_F5],           "F5");
  strcpy(LOC_caButtons[MCS_W1],           "W1");
  strcpy(LOC_caButtons[MCS_W2],           "W2");
  strcpy(LOC_caButtons[MCS_W3],           "W3");
  strcpy(LOC_caButtons[MCS_W4],           "W4");
  strcpy(LOC_caButtons[MCS_W5],           "W5");
  strcpy(LOC_caButtons[MCS_W6],           "W6");

  strcpy(LOC_caButtons[MCS_SHIFT_RECORD],       "Shift Record");
  strcpy(LOC_caButtons[MCS_SHIFT_F6],           "Shift F6");
  strcpy(LOC_caButtons[MCS_SHIFT_F4],           "Shift F4");
  strcpy(LOC_caButtons[MCS_SHIFT_W7],           "Shift W7");
  strcpy(LOC_caButtons[MCS_SHIFT_PLAY],         "Shift Play");
  strcpy(LOC_caButtons[MCS_SHIFT_STOP],         "Shift Stop");
  strcpy(LOC_caButtons[MCS_SHIFT_FAST_FORWARD], "Shift Fast Forward");
  strcpy(LOC_caButtons[MCS_SHIFT_REWIND],       "Shift Rewind");
  strcpy(LOC_caButtons[MCS_SHIFT_VSTICK_DOWN],  "Shift V-Stick Down");
  strcpy(LOC_caButtons[MCS_SHIFT_VSTICK_UP],    "Shift V-Stick Up");
  strcpy(LOC_caButtons[MCS_SHIFT_VSTICK_LEFT],  "Shift V-Stick Left");
  strcpy(LOC_caButtons[MCS_SHIFT_VSTICK_RIGHT], "Shift V-Stick Right");
  strcpy(LOC_caButtons[MCS_SHIFT_F3],           "Shift F3");
  strcpy(LOC_caButtons[MCS_SHIFT_F2],           "Shift F2");
  strcpy(LOC_caButtons[MCS_SHIFT_F1],           "Shift F1");
  strcpy(LOC_caButtons[MCS_SHIFT_F5],           "Shift F5");
  strcpy(LOC_caButtons[MCS_SHIFT_W1],           "Shift W1");
  strcpy(LOC_caButtons[MCS_SHIFT_W2],           "Shift W2");
  strcpy(LOC_caButtons[MCS_SHIFT_W3],           "Shift W3");
  strcpy(LOC_caButtons[MCS_SHIFT_W4],           "Shift W4");
  strcpy(LOC_caButtons[MCS_SHIFT_W5],           "Shift W5");
  strcpy(LOC_caButtons[MCS_SHIFT_W6],           "Shift W6");

#ifdef _WINDOWS
  ws_port = ConnectToMCS (iNCommPort);
  if (ws_port==NULL)
  {
    fprintf(stderr, "ConnectToMCS failed.  Returning...\n");
    return(NULL);
  }

  return(ws_port);
#else
  if (!(iFD = ConnectToMCS(iNCommPort)))
  {
    fprintf(stderr, "ConnectToMCS failed.  Returning...\n");
    return(FALSE);
  }

  return(iFD);
#endif
}

/**********************************************************************/
/*                                                                    */
/*  CheckMCS() - checks for a command from the MCS and returns TRUE   */
/*    if present, along with calling ParseMCSCommand to split the     */
/*    command into separate useful bytes.                             */
/*                                                                    */
/*    Creation Date: 12/24/2001                                       */
/*    Creator:       Michael Russell                                  */
/*    Modification History:                                           */
/*      1/16/2002 - mpr - moved bulk of code to ParseMCSCommand().    */
/*                                                                    */
/**********************************************************************/
int  CheckMCS(UCHAR *ucpType, UCHAR *ucpButton, UCHAR *ucpPressRelease,
              SCHAR *cpShuttlePos, SCHAR *cpJogPos)
{
  int           iFill;
  int           iRet = FALSE;
  unsigned char ucaResponse[16];
#ifdef _WINDOWS
  stamp_t       st;
#endif

//printf("Checking...\n");

#ifdef _WINDOWS
  iFill = ReadBytes(LOC_ws_port, ucaResponse, 2);
#else
  iFill = ReadBytes(LOC_MCS_iFD, ucaResponse, 2);
#endif
//    printf ("iFill: %d\n", iFill);

  if (iFill == 0)
    iRet = 0;
  else if (iFill == 2)
    iRet = ParseMCSCommand(ucaResponse, ucpType, ucpButton, ucpPressRelease,
                           cpShuttlePos, cpJogPos);
  else if (iFill == -1)
  {
    perror("ReadBytes Error:");
    iRet = -1;
  }
  else
  {
    fprintf(stderr, "Unexpected number of bytes %d\n", iFill);
    iRet = -1;
  }

  return(iRet);
}

/**********************************************************************/
/*                                                                    */
/*  ConnectToMCS() - opens the MCS port and verifies that it is       */
/*    present.  Note that the MCS must be used at 2400 baud,          */
/*    no parity.  Returns the FD of the port if successful, -1 if     */
/*    not.                                                            */
/*                                                                    */
/*    Creation Date: 12/24/2001                                       */
/*    Creator:       Michael Russell                                  */
/*    Modification History:                                           */
/*                                                                    */
/**********************************************************************/
#ifdef _WINDOWS
WSerial *ConnectToMCS (int iNCommPort)
#else
int ConnectToMCS (int iNCommPort)
#endif
{
  char   caFile[64];
  char   caError[256];
  int    i;
#ifndef _WINDOWS
  struct termios termios;
#endif

#ifdef _WINDOWS
  sprintf (caFile, "\\\\.\\COM%1d", iNCommPort);

  LOC_ws_port = new WSerial();
  if (LOC_ws_port == (WSerial*) 0) {
    sprintf(caError, "ConnectToMCS: new WSerial() failed");
    perror(caError);
    return(NULL);
  }
//printf("TTT WSerial() OK\n");
#else
  sprintf (caFile, "/dev/ttyd%d", iNCommPort);

  LOC_MCS_iFD = open(caFile, O_RDWR | O_NDELAY);

  if (LOC_MCS_iFD < 0)
  {
    sprintf(caError, "ConnectToMCS: Couldn't open port '%s'", caFile);
    perror(caError);
    return(FALSE);
  }
#endif

/* set tty to 2400 baud, 8 bits, no parity, 1 (default) stop bit */
#ifdef _WINDOWS
  LOC_ws_port->Baud     = 2400;
  LOC_ws_port->Bits     = 8;
  LOC_ws_port->StopBits = ONESTOPBIT;
  LOC_ws_port->Parity   = NOPARITY;

  if (!LOC_ws_port->Open(caFile))
  {
    sprintf(caError, "ConnectToMCS: Open(%s) failed", caFile);
    perror(caError);
    return(NULL);
  }
//printf("TTT LOC_ws_port open\n");
#else
  termios.c_iflag  = 0;
  termios.c_oflag  = 0;
  termios.c_cflag  = CS8 | CREAD | CLOCAL;
  termios.c_lflag  = 0;
  termios.c_ospeed = B2400;
  termios.c_ispeed = 0;
  for (i=0; i<NCCS; i++)	/* Fill entire c_cc array or weird problems */
    termios.c_cc[i]  = 0;

  if (ioctl(LOC_MCS_iFD, TCSETA, &termios) == -1)
  {
    fprintf (stderr, "ioctl A returned an MCS_FAILURE\n");
    return(FALSE);
  }

/*
        Suppress SGI's attempt to improve the throughput by delaying some
        commands.
*/

#if defined(__sgi)
  if (ioctl(LOC_MCS_iFD, SIOC_ITIMER, 0) == -1)
  {
    fprintf (stderr, "ioctl B returned an MCS_FAILURE\n");
    return(FALSE);
  }
#endif /* defined(__sgi) */
#endif /* else of ifdef _WINDOWS */

  /* Now that the port's open, see if there's an MCS there */
  if (HaveMCS())
  {
    if (LOC_iDEBUG_F)
      fprintf(stderr, "MCS found on port %s\n", caFile);
#ifdef _WINDOWS
    return(LOC_ws_port);
#else
    return(LOC_MCS_iFD);
#endif
  }
  else
  {
    fprintf (stderr, "Port connected, but MCS not found\n");
#ifdef _WINDOWS
    return(NULL);
#else
    close(LOC_MCS_iFD);		/* Since not there, we close the port */
    return(FALSE);
#endif
  }
}

/**********************************************************************/
/*                                                                    */
/*  HaveMCS() - check if MCS attached (it must have already been      */
/*    opened.  This is done by sending the MCS Inquiry command        */
/*    and waiting for the reply.                                      */
/*                                                                    */
/*    Creation Date: 12/24/2001                                       */
/*    Creator:       Michael Russell                                  */
/*    Modification History:                                           */
/*                                                                    */
/**********************************************************************/
int HaveMCS()
{
  unsigned char ucaCommand[1];
  unsigned char ucaResponse[2];
  bool          bDoneF;
  int           iNumLoops = 0;
  int           iFill;
  int           iFound = FALSE;
#ifdef _WINDOWS
  stamp_t       st;
#endif

  // zero ucaResponse
  ucaResponse[0] = NULL;
  ucaResponse[1] = NULL;

#ifdef _WINDOWS
  if (LOC_ws_port != NULL)
#else
  if (LOC_MCS_iFD != -1)
#endif
  {
    ucaCommand[0] = MCS_INQUIRY;

    /* Ask it */
#ifdef _WINDOWS
    if (LOC_ws_port->SendBuffer((const char*)ucaCommand, 1) < 0)
#else
    if (write(LOC_MCS_iFD, ucaCommand, 1) < 0)
#endif
    {
      fprintf (stderr, "Error writing to MCS\n");
      return(FALSE);
    }

    /* Check it */
#ifdef _WINDOWS
    iFill = ReadBytes(LOC_ws_port, ucaResponse, 2);
#else
    iFill = ReadBytes(LOC_MCS_iFD, ucaResponse, 2);
#endif

    bDoneF = FALSE;

    while (!bDoneF)
    {
      if (iFill == -1)
      {
        perror("ReadBytes error reading from MCS");
        bDoneF = TRUE;
        iFound = FALSE;
      }
      else if (iFill == 0)
      {
        iNumLoops++;
        MTImillisleep(20);

        if (iNumLoops > 100)
        {
printf("TTT Never got Response from MCS\n");
          bDoneF = TRUE;
          iFound = FALSE;
        }
        else
        {
#ifdef _WINDOWS
          iFill = ReadBytes(LOC_ws_port, ucaResponse, 2);
#else
          iFill = ReadBytes(LOC_MCS_iFD, ucaResponse, 2);
#endif
        }
      }
      else
      {
        if ((ucaResponse[0] == MCS_RESPONSE_1)
         && (ucaResponse[1] == MCS_RESPONSE_2))
        {
//          printf("TTT Got 0x%x 0x%x\n", ucaResponse[0], ucaResponse[1]);
          bDoneF = TRUE;
          iFound = TRUE;
        }
      }
    }

    return(iFound);
  }
  else
  {
    fprintf(stderr, "Must open MCS first with ConnectToMCS\n");
    return(FALSE);
  }
}

/**********************************************************************/
/*                                                                    */
/*  ReadBytes() - reads specified number of bytes from the specified  */
/*    file descriptor.  The MCS always responds with 2 bytes, so      */
/*    using this routine saves the programmer from having to assemble */
/*    the two bytes into one unsigned char *.                         */
/*      Note that if 0 bytes are read, this routine returns           */
/*    immediately (just like read).  However if 1 byte is read this   */
/*    routine will wait a short amount of time to get the additional  */
/*    bytes.  It will return the number of bytes actually read.       */
/*                                                                    */
/*    Creation Date: 12/24/2001                                       */
/*    Creator:       Michael Russell                                  */
/*    Modification History:                                           */
/*                                                                    */
/**********************************************************************/
#ifdef _WINDOWS
int ReadBytes(WSerial *ws_port, unsigned char *ucpResponse,
              const int iNumToRead)
#else
int ReadBytes(const int iFD, unsigned char *ucpResponse, const int iNumToRead)
#endif
{
  int           iFill;
  unsigned char ucaTmp[1];
  bool          bDoneF;
  int           iPos = 0;
  bool          bGotDataF = FALSE;
  int           iNumLoops = 0;
#ifdef _WINDOWS
  stamp_t       st;
#endif

  bDoneF = FALSE;
  while (!bDoneF)
  {
#ifdef _WINDOWS
    iFill = ws_port->ReadBuffer(ucaTmp, 1, 0, &st);
#else
    iFill = read(iFD, ucaTmp, 1);
#endif
//printf("TTT iFill %d\n", iFill);

    if (iFill == 0)
    {
      if (!bGotDataF)		/* return immediately if nothing */
        return(0);
      else
      {
        iNumLoops++;
        MTImillisleep(20);

        if (iNumLoops > 10)
        {
printf("TTT Only got %d chars - wanted %d (%d)\n", iPos, iNumToRead, iNumLoops);
{
int i;
for (i=0; i<iPos;i++)
 printf("%d: 0x%x\n", i, ucpResponse[i]);
}
          bDoneF = TRUE;
          return(iPos);
        }
      }
    }
    else if (iFill < 0)
    {
//      fprintf(stderr, "Unexpected iFill %d\n", iFill);
//printf("TTT Only got %d chars - wanted %d (%d)\n", iPos, iNumToRead, iNumLoops);
      bDoneF = TRUE;
      return(iFill);
    }
    else
    {
      bGotDataF = TRUE;
      ucpResponse[iPos] = ucaTmp[0];
//printf("TTT Got 0x%x\n", ucaTmp[0]);
      iPos++;
      if (iPos == iNumToRead)
        bDoneF = 1;
    }
  }

  return(iPos);
}

/**********************************************************************/
/*                                                                    */
/*  ParseMCSCommand() - parses the 2 bytes from the MCS and sets the  */
/*    passed arguments to the parsed values.                          */
/*      *ucpType will specify whether a button was pressed or the jog */
/*    wheel or shuttle rings was moved.  In the case of a button      */
/*    press/release, *ucpButton will reflect which button and         */
/*    *ucpPressRelease whether a press or release was caught.  In the */
/*    case of a change in the shuttle ring, *ucpShuttlePos will be    */
/*    set to the absolute position of the Ring from -12 to 12.  And,  */
/*    for the jog wheel, the change since the last movement will be   */
/*    reported.  The value will range from -62 to 63.  Note that a    */
/*    full turn of the jog wheel is approximately 200 units.          */
/*      You must check *ucpType to determine which of the other       */
/*    values are meaningful.                                          */
/*                                                                    */
/*    Creation Date: 01/16/2002                                       */
/*    Creator:       Michael Russell                                  */
/*    Modification History:                                           */
/*                                                                    */
/**********************************************************************/
int  ParseMCSCommand(const UCHAR *ucaResponse, UCHAR *ucpType,
                     UCHAR *ucpButton, UCHAR *ucpPressRelease,
                     SCHAR *cpShuttlePos, SCHAR *cpJogPos)
{
  static bool bShiftStatus = FALSE;

  *ucpType         = 0;	/* Initialize these */
  *ucpButton       = 0;
  *ucpPressRelease = 0;
  *cpShuttlePos    = 0;
  *cpJogPos        = 0;

  if (ucaResponse[0] == 0x80)
    *ucpType = MCS_BUTTON;
  else if (ucaResponse[0] == 0x81)
    *ucpType = MCS_JOG_WHEEL;
  else if (ucaResponse[0] == 0x82)
    *ucpType = MCS_SHUTTLE_RING;
  else
  {
    fprintf(stderr, "Unexpected command from MCS 0x%x\n", ucaResponse[0]);
    return(-1);
  }

  if (*ucpType == MCS_BUTTON)
  {
    *ucpButton       = ucaResponse[1] & 0x1f;		/* last 5 bits */
    *ucpPressRelease = (ucaResponse[1] & 0x40) >> 6;	/* bit 6 */
//    printf("TTT bef %s %s %s (0x%x)\n", LOC_caCommand[*ucpType],
//           LOC_caButtons[*ucpButton],
//           (*ucpPressRelease == 0) ? "release" : "press", ucaResponse[1]);

    if (*ucpButton == MCS_STOP)		/* STOP held down is shift */
    {
      if (*ucpPressRelease == MCS_PRESS)
        bShiftStatus = TRUE;
      else	/* MCS_RELEASE */
        bShiftStatus = FALSE;
    }
    else
    {
      if (bShiftStatus)
        (*ucpButton) += MCS_SHIFT_DIFF;
    }

    printf("TTT %s %s %s (0x%x)\n", LOC_caCommand[*ucpType],
           LOC_caButtons[*ucpButton],
           (*ucpPressRelease == 0) ? "release" : "press", ucaResponse[1]);
  }
  else if (*ucpType == MCS_JOG_WHEEL)
  {
    if ((ucaResponse[1] & 0x40) == 0x40)	/* negative, so sign extend */
      *cpJogPos = ((signed char) ucaResponse[1]) | 0x80;
    else
      *cpJogPos = (signed char) ucaResponse[1];

    printf("%s %d (0x%x)\n", LOC_caCommand[*ucpType], (int) *cpJogPos,
       ucaResponse[1]);
  }
  else if (*ucpType == MCS_SHUTTLE_RING)
  {
    if ((ucaResponse[1] & 0x40) == 0x40)	/* negative, so sign extend */
      *cpShuttlePos = ((signed char) ucaResponse[1]) | 0x80;
    else
      *cpShuttlePos = (signed char) ucaResponse[1];

    if (*cpShuttlePos == 1)
      printf("%s %d (or I'm an MCS-3)\n", LOC_caCommand[*ucpType],
             (int) *cpShuttlePos);
    else
      printf("%s %d\n", LOC_caCommand[*ucpType], (int) *cpShuttlePos);
  }
  else
  {
    fprintf(stderr, "Invalid MCS Command received.\n");
    return(-1);
  }

  return(TRUE);
}
