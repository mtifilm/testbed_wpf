/*
   File Name: SwitchUser.cpp
   Created March 27 by John Mertus

   This allows an process with sticky bit set to switch back and
   forth between users.  See header for details

*/
#include "SwitchUser.h"
#include "machine.h"
#ifdef _WINDOWS
#include "SwitchUserWin.cc"
#elif __sgi
#include "SwitchUserSGI.cc"
#endif
