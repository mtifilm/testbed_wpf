#ifndef EventH
#define EventH

#include "IniFile.h"
#include <memory>

class Event;
typedef std::shared_ptr<Event> EventSharedPtr;

///////////////////////////////////////////////////////////////////////
///////// E V E N T  W R A P P E R  C L A S S /////////////////////////
///////////////////////////////////////////////////////////////////////

class Event
{
	HANDLE _eventHandle = INVALID_HANDLE_VALUE;

public:
	//-------------------------------
	static const int WaitForever = -1;

	//-------------------------------
	Event()
	{
		_eventHandle = ::CreateEvent(
								NULL,                 // default security attributes
								TRUE,                 // manual-reset event
								FALSE,                // initial state is nonsignaled
								NULL                  // no object name
								);

		// TODO: reaky handle failure.
		MTIassert(_eventHandle != INVALID_HANDLE_VALUE);
	};

	~Event()
	{
		::CloseHandle(_eventHandle);
		_eventHandle = INVALID_HANDLE_VALUE;
	};

	//-------------------------------
	void signal()
	{
		BOOL setEventSucceeded = ::SetEvent(_eventHandle);

		// TODO: really handle failure.
		MTIassert(setEventSucceeded);
	};

	//-------------------------------
	bool wait(int msecs = WaitForever)
	{
		int waitResult = ::WaitForSingleObject(
									_eventHandle,
									(msecs == WaitForever) ? INFINITE : msecs);

		// TODO: Really handle WAIT_FAILED
		MTIassert(waitResult != WAIT_FAILED);

		return waitResult == WAIT_OBJECT_0;
	};

	//-------------------------------
	void reset()
	{
		BOOL resetEventSucceeded = ResetEvent(_eventHandle);

		// TODO: Really handle WAIT_FAILED
		MTIassert(resetEventSucceeded);
	};
};

#endif

