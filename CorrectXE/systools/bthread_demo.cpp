/*
	file: bthread_demo.c
	by:   Kevin Manbeck
	date: April 1, 2001

	This program shows how to call the bthread functions.

	cc bthread_demo.c bthread.c -I../include -o bthread_demo -lpthread -lm
*/

#include <stdio.h>
#include "bthread.h"
#include <math.h>

typedef struct
 {
  int
    iNapAmount,
    iAppID;

  int
    iGrandTotalNapping;

  void
    *vpCriticalSection;
 } APP_DATA_STRUCT;

void AppFcn (void *vpAppData, void *vpReserved)
{
  APP_DATA_STRUCT *adsp = (APP_DATA_STRUCT *)vpAppData;
  int i, iNapAmount, iAppID, j;
  float fTst;
  
  printf ("I have entered AppFcn: %d\n", adsp->iAppID);

		// extract the volatile data from vpAppData
  iNapAmount = adsp->iNapAmount;
  iAppID = adsp->iAppID;

  printf ("I am unblocking my parent: %d\n", adsp->iAppID);

	// Pass the reserved pointer in order to release the parent
  if (BThreadBegin (vpReserved))
   {
    printf ("BThreadBegin was a FAILURE\n");
    exit (1);
   }

  for (i = 0; i < 50; i++)
   {
    if (i == 10)
     {
      if (CriticalSectionEnter (adsp->vpCriticalSection))
       {
        printf ("CriticalSectionEnter() ended in error\n");
        perror ("error:");
       }
      printf ("\n\n Entering critical section for %d\n", iAppID);
     }
    if (i == 20)
     {
      printf (" Exiting critical section for %d\n", iAppID);
      if (CriticalSectionExit (adsp->vpCriticalSection))
       {
        printf ("CriticalSectionExit() ended in error\n");
        perror ("error:");
       }
     }
    fTst = tan(exp(cos(sin(atan((float)(i+iNapAmount))))));
    for (j = 0; j < 10000; j++)
     {
      fTst = tan(exp(cos(sin(atan(fTst + (float)j)))));
     }
   }

  printf ("I am finishing the appliction function: %d\n", iAppID);

////  if (BThreadEnd (vpReserved))
////   {
////    printf ("BThreadEnd was a FAILURE\n");
////   }

////  printf ("The code should never reach this point\n");
}  /* AppFcn */

main ()
{
  APP_DATA_STRUCT ads;

  ads.iGrandTotalNapping = 0;
  ads.vpCriticalSection = CriticalSectionAlloc ();
  if (ads.vpCriticalSection == NULL)
    printf ("Error in CriticalSectionAlloc\n");

	// start up the first bthread
  ads.iNapAmount = 20;
  ads.iAppID = 0;
  BThreadSpawn (&AppFcn, &ads);

	// start up the second bthread
  ads.iNapAmount = 30;
  ads.iAppID = 1;
  BThreadSpawn (&AppFcn, &ads);

	// start up the third bthread
  ads.iNapAmount = 10;
  ads.iAppID = 2;
  BThreadSpawn (&AppFcn, &ads);

	// start up the fourth bthread
  ads.iNapAmount = 0;
  ads.iAppID = 3;
  BThreadSpawn (&AppFcn, &ads);

	// start up the fifth bthread
  ads.iNapAmount = 15;
  ads.iAppID = 4;
  BThreadSpawn (&AppFcn, &ads);

	// start up the sixth bthread
  ads.iNapAmount = 25;
  ads.iAppID = 5;
  BThreadSpawn (&AppFcn, &ads);

	// wait here while the processes run

  sginap (500);

  if (CriticalSectionFree (ads.vpCriticalSection))
    printf ("Error in CriticalSectionFree\n");

}  /* main */
