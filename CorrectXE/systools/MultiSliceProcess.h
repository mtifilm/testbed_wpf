// ---------------------------------------------------------------------------

#ifndef MultiSliceProcessH
#define MultiSliceProcessH
// ---------------------------------------------------------------------------

#include "systoolsDLL.h"
#include "Ippheaders.h"
#include <algorithm>

// This defines an ROI
struct RoiRect
{
  void *data;           // Uppder left hand corner
  IppiSize roiSize;     // Width and height of ROI
  int step;              // Step in bytes
  double maxValue;      // The maximum source value: must be (power of 2) - 1
};

struct SliceParams
{
    RoiRect srcRoi;
    RoiRect dstRoi;
};

typedef void(*ImageWork)(const SliceParams & params, void *args, ...);

void MTI_SYSTOOLSDLL_API Convert16uToNormalized32f(Ipp16u* src, int srcStep,
    Ipp32f *dst, int dstStep, IppiSize size, Ipp16u maxValue);

#endif
