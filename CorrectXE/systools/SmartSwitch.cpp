/**********************************************************************/
/*                                                                    */
/*  SmartSwitch Control Code                                          */
/*                                                                    */
/*     11/03/02 - Michael Russell - Creation                          */
/*      2/24/03 - mpr - clean up for first SGI release                */
/*                                                                    */
/*  Notes: SGI only                                                   */
/*                                                                    */
/**********************************************************************/
      
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <fcntl.h>
#include <errno.h>
#include <limits.h>
#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <stropts.h>

#include <sys/types.h>
#include <sys/stat.h>

#define TRUE  1
#define FALSE 0

#define ESC '\x1b'		/* decimal 27 */
#define STX '\x2'
#define EOT '\x4'

#include "SmartSwitch.h"

/**********************************************************************/
/*                                                                    */
/*  SetSmartSwitch() - sets smart switch to the specified port.       */
/*    It is called with the device name and port label.               */
/*                                                                    */
/*      e.g. SetSmartSwitch("/dev/ttyd1", 'B');                       */
/*                                                                    */
/*    Returns TRUE (1) upon success or FALSE (0) upon failure.        */
/*                                                                    */
/*    Creation Date: 02/24/2003                                       */
/*    Creator:       Michael Russell                                  */
/*    Modification History:                                           */
/*                                                                    */
/**********************************************************************/
int SetSmartSwitch(const char *caSerialPort, const char cSwitchPort)
{
  int            fd;
  struct termios termios;
  char           caCommand[3];
  ssize_t        stRet;
  int            i;
  int            iRet;
  struct stat    statbuf;

  /* Error Checking */
  if ((cSwitchPort < 'A') || (cSwitchPort > 'D'))
  {
    printf("SmartSwitch Error: Illegal Port - Expected 'A' -> 'D', Got '%c'\n",
            cSwitchPort);
    fflush(stdout);
    return FALSE;
  }

//  TRACE_2(errout << "SmartSwitch: dev: '" << caSerialPort << "', port: '" <<
//           cSwitchPort << "'");
#ifdef DEBUG
  printf("SmartSwitch: dev: '%s', port: '%c'\n", caSerialPort, cSwitchPort);
  fflush(stdout);
#endif

  /* Make sure device exists */
  if (stat(caSerialPort, &statbuf) != 0)
  {
    printf("SmartSwitch Error: Serial Port '%s' does not exist\n",
           caSerialPort);
    fflush(stdout);
    return(FALSE);
  }

  /* Make sure device is a character device */
  if (!S_ISCHR(statbuf.st_mode))
  {
    printf("SmartSwitch Error: Serial Port '%s' not CHR device\n",
           caSerialPort);
    fflush(stdout);
    return(FALSE);
  }

  /* Open Port */
  fd = open(caSerialPort, O_RDWR);
  if (fd < 0)
  {
    printf("SmartSwitch Error: Cannot open Serial Port '%s'\n", caSerialPort);
    fflush(stdout);
    return FALSE;
  }

  /* Set Baud Rate, etc. */
  termios.c_iflag  = 0;
  termios.c_oflag  = 0;
  termios.c_cflag  = CS8 | CREAD | CLOCAL;
  termios.c_lflag  = 0;
  termios.c_ospeed = B1200;
  termios.c_ispeed = 0;

  for (i=0; i<NCCS; i++)        /* Fill entire c_cc array or weird problems */
    termios.c_cc[i] = 0;

  if (ioctl(fd, TCSETA, &termios) == -1)
  {
    printf("SmartSwitch Error: ioctl 1 problem\n");
    fflush(stdout);
    perror("");
    return FALSE;
  }

  /* Send "Disable all ports" */
  caCommand[0] = ESC;
  caCommand[1] = STX;
  caCommand[2] = EOT;
  stRet = write(fd, caCommand, 3);

  if (stRet != 3)
  {
    printf("SmartSwitch Error: Write #1 to port %s, exp = 3, got = %d\n",
            caSerialPort, stRet);
    fflush(stdout);
    perror("");
    return FALSE;
  }
  
  sginap (2);

  /* Send "Select specified port" */
  caCommand[0] = ESC;
  caCommand[1] = STX;
  caCommand[2] = cSwitchPort;

  stRet = write(fd, caCommand, 3);
  if (stRet != 3)
  {
    printf("SmartSwitch Error: Write #2 to port %s, exp = 3, got = %d\n",
            caSerialPort, stRet);
    fflush(stdout);
    perror("");
    return FALSE;
  }

  sginap (20);

  iRet = close(fd);
  if (iRet == -1)
  {
    printf("SmartSwitch Error: close failed\n");
    fflush(stdout);
    perror("");
    return FALSE;
  }

  return TRUE;
}

/**********************************************************************/
/*                                                                    */
/*  SetSmartSwitchNumeric() - sets smart switch to specified port,    */
/*    where the numbers 1..4 are remapped to A..D and the real call   */
/*    to SetSmartSwitch is made.                                      */
/*      It is called with the device name and port label.             */
/*                                                                    */
/*         e.g. SetSmartSwitch("/dev/ttyd1", 2);                      */
/*                                                                    */
/*    Returns TRUE (1) upon success or FALSE (0) upon failure.        */
/*                                                                    */
/*    Creation Date: 02/24/2003                                       */
/*    Creator:       Michael Russell                                  */
/*    Modification History:                                           */
/*                                                                    */
/**********************************************************************/
int SetSmartSwitchNumeric(const char *caSerialPort, const int iSwitchPort)
{
  char cSwitchPort;
  int  iRet = -1;

  if (iSwitchPort == 1)
    cSwitchPort = 'A';
  else if (iSwitchPort == 2)
    cSwitchPort = 'B';
  else if (iSwitchPort == 3)
    cSwitchPort = 'C';
  else if (iSwitchPort == 4)
    cSwitchPort = 'D';
  else
  {
    printf("Error: Illegal Port - Expected '1' -> '4', Got '%d'\n",
            iSwitchPort);
    return FALSE;
  }

  iRet = SetSmartSwitch(caSerialPort, cSwitchPort);

  return iRet;
}
