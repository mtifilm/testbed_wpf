//---------------------------------------------------------------------------

#ifndef SynchronousThreadRunnerH
#define SynchronousThreadRunnerH
//---------------------------------------------------------------------------

#include "IniFile.h"
#include "Event.h"
#include "systoolsDLL.h"
#include "ThreadLocker.h"
#include "Ippheaders.h"

#include <atomic>
#include <mutex>
//---------------------------------------------------------------------------

MTI_SYSTOOLSDLL_API class SynchronousThreadRunner
{
	typedef int (*ThreadWorkFunctionType)(void *, int iJob, int totalJobs);

public:
	SynchronousThreadRunner(int nJobs, void *parameter, ThreadWorkFunctionType primaryFunctionPtr);
   SynchronousThreadRunner(int nJobs, int nThreads, void *parameter, ThreadWorkFunctionType primaryFunctionPtr);
	SynchronousThreadRunner(void *parameter, ThreadWorkFunctionType primaryFunctionPtr);
	~SynchronousThreadRunner();

	int GetNumberOfThreads();

   static IppiRect findSliceRoi(int jobNumber, int totalJobs, const IppiSize &size);

	int Run();

private:
	ThreadWorkFunctionType _workFunction = nullptr;
	void *_workFunctionParam = nullptr;
   int _numberOfThreadsToUse = 0;
	int _totalNumberOfJobs = 0;
	int _numberOfJobsRun = 0;
   int _numberOfThreadsStillRunning = 0;
	int _firstError;
	CSpinLock _jobLock;
	Event _allJobsCompleteEvent;
	bool _allWorkIsComplete = true;

	static void BounceToWorkerThread(void *vp, void *vpReserved);
	void RunWorkerThread();
};

#endif
