//---------------------------------------------------------------------------

#pragma hdrstop

#include "ran266.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

/*************************************************
	UNIFORM DISTRIBUTION [0,1) generator ran266

	From Kevin Manbeck, August 9, 1994

	NOTE:  Use ODD seed for good performance
**************************************************/

#define RND_MODULUS 67108864

#if 0
DON'T USE THIS SHIT - Makes nasty lines in DRS & PAINT when fix width is close to a power of two
float ran266(long *lseed)

{
	float ran;

	*lseed = 25*(*lseed);
	*lseed = *lseed % RND_MODULUS;
	*lseed = 25*(*lseed);
	*lseed = *lseed % RND_MODULUS;
	*lseed = 5*(*lseed);
	*lseed = *lseed % RND_MODULUS;

	ran = (float) *lseed / ((float)RND_MODULUS);

	return (ran);
}
#else

// faster, more uniform (lower stdDev),
// produces no artifacts in DRS & PAINT
// note use of bswap instruction

float ran266(long *lseed)
{
#if USE_INTEL_ASM_SYNTAX

	long fct = *lseed;

	_asm {

		mov ebx,25
		mov ecx,RND_MODULUS

		mov eax,fct
		mul ebx
		div ecx

		bswap edx

		mov eax,edx
		mul ebx
		div ecx

		mov fct,edx
   }

	*lseed = fct;

#else // ATT syntax

	__asm__
	(
	"	movl    $25,%%ebx;       "
	"  movl    $67108864,%%ecx; "
	"                           "
	"  movl    (%0),%%eax;      "
	"  mull    %%ebx;           "
	"  divl    %%ecx;           "
	"                           "
	"  bswapl  %%edx;           "
	"                           "
	"  movl    %%edx,%%eax;     "
	"  mull    %%ebx;           "
	"  divl    %%ecx;           "
	"                           "
	"  movl    %%edx,(%0);      "
	: // No outputs
	: "r"(lseed)  // input (--> register referred to as %0)
	: "eax", "ebx", "ecx", "edx");  // Register clobber list

#endif

   // float conversion adds overhead
	return ((float) *lseed) / ((float)RND_MODULUS);
}

#endif
