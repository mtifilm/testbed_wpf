/*
   File Name:  SafeClasses.cpp
   Created: December 3, 2000 by John Mertus
   Version 1.00
*/

#include "SafeClasses.h"
#include "IniFile.h"
#include "guid_mti.h"

#ifdef __unix
#include <unistd.h>
#include <sys/param.h>
#endif


MTI_SYSTOOLSDLL_API string MTIUniqueFileName(const string &Template)
//
//  Call this function with the COMPLETE path and file name in template.
//  Return will the template + GUID if template+GUID is a possible file name
//  "" if the new file name would be too long
//
//***************************************************************************
{
   // Create a UID
   string s = guid_to_string(guid_create());

   // Kill all hyphens
   SearchAndReplace(s, "-", "");

   // Create an extension with the GUI
   string Result = Template + "." + s;

   // Check for file too big
#ifdef _WINDOWS
   if (Result.size() >= (MAX_PATH - 1)) return "";
#endif

#ifdef __unix
   if (Result.size() >= pathconf(Template.c_str(), _PC_PATH_MAX)) return "";
#endif
   return Result;
}

