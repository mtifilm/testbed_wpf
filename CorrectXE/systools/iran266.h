//---------------------------------------------------------------------------

#ifndef iran266H
#define iran266H
//---------------------------------------------------------------------------

#include "systoolsDLL.h"

// This has to be in its own cpp module because of asm code.
MTI_SYSTOOLSDLL_API unsigned long iran266(long *lseed);

#endif
