//
// MCS .h file
//

// #include <Xm/Xm.h>
#include <X11/Intrinsic.h>

// Forward Declarations
int MCSConnect(void);
int MCSAppAddInput(XtAppContext xtAC);
int MCSAddCallback(int, int, int, XtCallbackProc, XtPointer);
int MCSSetJogWheelPosition(int);
int MCSGetJogWheelPosition(void);

class CMCS
{
  public:
    CMCS(void);
    virtual ~CMCS();
    
    int  Get_iFDPipeFromDaemon();
    int  GetJogWheelPosition();
    int  SetJogWheelPosition(int);

    bool bInitialized;

    XtInputId xtInputID;

    void ProcessBoxDataCallback(XtPointer client_data,
                               int       *iFDIn,
                               XtInputId *XtInputID);
    void AddCallback(int            iItem,
                     int            iButtonWheel,
                     int            iPressRelease,
                     XtCallbackProc xtCallback,
                     XtPointer      xtData);

  private: 
    virtual void ProcessBoxData(XtPointer client_data,
                               int       *iFDIn,
                               XtInputId *XtInputID);
    int iFDPipeFromDaemon;
    int iFDPipeToDaemon;
    int iJogWheelPosition;

    struct MyCallBack
    {
      XtCallbackProc funcCallBack;
      XtPointer      funcData;
    } scbCallBack[MAX_TYPE_NUM + 1][MAX_BUTTON_NUM + 1][2];

    struct MyStruct
    {
      int i;
      int j;
    } stMyData;
};
