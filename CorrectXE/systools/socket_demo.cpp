
#include <stdio.h>
#include <stdlib.h>
#include "machine.h"
#include "socket.h"
#include "getopt.h"

#ifdef __unix
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <signal.h>
#include <unistd.h>
#include <ctype.h>
#endif


#ifdef _WINDOWS
#endif


#define TRUE 1
#define FALSE 0

void printUsage()
{
  fprintf (stderr, "Usage net_demo [-s socket] [-n net_name] [-a] [-b]\n");
  fprintf (stderr, "socket is the socket number to use\n");
  fprintf (stderr, "net_name is the network name of the socket server\n");
  fprintf (stderr, "-a to run in server mode\n");
  fprintf (stderr, "-b to run in client mode\n");

  return;
}

int main(int iArgC, char **cpArgV)
{
  char bufferOUT[100];
  char bufferIN[100];
  int iFDSocket, iClientMode, iOpt, iSocket;
  char caNetName[256];

  if (iArgC < 2)
   {
    printUsage();
    return 1;
   }

  iSocket = 3122;
  sprintf (caNetName, "localhost");
  iClientMode = -1;

  while ((iOpt = getopt (iArgC, cpArgV, "s:n:ab")) != EOF)
   {
    switch (iOpt)
     {
      case 's':
        iSocket = atoi(optarg);
      break;
      case 'n':
        strcpy (caNetName, optarg);
      break;
      case 'a':
        iClientMode = FALSE;
      break;
      case 'b':
        iClientMode = TRUE;
      break;
     }
   }

  if (iSocket == 0)
   {
    fprintf (stderr, "Must enter a valid socket number\n");
    return 1;
   }

  if (iClientMode == TRUE && caNetName[0] == '\0')
   {
    fprintf (stderr, "Must enter a valid name for the socket server\n");
    return 1;
   }

  if (iClientMode == -1)
   {
    fprintf (stderr, "Must use either SERVER (-a) or CLIENT (-b) mode\n");
    return 1;
   }

#ifdef _WINDOWS
  // open network interface
  int iRet = NetOpenInterface();
  if (iRet)
   {
    fprintf (stderr, "Network startup failed, code: %d\n", iRet);
   }
#endif

  if (iClientMode == FALSE)
   {
/*
	Open up a socket for use as a server
*/

    iFDSocket = NetMakeWelcome(&iSocket);
    if (iFDSocket < 0)
     {
      fprintf (stderr, "Unable to open the socket server\n");
      return 1;
     }

    printf("Socket created at %d\n", iSocket);

    for(;;)
     {
      ClientStruct cl;

      printf("Waiting for another connection\n");
      NetNewConnection(iFDSocket, &cl);
      printf("Connection from %s\n", GetClientHname(&cl));

#ifdef TTT
      if(fork() == 0)
       {			/* child */
        char buffer[100];
        int cnt;

        for(;;)
         {
          cnt = recv(GetClientFd(&cl), buffer, 1,0);
          if (cnt > 0)
           {
            printf("%d bytes from client %s\n",cnt, GetClientHname(&cl));
  	              // Send back an acknowlegement
            send(GetClientFd(&cl),"got it.  Thank you.\n",20, 0);
           }
         }
        NetCloseConnection(&cl);	/* not reached */
       }
#endif
     }

    NetReleaseContact (iFDSocket);
   }
  else
   {
    iFDSocket = NetMakeContact(caNetName, iSocket);
    if(iFDSocket==-1) 
     {
      fprintf(stderr, "Network error: %s\n", NetErrStr()); 
      exit(-1);
     }

#ifdef TTT
    if (fork() == 0)
     {
     /* the child */

      for (;;)
       {
        if (recv(iFDSocket,bufferIN,1, 0) != 1)
         {
          printf ("TTT Unable to read\n");
         }
        else
         {
          printf ("%c", bufferIN[0]);
         }
       }
     }

    for (;;)
     {
      scanf ("%s", bufferOUT);
      if (send(iFDSocket, bufferOUT, strlen(bufferOUT), 0)!=strlen(bufferOUT))
       {
        printf ("TTT Unable to write\n");
       }
     }
#else
    for (;;)
     {
     }
#endif

    NetReleaseContact (iFDSocket);
   }
}
