/*
   File Name: SwitchUser.cpp
   Created March 27 by John Mertus

   This allows an process with sticky bit set to switch back and
   forth between users.  See header for details

*/

#include "machine.h"
#include "SwitchUser.h"

#include "IniFile.h"
#include <errno.h>
#include <sched.h>
#include <sys/types.h>
#include <pwd.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <HRTimer.h>
//
// Storage for the static switcher
  static uid_t uidmti=-1;
  static gid_t gidmti=-1;
  static uid_t uidroot=-1;
  static gid_t gidroot=-1;
  static uid_t uiduser=-1;
  static gid_t giduser=-1;
  static bool bCanBeRoot;


CSwitchUser theSwitchUser;

//-------------------SetupPIDs--------------------John Mertus----Mar 2002---*/

  bool CSwitchUser::SetupPIDs(void)

//   This is never run by the user, but called by the SwitchToXXX functions
// Its responsibility is to load 
//
//
//****************************************************************************
{
  // Find the mti id and group
  if (uidmti == -1)
    {
      // Get MTI id
      struct passwd *pw = getpwnam("mti");
      if (pw == NULL)
	{
	  TRACE_0(errout << "getpwnam failed for mti " << strerror(errno));
	  return(false);
	}
      uidmti = pw->pw_uid;
      gidmti = pw->pw_gid;

      // Get root id, this is always 0
      pw = getpwnam("root");
      if (pw == NULL)
	{
	  TRACE_0(errout << "getpwnam failed for root " << strerror(errno));
	  return(false);
	}
      uidroot = pw->pw_uid;
      gidroot = pw->pw_gid;
      
      // get the user id
      uiduser = getuid();
      giduser = getgid();

      // See if we are running as root or mti
      bCanBeRoot = (geteuid() == uidroot);
    }

  return(true);
}

//-----------------------ToMTI---------------------John Mertus----Mar 2002----

  bool CSwitchUser::ToMti(void)

//   This trys to switch the user to MTI
//   return is true if user is not MTI
//   return is false if user has not changed
//
//
//****************************************************************************
{
  // If already set, do nothing
  if (!SetupPIDs()) return(false);

  if (geteuid() == uidmti) return(true);
  ToRoot();

  // We must switch groups first
  if (setregid(getegid(),gidmti) != 0)
    {
      TRACE_0(errout << "Could not switch to group MTI " << strerror(errno));
      return(false);
    }


  if (setreuid(geteuid(),uidmti) != 0)
    {
      TRACE_0(errout << "Could not switch to user MTI " << strerror(errno));
      return(false);
    }

  return(true);
}

//---------------------ToUser---------------------John Mertus----Mar 2002----

  bool CSwitchUser::ToUser(void)

//   This trys to switch the user to user
//   return is true if user is not user
//   return is false if user has not changed
//
//
//****************************************************************************
{
  if (!SetupPIDs()) return(false);

  // If already set, do nothing
  if (geteuid() == uiduser) return(true);
  ToRoot();

  // We must switch groups first
  if (setregid(getegid(),giduser) != 0)
    {
      TRACE_0(errout << "Could not switch to group user " << strerror(errno));
      return(false);
    }


  if (setreuid(geteuid(),uiduser) != 0)
    {
      TRACE_0(errout << "Could not switch to user user " << strerror(errno));
      return(false);
    }

  return(true);
}

//-----------------------ToRoot-------------------John Mertus----Mar 2002----

  bool CSwitchUser::ToRoot(void)

//   This trys to switch the user to root
//   return is true if user is not root
//   return is false if user has not changed
//
//
//****************************************************************************
{
  // If already set, do nothing
  if (geteuid() == uidroot) return(true);
  if (!SetupPIDs()) return(false);
  if (setregid(getegid(),gidroot) != 0)
    {
      TRACE_0(errout << "Could not switch to group root " << strerror(errno));
      return(false);
    }

  if (setreuid(geteuid(),uidroot) != 0)
    {
      TRACE_0(errout << "Could not switch to user root" << strerror(errno));
      return(false);
    }

  return(true);
}


//----------------SetHighPriority-----------------John Mertus----Feb 2002---*/

  void SetHighPriority(void)

//   This sets the calling process, as well as the resulting threads to
// a real time priority.
//
//****************************************************************************
{
  // WARNING, this may be NON portable, TBD use switch
  char s[5][5] = {"UNK","FIFO","RR","TS","NP"};
  struct sched_param param;

  // This is a suck cpu priority,  for SGI is 54
  param.sched_priority = 54; // (NDPNORMMIN+NDPNORMMAX)/2-29;
  TRACE_1(errout << "Setting Priority to " << param.sched_priority 
	  << " " << s[SCHED_FIFO]);

  int iRet = sched_setscheduler(0, SCHED_FIFO, &param);
  if (iRet < 0) perror("SetHighPriority");
  //
  // See what it really is set to
  iRet = sched_getparam(0, &param);
  TRACE_1(errout << "Priority set to " << param.sched_priority 
	  << " " << s[sched_getscheduler(0)]);
}

//----------------SetRealTimePriority-------------John Mertus----Feb 2002---*/

  void SetRealTimePriority(void)

{
  SetHighPriority ();
}

//
// This is set if the program succeeds in locking itself
// Used by IsCurrentProcessRunning and KillOtherProcess
static bool bLocked = false;

//----------------IsProcessRunning----------------John Mertus----Apr 2002-----

  bool IsCurrentProcessRunning(char *name)

//  This is used to find if the current process is running 
//  name is the name of the process; e.g., EditController
//  Note this works by file locking so require cooperation of the
//  other programs by using this function at startup.
//
//****************************************************************************
{
  int lock_fd;
  string s;

  // If we are locked, return
  if (bLocked) return(false);

  // Try to open the file
  s = (string)"/tmp/" + name + ".pid";
  lock_fd = open(s.c_str(), O_WRONLY|O_CREAT, S_IRUSR|S_IWUSR|
                  S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);	/* rw-rw-rw */
  if (lock_fd < 0)
    {
      // This should never happen
      TRACE_0(errout << "Cannot open temporary file " << s);
      throw logic_error((string)"Cannot open temporary file " + s);
      return(false);
    }
  // Test lock on file
  if (lockf(lock_fd, F_TLOCK, 0))
    {
      if((errno == EACCES) || (errno == EAGAIN))
	{
	  close(lock_fd);
	  return(true);
	}
      else
	{
	  TRACE_0(errout << "Request to check lock failed");
	  throw runtime_error(string("Call failed to fcntl"));
	}
    }

  // File not locked, lock it
  char buff[16];

  // Erase the file and write the PID to it
  ftruncate(lock_fd, 0);
  snprintf(buff, 15, "%d\n", getpid());
  buff[15] = 0;
  write(lock_fd, buff, strlen(buff));

  // Write the lock
  if (lockf(lock_fd, F_LOCK, 0) < 0)
    {
      TRACE_0(errout << "Lock file failed to lock");
      throw runtime_error(string("call failed to fcntl"));
    }

  bLocked = true;
  return(false);
}

//----------------KillOtherProcess----------------John Mertus----Apr 2002-----

  bool KillOtherProcess(char *name)

//  This kills any other process with name name. 
//  Return is true if program is killed or no other progam exists.
//
//  Note:  The other process name must have been created with a call
//  to IsCurrentPrcessRunning 
//
//****************************************************************************
{
  FILE *fp;
  string s;

  //
  // If already locked return
  if (bLocked) return(true);

  // Try to open the file
  s = (string)"/tmp/" + name + ".pid";
  fp = fopen(s.c_str(), "r");
  if (fp == NULL)
    {
      // This could happen if no lock file exists
      TRACE_0(errout << "Cannot open temporary file " << s << endl
	      << "Because " << strerror(errno));
      return(false);
    }
  
  // Read the pid
  int pid;
  if (fscanf(fp, "%d", &pid) <= 0) 
    {
      // This could happen if lock file is empty
      TRACE_0(errout << "Cannot read temporary file " << s << endl
	      << "Because " << strerror(errno));
      fclose(fp);
      return(false);
    }
      
  // Done with file
  fclose(fp);

  // NOTE: pid cannot be current pid
  TRACE_1(errout << "Killing " << hex << pid);
  int iErr = kill(pid, SIGKILL); 

  // not found pid is not an error
  if ((iErr != ESRCH) && (iErr != 0))
    {
      TRACE_1(errout << "KillotherProcess failed " << strerror(iErr));
      return(false);  // could not kill
    }

  // Delay a short time for the process to really die
  SleepMS(250);

  // Now try to lock the process again
  return !IsCurrentProcessRunning(name);
}
