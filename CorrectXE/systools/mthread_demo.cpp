/*
 *  Sample program for exercising the thread code
 * 
 */

#include <stdio.h>
#include "mthread.h"
#include <unistd.h>
#include <math.h>
#include <sys/time.h>

typedef struct
 {
  int
    iNData,
    iNJob;
  float
    *fpData;

  float
    faResult[30];
 } APPLICATION_STRUCT;

void PrimaryFunction (void *vp, int iJob)
/*
	vp is the APPLICATION data
*/
{
  int i, iDataStart, iDataStop;
  APPLICATION_STRUCT *asp;
  float *fpData;

  asp = (APPLICATION_STRUCT *) vp;
  iDataStart = (float)iJob / (float)asp->iNJob * asp->iNData;
  iDataStop = (float)(iJob+1) / (float)asp->iNJob * asp->iNData;

  fpData = asp->fpData;

  asp->faResult[iJob] = 0.;
  for (i = iDataStart; i < iDataStop; i++)
   {
    asp->faResult[iJob] += atan ((double)fpData[i]);
   }

  return;
}  /* PrimaryFunction */

void SecondaryFunction (void *vp)
/*
	vp is the APPLICATION data
*/
{
  int iJob;
  float fResult;
  APPLICATION_STRUCT *asp;

  asp = (APPLICATION_STRUCT *) vp;

  fResult = 0.;
  for (iJob = 0; iJob < asp->iNJob; iJob++)
   {
    fResult += asp->faResult[iJob];
   }

  printf ("All the work is now complete: %f\n", fResult);

  return;
}  /* SecondaryFunction */

void CallBackFunction (void *vp)
/*
	vp is the APPLICATION data
*/
{
  printf ("The callback function \n");

  return;
}  /* CallBackFunction */

int main(int argc, char **argv)
{
  int iRet, iData;
  THREAD_STRUCT tsThread;
  APPLICATION_STRUCT as;
  long lMicroSec;
  struct timeval tStart, tStop;

  tsThread.PrimaryFunction = &PrimaryFunction;
  tsThread.SecondaryFunction = &SecondaryFunction;
  tsThread.CallBackFunction = NULL;
  tsThread.vpApplicationData = NULL;
  tsThread.iNThread = 4;
  tsThread.iNJob = 10;

  as.iNData = 100000;
  as.iNJob = tsThread.iNJob;
  as.fpData = (float *) malloc (as.iNData * sizeof(float));
  for (iData = 0; iData < as.iNData; iData++)
   {
    as.fpData[iData] = (float)(iData % 1000);
   }
  tsThread.vpApplicationData = &as;

tsThread.iRetVal = 0;

  printf ("Calling AllocThread()\n");
  iRet = AllocThread(&tsThread);
  if (iRet)
   {
    printf ("AllocThread() returned %d\n", iRet);
    exit (1);
   }

  while (1)
   {
    printf ("Calling StartThread()\n");
    gettimeofday (&tStart);
    iRet = StartThread (&tsThread);
    if (iRet)
     {
      printf ("StartThread() returned %d\n", iRet);
      exit (1);
     }
    gettimeofday (&tStop);

    lMicroSec = (tStop.tv_sec - tStart.tv_sec)*1000000 + 
		(tStop.tv_usec - tStart.tv_usec);
    printf ("Elapsed time: %f millisec", (float)lMicroSec / 1000.);
   }

  printf ("Calling FreeThread()\n");
  iRet = FreeThread (&tsThread);
  if (iRet)
   {
    printf ("FreeThread() returned %d\n", iRet);
    exit (1);
   }

  free (as.fpData);

  printf ("Exiting normally \n");

  exit (1);
}  /* main */
