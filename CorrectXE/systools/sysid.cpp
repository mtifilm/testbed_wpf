#include "machine.h"

#ifndef WIN32
#include <stdio.h>
#include <net/if.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <unistd.h>
#endif /* not WIN32 */

#ifdef __sgi
/* can't find the header that has this */
#ifdef __cplusplus
extern "C"
{
#endif
unsigned sysid (unsigned char id[16]);
#ifdef __cplusplus
}
#endif
#endif /* __sgi */

#ifdef WIN32
#include "IPTypes.h"
// Stolen from <ifcons.h>:
#define MIB_IF_TYPE_OTHER               1
#define MIB_IF_TYPE_ETHERNET            6
#define MIB_IF_TYPE_TOKENRING           9
#define MIB_IF_TYPE_FDDI                15
#define MIB_IF_TYPE_PPP                 23
#define MIB_IF_TYPE_LOOPBACK            24
#define MIB_IF_TYPE_SLIP                28

extern "C"
// Stolen from iphlpapi.h
DWORD WINAPI GetAdaptersInfo(
		PIP_ADAPTER_INFO pAdapterInfo, PULONG pOutBufLen
		);

#define MAX_MAC_ADDRS 6
#define MAC_ADDR_SIZE 18  // strlen("XX-XX-XX-XX-XX-XX"+1)
#endif /* WIN32 */


int main (int iArgC, char **cpArgV)
{
#ifdef WIN32
// I have no idea how Borland does unsigned-long-long constants!
// So this is dynamically initialized, below.
    MTI_UINT64 ullSysID;
#else
    MTI_UINT64 ullSysID = ~(0ULL);
#endif /* WIN32 */

#ifdef __linux
    unsigned char *pSysID;
    struct ifreq ifr;
    
    int sd = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
    if (sd < 0) {
        /* socket failed */
      fprintf (stderr, "Unable to open the socket\n");
      exit (1);
    }

    pSysID = NULL;

    if (pSysID == NULL)
     {
      strncpy(ifr.ifr_name, "eth0", IFNAMSIZ);
      if (ioctl(sd, SIOCGIFHWADDR, &ifr) < 0) {
        /* ioctl success */
       pSysID = (unsigned char *) ifr.ifr_hwaddr.sa_data;
      }
    }

    if (pSysID == NULL)
     {
      strncpy(ifr.ifr_name, "nb0", IFNAMSIZ);
      if (ioctl(sd, SIOCGIFHWADDR, &ifr) < 0) {
        /* ioctl success */
       pSysID = (unsigned char *) ifr.ifr_hwaddr.sa_data;
      }
    }


    if (pSysID == NULL)
     {
      fprintf (stderr, "No address found.  Try /sbin/ifconfig\n");
      exit (1);
     }

    ullSysID = (((MTI_UINT64) pSysID[0]) << 40) +
        (((MTI_UINT64) pSysID[1]) << 32) +
        (((MTI_UINT64) pSysID[2]) << 24) +
        (((MTI_UINT64) pSysID[3]) << 16) +
        (((MTI_UINT64) pSysID[4]) << 8) +
        ((MTI_UINT64) pSysID[5]); 

  close (sd);

    fprintf (stderr, "%lld\n", ullSysID);

#elif defined(__sgi)

    /* just call SGI's sysid() */

    ullSysID = (MTI_UINT64) sysid(NULL);
    fprintf (stderr, "%lld\n", ullSysID);

#elif defined(WIN32)

    PIP_ADAPTER_INFO pAdaptersInfo = NULL, pAdapter = NULL;
    DWORD err = 0, adaptersInfoSize = 0;

    // I have no idea how Borland does unsigned-long-long constants!
    ullSysID = (((MTI_UINT64) 0xFF) << 40) +
        (((MTI_UINT64) 0xFF) << 32) +
        (((MTI_UINT64) 0xFF) << 24) +
        (((MTI_UINT64) 0xFF) << 16) +
        (((MTI_UINT64) 0xFF) << 8) +
         ((MTI_UINT64) 0xFF);

    err = GetAdaptersInfo(NULL, &adaptersInfoSize);
    if (err != ERROR_BUFFER_OVERFLOW) {
        return ullSysID;
    }
    pAdaptersInfo = (PIP_ADAPTER_INFO) GlobalAlloc(GPTR, adaptersInfoSize);
    if (pAdaptersInfo == NULL) {
        return ullSysID;
    }
    err = GetAdaptersInfo(pAdaptersInfo, &adaptersInfoSize);
    if (!err) {
	for (pAdapter = pAdaptersInfo; pAdapter; pAdapter = pAdapter->Next) {
	    /* Try to make sure it's a hardware adapter */
	    switch (pAdapter->Type) {
	        case MIB_IF_TYPE_ETHERNET:
	        case MIB_IF_TYPE_FDDI:
		    break;
                default:
                    continue;
	    }
	    /* Screen out oddball lengths and AOL addresses */
	    if (pAdapter->AddressLength != 6 ||
                (pAdapter->Address[0] == 0x00 &&
                 pAdapter->Address[1] == 0x03 &&
		 pAdapter->Address[2] == 0x8A)) {
	        continue;
            }
           ullSysID = (((MTI_UINT64) pAdapter->Address[0]) << 40) +
                      (((MTI_UINT64) pAdapter->Address[1]) << 32) +
                      (((MTI_UINT64) pAdapter->Address[2]) << 24) +
                      (((MTI_UINT64) pAdapter->Address[3]) << 16) +
                      (((MTI_UINT64) pAdapter->Address[4]) << 8) +
                       ((MTI_UINT64) pAdapter->Address[5]);
           /***
            printf("MAC ADDRESS IS %02.2X-%02.2X-%02.2X-%02.2X-%02.2X-%02.2X\n",
                     pAdapter->Address[0], pAdapter->Address[1],
                     pAdapter->Address[2], pAdapter->Address[3],
                     pAdapter->Address[4], pAdapter->Address[5]);
           ***/
            break;
        }
    }
    GlobalFree(pAdaptersInfo);

    fprintf (stderr, "%Ld\n", ullSysID);    // Borland

#endif /* __linux */

  exit (0);
}
