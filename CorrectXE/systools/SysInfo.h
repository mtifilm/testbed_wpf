#ifndef SYSINFOH
#define SYSINFOH

#include "systoolsDLL.h"

// Functions to get information regarding the system we are running on.
// Implemented so far:
//    AvailableProcessorCount()
//            Count number of logical processors usable by the callers process
//            (i.e. are present in hardware, enabled in BIOS and is included
//            in the process's "affinity" set.
//    AvailableProcessorCoreCount()
//             Count number of physical processor cores, if we can figure
//             it out (eliminates hyperthread logical preocessors), else
//             returns number of logical processors
//    GetUserMaxAddress()
//             The address of the last byte addressable in the program's
//             virtual address space (e.g. 4GB-1 for 64-bit OS)
//    GetPhysicalMemorySizeInGB()
//             The amount in GB of physical RAM installed in this system
//    SetPrivilege()
//             Tries to set a privilege for the user, providing the
//             privilege is settable for the current user as per the
//             system's "local policy"
//    GetSystemPageSize()
//             Gets the system page size, duh.

namespace SysInfo
{
  // get the max address available to a user process
  MTI_SYSTOOLSDLL_API MTI_UINT32 GetUserMaxAddress();

  // get size in GB of physical RAM
  MTI_SYSTOOLSDLL_API MTI_UINT32 GetPhysicalMemorySizeInGB();

  // get current working set size
  MTI_SYSTOOLSDLL_API size_t GetWorkingSetSize();

  // get number of LOGICAL PROCESSORS (including hyperthreads)
  MTI_SYSTOOLSDLL_API int AvailableProcessorCount();

  // get number of PHYSICAL PROCESSOR cores (i.e. dual core = 2)
  MTI_SYSTOOLSDLL_API int AvailableProcessorCoreCount();

  // set user privilege
  MTI_SYSTOOLSDLL_API BOOL SetPrivilege(
                    HANDLE hToken,          // access token handle
                 	LPCTSTR lpszPrivilege,  // name of privilege
	                BOOL bEnablePrivilege); // to enable or disable privilege
                    
  // use this one if you don't already have an access token
  MTI_SYSTOOLSDLL_API BOOL SetPrivilege(
                    LPCTSTR lpszPrivilege,
                    BOOL bEnablePrivilege);

  // get the system page size
  MTI_SYSTOOLSDLL_API size_t GetSystemPageSize();

  // get the error message corresponding to a particular system error code
  MTI_SYSTOOLSDLL_API string GetSystemErrorMessage(int errorCode);

  MTI_SYSTOOLSDLL_API const std::string opDescription(const MTI_UINT64 opcode);

  MTI_SYSTOOLSDLL_API const std::string seDescription( const unsigned int& code);

};

#endif // __SYSINFO_H__
