/*
	File:  Encrypt.h
	By:    Kevin Manbeck
	Date:  Sept 25, 2002

	This contains functions for encrypting sensitive data.
*/
#ifndef EncryptH
#define EncryptH
#include "systoolsDLL.h"

#define SYSTEM_ID_SUCCESS          0
#define SYSTEM_ID_GENERAL_FAILURE -1
#define SYSTEM_ID_NO_ADAPTERS     -2

MTI_SYSTOOLSDLL_API MTI_UINT64 UsageEncrypt (MTI_UINT64 ullValue);
MTI_SYSTOOLSDLL_API int WriteSystemID ();
MTI_SYSTOOLSDLL_API int ReadSystemID ();
MTI_SYSTOOLSDLL_API int ReadDonglePort ();
MTI_SYSTOOLSDLL_API int WriteDonglePort (int iDonglePort);

#endif
