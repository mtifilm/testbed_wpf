// ---------------------------------------------------------------------------

#pragma hdrstop

#include "MultiSliceProcess.h"
#include "HRTimer.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)

namespace
{
    struct ConvertParams
    {
        Ipp16u *src; // Pointer to first source element of interest
        int srcStep; // Source row pitch in bytes
        Ipp32f *dst; // Pointer to first destination element
        int dstStep; // Destination row pitch in bytes
        IppiSize size; // Witdh and height of pixels to be converted
        Ipp16u maxValue; // The maximum source value: must be (power of 2) - 1
        int numberOfJobs;
        // The total number of jobs for segmentation calculation
    };

    void RunConversionJob(void *vp, int jobNumber, ImageWork sliceProcess,
        void *args, ...)
    {
        if (vp == nullptr)
        {
            return;
        }

        // Image segmented by blocks of rows.
        auto params = static_cast<ConvertParams*>(vp);
        SliceParams sliceParams;



        // Evenly distribute n extra rows over the first n jobs.
        auto minRowsPerJob = params->size.height / params->numberOfJobs;
        auto extraRows = params->size.height % params->numberOfJobs;
        auto jobY = (minRowsPerJob * jobNumber) + std::min<int>(extraRows,
            jobNumber);

        sliceParams.srcRoi.data =
            params->src + (jobY * (params->srcStep));
        sliceParams.srcRoi.roiSize = {params->size.width, minRowsPerJob + ((jobNumber < extraRows) ? 1 : 0)};
        sliceParams.srcRoi.step = params->srcStep;
        sliceParams.srcRoi.maxValue = params->maxValue;

        sliceParams.dstRoi.data =
            params->dst + (jobY * (params->dstStep));
        sliceParams.dstRoi.roiSize = sliceParams.srcRoi.roiSize;
        sliceParams.dstRoi.step = params->dstStep;
        sliceParams.dstRoi.maxValue = params->maxValue;

        // Do this job!
        CHRTimer timer;
        sliceProcess(sliceParams, args);
    }

} // Anonymous namespace

void MultiSliceProcess(const SliceParams fullImage, ImageWork sliceProcess, void *args, ...)
{
//   ConvertParams params;
//   params.maxValue = _maxValue;
//   params.size.width = _roiSize.width;
//   params.size.height = _roiSize.height;
//
//   params.src = imageData + (((_roiOffset.y * _imagePitchInPixels) + _roiOffset.x) * NUM_COMPONENTS);
//   params.srcStep = _imagePitchInPixels * NUM_COMPONENTS * sizeof(Ipp16u);
//
//   Ipp32fArray result(_roiSize.height, _roiSize.width, NUM_COMPONENTS);
//   params.dst = result.GetPointerToElement(0, 0, 0);
//   params.dstStep = result._rowPitchInBytes;
//
//   SynchronousMthreadRunner mthreads(&params, RunConversionJob);
//   params.numberOfJobs = mthreads.GetNumberOfThreads();
//
//   CHRTimer timer;
//   mthreads.Run();
//
//   return result;
}
