#ifndef MT_MathH
#define MT_MathH

#include "systoolsDLL.h"
#include "ran266.h"
#include "iran266.h"


MTI_SYSTOOLSDLL_API int InvertMatrix (float **fpSrc, float **fpDst, int iDim);
MTI_SYSTOOLSDLL_API float Degrees2Sine(float degrees);
MTI_SYSTOOLSDLL_API float Degrees2Cosine(float degrees);


#endif
