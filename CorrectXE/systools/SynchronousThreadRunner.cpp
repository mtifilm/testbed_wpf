//---------------------------------------------------------------------------

#pragma hdrstop

#include "SynchronousThreadRunner.h"

#include "bthread.h"
#include "IniFile.h"
#include "SysInfo.h"

#pragma package(smart_init)
//---------------------------------------------------------------------------

////#define NO_MULTITHREADING

static CSpinLock StaticLock;  // Need this to serialize thread termination!

//---------------------------------------------------------------------------

SynchronousThreadRunner::SynchronousThreadRunner(int nJobs, void *param, ThreadWorkFunctionType workFunction)
: SynchronousThreadRunner(nJobs, std::min<int>(nJobs, SysInfo::AvailableProcessorCount()), param, workFunction)
{
}

//---------------------------------------------------------------------------

SynchronousThreadRunner::SynchronousThreadRunner(int nJobs, int nThreads, void *param, ThreadWorkFunctionType workFunction)
:  _numberOfThreadsToUse(nThreads)
,  _totalNumberOfJobs(nJobs)
,	_workFunction(workFunction)
,	_workFunctionParam(param)
{
}

//---------------------------------------------------------------------------

SynchronousThreadRunner::SynchronousThreadRunner(void *param, ThreadWorkFunctionType workFunction)
: SynchronousThreadRunner(SysInfo::AvailableProcessorCount(), param, workFunction)
{
}
//---------------------------------------------------------------------------

SynchronousThreadRunner::~SynchronousThreadRunner()
{
	while (!_allWorkIsComplete)
	{
		MTImillisleep(1);
	}
}
//---------------------------------------------------------------------------

int SynchronousThreadRunner::GetNumberOfThreads()
{
	return _numberOfThreadsToUse;
}

//---------------------------------------------------------------------------

int SynchronousThreadRunner::Run()
{
	_allJobsCompleteEvent.reset();
	_numberOfJobsRun = 0;
	_firstError = 0;
	_allWorkIsComplete = false;

#ifdef NO_MULTITHREADING
	_numberOfThreadsStillRunning = 1;
	RunWorkerThread();

#else

	_numberOfThreadsStillRunning = _numberOfThreadsToUse;
	for (int threadNumber = 0; threadNumber < _numberOfThreadsToUse; ++threadNumber)
	{
		BThreadSpawn(&BounceToWorkerThread, this);
	}

	_allJobsCompleteEvent.wait();

#endif

	_allWorkIsComplete = true;

	return _firstError;
}
//---------------------------------------------------------------------------

/* static trampoline */
void SynchronousThreadRunner::BounceToWorkerThread(void *vp, void *vpReserved)
{
	SynchronousThreadRunner *runner = (SynchronousThreadRunner *) vp;
	BThreadBegin(vpReserved);
	if (runner == nullptr)
	{
		return;
	}

	runner->RunWorkerThread();

	// Do NOT try to access runner past this point -- it may have been deleted!
}
//---------------------------------------------------------------------------

void SynchronousThreadRunner::RunWorkerThread()
{
   {
      CAutoSpinLocker autolock(_jobLock);
      while (_numberOfJobsRun < _totalNumberOfJobs)
      {
         int myJobNumber = _numberOfJobsRun++;
         int retVal;

         try
         {
            CAutoSpinUnlocker autounlock(_jobLock);
            retVal = _workFunction(_workFunctionParam, myJobNumber, _totalNumberOfJobs);
         }
         catch (const std::exception &ex)
         {
            TRACE_0(errout << "***ERROR**** a SyncThread has caught std::exception: " << ex.what());
            retVal = -10010;
         }
         catch (...)
         {
            TRACE_0(errout << "***ERROR**** a SyncThread has thrown an unknown error");
            retVal = -10011;
         }

         if (retVal && !_firstError)
         {
            _firstError = retVal;
         }
      }
   }

   // This has to be protected with a non-instance-variable lock, so we
   // just use a static one!
   {
      CAutoSpinLocker autolock(StaticLock);

      if (--_numberOfThreadsStillRunning == 0)
      {
         _allJobsCompleteEvent.signal();

         //////////////////// CAREFUL! ///////////////////////////////////
         // DO NOT TRY TO ACCESS ANY INSTANCE VARIABLES AFTER THIS POINT!!
         //////////////////// CAREFUL! ///////////////////////////////////
      }
   }
}
//---------------------------------------------------------------------------

IppiRect SynchronousThreadRunner::findSliceRoi(int jobNumber, int totalJobs, const IppiSize &size)
{
	IppiRect jobRoi;

	jobRoi.width = size.width;

	// Evenly distribute n extra rows over the first n jobs.
	auto minRowsPerJob = size.height / totalJobs;
	auto extraRows = size.height % totalJobs;
	jobRoi.y = (minRowsPerJob * jobNumber) + std::min<int>(extraRows, jobNumber);
	jobRoi.x = 0;
	jobRoi.height = minRowsPerJob + ((jobNumber < extraRows) ? 1 : 0);

	return jobRoi;
}

//---------------------------------------------------------------------------

