#ifndef SOCKET_H
#define SOCKET_H

#include "systoolsDLL.h"
#include "machine.h"

// This defines some names so the window/unix calls are the same.
#ifdef _WINDOWS
//
//  Borland VCL turns off socket support (it provides its own)
//  you must define VCL_FULL in the Project Options-Directories/Conditionals
//  to define SOCKET
#include <windows.h>
#else
//
// Unix
typedef int SOCKET;
#define closesocket close          // Windows uses closesocket
#define ioctlsocket ioctl          // same for control
#include <sys/types.h>
#include <sys/socket.h>
#endif

// Use this socket to create a temporary socket
#define TEMPORARY_SOCKET_PORT (0)

typedef enum {E_NET_ERRNO=-1, E_NET_OK=0} NetErrnoType;

typedef struct
{
	int fd;
	char hostname[200];
} ClientStruct;

typedef ClientStruct *ClientPtr;

MTI_SYSTOOLSDLL_API void InitClient(ClientPtr cl);
MTI_SYSTOOLSDLL_API void ResetClient(ClientPtr cl);
MTI_SYSTOOLSDLL_API void SetClientFd(ClientPtr cl, SOCKET fd);
MTI_SYSTOOLSDLL_API SOCKET GetClientFd(ClientPtr cl);
MTI_SYSTOOLSDLL_API void CloseClient(ClientPtr cl);
MTI_SYSTOOLSDLL_API char *GetClientHname(ClientPtr cl);
MTI_SYSTOOLSDLL_API int GetClientMaxHlen(ClientPtr cl);
MTI_SYSTOOLSDLL_API void HandleNetError();

MTI_SYSTOOLSDLL_API const char *NetErrStr();
MTI_SYSTOOLSDLL_API NetErrnoType NetErrNo();

MTI_SYSTOOLSDLL_API SOCKET NetMakeWelcome(int *port);
MTI_SYSTOOLSDLL_API int NetClientDataAvail(ClientPtr c);

MTI_SYSTOOLSDLL_API int NetNewConnection(SOCKET w, ClientPtr newc);
MTI_SYSTOOLSDLL_API int NetReleaseContact(SOCKET w);
MTI_SYSTOOLSDLL_API SOCKET NetMakeContact(const char *hostname, int port);
MTI_SYSTOOLSDLL_API char *NetGetClientHostname(ClientPtr c, char *hname, int maxhostlen);
MTI_SYSTOOLSDLL_API void NetCloseConnection(ClientPtr c);
//
//  For windows we need an open/close
MTI_SYSTOOLSDLL_API int NetOpenInterface(void);
MTI_SYSTOOLSDLL_API int NetCloseInterface(void);

#if !defined(__linux)
// Linux uses socklen_t for the length of sockaddr structs whereas
// others still use int.
//   7/22/03 - mpr - added a layer of abstraction (i.e. new typedef of
//                   sockaddr_len) because SGI (as of IRIX 6.5.18) now
//                   defines socklen_t, but doesn't use it in the same
//                   places as linux.  Hopefully this fix will keep
//                   everyone (pre-6.5.18, 6.5.18+, linux & windows)
//                   happy.
//
typedef int sockaddr_len;
#else
typedef socklen_t sockaddr_len;
#endif

#endif /* SOCKET_H */


