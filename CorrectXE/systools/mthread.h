#ifndef MThreadH
#define MThreadH

/*
		IMPORTANT!  IMPORTANT!

	After calling MThreadAlloc(), it is important that the THREAD_STRUCT
	in the calling program is not destroyed until the threads are 
	finished.

	This is the callers responsibility to ensure that the THREAD_STRUCT
	is not destroyed.

	April 1, 2001
	This code was modified to incorporate the bthread (blocking thread)
	code and to implement multiple (parallel) threads under WIN32.
*/

#include "systoolsDLL.h"
#include "bthread.h"	// the blocking thread header

#ifdef MAX_THREAD
#undef MAX_THREAD
#endif
#define MAX_THREAD 128
#define MAX_JOB (4*1024)   // increased from 2*1024 for 4K
#define NULL_JOB (-1)

typedef struct
 {
/* - - - - - - - - General purpose inputs - - - - - - - - - - - - - - -*/
  int
    iNThread,		/* zero to skip use of threads */
    iNJob;

  void
    (*PrimaryFunction)(void *, int iJob),	/* called once for each job */
    (*SecondaryFunction)(void *),	/* called by manager when all jobs
					have finished */
    (*CallBackFunction)(void *);	/* set to NULL to force return on
					completion */

  void
    *vpApplicationData;			/* use this to customize thead struct */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

/* - - - - - - - - Reserved for internal use - - - - - - - - - - - - - */
  void
    *vpSemCaller;		// used to suspend and release caller

  void
    *vpThreadManager,		// BThread handle of manager thread
    *vpaThreadWorker[MAX_THREAD];// BThread handle of worker threads

  void
    *vpCriticalManager;		// CriticalSection handle for manager

  int
    iThread,
    iaJobList[MAX_THREAD],
    iManagerBusy,
    iActiveJobs,
    iRetVal;

  int
    iStopRequested,
    iThreadManagerStopped,
    iaThreadWorkerStopped[MAX_THREAD];
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

 } MTHREAD_STRUCT;


		// Functions called by the application
#define MThreadAlloc(tsp) MThreadAllocEx(tsp, __FILE__, __LINE__)
MTI_SYSTOOLSDLL_API int MThreadAllocEx (MTHREAD_STRUCT *tsp, const char * callerFilePath, int callerLineNumber);
MTI_SYSTOOLSDLL_API int MThreadFree (MTHREAD_STRUCT *tsp);
MTI_SYSTOOLSDLL_API int MThreadStart (MTHREAD_STRUCT *tsp);

		// Internal functions called only by the library
MTI_SYSTOOLSDLL_API int FinishThread (MTHREAD_STRUCT *tsp);
MTI_SYSTOOLSDLL_API void ThreadManager (void *vp, void *vpReserved);
MTI_SYSTOOLSDLL_API void ThreadWorker (void *vp, void *vpReserved);

//==================================================================
// Welcome to the wonderful world of C++
// This is an implementation of the most basic usage of mthreads.
// Its efficiency relies on the fact that bthreads are now pooled.
typedef void (*MThreadPrimaryFunctionType)(void *, int iJob);
typedef void (*MThreadSecondaryFunctionType)(void *);

MTI_SYSTOOLSDLL_API class SynchronousMthreadRunner
{
public:
   SynchronousMthreadRunner(
      int nJobs,
      void *parameter,
      MThreadPrimaryFunctionType primaryFunctionPtr,
      MThreadSecondaryFunctionType secondaryFunctionPtr = nullptr
      );
   SynchronousMthreadRunner(
      void *parameter,
      MThreadPrimaryFunctionType primaryFunctionPtr,
      MThreadSecondaryFunctionType secondaryFunctionPtr = nullptr
      );
   ~SynchronousMthreadRunner();

   int GetNumberOfThreads();
   int Run();

private:
   MTHREAD_STRUCT myMthreadStruct;
};

#endif
