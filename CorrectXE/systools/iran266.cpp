//---------------------------------------------------------------------------

#pragma hdrstop

#include "iran266.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)


// integer version returns numbers between
// 0 and RND_MODULUS-1, which can be compared
// to suitable integer thresholds to create
// grain & other effects
//

#define RND_MODULUS 67108864

#if 0

unsigned long iran266(long *lseed)

{

	*lseed = 25*(*lseed);
	*lseed = *lseed % RND_MODULUS;
	*lseed = 25*(*lseed);
	*lseed = *lseed % RND_MODULUS;
	*lseed = 5*(*lseed);
	*lseed = *lseed % RND_MODULUS;

	return (*lseed);
}

#else

unsigned long iran266(long *lseed)
{
#if USE_INTEL_ASM_SYNTAX

	long fct = *lseed;

	_asm {

		mov ebx,25
		mov ecx,RND_MODULUS

		mov eax,fct
		mul ebx
		div ecx

		bswap edx

		mov eax,edx
		mul ebx
		div ecx

		mov fct,edx
	}

	*lseed = fct;

#else // ATT syntax

	__asm__
	(
	"	movl    $25,%%ebx;       "
	"  movl    $67108864,%%ecx; "
	"                           "
	"  movl    (%0),%%eax;      "
	"  mull    %%ebx;           "
	"  divl    %%ecx;           "
	"                           "
	"  bswapl  %%edx;           "
	"                           "
	"  movl    %%edx,%%eax;     "
	"  mull    %%ebx;           "
	"  divl    %%ecx;           "
	"                           "
	"  movl    %%edx,(%0);      "
	: // No outputs
	: "r"(lseed)  // input (--> register referred to as %0)
	: "eax", "ebx", "ecx", "edx");  // Register clobber list

#endif

	return *lseed;
}

#endif

/*
   // this code can be used to test iran266
   // by building a histogram and determin-
   // ing the stdDev of the bins
#define NUM_TRIALS 100000
#define NUM_BINS 100
#define AVG_BIN (NUM_TRIALS / NUM_BINS)

   int bins[NUM_BINS];

   for (int i=0; i<NUM_BINS; i++)
      bins[i] = 0;

   for (int i=0;i<NUM_TRIALS;i++) {

      int k= iran266(&lseed)*NUM_BINS/RND_MODULUS;

      bins[k]++;
   }

   // examine the histogram under debug
   //
   float variance = 0;

   for (int i=0; i<NUM_BINS; i++) {

      float temp = (float)(bins[i] - AVG_BIN);

      variance += temp*temp;
   }

   float stdev = sqrt(variance/(float)NUM_BINS);

   float percentStdDev = 100.*stddev / (float)AVG_BIN;
*/

