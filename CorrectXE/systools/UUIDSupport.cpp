// ---------------------------------------------------------------------------

#include "UUIDSupport.h"
#include "MTImalloc.h"

std::string UuidSupport::UuidToStdString(const UUID &uuid)
{
   WCHAR* wszUuid = nullptr;
   UuidToStringW(&uuid, (RPC_WSTR*)&wszUuid);
   if (wszUuid != nullptr)
   {
      // We know the size of a UUID string is 36, don't know if need padding
      char s0[40];
      WideCharToMultiByte(CP_UTF8, 0, wszUuid, -1, s0, 40, NULL, NULL);
      std::ostringstream ss;
      ss << s0;
      auto result = ss.str();

      // free up the allocated string
      RpcStringFreeW((RPC_WSTR*)&wszUuid);
      auto n = result.size();
      return result;
   }

   return "";
}

UUID UuidSupport::StdStringToUuid(const string &uuidString)
{
   UUID result;
   if (UuidFromString((unsigned char *)(LPCTSTR)uuidString.c_str(), &result) == RPC_S_OK)
   {
      return result;
   }

   return NilUuid;
}

UUID UuidSupport::GuidToUuid(const guid_t &guid)
{
   // Inefficient if we are doing a lot
   UUID uuid;
   MTImemcpy(&uuid, &guid, sizeof(UUID));

   return uuid;
}

// ---------------------------------------------------------------------------

UUID UuidSupport::UuidBySeed(const UUID originalUuid, int seed)
{
   // Inefficient if we are doing a lot
   UUID uuid = originalUuid;
   auto u = (int *)&uuid;
   *u = *u ^ seed;
   return uuid;
}

// ---------------------------------------------------------------------------

UUID UuidSupport::CreateUuid()
 {
    UUID result;
    ::UuidCreate(&result);
    return result;
 }

// ---------------------------------------------------------------------------

string UuidSupport::CreateUuidString()
{
    return UuidToStdString(CreateUuid());
}

// ---------------------------------------------------------------------------


