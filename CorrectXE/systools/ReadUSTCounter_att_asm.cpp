#include <windows.h>
#include "ReadUSTCounter.h"

// mlm: to-do: what does this pragma do?  It was introduced in a
// Borland environment most likely.
#pragma option -5

void ReadUSTCounter(LARGE_INTEGER &li)

//  Just read the UST counter in the pentium.
//  li returns the count as a LARGE_INTEGER
//
//****************************************************************************
{
#if MTI_ASM_X86_ATT
   unsigned long lp;
   long hp;
        
   // GCC uses AT&T syntax assembly; see
   // http://www.ibiblio.org/gferg/ldp/GCC-Inline-Assembly-HOWTO.html#s2

   // This is a translation of the code in ReadUSTCounter.cpp
   // that uses Intel syntax.  This could potentially be a lot
   // simpler; see
   // http://www.technovelty.org/code/c/reading-rdtsc.html.

   // cpuid forces CPU to complete preceding instructions before
   // rdtsc reads the Time Stamp Counter.  I don't know if this
   // is really required or just programmers' superstition
   //  Note by JAM
   //    The cpuid is necessary if trying to count the actual
   //    number of cpu cycles, but in our case, our time periods
   //    are large enough for out of sequence execution to
   //    matter.
   //
   //  However, the cpuid creates a problem when the borland (and
   //  Visual C++) compilers force optimization.  This is because
   //  cpuid destroys the ebx and ecx registers and the optimizer
   //  does not know about this.  A compiler bug


   // mlm: to-do: get this reviewed for accuracy
   __asm__ __volatile__ ("pushl %%ebx\n\t"
                         "pushl %%ecx\n\t"

                         "xorl %%eax, %%eax\n\t"
                         "xorl %%edx, %%edx\n\t"
                         "cpuid\n\t"
                         "rdtsc\n\t"
                         "movl %%edx, %0\n\t"
                         "movl %%eax, %1\n\t"

                         "popl %%ecx\n\t"
                         "popl %%ebx"
                         : "=r"(hp), "=r"(lp)
                         : /* no inputs */
                         : /* clobber list? */
      );
   li.HighPart = hp;
   li.LowPart = lp;
#else
#warning "function not implemented for this platform"
#endif
}
