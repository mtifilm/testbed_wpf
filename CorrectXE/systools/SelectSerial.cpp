/**********************************************************************/
/*                                                                    */
/*  SelectSerial - standalone program used to switch Smart Switch.    */
/*                                                                    */
/*     11/07/02 - Michael Russell - Creation                          */
/*      2/24/03 - mpr - clean up for first SGI release                */
/*                                                                    */
/*  Notes: SGI only                                                   */
/*                                                                    */
/**********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "SmartSwitch.h"

void main (int iArgC, char **cpArgV)
{
  int  i, j;
  char c;
  int  bQuitF = 0;
  char caPort[64];

  if (iArgC != 3)
  {
    printf("Usage: %s system_device switch_port\n", cpArgV[0]);
    printf("       where:\n");
    printf("         system_device = e.g. '/dev/ttyd1'\n");
    printf("         system_device = e.g. '1' '/dev/ttyd' is prepended)\n");
    printf("         switch_port   = A..D or 1..4 or 'dance'\n");
    printf("       e.g. %s /dev/ttyd1 A\n", cpArgV[0]);
    printf("         or %s /dev/ttyd1 dance\n", cpArgV[0]);
    exit(-1);
  }

  strcpy(caPort, "");		// Initialize

  if (cpArgV[1][0] != '/')	// short name, so make /dev/ttyd...
    sprintf(caPort, "/dev/ttyd%s", cpArgV[1]);
  else
    strcpy(caPort, cpArgV[1]);

  if (strcmp(cpArgV[2], "dance") == 0)	// Cycle thru all ports a few times
  {
    for (i = 0; i < 4 && (!bQuitF); i++)
    {
      for (j = 0; j < 4 && (!bQuitF); j++)
      {
        c = 'A' + j;
        if (SetSmartSwitch(caPort, c))
          printf("Port %c\n", c);
        else
        {
          printf("Failure switching SmartSwitch\n");
          bQuitF = 1;
        }
      }
    }
  }
  else
  {
    if (isdigit(cpArgV[2][0]))		// Number 1-4
    {
       if (SetSmartSwitchNumeric(caPort, atoi(cpArgV[2])))
        printf("Smart Switch appears to have switched successfully\n");
      else
        printf("Failure switching SmartSwitch\n");
    }
    else				// Char A-D
    {
      if (islower(cpArgV[2][0]))	// lowercasify
        cpArgV[2][0] = cpArgV[2][0] - ('a' - 'A');

      if (SetSmartSwitch(caPort, cpArgV[2][0]))
        printf("Smart Switch appears to have switched successfully\n");
      else
        printf("Failure switching SmartSwitch\n");
    }
  }
}
