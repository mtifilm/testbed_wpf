/*
 This file contains all functions required to perform parallel
 processing.

 Written by:	Kevin Manbeck
 Date:		Jan 5, 1998
 */

// #define PRINT_OUT

#include <stdio.h>
#include "mthread.h"
#include "cpmp_err.h"
#include "HRTimer.h"
#include "IniFile.h"
#include "MTIsleep.h"
#include "SysInfo.h"

int MThreadAllocEx(MTHREAD_STRUCT *msp, const char * callerFilePath, int callerLineNumber)
{
   int iCnt, iRet = RET_SUCCESS;

   msp->vpSemCaller = NULL;
   msp->vpCriticalManager = NULL;
   msp->vpThreadManager = NULL;
   for (iCnt = 0; iCnt < MAX_THREAD; iCnt++)
   {
      msp->vpaThreadWorker[iCnt] = NULL;
   }

   if (msp->iNThread > MAX_THREAD || msp->iNThread < 0)
   {
      msp->iRetVal = ERR_THREAD_BAD_PARAMETER;
      return msp->iRetVal;
   }

   if (msp->iNThread)
   {
      msp->vpThreadManager = BThreadSpawn(&ThreadManager, msp);
      if (msp->vpThreadManager == NULL)
      {
         iRet = ERR_THREAD_CREATE;
      }

      for (iCnt = 0; iCnt < msp->iNThread; iCnt++)
      {
			msp->iThread = iCnt;
			////TRACE_0(errout << "Spawn worker thread " << iCnt);
			msp->vpaThreadWorker[iCnt] = BThreadSpawnEx(&ThreadWorker, msp, callerFilePath, callerLineNumber);
         if (msp->vpaThreadWorker[iCnt] == NULL)
         {
            iRet = ERR_THREAD_CREATE;
			}
			////TRACE_0(errout << "Back from spawning " << iCnt);
			msp->iaJobList[iCnt] = NULL_JOB;
      }
   }

   msp->iManagerBusy = FALSE;

   msp->vpCriticalManager = CriticalSectionAlloc();
   if (msp->vpCriticalManager == NULL)
   {
      iRet = RET_FAILURE;
   }

   /*
    Set up a semaphore for the caller, so we can suspend and release
    the caller as necessary.
    */

   msp->vpSemCaller = SemAlloc();
   if (msp->vpSemCaller == NULL)
   {
      msp->iRetVal = RET_FAILURE;
   }

   msp->iRetVal = iRet;

   msp->iStopRequested = 0;
   msp->iThreadManagerStopped = 0;
   for (iCnt = 0; iCnt < msp->iNThread; iCnt++)
   {
      msp->iaThreadWorkerStopped[iCnt] = 0;
   }

   return msp->iRetVal;
} /* MThreadAlloc */

int MThreadFree(MTHREAD_STRUCT *msp)
{
	int iCnt, iRet = RET_SUCCESS, iStillRunning;

	/*
    Instruct the threads to stop and wait for this to occur
    */

   if (msp->iNThread)
   {
		msp->iStopRequested = 1;

		////TRACE_0(errout << "Tell all threads to die");

      // issue resume instructions to all threads

      if (BThreadResume(msp->vpThreadManager))
      {
         msp->iRetVal = ERR_THREAD_UNBLOCK;
      }

      for (iCnt = 0; iCnt < msp->iNThread; iCnt++)
      {
         if (BThreadResume(msp->vpaThreadWorker[iCnt]))
         {
            msp->iRetVal = ERR_THREAD_UNBLOCK;
         }
      }

		iCnt = 0;

		////TRACE_0(errout << "Tell all threads to die");
		CHRTimer stopwatch;

      do
      {
         iStillRunning = 0;

         if (msp->iThreadManagerStopped)
         {
            msp->vpThreadManager = NULL;
         }
         else
         {
            iStillRunning++;
         }

         for (int i = 0; i < msp->iNThread; i++)
         {
            if (msp->iaThreadWorkerStopped[i])
            {
               msp->vpaThreadWorker[i] = NULL;
            }
            else
            {
               iStillRunning++;
            }
         }

			if (iStillRunning)
			{
				MTImillisleep(1);
			}
      }
		while (iStillRunning && stopwatch.Read() < 1000);

      // if any threads are still alive, kill them.
      if (iStillRunning)
      {
			////TRACE_0(errout << iStillRunning << " mthreads did not die. now killing them");
			for (iCnt = 0; iCnt < msp->iNThread; iCnt++)
         {
            if (msp->vpaThreadWorker[iCnt])
            {
					if (BThreadKill(msp->vpaThreadWorker[iCnt]))
               {
                  iRet = ERR_THREAD_DESTROY;
					}

               msp->vpaThreadWorker[iCnt] = NULL;
            }
         }

         if (msp->vpThreadManager)
         {
            if (BThreadKill(msp->vpThreadManager))
            {
               iRet = ERR_THREAD_DESTROY;
				}

            msp->vpThreadManager = NULL;
         }
      }
   }

   iRet |= CriticalSectionFree(msp->vpCriticalManager);

   iRet |= SemFree(msp->vpSemCaller);

   msp->iRetVal = iRet;

   return msp->iRetVal;
} /* MThreadFree */

int MThreadStart(MTHREAD_STRUCT *msp)
   /*
    This function is called from the main application.
    It fires off the thread manager and returns.
    */ {

   if (CriticalSectionEnter(msp->vpCriticalManager))
   {
      msp->iRetVal = RET_FAILURE;
      return msp->iRetVal;
   }

   /*
    Is the manager already busy doing other work?
    */

   if (msp->iManagerBusy)
   {
      msp->iRetVal = ERR_THREAD_BUSY;
   }

   msp->iManagerBusy = TRUE;

   if (CriticalSectionExit(msp->vpCriticalManager))
   {
      msp->iRetVal = RET_FAILURE;
      return msp->iRetVal;
   }

   if (msp->iNJob > MAX_JOB || msp->iNJob <= 0)
   {
      msp->iRetVal = ERR_THREAD_BAD_PARAMETER;
      return msp->iRetVal;
   }

   if (msp->iNThread)
   {
      /*
       Send a signal to wake up the manager function
       */
		////TRACE_0(errout << "Issuing RESUME to ThreadManager");

      if (BThreadResume(msp->vpThreadManager))
      {
         msp->iRetVal = ERR_THREAD_UNBLOCK;
      }

      if (msp->CallBackFunction == NULL)
      {
         /*
          Stay here until all threads are complete
          */
         SemSuspend(msp->vpSemCaller);
      }
   }
   else /* call thread functions directly */
   {
      ThreadManager((void *)msp, NULL);
   }

   return (FinishThread(msp));
} /* MThreadStart */

int FinishThread(MTHREAD_STRUCT *msp)
{
   int iRet;

   msp->iManagerBusy = FALSE;
   iRet = msp->iRetVal;

   msp->iRetVal = RET_SUCCESS;

   return iRet;
} /* FinishThread */

void ThreadManager(void *vp, void *vpReserved)
   /*
    This function runs as a sproc.  It blocks itself and waits
    for a signal.
    */ {
   int iJob, iThread, iSuspend;
   MTHREAD_STRUCT *msp;

   msp = (MTHREAD_STRUCT*)vp;

   if (vpReserved)
   {
      // Release the parent thread
      if (BThreadBegin(vpReserved))
      {
         msp->iRetVal = ERR_THREAD_UNBLOCK;
      }
   }

	////TRACE_0(errout << "TheadManager started");

   if (msp->iNThread)
   {
      while (TRUE)
      {
			////TRACE_0(errout << "Blocking ThreadManager");
			BThreadSuspend(vpReserved);

			////TRACE_0(errout << "ThreadManager awake");

         if (msp->iStopRequested)
         {
            msp->iThreadManagerStopped = 1;

            // Returning from sproc will kill this thread.
            return;
			}

         iJob = 0;
         iSuspend = 0;
         while (iJob < msp->iNJob)
         {
				iThread = 0;
            while (iThread < msp->iNThread && iJob < msp->iNJob)
            {
               if (msp->iaJobList[iThread] == NULL_JOB)
               {
                  msp->iaJobList[iThread] = iJob;
                  iJob++;

                  /*
                   Send a signal to wake up the worker function
                   */
						////TRACE_0(errout << "ThreadManager tells thread " << iThread << " to run job " << iJob);

                  msp->iActiveJobs++;
                  if (BThreadResume(msp->vpaThreadWorker[iThread]))
                  {
                     msp->iRetVal = ERR_THREAD_UNBLOCK;
                  }
               }
               iThread++;
            }

            if (msp->iNJob > iJob)
            {
               // Wait for one job to finish
					////TRACE_0(errout << "Suspending (single) the manager for iSuspend: " << iSuspend << " out of " << msp->iNJob);
					iSuspend++;
               BThreadSuspend(msp->vpThreadManager);
            }
            else
            {
               // Wait for rest of jobs to finish
               while (iSuspend < msp->iNJob)
               {
						////TRACE_0(errout << "Suspending the manager for iSuspend: " << iSuspend << " out of " << msp->iNJob);
						iSuspend++;
                  BThreadSuspend(msp->vpThreadManager);
               }
            }
				////TRACE_0(errout << "ThreadManager awake");

				if (msp->iStopRequested)
            {
               // Returning from sproc will kill this thread.
               return;
				}
         }

         /*
          All jobs are finished.  Call the clean up function
          */

         if (msp->SecondaryFunction)
         {
            msp->SecondaryFunction(msp->vpApplicationData);
         }

			if (msp->CallBackFunction == NULL)
         {
            /*
             Wake up the caller
             */

            if (SemResume(msp->vpSemCaller))
            {
               msp->iRetVal = ERR_THREAD_UNBLOCK;
            }
         }
         else
         {
            msp->CallBackFunction(msp->vpApplicationData);
         }
      }
   }
	else /* no threads, call directly */
   {
      for (iJob = 0; iJob < msp->iNJob; iJob++)
      {
         msp->iThread = 0;
         msp->iaJobList[msp->iThread] = iJob;
         ThreadWorker((void *)msp, NULL);
      }

      if (msp->SecondaryFunction)
         msp->SecondaryFunction(msp->vpApplicationData);
   }

   return;
} /* ThreadManager */

void ThreadWorker(void *vp, void *vpReserved)
   /*
    This function runs as a sproc.  It blocks itself and waits
    for a signal.
    */ {
   int iThread;
   MTHREAD_STRUCT *msp;

   msp = (MTHREAD_STRUCT*)vp;
	iThread = msp->iThread;

	////TRACE_0(errout << "ThreadWorker " << iThread << " started");

   if (msp->iNThread)
   {
      if (BThreadBegin(vpReserved))
      {
         msp->iRetVal = ERR_THREAD_UNBLOCK;
      }

      while (TRUE)
      {
			////TRACE_0(errout << "Blocking ThreadWorker " << iThread);
			BThreadSuspend(vpReserved);

			////TRACE_0(errout << "ThreadWorker " << iThread << " was told to run job " << msp->iaJobList[iThread]);

         if (msp->iStopRequested)
         {
            msp->iaThreadWorkerStopped[iThread] = 1;

            // Returning from sproc will kill this thread.
				return;
			}

			msp->PrimaryFunction(msp->vpApplicationData, msp->iaJobList[iThread]);

         /*
          Send a signal to wake up the manager function
          */
	////TRACE_0(errout << "ThreadWorker " << iThread << " completed job " << msp->iaJobList[iThread]);

         msp->iaJobList[iThread] = NULL_JOB;

         if (BThreadResume(msp->vpThreadManager))
         {
            msp->iRetVal = ERR_THREAD_UNBLOCK;
         }
      }
   }
   else
   {
      msp->PrimaryFunction(msp->vpApplicationData, msp->iaJobList[iThread]);
   }

   return;
} /* ThreadWorker */

#define NO_MULTITHREADING 0

SynchronousMthreadRunner::SynchronousMthreadRunner(
      int nJobs,
      void *parameter,
      MThreadPrimaryFunctionType primaryFunctionPtr,
      MThreadSecondaryFunctionType secondaryFunctionPtr
      )
{
   myMthreadStruct.PrimaryFunction = primaryFunctionPtr;
   myMthreadStruct.SecondaryFunction = secondaryFunctionPtr;
   myMthreadStruct.CallBackFunction = nullptr;
   myMthreadStruct.vpApplicationData = parameter;
#if NO_MULTITHREADING
   myMthreadStruct.iNThread = 0;
#else
   myMthreadStruct.iNThread = SysInfo::AvailableProcessorCount();
#endif
   myMthreadStruct.iNJob = nJobs;

   int retVal = MThreadAlloc(&myMthreadStruct);
   MTIassert(retVal == 0);
}

SynchronousMthreadRunner::SynchronousMthreadRunner(
      void *parameter,
      MThreadPrimaryFunctionType primaryFunctionPtr,
      MThreadSecondaryFunctionType secondaryFunctionPtr
      )
: SynchronousMthreadRunner(
#if NO_MULTITHREADING
      1,
#else
      SysInfo::AvailableProcessorCount(),
#endif
      parameter,
      primaryFunctionPtr,
      secondaryFunctionPtr)
{
}

SynchronousMthreadRunner::~SynchronousMthreadRunner()
{
   int retVal = MThreadFree(&myMthreadStruct);
   MTIassert(retVal == 0);
}

int SynchronousMthreadRunner::GetNumberOfThreads()
{
   return myMthreadStruct.iNThread > 0 ? myMthreadStruct.iNThread : 1;
}

int SynchronousMthreadRunner::Run()
{

   int retVal = MThreadStart(&myMthreadStruct);
   MTIassert(retVal == 0);
   if (retVal != 0)
   {
      ////TRACE_0(errout << retVal);
      return retVal;
   }

   return 0;
}

