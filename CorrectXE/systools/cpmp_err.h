
#ifndef ERRORS_H
#define ERRORS_H

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif


/*
	Various return values
*/

#define RET_SUCCESS 0
#define RET_FAILURE (-1)

#define ERR_REPAIR_TYPE (-2)
#define ERR_ALLOC (-3)		/* unable to allocate */
#define ERR_WIDTH (-4)
#define ERR_DIVIDE_BY_ZERO (-5)
#define ERR_NO_BEST_VALUE (-6)
#define ERR_MEMORY (-7)		/* uninitialized memory */
#define ERR_FILE_OPEN (-8)	/* unable to open a file */
#define ERR_FILE_READ (-9)	/* unable to read from a file */
#define ERR_FILE_WRITE (-11)	/* unable to write to a file */
#define ERR_FILE_SEEK (-12)	/* unable to seek in a file */
#define ERR_BOX_TOO_NARROW (-13)
#define ERR_VERSION_NUMBER (-14) /* unsupported version number */
#define ERR_FILE_IO (-15)
#define ERR_REALLOC (-16)
#define ERR_CRITICAL_SECTION_ALLOC (-17)
#define ERR_TEMP_MEM_NOT_INITIALIZED (-18)

#define ERR_GRAMP_SIZE (-100)
#define ERR_GRAMP_PRECISION (-101)
#define ERR_GRAMP_STATUS (-102)
#define ERR_GRAMP_READ (-103)

#define ERR_THREAD_CREATE (-200)
#define ERR_THREAD_DESTROY (-201)
#define ERR_THREAD_BUSY (-202)
#define ERR_THREAD_SIGNAL (-203)
#define ERR_THREAD_PID (-204)
#define ERR_THREAD_BLOCK (-205)
#define ERR_THREAD_UNBLOCK (-206)
#define ERR_THREAD_SIGNAL_SETUP (-207)
#define ERR_THREAD_BAD_PARAMETER (-208)

#define ERR_CADENCE_VIDEO_SIGNAL_DETECTED (-300)
#define ERR_CADENCE_BAD_PT (-301)
#define ERR_CADENCE_OBS_PARA (-302)
#define ERR_CADENCE_RENDER_TOO_FAR_AWAY (-303)
#define ERR_CADENCE_BAD_ACTION (-304)
#define ERR_CADENCE_BAD_REQUEST (-305)

#define ERR_HDIO_FAILURE (-401)	/* general failure */
#define ERR_HDIO_OPEN (-402)	/* trouble opening external device */
#define ERR_HDIO_UNKNOWN_DEVICE (-403) /* bad value of iDeviceType */
#define ERR_HDIO_UNKNOWN_TYPE (-404) /* bad value of iImageType */
#define ERR_HDIO_IO (-405)		/* I/O failure */
#define ERR_HDIO_ALLOC (-406)	/* unable to allocate storage */
#define ERR_HDIO_BUFFER (-407)	/* invalid ucpField0 or ucpField1 */

#define ERR_VARI_MIN_RATE (-501) /* iMinDropRate too small */
#define ERR_VARI_REGULARITY (-502) /* fRegularity too small */
#define ERR_VARI_DROP_FIELD (-503) /* fRegularity too small */

#define ERR_AP_BAD_PARAMETER (-601)	/* bad parameter in Auto Process */
#define ERR_AP_MUST_REALLOC (-602)	/* need to AllocAP() */
#define ERR_AP_BAD_PROCESS_TYPE (-603)	/* unsupported ProcessType */


#define ERR_DNGL_FAILURE (-701)
#define ERR_DNGL_BAD_COMMAND (-702)
#define ERR_DNGL_MISSING (-703)
#define ERR_DNGL_BAD_STRING (-704)
#define ERR_DNGL_USED_UP (-705)
#define ERR_DNGL_MISMATCH (-706)
#define ERR_DNGL_WEAK (-707)
#define ERR_DNGL_NEGATIVE_TIME (-708)
#define ERR_DNGL_INVALID (-709)
#define ERR_DNGL_EXPIRE (-710)
#define ERR_DNGL_FILE_READ (-711)

#define ERR_DNGL_TIME_WARNING (-720)
#define ERR_DNGL_TIME_FAILURE (-721)
#define ERR_DNGL_TIME_TIME_SENSE (-722)
#define ERR_DNGL_TIME_TIME_BACKWARDS (-723)
#define ERR_DNGL_TIME_INCONSISTENCY (-724)
#define ERR_DNGL_TIME_NO_TIME (-725)
#define ERR_DNGL_TIME_OPEN_INI_FILE (-726)
#define ERR_DNGL_TIME_WRITE_CHECK (-727)
#define ERR_DNGL_TIME_SAVE_INI_FILE (-728)

#define ERR_DNGL_SMARTSWITCH_LOCALMACHINE_INI  (-730)
#define ERR_DNGL_SMARTSWITCH_FAILURE_SWITCHING (-731)



#endif
