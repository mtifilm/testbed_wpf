
/*
   This defines an interface for producing and read/writing guids.

   GUIDs are Universal Unique Identifiers that are bit strings that may be generated
   independently on separate nodes (hosts) such that globally unique strings
   result without requiring the hosts to be in communication with each other
   to ensure uniqueness.  They are a component of DCE that have been
   independently reimplemented here, but is in near accordance with the DCE
   specification.  This implementation is NOT API compatible with the DCE
   implementation.

   -John Mertus
*/

#ifndef _GUID_MTI_H
#define _GUID_MTI_H

#include "machine.h"
#include "systoolsDLL.h"


// This is a 128 bit GUID
typedef struct _guid_t {
   MTI_UINT32          time_low;
   MTI_UINT16          time_mid;
   MTI_UINT16          time_hi_and_version;
   MTI_UINT8           clock_seq_hi_and_reserved;
   MTI_UINT8           clock_seq_low;
   unsigned char       node[6];
} guid_t;

static_assert (sizeof(_guid_t) == 16, "Size of _guid_t is incorrect");

// guid_create this generate a UUID
MTI_SYSTOOLSDLL_API guid_t guid_create(void);

// This convers a guid to a 36 character string
MTI_SYSTOOLSDLL_API string guid_to_string(const guid_t &u);

// This converts a 36 character string to a guid
MTI_SYSTOOLSDLL_API bool guid_from_string(guid_t &u, const string sguid);

// guid_create_from_name -- create a UUID using a "name"
// from a "name space"
MTI_SYSTOOLSDLL_API guid_t guid_create_from_name( const guid_t &nsid, const string name);

// guid_compare --  Compare two UUID's "lexically" and return
//       -1   u1 is lexically before u2
//        0   u1 is equal to u2
//        1   u1 is lexically after u2
//  Note:   lexical ordering is not temporal ordering!
//
MTI_SYSTOOLSDLL_API int guid_compare(const guid_t &u1, const guid_t u2);

#endif
