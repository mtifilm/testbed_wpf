#include "TSerial.h"

#ifdef __sgi
#include <sys/time.h>  // for stamp_t

TSstatus mtiTSRead (TSport port, MTI_UINT8 *data, MTI_INT64 *stamps,
		int nbytes)
{
  return tsRead (port, data, (stamp_t *)stamps, nbytes);
}  /* mtiTSRead */

TSstatus mtiTSWrite (TSport port, MTI_UINT8 *data, MTI_INT64 *stamps,
		int nbytes)
{
  return tsWrite (port, data, (stamp_t *)stamps, nbytes);
}  /* mtiTSWrite */

#else

/*
 * W32TSerial - timestamped, non-STREAMS, low-overhead serial port
 */

#include "W32Serial.h"
#include "ThreadLocker.h"
#include "MTImalloc.h"
#include <memory.h>		// memcpy
#include <io.h>			// open,close,read,write
#include <string>
#include <list>


struct _TSconfig {
	string		portName;
	int			direction;
	int			queueSize;
	tcflag_t	cflag;
	speed_t		ospeed;
	int			protocol;

	_TSconfig()		
		: portName	("")
		, direction	(TS_DIRECTION_TRANSMIT)
		, queueSize	(256)
		, cflag		(CS8)	// 8 bits + 1 stop bit + no parity
		, ospeed	(9600)
		, protocol	(TS_PROTOCOL_RS232)
	{};
};

struct _TSport {
	TSconfig	config;
	WSerial		*ws_port;

	_TSport(TSconfig in_config = (TSconfig) 0)
		: config	((TSconfig) 0)
		, ws_port	((WSerial*) 0)
	{
		if (in_config != (TSconfig) 0)
			config = new _TSconfig(*in_config);
	};

	~_TSport() {
		delete ws_port;
		delete config;
	};
};

struct _WSerialListEntry {
	string	portName;
	WSerial *ws_port;
	TSport	transmitter;
	TSport	receiver;

	_WSerialListEntry(
			string	 in_portName = "",
			TSport	 in_transmitter = (TSport) 0,
			TSport	 in_receiver = (TSport) 0,
			WSerial* in_ws_port = (WSerial*) 0
			)
		: portName		(in_portName)
		, ws_port		(in_ws_port)
		, transmitter	(in_transmitter)
		, receiver		(in_receiver)
	{};

	bool operator== (_WSerialListEntry& _rhs) {
		return (this->portName == _rhs.portName);
	};

	bool operator!= (_WSerialListEntry& _rhs) {
		return (this->portName != _rhs.portName);
	};
};

struct WSerialListEntry
{
	_WSerialListEntry* item;

	WSerialListEntry(_WSerialListEntry* in_item)
		:item (in_item)
	{};

	bool operator== (WSerialListEntry& _rhs) {
		return (this->item->portName == (_rhs.item)->portName);
	};

	bool operator!= (WSerialListEntry& _rhs) {
		return (this->item->portName != (_rhs.item)->portName);
	};
	bool operator== (WSerialListEntry* _rhs) {
		if (this->item == (_WSerialListEntry*) 0
			|| _rhs->item == (_WSerialListEntry*) 0)
		{
			return false;
		}
		return (this->item->portName == _rhs->item->portName);
	};

	bool operator!= (WSerialListEntry* _rhs) {
		if (this->item == (_WSerialListEntry*) 0
			|| _rhs->item == (_WSerialListEntry*) 0)
		{
			return true;
		}
		return (this->item->portName != _rhs->item->portName);
	};
	
};

list<WSerialListEntry> WSerialList;
static CThreadLock WSerialListLock;

typedef int TSstatus;
static TSstatus openWSerialPort(TSport ts_port);
static TSstatus closeWSerialPort(TSport ts_port);

/*
 * make a new TSconfig.  you must set certain parameters before using
 * this config with tsOpenPort.  see tsOpenPort.
 * see above for code example.
 *
 * can fail with TS_ERROR_OUT_OF_MEM
 */
TSconfig tsNewConfig(void)
{
	return new _TSconfig;
}

/*
 * make a new TSconfig which has all the parameters of a currently
 * open TSport.
 *
 * can fail with TS_ERROR_OUT_OF_MEM
 */
TSconfig tsNewConfigFromPort(TSport port)
{
	return new _TSconfig(*port->config);
}

/*
 * copy one TSconfig to another.  You'll have to free the returned
 * TSconfig separately.
 *
 * can fail with TS_ERROR_OUT_OF_MEM
 */
TSconfig tsCopyConfig(TSconfig from)
{
	return new _TSconfig(*from);
}

/*
 * free a TSconfig.
 */
TSstatus tsFreeConfig(TSconfig config)
{
	delete config;
	return TS_SUCCESS;
}


/* set UNIX filename of timestamped serial port to open:
 *
 *   - this should be one of the nodes /dev/ttyts*
 *   - /dev/ttytsN refers to the same physical port as /dev/tty[fd]N
 *   - opening the STREAMS device for a given serial port
 *     (/dev/tty[fd]N) and opening a TSport to that port are
 *     mutually exclusive.  tsOpenPort will return
 *     TS_ERROR_PORT_BUSY if the port is already in use as a
 *     STREAMS device.
 *   - assuming the STREAMS device for a given port is not
 *     open, then you can tsOpenPort() that port as many times
 *     as you want for input, as long as the communications parameters
 *     match those used in the first open.  tserialio will allow only one
 *     open for writing at a time.  Again, unsuccessful
 *     attempts in all these cases will return TS_ERROR_PORT_BUSY
 *
 * can fail with TS_ERROR_OUT_OF_MEM
 */
TSstatus tsSetPortName(TSconfig config, char *name)
{
	config->portName = name;
	return TS_SUCCESS;
}


/* 
 * specify tx/rx direction of timestamped serial port
 *
 * can fail with TS_ERROR_BAD_LIBRARY_CALL if token bad
 */
TSstatus tsSetDirection(TSconfig config, int direction)
{
	TSstatus retval = TS_SUCCESS;

	switch (direction) {

		case TS_DIRECTION_TRANSMIT:
		case TS_DIRECTION_RECEIVE :
			config->direction = direction;
			break;

		default:
			retval = TS_ERROR_BAD_LIBRARY_CALL;
			break;
	}
	return retval;
}


/* specify queue size of timestamped serial port
 *
 * when you create a TSport, tserialio allocates a queue.  this
 * parameter specifies the number of (byte,timestamp) pairs which
 * this queue can hold.
 *
 * for an input port, this queue holds the bytes which have been
 * received on the serial port but which you have not yet read.  
 * if you allow this queue to fill up to capacity, and then 
 * further characters arrive on the serial port, those new
 * characters will be discarded.  therefore, it is very important
 * to choose a queue capacity and a frequency of reading the
 * queue which will keep the queue from overflowing.
 *
 * for an output port, this queue holds the bytes which you have 
 * written but which have not yet been transmitted out the serial port.
 * if you attempt to write so much data that this queue would fill
 * past its capacity, then tsWrite() will block until enough
 * space has become available to write all of your data.
 *
 * can fail with TS_ERROR_BAD_LIBRARY_CALL if qsize <0
 */
TSstatus tsSetQueueSize(TSconfig config, int queuesize)
{
	TSstatus retval = TS_SUCCESS;

	if (queuesize < 0)
		retval = TS_ERROR_BAD_LIBRARY_CALL;
	else
		config->queueSize = queuesize;

	return retval;
}


/*
 * most communication parameters are specified here using
 *   traditional struct termios.c_cflag flags: 
 *     CSIZE bits (CS5, CS6, CS7, CS8)
 *     CSTOPB bit (1==2 stop bits, 0==1 stop bits)
 *     PARENB (0==no parity, 1==see PARODD)
 *     PARODD (1==odd parity, 0==even parity)
 *     CBAUD (B9600 etc.) is IGNORED
 *           this field of c_cflag has been obsoleted. use tsSetOspeed instead.
 */
TSstatus tsSetCflag(TSconfig config, tcflag_t cflag)
{
	config->cflag = cflag;
	return TS_SUCCESS;
}


/*
 * baud rate, specified as a straight integer in symbols per
 * second (examples: 9600, 31250 (MIDI), 38400 (video deck control)).
 * this parameter replaces the CBAUD bits of TS_CFLAG.  this parameter
 * exists because the termio flags omit several necessary baud rates.
 * baud rate of 0, meaning "hang up" or "disconnect," is not supported.
 *
 * can fail with TS_ERROR_BAD_LIBRARY_CALL if ospeed==0
 */
TSstatus tsSetOspeed(TSconfig config, speed_t ospeed)
{
	TSstatus retval = TS_SUCCESS;

	if (ospeed == 0)
		retval = TS_ERROR_BAD_LIBRARY_CALL;
	else
		config->ospeed = ospeed;

	return retval;
}


/*
 * electrical protocol to use on the port:
 *   TS_PROTOCOL_RS232 (the default)
 *   TS_PROTOCOL_RS422
 * for MIDI on Indigo, Indy, and Indigo2, choose TS_PROTOCOL_RS422
 * for video deck control, choose TS_PROTOCOL_RS422
 *
 * can fail with TS_ERROR_BAD_LIBRARY_CALL if token bad
 */
#define TS_PROTOCOL_RS232 0
#define TS_PROTOCOL_RS422 1
TSstatus tsSetProtocol(TSconfig config, int protocol)
{
	TSstatus retval = TS_SUCCESS;

	switch (protocol) {

		case TS_PROTOCOL_RS232:
		case TS_PROTOCOL_RS422:
			config->protocol = protocol;
			break;

		default:
			retval = TS_ERROR_BAD_LIBRARY_CALL;
			break;
	}
	return retval;
}


/*
 * normally, an asynchronous serial port runs off its own internal
 * clock.  in some situations, such as MIDI on Indigo, Indy, and
 * Indigo2, the port clocks off of an external signal present on
 * one of the pins of the serial port.  this parameter specifies
 * where the serial port should look for its clock source.
 *
 * 0 (the default) means the serial port should use its internal clock
 * N (N > 1) means the serial port should clock itself off
 * of the provided external clock divided by N.
 *
 * if this parameter is nonzero, then the CBAUD bits of the TS_CFLAG
 * parameter and the TS_SPEED parameter are ignored.
 *
 * for Indigo, Indy, and Indigo2, N can be 1, 16, 32, or 64.  
 *
 * Macintosh-compatible MIDI dongles produce a 1 MHz external
 * clock signal.  therefore, when you plug such dongles into an 
 * Indigo, Indy, or Indigo2, you want to choose a clock factor of 32 so
 * that you get the MIDI baud rate of (1,000,000/32==) 31.25kHz.
 *
 * for video deck control, choose 0.
 *
 * for O2 MIDI, choose 0, since you need to use an RS232->Mac dongle.
 *
 * can fail with TS_ERROR_BAD_LIBRARY_CALL if extclock_factor<0
 */
TSstatus tsSetExternalClockFactor(TSconfig config, int extclock_factor)
{
	// NOT SUPPORTED
	TSstatus retval = TS_ERROR_BAD_LIBRARY_CALL;

	if (extclock_factor == 0)
		retval = TS_SUCCESS;

	return retval;
}


/*
 * tsOpenPort: open a timestamped serial port
 *
 * each TSport represents a connection to one physical serial port
 * in one direction.  here are the parameters:
 *
 * --- flow control for RS-232 operation
 *
 * tserialio ignores the state of DCD and CTS; it assumes both signals
 * are always asserted, so data can always be received or transmitted.
 *
 * tserialio does not provide control over the DTR or RTS lines.
 * when a port is open under tserialio, these lines are always asserted
 * on the physical serial port.
 *
 * control may be added at a later time.  for now, the main applications
 * of tserialio (MIDI and deck control) are 422 applications, and do not 
 * require flow control.
 *
 * can fail with TS_ERROR_OUT_OF_MEM
 * can fail with TS_ERROR_BAD_LIBRARY_CALL if args invalid
 * can fail with TS_ERROR_BAD_LIBRARY_CALL if portname not specified in config
 * can fail with TS_ERROR_BAD_LIBRARY_CALL if direction not specified in config
 * can fail with TS_ERROR_BAD_LIBRARY_CALL if queuesize not specified in config
 * can fail with TS_ERROR_BAD_LIBRARY_CALL if cflag not specified in config
 * can fail with TS_ERROR_BAD_LIBRARY_CALL if ospeed not specified in config
 * can fail with TS_ERROR_OPENING_PORT if communication with driver fails
 * can fail with TS_ERROR_OPENING_PORT if you use feature not supported on hw
 */      
TSstatus tsOpenPort(TSconfig config, TSport *returnPort)
{
	TSstatus retval = TS_SUCCESS;
	TSport ts_port = new _TSport(config);
	
	retval = openWSerialPort(ts_port);

	if (retval == TS_SUCCESS) {
	
		if (ts_port == (TSport) 0 || ts_port->ws_port == (WSerial*) 0)
			retval = TS_ERROR_OUT_OF_MEM;
		else if (config == (TSconfig) 0 || returnPort == (TSport*) 0)
			retval = TS_ERROR_BAD_LIBRARY_CALL;
		else if (config->portName.empty())
			retval = TS_ERROR_BAD_LIBRARY_CALL;
		
		if (retval == TS_SUCCESS) {
			WSerial* ws_port = ts_port->ws_port;
			ws_port->Parity = 0; // ???????

			ws_port->PortName = config->portName;
			ws_port->Baud = config->ospeed;

			switch (config->cflag & CSIZE) {
				case CS5:
					ws_port->Bits = 5;
					break;
				case CS6:
					ws_port->Bits = 6;
					break;
				case CS7:
					ws_port->Bits = 7;
					break;
				case CS8:
					ws_port->Bits = 8;
					break;
			}
			if ((config->cflag & CSTOPB) == 0)
				ws_port->StopBits = ONESTOPBIT;
			else
				ws_port->StopBits = TWOSTOPBITS;

			if ((config->cflag & PARENB) == 0)
				ws_port->Parity = NOPARITY;
			else if ((config->cflag & PARODD) == 0)
				ws_port->Parity = EVENPARITY;
			else
				ws_port->Parity = ODDPARITY;

			if (!ws_port->Open(config->portName))
				retval = TS_ERROR_OPENING_PORT;
		}
	}
	if (retval != TS_SUCCESS) {
		delete ts_port;
		ts_port = (TSport) 0;
	}
	*returnPort = ts_port;

	return retval;
}

/* 
 * close TSport.
 */
TSstatus tsClosePort(TSport ts_port)
{
	TSstatus retval = TS_SUCCESS;

	retval = closeWSerialPort(ts_port);
	delete ts_port;

	return retval;
}


/* 
 * returns same quantity as TS_QUEUE_SIZE parameter to tsOpenPort--this
 * quantity is fixed for a given port and will not change over time.
 */
int tsGetQueueSize(TSport port)
{
	return port->config->queueSize;
}


/* 
 * returns the total number of (byte,timestamp) pairs currently in
 * the TSport's queue.
 */
int tsGetFilledBytes(TSport port)
{
	// NOT CORRECTLY IMPLEMENTED
	return (port == (TSport) 0)? 0
		: ((port->ws_port == (WSerial*) 0)? 0
			: port->ws_port->Empty()? 0 : 1);
}


/*
 * tsRead() reads nbytes (byte,timestamp) pairs from the specified port.
 * the function returns the data of each byte in data[i], and the UST
 * time at which the byte came in the input jack of the machine in
 * stamps[i].  the timestamp on each byte is accurate to plus or minus 
 * one millisecond.  if nbytes (byte,timestamp) pairs are not currently
 * available in the port's queue, then tsRead() will block until
 * it has been able to read all nbytes pairs.
 *
 * tsWrite writes nbytes (byte,timestamp) pairs to the specified port.
 * tserialio will then output data[i] at the UST time given by stamps[i],
 * with an accuracy of plus or minus one millisecond.  if sufficient
 * space is not available in the port's queue to write all nbytes
 * (byte,timestamp) pairs immediately, then tsWrite() will block
 * until it has been able to write all nbytes pairs.  the timestamps
 * you specify should be non-decreasing; the behavior is not defined
 * if the timestamps ever decrease.
 *
 * these UST timestamps can be used to measure or schedule serial i/o
 * relative to other signals on the system.  see dmGetUST(3dm), 
 * alGetFrameTime(3dm), and vlGetUSTMSCPair(3dm).
 *
 * see below for an important NOTE about exactly what accuracy and
 * latency guarantees tserialio offers.
 *
 * if tsRead() or tsWrite() need to block, they will call select().
 * if that select fails for any reason other than EINTR, the calls
 * will return with TS_ERROR_SELECT_FAILED.
 */
TSstatus mtiTSRead(TSport port, unsigned char *data, MTI_INT64 *stamps, 
                int nbytes)
{
	TSstatus retval =  TS_SUCCESS;
	int count = 0;
	WSerial* ws_port = port->ws_port;
	
	count = ws_port->ReadBuffer(data, nbytes, stamps);
	if (count != nbytes)
		retval = TS_ERROR_SELECT_FAILED;

	return retval;
}

TSstatus mtiTSWrite(TSport port, unsigned char *data, MTI_INT64 *stamps, 
                 int nbytes)
{
	// WRITING WITH TIME STAMPS IS NOT SUPPORTED
	// "stamps" must be NULL of must point at an array of 0's.
	TSstatus retval = TS_SUCCESS;

	if (stamps != (MTI_INT64*) 0)
	{
		int i;
		for (i = 0; i < nbytes; ++i)
			if (stamps[i] != (MTI_INT64) 0)
				retval = TS_ERROR_BAD_LIBRARY_CALL;
	}
	if (retval == TS_SUCCESS) {
		WSerial* ws_port = port->ws_port;
		
		ws_port->SendBuffer((char *)data, nbytes);    // Sends N character in buffer ucp, return number sent

	}
	return retval;
}


/*
 * tsGetFD() returns a file descriptor which you can pass to select()
 * or poll() if you want to block until data becomes available in
 * an input port, or space becomes available in an output port.
 *
 * before calling select() or poll(), you must first call tsSetFillPointBytes()
 * to specify when you want to unblock:
 *
 * INPUT PORTS:  will unblock from select() or poll() when
 *               tsGetFilledBytes() >= tsGetFillPoint()
 *
 * OUTPUT PORTS: will unblock from select() or poll() when
 *               tsGetFilledBytes() <  tsGetFillPoint()
 * 
 * the calls tsWrite() and tsRead() may change the fillpoint, so you should
 * make sure to call tsSetFillPointBytes() before each invocation of select()
 * or poll().
 *
 * *NOTE* the definition of output fillpoint differs from that in the AL.
 * the AL file descriptor unblocks when there are more than "fillpoint"
 * spaces in the queue.  this change was necessary to facilitate
 * a future feature of this library: the ability to choose fillpoints 
 * in units of time rather than data.
 *
 * tsSetFillPointBytes() can fail with TS_ERROR_BAD_LIBRARY_CALL if point bad
 */
TSstatus tsSetFillPointBytes(TSport port, int nbytes)
{
	// NOT SUPPORTED
	return TS_ERROR_BAD_LIBRARY_CALL;
}

int tsGetFillPointBytes(TSport port)
{
	// NOT SUPPORTED
	return 0;
}

int tsGetFD(TSport port)
{
	// NOT SUPPORTED
	return -1;
}


/*
 * openWSerialPort() either finds an existing serial port
 * which was already opened in the other direction, or opens
 * a new WSerial port if the port is not presently open.
 * Fails (returning NULL) if the port is already open in the
 * desired direction.
 */
static TSstatus openWSerialPort(TSport ts_port)
{
	TSstatus retval = TS_SUCCESS;
	list<WSerialListEntry>::iterator ws_iter;

	if (ts_port == (TSport) 0 || ts_port->config == (TSconfig) 0) {
		retval = TS_ERROR_OUT_OF_MEM;
	}
	else {
		TSconfig config = ts_port->config;
		WSerialListEntry* ws_listEntry = new WSerialListEntry(
			new _WSerialListEntry(config->portName,
				(config->direction == TS_DIRECTION_TRANSMIT)? ts_port : (TSport) 0,
				((config->direction == TS_DIRECTION_RECEIVE)?  ts_port : (TSport) 0))
			);

		if (ws_listEntry == (WSerialListEntry*) 0
			|| ws_listEntry->item == (_WSerialListEntry*) 0)
		{
			retval = TS_ERROR_OUT_OF_MEM;
		}
		else {
			CAutoThreadLocker lock(WSerialListLock);
			ws_iter = find(WSerialList.begin(), WSerialList.end(), ws_listEntry);
			if (ws_iter == WSerialList.end()) {
				//  Dif not find the WSerial port, so make a new one.
				ts_port->ws_port = new WSerial();
				if (ts_port->ws_port == (WSerial*) 0) {
					retval = TS_ERROR_OUT_OF_MEM;
				}
				else {
					WSerialList.push_back(*ws_listEntry);
					ws_listEntry->item->ws_port = ts_port->ws_port;
				}
			}
			else {
				//  Found the WSerial port, make sure noone has it
				//  open in the direction that we want.
				delete ws_listEntry;
				_WSerialListEntry* ws_listEntryItem = (*ws_iter).item;
				if (config->direction == TS_DIRECTION_TRANSMIT) {
					if (ws_listEntryItem->transmitter != (TSport) 0
						&& ws_listEntryItem->transmitter != ts_port) {
							retval = TS_ERROR_BAD_LIBRARY_CALL;
						}
					else {
						ws_listEntryItem->transmitter = ts_port;
						ts_port->ws_port = ws_listEntryItem->ws_port;
					}
				}
				else if (config->direction == TS_DIRECTION_RECEIVE) {
					if (ws_listEntryItem->receiver != (TSport) 0
						&& ws_listEntryItem->receiver != ts_port) {
							retval = TS_ERROR_BAD_LIBRARY_CALL;
						}
					else {
						ws_listEntryItem->receiver = ts_port;
						ts_port->ws_port = ws_listEntryItem->ws_port;
					}
				}
			}
		}
	}
	return retval;
}


/*
 * closeWSerialPort()
 */
static TSstatus closeWSerialPort(TSport ts_port)
{
	TSstatus retval = TS_SUCCESS;
	list<WSerialListEntry>::iterator ws_iter;

	if (ts_port == (TSport) 0 || ts_port->config == (TSconfig) 0) {
		retval = TS_ERROR_OUT_OF_MEM;
	}
	else {
		TSconfig config = ts_port->config;
		WSerialListEntry* ws_listEntry = new WSerialListEntry (
			new _WSerialListEntry(config->portName)
		);

		if (ws_listEntry == (WSerialListEntry*) 0) {
			retval = TS_ERROR_OUT_OF_MEM;
		}
		else {
			CAutoThreadLocker lock(WSerialListLock);
			ws_iter = find(WSerialList.begin(), WSerialList.end(), ws_listEntry);
			if (ws_iter == WSerialList.end()) {
				// Can't happen.
				retval = TS_ERROR_BAD_LIBRARY_CALL;
			}
			else {
				_WSerialListEntry* ws_listEntryItem = (*ws_iter).item;
				if (config->direction == TS_DIRECTION_TRANSMIT)
					ws_listEntryItem->transmitter = (TSport) 0;
				else if (config->direction == TS_DIRECTION_RECEIVE)
					ws_listEntryItem->receiver = (TSport) 0;
				if (ws_listEntryItem->transmitter == (TSport) 0 &&
					ws_listEntryItem->receiver == (TSport) 0)
				{
					WSerialList.erase(ws_iter);
					delete ws_listEntryItem->ws_port;
					delete ws_listEntryItem;
					ws_listEntryItem = (_WSerialListEntry*) 0;
					delete ws_listEntry;
					ws_listEntry = (WSerialListEntry*) 0;
				}
			}
		}
		ts_port->ws_port = (WSerial*) 0;
	}
	return retval;
}
#endif
