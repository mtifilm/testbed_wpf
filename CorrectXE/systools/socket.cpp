#include <stdio.h>
#include "machine.h"

#ifdef _WINDOWS
//
//  Borland VCL turns off socket support (it provides its own)
//  you must define VCL_FULL in the Project Options-Directories/Conditionals
//  to define SOCKET
#else
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/ioctl.h>
#include <string.h>
#include <unistd.h>
#endif

#include <errno.h>
#include "socket.h"

static char *AddrToHost(struct in_addr *addr, char *hname, int maxhostlen);
static void NetSetErrno(NetErrnoType e);

static NetErrnoType net_errno = E_NET_OK;
static int saved_errno = 0;

/* Translations between ``net_errno'' values and human readable strings.
*/
static const char *net_syserrlist[] = {
	"No Error"
};



#ifdef STRERROR_NOT_DEFINED
const char *strerror(int errno) { return sys_errlist[errno]; }
#endif


#ifdef _WINDOWS
static int InitFlag = false;
static char cErrorMessage[255];

/*-----------GetSystemErrorMessage------------------------JAM--Mar 2000----*/

  char *GetSystemMessage(int ErrorCode)

/* This function returns a string that contains a human readable     */
/* version of the error message in ErrorCode                         */
/* Ususal usage is ErrorMessage = GetSystemMessage(GetLastError());  */
/* This is NOT reentrent code.                                       */
/*                                                                   */
/*********************************************************************/
{

  // Do nothing if not an error
  if (ErrorCode == 0)
    {
       cErrorMessage[0] = '\0';
       return((char *)&cErrorMessage);
    }

  FormatMessage
          (
             FORMAT_MESSAGE_FROM_SYSTEM,
             NULL,
             ErrorCode,
             MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
             (LPTSTR) &cErrorMessage,
             255,
             NULL
          );

  return((char *)&cErrorMessage);
}

int NetOpenInterface(void)
{
    WORD wVersionRequested;
    WSADATA wsaData;
    if (InitFlag) return(0);
    InitFlag = true;

    wVersionRequested = MAKEWORD( 2, 0 );
    return(WSAStartup( wVersionRequested, &wsaData ));
}

int NetCloseInterface(void)
{
  return(WSACleanup());
}
#else
// Unix Linux
int NetOpenInterface(void)
{
  return(0);
}

int NetCloseInterface(void)
{
  return(0);
}

#endif

/* NetErrStr()
 *--------------------------------------------------------------------
 * Returns a diagnostic message for the last failure.
 */

const char *NetErrStr()
{
#ifdef _WINDOWS
  return(GetSystemMessage(WSAGetLastError()));
#else
  return (net_errno==E_NET_ERRNO) ? strerror(saved_errno) :
    net_syserrlist[net_errno];
#endif
}

/* NetErrNo()
 *--------------------------------------------------------------------
 * Returns a diagnostic number for the last failure.
 */
NetErrnoType NetErrNo()
{
  return net_errno;
}



/* NetMakeWelcome()
 *--------------------------------------------------------------------
 * Creates a socket that listens for incoming connections.
 *--------------------------------------------------------------------
 * Creates a socket on the local machine at on given ``port''.
 * Call @NewConnection() and pass the socket returned by this function to
 * it.
 *
 * Returns a socket on success or -1 on error.
 */
SOCKET NetMakeWelcome(int *port)
{
  int fd;
  struct sockaddr_in addr;
  int v;

  fd = socket(AF_INET, SOCK_STREAM, 0);
  if(fd == -1)
    {
      NetSetErrno(E_NET_ERRNO);
      return -1;
    }

  v = 1;
  setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char *)&v, sizeof(v));

  addr.sin_family = AF_INET;
  addr.sin_port = htons(*port);
  addr.sin_addr.s_addr = INADDR_ANY;
  if(bind(fd, (struct sockaddr *)&addr, sizeof(addr)))
    {
      NetSetErrno(E_NET_ERRNO);
      return -1;
    }

/*
   If a temporary socket, Inquire of the socket its actual number.
 */
  if (*port == 0)
    {
      sockaddr_len iLenSockName = sizeof(addr);
      getsockname (fd, (struct sockaddr *)&addr, &iLenSockName);
      *port =  ntohs(addr.sin_port);
    }

  if(listen(fd, 5))
    {
      NetSetErrno(E_NET_ERRNO);
      return -1;
    }

  NetSetErrno(E_NET_OK);
  return fd;
}



/* NetNewConnection()
 *--------------------------------------------------------------------
 * Waits for a new connection.
 *--------------------------------------------------------------------
 * Waits for a new connection on the welcome socket. Once a connection
 * is attempted from a remote host, initializes the client ``newc''
 * and creates a new socket for it to create.
 *
 * Returns -1 on error and 0 on success.
 */
int NetNewConnection(SOCKET w, ClientPtr newc)
{
  int newfd;
  struct sockaddr_in addr;
  sockaddr_len addrlen = sizeof(addr);

  do
      {
	  errno = 0;
	  if(((newfd = accept(w, (struct sockaddr *)&addr, &addrlen)) == -1)
	     && errno != EINTR)
	      {
		NetSetErrno(E_NET_ERRNO);
		return -1;
	      }
      } while(newfd==-1);

  ResetClient(newc);
  InitClient(newc);
  SetClientFd(newc, newfd);

  if(GetClientMaxHlen(newc))
      AddrToHost(&addr.sin_addr,
		 GetClientHname(newc), GetClientMaxHlen(newc));

  NetSetErrno(E_NET_OK);
  return 0;
}


/* NetCloseConnection()
 *--------------------------------------------------------------------
 * Closes a client's connection.
 */
void NetCloseConnection(ClientPtr c)
{
    closesocket(GetClientFd(c));
    CloseClient(c);
    NetSetErrno(E_NET_OK);
}



/* NetClientDataAvail()
 *--------------------------------------------------------------------
 * Returns the number of bytes available on the connection to a client.
 */
int NetClientDataAvail(ClientPtr c)
{
  unsigned long len;
  int fd = GetClientFd(c);

  if(ioctlsocket(fd, FIONREAD, &len))
    {
      
      return -1;
    }

  NetSetErrno(E_NET_OK);
  return len;
}



/* NetGetClientHostname()
 *--------------------------------------------------------------------
 * Gets the hostname of a remote client.
 *--------------------------------------------------------------------
 * Given a buffer ``hanme'' and the size of that buffer, ``maxhostlen'',
 * puts the hostname of the remote client into the buffer.
 *
 * Usually, the client's hostname will will be available through
 * @GetClientHname() if @GetClientMaxHlen() returns non-zero on the
 * client. If however, you have defined @GetClientMaxHlen() to return
 * zero, @GetClientHostname() is the function to call, since the
 * data will not be available through @GetClientHname().
 *
 * Returns a pointer to the buffer on success, or NULL on error.
 */
char *
NetGetClientHostname(ClientPtr c, char *hname, int maxhostlen)
{
  struct sockaddr_in name;
  sockaddr_len nlen = sizeof(name);

  if(getpeername(GetClientFd(c),  (struct sockaddr *)&name, &nlen))
    {
      NetSetErrno(E_NET_ERRNO);
      return NULL;
    }

  NetSetErrno(E_NET_OK);
  return AddrToHost(&name.sin_addr, hname, maxhostlen);
}


/* This is not for you. */
static char *
AddrToHost(struct in_addr *addr, char *hname, int maxhostlen)
{
    struct hostent *hent;

    hent = gethostbyaddr((char *)addr, sizeof(*addr), AF_INET);
    if(hent == NULL)
	    /* Use the ip address as the hostname */
	    strncpy(hname, inet_ntoa(*addr), maxhostlen);
    else
	strncpy(hname, hent->h_name, maxhostlen);

    return hname;
}


static void NetSetErrno(NetErrnoType e)
{
  if(e == E_NET_ERRNO)saved_errno = errno;
  net_errno = e;
  if(net_errno != E_NET_OK) HandleNetError();
}


/* NetMakeContact()
 *--------------------------------------------------------------------
 * Makes a tcp connection to a host:port pair.
 *--------------------------------------------------------------------
 * ``Hostname'' can either be in the form of a hostname or an IP address
 * represented as a string. If the hostname is not found as it is,
 * ``hostname'' is assumed to be an IP address, and it is treated as such.
 *
 * If the lookup succeeds, a TCP connection is established with the
 * specified ``port'' number on the remote host and a stream socket is
 * returned.
 *
 * On any sort of error, an error code can be obtained with @NetErrNo()
 * and a message with @NetErrStr().
 */
// Use NetReleseContact to close this function

SOCKET NetMakeContact(const char *hname, int port)
{
  int fd;
  struct sockaddr_in addr;
  struct hostent *hent;

  fd = socket(AF_INET, SOCK_STREAM, 0);
  if(fd == -1)
    {
      NetSetErrno(E_NET_ERRNO);
      return -1;
    }


  hent = gethostbyname(hname);
  if(hent == NULL)
    addr.sin_addr.s_addr = inet_addr(hname);
  else
    memcpy(&addr.sin_addr, hent->h_addr, hent->h_length);
  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);

  if(connect(fd, (struct sockaddr *)&addr, sizeof(addr)))
    {
      NetSetErrno(E_NET_ERRNO);
      return -1;
    }

  NetSetErrno(E_NET_OK);
  return fd;
}

// This is the close for NetMakeContact
int NetReleaseContact(SOCKET w)
{
   shutdown(w,2); 
   int Result = closesocket(w);
   return(Result);
}

void InitClient(ClientPtr cl) { cl->fd = -1; }
void ResetClient(ClientPtr cl) { cl->fd = -1; }
void SetClientFd(ClientPtr cl, SOCKET fd) { cl->fd = fd; }
SOCKET GetClientFd(ClientPtr cl) { return cl->fd; }
void CloseClient(ClientPtr cl) { ResetClient(cl);}
char *GetClientHname(ClientPtr cl) { return cl->hostname; }
int GetClientMaxHlen(ClientPtr cl) { return sizeof(cl->hostname); }
void HandleNetError() {
  fprintf(stderr, "Network error: %s\n", NetErrStr());
}



