#include "systoolsDLL.h"
#include <machine.h>

#ifdef _WINDOWS
#include "W32Serial.h"
#endif

/* Possible typedef for 'bool' */
#if !defined(__cplusplus)
typedef char bool;
#endif /* bool */

#if !defined(UCHAR)
typedef unsigned char UCHAR;
#endif /* UCHAR */

#if !defined(SCHAR)
typedef signed char SCHAR;
#endif /* SCHAR */

/* Defines */

#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

#define SENDER_MCSD      42

#define MCS_INQUIRY    0x82
#define MCS_RESPONSE_1 0x82
#define MCS_RESPONSE_2 0x01

#define MCS_BUTTON        1
#define MCS_JOG_WHEEL     2
#define MCS_SHUTTLE_RING  3
#define MCS_DAEMON_CMD   99

#define MIN_TYPE_NUM      1	/* Make this the same as the first real thing
				   e.g. MCS_BUTTON */
#define MAX_TYPE_NUM      3	/* Make this the same as the last real thing
				   e.g. MCS_SHUTTLE_RING */

#define MCS_RELEASE       0
#define MCS_PRESS         1

#define MCS_RECORD              0
#define MCS_F6                  1
#define MCS_F4                  2
#define MCS_W7                  3
#define MCS_PLAY                4
#define MCS_STOP                5
#define MCS_FAST_FORWARD        6
#define MCS_REWIND              7
#define MCS_VSTICK_DOWN         8
#define MCS_VSTICK_UP           9
#define MCS_VSTICK_LEFT        10
#define MCS_VSTICK_RIGHT       11
#define MCS_F3                 12
#define MCS_F2                 13
#define MCS_F1                 14
#define MCS_F5                 15
#define MCS_W1                 16
#define MCS_W2                 17
#define MCS_W3                 18
#define MCS_W4                 19
#define MCS_W5                 20
#define MCS_W6                 21
#define MCS_SHIFT_RECORD       22	// The SHIFTs are faked internally
#define MCS_SHIFT_F6           23       //   and to be implemented - TTT
#define MCS_SHIFT_F4           24
#define MCS_SHIFT_W7           25
#define MCS_SHIFT_PLAY         26
#define MCS_SHIFT_STOP         27
#define MCS_SHIFT_FAST_FORWARD 28
#define MCS_SHIFT_REWIND       29
#define MCS_SHIFT_VSTICK_DOWN  30
#define MCS_SHIFT_VSTICK_UP    31
#define MCS_SHIFT_VSTICK_LEFT  32
#define MCS_SHIFT_VSTICK_RIGHT 33
#define MCS_SHIFT_F3           34
#define MCS_SHIFT_F2           35
#define MCS_SHIFT_F1           36
#define MCS_SHIFT_F5           37
#define MCS_SHIFT_W1           38
#define MCS_SHIFT_W2           39
#define MCS_SHIFT_W3           40
#define MCS_SHIFT_W4           41
#define MCS_SHIFT_W5           42
#define MCS_SHIFT_W6           43
#define MCS_SHIFT_DIFF         (MCS_SHIFT_W6 - MCS_W6)   /* Shift difference */

#define MIN_BUTTON_NUM    0	/* Make this the same as the lowest numbered
				   button above */
#define MAX_BUTTON_NUM   43	/* Make this the same as the highest numbered
				   button above */

/* Forward Declarations */
#ifdef __cplusplus
extern "C" {
#endif
#ifdef _WINDOWS
MTI_SYSTOOLSDLL_API WSerial *InitMCS(int iNCommPort);
#else
MTI_SYSTOOLSDLL_API int  InitMCS(int iNCommPort);
#endif
//int  CheckMCS(int *ipType, int *ipButton, int *ipPressRelease,
//              int *ipShuttlePos, int *ipJogPos);
MTI_SYSTOOLSDLL_API int  CheckMCS(UCHAR *ucpType, UCHAR *ucpButton, UCHAR *ucpPressRelease,
              SCHAR *cpShuttlePos, SCHAR *cpJogPos);
MTI_SYSTOOLSDLL_API int  ParseMCSCommand(const UCHAR *ucaResponse, UCHAR *ucpType,
                     UCHAR *ucpButton, UCHAR *ucpPressRelease,
                     SCHAR *cpShuttlePos, SCHAR *cpJogPos);
#ifdef _WINDOWS
MTI_SYSTOOLSDLL_API int  ReadBytes(WSerial *ws_port, unsigned char *ucpResponse,
               const int iNumToRead);
#else
MTI_SYSTOOLSDLL_API int  ReadBytes(const int iFD, unsigned char *ucpResponse, const int iNumToRead);
#endif

#ifdef __cplusplus
};
#endif
