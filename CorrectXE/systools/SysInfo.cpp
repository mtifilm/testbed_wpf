#include "machine.h"
#include "SysInfo.h"
#include "ThreadLocker.h"
#include <thread>
#include <vector>
using std::vector;
#include <iomanip>
#include <IniFile.h>   // for TRACE_N!!!
#include <imagehlp.h>
#include <excpt.h>
#include <psapi.h>

//-------------------------------------------------------------------------

namespace SysInfo {

MTI_UINT32 GetUserMaxAddress()
{
   MTI_UINT32 maxAppAddr;
   double availableAddrSpaceInGB;
   const double oneGB = 1024.0 * 1024.0;
   static bool oneShot = true;
   SYSTEM_INFO siSysInfo;

   GetSystemInfo(&siSysInfo);
   maxAppAddr = (MTI_UINT32) siSysInfo.lpMaximumApplicationAddress;
   availableAddrSpaceInGB = (maxAppAddr + 1.0) / oneGB;

   if (oneShot)
   {
      TRACE_0(errout << "INFO: Available address space is "
                     << std::setprecision(2) << std::fixed << availableAddrSpaceInGB);
      oneShot = false;
   }

   return maxAppAddr;
}

MTI_UINT32 GetPhysicalMemorySizeInGB()
{
   MEMORYSTATUSEX memoryStatus;
   const MTI_UINT64 oneGB = (1024*1024*1024);

   memoryStatus.dwLength = sizeof(memoryStatus);
   GlobalMemoryStatusEx(&memoryStatus);

   // Need to round up to the next GB because the OS lies
   return MTI_UINT32((memoryStatus.ullTotalPhys + (oneGB - 1)) / oneGB);
}

} // end SysInfo namespace
//-------------------------------------------------------------------------


#ifndef _MSC_VER   // Actually _WIN64

// Code for counting CPUs was found in an msdn blog "The Old New Thing"
// at URL http://blogs.msdn.com/oldnewthing/archive/2005/12/16/504659.aspx
// contributed by David Heffernan who claims he ported code provided by
// Intel on their web site. I would've started with that code, but the
// link got me source code for determining how much battery power was left
// or something like that, instead of the advertised cpu counting code.
// - mbraca 1/24/2006

namespace {  // ============== BEGIN LOCAL NAMESPACE ==============

int cachedProcessorCount = -1;
CSpinLock cachedProcessorCountLock;

#ifndef _WIN64
unsigned long GetAPIC_ID()
{
  static unsigned long RegEBX;

  // AVOID BORLAND COMPILER BUG: save registers in variables instead of
  // pushing them on the stack!!
  static long SaveEBX, SaveECX, SaveEDX;

  _asm
   {
    mov SaveEBX,ebx     // push ebx
    mov SaveECX,ecx     // push ecx
    mov SaveEDX,edx     // push edx

    mov eax,1
    cpuid
    mov RegEBX,ebx

    mov edx,SaveEDX     // pop edx
    mov ecx,SaveECX     // pop ecx
    mov ebx,SaveEBX     // pop ebx
   }
  return (RegEBX >> 24);
}

unsigned long GetMaxBasicCPUIDLeaf()
{
  static unsigned long retval;

  // AVOID BORLAND COMPILER BUG: save registers in variables instead of
  // pushing them on the stack!!
  static long SaveEBX, SaveECX, SaveEDX;

  _asm {
	mov SaveEBX,ebx     // push ebx
	mov SaveECX,ecx     // push ecx
	mov SaveEDX,edx     // push edx

	mov eax,0
	cpuid
	mov retval,eax

	mov edx,SaveEDX     // pop edx
	mov ecx,SaveECX     // pop ecx
	mov ebx,SaveEBX     // pop ebx
  }
  return retval;
}

bool  ProcessorPackageSupportsLogicalProcessors()
{
  const unsigned long HT_BIT = 0x10000000;
  // static unsigned long FAMILY_ID = 0x00000F00;
  // static unsigned long EXT_FAMILY_ID = 0x00F00000;
  // static unsigned long PENTIUM4_ID = 0x00000F00;
  static char VendorID[13];
  static long *VendorID0 = (long *) &VendorID[0];
  static long *VendorID4 = (long *) &VendorID[4];
  static long *VendorID8 = (long *) &VendorID[8];
  static unsigned long RegEDX;
  static bool Result;
  //bool ProcessorSupportsHT;

  // AVOID BORLAND COMPILER BUG: save registers in variables instead of
  // pushing them on the stack!!
  static long SaveEBX, SaveECX, SaveEDX;

  memset(VendorID, 0, 13);
  RegEDX = 0;

  Result = false; //may be overwritten later
  _asm {
	mov SaveEBX,ebx     // push ebx
	mov SaveECX,ecx     // push ecx
	mov SaveEDX,edx     // push edx

	// call CPUID with EAX=0 and record the result in VendorID
	mov eax,0
	cpuid

	// test the maximum basic CPUID leaf and quit if it's less than 1 which we
	// need below
	cmp eax,1
	jl Q1

	//record Vendor ID
	mov eax, VendorID0
	mov dword ptr [eax],ebx
	mov eax, VendorID4
	mov dword ptr [eax],edx
	mov eax, VendorID8
	mov dword ptr [eax],ecx

	//call CPUID with EAX=1 and record the EDX register
	mov eax,1
	cpuid
	mov RegEDX,edx

  Q1:
	mov edx,SaveEDX     // pop edx
	mov ecx,SaveECX     // pop ecx
	mov ebx,SaveEBX     // pop ebx
  }

  if (0 == strcmp(VendorID, "GenuineIntel"))
   {
	Result = ((RegEDX & HT_BIT) != 0);
   }

  return Result;
}

unsigned long GetLogicalProcessorCountPerPackage()
{
  const unsigned long NUM_LOGICAL_BITS = 0x00FF0000;
  static unsigned long RegEBX;

  // AVOID BORLAND COMPILER BUG: save registers in variables instead of
  // pushing them on the stack!!
 static long SaveEBX, SaveECX, SaveEDX;


  _asm {
	mov SaveEBX,ebx     // push ebx
	mov SaveECX,ecx     // push ecx
	mov SaveEDX,edx     // push edx

	mov eax,1
	cpuid
	mov RegEBX,ebx

	mov edx,SaveEDX     // pop edx
	mov ecx,SaveECX     // pop ecx
	mov ebx,SaveEBX     // pop ebx
  }

  return ((RegEBX & NUM_LOGICAL_BITS) >> 16);
}

unsigned long GetMaxCoresPerPackage()
{
  static unsigned long RegEAX;
  unsigned long Result;

  // AVOID BORLAND COMPILER BUG: save registers in variables instead of
  // pushing them on the stack!!
  static long SaveEBX, SaveECX, SaveEDX;

  if (GetMaxBasicCPUIDLeaf() >= 4) {
	_asm
	 {
	  mov SaveEBX,ebx     // push ebx
	  mov SaveECX,ecx     // push ecx
	  mov SaveEDX,edx     // push edx

	  mov eax,4
	  mov ecx,0
	  cpuid
	  mov RegEAX,eax

	  mov edx,SaveEDX     // pop edx
	  mov ecx,SaveECX     // pop ecx
	  mov ebx,SaveEBX     // pop ebx
	 }
	Result = (RegEAX >> 26) + 1;
  }
 else
  {
   Result = 1;
  }
  return Result;
}
#endif

} // ================= END LOCAL NAMESPACE ==========================

#endif // _MSC_VER
//-------------------------------------------------------------------------

namespace SysInfo {
//-------------------------------------------------------------------------

int AvailableProcessorCount()
{
  CAutoSpinLocker lock(cachedProcessorCountLock);

  if (cachedProcessorCount > 0)
  {
	  return cachedProcessorCount;
  }

  SYSTEM_INFO systemInfo;
  GetSystemInfo(&systemInfo);
  int Result = systemInfo.dwNumberOfProcessors;


  // Note that Windows won't let us use more than 64 logical processors.
#ifdef _DEBUG
  TRACE_0(errout << "Hardware concurrency = " << Result);
  if (Result > 16)
  {
		TRACE_0(errout << "Hardware concurrency LIMITED TO 16 PROCESSORS!");
		Result = 16;
  }
#else
  if (Result > 64)
  {
		Result = 64;
  }
#endif

  // Because we don't do processor affinity, we leave 4 free logical processors
  // if total is 64, 2 free if 32, and 1 free if 16.
  Result -= Result / 16;

  cachedProcessorCount = Result;

  return Result;
}
//-------------------------------------------------------------------------

int AvailableProcessorCoreCount()
{
#ifdef _WIN64 // Actually should be _WIN64?

	int Result = AvailableProcessorCount();   // Don't discriminate between phys and log

#else
/*
  Returns total number of processors available to system excluding logical
  hyperthreaded processors.  We only have to do significant work for Intel
  processors since they are the only ones which implement hyperthreading.
  It's not 100% clear whether the hyperthreading bit (CPUID(1) -> EDX[28])
  will be set for processors with multiple cores but without hyperthreading.
  My reading of the documentation is that it will be set but the code is
  conservative and performs the APIC ID decoding if either:

  1. The hyperthreading bit is set, or
  2. The processor reports >1 cores on the physical package.

  If either of these conditions hold then we proceed to read the APIC ID
  for each logical processor recognised by the OS. This ID can be decoded
  to the form (PACKAGE_ID, CORE_ID, LOGICAL_ID) where PACKAGE_ID identifies
  the physical processor package, CORE_ID identifies a physical core on that
  package and LOGICAL_ID identifies a hyperthreaded processor on that core.

  The job of this routine is therefore to count the number of unique cores,
  that is the number of unique pairs (PACKAGE_ID, CORE_ID).

  If the chip is not an Intel processor, or if it is Intel but doesn't have
  multiple logical processors on a physical package then the routine simply
  returns AvailableProcessorCount.
*/
  int i;
  vector<int> PackCoreList;

  unsigned long LogicalProcessorCountPerPackage, MaxCoresPerPackage, LogicalPerCore,
        APIC_ID, /* PACKAGE_ID, CORE_ID, LOGICAL_ID, */ PACKAGE_CORE_ID,
        CORE_ID_MASK, CORE_ID_SHIFT, LOGICAL_ID_MASK, LOGICAL_ID_SHIFT,
        ProcessAffinityMask, SystemAffinityMask, ThreadAffinityMask, Mask;

  int Result = 0;

  try
   {
    // see Intel documentation (Y:\Intel\IA32_manuals) for details on
    // logical processor topology
    char *cp = getenv("OS");
    if (cp != NULL && 0 == strcmp(cp, "Windows_NT"))
     {
      MaxCoresPerPackage = GetMaxCoresPerPackage();
      if (MaxCoresPerPackage < 1)
        MaxCoresPerPackage = 1;
      if (ProcessorPackageSupportsLogicalProcessors() || (MaxCoresPerPackage > 1))
       {
        LogicalProcessorCountPerPackage = GetLogicalProcessorCountPerPackage();
        LogicalPerCore = LogicalProcessorCountPerPackage / MaxCoresPerPackage;

        LOGICAL_ID_MASK = 0xFF;
        LOGICAL_ID_SHIFT = 0;
        for (i = 1; i < (int) LogicalPerCore; i <<= 1)
         {
          LOGICAL_ID_MASK <<= 1;
          ++LOGICAL_ID_SHIFT;
         }

		CORE_ID_SHIFT = 0;
        if (MaxCoresPerPackage > 1)
         {
          CORE_ID_MASK = LOGICAL_ID_MASK;
          for (i = 1; i < (int) MaxCoresPerPackage; i <<= 1)
           {
            CORE_ID_MASK <<= 1;
            ++CORE_ID_SHIFT;
           }
         }
        else
         {
          CORE_ID_MASK = 0xFF;
         }

        LOGICAL_ID_MASK = ~LOGICAL_ID_MASK;
        CORE_ID_MASK = ~CORE_ID_MASK;

		if (GetProcessAffinityMask(GetCurrentProcess(), (PDWORD_PTR) &ProcessAffinityMask,
											(PDWORD_PTR) &SystemAffinityMask))
         {
          ThreadAffinityMask = SetThreadAffinityMask(GetCurrentThread(),
                           ProcessAffinityMask);//get the current thread affinity
          if (ThreadAffinityMask != 0)
           {
            PackCoreList.clear();
            try
             {
              for (i = 0; i < 32; ++i)
               {
                Mask = 1 << i;
                if ((ProcessAffinityMask & Mask) != 0)
                 {
                  if (SetThreadAffinityMask(GetCurrentThread(), Mask) != 0)
                   {
                    vector<int>::iterator iter;

                    Sleep(0); //allow OS to reschedule thread onto the selected processor
					APIC_ID = GetAPIC_ID();
                    // LOGICAL_ID = APIC_ID & LOGICAL_ID_MASK;
                    // CORE_ID = (APIC_ID & CORE_ID_MASK) >> LOGICAL_ID_SHIFT;
                    // PACKAGE_ID = APIC_ID >> (LOGICAL_ID_SHIFT + CORE_ID_SHIFT);

                    PACKAGE_CORE_ID = APIC_ID & (~LOGICAL_ID_MASK); //mask out LOGICAL_ID
                    // identifies the processor core - it's not a value defined by Intel,
                    // rather it's defined by us!

                    //count the number of unique processor cores
                    for (iter = PackCoreList.begin(); iter != PackCoreList.end(); ++iter)
                     {
                      if (*iter == (int) PACKAGE_CORE_ID)
                        break;
                     }
                    if (iter ==  PackCoreList.end())
                     { // not found
                      PackCoreList.push_back(PACKAGE_CORE_ID);
                     }
                   }
                 }
               }
              Result = PackCoreList.size();
             }
            catch(...) {}

            //restore thread affinity
            SetThreadAffinityMask(GetCurrentThread(), ThreadAffinityMask);
           }
         }
       }
     }
   }
  catch (...) {}
  if (Result == 0)
   {
    // if we haven't modified Result above, then assume that all logical
    // processors are true physical processor cores
    Result = AvailableProcessorCount();
   }
#endif // _MSC_VER

  return Result;

} /* AvailableProcessorCoreCount */
//-------------------------------------------------------------------------

BOOL SetPrivilege(
	HANDLE hToken,          // access token handle
	LPCTSTR lpszPrivilege,  // name of privilege to enable/disable
	BOOL bEnablePrivilege   // to enable or disable privilege
	)
{
	TOKEN_PRIVILEGES tp;
	LUID luid;

	if (!LookupPrivilegeValue(
		NULL,            // lookup privilege on local system
		lpszPrivilege,   // privilege to lookup
		&luid))          // receives LUID of privilege
	{
		TRACE_0(errout << "ERROR: DPX read cache: LookupPrivilegeValue error: "
                       << GetLastError());
		return FALSE;
	}

	tp.PrivilegeCount = 1;
	tp.Privileges[0].Luid = luid;
	if (bEnablePrivilege)
	{
		tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
	}
	else
	{
		tp.Privileges[0].Attributes = 0;
	}

	// Enable the privilege or disable all privileges.
	if (!AdjustTokenPrivileges(
		hToken,
		FALSE,
		&tp,
		sizeof(TOKEN_PRIVILEGES),
		(PTOKEN_PRIVILEGES) NULL,
		(PDWORD) NULL))
	{
		TRACE_0(errout << "ERROR: DPX read cache: AdjustTokenPrivileges error: "
                       << GetLastError());
		return FALSE;
	}

	if (GetLastError() == ERROR_NOT_ALL_ASSIGNED)

	{
		TRACE_0(errout << "ERROR: DPX read cache: The token does not have the specified privilege");
		return FALSE;
	}

    TRACE_3(errout << "RRRRRRRRRRRR DPXBRC set privilege " << lpszPrivilege);

	return TRUE;
}
//-------------------------------------------------------------------------

BOOL SetPrivilege(
	LPCTSTR lpszPrivilege,  // name of privilege to enable/disable
	BOOL bEnablePrivilege   // to enable or disable privilege
	)
{
	HANDLE token;
	BOOL result;

	// Open the token.

	result = OpenProcessToken(GetCurrentProcess(),
		TOKEN_ADJUST_PRIVILEGES,
		&token);

	if (result != TRUE)
	{
		TRACE_0(errout << "ERROR: DPX read cache: Cannot open process token, error: "
                       << GetLastError());
		return FALSE;
	}

	result = SetPrivilege(token, lpszPrivilege, bEnablePrivilege);

	int lastError = GetLastError();
	if( result != TRUE )
	{
		TRACE_0(errout << "ERROR: DPX read cache: Cannot adjust token privileges, error: "
                       << lastError);
	}
	else if (lastError != ERROR_SUCCESS )
	{
		TRACE_0(errout << "ERROR: DPX read cache: Cannot enable the "
                       << lpszPrivilege << " privilege, error: " << lastError);
		TRACE_0(errout << "************* Please check the local policy! ***************");
		printf ("please check the local policy.\n");
		result = FALSE;
	}

	CloseHandle( token );
	return result;
}
//-------------------------------------------------------------------------

size_t GetSystemPageSize()
{
   static size_t pageSize = 0;

   if (pageSize == 0)
   {
      SYSTEM_INFO sSysInfo;
      GetSystemInfo(&sSysInfo);
      pageSize = (size_t)sSysInfo.dwPageSize;
   }

   return pageSize;
}
//-------------------------------------------------------------------------

string GetSystemErrorMessage(int errorCode)
{
  // Do nothing if not an error
  if (errorCode == 0)
  {
     return("");
  }

  char errorMessage[255];

  FormatMessage
          (
             FORMAT_MESSAGE_FROM_SYSTEM,
             NULL,
             errorCode,
             MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
             (LPTSTR) &errorMessage,
             255,
             NULL
          );

  return errorMessage;
};

   const std::string opDescription(const MTI_UINT64 opcode)
   {
      switch (opcode)
      {
      case 0:
         return "read";
      case 1:
         return "write";
      case 8:
         return "user-mode data execution prevention (DEP) violation";
      default:
         return "unknown";
      }
   }

   const std::string seDescription(const unsigned int& code)
   {
      switch (code)
      {
      case EXCEPTION_ACCESS_VIOLATION:
         return "EXCEPTION_ACCESS_VIOLATION";
      case EXCEPTION_ARRAY_BOUNDS_EXCEEDED:
         return "EXCEPTION_ARRAY_BOUNDS_EXCEEDED";
      case EXCEPTION_BREAKPOINT:
         return "EXCEPTION_BREAKPOINT";
      case EXCEPTION_DATATYPE_MISALIGNMENT:
         return "EXCEPTION_DATATYPE_MISALIGNMENT";
      case EXCEPTION_FLT_DENORMAL_OPERAND:
         return "EXCEPTION_FLT_DENORMAL_OPERAND";
      case EXCEPTION_FLT_DIVIDE_BY_ZERO:
         return "EXCEPTION_FLT_DIVIDE_BY_ZERO";
      case EXCEPTION_FLT_INEXACT_RESULT:
         return "EXCEPTION_FLT_INEXACT_RESULT";
      case EXCEPTION_FLT_INVALID_OPERATION:
         return "EXCEPTION_FLT_INVALID_OPERATION";
      case EXCEPTION_FLT_OVERFLOW:
         return "EXCEPTION_FLT_OVERFLOW";
      case EXCEPTION_FLT_STACK_CHECK:
         return "EXCEPTION_FLT_STACK_CHECK";
      case EXCEPTION_FLT_UNDERFLOW:
         return "EXCEPTION_FLT_UNDERFLOW";
      case EXCEPTION_ILLEGAL_INSTRUCTION:
         return "EXCEPTION_ILLEGAL_INSTRUCTION";
      case EXCEPTION_IN_PAGE_ERROR:
         return "EXCEPTION_IN_PAGE_ERROR";
      case EXCEPTION_INT_DIVIDE_BY_ZERO:
         return "EXCEPTION_INT_DIVIDE_BY_ZERO";
      case EXCEPTION_INT_OVERFLOW:
         return "EXCEPTION_INT_OVERFLOW";
      case EXCEPTION_INVALID_DISPOSITION:
         return "EXCEPTION_INVALID_DISPOSITION";
      case EXCEPTION_NONCONTINUABLE_EXCEPTION:
         return "EXCEPTION_NONCONTINUABLE_EXCEPTION";
      case EXCEPTION_PRIV_INSTRUCTION:
         return "EXCEPTION_PRIV_INSTRUCTION";
      case EXCEPTION_SINGLE_STEP:
         return "EXCEPTION_SINGLE_STEP";
      case EXCEPTION_STACK_OVERFLOW:
         return "EXCEPTION_STACK_OVERFLOW";
      default:
         {
            std::ostringstream os;
            os << std::hex << std::setfill('0') << std::setw(8) << "Unknown SEH error 0x" << code;
            return os.str();
         }
      }
   }

//-------------------------------------------------------------------------

//typedef struct _PROCESS_MEMORY_COUNTERS_EX {
//  DWORD  cb;
//  DWORD  PageFaultCount;
//  SIZE_T PeakWorkingSetSize;
//  SIZE_T WorkingSetSize;
//  SIZE_T QuotaPeakPagedPoolUsage;
//  SIZE_T QuotaPagedPoolUsage;
//  SIZE_T QuotaPeakNonPagedPoolUsage;
//  SIZE_T QuotaNonPagedPoolUsage;
//  SIZE_T PagefileUsage;
//  SIZE_T PeakPagefileUsage;
//  SIZE_T PrivateUsage;
//} PROCESS_MEMORY_COUNTERS_EX;


size_t GetWorkingSetSize()
{
   PROCESS_MEMORY_COUNTERS processMemoryCounters;
   processMemoryCounters.cb = sizeof(processMemoryCounters);
   size_t retVal = 0;
   if (GetProcessMemoryInfo(::GetCurrentProcess(), &processMemoryCounters, processMemoryCounters.cb))
   {
      retVal = processMemoryCounters.WorkingSetSize;
   }

   return retVal;
}
//-------------------------------------------------------------------------

} // End SysInfo namespace
