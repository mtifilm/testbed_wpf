#include <windows.h>
#include "ReadUSTCounter.h"
#include "winnt.h"

#pragma option -5
//-----------------ReadUSTCounter-----------------John Mertus----Mar 2003-----

  void ReadUSTCounter(LARGE_INTEGER &li)

//  Just read the UST counter in the pentium.
//  li returns the count as a LARGE_INTEGER
//
//****************************************************************************
{
#if _WIN64
	__asm__ __volatile__("rdtsc" : "=a" (li.u.LowPart), "=d" (li.u.HighPart));

#elif MTI_ASM_X86_INTEL == 1
	unsigned long lp;
	long hp;
        
	_asm
	{
		// cpuid forces CPU to complete preceding instructions
		// before rdtsc reads the Time Stamp Counter.  I don't know
		// if this is really required or just programmers' superstition
		//  Note by JAM
		//    The cpuid is necessary if trying to count the actual
		//    number of cpu cycles, but in our case, our time periods
		//    are large enough for out of sequence execution to matter.
		//
		//  However, the cpuid creates a problem when the borland (and
		//  Visual C++) compilers force optimization.  This is because
		//  cpuid destroys the ebx and ecx registers and the optimizer
		//  does not know about this.  A compiler bug
		push ebx
		push ecx
				
		xor eax,eax
		xor edx,edx
		cpuid
		rdtsc
		mov hp,edx
		mov lp,eax

		pop ecx
		pop ebx
				
	}
	li.HighPart = hp;
	li.LowPart = lp;
#else
#warning "function not implemented for this platform **************************************************"
#endif
}

