/*
   File Name:  systoolsDLL.h
   Created Aug 22/2001 by John Mertus

   This just defines the export/import macros
   if MTI_SYSTOOLSDLL is defined, exports are defined
   else imports are defined
*/

#ifndef SYSTOOLSDLL_H
#define SYSTOOLSDLL_H

//////////////////////////////////////////////////////////////////////

#include "machine.h"

#ifdef _WIN32                  // Window compilers need export/import
// Define Import/Export for windows
#ifdef MTI_SYSTOOLSDLL
#define MTI_SYSTOOLSDLL_API  __declspec(dllexport)
#else
#define MTI_SYSTOOLSDLL_API  __declspec(dllimport)
#endif

//--------------------UNIX---------------------------
#else

// Needs nothing for SGI, UNIX, LINUX
#define MTI_SYSTOOLSDLL_API
#endif   // End of UNIX

#endif   // Header file


