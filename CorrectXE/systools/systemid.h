#include "machine.h"
#include "systoolsDLL.h"

MTI_SYSTOOLSDLL_API MTI_UINT64 sysid64(void);
MTI_SYSTOOLSDLL_API int getSysID(MTI_UINT64 *ullp, int iMaxSize);
