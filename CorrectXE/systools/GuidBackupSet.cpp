/** @file GuidBackupSet.cpp
 *
 * Implementation of CGuidBackupSet.
 */

#include "GuidBackupSet.hpp"
#include "MTIio.h"
#include "guid_mti.h"

/** @brief Create a GUID string without hyphens.
 *
 * Backup identifiers are commonly used in file names.  Removes
 * hyphens to help keep identifiers within the limitations imposed by
 * file systems.
 */
static std::string CreateUnhyphenatedGuidString()
{
    std::string guidString = guid_to_string(guid_create());

    // Kill all hyphens to make the string shorter.
    SearchAndReplace(guidString, "-", "");

    return guidString;
}


CGuidBackupSet::CGuidBackupSet()
    : CBackupSet(CreateUnhyphenatedGuidString())
{
    // nothing else to do
}


CGuidBackupSet::CGuidBackupSet(std::string const &aBackupSetString)
    : CBackupSet(aBackupSetString)
{
    // nothing else to do
}
