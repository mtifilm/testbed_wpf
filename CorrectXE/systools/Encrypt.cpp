/*
	File:  Encrypt.cpp
	By:    Kevin Manbeck
	Date:  Sept 25, 2002

	This contains functions for encrypting sensitive data.
*/
#include "machine.h"
#include "Encrypt.h"
#include "IniFile.h"
#include "systemid.h"
#include "MTIstringstream.h"

MTI_UINT64 UsageEncrypt (MTI_UINT64 ullValue)
/*
        This function takes an input value and encrypts it to create
        the output value.

        The encryption cannot be reversed.  It's main use is as a
        kind of check sum.  When an encryption does not match,
        there is an indication something is wrong.
*/
{
  MTI_UINT64 ullX, ullX0, ullX1, ullX2, ullX3, ullX4, ullX5, ullX6, ullX7,
	ullX8, ullX9, ullX10, ullX11, ullX12, ullX13, ullX14, ullX15;
  MTI_UINT64 ullCrypt;

  ullX = (ullValue + 3141) * 271; /* just to make things more difficult
                                                to decipher */

  ullX0 = ullX&0xF;
  ullX1 = (ullX&0xF0)>>4;
  ullX2 = (ullX&0xF00)>>8;
  ullX3 = (ullX&0xF000)>>12;
  ullX4 = (ullX&0xF0000)>>16;
  ullX5 = (ullX&0xF00000)>>20;
  ullX6 = (ullX&0xF000000)>>24;
  ullX7 = (ullX&0xF0000000)>>28;
  ullX8 = (ullX&UINT64_LITERAL(0xF00000000))>>32;
  ullX9 = (ullX&UINT64_LITERAL(0xF000000000))>>36;
  ullX10= (ullX&UINT64_LITERAL(0xF0000000000))>>40;
  ullX11= (ullX&UINT64_LITERAL(0xF00000000000))>>44;
  ullX12= (ullX&UINT64_LITERAL(0xF000000000000))>>48;
  ullX13= (ullX&UINT64_LITERAL(0xF0000000000000))>>52;
  ullX14= (ullX&UINT64_LITERAL(0xF00000000000000))>>56;
  ullX15= (ullX&UINT64_LITERAL(0xF000000000000000))>>60;

  ullCrypt =	(ullX0<<60) +
		(ullX2<<56) +
		(ullX4<<52) +
		(ullX6<<48) +
		(ullX8<<44) +
		(ullX10<<40) +
		(ullX12<<36) +
		(ullX14<<32) +
                (ullX15<<28) +
                (ullX13<<24) +
                (ullX11<<20) +
                (ullX9<<16) +
                (ullX7<<12) +
		(ullX5<<8) +
		(ullX3<<4) +
		ullX1;

  return ullCrypt;
}  /* UsageEncrypt */

int WriteSystemID ()
{
  MTI_UINT64 ullSysID, ullEncrypt;
  string strDongleConfig;
  CIniFile *ini=0;

  // get the system id
  ullSysID = sysid64 ();

  // encrypt the system id
  ullEncrypt = UsageEncrypt (ullSysID);

  // get the name of the dongle config file
  ini = CPMPOpenLocalMachineIniFile ();
  if (ini == NULL)
   {
    return -1;
   }

  strDongleConfig = ini->ReadFileNameCreate ("General", "DongleConfig",
		"$(CPMP_LOCAL_DIR)dongle.cfg");

  // close the ini file
  delete ini;
  ini = 0;

  // open the dongle config file
  ini = CreateIniFile (strDongleConfig);
  if (ini == NULL)
   {
    return -1;
   }

  ini->WriteUnsignedInteger ("Dongle", "System", ullEncrypt);

  // close dongle config file
  delete ini;
  ini = 0;
  
  return 0;
}  /* WriteSystemID */

/*
 *   ReadSystemID() - checks the system id based on the MAC addresses
 *                    of the system versus the one stored in the
 *                    dongle.cfg file.
 *      Returns:
 *         0 - SYSTEM_ID_SUCCESS
 *        -1 - SYSTEM_ID_GENERAL_FAILURE
 *        -2 - SYSTEM_ID_NO_ADAPTERS
*/
int ReadSystemID ()
{
  MTI_UINT64 ullaSysID[10], ullEncrypt, ullFromFile;
  int iRetVal, iNSysID, iSysID;
  string strDongleConfig;
  CIniFile *ini=0;

  // get the name of the dongle config file
  ini = CPMPOpenLocalMachineIniFile ();
  if (ini == NULL)
   {
    return SYSTEM_ID_GENERAL_FAILURE;
   }

  strDongleConfig = ini->ReadFileNameCreate ("General", "DongleConfig",
		"$(CPMP_LOCAL_DIR)dongle.cfg");

  // close the ini file
  delete ini;
  ini = 0;

  // open the dongle config file
  ini = CreateIniFile (strDongleConfig);
  if (ini == NULL)
   {
    return SYSTEM_ID_GENERAL_FAILURE;
   }

  ullFromFile = ini->ReadUnsignedInteger ("Dongle", "System", 0);

  // close dongle config file
  delete ini;
  ini = 0;

  // get the list of system ids
  iNSysID = getSysID (ullaSysID, 10);

  if (iNSysID == 0)               // No MAC addresses - probably all network
    return SYSTEM_ID_NO_ADAPTERS; //   adapters are disabled.

  iRetVal = SYSTEM_ID_GENERAL_FAILURE;  // assume failure condition

  for (iSysID = 0; iSysID < iNSysID; iSysID++)
   {
    MTIostringstream ostrMAC;
    ostrMAC << std::setw(12) << std::setfill('0') << std::hex << ullaSysID[iSysID];
    string strtmp = ostrMAC.str();
    string strMAC = strtmp.substr(0, 2) + ":" + strtmp.substr(2, 2) + ":"
                    + strtmp.substr(4, 2) + ":" + strtmp.substr(6, 2) + ":"
                    + strtmp.substr(8, 2) + ":" + strtmp.substr(10, 2);
    TRACE_2(errout << "MAC: " << strMAC << endl);
    // encrypt the system id
    ullEncrypt = UsageEncrypt (ullaSysID[iSysID]);

    // if entry in file does match, return success
    if (ullFromFile == ullEncrypt)
      iRetVal = SYSTEM_ID_SUCCESS;
   }

  return iRetVal;
}  /* ReadSystemID */

int ReadDonglePort ()
{
  int iDonglePort = -1;
  string strDongleConfig;
  CIniFile *ini=0;

  // get the name of the dongle config file
  ini = CPMPOpenLocalMachineIniFile ();
  if (ini == NULL)
   {
    return iDonglePort;
   }

  strDongleConfig = ini->ReadFileNameCreate ("General", "DongleConfig",
		"$(CPMP_LOCAL_DIR)dongle.cfg");

  // close the ini file
  delete ini;
  ini = 0;

  // open the dongle config file
  ini = CreateIniFile (strDongleConfig);
  if (ini == NULL)
   {
    return iDonglePort;
   }

  iDonglePort = ini->ReadInteger ("Dongle", "Port", iDonglePort);

  // close dongle config file
  delete ini;
  ini = 0;
  
  return iDonglePort;
}  /* ReadDonglePort */

int WriteDonglePort (int iDonglePort)
{
  string strDongleConfig;
  CIniFile *ini=0;

  // get the name of the dongle config file
  ini = CPMPOpenLocalMachineIniFile ();
  if (ini == NULL)
   {
    return -1;
   }

  strDongleConfig = ini->ReadFileNameCreate ("General", "DongleConfig",
		"$(CPMP_LOCAL_DIR)dongle.cfg");

  // close the ini file
  delete ini;
  ini = 0;

  // open the dongle config file
  ini = CreateIniFile (strDongleConfig);
  if (ini == NULL)
   {
    return -1;
   }

  ini->WriteInteger ("Dongle", "Port", iDonglePort);

  // close dongle config file
  delete ini;
  ini = 0;
  
  return 0;
}  /* WriteDonglePort */
