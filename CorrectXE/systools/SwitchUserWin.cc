/*
   File Name: SwitchUserWin.cc
   Created July 22 by John Mertus

   Windows version of switching
   DUMMY FOR NOW
*/

//
// Storage for the static switcher
CSwitchUser theSwitchUser;
#include "IniFile.h"
#include "DllSupport.h"

//-------------------SetupPIDs--------------------John Mertus----Mar 2002---*/

  bool CSwitchUser::SetupPIDs(void)

//   This is never run by the user, but called by the SwitchToXXX functions
// Its responsibility is to load
//
//
//****************************************************************************
{
  return(true);
}

//-----------------------ToMTI---------------------John Mertus----Mar 2002----

  bool CSwitchUser::ToMti(void)

//   This trys to switch the user to MTI
//   return is true if user is not MTI
//   return is false if user has not changed
//
//
//****************************************************************************
{
  return(true);
}

//---------------------ToUser---------------------John Mertus----Mar 2002----

  bool CSwitchUser::ToUser(void)

//   This trys to switch the user to user
//   return is true if user is not user
//   return is false if user has not changed
//
//
//****************************************************************************
{
  return(true);
}

//-----------------------ToRoot-------------------John Mertus----Mar 2002----

  bool CSwitchUser::ToRoot(void)

//   This trys to switch the user to root
//   return is true if user is not root
//   return is false if user has not changed
//
//
//****************************************************************************
{
  return(true);
}


//----------------SetHighPriority-----------------John Mertus----Feb 2002---*/

  void SetHighPriority(void)

//   This sets the calling process, as well as the resulting threads to
// a real time priority.
//
//****************************************************************************
{
  return;
#ifdef TTT
  if (!SetPriorityClass(GetCurrentProcess(), HIGH_PRIORITY_CLASS))
    TRACE_0(errout << "SetPriorityClass Failed, " << GetSystemMessage(GetLastError()));
  if (!SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_TIME_CRITICAL))
    TRACE_0(errout << "SetThreadPriority Failed, " << GetSystemMessage(GetLastError()));
#else
  if (!SetPriorityClass(GetCurrentProcess(), NORMAL_PRIORITY_CLASS))
    TRACE_0(errout << "SetPriorityClass Failed, " << GetSystemMessage(GetLastError()));
  if (!SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_NORMAL))
    TRACE_0(errout << "SetThreadPriority Failed, " << GetSystemMessage(GetLastError()));
#endif
}

//----------------SetRealTimePriority--------------John Mertus----Feb 2002---*/

  void SetRealTimePriority(void)

//   This sets the calling process, as well as the resulting threads to
// a real time priority.
//
//****************************************************************************
{
  return;
#ifdef TTT
  if (!SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS))
    TRACE_0(errout << "SetPriorityClass Failed, " << GetSystemMessage(GetLastError()));
  if (!SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_TIME_CRITICAL))
    TRACE_0(errout << "SetThreadPriority Failed, " << GetSystemMessage(GetLastError()));
#else
  if (!SetPriorityClass(GetCurrentProcess(), NORMAL_PRIORITY_CLASS))
    TRACE_0(errout << "SetPriorityClass Failed, " << GetSystemMessage(GetLastError()));
  if (!SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_NORMAL))
    TRACE_0(errout << "SetThreadPriority Failed, " << GetSystemMessage(GetLastError()));
#endif
}


//
// This is set if the program succeeds in locking itself
// Used by IsCurrentProcessRunning and KillOtherProcess
static bool bLocked = false;

//----------------IsProcessRunning----------------John Mertus----Apr 2002-----

  bool IsCurrentProcessRunning(const char *name)

//  This is used to find if the current process is running
//  name is the name of the process; e.g., EditController
//  Note this works by file locking so require cooperation of the
//  other programs by using this function at startup.
//
//****************************************************************************
{
  return false;
}

//----------------KillOtherProcess----------------John Mertus----Apr 2002-----

  bool KillOtherProcess(const char *name)

//  This kills any other process with name name.
//  Return is true if program is killed or no other progam exists.
//
//  Note:  The other process name must have been created with a call
//  to IsCurrentPrcessRunning
//
//****************************************************************************
{
  return true;
}
