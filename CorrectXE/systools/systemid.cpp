#include "systemid.h"

#ifndef WIN32
#include <stdio.h>
#include <net/if.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <unistd.h>
#endif /* not WIN32 */

#ifdef __sgi
/* can't find the header that has this */
#ifdef __cplusplus
extern "C"
{
#endif
unsigned sysid (unsigned char id[16]);
#ifdef __cplusplus
}
#endif
#endif /* __sgi */

#ifdef WIN32
#include "IPTypes.h"
// Stolen from <ifcons.h>:
#define MIB_IF_TYPE_OTHER               1
#define MIB_IF_TYPE_ETHERNET            6
#define MIB_IF_TYPE_TOKENRING           9
#define MIB_IF_TYPE_FDDI                15
#define MIB_IF_TYPE_PPP                 23
#define MIB_IF_TYPE_LOOPBACK            24
#define MIB_IF_TYPE_SLIP                28

// Stolen from iphlpapi.h
extern "C"
DWORD WINAPI GetAdaptersInfo(
		PIP_ADAPTER_INFO pAdapterInfo, PULONG pOutBufLen
		);

#define MAX_MAC_ADDRS 6
#define MAC_ADDR_SIZE 18  // strlen("XX-XX-XX-XX-XX-XX"+1)
#endif /* WIN32 */


MTI_UINT64 sysid64(void)
{
    /* Get a unique node identifier based on eth0's HW address. */

    MTI_UINT64 ullSysID;

    if (getSysID (&ullSysID, 1) == 0)
     {
      // initialize the return value
      ullSysID =
	(((MTI_UINT64) 0xFF) << 40) +
        (((MTI_UINT64) 0xFF) << 32) +
        (((MTI_UINT64) 0xFF) << 24) +
        (((MTI_UINT64) 0xFF) << 16) +
        (((MTI_UINT64) 0xFF) << 8) +
         ((MTI_UINT64) 0xFF);
     }

    return ullSysID;
}

int getSysID (MTI_UINT64 *ullp, int iMaxSize)
/*
	ullp is a pointer to an unsigned long long array.
	iMaxSize is the maximumn number of entries in ullp

	This function fills system id values into ullp and returns
	the number of entries found.

	This function is useful because some Windows machines have more
	than 1 ethernet card.  From time-to-time, windows changes
	which ethernet card appears first in the list.   Code that simply
	assumes the first ethernet card found is the system id will fail
	when Windows changes the order.
*/
{
  int iRetVal=0;

  if (iMaxSize <= 0)
    return iRetVal;

#ifdef __linux
    unsigned char *pSysID;
    struct ifreq ifr;

    int sd = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
    if (sd < 0) {
        /* socket failed */
        return iRetVal;
    }

    pSysID = NULL;

    if (pSysID == NULL)
     {
      strncpy(ifr.ifr_name, "eth0", IFNAMSIZ);
      if (ioctl(sd, SIOCGIFHWADDR, &ifr) < 0) {
        /* ioctl success */
       pSysID = (unsigned char *) ifr.ifr_hwaddr.sa_data;
      }
    }

    if (pSysID == NULL)
     {
      strncpy(ifr.ifr_name, "nb0", IFNAMSIZ);
      if (ioctl(sd, SIOCGIFHWADDR, &ifr) < 0) {
        /* ioctl success */
       pSysID = (unsigned char *) ifr.ifr_hwaddr.sa_data;
      }
    }


    if (pSysID == NULL)
     {
	/* failed to find address, so return */
       close (sd);
        return iRetVal;
     }

    ullp[0] = (((MTI_UINT64) pSysID[0]) << 40) +
        (((MTI_UINT64) pSysID[1]) << 32) +
        (((MTI_UINT64) pSysID[2]) << 24) +
        (((MTI_UINT64) pSysID[3]) << 16) +
        (((MTI_UINT64) pSysID[4]) << 8) +
        ((MTI_UINT64) pSysID[5]);
    iRetVal = 1;


#ifdef NDEBUG
    printf("sysid: %02x:%02x:%02x:%02x:%02x:%02x (%llu)\n",
           pSysID[0], pSysID[1], pSysID[2], pSysID[3], pSysID[4], pSysID[5],
           ullSysID);
#endif

  close (sd);

#elif defined(__sgi)

    /* just call SGI's sysid() */

    ullp[0] = (MTI_UINT64) sysid(NULL);
    iRetVal = 1;

#elif defined(WIN32)

    PIP_ADAPTER_INFO pAdaptersInfo = NULL, pAdapter = NULL;
    DWORD err = 0, adaptersInfoSize = 0;

    err = GetAdaptersInfo(NULL, &adaptersInfoSize);
    if (err != ERROR_BUFFER_OVERFLOW) {
        return iRetVal;
    }
    pAdaptersInfo = (PIP_ADAPTER_INFO) GlobalAlloc(GPTR, adaptersInfoSize);
    if (pAdaptersInfo == NULL) {
        return iRetVal;
    }
    err = GetAdaptersInfo(pAdaptersInfo, &adaptersInfoSize);
    if (!err) {
	for (pAdapter = pAdaptersInfo; (pAdapter != 0) && (iRetVal < iMaxSize);
		pAdapter = pAdapter->Next) {

	    /* Try to make sure it's a hardware adapter */
	    switch (pAdapter->Type) {
	        case MIB_IF_TYPE_ETHERNET:
	        case MIB_IF_TYPE_FDDI:
		    break;
                default:
                    continue;
	    }
	    /* Screen out oddball lengths and AOL addresses */
	    if (pAdapter->AddressLength != 6 ||
                (pAdapter->Address[0] == 0x00 &&
                 pAdapter->Address[1] == 0x03 &&
		 pAdapter->Address[2] == 0x8A)) {
	        continue;
            }
           ullp[iRetVal] = (((MTI_UINT64) pAdapter->Address[0]) << 40) +
                      (((MTI_UINT64) pAdapter->Address[1]) << 32) +
                      (((MTI_UINT64) pAdapter->Address[2]) << 24) +
                      (((MTI_UINT64) pAdapter->Address[3]) << 16) +
                      (((MTI_UINT64) pAdapter->Address[4]) << 8) +
                       ((MTI_UINT64) pAdapter->Address[5]);
           iRetVal++;

           /***
            printf("MAC ADDRESS IS %02.2X-%02.2X-%02.2X-%02.2X-%02.2X-%02.2X\n",
                     pAdapter->Address[0], pAdapter->Address[1],
                     pAdapter->Address[2], pAdapter->Address[3],
                     pAdapter->Address[4], pAdapter->Address[5]);
           ***/
        }
    }
    GlobalFree(pAdaptersInfo);

#endif /* __linux */

    return iRetVal;
}
