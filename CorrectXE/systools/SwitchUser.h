/*
   File Name:  SwitchUser.h
   Created: March 27, 2002 by John Mertus

*/
#ifndef _SWITCHUSER_H
#define _SWITCHUSER_H

#include "machine.h"
#include "systoolsDLL.h"
//
//  This class allows the program which has the sticky bit set
// to properly switch back and forth between users.
//
// NOTE:  The class is a singleton and has the global name
// theSwitchUser.//
//
// Call
//     theSwitchUser.ToMti()  to change to MTI
//     theSwitchUser.ToUser()) to change to calling user
//     theSwitchUser.ToRoot() to change to root
// Each returns true if this succeeds.
//

class MTI_SYSTOOLSDLL_API CSwitchUser
{
public:
  CSwitchUser() {};         // Null constructors and destructors
  ~CSwitchUser() {};

  bool ToMti(void);     // Switches to the mti user
  bool ToRoot(void);    // Switch to the root user
  bool ToUser(void);    // Switchs to the calling user

private:
  bool SetupPIDs(void);       // Used to initialize the statics
};

//
// This is the singleton to always use
extern MTI_SYSTOOLSDLL_API CSwitchUser theSwitchUser;

//
// Misc functions
//
MTI_SYSTOOLSDLL_API void SetHighPriority(void);
MTI_SYSTOOLSDLL_API void SetRealTimePriority(void);
MTI_SYSTOOLSDLL_API bool IsCurrentProcessRunning(const char *name);
MTI_SYSTOOLSDLL_API bool KillOtherProcess(const char *name);

#endif
