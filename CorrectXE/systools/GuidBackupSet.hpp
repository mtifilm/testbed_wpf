/** @file GuidBackupSet.hpp
 *
 * Interface for CGuidBackupSet.  CGuidBackupSet implements the
 * CBackupSet interface.
 */

#ifndef GUIDBACKUPSET_HPP
#define GUIDBACKUPSET_HPP

#include "BackupSet.hpp"
#include "systoolsDLL.h"
#include <string>

/** @brief CGuidBackupSet implements CBackupSet's interface.
 *
 * The backup set identifier must be representable as a string.  The
 * CGuidBackupSet implementation uses a GUID as the backup identifier.
 *
 * The motivation for implementing the GUID capability in this class
 * rather than in the base class directly is to avoid introducing a
 * dependency within core_code (CBackupSet) on systools (GUID
 * capabilities).
 */
class MTI_SYSTOOLSDLL_API CGuidBackupSet : public CBackupSet
{
public:
    /** Construct a CGuidBackupSet with a new unique backup set identifier. */
    CGuidBackupSet();

    /** Construct a CGuidBackupSet using an existing identifier. */
    CGuidBackupSet(std::string const &aBackupSetString);
};

#endif
