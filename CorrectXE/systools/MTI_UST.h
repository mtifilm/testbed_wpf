//---------------------------------------------------------------------------
// MTI UST utility.
//---------------------------------------------------------------------------
#ifndef _MTI_UST_H
#define _MTI_UST_H

#include "machine.h"

#ifdef __cplusplus
extern "C" {
#endif
#include "systoolsDLL.h"


//---------------------------------------------------------------------------
// Get current UST, as nanoseconds since boot.
// For UST utility to work best, call this at
// least once/second.
MTI_SYSTOOLSDLL_API int mtiGetUST(MTI_INT64 *stamp);

// This is exposed for testing only, do not use
MTI_SYSTOOLSDLL_API int mtiGetUST_old(MTI_INT64 *stamp);

//---------------------------------------------------------------------------
// Convert a "timestamp counter" (aka "pentium
// cpu cycle counter" value to UST.
MTI_SYSTOOLSDLL_API MTI_INT64  mtiConvertToUST(MTI_INT64 counterValue);

//---------------------------------------------------------------------------
#ifdef __cplusplus
}
#endif

#endif

