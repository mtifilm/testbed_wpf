//---------------------------------------------------------------------------

#pragma hdrstop

#include "ExrHeader.h"

#include "err_file.h"
#include "EXRWrapper.h"
#include "HRTimer.h"
#include "pTimecode.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

//---------------------------------------------------------------------------

//struct OpaqueIlmHeaderReleaser {
//	 void operator()(Foo* p) const
//	 {
//		TRACE_3(errout << "RELEASE ILM HEADER @ 0x" << setbase(16) << setw(8) << setfill('0') << (*(unsigned int *) headerPtr));
//		EXR_ReleaseOpaqueHeader(p);
//};
//---------------------------------------------------------------------------

namespace
{
	void _releaseIlmHeader(void *headerPtr)
	{
		TRACE_3(errout << "RELEASE ILM HEADER @ 0x" << std::setbase(16) << std::setw(8) << std::setfill('0') << (*(unsigned int *) headerPtr));
		EXR_ReleaseOpaqueHeader(headerPtr);
	}
}
//////////////////////////////////////////////////////////////////////

int ExrHeader::extractFromFrameBuffer(void *bufferPtr, size_t bufferSize)
{
	int retVal = 0;
	char buffer[1001];
	CHRTimer timer;

	// Tell OpenEXR to use our memory-resident file.
	void *openExrHandle = EXR_OpenInputMemoryFile((char *)bufferPtr, bufferSize, &retVal);
	if (openExrHandle == nullptr)
	{
		return FILE_ERROR_READ;
	}

	double openTime = timer.Read();

	EXR_Width(openExrHandle, &width);
	EXR_Height(openExrHandle, &height);

	// Frame rate
	EXR_FrameRate(openExrHandle, &frameRate);

	// Starting timecode
	EXR_Timecode(openExrHandle, buffer, 1000);
   timecode = string(buffer);

	// Shoot date
	EXR_Date(openExrHandle, buffer, 1000);
	shootDate = string(buffer);

	// Look mod transform function name
	EXR_LookModTransform(openExrHandle, buffer, 1000);
	lookModTransformFunctionName = string(buffer);

	// UUID
	EXR_UUID(openExrHandle, buffer, 1000);
	UUID = string(buffer);

	// Is uncompressed?
	EXR_IsUncompressed(openExrHandle, &isUncompressed);

	// Is RGB or RGBA data?
	EXR_IsRGB(openExrHandle, &isRGB);

	// Number of channels
	EXR_NumChannels(openExrHandle, &numChannels);

	double propTime = timer.Read() - openTime;

	// Grab the opaque header
	void *tempPtr;
	EXR_GetOpaqueHeader(openExrHandle, tempPtr);
	opaqueIlmHeader = std::shared_ptr<void> (tempPtr, [](void * p) { _releaseIlmHeader(p); });
	tempPtr = nullptr;

	double opaqueTime = timer.Read() - openTime - propTime;

//	EXR_DumpHeader(openExrHandle, buffer, 1000);
//	TRACE_3(errout << " [Exr header dump] " << buffer);
//
//	TRACE_3(errout << " [Exr header tostring] " << toString());
	TRACE_3(errout << " [Exr header] " << endl << toString());

	double dumpTime = timer.Read() - openTime - propTime - opaqueTime;

	// Done with the file for now.
	EXR_CloseFile(openExrHandle);
	openExrHandle = nullptr;

	double closeTime = timer.Read() - openTime - propTime - opaqueTime - dumpTime;

	TRACE_3(errout << "GET ILM HEADER took " << timer.Read() << " msec (OP=" << openTime << ", PROP=" << propTime
				<< ", OPQ=" << opaqueTime << ", DMP=" << dumpTime << ", CF=" << closeTime << ")");

	return 0;
}
//---------------------------------------------------------------------------

int ExrHeader::convertToImageFormat(CImageFormat &imageFormat)
{
	auto pixelComponents = (numChannels == 3) ? IF_PIXEL_COMPONENTS_RGB : IF_PIXEL_COMPONENTS_RGBA;
	auto alphaMatteType = (pixelComponents == IF_PIXEL_COMPONENTS_RGBA)
																? IF_ALPHA_MATTE_16_BIT_INTERLEAVED
																: IF_ALPHA_MATTE_NONE;

	imageFormat.setToDefaults(IF_TYPE_FILE_EXR, IF_PIXEL_PACKING_1_Half_IN_2Bytes, pixelComponents, height, width);
	imageFormat.setAlphaMatteType(alphaMatteType);

	return 0;
}
//---------------------------------------------------------------------------

CTimecode ExrHeader::getTimecode()
{
	if (timecode.empty())
	{
		return CTimecode::NOT_SET;
	}

   bool df = false;
   int fps = my_lround(frameRate);

   if (fps <= 0)
   {
      PTimecode mtiTimecode(timecode, 0, 100);
      int hrs = mtiTimecode.getHours();
      int mins = mtiTimecode.getMinutes();
      int secs = mtiTimecode.getSeconds();
      int frms = mtiTimecode.getFrames();
      mtiTimecode.getDefaultFpsAndDropFrame(fps, df);
	   return CTimecode(hrs, mins, secs, frms, (df? DROPFRAME : 0), fps);
   }

   return PTimecode(timecode, df, fps);

}
//---------------------------------------------------------------------------

string ExrHeader::toString()
{
	if (!opaqueIlmHeader)
	{
		return "Error reading header";
	}

	char buffer[10000];
	EXR_DumpHeader(opaqueIlmHeader.get(), buffer, 10000);
	return buffer;
}
//---------------------------------------------------------------------------



