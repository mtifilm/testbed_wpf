//---------------------------------------------------------------------------

#pragma hdrstop

#include "DpxFrameHacker.h"

#include "DpxFrameReader.h"
#include "DpxFrameWriter.h"
#include "DpxHeader.h"
#include "err_file.h"
#include "ImageInfo.h"
#include "Ippheaders.h"
#include "MediaFrameBuffer.h"
#include "MTImalloc.h"

// WTF std::fpclassify not defined
//#include <cmath>
// use this non-standard Microsoft shit instead
#include <float.h>

//---------------------------------------------------------------------------
#pragma package(smart_init)

//---------------------------------------------------------------------------

//DpxFrameHacker::DpxFrameHacker()
//{
//	_inputBuffer = nullptr;
//	_outputBuffer = nullptr;
//}


DpxFrameHacker::~DpxFrameHacker()
{
   ippFree(_inputBuffer);
   ippFree(_outputBuffer);
}
//---------------------------------------------------------------------------

// Can't do this until I figure out how to get the image format to down here.
// For now making stripAlphaChannel() public.
int DpxFrameHacker::hack(const string &filePath, FileHackCommand command)
{
//   if (command == FILE_HACK_DESTROY_ALPHA_MATTE)
//   {
//      return stripAlphaChannel(filePath, imageFormat);
//   }

   return FILE_ERROR_INTERNAL;
}
//---------------------------------------------------------------------------

namespace
{
   size_t getSectorAlignedSize(const string &filename, size_t possiblyUnalignedSize)
   {
      unsigned long bytesPerSector = 0;

      bool success = GetBytesPerSector(const_cast<char *>(filename.c_str()), &bytesPerSector);
      if (!success || bytesPerSector == 0)
      {
         bytesPerSector = 512;
      }

      return bytesPerSector * ((possiblyUnalignedSize + bytesPerSector - 1) / bytesPerSector);
   }
};
//---------------------------------------------------------------------------

int DpxFrameHacker::stripAlphaChannel(const string &filePath, const CImageFormat &inputImageFormat)
{
   CAutoErrorReporter errorReporter("DpxFrameHacker::stripAlphaChannel", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);

   _inputFilePath = filePath;
   _outputFilePath = ReplaceFileExt(filePath, string(".tmp"));

   // This will allocate or enlarge the input buffer as needed.
   int retVal = readRgbaFrameFileFromDisk();
   if (retVal)
   {
      switch (retVal)
      {
         case FILE_ERROR_DPX_FILE_IS_ALREADY_16BIT_RGB:
            retVal = 0; // This file does not need to be converted!
            break;

         case FILE_ERROR_DPX_FILE_IS_NOT_16BIT_RGBA:
            errorReporter.msg << "INTERNAL ERROR: DPX Alpha Stripper: wrong image format: " << filePath;
            break;

         case FILE_ERROR_DPX_HEADER_DECODE_FAILED:
            errorReporter.msg << "ERROR: DPX Alpha Stripper could not decode file " << filePath;
            break;

         default:
            errorReporter.msg << "ERROR: DPX Alpha Stripper could not read file " << filePath;
            break;
      }

      errorReporter.errorCode = retVal;
      return retVal;
   }

   //========================================
   // Allocate output buffer
   auto outputImageFormat = inputImageFormat;
   outputImageFormat.setPixelComponents(IF_PIXEL_COMPONENTS_RGB);
   outputImageFormat.setAlphaMatteType(IF_ALPHA_MATTE_NONE);
   outputImageFormat.setAlphaMatteHiddenType(IF_ALPHA_MATTE_NONE);
   auto imageDataOffset = _dpxHeader.imageElement[0].ulOffsetToData;
   _sizeOfOutputFile = outputImageFormat.getBytesPerField() + imageDataOffset;
// auto rawMediaFrameBufferSharedPtr = std::make_shared<RawMediaFrameBuffer>(outputBufferSize);
// auto outputMediaFrameBufferSharedPtr = MediaFrameBuffer::CreateSharedBuffer(rawMediaFrameBufferSharedPtr, outputImageFormat, false);
// auto _outputBuffer = outputMediaFrameBufferSharedPtr->getImageDataPtr();

   size_t alignedFileSize = getSectorAlignedSize(_outputFilePath, _sizeOfOutputFile);
   if (_outputBuffer == nullptr || alignedFileSize > _sizeOfOutputBuffer)
   {
      ippFree(_outputBuffer);
      _outputBuffer = (unsigned char *) ippMalloc(alignedFileSize);
      _sizeOfOutputBuffer = alignedFileSize;
   }

	if (_outputBuffer == nullptr)
	{
      errorReporter.errorCode = FILE_ERROR_MALLOC;
      errorReporter.msg << "ERROR: OUT OF MEMORY!";
      _sizeOfOutputBuffer = 0;
		return FILE_ERROR_MALLOC;
	}

   //========================================
   // Convert the image data from AC4 to C3
   auto pSrc = (Ipp16u *) (_inputBuffer + imageDataOffset);
   auto srcStep = inputImageFormat.getPixelsPerLine() * 4 * sizeof(Ipp16u);
   auto pDst = (Ipp16u *) (_outputBuffer + imageDataOffset);
   auto dstStep = inputImageFormat.getPixelsPerLine() * 3 * sizeof(Ipp16u);
   IppiSize roiSize = {int(inputImageFormat.getPixelsPerLine()), int(inputImageFormat.getLinesPerFrame())};

   auto ippStatus = ippiCopy_16u_AC4C3R(pSrc, srcStep, pDst, dstStep, roiSize);

   if (ippStatus != 0)
   {
      errorReporter.errorCode = FILE_ERROR_IMAGE_COPY_FAILED;
      errorReporter.msg << "ERROR: DPX Alpha Stripper: Image data copy failed for file " << filePath
                        << "(ippstatus=" << ippStatus << ")";
      return FILE_ERROR_IMAGE_COPY_FAILED;
   }

   //========================================
   // Change two fields in the header and write it to the output buffer.
   _dpxHeader.imageElement[0].ucDescriptor = IF_PIXEL_COMPONENTS_RGB;
   _dpxHeader.ulFileSize = imageDataOffset + outputImageFormat.getBytesPerField();
   _dpxHeader.replaceToFrameBuffer(_outputBuffer, _dpxHeader.ulFileSize);

   //========================================
   // Copy crap between the header and the image data if offset > 2048.
   if (imageDataOffset > DPX_HEADER_SIZE_WITHOUT_USER_DEFINED_CRAP)
   {
      MTImemcpy(
         _outputBuffer + DPX_HEADER_SIZE_WITHOUT_USER_DEFINED_CRAP,
         _inputBuffer + DPX_HEADER_SIZE_WITHOUT_USER_DEFINED_CRAP,
         imageDataOffset - DPX_HEADER_SIZE_WITHOUT_USER_DEFINED_CRAP);
   }

   //========================================
   // Lock the cache for the duration of writing the new frame.
   // Also remove a cached RGBA file if there is one.
   {
	   MediaFrameBufferReadCache readCache;
      MediaFrameBufferReadCache::AutoCacheLocker autoCacheLocker;
      readCache.removeBufferFromCache(filePath);

      //========================================
      // Write the output buffer to the tmp file
      int retval = writeFrameFileToDisk();
      if (retVal != 0)
      {
         errorReporter.errorCode = FILE_ERROR_IMAGE_COPY_FAILED;
         errorReporter.msg << "ERROR: DPX Alpha Stripper could not write temporary file " << _outputFilePath;
         return retVal;
      }

      //========================================
      // Replace the original file safely.
      if (!::MoveFileEx(_outputFilePath.c_str(), _inputFilePath.c_str(), MOVEFILE_REPLACE_EXISTING))
      {
         int errorCode = ::GetLastError();
         string errorMsg = ::GetSystemErrorMessage(errorCode);
         errorReporter.errorCode = FILE_ERROR_IMAGE_COPY_FAILED;
         errorReporter.msg << "ERROR: Alpha Stripper cannot replace " << endl
                           << _inputFilePath << endl
                           << "       with temp file " << endl
                           << _outputFilePath << endl
                           << "Reason: " << errorMsg << "(" << errorCode << ")";
         return FILE_ERROR_FILE_REPLACEMENT_FAILED;
      }
   }

   //========================================
   // DONE!

   return 0;
}
//---------------------------------------------------------------------------

int DpxFrameHacker::getFrameRate(const string &filePath, float &tvFrameRate, float &filmFrameRate)
{
   CAutoErrorReporter errorReporter("DpxFrameHacker::changeFrameRate", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
   constexpr DWORD headerReadLength = 4096;

   //========================================
   // Open the file.
	HANDLE handle = ::CreateFile(filePath.c_str(),
											GENERIC_READ,
											FILE_SHARE_READ | FILE_SHARE_WRITE,
											NULL,
											OPEN_EXISTING,
											FILE_FLAG_NO_BUFFERING,
											NULL);

	if (handle == INVALID_HANDLE_VALUE)
	{
		int errorCode = ::GetLastError();
		string errorMsg = ::GetSystemErrorMessage(errorCode);
      MTIostringstream errorLog;
      errorLog << "ERROR: DpxFrameHacker: CreateFile FAILED:" << endl
               << "    Folder " << GetFilePath(filePath) << endl
               << "    File " << GetFileNameWithExt(filePath) << endl
               << "    Error " << errorCode << " = " << errorMsg;
		TRACE_0(errout << errorLog.str());
		return errorCode + 100000;
	}

   //========================================
   // Check the file size.
	LARGE_INTEGER fileSizeAsLargeInteger;
	if (! ::GetFileSizeEx(handle, &fileSizeAsLargeInteger))
	{
		int errorCode = ::GetLastError();
		string errorMsg = ""; //::GetSystemErrorMessage(errorCode);

		TRACE_0(errout << "ERROR: DPX Header Editor cannot get file size for " << _inputFilePath);
		TRACE_0(errout << "Reason: " << errorMsg << " (" << errorCode << ")");

		::CloseHandle(handle);

		return errorCode + 100000;
	}

	MTIassert(fileSizeAsLargeInteger.QuadPart <= 0x7FFFFFFF);
	_sizeOfInputFile = (size_t)fileSizeAsLargeInteger.QuadPart;

	if (_sizeOfInputFile < headerReadLength)
	{
		TRACE_0(errout << "ERROR: " << _inputFilePath << " is empty!");
		::CloseHandle(handle);
		return FILE_ERROR_DPX_HEADER_DECODE_FAILED;
	}

   //========================================
   // Create the buffer if necessary.
   if (_headerBuffer == nullptr)
   {
      _headerBuffer = (unsigned char *) ippMalloc(headerReadLength);

      if (_headerBuffer == nullptr)
      {
         TRACE_0(errout << "ERROR: OUT OF MEMORY!");
         ::CloseHandle(handle);
         return FILE_ERROR_MALLOC;
      }
   }

   //========================================
   // Read the DPX file header
	int numberOfBytesRead;
	bool success = ::ReadFile(handle,
									 (LPVOID) _headerBuffer,
									 headerReadLength,
									 (LPDWORD) &numberOfBytesRead,
									 0);
	if (!success)
	{
		int errorCode = ::GetLastError();
		string errorMsg = ::GetSystemErrorMessage(errorCode);
      TRACE_0(errout << "ERROR: DPX Header Editor: Header ReadFile FAILED" << endl
                     << "    Folder " << GetFilePath(filePath) << endl
                     << "    File " << GetFileNameWithExt(filePath) << endl
                     << "    Error " << errorCode << ": " << errorMsg);

		::CloseHandle(handle);
		return 100000 + errorCode;
	}

   ::CloseHandle(handle);

   int retVal = _dpxHeader.extractFromFrameBuffer(_headerBuffer, headerReadLength);
   if (retVal != 0)
   {
		::CloseHandle(handle);
      return retVal;
   }

   tvFrameRate = 0.F;
   filmFrameRate = 0.F;

//   if (std::fpclassify(_dpxHeader.fFrameRate) == FP_NORMAL &&
//       std::signbit(_dpxHeader.fFrameRate) == false)
//   {
//      tvFrameRate = _dpxHeader.fFrameRate;
//   }
//
//   if (std::fpclassify(_dpxHeader.fTemporalSamplingRate) == FP_NORMAL &&
//       std::signbit(_dpxHeader.fTemporalSamplingRate) == false)
//   {
//      filmFrameRate = _dpxHeader.fTemporalSamplingRate;
//   }

   if (_fpclass(_dpxHeader.fFrameRate) == _FPCLASS_PN)
   {
      tvFrameRate = _dpxHeader.fFrameRate;
   }

   if (_fpclass(_dpxHeader.fTemporalSamplingRate) == _FPCLASS_PN)
   {
      filmFrameRate = _dpxHeader.fTemporalSamplingRate;
   }

   return 0;
}
//---------------------------------------------------------------------------

int DpxFrameHacker::changeFrameRate(const string &filePath, float newFrameRate)
{
   CAutoErrorReporter errorReporter("DpxFrameHacker::changeFrameRate", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
   constexpr DWORD headerReadLength = 4096;

   //========================================
   // Open the file.
	HANDLE handle = ::CreateFile(filePath.c_str(),
											GENERIC_READ | GENERIC_WRITE,
											FILE_SHARE_READ | FILE_SHARE_WRITE,
											NULL,
											OPEN_EXISTING,
											FILE_FLAG_NO_BUFFERING,
											NULL);

	if (handle == INVALID_HANDLE_VALUE)
	{
		int errorCode = ::GetLastError();
		string errorMsg = ::GetSystemErrorMessage(errorCode);
      MTIostringstream errorLog;
      errorLog << "ERROR: DpxFrameHacker: CreateFile FAILED:" << endl
               << "    Folder " << GetFilePath(filePath) << endl
               << "    File " << GetFileNameWithExt(filePath) << endl
               << "    Error " << errorCode << " = " << errorMsg;
		TRACE_0(errout << errorLog.str());
		return errorCode + 100000;
	}

   //========================================
   // Check the file size.
	LARGE_INTEGER fileSizeAsLargeInteger;
	if (! ::GetFileSizeEx(handle, &fileSizeAsLargeInteger))
	{
		int errorCode = ::GetLastError();
		string errorMsg = ""; //::GetSystemErrorMessage(errorCode);

		TRACE_0(errout << "ERROR: DPX Header Editor cannot get file size for " << _inputFilePath);
		TRACE_0(errout << "Reason: " << errorMsg << " (" << errorCode << ")");

		::CloseHandle(handle);

		return errorCode + 100000;
	}

	MTIassert(fileSizeAsLargeInteger.QuadPart <= 0x7FFFFFFF);
	_sizeOfInputFile = (size_t)fileSizeAsLargeInteger.QuadPart;

	if (_sizeOfInputFile < headerReadLength)
	{
		TRACE_0(errout << "ERROR: " << _inputFilePath << " is empty!");
		::CloseHandle(handle);
		return FILE_ERROR_DPX_HEADER_DECODE_FAILED;
	}

   //========================================
   // Create the buffer if necessary.
   if (_headerBuffer == nullptr)
   {
      _headerBuffer = (unsigned char *) ippMalloc(headerReadLength);

      if (_headerBuffer == nullptr)
      {
         TRACE_0(errout << "ERROR: OUT OF MEMORY!");
         ::CloseHandle(handle);
         return FILE_ERROR_MALLOC;
      }
   }

   //========================================
   // Read the DPX file header
	int numberOfBytesRead;
	bool success = ::ReadFile(handle,
									 (LPVOID) _headerBuffer,
									 headerReadLength,
									 (LPDWORD) &numberOfBytesRead,
									 0);
	if (!success)
	{
		int errorCode = ::GetLastError();
		string errorMsg = ::GetSystemErrorMessage(errorCode);
      TRACE_0(errout << "ERROR: DPX Header Editor: Header ReadFile FAILED" << endl
                     << "    Folder " << GetFilePath(filePath) << endl
                     << "    File " << GetFileNameWithExt(filePath) << endl
                     << "    Error " << errorCode << ": " << errorMsg);

		::CloseHandle(handle);
		return 100000 + errorCode;
	}

   //========================================
   // Fix frame rate if different.
   int retVal = _dpxHeader.extractFromFrameBuffer(_headerBuffer, headerReadLength);
   if (retVal != 0)
   {
		::CloseHandle(handle);
      return retVal;
   }

   if (_dpxHeader.fFrameRate == newFrameRate && _dpxHeader.fTemporalSamplingRate == newFrameRate)
   {
      // Nothing to do!!
		::CloseHandle(handle);
      return 0;
   }

   _dpxHeader.fFrameRate = newFrameRate;
   _dpxHeader.fTemporalSamplingRate = newFrameRate;

   DWORD numberOfBytesActuallyWritten = 0;
   retVal = _dpxHeader.replaceToFrameBuffer(_headerBuffer, headerReadLength);

   //========================================
   // Seek back to start of file.
   LARGE_INTEGER begin;
   begin.QuadPart = (LONGLONG) 0;
   LARGE_INTEGER seekPos;
   seekPos.QuadPart = (LONGLONG) 0;
   success = ::SetFilePointerEx(handle, begin, &seekPos, FILE_BEGIN);
   if (!success)
   {
      DWORD errorCode = ::GetLastError();
      string errorMsg = GetSystemErrorMessage(errorCode);
      TRACE_0(errout << "ERROR: DPX Header Editor: writing hacked header: SEEK TO 0 FAILED" << endl
                     << "    Folder " << GetFilePath(filePath) << endl
                     << "    File " << GetFileNameWithExt(filePath) << endl
                     << "    Ended up at " << seekPos.LowPart << " bytes" << endl
                     << "    Error " << errorCode << ": " << errorMsg);

      ::CloseHandle(handle);
      return 100000 + errorCode;
   }

   //========================================
   // Lock the cache for the duration of writing the new frame.
   // Also remove a cached file if there is one.
   {
	   MediaFrameBufferReadCache readCache;
      MediaFrameBufferReadCache::AutoCacheLocker autoCacheLocker;
      readCache.removeBufferFromCache(filePath);

      //========================================
      // Write the header back out.
      success = ::WriteFile(handle, _headerBuffer, headerReadLength, &numberOfBytesActuallyWritten, NULL);
      if (!success)
      {
         DWORD errorCode = ::GetLastError();
         string errorMsg = GetSystemErrorMessage(errorCode);
         TRACE_0(errout << "ERROR: DPX Header Editor: writing hacked header: WriteFile FAILED:" << endl
                        << "    Folder " << GetFilePath(filePath) << endl
                        << "    File " << GetFileNameWithExt(filePath) << endl
                        << "    Tried to write " << headerReadLength << " bytes" << endl
                        << "    Error " << errorCode << ": " << errorMsg);

         ::CloseHandle(handle);
         return 100000 + errorCode;
      }

      ::CloseHandle(handle);
   }

   return 0;
}
//---------------------------------------------------------------------------

int DpxFrameHacker::readRgbaFrameFileFromDisk()
{
   //========================================
   // Open the file.
	HANDLE handle = ::CreateFile(_inputFilePath.c_str(),
											GENERIC_READ,
											FILE_SHARE_READ | FILE_SHARE_WRITE,
											NULL,
											OPEN_EXISTING,
											FILE_FLAG_NO_BUFFERING,
											NULL);

	if (handle == INVALID_HANDLE_VALUE)
	{
		int errorCode = ::GetLastError();
		string errorMsg = ::GetSystemErrorMessage(errorCode);
		TRACE_0(errout << "ERROR: DPX Alpha Stripper cannot open " << _inputFilePath);
		TRACE_0(errout << "Reason: " << errorMsg << " (" << errorCode << ")");
		return errorCode + 100000;
	}

   //========================================
   // Get the file size.
	LARGE_INTEGER fileSizeAsLargeInteger;
	if (! ::GetFileSizeEx(handle, &fileSizeAsLargeInteger))
	{
		int errorCode = ::GetLastError();
		string errorMsg = ""; //::GetSystemErrorMessage(errorCode);

		TRACE_0(errout << "ERROR: DPX Alpha Stripper cannot get file size for " << _inputFilePath);
		TRACE_0(errout << "Reason: " << errorMsg << " (" << errorCode << ")");

		::CloseHandle(handle);

		return errorCode + 100000;
	}

	MTIassert(fileSizeAsLargeInteger.QuadPart <= 0x7FFFFFFF);
	_sizeOfInputFile = (size_t)fileSizeAsLargeInteger.QuadPart;

   // Create the buffer if necessary.
   constexpr DWORD headerReadLength = 4096;
	if (_sizeOfInputFile < headerReadLength)
	{
		TRACE_0(errout << "ERROR: " << _inputFilePath << " is empty!");
		::CloseHandle(handle);
		return FILE_ERROR_DPX_HEADER_DECODE_FAILED;
	}

   size_t alignedFileSize = getSectorAlignedSize(_inputFilePath, _sizeOfInputFile);
   if (_inputBuffer == nullptr || alignedFileSize > _sizeOfInputBuffer)
   {
      ippFree(_inputBuffer);
      _inputBuffer = (unsigned char *) ippMalloc(alignedFileSize);
      _sizeOfInputBuffer = alignedFileSize;
   }

	if (_inputBuffer == nullptr)
	{
		TRACE_0(errout << "ERROR: OUT OF MEMORY!");
		::CloseHandle(handle);
      _sizeOfInputBuffer = 0;
		return -4;
	}

   //========================================
   // Read the DPX file header
	int numberOfBytesRead;
	bool success = ::ReadFile(handle,
									 (LPVOID) _inputBuffer,
									 headerReadLength,
									 (LPDWORD) &numberOfBytesRead,
									 0);
	if (!success)
	{
		int errorCode = ::GetLastError();
		string errorMsg = ::GetSystemErrorMessage(errorCode);

		TRACE_0(errout << "ERROR: DPX Alpha Stripper: Header ReadFile(" << _inputFilePath << ") FAILED! (" << errorCode << ")");
		TRACE_0(errout << "Reason: " << errorMsg);

		::CloseHandle(handle);

		return 100000 + errorCode;
	}

   //========================================
   // Check DPX file properties
   int retVal = _dpxHeader.extractFromFrameBuffer(_inputBuffer, _sizeOfInputBuffer);
   if (retVal != 0)
   {
		::CloseHandle(handle);
      return retVal;
   }

   if (_dpxHeader.usNumImageElements != 1
   || _dpxHeader.imageElement[0].ucBitSize != 16)
   {
		::CloseHandle(handle);
      return FILE_ERROR_DPX_FILE_IS_NOT_16BIT_RGBA;
   }

   if (_dpxHeader.imageElement[0].ucDescriptor == IF_PIXEL_COMPONENTS_RGB)
   {
      // Alread RGB = nothing to do!
		::CloseHandle(handle);
      return FILE_ERROR_DPX_FILE_IS_ALREADY_16BIT_RGB;
   }

   if (_dpxHeader.imageElement[0].ucDescriptor != IF_PIXEL_COMPONENTS_RGBA)
   {
		::CloseHandle(handle);
      return FILE_ERROR_DPX_FILE_IS_NOT_16BIT_RGBA;
   }

   //========================================
   // Read the rest of the file.
	success = ::ReadFile(handle,
								(LPVOID) (_inputBuffer + headerReadLength),
								(DWORD) alignedFileSize - headerReadLength,
								(LPDWORD) &numberOfBytesRead,
								0);
	if (!success)
	{
		int errorCode = ::GetLastError();
		string errorMsg = ::GetSystemErrorMessage(errorCode);

		TRACE_0(errout << "ERROR: DPX Alpha Stripper: Full frame ReadFile(" << _inputFilePath << ") FAILED! (" << errorCode << ")");
		TRACE_0(errout << "Reason: " << errorMsg);

		::CloseHandle(handle);

		return FILE_ERROR_READ;
	}

	MTIassert((numberOfBytesRead + headerReadLength) >= _sizeOfInputFile);

	::CloseHandle(handle);

	return 0;
}
////---------------------------------------------------------------------------
//
//int DpxFrameHacker::writeFrameFileToDisk()
//{
//   // Open the file.
//	HANDLE handle = ::CreateFile(_outputFilePath.c_str(),
//											GENERIC_WRITE,
//											0,     // No sharing!
//											NULL,
//											CREATE_ALWAYS,
//											FILE_FLAG_NO_BUFFERING,
//											NULL);
//
//	if (handle == INVALID_HANDLE_VALUE)
//	{
//		int errorCode = ::GetLastError();
//		string errorMsg = ::GetSystemErrorMessage(errorCode);
//		TRACE_0(errout << "ERROR: Alpha Stripper cannot crete " << _outputFilePath << " - error code " << errorCode);
//		TRACE_0(errout << "Reason: " << errorMsg);
//		return -1;
//	}
//
//	int numberOfBytesWritten;
//	bool success = ::WriteFile(handle,
//									 (LPVOID) _outputBuffer,
//									 (DWORD) _sizeOfOutputData,
//									 (LPDWORD) &numberOfBytesWritten,
//									 0);
//	if (!success)
//	{
//		int errorCode = ::GetLastError();
//		string errorMsg = ::GetSystemErrorMessage(errorCode);
//
//		MTIostringstream os1;
//		MTIostringstream os2;
//		os1 << "ptr=0x" << right << setfill('0') << setbase(16) << setw(16) << (MTI_UINT64)rawMediaFrameBufferSharedPtr->getBufferPtr();
//		os2 << "size=0x" << right << setfill('0') << setbase(16) << setw(8) << (MTI_UINT32)rawMediaFrameBufferSharedPtr->getBufferSize();
//		TRACE_0(errout << "ERROR: WriteFile(" << _filename << ", " << os1.str() << ", " << os2.str() << ") FAILED! (" << errorCode << ")");
//		TRACE_0(errout << "Reason: " << errorMsg);
//
//		::CloseHandle(handle);
//
//		return -5;
//	}
//
//	MTIassert(numberOfBytesRead >= fileSize);
//
//	::CloseHandle(handle);
//
//	return 0;
//}
//---------------------------------------------------------------------------

int DpxFrameHacker::writeFrameFileToDisk()
{
	int retVal = 0;

	HANDLE handle = ::CreateFile(
		_outputFilePath.c_str(),
		GENERIC_WRITE,
		0,
		NULL,
		CREATE_ALWAYS,  // truncates existing file
		FILE_FLAG_NO_BUFFERING,
		NULL);

	if (handle == INVALID_HANDLE_VALUE)
	{
		DWORD errorCode = ::GetLastError();
		string errorMsg = GetSystemErrorMessage(errorCode);
		TRACE_0(errout << "ERROR: DPX Alpha Stripper: CreateFile for output FAILED:" << endl
		<< "       File " << _outputFilePath << endl
		<< "       Error was: " << errorMsg << " (" << errorCode << ")");

		return FILE_ERROR_OPEN;
	}

	DWORD numberOfBytesActuallyWritten = 0;
	DWORD actualFileSize = (DWORD) _sizeOfOutputFile;
	DWORD roundedSize = (DWORD)getSectorAlignedSize(_outputFilePath, _sizeOfOutputFile);
   MTIassert(roundedSize <= _sizeOfOutputBuffer);

	if (!WriteFile(handle, _outputBuffer, roundedSize, &numberOfBytesActuallyWritten, NULL))
	{
		DWORD errorCode = ::GetLastError();
		string errorMsg = GetSystemErrorMessage(errorCode);
		TRACE_0(errout << "ERROR: DPX Alpha Stripper: writeFrameFiletoDisk: WriteFile FAILED:" << endl
		               << "       File " << _outputFilePath << endl
		               << "       Tried to write " << roundedSize << " bytes" << endl
			            << "       Error was: " << errorMsg << " (" << errorCode << ")");

		retVal = FILE_ERROR_WRITE;
	}

  ::CloseHandle(handle);

	// Fix the EOF if necessary
	if (roundedSize != actualFileSize)
	{
		handle = ::CreateFile(
			_outputFilePath.c_str(),
			GENERIC_WRITE,
			FILE_SHARE_READ | FILE_SHARE_WRITE,    // 0? QQQ
			NULL,
			OPEN_EXISTING,
			0,
			NULL);

		if (handle == INVALID_HANDLE_VALUE)
		{
			DWORD errorCode = ::GetLastError();
			string errorMsg = GetSystemErrorMessage(errorCode);
			TRACE_0(errout << "ERROR: DPX Alpha Stripper: writeFrameFiletoDisk: CreateFile for SEEK FAILED:" << endl
			<< "       File " << _outputFilePath << endl
			<< "       Error was: " << errorMsg << " (" << errorCode << ")");
		}
		else
		{
			LARGE_INTEGER eof;
			eof.QuadPart = (LONGLONG) actualFileSize;
			LARGE_INTEGER seekPos;
			seekPos.QuadPart = (LONGLONG) 0;
			bool success = ::SetFilePointerEx(handle, eof, &seekPos, FILE_BEGIN);
			if (!success)
			{
				DWORD errorCode = ::GetLastError();
				string errorMsg = GetSystemErrorMessage(errorCode);
				TRACE_0(errout << "ERROR: DPX Alpha Stripper: writeFrameFiletoDisk: SEEK TO EOF FAILED" << endl
				<< "       File " << _outputFilePath << endl
				<< "       Tried to change size from " << roundedSize << " bytes to " << actualFileSize << " bytes"  << endl
				<< "       Ended up at " << seekPos.LowPart << " bytes" << endl
				<< "       Error was: " << errorMsg << " (" << errorCode << ")");
			}
			else
			{
				success = ::SetEndOfFile(handle);
				if (!success)
				{
					DWORD errorCode = ::GetLastError();
					string errorMsg = GetSystemErrorMessage(errorCode);
					TRACE_0(errout << "ERROR: DPX Alpha Stripper: writeFrameFiletoDisk: FAILED TO SET CORRECT FILE SIZE" << endl
					<< "       File " << _outputFilePath << endl
					<< "       Tried to change size from " << roundedSize << " bytes to " << actualFileSize << " bytes"  << endl
					<< "       Error was: " << errorMsg << " (" << errorCode << ")");
				}
			}
		}

  		::CloseHandle(handle);

//		MTIassert(success);
	}

  return retVal;
}
//---------------------------------------------------------------------------

