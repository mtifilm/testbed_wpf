//---------------------------------------------------------------------------

#ifndef ExrHeaderCacheH
#define ExrHeaderCacheH
//---------------------------------------------------------------------------

#include "ExrHeader.h"


/////////////////////////////////////////////////////////////
// ExrHeader header cache                                   //
/////////////////////////////////////////////////////////////

#define EXR_HEADER_CACHE_MAX_SIZE 1000

class ExrHeaderCache
{
public:
	void add(const string &strFileName, const ExrHeader &header);
	bool retrieve(const string &strFileName, ExrHeader &header);
	void remove(const string &strFileName);
   void clear();

private:

	struct CacheEntry
	{
		string fileName;
		ExrHeader header;

		CacheEntry(
			const string &fileNameArg,
			const ExrHeader &headerArg)
		: fileName(fileNameArg)
		, header(headerArg)
		{
		};
	};

	deque< CacheEntry > _cache;
	CSpinLock _lock;
};

#endif
