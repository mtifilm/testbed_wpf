//---------------------------------------------------------------------------

#pragma hdrstop

#include "ExrFrameReader.h"

#include "err_file.h"
#include "EXRWrapper.h"
#include "HRTimer.h"
#include "ImageDatumConvert.h"
#include "mthread.h"
#include "TheError.h"
#include "ThreadLocker.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)


ExrHeaderCache ExrFrameReader::_HeaderCache;
//---------------------------------------------------------------------------

ExrFrameReader::ExrFrameReader(const string &filename)
{
	_filename = filename;
}
//---------------------------------------------------------------------------

ExrFrameReader::~ExrFrameReader()
{
}
//---------------------------------------------------------------------------

int ExrFrameReader::decodeImage(MediaFrameBufferSharedPtr frameBufferIn, MediaFrameBufferSharedPtr &frameBufferOut)
{
//	TRACE_0(errout << "START EXR DECODE OF " << _filename);
	CHRTimer timer;

	static bool firstTimeFlag = true;
	if (firstTimeFlag)
	{
		firstTimeFlag = false;
		EXR_SetGlobalThreadCount(8);
	}

	MTIassert(frameBufferIn);
	if (!frameBufferIn)
	{
		return FILE_ERROR_READ;
	}

   auto rawMediaFrameBuffer = frameBufferIn->getRawMediaFrameBuffer();
   MTIassert(rawMediaFrameBuffer);
   if (!rawMediaFrameBuffer)
   {
      return FILE_ERROR_UNEXPECTED_NULL_POINTER;
   }

	MTIassert(!frameBufferIn->isDecoded());
	if (frameBufferIn->isDecoded())
	{
		frameBufferOut = frameBufferIn;
		return 0;
	}

	// Parse the header. Presumably we'll get the file buffer from the cache.
	auto opaqueMetadata = shared_ptr<MediaFrameBuffer::OpaqueMetadata>(new ExrOpaqueMetadata);
	auto exrOpaqueMetadata = dynamic_cast<ExrOpaqueMetadata *>(opaqueMetadata.get());
	MTIassert(exrOpaqueMetadata);
	ExrHeader &header = *exrOpaqueMetadata->getHeaderPtr();
	int retVal = extractHeader(frameBufferIn, header);
	if (retVal != 0)
	{
		TRACE_0(errout << "ERROR: EXR PARSE HEADER FAIL (code " << retVal << " for file " << _filename);
		return FILE_ERROR_EXR_HEADER_DECODE_FAILED;
	}

	double headerTime = timer.Read();

	// Make a shared "cooked" buffer to have OpenEXR unpack the raw data into.
	CImageFormat imageFormat;
	header.convertToImageFormat(imageFormat);
	size_t cookedBufferSize = std::max<size_t>((size_t)imageFormat.getBytesPerField(), rawMediaFrameBuffer->getBufferSize());
	RawMediaFrameBufferSharedPtr cookedFrameBuffer = std::make_shared<RawMediaFrameBuffer>(cookedBufferSize);
	if (!cookedFrameBuffer)
	{
		return FILE_ERROR_MALLOC;
	}

	double rawbufTime = timer.Read() - headerTime;

	// Have OpenEXR convert the data into something we can use.
	void *inputData = frameBufferIn->getBufferPtr();
	void *outputData = cookedFrameBuffer->getBufferPtr();
	char lastErrorMessage[1001];
	lastErrorMessage[0] = '\0';

	// Tell OpenEXR to use our memory-resident file.
	void *openExrHandle = EXR_OpenInputMemoryFile((char *) inputData, (unsigned int) cookedBufferSize, &retVal);
//		TRACE_3(errout << " [XYZZY] " << "EXR_OpenMemoryFile(" << fileSize << ") => " << retVal);
	if (openExrHandle == nullptr)
	{
		return FILE_ERROR_OPEN;
	}

	double openTime = timer.Read() - headerTime - rawbufTime;
	double waitTime;
	double readTime;
	static CThreadLock threadLock;
	{
		CAutoThreadLocker lock(threadLock);

		waitTime = timer.Read() - headerTime - rawbufTime - openTime;

//		TRACE_3(errout << " [XYZZY] " << "EXR_ReadPixels()");

		try
		{
			retVal = EXR_ReadPixels(openExrHandle, (unsigned char *) outputData);
		}
		catch (...)
		{
			retVal = FILE_ERROR_EXR_READ_EXCEPTION;
		}

		readTime = timer.Read() - headerTime - rawbufTime - openTime - waitTime;

		if (retVal != 0)
		{
			EXR_GetLastErrorMessage(openExrHandle, lastErrorMessage, 1000);
		}
	}

	EXR_CloseFile(openExrHandle);
	openExrHandle = nullptr;
	inputData = nullptr;

	double closeTime = timer.Read() - headerTime - rawbufTime - openTime - waitTime - readTime;

	if (retVal != 0)
	{
		TRACE_0(errout << "ERROR: EXR DECODE DATA FAIL (code " << retVal << ") for file " << _filename << " because " << lastErrorMessage);
		theError.set(retVal, lastErrorMessage);
		return FILE_ERROR_READ;
	}

//	TRACE_3(errout << " [XYZZY] " << "ConvertHalfsToImageData() START");

	// Convert to our wacky half format.
	// WE SHOULD DO THIS BETTER - should be done as part of extraction/conversion!  QQQ
	// OR do the unpacking ourself if uncompressed!
//	TRACE_3(errout << " [XYZZY] " << "ConvertHalfsToImageData(" <<
//			(header.width * header.height * header.numChannels * 2) << ")");
	try
	{
		ConvertHalfsToImageData((unsigned short *)outputData, header.width * header.height * header.numChannels);
	}
	catch (...)
	{
		return 10001;
	}

	double convertTime = timer.Read() - headerTime - rawbufTime - openTime - waitTime - readTime - closeTime;

//	TRACE_3(errout << " [XYZZY] " << "ConvertHalfsToImageData() END");

	frameBufferOut = MediaFrameBuffer::CreateSharedBuffer(cookedFrameBuffer, imageFormat, true);
	frameBufferOut->setOpaqueMetadata(opaqueMetadata);
	frameBufferOut->setTimecode(header.getTimecode());

	TRACE_3(errout << " [XYZZY] EXR decode of " << _filename << " took " << timer.Read()
						<< " msecs (HDR=" << headerTime << ", RB=" << rawbufTime
						<< ", OP=" << openTime << ", WT=" << waitTime << ", RP=" << readTime
						<< ", CL=" << closeTime << ", CVT=" << convertTime << ")");

	return 0;
}
//---------------------------------------------------------------------------

int ExrFrameReader::extractHeader(MediaFrameBufferSharedPtr frameBufferIn, ExrHeader &header)
{
	if (!_HeaderCache.retrieve(_filename, header))
	{
		MTIassert(frameBufferIn);
      auto rawMediaFrameBuffer = frameBufferIn->getRawMediaFrameBuffer();
      MTIassert(rawMediaFrameBuffer);
      if (!rawMediaFrameBuffer)
      {
         return FILE_ERROR_UNEXPECTED_NULL_POINTER;
      }

		void *bufferPtr = rawMediaFrameBuffer->getBufferPtr();
		size_t bufferSize = rawMediaFrameBuffer->getBufferSize();
		header.extractFromFrameBuffer(bufferPtr, bufferSize);
		_HeaderCache.add(_filename, header);
	}

	return 0;
}
//---------------------------------------------------------------------------

size_t ExrFrameReader::computeMinBufferSize(const CImageFormat &imageFormat)
{
	// Can't compute anything if the image format isn't set. In that case just
	// return 0 so the actual file size will be used for the buffer.
	int retVal = CImageInfo::isImageFormatTypeValid(imageFormat.getImageFormatType());
	if (retVal != 0)
	{
		return 0;
	}

	// We will return the size of the image data for the format plus an
	// arbitrary 64K for metadata.
	return imageFormat.getBytesPerField() + (64 * 1024);
}
//---------------------------------------------------------------------------

int ExrFrameReader::dumpFileHeader(string &headerDumpString)
{
	ExrHeader header;
	int retVal;
	if (!_HeaderCache.retrieve(_filename, header))
	{
		MediaFrameBufferSharedPtr frameBuffer;
		CImageFormat invalidImageFormat;
		retVal = getFrameBuffer(frameBuffer, invalidImageFormat);
		if (retVal != 0)
		{
			headerDumpString = "Read failed for file " + _filename;
			return retVal;
		}

		retVal = extractHeader(frameBuffer, header);
		if (retVal != 0)
		{
			headerDumpString = "Decode failed for file " + _filename;
			return retVal;
		}
	}

	headerDumpString = header.toString();

	return 0;
}
//---------------------------------------------------------------------------

/* static */ void ExrFrameReader::RemoveFromHeaderCache(const string &filename)
{
	_HeaderCache.remove(filename);
}
//---------------------------------------------------------------------------

/* static */ void ExrFrameReader::ClearHeaderCache()
{
	_HeaderCache.clear();
}
//---------------------------------------------------------------------------
