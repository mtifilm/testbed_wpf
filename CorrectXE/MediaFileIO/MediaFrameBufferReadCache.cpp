//---------------------------------------------------------------------------

#include "MediaFrameBufferReadCache.h"
//---------------------------------------------------------------------------

#include "DllSupport.h"
#include "err_file.h"
#include "HRTimer.h"
#include "MTImalloc.h"
#include "SysInfo.h"
#include <stddef.h>
//---------------------------------------------------------------------------

MediaFrameBufferReadCache::cacheListType MediaFrameBufferReadCache::_cacheList;
size_t MediaFrameBufferReadCache::_totalPagesAllocated = 0;
size_t MediaFrameBufferReadCache::_cachePageAllowance = 0;
size_t MediaFrameBufferReadCache::_totalPagesInUse = 0;
size_t MediaFrameBufferReadCache::_pageSizeInBytes = SysInfo::GetSystemPageSize();
size_t MediaFrameBufferReadCache::_originalMinWorkingSetSize = 0;
size_t MediaFrameBufferReadCache::_originalMaxWorkingSetSize = 0;
int MediaFrameBufferReadCache::_serialNumber = 0;
bool MediaFrameBufferReadCache::_enabledByUser = false;
CSpinLock MediaFrameBufferReadCache::_cacheLock;

inline string pages2gbString(size_t pages)
{
   const size_t oneGB = 1024 *1024 * 1024;
   size_t bytes = pages * SysInfo::GetSystemPageSize();
   int gb = bytes / oneGB;
   int millions = (bytes % oneGB) / (1024 * 1024);
   MTIostringstream os;
   os << gb << "." << std::setw(3) << std::setfill('0') << millions;
   return os.str();
}

/////////////////////////////////////////////////////////////
// Non-denominational image body read cache                               //
/////////////////////////////////////////////////////////////

int MediaFrameBufferReadCache::setSizeInPages(size_t numberOfPages, /* out */ size_t *actualNumberOfPagesAllocated)
{
	CAutoSpinLocker lock(_cacheLock);
   const size_t oneGigabyte = 1024 * 1024 * 1024;

   SET_CBHOOK(onRanOutOfBufferMemory, RawMediaFrameBuffer::RanOutOfLockableMemory);

	// Save original working set sizes to restore them.
	if (_originalMinWorkingSetSize == 0)
	{
		if (!::GetProcessWorkingSetSize(::GetCurrentProcess(), &_originalMinWorkingSetSize, &_originalMaxWorkingSetSize))
		{
			string errorMessage = GetSystemMessage(GetLastError());
			TRACE_0(errout << "ERROR: GetProcessWorkingSetSize() failed because: " << errorMessage);
			return 0;
		}

		TRACE_3(errout << "[" << _threadid << "] READ CACHE: OLD WORKING SET SIZE: min="
                     << _originalMinWorkingSetSize << ", max=" << _originalMaxWorkingSetSize);
	}

   // Setting size to zero means that the user's preference is "Cache Disabled"
   // but proper operation of the frame buffer stuff requires some caching so we set
   // a minimum of 2 GB for that purpose (after clearing it!).
   _enabledByUser = numberOfPages > 0;
   if (!_enabledByUser)
   {
      clearCacheInternal();
   }

	size_t minCacheSizeInPages = (2 * oneGigabyte) / SysInfo::GetSystemPageSize();
	numberOfPages = std::max<size_t>(numberOfPages, minCacheSizeInPages);

	// In case SetProcessWorkingSetSize() fails.
	auto old_totalPagesAllocated = _totalPagesAllocated;
	auto old_cachePageAllowance = _cachePageAllowance;

	// If we are shrinking the working set size, we need to make sure our
	// locked pages don't exceed that size. I arbitrarily pick 256 MB as
	// a "safe" amount of headroom to make sure setting the working set
	// size doesn't fail.
	size_t quarterGBInPages = (oneGigabyte / 4) / _pageSizeInBytes;
   size_t freeListPageAllowance = numberOfPages / 10;
   _cachePageAllowance = numberOfPages - freeListPageAllowance - quarterGBInPages;
	_totalPagesAllocated = numberOfPages;
	trimCacheSize(_cachePageAllowance);

   _cachePageAllowance = numberOfPages - freeListPageAllowance;
   RawMediaFrameBuffer::SetLockableMemorySizes(pages2bytes(numberOfPages), pages2bytes(freeListPageAllowance));
  	if (actualNumberOfPagesAllocated != nullptr)
	{
		*actualNumberOfPagesAllocated = numberOfPages;
	}

   // Add two GB fudge factor for tool buffers and maybe file i/o.
	size_t newMinWorkingSetSize = _originalMinWorkingSetSize + (2 * oneGigabyte) + (numberOfPages * _pageSizeInBytes);
	size_t newMaxWorkingSetSize = _originalMaxWorkingSetSize + (2 * oneGigabyte) + (numberOfPages * _pageSizeInBytes);
	if (!::SetProcessWorkingSetSize(::GetCurrentProcess(), newMinWorkingSetSize, newMaxWorkingSetSize))
	{
		int errorCode = GetLastError();
		string errorMessage = GetSystemMessage(errorCode);
		TRACE_0(errout << "ERROR: SetProcessWorkingSetSize(" << newMinWorkingSetSize << ", "
							<< newMaxWorkingSetSize << ") failed because: " << errorMessage << " (" << errorCode << ")");

		// If we are reducing the size of the cache, try again after clearing all
		// the locked pages. Also wait 5 seconds in case the OS is busy cleaning page tables from a previous run.
      MTImillisleep(5000);
		bool recovered = false;
		if (numberOfPages < old_totalPagesAllocated)
		{
			clearCacheInternal();
			recovered = ::SetProcessWorkingSetSize(::GetCurrentProcess(), newMinWorkingSetSize, newMaxWorkingSetSize);
			if (!recovered)
			{
				// No dice!
				errorCode = GetLastError();
				string errorMessage = GetSystemMessage(errorCode);
				TRACE_0(errout << "ERROR: RETRY SetProcessWorkingSetSize(" << newMinWorkingSetSize << ", "
									<< newMaxWorkingSetSize << ") after clear failed because: " << errorMessage << " (" << errorCode << ")");
			}
		}

		if (!recovered)
		{
			_totalPagesAllocated = old_totalPagesAllocated;
	      _cachePageAllowance = old_cachePageAllowance;
         freeListPageAllowance = _totalPagesAllocated - _cachePageAllowance;
         RawMediaFrameBuffer::SetLockableMemorySizes(pages2bytes(_totalPagesAllocated), pages2bytes(freeListPageAllowance));
			if (actualNumberOfPagesAllocated != nullptr)
			{
				*actualNumberOfPagesAllocated = _totalPagesAllocated;
			}

			return FILE_ERROR_INSUFFICIENT_PHYS_MEM;
		}
	}

	TRACE_3(errout << "[" << _threadid << "] READ CACHE: NEW WORKING SET SIZE: min=" << newMinWorkingSetSize << ", max=" << newMaxWorkingSetSize);
	TRACE_3(errout << "[" << _threadid << "] READ CACHE: inited, pages=" << numberOfPages);

	return 0;
}
// -------------------------------------------------------------------------

size_t MediaFrameBufferReadCache::getMediaFrameBufferCacheSizeInBytes()
{
	return _totalPagesAllocated * _pageSizeInBytes;
}
// -------------------------------------------------------------------------

void MediaFrameBufferReadCache::addBufferToCache(const string &filename, int frameIndex, MediaFrameBufferSharedPtr buffer)
{
	CAutoSpinLocker lock(_cacheLock);

	MTIassert(!filename.empty());
	if (filename.empty())
	{
      return;
	}

	CHRTimer timer;

	// Make sure cache was properly initialized
	if (_totalPagesAllocated == 0)
	{
		TRACE_3(errout << "[" << _threadid << "] READ CACHE WAS NOT INITIALIZED!");
		return;
	}

	// Ensure no duplicates!
	removeBufferFromCacheInternal(filename);

   // Do NOT store any buffers with errors or ones without shared raw buffers
   // or ones with zero size!
   bool cantCache = false;
   if (buffer->getErrorInfo().code != 0)
   {
      TRACE_0(errout << "INTERNAL ERROR: tried to cache buffer with an error!");
      cantCache = true;
   }

   auto rawMediaFrameBuffer = buffer->getRawMediaFrameBuffer();
   if (!rawMediaFrameBuffer)
   {
      TRACE_0(errout << "INTERNAL ERROR: tried to cache wrong type of media buffer!");
      cantCache = true;
   }

   if (buffer->getImageDataSize() == 0)
   {
      TRACE_0(errout << "INTERNAL ERROR: tried to cache empty media buffer!");
      cantCache = true;
   }

   MTIassert(!cantCache);
   if (cantCache)
   {
      return;
   }

	// Put it on the front of the queue.
	_cacheList.emplace_front(filename, buffer);

	// Bookkeeping.
	_totalPagesInUse += bytes2pages(buffer->getCacheFootprint());
	++_serialNumber;

   int bufferId = buffer->getRawMediaFrameBuffer() ? buffer->getRawMediaFrameBuffer()->getBufferId() : -1;
   MTIassert(bufferId != -1);

	TRACE_3(errout << "[" << _threadid << "] READ CACHE: ADDED: " << filename
                  << ", size=" << buffer->getImageDataSize()
                  << ", id=" << bufferId
                  << " (" << int(timer.Read() + 0.5) << " msecs)");
}
// -------------------------------------------------------------------------

MediaFrameBufferSharedPtr MediaFrameBufferReadCache::retrieveBufferFromCache(const string &filename)
{
	CAutoSpinLocker lock(_cacheLock);
	CHRTimer timer;

	// Find the entry for this file in the cache
	cacheListIteratorType iter = find(filename);
	if (iter == _cacheList.end())
	{
		return nullptr;
	}

	// Found a cached body for the file. Copy the shared pointer before moving
	// it to the front of the cache list.
	MediaFrameBufferSharedPtr retrievedBuffer(iter->second);

	// Bump the cache entry to the front of the cache (MRU position)
	_cacheList.erase(iter);
	_cacheList.emplace_front(filename, retrievedBuffer);

   int bufferId = retrievedBuffer->getRawMediaFrameBuffer()
                        ? retrievedBuffer->getRawMediaFrameBuffer()->getBufferId()
                        : -1;
   MTIassert(bufferId != -1);

	TRACE_3(errout << "[" << _threadid << "] READ CACHE: RETRIEVED: " << filename
                  << ", size=" << retrievedBuffer->getImageDataSize()
                  << ", id=" << bufferId
                  << " (" << timer.Read() << " msec)");

	return retrievedBuffer;
}
// -------------------------------------------------------------------------

void MediaFrameBufferReadCache::removeBufferFromCache(const string &filename)
{
	CAutoSpinLocker lock(_cacheLock);
   removeBufferFromCacheInternal(filename);
}
// -------------------------------------------------------------------------
// QQQ IT SEEMS WE SHOULD ALSO BE REPLACING THE FRAME NUMBER HERE!!!

void MediaFrameBufferReadCache::reKeyBufferInCache(const string &strFileNameFrom, const string &strFileNameTo)
{
	CAutoSpinLocker lock(_cacheLock);

	// Find the entry for the file in the cache
	cacheListIteratorType iter = find(strFileNameFrom);
	if (iter == _cacheList.end())
	{
		TRACE_3(errout << "[" << _threadid << "] READ CACHE: REKEY BUFFER FAILED, buffer wasn't found for: " << strFileNameFrom);
		return;
	}

	// Make a new cache entry at the front of the list with the new name
	// and delete the old entry. Careful, push_front invalidates the iterator!
	MediaFrameBufferSharedPtr temp = iter->second;
	_cacheList.erase(iter);
   _cacheList.emplace_front(strFileNameTo, temp);

	TRACE_3(errout << "[" << _threadid << "] READ CACHE: REKEY BUFFER: " << strFileNameFrom << " to " << strFileNameTo);

	// Bookkeeping.
	++_serialNumber;
}
// -------------------------------------------------------------------------

void MediaFrameBufferReadCache::clearCache()
{
	CAutoSpinLocker lock(_cacheLock);
	clearCacheInternal();
}
// -------------------------------------------------------------------------

int MediaFrameBufferReadCache::getSerialNumber()
{
	return _serialNumber;
}
// -------------------------------------------------------------------------

void MediaFrameBufferReadCache::lockCacheFramesInMemory(bool flag)
{
   if (flag == RawMediaFrameBuffer::GetReallyLockPagesInMemory())
   {
      return;
   }

	CAutoSpinLocker lock(_cacheLock);
	clearCacheInternal();
	RawMediaFrameBuffer::SetReallyLockPagesInMemory(flag);
}
// -------------------------------------------------------------------------

size_t MediaFrameBufferReadCache::getPageSize()
{
	return _pageSizeInBytes;
}
// -------------------------------------------------------------------------

bool MediaFrameBufferReadCache::isCachingEnabledByUser()
{
	return _enabledByUser;
}
// -------------------------------------------------------------------------

void MediaFrameBufferReadCache::onRanOutOfBufferMemory(void *Sender)
{
	CAutoSpinLocker lock(_cacheLock);

   TRACE_3(errout << "[" << _threadid << "] READ CACHE was notified that we ran out of buffer memory!");

   // Sanity check.
   MTIassert(!_cacheList.empty());
   if (_cacheList.empty())
   {
      return;
   }

   // Release oldest entry.
   removeBufferFromCacheInternal(_cacheList.end() - 1);
}

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// ---------------------- P R I V A T E S ----------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------

void MediaFrameBufferReadCache::removeBufferFromCacheInternal(const string &filename)
{
	// CACHE MUST HAVE BEEN LOCKED BY CALLER.

	// Find the entry for the file in the cache
	cacheListIteratorType iter = find(filename);
	if (iter == _cacheList.end())
	{
		return;
	}

	// Found a cached body for the file - unlock and free the memory.
	removeBufferFromCacheInternal(iter);
}
// -------------------------------------------------------------------------

void MediaFrameBufferReadCache::removeBufferFromCacheInternal(cacheListIteratorType iter)
{
	// CACHE MUST HAVE BEEN LOCKED BY CALLER.

   auto filename = iter->first;
	size_t footprint  = iter->second->getCacheFootprint();
	CHRTimer timer;

	MTIassert(footprint > 0);

	// Nuke the cache entry.
   MTIassert(iter->second->getRawMediaFrameBuffer());
   if (iter->second->getRawMediaFrameBuffer())
   {
      TRACE_3(errout << "[" << _threadid << "] READ CACHE: remove MFB with raw buffer "
                     << iter->second->getRawMediaFrameBuffer()->getBufferId()
                     << ", frame buffer use count = " << iter->second.use_count()
                     << ", raw buffer use count = " << iter->second->getRawMediaFrameBuffer().use_count());
   }

	_cacheList.erase(iter);
	TRACE_3(errout << "[" << _threadid << "] READ CACHE: AFTER _cacheList.erase(iter)");

	// Bookkeeping.
	_totalPagesInUse = _cacheList.empty()
							? 0
							: (_totalPagesInUse - std::min<size_t>(_totalPagesInUse, bytes2pages(footprint)));
	++_serialNumber;

	TRACE_3(errout << "[" << _threadid << "] READ CACHE: REMOVED BUFFER " << filename
                  << " (" << timer.Read() << " msec)");
	TRACE_3(errout << "[" << _threadid << "] READ CACHE: In use: " << pages2gbString(_totalPagesInUse)
                  << " / " << pages2gbString(_cachePageAllowance) << " GB in "
                  << _cacheList.size() << " buffers");
}
// -------------------------------------------------------------------------

void MediaFrameBufferReadCache::clearCacheInternal()
{
	// CACHE MUST HAVE BEEN LOCKED BY CALLER.

	CHRTimer timer;

	// Remove all the cache entries.
	_cacheList.clear();

	// Bookkeeping.
	_totalPagesInUse = 0;
	++_serialNumber;

	TRACE_3(errout << "[" << _threadid << "] READ CACHE: CLEARED! (" << timer.ReadAsString() << " msec)");
}
// -------------------------------------------------------------------------

MediaFrameBufferReadCache::cacheListIteratorType MediaFrameBufferReadCache::find(const string &filename)
{
	// CACHE MUST HAVE BEEN LOCKED BY CALLER.

   // Short circuit - never match if string is empty
   if (filename.empty())
   {
      return _cacheList.end();
   }

	// Find the entry for the file in the cache
	cacheListIteratorType iter;
	for (iter = _cacheList.begin(); iter != _cacheList.end() && iter->first != filename; ++iter)
   {
      // Do nothing else;
   }

   return iter;
}
// -------------------------------------------------------------------------

void MediaFrameBufferReadCache::trimCacheSize(size_t pages)
{
	// CACHE MUST HAVE BEEN LOCKED BY CALLER.

	if (_cacheList.empty())
	{
		// Nothing we can do!
		return;
	}

	string filename = _cacheList.back().first;
	MTIassert(!filename.empty());

	while(_totalPagesInUse > pages && !_cacheList.empty())
	{
	   TRACE_3(errout << "[" << _threadid << "] READ CACHE trimming: " << _cacheList.size() << " buffers using "
                     << pages2gbString(_totalPagesInUse) << " / " << pages2gbString(_cachePageAllowance) << " bytes");
		cacheListIteratorType oldestEntry = _cacheList.end() - 1;
		removeBufferFromCacheInternal(oldestEntry);
	}
}
// -------------------------------------------------------------------------


