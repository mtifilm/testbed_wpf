//---------------------------------------------------------------------------

#pragma hdrstop

#include "DpxFrameReader.h"

#include "HRTimer.h"
#include "MTImalloc.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

//---------------------------------------------------------------------------

#pragma hdrstop

#include "DpxFrameReader.h"

#include "err_file.h"
#include "ImageDatumConvert.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------

//DpxHeaderCache DpxFrameReader::_HeaderCache;
//---------------------------------------------------------------------------

DpxFrameReader::DpxFrameReader(const string &filename)
{
	_filename = filename;
}
//---------------------------------------------------------------------------

DpxFrameReader::~DpxFrameReader()
{
}
//---------------------------------------------------------------------------

//int DpxFrameReader::extractImageFormat(MediaFrameBufferSharedPtr frameBufferIn, CImageFormat &imageFormatOut)
//{
//	return -1;
//}
//---------------------------------------------------------------------------

int DpxFrameReader::decodeImage(MediaFrameBufferSharedPtr frameBufferIn, MediaFrameBufferSharedPtr &frameBufferOut)
{
	CHRTimer timer;

	MTIassert(frameBufferIn);
	if (!frameBufferIn)
	{
		return FILE_ERROR_READ;
	}

   auto rawMediaFrameBufferIn = frameBufferIn->getRawMediaFrameBuffer();
	MTIassert(rawMediaFrameBufferIn);
	if (!rawMediaFrameBufferIn)
	{
		return FILE_ERROR_INTERNAL;
	}

	// Already decoded?
	if (frameBufferIn->isDecoded())
	{
		frameBufferOut = frameBufferIn;
		return 0;
	}

	// Parse the header.
	DpxHeader header;
	int retVal = extractHeader(frameBufferIn, header);
	if (retVal != 0)
	{
		TRACE_0(errout << "ERROR: DPX PARSE HEADER FAIL (code " << retVal << " for file " << _filename);
		return FILE_ERROR_DPX_HEADER_DECODE_FAILED;
	}

	// Convert to image format.
	CImageFormat imageFormat;
	header.convertToImageFormat(imageFormat);
	EPixelPacking pixelPacking = imageFormat.getPixelPacking();

	// If there is a separate alpha image and it doesn't start immediately
	// after the primary image, we have some work to do.
	auto alphaType = imageFormat.getAlphaMatteType();
	bool hasSeparateAlpha = alphaType == IF_ALPHA_MATTE_1_BIT
									|| alphaType == IF_ALPHA_MATTE_8_BIT
									|| alphaType == IF_ALPHA_MATTE_16_BIT;
	size_t alphaSrcOffset = 0;
	size_t alphaDstOffset = 0;
	if (hasSeparateAlpha)
	{
		MTIassert(header.usNumImageElements > 1);
		MTIassert(imageFormat.getPixelComponents() != IF_PIXEL_COMPONENTS_Y)
		if (header.usNumImageElements > 1 && imageFormat.getPixelComponents() != IF_PIXEL_COMPONENTS_Y)
		{
			alphaSrcOffset = header.imageElement[1].ulOffsetToData;
			alphaDstOffset = header.imageElement[0].ulOffsetToData + imageFormat.getBytesPerFieldExcAlpha();
		}
		else
		{
			hasSeparateAlpha = false;
      }
	}

	// DECEIVING: The header being swapped means the data is big-endian, so swap if we want to
	// end up as little-endian. Always swap 16-bit data as 2-byte entities.
	bool needToSwap16 = header.isFileSwapped() && (pixelPacking == IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE);

	// Nasty, nasty, nasty, nasty hack! These represent monochrome data
	// with source packing 3_10Bits_IN_4Bytes_L and 3_10Bits_IN_4Bytes_R,
	// respectively, which the Y->YYY expander wants to receive in
	// little-endian ordering. Bleah!
	bool needToSwap32 = (header.isFileSwapped()
							  && (pixelPacking == IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L
								  || pixelPacking == IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R));

	// These are the "normal" swapping cases. The extractors expect big-endian
	// input so if the file is little endian we do a data swap.
	// QQQ someday eliminate this by implementing the missing extractors and
	// line engines!
	if (!header.isFileSwapped())
	{
		switch (pixelPacking)
		{
		// Anything that's going to end up as two bytes wants to stay little-endian!
		case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L:
		case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R:
		case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_LE:
		case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_BE:
		case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
			break;

		default:
		case IF_PIXEL_PACKING_INVALID:
		case IF_PIXEL_PACKING_1_Half_IN_2Bytes:
		case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes:
		case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:
		case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVSa:
		case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes_LR:
		case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_INT: // INTERNAL FORMAY ONLY
		case IF_PIXEL_PACKING_1_8Bits_IN_2Bytes:
		case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes:
		case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE: // always byte-swapped to LE now!!
			// ERROR - we don't expect these packings in DPX files
			MTIassert(false);
			/* FALL THROUGH */

			// Anything not marked _LE or _BE needs to be byte-swapped!
			// Always byte-swap as 32-bit entities!!
		case IF_PIXEL_PACKING_8Bits_IN_1Byte:
		case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
		case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R:
		case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes: // ?? Your guess is as good as mine!
		case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes: // ?? Your guess is as good as mine!
			needToSwap32 = true;
			break;
		}
   }

//	bool needToConvertYtoYYY = false; //imageFormat.getPixelComponents() == IF_PIXEL_COMPONENTS_Y;
   bool needToFlipItUpsideDown = header.usImageOrientation == 2;
   bool canShareInputRawBuffer = !needToSwap16 && !needToSwap32 && !needToFlipItUpsideDown //  && !needToConvertYtoYYY
                                 && (!hasSeparateAlpha || (alphaSrcOffset == alphaDstOffset));

	if (canShareInputRawBuffer)
	{
		frameBufferOut = MediaFrameBuffer::CreateSharedBuffer(rawMediaFrameBufferIn, imageFormat, true);

      if (frameBufferOut->getErrorInfo().code != 0)
      {
         return frameBufferOut->getErrorInfo().code;
      }

		if (!frameBufferOut)
		{
			return FILE_ERROR_MALLOC;
		}

		frameBufferOut->setImageDataOffset(header.imageElement[0].ulOffsetToData);
		frameBufferOut->setHasSeparateAlpha(hasSeparateAlpha);
		frameBufferOut->setAlphaDataOffset(alphaSrcOffset);
		frameBufferOut->setTimecode(header.getTimecode());

		TRACE_3(errout << "::::::::::::::: DECODED frame " << _filename << " in " << timer.Read()
							<< " msec (no swap or y->yyy needed) :::::::::::::::");

		return 0;
	}

	// Make a shared "cooked" buffer to swap the raw data into.
	size_t fileSize = header.ulFileSize;

	// FOR PERFORMANCE REASONS, USE LARGER OF IN BUFFER SIZE vs DESIRED SIZE!
	size_t nBytes = (size_t) std::max<size_t>(
										(size_t)imageFormat.getBytesPerField(),
										rawMediaFrameBufferIn->getBufferSize());
//	DBTRACE(nBytes);
	RawMediaFrameBufferSharedPtr cookedFrameBuffer = std::make_shared<RawMediaFrameBuffer>(nBytes);
	if (!cookedFrameBuffer)
	{
		return FILE_ERROR_MALLOC;
	}

	double swapTime = 0.0;
	double yyyTime = 0.0;

   MTI_UINT8 *src = (MTI_UINT8 *) frameBufferIn->getBufferPtr();
   size_t imageSrcOffset = header.imageElement[0].ulOffsetToData;
   MTI_UINT8 *dst = (MTI_UINT8 *) cookedFrameBuffer->getBufferPtr();
   size_t imageDstOffset = 0;
	if (hasSeparateAlpha)
	{
      // QQQ - for Y->YYY case, seems to be swapping YYY instead of just Y here!
		size_t imageSize = imageFormat.getBytesPerFieldExcAlpha();
		alphaDstOffset = imageDstOffset + imageSize;
		size_t alphaSize = imageFormat.getAlphaBytesPerField();

      if (needToFlipItUpsideDown == false) // || needToConvertYtoYYY == true)
      {
		   // No flipping, or flipping but also converting Y to YYY, so just swap image data.
         if (needToSwap16)
         {
            SwapINT16((MTI_UINT16*) (src + imageSrcOffset), (MTI_UINT16*) (dst + imageDstOffset), imageSize / sizeof(MTI_UINT16));
         }
         else if (needToSwap32)
         {
            SwapINT32((MTI_UINT32*) (src + imageSrcOffset), (MTI_UINT32*) (dst + imageDstOffset), imageSize / sizeof(MTI_UINT32));
         }
         else
         {
            MTImemcpy(dst + imageDstOffset, src + imageSrcOffset, imageSize);
         }

         // ONLY 16-BIT MATTES GET SWAPPED! 1-bit and 8-bit mattes just get copied.
         if ((alphaType == IF_ALPHA_MATTE_16_BIT) && (needToSwap16 || needToSwap32))
         {
            // Always swap 16!
            SwapINT16((MTI_UINT16*) (src + alphaSrcOffset), (MTI_UINT16*) (dst + alphaDstOffset), alphaSize / sizeof(MTI_UINT16));
         }
         else
         {
            MTImemcpy(dst + alphaDstOffset, src + alphaSrcOffset, alphaSize);
         }

         src = dst;
         imageSrcOffset = imageDstOffset;
      }
      else
      {
         int nRows = imageFormat.getLinesPerFrame();
         MTIassert((imageSize % nRows) == 0);
         MTIassert((alphaSize % nRows) == 0);
         int imagePitchInBytes = imageSize / nRows;
         int alphaPitchInBytes = alphaSize / nRows;
         MTI_UINT8 *imageSrcRowStart = src + imageSrcOffset + ((nRows - 1) * imagePitchInBytes);
         MTI_UINT8 *imageDstRowStart = dst + imageDstOffset;
         MTI_UINT8 *alphaSrcRowStart = src + alphaSrcOffset + ((nRows - 1) * alphaPitchInBytes);
         MTI_UINT8 *alphaDstRowStart = dst + alphaDstOffset;
         for (int row = 0; row < nRows; ++row)
         {
            if (needToSwap16)
            {
               int imagePitchInShorts = imagePitchInBytes / sizeof(MTI_UINT16);
               SwapINT16(reinterpret_cast<MTI_UINT16 *>(imageSrcRowStart),
                         reinterpret_cast<MTI_UINT16 *>(imageDstRowStart),
                         imagePitchInShorts);
            }
            else if (needToSwap32)
            {
               int imagePitchInLongs = imagePitchInBytes / sizeof(MTI_UINT32);
               SwapINT32(reinterpret_cast<MTI_UINT32 *>(imageSrcRowStart),
                         reinterpret_cast<MTI_UINT32 *>(imageDstRowStart),
                         imagePitchInLongs);
            }
            else
            {
               MTImemcpy(imageDstRowStart, imageSrcRowStart, imagePitchInBytes);
            }

            // ONLY 16-BIT MATTES GET SWAPPED! 1-bit and 8-bit mattes just get copied.
            if ((alphaType == IF_ALPHA_MATTE_16_BIT) && (needToSwap16 || needToSwap32))
            {
               // Always swap 16!
               int alphaPitchInShorts = alphaPitchInBytes / sizeof(MTI_UINT16);;
               SwapINT16(reinterpret_cast<MTI_UINT16 *>(alphaSrcRowStart),
                         reinterpret_cast<MTI_UINT16 *>(alphaDstRowStart),
                         alphaPitchInShorts);
            }
            else
            {
               MTImemcpy(alphaDstRowStart, alphaSrcRowStart, alphaPitchInBytes);
            }

            imageSrcRowStart -= imagePitchInBytes;
            imageDstRowStart += imagePitchInBytes;
            alphaSrcRowStart -= alphaPitchInBytes;
            alphaDstRowStart += alphaPitchInBytes;
         }

         src = dst;
         imageSrcOffset = imageDstOffset;
         needToFlipItUpsideDown = false;;
      }
	}
	else // No alpha, or alpha is embedded (RGBA)
	{
		size_t srcDataSize = frameBufferIn->getImageDataSize() - imageSrcOffset;
      if (needToFlipItUpsideDown == false) // || needToConvertYtoYYY == true)
      {
         if (needToSwap16)
         {
            // Swap first, then maybe expand.
            SwapINT16((MTI_UINT16*) (src + imageSrcOffset), (MTI_UINT16*) (dst + imageDstOffset), srcDataSize / sizeof(MTI_UINT16));
            src = dst;
            imageSrcOffset = imageDstOffset;
         }
         else if (needToSwap32)
         {
            // Swap first, then maybe expand.
            SwapINT32((MTI_UINT32*) (src + imageSrcOffset), (MTI_UINT32*) (dst + imageDstOffset), srcDataSize / sizeof(MTI_UINT32));
            src = dst;
            imageSrcOffset = imageDstOffset;
         }
         else
         {
            MTImemcpy(dst + imageDstOffset, src + imageSrcOffset, srcDataSize);
            src = dst;
            imageSrcOffset = imageDstOffset;
         }
      }
      else
      {
         int nRows = imageFormat.getLinesPerFrame();
         int pitchInBytes = imageFormat.getBytesPerFieldExcAlpha() / nRows;
         MTI_UINT8 *srcRowStart = src + imageSrcOffset + ((nRows - 1) * pitchInBytes);
         MTI_UINT8 *dstRowStart = dst + imageDstOffset;
         for (int row = 0; row < nRows; ++row)
         {
            if (needToSwap16)
            {
               const int pitchInShorts = pitchInBytes / sizeof(MTI_UINT16);;
               SwapINT16(reinterpret_cast<MTI_UINT16 *>(srcRowStart),
                         reinterpret_cast<MTI_UINT16 *>(dstRowStart),
                         pitchInShorts);
            }
            else if (needToSwap32)
            {
               const int pitchInLongs = pitchInBytes / sizeof(MTI_UINT32);;
               SwapINT32(reinterpret_cast<MTI_UINT32 *>(srcRowStart),
                         reinterpret_cast<MTI_UINT32 *>(dstRowStart),
                         pitchInLongs);
            }
            else
            {
               MTImemcpy(dstRowStart, srcRowStart, pitchInBytes);
            }

            srcRowStart -= pitchInBytes;
            dstRowStart += pitchInBytes;
         }

         src = dst;
         imageSrcOffset = imageDstOffset;
         needToFlipItUpsideDown = false;;
      }
	}

   if (needToSwap16 || needToSwap32 || needToFlipItUpsideDown)
   {
      swapTime = timer.Read();
   }

//   if (needToConvertYtoYYY)
//   {
//      ExpandYToYYY(imageFormat, src, imageSrcOffset, dst, imageDstOffset);
//      yyyTime = timer.Read() - swapTime;
//
//      // GETTING A DOUBLE FLIP ON FIRST VERSION CLIP WRITE, SO DISABLE FOR NOW
////      if (needToFlipItUpsideDown)
////      {
////         int nRows = imageFormat.getLinesPerFrame();
////         int pitchInBytes = imageFormat.getFramePitch();
////         MTI_UINT8 *buffer = new MTI_UINT8[pitchInBytes];
////         // NOTE: we're doing this IN PLACE, so source is the destination
////         MTI_UINT8 *srcRowStart = dst + imageDstOffset;
////         MTI_UINT8 *dstRowStart = dst + imageDstOffset + ((nRows - 1) * pitchInBytes);
////         for (int row = 0; row < (nRows / 2); ++row)
////         {
////            MTImemcpy(buffer, dstRowStart, pitchInBytes);
////            MTImemcpy(dstRowStart, srcRowStart, pitchInBytes);
////            MTImemcpy(srcRowStart, buffer, pitchInBytes);
////
////            srcRowStart += pitchInBytes;
////            dstRowStart -= pitchInBytes;
////         }
////
////         delete[] buffer;
////      }
//   }

	void *headerData = frameBufferIn->getBufferPtr();
	size_t headerDataSize = header.imageElement[0].ulOffsetToData;
	// FIXME QQQ - duplicating the separate alpha matte.
	void *trailerData = (unsigned char *)headerData + headerDataSize + imageFormat.getBytesPerFieldExcAlpha();
	size_t trailerDataSize = ((headerDataSize + imageFormat.getBytesPerFieldExcAlpha()) > header.ulFileSize)
                                ? 0
                                : header.ulFileSize - (headerDataSize + imageFormat.getBytesPerFieldExcAlpha());

	frameBufferOut = MediaFrameBuffer::CreateSharedBuffer(cookedFrameBuffer, imageFormat, true);
	frameBufferOut->setHeaderData(headerData, headerDataSize);
	frameBufferOut->setTrailerData(trailerData, trailerDataSize);
	bool setSizeError = !frameBufferOut->setTotalDataSize((size_t)imageFormat.getBytesPerFieldExcAlpha());
	frameBufferOut->setHasSeparateAlpha(hasSeparateAlpha);
	frameBufferOut->setAlphaDataOffset(alphaDstOffset);
	frameBufferOut->setTimecode(header.getTimecode());

	if (setSizeError)
   {
      TRACE_0(errout << "Failed setTotalDataSize() was called from DpxFrameReader::decodeImage()");
   }

	TRACE_3(errout << "::::::::::::::: DECODED frame " << _filename << " in " << timer.Read()
						<< " msec (swap=" << swapTime << ", y->yyy=" << yyyTime << ") :::::::::::::::");

	return 0;
}
//---------------------------------------------------------------------------

int DpxFrameReader::dumpFileHeader(string &headerDumpString)
{
	MediaFrameBufferSharedPtr frameBuffer;
	CImageFormat invalidImageFormat;
	int retVal = getFrameBuffer(frameBuffer, invalidImageFormat);
	if (retVal != 0)
	{
		return retVal;
	}

	void *headerPtr = frameBuffer->getHeaderDataPtr();
	if (headerPtr == nullptr)
	{
	   auto rawMediaFrameBuffer = frameBuffer->getRawMediaFrameBuffer();
      MTIassert(rawMediaFrameBuffer);
      if (!rawMediaFrameBuffer)
      {
         return FILE_ERROR_UNEXPECTED_NULL_POINTER;
      }

		headerPtr = rawMediaFrameBuffer->getBufferPtr();
	}

	DpxHeader header;
	retVal = header.extractFromFrameBuffer(headerPtr, DPX_HEADER_SIZE_WITHOUT_USER_DEFINED_CRAP);
	if (retVal != 0)
	{
		TRACE_0(errout << "ERROR: DPX PARSE HEADER FAIL (code " << retVal << " for file " << _filename);
		return FILE_ERROR_DPX_HEADER_DECODE_FAILED;
	}

	headerDumpString = header.toString();
	return 0;
}
//---------------------------------------------------------------------------

int DpxFrameReader::extractHeader(MediaFrameBufferSharedPtr frameBufferIn, DpxHeader &headerOut)
{
   MTIassert(frameBufferIn);
	MTIassert(!frameBufferIn->isDecoded());
   auto rawMediaFrameBuffer = frameBufferIn->getRawMediaFrameBuffer();
   MTIassert(rawMediaFrameBuffer);
   if (!rawMediaFrameBuffer)
   {
      return FILE_ERROR_UNEXPECTED_NULL_POINTER;
   }

   int retVal = headerOut.extractFromFrameBuffer(
                     rawMediaFrameBuffer->getBufferPtr(),
                     rawMediaFrameBuffer->getBufferSize(),
                     rawMediaFrameBuffer->getDataSize());

	return retVal;
}
//---------------------------------------------------------------------------

///* static */
//void DpxFrameReader::RemoveFromHeaderCache(const string &filename)
//{
////	_HeaderCache.remove(filename);
//}
////---------------------------------------------------------------------------
//
///* static */
//void DpxFrameReader::ClearHeaderCache()
//{
////	_HeaderCache.clear();
//}
//---------------------------------------------------------------------------
