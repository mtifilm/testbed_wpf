//---------------------------------------------------------------------------

#pragma hdrstop

#include "DpxHeader.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------

#include "BCDLib.h"
#include "err_file.h"
#include "MTImalloc.h"
#include "pTimecode.h"
#include "SMPTETimecode.h"
//---------------------------------------------------------------------------

namespace
{
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	void SwapHeader(DpxHeader *header)
	{
		int iImageElem;

		// DO NOT SWAP the magic number - it tells us the endanness of the file
		// without having to add another flag.
		// SwapINT32(&header->ulMagicNumber);

		byteSwap(header->ulDataOffset);
		byteSwap(header->ulFileSize);
		byteSwap(header->ulDittoKey);
		byteSwap(header->ulGenericSectionLength);
		byteSwap(header->ulIndustrySpecificLength);
		byteSwap(header->ulUserDefinedLength);
		byteSwap(header->ulEncryptionKey);
		byteSwap(header->usImageOrientation);
		byteSwap(header->usNumImageElements);
		byteSwap(header->ulPixelsPerLine);
		byteSwap(header->ulLinesPerImage);
		for (iImageElem = 0; iImageElem < NUM_IMAGE_ELEM; iImageElem++)
		{
			byteSwap(header->imageElement[iImageElem].ulDataSign);
			byteSwap(header->imageElement[iImageElem].ulLowDataValue);
			byteSwap(header->imageElement[iImageElem].fLowDataQuantity);
			byteSwap(header->imageElement[iImageElem].ulHighDataValue);
			byteSwap(header->imageElement[iImageElem].fHighDataQuantity);
			byteSwap(header->imageElement[iImageElem].usPacking);
			byteSwap(header->imageElement[iImageElem].usEncoding);
			byteSwap(header->imageElement[iImageElem].ulOffsetToData);
			byteSwap(header->imageElement[iImageElem].ulEndOfLinePadding);
			byteSwap(header->imageElement[iImageElem].ulEndOfImagePadding);
		}

		byteSwap(header->ulHorizontalOffset);
		byteSwap(header->ulVerticalOffset);
		byteSwap(header->fHorizontalCenter);
		byteSwap(header->fVerticalCenter);
		byteSwap(header->ulHorizontalOrigSize);
		byteSwap(header->ulVerticalOrigSize);
		byteSwap(header->usBorderValidityLeft);
		byteSwap(header->usBorderValidityRight);
		byteSwap(header->usBorderValidityTop);
		byteSwap(header->usBorderValidityBottom);
		byteSwap(header->ulHorizontalPixelAspect);
		byteSwap(header->ulVerticalPixelAspect);
		byteSwap(header->ulFramePosition);
		byteSwap(header->ulSequenceLength);
		byteSwap(header->ulHeldCount);
		byteSwap(header->fFrameRate);
		byteSwap(header->fShutterAngleDegree);
		byteSwap(header->ulSMPTETimeCode);
		byteSwap(header->ulSMPTEUserBits);
		byteSwap(header->fHorizontalSamplingRate);
		byteSwap(header->fVerticalSamplingRate);
		byteSwap(header->fTemporalSamplingRate);
		byteSwap(header->fTimeOffsetFromSync);
		byteSwap(header->fGamma);
		byteSwap(header->fBlackLevelValue);
		byteSwap(header->fBlackGain);
		byteSwap(header->fBreakpoint);
		byteSwap(header->fReferenceWhiteLevel);
		byteSwap(header->fIntegrationTime);
	}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
}
//---------------------------------------------------------------------------

int DpxHeader::extractFromFrameBuffer(void *rawFrameBuffer, size_t bufferSize, size_t fileSizeOverride)
{
	ulMagicNumber = 0;
	if (!rawFrameBuffer || bufferSize < DPX_HEADER_SIZE_WITHOUT_USER_DEFINED_CRAP)
	{
		// Missing or corrupt file
		return FILE_ERROR_SEEK;
	}

	void *copyFrom = rawFrameBuffer;
	void *copyTo = &ulMagicNumber;
	MTImemcpy(copyTo, copyFrom, DPX_HEADER_SIZE_WITHOUT_USER_DEFINED_CRAP);

	if (ulMagicNumber != MAGIC_NUMBER_DPX)
	{
		if (ulMagicNumber != MAGIC_NUMBER_DPX_SWAPPED)
		{
			return FILE_ERROR_BAD_MAGIC_NUMBER;
		}

		// File is big-endian. Swap each numeric header value but NOT the
		// magic number so we know whether it was swapped or not.
		SwapHeader(this);
	}

	// HACK - replace the header 'file size' field with the correct value.
	// Some programs set it wrong.
	if (fileSizeOverride > 0)
	{
		ulFileSize = (MTI_UINT32) fileSizeOverride;
	}

	return 0;
}
//---------------------------------------------------------------------------

int DpxHeader::replaceToFrameBuffer(void *rawFrameBuffer, size_t bufferSize)
{
	if (!rawFrameBuffer || bufferSize < DPX_HEADER_SIZE_WITHOUT_USER_DEFINED_CRAP)
	{
		// Internal error
		return FILE_ERROR_SEEK;
	}

	if (ulMagicNumber != MAGIC_NUMBER_DPX)
	{
		if (ulMagicNumber != MAGIC_NUMBER_DPX_SWAPPED)
		{
			return FILE_ERROR_BAD_MAGIC_NUMBER;
		}

		// File is big-endian. Swap each numeric header value but NOT the
		// magic number, which wasn't swapped on extraction.
		SwapHeader(this);
	}

	void *copyFrom = &ulMagicNumber;
	void *copyTo = rawFrameBuffer;
	MTImemcpy(copyTo, copyFrom, DPX_HEADER_SIZE_WITHOUT_USER_DEFINED_CRAP);

	return 0;
}
//---------------------------------------------------------------------------

int DpxHeader::convertToImageFormat(CImageFormat &imageFormat)
{
	// Check for unsupported DPX configurations.
	if (usImageOrientation != 0)
	{
		// JUST LOG A WARNING - let them fix the image upside down if they want!
		TRACE_0(errout << "WARNING: DPX attribute [Image Orientation = " << usImageOrientation << "] is not supported!");
	}

	const int PIXEL_COMPONENTS_INVALID = 0;
	const int PIXEL_COMPONENTS_LUMINANCE = 6;      // Luminance only, B&W
	const int PIXEL_COMPONENTS_RGB = 50;
	const int PIXEL_COMPONENTS_RGBA = 51;          // A = alpha matte channel
	const int PIXEL_COMPONENTS_YUV422 = 100;       // CbYCrY
	const int PIXEL_COMPONENTS_YUV4224 = 101;      // YUV + alpha, CbYACrYA
	const int PIXEL_COMPONENTS_YUV444 = 102;       // CbYCr
	const int PIXEL_COMPONENTS_Y = 256;            // Like LUMINANCE, but expanded to 3 channels (HACK)
	const int PIXEL_COMPONENTS_YYY = 257;          // Like RGB, but all channels should have same values
	const int PIXEL_COMPONENTS_BGR = 258;          // Like RGB but B & R are swapped
	const int PIXEL_COMPONENTS_BGRA = 259;         // Like RGBA but B & R are swapped


	// ucDescriptor=6	or 0 means Y only
	// ucDescriptor=50	  means RGB data
	// ucDescriptor=51	  means RGBA data
	// ucDescriptor=100    means CrYCbY (based on SMPTE 125M)
	//
	// ucBitSize = N	     means N bit data (where N = 8, 10, 12 or 16)
	//
	// usPacking = 0       means no filling
	// usPacking = 1       means values padded to fill each 4 bytes with data shifted LEFT
	// usPacking = 2       means values padded to fill each 4 bytes with data shifted RIGHT
	//
	// For other packing values, we interpret even values to be 0 and odd
	// values to be 1. Observed data points:
	// - Kevin has seen 2 which meant 0  [THIS IS OVERRIDDEN BY THE v2.0 DPX SPEC]
	// - Combustion sets it to 3 which means 1
	// - Illusion sets it to 5 which means 1

	// Hmmm in version 2.0 of DPX spec the legal values are 0, 1 and 2.
	// // Make sure packing is in legal range (0 - 1)
	// imageElement[0].usPacking &= 1;
	unsigned short rawPacking = imageElement[0].usPacking;
	rawPacking &= 3;
	rawPacking = (rawPacking == 3) ? 1 : rawPacking;

	////////////////////////////////////////////////////////////////////
	// SANITY CHECKS

	// Handle ImageMagick converted DPX's which claim to have 0 images!
	if (usNumImageElements == 0)
	{
		usNumImageElements = 1;
	}

	switch (imageElement[0].ucDescriptor)
	{
		case PIXEL_COMPONENTS_INVALID:
		case PIXEL_COMPONENTS_LUMINANCE:
		case PIXEL_COMPONENTS_RGB:
		case PIXEL_COMPONENTS_RGBA:
		case PIXEL_COMPONENTS_YUV422:
			// It's OK!
			break;

		default:
			return FILE_ERROR_UNSUPPORTED_OPTION; // unsupported at this time
	}

	switch (imageElement[0].ucBitSize)
	{
		case  8:
		case 10:
		case 12:
		case 16:
			// It's OK!
			break;

		default:
			return FILE_ERROR_UNSUPPORTED_OPTION; // unsupported at this time
	}

	////////////////////////////////////////////////////////////////////
	// Create the ImageFormat.


	// 1. Determine the Pixel Components
	EPixelComponents pixelComponents;
	if (imageElement[0].ucDescriptor == PIXEL_COMPONENTS_INVALID
	|| imageElement[0].ucDescriptor == PIXEL_COMPONENTS_LUMINANCE)
	{
//		pixelComponents = (imageElement[0].ucBitSize == 16 || imageElement[0].ucBitSize == 10)
//									? IF_PIXEL_COMPONENTS_Y
//									: IF_PIXEL_COMPONENTS_LUMINANCE;
		pixelComponents = IF_PIXEL_COMPONENTS_Y;
	}
	else if (imageElement[0].ucDescriptor == PIXEL_COMPONENTS_RGB)
	{
		// Clip is RGB unless user is forcing BGR (ignore the YYY crap, it should all be ripped out).
		pixelComponents = IF_PIXEL_COMPONENTS_RGB;
	}
	else if (imageElement[0].ucDescriptor == PIXEL_COMPONENTS_RGBA)
	{
		pixelComponents = IF_PIXEL_COMPONENTS_RGBA;
	}
	else if (imageElement[0].ucDescriptor == PIXEL_COMPONENTS_YUV422)
	{
		pixelComponents = IF_PIXEL_COMPONENTS_YUV422;
	}
	else
	{
		return FILE_ERROR_BAD_COMPONENTS;
	}

	// determine pixel packing
	// Now modified for Version 2.0 of the spec.
	int packing = imageElement[0].usPacking & 3;
	bool swap = ulMagicNumber == MAGIC_NUMBER_DPX_SWAPPED;

	EPixelPacking pixelPacking;
	switch (imageElement[0].ucBitSize)
	{
	case 16:
		// iPixelPacking = swap
		// ? IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE
		// : IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE;
		// now always produce LE!
		pixelPacking = IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE;
		break;

	case 12:
		if (packing == 2)
		{
			// v 2.0 format presently unsupported
			return FILE_ERROR_BAD_BIT_DEPTH;
		}

		pixelPacking = (packing == 0)
								? IF_PIXEL_PACKING_2_12Bits_IN_3Bytes
								: (swap
									? IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_BE
									: IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_LE);
		break;

	case 10:
      pixelPacking = (packing == 0)
                        ? IF_PIXEL_PACKING_4_10Bits_IN_5Bytes
                        : ((packing == 1 || packing == 3)
                           ? IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L
                           : IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R);
		break;

	case 8:
		pixelPacking = IF_PIXEL_PACKING_8Bits_IN_1Byte;
		break;

	default:
		return FILE_ERROR_BAD_BIT_DEPTH;
	}

	int iRet = imageFormat.setToDefaults(
						  IF_TYPE_FILE_DPX,
						  pixelPacking,
						  pixelComponents,
						  ulLinesPerImage,
						  ulPixelsPerLine);
	if (iRet)
	{
		return iRet;
	}

	////////////////////////////////////////////////////////////////////
	// Look for Alpha bits.

	EAlphaMatteType alphaMatteType = IF_ALPHA_MATTE_NONE;

	// Detect the presence of a one-bit Alpha matte
	if (usNumImageElements == 2                // 2 images
	&& imageElement[0].ucDescriptor == 50      // Image 0: RGB data
	&& imageElement[1].ucDescriptor == 4)      // Image 1: Alpha Channel
	{
		alphaMatteType = (imageElement[1].ucBitSize == 1)
								? IF_ALPHA_MATTE_1_BIT
								: ((imageElement[1].ucBitSize == 8)
									? IF_ALPHA_MATTE_8_BIT
									: ((imageElement[1].ucBitSize == 16)
										? IF_ALPHA_MATTE_16_BIT
										: IF_ALPHA_MATTE_NONE));
	}
	else if (imageElement[0].ucDescriptor == 50     // RGB
	&& imageElement[0].ucBitSize == 10              // 10 bits/component
	&& (imageElement[0].usPacking & 1) == 1)         // packing
	{
		alphaMatteType = IF_ALPHA_MATTE_2_BIT_EMBEDDED;
	}
	else if (imageElement[0].ucDescriptor == 51
	&& imageElement[0].ucBitSize == 10              // 10 bits/component
	&& imageElement[0].usPacking != 0               // packing
	&& imageElement[0].usEncoding == 0)             // encoding
	{
		alphaMatteType = IF_ALPHA_MATTE_10_BIT_INTERLEAVED;
	}
	else if (imageElement[0].ucDescriptor == 51     // Image 0: RGBA data
	&& (imageElement[0].ucBitSize == 8 || imageElement[0].ucBitSize == 16)
	&& imageElement[0].usEncoding == 0)
	{
		alphaMatteType = (imageElement[0].ucBitSize == 8)
											? IF_ALPHA_MATTE_8_BIT_INTERLEAVED
											: IF_ALPHA_MATTE_16_BIT_INTERLEAVED;
	}

	// Set the alpha matte types in the image format
	imageFormat.setAlphaMatteType(alphaMatteType);

	//////////////////////////////////////
	// Set anything internal to DPX format
	//////////////////////////////////////

	EColorSpace iTransferCharacteristic = IF_COLOR_SPACE_INVALID;

	// If colorspace is already set, it's an override!
	EColorSpace iColorSpace = CImageInfo::GetDpxIsDefaultLog() ? IF_COLOR_SPACE_LOG : IF_COLOR_SPACE_LINEAR;
	if (pixelComponents == IF_PIXEL_COMPONENTS_YUV422)
	{
		// Default to CCIR 601 for SD, or CCIR 709 for HD
		iColorSpace = IF_COLOR_SPACE_CCIR_709;
		iTransferCharacteristic = IF_COLOR_SPACE_CCIR_709;

		if (ulPixelsPerLine <= 720)
		{
			if (ulLinesPerImage <= 525)
			{
				// NTSC
				iColorSpace = IF_COLOR_SPACE_CCIR_601_M;
				iTransferCharacteristic = IF_COLOR_SPACE_CCIR_601_M;
			}
			else
			{
				// PAL
				iColorSpace = IF_COLOR_SPACE_CCIR_601_BG;
				iTransferCharacteristic = IF_COLOR_SPACE_CCIR_601_BG;
			}
		}
	}
	else if (imageElement[0].ucBitSize == 16)
	{
		// 16-bit files are never LOG, even if the header claims it is,
		// but the user can override that
		iColorSpace = IF_COLOR_SPACE_LINEAR;
	}

	// TransferCharacteristic and ColorimetricSpec seem to have overlap
	// they don't seem to be used all the consistently in DPX files
	if (imageElement[0].ucTransferCharacteristic == 5)
	{
		iTransferCharacteristic = IF_COLOR_SPACE_SMPTE_240;
	}
	else if (imageElement[0].ucTransferCharacteristic == 6)
	{
		iTransferCharacteristic = IF_COLOR_SPACE_CCIR_709;
	}
	else if (imageElement[0].ucTransferCharacteristic == 7)
	{
		iTransferCharacteristic = IF_COLOR_SPACE_CCIR_601_BG;
	}
	else if (imageElement[0].ucTransferCharacteristic == 8)
	{
		iTransferCharacteristic = IF_COLOR_SPACE_CCIR_601_M;
	}

	// TransferCharacteristic and ColorimetricSpec seem to have overlap
	// they don't seem to be used all the consistently in DPX files
	if (imageElement[0].ucTransferCharacteristic == 1 || imageElement[0].ucTransferCharacteristic == 3 ||
			imageElement[0].ucColorimetricSpec == 1 || imageElement[0].ucColorimetricSpec == 3)
	{
		iColorSpace = CImageInfo::GetDpxIsDefaultLog() ? IF_COLOR_SPACE_LOG : IF_COLOR_SPACE_LINEAR;
	}
	else if (imageElement[0].ucColorimetricSpec == 2)
	{
		iColorSpace = IF_COLOR_SPACE_LINEAR;
	}
	else if (imageElement[0].ucColorimetricSpec == 5)
	{
		iColorSpace = IF_COLOR_SPACE_SMPTE_240;
	}
	else if (imageElement[0].ucColorimetricSpec == 6)
	{
		iColorSpace = IF_COLOR_SPACE_CCIR_709;
	}
	else if (imageElement[0].ucColorimetricSpec == 7)
	{
		iColorSpace = IF_COLOR_SPACE_CCIR_601_BG;
	}
	else if (imageElement[0].ucColorimetricSpec == 8)
	{
		iColorSpace = IF_COLOR_SPACE_CCIR_601_M;
	}
	else if (imageElement[0].ucTransferCharacteristic == 2)
	{
		iColorSpace = IF_COLOR_SPACE_LINEAR;
	}

	// Transfer characteristic is an override; if it's the same, set to invalid
	imageFormat.setColorSpace(iColorSpace);
	if (iTransferCharacteristic == iColorSpace)
	{
		iTransferCharacteristic = IF_COLOR_SPACE_INVALID;
	}

	imageFormat.setTransferCharacteristic(iTransferCharacteristic);

	return 0;
}
//---------------------------------------------------------------------------

CTimecode DpxHeader::getTimecode()
{
	// pull the timecode out of the TV section of the header;
	// unfortunately, there is no frame rate information so see
	// if the caller set it already, or else just guess 24,
	// which will often be wrong
	MTI_UINT32 rawTimecode = ulSMPTETimeCode;

	// assume timecode not set if it's all 0's or 1's
	if (rawTimecode == 0 && rawTimecode == ((MTI_UINT32)(-1)))
	{
		return CTimecode::NOT_SET;
	}

	CSMPTETimecode smpteTimecode;
	smpteTimecode.setRawTimecode(rawTimecode);

	// in the following, use 60 frames per second because we may not
	// know what it is, and we want to just capture the HH, MM, SS, and FF.
	PTimecode mtiTimecode = smpteTimecode.getTimecode(60);
	int hrs = mtiTimecode.getHours();
	int mins = mtiTimecode.getMinutes();
	int secs = mtiTimecode.getSeconds();
	int frms = mtiTimecode.getFrames();
	int flags = mtiTimecode.getFlags();
	bool df;
	int fps;
	mtiTimecode.getDefaultFpsAndDropFrame(fps, df);

	return CTimecode(hrs, mins, secs, frms, flags, fps);
}
//---------------------------------------------------------------------------

bool DpxHeader::isFileSwapped()
{
	return ulMagicNumber == MAGIC_NUMBER_DPX_SWAPPED;
}
//---------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
//
// New header dumper
//
///////////////////////////////////////////////////////////////////////////////

namespace
{

	const string strChanged("  <<<CHANGED!<<<");
	const int W1 = 30;
	const int W2 = 14;
	const int W3 = 12;
	const string UNDEF_PHRASE("--");

	string genLabel(const char *label, unsigned int level)
	{
		MTIostringstream labelField;

		if (level > 0)
		{
			labelField << "[" << level << "] ";
		}
		labelField << label << ':';

		return labelField.str();
	}

	string genHex(unsigned int value, unsigned int numDigits)
	{
		MTIostringstream hexValue;

		hexValue << "(0x" << std::right << std::setfill('0') << std::setbase(16) << std::setw(numDigits) << value << ")";

		return hexValue.str();
	}

	string genUnsigned(const char *label, unsigned long value, unsigned long fieldWidth, unsigned long undef, bool bChanged,
			unsigned int level)
	{
		MTIostringstream line;

		line << std::setw(W1) << std::left << std::setfill(' ') << genLabel(label, level) << std::setw(W2);
		if (value == undef)
		{
			line << UNDEF_PHRASE;
		}
		else
		{
			line << std::setbase(10) << value;
		}
		line << std::setw(W3) << genHex(value, fieldWidth * 2);
		if (bChanged)
		{
			line << strChanged;
		}
		return line.str();
	}

	string genFloat(const char *label, float value, float oldValue, unsigned int level = 0)
	{
		MTIostringstream line;
		const unsigned long FLOAT_UNDEF = 0xFFFFFFFFUL;
		unsigned long valueAsUnsignedLong = *(unsigned long *)&value;
		unsigned long oldValueAsUnsignedLong = *(unsigned long *)&oldValue;

		line << std::setw(W1) << std::left << std::setfill(' ') << genLabel(label, level) << std::setw(W2);
		if (valueAsUnsignedLong == FLOAT_UNDEF)
		{
			line << UNDEF_PHRASE;
		}
		else
		{
			line << value;
		}
		line << std::setw(W3) << genHex(valueAsUnsignedLong, 8);

		// Avoid NAN exceptions by comparing the bits instead of the float values
		if (valueAsUnsignedLong != oldValueAsUnsignedLong)
		{
			line << strChanged;
		}
		return line.str();
	}

	inline string genUlong(const char *label, unsigned int value, unsigned int oldValue, unsigned int level = 0)
	{
		return genUnsigned(label, value, 4, 0xFFFFFFFFUL, value != oldValue, level);
	}

	inline string genUshort(const char *label, unsigned short value, unsigned short oldValue, unsigned int level = 0)
	{
		return genUnsigned(label, value, 2, 0xFFFFUL, value != oldValue, level);
	}

	inline string genUchar(const char *label, unsigned char value, unsigned char oldValue, unsigned int level = 0)
	{
		return genUnsigned(label, value, 1, 0xFFUL, value != oldValue, level);
	}

	string genString(const char *label, const signed char *valueArg, unsigned int fieldWidth, const signed char *oldValueArg,
			unsigned int level = 0)
	{
		bool hasNonPrintables = false;
		bool doesntMatchOldValue = false;
		unsigned int i, actualWidth;

		char *value = new char[fieldWidth + 1];
		for (i = 0; i < fieldWidth && valueArg[i] != 0; ++i)
		{
			if (isprint(valueArg[i]))
			{
				value[i] = valueArg[i];
			}
			else
			{
				value[i] = '.';
				hasNonPrintables = true;
			}
			if (valueArg[i] != oldValueArg[i])
			{
				doesntMatchOldValue = true;
			}
		}
		if (i < fieldWidth && oldValueArg[i] != 0)
		{
			doesntMatchOldValue = true;
		}
		actualWidth = i;
		value[actualWidth] = '\0';

		MTIostringstream line;
		line << std::setw(W1) << std::left << std::setfill(' ') << genLabel(label, level) << std::setw(W2);
		if (actualWidth == 0)
		{
			line << UNDEF_PHRASE << std::setw(W3) << "(\"\")";
		}
		else
		{
			string strValue;
			const string POP("\"");
			strValue = POP + string(value) + POP;

			line << strValue;
			if (hasNonPrintables)
			{
				line << " (";
				for (i = 0; i < actualWidth; ++i)
				{
					if (i != 0)
					{
						line << " ";
					}
					line << "0x" << std::setw(2) << std::setbase(16) << std::setfill('0') << (((unsigned int) valueArg[i]) & 0xFF);
				}
				line << ")";
			}
		}
		if (doesntMatchOldValue)
		{
			line << strChanged;
		}

		delete[]value;
		return line.str();
	}

	string genImageElementHeader(unsigned int elemNum)
	{
		MTIostringstream line;
		line << "Image Element " << elemNum << ":";

		return line.str();
	}

	string gen4cc(const char *label, unsigned long value, unsigned long oldValue, unsigned int level = 0)
	{
		MTIostringstream line;
		MTIostringstream fourCC;

		fourCC << (char)((value >> 24) & 0xFF) << (char)((value >> 16) & 0xFF) << (char)((value >> 8) & 0xFF) << (char)((value >> 0) & 0xFF);

		line << std::setw(W1) << std::setfill(' ') << std::left << genLabel(label, level) << std::setw(W2) << fourCC.str() << std::setw(W3) << genHex(value, 8);

		if (value != oldValue)
		{
			line << strChanged;
		}
		return line.str();
	}

	string genImgDesc(const char *label, unsigned long value, unsigned long oldValue, unsigned int level = 0)
	{
		MTIostringstream line;
		string imgDesc;

		switch (value)
		{
		case 0:
			imgDesc = "USER_1_COMP";
			break;
		case 1:
			imgDesc = "RED";
			break;
		case 2:
			imgDesc = "GREEN";
			break;
		case 3:
			imgDesc = "BLUE";
			break;
		case 4:
			imgDesc = "ALPHA";
			break;

		case 6:
			imgDesc = "LUMINANCE";
			break;
		case 7:
			imgDesc = "CHROMINANCE";
			break;
		case 8:
			imgDesc = "DEPTH";
			break;
		case 9:
			imgDesc = "COMPOSITE";
			break;

		case 50:
			imgDesc = "RGB";
			break;
		case 51:
			imgDesc = "RGBA ";
			break;
		case 52:
			imgDesc = "ABGR";
			break;

		case 100:
			imgDesc = "CbYCrY";
			break;
		case 101:
			imgDesc = "CbYaCrYa";
			break;
		case 102:
			imgDesc = "CbYCr";
			break;
		case 103:
			imgDesc = "CbYCra";
			break;

		case 150:
			imgDesc = "USER_2_COMP";
			break;
		case 151:
			imgDesc = "USER_3_COMP";
			break;
		case 152:
			imgDesc = "USER_4_COMP";
			break;
		case 153:
			imgDesc = "USER_5_COMP";
			break;
		case 154:
			imgDesc = "USER_6_COMP";
			break;
		case 155:
			imgDesc = "USER_7_COMP";
			break;
		case 156:
			imgDesc = "USER_8_COMP";
			break;

		default:
			imgDesc = "**INVALID**";
			break;
		}
		line << std::setw(W1) << std::setfill(' ') << std::left << genLabel(label, level) << std::setw(W2) << imgDesc << std::setw(W3) << genHex(value, 2);

		if (value != oldValue)
		{
			line << strChanged;
		}
		return line.str();
	}

	string genXfrChar(const char *label, unsigned long value, unsigned long oldValue, unsigned int level = 0)
	{
		MTIostringstream line;
		string xfrChar;

		switch (value)
		{
		case 0:
			xfrChar = "USER_DEF";
			break;
		case 1:
			xfrChar = "PRINT_DENSITY";
			break;
		case 2:
			xfrChar = "LINEAR";
			break;
		case 3:
			xfrChar = "LOG";
			break;
		case 4:
			xfrChar = "UNSPEC_VIDEO";
			break;
		case 5:
			xfrChar = "SMPTE_240M";
			break;
		case 6:
			xfrChar = "CCIR_709";
			break;
		case 7:
			xfrChar = "CCIR_601_B/G";
			break;
		case 8:
			xfrChar = "CCIR_601_M";
			break;
		case 9:
			xfrChar = "NTSC_COMP";
			break;
		case 10:
			xfrChar = "PAL_COMP";
			break;
		case 11:
			xfrChar = "Z_LINEAR";
			break;
		case 12:
			xfrChar = "Z_HOMOG";
			break;

		default:
			xfrChar = "**INVALID**";
			break;
		}
		line << std::setw(W1) << std::setfill(' ') << std::left << genLabel(label, level) << std::setw(W2) << xfrChar << std::setw(W3) << genHex(value, 2);

		if (value != oldValue)
		{
			line << strChanged;
		}
		return line.str();
	}

	string genColorim(const char *label, unsigned long value, unsigned long oldValue, unsigned int level = 0)
	{
		MTIostringstream line;
		string colorim;

		switch (value)
		{
		case 0:
			colorim = "USER_DEF";
			break;
		case 1:
			colorim = "PRINT_DENSITY";
			break;
		case 4:
			colorim = "UNSPEC_VIDEO";
			break;
		case 5:
			colorim = "SMPTE_240M";
			break;
		case 6:
			colorim = "CCIR_709";
			break;
		case 7:
			colorim = "CCIR_601_B/G";
			break;
		case 8:
			colorim = "CCIR_601_M";
			break;
		case 9:
			colorim = "NTSC_COMP";
			break;
		case 10:
			colorim = "PAL_COMP";
			break;

		default:
			colorim = "**INVALID**";
			break;
		}
		line << std::setw(W1) << std::setfill(' ') << std::left << genLabel(label, level) << std::setw(W2) << colorim << std::setw(W3) << genHex(value, 2);

		if (value != oldValue)
		{
			line << strChanged;
		}

		return line.str();
	}

	string genSMPTETC(const char *label, unsigned long value, unsigned long oldValue, unsigned int level = 0)
	{
		MTIostringstream line;
		MTIostringstream tc;

		// assume timecode not set if it's all 0's or 1's
		if (value == 0xFFFFFFFFUL)
		{
			tc << UNDEF_PHRASE;
		}
		else
		{
			unsigned char *ucp = reinterpret_cast<unsigned char *>(&value);
			tc << std::setfill('0') << std::setw(2) << BCDToInt(ucp[0]) << std::setw(1) << ':' << std::setw(2) << BCDToInt(ucp[1]) << std::setw(1) << ':' << std::setw(2)
					<< BCDToInt(ucp[2]) << std::setw(1) << ':' << std::setw(2) << BCDToInt(ucp[3]) << std::setw(1);
		}
		line << std::setw(W1) << std::setfill(' ') << std::left << genLabel(label, level) << std::setw(W2) << tc.str() << std::setw(W3) << genHex(value, 2);

		if (value != oldValue)
		{
			line << strChanged;
		}
		return line.str();
	}

	string genHeaderString(const DpxHeader &header)
	{
		MTI_UINT16 usImageElem;
		MTI_UINT32 ulSwappedMagic(header.ulMagicNumber);
		byteSwap(ulSwappedMagic);
		MTIostringstream os;

		os << "============\r\n";
		os << gen4cc("Magic Number", ulSwappedMagic, ulSwappedMagic) << "\r\n";
		os << genUlong("Data Offset", header.ulDataOffset, header.ulDataOffset) << "\r\n";
		os << genString("Version Number", header.caVersionNumber, 8, header.caVersionNumber) << "\r\n";
		os << genUlong("File Size", header.ulFileSize, header.ulFileSize) << "\r\n";
		os << genUlong("Ditto Key", header.ulDittoKey, header.ulDittoKey) << "\r\n";
		os << genUlong("Generic Section Length", header.ulGenericSectionLength, header.ulGenericSectionLength) << "\r\n";
		os << genUlong("Industry-Specific Length", header.ulIndustrySpecificLength, header.ulIndustrySpecificLength) << "\r\n";
		os << genUlong("User-Defined Length", header.ulUserDefinedLength, header.ulUserDefinedLength) << "\r\n";
		os << genString("Image File Name", header.caImageFileName, 100, header.caImageFileName) << "\r\n";
		os << genString("Creation Date", header.caCreationDate, 24, header.caCreationDate) << "\r\n";
		os << genString("Creator", header.caCreator, 100, header.caCreator) << "\r\n";
		os << genString("Project Name", header.caProjectName, 200, header.caProjectName) << "\r\n";
		os << genString("Right to Use", header.caRightToUse, 200, header.caRightToUse) << "\r\n";
		os << genUlong("Encryption Key", header.ulEncryptionKey, header.ulEncryptionKey) << "\r\n";
		os << "============\r\n";
		// os << genStr ("Reserved0: %s\n", header.ucaReserved0) << "\r\n";
		os << genUshort("Image Orientation", header.usImageOrientation, header.usImageOrientation) << "\r\n";
		os << genUshort("Number of Image Elements", header.usNumImageElements, header.usNumImageElements) << "\r\n";
		os << genUlong("Pixels per Line", header.ulPixelsPerLine, header.ulPixelsPerLine) << "\r\n";
		os << genUlong("Lines per Image", header.ulLinesPerImage, header.ulLinesPerImage) << "\r\n";

		for (usImageElem = 0; usImageElem < header.usNumImageElements; usImageElem++)
		{
			unsigned int N = usImageElem + 1;
			os << "============\r\n";
			os << genImageElementHeader(N) << "\r\n";
			os << genUlong("Data Sign", header.imageElement[usImageElem].ulDataSign, header.imageElement[usImageElem].ulDataSign, N) << "\r\n";
			os << genUlong("Low Data Value", header.imageElement[usImageElem].ulLowDataValue, header.imageElement[usImageElem].ulLowDataValue, N) << "\r\n";
			os << genFloat("Low Data Quantity", header.imageElement[usImageElem].fLowDataQuantity, header.imageElement[usImageElem].fLowDataQuantity, N)
					<< "\r\n";
			os << genUlong("High Data Value", header.imageElement[usImageElem].ulHighDataValue, header.imageElement[usImageElem].ulHighDataValue, N) << "\r\n";
			os << genFloat("High Data Quantity", header.imageElement[usImageElem].fHighDataQuantity, header.imageElement[usImageElem].fHighDataQuantity, N)
					<< "\r\n";
			os << genImgDesc("Descriptor", header.imageElement[usImageElem].ucDescriptor, header.imageElement[usImageElem].ucDescriptor, N) << "\r\n";
			os << genXfrChar("Transfer Characteristic", header.imageElement[usImageElem].ucTransferCharacteristic,
					header.imageElement[usImageElem].ucTransferCharacteristic, N) << "\r\n";
			os << genColorim("Colorimetric Spec", header.imageElement[usImageElem].ucColorimetricSpec, header.imageElement[usImageElem].ucColorimetricSpec, N)
					<< "\r\n";
			os << genUchar("Bit Size", header.imageElement[usImageElem].ucBitSize, header.imageElement[usImageElem].ucBitSize, N) << "\r\n";
			os << genUshort("Packing", header.imageElement[usImageElem].usPacking, header.imageElement[usImageElem].usPacking, N) << "\r\n";
			os << genUshort("Encoding", header.imageElement[usImageElem].usEncoding, header.imageElement[usImageElem].usEncoding, N) << "\r\n";
			os << genUlong("Offset To Data", header.imageElement[usImageElem].ulOffsetToData, header.imageElement[usImageElem].ulOffsetToData, N) << "\r\n";
			os << genUlong("End Of Line Padding", header.imageElement[usImageElem].ulEndOfLinePadding, header.imageElement[usImageElem].ulEndOfLinePadding, N)
					<< "\r\n";
			os << genUlong("End Of Image Padding", header.imageElement[usImageElem].ulEndOfImagePadding, header.imageElement[usImageElem].ulEndOfImagePadding,
					N) << "\r\n";
			os << genString("Description", header.imageElement[usImageElem].caDescription, 32, header.imageElement[usImageElem].caDescription, N) << "\r\n";
		}
		os << "============\r\n";
		// os << genStr ("Reserved1: %s\n", header.ucaReserved1) << "\r\n";
		os << genUlong("Horizontal Offset", header.ulHorizontalOffset, header.ulHorizontalOffset) << "\r\n";
		os << genUlong("Vertical Offset", header.ulVerticalOffset, header.ulVerticalOffset) << "\r\n";
		os << genFloat("Horizontal Center", header.fHorizontalCenter, header.fHorizontalCenter) << "\r\n";
		os << genFloat("Vertical Center", header.fVerticalCenter, header.fVerticalCenter) << "\r\n";
		os << genUlong("Horizontal Orig Size", header.ulHorizontalOrigSize, header.ulHorizontalOrigSize) << "\r\n";
		os << genUlong("Vertical Orig Size", header.ulVerticalOrigSize, header.ulVerticalOrigSize) << "\r\n";
		os << genString("Source Image File Name", header.caSourceImageFileName, 100, header.caSourceImageFileName) << "\r\n";
		os << genString("Source Image Date", header.caSourceImageDate, 24, header.caSourceImageDate) << "\r\n";
		os << genString("Input Device Name", header.caInputDeviceName, 32, header.caInputDeviceName) << "\r\n";
		os << genString("Input Device Serial Number", header.caInputDeviceSN, 32, header.caInputDeviceSN) << "\r\n";
		os << genUshort("Border Validity Left", header.usBorderValidityLeft, header.usBorderValidityLeft) << "\r\n";
		os << genUshort("Border Validity Right", header.usBorderValidityRight, header.usBorderValidityRight) << "\r\n";
		os << genUshort("Border Validity Top", header.usBorderValidityTop, header.usBorderValidityTop) << "\r\n";
		os << genUshort("Border Validity Bottom", header.usBorderValidityBottom, header.usBorderValidityBottom) << "\r\n";
		os << genUlong("Pixel Aspect Ratio (H)", header.ulHorizontalPixelAspect, header.ulHorizontalPixelAspect) << "\r\n";
		os << genUlong("Pixel Aspect Ratio (V)", header.ulVerticalPixelAspect, header.ulVerticalPixelAspect) << "\r\n";
		os << "============\r\n";
		if (header.ulIndustrySpecificLength > 0)
		{
			// os << genString ("Reserved2: %s\n", header.ucaReserved2) << "\r\n";
			os << genString("Film Mfg ID", header.caFilmMfgID, 2, header.caFilmMfgID) << "\r\n";
			os << genString("Film Type", header.caFilmType, 2, header.caFilmType) << "\r\n";
			os << genString("Offset In Perfs", header.caOffsetInPerfs, 2, header.caOffsetInPerfs) << "\r\n";
			os << genString("Film Edge Code Prefix", header.caFilmEdgeCodePrefix, 6, header.caFilmEdgeCodePrefix) << "\r\n";
			os << genString("Film Edge Code Count", header.caFilmEdgeCodeCount, 4, header.caFilmEdgeCodeCount) << "\r\n";
			os << genString("Format", header.caFormat, 32, header.caFormat) << "\r\n";
			os << genUlong("Frame Position", header.ulFramePosition, header.ulFramePosition) << "\r\n";
			os << genUlong("Sequence Length", header.ulSequenceLength, header.ulSequenceLength) << "\r\n";
			os << genUlong("Held Count", header.ulHeldCount, header.ulHeldCount) << "\r\n";
			os << genFloat("Frame Rate", header.fFrameRate, header.fFrameRate) << "\r\n";
			os << genFloat("Shutter Angle of Camera", header.fShutterAngleDegree, header.fShutterAngleDegree) << "\r\n";
			os << genString("Frame ID", header.caFrameID, 32, header.caFrameID) << "\r\n";
			os << genString("Slate Information", header.caSlateInformation, 100, header.caSlateInformation) << "\r\n";
			os << "============\r\n";
			// os << genString ("Reserved3: %s\n", header.ucaReserved3) << "\r\n";
			os << genSMPTETC("SMPTE Time Code", header.ulSMPTETimeCode, header.ulSMPTETimeCode) << "\r\n";
			os << genUlong("SMPTE User Bits", header.ulSMPTEUserBits, header.ulSMPTEUserBits) << "\r\n";
			os << genUchar("Interlace", header.ucInterlace, header.ucInterlace) << "\r\n";
			os << genUchar("Field Number", header.ucFieldNumber, header.ucFieldNumber) << "\r\n";
			os << genUchar("Video Signal Standard", header.ucVideoStandard, header.ucVideoStandard) << "\r\n";
			os << genFloat("Horizontal Sampling Rate", header.fHorizontalSamplingRate, header.fHorizontalSamplingRate) << "\r\n";
			os << genFloat("Vertical Sampling Rate", header.fVerticalSamplingRate, header.fVerticalSamplingRate) << "\r\n";
			os << genFloat("Frame Rate", header.fTemporalSamplingRate, header.fTemporalSamplingRate) << "\r\n";
			os << genFloat("Time OFfset From Sync", header.fTimeOffsetFromSync, header.fTimeOffsetFromSync) << "\r\n";
			os << genFloat("Gamma", header.fGamma, header.fGamma) << "\r\n";
			os << genFloat("Black Level", header.fBlackLevelValue, header.fBlackLevelValue) << "\r\n";
			os << genFloat("Black Gain", header.fBlackGain, header.fBlackGain) << "\r\n";
			os << genFloat("Breakpoint", header.fBreakpoint, header.fBreakpoint) << "\r\n";
			os << genFloat("White Level", header.fReferenceWhiteLevel, header.fReferenceWhiteLevel) << "\r\n";
			os << genFloat("Integration Time", header.fIntegrationTime, header.fIntegrationTime) << "\r\n";
			os << "============\r\n";
		}
		else
		{
			os << "********ERROR: NO INDUSTRY-SPECIFIC DATA IN HEADER **********\r\n";
		}
		// os << genString ("Reserved4: %s\n", header.ucaReserved4) << "\r\n";
		if (header.ulUserDefinedLength > 0)
		{
			os << genString("User Data:", header.caUserID, 32, header.caUserID) << "\r\n";
		}
		else
		{
			os << "******** NO USER-DEFINED DATA IN HEADER **********\r\n";
		}
		os << "============\r\n";

		return os.str();
	}

} // end of local namespace
//---------------------------------------------------------------------------

string DpxHeader::toString()
{
	return genHeaderString(*this);
}
//---------------------------------------------------------------------------


