#ifndef MediaFileIoH
#define MediaFileIoH

//#include "MTIio.h"                    // Unix <-> Win wrappers
//#include <string>
//#include <iostream>
//#include <vector>

#include "MediaFileIODLL.h"             // Export/Import Defs
#include "err_file.h"

#include "MediaFileIoDefs.h"
#include "MediaFrameBufferReadCache.h"
#include "MediaFrameBufferWriteCache.h"
#include "MediaFrameFileReader.h"
#include "MediaFrameFileWriter.h"

//#include "machine.h"
//#include "MediaFrameFileReader.h"
//#include "MediaFrameFileWriter.h"
//#include "ImageBodyWriteCache.h"
//#include "ImageInfo.h"
//#include "MMFIO.h"
//#include "timecode.h"
//using std::string;
//using std::ostream;
//using std::vector;

// file hacks
enum FileHackCommand
{
   FILE_HACK_HIDE_ALPHA_MATTE    = 1,
   FILE_HACK_SHOW_ALPHA_MATTE    = 2,
   FILE_HACK_DESTROY_ALPHA_MATTE = 3,
   FILE_HACK_CACHE_FRAME         = 4,
};

class MTI_MEDIAFILEIODLL_API MediaFileIO
{
public:

	MediaFileType getFileType(const string &imageFilePath);
//	int cloneFrameBufferWithoutImageData(
//		MediaFrameBufferSharedPtr frameBufferIn,
//		MediaFrameBufferSharedPtr &frameBufferOut);
	int prefetchFrameFile(const string &imageFilePath);
	int prefetchFrameFile(const string &imageFilePath, const CImageFormat &imageFormat);
	int readFrameBuffer(const string &imageFilePath, MediaFrameBufferSharedPtr &frameBuffer);
	int readFrameBuffer(const string &imageFilePath, MediaFrameBufferSharedPtr &frameBuffer, const CImageFormat &imageFormat);
	int writeFrameBuffer(const string &imageFilePath, MediaFrameBufferSharedPtr frameBuffer);
	int getImageFormat(const string &imageFilePath, CImageFormat &imageFormat);
	string dumpFileHeader(const string &imageFilePath);
	int dumpFileHeaderToString(const string &imageFilePath, string &headerDumpString);

	static int MoveImage(const string &fromFileName, const string &toFileName);
	static int HackImage(const string &imageFileName, int fileHackCommand);
	static int DestroyImage(const string &imageFileName);

	static MediaFrameFileReader *CreateMediaFrameFileReader(const string &filename);
	static MediaFrameFileWriter *CreateMediaFrameFileWriter(const string &filename);


	static void ClearAllCaches();

	static int SetFrameReadCacheSize(size_t sizeInPages, /* out */ size_t *actualNumberOfPagesAllocated);
	static void LockReadCacheFramesInMemory(bool flag);
	static void ClearFrameReadCache();
	static void SetFrameReadCacheHints(int currentFrameIndex,int markInIndex, int markOutIndex);
	static size_t GetFrameReadCacheSizeInBytes();
	static void AddFrameToReadCache(const string &filename, int frameIndex, MediaFrameBufferSharedPtr buffer);
	static MediaFrameBufferSharedPtr RetrieveFrameFromReadCache(const string &filename);
	static void RemoveFrameFromReadCache(const string &filename);

	static void PutFrameToWriteCache(const string &filename, MediaFrameBufferSharedPtr buffer);

	static void Shutdown();

private:
	static MediaFrameBufferWriteCache FrameBufferWriteCache;
};

#if 0

/////////////////////////////////////////////////////////////////////////
// Forward Declarations

class CImageFormat;
class CThreadLock;

/////////////////////////////////////////////////////////////////////////

enum MTI_WHENCE
{
   BEGINNING_OF_FILE, PRESENT_POSITION_IN_FILE, END_OF_FILE
};

#include "ImageFileIO_DPX.h"
#include "ImageFileIO_TIF.h"
#include "ImageFileIO_EXR.h"


/*
 Supported file types.  This is a bit field that can be OR-ed together
 */

#define FILE_TYPE_NONE 0x0
// general information about file types
#define FILE_TYPE_SWAP_HEADER_BYTES         0x1
#define FILE_TYPE_YUV_SHIFT_HACKED          0x1
#define FILE_TYPE_SWAP_IMAGE_BYTES          0x2
#define FILE_TYPE_COMPRESSED_RLE            0x4
#define FILE_TYPE_SPEED_WARNING             0x8
#define FILE_TYPE_COLOR_MAP                0x10

#define FILE_TYPE_ORIENTATION_UPPER_LEFT   0x20
#define FILE_TYPE_ORIENTATION_LOWER_LEFT   0x40
#define FILE_TYPE_ORIENTATION_UPPER_RIGHT  0x80
#define FILE_TYPE_ORIENTATION_LOWER_RIGHT 0x100

#define FILE_TYPE_CINTEL_DPX_ALPHA_HACK   0x200

// specific file types
#define FILE_TYPE_DPX     0x2000
#define FILE_TYPE_TIFF    0x4000
#define FILE_TYPE_EXR   0x100000

// file hacks
#define FILE_HACK_HIDE_ALPHA_MATTE    0x1
#define FILE_HACK_SHOW_ALPHA_MATTE    0x2
#define FILE_HACK_DESTROY_ALPHA_MATTE 0x3
#define FILE_HACK_CACHE_FRAME         0x4

struct RawMediaFileBuffer
{
   void *buffer = nullptr;
   size_t size = 0;
};

typedef shared_ptr<RawMediaFileBuffer> RawMediaFileBufferSharedPtr;




class MTI_MEDIAFILEIODLL_API MediaFrameFileReader
{
public:

	// Factory
	static MediaFileIO *CreateReader(const string &filename);

	// Virtual destructor
	virtual ~MediaFrameFileReader();

	// Read the file and deduce ImageFormat.
	virtual int FetchRawFrameFile();

	virtual CImageFormat GetImageFormat();
	virtual void SetImageFormat(const CImageFormat &newImageFormat);
	virtual MediaFileType GetMediaFileType();
	virtual size_t GetImageDataSize();

	// Return a frame buffer that contains interleaved RGB components
	// with an optional separate Alpha layer.
	virtual int GetUncompressedFrame(MediaFrameBuffer *&outFrame);

private:
	MediaFrameFileReader();

private:
	CImageFormat _imageFormat;
	string _filename;
	RawMediaFrameBufferSharedPtr _rawMediaFrameBufferSharedPtr = nullptr;
};

class MTI_MEDIAFILEIODLL_API MediaFileReader_EXR : public MediaFileReader
{
}

class MediaFileDecoder
{
   static MediaFileDecoder CreateDecoder(const string &filename);
   static MediaFrameBufferDecode(const string &filename);
};



class MTI_MEDIAFILEIODLL_API CImageFileIO
{
public:
   CImageFileIO();
   ~CImageFileIO();

   // OK
   int InitFromExistingFile(const string &strFileName);

   int DestroyImage(const string &strFileName);
	int MoveImage(const string &fromFileName, const string &toFileName);

   // New
	MediaFrameBuffer *GetMediaFrameBuffer(const string &fileName, int frameIndex, int &retVal);
   int ReadMediaFrameData(const string &fileName, int frameIndex, unsigned char *userBuffer);

   bool CanDoAsynchReadImage();
   int ReadImageNew_Asynch_Start(const string &strFileName, int iFrameIndex, MTI_UINT8 **ucpData, void * &readContext, bool dontNeedAlpha);
   bool Is_ReadImageNew_Asynch_Done(void *readContext);
   int ReadImageNew_Asynch_Finish(void * &readContext, size_t &dataOffset, CThreadLock *threadLock);

   bool canReadFrameBuffers();
	int startAsyncFrameBufferRead(const string &filename, void* &readContext);
   bool isAsyncFrameBufferReadDone(void* readContext);
   int finishAsynchFrameBufferRead(void* &readContext, MediaFrameBuffer* &frameBufferOut, CThreadLock * threadLock);

   // Legacy
   int WriteImage(const string &strFileName, int iFrameIndex, MTI_UINT8 **ucpData, bool bNoCache = false);
   int ReadImage(const string &strFileName, int iFrameIndex, MTI_UINT8 **ucpData);
   int ReadImageNew(const string &strFileName, int iFrameIndex, MTI_UINT8 **ucpData, size_t &dataOffset);

   // Questionable
   int InitFromImageFormat(const CImageFormat &ifImageFormat, int iFileType);
//   int OverrideImageFormat(const CImageFormat &ifImageFormat);
   int HackImage(const string &strFileName, int fileHackCommand);

   // No

//   int ReadTwoImages(const string &strFileName1, const string &strFileName2, int iFrameIndex, MTI_UINT8 **ucpData, size_t &dataOffset);
//   int WriteTwoImages(const string &strFileName1, const string &strFileName2, int iFrameIndex, MTI_UINT8 **ucpData);

   int CreateNewImage(const string &strFileName, int iFrameIndex, MTI_UINT8 **ucpData);
//   int CreateTwoNewImages(const string &strFileName1, const string &strFileName2, int iFrameIndex, MTI_UINT8 **ucpData);


   int DumpHeader(const string &strFileName);
   int GetHeaderDumpSize(const string &strFileName);
   int DumpHeaderToCharArray(char *headerDump, const string &strFileName);
   int GetHeader(MTI_UINT8 *ucpBuffer, MTI_UINT32 ulBufferSize);
   int PutHeader(const MTI_UINT8 *ucpBuffer, MTI_UINT32 ulBufferSize);

   CImageFormat *getImageFormatPtr() const ;
   int getFileType() const ;
   int getFileType(const string &strFileName) const ;
   int getBodySize() const ;
   MTI_INT64 getAlignedBufferSize() const ;

   size_t getMaxDataOffset() const ;
   MTI_UINT32 getUnpaddedFileLength() const ;

   int InitImageFormat(CImageFormat *ifp, int iNRow, int iNCol, EPixelPacking iPacking, EPixelComponents iComponent,
         EImageFormatType iFormat, bool bInterlacedOK = false);

   static bool isCompatibleFormat(int fileType, CImageFormat *imageFormat);

   void setFrame0Timecode(CTimecode newTimecode);
   CTimecode getTimecode(MTI_INT32 iFrameIndex);

   void CopySourceFileHeaderHack(const char *cpHeaderSourceFile);

   // Move to cache module
	static int SetFrameCacheSize(size_t sizeInPages, size_t *actualNumberOfPagesAllocated = nullptr);
	static size_t GetFrameCacheSizeInBytes();
   // Hack to be able to show on the TimeLine what frames are cached
   static int GetFrameCacheSerialNumber();
   static const IntegerList &GetListOfCachedImageBodyFrameIndices();
   static void SetFrameCacheHints(int currentFrame, int markIn, int markOut);
	static void LockCacheFramesInMemory(bool flag);
   static void ClearAllCaches();

   // No
   // Callers MUST LOCK THIS lock before calling anything else in here
   CThreadLock &getLock()
   {
      return _threadLock;
   };

   // Called at exit by mainWindowUnit so we can clean up caches
   static void Shutdown();


private:
   int OpenFile(const string &strFileName, int iFrameIndex, int iFlag, EIoStyle ioStyleArg = IO_STYLE_DEFAULT);
   int ReopenFileBuffered();
   int CloseFile(void);
   int ReadHeader(const string &strFileName, int iFrameIndex);
   int InitFromImageFormat(const CImageFormat &ifImageFormat, int iFileType, const string &strFileName);

   MTI_INT64 getDataOffset() const ;

   void ClearFormat();
   MTI_INT64 getSectorAlignedSize(const char *caFilename, MTI_INT64 llFileSize, MTI_UINT32 *sectorSize = NULL);
//   void ExpandYToYYY(MTI_UINT8 *buffer, int offsetToDataInBuffer) { ExpandYToYYY(buffer, offsetToDataInBuffer, buffer, offsetToDataInBuffer); };
//   void ExpandYToYYY(MTI_UINT8 *srcBuffer, int srcDataOffset, MTI_UINT8 *dstBuffer, int dstDataOffset);
//   void CompressYYYToY(MTI_UINT8 *buffer, int offsetToDataInBuffer) { CompressYYYToY(buffer, offsetToDataInBuffer, buffer, offsetToDataInBuffer); };
//   void CompressYYYToY(MTI_UINT8 *srcBuffer, int srcDataOffset, MTI_UINT8 *dstBuffer, int dstDataOffset);

   int AllocZeros(long lNByte);
   void setCharArray(MTI_INT8 *cpResult, int iN, const char *cpString);
   void setCharArray(MTI_UINT8 *ucpResult, int iN, const char *cpString);


   bool MTI_lseek(MTI_UINT64 u64SeekPos);

private:

   MTI_INT64 MTI_lseek_ex(MTI_INT64 u64SeekPos, MTI_WHENCE whence);
   bool MTI_truncate(const string strFileName, MTI_UINT64 u64SeekPos);
   MTI_INT32 MTI_read(void *vpData, MTI_INT32 i32NumBytes);
   MTI_INT32 MTI_write(void *vpData, MTI_INT32 i32NumBytes, bool bRoundSector);
   MTI_INT32 MTI_chsize(MTI_INT32 i32NumBytes);

   MTI_INT32 MTI_read_chunked(void *vpData, MTI_INT32 i32NumBytes);
   MTI_INT32 MTI_write_chunked(void *vpData, MTI_INT32 i32NumBytes);

   // code found in ImageFileIO_DPX.cpp
   int ReadHeader_DPX(const string &strFileName, int iFrameIndex);
   int ReadHeader_DPX_New(const string &strFileName, int iFrameIndex);
   int ParseHeader_DPX_New(const HEADER_STRUCT_DPX &hsLocal, MTI_INT64 llRealFileSize);
   int CopyFromRawHeader_DPX(const HEADER_STRUCT_DPX *hspSrc, HEADER_STRUCT_DPX *hspDst);
   int CopyToRawHeader_DPX(const HEADER_STRUCT_DPX *hspSrc, HEADER_STRUCT_DPX *hspDst);
   int WriteHeader_DPX();
   void SetHeaderTimecode_DPX(MTI_UINT32 frameOffset);
   int InitHeader_DPX();
   int InitHeader_DPX(const string &strFileName);
   void ClearHeader_DPX();
   void DumpHeader_DPX();
   size_t GetBytesPerFieldInFile_DPX();
   size_t GetBytesPerFieldInFileExcAlpha_DPX();

   int ReadBody_DPX(const string &strFileName, int iFrameIndex,MTI_UINT8 **ucpData);
   int ReadFile_DPX(const string &strFileName, int iFrameIndex, MTI_UINT8 **ucpData, size_t &dataOffset);
   int ReadFile_DPX_Chunked(const string &strFileName, int iFrameIndex, MTI_UINT8 **ucpData);
   bool CanDoAsynchReadFile_DPX();
   int ReadFile_DPX_Asynch_Start(const string &strFileName, int iFrameIndex, MTI_UINT8 **ucpData, void * &opaqueInfo, bool dontNeedAlpha);
   bool Is_ReadFile_DPX_Asynch_Done(void *opaqueInfo);
   int GetAsychBodyBuffer_DPX(void *opaqueInfo, MTI_UINT8* &buffer);
   int ReadFile_DPX_Asynch_Finish(void * &opaqueInfo, size_t &dataOffset, CThreadLock *threadLock);
//   int ReadTwoFiles_DPX(const string &strFileName1, const string &strFileName2, int iFrameIndex, MTI_UINT8 **ucpData, size_t &dataOffset);

   int WriteBody_DPX(MTI_UINT8 **ucpData);
   int WriteBody_DPX_Fast(MTI_UINT8 **ucpData);
   int WriteFile_DPX(MTI_UINT8 **ucpData);
   int WriteFile_DPX_Fast(MTI_UINT8 **ucpData);
   int WriteFile_DPX_New(const string &strFileName, int iFrameIndex, MTI_UINT8 **ucpData);
//   int WriteTwoFiles_DPX(const string &strFileName1, const string &strFileName2, int iFrameIndex, MTI_UINT8 **ucpData);
   int WriteFileAsync_DPX(const string &strFileName, int iFrameIndex, MTI_UINT8 *ucpData, CScatterGatherIO &sgIO);

   int HackImage_DPX(const string &strFileName, int fileHackCommand);
   bool HasHideableAlphaMatte_DPX(HEADER_STRUCT_DPX &header);
   int HideAlphaMatte_DPX(HEADER_STRUCT_DPX &header);
   int ShowAlphaMatte_DPX(HEADER_STRUCT_DPX &header);
   int DestroyAlphaMatte_DPX(HEADER_STRUCT_DPX &header);

   int PositionAlphaMatte_DPX(MTI_UINT8 **ucpData, const HEADER_STRUCT_DPX &hsr, EAlphaMatteType eAlphaMatteType);
   void SuckCintelAlphaMatteFromData_DPX(MTI_UINT8 *ucpData, // NOT UINT8** !
         MTI_UINT32 totPels);
   void PutCintelAlphaMatteBackInData_DPX(MTI_UINT8 *ucpData, // NOT UINT8** !
         MTI_UINT32 totPels);
	int CopyMasterFileAndReplaceBody_DPX(const string &strFileName, const string &strMasterFileName, int iFrameIndex, MTI_UINT8 **ucpData);
   string DumpHeaderToString_DPX(const string &filename);

   void HackIncomingDataForDave(EPixelPacking packing, MTI_UINT8 *ucp, MTI_UINT32 lengthInBytes);

   // code found in ImageFileIO_TIF.cpp
   int ReadHeader_TIFF(const string &strFileName, HEADER_STRUCT_TIF &hsp);
   int ParseHeader_TIFF(const string &filename, HEADER_STRUCT_TIF &hspOut, CImageFormat &imageFormatOut);
   int ReadFile_TIFF(const string &strFileName, MTI_UINT8 **ucpData);
   int CopyMasterFileAndReplaceBody_TIFF(const string &strFileName, const string &strHeaderCopyHackSourceFile, int iFrameIndex,
         MTI_UINT8 **ucpData);
   int InitHeaderForNewFile_TIFF();
//   int WriteNewFile_TIFF(const string &strFileName, MTI_UINT8 **ucpData);
   int WriteFile_TIFF(const string &strFileName, MTI_UINT8 **ucpData);

   int startAsyncFrameBufferRead_TIFF(const string &filename, void* &readContext);
   bool isAsyncFrameBufferReadDone_TIFF(void* readContext);
   int finishAsynchFrameBufferRead_TIFF(void* &readContext, MediaFrameBuffer* &frameBufferOut, CThreadLock * threadLock);

//public:
//   int ReadCallback_TIFF(void *vpData, MTI_INT32 i32NumBytes);
//   int WriteCallback_TIFF(void *vpData, MTI_INT32 i32NumBytes);
//   MTI_INT64 SeekCallback_TIFF(MTI_INT64 off, MTI_WHENCE mti_whence);

private:
   // code found in ImageFileIO_EXR.cpp
   int OpenFile_EXR(const char *cpFilename, int flags);
   void CloseFile_EXR();
   int ReadHeader_EXR(const string &filename, HEADER_STRUCT_EXR &hsp);
	int ParseHeader_EXR(const string &filename, HEADER_STRUCT_EXR &hspOut, CImageFormat &imageFormatOut);
	int InitHeaderForNewFile_EXR();
   int ReadFile_EXR(const string &filename, MTI_UINT8 **ucpData);
   int CopyMasterFileAndReplaceBody_EXR(const string &strFileName, const string &strHeaderCopyHackSourceFile, int iFrameIndex,
         MTI_UINT8 **ucpData);
   int WriteFile_EXR(const string &strFileName, MTI_UINT8 **ucpData);
	int WriteNewFile_EXR(const string &strFileName, MTI_UINT8 **ucpData);

	int startAsyncFrameBufferRead_EXR(const string &filename, void* &readContext);
	bool isAsyncFrameBufferReadDone_EXR(void* readContext);
	int finishAsynchFrameBufferRead_EXR(void* &readContext, MediaFrameBuffer* &frameBufferOut, CThreadLock * threadLock);

//********************************************************************

#define INVALID_FILE_DESCRIPTOR (-1)
	int _iFD = INVALID_FILE_DESCRIPTOR; // file descriptor

	HANDLE _hHandle = INVALID_HANDLE_VALUE; // For Win2K fast File I/O
	void *_globalOpenExrHandle = nullptr;
	CWinMMFIO *_memoryMappedFile = nullptr;
	string _strOpenFileName; // The file name of the open file

	int _iOpenFlags = 0; // The flags used for OpenFile, in case of re-open
	int _iOpenFrameIndex = -1; // the frame index for the file (for DPX cache hack)

	EIoStyle _eIoStyle = IO_STYLE_DEFAULT;
	CImageFormat *_ifpInitializedImageFormat = nullptr;

	long _lCurrentFileType = 0;                        // type of current file
	EAlphaMatteType _eCurrentAlphaMatteType = IF_ALPHA_MATTE_INVALID;       // type of current file's alpha matte
	EAlphaMatteType _eCurrentAlphaMatteHiddenType = IF_ALPHA_MATTE_INVALID; // type of current file's hidden alpha matte
	MTI_INT64 _llCurrentFileLength = 0;                // length of current file
	long _lCurrentBodySize = 0;                        // size of the body of current file
	MTI_INT64 _llCurrentAlignedBufferSize = 0;         // size of the buffer, aligned to disk segment
	MTI_INT64 _llCurrentDataOffset = 0;                // offset from beginning of current file to data

	long _lInitializedFileType = 0;                        // type of reference file
	EAlphaMatteType _eInitializedAlphaMatteType = IF_ALPHA_MATTE_INVALID;       // type of reference file's alpha matte
	EAlphaMatteType _eInitializedAlphaMatteHiddenType = IF_ALPHA_MATTE_INVALID; // type of reference file's hidden alpha matte
	MTI_INT64 _llInitializedFileLength = 0;                // length of reference file
	long _lInitializedBodySize = 0;                        // size of the body of the reference file
	MTI_INT64 _llInitializedAlignedBufferSize = 0;         // size of the reference buffer, aligned to disk segment.
	MTI_INT64 _llInitializedDataOffset = 0;                // offset from beginning of reference file to data

	MTI_INT64 _llSpecialDpxHackBufferSize = 0; // Buffer with 4KB extra space up front
	MTI_INT64 _llSpecialDpxHackOffset = 0;     // 4K before first read, then real offset % 4K
														// I'm sorry, it is too hard to figure out the
														// right way to do this....

	unsigned long _ulCurrentBytesPerSector; // Number of Bytes Per Sector for last check

	CTimecode _tcInitializedTimecode = CTimecode(1, 0, 0, 0, 0, 24); // for timecode hack
	bool _bTimecodeWasSet = false; // for timecode hack

	string _strHeaderCopyHackSourceFile; // Dave's DPX Header Copy Hack

	bool _bTryToCopyHeaderInfoFromAnotherFile = false; // Dave's DPX Header Copy Hack

	HEADER_STRUCT_DPX _hsInitializedDPX;

	HEADER_STRUCT_TIF _hsInitializedTIF;

	HEADER_STRUCT_EXR _hsInitializedEXR;

	CScatterGatherIO *_sgioDPX; // Special I/O module for DPX files

	bool _bFileIsOpenBuffered = false; // stupid hack flag for fast tiff mode

   // Single-frame write-behind DPX image body write cache
	static CImageBodyWriteCache _ImageBodyWriteCache;

//   // Multi-frame DPX image body read cache
//   static FrameCache FrameCache;

   // Multi-frame DPX image header read cache
	static CDPXImageHeaderReadCache _DPXImageHeaderReadCache;

   // Multi-frame TIFF image header read cache
	static CTIFFImageHeaderReadCache _TIFFImageHeaderReadCache;

   // Multi-frame EXR image header read cache
	static CEXRImageHeaderReadCache _EXRImageHeaderReadCache;

   // Hints for input to cache policy decisions
	static int _iCurrentFrameIndexHint;
	static int _iMarkInFrameIndexHint;
	static int _iMarkOutFrameIndexHint;

   static IntegerList _listOfCachedFrameIndices;

   // Callers MUST LOCK THIS before calling anything else in here
   // QQQ is it OK that this is not STATIC??   Let's change it to be static!!
   static CThreadLock _threadLock;
};
#endif

#endif

