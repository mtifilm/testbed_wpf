//---------------------------------------------------------------------------

#include "RawMediaFrameBuffer.h"

#include "DllSupport.h"
#include "HRTimer.h"
#include "IniFile.h"
#include "SysInfo.h"
#include <stddef.h>    // for _threadid macro
//---------------------------------------------------------------------------

std::atomic_size_t    RawMediaFrameBuffer::_TotalAllocatedSize = 0;
std::atomic_size_t    RawMediaFrameBuffer::_TotalBufferCount = 0;
std::atomic_int     RawMediaFrameBuffer::_PreviousBufferId = 0;

deque<RawMediaFrameBuffer::FreeListEntry> RawMediaFrameBuffer::_FreeList;
CSpinLock RawMediaFrameBuffer::_AllocBufferLock;
CSpinLock RawMediaFrameBuffer::_FreeListLock;
CSpinLock RawMediaFrameBuffer::_RequestingMemoryReleaseLock;
size_t    RawMediaFrameBuffer::_FreeListByteCount = 0;
size_t    RawMediaFrameBuffer::_FreeListBufferCount = 0;
size_t    RawMediaFrameBuffer::_FreeListByteAllowance = 0;

// These initial values will be replaced by Preferences. Initialized because
// of a timing issue where we may try to allocate buffers before the real
// values are set. SHOULD FIX THE TIMING ISSUE! QQQ
size_t    RawMediaFrameBuffer::_TotalNumberOfLockableBytes = 1024 * 1024 * 1024;
bool      RawMediaFrameBuffer::_ReallyLockPagesInMemory = false;

Event     RawMediaFrameBuffer::_PreallocThreadKicker;
void *    RawMediaFrameBuffer::_PreallocThreadId = nullptr;
size_t    RawMediaFrameBuffer::_LastRequestedSize = 0;
bool      RawMediaFrameBuffer::_LastRequestedSizeChanged = false;

CBHook RawMediaFrameBuffer::RanOutOfLockableMemory;

inline string bytes2gbString(size_t bytes)
{
   const size_t oneGB = 1024 *1024 * 1024;
   int gb = bytes / oneGB;
   int millions = (bytes % oneGB) / (1024 * 1024);
   MTIostringstream os;
   os << gb << "." << std::setw(3) << std::setfill('0') << millions;
   return os.str();
}

#define ENABLE_LOCK_TRACING
#ifdef ENABLE_LOCK_TRACING
#define TRACE_LOCKS(a) TRACE_3(errout << "[" << _threadid << "] LOCKTRACE" << a)
#define START_LOCK_TIMER  CHRTimer lockTimer
#define RESTART_LOCK_TIMER  lockTimer.Start()
#else
#define TRACE_LOCKS(a)
#define START_LOCK_TIMER
#define RESTART_LOCK_TIMER
#endif

//---------------------------------------------------------------------------

const int FreeListMaxEntries = 20;
const int FreeListLowWaterMark = 3;
const int FreeListHighWaterMark = 10;
const size_t SystemPageSize = SysInfo::GetSystemPageSize();


#define VIRTUALALLOC_GRANULARITY 65536
//---------------------------------------------------------------------------

RawMediaFrameBuffer::RawMediaFrameBuffer(size_t size)
{
   _bufferId = ++_PreviousBufferId;
   TRACE_3(errout << "[" << _threadid << "] RMFB: CREATE BUFFER " << _bufferId);

	MTIassert(size <= (0x7FFFFFFF - VIRTUALALLOC_GRANULARITY));
   _dataSize = (MTI_UINT32)size;
	_bufferSize = VIRTUALALLOC_GRANULARITY *
						(MTI_UINT32)((((MTI_UINT64)_dataSize) + VIRTUALALLOC_GRANULARITY - 1)
						/ VIRTUALALLOC_GRANULARITY);
	CHRTimer timer;
	_bufferPtr = AllocateBuffer(_bufferSize);
	if (_bufferPtr == nullptr)
	{
		// Allocation failed!!?!
      TRACE_0(errout << "INTERNAL ERROR: Raw media frame buffer allocation FAILED!");
		_bufferSize = 0;
	}

	TRACE_3(errout << "[" << _threadid << "] RMFB: RawMediaFrameBuffer(" << size << " => " << _bufferSize << ") construction took " << timer.Read() << " msec");
}
//---------------------------------------------------------------------------

RawMediaFrameBuffer::~RawMediaFrameBuffer()
{
   TRACE_3(errout << "[" << _threadid << "] RMFB: DESTROY BUFFER " << _bufferId);

   if (_bufferPtr != nullptr)
   {
      CHRTimer timer;
		FreeBuffer(_bufferPtr, _bufferSize);
		TRACE_3(errout << "[" << _threadid << "] RMFB: FreeBuffer() took " << timer.Read() << " msec");
	}
}
//---------------------------------------------------------------------------

/* static */
void *RawMediaFrameBuffer::AllocateBuffer(size_t size)
{
   START_LOCK_TIMER;
   TRACE_LOCKS("_AllocBufferLock (AllocateBuffer) WAIT");
   CAutoSpinLocker allocLock(_AllocBufferLock);  // first!
   TRACE_LOCKS("_AllocBufferLock (AllocateBuffer) LOCKED, waited " << lockTimer.ReadAsString() << " msec");
   RESTART_LOCK_TIMER;
   TRACE_LOCKS("_FreeListLock (AllocateBuffer) WAIT");
	CAutoSpinLocker freeLock(_FreeListLock);
   TRACE_LOCKS("_FreeListLock (AllocateBuffer) LOCKED, waited " << lockTimer.ReadAsString() << " msec");

   TRACE_3(errout << "[" << _threadid << "] ENTRY AllocateBuffer(" << size << ")");
	void *bufferPtr = nullptr;

   if (_LastRequestedSize != size)
   {
      _LastRequestedSize = size;
      _LastRequestedSizeChanged = true;
   }

   while (true)
   {
      //--------------
      // Best case: find a suitable buffer on the free list.
      auto iter = _FreeList.begin();
      for (; iter != _FreeList.end(); ++iter)
      {
         if (iter->size == size)
         {
            break;
         }
      }

      if (iter != _FreeList.end())
      {
         // Yay! We're good to go!
         bufferPtr = iter->address;
         _FreeList.erase(iter);
         _FreeListByteCount -= size;

         TRACE_3(errout << "[" << _threadid << "] RMFB AllocateBuffer: found a suitable buffer on the free list");
	      dumpFreeList();

         break;
      }

      //----------------
      // No suitable buffer found - if there isn't enough memory available
      // to create a buffer of the size we want, see if we can release enough
      // memory by destroying free list buffers that are the wrong size.
      if ((_TotalAllocatedSize + size) > _TotalNumberOfLockableBytes
      && (_TotalAllocatedSize - _FreeListByteCount + size) <= _TotalNumberOfLockableBytes)
      {
         TRACE_3(errout << "[" << _threadid << "]  RMFB AllocateBuffer() Destroying existing free buffers: ("
                        << bytes2gbString(_TotalAllocatedSize)
                        << " - " <<  bytes2gbString(_FreeListByteCount)
                        << " + " << bytes2gbString(size) << ") <= "
                        << bytes2gbString(_TotalNumberOfLockableBytes));

         // Free up non-matching buffers until there's enough space or we run out of buffers to free.
         while (((_TotalAllocatedSize + size) > _TotalNumberOfLockableBytes)
         && !_FreeList.empty())
         {
            TRACE_3(errout << "[" << _threadid << "]  RMFB AllocateBuffer() destroying a free list buffer");
            DestroyOldestBuffer();
         }
      }

      //----------------
      // If there's still no room to create a new buffer, need to ask the cache
      // to release something. After a buffer is released, we return to the top
      // of this loop to see if we can just use the newly freed buffer.
      if ((_TotalAllocatedSize + size) > _TotalNumberOfLockableBytes)
      {
         int sanityCount = 100;
         size_t oldFreeListByteCount = _FreeListByteCount;
         while (oldFreeListByteCount == _FreeListByteCount && --sanityCount > 0)
         {
            TRACE_3(errout << "[" << _threadid << "]  RMFB AllocateBuffer() Asking the cache to free up a buffer");
            TRACE_LOCKS("_FreeListLock UNLOCK (notify)");
            CAutoSpinUnlocker freeUnlock(_FreeListLock);
            RanOutOfLockableMemory.Notify();
            TRACE_LOCKS("_FreeListLock RELOCK (notify)");
         }

         MTIassert(sanityCount > 0);
         if (sanityCount == 0)
         {
            TRACE_0(errout << "ERROR: OUT OF MEMORY! Can't free up memory for a new buffer!");
            break;
         }

         // Something was freed. Return to the top of the loop to check it out.
         continue;
      }

      //----------------
      // OK, there is room to create a new buffer.
      {
         TRACE_LOCKS("_FreeListLock (AllocateBuffer) UNLOCK 2");
         CAutoSpinUnlocker freeUnlock(_FreeListLock);

         // No buffer found with the correct size, so create a new one.
         TRACE_3(errout << "[" << _threadid << "] RMFB: SYNCHRONOUS CreateBuffer(" << size << ")");
         bufferPtr = CreateNewBuffer(size);

         TRACE_LOCKS("_FreeListLock (AllocateBuffer) RELOCK 2");
         break;
      }
   }

   if (_PreallocThreadId == nullptr)
   {
      BThreadSpawn(RunPreallocThread, nullptr);
   }

   _PreallocThreadKicker.signal();

   TRACE_LOCKS("_FreeListLock (AllocateBuffer) RELEASE");
   TRACE_LOCKS("_AllocBufferLock (AllocateBuffer) RELEASE");
	return bufferPtr;
}
//---------------------------------------------------------------------------

/* static */
void *RawMediaFrameBuffer::CreateNewBuffer(size_t size)
{
	// Caller must ensure _FreeListLock is UNLOCKED!
	// Caller must ensure _AllocBufferLock is LOCKED!
   TRACE_3(errout << "[" << _threadid << "] RMFB CreateNewBuffer(" << size << ")");

	void *bufferPtr = AllocateBufferMemory(size);
	if (bufferPtr == nullptr)
	{
		// Ugh, VirtualAlloc failed??
		// Retry after getting rid of all the free buffers.
      TRACE_3(errout << "[" << _threadid << "] RMFB FREELIST: "
                     << ((GetFreeListBufferCount() > 0)
                        ? "Destroy all free list buffers afer VirtualAlloc fail"
                        : "NO BUFFERS TO DESTROY ON FREE LIST!"));
      {
         START_LOCK_TIMER;
         TRACE_LOCKS("_FreeListLock (Create) WAIT");
         CAutoSpinLocker lock(_FreeListLock);
         TRACE_LOCKS("_FreeListLock (Create LOCKED, waited " << lockTimer.ReadAsString() << " msec");
         while (GetFreeListBufferCount() > 0)
         {
            DestroyOldestBuffer();
         }

         TRACE_LOCKS("_FreeListLock (Create) RELEASE");
      }

      TRACE_3(errout << "[" << _threadid << "] RMFB RETRY AllocateBufferMemory(" << size << ")");
		bufferPtr = AllocateBufferMemory(size);
	}

	return bufferPtr;
}
//---------------------------------------------------------------------------

/* static */
void *RawMediaFrameBuffer::AllocateBufferMemory(size_t size)
{
	// Caller must ensure _FreeListLock is UNLOCKED!
	// Caller must ensure _AllocbufferLock is LOCKED!

   TRACE_3(errout << "[" << _threadid << "] RMFB: AllocateBufferMemory(" << size << ")");

   MTIassert(size > 0);
   if (size == 0)
   {
      TRACE_0(errout << "INTERNAL ERROR: RawMediaFrameBuffer::AllocateBufferMemory() called with size 0!");
      return nullptr;
   }

   // Do these before unlocking - assume this is going to work!
	_TotalAllocatedSize += size;
	++_TotalBufferCount;
   int lastError = 0;
   void *bufferPtr = nullptr;
   string errorMessage;
   {
      TRACE_LOCKS("_AllocbufferLock (AllocateBufferMemory) UNLOCK");
      CAutoSpinUnlocker allocUnlock(_AllocBufferLock);

      CHRTimer timer;
      double allocTime = 0.0;
      double touchTime = 0.0;
      double pinTime = 0.0;

      bufferPtr = ::VirtualAlloc(nullptr, size, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
      if (bufferPtr == nullptr)
      {
         lastError = ::GetLastError();
         errorMessage = GetSystemMessage(lastError);
         TRACE_0(errout << "ERROR: VirtualAlloc(" << size << ") failed because: " << errorMessage << " (" << lastError << ")");
         _TotalAllocatedSize -= size;
         --_TotalBufferCount;
         TRACE_LOCKS("_AllocbufferLock (AllocateBufferMemory) RELOCK");
         return nullptr;
      }

      allocTime = timer.Read();

      TRACE_3(errout << "[" << _threadid << "] RMFB: total buffer count = " << _TotalBufferCount
                     << ", total size = (" << bytes2gbString(_TotalAllocatedSize)
                     << " / " << bytes2gbString(_TotalNumberOfLockableBytes) << " gb)");

      if (_TotalAllocatedSize > _TotalNumberOfLockableBytes)
      {
         TRACE_0(errout << "ERROR: _TotalAllocatedSize > _TotalNumberOfLockableBytes ("
                        << _TotalAllocatedSize << " > " << _TotalNumberOfLockableBytes << ")");
      }

      // For some unknown reason, if we take the soft page faults up front,
      // pinning the pages is much faster and more consistent in terms of timing.
      for (auto offset = 0; offset < size; offset += SystemPageSize)
      {
         *(reinterpret_cast<char *>(bufferPtr) + offset) = 0;
      }

      touchTime = timer.Read() - allocTime;

      if (_ReallyLockPagesInMemory)
      {
         if (!::VirtualLock(bufferPtr, size))
         {
            int lastError = ::GetLastError();
            string errorMessage = GetSystemMessage(lastError);
            TRACE_0(errout << "ERROR: VirtualLock(" << size << ") failed because: " << errorMessage << " (" << lastError << ")");
         }
      }

      pinTime = timer.Read() - touchTime - allocTime;

      TRACE_3(errout << "[" << _threadid << "] RMFB: ALLOC = " << allocTime << " msec"
                     << ", SOFTFAULT = " << touchTime << " msec"
                     << ", PIN = " << pinTime << " msec");
   }

   TRACE_LOCKS("_AllocbufferLock (AllocateBufferMemory) RELOCK");
	return bufferPtr;
}
//---------------------------------------------------------------------------

/* static */
bool RawMediaFrameBuffer::DestroyOldestBufferThatDoesntMatchSize(size_t protectedSize)
{
   TRACE_3(errout << "[" << _threadid << "] RMFB FREELIST: DESTROY: protected size = " << protectedSize
                  << ", free buffer count = " << _FreeList.size()
                  << ", free list GB = " << bytes2gbString(_FreeListByteCount));

	// Caller must ensure that the _FreeListLock is LOCKED!
	deque<FreeListEntry>::iterator iter = _FreeList.begin();
	for (; iter != _FreeList.end(); ++iter)
	{
		if (iter->size != protectedSize)
		{
			break;
		}
	}

	if (iter == _FreeList.end())
	{
      TRACE_3(errout << "[" << _threadid << "] RMFB FREELIST: Did not find a buffer to destroy!");
		return false;
	}

	FreeListEntry oldestEntry = *iter;
	_FreeList.erase(iter);
	_FreeListByteCount -= oldestEntry.size;
	{
      CHRTimer timer;
      TRACE_LOCKS("_FreeListLock (Destroy) UNLOCK");
		CAutoSpinUnlocker unlock(_FreeListLock);

      _TotalAllocatedSize -= oldestEntry.size;
      --_TotalBufferCount;

		::VirtualFree(oldestEntry.address, 0, MEM_RELEASE);

		TRACE_3(errout << "[" << _threadid << "] RMFB FREELIST: VirtualFree " << (protectedSize?"NONMATCHING":"EXCESS") << " buffer (" << oldestEntry.size << " bytes) took " << timer.ReadAsString() << " msec");
      TRACE_LOCKS("_FreeListLock (Destroy) RELOCK");
	}

	dumpFreeList();

	return true;
}
//---------------------------------------------------------------------------

/* static */
void RawMediaFrameBuffer::DestroyAllFreeBuffers()
{
   CHRTimer lockTimer;
   TRACE_LOCKS("_FreeListLock (DestroyAllFreeBuffers) WAIT");
	CAutoSpinLocker freeLock(_FreeListLock);
   TRACE_LOCKS("_FreeListLock (DestroyAllFreeBuffers) LOCKED, waited " << lockTimer.ReadAsString() << " msec");

   while(DestroyOldestBuffer())
   {
      // Do nothing - DestroyOldestBuffer() returns false when no free buffers left.
   }

   dumpFreeList();

   TRACE_LOCKS("_FreeListLock (DestroyAllFreeBuffers) RELEASE");
}
//---------------------------------------------------------------------------

/* static */
void RawMediaFrameBuffer::FreeBuffer(void *bufferPtr, size_t size)
{
   START_LOCK_TIMER;
   TRACE_LOCKS("_FreeListLock (FreeBuffer) WAIT");
	CAutoSpinLocker lock(_FreeListLock);
   TRACE_LOCKS("_FreeListLock (FreeBuffer LOCKED, waited " << lockTimer.ReadAsString() << " msec");

   TRACE_3(errout << "[" << _threadid << "] RMFB: Adding buffer of size " << size << " to the free list.");
	FreeListEntry newEntry = { bufferPtr, size };
	_FreeList.push_back(newEntry);
	_FreeListByteCount += size;
	if (_FreeList.size() > FreeListMaxEntries)
	{
		_PreallocThreadKicker.signal();
	}

   dumpFreeList();

   TRACE_LOCKS("_FreeListLock (FreeBuffer) RELEASE");
}
//---------------------------------------------------------------------------

void RawMediaFrameBuffer::dumpFreeList()
{
	int s = _FreeList.size();
   TRACE_3(errout << "RMFB FREELIST has " << bytes2gbString(_FreeListByteCount) << " GB in "
                  << _FreeList.size() << " buffers" << endl
  		 				<< " " << ((s>0)?_FreeList[0].size:0)
		 				<< " " << ((s>1)?_FreeList[1].size:0)
						<< " " << ((s>2)?_FreeList[2].size:0)
						<< " " << ((s>3)?_FreeList[3].size:0)
						<< " " << ((s>4)?_FreeList[4].size:0)
						<< " " << ((s>5)?_FreeList[5].size:0)
						<< " " << ((s>6)?_FreeList[6].size:0)
						<< " " << ((s>7)?_FreeList[7].size:0)
						<< " " << ((s>8)?_FreeList[8].size:0)
						<< " " << ((s>9)?_FreeList[9].size:0)
						<< " " << ((s>10)?_FreeList[10].size:0)
						<< " " << ((s>11)?_FreeList[11].size:0)
						<< " " << ((s>12)?_FreeList[12].size:0)
						<< " " << ((s>13)?_FreeList[13].size:0)
						<< " " << ((s>14)?_FreeList[14].size:0)
						<< " " << ((s>15)?_FreeList[15].size:0)
						<< " " << ((s>16)?_FreeList[16].size:0)
						<< " " << ((s>17)?_FreeList[17].size:0)
						<< " " << ((s>18)?_FreeList[18].size:0)
						<< " " << ((s>19)?_FreeList[19].size:0)
						<< " " << ((s>20)?_FreeList[20].size:0)
						<< " " << ((s>21)?_FreeList[21].size:0)
						<< " " << ((s>22)?_FreeList[22].size:0)
						<< " " << ((s>23)?_FreeList[23].size:0)
						<< " " << ((s>24)?_FreeList[24].size:0)
						<< " " << ((s>25)?_FreeList[25].size:0)
						<< " " << ((s>26)?_FreeList[26].size:0)
						<< " " << ((s>27)?_FreeList[27].size:0)
						<< " " << ((s>28)?_FreeList[28].size:0)
						<< " " << ((s>29)?_FreeList[28].size:0)
         );
}
//---------------------------------------------------------------------------

/* static */
size_t RawMediaFrameBuffer::GetFreeListByteCount()
{
	CAutoSpinLocker lock(_FreeListLock);
	return _FreeListByteCount;
}

//---------------------------------------------------------------------------

/* static */
size_t RawMediaFrameBuffer::GetFreeListBufferCount()
{
	CAutoSpinLocker lock(_FreeListLock);
	return _FreeList.size();
}

//---------------------------------------------------------------------------

/* static */
void RawMediaFrameBuffer::SetLockableMemorySizes(size_t totalNumberOfLockableBytes, size_t freeListAllowance)
{
	CAutoSpinLocker lock(_FreeListLock);
   _TotalNumberOfLockableBytes = totalNumberOfLockableBytes;
	_FreeListByteAllowance = freeListAllowance;
}
//---------------------------------------------------------------------------

/* static */
void RawMediaFrameBuffer::SetReallyLockPagesInMemory(bool flag)
{
   if (_ReallyLockPagesInMemory == flag)
   {
      return;
   }

   _ReallyLockPagesInMemory = flag;

   //Could just lock or unlock the buffers, but this is easier.
   DestroyAllFreeBuffers();
   _PreallocThreadKicker.signal();  // Make some new free buffers!
}
//---------------------------------------------------------------------------

/* static */
bool RawMediaFrameBuffer::GetReallyLockPagesInMemory()
{
   return _ReallyLockPagesInMemory;
}
//---------------------------------------------------------------------------

void RawMediaFrameBuffer::RunPreallocThread(void *vpAppData, void *vpReserved)
{
	// "Reserved" parameter is the thread ID. We don't use the AppData parameter.
	_PreallocThreadId = vpReserved;

   int retVal = BThreadBegin(vpReserved);
	MTIassert(retVal == 0);

//	bool throttlingBack = false;

   START_LOCK_TIMER;
   TRACE_LOCKS("_FreeListLock (Prealloc) WAIT");
	CAutoSpinLocker lock(_FreeListLock);
   TRACE_LOCKS("_FreeListLock (Prealloc) LOCKED, waited " << lockTimer.ReadAsString() << " msec");
	while (true)
	{
		_PreallocThreadKicker.reset();
		size_t lastRequestedSize = _LastRequestedSize;   // don't want this changing on us here
		if (lastRequestedSize > 0)
		{
			// Create 0, 1 or 2 new free buffers.
			for (auto i = 0; i < 2; ++i)
			{
				int lastRequestedSizeCount = 0;
				for (auto iter = _FreeList.begin(); iter != _FreeList.end(); ++iter)
				{
					if (iter->size == lastRequestedSize)
					{
						++lastRequestedSizeCount;
					}
				}
//
//				if (throttlingBack && lastRequestedSizeCount >= FreeListLowWaterMark)
//				{
//					break;
//				}
//
//				if (!throttlingBack
//            && (lastRequestedSizeCount >= FreeListMaxEntries || _FreeListByteCount > _FreeListByteAllowance))
//				{
//					throttlingBack = true;
//					break;
//				}
//
//				if (throttlingBack
//            && lastRequestedSizeCount < FreeListMaxEntries
//            &&  _FreeListByteCount <= _FreeListByteAllowance)
//				{
//					throttlingBack = false;
//            }

				// Maybe destroy the oldest entry if the list is full and the number
            // of free buffers at the last requested size is below the low water mark.
				if (lastRequestedSizeCount < FreeListLowWaterMark
            && lastRequestedSizeCount < _FreeList.size()
				&& (_FreeList.size() >= FreeListMaxEntries
                  //|| (_FreeListByteCount + lastRequestedSize) >= _FreeListByteAllowance
                  || (_TotalAllocatedSize + lastRequestedSize) >= _TotalNumberOfLockableBytes))
				{
               TRACE_3(errout << "[" << _threadid << "] RMFB PREALLOC THREAD: Destroy oldest buffer - current size count = " << lastRequestedSizeCount
                              << " < 4?  free list count: " << _FreeList.size() << " >= " << FreeListMaxEntries
                              ////<< "?  byte count: " << _FreeListByteCount << " >= " << _FreeListByteAllowance
                              << "?  size of oldest buffer = " <<  _FreeList.front().size
                              << " != " << lastRequestedSize << "?");
					 DestroyOldestBufferThatDoesntMatchSize(lastRequestedSize);
				}

            // Maybe create a new buffer to add to the free list if there's room
            // and the number of free buffers at the last requested size is below
            // the high water mark.
            if (lastRequestedSizeCount < FreeListHighWaterMark
            && _FreeList.size() < FreeListMaxEntries
            ////&& (_FreeListByteCount + lastRequestedSize) < _FreeListByteAllowance
            && (_TotalAllocatedSize + lastRequestedSize) < _TotalNumberOfLockableBytes)
            {
               TRACE_3(errout << "[" << _threadid << "] RMFB PREALLOC THREAD: CreateBuffer - "
                     << "current size count = " << lastRequestedSizeCount << " < 3?  "
                     << "free list count: " << _FreeList.size() << " < " << FreeListMaxEntries << "?  "
//                     << "free list bytes: (" << bytes2gbString(_FreeListByteCount) << " gb + "
//                        << lastRequestedSize << " bytes) < " << bytes2gbString(_FreeListByteAllowance) << " gb?  "
                     << "total locked: (" << bytes2gbString(_TotalAllocatedSize) << " gb + "
                        << lastRequestedSize << " bytes) < " << bytes2gbString(_TotalNumberOfLockableBytes) << " gb?  ");
               {
                  TRACE_LOCKS("_FreeListLock (Prealloc) UNLOCK 1");
                  CAutoSpinUnlocker unlock(_FreeListLock);    // BEFORE  allocLock
                  RESTART_LOCK_TIMER;
                  TRACE_LOCKS("_AllocBufferLock (Prealloc) WAIT");
                  CAutoSpinLocker allocLock(_AllocBufferLock);
                  TRACE_LOCKS("_AllocBufferLock (Prealloc LOCKED, waited " << lockTimer.ReadAsString() << " msec");

                  void *bufferPtr = CreateNewBuffer(lastRequestedSize);

                  if (bufferPtr != nullptr)
                  {
                     // FreeBuffer will lock _FreeListLock.
                     // It puts the newly created buffer onto the free list for us.
                     FreeBuffer(bufferPtr, lastRequestedSize);
                  }


                  TRACE_LOCKS("_AllocBufferLock (Prealloc) RELEASE 1");
                  TRACE_LOCKS("_FreeListLock (Prealloc) RELOCK 1");
               }

               // Not sure if this is useful, but yield in case we might
               // be blocking other VirtualAllocs from taking place.
               {
                  TRACE_LOCKS("_FreeListLock (Prealloc) UNLOCK 2");
                  CAutoSpinUnlocker unlock(_FreeListLock);
                  MTImillisleep(2);
                  TRACE_LOCKS("_FreeListLock (Prealloc) RELOCK 2");
               }
            }
			}
		}

		while (_FreeList.size() > FreeListMaxEntries /* || _FreeListByteCount > _FreeListByteAllowance */)
		{
         TRACE_3(errout << "[" << _threadid << "] RMFB PREALLOC THREAD: Destroy some non-current-sized buffers - "
                        << "free list count: " << _FreeList.size() << " > " << FreeListMaxEntries
                        << "?  byte count: " << _FreeListByteCount << " > " << _FreeListByteAllowance
                        << "?");
			if (!DestroyOldestBufferThatDoesntMatchSize(lastRequestedSize))
			{
            TRACE_3(errout << "[" << _threadid << "] RMFB PREALLOC THREAD: Destroy some any-size buffers - "
                        << "free list count: " << _FreeList.size() << " > " << FreeListMaxEntries
                        << "?  byte count: " << _FreeListByteCount << " > " << _FreeListByteAllowance
                        << "?");
				DestroyOldestBuffer();
			}
		}

		{
         ////TRACE_LOCKS("_FreeListLock (Prealloc) UNLOCK 3");
			CAutoSpinUnlocker unlock(_FreeListLock);
			_PreallocThreadKicker.wait(250);
         ////TRACE_LOCKS("_FreeListLock (Prealloc) RELOCK 3");
		}

      static size_t oldWsSize = 0;
      auto wsSize = SysInfo::GetWorkingSetSize();
      const size_t oneGB = 1024 * 1024 * 1024;

      if (wsSize > (oldWsSize + (oneGB / 4)) || wsSize < (oldWsSize - (oneGB / 4)))
      {
         oldWsSize = wsSize;
         TRACE_1(errout << "WORKING SET SIZE = " << bytes2gbString(wsSize) << " GB");
      }

#ifdef _DEBUG
      static bool oneShotFlag = true;
      if (oneShotFlag)
      {
         auto physicalMemorySizeInGB = SysInfo::GetPhysicalMemorySizeInGB();
         if (wsSize > ((physicalMemorySizeInGB - 6) * oneGB))
         {
            CAutoErrorReporter autoErr("SysInfo::GetWorkingSetSize()", AUTO_ERROR_NOTIFY_NO_WAIT, AUTO_ERROR_TRACE_0);
            autoErr.errorCode = -1;
            autoErr.msg << "The working set size is getting dangerously large (" << bytes2gbString(wsSize) << " GB)." << endl
                        << "You should not be seeing this, and there's nothing you can do about it." << endl
                        << "Just keep working, you'll most probably be OK!";
            oneShotFlag = false;
         }
      }
#endif
	}
}
//---------------------------------------------------------------------------
