#include "MediaFileIO.h"

#include "err_file.h"
#include "MTImalloc.h"
#include "RawMediaFrameBuffer.h"

MediaFrameBufferWriteCache MediaFileIO::FrameBufferWriteCache;


//------------------------------------------------------------------------------
namespace
{
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

string makeBackupFileName(const string &fileName)
{
	// TODO: MAKE IT A HIDDEN FILE (start with ".")
	char tmpStr[64];
	time_t ltime;
	time(&ltime);
	sprintf(tmpStr, "%lx", (long)ltime);

	return fileName + "." + tmpStr + ".BAK";
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

int checkFileAccessibility(const string &fileName)
{
	// Determine if the named file exists and is statable,
	// don't care if it is a regular file, a directory or whatever.

	// Get file status information
	struct stat buf;
	int result = stat(fileName.c_str(), &buf);
	if (result != 0)
	{
		// Not accessible
		return FILE_ERROR_STAT;
	}

	return 0; // File exists
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

int myRenameFile(const string &oldName, const string &newName)
{
	// Rename the file - retVal 0 means failure!
	int retVal = MoveFile(oldName.c_str(), newName.c_str());

	if (retVal == 0)
	{
		TRACE_1(errout << "WARNING: Failed to rename image " << oldName << endl
							<< "         to " << newName << endl
							<< "         Reason: " << GetLastSystemErrorMessage());

		return FILE_ERROR_RENAME;
	}

	return 0;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

int myDeleteFile(const string &fileName)
{
	// Delete the file - retVal 0 means failure!
	int retVal = DeleteFile(fileName.c_str());

	if (retVal == 0)
   {
		TRACE_1(errout << "WARNING: Failed to delete file " << fileName << endl
							<< "         Reason: " << GetLastSystemErrorMessage());

      return FILE_ERROR_DELETE;
	}

	return 0;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

inline string uint2hex(unsigned int value)
{
   MTIostringstream hexValue;
	hexValue << "0x" << std::right << std::setfill('0') << std::setbase(16) << std::setw(8) << value;
	return hexValue.str();
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

} // anonymous namespace
//------------------------------------------------------------------------------

int MediaFileIO::MoveImage(const string &fromFileName, const string &toFileName)
{
	// In case the file is sitting in the write cache
	FrameBufferWriteCache.FlushDirtyFrameToDiskIfMatch(fromFileName);
	FrameBufferWriteCache.FlushDirtyFrameToDiskIfMatch(toFileName);

	// Make sure the source file is accessible
	int retVal = checkFileAccessibility(fromFileName);
	if (retVal != 0)
	{
		return retVal;
	}

	// Two cases:
	// (1) target file exists and must be moved out of the way
	// (2) target file doesn't exist

	bool targetExisted = false;
	string backupName;

	if (0 == checkFileAccessibility(toFileName))
	{
		// Back up the target file
		backupName = makeBackupFileName(toFileName);
		retVal     = myRenameFile(toFileName, backupName);
		if (retVal != 0)
		{
			return retVal;
		}

		targetExisted = true;
	}

   // Move the file
   retVal = myRenameFile(fromFileName, toFileName);
	if (retVal != 0)
	{
		// Rename failed - restore from backup!
		if (targetExisted)
		{
			myRenameFile(backupName, toFileName);
		}

		return retVal;
	}

	// Hopefully we don't need this anymore!
	myDeleteFile(backupName);

	// Fix the caching by simply removing anything associated with either file
	// (theoretically I could just change the name in the cache entry instead)
	MediaFrameBufferReadCache readCache;
	readCache.reKeyBufferInCache(fromFileName, toFileName);
	MediaFrameFileReader::RemoveFromHeaderCache(fromFileName);
	MediaFrameFileReader::RemoveFromHeaderCache(toFileName);

	return 0;
}
//------------------------------------------------------------------------------

int MediaFileIO::HackImage(const string &imageFileName, int fileHackCommand)
{
	if (fileHackCommand == FILE_HACK_CACHE_FRAME)
	{
		TRACE_3(errout << ">>>>> FBWC: FOCUS ON " << imageFileName);
		FrameBufferWriteCache.SetDelayedWriteFilename(imageFileName);
		return 0;
	}

//	if (lInitializedFileType & FILE_TYPE_DPX)
//	{
//		iRet = HackImage_DPX(strFileName, fileHackCommand);
//	}
//	else if (lInitializedFileType & FILE_TYPE_TIFF)
//	{
//		iRet = FILE_ERROR_UNSUPPORTED_COMMAND;
//	}
//	else if (lInitializedFileType & FILE_TYPE_EXR)
//	{
//		iRet = FILE_ERROR_UNSUPPORTED_COMMAND;
//	}
//	else
//	{
//		iRet = FILE_ERROR_UNKNOWN_FILE_TYPE;
//	}

	return FILE_ERROR_INTERNAL;
}
//------------------------------------------------------------------------------

int MediaFileIO::DestroyImage(const string &imageFileName)
{
	FrameBufferWriteCache.FlushDirtyFrameToDiskIfMatch(imageFileName);
	MediaFrameBufferReadCache readCache;
	readCache.removeBufferFromCache(imageFileName);
	MediaFrameFileReader::RemoveFromHeaderCache(imageFileName);

	return myDeleteFile(imageFileName);
}
//------------------------------------------------------------------------------

MediaFrameFileReader *MediaFileIO::CreateMediaFrameFileReader(const string &filename)
{
	return MediaFrameFileReader::CreateMediaFrameFileReader(filename);
}
//------------------------------------------------------------------------------

MediaFrameFileWriter *MediaFileIO::CreateMediaFrameFileWriter(const string &filename)
{
	return MediaFrameFileWriter::CreateMediaFrameFileWriter(filename);
}
//------------------------------------------------------------------------------

void MediaFileIO::ClearAllCaches()
{
	FrameBufferWriteCache.FlushCacheSynchronously();
	MediaFrameBufferReadCache readCache;
	readCache.clearCache();
	MediaFrameFileReader::ClearHeaderCache();
}
//------------------------------------------------------------------------------

int MediaFileIO::SetFrameReadCacheSize(size_t sizeInPages, /* out */ size_t *actualNumberOfPagesAllocated)
{
	MediaFrameBufferReadCache readCache;
	return readCache.setSizeInPages(sizeInPages, /* out */ actualNumberOfPagesAllocated);
}
//------------------------------------------------------------------------------

void MediaFileIO::ClearFrameReadCache()
{
	MediaFrameBufferReadCache readCache;
	readCache.clearCache();
}
//------------------------------------------------------------------------------

void MediaFileIO::LockReadCacheFramesInMemory(bool flag)
{
	MediaFrameBufferReadCache readCache;
	readCache.lockCacheFramesInMemory(flag);
}
//------------------------------------------------------------------------------

void MediaFileIO::SetFrameReadCacheHints(int currentFrameIndex,int markInIndex, int markOutIndex)
{
	//// ignored for now:
	//// FrameBufferReadCache.SetFrameReadCacheHints(currentFrameIndex,markInIndex, markOutIndex);
}
//------------------------------------------------------------------------------

size_t MediaFileIO::GetFrameReadCacheSizeInBytes()
{
	MediaFrameBufferReadCache readCache;
	return readCache.getMediaFrameBufferCacheSizeInBytes();
}
//------------------------------------------------------------------------------

void MediaFileIO::AddFrameToReadCache(const string &filename, int frameIndex, MediaFrameBufferSharedPtr buffer)
{
	MediaFrameBufferReadCache readCache;
	return readCache.addBufferToCache(filename, frameIndex, buffer);
}
//------------------------------------------------------------------------------

MediaFrameBufferSharedPtr MediaFileIO::RetrieveFrameFromReadCache(const string &filename)
{
	MediaFrameBufferReadCache readCache;
	return readCache.retrieveBufferFromCache(filename);
}
//------------------------------------------------------------------------------

void MediaFileIO::RemoveFrameFromReadCache(const string &filename)
{
	MediaFrameBufferReadCache readCache;
	return readCache.removeBufferFromCache(filename);
}
//-----------------------------------------------------------------------------

void MediaFileIO::Shutdown()
{
	FrameBufferWriteCache.ShutDown();
//	MediaFrameBufferReadCache readCache;
//	readCache.setSizeInPages(-1);
//	MediaFrameFileReader::ClearHeaderCache();
}
//------------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
//
// New non-static stuff
//
///////////////////////////////////////////////////////////////////////////////

MediaFileType MediaFileIO::getFileType(const string &imageFilePath)
{
	auto reader = std::unique_ptr<MediaFrameFileReader>(CreateMediaFrameFileReader(imageFilePath));
	if (!reader)
	{
		return MediaFileType::Invalid;
	}

	return reader->getFileType();
}
//------------------------------------------------------------------------------

//int MediaFileIO::cloneFrameBufferWithoutImageData(
//	MediaFrameBufferSharedPtr frameBufferIn,
//	MediaFrameBufferSharedPtr &frameBufferOut)
//{
//	// Can only work on decoded buffers!
//	MTIassert(frameBufferIn->isDecoded());
//	if (!frameBufferIn->isDecoded())
//	{
//		return FORMAT_ERROR_UNSUPPORTED_OPTION;
//	}
//
//	// QQQ Check total buffer size vs actual file size
//	auto rawMediaFrameBufferIn = frameBufferIn->getRawMediaFrameBuffer();
//	size_t totalBufferSize = rawMediaFrameBufferIn->getBufferSize();
//	auto rawMediaFrameBufferOut = std::make_shared<RawMediaFrameBuffer>(totalBufferSize);
//	size_t fileSize = rawMediaFrameBufferIn->getDataSize();
//	rawMediaFrameBufferOut->setDataSize(fileSize);
//
//	size_t preDataSize = frameBufferIn->getImageDataOffset();
//	size_t postDataOffset = preDataSize + frameBufferIn->getImageDataSize();
//	int postDataSizeAsInt = fileSize - postDataOffset;
//	MTIassert(postDataSizeAsInt >= 0);
//	size_t postDataSize = (postDataSizeAsInt > 0) ? (size_t) postDataSizeAsInt : 0;
//
//	if (preDataSize > 0)
//	{
//		MTImemcpy(rawMediaFrameBufferOut->getBufferPtr(),
//					 rawMediaFrameBufferIn->getBufferPtr(),
//					 preDataSize);
//	}
//
//	if (postDataSize > 0)
//	{
//		MTImemcpy((unsigned char *)rawMediaFrameBufferOut->getBufferPtr() + postDataOffset,
//					 (unsigned char *)rawMediaFrameBufferIn->getBufferPtr() + postDataOffset,
//					 postDataSize);
//	}
//
//	CImageFormat imageFormat = frameBufferIn->getImageFormat();
//	frameBufferOut = MediaFrameBuffer::CreateSharedBuffer(rawMediaFrameBufferOut, imageFormat, true);
//	frameBufferOut->setHeaderData(frameBufferIn->getHeaderDataPtr(), frameBufferIn->getHeaderDataSize());
//	frameBufferOut->setTrailerData(frameBufferIn->getTrailerDataPtr(), frameBufferIn->getTrailerDataSize());
//	frameBufferOut->setOpaqueMetadata(frameBufferIn->getOpaqueMetadata());
//	frameBufferOut->setImageDataOffset(preDataSize);
//	frameBufferOut->setHasSeparateAlpha(frameBufferIn->hasSeparateAlpha());
//	if (frameBufferIn->hasSeparateAlpha())
//	{
//		frameBufferOut->setAlphaDataOffset(
//									((char *) frameBufferIn->getAlphaDataPtr())
//										- ((char *) frameBufferIn->getRawMediaFrameBuffer()->getBufferPtr()));
//	}
//	frameBufferOut->setHasSeparateAlpha(frameBufferIn->hasSeparateAlpha());
//
//	bool _isDecoded = false;
//	CImageFormat _imageFormat;
//	size_t _totalDataSize = 0;
//	size_t _imageDataOffset = 0;
//	size_t _alphaDataOffset = 0;
//	bool _hasSeparateAlpha = false;
//
//	void *_headerData = nullptr;
//	size_t _headerDataSize = 0;
//	void *_trailerData = nullptr;
//	size_t _trailerDataSize = 0;
//
//	OpaqueMetadataSharedPtr _opaqueMetadata;
//
//	CTimecode _timecode = CTimecode::NOT_SET;
//
//	SwapStatus _dataSwapStatus = NotSwapped;
//
//	ErrorInfo _errorInfo;
//
//	return 0;
//}
//------------------------------------------------------------------------------

int MediaFileIO::prefetchFrameFile(const string &imageFilePath)
{
	CImageFormat imageFormat;
	return prefetchFrameFile(imageFilePath, imageFormat);
}
//------------------------------------------------------------------------------

int MediaFileIO::prefetchFrameFile(const string &imageFilePath, const CImageFormat &imageFormat)
{
	auto reader = std::unique_ptr<MediaFrameFileReader>(CreateMediaFrameFileReader(imageFilePath));
	if (!reader)
	{
		return FILE_ERROR_UNKNOWN_FILE_TYPE;
	}

	int retVal = reader->prefetchFrameFile(imageFormat);
	if (retVal != 0)
	{
		return retVal;
	}

	return 0;
}
//------------------------------------------------------------------------------

int MediaFileIO::readFrameBuffer(const string &imageFilePath, MediaFrameBufferSharedPtr &frameBuffer)
{
	CImageFormat imageFormat;
	return readFrameBuffer(imageFilePath, frameBuffer, imageFormat);
}
//------------------------------------------------------------------------------

int MediaFileIO::readFrameBuffer(const string &imageFilePath, MediaFrameBufferSharedPtr &frameBuffer, const CImageFormat &imageFormat)
{
	auto reader = std::unique_ptr<MediaFrameFileReader>(CreateMediaFrameFileReader(imageFilePath));
	if (!reader)
	{
		return FILE_ERROR_UNKNOWN_FILE_TYPE;
	}

	int retVal = reader->getFrameBuffer(frameBuffer, imageFormat);
	if (retVal != 0)
	{
		return retVal;
	}

	return 0;
}
//------------------------------------------------------------------------------

int MediaFileIO::writeFrameBuffer(const string &imageFilePath, MediaFrameBufferSharedPtr frameBuffer)
{
	// First replace old version in the READ cache.
	AddFrameToReadCache(imageFilePath, -1, frameBuffer);

	// Stick it in the WRITE cache, where it will be written asynchronously.
	FrameBufferWriteCache.AddFrameBufferToCache(imageFilePath, frameBuffer);

	return 0;
}
//------------------------------------------------------------------------------

int MediaFileIO::getImageFormat(const string &imageFilePath, CImageFormat &imageFormat)
{
	auto reader = std::unique_ptr<MediaFrameFileReader>(CreateMediaFrameFileReader(imageFilePath));
	if (!reader)
	{
		return FILE_ERROR_UNKNOWN_FILE_TYPE;
	}

	int retVal = reader->getImageFormat(imageFormat);
	if (retVal != 0)
	{
		return retVal;
	}

	return 0;
}
//------------------------------------------------------------------------------

int MediaFileIO::dumpFileHeaderToString(const string &imageFilePath, string &headerDumpString)
{
	auto reader = std::unique_ptr<MediaFrameFileReader>(CreateMediaFrameFileReader(imageFilePath));
	if (!reader)
	{
		headerDumpString = "";
		return FILE_ERROR_UNKNOWN_FILE_TYPE;
	}

	return reader->dumpFileHeader(headerDumpString);
}
//------------------------------------------------------------------------------



