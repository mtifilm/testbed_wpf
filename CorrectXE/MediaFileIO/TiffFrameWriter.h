//---------------------------------------------------------------------------

#ifndef TiffFrameWriterH
#define TiffFrameWriterH
//---------------------------------------------------------------------------

#include "MediaFrameFileWriter.h"
//---------------------------------------------------------------------------

class TiffFrameWriter : public MediaFrameFileWriterBase
{
public:
	TiffFrameWriter(const string &filename);
	~TiffFrameWriter() = default;

	int encodeImage(
			MediaFrameBufferSharedPtr frameBufferIn,
			MediaFrameBufferSharedPtr &frameBufferOut);
};
//---------------------------------------------------------------------------

#endif
