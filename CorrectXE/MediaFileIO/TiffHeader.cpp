//---------------------------------------------------------------------------

#pragma hdrstop

#include "TiffHeader.h"

#include "err_file.h"
#include "TiffLibWrapper.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
//---------------------------------------------------------------------------

int TiffHeader::extractFromFrameBuffer(MediaFrameBufferSharedPtr frameBuffer)
{
	// Build a TIFF memory file handle from tha addrees of the image buffer.
   auto rawMediaFrameBufferSharedPtr = frameBuffer->getRawMediaFrameBuffer();
   if (!rawMediaFrameBufferSharedPtr)
   {
      return FILE_ERROR_UNEXPECTED_NULL_POINTER;
   }

	TiffLibHandle handle(rawMediaFrameBufferSharedPtr);
	int retVal = TiffLibWrapper::ReadHeader((thandle_t)&handle, this);
	if (retVal)
	{
		// Corrupt file!!
		return retVal;
	}

	return 0;
}
//---------------------------------------------------------------------------

int TiffHeader::convertToImageFormat(CImageFormat &imageFormat)
{
	bool isByteSwapped = false;
	if (sMagic != TIFF_MAGIC_NUMBER_LE)
	{
		if (sMagic == TIFF_MAGIC_NUMBER_BE)
		{
			isByteSwapped = true;
		}
		else
		{
			return FILE_ERROR_BAD_MAGIC_NUMBER;
		}
	}

	EPixelPacking pixelPacking;

	switch (bitsPerSample)
	{
	case 8:
		pixelPacking = IF_PIXEL_PACKING_8Bits_IN_1Byte;
		break;

	case 12:
		return FILE_ERROR_UNSUPPORTED_OPTION;

	case 16:
		pixelPacking = isByteSwapped
							 ? IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE
							 : IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE;
		break;

	default:
		return FILE_ERROR_BAD_BIT_DEPTH;
	}

	if (photometric != PHOTOMETRIC_RGB && photometric != PHOTOMETRIC_MINISBLACK)
	{
		// Only RGB (and RGBA) and monochrome are supported.
		return FILE_ERROR_UNSUPPORTED_OPTION;
	}

	EPixelComponents pixelComponents;
	EAlphaMatteType alphaMatteType = IF_ALPHA_MATTE_NONE;

	switch (samplesPerPixel)
	{
	case 1:
		pixelComponents = IF_PIXEL_COMPONENTS_Y;
		break;

	case 3:
		pixelComponents = IF_PIXEL_COMPONENTS_RGB;
		break;

	case 4:
		pixelComponents = IF_PIXEL_COMPONENTS_RGBA;
		alphaMatteType = IF_ALPHA_MATTE_16_BIT_INTERLEAVED;
		break;

	default:
		return FILE_ERROR_UNSUPPORTED_OPTION;
	}

	// Don't bother to error out, just always show the picture even if it's
	// flipped or rotated!
	//if (orientation != ORIENTATION_TOPLEFT)
	//{
	//	// Only the normal orientation is supported.
	//	return FILE_ERROR_UNSUPPORTED_OPTION;
	//}

	imageFormat.setToDefaults(IF_TYPE_FILE_TIFF, pixelPacking, pixelComponents, height, width);
	imageFormat.setAlphaMatteType(alphaMatteType);

	return 0;
}
//---------------------------------------------------------------------------


