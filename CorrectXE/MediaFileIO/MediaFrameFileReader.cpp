//---------------------------------------------------------------------------

#include "MediaFrameFileReader.h"
#include "DpxFrameReader.h"
#include "ExrFrameReader.h"
#include "TiffFrameReader.h"

#include "err_file.h"
#include "FileSweeper.h"
//#include "HRTimer.h"
#include "MediaFileIO.h"
#include "MTImalloc.h"      // for MTImemset

#include <algorithm>

//---------------------------------------------------------------------------

//#define USE_OVERLAPPED_READ

//---------------------------------------------------------------------------

/* static */
MediaFrameFileReader *MediaFrameFileReader::CreateMediaFrameFileReader(const string &filename)
{
	// Brute force
	string fileExt = ::GetFileExt(filename);
	std::transform(fileExt.begin(), fileExt.end(), fileExt.begin(), ::tolower);

	if (fileExt == ".dpx")
	{

		return new DpxFrameReader(filename);
	}

	if (fileExt == ".tiff" || fileExt == ".tif")
	{
		return new TiffFrameReader(filename);
	}

	if (fileExt == ".exr")
	{
		return new ExrFrameReader(filename);
	}

	return nullptr;
}
//---------------------------------------------------------------------------

/* static */
void MediaFrameFileReader::RemoveFromHeaderCache(const string &filename)
{
   ExrFrameReader::RemoveFromHeaderCache(filename);
}
//---------------------------------------------------------------------------

/* static */
void MediaFrameFileReader::ClearHeaderCache()
{
	ExrFrameReader::ClearHeaderCache();
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//----------------------------BASE-------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

int MediaFrameFileReaderBase::prefetchFrameFile(const CImageFormat &imageFormat)
{
	// Read the file into the cache, then release the buffer (we want to be
	// as stateless as possible). Depends on the minimum size of the cache
	// being something reasonable to acommodate multiple prefetched buffers.
	auto rawFrameBuffer = readFrameFile(imageFormat);
	if (!rawFrameBuffer)
	{
		return FILE_ERROR_READ;
	}

	if (rawFrameBuffer->getErrorInfo().code != 0)
	{
		return rawFrameBuffer->getErrorInfo().code;
	}

	return 0;
}
//---------------------------------------------------------------------------

int MediaFrameFileReaderBase::getFrameBuffer(MediaFrameBufferSharedPtr &outFrameBuffer, const CImageFormat &imageFormat)
{
	auto frameBuffer = readFrameFile(imageFormat);
	if (!frameBuffer)
	{
		return FILE_ERROR_READ;
	}

	if (frameBuffer->getErrorInfo().code != 0)
	{
		frameBuffer->setIsDecoded(true);
		frameBuffer->setImageFormat(imageFormat);
		outFrameBuffer = frameBuffer;
		return frameBuffer->getErrorInfo().code;
	}

	if (frameBuffer->isDecoded())
	{
		outFrameBuffer = frameBuffer;
		return 0;
	}

	int retVal = decodeImage(frameBuffer, outFrameBuffer);
	if (retVal)
	{
		return retVal;
	}

	MediaFileIO::AddFrameToReadCache(_filename, -1, outFrameBuffer);

	return 0;
}
//---------------------------------------------------------------------------

int MediaFrameFileReaderBase::getImageFormat(CImageFormat &imageFormat)
{
	// Just get the frame buffer - it contains the image format.
	MediaFrameBufferSharedPtr frameBuffer;
	CImageFormat invalidImageFormat;
	int retVal = getFrameBuffer(frameBuffer, invalidImageFormat);
	if (retVal)
	{
		return retVal;
	}

	imageFormat = frameBuffer->getImageFormat();

	return 0;
}
//---------------------------------------------------------------------------

int MediaFrameFileReaderBase::dumpFileHeader(string &headerDumpString)
{
	headerDumpString = "";
	return FILE_ERROR_UNSUPPORTED_COMMAND;
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

namespace {
//----------------------------------

	MediaFrameBufferSharedPtr makeErrorFrameBuffer(const MediaFrameBuffer::ErrorInfo errorInfo)
	{
		RawMediaFrameBufferSharedPtr rawMediaFrameBuffer = nullptr;
      CImageFormat uninitializedImageFormat;
		auto mediaFrameBufferSharedPtr = MediaFrameBuffer::CreateSharedBuffer(
															rawMediaFrameBuffer,
															uninitializedImageFormat,
															true); // ??? QQQ  isDecoded
		mediaFrameBufferSharedPtr->setErrorInfo(errorInfo);
		return mediaFrameBufferSharedPtr;
	}
//----------------------------------

	// Get the Aligned Size for a given file - takes into account the sector
	// size of the disk the file exists on in order to determine the
	// Aligned Size needed for a given file.  This is important in the
	// case of CreateFile() / ReadFile() in Windows where we wish to use
	// the FILE_FLAG_NO_BUFFERING flag to speed up reading of image files
	// by avoiding O/S caching.
	//
	// Return Values:
	// success = the aligned size
	// failure = the passed in size
	MTI_INT64 getSectorAlignedSize(
		const string &filename,
		MTI_INT64 llFileSize,
		MTI_UINT32 *sectorSize) // output
	{
		bool success                   = false;
		unsigned long ulBytesPerSector = 0;
		MTI_INT64 llOrigNumBytes       = 0;
		MTI_INT64 llAlignedSize        = 0;

		success = GetBytesPerSector(const_cast<char *>(filename.c_str()), &ulBytesPerSector);
		if (!success || ulBytesPerSector == 0)
		{
			ulBytesPerSector = 512;
		}

		if (sectorSize != NULL)
		{
			*sectorSize = ulBytesPerSector;
		}

		return ulBytesPerSector * ((llFileSize + ulBytesPerSector - 1) / ulBytesPerSector);
	}
//----------------------------------
}
//---------------------------------------------------------------------------

size_t MediaFrameFileReaderBase::computeMinBufferSize(const CImageFormat &imageFormat)
{
	// HACK ** HACK ** HACK ** HACK ** HACK ** HACK ** HACK ** HACK **
	// UGGH because of the Y-> YYY hack, if the image is Y format, we
	// override the buffer size to match the YYY buffer size because the
	// buffer free list management works more smoothly if all the buffers
	// being requested are the same size.
	return (imageFormat.getPixelComponents() == IF_PIXEL_COMPONENTS_Y)
																? imageFormat.getBytesPerField()
																: 0;
}
//---------------------------------------------------------------------------

MediaFrameBufferSharedPtr MediaFrameFileReaderBase::readFrameFile(const CImageFormat &imageFormat)
{
	auto frameBuffer = retrieveFrameFileFromCache();
	if (!frameBuffer)
	{
		size_t minSize = computeMinBufferSize(imageFormat);
		frameBuffer = readFrameFileFromDisk(minSize);
	}

	if (!frameBuffer->getRawMediaFrameBuffer())
	{
      // ???? QQQ
		if (imageFormat.validate() != 0)
		{
			return nullptr;
		}

      frameBuffer = MediaFrameBuffer::CreateErrorBuffer(imageFormat, frameBuffer->getErrorInfo());
	}

	return frameBuffer;
}
//---------------------------------------------------------------------------

MediaFrameBufferSharedPtr MediaFrameFileReaderBase::retrieveFrameFileFromCache()
{
  return MediaFileIO::RetrieveFrameFromReadCache(_filename);
}
//---------------------------------------------------------------------------

namespace {
}
//---------------------------------------------------------------------------

MediaFrameBufferSharedPtr MediaFrameFileReaderBase::readFrameFileFromDisk(size_t minBufferSize)
{
	CHRTimer timer;
	CImageFormat uninitializedImageFormat;
	TRACE_3(errout << "Reading " << _filename << ") from disk");

	HANDLE handle = ::CreateFile(_filename.c_str(),
											GENERIC_READ,
											FILE_SHARE_READ, // | FILE_SHARE_WRITE,
											NULL,
											OPEN_EXISTING,
											FILE_FLAG_NO_BUFFERING,
											NULL);
	if (handle == INVALID_HANDLE_VALUE)
	{
		int errorCode = ::GetLastError();
		string errorMsg = ::GetSystemErrorMessage(errorCode);
		TRACE_0(errout << "ERROR: Open(" << _filename << ") FAILED! (" << errorCode << ")");
		TRACE_0(errout << "Reason: " << errorMsg);

		string shortString = (errorCode == 2)
									? "MEDIA FILE IS MISSING"
									: ((errorCode == 3)
										? "MEDIA FOLDER IS MISSING"
										: (string("MEDIA FILE OPEN FAILED : ") + errorMsg));
		string fileOrFolderName = (errorCode == 3)
											? GetFilePath(_filename)
											: _filename;
		MediaFrameBuffer::ErrorInfo errorInfo { errorCode + 100000, fileOrFolderName, shortString, "" };
		return makeErrorFrameBuffer(errorInfo);
	}

	double createTime = timer.Read();

	LARGE_INTEGER fileSizeAsLargeInteger;
	if (! ::GetFileSizeEx(handle, &fileSizeAsLargeInteger))
	{
		int errorCode = ::GetLastError();
		string errorMsg = ""; //::GetSystemErrorMessage(errorCode);

		TRACE_0(errout << "ERROR: GetFileSize(" << _filename << ") FAILED! (" << errorCode << ")");
		TRACE_0(errout << "Reason: " << errorMsg);

		::CloseHandle(handle);

		MediaFrameBuffer::ErrorInfo errorInfo { errorCode + 100000, _filename, "GET MEDIA FILE SIZE FAILED", errorMsg };
		return makeErrorFrameBuffer(errorInfo);
	}

	double fileSizeTime = timer.Read() - createTime;

	MTIassert(fileSizeAsLargeInteger.QuadPart <= 0x7FFFFFFF);
	size_t fileSize = (size_t)fileSizeAsLargeInteger.QuadPart;
	size_t alignedFileSize = getSectorAlignedSize(_filename, fileSize, nullptr);
	size_t bufferSize = std::max<size_t>(alignedFileSize, minBufferSize);
	if (bufferSize == 0)
	{
		TRACE_0(errout << "ERROR: " << _filename << " is empty!");
		::CloseHandle(handle);

		MediaFrameBuffer::ErrorInfo errorInfo { FILE_ERROR_BAD_IMAGE_SIZE, _filename, "MEDIA FILE IS EMPTY", "" };
		return makeErrorFrameBuffer(errorInfo);
	}

	auto rawMediaFrameBufferSharedPtr = std::make_shared<RawMediaFrameBuffer>(bufferSize);
	int numberOfBytesRead;

	bool success = ::ReadFile(handle,
									 (LPVOID)(rawMediaFrameBufferSharedPtr->getBufferPtr()),
									 (DWORD) alignedFileSize,
									 (LPDWORD) &numberOfBytesRead,
									 0);
	if (!success)
	{
		int errorCode = ::GetLastError();
		string errorMsg = ::GetSystemErrorMessage(errorCode);

		MTIostringstream os1;
		MTIostringstream os2;
		os1 << "ptr=0x" << std::right << std::setfill('0') << std::setbase(16) << std::setw(16) << (MTI_UINT64)rawMediaFrameBufferSharedPtr->getBufferPtr();
		os2 << "size=0x" << std::right << std::setfill('0') << std::setbase(16) << std::setw(8) << (MTI_UINT32)rawMediaFrameBufferSharedPtr->getBufferSize();
		TRACE_0(errout << "ERROR: ReadFile(" << _filename << ", " << os1.str() << ", " << os2.str() << ") FAILED! (" << errorCode << ")");
		TRACE_0(errout << "Reason: " << errorMsg);

		::CloseHandle(handle);

		MediaFrameBuffer::ErrorInfo errorInfo { errorCode + 100000, _filename, string("MEDIA FILE READ FAILED : ") + errorMsg, "" };
		return makeErrorFrameBuffer(errorInfo);
	}

	MTIassert(numberOfBytesRead >= fileSize);
	rawMediaFrameBufferSharedPtr->setDataSize(numberOfBytesRead);
	double readTime = timer.Read() - createTime - fileSizeTime;

	::CloseHandle(handle);

	double closeTime = timer.Read() - createTime - fileSizeTime - readTime;

	auto mediaFrameBufferSharedPtr = MediaFrameBuffer::CreateSharedBuffer(rawMediaFrameBufferSharedPtr, uninitializedImageFormat, false);

	MediaFileIO::AddFrameToReadCache(_filename, -1, mediaFrameBufferSharedPtr);

	TRACE_3(errout << "TOTAL READ TIME of " << _filename << ": " << timer.Read()
						<< " msec (CF=" << createTime << ", GFS=" << fileSizeTime
						<< ", RF=" << readTime << ", CH=" << closeTime);

	return mediaFrameBufferSharedPtr;
}
//---------------------------------------------------------------------------

//void MediaFrameFileReaderBase::ExpandYToYYY(
//	const CImageFormat &imageFormat,
//	MTI_UINT8 *srcBuffer, int srcDataOffset,
//	MTI_UINT8 *dstBuffer, int dstDataOffset)
//{
//	TRACE_3(errout << "X*X*X*X*X*X*X*X*X*X ExpandYToYYY BUFFER%4k=" << ((*(int*)&srcBuffer)%4096) << ", OFFSET=" << srcDataOffset);
//	if (imageFormat.getPixelComponents() != IF_PIXEL_COMPONENTS_Y)
//	{
//      return;
//   }
//
//	if (imageFormat.getComponentCount() != 3
//	|| (imageFormat.getBitsPerComponent() != 16 && imageFormat.getBitsPerComponent() != 10))
//	{
//		// QQQ ERROR!
//		TRACE_0(errout << "INTERNAL ERROR: improper use of pixel components == Y");
//      MTIassert(false);
//      return;
//   }
//
//   int numberOfPixels = imageFormat.getTotalFrameHeight() * imageFormat.getTotalFrameWidth();
//
//   switch (imageFormat.getPixelPacking())
//   {
//   case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
//	case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:
//		{
//         MTI_UINT16 *srcData = (MTI_UINT16*)(srcBuffer + srcDataOffset);
//         MTI_UINT16 *dstData = (MTI_UINT16*)(dstBuffer + dstDataOffset);
//
//         // CAREFUL src may == dst, so might be expanding in place.
//			for (int i = (numberOfPixels - 1); i >= 0; --i)
//         {
//            int iTimes3   = i * 3;
//            dstData[iTimes3] = dstData[iTimes3 + 1] = dstData[iTimes3 + 2] = srcData[i];
//         }
//
//         break;
//      }
//
//      // Nasty, nasty, nasty, nasty hack! These represent monochrome data
//      // with source packing 3_10Bits_IN_4Bytes_L and 3_10Bits_IN_4Bytes_R,
//		// respectively, which here we unpack to 3 16-bit RGB components from
//      // each 10-bit Y component. Bleah!
//   case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L:
//   case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R:
//      {
//         // CAREFUL: in-place expansion !!
//         MTI_UINT32 *srcPtr  = (MTI_UINT32*)(srcBuffer + srcDataOffset);
//         MTI_UINT16 *destPtr = (MTI_UINT16*)(dstBuffer + dstDataOffset);
//         bool L              = imageFormat.getPixelPacking() == IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L;
//
//			int numberOfPixelsPercent3   = numberOfPixels % 3;
//         int numberOfPixelsDividedBy3 = numberOfPixels / 3;
//         int numberOfPixelsTimes3     = numberOfPixels * 3;
//         if (numberOfPixelsPercent3 > 0)
//         {
//            // there are three one-component pixels packed in 32 bits
//            MTI_UINT32 srcDatum = srcPtr[numberOfPixelsDividedBy3 + 1] >> (L ? 2 : 0);
//
//            if (numberOfPixelsPercent3 == 2)
//            {
//               destPtr[numberOfPixelsTimes3 - 1] = destPtr[numberOfPixelsTimes3 - 2] = destPtr[numberOfPixelsTimes3 - 3] =
//                     (srcDatum >> 10) & 0x000003FF;
//
//               destPtr[numberOfPixelsTimes3 - 4] = destPtr[numberOfPixelsTimes3 - 5] = destPtr[numberOfPixelsTimes3 - 6] =
//                     (srcDatum >> 20) & 0x000003FF;
//            }
//            else
//            {
//               destPtr[numberOfPixelsTimes3 - 1] = destPtr[numberOfPixelsTimes3 - 2] = destPtr[numberOfPixelsTimes3 - 3] =
//                     (srcDatum >> 20) & 0x000003FF;
//            }
//         }
//
//			// CAREFUL src may == dst, so might be expanding in place.
//			int i = (numberOfPixels / 3) - 1;
//			int iTimes9 = i * 9;
//			if (L)
//			{
//				for (; i >= 0; --i, iTimes9 -= 9)
//				{
//					MTI_UINT32 yyy = srcPtr[i];
//					destPtr[iTimes9 + 8] = destPtr[iTimes9 + 7] = destPtr[iTimes9 + 6] = (yyy         >> 22);
//					destPtr[iTimes9 + 5] = destPtr[iTimes9 + 4] = destPtr[iTimes9 + 3] = ((yyy << 10) >> 22);
//					destPtr[iTimes9 + 2] = destPtr[iTimes9 + 1] = destPtr[iTimes9 + 0] = ((yyy << 20) >> 22);
//				}
//			}
//			else
//			{
//				for (; i >= 0; --i, iTimes9 -= 9)
//				{
//					MTI_UINT32 yyy = srcPtr[i];
//					destPtr[iTimes9 + 8] = destPtr[iTimes9 + 7] = destPtr[iTimes9 + 6] = ((yyy <<  2) >> 22);
//					destPtr[iTimes9 + 5] = destPtr[iTimes9 + 4] = destPtr[iTimes9 + 3] = ((yyy << 12) >> 22);
//					destPtr[iTimes9 + 2] = destPtr[iTimes9 + 1] = destPtr[iTimes9 + 0] = ((yyy << 22) >> 22);
//				}
//			}
//
//			break;
//      }
//
//   default:
//      TRACE_0(errout << "INTERNAL ERROR... Unsupported Y format pixel packing!");
//      MTIassert(false);
//      return;
//   }
//}
//---------------------------------------------------------------------------
