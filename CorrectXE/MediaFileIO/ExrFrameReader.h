//---------------------------------------------------------------------------

#ifndef ExrFrameReaderH
#define ExrFrameReaderH
//---------------------------------------------------------------------------

#include "ExrHeaderCache.h"
#include "MediaFrameFileReader.h"
//---------------------------------------------------------------------------

class ExrFrameReader : public MediaFrameFileReaderBase
{
public:
	ExrFrameReader(const string &filename);
	~ExrFrameReader();

	virtual MediaFileType getFileType() { return MediaFileType::Exr; };
	int dumpFileHeader(string &headerDumpString);

	static void RemoveFromHeaderCache(const string &filename);
	static void ClearHeaderCache();

protected:
//	virtual int extractImageFormat(RawMediaFrameBufferSharedPtr buffer, CImageFormat &imageFormat);
	int decodeImage(MediaFrameBufferSharedPtr frameBufferIn, MediaFrameBufferSharedPtr &frameBufferOut);
	size_t computeMinBufferSize(const CImageFormat &imageFormat);

private:
	static ExrHeaderCache _HeaderCache;

	int extractHeader(MediaFrameBufferSharedPtr rawFrameBuffer, ExrHeader &header);
};

#endif

