//---------------------------------------------------------------------------

#pragma hdrstop

#include "TiffFrameReader.h"

#include "err_file.h"
#include "HRTimer.h"
#include "tiffio.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
//---------------------------------------------------------------------------

//TiffHeaderCache TiffFrameReader::_HeaderCache;
//---------------------------------------------------------------------------

///* static */ void TiffFrameReader::RemoveFromHeaderCache(const string &filename)
//{
//	_HeaderCache.remove(filename);
//}
//---------------------------------------------------------------------------

///* static */ void TiffFrameReader::ClearHeaderCache()
//{
//	_HeaderCache.clear();
//}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

int TiffFrameReader::decodeImage(MediaFrameBufferSharedPtr frameBufferIn, MediaFrameBufferSharedPtr &frameBufferOut)
{
//	DBCOMMENT("DECODE TIFF IMAGE");
//	DBTRACE(frameBufferIn->getBufferSize());

	MTIassert(frameBufferIn);
	if (!frameBufferIn)
	{
		return FILE_ERROR_READ;
	}

	MTIassert(!frameBufferIn->isDecoded());
	if (frameBufferIn->isDecoded())
	{
		frameBufferOut = frameBufferIn;
		return 0;
	}

	// Parse the header.
	TiffHeader header;
	int retVal = header.extractFromFrameBuffer(frameBufferIn);
	if (retVal != 0)
	{
		TRACE_0(errout << "ERROR: TIFF PARSE HEADER FAIL (code " << retVal << " for file " << _filename);
		return FILE_ERROR_TIFF_HEADER_READ_FAILED;
	}

//	DBTRACE(header.ToString());

	// Convert to image format.
	CImageFormat imageFormat;
	retVal = header.convertToImageFormat(imageFormat);
	if (retVal != 0)
	{
		TRACE_0(errout << "ERROR: Could not determine image format from TIFF file (code " << retVal << ") for file " << _filename);
		return FILE_ERROR_TIFF_HEADER_DECODE_FAILED;
	}

	EPixelPacking pixelPacking = imageFormat.getPixelPacking();

	// HACK: KurtKode doesn't handle IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE,
	// so we need to swap here and pretend the data was LE.
	if (pixelPacking == IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE)
	{
		pixelPacking = IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE;
	}

	// DECEIVING: The header being swapped means the data is big-endian, so swap if we want to
	// end up as little-endian. Always swap 16-bit data as 2-byte entities.
	bool needToSwap16 = header.isFileSwapped() && (pixelPacking == IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE);
//	bool needToConvertYtoYYY = imageFormat.getPixelComponents() == IF_PIXEL_COMPONENTS_Y;

	if (!needToSwap16) // && !needToConvertYtoYYY)
	{
		frameBufferOut = MediaFrameBuffer::CreateSharedBuffer(frameBufferIn->getRawMediaFrameBuffer(), imageFormat, true);
		if (frameBufferOut)
		{
			frameBufferOut->setImageDataOffset(header.dataOffset);
		}

		return 0;
	}

	// Make a shared "cooked" buffer to swap the raw data into.
	size_t fileSize = header.fileSize;
//	DBTRACE(fileSize);
	size_t imageDataLength = (size_t)imageFormat.getBytesPerField();
//	DBTRACE(imageDataLength);
//	size_t imageDataLengthOnDisk = needToConvertYtoYYY ? (imageDataLength / 3) : imageDataLength;
	size_t imageDataLengthOnDisk = imageDataLength;
//	DBTRACE(imageDataLengthOnDisk);
	// FOR PERFORMANCE REASONS, USE LARGER OF IN BUFFER SIZE vs DESIRED SIZE!
	size_t nBytes = (size_t) std::max<size_t>(imageDataLength,
										 frameBufferIn->getRawMediaFrameBuffer()->getBufferSize());
//	DBTRACE(nBytes);
	RawMediaFrameBufferSharedPtr cookedFrameBuffer = std::make_shared<RawMediaFrameBuffer>(nBytes);
	if (!cookedFrameBuffer)
	{
		return FILE_ERROR_MALLOC;
	}

	MTI_UINT8 *src = (MTI_UINT8 *) frameBufferIn->getBufferPtr();
	size_t srcOffset = header.dataOffset;
	MTI_UINT8 *dst = (MTI_UINT8 *) cookedFrameBuffer->getBufferPtr();
	size_t dstOffset = 0;
	if (needToSwap16)
	{
//		DBCOMMENT("Before SWAP");
		// Swap first, then maybe expand.
		SwapINT16((MTI_UINT16*) (src + srcOffset), (MTI_UINT16*) (dst + dstOffset), imageDataLengthOnDisk / sizeof(MTI_UINT16));
		src = dst;
		srcOffset = dstOffset;
		imageFormat.setPixelPacking(IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE);
	}

//	if (needToConvertYtoYYY)
//	{
//		DBCOMMENT("Before EXPAND");
//		ExpandYToYYY(imageFormat, src, srcOffset, dst, dstOffset);
//	}

	frameBufferOut = MediaFrameBuffer::CreateSharedBuffer(cookedFrameBuffer, imageFormat, true);

	void *srcHeaderData = frameBufferIn->getBufferPtr();
	size_t headerDataSize = header.dataOffset;
	void *srcTrailerData = (unsigned char *)srcHeaderData + headerDataSize + imageDataLengthOnDisk;
	size_t trailerDataSize = fileSize - (headerDataSize + imageDataLengthOnDisk);

	frameBufferOut->setHeaderData(srcHeaderData, headerDataSize);
	frameBufferOut->setTrailerData(srcTrailerData, trailerDataSize);
	auto setSizeError = !frameBufferOut->setTotalDataSize((size_t)imageFormat.getBytesPerField());

	if (setSizeError)
   {
      TRACE_0(errout << "Failed setTotalDataSize() was called from TiffFrameReader::decodeImage()");
   }

//	DBTRACE(frameBufferOut->getImageDataSize());
//	DBTRACE(frameBufferOut->getImageDataOffset());
//	DBTRACE(frameBufferOut->hasSeparateAlpha());
//	DBTRACE(frameBufferOut->getAlphaDataSize());
//	DBTRACE(frameBufferOut->getHeaderDataSize());
//	DBTRACE(frameBufferOut->getTrailerDataSize());

	return 0;
}
//---------------------------------------------------------------------------
//
//int TiffFrameReader::extractHeader(MediaFrameBufferSharedPtr frameBufferIn, TiffHeader &header)
//{
//	MTI_INT64 realFileSize = -1;
////	if (!_HeaderCache.retrieve(_filename, header))
////	{
//		MTIassert(frameBufferIn);
//		int retVal = header.extractFromFrameBuffer(
//							frameBufferIn->getRawFrameBuffer()->getBufferPtr(),
//							frameBufferIn->getRawFrameBuffer()->getBufferSize());
////		_HeaderCache.add(_filename, header);
////	}
//
//	return 0;
//}
//---------------------------------------------------------------------------
//
//int TiffFrameReader::extractImageFormat(MediaFrameBufferSharedPtr frameBufferIn, CImageFormat &imageFormat)
//{
//	TiffHeader header;
//	int retVal = extractHeader(frameBufferIn, header);
//	if (retVal)
//	{
//		return retVal;
//	}
//
//	retVal = header.convertToImageFormat(imageFormat);
//	if (retVal)
//	{
//		return retVal;
//	}
//
//	return 0;
//}
//---------------------------------------------------------------------------


