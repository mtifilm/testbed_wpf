//---------------------------------------------------------------------------

#pragma hdrstop

#include "TinyExrApi.h"
#include "MTImalloc.h"  // for MTIstrdup
//---------------------------------------------------------------------------
#pragma package(smart_init)

#define strnlen strnlen_s
#define _strdup strdup

#define TINYEXR_IMPLEMENTATION
#include "tinyexr.h"
