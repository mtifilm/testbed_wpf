//---------------------------------------------------------------------------

#pragma hdrstop

#include "ExrHeaderCache.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
// -------------------------------------------------------------------------

void ExrHeaderCache::add(const string &strFileName, const ExrHeader &header)
{
	CAutoSpinLocker lock(_lock);

	// Ensure no duplicates!
	remove(strFileName);

   // Drop oldest entry if cache is full
	if (_cache.size() >= EXR_HEADER_CACHE_MAX_SIZE)
   {
		_cache.pop_back();
   }

	// Finally add the new entry at the front
	CacheEntry cacheEntry(strFileName, header);
	_cache.push_front(cacheEntry);
}
// -------------------------------------------------------------------------

bool ExrHeaderCache::retrieve(const string &strFileName, ExrHeader &header)
{
	CAutoSpinLocker lock(_lock);

	bool retVal = false;

	// Find the entry for this file in the cache
	deque<CacheEntry>::iterator iter;
	for (iter = _cache.begin(); iter != _cache.end() && (*iter).fileName != strFileName; ++iter)
	{
		// Do nothing else;
	}

	if (iter != _cache.end())
	{
		// Found a cached header for the file - return it, and bump the
		// cache entry to the front of the cache
		CacheEntry cacheEntry(strFileName, (*iter).header);

		header = (*iter).header;
		_cache.erase(iter);
      _cache.push_front(cacheEntry);
      retVal = true;
	}

   return retVal;
}
// -------------------------------------------------------------------------

void ExrHeaderCache::remove(const string &strFileName)
{
	CAutoSpinLocker lock(_lock);

	// Find the entry for the file in the cache
	deque<CacheEntry>::iterator iter;
	for (iter = _cache.begin(); iter != _cache.end() && (*iter).fileName != strFileName; ++iter)
   {
      // Do nothing else;
   }

   if (iter != _cache.end())
   {
      // Found a cached header for the file - zap it
      _cache.erase(iter);
   }
}
// -------------------------------------------------------------------------

void ExrHeaderCache::clear()
{
	CAutoSpinLocker lock(_lock);

	_cache.clear();
}
// -------------------------------------------------------------------------
