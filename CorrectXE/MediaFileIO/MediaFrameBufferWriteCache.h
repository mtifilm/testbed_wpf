//---------------------------------------------------------------------------

#ifndef MediaFrameBufferWriteCacheH
#define MediaFrameBufferWriteCacheH
//---------------------------------------------------------------------------

#include "machine.h"
#include "Event.h"
#include "HRTimer.h"
#include "IniFile.h"	// for TRACE
#include "MediaFrameBuffer.h"

/////////////////////////////////////////////////////////////
// GLOBAL FRAME BUFFER WRITE CACHE ACCESSOR CLASS
// Basic usage:
//
//    MediaFrameBufferWriteCache cache;
//    cache.AddFrameBufferToCache(filename, frameBuffer);
//
// The cache takes care of actually writing the frame buffer to disk!
/////////////////////////////////////////////////////////////


class CImageFileIO;

class MediaFrameBufferWriteCache
{
public:

   MediaFrameBufferWriteCache();
   ~MediaFrameBufferWriteCache();

	void SetDelayedWriteFilename(const string &filename);
	void AddFrameBufferToCache(const string &filename, MediaFrameBufferSharedPtr frameBuffer);
	MediaFrameBufferSharedPtr RetrieveCachedFrameBuffer(const string &filename);
	void FlushDirtyFrameToDiskIfMatch(const string &filename);
	void FlushCacheSynchronously();
	void ShutDown();

private:

	CHRTimer _timer;
	CSpinLock _lock;

	string _delayedWriteFilename;
	MediaFrameBufferSharedPtr _delayedWriteFrameBuffer;
	double _delayedWriteFrameBufferWriteTime = 0.0;
//	string _filenameOfDelayedWriteInProgress;
//	MediaFrameBufferSharedPtr _frameBufferOfDelayedWriteInProgress;

	typedef std::pair< string, MediaFrameBufferSharedPtr > cacheEntryType;
	typedef std::deque< cacheEntryType > cacheListType;
	typedef cacheListType::iterator cacheListIteratorType;
	cacheListType _regularWriteCache;
	string _filenameOfRegularWriteInProgress;
	MediaFrameBufferSharedPtr _frameBufferOfRegularWriteInProgress;

	bool _cacheFlushInProgress = false;
	bool _terminateDelayedWriterThread = false;
	bool _terminateRegularWriterThread = false;

	void *_delayedWriterThreadID = nullptr;
	void *_regularWriterThreadID = nullptr;

	Event _delayedWriteEvent;
	Event _writeCacheNotEmpty;
	Event _writeCacheNotFull;

	void setDelayedWriteFrameBuffer(MediaFrameBufferSharedPtr frameBuffer);
	void queueFrameBufferForRegularOutput(const string &filename, MediaFrameBufferSharedPtr frameBuffer, bool nonBlocking = false);
	int writeFrameBufferToDiskSynchronously(const string &filename, MediaFrameBufferSharedPtr frameBuffer);
	void removeBufferFromCache(const string &filename);

	void spawnBackgroundThreads();
	void killBackgroundThreads();
	static void startDelayedWriterThread(void *vpAppData, void *vpReserved);
	void runDelayedWriterThread();
	static void startRegularWriterThread(void *vpAppData, void *vpReserved);
	void runRegularWriterThread();
 };

#endif
