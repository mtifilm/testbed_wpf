//---------------------------------------------------------------------------

#ifndef MediaFrameFileReaderH
#define MediaFrameFileReaderH

#include "timecode.h"
#include "MediaFileIoDefs.h"
#include "RawMediaFrameBuffer.h"
#include "MediaFrameBufferReadCache.h"
#include "MediaFrameBuffer.h"
//---------------------------------------------------------------------------

class MTI_MEDIAFILEIODLL_API MediaFrameFileReader
{
public:
	static MediaFrameFileReader *CreateMediaFrameFileReader(const string &filename);
	virtual ~MediaFrameFileReader() = default;

	virtual MediaFileType getFileType() = 0;

	virtual int prefetchFrameFile(const CImageFormat &imageFormat) = 0;
	virtual int getFrameBuffer(MediaFrameBufferSharedPtr &outFrameBuffer, const CImageFormat &imageFormat) = 0;

	virtual int getImageFormat(CImageFormat &imageFormat) = 0;
	virtual int dumpFileHeader(string &headerDumpString) = 0;

//	virtual void setTimecode(CTimecode timecode) = 0;
	virtual CTimecode getTimecode() = 0;

	static void RemoveFromHeaderCache(const string &filename);
	static void ClearHeaderCache();

protected:
	MediaFrameFileReader() = default;
};

class MediaFrameFileReaderBase : public MediaFrameFileReader
{
public:
	virtual ~MediaFrameFileReaderBase() = default;

	virtual MediaFileType getFileType() = 0;

	int prefetchFrameFile(const CImageFormat &imageFormat);
	int getFrameBuffer(MediaFrameBufferSharedPtr &outFrameBuffer, const CImageFormat &imageFormat);
	int getImageFormat(CImageFormat &imageFormat);
	int dumpFileHeader(string &headerDumpString);

////	virtual void setTimecode(CTimecode timecode) {};
	virtual CTimecode getTimecode() { return CTimecode::NOT_SET; };

protected:
	string _filename;
//	bool _imageFormatIsValid = false;
//	CImageFormat _imageFormat;

//	virtual int extractImageFormat(MediaFrameBufferSharedPtr buffer, CImageFormat &imageFormat) = 0;
	virtual int decodeImage(MediaFrameBufferSharedPtr rawFrameBuffer, MediaFrameBufferSharedPtr &decodedFrameBuffer) = 0;
	virtual size_t computeMinBufferSize(const CImageFormat &imageFormat);

	MediaFrameBufferSharedPtr readFrameFile(const CImageFormat &imageFormat);

//	void ExpandYToYYY(
//			const CImageFormat &imageFormat,
//			MTI_UINT8 *srcBuffer, int srcDataOffset,
//			MTI_UINT8 *dstBuffer, int dstDataOffset);

private:
	MediaFrameBufferSharedPtr retrieveFrameFileFromCache();
	MediaFrameBufferSharedPtr readFrameFileFromDisk(size_t minBufferSize);
};

#endif
