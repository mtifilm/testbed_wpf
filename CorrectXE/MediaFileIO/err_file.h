// -*- C++ -*-

#ifndef FILE_ERROR_H
#define FILE_ERROR_H

//////////////////////////////////////////////////////////////////////
// Error Code Defines (Numeric Order)
//
// Note: Keep numbers in the range of -6000 to -6199

#define FILE_ERROR_MALLOC                               -6069
#define FILE_ERROR_UNKNOWN_FILE_TYPE                    -6070
#define FILE_ERROR_OPEN                                 -6071
#define FILE_ERROR_SEEK                                 -6072
//                                                      -6073
#define FILE_ERROR_UNSUPPORTED_COMMAND                  -6074
#define FILE_ERROR_READ                                 -6075
#define FILE_ERROR_BAD_MAGIC_NUMBER                     -6076
#define FILE_ERROR_BAD_IMAGE_SIZE                       -6077
#define FILE_ERROR_UNSUPPORTED_OPTION                   -6078
//                                                      -6079
#define FILE_ERROR_BAD_COMPONENTS                       -6080
#define FILE_ERROR_BAD_BIT_DEPTH                        -6081
#define FILE_ERROR_WRITE                                -6082
#define FILE_ERROR_REPLACE                              -6083
#define FILE_ERROR_DELETE                               -6094
#define FILE_ERROR_STAT                                 -6095
#define FILE_ERROR_RENAME                               -6096
#define FILE_ERROR_INSUFFICIENT_PHYS_MEM                -6100
#define FILE_ERROR_IMAGE_COPY_FAILED                    -6104
#define FILE_ERROR_FILE_REPLACEMENT_FAILED              -6105

#define FILE_ERROR_DPX_FILE_IS_NOT_16BIT_RGBA           -6190
#define FILE_ERROR_DPX_FILE_IS_ALREADY_16BIT_RGB        -6191

#define FILE_ERROR_EXR_READ_EXCEPTION                   -6192
#define FILE_ERROR_INTERNAL                             -6193
#define FILE_ERROR_UNEXPECTED_NULL_POINTER              -6194

#define FILE_ERROR_EXR_HEADER_DECODE_FAILED             -6195
#define FILE_ERROR_DPX_HEADER_DECODE_FAILED             -6196
#define FILE_ERROR_TIFF_HEADER_READ_FAILED              -6197
#define FILE_ERROR_TIFF_HEADER_DECODE_FAILED            -6198
#define FILE_MEDIA_FILE_IS_TOO_SHORT                    -6199


//////////////////////////////////////////////////////////////////////
// Error Code Descriptions (Alpha Order)

// FORMAT_ERROR_BAD_BIT_DEPTH
//    the bit depth or packing of the file is not supported

// FORMAT_ERROR_BAD_COMPONENTS
//    the number of components in the file is not supported

// FORMAT_ERROR_BAD_DIMENSION
//    The image dimensions are bad

// FORMAT_ERROR_BAD_IMAGE_SIZE
//    the size of the image does not match the header info

// FORMAT_ERROR_BAD_MAGIC_NUMBER
//    the image does not have a valid MAGIC number in the header

// FORMAT_ERROR_INVALID_ALPHA_MATTE_TYPE
//    The alpha matte type is invalid for the image format

// FORMAT_ERROR_INVALID_BITS_PER_COMPONENT
//    The number of bits per component is invalid for the image format
//    or color space
   
// FORMAT_ERROR_INVALID_CHANNEL_COUNT
//    The Channel Count value is invalid or out-of-range in the Audio
//    Format section of a Clip File or a Clip Scheme

// FORMAT_ERROR_INVALID_COLOR_SPACE
//    The Color Space type is invalid in the Image Format section of a
//    Clip file or a Clip Scheme.

// FORMAT_ERROR_INVALID_FRAME_ASPECT_RATIO
//    The Frame Aspect Ratio is invalid in the Image Format section of
//    a Clip File or a Clip Scheme

// FORMAT_ERROR_INVALID_GAMMA
//    The Gamma is invalid in the Image Format section of
//    a Clip File or a Clip Scheme

// FORMAT_ERROR_INVALID_HORIZONTAL_INTERVAL_PIXELS
//    The Horizontal Interval Pixels value is invalid in the Image
//    Format section of a Clip file or a Clip Scheme

// FORMAT_ERROR_INVALID_IMAGE_FORMAT_TYPE
//    The Image Format type is invalid in the Image Format section of
//    a Clip file or a Clip Scheme or as an argument to a CImageFormat
//    class function

// FORMAT_ERROR_INVALID_LINES_PER_FRAME
//    The Lines per Frame is invalid in the Image Format section of a
//    Clip file or a Clip Scheme

// FORMAT_ERROR_INVALID_MEDIA_FORMAT_TYPE
//    Invalid or unexpected CMediaFormat type.  Possibly a corrupted
//    clip or clip initialization info.

// FORMAT_ERROR_INVALID_PIXEL_ASPECT_RATIO
//    The Pixel Aspect Ratio is invalid in the Image Format section of
//    a Clip file or a Clip Scheme.

// FORMAT_ERROR_INVALID_PIXEL_COMPONENTS
//    The Pixel Components type is invalid in the Image Format section
//    of a Clip file or a Clip Scheme.

// FORMAT_ERROR_INVALID_PIXEL_PACKING
//    The Pixel Packing type is invalid in the Image Format section of
//    a Clip file or a Clip Scheme.

// FORMAT_ERROR_INVALID_PIXELS_PER_LINE
//    The Pixels per Line value is invalid in the Image Format section
//    of a Clip file or a Clip Scheme

// FORMAT_ERROR_INVALID_SAMPLE_DATA_PACKING
//    The Sample Data Packing is invalid in the Audio Format section
//    of a Clip File or a Clip Scheme

// FORMAT_ERROR_INVALID_SAMPLE_DATA_SIZE
//    The Sample Data Size is invalid in the Audio Format section of a
//    Clip File or a Clip Scheme

// FORMAT_ERROR_INVALID_SAMPLE_DATA_TYPE
//    The Sample Data Type is invalid in the Audio Format section of a
//    Clip File or a Clip Scheme

// FORMAT_ERROR_INVALID_SAMPLE_ENDIAN
//    The Sample Data Endian is invalid in the Audio Format of a clip
//    or clip scheme

// FORMAT_ERROR_INVALID_SAMPLE_RATE
//    The Sample Rate value is invalid in the Audio Format section of
//    a Clip File or a Clip Scheme

// FORMAT_ERROR_INVALID_SAMPLES_PER_FIELD
//    The Samples per Field value is invalid in the Audio Format
//    section of a Clip File or a Clip Scheme

// FORMAT_ERROR_INVALID_TRANSFER_CHARACTERISTICS
//    The Transfer Characteristics value is invalid in the Image Format
//    section of a Clip file or a Clip Scheme

// FORMAT_ERROR_INVALID_VERTICAL_INTERVAL_ROWS
//    The Vertical Interval Rows value is invalid in the Image Format
//    section of a Clip file or a Clip Scheme

// FORMAT_ERROR_MALLOC
//    Trouble allocating storage

// FORMAT_ERROR_NO_AUDIO_FORMAT
//    The audio header data is missing the format section

// FORMAT_ERROR_NO_OPEN_AUDIO_FILE
//    The audio file has not been opened

// FORMAT_ERROR_NO_REMAINING_AUDIO_SAMPLES
//    The audio file does not have any more audio samples

// FORMAT_ERROR_OPEN
//    trouble opening file

// FORMAT_ERROR_PARAMETER_MISMATCH
//    current file parameters do not match prototype, or first, image

// FORMAT_ERROR_READ
//    trouble reading from file

// FORMAT_ERROR_SEEK
//    trouble seeking in file

// FORMAT_ERROR_UNKNOWN_FILE_TYPE
//    this file type is not supported

// FORMAT_ERROR_UNKNOWN_OPTION
//    this option is not known

// FORMAT_ERROR_UNSUPPORTED_AUDIO_OPTION
//    an audio option in the header is not supported

// FORMAT_ERROR_UNSUPPORTED_COMMAND
//    this command is not supported

// FORMAT_ERROR_UNSUPPORTED_OPTION
//    an option in the header is not supported

// FORMAT_ERROR_WRITE
//    trouble writing to hard disk

// FORMAT_ERROR_MAX_LICENSE_RESOLUTION_EXCEEDED
//    The maximum resolution is 1920 x 1080 but this file contains
//      a resolution greater than that.  This is related to the video-only
//      license - e.g. Teranex bundle.

// FORMAT_ERROR_CANT_DO_ASYNCH_IO
//    Was asked to do asynchronous I/O, but no can do.

// FORMAT_ERROR_HEADER_COPY_FAILED
//    Got an error while trying to copy the header from a maaster clip
//    frame to the corresponding dirty version clip frame


#endif // FORMAT_ERROR_H
