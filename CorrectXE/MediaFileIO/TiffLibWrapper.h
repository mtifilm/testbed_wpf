//---------------------------------------------------------------------------

#ifndef TiffLibWrapperH
#define TiffLibWrapperH
//---------------------------------------------------------------------------

#include "RawMediaFrameBuffer.h"
#include "tiffio.h"

class TiffHeader;

//////////////////////////////////////////////////////////////////////

////////////////////////////////////
// libtiff wrapper static class   //
////////////////////////////////////

//---------------------------------------------------------------------------

struct TiffLibHandle
{
   RawMediaFrameBufferSharedPtr frameBuffer;
   toff_t currentOffset;

	TiffLibHandle(RawMediaFrameBufferSharedPtr frameBufferArg)
   : frameBuffer(frameBufferArg)
   , currentOffset(0)
   {
   }

   ~TiffLibHandle()
   {
		frameBuffer = nullptr;
   }
};

typedef TiffLibHandle *TiffLibHandle_t;
//---------------------------------------------------------------------------

class TiffLibWrapper
{
	TiffLibWrapper() = default;

public:
	static tsize_t ReadProc(thandle_t thandle, tdata_t buf, tsize_t i32NumBytes);
	static tsize_t WriteProc(thandle_t thandle, tdata_t buf, tsize_t size);
	static  toff_t SeekProc(thandle_t thandle, toff_t offsetArg, int whence);
	static     int CloseProc(thandle_t thandle);
	static  toff_t SizeProc(thandle_t thandle);
	static     int MapProc(thandle_t thandle, tdata_t* pbase, toff_t* psize);
	static    void UnmapProc(thandle_t thandle, tdata_t base, toff_t size);
	static     int ReadHeader(thandle_t thandle, TiffHeader *hsp);
	static    void ErrorHandler(const char *module, const char *fmt, va_list ap);
	static    void WarningHandler(const char *module, const char *fmt, va_list ap);

/***************************************************************************
 TIFFClientOpen OPTIONS
 ****************************************************************************
 The open mode parameter can include the following flags in addition to the
 ``r'', ``w'', and ``a'' flags. Note however that option flags must follow
 the read-write-append specification.

 l   When creating a new file force information be written with Little-Endian
 byte order (but see below). By default the library will create new files
 using the native CPU byte order.

 b   When creating a new file force information be written with Big-Endian
 byte order (but see below). By default the library will create new files
 using the native CPU byte order.

 L   Force image data that is read or written to be treated with bits filled
 from Least Significant Bit (LSB) to Most Significant Bit (MSB). Note that
 this is the opposite to the way the library has worked from its inception.

 B   Force image data that is read or written to be treated with bits filled
 from Most Significant Bit (MSB) to Least Significant Bit (LSB); this is
 the default.

 H   Force image data that is read or written to be treated with bits filled
 in the same order as the native CPU.

 M   Enable the use of memory-mapped files for images opened read-only. If
 the underlying system does not support memory-mapped files or if the
 specific image being opened cannot be memory-mapped then the library
 will fallback to using the normal system interface for reading
 information.
 By default the library will attempt to use memory-mapped files.

 m   Disable the use of memory-mapped files.

 C   Enable the use of ``strip chopping'' when reading images that are
 comprised of a single strip or tile of uncompressed data. Strip chopping
 is a mechanism by which the library will automatically convert the
 single-strip image to multiple strips, each of which has about 8 KB
 of data. This facility can be useful in reducing the amount of memory
 used to read an image because the library normally reads each strip in
 its entirety. Strip chopping does however alter the apparent contents
 of the image because when an image is divided into multiple strips it
 looks as though the underlying file contains multiple separate strips.
 Finally, note that default handling of strip chopping is a compile-time
 configuration parameter. The default behaviour, for backwards
 compatibility, is to enable strip chopping.

 c   Disable the use of strip chopping when reading images.

 h   Read TIFF header only, do not load the first image directory. That could
 be useful in case of the broken first directory. We can open the file and
 proceed to the other directories.

 *************************************************************************** */

	static TIFF* ClientOpen(thandle_t thandle, const char* name, const char* mode);
 };

// -------------------------------------------------------------------------





#endif
