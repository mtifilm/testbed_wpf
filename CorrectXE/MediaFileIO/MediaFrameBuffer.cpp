//---------------------------------------------------------------------------

#include "MediaFrameBuffer.h"
#include "DllSupport.h"
#include "err_file.h"
#include "IniFile.h"
#include "MediaFrameFileReader.h"
#include "MTImalloc.h"
#include "SysInfo.h"
//---------------------------------------------------------------------------

unsigned char *MediaFrameBuffer::_Blackness = nullptr;
int MediaFrameBuffer::_SizeOfBlacknessInBytes = 0;
CSpinLock MediaFrameBuffer::_BlacknessLock;
//---------------------------------------------------------------------------

/* static */
MediaFrameBufferSharedPtr MediaFrameBuffer::CreatePrivateBuffer(const CImageFormat &imageFormat)
{
//	return new MediaFrameBufferWithPrivateData(imageFormat);
	return std::shared_ptr<MediaFrameBuffer>(new MediaFrameBufferWithPrivateData(imageFormat));
}
//-------------------------------------------------------------------------

/* static */
MediaFrameBufferSharedPtr MediaFrameBuffer::CreatePrivateBufferWithTemplate(MediaFrameBuffer &templateBuffer, size_t newSize)
{
//	return new MediaFrameBufferWithPrivateData(templateBuffer._imageFormat, newSize);
	return std::shared_ptr<MediaFrameBuffer>(new MediaFrameBufferWithPrivateData(templateBuffer._imageFormat, newSize));
}
//-------------------------------------------------------------------------

/* static */
MediaFrameBufferSharedPtr MediaFrameBuffer::CreatePrivateBufferWithCopyOfImageData(MediaFrameBuffer &sourceBuffer)
{
	MediaFrameBuffer *newMediaFrameBuffer = new MediaFrameBufferWithPrivateData(sourceBuffer._imageFormat, sourceBuffer.getBufferSize());
	MTImemcpy(newMediaFrameBuffer->getBufferPtr(), sourceBuffer.getBufferPtr(), sourceBuffer.getBufferSize());
//	return newMediaFrameBuffer;
	return std::shared_ptr<MediaFrameBuffer>(newMediaFrameBuffer);
}
//-------------------------------------------------------------------------

/* static */
MediaFrameBufferSharedPtr MediaFrameBuffer::CreateWrappedImageDataBuffer(MTI_UINT8 *imageData, const CImageFormat &imageFormat)
{
//	return new MediaFrameBufferWrappingImageData(imageData, imageFormat);
	return std::shared_ptr<MediaFrameBuffer>(new MediaFrameBufferWrappingImageData(imageData, imageFormat));
}
//-------------------------------------------------------------------------

///* static */
//MediaFrameBufferSharedPtr MediaFrameBuffer::CreateSharedBuffer(const string &filename, RawMediaFrameBufferSharedPtr rawFrameBuffer, const CImageFormat &imageFormat)
//{
//	return new MediaFrameBufferWithSharedData(filename, rawFrameBuffer, imageFormat);
//}
/* static */
MediaFrameBufferSharedPtr MediaFrameBuffer::CreateSharedBuffer(RawMediaFrameBufferSharedPtr rawFrameBuffer, const CImageFormat &imageFormat, bool isDecodedFlag)
{
   auto buffer = std::shared_ptr<MediaFrameBuffer>(new MediaFrameBufferWithSharedData(rawFrameBuffer, imageFormat, isDecodedFlag));
   if (buffer->getErrorInfo().code != 0)
   {
      buffer = CreateErrorBuffer(imageFormat, buffer->getErrorInfo());
   }

   return buffer;
}

/* static */
MediaFrameBufferSharedPtr MediaFrameBuffer::CreateErrorBuffer(const CImageFormat &imageFormat, const ErrorInfo &errorInfo)
{
   CAutoSpinLocker lock(_BlacknessLock);
   if (_Blackness == nullptr || _SizeOfBlacknessInBytes < imageFormat.getBytesPerField())
   {
      MTIfree(_Blackness);
      _SizeOfBlacknessInBytes = imageFormat.getBytesPerField();
      _Blackness = (unsigned char *) MTImalloc(_SizeOfBlacknessInBytes);
      MTImemset(_Blackness, 0, _SizeOfBlacknessInBytes);
   }

   MediaFrameBufferSharedPtr errorFrameBuffer = CreateWrappedImageDataBuffer(_Blackness, imageFormat);
   errorFrameBuffer->setErrorInfo(errorInfo);
   return errorFrameBuffer;
}


//-------------------------------------------------------------------------


///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

MediaFrameBuffer::MediaFrameBuffer()
{
}
//-------------------------------------------------------------------------

MediaFrameBuffer::~MediaFrameBuffer()
{
	MTIfree(_headerData);
	MTIfree(_trailerData);
}
//-------------------------------------------------------------------------

void MediaFrameBuffer::setImageDataOffset(size_t offset)
{
   size_t bufferSize = getBufferSize();
   if (offset >= bufferSize)
   {
      offset = 0;
   }

	_imageDataOffset = offset;
}
//-------------------------------------------------------------------------

bool MediaFrameBuffer::setTotalDataSize(size_t size)
{
   bool retVal = true;
	size_t bufferSize = getBufferSize();
	if (size > bufferSize)
   {
      TRACE_0(errout << "INTERNAL ERROR: MediaFrameBuffer::setTotalDataSize(" << size << ") > buffer size " << bufferSize);
      size = bufferSize;
      retVal = false;
	}

	if (size <= 0)
	{
		_totalDataSize = bufferSize;
	}
	else
	{
		_totalDataSize = size;
	}

   return retVal;
}
//-------------------------------------------------------------------------

unsigned char *MediaFrameBuffer::getImageDataPtr()
{
   unsigned char *bufferPtr = (unsigned char *)getBufferPtr();
   if (bufferPtr == nullptr)
   {
      return nullptr;
   }

	return bufferPtr + _imageDataOffset;
}
//-------------------------------------------------------------------------

size_t MediaFrameBuffer::getImageDataSize()
{
	return _totalDataSize;
}
//-------------------------------------------------------------------------

void MediaFrameBuffer::setHasSeparateAlpha(bool flag)
{
	_hasSeparateAlpha = flag;
}
//-------------------------------------------------------------------------

bool MediaFrameBuffer::hasSeparateAlpha()
{
   return _hasSeparateAlpha;
}
//-------------------------------------------------------------------------

void MediaFrameBuffer::setAlphaDataOffset(size_t offset)
{
	size_t bufferSize = getBufferSize();
	MTIassert(offset == 0 || offset < bufferSize);
	if (offset > 0 && offset >= bufferSize)
	{
		offset = 0;
	}

	_alphaDataOffset = offset;
}
//-------------------------------------------------------------------------

size_t MediaFrameBuffer::getAlphaDataSize()
{
   size_t alphaSize = _imageFormat.getAlphaBytesPerField();
	MTIassert((_alphaDataOffset + alphaSize) < getBufferSize());
   return alphaSize;
}
//-------------------------------------------------------------------------

unsigned char *MediaFrameBuffer::getAlphaDataPtr()
{
   unsigned char *bufferPtr = getBufferPtr();
   if (bufferPtr == nullptr)
   {
      return nullptr;
   }

	return bufferPtr + _alphaDataOffset;
}
//-------------------------------------------------------------------------
//
//void MediaFrameBuffer::setFilename(const string &newFilename)
//{
//	_filename = newFilename;
//}
//-------------------------------------------------------------------------
//
//string MediaFrameBuffer::getFilename()
//{
//	return _filename;
//}
//-------------------------------------------------------------------------
//
//void MediaFrameBuffer::setFrameIndex(int newIndex)
//{
//   _frameIndex = newIndex;
//}
//-------------------------------------------------------------------------
//
//int MediaFrameBuffer::getFrameIndex()
//{
//	return _frameIndex;
//}
//-------------------------------------------------------------------------

void MediaFrameBuffer::setImageFormat(const CImageFormat newFormat)
{
   _imageFormat = newFormat;
}
//-------------------------------------------------------------------------

CImageFormat MediaFrameBuffer::getImageFormat()
{
   return _imageFormat;
}
//-------------------------------------------------------------------------

size_t MediaFrameBuffer::getCacheFootprint()
{
   // Changed this to only report the LOCKED memory.
   const size_t KB64 = 64 * 1024;
   size_t paddedBufferSize = KB64 * ((getBufferSize() + KB64 - 1) / KB64);
	return paddedBufferSize;
}
//-------------------------------------------------------------------------

RawMediaFrameBufferSharedPtr MediaFrameBuffer::getRawMediaFrameBuffer()
{
//	auto newFrameBuffer = make_shared<RawMediaFrameBuffer>(getBufferSize());
//	MTImemcpy(newFrameBuffer->getBufferPtr(), getBufferPtr(), getBufferSize());
//   return newFrameBuffer;
   return nullptr;
}
//-------------------------------------------------------------------------

void MediaFrameBuffer::setHeaderData(void *headerData, size_t headerDataSize)
{
	MTIfree(_headerData);
	_headerDataSize = 0;
	_headerData = nullptr;

	if (headerDataSize == 0 || headerData == nullptr)
	{
		return;
	}

	_headerDataSize = headerDataSize;
	_headerData = MTImalloc(headerDataSize);
	MTImemcpy(_headerData, headerData, headerDataSize);
}
//-------------------------------------------------------------------------

void MediaFrameBuffer::setTrailerData(void *trailerData, size_t trailerDataSize)
{
	MTIfree(_trailerData);
	_trailerDataSize = 0;
	_trailerData = nullptr;

	if (trailerDataSize == 0 || trailerData == nullptr)
	{
		return;
	}

	_trailerDataSize = trailerDataSize;
	_trailerData = MTImalloc(trailerDataSize);
	MTImemcpy(_trailerData, trailerData, trailerDataSize);
}
//------------------------------------------------------------------------------

MediaFrameBufferSharedPtr MediaFrameBuffer::clone()
{
	MediaFrameBufferSharedPtr clone;
	auto rawMediaFrameBufferIn = getRawMediaFrameBuffer();
   MTIassert(rawMediaFrameBufferIn);
   if (!rawMediaFrameBufferIn)
   {
      ErrorInfo errorInfo { FILE_ERROR_UNEXPECTED_NULL_POINTER, "", "Internal error", "" };
      return CreateErrorBuffer(_imageFormat, errorInfo);
   }

	size_t totalBufferSize = rawMediaFrameBufferIn->getBufferSize();
	auto rawMediaFrameBufferOut = std::make_shared<RawMediaFrameBuffer>(totalBufferSize);
	size_t dataSize = rawMediaFrameBufferIn->getDataSize();
	rawMediaFrameBufferOut->setDataSize(dataSize);

	MTImemcpy(rawMediaFrameBufferOut->getBufferPtr(),
				 rawMediaFrameBufferIn->getBufferPtr(),
				 totalBufferSize);

	clone = CreateSharedBuffer(rawMediaFrameBufferOut, _imageFormat, _isDecoded);

	clone->_totalDataSize = _totalDataSize;
	clone->_imageDataOffset = _imageDataOffset;
	clone->_alphaDataOffset = _alphaDataOffset;
	clone->_hasSeparateAlpha = _hasSeparateAlpha;
	clone->_opaqueMetadata = _opaqueMetadata;
	clone->_timecode = _timecode;
	clone->_dataSwapStatus = _dataSwapStatus;
	clone->_errorInfo = _errorInfo;

	clone->setHeaderData(this->getHeaderDataPtr(), this->getHeaderDataSize());
	clone->setTrailerData(this->getTrailerDataPtr(), this->getTrailerDataSize());
	clone->setOpaqueMetadata(this->getOpaqueMetadata());
	clone->setImageDataOffset(this->getImageDataOffset());

	return clone;
}
//------------------------------------------------------------------------------

MediaFrameBufferSharedPtr MediaFrameBuffer::cloneWithoutImageData()
{
	MediaFrameBufferSharedPtr clone;

	// Can only work on decoded buffers!
	MTIassert(isDecoded());
	if (!isDecoded())
	{
		return clone;
	}

	// QQQ Check total buffer size vs actual file size
	auto rawMediaFrameBufferIn = getRawMediaFrameBuffer();
   MTIassert(rawMediaFrameBufferIn);
   if (!rawMediaFrameBufferIn)
   {
      ErrorInfo errorInfo { FILE_ERROR_UNEXPECTED_NULL_POINTER, "", "Internal error", "" };
      return CreateErrorBuffer(_imageFormat, errorInfo);
   }

	size_t totalBufferSize = rawMediaFrameBufferIn->getBufferSize();
//	DBTRACE(totalBufferSize);
	auto rawMediaFrameBufferOut = std::make_shared<RawMediaFrameBuffer>(totalBufferSize);
	size_t dataSize = rawMediaFrameBufferIn->getDataSize();
//	DBTRACE(dataSize);
	rawMediaFrameBufferOut->setDataSize(dataSize);

	size_t preDataSize = getImageDataOffset();
//	DBTRACE(preDataSize);
	//size_t postDataOffset = preDataSize + getImageDataSize();
	size_t postDataOffset = preDataSize + _imageFormat.getBytesPerFieldExcAlpha();
//	DBTRACE(postDataOffset);
	int postDataSizeAsInt = dataSize - postDataOffset;
//	DBTRACE(postDataSizeAsInt);
	MTIassert(postDataSizeAsInt >= 0);
	size_t postDataSize = (postDataSizeAsInt > 0) ? (size_t) postDataSizeAsInt : 0;
//	DBTRACE(postDataSize);

	if (preDataSize > 0)
	{
		MTImemcpy(rawMediaFrameBufferOut->getBufferPtr(),
					 rawMediaFrameBufferIn->getBufferPtr(),
					 preDataSize);
	}

	if (postDataSize > 0)
	{
		MTImemcpy((unsigned char *)rawMediaFrameBufferOut->getBufferPtr() + postDataOffset,
					 (unsigned char *)rawMediaFrameBufferIn->getBufferPtr() + postDataOffset,
					 postDataSize);
	}

	clone = CreateSharedBuffer(rawMediaFrameBufferOut, _imageFormat, true);

	clone->_totalDataSize = _totalDataSize;
	clone->_imageDataOffset = _imageDataOffset;
	clone->_alphaDataOffset = _alphaDataOffset;
	clone->_hasSeparateAlpha = _hasSeparateAlpha;
//	DBTRACE(clone->_totalDataSize);
//	DBTRACE(clone->_imageDataOffset);
//	DBTRACE(clone->_alphaDataOffset);
//	DBTRACE(clone->_hasSeparateAlpha);
	clone->_opaqueMetadata = _opaqueMetadata;
	clone->_timecode = _timecode;
	clone->_dataSwapStatus = _dataSwapStatus;
	clone->_errorInfo = _errorInfo;

	clone->setHeaderData(this->getHeaderDataPtr(), this->getHeaderDataSize());
	clone->setTrailerData(this->getTrailerDataPtr(), this->getTrailerDataSize());
	clone->setOpaqueMetadata(this->getOpaqueMetadata());
	clone->setImageDataOffset(this->getImageDataOffset());

	return clone;
}
//-------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

struct MTIfree_deleter
{
	void operator()(void *p)
	{
		MTIfree(p);
	}
};

MediaFrameBufferWithPrivateData::MediaFrameBufferWithPrivateData(const CImageFormat &imageFormat)
: MediaFrameBufferWithPrivateData(imageFormat, imageFormat.getBytesPerField())
{
}
//-------------------------------------------------------------------------

MediaFrameBufferWithPrivateData::MediaFrameBufferWithPrivateData(const CImageFormat &imageFormat, size_t overrideSizeInBytes)
{
   size_t pageSize = SysInfo::GetSystemPageSize();
	_imageFormat = imageFormat;
	_bufferSize = pageSize * ((overrideSizeInBytes + pageSize - 1) / pageSize);
	_bufferPtr = (MTI_UINT8*) MTImemalign(pageSize, _bufferSize);

	_imageDataOffset = 0;    // Needs to be corrected at higher level
	// QQQ THIS IS WRONG -- NEED TO ADD THE GAP BETWEEN IMAGE AND ALPHA DATA
	auto setSizeError = !setTotalDataSize(imageFormat.getBytesPerField());
	setHasSeparateAlpha(false);
	_alphaDataOffset = 0;

	if (setSizeError)
   {
      TRACE_0(errout << "Failed setTotalDataSize() was called from MediaFrameBufferWithPrivateData constructor()");
   }
}
//-------------------------------------------------------------------------

MediaFrameBufferWithPrivateData::~MediaFrameBufferWithPrivateData()
{
	if (_bufferPtr && _bufferSize > 0)
	{
		MTIfree(_bufferPtr);
	}
}
//-------------------------------------------------------------------------

unsigned char *MediaFrameBufferWithPrivateData::getBufferPtr()
{
	return _bufferPtr;
}
//-------------------------------------------------------------------------

size_t MediaFrameBufferWithPrivateData::getBufferSize()
{
   return _bufferSize;
}
//-------------------------------------------------------------------------


///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

MediaFrameBufferWithSharedData::MediaFrameBufferWithSharedData(
	RawMediaFrameBufferSharedPtr rawFrameBuffer,
	const CImageFormat &imageFormat,
	bool isDecodedFlag)
{
	_rawMediaFrameBufferSharedPtr = rawFrameBuffer;
	_imageFormat = imageFormat;

	_imageDataOffset = 0;    // Needs to be corrected at higher level
	auto setSizeError = !setTotalDataSize(rawFrameBuffer ? getBytesPerFieldInFile() : 0);
	setHasSeparateAlpha(false);
	_alphaDataOffset = 0;
	setIsDecoded(isDecodedFlag);

	if (setSizeError)
   {
      TRACE_0(errout << "Failed setTotalDataSize() was called from MediaFrameBufferWithSharedData constructor()");
      string shortString = "Frame media file is corrupt (too short)";
      setErrorInfo({FILE_MEDIA_FILE_IS_TOO_SHORT, "", shortString, ""});
   }
}
//-------------------------------------------------------------------------

MediaFrameBufferWithSharedData::~MediaFrameBufferWithSharedData()
{
   if (!_rawMediaFrameBufferSharedPtr)
   {
      TRACE_3(errout << "DELETING MEDIA FRAME BUFFER with null raw buffer");
      return;
   }

   TRACE_3(errout << "DELETE MEDIA FRAME BUFFER with raw buffer id = "
                  << _rawMediaFrameBufferSharedPtr->getBufferId()
                  << ", raw shared count = " <<  _rawMediaFrameBufferSharedPtr.use_count());
   _rawMediaFrameBufferSharedPtr = nullptr;
}
//-------------------------------------------------------------------------

unsigned char *MediaFrameBufferWithSharedData::getBufferPtr()
{
	if (!_rawMediaFrameBufferSharedPtr)
   {
      return nullptr;
   }

   return (unsigned char *)_rawMediaFrameBufferSharedPtr->getBufferPtr();
}
//-------------------------------------------------------------------------

size_t MediaFrameBufferWithSharedData::getBufferSize()
{
   if (!_rawMediaFrameBufferSharedPtr)
   {
      return 0;
   }

	return _rawMediaFrameBufferSharedPtr->getDataSize();
}
// --------------------------------------------------------------------

size_t MediaFrameBufferWithSharedData::getBytesPerFieldInFile()
{
	size_t bytesPerField = (size_t)_imageFormat.getBytesPerField();

//	// If we are pretending that a 1-channel monochrome clip has three channels,
//	// need to subtract off the size of the two phantom channels.
//	if (_imageFormat.getPixelComponents() == IF_PIXEL_COMPONENTS_Y)
//	{
//		if (_imageFormat.getPixelPacking() == IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE ||
//				_imageFormat.getPixelPacking() == IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE)
//		{
//			// 4 = 2 components * 2 bytes/component
//			bytesPerField -= 4 * _imageFormat.getPixelsPerLine() * _imageFormat.getLinesPerFrame();
//		}
//	}

	return bytesPerField;
}
// --------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

MediaFrameBufferWrappingImageData::MediaFrameBufferWrappingImageData(unsigned char *wrappedImageData, const CImageFormat &imageFormat)
: _wrappedImageData(wrappedImageData)
{
	setImageFormat(imageFormat);
	_imageDataOffset = 0;
	setTotalDataSize(imageFormat.getBytesPerField());
	setHasSeparateAlpha(false);
	_alphaDataOffset = 0;
}
// --------------------------------------------------------------------

MediaFrameBufferWrappingImageData::~MediaFrameBufferWrappingImageData()
{
}
// --------------------------------------------------------------------

unsigned char *MediaFrameBufferWrappingImageData::getBufferPtr()
{
	return _wrappedImageData;
}
// --------------------------------------------------------------------

size_t MediaFrameBufferWrappingImageData::getBufferSize()
{
	return getImageDataSize();
}
//-------------------------------------------------------------------------
