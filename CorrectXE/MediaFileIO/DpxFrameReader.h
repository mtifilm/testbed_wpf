//---------------------------------------------------------------------------

#ifndef DpxFrameReaderH
#define DpxFrameReaderH
//---------------------------------------------------------------------------

#include "DpxHeader.h"
#include "MediaFrameFileReader.h"
//---------------------------------------------------------------------------

class DpxFrameReader : public MediaFrameFileReaderBase
{
public:
	DpxFrameReader(const string & filename);
	~DpxFrameReader();

	MediaFileType getFileType() { return MediaFileType::Dpx; };
	int decodeImage(MediaFrameBufferSharedPtr frameBufferIn, MediaFrameBufferSharedPtr &frameBufferOut);
	int dumpFileHeader(string &headerDumpString);

//	static void RemoveFromHeaderCache(const string &filename);
//	static void ClearHeaderCache();

private:
//	static DpxHeaderCache _HeaderCache;

	int extractHeader(MediaFrameBufferSharedPtr frameBufferIn, DpxHeader &headerOut);
};
//---------------------------------------------------------------------------

#endif

