//
// MMOFStream.h
//

#ifndef MMOFSTREAM_H
#define MMOFSTREAM_H

#include <ImfStdIO.h>
#include <ImfIO.h>
#include "Iex.h"

using namespace Imath;

class MMOFStream: public Imf::OStream
{
public:
    MMOFStream(const char fileName[]);
    ~MMOFStream();

    virtual void	write(const char c[/*n*/], int n);
    virtual Int64	tellp();
    virtual void	seekp(Int64 pos);

private:
    FILE *_file;
};

#endif