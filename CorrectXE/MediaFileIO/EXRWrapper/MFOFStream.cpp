//
//  MFOFStream.cpp
//

#include "MFOFStream.h"
#include <memory.h>

//---------------------------------------------------------
MFOFStream::MFOFStream(char *bufferPtr, Int64 bufferSize, Int64 *eofOut)
	: _bufferPtr(bufferPtr)
	, _bufferSize(bufferSize)
	, _eofOut(eofOut)
	, _pos(0)
	, OStream("MEMORY FILE")
{
	if (_eofOut)
	{
		*_eofOut = 0;
	}
}

//---------------------------------------------------------
MFOFStream::~MFOFStream()
{
}

//---------------------------------------------------------
void MFOFStream::write(const char c[/*n*/], int n)
{
	if ((_pos + n) > _bufferSize)
	{
		throw Iex::IoExc("INTERNAL ERROR: EXR output file buffer is too small!");
	}

	memcpy(_bufferPtr + _pos, c, n);
	_pos += n;
	if (_eofOut && _pos > *_eofOut)
	{
		*_eofOut = _pos;
	}
}

//---------------------------------------------------------
Int64 MFOFStream::tellp()
{
    return _pos;
}

//---------------------------------------------------------
void MFOFStream::seekp(Int64 pos)
{
	if (pos > _bufferSize)
	{
		throw Iex::IoExc("INTERNAL ERROR: EXR tried to seek past end of file output buffer!");
	}

	if (pos < 0)
	{
		throw Iex::IoExc("INTERNAL ERROR: EXR tried to seek to a negative position!");
	}

	_pos = pos;
	if (_eofOut && _pos > *_eofOut)
	{
		*_eofOut = _pos;
	}
}