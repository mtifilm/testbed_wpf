//
//  EXRWrapper.h
//

#ifndef EXR_WRAPPER_H
#define EXR_WRAPPER_H

extern "C" {

    // EXRCAMDLL_EXPORTS is defined when the dll is built.
    // Users of the dll do not define EXRCAMDLL_EXPORTS

#ifdef EXR_WRAPPER_NO_DLL
# define EXRWRAPPER_API
#else
#if defined(EXRWRAPPER_EXPORTS)
# define EXRWRAPPER_API __declspec(dllexport)
#else
# define EXRWRAPPER_API __declspec(dllimport)
#endif
#endif

#define EXR_WRAP_RET_SUCCESS                   0  // successful return
#define EXR_WRAP_ERR_MALLOC               -10225
#define EXR_WRAP_ERR_NOT_ALLOC            -10226
#define EXR_WRAP_ERR_CORRUPT_FILE         -10227
#define EXR_WRAP_ERR_NULL_PARAMETER       -10228
#define EXR_WRAP_ERR_BAD_FILE_NAME        -10229
#define EXR_WRAP_ERR_BUFFER_TOO_SMALL     -10230
#define EXR_WRAP_ERR_BAD_TIMECODE         -10231
#define EXR_WRAP_ERR_WRITE_FAIL           -10232
#define EXR_WRAP_ERR_READ_FAIL            -10233
#define EXR_WRAP_ERR_OPEN_FAIL            -10234
#define EXR_WRAP_ERR_FORMAT_NOT_SUPPORTED -10235

    ///////////////////////////////////////////////////////////
    // Functions for decoding an existing EXR sequence

    // sets the global thread count
    EXRWRAPPER_API int EXR_SetGlobalThreadCount(int threadCount);

    // Open an EXR file on disk.
    EXRWRAPPER_API void *EXR_OpenInputFile(const char *cpFileName, int *ipRetVal);

	// Open an EXR memory file.
	EXRWRAPPER_API void *EXR_OpenInputMemoryFile(char *dataPtr, unsigned int dataLen, int *ipRetVal);

	// Get/release the IMF header
	EXRWRAPPER_API int EXR_GetOpaqueHeader(void *vpExrFile, void *&opaqueHeader);
	EXRWRAPPER_API void EXR_ReleaseOpaqueHeader(void *opaqueHeader);

	// Get image width of an opened EXR file.
    EXRWRAPPER_API int EXR_Width(void *vpExrFile, int *iWidth);

    // get image height of an opened EXR file.
    EXRWRAPPER_API int EXR_Height(void *vpExrFile, int *iHeight);

    // get the frame rate
    EXRWRAPPER_API int EXR_FrameRate(void *vpExrFile, float *fpFrameRate);

    // get starting timecode of an opened EXR file.
    EXRWRAPPER_API int EXR_Timecode(void *vpExrFile, char *cpTimecode, int iBufferLength);

    // get shoot date of an opened EXR file.
    EXRWRAPPER_API int EXR_Date(void *vpExrFile, char *cpDate, int iBufferLength);

    // get look mod transform function name of an opened EXR file.
    EXRWRAPPER_API int EXR_LookModTransform(void *vpExrFile, char *cpTransform, int iBufferLength);

	// get UUID of an opened EXR file.
	EXRWRAPPER_API int EXR_UUID(void *vpExrFile, char *cpUUID, int iBufferLength);

	// Is uncompressed?
	EXRWRAPPER_API int EXR_IsUncompressed(void *vpExrFile, bool *isUncompressedOutPtr);

	// Is RGB or RGBA data?
	EXRWRAPPER_API int EXR_IsRGB(void *vpExrFile, bool *isRGBOutPtr);

	// Number of channels
	EXRWRAPPER_API int EXR_NumChannels(void *vpExrFile, int *numChannelsOutPtr);

    // Decode exr frame into a 16-bit floating point RGBA image buffer
    EXRWRAPPER_API int EXR_ReadPixels(void *vpExrFile, unsigned char *ucpBuffer);

    ///////////////////////////////////////////////////////////
    // Functions for creating a new EXR sequence

	// Create an EXR file..
	EXRWRAPPER_API void *EXR_OpenOutputFile(const char *cpFileName, void *opaqueHeader, int *ipRetVal);

	// Create an in-memory EXR file.
	EXRWRAPPER_API void *EXR_OpenOutputMemoryFile(unsigned char *fileBuffer, unsigned int fileBufferSize, void *opaqueHeader, unsigned int *fileEofOut, int *ipRetVal);

    // Set image width of a new EXR sequence
    EXRWRAPPER_API int EXR_SetWidth(void *vpExrFile, int iWidth);

    // Set image height of a new EXR sequence
    EXRWRAPPER_API int EXR_SetHeight(void *vpExrFile, int iHeight);

    // Set the frame rate
    EXRWRAPPER_API int EXR_SetFrameRate(void *vpExrFile, float fFrameRate);

    // set timecode for EXR metadata
    EXRWRAPPER_API int EXR_SetTimecode(void *vpExrFile, const char *cpTimecode);

	// Encode exr frame from a 16-bit floating point (A)RGB image buffer, writes output .exr file
	EXRWRAPPER_API int EXR_WritePixels(void *vpExrFile, const unsigned char *ucpBuffer);

    // Done
    EXRWRAPPER_API int EXR_CloseFile(void *vpExrFile);

    ///////////////////////////////////////////////////////////
    // Misc funcs

    EXRWRAPPER_API int EXR_DumpHeader(void *ilmHeader, char *outBuffer, int n);

    EXRWRAPPER_API void EXR_GetLastErrorMessage(void *vpExrFILE, char *outBuffer, int n);
};

#endif