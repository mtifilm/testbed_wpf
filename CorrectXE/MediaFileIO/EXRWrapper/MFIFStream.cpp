//
// MFIFStream.cpp
//

#include "MFIFStream.h"
#include "windows.h"

//------------------------------------------------------------------------------

MFIFStream::MFIFStream(char *dataPtr, size_t dataLen)
	: IStream("MemoryFile")
	, _buffer(dataPtr)
	, _length(dataLen)
	, _pos(0)
{
	// Empty
}

MFIFStream::MFIFStream(const char fileName[])
	: IStream(fileName)
	, _buffer(nullptr)
	, _length(0)
	, _pos(0)
{
	// Empty
}
//------------------------------------------------------------------------------

MFIFStream::~MFIFStream()
{
   // Empty.
}
//------------------------------------------------------------------------------

bool  MFIFStream::read (char c[/*n*/], int n)
{
    if (n == 1 && _pos < _length)
    {
       c[0] = _buffer[_pos++];
       return true;
    }

    if (_length == 0)
    {
        return false;
    }
    
    if ((_pos < 0 || _pos >= _length) && n != 0)
    {
        throw Iex::InputExc ("Unexpected end of file.");
    }

    if (n == 1)
    {
       c[0] = _buffer[_pos++];
       return true;
    }

    Int64 n2 = n;
    bool retVal = true;

    if (_length - _pos <= n2)
    {
        n2 = _length - _pos;
        retVal = false;
    }

    memcpy (c, &(_buffer[_pos]), (size_t)n2);
    _pos += n2;
    return retVal;
}
//------------------------------------------------------------------------------

char*  MFIFStream::readMemoryMapped (int n)
{
    if (_pos < 0 || _pos >= _length)
    {
        throw Iex::InputExc ("Unexpected end of file.");
    }

    if (_pos + n > _length)
    {
        throw Iex::InputExc ("Reading past end of file.");
    }

    char* retVal = &(_buffer[_pos]);
    _pos += n;
    return retVal;
}


