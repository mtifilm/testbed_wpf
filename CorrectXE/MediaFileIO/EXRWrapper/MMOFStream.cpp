//
//  MMOFStream.cpp
//

#include "MMOFStream.h"
#include <windows.h>   // for DeleteFile()

static TCHAR lastErrorMessage[256];

//---------------------------------------------------------
MMOFStream::MMOFStream(const char fileName[])
:	OStream(fileName)
{
   // Delete the file
   int retVal = DeleteFile(fileName);
   if (retVal == 0)
   {
      // This is the windows call to get the proper error message
      FormatMessage(
         FORMAT_MESSAGE_FROM_SYSTEM,
         NULL,
         GetLastError(),
         MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US),
         lastErrorMessage,
         256,
         NULL);
   }

   _file = fopen(fileName, "wb");
   if (_file == NULL)
   {
        Iex::throwErrnoExc();
   }
}

//---------------------------------------------------------
MMOFStream::~MMOFStream()
{
   if (_file != NULL)
   {
      fclose(_file);
   }
}

//---------------------------------------------------------
void MMOFStream::write(const char c[/*n*/], int n)
{
   if (_file == NULL)
   {
      throw Iex::IoExc("Broken output stream");
   }

   clearerr(_file);

   if (n != fwrite(c, 1, n, _file))
   {
      Iex::throwErrnoExc();
   }
}

//---------------------------------------------------------
Int64 MMOFStream::tellp()
{
   if (_file == NULL)
   {
      throw Iex::IoExc("Broken output stream");
   }

    return ftell(_file);
}

//---------------------------------------------------------
void MMOFStream::seekp(Int64 pos)
{
   if (_file == NULL)
   {
      throw Iex::IoExc("Broken output stream");
   }

    clearerr(_file);
    fseek(_file, (long)pos, SEEK_SET);
}