// 
//  ExrFILE.h
//

#ifndef EXRFILE_H
#define EXRFILE_H

#include "ImfHeader.h"
#include "ImfInputFile.h"
#include "ImfOutputFile.h"
#include "ImfBoxAttribute.h"
#include "ImfChannelListAttribute.h"
#include "ImfChromaticitiesAttribute.h"
#include "ImfCompressionAttribute.h"
#include "ImfDoubleAttribute.h"
#include "ImfEnvmapAttribute.h"
#include "ImfFloatAttribute.h"
#include "ImfIntAttribute.h"
#include "ImfKeyCodeAttribute.h"
#include "ImfLineOrderAttribute.h"
#include "ImfMatrixAttribute.h"
#include "ImfPreviewImageAttribute.h"
#include "ImfRationalAttribute.h"
#include "ImfStringAttribute.h"
#include "ImfStringVectorAttribute.h"
#include "ImfTileDescriptionAttribute.h"
#include "ImfTimeCodeAttribute.h"
#include "ImfVecAttribute.h"
#include "ImfVersion.h"
using namespace Imf;

#include <vector>
using std::vector;
#include <string>
using std::string;

enum ExrChannels
{
	Other,
	RGB,
	RGBA
};

struct ExrFILE
{
    int iMinX = 0;
	int iMinY = 0;
	int iMaxY = 0;
	int iWidth = 0;
	int iHeight = 0;

	ExrChannels _channels = Other;
	vector<string> _layerList;

	bool isUncompressed = false;
	bool bAcesImageContainer = false;   // true iff an ACES file, and not just an openEXR file

    unsigned long ulEndOfHeader = 0;   // offset from start of file to start of line offset table.
	unsigned long fileSize = 0;
	unsigned long *externalFileSizePtr = nullptr;
    string strFileName;
	char *memoryFileBuffer = nullptr;
	unsigned int memoryFileBufferLength = 0;

    string strTimecode;
    string strDate;
    string strLookModTransform;
    string strUUID;
	float fWhiteLuminance = 0.0;
	float fFrameRate = 0.0;

    string lastErrorMessage;

    Imf::Header _imfHeader;

	void populateFrmImfHeader(Imf::Header imfHeader)
	{
		_imfHeader = imfHeader;

		Imath::Box2i dataWindow = imfHeader.dataWindow();
		iWidth = dataWindow.max.x - dataWindow.min.x + 1;
		iHeight = dataWindow.max.y - dataWindow.min.y + 1;
		iMinX = dataWindow.min.x;
		iMinY = dataWindow.min.y;
		iMaxY = dataWindow.max.y;

		_imfHeader.channels();
		const Imf::ChannelList imfChannels = _imfHeader.channels();
		bool gotR = false;
		bool gotG = false;
		bool gotB = false;
		bool gotA = false;
		for (ChannelList::ConstIterator i = imfChannels.begin(); i != imfChannels.end(); ++i)
		{
			gotR = gotR || (i.name()[0] == 'R' || i.name()[0] == 'r');
			gotG = gotG || (i.name()[0] == 'G' || i.name()[0] == 'g');
			gotB = gotB || (i.name()[0] == 'B' || i.name()[0] == 'b');
			gotA = gotA || (i.name()[0] == 'A' || i.name()[0] == 'a');

			_layerList.push_back(i.name());
		}

		_channels = ExrChannels::Other;
		if (gotR && gotG && gotB)
		{
			if (_layerList.size() == 3)
			{
				_channels = ExrChannels::RGB;
			}
			else if (gotA && _layerList.size() == 4)
			{
				_channels = ExrChannels::RGBA;
			}
		}

		// Need to expand this flag to mean "we can bypass the imf library".
		isUncompressed = imfHeader.compression() == NO_COMPRESSION;

		// get metadata timecode
		const TimeCodeAttribute *ta = _imfHeader.findTypedAttribute<TimeCodeAttribute>("timeCode");
		if (ta)
		{
			TimeCode tc = ta->value();
			char caTimecode[12];
			sprintf(caTimecode, "%02d:%02d:%02d:%02d", tc.hours(), tc.minutes(), tc.seconds(), tc.frame());
			strTimecode = caTimecode;
		}
		else
		{
			strTimecode = "";
		}

		const RationalAttribute *ra = _imfHeader.findTypedAttribute<RationalAttribute>("framesPerSecond");
		fFrameRate = ra ? float(ra->value()) : 0.0f;

		// is this a ACES file, or just an openEXR file
		bAcesImageContainer = _imfHeader.findTypedAttribute<IntAttribute>("acesImageContainer") ? true : false;

		// get shooting date & time
		const StringAttribute *sa = _imfHeader.findTypedAttribute<StringAttribute>("capDate");
		strDate = sa ? sa->value() : "";

		// find offset to start of line offset table, can be used to optimize reading of successive frames.
		const IntAttribute *ia = _imfHeader.findTypedAttribute<IntAttribute>("endOfheader");
		ulEndOfHeader = ia ? (unsigned long)ia->value() : 0;

		// find name of look mod transform function
		sa = _imfHeader.findTypedAttribute<StringAttribute>("lookModTransform");
		strLookModTransform = sa ? sa->value() : "";

		// find UUID string
		sa = _imfHeader.findTypedAttribute<StringAttribute>("uuid");
		strUUID = sa ? sa->value() : "";

		// get luminance of a pixel that has RGB values of 1.0,1.0,1.0
		const FloatAttribute *fa = _imfHeader.findTypedAttribute<FloatAttribute>("whiteLuminance");
		fWhiteLuminance = fa ? fa->value() : 0.0f;
	}
};

#endif