// EXRWrapperTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "EXRWrapper.h"
#include <string>
#include <Windows.h>
const size_t MaxFileSize = 128 * 1024 * 1024;

#define USE_MEMORY_MAPPING


int _tmain(int argc, _TCHAR* argv[])
{
    setlocale( LC_ALL, "" );
    if (argc < 2)
    {
        printf("Usage: %s filename.exr", argv[0]);
        return 0;
    }

    wstring wstrFilename = argv[1];
    string strFilename(wstrFilename.begin(), wstrFilename.end());
    int retVal;

#ifdef USE_MEMORY_MAPPING
    void *exrFile = EXR_OpenInputFile(strFilename.c_str(), &retVal);
    if (retVal != EXR_WRAP_RET_SUCCESS)
    {
        return retVal;
    }
#else
	HANDLE handle = ::CreateFile(wstrFilename.c_str(), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_FLAG_NO_BUFFERING, NULL);
	if (handle == INVALID_HANDLE_VALUE)
	{
		return -1;
	}

	DWORD numberOfBytesRead;
	char *buffer = (char *)_aligned_malloc(MaxFileSize, 4096);
	bool success = ::ReadFile(handle, buffer, MaxFileSize, &numberOfBytesRead, NULL);
	if (!success)
	{
		return -2;
	}

	::CloseHandle(handle);

	// Tell OpenEXR to use our memory-resident file.
	void *exrFile = EXR_OpenMemoryFile(buffer, MaxFileSize, &retVal);
	retVal = EXR_ReadPixels(exrFile, (unsigned char *)buffer);
	EXR_CloseFile(exrFile);

	if (retVal != 0)
	{
		return -3;
	}
#endif

    char dumpBuffer[10001];
    EXR_DumpHeader(exrFile, dumpBuffer, 1000);
    printf(dumpBuffer);
    int width;
    int height;
    EXR_Width(exrFile, &width);
    EXR_Height(exrFile, &height);

    unsigned char* pixelBuffer = new unsigned char[width * height * 4 * 2];


    EXR_ReadPixels(exrFile, pixelBuffer);

    EXR_CloseFile(exrFile);
	exrFile = NULL;

    strFilename[strFilename.size() - 5] = 'x';
    exrFile = EXR_OpenOutputFile(strFilename.c_str(), nullptr, &retVal);
    if (retVal != EXR_WRAP_RET_SUCCESS)
    {
        return retVal;
    }

	EXR_SetWidth(exrFile, width);
    EXR_SetHeight(exrFile, height);
    EXR_SetFrameRate(exrFile, 24);
    EXR_SetTimecode(exrFile, "11:00:00:00");

    EXR_WritePixels(exrFile, pixelBuffer);
    EXR_CloseFile(exrFile);

	return 0;
}

