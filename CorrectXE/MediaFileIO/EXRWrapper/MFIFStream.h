//
// MFIFStream.h
//

#ifndef MFIFSTREAM_H
#define MFIFSTREAM_H

//#include <ImfStdIO.h>
#include <ImfIO.h>
#include "Iex.h"

using namespace Imath;

class MFIFStream : public Imf::IStream
{
public:
    //-------------------------------------------------------
    // A constructor that opens the file with the given name.
    // It reads the whole file into an internal buffer and
    // then immediately closes the file.
    //-------------------------------------------------------

	MFIFStream::MFIFStream(char *dataPtr, Int64 dataLen);

    virtual ~MFIFStream ();

    virtual bool      isMemoryMapped () const { return true; }

    virtual bool	  read(char c[/*n*/], int n);
    virtual char*     readMemoryMapped(int n);
    virtual Int64	  tellg() { return _pos; }
    virtual void	  seekg(Int64 pos) { _pos = pos; }
    virtual void	  clear() {}

protected:
	MFIFStream(const char fileName[]);
    char*           _buffer;
    Int64           _length;
    Int64           _pos;
};


#endif
