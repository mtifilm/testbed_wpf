//
// MMIFStream.h
//

#ifndef MMIFSTREAM_H
#define MMIFSTREAM_H

#include "MFIFStream.h"

class MMIFStream: public MFIFStream
{
public:
    //------------------------------------------------------
    // The constructor maps the file into memory.
    //-------------------------------------------------------
    MMIFStream (const char fileName[]);

    //------------------------------------------------------
    // The destructor unmaps the file.
    //-------------------------------------------------------
    virtual ~MMIFStream ();

private:
    void *_hFile;
    void *_hFileMapping;
};


#endif
