//
//  EXRWrapper.cpp
//

#include "EXRWrapper.h"

#include "ExrFILE.h"
#include "IexBaseExc.h"
#include "MMIFStream.h"
#include "MMOFStream.h"
#include "MFOFStream.h"

#include "ImfNamespace.h"
#include <ImfMultiPartInputFile.h>
#include <ImfBoxAttribute.h>
#include <ImfChannelListAttribute.h>
#include <ImfChromaticitiesAttribute.h>
#include <ImfCompressionAttribute.h>
#include <ImfDoubleAttribute.h>
#include <ImfEnvmapAttribute.h>
#include <ImfFloatAttribute.h>
#include <ImfIntAttribute.h>
#include <ImfKeyCodeAttribute.h>
#include <ImfLineOrderAttribute.h>
#include <ImfMatrixAttribute.h>
#include <ImfPreviewImageAttribute.h>
#include <ImfRationalAttribute.h>
#include <ImfStringAttribute.h>
#include <ImfStringVectorAttribute.h>
#include <ImfTileDescriptionAttribute.h>
#include <ImfTimeCodeAttribute.h>
#include <ImfVecAttribute.h>
#include <ImfVersion.h>
#include <ImfHeader.h>

#include <iostream>
#include <iomanip>


using namespace OPENEXR_IMF_NAMESPACE;

#include <sstream>

using namespace Imf;

#define TRIES 8
#define NumberOfDecodeThreads 8

static string noFileLastErrorMessage;

#ifdef EXR_WRAPPER_LOG
#include <chrono>
const string logFileName = "C:\\temp\\___EXR_WRAPPER_OUT.log";
bool logIsOpen = false;
#include <fstream> 
std::fstream fs;
#endif


namespace
{
	//--------------------------------------------------------
	ExrFILE *readAndParseExrHeader(InputFile *exrInputFile)
	{
		try
		{
			exrInputFile->header().sanityCheck();
		}
		catch (...)
		{
			return NULL;
		}

		ExrFILE *exrFile = new ExrFILE;
		exrFile->strFileName = exrInputFile->fileName();

		exrFile->populateFrmImfHeader(exrInputFile->header());

		return exrFile;
	}
}

//--------------------------------------------------------
int EXR_SetGlobalThreadCount(int threadCount)
{
   setGlobalThreadCount(threadCount);
   return EXR_WRAP_RET_SUCCESS;
}

//--------------------------------------------------------
void *EXR_OpenInputFile(const char *cpFileName, int *ipRetVal)
{
#ifdef EXR_WRAPPER_LOG
	if (!logIsOpen)
	{
		logIsOpen = true;
		fs.open(logFileName.c_str(), std::fstream::out);
	}
#endif

	if (cpFileName == NULL)
	{
		noFileLastErrorMessage = "EXR INTERNAL ERROR 1";
		return NULL;
	}

#ifdef EXR_WRAPPER_LOG
	auto start = std::chrono::system_clock::now();
#endif

	InputFile *exrInputFile = NULL;
	try
	{
		exrInputFile = new InputFile(cpFileName, NumberOfDecodeThreads);
	}
	catch (Iex::BaseExc ex)
	{
		std::ostringstream os;
		os << "EXR: Open FAIL: " << ex.c_str();
		noFileLastErrorMessage = os.str();
		*ipRetVal = EXR_WRAP_ERR_OPEN_FAIL;
		return NULL;
	}

#ifdef EXR_WRAPPER_LOG
	auto end1 = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end1 - start;
	fs << "new InputFile took " << (elapsed_seconds.count() * 1000) << " msecs\n";
	fs.flush();
#endif

	ExrFILE *exrFile = NULL;
	try
	{
		exrFile = readAndParseExrHeader(exrInputFile);
	}
	catch (Iex::BaseExc ex)
	{
		std::ostringstream os;
		os << "EXR: cannot get attributes for " << exrFile->strFileName << " because " << ex.c_str();
		noFileLastErrorMessage = os.str();
		*ipRetVal = EXR_WRAP_ERR_CORRUPT_FILE;
		delete exrFile;
		delete exrInputFile;
		return NULL;
	}

#ifdef EXR_WRAPPER_LOG
	auto end2 = std::chrono::system_clock::now();
	elapsed_seconds = end2 - end1;
	fs << "readAndParseExrHeader took " << (elapsed_seconds.count() * 1000) << " msecs\n";
	fs.flush();
#endif


	delete exrInputFile;
	*ipRetVal = EXR_WRAP_RET_SUCCESS;
	noFileLastErrorMessage = "";

#ifdef EXR_WRAPPER_LOG
	auto end3 = std::chrono::system_clock::now();
	elapsed_seconds = end3 - start;
	fs << "EXR_OpenInputFile took " << (elapsed_seconds.count() * 1000) << " msecs\n";
	fs.flush();
#endif

	return exrFile;
}

//--------------------------------------------------------
int EXR_GetOpaqueHeader(void *vpExrFile, void *&opaqueHeader)
{
	ExrFILE *exrFile = (ExrFILE*)vpExrFile;
	if (exrFile == NULL)
	{
		return EXR_WRAP_ERR_NOT_ALLOC;
	}

	opaqueHeader = new Imf::Header(exrFile->_imfHeader);

	return EXR_WRAP_RET_SUCCESS;
}

//--------------------------------------------------------
void EXR_ReleaseOpaqueHeader(void *opaqueHeader)
{
	if (opaqueHeader == nullptr)
	{
		return;
	}
		
	Imf::Header *imfHeaderPtr = static_cast<Imf::Header *>(opaqueHeader);
	delete imfHeaderPtr;
}

//--------------------------------------------------------
void *EXR_OpenInputMemoryFile(char *dataPtr, unsigned int dataLen, int *ipRetVal)
{
#ifdef EXR_WRAPPER_LOG
	if (!logIsOpen)
	{
		logIsOpen = true;
		fs.open(logFileName.c_str(), std::fstream::out);
	}

	auto start = std::chrono::system_clock::now();
#endif

	InputFile *exrInputFile = NULL;
	MFIFStream mfStream(dataPtr, dataLen);
	try
	{
		exrInputFile = new InputFile(mfStream, NumberOfDecodeThreads);
	}
	catch (Iex::BaseExc ex)
	{
		std::ostringstream os;
		os << "EXR: Open FAIL: " << ex.c_str();
		noFileLastErrorMessage = os.str();
		*ipRetVal = EXR_WRAP_ERR_OPEN_FAIL;
		return NULL;
	}

#ifdef EXR_WRAPPER_LOG
	auto end1 = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end1 - start;
	fs << "new InputFile took " << (elapsed_seconds.count() * 1000) << " msecs\n";
	fs.flush();
#endif

	ExrFILE *exrFile = NULL;

	try
	{
		exrFile = readAndParseExrHeader(exrInputFile);
	}
	catch (Iex::BaseExc ex)
	{
		std::ostringstream os;
		os << "EXR: cannot get attributes for memory file because " << ex.c_str();
		noFileLastErrorMessage = os.str();
		*ipRetVal = EXR_WRAP_ERR_CORRUPT_FILE;
		delete exrFile;
		delete exrInputFile;
		return NULL;
	}

#ifdef EXR_WRAPPER_LOG
	auto end2 = std::chrono::system_clock::now();
	elapsed_seconds = end2 - end1;
	fs << "readAndParseExrHeader took " << (elapsed_seconds.count() * 1000) << " msecs\n";
	fs.flush();
#endif

	exrFile->memoryFileBuffer = dataPtr;
	exrFile->memoryFileBufferLength = dataLen;
	delete exrInputFile;
	*ipRetVal = EXR_WRAP_RET_SUCCESS;
	noFileLastErrorMessage = "";

#ifdef EXR_WRAPPER_LOG
	auto end3 = std::chrono::system_clock::now();
	elapsed_seconds = end3 - start;
	fs << "EXR_OpenInputMemoryFile took " << (elapsed_seconds.count() * 1000) << " msecs\n";
	fs.flush();
#endif

	return exrFile;
}

//--------------------------------------------------------
int EXR_Width(void *vpExrFile, int *ipWidth)
{
    ExrFILE *exrFile = (ExrFILE*)vpExrFile;
    if (exrFile == NULL)
    {
        return EXR_WRAP_ERR_NOT_ALLOC;
    }

    if (ipWidth == NULL)
    {
        return EXR_WRAP_ERR_NULL_PARAMETER;
    }

    *ipWidth = exrFile->iWidth;

    return EXR_WRAP_RET_SUCCESS;
}

//--------------------------------------------------------
int EXR_Height(void *vpExrFile, int *ipHeight)
{
    ExrFILE *exrFile = (ExrFILE*)vpExrFile;
    if (exrFile == NULL)
    {
        return EXR_WRAP_ERR_NOT_ALLOC;
    }

    if (ipHeight == NULL)
    {
        return EXR_WRAP_ERR_NULL_PARAMETER;
    }

    *ipHeight = exrFile->iHeight;

    return EXR_WRAP_RET_SUCCESS;
}
    
//----------------------------------------------------------
// Get the clip frame rate
int EXR_FrameRate(void *vpExrFile, float *fpFrameRate)
{
    ExrFILE *exrFile = (ExrFILE*)vpExrFile;
    if (exrFile == NULL)
    {
        return EXR_WRAP_ERR_NOT_ALLOC;
    }

    if (fpFrameRate == NULL)
    {
        return EXR_WRAP_ERR_NULL_PARAMETER;
    }

    *fpFrameRate = exrFile->fFrameRate;

    return EXR_WRAP_RET_SUCCESS;
}

//----------------------------------------------------------
// Get starting timecode
int EXR_Timecode(void *vpExrFile, char *cpTimecode, int iBufferLength)
{
    ExrFILE *exrFile = (ExrFILE*)vpExrFile;
    if (exrFile == NULL)
    {
        return EXR_WRAP_ERR_NOT_ALLOC;
    }

    if (cpTimecode == NULL)
    {
        return EXR_WRAP_ERR_NULL_PARAMETER;
    }
    
    if (iBufferLength < (int)(exrFile->strTimecode.length() + 1))
    {
        return EXR_WRAP_ERR_BUFFER_TOO_SMALL;
    }

    cpTimecode[0] = 0;
    strncat(cpTimecode, exrFile->strTimecode.c_str(), iBufferLength - 1);

    return EXR_WRAP_RET_SUCCESS;
}

//----------------------------------------------------------
// Get shooting date, in the form YYYY:MM:DD hh:mm:ss
int EXR_Date(void *vpExrFile, char *cpDate, int iBufferLength)
{
    ExrFILE *exrFile = (ExrFILE*)vpExrFile;
    if (exrFile == NULL)
    {
        return EXR_WRAP_ERR_NOT_ALLOC;
    }

    if (cpDate == NULL)
    {
        return EXR_WRAP_ERR_NULL_PARAMETER;
    }
    
    if (iBufferLength < (int)(exrFile->strDate.length() + 1))
    {
        return EXR_WRAP_ERR_BUFFER_TOO_SMALL;
    }

    cpDate[0] = 0;
    strncat(cpDate, exrFile->strDate.c_str(), iBufferLength - 1);

    return EXR_WRAP_RET_SUCCESS;
}

//----------------------------------------------------------
// Get Look Mod Transform function name
int EXR_LookModTransform(void *vpExrFile, char *cpFunctionName, int iBufferLength)
{
    ExrFILE *exrFile = (ExrFILE*)vpExrFile;
    if (exrFile == NULL)
    {
        return EXR_WRAP_ERR_NOT_ALLOC;
    }

    if (cpFunctionName == NULL)
    {
        return EXR_WRAP_ERR_NULL_PARAMETER;
    }
    
    if (iBufferLength < (int)(exrFile->strLookModTransform.length() + 1))
    {
        return EXR_WRAP_ERR_BUFFER_TOO_SMALL;
    }

    cpFunctionName[0] = 0;
    strncat(cpFunctionName, exrFile->strLookModTransform.c_str(), iBufferLength - 1);

    return EXR_WRAP_RET_SUCCESS;
}

	//----------------------------------------------------------
// Get UUID name
int EXR_UUID(void *vpExrFile, char *cpUUID, int iBufferLength)
{
ExrFILE *exrFile = (ExrFILE*)vpExrFile;
    if (exrFile == NULL)
    {
        return EXR_WRAP_ERR_NOT_ALLOC;
    }

    if (cpUUID == NULL)
    {
        return EXR_WRAP_ERR_NULL_PARAMETER;
    }
    
    if (iBufferLength < (int)(exrFile->strUUID.length() + 1))
    {
        return EXR_WRAP_ERR_BUFFER_TOO_SMALL;
    }

    cpUUID[0] = 0;
    strncat(cpUUID, exrFile->strUUID.c_str(), iBufferLength - 1);

    return EXR_WRAP_RET_SUCCESS;
}

//----------------------------------------------------------
// Is uncompressed?
int EXR_IsUncompressed(void *vpExrFile, bool *isUncompressedOutPtr)
{
	ExrFILE *exrFile = (ExrFILE*)vpExrFile;
	if (exrFile == NULL)
	{
		*isUncompressedOutPtr = false;
		return EXR_WRAP_ERR_NOT_ALLOC;
	}

	if (isUncompressedOutPtr == NULL)
	{
		return EXR_WRAP_ERR_NULL_PARAMETER;
	}

	*isUncompressedOutPtr = exrFile->isUncompressed;

	return EXR_WRAP_RET_SUCCESS;
}

//----------------------------------------------------------
// Is RGB or RGBA data?
int EXR_IsRGB(void *vpExrFile, bool *isRGBOutPtr)
{
	ExrFILE *exrFile = (ExrFILE*)vpExrFile;
	if (exrFile == NULL)
	{
		*isRGBOutPtr = false;
		return EXR_WRAP_ERR_NOT_ALLOC;
	}

	if (isRGBOutPtr == NULL)
	{
		return EXR_WRAP_ERR_NULL_PARAMETER;
	}

	*isRGBOutPtr = exrFile->_channels == RGB || exrFile->_channels == RGBA;

	return EXR_WRAP_RET_SUCCESS;
}

//----------------------------------------------------------
// Number of _channels
int EXR_NumChannels(void *vpExrFile, int *numChannelsOutPtr)
{
	ExrFILE *exrFile = (ExrFILE*)vpExrFile;
	if (exrFile == NULL)
	{
		*numChannelsOutPtr = 0;
		return EXR_WRAP_ERR_NOT_ALLOC;
	}

	if (numChannelsOutPtr == NULL)
	{
		return EXR_WRAP_ERR_NULL_PARAMETER;
	}

	*numChannelsOutPtr = (exrFile->_channels == RGB) ? 3 : ((exrFile->_channels == RGBA) ? 4 : 0);

	return EXR_WRAP_RET_SUCCESS;
}

//--------------------------------------------------------
int EXR_ReadPixels(void *vpExrFile, unsigned char *ucpBuffer)
{
    ExrFILE *exrFile = (ExrFILE*)vpExrFile;
    if (exrFile == NULL)
    {
        return EXR_WRAP_ERR_NOT_ALLOC;
    }

	// At this time we only handle files with just RGB and RGBA layers
	// (with no extras)
	bool isRGBorRGBA = false;
	if (EXR_IsRGB(exrFile, &isRGBorRGBA) != 0 || !isRGBorRGBA)
	{
		exrFile->lastErrorMessage = "EXR file format not supported - RGB or RGBA only";
		return EXR_WRAP_ERR_FORMAT_NOT_SUPPORTED;
	}

	try
    {
		FrameBuffer frameBuffer;
		int numLayers = int(exrFile->_layerList.size());
		size_t xStride = numLayers * sizeof(half);
		size_t yStride = xStride * exrFile->iWidth;
		for (int i = 0; i < numLayers; i++)
		{
			// Interleaved (A)BGR
			char *base = (char*)ucpBuffer + (i * sizeof(half));
			frameBuffer.insert(exrFile->_layerList[i], Slice(Imf::HALF, base, xStride, yStride));
		}

		if (exrFile->memoryFileBuffer != nullptr && exrFile->memoryFileBufferLength > 0)
		{
			MFIFStream ifs(exrFile->memoryFileBuffer, exrFile->memoryFileBufferLength);
			InputFile inputFile(ifs, NumberOfDecodeThreads);
			inputFile.setFrameBuffer(frameBuffer);
			inputFile.readPixels(exrFile->iMinY, exrFile->iMaxY);
		}
		else if (!exrFile->strFileName.empty())
		{
			MMIFStream ifs(exrFile->strFileName.c_str());
			InputFile inputFile(ifs, NumberOfDecodeThreads);
			inputFile.setFrameBuffer(frameBuffer);
			inputFile.readPixels(exrFile->iMinY, exrFile->iMaxY);
		}
		else
		{
			return EXR_WRAP_ERR_NULL_PARAMETER;
		}
    }
    catch(Iex::BaseExc ex)
    {
        std::ostringstream os;
        os << "EXR: cannot read from " << exrFile->strFileName << " because " << ex.c_str();
        exrFile->lastErrorMessage = os.str();
        return EXR_WRAP_ERR_READ_FAIL;
    }

    return EXR_WRAP_RET_SUCCESS;
}

//--------------------------------------------------------
int EXR_CloseFile(void *vpExrFile)
{
    ExrFILE *exrFile = (ExrFILE*)vpExrFile;
    if (exrFile == NULL)
    {
        return EXR_WRAP_ERR_NOT_ALLOC;
    }

    delete exrFile;

    return EXR_WRAP_RET_SUCCESS;
}

//--------------------------------------------------------
void *EXR_OpenOutputFile(const char *cpFileName, void *opaqueHeader, int *ipRetVal)
{
    ExrFILE *exrFile = new ExrFILE;
    if (exrFile == NULL)
    {
        *ipRetVal =  EXR_WRAP_ERR_MALLOC;
        return NULL;
    }

    exrFile->strFileName = cpFileName;

	Imf::Header *imfHeaderPtr = NULL;
	if (opaqueHeader != nullptr)
	{
		imfHeaderPtr = static_cast<Imf::Header *>(opaqueHeader);

		if (imfHeaderPtr)
		{
			exrFile->populateFrmImfHeader(*imfHeaderPtr);
		}
	}

	if (!imfHeaderPtr)
	{
		// Use lossless compression, supported by IIF/ACES
		exrFile->_imfHeader.compression() = PIZ_COMPRESSION;  // QQQ
	}

    *ipRetVal = EXR_WRAP_RET_SUCCESS;
    return (void*)exrFile;
}

//--------------------------------------------------------
void *EXR_OpenOutputMemoryFile(unsigned char *fileBuffer, unsigned int fileBufferSize, void *opaqueHeader, unsigned int *fileEofOut, int *ipRetVal)
{
	ExrFILE *exrFile = new ExrFILE;
	if (exrFile == NULL)
	{
		*ipRetVal = EXR_WRAP_ERR_MALLOC;
		return NULL;
	}

	exrFile->strFileName = "MEMORY FILE";
	Imf::Header *imfHeaderPtr = NULL;
	if (opaqueHeader != nullptr)
	{
		imfHeaderPtr = static_cast<Imf::Header *>(opaqueHeader);

		if (imfHeaderPtr)
		{
			exrFile->populateFrmImfHeader(*imfHeaderPtr);
		}
	}

	exrFile->memoryFileBuffer = (char *) fileBuffer;
	exrFile->memoryFileBufferLength = fileBufferSize;
	exrFile->externalFileSizePtr = (unsigned long *) fileEofOut;

	*ipRetVal = EXR_WRAP_RET_SUCCESS;

	return (void*)exrFile;
}

//--------------------------------------------------------
int EXR_SetWidth(void *vpExrFile, int iWidth)
{
    ExrFILE *exrFile = (ExrFILE*)vpExrFile;
    if (exrFile == NULL)
    {
        return EXR_WRAP_ERR_NOT_ALLOC;
    }

    exrFile->iWidth = iWidth;
    exrFile->_imfHeader.dataWindow().min.x = 0;
    exrFile->_imfHeader.dataWindow().max.x = iWidth - 1;
    exrFile->_imfHeader.displayWindow().min.x = 0;
    exrFile->_imfHeader.displayWindow().max.x = iWidth - 1;

    return EXR_WRAP_RET_SUCCESS;
}

//--------------------------------------------------------
int EXR_SetHeight(void *vpExrFile, int iHeight)
{
    ExrFILE *exrFile = (ExrFILE*)vpExrFile;
    if (exrFile == NULL)
    {
        return EXR_WRAP_ERR_NOT_ALLOC;
    }

    exrFile->iHeight = iHeight;
    exrFile->_imfHeader.dataWindow().min.y = 0;
    exrFile->_imfHeader.dataWindow().max.y = iHeight - 1;
    exrFile->_imfHeader.displayWindow().min.y = 0;
    exrFile->_imfHeader.displayWindow().max.y = iHeight - 1;

    return EXR_WRAP_RET_SUCCESS;
}

//--------------------------------------------------------
int EXR_SetTimecode(void *vpExrFile, const char *cpTimecode)
{
    ExrFILE *exrFile = (ExrFILE*)vpExrFile;
    if (exrFile == NULL)
    {
        return EXR_WRAP_ERR_NOT_ALLOC;
    }

    if (cpTimecode == NULL)
    {
        return EXR_WRAP_ERR_BAD_TIMECODE;
    }

    if (strlen(cpTimecode) != 11)
    {
        return EXR_WRAP_ERR_BAD_TIMECODE;
    }

    exrFile->strTimecode = string(cpTimecode);

    int h,m,s,f;

    h = atoi(exrFile->strTimecode.substr(0,2).c_str());
    m = atoi(exrFile->strTimecode.substr(3,2).c_str());
    s = atoi(exrFile->strTimecode.substr(6,2).c_str());
    f = atoi(exrFile->strTimecode.substr(9,2).c_str());

    TimeCode tc(h, m, s, f);
    TimeCodeAttribute ta(tc);
    exrFile->_imfHeader.insert("timeCode", ta);

    return EXR_WRAP_RET_SUCCESS;
}

//--------------------------------------------------------
int EXR_SetFrameRate(void *vpExrFile, float frameRate)
{
    ExrFILE *exrFile = (ExrFILE*)vpExrFile;
    if (exrFile == NULL)
    {
        return EXR_WRAP_ERR_NOT_ALLOC;
    }

    exrFile->fFrameRate = frameRate;

    Rational rfr((int)frameRate * 1000, 1000);
    exrFile->_imfHeader.insert("framesPerSecond", RationalAttribute(rfr));

    return EXR_WRAP_RET_SUCCESS;
}

//--------------------------------------------------------
int EXR_WritePixels(void *vpExrFile, const unsigned char *ucpBuffer)
{
    ExrFILE *exrFile = (ExrFILE*)vpExrFile;
    if (exrFile == NULL)
    {
        return EXR_WRAP_ERR_NOT_ALLOC;
    }

    try
    {
		FrameBuffer frameBuffer;
		int numLayers = int(exrFile->_layerList.size());
		size_t xStride = numLayers * sizeof(half);
		size_t yStride = xStride * exrFile->iWidth;
		for (int i = 0; i < numLayers; i++)
		{
			// Interleaved (A)BGR
			char *base = (char*)ucpBuffer + (i * sizeof(half));
			frameBuffer.insert(exrFile->_layerList[i], Slice(Imf::HALF, base, xStride, yStride));
		}

		if (exrFile->memoryFileBuffer != nullptr && exrFile->memoryFileBufferLength > 0)
		{
			Int64 eof = 0;
			MFOFStream ofs(exrFile->memoryFileBuffer, exrFile->memoryFileBufferLength, &eof);
			OutputFile outputFile(ofs, exrFile->_imfHeader);
			outputFile.setFrameBuffer(frameBuffer);
			outputFile.writePixels(exrFile->iHeight);
			if (exrFile->externalFileSizePtr != nullptr)
			{
				*(exrFile->externalFileSizePtr) = (unsigned int)eof;
			}
		}
		else if (!exrFile->strFileName.empty())
		{
			MMOFStream ofs(exrFile->strFileName.c_str());
			OutputFile outputFile(ofs, exrFile->_imfHeader);
			outputFile.setFrameBuffer(frameBuffer);
			outputFile.writePixels(exrFile->iHeight);
		}
		else
		{
			return EXR_WRAP_ERR_NULL_PARAMETER;
		}
	}
    catch(Iex::BaseExc ex)
    {
        std::ostringstream os;
        os << "EXR: cannot write to " << exrFile->strFileName << " because " << ex.c_str();
        exrFile->lastErrorMessage = os.str();
        return EXR_WRAP_ERR_WRITE_FAIL;
    }

    return EXR_WRAP_RET_SUCCESS;
}

namespace {

	///////////////////////////////////////////////////////////////////////////
	//
	// Copyright (c) 2012, Industrial Light & Magic, a division of Lucas
	// Digital Ltd. LLC
	// 
	// All rights reserved.
	// 
	// Redistribution and use in source and binary forms, with or without
	// modification, are permitted provided that the following conditions are
	// met:
	// *       Redistributions of source code must retain the above copyright
	// notice, this list of conditions and the following disclaimer.
	// *       Redistributions in binary form must reproduce the above
	// copyright notice, this list of conditions and the following disclaimer
	// in the documentation and/or other materials provided with the
	// distribution.
	// *       Neither the name of Industrial Light & Magic nor the names of
	// its contributors may be used to endorse or promote products derived
	// from this software without specific prior written permission. 
	// 
	// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
	// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
	// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
	// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
	// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
	// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
	// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	//
	///////////////////////////////////////////////////////////////////////////


	//-----------------------------------------------------------------------------
	//
	//	Utility program to print an image file's header
	//
	//-----------------------------------------------------------------------------

	//#include "ImfNamespace.h"
	//#include <ImfMultiPartInputFile.h>
	//#include <ImfBoxAttribute.h>
	//#include <ImfChannelListAttribute.h>
	//#include <ImfChromaticitiesAttribute.h>
	//#include <ImfCompressionAttribute.h>
	//#include <ImfDoubleAttribute.h>
	//#include <ImfEnvmapAttribute.h>
	//#include <ImfFloatAttribute.h>
	//#include <ImfIntAttribute.h>
	//#include <ImfKeyCodeAttribute.h>
	//#include <ImfLineOrderAttribute.h>
	//#include <ImfMatrixAttribute.h>
	//#include <ImfPreviewImageAttribute.h>
	//#include <ImfRationalAttribute.h>
	//#include <ImfStringAttribute.h>
	//#include <ImfStringVectorAttribute.h>
	//#include <ImfTileDescriptionAttribute.h>
	//#include <ImfTimeCodeAttribute.h>
	//#include <ImfVecAttribute.h>
	//#include <ImfVersion.h>
	//#include <ImfHeader.h>
	//
	//#include <iostream>
	//#include <iomanip>
	//
	//
	//	using namespace OPENEXR_IMF_NAMESPACE;


	string compressionToString(Compression ct)
	{
		switch (ct)
		{
		case NO_COMPRESSION:
			return "none";
		case RLE_COMPRESSION:
			return "run-length encoding";
		case ZIPS_COMPRESSION:
			return "zip, individual scanlines";
		case ZIP_COMPRESSION:
			return "zip, multi-scanline blocks";
		case PIZ_COMPRESSION:
			return "piz";
		case PXR24_COMPRESSION:
			return "pxr24";
		case B44_COMPRESSION:
			return "b44";
		case B44A_COMPRESSION:
			return "b44a";
		case DWAA_COMPRESSION:
			return "dwa, small scanline blocks";
		case DWAB_COMPRESSION:
			return "dwa, medium scanline blocks";
		default:
			break;
		}

		std::ostringstream os;
		os << "unknown compression type " << int(ct);
		return os.str();
	}


	string lineOrderToString(LineOrder lo)
	{
		switch (lo)
		{
		case INCREASING_Y:
			return "increasing y";
		case DECREASING_Y:
			return "decreasing y";
		case RANDOM_Y:
			return "random y";
		default:
			break;
		}

		std::ostringstream os;
		os << "unknown line order " << int(lo);
		return os.str();
	}


	string pixelTypeToString(PixelType pt)
	{
		switch (pt)
		{
		case UINT:
			return "32-bit unsigned integer";
		case HALF:
			return "16-bit floating-point";
		case FLOAT:
			return "32-bit floating-point";
		default:
			break;
		}

		std::ostringstream os;
		os << "unknown type " << int(pt);
		return os.str();
	}


	string levelModeToString(LevelMode lm)
	{
		switch (lm)
		{
		case ONE_LEVEL:
			return "single level";
		case MIPMAP_LEVELS:
			return "mip-map";
		case RIPMAP_LEVELS:
			return "rip-map";
		default:
			break;
		}

		std::ostringstream os;
		os << "unknown mode " << int(lm);
		return os.str();
	}


	string roundingModeToString(LevelRoundingMode rm)
	{
		switch (rm)
		{
		case ROUND_DOWN:
			return "down";
		case ROUND_UP:
			return "up";
		default:
			break;
		}

		std::ostringstream os;
		os << "unknown mode " << int(rm);
		return os.str();
	}


	string timeCodeToString(TimeCode tc)
	{
		std::ostringstream os;
		os << "    "
			"time " <<
			std::setfill('0') <<
#ifndef HAVE_COMPLETE_IOMANIP
			std::setw(2) << tc.hours() << ":" <<
			std::setw(2) << tc.minutes() << ":" <<
			std::setw(2) << tc.seconds() << ":" <<
			std::setw(2) << tc.frame() << "\n" <<
#else
			std::setw(2) << std::right << tc.hours() << ":" <<
			std::setw(2) << std::right << tc.minutes() << ":" <<
			std::setw(2) << std::right << tc.seconds() << ":" <<
			std::setw(2) << std::right << tc.frame() << "\n" <<
#endif
			std::setfill(' ') <<
			"    "
			"drop frame " << tc.dropFrame() << ", "
			"color frame " << tc.colorFrame() << ", "
			"field/phase " << tc.fieldPhase() << "\n"
			"    "
			"bgf0 " << tc.bgf0() << ", "
			"bgf1 " << tc.bgf1() << ", "
			"bgf2 " << tc.bgf2() << "\n"
			"    "
			"user data 0x" << std::hex << tc.userData() << std::dec;

		return os.str();
	}


	string envmapToString(Envmap em)
	{
		switch (em)
		{
		case ENVMAP_LATLONG:
			return "latitude-longitude map";
			break;

		case ENVMAP_CUBE:
			return "cube-face map";
			break;

		default:
			break;
		}

		std::ostringstream os;
		os << "unknown map type " << int(em);
		return os.str();
	}


	string channelListToString(const ChannelList &cl)
	{
		std::ostringstream os;
		for (ChannelList::ConstIterator i = cl.begin(); i != cl.end(); ++i)
		{
			os << "\n    " << i.name() << ", " << pixelTypeToString(i.channel().type);

			os << ", sampling " <<
				i.channel().xSampling << " " <<
				i.channel().ySampling;

			if (i.channel().pLinear)
				os << ", plinear";
		}

		return os.str();
	}


	string oneHeaderToString(const Header &h)
	{
		std::ostringstream os;

		for (Header::ConstIterator i = h.begin(); i != h.end(); ++i)
		{
			const Attribute *a = &i.attribute();
			os << i.name() << " (type " << a->typeName() << ")";

			if (const Box2iAttribute *ta =
				dynamic_cast <const Box2iAttribute *> (a))
			{
				os << ": " << ta->value().min << " - " << ta->value().max;
			}

			else if (const Box2fAttribute *ta =
				dynamic_cast <const Box2fAttribute *> (a))
			{
				os << ": " << ta->value().min << " - " << ta->value().max;
			}
			else if (const ChannelListAttribute *ta =
				dynamic_cast <const ChannelListAttribute *> (a))
			{
				os << ":" << channelListToString(ta->value());
			}
			else if (const ChromaticitiesAttribute *ta =
				dynamic_cast <const ChromaticitiesAttribute *> (a))
			{
				os << ":\n"
					"    red   " << ta->value().red << "\n"
					"    green " << ta->value().green << "\n"
					"    blue  " << ta->value().blue << "\n"
					"    white " << ta->value().white;
			}
			else if (const CompressionAttribute *ta =
				dynamic_cast <const CompressionAttribute *> (a))
			{
				os << ": " << compressionToString(ta->value());
			}
			else if (const DoubleAttribute *ta =
				dynamic_cast <const DoubleAttribute *> (a))
			{
				os << ": " << ta->value();
			}
			else if (const EnvmapAttribute *ta =
				dynamic_cast <const EnvmapAttribute *> (a))
			{
				os << ": " << envmapToString(ta->value());
			}
			else if (const FloatAttribute *ta =
				dynamic_cast <const FloatAttribute *> (a))
			{
				os << ": " << ta->value();
			}
			else if (const IntAttribute *ta =
				dynamic_cast <const IntAttribute *> (a))
			{
				os << ": " << ta->value();
			}
			else if (const KeyCodeAttribute *ta =
				dynamic_cast <const KeyCodeAttribute *> (a))
			{
				os << ":\n"
					"    film manufacturer code " <<
					ta->value().filmMfcCode() << "\n"
					"    film type code " <<
					ta->value().filmType() << "\n"
					"    prefix " <<
					ta->value().prefix() << "\n"
					"    count " <<
					ta->value().count() << "\n"
					"    perf offset " <<
					ta->value().perfOffset() << "\n"
					"    perfs per frame " <<
					ta->value().perfsPerFrame() << "\n"
					"    perfs per count " <<
					ta->value().perfsPerCount();
			}
			else if (const LineOrderAttribute *ta =
				dynamic_cast <const LineOrderAttribute *> (a))
			{
				os << ": " << lineOrderToString(ta->value());
			}
			else if (const M33fAttribute *ta =
				dynamic_cast <const M33fAttribute *> (a))
			{
				os << ":\n"
					"   (" <<
					ta->value()[0][0] << " " <<
					ta->value()[0][1] << " " <<
					ta->value()[0][2] << "\n    " <<
					ta->value()[1][0] << " " <<
					ta->value()[1][1] << " " <<
					ta->value()[1][2] << "\n    " <<
					ta->value()[2][0] << " " <<
					ta->value()[2][1] << " " <<
					ta->value()[2][2] << ")";
			}
			else if (const M44fAttribute *ta =
				dynamic_cast <const M44fAttribute *> (a))
			{
				os << ":\n"
					"   (" <<
					ta->value()[0][0] << " " <<
					ta->value()[0][1] << " " <<
					ta->value()[0][2] << " " <<
					ta->value()[0][3] << "\n    " <<
					ta->value()[1][0] << " " <<
					ta->value()[1][1] << " " <<
					ta->value()[1][2] << " " <<
					ta->value()[1][3] << "\n    " <<
					ta->value()[2][0] << " " <<
					ta->value()[2][1] << " " <<
					ta->value()[2][2] << " " <<
					ta->value()[2][3] << "\n    " <<
					ta->value()[3][0] << " " <<
					ta->value()[3][1] << " " <<
					ta->value()[3][2] << " " <<
					ta->value()[3][3] << ")";
			}
			else if (const PreviewImageAttribute *ta =
				dynamic_cast <const PreviewImageAttribute *> (a))
			{
				os << ": " <<
					ta->value().width() << " by " <<
					ta->value().height() << " pixels";
			}
			else if (const StringAttribute *ta =
				dynamic_cast <const StringAttribute *> (a))
			{
				os << ": \"" << ta->value() << "\"";
			}
			else if (const StringVectorAttribute * ta =
				dynamic_cast<const StringVectorAttribute *>(a))
			{
				os << ":";

				for (StringVector::const_iterator i = ta->value().begin();
					i != ta->value().end();
					++i)
				{
					os << "\n    \"" << *i << "\"";
				}
			}
			else if (const RationalAttribute *ta =
				dynamic_cast <const RationalAttribute *> (a))
			{
				os << ": " << ta->value().n << "/" << ta->value().d <<
					" (" << double(ta->value()) << ")";
			}
			else if (const TileDescriptionAttribute *ta =
				dynamic_cast <const TileDescriptionAttribute *> (a))
			{
				os << ":\n    " << levelModeToString(ta->value().mode);

				os << "\n    tile size " <<
					ta->value().xSize << " by " <<
					ta->value().ySize << " pixels";

				if (ta->value().mode != ONE_LEVEL)
				{
					os << "\n    level sizes rounded " << roundingModeToString(ta->value().roundingMode);
				}
			}
			else if (const TimeCodeAttribute *ta =
				dynamic_cast <const TimeCodeAttribute *> (a))
			{
				os << ":\n" << timeCodeToString(ta->value());
			}
			else if (const V2iAttribute *ta =
				dynamic_cast <const V2iAttribute *> (a))
			{
				os << ": " << ta->value();
			}
			else if (const V2fAttribute *ta =
				dynamic_cast <const V2fAttribute *> (a))
			{
				os << ": " << ta->value();
			}
			else if (const V3iAttribute *ta =
				dynamic_cast <const V3iAttribute *> (a))
			{
				os << ": " << ta->value();
			}
			else if (const V3fAttribute *ta =
				dynamic_cast <const V3fAttribute *> (a))
			{
				os << ": " << ta->value();
			}

			os << '\n';
		}

		return os.str();
	}

	string fileHeaderToString(const char fileName[])
	{
		MultiPartInputFile in(fileName);
		int parts = in.parts();
		std::ostringstream os;

		//
		// Check to see if any parts are incomplete
		//

		bool fileComplete = true;

		for (int i = 0; i < parts && fileComplete; ++i)
			if (!in.partComplete(i))
				fileComplete = false;

		//
		// Print file name and file format version
		//

		os << "\nfile " << fileName <<
			(fileComplete ? "" : " (incomplete)") <<
			":\n\n";

		os << "file format version: " <<
			getVersion(in.version()) << ", "
			"flags 0x" <<
			std::setbase(16) << getFlags(in.version()) << std::setbase(10) << "\n";

		//
		// Print the header of every part in the file
		//

		for (int p = 0; p < parts; ++p)
		{
			const Header & h = in.header(p);

			if (parts != 1)
			{
				os << "\n\n part " << p <<
					(in.partComplete(p) ? "" : " (incomplete)") <<
					":\n";

			}

			os << oneHeaderToString(h);
		}

		os << std::endl;
		return os.str();
	}

} // Anonymous namespace

//--------------------------------------------------------
int EXR_DumpHeader(void *ilmHeader, char *outBuffer, int n)
{
    outBuffer[0] = '\0';

	Header *header = (Header *)ilmHeader;
	if (header == nullptr)
    {
        return 1;
    }

	string headerAsString = oneHeaderToString(*header);
	string result = headerAsString.substr(0, n - 1);
	strcpy(outBuffer, result.c_str());

	return int(headerAsString.size() + 1);
}

//--------------------------------------------------------
void EXR_GetLastErrorMessage(void *vpExrFILE, char *outBuffer, int n)
{
   ExrFILE *exrFile = (ExrFILE *)vpExrFILE;
   const char *message = (vpExrFILE == 0)
                         ? noFileLastErrorMessage.c_str()
                         : exrFile->lastErrorMessage.c_str();
   outBuffer[0] = 0;
   strncat(outBuffer, message, n - 1);
}

//--------------------------------------------------------


