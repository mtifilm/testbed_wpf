//
// MMOFStream.h
//

#ifndef MFOFSTREAM_H
#define MFOFSTREAM_H

//#include <ImfStdIO.h>
#include <ImfIO.h>
#include "Iex.h"

using namespace Imath;

class MFOFStream: public Imf::OStream
{
public:
	MFOFStream(char *bufferPtr, Int64 bufferSize, Int64 *eofOut);
    ~MFOFStream();

    virtual void	write(const char c[/*n*/], int n);
    virtual Int64	tellp();
    virtual void	seekp(Int64 pos);

protected:
	char*           _bufferPtr;
	Int64           _bufferSize;
	Int64           _pos;
	Int64			*_eofOut;
};

#endif
