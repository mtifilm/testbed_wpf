//
// MMIFStream.cpp
//

#include "MMIFStream.h"
#include "windows.h"

//------------------------------------------------------------------------------
MMIFStream::MMIFStream(const char fileName[])
: MFIFStream(fileName)
{
   _hFile = reinterpret_cast<void *>(
      CreateFile(fileName, GENERIC_READ, 0, 0, OPEN_EXISTING,
                 FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, 0));
   if (_hFile == INVALID_HANDLE_VALUE)
   {
        throw Iex::IoExc("File open FAIL");
   }

   _length = GetFileSize((HANDLE)_hFile, 0);

   _hFileMapping = reinterpret_cast<void *>(CreateFileMapping(_hFile, 0, PAGE_READONLY, 0, DWORD(_length), 0));
   // In the following, look for NULL, not INVALID_FILE_HANDLE!
   if (_hFile == NULL)
   {
        throw Iex::IoExc("File mapping FAIL");
   }

   _buffer = (char *) MapViewOfFile((HANDLE)_hFileMapping, FILE_MAP_READ, 0, 0, _length);
   if (_buffer == NULL)
   {
        throw Iex::IoExc("File map view creation FAIL");
   }

   _pos = 0;
}

//------------------------------------------------------------------------------
MMIFStream::~MMIFStream ()
{
   if (_buffer != 0)
   {
      UnmapViewOfFile(_buffer);
   }

   CloseHandle((HANDLE)_hFileMapping);
   CloseHandle((HANDLE)_hFile);
}

//------------------------------------------------------------------------------
