//---------------------------------------------------------------------------

#ifndef TiffHeaderH
#define TiffHeaderH
//---------------------------------------------------------------------------

#include "machine.h"
#include "ImageFormat3.h"
#include "MediaFrameBuffer.h"
//---------------------------------------------------------------------------

const MTI_UINT16 TIFF_MAGIC_NUMBER_BE = 0x4D4D; // "MM" = Motorola byte order (BE)
const MTI_UINT16 TIFF_MAGIC_NUMBER_LE = 0x4949; // "II" = Intel byte order (LE)
//---------------------------------------------------------------------------

struct TiffHeader
{
	MTI_UINT16 sMagic = 0;
	MTI_UINT32 height = 0;
	MTI_UINT32 width = 0;
	MTI_UINT16 bitsPerSample = 0;
	MTI_UINT16 samplesPerPixel = 0;
	MTI_UINT32 rowsPerStrip = 0xFFFFFFFFU;
	MTI_UINT16 photometric = 0;
	MTI_UINT16 compression = 0;
	MTI_UINT16 quality = 0;
	MTI_UINT16 orientation = 0;
	MTI_UINT32 x = 0;
	MTI_UINT32 y = 0;
	MTI_UINT32 dataOffset = 0;
	MTI_UINT32 scanLineLength = 0;
	MTI_UINT64 fileSize = 0;
	float resolution = 0;
	float offset = 0;

	//---------------------------------------------------------

	int extractFromFrameBuffer(MediaFrameBufferSharedPtr frameBuffer);
	int convertToImageFormat(CImageFormat &imageFormat);
	bool isFileSwapped() const { return sMagic == TIFF_MAGIC_NUMBER_BE; };
	static bool isFileSwapped(const MTI_UINT16 *dataPtr) { return *dataPtr == TIFF_MAGIC_NUMBER_BE; };
	//---------------------------------------------------------
	string ToString()
	{
		MTIostringstream os;
		os << "TIFF HEADER DUMP:" << endl;
		os << "sMagic = " << sMagic << endl;
		os << "isFileSwapped = " << isFileSwapped() << endl;
		os << "height = " << height << endl;
		os << "width = " << width << endl;
		os << "bitsPerSample = " << bitsPerSample << endl;
		os << "samplesPerPixel = " << samplesPerPixel << endl;
		os << "rowsPerStrip = " << rowsPerStrip << endl;
		os << "photometric = " << photometric << endl;
		os << "compression = " << compression << endl;
		os << "quality = " << quality << endl;
		os << "orientation = " << orientation << endl;
		os << "x = " << x << endl;
		os << "y = " << y << endl;
		os << "dataOffset = " << dataOffset << endl;
		os << "scanLineLength = " << scanLineLength << endl;
		os << "fileSize = " << fileSize << endl;
		os << "resolution = " << resolution << endl;
		os << "offset = " << offset;

		return os.str();
	}
};
//---------------------------------------------------------------------------

#endif
