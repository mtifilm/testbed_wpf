//---------------------------------------------------------------------------

#ifndef DpxFrameWriterH
#define DpxFrameWriterH
//---------------------------------------------------------------------------

#include "MediaFrameFileWriter.h"
//---------------------------------------------------------------------------

class DpxFrameWriter : public MediaFrameFileWriterBase
{
public:
	DpxFrameWriter(const string &filename);
	~DpxFrameWriter() = default;

	int encodeImage(
			MediaFrameBufferSharedPtr frameBufferIn,
			MediaFrameBufferSharedPtr &frameBufferOut);
};
//---------------------------------------------------------------------------

#endif
