//---------------------------------------------------------------------------

#pragma hdrstop

#include "TiffLibWrapper.h"

#include "err_file.h"
#include "MTImalloc.h"
#include "TiffHeader.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

//////////////////////////////////////////////////////////////////////

///////////////////////////////
// libtiff I/O callbacks   //
///////////////////////////////

//////////////////////////////////////////////////////////////////////

tsize_t TiffLibWrapper::ReadProc(thandle_t thandle, tdata_t buf, tsize_t i32NumBytes)
{
	auto tiffLibHandle = reinterpret_cast<TiffLibHandle_t>(thandle);
	if (tiffLibHandle == nullptr)
	{
		return 0;
	}

   auto frameBuffer = tiffLibHandle->frameBuffer;
   unsigned char *imageData = (unsigned char *)(frameBuffer->getBufferPtr());
   tsize_t offset = tiffLibHandle->currentOffset;
   tsize_t roomLeft = (tsize_t)(frameBuffer->getBufferSize()) - offset;
   tsize_t count = (size_t)std::min<tsize_t>(i32NumBytes, roomLeft);

	MTImemcpy((unsigned char *)buf, imageData + offset, (size_t)count);
   tiffLibHandle->currentOffset += count;

   return (tsize_t)count;
}

tsize_t TiffLibWrapper::WriteProc(thandle_t thandle, tdata_t buf, tsize_t size)
{
	// TRACE_0(errout << "WriteProc(" << size << ")");
   auto tiffLibHandle = reinterpret_cast<TiffLibHandle *>(thandle);
	if (tiffLibHandle == nullptr)
	{
		return 0;
	}

   auto frameBuffer = tiffLibHandle->frameBuffer;
   unsigned char *imageData = (unsigned char *)frameBuffer->getBufferPtr();
   toff_t offset = tiffLibHandle->currentOffset;
   tsize_t roomLeft = (tsize_t)(frameBuffer->getBufferSize()) - offset;
   tsize_t count = (tsize_t)std::min<tsize_t>(size, roomLeft);

	MTImemcpy(imageData + offset, (unsigned char *)buf, (size_t)count);
   tiffLibHandle->currentOffset += count;

   return count;
}

toff_t TiffLibWrapper::SeekProc(thandle_t thandle, toff_t offsetArg, int whence)
{
	// TRACE_0(errout << "tiffSeekProc(" << off << ")");
   auto tiffLibHandle = reinterpret_cast<TiffLibHandle_t>(thandle);
	if (tiffLibHandle == nullptr)
	{
		return 0;
	}

   toff_t newOffset;
   toff_t oldOffset = tiffLibHandle->currentOffset;
   tsize_t bufferSize = (tsize_t)(tiffLibHandle->frameBuffer->getBufferSize());

	switch (whence)
	{
      default:
      case SEEK_SET:
         newOffset = offsetArg;;
         break;
      case SEEK_CUR:
         // Oddly, toff_t is unsigned!!
         newOffset = oldOffset + offsetArg;
         break;
      case SEEK_END:
         // Oddly, toff_t is unsigned!!
         newOffset = bufferSize - offsetArg;
         break;
	}

   tiffLibHandle->currentOffset = std::max<toff_t>(std::min<toff_t>(newOffset, bufferSize), 0);

   return tiffLibHandle->currentOffset;
}

int TiffLibWrapper::CloseProc(thandle_t thandle)
{
	// TRACE_0(errout << "tiffCloseProc()");

	// Nothing to do here

	return 0;
}

toff_t TiffLibWrapper::SizeProc(thandle_t thandle)
{
	// TRACE_0(errout << "tiffSizeProc()");

	// Not sure why this is no-op'ed. For performance, I hope it isn't called

	return 0;
}

int TiffLibWrapper::MapProc(thandle_t thandle, tdata_t* pbase, toff_t* psize)
{
	// TRACE_0(errout << "tiffMapProc()");

	// We told libtiff to not do file mapping, so this will never get called

	return (0);
}

void TiffLibWrapper::UnmapProc(thandle_t thandle, tdata_t base, toff_t size)
{
	// TRACE_0(errout << "tiffUnmapProc()");

	// We told libtiff to no0t do file mapping, so this will never get called

}

int TiffLibWrapper::ReadHeader(thandle_t thandle, TiffHeader *header)
{
	TIFF* tiff;
	try
	{
		// r="read", m="no memory thandle", c="no data chopping"
		tiff = ClientOpen(thandle, "TIFF_IMAGE", "rmc");

	}
	catch (...)
	{
		return FILE_ERROR_READ;
	}

	// check if it's a tiff file
	if (!tiff)
	{
		return FILE_ERROR_BAD_MAGIC_NUMBER;
	}

	// assuming a single image per file
	// set directory to read as the first image
	if (!TIFFSetDirectory(tiff, (uint16)0))
	{
		TIFFClose(tiff);
		return FILE_ERROR_BAD_COMPONENTS;
	}

	// Compute the magic number - not supported directly in tiffio.h
	header->sMagic = TIFFIsByteSwapped(tiff) ? TIFF_MAGIC_NUMBER_BE : TIFF_MAGIC_NUMBER_LE;

	// Get some interesting tags
	TIFFGetField(tiff, TIFFTAG_COMPRESSION, &header->compression);
	TIFFGetField(tiff, TIFFTAG_IMAGEWIDTH, &header->width);
	TIFFGetField(tiff, TIFFTAG_IMAGELENGTH, &header->height);
	TIFFGetField(tiff, TIFFTAG_SAMPLESPERPIXEL, &header->samplesPerPixel);
	TIFFGetField(tiff, TIFFTAG_BITSPERSAMPLE, &header->bitsPerSample);
	TIFFGetField(tiff, TIFFTAG_ROWSPERSTRIP, &header->rowsPerStrip);
	TIFFGetField(tiff, TIFFTAG_PHOTOMETRIC, &header->photometric);
	TIFFGetField(tiff, TIFFTAG_ORIENTATION, &header->orientation);
	header->scanLineLength = TIFFScanlineSize(tiff);

	if (header->compression == COMPRESSION_JPEG)
	{
		TIFFGetField(tiff, TIFFTAG_JPEGQUALITY, header->quality);
	}

	MTI_UINT32 *stripOffsets = 0;
	TIFFGetField(tiff, TIFFTAG_STRIPOFFSETS, &stripOffsets);
	header->dataOffset = stripOffsets[0];

   auto tiffLibHandle = reinterpret_cast<TiffLibHandle_t>(thandle);
	if (tiffLibHandle != nullptr && tiffLibHandle->frameBuffer != nullptr)
	{
		header->fileSize = tiffLibHandle->frameBuffer->getDataSize();
	}

	// we have all the info, close tiff handle
	TIFFClose(tiff);

	return 0;
}

void TiffLibWrapper::ErrorHandler(const char *module, const char *fmt, va_list ap)
{
	char buffer[1001];
	vsprintf(buffer, fmt, ap);
	TRACE_0(errout << "TIFF ERROR: " << module << ": " << buffer);
}

void TiffLibWrapper::WarningHandler(const char *module, const char *fmt, va_list ap)
{
	char buffer[1001];
	vsprintf(buffer, fmt, ap);
	TRACE_0(errout << "TIFF WARNING: " << module << ": " << buffer);
}

/***************************************************************************
 TIFFClientOpen OPTIONS
 ****************************************************************************
 The open mode parameter can include the following flags in addition to the
 ``r'', ``w'', and ``a'' flags. Note however that option flags must follow
 the read-write-append specification.

 l   When creating a new file force information be written with Little-Endian
 byte order (but see below). By default the library will create new files
 using the native CPU byte order.

 b   When creating a new file force information be written with Big-Endian
 byte order (but see below). By default the library will create new files
 using the native CPU byte order.

 L   Force image data that is read or written to be treated with bits filled
 from Least Significant Bit (LSB) to Most Significant Bit (MSB). Note that
 this is the opposite to the way the library has worked from its inception.

 B   Force image data that is read or written to be treated with bits filled
 from Most Significant Bit (MSB) to Least Significant Bit (LSB); this is
 the default.

 H   Force image data that is read or written to be treated with bits filled
 in the same order as the native CPU.

 M   Enable the use of memory-mapped files for images opened read-only. If
 the underlying system does not support memory-mapped files or if the
 specific image being opened cannot be memory-mapped then the library
 will fallback to using the normal system interface for reading
 information.
 By default the library will attempt to use memory-mapped files.

 m   Disable the use of memory-mapped files.

 C   Enable the use of ``strip chopping'' when reading images that are
 comprised of a single strip or tile of uncompressed data. Strip chopping
 is a mechanism by which the library will automatically convert the
 single-strip image to multiple strips, each of which has about 8 KB
 of data. This facility can be useful in reducing the amount of memory
 used to read an image because the library normally reads each strip in
 its entirety. Strip chopping does however alter the apparent contents
 of the image because when an image is divided into multiple strips it
 looks as though the underlying file contains multiple separate strips.
 Finally, note that default handling of strip chopping is a compile-time
 configuration parameter. The default behaviour, for backwards
 compatibility, is to enable strip chopping.

 c   Disable the use of strip chopping when reading images.

 h   Read TIFF header only, do not load the first image directory. That could
 be useful in case of the broken first directory. We can open the file and
 proceed to the other directories.

 *************************************************************************** */

TIFF* TiffLibWrapper::ClientOpen(thandle_t thandle, const char* name, const char* mode)
{
	// TRACE_0(errout << "tiffClientOpen()");
	TIFF* tif;

	TIFFSetErrorHandler(ErrorHandler);
	TIFFSetWarningHandler(WarningHandler);

	tif = TIFFClientOpen(name, mode, thandle,
								ReadProc,
								WriteProc,
								SeekProc,
								CloseProc,
								SizeProc,
								MapProc,
								UnmapProc);

	return tif;
}


// -------------------------------------------------------------------------
