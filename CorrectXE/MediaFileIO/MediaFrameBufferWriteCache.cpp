//---------------------------------------------------------------------------

#pragma hdrstop

#include "MediaFrameBufferWriteCache.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#include "bthread.h"
#include "err_file.h"
#include "FileSweeper.h"        // For GetLastSystemErrorMessage()
#include "HRTimer.h"
#include "MediaFileIO.h"
#include "MediaFrameFileWriter.h"
#include "MTImalloc.h"
#include "MTIsleep.h"

// -------------------------------------------------------------------------

const int MAX_QUEUED_WRITE_CACHE_BUFFERS = 4;
// -------------------------------------------------------------------------

MediaFrameBufferWriteCache::MediaFrameBufferWriteCache()
{
}
// -------------------------------------------------------------------------

MediaFrameBufferWriteCache::~MediaFrameBufferWriteCache()
{
	ShutDown();
}
// -------------------------------------------------------------------------

void MediaFrameBufferWriteCache::SetDelayedWriteFilename(const string &filename)
{
	// Short circuit?
	if (filename == _delayedWriteFilename)
	{
		return;
	}

	CAutoSpinLocker lock(_lock);
	// TRACE_3(errout << "WWWWWWWWWWWW IBWBC: setWriteCachedFile");

	// If there's an existing dirty frame, move it to the regular output queue.
	if (_delayedWriteFrameBuffer)
	{
		queueFrameBufferForRegularOutput(_delayedWriteFilename, _delayedWriteFrameBuffer, true);  // non-blocking
		_delayedWriteFrameBuffer = nullptr;
	}

	// OK! New delayed-write frame!
	_delayedWriteFilename = filename;
}
// -------------------------------------------------------------------------

void MediaFrameBufferWriteCache::AddFrameBufferToCache(const string &filename, MediaFrameBufferSharedPtr frameBuffer)
{
	CAutoSpinLocker lock(_lock);

	if (filename == _delayedWriteFilename)
   {
		setDelayedWriteFrameBuffer(frameBuffer);
	}
	else
	{
		queueFrameBufferForRegularOutput(filename, frameBuffer);
	}
}
// -------------------------------------------------------------------------

MediaFrameBufferSharedPtr MediaFrameBufferWriteCache::RetrieveCachedFrameBuffer(const string &filename)
{
	CAutoSpinLocker lock(_lock);

	// We first check if a write to disk is in progress.
//	if (filename == _filenameOfDelayedWriteInProgress)
//	{
//		TRACE_3(errout << "WWWWWWWWWWWW MFBWC retrieved buffer w/ delayed write in progress: " << filename);
//		return _frameBufferOfDelayedWriteInProgress;
//	}

	if (filename == _filenameOfRegularWriteInProgress)
	{
		TRACE_3(errout << "WWWWWWWWWWWW MFBWC retrieved buffer w/ regular write in progress: " << filename);
		return _frameBufferOfRegularWriteInProgress;
	}

	// Waiting to be delayed written?
	if (filename == _delayedWriteFilename)
	{
		TRACE_3(errout << "WWWWWWWWWWWW MFBWC retrieved waiting delayed write buffer: " << filename);
		return _delayedWriteFrameBuffer;
	}

	// In queue to be regularly written?
	cacheListIteratorType iter;
	for (iter = _regularWriteCache.begin(); iter != _regularWriteCache.end() && iter->first != filename; ++iter)
	{
		// Do nothing else;
	}

	if (iter != _regularWriteCache.end())
	{
		TRACE_3(errout << "WWWWWWWWWWWW MFBWC retrieved from regular queue: " << filename);
		return iter->second;
	}

	// No dice!
	TRACE_3(errout << "WWWWWWWWWWWW MFBWC not found in write cache: " << filename);
	return nullptr;
}
// -------------------------------------------------------------------------

void MediaFrameBufferWriteCache::FlushDirtyFrameToDiskIfMatch(const string &filename)
{
	// Short circuit.
	if (filename.empty())
	{
		return;
	}

	CAutoSpinLocker lock(_lock);

	// Synchronously flush the "delayed write" buffer if it matches.
	if (_delayedWriteFrameBuffer && _delayedWriteFilename == filename)
	{
		auto frameBuffer = _delayedWriteFrameBuffer;
		_delayedWriteFrameBuffer = nullptr;

		{
			CAutoSpinUnlocker unlock(_lock);
			TRACE_3(errout << "WWWWWWWWWWWW MFBWC: SPOT FLUSHING delayed: " << filename);
//			DBCOMMENT("SPOT FLUSH DELAYED");
//			DBTRACE(frameBuffer->getHeaderDataSize());
//			DBTRACE(frameBuffer->getImageDataSize());
//			DBTRACE(frameBuffer->getTrailerDataSize());
			writeFrameBufferToDiskSynchronously(_delayedWriteFilename, frameBuffer);
		}
	}

	// Find the entry for the file in the cache
	cacheListIteratorType iter;
	for (iter = _regularWriteCache.begin(); iter != _regularWriteCache.end() && iter->first != filename; ++iter)
	{
		// Do nothing else;
	}

	if (iter != _regularWriteCache.end())
	{
		auto frameBuffer = iter->second;
		_regularWriteCache.erase(iter);

		{
			CAutoSpinUnlocker unlock(_lock);
			TRACE_3(errout << "WWWWWWWWWWWW MFBWC: Spot flushing regular: " << filename);
//			DBCOMMENT("SPOT FLUSH REGULAR");
//			DBTRACE(frameBuffer->getHeaderDataSize());
//			DBTRACE(frameBuffer->getImageDataSize());
//			DBTRACE(frameBuffer->getTrailerDataSize());
			writeFrameBufferToDiskSynchronously(filename, frameBuffer);
		}
	 }

	// Spin wait for delayed write frame to finish writing if match.
	CHRTimer timer2;
	while (_filenameOfRegularWriteInProgress == filename && timer2.Read() < 250.0)
	{
		CAutoSpinUnlocker lock(_lock);
		MTImillisleep(2);
	}
}

// -------------------------------------------------------------------------

void MediaFrameBufferWriteCache::FlushCacheSynchronously()
{
	// Necessary because we need to unlock at some point to release waiting
	// write threads, and we don't want these running amok!
	killBackgroundThreads();

	// THIS MIGHT BE VERY, VERY SLOW!
	CAutoSpinLocker lock(_lock);
	_cacheFlushInProgress = true;

	// Flush the "delayed write" buffer.
	if (_delayedWriteFrameBuffer)
	{
		TRACE_3(errout << "WWWWWWWWWWWW MFBWC: Flushing delayed write buffer to output queue: " << _delayedWriteFilename);
		queueFrameBufferForRegularOutput(_delayedWriteFilename, _delayedWriteFrameBuffer, true);  // guaranteed non-blocking, non-unlocking
		_delayedWriteFrameBuffer = nullptr;
	}

	while (_regularWriteCache.size() > 0)
	{
		do
		{
			auto &cacheEntry = _regularWriteCache.front();
			_filenameOfRegularWriteInProgress = cacheEntry.first;
			_frameBufferOfRegularWriteInProgress = cacheEntry.second;
			_regularWriteCache.pop_front();
			TRACE_3(errout << "WWWWWWWWWWWW MFBWC: Flushing regular: " << _filenameOfRegularWriteInProgress);
//			DBCOMMENT("REGULAR FLUSH");
//			DBTRACE(_frameBufferOfRegularWriteInProgress->getHeaderDataSize());
//			DBTRACE(_frameBufferOfRegularWriteInProgress->getImageDataSize());
//			DBTRACE(_frameBufferOfRegularWriteInProgress->getTrailerDataSize());
			writeFrameBufferToDiskSynchronously(_filenameOfRegularWriteInProgress, _frameBufferOfRegularWriteInProgress);
			_filenameOfRegularWriteInProgress = "";
			_frameBufferOfRegularWriteInProgress = nullptr;
		}
		while (_regularWriteCache.size() > 0);

		{
			CAutoSpinUnlocker unlock(_lock);
			_writeCacheNotFull.signal();
			MTImillisleep(1);
		}
	}

	_cacheFlushInProgress = false;
	MTIassert(!_delayedWriteFrameBuffer);
	MTIassert(_regularWriteCache.size() == 0);
}
// -------------------------------------------------------------------------

void MediaFrameBufferWriteCache::ShutDown()
{
	FlushCacheSynchronously();
	killBackgroundThreads();
}
// -------------------------------------------------------------------------

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------

void MediaFrameBufferWriteCache::setDelayedWriteFrameBuffer(MediaFrameBufferSharedPtr frameBuffer)
{
	// _lock MUST BE LOCKED BY CALLER!

	TRACE_3(errout << "WWWWWWWWWWWW MFBWC added for delayed write: " << _delayedWriteFilename);
	_delayedWriteFrameBuffer = frameBuffer;
	_delayedWriteFrameBufferWriteTime = _timer.Read() + 5000.0;  // QQQ manifest
	_delayedWriteEvent.signal();

	// Lazy background thread start.
	if (_delayedWriterThreadID == nullptr && !_cacheFlushInProgress)
	{
		spawnBackgroundThreads();
	}
}
// -------------------------------------------------------------------------

void MediaFrameBufferWriteCache::queueFrameBufferForRegularOutput(
	const string &filename,
	MediaFrameBufferSharedPtr buffer,
	bool nonBlocking)
{
	// _lock MUST BE LOCKED BY CALLER!

	MTIassert(!filename.empty());
	if (filename.empty())
	{
		return;
	}

	MTIassert(buffer);
	if (!buffer)
	{
		return;
	}

	// Ensure no duplicates!
	removeBufferFromCache(filename);

	while ((_regularWriteCache.size() >= MAX_QUEUED_WRITE_CACHE_BUFFERS) && !_cacheFlushInProgress && !nonBlocking)
	{
		_writeCacheNotFull.reset();
		{
			CAutoSpinUnlocker unlock(_lock);
			_writeCacheNotFull.wait(50);
		}
	}

	// Put it on the back of the queue.
	_regularWriteCache.emplace_back(filename, buffer);
	_writeCacheNotEmpty.signal();

	TRACE_3(errout << "WWWWWWWWWWWW MFBWC added frame buffer for: " << filename << " (regular output)");

	// Lazy background thread start.
	if (_regularWriterThreadID == nullptr && !_cacheFlushInProgress)
	{
		spawnBackgroundThreads();
	}
}
// -------------------------------------------------------------------------

void MediaFrameBufferWriteCache::removeBufferFromCache(const string &filename)
{
	// _lock MUST BE LOCKED BY CALLER!

	// Short circuit.
	if (filename.empty())
	{
		return;
	}

	// Find the entry for the file in the cache
	cacheListIteratorType iter;
	for (iter = _regularWriteCache.begin(); iter != _regularWriteCache.end() && iter->first != filename; ++iter)
	{
		// Do nothing else;
	}

	// Remove it if found.
	if (iter != _regularWriteCache.end())
	{
		_regularWriteCache.erase(iter);
	}
}
// -------------------------------------------------------------------------

int MediaFrameBufferWriteCache::writeFrameBufferToDiskSynchronously(const string &filename, MediaFrameBufferSharedPtr frameBuffer)
{
	CAutoErrorReporter autoErr("writeCachedDataToDisk", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
	int retVal;

	// _lock must NOT be locked here!
	MTIassert(frameBuffer);
	MTIassert(!filename.empty());
	if (filename.empty() || !frameBuffer)
	{
		retVal = FILE_ERROR_INTERNAL;
		autoErr.errorCode = retVal;
		autoErr.msg << "Synchronous disk write FAILED" << endl
						<< "    File " << filename << endl
						<< "    INTERNAL ERROR: BAD PARAMETERS";
		return retVal;
	}

	auto writer = std::unique_ptr<MediaFrameFileWriter>(MediaFrameFileWriter::CreateMediaFrameFileWriter(filename));
	if (!writer)
	{
		retVal = FILE_ERROR_INTERNAL;
		autoErr.errorCode = retVal;
		autoErr.msg << "Synchronous disk write FAILED" << endl
						<< "    File " << filename << endl
						<< "    Error was: Could not create media writer!";
		return retVal;
	}

	MediaFrameBufferSharedPtr encodedFrameBuffer;
	if (frameBuffer->isDecoded())
	{
		retVal = writer->encodeImage(frameBuffer, encodedFrameBuffer);
		if (retVal != 0)
		{
			autoErr.errorCode = retVal;
			autoErr.msg << "Synchronous disk write FAILED" << endl
							<< "    File " << filename << endl
							<< "    Error was: Frame buffer encoding failed!";
			return retVal;
		}
	}
	else
	{
		encodedFrameBuffer = frameBuffer;
	}

	retVal = writer->writeFrameFiletoDiskSync(encodedFrameBuffer);
	if (retVal != 0)
	{
		autoErr.RepeatTheError();
	}

	return 0;
}
// -------------------------------------------------------------------------

void MediaFrameBufferWriteCache::spawnBackgroundThreads()
{
	if (_delayedWriterThreadID == NULL)
	{
		BThreadSpawn(startDelayedWriterThread, reinterpret_cast<void *>(this));
		TRACE_3(errout << "WWWWWWWWWWWW IBWBC: Delayed write thread ID = 0x" << std::setbase(16) << std::setw(8) << std::setfill('0') << _delayedWriterThreadID);
	}

	if (_regularWriterThreadID == NULL)
	{
		BThreadSpawn(startRegularWriterThread, reinterpret_cast<void *>(this));
		TRACE_3(errout << "WWWWWWWWWWWW IBWBC: Regular write thread ID = 0x" << std::setbase(16) << std::setw(8) << std::setfill('0') << _regularWriterThreadID);
	}
}
// -------------------------------------------------------------------------

void MediaFrameBufferWriteCache::killBackgroundThreads()
{
	CAutoSpinLocker lock(_lock);

	if (_delayedWriterThreadID != nullptr)
	{
		_terminateDelayedWriterThread = true;
		_delayedWriteEvent.signal();
	}

	if (_regularWriterThreadID != nullptr)
	{
		_terminateRegularWriterThread = true;
		_writeCacheNotEmpty.signal();
	}

	CHRTimer timer;

	while (_terminateDelayedWriterThread || _terminateRegularWriterThread)
	{
		CAutoSpinUnlocker unlock(_lock);
		Sleep(0);

		// Time out after five seconds
		if (timer.Read() > 5000)
		{
			if (_terminateDelayedWriterThread && _delayedWriterThreadID)
			{
				//BThreadKill(_delayedWriterThreadID);
				_delayedWriterThreadID = nullptr;
				_terminateDelayedWriterThread = false;
			}

			if (_terminateRegularWriterThread && _regularWriterThreadID)
			{
				//BThreadKill(_regularWriterThreadID);
				_regularWriterThreadID = nullptr;
				_terminateRegularWriterThread = false;
			}
		}
	}
}
// -------------------------------------------------------------------------

/* static */
void MediaFrameBufferWriteCache::startDelayedWriterThread(void *vpAppData, void *vpReserved)
{
   MediaFrameBufferWriteCache *_this = static_cast<MediaFrameBufferWriteCache*>(vpAppData);

	_this->_delayedWriterThreadID = vpReserved;
	TRACE_3(errout << "WWWWWWWWWWWW IBWBC: Delayed write threadID = 0x" << std::setbase(16) << std::setw(8) << std::setfill('0') << GetCurrentThreadId());

   if (BThreadBegin(vpReserved))
   {
      exit(1); // say what???
   }

	_this->runDelayedWriterThread();

	_this->_delayedWriterThreadID = NULL; // signal we have exited
}
// -------------------------------------------------------------------------

void MediaFrameBufferWriteCache::runDelayedWriterThread()
{
	int sleepTimeInMilliseconds = 500;
	CAutoSpinLocker lock(_lock);

	while (!_terminateDelayedWriterThread)
	{
		double timeNow = _timer.Read();
		if (_delayedWriteFrameBuffer && timeNow >= _delayedWriteFrameBufferWriteTime)
		{
			TRACE_3(errout << "WWWWWWWWWWWW MFBWC: Moving delayed write buffer to output queue: " << _delayedWriteFilename);
			queueFrameBufferForRegularOutput(_delayedWriteFilename, _delayedWriteFrameBuffer, true);  // guaranteed non-blocking, non-unlocking
			_delayedWriteFrameBuffer = nullptr;
		}
      else
		{
			if (_terminateDelayedWriterThread)
			{
				break;
			}

			sleepTimeInMilliseconds = _delayedWriteFrameBuffer
												? ((int) std::max<double>(1.0, _delayedWriteFrameBufferWriteTime - timeNow + 1.0))
												: 1000;
			_delayedWriteEvent.reset();
			{
				CAutoSpinUnlocker unlock(_lock);
				_delayedWriteEvent.wait(sleepTimeInMilliseconds);
			}
		}
	}

	_terminateDelayedWriterThread = false;
}
// -------------------------------------------------------------------------

/* static */
void MediaFrameBufferWriteCache::startRegularWriterThread(void *vpAppData, void *vpReserved)
{
   MediaFrameBufferWriteCache *_this = static_cast<MediaFrameBufferWriteCache*>(vpAppData);

	_this->_regularWriterThreadID = vpReserved;
	TRACE_3(errout << "WWWWWWWWWWWW IBWBC: Regular writer threadID = 0x" << std::setbase(16) << std::setw(8) << std::setfill('0') << GetCurrentThreadId());

   if (BThreadBegin(vpReserved))
   {
      exit(1); // say what???
   }

	_this->runRegularWriterThread();

	_this->_regularWriterThreadID = NULL; // signal we have exited
}
// -------------------------------------------------------------------------

void MediaFrameBufferWriteCache::runRegularWriterThread()
{
	int sleepTimeInMilliseconds = 500;
	CAutoSpinLocker lock(_lock);

	while (!_terminateRegularWriterThread)
   {
		double timeNow = _timer.Read();
		if (_regularWriteCache.size() > 0)
		{
			auto &cacheEntry = _regularWriteCache.front();
			_filenameOfRegularWriteInProgress = cacheEntry.first;
			_frameBufferOfRegularWriteInProgress = cacheEntry.second;
			_regularWriteCache.pop_front();
			_writeCacheNotFull.signal();

			{
				CAutoSpinUnlocker unlock(_lock);
				TRACE_3(errout << "WWWWWWWWWWWW MFBWC: Writing (regular): " << _filenameOfRegularWriteInProgress);
//				DBCOMMENT("REGULAR WRITE");
//				DBTRACE(_frameBufferOfRegularWriteInProgress->getHeaderDataSize());
//				DBTRACE(_frameBufferOfRegularWriteInProgress->getImageDataSize());
//				DBTRACE(_frameBufferOfRegularWriteInProgress->getTrailerDataSize());
				writeFrameBufferToDiskSynchronously(_filenameOfRegularWriteInProgress, _frameBufferOfRegularWriteInProgress);
			}

			_filenameOfRegularWriteInProgress = "";
			_frameBufferOfRegularWriteInProgress = nullptr;
		}
		else
		{
			sleepTimeInMilliseconds = 50;
			_writeCacheNotEmpty.reset();
			{
				CAutoSpinUnlocker unlock(_lock);
				_writeCacheNotEmpty.wait(sleepTimeInMilliseconds);
			}
		}
	}

	_terminateRegularWriterThread = false;
}
// -------------------------------------------------------------------------
