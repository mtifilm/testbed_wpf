//---------------------------------------------------------------------------

#pragma hdrstop

#include "ExrFrameWriter.h"

#include "err_file.h"
#include "ExrHeader.h"
#include "EXRWrapper.h"
#include "HRTimer.h"
#include "ImageDatumConvert.h"
#include "MTImalloc.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
//---------------------------------------------------------------------------

ExrFrameWriter::ExrFrameWriter(const string &filename)
: MediaFrameFileWriterBase(filename)
{
}
//---------------------------------------------------------------------------

namespace
{
	///////////////////////////////////////////////////////////////////////////////
	//
	// Get the Aligned Size for a given file - takes into account the sector
	// size of the disk the file exists on in order to determine the
	// Aligned Size needed for a given file.  This is important in the
	// case of CreateFile() / ReadFile() in Windows where we wish to use
	// the FILE_FLAG_NO_BUFFERING flag to speed up reading of image files
	// by avoiding O/S caching.
	//
	// Return Values:
	// success = the aligned size
	// failure = the passed in size
	//
	// 10/24/2003 - mpr
	//
	///////////////////////////////////////////////////////////////////////////////
	size_t getSectorAlignedSize(const string &filename, size_t fileSize)
	{
		unsigned long bytesPerSector = 0;

		bool success = ::GetBytesPerSector(const_cast<char *>(filename.c_str()), &bytesPerSector);
		if (!success || bytesPerSector == 0)
		{
			bytesPerSector = 512;
		}

		return bytesPerSector * ((fileSize + bytesPerSector - 1) / bytesPerSector);
	}
}
//---------------------------------------------------------------------------

int ExrFrameWriter::encodeImage(MediaFrameBufferSharedPtr frameBufferIn, MediaFrameBufferSharedPtr &frameBufferOut)
{
	CHRTimer timer;
	MTIassert(frameBufferIn);
	if (!frameBufferIn)
	{
		return FILE_ERROR_READ;
	}

	// If we were handed an encoded frame buffer (which contains the raw file)
	// just blast it out.
	if (!frameBufferIn->isDecoded())
	{
		return MediaFrameFileWriterBase::writeFrameFiletoDiskSync(frameBufferIn);
	}

	//-----------------
	// Get the image format.
	CImageFormat imageFormat = frameBufferIn->getImageFormat();
	unsigned dataLength = imageFormat.getBytesPerField();

	//-----------------
	// Copy out the data, changing it to HALF format.
	int width = imageFormat.getPixelsPerLine();
	int height = imageFormat.getLinesPerFrame();
	int comp = imageFormat.getComponentsPerPixel();
	int totalComp = width * height * comp;
	auto halfData = std::unique_ptr<unsigned short>(new unsigned short[totalComp]);
	ConvertImageDataToHalfs(
			(unsigned short *)frameBufferIn->getImageDataPtr(),
			(unsigned short *)halfData.get(),
			totalComp);

	double decodeTime = timer.Read();
	TRACE_3(errout << "XXXXXXXXXXXX EXR DECODE of " << _filename << " took " << decodeTime << " msec XXXXXXXXXXXX");

	//-----------------
	// We stashed the EXR header in the "opaque metadata" field of the frame buffer.
	auto &opaqueMetadata = frameBufferIn->getOpaqueMetadata();
	MTIassert(opaqueMetadata);
	auto exrOpaqueMetadata = static_cast<ExrOpaqueMetadata *>(opaqueMetadata.get());
	MTIassert(exrOpaqueMetadata);
	ExrHeader *exrHeaderPtr = exrOpaqueMetadata->getHeaderPtr();
	void *ilmHeaderPtr = exrHeaderPtr ? exrHeaderPtr->opaqueIlmHeader.get() : nullptr;

	//-----------------
	// Allocate a new raw media buffer with enough room to write the header and data.
	const size_t RoomForTheFileHeader = 1024 * 1024; // very generous!
	size_t rawBufferSize = getSectorAlignedSize(_filename, dataLength + RoomForTheFileHeader);
	auto rawMediaFrameBuffer = std::make_shared<RawMediaFrameBuffer>(rawBufferSize);

	//-----------------
	// Encode the data to a memory file in the raw buffer.
	int retVal = 0;
//	void *openExrHandle = EXR_OpenOutputFile(_filename.c_str(), ilmHeaderPtr, &retVal);

	unsigned int eof = 0;
	void *openExrHandle = EXR_OpenOutputMemoryFile(
										(unsigned char *)rawMediaFrameBuffer->getBufferPtr(),
										(unsigned int)rawMediaFrameBuffer->getBufferSize(),
										ilmHeaderPtr,
										&eof,
										&retVal);

	MTIassert(openExrHandle != nullptr);
	if (openExrHandle == nullptr)     // QQQ handle will always be null when retVal != 0?
	{
		TRACE_0(errout << "ERROR: EXR OPEN FAIL (code " << retVal << ") : " << _filename);
		return FILE_ERROR_WRITE;
	}

	retVal = EXR_WritePixels(openExrHandle, (unsigned char *)halfData.get());
	if (retVal != 0)
	{
		TRACE_0(errout << "ERROR: EXR WRITE FAIL (code " << retVal << ") : " << _filename);
	}

	rawMediaFrameBuffer->setDataSize(eof);

	retVal = EXR_CloseFile(openExrHandle);
	if (retVal != 0)
	{
		TRACE_0(errout << "ERROR: EXR CLOSE FAIL (code " << retVal << ") : " << _filename);
	}

	//-----------------
	// Construct the output frame buffer.
	CImageFormat nullImageFormat;
	frameBufferOut = MediaFrameBuffer::CreateSharedBuffer(rawMediaFrameBuffer, nullImageFormat, false);

	double totalTime = timer.Read();
	TRACE_3(errout << "XXXXXXXXXXXX EXR WRITE of " << _filename << " took " << (totalTime - decodeTime)
						<< " msec, total = " << totalTime << " msec XXXXXXXXXXXX");

	return retVal;
}
//---------------------------------------------------------------------------


