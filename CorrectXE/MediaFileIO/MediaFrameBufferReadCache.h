//---------------------------------------------------------------------------

#ifndef MediaFrameBufferReadCacheH
#define MediaFrameBufferReadCacheH
//---------------------------------------------------------------------------

#include "machine.h"
#include "IniFile.h"	// for TRACE
#include "MediaFrameBuffer.h"

/////////////////////////////////////////////////////////////
// GLOBAL FRAME BUFFER READ CACHE ACCESSOR CLASS
// Basic usage:
//
//    MediaFrameBufferReadCache cache;
//    cache.addBufferToCache(filename, frameBuffer);
//
// and elsewhere:
//
//    MediaFrameBufferReadCache cache;
//    auto frameBuffer = cache.retrieveBufferFromCache(filename);
//
/////////////////////////////////////////////////////////////

class MTI_MEDIAFILEIODLL_API MediaFrameBufferReadCache
{
public:

	void addBufferToCache(const string &filename, int frameIndex, MediaFrameBufferSharedPtr buffer);
	MediaFrameBufferSharedPtr retrieveBufferFromCache(const string &filename);
	void removeBufferFromCache(const string &filename);
	void reKeyBufferInCache(const string &strFileNameFrom, const string &strFileNameTo);

	int setSizeInPages(size_t numberOfPages, /* out */ size_t *actualNumberOfPagesAllocated = nullptr);
	int getSerialNumber();
   size_t getPageSize();
	size_t getMediaFrameBufferCacheSizeInBytes();

	void lockCacheFramesInMemory(bool flag);

	void clearCache();
   bool isCachingEnabledByUser();

   class AutoCacheLocker
   {
   public:
      AutoCacheLocker()  { MediaFrameBufferReadCache::_cacheLock.lock(); }
      ~AutoCacheLocker() { MediaFrameBufferReadCache::_cacheLock.unlock(); }
   };

private:

	typedef std::pair< string, MediaFrameBufferSharedPtr > cacheEntryType;
	typedef std::deque< cacheEntryType > cacheListType;
   typedef cacheListType::iterator cacheListIteratorType;

	static cacheListType _cacheList;
	static size_t _totalPagesAllocated;
   static size_t _cachePageAllowance;
	static size_t _totalPagesInUse;
	static size_t _pageSizeInBytes;
	static size_t _originalMinWorkingSetSize;
	static size_t _originalMaxWorkingSetSize;
	static int _serialNumber;
	static bool _enabledByUser;

	static CSpinLock _cacheLock;

   friend class AutoMediaFrameBufferReadCacheLocker;

	cacheListIteratorType find(const string &filename);
	size_t bytes2pages(size_t bytes) { return (bytes + _pageSizeInBytes - 1) / _pageSizeInBytes; };
	size_t pages2bytes(size_t pages) { return pages * _pageSizeInBytes; };
	void trimCacheSize(size_t pages);
	void removeBufferFromCacheInternal(const string &filename);
	void removeBufferFromCacheInternal(cacheListIteratorType iter);
	void clearCacheInternal();

   DEFINE_CBHOOK(onRanOutOfBufferMemory, MediaFrameBufferReadCache);
};

   #endif
