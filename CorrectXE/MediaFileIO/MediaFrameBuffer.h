//---------------------------------------------------------------------------

#ifndef MediaFrameBufferH
#define MediaFrameBufferH
//---------------------------------------------------------------------------

#include "MediaFileIODLL.h"             // Export/Import Defs
#include "ImageFormat3.h"
#include "IniFile.h"
#include "RawMediaFrameBuffer.h"
#include "timecode.h"
#include <memory>
//---------------------------------------------------------------------------

class MediaFrameBuffer;
typedef std::shared_ptr<MediaFrameBuffer> MediaFrameBufferSharedPtr;
//---------------------------------------------------------------------------

class MTI_MEDIAFILEIODLL_API MediaFrameBuffer
{
protected:
   MediaFrameBuffer();

public:
	virtual ~MediaFrameBuffer();

	struct ErrorInfo
	{
		int code = 0;
		string filename;
		string shortMessage;
		string longMessage;

		ErrorInfo(int c=0, string f="", string s="", string l="") : code(c), filename(f), shortMessage(s), longMessage(l) {}
	};

	static std::shared_ptr<MediaFrameBuffer> CreatePrivateBuffer(const CImageFormat &imageFormat);
	static std::shared_ptr<MediaFrameBuffer> CreatePrivateBufferWithTemplate(MediaFrameBuffer &templateBuffer, size_t newSize);
	static std::shared_ptr<MediaFrameBuffer> CreatePrivateBufferWithCopyOfImageData(MediaFrameBuffer &sourceBuffer);
	static std::shared_ptr<MediaFrameBuffer> CreateWrappedImageDataBuffer(unsigned char *otherData, const CImageFormat &imageFormat);
//	static std::shared_ptr<MediaFrameBuffer> CreateSharedBuffer(const string &filename, RawMediaFrameBufferSharedPtr rawFrameBuffer, const CImageFormat &imageFormat);
	static std::shared_ptr<MediaFrameBuffer> CreateSharedBuffer(RawMediaFrameBufferSharedPtr rawFrameBuffer, const CImageFormat &imageFormat, bool isDecodedFlag);
   static std::shared_ptr<MediaFrameBuffer> CreateErrorBuffer(const CImageFormat &imageFormat, const ErrorInfo &errorInfo);

	MediaFrameBuffer *copyImageData();
	MediaFrameBufferSharedPtr clone();
	MediaFrameBufferSharedPtr cloneWithoutImageData();

//	void setFilename(const string &newFilename);
//	string getFilename();
//	void setFrameIndex(int newIndex);
//	int getFrameIndex();
   void setImageFormat(const CImageFormat newFormat);
   CImageFormat getImageFormat();

	unsigned char *getImageDataPtr();
	size_t getImageDataSize();

	virtual RawMediaFrameBufferSharedPtr getRawMediaFrameBuffer();
	size_t getImageDataOffset() { return _imageDataOffset; };

	bool hasSeparateAlpha();
	unsigned char *getAlphaDataPtr();
	size_t getAlphaDataSize();          // from ImageFormat

	void setHeaderData(void *headerData, size_t headerDataSize);
	void *getHeaderDataPtr()    { return _headerData; }
	size_t getHeaderDataSize()  { return _headerDataSize; }

	void setTrailerData(void *trailerData, size_t trailerDataSize);
	void *getTrailerDataPtr()   { return _trailerData; }
	size_t getTrailerDataSize() { return _trailerDataSize; }

//	void setOpaqueMetadata(const std::std::shared_ptr<void> &opaqueMetadata) { _opaqueMetadata = opaqueMetadata; }
//	std::std::shared_ptr<void> getOpaqueMetadata() { return _opaqueMetadata; }

	class OpaqueMetadata
	{
	public:
		OpaqueMetadata() = default;
		virtual ~OpaqueMetadata() = default;
	};

	typedef std::shared_ptr<OpaqueMetadata> OpaqueMetadataSharedPtr;

	void setOpaqueMetadata(OpaqueMetadataSharedPtr &opaqueMetadata) { _opaqueMetadata = opaqueMetadata; }
	OpaqueMetadataSharedPtr &getOpaqueMetadata() { return _opaqueMetadata; }

	enum SwapStatus
   {
      NotSwapped,
      Swapped16,
      Swapped32
   };

   SwapStatus getSwapStatus() { return _dataSwapStatus; };

	bool isDecoded() { return _isDecoded; };
	size_t getCacheFootprint();

	CTimecode getTimecode() { return _timecode; }
	void setTimecode(CTimecode timecode) { _timecode = timecode; }

	void setErrorInfo(const ErrorInfo &info) { _errorInfo = info; }
	const ErrorInfo &getErrorInfo()          { return _errorInfo; }
	void clearErrorInfo()                    { _errorInfo = ErrorInfo(); }

protected:
	friend class DpxFrameReader;
	friend class ExrFrameReader;
	friend class TiffFrameReader;
	friend class MediaFrameFileReaderBase;
	friend class MediaFileIO;

	void setImageDataOffset(size_t offset);
	bool setTotalDataSize(size_t size);

	void setHasSeparateAlpha(bool flag);
	void setAlphaDataOffset(size_t offset);

	void setIsDecoded(bool flag) { _isDecoded = flag; };
	void setSwapStatus(SwapStatus swapped) { _dataSwapStatus = swapped; };

   virtual unsigned char *getBufferPtr() = 0;
	virtual size_t getBufferSize() = 0;

	bool _isDecoded = false;
	CImageFormat _imageFormat;
	size_t _totalDataSize = 0;
	size_t _imageDataOffset = 0;
	size_t _alphaDataOffset = 0;
	bool _hasSeparateAlpha = false;

	void *_headerData = nullptr;
	size_t _headerDataSize = 0;
	void *_trailerData = nullptr;
	size_t _trailerDataSize = 0;

//	std::shared_ptr<void> _opaqueMetadata;
	OpaqueMetadataSharedPtr _opaqueMetadata;

	CTimecode _timecode = CTimecode::NOT_SET;

	SwapStatus _dataSwapStatus = NotSwapped;

	ErrorInfo _errorInfo;

private:
   static unsigned char *_Blackness;
   static int _SizeOfBlacknessInBytes;
   static CSpinLock _BlacknessLock;
};


///////////////////////////////////////////////////////////////////////////

class MediaFrameBufferWithPrivateData : public MediaFrameBuffer
{
//	std::shared_ptr<unsigned char> _bufferPtr = nullptr;
   MTI_UINT8 * _bufferPtr = nullptr;
   size_t _bufferSize = 0;

public:
   MediaFrameBufferWithPrivateData(const CImageFormat &imageFormat);
   MediaFrameBufferWithPrivateData(const CImageFormat &imageFormat, size_t overrideSizeInBytes);
   virtual ~MediaFrameBufferWithPrivateData();

   virtual unsigned char *getBufferPtr();
   virtual size_t getBufferSize();
};

///////////////////////////////////////////////////////////////////////////

class MediaFrameBufferWithSharedData : public MediaFrameBuffer
{
	RawMediaFrameBufferSharedPtr _rawMediaFrameBufferSharedPtr;

public:
	MediaFrameBufferWithSharedData(
//			const string &filename,
			RawMediaFrameBufferSharedPtr rawFrameBuffer,
			const CImageFormat &imageFormat,
			bool isDecodedFlag);
	virtual ~MediaFrameBufferWithSharedData();

	virtual unsigned char *getBufferPtr();
	virtual size_t getBufferSize();
	virtual RawMediaFrameBufferSharedPtr getRawMediaFrameBuffer() { return _rawMediaFrameBufferSharedPtr; };

private:
	size_t getBytesPerFieldInFile();
};

///////////////////////////////////////////////////////////////////////////

class MediaFrameBufferWrappingImageData : public MediaFrameBuffer
{
	unsigned char *_wrappedImageData = nullptr;

public:
	MediaFrameBufferWrappingImageData(unsigned char *wrappedImageData, const CImageFormat &imageFormat);
	virtual ~MediaFrameBufferWrappingImageData();

	virtual unsigned char *getBufferPtr();
	virtual size_t getBufferSize();
};

#endif
