//---------------------------------------------------------------------------

#ifndef MediaFrameFileWriterH
#define MediaFrameFileWriterH
//---------------------------------------------------------------------------

#include "MediaFileIoDefs.h"
#include "MediaFrameBuffer.h"
#include "RawMediaFrameBuffer.h"
//---------------------------------------------------------------------------

class MediaFrameFileWriter
{
public:
	static MediaFrameFileWriter *CreateMediaFrameFileWriter(const string &filename);
	virtual ~MediaFrameFileWriter() = default;

	virtual int encodeImage(
						MediaFrameBufferSharedPtr frameBufferIn,
						MediaFrameBufferSharedPtr &frameBufferOut) =  0;

	virtual int writeFrameFiletoDiskSync(MediaFrameBufferSharedPtr frameBuffer) = 0;

protected:
	MediaFrameFileWriter() = default;
};
//---------------------------------------------------------------------------

class MediaFrameFileWriterBase : public MediaFrameFileWriter
{
public:
	virtual ~MediaFrameFileWriterBase() = default;

	virtual int encodeImage(
						MediaFrameBufferSharedPtr frameBufferIn,
						MediaFrameBufferSharedPtr &frameBufferOut) =  0;

	virtual int writeFrameFiletoDiskSync(MediaFrameBufferSharedPtr frameBuffer);

protected:
	 MediaFrameFileWriterBase(const string &filename)
	 : _filename(filename) {}

	string _filename;

//	void CompressYYYToY(
//			const CImageFormat &imageFormat,
//			MTI_UINT8 *srcBuffer, int srcDataOffset,
//			MTI_UINT8 *dstBuffer, int dstDataOffset);

//   size_t ComputeSizeOfImageDataOnDisk(const CImageFormat &imageFormat);
};
//---------------------------------------------------------------------------

#endif
