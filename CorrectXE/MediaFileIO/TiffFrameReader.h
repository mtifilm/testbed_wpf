//---------------------------------------------------------------------------

#ifndef TiffFrameReaderH
#define TiffFrameReaderH
//---------------------------------------------------------------------------

#include "MediaFrameFileReader.h"
#include "TiffHeader.h"
//---------------------------------------------------------------------------

struct TiffMetadata
{
	MTI_UINT16 sMagic = 0;
	MTI_UINT32 height = 0;
	MTI_UINT32 width = 0;
	MTI_UINT16 bitspersample = 0;
	MTI_UINT16 samplesperpixel = 0;
	MTI_UINT32 rowsperstrip = 0xFFFFFFFFU;
	MTI_UINT16 photometric = 0;
	MTI_UINT16 compression = 0;
	MTI_UINT16 quality = 0;
	MTI_UINT16 orientation = 0;
	MTI_UINT32 x = 0;
	MTI_UINT32 y = 0;
	MTI_UINT32 dataOffset = 0;
	MTI_UINT32 scanLineLength = 0;
	MTI_UINT64 fileSize = 0;
	float resolution = 0;
	float offset = 0;
};
//---------------------------------------------------------------------------

class TiffFrameReader : public MediaFrameFileReaderBase
{
public:
	TiffFrameReader(const string & filename) { _filename = filename; };
	~TiffFrameReader() = default;

	MediaFileType getFileType() { return MediaFileType::Tiff; };

//	static void RemoveFromHeaderCache(const string &filename);
//	static void ClearHeaderCache();

private:
//	int extractHeader(RawMediaFrameBufferSharedPtr rawFrameBuffer, TiffHeader &header);
//	int extractImageFormat(MediaFrameBufferSharedPtr rawFrameBuffer, CImageFormat &imageFormat);
	int decodeImage(MediaFrameBufferSharedPtr frameBufferIn, MediaFrameBufferSharedPtr &frameBufferOut);

//	static TiffHeaderCache _HeaderCache;
};


#endif
