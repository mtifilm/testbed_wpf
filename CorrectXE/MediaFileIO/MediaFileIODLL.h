/*
   File Name:  MediaFileIODLL.h

   This just defines the export/import macros
   if MTI_MEDIAFILEIODLL_API is defined, exports are defined
   else imports are defined
*/

#ifndef MediaFileIoDllH
#define MediaFileIoDllH

//////////////////////////////////////////////////////////////////////
#include "machine.h"

// Define Import/Export for windows
#ifdef MTI_MEDIAFILEIODLL
#define MTI_MEDIAFILEIODLL_API  __declspec(dllexport)
#else
#define MTI_MEDIAFILEIODLL_API  __declspec(dllimport)
#endif

#endif   // Header file


