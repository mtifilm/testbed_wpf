//---------------------------------------------------------------------------

#ifndef RawMediaFrameBufferH
#define RawMediaFrameBufferH
//---------------------------------------------------------------------------

#include "MediaFileIODLL.h"             // Export/Import Defs
#include "bthread.h"
#include "Event.h"
#include "IniFile.h"
#include "ThreadLocker.h"

#include <atomic>
#include <memory>
#include <deque>
using std::deque;
//---------------------------------------------------------------------------

class MTI_MEDIAFILEIODLL_API RawMediaFrameBuffer
{
public:

	RawMediaFrameBuffer(size_t size);
	~RawMediaFrameBuffer();

   int getBufferId() { return _bufferId; }

	void *getBufferPtr() { return _bufferPtr; }
	MTI_UINT32 getBufferSize() { return _bufferSize; }

	MTI_UINT32 getDataSize() { return _dataSize; }
	void setDataSize(MTI_UINT32 newSize)
	{
		MTIassert(newSize <= _bufferSize);
		_dataSize = newSize;
	}

	static size_t GetTotalAllocatedSize() { return _TotalAllocatedSize; }
	static size_t GetTotalBufferCount() { return _TotalBufferCount; }
	static size_t GetFreeListByteCount();
	static size_t GetFreeListBufferCount();
   static void SetLockableMemorySizes(size_t totalNumberOfLockableBytes, size_t freeListAllowance);
   static void SetReallyLockPagesInMemory(bool flag);
   static bool GetReallyLockPagesInMemory();
   static CBHook RanOutOfLockableMemory;

protected:

   int _bufferId = 0;
	void *_bufferPtr = nullptr;
   MTI_UINT32 _bufferSize = 0;
	MTI_UINT32 _dataSize = 0;

private:

	static std::atomic_size_t _TotalAllocatedSize;
	static std::atomic_size_t _TotalBufferCount;
   static std::atomic_int _PreviousBufferId;

	// QQQ this stuff should be in a separate "FreePool" class!
	static void *AllocateBuffer(size_t size);
	static void FreeBuffer(void *bufferPtr, size_t size);
	static void RunPreallocThread(void *vpAppData, void *vpReserved);
	static void *CreateNewBuffer(size_t size);
	static void *AllocateBufferMemory(size_t size);
	static bool DestroyOldestBufferThatDoesntMatchSize(size_t protectedSize);
	static bool DestroyOldestBuffer() { return DestroyOldestBufferThatDoesntMatchSize(0); }
   static void DestroyAllFreeBuffers();
   static void dumpFreeList();

	struct FreeListEntry { void *address; size_t size; };
	static deque<FreeListEntry> _FreeList;
	static CSpinLock _AllocBufferLock;
	static CSpinLock _FreeListLock;
	static CSpinLock _RequestingMemoryReleaseLock;
	static size_t _FreeListByteCount;
	static size_t _FreeListBufferCount;
   static size_t _FreeListByteAllowance;
   static size_t _TotalNumberOfLockableBytes;
   static bool _ReallyLockPagesInMemory;
	static size_t _LastRequestedSize;
	static bool _LastRequestedSizeChanged;
	static Event _PreallocThreadKicker;
	static void *_PreallocThreadId;

};

typedef std::shared_ptr<RawMediaFrameBuffer> RawMediaFrameBufferSharedPtr;

#endif
