//---------------------------------------------------------------------------

#pragma hdrstop

#include "TiffFrameWriter.h"

#include "TiffHeader.h"
#include "err_file.h"
#include "MTImalloc.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
//---------------------------------------------------------------------------

TiffFrameWriter::TiffFrameWriter(const string &filename)
: MediaFrameFileWriterBase(filename)
{
}
//---------------------------------------------------------------------------

namespace
{
	///////////////////////////////////////////////////////////////////////////////
	//
	// Get the Aligned Size for a given file - takes into account the sector
	// size of the disk the file exists on in order to determine the
	// Aligned Size needed for a given file.  This is important in the
	// case of CreateFile() / ReadFile() in Windows where we wish to use
	// the FILE_FLAG_NO_BUFFERING flag to speed up reading of image files
	// by avoiding O/S caching.
	//
	// Return Values:
	// success = the aligned size
	// failure = the passed in size
	//
	// 10/24/2003 - mpr
	//
	///////////////////////////////////////////////////////////////////////////////
	size_t getSectorAlignedSize(const string &filename, size_t fileSize)
	{
		unsigned long bytesPerSector = 0;

		bool success = ::GetBytesPerSector(const_cast<char *>(filename.c_str()), &bytesPerSector);
		if (!success || bytesPerSector == 0)
		{
			bytesPerSector = 512;
		}

		return bytesPerSector * ((fileSize + bytesPerSector - 1) / bytesPerSector);
	}
}
//---------------------------------------------------------------------------

int TiffFrameWriter::encodeImage(MediaFrameBufferSharedPtr frameBufferIn, MediaFrameBufferSharedPtr &frameBufferOut)
{
//	DBCOMMENT("WRITING TIFF");
	MTIassert(frameBufferIn);
	if (!frameBufferIn)
	{
		return FILE_ERROR_READ;
	}

	// Already encoded?
	if (!frameBufferIn->isDecoded())
	{
		frameBufferOut = frameBufferIn;
		return 0;
	}

	//-----------------
	// Get the image format.
	CImageFormat imageFormat = frameBufferIn->getImageFormat();

	//-----------------
	// Find the header. It's either in the frame buffer's headerData or is
	// at the head of the imageData.
	void *headerPtr = nullptr;
	size_t headerSize;
	bool headerAndTrailerAreEmbedded;
	if (frameBufferIn->getHeaderDataSize() > 0)
	{
		headerPtr = frameBufferIn->getHeaderDataPtr();
		headerSize = frameBufferIn->getHeaderDataSize();
		headerAndTrailerAreEmbedded = false;
	}
	else
	{
		headerPtr = frameBufferIn->getRawMediaFrameBuffer()->getBufferPtr();
		headerSize = (size_t) frameBufferIn->getImageDataOffset();
		headerAndTrailerAreEmbedded = true;
	}

//	DBTRACE(headerSize);

	// DECEIVING: The header being swapped means the data is big-endian, so swap if we want to
	// end up as little-endian.
	bool needToSwap16 = TiffHeader::isFileSwapped((const MTI_UINT16 *)headerPtr);
//	bool needToConvertYYYtoY = imageFormat.getPixelComponents() == IF_PIXEL_COMPONENTS_YYY
//										|| imageFormat.getPixelComponents() == IF_PIXEL_COMPONENTS_Y;
//	DBTRACE(needToSwap16);
//	DBTRACE(needToConvertYYYtoY);

 	size_t imageDataLength = imageFormat.getBytesPerField();
//	size_t imageDataLengthOnDisk = needToConvertYYYtoY ? (imageDataLength / 3) : imageDataLength;
	size_t imageDataLengthOnDisk = imageDataLength;

//	// This is so fucked up. We have to compute the size that the image data
//	// will be on disk, all due to the fucking Y-YYY hack!
//	size_t sizeOfImageDataOnDisk = ComputeSizeOfImageDataOnDisk(imageFormat);
   size_t sizeOfImageDataOnDisk = imageFormat.getBytesPerFieldExcAlpha();

	//-----------------
	// Find the trailer. It's either in the frame buffer's trailerData
	// or is at the end of the imageData. Or maybe there is no trailer.
	void *trailerPtr = nullptr;
	size_t trailerSize = 0;
	size_t trailerOffset = headerSize + sizeOfImageDataOnDisk;

	if (headerAndTrailerAreEmbedded)
	{
		trailerPtr = (unsigned char *) frameBufferIn->getRawMediaFrameBuffer()->getBufferPtr() + trailerOffset;
		trailerSize = frameBufferIn->getRawMediaFrameBuffer()->getDataSize() - trailerOffset;
	}
	else
	{
		trailerPtr = frameBufferIn->getTrailerDataPtr();
		trailerSize = frameBufferIn->getTrailerDataSize();
	}

//	DBTRACE(trailerSize);
//	DBTRACE(headerAndTrailerAreEmbedded);

	//-----------------
	// Compute the file size.
	size_t fileSize = headerSize + sizeOfImageDataOnDisk + trailerSize;
//	DBTRACE(fileSize);

	//-----------------
	// Determine swizzling requirements.
	EPixelPacking pixelPacking = imageFormat.getPixelPacking();

	// See if the input frame buffer is OK as-is.
	// QQQ NEED MORE CHECKS HERE!
	if (!needToSwap16) //  && !needToConvertYYYtoY)
	{
		// QQQ Presumably it's OK to share this raw buffer!
		CImageFormat nullImageFormat;
		frameBufferOut = MediaFrameBuffer::CreateSharedBuffer(frameBufferIn->getRawMediaFrameBuffer(), nullImageFormat, false);

		return 0;
	}

	//-----------------
	// Make a new raw buffer to swizzle the data into.
	size_t alignedFileSize = getSectorAlignedSize(_filename, fileSize);
//	DBTRACE(fileSize);
	MTIassert(alignedFileSize < (200 * 1024 * 1024));
	auto rawMediaFrameBufferOut = std::make_shared<RawMediaFrameBuffer>(alignedFileSize);
	rawMediaFrameBufferOut->setDataSize(fileSize);

	//-----------------
	// Copy the header and the trailer, if there is one.
	MTI_UINT8 *dst = (MTI_UINT8 *) rawMediaFrameBufferOut->getBufferPtr();
	MTImemcpy(dst, headerPtr, headerSize);
	if (trailerSize > 0)
	{
		MTImemcpy(dst + trailerOffset, trailerPtr, trailerSize);
	}

	//-----------------
	// Convert YYY to Y if called for and then maybe swap bytes.
	MTI_UINT8 *src = (MTI_UINT8 *) frameBufferIn->getImageDataPtr();
	size_t srcOffset = 0;
	size_t dstOffset = headerSize;

//	if (needToConvertYYYtoY)
//	{
//		CompressYYYToY(imageFormat, src, srcOffset, dst, dstOffset);
//		src = dst;
//		srcOffset = dstOffset;
//	}

	if (needToSwap16)
	{
		SwapINT16((MTI_UINT16*) (src + srcOffset), (MTI_UINT16*) (dst + dstOffset), imageDataLengthOnDisk / sizeof(MTI_UINT16));
	}

	//-----------------
	// It's a wrap!
	CImageFormat nullImageFormat;
	frameBufferOut = MediaFrameBuffer::CreateSharedBuffer(rawMediaFrameBufferOut, nullImageFormat, false);

	return 0;
}
//---------------------------------------------------------------------------


