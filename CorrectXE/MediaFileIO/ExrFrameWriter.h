//---------------------------------------------------------------------------

#ifndef ExrFrameWriterH
#define ExrFrameWriterH
//---------------------------------------------------------------------------

#include "MediaFrameFileWriter.h"
//---------------------------------------------------------------------------

class ExrFrameWriter : public MediaFrameFileWriterBase
{
public:
	ExrFrameWriter(const string &filename);
	~ExrFrameWriter() = default;

	int encodeImage(
			MediaFrameBufferSharedPtr frameBufferIn,
			MediaFrameBufferSharedPtr &frameBufferOut);
};
//---------------------------------------------------------------------------

#endif
