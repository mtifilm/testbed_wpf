#ifndef DpxFrameHackerH
#define DpxFrameHackerH
//---------------------------------------------------------------------------

#include "MediaFileIODLL.h"             // Export/Import Defs

#include "DpxHeader.h"
#include "ImageFormat3.h"
#include "MediaFileIO.h"
#include <string>
using std::string;
//---------------------------------------------------------------------------

class MTI_MEDIAFILEIODLL_API DpxFrameHacker
{
public:
   DpxFrameHacker() {};    // = default; doesn't work
   ~DpxFrameHacker();

   // This can't work until I add an argument pointer to the whole hacking business.
   int hack(const string &filename, FileHackCommand command);

//private:
   int stripAlphaChannel(const string &filePath, const CImageFormat &inputImageFormat);
   int getFrameRate(const string &filePath, float &tvFrameRate, float &filmFrameRate);
   int changeFrameRate(const string &filePath, float newFrameRate);

private:
   string _inputFilePath;
   unsigned char *_inputBuffer = nullptr;
   size_t _sizeOfInputBuffer = 0;
   size_t _sizeOfInputFile = 0;

   string _outputFilePath;
   unsigned char *_outputBuffer = nullptr;
   size_t _sizeOfOutputBuffer = 0;
   size_t _sizeOfOutputFile = 0;

   DpxHeader _dpxHeader;
   unsigned char *_headerBuffer = nullptr;

   int readRgbaFrameFileFromDisk();
   int writeFrameFileToDisk();
};
//---------------------------------------------------------------------------

#endif
