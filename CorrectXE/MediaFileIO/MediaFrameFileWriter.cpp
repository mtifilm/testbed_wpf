//---------------------------------------------------------------------------

#pragma hdrstop

#include "MediaFrameFileWriter.h"

#include "DpxFrameWriter.h"
#include "ExrFrameWriter.h"
#include "TiffFrameWriter.h"

#include "err_file.h"
#include "FileSweeper.h"   // to get disk sector size
//---------------------------------------------------------------------------
#pragma package(smart_init)

//MediaFrameBufferWriteCache MediaFrameFileWriter::Cache;
size_t _getSectorAlignedSize(string filename, size_t possiblyUnalignedSize);
//---------------------------------------------------------------------------

/* static */
MediaFrameFileWriter *MediaFrameFileWriter::CreateMediaFrameFileWriter(const string &filename)
{
	// Brute force
	string fileExt = ::GetFileExt(filename);
	std::transform(fileExt.begin(), fileExt.end(), fileExt.begin(), ::tolower);

	if (fileExt == ".dpx")
	{
		return new DpxFrameWriter(filename);
	}

	if (fileExt == ".tiff" || fileExt == ".tif")
	{
		return new TiffFrameWriter(filename);
	}

	if (fileExt == ".exr")
	{
		return new ExrFrameWriter(filename);
	}

	return nullptr;
}
////---------------------------------------------------------------------------
//
//int MediaFrameFileWriterBase::writeFrameFileAsyncWithDelay(MediaFrameBufferSharedPtr frameBuffer)
//{
//   int retVal = 0;
//
//	if (!Cache.AddFrameBufferToCache(_filename, frameBuffer))
//   {
//		retVal = writeFrameFiletoDiskSync(frameBuffer);
//		CAutoErrorReporter autoErr("writeCachedDataToDisk", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
//
//		retVal = writeFrameFiletoDiskSync(frameBuffer);
//      if (retVal != 0)
//		{
//			autoErr.errorCode = retVal;
//         autoErr.msg << "Modified frame disk write FAILED" << endl
//							<< "    File " << _filename << endl
//                     << "    Error was " << GetSystemErrorMessage(retVal);
//      }
//   }
//
//   return retVal;
//}
//---------------------------------------------------------------------------
//
//int MediaFrameFileWriterBase::writeFrameFileAsync(MediaFrameBufferSharedPtr frameBuffer)
//{
//	return writeFrameFileAsyncWithDelay(frameBuffer);
//}
//---------------------------------------------------------------------------
//
//MediaFrameBufferSharedPtr MediaFrameFileWriterBase::retrieveFrameFileFromCache(const string &filename)
//{
//  return Cache.retrieveFrameFromCache(filename);
//}
//---------------------------------------------------------------------------

namespace
{
   inline string uint2hex(unsigned int value)
   {
      MTIostringstream hexValue;
      hexValue << "0x" << std::right << std::setfill('0') << std::setbase(16) << std::setw(8) << value;
      return hexValue.str();
   }

   bool isInsufficientSystemResourcesError(int windowsErrorCode)
   {
      return windowsErrorCode == ERROR_NO_SYSTEM_RESOURCES           // 1450
            || windowsErrorCode == ERROR_NONPAGED_SYSTEM_RESOURCES   // 1451
            || windowsErrorCode == ERROR_PAGED_SYSTEM_RESOURCES      // 1452
            || windowsErrorCode == ERROR_WORKING_SET_QUOTA           // 1453
            || windowsErrorCode == ERROR_PAGEFILE_QUOTA              // 1454
            || windowsErrorCode == ERROR_COMMITMENT_LIMIT;           // 1455
   }

   string makeTempFrameFileName(const string &originalFileName)
   {
      auto path = GetFilePath(originalFileName);
      auto frameFileName = GetFileName(originalFileName);
      auto extension = GetFileExt(originalFileName); // includes the "."
      auto tempFileName = AddDirSeparator(path) + frameFileName + ".temp" + extension;

      return tempFileName;
   }
}

//---------------------------------------------------------------------------

int MediaFrameFileWriterBase::writeFrameFiletoDiskSync(MediaFrameBufferSharedPtr frameBuffer)
{
	auto rawFrameBuffer = frameBuffer->getRawMediaFrameBuffer();
   auto numberOfRetries = 0;
   const int MaxNumberOfRetries = 2;
   const int RetryDelayInMsecs = 100;
   bool success = true;
   bool tryPlanB = true;
   bool isReadOnly = false;
   MTIostringstream errorLog;
   HANDLE handle;

   do
   {
      success = true;

      // QQQ What should share flags be? 0 or FILE_SHARE_READ or  FILE_SHARE_READ | FILE_SHARE_WRITE ??
      handle = ::CreateFile(
                     _filename.c_str(),
                     GENERIC_WRITE,
                     FILE_SHARE_READ | FILE_SHARE_WRITE,
                     NULL,
                     OPEN_ALWAYS, // Do NOT truncate the file!
                     FILE_FLAG_NO_BUFFERING,
                     NULL);

      DWORD errorCode = ::GetLastError();
      if (handle == INVALID_HANDLE_VALUE)
      {
         success = false;

         string errorMsg = GetSystemErrorMessage(errorCode);
         errorLog << "ERROR: MediaFrameFileWriter: CreateFile FAILED:" << endl
                  << "    Folder " << GetFilePath(_filename) << endl
                  << "    File " << GetFileNameWithExt(_filename) << endl
                  << "    Error " << errorCode << " = " << errorMsg;
         TRACE_0(errout << errorLog.str());

         // Check if file is readonly.
         DWORD fileAttributes = ::GetFileAttributes(_filename.c_str());
         if (fileAttributes == INVALID_FILE_ATTRIBUTES)
         {
            // File doesn't exist. And if we can't create a new file,
            // there is no point to trying to create a temp!
            tryPlanB = false;
            errorLog << "    Cannot create a new file." << endl;
            TRACE_0(errout << "We are tying to create a new file.");
         }
         else if (fileAttributes & FILE_ATTRIBUTE_READONLY)
         {
            // File exists but is unwritable. That means it's also
            // unreplaceable and undeletable, so don't bother creating a temp!
            tryPlanB = false;
            isReadOnly = true;
            errorLog << "    File exists and is marked READONLY!" << endl;
            TRACE_0(errout << "ERROR: File exists and is marked READONLY!");
         }
      }
      else if (errorCode == ERROR_ALREADY_EXISTS)
      {
         // File previously existed, so seek to the beginning.
         // I don't think this is necessary, I think the file pointer
         // always starts at 0 if the file isn't currently open.
         LARGE_INTEGER newFilePointer;
         newFilePointer.QuadPart = (LONGLONG) 0;
         LARGE_INTEGER position;
         position.QuadPart = (LONGLONG) 0;
         ::SetFilePointerEx(handle, position, &newFilePointer, FILE_BEGIN);
      }

      if (!success && !isReadOnly && (numberOfRetries < MaxNumberOfRetries))
      {
         // In case it's some kind of weird sharing issue, retry up to two times!
         // But first we delay to let whatever's happening finish up.
         MTImillisleep(RetryDelayInMsecs);
         TRACE_0(errout << "Retrying the write...");
         errorLog.clear();    // fresh start - clears state
         errorLog.str("");    // fresh start - clears data
      }
   }
   while (!success && !isReadOnly && (numberOfRetries++ < MaxNumberOfRetries));

   // If we are trying to overwrite an existing non-readonly file and that failed,
   // let's try writing a temp file instead, then replacing the original file.
   // We don't bother if the original file is readonly because the ReplaceFile
   // would fail in that case.
   auto createdTempFile = false;
   string tempFrameFileName;
   if (!success && tryPlanB)
   {
      // Try writing it to a temp file instead
      tempFrameFileName = makeTempFrameFileName(_filename);
      handle = ::CreateFile(
                        tempFrameFileName.c_str(),
                        GENERIC_WRITE,
                        FILE_SHARE_READ | FILE_SHARE_WRITE,
                        NULL,
                        CREATE_ALWAYS,
                        FILE_FLAG_NO_BUFFERING,
                        NULL);
      if (handle == INVALID_HANDLE_VALUE)
      {
         DWORD errorCode = ::GetLastError();
         string errorMsg = GetSystemErrorMessage(errorCode);
         success = false;

         MTIostringstream os;
         os << "ERROR: Can't create temp file:" << endl
            << "    Folder " << GetFilePath(tempFrameFileName) << endl
            << "    File " << GetFileNameWithExt(tempFrameFileName) << endl
            << "    Error " << errorCode << " = " << errorMsg;
         errorLog << endl << os.str();
         TRACE_0(errout << os.str());
      }
      else
      {
         createdTempFile = true;
         success = true;
         TRACE_0(errout << "Successfully created temp file!");
      }
   }

   if (!success)
   {
      TRACE_0(errout << "Giving up!");
      theError.setError(FILE_ERROR_OPEN);
      theError.setMessage(errorLog.str());
      return FILE_ERROR_OPEN;
   }

	int retVal = 0;
	DWORD numberOfBytesActuallyWritten = 0;
	DWORD actualFileSize = (DWORD) rawFrameBuffer->getDataSize();
	DWORD roundedSize = (DWORD)_getSectorAlignedSize(_filename, rawFrameBuffer->getDataSize());

	if (!::WriteFile(handle, rawFrameBuffer->getBufferPtr(), roundedSize, &numberOfBytesActuallyWritten, NULL))
	{
		DWORD errorCode = ::GetLastError();
		string errorMsg = GetSystemErrorMessage(errorCode);
      errorLog << "ERROR: MediaFrameFileWriter: WriteFile FAILED:" << endl
               << "    Folder " << GetFilePath(_filename) << endl
               << "    File " << GetFileNameWithExt(_filename) << endl
               << "    Tried to write " << roundedSize << " (" << uint2hex((unsigned) roundedSize) << ") bytes @ "
                       << uint2hex(*(unsigned *) rawFrameBuffer->getBufferPtr()) << endl
               << "    Error " << errorCode << " = " << errorMsg;
		TRACE_0(errout << errorLog.str());
      theError.setError(FILE_ERROR_OPEN);
      theError.setMessage(errorLog.str());
		retVal = FILE_ERROR_WRITE;
	}
   else if (roundedSize != numberOfBytesActuallyWritten)
   {
      MTIostringstream os;
      TRACE_0(errout << "ERROR: MediaFrameFileWriter: WriteFile short write:" << endl
                     << "    Folder " << GetFilePath(_filename) << endl
                     << "    File " << GetFileNameWithExt(_filename) << endl
		               << "    Wrote " << numberOfBytesActuallyWritten << " out of " << roundedSize << " bytes ");
		TRACE_0(errout << os.str());

      // FATAL?? Seems to never happen.
   }

   if (!::CloseHandle(handle))
   {
      DWORD errorCode = ::GetLastError();
      string errorMsg = GetSystemErrorMessage(errorCode);
      TRACE_0(errout << "ERROR: MediaFrameFileWriter: CloseFile 1 FAILED:" << endl
                     << "    Folder " << GetFilePath(_filename) << endl
                     << "    File " << GetFileNameWithExt(_filename) << endl
                     << "    Error " << errorCode << " = " << errorMsg);
      // Non-fatal error
   }

	// Fix the EOF if necessary
	if (retVal == 0 && roundedSize != actualFileSize)
	{
      // QQQ Should flags be FILE_SHARE_READ or FILE_SHARE_READ | FILE_SHARE_WRITE ??
		handle = ::CreateFile(
                     _filename.c_str(),
                     GENERIC_WRITE,
                     FILE_SHARE_READ | FILE_SHARE_WRITE,
                     NULL,
                     OPEN_EXISTING,
                     0,
                     NULL);

		if (handle == INVALID_HANDLE_VALUE)
		{
			DWORD errorCode = ::GetLastError();
			string errorMsg = GetSystemErrorMessage(errorCode);
			TRACE_0(errout << "ERROR: MediaFrameFileWriter: CreateFile for SEEK FAILED:" << endl
                        << "    Folder " << GetFilePath(_filename) << endl
                        << "    File " << GetFileNameWithExt(_filename) << endl
                        << "    Error " << errorCode << " = " << errorMsg);
         // Non-fatal error
		}
		else
		{
			LARGE_INTEGER eof;
			eof.QuadPart = (LONGLONG) actualFileSize;
			LARGE_INTEGER seekPos;
			seekPos.QuadPart = (LONGLONG) 0;

			if (!::SetFilePointerEx(handle, eof, &seekPos, FILE_BEGIN))
			{
				DWORD errorCode = ::GetLastError();
				string errorMsg = GetSystemErrorMessage(errorCode);
				TRACE_0(errout << "ERROR: MediaFrameFileWriter: SEEK TO EOF FAILED" << endl
                           << "    Folder " << GetFilePath(_filename) << endl
                           << "    File " << GetFileNameWithExt(_filename) << endl
                           << "    Tried to change size from " << roundedSize << " (" << uint2hex((unsigned) roundedSize) << ") bytes to "
                                   << actualFileSize << " (" << uint2hex((unsigned) actualFileSize) << ") bytes"  << endl
                           << "    Ended up at " << seekPos.LowPart << " (" << uint2hex((unsigned) seekPos.LowPart) << ") bytes" << endl
                           << "    Error " << errorCode << " = " << errorMsg);
            // Non-fatal error
			}
			else if (!::SetEndOfFile(handle))
         {
            DWORD errorCode = ::GetLastError();
            string errorMsg = GetSystemErrorMessage(errorCode);
            TRACE_0(errout << "ERROR: MediaFrameFileWriter: Failed to set correct file size." << endl
                           << "    Folder " << GetFilePath(_filename) << endl
                           << "    File " << GetFileNameWithExt(_filename) << endl
                           << "    Tried to change size from " << roundedSize << " (" << uint2hex((unsigned) roundedSize)
                                   << ") bytes to "
                                   << actualFileSize << " (" << uint2hex((unsigned) actualFileSize) << ") bytes"  << endl
                           << "    Error " << errorCode << " = " << errorMsg);
            // Non-fatal error
         }
		}

      if (!::CloseHandle(handle))
      {
         DWORD errorCode = ::GetLastError();
         string errorMsg = GetSystemErrorMessage(errorCode);
         TRACE_0(errout << "ERROR: MediaFrameFileWriter: CloseFile 2 FAILED:" << endl
                        << "    Folder " << GetFilePath(_filename) << endl
                        << "    File " << GetFileNameWithExt(_filename) << endl
                        << "    Error " << errorCode << " = " << errorMsg);
         // Non-fatal error
      }
   }

   if (createdTempFile)
   {
      if (retVal == 0)
      {
         success = ::ReplaceFile(
                        _filename.c_str(),
                        tempFrameFileName.c_str(),
                        nullptr,
                        REPLACEFILE_IGNORE_MERGE_ERRORS + REPLACEFILE_IGNORE_ACL_ERRORS,
                        nullptr,
                        nullptr);

         if (!success)
         {
            DWORD errorCode = ::GetLastError();
            string errorMsg = GetSystemErrorMessage(errorCode);
            errorLog << endl
                     << "ERROR: MediaFrameFileWriter: ReplaceFile FAILED:" << endl
                     << "    Folder " << GetFilePath(_filename) << endl
                     << "    File " << GetFileNameWithExt(_filename) << endl
                     << "    Temp File " << GetFileNameWithExt(tempFrameFileName) << endl
                     << "    Error " << errorCode << " = " << errorMsg;
            TRACE_0(errout << errorLog.str());
            theError.setError(FILE_ERROR_REPLACE);
            theError.setMessage(errorLog.str());
            retVal = FILE_ERROR_REPLACE;
         }
         else
         {
            TRACE_0(errout << "Successfully replaced original frame with temp file!");
         }
      }

      if (retVal != 0)
      {
         success = ::DeleteFile(tempFrameFileName.c_str());
         if (!success)
         {
            DWORD errorCode = ::GetLastError();
            string errorMsg = GetSystemErrorMessage(errorCode);
            TRACE_0(errout << "ERROR: MediaFrameFileWriter::writeFrameFiletoDiskSync: DeleteFile FAILED:" << endl
                           << "    Folder " << GetFilePath(tempFrameFileName) << endl
                           << "    File " << GetFileNameWithExt(_filename) << endl
                           << "    Error " << errorCode << " = " << errorMsg);
         }
         else
         {
            TRACE_0(errout << "Successfully cleaned up temp file!");
         }
      }
   }

   return retVal;
}
//---------------------------------------------------------------------------

size_t _getSectorAlignedSize(string filename, size_t possiblyUnalignedSize)
{
	unsigned long bytesPerSector = 0;

	bool success = GetBytesPerSector(const_cast<char *>(filename.c_str()), &bytesPerSector);
	if (!success || bytesPerSector == 0)
	{
		bytesPerSector = 512;
	}

	return bytesPerSector * ((possiblyUnalignedSize + bytesPerSector - 1) / bytesPerSector);
}
//---------------------------------------------------------------------------

//void MediaFrameFileWriterBase::CompressYYYToY(
//	const CImageFormat &imageFormat,
//	MTI_UINT8 *srcBuffer, int srcDataOffset,
//	MTI_UINT8 *dstBuffer, int dstDataOffset)
//{
//   TRACE_3(errout << "X*X*X*X*X*X*X*X*X*X CompressYYYToY BUFFER%4k=" << ((*(int*)&srcBuffer)%4096) << ", OFFSET=" << srcDataOffset);
//   if (imageFormat.getPixelComponents() != IF_PIXEL_COMPONENTS_Y)
//   {
//      return;
//   }
//
//	if (imageFormat.getComponentCount() != 3
//	|| (imageFormat.getBitsPerComponent() != 16 && imageFormat.getBitsPerComponent() != 10))
//	{
//      // QQQ ERROR!
//      TRACE_0(errout << "INTERNAL ERROR: improper use of pixel components == Y");
//      MTIassert(false);
//      return;
//   }
//
//   int numberOfPixels = imageFormat.getTotalFrameHeight() * imageFormat.getTotalFrameWidth();
//
//	switch (imageFormat.getPixelPacking())
//   {
//	case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
//	case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:
//		{
//         MTI_UINT16 *srcPtr = (MTI_UINT16*)(srcBuffer + srcDataOffset);
//         MTI_UINT16 *dstPtr = (MTI_UINT16*)(dstBuffer + dstDataOffset);
//
//         for (int i = 1; i < numberOfPixels; ++i)
//         {
//            dstPtr[i] = srcPtr[i * 3];
//         }
//
//         break;
//      }
//
//   case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L:
//   case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R:
//      {
//         // CAREFUL: in-place compression !!
//         MTI_UINT16 *srcPtr = (MTI_UINT16*)(srcBuffer + srcDataOffset);
//         MTI_UINT32 *dstPtr = (MTI_UINT32*)(dstBuffer + dstDataOffset);
//
//			bool L = imageFormat.getPixelPacking() == IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L;
//         int numberOfPixelsPercent3 = numberOfPixels % 3;
//         int remainingComponents = (numberOfPixels - numberOfPixelsPercent3) * 3;
//         int i;
//         for (i = 0; i < remainingComponents; i += 9)
//         {
//            MTI_UINT32 y1 = ((MTI_UINT32)srcPtr[i + 0] + (MTI_UINT32)srcPtr[i + 1] + (MTI_UINT32)srcPtr[i + 2]) / 3;
//
//            MTI_UINT32 y2 = ((MTI_UINT32)srcPtr[i + 3] + (MTI_UINT32)srcPtr[i + 4] + (MTI_UINT32)srcPtr[i + 5]) / 3;
//
//            MTI_UINT32 y3 = ((MTI_UINT32)srcPtr[i + 6] + (MTI_UINT32)srcPtr[i + 7] + (MTI_UINT32)srcPtr[i + 8]) / 3;
//
//            dstPtr[i / 9] = (y1 & 0x000003FF) << (L ? 2 : 0) | (y2 & 0x000003FF) << (L ? 12 : 10) | (y3 & 0x000003FF) << (L ? 22 : 20);
//         }
//
//         if (numberOfPixelsPercent3 > 0)
//         {
//            MTI_UINT32 y1 = ((MTI_UINT32)srcPtr[i + 0] + (MTI_UINT32)srcPtr[i + 1] + (MTI_UINT32)srcPtr[i + 2]) / 3;
//            MTI_UINT32 y2 = (numberOfPixelsPercent3 == 1)
//                             ? 0
//                             : ((MTI_UINT32)srcPtr[i + 3] + (MTI_UINT32)srcPtr[i + 4] + (MTI_UINT32)srcPtr[i + 5]) / 3;
//            dstPtr[i / 9] = (y1 & 0x000003FF) << (L ? 2 : 0) | (y2 & 0x000003FF) << (L ? 12 : 10);
//         }
//
//         break;
//      }
//
//   default:
//      TRACE_0(errout << "INTERNAL ERROR... Unsupported Y format pixel packing!");
//      break;
//   }
//}
//---------------------------------------------------------------------------

//size_t MediaFrameFileWriterBase::ComputeSizeOfImageDataOnDisk(const CImageFormat &imageFormat)
//{
//   if (imageFormat.getPixelComponents() != IF_PIXEL_COMPONENTS_Y
//   || imageFormat.getComponentCount() != 3)
//   {
//      return imageFormat.getBytesPerFieldExcAlpha();
//   }
//
//   if (imageFormat.getBitsPerComponent() != 10)
//   {
//      return imageFormat.getBytesPerFieldExcAlpha() / 3;
//   }
//
//   // 10 bit
//	MTIassert(imageFormat.getPixelPacking() == IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L
//             || imageFormat.getPixelPacking() == IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R);
//
//   int numberOfPixels = imageFormat.getTotalFrameHeight() * imageFormat.getTotalFrameWidth();
//   return ((numberOfPixels + 2) / 3) * sizeof(MTI_UINT32);
//}
//---------------------------------------------------------------------------

