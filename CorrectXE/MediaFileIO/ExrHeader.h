//---------------------------------------------------------------------------

#ifndef ExrHeaderH
#define ExrHeaderH
//---------------------------------------------------------------------------

#include "machine.h"
#include "ImageFormat3.h"
#include "MTIstringstream.h"
#include "MediaFrameBuffer.h"
#include <string.h>
#include <memory>
using std::string;
using std::shared_ptr;
//---------------------------------------------------------------------------

#define MAGIC_NUMBER_EXR	0x4D42
//---------------------------------------------------------------------------

struct ExrHeader
{
	//---------------------------------------------------------

	// Image width
	int width = -1;

	// Image height
	int height = -1;

	// Is the data uncompressed?
	bool isUncompressed = false;

	// Is RGB or RGBA data?
	bool isRGB = false;

	// Number of channels
	int numChannels = 0;

	// Frame rate
	float frameRate = 0.F;

	// Starting timecode
	string timecode;

	// Shoot date
	string shootDate;

	// Look mod transform function name
	string lookModTransformFunctionName;

	// UUID
	string UUID;
//
//	// File Size
//	size_t fileSize = 0;

	// We need to keep the whole ILM header to facilitate frame writing.
	shared_ptr<void> opaqueIlmHeader;

	//---------------------------------------------------------

	int extractFromFrameBuffer(void *bufferPtr, size_t bufferSize);
	int convertToImageFormat(CImageFormat &imageFormat);
	CTimecode getTimecode();

	string toString();

	//---------------------------------------------------------
};
//---------------------------------------------------------------------------

class ExrOpaqueMetadata : public MediaFrameBuffer::OpaqueMetadata
{
	ExrHeader *_exrHeader;

public:

	ExrOpaqueMetadata() : _exrHeader(new ExrHeader) {}
	~ExrOpaqueMetadata() { delete _exrHeader; }

	ExrHeader *getHeaderPtr() { return _exrHeader; }
};
//---------------------------------------------------------------------------

#endif
