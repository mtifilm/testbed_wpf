//---------------------------------------------------------------------------

#ifndef DpxHeaderH
#define DpxHeaderH
//---------------------------------------------------------------------------

#include "ImageFormat3.h"
#include "machine.h"
#include "MTIstringstream.h"
#include "RawMediaFrameBuffer.h"
#include "timecode.h"

#include <string.h>
#include <memory>
using std::string;
using std::shared_ptr;
//---------------------------------------------------------------------------

#define MAGIC_NUMBER_DPX         0x53445058    // SDPX
#define MAGIC_NUMBER_DPX_SWAPPED 0x58504453    // XPDS

// This size eliminates the "user defined" data
#define DPX_HEADER_SIZE_WITHOUT_USER_DEFINED_CRAP 2048

#define NUM_IMAGE_ELEM 8
//---------------------------------------------------------------------------

struct DpxHeaderImageElement
{
	MTI_UINT32 ulDataSign;
	MTI_UINT32 ulLowDataValue;
	MTI_REAL32 fLowDataQuantity;
	MTI_UINT32 ulHighDataValue;
	MTI_REAL32 fHighDataQuantity;
	MTI_UINT8 ucDescriptor;
	MTI_UINT8 ucTransferCharacteristic;
	MTI_UINT8 ucColorimetricSpec;
	MTI_UINT8 ucBitSize;
	MTI_UINT16 usPacking;
	MTI_UINT16 usEncoding;
	MTI_UINT32 ulOffsetToData;
	MTI_UINT32 ulEndOfLinePadding;
	MTI_UINT32 ulEndOfImagePadding;
	MTI_INT8 caDescription[32];
};

struct DpxHeader
{
	// ---------------------------------------------------------

	MTI_UINT32 ulMagicNumber;
	MTI_UINT32 ulDataOffset;
	MTI_INT8 caVersionNumber[8];
	MTI_UINT32 ulFileSize;
	MTI_UINT32 ulDittoKey;
	MTI_UINT32 ulGenericSectionLength;
	MTI_UINT32 ulIndustrySpecificLength;
	MTI_UINT32 ulUserDefinedLength;
	MTI_INT8 caImageFileName[100];
	MTI_INT8 caCreationDate[24];
	MTI_INT8 caCreator[100];
	MTI_INT8 caProjectName[200];
	MTI_INT8 caRightToUse[200];
	MTI_UINT32 ulEncryptionKey;
	MTI_UINT8 ucaReserved0[104];
	MTI_UINT16 usImageOrientation;
	MTI_UINT16 usNumImageElements;
	MTI_UINT32 ulPixelsPerLine;
	MTI_UINT32 ulLinesPerImage;
	DpxHeaderImageElement imageElement[NUM_IMAGE_ELEM];
	MTI_UINT8 ucaReserved1[52];
	MTI_UINT32 ulHorizontalOffset;
	MTI_UINT32 ulVerticalOffset;
	MTI_REAL32 fHorizontalCenter;
	MTI_REAL32 fVerticalCenter;
	MTI_UINT32 ulHorizontalOrigSize;
	MTI_UINT32 ulVerticalOrigSize;
	MTI_INT8 caSourceImageFileName[100];
	MTI_INT8 caSourceImageDate[24];
	MTI_INT8 caInputDeviceName[32];
	MTI_INT8 caInputDeviceSN[32];
	MTI_UINT16 usBorderValidityLeft,
			usBorderValidityRight, usBorderValidityTop, usBorderValidityBottom;
	MTI_UINT32 ulHorizontalPixelAspect,
			ulVerticalPixelAspect;
	MTI_UINT8 ucaReserved2[28];
	MTI_INT8 caFilmMfgID[2];
	MTI_INT8 caFilmType[2];
	MTI_INT8 caOffsetInPerfs[2];
	MTI_INT8 caFilmEdgeCodePrefix[6];
	MTI_INT8 caFilmEdgeCodeCount[4];
	MTI_INT8 caFormat[32];
	MTI_UINT32 ulFramePosition;
	MTI_UINT32 ulSequenceLength;
	MTI_UINT32 ulHeldCount;
	MTI_REAL32 fFrameRate;
	MTI_REAL32 fShutterAngleDegree;
	MTI_INT8 caFrameID[32];
	MTI_INT8 caSlateInformation[100];
	MTI_UINT8 ucaReserved3[56];
	MTI_UINT32 ulSMPTETimeCode;
	MTI_UINT32 ulSMPTEUserBits;
	MTI_UINT8 ucInterlace;
	MTI_UINT8 ucFieldNumber;
	MTI_UINT8 ucVideoStandard;
	MTI_UINT8 ucZero;
	MTI_REAL32 fHorizontalSamplingRate;
	MTI_REAL32 fVerticalSamplingRate;
	MTI_REAL32 fTemporalSamplingRate;
	MTI_REAL32 fTimeOffsetFromSync;
	MTI_REAL32 fGamma;
	MTI_REAL32 fBlackLevelValue;
	MTI_REAL32 fBlackGain;
	MTI_REAL32 fBreakpoint;
	MTI_REAL32 fReferenceWhiteLevel;
	MTI_REAL32 fIntegrationTime;
	MTI_UINT8 ucaReserved4[76];
	MTI_INT8 caUserID[32];

	//---------------------------------------------------------

	int extractFromFrameBuffer(void *rawFrameBuffer, size_t bufferSize, size_t fileSizeOverride = 0);
   int replaceToFrameBuffer(void *rawFrameBuffer, size_t bufferSize);
	int convertToImageFormat(CImageFormat &imageFormat);
	string toString();
	CTimecode getTimecode();
	bool isFileSwapped();

	//---------------------------------------------------------
};

#endif
