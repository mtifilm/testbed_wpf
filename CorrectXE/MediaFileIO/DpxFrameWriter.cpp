//---------------------------------------------------------------------------

#pragma hdrstop

#include "DpxFrameWriter.h"

#include "DpxHeader.h"
#include "err_file.h"
#include "MTImalloc.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
//---------------------------------------------------------------------------

DpxFrameWriter::DpxFrameWriter(const string &filename)
: MediaFrameFileWriterBase(filename)
{
}
//---------------------------------------------------------------------------

namespace
{
	///////////////////////////////////////////////////////////////////////////////
	//
	// Get the Aligned Size for a given file - takes into account the sector
	// size of the disk the file exists on in order to determine the
	// Aligned Size needed for a given file.  This is important in the
	// case of CreateFile() / ReadFile() in Windows where we wish to use
	// the FILE_FLAG_NO_BUFFERING flag to speed up reading of image files
	// by avoiding O/S caching.
	//
	// Return Values:
	// success = the aligned size
	// failure = the passed in size
	//
	// 10/24/2003 - mpr
	//
	///////////////////////////////////////////////////////////////////////////////
	size_t getSectorAlignedSize(const string &filename, size_t fileSize)
	{
		unsigned long bytesPerSector = 0;

		bool success = ::GetBytesPerSector(const_cast<char *>(filename.c_str()), &bytesPerSector);
		if (!success || bytesPerSector == 0)
		{
			bytesPerSector = 512;
		}

		return bytesPerSector * ((fileSize + bytesPerSector - 1) / bytesPerSector);
	}
}
//---------------------------------------------------------------------------

int DpxFrameWriter::encodeImage(MediaFrameBufferSharedPtr frameBufferIn, MediaFrameBufferSharedPtr &frameBufferOut)
{
	MTIassert(frameBufferIn);
	if (!frameBufferIn)
	{
		return FILE_ERROR_READ;
	}

   auto rawMediaFrameBuffer = frameBufferIn->getRawMediaFrameBuffer();
   MTIassert(rawMediaFrameBuffer);
   if (!rawMediaFrameBuffer)
   {
      return FILE_ERROR_UNEXPECTED_NULL_POINTER;
   }

	// Already encoded?
	if (!frameBufferIn->isDecoded())
	{
		frameBufferOut = frameBufferIn;
		return 0;
	}

	//-----------------
	// Get the image format.
	CImageFormat imageFormat = frameBufferIn->getImageFormat();

	//-----------------
	// Find the "header", which is everything that precedes the first pixel of
   // image data. It's either in the frame buffer's headerData or is
	// at the head of the imageData.
	void *headerPtr = nullptr;
	size_t headerSize;
	bool headerAndTrailerAreEmbedded;
	if (frameBufferIn->getHeaderDataSize() > 0)
	{
		headerPtr = frameBufferIn->getHeaderDataPtr();
		headerSize = frameBufferIn->getHeaderDataSize();
		headerAndTrailerAreEmbedded = false;
	}
	else
	{
		headerPtr = rawMediaFrameBuffer->getBufferPtr();
		headerSize = (size_t) frameBufferIn->getImageDataOffset();
		headerAndTrailerAreEmbedded = true;
	}

//	// This is so fucked up. We have to compute the size that the image data
//	// will be on disk, all due to the fucking Y-YYY hack!
//	size_t sizeOfImageDataOnDisk = ComputeSizeOfImageDataOnDisk(imageFormat);
   size_t sizeOfImageDataOnDisk = imageFormat.getBytesPerFieldExcAlpha();

	//-----------------
	// Find the "trailer", which is everything that follows the last pixel of
   // image data. It's either in the frame buffer's trailerData
	// or is at the end of the imageData. Typically it would be non-empty
   // because it contains a separate alpha image.
	void *trailerPtr = nullptr;
	size_t trailerSize = 0;
	size_t trailerOffsetInFileOnDisk = headerSize + sizeOfImageDataOnDisk;

	if (headerAndTrailerAreEmbedded)
	{
		trailerPtr = (unsigned char *) rawMediaFrameBuffer->getBufferPtr() + trailerOffsetInFileOnDisk;
		trailerSize = rawMediaFrameBuffer->getDataSize() - trailerOffsetInFileOnDisk;
	}
	else
	{
		trailerPtr = frameBufferIn->getTrailerDataPtr();
		trailerSize = frameBufferIn->getTrailerDataSize();
	}

	//-----------------
	// Compute the file size.
	size_t fileSize = headerSize + sizeOfImageDataOnDisk + trailerSize;

	//-----------------
	// Extract the DPX header.
	DpxHeader header;
	int retVal = header.extractFromFrameBuffer(
								headerPtr,
								headerSize,
								fileSize);
	if (retVal)
	{
		MTIassert(false);
		return retVal;
	}

	//-----------------
	// Determine swizzling requirements.
	EPixelPacking pixelPacking = imageFormat.getPixelPacking();

	// DECEIVING: The header being swapped means the data is big-endian, so swap if we want to
	// end up as little-endian. Always swap 16-bit data as 2-byte entities.
	bool needToSwap16 = header.isFileSwapped() && (pixelPacking == IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE);

	// Nasty, nasty, nasty, nasty hack! These represent monochrome data
	// with source packing 3_10Bits_IN_4Bytes_L and 3_10Bits_IN_4Bytes_R,
	// respectively, which the Y->YYY expander wants to receive in
	// little-endian ordering. Bleah!
	bool needToSwap32 = (header.isFileSwapped()
							  && (pixelPacking == IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L
								  || pixelPacking == IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R));

	// These are the "normal" swapping cases. The extractors expect big-endian
	// input so if the file is little endian we do a data swap.
	// QQQ someday eliminate this by implementing the missing extractors and
	// line engines!
	if (!header.isFileSwapped())
	{
		switch (pixelPacking)
		{
		// Anything that's going to end up as two bytes wants to stay little-endian!
		case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L:
		case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R:
		case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_LE:
		case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_BE:
		case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
			break;

		default:
		case IF_PIXEL_PACKING_INVALID:
		case IF_PIXEL_PACKING_1_Half_IN_2Bytes:
		case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes:
		case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:
		case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVSa:
		case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes_LR:
		case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_INT: // INTERNAL FORMAY ONLY
		case IF_PIXEL_PACKING_1_8Bits_IN_2Bytes:
		case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes:
		case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE: // always byte-swapped to LE now!!
			// ERROR - we don't expect these packings in DPX files
			MTIassert(false);
			/* FALL THROUGH */

			// Anything not marked _LE or _BE needs to be byte-swapped!
			// Always byte-swap as 32-bit entities!!
		case IF_PIXEL_PACKING_8Bits_IN_1Byte:
		case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
		case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R:
		case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes: // ?? Your guess is as good as mine!
		case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes: // ?? Your guess is as good as mine!
			needToSwap32 = true;
			break;
		}
   }

//	bool needToConvertYYYtoY = imageFormat.getPixelComponents() == IF_PIXEL_COMPONENTS_YYY
//										|| imageFormat.getPixelComponents() == IF_PIXEL_COMPONENTS_Y;
   bool needToFlipItUpsideDown = header.usImageOrientation == 2;

	// See if the input frame buffer is OK as-is.
	// QQQ NEED MORE CHECKS HERE!
	if (!needToSwap16 && !needToSwap32 && !needToFlipItUpsideDown) // && !needToConvertYYYtoY
	{
		// QQQ Presumably it's OK to share this raw buffer!
		CImageFormat nullImageFormat;
		frameBufferOut = MediaFrameBuffer::CreateSharedBuffer(rawMediaFrameBuffer, nullImageFormat, false);

		return 0;
	}

	//-----------------
	// Make a new raw buffer to swizzle the data into.
	size_t alignedFileSize = getSectorAlignedSize(_filename, fileSize);
	MTIassert(alignedFileSize < (500 * 1024 * 1024));
	auto rawMediaFrameBufferOut = std::make_shared<RawMediaFrameBuffer>(alignedFileSize);
	rawMediaFrameBufferOut->setDataSize(header.ulFileSize);   // ?? why not just fileSize? QQQ

	//-----------------
	// Convert YYY to Y if called for and then maybe swap bytes.
	MTI_UINT8 *src = (MTI_UINT8 *) frameBufferIn->getImageDataPtr();
	size_t imageSrcOffset = 0;
	MTI_UINT8 *dst = (MTI_UINT8 *) rawMediaFrameBufferOut->getBufferPtr();
	size_t imageDstOffset = header.imageElement[0].ulOffsetToData;

//   if (needToConvertYYYtoY)
//   {
//      // For stupid yyy hack, we flip the image here because YYY it's
//      // hard to flip 10-bit mono DPX because rows might start in the middle
//      // of a 32-bit element. YYY is easy because it's always 16-bit components.
//      // NOTE: We're doing this IN PLACE, so the destination is the source!
//      // GETTING A DOUBLE FLIP ON FIRST VERSION CLIP WRITE, SO DISABLE FOR NOW
//      if (needToFlipItUpsideDown)
//      {
////         int nRows = imageFormat.getLinesPerFrame();
////         int pitchInBytes = imageFormat.getFramePitch();
////         MTI_UINT8 *buffer = new MTI_UINT8[pitchInBytes];
////         MTI_UINT8 *srcRowStart = src + imageSrcOffset;
////         // NOTE: We're doing this IN PLACE, so the destination is the source!
////         MTI_UINT8 *dstRowStart = src + imageSrcOffset + ((nRows - 1) * pitchInBytes);
////         for (int row = 0; row < (nRows / 2); ++row)
////         {
////            MTImemcpy(buffer, dstRowStart, pitchInBytes);
////            MTImemcpy(dstRowStart, srcRowStart, pitchInBytes);
////            MTImemcpy(srcRowStart, buffer, pitchInBytes);
////
////            srcRowStart += pitchInBytes;
////            dstRowStart -= pitchInBytes;
////         }
////
////         delete[] buffer;
//         needToFlipItUpsideDown = false;
//      }
//
//      CompressYYYToY(imageFormat, src, imageSrcOffset, dst, imageDstOffset);
//      src = dst;
//      imageSrcOffset = imageDstOffset;
//   }

	int imageDataLengthInBytes = sizeOfImageDataOnDisk;

   if (needToFlipItUpsideDown == false)
   {
      if (needToSwap16)
      {
         SwapINT16((MTI_UINT16*) (src + imageSrcOffset), (MTI_UINT16*) (dst + imageDstOffset), imageDataLengthInBytes / sizeof(MTI_UINT16));
      }

      if (needToSwap32)
      {
         SwapINT32((MTI_UINT32*) (src + imageSrcOffset), (MTI_UINT32*) (dst + imageDstOffset), imageDataLengthInBytes / sizeof(MTI_UINT32));
      }
   }
   else
   {
      // NOTE: If there is a separate alpha matte, we don't have to flip it
      // back because the unflipped version already exists in the "trailer"
      // area. We never change the alpha matte on disk.
      int nRows = imageFormat.getLinesPerFrame();
      MTIassert((imageDataLengthInBytes % nRows) == 0);
      int pitchInBytes = imageDataLengthInBytes / nRows;
      MTI_UINT8 *srcRowStart = src + imageSrcOffset + ((nRows - 1) * pitchInBytes);
      MTI_UINT8 *dstRowStart = dst + imageDstOffset;
      for (int row = 0; row < nRows; ++row)
      {
         if (needToSwap16)
         {
            int pitchInShorts = pitchInBytes / sizeof(MTI_UINT16);;
            SwapINT16(reinterpret_cast<MTI_UINT16 *>(srcRowStart),
                      reinterpret_cast<MTI_UINT16 *>(dstRowStart),
                      pitchInShorts);
         }
         else if (needToSwap32)
         {
            int pitchInLongs = pitchInBytes / sizeof(MTI_UINT32);;
            SwapINT32(reinterpret_cast<MTI_UINT32 *>(srcRowStart),
                      reinterpret_cast<MTI_UINT32 *>(dstRowStart),
                      pitchInLongs);
         }
         else
         {
            MTImemcpy(dstRowStart, srcRowStart, pitchInBytes);
         }

         srcRowStart -= pitchInBytes;
         dstRowStart += pitchInBytes;
      }
   }

	//-----------------
	// Copy the header and the trailer, if there is one.
	MTIassert(headerSize == header.imageElement[0].ulOffsetToData);
	MTImemcpy(dst, headerPtr, headerSize);
	if (trailerSize > 0)
	{
		MTImemcpy(dst + trailerOffsetInFileOnDisk, trailerPtr, trailerSize);
	}

	//-----------------
	// It's a wrap!
	CImageFormat nullImageFormat;
	frameBufferOut = MediaFrameBuffer::CreateSharedBuffer(rawMediaFrameBufferOut, nullImageFormat, false);

	return 0;
}
//---------------------------------------------------------------------------


