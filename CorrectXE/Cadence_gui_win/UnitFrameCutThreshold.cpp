//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UnitFrameCutThreshold.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "VTimeCodeEdit"
#pragma resource "*.dfm"
TFrameCutThreshold *FrameCutThreshold;
//---------------------------------------------------------------------------
__fastcall TFrameCutThreshold::TFrameCutThreshold(TComponent* Owner)
        : TFrame(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFrameCutThreshold::Button1Click(TObject *Sender)
{
  VTimeCodeEdit1->tcP = "00:00:00:00";
  Edit1->Text = "";
}
//---------------------------------------------------------------------------

void __fastcall TFrameCutThreshold::VTimeCodeEdit1TimecodeChange(
      TObject *Sender)
{

  if (fpCutStatistic == 0)
    return;

  VTimeCodeEdit *tce = (VTimeCodeEdit *)Sender;

  // get the frame index
  int iIndex = tce->tcP.getFrameCount() - tcInPoint.getFrameCount() + iHandle;

  if (iIndex > iNFrame || iIndex < 0)
   {
    Edit1->Text = "";
    return;
   }

  int iThresh;
  iThresh = fpCutStatistic[iIndex] * 100.;
  char caTmp[16];
  sprintf (caTmp, "%d", iThresh);

  Edit1->Text = caTmp;

}
//---------------------------------------------------------------------------


void TFrameCutThreshold::setInPoint (CTimecode tcNew, int Handle, int NFrame)
{
  tcInPoint = tcNew;
  VTimeCodeEdit1->tcP = tcInPoint;

  iHandle = Handle;
  iNFrame = NFrame;

  Button1Click(NULL);
}

void TFrameCutThreshold::setCutStatistic (float *fpNew)
{
  fpCutStatistic = fpNew;
}
