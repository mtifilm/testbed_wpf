//---------------------------------------------------------------------------


#ifndef UnitFrameCutThresholdH
#define UnitFrameCutThresholdH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "VTimeCodeEdit.h"
//---------------------------------------------------------------------------
class TFrameCutThreshold : public TFrame
{
__published:	// IDE-managed Components
        VTimeCodeEdit *VTimeCodeEdit1;
        TEdit *Edit1;
        TButton *Button1;
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall VTimeCodeEdit1TimecodeChange(TObject *Sender);
private:	// User declarations


  CTimecode tcInPoint;
  float *fpCutStatistic;
  int iHandle, iNFrame;

public:		// User declarations
        __fastcall TFrameCutThreshold(TComponent* Owner);


  void setInPoint (CTimecode tcNew, int Handle, int NFrame);
  void setCutStatistic (float *fpNew);
};
//---------------------------------------------------------------------------
extern PACKAGE TFrameCutThreshold *FrameCutThreshold;
//---------------------------------------------------------------------------
#endif
