
#include "CadenceBinMgrGUIInterface.h"
#include "BinManagerGUIUnit.h"
#include "UnitCadence.h"


//---------------------------------------------------------------------------
// Bin Manager GUI Interface Implementation


CCadenceBinMgrGUIInterface::CCadenceBinMgrGUIInterface()
  : CBinMgrGUIInterface()
{
}

void CCadenceBinMgrGUIInterface::ShowBinManagerGUI(bool show)
{
   // 2nd argument makes Bin Manager fsNormal
   CBinMgrGUIInterface::ShowBinManagerGUI(show, false);
   if (FormCadence->getCurrentClipFileNameWithExt() == "")
    {
     BinManagerForm->CancelHighlightClip ();
    }
   else
    {
     BinManagerForm->HighlightClip(FormCadence->getCurrentClipFileNameWithExt(),
		true);
    }
}

void CCadenceBinMgrGUIInterface::ClipWillBeDeleted(const string& clipFileName)
{
  // if this is the loaded clip, remove it from the GUI
  if (clipFileName == FormCadence->getCurrentClipFileNameWithExt())
   {
    // this clip is currently being used by the UI.  Close it first
    FormCadence->CloseClip ();
   }

}

