//---------------------------------------------------------------------------

#include <vcl.h> 
#pragma hdrstop
//---------------------------------------------------------------------------
USEFORM("UnitCadence.cpp", FormCadence);
USEFORM("UnitFrameCutThreshold.cpp", FrameCutThreshold); /* TFrame: File Type */
USEFORM("C:\MTIBorland\Repository\MTIUNIT.cpp", MTIForm);
USEFORM("UnitAbout.cpp", FormAbout);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TFormCadence), &FormCadence);
                 Application->CreateForm(__classid(TMTIForm), &MTIForm);
                 Application->CreateForm(__classid(TFormAbout), &FormAbout);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        catch (...)
        {
                 try
                 {
                         throw Exception("");
                 }
                 catch (Exception &exception)
                 {
                         Application->ShowException(&exception);
                 }
        }
        return 0;
}
//---------------------------------------------------------------------------
