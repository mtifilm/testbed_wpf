//---------------------------------------------------------------------------

#ifndef UnitCadenceH
#define UnitCadenceH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include "UnitFrameCutThreshold.h"
#include <ComCtrls.hpp>
#include <Menus.hpp>
#include "TB2Dock.hpp"
#include "TB2Item.hpp"
#include "TB2Toolbar.hpp"
#include "MTIUNIT.h"

#define STATUS_NONE 0
#define STATUS_FINISHED 1
#define STATUS_ABORT 2

#define BUF_ALIGN 0x1000  // 4096 bytes

#define COL_SRC_IN 0
#define COL_SRC_LABEL 1
#define COL_DST_IN 2
#define COL_DST_LABEL 3
#define COL_PROCESS 4
#define COL_THRESH 5
#define COL_INDEX 6

// structure to organize information about each shot
typedef struct
 {
  int
    iSrcIndex,
    iSrcFrameCount,
    iSrcLabel,
    iDstIndex,
    iFirstSrc,  // first source frame used to build 32 sequence
    iLastSrc,  // last source frame used to build 32 sequence
    iDstLabel,
    iDstFrameCount,
    iDstFrameCountNo32;  // number of film frames in the shot

  bool
    bProcess;
 } SShotInfo;


//---------------------------------------------------------------------------
class TFormCadence : public TMTIForm
{
__published:	// IDE-managed Components
        TButton *ButtonPrevious;
        TButton *ButtonNext;
        TBitBtn *BitBtn1;
        TPanel *Panel1;
        TLabel *Label1;
        TGroupBox *GroupBox1;
        TLabel *Label2;
        TLabel *Label3;
        TFrameCutThreshold *FrameCutThreshold1;
        TFrameCutThreshold *FrameCutThreshold2;
        TFrameCutThreshold *FrameCutThreshold3;
        TFrameCutThreshold *FrameCutThreshold4;
        TFrameCutThreshold *FrameCutThreshold5;
        TBevel *Bevel1;
        TEdit *EditThreshold;
        TLabel *Label4;
        TButton *ButtonFactory;
        TButton *ButtonAuto;
        TPanel *Panel2;
        TLabel *Label5;
        TListView *ListView1;
        TTBToolbar *TBToolbar1;
        TTBSubmenuItem *MenuAbout;
        TTBItem *itemAbout;
        TPanel *Panel3;
        TLabel *Label6;
        TLabel *Label7;
        TLabel *Label8;
        TPopupMenu *PopupMenu1;
        TMenuItem *DisableProcessing1;
        TMenuItem *EnableProcessing1;
        TMenuItem *N1;
        TMenuItem *SetSrcLabeltoAa;
        TMenuItem *SetSrcLabeltoBb;
        TMenuItem *SetSrcLabeltoBc;
        TMenuItem *SetSrcLabeltoCd;
        TMenuItem *SetSrcLabeltoDd;
        TMenuItem *SetSrcLabeltoVv;
        TGroupBox *GroupBox2;
        TLabel *Label9;
        TLabel *Label10;
        TLabel *Label11;
        TLabel *Label12;
        TPanel *Panel0;
        TLabel *Label14;
        TButton *ButtonOpenClip;
        TButton *ButtonCancelCadence;
        TMenuItem *N2;
        TMenuItem *SetDstLabeltoBb;
        TMenuItem *SetDstLabeltoBc;
        TMenuItem *SetDstLabeltoCd;
        TMenuItem *SetDstLabeltoDd;
        TMenuItem *SetDstLabeltoAa;
        void __fastcall FormShow(TObject *Sender);
        void __fastcall ButtonPreviousClick(TObject *Sender);
        void __fastcall ButtonNextClick(TObject *Sender);
        void __fastcall itemAboutClick(TObject *Sender);
        void __fastcall ButtonFactoryClick(TObject *Sender);
        void __fastcall ButtonAutoClick(TObject *Sender);
        void __fastcall ButtonOpenClipClick(TObject *Sender);
        void __fastcall ButtonCancelCadenceClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall ListView1CustomDrawSubItem(TCustomListView *Sender,
          TListItem *Item, int SubItem, TCustomDrawState State,
          bool &DefaultDraw);
        void __fastcall SetSrcLabelClick(TObject *Sender);
        void __fastcall SetDstLabelClick(TObject *Sender);
protected:
    virtual void StartForm(void);

private:	// User declarations
int iCurrentPanel;
void SwitchPanel ();

string strClip, strBin;

CCadenceBinMgrGUIInterface *binMgrGUIInterface;

string strCurrentClipFileNameWithExt;

ClipSharedPtr clip;
CVideoFrameList *vflp;

int analyzeDiffFile (bool bInitialCall);

int *diffFrameLabels;
float *cutStatistic;
int *ipFrame0;
int *ipFrame1;

void InitListView ();

int SaveCadenceFile ();
int LoadCadenceFile ();

AnsiString EditThresholdText;

void ReleaseStorage ();

vector <SShotInfo> listShotInfo;

void BuildShotList ();
void AssignDstFrameLabel (int iLabel, unsigned int iStartEvent);
void IncrementLabel (int *ipLabel);

void BuildFieldList ();

vector <int> listDstLabel;


bool bIsLicensed;
string strStartupFailedMessage;

public:		// User declarations
        __fastcall TFormCadence(TComponent* Owner);

        
string getCurrentClipFileNameWithExt ();

void CloseClip ();




};
//---------------------------------------------------------------------------
extern PACKAGE TFormCadence *FormCadence;
//---------------------------------------------------------------------------
#endif
