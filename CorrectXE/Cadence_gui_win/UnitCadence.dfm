object FormCadence: TFormCadence
  Left = 495
  Top = 281
  Caption = 'MTI Cadence Repair - no clip loaded'
  ClientHeight = 732
  ClientWidth = 681
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel3: TPanel
    Left = 8
    Top = 32
    Width = 673
    Height = 641
    TabOrder = 6
    object Label6: TLabel
      Left = 32
      Top = 32
      Width = 256
      Height = 20
      Caption = 'The cadence repair is complete '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object Panel1: TPanel
    Left = 8
    Top = 32
    Width = 673
    Height = 641
    TabOrder = 3
    object Label1: TLabel
      Left = 32
      Top = 32
      Width = 537
      Height = 20
      Caption = 
        'Establish parameters to control the behavior of the cadence edit' +
        'or '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object GroupBox1: TGroupBox
      Left = 128
      Top = 88
      Width = 313
      Height = 337
      Caption = 'Cut Threshold Calculator'
      TabOrder = 0
      object Label2: TLabel
        Left = 32
        Top = 176
        Width = 47
        Height = 13
        Caption = 'Timecode'
      end
      object Label3: TLabel
        Left = 136
        Top = 176
        Width = 47
        Height = 13
        Caption = 'Threshold'
      end
      object Bevel1: TBevel
        Left = 24
        Top = 152
        Width = 265
        Height = 3
      end
      object Label4: TLabel
        Left = 32
        Top = 96
        Width = 66
        Height = 13
        Caption = 'Cut Threshold'
      end
      object Label7: TLabel
        Left = 32
        Top = 24
        Width = 255
        Height = 13
        Caption = 'Enter a cut threshold.  If you don'#39't know what value to'
      end
      object Label8: TLabel
        Left = 32
        Top = 40
        Width = 249
        Height = 13
        Caption = 'use, enter the timecodes of some known cuts  in the '
      end
      object Label12: TLabel
        Left = 32
        Top = 56
        Width = 46
        Height = 13
        Caption = 'list below.'
      end
      object EditThreshold: TEdit
        Left = 32
        Top = 112
        Width = 81
        Height = 21
        TabOrder = 0
        Text = '90'
      end
      object ButtonFactory: TButton
        Left = 128
        Top = 112
        Width = 65
        Height = 21
        Hint = 'Use factory default'
        Caption = 'Factory'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = ButtonFactoryClick
      end
      object ButtonAuto: TButton
        Left = 211
        Top = 112
        Width = 65
        Height = 21
        Hint = 'Automatically use threshold from list below'
        Caption = 'Automatic'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = ButtonAutoClick
      end
    end
  end
  object Panel0: TPanel
    Left = 8
    Top = 32
    Width = 673
    Height = 641
    TabOrder = 5
    object Label14: TLabel
      Left = 32
      Top = 32
      Width = 285
      Height = 20
      Caption = 'Open a clip to begin cadence repair'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ButtonOpenClip: TButton
      Left = 264
      Top = 152
      Width = 129
      Height = 25
      Caption = 'Open a clip'
      TabOrder = 0
      OnClick = ButtonOpenClipClick
    end
    object ButtonCancelCadence: TButton
      Left = 264
      Top = 208
      Width = 129
      Height = 25
      Caption = 'Cancel Cadence Repair'
      TabOrder = 1
      OnClick = ButtonCancelCadenceClick
    end
  end
  object Panel2: TPanel
    Left = 8
    Top = 32
    Width = 673
    Height = 641
    TabOrder = 4
    object Label5: TLabel
      Left = 32
      Top = 32
      Width = 522
      Height = 20
      Caption = 'Review the events which will be processed by the cadence editor '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ListView1: TListView
      Left = 40
      Top = 88
      Width = 553
      Height = 433
      Columns = <
        item
          Caption = 'Event'
        end
        item
          Caption = 'Src In Point'
          Width = 75
        end
        item
          Caption = 'Src Label'
          Width = 60
        end
        item
          Caption = 'Dst In Point'
          Width = 75
        end
        item
          Caption = 'Dst Label'
          Width = 60
        end
        item
          Caption = 'Process'
        end
        item
          Caption = 'Cut Thresh'
          Width = 75
        end
        item
          Caption = 'Index'
          Width = 75
        end>
      MultiSelect = True
      RowSelect = True
      PopupMenu = PopupMenu1
      TabOrder = 0
      ViewStyle = vsReport
      OnCustomDrawSubItem = ListView1CustomDrawSubItem
    end
    object GroupBox2: TGroupBox
      Left = 80
      Top = 552
      Width = 305
      Height = 80
      Caption = 'Color Coding'
      TabOrder = 1
      object Label9: TLabel
        Left = 16
        Top = 24
        Width = 112
        Height = 13
        Caption = 'Event will be processed'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label10: TLabel
        Left = 16
        Top = 40
        Width = 269
        Height = 13
        Caption = 'Event will not be processed because of operator override'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 54741
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label11: TLabel
        Left = 16
        Top = 56
        Width = 133
        Height = 13
        Caption = 'Event will not be processed '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
    end
  end
  object ButtonPrevious: TButton
    Left = 24
    Top = 696
    Width = 75
    Height = 25
    Caption = '&Previous'
    TabOrder = 0
    OnClick = ButtonPreviousClick
  end
  object ButtonNext: TButton
    Left = 112
    Top = 696
    Width = 75
    Height = 25
    Caption = '&Next'
    TabOrder = 1
    OnClick = ButtonNextClick
  end
  object BitBtn1: TBitBtn
    Left = 256
    Top = 696
    Width = 75
    Height = 25
    Kind = bkClose
    NumGlyphs = 2
    TabOrder = 2
  end
  object PopupMenu1: TPopupMenu
    Left = 504
    Top = 208
    object DisableProcessing1: TMenuItem
      Caption = 'Disable Processing'
    end
    object EnableProcessing1: TMenuItem
      Caption = 'Enable Processing'
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object SetSrcLabeltoAa: TMenuItem
      Caption = 'Set Src Label to "Aa"'
      OnClick = SetSrcLabelClick
    end
    object SetSrcLabeltoBb: TMenuItem
      Caption = 'Set Src Label to "Bb"'
      OnClick = SetSrcLabelClick
    end
    object SetSrcLabeltoBc: TMenuItem
      Caption = 'Set Src Label to "Bc"'
      OnClick = SetSrcLabelClick
    end
    object SetSrcLabeltoCd: TMenuItem
      Caption = 'Set Src Label to "Cd"'
      OnClick = SetSrcLabelClick
    end
    object SetSrcLabeltoDd: TMenuItem
      Caption = 'Set Src Label to "Dd"'
      OnClick = SetSrcLabelClick
    end
    object SetSrcLabeltoVv: TMenuItem
      Caption = 'Set Src Label to "Vv"'
      OnClick = SetSrcLabelClick
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object SetDstLabeltoAa: TMenuItem
      Caption = 'Set Dst Label to "Aa"'
      OnClick = SetDstLabelClick
    end
    object SetDstLabeltoBb: TMenuItem
      Caption = 'Set Dst Label to "Bb"'
      OnClick = SetDstLabelClick
    end
    object SetDstLabeltoBc: TMenuItem
      Caption = 'Set Dst Label to "Bc"'
      OnClick = SetDstLabelClick
    end
    object SetDstLabeltoCd: TMenuItem
      Caption = 'Set Dst Label to "Cd"'
      OnClick = SetDstLabelClick
    end
    object SetDstLabeltoDd: TMenuItem
      Caption = 'Set Dst Label to "Dd"'
      OnClick = SetDstLabelClick
    end
  end
end
