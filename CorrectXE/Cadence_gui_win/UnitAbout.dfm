object FormAbout: TFormAbout
  Left = 485
  Top = 352
  Width = 675
  Height = 390
  Caption = 'About'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 176
    Top = 56
    Width = 273
    Height = 32
    Caption = 'MTI Cadence Repair'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 248
    Top = 104
    Width = 120
    Height = 25
    Caption = 'Version 2.0'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 224
    Top = 136
    Width = 167
    Height = 25
    Caption = 'December 2005'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 32
    Top = 320
    Width = 340
    Height = 13
    Caption = 
      'U.S. Patent 6,542,199, MTI Film, L.L.C. Copyright MTI Film, L.L.' +
      'C., 2006'
  end
end
