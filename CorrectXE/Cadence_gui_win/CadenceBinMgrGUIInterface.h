#ifndef CADENCE_BIN_MGR_GUI_INTERFACE_H
#define CADENCE_BIN_MGR_GUI_INTERFACE_H


#include "BinMgrGUIInterface.h"

// Bin Manager GUI Interface
class CCadenceBinMgrGUIInterface : public CBinMgrGUIInterface
{
public:
   CCadenceBinMgrGUIInterface();

   void ShowBinManagerGUI(bool show);
   void ClipWillBeDeleted (const string &clipFileName);
};

#endif
