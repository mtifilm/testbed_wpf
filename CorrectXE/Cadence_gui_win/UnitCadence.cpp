//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop


#include "MTIWinInterface.h"
#include "CadenceBinMgrGUIInterface.h"
#include "BinManagerGUIUnit.h"
#include "ClipAPI.h"

#include "UnitCadence.h"
#include "UnitAbout.h"

#include "FileSweeper.h"

#include "IniFile.h"  // for TRACE

#include "MetaData.h"


#include <io.h>
#include <fcntl.h>
#include <dos.h>

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "UnitFrameCutThreshold"
#pragma link "TB2Dock"
#pragma link "TB2Item"
#pragma link "TB2Toolbar"
#pragma link "MTIUNIT"
#pragma resource "*.dfm"
TFormCadence *FormCadence;

// Encryption for "CORRECT-CADENCE"
MTI_UINT32 FEATURE_CORRECT_CADENCE[] = {0x554c910, 0xbfd829d, 0xa13e7166, 0x60f3e30e, 0xa46ae470, 0x0b059a22};

#ifndef NO_LICENSING
CRsrcCtrl License;
#endif

//---------------------------------------------------------------------------
__fastcall TFormCadence::TFormCadence(TComponent* Owner)
        : TMTIForm(Owner)
{
#ifndef NO_LICENSING
   CRsrcCtrl rsrcCtrl;       // Protocol: Must use local instance!!
   bIsLicensed = rsrcCtrl.IsFeatureLicensed(FEATURE_CORRECT_CADENCE);
#else
   bIsLicensed = false;
#endif
   strStartupFailedMessage = "Cadence Repaire is not licensed on this computer\nPlease contact MTI-Film for more information\n";
   strStartupFailedMessage += License.LastErrorMessage();
   LoadMainIcons();

   PostMessage(FormCadence->Handle, CW_START_PROGRAM, 0, 0);
}
//---------------------------------------------------------------------------
void __fastcall TFormCadence::FormShow(TObject *Sender)
{

  // set the tags on the various panels
  Panel0->Tag = 0;
  Panel1->Tag = 1;
  Panel2->Tag = 2;
  Panel3->Tag = 3;

  // put up the zero panel
  iCurrentPanel = 0;
  SwitchPanel ();

  binMgrGUIInterface = new CCadenceBinMgrGUIInterface;

  clip = 0;
  vflp = 0;

  FrameCutThreshold1->Button1Click(NULL);
  FrameCutThreshold2->Button1Click(NULL);
  FrameCutThreshold3->Button1Click(NULL);
  FrameCutThreshold4->Button1Click(NULL);
  FrameCutThreshold5->Button1Click(NULL);

  diffFrameLabels = 0;
  cutStatistic = 0;
  ipFrame0 = 0;
  ipFrame1 = 0;

  EditThresholdText = "";
}
//---------------------------------------------------------------------------

void __fastcall TFormCadence::ButtonPreviousClick(TObject *Sender)
{
  iCurrentPanel--;
  if (iCurrentPanel < 0)
    iCurrentPanel = 0;

  SwitchPanel ();
}
//---------------------------------------------------------------------------

void __fastcall TFormCadence::ButtonNextClick(TObject *Sender)
{
  if (clip == 0)
   {
    _MTIErrorDialog(Handle, "You must load a clip to move to the next step.");
    return;
   }

  iCurrentPanel++;
  if (iCurrentPanel > 4)
    iCurrentPanel = 4;

  SwitchPanel ();

}
//---------------------------------------------------------------------------


string TFormCadence::getCurrentClipFileNameWithExt ()
{
  return strCurrentClipFileNameWithExt;
}

void __fastcall TFormCadence::itemAboutClick(TObject *Sender)
{
  FormAbout->ShowModal ();
}
//---------------------------------------------------------------------------

void TFormCadence::SwitchPanel()
{
  int iRet;

  // make the proper panels visible
  Panel0->Visible = (iCurrentPanel == Panel0->Tag);
  Panel1->Visible = (iCurrentPanel == Panel1->Tag);
  Panel2->Visible = (iCurrentPanel == Panel2->Tag);
  Panel3->Visible = (iCurrentPanel == Panel3->Tag);


  // make the previous/next buttons active
  ButtonPrevious->Enabled = (iCurrentPanel != 0);
  ButtonNext->Enabled = (iCurrentPanel != 3);


  // perform the required action
  switch (iCurrentPanel)
   {
    case 0:
    break;
    case 1:
    break;
    case 2:
      InitListView ();
    break;
    case 3:
      // build the field list
      BuildFieldList ();

      // save the .crt file
      iRet = SaveCadenceFile ();
      if (iRet)
       {
        _MTIErrorDialog(Handle, "Unable to save the cadence instruction file");
        return;
       }

      // tell the clip that the cadence has been repaired
      iRet = clip->setCadenceRepairFlag (true);
      if (iRet)
       {
        _MTIErrorDialog(Handle, "Unable to set the cadence repair flag in the clip.");
       }
    break;
   }

}

void __fastcall TFormCadence::ButtonFactoryClick(TObject *Sender)
{
  EditThreshold->Text = "90";
}
//---------------------------------------------------------------------------


void TFormCadence::CloseClip ()
{
  if (clip == 0)
   {
    CBinManager bmBinMgr;
    bmBinMgr.closeClip (clip);
    clip = 0;
   }

  strCurrentClipFileNameWithExt = "";
  Caption = "MTI Cadence Repair - no clip loaded";
}

#define INFINITY_THRESH 1000000
void __fastcall TFormCadence::ButtonAutoClick(TObject *Sender)
{
  // read all the threshold values and compute the smallest
  int iThresh, iMinThresh;
  iMinThresh = INFINITY_THRESH;

  if (sscanf (FrameCutThreshold1->Edit1->Text.c_str(), "%d", &iThresh) == 1)
   {
    if (iThresh < iMinThresh)
      iMinThresh = iThresh;
   }
  if (sscanf (FrameCutThreshold2->Edit1->Text.c_str(), "%d", &iThresh) == 1)
   {
    if (iThresh < iMinThresh)
      iMinThresh = iThresh;
   }
  if (sscanf (FrameCutThreshold3->Edit1->Text.c_str(), "%d", &iThresh) == 1)
   {
    if (iThresh < iMinThresh)
      iMinThresh = iThresh;
   }
  if (sscanf (FrameCutThreshold4->Edit1->Text.c_str(), "%d", &iThresh) == 1)
   {
    if (iThresh < iMinThresh)
      iMinThresh = iThresh;
   }
  if (sscanf (FrameCutThreshold5->Edit1->Text.c_str(), "%d", &iThresh) == 1)
   {
    if (iThresh < iMinThresh)
      iMinThresh = iThresh;
   }

  if (iMinThresh == INFINITY_THRESH)
   {
    _MTIErrorDialog(Handle, "Please enter the timecodes of some known cuts\n"
    "in the list below.");
    return;
   }

  char caMessage[16];
  sprintf (caMessage, "%d", iMinThresh);
  EditThreshold->Text = caMessage;
}
//---------------------------------------------------------------------------


void __fastcall TFormCadence::ButtonOpenClipClick(TObject *Sender)
{
#ifndef NO_LICENSING
  CRsrcCtrl rsrcCtrl;       // Protocol: Must use local instance!!
  if (!rsrcCtrl.IsFeatureLicensed(FEATURE_CORRECT_CADENCE)) return;
#endif
  binMgrGUIInterface->ShowBinManagerGUI (false);
  if (BinManagerForm->ShowModal() == mrOk)
   {
    // load the selected clip
    StringList sl = BinManagerForm->SelectedClipNames();
    if (!sl.empty())
     {
      // start the spinning arrows
      CBusyCursor Busy(true);

      // close the current clip
      CloseClip ();


      CBinManager bmBinMgr;
      int iRet;

      iRet = bmBinMgr.splitClipFileName (sl[0], strBin, strClip);
      if (iRet)
       {
        _MTIErrorDialog(Handle, "Unable to parse the clip name");
        return;
       }

      clip = bmBinMgr.openClip (strBin, strClip, &iRet);
      if (iRet)
       {
        char caMessage[64];
        sprintf (caMessage, "Opening the media returned ERROR:  %d", iRet);
        _MTIErrorDialog(Handle, caMessage);
        return;
       }

      strCurrentClipFileNameWithExt = bmBinMgr.makeClipFileNameWithExt (strBin, strClip);

      // update the caption
      string strTmp = "MTI Cadence Repair - ";
      strTmp += sl[0];

      Caption = strTmp.c_str();


      // get the video frame list
      vflp = clip->getVideoFrameList (VIDEO_SELECT_NORMAL, FRAMING_SELECT_VIDEO);
      if (vflp == 0)
       {
        _MTIErrorDialog(Handle, "The clip does not have the required video frame list");
        CloseClip ();
        return;
       }

      // is this format supported by the cadence editor
      EImageFormatType ift = vflp->getImageFormat()->getImageFormatType();

      if (ift != IF_TYPE_525_5994 && ift != IF_TYPE_525_5994_DVS)
       {
        _MTIErrorDialog(Handle, "The format of this clip is not supported by the cadence repair tool.");
        CloseClip ();
        return;
       }

      // read in the difference values
#ifndef NO_LICENSING
      CRsrcCtrl rsrcCtrl;       // Protocol: Must use local instance!!
      if (!rsrcCtrl.IsFeatureLicensed(FEATURE_CORRECT_CADENCE))
#endif
       {
         _MTIErrorDialog(Handle, "Unable to analyze the pesudo difference file.");
         CloseClip ();
         return;
       }

      iRet = analyzeDiffFile (true);
      if (iRet)
       {
        _MTIErrorDialog(Handle, "Unable to analyze the field difference file.");
        CloseClip ();
        return;
       }


      
      // set the timecode widgets
      FrameCutThreshold1->setInPoint (clip->getInTimecode(),
        vflp->getFrameIndex(clip->getInTimecode()), vflp->getTotalFrameCount());
      FrameCutThreshold2->setInPoint (clip->getInTimecode(),
        vflp->getFrameIndex(clip->getInTimecode()), vflp->getTotalFrameCount());
      FrameCutThreshold3->setInPoint (clip->getInTimecode(),
        vflp->getFrameIndex(clip->getInTimecode()), vflp->getTotalFrameCount());
      FrameCutThreshold4->setInPoint (clip->getInTimecode(),
        vflp->getFrameIndex(clip->getInTimecode()), vflp->getTotalFrameCount());
      FrameCutThreshold5->setInPoint (clip->getInTimecode(),
        vflp->getFrameIndex(clip->getInTimecode()), vflp->getTotalFrameCount());


     }
   }
}
//---------------------------------------------------------------------------

void __fastcall TFormCadence::ButtonCancelCadenceClick(TObject *Sender)
{
  if (clip == 0)
   {
    _MTIErrorDialog(Handle, "You must load a clip to cancel a cadence repair.");
    return;
   }

  int iRet = clip->setCadenceRepairFlag (false);
  if (iRet)
   {
    _MTIErrorDialog(Handle, "Unable to clear the cadence repair flag in the clip.");
   }

  // delete the .crt file
  CBinManager binMgr;
  string cadenceFileName =
    binMgr.makeRepairedCadenceFilePath(strBin, strClip);

  if (DoesFileExist (cadenceFileName))
   {
    if (unlink (cadenceFileName.c_str()) == -1)
     {
      _MTIErrorDialog(Handle, "Unable to delete the cadence instruction file");
     }
   }
}
//---------------------------------------------------------------------------

int TFormCadence::analyzeDiffFile(bool bInitialCall)
{
   int retVal;
   EPulldown pulldown = IF_PULLDOWN_SWAPPED_FIELD_3_2;  // 525 material
   CBinManager binMgr;
   string diffFileName = binMgr.makeClipFileName(strBin, strClip) + ".diff";
   string cadenceFileName =
     binMgr.makeRepairedCadenceFilePath(strBin, strClip);

   listDstLabel.clear();

   int videoFrameCount = vflp->getTotalFrameCount();

   ReleaseStorage ();

   CMetaData *metaData = 0;
   try
      {
      // Allocate buffers for frame labels
      diffFrameLabels = new int[videoFrameCount];
      cutStatistic = new float[videoFrameCount];
      ipFrame0 = new int[videoFrameCount];
      ipFrame1 = new int[videoFrameCount];
      metaData = new CMetaData(diffFileName);
      }
   catch (...)
      {
      // Memory allocation failed
      delete metaData;
      delete[] diffFrameLabels;
      diffFrameLabels = 0;
      delete[] cutStatistic;
      cutStatistic = 0;
      delete[] ipFrame0;
      ipFrame0 = 0;
      delete[] ipFrame1;
      ipFrame1 = 0;
      return -1;
      }

   // Get frame labels from difference (Diff) file
   float fCutThresh;
   EditThresholdText = EditThreshold->Text;
   sscanf (EditThreshold->Text.c_str(), "%f", &fCutThresh);
   fCutThresh /= 100.;

   retVal = metaData->AssignCadenceRepairLabels(0, videoFrameCount,
                                        diffFrameLabels, pulldown,
                                        cutStatistic, fCutThresh);
   if (retVal != 0)
      {
      // ERROR: Could not get frame labels
      delete metaData;
      delete[] diffFrameLabels;
      diffFrameLabels = 0;
      delete[] cutStatistic;
      cutStatistic = 0;
      delete[] ipFrame0;
      ipFrame0 = 0;
      delete[] ipFrame1;
      ipFrame1 = 0;
      return -1;
      }


   // finished with metadata
   delete metaData;

   // if the .crt file already exists, use it
   if ((vflp->FileDateCompare (cadenceFileName, diffFileName) > 0) &&
		bInitialCall)
    {
     // the .crt file exists and is more recent than the diff file.
     retVal = LoadCadenceFile ();
     if (retVal)
      {
       // ERROR:  could not load the .crt file
       delete[] diffFrameLabels;
       diffFrameLabels = 0;
       delete[] cutStatistic;
       cutStatistic = 0;
       delete[] ipFrame0;
       ipFrame0 = 0;
       delete[] ipFrame1;
       ipFrame1 = 0;
       return -1;
      }
    }


   FrameCutThreshold1->setCutStatistic (cutStatistic);
   FrameCutThreshold2->setCutStatistic (cutStatistic);
   FrameCutThreshold3->setCutStatistic (cutStatistic);
   FrameCutThreshold4->setCutStatistic (cutStatistic);
   FrameCutThreshold5->setCutStatistic (cutStatistic);


   // build the shot list
   BuildShotList ();
  
   return 0;

} // analyzeDiffFile


void __fastcall TFormCadence::FormClose(TObject *Sender,
      TCloseAction &Action)
{
  ReleaseStorage ();
}
//---------------------------------------------------------------------------

void TFormCadence::ReleaseStorage ()
{

  delete [] diffFrameLabels;
  diffFrameLabels = 0;
  delete[] cutStatistic;
  cutStatistic = 0;

  delete[] ipFrame0;
  ipFrame0 = 0;
  delete[] ipFrame1;
  ipFrame1 = 0;

  FrameCutThreshold1->setCutStatistic (cutStatistic);
  FrameCutThreshold2->setCutStatistic (cutStatistic);
  FrameCutThreshold3->setCutStatistic (cutStatistic);
  FrameCutThreshold4->setCutStatistic (cutStatistic);
  FrameCutThreshold5->setCutStatistic (cutStatistic);
}

void TFormCadence::InitListView ()
{

  // if the threshold has changed, regenerate labels
  if (EditThresholdText != EditThreshold->Text)
   {
    int iRet = analyzeDiffFile (false);
    if (iRet)
     {
      _MTIErrorDialog(Handle, "Unable to analyze the field difference file.");
      return;
     }

   EditThresholdText = EditThreshold->Text;
  }


  ListView1->Items->Clear();


  // run through the listShotInfo and fill in the list view

  for (unsigned int iList = 0; iList < listShotInfo.size(); iList++)
   {
    SShotInfo *sip = &listShotInfo.at(iList);

    // establish entries in the list view
    TListItem *lip = ListView1->Items->Add();

    char caTmp[32];
    sprintf (caTmp, "%d", iList+1);
    lip->Caption = caTmp;

    for (int i = 0; i < 7; i++)
     {
      lip->SubItems->Add(" ");
     }

    // get the time code of Break0
    vflp->getTimecodeForFrameIndex (sip->iSrcIndex).getTimeASCII(caTmp);
    lip->SubItems->Strings[COL_SRC_IN] = caTmp;
    vflp->getTimecodeForFrameIndex (sip->iDstIndex).getTimeASCII(caTmp);
    lip->SubItems->Strings[COL_DST_IN] = caTmp;

    switch (sip->iSrcLabel)
     {
      case FL_aA:
        lip->SubItems->Strings[COL_SRC_LABEL] = "Aa";
      break;
      case FL_bB:
        lip->SubItems->Strings[COL_SRC_LABEL] = "Bb";
      break;
      case FL_cB:
        lip->SubItems->Strings[COL_SRC_LABEL] = "Bc";
      break;
      case FL_dC:
        lip->SubItems->Strings[COL_SRC_LABEL] = "Cd";
      break;
      case FL_dD:
        lip->SubItems->Strings[COL_SRC_LABEL] = "Dd";
      break;
      default:
        lip->SubItems->Strings[COL_SRC_LABEL] = "Vv";
     }

    switch (sip->iDstLabel)
     {
      case FL_aA:
        lip->SubItems->Strings[COL_DST_LABEL] = "Aa";
      break;
      case FL_bB:
        lip->SubItems->Strings[COL_DST_LABEL] = "Bb";
      break;
      case FL_cB:
        lip->SubItems->Strings[COL_DST_LABEL] = "Bc";
      break;
      case FL_dC:
        lip->SubItems->Strings[COL_DST_LABEL] = "Cd";
      break;
      case FL_dD:
        lip->SubItems->Strings[COL_DST_LABEL] = "Dd";
      break;
      default:
        lip->SubItems->Strings[COL_DST_LABEL] = "Vv";
     }


    if (sip->bProcess)
     {
      lip->SubItems->Strings[COL_PROCESS] = "Yes";
     }
    else
     {
      lip->SubItems->Strings[COL_PROCESS] = "No";
     }

    sprintf (caTmp, "%d", (int)(100. * cutStatistic[sip->iSrcIndex]));
    lip->SubItems->Strings[COL_THRESH] = caTmp;
    sprintf (caTmp, "%d", sip->iSrcIndex);
    lip->SubItems->Strings[COL_INDEX] = caTmp;
   }
}

void __fastcall TFormCadence::ListView1CustomDrawSubItem(
      TCustomListView *Sender, TListItem *Item, int SubItem,
      TCustomDrawState State, bool &DefaultDraw)
{
  TColor color = clBlack;

  if (strcmp (Item->SubItems->Strings[COL_PROCESS].c_str(), "Yes") == 0)
   {
    color = Label9->Font->Color;   // green
   }
  else if (strcmp (Item->SubItems->Strings[COL_SRC_LABEL].c_str(), "Vv") == 0)
   {
    color = Label11->Font->Color;  // red
   }
  else
   {
    color = Label10->Font->Color; // yellow
   }

  Sender->Canvas->Font->Color = color;

}
//---------------------------------------------------------------------------

int TFormCadence::SaveCadenceFile ()
{
  int iFD;
  CBinManager binMgr;
  string cadenceFileName =
    binMgr.makeRepairedCadenceFilePath(strBin, strClip);

  iFD = open (cadenceFileName.c_str(), O_WRONLY|O_TRUNC|O_BINARY|O_CREAT,
                S_IREAD|S_IWRITE);
  if (iFD == -1)
   {
    return -1;
   }

  // write out a banner
  char caBanner[128];
  memset (caBanner, 0, 128);
  sprintf (caBanner, "MTI Cadence Repair Instruction File.  Version 1.0\n");
  if (write (iFD, caBanner, 128) == -1)
   {
    close (iFD);
    return -1;
   }


  // write out the data
  if (write (iFD, diffFrameLabels, sizeof(int) * vflp->getTotalFrameCount()) == -1)
   {
    close (iFD);
    return -1;
   }

  if (write (iFD, ipFrame0, sizeof(int) * vflp->getTotalFrameCount()) == -1)
   {
    close (iFD);
    return -1;
   }

  if (write (iFD, ipFrame1, sizeof(int) * vflp->getTotalFrameCount()) == -1)
   {
    close (iFD);
    return -1;
   }

  // write out the destination labels of each event
  unsigned int iSize = listShotInfo.size();
  if (write (iFD, &iSize, sizeof(int)) == -1)
   {
    close (iFD);
    return -1;
   }

  for (unsigned int iList = 0; iList < listShotInfo.size(); iList++)
   {
    if (write (iFD, &listShotInfo.at(iList).iDstLabel, sizeof(int)) == -1)
     {
      close (iFD);
      return -1;
     }
   }

  // write out the threshold used here
  int iThresh;
  sscanf (EditThresholdText.c_str(), "%d", &iThresh);
  if (write (iFD, &iThresh, sizeof(int)) == -1)
   {
    close (iFD);
    return -1;
   }


  close (iFD);
  return 0;
}

int TFormCadence::LoadCadenceFile ()
{
  int iFD;
  CBinManager binMgr;
  string cadenceFileName =
    binMgr.makeRepairedCadenceFilePath(strBin, strClip);

  iFD = open (cadenceFileName.c_str(), O_RDONLY|O_BINARY);
  if (iFD == -1)
   {
    return -1;
   }

  // skip banner
  if (lseek (iFD, 128, SEEK_SET) == -1)
   {
    close (iFD);
    return -1;
   }

  // write out the data
  if (read (iFD, diffFrameLabels, sizeof(int) * vflp->getTotalFrameCount()) == -1)
   {
    close (iFD);
    return -1;
   }

  // skip the frame indices
  if (lseek (iFD, 2 * sizeof(int) * vflp->getTotalFrameCount(), SEEK_CUR) == -1)
   {
    close (iFD);
    return -1;
   }

  // read in the destination labels
  int iNum, iLabel;
  if (read (iFD, &iNum, sizeof(int)) == -1)
   {
    close (iFD);
    return -1;
   }

  for (int i = 0; i < iNum; i++)
   {
    if (read (iFD, &iLabel, sizeof(int)) == -1)
     {
      close (iFD);
      return -1;
     }

    listDstLabel.push_back (iLabel);
   }

  // read in the threshold used here
  int iThresh;
  if (read (iFD, &iThresh, sizeof(int)) == -1)
   {
    close (iFD);
    return -1;
   }
  char caTmp[16];
  sprintf (caTmp, "%d", iThresh);
  EditThresholdText = caTmp;
  EditThreshold->Text = caTmp;

  close (iFD);
  return 0;
}


void TFormCadence::BuildShotList ()
{
  // clear the previous shot list
  listShotInfo.clear();

  // keep looking until no more shots exist
  int iFrame = 0;
  int iNFrame = vflp->getTotalFrameCount();

  while (iFrame < iNFrame)
   {
    // add this new shot to the list
    SShotInfo siTmp;
    siTmp.iSrcIndex = iFrame;
    siTmp.iSrcFrameCount = 0;
    siTmp.iSrcLabel = diffFrameLabels[iFrame] & (FL_aA|FL_bB|FL_cB|FL_dC|FL_dD);
    if (
                (siTmp.iSrcLabel != FL_aA) &&
                (siTmp.iSrcLabel != FL_bB) &&
                (siTmp.iSrcLabel != FL_cB) &&
                (siTmp.iSrcLabel != FL_dC) &&
                (siTmp.iSrcLabel != FL_dD)
       )
     {
      siTmp.iSrcLabel = FL_NONE;
     }

    if (siTmp.iSrcLabel == FL_NONE)
     {
      siTmp.bProcess = false;
     }
    else if (diffFrameLabels[iFrame] & FL_USER_OVERRIDE)
     {
      siTmp.bProcess = false;
     }
    else
     {
      siTmp.bProcess = true;
     }

    siTmp.iDstIndex = 0;
    siTmp.iFirstSrc = -1;
    siTmp.iLastSrc = -1;
    siTmp.iDstLabel = FL_NONE;
    siTmp.iDstFrameCount = 0;
    siTmp.iDstFrameCountNo32 = 0;

    bool bField0, bField1;
    bField0 = false;
    bField1 = false;

    int iSrcLabel = siTmp.iSrcLabel;

    // keep looking until next cut is found
    do
     {
      if (siTmp.bProcess == false)
       {
        // this will be a simple copy of the original video frames
        siTmp.iDstFrameCountNo32++;
        if (siTmp.iFirstSrc == -1)
         {
          siTmp.iFirstSrc = iFrame;
         }
        siTmp.iLastSrc = iFrame;
       }
      else
       {
        if (
	  (iSrcLabel & FL_aA) ||
	  (iSrcLabel & FL_bB) ||
	  (iSrcLabel & FL_dD)
           )
         {
          bField0 = true;
          bField1 = true;
         }
        else if (iSrcLabel & FL_cB)
         {
          // only keep first field
          bField0 = true;
          bField1 = false;
         }
        else if (iSrcLabel & FL_dC)
         {
          bField1 = true;
         }
        else
         {
          // error condition, no known label
          bField0 = false;
          bField1 = false;
         }

        // if we have found two fields to use, update the dst frame count
        if (bField0 && bField1)
         {
          if (siTmp.iFirstSrc == -1)
           {
            siTmp.iFirstSrc = iFrame;
           }
          siTmp.iLastSrc = iFrame;
          siTmp.iDstFrameCountNo32++;
         }
       }

      // increment the frame count
      siTmp.iSrcFrameCount++;
      iFrame++;
      IncrementLabel (&iSrcLabel);
     } while (iFrame < iNFrame && (diffFrameLabels[iFrame] & FL_CUT) == 0);

    listShotInfo.push_back (siTmp);
   }

  // assign destination frame labels
  AssignDstFrameLabel (FL_aA, 0);

  // override the labels, if a list of previous labels exists
  for (unsigned int i = 0; i < listDstLabel.size(); i++)
   {
    AssignDstFrameLabel (listDstLabel.at(i), i);
   }
}


void TFormCadence::AssignDstFrameLabel (int iLabel, unsigned int iStartEvent)
{
  // assign iLabel to the listShotInfo.at(iStartEvent), then extrapolate
  // for the rest of the events

  while (iStartEvent < listShotInfo.size())
   {
    // keep track of starting index after applying 3:2
    if (iStartEvent == 0)
     {
      listShotInfo.at(iStartEvent).iDstIndex = 0;
     }
    else
     {
      listShotInfo.at(iStartEvent).iDstIndex =
        listShotInfo.at(iStartEvent-1).iDstIndex +
                listShotInfo.at(iStartEvent-1).iDstFrameCount;
     }

    listShotInfo.at(iStartEvent).iDstFrameCount = 0;

    if (listShotInfo.at(iStartEvent).bProcess)
     {
      listShotInfo.at(iStartEvent).iDstLabel = iLabel;

      // increment the label value
      for (int iFrame = 0; iFrame < listShotInfo.at(iStartEvent).iDstFrameCountNo32;
		iFrame++)
       {
        IncrementLabel (&iLabel);

        listShotInfo.at(iStartEvent).iDstFrameCount++;

        // if this is a dC frame, we need to call again
        if (iLabel == FL_dC)
         {
          IncrementLabel (&iLabel);
          listShotInfo.at(iStartEvent).iDstFrameCount++;
         }
       }
     }
    else
     {
      listShotInfo.at(iStartEvent).iDstLabel = FL_NONE;  // destination is video
      // just doing a straight copy
      listShotInfo.at(iStartEvent).iDstFrameCount =
                listShotInfo.at(iStartEvent).iSrcFrameCount;

      iLabel = FL_aA;  // reset to aA frame
     }

    // move on to the next event
    iStartEvent++;
   }

}

void TFormCadence::IncrementLabel (int *ipLabel)
{
  switch (*ipLabel)
   {
    case FL_aA:
      *ipLabel = FL_bB;
    break;
    case FL_bB:
      *ipLabel = FL_cB;
    break;
    case FL_cB:
      *ipLabel = FL_dC;
    break;
    case FL_dC:
      *ipLabel = FL_dD;
    break;
    case FL_dD:
      *ipLabel = FL_aA;
    break;
   }
}

void TFormCadence::BuildFieldList ()
{
  int iNFrame = vflp->getTotalFrameCount();
  int iDstFrame = 0;
  int iDstPrev = 0;

  // for each event in listShotInfo, convert the source to a 24 frame list.
  // then add 3:2 to generate the output field order

  vector <int> listFrame0, listFrame1;

  for (unsigned int iList = 0; iList < listShotInfo.size(); iList++)
   {
    SShotInfo *sip = &listShotInfo.at(iList);

    listFrame0.clear();
    listFrame1.clear();

    int iSrcLabel = sip->iSrcLabel;
    for (int iFrame = sip->iSrcIndex; iFrame < sip->iFirstSrc; iFrame++)
     {
      IncrementLabel (&iSrcLabel);
     }

    // remove the 3:2 from the source
    for (int iFrame = sip->iFirstSrc; iFrame <= sip->iLastSrc; iFrame++)
     {
      switch (iSrcLabel)
       {
        case FL_cB:
          // do nothing.  Already processed by dC
        break;
        case FL_dC:
          // first field comes from last frame
          listFrame0.push_back (iFrame-1);
          // second field comes from current frame
          listFrame1.push_back (iFrame);
        break;
        default:
          // copy current frame as-is
          listFrame0.push_back (iFrame);
          listFrame1.push_back (iFrame);
       }

      IncrementLabel (&iSrcLabel);
     }

    // now run through all the destination frames and generate 3:2
    int iLabel = sip->iDstLabel;

    unsigned int iFrame = 0;
    while (iFrame < listFrame0.size())
     {
      iDstPrev = iDstFrame - 1;
      if (iDstPrev < 0)
        iDstPrev = 0;
      switch (iLabel)
       {
        case FL_aA:
        case FL_bB:
        case FL_NONE:
          ipFrame0[iDstFrame] = listFrame0.at(iFrame);
          ipFrame1[iDstFrame] = listFrame1.at(iFrame);
          iDstFrame++;
          iFrame++;
        break;
        case FL_cB:
          ipFrame0[iDstFrame] = listFrame0.at(iFrame);
          ipFrame1[iDstFrame] = ipFrame1[iDstPrev];  // repeat the last frame
          iDstFrame++;
          if (iDstFrame < iNFrame)
           {
            ipFrame1[iDstFrame] = listFrame1.at(iFrame);
            iDstFrame++;
           }
          iFrame++;
        break;
        case FL_dC:
          // do nothing.
          // the "d" is handled by dD.  The "C" is handled by cB
        break;
        case FL_dD:
          ipFrame0[iDstPrev] = listFrame0.at(iFrame);
          ipFrame0[iDstFrame] = listFrame0.at(iFrame);
          ipFrame1[iDstFrame] = listFrame1.at(iFrame);
          iDstFrame++;
          iFrame++;
        break;
       }

      IncrementLabel (&iLabel);
     }
   }

  // fill in any empty fields at the end
  iDstPrev = iDstFrame - 1;
  if (iDstPrev < 0)
    iDstPrev = 0;
  for ( ; iDstFrame < iNFrame; iDstFrame++)
   {
    ipFrame0[iDstFrame] = ipFrame0[iDstPrev];
    ipFrame1[iDstFrame] = ipFrame1[iDstPrev];
   }
}

void __fastcall TFormCadence::SetSrcLabelClick(TObject *Sender)
{
  // change the source label to the indicated value
  int iLabel = FL_NONE;
  if (Sender == SetSrcLabeltoAa)
   {
    iLabel = FL_aA;
   }
  else if (Sender == SetSrcLabeltoBb)
   {
    iLabel = FL_bB;
   }
  else if (Sender == SetSrcLabeltoBc)
   {
    iLabel = FL_cB;
   }
  else if (Sender == SetSrcLabeltoCd)
   {
    iLabel = FL_dC;
   }
  else if (Sender == SetSrcLabeltoDd)
   {
    iLabel = FL_dD;
   }

  // run through each selected item and adjust
  TListItem *selectedItem = ListView1->Selected;
  TItemStates selected = TItemStates() << isSelected;
  while (selectedItem != 0)
   {
    SShotInfo *sip = &listShotInfo.at(selectedItem->Index);

    sip->iSrcLabel = iLabel;
    diffFrameLabels[sip->iSrcIndex] &= ~(FL_aA|FL_bB|FL_cB|FL_dC|FL_dD);
    diffFrameLabels[sip->iSrcIndex] |= iLabel;

    selectedItem = ListView1->GetNextItem (selectedItem, sdAll, selected);
   }

  // the source has changed, so build the shot list
  BuildShotList ();
  InitListView ();

}
//---------------------------------------------------------------------------

void __fastcall TFormCadence::SetDstLabelClick(TObject *Sender)
{
  // change the source label to the indicated value
  int iLabel = FL_NONE;
  if (Sender == SetDstLabeltoAa)
   {
    iLabel = FL_aA;
   }
  else if (Sender == SetDstLabeltoBb)
   {
    iLabel = FL_bB;
   }
  else if (Sender == SetDstLabeltoBc)
   {
    iLabel = FL_cB;
   }
  else if (Sender == SetDstLabeltoCd)
   {
    iLabel = FL_dC;
   }
  else if (Sender == SetDstLabeltoDd)
   {
    iLabel = FL_dD;
   }

  if (iLabel == FL_NONE)
    return;

  // run through each selected item and adjust
  TListItem *selectedItem = ListView1->Selected;
  TItemStates selected = TItemStates() << isSelected;
  while (selectedItem != 0)
   {
    SShotInfo *sip = &listShotInfo.at(selectedItem->Index);

    // not allowed to change from FL_NONE
    if (sip->iDstLabel != FL_NONE)
     {
      sip->iDstLabel = iLabel;
     }

    selectedItem = ListView1->GetNextItem (selectedItem, sdAll, selected);
   }

  InitListView ();

}
//---------------------------------------------------------------------------

    void TFormCadence::StartForm(void)
{
     if (!bIsLicensed)
     {
        MTIErrorDialog(strStartupFailedMessage);
        exit(0);
     }

}

