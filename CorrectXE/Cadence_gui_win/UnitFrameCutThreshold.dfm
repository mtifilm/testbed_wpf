object FrameCutThreshold: TFrameCutThreshold
  Left = 0
  Top = 0
  Width = 246
  Height = 29
  TabOrder = 0
  object VTimeCodeEdit1: VTimeCodeEdit
    Left = 8
    Top = 2
    Width = 72
    Height = 21
    Hint = 'Enter the timecode of a known cut'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    OnDropFrame = VTimeCodeEdit1TimecodeChange
    OnTimecodeChange = VTimeCodeEdit1TimecodeChange
  end
  object Edit1: TEdit
    Left = 112
    Top = 2
    Width = 65
    Height = 21
    Hint = 'This is the threshold corresponding to this timecode'
    Enabled = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
  end
  object Button1: TButton
    Left = 200
    Top = 2
    Width = 21
    Height = 21
    Hint = 'Cancel this timecode'
    Caption = 'X'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    OnClick = Button1Click
  end
end
