// -*- C++ -*-

#ifndef FORMAT_ERROR_H
#define FORMAT_ERROR_H

//////////////////////////////////////////////////////////////////////
// Error Code Defines (Numeric Order)
//
// Note: try to keep numbers in the range of -5050 to -5200

#define FORMAT_ERROR_INVALID_PIXEL_COMPONENTS   -5050
#define FORMAT_ERROR_INVALID_MEDIA_FORMAT_TYPE  -5051
#define FORMAT_ERROR_INVALID_COLOR_SPACE        -5052
#define FORMAT_ERROR_INVALID_FRAME_ASPECT_RATIO -5053
#define FORMAT_ERROR_INVALID_HORIZONTAL_INTERVAL_PIXELS -5054
#define FORMAT_ERROR_INVALID_IMAGE_FORMAT_TYPE          -5055
#define FORMAT_ERROR_INVALID_LINES_PER_FRAME            -5056
#define FORMAT_ERROR_INVALID_PIXEL_ASPECT_RATIO         -5057
#define FORMAT_ERROR_INVALID_PIXEL_PACKING              -5058
#define FORMAT_ERROR_INVALID_PIXELS_PER_LINE            -5059
#define FORMAT_ERROR_INVALID_VERTICAL_INTERVAL_ROWS     -5060
#define FORMAT_ERROR_INVALID_BITS_PER_COMPONENT         -5061
#define FORMAT_ERROR_INVALID_SAMPLE_RATE                -5062
#define FORMAT_ERROR_INVALID_SAMPLES_PER_FIELD          -5063
#define FORMAT_ERROR_INVALID_CHANNEL_COUNT              -5064
#define FORMAT_ERROR_INVALID_SAMPLE_ENDIAN              -5065
#define FORMAT_ERROR_INVALID_SAMPLE_DATA_PACKING        -5066
#define FORMAT_ERROR_INVALID_SAMPLE_DATA_SIZE           -5067
#define FORMAT_ERROR_INVALID_SAMPLE_DATA_TYPE           -5068
#define FORMAT_ERROR_MALLOC                             -5069
#define FORMAT_ERROR_UNKNOWN_FILE_TYPE                  -5070
#define FORMAT_ERROR_OPEN                               -5071
#define FORMAT_ERROR_SEEK                               -5072
#define FORMAT_ERROR_BAD_DIMENSION                      -5073
//#define FORMAT_ERROR_UNSUPPORTED_COMMAND                -5074
//#define FORMAT_ERROR_READ                               -5075
#define FORMAT_ERROR_BAD_MAGIC_NUMBER                   -5076
//#define FORMAT_ERROR_BAD_IMAGE_SIZE                     -5077
#define FORMAT_ERROR_UNSUPPORTED_OPTION                 -5078
#define FORMAT_ERROR_PARAMETER_MISMATCH                 -5079
#define FORMAT_ERROR_BAD_COMPONENTS                     -5080
#define FORMAT_ERROR_BAD_BIT_DEPTH                      -5081
#define FORMAT_ERROR_WRITE                              -5082
#define FORMAT_ERROR_UNKNOWN_OPTION                     -5083
#define FORMAT_ERROR_INVALID_COLOR_SPACE_COMPONENT      -5084
#define FORMAT_ERROR_NO_OPEN_AUDIO_FILE                 -5085
#define FORMAT_ERROR_NO_REMAINING_AUDIO_SAMPLES         -5086
#define FORMAT_ERROR_UNSUPPORTED_AUDIO_OPTION           -5087
#define FORMAT_ERROR_NO_AUDIO_FORMAT                    -5088
#define FORMAT_ERROR_MAX_LICENSE_RESOLUTION_EXCEEDED    -5089
#define FORMAT_ERROR_INVALID_GAMMA                      -5090
#define FORMAT_ERROR_INVALID_TRANSFER_CHARACTERISTICS   -5091
#define FORMAT_ERROR_INVALID_ALPHA_MATTE_TYPE           -5092
#define FORMAT_ERROR_CHSIZE                             -5093
#define FORMAT_ERROR_DELETE                             -5094
#define FORMAT_ERROR_STAT                               -5095
#define FORMAT_ERROR_RENAME                             -5096
#define FORMAT_ERROR_CANT_DO_ASYNCH_IO                  -5097
#define FORMAT_ERROR_INTERNAL                           -5098
#define FORMAT_ERROR_HEADER_COPY_FAILED                 -5099
#define FORMAT_ERROR_INSUFFICIENT_PHYS_MEM              -5100
#define FORMAT_ERROR_PHYS_MEM_ALLOC_FAIL                -5101
#define FORMAT_ERROR_VIRTUAL_MEM_ALLOC_FAIL             -5102
#define FORMAT_ERROR_PAGE_MAPPING_FAIL                  -5103
#define FORMAT_ERROR_BAD_PACKING                        -5104
#define FORMAT_ERROR_VERTICAL_DIM                       -5105
#define FORMAT_ERROR_HORIZONTAL_DIM                     -5106
#define FORMAT_ERROR_BAD_COLORSPACE                     -5107
#define FORMAT_ERROR_PIXEL_COMPONENT                    -5108
#define FORMAT_ERROR_NOT_INITIALIZED                    -5109


//////////////////////////////////////////////////////////////////////
// Error Code Descriptions (Alpha Order)

// FORMAT_ERROR_BAD_BIT_DEPTH
//    the bit depth or packing of the file is not supported

// FORMAT_ERROR_BAD_COMPONENTS
//    the number of components in the file is not supported

// FORMAT_ERROR_BAD_DIMENSION
//    The image dimensions are bad

// FORMAT_ERROR_BAD_IMAGE_SIZE
//    the size of the image does not match the header info

// FORMAT_ERROR_BAD_MAGIC_NUMBER
//    the image does not have a valid MAGIC number in the header

// FORMAT_ERROR_INVALID_ALPHA_MATTE_TYPE
//    The alpha matte type is invalid for the image format

// FORMAT_ERROR_INVALID_BITS_PER_COMPONENT
//    The number of bits per component is invalid for the image format
//    or color space
   
// FORMAT_ERROR_INVALID_CHANNEL_COUNT
//    The Channel Count value is invalid or out-of-range in the Audio
//    Format section of a Clip File or a Clip Scheme

// FORMAT_ERROR_INVALID_COLOR_SPACE
//    The Color Space type is invalid in the Image Format section of a
//    Clip file or a Clip Scheme.

// FORMAT_ERROR_INVALID_FRAME_ASPECT_RATIO
//    The Frame Aspect Ratio is invalid in the Image Format section of
//    a Clip File or a Clip Scheme

// FORMAT_ERROR_INVALID_GAMMA
//    The Gamma is invalid in the Image Format section of
//    a Clip File or a Clip Scheme

// FORMAT_ERROR_INVALID_HORIZONTAL_INTERVAL_PIXELS
//    The Horizontal Interval Pixels value is invalid in the Image
//    Format section of a Clip file or a Clip Scheme

// FORMAT_ERROR_INVALID_IMAGE_FORMAT_TYPE
//    The Image Format type is invalid in the Image Format section of
//    a Clip file or a Clip Scheme or as an argument to a CImageFormat
//    class function

// FORMAT_ERROR_INVALID_LINES_PER_FRAME
//    The Lines per Frame is invalid in the Image Format section of a
//    Clip file or a Clip Scheme

// FORMAT_ERROR_INVALID_MEDIA_FORMAT_TYPE
//    Invalid or unexpected CMediaFormat type.  Possibly a corrupted
//    clip or clip initialization info.

// FORMAT_ERROR_INVALID_PIXEL_ASPECT_RATIO
//    The Pixel Aspect Ratio is invalid in the Image Format section of
//    a Clip file or a Clip Scheme.

// FORMAT_ERROR_INVALID_PIXEL_COMPONENTS
//    The Pixel Components type is invalid in the Image Format section
//    of a Clip file or a Clip Scheme.

// FORMAT_ERROR_INVALID_PIXEL_PACKING
//    The Pixel Packing type is invalid in the Image Format section of
//    a Clip file or a Clip Scheme.

// FORMAT_ERROR_INVALID_PIXELS_PER_LINE
//    The Pixels per Line value is invalid in the Image Format section
//    of a Clip file or a Clip Scheme

// FORMAT_ERROR_INVALID_SAMPLE_DATA_PACKING
//    The Sample Data Packing is invalid in the Audio Format section
//    of a Clip File or a Clip Scheme

// FORMAT_ERROR_INVALID_SAMPLE_DATA_SIZE
//    The Sample Data Size is invalid in the Audio Format section of a
//    Clip File or a Clip Scheme

// FORMAT_ERROR_INVALID_SAMPLE_DATA_TYPE
//    The Sample Data Type is invalid in the Audio Format section of a
//    Clip File or a Clip Scheme

// FORMAT_ERROR_INVALID_SAMPLE_ENDIAN
//    The Sample Data Endian is invalid in the Audio Format of a clip
//    or clip scheme

// FORMAT_ERROR_INVALID_SAMPLE_RATE
//    The Sample Rate value is invalid in the Audio Format section of
//    a Clip File or a Clip Scheme

// FORMAT_ERROR_INVALID_SAMPLES_PER_FIELD
//    The Samples per Field value is invalid in the Audio Format
//    section of a Clip File or a Clip Scheme

// FORMAT_ERROR_INVALID_TRANSFER_CHARACTERISTICS
//    The Transfer Characteristics value is invalid in the Image Format
//    section of a Clip file or a Clip Scheme

// FORMAT_ERROR_INVALID_VERTICAL_INTERVAL_ROWS
//    The Vertical Interval Rows value is invalid in the Image Format
//    section of a Clip file or a Clip Scheme

// FORMAT_ERROR_MALLOC
//    Trouble allocating storage

// FORMAT_ERROR_NO_AUDIO_FORMAT
//    The audio header data is missing the format section

// FORMAT_ERROR_NO_OPEN_AUDIO_FILE
//    The audio file has not been opened

// FORMAT_ERROR_NO_REMAINING_AUDIO_SAMPLES
//    The audio file does not have any more audio samples

// FORMAT_ERROR_OPEN
//    trouble opening file

// FORMAT_ERROR_PARAMETER_MISMATCH
//    current file parameters do not match prototype, or first, image

// FORMAT_ERROR_READ
//    trouble reading from file

// FORMAT_ERROR_SEEK
//    trouble seeking in file

// FORMAT_ERROR_UNKNOWN_FILE_TYPE
//    this file type is not supported

// FORMAT_ERROR_UNKNOWN_OPTION
//    this option is not known

// FORMAT_ERROR_UNSUPPORTED_AUDIO_OPTION
//    an audio option in the header is not supported

// FORMAT_ERROR_UNSUPPORTED_COMMAND
//    this command is not supported

// FORMAT_ERROR_UNSUPPORTED_OPTION
//    an option in the header is not supported

// FORMAT_ERROR_WRITE
//    trouble writing to hard disk

// FORMAT_ERROR_MAX_LICENSE_RESOLUTION_EXCEEDED
//    The maximum resolution is 1920 x 1080 but this file contains
//      a resolution greater than that.  This is related to the video-only
//      license - e.g. Teranex bundle.

// FORMAT_ERROR_CANT_DO_ASYNCH_IO
//    Was asked to do asynchronous I/O, but no can do.

// FORMAT_ERROR_HEADER_COPY_FAILED
//    Got an error while trying to copy the header from a maaster clip
//    frame to the corresponding dirty version clip frame


#endif // FORMAT_ERROR_H
