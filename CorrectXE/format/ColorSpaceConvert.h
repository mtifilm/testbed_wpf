#ifndef ColorSpaceConvertH
#define ColorSpaceConvertH

/*
	File:	ColorSpaceConvert.h
	By:	Kevin Manbeck
	Date:	March 6, 2001

	The ColorSpaceConvert class is used to translate color between
	two different image formats.

	There are three functions defined by the API:

		Init ()
		ConvertOneFrame ()
		ConvertOneValue ()
		SetColorRepresentation ()
		SetTransferFunction ()
		SetEncodeParameters ()
		GetColorRepresentation ()
		GetTransferFunction ()
		GetEncodeParameters ()

	Init()
		must be called before either ConvertOneFrame() or
	ConvertOneValue ().  Init() sets up the parameters need to 
	perform a conversion from the source color space to the destination
	color space.

	The arguements for Init() are the ImageFormat class of the source
	and the ImageFormat class of the destination.

	There are several optional arguments:
		1.  a pointer to a LUT class.  Useful in multi threaded 
		    applications that wish to share the internal LUTs
		2.  seed value for randomizations.  Usefule in multi threaded
		    applications.
		3.  SourceLogSpace.  Useful when caller wishes to override
		    default log space of source
		4.  DisplayGamma.  Useful when caller wishes to override
		    default gamma of the destination. [NOTE: Display gamma was
            removed because the image format now contains a gamma, so
            the display gamma should be set there]



	ConvertOneFrame()
		will convert the source material to the destination
	material.  It will perform an actual color conversion only if
	the color space of the source is different from that of the destination.
	By specifying the same color space for the source and destination,
	this library allows an efficient means of unpacking to 16 bits or
	repacking from 16 bits.

	The arguements for ConvertOneFrame() are **ucpSrc and **ucpDst.
	The arguments are double pointers which allows two fields of data
	to be transferred to and from the library.

	ConvertOneValue()
		will convert a source color value to its
	corresponding destination color value.  This is useful for 
	creating a look up table. 

	The arguements for ConvertOneValue() are Src0, Src1, Src2 and
	Dst0, Dst1, Dst2.  In the case of YUV (and YCbCr) data, the library
	assumes that the Y value occupies component 0, U is component 1,
	and V is component 2.  In the case of RGB data, the R is component 0,
	G is component 1, and B is component 2.

	SetColorRepresentation ()
		used to override the default color space representation

	SetTransferFunction ()
		used to override the default transfer function parameters

	SetEncodeParameters ()
		used to override the default encode parameters

	GetColorRepresentation ()
		returns the current color space representation

	GetTransferFunction ()
		returns the current transfer function parameters

	GetEncodeParameters ()
		returns the current encode parameters


				TECHNICAL DETAILS

	There are eight steps to converting from one format to another

		1.  Invert the Data Encode of the Source
		2.  Invert the Color Representation of the Source
		3.  Invert the Transfer Function of the Source
		4.  Apply the Transfer Function of the Destination	
		5.  Apply the Color Representation of the Destination
		6.  Apply the Data Encode of the Destination

	Steps 1 and 2 and combined into 9 look up tables, called LUTA.
	Steps 3,4, and 5 are combined into 9 look up tables, called LUTB.
	Step 6 is implemented as three dithered look up tables, called LUTC.

	When converting a bona fide source clip into a bona fide destination
	clip, the arguements of Init() should be the ImageFormat class
	pointers to the source and destination.  When converting to or
	from an virtual clip, the arguements to Init() should be locally
	constructed, temporary ImageFormats.  To create one of these image
	formats, it is sufficient to specify:

    			ifLocal.setColorSpace();
    			ifLocal.setPixelComponents();
    			ifLocal.setPixelPacking();
*/

#include <string>
#include <iostream>

#include "machine.h"
#include "formatDLL.h"

using std::string;
using std::ostream;

/////////////////////////////////////////////////////////////////////////
// Forward Declarations

class CImageFormat;
class CColorConvertFancyLUT;

/////////////////////////////////////////////////////////////////////////

#define BIT_PER_LUT 19		// since the 16 bits is the most precise
				// image we deal with, set BIT_PER_LUT slightly
				// higher
#define MAX_SPEED_UP_FLAG 5	// it is expensive to calculate the LUT
				// entries.  We speed up the process by 
				// calculating the LUT's coarsely and then
				// interpolating the fine scale.
#define NUM_RAND 99611     // this is a large prime number

#define MAX_COMPONENT 3
#define NUM_DITHER 5		// we dither the Data Encode step to prevent
				// banding.  This is the number of separate
				// Data Encode lookup tables.

#define DATA_ENCODE_FUNCTION_TYPE_LINEAR 1
#define DATA_ENCODE_FUNCTION_TYPE_PRINT_DENSITY 2

#define INDEX_SHIFT  7		// used by approximate conversion code


/* 
	The KODAK documentation describes how to go between linear space 
	and print density:

	1.  The full 10 bits represent 2.048 in density space.  Thus,
	each bit represents a 0.002 density increment.

	2.  The black value (1% density) should fall at 95

	3.  The white value (90% density) should fall at 685

	4.  The gamma of film is 0.6

	Starting with P, print density, between 95 and 685:

		a.  convert to E, relative exposure:

			E = (P * .002) / .6

		b.  shift E, so it achieves 0 at 685:

			E' = E - (685 * .002) / .6

		c.  antilog to get linear value:

			L = 10^E'

		    note:  the minimum value of L is 10^((95-685)*.002/.6)

	Then L is rescaled into the desired range


	Starting with L between 10^((95-685)*.002/.6) and 0:

		a.  convert to E':

			E' = log (L);

		b.  shift to E:

			E = E' + (685 * .002) / .6

		c.  convert to print density

			P = E * .6 / .002;


	To simplify, assume D is log data in the range D0 to D1.  Convert
	to Linear in the range 0. to 1.:

		T = 10 ^ ( (95-685)(D1-D)/(D1-D0) * .002 / .6 );
                T0 = 10 ^ ((95-685)*.002/.6)
		L = (T - T0) / (1. - T0);  // L is in range 0 to 1

	To simply, assume L is in range 0 to 1.  Convert to D in range
	D0 to D1:

                T0 = 10 ^ ((95-685)*.002/.6)
		T = L * (1. - T0) + T0;
		D = D1 - log (T) * (.6 / .002) * ((D1-D0)/(95-685));

	In the code,
		PRINT_DENSITY_BLACK is 95
		PRINT_DENSITY_WHITE is 685
		PRINT_DENSITY_INCREMENT is .002
		PRINT_DENSITY_GAMMA is .6

*/

#define PRINT_DENSITY_BLACK 95
#define PRINT_DENSITY_WHITE 685
#define PRINT_DENSITY_INCREMENT .002
#define PRINT_DENSITY_GAMMA .6

		// Values of ProcessFlow
#define PF_NONE 0x0
#define PF_UNPACK_NO 0x1	// does the source need unpacking?
#define PF_UNPACK_YES 0x2	// does the source need unpacking?
#define PF_CONVERT_NO 0x4	// is a conversion required?
#define PF_CONVERT_YES 0x8	// is a conversion required?
#define PF_REPACK_NO 0x10	// does the destination need repacking?
#define PF_REPACK_YES 0x20	// does the destination need repacking?

/*

	The allowed paths to convert from the source to the destination:

			unpack, color convert, and repack:
	Src -> (unpack) -> Tmp -> (color convert) -> Tmp -> (repack) -> Dst

			color convert and repack:
	Src -> (color convert) -> Tmp -> (repack) -> Dst

			unpack to 16 bits:
	Src -> (unpack) -> Dst

			unpack to other than 16 bits:
	Src -> (unpack) -> Tmp -> (repack) -> Dst

			unpack and color convert
	Src -> (unpack) -> Dst -> (color convert) -> Dst

			color convert
	Src -> (color convert) -> Dst

			repack
	Src -> (repack) -> Dst

			no unpack, no convert, no repack:  currently not
			supported
	Src -> Dst

*/

typedef struct
{
  long
    lNComponents;

  float
    faColorRepresentation[MAX_COMPONENT][MAX_COMPONENT],
    faColorRepresentationInv[MAX_COMPONENT][MAX_COMPONENT];

  float
    fTransferFunctionGain,
    fTransferFunctionBreakPoint,
    fTransferFunctionGamma;

  long
    lDataEncodeFunctionType;

  long
    laDataEncodeMinimum[MAX_COMPONENT],
    laDataEncodeMaximum[MAX_COMPONENT],
    laDataEncodeClipLower[MAX_COMPONENT],
    laDataEncodeClipUpper[MAX_COMPONENT];

		// Derived parameters
  float
    faDataEncodeSlope[MAX_COMPONENT],
    faDataEncodeIntercept[MAX_COMPONENT];

  float
    fTransferFunctionNormalize,
    fTransferFunctionThreshold;
} COLOR_SPACE_STRUCT;

struct CColorConvertFancyLUT
{
  long
    lNEntryLUT,		/* number of entries in the LUT.  (1<<BIT_PER_LUT) */
    *lpLUTA[MAX_COMPONENT][MAX_COMPONENT],	/* steps 1 and 2 */
    *lpLUTB[MAX_COMPONENT][MAX_COMPONENT],	/* steps 3,4 and 5 */
    *lpLUTC[MAX_COMPONENT][NUM_DITHER],		/* step 6 */
    laMinIndexLUTC[MAX_COMPONENT],
    laMaxIndexLUTC[MAX_COMPONENT];

  float
    faSlopeC[MAX_COMPONENT],
    faInterceptC[MAX_COMPONENT];

  long
    *lpRandLUT;

  CColorConvertFancyLUT ();
  ~CColorConvertFancyLUT ();
  void ApplyLUTC (long lX0, long lX1, long lX2, long *lpY0, long *lpY1,
		long *lpY2, long *lpDitherIndex);
};

class MTI_FORMATDLL_API CColorSpaceConvert
{
public:
  CColorSpaceConvert();
  ~CColorSpaceConvert();

  int Init (const CImageFormat *ifpSrcArg, const CImageFormat *ifpDstArg,
        CColorConvertFancyLUT *ccflpLutArg = NULL, int iDitherSeed = 0,
	int iSourceLogSpace = -1);
  int EstablishLUT (CColorConvertFancyLUT **ccflpLUT = NULL);
  int ConvertOneFrame (MTI_UINT8 **ucpSrc, MTI_UINT8 **ucpDst);
  void ConvertOneValue (MTI_UINT16 usSrc0, MTI_UINT16 usSrc1, MTI_UINT16 usSrc2,
		MTI_UINT16 *uspDst0, MTI_UINT16 *uspDst1, MTI_UINT16 *uspDst2);
  void ConvertOneValue (MTI_UINT16 usSrc0, MTI_UINT16 usSrc1, MTI_UINT16 usSrc2,
		float *fpDst0, float *fpDst1, float *fpDst2);
  void ConvertOneValue (float fSrc0, float fSrc1, float fSrc2,
		float *fpDst0, float *fpDst1, float *fpDst2);
  int SetColorRepresentation (bool bSrc,
	float faRGBtoXYZ[MAX_COMPONENT][MAX_COMPONENT]);
  void SetTransferFunction (bool bSrc,
	float fGain, float fBreakPoint, float fGamma);
  void SetEncodeParameters (bool bSrc,
	long laMin[MAX_COMPONENT], long laMax[MAX_COMPONENT],
	long laLower[MAX_COMPONENT], long laUpper[MAX_COMPONENT]);
  void GetColorRepresentation (bool bSrc,
	float faRGBtoXYZ[MAX_COMPONENT][MAX_COMPONENT]);
  void GetTransferFunction (bool bSrc,
	float &fGain, float &fBreakPoint, float &fGamma);
  void GetEncodeParameters (bool bSrc,
	long laMin[MAX_COMPONENT], long laMax[MAX_COMPONENT],
	long laLower[MAX_COMPONENT], long laUpper[MAX_COMPONENT]);

private:

  bool
    bInitialized,
    bEstablishLUT,
    bWeOwnTheFancyLUT,
    bUseApproxConvert;

  COLOR_SPACE_STRUCT
    cssSrc,
    cssDst;

  MTI_UINT16
    *uspTmp;	/* temporary storage used as intermediate in conversion */

  CColorConvertFancyLUT *ccflpLUT;

  long
    lNRow,
    lNCol,
    lProcessFlow;	/* see PF_* for values */

  CImageFormat
   *ifpSrc,
   *ifpDst;

  float
    *fpLUT,		// used for the approximate conversion
    *fpWeiLB,
    *fpWeiUB;

  long
    lNIndex,
    lLBShift,
    lRandIndex;

  int EstablishParam (COLOR_SPACE_STRUCT *cssp, CImageFormat *ifp,
		int iLogSpace);
  void MatrixCombine (long lX0, long lX1, long lX2, long *lpY0, long *lpY1,
		long *lpY2, long *lpLoc[MAX_COMPONENT][MAX_COMPONENT]);
  int EstablishFancyLUT ();
  int EstablishLUTApprox ();
  int EstablishFlow ();
  void FindDataEncodeFactors (COLOR_SPACE_STRUCT *cssp);
  float CalcDataEncodeInv (COLOR_SPACE_STRUCT *cssp, long lComponent,
		long lEncode);
  float CalcDataEncode (COLOR_SPACE_STRUCT *cssp, long lComponent,
		float fValue);
  void FindDataEncodeMinMax (COLOR_SPACE_STRUCT *cssp, long lComponent,
		float *fpMin, float *fpMax);
  float CalcTransferFunctionInv (COLOR_SPACE_STRUCT *cssp, float fTransfer);
  float CalcTransferFunction (COLOR_SPACE_STRUCT *cssp, float fTransferInv);
  void FindTransferFunctionNormalize (COLOR_SPACE_STRUCT *cssp);
  void Free ();
  int UnpackSourceData (MTI_UINT8 **ucpSrc, MTI_UINT16 *uspIntermediate);
  void ConvertSourceData (MTI_UINT16 *uspSrcInter, MTI_UINT16 *uspDstInter);
  void ConvertSourceDataApprox (MTI_UINT16 *uspSrcInter, MTI_UINT16 *uspDstInter);
  int RepackDestinationData (MTI_UINT8 **ucpDst, MTI_UINT16 *uspIntermediate);
  bool DifferentParameters();
  void LinearInterpolateLUT (long *lp);
};

#endif
