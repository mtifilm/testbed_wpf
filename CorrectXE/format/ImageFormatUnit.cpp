//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ImageFormatUnit.h"
#include "ImageInfo.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
   : TForm(Owner)
{
   int formatCount = CImageInfo::queryImageFormatTypeDescriptionCount();

   string name, descr1, descr2;

   for (int i = 0; i < formatCount; ++i)
      {
      EImageFormatType imageFormatType
                        = CImageInfo::queryImageFormatTypeDescriptionTable(i);

      // Add another row to the list view
      TListItem *ListItem = FormatListView->Items->Add();

      //  Add the ID string
      name = CImageInfo::queryImageFormatTypeName(imageFormatType);
      ListItem->Caption = name.c_str();

      // Add whether it is an image format used by conventional VTRs
      bool conventionalVTR = CImageInfo::queryIsFormatConventionalVTR(imageFormatType);
      ListItem->SubItems->Add(conventionalVTR ? "Y" : "N");

      //  Add Description 1 & 2
      CImageInfo::queryImageFormatTypeDescription(imageFormatType, descr1, descr2);
      ListItem->SubItems->Add(descr1.c_str());
      ListItem->SubItems->Add(descr2.c_str());
      }
}
//---------------------------------------------------------------------------
 
