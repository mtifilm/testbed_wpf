// AudioInfo.cpp: implementation of the CAudioInfo class.
//
/*
$Header: /usr/local/filmroot/format/source/AudioInfo.cpp,v 1.1.1.1 2002/12/19 00:02:16 mlm Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "err_format.h"
#include "AudioInfo.h"
#include "MTIio.h"
#include "MTIstringstream.h"

//////////////////////////////////////////////////////////////////////
// Static Data
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////

typedef struct 
{
   const char* name;
   EAudioSamplePacking value;
} SAudioSamplePackingMap;

static const SAudioSamplePackingMap audioPackingMap[] =
{
   {"1_Byte",    AI_SAMPLE_DATA_PACKING_1_Byte    },
   {"2_Bytes_L", AI_SAMPLE_DATA_PACKING_2_Bytes_L },
   {"2_Bytes_R", AI_SAMPLE_DATA_PACKING_2_Bytes_R },
   {"3_Bytes_L", AI_SAMPLE_DATA_PACKING_3_Bytes_L },
   {"3_Bytes_R", AI_SAMPLE_DATA_PACKING_3_Bytes_R },
   {"4_Bytes_L", AI_SAMPLE_DATA_PACKING_4_Bytes_L },
   {"4_Bytes_R", AI_SAMPLE_DATA_PACKING_4_Bytes_R },
};

#define AUDIO_SAMPLE_PACKING_TYPE_COUNT (sizeof(audioPackingMap)/sizeof(SAudioSamplePackingMap))


//////////////////////////////////////////////////////////////////////
// Sample Data Type

typedef struct
{
   const char* name;
   EAudioSampleType value;
} SAudioSampleTypeMap;

static const SAudioSampleTypeMap audioSampleTypeMap[] =
{
   {"INT",     AI_SAMPLE_DATA_TYPE_INT   },     // Signed integer
   {"UINT",    AI_SAMPLE_DATA_TYPE_UINT  },     // Unsigned integer
   {"FLOAT",   AI_SAMPLE_DATA_TYPE_FLOAT }      // Floating point
};

#define AUDIO_SAMPLE_TYPE_COUNT (sizeof(audioSampleTypeMap)/sizeof(SAudioSampleTypeMap))

// ----------------------------------------------------------------------------
// Sample Rate Type/Name Mapping Table

struct SSampleRateTypeMap
{
   const char* name;
   EAudioSampleRate value;
};

static const SSampleRateTypeMap sampleRateTypeMap[] =
{
   {"44056",   AI_SAMPLE_RATE_44056 },  // 44.056 KHz (44.1/1.001)
   {"44100",   AI_SAMPLE_RATE_44100 },  // 44.1 KHz (Compact Disks)
   {"48000",   AI_SAMPLE_RATE_48000 }   // 48 KHz (Pro Audio)
};
#define SAMPLE_RATE_TYPE_COUNT (sizeof(sampleRateTypeMap)/sizeof(SSampleRateTypeMap))

//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    querySampleDataPacking
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
EAudioSamplePacking
CAudioInfo::querySampleDataPacking(const string& audioPackingString)
{
   string name;

   for (int i = 0; i < (int)AUDIO_SAMPLE_PACKING_TYPE_COUNT; ++i)
      {
      name = audioPackingMap[i].name;
      if (EqualIgnoreCase(name, audioPackingString))
         return audioPackingMap[i].value;
      }

   return AI_SAMPLE_DATA_PACKING_INVALID;
}
//------------------------------------------------------------------------
//
// Function:    querySampleDataPackingName
//
// Description: Gets name string of a Audio Sample packing type.  This is a
//              static member function which means that it can be called without
//              a CAudioInfo object instance.
//
// Arguments    EAudioSamplePacking audioPacking   Audio Sample Packing type
//
// Returns:     string containing name of Audio Sample type
//
//------------------------------------------------------------------------
string CAudioInfo::querySampleDataPackingName(EAudioSamplePacking audioPacking)
{
   for (int i = 0; i < (int)AUDIO_SAMPLE_PACKING_TYPE_COUNT; ++i)
      {
      if (audioPackingMap[i].value == audioPacking)
         return string(audioPackingMap[i].name);
      }

   return string("INVALID");
}

//------------------------------------------------------------------------
//
// Function:    querySampleDataType
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
EAudioSampleType
CAudioInfo::querySampleDataType(const string& audioSampleTypeString)
{
   string name;

   for (int i = 0; i < (int)AUDIO_SAMPLE_TYPE_COUNT; ++i)
      {
      name = audioSampleTypeMap[i].name;
      if (EqualIgnoreCase(name, audioSampleTypeString))
         return audioSampleTypeMap[i].value;
      }

   return AI_SAMPLE_DATA_TYPE_INVALID;
}
//------------------------------------------------------------------------
//
// Function:    querySampleDataTypeName
//
// Description: Gets name string of a Audio Sample data type.  This is a
//              static member function which means that it can be called without
//              a CAudioInfo object instance.
//
// Arguments    EAudioSampleType sampleType   Audio Sample Data type
//
// Returns:     string containing name of Audio Sample data type
//
//------------------------------------------------------------------------
string CAudioInfo::querySampleDataTypeName(EAudioSampleType sampleType)
{
   for (int i = 0; i < (int)AUDIO_SAMPLE_TYPE_COUNT; ++i)
      {
      if (audioSampleTypeMap[i].value == sampleType)
         return string(audioSampleTypeMap[i].name);
      }

   return string("INVALID");
}

//------------------------------------------------------------------------
//
// Function:    queryFrameRate
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
EAudioSampleRate CAudioInfo::querySampleRate(const string& sampleRateName,
                                             double &newSampleRate)
{
   // Search table of standard sample rates for match with sampleRateName
   for (int i = 0; i < (int)SAMPLE_RATE_TYPE_COUNT; ++i)
      {
      if (EqualIgnoreCase(sampleRateTypeMap[i].name, sampleRateName))
         {
         EAudioSampleRate sampleRateEnum = sampleRateTypeMap[i].value;
         newSampleRate = queryStandardSampleRate(sampleRateEnum);
         return sampleRateEnum;
         }
      }

   // Did not find a match among standard sample rates, so attempt to
   // convert string to a double value
   MTIistringstream strStream;
   strStream.str(sampleRateName);
   strStream >> newSampleRate;
   if ((bool)strStream)
      {
      return AI_SAMPLE_RATE_NON_STANDARD;
      }
   else
      {
      // Could not convert string
      return AI_SAMPLE_RATE_INVALID;
      }
}

//------------------------------------------------------------------------
//
// Function:    querySampleRateName
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
string CAudioInfo::querySampleRateName(EAudioSampleRate sampleRate)
{
   for (int i = 0; i < (int)SAMPLE_RATE_TYPE_COUNT; ++i)
      {
      if (sampleRateTypeMap[i].value == sampleRate)
         return string(sampleRateTypeMap[i].name);
      }

   return string("INVALID");
}

//------------------------------------------------------------------------
//
// Function:    queryStandardSampleRate
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
double CAudioInfo::queryStandardSampleRate(EAudioSampleRate sampleRateEnum)
{
   double sampleRate;

   switch(sampleRateEnum)
      {
      case AI_SAMPLE_RATE_44056 :
         sampleRate = AI_STD_SAMPLE_RATE_44056;
         break;
      case AI_SAMPLE_RATE_44100 :
         sampleRate = AI_STD_SAMPLE_RATE_44100;
         break;
      case AI_SAMPLE_RATE_48000 :
         sampleRate = AI_STD_SAMPLE_RATE_48000;
         break;
      case AI_SAMPLE_RATE_NON_STANDARD :
      case AI_SAMPLE_RATE_INVALID :
      default :
         // These are all non-standard sample rates, so return zero,
         // which will probably cause trouble
         sampleRate = 0.;
         break;
      }

   return(sampleRate);
}

//////////////////////////////////////////////////////////////////////
// Static Validation Functions
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    isChannelCountValid
//
// Description: Validates an audio format channel count.  Must be at least
//              1, upper limit is arbitrarily set at 256
//
// Arguments    int channelCount
//
// Returns:     0 if okay
//              FORMAT_ERROR_INVALID_CHANNEL_COUNT if invalid channel count
//
//------------------------------------------------------------------------
int CAudioInfo::isChannelCountValid(int channelCount)
{
   // Validate channel count.  Upper limit is arbitrary
   if (channelCount < 1 || channelCount > 256)
      return FORMAT_ERROR_INVALID_CHANNEL_COUNT;

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    isSampleDataEndianValid
//
// Description: Validates an audio format sample data endian.
//
// Arguments    EAudioSampleEndian sampleDataEndian
//
// Returns:     0 if okay
//              FORMAT_ERROR_INVALID_SAMPLE_ENDIAN if endian is invalid
//
//------------------------------------------------------------------------
int CAudioInfo::isSampleDataEndianValid(EAudioSampleEndian sampleDataEndian)
{
   // Validate sample data endian.
   switch (sampleDataEndian)
      {
      case AI_SAMPLE_DATA_ENDIAN_LITTLE:
      case AI_SAMPLE_DATA_ENDIAN_BIG:
         // Okay
         break;

      case AI_SAMPLE_DATA_ENDIAN_INVALID:
      default :
         // Ce n'est pas bon
         return FORMAT_ERROR_INVALID_SAMPLE_ENDIAN;
      }

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    isSampleDataPackingValid
//
// Description: Validates an audio format sample data packing.
//
// Arguments    EAudioSamplePacking sampleDataPacking
//
// Returns:     0 if okay
//              FORMAT_ERROR_INVALID_SAMPLE_DATA_PACKING if packing is invalid
//
//------------------------------------------------------------------------
int CAudioInfo::isSampleDataPackingValid(EAudioSamplePacking sampleDataPacking)
{
   // Validate sample data packing
   switch(sampleDataPacking)
      {
      case AI_SAMPLE_DATA_PACKING_1_Byte:
      case AI_SAMPLE_DATA_PACKING_2_Bytes_L:
      case AI_SAMPLE_DATA_PACKING_2_Bytes_R:
      case AI_SAMPLE_DATA_PACKING_3_Bytes_L:
      case AI_SAMPLE_DATA_PACKING_3_Bytes_R:
      case AI_SAMPLE_DATA_PACKING_4_Bytes_L:
      case AI_SAMPLE_DATA_PACKING_4_Bytes_R:
         // Okay
         break;
      case AI_SAMPLE_DATA_PACKING_INVALID:
      default:
         // Not good
         return FORMAT_ERROR_INVALID_SAMPLE_DATA_PACKING;
      }

   return 0; // Okay
}

//------------------------------------------------------------------------
//
// Function:    isSampleDataSizeValid
//
// Description: Validates an audio format sample data size.
//              Allowable range is 8 - 32 bits.
//
// Arguments    int sampleDataSize
//
// Returns:     0 if okay
//              FORMAT_ERROR_INVALID_SAMPLE_DATA_SIZE if size is invalid
//
//------------------------------------------------------------------------
int CAudioInfo::isSampleDataSizeValid(int sampleDataSize)
{
   // Validate the sample data size
   if (sampleDataSize < AI_SAMPLE_BIT_COUNT_MIN
       || sampleDataSize > AI_SAMPLE_BIT_COUNT_MAX)
      return FORMAT_ERROR_INVALID_SAMPLE_DATA_SIZE;

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    isSampleDataTypeValid
//
// Description: Validates an audio format sample data type.
//              Allowable range is 8 - 32 bits.
//
// Arguments    EAudioSampleType sampleDataType
//
// Returns:     0 if okay
//              FORMAT_ERROR_INVALID_SAMPLE_DATA_TYPE if type is invalid
//
//------------------------------------------------------------------------
int CAudioInfo::isSampleDataTypeValid(EAudioSampleType sampleDataType)
{
   // Validate the sample data type
   switch (sampleDataType)
      {
      case AI_SAMPLE_DATA_TYPE_INT:       // Signed integer
      case AI_SAMPLE_DATA_TYPE_UINT:      // Unsigned integer
      case AI_SAMPLE_DATA_TYPE_FLOAT:     // Floating point
         // Okay
         break;
      case AI_SAMPLE_DATA_TYPE_INVALID:
      default :
         // Invalid
         return FORMAT_ERROR_INVALID_SAMPLE_DATA_TYPE;
      }

   return 0;
}

//------------------------------------------------------------------------
//
// Function:    isSampleRateEnumValid
//
// Description: Validates an audio format sample rate enumerator.
//              44,056, 44,100 and 48,000 Hz are acceptable.
//              Non-standard rates are not considered valid (although
//              they could be implemented).
//
// Arguments    EAudioSampleRate sampleRateEnum
//
// Returns:     0 if okay
//              FORMAT_ERROR_INVALID_SAMPLE_RATE if sample rate is invalid
//
//------------------------------------------------------------------------
int CAudioInfo::isSampleRateEnumValid(EAudioSampleRate sampleRateEnum)
{
   // Validate Sample Rate
   switch(sampleRateEnum)
      {
      case AI_SAMPLE_RATE_44056 :
      case AI_SAMPLE_RATE_44100 :
      case AI_SAMPLE_RATE_48000 :
         // Okay
         break;
      case AI_SAMPLE_RATE_NON_STANDARD :
      case AI_SAMPLE_RATE_INVALID :
      default :
         // No good
         return FORMAT_ERROR_INVALID_SAMPLE_RATE;
      }

   return 0;
}

