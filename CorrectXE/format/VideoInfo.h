// VideoInfo.h
//
// Video I/O Constants. 
// 
// Note: Image constants are found in ImageInfo.h
//
// Generally these constants are independent of the video I/O board,
// platform (SGI, Intel, etc.) and OS (IRIX, NT, Linux, etc.)
//
//////////////////////////////////////////////////////////////////////

#ifndef VIDEO_INFO_H
#define VIDEO_INFO_H

#include "formatDLL.h"

//////////////////////////////////////////////////////////////////////

// Note: Enumerator values are explicitly set to make file compatibility
//       easier.  New enumerators should be added with explicit values
//       and not repeat a value.

//////////////////////////////////////////////////////////////////////
// Video Sync Source

enum EVideoSyncSource
{
   VI_SYNC_SOURCE_INVALID = 0,
   VI_SYNC_SOURCE_INTERNAL = 1,
   VI_SYNC_SOURCE_VIDEO_INPUT_A = 2,
   VI_SYNC_SOURCE_VIDEO_INPUT_B = 3,
   VI_SYNC_SOURCE_EXTERNAL_525_60 = 4,
   VI_SYNC_SOURCE_EXTERNAL_625_50 = 5,
   VI_SYNC_SOURCE_EXTERNAL_750_60 = 6,
   VI_SYNC_SOURCE_EXTERNAL_750_5994 = 7,
   VI_SYNC_SOURCE_EXTERNAL_1125_60 = 8,
   VI_SYNC_SOURCE_EXTERNAL_1125_5994 = 9,
   VI_SYNC_SOURCE_EXTERNAL_1125_50 = 10,
   VI_SYNC_SOURCE_EXTERNAL_1125_48 = 11,
   VI_SYNC_SOURCE_EXTERNAL_1125_4796 = 12,
   VI_SYNC_SOURCE_EXTERNAL_1125_25 = 13,
   VI_SYNC_SOURCE_EXTERNAL_1125_24 = 14,
   VI_SYNC_SOURCE_EXTERNAL_1125_2398 = 15,
   VI_SYNC_SOURCE_EXTERNAL_750_50 = 16,
   VI_SYNC_SOURCE_NONE = 17
};

//////////////////////////////////////////////////////////////////////
// Video Field Rate
//
// Note: In video engineering terminology the "field rate" is more 
//       commonly known as the "frame rate" even though it measures
//       how fast the fields leave the video board

enum EVideoFieldRate
{
   VI_VIDEO_FIELD_RATE_INVALID = 0,
   VI_VIDEO_FIELD_RATE_60 = 1,
   VI_VIDEO_FIELD_RATE_5994 = 2,
   VI_VIDEO_FIELD_RATE_50 = 3,
   VI_VIDEO_FIELD_RATE_48 = 4,
   VI_VIDEO_FIELD_RATE_4796 = 5,
   VI_VIDEO_FIELD_RATE_30 = 6,
   VI_VIDEO_FIELD_RATE_2997 = 7,
   VI_VIDEO_FIELD_RATE_25 = 8,
   VI_VIDEO_FIELD_RATE_24 = 9,
   VI_VIDEO_FIELD_RATE_2398 = 10,
   VI_VIDEO_FIELD_RATE_1498 = 11,
   VI_VIDEO_FIELD_RATE_15 = 12,
   VI_VIDEO_FIELD_RATE_2997_25 = 13,
   VI_VIDEO_FIELD_RATE_30_25 = 14,
   VI_VIDEO_FIELD_RATE_1498_25 = 15,
   VI_VIDEO_FIELD_RATE_15_25 = 16
};

//////////////////////////////////////////////////////////////////////
// Video Progressive Type
//
// Note: "progressive video" has two flavors:  P and SF.  These
//       correspond to "progressive" and "segmented fields"

enum EVideoProgressiveType
{
   VI_VIDEO_PROGRESSIVE_TYPE_INVALID = 0,
   VI_VIDEO_PROGRESSIVE_TYPE_P = 1,
   VI_VIDEO_PROGRESSIVE_TYPE_SF = 2
};

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Video Loopback
//
// Note: some boards allow output video to loop back in incoming

enum EVideoLoopback
{
   VI_VIDEO_LOOPBACK_INVALID = 0,
   VI_VIDEO_LOOPBACK_OFF = 1,
   VI_VIDEO_LOOPBACK_ON = 2
};

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Video Precision
//
// Note: some boards allow output video to have either 8 bits or 10 bits
// of precision.  This precision is independent of the packing of the
// material.

enum EVideoExternalPrecision
{
   VI_EXTERNAL_PRECISION_INVALID = 0,
   VI_EXTERNAL_PRECISION_8 = 1,
   VI_EXTERNAL_PRECISION_10 = 2
};

//////////////////////////////////////////////////////////////////////

class CIniAssociations;

extern MTI_FORMATDLL_API CIniAssociations SynchAssociations;
extern MTI_FORMATDLL_API CIniAssociations VideoLoopbackAssociations;
extern MTI_FORMATDLL_API CIniAssociations VideoPrecisionAssociations;
extern MTI_FORMATDLL_API CIniAssociations VideoProgressiveAssociations;
extern MTI_FORMATDLL_API CIniAssociations VideoFieldRateAssociations;

#endif // #ifndef VIDEO_INFO_H
